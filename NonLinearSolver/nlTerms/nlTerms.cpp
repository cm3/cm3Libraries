//
// C++ Interface: terms
//
// Description: Basic term for non linear solver
//
//
// Author:  <Gauthier BECKER>, (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "nlTerms.h"
#include "MInterfaceElement.h"
#include "unknownField.h"
#include "ipField.h"
#include "nlsFunctionSpace.h"
/*
 template<> void BilinearTermPerturbation<double>::get(MElement *ele,int npts,IntPt *GP,fullMatrix<double> &m) const
{
 
  static fullVector<double> fval, fvalplus;
  MInterfaceElement *iele = dynamic_cast<MInterfaceElement*>(ele);
  std::vector<Dof> R;
	nonLinearTermBase<double>* nonlinearTerm =  dynamic_cast<nonLinearTermBase<double>*>(_nlterm);
	if (nonlinearTerm != NULL)
  {
		if(nonlinearTerm->isData())
			nonlinearTerm->set(&disp);
	}
  
  // Size of force vector
  _nlterm->get(ele,npts,GP,fval);
  
  if(iele == NULL)
  { // compute on element
    int nbdof = space1.getNumKeys(ele);
    m.resize(nbdof,nbdof,true); // set operation after
    disp.resize(nbdof);
    space1.getKeys(ele,R);
    _ufield->get(R,disp);
    
    // compute fp with pert
    for(int i=0;i<nbdof;i++)
    {
      // pertubation +
      disp(i) += _eps;
      _dom->computeIpv(_ipf->getAips(),ele,IPStateBase::current,_dom->getMaterialLaw(),disp,false);
      _nlterm->get(ele,npts,GP,fvalplus);
      for (int j=0; j< nbdof; j++)
      {
        m(j,i) =  (fvalplus(j) - fval(j))/_eps;
      }
      // restore dof value
      disp(i) -= _eps;
    };
    // back ipstate as previous
    _dom->computeIpv(_ipf->getAips(),ele,IPStateBase::current,_dom->getMaterialLaw(),disp,true);
  }
  else
  {
    // interface element
    dgPartDomain *dgdom = static_cast<dgPartDomain*>(_dom);
    // get displacement of element
    space1.getKeys(iele->getElem(0),R);
    if(iele->getElem(0) != iele->getElem(1))
    {
      space2.getKeys(iele->getElem(1),R);
    }
    if (dgdom->IsMultipleFieldFormulation()){
      dgdom->getInterfaceFunctionSpace()->getKeys(ele,R);
    }
    // unknonwn values
    disp.resize(R.size());
    _ufield->get(R,disp);

    bool virt=false;
    if(iele->getElem(0) == iele->getElem(1))
    {
      virt = true;
    }
    
    // store in dispm and dispp
    int nbdof = 0;
    int nbdof_m = space1.getNumKeys(iele->getElem(0));
    int nbdof_p = 0;
    int nbdof_extra = 0;
    dispm.resize(nbdof_m);
    for(int i=0;i<nbdof_m;i++)
    {
      dispm(i) = disp(i);
    }
    if(!virt)
    {
      // interface element
      nbdof_p = space2.getNumKeys(iele->getElem(1));
      dispp.resize(nbdof_p);
      for(int i=0;i<nbdof_p;i++)
      {
        dispp(i) = disp(i+nbdof_m);
      }

      if (dgdom->IsMultipleFieldFormulation())
      {
        nbdof_extra = dgdom->getInterfaceFunctionSpace()->getNumKeys(ele);
        dispExtra.resize(nbdof_extra);
        for(int i=0;i<nbdof_extra;i++)
        {
          dispExtra(i) = disp(i+nbdof_m+nbdof_p);
        }
      }
      nbdof = nbdof_m+nbdof_p+nbdof_extra;
      m.resize(nbdof_m+nbdof_p+nbdof_extra,nbdof_m+nbdof_p+nbdof_extra);
    }
    else
    {
      // virtual element
      nbdof = nbdof_m;
      m.resize(nbdof_m,nbdof_m,true);
    }
    
    // Perturbation on minus element
    for(int i=0;i<nbdof_m;i++)
    {
      // dof perturbation +
      disp(i) += _eps;
      dispm(i)+=_eps;
      dgdom->computeIpv(_ipf->getAips(),iele,GP,IPStateBase::current,dgdom->getMinusDomain(),dgdom->getPlusDomain(),
                        dgdom->getMaterialLawMinus(),dgdom->getMaterialLawPlus(),dispm,dispp,dispExtra,virt,false,false); // 0 for - elem and npts for + elem
      _nlterm->get(ele,npts,GP,fvalplus);
      
      for (int j=0; j< nbdof; j++)
      {
        m(j,i) =  (fvalplus(j) - fval(j))/_eps;
      }
      disp(i) -= _eps;
      dispm(i) -=_eps;
    }
    //
    if(!virt)
    { // Otherwise virtual interface element
      // Perturbation on plus element
      for(int i=0;i<nbdof_p;i++)
      {
        // dof perturbation +
        disp(i+nbdof_m) += _eps;
        dispp(i)+=_eps;
        dgdom->computeIpv(_ipf->getAips(),iele,GP,IPStateBase::current,dgdom->getMinusDomain(),dgdom->getPlusDomain(),
                          dgdom->getMaterialLawMinus(),dgdom->getMaterialLawPlus(),dispm,dispp,dispExtra,virt,false,false); // 0 for - elem and npts for + elem
        _nlterm->get(ele,npts,GP,fvalplus);
        //
        for (int j=0; j< nbdof; j++)
        {
          m(j,i+nbdof_m) =  (fvalplus(j) - fval(j))/_eps;
        }
        
        // dof perturbation +
        disp(i+nbdof_m)-=_eps;
        dispp(i)-=_eps;
      }

      //
      if (dgdom->IsMultipleFieldFormulation())
      {
        for(int i=0;i<nbdof_extra;i++){
          // dof perturbation +
          disp(i+nbdof_m+nbdof_p)+=_eps;
          dispExtra(i)+=_eps;
          dgdom->computeIpv(_ipf->getAips(),iele,GP,IPStateBase::current,dgdom->getMinusDomain(),dgdom->getPlusDomain(),
                            dgdom->getMaterialLawMinus(),dgdom->getMaterialLawPlus(),dispm,dispp,dispExtra,virt,false,false); // 0 for - elem and npts for + elem
          _nlterm->get(ele,npts,GP,fvalplus);
          for (int j=0; j< nbdof; j++)
          {
            m(j,i+nbdof_m+nbdof_p) =  (fvalplus(j) - fval(j))/_eps;
          }
          disp(i+nbdof_m+nbdof_p)-=_eps;
          dispExtra(i)-=_eps;
        }
      }
    }
          // restore ipv values
    dgdom->computeIpv(_ipf->getAips(),iele,GP,IPStateBase::current,dgdom->getMinusDomain(),dgdom->getPlusDomain(),
                        dgdom->getMaterialLawMinus(),dgdom->getMaterialLawPlus(),dispm,dispp,dispExtra,virt,false, false);

  }
  //m.print("stiff pertu\n");
}
 */
template<> void BilinearTermPerturbation<double>::get(MElement *ele,int npts,IntPt *GP,fullMatrix<double> &m) const
{
  MInterfaceElement *iele = dynamic_cast<MInterfaceElement*>(ele);
  std::vector<Dof> R;
  nonLinearTermBase<double>* nonlinearTerm =  dynamic_cast<nonLinearTermBase<double>*>(_nlterm);
  if (nonlinearTerm != NULL){
    if(nonlinearTerm->isData())
      nonlinearTerm->set(&disp);
  }
  if(iele == NULL){ // compute on element
    int nbdof = space1.getNumKeys(ele);
    m.resize(nbdof,nbdof,false); // set operation after
    disp.resize(nbdof);
    space1.getKeys(ele,R);
    _ufield->get(R,disp);

    // Size of force vector
    fp.resize(nbdof);
    fm.resize(nbdof);

    for(int i=0;i<nbdof;i++){
      // pertubation +
      disp(i) += _eps;
      _dom->computeIpv(_ipf->getAips(),ele,IPStateBase::current,_dom->getMaterialLaw(),disp,false);

      _nlterm->get(ele,npts,GP,fp);
      // perturbation -
      disp(i) -=_twoeps;
      _dom->computeIpv(_ipf->getAips(),ele,IPStateBase::current,_dom->getMaterialLaw(),disp,false);
      _nlterm->get(ele,npts,GP,fm);
      // restore dof value
      disp(i) += _eps;
      fp.axpy(fm,-1);
      m.copyOneColumn(fp,i); // divide by 1/(2eps) at the end
    }
    _dom->computeIpv(_ipf->getAips(),ele,IPStateBase::current,_dom->getMaterialLaw(),disp,true);
  }
  else{
    dgPartDomain *dgdom = static_cast<dgPartDomain*>(_dom);
    // get displacement of element
    space1.getKeys(iele->getElem(0),R);
    if(iele->getElem(0) != iele->getElem(1))
      space2.getKeys(iele->getElem(1),R);

    disp.resize(R.size());
    _ufield->get(R,disp);

    bool virt=false;
    if(iele->getElem(0) == iele->getElem(1)) virt = true;
    // store in dispm and dispp
    int nbdof_m = space1.getNumKeys(iele->getElem(0));
    int nbdof_p = 0;
    int nbdof_extra = 0;
    dispm.resize(nbdof_m);
    for(int i=0;i<nbdof_m;i++)
      dispm(i) = disp(i);
    if(!virt){
      nbdof_p = space2.getNumKeys(iele->getElem(1));
      dispp.resize(nbdof_p);
      for(int i=0;i<nbdof_p;i++)
        dispp(i) = disp(i+nbdof_m);

      m.resize(nbdof_m+nbdof_p,nbdof_m+nbdof_p);
    }
    else
      m.resize(nbdof_m,nbdof_m,false);
    fp.resize(disp.size()); // use disp size to know the number of dof (here unknow if interafce or virtual interface)
    fm.resize(disp.size());

    // Perturbation on minus element
    for(int i=0;i<nbdof_m;i++){
      // dof perturbation +
      disp(i)+=_eps;
      dispm(i)+=_eps;
      dgdom->computeIpv(_ipf->getAips(),iele,GP,IPStateBase::current,dgdom->getMinusDomain(),dgdom->getPlusDomain(),
                        dgdom->getMaterialLawMinus(),dgdom->getMaterialLawPlus(),dispm,dispp,virt,false,false); // 0 for - elem and npts for + elem
      _nlterm->get(ele,npts,GP,fp);
      // dof perturbation -
      disp(i)-=_twoeps;
      dispm(i)-=_twoeps;
      dgdom->computeIpv(_ipf->getAips(),iele,GP,IPStateBase::current,dgdom->getMinusDomain(),dgdom->getPlusDomain(),
                        dgdom->getMaterialLawMinus(),dgdom->getMaterialLawPlus(),dispm,dispp,virt,false,false); // 0 for - elem and npts for + elem
      _nlterm->get(ele,npts,GP,fm);
      disp(i)+=_eps;
      dispm(i)+=_eps;
      fp.axpy(fm,-1);
      m.copyOneColumn(fp,i); // divide by 1/(2eps) at the end
    }
    // restore ipv value
    dgdom->computeIpv(_ipf->getAips(),iele,GP,IPStateBase::current,dgdom->getMinusDomain(), dgdom->getPlusDomain(),
                      dgdom->getMaterialLawMinus(),dgdom->getMaterialLawPlus(),dispm,dispp,virt,true,false); // 0 for - elem and npts for + elem
    if(!virt){ // Otherwise virtual interface element
      // Perturbation on plus element
      for(int i=0;i<nbdof_p;i++){
        // dof perturbation +
        disp(i+nbdof_m)+=_eps;
        dispp(i)+=_eps;
        dgdom->computeIpv(_ipf->getAips(),iele,GP,IPStateBase::current,dgdom->getMinusDomain(),dgdom->getPlusDomain(),
                          dgdom->getMaterialLawMinus(),dgdom->getMaterialLawPlus(),dispm,dispp,virt,false,false); // 0 for - elem and npts for + elem
        _nlterm->get(ele,npts,GP,fp);
        // dof perturbation +
        disp(i+nbdof_m)-=_twoeps;
        dispp(i)-=_twoeps;
        dgdom->computeIpv(_ipf->getAips(),iele,GP,IPStateBase::current,dgdom->getMinusDomain(),dgdom->getPlusDomain(),
                          dgdom->getMaterialLawMinus(), dgdom->getMaterialLawPlus(),dispm,dispp,virt,false,false); // 0 for - elem and npts for + elem
        _nlterm->get(ele,npts,GP,fm);
        disp(i+nbdof_m)+=_eps;
        dispp(i)+=_eps;
        fp.axpy(fm,-1);
        m.copyOneColumn(fp,i+nbdof_m); // divide by 1/(2eps) at the end
      }
      // restore ipv values
      dgdom->computeIpv(_ipf->getAips(),iele,GP,IPStateBase::current,dgdom->getMinusDomain(),dgdom->getPlusDomain(),
                        dgdom->getMaterialLawMinus(),dgdom->getMaterialLawPlus(),dispm,dispp,virt,true, false);
    }
  }
  // divide all components by 1/2eps
  m.scale(_onedivtwoeps);
  //m.print("stiff pertu\n");
}

void mass3D::get(MElement *ele,int npts,IntPt *GP,fullMatrix<double> &m) const
{
  if(ele->getTypeForMSH() == MSH_TRI_3){
    // integration on Gauss' points to know area
    double area = 0.;
    double onediv3 = 1./3.;
    int nbdof = BilinearTerm<double,double>::space1.getNumKeys(ele);
    //int nbFF = ele->getNumShapeFunctions();
    m.resize(nbdof, nbdof,true); // true --> setAll 0.

    for (int i = 0; i < npts; i++)
    {
      double weight = GP[i].weight;
      double detJ = ele->getJacobianDeterminant(GP[i].pt[0],GP[i].pt[1],GP[i].pt[2]);
      area+=weight*detJ;
    }
    double massele = area*_rho; // rho is the density by unit thickness
    // mass in three directions
    for(int kk=0;kk<3;kk++){
       int nbFFRow = (dynamic_cast<const nlsFunctionSpace<double>&>( BilinearTerm<double,double>::space1)).getNumShapeFunctions(ele,kk);
       int nbFFTotalLastRow = (dynamic_cast<const nlsFunctionSpace<double>&>( BilinearTerm<double,double>::space1)).getShapeFunctionsIndex(ele,kk);
       m(nbFFTotalLastRow,nbFFTotalLastRow) += onediv3*massele;
       m(1+nbFFTotalLastRow,1+nbFFTotalLastRow) += onediv3*massele;
       m(2+nbFFTotalLastRow,2+nbFFTotalLastRow) += onediv3*massele;
    }
    // non local
    for (int nl = 0; nl< getNumberNonLocalVariable(); nl++)
    {                
      int nbFFRow = (dynamic_cast<const nlsFunctionSpace<double>&>( BilinearTerm<double,double>::space1)).getNumShapeFunctions(ele,3+nl);
      int nbFFTotalLastRow = (dynamic_cast<const nlsFunctionSpace<double>&>( BilinearTerm<double,double>::space1)).getShapeFunctionsIndex(ele,3+nl);
      if(nbFFRow==3)
      {
        m(nbFFTotalLastRow,nbFFTotalLastRow) += onediv3*massele*_rhoNL/_rho;
        m(1+nbFFTotalLastRow,1+nbFFTotalLastRow) += onediv3*massele*_rhoNL/_rho;
        m(2+nbFFTotalLastRow,2+nbFFTotalLastRow) += onediv3*massele*_rhoNL/_rho;              
      }
      else
        Msg::Error("Mass matrix is not implemented for TRI_3 with non local variable and %d non local nodes", nbFFRow);
    } 
    //m.print("mass matrix");
  }
  else if(ele->getTypeForMSH() == MSH_TRI_6){
    // integration on Gauss' points to know area
    double area = 0.;
    double onediv12 = 1./12.;
    int nbdof = BilinearTerm<double,double>::space1.getNumKeys(ele);
    //int nbFF = ele->getNumShapeFunctions();
    m.resize(nbdof, nbdof,true); // true --> setAll 0.

    for (int i = 0; i < npts; i++)
    {
      double weight = GP[i].weight;
      double detJ = ele->getJacobianDeterminant(GP[i].pt[0],GP[i].pt[1],GP[i].pt[2]);
      area+=weight*detJ;
    }
    double massele = area*_rho; // rho is the density by unit thickness
    // mass in three directions
    for(int kk=0;kk<3;kk++){
       int nbFFRow = (dynamic_cast<const nlsFunctionSpace<double>&>( BilinearTerm<double,double>::space1)).getNumShapeFunctions(ele,kk);
       int nbFFTotalLastRow = (dynamic_cast<const nlsFunctionSpace<double>&>( BilinearTerm<double,double>::space1)).getShapeFunctionsIndex(ele,kk);
       m(nbFFTotalLastRow,nbFFTotalLastRow) += onediv12*massele;
       m(1+nbFFTotalLastRow,1+nbFFTotalLastRow) += onediv12*massele;
       m(2+nbFFTotalLastRow,2+nbFFTotalLastRow) += onediv12*massele;
       m(3+nbFFTotalLastRow,3+nbFFTotalLastRow) += 0.25*massele;
       m(4+nbFFTotalLastRow,4+nbFFTotalLastRow) += 0.25*massele;
       m(5+nbFFTotalLastRow,5+nbFFTotalLastRow) += 0.25*massele;
    }
    // non local
    for (int nl = 0; nl< getNumberNonLocalVariable(); nl++)
    {                
      int nbFFRow = (dynamic_cast<const nlsFunctionSpace<double>&>( BilinearTerm<double,double>::space1)).getNumShapeFunctions(ele,3+nl);
      int nbFFTotalLastRow = (dynamic_cast<const nlsFunctionSpace<double>&>( BilinearTerm<double,double>::space1)).getShapeFunctionsIndex(ele,3+nl);
      if(nbFFRow==3)
      {
        m(nbFFTotalLastRow,nbFFTotalLastRow) += massele*_rhoNL/_rho/3.;
        m(1+nbFFTotalLastRow,1+nbFFTotalLastRow) += massele*_rhoNL/_rho/3.;
        m(2+nbFFTotalLastRow,2+nbFFTotalLastRow) += massele*_rhoNL/_rho/3.;              
      }
      else if(nbFFRow==6)
      {
        m(nbFFTotalLastRow,nbFFTotalLastRow) += onediv12*massele*_rhoNL/_rho;
        m(1+nbFFTotalLastRow,1+nbFFTotalLastRow) += onediv12*massele*_rhoNL/_rho;
        m(2+nbFFTotalLastRow,2+nbFFTotalLastRow) += onediv12*massele*_rhoNL/_rho;
        m(3+nbFFTotalLastRow,3+nbFFTotalLastRow) += 0.25*massele*_rhoNL/_rho;
        m(4+nbFFTotalLastRow,4+nbFFTotalLastRow) += 0.25*massele*_rhoNL/_rho;
        m(5+nbFFTotalLastRow,5+nbFFTotalLastRow) += 0.25*massele*_rhoNL/_rho;       
      }
      else
        Msg::Error("Mass matrix is not implemented for TRI_6 with non local variable and %d non local nodes", nbFFRow);
    } 
    //m.print("mass matrix");
  }
  else if(ele->getTypeForMSH() == MSH_QUA_4){
    // integration on Gauss' points to know area
    double area = 0.;
    double onediv4 = 1./4.;
    int nbdof = BilinearTerm<double,double>::space1.getNumKeys(ele);
    //int nbFF = ele->getNumShapeFunctions();
    m.resize(nbdof, nbdof,true); // true --> setAll 0.

    for (int i = 0; i < npts; i++)
    {
      double weight = GP[i].weight;
      double detJ = ele->getJacobianDeterminant(GP[i].pt[0],GP[i].pt[1],GP[i].pt[2]);
      area+=weight*detJ;
    }
    double massele = area*_rho; // rho is the density by unit thickness
    // mass in three directions
    for(int kk=0;kk<3;kk++){
       int nbFFRow = (dynamic_cast<const nlsFunctionSpace<double>&>( BilinearTerm<double,double>::space1)).getNumShapeFunctions(ele,kk);
       int nbFFTotalLastRow = (dynamic_cast<const nlsFunctionSpace<double>&>( BilinearTerm<double,double>::space1)).getShapeFunctionsIndex(ele,kk);
       m(nbFFTotalLastRow,nbFFTotalLastRow) += onediv4*massele;
       m(1+nbFFTotalLastRow,1+nbFFTotalLastRow) += onediv4*massele;
       m(2+nbFFTotalLastRow,2+nbFFTotalLastRow) += onediv4*massele;
       m(3+nbFFTotalLastRow,3+nbFFTotalLastRow) += onediv4*massele;
    }
    // non local
    for (int nl = 0; nl< getNumberNonLocalVariable(); nl++)
    {                
      int nbFFRow = (dynamic_cast<const nlsFunctionSpace<double>&>( BilinearTerm<double,double>::space1)).getNumShapeFunctions(ele,3+nl);
      int nbFFTotalLastRow = (dynamic_cast<const nlsFunctionSpace<double>&>( BilinearTerm<double,double>::space1)).getShapeFunctionsIndex(ele,3+nl);
      if(nbFFRow==4)
      {
        m(nbFFTotalLastRow,nbFFTotalLastRow) += onediv4*massele*_rhoNL/_rho;
        m(1+nbFFTotalLastRow,1+nbFFTotalLastRow) += onediv4*massele*_rhoNL/_rho;
        m(2+nbFFTotalLastRow,2+nbFFTotalLastRow) += onediv4*massele*_rhoNL/_rho;
        m(3+nbFFTotalLastRow,3+nbFFTotalLastRow) += onediv4*massele*_rhoNL/_rho;
      }
      else
        Msg::Error("Mass matrix is not implemented for QUAD_4 with non local variable and %d non local nodes", nbFFRow);
    } 
    //m.print("mass matrix");
  }
  else if(ele->getTypeForMSH() == MSH_QUA_8){
    // integration on Gauss' points to know area
    double area = 0.;
    double onediv4 = 1./4.;
    double onediv36 = 1./36.;
    int nbdof = BilinearTerm<double,double>::space1.getNumKeys(ele);
    //int nbFF = ele->getNumShapeFunctions();
    m.resize(nbdof, nbdof,true); // true --> setAll 0.

    for (int i = 0; i < npts; i++)
    {
      double weight = GP[i].weight;
      double detJ = ele->getJacobianDeterminant(GP[i].pt[0],GP[i].pt[1],GP[i].pt[2]);
      area+=weight*detJ;
    }
    double massele = area*_rho; // rho is the density by unit thickness
    // mass in three directions
    for(int kk=0;kk<3;kk++){
       int nbFFRow = (dynamic_cast<const nlsFunctionSpace<double>&>( BilinearTerm<double,double>::space1)).getNumShapeFunctions(ele,kk);
       int nbFFTotalLastRow = (dynamic_cast<const nlsFunctionSpace<double>&>( BilinearTerm<double,double>::space1)).getShapeFunctionsIndex(ele,kk);
       m(nbFFTotalLastRow,nbFFTotalLastRow) += onediv36*massele;
       m(1+nbFFTotalLastRow,1+nbFFTotalLastRow) += onediv36*massele;
       m(2+nbFFTotalLastRow,2+nbFFTotalLastRow) += onediv36*massele;
       m(3+nbFFTotalLastRow,3+nbFFTotalLastRow) += onediv36*massele;
       m(4+nbFFTotalLastRow,4+nbFFTotalLastRow) += onediv36*massele*8.;
       m(5+nbFFTotalLastRow,5+nbFFTotalLastRow) += onediv36*massele*8.;
       m(6+nbFFTotalLastRow,6+nbFFTotalLastRow) += onediv36*massele*8.;
       m(7+nbFFTotalLastRow,7+nbFFTotalLastRow) += onediv36*massele*8.;       
    }
    // non local
    for (int nl = 0; nl< getNumberNonLocalVariable(); nl++)
    {                
      int nbFFRow = (dynamic_cast<const nlsFunctionSpace<double>&>( BilinearTerm<double,double>::space1)).getNumShapeFunctions(ele,3+nl);
      int nbFFTotalLastRow = (dynamic_cast<const nlsFunctionSpace<double>&>( BilinearTerm<double,double>::space1)).getShapeFunctionsIndex(ele,3+nl);
      if(nbFFRow==4)
      {
        m(nbFFTotalLastRow,nbFFTotalLastRow) += onediv4*massele*_rhoNL/_rho;
        m(1+nbFFTotalLastRow,1+nbFFTotalLastRow) += onediv4*massele*_rhoNL/_rho;
        m(2+nbFFTotalLastRow,2+nbFFTotalLastRow) += onediv4*massele*_rhoNL/_rho;
        m(3+nbFFTotalLastRow,3+nbFFTotalLastRow) += onediv4*massele*_rhoNL/_rho;
      }
      else if(nbFFRow==8)
      {
        m(nbFFTotalLastRow,nbFFTotalLastRow) += onediv36*massele*_rhoNL/_rho;
        m(1+nbFFTotalLastRow,1+nbFFTotalLastRow) += onediv36*massele*_rhoNL/_rho;
        m(2+nbFFTotalLastRow,2+nbFFTotalLastRow) += onediv36*massele*_rhoNL/_rho;
        m(3+nbFFTotalLastRow,3+nbFFTotalLastRow) += onediv36*massele*_rhoNL/_rho;
        m(4+nbFFTotalLastRow,4+nbFFTotalLastRow) += onediv36*massele*8.*_rhoNL/_rho;
        m(5+nbFFTotalLastRow,5+nbFFTotalLastRow) += onediv36*massele*8.*_rhoNL/_rho;
        m(6+nbFFTotalLastRow,6+nbFFTotalLastRow) += onediv36*massele*8.*_rhoNL/_rho;
        m(7+nbFFTotalLastRow,7+nbFFTotalLastRow) += onediv36*massele*8.*_rhoNL/_rho;         
      }
      else
        Msg::Error("Mass matrix is not implemented for QUAD_8 with non local variable and %d non local nodes", nbFFRow);
    } 
    //m.print("mass matrix");
  }
  else if(ele->getTypeForMSH() == MSH_TET_4){
    // integration on Gauss' points to know area
    double vol = 0.;
    double onediv4 = 1./4.;
    int nbdof = BilinearTerm<double,double>::space1.getNumKeys(ele);
    //int nbFF = ele->getNumShapeFunctions();
    m.resize(nbdof, nbdof,true); // true --> setAll 0.
    //double jac[3][3];
    for (int i = 0; i < npts; i++)
    {
      double weight = GP[i].weight;
      double detJ = ele->getJacobianDeterminant(GP[i].pt[0],GP[i].pt[1],GP[i].pt[2]);
      vol+=weight*detJ;
    }
    double massele = vol*_rho;
    // mass in three directions
    for(int kk=0;kk<3;kk++){
       int nbFFRow = (dynamic_cast<const nlsFunctionSpace<double>&>( BilinearTerm<double,double>::space1)).getNumShapeFunctions(ele,kk);
       int nbFFTotalLastRow = (dynamic_cast<const nlsFunctionSpace<double>&>( BilinearTerm<double,double>::space1)).getShapeFunctionsIndex(ele,kk);
       m(nbFFTotalLastRow,nbFFTotalLastRow) += onediv4*massele;
       m(1+nbFFTotalLastRow,1+nbFFTotalLastRow) += onediv4*massele;
       m(2+nbFFTotalLastRow,2+nbFFTotalLastRow) += onediv4*massele;
       m(3+nbFFTotalLastRow,3+nbFFTotalLastRow) += onediv4*massele;
    }
    // non local
    for (int nl = 0; nl< getNumberNonLocalVariable(); nl++)
    {                
      int nbFFRow = (dynamic_cast<const nlsFunctionSpace<double>&>( BilinearTerm<double,double>::space1)).getNumShapeFunctions(ele,3+nl);
      int nbFFTotalLastRow = (dynamic_cast<const nlsFunctionSpace<double>&>( BilinearTerm<double,double>::space1)).getShapeFunctionsIndex(ele,3+nl);
      if(nbFFRow==4)
      {
        m(nbFFTotalLastRow,nbFFTotalLastRow) += onediv4*massele*_rhoNL/_rho;
        m(1+nbFFTotalLastRow,1+nbFFTotalLastRow) += onediv4*massele*_rhoNL/_rho;
        m(2+nbFFTotalLastRow,2+nbFFTotalLastRow) += onediv4*massele*_rhoNL/_rho;      
        m(3+nbFFTotalLastRow,3+nbFFTotalLastRow) += onediv4*massele*_rhoNL/_rho;                      
      }
      else
        Msg::Error("Mass matrix is not implemented for TET_4 with non local variable and %d non local nodes", nbFFRow);
    }
  }
  else if(ele->getTypeForMSH() == MSH_TET_10){
    // integration on Gauss' points to know area
    double vol = 0.;
    double onediv32 = 1./32.;
    int nbdof = BilinearTerm<double,double>::space1.getNumKeys(ele);
    //int nbFF = ele->getNumShapeFunctions();
    m.resize(nbdof, nbdof,true); // true --> setAll 0.
    //double jac[3][3];
    for (int i = 0; i < npts; i++)
    {
      double weight = GP[i].weight;
      double detJ = ele->getJacobianDeterminant(GP[i].pt[0],GP[i].pt[1],GP[i].pt[2]);
      vol+=weight*detJ;
    }
    double massele = vol*_rho;
    // mass in three directions
    for(int kk=0;kk<3;kk++){
       int nbFFRow = (dynamic_cast<const nlsFunctionSpace<double>&>( BilinearTerm<double,double>::space1)).getNumShapeFunctions(ele,kk);
       int nbFFTotalLastRow = (dynamic_cast<const nlsFunctionSpace<double>&>( BilinearTerm<double,double>::space1)).getShapeFunctionsIndex(ele,kk);
       m(nbFFTotalLastRow,nbFFTotalLastRow) += onediv32*massele;
       m(1+nbFFTotalLastRow,1+nbFFTotalLastRow) += onediv32*massele;
       m(2+nbFFTotalLastRow,2+nbFFTotalLastRow) += onediv32*massele;
       m(3+nbFFTotalLastRow,3+nbFFTotalLastRow) += onediv32*massele;
       m(4+nbFFTotalLastRow,4+nbFFTotalLastRow) += 7.*massele/48.;
       m(5+nbFFTotalLastRow,5+nbFFTotalLastRow) += 7.*massele/48.;
       m(6+nbFFTotalLastRow,6+nbFFTotalLastRow) += 7.*massele/48.;
       m(7+nbFFTotalLastRow,7+nbFFTotalLastRow) += 7.*massele/48.;
       m(8+nbFFTotalLastRow,8+nbFFTotalLastRow) += 7.*massele/48.;
       m(9+nbFFTotalLastRow,9+nbFFTotalLastRow) += 7.*massele/48.;
    }
    // non local
    for (int nl = 0; nl< getNumberNonLocalVariable(); nl++)
    {                
      int nbFFRow = (dynamic_cast<const nlsFunctionSpace<double>&>( BilinearTerm<double,double>::space1)).getNumShapeFunctions(ele,3+nl);
      int nbFFTotalLastRow = (dynamic_cast<const nlsFunctionSpace<double>&>( BilinearTerm<double,double>::space1)).getShapeFunctionsIndex(ele,3+nl);
      if(nbFFRow==4)
      {
        m(nbFFTotalLastRow,nbFFTotalLastRow) += massele*_rhoNL/_rho/4.;
        m(1+nbFFTotalLastRow,1+nbFFTotalLastRow) += massele*_rhoNL/_rho/4.;
        m(2+nbFFTotalLastRow,2+nbFFTotalLastRow) += massele*_rhoNL/_rho/4.;      
        m(3+nbFFTotalLastRow,3+nbFFTotalLastRow) += massele*_rhoNL/_rho/4.;                      
      }
      else if(nbFFRow==10)
      {
        m(nbFFTotalLastRow,nbFFTotalLastRow) += onediv32*massele*_rhoNL/_rho;
        m(1+nbFFTotalLastRow,1+nbFFTotalLastRow) += onediv32*massele*_rhoNL/_rho;
        m(2+nbFFTotalLastRow,2+nbFFTotalLastRow) += onediv32*massele*_rhoNL/_rho;
        m(3+nbFFTotalLastRow,3+nbFFTotalLastRow) += onediv32*massele*_rhoNL/_rho;
        m(4+nbFFTotalLastRow,4+nbFFTotalLastRow) += 7.*massele/48.*_rhoNL/_rho;
        m(5+nbFFTotalLastRow,5+nbFFTotalLastRow) += 7.*massele/48.*_rhoNL/_rho;
        m(6+nbFFTotalLastRow,6+nbFFTotalLastRow) += 7.*massele/48.*_rhoNL/_rho;
        m(7+nbFFTotalLastRow,7+nbFFTotalLastRow) += 7.*massele/48.*_rhoNL/_rho;
        m(8+nbFFTotalLastRow,8+nbFFTotalLastRow) += 7.*massele/48.*_rhoNL/_rho;
        m(9+nbFFTotalLastRow,9+nbFFTotalLastRow) += 7.*massele/48.*_rhoNL/_rho;        
      }
      else
        Msg::Error("Mass matrix is not implemented for TET_10 with non local variable and %d non local nodes", nbFFRow);
    }
  }
  else if(ele->getTypeForMSH() == MSH_TET_20){
    // integration on Gauss' points to know area
    double vol = 0.;
    double onediv32 = 1./32.;
    int nbdof = BilinearTerm<double,double>::space1.getNumKeys(ele);
    //int nbFF = ele->getNumShapeFunctions();
    m.resize(nbdof, nbdof,true); // true --> setAll 0.
    //double jac[3][3];
    for (int i = 0; i < npts; i++)
    {
      double weight = GP[i].weight;
      double detJ = ele->getJacobianDeterminant(GP[i].pt[0],GP[i].pt[1],GP[i].pt[2]);
      vol+=weight*detJ;
    }
    double massele = vol*_rho;
    // three weights
    double w1 = 0.0162;
    double w2 = 0.0758;
    double w3 = 0.0064;
    // mass in three directions
    for(int kk=0;kk<3;kk++){
       int nbFFRow = (dynamic_cast<const nlsFunctionSpace<double>&>( BilinearTerm<double,double>::space1)).getNumShapeFunctions(ele,kk);
       int nbFFTotalLastRow = (dynamic_cast<const nlsFunctionSpace<double>&>( BilinearTerm<double,double>::space1)).getShapeFunctionsIndex(ele,kk);
       m(nbFFTotalLastRow,nbFFTotalLastRow) += w1*massele;
       m(1+nbFFTotalLastRow,1+nbFFTotalLastRow) += w1*massele;
       m(2+nbFFTotalLastRow,2+nbFFTotalLastRow) += w1*massele;
       m(3+nbFFTotalLastRow,3+nbFFTotalLastRow) += w1*massele;
       m(4+nbFFTotalLastRow,4+nbFFTotalLastRow) += w2*massele;
       m(5+nbFFTotalLastRow,5+nbFFTotalLastRow) += w2*massele;
       m(6+nbFFTotalLastRow,6+nbFFTotalLastRow) += w2*massele;
       m(7+nbFFTotalLastRow,7+nbFFTotalLastRow) += w2*massele;
       m(8+nbFFTotalLastRow,8+nbFFTotalLastRow) += w2*massele;
       m(9+nbFFTotalLastRow,9+nbFFTotalLastRow) += w2*massele;
       m(10+nbFFTotalLastRow,10+nbFFTotalLastRow) += w2*massele;
       m(11+nbFFTotalLastRow,11+nbFFTotalLastRow) += w2*massele;
       m(12+nbFFTotalLastRow,12+nbFFTotalLastRow) += w2*massele;
       m(13+nbFFTotalLastRow,13+nbFFTotalLastRow) += w2*massele;
       m(14+nbFFTotalLastRow,14+nbFFTotalLastRow) += w2*massele;
       m(15+nbFFTotalLastRow,15+nbFFTotalLastRow) += w2*massele;
       m(16+nbFFTotalLastRow,16+nbFFTotalLastRow) += w3*massele;
       m(17+nbFFTotalLastRow,17+nbFFTotalLastRow) += w3*massele;
       m(18+nbFFTotalLastRow,18+nbFFTotalLastRow) += w3*massele;
       m(19+nbFFTotalLastRow,19+nbFFTotalLastRow) += w3*massele;
    }
    // non local
    for (int nl = 0; nl< getNumberNonLocalVariable(); nl++)
    {                
      int nbFFRow = (dynamic_cast<const nlsFunctionSpace<double>&>( BilinearTerm<double,double>::space1)).getNumShapeFunctions(ele,3+nl);
      int nbFFTotalLastRow = (dynamic_cast<const nlsFunctionSpace<double>&>( BilinearTerm<double,double>::space1)).getShapeFunctionsIndex(ele,3+nl);
      if(nbFFRow==4)
      {
        m(nbFFTotalLastRow,nbFFTotalLastRow) += massele*_rhoNL/_rho/4.;
        m(1+nbFFTotalLastRow,1+nbFFTotalLastRow) += massele*_rhoNL/_rho/4.;
        m(2+nbFFTotalLastRow,2+nbFFTotalLastRow) += massele*_rhoNL/_rho/4.;      
        m(3+nbFFTotalLastRow,3+nbFFTotalLastRow) += massele*_rhoNL/_rho/4.;                      
      }
      else if(nbFFRow==10)
      {
        m(nbFFTotalLastRow,nbFFTotalLastRow) += onediv32*massele*_rhoNL/_rho;
        m(1+nbFFTotalLastRow,1+nbFFTotalLastRow) += onediv32*massele*_rhoNL/_rho;
        m(2+nbFFTotalLastRow,2+nbFFTotalLastRow) += onediv32*massele*_rhoNL/_rho;
        m(3+nbFFTotalLastRow,3+nbFFTotalLastRow) += onediv32*massele*_rhoNL/_rho;
        m(4+nbFFTotalLastRow,4+nbFFTotalLastRow) += 7.*massele/48.*_rhoNL/_rho;
        m(5+nbFFTotalLastRow,5+nbFFTotalLastRow) += 7.*massele/48.*_rhoNL/_rho;
        m(6+nbFFTotalLastRow,6+nbFFTotalLastRow) += 7.*massele/48.*_rhoNL/_rho;
        m(7+nbFFTotalLastRow,7+nbFFTotalLastRow) += 7.*massele/48.*_rhoNL/_rho;
        m(8+nbFFTotalLastRow,8+nbFFTotalLastRow) += 7.*massele/48.*_rhoNL/_rho;
        m(9+nbFFTotalLastRow,9+nbFFTotalLastRow) += 7.*massele/48.*_rhoNL/_rho;        
      }
      else if(nbFFRow==20)
      {
       m(nbFFTotalLastRow,nbFFTotalLastRow) += w1*massele*_rhoNL/_rho;
       m(1+nbFFTotalLastRow,1+nbFFTotalLastRow) += w1*massele*_rhoNL/_rho;
       m(2+nbFFTotalLastRow,2+nbFFTotalLastRow) += w1*massele*_rhoNL/_rho;
       m(3+nbFFTotalLastRow,3+nbFFTotalLastRow) += w1*massele*_rhoNL/_rho;
       m(4+nbFFTotalLastRow,4+nbFFTotalLastRow) += w2*massele*_rhoNL/_rho;
       m(5+nbFFTotalLastRow,5+nbFFTotalLastRow) += w2*massele*_rhoNL/_rho;
       m(6+nbFFTotalLastRow,6+nbFFTotalLastRow) += w2*massele*_rhoNL/_rho;
       m(7+nbFFTotalLastRow,7+nbFFTotalLastRow) += w2*massele*_rhoNL/_rho;
       m(8+nbFFTotalLastRow,8+nbFFTotalLastRow) += w2*massele*_rhoNL/_rho;
       m(9+nbFFTotalLastRow,9+nbFFTotalLastRow) += w2*massele*_rhoNL/_rho;
       m(10+nbFFTotalLastRow,10+nbFFTotalLastRow) += w2*massele*_rhoNL/_rho;
       m(11+nbFFTotalLastRow,11+nbFFTotalLastRow) += w2*massele*_rhoNL/_rho;
       m(12+nbFFTotalLastRow,12+nbFFTotalLastRow) += w2*massele*_rhoNL/_rho;
       m(13+nbFFTotalLastRow,13+nbFFTotalLastRow) += w2*massele*_rhoNL/_rho;
       m(14+nbFFTotalLastRow,14+nbFFTotalLastRow) += w2*massele*_rhoNL/_rho;
       m(15+nbFFTotalLastRow,15+nbFFTotalLastRow) += w2*massele*_rhoNL/_rho;
       m(16+nbFFTotalLastRow,16+nbFFTotalLastRow) += w3*massele*_rhoNL/_rho;
       m(17+nbFFTotalLastRow,17+nbFFTotalLastRow) += w3*massele*_rhoNL/_rho;
       m(18+nbFFTotalLastRow,18+nbFFTotalLastRow) += w3*massele*_rhoNL/_rho;
       m(19+nbFFTotalLastRow,19+nbFFTotalLastRow) += w3*massele*_rhoNL/_rho;
     }
      else
        Msg::Error("Mass matrix is not implemented for TET_20 with non local variable and %d non local nodes", nbFFRow);
    }    
  }
  else if(ele->getTypeForMSH() == MSH_HEX_8){
    // integration on Gauss' points to know area
    double vol = 0.;
    double onediv8 = 1./8.;
    int nbdof = BilinearTerm<double,double>::space1.getNumKeys(ele);
    int nbFF = ele->getNumShapeFunctions();
    m.resize(nbdof, nbdof,true); // true --> setAll 0.
    for (int i = 0; i < npts; i++)
    {
      double weight = GP[i].weight;
      double detJ = ele->getJacobianDeterminant(GP[i].pt[0],GP[i].pt[1],GP[i].pt[2]);

      vol+=weight*detJ;
    }
    double massele = vol*_rho;
    // mass in three directions
    for(int kk=0;kk<3;kk++)
    {
       int nbFFRow = (dynamic_cast<const nlsFunctionSpace<double>&>( BilinearTerm<double,double>::space1)).getNumShapeFunctions(ele,kk);
       int nbFFTotalLastRow = (dynamic_cast<const nlsFunctionSpace<double>&>( BilinearTerm<double,double>::space1)).getShapeFunctionsIndex(ele,kk);
       m(nbFFTotalLastRow,nbFFTotalLastRow) += onediv8*massele;
       m(1+nbFFTotalLastRow,1+nbFFTotalLastRow) += onediv8*massele;
       m(2+nbFFTotalLastRow,2+nbFFTotalLastRow) += onediv8*massele;
       m(3+nbFFTotalLastRow,3+nbFFTotalLastRow) += onediv8*massele;
       m(4+nbFFTotalLastRow,4+nbFFTotalLastRow) += onediv8*massele;
       m(5+nbFFTotalLastRow,5+nbFFTotalLastRow) += onediv8*massele;
       m(6+nbFFTotalLastRow,6+nbFFTotalLastRow) += onediv8*massele;
       m(7+nbFFTotalLastRow,7+nbFFTotalLastRow) += onediv8*massele;
    }
    // non local
    for (int nl = 0; nl< getNumberNonLocalVariable(); nl++)
    {                
      int nbFFRow = (dynamic_cast<const nlsFunctionSpace<double>&>( BilinearTerm<double,double>::space1)).getNumShapeFunctions(ele,3+nl);
      int nbFFTotalLastRow = (dynamic_cast<const nlsFunctionSpace<double>&>( BilinearTerm<double,double>::space1)).getShapeFunctionsIndex(ele,3+nl);
      if(nbFFRow==8)
      {
        m(nbFFTotalLastRow,nbFFTotalLastRow) += onediv8*massele;
        m(1+nbFFTotalLastRow,1+nbFFTotalLastRow) += onediv8*massele;
        m(2+nbFFTotalLastRow,2+nbFFTotalLastRow) += onediv8*massele;
        m(3+nbFFTotalLastRow,3+nbFFTotalLastRow) += onediv8*massele;
        m(4+nbFFTotalLastRow,4+nbFFTotalLastRow) += onediv8*massele;
        m(5+nbFFTotalLastRow,5+nbFFTotalLastRow) += onediv8*massele;
        m(6+nbFFTotalLastRow,6+nbFFTotalLastRow) += onediv8*massele;
        m(7+nbFFTotalLastRow,7+nbFFTotalLastRow) += onediv8*massele;                     
      }
      else
        Msg::Error("Mass matrix is not implemented for HEX_8 with non local variable and %d non local nodes", nbFFRow);
    }
    
  }
  else if(ele->getTypeForMSH() == MSH_HEX_27){
    // integration on Gauss' points to know area
    double vol = 0.;
    double onediv216 = 1./216.;
    int nbdof = BilinearTerm<double,double>::space1.getNumKeys(ele);
    //int nbFF = ele->getNumShapeFunctions();
    m.resize(nbdof, nbdof,true); // true --> setAll 0.
    //double jac[3][3];
    for (int i = 0; i < npts; i++)
    {
      double weight = GP[i].weight;
      double detJ = ele->getJacobianDeterminant(GP[i].pt[0],GP[i].pt[1],GP[i].pt[2]);
      vol+=weight*detJ;
    }
    double massele = vol*_rho;
    // mass in three directions
    for(int kk=0;kk<3;kk++){
       int nbFFRow = (dynamic_cast<const nlsFunctionSpace<double>&>( BilinearTerm<double,double>::space1)).getNumShapeFunctions(ele,kk);
       int nbFFTotalLastRow = (dynamic_cast<const nlsFunctionSpace<double>&>( BilinearTerm<double,double>::space1)).getShapeFunctionsIndex(ele,kk);
       m(nbFFTotalLastRow,nbFFTotalLastRow) += onediv216*massele;
       m(1+nbFFTotalLastRow,1+nbFFTotalLastRow) += onediv216*massele;
       m(2+nbFFTotalLastRow,2+nbFFTotalLastRow) += onediv216*massele;
       m(3+nbFFTotalLastRow,3+nbFFTotalLastRow) += onediv216*massele;
       m(4+nbFFTotalLastRow,4+nbFFTotalLastRow) += onediv216*massele;
       m(5+nbFFTotalLastRow,5+nbFFTotalLastRow) += onediv216*massele;
       m(6+nbFFTotalLastRow,6+nbFFTotalLastRow) += onediv216*massele;
       m(7+nbFFTotalLastRow,7+nbFFTotalLastRow) += onediv216*massele;
       m(8+nbFFTotalLastRow,8+nbFFTotalLastRow) += onediv216*massele*4.;
       m(9+nbFFTotalLastRow,9+nbFFTotalLastRow) += onediv216*massele*4.;
       m(10+nbFFTotalLastRow,10+nbFFTotalLastRow) += onediv216*massele*4.;
       m(11+nbFFTotalLastRow,11+nbFFTotalLastRow) += onediv216*massele*4.;
       m(12+nbFFTotalLastRow,12+nbFFTotalLastRow) += onediv216*massele*4.;
       m(13+nbFFTotalLastRow,13+nbFFTotalLastRow) += onediv216*massele*4.;
       m(14+nbFFTotalLastRow,14+nbFFTotalLastRow) += onediv216*massele*4.;
       m(15+nbFFTotalLastRow,15+nbFFTotalLastRow) += onediv216*massele*4.;
       m(16+nbFFTotalLastRow,16+nbFFTotalLastRow) += onediv216*massele*4.;
       m(17+nbFFTotalLastRow,17+nbFFTotalLastRow) += onediv216*massele*4.;
       m(18+nbFFTotalLastRow,18+nbFFTotalLastRow) += onediv216*massele*4.;
       m(19+nbFFTotalLastRow,19+nbFFTotalLastRow) += onediv216*massele*4.;
       m(20+nbFFTotalLastRow,20+nbFFTotalLastRow) += onediv216*massele*16.;
       m(21+nbFFTotalLastRow,21+nbFFTotalLastRow) += onediv216*massele*16.;
       m(22+nbFFTotalLastRow,22+nbFFTotalLastRow) += onediv216*massele*16.;
       m(23+nbFFTotalLastRow,23+nbFFTotalLastRow) += onediv216*massele*16.;
       m(24+nbFFTotalLastRow,24+nbFFTotalLastRow) += onediv216*massele*16.;
       m(25+nbFFTotalLastRow,25+nbFFTotalLastRow) += onediv216*massele*16.;
       m(26+nbFFTotalLastRow,26+nbFFTotalLastRow) += onediv216*massele*64.;
    }
    // non local
    for (int nl = 0; nl< getNumberNonLocalVariable(); nl++)
    {                
      int nbFFRow = (dynamic_cast<const nlsFunctionSpace<double>&>( BilinearTerm<double,double>::space1)).getNumShapeFunctions(ele,3+nl);
      int nbFFTotalLastRow = (dynamic_cast<const nlsFunctionSpace<double>&>( BilinearTerm<double,double>::space1)).getShapeFunctionsIndex(ele,3+nl);
      if(nbFFRow==8)
      {
        m(nbFFTotalLastRow,nbFFTotalLastRow) += massele*_rhoNL/_rho/8.;
        m(1+nbFFTotalLastRow,1+nbFFTotalLastRow) += massele*_rhoNL/_rho/8.;
        m(2+nbFFTotalLastRow,2+nbFFTotalLastRow) += massele*_rhoNL/_rho/8.;      
        m(3+nbFFTotalLastRow,3+nbFFTotalLastRow) += massele*_rhoNL/_rho/8.;                      
        m(4+nbFFTotalLastRow,4+nbFFTotalLastRow) += massele*_rhoNL/_rho/8.;
        m(5+nbFFTotalLastRow,5+nbFFTotalLastRow) += massele*_rhoNL/_rho/8.;
        m(6+nbFFTotalLastRow,6+nbFFTotalLastRow) += massele*_rhoNL/_rho/8.;      
        m(7+nbFFTotalLastRow,7+nbFFTotalLastRow) += massele*_rhoNL/_rho/8.;                      
      }
      else if(nbFFRow==27)
      {
       m(nbFFTotalLastRow,nbFFTotalLastRow) += onediv216*massele*_rhoNL/_rho;
       m(1+nbFFTotalLastRow,1+nbFFTotalLastRow) += onediv216*massele*_rhoNL/_rho;
       m(2+nbFFTotalLastRow,2+nbFFTotalLastRow) += onediv216*massele*_rhoNL/_rho;
       m(3+nbFFTotalLastRow,3+nbFFTotalLastRow) += onediv216*massele*_rhoNL/_rho;
       m(4+nbFFTotalLastRow,4+nbFFTotalLastRow) += onediv216*massele*_rhoNL/_rho;
       m(5+nbFFTotalLastRow,5+nbFFTotalLastRow) += onediv216*massele*_rhoNL/_rho;
       m(6+nbFFTotalLastRow,6+nbFFTotalLastRow) += onediv216*massele*_rhoNL/_rho;
       m(7+nbFFTotalLastRow,7+nbFFTotalLastRow) += onediv216*massele*_rhoNL/_rho;
       m(8+nbFFTotalLastRow,8+nbFFTotalLastRow) += onediv216*massele*4.*_rhoNL/_rho;
       m(9+nbFFTotalLastRow,9+nbFFTotalLastRow) += onediv216*massele*4.*_rhoNL/_rho;
       m(10+nbFFTotalLastRow,10+nbFFTotalLastRow) += onediv216*massele*4.*_rhoNL/_rho;
       m(11+nbFFTotalLastRow,11+nbFFTotalLastRow) += onediv216*massele*4.*_rhoNL/_rho;
       m(12+nbFFTotalLastRow,12+nbFFTotalLastRow) += onediv216*massele*4.*_rhoNL/_rho;
       m(13+nbFFTotalLastRow,13+nbFFTotalLastRow) += onediv216*massele*4.*_rhoNL/_rho;
       m(14+nbFFTotalLastRow,14+nbFFTotalLastRow) += onediv216*massele*4.*_rhoNL/_rho;
       m(15+nbFFTotalLastRow,15+nbFFTotalLastRow) += onediv216*massele*4.*_rhoNL/_rho;
       m(16+nbFFTotalLastRow,16+nbFFTotalLastRow) += onediv216*massele*4.*_rhoNL/_rho;
       m(17+nbFFTotalLastRow,17+nbFFTotalLastRow) += onediv216*massele*4.*_rhoNL/_rho;
       m(18+nbFFTotalLastRow,18+nbFFTotalLastRow) += onediv216*massele*4.*_rhoNL/_rho;
       m(19+nbFFTotalLastRow,19+nbFFTotalLastRow) += onediv216*massele*4.*_rhoNL/_rho;
       m(20+nbFFTotalLastRow,20+nbFFTotalLastRow) += onediv216*massele*16.*_rhoNL/_rho;
       m(21+nbFFTotalLastRow,21+nbFFTotalLastRow) += onediv216*massele*16.*_rhoNL/_rho;
       m(22+nbFFTotalLastRow,22+nbFFTotalLastRow) += onediv216*massele*16.*_rhoNL/_rho;
       m(23+nbFFTotalLastRow,23+nbFFTotalLastRow) += onediv216*massele*16.*_rhoNL/_rho;
       m(24+nbFFTotalLastRow,24+nbFFTotalLastRow) += onediv216*massele*16.*_rhoNL/_rho;
       m(25+nbFFTotalLastRow,25+nbFFTotalLastRow) += onediv216*massele*16.*_rhoNL/_rho;
       m(26+nbFFTotalLastRow,26+nbFFTotalLastRow) += onediv216*massele*64.*_rhoNL/_rho;
     }
      else
        Msg::Error("Mass matrix is not implemented for HEX_27 with non local variable and %d non local nodes", nbFFRow);
    }
  }
  else if (ele->getTypeForMSH() == MSH_PRI_6){
    // integration on Gauss' points to know area
    double vol = 0.;
    int nbdof = BilinearTerm<double,double>::space1.getNumKeys(ele);
    int nbFF = ele->getNumShapeFunctions();
    m.resize(nbdof, nbdof,true); // true --> setAll 0.
    for (int i = 0; i < npts; i++)
    {
      double weight = GP[i].weight;
      double detJ = ele->getJacobianDeterminant(GP[i].pt[0],GP[i].pt[1],GP[i].pt[2]);
      vol+=weight*detJ;
    }
    double massele = vol*_rho;
    // mass in three directions
    double oneOver = (1./6.);
    for(int kk=0;kk<3;kk++)
    {
       int nbFFRow = (dynamic_cast<const nlsFunctionSpace<double>&>( BilinearTerm<double,double>::space1)).getNumShapeFunctions(ele,kk);
       int nbFFTotalLastRow = (dynamic_cast<const nlsFunctionSpace<double>&>( BilinearTerm<double,double>::space1)).getShapeFunctionsIndex(ele,kk);
       m(nbFFTotalLastRow,nbFFTotalLastRow) += oneOver*massele;
       m(1+nbFFTotalLastRow,1+nbFFTotalLastRow) += oneOver*massele;
       m(2+nbFFTotalLastRow,2+nbFFTotalLastRow) += oneOver*massele;
       m(3+nbFFTotalLastRow,3+nbFFTotalLastRow) += oneOver*massele;
       m(4+nbFFTotalLastRow,4+nbFFTotalLastRow) += oneOver*massele;
       m(5+nbFFTotalLastRow,5+nbFFTotalLastRow) += oneOver*massele;
    }
    // non local
    for (int nl = 0; nl< getNumberNonLocalVariable(); nl++)
    {                
      int nbFFRow = (dynamic_cast<const nlsFunctionSpace<double>&>( BilinearTerm<double,double>::space1)).getNumShapeFunctions(ele,3+nl);
      int nbFFTotalLastRow = (dynamic_cast<const nlsFunctionSpace<double>&>( BilinearTerm<double,double>::space1)).getShapeFunctionsIndex(ele,3+nl);
      if(nbFFRow==6)
      {
        m(nbFFTotalLastRow,nbFFTotalLastRow) += massele*_rhoNL/_rho/6.;
        m(1+nbFFTotalLastRow,1+nbFFTotalLastRow) += massele*_rhoNL/_rho/6.;
        m(2+nbFFTotalLastRow,2+nbFFTotalLastRow) += massele*_rhoNL/_rho/6.;      
        m(3+nbFFTotalLastRow,3+nbFFTotalLastRow) += massele*_rhoNL/_rho/6.;                      
        m(4+nbFFTotalLastRow,4+nbFFTotalLastRow) += massele*_rhoNL/_rho/6.;
        m(5+nbFFTotalLastRow,5+nbFFTotalLastRow) += massele*_rhoNL/_rho/6.;
      }
      else
        Msg::Error("Mass matrix is not implemented for PRI_6 with non local variable and %d non local nodes", nbFFRow);
    }
  }
  else if (ele->getTypeForMSH() == MSH_PRI_18){
    // integration on Gauss' points to know area
    double vol = 0.;
    int nbdof = BilinearTerm<double,double>::space1.getNumKeys(ele);
    int nbFF = ele->getNumShapeFunctions();
    m.resize(nbdof, nbdof,true); // true --> setAll 0.
    for (int i = 0; i < npts; i++)
    {
      double weight = GP[i].weight;
      double detJ = ele->getJacobianDeterminant(GP[i].pt[0],GP[i].pt[1],GP[i].pt[2]);
      vol+=weight*detJ;
    }
    double massele = vol*_rho;
    // mass in three directions
    double oneOver = (1./(12.*6.));
    for(int kk=0;kk<3;kk++)
    {
       int nbFFRow = (dynamic_cast<const nlsFunctionSpace<double>&>( BilinearTerm<double,double>::space1)).getNumShapeFunctions(ele,kk);
       int nbFFTotalLastRow = (dynamic_cast<const nlsFunctionSpace<double>&>( BilinearTerm<double,double>::space1)).getShapeFunctionsIndex(ele,kk);
       m(nbFFTotalLastRow,nbFFTotalLastRow) += oneOver*massele;
       m(1+nbFFTotalLastRow,1+nbFFTotalLastRow) += oneOver*massele;
       m(2+nbFFTotalLastRow,2+nbFFTotalLastRow) += oneOver*massele;

       m(6+nbFFTotalLastRow,6+nbFFTotalLastRow) += 3.*oneOver*massele;
       m(7+nbFFTotalLastRow,7+nbFFTotalLastRow) += 3.*oneOver*massele;
       m(9+nbFFTotalLastRow,9+nbFFTotalLastRow) += 3.*oneOver*massele;

       m(8+nbFFTotalLastRow,8+nbFFTotalLastRow) += 4.*oneOver*massele;
       m(10+nbFFTotalLastRow,10+nbFFTotalLastRow) += 4.*oneOver*massele;
       m(11+nbFFTotalLastRow,11+nbFFTotalLastRow) += 4.*oneOver*massele;

       m(15+nbFFTotalLastRow,15+nbFFTotalLastRow) += 3.*4.*oneOver*massele;
       m(16+nbFFTotalLastRow,16+nbFFTotalLastRow) += 3.*4.*oneOver*massele;
       m(17+nbFFTotalLastRow,17+nbFFTotalLastRow) += 3.*4.*oneOver*massele;

       m(3+nbFFTotalLastRow,3+nbFFTotalLastRow) += oneOver*massele;
       m(4+nbFFTotalLastRow,4+nbFFTotalLastRow) += oneOver*massele;
       m(5+nbFFTotalLastRow,5+nbFFTotalLastRow) += oneOver*massele;

       m(12+nbFFTotalLastRow,12+nbFFTotalLastRow) += 3.*oneOver*massele;
       m(13+nbFFTotalLastRow,13+nbFFTotalLastRow) += 3.*oneOver*massele;
       m(14+nbFFTotalLastRow,14+nbFFTotalLastRow) += 3.*oneOver*massele;
    }
    // non local
    for (int nl = 0; nl< getNumberNonLocalVariable(); nl++)
    {                
      int nbFFRow = (dynamic_cast<const nlsFunctionSpace<double>&>( BilinearTerm<double,double>::space1)).getNumShapeFunctions(ele,3+nl);
      int nbFFTotalLastRow = (dynamic_cast<const nlsFunctionSpace<double>&>( BilinearTerm<double,double>::space1)).getShapeFunctionsIndex(ele,3+nl);
      if(nbFFRow==6)
      {
        m(nbFFTotalLastRow,nbFFTotalLastRow) += massele*_rhoNL/_rho/6.;
        m(1+nbFFTotalLastRow,1+nbFFTotalLastRow) += massele*_rhoNL/_rho/6.;
        m(2+nbFFTotalLastRow,2+nbFFTotalLastRow) += massele*_rhoNL/_rho/6.;      
        m(3+nbFFTotalLastRow,3+nbFFTotalLastRow) += massele*_rhoNL/_rho/6.;                      
        m(4+nbFFTotalLastRow,4+nbFFTotalLastRow) += massele*_rhoNL/_rho/6.;
        m(5+nbFFTotalLastRow,5+nbFFTotalLastRow) += massele*_rhoNL/_rho/6.;
      }
      else if(nbFFRow==18)
      {
        m(nbFFTotalLastRow,nbFFTotalLastRow) += oneOver*massele;
        m(1+nbFFTotalLastRow,1+nbFFTotalLastRow) += oneOver*massele;
        m(2+nbFFTotalLastRow,2+nbFFTotalLastRow) += oneOver*massele;

        m(6+nbFFTotalLastRow,6+nbFFTotalLastRow) += 3.*oneOver*massele;
        m(7+nbFFTotalLastRow,7+nbFFTotalLastRow) += 3.*oneOver*massele;
        m(9+nbFFTotalLastRow,9+nbFFTotalLastRow) += 3.*oneOver*massele;

        m(8+nbFFTotalLastRow,8+nbFFTotalLastRow) += 4.*oneOver*massele;
        m(10+nbFFTotalLastRow,10+nbFFTotalLastRow) += 4.*oneOver*massele;
        m(11+nbFFTotalLastRow,11+nbFFTotalLastRow) += 4.*oneOver*massele;

        m(15+nbFFTotalLastRow,15+nbFFTotalLastRow) += 3.*4.*oneOver*massele;
        m(16+nbFFTotalLastRow,16+nbFFTotalLastRow) += 3.*4.*oneOver*massele;
        m(17+nbFFTotalLastRow,17+nbFFTotalLastRow) += 3.*4.*oneOver*massele;

        m(3+nbFFTotalLastRow,3+nbFFTotalLastRow) += oneOver*massele;
        m(4+nbFFTotalLastRow,4+nbFFTotalLastRow) += oneOver*massele;
        m(5+nbFFTotalLastRow,5+nbFFTotalLastRow) += oneOver*massele;

        m(12+nbFFTotalLastRow,12+nbFFTotalLastRow) += 3.*oneOver*massele;
        m(13+nbFFTotalLastRow,13+nbFFTotalLastRow) += 3.*oneOver*massele;
        m(14+nbFFTotalLastRow,14+nbFFTotalLastRow) += 3.*oneOver*massele;

      }
      else
        Msg::Error("Mass matrix is not implemented for PRI_18 with non local variable and %d non local nodes", nbFFRow);
    }
  }
  else{
    if(sym){
      // Initialization of some data
      int nbdof = BilinearTerm<double,double>::space1.getNumKeys(ele);
      //int nbFF = ele->getNumShapeFunctions();
      m.resize(nbdof, nbdof,true); // true --> setAll 0.

      for (int i = 0; i < npts; i++)
      {
        // Coordonate of Gauss' point i
        double u = GP[i].pt[0]; double v = GP[i].pt[1]; double w = GP[i].pt[2];
        // Weight of Gauss' point i
        double weight = GP[i].weight;
        double detJ = ele->getJacobianDeterminant(u,v,w);
        std::vector<TensorialTraits<double>::ValType> Vals;
        BilinearTerm<double,double>::space1.fuvw(ele,u, v, w, Vals);
        for(int kk=0;kk<3;kk++)
        {
          int nbFFRow = (dynamic_cast<const nlsFunctionSpace<double>&>( BilinearTerm<double,double>::space1)).getNumShapeFunctions(ele,kk);
          int nbFFTotalLastRow = (dynamic_cast<const nlsFunctionSpace<double>&>( BilinearTerm<double,double>::space1)).getShapeFunctionsIndex(ele,kk);
          for(int j=0;j<nbFFRow; j++)
          {
            for(int k=0;k<nbFFRow;k++)
            {
              // same mass in the 3 directions
              double mij = detJ*weight*_rho*Vals[j+nbFFTotalLastRow]*Vals[k+nbFFTotalLastRow];
              m(j+nbFFTotalLastRow,k+nbFFTotalLastRow) += mij;
            }
          }
        }
        // non local
        for (int nl = 0; nl< getNumberNonLocalVariable(); nl++)
        {                
          int nbFFRow = (dynamic_cast<const nlsFunctionSpace<double>&>( BilinearTerm<double,double>::space1)).getNumShapeFunctions(ele,3+nl);
          int nbFFTotalLastRow = (dynamic_cast<const nlsFunctionSpace<double>&>( BilinearTerm<double,double>::space1)).getShapeFunctionsIndex(ele,3+nl);
          for(int j=0;j<nbFFRow; j++)
          {
            for(int k=0;k<nbFFRow;k++)
            {
              double mij = detJ*weight*_rhoNL*Vals[j+nbFFTotalLastRow]*Vals[k+nbFFTotalLastRow];
              m(j+nbFFTotalLastRow,k+nbFFTotalLastRow) += mij;
            }
          }
        }
     } 
   }
   else
     Msg::Error("Mass matrix is not implemented for dg3D with two different function spaces");
  }
  //m.print("mass");
}


void mass3DHierarchicalFE::get(MElement *ele,int npts,IntPt *GP,fullMatrix<double> &m) const
{
  if(sym)
  {
    // Initialization of some data
    const nlsFunctionSpace<double>* hsp = dynamic_cast<const nlsFunctionSpace<double>*>(&space1);
    int nbdof = hsp->getNumKeys(ele);
    //int nbFF = hsp->getNumShapeFunctions(ele,0);
    m.resize(nbdof, nbdof,true); // true --> setAll 0.

    for (int i = 0; i < npts; i++)
    {
      // Coordonate of Gauss' point i
      double u = GP[i].pt[0]; 
      double v = GP[i].pt[1]; 
      double w = GP[i].pt[2];
      // Weight of Gauss' point i
      double weight = GP[i].weight;
      double detJ = ele->getJacobianDeterminant(u,v,w);
      std::vector<TensorialTraits<double>::ValType> Vals;
      hsp->fuvw(ele,u, v, w, Vals);
      for(int jj=0;jj<3;jj++)
      {
        int nbFFRow = (dynamic_cast<const nlsFunctionSpace<double>&>( BilinearTerm<double,double>::space1)).getNumShapeFunctions(ele,jj);
        int nbFFTotalLastRow = (dynamic_cast<const nlsFunctionSpace<double>&>( BilinearTerm<double,double>::space1)).getShapeFunctionsIndex(ele,jj);
        for(int j=0;j<nbFFRow; j++)
        {
          for (int kk=0;kk<3; kk++)
          {          
            int nbFFColumn = (dynamic_cast<const nlsFunctionSpace<double>&>( BilinearTerm<double,double>::space1)).getNumShapeFunctions(ele,kk);
            int nbFFTotalLastColumn = (dynamic_cast<const nlsFunctionSpace<double>&>( BilinearTerm<double,double>::space1)).getShapeFunctionsIndex(ele,kk);
            for(int k=0;k<nbFFColumn;k++){
              // same mass in the 3 directions       
              double mij = detJ*weight*_rho*Vals[j+nbFFTotalLastRow]*Vals[k+nbFFTotalLastColumn];
              m(j+nbFFTotalLastRow,k+nbFFTotalLastColumn) += mij;
            }
          }
        }
      }
      // non local
      for (int nlRow = 0; nlRow< getNumberNonLocalVariable(); nlRow++)
      {                
        int nbFFRow = (dynamic_cast<const nlsFunctionSpace<double>&>( BilinearTerm<double,double>::space1)).getNumShapeFunctions(ele,3+nlRow);
        int nbFFTotalLastRow = (dynamic_cast<const nlsFunctionSpace<double>&>( BilinearTerm<double,double>::space1)).getShapeFunctionsIndex(ele,3+nlRow);
        for(int j=0;j<nbFFRow; j++)
        {
          for (int nlCol = 0; nlCol< getNumberNonLocalVariable(); nlCol++)
          {                
            int nbFFColumn = (dynamic_cast<const nlsFunctionSpace<double>&>( BilinearTerm<double,double>::space1)).getNumShapeFunctions(ele,3+nlCol);
            int nbFFTotalLastColumn = (dynamic_cast<const nlsFunctionSpace<double>&>( BilinearTerm<double,double>::space1)).getShapeFunctionsIndex(ele,3+nlCol);
            for(int k=0;k<nbFFColumn;k++){
              // same mass in the 3 directions       
              double mij = detJ*weight*_rhoNL*Vals[j+nbFFTotalLastRow]*Vals[k+nbFFTotalLastColumn];
              m(j+nbFFTotalLastRow,k+nbFFTotalLastColumn) += mij;
            }
          }
        }
      }
    }
   //m.print("mass");
  }
  else
    Msg::Error("Mass matrix is not implemented for dg3D with two different function spaces");
}
