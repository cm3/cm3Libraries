//
// C++ Interface: terms
//
// Description: Basic term for non linear solver
//
//
// Author:  <Gauthier BECKER>, (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef NONLINEARTERMS_H_
#define NONLINEARTERMS_H_
#include "terms.h"
#include "mlaw.h"
class unknownField;
class IPField;
class partDomain;
class  BiNonLinearTermBase : public BilinearTermBase
{
 protected: 
  bool _accountBodyForceForHO;
 public :
  BiNonLinearTermBase() : _accountBodyForceForHO(true) {}
  BiNonLinearTermBase(const BiNonLinearTermBase &b) : BilinearTermBase(b), _accountBodyForceForHO(b._accountBodyForceForHO) {}
  virtual ~BiNonLinearTermBase() {}
  virtual void get(MElement *ele, int npts, IntPt *GP, fullMatrix<double> &m) const = 0;
  virtual void set(const fullVector<double> *datafield) = 0 ;
  virtual const bool isData() const=0; // true if unknown (via datafield) must be used 
  virtual void setAccountBodyForceForHO(bool bl) {_accountBodyForceForHO=bl;};
  virtual bool getAccountBodyForceForHO() const {return _accountBodyForceForHO;};
  
};

// void term does nothing to avoid null pointer
class BiNonLinearTermVoid : public BiNonLinearTermBase
{
 public:
  BiNonLinearTermVoid(){}
  virtual ~BiNonLinearTermVoid(){}
  virtual void get(MElement *ele, int npts, IntPt *GP, fullMatrix<double> &m) const {};
  virtual void get(MElement *ele, int npts, IntPt *GP, std::vector<fullMatrix<double> > &mve) const{}
  virtual void set(const fullVector<double> *datafield) {}
  const bool isData() const {return false;}
  virtual BilinearTermBase* clone () const
  {
    return new BiNonLinearTermVoid();
  }
};

template<class T2=double> class  nonLinearTermBase : public LinearTermBase<T2>
{
 public:
  nonLinearTermBase(){}
  virtual ~nonLinearTermBase(){}
  virtual void set(const fullVector<double> *datafield) = 0;
  virtual const bool isData() const=0; // true if unknown (via datafield) must be used 
};

class nonLinearTermVoid : public nonLinearTermBase<double>
{
 public:
  virtual ~nonLinearTermVoid(){}
  virtual void get(MElement *ele, int npts, IntPt *GP, fullVector<double> &v) const{};
  virtual void get(MElement *ele, int npts, IntPt *GP, std::vector<fullVector<double> > &vv) const{}
  virtual void set(const fullVector<double> *datafield){};
  virtual const bool isData() const{return false;};
  virtual LinearTermBase<double>* clone () const
  {
    return new nonLinearTermVoid();
  }
};

class nonLinearScalarTermVoid : public ScalarTerm<double>
{
 public :
  nonLinearScalarTermVoid(){}
  virtual ~nonLinearScalarTermVoid() {}
  virtual void get(MElement *ele, int npts, IntPt *GP, double &val) const{};
  virtual void get(MElement *ele, int npts, IntPt *GP, std::vector<double> &vval) const{};
  virtual ScalarTermBase<double>* clone () const {return new nonLinearScalarTermVoid();}
};

// BiNonLinear term by perturbation via nonLinearterm
template <class T2=double> class BilinearTermPerturbation : public BilinearTermBase{
 protected:
  LinearTermBase<T2> *_nlterm;
  FunctionSpaceBase &space1;
  FunctionSpaceBase &space2;
  unknownField *_ufield;
  partDomain *_dom;
  IPField *_ipf;
  const double _eps;
  const double _twoeps;
  const double _onedivtwoeps;
 private:
  mutable fullVector<double> dispm;
  mutable fullVector<double> dispp;
  mutable fullVector<double> disp;
  mutable fullVector<double> fm;
  mutable fullVector<double> fp;
 public:
  BilinearTermPerturbation(LinearTermBase<T2> *lterm, FunctionSpaceBase &sp1, FunctionSpaceBase &sp2,
                       unknownField *ufield, IPField *ipf, partDomain *dom,
                       const double pert=1e-8) : space1(sp1), space2(sp2), _nlterm(lterm),
                                                   _ufield(ufield), _dom(dom),
                                                   _ipf(ipf), _eps(pert), _twoeps(pert+pert),
                                                   _onedivtwoeps(1./(pert+pert)){}
  virtual ~BilinearTermPerturbation(){}
  virtual void get(MElement *ele,int npts,IntPt *GP,fullMatrix<T2> &m) const;
  virtual void get(MElement *ele, int npts, IntPt *GP, std::vector<fullMatrix<double> > &mv) const
  {
    Msg::Error("Define me ?? get without integration BilinearTermPerturbation");
  }
  virtual BilinearTermBase* clone () const
  {
    return new BilinearTermPerturbation<T2>(_nlterm,space1,space2,_ufield,_ipf,_dom,_eps);
  }
};

// Generic 3D elementary mass matrix
class mass3D : public BilinearTerm<double,double>{
 protected:
  double _rho;
  double _rhoNL;
  int _numNonLocalVariable;
  bool sym;
 public:
  mass3D(FunctionSpace<double> &space1_,
                materialLaw *mlaw, int nlVar): BilinearTerm<double,double>(space1_,space1_), sym(true), _rho(mlaw->density()), _rhoNL(mlaw->nLDensity()), _numNonLocalVariable(nlVar){}
  mass3D(FunctionSpace<double> &space1_, FunctionSpace<double> &space2_,
                 materialLaw *mlaw, int nlVar): BilinearTerm<double,double>(space1_,space2_), _rho(mlaw->density()), _rhoNL(mlaw->nLDensity()),_numNonLocalVariable(nlVar)
  {
    sym=(&space1_==&space2_);
  }
  mass3D(FunctionSpace<double> &space1_, FunctionSpace<double> &space2_,
                 double rho, double rhoNL, int numNonLocalVariable): BilinearTerm<double,double>(space1_,space2_), _rho(rho), _rhoNL(rhoNL), _numNonLocalVariable(numNonLocalVariable)
  {
    sym=(&space1_==&space2_);
  }
  virtual void get(MElement *ele,int npts,IntPt *GP,fullMatrix<double> &m) const;
  virtual void get(MElement *ele, int npts, IntPt *GP, std::vector<fullMatrix<double> > &mv) const
  {
    Msg::Error("Define me get by integration point mass3D");
  }
  virtual BilinearTermBase* clone () const
  {
    return new mass3D(space1,space2,_rho, _rhoNL, _numNonLocalVariable);
  }
  
  virtual void setNumberNonLocalVariable(int num) {_numNonLocalVariable=num;};
  virtual int getNumberNonLocalVariable() const {return _numNonLocalVariable;};
  
};

class mass3DHierarchicalFE : public BilinearTerm<double,double>{
 protected:
  double _rho;
  double _rhoNL;
  int _numNonLocalVariable;
  bool sym;
 public:
  mass3DHierarchicalFE(FunctionSpace<double> &space1_,
                materialLaw *mlaw, int nlVar): BilinearTerm<double,double>(space1_,space1_), sym(true), _rho(mlaw->density()), _rhoNL(mlaw->nLDensity()), _numNonLocalVariable(nlVar){}
  mass3DHierarchicalFE(FunctionSpace<double> &space1_, FunctionSpace<double> &space2_,
                 materialLaw *mlaw, int nlVar): BilinearTerm<double,double>(space1_,space2_), _rho(mlaw->density()), _rhoNL(mlaw->nLDensity()),_numNonLocalVariable(nlVar)
  {
    sym=(&space1_==&space2_);
  }
  mass3DHierarchicalFE(FunctionSpace<double> &space1_, FunctionSpace<double> &space2_,
                 double rho, double rhoNL, int numNonLocalVariable): BilinearTerm<double,double>(space1_,space2_), _rho(rho), _rhoNL(rhoNL), _numNonLocalVariable(numNonLocalVariable)
  {
    sym=(&space1_==&space2_);
  }
  virtual void get(MElement *ele,int npts,IntPt *GP,fullMatrix<double> &m) const;
  virtual void get(MElement *ele, int npts, IntPt *GP, std::vector<fullMatrix<double> > &mv) const
  {
    Msg::Error("Define me get by integration point mass3DHierarchicalFE");
  }
  virtual BilinearTermBase* clone () const
  {
    return new mass3DHierarchicalFE(space1,space2,_rho, _rhoNL, _numNonLocalVariable);
  }
  
  virtual void setNumberNonLocalVariable(int num) {_numNonLocalVariable=num;};
  virtual int getNumberNonLocalVariable() const {return _numNonLocalVariable;}
};


#endif // NONLINEARTERMS_H_

