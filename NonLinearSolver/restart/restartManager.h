/**
 * Class to manage the restart
 * A restart can be created by
 * 1) given a number of step
 * 2) Specifying a time of execution
 */

#ifndef RESTART_MANAGER_H_
#define RESTART_MANAGER_H_
//#include <pthread.h>
#include <string.h>
#include <string>
#include <fstream>
#include <vector>
#include <set>
#include "GmshMessage.h"
static void SwapBytes(char *array, int size, int n)
{
  char *x = new char[size];
  for(int i = 0; i < n; i++) {
    char *a = &array[i * size];
    memcpy(x, a, size);
    for(int c = 0; c < size; c++)
      a[size - 1 - c] = x[c];
  }
  delete [] x;
  return;
}

class restartManager{
 private:
  static bool _swapbinary; // Swap in case of binary
  static int _stepBetweenRestart; // if = -1 no restart based on the number of step
  static int _lastSavedStep; // count the number of steps between last restart
                           // and  is initialized at restart to the step where the last checkpoint
                           // has been created (to avoid checkpoint creation at restart!)
  static size_t _timeLastCheckpoint;
  static double _secondBetweenCheckpoint; // init to -1 and no checkpoint creation based on time in this case
                               // will be saved Small file never in binary format except for "one"
                               // which allows to know if the bytes must be swapped
  static std::ofstream _ofile; // The output file is stored here
  static std::ifstream _ifile; // The input file is stored here
  static bool _readMode; // If false --> write mode
 public:
  restartManager(){}
  static void Init(const int nstep=-1);
  static void InitFromRestartFile();
  static void InitFromRestartFile(const std::string &fname);
  static void setTimeBetweenCheckpoint(const int day,const int hour,const int minute,const int second);
  static void setStepBetweenCheckpoint(const int numstep);
  static bool checkpoint(const int step=0);
  static void createRestartFile(const std::string &fname);
  static void openRestartFile(const std::string &fname);
  static void createInternalStateRestartFile(const std::string &fname);
  static void openInternalStateRestartFile(const std::string &fname);
  static void closeRestartFile();
  static bool available();
  static bool internalStateRestartAvailable();
  static void resetLastSavedStep();
  template<class T> static void createRestart(const std::string &name, const T* obj)
  {
    restartManager::createRestartFile(name);
    obj->restart();
    restartManager::closeRestartFile();
    return;
  }
  template<class T> static void setFromRestart(const std::string &name, T* obj)
  {
    restartManager::openRestartFile(name);
    obj->restart();
    restartManager::closeRestartFile();
    return;
  }

  template<class T> static void restart(T& vec) {(_readMode) ? read(vec) : write(vec); }
  template<class T> static void restart(std::vector<T> &vec){ (_readMode) ? read(vec) : write(vec);    }
  template<class T> static void restart(std::set<T> &myset) {(_readMode) ? read(myset) : write(myset); }
  template<class key, class T> static void restart(std::map<key,T> &mymap) {(_readMode) ? read(mymap) : write(mymap); }
  template<class T> static void restart(T* myarray,const int mysize){ (_readMode) ? read(myarray,mysize) : write(myarray,mysize);}
  template<class T> static void restart(T* obj)
  {
    if(_readMode)
    {
      int readData;
      read(&readData,1);
      if(readData == 1)
      {
        (obj == NULL) ? Msg::Error("An object must be initialized before you can restart it! (It is NULL)") :obj->restart();
      }
    }
    else
    {
      int writeData = (obj == NULL) ? 0 : 1;
      write(&writeData,1);
      if(writeData==1) obj->restart();
    }
    return;
  }

 protected:
  template<class T> static void write(const T& vec);
  template<class T> static void write(const std::vector<T> &vec)
  {
    int vsize = vec.size();
    restartManager::write(&vsize,1);
    restartManager::write(&vec[0],vsize);
    return;
  }

  template<class T> static void write(const std::set<T> &myset)
  {
    int ssize = myset.size();
    restartManager::write(&ssize,1);
    for(typename std::set<T>::iterator it=myset.begin();it!=myset.end();++it)
    {
      restartManager::write(*it);
    }
    return;
  }
  template<class key, class T> static void write(const std::map<key, T> &mymap)
  {
    int ssize = mymap.size();
    restartManager::write(&ssize,1);
    for(typename std::map<key,T>::const_iterator it=mymap.begin();it!=mymap.end();++it)
    {
      restartManager::write(it->first);
      restartManager::write(it->second);
    }
    return;
  }
  template<class T> static void write(const T* myarray,const int mysize)
  {
    _ofile.write(reinterpret_cast<const char*>(myarray), sizeof(T)*mysize);
    return;
  }
  template<class T> static void read(T& vec);
  template<class T> static void read(std::vector<T>& vec)
  {
    int vsize;
    restartManager::read(&vsize,1);
    vec.resize(vsize);
    restartManager::read(&vec[0],vsize);
    return;
  }
  template<class T> static void read(std::set<T>& myset)
  {
    int ssize;
    restartManager::read(&ssize,1);
    for(int i=0;i<ssize;++i)
    {
      T obj;
      restartManager::read(obj);
      myset.insert(obj);
    }
    return;
  }
  
  template<class key, class T> static void read(std::map<key,T>& mymap)
  {
    int ssize;
    restartManager::read(&ssize,1);
    for(int i=0;i<ssize;++i)
    {
      key k;
      restartManager::read(k);
      T obj;
      restartManager::read(obj);
      mymap.insert(std::pair<key,T>(k,obj));
    }
    return;
  }
  
  template<class T> static void read(T* myarray,const int mysize)
  {
    _ifile.read(reinterpret_cast<char*>(myarray),sizeof(T)*mysize);
    if(_swapbinary) SwapBytes((char*)myarray, sizeof(T), mysize);
    return;
  }
  template<class T> static T read()
  {
    T obj;
    restartManager::read(obj);
    return obj;
  }
 private:
  static void createMyRestartFile(const int step);
};
#endif // RESTART_MANAGER_H_
