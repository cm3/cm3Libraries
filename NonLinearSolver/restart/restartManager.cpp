#include "restartManager.h"
#include "GmshMessage.h"
#include <iostream>
#include <sstream>
#include <stdlib.h>
#include <stdio.h>
#include <vector>
#include "STensor3.h"
#include "STensor33.h"
#include "STensor43.h"
#include "STensor53.h"
#include "STensor63.h"
#include "SVector3.h"
#include "staticDofManager.h"
#include "ipField.h"

bool restartManager::_swapbinary = false;
int  restartManager::_stepBetweenRestart = -1;
int  restartManager::_lastSavedStep = 0;
size_t restartManager::_timeLastCheckpoint =0;
double restartManager::_secondBetweenCheckpoint =-1.; // -1 --> no check
std::ofstream restartManager::_ofile;
std::ifstream restartManager::_ifile;
bool restartManager::_readMode = false;

void restartManager::Init(const int nstep)
{
  _swapbinary = false;
  _stepBetweenRestart = nstep;
  _lastSavedStep = 0;
  _timeLastCheckpoint = time(NULL);
  _secondBetweenCheckpoint = -1.;
}

void restartManager::InitFromRestartFile()
{
  _lastSavedStep = 0;
  restartManager::openRestartFile("restart");

  //restartManager::read(&_stepBetweenRestart,1);
  //restartManager::read(&_secondBetweenCheckpoint,1);

  _swapbinary = false;
  int one;
  restartManager::read(&one,1);
  if(one != 1)
  {
    _swapbinary = true;
    Msg::Info("SwapBytes will be called");
  }
  restartManager::read(&_lastSavedStep,1);
  restartManager::closeRestartFile();
  return;
}

void restartManager::InitFromRestartFile(const std::string &fname)
{
    _lastSavedStep = 0;
    restartManager::openRestartFile(fname);

    //restartManager::read(&_stepBetweenRestart,1);
    //restartManager::read(&_secondBetweenCheckpoint,1);

    _swapbinary = false;
    int one;
    restartManager::read(&one,1);
    if(one != 1)
    {
        _swapbinary = true;
        Msg::Info("SwapBytes will be called");
    }
    restartManager::read(&_lastSavedStep,1);
    restartManager::closeRestartFile();
    return;
}

void restartManager::setTimeBetweenCheckpoint(const int day,const int hour,const int minute,const int second)
{
  _secondBetweenCheckpoint = (double)(second+60.*minute+3600.*(hour+day*24.));
  return;
}

void restartManager::setStepBetweenCheckpoint(const int numstep)
{
  _stepBetweenRestart = numstep;
  return;
}

bool restartManager::checkpoint(const int currentstep)
{
  // restart based on a step number
  if((currentstep-_lastSavedStep) >= _stepBetweenRestart)
  {
    Msg::Info("Checkpoint creation due to number of step requirement");
    restartManager::createMyRestartFile(currentstep);
    return true;
  }

  // restart based on time
  if(_secondBetweenCheckpoint != -1.)
  {
    Msg::Barrier(); // MPI time synchronization
    size_t currentTime = time(NULL);
    double nsec = difftime(currentTime,_timeLastCheckpoint);
    if(nsec>=_secondBetweenCheckpoint)
    {
      Msg::Info("Checkpoint creation due to time requirement");
      _timeLastCheckpoint = currentTime;
      restartManager::createMyRestartFile(currentstep);
      return true;
    }
  }
  return false;
}

void restartManager::createRestartFile(const std::string &fname)
{
  // include the partition number in the file name
  std::ostringstream oss;
  oss << Msg::GetCommRank();
  std::string srank = oss.str();
  srank.append(".ckp");
  _ofile.open((fname+srank).c_str(), std::fstream::binary);
  _readMode = false;
  return;
}

void restartManager::closeRestartFile()
{
  if(!_ofile.fail()) _ofile.close();
  if(!_ifile.fail()) _ifile.close();
  return;
}

bool restartManager::available()
{
  std::ostringstream oss;
  oss << Msg::GetCommRank();
  std::string srank("restart");
  srank += oss.str();
  srank.append(".ckp");
  std::ifstream tempofile;
  tempofile.open(srank.c_str());
  return (!tempofile.fail());
}

bool restartManager::internalStateRestartAvailable()
{
    std::ostringstream oss;
    oss << Msg::GetCommRank();
    std::string srank("restartInternalState");
    srank += oss.str();
    srank.append(".ckp");
    std::ifstream tempofile;
    tempofile.open(srank.c_str());
    return (!tempofile.fail());
}

void restartManager::resetLastSavedStep()
{
  _lastSavedStep = 0;
}

void restartManager::openRestartFile(const std::string &fname)
{
  // include the partition number in the file name
  std::ostringstream oss;
  oss << Msg::GetCommRank();
  std::string srank = oss.str();
  srank.append(".ckp");
  _ifile.open((fname+srank).c_str(), std::fstream::binary);
  if(_ifile.fail())
  {
    Msg::Error("Cannot open the file %s. Restart operation fails!",(fname+srank).c_str());
  }
  _readMode = true;
  return;
}


void restartManager::createInternalStateRestartFile(const std::string &fname)
{
    // include the partition number in the file name
    std::ostringstream oss;
    oss << Msg::GetCommRank();
    std::string srank = oss.str();
    srank.append(".ckp");
    _ofile.open((fname+srank).c_str(), std::fstream::binary & std::fstream::app);
    _readMode = false;
    return;
}

void restartManager::openInternalStateRestartFile(const std::string &fname)
{
    // include the partition number in the file name
    std::ostringstream oss;
    oss << Msg::GetCommRank();
    std::string srank = oss.str();
    srank.append(".ckp");
    _ifile.open((fname+srank).c_str(), std::fstream::binary & std::fstream::app);
    if(_ifile.fail())
    {
        Msg::Error("Cannot open the file %s. Restart operation fails!",(fname+srank).c_str());
    }
    _readMode = true;
    return;
}

template<> void
restartManager::write<std::string>(const std::string &vec) // extra operation for string --> must be specialized for each type (int,real,...) How to avoid this?
{
  int ssize = vec.size();
  char const * cstring = vec.data();
  restartManager::write(&ssize,1);
  restartManager::write(cstring,ssize);
  return;
}

template<> void
restartManager::write<int>(const int &vec)
{
  restartManager::write(&vec,1);
  return;
}

template<> void
restartManager::write<size_t>(const size_t &vec)
{
  restartManager::write(&vec,1);
  return;
}

template<> void
restartManager::write<long int>(const long int &vec)
{
  restartManager::write(&vec,1);
  return;
}

template<> void
restartManager::write<double>(const double &vec)
{
  restartManager::write(&vec,1);
  return;
}

template<> void
restartManager::write<bool>(const bool &vec)
{
  int bint = (vec) ? 1 : 0;
  restartManager::write(bint);
  return;
}

template<> void
restartManager::write<TwoNum>(const TwoNum & tn)
{
  restartManager::write(&(tn.small),1);
  restartManager::write(&(tn.large),1);
  return;
}

template<> void
restartManager::write<STensor3>(const STensor3 &vec)
{
  restartManager::write(vec.data(),9);
  return;
}

template<> void
restartManager::write<STensor33>(const STensor33 &vec)
{
  restartManager::write(vec.data(),27);
  return;
}

template<> void
restartManager::write<STensor43>(const STensor43 &vec)
{
  restartManager::write(vec.data(),81);
  return;
}

template<> void
restartManager::write<STensor53>(const STensor53 &vec)
{
  for (int i=0; i<3; i++)
    for (int j=0; j<3; j++)
      for (int k=0; k<3; k++)
        for (int l=0; l<3; l++)
          for (int m=0; m<3; m++)
            restartManager::write((vec(i,j,k,l,m)));
  return;
}

template<> void
restartManager::write<STensor63>(const STensor63 &vec)
{
  for (int i=0; i<3; i++)
    for (int j=0; j<3; j++)
      for (int k=0; k<3; k++)
        for (int l=0; l<3; l++)
          for (int m=0; m<3; m++)
            for (int n=0; n<3; n++)
              restartManager::write((vec(i,j,k,l,m,n)));
  return;
}

template<> void
restartManager::write<SVector3>(const SVector3 &vec)
{
  restartManager::write(vec.data(),3);
  return;
}

template<> void
restartManager::write<SPoint3>(const SPoint3 &vec)
{
  restartManager::write(vec[0]);
  restartManager::write(vec[1]);
  restartManager::write(vec[2]);
  return;
}

template<> void
restartManager::write<short int>(const short int &vec)
{
  restartManager::write(&vec,1);
  return;
}

template<> void
restartManager::write<Dof>(const Dof& mydof)
{
  restartManager::write(mydof.getEntity());
  restartManager::write(mydof.getType());
  return;
}

template<> void
restartManager::write<std::map<Dof,double> >(const std::map<Dof,double> & dofmap)
{
  restartManager::write(dofmap.size());
  for(std::map<Dof,double>::const_iterator itd = dofmap.begin(); itd != dofmap.end();++itd)
  {
    const Dof & dw = itd->first;
    restartManager::write(itd->first);
    restartManager::write(itd->second);
  }
  return;
}

template<> void
restartManager::read<std::string>(std::string& vec)
{
  int ssize;
  restartManager::read(&ssize,1);
  std::vector<char> vstring(ssize);
  restartManager::read(&vstring[0],ssize);
  vec.assign(vstring.begin(),vstring.end());
  return;
}

template<> void
restartManager::read<int>(int& vec)
{
  restartManager::read(&vec,1);
  return;
}

template<> void
restartManager::read<size_t>(size_t& vec)
{
  restartManager::read(&vec,1);
  return;
}

template<> void
restartManager::read<long int>(long int& vec)
{
  restartManager::read(&vec,1);
  return;
}

template<> void
restartManager::read<double>(double& vec)
{
  restartManager::read(&vec,1);
  return;
}

template<> void
restartManager::read<bool>(bool& vec)
{
  int bint;
  restartManager::read(bint);
  vec = (bint == 1) ? true : false;
  return;
}

template<> void
restartManager::read<TwoNum>(TwoNum& tn)
{
  restartManager::read(&(tn.small),1);
  restartManager::read(&(tn.large),1);
  return;
}

template<> void
restartManager::read<STensor3>(STensor3& vec)
{
  restartManager::read(vec.data(),9);
  return;
}

template<> void
restartManager::read<STensor33>(STensor33& vec)
{
  restartManager::read(vec.data(),27);
  return;
}

template<> void
restartManager::read<STensor43>(STensor43& vec)
{
  restartManager::read(vec.data(),81);
  return;
}

template<> void
restartManager::read<STensor53>(STensor53 &vec)
{
  for (int i=0; i<3; i++)
    for (int j=0; j<3; j++)
      for (int k=0; k<3; k++)
        for (int l=0; l<3; l++)
          for (int m=0; m<3; m++)
            restartManager::read(&(vec(i,j,k,l,m)),1);
  return;
}

template<> void
restartManager::read<STensor63>(STensor63 &vec)
{
  for (int i=0; i<3; i++)
    for (int j=0; j<3; j++)
      for (int k=0; k<3; k++)
        for (int l=0; l<3; l++)
          for (int m=0; m<3; m++)
            for (int n=0; n<3; n++)
              restartManager::read(&(vec(i,j,k,l,m,n)),1);
  return;
}

template<> void
restartManager::read<SVector3>(SVector3& vec)
{
  restartManager::read(vec.data(),3);
  return;
}
template<> void
restartManager::read<SPoint3>(SPoint3& vec)
{
  restartManager::read(vec[0]);
  restartManager::read(vec[1]);
  restartManager::read(vec[2]);
  return;
}

template<> void
restartManager::read<short int>(short int& vec)
{
  restartManager::read(&vec,1);
  return;
}

template<> void
restartManager::read<Dof>(Dof & mydof)
{
  long int entity;
  restartManager::read(entity);
  int type;
  restartManager::read(type);
  mydof = Dof(entity,type);
  return;
}

template<> void
restartManager::read<std::map<Dof,double> >(std::map<Dof,double> & mapdof)
{
  size_t mapsize;
  restartManager::read(mapsize);
  Dof tdof(0,0);
  double val;
  for(size_t i=0;i<mapsize;++i)
  {
    restartManager::read(tdof);
    restartManager::read(val);
    mapdof.insert(std::pair<Dof,double>(tdof,val));
  }
  return;
}

void
restartManager::createMyRestartFile(const int step)
{
  _lastSavedStep = step;
  int ok = system("rm -rf *.ckp");
  // In  MPI, wait all here to be sure that the new files *.ckp are not deleted by another rank calling the system command 2 lines above...
  Msg::Barrier();

  restartManager::createRestartFile("restart");
  //restartManager::write(&_stepBetweenRestart,1); //we allow to change this one
  //restartManager::write(&_secondBetweenCheckpoint,1); //we allow to change this one
  // write one to know if the bytes will have to be swapped.
  int one = 1;
  restartManager::write(&one,1);
  restartManager::write(&_lastSavedStep,1);
  restartManager::closeRestartFile();
  return;
}
