//
//
// Description: Class for tracking fragment
//
//
// Author:  <Gauthier BECKER>, (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "fragment.h"
interfaceFragment::interfaceFragment(const MInterfaceElement *ielem,const double rhominus,
                                     const double rhoplus): pairElem(ielem->getElem(0),ielem->getElem(1)),
                                                            pairDensity(rhominus,rhoplus){}
interfaceFragment::interfaceFragment(const interfaceFragment &source) : pairDensity(source.pairDensity), pairElem(source.pairElem){}

bool interfaceFragment::matchElem(const MElement *ele) const
{
  if(ele->getNum() == pairElem.first->getNum())
    return true;
  else if(ele->getNum() == pairElem.second->getNum())
    return true;
  return false;
}
double interfaceFragment::getDensity(const int i) const
{
  if(i==0) return pairDensity.first;
  else return pairDensity.second;
}

MElement* interfaceFragment::getElem(const int i) const
{
  if(i==0) return pairElem.first;
  else return pairElem.second;
}

bool interfaceFragment::operator<(const interfaceFragment &ie2) const
{
  return (pairElem<ie2.pairElem);
}


pairMassElem::pairMassElem(MElement *ele_,const double rho_) : ele(ele_), rho(rho_){}
pairMassElem::pairMassElem(const pairMassElem &source) : ele(source.ele), rho(source.rho){}
bool pairMassElem::operator<(const pairMassElem &pme2) const{return ele < pme2.ele;}

fragment::fragment(){}
fragment::fragment(const fragment &source): _data(source._data){}

fragment::fragment(const interfaceFragment &source)
{
  _data.insert(pairMassElem(source.getElem(0),source.getDensity(0)));
  _data.insert(pairMassElem(source.getElem(1),source.getDensity(1)));
}

fragment::fragment(const pairMassElem &source)
{
  _data.insert(pairMassElem(source));
}

void fragment::add(const interfaceFragment &source) const
{
  // choose the element to insert
  MElement *ele = source.getElem(0);
  std::set<pairMassElem>::iterator it;
  for(it=_data.begin(); it!=_data.end();++it)
  {
    if((*it).ele==ele) break;
  }
  if(it!=_data.end())
    _data.insert(pairMassElem(source.getElem(1),source.getDensity(1)));
  else
   _data.insert(pairMassElem(source.getElem(0),source.getDensity(0)));
}

void fragment::add(const fragment &frag2) const
{
  for(std::set<pairMassElem>::iterator it=frag2._data.begin();it!=frag2._data.end();++it)
  {
    _data.insert(*it);
  }
}

bool fragment::match(const interfaceFragment &frag) const
{
  bool found=false;
  for(std::set<pairMassElem>::const_iterator it=_data.begin(); it!=_data.end();++it)
  {
    found = frag.matchElem(it->ele);
    if(found) break;
  }
  return found;
}

bool fragment::match(const pairMassElem &source) const
{
  MElement *eleiso = source.ele;
  for(std::set<pairMassElem>::const_iterator it=_data.begin(); it!=_data.end();++it)
  {
    if(eleiso==(*it).ele)
      return true;
  }
  return false;
}

int fragment::numberOfElements() const
{
  return _data.size();
}

double fragment::mass() const
{
  double m=0.;
  for(std::set<pairMassElem>::iterator it=_data.begin(); it!=_data.end();++it)
  {
    m+=(*it).rho*(*it).ele->getVolume(); // getVolume return area for 2D elements ??
  }
  return m;
}

bool fragment::isElem(const int elenum) const
{
  for(std::set<pairMassElem>::iterator it=_data.begin(); it!=_data.end();++it)
  {
    if((*it).ele->getNum()==elenum)
      return true;
  }
  return false;
}

bool fragment::operator<(const fragment &frag2) const
{
  std::set<pairMassElem>::iterator it1 = _data.begin();
  std::set<pairMassElem>::iterator it2 = frag2._data.begin();
  return (*it1).ele< (*it2).ele;
}

#if defined(HAVE_MPI)
bool fragment::definedOnMoreThanOneRank() const
{
  for(std::set<pairMassElem>::iterator it=_data.begin(); it!=_data.end(); ++it)
  {
    if(((*it).ele->getPartition()!=0) and ((*it).ele->getPartition()!=Msg::GetCommRank()+1))
      return true;
  }
  return false;
}

int fragment::numberOfValueToCommunicateMPI() const
{
  // scheme of transfert is the number of fragment following by the pair element num element mass
  return 1+2*_data.size();
}

pairMassElemMPI::pairMassElemMPI(const pairMassElem &source) : elenum(source.ele->getNum()), mass(source.rho*source.ele->getVolume()){}
pairMassElemMPI::pairMassElemMPI(const int num,const double m) : elenum(num), mass(m){}
bool pairMassElemMPI::operator<(const pairMassElemMPI &pme2) const{return elenum < pme2.elenum;}

fragmentMPI::fragmentMPI(const fragment &frag)
{
  for(std::set<pairMassElem>::iterator it=frag._data.begin(); it!=frag._data.end();++it)
  {
    _data.insert(*it);
  }
}

int fragmentMPI::fillMPIvector(double* mpivector) const
{
  mpivector[0] = _data.size();
  std::set<pairMassElemMPI>::iterator it= _data.begin();
  for(int i=1;i<=2*_data.size();i+=2)
  {
    mpivector[i] = (double)(*it).elenum;
    mpivector[i+1]= (*it).mass; // for 2D element getVolume return the mass of element
    it++;
  }
  return 1+2*_data.size();
}

bool fragmentMPI::operator<(const fragmentMPI &frag2) const
{
  std::set<pairMassElemMPI>::iterator it1 = _data.begin();
  std::set<pairMassElemMPI>::iterator it2 = frag2._data.begin();
  return (*it1).elenum< (*it2).elenum;
}

fragmentMPI::fragmentMPI(double* &fraglist,const int curpos)
{
  int newpos = 1+2*(int)fraglist[curpos];
  for(int i=curpos+1;i<(curpos+newpos);i+=2){
    _data.insert(pairMassElemMPI((int)fraglist[i],fraglist[i+1]));
  }
}

bool fragmentMPI::match(const fragmentMPI &frag) const
{
  for(std::set<pairMassElemMPI>::const_iterator it=_data.begin(); it!=_data.end();++it)
  {
    if(frag.match(it->elenum))
      return true;
  }
  return false;
}

bool fragmentMPI::match(const int num) const
{
  for(std::set<pairMassElemMPI>::const_iterator it=_data.begin(); it!=_data.end();++it)
  {
    if(num == it->elenum)
      return true;
  }
  return false;
}

void fragmentMPI::add(const fragmentMPI &frag) const
{
  for(std::set<pairMassElemMPI>::iterator it=frag._data.begin(); it!=frag._data.end();++it)
  {
    _data.insert(*it);
  }
}

int fragmentMPI::numberOfElements() const
{
  return _data.size();
}

double fragmentMPI::mass() const
{
  double m=0.;
  for(std::set<pairMassElemMPI>::iterator it=_data.begin(); it!=_data.end();++it)
  {
    m+=(*it).mass;
  }
  return m;
}

bool fragmentMPI::isElem(const int elenum) const
{
  for(std::set<pairMassElemMPI>::iterator it=_data.begin(); it!=_data.end();++it)
  {
    if((*it).elenum == elenum)
      return true;
  }
  return false;
}

int fragmentMPI::memorySize() const
{
  return 1+2*_data.size();
}
#endif // HAVE_MPI
