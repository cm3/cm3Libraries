//
// contact base term
//
// Description: contact with a rigid plane without friction
//
//
// Author:  <Van Dung NGUYEN>, (C) 2015
//
// Copyright: See COPYING file that comes with this distribution
//

#include "rigidPlaneContactTerms.h"
#include "contactDomain.h"

massRigidPlane::massRigidPlane(rigidPlaneContactDomain *cdom, rigidContactSpaceBase *spc) :
                                      _thickness(cdom->getThickness()),_rho(cdom->getDensity()),
                                      _spc(spc){}

void massRigidPlane::get(MElement *ele, int npts, IntPt *GP, fullMatrix<double> &m) const{
  // get the number of cylinder component (same mass in all direction)
  int nbdofs = _spc->getNumKeysOfGravityCenter();
  m.resize(nbdofs,nbdofs,true); // true --> setAll(0.)
  double masse = _thickness*_thickness*_thickness*_rho;

  for(int i=0;i<nbdofs;i++)
    m(i,i) = masse;
};


void forceRigidPlaneContact::get(const MVertex *ver, const double disp[6],double mvalue[6]) const{
  // change in P
  SPoint3 P(ver->point());
  P[0] += disp[0];
  P[1] += disp[1];
  P[2] += disp[2];

  // change in root point
  SPoint3 A(_cdom->getGC()->point());
  A[0] += disp[3];
  A[1] += disp[4];
  A[2] += disp[5];

  SVector3 PA(P,A);
  double delta = dot(PA,_cdom->getNormal());

 // Msg::Info("delta = %e",delta);

  if (delta > 0){
    // in contact
    _lastNormalContact = _cdom->getNormal();

    for(int j=0;j<3;j++){
      mvalue[j] = _cdom->getPenalty()*(delta)*_lastNormalContact(j);
      mvalue[j+3] = -_cdom->getPenalty()*(delta)*_lastNormalContact(j);
    }
  }
};

void stiffnessRigidPlaneContact::get(MElement *ele, const int verIndex, fullMatrix<double> &m) const
{
  double _perturbation = 1e-10; // numerical perturbation
  double _doublepertexpm1 = 1./(2.e-10);
  // Data for get function (Allocated Once)
  double fp[6];
  double fm[6];
  int nbdofs;
  MVertex *ver;
  std::vector<Dof> R;
  std::vector<double> disp;
  double pertdisp[6];

  // perturbation numeric
  rigidContactLinearTermBase<double>* rlterm = static_cast<rigidContactLinearTermBase<double>*>(_lterm);
  int nbdofselem = _spc->getNumKeys(ele) - _spc->getNumKeysOfGravityCenter();
  int nbFF = ele->getNumVertices();
  ver = ele->getVertex(verIndex);
  disp.clear(); R.clear();
  _spc->getKeys(ele,verIndex,R);
  _ufield->get(R,disp);
  for(int j=0;j<6;j++)
    pertdisp[j] = disp[j];

  // perturbation on each Dofs 3 for vertex and 3 for GC of cylinder
  for(int i=0;i<6;i++){
    // set force component to zero
    for(int j=0;j<6;j++)
      fp[j] = fm[j] = 0.;

    // Force -
    pertdisp[i] = disp[i] - _perturbation;
    rlterm->get(ver,pertdisp,fm);

    // Force +
    pertdisp[i] = disp[i] + _perturbation;
    rlterm->get(ver,pertdisp,fp);

    // Assembly in matrix
    if(i<3){ // perturbation on vertex
      for(int j=0; j<3; j++){
        m(verIndex+nbFF*j,verIndex+nbFF*i) -= (fp[j]-fm[j])*_doublepertexpm1;
        m(nbdofselem+j,verIndex+nbFF*i) -= (fp[j+3]-fm[j+3])*_doublepertexpm1;
      }
    }
    else{ // perturbation on gravity center
      for(int j=0; j<3; j++){
        m(verIndex+nbFF*j,nbdofselem+i-3) -= (fp[j]-fm[j])*_doublepertexpm1;
        m(nbdofselem+j,nbdofselem+i-3) -= (fp[j+3]-fm[j+3])*_doublepertexpm1;
      }
    }
    // virgin pertdisp
    pertdisp[i] = disp[i];
  }
}

void stiffnessRigidPlaneContact::get(MElement *ele, int npts, IntPt *GP, fullMatrix<double> &m) const{
  int nbdofs = _spc->getNumKeys(ele);
  m.resize(nbdofs,nbdofs,true); // true --> setAll(0.);

  int nDofGrad = _spc->getNumKeysOfGravityCenter();
  int numKeysEle = nbdofs - nDofGrad;

  // search the vertices for which the matrix has to be computed
  contactNodeStiffness * contactNodes = _lterm->getContactNode();

  for(contactNodeStiffness::nodeContainer::const_iterator itn = contactNodes->nBegin(); itn!= contactNodes->nEnd(); ++itn){
    if((*itn)->_ele == ele){ // contact
     //this->get(ele,itn->_verIndex,m);

      int idx = (*itn)->_verIndex;
      const SVector3& normal = (*itn)->_normal;
      int nbFF = ele->getNumShapeFunctions();

      std::vector<Dof> R;
      _spc->getKeys(ele,R);

      std::vector<double> disp;
      _ufield->get(R,disp);


      for (int i=0; i<3; i++){
        for (int j=0; j<3; j++){
          m(idx+i*nbFF,idx+j*nbFF) += _cdom->getPenalty()*normal[i]*normal[j];
          m(numKeysEle+i, numKeysEle+j) -= _cdom->getPenalty()*normal[i]*normal[j];
        }
      }
    }


  }
};
