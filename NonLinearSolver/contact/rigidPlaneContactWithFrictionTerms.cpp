//
// contact base term
//
// Description: contact with a rigid plane with friction
//
//
// Author:  <Van Dung NGUYEN>, (C) 2015
//
// Copyright: See COPYING file that comes with this distribution
//

#include "rigidPlaneContactWithFrictionTerms.h"
#include "unknownField.h"
#include "contactDomain.h"
#include "nodeStiffnessContactWithFriction.h"

void forceRigidPlaneContactWithFriction::get(MElement *ele, int npts, IntPt *GP,fullVector<double> &m) const
{
  const contactNodeStiffness::nodeContainer& previousList = this->getContactNode()->getPreviousContactNodes();

  bool isDG = _cdom->getDomain()->getFormulation();

  int nbdofs = _spc->getNumKeys(ele);
  m.resize(nbdofs);
  m.setAll(0.);

  int nDofGrad = _spc->getNumKeysOfGravityCenter();
  int numKeysEle = nbdofs - nDofGrad;
  int nbFF = ele->getNumShapeFunctions();

  for (int idx=0; idx< ele->getNumVertices(); idx++){
    MVertex* v= ele->getVertex(idx);

    if (vertexInContact.find(v) == vertexInContact.end()){
      if (!isDG){
        vertexInContact.insert(v);
      }
      // check vertex v
      std::vector<Dof> keys;
      _spc->getKeys(ele,idx,keys);
      std::vector<double> disp;
      _ufield->get(keys,disp);

      // change in P
      SPoint3 phis(v->point());
      phis[0] += disp[0];
      phis[1] += disp[1];
      phis[2] += disp[2];
      SVector3 dispSlave(disp[0],disp[1],disp[2]);
      // change in root point
      SPoint3 phim(_cdom->getGC()->point());
      phim[0] += disp[3];
      phim[1] += disp[4];
      phim[2] += disp[5];
      SVector3 dispMaster(disp[3],disp[4],disp[5]);

      SVector3 gapFirst(phis);
      gapFirst -= phim;
      double gapN = dot(gapFirst,_cdom->getNormal());
      if (gapN < 0) {
        // in contact
        //find if previously in contact
        bool found = false;
        const dataContactStiffnessNodeWithFriction* dataPrev = NULL;
        for (int i=0; i< previousList.size(); i++){
          const dataContactStiffnessNode* dtH= previousList[i];
          if (ele->getNum() == dtH->_ele->getNum() and idx == dtH->_verIndex){
            found = true;
            dataPrev = dynamic_cast<const dataContactStiffnessNodeWithFriction*>(dtH);
            break;
          }
        }

        if (found){
          //Msg::Info("previously contact");
          // already contact previously
          if (dataPrev == NULL) Msg::Error("dataContactStiffnessNodeWithFriction must be used");

          SVector3 gap(0.);
          for (int ii =0; ii< 3; ii++){
            gap(ii) = dispSlave(ii) - dataPrev->_dispSlave(ii) + dataPrev->_gap(ii) - (dispMaster(ii) - dataPrev->_dispMaster(ii));
          }
          dataContactStiffnessNodeWithFriction contacNode(ele,idx,_cdom->getNormal(),gap,dispMaster,dispSlave);
          _cdom->getFrictionLaw()->compute(dataPrev,&contacNode,true);

          // note that contact force is assembed to RHS
          for (int i=0; i<3; i++){
            m(idx+i*nbFF) -= contacNode._contactForce(i);
            m(numKeysEle+i) += contacNode._contactForce(i);
          }

          getContactNode()->insert(contacNode.clone());
        }
        else{
          //Msg::Info("first contact");
          // firstime in contact
          SVector3 gap(0.);
          for (int ii =0; ii< 3; ii++){
            gap(ii) = _cdom->getNormal()(ii)*gapN;
          }
          dataContactStiffnessNodeWithFriction contacNode(ele,idx,_cdom->getNormal(),gap,dispMaster,dispSlave);
          _cdom->getFrictionLaw()->compute(NULL,&contacNode,true);

          // note that contact force is assembed to RHS
          for (int i=0; i<3; i++){
            m(idx+i*nbFF) -= contacNode._contactForce(i);
            m(numKeysEle+i) += contacNode._contactForce(i);
          }

          getContactNode()->insert(contacNode.clone());
        }
      }
    }
  }
}


void stiffnessRigidPlaneContactWithFriction::get(MElement *ele, int npts, IntPt *GP, fullMatrix<double> &m) const{
  int nbdofs = _spc->getNumKeys(ele);
  m.resize(nbdofs,nbdofs);
  m.setAll(0.);

  int nDofGrad = _spc->getNumKeysOfGravityCenter();
  int numKeysEle = nbdofs - nDofGrad;

  // search the vertices for which the matrix has to be computed
  contactNodeStiffness * contactNodes = _lterm->getContactNode();
  for(contactNodeStiffness::nodeContainer::const_iterator itn = contactNodes->nBegin(); itn!= contactNodes->nEnd(); ++itn){
    if((*itn)->_ele == ele){
      int idx = (*itn)->_verIndex;
      const dataContactStiffnessNodeWithFriction* fdata = dynamic_cast<const dataContactStiffnessNodeWithFriction*>(*itn);
      const STensor3& DforceDgap = fdata->_DcontactForceDgap;
     // DforceDgap.print("DforceDgap");
      int nbFF = ele->getNumShapeFunctions();
      for (int i=0; i<3; i++){
        for (int j=0; j<3; j++){
          m(idx+i*nbFF,idx+j*nbFF) += DforceDgap(i,j);
          m(numKeysEle+i, numKeysEle+j) -= DforceDgap(i,j);
        }
      }
    }
  }
};

