//
// contact stiffness
//
// Description: Class to manage the Dof for which the stiffness must be computed
//
//
// Author:  <Gauthier BECKER>, (C) 2010
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef CONTACTNODESTIFFNESS_H_
#define CONTACTNODESTIFFNESS_H_
#include "MElement.h"
#include "SVector3.h"
#include "elementGroup.h"

class dataContactStiffnessNode{
 public:
  MElement *_ele;
  int _verIndex;
  SVector3 _normal;
  dataContactStiffnessNode() : _ele(NULL), _verIndex(0), _normal(0.,0.,0.){};
  dataContactStiffnessNode(MElement *ele, const int ind, const SVector3 &nor) : _ele(ele), _verIndex(ind), _normal(nor){}
  dataContactStiffnessNode(const dataContactStiffnessNode &source){
    _ele = source._ele;
    _verIndex = source._verIndex;
    _normal = source._normal;
  }
  virtual ~dataContactStiffnessNode(){};
  virtual dataContactStiffnessNode* clone() const {return new dataContactStiffnessNode(*this);};
};

class contactNodeStiffness{
 public:
  typedef std::vector<dataContactStiffnessNode*> nodeContainer;
 protected:
  nodeContainer _nodeInContact;
  elementGroup *_g; // group with elements in contact
 public:
  contactNodeStiffness(){_g = new elementGroup();}
  contactNodeStiffness(const contactNodeStiffness &source){
    _nodeInContact = source._nodeInContact;
    _g = source._g;
  }
  virtual ~contactNodeStiffness()
  {
    for (nodeContainer::iterator it = _nodeInContact.begin(); it!= _nodeInContact.end(); it++){
      if (*it) delete *it;
    }
    _nodeInContact.clear();
    if(_g !=NULL) delete _g;
  };

  virtual nodeContainer& getCurrentContactNodes(){
   return _nodeInContact;
  }
  virtual const nodeContainer& getCurrentContactNodes() const{
    return _nodeInContact;
  }
  virtual nodeContainer& getPreviousContactNodes(){
    Msg::Warning("this function is used with friction contact");
    static nodeContainer tmp;
    return tmp;
  }

  virtual const nodeContainer& getPreviousContactNodes() const{
    Msg::Warning("this function is used with friction contact");
    static nodeContainer tmp;
    return tmp;
  }

   virtual void nextStep(){}

  virtual void insert(dataContactStiffnessNode* data){
    getCurrentContactNodes().push_back(data);
    _g->insert(data->_ele);
  }

  virtual void insert(MElement *ele,const int i, SVector3 &normal){
    getCurrentContactNodes().push_back(new dataContactStiffnessNode(ele,i,normal));
    _g->insert(ele);
  }
  virtual void clear(){
    for (nodeContainer::iterator it = getCurrentContactNodes().begin(); it!= getCurrentContactNodes().end(); it++){
      if (*it) delete *it;
    }
    getCurrentContactNodes().clear();
    if(_g != NULL) delete _g;
    _g = new elementGroup();
  }

  nodeContainer::const_iterator nBegin() const{return getCurrentContactNodes().begin();}
  nodeContainer::const_iterator nEnd() const{return getCurrentContactNodes().end();}
  elementGroup::elementContainer::const_iterator elemBegin() const{return _g->begin();}
  elementGroup::elementContainer::const_iterator elemEnd() const{return _g->end();}
};
#endif // contactNodeStiffness
