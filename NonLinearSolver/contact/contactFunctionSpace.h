//
// functional space for contact
//
// Description:
//
//
// Author:  <Gauthier BECKER>, (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//

#ifndef CONTACTSPACE_H_
#define CONTACTSPACE_H_
#include "functionSpace.h"
#include "contactElement.h"
template <class T1> class ContactSpaceBase : public FunctionSpaceBase{
  private:
    FunctionSpace<T1> *_spaceSlave; // functional space of slave domain 
    const int _id;
    std::vector<int> _comp;
  public:
    ContactSpaceBase(const int id, FunctionSpace<double> *sp) : FunctionSpaceBase(), _id(id), _spaceSlave(sp){
      _comp.push_back(0);
      _comp.push_back(1);
      _comp.push_back(2);
    }
    ContactSpaceBase(const int id, FunctionSpace<double> *sp, const int comp1,
                     const int comp2 = -1, const int comp3 =-1) : FunctionSpaceBase(), _id(id), _spaceSlave(sp)
    {
      _comp.push_back(comp1);
      if(comp2 !=-1)
        _comp.push_back(comp2);
      if(comp3 != -1)
        _comp.push_back(comp3);
    }
    virtual int getId(void) const{return _id;};

    virtual void getKeys(MElement *ele,std::vector<Dof> &keys) const{
      _spaceSlave->getKeys(ele,keys);
    }
    virtual int getNumKeys(MElement *ele) const{
      return _spaceSlave->getNumKeys(ele);
    }
    virtual FunctionSpace<T1> *getRefToSpaceSlave()
    {
      return _spaceSlave;
    }
    virtual const FunctionSpace<T1> *getConstRefToSpaceSlave() const
    {
      return _spaceSlave;
    }
    virtual const std::vector<int> & getConstRefToComp() const
    {
      return _comp;
    }
};

class rigidContactSpaceBase : public ContactSpaceBase<double>{
 protected:
  const MVertex *_gc; // Node of gravity center
 public:
  rigidContactSpaceBase(const int id, FunctionSpace<double> *sp, MVertex *ver) : ContactSpaceBase<double>(id,sp), _gc(ver){}
  rigidContactSpaceBase(const int id, FunctionSpace<double> *sp, MVertex *ver, const int comp1,
                   const int comp2 = -1, const int comp3 =-1) : ContactSpaceBase<double>(id,sp,comp1,comp2,comp3),_gc(ver){}
  virtual void getKeys(MElement *ele,std::vector<Dof> &keys) const=0;
  virtual int getNumKeys(MElement *ele) const =0;
  virtual void getKeysOfGravityCenter(std::vector<Dof> &keys) const=0;
  virtual int getNumKeysOfGravityCenter() const=0;
  virtual void getKeys(MElement *ele, const int ind, std::vector<Dof> &keys) const=0;
  virtual int getNumKeys(MElement *ele, int ind) const=0; // number key in one component + number of key of Master
};
class defoDefoContactSpaceBase : public ContactSpaceBase<double>{
 protected:
  FunctionSpaceBase* _spaceMaster;
 // Node of gravity center
 public:
  defoDefoContactSpaceBase(const int id, FunctionSpace<double> *spSlave, FunctionSpaceBase *spMaster) : 
                 ContactSpaceBase<double>(id,spSlave), _spaceMaster(spMaster){}
  defoDefoContactSpaceBase(const int id, FunctionSpace<double> *spSlave, FunctionSpaceBase *spMaster, const int comp1,
                   const int comp2 = -1, const int comp3 =-1) : ContactSpaceBase<double>(id,spSlave,comp1,comp2,comp3),_spaceMaster(spMaster) {}
  virtual void getKeys(contactElement *cE,std::vector<Dof> &keys)=0;
  virtual int getNumKeys(contactElement *cE) =0;
  virtual void getKeysMasterNode(contactElement *cE, const int ind, std::vector<Dof> &keys)=0;
  virtual int getNumKeysMasterNode(contactElement *cE, int ind)=0; // number key in one component + number of key of Master
  virtual void getKeysSlaveNode(contactElement *cE, const int ind, std::vector<Dof> &keys)=0;
  virtual int getNumKeysSlaveNode(contactElement *cE, int ind)=0; // number key in one component + number of key of Master
  virtual FunctionSpaceBase *getRefToSpaceMaster()
  {
    return _spaceMaster;
  }
  virtual const FunctionSpaceBase *getConstRefToSpaceMaster() const
  {
    return _spaceMaster;
  }
  virtual int getNumKeysSlaveNodes(contactElement *cE)=0;
  virtual int getNumKeysMasterNodes(contactElement *cE)=0;
};
#endif // CONTACTSPACE_H_

