//
//
// Description: Class to set Defo Defo Contact
//
//
// Author:  <Ludovic Noels>, (C) 2019
//
// Copyright: See COPYING file that comes with this distribution
//
//
// Has to be grouped with BoundaryCondition defined in elasticitySolver.h These BC have to be defined in a separated file
// I add nonLinear above to avoid ambiguities
#ifndef DEFODEFOCONTACT_H_
#define DEFODEFOCONTACT_H_
#ifndef SWIG
#include "elementGroup.h"
#include "simpleFunction.h"
#include "SVector3.h"
#include "quadratureRules.h"
#include "timeFunction.h"
#include "contactFunctionSpace.h"
#include "solverAlgorithms.h" // change this
#include "pbcVertex.h"
#include "nonLinearMicroBC.h"
#include "unknownField.h"
#include "contactElement.h"
#endif
class partDomain;

class defoDefoContactDomain
{
 public:
  enum whichConditionContact{position=0, velocity=1, acceleration=2};
  enum locationContact{UNDEF,ON_VERTEX,ON_EDGE,ON_FACE,ON_VOLUME,ON_FACE_VERTEX};
  enum typeContact{UNDEFTYPE,NODE_ON_SURFACE};

 protected:
  #ifndef SWIG
  locationContact onWhat; // on vertices or elements
  int _tag;
  int _physMaster; // tag for the dofManager or Physical number of BC
  int _dimMaster;
  int _physSlave; // tag for the dofManager or Physical number of BC
  int _dimSlave;
  bool _stiffByPerturbation;
  double _perturbation;
  /* BE AWARE the four following vectors have to be filles in the same time */
  std::vector<defoDefoContactSpaceBase*> _vspace; //here we want to use the defoDefoContactFunctionSpace
//  std::vector<elementGroup*> _vgroupSlave;
//  std::vector<elementGroup*> _vgroupMaster;
  std::vector< std::set<contactElement*> > _group; //when filling the group master and slave we need to fillgroup at the same time Could be removed?
  std::vector<QuadratureBase*> _vquadratureMaster;
  std::vector<partDomain*> _vdomMaster; // Used to create the term after
  partDomain* _vdomSlave; // Used to create the term after
  FilterDof *_filterMaster;
  FilterDof *_filterSlave;
  elementFilter *_fSlave;
  // is fill after
  std::vector<LinearTermBase<double>*> _vterm; // given by partDomain
  std::vector<BilinearTermBase*> _vmatrix_term; // given by partDomain
 public:
  defoDefoContactDomain() : _tag(0),_dimMaster(0),_dimSlave(0),_physMaster(0),_physSlave(0),_fSlave(NULL),onWhat(UNDEF), _stiffByPerturbation(false), _perturbation(1.e-8) {};
  defoDefoContactDomain(const defoDefoContactDomain &source);
  defoDefoContactDomain(const int tag, const int dimMaster, const int phyMaster,
                            const int dimSlave, const int phySlave, elementFilter *fslave=NULL);
  virtual int getTag() const { return _tag;}
  virtual void setTag(int tag) { _tag=tag;}
  virtual int getPhysMaster() const { return _physMaster;}
  virtual int getPhysSlave() const { return _physSlave;}
  virtual void setPhysMaster(int tag) { _physMaster=tag;}
  virtual void setPhysSlave(int tag) { _physSlave=tag;}
  virtual int getDimMaster() const { return _dimMaster;}
  virtual int getDimSlave() const { return _dimSlave;}
  virtual void setDimMaster(int tag) { _dimMaster=tag;}
  virtual void setDimSlave(int tag) { _dimSlave=tag;}

  virtual bool getStiffByPerturbation() const {return _stiffByPerturbation; };
  virtual void setStiffByPerturbation(bool p) { _stiffByPerturbation=p; };
  virtual double getPerturbation() const {return _perturbation; };
  virtual void setPerturbation(double pert) {_perturbation=pert;};


  //virtual std::vector<elementGroup*>& getRefToVGroupSlave() {return _vgroupSlave;};
  //virtual const std::vector<elementGroup*>& getConstRefToVGroupSlave() const {return _vgroupSlave;};
  //virtual std::vector<elementGroup*>& getRefToVGroupMaster() {return _vgroupMaster;};
  //virtual const std::vector<elementGroup*>& getConstRefToVGroupMaster() const {return _vgroupMaster;};
  virtual std::vector<QuadratureBase*> & getRefToVQuadratureMaster() { return _vquadratureMaster;};
  virtual const std::vector<QuadratureBase*>& getConstRefToVQuadratureMaster() const { return _vquadratureMaster;};
  virtual std::vector<partDomain*> &getRefToVDomMaster() {return _vdomMaster;}; // Used to create the term after
  virtual const std::vector<partDomain*> &getConstRefToVDomMaster() const {return _vdomMaster;}; // Used to create the term after

  virtual std::vector<defoDefoContactSpaceBase*> &getRefToVSpace() {return _vspace;}; 
  virtual const std::vector<defoDefoContactSpaceBase*> &getConstRefToVSpace() const {return _vspace;}; 
  virtual std::vector<LinearTermBase<double>*> &getRefToForceTerm(){return _vterm;}; 
  virtual const std::vector<LinearTermBase<double>*> &getConstRefToForceTerm() const {return _vterm;}; 
  virtual std::vector<BilinearTermBase*> getRefToStiffnessTerm() { return _vmatrix_term;}; 
  virtual const std::vector<BilinearTermBase*> getConstRefToStiffnessTerm() const { return _vmatrix_term;}; 

  //we need to also store the group of all the elements
  virtual std::vector< contactElement::groupOfContactElements>& getRefToGroup() {return _group;};
  virtual const std::vector< contactElement::groupOfContactElements >& getConstRefToGroup() const {return _group;};


  virtual ~defoDefoContactDomain()
  {
    this->clear();
  }
  void clear();
  virtual defoDefoContactDomain::typeContact getType()const {
     return defoDefoContactDomain::UNDEFTYPE;
  };
  virtual void setSlaveDomain(partDomain *dom){Msg::Error("defoDefoContactDomain::setSlaveDomain should be defined from dG3DContact");};
  virtual void insertSlaveMasterDomainsAndFunctionSpace(partDomain *slaveDom, elementGroup *gSlave, partDomain *masterDom, elementGroup *gMaster)
  { Msg::Error("defoDefoContactDomain::insertSlaveMasterDomainsAndFunctionSpace should be defined from dG3DContact");};
  virtual void nextStep();
  virtual void initializeTerms(const unknownField *ufield)
  { Msg::Error("defoDefoContactDomain::initializeTerms do not exist");};
  virtual void restart();

#endif
};
/*#ifndef SWIG
// group will be not used but allow to store in same vector
// tag == Master physical number
class rigidContactBC : public nonLinearBoundaryCondition
{
 public:
  rigidContactSpaceBase *space;
  int _comp; // component
  simpleFunctionTime<double> *_f;
  FilterDof *_filter;
  virtual nonLinearBoundaryCondition::type getType() const{return nonLinearBoundaryCondition::RCONTACT;}
  rigidContactBC(const int physMaster) : nonLinearBoundaryCondition() , _filter(NULL),_f(NULL){
    _tag = physMaster;
    onWhat = RIGIDCONTACT;
  }
  rigidContactBC(const rigidContactBC &source) : nonLinearBoundaryCondition(source)
  {
    _comp = source._comp;
    space = source.space;
    _f = source._f;
    _filter = source._filter;
  }
  virtual ~rigidContactBC(){
		if (_filter != NULL) {delete _filter; _filter = NULL;};
		if (_f != NULL){delete _f; _f = NULL;} 
	}
  void setSpace(rigidContactSpaceBase *sp){space = sp;}
};
#endif // SWIG*/
class nodeOnSurfaceDefoDefoContactDomain  : public defoDefoContactDomain
{
 protected: 
  double _penalty, _thickness;

 public:
  nodeOnSurfaceDefoDefoContactDomain () : defoDefoContactDomain(),_penalty(0.), _thickness(0.) {}

  nodeOnSurfaceDefoDefoContactDomain (const int tag, const int dimMaster, const int phyMaster,
                            const int dimSlave, const int phySlave, const double penalty, const double thick,
                            elementFilter *fslave=NULL) : defoDefoContactDomain(tag,dimMaster,phyMaster,dimSlave,phySlave,fslave),
                                _penalty(penalty), _thickness(thick)
  {
    if(dimMaster==2)
      onWhat=ON_FACE;
    else if(dimMaster==1)
      onWhat=ON_EDGE;

  }

  nodeOnSurfaceDefoDefoContactDomain(const nodeOnSurfaceDefoDefoContactDomain &source); 
  virtual ~nodeOnSurfaceDefoDefoContactDomain(){
    this->clear();
  }
  virtual defoDefoContactDomain::typeContact getType() const {return defoDefoContactDomain::NODE_ON_SURFACE;}
  void clear();
  double getPenalty() const {return _penalty;}
  double getThickness() const {return _thickness;}
  virtual void setSlaveDomain(partDomain *dom){Msg::Error("nodeOnSurfaceDefoDefoContactDomain::setSlaveDomain should be defined from dG3DContact");};
  virtual void insertSlaveMasterDomainsAndFunctionSpace(partDomain *slaveDom, elementGroup *gSlave, partDomain *masterDom, elementGroup *gMaster)
  { Msg::Error("nodeOnSurfaceDefoDefoContactDomain::insertSlaveMasterDomainsAndFunctionSpace should be defined from dG3DContact");};
  virtual void initializeTerms(const unknownField *ufield);
  virtual void restart();
};


#endif // DEFODEFOCONTACT
