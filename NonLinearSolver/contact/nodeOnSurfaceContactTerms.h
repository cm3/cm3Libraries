//
// contact base term
//
// Description: contact between a node and a patch
//
//
// Author:  <L. Noels>, (C) 2019
//
// Copyright: See COPYING file that comes with this distribution
//

#ifndef NODEONSURFACECONTACTTERMS_H_
#define NODEONSURFACECONTACTTERMS_H_


#include "contactTerms.h"
#include "contactFunctionSpace.h"

class contactTools{
  public:
    contactTools(){}
    contactTools(const contactTools &source){}
    ~contactTools(){}
    virtual bool evaluateNormalDirection(const partDomain* domMaster, contactElement *ele) const;
  
  protected:
    virtual void computeNormalOfElement2D(const partDomain* d,SVector3& normal) const;
    virtual bool isSlaveInBox(const fullVector<double> &disp, defoDefoContactSpaceBase *spc, contactElement *ele, const SPoint3 &pSlave, double thickContact, const  std::vector<SPoint3> &pMaster) const;
    virtual void getUpdatedPosition(const fullVector<double> &disp, defoDefoContactSpaceBase *spc, contactElement *ele, int idx, bool isSlave, SPoint3 &pos) const;
    virtual void getLocalBasisAtUVW(const fullVector<double> &disp, defoDefoContactSpaceBase *spc, contactElement *ele, const  std::vector<SPoint3> &pMaster, 
                                      const SVector3 &domainNormalFor2D,
                                      const SPoint3 &uvw, SPoint3 &pOnSurface, SVector3& n, SVector3& t, SVector3& b, 
                                      STensor3 &dPOnSurfacedUVW, STensor3 &dTdUVW,STensor3 &dBdUVW, bool evaluateDerivativesUVW,
                                      std::vector<STensor3> &dPOnSurfacedMasterNode, std::vector<STensor3> &dTdMasterNode, std::vector<STensor3> &dBdMasterNode, 
                                      bool evaluateDerivativesMasterNode) const;
    virtual bool projectSlaveOnMaster(const fullVector<double> &disp, defoDefoContactSpaceBase *spc, contactElement *ele, const  std::vector<SPoint3> &pMaster, 
                                      const SVector3 &domainNormalFor2D, const SPoint3 &pSlave,
                                      SPoint3 &uvw, SPoint3 &pOnSurface, SVector3& n, SVector3& t, SVector3& b) const;
    virtual void getUpdatedMasterPositions(const fullVector<double> &disp, defoDefoContactSpaceBase *spc, contactElement *ele, std::vector<SPoint3> &pMaster) const;
    
};


class forceNodeOnSurfaceDefoDefoContactTerm : public defoDefoContactLinearTermBase<double>, public contactTools{
  protected:
    double _thickContact,_penalty;
  public:
    forceNodeOnSurfaceDefoDefoContactTerm(defoDefoContactSpaceBase *sp, const unknownField *ufield,
                                const partDomain* domSlave, const partDomain* domMaster,
                                double thickContact, double penalty) : 
                                   defoDefoContactLinearTermBase<double>(sp, ufield, domSlave, domMaster), contactTools(),
                                        _thickContact(thickContact),_penalty(penalty) 
    {
      if(_domMaster->getDim()==2)
      {
        computeNormalOfElement2D(_domMaster,_domainNormalFor2D);
      }
    };

    forceNodeOnSurfaceDefoDefoContactTerm(const forceNodeOnSurfaceDefoDefoContactTerm &source) :
                                       defoDefoContactLinearTermBase<double>(source), contactTools(source), _thickContact(source._thickContact),
                                         _penalty(source._penalty)
    {    }
    virtual ~forceNodeOnSurfaceDefoDefoContactTerm(){}

    virtual void get(contactElement *cE, int npts, IntPt *GP, fullVector<double> &v) const;
    virtual void get(contactElement *cE, int npts, IntPt *GP, fullVector<double> &vSlave, const fullVector<double> &disp) const;

    virtual LinearTermBase<double>* clone () const
    {
      return new forceNodeOnSurfaceDefoDefoContactTerm(*this);
    }
    virtual void detectNodesInContact(contactElement *cE, const fullVector<double> &disp) const;
  protected:
    virtual void computeNormalOfElement2D(const partDomain* d, SVector3& normal) const
    {
      contactTools::computeNormalOfElement2D(d, normal);  
    }  
};

class stiffnessNodeOnSurfaceDefoDefoContactTerm : public defoDefoContactBilinearTermBase<double>, public contactTools{
 protected:
  double _thickContact,_penalty;

 public:
  stiffnessNodeOnSurfaceDefoDefoContactTerm(defoDefoContactSpaceBase *sp, contactLinearTermBase<double> *lterm, const unknownField *ufield,
                                const partDomain* domSlave, const partDomain* domMaster,
                                double thickContact, double penalty) : 
                                   defoDefoContactBilinearTermBase<double>(sp, lterm, ufield, domSlave, domMaster), contactTools(),
                                        _thickContact(thickContact),_penalty(penalty) 
  {
    if(_domMaster->getDim()==2)
    {
      computeNormalOfElement2D(_domMaster,_domainNormalFor2D);
    }
  }
  stiffnessNodeOnSurfaceDefoDefoContactTerm(const stiffnessNodeOnSurfaceDefoDefoContactTerm &source) :
                                       defoDefoContactBilinearTermBase<double>(source), contactTools(source), _thickContact(source._thickContact),
                                         _penalty(source._penalty)
  {
  }

  ~stiffnessNodeOnSurfaceDefoDefoContactTerm(){}

  virtual void get(contactElement *ele, int npts, IntPt *GP, fullMatrix<double> &m) const;
  virtual BilinearTermBase* clone () const
  {
    return new stiffnessNodeOnSurfaceDefoDefoContactTerm(*this);
  }
 protected:
  virtual void computeNormalOfElement2D(const partDomain* d, SVector3& normal) const
  {
      contactTools::computeNormalOfElement2D(d,normal);  
  }  

};


#endif // NODEONSURFACECONTACTTERMS_H_
