//
// contact base term
//
// Description: contact with a rigid sphere
//
//
// Author:  <Tianyu ZHANG>, (C) 2020
//
// Copyright: See COPYING file that comes with this distribution
//

#ifndef RIGIDCONTACTSPHERE_H_
#define RIGIDCONTACTSPHERE_H_

#include "contactTerms.h"
#include "contactFunctionSpace.h"

class rigidSphereContactDomain; // import the class in contactDomain.h to avoid the crossed include
class massRigidSphere : public BilinearTermBase{
 protected:
  double _radius, _rho;
  rigidContactSpaceBase *_spc;

 public:
  massRigidSphere(rigidSphereContactDomain *cdom,rigidContactSpaceBase *spc);
  massRigidSphere(rigidContactSpaceBase *spc,double radius,double rho) : _spc(spc), _radius(radius), _rho(rho){}
  // Arguments are useless but it's allow to use classical assemble function (?)
  void get(MElement *ele, int npts, IntPt *GP, fullMatrix<double> &m) const;
  virtual void get(MElement *ele, int npts, IntPt *GP, std::vector<fullMatrix<double> > &mv) const
  {
    Msg::Error("Define me get by integration point massRigidSphere");
  }
  virtual BilinearTermBase* clone () const
  {
    return new massRigidSphere(_spc,_radius,_rho);
  }
};

class forceRigidSphereContact : public rigidContactLinearTermBase<double>{
 protected:
  const rigidSphereContactDomain *_cdom;
  double _fdgfactor, _facGC;
  const double _radius;
  double _penalty;
  const SPoint3 _GC; // SPoint3 object should be declared in .h once and for all because its creation could take more time than double 
  // Data of get function (Allocated once)
  mutable SPoint3 B,Bdisp,A,Adisp;
 public:
  forceRigidSphereContact(const rigidSphereContactDomain *cdom, rigidContactSpaceBase *sp, const unknownField *ufield);
  ~forceRigidSphereContact(){}
//  void get(MElement *ele,int npts, IntPt *GP,fullVector<double> &m);
  // specific function to this contact type ( put in rigidContactSpaceBase ??)
  // This one cannot be called by Assemble function (?)
  void get(const MVertex *ver, const double disp[6],double mvalue[6]) const;
  virtual void get(MElement *ele, int npts, IntPt *GP, std::vector<fullVector<double> > &vv) const
  {
    Msg::Error("define me get by gauss point forceRigidSphereContact");
  }
  virtual LinearTermBase<double>* clone () const
  {
    return new forceRigidSphereContact(_cdom,this->_spc,_ufield);
  }
};

class stiffnessRigidSphereContact : public contactBilinearTermBase<double>{
 protected:
  const unknownField *_ufield;
  rigidContactSpaceBase *_spc;
//  const rigidPlaneContactDomain *_cdom; (need for analytic, also need to be added in following constructor and initializer list)
  void get(MElement *ele, const int verIndex, fullMatrix<double> &m) const;
 public:
  stiffnessRigidSphereContact(rigidContactSpaceBase *space, contactLinearTermBase<double> *lterm, const unknownField *ufield) :  			contactBilinearTermBase<double>(lterm),_ufield(ufield),_spc(space){} // (if analytic : add _cdom(cdom))
  ~stiffnessRigidSphereContact(){}
  void get(MElement *ele, int npts, IntPt *GP, fullMatrix<double> &m) const;
  virtual void get(MElement *ele, int npts, IntPt *GP, std::vector<fullMatrix<double> > &mv) const
  {
    Msg::Error("Define me get by integration point stiffnessRigidSphereContact");
  }
  virtual BilinearTermBase* clone () const
  {
    return new stiffnessRigidSphereContact(_spc,_lterm,_ufield); // (if analytic : add _cdom(cdom))
  }
};


#endif // RIGIDCONTACTSPHERE_H_

