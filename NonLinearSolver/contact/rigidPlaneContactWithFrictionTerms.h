//
// contact base term
//
// Description: contact with a rigid plane with friction
//
//
// Author:  <Van Dung NGUYEN>, (C) 2015
//
// Copyright: See COPYING file that comes with this distribution
//


#ifndef RIGIDPLANECONTACTWITHFRICTIONTERMS_H_
#define RIGIDPLANECONTACTWITHFRICTIONTERMS_H_

#include "rigidPlaneContactTerms.h"
#include "nodeStiffnessContactWithFriction.h"

class forceRigidPlaneContactWithFriction : public rigidContactLinearTermBase<double>{
  protected:
    const rigidPlaneContactDomain *_cdom;

  private:
    mutable std::set<MVertex*> vertexInContact;

  public:
    forceRigidPlaneContactWithFriction(const rigidPlaneContactDomain *cdom,
                            rigidContactSpaceBase *sp,
                            const unknownField *ufield): rigidContactLinearTermBase<double>(sp,ufield),_cdom(cdom){
      if (_allcontactNode) delete _allcontactNode;
      _allcontactNode = new contactNodeStiffnessWithFriction();
    };
    virtual ~forceRigidPlaneContactWithFriction(){}

    void get(const MVertex *ver, const double disp[6],double mvalue[6]) const{
      Msg::Warning("forceRigidPlaneContactWithFriction::get(const MVertex *ver, const double disp[6],double mvalue[6]) is not used");
    };
    virtual void get(MElement *ele, int npts, IntPt *GP,fullVector<double> &m) const;
    virtual void get(MElement *ele, int npts, IntPt *GP, std::vector<fullVector<double> > &vv) const
    {
      Msg::Error("define me get by gauss point forceRigidPlaneContactWithFriction");
    }
    virtual LinearTermBase<double>* clone () const
    {
      return new forceRigidPlaneContactWithFriction(_cdom,this->_spc,_ufield);
    }

    virtual void clearContactNodes(){
      rigidContactLinearTermBase<double>::clearContactNodes();
      vertexInContact.clear();
    }
};


class stiffnessRigidPlaneContactWithFriction : public contactBilinearTermBase<double>{
 protected:
  const unknownField *_ufield;
  rigidContactSpaceBase *_spc;
  const rigidPlaneContactDomain *_cdom;

 public:
  stiffnessRigidPlaneContactWithFriction(const rigidPlaneContactDomain *cdom, rigidContactSpaceBase *space,
                    contactLinearTermBase<double> *lterm, const unknownField *ufield) : contactBilinearTermBase<double>(lterm),
                                                                                                _cdom(cdom),_ufield(ufield),_spc(space){}
  virtual ~stiffnessRigidPlaneContactWithFriction(){}
  void get(MElement *ele, int npts, IntPt *GP, fullMatrix<double> &m) const;
  virtual void get(MElement *ele, int npts, IntPt *GP, std::vector<fullMatrix<double> > &mv) const
  {
    Msg::Error("Define me get by integration point stiffnessRigidPlaneContactWithFriction");
  }
  virtual BilinearTermBase* clone () const
  {
    return new stiffnessRigidPlaneContactWithFriction(_cdom,_spc,_lterm,_ufield);
  }
};

#endif // RIGIDPLANECONTACTWITHFRICTIONTERMS_H_
