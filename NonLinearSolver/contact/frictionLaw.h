//
//
// Description: friction law
//
//
// Author:  <Van Dung NGUYEN>, (C) 2015
//
// Copyright: See COPYING file that comes with this distribution
//


#ifndef FRICTIONLAW_H_
#define FRICTIONLAW_H_

#ifndef SWIG
class dataContactStiffnessNodeWithFriction;
#endif // SWIG
class frictionLaw {
#ifndef SWIG
  protected:
    int _num;
    bool _init;

  public:
    frictionLaw(const int n, const bool init = true): _num(n),_init(init){}
    frictionLaw(const frictionLaw& src):_num(src._num),_init(src._init){}
    virtual ~frictionLaw(){}
    virtual void compute(const dataContactStiffnessNodeWithFriction* prev, dataContactStiffnessNodeWithFriction* curr, const bool stiff) const = 0;
    virtual frictionLaw* clone() const= 0;
 #endif // SWIG
};

// f = abs(t_T) - A - mu*abs(t_N)
class CoulombFrictionLaw : public frictionLaw{
  protected:
    double _A; // adherence
    double _mu; // friction coefficient
    double _kN; // normal penalty
    double _kT; // tangen penalty

  public:
    CoulombFrictionLaw(const int n, const double A, const double mu,
                        const double kn, const double kt, const bool init = true);
    #ifndef SWIG
    CoulombFrictionLaw(const CoulombFrictionLaw& src): frictionLaw(src),_mu(src._mu),_A(src._A),
                        _kN(src._kN),_kT(src._kT){}
    virtual ~CoulombFrictionLaw(){}

    virtual void compute(const dataContactStiffnessNodeWithFriction* prev, dataContactStiffnessNodeWithFriction* curr, const bool stiff) const;
    virtual frictionLaw* clone() const {return new CoulombFrictionLaw(*this);};
    #endif // SWIG
};
#endif // FRICTIONLAW_H_
