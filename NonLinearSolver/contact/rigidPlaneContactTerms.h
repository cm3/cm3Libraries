//
// contact base term
//
// Description: contact with a rigid plane without friction
//
//
// Author:  <Van Dung NGUYEN>, (C) 2015
//
// Copyright: See COPYING file that comes with this distribution
//

#ifndef RIGIDPLANECONTACTTERMS_H_
#define RIGIDPLANECONTACTTERMS_H_


#include "contactTerms.h"
#include "contactFunctionSpace.h"


class rigidPlaneContactDomain;
class massRigidPlane : public BilinearTermBase{
 protected:
  double _thickness, _rho;
  rigidContactSpaceBase *_spc;

 public:
  massRigidPlane(rigidPlaneContactDomain *cdom,rigidContactSpaceBase *spc);
  massRigidPlane(rigidContactSpaceBase *spc, double thick,double rho) : _spc(spc),_thickness(thick), _rho(rho){}
  // Arguments are useless but it's allow to use classical assemble function
  void get(MElement *ele, int npts, IntPt *GP, fullMatrix<double> &m) const;
  virtual void get(MElement *ele, int npts, IntPt *GP, std::vector<fullMatrix<double> > &mv) const
  {
    Msg::Error("Define me get by integration point massRigidCylinder");
  }
  virtual BilinearTermBase* clone () const
  {
    return new massRigidPlane(_spc,_thickness,_rho);
  }
};

class forceRigidPlaneContact : public rigidContactLinearTermBase<double>{
  protected:
    const rigidPlaneContactDomain *_cdom;

  public:
    forceRigidPlaneContact(const rigidPlaneContactDomain *cdom,
                            rigidContactSpaceBase *sp,
                            const unknownField *ufield): rigidContactLinearTermBase<double>(sp,ufield),_cdom(cdom){};
    virtual ~forceRigidPlaneContact(){}

    void get(const MVertex *ver, const double disp[6],double mvalue[6]) const;
    virtual void get(MElement *ele, int npts, IntPt *GP, std::vector<fullVector<double> > &vv) const
    {
      Msg::Error("define me get by gauss point forceRigidPlaneContact");
    }
    virtual LinearTermBase<double>* clone () const
    {
      return new forceRigidPlaneContact(_cdom,this->_spc,_ufield);
    }
};


class stiffnessRigidPlaneContact : public contactBilinearTermBase<double>{
 protected:
  const unknownField *_ufield;
  rigidContactSpaceBase *_spc;
  const rigidPlaneContactDomain *_cdom;

  void get(MElement *ele, const int verIndex, fullMatrix<double> &m) const;

 public:
  stiffnessRigidPlaneContact(const rigidPlaneContactDomain *cdom, rigidContactSpaceBase *space,
                    contactLinearTermBase<double> *lterm, const unknownField *ufield) : contactBilinearTermBase<double>(lterm),
                                                                                                _cdom(cdom),_ufield(ufield),_spc(space){}
  ~stiffnessRigidPlaneContact(){}
  void get(MElement *ele, int npts, IntPt *GP, fullMatrix<double> &m) const;
  virtual void get(MElement *ele, int npts, IntPt *GP, std::vector<fullMatrix<double> > &mv) const
  {
    Msg::Error("Define me get by integration point stiffnessRigidPlaneContact");
  }
  virtual BilinearTermBase* clone () const
  {
    return new stiffnessRigidPlaneContact(_cdom,_spc,_lterm,_ufield);
  }
};


#endif // RIGIDPLANECONTACTTERMS_H_
