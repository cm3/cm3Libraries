//
// contact base term
//
// Description: contact with a rigid cone
//
//
// Author:  <Gauthier BECKER>, (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
#include "rigidConeContactTerms.h"
#include "contactDomain.h"
#include "unknownField.h"
massRigidCone::massRigidCone(rigidConeContactDomain *cdom,rigidContactSpaceBase *spc) : _heightCone(cdom->getHeightCone()),
                                                                                        _heightCylinder(cdom->getHeightCylinder()),
                                                                                        _baseRadius(cdom->getRadius()),
                                                                                        _thick(cdom->getThickness()),
                                                                                        _rho(cdom->getDensity()),
                                                                                        _spc(spc){}
void massRigidCone::get(MElement *ele,int npts,IntPt *GP,fullMatrix<double> &m) const
{
  // full cone only for now
  int nbdofs = _spc->getNumKeysOfGravityCenter();
  m.resize(nbdofs,nbdofs,true); // true reset all value to zero
  double masse;
  if(_thick > _baseRadius)
  {
    masse = _rho * M_PI*_baseRadius*_baseRadius * (0.333333333*_heightCone + _heightCylinder);
  }
  else
  {
    Msg::Error("Hollow cone contact is not implemented");
  }
  for(int i=0;i<nbdofs;i++)
    m(i,i) = masse;
}

forceRigidConeContact::forceRigidConeContact(const rigidConeContactDomain *cdom,
                                             rigidContactSpaceBase *sp,const double thick,
                                             const unknownField *ufield) : rigidContactLinearTermBase<double>(sp,ufield), _cdom(cdom),
                                                                           _vergcCone(cdom->getGCcone()),
                                                                           _vergcTot(cdom->getGC()),
                                                                           _axisDirector(cdom->getAxisDirector()),
                                                                           _GCcone(cdom->getGCcone()->point()),
                                                                           _GCtot(cdom->getGC()->point()),
                                                                           _penalty(cdom->getPenalty()),
                                                                           _heightCone(cdom->getHeightCone()),
                                                                           _heightCylinder(cdom->getHeightCylinder()),
                                                                           _alpha(cdom->getAlpha()),
                                                                           _baseRadius(cdom->getRadius())
{
  _ptTop.setPosition(_GCcone,_axisDirector->point(),0.75*_heightCone);
  if(_cdom->getDomain()->IsInterfaceTerms()){
    partDomain *dom = _cdom->getDomain();
    if(dom->getFormulation()){
      _facGC =0.5;
    }
    else{
      _facGC =1.;
    }
  }
  else{
    _facGC = 1.;
  }
}

void forceRigidConeContact::get(const MVertex *ver, const double disp[6],double mvalue[6]) const
{
  // B = axis' center
  // A = vertex for which we look for contact
  B = _GCcone; // origin at cone gravity center
  Bdisp.setPosition(disp[3],disp[4],disp[5]);
  B+=Bdisp;
  // Distance between vertex and cylinder axis
  // line BA
  A = ver->point();
  Adisp.setPosition(disp[0],disp[1],disp[2]);
  A+=Adisp;
  SVector3 BA(A,B);
  dirAC = crossprod(BA,*_axisDirector);
  double d = dirAC.norm(); // norm _axisDirector == 1.

  // for the cone the contact radius depends on the position of C
  dirAC.normalize();
  _lastNormalContact = crossprod(dirAC,*_axisDirector);
  _lastNormalContact.normalize();
  C.setPosition(A,_lastNormalContact.point(),-d);
  // check normal direction if C is further of B than A the direction is wrong
  if(C.distance(B) > A.distance(B)){
    _lastNormalContact.negate();
    _lastNormalContact.normalize();
    C.setPosition(A,_lastNormalContact.point(),-d);
  }
  // length DC = d*tg(alpha)
  double dDC = d*tan(_alpha);
  D.setPosition(C,_axisDirector->point(),-dDC);
  // Check if contact occurs in the cone or cylinder zone
  double dBC = C.distance(B);
  SVector3 BC = SVector3(B,C);
  bool contactZone=false;
  bool contactZoneCylinder=false;
  double BCdotAxis = BC(0)*_axisDirector->operator()(0) + BC(1)*_axisDirector->operator()(1) + BC(2)*_axisDirector->operator()(2);
  if( BCdotAxis< 0) // contact between the base and the GC (length = h/4)
  {
    if(dBC < 0.25*_heightCone)
      contactZone = true;
    else if(dBC < 0.25*_heightCone + _heightCylinder)
      contactZoneCylinder = true;
  }
  else
  {
    if(dBC < 0.75*_heightCone)
    {
      contactZone = true;
    }
  }
  if(contactZone) // contact possibilities
  {
    /* value of cylinder radius at point C */
    // current position of top
    SPoint3 ptTop = _ptTop;
    ptTop += Bdisp; // same displacement than GC as rigid
    double dCptTop = C.distance(ptTop);
    double coner = dCptTop*tan(_alpha);

    /* contact */
    if(d <coner)
    {
      double penpen = _penalty * (coner-d)/cos(_alpha); // penetration = (radius - distance to axis)/cos(angle)
      // normal value (line DA)
      SVector3 DA(D,A);
      DA.normalize();
      _lastNormalContact = DA;
      for(int j=0;j<3;j++){
        mvalue[j]+= penpen*_lastNormalContact(j);
        mvalue[j+3] -=_facGC*penpen*_lastNormalContact(j); // count two times
      }
    }
  }
  else if(contactZoneCylinder) // contact in the cylindrical part
  {
    double penpen = _penalty * (_baseRadius-d);
    // normal = vector CA
    SVector3 CA(C,A);
    CA.normalize();
    _lastNormalContact = CA;
    for(int j=0;j<3;j++){
      mvalue[j]+= penpen*_lastNormalContact(j);
      mvalue[j+3] -=_facGC*penpen*_lastNormalContact(j); // count two times
    }
  }
}


void stiffnessRigidConeContact::get(MElement *ele, int npts, IntPt *GP, fullMatrix<double> &m) const
{
  int nbdofs = _spc->getNumKeys(ele);
  m.resize(nbdofs,nbdofs,true); // true --> setAll(0.)
  // search the vertices for which the matrix has to be computed
  contactNodeStiffness * contactNodes = _lterm->getContactNode();
  for(contactNodeStiffness::nodeContainer::const_iterator itn = contactNodes->nBegin(); itn!= contactNodes->nEnd(); ++itn){
    if((*itn)->_ele == ele){ // contact
      this->get(ele,(*itn)->_verIndex,m);
    }
  }
}

// Protected can only be called by stiffnessRigidCylinderContact::get(MElement *ele, int npts, IntPt *GP, fullMatrix<double> &m)
// NO RESIZE
void stiffnessRigidConeContact::get(MElement *ele, const int verIndex, fullMatrix<double> &m) const
{
  // perturbation numeric
  rigidContactLinearTermBase<double>* rlterm = static_cast<rigidContactLinearTermBase<double>*>(_lterm);
  int nbdofselem = _spc->getNumKeys(ele) - _spc->getNumKeysOfGravityCenter();
  int nbFF = ele->getNumVertices();
  MVertex *ver = ele->getVertex(verIndex);
  disp.clear(); R.clear();
  _spc->getKeys(ele,verIndex,R);
  _ufield->get(R,disp);
  for(int j=0;j<6;j++)
    pertdisp[j] = disp[j];

  // perturbation on each Dofs 3 for vertex and 3 for GC of cylinder
  for(int i=0;i<6;i++){
    // set force component to zero
    for(int j=0;j<6;j++)
      fp[j] = fm[j] = 0.;

    // Force -
    pertdisp[i] = disp[i] - _perturbation;
    rlterm->get(ver,pertdisp,fm);

    // Force +
    pertdisp[i] = disp[i] + _perturbation;
    rlterm->get(ver,pertdisp,fp);

    // Assembly in matrix
    if(i<3){ // perturbation on vertex
      for(int j=0; j<3; j++){
        m(verIndex+nbFF*j,verIndex+nbFF*i) -= (fp[j]-fm[j])*_doublepertexpm1;
        m(nbdofselem+j,verIndex+nbFF*i) -= (fp[j+3]-fm[j+3])*_doublepertexpm1;
      }
    }
    else{ // perturbation on gravity center
      for(int j=0; j<3; j++){
        m(verIndex+nbFF*j,nbdofselem+i-3) -= (fp[j]-fm[j])*_doublepertexpm1;
        m(nbdofselem+j,nbdofselem+i-3) -= (fp[j+3]-fm[j+3])*_doublepertexpm1;
      }
    }
    // virgin pertdisp
    pertdisp[i] = disp[i];
  }

  // ANALYTIC WAY MUST BE FIXED
//  int nbdofs = _spc->getNumKeys(ele);
//  int nbvertex = ele->getNumVertices();
//  int nbdofGC = _spc->getNumKeysOfGravityCenter();
//  int nbcomp = (nbdofs-nbdofGC)/nbvertex;
//  int nbcompTimesnbVertex = nbcomp*nbvertex;
//  m.resize(nbdofs,nbdofs);
//  m.setAll(0.);
//  double penalty = _fdgfactor*_cdom->getPenalty();
//  for(int i=0;i<nbvertex;i++){
//    SVector3 nor = _allcontactNode->getNormal(ele,i);
//    if(nor.norm() != 0.){ // otherwise no contact
 //     double dianor[3][3];
 //     diaprod(nor,nor,dianor);
 //     for(int j=0;j<3;j++)
 //       for(int k=0;k<3;k++)
  //        dianor[j][k]*=penalty;
  //    for(int j=0;j<nbcomp;j++)
  //      for(int k=0;k<nbcomp;k++){
  //        m(i+j*nbvertex,i+k*nbvertex) -= dianor[j][k];
  //        m(nbcompTimesnbVertex+j,nbcompTimesnbVertex+k) -=0.5*dianor[j][k];
  //      }
  //  }
  //}
}

