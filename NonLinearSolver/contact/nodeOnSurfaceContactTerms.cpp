//
// contact base term
//
// Description: contact between a node and a patch
//
//
// Author:  <L. Noels>, (C) 2019
//
// Copyright: See COPYING file that comes with this distribution
//

#include "nodeOnSurfaceContactTerms.h"
#include "defoDefoContactDomain.h"



void forceNodeOnSurfaceDefoDefoContactTerm::detectNodesInContact(contactElement *ele, const fullVector<double> &disp) const
{
 //here we need to fill slave node 1, x, slave node 2, x,slave node 3, x, ... slave node 1, y etc and then master node 1, x, master node 2, x, etc
  nodeOnSurfaceContactElement *cele= static_cast<nodeOnSurfaceContactElement*>(ele); 
  nodesOnSurfaceInContact& nodesInContact=cele->getRefToNodesOnSurfaceInContact();
  nodesInContact.clear(); 

  bool rightNormal=cele->getNormalOrientation();

  int nbFFSlave = ele->getConstRefToElementSlave()->getNumShapeFunctions();
  int nbFFMaster = ele->getConstRefToElementMaster()->getNumShapeFunctions();
  int dim = ele->getConstRefToElementMaster()->getDim();

  int nbVerticesSlave = ele->getConstRefToElementSlave()->getNumPrimaryVertices(); //getNumVertices(); in order to avoid nodes with negative shape functions to be involved
  int nbVerticesMaster = ele->getConstRefToElementMaster()->getNumVertices();

  static std::vector<SPoint3> pMaster;
  pMaster.clear();
  getUpdatedMasterPositions(disp, _spc, ele, pMaster);

  SPoint3 pSlave;
  for (int idx=0; idx< nbVerticesSlave; idx++)
  {

    getUpdatedPosition(disp, _spc, ele,idx,true,pSlave);

    bool inBox=isSlaveInBox(disp,_spc,ele,pSlave,_thickContact, pMaster);
    //if(inBox) Msg::Info("slave node %d is in box",idx);
    //if(!inBox) Msg::Info("slave node %d is not in box",idx);
    if(inBox)
    {
      SPoint3 uvw(0.,0.,0.);
      SPoint3 pOnSurface;
      SVector3 n,t,b;
      bool succeed=projectSlaveOnMaster(disp, _spc,  ele, pMaster, _domainNormalFor2D, pSlave,
                              uvw, pOnSurface, n, t, b);
      if(succeed)
      {
        SVector3 projToSlave(pSlave[0]-pOnSurface[0],pSlave[1]-pOnSurface[1],pSlave[2]-pOnSurface[2]);
        double gap=dot(projToSlave,n)/n.norm();
        if(!rightNormal) gap*=-1.; //only used to check if penetration or not, the forces will be correct
        double tol=1.e-8*n.norm(); //avoid considering nodes too close from the surface
        if(gap<-tol and -gap<_thickContact and ele->getConstRefToElementMaster()->isInside(uvw[0],uvw[1],uvw[2]))
        {
          //Msg::Info("I am here");
          nodesInContact.insert(idx, n, t, b, uvw, pOnSurface);
        } 
      }
    }
  } 
}
void forceNodeOnSurfaceDefoDefoContactTerm::get(contactElement *cE, int npts, IntPt *GP, fullVector<double> &m, const fullVector<double> &disp) const
{
  this->detectNodesInContact(cE,disp);  //we should do the detection before this get, to come...

  nodeOnSurfaceContactElement *cele= static_cast<nodeOnSurfaceContactElement*>(cE); 
  const nodesOnSurfaceInContact& nodesInContact=cele->getConstRefToNodesOnSurfaceInContact();
  //Msg::Error("HERE TO FILL forceNodeOnSurfaceDefoDefoContactTerm::get");

  int nbdofs = _spc->getNumKeys(cE);
  int nbdofpervertex = _spc->getNumKeysSlaveNode(cE, 0);

  m.resize(nbdofs);
  m.setAll(0.); //here we need to fill slave node 1, x, slave node 2, x,slave node 3, x, ... slave node 1, y etc and then master node 1, x, master node 2, x, etc

  int nbFFSlave = cele->getConstRefToElementSlave()->getNumShapeFunctions();
  int nbFFMaster = cele->getConstRefToElementMaster()->getNumShapeFunctions();

  int nbSlaveKeys=_spc->getNumKeysSlaveNodes(cE);
  static std::vector<TensorialTraits<double>::ValType> s;
  double suvw[256];


  for(nodesOnSurfaceInContact::nodesContainer::const_iterator it=nodesInContact.nBegin(); it!=nodesInContact.nEnd(); it++)
  { 
    //(*it).print();
    const SVector3 &n=(*it).getConstRefToNormal();
    const SVector3 &t1=(*it).getConstRefToTangent1();
    const SVector3 &t2=(*it).getConstRefToTangent2();
    const SPoint3 &uvw=(*it).getConstRefToRedCoordinates();
    const SPoint3 &proj=(*it).getConstRefToProjection();
    int idSlave=(*it).getSlaveIndex();
    SPoint3 pSlave;
    getUpdatedPosition(disp, _spc, cE,idSlave,true,pSlave);
    SVector3 projToSlave(pSlave[0]-proj[0],pSlave[1]-proj[1],pSlave[2]-proj[2]);
    double gap=dot(projToSlave,n)/n.norm();    

    SVector3 force(n);
    force.normalize();
    force*=_penalty*(-gap); //since gap is <0, this gives the load on the slave node
    // NOT SURE IN 2D
    for(int i=0; i< nbdofpervertex; i++)
      m(nbFFSlave*i+idSlave) -= force(i); //put in internal forces for path following

    //to integrate on the master surface we can use the shape function at uvw for each vertex of the master.
    s.clear();
    cE->getConstRefToElementMaster()->getShapeFunctions(uvw[0], uvw[1], uvw[2], suvw);
    for(int i = 0; i < nbFFMaster; ++i){
      s.push_back(TensorialTraits<double>::ValType(suvw[i]));
    }
    /*double sumFF=0.;
    for(int k=0;k<nbFFMaster;k++)
    {
      if(s[k]>0.)
        sumFF-=s[k];
    }*/

    for(int k=0;k<nbFFMaster;k++)
    {
      for(int i=0; i< nbdofpervertex; i++)
          m(nbSlaveKeys  + nbFFMaster*i+ k) += s[k]*force(i);
        //if(s[k]>0.)
        //  m(nbSlaveKeys  + nbFFMaster*i+ k) += s[k]*force(i)/sumFF;
    }
  }

}


void forceNodeOnSurfaceDefoDefoContactTerm::get(contactElement *ele, int npts, IntPt *GP, fullVector<double> &m) const
{
  int nbdofs = _spc->getNumKeys(ele);
  m.resize(nbdofs);
  m.setAll(0.); //here we need to fill slave node 1, x, slave node 2, x,slave node 3, x, ... slave node 1, y etc and then master node 1, x, master node 2, x, etc
  static std::vector<Dof> keys;
  keys.clear();
  _spc->getKeys(ele,keys);
  static fullVector<double> disp;
  disp.resize(nbdofs);
  disp.setAll(0.);
  _ufield->get(keys,disp);  

  get(ele,npts,GP, m, disp); // we proceed like that so we can call the function with disp to get the matrix by perturbation

  //m.print("contact forces");
}

void stiffnessNodeOnSurfaceDefoDefoContactTerm::get(contactElement *ele, int npts, IntPt *GP, fullMatrix<double> &m) const
{
  nodeOnSurfaceContactElement *cele= static_cast<nodeOnSurfaceContactElement*>(ele); 
  const nodesOnSurfaceInContact& nodesInContact=cele->getConstRefToNodesOnSurfaceInContact();
  int dim = ele->getConstRefToElementMaster()->getDim();

  int nbdofs = _spc->getNumKeys(ele);
  m.resize(nbdofs,nbdofs);
  m.setAll(0.); //here we need to fill slave node 1, x, slave node 2, x,slave node 3, x, ... slave node 1, y etc and then master node 1, x, master node 2, x, etc

  int nbdofpervertex = _spc->getNumKeysSlaveNode(ele, 0);
  // Be carefull the stiffness matrix is related to internal forces, so change the sign compared to forces
  static std::vector<Dof> keys;
  keys.clear();
  _spc->getKeys(ele,keys);
  static fullVector<double> disp;
  disp.resize(nbdofs);
  disp.setAll(0.);
  _ufield->get(keys,disp);  

  int nbFFSlave = ele->getConstRefToElementSlave()->getNumShapeFunctions();
  int nbFFMaster = ele->getConstRefToElementMaster()->getNumShapeFunctions();

  int nbSlaveKeys=_spc->getNumKeysSlaveNodes(ele);

  static std::vector<SPoint3> pMaster;
  pMaster.clear();
  getUpdatedMasterPositions(disp, _spc, ele, pMaster);

  static std::vector<TensorialTraits<double>::ValType> s;
  static std::vector<TensorialTraits<double>::GradType> Grads;
  double suvw[256];
  double gradsuvw[256][3];


  for(nodesOnSurfaceInContact::nodesContainer::const_iterator it=nodesInContact.nBegin(); it!=nodesInContact.nEnd(); it++)
  { 
    SVector3 n=(*it).getConstRefToNormal();
    SVector3 t=(*it).getConstRefToTangent1();
    SVector3 b=(*it).getConstRefToTangent2();
    const SPoint3 &uvw=(*it).getConstRefToRedCoordinates();
    SPoint3 proj=(*it).getConstRefToProjection();
    int idSlave=(*it).getSlaveIndex();

    ele->getConstRefToElementMaster()->getShapeFunctions(uvw[0], uvw[1], uvw[2], suvw);
    ele->getConstRefToElementMaster()->getGradShapeFunctions(uvw[0], uvw[1], uvw[2], gradsuvw);
    s.clear();
    Grads.clear();
    for(int i = 0; i < nbFFMaster; ++i){
      s.push_back(TensorialTraits<double>::ValType(suvw[i]));
      Grads.push_back(TensorialTraits<double>::GradType(gradsuvw[i][0], gradsuvw[i][1], gradsuvw[i][2]));
    }


    SPoint3 pSlave;
    getUpdatedPosition(disp, _spc, ele,idSlave,true,pSlave);
    SVector3 projToSlave(pSlave[0]-proj[0],pSlave[1]-proj[1],pSlave[2]-proj[2]);
    double gap=dot(projToSlave,n)/n.norm();
    SVector3 force(n);
    force.normalize();
    force*=_penalty*(-gap); //since gap is <0, this gives the load on the slave node


    static STensor3 dPOnSurfacedUVW, dTdUVW, dBdUVW;
    static std::vector<STensor3> dPOnSurfacedMasterNode, dTdMasterNode, dBdMasterNode; 
    getLocalBasisAtUVW(disp, _spc, ele, pMaster, _domainNormalFor2D, uvw, proj, n, t, b,dPOnSurfacedUVW, dTdUVW, dBdUVW, true,
                          dPOnSurfacedMasterNode, dTdMasterNode, dBdMasterNode,true);  //gives the partial derivatives

    static STensor3 dRdUVW,invDRdUVW; //- d Residu / d uvw and inverse ; see projection for residu definition
    STensorOperation::zero(dRdUVW);
    STensorOperation::unity(invDRdUVW);
    dRdUVW(2,2)=1.;
    if(dim==1) dRdUVW(1,1)=1.;
    for(int i=0;i<dim;i++)
    {
      for(int j=0;j<3;j++)
      {
        dRdUVW(0,i) += -t(j)*dPOnSurfacedUVW(j,i) + projToSlave(j)*dTdUVW(j,i);
        if(dim>1)
          dRdUVW(1,i) += -b(j)*dPOnSurfacedUVW(j,i) + projToSlave(j)*dBdUVW(j,i);
      }
    }
    double det= dRdUVW.determinant();
    if(det==0.) Msg::Error("stiffness evalution impossible because of 0 projection derivative");
    if(dim==1)
    {
      invDRdUVW(0,0)=1./det;
    }
    else if(dim==2)
    {
       invDRdUVW(0,0) = dRdUVW(1,1)/det;
       invDRdUVW(0,1) = -dRdUVW(0,1)/det;
       invDRdUVW(1,1) = dRdUVW(0,0)/det;
       invDRdUVW(1,0) = -dRdUVW(1,0)/det;
    }
    else
       Msg::Error("stiffness evaluation not implemented for master of dim==3");

    static STensor3 mdRdSlaveNode; // - d Residu / d x slave ; see projection for residu definition
    mdRdSlaveNode(0,0)=-t(0);
    mdRdSlaveNode(0,1)=-t(1);
    mdRdSlaveNode(0,2)=-t(2);
    mdRdSlaveNode(1,0)=-b(0);
    mdRdSlaveNode(1,1)=-b(1);
    mdRdSlaveNode(1,2)=-b(2);
    mdRdSlaveNode(2,0)=0.;
    mdRdSlaveNode(2,1)=0.;
    mdRdSlaveNode(2,2)=0.;
    STensor3 dUVWdSlaveNode(invDRdUVW);
    dUVWdSlaveNode*=mdRdSlaveNode;  // partial uvw / partial x slave

    STensor3 trueDTdSlaveNode(dTdUVW);  // dt/d xs = partial t /partial x slave + partial t/partial uvw * partial uvw / partial x slave = partial t/partial uvw * partial uvw / partial x slave
    trueDTdSlaveNode*=dUVWdSlaveNode;
    STensor3 trueDBdSlaveNode(dBdUVW);  // db/d xs = partial b /partial x slave + partial b/partial uvw * partial uvw / partial x slave = partial b/partial uvw * partial uvw / partial x slave
    trueDBdSlaveNode*=dUVWdSlaveNode;
    STensor3 trueDPOnSurfacedSlaveNode(dPOnSurfacedUVW);  // d proj /d x slave = partial proj /partial x slave + partial proj/partial uvw * partial uvw / partial x slave = partial proj/partial uvw * partial uvw / partial x slave
    trueDPOnSurfacedSlaveNode*=dUVWdSlaveNode;
 
    static STensor3 trueDNdSlaveNode;  // dn/dx = dt/dx vec b +t vec db/dx 
    STensorOperation::zero(trueDNdSlaveNode);
    for(int i=0; i<3; i++)
    {
      trueDNdSlaveNode(0,i)= trueDTdSlaveNode(1,i)*b(2)-trueDTdSlaveNode(2,i)*b(1)+t(1)*trueDBdSlaveNode(2,i)-t(2)*trueDBdSlaveNode(1,i);
      trueDNdSlaveNode(1,i)= trueDTdSlaveNode(2,i)*b(0)-trueDTdSlaveNode(0,i)*b(2)+t(2)*trueDBdSlaveNode(0,i)-t(0)*trueDBdSlaveNode(2,i);
      trueDNdSlaveNode(2,i)= trueDTdSlaveNode(0,i)*b(1)-trueDTdSlaveNode(1,i)*b(0)+t(0)*trueDBdSlaveNode(1,i)-t(1)*trueDBdSlaveNode(0,i);
    }
    double nNorm=n.norm();
    STensor3 dNormalizedNdSlaveNode(trueDNdSlaveNode); // d (n/||n||) dx = 1/||n|| dn/dx - n/||n||^3 otimes (n cdot dn/dx)
    dNormalizedNdSlaveNode*=1./nNorm;
    for(int i=0; i<3; i++)
    {
      for(int j=0; j<3; j++)
      {
        for(int l=0; l<3; l++)
        {
          dNormalizedNdSlaveNode(i,j)-= n(i)*n(l)*trueDNdSlaveNode(l,j)/nNorm/nNorm/nNorm;
        }
      }
    }
    SVector3 dGapdSlaveNode(n); // dg d x slave =n/||n||* ( dxslave/dxslave - dproj/dxslave) + (x slave-proj)* d (n/||n||)/dx
    dGapdSlaveNode*=1./nNorm;
    for(int i=0; i<3; i++)
    {
      for(int j=0; j<3; j++)
      {
        dGapdSlaveNode(i) -= trueDPOnSurfacedSlaveNode(j,i)*n(j)/nNorm;
        dGapdSlaveNode(i) += projToSlave(j)*dNormalizedNdSlaveNode(j,i);
      }
    }
    STensor3 dFcdSlaveNode(dNormalizedNdSlaveNode); // dfc / d x slave = -k n/||n|| otimes d gap /d x slave - k gap d (n/||n||) /d x slave
    dFcdSlaveNode*=_penalty*(-gap);
    for(int i=0; i<3; i++)
    {
      for(int j=0; j<3; j++)
      {
        dFcdSlaveNode(i,j)-=_penalty*n(i)*dGapdSlaveNode(j)/nNorm;
      }
    }
    // NOT SURE IN 2D
    for(int i=0; i< nbdofpervertex; i++)
      for(int j=0; j< nbdofpervertex; j++)
        m(nbFFSlave*i+idSlave,nbFFSlave*j+idSlave) -= dFcdSlaveNode(i,j);



    for(int k=0;k<nbFFMaster;k++)
    {
      static STensor3 mdRdMasterNode; //- d Residu / d x master
      STensorOperation::zero(mdRdMasterNode);
      mdRdMasterNode(2,2)=0.;
      if(dim==1) mdRdMasterNode(1,1)=0.;
      for(int i=0;i<dim;i++)
      {
        for(int j=0;j<3;j++)
        {
          mdRdMasterNode(0,i) -= -t(j)*dPOnSurfacedMasterNode[k](j,i) + projToSlave(j)*dTdMasterNode[k](j,i);
          if(dim>1)
            mdRdMasterNode(1,i) -= -b(j)*dPOnSurfacedMasterNode[k](j,i) + projToSlave(j)*dTdMasterNode[k](j,i);
        }
      }      
      STensor3 dUVWdMasterNode(invDRdUVW);
      dUVWdMasterNode*=mdRdMasterNode; // partial uvw / partial x master

      STensor3 trueDTdMasterNode(dTdUVW);  // dt/d x master = partial t /partial x master + partial t/partial uvw * partial uvw / partial x master 
      trueDTdMasterNode*=dUVWdMasterNode;
      trueDTdMasterNode+=dTdMasterNode[k];
      STensor3 trueDBdMasterNode(dBdUVW);  // db/d x master = partial b /partial x master + partial b/partial uvw * partial uvw / partial x master 
      trueDBdMasterNode*=dUVWdMasterNode;
      trueDBdMasterNode+=dBdMasterNode[k];
      STensor3 trueDPOnSurfacedMasterNode(dPOnSurfacedUVW);  // d proj /d x master = partial proj /partial x master + partial proj/partial uvw * partial uvw / partial x master 
      trueDPOnSurfacedMasterNode*=dUVWdMasterNode;
      trueDPOnSurfacedMasterNode+=dPOnSurfacedMasterNode[k];
 
      static STensor3 trueDNdMasterNode;  // dn/dx = dt/dx vec b +t vec db/dx 
      STensorOperation::zero(trueDNdMasterNode);
      for(int i=0; i<3; i++)
      {
        trueDNdMasterNode(0,i)= trueDTdMasterNode(1,i)*b(2)-trueDTdMasterNode(2,i)*b(1)+t(1)*trueDBdMasterNode(2,i)-t(2)*trueDBdMasterNode(1,i);
        trueDNdMasterNode(1,i)= trueDTdMasterNode(2,i)*b(0)-trueDTdMasterNode(0,i)*b(2)+t(2)*trueDBdMasterNode(0,i)-t(0)*trueDBdMasterNode(2,i);
        trueDNdMasterNode(2,i)= trueDTdMasterNode(0,i)*b(1)-trueDTdMasterNode(1,i)*b(0)+t(0)*trueDBdMasterNode(1,i)-t(1)*trueDBdMasterNode(0,i);
      }
      STensor3 dNormalizedNdMasterNode(trueDNdMasterNode); // d (n/||n||) dx = 1/||n|| dn/dx - n/||n||^3 otimes (n cdot dn/dx)
      dNormalizedNdMasterNode*=1./nNorm;
      for(int i=0; i<3; i++)
      {
        for(int j=0; j<3; j++)
        {
          for(int l=0; l<3; l++)
          {
            dNormalizedNdMasterNode(i,j)-= n(i)*n(l)*trueDNdMasterNode(l,j)/nNorm/nNorm/nNorm;
          }
        }
      }
      SVector3 dGapdMasterNode(0.); // dg d x master =n/||n||* ( dxslave/dxmaster - dproj/dxmaster) + (x slave-proj)* d (n/||n||)/dx =n/||n||* (- dproj/dxmaster) + (x slave-proj)* d (n/||n||)/dx
      for(int i=0; i<3; i++)
      {
        for(int j=0; j<3; j++)
        {
          dGapdMasterNode(i) -= trueDPOnSurfacedMasterNode(j,i)*n(j)/nNorm;
          dGapdMasterNode(i) += projToSlave(j)*dNormalizedNdMasterNode(j,i);
        }
      }
      STensor3 dFcdMasterNode(dNormalizedNdMasterNode); // dfc / d x master = -k n/||n|| otimes d gap /d x master - k gap d (n/||n||) /d x master
      dFcdMasterNode*=_penalty*(-gap);
      for(int i=0; i<3; i++)
      {
        for(int j=0; j<3; j++)
        {
          dFcdMasterNode(i,j)-=_penalty*n(i)*dGapdMasterNode(j)/nNorm;
        }
      }
      // NOT SURE IN 2D
      for(int i=0; i< nbdofpervertex; i++)
        for(int j=0; j< nbdofpervertex; j++)
          m(nbFFSlave*i+idSlave,nbSlaveKeys + nbFFMaster*j+ k) -= dFcdMasterNode(i,j);

      //to get the derivative of the force on the master node, we need to derive the ff -> grad ff * (dUVW/d x master +dUVW/d x slave) 
      //               -> a cross-contribution between this master and the slave AND between this master and all the other masters 
      for(int i=0; i< nbdofpervertex; i++)
      {
        for(int j=0; j< nbdofpervertex; j++)
        {
          m(nbSlaveKeys + nbFFMaster*i+ k, nbFFSlave*j+idSlave) += s[k]*dFcdSlaveNode(i,j);
          for(int l=0; l< dim; l++)
          {
            m(nbSlaveKeys + nbFFMaster*i+ k, nbFFSlave*j+idSlave) += force(i)*Grads[k](l)*dUVWdSlaveNode(l,j)  ;
          }
        }
      }
      for(int k2=0;k2<nbFFMaster;k2++)
      {
        for(int i=0; i< nbdofpervertex; i++)
        {
          for(int j=0; j< nbdofpervertex; j++)
          {
            m(nbSlaveKeys + nbFFMaster*i+ k2, nbSlaveKeys +nbFFMaster*j+k) += s[k2]*dFcdMasterNode(i,j);
            for(int l=0; l< dim; l++)
            {
              m(nbSlaveKeys + nbFFMaster*i+ k2, nbSlaveKeys +nbFFMaster*j+k) += force(i)*Grads[k2](l)*dUVWdMasterNode(l,j)  ;
            }
          }
        }


      }
    }
  }
  //m.print("stiffness matrix");
}


void contactTools::getUpdatedPosition(const fullVector<double> &disp, defoDefoContactSpaceBase *spc, contactElement *ele, 
                                                               int idx, bool isSlave, SPoint3 &pos) const
{
  int nbdofs = spc->getNumKeys(ele);
  int nbdofpervertex = spc->getNumKeysSlaveNode(ele, 0);
  int nbFFSlave = ele->getConstRefToElementSlave()->getNumShapeFunctions();
  int nbFFMaster = ele->getConstRefToElementMaster()->getNumShapeFunctions();
  int nbSlaveKeys=spc->getNumKeysSlaveNodes(ele);

  const MVertex* v;
  SPoint3 d;
  if(isSlave)
  {
    v= ele->getConstRefToElementSlave()->getVertex(idx);
    for(int j=0; j<3; j++)
      d[j]= disp(j*nbFFSlave+idx);
  }
  else      
  {
    v= ele->getConstRefToElementMaster()->getVertex(idx);
    for(int j=0; j<3; j++)
      d[j]= disp(nbSlaveKeys+j*nbFFMaster+idx);
  }  
  pos=v->point();
  pos[0] += d[0];
  pos[1] += d[1];
  pos[2] += d[2];

}
void contactTools::computeNormalOfElement2D(const partDomain* d, SVector3& normal) const
{
    MElement *ele=d->element_begin()->second;
    if (ele->getDim() == 2){
      SPoint3 center= ele->barycenter();
      double xyz[3] = {center[0],center[1],center[2]};
      double uvw[3];
      ele->xyz2uvw(xyz,uvw);
      SVector3 phi0[2];
      STensorOperation::zero(phi0[0]);
      STensorOperation::zero(phi0[1]);

      const FunctionSpace<double>* space = dynamic_cast<const FunctionSpace<double>*> (d->getFunctionSpace());
      std::vector<TensorialTraits<double>::GradType> gradUVW;
      space->gradfuvw(ele,uvw[0],uvw[1],uvw[2],gradUVW);
      int nbFF = ele->getNumShapeFunctions();
      for(int k=0;k<nbFF;k++)
      {
        double x = ele->getVertex(k)->x();
        double y = ele->getVertex(k)->y();
        double z = ele->getVertex(k)->z();
        //evaulate displacements at interface
        for(int i=0;i<2;i++)
        {
         phi0[i](0) += gradUVW[k](i)*x;
         phi0[i](1) += gradUVW[k+nbFF](i)*y;
         phi0[i](2) += gradUVW[k+nbFF+nbFF](i)*z;
        }
      }
      normal = crossprod(phi0[0],phi0[1]);
      normal.normalize();
    }
}
bool contactTools::isSlaveInBox(const fullVector<double> &disp, defoDefoContactSpaceBase *spc, contactElement *ele, const SPoint3 &pSlave, double thickContact, const  std::vector<SPoint3> &pMaster) const
{
  static SPoint3 pMasterMin, pMasterMax;
  //Msg::Info("Slave node %d position: %f %f %f",idx,pSlave[0],pSlave[1],pSlave[2]);
  for(int i=0; i<3; i++)
  {
    pMasterMin[i]=1.e16;
    pMasterMax[i]=-1.e16;
  }
  int nbVerticesMaster = ele->getConstRefToElementMaster()->getNumVertices();
  for (int idxM=0; idxM< nbVerticesMaster; idxM++)
  {
    for(int i=0;i<3;i++)
    {
      if(pMaster[idxM][i]<pMasterMin[i]) pMasterMin[i]=pMaster[idxM][i];
      if(pMaster[idxM][i]>pMasterMax[i]) pMasterMax[i]=pMaster[idxM][i];
    }
  }
  bool inBox=true;
  for(int i=0;i<3;i++)
  {
    if(pSlave[i]>pMasterMax[i]+thickContact)
    {
      inBox=false;
      break;
    }
    if(pSlave[i]<pMasterMin[i]-thickContact)
    {
      inBox=false;
      break;
    }
  }
  return inBox;
}

void contactTools::getLocalBasisAtUVW(const fullVector<double> &disp, defoDefoContactSpaceBase *spc, contactElement *ele, const  std::vector<SPoint3> &pMaster, 
                                      const SVector3 &domainNormalFor2D,
                                      const SPoint3 &uvw, SPoint3 &pOnSurface, SVector3& n, SVector3& t, SVector3& b,
                                      STensor3 &dPOnSurfacedUVW, STensor3 &dTdUVW,STensor3 &dBdUVW, bool evaluateDerivativesUVW,
                                      std::vector<STensor3> &dPOnSurfacedMasterNode, std::vector<STensor3> &dTdMasterNode, std::vector<STensor3> &dBdMasterNode, 
                                      bool evaluateDerivativesMasterNode) const
{
  const MElement *master= ele->getConstRefToElementMaster();
  int nbvertexMaster = master->getNumVertices();

  static std::vector<TensorialTraits<double>::ValType> s;
  s.clear();
  static std::vector<TensorialTraits<double>::GradType> Grads;
  Grads.clear();
  double suvw[256];
  double gradsuvw[256][3];
  master->getShapeFunctions(uvw[0], uvw[1], uvw[2], suvw);
  master->getGradShapeFunctions(uvw[0], uvw[1], uvw[2], gradsuvw);
  for(int i = 0; i < nbvertexMaster; ++i){
    s.push_back(TensorialTraits<double>::ValType(suvw[i]));
    Grads.push_back(TensorialTraits<double>::GradType(gradsuvw[i][0], gradsuvw[i][1], gradsuvw[i][2]));
  }
  for(int i=0; i<3; i++)
  {
    pOnSurface[i]=0.;
    t[i] = 0.;
    b[i] = 0.;
    n[i] = 0.;
  }
  for(int k=0;k<nbvertexMaster;k++)
  {
    double x = pMaster[k][0];
    double y = pMaster[k][1];
    double z = pMaster[k][2];
    pOnSurface[0] += s[k]*x;
    pOnSurface[1] += s[k]*y;
    pOnSurface[2] += s[k]*z;
    int indext=0;
    int indexb=1;
    t(0) += Grads[k](indext)*x;
    t(1) += Grads[k](indext)*y;
    t(2) += Grads[k](indext)*z;
    if (master->getDim() == 2)
    {
      b(0) += Grads[k](indexb)*x;
      b(1) += Grads[k](indexb)*y;
      b(2) += Grads[k](indexb)*z;
    }
  }
  if (master->getDim() == 1)
  {
    b=domainNormalFor2D;
  }
  n =  crossprod(t, b);
  //printf("local basis at uvw =[ %e %e %e]: projection = [%e %e %e], n = [%e %e %e], t = [%e %e %e] ; b =[%e %e %e]\n",
  //                       uvw[0], uvw[1], uvw[2], pOnSurface[0], pOnSurface[1], pOnSurface[2], n[0],n[1],n[2],t[0],t[1],t[2],b[0],b[1],b[2]);
  if(evaluateDerivativesUVW or evaluateDerivativesMasterNode)
  {
    STensorOperation::zero(dPOnSurfacedUVW);
    STensorOperation::zero(dTdUVW);
    STensorOperation::zero(dBdUVW);
    static std::vector<TensorialTraits<double>::HessType> GradGrads;
    GradGrads.clear();
    double gradgradsuvw[256][3][3];
    master->getHessShapeFunctions(uvw[0], uvw[1], uvw[2], gradgradsuvw);
    for(int i = 0; i < nbvertexMaster; ++i){
      STensor3 tmp;
      for(int j=0; j<3; j++)
        for(int k=0; k<3; k++)
          tmp(j,k)=gradgradsuvw[i][j][k];
      GradGrads.push_back(TensorialTraits<double>::HessType(tmp));
    }
    for(int k=0;k<nbvertexMaster;k++)
    {
      double x = pMaster[k][0];
      double y = pMaster[k][1];
      double z = pMaster[k][2];
      for(int xi=0; xi<3; xi++)
      {
        dPOnSurfacedUVW(0,xi) += Grads[k][xi]*x;
        dPOnSurfacedUVW(1,xi) += Grads[k][xi]*y;
        dPOnSurfacedUVW(2,xi) += Grads[k][xi]*z;
        int indext=0;
        int indexb=1;
        dTdUVW(0,xi) += GradGrads[k](indext,xi)*x;
        dTdUVW(1,xi) += GradGrads[k](indext,xi)*y;
        dTdUVW(2,xi) += GradGrads[k](indext,xi)*z;
        if (master->getDim() == 2)
        {
          dBdUVW(0,xi) += GradGrads[k](indexb,xi)*x;
          dBdUVW(1,xi) += GradGrads[k](indexb,xi)*y;
          dBdUVW(2,xi) += GradGrads[k](indexb,xi)*z;
        }
      }
    }
    //dPOnSurfacedUVW.print("dPOnSurfacedUVW");
    //dTdUVW.print("dTdUVW");
    //dBdUVW.print("dBdUVW");
    if(evaluateDerivativesMasterNode)
    {
      dPOnSurfacedMasterNode.clear();
      dTdMasterNode.clear();
      dBdMasterNode.clear();
      static STensor3 dPdxm,dTdxm,dBdxm;
      
      for(int k=0;k<nbvertexMaster;k++)
      {
        STensorOperation::unity(dPdxm);
        dPdxm*=s[k];
        dPOnSurfacedMasterNode.push_back(dPdxm);
        int indext=0;
        int indexb=1;

        STensorOperation::unity(dTdxm);
        dTdxm*=Grads[k](indext);
        dTdMasterNode.push_back(dTdxm);
        if (master->getDim() == 2)
        {
          STensorOperation::unity(dBdxm);
          dBdxm*=Grads[k](indexb);
          dBdMasterNode.push_back(dBdxm);
        }
        else if (master->getDim() == 1)
        {
          STensorOperation::zero(dBdxm);
          dBdMasterNode.push_back(dBdxm);
        }
      }
    }
  }
}

bool contactTools::projectSlaveOnMaster(const fullVector<double> &disp, defoDefoContactSpaceBase *spc, contactElement *ele, const  std::vector<SPoint3> &pMaster, 
                                      const SVector3 &domainNormalFor2D, const SPoint3 &pSlave,
                                      SPoint3 &uvw, SPoint3 &pOnSurface, SVector3& n, SVector3& t, SVector3& b) const
{
      int dim = ele->getConstRefToElementMaster()->getDim();
      bool succeed=false;
      uvw[0]=0.;
      uvw[1]=0.;
      uvw[2]=0.;
      STensor3 dPOnSurfacedUVW, dTdUVW, dBdUVW;
      std::vector<STensor3> dPOnSurfacedMasterNode, dTdMasterNode, dBdMasterNode; 
      getLocalBasisAtUVW(disp, spc, ele, pMaster, domainNormalFor2D, uvw, pOnSurface, n, t, b,dPOnSurfacedUVW, dTdUVW, dBdUVW, true,
                          dPOnSurfacedMasterNode, dTdMasterNode, dBdMasterNode,false);
      SVector3 projToSlave(pSlave[0]-pOnSurface[0],pSlave[1]-pOnSurface[1],pSlave[2]-pOnSurface[2]);
      double tol=n.norm()*1.e-8;
      for(int ite=0; ite<1000; ite++)
      {
         // Residu is defined as projToSlave \cdot t and b =0
         double rt=dot(projToSlave,t); 
         double rb=dot(projToSlave,b);
         if(dim==1)
         {
           rb=0.;
         }
         double res=pow(rt/t.norm()*rt/t.norm()+rb/b.norm()*rb/b.norm(),0.5);
         //Msg::Info("Residual of project %e for uvw=[%e %e %e], with slave P(%e,%e,%e) and surface point P(%e,%e,%e)",res,uvw.x(),uvw.y(),uvw.z(),
         //        pSlave[0],pSlave[1],pSlave[2],pOnSurface[0],pOnSurface[1],pOnSurface[2]);

         if(res<tol)
         {
           succeed=true;
           break;
         }
         double du=0.;
         double dv=0.;
         STensor3 dRdUVW;
         STensorOperation::zero(dRdUVW);
         dRdUVW(2,2)=1.;
         if(dim==1) dRdUVW(1,1)=1.;
         for(int i=0;i<dim;i++)
         {
           for(int j=0;j<3;j++)
           {
             dRdUVW(0,i) += -t(j)*dPOnSurfacedUVW(j,i) + projToSlave(j)*dTdUVW(j,i);
             if(dim>1)
               dRdUVW(1,i) += -b(j)*dPOnSurfacedUVW(j,i) + projToSlave(j)*dBdUVW(j,i);
           }
         }
         
         double det= dRdUVW.determinant();
         if(det==0.) Msg::Error("projection impossible because of 0 projection derivative");
         if(dim==1)
         {
           du=-rt/det;
         }
         else if(dim==2)
         {
           du= -(dRdUVW(1,1)*rt-dRdUVW(0,1)*rb)/det;
           dv= -(dRdUVW(0,0)*rb-dRdUVW(1,0)*rb)/det;
         }
         else
           Msg::Error("projection not implemented for master of dim==3");

         uvw[0]+=du;
         uvw[1]+=dv;
         getLocalBasisAtUVW(disp, spc, ele, pMaster, domainNormalFor2D, uvw, pOnSurface, n, t, b,dPOnSurfacedUVW, dTdUVW, dBdUVW, true,
                          dPOnSurfacedMasterNode, dTdMasterNode, dBdMasterNode,false);
         projToSlave(0)=(pSlave[0]-pOnSurface[0]);
         projToSlave(1)=(pSlave[1]-pOnSurface[1]);
         projToSlave(2)=(pSlave[2]-pOnSurface[2]);
      }
      if(!succeed) 
        Msg::Error("forceNodeOnSurfaceDefoDefoContactTerm::detectNodesInContact could not project node");
      return succeed;
}
void contactTools::getUpdatedMasterPositions(const fullVector<double> &disp, defoDefoContactSpaceBase *spc, contactElement *ele, std::vector<SPoint3> &pMaster) const
{
  const MElement *master= ele->getConstRefToElementMaster();
  int nbvertexMaster = master->getNumVertices();
  pMaster.clear();
  for(int idx=0;idx<nbvertexMaster; idx++)
  {
    SPoint3 pM;
    getUpdatedPosition(disp, spc, ele, idx,false,pM);
    pMaster.push_back(pM);

  }
}
bool contactTools::evaluateNormalDirection(const partDomain* domMaster, contactElement *ele) const
{
  const MElement *master= ele->getConstRefToElementMaster();
  int nbvertexMaster = master->getNumVertices();
  //first evaluate reference normal
  SVector3 refN,t,b;
  static std::vector<TensorialTraits<double>::ValType> s;
  s.clear();
  static std::vector<TensorialTraits<double>::GradType> Grads;
  Grads.clear();
  double gradsuvw[256][3];
  SPoint3 centerFace= master->barycenter();
  double xyzFace[3] = {centerFace[0],centerFace[1],centerFace[2]};
  double uvwFace[3];
  master->xyz2uvw(xyzFace,uvwFace);
  master->getGradShapeFunctions(uvwFace[0], uvwFace[1], uvwFace[2], gradsuvw);
  for(int i = 0; i < nbvertexMaster; ++i){
    Grads.push_back(TensorialTraits<double>::GradType(gradsuvw[i][0], gradsuvw[i][1], gradsuvw[i][2]));
  }
  for(int i=0; i<3; i++)
  {
    t[i] = 0.;
    b[i] = 0.;
    refN[i] = 0.;
  }
  if (master->getDim() == 2)
  {
    for(int k=0;k<nbvertexMaster;k++)
    {
      const MVertex* v;
      v= master->getVertex(k);

      double x = v->point().x();
      double y = v->point().y();
      double z = v->point().z();
      t(0) += Grads[k](0)*x;
      t(1) += Grads[k](0)*y;
      t(2) += Grads[k](0)*z;
      b(0) += Grads[k](1)*x;
      b(1) += Grads[k](1)*y;
      b(2) += Grads[k](1)*z;
    }
    refN =  crossprod(t, b);
  }
  else if (master->getDim() == 1)
  {
    for(int k=0;k<nbvertexMaster;k++)
    {
      const MVertex* v;
      v= master->getVertex(k);
      double x = v->point().x();
      double y = v->point().y();
      double z = v->point().z();
      t(0) += Grads[k](0)*x;
      t(1) += Grads[k](0)*y;
      t(2) += Grads[k](0)*z;
    }
    SVector3 domainNormalFor2D;
    computeNormalOfElement2D(domMaster, domainNormalFor2D);
    b=domainNormalFor2D;
    refN =  crossprod(t, b);
  }	
  //then fin the volume element
  elementGroup::elementContainer::const_iterator itdom;
  for(itdom=domMaster->element_begin(); itdom!=domMaster->element_end(); itdom++)
  {
    int nbCommonVertices=0;
    int nbVertexVolEle=itdom->second->getNumVertices();
    for(int i=0; i<nbvertexMaster; i++)
    {
      for(int j=0; j<nbVertexVolEle; j++)
      {
        if(master->getVertex(i)==itdom->second->getVertex(j))
        {
          nbCommonVertices++;
          break;
        }
      }
    }
    if(nbCommonVertices==nbvertexMaster)
      break;
  }
  if(itdom==domMaster->element_end())
    Msg::Error("contactTools::evaluateNormalDirection could not find volume element");
  SPoint3 centerVol(itdom->second->barycenter());
  SVector3 volToFace(centerFace[0]-centerVol[0],centerFace[1]-centerVol[1],centerFace[2]-centerVol[2]);   
  if(dot(volToFace,refN)<0.)
    return false;
  return true;

}

