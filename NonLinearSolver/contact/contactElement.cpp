//
//
// Description: Class to set Contact Element
//
//
// Author:  <Ludovic Noels>, (C) 2019
//
// Copyright: See COPYING file that comes with this distribution
//

#include "contactElement.h"

contactElement::contactElement(int nb, MElement *elementMaster,MElement *elementSlave) : _elementMaster(elementMaster), _elementSlave(elementSlave), _nb(nb)
{

}
contactElement::contactElement(const contactElement &source) : _elementMaster(source._elementMaster), _elementSlave(source._elementSlave), 
                                           _nb(source._nb)
{

}

nodeOnSurfaceContactElement::nodeOnSurfaceContactElement(int nb, MElement *elementMaster,MElement *elementSlave) : contactElement(nb, elementMaster,elementSlave), _nodes()
{

} 
nodeOnSurfaceContactElement::nodeOnSurfaceContactElement(const nodeOnSurfaceContactElement &source): contactElement(source), _nodes(source._nodes),  
                                                                         _normalOrientation(source._normalOrientation)
{

}

