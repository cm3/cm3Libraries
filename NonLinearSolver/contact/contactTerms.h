//
// contact base term
//
// Description: define contact terms
//
//
// Author:  <Gauthier BECKER>, (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef CONTACTTERMS_H_
#define CONTACTTERMS_H_
#include "terms.h"
#include "nodeStiffnessContact.h"
#include "contactFunctionSpace.h"
#include "partDomain.h"

class unknownField;
template<class T1=double> class contactLinearTermBase : public LinearTermBase<T1>{
 protected:
  contactNodeStiffness *_allcontactNode; // need to compute BilinearTerm with more efficiency (compute only for nodes in contact)
 public:
  contactLinearTermBase(){
    _allcontactNode = new contactNodeStiffness();
  }
  contactLinearTermBase(const contactLinearTermBase &source){
    _allcontactNode = source._allcontactNode;
  }
  virtual ~contactLinearTermBase(){delete _allcontactNode;}
  contactNodeStiffness* getContactNode() const {return _allcontactNode;}
  virtual void clearContactNodes(){_allcontactNode->clear();}
//  virtual void get(MElement *ele, int npts, IntPt *GP, fullVector<T1> &v)=0;
};

template <class T2=double>class  contactBilinearTermBase : public BilinearTermBase
{
 protected:
  contactLinearTermBase<T2> *_lterm; // because bilinear terms is compute only for nodes
                                 // in contact. These nodes are in the linear terms associated to bilinearTerm
 public:
  contactBilinearTermBase<T2>(contactLinearTermBase<T2> *lterm) : _lterm(lterm){}
  virtual ~contactBilinearTermBase(){}
//  virtual void get(MElement *ele, int npts, IntPt *GP, fullMatrix<T2> &m) const=0;
  // same for all contact
  contactLinearTermBase<T2>* getLterm() const{return _lterm;}
  contactNodeStiffness * getContactNodes() const{return _lterm->getContactNode();}
  virtual void clearContactNodes(){_lterm->clearContactNodes();}
};

template<class T2=double> class rigidContactLinearTermBase : public contactLinearTermBase<T2>{
 protected:
  const unknownField *_ufield;
  rigidContactSpaceBase *_spc;
  // data for get function (Allocated once)
  mutable SVector3 _lastNormalContact; // allow to use the normal in the 2 get functions not very elegant
  mutable int nbdofs,nbdofsGC, nbvertex,nbcomp,nbdofsEle;
  mutable std::vector<double> disp;
  mutable std::vector<Dof> R;
  mutable double verdisp[6];
  mutable double mvalue[6];
  mutable MVertex *ver;
 public:
  rigidContactLinearTermBase(rigidContactSpaceBase *sp,
                             const unknownField *ufield) : contactLinearTermBase<T2>(),
                                                                 _ufield(ufield), _spc(sp), _lastNormalContact(0.,0.,0.),
                                                                 nbdofs(0), nbdofsGC(0), nbvertex(0), nbcomp(0), nbdofsEle(0)
  {

  }
  rigidContactLinearTermBase(const rigidContactLinearTermBase<T2> &source) : contactLinearTermBase<T2>(source){
    _ufield = source._ufield;
    _spc = source._spc;
  }
  ~rigidContactLinearTermBase(){}
//  virtual void get(MElement *ele, fullVector<double> &m);
  virtual void get(const MVertex *ver, const double disp[6],double mvalue[6]) const=0;
  // same for ALL RIGID CONTACT
  virtual void get(MElement *ele, int npts, IntPt *GP, fullVector<T2> &v) const;
};


template<class T2=double> class defoDefoContactLinearTermBase : public contactLinearTermBase<T2>{
 protected:
  const unknownField *_ufield;
  defoDefoContactSpaceBase *_spc;
  const partDomain* _domSlave;
  const partDomain* _domMaster;
  SVector3 _domainNormalFor2D;

 public:
  defoDefoContactLinearTermBase(defoDefoContactSpaceBase *sp, const unknownField *ufield,
                                const partDomain* domSlave, const partDomain* domMaster) : 
                                                 contactLinearTermBase<T2>(),
                                                 _ufield(ufield), _spc(sp), _domSlave(domSlave), _domMaster(domMaster)
  {
  }
  defoDefoContactLinearTermBase(const defoDefoContactLinearTermBase<T2> &source) : contactLinearTermBase<T2>(source){
    _ufield     = source._ufield;
    _spc        = source._spc;
    _domSlave   = source._domSlave;
    _domMaster   = source._domMaster;
    _domainNormalFor2D=source._domainNormalFor2D;
  }
  virtual void get(MElement *ele, int npts, IntPt *GP, fullVector<T2> &v) const
  {
    Msg::Error("defoDefoContactLinearTermBase::get needs contactElements");
  }
  virtual void get(MElement *cE, int npts, IntPt *GP, std::vector<fullVector<T2> > &vSlave) const
  {
    Msg::Error("defoDefoContactLinearTermBase::get needs to be particularized");
  }  
  virtual void get(contactElement *cE, int npts, IntPt *GP, fullVector<T2> &vSlave) const
  {
    Msg::Error("defoDefoContactLinearTermBase::get needs to be particularized");
  }  
  virtual void get(contactElement *cE, int npts, IntPt *GP, fullVector<T2> &vSlave, const fullVector<T2> &disp) const
  {
    Msg::Error("defoDefoContactLinearTermBase::get needs to be particularized");
  }  
  virtual void computeNormalOfElement2D(const partDomain* d, SVector3& normal) const
  {
    Msg::Error("defoDefoContactLinearTermBase::computeNormalOfElement2D needs to be particularized");
  }  
  virtual void detectNodesInContact(contactElement *cE, const fullVector<double> &disp) const
  {
    Msg::Error("defoDefoContactLinearTermBase::detectNodesInContact needs to be particularized");

  }

};
template <class T2=double> class defoDefoContactBilinearTermBase : public contactBilinearTermBase<T2>
{
 protected:
  const unknownField *_ufield;
  defoDefoContactSpaceBase *_spc;
  const partDomain* _domSlave;
  const partDomain* _domMaster;
  SVector3 _domainNormalFor2D;


 public:
  defoDefoContactBilinearTermBase<T2>(contactLinearTermBase<T2> *lterm) : contactBilinearTermBase<T2>(lterm){}
  virtual ~defoDefoContactBilinearTermBase(){}
//  virtual void get(MElement *ele, int npts, IntPt *GP, fullMatrix<T2> &m) const=0;
  defoDefoContactBilinearTermBase(defoDefoContactSpaceBase *sp, contactLinearTermBase<T2> *lterm, const unknownField *ufield,
                                const partDomain* domSlave, const partDomain* domMaster) : 
                                                 contactBilinearTermBase<T2>(lterm),
                                                 _ufield(ufield), _spc(sp), _domSlave(domSlave), _domMaster(domMaster)
  {
  }
  defoDefoContactBilinearTermBase(const defoDefoContactBilinearTermBase<T2> &source) : contactBilinearTermBase<T2>(source){
    _ufield     = source._ufield;
    _spc        = source._spc;
    _domSlave   = source._domSlave;
    _domMaster   = source._domMaster;
    _domainNormalFor2D=source._domainNormalFor2D;
  }
  virtual void get(MElement *ele, int npts, IntPt *GP, fullMatrix<T2> &v) const
  {
    Msg::Error("defoDefoContactBiinearTermBase::get needs contactElements");
  }
  virtual void get(MElement *ele, int npts, IntPt *GP, std::vector<fullMatrix<T2> > &v) const
  {
    Msg::Error("defoDefoContactBiinearTermBase::get needs contactElements");
  }
  virtual void get(contactElement *cE, int npts, IntPt *GP, fullMatrix<T2> &v) const
  {
    Msg::Error("defoDefoContactBiinearTermBase::get needs to be particularized");
  }
  virtual void computeNormalOfElement2D(const partDomain* d, SVector3& normal) const
  {
    Msg::Error("defoDefoContactLinearTermBase::computeNormalOfElement2D needs to be particularized");
  }  
};

template <class T2=double> class defoDefoContactBilinearTermByPerturbation : public contactBilinearTermBase<T2>
{
 protected:
  const unknownField *_ufield;
  defoDefoContactSpaceBase *_spc;
  const partDomain* _domSlave;
  const partDomain* _domMaster;
  double   _pert;


 public:
  defoDefoContactBilinearTermByPerturbation<T2>(contactLinearTermBase<T2> *lterm) : contactBilinearTermBase<T2>(lterm){}
  virtual ~defoDefoContactBilinearTermByPerturbation(){}
//  virtual void get(MElement *ele, int npts, IntPt *GP, fullMatrix<T2> &m) const=0;
  defoDefoContactBilinearTermByPerturbation(defoDefoContactSpaceBase *sp, contactLinearTermBase<T2> *lterm, const unknownField *ufield,
                                const partDomain* domSlave, const partDomain* domMaster, double pert) : 
                                                 contactBilinearTermBase<T2>(lterm),
                                                 _ufield(ufield), _spc(sp), _domSlave(domSlave), _domMaster(domMaster), _pert(pert)
  {
  }
  defoDefoContactBilinearTermByPerturbation(const defoDefoContactBilinearTermByPerturbation<T2> &source) : contactBilinearTermBase<T2>(source){
    _ufield     = source._ufield;
    _spc        = source._spc;
    _domSlave   = source._domSlave;
    _domMaster   = source._domMaster;
    _pert=source._pert;
  }
  virtual void get(MElement *ele, int npts, IntPt *GP, fullMatrix<T2> &v) const
  {
    Msg::Error("defoDefoContactBiinearTermBase::get needs contactElements");
  }
  virtual void get(MElement *ele, int npts, IntPt *GP, std::vector<fullMatrix<T2> > &v) const
  {
    Msg::Error("defoDefoContactBiinearTermBase::get needs contactElements");
  }
  virtual void get(contactElement *cE, int npts, IntPt *GP, fullMatrix<T2> &v) const;
  virtual void computeNormalOfElement2D(const partDomain* d, SVector3& normal) const
  {
    Msg::Error("defoDefoContactLinearTermBase::computeNormalOfElement2D needs to be particularized");
  }  
  virtual BilinearTermBase* clone () const
  {
    return new defoDefoContactBilinearTermByPerturbation<T2>(*this);
  }};


#endif // CONTACTTERMS_H_
