//
//
// Description: Class to set Contact Element
//
//
// Author:  <Ludovic Noels>, (C) 2019
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef CONTACTELEMENT_H_
#define CONTACTELEMENT_H_

#include "MElement.h"
#include "restartManager.h"

#ifndef SWIG


class contactElement {
  protected:
    MElement *_elementMaster;
    MElement *_elementSlave;
    int      _nb;
  public:
    typedef std::set<contactElement*> groupOfContactElements;
    contactElement(int nb, MElement *elementMaster,MElement *elementSlave); 
    contactElement(const contactElement &cE);
    virtual ~contactElement(){}
    MElement *getRefToElementMaster () {return _elementMaster;}
    const MElement *getConstRefToElementMaster () const {return _elementMaster;}
    MElement *getRefToElementSlave () {return _elementSlave;}
    const MElement *getConstRefToElementSlave () const {return _elementSlave;}
    int getNb() {return _nb;}
    virtual void restart(){}
};

class dataNodeOnSurfaceContact{
 public:
  int _verIndex;
  SVector3 _normal;
  SVector3 _t1;
  SVector3 _t2;
  SPoint3 _projection; //x y z on master surface 
  SPoint3 _redCoordinates; //xi mu zeta on master surface 

 public:
  dataNodeOnSurfaceContact() : _verIndex(0), _normal(0.,0.,0.), _t1(0.,0.,0.), _t2(0.,0.,0.), _redCoordinates(0.,0.,0.), _projection(0.,0.,0.)
  {};
  dataNodeOnSurfaceContact(const int ind, const SVector3 &nor, const SVector3 &t1, const SVector3 &t2, const SPoint3& redCoordinates, const SPoint3& proj) :  
                                               _verIndex(ind), _normal(nor), _t1(t1), _t2(t2), _redCoordinates(redCoordinates), _projection(proj)
  {};
  dataNodeOnSurfaceContact(const dataNodeOnSurfaceContact &source){
    _verIndex = source._verIndex;
    _normal = source._normal;
    _t1 = source._t1;
    _t2 = source._t2;
    _redCoordinates=source._redCoordinates;
    _projection=source._projection;
  }
  dataNodeOnSurfaceContact& operator= (const dataNodeOnSurfaceContact& source){
    _verIndex = source._verIndex;
    _normal = source._normal;
    _t1 = source._t1;
    _t2 = source._t2;
    _redCoordinates=source._redCoordinates;
    _projection=source._projection;
    return *this;
  } 
  virtual ~dataNodeOnSurfaceContact(){};
  virtual dataNodeOnSurfaceContact* clone() const {return new dataNodeOnSurfaceContact(*this);};
  int getSlaveIndex() const {return _verIndex;};
  SVector3 &getRefToNormal(){return _normal; };
  const SVector3 &getConstRefToNormal() const{return _normal; };
  SVector3 &getRefToTangent1(){ return _t1;};
  const SVector3 &getConstRefToTangent1() const{ return _t1;};
  SVector3 &getRefToTangent2(){ return _t2;};
  const SVector3 &getConstRefToTangent2() const { return _t2;};
  SPoint3 &getRefToRedCoordinates() {return _redCoordinates;}; //xi mu zeta on master surface 
  const SPoint3 &getConstRefToRedCoordinates() const {return _redCoordinates;}; //xi mu zeta on master surface 
  SPoint3 &getRefToProjection() {return _projection;}; //xi mu zeta on master surface 
  const SPoint3 &getConstRefToProjection() const {return _projection;}; //xi mu zeta on master surface 

  virtual void restart(){
   restartManager::restart(_verIndex);
   restartManager::restart(_normal);
   restartManager::restart(_t1);
   restartManager::restart(_t2);
   restartManager::restart(_redCoordinates);
   restartManager::restart(_projection);
  };
  virtual void print() const
  {
    Msg::Info("Slave node %d project on P [%e %e %e] at uvw=[%e %e %e]",_verIndex,_projection.x(),_projection.y(),_projection.z(),_redCoordinates.x(),_redCoordinates.y(),_redCoordinates.z());
    _normal.print("Normal: ");
    _t1.print("Tangent u: ");
    _t2.print("Tangent v: ");
  }
};

class nodesOnSurfaceInContact{
 public:
  typedef std::vector<dataNodeOnSurfaceContact> nodesContainer;
 protected:
  nodesContainer _nodesInContact;
 public:
  nodesOnSurfaceInContact(){
    _nodesInContact.clear();
  }
  nodesOnSurfaceInContact(const nodesOnSurfaceInContact &source){
    _nodesInContact = source._nodesInContact;
  }
  virtual ~nodesOnSurfaceInContact()
  {
    //for (nodesContainer::iterator it = _nodesInContact.begin(); it!= _nodesInContact.end(); it++){
    //  if (*it) delete *it;
    //}
    _nodesInContact.clear();
  };
 virtual nodesContainer& getCurrentNodesInContact(){
   return _nodesInContact;
  }
  virtual const nodesContainer& getCurrentNodesInContact() const{
    return _nodesInContact;
  }
  virtual nodesContainer& getPreviousNodesInContact(){
    Msg::Warning("this function is not implemented");
    static nodesContainer tmp;
    return tmp;
  }

  virtual const nodesContainer& getPreviousNodesInContact() const{
    Msg::Warning("this function is not implemented");
    static nodesContainer tmp;
    return tmp;
  }

  virtual void nextStep(){}

  virtual void insert(const dataNodeOnSurfaceContact &data){
   getCurrentNodesInContact().push_back(data);
  }

  virtual void insert(const int i, const SVector3 &normal,  const SVector3 &t1, const SVector3 &t2, const SPoint3 &redCoordinates, const SPoint3 &projection){
    dataNodeOnSurfaceContact data(i,normal,t1,t2,redCoordinates,projection);
    getCurrentNodesInContact().push_back(data);
  }
  virtual void clear(){
    //for (nodesContainer::iterator it = getCurrentNodesInContact().begin(); it!= getCurrentNodesInContact().end(); it++){
    //  if (*it) delete *it;
    //}
    getCurrentNodesInContact().clear();
  }

  nodesContainer::const_iterator nBegin() const{return getCurrentNodesInContact().begin();}
  nodesContainer::const_iterator nEnd() const{return getCurrentNodesInContact().end();}
  virtual void restart(){
   Msg::Info("Before restart");
   print();
   restartManager::restart(_nodesInContact);
   Msg::Info("After restart");
   print();
  }
  virtual void print() const
  {
    for (nodesContainer::const_iterator it = getCurrentNodesInContact().begin(); it!= getCurrentNodesInContact().end(); it++){
     (*it).print();
    }
  }
};


class nodeOnSurfaceContactElement :public contactElement
{
  protected:
    nodesOnSurfaceInContact _nodes;
    bool _normalOrientation; //true if normal is outward false if not

  public:
    nodeOnSurfaceContactElement(int nb, MElement *elementMaster,MElement *elementSlave); 
    nodeOnSurfaceContactElement(const nodeOnSurfaceContactElement &cE);
    virtual ~nodeOnSurfaceContactElement(){}

    virtual nodesOnSurfaceInContact& getRefToNodesOnSurfaceInContact()
    {
      return _nodes;
    }
    virtual const nodesOnSurfaceInContact& getConstRefToNodesOnSurfaceInContact() const
    {
      return _nodes;
    } 
    virtual bool getNormalOrientation()const { return _normalOrientation;}
    virtual void setNormalOrientation(bool o) {_normalOrientation=o;}
    virtual void restart(){
      contactElement::restart();
      _nodes.restart();
      restartManager::restart(_normalOrientation);
    }
};


#endif
#endif
