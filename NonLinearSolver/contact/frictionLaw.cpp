//
//
// Description: friction law
//
//
// Author:  <Van Dung NGUYEN>, (C) 2015
//
// Copyright: See COPYING file that comes with this distribution
//

#include "frictionLaw.h"
#include "nodeStiffnessContactWithFriction.h"
#include "STensorOperations.h"   

CoulombFrictionLaw::CoulombFrictionLaw(const int n, const double A, const double mu,
                        const double kn, const double kt, const bool init):frictionLaw(n,init),_A(A),_mu(mu),
                        _kN(kn),_kT(kt){}

void CoulombFrictionLaw::compute(const dataContactStiffnessNodeWithFriction* prev, dataContactStiffnessNodeWithFriction* curr, const bool stiff) const{
  SVector3& force = curr->_contactForce;
  SVector3& tangentForce = curr->_tangentContactForce;
  STensor3& DforceDg = curr->_DcontactForceDgap;
  STensorOperation::zero(force);
  STensorOperation::zero(tangentForce);

  //
  const SVector3& gap =  curr->_gap;
  const SVector3& normal = curr->_normal;
  double gN = dot(gap,normal);
  const SVector3& gT = curr->_tangentGap;

 // Msg::Info("gN = %e",gN);
  //gT.print("gT");

  SVector3 gT_prev(0.);
  SVector3 tT_prev(0.);

  if (prev != NULL){
    gT_prev += prev->_tangentGap;
    tT_prev += prev->_tangentContactForce;
  }

  // predictor
  double tN = _kN*gN;
  SVector3 tTpr(0.);
  for (int i=0; i<3; i++){
    tTpr(i) = tT_prev(i) + _kT*(gT(i) - gT_prev(i));
  }

  tangentForce += (tTpr);
  // check
  double f = tangentForce.norm() - _A - _mu*fabs(tN);
  //Msg::Info("f = %e",f);
  double Gamma = 0.;
  if (f > 0){
    Gamma = f/(_kT);
    SVector3 tangentPr(tTpr);
    tangentPr.normalize();
    for (int i=0; i<3; i++){
      tangentForce(i) -= _kT*Gamma*tangentPr(i);
    }
  }
 // Msg::Info("tN = %e",tN);
 // tangentForce.print("tangentForce");

  for (int i=0; i<3; i++){
    force(i) = tN*normal(i)+ tangentForce(i);
  }

  if (stiff){
    STensorOperation::zero(DforceDg);

    // suppose DnormalDgap = 0.
    SVector3 DtNDg(normal);
    DtNDg *= _kN;

    STensor3 DtTprDg(0.);
    if (prev != NULL){
      for (int i=0; i<3; i++){
        for (int j=0; j<3; j++){
          if (i == j)
            DtTprDg(i,j) += 1- normal(i)*normal(j);
          else
            DtTprDg(i,j) -= normal(i)*normal(j);
        }
      }
       DtTprDg*=_kT;
    }




    double beta = 1.;
    SVector3 DbetaDg(0.);
    if (f > 0){
      beta = 1.-_kT*Gamma/tTpr.norm();

      SVector3 dtTprNormDg(0.);
      for (int i=0; i<3; i++){
        for (int j=0; j<3; j++){
          dtTprNormDg(i) += tTpr(j)*DtTprDg(j,i)/tTpr.norm();
        }
      }

      SVector3 DkTGammaDg(DtNDg);
      DkTGammaDg *= _mu;
      DkTGammaDg += dtTprNormDg;

      for (int i=0; i<3; i++){
        DbetaDg(i) += -DkTGammaDg(i)/tTpr.norm()+ _kT*Gamma/(tTpr.norm()*tTpr.norm()*tTpr.norm())*dtTprNormDg(i);
      }

    }

    for (int i=0; i<3; i++){
      for (int j=0; j<3; j++){
        DforceDg(i,j) += DtNDg(j)*normal(i);
        DforceDg(i,j) += beta*DtTprDg(i,j);

        DforceDg(i,j) += tTpr(i)*DbetaDg(j);
      }
    }
  }
};
