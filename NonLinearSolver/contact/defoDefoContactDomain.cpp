//
//
// Description: Class to set Defo Defo Contact
//
//
// Author:  <L. Noels>, (C) 2019
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "defoDefoContactDomain.h"
#include "contactTerms.h"
#include "nodeOnSurfaceContactTerms.h"

defoDefoContactDomain::defoDefoContactDomain(const defoDefoContactDomain &source){
    this->onWhat = source.onWhat;
    this->_tag = source._tag;
    this->_dimMaster = source._dimMaster;
    this->_dimSlave = source._dimSlave;
    this->_physMaster = source._physMaster;
    this->_physSlave = source._physSlave;
    this->_vspace = source._vspace;
//    this->_vgroupSlave = source._vgroupSlave; 
//    this->_vgroupMaster = source._vgroupMaster; 
    this->_group = source._group; 
    this->_vquadratureMaster = source._vquadratureMaster;
    this->_vdomMaster = source._vdomMaster;
    this->_vdomSlave = source._vdomSlave;
    this->_filterMaster = source._filterMaster;
    this->_filterSlave = source._filterSlave;
    this->_fSlave = source._fSlave; 
    this->_vterm = source._vterm;
    this->_vmatrix_term = source._vmatrix_term;
    this->_stiffByPerturbation =source._stiffByPerturbation;
    this->_perturbation=source._stiffByPerturbation;
  }
defoDefoContactDomain::defoDefoContactDomain::defoDefoContactDomain(const int tag, const int dimMaster, const int phyMaster,
                            const int dimSlave, const int phySlave, elementFilter *fslave) : 
                     _fSlave(fslave),_tag(tag),_dimMaster(dimMaster),_dimSlave(dimSlave),_physMaster(phyMaster),_physSlave(phySlave),
      onWhat(UNDEF), _stiffByPerturbation(false), _perturbation(1.e-8) 
{

};
void defoDefoContactDomain::nextStep(){
  for(int i=0;i<_vspace.size();i++)
  {
    contactLinearTermBase<double> *lterm = static_cast<contactLinearTermBase<double>*>(_vterm[i]);
    lterm->getContactNode()->nextStep();
  }
};


void defoDefoContactDomain::clear()
{
  for(int i=0;i<_vspace.size();i++)
  {
    delete _vspace[i];
 //   _vgroupSlave[i]->clearAll(); // end problem ??
 //   _vgroupMaster[i]->clearAll(); // end problem ??
    for(contactElement::groupOfContactElements::iterator it=_group[i].begin(); it!=_group[i].end(); it++)
    { 
      delete *it;
    }
    _group[i].clear();
    //delete _group[i];
    delete _vquadratureMaster[i];
    delete _vterm[i];
    delete _vmatrix_term[i];
  }
  _vspace.clear();
  //_vgroupMaster.clear();
  //_vgroupSlave.clear();
  _group.clear();
  _vquadratureMaster.clear();
  _vdomMaster.clear();
  _vterm.clear();
  _vmatrix_term.clear();
}
void defoDefoContactDomain::restart()
{
  for(int i=0;i<_vspace.size();i++)
  {
    for(contactElement::groupOfContactElements::iterator it=_group[i].begin(); it!=_group[i].end(); it++)
    { 
      (*it)->restart();
    }
  }
}

nodeOnSurfaceDefoDefoContactDomain::nodeOnSurfaceDefoDefoContactDomain(const nodeOnSurfaceDefoDefoContactDomain &source) :
                                                          _penalty(source._penalty), _thickness(source._thickness)
{

}

void nodeOnSurfaceDefoDefoContactDomain::clear()
{
  defoDefoContactDomain::clear();
}


void  nodeOnSurfaceDefoDefoContactDomain::initializeTerms(const unknownField *ufield){
  for(int i=0;i<_vterm.size();i++)
  {
    delete _vterm[i];
    delete _vmatrix_term[i];
  }
  _vterm.clear();
  _vmatrix_term.clear();
  for(int i=0;i<_vspace.size();i++)
  {
    defoDefoContactSpaceBase* sp = _vspace[i];
    partDomain* domMaster        = _vdomMaster[i];
    partDomain* domSlave         = _vdomSlave;
    //elementGroup* gSlave      = _vgroupSlave[i];
    //elementGroup* gMaster     = _vgroupMaster[i];
    contactElement::groupOfContactElements &gAll        = _group[i];
    LinearTermBase<double>* _lterm = new forceNodeOnSurfaceDefoDefoContactTerm(sp,ufield,domSlave,domMaster,_thickness,_penalty);
    forceNodeOnSurfaceDefoDefoContactTerm *rlterm = static_cast<forceNodeOnSurfaceDefoDefoContactTerm*>(_lterm);
    const forceNodeOnSurfaceDefoDefoContactTerm *crlterm = static_cast<const forceNodeOnSurfaceDefoDefoContactTerm*>(_lterm);
    BilinearTermBase* _bterm;
    if(!getStiffByPerturbation())
      _bterm = new stiffnessNodeOnSurfaceDefoDefoContactTerm(sp,rlterm,ufield,domSlave,domMaster,_thickness,_penalty);
    else
      _bterm = new defoDefoContactBilinearTermByPerturbation<double>(sp,rlterm,ufield,domSlave,domMaster,getPerturbation());
 
    _vterm.push_back(_lterm);
    _vmatrix_term.push_back(_bterm);
    for(contactElement::groupOfContactElements::iterator itce=gAll.begin(); itce!=gAll.end(); itce++)
    {
       nodeOnSurfaceContactElement *cele=static_cast<nodeOnSurfaceContactElement*>(*itce);
       bool ndir=crlterm->evaluateNormalDirection(domMaster, cele);
       cele->setNormalOrientation(ndir);
    }

  }
}

void nodeOnSurfaceDefoDefoContactDomain::restart()
{
  defoDefoContactDomain::restart();
}

