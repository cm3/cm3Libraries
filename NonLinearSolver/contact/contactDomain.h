//
// contact Domain
//
// Description: Domain to solve contact problem
//
//
// Author:  <Gauthier BECKER>, (C) 2010
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef CONTACTDOMAIN_H_
#define CONTACTDOMAIN_H_
#ifndef SWIG
#include "elementGroup.h"
#include "partDomain.h"
#include "contactTerms.h"
#include "rigidCylinderContactTerms.h"
#include "rigidConeContactTerms.h"
#include "rigidPlaneContactTerms.h"
#include "rigidSphereContactTerms.h"
#include "MVertex.h"
#include "MElement.h"
#include "frictionLaw.h"

template<class T2> class contactBilinearTermBase;
template<class T2> class contactLinearTermBase;
#endif
class contactDomain{
 public:
  enum contact{rigidCylinder, rigidSphere, rigidPlane};
 protected:
  int _phys;
  int _physSlave;
  int _tag;
  double _penalty; // penalty for normal contact force --> penetration
  BilinearTermBase* _bterm;
  BilinearTermBase* _massterm;
  LinearTermBase<double>* _lterm;
  partDomain *_dom;
  contact _contype;
  bool _rigid;
  FunctionSpaceBase *_space;
  QuadratureBase *_integ;
  elementFilter *_filterSlave; // search contact only on a part of slave domain

  bool _withFriction; // true if friction is used
  frictionLaw* _frictionlaw; // Coulomb friction Coefficient
  double _tangentPenalty; // penalty for tangent contact form--> sliding



 public:
  elementGroup *gMaster;
  elementGroup *gSlave;
#ifndef SWIG
  contactDomain() : gMaster(0), gSlave(0), _phys(0), _physSlave(0), _tag(0), _contype(rigidCylinder),
                _dom(0), _penalty(0.), _rigid(true),
                _withFriction(false),_frictionlaw(NULL),_tangentPenalty(0.){}
  contactDomain(const int tag, const int phys, const int physSlave, double pe,
                contact conty,elementFilter *fil=NULL, const bool rigid=false) : _tag(tag), _phys(phys), _physSlave(physSlave),
                                                          _penalty(pe), _contype(conty),
                                                          _rigid(rigid), _space(NULL), _filterSlave(fil),
                                                          _tangentPenalty(0.),_frictionlaw(NULL),_withFriction(false){}
  contactDomain(const contactDomain &source);
  virtual ~contactDomain(){
    if (_frictionlaw) delete _frictionlaw;
  }

  virtual frictionLaw* getFrictionLaw() {return _frictionlaw;};
  virtual const frictionLaw* getFrictionLaw() const {return _frictionlaw;};

  virtual void nextStep();
  virtual bool withFriction() const {return _withFriction;};
  virtual double getTangentPenalty() const {return _tangentPenalty;};
  virtual double getNormalPenalty() const{return _penalty;};

  virtual int getTag() const{return _tag;}
  virtual int getPhys() const{return _phys;}
  virtual int getPhysSlave() const{return _physSlave;}
  virtual double getPenalty() const{return _penalty;}
  virtual contact getContactType() const{return _contype;}
  virtual partDomain* getDomain() const {return _dom;}
  virtual bool isRigid() const {return _rigid;}
  virtual BilinearTermBase* getMassTerm(){return _massterm;}
  virtual BilinearTermBase* getStiffnessTerm(){return _bterm;}
  virtual LinearTermBase<double>* getForceTerm(){return _lterm;}
  virtual FunctionSpaceBase* getSpace() {return _space;}
  virtual const FunctionSpaceBase* getSpace() const {return _space;}
  virtual QuadratureBase* getGaussIntegration() const{return _integ;}
  virtual FilterDof* createFilterDof(const int comp)const {return _dom->createFilterDof(comp);}
  virtual void setDomainAndFunctionSpace(partDomain *dom)=0;
  virtual void initializeTerms(const unknownField *ufield)=0;
  virtual MVertex* getGC()const=0;

//  static void registerBindings(binding *b);
#endif
  virtual void addFrictionLaw(const frictionLaw& flaw);
  virtual void setFriction(const bool flag = true) ;
  virtual void setTangentPenalty(const double val){_tangentPenalty= val;};
  virtual void setNormalPenalty(const double val) {_penalty = val;};
  virtual void setTag(const int t){ _tag =t;}
  virtual void setPhys(const int p){_phys =p;}
  virtual void setPenalty(const double pe){_penalty=pe;}
  virtual void setContactType(const contact ct);
};

class rigidCylinderContactDomain : public contactDomain{
 protected:
  double _length; // length of cylinder;
  double _radius; // outer radius of cylinder;
  double _thick;  // thickness of cylinder;
  double _thickContact; // use for shell (contact with neutral axis of external fiber is !=0)
  double _density; // density of cylinder Not a material law for now
  MVertex *_vergc;  // vertex of gravity center
  SVector3 *_axisDirector; // normalized vector director of cylinder's axis
 public:
 #ifndef SWIG
  rigidCylinderContactDomain(const int tag, const int physMaster, const int physSlave, const int physpt1,
                       const int physpt2,const double penalty,const double h,const double rho,
                       elementFilter *filSlave=NULL);

  rigidCylinderContactDomain(const int tag, const int dimMaster, const int physMaster,
                       const int dimSlave, const int physSlave, const int physpt1,
                       const int physpt2,const double penalty,const double h,const double rho,
                       elementFilter *filSlave=NULL);

  rigidCylinderContactDomain() : contactDomain(), _length(0.), _radius(0.), _thick(0.){
    _contype = rigidCylinder;
    _rigid = true;
    _integ = new QuadratureVoid();
  }
  rigidCylinderContactDomain(const rigidCylinderContactDomain &source);
  virtual ~rigidCylinderContactDomain()
  {
    delete _axisDirector;
   #if defined(HAVE_MPI)
    if(Msg::GetCommSize()>1) delete _filterSlave;
   #endif // HAVE_MPI
  }
  virtual void initializeTerms(const unknownField *ufield);
  SVector3* getAxisDirector() const{return _axisDirector;}
  virtual void setDomainAndFunctionSpace(partDomain *dom)=0;
  virtual MVertex* getGC() const{return _vergc;}
  double getDensity() const{return _density;}
  double getLength() const{return _length;}
  double getRadius() const{return _radius;}
  double getThickness() const{return _thick;}
 #endif // SWIG
};

class rigidConeContactDomain : public contactDomain{
 protected:
  double _baseRadius; // radius at the cone's bottom
  double _thick; // thickness to compute mass
  double _heightCone; // height of cone
  double _heightCylinder; // height of cylinder at the end of cone can be == 0
  double _thickContact; // use for shell (contact with external fiber if = 0)
  double _density; // pass for a material law ??
  MVertex *_vergcCone; // vertex at gravity center
  MVertex *_vergcTot;
  SVector3* _axisDirector; // normalized vector director of cylinder's axis (base --> top)
  double _alpha; // half angle at cone's top in radian
 public:
 #ifndef SWIG
  rigidConeContactDomain(const int tag, const int physMaster, const int physSlave,const int physptBase,
                         const int physptTop, const int physptBot, const double penalty,const double bradius,
                         const double h,const double rho,elementFilter *filSlave=NULL);
  rigidConeContactDomain(const int tag, const int dimMaster, const int physMaster,
                         const int dimSlave, const int physSlave,const int physptBase,
                         const int physptTop, const int physptBot, const double penalty,const double bradius,
                         const double h,const double rho,elementFilter *filSlave=NULL);
  rigidConeContactDomain(const rigidConeContactDomain &source);
  virtual ~rigidConeContactDomain()
  {
   #if defined(HAVE_MPI)
    if(Msg::GetCommSize()>1) delete _filterSlave;
   #endif // HAVE_MPI
  }
  virtual void initializeTerms(const unknownField *ufield);
  SVector3* getAxisDirector() const{return _axisDirector;}
  virtual void setDomainAndFunctionSpace(partDomain *dom)=0;
  virtual MVertex* getGCcone() const{return _vergcCone;}
  virtual MVertex* getGC() const{return _vergcTot;}
  double getDensity() const{return _density;}
  double getHeightCone() const{return _heightCone;}
  double getHeightCylinder()const{return _heightCylinder;}
  double getRadius() const{return _baseRadius;}
  double getThickness() const{return _thick;}
  double getAlpha() const{return _alpha;}
 #endif // SWIG
};

//
//

class rigidPlaneContactDomain : public contactDomain{

  protected:
    MVertex* _vergc; // gravity point-
    SVector3 _normal; // normal to plane, + _vergc--> to contract a fave
    // loading on _vergc+ _normal for rigid plane contact
    double _density; // density of cylinder
    double _thickness; // thickness of plate

  public:
     #ifndef SWIG
    rigidPlaneContactDomain(const int tag, const int dimMaster, const int phyMaster,
                            const int dimSlave, const int phySlave, const int phyPointBase,
                            const double nx, const double ny, const double nz, const double penalty, const double thick,
                            const double rho, elementFilter *filSlave=NULL);

    rigidPlaneContactDomain(const rigidPlaneContactDomain& src): contactDomain(src), _vergc(src._vergc),_normal(src._normal),
                    _density(src._density),_thickness(src._thickness){};
    virtual ~rigidPlaneContactDomain()
     {
        #if defined(HAVE_MPI)
        if(Msg::GetCommSize()>1 and _filterSlave!=NULL) delete _filterSlave;
        #endif // HAVE_MPI
     }


    virtual MVertex* getGC()const {return _vergc;};
    virtual void setDomainAndFunctionSpace(partDomain *dom)=0;
    virtual void initializeTerms(const unknownField *ufield);

    const SVector3& getNormal() const {return _normal;};
    double getDensity() const{return _density;}
    double getThickness() const {return _thickness;};
  #endif // SWIG
};

class rigidSphereContactDomain : public contactDomain{

 protected:
  MVertex *_vergc;  // vertex of gravity center of sphere
  double _radius; // radius of sphere;
//  double _thickContact; // (?) use for shell (contact with neutral axis of external fiber is !=0)
  double _density; // density of sphere (? Not a material law for now ?)

 public:
 #ifndef SWIG
  rigidSphereContactDomain(const int tag, const int dimMaster, const int physMaster,
                           const int dimSlave, const int physSlave, const int physPointBase, const double radius,
                           const double penalty, const double rho, elementFilter *filSlave=NULL);

  rigidSphereContactDomain(const rigidSphereContactDomain& src): contactDomain(src),_vergc(src._vergc),
                           _radius(src._radius),_density(src._density){};
  virtual ~rigidSphereContactDomain(){}
  virtual MVertex* getGC() const{return _vergc;};
  virtual void setDomainAndFunctionSpace(partDomain *dom)=0; // (?)
  virtual void initializeTerms(const unknownField *ufield); // (?)
  
  double getRadius() const{return _radius;}
  double getDensity() const{return _density;}
 #endif // SWIG
};

#endif // CONTACTDOMAIN_H_
