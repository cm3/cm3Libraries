//
// contact stiffness
//
// Description: Class to manage the Dof for which the stiffness must be computed
//
//
// Author:  <Van Dung Nguyen>, (C) 2015
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef CONTACTNODESTIFFNESSWITHFAILURE_H_
#define CONTACTNODESTIFFNESSWITHFAILURE_H_
#include "nodeStiffnessContact.h"
#include "STensor3.h"

class dataContactStiffnessNodeWithFriction : public dataContactStiffnessNode{
 public:
  //
  SVector3 _tangentGap; // tangent to the master surface;
  SVector3 _gap; // _gap = _normalgap*_normal + _tangentgap*_tangent
  SVector3 _dispMaster; // displacement of master rigid surface
  SVector3 _dispSlave; // displacement fo slave nodes

  // to be computed
  SVector3 _contactForce; // contact force
  SVector3 _tangentContactForce; // tengent part
  STensor3 _DcontactForceDgap; // DcontactforceDgap

  dataContactStiffnessNodeWithFriction():dataContactStiffnessNode(), _tangentGap(0.),_gap(0.),_tangentContactForce(0.),
                              _contactForce(0.),_DcontactForceDgap(0.),_dispMaster(0.),_dispSlave(0.){};
  dataContactStiffnessNodeWithFriction(MElement *ele, const int ind, const SVector3 &nor):dataContactStiffnessNode(ele,ind,nor),
                                  _tangentGap(0.),_gap(0.),_tangentContactForce(0.),
                              _contactForce(0.),_DcontactForceDgap(0.),_dispMaster(0.),_dispSlave(0.){}
  dataContactStiffnessNodeWithFriction(MElement *ele, const int ind, const SVector3 &nor, const SVector3& gap, const SVector3& dispM, const SVector3& dispSl):dataContactStiffnessNode(ele,ind,nor),
                _gap(gap), _contactForce(0.),_tangentContactForce(0.),_DcontactForceDgap(0.), _dispMaster(dispM),_dispSlave(dispSl){

    double normalgap = dot(_gap,_normal);
    for (int i=0; i<3; i++){
      _tangentGap(i) = _gap(i) - normalgap*_normal(i);
    }
  }

  dataContactStiffnessNodeWithFriction(const dataContactStiffnessNodeWithFriction &source):dataContactStiffnessNode(source){

    _tangentGap = source._tangentGap;
    _gap = source._gap;

    _dispMaster = source._dispMaster;
    _dispSlave = source._dispSlave;

    _contactForce = source._contactForce;
    _tangentContactForce = source._tangentContactForce;
    _DcontactForceDgap = source._DcontactForceDgap;
  }
  virtual ~dataContactStiffnessNodeWithFriction(){}
  virtual dataContactStiffnessNode* clone() const {return new dataContactStiffnessNodeWithFriction(*this);};
};

class contactNodeStiffnessWithFriction: public contactNodeStiffness{
 protected:
  nodeContainer _nodeInContact2; // nodes in contact in previous time step
  bool _state;

 public:
  contactNodeStiffnessWithFriction(): contactNodeStiffness(),_state(true){};
  contactNodeStiffnessWithFriction(const contactNodeStiffnessWithFriction &source): contactNodeStiffness(source){
    _nodeInContact2 = source._nodeInContact2;
    _state = source._state;
  }
  virtual ~contactNodeStiffnessWithFriction(){
    for (nodeContainer::iterator it = _nodeInContact2.begin(); it!= _nodeInContact2.end(); it++){
      if (*it) delete *it;
    }
    _nodeInContact2.clear();
  };

  virtual nodeContainer& getCurrentContactNodes(){
    if (_state) return _nodeInContact;
    else return _nodeInContact2;
  }

  virtual const nodeContainer& getCurrentContactNodes() const{
    if (_state) return _nodeInContact;
    else return _nodeInContact2;
  }

  virtual nodeContainer& getPreviousContactNodes(){
    if (_state) return _nodeInContact2;
    else return _nodeInContact;
  }

  virtual const nodeContainer& getPreviousContactNodes() const{
    if (_state) return _nodeInContact2;
    else return _nodeInContact;
  }

  virtual void nextStep(){
    if (_state) _state = false;
    else _state = true;
  }

  void insert(MElement *ele,const int i, const SVector3 &normal){
    getCurrentContactNodes().push_back(new dataContactStiffnessNodeWithFriction(ele,i,normal));
    _g->insert(ele);
  }
};
#endif // CONTACTNODESTIFFNESSWITHFAILURE_H_
