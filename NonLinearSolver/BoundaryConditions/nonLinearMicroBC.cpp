//
//
// Description: boundary condition for microscopic BVP in multiscale analyses
//
// Author:  <Van Dung NGUYEN>, (C) 2012
//
// Copyright: See COPYING file that comes with this distribution
//
//

#include "nonLinearMicroBC.h"
#include "nonLinearMechSolver.h"

nonLinearMicroBC::nonLinearMicroBC(const int tag, const int dim, const int addDofPerVertex) : _tag(tag),_dim(dim),Fcur(1.0),
                    Fprev(1.),Gcur(0.0),Gprev(0.),
                    _time(1.0),_order(1),_timeDependency(true),FI(0),G(0.){

	_totalMechaDofNumber = 3; // by default , functionSpace generates 3 DOFs per node
  _totalAdditionalDofNumber = addDofPerVertex;
	_totalConDofNumber = 0;
	_totalNonConDofNumber = 0;
	// if 2D problem is considered, please formulated problem in X-Y plan
	// two fist DOF components (following x,y) are constrained
	// third DOF (following z) is constrained by usual DIRICHLET BC
	for (int i=0; i< _dim; i++){
		_constrainedDofs.insert(i);
		_constrainedMechaDofs.insert(i);
	}
  for (int i=3; i< 3+addDofPerVertex; i++)
  {
    _constrainedDofs.insert(i);
    _constrainedAdditionalDofs.insert(i);
  }
   for(int i=0;i<9;i++)
    _pathFunctionDefoGradientF.push_back(new linearScalarFunction(0,0,1.)); // f = time
   for(int i=0;i<27;i++)
    _pathFunctionDefoGradientG.push_back(new linearScalarFunction(0,0,1.)); // f = time
   for(int i=0;i<3;i++)
    _pathFunctionDefoGradientGradT.push_back(new linearScalarFunction(0,0,1.)); // f = time
   for(int i=0;i<3;i++)
    _pathFunctionDefoGradientGradV.push_back(new linearScalarFunction(0,0,1.)); // f = time
   for(int i=0;i<1;i++)
    _pathFunctionDefoGradientT.push_back(new linearScalarFunction(0,0,1.)); // f = time
};

nonLinearMicroBC::nonLinearMicroBC(const nonLinearMicroBC& src):
 _time(src._time),_g(src._g),_physical(src._physical),_order(src._order),_timeDependency(src._timeDependency),_dim(src._dim),_tag(src._tag),
 _constrainedDofs(src._constrainedDofs),_constrainedMechaDofs(src._constrainedMechaDofs),_constrainedAdditionalDofs(src._constrainedAdditionalDofs),
 _constrainedConDofs(src._constrainedConDofs),
 _constrainedNonConDofs(src._constrainedNonConDofs), 
 _totalMechaDofNumber(src._totalMechaDofNumber),_totalAdditionalDofNumber(src._totalAdditionalDofNumber),_totalConDofNumber(src._totalConDofNumber),
  _totalNonConDofNumber(src._totalNonConDofNumber),Fcur(src.Fcur),Fprev(src.Fprev),Gcur(src.Gcur),Gprev(src.Gprev),
  gradTcur(src.gradTcur),gradTprev(src.gradTprev),Tcur(src.Tcur),Tprev(src.Tprev),Tinit(src.Tinit),gradVcur(src.gradVcur),
  gradVprev(src.gradVprev),FI(src.FI),G(src.G),gradT(src.gradT),T(src.T),gradV(src.gradV)
{

  _pathFunctionDefoGradientF.clear();
   for(int i=0;i<9;i++)
      _pathFunctionDefoGradientF.push_back(src._pathFunctionDefoGradientF[i]->clone());
  _pathFunctionDefoGradientG.clear();
   for(int i=0;i<27;i++)
      _pathFunctionDefoGradientG.push_back(src._pathFunctionDefoGradientG[i]->clone());
  _pathFunctionDefoGradientGradT.clear();
   for(int i=0;i<3;i++)
      _pathFunctionDefoGradientGradT.push_back(src._pathFunctionDefoGradientGradT[i]->clone());
  _pathFunctionDefoGradientGradV.clear();
   for(int i=0;i<3;i++)
      _pathFunctionDefoGradientGradV.push_back(src._pathFunctionDefoGradientGradV[i]->clone());
  _pathFunctionDefoGradientT.clear();
   for(int i=0;i<1;i++)
      _pathFunctionDefoGradientT.push_back(src._pathFunctionDefoGradientT[i]->clone());
}

void nonLinearMicroBC::collecDataFromPrevious(const nonLinearMicroBC& src){
	// only values are transferred
	_constrainedDofs = src._constrainedDofs;
	_constrainedMechaDofs = src._constrainedMechaDofs;
  _constrainedAdditionalDofs = src._constrainedAdditionalDofs;
	_constrainedConDofs = src._constrainedConDofs;
	_constrainedNonConDofs = src._constrainedNonConDofs;
	_totalMechaDofNumber = src._totalMechaDofNumber;
  _totalAdditionalDofNumber = src._totalAdditionalDofNumber;
	_totalConDofNumber = src._totalConDofNumber;
	_totalNonConDofNumber = src._totalNonConDofNumber;
	// previous kinematic variables
	Fprev = src.Fcur;
	Gprev = src.Gcur;
	gradTprev = src.gradTcur;
	Tprev = src.Tcur;
	Tinit = src.Tinit;
  gradVprev = src.gradVcur;
	STensorOperation::zero(FI);
	STensorOperation::zero(G);
	for (int i=0; i< gradT.size(); i++){
		STensorOperation::zero(gradT[i]);
		T[i] = 0.;
	}
	for (int i=0; i< gradV.size(); i++){
		STensorOperation::zero(gradV[i]);
	}
   for(int i=0;i<9;i++)
      _pathFunctionDefoGradientF[i]->operator=(*src._pathFunctionDefoGradientF[i]);
   for(int i=0;i<27;i++)
      _pathFunctionDefoGradientG[i]->operator=(*src._pathFunctionDefoGradientG[i]);
   for(int i=0;i<3;i++)
      _pathFunctionDefoGradientGradT[i]->operator=(*src._pathFunctionDefoGradientGradT[i]);
   for(int i=0;i<3;i++)
      _pathFunctionDefoGradientGradV[i]->operator=(*src._pathFunctionDefoGradientGradV[i]);
   for(int i=0;i<1;i++)
      _pathFunctionDefoGradientT[i]->operator=(*src._pathFunctionDefoGradientT[i]);
};

nonLinearMicroBC::~nonLinearMicroBC(){
	for (int i=0; i< _g.size(); i++){
		if (_g[i]!= NULL) delete _g[i];
	}
   for(int i=0;i<9;i++)
     delete _pathFunctionDefoGradientF[i];
   _pathFunctionDefoGradientF.clear();
   for(int i=0;i<27;i++)
     delete _pathFunctionDefoGradientG[i];
   _pathFunctionDefoGradientG.clear();
   for(int i=0;i<3;i++)
     delete _pathFunctionDefoGradientGradT[i];
   _pathFunctionDefoGradientGradT.clear();
   for(int i=0;i<3;i++)
     delete _pathFunctionDefoGradientGradV[i];
   _pathFunctionDefoGradientGradV.clear();
   for(int i=0;i<1;i++)
     delete _pathFunctionDefoGradientT[i];
   _pathFunctionDefoGradientT.clear();
};


void nonLinearMicroBC::setBCPhysical(const int g1, const int g2, const int g3, const int g4, const int g5, const int g6){
	_physical.clear();
	_g.clear();

	if (g1>0) _physical.push_back(g1);
	if (g2>0) _physical.push_back(g2);
	if (g3>0) _physical.push_back(g3);
	if (g4>0) _physical.push_back(g4);
	if (g5>0) _physical.push_back(g5);
	if (g6>0) _physical.push_back(g6);
	// all
};


void nonLinearMicroBC::setOrder(const int order){ _order =order;};
void nonLinearMicroBC::setDofPartition(const int mechaDofPerNode, const int conDofPerNode, const int nonConDofPerNode, const int addDofPerNode){
	// by default, all extraDof will be constrained in the microsocpic problem
	// mechanical, displacment, constitutive extraDofs, and non-constitutive extraDofs will be constrained
	// other DOFs (e.g. non-local variable) are not constrained by default
	//
	// an arbitrary DOF can be constrained by using two functions
	// clearAllConstraintDofs()
	// insertConstrainDof(comp)

	clearAllConstraintDofs();

	// only mechanical DOF from
	_totalMechaDofNumber = mechaDofPerNode;
	for (int i=0; i< std::min(_dim,_totalMechaDofNumber); i++){
		_constrainedDofs.insert(i);
		_constrainedMechaDofs.insert(i);
	}
  
  _totalAdditionalDofNumber = addDofPerNode;
  for (int i=0; i< _totalAdditionalDofNumber; i++)
  {
    int comp = _totalMechaDofNumber+i;
    _constrainedDofs.insert(comp);
    _constrainedAdditionalDofs.insert(comp);
  }

	// constitive extarDof
	_totalConDofNumber = conDofPerNode;
	for (int i=0; i< _totalConDofNumber; i++){
		int comp = _totalMechaDofNumber+_totalAdditionalDofNumber+i;
		_constrainedConDofs.insert(comp);
		_constrainedDofs.insert(comp);
	}
	// non-constitutive extraDof
	_totalNonConDofNumber = nonConDofPerNode;
	for (int i=0; i< _totalNonConDofNumber; i++){
		int comp = _totalMechaDofNumber+_totalAdditionalDofNumber+_totalConDofNumber+i;
		_constrainedNonConDofs.insert(comp);
		_constrainedDofs.insert(comp);
	}
	// allocated kinematic vector
	if (_totalMechaDofNumber > 0){
		STensorOperation::zero(Fcur); Fcur(0,0) =1.; Fcur(1,1) = 1.; Fcur(2,2) = 1.;
		STensorOperation::zero(Fprev); Fprev(0,0) =1.; Fprev(1,1) = 1.; Fprev(2,2) = 1.;
		STensorOperation::zero(FI);
		STensorOperation::zero(Gcur);
		STensorOperation::zero(Gprev);
		STensorOperation::zero(G);
	}

	gradTcur.clear();
	gradTprev.clear();
	gradT.clear();
	Tcur.clear();
	Tprev.clear();
	T.clear();
	Tinit.clear();
	for (int i=0; i< _totalConDofNumber; i++){
		gradTcur.push_back(SVector3(0.,0.,0.));
		gradTprev.push_back(SVector3(0.,0.,0.));
		gradT.push_back(SVector3(0.,0.,0.));
		Tcur.push_back(0.);
		Tprev.push_back(0.);
		T.push_back(0.);
		Tinit.push_back(0.);
	}

	gradVcur.clear();
	gradVprev.clear();
	gradV.clear();
	for (int i=0; i< _totalNonConDofNumber; i++){
		gradVcur.push_back(SVector3(0.,0.,0.));
		gradVprev.push_back(SVector3(0.,0.,0.));
		gradV.push_back(SVector3(0.,0.,0.));
	}
};


void nonLinearMicroBC::setInitialConstitutiveExtraDofDiffusionValue(const int index, double val) {
	Tinit[index]=val;
	// initialize previous and current values with initial value
	Tcur[index] = val;
	Tprev[index] = val;
};

// for kinematic variable
void nonLinearMicroBC::setDeformationGradient(const int i,const int j, const double val){
	Fcur(i,j) = val;
};

void nonLinearMicroBC::setDeformationGradient(const STensor3& F){
	Fcur = F;
};

void nonLinearMicroBC::setDeformationGradient(const double F00, const double F01, const double F02,
                              const double F10, const double F11, const double F12,
                              const double F20, const double F21, const double F22){
  Fcur(0,0) = F00; Fcur(0,1) = F01; Fcur(0,2) = F02;
	Fcur(1,0) = F10; Fcur(1,1) = F11; Fcur(1,2) = F12;
	Fcur(2,0) = F20; Fcur(2,1) = F21; Fcur(2,2) = F22;

};

void nonLinearMicroBC::setDeformationGradient(const double F00, const double F01,
                              const double F10, const double F11){
  Fcur(0,0) = F00; Fcur(0,1) = F01; Fcur(0,2) = 0.;
	Fcur(1,0) = F10; Fcur(1,1) = F11; Fcur(1,2) = 0.;
	Fcur(2,0) = 0.; Fcur(2,1) = 0.; Fcur(2,2) = 1.;

};

void nonLinearMicroBC::setGradientOfDeformationGradient(const int i, const int j,const int k, const double val){
	Gcur(i,j,k) = val;
};

void nonLinearMicroBC::setGradientOfDeformationGradient(const STensor33& G){
	Gcur = G;
}

void nonLinearMicroBC::setConstitutiveExtraDofDiffusionGradient(const int index, const int i,const double val){
	if (index >= gradTcur.size()) Msg::Error("wrong constitutive extra Dof index");
	gradTcur[index](i) = val;
};

void nonLinearMicroBC::setConstitutiveExtraDofDiffusionGradient(const int index, const SVector3& gradT){
	if (index >= gradTcur.size()) Msg::Error("wrong constitutive extra Dof index");
	gradTcur[index] = gradT;
};

void nonLinearMicroBC::setConstitutiveExtraDofDiffusionGradient(const int index, const double grad0, const double grad1, const double grad2){
	if (index >= gradTcur.size()) Msg::Error("wrong constitutive extra Dof index");
	gradTcur[index](0) = grad0;
	gradTcur[index](1) = grad1;
	gradTcur[index](2) = grad2;
};
void nonLinearMicroBC::setConstitutiveExtraDofDiffusionValue(const int index, double val){
	if (index >= Tcur.size()) Msg::Error("wrong constitutive extra Dof index");
	Tcur[index] = val;
};
void nonLinearMicroBC::setNonConstitutiveExtraDofDiffusionGradient(const int index, const int i,const double val){
	if (index >= gradVcur.size()) Msg::Error("wrong nonconstitutive extra Dof index");
	gradVcur[index](i) = val;
};

void nonLinearMicroBC::setNonConstitutiveExtraDofDiffusionGradient(const int index, const double grad0, const double grad1, const double grad2){
	if (index >= gradVcur.size()) Msg::Error("wrong nonconstitutive extra Dof index");
	gradVcur[index](0) = grad0;
	gradVcur[index](1) = grad1;
	gradVcur[index](2) = grad2;
};

void nonLinearMicroBC::setNonConstitutiveExtraDofDiffusionGradient(const int index, const SVector3& gradV){
	if (index >= gradVcur.size()) Msg::Error("wrong nonconstitutive extra Dof index");
	gradVcur[index] = gradV;
};

void nonLinearMicroBC::clearAllConstraintDofs(){
	_constrainedDofs.clear(); // all Dofs in constraintes
  _constrainedAdditionalDofs.clear();
	_constrainedMechaDofs.clear(); // mechanical part
	_constrainedConDofs.clear(); // constitutive part
	_constrainedNonConDofs.clear();  // non-constitutive part
};
void nonLinearMicroBC::insertConstrainDof(const int dofNum){
	_constrainedDofs.insert(dofNum);
	// check what kind of DOF
	if (dofNum < _totalMechaDofNumber) 
  {
		_constrainedMechaDofs.insert(dofNum);
	}
  else if ((dofNum >= _totalMechaDofNumber ) and (dofNum < _totalMechaDofNumber+_totalAdditionalDofNumber))
  {
    _constrainedAdditionalDofs.insert(dofNum);
  }
	else if ((dofNum>= _totalMechaDofNumber+_totalAdditionalDofNumber) and 
           (dofNum < _totalMechaDofNumber+_totalAdditionalDofNumber+_totalConDofNumber))
  {
		_constrainedConDofs.insert(dofNum);
	}
	else if ((dofNum>= _totalMechaDofNumber+_totalAdditionalDofNumber+_totalConDofNumber) 
       and (dofNum < _totalMechaDofNumber+_totalAdditionalDofNumber+_totalConDofNumber+_totalNonConDofNumber)){
    
    _constrainedNonConDofs.insert(dofNum);
	}
	else
		Msg::Error("component %d cannot be constrained ",dofNum);
};

void nonLinearMicroBC::setPathFunctionDeformationGradient(const scalarFunction& fct){
   for(int i=0;i<9;i++)
   {
     delete _pathFunctionDefoGradientF[i];
     _pathFunctionDefoGradientF[i] = fct.clone();
   }
};
void nonLinearMicroBC::setPathFunctionDeformationGradient(int compi, int compj, const scalarFunction& fct){
   int index=Tensor23::getIndex(compi,compj);
   delete _pathFunctionDefoGradientF[index];
   _pathFunctionDefoGradientF[index] = fct.clone();
};

void nonLinearMicroBC::setPathFunctionGradientOfDeformationGradient(int compi, int compj, int compk, const scalarFunction& fct){
   int index=Tensor33::getIndex(compi,compj,compk);
   delete _pathFunctionDefoGradientG[index];
   _pathFunctionDefoGradientG[index] = fct.clone();
};

nonLinearPeriodicBC::nonLinearPeriodicBC(const int tag, const int dim, const int addDofPerVertex):
                          nonLinearMicroBC(tag,dim,addDofPerVertex),_XaddNewVertices(false),_Xdegree(3),
                        _YaddNewVertices(false),_Ydegree(3),
                        _ZaddNewVertices(false),_Zdegree(3),
                        _wM(nonLinearPeriodicBC::CEM),_forceInterp(false),
                        _usingSerendip(false){ }
nonLinearPeriodicBC::nonLinearPeriodicBC(const nonLinearPeriodicBC& src):nonLinearMicroBC(src),
  _wM(src._wM),_Xdegree(src._Xdegree),_XaddNewVertices(src._XaddNewVertices),_Ydegree(src._Ydegree),
  _YaddNewVertices(src._YaddNewVertices),_Zdegree(src._Zdegree),_ZaddNewVertices(src._ZaddNewVertices),
  _forceInterp(src._forceInterp),_usingSerendip(src._usingSerendip)
{

};

void nonLinearPeriodicBC::setPeriodicBCOptions(const int method, const int seg, const bool add){
	this->setPBCMethod(method);
	this->setPBCPolynomialDegree(0,seg);
	this->setPBCPolynomialDegree(1,seg);
	this->setPBCPolynomialDegree(2,seg);

  this->setAddNewVertices(0,add);
  this->setAddNewVertices(1,add);
  this->setAddNewVertices(2,add);
  
  if (this->getPBCMethod() == nonLinearPeriodicBC::CEM){
    printf("Periodic mesh formulation is used for building the PBC constraint \n");
    return;
  }
  else if (this->getPBCMethod() == nonLinearPeriodicBC::CSIM){
    printf("Cubic Spline Interpolation Method is used for building the PBC constraint\n");
    printf("Segment number X: %d  \n",this->getPBCPolynomialDegree(0));
    printf("Segment number Y: %d  \n",this->getPBCPolynomialDegree(1));
    if (_dim == 3)
      printf("Segment number Z: %d  \n",this->getPBCPolynomialDegree(2));
  }
  else if (this->getPBCMethod() == nonLinearPeriodicBC::LIM){
    printf("Lagrange Interpolation Method is used for building the PBC constraint\n");
    printf("Polynomial order X: %d \n",this->getPBCPolynomialDegree(0));
    printf("Polynomial order Y: %d \n",this->getPBCPolynomialDegree(1));
    if (_dim == 3)
      printf("Polynomial order Z: %d \n",this->getPBCPolynomialDegree(2));
  }
  else if (this->getPBCMethod() == nonLinearPeriodicBC::FE_LIN){
    printf("Linear segment is used for building the PBC constraint\n");
  }
  else if (this->getPBCMethod() == nonLinearPeriodicBC::FE_QUA){
    printf("Quadratic segment is used for building the PBC constraint\n");
  }
  else if (this->getPBCMethod() == nonLinearPeriodicBC::PROJECT){
        printf("Project method is used for building the PBC constraint\n");
  }
  else if (this->getPBCMethod() == nonLinearPeriodicBC::FE_HIGH_ORDER){
    printf("FE_HIGH_ORDER method is used for building the PBC constraint\n");
  }
  else{
    Msg::Error("This PBC method is not implemented yet");
  }

  if (this->addNewVertices(0)) {
    printf("Add new vertices X for periodic boudary condition \n");
  }
  if (this->addNewVertices(1)) {
    printf("Add new vertices Y for periodic boudary condition \n");
  }
  if (this->addNewVertices(2) && _dim == 3) {
    printf("Add new vertices Z for periodic boudary condition \n");
  }

};



nonLinearShiftedPeriodicBC::nonLinearShiftedPeriodicBC(const int tag, const int dim, const int addDofPerVertex): nonLinearPeriodicBC(tag,dim,addDofPerVertex),
     _shiftPBCNormal(0.,0.,0.){};

nonLinearShiftedPeriodicBC::nonLinearShiftedPeriodicBC(const nonLinearShiftedPeriodicBC& src): nonLinearPeriodicBC(src)
      ,_shiftPBCNormal(src._shiftPBCNormal){};


void nonLinearShiftedPeriodicBC::setShiftPBCNormal(const double n1, const double n2, const double n3){
	_shiftPBCNormal[0] = n1;
	_shiftPBCNormal[1] = n2;
	_shiftPBCNormal[2] = n3;
	printf("set Shifted PBC = [%f %f %f]\n",n1,n2,n3);
  if (_shiftPBCNormal.norm()> 0.){
    _shiftPBCNormal.normalize();
  }
};
void nonLinearShiftedPeriodicBC::setShiftPBCNormal(const SVector3& n){
	this->setShiftPBCNormal(n[0],n[1],n[2]);
	printf("set Shifted PBC = [%f %f %f]\n",n[0],n[1],n[2]);
};

void nonLinearShiftedPeriodicBC::updateWithSolver(const nonLinearMechSolver* s){
	_shiftPBCNormal = s->getLostSolutionUniquenssNormal();
	printf("shitfed with lost of solution uniqueness normal : n = [%f %f %f]",_shiftPBCNormal[0],_shiftPBCNormal[1],_shiftPBCNormal[2]);
};


nonLinearGeneralPeriodicBC::nonLinearGeneralPeriodicBC(const int tag, const int dim, const int addDofPerVertex):nonLinearPeriodicBC(tag,dim,addDofPerVertex),
    _pbcNormal(0.){};
nonLinearGeneralPeriodicBC::nonLinearGeneralPeriodicBC(const nonLinearGeneralPeriodicBC& src):nonLinearPeriodicBC(src),
    _pbcNormal(src._pbcNormal){};
    
void nonLinearGeneralPeriodicBC::setPBCNormal(const double n1, const double n2, const double n3){
    _pbcNormal[0] = n1;
    _pbcNormal[1] = n2;
    _pbcNormal[2] = n3;
    if (_pbcNormal.norm() >0){
      _pbcNormal.normalize();
    }
    printf("set PBC normal = [%f %f %f]\n",_pbcNormal[0],_pbcNormal[1],_pbcNormal[2]);
};

nonLinearDisplacementBC::nonLinearDisplacementBC( const int tag, const int dim, const int addDofPerVertex):
                    nonLinearMicroBC(tag,dim,addDofPerVertex){};

nonLinearDisplacementBC::nonLinearDisplacementBC(const nonLinearDisplacementBC& src) : nonLinearMicroBC(src){}

nonLinearMinimalKinematicBC::nonLinearMinimalKinematicBC(const int tag, const int dim, const int addDofPerVertex) :
                 nonLinearMicroBC(tag,dim, addDofPerVertex){
};
nonLinearMinimalKinematicBC::nonLinearMinimalKinematicBC(const nonLinearMinimalKinematicBC& src): nonLinearMicroBC(src){}

nonLinearMixedBC::nonLinearMixedBC(const int tag, const int dim, const int addDofPerVertex) : nonLinearMicroBC(tag,dim,addDofPerVertex),_negativeFactor(1.),
  _pbcMethod(nonLinearPeriodicBC::CEM),_interpolationDegree(0),_addNewVertex(false){}
nonLinearMixedBC::nonLinearMixedBC(const nonLinearMixedBC& src) : nonLinearMicroBC(src),
  _kPhysical(src._kPhysical),_sPhysical(src._sPhysical),_fixVertices(src._fixVertices),
 _additionalSecondOrderStaticPhysical(src._additionalSecondOrderStaticPhysical),_negativeFactor(src._negativeFactor),
 _periodicNegative(src._periodicNegative),_periodicPositive(src._periodicPositive),
 _sPhysicalPositive(src._sPhysicalPositive),_sPhysicalNegative(src._sPhysicalNegative),
 _pbcMethod(src._pbcMethod),_interpolationDegree(src._interpolationDegree),_addNewVertex(src._addNewVertex){}

void nonLinearMixedBC::setPeriodicBCOptions(const int method, const int seg, const bool add){
	_pbcMethod = nonLinearPeriodicBC::whichMethod(method);
  _interpolationDegree = seg;
  _addNewVertex = add;

  if (this->getPBCMethod() == nonLinearPeriodicBC::CEM){
    printf("Periodic mesh formulation is used for building the PBC constraint \n");
  }
  else if (this->getPBCMethod() == nonLinearPeriodicBC::CSIM){
    printf("Cubic Spline Interpolation Method is used for building the PBC constraint\n");
    printf("Segment number: %d  \n",seg);
  }
  else if (this->getPBCMethod() == nonLinearPeriodicBC::LIM){
    printf("Lagrange Interpolation Method is used for building the PBC constraint\n");
    printf("Polynomial order: %d \n",seg);
  }
  else if (this->getPBCMethod() == nonLinearPeriodicBC::FE_LIN){
    printf("Linear segment is used for building the PBC constraint\n");
  }
  else if (this->getPBCMethod() == nonLinearPeriodicBC::FE_QUA){
    printf("Quadratic segment is used for building the PBC constraint\n");
  }
  else if (this->getPBCMethod() == nonLinearPeriodicBC::PROJECT){
        printf("Project method is used for building the PBC constraint\n");
  }
  else if (this->getPBCMethod() == nonLinearPeriodicBC::FE_HIGH_ORDER){
        printf("FE_HIGH_ORDER method is used for building the PBC constraint\n");
  }
  else{
    Msg::Error("This PBC method is not implemented yet");
  }

  if (_addNewVertex) {
    printf("Add new vertices X for periodic boudary condition \n");
  }
};

nonLinearOrthogonalMixedBCDirectionFollowing::nonLinearOrthogonalMixedBCDirectionFollowing(const int tag, const int dim, const int addDofPerVertex):
nonLinearMicroBC(tag,dim,addDofPerVertex){}

nonLinearOrthogonalMixedBCDirectionFollowing::nonLinearOrthogonalMixedBCDirectionFollowing(const nonLinearOrthogonalMixedBCDirectionFollowing& src):
nonLinearMicroBC(src),_KUBCDirection(src._KUBCDirection),_SUBCDirection(src._SUBCDirection){}

void nonLinearOrthogonalMixedBCDirectionFollowing::setKUBCDirection(const SVector3& dir){
_KUBCDirection.push_back(dir);
};
void nonLinearOrthogonalMixedBCDirectionFollowing::setSUBCDirection(const SVector3& dir){
_SUBCDirection.push_back(dir);
};

void nonLinearOrthogonalMixedBCDirectionFollowing::setKUBCDirection(const double nx, const double ny, const double nz){
	SVector3 n(nx,ny,nz);
	n.normalize();
	_KUBCDirection.push_back(n);
};
void nonLinearOrthogonalMixedBCDirectionFollowing::setSUBCDirection(const double nx, const double ny, const double nz){
	SVector3 n(nx,ny,nz);
	n.normalize();
	_SUBCDirection.push_back(n);
};

void nonLinearOrthogonalMixedBCDirectionFollowing::clearDirs(){ _KUBCDirection.clear(); _SUBCDirection.clear();};

