//
//
// Description: boundary condition for microscopic BVP in multiscale analyses
//
// Author:  <Van Dung NGUYEN>, (C) 2012
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef NONLINEARMICROBC_H_
#define NONLINEARMICROBC_H_

#ifndef SWIG
#include "nonLinearBC.h"
#include "highOrderTensor.h"
#include "numericalFunctions.h"
#endif //SWIG
#include "scalarFunction.h"

// periodic BC

class nonLinearMechSolver;

class nonLinearMicroBC{
	
	public:
		enum whichBCType{LDBC =0, PBC=1, MKBC=2, MIXBC=3, OrthogonalDirectionalMixedBC =4,  GeneralPBC=5, ShiftedPBC = 6}; // Linear displacement BC, periodic BC, minimal kinematical BC, mixed BC

	#ifndef SWIG
	protected:
	  // kinematical variables: deformation gradient F, gradient of deformation gradient
		STensor3 Fcur, Fprev;
	  STensor33 Gcur, Gprev;
		
		// constitutive extraDof
		std::vector<SVector3> gradTcur, gradTprev;
		std::vector<double> Tcur, Tprev;
		std::vector<double> Tinit;
		
	  // non-constitutive extraDof
		std::vector<SVector3> gradVcur, gradVprev;
		
		
		// kinematic variables 
	  STensor3 FI; // Fminus I
	  STensor33 G;

	  std::vector<SVector3> gradT; // gradient of the field
	  std::vector<double> T; //uniform field
		
	  std::vector<SVector3> gradV;
    
    std::vector<scalarFunction*> _pathFunctionDefoGradientF;
    std::vector<scalarFunction*> _pathFunctionDefoGradientG;
    std::vector<scalarFunction*> _pathFunctionDefoGradientGradV;
    std::vector<scalarFunction*> _pathFunctionDefoGradientGradT;
    std::vector<scalarFunction*> _pathFunctionDefoGradientT;


	  double _time;
	  std::vector<elementGroup*> _g;	// boundary element groups 1 3 5 - 2 4 6 means 1-2 3-4 5-6 periodicity
		std::vector<int> _physical;				// physical ofr apply  micro BC
		int _order;  // order of homogenization
		bool _timeDependency; // true if depending on time
		int _dim; // dimension 1- node periodicity, 2- line periodicity, 3 -face periodicity
		int _tag; // tag
		
		
		std::set<int> _constrainedDofs; // all Dofs in constraintes
		std::set<int> _constrainedMechaDofs; // mechanical part
    std::set<int> _constrainedAdditionalDofs; // additional part
		std::set<int> _constrainedConDofs; // constitutive part
		std::set<int> _constrainedNonConDofs;  // non-constitutive part
		
		// DOF partition --> depends on functionSpace
		// note that this dof partition does not relate to the constrained DOf defined above
		// these quantities are used to construct global kinematic vector,
		// extraField index, and position of kinematic Variable of the constrained DOF in the global vector
		int _totalMechaDofNumber; //total mecanichal DOFs per vertex
    int _totalAdditionalDofNumber; // total additional dofs per vertex
		int _totalConDofNumber; // total constitutive DOFs per vertex
		int _totalNonConDofNumber; // total non-constitutive DOFs per vertex
		// note that, in functionSpace, all DOFs are created as a vector of
		// 0->_totalMechaDofNumber-1;
    // _totalMechaDofNumber -> _totalMechaDofNumber + _totalAdditionalDofNumber-1
		// _totalMechaDofNumber+_totalAdditionalDofNumber-> _totalMechaDofNumber+_totalAdditionalDofNumber+_totalConDofNumber-1;
		//_totalMechaDofNumber+_totalAdditionalDofNumber+_totalConDofNumber-> _totalMechaDofNumber+_totalAdditionalDofNumber+_totalConDofNumber+_totalNonConDofNumber-1
	#endif //SWIG
public:
		void setOrder(const int order);
		void setBCPhysical(const int g1, const int g2, const int g3, const int g4, const int g5=0, const int g6=0);
		void setDofPartition(const int mechaDofPerNode, const int conDofPerNode, const int nonConDofPerNode, const int addDofPerNode=0);
		void clearAllConstraintDofs();
		void insertConstrainDof(const int dofNum);
    
    void setPathFunctionDeformationGradient(const scalarFunction& fct);
    void setPathFunctionDeformationGradient(int compi, int compj, const scalarFunction& fct);
    void setPathFunctionGradientOfDeformationGradient(int compi, int compj, int compk, const scalarFunction& fct);
		
		// mechanics
		void setDeformationGradient(const int i,const int j, const double val);
    void setDeformationGradient(const double F00, const double F01, const double F02,
                              const double F10, const double F11, const double F12,
                              const double F20, const double F21, const double F22);
    void setDeformationGradient(const double F00, const double F01,
                              const double F10, const double F11);
		void setGradientOfDeformationGradient(const int i, const int j,const int k, const double val);
		
		// constitutive extraDofs
		void setConstitutiveExtraDofDiffusionGradient(const int index, const int i,const double val);
		void setConstitutiveExtraDofDiffusionGradient(const int index, const double grad0, const double grad1, const double grad2);
		void setConstitutiveExtraDofDiffusionValue(const int index, double val);
		void setInitialConstitutiveExtraDofDiffusionValue(const int index, double val);
		
		// non-constitutive extraDofs
		void setNonConstitutiveExtraDofDiffusionGradient(const int index, const int i,const double val);
		void setNonConstitutiveExtraDofDiffusionGradient(const int index, const double grad0, const double grad1, const double grad2);
		
		
	#ifndef SWIG
    nonLinearMicroBC(const int tag, const int dim, const int addDofPerVertex=0);
    nonLinearMicroBC(const nonLinearMicroBC& src);
		
		virtual void updateWithSolver(const nonLinearMechSolver* s){};
		
		virtual void collecDataFromPrevious(const nonLinearMicroBC& src);

    const STensor3& getPreviousDeformationGradient() const {return Fprev;};
    const STensor3& getCurrentDeformationGradient() const{return Fcur;};
    const STensor33& getPreviousDeformationGradientGradient() const {return Gprev;};
    const STensor33& getCurrentDeformationGradientGradient() const{return Gcur;};
		
		void setDeformationGradient(const STensor3& F);
		void setGradientOfDeformationGradient(const STensor33& G);
		void setConstitutiveExtraDofDiffusionGradient(const int index, const SVector3& gradT);
		void setNonConstitutiveExtraDofDiffusionGradient(const int index, const SVector3& gradV);

    virtual nonLinearMicroBC* clone() const = 0; // copy 
		
    virtual ~nonLinearMicroBC();
    virtual nonLinearMicroBC::whichBCType getType() const  = 0;
		
    int getTag() const {return _tag;};
    int getOrder() const {return _order;};
    int getDim() const{return _dim;};
   
		
		int getTotalNumberOfMechanicalDofs() const{return _totalMechaDofNumber;}
    int getTotalNumberOfAdditionalDofs() const{return _totalAdditionalDofNumber;};
		int getTotalNumberOfConDofs() const {return _totalConDofNumber;};
		int getTotalNumberOfNonConDofs() const {return _totalNonConDofNumber;}
		
		int getNumberOfConstrainedMechanicalDofs() const {return _constrainedMechaDofs.size();};
		int getNumberOfConstrainedConstitutiveExtraDofs() const {return _constrainedConDofs.size();};
		int getNumberOfConstrainedNonConstitutiveExtraDofs() const {return _constrainedNonConDofs.size();};
		int getNumberOfConstrainedDofsPerVertex() const{return _constrainedDofs.size();};
		
		// get constrained comp
		const std::set<int>& getConstrainedComps() const{return _constrainedDofs;};
		const std::set<int>& getMechanicsConstrainedComps() const{ return _constrainedMechaDofs;} 
    const std::set<int>& getAdditionalConstrainedComps() const {return _constrainedAdditionalDofs;};
		const std::set<int>& getConstitutiveExtraConstrainedComps() const {return _constrainedConDofs;};
		const std::set<int>& getNonConstitutiveExtraConstrainedComps() const {return _constrainedNonConDofs;};
		
		
    double getInitialConstitutiveExtraDofDiffusionValue(const int index) const {return Tinit[index];};
    int size() const {return _physical.size();};

    const std::vector<elementGroup*>& getBoundaryElementGroup() const {return _g;};
		std::vector<elementGroup*>& getBoundaryElementGroup() {return _g;};
    const std::vector<int>& getBoundaryPhysicals() const{return _physical;};

    const elementGroup* getBCGroup(const int physical) const{
      for (int i=0; i<_physical.size(); i++){
        if (_physical[i] == physical){
          return _g[i];
        }
      }
      Msg::Error("physical is not exist in this boundary group");
    };

    elementGroup* getBCGroup(const int physical){
      for (int i=0; i<_physical.size(); i++){
        if (_physical[i] == physical){
          return _g[i];
        }
      }
      Msg::Error("physical is not exist in this boundary group");
      return NULL;
    };


    void setTime(double t)
    {
       _time = t;
       if (_totalMechaDofNumber > 0)
       {
         FI = Fcur;
         FI -= Fprev;
         if (_timeDependency)
         {
           for(int compi=0;compi<3;compi++)
           {
             for(int compj=0;compj<3;compj++)
             {
               int index=Tensor23::getIndex(compi,compj);
               double ftime = _pathFunctionDefoGradientF[index]->getVal(_time);
               FI(compi,compj)*= ftime;
             }
           }
         }
         FI += Fprev;
         FI(0,0) -= 1.;
         FI(1,1) -= 1.;
         FI(2,2) -= 1.;
	       //FI.print("FI");
         if (getOrder() == 2)
         {
           G = Gcur;
           G -= Gprev;
           if (_timeDependency)
           {
             for(int compi=0;compi<3;compi++)
             {
               for(int compj=0;compj<3;compj++)
               {
                 for(int compk=0;compk<3;compk++)
                 {
                   int index=Tensor33::getIndex(compi,compj,compk);
                   double ftime = _pathFunctionDefoGradientG[index]->getVal(_time);
                   G(compi,compj,compk)*= ftime;
                 }
               }
             }
           }
           G += Gprev;
         }
       }		
       for (int i=0; i< _totalConDofNumber; i++)
       {
         gradT[i] = gradTcur[i];
         gradT[i] -= gradTprev[i];
         if (_timeDependency)
         {
           for(int compi=0;compi<3;compi++)
           {
             double ftime = _pathFunctionDefoGradientGradT[compi]->getVal(_time);
             gradT[i][compi]*= ftime;
           }
         }
         gradT[i] += gradTprev[i];
				 
         T[i] = Tcur[i] - Tprev[i];
         if (_timeDependency)
         {
           double ftime = _pathFunctionDefoGradientT[0]->getVal(_time);
           T[i]*= ftime;
         }
         T[i] += Tprev[i];
       };
       for (int i=0; i< _totalNonConDofNumber; i++){
          gradV[i] = gradVcur[i];
          gradV[i] -= gradVprev[i];
          if (_timeDependency)
          {
            for(int compi=0;compi<3;compi++)
            {
              double ftime = _pathFunctionDefoGradientGradV[compi]->getVal(_time);
              gradV[i][compi]*= ftime;
            }
          }
          gradV[i] += gradVprev[i];
       }
			
    };
		
      void getKinematicalVector(fullVector<double>& kinVec) const{
      kinVec.resize(getNumberOfMacroToMicroKinematicVariables());
			kinVec.setAll(0.);
      int comp = 0;
      int maxComp = 0;
			if (_totalMechaDofNumber > 0){
				for (int i=0; i< 3; i++){
					for (int j=0; j<3; j++){
						comp = maxComp+ Tensor23::getIndex(i,j);
						kinVec(comp) = FI(i,j);
					}
				}
				maxComp += 9;

				if (getOrder() == 2){
					for (int i=0; i< 3; i++){
						for (int j=0; j<3; j++){
							for (int k=0; k<3; k++){
								comp = maxComp + Tensor33::getIndex(i,j,k);
								kinVec(comp) =G(i,j,k);
							}
						}
					}
					maxComp += 27;
				}			
			}

      for (int index=0; index< _totalConDofNumber; index++){
        for (int i=0; i<3; i++){
          comp = maxComp + Tensor13::getIndex(i);
          kinVec(comp) = gradT[index](i);
        }
        maxComp += 3;

        comp = maxComp + Tensor11::getIndex();
        kinVec(comp) = T[index];
        maxComp += 1;
      };
			
			for (int index=0; index< _totalNonConDofNumber; index++){
        for (int i=0; i<3; i++){
          comp = maxComp + Tensor13::getIndex(i);
          kinVec(comp) = gradV[index](i);
        }
        maxComp += 3;
			}
    }
    // general function 
    void getBasedKinematicMatrix(const std::vector<int>& component, const SVector3& L, const STensor3& XX, fullMatrix<double>& S, const bool pointBased) const {
      int numKinVar = this->getNumberOfMacroToMicroKinematicVariables();
      int ndofs = component.size();
      S.resize(ndofs,numKinVar,true);
      
      // important to determine the postition of kinematic variable associaetd with current dof in
      // global macroscopic kinematic vector
      int mechanicsNdofs = this->getTotalNumberOfMechanicalDofs();
      int addNDof = this->getTotalNumberOfAdditionalDofs();
      int conExtraNDofs  = this->getTotalNumberOfConDofs();
      int nonConExtraNDofs = this->getTotalNumberOfNonConDofs();
      
      int numMechaKinVar=  this->getNumberOfMechanicalKinematicVariables();
      int numConKinVar = this->getNumberOfConstitutiveExtraDofKinematicVariables();
      int numNonConKinVar = this->getNumberOfNonConstitutiveExtraDofKinematicVariables();
      
      const std::set<int>& mechComps = this->getMechanicsConstrainedComps();
      const std::set<int>& addComps = this->getAdditionalConstrainedComps();
      const std::set<int>& conComps = this->getConstitutiveExtraConstrainedComps();
      const std::set<int>& nonConComps = this->getNonConstitutiveExtraConstrainedComps();
      
      fullMatrix<double> Smechanics;
      getMatrixFromVector(L,Smechanics);
      
      fullMatrix<double> Tmechanics;
      if (this->getOrder() == 2){
        getMatrixFromTensor3(XX,Tmechanics);
      }
      
      for (int i=0; i< ndofs; i++){
        int comp = component[i];
        // mechanics Dofs --> 9 or 9+27 kin variables
        if (mechComps.find(comp) != mechComps.end()){
          for (int j=0; j< 9; j++){
            S(i,j) = Smechanics(comp,j);
          }
          if (this->getOrder() == 2){
            for (int j=0; j<27; j++){
              S(i,9+j) = Tmechanics(comp,j);
            }
          }
        }
        else if (addComps.find(comp) != addComps.end())
        {
          Msg::Info("constrain additional comp %d",comp);
        }
        else if (conComps.find(comp) != conComps.end()){
          // constitutive dofs -_> _conExtraNDofs*4 kin variables
          int extraDofIndex = comp- (mechanicsNdofs+addNDof);
          for (int j=0; j<3; j++){
            S(i,numMechaKinVar+j+extraDofIndex*4) = L(j);
          }
          
          if (pointBased){
            S(i,numMechaKinVar+3+extraDofIndex*4) = 1.; //for direct constraint
          }
        }
        else if (nonConComps.find(comp) != nonConComps.end()){
          // non constitutive dofs --> _nonConExtraNDofs*3 kin variables
          int extraDofIndex = comp- (mechanicsNdofs+addNDof+conExtraNDofs);
          for (int j=0; j<3; j++){
            S(i,numMechaKinVar+numConKinVar+j+extraDofIndex*3) = L(j);
          }
        }
        else{
          Msg::Error("This dof type %d is not defined in constraintElement::getBasedKinematicMatrix",comp);
          auto printSet=[](std::string name, const std::set<int>& vals)
          {
            printf("%s",name.c_str());
            for (std::set<int>::const_iterator itF = vals.begin(); itF != vals.end(); itF++)
            {
              printf(" %d",*itF);
            }
            printf("\n");
          };
          printSet("mechComps",mechComps);
          printSet("addComps",addComps);
          printSet("conComps",conComps);
          printSet("nonConComps",nonConComps);
        }
        
      };
    };
    
    // from [dofMeca	]   [FmI*L						 	]
    //			[dofCon 	] = [Field+ gradField*L	] --> dof = S*kinVar
    //			[dofNonCon]   [gradField*L				]
    void getPointKinematicMatrix(const std::vector<int>& component, const SVector3& L, const STensor3& XX, fullMatrix<double>& S) const{
      getBasedKinematicMatrix(component,L,XX,S,true);
    };
    // from [dofMecaPos		-		dofMecaNeg	]   [FmI*L						 	]
    //			[dofConPos 		-  	dofConNeg		] = [Field+ gradField*L	] --> C*dof = S*kinVar
    //			[dofNonConPos - 	dofNonConNeg]   [gradField*L				]
    void getPBCKinematicMatrix(const std::vector<int>& component, const SVector3& L, const STensor3& XX, fullMatrix<double>& S) const{
      getBasedKinematicMatrix(component, L,XX,S,false);
    };


    int getNumberOfMacroToMicroKinematicVariables() const{
      // number of varibles to downscale
      int num = 9; // strain
      if (getOrder()==2){
        num += 27; // higher-order strain
      }
			
			num += (4*_totalConDofNumber + 3*_totalNonConDofNumber);
    
      return num;
    };
		
		int getNumberOfMechanicalKinematicVariables() const{ 
			if (getOrder() == 1) return 9;
			else if (getOrder() == 2) return 9+27;
			else{Msg::Error("Order %d is not defined",getOrder());};
      return 0.;
		}
		int getNumberOfConstitutiveExtraDofKinematicVariables() const{ 
			return _totalConDofNumber*4;
		}
		int getNumberOfNonConstitutiveExtraDofKinematicVariables() const{
			return _totalNonConDofNumber*3;
		}

    double getTime() const {return _time;}
    bool isTimeDependency() const{ return _timeDependency;};
		
		// direct access to kinematic variable for tangent computation based  on perturbation
    void setFirstOrderKinematicalVariable(const STensor3& F){
      FI = F;
    }
    void setSecondOrderKinematicalVariable(const STensor33& G_){
      G = G_;
    }
    void setConstitutiveExtraDofDiffusionKinematicalVariable(const int index, const SVector3& grad){
			if (index >= gradT.size()) Msg::Error("wrong constitutive extra Dof index");
      gradT[index] = grad;
    }
    void setConstitutiveExtraDofDiffusionConstantVariable(const int index, const double& T_){
			if (index >= gradT.size()) Msg::Error("wrong constitutive extra Dof index");
      T[index] = T_;
    }
		void setNonConstitutiveExtraDofDiffusionKinematicalVariable(const int index, const SVector3& grad){
			if (index >= gradV.size()) Msg::Error("wrong non-constitutive extra Dof index");
      gradV[index] = grad;
    }

    const STensor3& getFirstOrderKinematicalVariable() const{ return FI;};
    const STensor33& getSecondOrderKinematicalVariable() const{ return G;};
    const SVector3& getConstitutiveExtraDofDiffusionKinematicalVariable(const int index) const{return gradT[index];};
    double getConstitutiveExtraDofDiffusionConstantVariable(const int index) const {return T[index];};
		const SVector3& getNonConstitutiveExtraDofDiffusionKinematicalVariable(const int index) const{return gradV[index];};

    void nextStep(){
			_time = 0;
			if (_totalMechaDofNumber > 0){
				Fprev = Fcur;
				if (getOrder() == 2){
					Gprev = Gcur;
				}				
			}
      
			for (int index =0; index < _totalConDofNumber; index++){
				gradTprev[index] = gradTcur[index];
				Tprev[index] = Tcur[index];
			};
			
			for (int index =0; index < _totalNonConDofNumber; index++){
				gradVprev[index] = gradVcur[index];
			}
    }

    bool notSameState(const double RelTol, const double AbsTol) const {
			double f = 0.;
			if (_g.size()>0){
				const elementGroup* g0 = _g[0];
				if (g0->size()>0){
					MElement* ele = (g0->begin()->second);
					int dim = ele->getDim();
					double volume = ele->getVolume();
					if (volume <= 0)
						Msg::Error("Error in nonLinearMicroBC::kinematicalResidual");

					if (dim ==1) f = 1./volume;
					else f = 1./sqrt(volume);
				}
			}
				
			if (_constrainedMechaDofs.size() > 0){
				STensor3 diff(Fcur);
				diff-= Fprev;
				double absolue = sqrt(dot(diff,diff));
				double valref = sqrt(dot(Fcur, Fcur)+dot(Fprev,Fprev));
				if ((absolue > AbsTol) and (valref > AbsTol)){
					if ((absolue/valref) > RelTol) return true;
				}

				if (_order==2){
					STensor33 diff33(Gcur);
					diff33 -= Gprev;
					absolue = sqrt(dot(diff33,diff33));
					valref = f+sqrt(dot(Gcur,Gcur)+ dot(Gprev,Gprev));
					if ((absolue > AbsTol) and (valref > AbsTol)){
						if ((absolue/valref) > RelTol) return true;
					}
				}
			}
			
			for (std::set<int>::const_iterator itcomp = _constrainedConDofs.begin(); itcomp != _constrainedConDofs.end(); itcomp++){
				int index = (*itcomp) - _totalMechaDofNumber;
				SVector3 diff3(gradTcur[index]);
				diff3 -= gradTprev[index];
				double absolue = sqrt(dot(diff3,diff3));
				double valref = f*Tinit[index]+sqrt(dot(gradTcur[index],gradTcur[index])+ dot(gradTprev[index],gradTprev[index]));
				if ((absolue > AbsTol) and (valref > AbsTol)){
					if ((absolue/valref) > RelTol) return true;
				}
				
				absolue = fabs(Tcur[index]-Tprev[index]);
				valref = Tinit[index];
				if ((absolue > AbsTol) and (valref > AbsTol)){
					if ((absolue/valref) > RelTol) return true;
				} 
			}
			
			for (std::set<int>::const_iterator itcomp = _constrainedNonConDofs.begin(); itcomp != _constrainedNonConDofs.end(); itcomp++){
				int index = (*itcomp) - _totalMechaDofNumber-_totalConDofNumber;
				SVector3 diff3(gradVcur[index]);
				diff3 -= gradVprev[index];
				double absolue = sqrt(dot(diff3,diff3));
				double valref = sqrt(dot(gradVcur[index],gradVcur[index])+ dot(gradVprev[index],gradVprev[index]));
				if ((absolue > AbsTol) and (valref > AbsTol)){
					if ((absolue/valref) > RelTol) return true;
				}
			}
      return false;
    }
		#endif //SWIG

};

class nonLinearPeriodicBC: public nonLinearMicroBC{
	
   public:
      enum whichMethod{CEM=0, LIM=1, CSIM=2, FE_LIN=3, FE_QUA=4, PROJECT=5, FE_HIGH_ORDER=6}; // constraint elimination, lagrange interpolation, cubic spline interpolation --> for constructing linear constraint

   protected:
     whichMethod _wM;
     int _Xdegree;		// for lagrange and spline interpolation
     bool _XaddNewVertices; // add new nodes for formultation


     int _Ydegree;		// for lagrange and spline interpolation
     bool _YaddNewVertices; // add new nodes for formultation

     int _Zdegree;		// for lagrange and spline interpolation
     bool _ZaddNewVertices; // add new nodes for formultation

     bool _forceInterp; // true if forcing setting value
                        // false if checking with existing vertex
     bool _usingSerendip; // in case of FE_HIGH_ORDER
                      // true if not using internal nodes
			
   public:
     nonLinearPeriodicBC(const int tag, const int dim, const int addDofPerVertex=0);
    
    #ifndef SWIG
		nonLinearPeriodicBC(const nonLinearPeriodicBC& src);
		virtual ~nonLinearPeriodicBC(){};

     virtual nonLinearMicroBC::whichBCType getType() const  {return nonLinearMicroBC::PBC;};
		 virtual nonLinearMicroBC* clone() const {return new nonLinearPeriodicBC(*this);};
    

		int getMaxDegree() const{
      int xy = std::max(_Xdegree,_Ydegree);
      if (this->getDim() == 2) return xy;
      else return std::max(_Zdegree,xy);
		};

		bool isForcedInterpolationDegree() const{
		  return _forceInterp;
		}

		bool addNewVertices(const int i) const { // add new Dof flag
      if (i ==0) return _XaddNewVertices;
      else if (i==1) return _YaddNewVertices;
      else if (i==2) return _ZaddNewVertices;
      else Msg::Error("direction must be correctly defined addNewVertices(i)");
      return 0;
    };

    whichMethod getPBCMethod() const{
			return _wM;
		};

    int getPBCPolynomialDegree(const int i) const{
      if (i==0) return _Xdegree;
      else if (i==1) return _Ydegree;
      else if (i==2) return _Zdegree;
      else Msg::Error("direction must be correctly defined getPBCPolynomialDegree(i)");
      return 0;
    }
    bool usingSerendipFlag() const {return _usingSerendip;};
    #endif //SWIG
		
    void setAddNewVertices(const int i, const bool add = true) {
      if (i==0) _XaddNewVertices= add;
      else if (i==1) _YaddNewVertices = add;
      else if (i==2) _ZaddNewVertices = add;
      else Msg::Error("direction must be correctly defined setAddNewVertices(i)");
    };

    	  
    void forceInterpolationDegree(const bool flag){
      _forceInterp = flag;
		};

    void setPBCPolynomialDegree(const int i, const int d){
      if (i==0) _Xdegree = d;
      else if (i==1) _Ydegree = d;
      else if (i==2) _Zdegree = d;
      else Msg::Error("direction must be correctly defined setPBCPolynomialDegree(i)");
    }

		void setPBCMethod(const int i){
      _wM = whichMethod(i);
		}
    void usingSerendip(const bool fl){_usingSerendip=fl;};
		void setPeriodicBCOptions(const int method, const int seg = 5, const bool addvertex = 0);
		
};

class nonLinearShiftedPeriodicBC : public nonLinearPeriodicBC{
  protected:
    #ifndef SWIG
  // for shiftedBC
		 SVector3 _shiftPBCNormal; // by a plane whose normal is _shiftPBCNormal
    #endif //SWIG
    
  public:
    nonLinearShiftedPeriodicBC(const int tag, const int dim, const int addDofPerVertex=0);
    #ifndef SWIG
    nonLinearShiftedPeriodicBC(const nonLinearShiftedPeriodicBC& src);
		virtual ~nonLinearShiftedPeriodicBC(){};

    virtual nonLinearMicroBC::whichBCType getType() const  {return nonLinearMicroBC::ShiftedPBC;};
    virtual nonLinearMicroBC* clone() const {return new nonLinearShiftedPeriodicBC(*this);};
    
    const SVector3& getShiftPBCNormal() const {return _shiftPBCNormal;};
    void setShiftPBCNormal(const SVector3& n);
		
		virtual void updateWithSolver(const nonLinearMechSolver* s);
    #endif
		void setShiftPBCNormal(const double n1, const double n2, const double n3);
};

class nonLinearGeneralPeriodicBC : public nonLinearPeriodicBC{
  protected:
    #ifndef SWIG
    SVector3 _pbcNormal; // unit normal of PBC plane
    #endif //SWIG
  public:
    nonLinearGeneralPeriodicBC(const int tag, const int dim, const int addDofPerVertex=0);
    #ifndef SWIG
    nonLinearGeneralPeriodicBC(const nonLinearGeneralPeriodicBC& src);
		virtual ~nonLinearGeneralPeriodicBC(){};
    virtual nonLinearMicroBC::whichBCType getType() const  {return nonLinearMicroBC::GeneralPBC;};
    virtual nonLinearMicroBC* clone() const {return new nonLinearGeneralPeriodicBC(*this);};
    
    const SVector3& getPBCNormal() const {return _pbcNormal;};
    void setPBCNormal(const SVector3& n) {_pbcNormal = n;}
    #endif //SWIG
    void setPBCNormal(const double n1, const double n2, const double n3);
};


class nonLinearDisplacementBC : public nonLinearMicroBC{

   public:
     nonLinearDisplacementBC( const int tag, const int dim, const int addDofPerVertex=0);
     #ifndef SWIG
     nonLinearDisplacementBC(const nonLinearDisplacementBC& src);
     virtual ~nonLinearDisplacementBC(){}
     virtual nonLinearMicroBC::whichBCType getType() const{
         return nonLinearMicroBC::LDBC;
     };
     virtual nonLinearMicroBC* clone() const {return new nonLinearDisplacementBC(*this);};
    #endif //SWIG
};

class nonLinearMinimalKinematicBC : public nonLinearMicroBC{
  public:
    nonLinearMinimalKinematicBC(const int tag, const int dim, const int addDofPerVertex=0);
    #ifndef SWIG
    nonLinearMinimalKinematicBC(const nonLinearMinimalKinematicBC& src);
    virtual ~nonLinearMinimalKinematicBC(){}
		virtual nonLinearMicroBC::whichBCType getType() const{
			return nonLinearMicroBC::MKBC;
		};
    virtual nonLinearMicroBC* clone() const {return new nonLinearMinimalKinematicBC(*this);};
    #endif //SWIG
};

class additionalSecondOrderElement {
  public:
    int physical;
    int comp;
    int direction;
    additionalSecondOrderElement(const int p, const int c, const int d):physical(p),comp(c),direction(d){}
    #ifndef SWIG
    additionalSecondOrderElement(const additionalSecondOrderElement& src):physical(src.physical),comp(src.comp),direction(src.direction){}
    virtual ~additionalSecondOrderElement(){}
    #endif // SWIG
};

class nonLinearMixedBC : public nonLinearMicroBC{
  protected:
    std::vector<std::pair<int,int> > _kPhysical; // null fluctuation physical + component
    std::vector<std::pair<int,int> > _sPhysical; // minimal BC physical + component
    std::vector<std::pair<int,int> > _fixVertices;
    
    std::vector<std::pair<int,int> > _sPhysicalPositive; // periodic avergate phys+comp
    std::vector<std::pair<int,int> > _sPhysicalNegative; // periodic avergate phys+comp
    
    std::vector<std::pair<int,int> > _periodicPositive; // periodic phys + comp
		std::vector<std::pair<int,int> > _periodicNegative; // periodic phys + comp 
		double _negativeFactor;
    nonLinearPeriodicBC::whichMethod _pbcMethod;
    int _interpolationDegree;
    bool _addNewVertex;

    std::vector<additionalSecondOrderElement> _additionalSecondOrderStaticPhysical;

  public:
    nonLinearMixedBC(const int tag, const int dim, const int addDofPerVertex=0);
    #ifndef SWIG
    nonLinearMixedBC(const nonLinearMixedBC& src);
    virtual ~nonLinearMixedBC(){};

    virtual nonLinearMicroBC::whichBCType getType() const{
			return nonLinearMicroBC::MIXBC;
		};
	  const std::vector<std::pair<int,int> >& getKinematicPhysical() const {return _kPhysical;}
		const std::vector<std::pair<int,int> >& getStaticPhysical() const {return _sPhysical;}
		const std::vector<std::pair<int,int> >& getFixVertices() const {return _fixVertices;};
		
    const std::vector<std::pair<int,int> >& getPositiveStaticPhysical() const {return _sPhysicalPositive;}
		const std::vector<std::pair<int,int> >& getNegativeStaticPhysical() const {return _sPhysicalNegative;}
    
    const std::vector<std::pair<int,int> >& getPositivePhysical() const {return _periodicPositive;}
		const std::vector<std::pair<int,int> >& getNegativePhysical() const {return _periodicNegative;}
		const double& getNegativeFactor() const {return _negativeFactor;};
    
    nonLinearPeriodicBC::whichMethod getPBCMethod() const {return _pbcMethod;};
    int getInterpolationDegree() const {return _interpolationDegree;};
    bool isAddNewVertex() const {return _addNewVertex;}; 
		
		const std::vector<additionalSecondOrderElement>& getAdditionalSecondOrderStaticPhysical() const{return _additionalSecondOrderStaticPhysical;};
    virtual nonLinearMicroBC* clone() const {return new nonLinearMixedBC(*this);}
    #endif //SWIG
		void setKinematicPhysical(const int i, const int comp){
      _kPhysical.push_back(std::pair<int,int>(i,comp));
		}
		void setStaticPhysical(const int i, const int comp){
      _sPhysical.push_back(std::pair<int,int>(i,comp));
		}
		void fixVertex(const int ver, const int comp){
      _fixVertices.push_back(std::pair<int,int>(ver,comp));
		}
		void setAdditionalSecondOrderStaticPhysical(const int phy, const int comp, const int dir){
		  _additionalSecondOrderStaticPhysical.push_back(additionalSecondOrderElement(phy,comp,dir));
		}
		void setPeriodicPhysicals(const int pos, const int neg, const int comp, const double negFact = 1.){
			_periodicPositive.push_back(std::pair<int,int>(pos,comp));
			_periodicNegative.push_back(std::pair<int,int>(neg,comp));
			_negativeFactor = negFact;
		};
    void setPeriodicBCOptions(const int method, const int deg, const bool addNew);
    
    void setPeriodicStaticPhysicals(const int positive, const int negative, const int comp){
      _sPhysicalPositive.push_back(std::pair<int,int>(positive,comp));
      _sPhysicalNegative.push_back(std::pair<int,int>(negative,comp));
    }
    
};

class nonLinearOrthogonalMixedBCDirectionFollowing : public nonLinearMicroBC{
	#ifndef SWIG
	protected:
		std::vector<SVector3> _KUBCDirection;
		std::vector<SVector3> _SUBCDirection;
	#endif // SWIG
	
	public:
		nonLinearOrthogonalMixedBCDirectionFollowing(const int tag, const int dim, const int addDofPerVertex=0);
		#ifndef SWIG
		nonLinearOrthogonalMixedBCDirectionFollowing(const nonLinearOrthogonalMixedBCDirectionFollowing& src);
		virtual ~nonLinearOrthogonalMixedBCDirectionFollowing(){};

		virtual nonLinearMicroBC::whichBCType getType() const{
			return nonLinearMicroBC::OrthogonalDirectionalMixedBC;
		};
		
		const std::vector<SVector3>& getKUBCDirection() const {return _KUBCDirection;};
		const std::vector<SVector3>& getSUBCDirection() const {return _SUBCDirection;};

		virtual nonLinearMicroBC* clone() const {return new nonLinearOrthogonalMixedBCDirectionFollowing(*this);}
		
		void setKUBCDirection(const SVector3& dir);
		void setSUBCDirection(const SVector3& dir);
		void clearDirs();
		#endif // SWIG
		void setKUBCDirection(const double nx, const double ny, const double nz);
		void setSUBCDirection(const double nx, const double ny, const double nz);
};


#endif // NONLINEARMICROBC_H_
