#!/bin/bash
#PBS -N install-petsc
#PBS -q main
#PBS -r y
#PBS -W group_list=mfoc
#
#PBS -l walltime=00:10:00
#PBS -l select=1:mem=512mb
#PBD -l pmem=512mb
#
#PBS -M l.noels@ulg.ac.be
#PBS -m abe
exec > ${PBS_O_WORKDIR}/${PBS_JOBNAME}_${PBS_JOBID}.log 
echo "------------------ Work dir --------------------" 
cd ${PBS_O_WORKDIR} && echo ${PBS_O_WORKDIR} 
echo "------------------ Job Info --------------------" 
echo "jobid : $PBS_JOBID" 
echo "jobname : $PBS_JOBNAME" 
echo "job type : $PBS_ENVIRONMENT" 
echo "submit dir : $PBS_O_WORKDIR" 
echo "queue : $PBS_O_QUEUE" 
echo "user : $PBS_O_LOGNAME" 
echo "threads : $OMP_NUM_THREADS" 

SUBDIR=$PETSC_DIR
# command (normally souldn't edit)
# module load openmpi/1.6.4/gcc/4.7.0
$MPI_RUN $SUBDIR/./conftest-$PETSC_ARCH 

qstat -f $PBS_JOBID
echo Finish
