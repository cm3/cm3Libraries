on nic3.ulg.ac.be launch your job thanks to : qsub dgshell.sh
                                              qstat to view your job's status
                                              qdel -f job_id to kill your job (in this case don't forget to delete $workdir manually)

on hmem.ucl.ac.be launch your job thanks to : sbatch dgshell.sh
                                              squeue to view your job's status
                                              scancel to kill a job (in this case don't forget to delete $workdir manually)
