#!/bin/bash

#SBATCH --job-name=mesh
#
#SBATCH --mail-user=l.noels@ulg.ac.be
#SBATCH --mail-type=ALL
#
#SBATCH --output="mesh.out"

#SBATCH --ntasks=1
#SBATCH --mem-per-cpu=200
#SBATCH --time=0:20:0

#module load openmpi/1.6.4/gcc/4.7.0
export OMPI_MCA_mtl=^psm


export PETSC_DIR=$HOME/local/petsc-3.6.4
export PETSC_ARCH=linux-gnu-c-opt
export SLEPC_DIR=$HOME/local/slepc-3.6.3
export SLEPC_ARCH=linux-gnu-c-opt


export LD_LIBRARY_PATH=$PETSC_DIR/$PETSC_ARCH/lib:$LD_LIBRARY_PATH
export PATH=$PETSC_DIR/$PETSC_ARCH/bin:$PATH


export MPI_HOME=$PETSC_DIR/$PETSC_ARCH/
export MPI_RUN=$MPI_HOME/bin/mpirun

export PATH=$HOME/local/bin:$PATH

export PATH=$PATH:$HOME/cm3Libraries/dG3D/release
export PATH=$PATH:$HOME/cm3Libraries/dG3D/release/NonLinearSolver/gmsh

export PYTHONPATH=$PYTHONPATH:$HOME/cm3Libraries/dG3D/release:$HOME/cm3Libraries/dG3D/release/NonLinearSolver/gmsh/wrappers


# place to launch computation
# place of simulation & files to launch
SUBDIR=$HOME/cm3Libraries/dG3D/benchmarks/taylorMPI
# command (normally souldn't edit)

echo "subdir=" $SUBDIR

cd $SUBDIR
mpiexec gmsh -3 -order 2 -part 128 taylor.geo
#gmsh -3 -order 2 -part 4 taylor.geo


echo Finish

