#!/bin/bash

#SBATCH --job-name=beamTest
#
#SBATCH --mail-user=gauthier.becker@ulg.ac.be
#SBATCH --mail-type=ALL
#SBATCH --time=0:5:0
#SBATCH --mem-per-cpu=200
#SBATCH --output="dgshell.out"

#SBATCH --ntasks=4


# copying mesh file
SCRATCH=/scratch/$USER/$SLURM_JOB_ID
WORKDIR=$HOME/workspace/mpiBeam
MYMPIDIR=/usr/local/openmpi/1.5.3-gcc-4.4.4/
PYFILE=beam.py
MSHFILE=beam.msh
echo Creating scratch dir $SCRATCH on $HOSTNAME
mkdir -p $SCRATCH || exit $?
cp $WORKDIR/$PYFILE $SCRATCH
cp $WORKDIR/$MSHFILE $SCRATCH

cd $SCRATCH
$MYMPIDIR/bin/mpiexec python $PYFILE

cp $SCRATCH/* $WORKDIR

echo Removing $SCRATCH on $HOSTNAME
rm -rf $SCRATCH || exit $?

