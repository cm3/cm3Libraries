#!/bin/bash -l

#PBS -N CrushModelling
#
#PBS -M xing.liu@vub.be
#
#PBS -o "CrushModelling.out"

#PBS -l mem=48gb
#PBS -l nodes=8:ppn=12
#PBS -l walltime=168:00:00
#PBS -l file=50gb

# place to launch computation
# place of simulation & files to launch
#module load GCC/4.9.3-2.25
#module load pgi/11.4
#module load OpenMPI/1.10.2-GCC-4.9.3-2.25

#module load GCC/6.4.0-2.28
#module load OpenMPI/2.1.1-GCC-6.4.0-2.28
#module load OpenBLAS/0.2.20-GCC-6.4.0-2.28
#module load ScaLAPACK/2.0.2-gompi-2017b-OpenBLAS-0.2.20
ml foss/2017b


export PETSC_DIR=$HOME/local/petsc-3.8.3
export PETSC_ARCH=linux-gnu-c-opt
export SLEPC_DIR=$HOME/local/slepc-3.8.2
export SLEPC_ARCH=linux-gnu-c-opt

MPI_HOME=$EBROOTOPENMPI
#/apps/brussel/CO7/magnycours-ib/software/OpenMPI/2.1.1-GCC-6.4.0-2.28
MPI_RUN=$MPI_HOME/bin/mpirun

#export MPI_HOME=/apps/brussel/magnycours/software/OpenMPI/1.10.2-GCC-4.9.3-2.25
#export MPI_HOME=$PETSC_DIR/$PETSC_ARCH/
#export MPI_RUN=$MPI_HOME/bin/mpirun

export LD_LIBRARY_PATH=$PETSC_DIR/$PETSC_ARCH/lib:$LD_LIBRARY_PATH
export PATH=$PETSC_DIR/$PETSC_ARCH/bin:$PATH


#export PATH=$HOME/local/bin:$PATH

export PATH=$PATH:$HOME/cm3Libraries/cm3apps/release/NonLinearSolver/gmsh
export PATH=$PATH:$HOME/cm3Libraries/dG3D/debug/NonLinearSolver/gmsh
export PATH=$PATH:$HOME/cm3Libraries/dG3D/release/NonLinearSolver/gmsh
export PYTHONPATH=$PYTHONPATH:$HOME/cm3Libraries/dG3D/release:$HOME/cm3Libraries/dG3D/debug:$HOME/cm3Libraries/dG3D/debug/NonLinearSolver/gmsh/wrappers:$HOME/cm3Libraries/dG3D/release/NonLinearSolver/gmsh/wrappers


export DIRNAME=Run_$PBS_JOBID
export SUBDIR=$WORKDIR/$DIRNAME
cd $WORKDIR
mkdir $DIRNAME
cd $TMPDIR
mkdir $DIRNAME
cd $SUBDIR



# export OMPI_MCA_mtl=^psm


cp $PBS_O_WORKDIR/*.msh $SUBDIR/
cp $PBS_O_WORKDIR/*.py $SUBDIR/

echo "subdir=" $SUBDIR
echo $MPI_RUN

echo "Python: $(which python)"

mpirun -np $PBS_NP python Tube_4_Ani-M-CL-090_QSI_DG-CG.py -temp $TMPDIR/$DIRNAME

echo Finish

