#!/bin/bash -l

#PBS -N petscinstall
#
#PBS -M l.noels@ulg.ac.be
#
#PBS -o "petscinstall.out"

#PBS -l mem=100mb
#PBS -l nodes=1:ppn=1
#PBS -l walltime=00:05:00

# place to launch computation
# place of simulation & files to launch
module load GCC/6.4.0-2.28
module load OpenMPI/2.1.1-GCC-6.4.0-2.28
module load OpenBLAS/0.2.20-GCC-6.4.0-2.28
module load ScaLAPACK/2.0.2-gompi-2017b-OpenBLAS-0.2.20

#module load GCC/6.3.0-2.27
#module load pgi/11.4
#module load pkg-config/0.29.1-GCCcore-6.3.0
#module load OpenMPI/2.0.2-GCC-6.3.0-2.27
#module load ORCA/4.0.0.2-OpenMPI-2.0.2


PETSC_DIR=$HOME/local/petsc-3.8.3
PETSC_ARCH=linux-gnu-c-opt
SLEPC_DIR=$HOME/local/slepc-3.8.2
SLEPC_ARCH=linux-gnu-c-opt

MPI_HOME=/apps/brussel/CO7/magnycours-ib/software/OpenMPI/2.1.1-GCC-6.4.0-2.28
MPI_RUN=$MPI_HOME/bin/mpirun

#MPI_HOME=/apps/brussel/magnycours/software/OpenMPI/2.0.2-GCC-6.3.0-2.27
#MPI_HOME=$PETSC_DIR/$PETSC_ARCH/
#MPI_RUN=$MPI_HOME/bin/mpirun

SUBDIR=$PBS_O_WORKDIR
cd $SUBDIR

# command (normally souldn't edit)
$MPI_RUN $SUBDIR/./conftest-$PETSC_ARCH

echo Finish


