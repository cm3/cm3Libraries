#!/bin/sh

# script example for nic3.ulg.ac.be
# the #dollar are pseudo command for qsub so they have to be edited !!


#$ -N beamTest
#$ -m beas
#$ -M l.noels@ulg.ac.be
#$ -l h_vmem=400M
#$ -l h_rt=0:06:00
#$ -cwd

source /etc/profile.d/modules.sh 
source /etc/profile.d/segi.sh 

module add gcc/4.7.2
module add openmpi/gcc/64/1.6.2

SUBDIR=/u/lnoels/local/petsc-3.4.3
echo "SUBDIR=" $SUBDIR

# command (normally souldn't edit)

$SUBDIR/./conftest-linux-gnu-c-opt

echo Finish
~                   
