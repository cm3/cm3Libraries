#!/bin/sh

# script example for nic3.ulg.ac.be
# the #dollar are pseudo command for qsub so they have to be edited !!


#$ -N composite60
#$ -m beas
#$ -M L.Noels@ulg.ac.be
#$ -l h_vmem=750M
#$ -l h_rt=479:00:00
#$ -pe openmpi 48
#$ -cwd
#$ -j yes

source /etc/profile.d/modules.sh 
source /etc/profile.d/segi.sh 

module add gcc/4.7.2
module add openmpi/gcc/64/1.6.2
subdir=/u/lnoels/cm3Libraries/dG3D/benchmarks/Composite60
echo "subdir=" $subdir

workdir=/tmpscratch/${USER}_$JOB_ID
echo "workdir=" $workdir

pyfile=composite60.py
mshfile=c50_2-48.msh


echo "cat \$TMPDIR/machines | uniq"
cat $TMPDIR/machines | uniq

for ii in `cat $TMPDIR/machines | uniq`
	do
		echo $ii
		ssh $ii mkdir $workdir 
		ssh $ii cp $subdir/$pyfile $workdir/ 
		ssh $ii cp $subdir/$mshfile $workdir/ 
	done

cd $workdir # needed to work in the tmpscrach dir otherwise work on home !!
#ls -artl
export PETSC_DIR=/u/lnoels/local/petsc
export PETSC_ARCH=linux-gnu-cxx-opt
export PYTHONPATH=$PYTHONPATH:/u/lnoels/cm3Libraries/dG3D/release:/u/lnoels/gmsh/cm3Libraries/release/NonLinearSolver/gmsh/wrappers
export LD_PRELOAD=$LD_PRELOAD:/cvos/shared/apps/openmpi/gcc/64/1.6.2/lib64/libmpi.so
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/cvos/shared/apps/openmpi/gcc/64/1.6.2/lib64
export PATH=$PATH:/cvos/shared/apps/openmpi/gcc/64/1.6.2/bin
export LD_LIBRARY_PATH=/u/lnoels/local/lib:$LD_LIBRARY_PATH


mpiexec -n 48 python  $workdir/$pyfile >& $workdir/output.txt

echo -e "\n"

sleep 5

for ii in `tac $TMPDIR/machines | uniq`
  do
     ssh $ii mv $workdir/output.txt $subdir/output_$ii.txt
     ssh $ii mv $workdir/* $subdir/ # copy archiving files
     ssh $ii rm -rf $workdir
  done
echo -e "\n"
