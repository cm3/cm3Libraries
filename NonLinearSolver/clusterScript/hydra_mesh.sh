#!/bin/bash -l

#PBS -N genMesh
#
#PBS -M l.noels@ulg.ac.be
#
#PBS -o "genMesh.out"

#PBS -l mem=100mb
#PBS -l nodes=1:ppn=1
#PBS -l walltime=00:05:00

# place to launch computation
# place of simulation & files to launch
#module load GCC/4.9.3-2.25
#module load pgi/11.4
#module load OpenMPI/1.10.2-GCC-4.9.3-2.25

export PETSC_DIR=$HOME/local/petsc-3.6.4
export PETSC_ARCH=linux-gnu-c-opt
export SLEPC_DIR=$HOME/local/slepc-3.6.3
export SLEPC_ARCH=linux-gnu-c-opt

#export MPI_HOME=/apps/brussel/magnycours/software/OpenMPI/1.10.2-GCC-4.9.3-2.25
export MPI_HOME=$PETSC_DIR/$PETSC_ARCH/
export MPI_RUN=$MPI_HOME/bin/mpirun

export LD_LIBRARY_PATH=$PETSC_DIR/$PETSC_ARCH/lib:$LD_LIBRARY_PATH
export PATH=$PETSC_DIR/$PETSC_ARCH/bin:$PATH


#export PATH=$HOME/local/bin:$PATH

export PATH=$PATH:$HOME/cm3Libraries/cm3apps/release/NonLinearSolver/gmsh
export PATH=$PATH:$HOME/cm3Libraries/dG3D/debug/NonLinearSolver/gmsh
export PATH=$PATH:$HOME/cm3Libraries/dG3D/release/NonLinearSolver/gmsh
export PYTHONPATH=$PYTHONPATH:$HOME/cm3Libraries/dG3D/release:$HOME/cm3Libraries/dG3D/debug:$HOME/cm3Libraries/dG3D/debug/NonLinearSolver/gmsh/wrappers:$HOME/cm3Libraries/dG3D/release/NonLinearSolver/gmsh/wrappers

export SUBDIR=$PBS_O_WORKDIR
cd $SUBDIR



# export OMPI_MCA_mtl=^psm



# place to launch computation
# place of simulation & files to launch
# SUBDIR=$HOME/cm3Libraries/dG3D/benchmarks/taylorMPI
# command (normally souldn't edit)

echo "subdir=" $SUBDIR

cd $SUBDIR
gmsh -3 -order 2 -part 4 taylor.geo

echo Finish

