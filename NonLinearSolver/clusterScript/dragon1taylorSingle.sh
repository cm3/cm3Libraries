#!/bin/sh

# script example for dragon1
#SBATCH --job-name taylorMPI
#SBATCH --mail-user=l.noels@ulg.ac.be
#SBATCH --mail-type=ALL
#SBATCH --output="out.txt"
#SBATCH --ntasks=1
#SBATCH --mem-per-cpu=800
#SBATCH --time=0:20:0


module load OpenMPI/3.1.1-GCC-7.3.0-2.30

SUBDIR=$HOME/cm3Libraries/dG3D/benchmarks/taylor
echo "subdir=" $SUBDIR

SCRATCH=/scratch/${USER}_$SLURM_JOB_ID
echo "workdir=" $SCRATCH


pyfile=taylorTetDGDyn.py
mshfile=taylor.msh
echo "node list of job"  >> $SUBDIR/output.po$SLURM_JOB_ID

srun hostname >> $SUBDIR/output.po$SLURM_JOB_ID

srun mkdir -p $SCRATCH || exit $?
srun cp $SUBDIR/$pyfile $SCRATCH/ || exit $?
srun cp $SUBDIR/$mshfile $SCRATCH/ || exit $? 

cd $SCRATCH # needed to work in the tmpscrach dir otherwise work on home !!
#ls -artl


export PETSC_DIR=$HOME/local/petsc-3.13.2
export PETSC_ARCH=arch-linux-cxx-opt

export PATH=$HOME/local/bin:$PATH

export PATH=$PATH:$HOME/cm3Libraries/dG3D/release/NonLinearSolver/gmsh


export PYTHONPATH=$PYTHONPATH:$HOME/cm3Libraries/dG3D/release:$HOME/cm3Libraries/dG3D/debug:$HOME/cm3Libraries/dG3D/debug/NonLinearSolver/gmsh/utils/wrappers:$HOME/cm3Libraries/dG3D/debug/NonLinearSolver/gmsh/utils/wrappers/gmshpy:$HOME/cm3Libraries/dG3D/release/NonLinearSolver/gmsh/utils/wrappers:$HOME/cm3Libraries/dG3D/release/NonLinearSolver/gmsh/utils/wrappers/gmshpy


mpirun python  $SCRATCH/$pyfile >& $SCRATCH/output.txt

echo -e "\n"

sleep 5

srun cp -f $SCRATCH/* $SUBDIR/  || exit $? 
srun rm -rf $SCRATCH ||  exit $? 
echo -e "\n"




