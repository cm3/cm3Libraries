#!/bin/bash

#SBATCH --job-name=petsc-install
#
#SBATCH --mail-user=l.noels@ulg.ac.be
#SBATCH --mail-type=ALL
#
#SBATCH --output="petsc-install.out"

#SBATCH --ntasks=1
#SBATCH --mem-per-cpu=200
#SBATCH --time=0:20:0

# place to launch computation 
# place of simulation & files to launch
SUBDIR=$PETSC_DIR
# command (normally souldn't edit)

$SUBDIR/./conftest-$PETSC_ARCH

echo Finish
