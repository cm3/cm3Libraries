#!/bin/sh

# script example for nic5
#SBATCH --job-name taylorMPI
#SBATCH --mail-user=l.noels@ulg.ac.be
#SBATCH --mail-type=ALL
#SBATCH --output="out.txt"
#SBATCH --ntasks=4
#SBATCH --mem-per-cpu=1000
#SBATCH --partition=batch
#SBATCH --time=0-00:45:00

#export OMP_NUM_THREADS=8
#export MKL_NUM_THREADS=8

module load OpenMPI/3.1.4-GCC-8.3.0
export OMPI_MCA_mtl=^psm
export OMPI_MCA_mtl=^psm

SUBDIR=$HOME/cm3Libraries/dG3D/benchmarks/taylorMPI
echo "subdir=" $SUBDIR

SCRATCH=$GLOBALSCRATCH/${USER}_$SLURM_JOB_ID
echo "workdir=" $SCRATCH

echo "node list of job"
hostname


pyfile=taylorCG.py
mshfile=taylor.msh

echo "node list of job"  >& $SUBDIR/output.po$SLURM_JOB_ID
srun hostname >& $SUBDIR/output.po$SLURM_JOB_ID

cd $GLOBALSCRATCH/
mkdir -p ${USER}_$SLURM_JOB_ID || exit $?
cp $SUBDIR/$pyfile $SCRATCH/ || exit $?
cp $SUBDIR/$mshfile $SCRATCH/ || exit $?
#cp $SUBDIR/*.i01 $SCRATCH/ || exit $?
#cp $SUBDIR//*.ckp $SCRATCH/ || exit $?
#cp $SUBDIR/restart*.msh $SCRATCH/ || exit $?
cd $SCRATCH


export PETSC_DIR=$HOME/local/petsc-3.14.4
export PETSC_ARCH=linux-gnu-c-opt
export SLEPC_DIR=$HOME/local/slepc-3.14.2
export SLEPC_ARCH=linux-gnu-c-opt


export PATH=$HOME/local/bin:$PATH
export PATH=$PATH:$HOME/cm3Libraries/dG3D/release
export PATH=$PATH:$HOME/cm3Libraries/dG3D/release/NonLinearSolver/gmsh

export PYTHONPATH=$PYTHONPATH:$HOME/cm3Libraries/dG3D/release:$HOME/cm3Libraries/dG3D/release/NonLinearSolver/gmsh/utils/wrappers

mpirun -np ${SLURM_NTASKS} python3 $SCRATCH/$pyfile >& $SCRATCH/output.txt


echo -e "\n"


sleep 5

cd $SUBDIR/
mkdir -p ${USER}_$SLURM_JOB_ID || exit $?
cp -f $SCRATCH/* $SUBDIR/${USER}_$SLURM_JOB_ID || exit $?
rm -rf $SCRATCH ||  exit $?
echo -e "\n"







