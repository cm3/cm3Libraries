#!/bin/bash

#SBATCH --job-name=petsc-install
#
#SBATCH --mail-user=l.noels@ulg.ac.be
#SBATCH --mail-type=ALL
#
#SBATCH --output="mesh.out"

#SBATCH --ntasks=1
#SBATCH --mem-per-cpu=200
#SBATCH --time=0:20:0

# place to launch computation 
# place of simulation & files to launch
#module load openmpi/qlc/gcc/64/1.6.4
#export LD_PRELOAD=""
export OMPI_MCA_mtl=^psm

SUBDIR=$HOME/cm3/dG3D/nonLocalDamageBenchmarks/Double_notch
echo "subdir=" $SUBDIR

export PATH=$PATH:$HOME/cm3Libraries/dG3D/release
export PATH=$PATH:$HOME/cm3Libraries/dG3D/release/NonLinearSolver/gmsh

cd $SUBDIR
gmsh -3 -order 2 -part 128 DNT_band.geo


echo Finish
