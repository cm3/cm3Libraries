#!/bin/bash

#SBATCH --job-name=taylorMPI
#
#SBATCH --mail-user=l.noels@ulg.ac.be
#SBATCH --mail-type=ALL
#
#SBATCH --output="taylorMPI.out"

#SBATCH --ntasks=8
#SBATCH --mem-per-cpu=2000
#SBATCH --time=1:00:00

#module load OpenMPI/1.7.3-GCC-4.8.2
module load python/2.7.3
module load GCC/4.8.2
export OMPI_MCA_mtl=^psm


SUBDIR=$HOME/cm3Libraries/dG3D/benchmarks/taylorMPI 
echo "subdir=" $SUBDIR


pyfile=taylorCG.py
mshfile=taylor.msh

echo "node list of job"  >> $SUBDIR/output.po$SLURM_JOB_ID

srun hostname >> $SUBDIR/output.po$SLURM_JOB_ID


export PETSC_DIR=$HOME/local/petsc-3.6.4
export PETSC_ARCH=linux-gnu-c-opt
export SLEPC_DIR=$HOME/local/slepc-3.6.3
export SLEPC_ARCH=linux-gnu-c-opt

export LD_LIBRARY_PATH=$PETSC_DIR/$PETSC_ARCH/lib:$LD_LIBRARY_PATH
export PATH=$PETSC_DIR/$PETSC_ARCH/bin:$PATH

export MPI_HOME=$PETSC_DIR/$PETSC_ARCH/
export MPI_RUN=$MPI_HOME/bin/mpirun

export PATH=$HOME/local/bin:$PATH

export PATH=$PATH:$HOME/cm3Libraries/dG3D/release
export PATH=$PATH:$HOME/cm3Libraries/dG3D/release/NonLinearSolver/gmsh




export PYTHONPATH=$PYTHONPATH:$HOME/cm3Libraries/dG3D/release:$HOME/cm3Libraries/dG3D/debug:$HOME/cm3Libraries/dG3D/debug/NonLinearSolver/gmsh/utils/wrappers:$HOME/cm3Libraries/dG3D/debug/NonLinearSolver/gmsh/utils/wrappers/gmshpy:$HOME/cm3Libraries/dG3D/release/NonLinearSolver/gmsh/utils/wrappers:$HOME/cm3Libraries/dG3D/release/NonLinearSolver/gmsh/utils/wrappers/gmshpy


#export PATH=$HOME/local/bin:$PATH

mpiexec -np 8 python  $SUBDIR/$pyfile -o $SUBDIR/output.txt

sleep 5

echo -e "\n"




