#!/bin/sh

# script example for nic4
#SBATCH --job-name DNT
#SBATCH --mail-user=l.noels@ulg.ac.be
#SBATCH --mail-type=ALL
#SBATCH --output="out.txt"
#SBATCH --ntasks=128
#SBATCH --mem-per-cpu=800
#SBATCH --time=3:00:0


#module load openmpi/qlc/gcc/64/1.6.4
#export LD_PRELOAD="/cm/shared/apps/openmpi/qlc/gcc/64/1.6.4/lib/libmpi.so"
export OMPI_MCA_mtl=^psm

SUBDIR=$HOME/cm3/dG3D/nonLocalDamageBenchmarks/Double_notch
echo "subdir=" $SUBDIR

pyfile=DNT.py
mshfile=DNT_band.msh
propfile=prop_ep.i01
echo "node list of job"  >> $SUBDIR/output.po$SLURM_JOB_ID

srun hostname >> $SUBDIR/output.po$SLURM_JOB_ID

export PETSC_DIR=$HOME/local/petsc-3.6.4
export PETSC_ARCH=linux-gnu-c-opt
export SLEPC_DIR=$HOME/local/slepc-3.6.3
export SLEPC_ARCH=linux-gnu-c-opt


export PATH=$HOME/local/bin:$PATH
export PATH=$PATH:$HOME/cm3Libraries/dG3D/release
export PATH=$PATH:$HOME/cm3Libraries/dG3D/release/NonLinearSolver/gmsh




export PYTHONPATH=$PYTHONPATH:$HOME/cm3Libraries/dG3D/release:$HOME/cm3Libraries/dG3D/debug:$HOME/cm3Libraries/dG3D/debug/NonLinearSolver/gmsh/utils/wrappers:$HOME/cm3Libraries/dG3D/debug/NonLinearSolver/gmsh/utils/wrappers/gmshpy:$HOME/cm3Libraries/dG3D/release/NonLinearSolver/gmsh/utils/wrappers:$HOME/cm3Libraries/dG3D/release/NonLinearSolver/gmsh/utils/wrappers/gmshpy



mpirun python $SUBDIR/$pyfile >& $SUBDIR/output.txt

echo -e "\n"






