#-*-coding:Utf-8-*-

# script python to regroup crackpath files at the end of a MPI simulation
from os import system,getcwd,listdir

# by default the name of the files is crackpath# with # the number of the MPI rank

namefile = "energy"

# remove previous files
system("rm -f %s.csv"%namefile)

# get the files of the folder
lfiles = listdir(getcwd())
lfilesener = []

for fname in lfiles:
    if(fname[:len(namefile)] == namefile):
        lfilesener.append(open(fname,'r'))

enerfile = open("%s.csv"%namefile,'w')

file0 = lfilesener[0]
lfilesener.remove(file0)
enerfile.write(file0.readline())
for filei in lfilesener: # skip the text line
    tempo = filei.readline()[:-1]

while 1:
    tempo = file0.readline()[:-1]
    if(tempo == ''):
        break
    lsttmp = tempo.split(';')
    lstener = []
    for i in range(len(lsttmp)):
        if(lsttmp[i]!=''):
            lstener.append(float(lsttmp[i]))
    # loop on other partition file
    for filei in lfilesener:
        tempo = filei.readline()[:-1]
        lsttmp = tempo.split(';')
        for i in range(len(lsttmp)-1): # the first value time is common to all rank and it is not summed !
            if(lsttmp[i+1] != ''):
                lstener[i+1] += float(lsttmp[i+1])
    lstw = []
    for i in range(len(lstener)):
        lstw.append("%e"%lstener[i])
    # write the result in the file
    enerfile.write(';'.join(lstw)+'\n')

enerfile.close()
for files in lfilesener :
    files.close()

print "regroup OK"
