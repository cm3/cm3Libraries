#-*-coding:Utf-8-*-

# script python to regroup crackpath files at the end of a MPI simulation
# only for mesh format 3
from os import system,getcwd,listdir

# by default the name of the files is crackpath# with # the number of the MPI rank

namefile = "jump"
meshname = "SampleHalfRef"
openfile = "disp"
minStep  = 2200
maxStep  = 2201
maxProc  = 10

computeStepNb     = -1 #-1 = all of them
# remove previous files
#system("rm -f %s*"%namefile)



meshfile = open("%s.msh"%meshname,'r')


while 1:
  tempo = meshfile.readline()[:-1]
  if(tempo == '$Nodes'):
    break
tempo = meshfile.readline()[:-1]
nnodes = int(tempo)
listnodes=[];
elemnodes=[];
elements=[];
while 1:
  tempo = meshfile.readline()[:-1]
  if(tempo == '$EndNodes'):
    break
  else:
    lsttmp = tempo.split(' ')
    listnodes.append([int(lsttmp[0]), float(lsttmp[1]), float(lsttmp[2]), float(lsttmp[3])]) #number pos
    elemnodes.append([int(lsttmp[0])])
while 1:
  tempo = meshfile.readline()[:-1]
  if(tempo == '$Elements'):
    break
tempo = meshfile.readline()[:-1]
nelements = 0
while 1:
  tempo = meshfile.readline()[:-1]
  if(tempo == '$EndElements'):
    break
  else:
    lsttmp = tempo.split(' ')
    found=0
    if(int(lsttmp[1])==5): #quad first order
      found=1
      nelements=nelements+1
      offset=int(lsttmp[3])-8
      elements.append([int(lsttmp[0]), int(lsttmp[2]), int(lsttmp[len(lsttmp)-8-offset]), int(lsttmp[len(lsttmp)-7-offset]), int(lsttmp[len(lsttmp)-6-offset]), int(lsttmp[len(lsttmp)-5-offset]), int(lsttmp[len(lsttmp)-4-offset]), int(lsttmp[len(lsttmp)-3-offset]), int(lsttmp[len(lsttmp)-2-offset]), int(lsttmp[len(lsttmp)-1-offset])]) #number, domain, nodes
    if(int(lsttmp[1])==12): #quad second order
      found=1
      nelements=nelements+1
      offset=int(lsttmp[3])-27
      elements.append([int(lsttmp[0]), int(lsttmp[2]), int(lsttmp[len(lsttmp)-27-offset]), int(lsttmp[len(lsttmp)-26-offset]), int(lsttmp[len(lsttmp)-25-offset]), int(lsttmp[len(lsttmp)-24-offset]), int(lsttmp[len(lsttmp)-23-offset]), int(lsttmp[len(lsttmp)-22-offset]), int(lsttmp[len(lsttmp)-21-offset]), int(lsttmp[len(lsttmp)-20-offset]), int(lsttmp[len(lsttmp)-19-offset]), int(lsttmp[len(lsttmp)-18-offset]), int(lsttmp[len(lsttmp)-17-offset]), int(lsttmp[len(lsttmp)-16-offset]), int(lsttmp[len(lsttmp)-15-offset]), int(lsttmp[len(lsttmp)-14-offset]), int(lsttmp[len(lsttmp)-13-offset]), int(lsttmp[len(lsttmp)-12-offset]), int(lsttmp[len(lsttmp)-11-offset]), int(lsttmp[len(lsttmp)-10-offset]), int(lsttmp[len(lsttmp)-9-offset]), int(lsttmp[len(lsttmp)-8-offset]), int(lsttmp[len(lsttmp)-7-offset]), int(lsttmp[len(lsttmp)-6-offset]), int(lsttmp[len(lsttmp)-5-offset]), int(lsttmp[len(lsttmp)-4-offset]), int(lsttmp[len(lsttmp)-3-offset]), int(lsttmp[len(lsttmp)-2-offset]), int(lsttmp[len(lsttmp)-1-offset])]) #number, domain, nodes
    if(int(lsttmp[1])==4): #tet first order
      found=1
      nelements=nelements+1
      offset=int(lsttmp[3])-4
      elements.append([int(lsttmp[0]), int(lsttmp[2]), int(lsttmp[len(lsttmp)-4-offset]), int(lsttmp[len(lsttmp)-3-offset]), int(lsttmp[len(lsttmp)-2-offset]), int(lsttmp[len(lsttmp)-1-offset])]) #number, domain, nodes
    if(int(lsttmp[1])==11): #tet second order
      found=1
      nelements=nelements+1
      offset=int(lsttmp[3])-10
      elements.append([int(lsttmp[0]), int(lsttmp[2]), int(lsttmp[len(lsttmp)-10-offset]), int(lsttmp[len(lsttmp)-9-offset]), int(lsttmp[len(lsttmp)-8]-offset), int(lsttmp[len(lsttmp)-7-offset]), int(lsttmp[len(lsttmp)-6-offset]), int(lsttmp[len(lsttmp)-5-offset]), int(lsttmp[len(lsttmp)-4-offset]), int(lsttmp[len(lsttmp)-3-offset]), int(lsttmp[len(lsttmp)-2-offset]), int(lsttmp[len(lsttmp)-1-offset])]) #number, domain, nodes
    if(found==1):
      for i in range(2,len(elements[nelements-1])):
        nn = elements[nelements-1][i]
        elemnodes[nn-1].append(elements[nelements-1][0])
    if(int(lsttmp[1])==6): #penta first order
      found=1
      nelements=nelements+1
      offset=int(lsttmp[3])-6
      elements.append([int(lsttmp[0]), int(lsttmp[2]), int(lsttmp[len(lsttmp)-6-offset]), int(lsttmp[len(lsttmp)-5-offset]), int(lsttmp[len(lsttmp)-4-offset]), int(lsttmp[len(lsttmp)-3-offset]), int(lsttmp[len(lsttmp)-2-offset]), int(lsttmp[len(lsttmp)-1-offset])]) #number, domain, nodes
    if(int(lsttmp[1])==13): #penta second order
      found=1
      nelements=nelements+1
      offset=int(lsttmp[3])-18
      elements.append([int(lsttmp[0]), int(lsttmp[2]), int(lsttmp[len(lsttmp)-18-offset]), int(lsttmp[len(lsttmp)-17-offset]), int(lsttmp[len(lsttmp)-16-offset]), int(lsttmp[len(lsttmp)-15-offset]), int(lsttmp[len(lsttmp)-14-offset]), int(lsttmp[len(lsttmp)-13-offset]), int(lsttmp[len(lsttmp)-12-offset]), int(lsttmp[len(lsttmp)-11-offset]), int(lsttmp[len(lsttmp)-10-offset]), int(lsttmp[len(lsttmp)-9-offset]), int(lsttmp[len(lsttmp)-8-offset]), int(lsttmp[len(lsttmp)-7-offset]), int(lsttmp[len(lsttmp)-6-offset]), int(lsttmp[len(lsttmp)-5-offset]), int(lsttmp[len(lsttmp)-4-offset]), int(lsttmp[len(lsttmp)-3-offset]), int(lsttmp[len(lsttmp)-2-offset]), int(lsttmp[len(lsttmp)-1-offset])]) #number, domain, nodes
    if(found==1):
      for i in range(2,len(elements[nelements-1])):
        nn = elements[nelements-1][i]
        elemnodes[nn-1].append(elements[nelements-1][0])


meshfile.close()
lfilesdisp=[];
for st in range(minStep,maxStep+1):
  for proc in range(maxProc+1):
    filename=str(openfile)+'_part'+str(proc)+'_step'+str(st)+'.msh'
    if(proc==maxProc):
      filename=str(openfile)+'_step'+str(st)+'.msh'
    try:
      of=open(filename,'r')           
    except:
      print('Not found file',filename)
    else:
      lfilesdisp.append(of)
  
steps=[] #steps[stepNb][nodeNb] = [x y z], [x y z], .. 
for files in lfilesdisp :
    print 'Now reading',files.name
    while 1:
      tempo = files.readline()[:-1]
      if(tempo == ''):
        break
      while 1:
        if(tempo == '$ElementNodeData'):
          break
        tempo = files.readline()[:-1]
      tempo = files.readline()[:-1]
      tempo = files.readline()[:-1]
      tempo = files.readline()[:-1]
      tempo = files.readline()[:-1]
      tempo = files.readline()[:-1]
      tempo = files.readline()[:-1]
      stNb = int(tempo)
      print 'read step ',stNb
      if(computeStepNb<0 or computeStepNb==stNb):
        print 'treating step ',stNb
        tempo = files.readline()[:-1]
        tempo = files.readline()[:-1]
        tempo = files.readline()[:-1]
        while 1:
          tempo = files.readline()[:-1]
          if(tempo == '$EndElementNodeData'):
            break
          lsttmp = tempo.split(' ')
          elemNum = int(lsttmp[0])
          elemNnodes = int(lsttmp[1]) 
          #find element
          row=0
          while 1:
            if(elements[row][0]==elemNum):
              break
            else:
              row=row+1
          for n in range(elemNnodes):
            x=float(lsttmp[2+n*3])
            y=float(lsttmp[2+n*3+1])
            z=float(lsttmp[2+n*3+2])
            nodeNumber=elements[row][n+2]
            findStep=0
            for st in range (len(steps)):
              if(steps[st][0]==stNb):
                #print steps
                findStep =1
                break
            if(findStep==1):
              findNode=0
              for nd in range (len(steps[st][1])):
                if(steps[st][1][nd][0]==nodeNumber):
                  findNode=1
                  steps[st][1][nd].append([x,y,z])
                  break
              if(findNode==0):
                steps[st][1].append([nodeNumber, [x, y, z]])
            else:
              steps.append([stNb,[[nodeNumber,[x,y,z]]]])
      else:
        while 1:
          tempo = files.readline()[:-1]
          if(tempo == '$EndElementNodeData'):
            break

#print steps
jumps=[]
for st in range(len(steps)):
    stnb = steps[st][0]
    for no in range(len(steps[st][1])):
      nonb = steps[st][1][no][0]
      nbdof= len(steps[st][1][no])-1
      xjump=0.
      yjump=0.
      zjump=0.
      for ndof1 in range(nbdof-1):
        for ndof2 in range(ndof1+1,nbdof):
          dx = steps[st][1][no][ndof1+1][0]-steps[st][1][no][ndof2+1][0]
          dy = steps[st][1][no][ndof1+1][1]-steps[st][1][no][ndof2+1][1]
          dz = steps[st][1][no][ndof1+1][2]-steps[st][1][no][ndof2+1][2]
          if(abs(dx)>xjump):
             xjump=abs(dx)
          if(abs(dy)>yjump):
             yjump=abs(dy)
          if(abs(dz)>zjump):
             zjump=abs(dz)
      #here need to check if jump exists
      findStep=0
      for st in range (len(jumps)):
        if(steps[st][0]==stnb):
          findStep =1
          break
      if(findStep==1):
        jumps[st][1].append([nonb,[xjump,yjump,zjump]])
      else:
        jumps.append([stnb,[[nonb,[xjump,yjump,zjump]]]])
  #print jumps
#write
for files in lfilesdisp :
    ext=files.name[len(openfile):]
    jumpfile = open("%s"%namefile+ext,"w")
    print 'Now writing',jumpfile.name
    files.seek(0)
    tempo = files.readline()[:-1]
    jumpfile.write(tempo+"\n")
    tempo = files.readline()[:-1]
    jumpfile.write(tempo+"\n")
    tempo = files.readline()[:-1]
    jumpfile.write(tempo+"\n")
    while 1:
      tmpout=[]
      tempo = files.readline()[:-1]
      if(tempo == ''):
        break
      while 1:
        if(tempo == '$ElementNodeData'):
          break
        tempo = files.readline()[:-1]
      tmpout.append(tempo+"\n")
      tempo = files.readline()[:-1]
      tmpout.append(tempo+"\n")
      tempo = files.readline()[:-1]
      tmpout.append('"opening"'+"\n")
      tempo = files.readline()[:-1]
      tmpout.append(tempo+"\n")
      tempo = files.readline()[:-1]
      tmpout.append(tempo+"\n")
      tempo = files.readline()[:-1]
      tmpout.append(tempo+"\n")
      tempo = files.readline()[:-1]
      tmpout.append(tempo+"\n")
      stNb = int(tempo)
      print 'now using step ', stNb
      if(stNb != computeStepNb and computeStepNb>0): 
        while 1:
          tmpout=[]
          while 1:
            if(tempo == ''):
              break
            if(tempo == '$ElementNodeData'):
              #tmpout.append(tempo+"\n")
              break
            tempo = files.readline()[:-1]
          if(tempo == ''):
            break
          tmpout.append(tempo+"\n")
          tempo = files.readline()[:-1]
          tmpout.append(tempo+"\n")
          tempo = files.readline()[:-1]
          tmpout.append(tempo+"\n")
          tempo = files.readline()[:-1]
          tmpout.append(tempo+"\n")
          tempo = files.readline()[:-1]
          tmpout.append(tempo+"\n")
          tempo = files.readline()[:-1]
          tmpout.append(tempo+"\n")
          tempo = files.readline()[:-1]
          tmpout.append(tempo+"\n")
          stNb = int(tempo)
          print 'now using step ',stNb
          if(stNb == computeStepNb):
            break
          else:
            while 1:
              tempo = files.readline()[:-1]
              if(tempo == '$EndElementNodeData' or tempo == ''):
                tmpout.append(tempo+"\n")
                break
      print 'treating step ',stNb
      stepnb=-1
      for st in range(len(jumps)):
        if (jumps[st][0]==stNb):
          stepnb=st
          break
      if(stepnb==-1):
        print 'could not find the jump: step not existing'
      else:
        tempo = files.readline()[:-1]
        tmpout.append(tempo+"\n")
        tempo = files.readline()[:-1]
        tmpout.append(tempo+"\n")
        tempo = files.readline()[:-1]
        tmpout.append(tempo+"\n")
        for lin in range(len(tmpout)):
          jumpfile.write(tmpout[lin])
        while 1:
          tempo = files.readline()[:-1]
          if(tempo == '$EndElementNodeData'):
            jumpfile.write(tempo+"\n")
            break
          lsttmp = tempo.split(' ')
          elemNum = int(lsttmp[0])
          elemNnodes = int(lsttmp[1]) 
          outline=[]
          outline.append("%d"%elemNum)
          outline.append("%d"%elemNnodes)
          #find element
          row=0
          while 1:
            if(elements[row][0]==elemNum):
              break
            else:
              row=row+1
          for n in range(elemNnodes):
            nodeNumber=elements[row][n+2]
            #print nodeNumber
            elnb=-1
            for el in range(len(jumps[stepnb][1])):
              if (jumps[stepnb][1][el][0]==nodeNumber):
                elnb=el
                break
            if(elnb==-1):
              print 'could not find the jump: node number not existing'
            dx=jumps[stepnb][1][elnb][1][0]
            dy=jumps[stepnb][1][elnb][1][1]
            dz=jumps[stepnb][1][elnb][1][2]
            outline.append("%e"%dx)
            outline.append("%e"%dy)
            outline.append("%e"%dz)
          jumpfile.write(' '.join(outline)+'\n')
         
    jumpfile.close()
    files.close()


#file0 = lfilesener[0]
#lfilesener.remove(file0)
#enerfile.write(file0.readline())
#for filei in lfilesener: # skip the text line
#    tempo = filei.readline()[:-1]

#while 1:
#    tempo = file0.readline()[:-1]
#    if(tempo == ''):
#        break
#    lsttmp = tempo.split(';')
#    lstener = []
#    for i in range(len(lsttmp)):
#        if(lsttmp[i]!=''):
#            lstener.append(float(lsttmp[i]))
#    # loop on other partition file
#    for filei in lfilesener:
#        tempo = filei.readline()[:-1]
#        lsttmp = tempo.split(';')
#        for i in range(len(lsttmp)-1): # the first value time is common to all rank and it is not summed !
#            if(lsttmp[i+1] != ''):
#                lstener[i+1] += float(lsttmp[i+1])
#    lstw = []
#    for i in range(len(lstener)):
#        lstw.append("%e"%lstener[i])
    # write the result in the file
#    enerfile.write(';'.join(lstw)+'\n')


print "computeJump OK"
