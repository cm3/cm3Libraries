#!/bin/bash -l

#PBS -N taylor
#
#PBS -M l.noels@ulg.ac.be
#
#PBS -o "taylor.out"

#PBS -l mem=500mb
#PBS -l nodes=1:ppn=4
#PBS -l walltime=01:00:00

# place to launch computation
# place of simulation & files to launch

module load GCC/6.4.0-2.28
module load OpenMPI/2.1.1-GCC-6.4.0-2.28
module load OpenBLAS/0.2.20-GCC-6.4.0-2.28
module load ScaLAPACK/2.0.2-gompi-2017b-OpenBLAS-0.2.20
module load CMake/3.9.1-GCCcore-6.4.0
module load METIS/5.1.0-GCCcore-6.4.0
export PETSC_DIR=$HOME/local/petsc-3.8.3
export PETSC_ARCH=linux-gnu-c-opt
export SLEPC_DIR=$HOME/local/slepc-3.8.2
export SLEPC_ARCH=linux-gnu-c-opt
export LD_LIBRARY_PATH=$PETSC_DIR/$PETSC_ARCH/lib:$LD_LIBRARY_PATH
#export PATH=$PETSC_DIR/$PETSC_ARCH/bin:$PATH
MPI_HOME=/apps/brussel/CO7/magnycours-ib/software/OpenMPI/2.1.1-GCC-6.4.0-2.28 #$PETSC_DIR/$PETSC_ARCH/
MPI_RUN=$MPI_HOME/bin/mpirun
#MPI_HOME=$EBROOTOPENMPI
#MPI_RUN=$MPI_HOME/bin/mpirun

export PATH=$PATH:$HOME/cm3Libraries/cm3apps/release/NonLinearSolver/gmsh
export PATH=$PATH:$HOME/cm3Libraries/dG3D/debug/NonLinearSolver/gmsh
export PATH=$PATH:$HOME/cm3Libraries/dG3D/release/NonLinearSolver/gmsh

export PYTHONPATH=$PYTHONPATH:$HOME/cm3Libraries/dG3D/release:$HOME/cm3Libraries/dG3D/release/NonLinearSolver/gmsh/utils/wrappers:$HOME/cm3Libraries/dG3D/release/NonLinearSolver/gmsh/utils/wrappers/gmshpy



export DIRNAME=Run_$PBS_JOBID
export SUBDIR=$WORKDIR/$DIRNAME
cd $WORKDIR
mkdir $DIRNAME
cd $TMPDIR
mkdir $DIRNAME
cd $SUBDIR



# export OMPI_MCA_mtl=^psm


cp $PBS_O_WORKDIR/*.msh $SUBDIR/
cp $PBS_O_WORKDIR/*.py $SUBDIR/

echo "subdir=" $SUBDIR
echo $MPI_RUN

mpirun -np 4 python taylorCG.py -temp $TMPDIR/$DIRNAME

echo Finish

