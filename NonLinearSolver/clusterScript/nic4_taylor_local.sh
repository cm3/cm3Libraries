#!/bin/sh

# script example for nic4
#SBATCH --job-name taylorMPI
#SBATCH --mail-user=l.noels@ulg.ac.be
#SBATCH --mail-type=ALL
#SBATCH --output="out.txt"
#SBATCH --ntasks=4
#SBATCH --mem-per-cpu=100
#SBATCH --time=0:30:0


#module load openmpi/qlc/gcc/64/1.6.4
#export LD_PRELOAD="/cm/shared/apps/openmpi/qlc/gcc/64/1.6.4/lib/libmpi.so"
export OMPI_MCA_mtl=^psm

SUBDIR=$HOME/cm3Libraries/dG3D/benchmarks/taylorMPI
echo "subdir=" $SUBDIR

pyfile=taylorCG.py
mshfile=taylor.msh
echo "node list of job"  >> $SUBDIR/output.po$SLURM_JOB_ID

srun hostname >> $SUBDIR/output.po$SLURM_JOB_ID

export PETSC_DIR=$HOME/local/petsc-3.6.4
export PETSC_ARCH=linux-gnu-c-opt
export SLEPC_DIR=$HOME/local/slepc-3.6.3
export SLEPC_ARCH=linux-gnu-c-opt


export PATH=$HOME/local/bin:$PATH
export PATH=$PATH:$HOME/cm3Libraries/dG3D/release
export PATH=$PATH:$HOME/cm3Libraries/dG3D/release/NonLinearSolver/gmsh

export PYTHONPATH=$PYTHONPATH:$HOME/cm3Libraries/dG3D/release:$HOME/cm3Libraries/dG3D/release/NonLinearSolver/gmsh/wrappers

mpirun python $SUBDIR/$pyfile >& $SUBDIR/output.txt

echo -e "\n"






