#!/bin/bash

#SBATCH --job-name=taylorMPI
#
#SBATCH --mail-user=l.noels@ulg.ac.be
#SBATCH --mail-type=ALL
#
#SBATCH --output="taylorMPI.out"

#SBATCH --nodes=2 
#SBATCH --ntasks-per-node=64
#SBATCH --mem-per-cpu=2000
#SBATCH --time=1:00:0
export OMPI_MCA_mtl=^psm


SUBDIR=$HOME/cm3Libraries/dG3D/benchmarks/taylorMPI 
echo "subdir=" $SUBDIR

SCRATCH=$TMPDIR/${USER}_$SLURM_JOB_ID
mkdir -p $SCRATCH || exit $?
echo "workdir=" $SCRATCH
ls $SCRATCH


pyfile=taylorCG.py
mshfile=taylor.msh

echo "node list of job"  >> $SUBDIR/output.po$SLURM_JOB_ID

srun hostname >> $SUBDIR/output.po$SLURM_JOB_ID


cp -f $SUBDIR/$pyfile $SCRATCH/ || exit $?
cp -f $SUBDIR/$mshfile $SCRATCH/ || exit $? 
cd $SCRATCH # needed to work in the tmpscrach dir otherwise work on home !!
ls -artl

export PETSC_DIR=$HOME/local/petsc-3.6.4
export PETSC_ARCH=linux-gnu-c-opt
export SLEPC_DIR=$HOME/local/slepc-3.6.3
export SLEPC_ARCH=linux-gnu-c-opt


export LD_LIBRARY_PATH=$PETSC_DIR/$PETSC_ARCH/lib:$LD_LIBRARY_PATH
export PATH=$PETSC_DIR/$PETSC_ARCH/bin:$PATH

export MPI_HOME=$PETSC_DIR/$PETSC_ARCH/
export MPI_RUN=$MPI_HOME/bin/mpirun

export PATH=$HOME/local/bin:$PATH

export PATH=$PATH:$HOME/cm3Libraries/dG3D/release
export PATH=$PATH:$HOME/cm3Libraries/dG3D/release/NonLinearSolver/gmsh

export PYTHONPATH=$PYTHONPATH:$HOME/cm3Libraries/dG3D/release:$HOME/cm3Libraries/dG3D/debug:$HOME/cm3Libraries/dG3D/debug/NonLinearSolver/gmsh/utils/wrappers:$HOME/cm3Libraries/dG3D/debug/NonLinearSolver/gmsh/utils/wrappers/gmshpy:$HOME/cm3Libraries/dG3D/release/NonLinearSolver/gmsh/utils/wrappers:$HOME/cm3Libraries/dG3D/release/NonLinearSolver/gmsh/utils/wrappers/gmshpy


mpirun -np 128 python  $SCRATCH/$pyfile -temp $SCRATCH -o $SCRATCH/output.txt

echo -e "\n"
echo "delete tmp dir"

sleep 5

cp -f $SCRATCH/* $SUBDIR/  || exit $? 
rm -rf $SCRATCH ||  exit $? 
echo -e "\n"




