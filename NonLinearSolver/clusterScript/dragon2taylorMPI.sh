#!/bin/sh

# script example for dragon2
#SBATCH --job-name taylorMPI
#SBATCH --mail-user=l.noels@ulg.ac.be
#SBATCH --mail-type=ALL
#SBATCH --output="out.txt"
#SBATCH --ntasks=4
#SBATCH --mem-per-cpu=800
#SBATCH --time=0:20:0


module load OpenMPI/3.1.4-GCC-8.3.0

SUBDIR=$HOME/cm3Libraries/dG3D/benchmarks/taylorMPI
echo "subdir=" $SUBDIR

SCRATCH=$GLOBALSCRATCH/${USER}_$SLURM_JOB_ID
echo "workdir=" $SCRATCH


pyfile=taylorCG.py
mshfile=taylor.msh
echo "node list of job"  >> $SUBDIR/output.po$SLURM_JOB_ID

hostname >> $SUBDIR/output.po$SLURM_JOB_ID

cd $GLOBALSCRATCH
mkdir -p ${USER}_$SLURM_JOB_ID || exit $?
cp $SUBDIR/$pyfile $SCRATCH/ || exit $?
cp $SUBDIR/$mshfile $SCRATCH/ || exit $? 

cd $SCRATCH # needed to work in the tmpscrach dir otherwise work on home !!
#ls -artl

export PETSC_DIR=$HOME/local/petsc-3.13.2
export PETSC_ARCH=arch-linux-cxx-opt

export PATH=$HOME/local/bin:$PATH

export PATH=$PATH:$HOME/cm3Libraries/dG3D/release/NonLinearSolver/gmsh


export PYTHONPATH=$PYTHONPATH:$HOME/cm3Libraries/dG3D/release:$HOME/cm3Libraries/dG3D/debug:$HOME/cm3Libraries/dG3D/debug/NonLinearSolver/gmsh/utils/wrappers:$HOME/cm3Libraries/dG3D/debug/NonLinearSolver/gmsh/utils/wrappers/gmshpy:$HOME/cm3Libraries/dG3D/release/NonLinearSolver/gmsh/utils/wrappers:$HOME/cm3Libraries/dG3D/release/NonLinearSolver/gmsh/utils/wrappers/gmshpy


mpirun -np 4 python  $SCRATCH/$pyfile >& $SCRATCH/output.txt

echo -e "\n"

sleep 5

cp -f $SCRATCH/* $SUBDIR/  || exit $? 
rm -rf $SCRATCH ||  exit $? 
echo -e "\n"




