//
//
// Description: operation for vertex
//
// Author:  <Van Dung NGUYEN>, (C) 2019
//
// Copyright: See COPYING file that comes with this distribution
//


#include "vertexGroupOperation.h"
#include "numericalFunctions.h"

void setInt::copyData(const setInt& src)
{
  values.clear();
  values.insert(src.values.begin(),src.values.end());
}
void setInt::insertPhysical(int dim, int phys)
{
  elementGroup g(dim,phys);
  for (elementGroup::vertexContainer::const_iterator it = g.vbegin(); it != g.vend(); it++)
  {
    MVertex* v = it->second;
    insert(v->getNum());
  }
}

void setInt::insert(int i)
{
  values.insert(i);
};

void setInt::clear()
{
  values.clear();
};

bool setInt::included(const int i) const
{
  return values.find(i) != values.end();
}

void linearCombinationOfVertices::setVertex(int verphy, int comp, double fact)
{
  elementGroup g(0,verphy);
  if (g.size() >0)
  {
    MVertex* v = (g.vbegin()->second);
    combinationV[v] = std::pair<int,double>(comp,fact);
  }
  else
  {
    Msg::Error("physical %d has no element",verphy);
  }
}

void linearCombinationOfVertices::clear()
{
  combinationV.clear();
}

vertexGroupOperation_trivial::vertexGroupOperation_trivial(): 
    vertexGroupOperation(){}
vertexGroupOperation_trivial::vertexGroupOperation_trivial(const vertexGroupOperation_trivial& src) : 
    vertexGroupOperation(src){}
vertexGroupOperation_trivial::~vertexGroupOperation_trivial(){}

void vertexGroupOperation_trivial::apply(const MVertex* vmaster, SPoint3& p) const
{
  p[0] = vmaster->x();
  p[1] = vmaster->y();
  p[2] = vmaster->z();
}

vertexGroupOperation_symmetric::vertexGroupOperation_symmetric(double aa, double bb, double cc, double dd): 
    vertexGroupOperation(),
    a(aa),b(bb),c(cc),d(dd){}
vertexGroupOperation_symmetric::vertexGroupOperation_symmetric(const vertexGroupOperation_symmetric& src) : 
    vertexGroupOperation(src), a(src.a),b(src.b),c(src.c),d(src.d){}
vertexGroupOperation_symmetric::~vertexGroupOperation_symmetric(){}

void vertexGroupOperation_symmetric::apply(const MVertex* vmaster, SPoint3& p) const
{
  SPoint3 pp = project(vmaster->point(),a,b,c,d);
  p[0] = 2.*pp[0]-vmaster->x();
  p[1] = 2.*pp[1]-vmaster->y();
  p[2] = 2.*pp[2]-vmaster->z();
}


vertexGroupOperation_periodic::vertexGroupOperation_periodic(double aa, double bb, double cc): 
    vertexGroupOperation(),
    Lx(aa),Ly(bb),Lz(cc){}
    
void vertexGroupOperation_periodic::setPeriodicityVector(double x, double y, double z)
{
  Lx = x; Ly=y; Lz = z;
  Msg::Info("set periodicity %e %e %e",x,y,z);
};

vertexGroupOperation_periodic::vertexGroupOperation_periodic(const vertexGroupOperation_periodic& src) : 
    vertexGroupOperation(src), Lx(src.Lx),Ly(src.Ly),Lz(src.Lz){}
vertexGroupOperation_periodic::~vertexGroupOperation_periodic(){}

void vertexGroupOperation_periodic::apply(const MVertex* vmaster, SPoint3& p) const
{
  p[0] = vmaster->x() - Lx;
  p[1] = vmaster->y() - Ly;
  p[2] = vmaster->z() - Lz;
}