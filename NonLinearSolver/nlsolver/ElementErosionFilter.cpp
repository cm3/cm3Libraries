//
// Author:  <Van Dung NGUYEN>, (C) 2017
//
// Copyright: See COPYING file that comes with this distribution
//
//

#include "ElementErosionFilter.h"
#include "restartManager.h"
#include "ipField.h"

void elementErosionFilter::addErodedElement(MElement* ele, bool interface)
{
  if (_erosionElements.find(ele->getNum()) == _erosionElements.end())
  {
    _erosionElements.insert(ele->getNum());
    _newErosionElements.insert(ele->getNum());
    if (interface)
    {
      _numberOfNewInterfaceElementsEroded ++;
    }
    else
    {
      _numberOfNewBulkElementsEroded++;
    }
  }
};
void elementErosionFilter::nextStep()
{
  resetCounter();
}
void elementErosionFilter::resetToPreviousStep(IPField* ipf)
{
  for (std::set<int>::iterator it = _newErosionElements.begin(); it != _newErosionElements.end(); it++)
  {
    std::set<int>::iterator itF = _erosionElements.find(*it);
    if (itF != _erosionElements.end())
    {
      _erosionElements.erase(itF);
    }
  }
  resetCounter();
};

void elementErosionFilter::restart()
{
  restartManager::restart(_erosionElements);
  restartManager::restart(_numberOfNewBulkElementsEroded);
  restartManager::restart(_numberOfNewInterfaceElementsEroded);
  //print();
}