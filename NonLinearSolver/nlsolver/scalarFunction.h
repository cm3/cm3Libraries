// Description: Derivate class of function with one input, one out out output
//
//
// Author:  <Van Dung NGUYEN>, (C) 2016
//
// Copyright: See COPYING file that comes with this distribution
//
// As this function is templated, it has to be in a separated files of others timeFunction (SWIG generation problem)

#ifndef SCALARFUNCTION_H_
#define SCALARFUNCTION_H_
#ifndef SWIG
#include <vector>
#include "GmshMessage.h"
#include "MElement.h"
#endif //SWIG
class scalarFunction{

 public :
  #ifndef SWIG
  scalarFunction(){}; // time=1 by default to avoid set time for Static linear Scheme

  scalarFunction(const scalarFunction& src){}
  virtual scalarFunction& operator=(const scalarFunction& src){return *this;}
  virtual ~scalarFunction(){};
  virtual void setElement(MElement* el) const{}
  virtual double getVal(const double x) const = 0; // function value at x
  virtual double getDiff(const double x) const = 0; // function derivative at x
  virtual double getDoubleDiff(const double x) const = 0; // function double derivative at x
  virtual double getVal(const std::vector<double>& input) const {return getVal(input[0]);};
  virtual int getInputDimension() const {return 1;};
  virtual void getDiff(const std::vector<double>& input, std::vector<double>& grad) const{
    if (grad.size() != 1)
      grad.resize(1);
    grad[0] = getDiff(input[0]);
  };
  virtual scalarFunction* clone() const = 0;
 #endif
};

class constantScalarFunction : public scalarFunction{
  protected:
    double _f;
  public:
    constantScalarFunction(const double A) : scalarFunction(),_f(A){}
    #ifndef SWIG
    constantScalarFunction(const constantScalarFunction& src) : _f(src._f){}
    virtual constantScalarFunction& operator = (const scalarFunction& src){
      scalarFunction::operator=(src);
      const constantScalarFunction* psrc = dynamic_cast<const constantScalarFunction*>(&src);
      if (psrc!= NULL){
        _f = psrc->_f;
      }
      return *this;
    }


    virtual ~constantScalarFunction(){};
    virtual double getVal(const double x) const {return _f;};
    virtual double getDiff(const double x) const {return 0.;};
    virtual double getDoubleDiff(const double x) const {return 0.;};
    virtual scalarFunction* clone() const {return new constantScalarFunction(*this);};
    #endif // SWIG
};

class elementWiseScalarFunction : public constantScalarFunction{
  protected:
    std::map<int, double> _constantOnElements;
    mutable MElement* _curELe;
  public:
    elementWiseScalarFunction(const double A) : constantScalarFunction(A),_curELe(NULL){}
    void setValueOnElement(const int i, const double val) {_constantOnElements[i] = val;};
    #ifndef SWIG
    elementWiseScalarFunction(const elementWiseScalarFunction& src) : constantScalarFunction(src),_constantOnElements(src._constantOnElements),_curELe(NULL){}
    virtual elementWiseScalarFunction& operator = (const scalarFunction& src){
      constantScalarFunction::operator=(src);
      const elementWiseScalarFunction* psrc = dynamic_cast<const elementWiseScalarFunction*>(&src);
      if (psrc!= NULL){
        _constantOnElements = psrc->_constantOnElements;
        _curELe = NULL;
      }
      return *this;
    }

    virtual void setElement(MElement* el) const {_curELe = el;} // must be acessed during constobject

    virtual ~elementWiseScalarFunction(){};
    virtual double getVal(const double x) const {
      if (!_curELe) return _f;
      std::map<int,double>::const_iterator it = _constantOnElements.find(_curELe->getNum());
      if (it != _constantOnElements.end()) return it->second;
      return _f;
    };
    virtual double getDiff(const double x) const {
      Msg::Error("elementWiseScalarFunction::getDiff should not be used");
      return 0.;
    };
    virtual double getDoubleDiff(const double x) const {      
      Msg::Error("elementWiseScalarFunction::getDoubleDiff should not be used");
      return 0.;
    };
    virtual scalarFunction* clone() const {return new elementWiseScalarFunction(*this);};
    #endif //SWIG
};

class twoValuesFunction : public scalarFunction{
  protected:
    double _x00, _x01; // f = A0 if x00 < x< x01
    double _A0, _A1; // f = A1 if x>x01 or x < x00

  public:
    twoValuesFunction(const double x00, const double x01,const double A0,const double A1):_x00(x00), _x01(x01),_A0(A0),_A1(A1){}
    #ifndef SWIG
    twoValuesFunction(const twoValuesFunction& src): scalarFunction(src),_x00(src._x00),_x01(src._x01),_A0(src._A0),_A1(src._A1){}
    virtual twoValuesFunction& operator = (const scalarFunction& src){
      scalarFunction::operator=(src);
      const twoValuesFunction* psrc = dynamic_cast<const twoValuesFunction*>(&src);
      if (psrc !=NULL){
        _x00 = psrc->_x00;
        _x01 = psrc->_x01;
        _A0 = psrc->_A0;
        _A1 = psrc->_A1;
      }
      return *this;
    }
    virtual ~twoValuesFunction(){}
    virtual double getVal(const double x) const {
      if (x < _x01 and x > _x00) return _A0;
      else return _A1;
    };
    virtual double getDiff(const double x) const {return 0.;};
    virtual double getDoubleDiff(const double x) const {return 0.;};
    virtual scalarFunction* clone() const {return new twoValuesFunction(*this);};
    #endif // SWIG
};

class linearScalarFunction : public scalarFunction{
  protected:
    double _f0, _x0, _a; // f(x)= f0+a*(x-x0)
  public:
    linearScalarFunction(const double x0, const double y0, const double a) : scalarFunction(),_a(a),_x0(x0),_f0(y0){}

    #ifndef SWIG
    linearScalarFunction(const linearScalarFunction& src): scalarFunction(src), _f0(src._f0),_x0(src._x0),_a(src._a){}
    virtual linearScalarFunction& operator=(const scalarFunction& src){
      scalarFunction::operator=(src);
      const linearScalarFunction* psrc = dynamic_cast<const linearScalarFunction*>(&src);
      if (psrc!= NULL){
        _f0 = psrc->_f0;
        _x0 = psrc->_x0;
        _a = psrc->_a;
      }
      return *this;
    }

    virtual ~linearScalarFunction(){};
    virtual double getVal(const double x) const {return _a*(x-_x0)+_f0;};
    virtual double getDiff(const double x) const {return _a;};
    virtual double getDoubleDiff(const double x) const {return 0.;};
    virtual scalarFunction* clone() const {return new linearScalarFunction(*this);};
    #endif // SWIG
};


// FLE 
class GlassTransitionScalarFunction : public scalarFunction{
    protected:
        double _PU, _PR, _Tg, _k, _m;
        int _c;  // c for choice ___=D____
                // P_gibson(T) = 0.5*(P_U + P_R) - 0.5*(P_U - P_R).*tanh(k_prime*(T-Tg))-_m*(T-_Tg);
                // P_huang(T) = P_U - (P_U - P_R)./( ( m.*exp(-m*k*(T-Tg)) + 1 ) ).^(1/m);
    public:
        GlassTransitionScalarFunction(const double PU, const double PR, const double Tg, const double k, const double m, const int c) : scalarFunction(),_PU(PU), _PR(PR), _Tg(Tg), _k(k), _m(m), _c(c){}
        
        #ifndef SWIG
        GlassTransitionScalarFunction(const GlassTransitionScalarFunction& src): scalarFunction(src), _PU(src._PU), _PR(src._PR), _Tg(src._Tg), _k(src._k), _m(src._m), _c(src._c){}
        virtual GlassTransitionScalarFunction& operator =(const scalarFunction& src){
            scalarFunction::operator=(src);
            const GlassTransitionScalarFunction* psrc = dynamic_cast<const GlassTransitionScalarFunction*>(&src);
            if (psrc!= NULL){
                _PU = psrc->_PU;
                _PR = psrc->_PR;
                _Tg = psrc->_Tg;
                _k = psrc->_k;
                _m = psrc->_m;
                _c = psrc->_c;
            }
            return *this;
        }
        
        virtual ~GlassTransitionScalarFunction(){};
        
        // This normalisation needs to be fixed.
        virtual double getVal(const double T) const {
            if (_c == 0) {         // Gibson et al
                return (0.5*(_PU+_PR)-0.5*(_PU-_PR)*tanh(_k*(T-_Tg))-_m*(T-_Tg))/_PU;
            }
            else {       // _c == 1   Huang et al
                return (_PU-(_PU-_PR)/(pow((_m*exp(-_m*_k*(T-_Tg))+1),(1/_m))))/_PU;
            }
        }; 
        virtual double getDiff(const double T) const {
            if (_c == 0) {         // Gibson et al
                // return ( 0.5*_k*(_PR-_PU)*(1-pow(tanh(_k*(T-_Tg)),2))-_m)/_PU;
                return 0.5*_k*(_PR-_PU)*pow( 1/cosh(_k*(T-_Tg)) ,2);
            }
            else {       // _c == 1   Huang et al
                // return (-(_PU-_PR)*(pow(_m,2.0)*_k*exp(-_m*_k*(T-_Tg)))/( _m* ( pow( (_m*exp(-_m*_k*(T-_Tg))+1),(1+1/_m) ) ) ))/_PU;
                return _m*_k*(_PR-_PU)*exp(-_m*_k*(T-_Tg) )* pow( (_m*exp(-_m*_k*(T-_Tg) ) +1 ), (-(1+_m)/_m) );
            }
        };
        virtual double getDoubleDiff(const double T) const {
            if (_c == 0) {         // Gibson et al
                return  pow(_k,2)*(_PU-_PR)*tanh(_k*(T-_Tg))* pow(1/cosh(_k*(T-_Tg)),2);
            }
            else {       // _c == 1   Huang et al
                return ( pow(_k*_m,2)*(_PU-_PR)* (exp(_k*_m*(T-_Tg))-1)*pow( _m*exp(_k*_m*(_Tg-T)) + 1, -1/_m) )/ (pow(  exp(_k*_m*(T-_Tg))+_m ,2)) ;
            }
        };
            
        virtual scalarFunction* clone() const {return new GlassTransitionScalarFunction(*this);};      
        #endif // SWIG
    };


class piecewiseScalarFunction : public scalarFunction{
protected:
    std::vector<double> _var;
    std::vector<double> _f;
  public:
    piecewiseScalarFunction() : scalarFunction(),_var(),_f(){}
    void put(const double x, const double f){
        _var.push_back(x);
        _f.push_back(f);
    }
    #ifndef SWIG
    piecewiseScalarFunction(const piecewiseScalarFunction& src): scalarFunction(src), _var(src._var),_f(src._f){}
    virtual piecewiseScalarFunction& operator=(const scalarFunction& src){
      scalarFunction::operator=(src);
      const piecewiseScalarFunction* psrc = dynamic_cast<const piecewiseScalarFunction*>(&src);
      if (psrc!= NULL){
        _var = psrc->_var;
        _f = psrc->_f;
      }
      return *this;
    }

    virtual ~piecewiseScalarFunction(){};
    virtual double getVal(const double x) const {
      int n = _var.size();
      if (x <= _var[0]) return _f[0];
      if (x >= _var[n-1]) return _f[n-1];

      double x0(0.), x1(0.), f0(0.), f1(0.);
      for (int i=0; i<n-1; i++){
        if (_var[i]<=x and x< _var[i+1]){
          x0 = _var[i]; f0 = _f[i];
          x1 = _var[i+1]; f1 = _f[i+1];
          return (x-x0)*(f1-f0)/(x1-x0) + f0;
        }
      }
      return 0.;
    };
    virtual double getDiff(const double x) const {
      int n = _var.size();
      if (x < _var[0]) return 0.;
      if (x > _var[n-1]) return 0.;

      double x0(0.), x1(0.), f0(0.), f1(0.);
      for (int i=0; i<n-1; i++){
        if (_var[i]<=x and x< _var[i+1]){
          x0 = _var[i]; f0 = _f[i];
          x1 = _var[i+1]; f1 = _f[i+1];
          return (f1-f0)/(x1-x0);
        }
      }
      return 0.;
    };
    virtual double getDoubleDiff(const double x) const {return 0.;};
    virtual scalarFunction* clone() const {return new piecewiseScalarFunction(*this);};
    #endif // SWIG
};

class twoVariableExponentialSaturationScalarFunction : public scalarFunction{
  protected:
    double _f1, _f2;
    bool _upperBound; // if upper bound  if f1 < f2, lower bound if f1>f2,

  public:
    twoVariableExponentialSaturationScalarFunction(const double f1, const double f2, const bool ub = true): scalarFunction(),_f1(f1),_f2(f2),_upperBound(ub){};
    #ifndef SWIG
    twoVariableExponentialSaturationScalarFunction(const twoVariableExponentialSaturationScalarFunction& src): scalarFunction(src),_f1(src._f1),_f2(src._f2),
          _upperBound(src._upperBound){}
    virtual ~twoVariableExponentialSaturationScalarFunction(){};
    virtual scalarFunction& operator=(const scalarFunction& src){
      scalarFunction::operator =(src);
      const twoVariableExponentialSaturationScalarFunction* psrc = dynamic_cast<const twoVariableExponentialSaturationScalarFunction*>(&src);
      if (psrc != NULL){
        _f1 = psrc->_f1;
        _f2 = psrc->_f2;
        _upperBound = psrc->_upperBound;
      }
      return *this;
    }

    virtual double getVal(const double x) const {
      if (_upperBound){
        if (x < _f1) return x;
        else {
          double fact = (x - _f1)/(_f2-_f1);
          return _f1 + (_f2-_f1)*(1. - exp(-fact));
        }
      }
      else{
        if (x > _f1) return x;
        else {
          double fact = (x - _f1)/(_f2-_f1);
          return _f1 + (_f2-_f1)*(1. - exp(-fact));
        }
      }
    };
    virtual double getDiff(const double x) const {
      if (_upperBound){
        if (x < _f1)
          return 1.;
        else {
          double fact = (x - _f1)/(_f2-_f1);
          return exp(-fact);
        }
      }
      else{
        if (x > _f1){
          return 1.;
        }
        else{
          double fact = (x - _f1)/(_f2-_f1);
          return exp(-fact);
        }
      }
    };
    virtual double getDoubleDiff(const double x) const {return 0.;};
    virtual scalarFunction* clone() const {return new twoVariableExponentialSaturationScalarFunction(*this);};
    #endif //SWIG
};




class fourVariableExponentialSaturationScalarFunction : public scalarFunction{
protected:
    // Lower and upper bounded saturation function (with two exponentials)
    double _f1min, _f2min, _f1max, _f2max;  // _f2min < f1min < f1max < f2max

  public:
    fourVariableExponentialSaturationScalarFunction(const double f1min, const double f2min, const double f1max, const double f2max): scalarFunction(),
        _f1min(f1min),_f2min(f2min), _f1max(f1max), _f2max(f2max)
    {
      if ((_f1min <= _f2min) or (_f1max >= _f2max) or (_f1min > _f1max)){
        Msg::Error("fourVariableExponentialSaturationScalarFunction::constructor: wrong parameter values");
      }
    };
    #ifndef SWIG
    fourVariableExponentialSaturationScalarFunction(const fourVariableExponentialSaturationScalarFunction& src): scalarFunction(src),
        _f1min(src._f1min),_f2min(src._f2min), _f1max(src._f1max), _f2max(src._f2max){};
    virtual ~fourVariableExponentialSaturationScalarFunction(){};
    virtual scalarFunction& operator=(const scalarFunction& src)
    {
      scalarFunction::operator =(src);
      const fourVariableExponentialSaturationScalarFunction* psrc = dynamic_cast<const fourVariableExponentialSaturationScalarFunction*>(&src);
      if (psrc != NULL){
        _f1min = psrc->_f1min;
        _f2min = psrc->_f2min;
        _f1max = psrc->_f1max;
        _f2max = psrc->_f2max;
      }
      return *this;
    }

    virtual double getVal(const double x) const{
      if (x < _f1min)
      {
        // Lower bound
        double fact = (x - _f1min)/(_f2min-_f1min);
        return _f1min + (_f2min-_f1min)*(1. - exp(-fact));
      }
      else if (x > _f1max)
      {
        // Upper bound
        double fact = (x - _f1max)/(_f2max-_f1max);
        return _f1max + (_f2max-_f1max)*(1. - exp(-fact));
      }
      else
      {
        // Between both bounds
        return x;
      }
    };


    virtual double getDiff(const double x) const {
      if (x < _f1min)
      {
        // Lower bound
        double fact = (x - _f1min)/(_f2min-_f1min);
        return exp(-fact);
      }
      else if (x > _f1max)
      {
        // Upper bound
        double fact = (x - _f1max)/(_f2max-_f1max);
        return exp(-fact);
      }
      else
      {
        // Between both bounds
        return 1.;
      }
    };
    virtual double getDoubleDiff(const double x) const {return 0.;};
    
    virtual scalarFunction* clone() const {return new fourVariableExponentialSaturationScalarFunction(*this);};
    #endif //SWIG
};







class polynomialScalarFunction : public scalarFunction{
  protected:
    int _order;
    std::vector<double> _coeffs;

  public:
    polynomialScalarFunction(const int order) : scalarFunction(),_order(order),_coeffs(order+1){}
    void setCoefficient(const int index, const double val){ _coeffs[index] = val;};
    #ifndef SWIG
    polynomialScalarFunction(const polynomialScalarFunction& src): scalarFunction(src),_order(src._order),_coeffs(src._coeffs){}
    virtual polynomialScalarFunction& operator=(const scalarFunction& src){
      scalarFunction::operator=(src);
      const polynomialScalarFunction* psrc = dynamic_cast<const polynomialScalarFunction*>(&src);
      if (psrc!= NULL){
        _order = psrc->_order;
        _coeffs = psrc->_coeffs;
      }
      return *this;
    }

    virtual ~polynomialScalarFunction(){};
    virtual double getVal(const double x) const {
      if (_order == 0) return _coeffs[0];
      else if (_order == 1) return _coeffs[0]+_coeffs[1]*x;
      else if (_order == 2) return _coeffs[0]+_coeffs[1]*x+_coeffs[2]*x*x;
      else if (_order == 3) return _coeffs[0]+_coeffs[1]*x+_coeffs[2]*x*x+_coeffs[3]*x*x*x;
      else Msg::Error("polynomialScalarFunction with order %d has not been implemented",_order);
			return 0.;
    };
    virtual double getDiff(const double x) const {
      if (_order == 0) return 0.;
      else if (_order == 1) return _coeffs[1];
      else if (_order == 2) return _coeffs[1]+2.*_coeffs[2]*x;
      else if (_order == 3) return _coeffs[1]+2.*_coeffs[2]*x+3*_coeffs[3]*x*x;
      else Msg::Error("polynomialScalarFunction with order %d has not been implemented",_order);
			return 0.;
    };
    virtual double getDoubleDiff(const double x) const {return 0.;};
    virtual scalarFunction* clone() const {return new polynomialScalarFunction(*this);};
    #endif //SWIG
};

class asinwt : public scalarFunction{
  protected:
    double _a, _omega;
  public:
    asinwt(const double a, const double w) : scalarFunction(),_a(a),_omega(w){}
    #ifndef SWIG
    asinwt(const asinwt& src) : _a(src._a),_omega(src._omega){}
    virtual asinwt& operator = (const scalarFunction& src){
      scalarFunction::operator=(src);
      const asinwt* psrc = dynamic_cast<const asinwt*>(&src);
      if (psrc!= NULL){
        _a = psrc->_a;
        _omega = psrc->_omega;
      }
      return *this;
    }


    virtual ~asinwt(){};
    virtual double getVal(const double x) const {return _a*sin(_omega*x);};
    virtual double getDiff(const double x) const {return _a*_omega*cos(_omega*x);};
    virtual double getDoubleDiff(const double x) const {return 0.;};
    virtual scalarFunction* clone() const {return new asinwt(*this);};
    #endif // SWIG
};

// function y = A*exp(-0.5*(x/xc)^n)
class aexpMinusXpoweredByn : public scalarFunction
{
  protected:
    double _a, _xc, _n;
    
  public:
    aexpMinusXpoweredByn(double a, double xc, double n) : scalarFunction(),_a(a),_xc(xc),_n(n){}
    #ifndef SWIG
    aexpMinusXpoweredByn(const aexpMinusXpoweredByn& src) : _a(src._a),_xc(src._xc),_n(src._n){}
    virtual aexpMinusXpoweredByn& operator = (const scalarFunction& src){
      scalarFunction::operator=(src);
      const aexpMinusXpoweredByn* psrc = dynamic_cast<const aexpMinusXpoweredByn*>(&src);
      if (psrc!= NULL){
        _a = psrc->_a;
        _xc = psrc->_xc;
        _n = psrc->_n;
      }
      return *this;
    }
    virtual ~aexpMinusXpoweredByn(){};
    virtual double getVal(const double x) const {
      double r = x/_xc;
      double pr = 0.5*pow(r,_n);
      return _a*exp(-pr);
    };
    virtual double getDiff(const double x) const {
      double r = x/_xc;
      double DrDx = 1./_xc;
      double pr = 0.5*pow(r,_n);
      double DprDx = _n*0.5*pow(r,_n-1.)*DrDx;
      return -DprDx*_a*exp(-pr);
    };
    virtual double getDoubleDiff(const double x) const {return 0.;};
    virtual scalarFunction* clone() const {return new aexpMinusXpoweredByn(*this);};
    #endif // SWIG
};

// function y = exp(- (_C1*(T - _Tref)) / (_C2 + T - _Tref) ) 
// NOTE: this is not normalized
class negExpWLFshiftFactor : public scalarFunction
{
  protected:
    double _C1, _C2, _Tref;
    
  public:
    negExpWLFshiftFactor(double C1, double C2, double Tref) : scalarFunction(),_C1(C1),_C2(C2),_Tref(Tref){}
    #ifndef SWIG
    negExpWLFshiftFactor(const negExpWLFshiftFactor& src) : _C1(src._C1),_C2(src._C2),_Tref(src._Tref){}
    virtual negExpWLFshiftFactor& operator = (const scalarFunction& src){
      scalarFunction::operator=(src);
      const negExpWLFshiftFactor* psrc = dynamic_cast<const negExpWLFshiftFactor*>(&src);
      if (psrc!= NULL){
        _C1 = psrc->_C1;
        _C2 = psrc->_C2;
        _Tref = psrc->_Tref;
      }
      return *this;
    }
    virtual ~negExpWLFshiftFactor(){};
    virtual double getVal(const double x) const {
      return exp(-(_C1*(x - _Tref)) / (_C2 + x - _Tref));
    };
    virtual double getDiff(const double x) const {
      return (this->getVal(x))*(-_C1*_C2)/pow( (_C2 + x-_Tref) ,2);
    };
    virtual double getDoubleDiff(const double x) const {
      return (this->getVal(x)) * (_C1*_C2) * (_C1*_C2 - 2*(_C2+ x-_Tref)) / pow( (_C2 + x-_Tref) ,4);
    };
    virtual scalarFunction* clone() const {return new negExpWLFshiftFactor(*this);};
    #endif // SWIG
};

// function y = exp(- C(x-Tref) ) 
class negativeExponentialFunction : public scalarFunction
{
  protected:
    double _C, _Tref;
    
  public:
    negativeExponentialFunction(double C, double Tref) : scalarFunction(),_C(C),_Tref(Tref){}
    #ifndef SWIG
    negativeExponentialFunction(const negativeExponentialFunction& src) : _C(src._C),_Tref(src._Tref){}
    virtual negativeExponentialFunction& operator = (const scalarFunction& src){
      scalarFunction::operator=(src);
      const negativeExponentialFunction* psrc = dynamic_cast<const negativeExponentialFunction*>(&src);
      if (psrc!= NULL){
        _C = psrc->_C;
        _Tref = psrc->_Tref;
      }
      return *this;
    }
    virtual ~negativeExponentialFunction(){};
    virtual double getVal(const double x) const {
      return exp(-_C*(x-_Tref));
    };
    virtual double getDiff(const double x) const {
      return (-_C*exp(-_C*(x-_Tref)));
    };
    virtual double getDoubleDiff(const double x) const {
      return (pow(_C,2)*exp(-_C*(x-_Tref)));
    };
    virtual scalarFunction* clone() const {return new negativeExponentialFunction(*this);};
    #endif // SWIG
};


#endif // SCALARFUNCTION_H_

