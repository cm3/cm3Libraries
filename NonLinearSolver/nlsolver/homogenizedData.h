//
//
// Description: store homogenized data
//
// Author:  <Van Dung NGUYEN>, (C) 2013
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef HOMOGENIZEDDATA_H_
#define HOMOGENIZEDDATA_H_

#include "dofManager.h"
#include <map>
#include "STensor3.h"
#include "STensor33.h"
#include "STensor43.h"
#include "STensor53.h"
#include "STensor63.h"

#include "highOrderTensor.h"

// brief notations
// u denot mechanics field gradu=F, stress = P, gradF=G, second-order Stress = Q
// T  denotes constitutive extraField, gradT, fluxT
// V denots non-constitutive extraField, gradV, fluxV

// all homogenized quantities conjugated to kinematic variables F, G, gradT, T, gradV
// P, Q, fluxT, fluxV, mecaSourceT
// fieldCapacity, _internalEnergyExtraDof are assumed to be constant in current time step

class nonLinearMechSolver;

class homogenizedData {
	friend class homogenizedDataFiles;
  private:
    STensor3   P;
    STensor33  Q;
    SVector3   BM;
    
		// for constitutive extraDof
    std::vector<SVector3> fluxT; // flux
    std::vector<double>  eT; // internal energy
    std::vector<double>  srcT; // mechanical source convert to heat
    std::vector<double> 	Cp; // field capacity


		// for non-constitutive extraDof
		std::vector<SVector3> fluxV; // flux

		// tangent
		STensor43 dPdF;
		STensor53 dPdG;
		std::vector<STensor33> dPdgradT;
		std::vector<STensor3> dPdT;
		std::vector<STensor33> dPdgradV;


		STensor53 dQdF;
		STensor63 dQdG;
		STensor63 dQdG_correction;
		std::vector<STensor43> dQdgradT;
		std::vector<STensor33> dQdT;
		std::vector<STensor43> dQdgradV;


		std::vector<STensor33> dfluxTdF;
		std::vector<STensor43> dfluxTdG;
		std::vector<std::vector<STensor3> > dfluxTdgradT;
		std::vector<std::vector<SVector3> > dfluxTdT;
		std::vector<std::vector<STensor3> > dfluxTdgradV;

		std::vector<STensor33> dfluxVdF;
		std::vector<STensor43> dfluxVdG;
		std::vector<std::vector<STensor3> > dfluxVdgradT;
		std::vector<std::vector<SVector3> > dfluxVdT;
		std::vector<std::vector<STensor3> > dfluxVdgradV;

		std::vector<STensor3> dsrcTdF;
		std::vector<STensor33> dsrcTdG;
		std::vector<std::vector<SVector3> > dsrcTdgradT;
		std::vector<std::vector<double> > dsrcTdT;
		std::vector<std::vector<SVector3> > dsrcTdgradV;

		bool cohesiveLawExtraction;

		SVector3 _cohesiveJump; // cohesive jump
    SVector3 _cohesiveTraction; // cohesive traction
    double _lostSolutionUniquenssCriterion;

    double _damageVolume;
    STensor3 _Fdam; // deformation average over active damage zone
    STensor3 _stressDam;
    double _averageCohesiveBandWidth;

		STensor33 dCohesivejumpdF;
		STensor43 dCohesivejumpdG;
		std::vector<STensor3> dCohesivejumpdgradT;
		std::vector<SVector3> dCohesivejumpdT;
		std::vector<STensor3> dCohesivejumpdgradV;

		STensor43 dFdamdF;
		STensor53 dFdamdG;
		std::vector<STensor33> dFdamdgradT;
		std::vector<STensor3> dFdamdT;
		std::vector<STensor33> dFdamdgradV;

		double deformationEnergy;
		double plasticEnergy;
		double irreversibleEnergy; // irreversible energy

		bool irreversibleEnergyExtraction;
		STensor3 dIrreversibleEnergydF; // dirreversible energy with respect to deformation gradient
		STensor33 dIrreversibleEnergydG;
		std::vector<SVector3> dIrreversibleEnergydgradT;
		std::vector<double> dIrreversibleEnergydT;
		std::vector<SVector3> dIrreversibleEnergydgradV;

    std::map<Dof, STensor3> dUdF;
    std::map<Dof, STensor33> dUdG;

  public:
    homogenizedData(const nonLinearMechSolver* solver, const bool failureExtr, const bool irrEnergExtract);
    homogenizedData(const homogenizedData& src);
    homogenizedData& operator = (const homogenizedData& src);

		bool getIrreversibleEnergyExtractionFlag() const {return irreversibleEnergyExtraction;};
		bool getCohesiveLawExtractionFlag() const {return cohesiveLawExtraction;};

    virtual homogenizedData* clone() const {return new homogenizedData(*this);};

		double& getDeformationEnergy() {return deformationEnergy;};
		const double& getDeformationEnergy() const {return deformationEnergy;};

		double& getPlasticEnergy() {return plasticEnergy;};
		const double& getPlasticEnergy() const {return plasticEnergy;};

		double& getIrreversibleEnergy(){return irreversibleEnergy;};
		const double& getIrreversibleEnergy() const{return irreversibleEnergy;};

		STensor3& getHomogenizedTangentOperator_IrreversibleEnergy_F() {return dIrreversibleEnergydF;};
		const STensor3& getHomogenizedTangentOperator_IrreversibleEnergy_F() const {return dIrreversibleEnergydF;};
		STensor33& getHomogenizedTangentOperator_IrreversibleEnergy_G() {return dIrreversibleEnergydG;};
		const STensor33& getHomogenizedTangentOperator_IrreversibleEnergy_G() const {return dIrreversibleEnergydG;};
		SVector3& getHomogenizedTangentOperator_IrreversibleEnergy_gradT(const int index) {return dIrreversibleEnergydgradT[index];}
		const SVector3& getHomogenizedTangentOperator_IrreversibleEnergy_gradT(const int index) const {return dIrreversibleEnergydgradT[index];}
		double& getHomogenizedTangentOperator_IrreversibleEnergy_T(const int index) {return dIrreversibleEnergydT[index];}
		const double& getHomogenizedTangentOperator_IrreversibleEnergy_T(const int index) const {return dIrreversibleEnergydT[index];}
		SVector3& getHomogenizedTangentOperator_IrreversibleEnergy_gradV(const int index) {return dIrreversibleEnergydgradV[index];};
		const SVector3& getHomogenizedTangentOperator_IrreversibleEnergy_gradV(const int index) const {return dIrreversibleEnergydgradV[index];};

		// Mechanical quantities
		STensor3& getHomogenizedStress(){return P;};
		const STensor3& getHomogenizedStress() const {return P;};
		
		// all tangents
		STensor43& getHomogenizedTangentOperator_F_F() {return dPdF;};
		const STensor43& getHomogenizedTangentOperator_F_F() const  {return dPdF;};
		STensor53& getHomogenizedTangentOperator_F_G() {return dPdG;};
		const STensor53& getHomogenizedTangentOperator_F_G() const {return dPdG;};
		STensor33& getHomogenizedTangentOperator_F_gradT(const int index){ return dPdgradT[index];}
		const STensor33& getHomogenizedTangentOperator_F_gradT(const int index) const{ return dPdgradT[index];}
		STensor3& getHomogenizedTangentOperator_F_T(const int index){return dPdT[index];}
		const STensor3& getHomogenizedTangentOperator_F_T(const int index) const{return dPdT[index];};
		// meca-nonconExtraDof coupling
		STensor33& getHomogenizedTangentOperator_F_gradV(const int index) {return dPdgradV[index];}
		const STensor33& getHomogenizedTangentOperator_F_gradV(const int index) const{return dPdgradV[index];}
		//
		STensor33& getHomogenizedSecondOrderStress() {return Q;};
		const STensor33& getHomogenizedSecondOrderStress() const {return Q;};
		SVector3& getHomogenizedBodyForce() {return BM;};
		const SVector3& getHomogenizedBodyForce() const {return BM;};
		
		STensor53& getHomogenizedTangentOperator_G_F() {return dQdF;};
		const STensor53& getHomogenizedTangentOperator_G_F() const {return dQdF;};
		STensor63& getHomogenizedTangentOperator_G_G() {return dQdG;};
		const STensor63& getHomogenizedTangentOperator_G_G() const {return dQdG;};
		STensor63& getHomogenizedCorrectionTerm_G_G() {return dQdG_correction;};
		const STensor63& getHomogenizedCorrectionTerm_G_G() const {return dQdG_correction;};

		STensor43& getHomogenizedTangentOperator_G_gradT(const int index) {return dQdgradT[index];}
		const STensor43& getHomogenizedTangentOperator_G_gradT(const int index) const{return dQdgradT[index];}
		STensor33& getHomogenizedTangentOperator_G_T(const int index) {return dQdT[index];}
		const STensor33& getHomogenizedTangentOperator_G_T(const int index) const {return dQdT[index];}
		STensor43& getHomogenizedTangentOperator_G_gradV(const int index) {return dQdgradV[index];}
		const STensor43& getHomogenizedTangentOperator_G_gradV(const int index) const {return dQdgradV[index];}

		SVector3& getHomogenizedCohesiveJump() {return _cohesiveJump;};
		const SVector3& getHomogenizedCohesiveJump() const {return _cohesiveJump;};

		SVector3& getHomogenizedCohesiveTraction() {return _cohesiveTraction;};
		const SVector3& getHomogenizedCohesiveTraction() const {return _cohesiveTraction;};

		double& getLostOfSolutionUniquenessCriterion() {return _lostSolutionUniquenssCriterion;};
		const double& getLostOfSolutionUniquenessCriterion() const {return _lostSolutionUniquenssCriterion;};

		double& getActiveDissipationVolume() {return _damageVolume;};
		const double& getActiveDissipationVolume() const{return _damageVolume;};

		STensor3& getHomogenizedActiveDissipationDeformationGradient() {return _Fdam;};
		const STensor3& getHomogenizedActiveDissipationDeformationGradient() const {return _Fdam;};

		STensor3& getHomogenizedActiveDissipationStress() {return _stressDam;};
		const STensor3& getHomogenizedActiveDissipationStress() const {return _stressDam;};

		double& getAverageLocalizationBandWidth() {return _averageCohesiveBandWidth;}
		const double& getAverageLocalizationBandWidth() const {return _averageCohesiveBandWidth;}

		STensor33& getHomogenizedTangentOperator_CohesiveJump_F() {return dCohesivejumpdF;};
		const STensor33& getHomogenizedTangentOperator_CohesiveJump_F() const {return dCohesivejumpdF;};
		STensor43& getHomogenizedTangentOperator_CohesiveJump_G() {return dCohesivejumpdG;};
		const STensor43& getHomogenizedTangentOperator_CohesiveJump_G() const {return dCohesivejumpdG;};
		STensor3& getHomogenizedTangentOperator_CohesiveJump_gradT(const int index) {return dCohesivejumpdgradT[index];}
		const STensor3& getHomogenizedTangentOperator_CohesiveJump_gradT(const int index) const {return dCohesivejumpdgradT[index];}
		SVector3& getHomogenizedTangentOperator_CohesiveJump_T(const int index) {return dCohesivejumpdT[index];}
		const SVector3& getHomogenizedTangentOperator_CohesiveJump_T(const int index) const {return dCohesivejumpdT[index];}
		STensor3& getHomogenizedTangentOperator_CohesiveJump_gradV(const int index) {return dCohesivejumpdgradV[index];};
		const STensor3& getHomogenizedTangentOperator_CohesiveJump_gradV(const int index) const {return dCohesivejumpdgradV[index];};

		STensor43& getHomogenizedTangentOperator_ActiveDissipationDeformationGradient_F(){return dFdamdF;};
		const STensor43& getHomogenizedTangentOperator_ActiveDissipationDeformationGradient_F() const{return dFdamdF;};
		STensor53& getHomogenizedTangentOperator_ActiveDissipationDeformationGradient_G(){return dFdamdG;};
		const STensor53& getHomogenizedTangentOperator_ActiveDissipationDeformationGradient_G() const {return dFdamdG;};
		STensor33& getHomogenizedTangentOperator_ActiveDissipationDeformationGradient_gradT(const int index){return dFdamdgradT[index];};
		const STensor33& getHomogenizedTangentOperator_ActiveDissipationDeformationGradient_gradT(const int index) const{return dFdamdgradT[index];};
		STensor3& getHomogenizedTangentOperator_ActiveDissipationDeformationGradient_T(const int index){return dFdamdT[index];};
		const STensor3& getHomogenizedTangentOperator_ActiveDissipationDeformationGradient_T(const int index) const{return dFdamdT[index];};
		STensor33& getHomogenizedTangentOperator_ActiveDissipationDeformationGradient_gradV(const int index) {return dFdamdgradV[index];};
		const STensor33& getHomogenizedTangentOperator_ActiveDissipationDeformationGradient_gradV(const int index) const{return dFdamdgradV[index];};

		// constitutive extraDof
		SVector3& getHomogenizedConstitutiveExtraDofFlux(const int index){return fluxT[index];};
		const SVector3& getHomogenizedConstitutiveExtraDofFlux(const int index) const{return fluxT[index];};

		// tangent conExtraDof-conExtraDof
		STensor3& getHomogenizedTangentOperator_gradT_gradT(const int fluxIndex, const int fieldIndex){return dfluxTdgradT[fluxIndex][fieldIndex];};
		const STensor3& getHomogenizedTangentOperator_gradT_gradT(const int fluxIndex, const int fieldIndex) const {return dfluxTdgradT[fluxIndex][fieldIndex];};
		SVector3& getHomogenizedTangentOperator_gradT_T(const int fluxIndex, const int fieldIndex) {return dfluxTdT[fluxIndex][fieldIndex];};
		const SVector3& getHomogenizedTangentOperator_gradT_T(const int fluxIndex, const int fieldIndex) const{return dfluxTdT[fluxIndex][fieldIndex];};

		// tangent conExtraDof-mechanic
		STensor33& getHomogenizedTangentOperator_gradT_F(const int fluxIndex) {return dfluxTdF[fluxIndex];};
		const STensor33& getHomogenizedTangentOperator_gradT_F(const int fluxIndex) const {return dfluxTdF[fluxIndex];};
		STensor43& getHomogenizedTangentOperator_gradT_G(const int fluxIndex){return dfluxTdG[fluxIndex];};
		const STensor43& getHomogenizedTangentOperator_gradT_G(const int fluxIndex) const {return dfluxTdG[fluxIndex];};

		// tangent conExtraDof non-constitutive extraDof
		STensor3& getHomogenizedTangentOperator_gradT_gradV(const int fluxIndex, const int fieldIndex) {return dfluxTdgradV[fluxIndex][fieldIndex];};
		const STensor3& getHomogenizedTangentOperator_gradT_gradV(const int fluxIndex, const int fieldIndex) const {return dfluxTdgradV[fluxIndex][fieldIndex];};

		double& getHomogenizedConstitutiveExtraDofMechanicalSource(const int index) {return srcT[index];};
		const double& getHomogenizedConstitutiveExtraDofMechanicalSource(const int index) const {return srcT[index];};

		// tangent conExtraDof-conExtraDof
		SVector3& getHomogenizedTangentOperator_ConstitutiveExtraDofMechanicalSource_gradT(const int fluxIndex, const int fieldIndex) {return dsrcTdgradT[fluxIndex][fieldIndex];}
		const SVector3& getHomogenizedTangentOperator_ConstitutiveExtraDofMechanicalSource_gradT(const int fluxIndex, const int fieldIndex) const{return dsrcTdgradT[fluxIndex][fieldIndex];}
		double& getHomogenizedTangentOperator_ConstitutiveExtraDofMechanicalSource_T(const int fluxIndex, const int fieldIndex){return dsrcTdT[fluxIndex][fieldIndex];}
		const double& getHomogenizedTangentOperator_ConstitutiveExtraDofMechanicalSource_T(const int fluxIndex, const int fieldIndex) const{return dsrcTdT[fluxIndex][fieldIndex];}

		// tangent conExtraDof-meca
		STensor3& getHomogenizedTangentOperator_ConstitutiveExtraDofMechanicalSource_F(const int fluxIndex){return dsrcTdF[fluxIndex];};
		const STensor3& getHomogenizedTangentOperator_ConstitutiveExtraDofMechanicalSource_F(const int fluxIndex) const {return dsrcTdF[fluxIndex];};
		STensor33& getHomogenizedTangentOperator_ConstitutiveExtraDofMechanicalSource_G(const int fluxIndex) {return dsrcTdG[fluxIndex];};
		const STensor33& getHomogenizedTangentOperator_ConstitutiveExtraDofMechanicalSource_G(const int fluxIndex) const {return dsrcTdG[fluxIndex];};

		// tangent conExtraDof- nonConExtraDof
		SVector3& getHomogenizedTangentOperator_ConstitutiveExtraDofMechanicalSource_gradV(const int fluxIndex, const int fieldIndex) {return dsrcTdgradV[fluxIndex][fieldIndex];}
		const SVector3& getHomogenizedTangentOperator_ConstitutiveExtraDofMechanicalSource_gradV(const int fluxIndex, const int fieldIndex) const{return dsrcTdgradV[fluxIndex][fieldIndex];}


		double& getHomogenizedConstitutiveExtraDofInternalEnergy(const int index) {return eT[index];}
		const double& getHomogenizedConstitutiveExtraDofInternalEnergy(const int index) const {return eT[index];}

		double& getHomogenizedConstitutiveExtraDofFieldCapacityPerUnitField(const int index) {return Cp[index];};
		const double& getHomogenizedConstitutiveExtraDofFieldCapacityPerUnitField(const int index) const {return Cp[index];};

		//nonConExtraDof
		SVector3& getHomogenizedNonConstitutiveExtraDofFlux(const int index) {return fluxV[index];};
		const SVector3& getHomogenizedNonConstitutiveExtraDofFlux(const int index) const {return fluxV[index];};

		// tangent nonConExtraDof-nonConExtraDof
		STensor3& getHomogenizedTangentOperator_gradV_gradV(const int fluxIndex, const int fieldIndex) {return dfluxVdgradV[fluxIndex][fieldIndex];}
		const STensor3& getHomogenizedTangentOperator_gradV_gradV(const int fluxIndex, const int fieldIndex) const {return dfluxVdgradV[fluxIndex][fieldIndex];}

		// tangent nonConExtraDof-meca
		STensor33& getHomogenizedTangentOperator_gradV_F(const int fluxIndex) {return dfluxVdF[fluxIndex];}
		const STensor33& getHomogenizedTangentOperator_gradV_F(const int fluxIndex) const {return dfluxVdF[fluxIndex];}
		STensor43& getHomogenizedTangentOperator_gradV_G(const int fluxIndex) {return dfluxVdG[fluxIndex];}
		const STensor43& getHomogenizedTangentOperator_gradV_G(const int fluxIndex) const {return dfluxVdG[fluxIndex];}

		// tangent nonConExtraDof-ConExtraDof
		STensor3& getHomogenizedTangentOperator_gradV_gradT(const int fluxIndex, const int fieldIndex) {return dfluxVdgradT[fluxIndex][fieldIndex];}
		const STensor3& getHomogenizedTangentOperator_gradV_gradT(const int fluxIndex, const int fieldIndex) const {return dfluxVdgradT[fluxIndex][fieldIndex];}

		SVector3& getHomogenizedTangentOperator_gradV_T(const int fluxIndex, const int fieldIndex){return dfluxVdT[fluxIndex][fieldIndex];}

		const SVector3& getHomogenizedTangentOperator_gradV_T(const int fluxIndex, const int fieldIndex) const{return dfluxVdT[fluxIndex][fieldIndex];}
	        std::map<Dof, STensor3>& getdUnknowndF() {return dUdF;};
	        std::map<Dof, STensor33>& getdUnknowndG() {return dUdG;};
};

class allFirstOrderMechanicsFiles{
	public:
		FILE	*F_File,
					*P_File,
					*dPdF_File,
					*defoEnerg_File,
					*plasticEnerg_File,
					*irreversibleEnergFile,
					*lostSolutionUniquenssCriterion_File;
	allFirstOrderMechanicsFiles():F_File(NULL),P_File(NULL),dPdF_File(NULL),defoEnerg_File(NULL),plasticEnerg_File(NULL),
	irreversibleEnergFile(NULL),lostSolutionUniquenssCriterion_File(NULL){};
	~allFirstOrderMechanicsFiles(){};
	void openFiles(const nonLinearMechSolver* solver,const bool strainToFile, const bool stressToFile, const bool failureToFile);
	void dataToFile(const double time, const nonLinearMechSolver* solver, const homogenizedData* data);
	void closeFiles(){
		if (F_File!=NULL) {fclose(F_File); F_File = NULL;};
		if (P_File!=NULL) {fclose(P_File); P_File = NULL;};
		if (dPdF_File!=NULL) {fclose(dPdF_File); dPdF_File = NULL;};
		if (defoEnerg_File != NULL) {fclose(defoEnerg_File); defoEnerg_File = NULL;};
		if (plasticEnerg_File != NULL) {fclose(plasticEnerg_File); plasticEnerg_File = NULL;};
		if (irreversibleEnergFile != NULL) {fclose(irreversibleEnergFile); irreversibleEnergFile = NULL;};
		if (lostSolutionUniquenssCriterion_File!=NULL){fclose(lostSolutionUniquenssCriterion_File);lostSolutionUniquenssCriterion_File=NULL;};
	};
};

class allFailureFiles{
	public:
		FILE 	*cohesiveJump_File,
					*damageVolume_File,
					*cohesiveTraction_File,
					*averageCohesiveBandWidth_File,
					*FDam_File,
					*dCohesiveJumpDF_File;
		allFailureFiles():cohesiveJump_File(NULL),
						damageVolume_File(NULL),cohesiveTraction_File(NULL),averageCohesiveBandWidth_File(NULL), FDam_File(NULL),
						dCohesiveJumpDF_File(NULL){};
		~allFailureFiles(){};
		void openFiles(const nonLinearMechSolver* solver,const bool strainToFile, const bool stressToFile, const bool failureToFile);
		void dataToFile(const double time, const nonLinearMechSolver* solver, const homogenizedData* data);
		void closeFiles(){
			if (cohesiveJump_File!=NULL){fclose(cohesiveJump_File);cohesiveJump_File=NULL;};
			if (damageVolume_File!=NULL){fclose(damageVolume_File);damageVolume_File=NULL;};
			if (cohesiveTraction_File!=NULL){fclose(cohesiveTraction_File);cohesiveTraction_File=NULL;};
			if (averageCohesiveBandWidth_File!=NULL) {fclose(averageCohesiveBandWidth_File);averageCohesiveBandWidth_File=NULL;};
			if (FDam_File !=NULL){fclose(FDam_File); FDam_File = NULL;};
			if (dCohesiveJumpDF_File!=NULL){fclose(dCohesiveJumpDF_File);dCohesiveJumpDF_File=NULL;}
		};

};

class allSecondOrderMechanicsFiles{
	public:
		FILE	*G_File,
					*Q_File,
					*dQdG_File,
					*dPdG_File,
					*dQdF_File,
					*dQdG_El_File;
		allSecondOrderMechanicsFiles():G_File(NULL),Q_File(NULL),dQdG_File(NULL),dPdG_File(NULL),dQdF_File(NULL),dQdG_El_File(NULL){};
		~allSecondOrderMechanicsFiles(){};
		void openFiles(const nonLinearMechSolver* solver,const bool strainToFile, const bool stressToFile, const bool failureToFile);

		void dataToFile(const double time, const nonLinearMechSolver* solver, const homogenizedData* data);
		void closeFiles(){
			if (G_File!=NULL){fclose(G_File); G_File=NULL;};
			if (Q_File!=NULL){fclose(Q_File); Q_File=NULL;};
			if (dQdG_File!=NULL){fclose(dQdG_File); dQdG_File=NULL;};
			if (dQdF_File!=NULL){fclose(dQdF_File); dQdF_File=NULL;};
			if (dPdG_File!=NULL){fclose(dPdG_File); dPdG_File=NULL;};
			if (dQdG_El_File!=NULL){fclose(dQdG_El_File); dQdG_El_File=NULL;};
		};
};

class allConExtraDofFiles{
	public:
		int fieldIndex;
		FILE 	*gradT_File,  // field gradient
					*T_File, // field
				  *fluxT_File,  // flux
					*eT_File, // internal energy
					*dFluxTdF_File, // dfluxTdF
          *dFluxTdGradT_File,  // dFluxTdgradT
					*dFluxTdT_File,  // dFluxTdT
					*dPdGradT_File,  // dPdgradT
          *dPdT_File, //  dPdT
          *mecaSource_File, // source
          *Cp_File; //
	allConExtraDofFiles(const int index):fieldIndex(index), gradT_File(NULL),T_File(NULL),fluxT_File(NULL),eT_File(NULL),dFluxTdF_File(NULL),dFluxTdGradT_File(NULL),
								dFluxTdT_File(NULL),dPdGradT_File(NULL),dPdT_File(NULL),mecaSource_File(NULL),Cp_File(NULL){};
	~allConExtraDofFiles(){};
	void openFiles(const nonLinearMechSolver* solver,const bool strainToFile, const bool stressToFile, const bool failureToFile);
	void dataToFile(const double time, const nonLinearMechSolver* solver, const homogenizedData* data);
	void closeFiles(){
		if (gradT_File !=NULL){ fclose(gradT_File);gradT_File=NULL;};
		if (T_File!=NULL){fclose(T_File); T_File = NULL;};
		if (fluxT_File!=NULL){fclose(fluxT_File); fluxT_File=NULL;};
		if (eT_File!=NULL){fclose(eT_File); eT_File=NULL;};
		if (dFluxTdGradT_File!=NULL){fclose(dFluxTdGradT_File);dFluxTdGradT_File=NULL;}
		if (dFluxTdT_File!=NULL) {fclose(dFluxTdT_File);dFluxTdT_File=NULL;}
		if (dPdGradT_File!=NULL) {fclose(dPdGradT_File);dPdGradT_File=NULL;};
		if (dFluxTdF_File!=NULL) {fclose(dFluxTdF_File);dFluxTdF_File=NULL;};
		if (dPdT_File !=NULL){fclose(dPdT_File);dPdT_File=NULL;};
		if (mecaSource_File!=NULL){fclose(mecaSource_File);mecaSource_File=NULL;}
		if (Cp_File!=NULL){fclose(Cp_File); Cp_File=NULL;};
	};
};

class allNonConExtraDofFiles{
	public:
		int fieldIndex;
		FILE 	*gradV_File,  // field gradient
				  *fluxV_File,  // flux
					*dfluxVdgradV_File;

	allNonConExtraDofFiles(const int index):fieldIndex(index),gradV_File(NULL),fluxV_File(NULL),dfluxVdgradV_File(NULL){};
	~allNonConExtraDofFiles(){};

	void openFiles(const nonLinearMechSolver* solver,const bool strainToFile, const bool stressToFile, const bool failureToFile);
	void dataToFile(const double time, const nonLinearMechSolver* solver, const homogenizedData* data);
	void closeFiles(){
		if (gradV_File!=NULL){fclose(gradV_File);gradV_File=NULL;};
		if (fluxV_File!=NULL){fclose(fluxV_File);fluxV_File=NULL;};
		if (dfluxVdgradV_File!=NULL){fclose(dfluxVdgradV_File);dfluxVdgradV_File=NULL;};
	};
};

// write to file some important results
class homogenizedDataFiles{
	private:
		allFirstOrderMechanicsFiles _allFirstOrderFiles;
		allSecondOrderMechanicsFiles _allSecondOrderFiles;
		allFailureFiles _allFailureFiles;
		std::vector<allConExtraDofFiles> _allConFiles;
		std::vector<allNonConExtraDofFiles> _allNonConFiles;

  public:
    homogenizedDataFiles(const nonLinearMechSolver* solver);
    ~homogenizedDataFiles(){}
    void openFiles(const nonLinearMechSolver* solver,
                  const bool strainToFile, const bool stressToFile, const bool failureToFile);
		void dataToFile(const double time, const nonLinearMechSolver* solver, const homogenizedData* data);

    void closeFiles(){
			_allFirstOrderFiles.closeFiles();
			_allSecondOrderFiles.closeFiles();
			_allFailureFiles.closeFiles();


			for (int index=0; index < _allConFiles.size(); index++){
				_allConFiles[index].closeFiles();
			}

			for (int index=0; index < _allNonConFiles.size(); index++){
				_allNonConFiles[index].closeFiles();
			}

    };

};

#endif // HOMOGENIZEDDATA_H_
