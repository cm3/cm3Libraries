//
// C++ explicit Newmark scheme
//
// Description: implementation of the explicit algorithms of alpha-generalized method
//              This system is derived from linearSystem from compatibility reason (use of Assemble function and DofManager
//              even if the system is not a no linear system FIX THIS ??
//              for now works only this PETsc
//
// Author:  <Gauthier BECKER>, (C) 2010
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef EXPLICITHULBERTCHUNGSYSTEM_H_
#define EXPLICITHULBERTCHUNGSYSTEM_H_

#include "GmshConfig.h"
#include "GmshMessage.h"
#include "linearSystem.h"
#include "partDomain.h"
#include "nonLinearSystems.h"
#include "restartManager.h"

// virtual class (several implementation of system BLAS and petsc)
template <class scalar>
class explicitHulbertChung : public linearSystem<scalar>, public nonLinearSystem<scalar>{
 protected:
  virtual double dynamicRelaxationFactor()=0; // internal function normally called by systemSolve()
  virtual int systemSize()const=0; // has to return the number of the system (use internally by to create a restart
 public :
  enum state{current, next};
  virtual bool isAllocated() const=0;
  virtual void allocate(int nbRows)=0;
  virtual void clear()=0;
  virtual void addToMatrix(int row, int col, const scalar &val)=0;
  virtual void addToRightHandSide(int row, const scalar &val, int ith=0)=0;
  virtual void addToRightHandSidePlus(int row, const scalar &val)=0;
  virtual void addToRightHandSideMinus(int row, const scalar &val)=0;
  virtual void getFromRightHandSide(int row, scalar &val) const=0;
  virtual void getFromRightHandSidePlus(int row, scalar &val) const=0;
  virtual void getFromRightHandSideMinus(int row, scalar &val) const=0;
  virtual void zeroMatrix(const nonLinearSystemBase::whichMatrix wm) {Msg::Error("explicitHulbertChung::zeroMatrix cannot be used");};
  virtual void zeroMatrix()=0;
  virtual void zeroRightHandSide()=0;
  virtual int systemSolve()=0;
  virtual double normInfRightHandSide() const=0;
  virtual void getFromMatrix(int row, int col, scalar &val) const=0;
  virtual double getExternalWork(const int syssize)const=0;
  virtual void getFromSolution(int row, scalar &val) const=0;
  virtual void nextStep()=0;
  virtual void setTimeStep(const double dt)=0;
  virtual double getTimeStep()const=0;
  virtual void getFromSolution(int row, scalar &val, const nonLinearBoundaryCondition::whichCondition wv) const=0;
  virtual void getFromMatrix(int row, int col, scalar &val,const nonLinearSystemBase::whichMatrix wm) const
  {
   #ifdef _DEBUG
    if(wm!=nonLinearSystemBase::mass)
    {
      Msg::Error("An explicit system has only a mass matrix. So cannot get a value in another matrix");
      val=0;
      return;
    }
   #endif // _DEBUG
    this->getFromMatrix(row,col,val);
  }
  virtual void addToSolution(int _row, const scalar &val) {Msg::Error("addToSolution must be defined for this system");};
  virtual void zeroSolution() {Msg::Error("zeroSolution must be defined for this system");};

  // Contact communication in mpi
  #if defined(HAVE_MPI)
  virtual double* getArrayIndexPreviousRightHandSidePlusMPI(const int index)=0;
//  virtual void setPreviousRightHandSidePlusMPI(const int row, const scalar &val)=0;
  #endif // HAVE_MPI
  // restart
  virtual void restart()=0;
};
#endif // EXPLICITHULBERTCHUNGSYSTEM_H_

