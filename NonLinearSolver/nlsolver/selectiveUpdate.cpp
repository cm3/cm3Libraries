//
// C++ Interface: selective update
//
// Author:  <Van Dung Nguyen>, (C) 2018
//
// Copyright: See COPYING file that comes with this distribution
//
//

#include "selectiveUpdate.h"
#include "nonLinearMechSolver.h"
#include "ipField.h"
#include "numericalMaterial.h"
#include "ipFiniteStrain.h"
#include "ipNonLocalPorosity.h"

#if defined(HAVE_MPI)
#include "mpi.h"
#endif //HAVE_MPI

void selectiveUpdateBase::nextStep(){
  _reSolveBySelectiveUpdate = false;
};

void selectiveUpdateBase::resetToPreviousStep(){
  _reSolveBySelectiveUpdate = false;
}


cohesiveCrackCriticalIPUpdate::cohesiveCrackCriticalIPUpdate():selectiveUpdateBase(){};
cohesiveCrackCriticalIPUpdate::cohesiveCrackCriticalIPUpdate(const cohesiveCrackCriticalIPUpdate& src):selectiveUpdateBase(src),
  _newFailedElements(src._newFailedElements),_newBlockedElements(src._newBlockedElements){};
cohesiveCrackCriticalIPUpdate::~cohesiveCrackCriticalIPUpdate(){}

void cohesiveCrackCriticalIPUpdate::nextStep(){
  selectiveUpdateBase::nextStep();
  _newFailedElements.clear();
  _newBlockedElements.clear();
};

void cohesiveCrackCriticalIPUpdate::resetToPreviousStep(){
  selectiveUpdateBase::resetToPreviousStep();
  _newFailedElements.clear();
  _newBlockedElements.clear();
}

void cohesiveCrackCriticalIPUpdate::checkSelectiveUpdate(nonLinearMechSolver* solver){
  const IPField* ipf = solver->getIPField();
  const std::set<int>& currentBrokenIPs = ipf->getConstRefToBrokenIPs();
  int brokenIPSize = currentBrokenIPs.size();

  #if defined(HAVE_MPI)
  if (solver->getNumRanks() >1){
    std::set<int> allRootRanks, allOtherRanks;
    solver->getAllRootRanks(allRootRanks);
    solver->getAllOtherRanks(allOtherRanks);
    int totalbrokenIP = 0;
    MPI_Allreduce(&brokenIPSize,&totalbrokenIP,1,MPI_INT,MPI_SUM,MPI_COMM_WORLD);
    brokenIPSize = totalbrokenIP;
  }
  #endif // HAVE_MPI

  if (brokenIPSize > 0){
    // new failed IPs are detected
    this->setSelectiveUpdate(true);
  }
  else{
    Msg::Info("no new IP failed is detected");
    // otherwise, no new IP is detected
    this->setSelectiveUpdate(false);
  }
};

void cohesiveCrackCriticalIPUpdate::selectiveUpdate(nonLinearMechSolver* solver){
  if (!solverIterateBySelectiveUpdate()) return;

  IPField* ipf = solver->getIPField();
  const std::set<int>& currentBrokenIPs = ipf->getConstRefToBrokenIPs();

  // find most failed IP
  int mostFailedIP =-1;
  TwoNum mostFailedEle(-1);
  int mostFailedEleNum = -1;
  double maxCr = -1e10;
  for (std::set<int>::const_iterator itnum = currentBrokenIPs.begin(); itnum != currentBrokenIPs.end(); itnum ++){
    int num = *itnum;
    int elnum, gpt;
    numericalMaterialBase::getTwoIntsFromType(num,elnum,gpt);
    AllIPState::ipstateContainer::const_iterator its = ipf->getAips()->getAIPSContainer()->find(elnum);
    if (its != ipf->getAips()->getAIPSContainer()->end()){
      const AllIPState::ipstateElementContainer& vips = its->second;
      const IPVariable* ipv = vips[gpt]->getState(IPStateBase::current);
      const IPVariable2ForFractureBase* ipvFrac = dynamic_cast<const IPVariable2ForFractureBase*>(ipv);
      if (ipvFrac != NULL){
        if (ipvFrac->getOnsetCriterion() > maxCr){
          mostFailedEleNum = elnum;
          mostFailedEle = solver->getMinusAndPlusElementNumber(elnum);
          mostFailedIP = gpt;
          maxCr = ipvFrac->getOnsetCriterion();
        }
      }
    }
    else{
      Msg::Error("IPstate of element %d, GP %d can not be found",elnum,gpt);
    }
  }

  if (mostFailedEleNum >0){
    #if defined(HAVE_MPI)
    if (Msg::GetCommSize() > 1)
    {
      printf("rank %d: most failure element em %d ep %d failCr = %e\n",Msg::GetCommRank(),mostFailedEle.small,mostFailedEle.large,maxCr);
    }
    else
    #endif // HAVE_MPI
    {
      printf("most failure element em %d ep %d failCr = %e\n",mostFailedEle.small,mostFailedEle.large,maxCr);
    }
  }

  if (currentBrokenIPs.size() > 0 and maxCr < 0){
    Msg::Error("error in using the IPVariable2ForFractureBase::getOnsetCriterion, this criterion must be positive if failure occurs, negative if it does not!!!");
  }

  // comminucation with MPI
  #if defined(HAVE_MPI)
  if (solver->getNumRootRanks() >1){
    std::vector<int> allSmall(solver->getNumRanks());
    std::vector<int> allLarge(solver->getNumRanks());
    std::vector<double> allMaxCr(solver->getNumRanks());
    MPI_Allgather(&mostFailedEle.small,1,MPI_INT,&allSmall[0],1,MPI_INT,MPI_COMM_WORLD);
    MPI_Allgather(&mostFailedEle.large,1,MPI_INT,&allLarge[0],1,MPI_INT,MPI_COMM_WORLD);
    MPI_Allgather(&maxCr,1,MPI_DOUBLE,&allMaxCr[0],1,MPI_DOUBLE,MPI_COMM_WORLD);

    // remove all duplicate
    std::map<TwoNum,double> uniqueMap;
    for (int i=0; i< solver->getNumRanks(); i++){
      if (allSmall[i] > 0 and allLarge[i]> 0){
        TwoNum tn(allSmall[i],allLarge[i]);
        uniqueMap[tn] = allMaxCr[i];
      }
    }
    if (uniqueMap.size() == 0) Msg::Error("error in cohesiveCrackCriticalIPUpdate::selectiveUpdate uniqueMap.size() == 0");

    TwoNum mostFailedEleMPI = uniqueMap.begin()->first;
    double mostFailedCrMPI = uniqueMap.begin()->second;
    for (std::map<TwoNum,double>::const_iterator it = uniqueMap.begin(); it != uniqueMap.end(); it++){
      if (it != uniqueMap.begin()){
        if (it->second > mostFailedCrMPI){
          mostFailedEleMPI = it->first;
          mostFailedCrMPI = it->second;
        }
      }
    }
    // at current rank
    if (mostFailedEle  == mostFailedEleMPI){
      // do nothing
      printf("rank %d: most failure element em %d ep %d failCr = %e is chosen to be cracked\n",Msg::GetCommRank(),mostFailedEle.small,mostFailedEle.large,maxCr);
    }
    else{
      if (mostFailedEleNum > 0){
        printf("rank %d: most failure element em %d ep %d failCr = %e is not chosen to be cracked as crack opened in other rank\n",Msg::GetCommRank(),mostFailedEle.small,mostFailedEle.large,maxCr);
      }
      // failure from other rank
      mostFailedEleNum = -1;
      mostFailedIP = -1;
      mostFailedEle = mostFailedEleMPI;
      maxCr = mostFailedCrMPI;
    }
  };
  #endif // HAVE_MPI


  // failure occurs at all failed IPs of element mostFailedEle
  std::set<int>& nonBrokenIPs = ipf->getRefToNonBrokenIPs();
  std::set<int>& failedElement = ipf->getRefToNewlyBrokenElements();
  std::set<int>& blockedElement = ipf->getRefToNewlyBlockedDissipationElements();
  std::set<int>& nonBlockedDissipationIPs =  ipf->getRefToNonBlockedDissipationIPs();

  nonBrokenIPs.clear();
  nonBlockedDissipationIPs.clear();

  if (mostFailedEle.small > 0){
    //#if defined(HAVE_MPI)
    //if (Msg::GetCommSize() > 1){
    //  printf("rank %d: the most failed element em %d ep %d \n",Msg::GetCommRank(),mostFailedEle.small,mostFailedEle.large);
    //}
    //else
    //#endif // HAVE_MPI
    //{
    //  printf("the most failed element em %d ep %d \n",mostFailedEle.small,mostFailedEle.large);
    //}

    // only break all IP belonging to most failed element
    for (std::set<int>::const_iterator itnum = currentBrokenIPs.begin(); itnum != currentBrokenIPs.end(); itnum ++){
      int num = *itnum;
      int elnum, gpt;
      numericalMaterialBase::getTwoIntsFromType(num,elnum,gpt);
      if (solver->getMinusAndPlusElementNumber(elnum) != mostFailedEle){
        nonBrokenIPs.insert(num);
      }
    }
    this->restoreNonBrokenIPVariable(ipf, nonBrokenIPs);

    // if mostFailedEle is totally broken
    if (failedElement.size() > 0 and mostFailedEleNum > 0){
      _newFailedElements.insert(mostFailedEleNum);
    }
    //

    // find interface element
    if (blockedElement.size() > 0 and mostFailedEleNum > 0){
      _newBlockedElements.insert(mostFailedEle.small);
      _newBlockedElements.insert(mostFailedEle.large);

      printf("block damage in elements %d %d",mostFailedEle.small,mostFailedEle.large);

      // modify blocked IP
      // only IP of blocked element are blocked
      const std::set<int>& currentBlockedDissipationIPs = ipf->getConstRefToBlockedDissipationIPs();
      for (std::set<int>::const_iterator itnum = currentBlockedDissipationIPs.begin(); itnum != currentBlockedDissipationIPs.end(); itnum ++){
        int num = *itnum;
        int elnum, gpt;
        numericalMaterialBase::getTwoIntsFromType(num,elnum,gpt);
        if (_newBlockedElements.find(elnum) == _newBlockedElements.end()){
          nonBlockedDissipationIPs.insert(num);
        }
      };
    }
  };

  // add current crack path increment
  failedElement.clear();
  failedElement.insert(_newFailedElements.begin(),_newFailedElements.end());

  blockedElement.clear();
  blockedElement.insert(_newBlockedElements.begin(),_newBlockedElements.end());

  // modify failure state
  ipf->flushFailureState(); // always called
};





cohesiveCrackCriticalIPPorousUpdate::cohesiveCrackCriticalIPPorousUpdate():
      cohesiveCrackCriticalIPUpdate(){};

cohesiveCrackCriticalIPPorousUpdate::cohesiveCrackCriticalIPPorousUpdate(const cohesiveCrackCriticalIPPorousUpdate& src):
      cohesiveCrackCriticalIPUpdate(src){};

cohesiveCrackCriticalIPPorousUpdate::~cohesiveCrackCriticalIPPorousUpdate(){};

void cohesiveCrackCriticalIPPorousUpdate::restoreNonBrokenIPVariable(IPField* ipf, const std::set<int>& nonBrokenIPs) const
{
  // Function to reset coalescence and nonlocalTolocal flags

  // Loop on  nonBrokenIPs
  for (std::set<int>::const_iterator itnum = nonBrokenIPs.begin(); itnum != nonBrokenIPs.end(); itnum ++){
    int num = *itnum;
    int elnum, gpt;
    numericalMaterialBase::getTwoIntsFromType(num,elnum,gpt);
    AllIPState::ipstateContainer::const_iterator its = ipf->getAips()->getAIPSContainer()->find(elnum);
    if (its != ipf->getAips()->getAIPSContainer()->end()){
      const AllIPState::ipstateElementContainer& vips = its->second;
      IPVariable* ipv = vips[gpt]->getState(IPStateBase::current);
      ipFiniteStrain* ipFStrain = dynamic_cast<ipFiniteStrain*>(ipv);
      IPNonLocalPorosity* ipPorous = dynamic_cast<IPNonLocalPorosity*>(ipFStrain->getInternalData());
      if (ipPorous == NULL){
        Msg::Error("cohesiveCrackCriticalIPPorousUpdate::restoreNonBrokenIPVariable: This SelectiveUpdate can be used with NonLocalPorous only !");
      };
      ipPorous->setNonLocalToLocal(false);
      ipPorous->getRefToIPCoalescence().getRefToCoalescenceOnsetFlag() = false;
      Msg::Info("IP restored");

    }
    else{
      Msg::Error("IPstate of element %d, GP %d can not be found",elnum,gpt);
    }
  }

}








