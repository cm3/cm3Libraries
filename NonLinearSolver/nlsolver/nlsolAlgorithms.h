//
// C++ Interface: terms
//
// Description: non linear assembly fonctions
//
//
// Author:  <Gauthier BECKER>, (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef NONLINEARSOLVERALGORITHMS_H_
#define NONLINEARSOLVERALGORITHMS_H_
#include "quadratureRules.h"
#include "MVertex.h"
#include "MInterfaceElement.h"
#include "functionSpace.h"
#include "nonLinearSystems.h"
#include "solverAlgorithms.h"
#include "staticDofManager.h"
#include "dofManagerMultiSystems.h"
#include "ThreeDLagrangeFunctionSpace.h"
#include "ThreeDHierarchicalFunctionSpace.h"

template <class Iterator, class Assembler>
void AssembleItMap(LinearTermBase<double> &term, FunctionSpaceBase &space,
              Iterator itbegin, Iterator itend, QuadratureBase &integrator,
              Assembler &assembler, elementFilter &efilter)
{
  fullVector<double> localVector;
  std::vector<Dof> R;
  for(Iterator it = itbegin; it != itend; ++it) {
    MElement *e = it->second;
    if(efilter(e)) {
      R.clear();
      IntPt *GP;
      int npts = integrator.getIntPoints(e, &GP);
      term.get(e, npts, GP, localVector); // localVector.print();
      space.getKeys(e, R);
      assembler.assemble(R, localVector);
    }
  }
}

template <class Iterator, class Assembler>
void AssembleItMap(BilinearTermBase &term, FunctionSpaceBase &space,
              Iterator itbegin, Iterator itend, QuadratureBase &integrator,
              Assembler &assembler, elementFilter &efilter)
// symmetric
{
  fullMatrix<double> localMatrix;
  std::vector<Dof> R;
  for(Iterator it = itbegin; it != itend; ++it) {
    MElement *e = it->second;
    if(efilter(e)) {
      R.clear();
      IntPt *GP;
      int npts = integrator.getIntPoints(e, &GP);
      term.get(e, npts, GP, localMatrix); // localMatrix.print();
      space.getKeys(e, R);
      assembler.assemble(R, localMatrix);
    }
  }
}

template <class Iterator, class Assembler>
void AssembleItMapWithCompFilter(FilterDof& filter, BilinearTermBase &term, FunctionSpaceBase &space,
              Iterator itbegin, Iterator itend, QuadratureBase &integrator,
              Assembler &assembler, elementFilter &efilter)
// symmetric
{
  fullMatrix<double> localMatrix, localMatrixComp;
  std::vector<Dof> R, Rcomp;
  std::vector<int> indexes;
  for(Iterator it = itbegin; it != itend; ++it) {
    MElement *e = it->second;
    if(efilter(e)) {
      R.clear();
      IntPt *GP;
      int npts = integrator.getIntPoints(e, &GP);
      term.get(e, npts, GP, localMatrix); // localMatrix.print();
      space.getKeys(e, R);
      
      indexes.clear();
      Rcomp.clear();
      for (int j=0; j< R.size(); j++)
      {
        if (filter(R[j]))
        {
          Rcomp.push_back(R[j]);
          indexes.push_back(j);
        }
      }
      int RcompSize = Rcomp.size();
      localMatrixComp.resize(RcompSize, RcompSize);
      for (int i=0; i< RcompSize; i++)
      {
        for (int j=0; j< RcompSize; j++)
        {
          localMatrixComp(i,j) = localMatrix(indexes[i], indexes[j]);
        }
      }
      assembler.assemble(Rcomp, localMatrixComp);
    }
  }
}


template<class Iterator, class Assembler> 
void AssemblerPathFollowingConstraint(ScalarTermBase<double>&term, Iterator itbegin, Iterator itend,
																			QuadratureBase &integrator, Assembler &assembler,
																			elementFilter& efilter){
	IntPt *GP;
	for (Iterator it = itbegin; it != itend; ++it){
    MElement *e = it->second;
		if (efilter(e)){
			int npts = integrator.getIntPoints(e, &GP);
			double scalar = 0.;
			term.get(e, npts, GP, scalar); 
			assembler.assemblePathFollowingConstraint(scalar);			
		}
  }																				
};

template<class Iterator, class Assembler> 
void AssemblerDPathFollowingConstraintDUnknowns(LinearTermBase<double>&term, FunctionSpaceBase &space,
																			Iterator itbegin, Iterator itend,
																			QuadratureBase &integrator, Assembler &assembler,
																			elementFilter& efilter){
																				
	fullVector<double> localVec;
  std::vector<Dof> R;
	IntPt *GP;
  for (Iterator it = itbegin; it != itend; ++it){
    MElement *e = it->second;
		if (efilter(e)){
			R.clear();
			int npts = integrator.getIntPoints(e, &GP);
			space.getKeys(e, R);
			localVec.resize(R.size());
			localVec.setAll(0.);
			term.get(e, npts, GP, localVec);
			assembler.assembleDPathFollowingConstraintDUnknowns(R, localVec);
		}
  }																		
};

template<class Iterator, class Assembler> void AssembleStressMatrix(BilinearTermBase &term, FunctionSpaceBase &space,
                                                        Iterator itbegin, Iterator itend,
                                                        QuadratureBase &integrator, Assembler &assembler,elementFilter& efilter)
  // symmetric
{
  fullMatrix<double> localMatrix;
  std::vector<Dof> R;
  IntPt *GP;
  for (Iterator it = itbegin; it != itend; ++it){
    MElement *e = it->second;
		if (efilter(e)){
			R.clear();
			int npts = integrator.getIntPoints(e, &GP);
			term.get(e, npts, GP, localMatrix); //localMatrix.print();
			space.getKeys(e, R);
			assembler.assembleToStressMatrix(R, localMatrix);
		}
  }
}

template<class Iterator, class Assembler> void AssembleBodyForceMatrix(BilinearTermBase &term, FunctionSpaceBase &space,
                                                        Iterator itbegin, Iterator itend,
                                                        QuadratureBase &integrator, Assembler &assembler,elementFilter& efilter)
  // symmetric
{
  fullMatrix<double> localMatrix;
  std::vector<Dof> R;
  IntPt *GP;
  for (Iterator it = itbegin; it != itend; ++it){
    MElement *e = it->second;
	if (efilter(e)){
	  R.clear();
	  int npts = integrator.getIntPoints(e, &GP);
	  term.get(e, npts, GP, localMatrix); //localMatrix.print();
	  space.getKeys(e, R);
	  assembler.assembleToBodyForceMatrix(R, localMatrix);
	}
  }
}


template<class Iterator, class Assembler> void AssembleBodyForceVector(LinearTermBase<double> &term, FunctionSpaceBase &space,
                                                        Iterator itbegin, Iterator itend,
                                                        QuadratureBase &integrator, Assembler &assembler,elementFilter& efilter)
{
  fullVector<double> localVector;
  std::vector<Dof> R;
  IntPt *GP;
  for (Iterator it = itbegin; it != itend; ++it){
    MElement *e = it->second;
	if (efilter(e)){
	  R.clear();
	  int npts = integrator.getIntPoints(e, &GP);
	  term.get(e, npts, GP, localVector); //localMatrix.print();
	  space.getKeys(e, R);
	  assembler.assembleToBodyForceVector(R, localVector);
	}
  }
}


// Derived to check the size of the matrix and skip the assembly process if the matrix is empty
template<class Iterator, class Assembler> void AssembleSkipEmpty(BilinearTermBase &term, FunctionSpaceBase &space,
                                                        Iterator itbegin, Iterator itend,
                                                        QuadratureBase &integrator, Assembler &assembler,
																												elementFilter& efilter)
  // symmetric
{
  fullMatrix<double> localMatrix;
  std::vector<Dof> R;
  for (Iterator it = itbegin; it != itend; ++it){
    MElement *e = it->second;
		if (efilter(e)){
			R.clear();
			IntPt *GP;
			int npts = integrator.getIntPoints(e, &GP);
			term.get(e, npts, GP, localMatrix); //localMatrix.print();
			if(localMatrix.size1()==0) continue;
			space.getKeys(e, R);
			assembler.assemble(R, localMatrix);
		}
  }
}


// Assemble nonlinearterm. The field of term can be pass (compute matrix by perturbation and avoid a additional getKeys)
// Additional argument is pass to give if rhsPlus (Fext) or rhsMinus (Fint) is used in system

template<class Iterator, class Assembler> void Assemble(LinearTermBase<double> *term, FunctionSpaceBase &space,
                                                        Iterator itbegin, Iterator itend,
                                                        QuadratureBase &integrator,
                                                        const unknownField *ufield,Assembler &assembler,
                                                        const nonLinearSystemBase::rhs wrhs,
																												elementFilter& efilter)
{
  fullVector<double> localVector(0);
  std::vector<Dof> R;
  nonLinearTermBase<double> *nlterm = dynamic_cast<nonLinearTermBase<double>*>(term);
	IntPt *GP;
  if( (nlterm !=NULL) and (nlterm->isData())){
    fullVector<double> disp;
    nlterm->set(&disp);
    for (Iterator it = itbegin; it != itend; ++it){
      MElement *e = it->second;
			if (efilter(e)){
				R.clear();
				int npts = integrator.getIntPoints(e, &GP);
				space.getKeys(e, R);
				for(int i=0;i<R.size();i++)
				disp.resize(R.size());
				ufield->get(R,disp);
				term->get(e, npts, GP, localVector); //localVector.print();
				assembler.assemble(R, localVector,wrhs);
			}
    }
  }
  else{
    for (Iterator it = itbegin; it != itend; ++it){
      MElement *e = it->second;
			if (efilter(e)){
				R.clear();
				int npts = integrator.getIntPoints(e, &GP);
				space.getKeys(e, R);
				term->get(e, npts, GP, localVector); //localVector.print();
				assembler.assemble(R, localVector,wrhs);
			}
    }

  }
}


// for rigid contact
template<class Iterator, class Assembler> void Assemble(LinearTermBase<double> &term, FunctionSpaceBase &space,
                                                        Iterator itbegin, Iterator itend,
                                                        QuadratureBase &integrator, Assembler &assembler,
                                                        const nonLinearSystemBase::rhs wrhs,
																												elementFilter& efilter)
{
  fullVector<double> localVector;
  std::vector<Dof> R;
	IntPt *GP;
  for (Iterator it = itbegin; it != itend; ++it){
    MElement *e = it->second;
		if (efilter(e)){
			R.clear();
			int npts = integrator.getIntPoints(e, &GP);
			term.get(e, npts, GP, localVector); //localVector.print();
			space.getKeys(e, R);
			assembler.assemble(R, localVector,wrhs);
		}
  }
}

// for defo defo contact
template<class Iterator, class Assembler> void AssembleDefoDefoContact(LinearTermBase<double> &term, FunctionSpaceBase &space,
                                                        Iterator itbegin, Iterator itend,
                                                        QuadratureBase &integrator, Assembler &assembler,
                                                        const nonLinearSystemBase::rhs wrhs, elementFilter& efilter)
{
  fullVector<double> localVector;
  std::vector<Dof> R;
  IntPt *GP;
  for (Iterator it = itbegin; it != itend; ++it){
    contactElement *e = *it;
    R.clear();
    defoDefoContactSpaceBase *sp=static_cast<defoDefoContactSpaceBase *> (&space);
    sp->getKeys(e, R);
    int npts = integrator.getIntPoints(e->getRefToElementMaster(), &GP);
    if (efilter(e->getRefToElementMaster())   && efilter(e->getRefToElementSlave()))
    {
        defoDefoContactLinearTermBase<double> *defoDefoTerm = static_cast<defoDefoContactLinearTermBase<double> *> (&term);
        defoDefoTerm->get(e, npts, GP, localVector); 
        //localVector.print();
        assembler.assemble(R, localVector,wrhs);
    }
  }
}

// Derived to check the size of the matrix and skip the assembly process if the matrix is empty
template<class Iterator, class Assembler> void AssembleSkipEmptyDefoDefoContact(BilinearTermBase &term, FunctionSpaceBase &space,
                                                        Iterator itbegin, Iterator itend,
                                                        QuadratureBase &integrator, Assembler &assembler, elementFilter& efilter)
  // symmetric
{
  fullMatrix<double> localMatrix;
  std::vector<Dof> R;
  for (Iterator it = itbegin; it != itend; ++it){
    contactElement *e = *it;
    if (efilter(e->getRefToElementMaster())   && efilter(e->getRefToElementSlave()))
    {
      R.clear();
      IntPt *GP;
      int npts = integrator.getIntPoints(e->getRefToElementMaster(), &GP);
      defoDefoContactBilinearTermBase<double> *defoDefoTerm = static_cast< defoDefoContactBilinearTermBase<double>* > (&term);
      defoDefoTerm->get(e, npts, GP, localMatrix); 
      //localMatrix.print();
      if(localMatrix.size1()==0) continue;
      defoDefoContactSpaceBase *sp=static_cast<defoDefoContactSpaceBase *> (&space);
      sp->getKeys(e, R);
      assembler.assemble(R, localMatrix);
    }
  }
}



// Function Assemble for mass matrix. An other function is needed because Two matrix in the system
template<class Iterator,class Assembler> void AssembleMass(BilinearTermBase *term,FunctionSpaceBase &space,Iterator itbegin,
                                                           Iterator itend,QuadratureBase &integrator,Assembler &assembler) // symmetric
{
  fullMatrix<double> localMatrix;
  // remove the Dynamic Cast How
  std::vector<Dof> R;
	IntPt *GP;
  for (Iterator it = itbegin;it!=itend; ++it)
  {
    MElement *e = it->second;
    R.clear();
    int npts=integrator.getIntPoints(e,&GP);
    term->get(e,npts,GP,localMatrix);
    space.getKeys(e,R);
    assembler.assemble(R, localMatrix,nonLinearSystemBase::mass);
  }
}

template<class Iterator,class Assembler> void AssembleMass(BilinearTermBase *term,FunctionSpaceBase &space,Iterator itbegin,
                                                           Iterator itend,QuadratureBase &integrator,Assembler &assembler,
																													 elementFilter& efilter) // symmetric
{
  fullMatrix<double> localMatrix;
  // remove the Dynamic Cast How
  std::vector<Dof> R;
	IntPt *GP;
  for (Iterator it = itbegin;it!=itend; ++it)
  {
    MElement *e = *it;
		if (efilter(e)){
			R.clear();
			int npts=integrator.getIntPoints(e,&GP);
			term->get(e,npts,GP,localMatrix);
			space.getKeys(e,R);
			assembler.assemble(R, localMatrix,nonLinearSystemBase::mass);
		}
  }
}

// Assemble mass for a rigid contact space. Only ddl of GC
template<class Assembler>
void AssembleMass(BilinearTermBase *term, rigidContactSpaceBase *space,Assembler* assembler){
  fullMatrix<double> localMatrix;
  std::vector<Dof> R;
  space->getKeysOfGravityCenter(R);
  MElement *ele;
  IntPt *GP;
  term->get(ele,0,GP,localMatrix);
  assembler->assemble(R, localMatrix,nonLinearSystemBase::mass);
}


template<class Assembler> void FixNodalDofs(FunctionSpaceBase *space,MElement *e,Assembler &assembler,simpleFunction<double> &fct,FilterDof &filter,bool fullDg, mixedFunctionSpaceBase::DofType dofType)
{
  if (dofType == mixedFunctionSpaceBase::DOF_STANDARD)
  {
    // there are some dofs, same comp must be contrained 0
    HierarchicalFunctionSpace* hspace = dynamic_cast<HierarchicalFunctionSpace*>(space);
    if (hspace != NULL)
    {
      std::vector<Dof> Rall;
      hspace->getKeys(e,Rall);
      for (int i=0; i<Rall.size(); i++)
      {
        if(filter(Rall[i]))
        {
          assembler.fixDof(Rall[i],0);
        }
      }
    };
    
    nlsFunctionSpace<double>* lagspace = static_cast<nlsFunctionSpace<double>*>(space);
    std::vector<int> comp;
    lagspace->getComp(comp);
    int nv = e->getNumVertices();
    std::vector<Dof> R;
    for (int i=0; i< nv; i++)
    {
      R.clear();
      space->getKeysOnVertex(e,e->getVertex(i),comp,R);
      for (int iR =0; iR < R.size(); iR++)
      {
        if(filter(R[iR]))
        {
          assembler.fixDof(R[iR], fct(e->getVertex(i)->x(),e->getVertex(i)->y(),e->getVertex(i)->z()));
        }
      }
    }
    
    
    
    /*
    std::vector<MVertex*> tabV;
    int nv=e->getNumVertices();
    std::vector<Dof> R;
    space->getKeys(e,R);
    tabV.reserve(nv);
    for (int i=0;i<nv;++i) tabV.push_back(e->getVertex(i));

    if(!fullDg){
      for (std::vector<Dof>::iterator itd=R.begin();itd!=R.end();++itd)
      {
        Dof key=*itd;
        if (filter(key))
        {
          for (int i=0;i<nv;++i)
          {
            if (tabV[i]->getNum()==key.getEntity())
            {
              //Msg::Info("Fix dof number %d comp %d",key.getEntity(),key.getType());
              assembler.fixDof(key, fct(tabV[i]->x(),tabV[i]->y(),tabV[i]->z()));
              break;
            }
          }
        }
      }
    }
    else{
      for (std::vector<Dof>::iterator itd=R.begin();itd!=R.end();++itd)
      {
        Dof key=*itd;
        if (filter(key))
        {
          for (int i=0;i<nv;++i)
          {
            //Msg::Info("Fix dof number %d comp %d on rank %d",key.getEntity(),key.getType(),Msg::GetCommRank());
            assembler.fixDof(key, fct(tabV[i]->x(),tabV[i]->y(),tabV[i]->z()));
            break;
          }
        }
      }
    }
     * */
  }
  else if (dofType == mixedFunctionSpaceBase::DOF_CURL)
  {
    std::vector<Dof> R;
    space->getKeys(e,R);
    for (int iR =0; iR < R.size(); iR++)
    {
      if(filter(R[iR]))
      {
        assembler.fixDof(R[iR], fct(0,0,0));
      }
    }
  }
  else
  {
    Msg::Error("FixNodalDofs is not implemented for this kind of DOF");
  }
}

template<class Iterator,class Assembler> void FixNodalDofs(FunctionSpaceBase *space,Iterator itbegin,Iterator itend,Assembler &assembler,
                                                           simpleFunction<double> &fct,FilterDof &filter,bool fullDg,
                                                           mixedFunctionSpaceBase::DofType dofType)
{
  for (Iterator it=itbegin;it!=itend;++it)
  {
    FixNodalDofs(space,it->second,assembler,fct,filter,fullDg,dofType);
  }
}

template<class Assembler>
void FixNodalDofs(rigidContactSpaceBase *space,simpleFunction<double> &fct,FilterDof &filter, Assembler &assembler){
  std::vector<Dof> R;
  space->getKeysOfGravityCenter(R);
  for(int i=0;i<R.size();i++){
    if(filter(R[i]))
      assembler.fixDof(R[i], fct(0.,0.,0.));
  }

}

template<class Assembler> void SetInitialDofs(FunctionSpaceBase *space,MElement *e,const nonLinearBoundaryCondition::whichCondition whichC,
                                              Assembler &assembler,const simpleFunctionTime<double> *fct,
                                              FilterDof &filter,bool fullDg, mixedFunctionSpaceBase::DofType dofType)
{
  if (dofType == mixedFunctionSpaceBase::DOF_STANDARD)
  {
    // there are some dofs, same comp must be contrained 0
    HierarchicalFunctionSpace* hspace = dynamic_cast<HierarchicalFunctionSpace*>(space);
    if (hspace != NULL)
    {
      std::vector<Dof> Rall;
      hspace->getKeys(e,Rall);
      for (int i=0; i<Rall.size(); i++)
      {
        if(filter(Rall[i]))
        {
          assembler.setInitialCondition(Rall[i],0,whichC);
        }
      }
    };
    
    nlsFunctionSpace<double>* lagspace = static_cast<nlsFunctionSpace<double>*>(space);
    std::vector<int> comp;
    lagspace->getComp(comp);
    int nv = e->getNumVertices();
    std::vector<Dof> R;
    for (int i=0; i< nv; i++)
    {
      double myvalue = fct->operator()(e->getVertex(i)->x(),e->getVertex(i)->y(),e->getVertex(i)->z());
      R.clear();
      space->getKeysOnVertex(e,e->getVertex(i),comp,R);
      for (int iR =0; iR < R.size(); iR++)
      {
        if(filter(R[iR]))
        {
          assembler.setInitialCondition(R[iR],myvalue,whichC);
        }
      }
    }
    
  }
  else if (dofType == mixedFunctionSpaceBase::DOF_CURL)
  {
    std::vector<Dof> R;
    space->getKeys(e,R);
    double myvalue = fct->operator()(0,0,0);
    for (int iR =0; iR < R.size(); iR++)
    {
      if(filter(R[iR]))
      {
        assembler.setInitialCondition(R[iR],myvalue,whichC);
      }
    }
  }
  else
  {
    Msg::Error("SetInitialDofs is not implemented for this kind of DOF");
  }
  
}

template<class Iterator,class Assembler> void SetInitialDofs(FunctionSpaceBase *space,Iterator itbegin,Iterator itend,
                                                           const nonLinearBoundaryCondition::whichCondition whichC,
                                                            Assembler &assembler, const simpleFunctionTime<double> *fct,
                                                            FilterDof &filter,bool fullDg, mixedFunctionSpaceBase::DofType dofType)
{
  for (Iterator it=itbegin;it!=itend;++it)
  {
    SetInitialDofs(space,it->second,whichC,assembler,fct,filter,fullDg,dofType);
  }
}


/* Consider the undeformed configuration for now !!!!*/
/* problem no comp function ??? --> can't filter the dof */
template<class Assembler> void SetNormalInitialDofs(FunctionSpaceBase *space,MElement *e,Assembler &assembler,
                                            const simpleFunction<double> *fct,const nonLinearBoundaryCondition::whichCondition whichC,
                                            FilterDof &filter,bool fullDg,elementGroup *groupDom)
{
  int nv=e->getNumVertices();
  std::vector<Dof> R;
  space->getKeys(e,R);
  // compute the value of the condition for each node in each direction
  double u,v,w; // local node position
  std::vector<TensorialTraits<double>::GradType> grads;
  FunctionSpace<double>* spd = static_cast<FunctionSpace<double>*>(space);
//  std::vector<double> vertexvalue(R.size);
  std::vector<int> nodeIndex;
  MElement *ele;
  if(groupDom == NULL){
    for(int i=0;i<nv;i++)
      nodeIndex.push_back(i);
    ele = e;
  }
  else{
    // find the element which e is the boundary
    MVertex *ver1 = e->getVertex(0);
    MVertex *ver2 = e->getVertex(1);
    for(elementGroup::elementContainer::const_iterator it = groupDom->begin(); it!=groupDom->end();++it)
    {
      MElement* eletmp = it->second;
      int numpver = eletmp->getNumPrimaryVertices();
      bool find1 = false, find2 = false;
      for(int i=0;i<numpver; i++)
      {
         if(eletmp->getVertex(i)->getNum() == ver1->getNum()) find1 = true;
         else if (eletmp->getVertex(i)->getNum() == ver2->getNum()) find2 = true;
      }
      if(find1 and find2)
      {
        ele = eletmp;
        int elenum = ele->getNum();
        for(int i=0;i<e->getNumVertices();i++)
        {
          for(int j=0;j<eletmp->getNumVertices();j++)
          {
            if(e->getVertex(i)->getNum() == eletmp->getVertex(j)->getNum()){
              nodeIndex.push_back(j);
              break;
            }
          }
        }
        break;
      }
    }
  }
  for(int i=0;i<nv;i++)
  {
     /* Value of normal */
     //get u,v of element
     ele->getNode(nodeIndex[i],u,v,w);
     // compute first derivative at this point
     grads.clear();
     spd->gradfuvw(ele,u,v,w,grads); // How to avoid this ??
     // tangent vector
     SVector3 t1(0.,0.,0.);
     SVector3 t2(0.,0.,0.);
     for(int j=0;j<ele->getNumVertices();j++){
       t1 += SVector3(grads[j](0)*ele->getVertex(j)->x(),grads[j](0)*ele->getVertex(j)->y(),grads[j](0)*ele->getVertex(j)->z());
       t2 += SVector3(grads[j](1)*ele->getVertex(j)->x(),grads[j](1)*ele->getVertex(j)->y(),grads[j](1)*ele->getVertex(j)->z());
     }
     SVector3 nor = crossprod(t1,t2);
     nor.normalize();
     double value = fct->operator()(ele->getVertex(nodeIndex[i])->x(),ele->getVertex(nodeIndex[i])->y(),ele->getVertex(nodeIndex[i])->z());
     /* Value of condition for this point */
     for(int j=0;j<3;j++) // impossible to make a filter on component for now
     {
       //vertexvalue[i+j*nv] = nor(j)*value;
       assembler.setInitialCondition(R[i+j*nv],value*nor(j),whichC);
     }
  }
/* // uncomment when get comp will be activated
  if(!fullDg){
    for (std::vector<Dof>::iterator itd=R.begin();itd!=R.end();++itd)
    {
      Dof key=*itd;
      if (filter(key))
      {
        for (int i=0;i<nv;++i)
        {
          if (e->getVertex(i)->getNum()==key.getEntity())
          {
            //printf("Fix dof number %d comp %d\n",key.getEntity(),key.getType());
            assembler.setInitialCondition(key,vertexvalue[i],whichC);
            break;
          }
        }
      }
    }
  }
  else{
    for (std::vector<Dof>::iterator itd=R.begin();itd!=R.end();++itd)
    {
      Dof key=*itd;
      if (filter(key))
      {
        for (int i=0;i<nv;++i)
        {
          assembler.setInitialCondition(key, value,whichC);
          break;
        }
      }
    }
  }
  */
}


template<class Iterator,class Assembler> void SetNormalInitialDofs(FunctionSpaceBase *space,Iterator itbegin,Iterator itend,
                                                            const nonLinearBoundaryCondition::whichCondition whichC,Assembler &assembler,
                                                            const simpleFunctionTime<double> *fct, FilterDof &filter,bool fullDg,
                                                            elementGroup *groupDom=NULL)
{
  for (Iterator it=itbegin;it!=itend;++it)
  {
    SetNormalInitialDofs(space,it->second,assembler,fct,whichC,filter,fullDg,groupDom);
  }
}

// Number the Dof by a vector (allow to hash it by a dofManagerMultiSystem)
template<class Iterator, class Assembler> void NumberDofsByVector(FunctionSpaceBase &space, Iterator itbegin,
                                                          Iterator itend, Assembler &assembler)
{
 for (Iterator it = itbegin; it != itend; ++it){
    MElement *e = it->second;
    std::vector<Dof> R;
    space.getKeys(e, R);
    assembler.numberDof(R);
  }
}

template<class Iterator, class Assembler> void SparsityDofs(FunctionSpaceBase &space, Iterator itbegin,
                                                          Iterator itend, Assembler &assembler)
{
  for (Iterator it = itbegin; it != itend; ++it){
    MElement *e = it->second;
    std::vector<Dof> R;
    space.getKeys(e, R);
    assembler.sparsityDof(R);
  }
}

// Function Numbering Dof for rigid contact (Create Three Dofs for GC of rigid bodies)
template<class Assembler>
void NumberDofs(rigidContactSpaceBase &space, Assembler &assembler){
  // get Dofs of GC
  std::vector<Dof> R;
  space.getKeysOfGravityCenter(R);
  // Put them into DofManager
  int nbdofs=R.size();
  for (int i=0;i<nbdofs;++i)
    assembler.numberDof(R[i]);
}
#endif //NONLINEARSOLVERALGORITHMS_H_

