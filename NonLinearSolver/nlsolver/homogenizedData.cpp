//
//
// Description: store homogenized data
//
// Author:  <Van Dung NGUYEN>, (C) 2013
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "homogenizedData.h"
#include "nonLinearMechSolver.h"

homogenizedData::homogenizedData(const nonLinearMechSolver* solver, const bool failureExtr, const bool irrEnergExtract): deformationEnergy(0.),plasticEnergy(0.),
irreversibleEnergyExtraction(irrEnergExtract), cohesiveLawExtraction(failureExtr),irreversibleEnergy(0.), _lostSolutionUniquenssCriterion(-1.){
	int numMecaDof = solver->getMicroBC()->getTotalNumberOfMechanicalDofs();
	int numConDof = solver->getMicroBC()->getTotalNumberOfConDofs();
	int numNonConDof = solver->getMicroBC()->getTotalNumberOfNonConDofs();

	if (numMecaDof > 0){
		for (int i=0; i< numConDof; i++){
			dPdgradT.push_back(STensor33(0.));
			dPdT.push_back(STensor3(0.));
			if (solver->getMicroBC()->getOrder() == 2){
				dQdgradT.push_back(STensor43(0.));
				dQdT.push_back(STensor33(0.));
			}
			if (cohesiveLawExtraction){
				dCohesivejumpdgradT.push_back(STensor3(0.));
				dCohesivejumpdT.push_back(SVector3(0.));

				dFdamdgradT.push_back(STensor33(0.));
				dFdamdT.push_back(STensor3(0.));
			}

			if (irreversibleEnergyExtraction){
				dIrreversibleEnergydgradT.push_back(SVector3(0.));
				dIrreversibleEnergydT.push_back(0.);
			}

		}
		for (int i=0; i< numNonConDof; i++){
			dPdgradV.push_back(STensor33(0.));
			if (solver->getMicroBC()->getOrder() == 2){
				dQdgradV.push_back(STensor43(0.));
			}
			if (cohesiveLawExtraction){
				dCohesivejumpdgradV.push_back(STensor3(0.));
				dFdamdgradV.push_back(STensor33(0.));
			}

			if (irreversibleEnergyExtraction){
				dIrreversibleEnergydgradV.push_back(SVector3(0.));
			}
		}
	}

	if (numConDof > 0){
		for (int index=0; index< numConDof; index++){
			fluxT.push_back(SVector3(0.));
			eT.push_back(0.);
			srcT.push_back(0.);
			Cp.push_back(0.);

			if (numMecaDof > 0){
				dfluxTdF.push_back(STensor33(0.));
				dsrcTdF.push_back(STensor3(0.));

				if (solver->getMicroBC()->getOrder() == 2){
					dfluxTdG.push_back(STensor43(0.));
					dsrcTdG.push_back(STensor33(0.));
				}
			}

			std::vector<STensor3> vecSTensor3;
			std::vector<SVector3> vecSVector3;
			std::vector<double> vecValue;
			for (int fieldIndex=0; fieldIndex< numConDof; fieldIndex++){
				vecSTensor3.push_back(STensor3(0.));
				vecSVector3.push_back(SVector3(0.));
				vecValue.push_back(0.);
			}
			dfluxTdgradT.push_back(vecSTensor3);
			dfluxTdT.push_back(vecSVector3);
			dsrcTdgradT.push_back(vecSVector3);
			dsrcTdT.push_back(vecValue);

			if (numNonConDof > 0){
				vecSTensor3.clear();
				vecSVector3.clear();
				for (int fieldIndex=0; fieldIndex< numNonConDof; fieldIndex++){
					vecSTensor3.push_back(STensor3(0.));
					vecSVector3.push_back(SVector3(0.));
				};
				dfluxTdgradV.push_back(vecSTensor3);
				dsrcTdgradV.push_back(vecSVector3);

			}
		}

	}

	if (numNonConDof > 0){
		for (int index=0; index< numNonConDof; index++){
			fluxV.push_back(SVector3(0.));
			if (numMecaDof > 0){
				dfluxVdF.push_back(STensor33(0.));
				if (solver->getMicroBC()->getOrder() == 2){
					dfluxVdG.push_back(STensor43(0.));
				}
			}
			if (numConDof > 0){
				std::vector<STensor3> vecSTensor3;
				std::vector<SVector3> vecSVector3;
				for (int fieldIndex = 0; fieldIndex < numConDof; fieldIndex++){
					vecSTensor3.push_back(STensor3(0.));
					vecSVector3.push_back(SVector3(0.));
				}
				dfluxVdgradT.push_back(vecSTensor3);
				dfluxVdT.push_back(vecSVector3);
			}
			std::vector<STensor3> vecSTensor3;
			for (int fieldIndex = 0; fieldIndex < numNonConDof; fieldIndex++){
				vecSTensor3.push_back(STensor3(0.));
			}
			dfluxVdgradV.push_back(vecSTensor3);
		}
	}	
};
homogenizedData::homogenizedData(const homogenizedData& src):
P(src.P),Q(src.Q),BM(src.BM),fluxT(src.fluxT),eT(src.eT),srcT(src.srcT),Cp(src.Cp),fluxV(src.fluxV),
dPdF(src.dPdF),dPdG(src.dPdG),dPdgradT(src.dPdgradT),dPdT(src.dPdT),dPdgradV(src.dPdgradV),
dQdF(src.dQdF),dQdG(src.dQdG),dQdgradT(src.dQdgradT),dQdT(src.dQdT),dQdgradV(src.dQdgradV),
dfluxTdF(src.dfluxTdF),dfluxTdG(src.dfluxTdG),dfluxTdgradT(src.dfluxTdgradT),dfluxTdT(src.dfluxTdT),dfluxTdgradV(src.dfluxTdgradV),
dfluxVdF(src.dfluxVdF),dfluxVdG(src.dfluxVdG),dfluxVdgradT(src.dfluxVdgradT),dfluxVdT(src.dfluxVdT),dfluxVdgradV(src.dfluxVdgradV),
dsrcTdF(src.dsrcTdF),dsrcTdG(src.dsrcTdG),dsrcTdgradT(src.dsrcTdgradT),dsrcTdT(src.dsrcTdT),dsrcTdgradV(src.dsrcTdgradV),
_cohesiveJump(src._cohesiveJump),_cohesiveTraction(src._cohesiveTraction),_lostSolutionUniquenssCriterion(src._lostSolutionUniquenssCriterion),
_damageVolume(src._damageVolume),_Fdam(src._Fdam),_stressDam(src._stressDam),
_averageCohesiveBandWidth(src._averageCohesiveBandWidth),
dCohesivejumpdF(src.dCohesivejumpdF),dCohesivejumpdG(src.dCohesivejumpdG),dCohesivejumpdgradT(src.dCohesivejumpdgradT),
dCohesivejumpdT(src.dCohesivejumpdT),dCohesivejumpdgradV(src.dCohesivejumpdgradV),
dFdamdF(src.dFdamdF),dFdamdG(src.dFdamdG),dFdamdgradT(src.dFdamdgradT),dFdamdT(src.dFdamdT),dFdamdgradV(src.dFdamdgradV),
deformationEnergy(src.deformationEnergy),plasticEnergy(src.plasticEnergy),irreversibleEnergy(src.irreversibleEnergy),
dIrreversibleEnergydF(src.dIrreversibleEnergydF), dIrreversibleEnergydG(src.dIrreversibleEnergydG),
dIrreversibleEnergydgradT(src.dIrreversibleEnergydgradT),dIrreversibleEnergydT(src.dIrreversibleEnergydT),
dIrreversibleEnergydgradV(src.dIrreversibleEnergydgradV),irreversibleEnergyExtraction(src.irreversibleEnergyExtraction),
cohesiveLawExtraction(src.cohesiveLawExtraction), dUdF(src.dUdF), dUdG(src.dUdG), dQdG_correction(src.dQdG_correction)
{};
homogenizedData& homogenizedData::operator = (const homogenizedData& src){
	P = src.P;
	Q = src.Q;
	BM = src.BM;
	fluxT = src.fluxT;
	eT = src.eT;
	srcT = src.srcT;
	Cp = src.Cp;
	fluxV = src.fluxV;
	dPdF = src.dPdF;
	dPdG = src.dPdG;
	dPdgradT = src.dPdgradT;
	dPdT = src.dPdT;
	dPdgradV = src.dPdgradV;
	dQdF = src.dQdF;
	dQdG =src.dQdG;
	dQdgradT = src.dQdgradT;
	dQdT = src.dQdT;
	dQdgradV = src.dQdgradV;
	dfluxTdF = src.dfluxTdF;
	dfluxTdG = src.dfluxTdG;
	dfluxTdgradT = src.dfluxTdgradT;
	dfluxTdT = src.dfluxTdT;
	dfluxTdgradV = src.dfluxTdgradV;
	dfluxVdF = src.dfluxVdF;
	dfluxVdG = src.dfluxVdG;
	dfluxVdgradT = src.dfluxVdgradT;
	dfluxVdT = src.dfluxVdT;
	dfluxVdgradV = src.dfluxVdgradV;
	dsrcTdF = src.dsrcTdF;
	dsrcTdG = src.dsrcTdG;
	dsrcTdgradT = src.dsrcTdgradT;
	dsrcTdT = src.dsrcTdT;
	dsrcTdgradV = src.dsrcTdgradV;
	_cohesiveJump = src._cohesiveJump;
	_cohesiveTraction = src._cohesiveTraction;
	_lostSolutionUniquenssCriterion = src._lostSolutionUniquenssCriterion;
	_damageVolume = src._damageVolume;
	_Fdam = src._Fdam;
	_stressDam = src._stressDam;
	_averageCohesiveBandWidth = src._averageCohesiveBandWidth;
	dCohesivejumpdF = src.dCohesivejumpdF;
	dCohesivejumpdG = src.dCohesivejumpdG;
	dCohesivejumpdgradT = src.dCohesivejumpdgradT;
	dCohesivejumpdT = src.dCohesivejumpdT;
	dCohesivejumpdgradV = src.dCohesivejumpdgradV;
	dFdamdF = src.dFdamdF;
	dFdamdG = src.dFdamdG;
	dFdamdgradT = src.dFdamdgradT;
	dFdamdT = src.dFdamdT;
	dFdamdgradV = src.dFdamdgradV;
	deformationEnergy = src.deformationEnergy;
	plasticEnergy = src.plasticEnergy;
	irreversibleEnergy = src.irreversibleEnergy;
	dIrreversibleEnergydF = src.dIrreversibleEnergydF;
	dIrreversibleEnergydG = src.dIrreversibleEnergydG;
	dIrreversibleEnergydgradT = src.dIrreversibleEnergydgradT;
	dIrreversibleEnergydT = src.dIrreversibleEnergydT;
	dIrreversibleEnergydgradV = src.dIrreversibleEnergydgradV;

	irreversibleEnergyExtraction = src.irreversibleEnergyExtraction;
	cohesiveLawExtraction = src.cohesiveLawExtraction;
	dUdF = src.dUdF;
	dUdG = src.dUdG;
	dQdG_correction = src.dQdG_correction;
	return *this;
};

void allFirstOrderMechanicsFiles::openFiles(const nonLinearMechSolver* solver,const bool strainToFile, const bool stressToFile, const bool failureToFile){
	if (strainToFile){
		std::string filename  = solver->getFileSavingPrefix()+"strain.csv";
		F_File = Tensor23::createFile(filename);
	}
	if (stressToFile){
		std::string filename =  solver->getFileSavingPrefix()+"stress.csv";
		P_File= Tensor23::createFile(filename);

		filename = solver->getFileSavingPrefix()+"tangent.csv";
		dPdF_File = Tensor43::createFile(filename);

		filename = solver->getFileSavingPrefix()+"defoEnergy.csv";
		defoEnerg_File = Tensor11::createFile(filename);

		filename = solver->getFileSavingPrefix()+"plasticEnergy.csv";
		plasticEnerg_File = Tensor11::createFile(filename);

		filename = solver->getFileSavingPrefix()+"irreversibleEnergy.csv";
		irreversibleEnergFile =Tensor11::createFile(filename);

		filename = solver->getFileSavingPrefix()+"lostSolutionUniquenssCriterion.csv";
		lostSolutionUniquenssCriterion_File = Tensor11::createFile(filename);
	}
};
void allFirstOrderMechanicsFiles::dataToFile(const double time, const nonLinearMechSolver* solver, const homogenizedData* data){
	if (F_File!=NULL){
		Tensor23::writeData(F_File,solver->getMicroBC()->getFirstOrderKinematicalVariable(),time);
	}
	if (P_File !=NULL){
		Tensor23::writeData(P_File,data->getHomogenizedStress(),time);
	}
	if (dPdF_File !=NULL){
		Tensor43::writeData(dPdF_File,data->getHomogenizedTangentOperator_F_F(),time);
	}
	if (defoEnerg_File != NULL){
		Tensor11::writeData(defoEnerg_File,data->getDeformationEnergy(),time);
	}
	if (plasticEnerg_File !=NULL){
		Tensor11::writeData(plasticEnerg_File,data->getPlasticEnergy(),time);
	}
	if (irreversibleEnergFile!=NULL){
		Tensor11::writeData(irreversibleEnergFile,data->getIrreversibleEnergy(),time);
	}
	if (lostSolutionUniquenssCriterion_File!=NULL){
		Tensor11::writeData(lostSolutionUniquenssCriterion_File,data->getLostOfSolutionUniquenessCriterion(),time);
	};
}

void allFailureFiles::openFiles(const nonLinearMechSolver* solver,const bool strainToFile, const bool stressToFile, const bool failureToFile){
	if (failureToFile){
		std::string filename = solver->getFileSavingPrefix()+"cohesiveJump.csv";
		cohesiveJump_File = Tensor13::createFile(filename);

		filename = solver->getFileSavingPrefix()+"damageVolume.csv";
		damageVolume_File = Tensor11::createFile(filename);

		filename = solver->getFileSavingPrefix()+"cohesiveTraction.csv";
		cohesiveTraction_File = Tensor13::createFile(filename);

		filename = solver->getFileSavingPrefix()+"averageCohesiveBandWidth.csv";
		averageCohesiveBandWidth_File = Tensor11::createFile(filename);

		filename = solver->getFileSavingPrefix()+"Fdam.csv";
		FDam_File = Tensor11::createFile(filename);

		filename = solver->getFileSavingPrefix()+"tangentCohesiveJumpDF.csv";
		dCohesiveJumpDF_File = Tensor33::createFile(filename);
	}
};
void allFailureFiles::dataToFile(const double time, const nonLinearMechSolver* solver, const homogenizedData* data){
	if (cohesiveJump_File!=NULL){
		Tensor13::writeData(cohesiveJump_File,data->getHomogenizedCohesiveJump(),time);
	};

	if (damageVolume_File!=NULL){
		Tensor11::writeData(damageVolume_File,data->getActiveDissipationVolume(),time);
	};
	if (cohesiveTraction_File!=NULL){
		Tensor13::writeData(cohesiveTraction_File,data->getHomogenizedCohesiveTraction(),time);
	};
	if (averageCohesiveBandWidth_File!=NULL) {
		Tensor11::writeData(averageCohesiveBandWidth_File,data->getAverageLocalizationBandWidth(),time);
	};
	if (FDam_File != NULL){
		Tensor23::writeData(FDam_File,data->getHomogenizedActiveDissipationDeformationGradient(),time);
	}
	if (dCohesiveJumpDF_File!=NULL){
		Tensor33::writeData(dCohesiveJumpDF_File,data->getHomogenizedTangentOperator_CohesiveJump_F(),time);
	}
};

void allSecondOrderMechanicsFiles::openFiles(const nonLinearMechSolver* solver,const bool strainToFile, const bool stressToFile, const bool failureToFile){
	if (strainToFile){
		std::string filename = solver->getFileSavingPrefix()+"second_strain.csv";
		G_File = Tensor33::createFile(filename);
	}

	if (stressToFile){
		std::string filename = solver->getFileSavingPrefix()+"second_stress.csv";
		Q_File = Tensor33::createFile(filename);

		filename = solver->getFileSavingPrefix()+"firstsecond_tangent.csv";
		dPdG_File = Tensor53::createFile(filename);

		filename = solver->getFileSavingPrefix()+"secondfirst_tangent.csv";
		dQdF_File = Tensor53::createFile(filename);

		filename = solver->getFileSavingPrefix()+"second_tangent.csv";
		dQdG_File = Tensor63::createFile(filename);
		
	}
};

void allSecondOrderMechanicsFiles::dataToFile(const double time, const nonLinearMechSolver* solver, const homogenizedData* data){
	if (G_File!=NULL){
		Tensor33::writeData(G_File,solver->getMicroBC()->getSecondOrderKinematicalVariable(),time);
	}
	if (Q_File!=NULL){
		Tensor33::writeData(Q_File,data->getHomogenizedSecondOrderStress(),time);
	}
	if (dPdG_File!=NULL){
		Tensor53::writeData(dPdG_File,data->getHomogenizedTangentOperator_F_G(),time);
	}
	if (dQdF_File!=NULL){
		Tensor53::writeData(dQdF_File,data->getHomogenizedTangentOperator_G_F(),time);
	}
	if (dQdG_File!=NULL){
		Tensor63::writeData(dQdG_File,data->getHomogenizedTangentOperator_G_G(),time);
	}
};

void allConExtraDofFiles::openFiles(const nonLinearMechSolver* solver,const bool strainToFile, const bool stressToFile, const bool failureToFile){
	if (solver->getMicroBC()->getTotalNumberOfConDofs() > 1){
		if (strainToFile){
			std::string filename  = solver->getFileSavingPrefix()+"gradExtraDof_"+int2str(fieldIndex)+".csv";
			gradT_File = Tensor13::createFile(filename);

			filename  = solver->getFileSavingPrefix()+"ExtraDofValue_"+int2str(fieldIndex)+".csv";
			T_File = Tensor11::createFile(filename);
		};
		if (stressToFile){
			std::string filename = solver->getFileSavingPrefix()+"ExtraDofFlux_"+int2str(fieldIndex)+".csv";
			fluxT_File = Tensor13::createFile(filename);

			filename = solver->getFileSavingPrefix()+"InternalEnergyExtraDof_"+int2str(fieldIndex)+".csv";
			eT_File = Tensor11::createFile(filename);

			filename = solver->getFileSavingPrefix()+"TangentFluxF_"+int2str(fieldIndex)+".csv";
			dFluxTdF_File = Tensor33::createFile(filename);

			filename = solver->getFileSavingPrefix()+"TangentFluxGradExtraDof_"+int2str(fieldIndex)+".csv";
			dFluxTdGradT_File = Tensor23::createFile(filename);

			filename = solver->getFileSavingPrefix()+"TangentFluxExtraDof_"+int2str(fieldIndex)+".csv";
			dFluxTdT_File = Tensor13::createFile(filename);

			filename = solver->getFileSavingPrefix()+"TangentStressGradExtraDof_"+int2str(fieldIndex)+".csv";
			dPdGradT_File = Tensor33::createFile(filename);

			filename = solver->getFileSavingPrefix()+"TangentStressExtraDofValue_"+int2str(fieldIndex)+".csv";
			dPdT_File = Tensor23::createFile(filename);

			filename = solver->getFileSavingPrefix()+"mechanicalSource_"+int2str(fieldIndex)+".csv";
			mecaSource_File = Tensor11::createFile(filename);

			filename = solver->getFileSavingPrefix()+"fieldCapacity_"+int2str(fieldIndex)+".csv";
			Cp_File = Tensor11::createFile(filename);
		}
	}
	else if (solver->getMicroBC()->getTotalNumberOfConDofs() == 1){
		if (strainToFile){
			std::string filename  = solver->getFileSavingPrefix()+"gradExtraDof.csv";
			gradT_File = Tensor13::createFile(filename);

			filename  = solver->getFileSavingPrefix()+"ExtraDofValue.csv";
			T_File = Tensor11::createFile(filename);
		};
		if (stressToFile){
			std::string filename = solver->getFileSavingPrefix()+"ExtraDofFlux.csv";
			fluxT_File = Tensor13::createFile(filename);

			filename = solver->getFileSavingPrefix()+"InternalEnergyExtraDof.csv";
			eT_File = Tensor11::createFile(filename);

			filename = solver->getFileSavingPrefix()+"TangentFluxF.csv";
			dFluxTdF_File = Tensor33::createFile(filename);

			filename = solver->getFileSavingPrefix()+"TangentFluxGradExtraDof.csv";
			dFluxTdGradT_File = Tensor23::createFile(filename);

			filename = solver->getFileSavingPrefix()+"TangentFluxExtraDof.csv";
			dFluxTdT_File = Tensor13::createFile(filename);

			filename = solver->getFileSavingPrefix()+"TangentStressGradExtraDof.csv";
			dPdGradT_File = Tensor33::createFile(filename);

			filename = solver->getFileSavingPrefix()+"TangentStressExtraDofValue.csv";
			dPdT_File = Tensor23::createFile(filename);

			filename = solver->getFileSavingPrefix()+"mechanicalSource.csv";
			mecaSource_File = Tensor11::createFile(filename);

			filename = solver->getFileSavingPrefix()+"fieldCapacity.csv";
			Cp_File = Tensor11::createFile(filename);
		}
	}
};
void allConExtraDofFiles::dataToFile(const double time, const nonLinearMechSolver* solver, const homogenizedData* data){
	if (gradT_File !=NULL){
		Tensor13::writeData(gradT_File,solver->getMicroBC()->getConstitutiveExtraDofDiffusionKinematicalVariable(fieldIndex),time);
	};
	if (T_File!=NULL){
		Tensor11::writeData(T_File,solver->getMicroBC()->getConstitutiveExtraDofDiffusionConstantVariable(fieldIndex),time);
	};
	if (fluxT_File!=NULL){
		Tensor13::writeData(fluxT_File,data->getHomogenizedConstitutiveExtraDofFlux(fieldIndex),time);
	};
	if (eT_File!=NULL){
		Tensor11::writeData(eT_File,data->getHomogenizedConstitutiveExtraDofInternalEnergy(fieldIndex),time);
	};
	if (dFluxTdGradT_File!=NULL){
		Tensor23::writeData(dFluxTdGradT_File,data->getHomogenizedTangentOperator_gradT_gradT(fieldIndex,fieldIndex),time);
	}
	if (dFluxTdT_File!=NULL) {
		Tensor13::writeData(dFluxTdT_File,data->getHomogenizedTangentOperator_gradT_T(fieldIndex,fieldIndex),time);
	};
	if (dPdGradT_File!=NULL) {
		Tensor33::writeData(dPdGradT_File,data->getHomogenizedTangentOperator_F_gradT(fieldIndex),time);
	};
	if (dFluxTdF_File!=NULL) {
		Tensor33::writeData(dFluxTdF_File,data->getHomogenizedTangentOperator_gradT_F(fieldIndex),time);
	};
	if (dPdT_File !=NULL){
		Tensor23::writeData(dPdT_File,data->getHomogenizedTangentOperator_F_T(fieldIndex),time);
	};
	if (mecaSource_File!=NULL){
		Tensor11::writeData(mecaSource_File,data->getHomogenizedConstitutiveExtraDofMechanicalSource(fieldIndex),time);
	}
	if (Cp_File!=NULL){
		Tensor11::writeData(Cp_File,data->getHomogenizedConstitutiveExtraDofFieldCapacityPerUnitField(fieldIndex),time);
	}
};

void allNonConExtraDofFiles::openFiles(const nonLinearMechSolver* solver, const bool strainToFile, const bool stressToFile, const bool failureToFile){
	if (solver->getMicroBC()->getTotalNumberOfNonConDofs() > 1){
		if (strainToFile){
			std::string filename  = solver->getFileSavingPrefix()+"gradNonConExtraDof_"+int2str(fieldIndex)+".csv";
			gradV_File = Tensor13::createFile(filename);
		};
		if (stressToFile){
			std::string filename = solver->getFileSavingPrefix()+"NonConExtraDofFlux_"+int2str(fieldIndex)+".csv";
			fluxV_File = Tensor13::createFile(filename);

			filename = solver->getFileSavingPrefix()+"TangentNonConFluxGradNonConExtraDof_"+int2str(fieldIndex)+".csv";
			dfluxVdgradV_File = Tensor23::createFile(filename);
		}

	}
	else if (solver->getMicroBC()->getTotalNumberOfNonConDofs() == 1){
		if (strainToFile){
			std::string filename  = solver->getFileSavingPrefix()+"gradNonConExtraDof.csv";
			gradV_File = Tensor13::createFile(filename);
		};
		if (stressToFile){
			std::string filename =solver->getFileSavingPrefix()+"NonConExtraDofFlux.csv";
			fluxV_File = Tensor13::createFile(filename);

			filename = solver->getFileSavingPrefix()+"TangentNonConFluxGradNonConExtraDof.csv";
			dfluxVdgradV_File = Tensor23::createFile(filename);
		}


	}
};

void allNonConExtraDofFiles::dataToFile(const double time, const nonLinearMechSolver* solver, const homogenizedData* data){
	if (gradV_File!=NULL){
		Tensor13::writeData(gradV_File,solver->getMicroBC()->getNonConstitutiveExtraDofDiffusionKinematicalVariable(fieldIndex),time);
	};
	if (fluxV_File!=NULL){
		Tensor13::writeData(fluxV_File,data->getHomogenizedNonConstitutiveExtraDofFlux(fieldIndex),time);
	};
	if (dfluxVdgradV_File!=NULL){
		Tensor23::writeData(dfluxVdgradV_File,data->getHomogenizedTangentOperator_gradV_gradV(fieldIndex,fieldIndex),time);
	};
};


homogenizedDataFiles::homogenizedDataFiles(const nonLinearMechSolver* solver): _allFirstOrderFiles(), _allSecondOrderFiles(),_allFailureFiles(){
	for (int index=0; index< solver->getMicroBC()->getTotalNumberOfConDofs(); index++){
		_allConFiles.push_back(allConExtraDofFiles(index));
	}
	for (int index=0; index< solver->getMicroBC()->getTotalNumberOfNonConDofs(); index++){
		_allNonConFiles.push_back(allNonConExtraDofFiles(index));
	}

};

void homogenizedDataFiles::openFiles(const nonLinearMechSolver* solver,
                  const bool strainToFile, const bool stressToFile, const bool failureToFile){
	if (solver->getMicroBC()->getTotalNumberOfMechanicalDofs()>0){
		_allFirstOrderFiles.openFiles(solver,strainToFile,stressToFile,failureToFile);
		if (solver->getMicroBC()->getOrder()==2){
			_allSecondOrderFiles.openFiles(solver,strainToFile,stressToFile,failureToFile);
		}
		if (failureToFile){
			_allFailureFiles.openFiles(solver,strainToFile,stressToFile,failureToFile);
		}
	}
	if (solver->getMicroBC()->getTotalNumberOfConDofs()>0){
		for (int index=0; index < solver->getMicroBC()->getTotalNumberOfConDofs(); index++){
			_allConFiles[index].openFiles(solver,strainToFile,stressToFile,failureToFile);
		}
	}
	if (solver->getMicroBC()->getTotalNumberOfNonConDofs()>0){
		for (int index=0; index < solver->getMicroBC()->getTotalNumberOfNonConDofs(); index++){
			_allNonConFiles[index].openFiles(solver,strainToFile,stressToFile,failureToFile);
		}
	}
};

void homogenizedDataFiles::dataToFile(const double time, const nonLinearMechSolver* solver, const homogenizedData* data){
	_allFirstOrderFiles.dataToFile(time,solver,data);
	_allSecondOrderFiles.dataToFile(time,solver,data);
	_allFailureFiles.dataToFile(time,solver,data);

	for (int index=0; index <_allConFiles.size(); index++){
		_allConFiles[index].dataToFile(time,solver,data);
	}

	for (int index=0; index < _allNonConFiles.size(); index++){
		_allNonConFiles[index].dataToFile(time,solver,data);
	}

};
