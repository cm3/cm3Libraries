//
//
// Description: strain mapping in order to quantify the homogenous deformation of discrete model
// for cellular materials
//
// Author:  <Van Dung NGUYEN>, (C) 2013
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef STRAINMAPPING_H_
#define STRAINMAPPING_H_

#include "partDomain.h"
#include "unknownField.h"
#include "GModel.h"
#include <map>
#include <vector>
#include <set>

class nlsDofManager;
class strainMapping{
  protected:
    nonLinearMechSolver& _solver;
    std::string _meshFileName;
    //
    GModel *_pModel;
    elementGroup* _g;
    std::map<MVertex*, std::set<MElement*> > _nodeElementMap;

    std::map<MElement*, SVector3> _elementValue;
    std::map<MVertex*, SVector3> _nodeValue;

    std::map<MElement*,std::map<partDomain*,elementGroup*> > _meanMap;
    
    int _lastSaveStep;
    int _numStepBetweenTwoSaves;
    
  protected:
    void getMeanValueOnElement(nlsDofManager* p, MElement* e, SVector3& val);
    void solve();
    void buildDisplacementView(const std::string postFileName);
    void buildStrainView(const std::string postFileName);

  public:
    strainMapping(nonLinearMechSolver& s, const int stepArch=1);
    ~strainMapping();
    std::string getMappingMeshFileName() const {return _meshFileName;};
    int getNumStepBetweenTwoSaves() const {return _numStepBetweenTwoSaves;};
    void readMesh(const std::string meshFileName);
    void buildDisplacementViewAndStrainView(int numstep, bool force);
};

#endif // STRAINMAPPING_H_
