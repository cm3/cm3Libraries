//
//
// Description: operation for vertex
//
// Author:  <Van Dung NGUYEN>, (C) 2019
//
// Copyright: See COPYING file that comes with this distribution
//

#ifndef VERTEXGROUPOPERATION_H_
#define VERTEXGROUPOPERATION_H_

#ifndef SWIG
#include "elementGroup.h"
#endif // SWIG

class setInt
{
  public: 
    setInt(){};
    ~setInt(){};
    void insert(int i);
    void insertPhysical(int dim, int phys);
    void clear();
    bool included(const int i) const;
    void copyData(const setInt& src);
    #ifndef SWIG
    std::set<int> values;
    setInt(const setInt& src): values(src.values){}
    setInt* clone() const {return new setInt(*this);};
    #endif //SWIG
};

class linearCombinationOfVertices
{
  public:
    linearCombinationOfVertices(){}
    void setVertex(int verphy, int comp, double fact);
    void clear();
    #ifndef SWIG
    std::map<MVertex*,std::pair<int,double> > combinationV;
    linearCombinationOfVertices(const linearCombinationOfVertices& src): combinationV(src.combinationV){}
    ~linearCombinationOfVertices(){};
    linearCombinationOfVertices* clone() const {return new linearCombinationOfVertices(*this);};
    #endif //SWIG
};

class vertexGroupOperation
{
  #ifndef SWIG
  public:
    vertexGroupOperation(){}
    vertexGroupOperation(const vertexGroupOperation& src){}
    virtual ~vertexGroupOperation(){}
    virtual void apply(const MVertex* master, SPoint3& p) const = 0;
    virtual vertexGroupOperation* clone() const = 0;
  #endif // SWIG
};

class vertexGroupOperation_trivial : public vertexGroupOperation
{
  #ifndef SWIG
  public:
    vertexGroupOperation_trivial();
    vertexGroupOperation_trivial(const vertexGroupOperation_trivial& src);
    virtual ~vertexGroupOperation_trivial();
    virtual void apply(const MVertex* master, SPoint3& p) const;
    virtual vertexGroupOperation* clone() const {return new vertexGroupOperation_trivial();};
  #endif // SWIG
};


class vertexGroupOperation_symmetric : public vertexGroupOperation
{
  // symmetric plane
  protected:
    #ifndef SWIG
    double a, b, c, d; // to define surface a*x +b*y +c*z +d = 0
    #endif //SWIG
  public:
    vertexGroupOperation_symmetric(double aa, double bb, double cc, double dd);
    #ifndef SWIG
    vertexGroupOperation_symmetric(const vertexGroupOperation_symmetric& src);
    virtual ~vertexGroupOperation_symmetric();
    virtual void apply(const MVertex* master, SPoint3& p) const;
    virtual vertexGroupOperation* clone() const {return new vertexGroupOperation_symmetric(a,b,c,d);};
    #endif // SWIG
};


class vertexGroupOperation_periodic: public vertexGroupOperation
{
  // symmetric plane
  protected:
    #ifndef SWIG
    double Lx, Ly, Lz; // to define vector of periodicity  Lx, Ly, Lz
    #endif //SWIG
  public:
    vertexGroupOperation_periodic(double nx, double ny, double nz);
    void setPeriodicityVector(double x, double y, double z);
    #ifndef SWIG
    vertexGroupOperation_periodic(const vertexGroupOperation_periodic& src);
    virtual ~vertexGroupOperation_periodic();
    virtual void apply(const MVertex* master, SPoint3& p) const;
    virtual vertexGroupOperation* clone() const {return new vertexGroupOperation_periodic(Lx,Ly,Lz);};
    #endif // SWIG
};

#endif // VERTEXGROUPOPERATION_H_