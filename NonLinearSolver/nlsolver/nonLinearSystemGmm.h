//
// Description: System for non linear static scheme
//
//
// Author:  <Gauthier BECKER>, (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
//
// The system solve K Delta x = F so add a vector xsol which contains the actual value of unknowns

#ifndef NONLINEARSYSTEMGMM_H_
#define NONLINEARSYSTEMGMM_H_

#include "nonLinearSystems.h"

// gmm
#include "linearSystemGmm.h"
template <class scalar>
class nonLinearSystemGmm : public linearSystemGmm<scalar>, public nonLinearSystem<scalar>{
 protected:
  mutable bool _flagb;
  std::vector<scalar> *_xsol;
  std::vector<scalar> *_xprev;
  std::vector<scalar> *_xitels;
  std::vector<scalar> *_Fext;
  std::vector<scalar> *_Fint;
  double _timeStep;
  // For external work
  mutable std::vector<scalar> *_Fextprev;
  const double _Tolls;
  const bool _lineSearch;
  double _alphaRneg, _alphaRpos, _alphaLsIte, _alphaLsPrev, _resNeg, _resPos, _resLsIte, _res0;
  int _lsite,_lsitemax;
 public:
  nonLinearSystemGmm(const bool ls=false, const bool tolls=1.e-2,
                     const int lsitemax = 10) : linearSystemGmm<scalar>(), nonLinearSystem<scalar>(),
                                                             _xsol(NULL), _xprev(NULL), _xitels(NULL), _Fext(NULL), _Fint(NULL),
                                                             _Fextprev(NULL), _flagb(false),
                                                             _lineSearch(ls), _Tolls(tolls), _alphaRneg(0.), _alphaRpos(1.),
                                                             _resNeg(0.), _resPos(0.), _lsite(0), _alphaLsIte(0.), _resLsIte(0.),
                                                             _res0(1.), _alphaLsPrev(1.),_lsitemax(lsitemax), _timeStep(0.) {}
  ~nonLinearSystemGmm(){} // // _xsol is destroied by clear() called by ~linearSystemGMM()
#if defined(HAVE_GMM)
  virtual void allocate(int nbRows)
  {
    linearSystemGmm<scalar>::allocate(nbRows);
    if(_xsol !=NULL) delete _xsol; // I don't known which clear is used
    if(_xprev !=NULL) delete _xprev; // I don't known which clear is used
    if(_xitels !=NULL) delete _xitels; // I don't known which clear is used
    if(_Fext !=NULL) delete _Fext; // I don't known which clear is used
    if(_Fint !=NULL) delete _Fint; // I don't known which clear is used
    if(_Fextprev!=NULL) delete _Fextprev; // I don't known which clear is used
    _xsol = new std::vector<scalar>(nbRows);
    _xprev = new std::vector<scalar>(nbRows);
    _xitels = new std::vector<scalar>(nbRows);
    _Fext = new std::vector<scalar>(nbRows);
    _Fint = new std::vector<scalar>(nbRows);
    _Fextprev = new std::vector<scalar>(nbRows);
  }
  virtual void clear(){
    linearSystemGmm<scalar>::clear();
    if(_xsol !=NULL) delete _xsol;
    if(_xprev !=NULL) delete _xprev;
    if(_xitels !=NULL) delete _xitels;
    if(_Fext !=NULL) delete _Fext;
    if(_Fint !=NULL) delete _Fint;
    if(_Fextprev!=NULL) delete _Fextprev;
    _xsol=NULL;
    _xprev=NULL;
    _xitels=NULL;
    _Fext=NULL;
    _Fint=NULL;
    _Fextprev=NULL;
    _flagb = false;
  }
  virtual void restart(){
    int nbRows=_xsol->size();
    int trow = nbRows;
    restartManager::restart(trow);
    if(trow != (int) nbRows) Msg::Error("Cannot load a petsc step with a different size from the saved one");

    restartManager::restart(*_xsol);
    restartManager::restart(*_xitels);
    restartManager::restart(*_xprev);
    restartManager::restart(*_Fext);
    restartManager::restart(*_Fint);
    restartManager::restart(_timeStep);
    Msg::Error("nonLinearSystemGmm::restart has not been tested yet");
  }
  virtual void getFromSolution(int row, scalar &val) const
  {
    val = (*_xsol)[row];
  }
  virtual int systemSolve(){
    // _b = _Fext - _Fint
    if(!_flagb){
      for(unsigned int i = 0; i < this->_b->size(); i++) (*this->_b)[i] = (*_Fext)[i]-(*_Fint)[i];
      _flagb = true;
    }
    linearSystemGmm<scalar>::systemSolve();

    // initialisation of line search
    if(_lineSearch)
    {
      _resNeg = 0.;
      for(unsigned int i = 0; i < _xsol->size(); i++)
      {
        (*_xitels)[i] = (*_xsol)[i]; // copy last solution vector
        _resNeg+= (*this->_b)[i] * (*this->_x)[i];
      }
      _resNeg =-_resNeg; // because b == - residu;
      // other initial values
      _alphaRneg = 0.;
      _alphaRpos = 1.;
      _alphaLsPrev = 1.;
      _alphaLsIte = 1.;
      _resPos = 0.;
      _res0 = _resNeg;
      _flagb = false;
      _lsite = 0;
    }

    // add to _xsol
    for(int i=0;i<_xsol->size(); i++)
    {
      (*_xsol)[i]+=(*this->_x)[i];
    }

    return 1;
  }

  virtual void getFromRightHandSide(int row, scalar &val) const{
    val = (*_Fext)[row];
  }
  virtual void getFromRightHandSidePlus(int row, scalar &val) const{
    val = (*_Fext)[row];
  }
  virtual void getFromRightHandSideMinus(int row, scalar &val) const{
    val = (*_Fint)[row];
  }
  virtual void addToRightHandSide(int row, const scalar &val, int ith=0) // kept for linear constraint
  {
    if(val != 0.0) (*_Fext)[row] += val;
  }
  virtual void addToRightHandSidePlus(int row, const scalar &val)
  {
    if(val != 0.0) (*_Fext)[row] += val;
  }
    virtual void addToRightHandSideMinus(int row, const scalar &val)
  {
    if(val != 0.0) (*_Fint)[row] += val;
  }
  
  
  virtual void zeroMatrix(const nonLinearSystemBase::whichMatrix wm)
  {
    if (wm == nonLinearSystemBase::mass)
    {
      Msg::Error("mass matrix does not exist");
    }
    else if (wm == nonLinearSystemBase::stiff)
    {
      linearSystemGmm<scalar>::zeroMatrix();
    }
    else
    {
      Msg::Error("matrix does not exist");
    }
    
  }
  

  virtual void zeroRightHandSide(){
    linearSystemGmm<scalar>::zeroRightHandSide();
    for(unsigned int i = 0; i < _Fext->size(); i++) (*_Fext)[i] = 0.;
    for(unsigned int i = 0; i < _Fint->size(); i++) (*_Fint)[i] = 0.;
    _flagb = false;
  }
  virtual double normInfRightHandSide() const {
    // compute _b
    if(!_flagb){
      for(unsigned int i = 0; i < this->_b->size(); i++) (*this->_b)[i] = (*_Fext)[i]-(*_Fint)[i];
      _flagb = true;
    }
    return linearSystemGmm<scalar>::normInfRightHandSide();
  }
  virtual double norm0Inf() const{
    double nor=0;
    double temp;
    for(unsigned int i=0;i<_Fext->size();i++){
      temp = (*_Fext)[i];
      if(temp<0) temp = -temp;
      if(nor<temp) nor=temp;
    }
    double tnor = nor;
    nor = 0;
    for(unsigned int i=0;i<_Fint->size();i++){
      temp = (*_Fint)[i];
      if(temp<0) temp = -temp;
      if(nor<temp) nor=temp;
    }
    return tnor+nor;
  }

    // function for linear search iteration return 1 when line search is converged
  virtual int lineSearch()
  {
    if(!_lineSearch) return 1;
    // compute (Fext-Fint)
    if(!_flagb){
      for(unsigned int i=0; i< this->_b->size(); i++)
      {
        (*this->_b)[i] = (*_Fext)[i] - (*_Fint)[i];
      }
    }
    _flagb = false;
    _resLsIte = 0.;
    for(unsigned int i=0; i < this->_b->size(); i++)
    {
      _resLsIte+=(*this->_b)[i]*(*this->_x)[i];
    }
    _resLsIte = - _resLsIte; // due to definition of _b that is -residu
    // new alpha value
    if(_lsite == 0)
    {
      if(_resLsIte < 0)
      {
        _alphaRpos+=1.; // for first itration we want _resPos > 0 increase alpha from 1 if it is not the case
        _alphaLsIte = _alphaRpos;
        if(_alphaRpos > 3)
        {
          for(unsigned int i=0; i<_xsol->size(); i++)
          {
            (*_xsol)[i] = (*this->_x)[i] + (*_xitels)[i]; //alpha = 1
          }
          return 1;
        }
      }
      else
      {
        _lsite++; // increase iteration counter;
        _resPos = _resLsIte;
        // new alpha
        _alphaLsPrev = _alphaLsIte;
        _alphaRpos = _alphaLsIte;
        _alphaLsIte = _alphaRneg - _resNeg*(_alphaRpos-_alphaRneg)/(_resPos - _resNeg);
      }
    }
    else if(_lsite > _lsitemax) // divergence of process
    {
      for(unsigned int i=0; i<_xsol->size(); i++)
      {
        (*_xsol)[i] = (*this->_x)[i] + (*_xitels)[i]; //alpha = 1
      }
      return 1;
    }
    else
    {
      _lsite++; // increase iteration counter;
      // check convergence on residu
      double relres = _resLsIte / _res0;
      if(relres < 0.) relres = -relres;
      if(relres < _Tolls) return 1; // achieve convergence
      // check convergence on alpha
      relres = (_alphaLsIte - _alphaLsPrev) / _alphaLsPrev;
      if(relres < 0.) relres = -relres;
      if(relres < _Tolls) return 1; // achieve convergence

      // update value of residu
      _alphaLsPrev = _alphaLsIte;
      if(_resLsIte <= 0.)
      {
        _resNeg = _resLsIte;
        _alphaRneg = _alphaLsIte;
      }
      else
      {
        _resPos = _resLsIte;
        _alphaRpos = _alphaLsIte;
      }
      // new alpha
      _alphaLsIte = _alphaRneg - _resNeg*(_alphaRpos-_alphaRneg)/(_resPos - _resNeg);
    }
    // new positions
    for(unsigned int i=0; i<_xsol->size(); i++)
    {
      (*_xsol)[i] = _alphaLsIte*(*this->_x)[i] + (*_xitels)[i];
    }
    return 0;
  }
  virtual void resetUnknownsToPreviousTimeStep()
  {
    for(unsigned int i=0; i<_xsol->size(); i++)
    {
      (*_xsol)[i] = (*_xprev)[i];
    }
  }
  virtual void nextStep()
  {
    for(unsigned int i=0; i<_xsol->size(); i++)
    {
      (*_xprev)[i] = (*_xsol)[i];
    }
  }
  virtual double getExternalWork(const int syssize) const
  {
    double wext=0;
    for(int i=0;i<syssize;i++)
    {
      wext += ((*_Fext)[i] + (*_Fextprev)[i])*((*_xsol)[i] - (*_xprev)[i]);
      (*_Fextprev)[i] = (*_Fext)[i];
    }
    for(int i=syssize;i<_xsol->size();i++)
    {
      (*_Fextprev)[i] = (*_Fext)[i];
    }
    return 0.5*wext;
  }
  virtual void setTimeStep(const double dt){_timeStep = dt;}
  virtual double getTimeStep() const{return _timeStep;}

  virtual double getKineticEnergy(const int syssize) const{return 0.;} // no kinetic energy in a QS scheme
  virtual void addToMatrix(int row, int col, const scalar &val, const nonLinearSystemBase::whichMatrix wm)
  {
   #ifdef _DEBUG
    if(wm!=nonLinearSystemBase::stiff)
      Msg::Error("Try to assemble a matrix different from stiffness in a QS Gmm system");
    else
   #endif //_DEBUG
     this->linearSystemGmm<scalar>::addToMatrix(row,col,val);
  }
  virtual void setInitialCondition(int row, scalar val, const typename nonLinearBoundaryCondition::whichCondition wc)
  {
    if(wc!=nonLinearBoundaryCondition::position)
    {
      Msg::Error("Can set a velocity or a aceleration in a QS scheme");
      return;
    }
    if(this->isAllocated())
    {
      (*_xsol)[row] = val;
      (*_xprev)[row] = val;
    }
    else{
      Msg::Error("Can't set an initial value in a non allocated system!");
    }
  }
  virtual void setDynamicRelaxation(const bool dynrel)
  {
    Msg::Error("Cannot use dynamic relaxation in an QS system");
  }
  virtual void getFromSolution(int row, scalar &val, const nonLinearBoundaryCondition::whichCondition wv) const
  {
    if(wv!=nonLinearBoundaryCondition::position)
    {
      Msg::Error("Cannot take velocity or acceleration in a QS scheme");
      val=0.;
      return;
    }
    this->getFromSolution(row,val);
  }
  virtual void getFromMatrix(int row, int col, scalar &val,const nonLinearSystemBase::whichMatrix wm) const
  {
    if(wm!=nonLinearSystemBase::stiff)
    {
      Msg::Error("A QS system has only a stiffness matrix. So cannot get a value in another matrix");
      val=0;
      return;
    }
    this->linearSystemGmm<scalar>::getFromMatrix(row,col,val);
  }
  #if defined(HAVE_MPI)
  virtual double* getArrayIndexPreviousRightHandSidePlusMPI(const int index)
  {
    Msg::Error("This function cannot be used in a QS Gmm scheme"); // has to return a reference on an array Impossible with a std::vector
    return NULL;
  }
  #endif // HAVE_MPI

  #else
  virtual void getFromRightHandSidePlus(int row, scalar &val) const{};
  virtual void getFromRightHandSideMinus(int row, scalar &val) const{};
  virtual void getFromRightHandSide(int row, scalar &val) const{};
  virtual void addToRightHandSidePlus(int row, const scalar &val){};
  virtual void addToLoadVector(int row, const scalar& val){} // this used in path following context
  virtual void zeroLoadVector() {}
  virtual void addToRightHandSideMinus(int row, const scalar &val){};
  virtual double norm0Inf() const{return 0.;};
  virtual int lineSearch(){return 0;} // if 1 the process in converged explicit no lineSearch so OK
  virtual void resetUnknownsToPreviousTimeStep(){};
  virtual void copy(const IPStateBase::whichState ws1, const IPStateBase::whichState ws2){}
  virtual void nextStep(){};
  virtual void setTimeStep(const double dt){};
  virtual double getTimeStep() const{return 0.;};
  virtual void restart(){Msg::Error("A restart file cannot be created as the function is not implemented for this scheme");}
  virtual double getExternalWork(const int syssize) const{return 0.;};
  virtual double getKineticEnergy(const int syssize) const{return 0.;};
  virtual void zeroMatrix(const nonLinearSystemBase::whichMatrix wm){}
  virtual void addToMatrix(int row, int col, const scalar &val, const nonLinearSystemBase::whichMatrix wm){};
  virtual void setInitialCondition(int row, scalar val, const typename nonLinearBoundaryCondition::whichCondition wc){};
  virtual void setDynamicRelaxation(const bool dynrel){};
  virtual void getFromSolution(int row, scalar &val, const nonLinearBoundaryCondition::whichCondition wv) const{};
  virtual void getFromMatrix(int row, int col, scalar &val,const nonLinearSystemBase::whichMatrix wm) const{};
  #if defined(HAVE_MPI)
  virtual double* getArrayIndexPreviousRightHandSidePlusMPI(const int index){ return NULL;};
  #endif // HAVE_MPI

  #endif // HAVE_GMM
};

#endif //NONLINEARSYSTEMGMM_H_