//
// Description: solve Ax = b with GMRESk algorithms written in MPI
//
// Author:  <Gauthier BECKER>, (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "linearSystemGMRESk.h"
#include <math.h>
#include <stdio.h>
#if defined(HAVE_BLAS)
// extern blas function declaration
#if !defined(F77NAME)
#define F77NAME(x) (x##_)
#endif // F77NAME
extern "C" {
  void F77NAME(daxpy)(int *n, double *alpha, double *x, int *incx, double *y, int *incy);
  void F77NAME(dscal)(int *n, double *alpha,double *x,  int *incx);
  void F77NAME(dcopy)(int *n, double *x,int *incx, double *y, int *incy);
  double F77NAME(ddot)(int *n,double *x,int *incx, double *y, int *incy);
  int F77NAME(idamax)(int *n,double *x,int *incx); // WARNING FIRST INDEX = 1 IN PLACE OF ZERO
  void F77NAME(dgemv)(const char *trans, int *m, int *n,double *alpha, double *a, int *lda,
                      double *x, int *incx, double *beta,double *y, int *incy);
  void F77NAME(dgemm)(const char *transa, const char *transb, int *m, int *n, int *k,
                      double *alpha, double *a, int *lda,
                      double *b, int *ldb, double *beta,
                      double *c, int *ldc);
}
#endif // HAVE_BLAS

#if defined(HAVE_MPI)
#include <mpi.h>
#endif // HAVE_MPI

// For debug
// print for debug
void printVector(double *a,int vsize,const char *vname){
  double val;
  for(int i=0; i<vsize;i++){
    val = a[i];
    printf("%s comp %d %e\n",vname,i,val);
  }
  printf("---\n");
}

void printMatrix(double *a,const int nr,const int nc,const char *vname){
  printf("print of matrix %s\n",vname);
  for(int i=0; i<nr;i++){
    for(int j=0;j<nc;j++)
      printf("(%d,%d)= %e , ",i,j,a[i+nr*j]);
    printf("\n");
  }
  printf("---\n");
}

double dot(const int nr,const double *a,const double *b)
{
  double d=0.;
  for(int i=0;i<nr;i++) d+=a[i]*b[i];
  return d;
}

template<> void linearSystemGMRESk<double>::clear()
{
  if(_a != NULL) delete[] _a;
  if(_x != NULL) delete[] _x;
  if(_b != NULL) delete[] _b;
  if(_sizeRank != NULL) delete[] _sizeRank;
  if(_V != NULL) delete[] _V;
  if(_r != NULL) delete[] _r;
  if(_Vku != NULL) delete[] _Vku;
  if(_H !=NULL) delete[] _H;
  if(_y !=NULL) delete[] _y;
  if(_c !=NULL) delete[] _c;
  if(_s !=NULL) delete[] _s;
  if(_z !=NULL) delete[] _z;
  for(int i=0;i<_k+1;i++)
    if(_VdotVu[i]!=NULL){
      delete[] _VdotVu[i];
      _VdotVu[i] = NULL;
    }
  _a = NULL;
  _x = NULL;
  _b = NULL;
  _sizeRank = NULL;
  _V = NULL;
  _r = NULL;
  _Vku = NULL;
  _H = NULL;
  _y = NULL;
  _c = NULL;
  _s = NULL;
  _z = NULL;
  _nbRows = 0;
  _nbColumns = 0;
}

// be aware the number of column has to be prescribed before allocation (otherwise square matrix)
template<> void linearSystemGMRESk<double>::allocate(int nbRows)
{
  if(_nbColumns == 0) _nbColumns = nbRows;
  _nbRows = nbRows;
  _a = new double[_nbRows*_nbColumns];
  _b = new double[_nbColumns];
  _x = new double[_nbColumns];
  int totalunknowns;
 #if defined(HAVE_MPI)
  if(Msg::GetCommSize() > 1){
    totalunknowns=0;
    for(int i=0;i<Msg::GetCommSize();i++) _sizeRank[i] = 0;
    _sizeRank[Msg::GetCommRank()] = _nbColumns;
    MPI_Allreduce(MPI_IN_PLACE,_sizeRank,Msg::GetCommSize(),MPI_INT,MPI_SUM,MPI_COMM_WORLD);
    for(int i=0;i<Msg::GetCommSize();i++) totalunknowns += _sizeRank[i];
  }
  else
 #endif // HAVE_MPI
  {
    totalunknowns = _nbColumns;
  }

  // initial value of _x
  for(int i=0;i<_nbColumns;i++) _x[i] = 0.;

  // number of kvector
  if(totalunknowns < _k)
  {
    _k = totalunknowns;
    _kp1 = _k+1;
  }

  // init cache data
  _V = new double[_nbColumns*_kp1];
  _r = new double[_nbColumns];
  _Vku = new double[_nbColumns*2];
  _y = new double[_kp1];
  _c = new double[_kp1];
  _s = new double[_kp1];
  _z = new double[_kp1];

   // initialization of cache data for system resolution
   _H = new double[_kp1*_k];
   for(int i=0;i<_kp1;i++)
     _VdotVu[i] = new double[i+i+2];
  _isAllocated = true;
}

template<> void linearSystemGMRESk<double>::getFromMatrix(int row, int col, double &val) const
{
  val = _a[row + _nbRows*col];
}

template<> void linearSystemGMRESk<double>::addToRightHandSide(int row, const double &val, int ith)
{
  _b[row] += val;
}

template<> void linearSystemGMRESk<double>::getFromRightHandSide(int row, double &val) const
{
  val = _b[row];
}

template<> double linearSystemGMRESk<double>::normInfRightHandSide() const
{

  double norlocal;
 #if defined(HAVE_BLAS)
  int INCX=1;
  int NC = _nbColumns;
  int indexmax = F77NAME(idamax)(&NC,_b,&INCX)-1; // idamax first index = 1 !!
  norlocal = fabs(_b[indexmax]);
 #else
  norlocal = _b[0];
  if(norlocal < 0) norlocal = -norlocal;
  for(int i=1;i<_nbColumns;i++)
  {
    if(_b[i] > norlocal) norlocal = _b[i];
    else if(-_b[i] > norlocal) norlocal = -_b[i];
  }
 #endif // HAVE_BLAS
 #if defined(HAVE_MPI)
  if(Msg::GetCommSize() != 1)
  {
    MPI_Allreduce(MPI_IN_PLACE,&norlocal,1,MPI_DOUBLE,MPI_MAX,MPI_COMM_WORLD);
  }
 #endif //HAVE_MPI
  return norlocal;
}

template<> void linearSystemGMRESk<double>::addToMatrix(int row, int col, const double &val)
{
  int index = row + _nbRows*col;
  _a[index] += val;
}

template<> void linearSystemGMRESk<double>::getFromSolution(int row, double &val) const
{
  val = _x[row];
}

template<> void linearSystemGMRESk<double>::zeroMatrix()
{
// dscal doesn't put = 0 but multiply
 int index = _nbRows*_nbColumns;
// #if defined(HAVE_BLAS)
//  int INCX = 1;
//  double alpha = 0.;
//  F77NAME(dscal)(&index,&alpha,_a,&INCX);
// #else
  for(int i=0;i<index;i++) _a[i] = 0.;
// #endif // HAVE_BLAS
}

template<> void linearSystemGMRESk<double>::zeroRightHandSide()
{
// dscal doesn't put = 0 but multiply
// #if defined(HAVE_BLAS)
//  int INCX = 1;
//  double alpha = 0.;
//  F77NAME(dscal)(&_nbRows,&alpha,_b,&INCX);
// #else
  for(int i=0;i<_nbRows;i++) _b[i] = 0.;
// #endif // HAVE_BLAS
}

template<> int linearSystemGMRESk<double>::systemSolve()
{
  Msg::Error("The solve function of linearSystemGMRESk doesn't work");
  // compute initial residu (local)
  printVector(_b,_nbColumns,"b");
 #if defined(HAVE_BLAS)
  int INCX = 1;
  double malpha = -1.;
  double alpha = 1.;
  double alpha0 =0.;
  double beta = 1.;
  F77NAME(dcopy)(&_nbColumns,_b,&INCX,_r,&INCX);
  F77NAME(dgemv)("N",&_nbRows,&_nbColumns,&malpha,_a,&_nbRows,_x,&INCX,&beta,_r,&INCX);
 #else
  for(int i=0;i<_nbRows;i++)
  {
    double val=0.;
    for(int j=0;j<_nbColumns;j++)
    {
      val += _a[i + _nbRows*j]*_x[j];
    }
    _r[i] = _b[i] - val;
  }
 #endif // HAVE_BLAS

  // iterative procedure
  int jj;
  for(jj=0;jj<_maxGMRESite;jj++)
  {
    // compute euclidienne norm of _r
    double rnorml2;
   #if defined(HAVE_BLAS)
    rnorml2 = F77NAME(ddot)(&_nbColumns,_r,&INCX,_r,&INCX);
   #else
    rnorml2 = 0.;
    for(int i=0;i<_nbColumns;i++) rnorml2 += _r[i]*_r[i];
   #endif // HAVE_BLAS
   #if defined(HAVE_MPI)
    if(Msg::GetCommSize() > 1)
    {
      MPI_Allreduce(MPI_IN_PLACE,&rnorml2,1,MPI_DOUBLE,MPI_SUM,MPI_COMM_WORLD);
    }
   #endif // HAVE_MPI
    rnorml2 = sqrt(rnorml2);

    /* reset _y and normalize _r */
   #if defined(HAVE_BLAS)
    _y[0] = rnorml2;
    for(int i=1;i<_k;i++) _y[i] = 0.;
    double invrnorml2 = 1./rnorml2;
    F77NAME(dscal)(&_nbColumns,&invrnorml2,_r,&INCX);
   #else
    _y[0] = rnorml2;
    for(int i=1;i<_k;i++) _y[i] = 0.;
    double invrnorml2 = 1./rnorml2;
    for(int i=0;i<_nbColumns;i++) _r[i]*=invrnorml2;
   #endif // HAVE_BLAS

    /* TRICKY PART ARNOLDI (EFFICIENT // ??) */
    // init of first Vector (copy residu in first column of V
   #if defined(HAVE_BLAS)
//    int nbcompV = _nbColumns*_kp1;
//    F77NAME(dscal)(&nbcompV,&alpha0,_V,&INCX); // check all components of V are put =0
      F77NAME(dcopy)(&_nbColumns,_r,&INCX,_V,&INCX);
   #else
    for(int i=0;i<_nbColumns;i++) _V[i] = _r[i];
   #endif // HAVE_BLAS

    for(int k=0;k<_k;k++)
    {
      // u0 local (and local scalar product)
     #if defined(HAVE_BLAS)
      // u0 (beawre mpi reduction on "distant" dofs)
      F77NAME(dgemv)("N",&_nbRows,&_nbColumns,&alpha,_a,&_nbRows,&_V[k*_nbColumns],&INCX,&alpha0,&_Vku[_nbColumns],&INCX); // Warning MPI for comm
      // copy Vk in _Vku
      F77NAME(dcopy)(&_nbColumns,&_V[k*_nbColumns],&INCX,_Vku,&INCX);
      // scalar product (regroup all in a Matrix Vector product);
 //     int M = _kp1, N = 2, K = _nbColumns;
 //     int LDA = _kp1, LDB = _nbColumns, LDC = _kp1;
 //     F77NAME(dgemm)("T","N",&M,&N,&K,&alpha,_V,&LDA,_Vku,&LDB,&alpha0,_VdotVu[k],&LDC); // All scalar product in one matrix -vector product better but it doesn't work ...
      for(int i=0;i<=k;i++) _VdotVu[k][i+(k+1)] = F77NAME(ddot)(&_nbColumns,&_Vku[_nbColumns],&INCX,&_V[_nbColumns*i],&INCX);
      // idem Vi*Vk
      for(int i=0;i<=k;i++) _VdotVu[k][i] = F77NAME(ddot)(&_nbColumns,_Vku,&INCX,&_V[_nbColumns*i],&INCX);
      // MPI Comm
      #if defined(HAVE_MPI)
      if(Msg::GetCommSize() > 1)
      {
        MPI_Allreduce(MPI_IN_PLACE,_VdotVu[k],2*k,MPI_DOUBLE,MPI_SUM,MPI_COMM_WORLD);
      }
      #endif // HAVE_MPI

      // //isation of this part (_H on all cpu anyway ??)
      int kp1 = k+1;
      F77NAME(dcopy)(&kp1,&_VdotVu[k][kp1],&INCX,&_H[_kp1*k],&INCX);
      for(int i=0;i<=k;i++){
        //_H[i + (_k+1)*k] = _VdotVu[k][i+(k+1)];
        for(int l=0;l<i;l++)
        {
          _H[i + _kp1*k] -= (_H[l +_kp1*k]*_VdotVu[i][l]); // with BLAS ??
        }
     }
     // end //isation ??
     // update u
     for(int i=0;i<=k;i++){
       double malpha = - _H[i + _kp1*k];
       F77NAME(daxpy)(&_nbColumns,&malpha,&_V[_nbColumns*i],&INCX,&_Vku[_nbColumns],&INCX);
     }
     // norm of u
     double nru = F77NAME(ddot)(&_nbColumns,&_Vku[_nbColumns],&INCX,&_Vku[_nbColumns],&INCX);
     #if defined(HAVE_MPI)
     if(Msg::GetCommSize() > 1)
     {
        double nrumpi;
        MPI_Allreduce(&nru,&nrumpi,1,MPI_DOUBLE,MPI_SUM,MPI_COMM_WORLD);
        _H[(k+1) + _kp1*k] = sqrt(nrumpi);
     }
     else
     #endif // HAVE_MPI
     {
       _H[(k+1) + _kp1*k] = sqrt(nru);
     }
     // Vk+1
     double invHkp1k = 1./_H[(k+1) + _kp1*k];
     F77NAME(daxpy)(&_nbColumns,&invHkp1k,&_Vku[_nbColumns],&INCX,&_V[_nbColumns*(k+1)],&INCX);

     #else

      for(int i=0;i<_nbRows;i++)
      {
        double val=0.;
        for(int j=0;j<_nbColumns;j++)
        {
          val += _a[i +_nbRows *j]*_V[j+ k*_nbColumns];
        }
        _Vku[i+_nbColumns] = val;
      }
/*
      // grosse berta OK
      for(int i=0;i<=k;i++)
      {
        _H[i + (_k+1)* k] = dot(_nbColumns,&_Vku[_nbColumns],&_V[_nbColumns*i]);
        for(int j=0;j<_nbColumns;j++) _Vku[j + _nbColumns] -= _H[i + _kp1* k]*_V[j+_nbColumns*i];
      }
      // norm u
//      printVector(_u,_nbColumns,"u");
      double nru = dot(_nbColumns,&_Vku[_nbColumns],&_Vku[_nbColumns]);
      _H[(k+1) + (_k+1)*k]= sqrt(nru);
      for(int j=0;j<_nbColumns;j++) _V[j + _nbColumns*(k+1)] = 1./_H[(k+1) + (_k+1)*k] * _Vku[j+_nbColumns];
*/
      // compute scalar product u*Vk
      for(int i=0;i<=k;i++) _VdotVu[k][i+(k+1)] = dot(_nbColumns,&_Vku[_nbColumns],&_V[_nbColumns*i]);
      // idem Vi*Vk
      for(int i=0;i<=k;i++) _VdotVu[k][i] = dot(_nbColumns,&_V[_nbColumns*k],&_V[_nbColumns*i]);
      #if defined(HAVE_MPI)
       if(Msg::GetCommSize() > 1)
       {
         MPI_Allreduce(MPI_IN_PLACE,&_Vku[k],2*k,MPI_DOUBLE,MPI_SUM,MPI_COMM_WORLD);
       }
      #endif // HAVE_MPI

      for(int i=0;i<=k;i++){
        _H[i + _kp1*k] = _VdotVu[k][i+(k+1)];
        for(int l=0;l<i;l++)
        {
          _H[i + _kp1*k] -= (_H[l +_kp1*k]*_VdotVu[i][l]);
        }
     }
     // update u
     for(int i=0;i<=k;i++)
      for(int l=0;l<_nbColumns;l++)
        _Vku[l + _nbColumns] -= _H[i + _kp1*k]*_V[l+ _nbColumns*i];

 //   printVector(&_Vku[_nbColumns],_nbColumns,"u");

    // V(k+1)
    double nru = dot(_nbColumns,&_Vku[_nbColumns],&_Vku[_nbColumns]);
    #if defined (HAVE_MPI)
    if(Msg::GetCommSize() > 1)
    {
      double nrumpi;
      MPI_Allreduce(&nru,&nrumpi,1,MPI_DOUBLE,MPI_SUM,MPI_COMM_WORLD);
      nru = nrumpi;
    }
    #endif // HAVE_MPI
    _H[(k+1) + (_k+1)*k]= sqrt(nru);
      for(int j=0;j<_nbColumns;j++) _V[j + _nbColumns*(k+1)] = 1./_H[(k+1) + _kp1*k] * _Vku[j + _nbColumns];
    #endif // HAVE_BLAS
    }
    /* end Arnoldi */

    // use of BLAS  and MPI operations in the following loop??
    // Do operation on all rank ??
    int ii,nr; // values used after loop
    double rho; // idem
    for(ii=0; ii < _k; ii++)
    {
      for(int k=1; k<=ii; k++) // for ii=0 this loop is skip so no problem for initialization of c and s
      {
        int km1 = k-1;
        int hind = km1 + _kp1*ii;
        double tmp = _H[hind];
        _H[hind] = _c[km1]*_H[hind] + _s[km1]*_H[hind+1];
        _H[hind+1]= -_s[km1]*tmp + _c[km1]*_H[hind+1];

      }
      int hind = ii + _kp1*ii;
      double invdelta = 1./sqrt(_H[hind]*_H[hind] + _H[hind+1]*_H[hind+1]);
      _c[ii] = _H[hind]*invdelta;
      _s[ii] = _H[hind+1]*invdelta;

      _H[hind] = _c[ii]*_H[hind] + _s[ii]*_H[hind+1];

      hind = ii*_kp1;
      for(int k=ii+1; k<_kp1;k++)
      {
        _H[hind+k] = 0.;
      }

      _y[ii+1] = -_s[ii]*_y[ii];
      _y[ii] = _c[ii]*_y[ii];

      rho = _y[ii+1];
      if(rho < 0.) rho = -rho;
      if(rho < _tolGMRES)
      {
        nr = ii;
        break;
      }
    }
//   #if defined(HAVE_BLAS)
//    int kp1 = _kp1;
//    F77NAME(dscal)(&kp1,&alpha0,_z,&INCX);
//   #else
    for(int k=0;k<_kp1;k++) _z[k] = 0.;
//   #endif // HAVE_BLAS

    if(ii >= (_k-1))
    {
      nr = _k;
      _z[nr-1] = _y[nr-1] / _H[(nr-1) +_kp1*(nr-1)];
    }

    for(int k=nr-2; k>=0; k--)
    {
      _z[k] = _y[k];
      for(int ll=k+1;ll<nr;ll++)
      {
        _z[k] -= _H[k +_kp1*ll]*_z[ll];
      }
      _z[k] /= _H[k +_kp1*k];
    }

    for(int i=0;i<nr; i++)
    {
     #if defined(HAVE_BLAS)
      F77NAME(daxpy)(&_nbColumns,&_z[i],&_V[_nbColumns*i],&INCX,_x,&INCX);
     #else
      for(int k=0;k<_nbColumns;k++)
      {
        _x[k] += _z[i]*_V[k + _nbColumns*i];
      }
     #endif // HAVE_BLAS
    }

    if(rho < _tolGMRES) break;

    // new residu
   #if defined(HAVE_BLAS)
    F77NAME(dcopy)(&_nbColumns,_b,&INCX,_r,&INCX);
    F77NAME(dgemv)("N",&_nbRows,&_nbColumns,&malpha,_a,&_nbRows,_x,&INCX,&beta,_r,&INCX); // be aware of MPI_COMM on distant dofs
   #else
    for(int i=0;i<_nbRows;i++)
    {
      double val=0.;
      for(int j=0;j<_nbColumns;j++)
      {
        val += _a[i + _nbRows*j]*_x[j];
      }
      _r[i] = _b[i] - val;
    }
   #endif // HAVE_BLAS
  Msg::Info("GMRES iteration %d",jj);
  }
  if(jj == _maxGMRESite)
    return 0;
  else
    return 1;
}

template<> void linearSystemGMRESk<double>::printSystem()
{
  printf("Solution vector:\n");
  for(int i=0;i<_nbColumns;i++)
  {
    printf("_x[%d]=%e\n",i,_x[i]);
  }
  printf("\n\n");
  printf("RHS vector:\n");
  for(int i=0;i<_nbColumns;i++)
  {
    printf("_b[%d]=%e\n",i,_b[i]);
  }
  printf("\n\n");
  printf("Matrix:\n");
  for(int i=0;i<_nbRows;i++){
    for(int j=0;j<_nbColumns;j++)
      printf("A(%d,%d)=%e , ",i,j,_a[i+j*_nbRows]);
  printf("\n");
  }
}
