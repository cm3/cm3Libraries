//
// Description: System for non linear static scheme
//
//
// Author:  <Gauthier BECKER>, (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
//
// The system solve K Delta x = F so add a vector xsol which contains the actual value of unknowns
// Only thing that change is the solve function (add operation _xsol += _x)
// The allocation (add the allocation of _xsol)
// The getFromSolution (return _xsol[i] in place of _x[i])
// clear (destroy also _xsol)
// Add a function to set initial values (has to derived from two types HOW TO AVOID THIS)
// Define two members of RightHandSide (optimization for archving and computation of energy)
// to have direct access to Fext and Fint. At begining of system _b = Fext - Fint

#include "nonLinearBC.h"
#include "ipstate.h"

#ifndef _NONLINEARSYSTEMS_H_
#define _NONLINEARSYSTEMS_H_
#if defined(HAVE_MPI)
  #include "mpi.h"
#endif // HAVE_MPI

class nonLinearSystemBase 
{
  public:
    enum rhs{Fext,Fint,LoadVector} ;
    enum whichMatrix{stiff, mass};
  
  protected:
    #if defined(HAVE_MPI)
    // if a system is created at multiple procs (_rootRank +_otherRanks)
    // but the sequential system is used;
    // system data (matrix,solution,RHSs) are only available at _rootRank
    // all data controlled NR scheme (normInfRHS, norm0RHS) must be transferred
    // from  _rootRank to _otherRanks (very important in multiscale analyses)
   
    int _rootRank;
    std::set<int> _otherRanks;
    #endif // HAVE_MPI
    
  public:
    nonLinearSystemBase(){
      #if defined(HAVE_MPI)
      _rootRank = Msg::GetCommRank();
      #endif // HAVE_MPI
    }
    virtual ~nonLinearSystemBase(){}
    #if defined(HAVE_MPI)
    virtual void setWorkingRanks(const int& rootRank, const std::set<int>& others)
    {
      _rootRank =rootRank;
      _otherRanks = others;
    }
    #endif // HAVE_MPI
    virtual void zeroMatrix(const nonLinearSystemBase::whichMatrix wm) = 0;
    virtual void zeroLoadVector() 
    {
      Msg::Error("nonLinearSystemBase::zeroLoadVector is not implemented for this scheme");
    }
    virtual double norm0Inf() const=0;
    virtual int lineSearch(){return 1;} // if 1 the process in converged explicit no lineSearch so OK
    virtual void resetUnknownsToPreviousTimeStep()=0;
    virtual void copy(const IPStateBase::whichState ws1, const IPStateBase::whichState ws2)
    {
      Msg::Error("nonLinearSystemBase::copy is not implemented for this scheme");
    }
    virtual void nextStep()=0;
    virtual void setTimeStep(const double dt)=0;
    virtual double getTimeStep() const=0;
    virtual bool isMultipleRHSAllocated() const 
    {
      Msg::Error("nonLinearSystemBase::isMultipleRHSAllocated is not implemented for this scheme");
      return false;
    }
    virtual void allocateMultipleRHS(int nbRHS)
    {
      Msg::Error("nonLinearSystemBase::allocateMultipleRHS is not implemented for this scheme");
    };
    virtual void zeroMultipleRHS()
    {
      Msg::Error("nonLinearSystemBase::zeroMultipleRHS is not implemented for this scheme");
    }
    virtual bool systemSolveMultipleRHS()
    {
      Msg::Error("nonLinearSystemBase::systemSolveMultipleRHS is not implemented for this scheme");
      return false;
    }
    virtual void restart()
    {
      Msg::Error("A restart file cannot be created as the function is not implemented for this scheme");
    };
    virtual double getExternalWork(const int syssize) const=0;
    virtual double getKineticEnergy(const int syssize) const=0;
    virtual void setDynamicRelaxation(const bool dynrel)=0;
    
    virtual void setNoMassDofKeys(const std::vector<int>& dofKeys)
    {
      Msg::Error("nonLinearSystemBase::setNoMassDofKeys is not implemented for this scheme");
    };
    virtual void useQuasiNewtonMethod(bool flag)
    {
      Msg::Error("nonLinearSystemBase::useQuasiNewtonMethod is not implemented for this scheme");
    };
    #if defined(HAVE_MPI)
    virtual double* getArrayIndexPreviousRightHandSidePlusMPI(const int index)=0;
    #endif // HAVE_MPI
};


template<class scalar>
class nonLinearSystem : public nonLinearSystemBase
{
  
 public:
    nonLinearSystem() : nonLinearSystemBase(){}
    virtual ~nonLinearSystem(){}

    virtual void getFromRightHandSidePlus(int row, scalar &val) const=0;
    virtual void getFromRightHandSideMinus(int row, scalar &val) const=0;
    virtual void addToRightHandSidePlus(int row, const scalar &val)=0;
    virtual void addToLoadVector(int row, const scalar& val) 
    {
      Msg::Error("nonLinearSystem::addToLoadVector is not implemented for this scheme");
    } 
    virtual void addToMultipleRHSMatrix(int row, int col, const scalar& val)
    {
      Msg::Error("nonLinearSystem::addToMultipleRHSMatrix is not implemented for this scheme");
    }
    virtual void getFromMultipleRHSSolution(int row, int col, scalar& val)
    {
      Msg::Error("nonLinearSystem::getFromMultipleRHSSolution is not implemented for this scheme");
    }
    virtual void addToRightHandSideMinus(int row, const scalar &val)=0;
    virtual void addToMatrix(int row, int col, const scalar &val, const nonLinearSystemBase::whichMatrix wm)=0;
    virtual void setInitialCondition(int row, scalar val, const typename nonLinearBoundaryCondition::whichCondition wc)=0;
    virtual void getFromSolution(int row, scalar &val, const nonLinearBoundaryCondition::whichCondition wv) const=0;
    virtual void getFromMatrix(int row, int col, scalar &val,const nonLinearSystemBase::whichMatrix wm) const=0;
};
#endif // _NONLINEARSYSTEMS_H_
