//
// Author:  <Van Dung NGUYEN>, (C) 2023
//
// Copyright: See COPYING file that comes with this distribution
//
//

#include "GPFilter.h"
#include "ipField.h"
#include "ipFiniteStrain.h"

GPFilterTrivial::GPFilterTrivial(): GPFilter(){};
GPFilterTrivial::GPFilterTrivial(const GPFilterTrivial& src):GPFilter(src){}; 
GPFilterTrivial::~GPFilterTrivial(){}
std::string GPFilterTrivial::getName() const  {return "";}
double GPFilterTrivial::getFactor(const IPVariable* ipvcur, const IPVariable* ipvprev) const {return 1.;};
GPFilter* GPFilterTrivial::clone() const {return new GPFilterTrivial(*this);};


GPFilterCurrentVolume::GPFilterCurrentVolume(): GPFilter(){};
GPFilterCurrentVolume::GPFilterCurrentVolume(const GPFilterCurrentVolume& src):GPFilter(src){}; 
GPFilterCurrentVolume::~GPFilterCurrentVolume(){}
std::string GPFilterCurrentVolume::getName() const  {return "CurrentVolume";}
double GPFilterCurrentVolume::getFactor(const IPVariable* ipvcur, const IPVariable* ipvprev) const 
{
  return ipvcur->get(IPField::JACOBIAN);
};
GPFilter* GPFilterCurrentVolume::clone() const {return new GPFilterCurrentVolume(*this);};


GPFilterCoalescence::GPFilterCoalescence(const std::string name): GPFilter(), _name(name){};
GPFilterCoalescence::GPFilterCoalescence(const GPFilterCoalescence& src):GPFilter(src),_name(src._name){}; 
GPFilterCoalescence::~GPFilterCoalescence(){}
std::string GPFilterCoalescence::getName() const  {return _name;}

double GPFilterCoalescence::getFactor(const IPVariable* ipvcur, const IPVariable* ipvprev) const 
{
  return ipvcur->get(IPField::COALESCENCE);
};
GPFilter* GPFilterCoalescence::clone() const {return new GPFilterCoalescence(*this);};

GPFilterActiveDamage::GPFilterActiveDamage(const std::string name): GPFilter(), _name(name){};
GPFilterActiveDamage::GPFilterActiveDamage(const GPFilterActiveDamage& src):GPFilter(src),_name(src._name){}; 
GPFilterActiveDamage::~GPFilterActiveDamage(){}
std::string GPFilterActiveDamage::getName() const  {return _name;}

double GPFilterActiveDamage::getFactor(const IPVariable* ipvcur, const IPVariable* ipvprev) const 
{
  const ipFiniteStrain* ipfcur = dynamic_cast<const ipFiniteStrain*>(ipvcur);
  if (ipfcur==NULL)
  {
    return 0;
  }
  else
  {
    if (ipfcur->dissipationIsActive())
    {
     return 1.;
    }
    else
    {
     return 0;
    }    
  }
};
GPFilter* GPFilterActiveDamage::clone() const {return new GPFilterActiveDamage(*this);};

GPFilterPlasticZone::GPFilterPlasticZone(const std::string name): GPFilter(), _name(name){};
GPFilterPlasticZone::GPFilterPlasticZone(const GPFilterPlasticZone& src):GPFilter(src),_name(src._name){}; 
GPFilterPlasticZone::~GPFilterPlasticZone(){}
std::string GPFilterPlasticZone::getName() const  {return _name;}

double GPFilterPlasticZone::getFactor(const IPVariable* ipvcur, const IPVariable* ipvprev) const 
{
  double p1 = ipvcur->get(IPField::PLASTICSTRAIN);
  if (p1 > 1e-10)
  {
    return 1;
  }
  else
  {
    return 0;
  }
};
GPFilter* GPFilterPlasticZone::clone() const {return new GPFilterPlasticZone(*this);};

GPFilterActivePlasticZone::GPFilterActivePlasticZone(const std::string name): GPFilter(), _name(name){};
GPFilterActivePlasticZone::GPFilterActivePlasticZone(const GPFilterActivePlasticZone& src):GPFilter(src),_name(src._name){}; 
GPFilterActivePlasticZone::~GPFilterActivePlasticZone(){}
std::string GPFilterActivePlasticZone::getName() const  {return _name;}

double GPFilterActivePlasticZone::getFactor(const IPVariable* ipvcur, const IPVariable* ipvprev) const 
{
  double p1 = ipvcur->get(IPField::PLASTICSTRAIN);
  double p0 = ipvprev->get(IPField::PLASTICSTRAIN);
  if ((p1-p0) > 1e-10*(p1+p0)*0.5)
  {
    return 1;
  }
  else
  {
    return 0;
  }
};
GPFilter* GPFilterActivePlasticZone::clone() const {return new GPFilterActivePlasticZone(*this);};

GPFilterIPValueBounds::GPFilterIPValueBounds(int ipVal, double lowerBound, double upperBound, const std::string name): 
    GPFilter(), _name(name),_lowerBound(lowerBound), _upperBound(upperBound), _ipVal(ipVal){};
GPFilterIPValueBounds::GPFilterIPValueBounds(const GPFilterIPValueBounds& src)
    :GPFilter(src),_name(src._name),_ipVal(src._ipVal), _lowerBound(src._lowerBound), _upperBound(src._upperBound){}; 
GPFilterIPValueBounds::~GPFilterIPValueBounds(){}
std::string GPFilterIPValueBounds::getName() const  {return _name;}

double GPFilterIPValueBounds::getFactor(const IPVariable* ipvcur, const IPVariable* ipvprev) const 
{
  double p = ipvcur->get(_ipVal);
  if ((p >= _lowerBound) and (p <=_upperBound))
  {
    return 1.;
  }
  else
  {
    return 0.;
  }
};
GPFilter* GPFilterIPValueBounds::clone() const {return new GPFilterIPValueBounds(*this);};