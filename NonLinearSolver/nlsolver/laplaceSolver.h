//
//
// Description: laplace solver with dirichlet BC
//
//
// Author:  <Van-Dung NGUYEN>, (C) 2023
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef LAPLACESOLVER_H_
#define LAPLACESOLVER_H_

#ifndef SWIG
#include "elementGroup.h"
#include "functionSpace.h"
#include "dofManager.h"
#include "MPoint.h"

class computeJElementFilter;
#endif //SWIG

class LaplaceSolver
{
  
  #ifndef SWIG
  protected:
    GModel* _pModel;
    elementGroup* _elements;
    FunctionSpace<double>* _space;
    std::map<MPoint*, double> _diricletBC;
    dofManager<double> *_pAssembler;
    
  protected:
    void assemble(linearSystem<double> *lsys);
    
  #endif //SWIG
  public:
    LaplaceSolver();
    ~LaplaceSolver();
    
    void loadDomain(const std::string meshFileName);
    void addDomain(int dim, int physical);
    void clearBCs();
    void setDirichletBC(int dim, int physical, double value);
    void solve();
    void builFieldView(const std::string postFileName);
    
    #ifndef SWIG
    void addDomainWithElementFilter(int dim, int physical, const computeJElementFilter& filter);
    void setVertexValue(MVertex* v, double value);
    elementGroup* getElementGroup() {return _elements;};
    double getField(MElement* ele, double u, double v, double w) const;
    SVector3 getGradField(MElement* ele, double u, double v, double w) const;
    #endif //SWIG
    
};


#endif // LAPLACESOLVER_H_