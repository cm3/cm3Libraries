//
// C++ Interface: group of elements
//
//
// Author:  <V.-D. Nguyen>, (C) 2020
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef ELEMENTGROUP_H_
#define ELEMENTGROUP_H_

#include <map>
#include "GFace.h"
#include "MElement.h"
#include "groupOfElements.h"

class elementGroup{
  public:
    typedef std::map<int,MElement*> elementContainer;
    typedef std::map<int,MVertex*> vertexContainer;

  protected:
    vertexContainer _vertices;
    elementContainer _elements;
    elementContainer _parents;

  public:
    elementGroup() {}
    elementGroup(int dim, int physical) { addPhysical(dim, physical); }
    elementGroup(GFace *);
    elementGroup(GRegion *);
    elementGroup(std::vector<MElement *> &elems);

    virtual ~elementGroup() {}

    virtual void addPhysical(int dim, int physical)
    {
      elementFilterTrivial filter;
      addPhysical(dim, physical, filter);
    }

    virtual void addElementary(GEntity *ge, const elementFilter &f);

    virtual void addPhysical(int dim, int physical, const elementFilter &);

    vertexContainer::const_iterator vbegin() const { return _vertices.begin(); }
    vertexContainer::const_iterator vend() const { return _vertices.end(); }
    elementContainer::const_iterator begin() const { return _elements.begin(); }
    elementContainer::const_iterator end() const { return _elements.end(); }

    size_t size() const { return _elements.size(); }
    size_t vsize() const { return _vertices.size(); }

    // FIXME : NOT VERY ELEGANT !!!
    bool find( MElement *e) const // if same parent but different physicals return true ?!
    {
      if(e->getParent() && _parents.find(e->getParent()->getNum()) != _parents.end())
        return true;
      return (_elements.find(e->getNum()) != _elements.end());
    }

    bool find(MVertex *v) const { return (_vertices.find(v->getNum()) != _vertices.end()); }

    inline void insert(MElement *e)
    {
      _elements[e->getNum()] = e;

      if(e->getParent()) {
        _parents[e->getParent()->getNum()] = e->getParent();
        for(std::size_t i = 0; i < e->getParent()->getNumVertices(); i++) {
          _vertices[e->getParent()->getVertex(i)->getNum()] = e->getParent()->getVertex(i);
        }
      }
      else {
        for(std::size_t i = 0; i < e->getNumVertices(); i++) {
          _vertices[e->getVertex(i)->getNum()] = e->getVertex(i);
        }
      }
    }
    inline void insert(const elementGroup&gr, const elementFilter &filter)
    {
      for (elementGroup::elementContainer::const_iterator it = gr.begin(); it != gr.end(); it++)
      {
        MElement* e = it->second;
        if(filter(e))
        {
          insert(e);
        }
      }
    }
    inline void insert(const elementGroup&gr)
    {
      elementFilterTrivial filter;
      insert(gr,filter);
    }

    inline void clearAll()
    {
      _vertices.clear();
      _elements.clear();
      _parents.clear();
    }
};

#endif // ELEMENTGROUP_H_