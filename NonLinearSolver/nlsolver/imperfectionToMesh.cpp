//
// Author:  <Van Dung NGUYEN>, (C) 2015
//
// Copyright: See COPYING file that comes with this distribution
//
//

 #include "imperfectionToMesh.h"
 #include "Context.h"

 sphericalCapImperfectionFunction::sphericalCapImperfectionFunction(const double xc, const double yc, const double zc,
                                  const double xdir, const double ydir, const double zdir,
                                  const double R, const double H): imperfectionFunction(),_root(xc,yc,zc),_direction(xdir,ydir,zdir),_R(R),_H(H){

  _direction.normalize();
}

sphericalCapImperfectionFunction::sphericalCapImperfectionFunction(const sphericalCapImperfectionFunction& src) : imperfectionFunction(src),
          _root(src._root),_direction(src._direction),_R(src._R),_H(src._H){};

void sphericalCapImperfectionFunction::getDirection(SVector3& u) const { u = _direction;};
double sphericalCapImperfectionFunction::get(double x, double y, double z) const{
  SPoint3 P(x,y,z);
  SVector3 CP(_root,P);
  double t = dot(CP,_direction)/_direction.norm();

  double rSq = CP.normSq() + _direction.normSq()*t*t - 2.*dot(CP,_direction)*t;
  if (rSq < 0) {
    rSq = 0.;
    Msg::Warning("rSq = %e < 0",rSq);
  }
  double r = sqrt(rSq);

  double K = sqrt(_R*_R - (_R-_H)*(_R-_H));
  if (r <= 0.98*K){
    double v = (sqrt(_R*_R - rSq) - (_R -_H));
    return v;
  }
  else{
    double r0 = 0.98*K;
    double v0 = (sqrt(_R*_R - r0*r0) - (_R -_H));
    double dv0dr = -r0/sqrt(_R*_R - r0*r0);
    double h = -v0/dv0dr;
    double v = v0*exp(-(r-r0)/h);
    return v;
  }
};

SinusSphericalCapImperfectionFunction::SinusSphericalCapImperfectionFunction(const double xc, const double yc, const double zc,
                                  const double xdir, const double ydir, const double zdir,
                                  const double R, const double H, const double omega,
                                  const double xdir2, const double ydir2, const double zdir2):
                                  sphericalCapImperfectionFunction(xc,yc,zc,xdir,ydir,zdir,R,H),_omega(omega),_axis(xdir2,ydir2,zdir2){};

SinusSphericalCapImperfectionFunction::SinusSphericalCapImperfectionFunction(const SinusSphericalCapImperfectionFunction& src):
        sphericalCapImperfectionFunction(src), _omega(src._omega),_axis(src._axis){}

double SinusSphericalCapImperfectionFunction::get(double x, double y, double z) const{
  double v = 0; // return value
  SPoint3 P(x,y,z);
  SVector3 CP(_root,P);
  double t = dot(CP,_direction)/_direction.norm();

  double rSq = CP.normSq() + _direction.normSq()*t*t - 2.*dot(CP,_direction)*t;
  if (rSq < 0) {
    rSq = 0.;
    Msg::Warning("rSq = %e < 0",rSq);
  }
  double r = sqrt(rSq);



  double K = sqrt(_R*_R - (_R-_H)*(_R-_H));
  if (r <= 0.98*K){
    v = (sqrt(_R*_R - rSq) - (_R -_H));
  }
  else{
    double r0 = 0.98*K;
    double v0 = (sqrt(_R*_R - r0*r0) - (_R -_H));
    double dv0dr = -r0/sqrt(_R*_R - r0*r0);
    double h = -v0/dv0dr;
    v = v0*exp(-(r-r0)/h);
  }


  // sin part
  double r2 = (_axis[0]*(x-_root[0]) + _axis[1]*(y-_root[1]) + _axis[2]*(z-_root[2]))/_axis.norm();
  double pi = 3.14159265358979323846;
  double sinpart = sin(_omega*pi*r2/K);

  return v*sinpart;

};

 axisymmetricImperfectionFunction::axisymmetricImperfectionFunction(const double xc, const double yc, const double zc,
                                  const double xdir, const double ydir, const double zdir,
                                  const double a, const double c): imperfectionFunction(),_root(xc,yc,zc),_direction(xdir,ydir,zdir),_a(a),_c(c){
   _direction.normalize();
};

void axisymmetricImperfectionFunction::getDirection(SVector3& u) const { u = _direction;};
double axisymmetricImperfectionFunction::get(double x, double y, double z) const{
  SPoint3 P(x,y,z);
  SVector3 CP(_root,P);
  double t = dot(CP,_direction)/_direction.norm();
  double rSq = CP.normSq() + _direction.normSq()*t*t - 2.*dot(CP,_direction)*t;

  return  _a*exp(-rSq/(2.*_c*_c));
}

axisymmetricImperfectionFunction::axisymmetricImperfectionFunction(const axisymmetricImperfectionFunction& src)
      : imperfectionFunction(src),_root(src._root),_direction(src._direction),_a(src._a),_c(src._c){};



imperfectionPart::imperfectionPart(){}
void imperfectionPart::addFunction(const imperfectionFunction& f){
  func = f.clone();
};
void imperfectionPart::addEntity(const int onwhat, const int num){
  GEntity* g = NULL;
  if (onwhat == 0)
    g = GModel::current()->getVertexByTag(num);
  else if (onwhat ==1)
    g = GModel::current()->getEdgeByTag(num);
  else if (onwhat == 2)
    g = GModel::current()->getFaceByTag(num);
  else if (onwhat == 3)
    g = GModel::current()->getRegionByTag(num);
  else
    Msg::Error("error");

  if (g == NULL)
    Msg::Error("Entity %d does not exist",num);
  else
    entities.push_back(g);
};

void imperfectionPart::addAllEntities(){
  // region
  for (GModel::riter it = GModel::current()->firstRegion(); it != GModel::current()->lastRegion(); it++){
    if (*it != NULL)
      entities.push_back(*it);
  };
  // face
  for (GModel::fiter it = GModel::current()->firstFace(); it != GModel::current()->lastFace(); it++){
    if (*it != NULL)
      entities.push_back(*it);
  };
  // edge
  for (GModel::eiter it = GModel::current()->firstEdge(); it != GModel::current()->firstEdge(); it++){
    if (*it != NULL)
      entities.push_back(*it);
  };
  // point
  for (GModel::viter it = GModel::current()->firstVertex(); it != GModel::current()->lastVertex(); it++){
    if (*it != NULL)
      entities.push_back(*it);
  };

};


 imperfectionToMesh::imperfectionToMesh():_pModel(NULL),_supperposed(false){}

void imperfectionToMesh::loadModel(const std::string meshFileName)
{
  _pModel = new GModel();
  _pModel->readMSH(meshFileName.c_str());
  _meshFileName = meshFileName;
}

void imperfectionToMesh::setSupperposed(const bool flag){
  _supperposed = flag;
};


void imperfectionToMesh::addImperfection(imperfectionPart* ip){
  _allImperfectionPart.push_back(ip);
};

void imperfectionToMesh::imposeImperfection(const std::string fname){
  Msg::Info("Begin writing perturbed mesh");
  GModel* dispgmodel = new GModel();
  dispgmodel->readMSH(_meshFileName.c_str());
  std::set<MVertex*> allPerturbedVertices;
  for (int i=0; i<_allImperfectionPart.size(); i++){
    std::vector<GEntity*> allg = _allImperfectionPart[i]->entities;
    imperfectionFunction* f = _allImperfectionPart[i]->func;
    SVector3 u;
    f->getDirection(u);
    u.normalize();
    for (int ig=0; ig< allg.size(); ig++){
      for (int iver =0; iver < allg[ig]->mesh_vertices.size(); iver++){
        int vnum = allg[ig]->mesh_vertices[iver]->getNum();
        MVertex* v = dispgmodel->getMeshVertexByTag(vnum);
        if (_supperposed == false){
          if (v != NULL and allPerturbedVertices.find(v) == allPerturbedVertices.end()){
            double val = f->get(v->x(),v->y(),v->z());
            v->x() += val*u[0];
            v->y() += val*u[1];
            v->z() += val*u[2];
            allPerturbedVertices.insert(v);
          }
        }
        else{
          if (v != NULL){
            double val = f->get(v->x(),v->y(),v->z());
            v->x() += val*u[0];
            v->y() += val*u[1];
            v->z() += val*u[2];
          }

        }

      }
    }
  }
  // write deformed mesh to file
  dispgmodel->writeMSH(fname, CTX::instance()->mesh.mshFileVersion,CTX::instance()->mesh.binary, CTX::instance()->mesh.saveAll,CTX::instance()->mesh.saveParametric, CTX::instance()->mesh.scalingFactor);

  delete dispgmodel;
	Msg::Info("End writing perturbed mesh");
};
