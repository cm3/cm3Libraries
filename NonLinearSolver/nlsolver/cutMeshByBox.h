//
//
// Description: cut finite model by a surface--> obtain a smaller cube from a larger FE model
//
// Author:  <Van Dung NGUYEN>, (C) 2013
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef CUTMESHBYBOX_H_
#define CUTMESHBYBOX_H_

#ifndef SWIG
#include "MElement.h"
#include "elementGroup.h"
#include "GModel.h"

MElement* create_Tetrahedron(MVertex* v0, MVertex* v1, MVertex* v2, MVertex* v3);

class face{
	protected:
		double _a, _b, _c, _d; // cut in order to ax+by+cZ+d >=0
		double _tolerance;

	public:
		face(double a, double b, double c, double d, double tol = 1.e-6);
		virtual ~face(){};

		bool inside(MElement* ele) const;
		bool boundary(MElement* ele) const;
		bool outside(MElement* ele) const;

		bool inside(MVertex* v) const;
		bool boundary(MVertex* v) const;
		bool outside(MVertex* v) const;

		bool inside(double x, double y, double z) const;
		bool boundary(double x, double y, double z) const;
		bool outside(double x, double y, double z) const;


		bool onFace(MElement* ele, std::vector<MElement*>& face) const;

		void cut(MVertex* v1, MVertex* v2, MVertex*& v) const;
		void cutElements(MElement* ele, std::vector<MElement*>& face, std::vector<MElement*>& volume) const;

};

#endif //SWIG
class cutMeshByBox {
	#ifndef SWIG
	public:
    static std::set<MVertex*> currentVertices;
    static MVertex* createVertexFromCoordinates(double x, double y, double z, double tol);

	protected:
		GModel* _pModel;
		elementGroup* _g;

		// for cut mesh
		elementGroup* _gVolume;
		std::vector<face*> _allCutFaces;

	protected:
		elementGroup* cutFace(face* f, elementGroup* in);


	#endif //SIWIG
	public:
		cutMeshByBox();
		virtual ~cutMeshByBox(){};

		void addModel(std::string filename);

		void cut(double a, double b, double c, double d);

		void write_MSH2(std::string filename);
};


class levelSet{
  #ifndef SWIG
  public:  
    levelSet() {};
    levelSet(const levelSet& src){};
    virtual ~levelSet(){}
    virtual levelSet* clone() const  = 0;
    virtual bool inside(double x, double y, double z) const = 0;
		virtual bool boundary(double x, double y, double z) const = 0;
		virtual bool outside(double x, double y, double z) const = 0;
    bool sameSide(double x1, double y1, double z1, double x2, double y2, double z2) const;
    void cut(double x1, double y1, double z1, double x2, double y2, double z2, double& x, double& y, double& z) const;
  #endif // SWIG
};

class sphericalLevelSet : public levelSet{
  #ifndef SWIG
  protected:
    double _radius;
    SPoint3 _center;
    bool _inverse;
  #endif 
  public:
    sphericalLevelSet(const double p1, const double p2, const double p3, const double r, const bool inv);
    #ifndef SWIG
    sphericalLevelSet(const sphericalLevelSet& src);
    virtual ~sphericalLevelSet(){};
    virtual bool inside(double x, double y, double z) const;
		virtual bool boundary(double x, double y, double z) const;
		virtual bool outside(double x, double y, double z) const;
    virtual levelSet* clone() const {return new sphericalLevelSet(*this);};
    #endif //SWIG
};

class cutMeshByLevelSet{
  #ifndef SWIG
  protected:
    std::set<MVertex*> allVertices;
    std::set<MElement*> intersectionElements;
    
    
    bool inside(const levelSet*l, MElement* ele) const;
		bool boundary(const levelSet*l, MElement* ele) const;
		bool outside(const levelSet*l, MElement* ele) const;
    MVertex* createNewVerticesFromCoordinates(double x, double y, double z, double tol = 1e-10);
    void cutElement(const levelSet* ls, MElement* ele, std::vector<MElement*>& cutElem);
    
    void write_MSH2(elementGroup* g,  const std::string filename);
  #endif //SWIG
  
  public:
    cutMeshByLevelSet(){}
    virtual ~cutMeshByLevelSet(){};
    void cut(const std::string inputModel, const levelSet* gl, const std::string outputModel);
};
#endif // CUTMESHBYBOX_H_
