//
// C++ Interface: dofManager
//
// Description: manage multi system with only 1 dofManager
//
// Author:  <Van Dung NGUYEN>, (C) 2020
//
// Copyright: See COPYING file that comes with this distribution
//
//


#include "dofManagerMultiSystems.h"

dofManagerMultiSystems::dofManagerMultiSystems(){};
dofManagerMultiSystems::~dofManagerMultiSystems()
{
  for (int i=0; i< _vmanager.size(); i++)
  {
    delete _vmanager[i];
  }
  _vmanager.clear();
  _vncomps.clear();
}

void dofManagerMultiSystems::add(nlsDofManager* manager, const int ncomp)
{
  int addcomp = 0;
  for(int i=0;i<_vncomps.size();i++)
  {
    addcomp+= _vncomps[i];
  };
  for (int i=0; i<ncomp; i++)
  {
    _managerByComp[addcomp+i] = manager;
  }
  _vmanager.push_back(manager);
  _vncomps.push_back(ncomp);
};

// get manager by comp, not not, return this
nlsDofManager* dofManagerMultiSystems::getManager(const int i)
{
  if (i < _vmanager.size())
  {
    return _vmanager[i];
  }
  else
  {
    Msg::Error("system %d does not exist",i);
    return NULL;
  }
};
const nlsDofManager* dofManagerMultiSystems::getManager(const int i) const 
{
  if (i < _vmanager.size())
  {
    return _vmanager[i];
  }
  else
  {
    Msg::Error("system %d does not exist",i);
    return NULL;
  }
};
nlsDofManager* dofManagerMultiSystems::getManagerByComp(int comp)
{
  std::map<int, nlsDofManager*>::iterator itMap = _managerByComp.find(comp);
  if (itMap != _managerByComp.end()) 
  {
    return itMap->second;
  }
  else
  {
    Msg::Error("Fail to find the manager associated to comp %d!",comp);
    return NULL;
  }
};

const nlsDofManager* dofManagerMultiSystems::getManagerByComp(int comp) const 
{
  std::map<int, nlsDofManager*>::const_iterator itMap = _managerByComp.find(comp);
  if (itMap != _managerByComp.end()) 
  {
    return itMap->second;
  }
  else
  {
    Msg::Error("Fail to find the manager associated to comp %d!",comp);
    return NULL;
  }
}

// collection data for new solving
void dofManagerMultiSystems::collectAllDofValues(DofCollection& collect) const
{
  for(int i=0;i<_vmanager.size();i++)
  {
    _vmanager[i]->collectAllDofValues(collect);
  }
};
// update data from dof collection
void dofManagerMultiSystems::updateDataFromCollection(const DofCollection& collect){
  for(int i=0;i<_vmanager.size();i++)
  {
    _vmanager[i]->updateDataFromCollection(collect);
  }
};
//

void dofManagerMultiSystems::getFixedDof(std::vector<Dof> &R) const
{
  for(int i=0;i<_vmanager.size();i++)
  {
    _vmanager[i]->getFixedDof(R);
  }
};

void dofManagerMultiSystems::getFixedDof(std::set<Dof> &R) const
{
  for(int i=0;i<_vmanager.size();i++)
  {
    _vmanager[i]->getFixedDof(R);
  }
};

void dofManagerMultiSystems::clearAllLineConstraints()
{
  for(int i=0;i<_vmanager.size();i++)
  {
    _vmanager[i]->clearAllLineConstraints();
  }
}

void dofManagerMultiSystems::setLinearConstraint(const Dof& key, DofAffineConstraint<double> &affineconstraint)
{
  int comp = getCompFromDof(key);
  nlsDofManager* manager = getManagerByComp(comp);
  if (manager != NULL)
  {
    manager->setLinearConstraint(key,affineconstraint);
  }
  else
  {
    Msg::Error("Dof %d %d does not corresponds to any system",key.getEntity(),key.getType());
  }
};

void dofManagerMultiSystems::getAllLinearConstraints(std::map<const Dof, const DofAffineConstraint<double>*>& map) const
{
  for(int i=0;i<_vmanager.size();i++)
  {
    _vmanager[i]->getAllLinearConstraints(map);
  }
}

bool dofManagerMultiSystems::isFixed(const Dof& key) const
{
  for(int i=0;i<_vmanager.size();i++)
  {
    if (_vmanager[i]->isFixed(key)) return true;
  }
  return false;
}
bool dofManagerMultiSystems::isAnUnknown(const Dof& key) const
{
  for(int i=0;i<_vmanager.size();i++)
  {
    if (_vmanager[i]->isAnUnknown(key)) return true;
  }
  return false;
}
bool dofManagerMultiSystems::isConstrained(const Dof& key) const
{
  for(int i=0;i<_vmanager.size();i++)
  {
    if (_vmanager[i]->isConstrained(key)) return true;
  }
  return false;
};
//
int dofManagerMultiSystems::sizeOfR() const
{
  int syssize=0;
  for(int i=0;i<_vmanager.size();i++)
    syssize+=_vmanager[i]->sizeOfR();
  return syssize;
}
int dofManagerMultiSystems::sizeOfF() const
{
  int fixsize=0;
  for(int i=0;i<_vmanager.size();i++)
    fixsize+=_vmanager[i]->sizeOfF();
  return fixsize;
};

void dofManagerMultiSystems::setFirstRigidContactUnknowns() 
{
  _vmanager[0]->setFirstRigidContactUnknowns();
}
int dofManagerMultiSystems::getFirstRigidContactUnknowns() const
{
  return _vmanager[0]->getFirstRigidContactUnknowns();
}

void dofManagerMultiSystems::assemble(const std::vector<Dof> &R, const fullMatrix<double> &m)
{
  assemble(R,m,nonLinearSystemBase::stiff);
}
// assemble for nonlinear solver,
void dofManagerMultiSystems::assemble(const std::vector<Dof> &R, const fullMatrix<double> &m,const whichMatrix wm)
{
  std::map<int,std::vector<Dof> > mapCompDof;
  seperateDofVecByComp(R,mapCompDof);
  for (std::map<int,std::vector<Dof> >::iterator it = mapCompDof.begin(); it != mapCompDof.end(); it++)
  {
    int comp  = it->first;
    nlsDofManager* manager = getManagerByComp(comp);
    if (manager != NULL)
    {
      if ((manager->getScheme() != nonLinearMechSolver::Explicit) || (wm != nonLinearSystemBase::stiff))
      {
        const std::vector<Dof>& Rpart = it->second;
        std::vector<int> loc;
        getLocalPosition(R,Rpart,loc);
        fullMatrix<double> mat;
        mat.resize(Rpart.size(),Rpart.size());
        for (int i=0; i< Rpart.size(); i++)
        {
          for (int j=0; j< Rpart.size(); j++)
          {
            mat(i,j) = m(loc[i],loc[j]);
          }
        }
        manager->assemble(Rpart,mat,wm);
      }
    }
  }
};


void dofManagerMultiSystems::assemble(const std::vector<Dof> &R, const fullVector<double> &m)
{
  std::map<int,std::vector<Dof> > mapCompDof;
  seperateDofVecByComp(R,mapCompDof);
  for (std::map<int,std::vector<Dof> >::iterator it = mapCompDof.begin(); it != mapCompDof.end(); it++)
  {
    int comp  = it->first;
    const std::vector<Dof>& Rpart = it->second;
    std::vector<int> loc;
    getLocalPosition(R,Rpart,loc);
    fullVector<double> vec;
    vec.resize(Rpart.size());
    for (int i=0; i< Rpart.size(); i++)
    {
      vec(i) = m(loc[i]);
    }
    nlsDofManager* manager = getManagerByComp(comp);
    if (manager != NULL)
    {
      manager->assemble(Rpart,vec);
    }
  }
};

void dofManagerMultiSystems::assemble(const std::vector<Dof> &R, const fullVector<double> &m,const whichRhs wrhs)
{
  std::map<int,std::vector<Dof> > mapCompDof;
  seperateDofVecByComp(R,mapCompDof);
  for (std::map<int,std::vector<Dof> >::iterator it = mapCompDof.begin(); it != mapCompDof.end(); it++)
  {
    int comp  = it->first;
    const std::vector<Dof>& Rpart = it->second;
    std::vector<int> loc;
    getLocalPosition(R,Rpart,loc);
    fullVector<double> vec;
    vec.resize(Rpart.size());
    for (int i=0; i< Rpart.size(); i++)
    {
      vec(i) = m(loc[i]);
    }
    nlsDofManager* manager = getManagerByComp(comp);
    if (manager != NULL)
    {
      manager->assemble(Rpart,vec,wrhs);
    }
  }
};

void dofManagerMultiSystems::setInitialCondition(const Dof &R, const double &value, const nonLinearBoundaryCondition::whichCondition wc)
{
  int compR = getCompFromDof(R);
  nlsDofManager* manager = getManagerByComp(compR);
  if (manager != NULL)
  {
    manager->setInitialCondition(R,value,wc);
  }
}
void dofManagerMultiSystems::setInitialCondition(const std::vector<Dof> &R, const std::vector<double> &disp, const nonLinearBoundaryCondition::whichCondition wc)
{
  for (int i=0; i < R.size(); i++)
  {
    setInitialCondition(R[i],disp[i],wc);
  }
};


void dofManagerMultiSystems::fixDof(const Dof& key, const double &value)
{
  int compR = getCompFromDof(key);
  nlsDofManager* manager = getManagerByComp(compR);
  if (manager != NULL)
  {
    manager->fixDof(key,value);
  }
};

void dofManagerMultiSystems::numberDof(const Dof& key)
{
  int compR = getCompFromDof(key);
  nlsDofManager* manager = getManagerByComp(compR);
  if (manager != NULL)
  {
    manager->numberDof(key);
  }
}
void dofManagerMultiSystems::numberDof(const std::vector<Dof> &R)
{
  for(int i = 0; i < R.size(); i++) 
  {
    numberDof(R[i]);
  }
}

// solving
void dofManagerMultiSystems::systemSolve()
{
  for(int i=0;i<_vmanager.size();i++)
    _vmanager[i]->systemSolve();
}
int dofManagerMultiSystems::systemSolveIntReturn()
{
  int globalSuc = 1;
  for(int i=0;i<_vmanager.size();i++)
  {
    int suc = _vmanager[i]->systemSolveIntReturn();
    if(suc==0)
    {
      Msg::Error("The system %d is not successfully solved!",i);
      globalSuc = 0;
    }
  }
  return globalSuc;
}
int dofManagerMultiSystems::lineSearch()
{
  int globalConverged = 1;
  for(int i=0;i<_vmanager.size();i++)
  {
    int ls = _vmanager[i]->lineSearch();
    if(ls == 0)
    {
      Msg::Warning("No line Search convergence for system %d !",i);
      globalConverged = 0;
    }
  }
  return globalConverged;
};

void dofManagerMultiSystems::insertInSparsityPattern(const Dof &R, const Dof &C)
{
  int compR = getCompFromDof(R);
  int compC = getCompFromDof(C);
  if (compR == compC)
  {
    nlsDofManager* manager = getManagerByComp(compR);
    if (manager != NULL)
    {
      if (manager->getScheme() == nonLinearMechSolver::Explicit) return; // no matrix --> skip
      manager->insertInSparsityPattern(R,C);
    }
  };
};
void dofManagerMultiSystems::sparsityDof(const std::vector<Dof>& R)
{
  for(int i=0;i<R.size();++i)
  {
    for(int j=0;j<R.size();++j)
    {
      insertInSparsityPattern(R[i],R[j]);
    }
  }
};

void dofManagerMultiSystems::allocateSystem()
{  for(int i=0;i<_vmanager.size();i++)
  {
    _vmanager[i]->allocateSystem();
  }
}
void dofManagerMultiSystems::preAllocateEntries()
{
  for(int i=0;i<_vmanager.size();i++)
  {
    _vmanager[i]->preAllocateEntries();
  }
}
// operations
void dofManagerMultiSystems::clearRHSfixed()
{
  for(int i=0;i<_vmanager.size();i++){
    _vmanager[i]->clearRHSfixed();
  }
}

double dofManagerMultiSystems::getNorm1ReactionForce() const
{
  double val = 0;
  for(int i=0;i<_vmanager.size();i++){
      val += _vmanager[i]->getNorm1ReactionForce();
  }
  return val;
}
void dofManagerMultiSystems::getReactionForceOnFixedPhysical(archiveForce& af) const
{
  double forceallsys = 0;
  for(int i=0;i<_vmanager.size();i++)
  {
    _vmanager[i]->getReactionForceOnFixedPhysical(af);
    forceallsys += af.fval;
  }
  af.fval = forceallsys;
};

void dofManagerMultiSystems::getForces(std::vector<archiveForce> &vaf) const
{
  std::vector<double> forceallsys(vaf.size(),0.);
  for(int i=0;i<_vmanager.size();i++)
  {
    _vmanager[i]->getForces(vaf);
    for(int j=0;j<vaf.size();j++)
      forceallsys[j]+= vaf[j].fval;
  }
  for(int j=0;j<forceallsys.size();j++)
    vaf[j].fval = forceallsys[j];
}
double dofManagerMultiSystems::getKineticEnergy(const int systemSizeWithoutRigidContact,FilterDofSet &fildofcontact) const
{
  double allsyskin=0.;
  for(int i=0;i<_vmanager.size();i++) 
    allsyskin += _vmanager[i]->getKineticEnergy(systemSizeWithoutRigidContact,fildofcontact);
  return allsyskin;
}

void dofManagerMultiSystems::zeroMatrix()
{
  for(int i=0;i<_vmanager.size();i++)
  {
    _vmanager[i]->zeroMatrix();
  }
}
void dofManagerMultiSystems::resetUnknownsToPreviousTimeStep()
{
  for(int i=0;i<_vmanager.size();i++)
    _vmanager[i]->resetUnknownsToPreviousTimeStep();
}
void dofManagerMultiSystems::nextStep()
{
  for(int i=0;i<_vmanager.size();i++)
    _vmanager[i]->nextStep();
}
void dofManagerMultiSystems::setTimeStep(const double dt)
{
  for(int i=0;i<_vmanager.size();i++)
    _vmanager[i]->setTimeStep(dt);
}
//
double dofManagerMultiSystems::normInfRightHandSide() const
{
  double sysnorm=0;
  for(int i=0;i<_vmanager.size();i++)
  {
    if(_vmanager[i]->getScheme()!=nonLinearMechSolver::Explicit)
    {
      double mynorm = _vmanager[i]->normInfRightHandSide();
      if(mynorm>sysnorm)
        sysnorm = mynorm;
    }
  }
  return sysnorm;
}

double dofManagerMultiSystems::normInfSolution() const
{
  double sysnorm=0;
  for(int i=0;i<_vmanager.size();i++)
  {
    double mynorm = _vmanager[i]->normInfSolution();
    if(mynorm>sysnorm)
      sysnorm = mynorm;
  }
  return sysnorm;
}

double dofManagerMultiSystems::norm0Inf() const
{
  double sysnorm=0;
  for(int i=0;i<_vmanager.size();i++)
  {
    if(_vmanager[i]->getScheme()!=nonLinearMechSolver::Explicit)
    {
      double mynorm = _vmanager[i]->norm0Inf();
      if(mynorm>sysnorm)
        sysnorm = mynorm;
    }
  }
  return sysnorm;
};
//
#if defined(HAVE_MPI)
void dofManagerMultiSystems::manageMPIComm(const int otherPartNum,const std::vector<Dof> &otherPartR)
{
  std::map<int,std::vector<Dof> > DofByComp;
  seperateDofVecByComp(otherPartR,DofByComp);
  for (std::map<int, std::vector<Dof> >::iterator it  = DofByComp.begin(); it != DofByComp.end(); it++)
  {
    nlsDofManager* manager = getManagerByComp(it->first);
    if (manager != NULL)
    {
      manager->manageMPIComm(otherPartNum,it->second);
    }
  }
};
void dofManagerMultiSystems::manageMPISparsity(const std::vector<Dof> &myR, const int otherPartNum,const std::vector<Dof> &otherPartR)
{
  std::map<int,std::vector<Dof> > RByComp,otherPartRByComp;
  seperateDofVecByComp(myR,RByComp);
  seperateDofVecByComp(otherPartR,otherPartRByComp);
  
  for (std::map<int, std::vector<Dof> >::iterator itR = RByComp.begin(); itR != RByComp.end(); itR++)
  {
    std::map<int, std::vector<Dof> >::iterator itOther = otherPartRByComp.find(itR->first);
    if (itOther != otherPartRByComp.end())
    {
      nlsDofManager* manager = getManagerByComp(itR->first);
      if (manager != NULL && itOther->second.size() > 0)
      {
        manager->manageMPISparsity(itR->second,otherPartNum,itOther->second);
      }      
    }
  }
}

void dofManagerMultiSystems::systemMPIComm()
{
  for(int i=0;i<_vmanager.size();i++)
    _vmanager[i]->systemMPIComm();
}
#endif //HAVE_MPI

void dofManagerMultiSystems::restart()
{
  for(int i=0;i<_vmanager.size();i++)
  {
    _vmanager[i]->restart();
  }
};

void dofManagerMultiSystems::getVertexMass(const std::vector<Dof> &R, std::vector<double> &vmass) const
{
  vmass.resize(R.size());
  std::map<int,std::vector<Dof> > RByComp;
  seperateDofVecByComp(R,RByComp);
  for (std::map<int, std::vector<Dof> >::iterator it  = RByComp.begin(); it != RByComp.end(); it++)
  {
    const nlsDofManager* manager = getManagerByComp(it->first);
    if (manager != NULL)
    {
      std::vector<double> vmassComp;
      manager->getVertexMass(it->second,vmassComp);
      std::vector<int> location;
      getLocalPosition(R,it->second,location);
      for (int i=0; i< location.size(); i++)
      {
        vmass[location[i]] = vmassComp[i];
      }
    }
  }
};

void dofManagerMultiSystems::getFixedRightHandSide(std::vector<Dof> &R, std::vector<double> &val) const
{
  for(int i=0;i<_vmanager.size();i++)
  {
    _vmanager[i]->getFixedRightHandSide(R,val);
  }
};

void dofManagerMultiSystems::getDofValue(const Dof& key,  double &val) const
{
  int compR = getCompFromDof(key);
  const nlsDofManager* manager = getManagerByComp(compR);
  if (manager != NULL)
  {
    manager->getDofValue(key,val);
  }
}
void dofManagerMultiSystems::getDofValue(const Dof& key,  double &val, nonLinearBoundaryCondition::whichCondition wv) const
{
  int compR = getCompFromDof(key);
  const nlsDofManager* manager = getManagerByComp(compR);
  if (manager != NULL)
  {
    manager->getDofValue(key,val,wv);
  }
}
void dofManagerMultiSystems::getDofValue(const std::vector<Dof> &keys,std::vector<double> &Vals) const
{
  Vals.resize(keys.size());
  for (int i=0; i< keys.size(); i++)
  {
    getDofValue(keys[i],Vals[i]);
  };
}
void dofManagerMultiSystems::getDofValue(const std::vector<Dof> &keys,std::vector<double> &Vals, const nonLinearBoundaryCondition::whichCondition wv) const
{
  Vals.resize(keys.size());
  for (int i=0; i< keys.size(); i++)
  {
    getDofValue(keys[i],Vals[i],wv);
  };
};

void dofManagerMultiSystems::getFixedDofValue(const Dof& key, double &val) const
{
  int compR = getCompFromDof(key);
  const nlsDofManager* manager = getManagerByComp(compR);
  if (manager != NULL)
  {
    manager->getFixedDofValue(key,val);
  }
};

const std::map<Dof, int>& dofManagerMultiSystems::getUnknownMap() const {
   Msg::Error("The getUnknownMap() is not implemented in dofManagerMultiSystems");
};
