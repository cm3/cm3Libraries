//
// Author:  <Van Dung NGUYEN>, (C) 2023
//
// Copyright: See COPYING file that comes with this distribution
// A class to add a factor depending on the IPVariable when performing volumetric integration
//
#ifndef GPFILTER_H_
#define GPFILTER_H_

#ifndef SWIG
#include <string>
class IPVariable;
#endif //SWIG
class GPFilter 
{
  #ifndef SWIG
  public:
    GPFilter(){}
    GPFilter(const GPFilter& src){}
    virtual ~GPFilter(){}
    virtual std::string getName() const  = 0;
    virtual double getFactor(const IPVariable* ipvcur, const IPVariable* ipvprev) const = 0; 
    virtual GPFilter* clone() const = 0;
  #endif
};

class GPFilterTrivial : public GPFilter
{
  public:
    GPFilterTrivial();
    GPFilterTrivial(const GPFilterTrivial& src); 
    virtual ~GPFilterTrivial();
    #ifndef SWIG
    virtual std::string getName() const;
    virtual double getFactor(const IPVariable* ipvcur, const IPVariable* ipvprev) const;
    virtual GPFilter* clone() const;
    #endif
};

class GPFilterCurrentVolume : public GPFilter
{
  public:
    GPFilterCurrentVolume();
    GPFilterCurrentVolume(const GPFilterCurrentVolume& src); 
    virtual ~GPFilterCurrentVolume();
    #ifndef SWIG
    virtual std::string getName() const;
    virtual double getFactor(const IPVariable* ipvcur, const IPVariable* ipvprev) const;
    virtual GPFilter* clone() const;
    #endif
};


class GPFilterCoalescence : public GPFilter
{
  protected:
      std::string _name;
  public:
    GPFilterCoalescence(const std::string name = "Coalescence");
    GPFilterCoalescence(const GPFilterCoalescence& src); 
    virtual ~GPFilterCoalescence();
    #ifndef SWIG
    virtual std::string getName() const;
    virtual double getFactor(const IPVariable* ipvcur, const IPVariable* ipvprev) const;
    virtual GPFilter* clone() const;
    #endif
};

class GPFilterActiveDamage : public GPFilter
{
  protected:
      std::string _name;
  public:
    GPFilterActiveDamage(const std::string name = "ActiveDamage");
    GPFilterActiveDamage(const GPFilterActiveDamage& src); 
    virtual ~GPFilterActiveDamage();
    #ifndef SWIG
    virtual std::string getName() const;
    virtual double getFactor(const IPVariable* ipvcur, const IPVariable* ipvprev) const;
    virtual GPFilter* clone() const;
    #endif
};

class GPFilterPlasticZone : public GPFilter
{
  protected:
      std::string _name;
  public:
    GPFilterPlasticZone(const std::string name = "Plastic");
    GPFilterPlasticZone(const GPFilterPlasticZone& src); 
    virtual ~GPFilterPlasticZone();
    #ifndef SWIG
    virtual std::string getName() const;
    virtual double getFactor(const IPVariable* ipvcur, const IPVariable* ipvprev) const;
    virtual GPFilter* clone() const;
    #endif
};


class GPFilterActivePlasticZone : public GPFilter
{
  protected:
      std::string _name;
  public:
    GPFilterActivePlasticZone(const std::string name = "ActivePlastic");
    GPFilterActivePlasticZone(const GPFilterActivePlasticZone& src); 
    virtual ~GPFilterActivePlasticZone();
    #ifndef SWIG
    virtual std::string getName() const;
    virtual double getFactor(const IPVariable* ipvcur, const IPVariable* ipvprev) const;
    virtual GPFilter* clone() const;
    #endif
};

class GPFilterIPValueBounds : public GPFilter
{
  protected:
      std::string _name;
      int _ipVal;
      double _lowerBound, _upperBound;      
  public:
    GPFilterIPValueBounds(int ipVal, double lowerBound, double upperBound, 
                                      const std::string name = "GPFilterIPValueBounds");
    GPFilterIPValueBounds(const GPFilterIPValueBounds& src); 
    virtual ~GPFilterIPValueBounds();
    #ifndef SWIG
    virtual std::string getName() const;
    virtual double getFactor(const IPVariable* ipvcur, const IPVariable* ipvprev) const;
    virtual GPFilter* clone() const;
    #endif
};

#endif //GPFILTER_H_