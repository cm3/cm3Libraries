//
//
// Description: laplace solver with dirichlet BC
//
//
// Author:  <Van-Dung NGUYEN>, (C) 2023
//
// Copyright: See COPYING file that comes with this distribution
//
//

#include "laplaceSolver.h"
#include "GModel.h"
#include "terms.h"
#include "linearSystemPETSc.h"
#include "linearSystemFull.h"
#include "linearSystemGmm.h"
#include "PView.h"
#include "quadratureRules.h"
#include "nlsolAlgorithms.h"
#include "JIntegralByDomainIntegration.h"

void LaplaceSolver::assemble(linearSystem<double> *lsys)
{
  if(_pAssembler) delete _pAssembler;
  _pAssembler = new dofManager<double>(lsys);
  
  // fix dof
  for (std::map<MPoint*, double>::iterator itfixed = _diricletBC.begin(); itfixed != _diricletBC.end(); itfixed++)
  {
    std::vector<Dof> R;
    _space->getKeys(itfixed->first,R);
    for (int j=0; j< R.size(); j++)
    {
      _pAssembler->fixDof(R[j], itfixed->second);
    }
  }
  // number dofs
  for (elementGroup::elementContainer::const_iterator it = _elements->begin(); it != _elements->end(); it++)
  {
    MElement* ele = it->second;
    std::vector<Dof> R;
    _space->getKeys(ele,R);
    _pAssembler->numberDof(R);
  }
  // allocate
  lsys->allocate(_pAssembler->sizeOfR());
  // preallocate
  for (elementGroup::elementContainer::const_iterator it = _elements->begin(); it != _elements->end(); it++)
  {
    MElement* ele = it->second;
    std::vector<Dof> R;
    _space->getKeys(ele,R);
    for (int i=0; i< R.size(); i++)
    {
      int NR = _pAssembler->getDofNumber(R[i]);
      if (NR >=0)
      {
        for (int j=0; j< R.size(); j++)
        {
          int NC = _pAssembler->getDofNumber(R[j]);
          if (NC >=0)
          {
            lsys->insertInSparsityPattern(NR, NC);
          }
        }
      }
    }
  };
  lsys->preAllocateEntries();
  
  // linear term
  LaplaceTerm<double, double> LapTerm(*_space);
  GaussQuadrature Integ_Laplace(GaussQuadrature::GradGrad);
  elementFilterTrivial filter;
  AssembleItMap(LapTerm, *_space,_elements->begin(),_elements->end(),Integ_Laplace,*_pAssembler,filter);
  
  Msg::Info("nDofs=%d nFixed=%d", _pAssembler->sizeOfR(), _pAssembler->sizeOfF());
};

LaplaceSolver::LaplaceSolver(): _pModel(NULL), _pAssembler(NULL)
{
  _elements = new elementGroup();
  _space = new ScalarLagrangeFunctionSpace();
}
LaplaceSolver::~LaplaceSolver()
{
  if (_elements) delete _elements;
  if (_space) delete _space;
  if (_pAssembler) delete _pAssembler;
};

void LaplaceSolver::loadDomain(const std::string meshFileName)
{
  _pModel = new GModel();
  _pModel->setAsCurrent();
  _pModel->readMSH(meshFileName.c_str());
};

void LaplaceSolver::addDomain(int dim, int physical)
{
  elementGroup gr(dim, physical);
  for (elementGroup::elementContainer::const_iterator ite = gr.begin(); ite != gr.end(); ite++)
  {
    _elements->insert(ite->second);
  }
};

void LaplaceSolver::addDomainWithElementFilter(int dim, int physical, const computeJElementFilter& filter)
{
  elementGroup gr(dim, physical);
  for (elementGroup::elementContainer::const_iterator ite = gr.begin(); ite != gr.end(); ite++)
  {
    MElement* ele = ite->second;
    if (filter(ele))
    {
      _elements->insert(ele);
    }
  }
};


void LaplaceSolver::clearBCs()
{
  _diricletBC.clear();
}

void LaplaceSolver::setDirichletBC(int dim, int physical, double value)
{
  elementGroup gr(dim, physical);
  for (elementGroup::vertexContainer::const_iterator itv = gr.vbegin(); itv != gr.vend(); itv++)
  {
    MPoint* pp = new MPoint(itv->second);
    _diricletBC[pp] = value;
  }
};

void LaplaceSolver::setVertexValue(MVertex* v, double value)
{
  MPoint* pp = new MPoint(v);
  _diricletBC[pp] = value;
};


void LaplaceSolver::solve()
{
  #if defined(HAVE_PETSC)
  linearSystemPETSc<double> *lsys = new linearSystemPETSc<double>(PETSC_COMM_SELF);
#elif defined(HAVE_GMM)
  linearSystemGmm<double> *lsys = new linearSystemGmm<double>();
  lsys->setGmres(1);
  lsys->setNoisy(1);
#else
  linearSystemFull<double> *lsys = new linearSystemFull<double>();
#endif
  assemble(lsys);
  lsys->systemSolve();
  Msg::Info("-- done solving!\n");
};

void LaplaceSolver::builFieldView(const std::string postFileName)
{
  Msg::Info("build field View");
  if (_pModel == NULL)
  {
    _pModel = GModel::current();
  }
  
  std::map<int, std::vector<double> > data;
  for(elementGroup::vertexContainer::const_iterator it = _elements->vbegin(); it != _elements->vend(); ++it) 
  {
    double val;
    MPoint p(it->second);
    std::vector<Dof> R;
    _space->getKeys(&p, R);
    std::vector<double> vec(R.size());
    for (int j=0; j< R.size(); j++)
    {
      _pAssembler->getDofValue(R[j], vec[j]);
    }
    data[it->second->getNum()] = vec;
  }
  PView *pv = new PView(postFileName, "NodeData", _pModel, data, 0.0, 1);
  pv->write(postFileName,5,false);
  delete pv;
};

double LaplaceSolver::getField(MElement* ele, double u, double v, double w) const
{
  std::vector<Dof> R;
  _space->getKeys(ele, R);
  std::vector<double> val;
  _pAssembler->getDofValue(R,val);
  return ele->interpolate(&val[0], u, v, w);
};
SVector3 LaplaceSolver::getGradField(MElement* ele, double u, double v, double w) const
{
  std::vector<Dof> R;
  _space->getKeys(ele, R);
  std::vector<double> val;
  _pAssembler->getDofValue(R,val);
  double gradval[3];
  ele->interpolateGrad(&val[0], u, v, w, gradval);
  return SVector3(gradval[0],gradval[1],gradval[2]);
};