//
// C++ Interface: terms
//
// Description: Function to prescribed BC
//
//
// Author:  <Gauthier BECKER>, (C) 2010
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef TIMEFUNCTION_H_
#define TIMEFUNCTION_H_
#include "simpleFunctionTime.h"
#include "SVector3.h"
#include <math.h>
#include "scalarFunction.h"

#ifndef SWIG
#include "interpolation.h"
#endif //SWIG
template<class scalar>
class simpleFunctionTimeWithConstant : public simpleFunctionTime<scalar>{
 protected:
  scalar _b;
 public:
  simpleFunctionTimeWithConstant(scalar val=0, bool td=true, double t=1.,scalar b=0.) : simpleFunctionTime<scalar>(val,td,t), _b(b){};
 #ifndef SWIG
  virtual ~simpleFunctionTimeWithConstant(){};
  virtual scalar operator () (double x, double y, double z) const { if(this->timeDependency) return this->time*this->_val+_b; else return this->_val;}
 #endif // SWIG
};

class simpleFunctionTimeWithScalarFunction: public simpleFunctionTime<double>{
  protected:
    scalarFunction* _fct;

  public:
    simpleFunctionTimeWithScalarFunction(const scalarFunction& fct):simpleFunctionTime<double>(){
      _fct = fct.clone();
    }
    #ifndef SWIG
    virtual ~simpleFunctionTimeWithScalarFunction(){};
    virtual double operator () (double x, double y, double z) const{
      return _fct->getVal(time);
    };
    #endif //SWIG
};

//f = Asin(wt)
class sinusoidalFunctionTime : public simpleFunctionTime<double>{
  protected:
    double _A;
    double _w;

  public:
    sinusoidalFunctionTime(const double A, const double w):simpleFunctionTime<double>(),_A(A),_w(w){};
    #ifndef SWIG
    virtual ~sinusoidalFunctionTime(){};
    virtual double operator () (double x, double y, double z) const{
      return _A*sin(_w*time);
    };
    #endif // SWIG
};

//f = Asin(wt+phi)
class sinusoidalwithPhaseAngleFunctionTime : public simpleFunctionTime<double>{
  protected:
    double _A;
    double _w;
    double _phi;

  public:
    sinusoidalwithPhaseAngleFunctionTime(const double A, const double w, const double phi):simpleFunctionTime<double>(),_A(A),_w(w),_phi(phi){};
    #ifndef SWIG
    virtual ~sinusoidalwithPhaseAngleFunctionTime(){};
    virtual double operator () (double x, double y, double z) const{
      return _A*sin(_w*time+_phi);
    };
    #endif // SWIG
};

//f = A(1-cos(wt))
class cosinusoidalFunctionTime : public simpleFunctionTime<double>{
  protected:
    double _A;
    double _w;

  public:
    cosinusoidalFunctionTime(const double A, const double w):simpleFunctionTime<double>(),_A(A),_w(w){};
    #ifndef SWIG
    virtual ~cosinusoidalFunctionTime(){};
    virtual double operator () (double x, double y, double z) const{
      return _A*(1.-cos(_w*time));
    };
    #endif // SWIG
};
//f = A1(1-cos(w1t))+A2(1-cos(w2t))
class cosinusoidalPlusSinusoidalFunctionTime : public simpleFunctionTime<double>{
  protected:
    double _A1,_A2;
    double _w1,_w2;

  public:
    cosinusoidalPlusSinusoidalFunctionTime(const double A1, const double w1,const double A2, const double w2):simpleFunctionTime<double>(),_A1(A1),_w1(w1),_A2(A2),_w2(w2){};
    #ifndef SWIG
    virtual ~cosinusoidalPlusSinusoidalFunctionTime(){};
    virtual double operator () (double x, double y, double z) const{
      return _A1*(1.-cos(_w1*time))+_A2*sin(_w2*time);
    };
    #endif // SWIG
};
class powerDecreasingFunctionTime : public simpleFunctionTime<double>{
 protected:
// _val = p0  const double p0; // minimal pressure
  const double _p1; // maximal pressure
  const double _a;  // power exponent
  const double _t0; // if t < t0 p = p0
  const double _t1; // if t0 < t < t1 p = p0 + (p1-p0)*(t-t0)/(t1-t0)
  const double _tf; // if t1 < t < tf p = (p1-p0)*(tf-t1)^-a*(tf-t)^a + p0
 public:
  powerDecreasingFunctionTime(double p0, const double p1, const double a, const double t0,
                              const double t1, const double tf) : simpleFunctionTime<double>(p0,true,1.),
                                                                     _p1(p1), _a(a), _t0(t0), _t1(t1), _tf(tf){}
#ifndef SWIG
  virtual ~powerDecreasingFunctionTime(){}
  virtual double operator () (double x, double y, double z) const{
    double p0 = _val;
    if((_t0<time) and(time<_t1))
      return p0 + (_p1-p0)*(time-_t0)/(_t1-_t0);
    else if ((_t1<time) and (time<_tf))
      return (_p1-p0)*pow(_tf-_t1,-_a)*pow(_tf-time,_a)+p0;
    else
      return p0;
  }
#endif //SWIG
};
class cycleFunctionTime : public simpleFunctionTime<double>{
 protected:
// _val = p0  const double p0; // minimal pressure
  const double _d1; // maximal displacement
  const double _d2; // minimal displacement
  const double _d3; // second maximal displacement
  const double _d4;
  const double _d5;
  const double _d6;

  const double _d7;
  const double _d8;
  const double _d9;
  const double _d10;
  const double _d11;
  const double _d12;
  const double _d13;
  const double _d14;
  const double _d15;
  const double _d16;
  const double _d17;

  const double _t1; // time for maximal displacement
  const double _t2; // time for minimal displacement
  const double _t3; // time for second maximal displacement
  const double _t4;
  const double _t5;
  const double _t6;

  const double _t7;
  const double _t8;
  const double _t9;
  const double _t10;
  const double _t11;
  const double _t12;
  const double _t13;
  const double _t14;
  const double _t15;
  const double _t16;
  const double _t17;
  double lastT, penT, lastD, penD;
  int nbInput;
 public:
  cycleFunctionTime(double t1, double d1, double t2, double d2) : simpleFunctionTime<double>(d1,true,1.),_d1(d1), _t1(t1),_d2(d2), _t2(t2),_d3(d2), _t3(t2), _t4(t2),
  _d4(d2),_t5(t2),_d5(d2),_t6(t2),_d6(d2), _t7(t2),_d7(d2),_t8(t2),_d8(d2),_t9(t2),_d9(d2), _t10(t2), _d10(d2), _t11(t2),_d11(d2), _t12(t2),_d12(d2), _t13(t2),_d13(d2),
  _t14(t2), _d14(d2), _t15(t2),_d15(d2), _t16(t2),_d16(d2), _t17(t2),_d17(d2)
  {
    nbInput = 2;
    lastT = _t2; penT = _t1;
    lastD = _d2; penD = _d1;
  }
  cycleFunctionTime(double t1, double d1, double t2, double d2,double t3, double d3) : simpleFunctionTime<double>(d1,true,1.),_d1(d1), _t1(t1),_d2(d2), _t2(t2),_d3(d3),
   _t3(t3), _t4(t3), _d4(d3),_t5(t3),_d5(d3),_t6(t3),_d6(d3), _t7(t3), _d7(d3),_t8(t3),_d8(d3),_t9(t3),_d9(d3), _t10(t3), _d10(d3), _t11(t3),_d11(d3), _t12(t3),_d12(d3),
   _t13(t3),_d13(d3), _t14(t3), _d14(d3), _t15(t3),_d15(d3), _t16(t3),_d16(d3), _t17(t3),_d17(d3)
  {
    nbInput = 3;
    lastT = _t3; penT = _t2;
    lastD = _d3; penD = _d2;
  }
  cycleFunctionTime(double t1, double d1, double t2, double d2,double t3, double d3, double t4, double d4) : simpleFunctionTime<double>(d1,true,1.),_d1(d1), _t1(t1),_d2(d2),
   _t2(t2),_d3(d3), _t3(t3), _t4(t4), _d4(d4),_t5(t4),_d5(d4),_t6(t4),_d6(d4), _t7(t4),_d7(d4),_t8(t4),_d8(d4),_t9(t4),_d9(d4), _t10(t4), _d10(d4), _t11(t4),_d11(d4), _t12(t4),
   _d12(d4), _t13(t4),_d13(d4),  _t14(t4), _d14(d4), _t15(t4),_d15(d4), _t16(t4),_d16(d4), _t17(t4),_d17(d4)
  {
    nbInput = 4;
    lastT = _t4; penT = _t3;
    lastD = _d4; penD = _d3;
  }
  cycleFunctionTime(double t1, double d1, double t2, double d2,double t3, double d3, double t4, double d4, double t5, double d5) : simpleFunctionTime<double>(d1,true,1.),
  _d1(d1), _t1(t1),_d2(d2), _t2(t2),_d3(d3), _t3(t3), _t4(t4), _d4(d4),_t5(t5),_d5(d5),_t6(t5),_d6(d5), _t7(t5),_d7(d5),_t8(t5),_d8(d5),_t9(t5),_d9(d5), _t10(t5), _d10(d5),
  _t11(t5),_d11(d5),_t12(t5),_d12(d5), _t13(t5),_d13(d5),  _t14(t5), _d14(d5), _t15(t5),_d15(d5), _t16(t5),_d16(d5), _t17(t5),_d17(d5)
  {
    nbInput = 5;
    lastT = _t5; penT = _t4;
    lastD = _d5; penD = _d4;
  }
  cycleFunctionTime(double t1, double d1, double t2, double d2,double t3, double d3, double t4, double d4, double t5, double d5, double t6, double d6) :
  simpleFunctionTime<double>(d1,true,1.),_d1(d1), _t1(t1),_d2(d2), _t2(t2),_d3(d3), _t3(t3), _t4(t4), _d4(d4),_t5(t5),_d5(d5),_t6(t6),_d6(d6), _t7(t6), _d7(d6),_t8(t6),
  _d8(d6),_t9(t6),_d9(d6), _t10(t6), _d10(d6),_t11(t6),_d11(d6),_t12(t6),_d12(d6), _t13(t6),_d13(d6),_t14(t6), _d14(d6), _t15(t6),_d15(d6), _t16(t6),_d16(d6), _t17(t6),_d17(d6)
  {
    nbInput = 6;
    lastT = _t6; penT = _t5;
    lastD = _d6; penD = _d5;
  }

   cycleFunctionTime(double t1, double d1, double t2, double d2, double t3, double d3, double t4, double d4, double t5, double d5, double t6, double d6,
                    double t7, double d7)
  : simpleFunctionTime<double>(d1,true,1.),_d1(d1), _t1(t1), _d2(d2), _t2(t2), _d3(d3), _t3(t3), _t4(t4), _d4(d4),_t5(t5),_d5(d5),_t6(t6),_d6(d6),
   _d7(d7), _t7(t7), _d8(d7), _t8(t7), _d9(d7), _t9(t7), _d10(d7), _t10(t7), _d11(d7), _t11(t7), _d12(d7), _t12(t7), _t13(t7),_d13(d7), _t14(t7),_d14(d7),
    _t15(t7),_d15(d7), _t16(t7),_d16(d7),_t17(t7),_d17(d7)
  {
    nbInput = 7;
    lastT = _t7; penT = _t6;
    lastD = _d7; penD = _d6;
  }

   cycleFunctionTime(double t1, double d1, double t2, double d2, double t3, double d3, double t4, double d4, double t5, double d5, double t6, double d6,
                    double t7, double d7, double t8, double d8, double t9, double d9)
  : simpleFunctionTime<double>(d1,true,1.),_d1(d1), _t1(t1), _d2(d2), _t2(t2), _d3(d3), _t3(t3), _t4(t4), _d4(d4),_t5(t5),_d5(d5),_t6(t6),_d6(d6),
   _d7(d7), _t7(t7), _d8(d8), _t8(t8), _d9(d9), _t9(t9), _d10(d9), _t10(t9), _d11(d9), _t11(t9), _d12(d9), _t12(t9), _t13(t9),_d13(d9), _t14(t9),_d14(d9),
    _t15(t9),_d15(d9), _t16(t9),_d16(d9),_t17(t9),_d17(d9)
  {
    nbInput = 9;
    lastT = _t9; penT = _t8;
    lastD = _d9; penD = _d8;
  }

  cycleFunctionTime(double t1, double d1, double t2, double d2, double t3, double d3, double t4, double d4, double t5, double d5, double t6, double d6,
                    double t7, double d7, double t8, double d8, double t9, double d9, double t10, double d10)
  : simpleFunctionTime<double>(d1,true,1.),_d1(d1), _t1(t1), _d2(d2), _t2(t2), _d3(d3), _t3(t3), _t4(t4), _d4(d4),_t5(t5),_d5(d5),_t6(t6),_d6(d6),
   _d7(d7), _t7(t7), _d8(d8), _t8(t8), _d9(d9), _t9(t9), _d10(d10), _t10(t10), _d11(d10), _t11(t10), _d12(d10), _t12(t10),
   _t13(t10),_d13(d10), _t14(t10),_d14(d10), _t15(t10),_d15(d10), _t16(t10),_d16(d10),_t17(t10),_d17(d10)
  {
    nbInput = 10;
    lastT = _t10; penT = _t9;
    lastD = _d10; penD = _d9;
  }


  cycleFunctionTime(double t1, double d1, double t2, double d2, double t3, double d3, double t4, double d4, double t5, double d5, double t6, double d6,
                    double t7, double d7, double t8, double d8, double t9, double d9, double t10, double d10, double t11, double d11)
  : simpleFunctionTime<double>(d1,true,1.),_d1(d1), _t1(t1), _d2(d2), _t2(t2), _d3(d3), _t3(t3), _t4(t4), _d4(d4),_t5(t5),_d5(d5),_t6(t6),_d6(d6),
   _d7(d7), _t7(t7), _d8(d8), _t8(t8), _d9(d9), _t9(t9), _d10(d10), _t10(t10), _d11(d11), _t11(t11), _d12(d11), _t12(t11),
   _t13(t11),_d13(d11), _t14(t11),_d14(d11), _t15(t11),_d15(d11), _t16(t11),_d16(d11),_t17(t11),_d17(d11)
  {
    nbInput = 11;
    lastT = _t11; penT = _t10;
    lastD = _d11; penD = _d10;
  }



  cycleFunctionTime(double t1, double d1, double t2, double d2, double t3, double d3, double t4, double d4, double t5, double d5, double t6, double d6,
                    double t7, double d7, double t8, double d8, double t9, double d9, double t10, double d10, double t11, double d11, double t12, double d12, double t13, double d13)
  : simpleFunctionTime<double>(d1,true,1.),_d1(d1), _t1(t1), _d2(d2), _t2(t2), _d3(d3), _t3(t3), _t4(t4), _d4(d4),_t5(t5),_d5(d5),_t6(t6),_d6(d6), _d7(d7), _t7(t7),
  _d8(d8), _t8(t8), _d9(d9), _t9(t9), _d10(d10), _t10(t10), _d11(d11), _t11(t11), _d12(d12), _t12(t12), _t13(t13),_d13(d13), _t14(t13),_d14(d13), _t15(t13),_d15(d13),
  _t16(t13),_d16(d13),_t17(t13),_d17(d13)
  {
    nbInput = 13;
    lastT = _t13; penT = _t12;
    lastD = _d13; penD = _d12;
  }

  cycleFunctionTime(double t1, double d1, double t2, double d2, double t3, double d3, double t4, double d4, double t5, double d5, double t6, double d6, double t7, double d7,
                    double t8, double d8, double t9, double d9, double t10, double d10, double t11, double d11, double t12, double d12, double t13, double d13,
                    double t14, double d14, double t15, double d15, double t16, double d16)
  : simpleFunctionTime<double>(d1,true,1.),_d1(d1), _t1(t1), _d2(d2), _t2(t2), _d3(d3), _t3(t3), _t4(t4), _d4(d4),_t5(t5),_d5(d5),_t6(t6),_d6(d6), _d7(d7), _t7(t7),
  _d8(d8),_t8(t8), _d9(d9),_t9(t9), _d10(d10),_t10(t10), _d11(d11),_t11(t11), _d12(d12),_t12(t12), _t13(t13),_d13(d13), _t14(t14),_d14(d14), _t15(t15),_d15(d15),
  _t16(t16),_d16(d16),_t17(t16),_d17(d16)
  {
    nbInput = 16;
    lastT = _t16; penT = _t15;
    lastD = _d16; penD = _d15;
  }


  cycleFunctionTime(double t1, double d1, double t2, double d2, double t3, double d3, double t4, double d4, double t5, double d5, double t6, double d6, double t7, double d7,
                    double t8, double d8, double t9, double d9, double t10, double d10, double t11, double d11, double t12, double d12, double t13, double d13,
                    double t14, double d14, double t15, double d15, double t16, double d16, double t17, double d17)
  : simpleFunctionTime<double>(d1,true,1.),_d1(d1), _t1(t1), _d2(d2), _t2(t2), _d3(d3), _t3(t3), _t4(t4), _d4(d4),_t5(t5),_d5(d5),_t6(t6),_d6(d6), _d7(d7), _t7(t7),
  _d8(d8),_t8(t8), _d9(d9),_t9(t9), _d10(d10),_t10(t10), _d11(d11),_t11(t11), _d12(d12),_t12(t12), _t13(t13),_d13(d13), _t14(t14),_d14(d14), _t15(t15),_d15(d15),
  _t16(t16),_d16(d16),_t17(t17),_d17(d17)
  {
    nbInput = 17;
    lastT = _t17; penT = _t16;
    lastD = _d17; penD = _d16;
  }

  virtual ~cycleFunctionTime(){}
  virtual double operator () (double x, double y, double z) const{
    double val =0.;
    if(time<=_t1)
      val = _d1; //_d1*time/_t1;
    else if ((_t1<time) and (time<=_t2) and (nbInput >2))
      val = _d1+((_d2-_d1)/(_t2-_t1)*(time-_t1));
    else if ((_t2<time) and (time<=_t3) and (nbInput >3))
      val =  _d2+((_d3-_d2)/(_t3-_t2)*(time-_t2));
    else if ((_t3<time) and (time<=_t4) and (nbInput >4))
      val =  _d3+((_d4-_d3)/(_t4-_t3)*(time-_t3));
    else if ((_t4<time) and (time<=_t5) and (nbInput >5))
      val =  _d4+((_d5-_d4)/(_t5-_t4)*(time-_t4));

    else if ((_t5<time) and (time<=_t6) and (nbInput >6))
      val =  _d5+((_d6-_d5)/(_t6-_t5)*(time-_t5));
    else if ((_t6<time) and (time<=_t7) and (nbInput >7))
      val =  _d6+((_d7-_d6)/(_t7-_t6)*(time-_t6));
    else if ((_t7<time) and (time<=_t8) and (nbInput >8))
      val =  _d7+((_d8-_d7)/(_t8-_t7)*(time-_t7));

    else if ((_t8<time) and (time<=_t9) and (nbInput >9))
      val =  _d8+((_d9-_d8)/(_t9-_t8)*(time-_t8));
    else if ((_t9<time) and (time<=_t10) and (nbInput >10))
      val =  _d9+((_d10-_d9)/(_t10-_t9)*(time-_t9));
    else if ((_t10<time) and (time<=_t11) and (nbInput >11))
      val =  _d10+((_d11-_d10)/(_t11-_t10)*(time-_t10));
    else if ((_t11<time) and (time<=_t12) and (nbInput >12))
      val =  _d11+((_d12-_d11)/(_t12-_t11)*(time-_t11));

    else if ((_t12<time) and (time<=_t13) and (nbInput >13))
      val =  _d12+((_d13-_d12)/(_t13-_t12)*(time-_t12));
    else if ((_t13<time) and (time<=_t14) and (nbInput >14))
      val =  _d13+((_d14-_d13)/(_t14-_t13)*(time-_t13));
    else if ((_t14<time) and (time<=_t15) and (nbInput >15))
      val =  _d14+((_d15-_d14)/(_t15-_t14)*(time-_t14));
    else if ((_t15<time) and (time<=_t16) and (nbInput >16))
      val =  _d15+((_d16-_d15)/(_t16-_t15)*(time-_t15));

    else
      val =  penD+(lastD-penD)/(lastT-penT)*(time-penT);
    return val;
  }
};

class stepFunctionTime : public simpleFunctionTime<double>{
 protected:
  const int _nstep;
  const double _stepvalue;
  const double _tswitch;
  const double _tend;
  double _currentstep;
  double _currenttime;
 public:
  // BE AWARE tend > t_final simulation!!
  stepFunctionTime(const double fvalue,const int nstep,const double tend) : simpleFunctionTime<double>(1.,true,1.), _nstep(nstep),
                                                                             _stepvalue(fvalue/nstep),_tswitch(tend/nstep), _tend(tend),
                                                                            _currentstep(_stepvalue),_currenttime(_tswitch){}
  virtual ~stepFunctionTime(){}
  virtual void setTime(const double t)
  {
    time=t;
    if(time >_currenttime)
    {
      _currenttime+=_tswitch;
      _currentstep+=_stepvalue;
    }
  }
  virtual double operator () (double x,double y, double z) const{
    return _currentstep;
  }
};

class beltmanPressure : public simpleFunctionTime<double>
{
 protected:
  const double _vcj;
  const double _pcj;
  const SPoint3 _detonationSource;
  const SVector3 _detonationDirection;
  mutable double T0;
 public:
  beltmanPressure(const double x0, const double y0,const double z0,
                  const double u, const double v, const double w,
                  const double vcj,const double pcj) : simpleFunctionTime<double>(0,true,1.),_detonationSource(x0,y0,z0),
                                                       _detonationDirection(u,v,w), _vcj(vcj), _pcj(pcj){}
 #ifndef SWIG
  virtual ~beltmanPressure(){}
  virtual double operator() (double x,double y,double z) const
  {
    // distance from the source
    SPoint3 baryelem(x,y,z);
    SVector3 tmp(baryelem,_detonationSource);
    double distOnvcj = fabs(dot(tmp,_detonationDirection)) / _vcj;

    if(time < distOnvcj) return 0.;
    if(distOnvcj==0.)
      T0 = 1.; // avoid dividing by 0.
    else
      T0 = 3.*distOnvcj;
    return (_pcj * exp(-(time-distOnvcj)/T0));
  }
 #endif // SWIG
};

class bulletPlatePressure : public simpleFunctionTime<double>
{
 protected:
  const double _bulletDiameter;
  const double _holePlateDiameter;
  const double _p0;
  const SPoint3 _plateCenter;
  const double _bulletVelocity;
  const double _tanbulletAngle; // angle in rad !!
  double _distContact; // change with time
 public:
  bulletPlatePressure(const double x0, const double y0, const double z0,
                      const double db, const double dh, const double p0,
                      const double vi, const double alpha) : simpleFunctionTime<double>(0,true,1.),
                                                             _bulletDiameter(db), _holePlateDiameter(dh),
                                                             _p0(p0), _plateCenter(x0,y0,z0), _distContact(0.),
                                                             _tanbulletAngle(tan(alpha)), _bulletVelocity(vi){}
 #ifndef SWIG
  virtual ~bulletPlatePressure(){}
  virtual void setTime(const double t)
  {
    this->time = t;
    // compute contact distance
    _distContact = 0.5*_holePlateDiameter + _bulletVelocity * t / _tanbulletAngle;
    if(_distContact > 0.5*_bulletDiameter)
    {
      _distContact = 0.5*_bulletDiameter;
    }
  }
  virtual double operator() (double x, double y,double z) const
  {
    SPoint3 bary(x,y,z);
    double d = bary.distance(_plateCenter);
    if( d > _distContact)
      return 0.;
    else{
      return _p0;
    }
  }
 #endif // SWIG
};

class functionVelocityDCB : public simpleFunctionTime<double>{
 protected:
  const int _beamAxis; // x=0, y=1, z=2
  const double _beamLength; // length of beam supposed to have origin in 0
  const double _3rdPowerLength; // _beamLength^3
  const double _halfLength; // 0.5*_beamLength
 public:
  functionVelocityDCB(const double val_, const int baxis, const double blength) : simpleFunctionTime<double>(val_,false,1.),
                                                                                  _beamAxis(baxis), _beamLength(blength),
                                                                                  _3rdPowerLength(blength*blength*blength),
                                                                                  _halfLength(0.5*blength){}
 #ifndef SWIG
  functionVelocityDCB(const functionVelocityDCB &source) : simpleFunctionTime<double>(source), _beamAxis(source._beamAxis),
                                                           _beamLength(source._beamLength), _3rdPowerLength(source._3rdPowerLength),
                                                           _halfLength(source._halfLength){}
  virtual ~functionVelocityDCB(){}
  virtual double operator() (double x, double y,double z) const
  {
    double axiscoord;
    switch(_beamAxis){
     case 0:
      axiscoord = x;
      break;
     case 1:
      axiscoord = y;
      break;
     case 2:
      axiscoord = z;
      break;
    }
    if(axiscoord > _halfLength)
      axiscoord = _beamLength - axiscoord; // Symmetric profile
    return ( (4*axiscoord*axiscoord)/_3rdPowerLength*(3*_beamLength - 4*axiscoord )*this->_val);
  }
 #endif // SWIG
};

class functionDistanceFromPointByAxis : public simpleFunctionTime<double>
{
 protected:
  const int _dir; // x=0, y=1, z=2
  const double _pt; // value from which the distance is computed
 public:
  functionDistanceFromPointByAxis(const int dir, const double pt,const double val) : simpleFunctionTime<double>(val,false,0.),
                                                                                     _dir(dir), _pt(pt){}
 #ifndef SWIG
  functionDistanceFromPointByAxis(const functionDistanceFromPointByAxis &source) : simpleFunctionTime<double>(source),
                                                                                   _dir(source._dir), _pt(source._pt){}
  virtual ~functionDistanceFromPointByAxis(){}
  virtual double operator() (double x, double y,double z) const
  {
    double axiscoord;
    switch(_dir){
     case 0:
      axiscoord = x;
      break;
     case 1:
      axiscoord = y;
      break;
     case 2:
      axiscoord = z;
      break;
    }
    return (axiscoord-_pt)*this->_val; // negative if the coordinate is less than the axis coordinate !!
  }
 #endif // SWIG
};


class radialVelocityRing : public simpleFunctionTime<double>
{
 protected:
  // _w0 = val;
  const double _t0;
  const SPoint3 _origin;
  const int _comp; // to known if vx(=0) or vy(=1) has to be returned
 public:
  radialVelocityRing(const double w,const double t,const int comp) : simpleFunctionTime<double>(w,true,0.), _t0(t),
                                                                     _origin(0.,0.,0.), _comp(comp){}
 #ifndef SWIG
  radialVelocityRing(const radialVelocityRing &source) : simpleFunctionTime<double>(source), _t0(source._t0),
                                                         _origin(source._origin), _comp(source._comp){}
  virtual ~radialVelocityRing(){}
  virtual double operator() (double x, double y,double z) const
  {
    double w = _val/(2.*3.141592654);
    if(time<=_t0)
    {
      w*=(time/_t0);
    }

    SPoint3 P(x,y,z);
    double radius = _origin.distance(P);
    double alpha = asin(fabs(x)/radius);
    double wr = w*radius;
    if(_comp==0)
    {
      double cosa = cos(alpha);
      if(y>0)
        return (-wr*cosa);
      else
        return (wr*cosa);
    }
    else if(_comp==1)
    {
      double sina = sin(alpha);
      if(x<0)
        return (-wr*sina);
      else
        return (wr*sina);
    }
    else
    {
      Msg::Error("_comp has to be equal to 0 or 1!");
    }
    return 0.;
  }
 #endif // SWIG
};



class centrifugalForceRing : public simpleFunctionTime<double>
{
 protected:
  // _w0 = val;
  const double _t0;
  const SPoint3 _origin;
  const int _comp; // to known if vx(=0) or vy(=1) has to be returned
  const double _rho; // density
  mutable double _w,_rw2;
 public:
  centrifugalForceRing(const double rho,const double w,const double t,const int comp) : simpleFunctionTime<double>(w/(2.*3.141592654),true,0.), _t0(t),
                                                                     _origin(0.,0.,0.), _comp(comp), _w(0.), _rho(rho), _rw2(0.){}
 #ifndef SWIG
  centrifugalForceRing(const centrifugalForceRing &source) : simpleFunctionTime<double>(source), _t0(source._t0),
                                                         _origin(source._origin), _comp(source._comp), _w(0.), _rho(source._rho), _rw2(0){}
  virtual ~centrifugalForceRing(){}
  virtual void setTime(const double t)
  {
    time = t;
    if(time<=_t0)
    {
      _w=_val*(time/_t0);
    }
    else
    {
      _w = _val;
    }
    _rw2 = _rho*_w*_w;
  }

  virtual double operator() (double x, double y,double z) const
  {
    if(_comp==0)
      return _rw2*x;
    else if(_comp==1)
      return _rw2*y;
    else
      Msg::Error("_comp has to be equal to 0 or 1!");
    return 0.;
  }
 #endif // SWIG
};

class centrifugalForce : public simpleFunctionTime<double>
{
 protected:
  // _w0 = val;
  const double _t0;
  const SPoint3 _origin;
  const SPoint3 _direction;
  const int _comp; // to known if vx(=0) or vy(=1) has to be returned
  const double _rho; // density
  mutable double _w,_rw2; // in this function, w is rad/s and is the value to be reached at time =t0
 public:
  centrifugalForce(const double rho,const double w,const double t,const int comp) : simpleFunctionTime<double>(w,true,0.), _t0(t),
                                                                     _origin(0.,0.,0.), _direction(0.,0.,1.), _comp(comp), _w(0.), _rho(rho), _rw2(0.){}
  centrifugalForce(const double rho,const double w,double x0, double y0, double z0, double dirx, double diry, double dirz, const double t,const int comp) : simpleFunctionTime<double>(w,true,0.), _t0(t),
                                                                     _origin(x0,y0,z0),
                                                              _direction(dirx/sqrt(dirx*dirx+diry*diry+dirz*dirz),diry/sqrt(dirx*dirx+diry*diry+dirz*dirz),dirz/sqrt(dirx*dirx+diry*diry+dirz*dirz)),
                                                               _comp(comp), _w(0.), _rho(rho), _rw2(0.)
   {
    double norm=sqrt(dirx*dirx+diry*diry+dirz*dirz);
    if(norm==0.)
       Msg::Error("CentrifugalForce, direction vector not correct");
   }
 #ifndef SWIG
  centrifugalForce(const centrifugalForce &source) : simpleFunctionTime<double>(source), _t0(source._t0),
                                                         _origin(source._origin),
                                                         _direction(source._direction),_comp(source._comp), _w(0.), _rho(source._rho), _rw2(0){}
  virtual ~centrifugalForce(){}
  virtual void setTime(const double t)
  {
    time = t;
    if(time<=_t0)
    {
      _w=_val*(time/_t0);
    }
    else
    {
      _w = _val;
    }
    _rw2 = _rho*_w*_w;
  }

  virtual double operator() (double x, double y,double z) const
  {
    SPoint3 loc(x,y,z);
    SPoint3 d(loc-_origin);
    double dot=d.x()*_direction.x()+d.y()*_direction.y()+d.z()*_direction.z();
    SPoint3 vec(loc-_direction*dot);

    if(_comp==0)
      return _rw2*vec.x();
    else if(_comp==1)
      return _rw2*vec.y();
    else if(_comp==2)
      return _rw2*vec.z();
    else
      Msg::Error("_comp has to be equal to 0, 1 or 2!");
    return 0.;
  }
 #endif // SWIG
};

class linearPositionFunctionTime : public simpleFunctionTime<double>{
  protected:
    double _a, _b, _c, _d; // ax+by+cz+d

  public:
    linearPositionFunctionTime(const double a, const double b, const double c, const double d,
                               bool td=true, double t=1.): simpleFunctionTime<double>(0,td,t),
                               _a(a),_b(b),_c(c),_d(d){};
    virtual ~linearPositionFunctionTime(){};
  #ifndef SWIG
    virtual double operator () (double x, double y, double z) const {
      if (this->timeDependency){
        return time*(_a*x+_b*y+_c*z+_d);
      }
      else return  _a*x+_b*y+_c*z+_d;
    }
  #endif //SWIG
};

class PiecewisePositionFunctionTime : public simpleFunctionTime<double>{
  protected:
    int component;
    std::vector<double> var;
    std::vector<double> func;
    int interpMethod; // 0 -linear, 1- cubic spline
    #ifndef SWIG
    mutable std::vector<double> dfunc; // cache for derivatives for cubic spline
    #endif //SWIG

  public:
    PiecewisePositionFunctionTime(const int comp,  const int interpM= 0, bool td = true, double t = 1.): simpleFunctionTime<double>(0,td,t),var(0),func(0),
      component(comp),interpMethod(interpM),dfunc(0){}
    void setInterpolationMethod(const int i) {interpMethod = i;};
    void put(const double v, const double f){
      var.push_back(v);
      func.push_back(f);
    }
    void scale(const double s){
      for (int i=0; i< func.size(); i++){
        func[i] *= s;
      }

      for (int i=0; i< dfunc.size(); i++){
        dfunc[i] *= s;
      }
    }
    #ifndef SWIG
    virtual ~PiecewisePositionFunctionTime(){};
    virtual double operator () (double x, double y, double z) const {
      double val, fval;
      if (component == 0){
        val = x;
      }
      else if (component == 1){
        val = y;
      }
      else if (component == 2){
        val = z;
      }
      else{
        Msg::Error("component = %d must be correctly defined by (0 == X, 1 == Y, 2 === Z)",component);
      }

      int N = func.size();
      if (interpMethod == 0){
        fval = interpolationMethod::interp_linear(val,&var[0],&func[0],N);
      }
      else if (interpMethod == 1){
        if  (dfunc.size() == 0){
          dfunc.resize(N);
          for (int i=0; i< N-1; i++){
            if (i == 0){
              dfunc[0] = (func[1] - func[0])/(var[1]-var[0]);
            }
            else if (i == N-1){
              dfunc[N-1] = (func[N-1] - func[N-2])/(var[N-1]-var[N-2]);
            }
            else{
              dfunc[i] = 0.5*(func[i]- func[i-1])/(var[i]-var[i-1])+ 0.5*(func[i+1]- func[i])/(var[i+1]-var[i]);
            }
          }
        }
        fval =  interpolationMethod::interp_cubicSpline(val,&var[0],&func[0],&dfunc[0],N);
      }
      else{
        Msg::Error("interp method %d has not been implemented in PiecewisePositionFunctionTime",interpMethod);
      }

      if (this->timeDependency){
        return time*fval;
      }
      else return  fval;
    }
  #endif //SWIG
};

class exponentialFunctionTime : public simpleFunctionTime<double>{
  // f = a+b*exp(c*t)
  protected:
    double _a, _b, _c;

  public:
    exponentialFunctionTime(const double a, const double b, const double c): simpleFunctionTime<double>(0,true,0),_a(a),_b(b),_c(c){}
    #ifndef SWIG
    virtual ~exponentialFunctionTime(){};
    virtual double operator () (double x, double y, double z) const {
      return _a+_b*exp(_c*time);
    };
    #endif //SWIG

};

class LinearFunctionTime : public simpleFunctionTime<double>{
  protected:
    double _f0, _t0, _a; // f = f0+a*(x-x0)
    double _t1, _f1;

  public:
    LinearFunctionTime(const double t0, const double f0, const double a):simpleFunctionTime<double>(f0,true,t0),
    _f0(f0),_t0(t0),_a(a),_t1(1e100),_f1(0.){}
    LinearFunctionTime(const double t0, const double f0, const double t1, const double f1):simpleFunctionTime<double>(f0,true,t0),
    _f0(f0),_t0(t0),_t1(t1),_f1(f1){
      _a = (f1-f0)/(t1-t0);
    }
  #ifndef SWIG
    virtual ~LinearFunctionTime(){}
    virtual double operator () (double x, double y, double z) const {
      if (time < _t1){
        return _f0+ _a*(time - _t0);
      }
      else{
        return _f1;
      }
    };
  #endif // SWIG
};

class  PiecewiseLinearFunction: public simpleFunctionTime<double>{
  protected:
    std::vector<double> _time;
    std::vector<double> _f;

  public:
    PiecewiseLinearFunction(bool td = true, double t = 1.): simpleFunctionTime<double>(0,td,t),_time(0),_f(0){}
    void put(const double t, const double val){
      _time.push_back(t);
      _f.push_back(val);
    }
    #ifndef SWIG
    virtual ~PiecewiseLinearFunction(){}
    virtual double operator () (double x, double y, double z) const {
      int s = _time.size();
      double t0(0.), t1(0.), v0(0.), v1(0.);
      if (time >= _time[s-1]){
        t0 = _time[s-2]; v0 = _f[s-2];
        t1 = _time[s-1]; v1 = _f[s-1];
      }
      else{
        for (int i=0; i<s-1; i++){
          if (_time[i]<=time and time< _time[i+1]){
            t0 = _time[i]; v0 = _f[i];
            t1 = _time[i+1]; v1 = _f[i+1];
            break;
          }
        }
      }

      double v = (time -t0)*(v1-v0)/(t1-t0) + v0;

      return v;
    }
  #endif //SWIG

};

class PiecewiseRateFunction: public simpleFunctionTime<double>{
  protected:
    std::vector<double> _time;
    std::vector<double> _rate;
    mutable std::vector<double> _pick;
    mutable bool _isEsPick;
  public:
    PiecewiseRateFunction(bool td = true, double t = 1.): simpleFunctionTime<double>(0,td,t),_time(0),_rate(0),_isEsPick(false){}
    void put(const double t, const double val){
      _time.push_back(t);
      _rate.push_back(val);
    }
    #ifndef SWIG
    virtual ~PiecewiseRateFunction(){}
    virtual double operator () (double x, double y, double z) const {
      int N = _time.size();
      if (_isEsPick == false){
        _isEsPick = true;
        _pick.resize(N);
        for (int i=0; i<N; i++){
          double dt =0.;
          if (i==0) dt = _time[0];
          else dt = _time[i] - _time[i-1];

          _pick[i] = 0.;
          if (i==0) _pick[i] = _rate[0]*dt;
          else _pick[i] = _pick[i-1]+_rate[i]*dt;

          Msg::Info("tpick= %e rate = %e Upick = %e",_time[i],_rate[i],_pick[i]);
        }
      }
      double t0(0.), t1(0.), v0(0.), v1(0.);
      if (time > _time[N-1]){
        t0 = _time[N-2]; v0 = _pick[N-2];
        t1 = _time[N-1]; v1 = _pick[N-1];
      }
      else if (time<= _time[0]){
        t0 = 0.; v0 = 0.;
        t1 = _time[0]; v1 = _pick[0];
      }
      else{
        for (int i=0; i<N-1; i++){
          if (_time[i]<=time and time<= _time[i+1]){
            t0 = _time[i]; v0 = _pick[i];
            t1 = _time[i+1]; v1 = _pick[i+1];
            break;
          }

        }
        if (t0 == t1)
          Msg::Error("t=%e, t0=%e, v0=%e, t1=%e, v1=%e",time,t0,v0,t1,v1);
      }

      double v = (time -t0)*(v1-v0)/(t1-t0) + v0;

      //Msg::Error("t=%e, v=%e, t0=%e, v0=%e, t1=%e, v1=%e",time,v,t0,v0,t1,v1);

      return v;
    }
    #endif // SWIG
};

class BCfunctionBendWings : public simpleFunctionTime<double>
{
  protected:
    const double _theta;
    const double _Lx;
    const double _nu;
  public:
    BCfunctionBendWings(const double magnitude,const double nu,const double Lx,
                        const double overhang) : simpleFunctionTime<double>(magnitude,false,1.),
                                               _theta(magnitude/overhang), _nu(nu), _Lx(Lx){}
    BCfunctionBendWings(const BCfunctionBendWings &source) : simpleFunctionTime<double>(source),
                                                             _theta(source._theta), _nu(source._nu), _Lx(source._Lx){}
    virtual ~BCfunctionBendWings(){}
  #ifndef SWIG
    virtual double operator () (double x, double y, double z) const {
      return -_theta*x*(1.0-x/_Lx)-_nu*_theta/_Lx*y*y;
    }
  #endif //SWIG
};
#endif // TIMEFUNCTION

