//
// Description: solve Ax = b with GMRESk algorithms written in MPI
//
// Author:  <Gauthier BECKER>, (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef LINEARSYSTEMGMRESK_H_
#define LINEARSYSTEMGMRESK_H_
#include "GmshConfig.h"
#include "GmshMessage.h"
#include "linearSystem.h"
#include <vector>
template <class scalar>
class linearSystemGMRESk : public linearSystem<scalar> {
  protected:
  int _nbRows, _nbColumns; // local size
  bool _isAllocated;
  double* _a; // _nbRows*_nbColumns matrix is store by column to be compatible with BLAS (i,j) = i + _nbRows*j
  double* _b; // _nbRows
  double* _x; // _nbRows
  // gmres parameters
  int _k,_kp1; // number of vector of subspace
  const int _maxGMRESite;
  const double _tolGMRES;
 private: // cache data for system solve
  int *_sizeRank; // Number of unknowns of each rank
  double* _H;
  double* _y;
  double* _V;
  double* _r;
  double* _z;
  double* _Vku;
  double* _c;
  double* _s;
  std::vector<double*> _VdotVu; // regroup all scalar product in a 2 columns matrix (one BLAS and MPI call)
 public:
  linearSystemGMRESk(const int k=36,const int maxite=1000,
                     const double tol=1.e-7)  : _isAllocated(false), _nbRows(0), _nbColumns(0),_sizeRank(NULL), _a(NULL), _b(NULL),
                                                _x(NULL), _k(k), _maxGMRESite(maxite), _tolGMRES(tol), _H(NULL), _y(NULL), _V(NULL),
                                                _r(NULL), _z(NULL), _c(NULL), _s(NULL),_VdotVu(k+1), _Vku(NULL), _kp1(k+1)
  {
   #if defined(HAVE_MPI)
    if(Msg::GetCommSize() > 1)
    {
      _sizeRank = new int[Msg::GetCommSize()];
    }
   #endif // HAVE_MPI
    Msg::Info("Default value for GMRESk. number of k vector max: %d, maximal iteration: %d, tolerance: %e",_k,_maxGMRESite,_tolGMRES);
  }
  virtual void clear();
  virtual ~linearSystemGMRESk()
  {
    this->clear();
  }
  virtual bool isAllocated() const { return _isAllocated; }
  virtual void setNbColumns(const int col){_nbColumns = col;}
  virtual void allocate(int nbRows);
  virtual void getFromMatrix(int row, int col, scalar &val) const;
  virtual void addToRightHandSide(int row, const scalar &val, int ith=0);
  virtual void getFromRightHandSide(int row, scalar &val) const;
  virtual double normInfRightHandSide() const;
  virtual void addToMatrix(int row, int col, const scalar &val);
  virtual void getFromSolution(int row, scalar &val) const;
  virtual void zeroMatrix();
  virtual void zeroRightHandSide();
  virtual int systemSolve();
  virtual void printSystem();

  virtual void addToSolution(int _row, const scalar &val) {};
  virtual void zeroSolution() {};
};



#endif //LINEARSYSTEMGMRESK_H_
