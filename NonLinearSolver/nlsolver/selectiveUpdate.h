//
// C++ Interface: selective update
//
// Author:  <Van Dung Nguyen>, (C) 2018
//
// Copyright: See COPYING file that comes with this distribution
//
//


#ifndef SELECTIVEUPDATE_H_
#define SELECTIVEUPDATE_H_

#ifndef SWIG
#include "ipvariable.h"
#include "ipField.h"
#endif // SWIG

class selectiveUpdateBase{
  protected:
    bool _reSolveBySelectiveUpdate;

  public:
    #ifndef SWIG
    selectiveUpdateBase():_reSolveBySelectiveUpdate(false){};
    selectiveUpdateBase(const selectiveUpdateBase& src):_reSolveBySelectiveUpdate(src._reSolveBySelectiveUpdate){}
    virtual ~selectiveUpdateBase(){}
    virtual bool solverIterateBySelectiveUpdate() const {return _reSolveBySelectiveUpdate;};

    virtual void nextStep();
    virtual void resetToPreviousStep();

    virtual void checkSelectiveUpdate(nonLinearMechSolver* solver) = 0;
    virtual void selectiveUpdate(nonLinearMechSolver* solver) = 0;
    
    virtual selectiveUpdateBase* clone() const = 0;
    
  protected:
    virtual void restoreNonBrokenIPVariable(IPField* ipf, const std::set<int>& nonBrokenIPs) const =0;
    virtual void setSelectiveUpdate(const bool fl){_reSolveBySelectiveUpdate=fl;};  // cannot use outside class
    #endif // SWIG
};


class cohesiveCrackCriticalIPUpdate : public selectiveUpdateBase{
  protected:
    std::set<int> _newFailedElements;
    std::set<int> _newBlockedElements;

  public:
    cohesiveCrackCriticalIPUpdate();
    #ifndef SWIG
    cohesiveCrackCriticalIPUpdate(const cohesiveCrackCriticalIPUpdate& src);
    virtual ~cohesiveCrackCriticalIPUpdate();

    virtual void nextStep();
    virtual void resetToPreviousStep();

    virtual void checkSelectiveUpdate(nonLinearMechSolver* solver);
    virtual void selectiveUpdate(nonLinearMechSolver* solver);
    
    virtual selectiveUpdateBase* clone() const {return new cohesiveCrackCriticalIPUpdate(*this);};
    
  protected:
    virtual void restoreNonBrokenIPVariable(IPField* ipf, const std::set<int>& nonBrokenIPs) const {};
    #endif // SWIG

};




class cohesiveCrackCriticalIPPorousUpdate : public cohesiveCrackCriticalIPUpdate{
  protected:

  public:
    cohesiveCrackCriticalIPPorousUpdate();
    #ifndef SWIG
    cohesiveCrackCriticalIPPorousUpdate(const cohesiveCrackCriticalIPPorousUpdate& src);
    virtual ~cohesiveCrackCriticalIPPorousUpdate();
    
    virtual selectiveUpdateBase* clone() const {return new cohesiveCrackCriticalIPPorousUpdate(*this);};
    
  protected:
    virtual void restoreNonBrokenIPVariable(IPField* ipf, const std::set<int>& nonBrokenIPs) const;
    #endif // SWIG

};


#endif // SELECTIVEUPDATE_H_
