//
// Author:  <Van Dung NGUYEN>, (C) 2020
//
// Copyright: See COPYING file that comes with this distribution
//
//


#ifndef SHOWDATAONMESH_H_
#define SHOWDATAONMESH_H_

#ifndef SWIG
#include "elementGroup.h"
#include "GModel.h"
#endif // SWIG

class showDataOnMesh 
{
  protected:
    #ifndef SWIG
    GModel* _pModel;
    elementGroup* _gr;
    std::string _meshFileName;
    std::map<MElement*, std::vector<double> > _GPData; 
    std::map<MElement*, std::vector<int> > _GPFound; 
    std::map<MVertex*, std::vector<double> > _nodeData;
    GModel* _part;
    void write_MSH2(elementGroup* g,  const std::string filename);
    #endif //
  public:
    showDataOnMesh(const std::string meshFile);
    ~showDataOnMesh();
    void dataOnPhysical(int dim, int phys, const std::string partMeshFile);
    void addDataPoint(double x, double y, double z, double val);
    //
    void writeElementDataToFile(const std::string filename);
    //
    void addNodeData(double x, double y, double z, double val, double tol=1e-4);
    void writeNodeDataToFile(const std::string filename);
};

#endif //SHOWDATAONMESH_H_