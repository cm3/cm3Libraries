//
// Description: JIntegral
//
//
// Author:  <Van-Dung NGUYEN>, (C) 2023
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef JINTEGRALBYDOMAININTEGRATION_H_
#define JINTEGRALBYDOMAININTEGRATION_H_

#ifndef SWIG
#include <vector>
#include <string>
#include "MElement.h"
class nonLinearMechSolver;
class LaplaceSolver;
#endif // SWIG

class computeJPlan
{
  public:
    #ifndef SWIG
    computeJPlan(){}
    virtual ~computeJPlan(){}
    virtual bool willCompute(double curTime, int step) const = 0;
    virtual computeJPlan* clone() const = 0;
    #endif //SWIG
};

class computedJPlanTrivial: public computeJPlan
{
  public:
    computedJPlanTrivial(): computeJPlan() {}
    virtual ~computedJPlanTrivial(){}
    #ifndef SWIG
    virtual bool willCompute(double curTime, int step) const {return true;};
    virtual computeJPlan* clone() const {return new computedJPlanTrivial();}
    #endif //SWIG
};

class computeJElementFilter 
{
  public:
    #ifndef SWIG
    virtual ~computeJElementFilter(){}
    virtual bool operator()(MElement *ele) const = 0;
    virtual computeJElementFilter* clone() const = 0;
    #endif //SWIG
};
class computeJElementFilterTrivial : public computeJElementFilter
{
  public:
    virtual ~computeJElementFilterTrivial(){}
    #ifndef SWIG
    virtual bool operator()(MElement *ele) const {return true;};
    virtual computeJElementFilter* clone() const {return new computeJElementFilterTrivial();}
    #endif //SWIG
};

class computeJElementFilterLayer : public computeJElementFilter
{
  protected:
      int _direction;
      double _lowerBound, _upperBound;
  public:
    computeJElementFilterLayer(int dir, double lowerBound, double upperBound):
          _direction(dir), _upperBound(upperBound), _lowerBound(lowerBound){}
    #ifndef SWIG
    virtual ~computeJElementFilterLayer(){}
    virtual bool operator()(MElement *ele) const;
    virtual computeJElementFilter* clone() const {return new computeJElementFilterLayer(_direction, _lowerBound, _upperBound);}
  protected:
    bool inside(MVertex* v) const;
    #endif //SWIG
};

class computeJIntegral
{
  protected:
    #ifndef SWIG
    computeJElementFilter* _elementFilter;
    LaplaceSolver* _laplaceSolver;
    std::vector<std::pair<int,int> > _innerBoundary;
    std::vector<std::pair<int,int> > _outerBoundary;
    std::vector<std::pair<int,int> > _integratedDomain;
    FILE* _pFile; // data to file
    std::string _name;
    computeJPlan* _plan;
    double _lastTime;
    int _lastIterationIndex;
    #endif
    
  public:
    computeJIntegral(const std::string name);
    ~computeJIntegral();
    std::string getName() const {return _name;};
    // multiple physicals can be set by calling this function multipe times
    void setIntegratedDomain(int dim, int physical);
    // multiple physicals can be set by calling this function multipe times
    void setInnerBoundary(int dim, int physical);
    // multiple physicals can be set by calling this function multipe times
    void setOuterBoundary(int dim, int physical);
    //
    void setElementFilter(const computeJElementFilter& eleFilter);
    void openFile();
    void closeFile();
    void setComputeJPlan(const computeJPlan& pl);
    void computeJAndWriteData(nonLinearMechSolver* solver, bool force=false);
    std::vector<double> computeJ(nonLinearMechSolver* solver);
};


#endif //JINTEGRALBYDOMAININTEGRATION_H_