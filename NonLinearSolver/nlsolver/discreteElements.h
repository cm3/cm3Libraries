//
//
// Description: Class of discret elements
//
//
// Author:  <V.D. NGUYEN>, (C) 2024
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef _DISCRETEELEMENT_H_
#define _DISCRETEELEMENT_H_

#include "fullMatrix.h"
#ifndef SWIG
#include "functionSpace.h"
#include "MVertex.h"
#include "pbcVertex.h"
#include "generalMapping.h"
#include "simpleFunctionTime.h"
#include "unknownField.h"
class nonLinearMechSolver;
#endif

class discreteElement
{
  #ifndef SWIG
  public:
    discreteElement(){}
    discreteElement(const discreteElement& src) {}
    virtual ~discreteElement(){}
    virtual discreteElement* clone() const = 0;
    virtual void initialize(nonLinearMechSolver* solver) = 0;
    virtual void setTime(double time) = 0;
    virtual void getForce(std::vector<Dof>& R, fullVector<double>& vFor) const = 0;
    virtual void getStiffness(std::vector<Dof>& RC, fullMatrix<double>& mat) const = 0;
  #endif
};


class MultipleDofNonLinearSpring : public discreteElement
{
  protected:
    #ifndef SWIG
    // F = _nonlinearFunction(_dependentPoints)*_func(t)
    // setting data
    SPoint3 _location;
    std::vector<std::pair<SPoint3, int> >_dependentPoints;
    int _comp; // direction
    generalMapping* _dofFunc;
    simpleFunctionTime<double>* _timeFunc;  
    
    // runtime data
    pbcVertex* _v; // location the spring is applied
    std::vector<std::pair<pbcVertex*, int> >_dependentVertices;
    nonLinearMechSolver* _solver;
    #endif //SWIG
    
  public:
    MultipleDofNonLinearSpring(double x, double y, double z, int comp, double A, generalMapping* func);
    MultipleDofNonLinearSpring(const MultipleDofNonLinearSpring& src);
    virtual ~MultipleDofNonLinearSpring();
    void setDependentPoint(double x, double y, double z, int comp);
    #ifndef SWIG
    virtual discreteElement* clone() const;
    virtual void initialize(nonLinearMechSolver* solver);
    virtual void setTime(double time);
    virtual void getForce(std::vector<Dof>& R, fullVector<double>& vFor) const;
    virtual void getStiffness(std::vector<Dof>& RC, fullMatrix<double>& mat) const;
    #endif
    
  private:
    #ifndef SWIG
    pbcVertex* getVertex(nonLinearMechSolver* solver,  const SPoint3& point) const;
    #endif //SWIG
};


#endif //_DISCRETEELEMENT_H_
