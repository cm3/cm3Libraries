//
// Author:  <Van Dung NGUYEN>, (C) 2015
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef IMPERFECTIONTOMESH_H_
#define IMPERFECTIONTOMESH_H_

#ifndef SWIG

#include "elementGroup.h"
#include "GModel.h"
#endif // SWIG


class imperfectionFunction{
  #ifndef SWIG
  public:
    imperfectionFunction(){}

    imperfectionFunction(const imperfectionFunction& src){}
    virtual ~imperfectionFunction(){}
    virtual void getDirection(SVector3& u) const = 0;
    virtual double getMaxPert() const = 0;
    virtual double get(double x, double y, double z) const = 0;
    virtual imperfectionFunction* clone() const = 0;
  #endif // SWIG
};

class sphericalCapImperfectionFunction : public imperfectionFunction{
  #ifndef SWIG
  protected:
    SVector3 _direction; // direction vector
    SPoint3 _root; // root
    double _R; // redius
    double _H; // maximal pert

  #endif // SWIG

  public:
    sphericalCapImperfectionFunction(const double xc, const double yc, const double zc,
                                  const double xdir, const double ydir, const double zdir,
                                  const double R, const double H);
    #ifndef SWIG
    sphericalCapImperfectionFunction(const sphericalCapImperfectionFunction& src);
    virtual ~sphericalCapImperfectionFunction(){}
    virtual double getMaxPert() const {return _H;};
    virtual void getDirection(SVector3& u) const;
    virtual double get(double x, double y, double z) const;
    virtual imperfectionFunction* clone() const{return new sphericalCapImperfectionFunction(*this);};
    #endif // SWIG
};

class SinusSphericalCapImperfectionFunction : public sphericalCapImperfectionFunction{
  #ifndef SWIG
  protected:
    double _omega; // for sin
    SVector3 _axis;
  #endif // SWIG

  public:

    SinusSphericalCapImperfectionFunction(const double xc, const double yc, const double zc,
                                  const double xdir, const double ydir, const double zdir,
                                  const double R, const double H, const double omega,
                                  const double xdir2, const double ydir2, const double dirz2);
    #ifndef SWIG
    SinusSphericalCapImperfectionFunction(const SinusSphericalCapImperfectionFunction& src);
    virtual ~SinusSphericalCapImperfectionFunction(){};
    virtual double getMaxPert() const {return sphericalCapImperfectionFunction::getMaxPert();};
    virtual void getDirection(SVector3& u) const {sphericalCapImperfectionFunction::getDirection(u);};
    virtual double get(double x, double y, double z) const;
    virtual imperfectionFunction* clone() const{return new SinusSphericalCapImperfectionFunction(*this);};
    #endif // SWIG
};


class axisymmetricImperfectionFunction : public imperfectionFunction{
  #ifndef SWIG
  //for point x,y,z, disp = a*exp(-r^2/(2*c^2)
  protected:
    // axis of symmetry
    SVector3 _direction; // direction vector
    SPoint3 _root; // root
    double _a, _c; // function

  #endif // SWIG

  public:
    axisymmetricImperfectionFunction(const double xc, const double yc, const double zc,
                                  const double xdir, const double ydir, const double zdir,
                                  const double a, const double c);
    #ifndef SWIG
    axisymmetricImperfectionFunction(const axisymmetricImperfectionFunction& src);
    virtual ~axisymmetricImperfectionFunction(){}
    virtual double getMaxPert() const {return _a;};
    virtual void getDirection(SVector3& u) const;
    virtual double get(double x, double y, double z) const;
    virtual imperfectionFunction* clone() const{return new axisymmetricImperfectionFunction(*this);};
    #endif // SWIG
  //
};


class imperfectionPart{
  #ifndef SWIG
  public:
    std::vector<GEntity*> entities;
    imperfectionFunction* func;
  #endif // SWIG
  public:
    imperfectionPart();
    void addFunction(const imperfectionFunction& f);
    void addAllEntities();
    void addEntity(const int dim, const int num);
   #ifndef SWIG
    virtual ~imperfectionPart(){}
   #endif // SWIG
};


class imperfectionToMesh{
  #ifndef SWIG
  protected:
    GModel* _pModel;
    std::string _meshFileName;
    // for perturbation in mesh
    std::vector<imperfectionPart*> _allImperfectionPart;
    bool _supperposed; // flage
  #endif // SWIG

  public:
    imperfectionToMesh();
    void setSupperposed(const bool flag);
    void loadModel(const std::string meshFileName);
    void addImperfection(imperfectionPart* ip);
    void imposeImperfection(const std::string fname = "perturbed_mesh.msh");

    #ifndef SWIG
    virtual ~imperfectionToMesh(){ if (_pModel) delete _pModel;}
    #endif // SWIG
};

#endif // IMPERFECTIONTOMESH_H_
