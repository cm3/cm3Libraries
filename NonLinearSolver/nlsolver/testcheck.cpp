#include "testcheck.h"
#include <cmath>
#include "GmshMessage.h"
#if defined(HAVE_MPI)
#include "mpi.h"
#endif //HAVE_MPI



TestCheck::TestCheck(){}

TestCheck::~TestCheck(){}

void
TestCheck::equal(double ref, double cur, double tol) const
{
  //if(Msg::GetCommRank() != 0) return; // check only on rank 0
  //
  double curTr = cur;
  #if defined(HAVE_MPI)
  MPI_Bcast(&curTr,1,MPI_DOUBLE,0,MPI_COMM_WORLD);
  #endif //HAVE_MPI

  if(fabs(ref-curTr)/fabs(ref)>tol){
    Msg::Error("reference %e current value %e relative error %e tol %e",ref,curTr,fabs(ref-curTr)/fabs(ref),tol);
  }
  return;
}

void
TestCheck::equal(int ref, int cur) const
{
  //if(Msg::GetCommRank() != 0) return; // check only on rank 0
  int curTr = cur;
  #if defined(HAVE_MPI)
  MPI_Bcast(&curTr,1,MPI_INT,0,MPI_COMM_WORLD);
  #endif //HAVE_MPI

  if(ref != curTr)
    exit(1);
  return;
}
