//
//
// Description: Filter element in a box defined by 2 points (x0,y0,z0) and (x1,y1,z1)
//
//
// Author:  <Gauthier BECKER>, (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef _ELEMENTFILTERBOX_H_
#define _ELEMENTFILTERBOX_H_
#include "groupOfElements.h"
#include "MElement.h"


class elementFilterLocation : public elementFilter
{
public:
  #ifndef SWIG
  elementFilterLocation(){};
  virtual ~elementFilterLocation(){}
  virtual bool operator()(MElement *) const = 0;
  virtual elementFilterLocation* clone() const = 0;
  #endif //SWIG
};

class elementFilterLocationTrivial : public elementFilterLocation
{
 public:
  elementFilterLocationTrivial(): elementFilterLocation(){};
  virtual ~elementFilterLocationTrivial(){}
  virtual bool operator() (MElement *ele) const {return true;};
  virtual elementFilterLocation* clone() const {return new elementFilterLocationTrivial(); }
};
class elementFilterBox : public elementFilterLocation {
 protected:
  const SPoint3 _pt0;
  const SPoint3 _pt1;
  double xmin,xmax,ymin,ymax,zmin,zmax;
 public :
  elementFilterBox(const double x0,const double y0,const double z0,const double x1,const double y1,const double z1);
  virtual ~elementFilterBox(){}
  // true if barycenter is in the box
  virtual bool operator() (MElement *ele) const;
  virtual elementFilterLocation* clone() const {return new elementFilterBox(xmin, ymin, zmin, xmax, ymax, zmax); }
};

#endif // _ELEMENTFILTERBOX_H_
