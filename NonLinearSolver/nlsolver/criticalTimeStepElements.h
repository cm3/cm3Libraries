//
// C++ Interface: terms explicit
//
// Description: Files with definition of function : nonLinearMechSolver::solveExplicit()
//
//
// Author:  <Gauthier BECKER>, (C) 2010
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef _CRITICALTIMESTEPELEMENTS_H_
#define _CRITICALTIMESTEPELEMENTS_H_


/*
inline double distanceMin(const double x1, const double x2, const double x3,
                    const double y1, const double y2, const double y3,
                    const double z1, const double z2, const double z3){
  double dist1 = sqrt((x1-x2)*(x1-x2)+(y1-y2)*(y1-y2)+(z1-z2)*(z1-z2));
  double dist2 = sqrt((x2-x3)*(x2-x3)+(y2-y3)*(y2-y3)+(z2-z3)*(z2-z3));
  if(dist1 > dist2)
    dist1 = sqrt((x3-x1)*(x3-x1)+(y3-y1)*(y3-y1)+(z3-z1)*(z3-z1));
  else
    dist2 = sqrt((x3-x1)*(x3-x1)+(y3-y1)*(y3-y1)+(z3-z1)*(z3-z1));
  if(dist1 > dist2)
    return dist2;
  else
    return dist1;
}
*/
// Define inner radius for high order element ...
inline double triangular2orderCharacteristicSize(MElement *ele, std::map<int,MElement*> mefo){
  MElement *elefo = mefo[ele->getType()];
  static std::vector<double> dist(4);
  elefo->setVertex(0,ele->getVertex(0)); elefo->setVertex(1,ele->getVertex(3)); elefo->setVertex(2,ele->getVertex(5));
  dist[0] = elefo->getInnerRadius();
  elefo->setVertex(0,ele->getVertex(3)); elefo->setVertex(1,ele->getVertex(4)); elefo->setVertex(2,ele->getVertex(5));
  dist[1] = elefo->getInnerRadius();
  elefo->setVertex(0,ele->getVertex(3)); elefo->setVertex(1,ele->getVertex(1)); elefo->setVertex(2,ele->getVertex(4));
  dist[2] = elefo->getInnerRadius();
  elefo->setVertex(0,ele->getVertex(5)); elefo->setVertex(1,ele->getVertex(4)); elefo->setVertex(2,ele->getVertex(2));
  dist[3] = elefo->getInnerRadius();
  return (*std::min_element(dist.begin(),dist.end()));
}
inline double triangular3orderCharacteristicSize(MElement *ele, std::map<int,MElement*> mefo){
  MElement *elefo = mefo[ele->getType()];
  static std::vector<double> dist(9);
  elefo->setVertex(0,ele->getVertex(0)); elefo->setVertex(1,ele->getVertex(3)); elefo->setVertex(2,ele->getVertex(8));
  dist[0] = elefo->getInnerRadius();
  elefo->setVertex(0,ele->getVertex(8)); elefo->setVertex(1,ele->getVertex(3)); elefo->setVertex(2,ele->getVertex(9));
  dist[1] = elefo->getInnerRadius();
  elefo->setVertex(0,ele->getVertex(3)); elefo->setVertex(1,ele->getVertex(4)); elefo->setVertex(2,ele->getVertex(9));
  dist[2] = elefo->getInnerRadius();
  elefo->setVertex(0,ele->getVertex(4)); elefo->setVertex(1,ele->getVertex(5)); elefo->setVertex(2,ele->getVertex(9));
  dist[3] = elefo->getInnerRadius();
  elefo->setVertex(0,ele->getVertex(4)); elefo->setVertex(1,ele->getVertex(5)); elefo->setVertex(2,ele->getVertex(1));
  dist[4] = elefo->getInnerRadius();
  elefo->setVertex(0,ele->getVertex(9)); elefo->setVertex(1,ele->getVertex(5)); elefo->setVertex(2,ele->getVertex(6));
  dist[5] = elefo->getInnerRadius();
  elefo->setVertex(0,ele->getVertex(7)); elefo->setVertex(1,ele->getVertex(9)); elefo->setVertex(2,ele->getVertex(6));
  dist[6] = elefo->getInnerRadius();
  elefo->setVertex(0,ele->getVertex(7)); elefo->setVertex(1,ele->getVertex(8)); elefo->setVertex(2,ele->getVertex(9));
  dist[7] = elefo->getInnerRadius();
  elefo->setVertex(0,ele->getVertex(7)); elefo->setVertex(1,ele->getVertex(6)); elefo->setVertex(2,ele->getVertex(2));
  dist[8] = elefo->getInnerRadius();
  return (*std::min_element(dist.begin(),dist.end()));
}

inline double quadrangle2orderCharacteristicSize(MElement *ele, std::map<int,MElement*> mefo){
  MElement *elefo = mefo[ele->getType()];
  static std::vector<double> dist(4);
  elefo->setVertex(0,ele->getVertex(0)); elefo->setVertex(1,ele->getVertex(4)); elefo->setVertex(2,ele->getVertex(8)); elefo->setVertex(3,ele->getVertex(7));
  dist[0] = elefo->getInnerRadius();
  elefo->setVertex(0,ele->getVertex(4)); elefo->setVertex(1,ele->getVertex(1)); elefo->setVertex(2,ele->getVertex(5)); elefo->setVertex(3,ele->getVertex(8));
  dist[1] = elefo->getInnerRadius();
  elefo->setVertex(0,ele->getVertex(8)); elefo->setVertex(1,ele->getVertex(5)); elefo->setVertex(2,ele->getVertex(2)); elefo->setVertex(3,ele->getVertex(6));
  dist[2] = elefo->getInnerRadius();
  elefo->setVertex(0,ele->getVertex(7)); elefo->setVertex(1,ele->getVertex(8)); elefo->setVertex(2,ele->getVertex(6)); elefo->setVertex(3,ele->getVertex(3));
  dist[3] = elefo->getInnerRadius();
  return (*std::min_element(dist.begin(),dist.end()));
}

inline double quadrangle3orderCharacteristicSize(MElement *ele, std::map<int,MElement*> mefo){
  MElement *elefo = mefo[ele->getType()];
  static std::vector<double> dist(9);
  elefo->setVertex(0,ele->getVertex(0)); elefo->setVertex(1,ele->getVertex(4)); elefo->setVertex(2,ele->getVertex(12)); elefo->setVertex(3,ele->getVertex(11));
  dist[0] = elefo->getInnerRadius();
  elefo->setVertex(0,ele->getVertex(4)); elefo->setVertex(1,ele->getVertex(5)); elefo->setVertex(2,ele->getVertex(13)); elefo->setVertex(3,ele->getVertex(12));
  dist[1] = elefo->getInnerRadius();
  elefo->setVertex(0,ele->getVertex(5)); elefo->setVertex(1,ele->getVertex(1)); elefo->setVertex(2,ele->getVertex(6)); elefo->setVertex(3,ele->getVertex(13));
  dist[2] = elefo->getInnerRadius();
  elefo->setVertex(0,ele->getVertex(13)); elefo->setVertex(1,ele->getVertex(6)); elefo->setVertex(2,ele->getVertex(7)); elefo->setVertex(3,ele->getVertex(14));
  dist[3] = elefo->getInnerRadius();
  elefo->setVertex(0,ele->getVertex(14)); elefo->setVertex(1,ele->getVertex(7)); elefo->setVertex(2,ele->getVertex(2)); elefo->setVertex(3,ele->getVertex(8));
  dist[4] = elefo->getInnerRadius();
  elefo->setVertex(0,ele->getVertex(15)); elefo->setVertex(1,ele->getVertex(14)); elefo->setVertex(2,ele->getVertex(8)); elefo->setVertex(3,ele->getVertex(9));
  dist[5] = elefo->getInnerRadius();
  elefo->setVertex(0,ele->getVertex(10)); elefo->setVertex(1,ele->getVertex(15)); elefo->setVertex(2,ele->getVertex(9)); elefo->setVertex(3,ele->getVertex(3));
  dist[6] = elefo->getInnerRadius();
  elefo->setVertex(0,ele->getVertex(11)); elefo->setVertex(1,ele->getVertex(12)); elefo->setVertex(2,ele->getVertex(15)); elefo->setVertex(3,ele->getVertex(10));
  dist[7] = elefo->getInnerRadius();
  elefo->setVertex(0,ele->getVertex(12)); elefo->setVertex(1,ele->getVertex(13)); elefo->setVertex(2,ele->getVertex(14)); elefo->setVertex(3,ele->getVertex(15));
  dist[8] = elefo->getInnerRadius();
  return (*std::min_element(dist.begin(),dist.end()));
}


inline double det3(const SVector3 &p1, const SVector3 &p2, const SVector3 &p3)
{
    double det;
    det = p1[0]*(p2[1]*p3[2]-p3[1]*p2[2]) -
          p1[1]*(p2[0]*p3[2]-p3[0]*p2[2]) +
          p1[2]*(p2[0]*p3[1]-p3[0]*p2[1]);
    return det;
}
inline double inradiusTetrahedron(const SVector3 &p1, const SVector3 &p2, const SVector3 &p3, const SVector3 &p4)
{
  double inradius;
  double l12,l13,l14,l23,l24,l34,S1,S2,S3,S4,volume;
  //  double r,s,t;
  l12 = (p1-p2).norm();
  l13 = (p1-p3).norm();
  l14 = (p1-p4).norm();
  l23 = (p3-p2).norm();
  l24 = (p4-p2).norm();
  l34 = (p4-p3).norm();

  S1 = (l12+l23+l13)/2;
  S1 = sqrt(S1*(S1-l12)*(S1-l23)*(S1-l13));

  S2 = (l12+l24+l14)/2;
  S2 = sqrt(S2*(S2-l12)*(S2-l24)*(S2-l14));

  S3 = (l23+l34+l24)/2;
  S3 = sqrt(S3*(S3-l23)*(S3-l34)*(S3-l24));

  S4 = (l13+l34+l14)/2;
  S4 = sqrt(S4*(S4-l13)*(S4-l34)*(S4-l14));

  volume = det3(p2,p3,p4);
  volume -= det3(p1,p3,p4);
  volume += det3(p1,p2,p4);
  volume -= det3(p1,p2,p3);
  volume /= 6;

  inradius = 3 * volume / (S1+S2+S3+S4);

  return inradius;
}

inline double tetrahedronCharacteristicSize(MElement *ele, const std::vector<double>& disp){
  SVector3 p1,p2,p3,p4;
  p1(0) = ele->getVertex(0)->x() + disp[0];
  p2(0) = ele->getVertex(1)->x() + disp[1];
  p3(0) = ele->getVertex(2)->x() + disp[2];
  p4(0) = ele->getVertex(3)->x() + disp[3];
  p1(1) = ele->getVertex(0)->y() + disp[4];
  p2(1) = ele->getVertex(1)->y() + disp[5];
  p3(1) = ele->getVertex(2)->y() + disp[6];
  p4(1) = ele->getVertex(3)->y() + disp[7];
  p1(2) = ele->getVertex(0)->z() + disp[8];
  p2(2) = ele->getVertex(1)->z() + disp[9];
  p3(2) = ele->getVertex(2)->z() + disp[10];
  p4(2) = ele->getVertex(3)->z() + disp[11];
  return inradiusTetrahedron(p1,p2,p3,p4);
}
inline double tetrahedron2orderCharacteristicSize(MElement *ele, const std::vector<double>& disp){
  SVector3 p1,p2,p3,p4,p5,p6,p7,p8,p9,p10;
  p1(0) = ele->getVertex(0)->x() + disp[0];
  p2(0) = ele->getVertex(1)->x() + disp[1];
  p3(0) = ele->getVertex(2)->x() + disp[2];
  p4(0) = ele->getVertex(3)->x() + disp[3];
  p5(0) = ele->getVertex(4)->x() + disp[4];
  p6(0) = ele->getVertex(5)->x() + disp[5];
  p7(0) = ele->getVertex(6)->x() + disp[6];
  p8(0) = ele->getVertex(7)->x() + disp[7];
  p9(0) = ele->getVertex(8)->x() + disp[8];
  p10(0) = ele->getVertex(9)->x() + disp[9];
  p1(1) = ele->getVertex(0)->y() + disp[10];
  p2(1) = ele->getVertex(1)->y() + disp[11];
  p3(1) = ele->getVertex(2)->y() + disp[12];
  p4(1) = ele->getVertex(3)->y() + disp[13];
  p5(1) = ele->getVertex(4)->y() + disp[14];
  p6(1) = ele->getVertex(5)->y() + disp[15];
  p7(1) = ele->getVertex(6)->y() + disp[16];
  p8(1) = ele->getVertex(7)->y() + disp[17];
  p9(1) = ele->getVertex(8)->y() + disp[18];
  p10(1) = ele->getVertex(9)->y() + disp[19];
  p1(2) = ele->getVertex(0)->z() + disp[20];
  p2(2) = ele->getVertex(1)->z() + disp[21];
  p3(2) = ele->getVertex(2)->z() + disp[22];
  p4(2) = ele->getVertex(3)->z() + disp[23];
  p5(2) = ele->getVertex(4)->z() + disp[24];
  p6(2) = ele->getVertex(5)->z() + disp[25];
  p7(2) = ele->getVertex(6)->z() + disp[26];
  p8(2) = ele->getVertex(7)->z() + disp[27];
  p9(2) = ele->getVertex(8)->z() + disp[28];
  p10(2) = ele->getVertex(9)->z() + disp[29];


  double r1=inradiusTetrahedron(p1, p5, p7, p8);
  double r2=inradiusTetrahedron(p2, p6, p5, p10);
  double r3=inradiusTetrahedron(p3, p7, p6, p9);
  double r4=inradiusTetrahedron(p8, p9, p4, p10);

  if (r1 < r2 and r1 < r3 and r1 < r4)
    return r1;
  else if (r2 < r1 and r2 < r3 and r2 < r4)
    return r2;
  else if (r3 < r1 and r3 < r2 and r3 < r4)
    return r3;
  else
    return r4;


}
inline double tetrahedron3orderCharacteristicSize(MElement *ele,const std::vector<double>& disp){
  SVector3 pt[20];
  for (int i=0; i<20; i++)
  {
    pt[i](0) = ele->getVertex(i)->x() + disp[i];
    pt[i](1) = ele->getVertex(i)->y() + disp[i+20];
    pt[i](2) = ele->getVertex(i)->z() + disp[i+40];
    //Msg::Info("Pt %d:%f %f %f",i,pt[i](0),pt[i](1),pt[i](2));
  }
  double r[20];
  r[0]=inradiusTetrahedron(pt[0], pt[4], pt[9], pt[11]);
  r[1]=inradiusTetrahedron(pt[5], pt[1], pt[6], pt[15]);
  r[2]=inradiusTetrahedron(pt[8], pt[7], pt[2], pt[13]);
  r[3]=inradiusTetrahedron(pt[10], pt[14], pt[12], pt[3]);

  r[4]=inradiusTetrahedron(pt[4], pt[5], pt[16], pt[17]);
  r[5]=inradiusTetrahedron(pt[10], pt[11], pt[18], pt[17]);
  r[6]=inradiusTetrahedron(pt[14], pt[15], pt[17], pt[19]);
  r[7]=inradiusTetrahedron(pt[7], pt[6], pt[13], pt[16]);
  r[8]=inradiusTetrahedron(pt[8], pt[9], pt[16], pt[13]);
  r[9]=inradiusTetrahedron(pt[14], pt[15], pt[17], pt[12]);
  r[10]=inradiusTetrahedron(pt[10], pt[11], pt[12], pt[17]);
  r[11]=inradiusTetrahedron(pt[10], pt[11], pt[18], pt[4]);
  r[12]=inradiusTetrahedron(pt[8], pt[9], pt[4], pt[18]);
  r[13]=inradiusTetrahedron(pt[7], pt[6], pt[19], pt[5]);
  r[14]=inradiusTetrahedron(pt[14], pt[15], pt[5], pt[19]);

  r[15]=inradiusTetrahedron(pt[6], pt[7], pt[16], pt[19]);
  r[16]=inradiusTetrahedron(pt[8], pt[9], pt[16], pt[18]);
  r[17]=inradiusTetrahedron(pt[10], pt[11], pt[18], pt[17]);
  r[18]=inradiusTetrahedron(pt[14], pt[15], pt[17], pt[19]);

  r[19]=inradiusTetrahedron(pt[16], pt[17], pt[19], pt[18]);

  double rad=r[0];
  //Msg::Info("Radius %d:%f",0,r[0]);
  for(int i=1;i<20;i++)
  {
     //Msg::Info("Radius %d:%f",i,r[i]);
     if(r[i]<0.) Msg::Error("negative radius %d",i);
     if(r[i]<rad) rad=r[i];
  }
  return rad;


}
/*
inline double tetrahedron2orderCharacteristicSize(MElement *ele, std::map<int,MElement*> mefo)
{
  MElement *elefo = mefo[ele->getType()];
  static std::vector<double> dist(4);
  elefo->setVertex(0,ele->getVertex(0)); elefo->setVertex(1,ele->getVertex(4)); elefo->setVertex(2,ele->getVertex(6)); elefo->setVertex(3,ele->getVertex(7));
  dist[0] = elefo->getInnerRadius();
  elefo->setVertex(0,ele->getVertex(1)); elefo->setVertex(1,ele->getVertex(5)); elefo->setVertex(2,ele->getVertex(4)); elefo->setVertex(3,ele->getVertex(9));
  dist[1] = elefo->getInnerRadius();
  elefo->setVertex(0,ele->getVertex(2)); elefo->setVertex(1,ele->getVertex(6)); elefo->setVertex(2,ele->getVertex(5)); elefo->setVertex(3,ele->getVertex(8));
  dist[2] = elefo->getInnerRadius();
  elefo->setVertex(0,ele->getVertex(7)); elefo->setVertex(1,ele->getVertex(8)); elefo->setVertex(2,ele->getVertex(3)); elefo->setVertex(3,ele->getVertex(9));
  dist[3] = elefo->getInnerRadius();
  return (*std::min_element(dist.begin(),dist.end()));
}
*/
inline double hexahedronCharacteristicSize(MElement *ele, const std::vector<double>& disp){
  SVector3 p1,p2,p3,p4,p5,p6,p7,p8;
  p1(0) = ele->getVertex(0)->x() + disp[0];
  p2(0) = ele->getVertex(1)->x() + disp[1];
  p3(0) = ele->getVertex(2)->x() + disp[2];
  p4(0) = ele->getVertex(3)->x() + disp[3];
  p5(0) = ele->getVertex(4)->x() + disp[4];
  p6(0) = ele->getVertex(5)->x() + disp[5];
  p7(0) = ele->getVertex(6)->x() + disp[6];
  p8(0) = ele->getVertex(7)->x() + disp[7];
  p1(1) = ele->getVertex(0)->y() + disp[8];
  p2(1) = ele->getVertex(1)->y() + disp[9];
  p3(1) = ele->getVertex(2)->y() + disp[10];
  p4(1) = ele->getVertex(3)->y() + disp[11];
  p5(1) = ele->getVertex(4)->y() + disp[12];
  p6(1) = ele->getVertex(5)->y() + disp[13];
  p7(1) = ele->getVertex(6)->y() + disp[14];
  p8(1) = ele->getVertex(7)->y() + disp[15];
  p1(2) = ele->getVertex(0)->z() + disp[16];
  p2(2) = ele->getVertex(1)->z() + disp[17];
  p3(2) = ele->getVertex(2)->z() + disp[18];
  p4(2) = ele->getVertex(3)->z() + disp[19];
  p5(2) = ele->getVertex(4)->z() + disp[20];
  p6(2) = ele->getVertex(5)->z() + disp[21];
  p7(2) = ele->getVertex(6)->z() + disp[22];
  p8(2) = ele->getVertex(7)->z() + disp[23];

  double r1=inradiusTetrahedron(p1,p2,p4,p5);
  double r2=inradiusTetrahedron(p2,p4,p5,p6);
  double r3=inradiusTetrahedron(p5,p6,p4,p8);
  double r4=inradiusTetrahedron(p2,p4,p6,p3);
  double r5=inradiusTetrahedron(p4,p8,p6,p3);
  double r6=inradiusTetrahedron(p6,p8,p7,p3);

  if (r1 < r2 and r1 < r3 and r1 < r4 and r1 < r5 and r1 < r6)
    return r1;
  else if (r2 < r1 and r2 < r3 and r2 < r4 and r2 < r5 and r2 < r6)
    return r2;
  else if (r3 < r1 and r3 < r2 and r3 < r4 and r3 < r5 and r3 < r6)
    return r3;
  else if (r4 < r1 and r4 < r2 and r4 < r3 and r4 < r5 and r4 < r6)
    return r4;
  else if (r5 < r1 and r5 < r2 and r5 < r3 and r5 < r4 and r5 < r6)
    return r5;
  else
    return r6;


}
inline double hexahedron2orderCharacteristicSize(MElement *ele, const std::vector<double>& disp){
  SVector3 p1,p2,p3,p4,p5,p6,p7,p8;
  p1(0) = ele->getVertex(0)->x() + disp[0];
  p2(0) = ele->getVertex(1)->x() + disp[1];
  p3(0) = ele->getVertex(2)->x() + disp[2];
  p4(0) = ele->getVertex(3)->x() + disp[3];
  p5(0) = ele->getVertex(4)->x() + disp[4];
  p6(0) = ele->getVertex(5)->x() + disp[5];
  p7(0) = ele->getVertex(6)->x() + disp[6];
  p8(0) = ele->getVertex(7)->x() + disp[7];
  p1(1) = ele->getVertex(0)->y() + disp[8];
  p2(1) = ele->getVertex(1)->y() + disp[27];
  p3(1) = ele->getVertex(2)->y() + disp[28];
  p4(1) = ele->getVertex(3)->y() + disp[29];
  p5(1) = ele->getVertex(4)->y() + disp[30];
  p6(1) = ele->getVertex(5)->y() + disp[31];
  p7(1) = ele->getVertex(6)->y() + disp[32];
  p8(1) = ele->getVertex(7)->y() + disp[33];
  p1(2) = ele->getVertex(0)->z() + disp[54];
  p2(2) = ele->getVertex(1)->z() + disp[55];
  p3(2) = ele->getVertex(2)->z() + disp[56];
  p4(2) = ele->getVertex(3)->z() + disp[57];
  p5(2) = ele->getVertex(4)->z() + disp[58];
  p6(2) = ele->getVertex(5)->z() + disp[59];
  p7(2) = ele->getVertex(6)->z() + disp[60];
  p8(2) = ele->getVertex(7)->z() + disp[61];

  double r1=inradiusTetrahedron(p1,p2,p4,p5)/2.;
  double r2=inradiusTetrahedron(p2,p4,p5,p6)/2.;
  double r3=inradiusTetrahedron(p5,p6,p4,p8)/2.;
  double r4=inradiusTetrahedron(p2,p4,p6,p3)/2.;
  double r5=inradiusTetrahedron(p4,p8,p6,p3)/2.;
  double r6=inradiusTetrahedron(p6,p8,p7,p3)/2.;

  if (r1 < r2 and r1 < r3 and r1 < r4 and r1 < r5 and r1 < r6)
    return r1;
  else if (r2 < r1 and r2 < r3 and r2 < r4 and r2 < r5 and r2 < r6)
    return r2;
  else if (r3 < r1 and r3 < r2 and r3 < r4 and r3 < r5 and r3 < r6)
    return r3;
  else if (r4 < r1 and r4 < r2 and r4 < r3 and r4 < r5 and r4 < r6)
    return r4;
  else if (r5 < r1 and r5 < r2 and r5 < r3 and r5 < r4 and r5 < r6)
    return r5;
  else
    return r6;


}
/*
inline double hexahedron2orderCharacteristicSize(MElement *ele, std::map<int,MElement*> mefo)
{
  MElement *elefo = mefo[TYPE_TET]; // as compute by dividing in Tetrahedra
  static std::vector<double> dist(6);
  elefo->setVertex(0,ele->getVertex(0)); elefo->setVertex(1,ele->getVertex(1)); elefo->setVertex(2,ele->getVertex(3)); elefo->setVertex(3,ele->getVertex(4));
  dist[0] = elefo->getInnerRadius();
  elefo->setVertex(0,ele->getVertex(1)); elefo->setVertex(1,ele->getVertex(3)); elefo->setVertex(2,ele->getVertex(4)); elefo->setVertex(3,ele->getVertex(5));
  dist[1] = elefo->getInnerRadius();
  elefo->setVertex(0,ele->getVertex(4)); elefo->setVertex(1,ele->getVertex(5)); elefo->setVertex(2,ele->getVertex(3)); elefo->setVertex(3,ele->getVertex(7));
  dist[2] = elefo->getInnerRadius();
  elefo->setVertex(0,ele->getVertex(1)); elefo->setVertex(1,ele->getVertex(3)); elefo->setVertex(2,ele->getVertex(5)); elefo->setVertex(3,ele->getVertex(2));
  dist[3] = elefo->getInnerRadius();
  elefo->setVertex(0,ele->getVertex(3)); elefo->setVertex(1,ele->getVertex(7)); elefo->setVertex(2,ele->getVertex(5)); elefo->setVertex(3,ele->getVertex(2));
  dist[4] = elefo->getInnerRadius();
  elefo->setVertex(0,ele->getVertex(5)); elefo->setVertex(1,ele->getVertex(7)); elefo->setVertex(2,ele->getVertex(6)); elefo->setVertex(3,ele->getVertex(2));
  dist[5] = elefo->getInnerRadius();
  return 0.5*(*std::min_element(dist.begin(),dist.end())); // 0.5 as it is defined before like that !
}
*/

#endif //_CRITICALTIMESTEPELEMENTS_H_