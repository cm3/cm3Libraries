//
//
// Description: Non linear solver for mechanic problems
//              quasi-static implicit scheme & dynamic explicit Hulbert-Chung scheme
//
// Author:  <Gauthier BECKER>, (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef _NONLINEARMECHSOLVER_H_
#define _NONLINEARMECHSOLVER_H_

#ifndef SWIG
#include <map>
#include <string>
#include "SVector3.h"
#include "simpleFunction.h"
#include "simpleFunctionTime.h"
#include "functionSpace.h"
#include "MInterfaceElement.h"
#include "elementGroup.h"
#include "contactDomain.h"
#include "NonLinearSolverConfig.h"
#include "eigenSolver.h"
#include "nonLinearMicroBC.h"
#include "ipField.h"
#include "unknownField.h"
#include "strainMapping.h"
#include "homogenizedData.h"
#include "ElementErosionFilter.h"
#include "failureCriterion.h"
#include "ipvariable.h"
#include "endSchemeMonitoring.h"
#include "selectiveUpdate.h"
#include "defoDefoContactDomain.h"
#include "partDomain.h"
#include "JIntegralByDomainIntegration.h"
#include "elementFilterBox.h"
#include "GPFilter.h"
class elementGroup;
class energeticField;
class restartManager;
class nlsDofManager;
class DofCollection;
class pbcAlgorithm;
class stiffnessCondensation;
class pbcConstraintElementGroup;
class discreteElement;
class FilterDof;
class archiveForce
{
 public:
  int numphys;
  int dim;
  int comp;
  int nstep;
  FILE *FP;
  int lastSaveStep;
  std::set<Dof> vdof; // With a set we are sur that a Dof will not be archived twice
  int contact_root; // rank where the archive is done for contact == -1 if not contact
  double fval; // tempory to get value from system
  archiveForce() : numphys(0), dim(0), comp(0), nstep(1), fval(0.), FP(NULL), contact_root(-1),lastSaveStep(-1){}
  archiveForce(const archiveForce &source) : numphys(source.numphys), dim(source.dim),
                                             comp(source.comp), nstep(source.nstep),
                                             fval(source.fval),
                                             FP(source.FP),
                                             contact_root(source.contact_root),
                                             lastSaveStep(source.lastSaveStep)
  {
    for(std::set<Dof>::const_iterator itD = source.vdof.begin(); itD!= source.vdof.end(); ++itD)
    {
      vdof.insert(*itD);
    }
  }
  void openFile(const std::string prefix)
  {
    // create file only if there is dof to archive
    if(vdof.size()!=0){
      std::ostringstream oss;
      oss << numphys;
      std::string s = oss.str();
      oss.str("");
      oss << comp;
      std::string s2 = oss.str();
     #if defined(HAVE_MPI)
      if(Msg::GetCommSize() != 1){
        oss.str("");
        oss << Msg::GetCommRank();
        s2 += "_part"+oss.str();
      }
     #endif // HAVE_MPI
      std::string fname =prefix+"force"+s+"comp"+s2+".csv";
      FP = fopen(fname.c_str(),"w");
    }
    #if defined(HAVE_MPI)
    else if(Msg::GetCommSize() == 1)
    {
      Msg::Warning("Can't archive forces on physical %d because it contains no dofs!",numphys);
    }
    #endif
  }

  archiveForce(int np, int d, int c,int ns) : numphys(np), dim(d), comp(c), nstep(ns), fval(0.), FP(NULL), contact_root(-1),
                          lastSaveStep(-1){ if (nstep <1) nstep=1;}
  ~archiveForce(){
    if(FP!=NULL) fclose(FP);
    FP=NULL;
  }
  void clear()
  {
    lastSaveStep = -1;
    if (FP!=NULL) 
    {
      fclose(FP); FP=NULL;
    }
    vdof.clear();
    contact_root = -1;
  }
  void resetArchiving(const std::string prefix)
  {
    lastSaveStep = -1;
    if(FP!=NULL) fclose(FP);
    openFile(prefix); 
  }
};

class FilterDofMultiple : public FilterDof
{
  public:
    enum OptType{OR=0, AND=1};
  protected:
    std::vector<FilterDof*> _allFilters;
    OptType _type;
  public:
    FilterDofMultiple(int type, std::vector<FilterDof*>& allFilters);
    virtual ~FilterDofMultiple();
    virtual bool operator()(Dof key);
};


class IPDataOnPhysical {
  public:
    std::vector<int> element; // ele and GP
    int physical; // physical
    int dim;
    int ipValue; // ipvalue IPField::Output
    int ipIndex; // -1 if all IP is save, ipIndex otherwise
    int lastSaveStep;
    int nbArch;
    bool isInitialized;
    bool oneFileForAllStep;
    IPDataOnPhysical(const int ipval, const int phy, const int d, const int nstep, const bool oneFile, int ipI): ipIndex(ipI), ipValue(ipval),physical(phy),dim(d),
                    nbArch(nstep),lastSaveStep(-1),isInitialized(false),oneFileForAllStep(oneFile){
    };
    ~IPDataOnPhysical(){
    };
    void resetArchiving()
    {
      lastSaveStep = -1;
    }
};

class IPValueTracking 
{
  public:
    std::vector<int> ipValue; // ipvalue IPField::Output
    GPFilter* gpFilter;
    elementFilterLocation* boxFilter;
    int nbArch;
    int lastSaveStep;
    IPValueTracking(const std::vector<int>& ipval, const int nstep, const GPFilter* gpF,  const elementFilterLocation* filterbox):
      ipValue(ipval), nbArch(nstep), lastSaveStep(-1)
      {
        if (gpF == NULL)
        {
          gpFilter = new GPFilterTrivial();
        }
        else
        {
          gpFilter = gpF->clone();
        }
        if (filterbox == NULL)
        {
          boxFilter = new elementFilterLocationTrivial();
        }
        else
        {
          boxFilter = filterbox->clone();
        }
      }
    ~IPValueTracking()
    {
      if (gpFilter!= NULL) delete gpFilter;
      gpFilter = NULL;
      if (boxFilter!= NULL) delete boxFilter;
      boxFilter = NULL;
    }
    void resetArchiving()
    {
      lastSaveStep = -1;
    }
};

class clustersData
{
  public: 
    std::string clusterDataFileName;
    std::map<int,int> materialMap; // map ip with material num
    std::map<int,int> clusterIndexMap; // map of clusterIndex and weight at gauss points
    std::map<int,double> weightMap; //map of ele+gpNum and weight*detJ at gauss points
    std::map<int, std::map<int,double> > clusterMap; // cluster map; clusterIndex, 
    bool isInitialized;
    clustersData():isInitialized(false),clusterDataFileName(""){}
    ~clustersData(){}
    void loadFromFile(const std::string fileName);
    int realClusterSize() const;
};

class AvergageCluster
{
  public:
    const clustersData* clusterData;
    int ipVal;
    int lastSaveStep;
    int nbArch;
    FILE* fp;
    AvergageCluster( const int ip, const int nstep): 
              clusterData(NULL),ipVal(ip),nbArch(nstep),lastSaveStep(-1),fp(NULL){};
    ~AvergageCluster(){if (fp!=NULL) fclose(fp);};
    std::string getString() const
    {
      if (clusterData == NULL)
      {
        Msg::Error("clusterData is not correctly initialized");
        Msg::Exit(0);
      }
      return "AverageCluster_nbClusters_"+int2str(clusterData->realClusterSize());
    }
    void openFile(const std::string prefix)
    {
      if (clusterData == NULL)
      {
        Msg::Error("clusterData is not correctly initialized");
        Msg::Exit(0);
      }
      std::string fname = prefix+getString()+"_"+IPField::ToString(ipVal);
		  #if defined(HAVE_MPI)
		  if (Msg::GetCommSize() > 1){
		    fname += "_part"+int2str(Msg::GetCommRank());
		  }
		  #endif //HAVE_MPI
		  fname += ".csv";
      fp =  fopen(fname.c_str(),"w");
    }
    void resetArchiving(const std::string prefix)
    {
      lastSaveStep = -1;
      if (fp!=NULL) fclose(fp);
      openFile(prefix);
    }
    void clear()
    {
      lastSaveStep = -1;
      if (fp!=NULL) fclose(fp);
      fp = NULL;
    }
    void getAverageValue(const IPField* ipf, std::vector<double>& val) const;
};

class IntegralVolume
{
  public:
    typedef enum {VolumeIntegral=1, VolumeAverage=2, ActiveDissipationAverage=3, ActiveDissipationIncrementAverage=4, IncrementVolumeIntegral=5, IncrementVolumeAverage=6} IntegralVolumeType;
    IntegralVolumeType integType;
    int ipVal;
    int physical; // -1 if operation is performed in all domain, >0, operation is performed in this physical
    int lastSaveStep;
    int nbArch;
    double value; //the value 
    GPFilter* gpFilter;
    FILE* fp;
    IntegralVolume(const int type, const int ip, const int p, const int nstep, const GPFilter* ft): 
          integType(IntegralVolumeType(type)),ipVal(ip), physical(p), nbArch(nstep),lastSaveStep(-1), value(0.),
          fp(NULL)
          {
            if (ft==NULL)
            {
              gpFilter= new GPFilterTrivial();
            }
            else
            {
              gpFilter = ft->clone();
            }
          }
    ~IntegralVolume()
    {
      if (fp!=NULL) 
      {
        fclose(fp);
      }
      if (gpFilter!=NULL)
      {
        delete gpFilter;
        gpFilter = NULL;
      }
    };
    std::string getString() const
    {
      std::string physicalText("");
      if (physical > 0)
      {
        physicalText = "_"+int2str(physical);
      }
      if (integType == VolumeIntegral) return gpFilter->getName()+"VolumeIntegral"+physicalText;
      else if (integType == IncrementVolumeIntegral) return gpFilter->getName()+"IncrementVolumeIntegral"+physicalText;
      else if (integType == VolumeAverage) return gpFilter->getName()+"Average"+physicalText;
      else if (integType == IncrementVolumeAverage) return gpFilter->getName()+"IncrementAverage"+physicalText;
      else if (integType == ActiveDissipationAverage) return "ActiveDissipationAverage"+physicalText;
      else if (integType == ActiveDissipationIncrementAverage) return "ActiveDissipationIncrementBasedAverage"+physicalText;
      else
      {
        Msg::Error("integType %d has not been defined",integType);
      }
      return "";
    }
    void openFile(const std::string prefix)
    {
      std::string fname = prefix+getString()+"_"+IPField::ToString(ipVal);
		  #if defined(HAVE_MPI)
		  if (Msg::GetCommSize() > 1){
		    fname += "_part"+int2str(Msg::GetCommRank());
		  }
		  #endif //HAVE_MPI
		  fname += ".csv";
      fp =  fopen(fname.c_str(),"w");
    }
    void clear()
    {
      lastSaveStep = -1;
      if (fp!=NULL) fclose(fp);
      fp = NULL;
    }
    void resetArchiving(const std::string prefix)
    {
      lastSaveStep = -1;
      if (fp!=NULL) fclose(fp);
      openFile(prefix);
    }
};

class archiveInternalForce
{
  public:
    class elementDomain 
    {
      public:
        partDomain* dom;
        MElement* ele;
      elementDomain(partDomain* d, MElement* e): dom(d),ele(e){}
      ~elementDomain(){}
      bool operator<(const elementDomain &other) const
      {
        if(dom->getPhysical() < other.dom->getPhysical()) return true;
        if(dom->getPhysical() > other.dom->getPhysical()) return false;
        if(ele->getNum() < other.ele->getNum()) return true;
        return false;
      }
      bool operator==(const elementDomain &other) const
      {
        return (dom->getPhysical() == other.dom->getPhysical() && ele->getNum() == other.ele->getNum());
      }
    };
  
  
    int phys;
    int dim;
    int nstep;
    FILE *FP;
    FILE *FPTotal;
    int lastSaveStep;
    std::map<int,std::map<elementDomain,int> > sharedElements;
    
    archiveInternalForce(int p, int d, int ns) : phys(p), dim(d),  nstep(ns), FP(NULL), FPTotal(NULL),
                          lastSaveStep(-1){ if (nstep <1) nstep=1;}
    ~archiveInternalForce()
    {
      if(FP!=NULL)
        fclose(FP);
      FP=NULL;
      if (FPTotal!=NULL) 
        fclose(FPTotal); 
      FPTotal=NULL;
    }
    void openFile(const std::string prefix)
    {
      std::string s = "";
      if (dim == 0) s = "Node";
      else if (dim == 1) s = "Line";
      else if (dim == 2) s = "Face";
      else if (dim == 3) s = "Volume";
      std::ostringstream oss;
      oss << phys;
      s += oss.str();
      
      std::string s2 ="";
     #if defined(HAVE_MPI)
      if(Msg::GetCommSize() != 1){
        oss.str("");
        oss << Msg::GetCommRank();
        s2 += "_part"+oss.str();
      }
     #endif // HAVE_MPI
      std::string fname =prefix+"internalForceOnPhysical"+s+s2+".csv";
      FP = fopen(fname.c_str(),"w");
      
      fname =prefix+"totalInternalForceOnPhysical"+s+s2+".csv";
      FPTotal = fopen(fname.c_str(),"w");
    }
    void clear()
    {
      lastSaveStep = -1;
      if (FP!=NULL) 
      {
        fclose(FP); FP=NULL;
      }
      if (FPTotal!=NULL)
      {
        fclose(FPTotal); FPTotal=NULL;
      }
    }

    void resetArchiving(const std::string prefix)
    {
      lastSaveStep = -1;
      if(FP!=NULL) fclose(FP);
      if (FPTotal!=NULL) fclose(FPTotal);
      openFile(prefix); 
    }
};

class implicitSolverOptions
{
  public:
    double beta, gamma, alpham, alphaf;
    bool noMassPredictorWithoutVelocityAndAcceleration; // allows predicting nomass DOf differently with massDOf if implicicit CH is used

    implicitSolverOptions():beta(0.25), gamma(0.5), alpham(0.), alphaf(0.),noMassPredictorWithoutVelocityAndAcceleration(false){}
    implicitSolverOptions(const implicitSolverOptions& src):beta(src.beta), gamma(src.gamma), alpham(src.alpham), alphaf(src.alphaf),
      noMassPredictorWithoutVelocityAndAcceleration(src.noMassPredictorWithoutVelocityAndAcceleration){}
    implicitSolverOptions& operator=(const implicitSolverOptions& src)
    {
      beta = src.beta;
      gamma = src.gamma;
      alpham = src.alpham;
      alphaf = src.alphaf;
      noMassPredictorWithoutVelocityAndAcceleration = src.noMassPredictorWithoutVelocityAndAcceleration;
      return *this;
    };
    ~implicitSolverOptions(){};
};

class explicitSolverOptions
{
  public:
    double beta, gamma, alpham, gammas;
    bool timeStepByBenson, dynamicRelaxation;
    int numstepExpl; // interval to re-estimate timestep
    int numstepImpl; // interval for implicit computation in multisystem
    explicitSolverOptions(): beta(0.), gamma(0.5), alpham(0.), gammas(0.6666),
                            timeStepByBenson(false),dynamicRelaxation(false),numstepExpl(1),numstepImpl(0){}
    explicitSolverOptions(const explicitSolverOptions& src): beta(src.beta), gamma(src.gamma), alpham(src.alpham), gammas(src.gammas),
                            timeStepByBenson(src.timeStepByBenson),dynamicRelaxation(src.dynamicRelaxation),numstepExpl(src.numstepExpl),numstepImpl(src.numstepImpl){}
    explicitSolverOptions& operator=(const explicitSolverOptions& src)
    {
      beta = src.beta;
      gamma = src.gamma;
      alpham = src.alpham;
      gammas = src.gammas;
      timeStepByBenson = src.timeStepByBenson;
      dynamicRelaxation = src.dynamicRelaxation;
      numstepExpl = src.numstepExpl;
      numstepImpl = src.numstepImpl;
      return *this;
    };
    ~explicitSolverOptions(){};
};

class eigenSolverOptions
{
  public:
    // eigen solver type
    enum EIGENSOLVER_TYPE{Static = 0, Dynamic=1};
    // eigensolver using for stability analysis
    int numeigenvalue;

    EIGENSOLVER_TYPE type; // static - K only, and dynamic K and M
    bool MKToFile;
    int maxNumIteration; // maximal number of iteration
    std::string method; // method to estimate eigven value
    double convergenCriterion; // toleranece
    bool hermitian; // true if matrix is symmetrical or not
    //
    bool isPerturbedEigenMode; // true if use eigenvector to create perturbe structure
    int numberPerturbedMode; // number of eigenmode is used
    double valPerturbedMode; // per
    //
    eigenSolverOptions():
      numeigenvalue(1),
      type(Dynamic),
      MKToFile(false),
      maxNumIteration(20),
      method("krylovschur"),
      convergenCriterion(1e-6),
      hermitian(true),
      isPerturbedEigenMode(false),
      numberPerturbedMode(1),
      valPerturbedMode(1.)
      {};
    eigenSolverOptions(const eigenSolverOptions& src):
      numeigenvalue(src.numeigenvalue),
      type(src.type),
      MKToFile(src.MKToFile),
      maxNumIteration(src.maxNumIteration),
      method(src.method),
      convergenCriterion(src.convergenCriterion),
      hermitian(src.hermitian),
      isPerturbedEigenMode(src.isPerturbedEigenMode),
      numberPerturbedMode(src.numberPerturbedMode),
      valPerturbedMode(src.valPerturbedMode){};
    eigenSolverOptions& operator= (const eigenSolverOptions& src)
    {
      numeigenvalue = src.numeigenvalue;
      type = src.type;
      MKToFile = src.MKToFile;
      maxNumIteration = src.maxNumIteration;
      method = src.method;
      convergenCriterion = src.convergenCriterion;
      hermitian = src.hermitian;
      isPerturbedEigenMode = src.isPerturbedEigenMode;
      numberPerturbedMode = src.numberPerturbedMode;
      valPerturbedMode = src.valPerturbedMode;
      return *this;
    };
    ~eigenSolverOptions(){}
};

class pathFollowingManager;

#endif // SWIG



class TimeManager
{   
  protected:
    #ifndef SWIG
    //
    std::vector<double> _timeSeries;
    std::vector<int> _numStepSeries;
    
    double _startTime; // start time
    double _endTime; // end time by user 
    double _numStep; // numstep  by user  
    double _tol; // as double value is sued, the comparison must be repect some criterion
    //
    double _lastTime; // last sucessful time, start from _startTime, const during solving, and update with time step if convergence
    int _lastIterationIndex; // last iteration index, start from 0, as previous during solving and and increased by 1 if convergence
    //
    double _timeStep; // time step
    double _previousTimeStep; // previous step
    double _maxTimeStepReached; // maxtime step ever reached during simulation
    // bounds
    double _upperBoundTimeStep; // maximal time step
    double _lowerBoundTimeStep; // minimal time step
    
    // time step adaptation to obtain optimal number of iterations
    bool _timeStepAdaptation; // true if step is adapted with number NRs
    int _numNROptimal; // num optimal
    double _expNROptimal;
    ///
    int _maxNRite; // if this number of iteration is reached the time step is reduce
    double _timeStepFactorReduction; // the time step is divided by this value if no convergence (default 2)
    int _maxAttemptStepReduction; // number of attemp to reduce time step (default 6 which leads to a decrease by 64 (2^6) of time step)
    int _niteTimeStepIncrease; // if convergence with nite < _nite the time step is increase (default 5)(but can be greater than its max value given by numstep/endtime)
    int _numberTimeStepReduction; // to store number of solver fails
    bool _timeStepIncreaseBecauseOfSolverFailure;
    friend class pathFollowingManager;
    #endif //SWIG
  public:
    TimeManager(double tol=1e-12);
    void reset();
    void activateTimeStepAdaptation(bool adap, int numNROptimal, double exp, double maximalStep, double minTimeStep);
    void setManageTimeStepSafeguard(const int miteNR,const int iteIncreaseTS, const double redfactor, const int maxAttemptRedFactor);
    void setMaximalNumberOfFails(int maxnbFails);
    void clearNumStepSeries();
    void setNumStepTimeInterval(double t, int nstep);
    TimeManager(const TimeManager& src);
    virtual ~TimeManager(){}
    virtual bool willArchive(double curtime, int numstep) const;
    virtual bool reachEndTime() const; // check if endtime reaches
    // 
    virtual double getTimeStep() const;
    virtual void reduceTimeStep();
    virtual void resetNumberTimeStepReduction();
    virtual void setTimeStep(double dt);
    //
    virtual double getStartTime() const;
    virtual double getEndTime() const;
    virtual void setStartTime(double startTime);
    virtual void setEndTime(double endtime); // the simumation is longer
    virtual void setNumSteps(int st);
    virtual int getNumSteps() const;
    //
    virtual double getLastTime() const;
    virtual int getLastIterationIndex() const;
    virtual double getTimeStepFactorReduction() const {return _timeStepFactorReduction;};
   
    //
    virtual int getMaxNbIterations() const;
    virtual int getMaxNbFails() const;
    virtual int getNbFails() const;
        
    virtual bool withTimeStepAdaptation() const;
    
    virtual void computeTimeStepForNextSolving(int niteNR); // in function of current number of iterations of iterative scheme
    virtual void saveTimeHistory();
    virtual void initializeTimeSteppingPlan();
    virtual TimeManager* clone() const {return new TimeManager(*this);};
    virtual void restart();
};

class PointwiseTimeManager : public TimeManager
{
  protected:
    std::vector<double> _savePoints;
    double _tolSave;
  public:
    PointwiseTimeManager(double tol = 1e-12, double tolSave=1e-12);
    void put(double val);
    PointwiseTimeManager(const PointwiseTimeManager& src):TimeManager(src),_savePoints(src._savePoints),_tolSave(src._tolSave){}
    virtual ~PointwiseTimeManager(){}
    virtual bool willArchive(double curtime, int numstep) const;
    virtual TimeManager* clone() const {return new PointwiseTimeManager(*this);};
};

class StepwiseBeginTimeManager : public TimeManager
{
  protected:
    int _nstepArchBegin;
  public:
    StepwiseBeginTimeManager(double tol = 1e-12, int nstepArchBegin = 1);
    StepwiseBeginTimeManager(const StepwiseBeginTimeManager& src):TimeManager(src),_nstepArchBegin(src._nstepArchBegin){}
    virtual ~StepwiseBeginTimeManager(){}
    virtual bool willArchive(double curtime, int numstep) const;
    virtual TimeManager* clone() const {return new StepwiseBeginTimeManager(*this);};
};

class pathFollowingManager 
{
  public:
    enum pathFollowingMethod{GLOBAL_ARC_LENGTH_BASED=0, LOCAL_BASED=1, HYPERELLIPTIC_BASED=2};
    enum pathFollowingLocalIncrementType{DEFO_ENERGY=0, DISSIPATION_ENERGY=1,PLASTIC_ENERGY=2,DAMAGE_ENERGY=3};
    enum pathFollowingLocation{BULK_INTERFACE=0,BULK=1,INTERFACE=2};
    
  public:
    #ifndef SWIG
      //_pathFollowingMethod holds the method of path following, 0- GLOBAL_ARC_LENGTH_BASED , 1 - LOCAL_BASED, and 2- HYPERELLIPTIC_BASED
    //GLOBAL_ARC_LENGTH_BASED based on the general path following constraint type 
    //
    //LOCAL_BASED is a combination of LOAD CONTROL+ DISSIPATION CONTROL after the onset of dissipation
    //
    //HYPERELLIPTIC_BASED uses several particular DOFs insteads of all DOF used in GLOBAL_ARC_LENGTH_BASED to build the path following constraint
    // 
    pathFollowingMethod _pathFollowingMethod; 

    // if _pathFollowingMethod=GLOBAL_ARC_LENGTH_BASED or HYPERELLIPTIC_BASED is used, 
    int _controlTypePathFollowing; // control type
    int _correctionMethodPathFollowing; // correction method
    int _tranversalCriterionPathFollowing; // method to correctly estimate load paramater in the predictor of path following constrain as two solutions exists 
    int _solverTypePathFollowing; // solve method two use, 
    double _pathFollowingEqRatio; // equa ratio
    std::vector<int> _hyperellipticControlComp; // comp used in hyperelliptic control
    //
    // if _pathFollowingMethod = LOCAL_BASED
    bool _switchedControlType;
    double _pathFollowingSwitchCriterion;
    pathFollowingLocalIncrementType _pathFollowingIncrementType;
    pathFollowingLocation _pathFollowingLocation;
    //
    
    /*TIME STEP AND PATHFOLLOWING STEP MANAGEMENT*/
    // for path following increment (arc-length or local == dissipation increment)
    double _localStep; // to local cr control
    double _localStepPrev;
    double _localStepMinimal;
    double _localStepMaximal;
    
    double _arcLengthStep; // for arc-length control
    double _arcLengthStepPrev;
    double _arcLengthStepMinimal;
    double _arcLengthStepMaximal;
    
    // time step adaptation to obtain optimal number of iterations
    bool _pfStepAdaptation; // true if step is adapted with number NRs
    int _numNROptimal; // num optimal
    double _expNROptimal;
    int _numNROptimalLocal; // num optimalLocal
    #endif //SWIG
  
  public:
    pathFollowingManager();
    #ifndef SWIG
    pathFollowingManager(const pathFollowingManager &src);
    pathFollowingManager& operator = (const pathFollowingManager &src);
    virtual ~pathFollowingManager(){};
    
    void reducePathFollowingStep(const TimeManager* tm);
    
    double getCurrentPathFollowingIncrement() const;
    void computePathFollowingIncrementForNextSolving(const nonLinearMechSolver* solver, int niteNR, const TimeManager* tm); // in function of current number of iterations of iterative scheme
    void saveIncrementHistory(const nonLinearMechSolver* solver);
    void restart();
    #endif //SWIG
};

class NLS_MPI
{
  public:
    NLS_MPI();
    ~NLS_MPI(){};
    static int commRank, commSize;
    static void Init();
    static int GetCommSize();
    static int GetCommRank();
    static void Barrier();
    static void Finalize();
};

class StiffnessModificationMonitoring
{
 #ifndef SWIG
  public:
    StiffnessModificationMonitoring(){};
    StiffnessModificationMonitoring(const StiffnessModificationMonitoring& src){}
    virtual ~StiffnessModificationMonitoring(){}
    virtual bool willModify(int nstep, int iter, double time) const = 0;
    virtual bool quasiNewtonUpdate() const= 0;
    virtual bool uncoupledStiffness() const = 0;
    virtual StiffnessModificationMonitoring* clone() const =0;
  #endif //SWIG
};

class NewtonStiffnessModificationMonitoring: public StiffnessModificationMonitoring
{
  // always modify stiffness matrix
  public:
    NewtonStiffnessModificationMonitoring(): StiffnessModificationMonitoring(){};
    NewtonStiffnessModificationMonitoring(const NewtonStiffnessModificationMonitoring& src){}
    virtual ~NewtonStiffnessModificationMonitoring(){}
    virtual bool willModify(int nstep, int iter, double time) const {return true;};
    virtual bool quasiNewtonUpdate() const {return false;}
    virtual bool uncoupledStiffness() const {return false;}
    virtual StiffnessModificationMonitoring* clone() const {return new NewtonStiffnessModificationMonitoring(*this);};
};

class ModifiedNewtonStiffnessModificationMonitoring: public StiffnessModificationMonitoring
{
  // only modify the stiffness at the beginning of each ime step
  public:
    ModifiedNewtonStiffnessModificationMonitoring(): StiffnessModificationMonitoring(){};
    ModifiedNewtonStiffnessModificationMonitoring(const ModifiedNewtonStiffnessModificationMonitoring& src){}
    virtual ~ModifiedNewtonStiffnessModificationMonitoring(){}
    virtual bool willModify(int nstep, int iter, double time) const 
    {
      if (iter==0) return true;
      else return false;
    };
    virtual bool quasiNewtonUpdate() const {return false;}
    virtual bool uncoupledStiffness() const {return false;}
    virtual StiffnessModificationMonitoring* clone() const {return new ModifiedNewtonStiffnessModificationMonitoring(*this);};
};

class BFGSStiffnessModificationMonitoring : public StiffnessModificationMonitoring
{
  protected:
    int _numIterationsBetween; // stiffness matrix will be modifed after _numIterationsBetween iterations
    bool _uncoupled;
  public:
    BFGSStiffnessModificationMonitoring(int numIterBetween, bool uncoupled): StiffnessModificationMonitoring(), _numIterationsBetween(numIterBetween), _uncoupled(uncoupled){};
    BFGSStiffnessModificationMonitoring(const BFGSStiffnessModificationMonitoring& src):
                  StiffnessModificationMonitoring(src), _numIterationsBetween(src._numIterationsBetween), _uncoupled(src._uncoupled){};
    virtual ~BFGSStiffnessModificationMonitoring(){};
    virtual bool willModify(int nstep, int iter, double time) const
    {
      if (iter%_numIterationsBetween == 0)
      {
        Msg::Info("stiffness will be modified");
        return true;
      }
      return false;
    }
    virtual bool quasiNewtonUpdate() const {return true;};
    virtual bool uncoupledStiffness() const {return _uncoupled;}
    virtual StiffnessModificationMonitoring* clone() const {return new BFGSStiffnessModificationMonitoring(*this);};
};


class nonLinearMechSolver
{
 public:
  typedef std::set<contactDomain*> contactContainer;
  typedef std::set<defoDefoContactDomain*> defoDefoContactContainer;
  enum unknownType{
    displacement = nonLinearBoundaryCondition::position,
    velocity = nonLinearBoundaryCondition::velocity,
    acceleration = nonLinearBoundaryCondition::acceleration
  };
 #ifndef SWIG
  enum solver{ Gmm=0,Taucs=1,Petsc=2, GMRESk=3};
  enum scheme{StaticLinear=0, StaticNonLinear=1, Explicit=2, Multi=3, Implicit=4, Eigen=5};
  enum elementErosionType{FIRST_IP_FAILED=1, ALL_IP_FAILED=2};

  
  /**
   * 3 method for constraint treatment
   * DISP_ELIM = displacement elimination
   * MULT_ELIM = multiplier elimination
   * DISP_MULT = displacement + multiplier system, no elimination
   * DISP_ELIM_UNIFIED = displacement elimination but using pbcSystem
   */
  enum SYSTEM_TYPE{DISP_ELIM=0, MULT_ELIM = 1, DISP_MULT=2, DISP_ELIM_UNIFIED=3};
   /**
     * homogenized stress method by volume or surface integral
     */
  enum STRESS_HOMO_TYPE{VOLUME=0, SURFACE=1};
  /**
    * homogenized tangent by perturbation of by condensation
    */
  enum TANGENT_HOMO_TYPE{PERTURB=0, CONDEN=1, INSYSTEMCONDEN=2, UNIFIED_CONDEN=3};
  /**
    * if  LOAD_CONTROL , solve until time = 1.
    * if ARC_CONTROL_EULER, solve by explicit Euler and only at microlevel
    */
  enum CONTROL_TYPE{LOAD_CONTROL=0, ARC_CONTROL_EULER=1};

 protected:
  GModel *pModel;
  int _dim, _tag;
  int _mpiUserDom;
	int _workingRank; // current working rank
  bool _isPartitioned; // true if FE mesh is partitioned
  // the mapRank is created from number of proc available and number of mesh partion,
  std::map<int,std::set<int> > _mapRanks;  
  std::string _meshFileName; // To transfert the mesh file from one folder to an other one

    // user solver option given as a string
  std::string _solver_options;
   // For Onelab display
  mutable int _GmshOneLabViewNum;

  //all main objects
  nlsDofManager *pAssembler;
  // IPField declared here to allow to pass it from one scheme to an other scheme
  IPField* _ipf;
	// unknown field
  unknownField* _ufield;
  // energetic field
  energeticField* _energField;
  
  solver whatSolver; //  Solver used to solve
  scheme whatScheme; // scheme used to solve equation

  
  /*TIME SETTING FOR NONLINEAR SOLVER*/
  // for time management
  TimeManager* _timeManager;
  /*FOR EXPLICIT SCHEME*/
  explicitSolverOptions _explicitOpts;

  //
  std::map<int,MElement*> _mapMElementTmp; // used to compute characteristique element size in the deformed configuration CLEAR THE MAP ???
  std::map<int,MElement*> _mapMElementFirstOrder; // idem
  
  /* FOR MULTI SYSTEM */
  /* MultiSystems solve */
  std::vector<int> _vcompBySys;
  std::vector<scheme> _vschemeBySys;
  
  /*FOR QUASI-STATIC AND IMPLICIT SCHEMES*/
  double _tol, _absTol; // relative and absolute tolerance for iteration
  StiffnessModificationMonitoring* _stiffnessModificationMonitoring;
  bool _iterativeNR; // true if using iterative procedure
  bool _lineSearch;
  
  /*FOR CH DYNAMIC SCHEME*/
  implicitSolverOptions _implicitOpts;
  
  /*FOR PATH FOLLOWING*/
  bool _pathFollowing; // true to activate path following
  pathFollowingManager _pfManager;
  
  /* data for restart */
  size_t _beginTime;
  std::string _restartMshFileName;
  bool _resetRestart; //to avoid restart when shitfing schemes
  bool _disableResetRestart; //to allow restat in battery only
  std::vector<int> _restartDomainTag; // tag of domain that needs restart to save/load InternalState
  

  /*FOR CRACK*/
   // physical entities that are initialy broken
  std::vector<std::pair<int, int> > initbrokeninter;
  std::vector<std::pair<int, int> > initbrokeninterInDomains;
  /* crack tracking */
  std::string _crackTrackingName;
  FILE *_crackTrackingFile; // name of files for crack tracking no name == NULL and no track
  /* fragmentation */
  std::string _fragmentationName;
  FILE *_fragmentationFile;
  
  // vector of all domains
  std::vector<partDomain*> domainVector;
  #if defined(HAVE_MPI)
  std::map<int,std::vector<elementGroup> >_mapOtherDomain; // map build by the creation of groupOfElement. Retrieve the element of other partition
  #endif // HAVE_MPI
  std::vector<partDomain*> _ghostDomainMPI; // to prescribed Dirichlet BC of other partition in MPI (avoid search in map in each iteration)
   // vector of all material law
  std::map<int,materialLaw*> maplaw;
  
  /*FOR CONTACT*/
  // contact
  contactContainer _allContact;
  // defo defo contact BC
  defoDefoContactContainer _allDefoDefoContact;

  /*FOR BOUNDARY CONDITIONS*/
  // neumann BC
  std::list<nonLinearNeumannBC> allNeumann;
  // dirichlet BC
  std::list<nonLinearDirichletBC> allDirichlet;
  // all periodic groups
  std::vector<int> _edgePeriodicPhysicals, _nodePeriodicPhysicals;
  std::list<nonLinearPeriodicBCBetweenTwoGroups> allPeriodic;
  //all average PBC groups
  std::list<nonLinearAveragePeriodicBCBetweenTwoGroups> allAveragePeriodic;
  // all samedisp bc
  std::list<nonLinearSameDisplacementBC> allSameDisp;
  // fix on face
  std::list<nonLinearFixOnFaceBC> allFixAllFace;
  // constraint BC
  std::list<nonLinearConstraintBC> allConstraint;
  // when using with microBC, BC can appy on corners of RVE
  std::list<nonLinearDirichletBCAtCorner> allCornerConstraint;
  // force BC
  std::list<nonLinearNeumannBCAtCorner> allCornerForce;
  // initial BC
  std::list<initialCondition> allinitial;
  // neumann BC theta (weak enforcement of rotation) group this with allNeumann ?
  std::list<nonLinearNeumannBC> allTheta;
  // dirichlet BC on rigid surface (prescribed the motion of gravity center)
  std::list<rigidContactBC> allContactBC;
  // general linear constrait BC
  std::list<nonLinearLinearConstraintBCBetweenTwoGroups> allLinearConstraintBC;


  /* FOR ARCHIVING*/
  // archivng internal force at nodes
  std::list<archiveInternalForce> vafIntenal;
  // std vector to archive a node displacement
  std::vector<unknownField::archiveNode> anoded;
  // std::vector to archive a force 
  std::vector<archiveForce> vaf;
  // std vector to archive ipvariable
  std::vector<IPField::ip2archive> vaip;
  // list to archive the integral operation in volume
  std::list<IntegralVolume> _dataVolumeIntegral;
  // list to archive IP data over physical (data at gauss point with coordinates)
  std::list<IPDataOnPhysical> _dataOnPhysical;
  std::list<IPValueTracking> _ipValueTracking;
  // list to archive average IP over clusters
  std::list<clustersData> _clusterData;
  std::list<AvergageCluster> _averageClusters;
  
  // compute J integral
  std::vector<computeJIntegral*> _allJIntegral;
  
  // for archiving energy and fracture energy
  int _energyComputation;  // equal to 0 if no energy is saved; >0 if energy is saved with _energyComputation as interval step of archiving
  int _fractureEnergyComputation; // equal to 0 if no energy is saved; >0 if energy is saved with _energyComputation as interval step of archiving

  /*FOR AVERAGING OVER TIME*/
  // list to store IP data over physical over time period ( data at gauss point with coordinates)
  // similar to _dataOnPhysical but used to postprocess IPVariable value to compute average over given time period
  std::list<IPDataOnPhysical> _dataOnPhysicalOverTime;
  // to compute average of IPVariable over time interval between tstart and tend
  double _tstart;
  double _tend;

  // flag for weak time-scale coupling between two different solvers or problems
  bool _weakTimeScaleCoupling;
  

  /*FOR VIEW*/ 
  // view of unknown --> disp file
  std::vector<nlsField::dataBuildView> unknownView;
  // view of ip field --> stress file
  std::vector<nlsField::dataBuildView> ipView;
  // view of ip field --> stress file
  std::vector<nlsField::dataBuildView> ipViewInterface;
  // view of energy field --> energy file, but it is not 
  std::vector<nlsField::dataBuildView> energyView;
  int nsba; // number of step between two view
 
  /* SWITCH scheme data */
  DofCollection* _collection; // to collec all data from one DofManager
  bool _previousInit; // To known if the initialization as already be made by an other scheme
  bool _notResetedBC; // To known if the BC are modified.
  bool _notResetedContact; // To known if the contact interaction are modified

  // strain mapping for foams
  strainMapping* _strainMap;
  // programe monitoring
  EndSchemeMonitoringBase* _endSchemeMonitoringObject;
  // selective update with cohesive crack
  selectiveUpdateBase* _selectiveObject;

  // element erosion control
  bool _bulkElementErosionFlag; // true if bulk element erosion
  bool _interfaceElementErosionFlag; // true if interface element erosion
  elementErosionType _erosionType; // all ip failed
  FailureCriterionBase* _erosionGlobalCriterion;
  elementErosionFilter _elementErosionFilter; // a set consists of all erosion elements
  
  /*EIGEN SOLVER*/
  eigenSolverOptions _eigOpts;
  std::vector<nlsField::dataBuildView> _eigview;
  
  /*DEFORMED MESH TO FILE*/
  bool _isWriteDeformedMeshToFile; // write deformed mesh to file


  /*FOR MULTISCALE ANALYSIS*/
  // Element number and Integration point number
  int _enumMinus,_enumPlus, _gnum;
  bool _sucessMicroSolve;
  
  // time and time step
  double _macroTimeStep; // for law which works on increment. (Use only in so no getTimeStep function)
  double _macroTime; // To save results vs time
  int _macroStep; // current macroscopic step

  //for micro flag
  SYSTEM_TYPE _systemType;
  CONTROL_TYPE _controlType;
  // micro flag--> true if microsolver is used
  bool _microFlag;
  bool _multiscaleFlag; // to know if a multiscale analysis is performed, to be true with both micro and marco solver 
  
  //micro BC
  nonLinearMicroBC* _microBC;
	nonLinearMicroBC* _microBCOld;
	nonLinearMicroBC* _microFailureBC;
  
  //stress flag and tangent flag
  bool _stressflag; // true if homogenized stress is estimated
  bool _tangentflag; // true if homogenizd tangnent is estimated
    // homogenized  method
  TANGENT_HOMO_TYPE _homogenizeTangentMethod;
  STRESS_HOMO_TYPE _homogenizeStressMethod;
  double _sameStateCriterion; // this parameter to check if same sate-> economise
  // for archiving
  bool _archive;
  // all homogenized filename
  bool _isHommProSaveToFile; // flag -->save homogenized properties to files
  bool _isHommStrainSaveToFile; //flag --> save homogenized strain to files
  bool _extractPerturbationToFile; //flag --> save perturbation on RVE boundary to file
  bool _messageView; // flag to archive message to file
  double _tangentPerturbation; // tangent by perturbation

  double _rho;  // homogenized density
  double _rveVolume; // rve volume
  STensor3 _rveGeoInertia;
  SVector3 _rveGeometry;
  // homogenized stresses
  homogenizedData* _currentState, *_initialState, *_elasticState, *_undamagedElasticState;


  // for managing periodic boudary condition
  pbcAlgorithm* _pAl;
  // for tangent estimation
  stiffnessCondensation* _condensation;
  // eigensolver using for stability analysis
  homogenizedDataFiles* _homogenizedFiles; // homogenized file data
  FILE* _outputFile; // output file --> if set _messagive

  // for test use microBC with macrosolver
  bool _testFlag;
  pbcConstraintElementGroup* _pbcGroup;

  // for batch computations
  bool _batchComputations;
  
  // local basis at element interfaces
  std::map<int, STensor3> _allInterfaceLocalBasis; // ele+ gp, [n b t]
  // to known interface
  std::map<TwoNum,int> _interfaceElements; // negative and positive element number versus interface
  std::map<int, TwoNum> _interfaceElementsInverseMap;
  // 
  bool _damageIsBlocked;
  bool _solverIsBroken;
  double _maximalLostEllipticityCriterion; // maximal value
  double _homogenizedCrackSurface;

  //
  bool _failureBCIsSwitched; // true if FailureBC is switsched after failure,
  bool _failureBasedOnPreviousState;
  bool _GModelIsRotated; 
  bool _checkFailureOnset; //
  bool _checkWithNormal; //
  bool _damageToCohesiveJump; // true if extracting cohsive law
  double _RVELengthInCohesiveNormal; // length perpendicular to cohesive normal
	double _surfaceReductionRatio; // load carrying surface over nominal surface following cohesive normal
  double _lostSolutionUniquenssTolerance;
	double _voidPartInLocalizationBand; // void part in localization band
	bool _extractIrreversibleEnergy; // flag


  STensor3 _FdamOnset; // average damage deformation atinitCornerBC failure onset
  SVector3 _lostSolutionUniquenssNormal; // normal to cohesive element
	// last extract value
	STensor3 _homogenizedStressLastActiveDissipation;
  STensor3 _homogenizedStrainLastActiveDissipation;
	STensor3 _homogenizedDissipationStrainLastActiveDissipation;
	SVector3 _homogenizedCohesiveJumpLastActiveDissipation;
  //
  bool _extractElasticTangentOperator; // true if elastic tangent operator is extracted at the same time as full tangent operator
  bool _extractUndamagedElasticTangentOperator; // true if undamaged elastic tangent operator is extracted at the same time as full tangent operator
  
  FILE*  _elasticDPDFTangentFile;
  FILE*  _undamagedElasticDPDFTangentFile;
  
  std::vector<discreteElement*> _discreteElements;

 protected:
  void commonModel(); // common operation for loadModel and createModel
  void initMapRanks(const std::set<int>& parts);
  void init();
  void init2(); 
  void moveFiles(const std::string &dirName, const std::string &cm);
  void initAllBCsOnDofs();
  void initTerms();
  void endOfScheme(const double endtime, const int endstep); // common operation at the end of a scheme
  void oneStepPreSolve(const double curtime, const double timestep, const int numstep);
	void oneStepPreSolvePathFollowing(const double curtime, const double timestep, const int numstep);
  
  void fillMapOfInterfaceElementsInOneDomain(MElement *e, std::vector<MElement*> &eleFound,
                                const elementGroup *g) const;

	void fillLocalReferenceBasisForAllInterfaceElement();


  void setPairSpaceElementForBoundaryConditions(); // new implementation

  // Function used by non linear solver (return the number of iteration to converge)
  int NewtonRaphson(const int numstep, double time);

  /*
  NewtonRaphson function for arc-length path following
  */
  int NewtonRaphsonPathFollowing(const int numstep, double time);

  double computeRightHandSide(bool withStiff);
  void computeLoadVector();
  void computeBodyForceVector();
  void computePathFollowingConstraint();
  void computeDPathFollowingConstraintDUnknowns();

  void computeStiffMatrix();
  void computeUncoupledStiffMatrix();
  void computeElasticStiffMatrix();
  void computeUndamagedElasticStiffMatrix();
  void computeMassMatrix();
  void computeExternalForces();
  void computeContactForces();
  void computeInternalForces();
  void fixNodalDofs();
  void updateDataFromPreviousSolver();
  void numberDofs();
  void insertTheta(const int numphys, elementGroup *goe);
  void initMapMElementToComputeTimeStep();
  bool considerForTimeStep(const partDomain* dom, MElement* ele) const;
  double initialCriticalExplicitTimeStep();
  double criticalExplicitTimeStep();
  void setInitialCondition();
  void setTimeForBC(double time);
  void setTimeForLaw(const double t,const double dt, const int numstep);
  void initArchiveForce();
  void forceArchiving(const double curtime,const int numstep, const bool forceSave);
  void initPathFollowingArchiving();
  void pathFollowingArchiving(const double curtime, const int numstep);
  void IPDataOnPhysicalArchiving(const double curtime, const int numstep, const bool forceSave);
  void IPTrackingArchiving(const double curtime, const int numstep, const bool forceSave);
  void IPVolumeIntegralArchiving(const double curtime, const int numstep, const bool forceSave);
  
  void initAverageCluster();
  void averageClusterArchiving(const double curtime, const int numstep, const bool forceSave);
  
  void initContactInteraction();
  void crackTracking(const double curtime); // 2D only for now !!!
  void postproFragment();

  void applyPBCBetweenTwoGroups();

  void applySameDispBC();
  void applyFixOnFace();

  void fillConstraintDof();
  void applyConstraintBC();

  void initCornerBC();
  void fixDofCorner();
  
  void applyLinearConstraintBetweenTwoGroups();

  // For Onelab
  void createOnelabViewOption(const std::string &fname,const int displayType) const;
  friend class restartManager;
  void restart();
  void restartErosion();
  void restartDefoDefoContact();
  void saveInternalState(const std::vector<int> tag);
  void loadInternalState(const std::vector<int> tag);
  void fillIPDataOnPhysical(const double curtime, const int numstep, const bool forceSave,
                            std::map< double, std::map< std::size_t, std::map< int, double > > > & IPVariableOverTime,
                            std::map< std::size_t, std::map< int, SPoint3 > > & coordinates);
  void computePeriodAveragedIPDataOnPhysical(const double tstart, const double tend,
                                             const std::map< double, std::map< std::size_t, std::map< int, double > > > & IPVariableOverTime,
                                             const std::map< std::size_t, std::map< int, SPoint3 > > & coordinates,
                                             std::map< std::size_t, std::map< int, double > > & avgIPVariable);
  void rewriteIPDataOnPhysical(const std::map< std::size_t, std::map< int, double > > & avgIPVariable);
  
  void writeDisturbedMeshByEigenVector(eigenSolver& eigS, int numberMode, double fact);
  void writeDeformedMesh(int step);
  
  void initArchiveInternalForce();
  void internalForceArchiving(const double curtime,const int numstep, const bool forceSave);
  #endif // SWIG
 public:
  nonLinearMechSolver(int tag);
  nonLinearMechSolver(const nonLinearMechSolver& src);
  nonLinearMechSolver* clone(const std::string mshFile, int tag) const;
  void copyOptionsToOtherSolver(nonLinearMechSolver* sv) const;
  void copyBCsToOtherSolver(nonLinearMechSolver* sv) const;
  void transferIPField(nonLinearMechSolver* other, const IPDataTransfer& datatransfer);
  #ifndef SWIG
  ~nonLinearMechSolver();
  nonLinearMechSolver* clone(int tag, 
                      const std::string mshFile, 
                      std::vector<partDomain*>& allDom, 
                      std::vector<materialLaw*>& allMat) const;
  
  int getNumRanks() const;
  int getNumRootRanks() const;
  bool rankOnSolver(const int rank) const;
  void getAllRootRanks(std::set<int>& rootRanks) const;
  void getAllOtherRanks(std::set<int>& otherRanks) const;
  void getRootRank(const int otherRank, int& rootRank) const;
  void getOtherRanks(const int rootRank, std::set<int>& otherRanks) const;
  
  bool isInitialized() const;
  bool isMultiscale() const;
  bool isDgDomain() const;
  bool withEnergyDissipation() const;
  bool withFractureLaw() const;
  bool computedUdF() const;
  partDomain::BodyForceModuliType useWhichModuliForBF() const;
  void getLocalBasis(const MElement* ele, const int gpt, SVector3& n, SVector3& t, SVector3& b) const;

  int getDim() const {return _dim;};
  int getTag() const {return _tag;};
  scheme getScheme() const{return whatScheme;}
  linearSystem<double>* createSNLSystem();
  linearSystem<double>* createExplicitSystem();
  linearSystem<double>* createImplicitSystem();
  linearSystem<double>* createEigenSystem();
  void createSystem();
 
  void checkElementErosion(const IPStateBase::whichState ws);
  double solveExplicit();
  double solveMulti();
  double solveEigen();
  void createInterfaceElement();
  materialLaw* getMaterialLaw(const int num);
  // create interfaceelement with dgGoupOfElement from dg project doesn't work (segmentation fault)
  void createInterfaceElement_2();
  bool mustRestart(const int numstep);
	pathFollowingManager::pathFollowingLocalIncrementType getPathFollowingLocalIncrementType() const{return _pfManager._pathFollowingIncrementType;};
	pathFollowingManager::pathFollowingLocation getPathFollowingLocation() const {return _pfManager._pathFollowingLocation;};
  pathFollowingManager::pathFollowingMethod getPathFollowingMethod() const{return _pfManager._pathFollowingMethod;};
  
  bool localPathFollowingSwitching() const;
  double computeLocalPathFollowingStep() const;

  bool withPathFollowing() const {return _pathFollowing;};
	void rotateModel(const SVector3& n1, const SVector3& n2, const SVector3& n3);
  bool GModelIsRotated() const {return _GModelIsRotated;}
  int getInterfaceElementNumber(const int em, const int ep) const;
  TwoNum getMinusAndPlusElementNumber(const int elnum) const;
  const std::map<int,TwoNum>& getInterfaceElementsInverseMap() const {return _interfaceElementsInverseMap;};
  
  const std::string& getMeshFileName() const {return _meshFileName;};
  
  int getStepBetweenArchiving() const {return nsba;};

	IPField* getIPField();
	const IPField* getIPField() const ;

	unknownField* getUnknownField();
	const unknownField* getUnknownField() const;

	nlsDofManager* getDofManager() ;
	const nlsDofManager* getDofManager() const;

	std::vector<partDomain*>* getDomainVector();
	const std::vector<partDomain*>* getDomainVector() const;

	std::vector<partDomain*>* getGhostDomainMPI();
	const std::vector<partDomain*>* getGhostDomainMPI() const;

	contactContainer* getAllContactDomain();
	const contactContainer* getAllContactDomain() const;

	defoDefoContactContainer* getAllDefoDefoContactDomain();
	const defoDefoContactContainer* getAllDefoDefoContactDomain() const;

	GModel* getGModel();
	const GModel* getGModel() const;

  std::map<int,materialLaw*>& getMaplaw() {return maplaw;};
  const std::map<int,materialLaw*>& getMaplaw() const {return maplaw;};

  bool withFailureBasedOnPreviousState() const  {return _failureBasedOnPreviousState;};
  void initArchiveReactionForceOnFixedPhysical(archiveForce& aforce);
  // for eigen value problem
  eigenSolver* eigenSolve(const int numstep);
  void nextStep(const double curtime, const int step);
  #endif
  
 public:
  std::string getFileSavingPrefix() const;
  // for eigensolver
  void eigenValueSolver(const int num = 10); // number of eigenvalue
  void setEigenSolverParamerters(const int type, const int numiter, const std::string method, const double tol, const bool mktofile = false, const bool hem = true);
  void setModeView(const int view, const int nbstepArch = 1); 
  void setDisturbedEigenMode(int numMode, double val, bool flag = true);
  
  double solveStaticLinear();
  double solveSNL();
  // functions for python (swig) interaction
  void loadModel(const std::string &meshFileName);
  void createModel(const std::string &geoFileName, const std::string &outFileName="",const int dim=3,const int order=1,
                   const bool incomplete=false);
  double solve(); // return the time reaches at the end of the computation can be !=endtime if solving problem
  void addDomain(partDomain *dom);
  void addMaterialLaw(materialLaw *mlaw);
  void Solver(const int s){whatSolver= (solver)s;}
  void Scheme(const int s){whatScheme=(scheme)s;}
  void lineSearch(const bool line);

  // path following data
	void pathFollowing(const bool p, const int method);
  void setPathFollowingControlType(const int i);
	void setPathFollowingCorrectionMethod(const int i);
	void setPathFollowingTranversalCriterion(const int i);
	void setPathFollowingSolverType( const int i, const double eqRatio);
  void setBoundsOfPathFollowingArcLengthSteps(const double lowerBound, const double upperBound);

	//increment adaptation in path following based on number NR iterations
	void setPathFollowingIncrementAdaptation(const bool adap, const int numNROptimal, const double exp = 1.);
  void setPathFollowingIncrementAdaptationLocal(const bool adap, const int numNROptimalLoad, const int numNROptimalLocal,const double exp = 1.);
	// increment for local pathFollowing
	void setPathFollowingLocalSteps(const double loadStep, const double localCrStep); // if setPathFollowingIncrementAdaptation is active, localCrStep is not used
	void setPathFollowingLocalIncrementType(const int i);
  void setBoundsOfPathFollowingLoadSteps(const double lowerBound, const double upperBound);
  void setBoundsOfPathFollowingLocalSteps(const double lowerBound, const double upperBound);
	// for global pathFollowing
	void setPathFollowingArcLengthStep(const double arcStep);
	// set path folowing location
	void setPathFollowingLocation(const int loc);
  // set _pathFollowingSwichCriterion
  void setPathFollowingSwitchCriterion(const double cr);

  // set _hyperellipticControlComp
  void clearAllHyperellipticControlComp();
  void setHyperellipticControlComp(const int comp);

  void stiffnessModification(const bool flag = true);
  void stiffnessModification(const StiffnessModificationMonitoring& stiffModifMonitor);
  void iterativeProcedure(const bool flag = true);
  void snlData(const int ns, const double et, const double reltol, const double absTol=1.e-21);
  void clearNumStepSeries();
  void setNumStepTimeInterval(double t, int nstep);
  void snlManageTimeStep(const int miteNR,const int iteIncreaseTS, const double redfactor, const int maxAttemptRedFactor);
  void setTimeIncrementAdaptation(const bool adap, const int numNROptimal, const double exp, const double maximalStep, const double minTimeStep, const int maxFailedIter);
  void addSystem(const int ncomp,const int s);
  void clearMultiSystem();
  void switchSystem(const int sysindex,const int s); // first sysindex => sysnum =0
  void explicitData(const double ftime, const double gams=0.666667, const double beta=0, const double gamma=0.5, const double alpham=0.,const bool benson=false);
  void implicitData(const double beta, const double gamma, const double alpham , const double alphaf);
  void explicitSpectralRadius(const double ftime,const double gams=0.666667,const double rho=1.,const bool benson=false);
  void options(const std::string &options);
  void implicitSpectralRadius(const double rho);
  void explicitTimeStepEvaluation(const int nst);
  void stepBetweenArchiving(const int n);
  void thetaBC(const int numphys);
  double getCoordinatesOfNodePhysical(const int numphys, const int comp);
  void displacementBC(std::string onwhat, const int numphys, const int comp, const double value);
  void displacementBC(std::string onwhat, const int numphys, const int comp, const double valueDiff, const double valueInit);
  void displacementBC(std::string onwhat, const int numphys, const int comp, const double value,elementFilter *filter);
  void displacementBC(std::string onwhat, const int numphys, const int comp, simpleFunctionTime<double> *fct);
  void curlDisplacementBC(std::string onwhat, const int numphys, const int comp, const double value);
  void displacementBC(std::string onwhat, const int numphys, const int comp, elementFilter *filter, simpleFunctionTime<double> *fct, const mixedFunctionSpaceBase::DofType dofType=mixedFunctionSpaceBase::DOF_STANDARD);
  void velocityBC(std::string onwhat, const int numphys, const int comp, const double value);
  void velocityBC(std::string onwhat, const int numphys, const int comp, const double value,elementFilter *filter);
  void velocityBC(std::string onwhat, const int numphys, const int comp, simpleFunctionTime<double> *fct);
  void velocityBC(std::string onwhat, const int numphys, const int comp, elementFilter *filter, simpleFunctionTime<double> *fct);
  void accelerationBC(std::string onwhat, const int numphys, const int comp, const double value);
  void constraintBC(std::string onwhat, const int numphys, const int comp);
  void addDiscreteELement(discreteElement* disElem);
  // create a Tree for gauged BC using tree-cotree
  void createTreeForBC(const std::string &PhysicalCurve, const std::string &PhysicalSurface, const std::string &PhysicalVolume, const int OutputPhysical);
  
  // contraint with (vertex[comp] in physMater) =  slFactor*(vertex[comp] in physSlave) + rh
  // op is an operation  to match a vertex in physMater with a vertex in  physSlave
  void linearConstraintBCBetweenTwoGroups(std::string onwhat, const int physMater, const int physSlave, const int comp, const double slFactor,
                                                    const linearCombinationOfVertices* rh,
                                                    const vertexGroupOperation* op,
                                                    const setInt* exclVertices);
  // contraint with (vertex[comp] in physMater) =  rh
  void linearConstraintBCBetweenTwoGroups(std::string onwhat, const int physMater, const int comp,
                                                    const linearCombinationOfVertices* rh,
                                                    const setInt* exclVertices);
  void addPeriodicEdgePhysicals(int edge);
  void addPeriodicNodePhysicals(int node);
	void periodicBC(std::string onwhat, const int phys1, const int phys2, const int v1, const int v2, const int comp);
  void averagePeriodicBC(std::string onwhat, const int phys1, const int phys2, const int v1, const int v2, const int comp);
  void sameDisplacementBC(std::string onwhat, const int phy, const int rootphy,
                                  const int comp, const double fact = 1.);
  void sameDisplacementBCBetweenTwoGroups(std::string onwhat, const int phy1, const int phy2, const int comp);
  void sameAverageDisplacementBCBetweenTwoGroups(std::string onwhat, const int phy1, const int phy2, const int comp);
  void fixOnFace(std::string onwhat, const int phy, const double A, const double B, const double C, const double D);
  void symetryBC(std::string onwhat, const int phy);
  void displacementRigidContactBC(const int numphys, const int comp,const double value);
  void displacementRigidContactBC(const int numphys, const int comp_, simpleFunctionTime<double> *fct);
  void initialBC(std::string onwhat, std::string whichC, const int numphys, const int comp, const double value);
  void initialBC(std::string onwhat, std::string whichC, const int numphys, const int comp, const double value, elementFilter *filter);
  void initialBC(std::string onwhat, std::string whichC, const int numphys, const int comp, simpleFunctionTime<double> *fct,elementFilter* filter=NULL);
  void initialDCBVeloBC(std::string onwhat,const int numphys,const int axiscomp,const int deflcomp,const double length, const double value);
  
  void forceBC(std::string onwhat, const int numphys, const int comp, const double val);
  void forceBC(std::string onwhat, const int numphys, const int comp, simpleFunctionTime<double> *fct);
  
  void scalarFluxBC(std::string onwhat, const int numphys, const int comp, const double val);
  void scalarFluxBC(std::string onwhat, const int numphys, const int comp, simpleFunctionTime<double> *fct);
  
  void independentDisplacementBC(std::string onwhat, const int numphys, const int comp, const double value);
  void independentForceBC(std::string onwhat, const int numphys, const int comp, const double val);
  
  void blastPressureBC(const int numphys,const double p0,const double p1, const double plexp, const double t0, const double t1);
  void pressureOnPhysicalGroupBC(const int numphys, const double pressDiff, const double p0);
  void pressureOnPhysicalGroupBC(const int numphys, simpleFunctionTime<double> *fct);
  
  void blastPressureBC(std::string onwhat,const int numphys,const double p0,const double p1, const double plexp, const double t0, const double t1);
  void pressureOnPhysicalGroupBC(std::string onwhat, const int numphys, const double pressDiff, const double p0);
  void pressureOnPhysicalGroupBC(std::string onwhat, const int numphys, simpleFunctionTime<double> *fct);
  
  
  void archivingInternalForceOnPhysicalGroup(const std::string onwhat, const int numphys,const int nstep=1);
  void archivingForceOnPhysicalGroup(const std::string onwhat, const int numphys, const int comp,const int nstep=1);
  void archivingRigidContactForce(const int numphys, const int comp,const int nstep=1);
  void archivingNodeDisplacement(const int num, const int comp,const int nstep=1);
  void archivingMeshVertexDisplacement(const int num, const int comp,const int nstep = 1);
  void archivingNodeDisplacementOnPhysical(const int dim, const int physical, const int comp,const int nstep=1);
  void archivingNodeDisplacementOnControlNode(const int pos, const int comp,const int nstep=1);
  
  /**volume averaging of a quantity at gP and gpFt is used as factor depending on GP */
  void archivingVolumeIntegralValue(const int num, const int nstep=1, const GPFilter* gpFt=NULL);
  void archivingVolumeIntegralValueOnPhysical(const int phy, const int num, const int nstep=1, const GPFilter* gpFt=NULL);
  void archivingAverageValue(const int num, const int nstep=1, const GPFilter* gpFt=NULL);
  void archivingAverageValueOnPhysical(const int phy, const int num, const int nstep=1, const GPFilter* gpFt=NULL);
  void archivingIncrementVolumeIntegralValue(const int num, const int nstep=1, const GPFilter* gpFt=NULL);
  void archivingIncrementVolumeIntegralValueOnPhysical(const int phy, const int num, const int nstep=1, const GPFilter* gpFt=NULL);
  void archivingIncrementAverageValue(const int num, const int nstep=1, const GPFilter* gpFt=NULL);
  void archivingIncrementAverageValueOnPhysical(const int phy, const int num, const int nstep=1, const GPFilter* gpFt=NULL);
  void archivingAverageValueActiveDissipation(const int num, const int nstep=1);
  void archivingIncrementBasedAverageValueActiveDissipation(const int num, const int nstep=1);
  
  // for clusters averaging
  void loadClustersDataFile(const std::string fileName);
  void archivingAverageValueOverClusters(const int num, const int nstep=1);
  
  void archivingIPValueTracking(const int ipval, const int nstep, const GPFilter* gpf=NULL, const elementFilterLocation* filbox=NULL);
  void archivingIPValueTracking(const int ipval1, const int ipval2,  const int nstep, const GPFilter* gpf=NULL, const elementFilterLocation* filbox=NULL);
  void archivingIPValueTracking(const int ipval1, const int ipval2, const int ipval3,  const int nstep, const GPFilter* gpf=NULL, const elementFilterLocation* filbox=NULL);
  void archivingIPValueTracking(const int ipval1, const int ipval2, const int ipval3,  const int ipval4,  const int nstep, const GPFilter* gpf=NULL, const elementFilterLocation* filbox=NULL);
  void archivingIPValueTracking(const std::vector<int>& ipvalvec, const int nstep, const GPFilter* gpf=NULL, const elementFilterLocation* filbox=NULL);
  void archivingIPDataOnPhysical(const int ipval, const int phys, const int dim, const int nstep=1, bool oneFileForAllStep = false,int ipLoc=-1);
  void averagingIPDataOnPhysicalOverTime(const double tstart, const double tend, const int ipval, const int phys, const int dim, const int nstep=1, bool oneFileForAllStep = true, int ipLoc=-1);
  void getTimeStepsToComputeAverageOverTime(double & tstart, double & tend) const;
  void setWeakTimeScaleCoupling(const bool weakTimeScaleCoupling);
  const bool getWeakTimeScaleCoupling() const;

  void archivingNodeVelocity(const int num, const int comp,const int nstep=1);
  void archivingNodeAcceleration(const int num, const int comp,const int nstep=1);
  void archivingNode(const int num, const int comp, nonLinearBoundaryCondition::whichCondition,const int nstep=1);
  void archivingNode(const int numpphys, const int dim, const int comp, nonLinearBoundaryCondition::whichCondition,const int nstep);
  
  void archivingNodeIP(const int numphys, const int ipval,const int elemval,const int nstep=1);
  void archivingNodeIPOnPhysicalGroup(const int dim, const int numphys, const int ipval, const int nstep=1);
  void archivingElementIP(const int elenum, const int ipval,const int elemval, const int numip=0, const int nstep=1);
  void archivingInterfaceElementIP(const int elenumMinus,const int elenumPlus, const int ipval,const int elemval, const int numip=0, const int nstep=1);
  void archivingIPOnPhysicalGroup(std::string onwhat, const int numphys, const int ipval, const int elemval,
                      const int nstep=1);

  // function if considering number of Simpson's point  > 1
  void archivingNodeIPShell(int loc, const int numphys, const int ipval,const int elemval,const int nstep=1);
  void archivingNodeIPShellOnPhysicalGroup(int loc,const int dim, const int numphys, const int ipval, const int nstep=1);
  void archivingElementIPShell(int loc,const int elenum, const int ipval,const int elemval, const int numip=0, const int nstep=1);
  void archivingInterfaceElementIPShell(int loc,const int elenumMinus,const int elenumPlus, const int ipval,const int elemval, const int numip=0, const int nstep=1);
  void archivingIPShellOnPhysicalGroup(int loc,std::string onwhat, const int numphys, const int ipval, const int elemval,
                      const int nstep=1);
                      
  void physInitBroken(const int dim, const int phys);
  void initialBrokenDomain(const int phys);
  void initialBrokenInterfaceDomain(const int physMinus, const int phyPlus);
  
//  void setInitOrRestartFileName(const std::string fname);
  void contactInteraction(contactDomain *cdom);
  void defoDefoContactInteraction(defoDefoContactDomain *cdom);
  void archivingRigidContact(const int numphys, const int comp, const int wc=0,const int nstep=1);
  void unknownBuildView(const int comp=0,const int nbstep=1);
  void OneUnknownBuildView(const std::string vname, const int comp,const int nbstep=1);
  void internalPointBuildView(const std::string vname,const int comp,const int nbstep=1,const int ev=-1, const int gp=0);
  void internalPointBuildViewInterface(const std::string vname,const int comp,const int nbstep=1,const int ev=-1, const int gp=0);
  void internalPointBuildViewIncrement(const std::string vname,const int comp,const int nbstep=1,const int ev=-1, const int gp=0);
  void internalPointBuildViewRate(const std::string vname,const int comp,const int nbstep=1,const int ev=-1,const int gp=0);
  void energyBuildView(const std::string vname,const int comp,const int nbstep=1);
  
  // for shell
  // if 1 Simpson's point is used, all usual views (internalPointBuildView, internalPointBuildViewIncrement, internalPointBuildViewRate) should be used
  // if >1 Simpson's point is used, all view with Shell should be used using loc to indicate the Simpson location, 0-> nsimpson -1
  void internalPointBuildViewShell(int loc, const std::string vname,const int comp,const int nbstep=1,const int ev=-1, const int gp=0);
  void internalPointBuildViewIncrementShell(int loc, const std::string vname,const int comp,const int nbstep=1,const int ev=-1, const int gp=0);
  void internalPointBuildViewRateShell(int loc, const std::string vname,const int comp,const int nbstep=1,const int ev=-1,const int gp=0);
  //
  void energyComputation(const int val=1);
  void fractureEnergy(const int val=0);
  void crackTracking(std::string fname);// time and time step
  void postproFragment(std::string fname);
  void resetBoundaryConditions(); 
  void resetContactInteraction();
  void disableResetRestart();  //to avoid restart at a change of solver
  void setRestartDomainTag(const int tag); // to set domain tag for restart
  const std::vector<int> getRestartDomainTag(); // get domain tag for restart set by user
  void setFactorOnArchivingFiles(const int fact); // only for csv files (for view use nstepBetweenArchiving)
  void dynamicRelaxation(const double gams,const double ftime,const double tol=1.e-6,const int wsolver=0,const bool benson=false);
  void createRestartByTime(const int day,const int hour=0,const int minute=0,const int second=0);
  void createRestartBySteps(const int numstep);

  void createStrainMapping(const std::string  filename, const int nbstepArch=1);
  // Get the value of a Dof contained in a txt file
  double getArchivedNodalValue(const int numphys,const int comp,nonLinearBoundaryCondition::whichCondition) const;
  double getArchivedForceOnPhysicalGroup(std::string onwhat, const int numphys,const int comp) const;
  double getEnergy(int we) const;
  double getHomogenizedStress(const int i, const int j) const;
  double getHomogenizedTangent(const int i, const int j, const int k, const int l) const;
  double getHomogenizedSecondStress(const int i, const int j, const int k) const;
  double getHomogenizedSecondTangent(const int i, const int j, const int k, const int p, const int q, const int r) const;
  double getHomogenizedFirstSecondTangent(const int i, const int j, const int p, const int q, const int r) const;
  double getHomogenizedSecondFirstTangent(const int i, const int j, const int k, const int p, const int q) const;
  double getHomogenizedCohesiveJump(const int i) const;
  double getNodeIP(const int physical, const int ipVal) const;
  void getHomogenizedTangent(fullMatrix<double>& Cel) const;
  std::string getHomogenizedTangentCompNameInMatrixForm(int row, int col) const;
  void getAcousticTensor(double n0, double n1, double n2, fullMatrix<double>& A) const; 

  void setSelectiveUpdate(const selectiveUpdateBase& selectiveObj);
  void endSchemeMonitoring(const EndSchemeMonitoringBase& endObj);
  bool solverEndedByMornitoring() const;
  void SetVerbosity(int vb) const {Msg::SetVerbosity(vb);}
  
  // when using implicit solver, the nomass dof (nonlocal, temperature) can be predicted without considering its velocity
  // and its acceleration, its predictor value at current step is equal to its value at previous converged step 
  void setNoMassPredictorWithoutVelocityAndAcceleration(const bool fl);

  // Failure control settings
    // for crack insertion
  void setMaxAllowedCrackInsertionAtOneStep(const int maxNum);
    // for element erosion
  void setElementErosion(const bool bulkErosion, const bool interfaceErosion, const int method);
  void setGlobalErosionCheck(const bool flag, const FailureCriterionBase* fcr);

  /** To control via Onelab **/
  // Explicit scheme control
  void initializeExplicitScheme();
  double oneExplicitStep(const double curtime, bool &forceTimeStepEvaluation);
  double finalizeExplicitScheme(const double curtime);
  // Implicit scheme control
  void initializeStaticScheme();
  int oneStaticStep(const double curtime, const double dt, const int numstep);
  void oneStepPostSolve(const double curtime, const int numstep);
  double finalizeStaticScheme(const double curtime, const int numstep);

  // Display control
  void createOnelabDisplacementView(const std::string &fname,const double time) const;
  void createOnelabVelocityView(const std::string &fname,const double time) const;
  // Onelab can only access to dof that are archived in txt files
  // They are given in the order defined by the user in his/her python files
  void getOnelabArchiveNodalUnknowns(fullVector<double> &nodalValues) const;

  bool checkVertexBelongToDomains(const int phyVer) const;
  
  void setTimeManager(const TimeManager& tm);
  const TimeManager* getTimeManager() const {return _timeManager;};
  TimeManager* getTimeManager() {return _timeManager;};
  
  // add J integral
  void computeJIntegralByDomainIntegration(computeJIntegral* JIntegralObj);

 protected:
  #ifndef SWIG
  void init2Micro();
  void switchMicroBC(const nonLinearMicroBC* mbc);
  void setTimeForMicroBC(const double time);
  void microNumberDof();
  
  void set2ndOrderHomogenizedTangentOperatorWithCorrection();
  void modifyHOStressTangent(fullMatrix<double>& L, const STensor53& dPdG, const STensor53& dQdF, const STensor63& dQdG);
  void HOStressTangent_modificationTerm(STensor53& dPdG, STensor53& dQdF, STensor63& dQdG);
  void setHomogeneousTangentToDomain(homogenizedData* homoData);

  void fillTangent(const fullMatrix<double>& L, homogenizedData* homoData);

  void computeSplittedStiffnessMatrix(bool accountBodyForceForHO);
  stiffnessCondensation* createStiffnessCondensation(const bool sflag, const bool tflag);

  /** get homogzenized stress in case of no tangent computed **/
  void extractAverageStressByCondensation(homogenizedData* homoData);
  void extractAverageStressByVolumeIntegral(homogenizedData* homoData);
  void extractAverageStress(homogenizedData* homoData);


  /**get homogenized stress and tangent if stress and tangent are computed at the same time **/
  void extractAveragePropertiesByCondensation(homogenizedData* homoData);
  void extractAveragePropertiesPerturbation(homogenizedData* homoData);
  void extractAveragePropertiesByInSystemCondensationDISP_MULT(homogenizedData* homoData);
  void extractAveragePropertiesByInSystemCondensation(homogenizedData* homoData);
  void extractdUdFByInSystemCondensation(homogenizedData* homoData);
  void extractAverageStressAndTangent(homogenizedData* homoData);

  void homogenizedDataToFile(const double time);
  void computeDensity();
  
  // newton raphson
  int microNewtonRaphson(const int numstep, double time);
  int microNewtonRaphsonPathFollowing(const int numstep, double time);

  int pathFollowingPerturbation();

  linearSystem<double>* createMicroSNLSystem();
  void createMicroSystem();

  double solveMicroSolverSNL();
  double solveMicroSolverStaticLinear();
  double solveMicroSolverForwardEuler();

  void OneStep(const int numstep, double time);

 

 public:
  void setTime(const double ctime,const double dtime, const int curstep);
  // RVE analysis newton raphson
  double microSolveStaticLinear();
  double microSolveSNL();
  //rve volume
  double getRVEVolume() const;
  const STensor3&  getRVEGeometricalInertia() const;
  const SVector3&  getRVEGeometry() const;
  bool dissipationIsBlocked() const;
  void blockDissipation(const IPStateBase::whichState ws, const bool fl);
  void extractAverageProperties(bool stiff);
  homogenizedData* getHomogenizationState(const IPStateBase::whichState state);
  const homogenizedData* getHomogenizationState(const IPStateBase::whichState state) const;

	nonLinearMicroBC* getMicroBC() {return _microBC;};
	const nonLinearMicroBC* getMicroBC() const {return _microBC;};

	nonLinearMicroBC* getMicroBCFailure() {return _microFailureBC;};
	const nonLinearMicroBC* getMicroBCFailure() const {return _microFailureBC;};


  void resetSolverToInitialStep();

	void checkFailureOnset();

	void computeHomogenizedCrackFace();

	double getConstitutiveExtraDofDiffusionEquationRatio(const int index) const;
	double getNonConstitutiveExtraDofEquationRatio(const int index) const;

	bool getExtractIrreversibleEnergyFlag() const;
	bool getDamageToCohesiveJumpFlag() const;
	int getHomogenizationOrder() const;
	bool inMultiscaleSimulation() const;
	const elementErosionFilter& getElementErosionFilter() const;
  elementErosionFilter& getElementErosionFilter();
  
	bool solverIsBroken() const;
	void brokenSolver(const bool fl);

	const SVector3& getLostSolutionUniquenssNormal() const;

	double& getHomogenizedCrackFace();
	const double& getHomogenizedCrackFace() const;

	STensor3& getFdamOnset();
	const STensor3& getFdamOnset() const;
 #endif // SWIG
 public:
  void createMicroModel(const std::string geoFile, const int dim, const int order, const bool incomplte  = false);
  void setMicroProblemIndentification(int ele, int gpt);
  void setMicroProblemIndentification(int eleMinus, int elePlus, int gpt);
  void initMicroSolver();
  bool microSolve(bool forced=false);

  void prepareForNextMicroSolve();
  void archiveData(const double curtime, const int numstep, const bool forceView);

  void setMessageView(const bool view);
  void setPeriodicity(const double x, const double y, const double z, const std::string direction);
  void addMicroBC(const nonLinearMicroBC* bc);
	void addMicroBCForFailure(const nonLinearMicroBC* bc);

  // get macro properties
  void tangentAveragingFlag(const bool fl = true);
  void stressAveragingFlag(const bool fl = true);

  void setStressAveragingMethod(const int method);
  void setTangentAveragingMethod(const int method, const double prec = 1.e-8);

  void setDisplacementAndIPArchiveFlag(const bool flag = true);
  void setHomogenizationPropertyArchiveFlag(const bool flg = true);
  void setStrainArchiveFlag(const bool flg = true);

  double getHomogenizedDensity() const;

  // compute arbitrary quantitiy from microscopic problem defined in IPField::Output
  void getTotalIntegralByVolumeIntegralOnPhysical(const int phys, const int num, double& val, const GPFilter* filter=NULL) const;
  void getTotalIntegralByVolumeIntegral(const int num, double& val, const GPFilter* filter=NULL) const;
  void getTotalIncrementIntegralByVolumeIntegralOnPhysical(const int phys, const int num, double& val, const GPFilter* filter=NULL) const;
  void getTotalIncrementIntegralByVolumeIntegral(const int num, double& val, const GPFilter* filter=NULL) const;
  
  void getHomogenizedPropertyByVolumeIntegral(const int num, double& val, const GPFilter* filter=NULL) const;
  void getHomogenizedPropertyByVolumeIntegralOnPhysical(const int phys, const int num, double & val, const GPFilter* filter=NULL) const;
  
  void getHomogenizedIncrementPropertyByVolumeIntegral(const int num, double& val, const GPFilter* filter=NULL) const;
  void getHomogenizedIncrementPropertyByVolumeIntegralOnPhysical(const int phys, const int num, double & val, const GPFilter* filter=NULL) const;
  
  void getHomogenizedPropertyByVolumeIntegralOnActiveDissipationDomain(const int num, double& val, const IPStateBase::whichState ws = IPStateBase::current) const;
  void getHomogenizedIncrementalPropertyByVolumeIntegralOnActiveDissipationDomain(const int num, double& val) const;
  void setExtractPerturbationToFileFlag(const bool flag);
  void setSystemType(const int i);
  void setControlType(const int i);
  void setMicroSolverFlag(const bool flag);
  void setMultiscaleFlag( const bool flag);
  void setSameStateCriterion(const double cr);
  void saveStiffnessMatrixToFile(const int iter);

  void setRVEVolume(const double val);
  void setRVEGeometricalInertia(const STensor3& val); 
  void setRVEGeometry(const SVector3& val); 
  void setWriteDeformedMeshToFile(bool flag);

	/** perform a particular test**/
	void activateTest(const bool flag);
	void initialize_test();
  void displacementBCOnControlNode(const int pos, const int comp, const double value);
  void displacementBCOnControlNode(const int pos, const int comp, simpleFunctionTime<double> *fct);
  void forceBCOnControlNode(const int pos, const int comp, const double value);
  void getDofOnControlNode(const int pos, std::vector<Dof>& R);

	// for lost of solution uniqueness problem
	void setExtractCohesiveLawFromMicroDamage(const bool check);
	void setLostSolutionUniquenssTolerance(const double tol);
	void setExtractIrreversibleEnergyFlag(const bool ex);
	void setLocalizationNormal(const SVector3& normal);
	void setLocalizationNormal(const double n1, const double n2, const double n3);
	void setRVELengthInNormalDirection(const double l, const double surfaceReductionRatio=1.); // surface ratio
	void setVoidPartInLocalizationBand(const double vp);

	void setCheckFailureOnset(const bool fl, const bool withNormal);
	void setSwitchMicroBCFlag(const bool fl);

  void setExtractElasticTangentOperator(const bool fl);
  void setExtractUndamagedElasticTangentOperator(const bool fl);

  void setBatchComputationsFlag(const bool fl);

};

#endif //_NONLINEARMECHSOLVER_H_

