// Description: general mapping 
//
//
// Author:  <Van Dung NGUYEN>, (C) 2024
//
// Copyright: See COPYING file that comes with this distribution
//
// As this function is templated, it has to be in a separated files of others timeFunction (SWIG generation problem)

#ifndef GENERALMAPPING_H_
#define GENERALMAPPING_H_
#ifndef SWIG
#include <vector>
#include "GmshMessage.h"
#endif //SWIG

class generalMapping
{
  public:
  #ifndef SWIG
  generalMapping(){}
  generalMapping(const generalMapping& src){}
  virtual ~generalMapping(){}
  virtual generalMapping* clone() const = 0;
  virtual std::vector<double> evaluate(const std::vector<double>& x) const = 0;
  #endif //SWIG
};


#endif //GENERALMAPPING_H_
