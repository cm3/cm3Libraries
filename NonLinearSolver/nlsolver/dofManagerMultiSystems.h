//
// C++ Interface: dofManager
//
// Description: manage multi system with only 1 dofManager
//
// Author: <Van Dung NGUYEN>, (C) 2020
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef DOFMANAGERMULTISYSTEMS_H_
#define DOFMANAGERMULTISYSTEMS_H_
#include "staticDofManager.h"

class dofManagerMultiSystems : public nlsDofManager
{
  public:
    typedef typename nonLinearSystemBase::whichMatrix whichMatrix;
    typedef typename nonLinearSystemBase::rhs whichRhs;
  
  protected:
    std::vector<nlsDofManager*> _vmanager;
    std::vector<int> _vncomps; // ième component = number of components of dofManager i
    
    std::map<int, nlsDofManager*> _managerByComp; // crack by map to
    
    int getCompFromDof(const Dof& key ) const
    {
      int t = key.getType();
      if (t < 0) t = -t;
      return t%100; // FIXME, it should be avoid 100
    }
    void seperateDofVecByComp(const std::vector<Dof>& R,  std::map<int, std::vector<Dof> > & DofByComp) const
    {
      for (int i=0; i<  R.size(); i++)
      {
        int comp = getCompFromDof( R[i]);
        std::vector<Dof>& allDof = DofByComp[comp];
        allDof.push_back( R[i]);
      }
    };
    void getLocalPosition(const std::vector<Dof>& R, const std::vector<Dof>& RSub, std::vector<int>& location) const
    {
      std::map<Dof, int> RLoc;
      for (int i=0; i< R.size(); i++)
      {
        RLoc[R[i]] = i;
      }
      location.resize(RSub.size());
      for (int i=0; i< RSub.size(); i++)
      {
        location[i] = RLoc[RSub[i]];
      };
    };
  
  public:
    dofManagerMultiSystems();
    virtual ~dofManagerMultiSystems();
    
    virtual void add(nlsDofManager* manager, const int ncomp);
    
    // get manager by comp, not not, return this
    virtual nlsDofManager* getManager(const int i);
    virtual const nlsDofManager* getManager(const int i) const;
    virtual nlsDofManager* getManagerByComp(int comp);
    virtual const nlsDofManager* getManagerByComp(int comp) const;
    //
    virtual linearSystem<double> *getLinearSystem(std::string &name)
    {
      Msg::Error("getLinearSystem cannot be called with dofManagerMultiSystems choose the system first");
      return NULL;
    };
    virtual const linearSystem<double> *getLinearSystem(std::string &name) const
    {
      Msg::Error("getLinearSystem cannot be called with dofManagerMultiSystems choose the system first");
      return NULL;
    };
    virtual void setCurrentMatrix(std::string name)
    {
      Msg::Error("setCurrentMatrix cannot be called with dofManagerMultiSystems, choose the system first");
    }
    //
    virtual nonLinearMechSolver::scheme getScheme()const {return nonLinearMechSolver::Multi;};
    
    // collection data for new solving
    virtual void collectAllDofValues(DofCollection& collect) const;
    // update data from dof collection
    virtual void updateDataFromCollection(const DofCollection& collect);
    
    virtual void getFixedDof(std::vector<Dof> &R) const;
    virtual void getFixedDof(std::set<Dof> &R) const;
    virtual int getDofNumber(const Dof &ky) const
    {
      Msg::Error("function getDofNumber cannot be called in dofManagerMultiSystems, choose the system first");
      return -1;
    }
    virtual void clearAllLineConstraints();
    virtual void setLinearConstraint(const Dof& key, DofAffineConstraint<double> &affineconstraint);
    virtual void getAllLinearConstraints(std::map<const Dof, const DofAffineConstraint<double>*>& map) const;
    
    virtual bool isFixed(const Dof& key) const;
    virtual bool isAnUnknown(const Dof& key) const;
    virtual bool isConstrained(const Dof& key) const;

    //
    virtual int sizeOfR() const;
    virtual int sizeOfF() const;
    
    virtual void setFirstRigidContactUnknowns() ;
    virtual int getFirstRigidContactUnknowns() const;
    

    virtual void assemble(const std::vector<Dof> &R, const fullMatrix<double> &m);
    virtual void assemble(const std::vector<Dof> &R, const fullMatrix<double> &m,const whichMatrix wm);
    
    virtual void assemble(const std::vector<Dof> &R, const fullVector<double> &m);
    virtual void assemble(const std::vector<Dof> &R, const fullVector<double> &m,const whichRhs wrhs);
    
    // for homogenized tangent estimation
    virtual void assembleToStressMatrix(const std::vector<Dof> &R, const fullMatrix<double> &m)
    {
      Msg::Error("function assembleToStressMatrix cannot be called in dofManagerMultiSystems, choose the system first");
    }
    virtual void assembleToBodyForceMatrix(const std::vector<Dof> &R, const fullMatrix<double> &m)
    {
      Msg::Error("function assembleToBodyForceMatrix cannot be called in dofManagerMultiSystems, choose the system first");
    }
    virtual void assembleToBodyForceVector(const std::vector<Dof> &R, const fullVector<double> &m)
    {
      Msg::Error("function assembleToBodyForceVector cannot be called in dofManagerMultiSystems, choose the system first");
    }
    virtual void assemblePathFollowingConstraint(const double val)
    {
      Msg::Error("function assemblePathFollowingConstraint cannot be called in dofManagerMultiSystems, choose the system first");
    }
    virtual void assembleDPathFollowingConstraintDUnknowns(const std::vector<Dof>& R, const fullVector<double> &m)
    {
      Msg::Error("function assembleDPathFollowingConstraintDUnknowns cannot be called in dofManagerMultiSystems, choose the system first");
    }
    
    virtual void setInitialCondition(const Dof &R, const double &value, const nonLinearBoundaryCondition::whichCondition wc);
    virtual void setInitialCondition(const std::vector<Dof> &R, const std::vector<double> &disp, const nonLinearBoundaryCondition::whichCondition wc);
    
    virtual void fixDof(const Dof& key, const double &value);
    virtual void numberDof(const Dof& key);
    virtual void numberDof(const std::vector<Dof> &R);

    // solving
    virtual void systemSolve();
    virtual int systemSolveIntReturn();
    virtual int lineSearch();
    
    virtual void insertInSparsityPattern(const Dof &R, const Dof &C);
    virtual void sparsityDof(const std::vector<Dof>& R);
    
    virtual void allocateSystem();
    virtual void preAllocateEntries();
    // operations
    virtual void clearRHSfixed();
    virtual double getNorm1ReactionForce() const;
    virtual void getReactionForceOnFixedPhysical(archiveForce& af) const;
    virtual void getForces(std::vector<archiveForce> &vaf) const;
    virtual double getKineticEnergy(const int systemSizeWithoutRigidContact,FilterDofSet &fildofcontact) const;
    
    virtual void zeroMatrix();
    virtual void resetUnknownsToPreviousTimeStep();
    virtual void nextStep();
    virtual void setTimeStep(const double dt);
    //
    virtual double normInfRightHandSide() const;
    virtual double normInfSolution() const;
    virtual double norm0Inf() const;
    //
    #if defined(HAVE_MPI)
    virtual void manageMPIComm(const int otherPartNum,const std::vector<Dof> &otherPartR);
    virtual void manageMPISparsity(const std::vector<Dof> &myR, const int otherPartNum,const std::vector<Dof> &otherPartR);
    virtual void systemMPIComm();
    #endif //HAVE_MPI
    
    virtual void restart();
        // get Data
    virtual void getVertexMass(const std::vector<Dof> &R, std::vector<double> &vmass) const;
    virtual void getFixedRightHandSide(std::vector<Dof> &R, std::vector<double> &val) const;

    virtual void getDofValue(const Dof& key,  double &val) const;
    virtual void getDofValue(const Dof& key,  double &val, nonLinearBoundaryCondition::whichCondition wv) const;
    virtual void getDofValue(const std::vector<Dof> &keys,std::vector<double> &Vals) const;
    virtual void getDofValue(const std::vector<Dof> &keys,std::vector<double> &Vals, const nonLinearBoundaryCondition::whichCondition wv) const;
    //
    virtual void getFixedDofValue(const Dof& key, double &val) const;
    virtual const std::map<Dof, int>& getUnknownMap() const;
};
#endif //DOFMANAGERMULTISYSTEMS_H_
