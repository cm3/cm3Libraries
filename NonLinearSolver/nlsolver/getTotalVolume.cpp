//
//
// Description: get total volume of a finite element mesh
//
// Author:  <Van Dung NGUYEN>, (C) 2013
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "getTotalVolume.h"
#include "GModel.h"
#include "quadratureRules.h"


getTotalVolume::getTotalVolume(): _g(NULL){}
getTotalVolume::~getTotalVolume(){
  if (_g != NULL) delete _g;
  _g = NULL;
};

void getTotalVolume::loadModel(const std::string filename){
  GModel* pModel = new GModel();
  pModel->readMSH(filename.c_str());
  int dim = pModel->getNumRegions() ? 3 : 2;
  std::map<int, std::vector<GEntity*> > groups[4];
  pModel->getPhysicalGroups(groups);
  std::map<int, std::vector<GEntity*> > &entmap = groups[dim];

  _g = new elementGroup();
  for (std::map<int, std::vector<GEntity*> >::iterator it = entmap.begin(); it!= entmap.end(); it++){
    std::vector<GEntity*> &ent = it->second;
    for (unsigned int i = 0; i < ent.size(); i++){
      for (unsigned int j = 0; j < ent[i]->getNumMeshElements(); j++){
        MElement *e = ent[i]->getMeshElement(j);
        _g->insert(e);
      }
    }
  }
  Msg::Info("num element = %d",_g->size());
};

double getTotalVolume::totalVolume(){
  double volume = 0.;
  IntPt* GP = NULL;
  double jac[3][3];
  GaussQuadrature Integ_Bulk(GaussQuadrature::GradGrad);
  for (elementGroup::elementContainer::const_iterator it = _g->begin(); it!= _g->end(); it++){
    MElement* e = it->second;
    int npts = Integ_Bulk.getIntPoints(e,&GP);
    for (int i=0; i<npts; i++){
      const double u = GP[i].pt[0];
      const double v = GP[i].pt[1];
      const double w = GP[i].pt[2];
      const double weight = GP[i].weight;
      const double detJ = e->getJacobian(u, v, w, jac);
      double ratio = detJ*weight;
      volume+=ratio;
    }
  }
  Msg::Info("total volume = %e",volume);
  return volume;
};

double getTotalVolume::volumePhysical(const int dim, const int phys){
  double volume = 0.;
  IntPt* GP = NULL;
  double jac[3][3];
  GaussQuadrature Integ_Bulk(GaussQuadrature::GradGrad);
  elementGroup g(dim,phys);
  for (elementGroup::elementContainer::const_iterator it = g.begin(); it!= g.end(); it++){
    MElement* e = it->second;
    int npts = Integ_Bulk.getIntPoints(e,&GP);
    for (int i=0; i<npts; i++){
      const double u = GP[i].pt[0];
      const double v = GP[i].pt[1];
      const double w = GP[i].pt[2];
      const double weight = GP[i].weight;
      const double detJ = e->getJacobian(u, v, w, jac);
      double ratio = detJ*weight;
      volume+=ratio;
    }
  }
  Msg::Info("total volume = %e",volume);
  return volume;
};
double getTotalVolume::smallestElementSize(){
  double volume = this->totalVolume();
  int elenum = 0;
  for (elementGroup::elementContainer::const_iterator it = _g->begin(); it!= _g->end(); it++){
    MElement* e = it->second;
    if (e->getVolume() < volume){
      volume = e->getVolume();
      elenum = e->getNum();
    }
  }
  int dim = _g->begin()->second->getDim();
  double elesize = 0.;
  double invdim = 1./double(dim);
  if (volume > 0.) elesize= pow(volume,invdim);
  Msg::Info("smallest element: num = %d, size = %e",elenum,elesize);
  return elesize;
};
