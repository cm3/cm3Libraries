//
//
// Description: Class of discret elements
//
//
// Author:  <V.D. NGUYEN>, (C) 2024
//
// Copyright: See COPYING file that comes with this distribution
//
//


#include "discreteElements.h"
#include "nonLinearMechSolver.h"


pbcVertex* MultipleDofNonLinearSpring::getVertex(nonLinearMechSolver* solver, const SPoint3& p) const
{
  pbcVertex* v1 = NULL;
  const std::vector<partDomain*>& allDomains = *(solver->getDomainVector());
  double tol = 1e-8;
  for (int i =0; i< allDomains.size(); i++)
  {
    partDomain* dom = allDomains[i];
    for (elementGroup::elementContainer::const_iterator ite = dom->element_begin(); ite != dom->element_end(); ite++)
    {
      MElement* e = ite->second;
      for (int j=0; j< e->getNumVertices(); j++)
      {
        MVertex* v = e->getVertex(j);
        SVector3 vec1(p, v->point());
        if (vec1.norm() < tol)
        {
          v1 = new pbcVertex(dom,e,v);
          break;
        }
      }
      if (v1 != NULL)
      {
        break;
      }
    }
    if (v1 != NULL)
    {
      break;
    }
  }
  
  if (v1 == NULL)
  {
    Msg::Error("The point %e %e %d can not be found in the mesh!",p[0],p[1],p[2]);
    Msg::Exit(0);
  }
  return v1;
}

MultipleDofNonLinearSpring::MultipleDofNonLinearSpring(double x, double y, double z, int comp, double A, generalMapping* func):
  discreteElement(),
  _location(x,y,z), _comp(comp), _dependentPoints(), 
  _v(NULL), _dependentVertices(), _solver(NULL)
{
  _dofFunc = func->clone();
  _timeFunc = new simpleFunctionTime<double>(A);

};
MultipleDofNonLinearSpring::MultipleDofNonLinearSpring(const MultipleDofNonLinearSpring& src):
  discreteElement(src),
  _location(src._location), _comp(src._comp), _dependentPoints(src._dependentPoints),
  _timeFunc(src._timeFunc), _v(src._v), _dependentVertices(src._dependentVertices), _solver(src._solver)
{
  _dofFunc=NULL;
  if (src._dofFunc != NULL)
  {
    _dofFunc = src._dofFunc->clone();
  }
    
}
MultipleDofNonLinearSpring::~MultipleDofNonLinearSpring()
{
  if (_timeFunc != NULL) delete _timeFunc;
  if (_v != NULL) delete _v;
  for (int i=0; i< _dependentVertices.size(); i++)
  {
    delete _dependentVertices[i].first;
  }
}

void MultipleDofNonLinearSpring::setDependentPoint(double x, double y, double z, int comp)
{
  _dependentPoints.push_back(std::pair<SPoint3, int>(SPoint3(x,y,z),comp));
}

discreteElement* MultipleDofNonLinearSpring::clone() const
{
  return new MultipleDofNonLinearSpring(*this);
}

void MultipleDofNonLinearSpring::initialize(nonLinearMechSolver* solver)
{
  _solver = solver;
  _v = getVertex(solver,_location);
  _dependentVertices.clear();
  for (int i=0; i< _dependentPoints.size(); i++)
  {
    pbcVertex* vv = getVertex(solver,_dependentPoints[i].first);
    _dependentVertices.push_back(std::pair<pbcVertex*, int>(vv, _dependentPoints[i].second));
  }
};


void MultipleDofNonLinearSpring::setTime(double time)
{
  _timeFunc->setTime(time);
};

void MultipleDofNonLinearSpring::getForce(std::vector<Dof>& R, fullVector<double>& vFor) const
{
   std::vector<int> allComps(1,_comp);
  _v->getKeys(allComps, R);
  std::vector<Dof> C;
  for (int i=0; i< _dependentVertices.size(); i++)
  {
    std::vector<int> compT(1,_dependentVertices[i].second);
    _dependentVertices[i].first->getKeys(compT,C);
  }
  std::vector<double> disp(C.size(),0.);
  const unknownField* uf = _solver->getUnknownField();
  for (int i=0; i< C.size(); i++)
  {
    uf->get(C[i], disp[i]);
  }
  std::vector<double> resFunc = _dofFunc->evaluate(disp);
  double timeVal = _timeFunc->operator ()(_location.x(),_location.y(),_location.z());
  vFor.resize(1);
  vFor(0) = resFunc[0]*timeVal;
};
void MultipleDofNonLinearSpring::getStiffness(std::vector<Dof>& RC, fullMatrix<double>& mat) const
{
  std::vector<int> allComps(1,_comp);
  _v->getKeys(allComps, RC);
  for (int i=0; i< _dependentVertices.size(); i++)
  {
    std::vector<int> compT(1,_dependentVertices[i].second);
    _dependentVertices[i].first->getKeys(compT,RC);
  }
  std::vector<double> disp(RC.size()-1,0.);
  const unknownField* uf = _solver->getUnknownField();
  for (int i=1; i< RC.size(); i++)
  {
    uf->get(RC[i], disp[i-1]);
  }
  std::vector<double> resFunc = _dofFunc->evaluate(disp);
  double timeVal = _timeFunc->operator ()(_location.x(),_location.y(),_location.z());
  
  mat.resize(RC.size(), RC.size());
  mat.setAll(0.);
  for (int i=1; i< RC.size(); i++)
  {
    mat(0,i) =  resFunc[i]*timeVal;
  };
};

