//
// Author:  <Van Dung NGUYEN>, (C) 2020
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "meshModification.h"
#include "Context.h"
#include "MElement.h"
#include "MTriangle.h"

TrivialOperation::TrivialOperation():modificationOperation(){};


meshModification::meshModification() : _pModel(NULL){
  _opt = new TrivialOperation();
}
meshModification::~meshModification()
{
  if (_pModel) delete _pModel;
  _pModel = NULL;
  if (_opt) delete _opt; _opt = NULL;
}
    
void meshModification::loadModel(const std::string meshFileName)
{
  if (_pModel) delete _pModel;
  _pModel = new GModel();
  _pModel->readMSH(meshFileName.c_str());
  _meshFileName = meshFileName;
};

void meshModification::loadGeo(const std::string geoFile, int dimToMesh, int order)
{
  if (_pModel) delete _pModel;
  _pModel = new GModel();
  _pModel->readGEO(geoFile);
  // Overwrite options
  CTX::instance()->mesh.order = order;
  _pModel->mesh(dimToMesh);
};

void meshModification::setOperation(const modificationOperation& opt)
{
  if (_opt == NULL) delete _opt;
  _opt = opt.clone();
};

RollingOperation::RollingOperation(int dir, double c, double maxLeng) : _dir(dir),_posAxis(c),_maxLength(maxLeng){}
void RollingOperation::transform(MVertex* v) const
{
  double& x = v->x();
  double& y = v->y();
  double& z = v->z();
  
  if (_dir == 2)
  {
    double pi = 3.1415926535;
    double xx = x + (_posAxis-x)*(1-cos(y*pi/_maxLength));
    double yy = (_posAxis-x)*sin(y*pi/_maxLength);
    x = xx;
    y = yy;
  }
  else
  {
    Msg::Error("this dir %d has not implemented yet",_dir);
  }
}

ScalingOperation::ScalingOperation(double x, double y, double z):_factX(x), _factY(y), _factZ(z){}
void ScalingOperation::transform(MVertex* v) const
{
  v->x() *= _factX;
  v->y() *= _factY;
  v->z() *= _factZ;
};


TranslationOperation::TranslationOperation(double x, double y, double z):_dX(x), _dY(y), _dZ(z){}
void TranslationOperation::transform(MVertex* v) const
{
  v->x() += _dX;
  v->y() += _dY;
  v->z() += _dZ;
};

void meshModification::applyModification(const std::string fname, double version)
{
  Msg::Info("start modifying mesh");
  
  int dim = _pModel->getNumRegions() ? 3 : 2;
  std::map<int, std::vector<GEntity*> > groups[4];
  _pModel->getPhysicalGroups(groups);
  std::map<int, std::vector<GEntity*> > &entmap = groups[dim];

  elementGroup g;
  for (std::map<int, std::vector<GEntity*> >::iterator it = entmap.begin(); it!= entmap.end(); it++){
    std::vector<GEntity*> &ent = it->second;
    for (unsigned int i = 0; i < ent.size(); i++){
      for (unsigned int j = 0; j < ent[i]->getNumMeshElements(); j++){
        MElement *e = ent[i]->getMeshElement(j);
        g.insert(e);
      }
    }
  }
  Msg::Info("num element = %d",g.size());
  Msg::Info("num vertex = %d",g.vsize());
  
  std::set<MVertex*> appliedVertices;
  for (elementGroup::vertexContainer::const_iterator itv = g.vbegin(); itv != g.vend(); itv++)
  {
    MVertex* v = itv->second;
    if (appliedVertices.find(v) == appliedVertices.end())
    {
      _opt->transform(v);
      appliedVertices.insert(v);
    }
  }
  Msg::Info("done operation");
  std::string saveFileName = fname;
  if (fname == "")
  {
    saveFileName = "modif_"+_meshFileName;
  }
  //_pModel->writeMSH(saveFileName, CTX::instance()->mesh.mshFileVersion,CTX::instance()->mesh.binary, CTX::instance()->mesh.saveAll,CTX::instance()->mesh.saveParametric, CTX::instance()->mesh.scalingFactor);
  _pModel->writeMSH(saveFileName,version,CTX::instance()->mesh.binary, CTX::instance()->mesh.saveAll,CTX::instance()->mesh.saveParametric, CTX::instance()->mesh.scalingFactor);
	Msg::Info("End modifying mesh");
}

void write_MSH2(const std::map<int, elementGroup>& gMap,  const std::string filename)
{
  std::set<MVertex*> allVertices;
  int totalNumberOfElements = 0;
  for (std::map<int, elementGroup>::const_iterator itg = gMap.begin(); itg!= gMap.end(); itg++)
  {
    const elementGroup* g = &(itg->second);
    totalNumberOfElements += g->size();
    for (elementGroup::vertexContainer::const_iterator itv = g->vbegin(); itv != g->vend(); itv++)
    {
      allVertices.insert(itv->second);
    }
  }

	FILE* fp = fopen(filename.c_str(),"w");
 	fprintf(fp, "$MeshFormat\n2.2 0 8\n$EndMeshFormat\n");

	fprintf(fp, "$Nodes\n");
	fprintf(fp, "%ld\n", allVertices.size());
	for (std::set<MVertex*>::const_iterator it = allVertices.begin(); it!= allVertices.end(); it++){
		MVertex* v = *it;
		fprintf(fp, "%ld %f %f %f\n",v->getNum(),v->x(),v->y(),v->z());
	}
	fprintf(fp, "$EndNodes\n");
 	fprintf(fp, "$Elements\n");
	fprintf(fp, "%d\n", totalNumberOfElements);
  for (std::map<int, elementGroup>::const_iterator itg = gMap.begin(); itg!= gMap.end(); itg++)
  {
    const elementGroup* g = &(itg->second);
    Msg::Info("num of eelement in gr %d = %ld",itg->first,g->size());
    for (elementGroup::elementContainer::const_iterator it = g->begin(); it!= g->end(); it++){
      MElement* ele = it->second;
      fprintf(fp, "%ld %d 2 %ld %d",ele->getNum(),ele->getTypeForMSH(),itg->first,itg->first);
      std::vector<MVertex*> vers;
      ele->getVertices(vers);
      for (int i=0; i< vers.size(); i++)
      {
        fprintf(fp, " %ld",vers[i]->getNum());
      }
      fprintf(fp, "\n");
    };
  }
	fprintf(fp, "$EndElements\n");
	fclose(fp);
};

bool makePhysicalByBox::box::inBox(MElement* ele) const
{
  SPoint3 P = ele->barycenter();
  SPoint3 Pc;
  Pc[0] = 0.5*(xmax+xmin);
  Pc[1] = 0.5*(ymax+ymin);
  Pc[2] = 0.;
  if ((P.distance(Pc) < r) && ( (P.x() >=xmin)&& (P.x()<=xmax) && (P.y() >=ymin)&& (P.y()<=ymax)))
  {
    return true;
  }
  else 
  {
    return false;
  }
};

void makePhysicalByBox::physicalToMesh(int dim, int phys, const std::string inputMeshFile, const std::string outputMeshFile, int newPhy)
{
  GModel pModel;
  pModel.readMSH(inputMeshFile.c_str());
  std::map<int, elementGroup> allGrps;
  if (newPhy > 0)
  {
    allGrps[newPhy-1].addPhysical(dim,phys);
  }
  else
  {
    allGrps[phys-1].addPhysical(dim,phys);
  }
  write_MSH2(allGrps,outputMeshFile);
};

void makePhysicalByBox::run(const std::string inputMeshFile, const std::string outputMeshFile, 
             double xmin, double xmax, int nx, 
             double ymin, double ymax, int ny)
{
  std::vector<box> allBox;
  double dx = (xmax-xmin)/nx;
  double dy = (ymax-ymin)/ny;
  int num = 0;
  for (int i=0; i< nx; i++)
  {
    for (int j=0; j< ny; j++)
    {
      allBox.emplace_back();
      box& b = allBox.back();
      b.r = 0.3*std::min(dx,dy);
      b.xmin = xmin+i*dx;
      b.xmax = xmin+(i+1)*dx;
      b.ymin = ymin+j*dy;
      b.ymax = ymin+(j+1)*dy;
      b.printData();
      num ++;
    }
  }
  
  Msg::Info("pass hare box size = %ld",allBox.size());
  
  // get all elemeents from model
  GModel pModel;
  pModel.readMSH(inputMeshFile.c_str());
  int dim = pModel.getNumRegions() ? 3 : 2;
  std::map<int, std::vector<GEntity*> > groups[4];
  pModel.getPhysicalGroups(groups);
  std::map<int, std::vector<GEntity*> > &entmap = groups[dim];

  std::map<int, elementGroup> allG;
  int boxSize = allBox.size();
  for (std::map<int, std::vector<GEntity*> >::iterator it = entmap.begin(); it!= entmap.end(); it++){
    std::vector<GEntity*> &ent = it->second;
    for (unsigned int i = 0; i < ent.size(); i++){
      for (unsigned int j = 0; j < ent[i]->getNumMeshElements(); j++){
        MElement *e = ent[i]->getMeshElement(j);
        bool found = false;
        for (int num=0; num < allBox.size(); num++)
        {
          box& b = allBox[num];
          if (b.inBox(e))
          {
            elementGroup& gr = allG[1];
            gr.insert(e);
            found = true;
            break;
          }
        }
        if (!found)
        {
          elementGroup& gr = allG[boxSize];
          gr.insert(e);
        }
      }
    }
  }
  if (dim <= 2)
  {
    std::map<int, std::vector<GEntity*> > &entmap1D = groups[1];
    Msg::Info("size of entmap1D = %d",entmap1D.size());
    int phy = allBox.size();
    for (std::map<int, std::vector<GEntity*> >::iterator it = entmap1D.begin(); it!= entmap1D.end(); it++){
      std::vector<GEntity*> &ent = it->second;
      phy++;
      for (unsigned int i = 0; i < ent.size(); i++){
        for (unsigned int j = 0; j < ent[i]->getNumMeshElements(); j++){
          MElement *e = ent[i]->getMeshElement(j);
          elementGroup& gr = allG[phy];
          gr.insert(e);          
        }
      }
    }
    
    std::map<int, std::vector<GEntity*> > &entmap0D = groups[0];
    Msg::Info("size of entmap0D = %d",entmap0D.size());
    for (std::map<int, std::vector<GEntity*> >::iterator it = entmap0D.begin(); it!= entmap0D.end(); it++){
      std::vector<GEntity*> &ent = it->second;
      phy++;
      for (unsigned int i = 0; i < ent.size(); i++){
        for (unsigned int j = 0; j < ent[i]->getNumMeshElements(); j++){
          MElement *e = ent[i]->getMeshElement(j);
          
          elementGroup& gr = allG[phy];
          gr.insert(e);  
        }
      }
    }
  }
  
  write_MSH2(allG,outputMeshFile);
};


QuadToTriangle::QuadToTriangle(): _pModel(NULL){};
QuadToTriangle::~QuadToTriangle()
{
  if (_pModel) delete _pModel;
};

void QuadToTriangle::loadModel(const std::string meshFileName)
{
  if (_pModel) delete _pModel;
  _pModel = new GModel();
  _pModel->readMSH(meshFileName.c_str());
};

void QuadToTriangle::remesh(MElement* ele, std::vector<MElement*>& newEle) const
{
  if (ele->getTypeForMSH() == MSH_QUA_4)
  {
    std::vector<MVertex*> vv;
    ele->getVertices(vv);
    
    std::vector<double > x(3,0.);
    for (int i=0; i< 4; i++)
    {
      x[0] += 0.25*vv[i]->x();
      x[1] += 0.25*vv[i]->y();
      x[2] += 0.25*vv[i]->z();
    }
    MVertex* newVer = new MVertex(x[0],x[1],x[2]);
    
    Msg::Info("add vertex %d",newVer->getNum());
    
    MElement* e1 = new MTriangle(vv[0],vv[1],newVer);
    MElement* e2 = new MTriangle(vv[1],vv[2],newVer);
    MElement* e3 = new MTriangle(vv[2],vv[3],newVer);
    MElement* e4 = new MTriangle(vv[3],vv[0],newVer);
    
    newEle.push_back(e1);
    newEle.push_back(e2);
    newEle.push_back(e3);
    newEle.push_back(e4);
  }
  else
  {
    Msg::Error("MSH_QUA_4 must be used");
  }
};

void QuadToTriangle::apply(const std::string fname)
{
  Msg::Info("start modifying mesh");
  
  int dim = _pModel->getNumRegions() ? 3 : 2;
  std::map<int, std::vector<GEntity*> > groups[4];
  _pModel->getPhysicalGroups(groups);
  std::map<int, std::vector<GEntity*> > &entmap = groups[dim];
  
  std::map<int, elementGroup> allG;  

  for (std::map<int, std::vector<GEntity*> >::iterator it = entmap.begin(); it!= entmap.end(); it++){
    std::vector<GEntity*> &ent = it->second;
    for (unsigned int i = 0; i < ent.size(); i++){
      for (unsigned int j = 0; j < ent[i]->getNumMeshElements(); j++){
        MElement *e = ent[i]->getMeshElement(j);
        std::vector<MElement*> enew;
        remesh(e, enew);
        for (int j=0; j< enew.size(); j++)
        {
          elementGroup& gr = allG[it->first];
          gr.insert(enew[j]);
        }
        

      }
    }
  }
  
  std::map<int, std::vector<GEntity*> > &entmap1D = groups[1];
  Msg::Info("size of entmap1D = %d",entmap1D.size());
  for (std::map<int, std::vector<GEntity*> >::iterator it = entmap1D.begin(); it!= entmap1D.end(); it++){
    std::vector<GEntity*> &ent = it->second;
    for (unsigned int i = 0; i < ent.size(); i++){
      for (unsigned int j = 0; j < ent[i]->getNumMeshElements(); j++){
        MElement *e = ent[i]->getMeshElement(j);
        elementGroup& gr = allG[it->first];
        gr.insert(e);          
      }
    }
  }
  
  std::map<int, std::vector<GEntity*> > &entmap0D = groups[0];
  Msg::Info("size of entmap0D = %d",entmap0D.size());
  for (std::map<int, std::vector<GEntity*> >::iterator it = entmap0D.begin(); it!= entmap0D.end(); it++){
    std::vector<GEntity*> &ent = it->second;
    for (unsigned int i = 0; i < ent.size(); i++){
      for (unsigned int j = 0; j < ent[i]->getNumMeshElements(); j++){
        MElement *e = ent[i]->getMeshElement(j);
        elementGroup& gr = allG[it->first];
        gr.insert(e);  
      }
    }
  }
  
  write_MSH2(allG,fname);
  
  Msg::Info("done modifying mesh");
};