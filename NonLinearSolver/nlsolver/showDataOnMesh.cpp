
//
// Author:  <Van Dung NGUYEN>, (C) 2020
//
// Copyright: See COPYING file that comes with this distribution
//
//

#include "showDataOnMesh.h"
#include "PView.h"
#include "quadratureRules.h"
#include "numericalFunctions.h"
showDataOnMesh::showDataOnMesh(const std::string meshFileName)
{
  _pModel = new GModel();
  _pModel->readMSH(meshFileName.c_str());
  _meshFileName = meshFileName;
  
  _gr = NULL;
  _part = NULL;
  };
showDataOnMesh::~showDataOnMesh()
{
  if (_pModel) delete _pModel;
  if (_gr) delete _gr;
  if (_part) delete _part;
};


void showDataOnMesh::dataOnPhysical(int dim, int phys, const std::string partMeshFile)
{
  if (_gr) delete _gr;
  _gr = new elementGroup(dim,phys);
  std::string filename = "partFile"+std::to_string(phys)+".msh";
  write_MSH2(_gr,partMeshFile.c_str());
  
  _part = new GModel();
  _part->readMSH(partMeshFile.c_str()); 
}

void showDataOnMesh::write_MSH2(elementGroup* g,  const std::string filename){

	FILE* fp = fopen(filename.c_str(),"w");
 	fprintf(fp, "$MeshFormat\n2.2 0 8\n$EndMeshFormat\n");

	fprintf(fp, "$Nodes\n");
	fprintf(fp, "%ld\n", g->vsize());
	for (elementGroup::vertexContainer::const_iterator it = g->vbegin(); it!= g->vend(); it++){
		MVertex* v = it->second;
		fprintf(fp, "%ld %f %f %f\n",v->getNum(),v->x(),v->y(),v->z());
	}
	fprintf(fp, "$EndNodes\n");
 	fprintf(fp, "$Elements\n");
	fprintf(fp, "%ld\n", g->size());

	
  for (elementGroup::elementContainer::const_iterator it = g->begin(); it!= g->end(); it++){
		MElement* ele = it->second;
		ele->writeMSH2(fp,2.2,0,0,1,11);
	};
	fprintf(fp, "$EndElements\n");
	fclose(fp);
};

void showDataOnMesh::addDataPoint(double x, double y, double z, double v)
{
  SPoint3 p(x,y,z);
  for (elementGroup::elementContainer::const_iterator ite = _gr->begin(); ite!= _gr->end(); ite++)
  {
    MElement* ele = ite->second;
    SPoint3 A0 = ele->getVertex(0)->point();
    SPoint3 A1 = ele->getVertex(1)->point();
    SPoint3 A2 = ele->getVertex(2)->point();
    
    SPoint3 pj = project(p,A0,A1,A2);
    double uvw[3];
    ele->xyz2uvw(pj.data(),uvw);
    if (ele->isInside(uvw[0],uvw[1],uvw[2]))
    {
      _GPData[ele].push_back(v);
      return;
    }
  }
  Msg::Error("x = %e ,y = %e, z = %e not found any element",x,y,z);
};

void showDataOnMesh::writeElementDataToFile(const std::string postFileName)
{
  auto mean = [](const std::vector<double>& val)
  {
    double m =0;
    for (int i=0; i< val.size(); i++)
    {
      m += val[i]/(double)val.size();
    }
    return m;
  };
  
  std::cout <<  "build view"<< std::endl;
  std::map<int, std::vector<double> > data, data2;
  for (elementGroup::elementContainer::const_iterator it = _gr->begin(); it != _gr->end(); ++it)
  {
    MElement *e=it->second;
    std::vector<double>& vec = data[e->getNum()];
    vec.resize(1);
    vec[0] = mean(_GPData[e]);
    
    std::vector<double>& vec2 = data2[e->getNum()];
    vec2.resize(1);
    vec2[0] = (double)_GPData[e].size();
    //Msg::Info("element = %d number of GPs = %d",e->getNum(),_fieldValue[e].size());
  }
  PView *pv = new PView ("Val", "ElementData", _part, data, 0.0);
  pv->write(postFileName,5,false);
  PView* pv2 = new PView ("Number of GPs", "ElementData", _part, data2, 0.0);
  pv2->write(postFileName,5,true);
  delete pv;
  delete pv2;
};


void showDataOnMesh::addNodeData(double x, double y, double z, double val, double tol)
{
  MVertex* v = NULL;
  SPoint3 pt(x,y,z);
  for (elementGroup::vertexContainer::const_iterator it = _gr->vbegin(); it != _gr->vend(); ++it)
  {
    MVertex* vv = it->second;
    SPoint3 pp = vv->point();
    if (pp.distance(pt) < tol)
    {
       v = vv;
       break;
    }
  }
  if (v == NULL)
  {
      Msg::Error("x = %e ,y = %e, z = %e not found any vertex",x,y,z);
  }
  else
  {
    if (_nodeData.find(v) != _nodeData.end())
    {
      Msg::Error("Point x = %e ,y = %e, z = %e is duplicated",x,y,z);
    }
    _nodeData[v].resize(1);
    _nodeData[v][0] = val;
  }
};

void showDataOnMesh::writeNodeDataToFile(const std::string postFileName)
{
  std::map<int, std::vector<double> > data;
  for (std::map<MVertex*, std::vector<double> >::const_iterator it = _nodeData.begin(); it != _nodeData.end(); ++it)
  {
    data[it->first->getNum()] = it->second;
  }
  PView *pv = new PView ("Val", "NodeData", _part, data, 0.0);
  pv->write(postFileName,5,false);
  delete pv;  
};
