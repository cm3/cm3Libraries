//
//
// Description: detection of the onset of falure
//
// Author:  <Van Dung NGUYEN>, (C) 2017
//
// Copyright: See COPYING file that comes with this distribution
//

#ifndef SWIG

#include "fullMatrix.h"
#include "SVector3.h"
#include "STensor43.h"

// if 2D problem, plan x-y must be used

namespace FailureDetection{
	inline void getLostSolutionUniquenessCriterionFollowingDirection(const int dim, const STensor43& L, const SVector3& N, double& cr){
		fullMatrix<double> Q;
		Q.resize(dim,dim);
		Q.setAll(0.);
		for (int i=0; i<dim; i++){
			for (int j=0; j<3; j++){
				for (int k=0; k<dim; k++){
					for (int l=0; l<3; l++){
						Q(i,k) += L(i,j,k,l)*N(j)*N(l);
					}
				}
			}
		}
		
		

		fullMatrix<double> eigVecRight(dim,dim);
		fullMatrix<double> eigVecLeft(dim,dim);

		fullVector<double> eigValReal(dim);
		fullVector<double> eigValImag(dim);

		Q.eig(eigValReal,eigValImag,eigVecLeft,eigVecRight,true);

		cr = eigValReal(0);
		 
		 //cr = Q.determinant();
	};
	
	// output is value and direction
	
	inline void getLostSolutionUniquenessCriterion(const int dim, const STensor43& L, SVector3& dir, double& cr){
		if (dim == 2){
			const double pi = 3.141592653589793238463;
			
			int N = 180;
			double dIn = pi/(double)N;
			
			for (int i=0; i<N; i++){
				double theta = (double)i*dIn;
				SVector3 n(0.);
				
				n(0) = cos(theta);
				n(1) = sin(theta);
				n(2) = 0.;				
				
				if (i==0){
					dir = n;
					FailureDetection::getLostSolutionUniquenessCriterionFollowingDirection(dim,L,dir,cr);
				}
				else{
					double minEg = 0.;
					FailureDetection::getLostSolutionUniquenessCriterionFollowingDirection(dim,L,n,minEg);
					if (minEg < cr){
						cr = minEg;
						dir = n;
					}
				}
			}
		}
		else{
			Msg::Error("getLostSolutionUniquenessCriterion is not implemenet for 3D");
		}
		
	};
};


#endif //SWIG
