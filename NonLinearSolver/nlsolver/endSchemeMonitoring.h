//
// C++ Interface: end scheme when some criterion
//
// Author:  <Van Dung Nguyen>, (C) 2017
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef ENDSCHEMEMONITORING_H_
# define ENDSCHEMEMONITORING_H_

#ifndef SWIG
#include "SVector3.h"
#include "ipstate.h"
class nonLinearMechSolver;
class archiveForce;
class IPField;
#endif //SWIG


class EndSchemeMonitoringBase{
  protected:
    bool _solverEndedByMonitoring;
    bool _reduceTimeStep;

  public:
    bool solverEndedByMornitoring() const {return _solverEndedByMonitoring;};
    bool reduceTimeStepByMornitoring() const {return _reduceTimeStep;};
    #ifndef SWIG
    EndSchemeMonitoringBase():_solverEndedByMonitoring(false),_reduceTimeStep(false){};
    EndSchemeMonitoringBase(const EndSchemeMonitoringBase& src):_solverEndedByMonitoring(src._solverEndedByMonitoring),_reduceTimeStep(src._reduceTimeStep){};
    virtual ~EndSchemeMonitoringBase(){}

    virtual void nextStep();
    virtual void resetToPreviousStep();

    void endSolverByMonitoring(const bool fl){_solverEndedByMonitoring = fl;};
    void reduceTimeStep(const bool fl) {_reduceTimeStep = fl;};

    virtual void checkEnd(nonLinearMechSolver* solver) = 0;
    virtual EndSchemeMonitoringBase* clone() const = 0;
    #endif //SWIG
};


class EndSchemeMonitoringWithZeroLoad : public EndSchemeMonitoringBase{
  protected:
    #ifndef SWIG
    double _tol;
    double _maxValCur,_maxValPrev; // cache
    SVector3 _normal;
    SVector3 _Tcur, _Tprev;
    #endif // SWIG

  public:
    EndSchemeMonitoringWithZeroLoad(const double tol, const double n1, const double n2, const double n3);
    #ifndef SWIG
    EndSchemeMonitoringWithZeroLoad(const EndSchemeMonitoringWithZeroLoad& src): EndSchemeMonitoringBase(src),
      _tol(src._tol),_normal(src._normal),_maxValCur(src._maxValCur),_maxValPrev(src._maxValPrev),_Tprev(src._Tprev),_Tcur(src._Tcur){};
    virtual ~EndSchemeMonitoringWithZeroLoad(){}
    virtual void nextStep();
    virtual void resetToPreviousStep();
    virtual void checkEnd(nonLinearMechSolver* solver);
    virtual EndSchemeMonitoringBase* clone() const {
        return new EndSchemeMonitoringWithZeroLoad(*this);
    };
    #endif //SWIG
};

class EndSchemeMonitoringWithPeakLoad : public EndSchemeMonitoringBase{
  protected:
    #ifndef SWIG
    double _tol;
    double _maxValCur,_maxValPrev; // cache
    SVector3 _normal;
    SVector3 _Tcur, _Tprev;
    #endif // SWIG

  public:
    EndSchemeMonitoringWithPeakLoad(const double tol, const double n1, const double n2, const double n3);
    #ifndef SWIG
    EndSchemeMonitoringWithPeakLoad(const EndSchemeMonitoringWithPeakLoad& src): EndSchemeMonitoringBase(src),
      _tol(src._tol),_normal(src._normal),_maxValCur(src._maxValCur),_maxValPrev(src._maxValPrev),_Tprev(src._Tprev),_Tcur(src._Tcur){};
    virtual ~EndSchemeMonitoringWithPeakLoad(){}
    virtual void nextStep();
    virtual void resetToPreviousStep();
    virtual void checkEnd(nonLinearMechSolver* solver);
    virtual EndSchemeMonitoringBase* clone() const {
        return new EndSchemeMonitoringWithPeakLoad(*this);
    };
    #endif //SWIG
};

class EndSchemeMonitoringWithZeroAverageValue : public EndSchemeMonitoringBase{
  protected:
    #ifndef SWIG
    double _tol;
    double _maxValCur, _maxValPrev; // cache
    double _curVal;
    double _previousVal;
    int _ipVal;
    #endif // SWIG

  public:
    EndSchemeMonitoringWithZeroAverageValue(const double tol, const int ipval);
    #ifndef SWIG
    EndSchemeMonitoringWithZeroAverageValue(const EndSchemeMonitoringWithZeroAverageValue& src): EndSchemeMonitoringBase(src),
      _tol(src._tol),_maxValCur(src._maxValCur),_maxValPrev(src._maxValPrev),_curVal(src._curVal),_previousVal(src._previousVal),_ipVal(src._ipVal){};
    virtual ~EndSchemeMonitoringWithZeroAverageValue(){}
    virtual void nextStep();
    virtual void resetToPreviousStep();
    virtual void checkEnd(nonLinearMechSolver* solver);
    virtual EndSchemeMonitoringBase* clone() const {
        return new EndSchemeMonitoringWithZeroAverageValue(*this);
    };
    #endif //SWIG
};

class EndSchemeMonitoringWithZeroForceOnPhysical : public EndSchemeMonitoringBase{
  protected:
    #ifndef SWIG
    archiveForce* _forceArchive;
    double _tol;
    double _maxValCur,_maxValPrev; // cache
    double _previousValue;
    double _currentValue;
		std::string onWhat;
		int physical;
		int comp;
	 #endif //SWIG

  public:
    EndSchemeMonitoringWithZeroForceOnPhysical(const double tol,std::string loc, const int numphys,const int c);
    #ifndef SWIG
    EndSchemeMonitoringWithZeroForceOnPhysical(const EndSchemeMonitoringWithZeroForceOnPhysical& src): EndSchemeMonitoringBase(src),
      _tol(src._tol),_maxValCur(0.),_maxValPrev(0.),onWhat(src.onWhat),physical(src.physical),comp(src.comp),_forceArchive(NULL),_previousValue(src._previousValue),
      _currentValue(src._currentValue){};
    virtual ~EndSchemeMonitoringWithZeroForceOnPhysical();
    virtual void nextStep();
    virtual void resetToPreviousStep();
    virtual void checkEnd(nonLinearMechSolver* solver);
    virtual EndSchemeMonitoringBase* clone() const {
        return new EndSchemeMonitoringWithZeroForceOnPhysical(*this);
    };
    #endif //SWIG
};

class EndSchemeMonitoringWithUpperBoundAverageValueIP : public EndSchemeMonitoringBase{
  protected:
    #ifndef SWIG
    double _boundValue;
		int _ipValue;
	 #endif //SWIG

  public:
    EndSchemeMonitoringWithUpperBoundAverageValueIP(const double boundValue, int ipVal);
    #ifndef SWIG
    EndSchemeMonitoringWithUpperBoundAverageValueIP(const EndSchemeMonitoringWithUpperBoundAverageValueIP& src): EndSchemeMonitoringBase(src),
      _boundValue(src._boundValue),_ipValue(src._ipValue){};
    virtual ~EndSchemeMonitoringWithUpperBoundAverageValueIP();
    virtual void nextStep();
    virtual void resetToPreviousStep();
    virtual void checkEnd(nonLinearMechSolver* solver);
    virtual EndSchemeMonitoringBase* clone() const {
        return new EndSchemeMonitoringWithUpperBoundAverageValueIP(*this);
    };
    #endif //SWIG
};



class EndSchemeMonitoringWithPeakForceOnPhysical : public EndSchemeMonitoringBase{
  protected:
    #ifndef SWIG
    archiveForce* _forceArchive;
    double _tol;
    double _maxValCur,_maxValPrev; // cache
    double _previousValue;
    double _currentValue;
		std::string onWhat;
		int physical;
		int comp;
	 #endif //SWIG

  public:
    EndSchemeMonitoringWithPeakForceOnPhysical(const double tol,std::string loc, const int numphys,const int c);
    #ifndef SWIG
    EndSchemeMonitoringWithPeakForceOnPhysical(const EndSchemeMonitoringWithPeakForceOnPhysical& src): EndSchemeMonitoringBase(src),
      _tol(src._tol),_maxValCur(0.),_maxValPrev(0.),onWhat(src.onWhat),physical(src.physical),comp(src.comp),_previousValue(src._previousValue),_forceArchive(NULL),
      _currentValue(src._currentValue){};
    virtual ~EndSchemeMonitoringWithPeakForceOnPhysical();
    virtual void nextStep();
    virtual void resetToPreviousStep();
    virtual void checkEnd(nonLinearMechSolver* solver);
    virtual EndSchemeMonitoringBase* clone() const {
        return new EndSchemeMonitoringWithPeakForceOnPhysical(*this);
    };
    #endif //SWIG
};

class EndSchemeMonitoringWithNumberOfCoalescenceIPs : public EndSchemeMonitoringBase{
  protected:
    int _maxNumLarger;

  public:
    EndSchemeMonitoringWithNumberOfCoalescenceIPs(const int maxNumber);
    #ifndef SWIG
    EndSchemeMonitoringWithNumberOfCoalescenceIPs(const EndSchemeMonitoringWithNumberOfCoalescenceIPs& src): EndSchemeMonitoringBase(src),
          _maxNumLarger(src._maxNumLarger){}
    virtual ~EndSchemeMonitoringWithNumberOfCoalescenceIPs(){};
    virtual void nextStep();
    virtual void resetToPreviousStep();
    virtual void checkEnd(nonLinearMechSolver* solver);
    virtual EndSchemeMonitoringBase* clone() const {return new EndSchemeMonitoringWithNumberOfCoalescenceIPs(_maxNumLarger);};
    #endif //SWIG
};

class EndSchemeMonitoringWithNumberOfCoalescenceIPsNotGreaterThan : public EndSchemeMonitoringBase{
  protected:
    int _maxNumLarger;

  public:
    EndSchemeMonitoringWithNumberOfCoalescenceIPsNotGreaterThan(const int maxNumber);
    #ifndef SWIG
    EndSchemeMonitoringWithNumberOfCoalescenceIPsNotGreaterThan(const EndSchemeMonitoringWithNumberOfCoalescenceIPsNotGreaterThan& src): EndSchemeMonitoringBase(src),
          _maxNumLarger(src._maxNumLarger){}
    virtual ~EndSchemeMonitoringWithNumberOfCoalescenceIPsNotGreaterThan(){};
    virtual void nextStep();
    virtual void resetToPreviousStep();
    virtual void checkEnd(nonLinearMechSolver* solver);
    virtual EndSchemeMonitoringBase* clone() const {return new EndSchemeMonitoringWithNumberOfCoalescenceIPsNotGreaterThan(_maxNumLarger);};
    #endif //SWIG
};

class ReduceTimeStepWithNumberOfCoalescenceIPs : public EndSchemeMonitoringBase{
  protected:
    int _maxNumLarger;

  public:
    ReduceTimeStepWithNumberOfCoalescenceIPs(const int maxNumber);
    #ifndef SWIG
    ReduceTimeStepWithNumberOfCoalescenceIPs(const ReduceTimeStepWithNumberOfCoalescenceIPs& src): EndSchemeMonitoringBase(src),
          _maxNumLarger(src._maxNumLarger){}
    virtual ~ReduceTimeStepWithNumberOfCoalescenceIPs(){};
    virtual void nextStep();
    virtual void resetToPreviousStep();
    virtual void checkEnd(nonLinearMechSolver* solver);
    virtual EndSchemeMonitoringBase* clone() const {return new ReduceTimeStepWithNumberOfCoalescenceIPs(_maxNumLarger);};
    #endif //SWIG
};


class ReduceTimeStepWithLargeCoalescenceOffset : public EndSchemeMonitoringBase{
  protected:
    double _maxVal;
    int _maxNumLarger;

  public:
    ReduceTimeStepWithLargeCoalescenceOffset(const double maximalValue, const int maxNumber);
    #ifndef SWIG
    ReduceTimeStepWithLargeCoalescenceOffset(const ReduceTimeStepWithLargeCoalescenceOffset& src): EndSchemeMonitoringBase(src),
          _maxVal(src._maxVal),_maxNumLarger(src._maxNumLarger){}
    virtual ~ReduceTimeStepWithLargeCoalescenceOffset(){};
    virtual void nextStep();
    virtual void resetToPreviousStep();
    virtual void checkEnd(nonLinearMechSolver* solver);
    virtual EndSchemeMonitoringBase* clone() const {return new ReduceTimeStepWithLargeCoalescenceOffset(_maxVal,_maxNumLarger);};
    #endif //SWIG
};


class ReduceTimeStepWithLargeYieldOffset : public EndSchemeMonitoringBase{
  protected:
    double _maxVal;
    int _maxNumLarger;

  public:
    ReduceTimeStepWithLargeYieldOffset(const double maximalValue, const int maxNumber);
    #ifndef SWIG
    ReduceTimeStepWithLargeYieldOffset(const ReduceTimeStepWithLargeYieldOffset& src): EndSchemeMonitoringBase(src),
          _maxVal(src._maxVal),_maxNumLarger(src._maxNumLarger){}
    virtual ~ReduceTimeStepWithLargeYieldOffset(){};
    virtual void nextStep();
    virtual void resetToPreviousStep();
    virtual void checkEnd(nonLinearMechSolver* solver);
    virtual EndSchemeMonitoringBase* clone() const {return new ReduceTimeStepWithLargeYieldOffset(_maxVal,_maxNumLarger);};
    #endif //SWIG
};


class ReduceTimeStepWithMaximalNumberOfErodedBulkElements : public EndSchemeMonitoringBase{
  protected:
    int _maxAllowedNewlyErodedElement;
    int _numberOfReductions; // number of time step reduction
    int _allowableNumberOfReductions;
    double _lastTimeStep;
    bool _keepInitialTimeStep; //keep time step before reduction
    nonLinearMechSolver* _solver;

  public:
    ReduceTimeStepWithMaximalNumberOfErodedBulkElements(const int maxnum, int maxReduction=3, bool keepInitialTimeSTep=false);
    #ifndef SWIG
    ReduceTimeStepWithMaximalNumberOfErodedBulkElements(const ReduceTimeStepWithMaximalNumberOfErodedBulkElements& src): EndSchemeMonitoringBase(src),
          _maxAllowedNewlyErodedElement(src._maxAllowedNewlyErodedElement),
          _numberOfReductions(src._numberOfReductions),
          _allowableNumberOfReductions(src._allowableNumberOfReductions),
          _lastTimeStep(src._lastTimeStep),
          _solver(NULL),
          _keepInitialTimeStep(src._keepInitialTimeStep){}
    virtual ~ReduceTimeStepWithMaximalNumberOfErodedBulkElements(){}
    virtual void nextStep();
    virtual void resetToPreviousStep();
    virtual void checkEnd(nonLinearMechSolver* solver);
    virtual EndSchemeMonitoringBase* clone() const {return new ReduceTimeStepWithMaximalNumberOfErodedBulkElements(_maxAllowedNewlyErodedElement, _allowableNumberOfReductions, _keepInitialTimeStep);};
    #endif // SWIG

};


class ReduceTimeStepWithMaximalNumberOfBrokenElements : public EndSchemeMonitoringBase{
  protected:
    int _maxAllowedNewlyBrokenElement;

  public:
    ReduceTimeStepWithMaximalNumberOfBrokenElements(const int maxnum);
    #ifndef SWIG
    ReduceTimeStepWithMaximalNumberOfBrokenElements(const ReduceTimeStepWithMaximalNumberOfBrokenElements& src): EndSchemeMonitoringBase(src),
          _maxAllowedNewlyBrokenElement(src._maxAllowedNewlyBrokenElement){}
    virtual ~ReduceTimeStepWithMaximalNumberOfBrokenElements(){}
    virtual void nextStep();
    virtual void resetToPreviousStep();
    virtual void checkEnd(nonLinearMechSolver* solver);
    virtual EndSchemeMonitoringBase* clone() const {return new ReduceTimeStepWithMaximalNumberOfBrokenElements(_maxAllowedNewlyBrokenElement);};
    #endif // SWIG

};

class EndSchemeWithDissipationOnElementFromActiveToInactive : public EndSchemeMonitoringBase{
  #ifndef SWIG
  protected:
    int _elementNumber;
    const AllIPState::ipstateElementContainer* vips;
  #endif // SWIG
  public:
    EndSchemeWithDissipationOnElementFromActiveToInactive(const int elenum);
    #ifndef SWIG
    EndSchemeWithDissipationOnElementFromActiveToInactive(const EndSchemeWithDissipationOnElementFromActiveToInactive& src);
    virtual ~EndSchemeWithDissipationOnElementFromActiveToInactive(){}
    virtual void nextStep();
    virtual void resetToPreviousStep();
    virtual void checkEnd(nonLinearMechSolver* solver);
    virtual EndSchemeMonitoringBase* clone() const {return new EndSchemeWithDissipationOnElementFromActiveToInactive(_elementNumber);};
    #endif // SWIG
};

class MultipleEndSchemeMonitoring : public EndSchemeMonitoringBase{
  #ifndef SWIG
  std::vector<EndSchemeMonitoringBase*> _allComp;
  bool _operation; // and, or (by default)
  #endif // SWIG
  protected:

  public:
    MultipleEndSchemeMonitoring(const bool fl=true);
    void setSchemeMonitoring(const EndSchemeMonitoringBase& c);
    #ifndef SWIG
    MultipleEndSchemeMonitoring(const MultipleEndSchemeMonitoring& src);
    virtual ~MultipleEndSchemeMonitoring();
    virtual void nextStep();
    virtual void resetToPreviousStep();
    virtual void checkEnd(nonLinearMechSolver* solver);
    virtual EndSchemeMonitoringBase* clone() const {return new MultipleEndSchemeMonitoring(*this);};
    #endif // SWIG
};
#endif // ENDSCHEMEMONITORING_H_
