#include<cstdlib>
#ifndef TESTCHECK_H
#define TESTCHECK_H
class TestCheck
{
 public:
  TestCheck();

  virtual ~TestCheck();
  void
  equal(double ref, double cur, double tol=1e-3) const;

  void
  equal(int ref, int cur) const;

};
#endif 
