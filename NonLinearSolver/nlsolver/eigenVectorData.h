//
//
// Description: store the eigenvector data
//
// Author:  <Van Dung NGUYEN>, (C) 2013
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef EIGENVECTORDATA_H_
#define EIGENVECTORDATA_H_

#include "eigenSolver.h"
#include "staticDofManager.h"


class eigenVectorData {
	protected:
		eigenSolver* _eig;
		nlsDofManager* _pAssembler;
		int _pertNum; // number of mode of disturbation
		double _pertVal; // degree of disturbation


	public:

		eigenVectorData(eigenSolver* e, nlsDofManager* p, int num, double val): _eig(e),_pAssembler(p), _pertNum(num),_pertVal(val){};
		virtual ~eigenVectorData(){};
		void get(const Dof& D, double& udof) const{
			udof = 0.;
			if (_pAssembler->isFixed(D)){
				double vall = 0.;
				_pAssembler->getFixedDofValue(D,vall);
				udof = vall;
				return;
			};

      int comp = _pAssembler->getDofNumber(D);
			if (comp>=0){
				for (int i=0; i<_pertNum; i++){
					std::complex<double> val = _eig->getEigenVectorComp(i,comp);
 	     		udof += _pertVal*val.real();
				};
			}
			else{
				std::map<const Dof, const DofAffineConstraint<double>* > constraints;
        _pAssembler->getAllLinearConstraints(constraints);
 	     	std::map<const Dof, const DofAffineConstraint<double>*>::iterator it =constraints.find(D);
 				if (it != constraints.end()){
 					for (unsigned i = 0; i < (it->second)->linear.size(); i++){
						double tmp = 0;
 						this->get(((it->second)->linear[i]).first, tmp);
 							udof += ((it->second)->linear[i]).second * tmp;
 					}
 				}

			};

		};

		void get(std::vector<Dof> &R, std::vector<double> &disp) const{
	    double du;
    	for (int i=0; i<R.size(); i++){
      	this->get(R[i],du);
      	disp.push_back(du);
    	};
 		};


};

#endif // EIGENVECTORDATA_H_
