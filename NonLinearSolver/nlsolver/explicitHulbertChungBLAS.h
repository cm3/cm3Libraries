//
// Description: explicit hulbert chung system based on BLAS implementation
//              with default operation if BLAS is not installed
//
// Author:  <Gauthier BECKER>, (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef EXPLICITHULBERTCHUNGBLAS_H_
#define EXPLICITHULBERTCHUNGBLAS_H_
#include "explicitHulbertChung.h"
#include <math.h>
#include "restartManager.h"

#if defined(HAVE_BLAS)
  // extern blas function declaration
  #if !defined(F77NAME)
   #define F77NAME(x) (x##_)
  #endif // F77NAME
  extern "C" {
    void F77NAME(daxpy)(int *n, double *alpha, double *x, int *incx, double *y, int *incy);
    void F77NAME(dscal)(int *n, double *alpha,double *x,  int *incx);
    void F77NAME(dcopy)(int *n, double *x,int *incx, double *y, int *incy);
    double F77NAME(ddot)(int *n,double *x,int *incx, double *y, int *incy);
    int F77NAME(idamax)(int *n,double *x,int *incx);
  }
#endif // HAVE_BLAS

// print for debug
void printBLASVector(double *a,int vsize,const char *vname){
  double val;
  for(int i=0; i<vsize;i++){
    val = a[i];
    printf("%s comp %d %e\n",vname,i,val);
  }
  printf("---\n");
}

// 4 vectors are necessary per step and two step are kept
// current and next. Creation of a class regrouping the four vector
template<class scalar>
class explicitStepBlas{
 protected:
  bool _benson;
  int _nbRows;
 public : // Direct access by the system
  scalar *_Fext;
  scalar *_Fint;
  scalar *_x;
  scalar *_xdot;
  scalar *_xddot;
  scalar *_Psi;
  explicitStepBlas(bool benson=false): _Fext(NULL), _Fint(NULL), _x(NULL), _xdot(NULL), _xddot(NULL), _Psi(NULL), _benson(benson), _nbRows(0){}
  void clear(){
   if(_Fext != NULL) delete[] _Fext;
   if(_Fint != NULL) delete[] _Fint;
   if(_x != NULL) delete[] _x;
   if(_xdot != NULL) delete[] _xdot;
   if(_xddot != NULL) delete[] _xddot;
   if(_Psi !=NULL) delete[] _Psi;
   _nbRows = 0;
  }

  ~explicitStepBlas(){this->clear();}

  void allocate(int nbRows){
    _nbRows = nbRows;
    _Fext = new scalar[nbRows];
    _Fint = new scalar[nbRows];
    _x = new scalar[nbRows];
    _xdot = new scalar[nbRows];
    _xddot = new scalar[nbRows];
    if(_benson)
    {
      _Psi = new scalar[nbRows];
      // initialize random values
      for(int i=0;i<nbRows;i++){ // double only !!!
        _Psi[i] = frand(0.,1.);
      }
    }
   #if defined(HAVE_BLAS)
    // all values to 0
    int INCX = 1;
    double alpha = 0.;
    F77NAME(dscal)(&nbRows,&alpha,_Fext,&INCX);
    F77NAME(dscal)(&nbRows,&alpha,_Fint,&INCX);
    F77NAME(dscal)(&nbRows,&alpha,_x,&INCX);
    F77NAME(dscal)(&nbRows,&alpha,_xdot,&INCX);
    F77NAME(dscal)(&nbRows,&alpha,_xddot,&INCX);
   #else
    for(int i=0;i<nbRows;i++){
      _Fext[i]=0.;
      _Fint[i]=0.;
      _x[i]=0.;
      _xdot[i]=0.;
      _xddot[i]=0.;
    }
   #endif // HAVE_BLAS
  }
  explicitStepBlas& operator=(const explicitStepBlas &other) // same size !!
  {
   #if defined(HAVE_BLAS)
    int INCX = 1;
    double alpha = 0.;
    F77NAME(dcopy)(&_nbRows,other._Fext,&INCX,_Fext,&INCX);
    F77NAME(dcopy)(&_nbRows,other._Fint,&INCX,_Fint,&INCX);
    F77NAME(dcopy)(&_nbRows,other._x,&INCX,_x,&INCX);
    F77NAME(dcopy)(&_nbRows,other._xdot,&INCX,_xdot,&INCX);
    F77NAME(dcopy)(&_nbRows,other._xddot,&INCX,_xddot,&INCX);
   #else
    for(int i=0;i<_nbRows;i++){
      _Fext[i]=other._Fext[i];
      _Fint[i]=other._Fint[i];
      _x[i]=other._x[i];
      _xdot[i]=other._xdot[i];
      _xddot[i]=other._xddot[i];
    }
   #endif // HAVE_BLAS
    return *this;
  }

  void restart()
  {
    bool tbenson = _benson;
    restartManager::restart(tbenson);
    if(tbenson != _benson) _Psi = new scalar[_nbRows];

    int tnrows=_nbRows; // Check the number of rows to see if the system has still the same size...
    restartManager::restart(tnrows);
    if(_nbRows != tnrows)
    {
      Msg::Error("The restarted system has a size different from the saved one! Cannot proceed further.");
    }
    restartManager::restart(_Fext,_nbRows);
    restartManager::restart(_Fint,_nbRows);
    restartManager::restart(_x,_nbRows);
    restartManager::restart(_xdot,_nbRows);
    restartManager::restart(_xddot,_nbRows);
    if(tbenson) // has been saved so must be read even if _benson is false now
      restartManager::restart(_Psi,_nbRows);
  }
};

template<class scalar>
class explicitHulbertChungBlas : public explicitHulbertChung<scalar>{
 public :

 protected:
  bool _isAllocated, _whichStep, _imassAllocated,_dynamicRelaxation, _timeStepByBenson;
  double _alpham, _beta,_gamma; // parameters of scheme
  double _timeStep; // value of time step
  int _nbRows; // To know the system size.
  // values used by solve function
  double _oneDivbyOneMinusAlpham,_minusAlphamDivbyOneMinusAlpham;
  double _oneMinusgammaDeltat,_gammaDeltat,_Deltat2halfMinusbeta,_Deltat2beta;
  explicitStepBlas<scalar> *_currentStep, *_previousStep, *_step1, *_step2;
  scalar *_M; // mass matrix
  scalar *_invM; // inverse of mass matrix
  mutable scalar *_v2; // square of velocitie (kinetic energy) // used as Fext n+1 + Fext n for external work
  scalar *_b; // tempory value _Fext - _Fint
  mutable scalar *_S; // for dynamicRelaxation (diagonal stiffness approximation) // used as x n+1 - x n for external work

  // function to invert mass matrix
  void invertMassMatrix(){
    if(_isAllocated){
      // no blas function ??
      for(int i=0;i<_nbRows;i++)
        _invM[i] = 1./_M[i];
      _imassAllocated=true;
    }
    else
      Msg::Error("Can't compute invert of mass matrix because the system is not allocated");
  }
  virtual int systemSize()const{return _nbRows;}
 public:
  explicitHulbertChungBlas(double alpham=0., double beta=0.,
                        double gamma=0.5,bool benson=false, bool dynrel=false) : _isAllocated(false), _whichStep(true),
                                                        _alpham(alpham), _beta(beta), _gamma(gamma),
                                                        _timeStep(0.), _nbRows(0), _imassAllocated(false),
                                                        _dynamicRelaxation(dynrel), _timeStepByBenson(benson){
    _oneDivbyOneMinusAlpham = 1./(1.-_alpham);
    _minusAlphamDivbyOneMinusAlpham = - _alpham * _oneDivbyOneMinusAlpham;
    _oneMinusgammaDeltat = (1.-_gamma)*_timeStep;
    _gammaDeltat = _gamma*_timeStep;
    _Deltat2halfMinusbeta = _timeStep*_timeStep *(0.5-_beta);
    _Deltat2beta = _timeStep * _timeStep * _beta;
    _step1 = new explicitStepBlas<scalar>(_timeStepByBenson);
    _step2 = new explicitStepBlas<scalar>(_timeStepByBenson);
    _currentStep = _step1;
    _previousStep = _step2;
  }
  ~explicitHulbertChungBlas(){delete _step1; delete _step2;}

  void nextStep(){
    if(_whichStep){
      _currentStep = _step2;
      _previousStep = _step1;
      _whichStep =false;
    }
    else{
     _currentStep = _step1;
     _previousStep = _step2;
     _whichStep = true;
    }
  }

  virtual void clear(){
    if(_isAllocated){
      delete[] _M;
      delete[] _invM;
      delete[] _v2;
      delete[] _b;
      delete[] _S;
      _step1->clear();
      _step2->clear();
      _nbRows=0;
    }
    _isAllocated=false;
    _imassAllocated=false;
  }
  // Or compute directly the time step here ??
  virtual void setTimeStep(const double dt){
    _timeStep = dt;
    // update variables which depends on _timeStep
    _oneMinusgammaDeltat = (1.-_gamma)*_timeStep;
    _gammaDeltat = _gamma*_timeStep;
    _Deltat2halfMinusbeta = _timeStep*_timeStep *(0.5-_beta);
    _Deltat2beta = _timeStep * _timeStep * _beta;

  }
  virtual double getTimeStep()const{return _timeStep;}
  virtual bool isAllocated() const { return _isAllocated; }
  virtual void allocate(int nbRows){
    clear();
    _M = new scalar[nbRows];
    _invM = new scalar[nbRows];
    _v2 = new scalar[nbRows];
    _b = new scalar [nbRows];
    _S = new scalar [nbRows]; // allocated as used for external work computation
    _step1->allocate(nbRows);
    _step2->allocate(nbRows);
    _isAllocated = true;
    _nbRows=nbRows;
    this->zeroMatrix();
    this->zeroRightHandSide();
  }
  // get the value of diagonalized mass matrix col is not used
  virtual void getFromMatrix(int row, int col, scalar &val) const{
    val = _M[row];
  }
  virtual void addToRightHandSide(int row, const scalar &val, int ith=0)
  {
    _currentStep->_Fext[row] += val;
    #ifdef _DEBUG
      Msg::Warning("addToRightHandSide is deprecated for explicitHulbertChung system. Please use addToRightHandSidePlus or addToRightHandSideMinus");
    #endif
  }
  virtual void addToRightHandSidePlus(int row, const scalar &val)
  {
    _currentStep->_Fext[row] += val;
  }
  virtual void addToRightHandSideMinus(int row, const scalar &val)
  {
    _currentStep->_Fint[row] += val;
  }

  virtual void getFromRightHandSide(int row, scalar &val) const
  {
    val = _currentStep->_Fext[row] - _currentStep->_Fint[row];
  }

  virtual void getFromRightHandSidePlus(int row, scalar &val) const
  {
    val = _currentStep->_Fext[row];
  }

  virtual void getFromRightHandSideMinus(int row, scalar &val) const
  {
    val = _currentStep->_Fint[row];
  }
  virtual double normInfRightHandSide() const
  {
    double nor;
   #if defined(HAVE_BLAS)
    int NR = _nbRows, INCX = 1, INCY = 1;
    double alpha = -1;
    F77NAME(dcopy)(&NR,_currentStep->_Fext,&INCX,_b,&INCY);
    F77NAME(daxpy)(&NR,&alpha,_currentStep->_Fint,&INCX,_b,&INCY);
    int indmax = F77NAME(idamax)(&NR,_b,&INCX) - 1; // idamax first index = 1 !!
    nor = fabs(_b[indmax]);
   #else
    for(int i=0;i<_nbRows;i++){
      _b[i] = _currentStep->_Fext[i] - _currentStep->_Fint[i];
    }
    nor = _b[0];
    if(nor < 0) nor = -nor;
    double temp;
    for(int i=1;i<_nbRows;i++){
      temp = _b[i];
      if(temp < 0) temp = -temp;
      if(temp > nor) nor = temp;
    }
   #endif // HAVE_BLAS

   #if defined(HAVE_MPI)
    if(Msg::GetCommSize() != 1)
    {
      double norMPI; // WHY MPI_IN_PLACE DOESN'T WORK
      MPI_Allreduce(&nor,&norMPI,1,MPI_DOUBLE,MPI_MAX,MPI_COMM_WORLD);
      nor = norMPI;
    }
   #endif //HAVE_MPI
     return nor;
  }
  virtual double norm0Inf() const{
    double norFext;
    double norFint;
   #if defined(HAVE_BLAS)
    int INCX = 1;
    int NR = _nbRows;
    int indFext = F77NAME(idamax)(&NR,_currentStep->_Fext,&INCX)-1; // idamax first index = 1 !!
    int indFint = F77NAME(idamax)(&NR,_currentStep->_Fint,&INCX)-1; // idamax first index = 1 !!
    norFext = fabs(_currentStep->_Fext[indFext]);
    norFint = fabs(_currentStep->_Fint[indFint]);
   #else
    norFext = _currentStep->_Fext[0];
    if(norFext < 0) norFext = - norFext;
    norFint = _currentStep->_Fint[0];
    if(norFint < 0) norFint = - norFint;
    double t_ext, t_int;
    for(int i=1;i<_nbRows;i++){
      t_ext = _currentStep->_Fext[i];
      t_int = _currentStep->_Fint[i];
      if(t_ext < 0) t_ext = -t_ext;
      if(t_int < 0) t_int = -t_int;
      if(t_ext > norFext) norFext = t_ext;
      if(t_int > norFint) norFint = t_int;
    }
   #endif // HAVE_BLAS
   #if defined(HAVE_MPI)
   if(Msg::GetCommSize() > 1)
   {
     double nn[2] = {norFext,norFint};
     double nntmp[2];
     MPI_Allreduce(nn,nntmp,2,MPI_DOUBLE,MPI_MAX,MPI_COMM_WORLD);
     norFext = nntmp[0];
     norFint = nntmp[1];
   }
   #endif // HAVE_MPI
    return norFext + norFint;
  }

  // Add to mass matrix (which is diagonalized)
  virtual void addToMatrix(int row, int col, const scalar &val){
    Msg::Error("No stiffness matrix for an explicit newmark scheme");
  }
  virtual void addToMatrix(int row, int col, const scalar &val, const nonLinearSystemBase::whichMatrix wm)
  {
    if(wm == nonLinearSystemBase::stiff){
      this->addToMatrix(row,col,val);
    }
    else if( wm == nonLinearSystemBase::mass){
      _M[row] += val;
    }
    else{
     Msg::Error("stiff and mass are the only possible matrix choice");
    }

  }

  virtual void getFromSolution(int row, scalar &val) const
  {
    val = _currentStep->_x[row];
  }

  virtual void getFromSolution(int row, scalar &val, const nonLinearBoundaryCondition::whichCondition wv) const
  {
    switch(wv){
     case nonLinearBoundaryCondition::position:
       this->getFromSolution(row,val);
     break;
     case nonLinearBoundaryCondition::velocity:
      val = _currentStep->_xdot[row];
     break;
     case nonLinearBoundaryCondition::acceleration:
      val = _currentStep->_xddot[row];
     break;
     default:
       Msg::Error("Impossible to get value from solution. Only possible choices position, velocity, acceleration ");
    }

  }

  virtual void zeroMatrix(){
    if (_isAllocated) {
#if defined(HAVE_BLAS)
      double alpha=0.;
      int INCX = 1;
      F77NAME(dscal)(&_nbRows, &alpha,_M,&INCX);
#else
      for(int i=0;i<_nbRows;i++){
        _M[i]=0.;
      }
#endif // HAVE_BLAS
    }
  }

  virtual void zeroRightHandSide()
  {
    if (_isAllocated) {
#if defined(HAVE_BLAS)
      double alpha = 0.;
      int INCX = 1;
      F77NAME(dscal)(&_nbRows, &alpha,_currentStep->_Fext,&INCX);
      F77NAME(dscal)(&_nbRows, &alpha,_currentStep->_Fint,&INCX);
#else
      for(int i=0;i<_nbRows;i++){
        _currentStep->_Fext[i]=0.;
        _currentStep->_Fint[i]=0.;
      }
#endif // HAVE_BLAS
    }
  }

  int systemSolve(){

    // check if the mass matrix is computed or not
    if(!_imassAllocated) this->invertMassMatrix();

#if defined(HAVE_BLAS)
   // compute _b first = _Fext - _Fint (PREVIOUS STEP !!!!!)
    int INCX = 1, INCY = 1;
    double alp = -1;
    F77NAME(dcopy)(&_nbRows,_previousStep->_Fext,&INCX,_b,&INCY);
    F77NAME(daxpy)(&_nbRows,&alp,_previousStep->_Fint,&INCX,_b,&INCY);

    // NO BLAS FUNCTION ??
    for(int i=0;i<_nbRows;i++){
      _currentStep->_xddot[i] = _invM[i]*_b[i];
    }
    // accelerations n+1
    F77NAME(dscal)(&_nbRows,&_oneDivbyOneMinusAlpham,_currentStep->_xddot,&INCX);
    F77NAME(daxpy)(&_nbRows,&_minusAlphamDivbyOneMinusAlpham,_previousStep->_xddot,&INCX,_currentStep->_xddot,&INCY);

    // velocities n+1
    F77NAME(dcopy)(&_nbRows,_previousStep->_xdot,&INCX,_currentStep->_xdot,&INCY);
    F77NAME(daxpy)(&_nbRows,&_oneMinusgammaDeltat,_previousStep->_xddot,&INCX,_currentStep->_xdot,&INCY);
    F77NAME(daxpy)(&_nbRows,&_gammaDeltat,_currentStep->_xddot,&INCX,_currentStep->_xdot,&INCY);
    /* dynamic relaxation */
    if(_dynamicRelaxation)
    {
      double dampingFactor = this->dynamicRelaxationFactor();
      F77NAME(dscal)(&_nbRows,&dampingFactor,_currentStep->_xdot,&INCX);
    }

    // positions n+1
    F77NAME(dcopy)(&_nbRows,_previousStep->_x,&INCX,_currentStep->_x,&INCY);
    F77NAME(daxpy)(&_nbRows,&_timeStep,_previousStep->_xdot,&INCX,_currentStep->_x,&INCY);
    F77NAME(daxpy)(&_nbRows,&_Deltat2halfMinusbeta,_previousStep->_xddot,&INCX,_currentStep->_x,&INCY);
    F77NAME(daxpy)(&_nbRows,&_Deltat2beta,_currentStep->_xddot,&INCX,_currentStep->_x,&INCY);
#else
 // default implementation
    double fextMinusfint;

    /* dynamic relaxation */
    double dampingFactor=1.;
    if(_dynamicRelaxation)
    {
      dampingFactor = this->dynamicRelaxationFactor();
    }

    for(int i=0;i<_nbRows;i++){

      // accelerations n+1
      fextMinusfint = _previousStep->_Fext[i]-_previousStep->_Fint[i];
      _currentStep->_xddot[i] = _oneDivbyOneMinusAlpham*_invM[i]*fextMinusfint + _minusAlphamDivbyOneMinusAlpham * _previousStep->_xddot[i];

      // velocities n+1
      _currentStep->_xdot[i] = _previousStep->_xdot[i] + _oneMinusgammaDeltat*_previousStep->_xddot[i]
                             + _gammaDeltat*_currentStep->_xddot[i];
      if(_dynamicRelaxation) _currentStep->_xdot[i]*=dampingFactor;

      // positions n+1
      _currentStep->_x[i] = _previousStep->_x[i] + _timeStep * _previousStep->_xdot[i]
                          + _Deltat2halfMinusbeta * _previousStep->_xddot[i] + _Deltat2beta * _currentStep->_xddot[i];
    }

#endif // HAVE_BLAS
    return 1;
  }

    // Specific functions (To put initial conditions)
    // set on current step a next step operation is necessary after the prescribtion of initial value step0->step1
    void setInitialCondition(int row, scalar val, const typename nonLinearBoundaryCondition::whichCondition wc){ //CANNOT PASS VAL BY REF WHY ??
      switch(wc){
       case nonLinearBoundaryCondition::position:
        _currentStep->_x[row] = val;
        break;
       case nonLinearBoundaryCondition::velocity:
        _currentStep->_xdot[row] = val;
        break;
       case nonLinearBoundaryCondition::acceleration:
        _currentStep->_xddot[row] = val;
        break;
       default:
        Msg::Warning("Invalid initial conditions");
      }
    }

    // Get mass of system
    virtual double getSystemMass(){
      double m;
      m=0.;
      for(int i=0;i<_nbRows;i++){
        m+=_M[i];
      }
      return m;
    }
  // function to get the kinetic energy
  double getKineticEnergy(const int syssize) const{
    if(isAllocated()){
      double ener;
     #if defined(HAVE_BLAS)
      int INCX = 1, INCY = 1;
      int ssize = syssize;
      for(int i=0;i<_nbRows;i++)
       _v2[i] = _currentStep->_xdot[i]*_currentStep->_xdot[i];       // NO BLAS FUNCTION FOR THIS ??
      ener = F77NAME(ddot)(&ssize,_M,&INCX,_v2,&INCY); // Don't take account rigid contact GC
     #else
      ener=0.;
      for(int i=0;i<syssize;i++)
        ener+=_M[i]*_currentStep->_xdot[i]*_currentStep->_xdot[i];
     #endif // HAVE_BLAS
      return 0.5*ener;
    }
    else return 0.;
  }
  virtual double getExternalWork(const int syssize) const{
    if(isAllocated()){
      double wext;
     #if defined(HAVE_BLAS)
      int INCX = 1, INCY = 1;
      double one = 1 , mone = -1;
      int nbr = this->_nbRows;
      int ssize = syssize;
      F77NAME(dcopy)(&nbr,_previousStep->_Fext,&INCX,_v2,&INCX);
      F77NAME(daxpy)(&nbr,&one,_currentStep->_Fext,&INCX,_v2,&INCX);
      F77NAME(dcopy)(&nbr,_currentStep->_x,&INCX,_S,&INCX);
      F77NAME(daxpy)(&nbr,&mone,_previousStep->_x,&INCX,_S,&INCX);
      wext = F77NAME(ddot)(&ssize,_v2,&INCX,_S,&INCY);
     #else
      wext=0.;
      for(int i=0;i<syssize;i++)
        wext+=(_currentStep->_Fext[i]+_previousStep->_Fext[i])*(_currentStep->_x[i]-_previousStep->_x[i]);
     #endif // HAVE_BLAS
      return 0.5*wext;
    }
    else return 0.;
  }
  virtual void resetUnknownsToPreviousTimeStep()
  {
    (*_currentStep) = (*_previousStep);
  }
  #if defined(HAVE_MPI)
  virtual scalar* getArrayIndexPreviousRightHandSidePlusMPI(const int index_)
  {
     return &(_previousStep->_Fext[index_]);
  }
//  virtual void setPreviousRightHandSidePlusMPI(const int row, const scalar &val)
//  {
//    _previousStep->_Fext[row] = val;
//  }
  #endif // HAVE_MPI
  virtual void setDynamicRelaxation(const bool dynrel)
  {
    _dynamicRelaxation = dynrel;
  }
  virtual double dynamicRelaxationFactor()
  {
    /* factor = (1+2*c*_timeStep)^{-1} */
    // compute c = \sqrt{\frac{x^T_n F^{int}_N}{x^T_n M x_n}}
    // for MPI compute local value and then communicate the result
    double xFint, xMx;
   #if defined(HAVE_BLAS)
    int INCX = 1;
    xFint = F77NAME(ddot)(&_nbRows,_previousStep->_x,&INCX,_previousStep->_Fint,&INCX);
   #else
     xFint = 0.;
     for(int i=0;i<_nbRows;i++)
     {
       xFint += _previousStep->_x[i]*_previousStep->_Fint[i];
     }
   #endif // HAVE_BLAS
    // MPI comm
   #if defined(HAVE_MPI)
    if(Msg::GetCommSize() > 1)
    {
      double mpitmp =0.;
      MPI_Allreduce(&xFint,&mpitmp,1,MPI_DOUBLE,MPI_SUM,MPI_COMM_WORLD);
      xFint = mpitmp;
    }
   #endif // HAVE_MPI
    if(xFint == 0.)
      return 1.; // no deplacement --> no damping factor --> c = 0 and factor = 1
    // no blas operation for this ??
    for(int i=0;i<_nbRows;i++)
      _S[i] = _previousStep->_x[i]*this->_M[i];

   #if defined(HAVE_BLAS)
    xMx = F77NAME(ddot)(&_nbRows,_S,&INCX,_previousStep->_x,&INCX);
   #else
    xMx = 0.;
    for(int i=0;i<_nbRows;i++)
     xMx += _S[i]*_previousStep->_x[i];
   #endif // HAVE_BLAS
    // MPI comm
   #if defined(HAVE_MPI)
    if(Msg::GetCommSize() > 1)
    {
      double mpitmp =0.;
      MPI_Allreduce(&xMx,&mpitmp,1,MPI_DOUBLE,MPI_SUM,MPI_COMM_WORLD);
      xMx = mpitmp;
    }
   #endif // HAVE_MPI
    return (1./(1.+2.*sqrt(fabs(xFint)/xMx)*_timeStep));
  }

  virtual void restart()
  {
    if(!_isAllocated)
    {
      Msg::Error("Cannot saved or load an unalocated system!");
    }
    restartManager::restart(_whichStep);
    restartManager::restart(_imassAllocated);
    bool tdynrel = _dynamicRelaxation;
    bool tbenson = _timeStepByBenson;
    restartManager::restart(tdynrel);
    if(tdynrel != _dynamicRelaxation) Msg::Warning("You switch the scheme between dynamic relaxation and central difference");
    restartManager::restart(tbenson);
    double tstep = _timeStep;
    restartManager::restart(tstep);
    setTimeStep(tstep);
    /// double _alpham, _beta,_gamma, _oneDivbyOneMinusAlpham,_minusAlphamDivbyOneMinusAlpham, _oneMinusgammaDeltat,_gammaDeltat,_Deltat2halfMinusbeta,_Deltat2beta
    int trows = _nbRows;
    restartManager::restart(trows);
    if(trows != _nbRows) Msg::Error("Cannot load a explicitit scheme with a different size from the saved one!");

    restartManager::restart(_step1);
    restartManager::restart(_step2);
    /// set _current and _previous step
    nextStep();
    // reset _whichStep;
    _whichStep = !_whichStep;
    restartManager::restart(_M,_nbRows);
    restartManager::restart(_invM,_nbRows);
    /// mutable scalar *_v2; // square of velocitie (kinetic energy) // used as Fext n+1 + Fext n for external work
    restartManager::restart(_b,_nbRows);
    restartManager::restart(_S,_nbRows);
    return;
  }
};
#endif //EXPLICITHULBERTCHUNGBLAS_H_
