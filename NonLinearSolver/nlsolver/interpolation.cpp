//
// C++ Interface: interpolation method
//
//
//
// Author:  <Van Dung NGUYEN>, (C) 2017
//
// Copyright: See COPYING file that comes with this distribution
//
//

#include "interpolation.h"
#include <stdio.h>

double interpolationMethod::interp_linear(const double p, const double* x, const double *y, const int N){
  if (N <=1) {
    printf("number of control points  must be at least equal to 2 \n");
    return y[0];
  };
  double x0, x1;
  double y0, y1;
  if (p < x[0]){
    x0 = x[0]; x1 = x[1];
    y0 = y[0]; y1 = y[1];
  }
  else if (p >= x[N-1]){
    x0 = x[N-2]; x1 = x[N-1];
    y0 = y[N-2]; y1 = y[N-1];
  }
  else{
    for (int i=0; i< N-1; i++){
      if ((p>= x[i]) and (p < x[i+1])){
        x0 = x[i];
        x1 = x[i+1];
        y0 = y[i];
        y1 = y[i+1];
        break;
      }
    }
  }
  
  return (y1-y0)*(p-x0)/(x1-x0) + y0;
};

double interpolationMethod::interp_cubicSpline(const double p, const double* x, const double *y, const double* dy, const int N){
  if (N <=1) {
    printf("number of control points  must be at least equal to 2 \n");
    return y[0];
  };
  
  double x0, x1;
  double y0, y1;
  double dy0, dy1;
  if (p < x[0]){
    x0 = x[0]; x1 = x[1];
    y0 = y[0]; y1 = y[1];
    dy0 = dy[0]; dy1 = dy[1];
  }
  else if (p >= x[N-1]){
    x0 = x[N-2]; x1 = x[N-1];
    y0 = y[N-2]; y1 = y[N-1];
    dy0 = dy[N-2]; dy1 = dy[N-1];
  }
  else{
    for (int i=0; i< N-1; i++){
      if ((p>= x[i]) and (p < x[i+1])){
        x0 = x[i];
        x1 = x[i+1];
        y0 = y[i];
        y1 = y[i+1];
        dy0 = dy[i];
        dy1 = dy[i+1];
        break;
      }
    }
  }
  
  double FF[4];
  double L=x1 -x0;
  double r=(p-x0)/(x1-x0);
  FF[0] = (2*r*r*r-3*r*r+1);
  FF[1] = ((r*r*r-2*r*r+r)*L);
  FF[2] = (3*r*r-2*r*r*r);
  FF[3] = ((r*r*r-r*r)*L);
  
  return FF[0]*y0 + FF[1]*dy0+ FF[2]*y1+FF[3]*dy1;
};