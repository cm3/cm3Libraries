//
// C++ Interface: Dof manager
//
// Description: dofManager for non linear system (rewritte of assemble(fullMatrix) no contribution for fixed Dof
//              The contribution is taken into account in the assemble(fullvector) of RightHandSide
//              More add functions to archiving force can be placed in dofManager but I put it here to not pollute gmsh code
// Author:  <Gauthier BECKER>, (C) 2010,
//          <Van Dung NGUYEN>, (C) 2020
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef _STATICDOFMANAGER_H_
#define _STATICDOFMANAGER_H_

#include "dofManager.h"
#include "nonLinearMechSolver.h"
#include "nonLinearSystems.h"

template<class T>
struct FixedNodeDynamicData
{
  public:
    typename dofTraits<T>::VecType position;
    typename dofTraits<T>::VecType velocity;
    typename dofTraits<T>::VecType acceleration;
    typename dofTraits<T>::VecType mass;
    FixedNodeDynamicData() : position(0.), velocity(0.), acceleration(0.), mass(0.){}
    FixedNodeDynamicData(const FixedNodeDynamicData & src) : position(src.position), velocity(src.velocity), acceleration(src.acceleration),
                                                               mass(src.mass) {}
    FixedNodeDynamicData& operator=(const FixedNodeDynamicData & src)
    {
      position = src.position;
      velocity = src.velocity;
      acceleration = src.acceleration;
      mass = src.mass;
      return *this;
    }
    ~FixedNodeDynamicData(){}
};

class nlsDofManager;
class DofCollection
{
  public:
    std::map<Dof, std::vector<double> > allDofs;
    DofCollection(){}
    ~DofCollection(){}
    void collect(nlsDofManager* p);
};

class nlsDofManager
{
  public:
    typedef typename nonLinearSystemBase::whichMatrix whichMatrix;
    typedef typename nonLinearSystemBase::rhs whichRhs;  

  public:
    virtual ~nlsDofManager(){}
    // get manager by comp, not not, return this
    virtual nlsDofManager* getManager(const int i) = 0;
    virtual const nlsDofManager* getManager(const int i) const = 0;
    virtual nlsDofManager* getManagerByComp(int comp) = 0;
    virtual const nlsDofManager* getManagerByComp(int comp) const = 0;
    
    virtual linearSystem<double> *getLinearSystem(std::string &name)= 0;
    virtual const linearSystem<double> *getLinearSystem(std::string &name) const = 0;
    virtual void setCurrentMatrix(std::string name) = 0;
    //
    virtual nonLinearMechSolver::scheme getScheme()const = 0;
    
    // collection data for new solving
    virtual void collectAllDofValues(DofCollection& collect) const = 0;
    // update data from dof collection
    virtual void updateDataFromCollection(const DofCollection& collect) = 0; 

    //
    virtual int sizeOfR() const = 0; // for infos
    virtual int sizeOfF() const = 0; // for infos only
    
    //
    virtual void getFixedDof(std::vector<Dof> &R) const = 0;
    virtual void getFixedDof(std::set<Dof> &R) const = 0;
    virtual int getDofNumber(const Dof &ky) const = 0;
    virtual void clearAllLineConstraints() = 0;
    virtual void setLinearConstraint(const Dof& key, DofAffineConstraint<double> &affineconstraint) = 0; 
    virtual void getAllLinearConstraints(std::map<const Dof, const DofAffineConstraint<double>*>& map) const = 0;
    virtual bool isFixed(const Dof& key) const = 0;
    virtual bool isAnUnknown(const Dof& key) const = 0;
    virtual bool isConstrained(const Dof& key) const = 0;

    //
    virtual void setFirstRigidContactUnknowns() = 0;
    virtual int getFirstRigidContactUnknowns() const = 0;
    
    virtual void assemble(const std::vector<Dof> &R, const fullMatrix<double> &m) = 0;
    virtual void assemble(const std::vector<Dof> &R, const fullMatrix<double> &m,const whichMatrix wm) = 0;
    
    // assemble force
    virtual void assemble(const std::vector<Dof> &R, const fullVector<double> &m) = 0;
    virtual void assemble(const std::vector<Dof> &R, const fullVector<double> &m,const whichRhs wrhs) = 0;
    
    // for homogenized tangent estimation
    virtual void assembleToStressMatrix(const std::vector<Dof> &R, const fullMatrix<double> &m) = 0;
    virtual void assembleToBodyForceMatrix(const std::vector<Dof> &R, const fullMatrix<double> &m) = 0;
    virtual void assembleToBodyForceVector(const std::vector<Dof> &R, const fullVector<double> &m) = 0;
    virtual void assemblePathFollowingConstraint(const double val) = 0;
    virtual void assembleDPathFollowingConstraintDUnknowns(const std::vector<Dof>& R, const fullVector<double> &m) = 0;
    
    virtual void setInitialCondition(const Dof &R, const double &value, const nonLinearBoundaryCondition::whichCondition wc) = 0;
    virtual void setInitialCondition(const std::vector<Dof> &R, const std::vector<double> &disp, const nonLinearBoundaryCondition::whichCondition wc) = 0;
    
    virtual void fixDof(const Dof& key, const double &value) = 0;
    virtual void numberDof(const Dof& key) = 0;
    virtual void numberDof(const std::vector<Dof> &R) = 0;
    // solving
    virtual void systemSolve() = 0;
    virtual int systemSolveIntReturn() = 0;
    virtual int lineSearch() = 0;
    
    virtual void insertInSparsityPattern(const Dof &R, const Dof &C) = 0;
    virtual void sparsityDof(const std::vector<Dof>& R) = 0;
    virtual void allocateSystem() = 0;
    virtual void preAllocateEntries() = 0;
    // operations
    virtual void clearRHSfixed() = 0;
    virtual double getNorm1ReactionForce() const = 0;
    virtual void getReactionForceOnFixedPhysical(archiveForce& af) const = 0;
    virtual void getForces(std::vector<archiveForce> &vaf) const = 0;
    virtual double getKineticEnergy(const int systemSizeWithoutRigidContact,FilterDofSet &fildofcontact) const = 0;
    
    virtual void zeroMatrix() = 0;
    virtual void resetUnknownsToPreviousTimeStep() =0;
    virtual void nextStep() = 0;
    virtual void setTimeStep(const double dt)= 0;
    //
    virtual double normInfRightHandSide() const = 0;
    virtual double normInfSolution() const = 0;
    virtual double norm0Inf() const = 0;
    //
    #if defined(HAVE_MPI)
    virtual void manageMPIComm(const int otherPartNum,const std::vector<Dof> &otherPartR) = 0;
    virtual void manageMPISparsity(const std::vector<Dof> &myR, const int otherPartNum,const std::vector<Dof> &otherPartR) = 0;
    virtual void systemMPIComm() = 0;
    #endif //HAVE_MPI
    virtual void restart() = 0;
    
    // get Data
    virtual void getVertexMass(const std::vector<Dof> &R, std::vector<double> &vmass) const = 0;
    virtual void getFixedRightHandSide(std::vector<Dof> &R, std::vector<double> &val) const = 0;
    //
    virtual void getDofValue(const Dof& key,  double &val) const = 0;
    virtual void getDofValue(const Dof& key,  double &val, nonLinearBoundaryCondition::whichCondition wv) const = 0;
    virtual void getDofValue(const std::vector<Dof> &keys,std::vector<double> &Vals) const = 0;
    virtual void getDofValue(const std::vector<Dof> &keys,std::vector<double> &Vals, const nonLinearBoundaryCondition::whichCondition wv) const = 0;
    
    //
    virtual void getFixedDofValue(const Dof& key, double &val) const = 0; 
    
    virtual const std::map<Dof, int>& getUnknownMap() const=0;
};

class staticDofManager : public nlsDofManager
{
  public:
    typedef typename nonLinearSystemBase::whichMatrix whichMatrix;
    typedef typename nonLinearSystemBase::rhs whichRhs;
  
  protected :
    // numbering of unknown dof blocks
    std::map<Dof, int> unknown;
    // general affine constraint on sub-blocks, treated by adding
    // equations:
    //   Dof = \sum_i dataMat_i x Dof_i + dataVec
    std::map<Dof, DofAffineConstraint<double> > constraints;

    // fixations on full blocks, treated by eliminating equations:
    //   DofVec = double
    std::map<Dof, double> fixed;
    //
    nonLinearMechSolver::scheme _scheme;
    // map to retains the force of fixed dof (needed for archiving) the other RHS value are accessible in RHS
    std::map<Dof, double> RHSfixed;
    std::map<Dof,FixedNodeDynamicData<double> > _fixedDynamic;
    
    bool _mpiSendGlobalPosition;
    bool _isPartitioned; // bool to know if the dofmanager use for micro or macro problem, set to true by defaut if MPI enable and false if MPI disable
    int _firstRigidContactDof;
    
    // linearSystems
    linearSystem<double> *_current;
    std::map<const std::string, linearSystem<double> *> _linearSystems;
    /* pointer to the current nonLinearSystem */ // normally only 1 system is stored by dofManager and we avoid multiple dynamic_cast
    nonLinearSystem<double> *_NLScurrent;
    int _numberOfUnknownsPreviouRank;
 
    #if defined(HAVE_MPI)
    bool _mpiInit;
    std::map<Dof,int> _otherPartUnknowns; // local position of the unknowns
    std::map<Dof,int> _globalOtherPartSystemPosition; // global position of unknowns in the system

    double* _mpiBuffer;
    int _sizeMPIBuffer;
    std::vector< std::set<Dof> > _mapMPIRecvComm;  //
    std::vector< std::vector<int>* > _mapMPIDof; // We are sure by construction that the Dof to transfert is not fixed so we can save directly the position of the Dof in the system
    int* _localNumberDofMPI; // array with the number of Dofs on each rank;

    double* _mpiRigidContactTMP; // array to communicate the unknown of rigid contact
    int _mpiRigidContactSize;

    /* static analysis involve only displacement here consider velocity and acceleration*/
    double* _otherPartPositions;   // only works with T == double
    double* _previousOtherPartPositions;
    double* _otherPartVelocities;  // velocity
    double* _previousOtherPartVelocities;
    double* _otherPartAccelerations; // acceleration
    double* _previousOtherPartAccelerations;
    #endif //HAVE_MPI

  // initialization of MPI communication (send the unknowns that this processor need and recieve from other rank the unknown to send

  protected:
    #if defined(HAVE_MPI)
    virtual void initMPI();
   #endif // HAVE_MPI

  public:
    staticDofManager(linearSystem< double > *l, nonLinearMechSolver::scheme sh, bool useGlobalSystemPosition, bool part);
    staticDofManager(linearSystem<double> *l1, linearSystem<double> *l2, bool part);
    virtual ~staticDofManager();
    
    // get manager by comp, not not, return this
    virtual nlsDofManager* getManager(const int i) {return this;};
    virtual const nlsDofManager* getManager(const int i) const {return this;};
    virtual nlsDofManager* getManagerByComp(int comp) {return this;};
    virtual const nlsDofManager* getManagerByComp(int comp) const {return this;};
    
    virtual linearSystem<double> *getLinearSystem(std::string &name);
    virtual const linearSystem<double> *getLinearSystem(std::string &name) const;
    virtual void setCurrentMatrix(std::string name);

    virtual nonLinearMechSolver::scheme getScheme()const {return _scheme;}
  
    virtual void collectAllDofValues(DofCollection& collect) const;
    virtual void updateDataFromCollection(const DofCollection& collect);
    
    virtual int sizeOfR() const {return unknown.size();}
    virtual int sizeOfF() const {return fixed.size();}; // for infos only
    //
    virtual int getDofNumber(const Dof &ky) const ;
    virtual void getFixedDof(std::vector<Dof> &R) const;
    virtual void getFixedDof(std::set<Dof> &R) const;
    virtual void clearAllLineConstraints();
    virtual void setLinearConstraint(const Dof& key, DofAffineConstraint<double> &affineconstraint);
    virtual void getAllLinearConstraints(std::map<const Dof, const DofAffineConstraint<double>*>& map) const;
    virtual bool isFixed(const Dof& key) const;
    virtual bool isAnUnknown(const Dof& key) const;
    virtual bool isConstrained(const Dof& key) const;
    
    virtual int systemSolveIntReturn();
    virtual void systemSolve();
    virtual int lineSearch();
    
    virtual void allocateSystem();
    virtual void preAllocateEntries();
    
    // zero for RHSfixed
    virtual void clearRHSfixed();    
    virtual double getNorm1ReactionForce() const;
    virtual void getReactionForceOnFixedPhysical(archiveForce& af) const;
    virtual void getForces(std::vector<archiveForce> &vaf) const;
    virtual double getKineticEnergy(const int systemSizeWithoutRigidContact,FilterDofSet &fildofcontact) const;
    //
    virtual void zeroMatrix();
    virtual void resetUnknownsToPreviousTimeStep();
    virtual void nextStep();
    virtual void setTimeStep(const double dt);
    //
    virtual double normInfRightHandSide() const;
    virtual double normInfSolution() const;
    virtual double norm0Inf() const;
    
    virtual void restart();
    
    #if defined(HAVE_MPI)
    virtual void manageMPIComm(const int otherPartNum,const std::vector<Dof> &otherPartR);
    virtual void manageMPISparsity(const std::vector<Dof> &myR, const int otherPartNum,const std::vector<Dof> &otherPartR);
    virtual void systemMPIComm();
    #endif // HAVE_MPI
    
    // sparsity system
    virtual void insertInSparsityPatternLinConst(const Dof &R, const Dof &C);
    virtual void insertInSparsityPattern(const Dof &R, const Dof &C);
    virtual void sparsityDof(const std::vector<Dof> &R);
    
    //// Function to fix dof (Create the key in RHS fixed too)
    virtual void fixDof(const Dof& key, const double &value);
    virtual void numberDof(const Dof& key);
    virtual void numberDof(const std::vector<Dof> &R);
    
    // contact
    virtual void setFirstRigidContactUnknowns();
    virtual int getFirstRigidContactUnknowns() const;
    
    
    // assemble stiffness
    virtual void assemble(const std::vector<Dof> &R, const fullMatrix<double> &m);
    virtual void assemble(const std::vector<Dof> &R, const fullMatrix<double> &m,const whichMatrix wm);
  
    // assemble force
    virtual void assemble(const std::vector<Dof> &R, const fullVector<double> &m);
    virtual void assemble(const std::vector<Dof> &R, const fullVector<double> &m,const whichRhs wrhs);
    
    // for homogenized tangent estimation
    virtual void assembleToStressMatrix(const std::vector<Dof> &R, const fullMatrix<double> &m);
    virtual void assembleToBodyForceMatrix(const std::vector<Dof> &R, const fullMatrix<double> &m);
    virtual void assembleToBodyForceVector(const std::vector<Dof> &R, const fullVector<double> &m);
    virtual void assemblePathFollowingConstraint(const double val);
    virtual void assembleDPathFollowingConstraintDUnknowns(const std::vector<Dof>& R, const fullVector<double> &m);
    
    // set initial condition
    virtual void setInitialCondition(const Dof &R, const double &value, const nonLinearBoundaryCondition::whichCondition wc);
    virtual void setInitialCondition(const std::vector<Dof> &R, const std::vector<double> &disp, const nonLinearBoundaryCondition::whichCondition wc);

    // get Data
    virtual void getVertexMass(const std::vector<Dof> &R, std::vector<double> &vmass) const;
    //
    virtual void getFixedRightHandSide(std::vector<Dof> &R, std::vector<double> &val) const;
    
    virtual void getDofValue(const Dof& key,  double &val) const;
    virtual void getDofValue(const Dof& key,  double &val, nonLinearBoundaryCondition::whichCondition wv) const;

    virtual void getDofValue(const std::vector<Dof> &keys,std::vector<double> &Vals) const;
    virtual void getDofValue(const std::vector<Dof> &keys,std::vector<double> &Vals, const nonLinearBoundaryCondition::whichCondition wv) const;
    
    virtual void getFixedDofValue(const Dof& key, double &val) const;
    
    virtual const std::map<Dof, int>& getUnknownMap() const {return unknown;};
    
  private:
    // assemble to stiffness matrix
    void assembleLinConst(const Dof &R, const Dof &C, const double& value);
    void assemble(const Dof &R, const Dof& C, const double &value);
    void assemble(const Dof &R, const double &value);
    void assemble(const Dof &R, const double &value, const whichRhs wrhs);
    void assembleDPathFollowingConstraintDUnknowns(const Dof &R, const double &value);
    
    // only for elastic solver as matrix assembling is different
    void linearElastic_assembleLinConst(const Dof &R, const Dof &C, const double &value);
    void linearElastic_assemble(const Dof &R, const Dof& C, const double &value);
    void linearElastic_assemble(const std::vector<Dof> &R, const fullMatrix<double> &m);
};


class staticDofManagerFullCG : public staticDofManager
{
  protected:    
    
    #if defined(HAVE_MPI)
    // staticDofManager::_otherPartUnknowns  all dofs at interfaces
    std::set<Dof> _localDofOtherPartUnknowns; // local dofs at currant rank needs to be send to other ranks    
    virtual void initMPI();
    #endif // HAVE_MPI
   
  public:
    staticDofManagerFullCG(linearSystem< double > *l, nonLinearMechSolver::scheme sh, bool useGlobalSystemPosition, bool part):
              staticDofManager(l,sh,useGlobalSystemPosition,part){};
    staticDofManagerFullCG(linearSystem<double> *l1, linearSystem<double> *l2, bool part):
              staticDofManager(l1,l2,part){};
    virtual ~staticDofManagerFullCG(){};  
    
    #if defined(HAVE_MPI)
    virtual int sizeOfR() const;
    virtual void allocateSystem();
    virtual void setFirstRigidContactUnknowns();
    virtual void manageMPIComm(const int otherPartNum,const std::vector<Dof> &otherPartR);
    virtual void manageMPISparsity(const std::vector<Dof> &myR, const int otherPartNum,const std::vector<Dof> &otherPartR);
    virtual void systemMPIComm();
    virtual void getDofValue(const Dof& key,  double &val, nonLinearBoundaryCondition::whichCondition wv) const;
    #endif // HAVE_MPI
};
#endif // STATICDOFMANAGERSYSTEM_H_

