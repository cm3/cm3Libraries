//
//
// Description: Class to get the current configuration (of element)
//
//
// Author:  <Gauthier BECKER>, (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef CURRENTCONFIG_H_
#define CURRENTCONFIG_H_
class currentConfig{
 public:
  currentConfig(){}
  ~currentConfig(){}
  static void elementPositionXYZ(MElement *ele,const fullVector<double> &disp, std::vector<double> &elempos)
  {
    int nbvertex = ele->getNumVertices();
    elempos.resize(3*nbvertex);
    MVertex *ver;
    for(int i=0;i<nbvertex;i++){
      ver = ele->getVertex(i);
      elempos[i] = ver->x() + disp(i);
      elempos[i+nbvertex] = ver->y() + disp(i+nbvertex);
      elempos[i+nbvertex+nbvertex] = ver->z() + disp(i+nbvertex+nbvertex);
    }
  }
  static void elementPositionX(MElement *ele,const fullVector<double> &disp, std::vector<double> &elempos)
  {
    int nbvertex = ele->getNumVertices();
    elempos.resize(nbvertex);
    MVertex *ver;
    for(int i=0;i<nbvertex;i++){
      ver = ele->getVertex(i);
      elempos[i] = ver->x() + disp(i);
    }
  }
  static void elementPositionY(MElement *ele,const fullVector<double> &disp, std::vector<double> &elempos)
  {
    int nbvertex = ele->getNumVertices();
    elempos.resize(nbvertex);
    MVertex *ver;
    for(int i=0;i<nbvertex;i++){
      ver = ele->getVertex(i);
      elempos[i] = ver->y() + disp(i);
    }
  }
  static void elementPositionZ(MElement *ele,const fullVector<double> &disp, std::vector<double> &elempos)
  {
    int nbvertex = ele->getNumVertices();
    elempos.resize(nbvertex);
    MVertex *ver;
    for(int i=0;i<nbvertex;i++){
      ver = ele->getVertex(i);
      elempos[i] = ver->z() + disp(i);
    }
  }
};

#endif // CURRENTCONFIG_H_
