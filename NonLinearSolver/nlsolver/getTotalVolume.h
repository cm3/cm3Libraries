//
//
// Description: get total volume of a finite element mesh
//
// Author:  <Van Dung NGUYEN>, (C) 2013
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef GETTOTALVOLUME_H_
#define GETTOTALVOLUME_H_

#include "elementGroup.h"

class getTotalVolume{
  protected:
    elementGroup* _g;

  public:
    getTotalVolume();
    ~getTotalVolume();

    void loadModel(const std::string filename);
    double totalVolume();
    double volumePhysical(const int dim, const int phys);
    double smallestElementSize();
};


#endif // GETTOTALVOLUME_H_
