//
// Author:  <Van Dung NGUYEN>, (C) 2020
//
// Copyright: See COPYING file that comes with this distribution
//
//


#ifndef MESH_MODIFICATION_H_
#define MESH_MODIFICATION_H_

#ifndef SWIG

#include "elementGroup.h"
#include "GModel.h"
#endif // SWIG

class modificationOperation
{
  public:
    #ifndef SWIG
    modificationOperation(){}
    virtual ~modificationOperation(){}
    virtual void transform(MVertex* v) const = 0;
    virtual modificationOperation* clone() const = 0;
    #endif //SWIG
};

class TrivialOperation : public modificationOperation
{
  public:
    TrivialOperation();
    #ifndef SWIG
    virtual ~TrivialOperation(){}
    virtual void transform(MVertex* v) const {};
    virtual modificationOperation* clone() const {return new TrivialOperation();}
    #endif //SWIG
};

class RollingOperation : public modificationOperation
{
  protected:
    int _dir;
    double _posAxis;
    double _maxLength;
    
  public:
    RollingOperation(int dir, double c, double maxLeng); // 0 x, 1-> y, 2 -z  c- position of center in  rolling axis 
    #ifndef SWIG
    virtual ~RollingOperation(){}
    virtual void transform(MVertex* v) const;
    virtual modificationOperation* clone() const {return new RollingOperation(_dir,_posAxis,_maxLength);}
    #endif //SWIG
};

class ScalingOperation : public modificationOperation
{
  protected:
    double _factX, _factY, _factZ;
    
  public:
    ScalingOperation(double x, double y, double z);
    #ifndef SWIG
    virtual ~ScalingOperation(){}
    virtual void transform(MVertex* v) const;
    virtual modificationOperation* clone() const {return new ScalingOperation(_factX, _factY, _factZ);}
    #endif //SWIG
};


class TranslationOperation : public modificationOperation
{
  protected:
    double _dX, _dY, _dZ;
    
  public:
    TranslationOperation(double x, double y, double z);
    #ifndef SWIG
    virtual ~TranslationOperation(){}
    virtual void transform(MVertex* v) const;
    virtual TranslationOperation* clone() const {return new TranslationOperation(_dX, _dY, _dZ);}
    #endif //SWIG
};


class meshModification 
{
  #ifndef SWIG
  protected:
    GModel* _pModel;
    std::string _meshFileName;
    modificationOperation* _opt;
  #endif // SWIG
    
  public:
    meshModification();
    ~meshModification();
    void loadModel(const std::string meshFileName);
    void loadGeo(const std::string geoFile, int dimToMesh, int order);
    void setOperation(const modificationOperation& opt);
    void applyModification(const std::string fname = "",double ver=4.1); 
};

void write_MSH2(const std::map<int, elementGroup>& gMap,  const std::string filename);

class makePhysicalByBox
{
  protected:
    #ifndef SWIG
    class box
    {
      public:
        double xmin, xmax, ymin, ymax;
        double r; //
        box():xmin(0.),xmax(0.),ymin(0.),ymax(0.), r(0.){}
        ~box(){}
        bool inBox(MElement* ele) const;
        void printData() const
        {
          printf("box: xmin = %e xmax = %e y min = %e, ymax = %e\n",xmin, xmax, ymin, ymax);
        }
    };
    #endif //SWIG

    
  public:
    makePhysicalByBox(){};
    ~makePhysicalByBox(){};
    
    void physicalToMesh(int dim, int phys, const std::string inputMeshFile, const std::string outputMeshFile, int newPhy=-1);
    void run(const std::string inputMeshFile, const std::string outputMeshFile, 
             double xmin, double xmax, int nx, 
             double ymin, double ymax, int ny);
};

class QuadToTriangle 
{
  #ifndef SWIG
  protected:
    GModel* _pModel;
  
  protected:
    void remesh(MElement* ele, std::vector<MElement*>& newEle) const;
  #endif // SWIG
    
  public:
    QuadToTriangle();
    ~QuadToTriangle();
    void loadModel(const std::string meshFileName);
    void apply(const std::string fname); 
    
};

#endif //MESH_MODIFICATION_H_