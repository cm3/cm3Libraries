//
// Description: System for non linear static scheme
//
//
// Author:  <Gauthier BECKER>, (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
//
// The system solve K Delta x = F so add a vector xsol which contains the actual value of unknowns

#ifndef NONLINEARSYSTEMPETSC_H_
#define NONLINEARSYSTEMPETSC_H_

#include "nonLinearSystems.h"
// petsc
#include "linearSystemPETSc.h"
#include "linearSystemPETSc.hpp"

// to debug
#if defined(HAVE_PETSC)
#include "functionPETSc.h"
using namespace functionPETSc;

static void nlprintPETScVector(Vec &a,int vsize,const char *vname) {
  double val;
  for(int i=0; i<vsize;i++){
    _try(VecGetValues(a, 1, &i, &val));
    printf("%s comp %d %e, proc %d\n",vname,i,val,Msg::GetCommRank());
  }
  printf("---\n");
}
static void nlprintPETScMatrix(Mat &a,int rsize, int csize,const char *mname)
{
  double val;
  for(int i=0;i<rsize;++i){
    for(int j=0;j<csize;++j){
      _try(MatGetValues(a, 1, &i, 1, &j, &val));
      printf("%s(%d,%d) = %e, princ %d\n",mname,i,j,val,Msg::GetCommRank());
    }
  }
  printf("----\n");
}

template<class scalar>
class nonLinearSystemPETSc : public linearSystemPETSc<scalar>, public nonLinearSystem<scalar>{
 protected:
  bool _allocatednldata;
  mutable bool _flagb;
 protected:
  Vec _xsol;  // _xsol = _xitels + alpha * _x; (alpha == line search parameter)
  Vec _xitels; // for line search the value of solution as to be kept
  Vec _xprev; // solution of previous time step
  Vec _xinit;// solution of initial time step
  Vec _xtemp; // temp step for cache data
  Vec _Fext;
  Vec _Fint;
  Vec _lastResidual; // Fint-Fext in the previous iteration
  double _timeStep;
  std::vector<Vec> _multipleRHS, _multipleRHSSolution;
  int _numRHS;
  bool _isMultipleRHSAllocated;
  // To compute external work
  mutable Vec _Fextprev;
  mutable Vec _Deltax; // u_{n+1} - u_n
  mutable Vec _FextpFextprev;
  // data for line search
  const double _Tolls; // tolerance for line search
  bool _lineSearch;
  double _alphaRneg, _alphaRpos, _alphaLsIte, _alphaLsPrev;
  PetscScalar _resNeg, _resPos, _resLsIte, _res0;
  int _lsite,_lsitemax;
  int _nbRows; // can be usefull to know the system's size for debug operation
  
  bool _withBFGS;
  int _BFGSCounter;
  mutable std::map<int, Vec> _BFGSGammas; // store residual increment
  mutable std::map<int, Vec> _BFGSDeltas; // store unknown increment
  mutable std::map<int, double> _BFGSRhos;
  
 protected:
   Vec& getState(const IPStateBase::whichState ws) {
    if (ws == IPStateBase::initial) return _xinit;
    else if (ws == IPStateBase::previous) return _xprev;
    else if (ws == IPStateBase::current) return _xsol;
    else if (ws == IPStateBase::temp) return _xtemp;
    else{
      Msg::Error("This state is not defined");
      static Vec a;
      return a;
    }
  }

  Vec& getRHS(const typename nonLinearSystemBase::rhs wrhs){
    if (wrhs == nonLinearSystem<scalar>::Fint) return _Fint;
    else if (wrhs == nonLinearSystem<scalar>::Fext) return _Fext;
    else Msg::Error("This RHS is not defined");
    static Vec a;
    return a;
  }  
  
 public:
  nonLinearSystemPETSc(MPI_Comm com = PETSC_COMM_WORLD, const bool ls=false, const double tolls=1e-6,
                       const int lsitemax=10) : linearSystemPETSc<scalar>(com), nonLinearSystem<scalar>(),
                                                                 _allocatednldata(false), _flagb(false),
                                                                 _lineSearch(ls), _Tolls(tolls),
                                                                 _alphaRneg(0.), _alphaRpos(1.), _resNeg(0.), _resPos(0), _lsite(0),
                                                                 _alphaLsIte(0.), _resLsIte(0.), _res0(1.),
                                                                 _alphaLsPrev(1.), _lsitemax(lsitemax), _timeStep(0.),
                                                                 _nbRows(0),_isMultipleRHSAllocated(false),_numRHS(0),
                                                                 _multipleRHS(0),_multipleRHSSolution(0),
                                                                 _BFGSCounter(0), _withBFGS(false),
                                                                 _BFGSGammas(), _BFGSDeltas(), _BFGSRhos()
  {
    this->_parameters["matrix_reuse"] = "same_sparsity";
  }
  virtual ~nonLinearSystemPETSc(){} // _xsol is destroied by clear() called by ~linearSystemPETSc()
  
  virtual void useQuasiNewtonMethod(bool fl)
  {
    if (fl)
    {
      Msg::Info("BFGS is activated");
    }
    else if (_withBFGS)
    {
      Msg::Info("BFGS is deasactivated");
    }
    _withBFGS = fl;
    _BFGSCounter = 0;
  }

  virtual void zeroMatrix()
  {
    linearSystemPETSc<scalar>::zeroMatrix();
    _BFGSCounter = 0;
  }
  
  virtual void zeroMatrix(const nonLinearSystemBase::whichMatrix wm) 
  {
    if (wm == nonLinearSystemBase::mass)
    {
      Msg::Error("nonLinearSystemPETSc has no mass");
    }
    else if (wm == nonLinearSystemBase::stiff)
    {
      linearSystemPETSc<scalar>::zeroMatrix();
    }
    else
    {
      Msg::Error("matrix does not exist");
    }
    _BFGSCounter = 0;
  };

  virtual void copy(const IPStateBase::whichState source,
                      const IPStateBase::whichState destination){
    Vec& src = this->getState(source);
    Vec& dst = this->getState(destination);
    _try(VecAssemblyBegin(src));
    _try(VecAssemblyEnd(src));

    _try(VecAssemblyBegin(dst));
    _try(VecAssemblyEnd(dst));

    _try(VecCopy(src,dst));
  };
  
  virtual bool isMultipleRHSAllocated() const 
  {
    return _isMultipleRHSAllocated;
  }
  virtual void allocateMultipleRHS(int nbRHS)
  {
    if (nbRHS > _multipleRHS.size())
    {
      int oldSize = _multipleRHS.size();
      // allocated if size is exceed
      for (int j=oldSize; j< nbRHS; j++)
      {
        Vec vecrhs, vecSol;
        _try(VecDuplicate(this->_x, &vecrhs));
        _try(VecDuplicate(this->_x, &vecSol));
        _multipleRHS.push_back(vecrhs);
        _multipleRHSSolution.push_back(vecSol);
      }
    };
    _isMultipleRHSAllocated = true;
    _numRHS = nbRHS;
  };
  virtual void zeroMultipleRHS()
  {
    if (_isMultipleRHSAllocated)
    {
      for (int j=0; j< _numRHS; j++)
      {
        _try(VecAssemblyBegin(_multipleRHS[j]));
        _try(VecAssemblyEnd(_multipleRHS[j]));
        _try(VecZeroEntries(_multipleRHS[j]));
        
        _try(VecAssemblyBegin(_multipleRHSSolution[j]));
        _try(VecAssemblyEnd(_multipleRHSSolution[j]));
        _try(VecZeroEntries(_multipleRHSSolution[j]));
      };
    };
  };
  
  virtual void addToMultipleRHSMatrix(int row, int col, const scalar& val)
  {
    PetscInt ii = row;
    PetscScalar s = val;
    if (col > _numRHS -1)
    {
      printf("col %d exceeds the number of RHS = %d",col,_numRHS);
      return;
    }
    _try(VecSetValues(_multipleRHS[col], 1, &ii, &s, ADD_VALUES));
  };
  virtual void getFromMultipleRHSSolution(int row, int col, scalar& val)
  {
    PetscInt ii = row;
    PetscScalar& s = val;
    if (col > _numRHS -1)
    {
      printf("col %d exceeds the number of RHS = %d",col,_numRHS);
      return;
    }
    _try(VecGetValues(_multipleRHSSolution[col], 1, &row, &val));
  };
  
  virtual bool systemSolveMultipleRHS()
  {
    for (int i=0; i< _numRHS; i++)
    {
      PetscInt col = i;
      _try(VecCopy(_multipleRHS[i],this->_b));
      PetscReal NormRHS;
      _try(VecNorm(this->_b,NORM_INFINITY,&NormRHS));
      if (NormRHS > 0.)
      {
        //printf("start solving column %d\n",i);
        // solve if norm greater than 0
        linearSystemPETSc<scalar>::systemSolve();
        _try(VecCopy(this->_x,_multipleRHSSolution[i]));
      }
    }
    return true;
  };
  
  virtual void allocate(int nbRows){
    linearSystemPETSc<scalar>::allocate(nbRows);
    // To avoid a segmentation fault with version petsc3.3 in // computation Better fix ??
    #if (((PETSC_VERSION_MAJOR == 3) && (PETSC_VERSION_MINOR >= 3)))
    if(Msg::GetCommSize()>1){
      _try(MatSetUp(this->_a));
    }
    #endif // (PETSC_VERSION_MAJOR == 3) && (PETSC_VERSION_MINOR >= 3)
    _try(VecDuplicate(this->_x, &_xsol));
    _try(VecDuplicate(this->_x, &_xprev));
    _try(VecDuplicate(this->_x, &_xinit));
    _try(VecDuplicate(this->_x, &_xtemp));
    _try(VecDuplicate(this->_x, &_xitels));
    _try(VecDuplicate(this->_x, &_Fext));
    _try(VecDuplicate(this->_x, &_Fint));
    _try(VecDuplicate(this->_x, &_Fextprev));
    _try(VecDuplicate(this->_x, &_Deltax));
    _try(VecDuplicate(this->_x, &_FextpFextprev));
    _try(VecDuplicate(this->_x, &_lastResidual));
    
    _nbRows = nbRows;
    _allocatednldata=true;
    _try(VecAssemblyBegin(_Fextprev));
    _try(VecAssemblyEnd(_Fextprev));
    _try(VecZeroEntries(_Fextprev));
    _try(VecAssemblyBegin(_xprev));
    _try(VecAssemblyEnd(_xprev));
    _try(VecZeroEntries(_xprev));

    _try(VecAssemblyBegin(_xinit));
    _try(VecAssemblyEnd(_xinit));
    _try(VecZeroEntries(_xinit));
    
    _try(VecAssemblyBegin(this->_b));
    _try(VecAssemblyEnd(this->_b));
    _try(VecZeroEntries(this->_b));
    
    _try(VecAssemblyBegin(_lastResidual));
    _try(VecAssemblyEnd(_lastResidual));
    _try(VecZeroEntries(_lastResidual));
  }
  virtual void clear(){
    linearSystemPETSc<scalar>::clear();
    if(_allocatednldata){
      _try(VecDestroy(&_xsol));
      _try(VecDestroy(&_xprev));
      _try(VecDestroy(&_xinit));
      _try(VecDestroy(&_xtemp));
      _try(VecDestroy(&_xitels));
      _try(VecDestroy(&_Fext));
      _try(VecDestroy(&_Fextprev));
      _try(VecDestroy(&_Fint));
      _try(VecDestroy(&_FextpFextprev));
      _try(VecDestroy(&_Deltax));
      _try(VecDestroy(&_lastResidual));
      if (_isMultipleRHSAllocated)
      {
        _isMultipleRHSAllocated = false;
        _numRHS = 0;
        for (int i=0; i< _multipleRHS.size(); i++)
        {
          _try(VecDestroy(&_multipleRHS[i]));
        }
        for (int i=0; i< _multipleRHSSolution.size(); i++)
        {
          _try(VecDestroy(&_multipleRHSSolution[i]));
        }
        _multipleRHS.clear();
        _multipleRHSSolution.clear();
      }
    }
    
    if (_withBFGS)
    {
      for (std::map<int,Vec>::iterator it = _BFGSDeltas.begin(); it != _BFGSDeltas.end(); it ++)
      {
        _try(VecDestroy(&(it->second)));
      } 
      for (std::map<int,Vec>::iterator it = _BFGSGammas.begin(); it != _BFGSGammas.end(); it ++)
      {
        _try(VecDestroy(&(it->second)));
      } 
      _BFGSDeltas.clear();
      _BFGSGammas.clear();
    }
    
    _nbRows = 0;
    _allocatednldata = false;
    _flagb = false;
  }
  virtual void restart(){
    if(_allocatednldata)
    {
      PetscInt nbRows;
      _try(VecGetLocalSize(_xsol,&nbRows));
      int trow = nbRows;
      restartManager::restart(trow);
      if(trow != (int)_nbRows) Msg::Error("Cannot load a petsc step with a different size from the saved one");

      //restartManager::restart(this->_isAllocated);
      //bool kspA=this->_kspAllocated;
      //restartManager::restart(kspA);
      //if(kspA!=this->_kspAllocated) Msg::Error("Cannot change kspAllocated");
      //this->_kspAllocated=kspA;
      //restartManager::restart(this->_entriesPreAllocated);
      //restartManager::restart(this->_matrixChangedSinceLastSolve);
      //restartManager::restart(this->_valuesNotAssembled);
      //restart_petsc_array(this->_a,_nbRows,_nbRows);
      //restart_petsc_array(this->_b,_nbRows);
      //restart_petsc_array(this->_x,_nbRows);
       //KSP
      //restartManager::restart(this->_localRowStart);
      //restartManager::restart(this->_localRowEnd);
      //restartManager::restart(this->_localSize);
      //restartManager::restart(this->_globalSize);


      //nlprintPETScVector(_xsol,nbRows,"sol");
      restart_petsc_array(_xsol,nbRows);
      restart_petsc_array(_xprev,nbRows);
      restart_petsc_array(_xinit,nbRows);
      //restart_petsc_array(_xtemp,nbRows);
      //restart_petsc_array(_xitels,nbRows);
      restart_petsc_array(_Fext,nbRows);
      restart_petsc_array(_Fextprev,nbRows);
      restart_petsc_array(_Fint,nbRows);
      restart_petsc_array(_FextpFextprev,nbRows);
      //restart_petsc_array(_Deltax,nbRows);
      restartManager::restart(_timeStep);

      //this->nextStep();
      //zeroRightHandSide();

  #if defined(HAVE_MPI)
      //restartManager::restart(this->_rootRank);
      //restartManager::restart(this->_otherRanks);
  #endif

      //restartManager::restart(_flagb); //we cannot restart this one
      //restartManager::restart(_resNeg);
      //restartManager::restart(_resPos);
      //restartManager::restart(_resLsIte);
      //restartManager::restart(_res0);
      //restartManager::restart(_lsite);
      //restartManager::restart(_lsitemax);
      //nlprintPETScVector(_xsol,_nbRows,"sol");
    }
    else
    {
      Msg::Error("Cannot restart solver: not allocated");
    }
  }

  virtual void printAll()
  {
    nlprintPETScVector(_xsol,_nbRows,"sol");
    //nlprintPETScVector(_xprev,_nbRows,"prev");
    //nlprintPETScVector(_xinit,_nbRows,"init");
    //nlprintPETScVector(_Fext,_nbRows,"fext");
    nlprintPETScVector(_Fint,_nbRows,"fint");
  }
  virtual void restart_petsc_array(Vec &v, int nbrow)
  {
    PetscScalar* tarray;
    _try(VecGetArray(v,&tarray));
    restartManager::restart(tarray,nbrow);
    _try(VecRestoreArray(v,&tarray));

  }
  virtual void restart_petsc_array(Mat &v, int nbrow, int nbcol)
  {
    PetscScalar* tarray;
    _try(MatDenseGetArray(v,&tarray));
    restartManager::restart(tarray,nbrow*nbcol);
    _try(MatDenseRestoreArray(v,&tarray));
  }
  virtual void getFromVec(Vec &v, int row, scalar &val) const
  {
    #if defined(PETSC_USE_COMPLEX)
    PetscScalar *tmp;
    _try(VecGetArray(v, &tmp));
    PetscScalar s = tmp[row];
    _try(VecRestoreArray(v, &tmp));
    val = s.real();
    #else
      _try(VecGetValues(v, 1, &row, &val));
    #endif //PETSC_USE_COMPLEX
  }
  virtual void insertInVec(Vec &v, const int i, scalar val){
    _try(VecSetValues(v, 1, &i, &val, INSERT_VALUES));
  }
  virtual void getFromSolution(int row, scalar &val) const
  {
    #if defined(PETSC_USE_COMPLEX)
    PetscScalar *tmp;
    _try(VecGetArray(_xsol, &tmp));
    PetscScalar s = tmp[row];
    _try(VecRestoreArray(_xsol, &tmp));
    val = s.real();
    #else
      _try(VecGetValues(_xsol, 1, &row, &val));
    #endif //PETSC_USE_COMPLEX
  }

  virtual int systemSolve(){
		#if defined HAVE_MPI
    int comSize = 1;
    MPI_Comm_size(this->getComm(),&comSize);

		if (Msg::GetCommRank() == this->_rootRank)
    #endif //HAVE_MPI
		{
			// _b = _Fext - _Fint Normally done via the called of normInfRightHandSide
			if(!_flagb){
			 #if defined(HAVE_MPI)
				if(comSize > 1)
				{
					_try(VecAssemblyBegin(_Fext));
					_try(VecAssemblyEnd(_Fext));
					_try(VecAssemblyBegin(_Fint));
					_try(VecAssemblyEnd(_Fint));
				}
			 #endif // HAVE_MPI
				_try(VecCopy(_Fext,this->_b));
				_try(VecAXPY(this->_b,-1,_Fint));
				_flagb = true;
			}
      // implementation based on the following work
			// Matthies, Hermann, and Gilbert Strang. 
			// "The solution of nonlinear finite element equations." 
			// International journal for numerical methods in engineering 14.11 (1979): 1613-1626.
			//
      if (_withBFGS)
      {
        Msg::Info("BFGS iteration = %d BFGSDeltas.size=%d BFGSGammas.size = %d",
                _BFGSCounter, _BFGSDeltas.size(), _BFGSGammas.size());
        if (_BFGSCounter == 0)
        {
          // solve system
          int ok = linearSystemPETSc<scalar>::systemSolve();
          if (ok != 1)
          {
            return 0;
          }
          if (_BFGSDeltas.find(_BFGSCounter+1) == _BFGSDeltas.end())
          {
            Vec& v = _BFGSDeltas[_BFGSCounter+1];
            _try(VecDuplicate(this->_x, &v));
          }
          _try(VecCopy(this->_x, _BFGSDeltas[_BFGSCounter+1]));
          _try(VecCopy(this->_b, _lastResidual));
          _try(VecScale(_lastResidual, -1.));
        }
        else
        {
          double val;
          Vec q;
          _try(VecDuplicate(this->_b, &q));
          _try(VecCopy(this->_b, q));
          _try(VecScale(q, -1));
          
          if (_BFGSGammas.find(_BFGSCounter) == _BFGSGammas.end())
          {
            Vec& v = _BFGSGammas[_BFGSCounter];
            _try(VecDuplicate(this->_x, &v));
          }
          _try(VecAXPBYPCZ(_BFGSGammas[_BFGSCounter], 1., -1., 0., q, _lastResidual));
          _try(VecDot(_BFGSDeltas[_BFGSCounter], _BFGSGammas[_BFGSCounter], &val));
          _BFGSRhos[_BFGSCounter] = 1./val;
          
          _try(VecCopy(q, _lastResidual));        
          //
          std::vector<double> alphas(_BFGSCounter,0.);
          for (int j= _BFGSCounter-1; j>-1; j--)
          {
            _try(VecDot(_BFGSDeltas[j+1], q, &val));
            alphas[j] = _BFGSRhos[j+1]*val;
            _try(VecAXPY(q, -alphas[j], _BFGSGammas[j+1]));
          }
          _try(VecCopy(q, this->_b));
          int ok = linearSystemPETSc<scalar>::systemSolve();
          if (ok != 1)
          {
            return 0;
          }
          for (int j=1; j< _BFGSCounter+1; j++)
          {
            _try(VecDot(_BFGSGammas[j], this->_x, &val));
            _try(VecAXPY(this->_x, alphas[j-1]-_BFGSRhos[j]*val, _BFGSDeltas[j]));
          }
           _try(VecDestroy(&q));
           // update solution
          _try(VecScale(this->_x, -1.));
          if (_BFGSDeltas.find(_BFGSCounter+1) == _BFGSDeltas.end())
          {
            Vec& v = _BFGSDeltas[_BFGSCounter+1];
            _try(VecDuplicate(this->_x, &v));
          }
          _try(VecCopy(this->_x, _BFGSDeltas[_BFGSCounter+1]));
        }
        _BFGSCounter += 1;
      }
      else
      {
        // solve system
        int ok = linearSystemPETSc<scalar>::systemSolve();
        if (ok != 1)
        {
          return 0;
        }
        _try(VecCopy(this->_b, _lastResidual));
        _try(VecScale(_lastResidual, -1.));
      }
      
			// print the matrix (debug purpose)
			// nlprintPETScMatrix(this->_a,_nbRows,_nbRows,"K");

			// initialisation of line search
			if(_lineSearch)
			{
				_try(VecCopy(_xsol,_xitels)); // copy last solution vector
				_try(VecDot(_lastResidual,this->_x,&_resNeg));
				// other initial values
				_alphaRneg = 0.;
				_alphaRpos = 1.;
				_alphaLsPrev = 1.;
				_alphaLsIte = 1.;
				_resPos = 0.;
				_res0 = _resNeg;
				_lsite = 0;
			}

			// update xsol (for line search alpha0 == 1)
			_try(VecAXPY(_xsol,1.,this->_x));
		}
    return 1;
  }
  virtual void getFromRightHandSide(int row, scalar &val) const{
    #if defined(HAVE_MPI)
    if (Msg::GetCommSize() >1 and (row < this->_localRowStart || row >= this->_localRowEnd))
    {
      val = 0.;
      return;
    }
    #endif //HAVE_MPI
    #if defined(PETSC_USE_COMPLEX)
    PetscScalar *tmp;
    _try(VecGetArray(_Fext, &tmp));
    PetscScalar s = tmp[row];
    _try(VecRestoreArray(_Fext, &tmp));
    // FIXME specialize this routine
    val = s.real();
    #else
    VecGetValues(_Fext, 1, &row, &val);
    #endif
  }
  virtual void getFromRightHandSidePlus(int row, scalar &val) const{
    #if defined(HAVE_MPI)
    if (Msg::GetCommSize() >1 and (row < this->_localRowStart || row >= this->_localRowEnd))
    {
      val = 0.;
      return;
    }
    #endif //HAVE_MPI
    #if defined(PETSC_USE_COMPLEX)
    PetscScalar *tmp;
    _try(VecGetArray(_Fext, &tmp));
    PetscScalar s = tmp[row];
    _try(VecRestoreArray(_Fext, &tmp));
    // FIXME specialize this routine
    val = s.real();
    #else
    VecGetValues(_Fext, 1, &row, &val);
    #endif
  }
  // retrieve the value _Fint and not -_Fint
  virtual void getFromRightHandSideMinus(int row, scalar &val) const{
    #if defined(HAVE_MPI)
    if (Msg::GetCommSize() >1 and (row < this->_localRowStart || row >= this->_localRowEnd))
    {
      val = 0.;
      return;
    }
    #endif //HAVE_MPI
    #if defined(PETSC_USE_COMPLEX)
    PetscScalar *tmp;
    _try(VecGetArray(_Fint, &tmp));
    PetscScalar s = tmp[row];
    _try(VecRestoreArray(_Fint, &tmp));
    // FIXME specialize this routine
    val = s.real();
    #else
    VecGetValues(_Fint, 1, &row, &val);
    #endif
  }

  virtual void addToRightHandSide(int row, const scalar &val, int ith=0){ // kept for linear constraint
    PetscInt i = row;
    PetscScalar s = val;
    _try(VecSetValues(_Fext, 1, &i, &s, ADD_VALUES));
  }
  virtual void addToSolution(int row, const scalar& val){
    PetscInt i = row;
    PetscScalar s = val;
    _try(VecSetValues(_xsol, 1, &i, &s, ADD_VALUES));
  }

  virtual void addToRightHandSidePlus(int row, const scalar &val){
    PetscInt i = row;
    PetscScalar s = val;
    _try(VecSetValues(_Fext, 1, &i, &s, ADD_VALUES));
  }
  virtual void addToRightHandSideMinus(int row, const scalar &val){
    PetscInt i = row;
    PetscScalar s = val;
    _try(VecSetValues(_Fint, 1, &i, &s, ADD_VALUES));
  }


  virtual void zeroRightHandSide(){
    linearSystemPETSc<scalar>::zeroRightHandSide();
    _flagb = false;
    if (_allocatednldata) {
      _try(VecAssemblyBegin(_Fext));
      _try(VecAssemblyEnd(_Fext));
      _try(VecZeroEntries(_Fext));
      _try(VecAssemblyBegin(_Fint));
      _try(VecAssemblyEnd(_Fint));
      _try(VecZeroEntries(_Fint));
    }
  }

  virtual void zeroSolution(){
    _try(VecAssemblyBegin(_xsol));
    _try(VecAssemblyEnd(_xsol));
    _try(VecZeroEntries(_xsol));
  };

  virtual double normInfSolution() const{
    PetscReal norm;
    _try(VecAssemblyBegin(_xsol));
    _try(VecAssemblyEnd(_xsol));
    _try(VecNorm(_xsol,NORM_INFINITY,&norm));
    return norm;
  }
  virtual double normInfRightHandSide() const{
    double normInfRHS = 0.;

    #if defined HAVE_MPI
    int comSize = 1;
    MPI_Comm_size(this->getComm(),&comSize);

    if (Msg::GetCommRank() == this->_rootRank)
    #endif //HAVE_MPI
    {
      // compute b
      if(!_flagb){
        /* For MPI Assemblebegin and end have to be called */
       #if defined(HAVE_MPI)
        if(comSize > 1)
        {
          _try(VecAssemblyBegin(_Fext));
          _try(VecAssemblyEnd(_Fext));
          _try(VecAssemblyBegin(_Fint));
          _try(VecAssemblyEnd(_Fint));
        }
       #endif // HAVE_MPI

        _try(VecCopy(_Fext,this->_b));
        _try(VecAXPY(this->_b,-1,_Fint));
        _flagb = true;
      }
      normInfRHS = linearSystemPETSc<scalar>::normInfRightHandSide();
    }

    #if defined(HAVE_MPI)
    if ((comSize ==1) and (this->_otherRanks.size() > 0)){
      MPI_Bcast(&normInfRHS,1,MPI_DOUBLE,0,MPI_COMM_WORLD);
    }
    #endif // HAVE_MPI

    return normInfRHS;
  }
  virtual double norm0Inf() const{
    double norm0RHS= 0.;
    #if defined HAVE_MPI
    int comSize = 1;
    MPI_Comm_size(this->getComm(),&comSize);

		if (Msg::GetCommRank() == this->_rootRank)
    #endif //HAVE_MPI
    {
      PetscReal norFext;
      PetscReal norFint;
      _try(VecNorm(_Fext, NORM_INFINITY, &norFext));
      _try(VecNorm(_Fint, NORM_INFINITY, &norFint));

      norm0RHS = norFext + norFint;
    }

    #if defined(HAVE_MPI)
    if ((comSize ==1) and (this->_otherRanks.size() > 0)){
      MPI_Bcast(&norm0RHS,1,MPI_DOUBLE,0,MPI_COMM_WORLD);
    }
    #endif // HAVE_MPI
    return norm0RHS;
  }
  // function for linear search iteration return 1 when line search is converged
  virtual int lineSearch()
  {
    if(!_lineSearch) return 1;
    // compute (Fext-Fint)
    Msg::Info("Line search iteration %d",_lsite);
    if(!_flagb){
      _try(VecCopy(_Fext,this->_b));
      _try(VecAXPY(this->_b,-1,_Fint));
    }
    _try(VecDot(this->_b,this->_x,&_resLsIte));
    Msg::Info("Residu of line search %e relatif res %e alpha %e",_resLsIte,_resLsIte/_res0,_alphaLsIte);
    _resLsIte = - _resLsIte; // due to definition of _b that is -residu
    // new alpha value
    if(_lsite == 0)
    {
      if(_resLsIte < 0)
      {
        _alphaRpos+=1.; // for first itration we want _resPos > 0 increase alpha from 1 if it is not the case
        _alphaLsIte = _alphaRpos;
        if(_alphaRpos > 3)
        {
          _alphaLsIte = 1.;
          _try(VecWAXPY(_xsol,_alphaLsIte,this->_x,_xitels));
          return 1;
        }
      }
      else
      {
        _lsite++; // increase iteration counter;
        _resPos = _resLsIte;
        // new alpha
        _alphaLsPrev = _alphaLsIte;
        _alphaRpos = _alphaLsIte;
        _alphaLsIte = _alphaRneg - _resNeg*(_alphaRpos-_alphaRneg)/(_resPos - _resNeg);
      }
    }
    else if(_lsite > _lsitemax) // divergence of process take 1
    {
      // new positions
      _alphaLsIte = 1.;
      _try(VecWAXPY(_xsol,_alphaLsIte,this->_x,_xitels));
      return 1;
    }
    else
    {
      _lsite++; // increase iteration counter;
      // check convergence on residu
      double relres = _resLsIte / _res0;
      if(relres < 0.) relres = -relres;
      if(relres < _Tolls) return 1; // achieve convergence
      // check convergence on alpha
      relres = (_alphaLsIte - _alphaLsPrev) / _alphaLsPrev;
      if(relres < 0.) relres = -relres;
      if(relres < _Tolls) return 1; // achieve convergence

      // update value of residu
      _alphaLsPrev = _alphaLsIte;
      if(_resLsIte <= 0.)
      {
        _resNeg = _resLsIte;
        _alphaRneg = _alphaLsIte;
      }
      else
      {
        _resPos = _resLsIte;
        _alphaRpos = _alphaLsIte;
      }
      // new alpha
      _alphaLsIte = _alphaRneg - _resNeg*(_alphaRpos-_alphaRneg)/(_resPos - _resNeg);
    }

    // new positions
    _try(VecWAXPY(_xsol,_alphaLsIte,this->_x,_xitels));
//    _try(VecWAXPY(_xsol,1.,this->_x,_xitels));
    return 0;
  }
  virtual void resetUnknownsToPreviousTimeStep()
  {
    _try(VecCopy(_xprev,_xsol));
    _BFGSCounter = 0;
    _try(VecAssemblyBegin(_lastResidual));
    _try(VecAssemblyEnd(_lastResidual));
    _try(VecZeroEntries(_lastResidual));
    
    if (_withBFGS)
    {
      for (std::map<int,Vec>::iterator it = _BFGSDeltas.begin(); it != _BFGSDeltas.end(); it ++)
      {
        _try(VecAssemblyBegin(it->second));
        _try(VecAssemblyEnd(it->second));
        _try(VecZeroEntries(it->second));
      } 
      for (std::map<int,Vec>::iterator it = _BFGSGammas.begin(); it != _BFGSGammas.end(); it ++)
      {
        _try(VecAssemblyBegin(it->second));
        _try(VecAssemblyEnd(it->second));
        _try(VecZeroEntries(it->second));
      }
    }
    
  }
  virtual void nextStep()
  {
    /* For MPI Assemblebegin and end have to be called */
   #if defined(HAVE_MPI)
    if(Msg::GetCommSize() > 1)
    {
      _try(VecAssemblyBegin(_xsol));
      _try(VecAssemblyEnd(_xsol));
      _try(VecAssemblyBegin(_xprev));
      _try(VecAssemblyEnd(_xprev));
    }
   #endif // HAVE_MPI
    _try(VecCopy(_xsol,_xprev));
    _BFGSCounter = 0;
    _try(VecAssemblyBegin(_lastResidual));
    _try(VecAssemblyEnd(_lastResidual));
    _try(VecZeroEntries(_lastResidual));
    
    if (_withBFGS)
    {
      for (std::map<int,Vec>::iterator it = _BFGSDeltas.begin(); it != _BFGSDeltas.end(); it ++)
      {
        _try(VecAssemblyBegin(it->second));
        _try(VecAssemblyEnd(it->second));
        _try(VecZeroEntries(it->second));
      } 
      for (std::map<int,Vec>::iterator it = _BFGSGammas.begin(); it != _BFGSGammas.end(); it ++)
      {
        _try(VecAssemblyBegin(it->second));
        _try(VecAssemblyEnd(it->second));
        _try(VecZeroEntries(it->second));
      }
    }
  }
  virtual double getExternalWork(const int syssize) const
  {
    PetscScalar wext;
    _try(VecCopy(_Fextprev,_FextpFextprev));
    _try(VecAXPY(_FextpFextprev,1,_Fext));
    _try(VecCopy(_xsol,_Deltax));
    _try(VecAXPY(_Deltax,-1,_xprev));
    // put zero for displacement of rigid contact node (avoid to take into account this contribution)
    PetscScalar s = 0.;
    for(int i=syssize; i<this->_nbRows;i++)
    {
      _try(VecSetValues(_Deltax, 1, &i, &s, INSERT_VALUES));
    }
     #if defined(HAVE_MPI)
      if(Msg::GetCommSize() > 1)
      {
        _try(VecAssemblyBegin(_Deltax));
        _try(VecAssemblyEnd(_Deltax));
      }
     #endif // HAVE_MPI
    _try(VecDot(_FextpFextprev,_Deltax,&wext));
    _try(VecCopy(_Fext,_Fextprev));
    return 0.5*wext;
  }
  virtual void setTimeStep(const double dt){_timeStep = dt;}
  virtual double getTimeStep() const{return _timeStep;}

  virtual double getKineticEnergy(const int syssize) const{return 0.;} // no kinetic energy in a QS scheme
  virtual void addToMatrix(int row, int col, const scalar &val, const nonLinearSystemBase::whichMatrix wm)
  {
   #ifdef _DEBUG
    if(wm!=nonLinearSystemBase::stiff)
      Msg::Warning("Try to assemble an inexisting matrix in a QS PETSc scheme");
    else
   #endif // _DEBUG
    this->linearSystemPETSc<scalar>::addToMatrix(row,col,val);
  }
  virtual void setInitialCondition(int row, scalar val, const typename nonLinearBoundaryCondition::whichCondition wc)
  {
    if(wc!=nonLinearBoundaryCondition::position)
    {
      Msg::Error("Can set a velocity or a aceleration in a QS scheme");
      return;
    }
    if(this->isAllocated())
    {
      _try(VecSetValues(_xsol, 1, &row, &val, INSERT_VALUES));
      _try(VecSetValues(_xprev, 1, &row, &val, INSERT_VALUES));
      _try(VecSetValues(_xinit, 1, &row, &val, INSERT_VALUES));
    }
    else{
      Msg::Error("Can't give an initial value in a non allocated system!");
    }
  }
  virtual void setDynamicRelaxation(const bool dynrel)
  {
    Msg::Error("Cannot use dynamic relaxation in an QS system");
  }
  virtual void getFromSolution(int row, scalar &val, const nonLinearBoundaryCondition::whichCondition wv) const
  {
    if(wv!=nonLinearBoundaryCondition::position)
    {
      Msg::Error("Cannot take velocity or acceleration in a QS scheme");
      val=0.;
      return;
    }
    this->getFromSolution(row,val);
  }

  virtual void getFromMatrix(int row, int col, scalar &val,const nonLinearSystemBase::whichMatrix wm) const
  {
    if(wm!=nonLinearSystemBase::stiff)
    {
      Msg::Error("A QS system has only a stiffness matrix. So cannot get a value in another matrix");
      val=0;
      return;
    }
    this->linearSystemPETSc<scalar>::getFromMatrix(row,col,val);
  }

  #if defined(HAVE_MPI)
  virtual double* getArrayIndexPreviousRightHandSidePlusMPI(const int index)
  {
    Msg::Error("This function cannot be used in a QS PETSc scheme"); // has to return a reference on an array Impossible with a _Vec PETSc
    return NULL;
  }
  #endif // HAVE_MPI

#else
template<class scalar>
class nonLinearSystemPETSc : public linearSystemPETSc<scalar>, public nonLinearSystem<scalar>{
	public:
		nonLinearSystemPETSc():linearSystemPETSc<scalar>(), nonLinearSystem<scalar>(){
			Msg::Error("Petsc is not available");
		};
		virtual ~nonLinearSystemPETSc(){}
		virtual void getFromRightHandSidePlus(int row, scalar &val) const{};
		virtual void getFromRightHandSideMinus(int row, scalar &val) const{};
		virtual void getFromRightHandSide(int row, scalar &val) const{};
		virtual void addToRightHandSidePlus(int row, const scalar &val){};
		virtual void addToLoadVector(int row, const scalar& val){} // this used in path following context
		virtual void zeroLoadVector() {}
    virtual void zeroMatrix(const nonLinearSystemBase::whichMatrix wm){}
		virtual void addToRightHandSideMinus(int row, const scalar &val){};
		virtual double norm0Inf() const{return 0.;};
		virtual int lineSearch(){return 0;} // if 1 the process in converged explicit no lineSearch so OK
		virtual void resetUnknownsToPreviousTimeStep(){};
		virtual void copy(const IPStateBase::whichState ws1, const IPStateBase::whichState ws2){}
		virtual void nextStep(){};
		virtual void setTimeStep(const double dt){};
		virtual double getTimeStep() const{return 0.;};
		virtual void restart(){Msg::Error("A restart file cannot be created as the function is not implemented for this scheme");}
		virtual double getExternalWork(const int syssize) const{return 0.;};
		virtual double getKineticEnergy(const int syssize) const{return 0.;};
		virtual void addToMatrix(int row, int col, const scalar &val, const nonLinearSystemBase::whichMatrix wm){};
		virtual void setInitialCondition(int row, scalar val, const typename nonLinearBoundaryCondition::whichCondition wc){};
		virtual void setDynamicRelaxation(const bool dynrel){};
		virtual void getFromSolution(int row, scalar &val, const nonLinearBoundaryCondition::whichCondition wv) const{};
		virtual void getFromMatrix(int row, int col, scalar &val,const nonLinearSystemBase::whichMatrix wm) const{};
		#if defined(HAVE_MPI)
		virtual double* getArrayIndexPreviousRightHandSidePlusMPI(const int index){ return NULL;};
		#endif // HAVE_MPI

	#endif // HAVE_PETSC
};
#endif //NONLINEARSYSTEMPETSC_H_
