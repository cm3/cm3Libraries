//
// C++ Interface: group of elements
//
//
// Author:  <V.-D. Nguyen>, (C) 2020
//
// Copyright: See COPYING file that comes with this distribution
//
//

#include "elementGroup.h"
#include "elementGroup.h"
#include "GModel.h"
#include "GEntity.h"

elementGroup::elementGroup(GFace *gf)
{
  elementFilterTrivial filter;
  addElementary(gf, filter);
}

elementGroup::elementGroup(GRegion *gr)
{
  elementFilterTrivial filter;
  addElementary(gr, filter);
}

elementGroup::elementGroup(std::vector<MElement *> &elems)
{
  elementFilterTrivial filter;
  for(std::vector<MElement *>::iterator it = elems.begin(); it != elems.end();
      it++) {
    MElement *e = *it;
    if(filter(e)) {
      insert(e);
    }
  }
}

void elementGroup::addElementary(GEntity *ge, const elementFilter &filter)
{
  for(std::size_t j = 0; j < ge->getNumMeshElements(); j++) {
    MElement *e = ge->getMeshElement(j);
    if(filter(e)) {
      insert(e);
    }
  }
}

void elementGroup::addPhysical(int dim, int physical,
                                  const elementFilter &filter)
{
  std::map<int, std::vector<GEntity *> > groups[4];
  GModel::current()->getPhysicalGroups(groups);
  std::vector<GEntity *> &ent = groups[dim][physical];
  for(std::size_t i = 0; i < ent.size(); i++) {
    addElementary(ent[i], filter);
  }
}
