//
// Author:  <Van Dung NGUYEN>, (C) 2017
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef _ELEMENTEROSIONFILTER_H_
#define _ELEMENTEROSIONFILTER_H_
#include "elementGroup.h"
class IPField;
class elementErosionFilter :public elementFilter{
	protected:
		std::set<int> _erosionElements, _newErosionElements;
    int _numberOfNewBulkElementsEroded;
    int _numberOfNewInterfaceElementsEroded;
	
	public:
		elementErosionFilter(): elementFilter(), _numberOfNewBulkElementsEroded(0), _numberOfNewInterfaceElementsEroded(0){}
		virtual ~elementErosionFilter(){}
		void resetCounter()
    {
      _numberOfNewBulkElementsEroded = 0;
      _numberOfNewInterfaceElementsEroded = 0;
      _newErosionElements.clear();
    };
    int getNumberOfNewBulkElementsEroded() const {return _numberOfNewBulkElementsEroded;};
    int getNumberOfNewInterfaceElementsEroded() const {return _numberOfNewInterfaceElementsEroded;};
		void addErodedElement(MElement* ele, bool interface);
    void nextStep();
    void resetToPreviousStep(IPField* ipf);
		void restart();
		void print() const{
			if (_erosionElements.size() == 0){
				//printf("No element is eroded\n");
			}
			else{
				#if defined(HAVE_MPI)
				if (Msg::GetCommSize() > 1){
					printf("on rank %d, %zd eroded elements: \n",Msg::GetCommRank(),_erosionElements.size());
				}
				else
				#endif //HAVE_MPI
				{
					printf("%zd eroded elements: \n",_erosionElements.size());
				}
				for (std::set<int>::const_iterator it = _erosionElements.begin(); it != _erosionElements.end(); it++){
					printf("%d ",*it);
				}
				printf("\n");
			}
		}
		
		virtual bool operator() (MElement *ele) const {
			if (_erosionElements.size() == 0) return true;
			else if (_erosionElements.find(ele->getNum()) != _erosionElements.end()){
				return false;
			}
			else return true;
		};
};

#endif // _ELEMENTEROSIONFILTER_H_
