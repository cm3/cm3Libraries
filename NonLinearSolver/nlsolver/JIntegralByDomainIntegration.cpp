
//
// Description: JIntegral
//
//
// Author:  <Van-Dung NGUYEN>, (C) 2023
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "JIntegralByDomainIntegration.h"
#include "laplaceSolver.h"
#include "nonLinearMechSolver.h"
#include "ipFiniteStrain.h"

#if defined(HAVE_MPI)
#include "mpi.h"
#endif //HAVE_MPI


bool computeJElementFilterLayer::operator()(MElement *ele) const 
{
  int numVer = ele->getNumVertices();
  for (int i=0; i< numVer; i++)
  {
    MVertex* v = ele->getVertex(i);
    if (!(inside(v)))
    {
      return false;
    }
  }
  return true;
};
  
bool computeJElementFilterLayer::inside(MVertex* v) const
{
  if (_direction == 0)
  {
    double x = v->x();
    if (x < _lowerBound)
    {
      return false;
    }
    else if (x > _upperBound)
    {
      return false;
    }
    else
    {
      return true;
    }
  }
  else if (_direction == 1)
  {
    double y = v->y();
    if (y < _lowerBound)
    {
      return false;
    }
    else if (y > _upperBound)
    {
      return false;
    }
    else
    {
      return true;
    }
  }
  else if (_direction == 2)
  {
    double z = v->z();
    if (z < _lowerBound)
    {
      return false;
    }
    else if (z > _upperBound)
    {
      return false;
    }
    else
    {
      return true;
    }
    
  }
  else
  {
    Msg::Error("direction %d is not defined",_direction);
  }
};

computeJIntegral::computeJIntegral(const std::string name): _laplaceSolver(NULL), _pFile(NULL), _name(name),
  _lastTime(0.),_lastIterationIndex(0)
{
  _plan = new computedJPlanTrivial();
  _elementFilter = new computeJElementFilterTrivial();
};
                        
computeJIntegral::~computeJIntegral()
{
  if (_laplaceSolver != NULL)
  {
    delete _laplaceSolver;
  }
  closeFile();
  if (_plan != NULL)
  {
    delete _plan; _plan=NULL;
  }
  if (_elementFilter) 
  {
    delete _elementFilter;
    _elementFilter = NULL;
  }
}

void computeJIntegral::setIntegratedDomain(int dim, int physical)
{
  _integratedDomain.push_back(std::pair<int,int>(dim,physical));
}

void computeJIntegral::setInnerBoundary(int dim, int physical)
{
  _innerBoundary.push_back(std::pair<int,int>(dim,physical));
}
void computeJIntegral::setOuterBoundary(int dim, int physical)
{
  _outerBoundary.push_back(std::pair<int,int>(dim,physical));
};

void computeJIntegral::setElementFilter(const computeJElementFilter& eleFilter)
{
  if (_elementFilter != NULL)
  {
    delete _elementFilter;
  }
  _elementFilter = eleFilter.clone();
}

void computeJIntegral::openFile()
{
  #if defined(HAVE_MPI)
  if (Msg::GetCommRank() == 0)
  #endif
  {
    std::string fname = "J"+_name+ ".csv";
    _pFile = fopen(fname.c_str(),"w");
    fprintf(_pFile, "Time;Step;J0;J1;J2\n");
    fflush(_pFile);
  }
};

void computeJIntegral::closeFile()
{
  if (_pFile != NULL)
  {
    fclose(_pFile);
  }
  _pFile = NULL;
};

void computeJIntegral::setComputeJPlan(const computeJPlan& pl)
{
  if (_plan != NULL)
  {
    delete _plan;
  } 
  _plan = pl.clone();
}

void computeJIntegral::computeJAndWriteData(nonLinearMechSolver* solver, bool force)
{
  if (_pFile == NULL)
  {
    openFile();
  }
  double curTime = solver->getTimeManager()->getLastTime();
  int step = solver->getTimeManager()->getLastIterationIndex();
  if (curTime > _lastTime or step > _lastIterationIndex)
  {
    _lastTime = curTime;
    _lastIterationIndex = step;
    if (_plan->willCompute(curTime,step) or force)
    {
      std::vector<double> J = computeJ(solver);
      if (_pFile != NULL)
      {
        fprintf(_pFile,"%.16g;%d;%.16g;%.16g;%.16g\n",curTime,step,J[0],J[1],J[2]);
        fflush(_pFile);
      }
    };    
  }
};

std::vector<double> computeJIntegral::computeJ(nonLinearMechSolver* solver)
{
  Msg::Info("start computing J with %s",_name.c_str());
  if (solver != NULL)
  {
    solver->getGModel()->setAsCurrent();
  }
  if (_laplaceSolver == NULL)
  {
    _laplaceSolver = new LaplaceSolver();
    for (int i=0; i< _integratedDomain.size(); i++)
    {
      _laplaceSolver->addDomainWithElementFilter(_integratedDomain[i].first, _integratedDomain[i].second, *_elementFilter);
    }
    
    if (_laplaceSolver->getElementGroup()->size() == 0)
    {
      Msg::Error("no element inside the integration domain");
      return std::vector<double>(3,0.);
    }
    
    for (int i=0; i< _innerBoundary.size(); i++)
    {
      int dim = _innerBoundary[i].first;
      int physical = _innerBoundary[i].second;
      elementGroup gr(dim, physical);
      for (elementGroup::vertexContainer::const_iterator itv = gr.vbegin(); itv != gr.vend(); itv++)
      {
        if (_laplaceSolver->getElementGroup()->find(itv->second))
        {
          _laplaceSolver->setVertexValue(itv->second,1.);
        }
      }
    }
    
    for (int i=0; i< _outerBoundary.size(); i++)
    {
      int dim = _outerBoundary[i].first;
      int physical = _outerBoundary[i].second;
      elementGroup gr(dim, physical);
      for (elementGroup::vertexContainer::const_iterator itv = gr.vbegin(); itv != gr.vend(); itv++)
      {
        if (_laplaceSolver->getElementGroup()->find(itv->second))
        {
          _laplaceSolver->setVertexValue(itv->second,0.);
        }
      }
    }
    _laplaceSolver->solve();
    
    #if defined(HAVE_MPI)
    if (Msg::GetCommRank()==0)
    #endif
    {
      _laplaceSolver->builFieldView("qvalue-field"+_name+".msh");
    }
  }
  
  elementGroup* eleGr = _laplaceSolver->getElementGroup();
  if (eleGr->size() == 0)
  {
    Msg::Error("no element inside the integration domain");
    return std::vector<double>(3,0.);
  }
  std::vector<partDomain*>& allDomains = *(solver->getDomainVector());
  IPField* ipf = solver->getIPField();
  std::vector<double> J(3,0);

  std::map<MElement*, partDomain*> eleDomainMap;
  for (int i=0; i< allDomains.size(); i++)
  {
    for (elementGroup::elementContainer::const_iterator ite = allDomains[i]->element_begin(); ite != allDomains[i]->element_end(); ite++)
    {
      MElement* ele = ite->second;
      eleDomainMap[ele] = allDomains[i];
    };
  };
  
  IntPt* GP;
  const ipFiniteStrain *vipv[256];
  for (elementGroup::elementContainer::const_iterator it = eleGr->begin(); it != eleGr->end(); ++it)
  {
    MElement *ele = it->second;
    std::map<MElement*, partDomain*>::iterator itF = eleDomainMap.find(ele);
    if (itF != eleDomainMap.end())
    {
      ipf->getIPv(ele,vipv);
      int npts_bulk=itF->second->getBulkGaussIntegrationRule()->getIntPoints(ele,&GP);
      for (int i=0; i<npts_bulk; i++)
      {
        double weight= GP[i].weight;
        double u = GP[i].pt[0];
        double v = GP[i].pt[1];
        double w = GP[i].pt[2];
        double detJ = ele->getJacobianDeterminant(u,v,w);
        const STensor3& P = vipv[i]->getConstRefToFirstPiolaKirchhoffStress();
        const STensor3& F = vipv[i]->getConstRefToDeformationGradient();
        double potentialEnergy = vipv[i]->defoEnergy()+vipv[i]->plasticEnergy();
        //Msg::Info("potentialEnergy=%e",potentialEnergy);
        
        STensor3 gradU(-1.);
        gradU += F;
        
        SVector3 grad_q = _laplaceSolver->getGradField(ele,u,v,w);
        for (int k=0; k< 3; k++)
        {
          J[k] -= potentialEnergy*grad_q[k]*weight*detJ;
          for (int p=0; p<3; p++)
          {
            for (int r=0; r<3; r++)
            {
                J[k] += P(p,r)*gradU(p,k)*grad_q[r]*weight*detJ;
            }
          }
        }
      };
    }
  }
  Msg::Info("done computing J with %s",_name.c_str());
  #if defined(HAVE_MPI)
  if (Msg::GetCommSize() >1)
  {
    MPI_Allreduce(MPI_IN_PLACE,&J[0],3,MPI_DOUBLE,MPI_SUM,MPI_COMM_WORLD);
  }
  #endif //HAVE_MPI
  
  return J;
};
