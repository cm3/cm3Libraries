//
// Description: System for non linear static scheme
//
//
// Author:  <Gauthier BECKER>, (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
//
// The system solve K Delta x = F so add a vector xsol which contains the actual value of unknowns

#ifndef NONLINEARSYSTEMGMRESK_H_
#define NONLINEARSYSTEMGMRESK_H_

#include "nonLinearSystems.h"
// GMREs
#include "linearSystemGMRESk.h"
#if defined(HAVE_BLAS)

// extern blas function declaration
#if !defined(F77NAME)
#define F77NAME(x) (x##_)
#endif // F77NAME
extern "C" {
  void F77NAME(daxpy)(int *n, double *alpha, double *x, int *incx, double *y, int *incy);
  void F77NAME(dscal)(int *n, double *alpha,double *x,  int *incx);
  void F77NAME(dcopy)(int *n, double *x,int *incx, double *y, int *incy);
  double F77NAME(ddot)(int *n,double *x,int *incx, double *y, int *incy);
  int F77NAME(idamax)(int *n,double *x,int *incx);
}
#endif // HAVE_BLAS
template<class scalar>
class nonLinearSystemGMRESk : public linearSystemGMRESk<scalar>, public nonLinearSystem<scalar>{
 private:
  bool _allocatednldata;
  mutable bool _flagb;
 protected:
  double* _xsol;  // _xsol = _xitels + alpha * _x; (alpha == line search parameter)
  double* _xitels; // for line search the value of solution as to be kept
  double* _xprev; // solution of previous time step
  double* _Fext;
  double* _Fint;
  double _timeStep;
  mutable double* _Fextprev;
  mutable double* _FextpFextprev;
  mutable double* _Deltax;
  // data for line search
  const double _Tolls; // tolerance for line search
  bool _lineSearch;
  double _alphaRneg, _alphaRpos, _alphaLsIte, _alphaLsPrev;
  double _resNeg, _resPos, _resLsIte, _res0;
  int _lsite,_lsitemax;
 public:
  nonLinearSystemGMRESk(const bool ls=false, const double tolls=1e-6,
                       const int lsitemax=10) : linearSystemGMRESk<scalar>(), nonLinearSystem<scalar>(),
                                                                 _allocatednldata(false), _flagb(false),
                                                                 _lineSearch(ls), _Tolls(tolls),
                                                                 _alphaRneg(0.), _alphaRpos(1.), _resNeg(0.), _resPos(0), _lsite(0),
                                                                 _alphaLsIte(0.), _resLsIte(0.), _res0(1.), _timeStep(0.),
                                                                 _alphaLsPrev(1.), _lsitemax(lsitemax){}
  ~nonLinearSystemGMRESk(){} // _xsol is destroied by clear() called by ~linearSystemPETSc()

  virtual void allocate(int nbRows){
    linearSystemGMRESk<scalar>::allocate(nbRows);
    _xsol = new double[this->_nbColumns];
    _xprev = new double[this->_nbColumns];
    _xitels = new double[this->_nbColumns];
    _Fext = new double[this->_nbColumns];
    _Fint = new double[this->_nbColumns];
    _FextpFextprev = new double[this->_nbColumns];
    _Fextprev = new double[this->_nbColumns];
    _Deltax = new double[this->_nbColumns];
    _allocatednldata=true;
  }
  virtual void clear(){
    linearSystemGMRESk<scalar>::clear();
    if(_allocatednldata){
      delete[] _xsol;
      delete[] _xprev;
      delete[] _xitels;
      delete[] _Fext;
      delete[] _Fint;
      delete[] _Fextprev;
      delete[] _FextpFextprev;
      delete[] _Deltax;
    }
    _allocatednldata = false;
    _flagb = false;
  }
  virtual void getFromSolution(int row, scalar &val) const
  {
    val = _xsol[row];
  }
  virtual int systemSolve(){
    // _b = _Fext - _Fint Normally done via the called of normInfRightHandSide
    if(!_flagb){
     #if defined(HAVE_BLAS)
      int INCX = 1;
      double alpha = -1.;
      F77NAME(dcopy)(&(this->_nbColumns),_Fext,&INCX,this->_b,&INCX);
      F77NAME(daxpy)(&(this->_nbColumns),&alpha,_Fint,&INCX,_Fext,&INCX);
     #else
      for(int i=0;i<this->_nbColumns;i++) this->_b[i] = _Fext[i] - _Fint[i];
     #endif // HAVE_BLAS
      _flagb = true;
    }
    // solve system
    linearSystemGMRESk<scalar>::systemSolve();

    // initialisation of line search
    if(_lineSearch)
    {
     #if defined(HAVE_BLAS)
      int INCX = 1;
      F77NAME(dcopy)(&(this->_nbColumns),_xsol,&INCX,_xitels,&INCX);
      _resNeg = F77NAME(ddot)(&(this->_nbColumns),this->_b,&INCX,this->_x,&INCX);
     #else
      _resNeg = 0.;
      for(int i=0;i<this->_nbColumns;i++)
      {
        _xitels[i] = _xsol[i];
        _resNeg+= this->_b[i]*this->_x[i];
      }
     #endif
      _resNeg =-_resNeg; // because b == - residu;
      // other initial values
      _alphaRneg = 0.;
      _alphaRpos = 1.;
      _alphaLsPrev = 1.;
      _alphaLsIte = 1.;
      _resPos = 0.;
      _res0 = _resNeg;
      _lsite = 0;
    }

    // update xsol (for line search alpha0 == 1)
   #if defined(HAVE_BLAS)
    int INCX = 1;
    double alpha = 1.;
    F77NAME(daxpy)(&(this->_nbColumns),&alpha,this->_x,&INCX,_xsol,&INCX);
   #else
    for(int i=0;i<this->_nbColumns;i++) _xsol[i] += this->_x[i];
   #endif // HAVE_BLAS
    return 1;
  }
  virtual void getFromRightHandSide(int row, scalar &val) const{
    val = _Fext[row];
  }
  virtual void getFromRightHandSidePlus(int row, scalar &val) const{
    val = _Fext[row];
  }
  // retrieve the value _Fint and not -_Fint
  virtual void getFromRightHandSideMinus(int row, scalar &val) const{
    val = _Fint[row];
  }

  virtual void addToRightHandSide(int row, const scalar &val, int ith=0){ // kept for linear constraint
    _Fext[row] += val;
  }
  virtual void addToRightHandSidePlus(int row, const scalar &val){
    _Fext[row] += val;
  }
  virtual void addToRightHandSideMinus(int row, const scalar &val){
    _Fint[row] += val;
  }
  virtual void restart(){

    int nbRows=this->_nbColumns;
    int trow = nbRows;
    restartManager::restart(trow);
    if(trow != (int) nbRows) Msg::Error("Cannot load a petsc step with a different size from the saved one");

    restartManager::restart(_xsol,this->_nbColumns);
    restartManager::restart(_xitels,this->_nbColumns);
    restartManager::restart(_xprev,this->_nbColumns);
    restartManager::restart(_Fext,this->_nbColumns);
    restartManager::restart(_Fint,this->_nbColumns);
    restartManager::restart(_timeStep);

    Msg::Error("nonLinearSystemGmm::restart has not been tested yet");
  }
  
  virtual void zeroMatrix(const nonLinearSystemBase::whichMatrix wm){
    if (wm == nonLinearSystemBase::mass)
    {
      Msg::Error("this system has no mass");
    }
    else if (wm == nonLinearSystemBase::stiff)
    {
      linearSystemGMRESk<scalar>::zeroMatrix();
    }
    else
    {
      Msg::Error("matrix does not exist");
    }
  }

  virtual void zeroRightHandSide(){
    linearSystemGMRESk<scalar>::zeroRightHandSide();
    _flagb = false;
    if (_allocatednldata) {
       // dscal multiply by 0. and not set
//     #if defined(HAVE_BLAS)
//      int INCX = 1;
//      double alpha = 0.;
//      F77NAME(dscal)(&(this->_nbColumns),&alpha,_Fext,&INCX);
//      F77NAME(dscal)(&(this->_nbColumns),&alpha,_Fint,&INCX);
//     #else
      for(int i=0; i<this->_nbColumns;i++)
      {
        _Fext[i] = 0.;
        _Fint[i] = 0.;
      }
//     #endif // HAVE_BLAS
    }
  }
  virtual double normInfRightHandSide() const{
    // compute b
    if(!_flagb){
     #if defined(HAVE_BLAS)
      int INCX = 1;
      double alpha = -1.;
      int nc = this->_nbColumns;
      F77NAME(dcopy)(&nc,_Fext,&INCX,this->_b,&INCX);
      F77NAME(daxpy)(&nc,&alpha,_Fint,&INCX,_Fext,&INCX);
     #else
      for(int i=0;i<this->_nbColumns;i++) this->_b[i] = _Fext[i] - _Fint[i];
     #endif // HAVE_BLAS
      _flagb = true;
    }
    return linearSystemGMRESk<scalar>::normInfRightHandSide();
  }
  virtual double norm0Inf() const{
    double norFext;
    double norFint;
   #if defined(HAVE_BLAS)
    int INCX = 1;
    int nc = this->_nbColumns;
    int indFext = F77NAME(idamax)(&nc,_Fext,&INCX)-1; // idamax first index = 1 !!
    int indFint = F77NAME(idamax)(&nc,_Fint,&INCX)-1; // idamax first index = 1 !!
    norFext = fabs(_Fext[indFext]);
    norFint = fabs(_Fint[indFint]);
   #else
    norFext = _Fext[0];
    norFint = _Fint[0];
    if(norFext < 0) norFext = - norFext;
    if(norFint < 0) norFint = - norFint;
    for(int i=1;i<this->_nbColumns;i++)
    {
      if(_Fext[i] > norFext) norFext = _Fext[i];
      else if (-_Fext[i] > norFext) norFext = - _Fext[i];
      if(_Fint[i] > norFint) norFint = _Fint[i];
      else if(-_Fint[i] > norFint) norFint = -_Fint[i];
    }
   #endif // HAVE_BLAS
   #if defined(HAVE_MPI)
    if(Msg::GetCommSize() > 1)
    {
      double nn[2] = {norFext,norFint};
      MPI_Allreduce(MPI_IN_PLACE,nn,2,MPI_DOUBLE,MPI_MAX,MPI_COMM_WORLD);
      norFext = nn[0];
      norFint = nn[1];
    }
   #endif // HAVE_MPI
    return norFext + norFint;
  }
  // function for linear search iteration return 1 when line search is converged

  virtual int lineSearch()
  {
    if(!_lineSearch) return 1;
    // compute (Fext-Fint)
    Msg::Info("Line search iteration %d",_lsite);
    if(!_flagb){
     #if defined(HAVE_BLAS)
       int INCX = 1;
       double alpha = -1;
       int nc = this->_nbColumns;
       F77NAME(dcopy)(&nc,_Fext,&INCX,this->_b,&INCX);
       F77NAME(daxpy)(&nc,&alpha,_Fint,&INCX,this->_b,&INCX);
     #else
       for(int i=0;i<this->_nbColumns;i++) this->_b[i] = _Fext[i] - _Fint[i];
     #endif // HAVE_BLAS
     _flagb = true;
    }
   #if defined(HAVE_BLAS)
    int INCX = 1;
    _resLsIte = F77NAME(ddot)(&(this->_nbColumns),this->_b,&INCX,this->_x,&INCX);
   #else
     _resLsIte = 0.;
     for(int i=0;i<this->_nbColumns;i++) _resLsIte += this->_b[i]*this->_x[i];
   #endif // HAVE_BLAS

    Msg::Info("Residu of line search %e relatif res %e alpha %e",_resLsIte,_resLsIte/_res0,_alphaLsIte);
    _resLsIte = - _resLsIte; // due to definition of _b that is -residu
    // new alpha value
    if(_lsite == 0)
    {
      if(_resLsIte < 0)
      {
        _alphaRpos+=1.; // for first itration we want _resPos > 0 increase alpha from 1 if it is not the case
        _alphaLsIte = _alphaRpos;
        if(_alphaRpos > 3)
        {
          _alphaLsIte = 1.;
         #if defined(HAVE_BLAS)
          int INCX = 1;
          F77NAME(dcopy)(&(this->_nbColumns),_xitels,&INCX,_xsol,&INCX);
          F77NAME(daxpy)(&(this->_nbColumns),&_alphaLsIte,this->_x,&INCX,_xsol,&INCX);
         #else
          for(int i=0;i<this->_nbColumns;i++) _xsol[i] = _xitels[i] + _alphaLsIte*this->_x[i];
         #endif // HAVE_BLAS
          return 1;
        }
      }
      else
      {
        _lsite++; // increase iteration counter;
        _resPos = _resLsIte;
        // new alpha
        _alphaLsPrev = _alphaLsIte;
        _alphaRpos = _alphaLsIte;
        _alphaLsIte = _alphaRneg - _resNeg*(_alphaRpos-_alphaRneg)/(_resPos - _resNeg);
      }
    }
    else if(_lsite > _lsitemax) // divergence of process take 1
    {
      // new positions
      _alphaLsIte = 1.;
     #if defined(HAVE_BLAS)
      int INCX = 1;
      F77NAME(dcopy)(&(this->_nbColumns),_xitels,&INCX,_xsol,&INCX);
      F77NAME(daxpy)(&(this->_nbColumns),&_alphaLsIte,this->_x,&INCX,_xsol,&INCX);
     #else
      for(int i=0;i<this->_nbColumns;i++) _xsol[i] = _xitels[i] + _alphaLsIte*this->_x[i];
     #endif // HAVE_BLAS
      return 1;
    }
    else
    {
      _lsite++; // increase iteration counter;
      // check convergence on residu
      double relres = _resLsIte / _res0;
      if(relres < 0.) relres = -relres;
      if(relres < _Tolls) return 1; // achieve convergence
      // check convergence on alpha
      relres = (_alphaLsIte - _alphaLsPrev) / _alphaLsPrev;
      if(relres < 0.) relres = -relres;
      if(relres < _Tolls) return 1; // achieve convergence

      // update value of residu
      _alphaLsPrev = _alphaLsIte;
      if(_resLsIte <= 0.)
      {
        _resNeg = _resLsIte;
        _alphaRneg = _alphaLsIte;
      }
      else
      {
        _resPos = _resLsIte;
        _alphaRpos = _alphaLsIte;
      }
      // new alpha
      _alphaLsIte = _alphaRneg - _resNeg*(_alphaRpos-_alphaRneg)/(_resPos - _resNeg);
    }

    // new positions
    #if defined(HAVE_BLAS)
     F77NAME(dcopy)(&(this->_nbColumns),_xitels,&INCX,_xsol,&INCX);
     F77NAME(daxpy)(&(this->_nbColumns),&_alphaLsIte,this->_x,&INCX,_xsol,&INCX);
    #else
     for(int i=0;i<this->_nbColumns;i++) _xsol[i] = _xitels[i] + _alphaLsIte*this->_x[i];
    #endif // HAVE_BLAS
    return 0;
  }
  virtual void resetUnknownsToPreviousTimeStep()
  {
   #if defined(HAVE_BLAS)
    int INCX = 1;
    F77NAME(dcopy)(&(this->_nbColumns),_xprev,&INCX,_xsol,&INCX);
   #else
    for(int i=0;i<this->_nbColumns;i++) _xsol[i] = _xprev[i];
   #endif // HAVE_BLAS
  }
  virtual void nextStep()
  {
   #if defined(HAVE_BLAS)
    int INCX = 1;
    F77NAME(dcopy)(&(this->_nbColumns),_xsol,&INCX,_xprev,&INCX);
   #else
    for(int i=0;i<this->_nbColumns; i++) _xprev[i] = _xsol[i];
   #endif // HAVE_BLAS
  }
  virtual double getExternalWork(const int syssize) const
  {
   #if defined(HAVE_BLAS)
    int INCX = 1;
    double one = 1;
    double mone = -1;
    int nbc = (this->_nbColumns);
    int ssize = syssize;
    F77NAME(dcopy)(&nbc,_Fextprev,&INCX,_FextpFextprev,&INCX);
    F77NAME(daxpy)(&nbc,&one,_Fext,&INCX,_FextpFextprev,&INCX);
    F77NAME(dcopy)(&nbc,_xsol,&INCX,_Deltax,&INCX);
    F77NAME(daxpy)(&nbc,&mone,_xprev,&INCX,_Deltax,&INCX);
    double wext = F77NAME(ddot)(&ssize,_FextpFextprev,&INCX,_Deltax,&INCX);
    F77NAME(dcopy)(&nbc,_Fext,&INCX,_Fextprev,&INCX);
    return 0.5*wext;
   #else
    double wext=0.;
    for(int i=0;i<syssize;i++)
    {
      wext += (_Fext[i]+_Fextprev[i])*(_xsol[i]-_xprev[i]);
      _Fextprev[i] = _Fext[i];
    }
    for(int i=syssize;i<this->_nbColumns;i++)
    {
      _Fextprev[i] = _Fext[i];
    }
   #endif // HAVE_BLAS
  }
  virtual double getKineticEnergy(const int syssize) const{return 0.;} // no kinetic energy in a QS scheme
  virtual void addToMatrix(int row, int col, const scalar &val, const nonLinearSystemBase::whichMatrix wm)
  {
   #ifdef _DEBUG
    if(wm!=nonLinearSystemBase::stiff)
      Msg::Error("Try to assemble a matrix different from stiffness in a QS Gmresk system");
    else
   #endif // _DEBUG
    this->linearSystemGMRESk<scalar>::addToMatrix(row,col,val);
  }


  virtual void setInitialCondition(int row, scalar val, const typename nonLinearBoundaryCondition::whichCondition wc)
  {
    if(wc!=nonLinearBoundaryCondition::position)
    {
      Msg::Error("Can set a velocity or a aceleration in a QS scheme");
      return;
    }
    if(this->isAllocated())
    {
      _xsol[row] = val;
      _xprev[row] = val;
    }
    else{
      Msg::Error("Can't give an initial value in a non allocated system!");
    }
  }
  virtual void setDynamicRelaxation(const bool dynrel)
  {
    Msg::Error("Cannot use dynamic relaxation in an QS system");
  }
  virtual void getFromSolution(int row, scalar &val, const nonLinearBoundaryCondition::whichCondition wv) const
  {
    if(wv!=nonLinearBoundaryCondition::position)
    {
      Msg::Error("Cannot take velocity or acceleration in a QS scheme");
      val=0.;
      return;
    }
    this->getFromSolution(row,val);
  }
  virtual void getFromMatrix(int row, int col, scalar &val,const nonLinearSystemBase::whichMatrix wm) const
  {
    if(wm!=nonLinearSystemBase::stiff)
    {
      Msg::Error("A QS system has only a stiffness matrix. So cannot get a value in another matrix");
      val=0;
      return;
    }
    this->linearSystemGMRESk<scalar>::getFromMatrix(row,col,val);
  }
  #if defined(HAVE_MPI)
  virtual double* getArrayIndexPreviousRightHandSidePlusMPI(const int index)
  {
    return (&(_Fextprev[index]));
  }
  #endif // HAVE_MPI
  virtual void setTimeStep(const double dt){_timeStep = dt;}
  virtual double getTimeStep() const{return _timeStep;}
};
#endif //NONLINEARSYSTEMGMRESK_H_