//
// C++ Interface: interpolation method
//
//
//
// Author:  <Van Dung NGUYEN>, (C) 2017
//
// Copyright: See COPYING file that comes with this distribution
//
//

namespace interpolationMethod{
  double interp_linear(const double p, const double* x, const double *y, const int N);
  double interp_cubicSpline(const double p, const double* x, const double *y, const double* dy, const int N);
};