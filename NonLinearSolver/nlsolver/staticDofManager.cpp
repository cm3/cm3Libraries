//
// C++ Interface: Dof manager
//
// Description: dofManager for non linear system (rewritte of assemble(fullMatrix) no contribution for fixed Dof
//              The contribution is taken into account in the assemble(fullvector) of RightHandSide
//              More add functions to archiving force can be placed in dofManager but I put it here to not pollute gmsh code
// Author:  <Gauthier BECKER>, (C) 2010,
//          <Van Dung NGUYEN>, (C) 2020
//
// Copyright: See COPYING file that comes with this distribution
//
//

#include "staticDofManager.h"
#include "dofManagerMultiSystems.h"
#include "pbcSystems.h"
#include "pathFollowingSystem.h"

#if defined(HAVE_BLAS)
// extern blas function declaration
#if !defined(F77NAME)
#define F77NAME(x) (x##_)
#endif // F77NAME
extern "C" {
  void F77NAME(dcopy)(int *n, double *x,int *incx, double *y, int *incy);
}
#endif // HAVE_BLAS

void DofCollection::collect(nlsDofManager* pAssembler)
{
  // clear all first
  allDofs.clear();
  //
  pAssembler->collectAllDofValues(*this);
};



staticDofManager::staticDofManager(linearSystem< double > *l, nonLinearMechSolver::scheme sh,
                 bool useGlobalSystemPosition, bool part): _current(l), _scheme(sh),
                 _mpiSendGlobalPosition(useGlobalSystemPosition),_isPartitioned(part),
                 _firstRigidContactDof(0), _numberOfUnknownsPreviouRank(0)
                 #if defined(HAVE_MPI)
                 ,_mpiInit(false), _mpiBuffer(NULL), _sizeMPIBuffer(0),
                 _mapMPIRecvComm((_isPartitioned)?Msg::GetCommSize():0, std::set<Dof>()),
                 _mapMPIDof((_isPartitioned)?Msg::GetCommSize():0,NULL),
                 _localNumberDofMPI(NULL),
                 _mpiRigidContactTMP(NULL), _mpiRigidContactSize(0),
                 _otherPartPositions(NULL), _previousOtherPartPositions(NULL),
                 _otherPartVelocities(NULL),_previousOtherPartVelocities(NULL),
                 _otherPartAccelerations(NULL),_previousOtherPartAccelerations(NULL)
                 #endif // HAVE_MPI
{
  _NLScurrent = dynamic_cast<nonLinearSystem<double>*>(l);
  _linearSystems["A"] = l;
  
  #if defined(HAVE_MPI)
  if(Msg::GetCommSize() > 1 && _isPartitioned)
  {
    _localNumberDofMPI = new int[Msg::GetCommSize()];
    for(int i=0;i<Msg::GetCommSize();i++)
    {
      _localNumberDofMPI[i] =0;
    }
  }
  #endif //HAVE_MPI
};

staticDofManager::staticDofManager(linearSystem<double> *l1, linearSystem<double> *l2, bool part): _current(l1), _scheme(nonLinearMechSolver::Eigen),
                 _mpiSendGlobalPosition(true),_isPartitioned(part),
                 _firstRigidContactDof(0), _numberOfUnknownsPreviouRank(0)
                 #if defined(HAVE_MPI)
                 ,_mpiInit(false), _mpiBuffer(NULL), _sizeMPIBuffer(0),
                 _mapMPIRecvComm((_isPartitioned)?Msg::GetCommSize():0, std::set<Dof>()),
                 _mapMPIDof((_isPartitioned)?Msg::GetCommSize():0,NULL),
                 _localNumberDofMPI(NULL),
                 _mpiRigidContactTMP(NULL), _mpiRigidContactSize(0),
                 _otherPartPositions(NULL), _previousOtherPartPositions(NULL),
                 _otherPartVelocities(NULL),_previousOtherPartVelocities(NULL),
                 _otherPartAccelerations(NULL),_previousOtherPartAccelerations(NULL)
                 #endif // HAVE_MPI
{
  _NLScurrent = dynamic_cast<nonLinearSystem<double>*>(l1);
  //
  _linearSystems.insert(std::make_pair("A", l1));
  _linearSystems.insert(std::make_pair("B", l2));
  
  #if defined(HAVE_MPI)
  if(Msg::GetCommSize() > 1 && _isPartitioned)
  {
    _localNumberDofMPI = new int[Msg::GetCommSize()];
    for(int i=0;i<Msg::GetCommSize();i++)
    {
      _localNumberDofMPI[i] =0;
    }
  }
  #endif //HAVE_MPI
};

staticDofManager::~staticDofManager()
{
  #if defined(HAVE_MPI)
  if(_sizeMPIBuffer != 0) delete[] _mpiBuffer;
  if(_localNumberDofMPI != NULL) delete[] _localNumberDofMPI;
  if (_otherPartUnknowns.size() > 0){
    delete[] _otherPartPositions;
    delete[] _previousOtherPartPositions;
    if(_scheme==nonLinearMechSolver::Explicit or _scheme == nonLinearMechSolver::Implicit)
    {
      delete[] _otherPartVelocities;
      delete[] _previousOtherPartVelocities;
      
      delete[] _otherPartAccelerations;
      delete[] _previousOtherPartAccelerations;
    }
  };        
  #endif //HAVE_MPI
  for (std::map<const std::string, linearSystem<double> *>::iterator its =  _linearSystems.begin(); its != _linearSystems.end(); its++)
  {
    delete its->second;
  }
  _NLScurrent = NULL;
};


#if defined(HAVE_MPI)
void staticDofManager::initMPI()
{
  if (!_isPartitioned) 
  {
    return;
  }
  if(Msg::GetCommSize() > 1)
  {
    this->_mpiRigidContactSize = this->unknown.size() - this->_firstRigidContactDof;
    if(this->_mpiRigidContactSize > 0)
      this->_mpiRigidContactTMP = new double[this->_mpiRigidContactSize];

    /* first give the number of unknowns to transfert */
    int* MPImySendComm = new int[Msg::GetCommSize()];
    int* MPImySendTmp = new int[Msg::GetCommSize()];
    for(int i=0;i<Msg::GetCommSize();i++)
    {
        MPImySendTmp[i] = int(_mapMPIRecvComm[i].size());
    }
    MPI_Alltoall(MPImySendTmp,1,MPI_INT,MPImySendComm,1,MPI_INT,MPI_COMM_WORLD);

    delete[] MPImySendTmp;

    if(_mpiSendGlobalPosition)
    {
     /* modify the unknnown map (static only !!). The access on matrix and vector is done with a global number
       So the sum of number of unknowns of previous ranks have to be added */
      // communicate the number of unknowns on each rank
      for(int i=0;i<Msg::GetCommSize();i++)
        _localNumberDofMPI[i] = 0;
      int* localbufferMPI = new int[Msg::GetCommSize()];
      for (int ii = 0; ii<Msg::GetCommSize(); ii++ )
      {
        localbufferMPI[ii] = 0;
      }
      localbufferMPI[Msg::GetCommRank()] = this->unknown.size();
      MPI_Allreduce(localbufferMPI,_localNumberDofMPI,Msg::GetCommSize(),MPI_INT,MPI_SUM,MPI_COMM_WORLD);
      delete[] localbufferMPI;
      // compute the number of unknowns
      int _numberOfUnknownsPreviouRank=0;
      for(int i=0;i<Msg::GetCommRank();i++)
        _numberOfUnknownsPreviouRank+= _localNumberDofMPI[i];
      // update unknown map
      if(_numberOfUnknownsPreviouRank > 0)
      {
        for(std::map<Dof,int>::iterator it=this->unknown.begin(); it!=this->unknown.end();++it)
        {
          it->second += _numberOfUnknownsPreviouRank;
        }
      }
    }
    // initiation of storage
    int nbdofOtherPart = _otherPartUnknowns.size();
    _otherPartPositions = new double[nbdofOtherPart];
    _previousOtherPartPositions = new double[nbdofOtherPart];
    for (int pp=0; pp < nbdofOtherPart; pp++)
    {
      _otherPartPositions[pp] = 0.;
      _previousOtherPartPositions[pp] = 0.;
    }

    // initiation of Communication (transfert the Dof to recieve from otherPart)
    long int * arrayBuffer;
    int currentBufferSize=0;
    // array to transfert the global system's position of the Dof to the other part
    int* arraySysPosition;
    int arraySysPositionSize=0;
    MPI_Status status;

    for(int i=0; i<Msg::GetCommSize(); i++)
    {
      // recieve first from lowest Rank
      if(Msg::GetCommRank() < i)
      {
        // transform Dof on the form of long int tabular (in sequence entity, type)
        std::set<Dof> &RecvSetDof = _mapMPIRecvComm[i];
        int commSize = 2*RecvSetDof.size(); // 2 components to transfert
        if(commSize > currentBufferSize){
          if(currentBufferSize != 0) delete[] arrayBuffer;
          currentBufferSize = commSize;
          arrayBuffer = new long int[commSize];
        }
        int j=0;
        for(std::set<Dof>::const_iterator it = RecvSetDof.begin(); it!=RecvSetDof.end(); ++it)
        {
          Dof R = *it;
          arrayBuffer[j] = R.getEntity();
          arrayBuffer[j+1] = (long int)R.getType();
          j+=2;
        }
        MPI_Send(arrayBuffer,commSize,MPI_LONG,i,Msg::GetCommRank(),MPI_COMM_WORLD);

        /* recieve the data from other rank */
        if(_mpiSendGlobalPosition){
          // recieve the dof position from other rank
          if(RecvSetDof.size() > arraySysPositionSize)
          {
            if(arraySysPositionSize !=0) delete[] arraySysPosition;
            arraySysPositionSize = RecvSetDof.size();
            arraySysPosition = new int[arraySysPositionSize];
          }
          MPI_Recv(arraySysPosition,RecvSetDof.size(),MPI_INT,i,i,MPI_COMM_WORLD,&status);
          j=0;
          for(std::set<Dof>::const_iterator it = RecvSetDof.begin(); it!=RecvSetDof.end(); ++it)
          {
            Dof R = *it;
            _globalOtherPartSystemPosition.insert(std::pair<Dof,int>(R,arraySysPosition[j]));
            j++;
          }
        }

        int commSizeSend = 2*MPImySendComm[i];
        if(commSizeSend > currentBufferSize){
          if(currentBufferSize != 0) delete[] arrayBuffer;
          currentBufferSize = commSizeSend;
          arrayBuffer = new long int[commSizeSend];
        }
        MPI_Recv(arrayBuffer,commSizeSend,MPI_LONG,i,i,MPI_COMM_WORLD,&status);
        // Vector to send global system position
        if(_mpiSendGlobalPosition){
          if(MPImySendComm[i] > arraySysPositionSize)
          {
            if(arraySysPositionSize !=0 ) delete[] arraySysPosition;
            arraySysPositionSize = MPImySendComm[i];
            arraySysPosition = new int [arraySysPositionSize];
          }
        }
        // build the map with dof to transfert during the iteration
        std::vector<int>* &mapDof = _mapMPIDof[i];
        if(mapDof !=NULL) delete mapDof;
        mapDof = new std::vector<int>;
        int jj=0; // arraySysPosition index
        for(int j=0;j<commSizeSend; j+=2)
        {
          Dof R = Dof(arrayBuffer[j],-(int)arrayBuffer[j+1]);
          // search the Dof in map
          std::map<Dof,int>::const_iterator itR = this->unknown.find(R);
           if(itR == this->unknown.end())
           {
             if(this->fixed.find(R) == this->fixed.end())
               Msg::Error(" It seems that a Dof is not existing for MPI transfer from rank %d to rank %d Dof ent = %d Dof type = %d ",Msg::GetCommRank(),i,R.getEntity(),R.getType());
             else
               Msg::Error("Ask to send a fixed dof for MPI transfer from rank %d to rank %d Dof ent = %d Dof type = %d ",Msg::GetCommRank(),i,R.getEntity(),R.getType());
           }
           else
          {
            mapDof->push_back(itR->second);
            if(_mpiSendGlobalPosition) arraySysPosition[jj] = itR->second;
          }
          jj++;
        }
        if(_mpiSendGlobalPosition)
          MPI_Send(arraySysPosition,MPImySendComm[i],MPI_INT,i,Msg::GetCommRank(),MPI_COMM_WORLD);

      }
      // invert the order of operation (Recv first)
      else if(Msg::GetCommRank() > i) // nothing to do if i == GetCommRank (self communication)
      {
        // recieve the data from other rank
        int commSizeSend = 2*MPImySendComm[i];
        if(commSizeSend > currentBufferSize){
          if(currentBufferSize != 0) delete[] arrayBuffer;
          currentBufferSize = commSizeSend;
          arrayBuffer = new long int[commSizeSend];
        }
        MPI_Recv(arrayBuffer,commSizeSend,MPI_LONG,i,i,MPI_COMM_WORLD,&status);

        // build the map with dof to transfert during the iteration
        std::vector<int>* &mapDof = _mapMPIDof[i];
        if(mapDof !=NULL) delete mapDof;
        mapDof = new std::vector<int>;
        if(_mpiSendGlobalPosition){
          // array to send the global system's position
          if(MPImySendComm[i] > arraySysPositionSize)
          {
            if(arraySysPositionSize !=0) delete[] arraySysPosition;
            arraySysPositionSize = MPImySendComm[i];
            arraySysPosition = new int[arraySysPositionSize];
          }
        }
        int jj=0; // arraySysPosition index
        for(int j=0;j<commSizeSend; j+=2)
        {
          Dof R = Dof(arrayBuffer[j],-(int)arrayBuffer[j+1]);
          // search the Dof in map
          std::map<Dof,int>::const_iterator itR = this->unknown.find(R);
           if(itR == this->unknown.end())
           {
             if(this->fixed.find(R) == this->fixed.end())
               Msg::Error(" It seems that a Dof is not existing for MPI transfer from rank %d to rank %d Dof ent = %d Dof type = %d ",Msg::GetCommRank(),i,R.getEntity(),R.getType());
             else
               Msg::Error("Ask to send a fixed dof for MPI transfer from rank %d to rank %d Dof ent = %d Dof type = %d ",Msg::GetCommRank(),i,R.getEntity(),R.getType());

           }
           else
          {
            mapDof->push_back(itR->second);
            if(_mpiSendGlobalPosition) arraySysPosition[jj] = itR->second;
          }
          jj++;
        }
        if(_mpiSendGlobalPosition)
          MPI_Send(arraySysPosition,MPImySendComm[i],MPI_INT,i,Msg::GetCommRank(),MPI_COMM_WORLD);

        // send now
        std::set<Dof> &RecvSetDof = _mapMPIRecvComm[i];
        int commSize = 2*RecvSetDof.size(); // 2 components to transfert
        if(commSize > currentBufferSize){
          if(currentBufferSize != 0) delete[] arrayBuffer;
          currentBufferSize = commSize;
          arrayBuffer = new long int[commSize];
        }
        int j=0;
        for(std::set<Dof>::const_iterator it = RecvSetDof.begin(); it!=RecvSetDof.end(); ++it)
        {
          Dof R = *it;
          arrayBuffer[j] = R.getEntity();
          arrayBuffer[j+1] = (long int)R.getType();
          j+=2;
        }
        MPI_Send(arrayBuffer,commSize,MPI_LONG,i,Msg::GetCommRank(),MPI_COMM_WORLD);

        if(_mpiSendGlobalPosition){
          // recieve the global system position
          if(RecvSetDof.size() > arraySysPositionSize)
          {
            if(arraySysPositionSize != 0) delete[] arraySysPosition;
            arraySysPositionSize = RecvSetDof.size();
            arraySysPosition = new int[arraySysPositionSize];
          }
          MPI_Recv(arraySysPosition,RecvSetDof.size(),MPI_INT,i,i,MPI_COMM_WORLD,&status);
          j=0;
          for(std::set<Dof>::const_iterator it = RecvSetDof.begin(); it!=RecvSetDof.end(); ++it)
          {
            Dof R = *it;
            _globalOtherPartSystemPosition.insert(std::pair<Dof,int>(R,arraySysPosition[j]));
            j++;
          }
        }
      }
    }

    if(currentBufferSize !=0) delete[] arrayBuffer;
    if(arraySysPositionSize!=0) delete[] arraySysPosition;
    delete[] MPImySendComm;
    // initiation of additional storage for Explicit Only
    if(_scheme==nonLinearMechSolver::Explicit or _scheme == nonLinearMechSolver::Implicit)
    {
      this->_otherPartVelocities = new double[nbdofOtherPart];
      this->_previousOtherPartVelocities = new double[nbdofOtherPart];
      this->_otherPartAccelerations = new double[nbdofOtherPart];
      this->_previousOtherPartAccelerations = new double[nbdofOtherPart];
      
      for (int pp=0; pp < nbdofOtherPart; pp++)
      {
        _otherPartVelocities[pp] = 0.;
        _previousOtherPartVelocities[pp] = 0.;
        _otherPartAccelerations[pp] = 0.;
        _previousOtherPartAccelerations[pp] = 0.;
      }
    }
  }
  _mpiInit = true;
}
#endif // HAVE_MPI

linearSystem<double> *staticDofManager::getLinearSystem(std::string &name)
{
  std::map<const std::string, linearSystem<double> *>::iterator it = _linearSystems.find(name);
  if(it != _linearSystems.end())
    return it->second;
  else
    return NULL;
};

const linearSystem<double> *staticDofManager::getLinearSystem(std::string &name) const
{
  std::map<const std::string, linearSystem<double> *>::const_iterator it = _linearSystems.find(name);
  if(it != _linearSystems.end())
    return it->second;
  else
    return NULL;
}

void staticDofManager::setCurrentMatrix(std::string name)
{
  std::map<const std::string, linearSystem<double> *>::iterator it =  _linearSystems.find(name);
  if(it != _linearSystems.end())
  {
    _current = it->second;
    _NLScurrent = dynamic_cast<nonLinearSystem<double>*>(this->_current);
  }
  else {
    Msg::Error("Current matrix %s not found ", name.c_str());
    _current = NULL;
    _NLScurrent = NULL;
  }
};

void staticDofManager::collectAllDofValues(DofCollection& collect) const
{
  //unknown dof
  for(std::map<Dof,int>::const_iterator itR = this->unknown.begin(); itR!=this->unknown.end();++itR)
  {
    const Dof& key = itR->first;
    int NR = itR->second;
    // get data from
    std::map<Dof,std::vector<double> >::const_iterator itF = collect.allDofs.find(key);
    if (itF ==collect.allDofs.end())
    {
      std::vector<double>& vals = collect.allDofs[key];
      vals.resize(3,0.);   
      this->getDofValue(key,vals[0]);
      vals[1] = 0.;
      vals[2] = 0.;
      if (this->getScheme() == nonLinearMechSolver::Implicit or this->getScheme() == nonLinearMechSolver::Explicit)
      {
        // with dof values
        this->getDofValue(key,vals[1],nonLinearBoundaryCondition::velocity);
        this->getDofValue(key,vals[2],nonLinearBoundaryCondition::acceleration);
      }
    }
  }
  
  // fixDynamtics
  for(std::map<Dof,FixedNodeDynamicData<double> >::const_iterator itFixedDyn=this->_fixedDynamic.begin(); itFixedDyn!=this->_fixedDynamic.end();++itFixedDyn)
  {
    const Dof& key = itFixedDyn->first;
    const FixedNodeDynamicData<double>& valDyn = itFixedDyn->second;
    // get data from
    std::map<Dof,std::vector<double> >::const_iterator itF = collect.allDofs.find(key);
    if (itF ==collect.allDofs.end())
    {
      std::vector<double>& vals = collect.allDofs[key];
      vals.resize(3,0);
      vals[0] = valDyn.position;
      vals[1] = valDyn.velocity;
      vals[2] = valDyn.acceleration;
    }
  }
  // fix
  for (std::map<Dof,double>::const_iterator itFixed = this->fixed.begin(); itFixed!= this->fixed.end(); itFixed++)
  {
    const Dof& key = itFixed->first;
    const double& valFixed = itFixed->second;
    // get data from
    std::map<Dof,std::vector<double> >::const_iterator itF = collect.allDofs.find(key);
    if (itF ==collect.allDofs.end())
    {
      std::vector<double>& vals = collect.allDofs[key];
      vals.resize(3,0);
      vals[0] = valFixed;
      vals[1] = 0.;
      vals[2] = 0.;
    }
  }

  // Dof other parts
  #if defined(HAVE_MPI)
  for(std::map<Dof,int>::const_iterator itOthers = this->_otherPartUnknowns.begin(); itOthers!=this->_otherPartUnknowns.end();++itOthers)
  {
    const Dof& key = itOthers->first;
    int NR = itOthers->second;
    // get data from
    std::map<Dof,std::vector<double> >::const_iterator itF = collect.allDofs.find(key);
    if (itF ==collect.allDofs.end())
    {
      std::vector<double>& vals = collect.allDofs[key];
      vals.resize(3,0);
      vals[0] = _otherPartPositions[NR];
      if(getScheme()==nonLinearMechSolver::Explicit or getScheme() == nonLinearMechSolver::Implicit)
      {
        vals[1] = _otherPartVelocities[NR];
        vals[2] = _otherPartAccelerations[NR];
      }
      else
      {
        vals[1] = 0.;
        vals[2] = 0.;
      }
    }
  }
  #endif //HAVE_MPI
}

void staticDofManager::updateDataFromCollection(const DofCollection& collect)
{ 
  if (collect.allDofs.size() == 0)
  {
    Msg::Warning("Dof collection has no element");
    return;
  }
  
  Msg::Info("start updating data form previous solving, solvertype = %d",getScheme());
  
  //unknown dof
  for(std::map<Dof,int>::iterator itR = this->unknown.begin(); itR!=this->unknown.end();++itR)
  {
    const Dof& key = itR->first;
    int NR = itR->second;
    const std::vector<double>* vals = NULL;
    std::map<Dof,std::vector<double> >::const_iterator itF = collect.allDofs.find(key);
    if (itF == collect.allDofs.end())
    {
      Msg::Warning("Dof does not exist in collection in updateDataFromCollection  - unknown: %d %d rank=%d",key.getEntity(),key.getType(),Msg::GetCommRank());
    }
    else
    {
      vals = &(itF->second);
    }
    if (vals != NULL)
    {
      // position
      _NLScurrent->setInitialCondition(NR,(*vals)[0],nonLinearBoundaryCondition::position);
      if (this->getScheme() == nonLinearMechSolver::Implicit or this->getScheme() == nonLinearMechSolver::Explicit)
      {
        // velocity
        _NLScurrent->setInitialCondition(NR,(*vals)[1],nonLinearBoundaryCondition::velocity);
        // accelerato
        _NLScurrent->setInitialCondition(NR,(*vals)[2],nonLinearBoundaryCondition::acceleration);
      }
    }
  }
  
  // fixed Dof
  for (std::map<Dof,double>::iterator itFixed = this->fixed.begin(); itFixed!= this->fixed.end(); itFixed++)
  {
    const Dof& key = itFixed->first;
    double& valFixed = itFixed->second;
    // get data from
    const std::vector<double>* vals = NULL;
    std::map<Dof,std::vector<double> >::const_iterator itF = collect.allDofs.find(key);
    if (itF == collect.allDofs.end())
    {
      Msg::Warning("Dof does not exist in collection in updateDataFromCollection fixed: %d %d rank=%d",key.getEntity(),key.getType(),Msg::GetCommRank());
    }
    else
    {
      vals = &(itF->second);
    }
    if (vals != NULL)
    {
      valFixed = (*vals)[0];
    }
  }
  // fixDynamtics
  for(std::map<Dof,FixedNodeDynamicData<double> >::iterator itFixedDyn=this->_fixedDynamic.begin(); itFixedDyn!=this->_fixedDynamic.end();++itFixedDyn)
  {
    const Dof& key = itFixedDyn->first;
    FixedNodeDynamicData<double>& valDyn = itFixedDyn->second;
    // get data from
    const std::vector<double>* vals = NULL;
    std::map<Dof,std::vector<double> >::const_iterator itF = collect.allDofs.find(key);
    if (itF == collect.allDofs.end())
    {
      Msg::Warning("Dof does not exist in collection in updateDataFromCollection fixedDynamic: %d %d rank=%d",key.getEntity(),key.getType(),Msg::GetCommRank());
    }
    else
    {
      vals = &(itF->second);
    }
    if (vals != NULL)
    {
      valDyn.position = (*vals)[0];
      valDyn.velocity = (*vals)[1];
      valDyn.acceleration = (*vals)[2];
    }
  }
  // Dof other parts
  #if defined(HAVE_MPI)
  for(std::map<Dof,int>::iterator itOthers = this->_otherPartUnknowns.begin(); itOthers!=this->_otherPartUnknowns.end();++itOthers)
  {
    const Dof& key = itOthers->first;
    int NR = itOthers->second;
    
    // get data from
    const std::vector<double>* vals = NULL;
    std::map<Dof,std::vector<double> >::const_iterator itF = collect.allDofs.find(key);
    if (itF == collect.allDofs.end())
    {
      Msg::Warning("Dof does not exist in collection in updateDataFromCollection otherPartiton: %d %d rank=%d",key.getEntity(),key.getType(),Msg::GetCommRank());
    }
    else
    {
      vals = &(itF->second);
    }
    if (vals != NULL)
    {
      // 
      _otherPartPositions[NR] = (*vals)[0];
      _previousOtherPartPositions[NR] = (*vals)[0];
      if(getScheme()==nonLinearMechSolver::Explicit or getScheme() == nonLinearMechSolver::Implicit)
      {
        _otherPartVelocities[NR] = (*vals)[1];
        _previousOtherPartVelocities[NR] = (*vals)[1];
        _otherPartAccelerations[NR] = (*vals)[2];
        _previousOtherPartAccelerations[NR] = (*vals)[2];
      }
    }
  }
  
  if (Msg::GetCommSize() > 1 and _isPartitioned)
  {
    // synchronize data
    this->systemMPIComm();
  } 
  #endif //HAVE_MPI
  
  Msg::Info("done updating data form previous solving, solvertype = %d",getScheme());
};

void staticDofManager::allocateSystem() 
{
  int nsize = sizeOfR();
  _current->allocate(nsize);
}
void staticDofManager::preAllocateEntries()
{
  _current->preAllocateEntries();
}

void staticDofManager::insertInSparsityPatternLinConst(const Dof &R, const Dof &C)
{
  std::map<Dof, int>::iterator itR = unknown.find(R);
  if(itR != unknown.end()) 
  {
    std::map<Dof, DofAffineConstraint<double> >::iterator  itConstraint = constraints.find(C);
    if(itConstraint != constraints.end())
    {
      for(unsigned i = 0; i < (itConstraint->second).linear.size(); i++) 
      {
        insertInSparsityPattern(R, (itConstraint->second).linear[i].first);
      }
    }
  }
  else { 
    std::map<Dof, DofAffineConstraint<double> >::iterator  itConstraint = constraints.find(R);
    if(itConstraint != constraints.end()) 
    {
      for(unsigned i = 0; i < (itConstraint->second).linear.size(); i++) 
      {
        insertInSparsityPattern((itConstraint->second).linear[i].first, C);
      }
    }
  }
}

void staticDofManager::insertInSparsityPattern(const Dof &R, const Dof &C)
{
  #if defined(HAVE_MPI)
  if(!_mpiInit ) this->initMPI();
  #endif // HAVE_MPI
  if (!this->_current->isAllocated()) this->_current->allocate (this->sizeOfR());
  std::map<Dof, int>::iterator itR = this->unknown.find(R);
  if (itR != this->unknown.end())
  {
    std::map<Dof, int>::iterator itC = this->unknown.find(C);
    if (itC != this->unknown.end())
    {
      this->_current->insertInSparsityPattern(itR->second+_numberOfUnknownsPreviouRank, itC->second+_numberOfUnknownsPreviouRank);
    }
    else
    {
      std::map<Dof, double>::iterator itFixed = this->fixed.find(C);
      if (itFixed != this->fixed.end()) {
      }
      else 
        this->insertInSparsityPatternLinConst(R, C);
    }
  }
  if (itR == this->unknown.end())
  {
    this->insertInSparsityPatternLinConst(R, C);
  }
};

void staticDofManager::sparsityDof(const std::vector<Dof> &R)
{
  if(_scheme == nonLinearMechSolver::Explicit) return; // no matrix --> skip
  for(int i=0;i<R.size();++i)
  {
    for(int j=0;j<R.size();++j)
    {
      this->insertInSparsityPattern(R[i],R[j]);
    }
  }
}

void staticDofManager::setFirstRigidContactUnknowns()
{
  _firstRigidContactDof = this->unknown.size();
}
int staticDofManager::getFirstRigidContactUnknowns() const {return _firstRigidContactDof;}

void staticDofManager::assembleLinConst(const Dof &R, const Dof &C, const double& value)
{
  std::map<Dof, int>::iterator itR = this->unknown.find(R);
  if (itR != this->unknown.end())
  {
    std::map<Dof, DofAffineConstraint<double> >::iterator  itConstraint = this->constraints.find(C);
    if (itConstraint != this->constraints.end())
    {
      double tmp(value);
      for (unsigned i = 0; i < (itConstraint->second).linear.size(); i++)
      {
        dofTraits<double>::gemm(tmp, (itConstraint->second).linear[i].second, value, 1, 0);
        this->assemble(R, (itConstraint->second).linear[i].first, tmp);
      }
    }
  }
  else
  {  
    std::map<Dof, DofAffineConstraint<double> >::iterator itConstraint = this->constraints.find(R);
    if (itConstraint != this->constraints.end())
    {
      double tmp(value);
      for (unsigned i = 0; i < (itConstraint->second).linear.size(); i++)
      {
        dofTraits<double>::gemm(tmp, itConstraint->second.linear[i].second, value, 1, 0);
        this->assemble((itConstraint->second).linear[i].first, C, tmp);
      }
    }
  }
}

void staticDofManager::assemble(const Dof &R, const Dof &C, const double &value)
{
  if (!this->_current->isAllocated()) this->_current->allocate(this->sizeOfR());
  std::map<Dof, int>::iterator itR = this->unknown.find(R);
  if (itR != this->unknown.end())
  {
    std::map<Dof, int>::iterator itC = this->unknown.find(C);
    if (itC != this->unknown.end())
    {
      this->_current->addToMatrix(itR->second, itC->second, value);
    }
    else
    {
      std::map<Dof, double>::iterator itFixed = this->fixed.find(C);
      if (itFixed == this->fixed.end()) 
      {
        this->assembleLinConst(R, C, value);
      };
    }
  }
  else
  {
    this->assembleLinConst(R, C, value);
  }
};

void staticDofManager::assemble(const std::vector<Dof> &R, const fullMatrix<double> &m)
{
  if (_scheme == nonLinearMechSolver::StaticLinear)
  {
    linearElastic_assemble(R,m);
  }
  else
  {
    this->assemble(R,m,nonLinearSystemBase::stiff);
  }
};

void staticDofManager::assemble(const std::vector<Dof> &R, const fullMatrix<double> &m,const whichMatrix wm)
{
  if (_scheme == nonLinearMechSolver::StaticLinear)
  {
    if (wm != nonLinearSystemBase::stiff)
    {
      Msg::Error("only stiff can be assemble for elastic solver");
    }
    else
    {
      linearElastic_assemble(R,m);
    }
    return;
  }
  
  if (!this->_current->isAllocated()) this->_current->allocate(this->sizeOfR());
  std::vector<int> NR(R.size()); // in MPI NR != NC
  std::vector<int> NC(R.size());

  for (unsigned int i = 0; i < R.size(); i++)
  {
    #if defined(HAVE_MPI)
    if(R[i].getType() < 0)
    {
      NR[i] = -1;
      std::map<Dof,int>::iterator itR = _globalOtherPartSystemPosition.find(R[i]);
      if(itR == _globalOtherPartSystemPosition.end())
      {
        NC[i] = -1;
        #ifdef _DEBUG
        std::map<Dof,double>::iterator itR = this->fixed.find(R[i]);
        if(itR == this->fixed.end())
          Msg::Error("Can't find the global column of a dof in mpi on rank  %d",Msg::GetCommRank());
        #endif // _DEBUG

      }
      else
      {
        NC[i] = itR->second;
      }
    }
    else
    #endif // HAVE_MPI
    {
      std::map<Dof, int>::iterator itLocal = this->unknown.find(R[i]);
      if (itLocal != this->unknown.end())
      {
        NR[i] = itLocal->second;
        NC[i] = itLocal->second;
      }
      else
      {
        NR[i] = -1;
        NC[i] = -1;
      }
    }
  }
  if(wm == nonLinearSystemBase::stiff)
  {
    for (unsigned int i = 0; i < R.size(); i++){
      if (NR[i] != -1)
      {
        for (unsigned int j = 0; j < R.size(); j++)
        {
          if (NC[j] != -1)
          {
            this->_current->addToMatrix(NR[i], NC[j], m(i, j));
          }
          else
          {
            std::map<Dof,  double>::iterator itFixed = this->fixed.find(R[j]);
            if (itFixed == this->fixed.end())
              this->assembleLinConst(R[i],R[j],m(i,j));
          }
        }
      }
      else
      {
        for (unsigned int j = 0; j < R.size(); j++){
          this->assembleLinConst(R[i],R[j],m(i,j));
        }
      }
    }
  }
  else
  { // mass matrix
    if (_NLScurrent == NULL)
    {
      Msg::Error("nonlinear system must be allocated to assemble mass matrix");
      return;
    }
  
    for (unsigned int i = 0; i < R.size(); i++)
    {
      if (NR[i] != -1)
      {
        for (unsigned int j = 0; j < R.size(); j++)
        {
          _NLScurrent->addToMatrix(NR[i], NC[j], m(i, j),nonLinearSystemBase::mass);
        }
      }
      else
      { // store mass in _fixedmass
        double linemass=0.;
        // if already exist in _fixed mass retrieve value to add sum because diagonal matrix
        std::map<Dof,FixedNodeDynamicData<double> >::iterator itm = this->_fixedDynamic.find(R[i]);
        if(itm != this->_fixedDynamic.end())
          linemass+=itm->second.mass;
        for(unsigned int j=0; j<R.size(); j++)
          linemass+=m(i,j);
        (this->_fixedDynamic[R[i]]).mass = linemass;
      }
    }
  }
};



// Function to fix dof (Create the key in RHS fixed too)
void staticDofManager::fixDof(const Dof& key, const double &value)
{
 // verify that the dof is fixed
  if(this->unknown.find(key) != this->unknown.end())
  {
    Msg::Error("It seems that you try to fix a dof that is declared as an unknown... fix this");
    return;
  }
  this->RHSfixed[key]=0.;
  std::map<Dof, double>::iterator itD = this->fixed.find(key);
  std::map<Dof,FixedNodeDynamicData<double> >::iterator itDyn;
  // Perform a new insertion in the system
  if(itD == this->fixed.end())
  {
    itD = this->fixed.insert(this->fixed.begin(), std::pair<Dof,double>(key,value));
    itDyn = this->_fixedDynamic.insert(this->_fixedDynamic.begin(), std::pair<Dof,FixedNodeDynamicData<double> >(key,FixedNodeDynamicData<double>()));
    return;
  }
  else
  {
    itDyn = this->_fixedDynamic.find(key); // By construction we are sure that the key exists in the map
  }

  // The value already exist --> update
  double& myvalue = itD->second; //= value;
  double& lastpos = itDyn->second.position;
  if(_scheme==nonLinearMechSolver::Explicit)
  {
    double timestep = _NLScurrent->getTimeStep();
    double& lastvelo= itDyn->second.velocity;

    double newvelo = (value-lastpos) / timestep;
    itDyn->second.acceleration = (newvelo-lastvelo) / timestep;
    lastvelo = newvelo;
  }
  lastpos = myvalue;
  myvalue = value;

  return;
};

void staticDofManager::numberDof(const Dof& key)
{
  if(fixed.find(key) != fixed.end()) return;
  if(constraints.find(key) != constraints.end()) return;
  std::map<Dof, int>::iterator it = unknown.find(key);
  if(it == unknown.end()) 
  {
    std::size_t size = unknown.size();
    unknown[key] = size;
  }
}
void staticDofManager::numberDof(const std::vector<Dof> &R)
{
  for(std::size_t i = 0; i < R.size(); i++) 
  {
    numberDof(R[i]);
  }
};

void staticDofManager::assemble(const Dof &R, const double &value, const whichRhs wrhs)
{
  if (!this->_current->isAllocated()) this->_current->allocate(this->sizeOfR());
  #if defined(HAVE_MPI)
  if(R.getType() >= 0)
  #endif // HAVE_MPI
  {
    std::map<Dof, int>::iterator itR = this->unknown.find(R);
    if(itR != this->unknown.end()){
      if(wrhs == nonLinearSystemBase::Fext)
      {
        if (_NLScurrent!=NULL)
        {
          _NLScurrent->addToRightHandSidePlus(itR->second,value);
        }
        else
        {
          _current->addToRightHandSide(itR->second,value);
        }
      }
      else if (wrhs == nonLinearSystemBase::LoadVector)
      {
        #ifdef _DEBUG
        if (_NLScurrent ==NULL)
        {
          Msg::Error("nonlinear system must be used to assemble load vector");
        }
        #endif //_DEBUG
        _NLScurrent->addToLoadVector(itR->second,value);
      }
      else
      {
        #ifdef _DEBUG
        if (_NLScurrent ==NULL)
        {
          Msg::Error("nonlinear system must be used to assemble minus force vector");
        }
        #endif //_DEBUG
        _NLScurrent->addToRightHandSideMinus(itR->second,value);
      }
    }
    else
    {
      std::map<Dof, DofAffineConstraint<double> >::iterator itConstraint = this->constraints.find(R);
      if (itConstraint != this->constraints.end())
      {
        for (unsigned j = 0; j < (itConstraint->second).linear.size(); j++)
        {
          double tmp(0.);
          dofTraits<double>::gemm(tmp, (itConstraint->second).linear[j].second, value, 1, 0);
          this->assemble((itConstraint->second).linear[j].first, tmp,wrhs);
        }
      }
    }
  }
}

void staticDofManager::assemble(const Dof &R, const double &value)
{
  if (!this->_current->isAllocated()) this->_current->allocate(this->sizeOfR());
  #if defined(HAVE_MPI)
  if(R.getType() >= 0)
  #endif // HAVE_MPI
  {
    std::map<Dof, int>::iterator itR = this->unknown.find(R);
    if(itR != this->unknown.end())
    {
      if (_NLScurrent!=NULL)
      {
        _NLScurrent->addToRightHandSidePlus(itR->second,value);
      }
      else
      {
        _current->addToRightHandSide(itR->second,value);
      }
    }
    else
    {
      std::map<Dof, DofAffineConstraint<double> >::iterator itConstraint = this->constraints.find(R);
      if (itConstraint != this->constraints.end())
      {
        for (unsigned j = 0; j < (itConstraint->second).linear.size(); j++)
        {
          double tmp(0.);
          dofTraits<double>::gemm(tmp, (itConstraint->second).linear[j].second, value, 1, 0);
          this->assemble((itConstraint->second).linear[j].first, tmp);
        }
      }
    }
  }
}

void staticDofManager::assemble(const std::vector<Dof> &R, const fullVector<double> &m) // for linear forms
{
  if (!this->_current->isAllocated()) this->_current->allocate(this->sizeOfR());
  std::vector<int> NR(R.size());
  // Archiving
  for (unsigned int i = 0; i < R.size(); i++)
  {
    std::map<Dof,  double>::iterator itR = RHSfixed.find(R[i]);
    if (itR != RHSfixed.end()) itR->second += m(i); // add of contribution set to zero when the value is by a function FIX it
    else NR[i] = -1;
  }

  for (unsigned int i = 0; i < R.size(); i++)
  {
    #if defined(HAVE_MPI)
    if(R[i].getType() < 0)
    {
      NR[i]  = -1;
    }
    else
    #endif // HAVE_MPI
    {
      std::map<Dof, int>::iterator itR = this->unknown.find(R[i]);
      if (itR != this->unknown.end()) NR[i] = itR->second;
      else NR[i] = -1;
    }
  }
  for (unsigned int i = 0; i < R.size(); i++)
  {
    if (NR[i] != -1)
    {
      if (_NLScurrent != NULL)
      {
        _NLScurrent->addToRightHandSidePlus(NR[i], m(i));
      }
      else
      {
        _current->addToRightHandSide(NR[i], m(i)); // plus because it's the old implementation that is kept for compatibility
      
      }
    }
    else
    {
      std::map<Dof,DofAffineConstraint<double> >::iterator itConstraint = this->constraints.find(R[i]);
      if (itConstraint != this->constraints.end())
      {
        for (unsigned j = 0; j < (itConstraint->second).linear.size(); j++)
        {
          double tmp=0;
          dofTraits<double>::gemm(tmp, (itConstraint->second).linear[j].second, m(i), 1, 0);
          assemble((itConstraint->second).linear[j].first, tmp);
        }
      }
    }
  }
};

void staticDofManager::assemble(const std::vector<Dof> &R, const fullVector<double> &m,const whichRhs wrhs) // for linear forms
{
  if (!this->_current->isAllocated()) this->_current->allocate(this->sizeOfR());
  std::vector<int> NR(R.size());
  // Archiving
  for (unsigned int i = 0; i < R.size(); i++)
  {
    #if defined(HAVE_MPI)
    if(R[i].getType() < 0)
    {
      NR[i] = -1;
    }
    else
    #endif // HAVE_MPI
    {
      std::map<Dof,  double>::iterator itR = RHSfixed.find(R[i]);
      if (itR != RHSfixed.end()) itR->second += m(i); // add of contribution set to zero when the value is by a function FIX it
      else NR[i] = -1;
    }
  }

  for (unsigned int i = 0; i < R.size(); i++)
  {
    std::map<Dof, int>::iterator itR = this->unknown.find(R[i]);
    if (itR != this->unknown.end()) NR[i] = itR->second;
    else NR[i] = -1;
  }
  if(wrhs == nonLinearSystemBase::Fext)
  { // Warning small duplication to avoid if for each component of R
    for (unsigned int i = 0; i < m.size(); i++) 
    {
      if (NR[i] != -1)
      {
        if (_NLScurrent !=NULL)
        {
          _NLScurrent->addToRightHandSidePlus(NR[i], m(i));
        }
        else
        {
          _current->addToRightHandSide(NR[i], m(i));
        }
      }
      else
      {
        std::map<Dof,DofAffineConstraint<double> >::iterator itConstraint;
        itConstraint = this->constraints.find(R[i]);
        if (itConstraint != this->constraints.end())
        {
          for (unsigned j = 0; j < (itConstraint->second).linear.size(); j++){
            double tmp(0.);
            dofTraits<double>::gemm(tmp, (itConstraint->second).linear[j].second, m(i), 1, 0);
            this->assemble((itConstraint->second).linear[j].first,tmp,wrhs);
          }
        }
      }
    }
  }
  else if (wrhs == nonLinearSystemBase::LoadVector){
    for (unsigned int i = 0; i < m.size(); i++) 
    {
      if (NR[i] != -1)
      {
        #ifdef _DEBUG
        if (_NLScurrent ==NULL)
        {
          Msg::Error("nonlinear system must be used to assemble load vector");
        }
        #endif //_DEBUG
        _NLScurrent->addToLoadVector(NR[i], m(i));
      }
      else
      {
        std::map<Dof,DofAffineConstraint<double> >::iterator itConstraint;
        itConstraint = this->constraints.find(R[i]);
        if (itConstraint != this->constraints.end())
        {
          for (unsigned j = 0; j < (itConstraint->second).linear.size(); j++)
          {
            double tmp(0.);
            dofTraits<double>::gemm(tmp, (itConstraint->second).linear[j].second, m(i), 1, 0);
            this->assemble((itConstraint->second).linear[j].first,tmp,wrhs);
          }
        }
      }
    }
  }
  else
  {  
    for (unsigned int i = 0; i < R.size(); i++)
    {
      if (NR[i] != -1)
      {
        #ifdef _DEBUG
        if (_NLScurrent ==NULL)
        {
          Msg::Error("nonlinear system must be used to assemble minus force vector");
        }
        #endif //_DEBUG
        _NLScurrent->addToRightHandSideMinus(NR[i], m(i));
        
      }
      else
      {
        std::map<Dof,DofAffineConstraint<double> >::iterator itConstraint;
        itConstraint = this->constraints.find(R[i]);
        if (itConstraint != this->constraints.end())
        {
          for (unsigned j = 0; j < (itConstraint->second).linear.size(); j++)
          {
            double tmp(0.);
            dofTraits<double>::gemm(tmp, (itConstraint->second).linear[j].second, m(i), 1, 0);
            this->assemble((itConstraint->second).linear[j].first,tmp,wrhs);
          }
        }
      }
    }
  }
};

void staticDofManager::assembleToStressMatrix(const std::vector<Dof> &R, const fullMatrix<double> &m)
{
  pbcSystemBase<double>* pbcSys = dynamic_cast<pbcSystemBase<double>*>(this->_current);
  if (pbcSys != NULL)
  {
    std::vector<int> NR(R.size());
    for (int i=0; i<R.size(); i++){
      std::map<Dof, int>::iterator itR = this->unknown.find(R[i]);
      if (itR != this->unknown.end()) NR[i] = itR->second;
      else {
        NR[i] = -1;
      }
    }
    int Row = m.size1();
    int Col = m.size2();
    for (int r=0; r< Row; r++){
      for (int c=0; c< Col; c++){
        if (NR[c]>=0){
          pbcSys->addToStressMatrix(r,NR[c],m(r,c));
        }
      }
    }
  }
  else{
    Msg::Error("pbcSystem has to used to estimate homogenized tangent");
  }
};


void staticDofManager::assembleToBodyForceMatrix(const std::vector<Dof> &R, const fullMatrix<double> &m)
{
  pbcSystemBase<double>* pbcSys = dynamic_cast<pbcSystemBase<double>*>(this->_current);
  if (pbcSys != NULL)
  {
    std::vector<int> NR(R.size());
    for (int i=0; i<R.size(); i++){
      std::map<Dof, int>::iterator itR = this->unknown.find(R[i]);
      if (itR != this->unknown.end()) NR[i] = itR->second;
      else {
        NR[i] = -1;
      }
    }
    int Row = m.size1();
    int Col = m.size2();
    for (int r=0; r< Row; r++){
      for (int c=0; c< Col; c++){
        if (NR[r]>=0){
          pbcSys->addToBodyForceMatrix(NR[r],c, m(r,c));
        }
      }
    }
  }
  else{
    Msg::Error("pbcSystem has to used to estimate homogenized tangent");
  }
};


void staticDofManager::assembleToBodyForceVector(const std::vector<Dof> &R, const fullVector<double> &m)
{
  pbcSystemBase<double>* pbcSys = dynamic_cast<pbcSystemBase<double>*>(this->_current);
  if (pbcSys != NULL)
  {
    std::vector<int> NR(R.size());
    for (int i=0; i<R.size(); i++){
      std::map<Dof, int>::iterator itR = this->unknown.find(R[i]);
      if (itR != this->unknown.end()) NR[i] = itR->second;
      else {
        NR[i] = -1;
      }
    }
    for (int r=0; r< R.size(); r++){
       if (NR[r]>=0){
          pbcSys->addToBodyForceVector(NR[r],m(r));
        }
    }
  }
  else{
    Msg::Error("pbcSystem has to used to conpute body force vector");
  }
};

void staticDofManager::assemblePathFollowingConstraint(const double val)
{
  pathFollowingSystem<double>* pfsys  = dynamic_cast<pathFollowingSystem<double>*>(this->_current);
  if (pfsys!=NULL)
  {
    pfsys->addToPathFollowingContraint(val);
  }
  else
  {
    Msg::Error("pathFollowingSystem must be used in staticDofManager::assemblePathFollowingConstraint");
  }
};

void staticDofManager::assembleDPathFollowingConstraintDUnknowns(const Dof &R, const double &value){
  #if defined(HAVE_MPI)
  if(R.getType() >= 0)
  #endif // HAVE_MPI
  {
    pathFollowingSystem<double>* pfsys  = dynamic_cast<pathFollowingSystem<double>*>(this->_current);
    if (pfsys != NULL){
      std::map<Dof, int>::iterator itR = this->unknown.find(R);
      if(itR != this->unknown.end()){
        pfsys->addToDPathFollowingConstraintDUnknown(itR->second, value);
      }
      else{
        std::map<Dof, DofAffineConstraint<double> >::iterator itConstraint;
        itConstraint = this->constraints.find(R);
        if (itConstraint != this->constraints.end()){
          for (unsigned j = 0; j < (itConstraint->second).linear.size(); j++){
            double tmp(0.);
            dofTraits<double>::gemm(tmp, (itConstraint->second).linear[j].second, value, 1, 0);
            this->assembleDPathFollowingConstraintDUnknowns((itConstraint->second).linear[j].first, tmp);
          }
        }
      }
    }
  }
};

void staticDofManager::assembleDPathFollowingConstraintDUnknowns(const std::vector<Dof>& R, const fullVector<double> &m)
{
  pathFollowingSystem<double>* pfsys  = dynamic_cast<pathFollowingSystem<double>*>(this->_current);
  if (pfsys != NULL){
    std::vector<int> NR(R.size());
    for (unsigned int i = 0; i < R.size(); i++)
    {
      #if defined(HAVE_MPI)
      if(R[i].getType() < 0)
      {
        NR[i] = -1;
      }
      else
      #endif // HAVE_MPI
      {
        std::map<Dof, int>::iterator itR = this->unknown.find(R[i]);
        if (itR != this->unknown.end()) NR[i] = itR->second;
        else NR[i] = -1;
      }
    }
    for (unsigned int i = 0; i < R.size(); i++)
    {
      if (NR[i] != -1)
      {
        pfsys->addToDPathFollowingConstraintDUnknown(NR[i], m(i));
      }
      else{
        std::map<Dof, DofAffineConstraint<double> >::iterator itConstraint;
        itConstraint = this->constraints.find(R[i]);
        if (itConstraint != this->constraints.end()){
          for (unsigned j = 0; j < (itConstraint->second).linear.size(); j++){
            double tmp(0.);
            dofTraits<double>::gemm(tmp, (itConstraint->second).linear[j].second, m(i), 1, 0);
            this->assembleDPathFollowingConstraintDUnknowns((itConstraint->second).linear[j].first, tmp);
          }
        }
      }
    }
  }
};


 // zero for RHSfixed
void staticDofManager::clearRHSfixed()
{
  this->_current->zeroRightHandSide();
  for(std::map<Dof,double>::iterator itR = RHSfixed.begin();itR != RHSfixed.end(); ++itR)
    itR->second = 0.;
};

double staticDofManager::getNorm1ReactionForce() const
{
  double maxVal = 0;
  for(std::map<Dof,double>::const_iterator itR = RHSfixed.begin();itR != RHSfixed.end(); ++itR)
  {
    maxVal += fabs(itR->second);
  }
  return maxVal;
};

void staticDofManager::getReactionForceOnFixedPhysical(archiveForce& af) const
{
  af.fval =0.;
  for(std::set<Dof>::const_iterator itD = af.vdof.begin(); itD!=af.vdof.end();++itD){
    // look in RHSfixed
    std::map<Dof, double>::const_iterator itRHS = RHSfixed.find(*itD);
    if(itRHS != RHSfixed.end()) {
      af.fval += itRHS->second; // Fext
    }
    else
    {
      std::map<Dof,int>::const_iterator itunk = this->unknown.find(*itD);
      if(itunk != this->unknown.end())
      {
        if (_NLScurrent != NULL)
        {
          double ff;
          _NLScurrent->getFromRightHandSidePlus(itunk->second,ff);
          af.fval += ff; // get from external force
        }
      }
    }
  }
};

void staticDofManager::getForces(std::vector<archiveForce> &vaf) const{
  for(int j=0;j<vaf.size();j++){
    double Fext =0.;
    for(std::set<Dof>::const_iterator itD = vaf[j].vdof.begin(); itD!=vaf[j].vdof.end();++itD){
      // look in RHSfixed
      std::map<Dof, double>::const_iterator itRHS = RHSfixed.find(*itD);
      if(itRHS != RHSfixed.end()) {
        Fext += itRHS->second; // Fext
      }
      else{
        std::map<Dof,int>::const_iterator itunk = this->unknown.find(*itD);
        if(itunk != this->unknown.end()){
          if (_NLScurrent != NULL)
          {
            double ff;
            _NLScurrent->getFromRightHandSidePlus(itunk->second,ff);
            Fext += ff; // get from external force
          }
        }
      }
    }
    vaf[j].fval= Fext;
  }
  #if defined(HAVE_MPI)
  /// !!only fix dof contact should be reduced !! The others are not considered for now
  /// cache force_contact
  int ndof_contact_2_reduce = 0;
  for(size_t j=0; j<vaf.size();++j) {
    if(vaf[j].contact_root == -1) continue;
    for(std::set<Dof>::const_iterator itD = vaf[j].vdof.begin(); itD!=vaf[j].vdof.end();++itD){
      // look in RHSfixed
      std::map<Dof, double>::const_iterator itRHS = RHSfixed.find(*itD);
      if(itRHS == RHSfixed.end()) {
          Msg::Error("rank: %d Reduce unfixed Dof rigid contact force which is not handle... dof %d %d",Msg::GetCommRank(),itD->getEntity(),itD->getType());
      }
    }
    ndof_contact_2_reduce++;
  }
  std::vector<double> fixdof_contact_force_to_archive(ndof_contact_2_reduce,0.);
  size_t current_contact_pos = 0;
  for(size_t j=0; j<vaf.size();++j) {
    if(vaf[j].contact_root != -1)
      fixdof_contact_force_to_archive[current_contact_pos++] = vaf[j].fval;
  }
  MPI_Allreduce(MPI_IN_PLACE,fixdof_contact_force_to_archive.data(),ndof_contact_2_reduce,MPI_DOUBLE,MPI_SUM,MPI_COMM_WORLD);
  current_contact_pos = 0;
  for(size_t j=0; j<vaf.size();++j) {
    if(vaf[j].contact_root != -1) {
      vaf[j].fval = fixdof_contact_force_to_archive[current_contact_pos++];
    }
  }
  #endif // HAVE_MPI
  return;
};

void staticDofManager::setInitialCondition(const Dof &R, const double &value, const nonLinearBoundaryCondition::whichCondition wc)
{
  if (!this->_current->isAllocated()) this->_current->allocate(this->sizeOfR());
  // find the dof in map if not find -1
  int NR=-1;
  std::map<Dof, int>::iterator itR = this->unknown.find(R);
  if (itR != this->unknown.end())
  {
    NR = itR->second;
    _NLScurrent->setInitialCondition(NR, value,wc);
    return;
  }
  // Set the initial condition on the ghost dofs if required
  #if defined(HAVE_MPI)
  if(R.getType() < 0)
  {
    std::map<Dof,int>::const_iterator itRg = this->_otherPartUnknowns.find(R);
    if(itRg == this->_otherPartUnknowns.end())
    {
      #ifdef _DEBUG
      Msg::Error("unknown ghost dof %d %d to set initial condition on rank %d",itRg->first.getEntity(),itRg->first.getType(),Msg::GetCommRank());
      #endif //_DEBUG
    }
    else
    {
      switch(wc)
      {
        case nonLinearBoundaryCondition::position :
          this->_otherPartPositions[itRg->second] = value;
          return;
        case nonLinearBoundaryCondition::velocity :
          this->_otherPartVelocities[itRg->second] = value;
          return;
        case nonLinearBoundaryCondition::acceleration :
          this->_otherPartAccelerations[itRg->second] = value;
          return;
      }
    }
  }
  #endif // HAVE_MPI
  return;
};

void staticDofManager::getVertexMass(const std::vector<Dof> &R, std::vector<double> &vmass) const
{
  vmass.resize(R.size());
  if(this->_current->isAllocated()){
    std::map<Dof, int>::const_iterator it;
    double mass;
    for(int i=0;i<R.size(); i++){
      it = this->unknown.find(R[i]);
      if(it != this->unknown.end()){
        _NLScurrent->getFromMatrix(it->second,it->second,mass,nonLinearSystemBase::mass);
        vmass[i] = mass;
      }
      else{  // find mass in _fixedmass
        std::map<Dof,FixedNodeDynamicData<double> >::const_iterator itm = this->_fixedDynamic.find(R[i]);
        if(itm != this->_fixedDynamic.end()) 
          vmass[i] = itm->second.mass;
        else 
          vmass[i] = 0;
      }
    }
  }
  else{
    for(int i=0;i<R.size(); i++) vmass[i] = 0;
  }
};

double staticDofManager::getKineticEnergy(const int systemSizeWithoutRigidContact,FilterDofSet &fildofcontact) const
{
  double ener = _NLScurrent->getKineticEnergy(systemSizeWithoutRigidContact); // energy of free dofs
  // add energy of fixed dof
  for(std::map<Dof,FixedNodeDynamicData<double> >::const_iterator it = this->_fixedDynamic.begin(); it!=this->_fixedDynamic.end(); ++it){
    if(!fildofcontact(it->first)){ // remove energy of the rigid body (contact)
      double mv = it->second.mass;
      double v =  it->second.velocity;
      ener += 0.5*mv*v*v;
    }
  }
  return ener;
};

void staticDofManager::getFixedRightHandSide(std::vector<Dof> &R, std::vector<double> &val) const
{
  for(std::map<Dof, double>::const_iterator it = RHSfixed.begin(); it != RHSfixed.end(); ++it){
    R.push_back(it->first);
    val.push_back(it->second);
  }
}

void staticDofManager::setInitialCondition(const std::vector<Dof> &R, const std::vector<double> &disp, const nonLinearBoundaryCondition::whichCondition wc)
{
  for (int i=0; i<R.size(); i++)
  {
    setInitialCondition(R[i],disp[i],wc);
  }
};

int staticDofManager::getDofNumber(const Dof &ky) const 
{
  std::map<Dof, int>::const_iterator it = unknown.find(ky);
  if(it == unknown.end()) {
    return -1;
  }
  else
    return it->second;
}

void staticDofManager::getFixedDof(std::vector<Dof> &R) const
{
  std::map<Dof, double>::const_iterator it;
  for(it = fixed.begin(); it != fixed.end(); ++it) {
    R.push_back(it->first);
  }
};

void staticDofManager::getFixedDof(std::set<Dof> &R) const
{
  std::map<Dof, double>::const_iterator it;
  for(it = fixed.begin(); it != fixed.end(); ++it) {
    R.insert(it->first);
  }
};

void staticDofManager::clearAllLineConstraints() 
{ 
  constraints.clear(); 
};

void staticDofManager::setLinearConstraint(const Dof& key, DofAffineConstraint<double> &affineconstraint)
{
  constraints[key] = affineconstraint;
};

void staticDofManager::getAllLinearConstraints(std::map<const Dof, const DofAffineConstraint<double>*>& map) const
{
  for (std::map<Dof, DofAffineConstraint<double> >::const_iterator it = constraints.begin(); it!= constraints.end(); it++)
  {
    map[it->first] = &(it->second);
  }
}

bool staticDofManager::isFixed(const Dof& key) const
{
  if(fixed.find(key) != fixed.end()) {
    return true;
  }
  return false;
}

bool staticDofManager::isAnUnknown(const Dof& key) const
{
  if(unknown.find(key) != unknown.end()) return true;
  return false;
}

bool staticDofManager::isConstrained(const Dof& key) const
{
  if(constraints.find(key) != constraints.end()) {
    return true;
  }
  return false;
}

int staticDofManager::systemSolveIntReturn()
{
 #if defined(HAVE_MPI)
 if(!this->_mpiInit) this->initMPI();
 #endif //HAVE_MPI
 int suc = this->_current->systemSolve();
 #if defined(HAVE_MPI)
 this->systemMPIComm();
 #endif // HAVE_MPI
  return suc;
}

void staticDofManager::systemSolve()
{
  // do mpi communication before system resolution (contact)
 #if defined(HAVE_MPI)
  if(!this->_mpiInit) this->initMPI();
  if(this->_mpiRigidContactSize != 0)
  {
      // copie the value before transfert
    double* firstContactDof = _NLScurrent->getArrayIndexPreviousRightHandSidePlusMPI(this->_firstRigidContactDof);
    #if defined(HAVE_BLAS)
    int INC = 1;
    F77NAME(dcopy)(&(this->_mpiRigidContactSize),firstContactDof,&INC,this->_mpiRigidContactTMP,&INC);
    #else
    for(int i=0;i<_mpiRigidContactSize;i++)
      this->_mpiRigidContactTMP[i] = firstContactDof[i];
    #endif // HAVE_BLAS
    // sum contribution of all cpu for the rigid contact unknown
    MPI_Allreduce(this->_mpiRigidContactTMP,firstContactDof,this->_mpiRigidContactSize,MPI_DOUBLE,MPI_SUM,MPI_COMM_WORLD);
  }
  #endif // HAVE_MPI
  _current->systemSolve();
  if(_scheme!=nonLinearMechSolver::Explicit) // no convergence in Explicit
    Msg::Warning("Void System Solve return. No succeed garantee Use systemSolveIntReturn");
  #if defined(HAVE_MPI)
  this->systemMPIComm();
  #endif // HAVE_MPI
};

void staticDofManager::getDofValue(const Dof& key,  double &val) const
{
  this->getDofValue(key,val,nonLinearBoundaryCondition::position);
}

void staticDofManager::getDofValue(const Dof& key,  double &val, nonLinearBoundaryCondition::whichCondition wv) const
{
  switch(wv)
  {
    case nonLinearBoundaryCondition::position:
    {
      // look first for fixed dof (the type can be negative)
      std::map<Dof, double>::const_iterator it = this->fixed.find(key);
      if (it != this->fixed.end()) 
      {
        val =  it->second;
        return ;
      }
      #if defined(HAVE_MPI)
      if(key.getType() < 0)
      {
        std::map<Dof,int>::const_iterator itR = this->_otherPartUnknowns.find(key);
        if(itR == _otherPartUnknowns.end())
        {
          Msg::Error("Can't get a value of ghost unknown entity: %d type %d from rank %d systype %d",key.getEntity(),key.getType(),Msg::GetCommRank(),this->_scheme);
        }
        else
        {
          val = this->_otherPartPositions[itR->second];
        }
        return;
      }
      #endif // HAVE_MPI
      std::map<Dof, int>::const_iterator itK = this->unknown.find(key);
      if (itK != this->unknown.end()) 
      {
        _current->getFromSolution(itK->second, val);
        return;
      }
      std::map<Dof, DofAffineConstraint< double > >::const_iterator itC = this->constraints.find(key);
      if (itC != this->constraints.end())
      {
        double tmp(val);
        val = itC->second.shift;
        for (unsigned i=0;i<(itC->second).linear.size();i++)
        {
           std::map<Dof, int>::const_iterator itu = this->unknown.find(((itC->second).linear[i]).first);
           getDofValue(((itC->second).linear[i]).first, tmp);
           dofTraits<double>::gemm(val,((itC->second).linear[i]).second, tmp, 1, 1);
        }
        return ;
      }
    }
    break;
    case nonLinearBoundaryCondition::velocity:
    {
      // look first for fixed Dof
      std::map<Dof, FixedNodeDynamicData<double> >::const_iterator it = this->_fixedDynamic.find(key);
      if (it != this->_fixedDynamic.end()) 
      {
        val =  it->second.velocity;
        return ;
      }
      #if defined(HAVE_MPI)
      if(key.getType() < 0)
      {
        std::map<Dof,int>::const_iterator itR = this->_otherPartUnknowns.find(key);
        if(itR == _otherPartUnknowns.end())
        {
          Msg::Error("Can't get a value of ghost unknown entity: %d type %d from rank %d systype %d",key.getEntity(),key.getType(),Msg::GetCommRank(),this->_scheme);
        }
        else
        {
          val = this->_otherPartVelocities[itR->second];
        }
        return;
      }
      #endif // HAVE_MPI
      std::map<Dof, int>::const_iterator itK = this->unknown.find(key);
      if (itK != this->unknown.end()) 
      {
        _NLScurrent->getFromSolution(itK->second, val,wv);
        return;
      }
    }
    break;
    case nonLinearBoundaryCondition::acceleration:
    {
      // look first for fixed Dof
      std::map<Dof, FixedNodeDynamicData<double> >::const_iterator it = this->_fixedDynamic.find(key);
      if (it != this->_fixedDynamic.end()) 
      {
        val =  it->second.acceleration;
        return ;
      }
      #if defined(HAVE_MPI)
      if(key.getType() < 0)
      {
        std::map<Dof,int>::const_iterator itR = this->_otherPartUnknowns.find(key);
        if(itR == _otherPartUnknowns.end())
        {
          Msg::Error("Can't get a value of ghost unknown entity: %d type %d from rank %d systype %d",key.getEntity(),key.getType(),Msg::GetCommRank(),this->_scheme);
        }
        else
        {
          val = this->_otherPartAccelerations[itR->second];
        }
        return;
      }
      #endif // HAVE_MPI
      std::map<Dof, int>::const_iterator itK = this->unknown.find(key);
      if (itK != this->unknown.end()) 
      {
        _NLScurrent->getFromSolution(itK->second, val,wv);
        return;
      }
    }
    break;
  };
};

void staticDofManager::getDofValue(const std::vector<Dof> &keys,std::vector<double> &Vals) const{
  this->getDofValue(keys,Vals,nonLinearBoundaryCondition::position);
}

void staticDofManager::getDofValue(const std::vector<Dof> &keys,std::vector<double> &Vals,
                                  const nonLinearBoundaryCondition::whichCondition wv) const
{
  int ndofs = keys.size();
  Vals.resize(ndofs);
  for (int i=0;i<ndofs;++i) getDofValue(keys[i], Vals[i],wv);
};

void staticDofManager::getFixedDofValue(const Dof& key, double &val) const
{
  std::map<Dof, double>::const_iterator it = fixed.find(key);
  if(it != fixed.end()) 
  {
    val = it->second;
  }
  else {
    Msg::Error("getFixedDof: Dof is not fixed");
    return;
  }
};

double staticDofManager::normInfRightHandSide() const
{
  return this->_current->normInfRightHandSide();
}

double staticDofManager::normInfSolution() const
{
  return this->_current->normInfSolution();
}

double staticDofManager::norm0Inf() const
{
  return _NLScurrent->norm0Inf();
}
void staticDofManager::zeroMatrix()
{
  this->_current->zeroMatrix();
}
int staticDofManager::lineSearch()
{
  return _NLScurrent->lineSearch();
}
void staticDofManager::resetUnknownsToPreviousTimeStep()
{
  _NLScurrent->resetUnknownsToPreviousTimeStep();
  #if defined(HAVE_MPI)
  #if defined(HAVE_BLAS)
  int one = 1.;
  int mysize = _otherPartUnknowns.size();
  F77NAME(dcopy)(&mysize,_previousOtherPartPositions,&one,_otherPartPositions,&one);
  if(_scheme==nonLinearMechSolver::Explicit or _scheme == nonLinearMechSolver::Implicit)
  {
    F77NAME(dcopy)(&mysize,_previousOtherPartVelocities,&one,_otherPartVelocities,&one);
    F77NAME(dcopy)(&mysize,_previousOtherPartAccelerations,&one,_otherPartAccelerations,&one);
  }
  #else
  for(int i=0;i<_otherPartUnknowns.size();i++){
    _otherPartPositions[i] = _previousOtherPartPositions[i];
    if(_scheme==nonLinearMechSolver::Explicit or _scheme == nonLinearMechSolver::Implicit){
      _otherPartVelocities[i] = _previousOtherPartVelocities[i];
      _otherPartAccelerations[i] = _previousOtherPartAccelerations[i];
    }
  }
  #endif // HAVE_BLAS
  #endif // HAVE_MPI
}
void staticDofManager::nextStep()
{
  if (_NLScurrent == NULL)
  {
    // not nextstep if nonlinear system does not exist
    return;
  }
  _NLScurrent->nextStep();
  #if defined(HAVE_MPI)
   #if defined(HAVE_BLAS)
    int one = 1.;
    int mysize = _otherPartUnknowns.size();
    F77NAME(dcopy)(&mysize,_otherPartPositions,&one,_previousOtherPartPositions,&one);
    if(_scheme==nonLinearMechSolver::Explicit or _scheme == nonLinearMechSolver::Implicit)
    {
      F77NAME(dcopy)(&mysize,_otherPartVelocities,&one,_previousOtherPartVelocities,&one);
      F77NAME(dcopy)(&mysize,_otherPartAccelerations,&one,_previousOtherPartAccelerations,&one);
    }
   #else
    for(int i=0;i<_otherPartUnknowns.size();i++){
      _previousOtherPartPositions[i] = _otherPartPositions[i];
      if(_scheme==nonLinearMechSolver::Explicit or _scheme == nonLinearMechSolver::Implicit){
        _previousOtherPartVelocities[i] = _otherPartVelocities[i];
        _previousOtherPartAccelerations[i] = _otherPartAccelerations[i];
      }
    }
   #endif // HAVE_BLAS
  #endif // HAVE_MPI
}
void staticDofManager::setTimeStep(const double dt)
{
  _NLScurrent->setTimeStep(dt);
}
void staticDofManager::restart()
{
  _NLScurrent->restart();
}
#if defined(HAVE_MPI)
void staticDofManager::manageMPIComm(const int otherPartNum,const std::vector<Dof> &otherPartR)
{
  // create the map of unknown to recevieve from cpu otherPartNum
  std::set<Dof> &RecvSetDof = _mapMPIRecvComm[otherPartNum];
  for(int i=0; i<otherPartR.size(); i++)
  {
    std::map<Dof,int>::const_iterator itR = _otherPartUnknowns.find(otherPartR[i]);
    std::map<Dof,double>::const_iterator itRfixed = this->fixed.find(otherPartR[i]);
    if((itR == _otherPartUnknowns.end()) and (itRfixed == this->fixed.end())) // add a communication Dof (otherwise it is already the case)
    {
      unsigned int size = _otherPartUnknowns.size();
      _otherPartUnknowns[otherPartR[i]] = size;
      RecvSetDof.insert(otherPartR[i]);
    }
  }
}

void staticDofManager::manageMPISparsity(const std::vector<Dof> &myR, const int otherPartNum,const std::vector<Dof> &otherPartR)
{
  if(_scheme == nonLinearMechSolver::Explicit) return;

  if(!_mpiInit) this->initMPI();
  // could not use the dofManager as we manage the MPI dofs at this level...
  for(int i=0; i<myR.size(); ++i)
  {
    std::map<Dof, int>::iterator itR = this->unknown.find(myR[i]);
    if (itR == this->unknown.end()) continue;
    for(int j=0; j<otherPartR.size();++j)
    {
      std::map<Dof, int>::iterator itC = this->_globalOtherPartSystemPosition.find(otherPartR[j]);
      if (itC != this->_globalOtherPartSystemPosition.end()){
        this->_current->insertInSparsityPattern(itR->second+_numberOfUnknownsPreviouRank, itC->second);
      }
    }
  }
  return;
}

void staticDofManager::systemMPIComm()
{
  if (!_isPartitioned) {
    return;
  }
  if(!_mpiInit) this->initMPI();
  // mpi communication post resolution (exchange dof values)
  for(int i=0;i<Msg::GetCommSize();i++)
  {
    // first recieve from lowest rank
    double valpos; // to get value from system
    double valvelo;
    double valacc;
    int nbdofPerNode =1;
    if(_scheme == nonLinearMechSolver::Explicit or _scheme == nonLinearMechSolver::Implicit)
    {
      nbdofPerNode+=2;
    }
    MPI_Status status;
    if(i < Msg::GetCommRank())
    {
       // dim of buffer;
       std::set<Dof> &RecvSetDof = _mapMPIRecvComm[i];
       int commSize = nbdofPerNode*RecvSetDof.size();
       if(commSize > 0) // otherwise nothing to do
       {
         if(_sizeMPIBuffer < commSize)
         {
            if(_sizeMPIBuffer != 0) delete []_mpiBuffer;
            _sizeMPIBuffer = commSize;
            _mpiBuffer = new double[commSize];
         }
         MPI_Recv(_mpiBuffer,commSize,MPI_DOUBLE,i,i,MPI_COMM_WORLD,&status);
         // put val in _otherPartUnknowns
         int j=0;
         for(std::set<Dof>::const_iterator itR = RecvSetDof.begin(); itR!=RecvSetDof.end(); ++itR)
         {
           std::map<Dof,int>::const_iterator it = _otherPartUnknowns.find(*itR);
           _otherPartPositions[it->second] = this->_mpiBuffer[j];
           if(_scheme == nonLinearMechSolver::Explicit or _scheme == nonLinearMechSolver::Implicit)
           {
             _otherPartVelocities[it->second]= this->_mpiBuffer[j+1];
             _otherPartAccelerations[it->second] = this->_mpiBuffer[j+2];
           }
           j+=nbdofPerNode;
         }
       }
      // send
      std::vector<int> &mapDof = *(_mapMPIDof[i]);
      commSize = nbdofPerNode*mapDof.size();
      if(commSize > 0)
      {
        if(_sizeMPIBuffer < commSize)
        {
          if(_sizeMPIBuffer != 0) delete []_mpiBuffer;
          _sizeMPIBuffer = commSize;
          _mpiBuffer = new double[commSize];
        }
        int j=0;
        for(int k=0; k< mapDof.size(); k++)
        {
          if(_scheme == nonLinearMechSolver::Explicit or _scheme == nonLinearMechSolver::Implicit)
          {
            _NLScurrent->getFromSolution(mapDof[k], valpos,nonLinearBoundaryCondition::position);
            _NLScurrent->getFromSolution(mapDof[k], valvelo,nonLinearBoundaryCondition::velocity);
            _NLScurrent->getFromSolution(mapDof[k], valacc,nonLinearBoundaryCondition::acceleration);
            _mpiBuffer[j] = valpos;
            _mpiBuffer[j+1] = valvelo;
            _mpiBuffer[j+2] = valacc;
          }
          else
          {
            (this->_current)->getFromSolution(mapDof[k], valpos);
            _mpiBuffer[j] = valpos;
          }
          j+=nbdofPerNode;
        }
        MPI_Send(_mpiBuffer,commSize,MPI_DOUBLE,i,Msg::GetCommRank(),MPI_COMM_WORLD);
      }
    }
    else if(i > Msg::GetCommRank()) // if i == Rank nothing to do (no self communication)
    {
      // send
      const std::vector<int> &mapDof = *(_mapMPIDof[i]);
      int commSize = nbdofPerNode*mapDof.size();
      if(commSize > 0)
      {
        if(_sizeMPIBuffer < commSize)
        {
          if(_sizeMPIBuffer != 0) delete []_mpiBuffer;
          _sizeMPIBuffer = commSize;
          _mpiBuffer = new double[commSize];
        }
        int j=0;
        for(int k=0; k<mapDof.size(); k++)
        {
          if(_scheme == nonLinearMechSolver::Explicit or _scheme == nonLinearMechSolver::Implicit)
          {
            _NLScurrent->getFromSolution(mapDof[k], valpos,nonLinearBoundaryCondition::position);
            _NLScurrent->getFromSolution(mapDof[k], valvelo,nonLinearBoundaryCondition::velocity);
            _NLScurrent->getFromSolution(mapDof[k], valacc,nonLinearBoundaryCondition::acceleration);
            _mpiBuffer[j] = valpos;
            _mpiBuffer[j+1] = valvelo;
            _mpiBuffer[j+2] = valacc;
          }
          else
          {
            this->_current->getFromSolution(mapDof[k], valpos);
            _mpiBuffer[j] = valpos;
          }
          j+=nbdofPerNode;
        }
        MPI_Send(_mpiBuffer,commSize,MPI_DOUBLE,i,Msg::GetCommRank(),MPI_COMM_WORLD);
      }
      // recieve
      std::set<Dof> &RecvSetDof = _mapMPIRecvComm[i];
      commSize = nbdofPerNode*RecvSetDof.size();
      if(commSize > 0) // otherwise nothing to do
      {
        if(_sizeMPIBuffer < commSize)
        {
           if(_sizeMPIBuffer != 0) delete []_mpiBuffer;
           _sizeMPIBuffer = commSize;
           _mpiBuffer = new double[commSize];
        }
        MPI_Recv(_mpiBuffer,commSize,MPI_DOUBLE,i,i,MPI_COMM_WORLD,&status);
        // put val in _otherPartUnknowns
        int j=0;
        for(std::set<Dof>::const_iterator itR = RecvSetDof.begin(); itR!=RecvSetDof.end(); ++itR)
        {
          std::map<Dof,int>::const_iterator it = _otherPartUnknowns.find(*itR);
          if(_scheme == nonLinearMechSolver::Explicit or _scheme == nonLinearMechSolver::Implicit)
          {
            _otherPartPositions[it->second] = this->_mpiBuffer[j];
            _otherPartVelocities[it->second]= this->_mpiBuffer[j+1];
            _otherPartAccelerations[it->second] = this->_mpiBuffer[j+2];
          }
          else
          {
            _otherPartPositions[it->second] = _mpiBuffer[j];
          }
          j+=nbdofPerNode;
        }
      }
    }
  }
};

void staticDofManager::linearElastic_assembleLinConst(const Dof &R, const Dof &C, const double &value)
{
  std::map<Dof, int>::iterator itR = unknown.find(R);
  if(itR != unknown.end()) 
  {
    std::map<Dof, DofAffineConstraint<double> >::iterator itConstraint = constraints.find(C);
    if(itConstraint != constraints.end()) 
    {
      double tmp(value);
      for(unsigned i = 0; i < (itConstraint->second).linear.size(); i++) 
      {
        dofTraits<double>::gemm(tmp, (itConstraint->second).linear[i].second, value, 1, 0);
        linearElastic_assemble(R, (itConstraint->second).linear[i].first, tmp);
      }
      double tmp2(value);
      dofTraits<double>::gemm(tmp2, value, itConstraint->second.shift, -1, 0);
      _current->addToRightHandSide(itR->second, tmp2);
    }
  }
  else 
  {
    std::map<Dof, DofAffineConstraint<double> >::iterator itConstraint = constraints.find(R);
    if(itConstraint != constraints.end())
    {
      double tmp(value);
      for(unsigned i = 0; i < (itConstraint->second).linear.size(); i++) 
      {
        dofTraits<double>::gemm(tmp, itConstraint->second.linear[i].second, value, 1, 0);
        linearElastic_assemble((itConstraint->second).linear[i].first, C, tmp);
      }
    }
  }
};

void staticDofManager::linearElastic_assemble(const Dof &R, const Dof& C, const double &value)
{
  if(!_current->isAllocated()) _current->allocate(sizeOfR());
  std::map<Dof, int>::iterator itR = unknown.find(R);
  if(itR != unknown.end()) 
  {
    std::map<Dof, int>::iterator itC = unknown.find(C);
    if(itC != unknown.end()) 
    {
      _current->addToMatrix(itR->second, itC->second, value);
    }
    else 
    {
      std::map<Dof, double>::iterator itFixed = fixed.find(C);
      if(itFixed != fixed.end()) 
      {
        double tmp(itFixed->second);
        dofTraits<double>::gemm(tmp, value, itFixed->second, -1, 0);
        _current->addToRightHandSide(itR->second, tmp);
      }
      else
      {
        linearElastic_assembleLinConst(R, C, value);
      }
    }
  }
  else
  {
    linearElastic_assembleLinConst(R, C, value);
  }
};

void staticDofManager::linearElastic_assemble(const std::vector<Dof> &R, const fullMatrix<double> &m)
{
  if (_scheme != nonLinearMechSolver::StaticLinear) return;
  if(!_current->isAllocated()) _current->allocate(sizeOfR());
  
  std::vector<int> NR(R.size()); // in MPI NR != NC
  std::vector<int> NC(R.size());

  for (unsigned int i = 0; i < R.size(); i++)
  {
   #if defined(HAVE_MPI)
    if(R[i].getType() < 0)
    {
      NR[i] = -1;
      std::map<Dof,int>::iterator itR = _globalOtherPartSystemPosition.find(R[i]);
      if(itR == _globalOtherPartSystemPosition.end())
      {
        NC[i] = -1;
       #ifdef _DEBUG
        std::map<Dof,double>::iterator itR = this->fixed.find(R[i]);
        if(itR == this->fixed.end())
          Msg::Error("Can't find the global column of a dof in mpi on rank  %d",Msg::GetCommRank());
       #endif // _DEBUG

      }
      else
      {
        NC[i] = itR->second;
      }
    }
    else
   #endif // HAVE_MPI
    {
     // std::map<Dof, int>::iterator itLocal = this->unknown.end();
      //for(itLocal = this->unknown.begin();  itLocal!= this->unknown.end(); itLocal ++)
       //      if(itLocal->first == R[i]) break;
      std::map<Dof, int>::iterator itLocal = this->unknown.find(R[i]);
      if (itLocal != this->unknown.end())
      {
        NR[i] = itLocal->second;
        NC[i] = itLocal->second;
      }
      else
      {
        NR[i] = -1;
        NC[i] = -1;
      }
    }
  }
      
  for(std::size_t i = 0; i < R.size(); i++)
  {
    if(NR[i] != -1) 
    {
      for(std::size_t j = 0; j < R.size(); j++) 
      {
        if(NC[j] != -1) 
        {
          _current->addToMatrix(NR[i], NC[j], m(i, j));
        }
        else 
        {
          std::map<Dof, double>::iterator itFixed =  fixed.find(R[j]);
          if(itFixed != fixed.end()) 
          {
            double tmp(itFixed->second);
            dofTraits<double>::gemm(tmp, m(i, j), itFixed->second, -1, 0);
            _current->addToRightHandSide(NR[i], tmp);
          }
          else
            linearElastic_assembleLinConst(R[i], R[j], m(i, j));
        }
      }
    }
    else {
      for(std::size_t j = 0; j < R.size(); j++) 
      {
        linearElastic_assembleLinConst(R[i], R[j], m(i, j));
      }
    }
  }
};


#if defined(HAVE_MPI)
int staticDofManagerFullCG::sizeOfR() const
{
  if (Msg::GetCommSize() > 1 and _isPartitioned)
  {
    return _localNumberDofMPI[Msg::GetCommRank()];
  }
  else
  {
    return staticDofManager::sizeOfR();
  }
};


void staticDofManagerFullCG::initMPI()
{
  if (!_isPartitioned) 
  {
    return;
  }
  
  if(Msg::GetCommSize() > 1)
  {    
    MPI_Status status;
    _otherPartUnknowns.clear();
    _localDofOtherPartUnknowns.clear();
    if (Msg::GetCommRank() > 0)      
    {
      // set data to other rank
      int localUnknownSize = unknown.size();
      std::vector<int> buffer(2*localUnknownSize);
      int j=0;
      for (std::map<Dof, int>::const_iterator it = unknown.begin(); it != unknown.end(); it++)
      {
        const Dof& D = it->first;
        buffer[j] = D.getEntity();
        buffer[j+1]=D.getType();
        j += 2;
      }
      MPI_Send(&localUnknownSize, 1, MPI_INT, 0, Msg::GetCommSize()+ Msg::GetCommRank(), MPI_COMM_WORLD);
      MPI_Send(&buffer[0], 2*localUnknownSize, MPI_INT, 0, Msg::GetCommRank(), MPI_COMM_WORLD);
    }
    else
    {
      std::map<int, std::map<Dof, int> > unknownRanksTrue;
      std::map<int, std::map<Dof, int> > unknownRanks;
      std::map<int, std::set<Dof> > allDofToSendOtherRanks;
      std::map<Dof, int> globalDofNumbers;
      globalDofNumbers = unknown;
      unknownRanks[0] = unknown;
      unknownRanksTrue[0]=unknown;
      _localNumberDofMPI[0] = unknown.size();
      
      for (int i=1; i < Msg::GetCommSize(); i++)
      {
        int localUnknownSizeOtherRank = 0;
        MPI_Recv(&localUnknownSizeOtherRank,1,MPI_INT,i,Msg::GetCommSize()+i,MPI_COMM_WORLD,&status);
        std::vector<int> buffer(2*localUnknownSizeOtherRank);
        MPI_Recv(&buffer[0],2*localUnknownSizeOtherRank,MPI_INT,i,i,MPI_COMM_WORLD,&status);
        for (int j=0; j< localUnknownSizeOtherRank; j++)
        {
          Dof key(buffer[2*j],buffer[2*j+1]);
          if (globalDofNumbers.find(key) == globalDofNumbers.end())
          {
            int oldSize = globalDofNumbers.size();
            globalDofNumbers[key] = oldSize;
            unknownRanksTrue[i][key] = oldSize;
          }
          unknownRanks[i][key] = globalDofNumbers[key];
          for (int k=0; k< i; k++)
          {
            // if key is present in lower rank
            if (unknownRanksTrue[k].find(key) != unknownRanksTrue[k].end())
            {
              allDofToSendOtherRanks[k].insert(key);
              if (_otherPartUnknowns.find(key) == _otherPartUnknowns.end())
              {
                int oldSize = _otherPartUnknowns.size();
                _otherPartUnknowns[key]= oldSize;
              }
              break;
            }
          }
        }
        _localNumberDofMPI[i] = globalDofNumbers.size();
        for (int j=0; j< i; j++)
        {
          _localNumberDofMPI[i] -= _localNumberDofMPI[j];
        }
      }
      
      for (std::map<Dof, DofAffineConstraint<double> >::const_iterator itc = constraints.begin(); itc != constraints.end(); itc++)
      {
        const DofAffineConstraint<double>& con = itc->second;
        for (int j=0; j< con.linear.size(); j++)
        {
          const Dof& D = con.linear[j].first;
          if (globalDofNumbers.find(D) != globalDofNumbers.end())
          {
            for (int i=0; i< Msg::GetCommSize(); i++)
            {
              if (unknownRanksTrue[i].find(D) != unknownRanksTrue[i].end())
              {
                allDofToSendOtherRanks[i].insert(D);
                if (_otherPartUnknowns.find(D) == _otherPartUnknowns.end())
                {
                  int oldSize = _otherPartUnknowns.size();
                  _otherPartUnknowns[D]= oldSize;
                }
              }
              else
              {
                unknownRanks[i][D] = globalDofNumbers[D];
              }
            }
          }
        }
      }
            
      // send to other rank
      for (int i=1; i < Msg::GetCommSize(); i++)
      {
        int localUnknownSizeOtherRank = unknownRanks[i].size();
        std::vector<int> buffer(4*localUnknownSizeOtherRank);
        int j=0;
        for (std::map<Dof, int>::const_iterator it = unknownRanks[i].begin(); it != unknownRanks[i].end(); it++)
        { 
          buffer[j] = it->first.getEntity();
          buffer[j+1] = it->first.getType();
          buffer[j+2] = it->second;
          if (allDofToSendOtherRanks[i].find(it->first) == allDofToSendOtherRanks[i].end())
          {
            buffer[j+3] =0; 
          }
          else
          {
            buffer[j+3] =1; 
          }
          j += 4;
        }
        MPI_Send(&localUnknownSizeOtherRank, 1, MPI_INT, i, Msg::GetCommSize()+i, MPI_COMM_WORLD);
        MPI_Send(&buffer[0], 4*localUnknownSizeOtherRank, MPI_INT, i, i, MPI_COMM_WORLD);
      }
      _localDofOtherPartUnknowns = allDofToSendOtherRanks[0];
      unknown = unknownRanks[0];
    }
    
    if (Msg::GetCommRank() > 0)
    {
      int localUnknownSizeOtherRank = 0;
      MPI_Recv(&localUnknownSizeOtherRank,1,MPI_INT,0, Msg::GetCommSize()+Msg::GetCommRank(),MPI_COMM_WORLD,&status);
      std::vector<int> buffer(4*localUnknownSizeOtherRank);
      MPI_Recv(&buffer[0],4*localUnknownSizeOtherRank,MPI_INT,0, Msg::GetCommRank(),MPI_COMM_WORLD,&status);
      for (int j=0; j< localUnknownSizeOtherRank; j++)
      {
        Dof key(buffer[4*j],buffer[4*j+1]);
        unknown[key] = buffer[4*j+2];
        if (buffer[4*j+3] == 1)
        {
          _localDofOtherPartUnknowns.insert(key);
        }
      }
    }
    
    MPI_Bcast(_localNumberDofMPI, Msg::GetCommSize(), MPI_INT, 0, MPI_COMM_WORLD);
    int numDofOtherRanks = 0;
    if (Msg::GetCommRank() ==0)
    {
      numDofOtherRanks = _otherPartUnknowns.size();
    }
    MPI_Bcast(&numDofOtherRanks, 1, MPI_INT, 0, MPI_COMM_WORLD);
    std::vector<int> otherPartUnknownsBuffer(3*numDofOtherRanks);
    if (Msg::GetCommRank() == 0)
    {
      int j=0;
      for (std::map<Dof, int>::const_iterator it = _otherPartUnknowns.begin(); it != _otherPartUnknowns.end(); it++)
      { 
        otherPartUnknownsBuffer[j] = it->first.getEntity();
        otherPartUnknownsBuffer[j+1] = it->first.getType();
        otherPartUnknownsBuffer[j+2] = it->second;
        j += 3;
      }
    }
    MPI_Bcast(&otherPartUnknownsBuffer[0], 3*numDofOtherRanks, MPI_INT, 0, MPI_COMM_WORLD);
    if (Msg::GetCommRank() > 0)
    {
      for (int j=0; j< numDofOtherRanks; j++)
      {
        Dof key(otherPartUnknownsBuffer[3*j],otherPartUnknownsBuffer[3*j+1]);
        _otherPartUnknowns[key] = otherPartUnknownsBuffer[3*j+2];
      }
    }
    
    
    int nbdofOtherPart = _otherPartUnknowns.size();
    this->_otherPartPositions = new double[nbdofOtherPart];
    this->_previousOtherPartPositions = new double[nbdofOtherPart];
    for (int pp=0; pp < nbdofOtherPart; pp++)
    {
      this->_otherPartPositions[pp] = 0;
      this->_previousOtherPartPositions[pp] = 0;
    }
    
    if(_scheme==nonLinearMechSolver::Explicit or _scheme == nonLinearMechSolver::Implicit)
    {
      this->_otherPartVelocities = new double[nbdofOtherPart];
      this->_previousOtherPartVelocities = new double[nbdofOtherPart];
      this->_otherPartAccelerations = new double[nbdofOtherPart];
      this->_previousOtherPartAccelerations = new double[nbdofOtherPart];
      for (int pp=0; pp < nbdofOtherPart; pp++)
      {
        _otherPartVelocities[pp] = 0.;
        _previousOtherPartVelocities[pp] = 0.;
        _otherPartAccelerations[pp] = 0.;
        _previousOtherPartAccelerations[pp] = 0.;
      }
    }
  }
  _mpiInit = true;
}

void staticDofManagerFullCG::allocateSystem()
{
  if(!_mpiInit) this->initMPI();
  staticDofManager::allocateSystem();
}

void staticDofManagerFullCG::setFirstRigidContactUnknowns()
{
  if (Msg::GetCommSize() > 1 and _isPartitioned)
  {
    if(!_mpiInit) this->initMPI();
    _firstRigidContactDof = _localNumberDofMPI[Msg::GetCommRank()];
  }
  else
  {
    staticDofManager::setFirstRigidContactUnknowns();
  }
}

void staticDofManagerFullCG::manageMPIComm(const int otherPartNum,const std::vector<Dof> &otherPartR)
{
  // do nothing
}
void staticDofManagerFullCG::manageMPISparsity(const std::vector<Dof> &myR, const int otherPartNum,const std::vector<Dof> &otherPartR)
{
  // do nothing
}

void staticDofManagerFullCG::systemMPIComm()
{
  if (!_isPartitioned) {
    return;
  }
  if(!_mpiInit) this->initMPI();
  
  int rank = Msg::GetCommRank();
  int numDofs = _localDofOtherPartUnknowns.size();
  int nbdofPerNode =1;
  if(_scheme == nonLinearMechSolver::Explicit or _scheme == nonLinearMechSolver::Implicit)
  {
    nbdofPerNode+=2;
  }
  int mpiBufferSize =_otherPartUnknowns.size() * nbdofPerNode;
  std::vector<double> mpiBuffer(mpiBufferSize);
  std::fill(mpiBuffer.begin(), mpiBuffer.end(),0.);
  for(std::set<Dof>::const_iterator itR = _localDofOtherPartUnknowns.begin(); itR!=_localDofOtherPartUnknowns.end(); itR++)
  {
    int loc = _otherPartUnknowns[*itR]; 
    int NR = unknown[*itR];
    if(_scheme == nonLinearMechSolver::Explicit or _scheme == nonLinearMechSolver::Implicit)
    {
      _NLScurrent->getFromSolution(NR, mpiBuffer[3*loc],nonLinearBoundaryCondition::position);
      _NLScurrent->getFromSolution(NR, mpiBuffer[3*loc+1],nonLinearBoundaryCondition::velocity);
      _NLScurrent->getFromSolution(NR, mpiBuffer[3*loc+2],nonLinearBoundaryCondition::acceleration);
    }
    else
    {
      (this->_current)->getFromSolution(NR, mpiBuffer[loc]);
    }
  }
  MPI_Allreduce(MPI_IN_PLACE,&mpiBuffer[0],mpiBufferSize,MPI_DOUBLE,MPI_SUM,MPI_COMM_WORLD);
  for (int j=0; j< _otherPartUnknowns.size(); j++)
  {
    _otherPartPositions[j] = mpiBuffer[nbdofPerNode*j];
    if(_scheme == nonLinearMechSolver::Explicit or _scheme == nonLinearMechSolver::Implicit)
    {
       _otherPartVelocities[j]= mpiBuffer[nbdofPerNode*j+1];
       _otherPartAccelerations[j] = mpiBuffer[nbdofPerNode*j+2];
    }
  }
};

void staticDofManagerFullCG::getDofValue(const Dof& key,  double &val, nonLinearBoundaryCondition::whichCondition wv) const
{
  switch(wv)
  {
    case nonLinearBoundaryCondition::position:
    {
      // look first for fixed dof (the type can be negative)
      std::map<Dof, double>::const_iterator it = this->fixed.find(key);
      if (it != this->fixed.end()) 
      {
        val =  it->second;
        return ;
      }
      std::map<Dof,int>::const_iterator itR = this->_otherPartUnknowns.find(key);
      if(itR != _otherPartUnknowns.end())
      {
        val = this->_otherPartPositions[itR->second];
        return;
      }
      std::map<Dof, int>::const_iterator itK = this->unknown.find(key);
      if (itK != this->unknown.end()) 
      {
        _current->getFromSolution(itK->second, val);
        return;
      }
      std::map<Dof, DofAffineConstraint< double > >::const_iterator itC = this->constraints.find(key);
      if (itC != this->constraints.end())
      {
        double tmp(val);
        val = itC->second.shift;
        for (unsigned i=0;i<(itC->second).linear.size();i++)
        {
           std::map<Dof, int>::const_iterator itu = this->unknown.find(((itC->second).linear[i]).first);
           getDofValue(((itC->second).linear[i]).first, tmp, wv);
           dofTraits<double>::gemm(val,((itC->second).linear[i]).second, tmp, 1, 1);
        }
        return ;
      }
      Msg::Error("position Dof not found %d %d",key.getEntity(), key.getType());
      Msg::Exit(0);
    }
    break;
    case nonLinearBoundaryCondition::velocity:
    {
      // look first for fixed Dof
      std::map<Dof, FixedNodeDynamicData<double> >::const_iterator it = this->_fixedDynamic.find(key);
      if (it != this->_fixedDynamic.end()) 
      {
        val =  it->second.velocity;
        return ;
      }
      std::map<Dof,int>::const_iterator itR = this->_otherPartUnknowns.find(key);
      if (itR != _otherPartUnknowns.end())
      {
        val = this->_otherPartVelocities[itR->second];
        return;
      }
      std::map<Dof, int>::const_iterator itK = this->unknown.find(key);
      if (itK != this->unknown.end()) 
      {
        _NLScurrent->getFromSolution(itK->second, val,wv);
        return;
      }
      Msg::Error("velocity Dof not found %d %d",key.getEntity(), key.getType());
      Msg::Exit(0);
    }
    break;
    case nonLinearBoundaryCondition::acceleration:
    {
      // look first for fixed Dof
      std::map<Dof, FixedNodeDynamicData<double> >::const_iterator it = this->_fixedDynamic.find(key);
      if (it != this->_fixedDynamic.end()) 
      {
        val =  it->second.acceleration;
        return ;
      }
      std::map<Dof,int>::const_iterator itR = this->_otherPartUnknowns.find(key);
      if(itR != _otherPartUnknowns.end())
      {
        val = this->_otherPartAccelerations[itR->second];
        return;
      }
      std::map<Dof, int>::const_iterator itK = this->unknown.find(key);
      if (itK != this->unknown.end()) 
      {
        _NLScurrent->getFromSolution(itK->second, val,wv);
        return;
      }
      Msg::Error("velocity Dof not found %d %d",key.getEntity(), key.getType());
      Msg::Exit(0);
    }
    break;
  };
};

#endif // HAVE_MPI

#endif // HAVE_MPI
