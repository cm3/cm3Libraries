//
// Description: Derivate class of SimpleFunction to include a time dependency
//
//
// Author:  <Gauthier BECKER>, (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
// As this function is templated, it has to be in a separated files of others timeFunction (SWIG generation problem)
#ifndef _SIMPLEFUNCTIONTIME_H_
#define _SIMPLEFUNCTIONTIME_H_
#include "simpleFunction.h"
#include "scalarFunction.h"
template<class scalar>
class simpleFunctionTime : public simpleFunction<scalar>{
 protected :
  double time;
  bool timeDependency;
  bool unknownDependency;
  scalarFunction* dependenceFunction;
  std::vector<int> dependenceComponents; // dependenceComponents = number of input of dependenceComponents
 public :
  simpleFunctionTime(scalar val=0, bool td=true, double t=1.) : simpleFunction<scalar>(val), time(t), timeDependency(td),
      unknownDependency(false),dependenceFunction(NULL),dependenceComponents(){}; // time=1 by default to avoid set time for Static linear Scheme
  virtual ~simpleFunctionTime(){
    if (unknownDependency){
      delete dependenceFunction;
      dependenceFunction = NULL;
      unknownDependency = false;
    }
  };
 #ifndef SWIG
  virtual scalar operator () (double x, double y, double z) const { if(timeDependency) return time*this->_val; else return this->_val;}
  virtual void setTime(const double t){time=t;}
  void scaleVal(const double fac){this->_val*=fac;}
  virtual const scalarFunction* getDependenceFunction() const {return dependenceFunction;};
  virtual const std::vector<int>& getDependenceComponents() const {return dependenceComponents;};
  virtual bool withUnknownDependency() const {return unknownDependency;};
 #endif
 
  virtual void setDependenceFunction(const scalarFunction& fct){
    unknownDependency = true;
    dependenceComponents.clear();
    if (dependenceFunction != NULL) delete dependenceFunction;
    dependenceFunction = fct.clone();
  }
  virtual void setDependenceComponents(const int comp){
    dependenceComponents.push_back(comp);
  }
};
#endif // _SIMPLEFUNCTIONTIME_H_

