//
// C++ Interface: terms
//
// Description: python function
//
//
// Author:  <Van Dung NGUYEN>, (C) 2019
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef _NLSPYTHONFUNCTION_
#define _NLSPYTHONFUNCTION_
#include "timeFunction.h"
#include "MIMOFunction.h"
#include "UserDirectionGenerator.h"
#include "generalMapping.h"

/**
 * Class for python user defined function
 * The user has to define in it driver a
 * def my_function(x,y,z,t,extra=None)
 *     ...
 *     return <a float>
 * where extra is a python tuple that contains
 * extra python argument for the function
 */
 #undef HAVE_DLOPEN
 #include <Python.h>
class PythonBCfunctionDouble : public simpleFunctionTime<double>
{
 protected:
  PyObject* _pyFunction;
  PyObject* _pyArgs; // Assume that is a tuple
 public:
  PythonBCfunctionDouble(PyObject* pyfunc, PyObject* args) : simpleFunctionTime<double>(),
                                                       _pyFunction(pyfunc), _pyArgs(args){}
 #ifndef SWIG
  PythonBCfunctionDouble(const PythonBCfunctionDouble &src) : simpleFunctionTime<double>(src),
                                                  _pyFunction(src._pyFunction),
                                                  _pyArgs(src._pyArgs){}
  virtual ~PythonBCfunctionDouble(){}
  virtual double operator() (double x, double y, double z) const {
    PyObject* tuple = Py_BuildValue("ddddO",x, y, z, this->time,_pyArgs);
    PyObject* returnVal = PyObject_Call(_pyFunction,tuple,NULL);
    return PyFloat_AsDouble(returnVal);
  }
 #endif // SWIG
};


class UserDirectionGeneratorPython : public UserDirectionGenerator
{
  protected:
    PyObject *_pycallback;
    // input with a python function
    // def pyFunc(x,y,z):
    //    # implementation
    // return n1, n2, n3
    // 
    
  public:
    UserDirectionGeneratorPython(PyObject *callback) :UserDirectionGenerator(),_pycallback(callback){}
    #ifndef SWIG
    UserDirectionGeneratorPython(const UserDirectionGeneratorPython& src): UserDirectionGenerator(src),_pycallback(src._pycallback){}
    virtual UserDirectionGenerator* clone() const{return new UserDirectionGeneratorPython(*this);}
    
    virtual void getTransverseDirection(SVector3 &A, const SVector3 &pg) const
    {
      PyObject *pyargs = Py_BuildValue("(ddd)", pg[0], pg[1], pg[2]);
      PyObject *result = PyEval_CallObject(_pycallback, pyargs);
      if(result) 
      {
        if (PyTuple_Size(result)!=3)
        {
          Msg::Error("The python function did not return a tuple of three doubles !!!");
          Msg::Exit(0);
        }
        PyObject* out1 = PyTuple_GetItem(result,0);
        PyObject* out2 = PyTuple_GetItem(result,1);
        PyObject* out3 = PyTuple_GetItem(result,2);
          
        int ok = PyArg_Parse(out1, "d", &A[0]);
        if (!ok)
        {
          Msg::Error("The python function did not return a tuple of three doubles !!!");
          Msg::Exit(0);
        }
        ok = PyArg_Parse(out2, "d", &A[1]);
        if (!ok)
        {
          Msg::Error("The python function did not return a tuple of three doubles !!!");
          Msg::Exit(0);
        }
        ok = PyArg_Parse(out3, "d", &A[2]);
        if (!ok)
        {
          Msg::Error("The python function did not return a tuple of three doubles !!!");
          Msg::Exit(0);
        };
        Py_DECREF(result);
      }
      else 
      {
        Msg::Error("An error occurs in the input python function in UserDirectionGeneratorPython!!!");
        Msg::Exit(0);
      }
      //
      Py_DECREF(pyargs);
    };
    #endif //SWIG
};

/**
 * class ScalerPython for data scaling
 */
 
static PyObject* convertFullMatrixToPythonListOfLists(const fullMatrix<double>& mat)
{
  int numRows = mat.size1();
  int numCols = mat.size2();
  PyObject *pyListObj =  PyList_New(numRows);
  for (int r =0; r < numRows; r++)
  {
    PyObject *oneRow =  PyList_New(numCols);
    for (int c = 0; c < numCols; c++)
    {
      double matVal = mat(r,c);
      PyObject* EVal = PyFloat_FromDouble(matVal);
      PyList_SetItem(oneRow, c, EVal);
    }
    PyList_SetItem(pyListObj, r, oneRow);
  };
  return pyListObj;
};

static void convertPythonListOfListsToFullMatrix(PyObject* pList, fullMatrix<double>& mat)
{
  if (!PyList_Check(pList))
  {
    Msg::Error("first output is not a python list");
    Msg::Exit(0);
  }
  bool allocated = false;
  int pListSize= PyList_Size(pList);
  for (int i=0; i< pListSize; i++)
  {
    PyObject* rowi= PyList_GetItem(pList,i);
    if (!rowi)
    {
      Msg::Error("cannot get value from list");
      Msg::Exit(0);
    }
    int pListSizeItem= PyList_Size(rowi);
    if (!allocated)
    {
      allocated = true;
      mat.resize(pListSize,pListSizeItem);
    };
    
    for (int j=0; j< pListSizeItem; j++)
    {
      PyObject* valij= PyList_GetItem(rowi,j);
      mat(i,j)= PyFloat_AsDouble(valij);
    }
  }
};

static PyObject* convertFullMatrixToPythonList(const fullMatrix<double>& mat)
{
  int numRows = mat.size1();
  if (numRows > 1)
  {
    return convertFullMatrixToPythonListOfLists(mat);
  }
  else if (numRows == 1)
  {
    int numCols = mat.size2();
    PyObject *oneRow =  PyList_New(numCols);
    for (int c = 0; c < numCols; c++)
    {
      double matVal = mat(0,c);
      PyObject* EVal = PyFloat_FromDouble(matVal);
      PyList_SetItem(oneRow, c, EVal);
    }
    return oneRow;
  }
  else
  {
    Msg::Error("matrix of zero rows");
    return NULL;
  }
};

static void convertPythonListToFullMatrix(PyObject* pList, fullMatrix<double>& mat)
{
  if (!PyList_Check(pList))
  {
    Msg::Error("first output is not a python list");
    Msg::Exit(0);
  }
  bool allocated = false;
  int pListSize= PyList_Size(pList);
  mat.resize(1,pListSize);
  for (int i=0; i< pListSize; i++)
  {
    PyObject* vali= PyList_GetItem(pList,i);
    if (!vali)
    {
      Msg::Error("cannot get value from list");
      Msg::Exit(0);
    }
    mat(0,i)= PyFloat_AsDouble(vali);
  }
};

 
class ScalerPython : public Scaler
{
   /* include the python object of the class with the following methods
    * class ASL:
    *   def tranform(A, bool):
    *       ....
    *       return (normedA, DnormedA_DA) if bool = True or (normedA, None) if bool = False
    *   def inverse_tranform(normedA, bool):
    *       ...
    *       return (A, DA_DnormedA) with bool = True or (A, None) if bool = False
    *  to make compatible, the terms A, normedA, DnormedA_DA, DA_DnormedA are all list of lists
    *  
    * */
  protected:
    PyObject *_pyObj;
  
  public:
    ScalerPython(PyObject* pyObj): _pyObj(pyObj){}
    ScalerPython(const ScalerPython& src): Scaler(src), _pyObj(src._pyObj){}
    virtual Scaler* clone() const {return new ScalerPython(_pyObj);};
    virtual ~ScalerPython(){}
    virtual void transform(const fullMatrix<double>& XReal, fullMatrix<double>& Xnormed,
                           bool stiff=false, fullMatrix<double>* DXnormedDXReal=NULL) const
    {
      PyObject *pyargsXReal = convertFullMatrixToPythonListOfLists(XReal);
      PyObject* pyargsDiffFlag = PyBool_FromLong(stiff);      
      PyObject *result = PyObject_CallMethod(_pyObj, "transform" ,"OO",pyargsXReal, pyargsDiffFlag);
      if (!result)
      {
        Msg::Error("python function cannot be called");
        Msg::Exit(0);
      }
      else if (!PyTuple_Check(result))
      {
        Msg::Error("output of python must be a tuple");
        Msg::Exit(0);
      }

      if (PyTuple_Size(result)!=2)
      {
        Msg::Error("output of python must be a tuple of 2 elements");
        Msg::Exit(0);
      }
      
      PyObject* out1 = PyTuple_GetItem(result,0);
      convertPythonListOfListsToFullMatrix(out1,Xnormed);
      if (stiff)
      {
        PyObject* out2 = PyTuple_GetItem(result,1);
        convertPythonListOfListsToFullMatrix(out2,*DXnormedDXReal);
      }
      Py_DECREF(pyargsXReal);   
      Py_DECREF(result);
    };
    virtual void inverse_transform(const fullMatrix<double>& Xnormed, fullMatrix<double>& XReal,
                                   bool stiff=false, fullMatrix<double>* DXRealDXnormed=NULL) const
    {
      PyObject *pyargsXReal = convertFullMatrixToPythonListOfLists(Xnormed);
      PyObject* pyargsDiffFlag = PyBool_FromLong(stiff);      
      PyObject *result = PyObject_CallMethod(_pyObj, "inverse_transform" ,"OO",pyargsXReal, pyargsDiffFlag);
      if (!result)
      {
        Msg::Error("python function cannot be called");
        Msg::Exit(0);
      }
      else if (!PyTuple_Check(result))
      {
        Msg::Error("output of python must be a tuple");
        Msg::Exit(0);
      }

      if (PyTuple_Size(result)!=2)
      {
        Msg::Error("output of python must be a tuple of 2 elements");
        Msg::Exit(0);
      }
      
      PyObject* out1 = PyTuple_GetItem(result,0);
      convertPythonListOfListsToFullMatrix(out1,XReal);
      if (stiff)
      {
        PyObject* out2 = PyTuple_GetItem(result,1);
        convertPythonListOfListsToFullMatrix(out2,*DXRealDXnormed);
      }
      Py_DECREF(pyargsXReal);   
      Py_DECREF(result);
    };
};

/**
 * Class for python user defined function
 * def my_function(E1, E0, q0, S0, stiff)
 *     ...
 *     return (S1, q1, DS1_DE1)
 * E1, E0, q0, S0, S1, q1 are lists 
 * DS1_DE1 is a list of lists
 */

class MIMOFunctionPython : public MIMOFunction
{
 protected:
  PyObject *_pyObj;
  int _type; // type = 0, a function, type = 1 an object with a equivalent method -object::predict
  
 public:
  MIMOFunctionPython(PyObject* pyo, int type=0): MIMOFunction(), _pyObj(pyo),_type(type){}
  MIMOFunctionPython(const MIMOFunctionPython &src) : MIMOFunction(src),_pyObj(src._pyObj), _type(src._type){}
  virtual ~MIMOFunctionPython(){}
  virtual MIMOFunction* clone() const {return new MIMOFunctionPython(_pyObj,_type);};
  virtual void predict(const fullMatrix<double>& E1,
                       const fullMatrix<double>& E0,
                       const fullMatrix<double>& q0,
                       const fullMatrix<double>& S0,
                       fullMatrix<double>& q1,
                       fullMatrix<double>& S1,
                       bool diff,
                       fullMatrix<double>* DSDE) const
  {
    //
    PyObject *pyargsE1 = convertFullMatrixToPythonList(E1);
    PyObject *pyargsE0 = convertFullMatrixToPythonList(E0);
    PyObject *pyargsQ0 = convertFullMatrixToPythonList(q0);
    PyObject *pyargsS0 = convertFullMatrixToPythonList(S0);
    PyObject* pyargsDiffFlag = PyBool_FromLong(diff);
    
    PyObject *pArgTuple = PyTuple_New(5);
    PyTuple_SetItem(pArgTuple, 0, pyargsE1);
    PyTuple_SetItem(pArgTuple, 1, pyargsE0);
    PyTuple_SetItem(pArgTuple, 2, pyargsQ0);
    PyTuple_SetItem(pArgTuple, 3, pyargsS0);
    PyTuple_SetItem(pArgTuple, 4, pyargsDiffFlag);
    
    // call function
    PyObject *result = NULL;
    if (_type == 0)
    {
       result= PyObject_CallObject(_pyObj, pArgTuple);
    }
    else if (_type == 1)
    {
      //result = PyObject_CallMethod(_pyObj, "predict" ,"OOOOO",pyargsE1,pyargsE0,pyargsQ0,pyargsS0,pyargsDiffFlag);
      result = PyObject_CallMethod(_pyObj, "predict" ,"O",pArgTuple);
    }
    else
    {
      Msg::Error("type %d is not defined in MIMOFunction::predict",_type);
    }

    if (!result)
    {
      Msg::Error("python function cannot be called in MIMOFunction::predict");
      Msg::Exit(0);
    }
    else if (!PyTuple_Check(result))
    {
      Msg::Error("output of python must be a tuple");
      Msg::Exit(0);
    }

    if (PyTuple_Size(result)!=3)
    {
      Msg::Error("output of python must be a tuple of 3 elements");
      Msg::Exit(0);
    }

    // first result
    PyObject* out1 = PyTuple_GetItem(result,0);
    convertPythonListToFullMatrix(out1,S1);
    
    PyObject* out2 = PyTuple_GetItem(result,1);
    convertPythonListToFullMatrix(out2,q1);
    
    if (diff)
    {
      // third result
      PyObject* out3 = PyTuple_GetItem(result,2);
      convertPythonListOfListsToFullMatrix(out3,*DSDE);
      //DSDE->print("DoDi");
    }

    Py_DECREF(result);
    Py_DECREF(pArgTuple);
    Py_DECREF(pyargsE1);
    Py_DECREF(pyargsE0);
    Py_DECREF(pyargsS0);
    Py_DECREF(pyargsQ0);
  }
};

class scalarFunctionPython  : public scalarFunction
{
  protected:
    PyObject *_pyObj;
  
  public:
    scalarFunctionPython(PyObject* pyo): scalarFunction(), _pyObj(pyo){}
    scalarFunctionPython(const scalarFunctionPython& src): scalarFunction(src), _pyObj(src._pyObj){}
    virtual ~scalarFunctionPython(){}
    virtual void setElement(MElement* el) const{}
    virtual double getVal(const double x) const
    {
      PyObject *result = PyEval_CallFunction(_pyObj, "d", x);
      if(result) 
      {
        if (PyTuple_Size(result)!=3)
        {
          Msg::Error("The python function did not return a tuple of three doubles !!!");
          Msg::Exit(0);
        }
        double A;
        PyObject* out1 = PyTuple_GetItem(result,0);
        int ok = PyArg_Parse(out1, "d", &A);
        if (!ok)
        {
          Msg::Error("The python function did not return a tuple of three doubles !!!");
          Msg::Exit(0);
        }
        Py_DECREF(result);      
        return A;
      }
      else 
      {
        Msg::Error("An error occurs in the input python function in scalarFunctionPython::getVal!!!");
        Msg::Exit(0);
      }
    };
    virtual double getDiff(const double x) const
    {
      PyObject *result = PyEval_CallFunction(_pyObj, "d", x);
      if(result) 
      {
        if (PyTuple_Size(result)!=3)
        {
          Msg::Error("The python function did not return a tuple of three doubles !!!");
          Msg::Exit(0);
        }
        double A;
        PyObject* out1 = PyTuple_GetItem(result,1);
        int ok = PyArg_Parse(out1, "d", &A);
        if (!ok)
        {
          Msg::Error("The python function did not return a tuple of three doubles !!!");
          Msg::Exit(0);
        }
        Py_DECREF(result);      
        return A;
      }
      else 
      {
        Msg::Error("An error occurs in the input python function in scalarFunctionPython::getDiff!!!");
        Msg::Exit(0);
      }
    }
    virtual double getDoubleDiff(const double x) const
    {
      PyObject *result = PyEval_CallFunction(_pyObj, "d", x);
      if(result) 
      {
        if (PyTuple_Size(result)!=3)
        {
          Msg::Error("The python function did not return a tuple of three doubles !!!");
          Msg::Exit(0);
        }
        double A;
        PyObject* out1 = PyTuple_GetItem(result,2);
        int ok = PyArg_Parse(out1, "d", &A);
        if (!ok)
        {
          Msg::Error("The python function did not return a tuple of three doubles !!!");
          Msg::Exit(0);
        }
        Py_DECREF(result); 
        return A;
      }
      else 
      {
        Msg::Error("An error occurs in the input python function in scalarFunctionPython::getDoubleDiff!!!");
        Msg::Exit(0);
      }
    }
    virtual scalarFunction* clone() const {return new scalarFunctionPython(_pyObj);};
};


class generalMappingPython : public generalMapping
{
  protected:
    PyObject *_pyObj; 
    /* 
     * any python function with the following form
    def my_function(x0, x1, ..., xN)
 *     ...
 *     return y0, y1, .. ,yM
 */
    
  public:
    generalMappingPython(PyObject* pyo): generalMapping(), _pyObj(pyo){}
    generalMappingPython(const generalMappingPython& src): generalMapping(src), _pyObj(src._pyObj){}
    virtual ~generalMappingPython(){}
    virtual generalMapping* clone() const {return new generalMappingPython(*this);}
    virtual std::vector<double> evaluate(const std::vector<double>& x) const
    {
      int numInputs = x.size();
      PyObject *pArgs = PyTuple_New(numInputs);
      for (int i=0; i< numInputs; i++)
      {
        PyObject* EVal = PyFloat_FromDouble(x[i]);
        PyTuple_SetItem(pArgs, i, EVal);
      }
      PyObject *result = PyEval_CallFunction(_pyObj, "O", pArgs);
      std::vector<double> outRes;
      if(result) 
      {
        int numOuts =  PyTuple_Size(result);
        outRes.resize(numOuts,0.);
        for (int i=0; i< numOuts; i++)
        {
          PyObject* out1 = PyTuple_GetItem(result,i);
          outRes[i] = PyFloat_AsDouble(out1);
        }
        
        Py_DECREF(pArgs);
        Py_DECREF(result);      
        return outRes;
      }
      else 
      {
        Msg::Error("An error occurs in the input python function in generalMappingPython::evaluate!!!");
        Msg::Exit(0);
      }
    };
};

#endif //  _NLSPYTHONFUNCTION_

