//
//
// Description: implicit CH scheme
//
// Author:  <Van Dung NGUYEN>, (C) 2013
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef IMPLICITEHULBERTCHUNGPETSC_H_
#define IMPLICITEHULBERTCHUNGPETSC_H_

#include "nonLinearSystems.h"
#if defined(HAVE_PETSC)

template<class scalar>
class implicitStepPetsc{
 public : // Direct access by the system
  Vec _Fext, _Fint, _x, _xdot, _xddot;
  implicitStepPetsc(){}
  ~implicitStepPetsc(){}
  void clear(){
   _try(VecDestroy(&_Fext));
   _try(VecDestroy(&_Fint));
   _try(VecDestroy(&_x));
   _try(VecDestroy(&_xdot));
   _try(VecDestroy(&_xddot));
  }
  implicitStepPetsc& operator=(const implicitStepPetsc &other)
  {
    _try(VecCopy(other._Fext,_Fext));
    _try(VecCopy(other._Fint,_Fint));
    _try(VecCopy(other._x,_x));
    _try(VecCopy(other._xdot,_xdot));
    _try(VecCopy(other._xddot,_xddot));
    return *this;
  }
  
  void restart()
  {
    auto restart_petsc_array = [](Vec &a,int vsize)
    {
      PetscScalar* tarray;
      _try(VecGetArray(a,&tarray));
      restartManager::restart(tarray,vsize);
      _try(VecRestoreArray(a,&tarray));
    };

    PetscInt nbRows;
    _try(VecGetLocalSize(_x,&nbRows));
    int trow = nbRows;
    restartManager::restart(trow);
    if(trow != (int)nbRows) Msg::Error("implicitStepPetsc::restart cannot load a petsc step with a different size from the saved one");

    restart_petsc_array(_x,nbRows);
    restart_petsc_array(_xdot,nbRows);
    restart_petsc_array(_xddot,nbRows);
    restart_petsc_array(_Fext,nbRows);
    restart_petsc_array(_Fint,nbRows);

    return;
  }


  void allocate(int nbRows, Vec* vec = NULL){
    if (vec == NULL){
      _try(VecCreate(PETSC_COMM_WORLD, &_x));
      _try(VecSetSizes(_x, nbRows, PETSC_DETERMINE));
      _try(VecSetFromOptions(_x)); 
    }
    else{
      _try(VecDuplicate(*vec,&_x));
    }

    _try(VecDuplicate(_x, &_Fext));
    _try(VecDuplicate(_x, &_Fint));
    _try(VecDuplicate(_x, &_xdot));
    _try(VecDuplicate(_x, &_xddot));
  }
};

/**
Class for solving the dynamic problem
(1-alpham)*M*xddot_{n+1} + alpham*xddot_n = (1-alphaf)*(fext_{n+1} - fint_{n+1})+ alphaf*(fext_n -fint_n)
**/


template<class scalar>
class implicitHulbertChungPetsc : public nonLinearSystem<scalar>,
                                  public linearSystemPETSc<scalar>{

  protected:
    double _alpham,_alphaf, _beta,_gamma; // parameters of scheme
    double _timeStep; // value of time step
    int _nbRows; // To know the system size
    implicitStepPetsc<scalar> *_currentStep, *_previousStep, *_initialStep;
    Mat _M; // mass matrix
    mutable bool _valuesNotAssembledMass;
		bool _valueNotAssembedInitialBC;
    
    // Predictor management
    int _nbNoMassKeys; // number of dof for which the predictor is equal to the previous step
    std::vector<int> _noMassKeys; // Keys of concerned dof
    std::vector<scalar> _previousValueNoMassKeys;
    std::vector<scalar> _zeros;
    
    int _numIteration;
    double OneMinusAlphaf; // 1 - alphaf
    double AlphafMinusOne; //
    double MinusAlphaf;
    double OneMinusAlpham; // 1-alpham
    double MinusAlpham; //
    double AlphamMinusOne; //
    double Deltat2halfMinusbeta;// dt^2(0.5-beta)
    double DeltatOneMinusGamma; // dt(1-gamma)
    double GammaDivByBetaDeltat; // gamma/(beta*dt)
    double OneMinusAlphamDivByBetaDeltat2; // (1-alpham)/(beta*dt^2)
    double OneDivByBetaDeltat2; // 1/(beta*dt^2)

  // For rigid contact in MPI ( a double* contains the MPIAllReduce performs in the dofManager which ensure MPI communication)
    scalar* _reducedRigidContactForceMPI;
    int _indexFirstRigidContactMPI;

    mutable bool _flagb; // flag for right hand side computation
    
    protected:
      void _assembleVecsInitialBCIfNeeded()
      {
        #if defined(HAVE_MPI)
        if(this->_comm == PETSC_COMM_WORLD) {
          if(Msg::GetCommSize() > 1) {
            int value = _valueNotAssembedInitialBC ? 1 : 0;
            int sumValue = 0;
            MPI_Allreduce((void *)&value, (void *)&sumValue, 1, MPI_INT, MPI_SUM,
                          this->_comm);
            if((sumValue > 0) && (sumValue < Msg::GetCommSize())) {
              _valueNotAssembedInitialBC = 1;
            }
          }
        }
        #endif
        if(_valueNotAssembedInitialBC) {
          _try(VecAssemblyBegin(_currentStep->_x));
          _try(VecAssemblyEnd(_currentStep->_x));
          
          _try(VecAssemblyBegin(_currentStep->_xdot));
          _try(VecAssemblyEnd(_currentStep->_xdot));

          _try(VecAssemblyBegin(_currentStep->_xddot));
          _try(VecAssemblyEnd(_currentStep->_xddot));
          _valueNotAssembedInitialBC = false;
        }
      }
      void _assembleMassMatrixIfNeeded() const
      {
      #if defined(HAVE_MPI)
        if(this->_comm == PETSC_COMM_WORLD) {
          if(Msg::GetCommSize() > 1) {
            int value = _valuesNotAssembledMass ? 1 : 0;
            int sumValue = 0;
            MPI_Allreduce((void *)&value, (void *)&sumValue, 1, MPI_INT, MPI_SUM,
                          this->_comm);
            if((sumValue > 0) && (sumValue < Msg::GetCommSize())) {
              _valuesNotAssembledMass = 1;
            }
          }
        }
      #endif
        if(_valuesNotAssembledMass) {
          _try(MatAssemblyBegin(_M, MAT_FINAL_ASSEMBLY));
          _try(MatAssemblyEnd(_M, MAT_FINAL_ASSEMBLY));
          _valuesNotAssembledMass = false;
        }
      }


  public:

    implicitHulbertChungPetsc(double alpham, double alphaf, double beta, double gamma):
          _alphaf(alphaf), _alpham(alpham),_beta(beta),_gamma(gamma),
          linearSystemPETSc<scalar>(),_timeStep(0.), _nbRows(0),
          _nbNoMassKeys(0), _noMassKeys(), _previousValueNoMassKeys(), _zeros(),
          _numIteration(0),_flagb(false),_initialStep(NULL),
          _reducedRigidContactForceMPI(NULL), _indexFirstRigidContactMPI(0),  
          _valuesNotAssembledMass(false),_valueNotAssembedInitialBC(false){
      OneMinusAlphaf = 1. - _alphaf;
      AlphafMinusOne = -1.* OneMinusAlphaf;
      OneMinusAlpham = 1. - _alpham;
      MinusAlphaf = -1.*_alphaf;
      AlphamMinusOne = -1.* OneMinusAlpham;
      MinusAlpham = -1.*_alpham;

      _currentStep = new implicitStepPetsc<scalar>();
      _previousStep = new implicitStepPetsc<scalar>();
    }
    virtual ~implicitHulbertChungPetsc(){
      delete _currentStep;
      delete _previousStep;
      if (_initialStep) delete _initialStep;
    }

    implicitStepPetsc<scalar>& getState(const IPStateBase::whichState ws) {
      if (ws == IPStateBase::initial){
        if (_initialStep == NULL){
          _initialStep = new implicitStepPetsc<scalar>();
          _initialStep->allocate(_nbRows,&_currentStep->_x);
        }
        return *_initialStep;
      }
      else if (ws == IPStateBase::previous) return *_previousStep;
      else if (ws == IPStateBase::current) return *_currentStep;
      else
        Msg::Error("this state does not exist");
        
      static implicitStepPetsc<scalar> a;
      return a;
    }
    
    virtual void restart()
    {
      if(!this->_isAllocated) Msg::Error("The implicitHulbertChungPetsc scheme must be saved before being loaded or saved");
      double tstep = _timeStep;
      restartManager::restart(tstep);
      setTimeStep(tstep);
      int tnrow = _nbRows;
      restartManager::restart(tnrow);
      if(tnrow != _nbRows) Msg::Error("Cannot load a petsc explicit scheme with a different size from the saved one!");
      restartManager::restart(_currentStep);
      restartManager::restart(_previousStep);

      #if defined(HAVE_MPI)
      restartManager::restart(_indexFirstRigidContactMPI);
      scalar* rrcfm = getArrayIndexPreviousRightHandSidePlusMPI(_indexFirstRigidContactMPI);
      restartManager::restart(_reducedRigidContactForceMPI,_nbRows-_indexFirstRigidContactMPI);
      #endif // HAVE_MPI
    };

    virtual void copy(const IPStateBase::whichState source,
                        const IPStateBase::whichState destination){
      implicitStepPetsc<scalar>& src = this->getState(source);
      implicitStepPetsc<scalar>& dst = this->getState(destination);
      dst = src;
    }
    
    virtual void setNoMassDofKeys(const std::vector<int>& dofKeys){
      // Copy the vector dof keys and set _nbZeroPredictorDofs and _zeros accordingly
      _nbNoMassKeys = dofKeys.size();
      if (_nbNoMassKeys > 0){
        _noMassKeys = dofKeys;
        _previousValueNoMassKeys.resize(_nbNoMassKeys,0);
        _zeros.resize(_nbNoMassKeys,0);
      }
    }
    

    void nextStep(){
			_assembleVecsInitialBCIfNeeded();
       (*_previousStep) = (*_currentStep);
      _numIteration = 0;
      if (_nbNoMassKeys){
        _try(VecGetValues(getState(IPStateBase::current)._x, _nbNoMassKeys,&_noMassKeys[0],&_previousValueNoMassKeys[0]));
      }
    }

    virtual void clear(){
      if (this->isAllocated()){
        linearSystemPETSc<scalar>::clear();
        _currentStep->clear();
        _previousStep->clear();
        _nbRows=0;

        if (_initialStep) _initialStep->clear();
        if(_reducedRigidContactForceMPI != NULL)
        {
          delete[] _reducedRigidContactForceMPI;
          _reducedRigidContactForceMPI = NULL;
        }

        _try(MatDestroy(&_M));
      }
    }
    
    virtual void zeroMatrix(const nonLinearSystemBase::whichMatrix wm) {
      if (wm == nonLinearSystemBase::mass)
      {
          #if defined(HAVE_MPI)
          if(this->_comm == PETSC_COMM_WORLD) {
            if(Msg::GetCommSize() > 1) {
              int value = this->_entriesPreAllocated ? 1 : 0;
              int sumValue = 0;
              MPI_Allreduce((void *)&value, (void *)&sumValue, 1, MPI_INT, MPI_SUM,
                            this->_comm);
              if((sumValue >= 0) && (sumValue < Msg::GetCommSize()) &&
                 !this->_entriesPreAllocated) {
                linearSystemPETSc<scalar>::preAllocateEntries();
              }
            }
          }
        #endif
        if(this->_isAllocated && this->_entriesPreAllocated) {
          _assembleMassMatrixIfNeeded();
          _try(MatZeroEntries(_M));
        }
      }
      else if (wm == nonLinearSystemBase::stiff)
      {
        linearSystemPETSc<scalar>::zeroMatrix();
      }
      else
      {
        Msg::Error("matrix does not exist");
      }
    }

    // Or compute directly the time step here ??
    virtual void setTimeStep(const double dt){
      _timeStep = dt;
      // update variables which depends on _timeStep
      Deltat2halfMinusbeta = _timeStep*_timeStep*(0.5- _beta);
      DeltatOneMinusGamma = _timeStep*(1.-_gamma);
      GammaDivByBetaDeltat = _gamma/(_beta*_timeStep);
      OneMinusAlphamDivByBetaDeltat2 = (1. - _alpham)/(_beta*_timeStep*_timeStep);
      OneDivByBetaDeltat2 = 1./(_beta*_timeStep*_timeStep);
    }

    virtual double getTimeStep()const{return _timeStep;}
    virtual void allocate(int nbRows){
      clear();
      linearSystemPETSc<scalar>::allocate(nbRows);
      _currentStep->allocate(nbRows,&this->_x);
      _previousStep->allocate(nbRows,&this->_x);
      _nbRows=nbRows;

      _try(MatCreate(this->_comm, &_M));
  		_try(MatSetSizes(_M, nbRows, nbRows,PETSC_DETERMINE, PETSC_DETERMINE));
      _try(MatSetFromOptions(_M)); 
    }
   virtual void preAllocateEntries(){
      if (!this->_entriesPreAllocated)
      {
				std::vector<int> nByRowDiag(this->_localSize), nByRowOffDiag(this->_localSize);
				if(this->_sparsity.getNbRows() == 0) {
					PetscInt prealloc = 100;
					PetscBool set;
					PetscOptionsGetInt(PETSC_NULL, "-petsc_prealloc", &prealloc, &set);
					prealloc = std::min(prealloc, this->_localSize);
					nByRowDiag.resize(0);
					nByRowDiag.resize(this->_localSize, prealloc);
				}
				else {
					for(int i = 0; i < this->_localSize; i++) {
						int n;
						const int *r = this->_sparsity.getRow(i, n);
						for(int j = 0; j < n; j++) {
							if(r[j] >= this->_localRowStart && r[j] < this->_localRowEnd)
							  nByRowDiag[i]++;
							else
							  nByRowOffDiag[i]++;
						}
					}
				}
				// MatXAIJSetPreallocation is not available in petsc < 3.3
				#if defined (HAVE_MPI)					
				int commSize = 1;
				MPI_Comm_size(this->_comm, &commSize);
				if(commSize == 1) 
				#endif //HAVE_MPI					
				{
					_try(MatSeqAIJSetPreallocation(_M, 0, &nByRowDiag[0]));
				}
        #if defined(HAVE_MPI)
				else {
					_try(MatMPIAIJSetPreallocation(_M, 0, &nByRowDiag[0], 0, &nByRowOffDiag[0]));
				}
        #endif //HAVE_MPI

				#if ((PETSC_VERSION_MAJOR == 3) && (PETSC_VERSION_MINOR >= 3))
				// Preallocation routines automatically set now
				// MAT_NEW_NONZERO_ALLOCATION_ERR, which causes a problem when the mask of the
				// matrix changes.  We must disable the error generation and allow new
				// allocation (if needed)
				_try(MatSetOption(_M, MAT_NEW_NONZERO_ALLOCATION_ERR, PETSC_FALSE));
				#endif
        linearSystemPETSc<scalar>::preAllocateEntries();
      }
    }

    virtual void addToRightHandSide(int row, const scalar &val)
    {
      PetscInt i = row;
      PetscScalar s = val;
      _try(VecSetValues(_currentStep->_Fext, 1, &i, &s, ADD_VALUES));
    }
    virtual void addToRightHandSidePlus(int row, const scalar &val)
    {
      PetscInt i = row;
      PetscScalar s = val;
      _try(VecSetValues(_currentStep->_Fext, 1, &i, &s, ADD_VALUES));
    }
    virtual void addToRightHandSideMinus(int row, const scalar &val)
    {
      PetscInt i = row;
      PetscScalar s = val;
      _try(VecSetValues(_currentStep->_Fint, 1, &i, &s, ADD_VALUES));
    }

    virtual void getFromRightHandSide(int row, scalar &val) const
    {
      #if defined(PETSC_USE_COMPLEX)
      PetscScalar *tmp;
      _try(VecGetArray(_currentStep->_Fext, &tmp));
      PetscScalar s = tmp[row];
      _try(VecRestoreArray(_currentStep->_Fext, &tmp));
      // FIXME specialize this routine
      val = s.real();
      #else
      _try(VecGetValues(_currentStep->_Fext, 1, &row, &val));
      #endif
    }

    virtual void getFromRightHandSidePlus(int row, scalar &val) const
    {
      #if defined(PETSC_USE_COMPLEX)
      PetscScalar *tmp;
      _try(VecGetArray(_currentStep->_Fext, &tmp));
      PetscScalar s = tmp[row];
      _try(VecRestoreArray(_currentStep->_Fext, &tmp));
      // FIXME specialize this routine
      val = s.real();
      #else
      VecGetValues(_currentStep->_Fext, 1, &row, &val);
      #endif
    }

    virtual void getFromRightHandSideMinus(int row, scalar &val) const
    {
      #if defined(PETSC_USE_COMPLEX)
      PetscScalar *tmp;
      _try(VecGetArray(_currentStep->_Fint, &tmp));
      PetscScalar s = tmp[row];
      _try(VecRestoreArray(_currentStep->_Fint, &tmp));
      // FIXME specialize this routine
      val = s.real();
      #else
      VecGetValues(_currentStep->_Fint, 1, &row, &val);
      #endif
    }

    virtual double normInfRightHandSide() const {
      // compute b
      if(!_flagb){
			 	_try(VecAssemblyBegin(this->_currentStep->_Fext));
    		_try(VecAssemblyEnd(this->_currentStep->_Fext));
				_try(VecAssemblyBegin(this->_currentStep->_Fint));
    		_try(VecAssemblyEnd(this->_currentStep->_Fint));

       Vec tempxdd;
       _try(VecDuplicate(this->_currentStep->_xddot,&tempxdd));
       _try(VecAXPY(tempxdd,AlphamMinusOne,this->_currentStep->_xddot));
       _try(VecAXPY(tempxdd,MinusAlpham,this->_previousStep->_xddot));

       _try(VecAXPY(this->_b,OneMinusAlphaf,this->_currentStep->_Fext));
       _try(VecAXPY(this->_b,AlphafMinusOne,this->_currentStep->_Fint));
       _try(VecAXPY(this->_b,this->_alphaf,this->_previousStep->_Fext));
       _try(VecAXPY(this->_b,MinusAlphaf,this->_previousStep->_Fint));

				_assembleMassMatrixIfNeeded();
       _try(MatMultAdd(_M,tempxdd,this->_b,this->_b));
        _flagb = true;

        _try(VecDestroy(&tempxdd));

      }
      double norm = linearSystemPETSc<scalar>::normInfRightHandSide();
      return norm;
    };

    virtual void addToMatrix(int row, int col, const scalar &val, const nonLinearSystemBase::whichMatrix wm)
    {
      if(wm == nonLinearSystemBase::stiff){
        linearSystemPETSc<scalar>::addToMatrix(row,col,val);
      }
      else if( wm == nonLinearSystemBase::mass){
        if (!this->_entriesPreAllocated){
          Msg::Error("system must be preallocated");
        }
        PetscInt i = row;
        PetscInt j = col;
        PetscScalar s = val;
        _try(MatSetValues(_M, 1, &i, 1, &j, &s, ADD_VALUES));
        _valuesNotAssembledMass = true;
      }
      else{
       Msg::Error("stiff and mass are the only possible matrix choice");
      }

    };

     virtual void getFromSolution(int row, scalar &val) const
    {
     #if defined(PETSC_USE_COMPLEX)
      PetscScalar *tmp;
      _try(VecGetArray(_currentStep->_x, &tmp));
      PetscScalar s = tmp[row];
      _try(VecRestoreArray(_currentStep->_x, &tmp));
      val = s.real();
     #else
      VecGetValues(_currentStep->_x, 1, &row, &val);
     #endif // PETSC_USE_COMPLEX
    }

    virtual void getFromSolution(int row, scalar &val, const nonLinearBoundaryCondition::whichCondition wv) const
    {
      switch(wv){
       case nonLinearBoundaryCondition::position:
         this->getFromSolution(row,val);
       break;
       case nonLinearBoundaryCondition::velocity:
        #if defined(PETSC_USE_COMPLEX)
        PetscScalar *tmp;
        _try(VecGetArray(_currentStep->_xdot, &tmp));
        PetscScalar s = tmp[row];
        _try(VecRestoreArray(_currentStep->_xdot, &tmp));
        val = s.real();
        #else
        VecGetValues(_currentStep->_xdot, 1, &row, &val);
        #endif
       break;
       case nonLinearBoundaryCondition::acceleration:
        #if defined(PETSC_USE_COMPLEX)
        PetscScalar *tmp;
        _try(VecGetArray(_currentStep->_xddot, &tmp));
        PetscScalar s = tmp[row];
        _try(VecRestoreArray(_currentStep->_xddot, &tmp));
        val = s.real();
        #else
        VecGetValues(_currentStep->_xddot, 1, &row, &val);
        #endif
       break;
       default:
         Msg::Error("Impossible to get value from solution. Only possible choices position, velocity, acceleration ");
      }

    }

    virtual double norm0Inf() const{
      PetscReal norFext;
      PetscReal norFint;
      _try(VecNorm(this->_currentStep->_Fext, NORM_INFINITY, &norFext));
      _try(VecNorm(this->_currentStep->_Fint, NORM_INFINITY, &norFint));

      PetscReal norFextPrev, norFintPrev;
      _try(VecNorm(this->_previousStep->_Fext, NORM_INFINITY, &norFextPrev));
      _try(VecNorm(this->_previousStep->_Fint, NORM_INFINITY, &norFintPrev));


      Vec tmp1, tmp2;
      _try(VecDuplicate(this->_x,&tmp1));
      _try(VecDuplicate(this->_x,&tmp2));


      _try(VecAXPY(tmp1,_alpham,_previousStep->_xddot));
      _try(VecAXPY(tmp1,OneMinusAlpham,_currentStep->_xddot));
			
			_assembleMassMatrixIfNeeded();
      _try(MatMult(_M,tmp1,tmp2));
      PetscReal norAcc;
      _try(VecNorm(tmp2,NORM_INFINITY,&norAcc));

      _try(VecDestroy(&tmp1));
      _try(VecDestroy(&tmp2));

      double norm = norAcc+fabs(OneMinusAlphaf)*(norFext+norFint)+ fabs(_alphaf)*(norFextPrev+norFintPrev);

      return norm;
    }

    virtual void zeroRightHandSide()
    {
      linearSystemPETSc<scalar>::zeroRightHandSide();
      if (this->isAllocated()) {
        _try(VecAssemblyBegin(_currentStep->_Fext));
        _try(VecAssemblyEnd(_currentStep->_Fext));
        _try(VecZeroEntries(_currentStep->_Fext));
        _try(VecAssemblyBegin(_currentStep->_Fint));
        _try(VecAssemblyEnd(_currentStep->_Fint));
        _try(VecZeroEntries(_currentStep->_Fint));
      }
      _flagb = false;
    }


    virtual int systemSolve(){
      if (_numIteration == 0){
        _try(VecCopy(this->_previousStep->_x,this->_currentStep->_x));
        _try(VecAXPY(this->_currentStep->_x,_timeStep,this->_previousStep->_xdot));
        _try(VecAXPY(this->_currentStep->_x,Deltat2halfMinusbeta,this->_previousStep->_xddot));

        _try(VecCopy(this->_previousStep->_xdot,this->_currentStep->_xdot));
        _try(VecAXPY(this->_currentStep->_xdot,DeltatOneMinusGamma,this->_previousStep->_xddot));

        _try(VecAssemblyBegin(this->_currentStep->_xddot));
        _try(VecAssemblyEnd(this->_currentStep->_xddot));
        _try(VecZeroEntries(this->_currentStep->_xddot));


        if(_nbNoMassKeys > 0)
        {
          _try(VecSetValues(this->_currentStep->_x,_nbNoMassKeys,&_noMassKeys[0],&_previousValueNoMassKeys[0],INSERT_VALUES));
          _try(VecSetValues(this->_currentStep->_xdot , _nbNoMassKeys, &_noMassKeys[0], &_zeros[0], INSERT_VALUES));
          _try(VecSetValues(this->_currentStep->_xddot, _nbNoMassKeys, &_noMassKeys[0], &_zeros[0], INSERT_VALUES));
					_try(VecAssemblyBegin(this->_currentStep->_x));
          _try(VecAssemblyEnd(this->_currentStep->_x));
					_try(VecAssemblyBegin(this->_currentStep->_xdot));
          _try(VecAssemblyEnd(this->_currentStep->_xdot));
					_try(VecAssemblyBegin(this->_currentStep->_xddot));
          _try(VecAssemblyEnd(this->_currentStep->_xddot));
        }
      }
      else{
        this->_assembleMatrixIfNeeded();
        this->_assembleMassMatrixIfNeeded();

        _try(MatScale(this->_a,OneMinusAlphaf));
        _try(MatAXPY(this->_a,OneMinusAlphamDivByBetaDeltat2,_M,DIFFERENT_NONZERO_PATTERN));
        if(!_flagb){
         Vec tempxdd;
         _try(VecDuplicate(this->_currentStep->_xddot,&tempxdd));
          
         _try(VecAXPY(tempxdd,AlphamMinusOne,this->_currentStep->_xddot));
         _try(VecAXPY(tempxdd,MinusAlpham,this->_previousStep->_xddot));

         // set the value of contact nodes in MPI (after reduction of force)
         #if defined(HAVE_MPI)
         if((Msg::GetCommSize()) > 1 and (_indexFirstRigidContactMPI > 0))
         {
           int k=0;
           for(int j=_indexFirstRigidContactMPI; j<_nbRows; j++)
           {
             // the value of forces are on the previous step !!
             PetscInt i = j;
             PetscScalar s = _reducedRigidContactForceMPI[k];
             _try(VecSetValues(_previousStep->_Fext, 1, &i, &s, INSERT_VALUES));
             k++;
           }
					 _try(VecAssemblyBegin(_previousStep->_Fext));
        	 _try(VecAssemblyEnd(_previousStep->_Fext));
         }
         #endif // HAVE_MPI
				
					_try(VecAssemblyBegin(this->_currentStep->_Fext));
        	_try(VecAssemblyEnd(this->_currentStep->_Fext));
					_try(VecAssemblyBegin(this->_currentStep->_Fint));
        	_try(VecAssemblyEnd(this->_currentStep->_Fint));

         _try(VecAXPY(this->_b,OneMinusAlphaf,this->_currentStep->_Fext));
         _try(VecAXPY(this->_b,AlphafMinusOne,this->_currentStep->_Fint));

         if (_alphaf != 0.){
          _try(VecAXPY(this->_b,this->_alphaf,this->_previousStep->_Fext));
          _try(VecAXPY(this->_b,MinusAlphaf,this->_previousStep->_Fint));
         }
         _try(MatMultAdd(_M,tempxdd,this->_b,this->_b));
          _flagb = true;

          _try(VecDestroy(&tempxdd));
        }

        linearSystemPETSc<scalar>::systemSolve();
        _try(VecAXPY(this->_currentStep->_x,1.,this->_x));
        _try(VecAXPY(this->_currentStep->_xddot,OneDivByBetaDeltat2,this->_x));
        _try(VecAXPY(this->_currentStep->_xdot,GammaDivByBetaDeltat,this->_x));
      }
      _numIteration++;
      return 1;
    };
    // Specific functions (To put initial conditions)
    // set on current step a next step operation is necessary after the prescribtion of initial value step0->step1
    virtual void setInitialCondition(int row, scalar val, const typename nonLinearBoundaryCondition::whichCondition wc){ //CANNOT PASS VAL BY REF WHY ??
      PetscInt i = row;
			_valueNotAssembedInitialBC = true;
      switch(wc){
       case nonLinearBoundaryCondition::position:
        _try(VecSetValues(_currentStep->_x, 1, &i, &val, INSERT_VALUES));
        break;
       case nonLinearBoundaryCondition::velocity:
        _try(VecSetValues(_currentStep->_xdot, 1, &i, &val, INSERT_VALUES));
        break;
       case nonLinearBoundaryCondition::acceleration:
        _try(VecSetValues(_currentStep->_xddot, 1 , &i, &val, INSERT_VALUES));
        break;
       default:
        Msg::Warning("Invalid initial conditions");
      }
    }

    virtual void resetUnknownsToPreviousTimeStep()
    {
      (*_currentStep) = (*_previousStep);
      _numIteration = 0;
    }

    virtual void getFromMatrix(int row, int col, scalar &val,const nonLinearSystemBase::whichMatrix wm) const
    {
      if(wm!=nonLinearSystemBase::stiff)
      {
        Msg::Error("A QS system has only a stiffness matrix. So cannot get a value in another matrix");
        val=0;
        return;
      }
      this->linearSystemPETSc<scalar>::getFromMatrix(row,col,val);
    }

    virtual double getKineticEnergy(const int syssize) const {
      Vec xpt, Mxpt;
      _try(VecDuplicate(_currentStep->_xdot,&xpt));
      _try(VecDuplicate(_currentStep->_xdot,&Mxpt));

      #if defined(HAVE_MPI)
      if (Msg::GetCommSize()>1){
        _try(VecAssemblyBegin(_currentStep->_xdot));
        _try(VecAssemblyEnd(_currentStep->_xdot));
      }
      #endif //HAVE_MPI

      _try(VecCopy(_currentStep->_xdot,xpt));
      PetscScalar s = 0.;
      for (int i= 0; i< this->_globalSize; i++){
        if (i< this->_localRowStart or i >this->_localRowEnd + syssize - _nbRows){
          _try(VecSetValues(xpt,1,&i,&s,INSERT_VALUES));
        }
      }
			_try(VecAssemblyBegin(xpt));
      _try(VecAssemblyEnd(xpt));
      
			_try(MatMult(_M,xpt,Mxpt));
      PetscScalar ener;
      _try(VecDot(Mxpt,xpt,&ener));

      _try(VecDestroy(&xpt));
      _try(VecDestroy(&Mxpt));
      return 0.5*ener;


    };
    virtual double getExternalWork(const int syssize) const {
      double energ = 0;
      for (int i=this->_localRowStart; i<this->_localRowEnd + syssize - _nbRows; i++){
        PetscScalar val, valprev, fext, fextprev;
        _try(VecGetValues(_currentStep->_x,1,&i,&val));
        _try(VecGetValues(_previousStep->_x,1,&i,&valprev));

        _try(VecGetValues(_currentStep->_Fext,1,&i,&fext));
        _try(VecGetValues(_previousStep->_Fext,1,&i,&fextprev));

        double ext = (fext+ fextprev)*(val- valprev);
        energ += ext;
      }
      energ *= 0.5;
      return energ;
    };

    virtual Mat& getMassMatrix(){return _M;};

    #if defined(HAVE_MPI)
    virtual scalar* getArrayIndexPreviousRightHandSidePlusMPI(const int index)
    {
      // create vector comm if needed
      if(index != _indexFirstRigidContactMPI)
      {
        _indexFirstRigidContactMPI = index;
        if(_reducedRigidContactForceMPI != NULL){
          delete[] _reducedRigidContactForceMPI;
          _reducedRigidContactForceMPI = NULL;
        }
      }
      if(!_reducedRigidContactForceMPI)
        _reducedRigidContactForceMPI = new scalar[_nbRows - index];
      return _reducedRigidContactForceMPI;
    };
    #endif // HAVE_MPI
    virtual void setDynamicRelaxation(const bool dynrel) {};

};

#else
template<class scalar>
class implicitHulbertChungPetsc : public nonLinearSystem<scalar>,
                                  public linearSystemPETSc<scalar>{
  public:
    implicitHulbertChungPetsc(){Msg::Error("Petsc is not available");};
    virtual ~implicitHulbertChungPetsc(){}
    virtual void getFromRightHandSidePlus(int row, scalar &val) const {};
    virtual void getFromRightHandSideMinus(int row, scalar &val) const {};
    virtual void getFromRightHandSide(int row, scalar &val) const{};
    virtual void addToRightHandSidePlus(int row, const scalar &val) {};
    virtual void addToRightHandSideMinus(int row, const scalar &val) {};
    virtual double norm0Inf() const {return 0.;};
    virtual void resetUnknownsToPreviousTimeStep() {};
    virtual void nextStep() {};
    virtual void setTimeStep(const double dt) {};
    virtual double getTimeStep() const {return 0.;};
    virtual double getExternalWork(const int syssize) const {return 0.;};
    virtual double getKineticEnergy(const int syssize) const {return 0.;};
    virtual void zeroMatrix(const nonLinearSystemBase::whichMatrix wm){};
    virtual void addToMatrix(int row, int col, const scalar &val, const nonLinearSystemBase::whichMatrix wm){};
    virtual void setInitialCondition(int row, scalar val, const typename nonLinearBoundaryCondition::whichCondition wc) {};
    virtual void setDynamicRelaxation(const bool dynrel) {};
    virtual void getFromSolution(int row, scalar &val, const nonLinearBoundaryCondition::whichCondition wv) const {};
    virtual void getFromMatrix(int row, int col, scalar &val,const nonLinearSystemBase::whichMatrix wm) const {};
    #if defined(HAVE_MPI)
    virtual double* getArrayIndexPreviousRightHandSidePlusMPI(const int index) {return NULL;};
    #endif // HAVE_MPI
};
#endif //HAVE_PETSC

#endif // IMPLICITEHULBERTCHUNGPETSC_H_
