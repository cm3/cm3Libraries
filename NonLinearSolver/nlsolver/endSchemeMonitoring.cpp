//
// C++ Interface: end scheme when some criterion
//
// Author:  <Van Dung Nguyen>, (C) 2017
//
// Copyright: See COPYING file that comes with this distribution
//
//

#include "endSchemeMonitoring.h"
#include "nonLinearMechSolver.h"
#include "STensorOperations.h"
#include "staticDofManager.h"
#include "ipField.h"
#include "GPFilter.h"

void EndSchemeMonitoringBase::nextStep(){
  _solverEndedByMonitoring = false;
  _reduceTimeStep = false;
};
void EndSchemeMonitoringBase::resetToPreviousStep(){
  _solverEndedByMonitoring = false;
  _reduceTimeStep = false;
};

void EndSchemeMonitoringWithZeroLoad::nextStep(){
  EndSchemeMonitoringBase::nextStep();
  _Tprev = _Tcur;
  _maxValPrev = _maxValCur;
};
void EndSchemeMonitoringWithZeroLoad::resetToPreviousStep(){
  EndSchemeMonitoringBase::resetToPreviousStep();
  _Tcur = _Tprev;
  _maxValCur = _maxValPrev;
};

EndSchemeMonitoringWithZeroLoad::EndSchemeMonitoringWithZeroLoad(const double tol, const double n1, const double n2, const double n3):
  EndSchemeMonitoringBase(),_tol(tol),_normal(n1,n2,n3),_maxValCur(0.),_maxValPrev(0.),_Tprev(0.),_Tcur(0.){};

void EndSchemeMonitoringWithZeroLoad::checkEnd(nonLinearMechSolver* solver){
  const std::vector<partDomain*>&  allDom = *(solver->getDomainVector());
  const IPField* ipf = solver->getIPField();
  static STensor3 Phomo;
  STensorOperation::zero(Phomo);
  double vsolid = 0.;

  for (int i=0; i< allDom.size(); i++){
    partDomain* dom = allDom[i];
    vsolid += dom->computeVolumeDomain(ipf);
    dom->computeAverageStress(ipf,Phomo);
  }
  if (vsolid > 0){
    Phomo *= (1./vsolid);
  }
  else{
    Msg::Error("zero volume");
  }

  STensorOperation::multSTensor3SVector3(Phomo,_normal,_Tcur);
  double val = _Tcur.norm();

  bool peakReached = true;
  if (_maxValCur < fabs(val)) {
    _maxValCur = val;
    peakReached = false;
  }
  Msg::Info("check fails: previous val = %e, val = %e, maxVal = %e val/maxVal = %e required < %e",_Tprev.norm(),val,_maxValCur,fabs(val/_maxValCur),_tol);
  if (peakReached and _maxValCur > 0){
    if (fabs(val/_maxValCur) < _tol or dot(_Tprev,_Tcur) < 0.)
      endSolverByMonitoring(true);
    else
      endSolverByMonitoring(false);
  }
  else
    endSolverByMonitoring(false);
};


void EndSchemeMonitoringWithPeakLoad::nextStep(){
  EndSchemeMonitoringBase::nextStep();
  _Tprev = _Tcur;
  _maxValPrev = _maxValCur;
};
void EndSchemeMonitoringWithPeakLoad::resetToPreviousStep(){
  EndSchemeMonitoringBase::resetToPreviousStep();
  _Tcur = _Tprev;
  _maxValCur = _maxValPrev;
};


EndSchemeMonitoringWithPeakLoad::EndSchemeMonitoringWithPeakLoad(const double tol, const double n1, const double n2, const double n3):
  EndSchemeMonitoringBase(),_tol(tol),_normal(n1,n2,n3),_maxValCur(0.),_maxValPrev(0.),_Tprev(0.),_Tcur(0.){};

void EndSchemeMonitoringWithPeakLoad::checkEnd(nonLinearMechSolver* solver){
  const std::vector<partDomain*>&  allDom = *(solver->getDomainVector());
  const IPField* ipf = solver->getIPField();
  static STensor3 Phomo;
  STensorOperation::zero(Phomo);
  double vsolid = 0.;

  for (int i=0; i< allDom.size(); i++){
    partDomain* dom = allDom[i];
    vsolid += dom->computeVolumeDomain(ipf);
    dom->computeAverageStress(ipf,Phomo);
  }
  if (vsolid > 0){
    Phomo *= (1./vsolid);
  }
  else{
    Msg::Error("zero volume");
  }

  STensorOperation::multSTensor3SVector3(Phomo,_normal,_Tcur);
  double val = _Tcur.norm();

  bool peakReached = true;
  if (_maxValCur < fabs(val)) {
    _maxValCur = fabs(val);
    peakReached = false;
  }

  Msg::Info("check peak: previous val = %e val = %e, maxVal = %e (fabs(fabs(val)-_maxVal)/_maxVal) = %e",_Tprev.norm(), val,_maxValCur,(fabs(fabs(val)-_maxValCur)/_maxValCur));

  if (peakReached and _maxValCur > 0.){
    if ((fabs(fabs(val)-_maxValCur)/_maxValCur) < _tol) {
      endSolverByMonitoring(true);
    }
    else{
      endSolverByMonitoring(false);
      reduceTimeStep(true);
    }
  }
  else{
    endSolverByMonitoring(false);
    reduceTimeStep(false);
  }

};

EndSchemeMonitoringWithZeroAverageValue::EndSchemeMonitoringWithZeroAverageValue(const double tol, const int ipVal):
  EndSchemeMonitoringBase(),_tol(tol),_ipVal(ipVal),_maxValCur(0.),_maxValPrev(0.),_curVal(0.),_previousVal(0.){};

void EndSchemeMonitoringWithZeroAverageValue::nextStep(){
  EndSchemeMonitoringBase::nextStep();
  _previousVal = _curVal;
  _maxValPrev = _maxValCur;
};
void EndSchemeMonitoringWithZeroAverageValue::resetToPreviousStep(){
  EndSchemeMonitoringBase::resetToPreviousStep();
  _curVal = _previousVal;
  _maxValCur = _maxValPrev;
};


void EndSchemeMonitoringWithZeroAverageValue::checkEnd(nonLinearMechSolver* solver){
  solver->getHomogenizedPropertyByVolumeIntegral(_ipVal,_curVal);

  #if defined(HAVE_MPI)
	if (Msg::GetCommSize() > 1){
    double valTemp = _curVal;
		MPI_Allreduce(&valTemp,&_curVal,1,MPI_DOUBLE,MPI_SUM,MPI_COMM_WORLD);
	}
	#endif // HAVE_MPI

  bool peakReached = true;
  if (_maxValCur < fabs(_curVal)) {
    _maxValCur = fabs(_curVal);
    peakReached = false;
  };
  Msg::Info("check fails previous val = %e, val = %e, maxVal = %e val/maxVal = %e required < %e",_previousVal,_curVal,_maxValCur,fabs(_curVal/_maxValCur),_tol);
  if (peakReached and _maxValCur> 0.){
    if (fabs(_curVal/_maxValCur) < _tol or _previousVal*_curVal < 0.)
      endSolverByMonitoring(true);
    else
      endSolverByMonitoring(false);
  }
  else
    endSolverByMonitoring(false);
};

EndSchemeMonitoringWithZeroForceOnPhysical::EndSchemeMonitoringWithZeroForceOnPhysical(const double tol,std::string loc, const int numphys,const int c):
			EndSchemeMonitoringBase(),_tol(tol),_maxValCur(0.),_maxValPrev(0.),onWhat(loc),physical(numphys),comp(c),_forceArchive(NULL),_previousValue(0.),
			_currentValue(0.){};

EndSchemeMonitoringWithZeroForceOnPhysical::~EndSchemeMonitoringWithZeroForceOnPhysical(){
  if (_forceArchive)  {
    delete _forceArchive; _forceArchive=NULL;
  }
};

void EndSchemeMonitoringWithZeroForceOnPhysical::nextStep(){
  EndSchemeMonitoringBase::nextStep();
  _previousValue = _currentValue;
  _maxValPrev = _maxValCur;
};
void EndSchemeMonitoringWithZeroForceOnPhysical::resetToPreviousStep(){
  EndSchemeMonitoringBase::resetToPreviousStep();
  _currentValue = _previousValue;
  _maxValCur = _maxValPrev;
};


void EndSchemeMonitoringWithZeroForceOnPhysical::checkEnd(nonLinearMechSolver* solver){
  if (_forceArchive == NULL){
    static std::string node("Node");
    static std::string edge("Edge");
    static std::string face("Face");
    static std::string volume("Volume");
    int dim=0;
    if(onWhat == node )
      dim = 0;
    else if(onWhat == edge)
      dim = 1;
    else if(onWhat == face)
      dim = 2;
    else if(onWhat == volume)
      dim = 3;
    _forceArchive = new archiveForce(physical,dim,comp,1);
    solver->initArchiveReactionForceOnFixedPhysical(*_forceArchive);
  }


  solver->getDofManager()->getReactionForceOnFixedPhysical(*_forceArchive);
  _currentValue =  _forceArchive->fval;

  #if defined(HAVE_MPI)
	if (Msg::GetCommSize() > 1){
    double valTemp = _currentValue;
		MPI_Allreduce(&valTemp,&_currentValue,1,MPI_DOUBLE,MPI_SUM,MPI_COMM_WORLD);
	}
	#endif // HAVE_MPI

  bool peakReached = true;
  if (_maxValCur < fabs(_currentValue)) {
    _maxValCur= fabs(_currentValue);
    peakReached = false;
  }
  Msg::Info("check fails previous val = %e, val = %e, maxVal = %e val/maxVal = %e required < %e",_previousValue,_currentValue,_maxValCur,fabs(_currentValue/_maxValCur),_tol);
  if (peakReached and _maxValCur > 0.){
    if (fabs(_currentValue/_maxValCur) < _tol or (_previousValue*_currentValue < 0))
      endSolverByMonitoring(true);
    else{
      endSolverByMonitoring(false);
    }
  }
  else {
    endSolverByMonitoring(false);
  }
};


EndSchemeMonitoringWithUpperBoundAverageValueIP::EndSchemeMonitoringWithUpperBoundAverageValueIP(const double boundValue, int ipVal):
			EndSchemeMonitoringBase(),_boundValue(boundValue),_ipValue(ipVal){};

EndSchemeMonitoringWithUpperBoundAverageValueIP::~EndSchemeMonitoringWithUpperBoundAverageValueIP(){
};

void EndSchemeMonitoringWithUpperBoundAverageValueIP::nextStep(){
  EndSchemeMonitoringBase::nextStep();
};
void EndSchemeMonitoringWithUpperBoundAverageValueIP::resetToPreviousStep(){
  EndSchemeMonitoringBase::resetToPreviousStep();
};


void EndSchemeMonitoringWithUpperBoundAverageValueIP::checkEnd(nonLinearMechSolver* solver)
{
  double value;
  GPFilterTrivial filter;
  solver->getHomogenizedPropertyByVolumeIntegral(_ipValue, value, &filter);
  if (value > _boundValue)
  {
    endSolverByMonitoring(true);
  }
  else
  {
    endSolverByMonitoring(false);
  }
};

EndSchemeMonitoringWithPeakForceOnPhysical::EndSchemeMonitoringWithPeakForceOnPhysical(const double tol,std::string loc, const int numphys,const int c):
			EndSchemeMonitoringBase(),_tol(tol),_maxValCur(0.),_maxValPrev(0.),onWhat(loc),physical(numphys),comp(c),_forceArchive(NULL),_previousValue(0.),
			_currentValue(0.){};

EndSchemeMonitoringWithPeakForceOnPhysical::~EndSchemeMonitoringWithPeakForceOnPhysical(){
  if (_forceArchive)  {
    delete _forceArchive; _forceArchive=NULL;
  }
};

void EndSchemeMonitoringWithPeakForceOnPhysical::nextStep(){
  EndSchemeMonitoringBase::nextStep();
  _previousValue = _currentValue;
  _maxValPrev = _maxValCur;
};
void EndSchemeMonitoringWithPeakForceOnPhysical::resetToPreviousStep(){
  EndSchemeMonitoringBase::resetToPreviousStep();
  _currentValue = _previousValue;
  _maxValCur = _maxValPrev;
};



void EndSchemeMonitoringWithPeakForceOnPhysical::checkEnd(nonLinearMechSolver* solver){
  if (_forceArchive == NULL){
    static std::string node("Node");
    static std::string edge("Edge");
    static std::string face("Face");
    static std::string volume("Volume");
    int dim=0;
    if(onWhat == node )
      dim = 0;
    else if(onWhat == edge)
      dim = 1;
    else if(onWhat == face)
      dim = 2;
    else if(onWhat == volume)
      dim = 3;
    _forceArchive = new archiveForce(physical,dim,comp,1);
    solver->initArchiveReactionForceOnFixedPhysical(*_forceArchive);
  }

 
  solver->getDofManager()->getReactionForceOnFixedPhysical(*_forceArchive);

  _currentValue =  _forceArchive->fval;
  #if defined(HAVE_MPI)
	if (Msg::GetCommSize() > 1){
    double valTemp = _currentValue;
		MPI_Allreduce(&valTemp,&_currentValue,1,MPI_DOUBLE,MPI_SUM,MPI_COMM_WORLD);
	}
	#endif // HAVE_MPI

  bool peakReached = true;
  if (_maxValCur< fabs(_currentValue)) {
    _maxValCur = fabs(_currentValue);
    peakReached = false;
  };
  Msg::Info("check peak: previous val = %e val = %e, maxVal = %e (fabs(fabs(val)-_maxVal)/_maxVal) = %e required tol = %e",_previousValue, _currentValue,_maxValCur,(fabs(fabs(_currentValue)-_maxValCur)/_maxValCur),_tol);

  if (peakReached and _maxValCur > 0.){
    if ((fabs(fabs(_currentValue)-_maxValCur)/_maxValCur) < _tol) {
      endSolverByMonitoring(true);
			Msg::Info("solver will be terminated");
    }
    else{
      endSolverByMonitoring(false);
      reduceTimeStep(true);
    }
  }
  else{
    endSolverByMonitoring(false);
    reduceTimeStep(false);
  }
};


#include "ipNonLocalPorosity.h"
#include "ipFiniteStrain.h"

EndSchemeMonitoringWithNumberOfCoalescenceIPs::EndSchemeMonitoringWithNumberOfCoalescenceIPs( const int maxNumber):
    EndSchemeMonitoringBase(),_maxNumLarger(maxNumber){};

void EndSchemeMonitoringWithNumberOfCoalescenceIPs::nextStep(){
  EndSchemeMonitoringBase::nextStep();
};
void EndSchemeMonitoringWithNumberOfCoalescenceIPs::resetToPreviousStep(){
  EndSchemeMonitoringBase::resetToPreviousStep();
};

void EndSchemeMonitoringWithNumberOfCoalescenceIPs::checkEnd(nonLinearMechSolver* solver){
  IPField* ipf = solver->getIPField();
  int numCoales= 0;
  for(AllIPState::ipstateContainer::iterator it=ipf->getAips()->getAIPSContainer()->begin(); it!=ipf->getAips()->getAIPSContainer()->end();++it){
    std::vector<IPStateBase*> *vips = &((*it).second);
    for(int i=0;i<vips->size();i++){
      IPVariable *ipv= (*vips)[i]->getState(IPStateBase::current);
      const IPVariable *ipvprev= (*vips)[i]->getState(IPStateBase::previous);
      ipFiniteStrain* ipvfinit = dynamic_cast<ipFiniteStrain*>(ipv);
      const IPNonLocalPorosity* ipvPoros = dynamic_cast<const IPNonLocalPorosity*>(ipvfinit->getInternalData());
      if (ipvPoros != NULL){
        const ipFiniteStrain* ipvfinitPrev = static_cast<const ipFiniteStrain*>(ipvprev);
        const IPNonLocalPorosity* ipvPorosPrev = static_cast<const IPNonLocalPorosity*>(ipvfinitPrev->getInternalData());

        const IPCoalescence* ipvCoales = &(ipvPoros->getConstRefToIPCoalescence());
        const IPCoalescence* ipvCoalesPrev = &(ipvPorosPrev->getConstRefToIPCoalescence());

        if (!ipvCoalesPrev->getCoalescenceOnsetFlag() and ipvCoales->getCoalescenceOnsetFlag()){
            // new IP only
            numCoales ++;
        }
      }
      #ifdef _DEBUG
      else{
        Msg::Warning("IPPorous is not internal variable of ipv");
      }
      #endif // _DEBUG
    }
  }

  int totalPass = numCoales;
  #if defined(HAVE_MPI)
	if (Msg::GetCommSize() > 1){
		MPI_Allreduce(&numCoales,&totalPass,1,MPI_INT,MPI_SUM,MPI_COMM_WORLD);
	}
	#endif // HAVE_MPI

  Msg::Info("num IP %d in coalescence",totalPass);

  if (totalPass> _maxNumLarger)
  {
    endSolverByMonitoring(true);
    Msg::Info("solver will be terminated");
  }
  else{
    endSolverByMonitoring(false);
  }
};

EndSchemeMonitoringWithNumberOfCoalescenceIPsNotGreaterThan::EndSchemeMonitoringWithNumberOfCoalescenceIPsNotGreaterThan( const int maxNumber):
    EndSchemeMonitoringBase(),_maxNumLarger(maxNumber){};

void EndSchemeMonitoringWithNumberOfCoalescenceIPsNotGreaterThan::nextStep(){
  EndSchemeMonitoringBase::nextStep();
};
void EndSchemeMonitoringWithNumberOfCoalescenceIPsNotGreaterThan::resetToPreviousStep(){
  EndSchemeMonitoringBase::resetToPreviousStep();
};

void EndSchemeMonitoringWithNumberOfCoalescenceIPsNotGreaterThan::checkEnd(nonLinearMechSolver* solver){
  IPField* ipf = solver->getIPField();
  int numCoales= 0;
  for(AllIPState::ipstateContainer::iterator it=ipf->getAips()->getAIPSContainer()->begin(); it!=ipf->getAips()->getAIPSContainer()->end();++it){
    std::vector<IPStateBase*> *vips = &((*it).second);
    for(int i=0;i<vips->size();i++){
      IPVariable *ipv= (*vips)[i]->getState(IPStateBase::current);
      const IPVariable *ipvprev= (*vips)[i]->getState(IPStateBase::previous);
      ipFiniteStrain* ipvfinit = dynamic_cast<ipFiniteStrain*>(ipv);
      const IPNonLocalPorosity* ipvPoros = dynamic_cast<const IPNonLocalPorosity*>(ipvfinit->getInternalData());
      if (ipvPoros != NULL){
        const ipFiniteStrain* ipvfinitPrev = static_cast<const ipFiniteStrain*>(ipvprev);
        const IPNonLocalPorosity* ipvPorosPrev = static_cast<const IPNonLocalPorosity*>(ipvfinitPrev->getInternalData());

        const IPCoalescence* ipvCoales = &(ipvPoros->getConstRefToIPCoalescence());
        const IPCoalescence* ipvCoalesPrev = &(ipvPorosPrev->getConstRefToIPCoalescence());

        if (!ipvCoalesPrev->getCoalescenceOnsetFlag() and ipvCoales->getCoalescenceOnsetFlag()){
            // new IP only
            numCoales ++;
        }
      }
      #ifdef _DEBUG
      else{
        Msg::Warning("IPPorous is not internal variable of ipv");
      }
      #endif // _DEBUG
    }
  }

  int totalPass = numCoales;
  #if defined(HAVE_MPI)
	if (Msg::GetCommSize() > 1){
		MPI_Allreduce(&numCoales,&totalPass,1,MPI_INT,MPI_SUM,MPI_COMM_WORLD);
	}
	#endif // HAVE_MPI

  Msg::Info("num IP %d in coalescence",totalPass);
  
  if (totalPass> _maxNumLarger)
  {
    reduceTimeStep(true);
    Msg::Info("time step is reduced due to num = %d coalescence IPs larger than %d" ,totalPass,_maxNumLarger);
    endSolverByMonitoring(false);
  }
  else if (totalPass>0)
  {
    reduceTimeStep(false);
    endSolverByMonitoring(true);
    Msg::Info("solver will be terminated");
  }
  else
  {
    reduceTimeStep(false);
    endSolverByMonitoring(false);
  }
};

ReduceTimeStepWithNumberOfCoalescenceIPs::ReduceTimeStepWithNumberOfCoalescenceIPs( const int maxNumber):
    EndSchemeMonitoringBase(),_maxNumLarger(maxNumber){};

void ReduceTimeStepWithNumberOfCoalescenceIPs::nextStep(){
  EndSchemeMonitoringBase::nextStep();
};
void ReduceTimeStepWithNumberOfCoalescenceIPs::resetToPreviousStep(){
  EndSchemeMonitoringBase::resetToPreviousStep();
};

void ReduceTimeStepWithNumberOfCoalescenceIPs::checkEnd(nonLinearMechSolver* solver){
  IPField* ipf = solver->getIPField();
  int numCoales= 0;
  for(AllIPState::ipstateContainer::iterator it=ipf->getAips()->getAIPSContainer()->begin(); it!=ipf->getAips()->getAIPSContainer()->end();++it){
    std::vector<IPStateBase*> *vips = &((*it).second);
    for(int i=0;i<vips->size();i++){
      IPVariable *ipv= (*vips)[i]->getState(IPStateBase::current);
      const IPVariable *ipvprev= (*vips)[i]->getState(IPStateBase::previous);
      ipFiniteStrain* ipvfinit = dynamic_cast<ipFiniteStrain*>(ipv);
      const IPNonLocalPorosity* ipvPoros = dynamic_cast<const IPNonLocalPorosity*>(ipvfinit->getInternalData());
      if (ipvPoros != NULL){
        const ipFiniteStrain* ipvfinitPrev = static_cast<const ipFiniteStrain*>(ipvprev);
        const IPNonLocalPorosity* ipvPorosPrev = static_cast<const IPNonLocalPorosity*>(ipvfinitPrev->getInternalData());

        const IPCoalescence* ipvCoales = &(ipvPoros->getConstRefToIPCoalescence());
        const IPCoalescence* ipvCoalesPrev = &(ipvPorosPrev->getConstRefToIPCoalescence());

        if (!ipvCoalesPrev->getCoalescenceOnsetFlag() and ipvCoales->getCoalescenceOnsetFlag()){
            // new IP only
            numCoales ++;
        }
      }
      #ifdef _DEBUG
      else{
        Msg::Warning("IPPorous is not internal variable of ipv");
      }
      #endif // _DEBUG
    }
  }

  int totalPass = numCoales;
  #if defined(HAVE_MPI)
	if (Msg::GetCommSize() > 1){
		MPI_Allreduce(&numCoales,&totalPass,1,MPI_INT,MPI_SUM,MPI_COMM_WORLD);
	}
	#endif // HAVE_MPI

  Msg::Info("num IP %d in coalescence",totalPass);

  if (totalPass> _maxNumLarger){
    reduceTimeStep(true);
    Msg::Info("time step is reduced due to num = %d coalescence IPs larger than %d" ,totalPass,_maxNumLarger);
  }
  else{
    reduceTimeStep(false);
  }
};

// ========== ReduceTimeStepWithLargeCoalescenceOffset
ReduceTimeStepWithLargeCoalescenceOffset::ReduceTimeStepWithLargeCoalescenceOffset(const double maximalValue, const int maxNumber):
    EndSchemeMonitoringBase(),_maxVal(maximalValue),_maxNumLarger(maxNumber){};

void ReduceTimeStepWithLargeCoalescenceOffset::nextStep(){
  EndSchemeMonitoringBase::nextStep();
};
void ReduceTimeStepWithLargeCoalescenceOffset::resetToPreviousStep(){
  EndSchemeMonitoringBase::resetToPreviousStep();
};

void ReduceTimeStepWithLargeCoalescenceOffset::checkEnd(nonLinearMechSolver* solver){
  IPField* ipf = solver->getIPField();
  double maxVal = 1.;
  int numPass= 0;
  for(AllIPState::ipstateContainer::iterator it=ipf->getAips()->getAIPSContainer()->begin(); it!=ipf->getAips()->getAIPSContainer()->end();++it){
    std::vector<IPStateBase*> *vips = &((*it).second);
    for(int i=0;i<vips->size();i++){
      IPVariable *ipv= (*vips)[i]->getState(IPStateBase::current);
      const IPVariable *ipvprev= (*vips)[i]->getState(IPStateBase::previous);
      ipFiniteStrain* ipvfinit = dynamic_cast<ipFiniteStrain*>(ipv);
      const IPNonLocalPorosity* ipvPoros = dynamic_cast<const IPNonLocalPorosity*>(ipvfinit->getInternalData());
      if (ipvPoros != NULL){
        const ipFiniteStrain* ipvfinitPrev = static_cast<const ipFiniteStrain*>(ipvprev);
        const IPNonLocalPorosity* ipvPorosPrev = static_cast<const IPNonLocalPorosity*>(ipvfinitPrev->getInternalData());

        const IPCoalescence* ipvCoales = &(ipvPoros->getConstRefToIPCoalescence());
        const IPCoalescence* ipvCoalesPrev = &(ipvPorosPrev->getConstRefToIPCoalescence());
        if (!ipvCoalesPrev->getCoalescenceOnsetFlag() and ipvCoales->getCoalescenceOnsetFlag()){
          if (maxVal < ipvCoales->getCrackOffsetOnCft()){
            maxVal = ipvCoales->getCrackOffsetOnCft();
          }
          if (ipvCoales->getCrackOffsetOnCft() > _maxVal){
            numPass ++;
          }
        }
      }
      #ifdef _DEBUG
      else{
        Msg::Warning("IPPorous is not internal variable of ipv");
      }
      #endif // _DEBUG
    }
  }

  int totalPass = numPass;
  double maximalVal = maxVal;
  #if defined(HAVE_MPI)
	if (Msg::GetCommSize() > 1){
		MPI_Allreduce(&numPass,&totalPass,1,MPI_INT,MPI_SUM,MPI_COMM_WORLD);
		MPI_Allreduce(&maxVal,&maximalVal,1,MPI_DOUBLE,MPI_MAX,MPI_COMM_WORLD);
	}
	#endif // HAVE_MPI

  Msg::Info("maximal offset = %e num IP exceeded %d",maximalVal,totalPass);

  if (totalPass> _maxNumLarger){
    reduceTimeStep(true);
    Msg::Info("time step is reduced due to large coalescence offset maxVal = %e, num = %d larger than %e" ,maximalVal,totalPass,_maxVal);
  }
  else{
    reduceTimeStep(false);
  }
};



// ======== ReduceTimeStepWithLargeYieldOffset
ReduceTimeStepWithLargeYieldOffset::ReduceTimeStepWithLargeYieldOffset(const double maximalValue, const int maxNumber):
    EndSchemeMonitoringBase(),_maxVal(maximalValue),_maxNumLarger(maxNumber){};

void ReduceTimeStepWithLargeYieldOffset::nextStep(){
  EndSchemeMonitoringBase::nextStep();
};
void ReduceTimeStepWithLargeYieldOffset::resetToPreviousStep(){
  EndSchemeMonitoringBase::resetToPreviousStep();
};

void ReduceTimeStepWithLargeYieldOffset::checkEnd(nonLinearMechSolver* solver)
{
  // Check if the current maximal unused yield surface is not over the threshold
  double currentMax = 0.;
  int numberLarger = 0;
  
  // Loop on all Coalescence IPVs
  IPField* ipf = solver->getIPField();
  for(AllIPState::ipstateContainer::iterator it=ipf->getAips()->getAIPSContainer()->begin(); it!=ipf->getAips()->getAIPSContainer()->end();++it){
    std::vector<IPStateBase*> *vips = &((*it).second);
    for(int i=0;i<vips->size();i++){
      IPVariable *ipv= (*vips)[i]->getState(IPStateBase::current);
      ipFiniteStrain* ipvfinit = dynamic_cast<ipFiniteStrain*>(ipv);
      const IPNonLocalPorosity* ipvPorous = dynamic_cast<const IPNonLocalPorosity*>(ipvfinit->getInternalData());
      if (ipvPorous != NULL){
        const IPCoalescence* ipvCoales = &(ipvPorous->getConstRefToIPCoalescence());
        // Save the max value and get the number of too large values
        if (currentMax < ipvCoales->getYieldOffset()){
          currentMax = ipvCoales->getYieldOffset();
        }
        if (ipvCoales->getYieldOffset() > _maxVal){
            numberLarger++;
        }
      }
      #ifdef _DEBUG
      else{
        Msg::Error("ReduceTimeStepWithLargeYieldOffset::checkEnd: IPPorous is not internal variable of ipv");
      }
      #endif // _DEBUG
    }
  }

  int totalLarger = numberLarger;
  double maximalVal = currentMax;
  
  #if defined(HAVE_MPI)
  if (Msg::GetCommSize() > 1){
    MPI_Allreduce(&numberLarger,&totalLarger,1,MPI_INT,MPI_SUM,MPI_COMM_WORLD);
    MPI_Allreduce(&currentMax,&maximalVal,1,MPI_DOUBLE,MPI_MAX,MPI_COMM_WORLD);
  }
  #endif // HAVE_MPI

  Msg::Info("maximal offset = %e num IP exceeded %d",maximalVal,totalLarger);

  if (totalLarger > _maxNumLarger){
    reduceTimeStep(true);
    Msg::Info("time step is reduced due to large coalescence offset: maxVal= %e. %d IPs are larger than %e" ,maximalVal,totalLarger,_maxVal);
  }
  else{
    reduceTimeStep(false);
  }
};



ReduceTimeStepWithMaximalNumberOfErodedBulkElements::ReduceTimeStepWithMaximalNumberOfErodedBulkElements(const int maxnum, int maxReduction, bool keepInitialTimeStep): EndSchemeMonitoringBase(),
      _maxAllowedNewlyErodedElement(maxnum), _allowableNumberOfReductions(maxReduction), _lastTimeStep(-1.), _numberOfReductions(0), _solver(NULL),
      _keepInitialTimeStep(keepInitialTimeStep){};
void ReduceTimeStepWithMaximalNumberOfErodedBulkElements::checkEnd(nonLinearMechSolver* solver)
{
  _solver = solver;
  Msg::Info("number of reductions = %d", _numberOfReductions);
  if (_numberOfReductions >=_allowableNumberOfReductions)
  {
    Msg::Info("The allowable number of time step reductions (%d >= %d) is reached, go to next time step",_numberOfReductions,_allowableNumberOfReductions);
    // do not continue reducing time step if the number of reductions larger than the allowable value
    _numberOfReductions= 0;
    reduceTimeStep(false);
    return;
  }
  
  solver->checkElementErosion(IPStateBase::current);
  Msg::Info("The number of new eroded elements: bulk %d, interface %d",
              solver->getElementErosionFilter().getNumberOfNewBulkElementsEroded(),
              solver->getElementErosionFilter().getNumberOfNewInterfaceElementsEroded());
              
  int erodedBulkElements = solver->getElementErosionFilter().getNumberOfNewBulkElementsEroded();
  #if defined(HAVE_MPI)
  if (solver->getNumRanks() > 1)
  {
    int totalVal = 0;
    MPI_Allreduce(&erodedBulkElements, &totalVal, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
    erodedBulkElements = totalVal;
  }
  #endif //HAVE_MPI

  if(erodedBulkElements > _maxAllowedNewlyErodedElement){
    reduceTimeStep(true);
    Msg::Warning("Too many bulk elements are eroded (= %d): will go back to previous step", erodedBulkElements);
  }
  else{
    reduceTimeStep(false);
  }
};

void ReduceTimeStepWithMaximalNumberOfErodedBulkElements::nextStep(){
  EndSchemeMonitoringBase::nextStep();
  _numberOfReductions=0;
  if ((_lastTimeStep > 0 and _solver != NULL) and _keepInitialTimeStep)
  {
    _solver->getTimeManager()->setTimeStep(_lastTimeStep);
    _solver->getTimeManager()->resetNumberTimeStepReduction();
  }
};
void ReduceTimeStepWithMaximalNumberOfErodedBulkElements::resetToPreviousStep(){
  EndSchemeMonitoringBase::resetToPreviousStep();
  if ((_numberOfReductions == 0 and _solver != NULL) and _keepInitialTimeStep)
  {
    _lastTimeStep = _solver->getTimeManager()->getTimeStep();
  } 
  _numberOfReductions ++;
};





ReduceTimeStepWithMaximalNumberOfBrokenElements::ReduceTimeStepWithMaximalNumberOfBrokenElements(const int maxnum): EndSchemeMonitoringBase(),_maxAllowedNewlyBrokenElement(maxnum){};
void ReduceTimeStepWithMaximalNumberOfBrokenElements::checkEnd(nonLinearMechSolver* solver){
  IPField* ipf = solver->getIPField();
  if (ipf->getNumOfNewlyBrokenInterfaceElementInAllDomains() > 0){
    printf("at rank %d crack insertion = %d \n",Msg::GetCommRank(), ipf->getLocalNumOfNewlyBrokenInterfaceElementInAllDomains());
  }

  if(ipf->getNumOfNewlyBrokenInterfaceElementInAllDomains() > _maxAllowedNewlyBrokenElement){
    reduceTimeStep(true);
    Msg::Warning("Too many crack insertion detected at Ipvs (= %d): will go back to previous step", ipf->getNumOfNewlyBrokenInterfaceElementInAllDomains());
  }
  else{
    reduceTimeStep(false);
  }
};

void ReduceTimeStepWithMaximalNumberOfBrokenElements::nextStep(){
  EndSchemeMonitoringBase::nextStep();
};
void ReduceTimeStepWithMaximalNumberOfBrokenElements::resetToPreviousStep(){
  EndSchemeMonitoringBase::resetToPreviousStep();
};

EndSchemeWithDissipationOnElementFromActiveToInactive::EndSchemeWithDissipationOnElementFromActiveToInactive(const int elenum):EndSchemeMonitoringBase(),
  _elementNumber(elenum),vips(NULL){};
EndSchemeWithDissipationOnElementFromActiveToInactive::EndSchemeWithDissipationOnElementFromActiveToInactive(const EndSchemeWithDissipationOnElementFromActiveToInactive& src):
  EndSchemeMonitoringBase(src),_elementNumber(src._elementNumber),vips(NULL){}

void EndSchemeWithDissipationOnElementFromActiveToInactive::nextStep(){
  EndSchemeMonitoringBase::nextStep();
};
void EndSchemeWithDissipationOnElementFromActiveToInactive::resetToPreviousStep(){
  EndSchemeMonitoringBase::resetToPreviousStep();
};


void EndSchemeWithDissipationOnElementFromActiveToInactive::checkEnd(nonLinearMechSolver* solver){
  if (vips == NULL){
    vips = solver->getIPField()->getAips()->getIPstate(_elementNumber);
  }
  // if one change
  int oneChanged = 0;
  for (int i=0; i< (*vips).size(); i++){
    const IPStateBase* ips = (*vips)[i];
    const IPVariableMechanics* ipvprev = static_cast<const IPVariableMechanics*>(ips->getState(IPStateBase::previous));
    const IPVariableMechanics* ipvcur = static_cast<const IPVariableMechanics*>(ips->getState(IPStateBase::current));
    if (ipvprev->dissipationIsActive() and !ipvcur->dissipationIsActive()){
      oneChanged = 1;
      break;
    }
  }

  #if defined(HAVE_MPI)
  if (solver->getNumRanks() > 1){
    int maxOneChanged = 0;
    MPI_Allreduce(&oneChanged,&maxOneChanged,1,MPI_INT,MPI_MAX,MPI_COMM_WORLD);
    oneChanged = maxOneChanged;
  }
  #endif // HAVE_MPI

  if (oneChanged){
    Msg::Info("end scheme by state on element %d change from dissipation to no dissipation",_elementNumber);
    this->endSolverByMonitoring(true);
  }
  else{
    Msg::Info("there is no ip changed from dissipation to no dissipation");
    this->endSolverByMonitoring(false);
  }
};


MultipleEndSchemeMonitoring::MultipleEndSchemeMonitoring(const bool fl): EndSchemeMonitoringBase(),_allComp(0),_operation(fl){};
MultipleEndSchemeMonitoring::MultipleEndSchemeMonitoring(const MultipleEndSchemeMonitoring& src): EndSchemeMonitoringBase(src),_operation(src._operation){
  _allComp.clear();
  for (int i=0; i< src._allComp.size(); i++){
    _allComp.push_back(src._allComp[i]->clone());
  }
}
MultipleEndSchemeMonitoring::~MultipleEndSchemeMonitoring(){
  for (int i=0; i< _allComp.size(); i++){
    delete _allComp[i];
  }
  _allComp.clear();
}

void MultipleEndSchemeMonitoring::nextStep(){
  EndSchemeMonitoringBase::nextStep();
  for (int i=0; i< _allComp.size(); i++){
    _allComp[i]->nextStep();
  }
};
void MultipleEndSchemeMonitoring::resetToPreviousStep(){
  EndSchemeMonitoringBase::resetToPreviousStep();
  for (int i=0; i< _allComp.size(); i++){
    _allComp[i]->resetToPreviousStep();
  }
};

void MultipleEndSchemeMonitoring::setSchemeMonitoring(const EndSchemeMonitoringBase& c){
  _allComp.push_back(c.clone());
};

void MultipleEndSchemeMonitoring::checkEnd(nonLinearMechSolver* solver){
  // check all object
  for (int i=0; i< _allComp.size(); i++){
    _allComp[i]->checkEnd(solver);
  }
  if (_operation){
    // end if one object end
    this->endSolverByMonitoring(false);
    for (int i=0; i< _allComp.size(); i++){
      if (_allComp[i]->solverEndedByMornitoring()){
        this->endSolverByMonitoring(true);
        break;
      }
    }
    //
    this->reduceTimeStep(false);
    for (int i=0; i< _allComp.size(); i++){
      if (_allComp[i]->reduceTimeStepByMornitoring()){
        this->reduceTimeStep(true);
        break;
      }
    }
  }
  else{
    // end if all object end
    this->endSolverByMonitoring(true);
    for (int i=0; i< _allComp.size(); i++){
      if (!_allComp[i]->solverEndedByMornitoring()){
        this->endSolverByMonitoring(false);
        break;
      }
    }
    //
    this->reduceTimeStep(true);
    for (int i=0; i< _allComp.size(); i++){
      if (!_allComp[i]->reduceTimeStepByMornitoring()){
        this->reduceTimeStep(false);
        break;
      }
    }
  }
};
