//
//
// Description: cut finite model by a surface--> obtain a smaller cube from a larger FE model
//
// Author:  <Van Dung NGUYEN>, (C) 2013
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "cutMeshByBox.h"
#include "MTetrahedron.h"
#include "MTriangle.h"
#include "MQuadrangle.h"
#include "MLine.h"
#include "math.h"
#include "numericalFunctions.h"
#include <set>
#include "Generator.h"


MElement* create_Tetrahedron(MVertex* v0, MVertex* v1, MVertex* v2, MVertex* v3){
	SPoint3 pt = project(v0->point(),v1->point(),v2->point(),v3->point());

	SVector3 vec12(v1->point(),v2->point());
	SVector3 vec13(v1->point(),v3->point());

	SVector3 v = crossprod(vec12,vec13);
	SVector3 vject(pt,v0->point());

	if (dot(v,vject) <0) {
		return new MTetrahedron(v0,v1,v2,v3);
	}
	else if (dot(v,vject)> 0){
		return new MTetrahedron(v0,v1,v3,v2);
	}
	else{
		Msg::Error("4 point on a same surface");
		return NULL;
	}

};

face::face(double a, double b, double c, double d, double tol){
	_a = a;
	_b = b;
	_c = c;
	_d = d;
	_tolerance = tol;
};

bool face::inside(MElement* ele) const{
	int n = ele->getNumPrimaryVertices();
	for (int i=0; i<n; i++){
		if (outside(ele->getVertex(i))) return false;
	};
	return true;
};
bool face::boundary(MElement* ele) const{
	if (inside(ele) or outside(ele)) return false;
	else return true;
};
bool face::outside(MElement* ele) const{
	int n = ele->getNumPrimaryVertices();
	for (int i=0; i<n; i++){
		if (inside(ele->getVertex(i))) return false;
	};
	return true;

};

bool face::inside(MVertex* v) const{
	return inside(v->x(),v->y(),v->z());
};
bool face::boundary(MVertex* v) const{
	return boundary(v->x(),v->y(),v->z());
};
bool face::outside(MVertex* v) const{
	return outside(v->x(),v->y(),v->z());
};

bool face::inside(double x, double y, double z) const{
	if (_a*x+_b*y+_c*z+_d >_tolerance) return true;
	else return false;
};
bool face::boundary(double x, double y, double z) const{
	if (inside(x,y,z) or outside(x,y,z)) return false;
	else return true;
};
bool face::outside(double x, double y, double z) const{
	if (_a*x+_b*y+_c*z+_d<-1*_tolerance) return true;
	else return false;
};


bool face::onFace(MElement* ele, std::vector<MElement*>& face) const{
	std::vector<MVertex*> vv;
	ele->getVertices(vv);
	int numver = ele->getNumPrimaryVertices();

	int numInside = 0;
	int numOutside = 0;
	int numBoundary = 0;
	std::vector<MVertex*> inVer, outVer, boundVer;

	for (int i=0; i<vv.size(); i++){
		if (inside(vv[i])) {
			numInside++;
			inVer.push_back(vv[i]);
		}
		else if (outside(vv[i])) {
			numOutside++;
			outVer.push_back(vv[i]);
		}
		else {
			numBoundary++;
			boundVer.push_back(vv[i]);
		};
	};

	if (ele->getTypeForMSH() == MSH_TET_4){
		if (numBoundary == 3){
			MElement* f1 = new MTriangle(boundVer[0],boundVer[1],boundVer[2]);
			face.push_back(f1);
			return true;
		};
	}
	else {
		Msg::Error("This is not implemented for this mesh type");
	};

	return false;
};

void face::cut(MVertex* v1, MVertex* v2, MVertex*& v) const{
	double x1 = v1->x();
	double y1 = v1->y();
	double z1 = v1->z();
	double x2 = v2->x();
	double y2 = v2->y();
	double z2 = v2->z();

	double t = -1.*(_a*x1+_b*y1+_c*z1+_d)/(_a*(x2-x1)+ _b*(y2-y1)+_c*(z2-z1));

	double x = x1+t*(x2-x1);
	double y = y1+t*(y2-y1);
	double z = z1+t*(z2-z1);
	double tol = v1->distance(v2)*_tolerance;
	v = cutMeshByBox::createVertexFromCoordinates(x,y,z,tol);
	v->setIndex(v->getNum());
};

void face::cutElements(MElement* ele, std::vector<MElement*>& face, std::vector<MElement*>& volume) const{
	std::vector<MVertex*> vv;
	ele->getVertices(vv);
	int numver = ele->getNumPrimaryVertices();

	if (ele->getTypeForMSH() == MSH_TET_4){
		int numInside = 0;
		int numOutside = 0;
		int numBoundary = 0;
		std::vector<MVertex*> inVer, outVer, boundVer;

		for (int i=0; i<vv.size(); i++){
			if (inside(vv[i])) {
				numInside++;
				inVer.push_back(vv[i]);
			}
			else if (outside(vv[i])) {
				numOutside++;
				outVer.push_back(vv[i]);
			}
			else {
				numBoundary++;
				boundVer.push_back(vv[i]);
			};
		};

		if (numInside == 1 and numOutside==3){
			MVertex* c1 = NULL;
			MVertex* c2 = NULL;
			MVertex* c3 = NULL;

			this->cut(inVer[0],outVer[0],c1);
			this->cut(inVer[0],outVer[1],c2);
			this->cut(inVer[0],outVer[2],c3);

			MElement* v1 = create_Tetrahedron(inVer[0],c1,c2,c3);
			volume.push_back(v1);
		}
		else if (numInside == 2 and numOutside == 2){
			MVertex* c1 = NULL;
			MVertex* c2 = NULL;
			MVertex* c3 = NULL;
			MVertex* c4 = NULL;

			this->cut(inVer[0],outVer[0],c1);
			this->cut(inVer[1],outVer[0],c2);
			this->cut(inVer[1],outVer[1],c3);
			this->cut(inVer[0],outVer[1],c4);

			MElement* v1 = create_Tetrahedron(inVer[0],inVer[1],c2,c3);
			volume.push_back(v1);
			MElement* v2 = create_Tetrahedron(inVer[0],c2,c3,c4);
			volume.push_back(v2);
			MElement* v3 = create_Tetrahedron(inVer[0],c1,c2,c4);
			volume.push_back(v3);
		}
		else if (numInside == 3 and numOutside== 1){
			MVertex* c1 = NULL;
			MVertex* c2 = NULL;
			MVertex* c3 = NULL;

			this->cut(inVer[0],outVer[0],c1);
			this->cut(inVer[1],outVer[0],c2);
			this->cut(inVer[2],outVer[0],c3);

			MElement* v1 = create_Tetrahedron(inVer[0],c1,c2,c3);
			volume.push_back(v1);
			MElement* v2 = create_Tetrahedron(inVer[0],c2,c3,inVer[1]);
			volume.push_back(v2);
			MElement* v3 = create_Tetrahedron(inVer[0],inVer[1],inVer[2],c3);
			volume.push_back(v3);

		}
		else if (numInside == 1 and numOutside == 2){
			MVertex* c1 = NULL;
			MVertex* c2 = NULL;
			this->cut(inVer[0],outVer[0],c1);
			this->cut(inVer[0],outVer[1],c2);


			MElement* e1 = create_Tetrahedron(boundVer[0],inVer[0],c1,c2);
			volume.push_back(e1);
		}
		else if (numInside == 2 and numOutside == 1){
			MVertex* c1 = NULL;
			MVertex* c2 = NULL;
			this->cut(inVer[0],outVer[0],c1);
			this->cut(inVer[1],outVer[0],c2);

			MElement* e1 = create_Tetrahedron(boundVer[0],inVer[0],c1,c2);
			volume.push_back(e1);
			MElement* e2 = create_Tetrahedron(boundVer[0],inVer[0],inVer[1],c2);
			volume.push_back(e2);
		}
		else if (numInside == 1 and numOutside==1 ){
			MVertex* c1 = NULL;
			this->cut(inVer[0],outVer[0],c1);

			MElement* e1 = create_Tetrahedron(boundVer[0],boundVer[1],c1,inVer[0]);
			volume.push_back(e1);
		}
		else if(numInside ==1 and numBoundary == 3){
			volume.push_back(ele);
		}
		else if(numOutside ==0 and numBoundary <3){
			volume.push_back(ele);
		}
		else{
			Msg::Info("Inside = %d, OutSide = %d, Boundary = %d",numInside, numOutside, numBoundary);
			Msg::Error("Missing case");
		};

	}
	else{
		Msg::Error("This is not implemented for this mesh type");
	};


};


MVertex* cutMeshByBox::createVertexFromCoordinates(double x, double y, double z, double tol){
  for (std::set<MVertex*>::iterator it = currentVertices.begin(); it!= currentVertices.end(); it++){
    MVertex* v = *it;
    double l = sqrt((v->x() -x)*(v->x()-x)+ (v->y()-y)*(v->y()-y)+ (v->z()-z)*(v->z()-z));
    if (l < tol) return v;
  }
  MVertex* v =  new MVertex(x,y,z);
  currentVertices.insert(v);
  return v;
};

std::set<MVertex*> cutMeshByBox::currentVertices;

cutMeshByBox::cutMeshByBox(): _pModel(NULL),_g(NULL),_gVolume(NULL){}

void cutMeshByBox::addModel(std::string meshFileName){
	_pModel = new GModel();
  _pModel->readMSH(meshFileName.c_str());
  int dim = _pModel->getNumRegions() ? 3 : 2;
  _g = new elementGroup();

  std::map<int, std::vector<GEntity*> > groups[4];
  _pModel->getPhysicalGroups(groups);
  std::map<int, std::vector<GEntity*> > &entmap = groups[dim];
  for (std::map<int, std::vector<GEntity*> >::iterator it = entmap.begin(); it!= entmap.end(); it++){
    std::vector<GEntity*> &ent = it->second;
    for (unsigned int i = 0; i < ent.size(); i++){
      for (unsigned int j = 0; j < ent[i]->getNumMeshElements(); j++){
        MElement *e = ent[i]->getMeshElement(j);
        _g->insert(e);
      }
    }
  }
  for (elementGroup::vertexContainer::const_iterator itv = _g->vbegin(); itv != _g->vend(); itv++)
  {
    currentVertices.insert(itv->second);
  }
};

elementGroup* cutMeshByBox::cutFace(face* f, elementGroup* in){
	if (in == NULL) return NULL;
	elementGroup* out = new elementGroup();
	for (elementGroup::elementContainer::const_iterator it = in->begin(); it!= in->end(); it++){
		MElement* ele = it->second;
		if (f->outside(ele) == false){
			std::vector<MElement*> face, volume;
			f->cutElements(ele,face,volume);
			for (int ie = 0; ie<volume.size(); ie++){
				out->insert(volume[ie]);
			};
		}
	}
	return out;
};


void cutMeshByBox::cut(double a, double b, double c, double d){
	face* f = new face(a,b,c,d);
	_allCutFaces.push_back(f);
	if (_gVolume == NULL) _gVolume = _g;

	elementGroup* gtemp = _gVolume;
	Msg::Info("begin cutting");
	_gVolume = this->cutFace(f,gtemp);
	Msg::Info("end cutting");
	delete gtemp;
};


void cutMeshByBox::write_MSH2(std::string filename){
	std::vector<elementGroup*> bgr;
	for (int i=0; i< _allCutFaces.size(); i++){
		elementGroup* gr = new elementGroup();
		bgr.push_back(gr);
	};
	for (elementGroup::elementContainer::const_iterator it = _gVolume->begin(); it!= _gVolume->end(); it++){
		MElement* e = it->second;

		for (int i=0; i<_allCutFaces.size(); i++){
			std::vector<MElement*> face;
			if (_allCutFaces[i]->onFace(e,face)){
				for (int ef =0; ef <face.size(); ef++){
					bgr[i]->insert(face[ef]);
				};
			};
		};
	}

	int numnodes = _gVolume->vsize();
	int numel = _gVolume->size();

	for (int i=0; i< _allCutFaces.size(); i++)
		numel += bgr[i]->size();


	FILE* fp = fopen(filename.c_str(),"w");
 	fprintf(fp, "$MeshFormat\n2.2 0 8\n$EndMeshFormat\n");


	fprintf(fp, "$Nodes\n");
	fprintf(fp, "%d\n", numnodes);
	for (elementGroup::vertexContainer::const_iterator it = _gVolume->vbegin(); it!= _gVolume->vend(); it++){
		MVertex* v = it->second;
		int okf = fprintf(fp, "%ld %f %f %f\n",v->getNum(),v->x(),v->y(),v->z());
	}
	fprintf(fp, "$EndNodes\n");
 	fprintf(fp, "$Elements\n");
	fprintf(fp, "%d\n", numel);

	for (int i=0; i< _allCutFaces.size(); i++){
		for (elementGroup::elementContainer::const_iterator it = bgr[i]->begin(); it!= bgr[i]->end(); it++){
			MElement* ele = it->second;
			ele->writeMSH2(fp,2.2,0,0,i+1,i+1);
		};
	};

  for (elementGroup::elementContainer::const_iterator it = _gVolume->begin(); it!= _gVolume->end(); it++){
		MElement* ele = it->second;
		ele->writeMSH2(fp,2.2,0,0,1,11);
	};
	fprintf(fp, "$EndElements\n");
	fclose(fp);
};

bool levelSet::sameSide(double x1, double y1, double z1, double x2, double y2, double z2) const{
  if ((inside(x1,y1,z1) and inside(x2,y2,z2) )or 
      (outside(x1,y1,z1) and outside(x2,y2,z2))){
    return true;
  }
  else return false;
};

void levelSet::cut(double x1, double y1, double z1, double x2, double y2, double z2, double& x, double& y, double & z) const{
  double tol = 1e-6;
  double refL = sqrt((x2-x1)*(x2-x1)+(y2-y1)*(y2-y1)+ (z2-z1)*(z2-z1));
  bool ok = true;
  double xl = x1, yl = y1, zl = z1;
  double xr = x2, yr = y2, zr = z2;
  while (ok){
    x = 0.5*(xl+xr);
    y = 0.5*(yl+yr);
    z = 0.5*(zl+zr);
    if (boundary(x,y,z)){
      break;
    } 
    else{
      if (sameSide(x,y,z,xl,yl,zl)) {
        xl = x;
        yl = y;
        zl = z;
      }
      else{
        xr = x;
        yr = y; 
        zr = z;
      }
      
      double curL = sqrt((xr-xl)*(xr-xl) + (yr-yl)*(yr-yl)+(zr-zl)*(zr-zl));
      if (curL < refL*tol) break;
    }
  }
  //Msg::Info("cut [%f %f %f] [%f %f %f] resulting in [%f %f %f] tol = %f",x1,y1,z1,x2,y2,z2,x,y,z,tol);
};

sphericalLevelSet::sphericalLevelSet(const double p1, const double p2, const double p3, const double r, const bool inv):
      _center(p1,p2,p3),_radius(r),_inverse(inv){};
sphericalLevelSet::sphericalLevelSet(const sphericalLevelSet& src):_center(src._center), _radius(src._radius),_inverse(src._inverse){};
bool sphericalLevelSet::inside(double x, double y, double z) const{
  SPoint3 p(x,y,z);
  double distance = p.distance(_center);
  if (_inverse){
    if (distance > _radius){
      return true;
    }
    else return false;    
  }
  else{
    if (distance < _radius){
      return true;
    }
    else return false;    
  }
};
bool sphericalLevelSet::boundary(double x, double y, double z) const{
  SPoint3 p(x,y,z);
  double distance = p.distance(_center);
  if (distance == _radius){
    return true;
  }
  else return false;
};
bool sphericalLevelSet::outside(double x, double y, double z) const{
  SPoint3 p(x,y,z);
  double distance = p.distance(_center);
  if (_inverse){
    if (distance < _radius){
      return true;
    }
    else return false;    
  }
  else{
    if (distance > _radius){
      return true;
    }
    else return false;    
  }
};

bool cutMeshByLevelSet::inside(const levelSet* l, MElement* ele) const{
  // one element is inside if no vertex outside
  int n = ele->getNumPrimaryVertices();
	for (int i=0; i<n; i++){
    MVertex* v = ele->getVertex(i); 
		if (l->outside(v->x(),v->y(),v->z())) { return false;};
	};
	return true;
};
bool cutMeshByLevelSet::outside(const levelSet*l, MElement* ele) const{
  // one element is outside if no vertex inside
  int n = ele->getNumPrimaryVertices();
	for (int i=0; i<n; i++){
    MVertex* v = ele->getVertex(i); 
		if (l->inside(v->x(),v->y(),v->z())) { return false;};
	};
	return true;  
};

bool cutMeshByLevelSet::boundary(const levelSet*l, MElement* ele) const{
  // one element is boundary when some ver inside, some ver outside
  // need to cut
  int n = ele->getNumPrimaryVertices();
  int numIn = 0;
  int numOut = 0;
	for (int i=0; i<n; i++){
    MVertex* v = ele->getVertex(i); 
		if (l->inside(v->x(),v->y(),v->z())) { 
      numIn++;
    }
    else if (l->outside(v->x(),v->y(),v->z())) {
      numOut++;
    };
    if ((numIn >0) and (numOut>0)) return true;
	};
	return false; 
};

MVertex* cutMeshByLevelSet::createNewVerticesFromCoordinates(double x, double y, double z, double tol){
  for (std::set<MVertex*>::iterator it = allVertices.begin(); it != allVertices.end(); it++){
    MVertex* v = *it;
    double dx = v->x()-x;
    double dy = v->y()-y;
    double dz = v->z()-z;
    
    if (sqrt(dx*dx+dy*dy+dz*dz) < tol){
      /*
      v->x() = x;
      v->y() = y;
      v->z() = z;
       */
      return v;
    }
  }
  MVertex* vc = new MVertex(x,y,z);
  Msg::Info("create new v : %d %f %f %f",vc->getNum(),x,y,z);
  allVertices.insert(vc);
  return vc;
};

void cutMeshByLevelSet::cutElement(const levelSet* ls, MElement* ele, std::vector<MElement*>& cutElem){
  int part = ele->getPartition();
  if (ele->getTypeForMSH() == MSH_TRI_3){
    Msg::Info("cuting triangular element %d",ele->getNum());

    std::vector<MVertex*> vin, vout;
    for (int i=0; i< 3; i++){
      MVertex* v = ele->getVertex(i);
      if (ls->outside(v->x(),v->y(),v->z())){
        vout.push_back(v);
      }
      else{
        vin.push_back(v);
      }
    }
    
    if (vin.size() == 1){
      double x, y, z;
      ls->cut(vin[0]->x(),vin[0]->y(),vin[0]->z(),
              vout[0]->x(),vout[0]->y(),vout[0]->z(),x,y,z);
       MVertex* vc1 = createNewVerticesFromCoordinates(x,y,z);
       ls->cut(vin[0]->x(),vin[0]->y(),vin[0]->z(),
              vout[1]->x(),vout[1]->y(),vout[1]->z(),x,y,z);
      
      MVertex* vc2 = createNewVerticesFromCoordinates(x,y,z);
      MElement* ele  = new MTriangle(vin[0],vc1,vc2,0,part);
      cutElem.push_back(ele);
      
      ele = new MLine(vc1,vc2,0,part);
      intersectionElements.insert(ele);
    }
    else if (vin.size() == 2){
      double x, y, z;
      ls->cut(vin[0]->x(),vin[0]->y(),vin[0]->z(),
              vout[0]->x(),vout[0]->y(),vout[0]->z(),x,y,z);
       MVertex* vc1 = createNewVerticesFromCoordinates(x,y,z);
       ls->cut(vin[1]->x(),vin[1]->y(),vin[1]->z(),
              vout[0]->x(),vout[0]->y(),vout[0]->z(),x,y,z);
      
      MVertex* vc2 = createNewVerticesFromCoordinates(x,y,z);
      
      
      MElement* ele  = new MTriangle(vin[0],vc1,vc2,0,part);
      Msg::Info("create new ver: %d %d %d %d",ele->getNum(),vin[0]->getNum(),vc1->getNum(),vc2->getNum());
      cutElem.push_back(ele);
      ele  = new MTriangle(vin[0],vin[1],vc2,0,part);
      Msg::Info("create new ver: %d %d %d %d",ele->getNum(),vin[0]->getNum(),vin[1]->getNum(),vc2->getNum());
      cutElem.push_back(ele);
       
      
      /*MElement* ele = new MQuadrangle(vin[0],vc1,vc2,vin[1],0,part);
      cutElem.push_back(ele);
      */
      
      ele = new MLine(vc1,vc2,0,part);
      intersectionElements.insert(ele);
    }
    
  }
  else if (ele->getTypeForMSH() == MSH_LIN_2){
    Msg::Info("cuting line element %d",ele->getNum());
    std::vector<MVertex*> vin, vout;
    for (int i=0; i< 2; i++){
      MVertex* v = ele->getVertex(i);
      if (ls->outside(v->x(),v->y(),v->z())){
        vout.push_back(v);
      }
      else{
        vin.push_back(v);
      }
    }
    
    double x, y, z;
    ls->cut(vin[0]->x(),vin[0]->y(),vin[0]->z(),
            vout[0]->x(),vout[0]->y(),vout[0]->z(),x,y,z);
    
    MVertex* vc = createNewVerticesFromCoordinates(x,y,z);
    MElement* ele = new MLine(vin[0],vc,0,part);
    Msg::Info("create new ele: %d %d %d",ele->getNum(),vin[0]->getNum(),vc->getNum());
    cutElem.push_back(ele);
     
  }
  else{
    Msg::Error("cut mesh has not been defined for this kind of element");
  }
  
};

void cutMeshByLevelSet::write_MSH2(elementGroup* g,  const std::string filename){

	FILE* fp = fopen(filename.c_str(),"w");
 	fprintf(fp, "$MeshFormat\n2.2 0 8\n$EndMeshFormat\n");


	fprintf(fp, "$Nodes\n");
	fprintf(fp, "%ld\n", g->vsize());
	for (elementGroup::vertexContainer::const_iterator it = g->vbegin(); it!= g->vend(); it++){
		MVertex* v = it->second;
		fprintf(fp, "%ld %f %f %f\n",v->getNum(),v->x(),v->y(),v->z());
	}
	fprintf(fp, "$EndNodes\n");
 	fprintf(fp, "$Elements\n");
	fprintf(fp, "%ld\n", g->size());

	
  for (elementGroup::elementContainer::const_iterator it = g->begin(); it!= g->end(); it++){
		MElement* ele = it->second;
		ele->writeMSH2(fp,2.2,0,0,1,11);
	};
	fprintf(fp, "$EndElements\n");
	fclose(fp);
};


void cutMeshByLevelSet::cut(const std::string inputModel, const levelSet* gl, const std::string outputModel){
  allVertices.clear();
  
  GModel pModel;
  pModel.readMSH(inputModel.c_str());
  int dim = pModel.getNumRegions() ? 3 : 2;
  
  Msg::Info("done loading");
  
  elementGroup g;
  std::map<int, std::vector<GEntity*> > groups[4];
  pModel.getPhysicalGroups(groups);

  Msg::Info("start collecting elemens");
  std::map<int,elementGroup> allElem[dim+1];  // vec 0-> dim, map-> physical-> element
  
  for (int id = 0; id < dim+1; id ++){
    std::map<int,elementGroup>& grMap = allElem[id];
    std::map<int, std::vector<GEntity*> > &entmap = groups[id];
    
    for (std::map<int, std::vector<GEntity*> >::iterator it = entmap.begin(); it!= entmap.end(); it++){
      elementGroup& gr = grMap[it->first];
      std::vector<GEntity*> &ent = it->second;
      for (unsigned int i = 0; i < ent.size(); i++){
        for (unsigned int j = 0; j < ent[i]->getNumMeshElements(); j++){
          MElement *e = ent[i]->getMeshElement(j);
          gr.insert(e);
          for (int iv = 0; iv < e->getNumVertices(); iv++){
            allVertices.insert(e->getVertex(iv));
          }
        }
        
      }
    }
    
  }
  Msg::Info("done collecting elemens");
  
  elementGroup allBoundaryElements;

  std::set<MVertex*> allActiveVertices;
  int numEle = 0;
  std::map<int,elementGroup> allElemCut[dim+1];
  for (int id = 0; id < dim+1; id++){
    std::map<int,elementGroup>& grMap = allElem[id];
    std::map<int,elementGroup>& grMapCut = allElemCut[id];
    for (std::map<int, elementGroup >::iterator it = grMap.begin(); it!= grMap.end(); it++){
      elementGroup& gr = it->second;
      elementGroup& grCut = grMapCut[it->first];
      for (elementGroup::elementContainer::const_iterator ite = gr.begin(); ite != gr.end(); ite++){
        MElement* ele = ite->second;
        if (inside(gl, ele)){
          grCut.insert(ele);
          numEle ++;
          for (int iv = 0; iv < ele->getNumVertices(); iv++){
            allActiveVertices.insert(ele->getVertex(iv));
          }
        }
        else if (boundary(gl, ele)){
          allBoundaryElements.insert(ele);
          
          std::vector<MElement*> newEl;
          cutElement(gl, ele,newEl);
          for (int ie=0; ie< newEl.size(); ie++){
            grCut.insert(newEl[ie]);
            numEle ++;
            for (int iv = 0; iv < newEl[ie]->getNumVertices(); iv++){
              allActiveVertices.insert(newEl[ie]->getVertex(iv));
            }
          }
        }
      }
    };
  }
  
  
  FILE* fp = fopen(outputModel.c_str(),"w");
 	fprintf(fp, "$MeshFormat\n2.2 0 8\n$EndMeshFormat\n");


	fprintf(fp, "$Nodes\n");
	fprintf(fp, "%ld\n", allActiveVertices.size());
	for (std::set<MVertex*>::iterator it = allActiveVertices.begin(); it!= allActiveVertices.end(); it++){
		MVertex* v = *it;
		fprintf(fp, "%ld %f %f %f\n",v->getNum(),v->x(),v->y(),v->z());
	}
  
	fprintf(fp, "$EndNodes\n");
  
  numEle+= intersectionElements.size();
 	fprintf(fp, "$Elements\n");
	fprintf(fp, "%d\n", numEle);
  
  int maxphys = 0;
  for (int id = 0; id < dim+1; id++){
    std::map<int,elementGroup>& grMapCut = allElemCut[id];
    for (std::map<int, elementGroup >::iterator it = grMapCut.begin(); it!= grMapCut.end(); it++){
      elementGroup& grCut = it->second;
      int phys = it->first;
      if (abs(phys)> maxphys) maxphys = abs(phys);
      for (elementGroup::elementContainer::const_iterator ite = grCut.begin(); ite != grCut.end(); ite++){
        MElement* ele = ite->second;
        fprintf(fp,"%ld %d 2 %d %d",ele->getNum(),ele->getTypeForMSH(),phys,phys);
        for (int i=0; i< ele->getNumVertices(); i++){
          fprintf(fp," %ld", ele->getVertex(i)->getNum());
        }
        fprintf(fp,"\n");
      }
    };
  }
  
  for (std::set<MElement*>::iterator ite = intersectionElements.begin(); ite != intersectionElements.end(); ite++){
    MElement* ele = *ite;
    fprintf(fp,"%ld %d 2 %d %d",ele->getNum(),ele->getTypeForMSH(),maxphys+1,maxphys+1);
    for (int i=0; i< ele->getNumVertices(); i++){
      fprintf(fp," %ld", ele->getVertex(i)->getNum());
    }
    fprintf(fp,"\n");
  }
  maxphys++;

	fprintf(fp, "$EndElements\n");
  
	fclose(fp);
  
  
  std::string bFile = "boundary_"+inputModel;
  write_MSH2(&allBoundaryElements,bFile);
  
};
