//
// C++ Interface: partDomain
//
// Description: Interface class to used solver. Your term ha to be store in a domain
//
// Author:  <Gauthier BECKER>, (C) 2010
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef PARTDOMAIN_H_
#define PARTDOMAIN_H_
#ifndef SWIG
#include "mlaw.h"
#include "SVector3.h"
#include "MInterfaceElement.h"
#include "elementGroup.h"
#include "timeFunction.h"
#include "functionSpaceType.h"
#include "nonLinearBC.h"
#include "nlTerms.h"
#include "unknownField.h"
#include "InterfaceBuilder.h"
#include "ElementErosionFilter.h"
#include "interfaceQuadrature.h"
#include "mixedFunctionSpace.h"
#include "GPFilter.h"
//# include "nonLinearMechSolver.h"
// class for a domain (pure virtual class)
#endif
class dgPartDomain;
class partDomain
{
	#ifndef SWIG
  public :
    enum BodyForceModuliType{Present =0, Previous, Homogeneous};

    
  protected :
    int _tag; // tag for the dofManager
    int _phys;
    bool _fullDg; // To know which formulation Cg/Dg or FullDg is used for this part
    functionSpaceType::whichSpace _wsp;
    BilinearTermBase* btermBulk;
    BilinearTermBase* btermElasticBulk;
    BilinearTermBase* btermUndamagedElasticBulk;
    BilinearTermBase* massterm;
    LinearTermBase<double>* ltermBulk;
    QuadratureBase* integBulk;
    BilinearTermBase* btermTangent; // tangent estimation by homogenization
    BilinearTermBase* btermBodyForceTangent; // tangent estimation of second order homogenization body force
    BilinearTermBase* btermElasticTangent; // elastic tangent estimation by homogenization
    BilinearTermBase* btermUndamagedElasticTangent; // undamaged elastic tangent estimation by homogenization
    // for pathfollowing
    ScalarTermBase<double>* scalarTermPF; // scalar pathfollowing term
    LinearTermBase<double>* linearTermPF; // linear pathFollowing term
    LinearTermBase<double>* linearTermBodyForcePF; // linear pathFollowing term

    // for matrix by perturbation
    bool _hasBodyForceForHO; // // = true if we correct low order with body force for high order
    BodyForceModuliType  _BodyForceModuliType;
    
    bool _nlNonLocal; //true to solve non local equation in an explicit way
    
    STensor3 _rveGeoInertia;
    STensor33 _GM;    // keep GM for path folllowing
    STensor43 _Homogeneous_K;
    double _rveVolume;
    int _numberNonLocalVariable; // number of nonlocal variables
    int _numberConstitutiveExtraDofDiffusionVariable; // number of nonlocal variables
    int _numberConstitutiveCurlVariable; // number of variables for mixed FEM
    
    bool _bmbp;
    double _eps;
    // To know if law is already set
    bool setmaterial;

    #if defined(HAVE_MPI)
    int _rootRank;
    std::set<int> _otherRanks;
    std::set<int> _domainIPBulk;  // bulk integration points lie on this domain
    std::map<int,std::set<int> > _mapIPBulk; // map IP for bulk elements
    #endif //HAVE_MPI
    bool _distributedOtherRanks;
    bool _distributedOnRoot;
    elementGroup *g; // support for this field
    const nonLinearMechSolver* _macroSolver; // solver where this domain belongs to
    
  protected:
    virtual void setBulkMatrixByPerturbation(const int i, const double eps=1e-8);

  public:
    // add a group of element for prescribed Dirichlet BC of other partition in MPI
    elementGroup* _groupGhostMPI;
    elementGroup::elementContainer::const_iterator element_begin() const {return g->begin();}
    elementGroup::elementContainer::const_iterator element_end() const {return g->end();}
    elementGroup::vertexContainer::const_iterator vertex_begin() const {return g->vbegin();}
    elementGroup::vertexContainer::const_iterator vertex_end() const {return g->vend();}
    size_t elementGroupSize() const {return g->size();}
    void addPhysicalGroup(int dim,int phys,elementFilter& elemfil)
    {
        if(g==NULL) g = new elementGroup();
        g->addPhysical(dim,phys,elemfil);
    }
    virtual void clearElementGroup() {g->clearAll();}
    bool g_find(MElement* e) const {return g->find(e);}
    bool g_vfind(MVertex* v) const {return g->find(v);};
    const elementGroup* elements() const {return g;}
    #endif // SWIG

    virtual void setHasBodyForceForHO(bool bf) {_hasBodyForceForHO=bf;}
    virtual bool hasBodyForceForHO() const {return _hasBodyForceForHO;}
    void setBodyForceModuliTypeType(const int i){ _BodyForceModuliType = (BodyForceModuliType)i;}
    virtual BodyForceModuliType useWhichModuliForBF() const {return _BodyForceModuliType;}
    
    virtual void setNLExplicit(bool nle) {_nlNonLocal=nle;}
    virtual bool getNLExplicit() const { return _nlNonLocal;}
    
    void setSecondOrderKinematic(const STensor33& val);
    const STensor33& getSecondOrderKinematic() const;
    void setHomogeneousTangent(const STensor43& val);
    const STensor43& getHomogeneousTangent() const;
    void setRVEGeometricalInertia(const STensor3& val); 
    const STensor3&  getRVEGeometricalInertia() const;
    void setRVEVolume(const double val); 
    const double  getRVEVolume() const;
    
  public:
    // _distributedRootRanks is true if the microscopic solver are computed at
    // root ranks, false otherwise
    virtual bool isDistributedRootRank() const;
    virtual void distributeOnRootRank(const int flg);
    // _distributedOtherRanks is true if microscopic solver are computed at other
   // ranks, false otherwise
   // if _distributedOtherRanks = true and _distributedRootRanks = true mean the
   // microsopic BVPs are solved in both root and other ranks
   // while _distributedRootRanks = false means that the microscopic BVPs are
   // solved in other ranks only --> better for manage load balancing
   // Clearly, _distributedOtherRanks and _distributedRootRanks can not be
   // simultaneously false
    virtual bool isDistributedOtherRanks() const;
    virtual void setDistributedOtherRanks(const bool flag);
  
  #ifndef SWIG
    // Constructors
    partDomain(const int tag, const int phys,
             const int ws = 0,const bool fdg=false, int numNL=0, int numExtraDof=0, int numberCurlData=0, bool hasBodyForceForHO=false, int BFModuliType = 0);  
    partDomain(const partDomain &source);
    virtual ~partDomain();
    
    BilinearTermBase* getBilinearBulkTerm() const{return btermBulk;}
    BilinearTermBase* getBilinearElasticBulkTerm() const{return btermElasticBulk;}
    BilinearTermBase* getUndamagedBilinearElasticBulkTerm() const{return btermUndamagedElasticBulk;}
    BilinearTermBase* getBilinearMassTerm() const{return massterm;}
    BilinearTermBase* getBilinearTangentTerm() const{return btermTangent;}
    BilinearTermBase* getBilinearBodyForceTangentTerm() const{return btermBodyForceTangent;}
    BilinearTermBase* getBilinearElasticTangentTerm() const{return btermElasticTangent;}
    BilinearTermBase* getBilinearUndamagedElasticTangentTerm() const{return btermUndamagedElasticTangent;}
    LinearTermBase<double>* getLinearBulkTerm() const{return ltermBulk;}
    ScalarTermBase<double>* getScalarTermPFBulk() const {return scalarTermPF;};
    LinearTermBase<double>* getLinearTermPFBulk() const {return linearTermPF;};
    LinearTermBase<double>* getLinearTermPFBodyForce() const {return linearTermBodyForcePF;};
    

    void setMacroSolver(const nonLinearMechSolver* solver);
    const nonLinearMechSolver* getMacroSolver() const;

    QuadratureBase* getBulkGaussIntegrationRule() const{return integBulk;}
    int getNumNonLocalVariable() const {return _numberNonLocalVariable;};
    int getNumConstitutiveExtraDofDiffusionVariable() const {return _numberConstitutiveExtraDofDiffusionVariable;};
    int getNumConstitutiveCurlVariable() const {return _numberConstitutiveCurlVariable;};
    virtual double getConstitutiveExtraDofDiffusionEqRatio(const int index) const {
      Msg::Error("partDomain::getConstitutiveExtraDofDiffusionEqRatio is not defined");
      return -1.;
    };
    virtual double getNonConstitutiveExtraDofEqRatio(const int index) const {
      Msg::Error("partDomain::getNonConstitutiveExtraDofEqRatio is not defined");
      return -1.;
    };
    virtual double getConstitutiveCurlEqRatio(const int index) const {
      Msg::Error("partDomain::getConstitutiveCurlEqRatio is not defined");
      return -1.;
    };

    // Dimension of domain
    virtual int getDim() const=0;
    int getTag()const{return _tag;}
    int getPhysical() const{return _phys;}

    virtual bool getAccountPathFollowingFlag() const {return true;};

    //check cohesive element insertion, failure
    virtual void checkFailure(IPField* ipf) const = 0;

    // check internal stata
    virtual void checkInternalState(IPField* ipf) const = 0;

    // restart internal state for saving/loading
    virtual void restartIPState(AllIPState * aips) const;

    virtual void initializeTerms(unknownField *uf,IPField *ip)=0;
    // alocate the object (new) but is not responsiblefor the delete (the NeumannBC is in charged of the delete)
    virtual LinearTermBase<double>* createNeumannTerm(FunctionSpace<double> *spneu, const elementGroup* g,
                                                      const simpleFunctionTime<double>* f, const unknownField *uf,
                                                      const IPField * ip, const nonLinearBoundaryCondition::location onWhat,
                                                      const nonLinearNeumannBC::NeumannBCType neumann_type,
                                                      const int comp) const=0;
    virtual BilinearTermBase* createNeumannMatrixTerm(FunctionSpace<double> *spneu, const elementGroup* g,
                                                      const simpleFunctionTime<double>* f, const unknownField *uf,
                                                      const IPField * ip, const nonLinearBoundaryCondition::location onWhat,
                                                      const nonLinearNeumannBC::NeumannBCType neumann_type,
                                                      const int comp) const;
    // true is domain has interface terms
    virtual bool IsInterfaceTerms() const=0;
    functionSpaceType::whichSpace getFunctionSpaceType() const{return _wsp;}

    virtual FunctionSpaceBase* getFunctionSpace() =0;
    virtual const FunctionSpaceBase* getFunctionSpace() const=0;
    
    virtual void getNoMassDofs(std::set<Dof>& DofVector) const{};
    
  // some data of BC have to be set by domain
    virtual FunctionSpaceBase* getSpaceForBC(const nonLinearBoundaryCondition::type bc_type, 
                                             const nonLinearBoundaryCondition::location bc_location,
                                             const nonLinearNeumannBC::NeumannBCType neumann_type,
                                             const int dof_comp,
                                             const mixedFunctionSpaceBase::DofType dofType, // for standard, curl dofs
                                             const elementGroup *groupBC) const=0; // for dirichlet, neumann and initial
                                                                                      // groupBC = element of domain where BC is applied
    virtual QuadratureBase* getQuadratureRulesForNeumannBC(nonLinearNeumannBC &neu) const=0;

    virtual void createIPState(AllIPState::ipstateContainer& map, const bool *state) const;

    virtual bool computeIPVariable(AllIPState *aips,const unknownField *ufield,const IPStateBase::whichState ws, bool stiff)=0;
    virtual void setDeformationGradientGradient(AllIPState *aips, const STensor33 &GM, const IPStateBase::whichState ws)=0;
    virtual void setdFmdFM(AllIPState *aips, std::map<Dof, STensor3> &dUdF,const IPStateBase::whichState ws)=0;  
    virtual void setdFmdGM(AllIPState *aips, std::map<Dof, STensor33> &dUdG,const IPStateBase::whichState ws)=0;
    virtual void setValuesForBodyForce(AllIPState *aips, const IPStateBase::whichState ws)=0;
    virtual bool computeIpv(AllIPState *aips,MElement *e, IPStateBase::whichState ws,
                             materialLaw *mlaw,fullVector<double> &disp, bool stiff)=0;
    virtual void setGaussIntegrationRule()=0;
    virtual bool getFormulation() const{return _fullDg;}
    virtual void setMaterialLaw(const std::map<int,materialLaw*> &maplaw)=0;

    // No materialLaw store here but the function to give the material law exist
    // The law has to be store in the derivated class
    virtual materialLaw* getMaterialLaw()=0;
    virtual const materialLaw* getMaterialLaw() const=0;
    virtual int getLawNum() const=0;
    virtual void setMaterialLawNumber(const int num) {Msg::Error("define me partDomain::setMaterialLawNumber");};
    // For dg formulation the stability parameters decrease the critical time step size
    // in explicit. So this function allows to scale the time step
    virtual double scaleTimeStep() const {return 1.;}
    // creation of interface. At least boundary to create interface domain
    // can be empty be not interdomain creation in this case
    virtual void createInterface(manageInterface &maninter)=0;
    virtual MElement* createVirtualInterface(IElement *ie) const=0;
    virtual MElement* createInterface(IElement* ie1,IElement* ie2) const=0;
    // create an interface domain. Use only in mpi case (an interface domain can be given or a new one has to be
                                                         // created if dgdom==NULL)
    virtual void createInterfaceMPI(const int rankOtherPart, const elementGroup &groupOtherPart,dgPartDomain* &dgdom){
      Msg::Error("To use parallel implementation you have to define createInterfaceMPI on your domain. Or your version is obsoleted to domain has to be given as last argument");
    }
    // Add for BC
    virtual FilterDof* createFilterDof(const int comp) const=0;

    // specify order of computational homogenization uses
    int getHomogenizationOrder() const;
    bool inMultiscaleSimulation()  const;
    bool isExtractCohesiveLawFromMicroDamage() const;
    bool getExtractIrreversibleEnergyFlag() const;
    const elementErosionFilter& getElementErosionFilter() const;

    virtual int getStressDimension() const{
      // this function is used to define the dimension of stress matrix in the
      // insystem nonliner system
      Msg::Error("This function must be defined if homogenized tangent is estimated by insystem method");
			return 0;
    }

    //compute mechanical average quantities
    virtual void computeAverageStress(const IPField* ipf, STensor3& stress) const;
    virtual void computeAverageBodyForce(const IPField* ipf, SVector3& BM) const;
    virtual void computeActiveDissipationAverageStressIncrement(const IPField* ipf, STensor3& stress) const;
    virtual void computeAverageHighOrderStress(const IPField* ipf, STensor33& stress) const;
    virtual void computeAverageHighOrderStressTangentModification(const IPField* ipf, double _rveVolume, STensor53& dPdGM, STensor53& dQdFM, STensor63& dQdGM) const;
    virtual double computeVolumeDomain(const IPField* ipf) const;
    virtual void computeBodyForceMultiplyPosition(const IPField* ipf, STensor3& BmX, STensor33& BmXoX) const;

    virtual void computeActiveDissipationCenter(const IPField* ipf, SPoint3& pos) const;
    virtual void computeActiveDissipationAverageStrainIncrement(const IPField* _ipf, STensor3& strain) const;
    virtual double computeVolumeActiveDissipationDomain(const IPField* ipf) const;

    // compute constitutive extra-dof quantities
    virtual void computeConstitutiveExtraDofAverageFlux(const int index, const IPField* _ipf, SVector3& extraDofFluxpart) const;
    virtual void computeConstitutiveExtraDofAverageInternalEnergy(const int index, const IPField* _ipf, double &internalEnergyExtraDofpart) const;
    virtual void computeConstitutiveExtraDofAverageFieldCapacityPerUnitField(const int index, const IPField* _ipf, double& fieldCapacityPart) const;
    virtual void computeConstitutiveExtraDofAverageMechanicalSource(const int index, const IPField* _ipf, double& mechanicalSource) const;

    // compute non-constitutive extra-dof quantities
    virtual void computeNonConstitutiveExtraDofAverageFlux(const int index, const IPField* _ipf, SVector3& extraDofFluxpart) const;

    //
    virtual double computeDeformationEnergy(const IPField* ipf) const;
    virtual double computePlasticEnergy(const IPField* ipf) const;
    virtual double computeIrreversibleEnergy(const IPField* ipf) const;

    virtual partDomain* clone() const{
      #ifdef _DEBUG
      Msg::Warning("Define copyDomain on your domain for multiscale problem");
      #endif
      return NULL;
    }
    #if defined(HAVE_MPI)
    virtual void setWorkingRanks(const int root, const std::set<int>& others);
    virtual int getRootRank() const;
    virtual void getOtherRanks(std::set<int>& others) const;
    virtual void clearOtherRanks();
    virtual void createIPMap();
    virtual const std::map<int,std::set<int> >& getMapIPBulk() const {return _mapIPBulk;};
    virtual const std::set<int>& getDomainIPBulk() const {return _domainIPBulk;};
    #endif //HAVE_MPI
    virtual double computeMeanValueDomain(const int num, const IPField* ipf, double& volume, const GPFilter* gpFilter) const;
    virtual double computeMeanIncrementValueDomain(const int num, const IPField* ipf, double& volume, const GPFilter* gpFilter) const;
    virtual double averagingOnActiveDissipationZone(const int num, const IPField* ipf, double & volume, const IPStateBase::whichState ws = IPStateBase::current) const;
    virtual double averagingIncrementOnActiveDissipationZone(const int num, const IPField* ipf, double & volume) const;
    virtual void initMicroMeshId();
    #endif // SWIG
};
// class for Dg part domain (pure virtual)
class dgPartDomain : public partDomain
{
	#ifndef SWIG
  protected :
    BilinearTermBase* btermBound;
    LinearTermBase<double>* ltermBound;
    BilinearTermBase* btermVirtBound;
    LinearTermBase<double>* ltermVirtBound;
    QuadratureBase* integBound;

    // for pathfollowing
    ScalarTermBase<double>* scalarTermPFBound; // scalar pathfollowing term
    LinearTermBase<double>* linearTermPFBound; // linear pathFollowing term

    // For matrix by perturbation
    bool _interByPert;
    bool _virtByPert;
    bool _averageStrainBased; // interface law evaluated with average strains
    #if defined(HAVE_MPI)
    std::set<int>  _domainIPInterfacePlus, // plus intergration points lie on this domain
                    _domainIPInterfaceMinus; //  minus integration points lie on this domain
    std::map<int,std::set<int> > _mapIPInterfacePlus, // map IP for positive part of interface elements
                                _mapIPInterfaceMinus; // map IP for negative part of interface elements
    #endif //HAVE_MPI

  public :
    // TODO protect these variables
    elementGroup *gi; // support for the interfaceElement TODO cast to a elementGroup
    elementGroup *gib; // support for the interfaceElement TODO cast to a elementGroup
    elementGroup *giv; // support for the virtual interface element (used to set Neumann and Dirichlet BC)

    virtual void clearElementGroup()
    {
      partDomain::clearElementGroup();
      gi->clearAll();
    }
    #endif // SWIG
  public:
    virtual void averageStrainBased(const bool flg);

  #ifndef SWIG
  public:
    dgPartDomain(const int tag, const int phys, const int ws = 0,
               const bool fdg=false, int numNL=0, int numCED=0, int numCurlData=0, bool hasBodyForceForHO=false, int BFModuliType=0);
    dgPartDomain(const dgPartDomain &source);
    virtual ~dgPartDomain();
  //                  delete integBound; delete gib; delete giv}
    BilinearTermBase* getBilinearInterfaceTerm(){return btermBound;}
    LinearTermBase<double>* getLinearInterfaceTerm()const{return ltermBound;}
    BilinearTermBase* getBilinearVirtualInterfaceTerm(){return btermVirtBound;}
    LinearTermBase<double>* getLinearVirtualInterfaceTerm(){return ltermVirtBound;}
    QuadratureBase* getInterfaceGaussIntegrationRule() const {return integBound;}

    ScalarTermBase<double>* getScalarTermPFBound() const {return scalarTermPFBound;};
    LinearTermBase<double>* getLinearTermPFBound() const {return linearTermPFBound;};

    virtual bool computeIPVariable(AllIPState *aips,const unknownField *ufield,const IPStateBase::whichState ws, bool stiff)=0;
    virtual bool computeIpv(AllIPState *aips,MInterfaceElement *ie, IntPt *GP,const IPStateBase::whichState ws,
                              partDomain* efMinus, partDomain *efPlus,materialLaw *mlawminus,
                              materialLaw *mlawplus,fullVector<double> &dispm,
                              fullVector<double> &dispp,
                              const bool virt, bool stiff,const bool checkfrac=true)=0;
    virtual bool computeIpv(AllIPState *aips,MElement *e, IPStateBase::whichState ws,
                              materialLaw *mlaw,fullVector<double> &disp, bool stiff)=0;
    
    virtual void initiallyBreakAllInterfaceIP(AllIPState *aips) const;
    #if defined(HAVE_MPI)
    virtual void createIPMap();
    virtual const std::map<int,std::set<int> >& getMapIPInterfacePlus() const {return _mapIPInterfacePlus;};
    virtual const std::set<int>& getDomainIPInterfacePlus() const {return _domainIPInterfacePlus;};
    virtual const std::map<int,std::set<int> >& getMapIPInterfaceMinus() const {return _mapIPInterfaceMinus;};
    virtual const std::set<int>& getDomainIPInterfaceMinus() const {return _domainIPInterfaceMinus;};
    #endif // HAVE_MPI

    virtual void setGaussIntegrationRule()=0;
    virtual void createIPState(AllIPState::ipstateContainer& map, const bool *state) const;
    virtual int getDim() const=0;
    virtual bool IsInterfaceTerms() const{return true;}
    virtual FunctionSpaceBase* getFunctionSpace() =0;
    virtual const FunctionSpaceBase* getFunctionSpace() const=0;

    virtual void matrixByPerturbation(const int ibulk, const int iinter, const int ivirt,const double eps=1e-8)=0;
    virtual void setMaterialLaw(const std::map<int,materialLaw*> &maplaw)=0;
    virtual materialLaw* getMaterialLaw(){Msg::Error("The law to retrieve is not given on a dgdom"); return NULL;}
    virtual const materialLaw* getMaterialLaw() const{Msg::Error("The law to retrieve is not given on a dgdom"); return NULL;}
    virtual materialLaw* getMaterialLawMinus()=0;
    virtual const materialLaw* getMaterialLawMinus() const=0;
    virtual materialLaw* getMaterialLawPlus()=0;
    virtual const materialLaw* getMaterialLawPlus() const=0;
    virtual int getLawNum() const=0;
    virtual int getMinusLawNum() const=0;
    virtual int getPlusLawNum() const=0;
    virtual const partDomain* getMinusDomain() const=0;
    virtual const partDomain* getPlusDomain() const=0;
    virtual partDomain* getMinusDomain()=0;
    virtual partDomain* getPlusDomain()=0;
    virtual void createInterface(manageInterface &maninter)=0;
    virtual MElement* createVirtualInterface(IElement *ie) const=0;
    virtual MElement* createInterface(IElement *ie1, IElement *ie2) const=0;
    // Add for BC
    virtual FilterDof* createFilterDof(const int comp) const=0;
    virtual interfaceQuadratureBase* getInterfaceQuadrature() const = 0;

    // as to fill the array (of size Msg::GetCommSize()) with at index i the number of values to communicate with rank i
    // Normally it is an array of zero in input
    virtual void numberValuesToTransfertMPI(AllIPState *aips,int* sizeeachrank)const;
    // fill the vector that will be communicate to rank rank
    virtual void fillArrayIPvariableMPI(AllIPState *aips,const int rank,double* arrayMPI) const;
    virtual void setIPVariableFromArrayMPI(AllIPState *aips,const int rank,const int arraysize,const double* arrayMPI)const;
    virtual void initMicroMeshId();
    
    #endif
};

class QuadratureFactory
{
  public:
    // bulk
    static void createBulkGaussQuadrature(MElement* e, QuadratureBase*& integBulk);
    static void createBulkGaussQuadratureHierarchicalFE(int order, MElement* e, QuadratureBase*& integBulk);
    // interface
    static void createInterfaceGaussQuadrature(MElement* ie, QuadratureBase* & integBound, interfaceQuadratureBase*& interQuad);
    static void createInterfaceLobattoQuadrature(MElement* ie, QuadratureBase* & integBound, interfaceQuadratureBase*& interQuad);
    //
    static void createInterfaceGaussQuadratureHierarchicalFE(int order, MElement* ie, QuadratureBase* & integBound, interfaceQuadratureBase*& interQuad);
    static void createInterfaceLobattoQuadratureHierarchicalFE(int order, MElement* ie, QuadratureBase* & integBound, interfaceQuadratureBase*& interQuad);
    
    //
    static void createGaussQuadratureForNeumannBC(MElement* e, QuadratureBase*& integ);
    static void createLobattoQuadratureForNeumannBC(MElement* e, QuadratureBase*& integ);
    
    static void createGaussQuadratureForNeumannBCHierarchicalFE(int order, MElement* e, QuadratureBase*& integ);
};
#endif
