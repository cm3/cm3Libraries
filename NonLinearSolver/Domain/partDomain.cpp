//
// C++ Interface: partDomain
//
// Description: Interface class to used solver. Your term ha to be store in a domain
//
// Author:  <Gauthier BECKER>, (C) 2010
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "partDomain.h"
#include "ipField.h"
#include "numericalMaterial.h"
#include "MInterfacePoint.h"
#include "ipFiniteStrain.h"
#include "extraDofIPVariable.h"
#include "nonLinearMechSolver.h"
#include "GaussLobatto2DQuadratureRules.h"


#if defined(HAVE_MPI)
#include "mpi.h"
#endif //HAVE_MPI

partDomain::partDomain(const int tag, const int phys,
             const int ws,const bool fdg, int numNL, int numExtraDof, int numberCurlData, bool hasBodyForceForHO, int BFModuliType) : 
_tag(tag), 
_phys(phys),
_fullDg(fdg),
_nlNonLocal(false),
btermBulk(NULL),
btermElasticBulk(NULL),
btermUndamagedElasticBulk(NULL),
massterm(NULL),
ltermBulk(NULL),
integBulk(NULL),
btermTangent(NULL),
btermBodyForceTangent(NULL),
btermElasticTangent(NULL),
btermUndamagedElasticTangent(NULL),
scalarTermPF(NULL),
linearTermPF(NULL),
linearTermBodyForcePF(NULL),
_hasBodyForceForHO(hasBodyForceForHO),
_BodyForceModuliType((BodyForceModuliType)BFModuliType),
_numberNonLocalVariable(numNL),
_numberConstitutiveExtraDofDiffusionVariable(numExtraDof),
_numberConstitutiveCurlVariable(numberCurlData),
_bmbp(false),
_eps(1e-8),
setmaterial(false),
_distributedOtherRanks(true),
_distributedOnRoot(true),
g(NULL), 
_macroSolver(NULL)

{
  _groupGhostMPI = new elementGroup();
  switch(ws){
   case 0:
    _wsp = functionSpaceType::Lagrange;
    break;
   case 1:
    _wsp = functionSpaceType::Hierarchical;
    break;
   case 10000: // Allow to set space later (used for interface domain)
    _wsp = functionSpaceType::Inter;
    break;
   default:
    Msg::Error("Function space type is unknown for partDomain %d. So Lagrange by default");
    _wsp = functionSpaceType::Lagrange;
  }
  #if defined(HAVE_MPI)
  _rootRank = Msg::GetCommRank();
  #endif //HAVE_MPI
}

partDomain::partDomain(const partDomain &source):
_tag(source._tag), 
_phys(source._phys),
_fullDg(source._fullDg),
_nlNonLocal(source._nlNonLocal),
_wsp(source._wsp),
btermBulk(NULL),
btermElasticBulk(NULL),
btermUndamagedElasticBulk(NULL),
massterm(NULL),
ltermBulk(NULL),
integBulk(NULL),
btermTangent(NULL),
btermBodyForceTangent(NULL),
btermElasticTangent(NULL),
btermUndamagedElasticTangent(NULL),
scalarTermPF(NULL),
linearTermPF(NULL),
linearTermBodyForcePF(NULL),
_hasBodyForceForHO(source._hasBodyForceForHO),
_BodyForceModuliType(source._BodyForceModuliType),
_rveGeoInertia(source._rveGeoInertia),
_rveVolume(source._rveVolume),
_GM(source._GM),
_Homogeneous_K(source._Homogeneous_K),
_numberNonLocalVariable(source._numberNonLocalVariable),
_numberConstitutiveExtraDofDiffusionVariable(source._numberConstitutiveExtraDofDiffusionVariable),
_numberConstitutiveCurlVariable(source._numberConstitutiveCurlVariable),
_bmbp(source._bmbp),
_eps(source._eps),
setmaterial(false),
_distributedOtherRanks(source._distributedOtherRanks),
_distributedOnRoot(source._distributedOnRoot),
g(NULL), 
_macroSolver(NULL)
{
	_groupGhostMPI = new elementGroup();
  #if defined(HAVE_MPI)
  _rootRank = Msg::GetCommRank();
  #endif //HAVE_MPI
}

partDomain::~partDomain(){
  if (btermBulk) {delete btermBulk; btermBulk = NULL;}
  if (ltermBulk) {delete ltermBulk; ltermBulk = NULL;}
  if (integBulk) {delete integBulk; integBulk = NULL;}
  if (btermElasticBulk) {delete btermElasticBulk; btermElasticBulk = NULL;};
  if (btermUndamagedElasticBulk) {delete btermUndamagedElasticBulk; btermUndamagedElasticBulk=NULL;}
  if (massterm) {delete massterm; massterm = NULL;}
  if (btermTangent) {delete btermTangent; btermTangent= NULL;};
  if (btermBodyForceTangent) {delete btermBodyForceTangent; btermBodyForceTangent= NULL;};
  if (g) {delete g; g = NULL;}
  if (_groupGhostMPI) {delete _groupGhostMPI; _groupGhostMPI = NULL;};
  if (scalarTermPF) {delete scalarTermPF; scalarTermPF = NULL;}
  if (linearTermPF) {delete linearTermPF; linearTermPF = NULL;}
  if (linearTermBodyForcePF) {delete linearTermBodyForcePF; linearTermBodyForcePF = NULL;}
}

bool partDomain::isDistributedRootRank() const {return _distributedOnRoot;};
void partDomain::distributeOnRootRank(const int flg){_distributedOnRoot = flg;};
bool partDomain::isDistributedOtherRanks() const {return _distributedOtherRanks;};
void partDomain::setDistributedOtherRanks(const bool flag) {_distributedOtherRanks = flag;};

void partDomain::setMacroSolver(const nonLinearMechSolver* solver){
	_macroSolver = solver;
}
const nonLinearMechSolver* partDomain::getMacroSolver() const {return _macroSolver;};

BilinearTermBase* partDomain::createNeumannMatrixTerm(FunctionSpace<double> *spneu, const elementGroup* g,
                                                      const simpleFunctionTime<double>* f, const unknownField *uf,
                                                      const IPField * ip, 
                                                      const nonLinearBoundaryCondition::location onWhat,
                                                      const nonLinearNeumannBC::NeumannBCType neumann_type,
                                                      const int comp) const
{
  return new BiNonLinearTermVoid();
};

void partDomain::createIPState(AllIPState::ipstateContainer& map, const bool *state) const{
  IntPt* GP;
  const materialLaw *mlaw = this->getMaterialLaw();
  if (mlaw== NULL) return;
	#if defined(HAVE_MPI)
  if (_otherRanks.size() > 0 and mlaw->isNumeric()){
    if (Msg::GetCommRank() == _rootRank){
      // create IP State
      for (elementGroup::elementContainer::const_iterator it = this->g->begin(); it != this->g->end(); ++it){
        MElement *ele = it->second;
        int npts_bulk=this->getBulkGaussIntegrationRule()->getIntPoints(ele,&GP);
        AllIPState::ipstateElementContainer tp(npts_bulk);
        for(int i=0;i<npts_bulk;i++){
          int num = numericalMaterialBase::createTypeWithTwoInts(ele->getNum(),i);
          if (_domainIPBulk.find(num) != _domainIPBulk.end())
            mlaw->createIPState(tp[i],_hasBodyForceForHO, state,ele,ele->getNumVertices(),&(GP[i]),i);
          else{
            const numericalMaterialBase* nummat = dynamic_cast<const numericalMaterialBase*>(mlaw);
            if (nummat)
              nummat->createIPState(false,tp[i],_hasBodyForceForHO,state,ele,ele->getNumVertices(),&(GP[i]),i);
            else
              Msg::Error("Error error at creating the bulk IPState");
          }
          tp[i]->setLocation(IPVariable::BULK);
        }
        map.insert(AllIPState::ipstatePairType(ele->getNum(),tp));
      }
    }
    else if (_otherRanks.find(Msg::GetCommRank()) != _otherRanks.end()) {
      for (std::set<int>::iterator it = _domainIPBulk.begin(); it != _domainIPBulk.end(); it++){
        int num = *it;
        int elnum, gnum;
        IntPt *GPnum;
        numericalMaterialBase::getTwoIntsFromType(num,elnum,gnum);
        MElement* ele = new MPoint(NULL,elnum,0);
        int npts_bulk=this->getBulkGaussIntegrationRule()->getIntPoints(ele,&GPnum);
        AllIPState::ipstateElementContainer tp(1);
        mlaw->createIPState(tp[0],_hasBodyForceForHO, state,ele,ele->getNumVertices(),&(GPnum[0]),gnum);
        tp[0]->setLocation(IPVariable::BULK);
        delete ele;
        map.insert(AllIPState::ipstatePairType(num,tp));
      };
    }
  }
  else
	#endif //HAVE_MPI
	{
    for (elementGroup::elementContainer::const_iterator it = this->g->begin(); it != this->g->end(); ++it){
      MElement *ele = it->second;
      int npts_bulk=this->getBulkGaussIntegrationRule()->getIntPoints(ele,&GP);
      AllIPState::ipstateElementContainer tp(npts_bulk);
      for(int i=0;i<npts_bulk;i++){
        mlaw->createIPState(tp[i],_hasBodyForceForHO, state,ele,ele->getNumVertices(),&(GP[i]),i);
        tp[i]->setLocation(IPVariable::BULK);
      }
      map.insert(AllIPState::ipstatePairType(ele->getNum(),tp));
    }
  }
};

double partDomain::computeMeanValueDomain(const int num, const IPField* ipf, double& solid, const GPFilter* filter) const{
  IntPt* GP;
  double meanVal = 0;
  solid = 0;
  const ipFiniteStrain *vipv[256];
  const ipFiniteStrain *vipvprev[256];
  for (elementGroup::elementContainer::const_iterator it = this->g->begin(); it != this->g->end(); ++it){
    MElement *ele = it->second;
    ipf->getIPv(ele,vipv, vipvprev);
    int npts_bulk=this->getBulkGaussIntegrationRule()->getIntPoints(ele,&GP);
    for (int i=0; i<npts_bulk; i++){
      double weight= GP[i].weight;
      double u = GP[i].pt[0];
      double v = GP[i].pt[1];
      double w = GP[i].pt[2];
      double detJ = ele->getJacobianDeterminant(u,v,w);
      double fact = filter->getFactor(vipv[i],vipvprev[i]);
      meanVal += fact*vipv[i]->get(num)*weight*detJ;
      solid += fact*weight*detJ;
    };
  }
  if (solid > 0){
    meanVal /= solid;
  }
  return meanVal;
};

double partDomain::computeMeanIncrementValueDomain(const int num, const IPField* ipf, double& solid, const GPFilter* filter) const{
  IntPt* GP;
  double meanVal = 0;
  solid = 0;
  const ipFiniteStrain *vipv[256];
  const ipFiniteStrain *vipvprev[256];
  for (elementGroup::elementContainer::const_iterator it = this->g->begin(); it != this->g->end(); ++it){
    MElement *ele = it->second;
    ipf->getIPv(ele,vipv, vipvprev);
    int npts_bulk=this->getBulkGaussIntegrationRule()->getIntPoints(ele,&GP);
    for (int i=0; i<npts_bulk; i++){
      double weight= GP[i].weight;
      double u = GP[i].pt[0];
      double v = GP[i].pt[1];
      double w = GP[i].pt[2];
      double detJ = ele->getJacobianDeterminant(u,v,w);
      double fact = filter->getFactor(vipv[i],vipvprev[i]);
      meanVal += fact*(vipv[i]->get(num)-vipvprev[i]->get(num))*weight*detJ;
      solid += fact*weight*detJ;
    };
  }
  if (solid > 0){
    meanVal /= solid;
  }
  return meanVal;
};



double partDomain::averagingOnActiveDissipationZone(const int num, const IPField* ipf, double& solid, const IPStateBase::whichState ws) const{
  IntPt* GP;
  double meanVal = 0;
  solid = 0.;
  const ipFiniteStrain *vipv[256];
  for (elementGroup::elementContainer::const_iterator it = this->g->begin(); it != this->g->end(); ++it){
    MElement *ele = it->second;
    ipf->getIPv(ele,vipv,ws);
    int npts_bulk=this->getBulkGaussIntegrationRule()->getIntPoints(ele,&GP);
    for (int i=0; i<npts_bulk; i++){
      // compute if damage is active and non unloading
      if (vipv[i]->dissipationIsActive()){
        double weight= GP[i].weight;
        double u = GP[i].pt[0];
        double v = GP[i].pt[1];
        double w = GP[i].pt[2];
        double detJ = ele->getJacobianDeterminant(u,v,w);
        double valnum = vipv[i]->get(num);
        if (num == IPField::F_XX or num == IPField::F_YY or num == IPField::F_ZZ or 
            num == IPField::FP_XX or num == IPField::FP_YY or num == IPField::FP_ZZ)
        {
          valnum -= 1.;
        }
        meanVal += valnum*weight*detJ;
        solid += weight*detJ;
      }
    };
  }
  if (solid > 0)
  {
    meanVal /= solid;
  }
  return meanVal;
};

double partDomain::averagingIncrementOnActiveDissipationZone(const int num, const IPField* ipf, double& solid) const{
  IntPt* GP;
  double meanVal = 0;
  solid = 0.;
  const ipFiniteStrain *vipv[256];
  const ipFiniteStrain *vipvprev[256];
  for (elementGroup::elementContainer::const_iterator it = this->g->begin(); it != this->g->end(); ++it){
    MElement *ele = it->second;
    ipf->getIPv(ele,vipv,IPStateBase::current);
    ipf->getIPv(ele,vipvprev,IPStateBase::previous);
    int npts_bulk=this->getBulkGaussIntegrationRule()->getIntPoints(ele,&GP);
    for (int i=0; i<npts_bulk; i++){
      // compute if damage is active and non unloading
      if (vipv[i]->dissipationIsActive()){
        double weight= GP[i].weight;
        double u = GP[i].pt[0];
        double v = GP[i].pt[1];
        double w = GP[i].pt[2];
        double detJ = ele->getJacobianDeterminant(u,v,w);
        double valnum = vipv[i]->get(num)-vipvprev[i]->get(num);
        meanVal += valnum*weight*detJ;
        solid += weight*detJ;
      }
    };
  }
  if (solid >0.)
  {
    meanVal /= solid;
  }
  return meanVal;
};

#if defined(HAVE_MPI)
void partDomain::setWorkingRanks(const int root, const std::set<int>& others) {
  _rootRank = root;
  _otherRanks = others;
};
int partDomain::getRootRank() const{
  return _rootRank;
};
void partDomain::getOtherRanks(std::set<int>& others) const{
  others = _otherRanks;
};

void partDomain::clearOtherRanks(){
  _otherRanks.clear();
};

void partDomain::createIPMap(){
  const materialLaw* mlaw = this->getMaterialLaw();
  if (mlaw == NULL) return;

  _mapIPBulk.clear();
  _domainIPBulk.clear();

  if (_otherRanks.size()>0 and mlaw->isNumeric()){
    if (Msg::GetCommRank() == _rootRank){
      // get all IP number
      std::vector<int> allRanks(_otherRanks.begin(),_otherRanks.end());
      if (_distributedOnRoot)
        allRanks.push_back(_rootRank);
      std::vector<int> allIPBulk;
      IntPt* GP;
      for (elementGroup::elementContainer::const_iterator it = this->g->begin(); it != this->g->end(); ++it){
        MElement *ele = it->second;
        int npts_bulk=this->getBulkGaussIntegrationRule()->getIntPoints(ele,&GP);
        for(int i=0;i<npts_bulk;i++){
          int num = numericalMaterialBase::createTypeWithTwoInts(ele->getNum(),i);
          allIPBulk.push_back(num);
        }
      }
      numericalMaterialBase::distributeMacroIP(allIPBulk,allRanks,_mapIPBulk);
      allIPBulk.clear();

      // Send data for bulk elements
      for (std::map<int,std::set<int> >::iterator it = _mapIPBulk.begin(); it!= _mapIPBulk.end(); it++){
        int rank = it->first;
        std::set<int>& numElOnRank = _mapIPBulk[rank];
        // at root rank
        if (rank == _rootRank){
          _domainIPBulk = numElOnRank;
        }
        // at other ranks
        else{
          int bufferSize = numElOnRank.size();
          MPI_Send(&bufferSize,1,MPI_INT,rank,numericalMaterialBase::createTypeWithTwoInts(getPhysical(),0),MPI_COMM_WORLD);
          if (bufferSize>0){
            int* buffer = new int[bufferSize];
            int idex = 0;
            for (std::set<int>::iterator its = numElOnRank.begin(); its != numElOnRank.end(); its++){
              buffer[idex] = *its;
              idex++;
            };
            MPI_Send(buffer,bufferSize,MPI_INT,rank,numericalMaterialBase::createTypeWithTwoInts(getPhysical(),1),MPI_COMM_WORLD);
            delete[] buffer;
          }
        }
      };
    }
    else if (_otherRanks.find(Msg::GetCommRank()) != _otherRanks.end()){
      // receive data from root for bulk elements
      MPI_Status status;
      int bufferSize;
      MPI_Recv(&bufferSize,1,MPI_INT,_rootRank,numericalMaterialBase::createTypeWithTwoInts(getPhysical(),0),MPI_COMM_WORLD,&status);
      if (bufferSize>0){
        int* buffer = new int[bufferSize];
        MPI_Recv(buffer,bufferSize,MPI_INT,_rootRank,numericalMaterialBase::createTypeWithTwoInts(getPhysical(),1),MPI_COMM_WORLD,&status);
        _domainIPBulk.clear();
        for (int i=0; i<bufferSize; i++)
          _domainIPBulk.insert(buffer[i]);
        delete[] buffer;
      }
    }
  }
};
#endif //HAVE_MPI

void partDomain::initMicroMeshId(){
  materialLaw* mlaw = this->getMaterialLaw();
  if (mlaw == NULL) return;
  if (mlaw->isNumeric() ){
    numericalMaterialBase* nummat = dynamic_cast<numericalMaterialBase*>(mlaw);
    if (nummat == NULL)
      Msg::Error("material law must be derived from numericalMaterialBase");
    IntPt* GP;
    for (elementGroup::elementContainer::const_iterator it = g->begin(); it!= g->end(); it++){
      MElement* ele = it->second;
      int npts = this->getBulkGaussIntegrationRule()->getIntPoints(ele,&GP);
      for (int i=0; i<npts; i++){
        nummat->assignMeshId(ele->getNum(),i);
      }
    }
  }
};

void partDomain::setBulkMatrixByPerturbation(const int i, const double eps)
{
  i == 0 ? _bmbp = false : _bmbp = true;
  _eps = eps;
}

int partDomain::getHomogenizationOrder() const {return _macroSolver->getHomogenizationOrder();}
bool partDomain::inMultiscaleSimulation()  const {return _macroSolver->inMultiscaleSimulation();};
bool partDomain::isExtractCohesiveLawFromMicroDamage() const {return _macroSolver->getDamageToCohesiveJumpFlag();};
bool partDomain::getExtractIrreversibleEnergyFlag() const {return _macroSolver->getExtractIrreversibleEnergyFlag();};
const elementErosionFilter& partDomain::getElementErosionFilter() const{return _macroSolver->getElementErosionFilter();}

void partDomain::computeAverageStress(const IPField* ipf, STensor3& sig) const{
	if (g->size() >0){
		IntPt* GP;
		const ipFiniteStrain *vipv[256];
		const FunctionSpaceBase* space = this->getFunctionSpace();
		for (elementGroup::elementContainer::const_iterator it= g->begin(); it!=g->end(); it++){
			MElement* ele= it->second;
			ipf->getIPv(ele,vipv);
			int npts= integBulk->getIntPoints(ele,&GP);
			for (int i=0; i<npts; i++){
				double weight= GP[i].weight;
				double &detJ = vipv[i]->getJacobianDeterminant(ele,GP[i]);
				const STensor3& P = vipv[i]->getConstRefToFirstPiolaKirchhoffStress();
				sig+=P*detJ*weight;
				
				if(vipv[i]->hasBodyForceForHO()){
				   SPoint3 pt;
				   std::vector<TensorialTraits<double>::ValType> &Vals = vipv[i]->f(space,ele,GP[i]);
				   ele->pnt(Vals,pt);
				   const SVector3& Bm = vipv[i]->getConstRefToBm();
				   for (int ii=0; ii<3; ++ii)
				      for (int jj=0; jj<3; ++jj)	
				         sig(ii,jj) -= Bm(ii)*pt[jj]*detJ*weight;
								
				}			
			};
		};
	}
};

const double partDomain::getRVEVolume() const {
   return _rveVolume;
};
void partDomain::setRVEVolume(const double val) {
   _rveVolume =val;
};

void partDomain::setSecondOrderKinematic(const STensor33& val){
  _GM = val;
}
const STensor33& partDomain::getSecondOrderKinematic() const{
   return _GM; 
}    
  
void partDomain::setHomogeneousTangent(const STensor43& val){
  _Homogeneous_K = val;
}
const STensor43& partDomain::getHomogeneousTangent() const{
   return  _Homogeneous_K; 
}    
    
const STensor3& partDomain::getRVEGeometricalInertia() const {
   return _rveGeoInertia;
};

void partDomain::setRVEGeometricalInertia(const STensor3& val){
  STensorOperation::zero(_rveGeoInertia);
  _rveGeoInertia(0,0) = val(0,0);
  _rveGeoInertia(1,1) = val(1,1);
  _rveGeoInertia(2,2) = val(2,2);
}


void partDomain::computeAverageBodyForce(const IPField* ipf, SVector3& BM) const{
	if (g->size() >0){
		IntPt* GP;
		const ipFiniteStrain *vipv[256];
		for (elementGroup::elementContainer::const_iterator it= g->begin(); it!=g->end(); it++){
			MElement* ele= it->second;
			ipf->getIPv(ele,vipv);
			int npts= integBulk->getIntPoints(ele,&GP);
			for (int i=0; i<npts; i++){
				double weight= GP[i].weight;
				double &detJ = vipv[i]->getJacobianDeterminant(ele,GP[i]);
				const SVector3& Bm = vipv[i]->getConstRefToBm();
				BM+= Bm*detJ*weight;
			};
		};
	}
};

void partDomain::computeActiveDissipationAverageStressIncrement(const IPField* ipf, STensor3& stress) const{
  // compute average strain on active damaging part
  // damaging volume will besed on ws= current, previous, or initial
	if ((g->size()>0) and (ipf->getNumOfActiveDissipationIPs() > 0)){
		IntPt* GP;
	// compute from damaging zone
		for (elementGroup::elementContainer::const_iterator it= g->begin(); it!=g->end(); it++){
			MElement* ele= it->second;

			const AllIPState::ipstateElementContainer *vips = ipf->getAips()->getIPstate(ele->getNum());

			int npts= integBulk->getIntPoints(ele,&GP);
			for (int i=0; i<npts; i++){
				const IPStateBase *ips        = (*vips)[i];
				const ipFiniteStrain *ipv     = static_cast<const ipFiniteStrain*>(ips->getState(IPStateBase::current));
				const ipFiniteStrain *ipvprev = static_cast<const ipFiniteStrain*>(ips->getState(IPStateBase::activeDissipation));

				if (ipvprev->dissipationIsActive()){
					double weight= GP[i].weight;
					const double &detJ = ipv->getJacobianDeterminant(ele,GP[i]);
                                      double ratio = detJ*weight;
					const STensor3& P =ipv->getConstRefToFirstPiolaKirchhoffStress();
                                      const STensor3& Pprev = ipvprev->getConstRefToFirstPiolaKirchhoffStress();
					for (int j=0; j<3; j++){
						for (int k=0; k<3; k++){
							stress(j,k) += (P(j,k)-Pprev(j,k))*ratio;
						}
					}
				}
			};
		};
	}
};


void partDomain::computeAverageHighOrderStress(const IPField* ipf, STensor33& sig) const{
	if (g->size()>0){
		IntPt* GP;
		const ipFiniteStrain *vipv[256];
		const FunctionSpaceBase* space = this->getFunctionSpace();
		for (elementGroup::elementContainer::const_iterator it= g->begin(); it!=g->end(); it++){
			MElement* ele= it->second;
			if(ele->getParent()) ele = ele->getParent();
			ipf->getIPv(ele,vipv);
			int npts= integBulk->getIntPoints(ele,&GP);
			for (int i=0; i<npts; i++){
				double weight= GP[i].weight;
				double &detJ = vipv[i]->getJacobianDeterminant(ele,GP[i]);
				SPoint3 pt;
				std::vector<TensorialTraits<double>::ValType> &Vals = vipv[i]->f(space,ele,GP[i]);
				ele->pnt(Vals,pt);
				const STensor3& P = vipv[i]->getConstRefToFirstPiolaKirchhoffStress(); 
				double ratio = detJ*weight;
				for (int ii=0; ii<3; ++ii){
					for (int jj=0; jj<3; ++jj){
						for (int kk=0; kk<3; ++kk){
							sig(ii,jj,kk) += 0.5*(P(ii,jj)*pt[kk]+P(ii,kk)*pt[jj])*ratio;
							
						}
					}
				}
				if(vipv[i]->hasBodyForceForHO()){
				   const SVector3& Bm = vipv[i]->getConstRefToBm();
				   for (int ii=0; ii<3; ++ii){
					for (int jj=0; jj<3; ++jj){
						for (int kk=0; kk<3; ++kk){
							sig(ii,jj,kk) -= 0.5*Bm(ii)*(pt[jj]*pt[kk]-_rveGeoInertia(jj,kk)/_rveVolume)*ratio;
							
						}
					}
				 }				   
			       }
			};
		};
	}
};

void partDomain::computeBodyForceMultiplyPosition(const IPField* ipf, STensor3& BmX, STensor33& BmXoX) const{
	if (g->size()>0){
		IntPt* GP;
		const ipFiniteStrain *vipv[256];
		const FunctionSpaceBase* space = this->getFunctionSpace();
		for (elementGroup::elementContainer::const_iterator it= g->begin(); it!=g->end(); it++){
			MElement* ele= it->second;
			if(ele->getParent()) ele = ele->getParent();
			ipf->getIPv(ele,vipv);
			int npts= integBulk->getIntPoints(ele,&GP);
			for (int i=0; i<npts; i++){
				double weight= GP[i].weight;
				double &detJ = vipv[i]->getJacobianDeterminant(ele,GP[i]);
				SPoint3 pt;
				std::vector<TensorialTraits<double>::ValType> &Vals = vipv[i]->f(space,ele,GP[i]);
				ele->pnt(Vals,pt);
				const SVector3& Bm = vipv[i]->getConstRefToBm();
				double ratio = detJ*weight;
				for (int ii=0; ii<3; ++ii){
					for (int jj=0; jj<3; ++jj){
						BmX(ii,jj) += Bm(ii)*pt[jj]*ratio;
						for (int kk=0; kk<3; ++kk)
						   BmXoX(ii,jj,kk) += 0.5*Bm(ii)*(pt[jj]*pt[kk]-_rveGeoInertia(jj,kk)/_rveVolume)*ratio;
										
					}
				}
							         
			};
		};
	}
};

void partDomain::computeAverageHighOrderStressTangentModification(const IPField* ipf, double _rveVolume, STensor53& dPdGM,  STensor53& dQdFM, STensor63& dQdGM) const{
    static const STensor3 I(1.);
    if (g->size()>0){
	IntPt* GP;
	SPoint3 pt;
	const ipFiniteStrain *vipv[256];
	const FunctionSpaceBase* space = this->getFunctionSpace();
	for (elementGroup::elementContainer::const_iterator it= g->begin(); it!=g->end(); it++){
	   MElement* ele= it->second;
	   if(ele->getParent()) ele = ele->getParent();
	   ipf->getIPv(ele,vipv);
	   int npts= integBulk->getIntPoints(ele,&GP);
	   for (int i=0; i<npts; i++){
	      double weight= GP[i].weight;
	      double &detJ = vipv[i]->getJacobianDeterminant(ele,GP[i]);
	      std::vector<TensorialTraits<double>::ValType> &Vals = vipv[i]->f(space,ele,GP[i]);
	      ele->pnt(Vals,pt);
	      if(!vipv[i]->hasBodyForceForHO()){
	         Msg::Error("Body Force is not used, HighOrderStressTangent doesn't need to be modifiied!");
	      }
	      const STensor43& dBmdGM = vipv[i]->getConstRefTodBmdGM(); 
              const STensor33& GM = vipv[i]->getConstRefToGM();
	      const STensor63& dCmdFm = vipv[i]->getConstRefTodCmdFm();
	      const STensor43& dFmdFM = vipv[i]->getConstRefToDFmdFM();
	      const STensor53& dFmdGM = vipv[i]->getConstRefToDFmdGM();
	      const STensor43& dPmdFm = vipv[i]->getConstRefToTangentModuli(); 	
	      static STensor43 dPmdFM;  
	      static STensor33 dFmdFM_GM;    
	      STensorOperation::zero(dPmdFM);         
	      STensorOperation::zero(dFmdFM_GM);     
	      for (int ii=0; ii<3; ++ii)
	        for (int jj=0; jj<3; ++jj)
		  for (int kk=0; kk<3; ++kk)
		   for (int ll=0; ll<3; ++ll)
	             for (int p=0; p<3; ++p){
	               dFmdFM_GM(ii,jj,kk) += dFmdFM(ii,jj,ll,p)*(GM(ll,p,kk)+ GM(ll,kk,p))*0.5;      
	               for (int q=0; q<3; ++q){	      
	                 dPmdFM(ii,jj,kk,ll) += dPmdFm(ii,jj,p,q)*dFmdFM(p,q,kk,ll);
	               }
	             }    	         
	      double ratio = detJ*weight;
	      
	      for (int ii=0; ii<3; ++ii)
	       for (int jj=0; jj<3; ++jj)
		for (int kk=0; kk<3; ++kk)
		 for (int ll=0; ll<3; ++ll)
		  for (int mm=0; mm<3; ++mm){
		    dPdGM(ii,jj,kk,ll,mm) -= dBmdGM(ii,kk,ll,mm)*pt[jj]*ratio;
		    for (int nn=0; nn<3; ++nn){
		      dQdGM(ii,jj,kk,ll,mm,nn) -= 0.5*dBmdGM(ii,ll,mm,nn)*pt[jj]*pt[kk]*ratio;			            
		      dQdGM(ii,jj,kk,ll,mm,nn) += 0.5*dBmdGM(ii,ll,mm,nn)*_rveGeoInertia(jj,kk)*ratio/_rveVolume;
	              dQdGM(ii,jj,kk,ll,mm,nn) -= 0.25*(dPmdFM(ii,jj,ll,mm)*_rveGeoInertia(nn,kk)+ dPmdFM(ii,kk,ll,mm)*_rveGeoInertia(nn,jj))*ratio/_rveVolume;
	              dQdGM(ii,jj,kk,ll,mm,nn) -= 0.25*(dPmdFM(ii,jj,ll,nn)*_rveGeoInertia(mm,kk)+ dPmdFM(ii,kk,ll,nn)*_rveGeoInertia(mm,jj))*ratio/_rveVolume;	
                    }  				    
	          }   
	          	     ///////////   dQdF, dQdG 
	      static STensor53 dCm_GM;    
	      STensorOperation::zero(dCm_GM);  
	      for (int ii=0; ii<3; ++ii)
               for (int jj=0; jj<3; ++jj)
                for (int r=0; r<3; ++r)
                  for (int s=0; s<3; ++s)
                   for (int ll=0; ll<3; ++ll)
	             for (int p=0; p<3; ++p)
	               for (int q=0; q<3; ++q){
	                 dCm_GM(ii,jj,r,s,ll) += dCmdFm(ii,jj,p,q,r,s)*dFmdFM_GM(p,q,ll);
	               } 	       
	         
	     for (int ii=0; ii<3; ++ii)
              for (int jj=0; jj<3; ++jj)
               for (int kk=0; kk<3; ++kk)
                for (int mm=0; mm<3; ++mm)
                 for (int nn=0; nn<3; ++nn)
                  for (int ll=0; ll<3; ++ll)
	            for (int r=0; r<3; ++r)
	              for (int s=0; s<3; ++s){
	                 //dQdF
		         dQdFM(ii,jj,kk,mm,nn) -= 0.5* dCm_GM(ii,jj,r,s,ll)*dFmdFM(r,s,mm,nn)*_rveGeoInertia(ll,kk)*ratio/_rveVolume;     
		         dQdFM(ii,jj,kk,mm,nn) -= 0.5* dCm_GM(ii,kk,r,s,ll)*dFmdFM(r,s,mm,nn)*_rveGeoInertia(ll,jj)*ratio/_rveVolume; 
		 
		         //dQdG
		         for (int t=0; t<3; ++t){
		           dQdGM(ii,jj,kk,ll,mm,nn) -= 0.5* dCm_GM(ii,jj,r,s,t)*dFmdGM(r,s,ll,mm,nn)*_rveGeoInertia(t,kk)*ratio/_rveVolume;     
		           dQdGM(ii,jj,kk,ll,mm,nn) -= 0.5* dCm_GM(ii,kk,r,s,t)*dFmdGM(r,s,ll,mm,nn)*_rveGeoInertia(t,jj)*ratio/_rveVolume;     
	                 }   
	               }      
       }
    }
  }  
}          

double partDomain::computeVolumeDomain(const IPField* ipf) const{
  double volume = 0;
	if (g->size()>0){
		IntPt* GP;
		const ipFiniteStrain *vipv[256];

		for (elementGroup::elementContainer::const_iterator it= g->begin(); it!=g->end(); it++){
			MElement* ele= it->second;
			if(ele->getParent()) ele = ele->getParent();
			int npts= integBulk->getIntPoints(ele,&GP);
			ipf->getIPv(ele,vipv);
			for (int i=0; i<npts; i++){
				double weight= GP[i].weight;
				double& detJ = vipv[i]->getJacobianDeterminant(ele,GP[i]);
				double ratio = detJ*weight;
				volume+=ratio;
			};
		};
	}
  return volume;
};

void partDomain::computeActiveDissipationCenter(const IPField* ipf, SPoint3& pos) const{
	if ((g->size()>0) and (ipf->getNumOfActiveDissipationIPs() > 0)){
		IntPt* GP;
	// compute from damaging zone
		for (elementGroup::elementContainer::const_iterator it= g->begin(); it!=g->end(); it++){
			MElement* ele= it->second;
			const AllIPState::ipstateElementContainer *vips = ipf->getAips()->getIPstate(ele->getNum());
			int npts= integBulk->getIntPoints(ele,&GP);
			for (int i=0; i<npts; i++){
				const IPStateBase *ips        = (*vips)[i];
				const ipFiniteStrain *ipv     = static_cast<const ipFiniteStrain*>(ips->getState(IPStateBase::current));
				const ipFiniteStrain *ipvprev = static_cast<const ipFiniteStrain*>(ips->getState(IPStateBase::activeDissipation));
				if (ipvprev->dissipationIsActive()){
					double weight= GP[i].weight;
					const double &detJ = ipv->getJacobianDeterminant(ele,GP[i]);
          double ratio = detJ*weight;
					SPoint3 P;
					ele->pnt(GP[i].pt[0],GP[i].pt[1],GP[i].pt[2],P);
					for (int j=0; j<3; j++){
						pos[j] += P[j]*ratio;
					}
				}
			};
		};
	}
};

void partDomain::computeActiveDissipationAverageStrainIncrement(const IPField* ipf, STensor3& strain) const{
  // compute average strain on active damaging part
  // damaging volume will besed on ws= current, previous, or initial
	if ((g->size()>0) and (ipf->getNumOfActiveDissipationIPs() > 0)){
		IntPt* GP;
	// compute from damaging zone
		for (elementGroup::elementContainer::const_iterator it= g->begin(); it!=g->end(); it++){
			MElement* ele= it->second;
			const AllIPState::ipstateElementContainer *vips = ipf->getAips()->getIPstate(ele->getNum());
			int npts= integBulk->getIntPoints(ele,&GP);
			for (int i=0; i<npts; i++){
				const IPStateBase *ips        = (*vips)[i];
				const ipFiniteStrain *ipv     = static_cast<const ipFiniteStrain*>(ips->getState(IPStateBase::current));
				const ipFiniteStrain *ipvprev = static_cast<const ipFiniteStrain*>(ips->getState(IPStateBase::activeDissipation));

				if (ipvprev->dissipationIsActive()){
					double weight= GP[i].weight;
					const double &detJ = ipv->getJacobianDeterminant(ele,GP[i]);
          double ratio = detJ*weight;
					const STensor3& F =ipv->getConstRefToDeformationGradient();
					const STensor3& Fprev = ipvprev->getConstRefToDeformationGradient();
					for (int j=0; j<3; j++){
						for (int k=0; k<3; k++){
							strain(j,k) += (F(j,k) - Fprev(j,k))*ratio;
						}
					}
				}
			};
		};
	}
};

double partDomain::computeVolumeActiveDissipationDomain(const IPField* ipf) const{
  double volume = 0.;
	if (g->size()>0){
		IntPt* GP;
		const ipFiniteStrain *vipvRef[256];
	// compute from damaging zone

		for (elementGroup::elementContainer::const_iterator it= g->begin(); it!=g->end(); it++){
			MElement* ele= it->second;
			ipf->getIPv(ele,vipvRef,IPStateBase::activeDissipation);
			int npts= integBulk->getIntPoints(ele,&GP);
			for (int i=0; i<npts; i++){
				if (vipvRef[i]->dissipationIsActive()){
					double weight= GP[i].weight;
					double &detJ = vipvRef[i]->getJacobianDeterminant(ele,GP[i]);
					volume+=detJ*weight;
				}
			};
		};
	}

  return volume;
};

void partDomain::computeConstitutiveExtraDofAverageFlux(const int index, const IPField* ipf, SVector3& extraDofFluxpart) const{
	if (g->size() > 0){
		IntPt* GP;
		const IPVariable *vipv[256];

		for (elementGroup::elementContainer::const_iterator it= g->begin(); it!=g->end(); it++){
			MElement* ele= it->second;
			ipf->getIPv(ele,vipv);
			int npts= integBulk->getIntPoints(ele,&GP);
			for (int i=0; i<npts; i++){
				double weight= GP[i].weight;
				double detJ = 0;
				const ipFiniteStrain* ipfs = dynamic_cast<const ipFiniteStrain*>(vipv[i]);
				if (ipfs == NULL){
					detJ =  ipfs->getJacobianDeterminant(ele,GP[i]);
				}
				else{
					detJ = ele->getJacobianDeterminant(GP[i].pt[0],GP[i].pt[1],GP[i].pt[2]);
				}
				const extraDofIPVariableBase* extraipv = dynamic_cast<const extraDofIPVariableBase*>(vipv[i]);
				if (extraipv != NULL){
					SVector3 q(0.);
					extraipv->getConstitutiveExtraDofFlux(index,q);
					extraDofFluxpart+=q*(detJ*weight);
				}
				else{
					Msg::Error("extraDofIPVariableBase is not used");
				}

			};
		};
	}
};

void partDomain::computeConstitutiveExtraDofAverageInternalEnergy(const int index, const IPField* ipf, double &internalEnergyExtraDofpart) const{
  if (g->size() > 0){
		IntPt* GP;
		const IPVariable *vipv[256];
		for (elementGroup::elementContainer::const_iterator it= g->begin(); it!=g->end(); it++){
			MElement* ele= it->second;
			ipf->getIPv(ele,vipv);
			int npts= integBulk->getIntPoints(ele,&GP);
			for (int i=0; i<npts; i++){
				double weight= GP[i].weight;
				double detJ = 0;
				const ipFiniteStrain* ipfs = dynamic_cast<const ipFiniteStrain*>(vipv[i]);
				if (ipfs == NULL){
					detJ =  ipfs->getJacobianDeterminant(ele,GP[i]);
				}
				else{
					detJ = ele->getJacobianDeterminant(GP[i].pt[0],GP[i].pt[1],GP[i].pt[2]);
				}
				const extraDofIPVariableBase* extraipv = dynamic_cast<const extraDofIPVariableBase*>(vipv[i]);
				if (extraipv!=NULL){
					double internalEnergy = extraipv->getConstitutiveExtraDofInternalEnergy(index);
					internalEnergyExtraDofpart+=internalEnergy*(detJ*weight);
				}
				else{
					Msg::Error("extraDofIPVariableBase is not used");
				}
			};
		};
	}
};

void partDomain::computeConstitutiveExtraDofAverageFieldCapacityPerUnitField(const int index, const IPField* ipf, double& fieldCapacityPart) const{
	if (g->size() > 0){
		IntPt* GP;
		const IPVariable *vipv[256];

		for (elementGroup::elementContainer::const_iterator it= g->begin(); it!=g->end(); it++){
			MElement* ele= it->second;
			if (this->inMultiscaleSimulation()){
				ipf->getIPv(ele,vipv,IPStateBase::initial);
			}
			else{
				ipf->getIPv(ele,vipv,IPStateBase::previous);
			}

			int npts= integBulk->getIntPoints(ele,&GP);
			for (int i=0; i<npts; i++){
				double weight= GP[i].weight;
				double detJ = 0;
				const ipFiniteStrain* ipfs = dynamic_cast<const ipFiniteStrain*>(vipv[i]);
				if (ipfs == NULL){
					detJ =  ipfs->getJacobianDeterminant(ele,GP[i]);
				}
				else{
					detJ = ele->getJacobianDeterminant(GP[i].pt[0],GP[i].pt[1],GP[i].pt[2]);
				}
				const extraDofIPVariableBase* extraipv = dynamic_cast<const extraDofIPVariableBase*>(vipv[i]);
				if (extraipv!=NULL){
					double extraDofStoredEnergyPerUnitField = extraipv->getConstitutiveExtraDofFieldCapacityPerUnitField(index);
					fieldCapacityPart+=extraDofStoredEnergyPerUnitField*(detJ*weight);
				}
				else{
					Msg::Error("extraDofIPVariableBase is not used");
				}
			};
		};
	}
};

void partDomain::computeConstitutiveExtraDofAverageMechanicalSource(const int index, const IPField* ipf, double& mechanicalSource) const{
	if (g->size() > 0){
		IntPt* GP;
		const IPVariable *vipv[256];
		for (elementGroup::elementContainer::const_iterator it= g->begin(); it!=g->end(); it++){
			MElement* ele= it->second;
			ipf->getIPv(ele,vipv);
			int npts= integBulk->getIntPoints(ele,&GP);
			for (int i=0; i<npts; i++){
				double weight= GP[i].weight;
				double detJ = 0;
				const ipFiniteStrain* ipfs = dynamic_cast<const ipFiniteStrain*>(vipv[i]);
				if (ipfs == NULL){
					detJ =  ipfs->getJacobianDeterminant(ele,GP[i]);
				}
				else{
					detJ = ele->getJacobianDeterminant(GP[i].pt[0],GP[i].pt[1],GP[i].pt[2]);
				}
				const extraDofIPVariableBase* extraipv = dynamic_cast<const extraDofIPVariableBase*>(vipv[i]);
				if (extraipv!=NULL){
					double msource = extraipv->getConstitutiveExtraDofMechanicalSource(index);
					mechanicalSource+=msource*(detJ*weight);
				}
				else{
					Msg::Error("extraDofIPVariableBase is not used");
				}
			};
		};
	}
};

void partDomain::computeNonConstitutiveExtraDofAverageFlux(const int index, const IPField* ipf, SVector3& extraDofFluxpart) const{
	if (g->size() > 0){
		IntPt* GP;
		const IPVariable *vipv[256];

		for (elementGroup::elementContainer::const_iterator it= g->begin(); it!=g->end(); it++){
			MElement* ele= it->second;
			ipf->getIPv(ele,vipv);
			int npts= integBulk->getIntPoints(ele,&GP);
			for (int i=0; i<npts; i++){
				double weight= GP[i].weight;
				double detJ = 0;
				const ipFiniteStrain* ipfs = dynamic_cast<const ipFiniteStrain*>(vipv[i]);
				if (ipfs == NULL){
					detJ =  ipfs->getJacobianDeterminant(ele,GP[i]);
				}
				else{
					detJ = ele->getJacobianDeterminant(GP[i].pt[0],GP[i].pt[1],GP[i].pt[2]);
				}
				const extraDofIPVariableBase* extraipv = dynamic_cast<const extraDofIPVariableBase*>(vipv[i]);
				if (extraipv!=NULL){
					SVector3 q;
					extraipv->getNonConstitutiveExtraDofFlux(index,q);
					extraDofFluxpart+=q*(detJ*weight);
				}
				else{
					Msg::Error("extraDofIPVariableBase is not used");
				}
			};
		};
	}
};

double partDomain::computeDeformationEnergy(const IPField* ipf) const{
	double energy = 0.;
	if (g->size()>0){
		IntPt* GP;
		const ipFiniteStrain *vipv[256];
		for (elementGroup::elementContainer::const_iterator it= g->begin(); it!=g->end(); it++){
			MElement* ele= it->second;
			ipf->getIPv(ele,vipv);
			int npts= integBulk->getIntPoints(ele,&GP);
			for (int i=0; i<npts; i++){
				double weight= GP[i].weight;
				double &detJ = vipv[i]->getJacobianDeterminant(ele,GP[i]);
				energy += (vipv[i]->defoEnergy()*detJ*weight);
			};
		};
	}
	return energy;
};
double partDomain::computePlasticEnergy(const IPField* ipf) const{
	double energy = 0.;
	if (g->size()>0){
		IntPt* GP;
		const ipFiniteStrain *vipv[256];
		for (elementGroup::elementContainer::const_iterator it= g->begin(); it!=g->end(); it++){
			MElement* ele= it->second;
			ipf->getIPv(ele,vipv);
			int npts= integBulk->getIntPoints(ele,&GP);
			for (int i=0; i<npts; i++){
				double weight= GP[i].weight;
				double &detJ = vipv[i]->getJacobianDeterminant(ele,GP[i]);
				energy += (vipv[i]->plasticEnergy()*detJ*weight);
			};
		};
	}
	return energy;
};

double partDomain::computeIrreversibleEnergy(const IPField* ipf) const{
	double energy = 0.;
	if (g->size()>0){
		IntPt* GP;
		const ipFiniteStrain *vipv[256];
		for (elementGroup::elementContainer::const_iterator it= g->begin(); it!=g->end(); it++){
			MElement* ele= it->second;
			ipf->getIPv(ele,vipv);
			int npts= integBulk->getIntPoints(ele,&GP);
			for (int i=0; i<npts; i++){
				double weight= GP[i].weight;
				double &detJ = vipv[i]->getJacobianDeterminant(ele,GP[i]);
				energy += (vipv[i]->irreversibleEnergy()*detJ*weight);
			};
		};
	}
	return energy;
};

// restart internal state for saving/loading
void partDomain::restartIPState(AllIPState * aips) const
{
    QuadratureBase * integBulk = this->getBulkGaussIntegrationRule();

    const elementGroup* elems = this->elements();
    for (elementGroup::elementContainer::const_iterator eleIt = elems->begin(); eleIt != elems->end(); ++eleIt)
    {
        MElement * e = eleIt->second;
        AllIPState::ipstateElementContainer *vips = aips->getIPstate(e->getNum());

        // Might have to check for element correctness through either GP, nodes, or something else
        // because remeshing can change element numbers for the same domain
        // with deformed mesh for the domain

        IntPt *GP;
        int npts_bulk=integBulk->getIntPoints(e,&GP);

        for(int j = 0; j < npts_bulk; ++j)
        {
            IPStateBase* ips = (*vips)[j];
            //ips->restart(); OR
            restartManager::restart(ips);
        }
    }
}


dgPartDomain::dgPartDomain(const int tag, const int phys, const int ws,
               const bool fdg, int numNL, int numCED, int numCurlData,bool hasBodyForceForHO,int BFModuliType) : 
               partDomain(tag,phys,ws,fdg,numNL,numCED,numCurlData,hasBodyForceForHO,BFModuliType), 
btermBound(NULL),
ltermBound(NULL),
btermVirtBound(NULL),
ltermVirtBound(NULL),
integBound(NULL),
scalarTermPFBound(NULL),
linearTermPFBound(NULL),
_interByPert(false), 
_virtByPert(false),
_averageStrainBased(false)
{
  gi = new elementGroup();
  gib = new elementGroup();
  giv = new elementGroup();
}
dgPartDomain::dgPartDomain(const dgPartDomain &source) : partDomain(source),
btermBound(NULL),
ltermBound(NULL),
btermVirtBound(NULL),
ltermVirtBound(NULL),
integBound(NULL),
scalarTermPFBound(NULL),
linearTermPFBound(NULL),
_interByPert(source._interByPert), 
_virtByPert(source._virtByPert),
_averageStrainBased(source._averageStrainBased)
{
	gi = new elementGroup();
  gib = new elementGroup();
  giv = new elementGroup();
}

dgPartDomain::~dgPartDomain(){
  if (btermBound) {delete btermBound; btermBound = NULL;};
  if (ltermBound) {delete ltermBound; ltermBound = NULL;};
  if (btermVirtBound) {delete btermVirtBound; btermVirtBound= NULL;};
  if (ltermVirtBound) {delete ltermVirtBound; ltermVirtBound=NULL;};
  if (integBound) {delete integBound; integBound = NULL;};
  if (gi) {delete gi; gi = NULL;};
  if (gib) {delete gib; gib = NULL;};
  if (giv) {delete giv; giv = NULL;};
  if (scalarTermPFBound) {delete scalarTermPFBound; scalarTermPFBound = NULL;}
  if (linearTermPFBound) {delete linearTermPFBound; linearTermPFBound = NULL;};
}

void dgPartDomain::averageStrainBased(const bool flg){_averageStrainBased = flg;};

#if defined(HAVE_MPI)
void dgPartDomain::createIPMap(){
	if (_otherRanks.size() == 0) return; // if no other rank;

	const materialLaw* mlaw = this->getMaterialLaw();
	const materialLaw* mlawMinus = this->getMaterialLawMinus();
	if (mlawMinus== NULL) Msg::Error("getMaterialLawMinus is NULL");
	const materialLaw* mlawPlus = this->getMaterialLawPlus();
	if (mlawPlus== NULL) Msg::Error("getMaterialLawPlus is NULL");

	bool useNumMat = false;
	if (mlaw!=NULL){
		if (mlaw->isNumeric())
			useNumMat = true;
	}
	if (mlawMinus->isNumeric()){
		useNumMat = true;
	}
	if (mlawPlus->isNumeric()){
		useNumMat = true;
	}
	if (!useNumMat) return;
	// initMap is created when at leat one material law (among mlaw, mlawMinus, mlawPlus) is numeric

	if (Msg::GetCommRank() == _rootRank){
		IntPt* GP;

		std::set<int> allIPBulk; // for finding

		for (elementGroup::elementContainer::const_iterator it = this->g->begin(); it != this->g->end(); ++it){
			MElement *ele = it->second;
			int npts_bulk=this->getBulkGaussIntegrationRule()->getIntPoints(ele,&GP);
			for(int i=0;i<npts_bulk;i++){
				int num = numericalMaterialBase::createTypeWithTwoInts(ele->getNum(),i);
				allIPBulk.insert(num);
			}
		}

		// for minus part

		std::set<int> allIPMinus; // for finding
		for(elementGroup::elementContainer::const_iterator it=this->gi->begin(); it!=this->gi->end();++it){
			MElement *ele = it->second;
			int npts=this->getInterfaceGaussIntegrationRule()->getIntPoints(ele,&GP);
			for(int i=0;i<npts;i++){
				int num = numericalMaterialBase::createTypeWithTwoInts(ele->getNum(),i);
				allIPMinus.insert(num);
			}
		}
		// for plus part
		std::set<int> allIPlus; // for finding
		for(elementGroup::elementContainer::const_iterator it=this->gi->begin(); it!=this->gi->end();++it){
			MElement *ele = it->second;
			int npts=this->getInterfaceGaussIntegrationRule()->getIntPoints(ele,&GP);
			for(int i=0;i<npts;i++){
				int num = numericalMaterialBase::createTypeWithTwoInts(ele->getNum(),i+npts);
				allIPlus.insert(num);
			}
		}

		// distribute
		std::vector<int> allIP;
		if (mlaw!=NULL){
			if (mlaw->isNumeric()){
				for (std::set<int>::iterator it = allIPBulk.begin(); it!= allIPBulk.end(); it++){
					allIP.push_back(*it);
				}
			}
		}

		if (mlawMinus->isNumeric()){
			for (std::set<int>::iterator it = allIPMinus.begin(); it!= allIPMinus.end(); it++){
				allIP.push_back(*it);
			}
		}

		if (mlawPlus->isNumeric()){
			if (_averageStrainBased and  (mlawMinus->getNum() == mlawPlus->getNum())){
				// do nothing
			}
			else{
				for (std::set<int>::iterator it = allIPlus.begin(); it!= allIPlus.end(); it++){
					allIP.push_back(*it);
				}
			}
		}

		std::vector<int> allRanks(_otherRanks.begin(),_otherRanks.end());
		if (_distributedOnRoot)
			allRanks.push_back(_rootRank);
		// distribute multiscale IP
		std::map<int,std::set<int> > mapAllIP;
		numericalMaterialBase::distributeMacroIP(allIP,allRanks,mapAllIP);

		_mapIPBulk.clear();
		_mapIPInterfaceMinus.clear();
		_mapIPInterfacePlus.clear();
		for (int ip=0; ip < allRanks.size(); ip++){
			std::set<int> bulkPart;
			std::set<int> minusPart;
			std::set<int> plusPart;
			const int& rank = allRanks[ip];
			const std::set<int>& ipOnRank = mapAllIP[rank];
			for (std::set<int>::const_iterator itIP = ipOnRank.begin(); itIP!= ipOnRank.end(); itIP++){
				const int& num =  *itIP;
				if (allIPBulk.find(num) != allIPBulk.end()){
					bulkPart.insert(num);
				}
				else if (allIPMinus.find(num) != allIPMinus.end()){
					minusPart.insert(num);
				}
				else if (allIPlus.find(num) != allIPlus.end()){
					plusPart.insert(num);
				}
				else{
					Msg::Error("IP %d is not correctly defined");
				}
			}

			_mapIPBulk[rank] = bulkPart;
			_mapIPInterfaceMinus[rank] = minusPart;
			_mapIPInterfacePlus[rank] = plusPart;
		}

		// send data to other Ranks
	 // Send data for bulk elements
		for (std::map<int,std::set<int> >::iterator it = _mapIPBulk.begin(); it!= _mapIPBulk.end(); it++){
			int rank = it->first;
			std::set<int>& numElOnRank = _mapIPBulk[rank];
			// at root rank
			if (rank == _rootRank){
				_domainIPBulk = numElOnRank;
			}
			// at other ranks
			else{
				int bufferSize = numElOnRank.size();
				MPI_Send(&bufferSize,1,MPI_INT,rank,numericalMaterialBase::createTypeWithTwoInts(getPhysical(),0),MPI_COMM_WORLD);
				if (bufferSize>0){
					int* buffer = new int[bufferSize];
					int idex = 0;
					for (std::set<int>::iterator its = numElOnRank.begin(); its != numElOnRank.end(); its++){
						buffer[idex] = *its;
						idex++;
					};
					MPI_Send(buffer,bufferSize,MPI_INT,rank,numericalMaterialBase::createTypeWithTwoInts(getPhysical(),1),MPI_COMM_WORLD);
					delete[] buffer;
				}
			}
		};

		// send data for minus interface IP
		for (std::map<int,std::set<int> >::iterator it = _mapIPInterfaceMinus.begin(); it!= _mapIPInterfaceMinus.end(); it++){
			int rank = it->first;
			std::set<int>& numElOnRank = _mapIPInterfaceMinus[rank];
			// at root rank
			if (rank == _rootRank){
				_domainIPInterfaceMinus = numElOnRank;
			}
			// at other ranks
			else{
				int bufferSize = numElOnRank.size();
				MPI_Send(&bufferSize,1,MPI_INT,rank,numericalMaterialBase::createTypeWithTwoInts(getPhysical(),2),MPI_COMM_WORLD);
				if (bufferSize>0){
					int* buffer = new int[bufferSize];
					int idex = 0;
					for (std::set<int>::iterator its = numElOnRank.begin(); its != numElOnRank.end(); its++){
						buffer[idex] = *its;
						idex++;
					};
					MPI_Send(buffer,bufferSize,MPI_INT,rank,numericalMaterialBase::createTypeWithTwoInts(getPhysical(),3),MPI_COMM_WORLD);
					delete[] buffer;
				}
			}
		};

		// send data for plus interface IP
		for (std::map<int,std::set<int> >::iterator it = _mapIPInterfacePlus.begin(); it!= _mapIPInterfacePlus.end(); it++){
			int rank = it->first;
			std::set<int>& numElOnRank = _mapIPInterfacePlus[rank];
			// at root rank
			if (rank == _rootRank){
				_domainIPInterfacePlus = numElOnRank;
			}
			// at other ranks
			else{
				int bufferSize = numElOnRank.size();
				MPI_Send(&bufferSize,1,MPI_INT,rank,numericalMaterialBase::createTypeWithTwoInts(getPhysical(),4),MPI_COMM_WORLD);
				if (bufferSize>0){
					int* buffer = new int[bufferSize];
					int idex = 0;
					for (std::set<int>::iterator its = numElOnRank.begin(); its != numElOnRank.end(); its++){
						buffer[idex] = *its;
						idex++;
					};
					MPI_Send(buffer,bufferSize,MPI_INT,rank,numericalMaterialBase::createTypeWithTwoInts(getPhysical(),5),MPI_COMM_WORLD);
					delete[] buffer;
				}
			}
		};

		// add to root rank if material is not multiscale
		if (mlaw!=NULL){
			if (!mlaw->isNumeric()){
				for (std::set<int>::iterator it = allIPBulk.begin(); it!= allIPBulk.end(); it++){
					_domainIPBulk.insert(*it);
				}
			}
		}

		if (!mlawMinus->isNumeric()){
			for (std::set<int>::iterator it = allIPMinus.begin(); it!= allIPMinus.end(); it++){
				_domainIPInterfaceMinus.insert(*it);
			}
		}

		if (!mlawPlus->isNumeric()){
			if (_averageStrainBased and  (mlawMinus->getNum() == mlawPlus->getNum())){
				// do nothing
			}
			else{
				for (std::set<int>::iterator it = allIPlus.begin(); it!= allIPlus.end(); it++){
					_domainIPInterfacePlus.insert(*it);
				}
			}
		}

	}
	else if (_otherRanks.find(Msg::GetCommRank()) != _otherRanks.end()) {
		// receive bulk part
		// receive data from root for bulk elements
		MPI_Status status;
		int bufferSize;
		int* buffer = NULL;

		MPI_Recv(&bufferSize,1,MPI_INT,_rootRank,numericalMaterialBase::createTypeWithTwoInts(getPhysical(),0),MPI_COMM_WORLD,&status);
		if (bufferSize>0){
			buffer = new int[bufferSize];
			MPI_Recv(buffer,bufferSize,MPI_INT,_rootRank,numericalMaterialBase::createTypeWithTwoInts(getPhysical(),1),MPI_COMM_WORLD,&status);
			_domainIPBulk.clear();
			for (int i=0; i<bufferSize; i++)
				_domainIPBulk.insert(buffer[i]);
			delete[] buffer;
		}

		// receive minus part
		MPI_Recv(&bufferSize,1,MPI_INT,_rootRank,numericalMaterialBase::createTypeWithTwoInts(getPhysical(),2),MPI_COMM_WORLD,&status);
		if (bufferSize>0){
			buffer = new int[bufferSize];
			MPI_Recv(buffer,bufferSize,MPI_INT,_rootRank,numericalMaterialBase::createTypeWithTwoInts(getPhysical(),3),MPI_COMM_WORLD,&status);
			_domainIPInterfaceMinus.clear();
			for (int i=0; i<bufferSize; i++)
				_domainIPInterfaceMinus.insert(buffer[i]);
			delete[] buffer;
		}

		// receive plus part
		MPI_Recv(&bufferSize,1,MPI_INT,_rootRank,numericalMaterialBase::createTypeWithTwoInts(getPhysical(),4),MPI_COMM_WORLD,&status);
		if (bufferSize>0){
			buffer = new int[bufferSize];
			MPI_Recv(buffer,bufferSize,MPI_INT,_rootRank,numericalMaterialBase::createTypeWithTwoInts(getPhysical(),5),MPI_COMM_WORLD,&status);
			_domainIPInterfacePlus.clear();
			for (int i=0; i<bufferSize; i++)
				_domainIPInterfacePlus.insert(buffer[i]);
			delete[] buffer;
		}

	}
};
#endif // HAVE_MPI

void dgPartDomain::createIPState(AllIPState::ipstateContainer& map, const bool *state) const{
  partDomain::createIPState(map,state);
  IntPt* GP;
  const materialLaw *mlawMinus = this->getMaterialLawMinus();
  const materialLaw *mlawPlus = this->getMaterialLawPlus();

	#if defined(HAVE_MPI)
  if ((mlawMinus->isNumeric() or mlawPlus->isNumeric()) and _otherRanks.size()>0){
    if (Msg::GetCommRank() == _rootRank){
      // loop
      for(elementGroup::elementContainer::const_iterator it=this->gi->begin(); it!=this->gi->end();++it){
        MElement *ele = it->second;
        MInterfaceElement* iele = dynamic_cast<MInterfaceElement*>(ele);
      // 2* because IP is duplicated for DG formulation
        int npts = this->getInterfaceGaussIntegrationRule()->getIntPoints(ele,&GP);
        int npts_inter=2*npts;
        AllIPState::ipstateElementContainer tp(npts_inter);
        for(int i=0;i<npts;i++){
          if (mlawMinus->isNumeric()){
            int num = numericalMaterialBase::createTypeWithTwoInts(ele->getNum(),i);
            if (_domainIPInterfaceMinus.find(num) != _domainIPInterfaceMinus.end()){
              mlawMinus->createIPState(tp[i],_hasBodyForceForHO,state,ele,iele->getElem(0)->getNumVertices(),&(GP[i]),i);
            }
            else{
              const numericalMaterialBase* numatMinus = dynamic_cast<const numericalMaterialBase*>(mlawMinus);
              if (numatMinus)
                numatMinus->createIPState(false,tp[i],_hasBodyForceForHO,state,ele,iele->getElem(0)->getNumVertices(),&(GP[i]),i);
              else
                Msg::Error("Error error at creating the negative IPState");
            }
          }
          else{
            mlawMinus->createIPState(tp[i],_hasBodyForceForHO, state,ele,iele->getElem(0)->getNumVertices(),&(GP[i]),i);
          }
          tp[i]->setLocation(IPVariable::INTERFACE_MINUS);
        }
        for(int i=npts;i<npts_inter;i++){
          if (mlawPlus->isNumeric()){
            int num = numericalMaterialBase::createTypeWithTwoInts(ele->getNum(),i);
            if (_domainIPInterfacePlus.find(num) != _domainIPInterfacePlus.end()){
              mlawPlus->createIPState(tp[i],_hasBodyForceForHO, state,ele,iele->getElem(1)->getNumVertices(),&(GP[i-npts]),i);
            }
            else{
              const numericalMaterialBase* numatPlus = dynamic_cast<const numericalMaterialBase*>(mlawPlus);
              if (numatPlus)
                numatPlus->createIPState(false,tp[i],_hasBodyForceForHO,state,ele,iele->getElem(1)->getNumVertices(),&(GP[i-npts]),i);
              else
                Msg::Error("Error error at creating the positive IPState");
            }
          }
          else{
            mlawPlus->createIPState(tp[i],_hasBodyForceForHO,state,ele,iele->getElem(1)->getNumVertices(),&(GP[i-npts]),i);
          }

          tp[i]->setLocation(IPVariable::INTERFACE_PLUS);
        }
        map.insert(AllIPState::ipstatePairType(ele->getNum(),tp));
      }
    }
    else if (_otherRanks.find(Msg::GetCommRank()) != _otherRanks.end()) {
      for (std::set<int>::iterator it = _domainIPInterfaceMinus.begin(); it != _domainIPInterfaceMinus.end(); it++){
        int num = *it;
        int elnum, gnum;
        IntPt *GPnum;
        numericalMaterialBase::getTwoIntsFromType(num,elnum,gnum);
        MElement* ele = new MInterfacePoint(NULL,elnum,0);
        AllIPState::ipstateElementContainer tp(1);
        int npts_int=this->getInterfaceGaussIntegrationRule()->getIntPoints(ele,&GPnum);
        mlawMinus->createIPState(tp[0],_hasBodyForceForHO,state,ele,ele->getNumVertices(),&(GPnum[0]),gnum);
        tp[0]->setLocation(IPVariable::INTERFACE_MINUS);
        delete ele;
        map.insert(AllIPState::ipstatePairType(num,tp));
      };

      for (std::set<int>::iterator it = _domainIPInterfacePlus.begin(); it != _domainIPInterfacePlus.end(); it++){
        int num = *it;
        int elnum, gnum;
        IntPt *GPnum;
        numericalMaterialBase::getTwoIntsFromType(num,elnum,gnum);
        MElement* ele = new MInterfacePoint(NULL,elnum,0);
        AllIPState::ipstateElementContainer tp(1);
        int npts_int=this->getInterfaceGaussIntegrationRule()->getIntPoints(ele,&GPnum);
        mlawPlus->createIPState(tp[0],_hasBodyForceForHO,state,ele,ele->getNumVertices(),&(GPnum[0]),gnum);
        tp[0]->setLocation(IPVariable::INTERFACE_PLUS);
        delete ele;
        map.insert(AllIPState::ipstatePairType(num,tp));
      };
    }
  }
  else
	#endif //HAVE_MPI
	{
    // loop
    for(elementGroup::elementContainer::const_iterator it=this->gi->begin(); it!=this->gi->end();++it){
      MElement *ele = it->second;
      MInterfaceElement* iele = dynamic_cast<MInterfaceElement*>(ele);
    // 2* because IP is duplicated for DG formulation
      int npts = this->getInterfaceGaussIntegrationRule()->getIntPoints(ele,&GP);
      int npts_inter=2* npts;
      AllIPState::ipstateElementContainer tp(npts_inter);
      for(int i=0;i<npts;i++){
        mlawMinus->createIPState(tp[i],_hasBodyForceForHO, state,ele,iele->getElem(0)->getNumVertices(),&(GP[i]),i);
        tp[i]->setLocation(IPVariable::INTERFACE_MINUS);
      }
      for(int i=npts;i<npts_inter;i++){
        mlawPlus->createIPState(tp[i],_hasBodyForceForHO,state,ele,iele->getElem(1)->getNumVertices(),&(GP[i-npts]),i);
        tp[i]->setLocation(IPVariable::INTERFACE_PLUS);
      }
      map.insert(AllIPState::ipstatePairType(ele->getNum(),tp));
    }
  }


  // Virtual interface element (no duplication)
  const materialLaw *mlaw = this->getMaterialLaw();
  for(elementGroup::elementContainer::const_iterator it=this->gib->begin(); it!=this->gib->end();++it){
    MElement *ele = it->second;
    MInterfaceElement *iele = dynamic_cast<MInterfaceElement*>(ele);
    int npts_inter=this->getInterfaceGaussIntegrationRule()->getIntPoints(ele,&GP);
    AllIPState::ipstateElementContainer tp(npts_inter);
    for(int i=0;i<npts_inter;i++){
      mlaw->createIPState(tp[i],_hasBodyForceForHO, state,ele,iele->getElem(0)->getNumVertices(),&(GP[i]),i);
      tp[i]->setLocation(IPVariable::VIRTUAL_INTERFACE);
    }
    map.insert(AllIPState::ipstatePairType(ele->getNum(),tp));
  }
};

void dgPartDomain::numberValuesToTransfertMPI(AllIPState *aips,int* sizeeachrank)const
{
 #if defined(HAVE_MPI)
  IntPt *GP;
  for(elementGroup::elementContainer::const_iterator ite=gi->begin(); ite!=gi->end(); ++ite)
  {
    MElement* ele = ite->second;
    MInterfaceElement *iele = dynamic_cast<MInterfaceElement*>(ele);
    int rankminus = iele->getElem(0)->getPartition()-1;
    int rankplus = iele->getElem(1)->getPartition()-1;
    int rankcomm=-1;
    if(rankminus != Msg::GetCommRank())
    {
       rankcomm = rankminus;
    }
    else if(rankplus !=Msg::GetCommRank())
    {
      rankcomm = rankplus;
    }
    if(rankcomm!=-1)
    {
      int npts=integBound->getIntPoints(ite->second,&GP);
      AllIPState::ipstateElementContainer *vips = aips->getIPstate(ele->getNum());
      int nvc=0;
      for(int j=0;j<npts;j++)
      {
         IPStateBase* ipsm = (*vips)[j];
         // take initial but supposed same value to transfert for all (so transfert only once)
         IPVariable *ipvm = static_cast<IPVariableMechanics*>(ipsm->getState(IPStateBase::initial));
         //Msg::Error("numberValuesToTransfertMPI %d",ipvm->numberValuesToCommunicateMPI());
         nvc += ipvm->numberValuesToCommunicateMPI();
      }
      if(nvc!=0) // otherwise no value to communicate
        sizeeachrank[rankcomm] += (2+nvc); // as the numbers of element minus and plus have to be communicate
    }
  }
 #endif // HAVE_MPI
}

void dgPartDomain::initiallyBreakAllInterfaceIP(AllIPState *aips) const
{
  const materialLaw* mlawMinus = this->getMaterialLawMinus();
  const materialLaw* mlawPlus = this->getMaterialLawPlus();
  const materialLaw2LawsInitializer* fracMinus = dynamic_cast<const materialLaw2LawsInitializer*>(mlawMinus);
  const materialLaw2LawsInitializer* fracPlus = dynamic_cast<const materialLaw2LawsInitializer*>(mlawPlus);
  #if defined(HAVE_MPI)
  if (Msg::GetCommRank() == _rootRank)
  #endif // HAVE_MPI
  {
    IntPt* GP;
    for (elementGroup::elementContainer::const_iterator it = gi->begin(); it!= gi->end(); it++){
      MElement* ele = it->second;

      AllIPState::ipstateElementContainer *vips = aips->getIPstate(ele->getNum());
      int npts=this->getInterfaceGaussIntegrationRule()->getIntPoints(ele,&GP);

      for (int j=0; j< npts; j++){
        IPStateBase* ipsm = (*vips)[j];
        IPStateBase* ipsp = (*vips)[j+npts];
        if (fracMinus!=NULL){
          fracMinus->initialBroken(ipsm);
        }
        if (fracPlus!=NULL){
          fracPlus->initialBroken(ipsp);
        }
      }
    }
  }
  #if defined(HAVE_MPI)
  else if (_otherRanks.find(Msg::GetCommRank()) != _otherRanks.end())
  {
    if ((mlawMinus->isNumeric()) and (fracMinus!=NULL)){
      for (std::set<int>::const_iterator it = _domainIPInterfaceMinus.begin(); it!= _domainIPInterfaceMinus.end(); it++){
        AllIPState::ipstateElementContainer *vips = aips->getIPstate(*it);
        fracMinus->initialBroken((*vips)[0]);
      }
    }

    if ((mlawPlus->isNumeric()) and (fracPlus!=NULL)){
      for (std::set<int>::const_iterator it = _domainIPInterfacePlus.begin(); it!= _domainIPInterfacePlus.end(); it++){
        AllIPState::ipstateElementContainer *vips = aips->getIPstate(*it);
        fracPlus->initialBroken((*vips)[0]);
      }
    }

  }
  #endif // HAVE_MPI
};

void dgPartDomain::fillArrayIPvariableMPI(AllIPState *aips,const int rank,double* arrayMPI) const
{
 #if defined(HAVE_MPI)
  // We communicate also the number of each element (to identify later the interface)
  // so by element the number of value to tranfert is equal to 2 + npts
  // Only the value of minus element is transfert after in init term val + = val -
  IntPt *GP;
  int curpos=0;
  for(elementGroup::elementContainer::const_iterator ite=gi->begin(); ite!=gi->end(); ++ite)
  {
    MElement* ele = ite->second;
    MInterfaceElement *iele = dynamic_cast<MInterfaceElement*>(ele);
    int rankminus = iele->getElem(0)->getPartition()-1;
    int rankplus = iele->getElem(1)->getPartition()-1;
    if((rankminus == rank) or (rankplus == rank))
    {
      // write information about the interface element (the interface element number is local --> different on each rank)
      arrayMPI[curpos] = (double) iele->getElem(0)->getNum();
      arrayMPI[curpos+1] = (double) iele->getElem(1)->getNum();
      // get the value of the cohesive strength on minus element of each integration point
      // same for initial, previous and current.
      int npts=integBound->getIntPoints(ite->second,&GP);
      AllIPState::ipstateElementContainer *vips = aips->getIPstate(ele->getNum());
      curpos +=2;
      for(int i=0;i<npts;i++)
      {
         IPStateBase* ipsm = (*vips)[i];
         IPVariable  *ipvm = ipsm->getState(IPStateBase::initial);
         ipvm->fillValuesToCommunicateMPI(&arrayMPI[curpos]);
         curpos+=ipvm->numberValuesToCommunicateMPI();
      }
    }
  }
 #endif // HAVE_MPI
}

void dgPartDomain::setIPVariableFromArrayMPI(AllIPState *aips,const int rank,const int arraysize,const double* arrayMPI)const
{
 #if defined(HAVE_MPI)
  int curpos=0;
  IntPt *GP;
  while(curpos<arraysize)
  {
     // get information to find the interelement (the number of the interface element is a local number which differ on each rank)
    int numminus = (int) arrayMPI[curpos];
    int numplus = (int) arrayMPI[curpos+1];
    MElement* elefound=NULL;
    for(elementGroup::elementContainer::const_iterator it=gi->begin(); it!=gi->end();++it)
    {
      MInterfaceElement *ie = dynamic_cast<MInterfaceElement*>(it->second);
      if((ie->getElem(0)->getNum()==numminus) and (ie->getElem(1)->getNum()==numplus))
      {
        elefound = it->second;
        break;
      }
    }
    if(elefound!=NULL){
      int npts=integBound->getIntPoints(elefound,&GP);
      AllIPState::ipstateElementContainer *vips = aips->getIPstate(elefound->getNum());
      curpos+=2;
      for(int i=0;i<npts;i++)
      {
         IPStateBase* ipsm = (*vips)[i];
         // same values for the 3 states
         IPVariable *ipvm = ipsm->getState(IPStateBase::initial);
         ipvm->getValuesFromMPI(&arrayMPI[curpos]);
         ipvm = ipsm->getState(IPStateBase::previous);
         ipvm->getValuesFromMPI(&arrayMPI[curpos]);
         ipvm = ipsm->getState(IPStateBase::current);
         ipvm->getValuesFromMPI(&arrayMPI[curpos]);
         curpos += ipvm->numberValuesToCommunicateMPI();
      }
    }
    else
    {
      Msg::Error("Can't find an interelement where IPVariable have to be set. dom %d rank %d",_phys,Msg::GetCommRank());
      Msg::Exit(0);
    }
  }
 #endif // HAVE_MPI
}

void dgPartDomain::initMicroMeshId(){
  // for bulk element
  materialLaw* mlaw = this->getMaterialLaw();
  if(mlaw!=NULL)
  {
    if (mlaw->isNumeric()){
      partDomain::initMicroMeshId();
    }
  }

  if (IsInterfaceTerms()){
    // for interface elements
    materialLaw* mlawMinus = this->getMaterialLawMinus();
    materialLaw* mlawPlus = this->getMaterialLawPlus();

    if (mlawPlus->isNumeric() or mlawMinus->isNumeric()){
      numericalMaterialBase* nummatMinus = dynamic_cast<numericalMaterialBase*>(mlawMinus);
      numericalMaterialBase* nummatPlus = dynamic_cast<numericalMaterialBase*>(mlawPlus);
      if ((nummatMinus == NULL) and (nummatPlus == NULL))
        Msg::Error("material law must be derived from numericalMaterialBase");

      IntPt* GP;
      for (elementGroup::elementContainer::const_iterator it = gi->begin(); it!= gi->end(); it++){
        MElement* ele = it->second;
        int npts = this->getInterfaceGaussIntegrationRule()->getIntPoints(ele,&GP);
        for (int i=0; i<npts; i++){
          if (mlawMinus->isNumeric() == true && mlawPlus->isNumeric() == false ){
            nummatMinus->assignMeshId(ele->getNum(),i);
          }
          else if (mlawMinus->isNumeric() == false && mlawPlus->isNumeric() ==true){
            nummatPlus->assignMeshId(ele->getNum(),i+npts);
          }
          else {
            #if defined(HAVE_MPI)
            MInterfaceElement* ie =dynamic_cast<MInterfaceElement*>(ele);
            int posProc = ie->getElem(1)->getPartition()-1;
            int negProc = ie->getElem(0)->getPartition()-1;
            if (posProc == negProc){
            #endif // HAVE_MPI
              if (mlawMinus->getNum() == mlawPlus->getNum()){
                // same material law
                nummatMinus->assignMeshId(ele->getNum(),i);
                nummatPlus->setMeshId(ele->getNum(),i+npts,nummatMinus->getMeshId(ele->getNum(),i));
              }
              else{
                // different material law
                nummatMinus->assignMeshId(ele->getNum(),i);
                nummatPlus->assignMeshId(ele->getNum(),i+npts);
              }

            #if defined(HAVE_MPI)
            }
            else{
              // id initiate in lower proc and send to upper proc
              int smaller = std::min(posProc,negProc);
              int larger = std::max(posProc,negProc);

              if (Msg::GetCommRank() == smaller){
                if (mlawMinus->getNum() == mlawPlus->getNum()){
                  // same material law
                  nummatMinus->assignMeshId(ele->getNum(),i);
                  nummatPlus->setMeshId(ele->getNum(),i+npts,nummatMinus->getMeshId(ele->getNum(),i));
                }
                else{
                  // different material law
                  nummatMinus->assignMeshId(ele->getNum(),i);
                  nummatPlus->assignMeshId(ele->getNum(),i+npts);
                }
                // send data
                int bufferMinus = nummatMinus->getMeshId(ele->getNum(),i);
                int tagMinus = numericalMaterialBase::createTypeWithTwoInts(ie->getElem(0)->getNum(),i);

                int tagPlus = numericalMaterialBase::createTypeWithTwoInts(ie->getElem(1)->getNum(),i+npts);
                int bufferPlus  = nummatPlus->getMeshId(ele->getNum(),i+npts);

                MPI_Send(&bufferMinus,1,MPI_INT,larger,tagMinus,MPI_COMM_WORLD);
                MPI_Send(&bufferPlus,1,MPI_INT,larger,tagPlus,MPI_COMM_WORLD);
              }
              else if (Msg::GetCommRank() == larger){
                 // receive data
                int bufferMinus, bufferPlus;
                int tagMinus = numericalMaterialBase::createTypeWithTwoInts(ie->getElem(0)->getNum(),i);
                int tagPlus = numericalMaterialBase::createTypeWithTwoInts(ie->getElem(1)->getNum(),i+npts);

                MPI_Status status;
                MPI_Recv(&bufferMinus,1,MPI_INT,smaller,tagMinus,MPI_COMM_WORLD,&status);
                MPI_Recv(&bufferPlus,1,MPI_INT,smaller,tagPlus,MPI_COMM_WORLD,&status);

                nummatMinus->setMeshId(ele->getNum(),i,bufferMinus);
                nummatPlus->setMeshId(ele->getNum(),i+npts,bufferPlus);
              }
            }

            #endif // HAVE_MPI
          }
        }
      }
    };
  }
};


void QuadratureFactory::createBulkGaussQuadrature(MElement* e, QuadratureBase*& integBulk)
{
  if(e->getTypeForMSH() == MSH_TET_4)
  {
    integBulk = new GaussQuadrature(2); // full integration
  }
  else if(e->getTypeForMSH() == MSH_TET_10)
  {
    integBulk = new GaussQuadrature(2); // under integration
  }
  else if(e->getTypeForMSH() == MSH_HEX_27)
  {
    integBulk = new GaussQuadrature(4); // full integration
  }
  else if(e->getTypeForMSH() == MSH_HEX_8)
  {
    integBulk = new GaussQuadrature(3); // full integration
  }
  else if (e->getTypeForMSH() == MSH_QUA_9)
  {
    integBulk = new GaussQuadrature(4);
  }
  else if (e->getTypeForMSH() == MSH_QUA_8)
  {
    integBulk = new GaussQuadrature(4);
  }
  else if (e->getTypeForMSH() == MSH_QUA_12)
  {
    integBulk = new GaussQuadrature(4);
  }
  else if (e->getTypeForMSH() == MSH_QUA_4)
  {
    integBulk = new GaussQuadrature(3);
  }
  else if (e->getTypeForMSH() == MSH_TRI_6)
  {
    integBulk = new GaussQuadrature(2); // 3GPs
  }
  else if (e->getTypeForMSH() == MSH_TRI_3)
  {
   // if (this->getNumNonLocalVariable() == 0)
   //   integBulk = new GaussQuadrature(1);
   // else{
      integBulk = new GaussQuadrature(2);
   // }
  }
  else if(e->getTypeForMSH() == MSH_TET_20)
  {
    integBulk = new GaussQuadrature(4);
  }
  else if (e->getTypeForMSH() == MSH_PRI_6)
  {
    integBulk = new GaussQuadrature(2);
  }
  else if (e->getTypeForMSH() == MSH_PRI_18)
  {
    integBulk = new GaussQuadrature(4);
  }
  else if (e->getTypeForMSH() == MSH_LIN_2)
  {
    integBulk = new GaussQuadrature(2);
  }
  else if (e->getTypeForMSH() == MSH_LIN_3)
  {
    integBulk = new GaussQuadrature(5);
  }
  else
  {
    Msg::Error("Bulk gauss quadrature is not defined for this element type by default");
  }
};

void QuadratureFactory::createBulkGaussQuadratureHierarchicalFE(int order, MElement* e, QuadratureBase*& integBulk)
{
  if (e->getType() == TYPE_PNT)
  {
    integBulk = new GaussQuadrature(2); // full integration
  }
  else if (e->getType() == TYPE_LIN)
  {
    if (order == 1)
    {
      integBulk = new GaussQuadrature(2);
    }
    else if (order == 2)
    {
      integBulk = new GaussQuadrature(5);
    }
    else
    {
      int integrationOrder = 3 * (order - 1) + 1;
      integBulk = new GaussQuadrature(integrationOrder);
    }
  }
  else if (e->getType() == TYPE_TRI)
  {
    if (order == 1)
    {
      integBulk = new GaussQuadrature(2);
    }
    else if (order == 2)
    {
      integBulk = new GaussQuadrature(2); // 3GPs
    }
    else
    {
      int integrationOrder = 3 * (order - 1) + 1;
      integBulk = new GaussQuadrature(integrationOrder);
    }
    
  }
  else if (e->getType() == TYPE_TET)
  {
    if (order == 1)
    {
      integBulk = new GaussQuadrature(2); // full integration
    }
    else if (order == 2)
    {
      integBulk = new GaussQuadrature(2); // under integration
    }
    else if (order = 3)
    {
      integBulk = new GaussQuadrature(4);
    }
    else
    {
      int integrationOrder = 3 * (order - 1) + 1;
      integBulk = new GaussQuadrature(integrationOrder);
    }
    
  }
  else if (e->getType() == TYPE_QUA)
  {
    if (order == 1)
    {
      integBulk = new GaussQuadrature(3);
    }
    else if (order == 2)
    {
      integBulk = new GaussQuadrature(4);
    }
    else if (order == 3)
    {
      integBulk = new GaussQuadrature(4);
    }
    else
    {
      int integrationOrder = 3 * (order - 1) + 1;
      integBulk = new GaussQuadrature(integrationOrder);
    }
  }
  else if (e->getType() == TYPE_HEX)
  {
    if (order == 1)
    {
      integBulk = new GaussQuadrature(3);
    }
    else if (order == 2)
    {
      integBulk = new GaussQuadrature(4);
    }
    else
    {
      int integrationOrder = 3 * (order - 1) + 1;
      integBulk = new GaussQuadrature(integrationOrder);
    }
    
  }
  else if (e->getType() == TYPE_PRI)
  {
    if (order == 1)
    {
      integBulk = new GaussQuadrature(2);
    }
    else if (order == 2)
    {
      integBulk = new GaussQuadrature(4);
    }
    else
    {
      int integrationOrder = 3 * (order - 1) + 1;
      integBulk = new GaussQuadrature(integrationOrder);
    }
  }
  else
  {
    Msg::Error("Bulk gauss quadrature is not defined for this element type by default");
  }
};


void QuadratureFactory::createInterfaceGaussQuadrature(MElement* ie, QuadratureBase* & integBound, interfaceQuadratureBase*& interQuad)
{
  if (ie->getTypeForMSH() == MSH_TRI_3)
  {
    integBound = new GaussQuadrature(2); // 3 Gauss points
    interQuad = new interface2DQuadrature(2,false);  // same number of Gauss point
  }
  else if (ie->getTypeForMSH() == MSH_TRI_6)
  {
    integBound = new GaussQuadrature(4);
    interQuad = new interface2DQuadrature(4,false);  // same number of Gauss point Gauss & Lobato = 2 ??
  }
  else if (ie->getTypeForMSH() == MSH_QUA_9)
  {
    integBound = new GaussQuadrature(4);
    interQuad = new interface2DQuadrature(4,false);  // same number of Gauss point Gauss
  }
  else if (ie->getTypeForMSH() == MSH_QUA_4)
  {
    integBound = new GaussQuadrature(3);
    interQuad = new interface2DQuadrature(3,false);  // same number of Gauss point Gauss
  }
  else if (ie->getTypeForMSH() == MSH_LIN_4)
  {
    integBound = new GaussQuadrature(7); // 4 pts
    interQuad = new interface1DQuadrature(7);
  }
  else if (ie->getTypeForMSH() == MSH_LIN_3)
  {
    integBound = new GaussQuadrature(5); // 3 pts
    interQuad = new interface1DQuadrature(5);
  }
  else if (ie->getTypeForMSH() == MSH_LIN_2)
  {
    integBound = new GaussQuadrature(3); // 2 pts
    interQuad = new interface1DQuadrature(3);
  }
  else if (ie->getTypeForMSH() == MSH_TRI_10)
  {
    integBound = new GaussQuadrature(7);
    interQuad = new interface2DQuadrature(7,false);
  }
  else if (ie->getTypeForMSH() == MSH_PNT)
  {
    integBound = new GaussQuadrature(1);
    interQuad = new interface1DQuadrature(1);
  }
  else
  {
    Msg::Error("Interface gauss quadrature is not defined for this element type by default");
  }
};

void QuadratureFactory::createInterfaceLobattoQuadrature(MElement* ie, QuadratureBase* & integBound, interfaceQuadratureBase*& interQuad)
{
  if (ie->getTypeForMSH() == MSH_TRI_3)
  {
    integBound = new GaussLobatto2DQuadrature(2); //order 2
    interQuad = new interface2DQuadrature(2,true);
  }
  else if (ie->getTypeForMSH() == MSH_TRI_6)
  {
    integBound = new GaussLobatto2DQuadrature(2); //order 2
    interQuad = new interface2DQuadrature(2,true);  // same number of Gauss point Gauss & Lobato = 2 ??
  }
  else if (ie->getTypeForMSH() == MSH_LIN_2)
  {
    integBound = new GaussLobatto2DQuadrature(5); 
    interQuad = new interface1DQuadrature(5,true);  
  }
  else if (ie->getTypeForMSH() == MSH_LIN_3)
  {
    integBound = new GaussLobatto2DQuadrature(5);
    interQuad = new interface1DQuadrature(5,true);  
  }
  else if (ie->getTypeForMSH() == MSH_QUA_9)
  {
    integBound = new GaussLobatto2DQuadrature(2); //order 2
    interQuad = new interface2DQuadrature(2,true);  // same number of Gauss point Gauss
  }
  else if (ie->getTypeForMSH() == MSH_QUA_4)
  {
    integBound = new GaussLobatto2DQuadrature(2); //order 2
    interQuad = new interface2DQuadrature(2,true);
  }
  else if (ie->getTypeForMSH() == MSH_TRI_10)
  {
    integBound = new GaussLobatto2DQuadrature(3); //order 2
    interQuad = new interface2DQuadrature(3,true);
  }
  else if (ie->getTypeForMSH() == MSH_PNT)
  {
    integBound = new GaussQuadrature(1);
    interQuad = new interface1DQuadrature(1);
  }
  else 
  {
    Msg::Error("Interface lobatto quadrature is not defined for this element type by default");
  }
  
};

void QuadratureFactory::createInterfaceGaussQuadratureHierarchicalFE(int order, MElement* ie, QuadratureBase* & integBound, interfaceQuadratureBase*& interQuad)
{
  if (ie->getType() == TYPE_PNT)
  {
    integBound = new GaussQuadrature(1); 
    interQuad = new interface1DQuadrature(1);
  }
  else if (ie->getType() == TYPE_LIN)
  {
    if (order == 1)
    {
      integBound = new GaussQuadrature(3); // 2 pts
      interQuad = new interface1DQuadrature(3);
    }
    else if (order == 2)
    {
      integBound = new GaussQuadrature(5); // 3 pts
      interQuad = new interface1DQuadrature(5);
    }
    else
    {
      int integrationOrder = 3 * (order - 1) + 1;
      integBound = new GaussQuadrature(integrationOrder);
      interQuad = new interface1DQuadrature(integrationOrder); 
    }
  }
  else if (ie->getType() == TYPE_TRI)
  {
    if (order == 1)
    {
      integBound = new GaussQuadrature(2); // 3 Gauss points
      interQuad = new interface2DQuadrature(2,false);  // same number of Gauss point
    }
    else if (order == 2)
    {
      integBound = new GaussQuadrature(4);
      interQuad = new interface2DQuadrature(4,false);  // same number of Gauss point Gauss & Lobato = 2 ??
    }
    else
    {
      int integrationOrder = 3 * (order - 1) + 1;
      integBound = new GaussQuadrature(integrationOrder);
      interQuad = new interface2DQuadrature(integrationOrder,false); 
    }
    
  }
  else if (ie->getType() == TYPE_QUA)
  {
    if (order == 1)
    {
      integBound = new GaussQuadrature(3);
      interQuad = new interface2DQuadrature(3,false);  // same number of Gauss point Gauss
    }
    else if (order == 2)
    {
      integBound = new GaussQuadrature(4);
      interQuad = new interface2DQuadrature(4,false);  // same number of Gauss point Gauss
    }
    else
    {
      int integrationOrder = 3 * (order - 1) + 1;
      integBound = new GaussQuadrature(integrationOrder);
      interQuad = new interface2DQuadrature(integrationOrder,false); 
    }
  }
  else
  {
    Msg::Error("interface gauss quadrature is not defined for this element type by default");
  }
  
};
void QuadratureFactory::createInterfaceLobattoQuadratureHierarchicalFE(int order, MElement* ie, QuadratureBase* & integBound, interfaceQuadratureBase*& interQuad)
{
  if (ie->getType() == TYPE_TRI)
  {
    if (order == 1)
    {
      integBound = new GaussLobatto2DQuadrature(2); //order 2
      interQuad = new interface2DQuadrature(2,true);
    }
    else if (order == 2)
    {
      integBound = new GaussLobatto2DQuadrature(2); //order 2
      interQuad = new interface2DQuadrature(2,true);  // same number of Gauss point Gauss & Lobato = 2 ??
    }
    else if (order == 3)
    {
      integBound = new GaussLobatto2DQuadrature(3); //order 2
      interQuad = new interface2DQuadrature(3,true);
    }
    else
    {
      int integrationOrder = 3 * (order - 1) + 1;
      integBound = new GaussQuadrature(integrationOrder);
      interQuad = new interface2DQuadrature(integrationOrder,true); 
    }
    
  }
  else if (ie->getType() == TYPE_QUA)
  {
    if (order == 1)
    {
      integBound = new GaussLobatto2DQuadrature(2); //order 2
      interQuad = new interface2DQuadrature(2,true);
    }
    else if (order == 2)
    {
      integBound = new GaussLobatto2DQuadrature(2); //order 2
      interQuad = new interface2DQuadrature(2,true);
    }
    else
    {
      int integrationOrder = 3 * (order - 1) + 1;
      integBound = new GaussQuadrature(integrationOrder);
      interQuad = new interface2DQuadrature(integrationOrder,true); 
    }
  }
  else
  {
    Msg::Error("interface lobatto quadrature is not defined for this element type by default");
  }
};

void QuadratureFactory::createGaussQuadratureForNeumannBC(MElement* ie, QuadratureBase*& integ)
{
  if (ie->getTypeForMSH() == MSH_TRI_3)
  {
    integ = new GaussQuadrature(2); // 3 Gauss points
  }
  else if (ie->getTypeForMSH() == MSH_TRI_6)
  {
    integ = new GaussQuadrature(4);
  }
  else if (ie->getTypeForMSH() == MSH_QUA_9)
  {
    integ = new GaussQuadrature(4);
  }
  else if (ie->getTypeForMSH() == MSH_QUA_4)
  {
    integ = new GaussQuadrature(3);
  }
  else if (ie->getTypeForMSH() == MSH_LIN_4)
  {
    integ = new GaussQuadrature(7); // 4 pts
  }
  else if (ie->getTypeForMSH() == MSH_LIN_3)
  {
    integ = new GaussQuadrature(5); // 3 pts
  }
  else if (ie->getTypeForMSH() == MSH_LIN_2)
  {
    integ = new GaussQuadrature(3); // 2 pts
  }
  else if (ie->getTypeForMSH() == MSH_TRI_10)
  {
    integ = new GaussQuadrature(7);
  }
  else if (ie->getTypeForMSH() == MSH_PNT)
  {
    integ = new GaussQuadrature(1);
  }
  else
  {
    Msg::Error("gauss quadrature for NeumannBC is not defined by default");
  }
};

void QuadratureFactory::createLobattoQuadratureForNeumannBC(MElement* ie, QuadratureBase*& integ)
{
  if (ie->getTypeForMSH() == MSH_TRI_3)
  {
    integ = new GaussLobatto2DQuadrature(2); //order 2
  }
  else if (ie->getTypeForMSH() == MSH_TRI_6)
  {
    integ = new GaussLobatto2DQuadrature(2); //order 2
  }
  else if (ie->getTypeForMSH() == MSH_QUA_9)
  {
    integ = new GaussLobatto2DQuadrature(2); //order 2
  }
  else if (ie->getTypeForMSH() == MSH_QUA_4)
  {
    integ = new GaussLobatto2DQuadrature(2); //order 2
  }
  else if (ie->getTypeForMSH() == MSH_TRI_10)
  {
    integ = new GaussLobatto2DQuadrature(3); //order 2
  }
  else
  {
    Msg::Error("gauss quadrature for NeumannBC is not defined by default");
  }
};

void QuadratureFactory::createGaussQuadratureForNeumannBCHierarchicalFE(int order, MElement* ie, QuadratureBase*& integ)
{
  if (ie->getType() == TYPE_PNT)
  {
    integ = new GaussQuadrature(1);
  }
  else if (ie->getType() == TYPE_LIN)
  {
    int integrationOrder = 2* order;
    integ = new GaussQuadrature(integrationOrder);
  }
  else if (ie->getType() == TYPE_TRI)
  {
    int integrationOrder = 2* order;
    integ = new GaussQuadrature(integrationOrder);
  }
  else if (ie->getType() == TYPE_QUA)
  {
    int integrationOrder = 2* order;
    if (order ==1) 
    {
      integrationOrder=3;
    }
    integ = new GaussQuadrature(integrationOrder);
  }
  else
  {
    Msg::Error("interface gauss quadrature is not defined for this element type by default");
  }
};
