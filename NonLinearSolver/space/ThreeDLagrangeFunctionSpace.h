//
// C++ Interface: Function Space
//
// Description: FunctionSpace used (scalar lagragian function space in 3D)
//
//
// Author:  <G. Becker & L. Noels>, (C) 2012
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef THREEDLAGRANGEFUNCTIONSPACE_H_
#define THREEDLAGRANGEFUNCTIONSPACE_H_
#include "nlsFunctionSpace.h"
#include "MInterfaceElement.h"


class VertexBasedLagrangeFunctionSpace : public nlsFunctionSpace<double>
{
  protected:
    bool _hessianComputation;
    bool _thirdDevComputation;
    
    std::vector<int> comp; // all fields
    std::vector<int> ifield; // id field for each field
    // number of comp;
    int _ncomp;
    std::set<int> _usePrimaryShapeFunctions; // set of all comps using primary shape functions
    
    
  protected:
    virtual void getKeysOnElement(MElement *ele, std::vector<Dof> &keys) const=0;

  public:
    static std::map<int,MElement*> primaryElementMap; // to avoid reallocated
    static MElement* getPrimaryElement(MElement* ele);
  
  public:
    VertexBasedLagrangeFunctionSpace(int id, int ncomp, bool hesscompute, bool thirdcompute) : 
    _ncomp(ncomp), _usePrimaryShapeFunctions(),
    _hessianComputation(hesscompute), _thirdDevComputation(thirdcompute)
    {
      _iField = id;
      ifield.resize(_ncomp);
      for (int i=0; i<_ncomp; i++)
      {
        ifield[i] = _iField;
      }
    }
    virtual ~VertexBasedLagrangeFunctionSpace(){};
    
    virtual std::string getFunctionSpaceName() const 
    {
      return "VertexBasedLagrangeFunctionSpace";
    }
    
    virtual void usePrimaryShapeFunction(int c)
    {
      Msg::Info("Field %d is considered with primary shape function in %s",c, getFunctionSpaceName().c_str());
      _usePrimaryShapeFunctions.insert(c);
    }
    virtual bool withPrimaryShapeFunction(int c) const
    {
      return _usePrimaryShapeFunctions.find(c) != _usePrimaryShapeFunctions.end();
    }
    
    virtual const std::set<int>& getCompsWithPrimaryShapeFunction() const
    {
      return _usePrimaryShapeFunctions;
    }
    
    virtual int getNumShapeFunctions(MElement* ele, int c) const
    {
      if (withPrimaryShapeFunction(c))
      {
        return ele->getNumPrimaryShapeFunctions();
      }
      else
      {
        return ele->getNumShapeFunctions();
      }
    };
    
    virtual int getShapeFunctionsIndex(MElement* ele, int c) const
    {
      int num = 0;
      bool found = false;
      for (int i=0; i< comp.size(); i++)
      {
        if (c == comp[i])
        {
          found = true;
          break;
        }
        num += getNumShapeFunctions(ele,comp[i]);
      }
      
      if (!found)
      {
        Msg::Error("field index %d does not exist",c);
      };
      
      return num;
    };
    
    virtual int getTotalNumShapeFunctions(MElement* ele) const
    {
      int num = 0;
      for (int i=0; i< comp.size(); i++)
      {
        num += getNumShapeFunctions(ele,comp[i]);
      }
      return num;
    };

    virtual bool withHessianComputation() const {return _hessianComputation;};
    virtual bool withThirdDevComputation() const {return _thirdDevComputation;};
    virtual int getNumComp() const {return _ncomp;};
    virtual void getComp(std::vector<int> &comp_) const
    {
      comp_.resize(comp.size());
      std::copy(comp.begin(),comp.end(),comp_.begin());
    }
    virtual void setNewIdForComp(int c, int type)
    {
      for (int i=0; i< comp.size(); i++)
      {
        if (c == comp[i])
        {
          ifield[i] = type;
          break;
        }
      }
    };
    
    virtual int getNumKeys(MElement *ele)  const 
    {
      MInterfaceElement *iele = dynamic_cast<MInterfaceElement*>(ele);
      if (iele)
      {
        int numK = getNumKeys(iele->getElem(0));
        if (iele->getElem(0) != iele->getElem(1))
        {
          numK += getNumKeys(iele->getElem(1));
        }
        return numK;
      }
      else
        return getTotalNumShapeFunctions(ele);
    };
    virtual void getKeys(MElement *ele, std::vector<Dof> &keys) const
    {
      MInterfaceElement *iele = dynamic_cast<MInterfaceElement*>(ele);
      if (iele)
      {
        this->getKeysOnElement(iele->getElem(0),keys);
        if(iele->getElem(0) != iele->getElem(1))
        {
          this->getKeysOnElement(iele->getElem(1),keys);
        }
      }
      else
      {
        this->getKeysOnElement(ele,keys);
      }
    }
    virtual void getKeysOnVertex(MElement* ele, MVertex* v, const std::vector<int>& vercomp, std::vector<Dof>& keys) const
    {
      // all Keys on this elements
      std::vector<Dof> elekeys;
      this->getKeys(ele,elekeys);
     
      // find position of vextex in list vertex of element
      int numVer =ele->getNumVertices();
      int idx = -1;
      // find vertex index in element
      bool found = false;
      for (int iv = 0; iv<numVer; iv++){
        if (ele->getVertex(iv)  == v){
          found = true;
          idx = iv;
          break;
        }
      }
      if (found == false){
        Msg::Error("Vertex %d does not belong to element %d",v->getNum(),ele->getNum());
        return;
      }
      
      std::vector<int> spaceComp;
      this->getComp(spaceComp);
      
      for (int ic = 0; ic < vercomp.size(); ic++)
      {
        // check if comp belong to comp list in space
        if (std::find(spaceComp.begin(),spaceComp.end(),vercomp[ic]) == spaceComp.end())
        {
          Msg::Error("comp %d does not exist in %s",vercomp[ic], getFunctionSpaceName().c_str());
        }
        else
        {
          int nbFF = getNumShapeFunctions(ele,vercomp[ic]);
          int nbFFTotalLastIndex  = getShapeFunctionsIndex(ele,vercomp[ic]);
          if (idx < nbFF)
          {
            keys.push_back(elekeys[nbFFTotalLastIndex+idx]);
          }
          #ifdef _DEBUG
          else
          {
            Msg::Warning("dof comp %d present only in primary vertices");
          }
          #endif //_DEBUG
        }
      }
    };
    
    // shape function
    virtual void f(MElement *ele, double u, double v, double w, std::vector<ValType> &vals) const
    {
      int valssize = vals.size();
      int totalnbFF = getTotalNumShapeFunctions(ele);
      vals.resize(valssize + totalnbFF);
      int lastTotalnbFF = 0;
      for(int l=0;l<_ncomp;l++)
      {
        int cc = comp[l];
        int nbFF = getNumShapeFunctions(ele,cc);
        if (withPrimaryShapeFunction(cc))
        {
          MElement* elePrimary = VertexBasedLagrangeFunctionSpace::getPrimaryElement(ele);
          elePrimary->getShapeFunctions(u,v,w,_ival);
        }
        else
        {
          ele->getShapeFunctions(u,v,w,_ival);
        }
        for(int i=0;i<nbFF;i++)          
        {
          vals[valssize+i+lastTotalnbFF] = _ival[i];
        }
        lastTotalnbFF += nbFF;
      }
    }
    
    virtual void fuvw(MElement *ele, double u, double v, double w, std::vector<ValType> &vals) const
    {
      int valssize = vals.size();
      int totalnbFF = getTotalNumShapeFunctions(ele);
      vals.resize(valssize + totalnbFF);
      int lastTotalnbFF = 0;
      for(int l=0;l<_ncomp;l++)
      {
        int cc = comp[l];
        int nbFF = getNumShapeFunctions(ele,cc);
        if (withPrimaryShapeFunction(cc))
        {
          MElement* elePrimary = VertexBasedLagrangeFunctionSpace::getPrimaryElement(ele);
          elePrimary->getShapeFunctions(u,v,w,_ival);
        }
        else
        {
          ele->getShapeFunctions(u,v,w,_ival);
        }
        for(int i=0;i<nbFF;i++)          
        {
          vals[valssize+i+lastTotalnbFF] = _ival[i];
        }
        lastTotalnbFF += nbFF;
      }
    }
    
    
    virtual void gradf(MElement *ele, double u, double v, double w,std::vector<GradType> &grads) const
    {
      int gradssize = grads.size();
      int totalnbFF = getTotalNumShapeFunctions(ele);
      grads.resize(gradssize + totalnbFF);
      int lastTotalnbFF = 0;
      double jac[3][3];
      double invjac[3][3];
      for(int l=0;l<_ncomp; l++)
      {
        int cc = comp[l];
        int nbFF = getNumShapeFunctions(ele,cc);
        
        if (withPrimaryShapeFunction(cc))
        {
          MElement* elePrimary = VertexBasedLagrangeFunctionSpace::getPrimaryElement(ele);
          elePrimary->getGradShapeFunctions(u,v,w,_igrad);
          double detJ = elePrimary->getJacobian(u, v, w, jac);
        }
        else
        {
          ele->getGradShapeFunctions(u,v,w,_igrad);
          double detJ = ele->getJacobian(u, v, w, jac);
        }
        inv3x3(jac, invjac);
      
        for(int i=0;i<nbFF;i++)
        {
          gradt(0)=invjac[0][0] * _igrad[i][0] + invjac[0][1] * _igrad[i][1] + invjac[0][2] * _igrad[i][2];
          gradt(1)=invjac[1][0] * _igrad[i][0] + invjac[1][1] * _igrad[i][1] + invjac[1][2] * _igrad[i][2];
          gradt(2)=invjac[2][0] * _igrad[i][0] + invjac[2][1] * _igrad[i][1] + invjac[2][2] * _igrad[i][2];
          grads[gradssize + i + lastTotalnbFF] = gradt;
        }
        lastTotalnbFF += nbFF;
      }
    }
    virtual void gradfuvw(MElement *ele, double u, double v, double w, std::vector<GradType> &grads) const
    {
      int gradssize = grads.size();
      int totalnbFF = getTotalNumShapeFunctions(ele);
      grads.resize(gradssize + totalnbFF);
      int lastTotalnbFF = 0;
      for(int l=0;l<_ncomp; l++)
      {
        int cc = comp[l];
        int nbFF = getNumShapeFunctions(ele,cc);
        if (withPrimaryShapeFunction(cc))
        {
          MElement* elePrimary = VertexBasedLagrangeFunctionSpace::getPrimaryElement(ele);
          elePrimary->getGradShapeFunctions(u,v,w,_igrad);
        }
        else
        {
          ele->getGradShapeFunctions(u,v,w,_igrad);
        }
      
        for(int i=0;i<nbFF;i++)
        {
          gradt(0) = _igrad[i][0];
          gradt(1) = _igrad[i][1];
          gradt(2) = _igrad[i][2];
          grads[gradssize + i + lastTotalnbFF] = gradt;
        }
        lastTotalnbFF += nbFF;
      }
    }
    
    virtual void hessfuvw(MElement *ele, double u, double v, double w,std::vector<HessType> &hess) const
    {
      int hesssize = hess.size();
      int totalnbFF = getTotalNumShapeFunctions(ele);
      hess.resize(hesssize + totalnbFF);
      int lastTotalnbFF = 0;
      for(int l=0;l<_ncomp; l++)
      {
        int cc = comp[l];
        int nbFF = getNumShapeFunctions(ele,cc);
        if (withPrimaryShapeFunction(cc))
        {
          MElement* elePrimary = VertexBasedLagrangeFunctionSpace::getPrimaryElement(ele);
          elePrimary->getHessShapeFunctions(u,v,w,_ihess);
        }
        else
        {
          ele->getHessShapeFunctions(u,v,w,_ihess);
        }
        for(int i=0;i<nbFF;i++)
        {
          hesst(0,0) = _ihess[i][0][0]; hesst(0,1) = _ihess[i][0][1]; hesst(0,2) = _ihess[i][0][2];
          hesst(1,0) = _ihess[i][1][0]; hesst(1,1) = _ihess[i][1][1]; hesst(1,2) = _ihess[i][1][2];
          hesst(2,0) = _ihess[i][2][0]; hesst(2,1) = _ihess[i][2][1]; hesst(2,2) = _ihess[i][2][2];
          hess[hesssize+i+lastTotalnbFF] = hesst;
        }
        lastTotalnbFF += nbFF;
      }
    }
    virtual void hessf(MElement *ele, double u, double v, double w,std::vector<HessType> &hess) const
    {
      int hesssize = hess.size();
      int totalnbFF = getTotalNumShapeFunctions(ele);
      hess.resize(hesssize + totalnbFF);
      int lastTotalnbFF = 0;
      double jac[3][3];
      double invjac[3][3];
      for(int l=0;l<_ncomp; l++)
      {
        int cc = comp[l];
        int nbFF = getNumShapeFunctions(ele,cc);
        int numver = 0;
        if (withPrimaryShapeFunction(cc))
        {
          MElement* elePrimary = VertexBasedLagrangeFunctionSpace::getPrimaryElement(ele);
          elePrimary->getGradShapeFunctions(u,v,w,_igrad);
          elePrimary->getHessShapeFunctions(u,v,w,_ihess);
          double detJ = elePrimary->getJacobian(u, v, w, jac);
          numver = elePrimary->getNumVertices();
        }
        else
        {
          ele->getGradShapeFunctions(u,v,w,_igrad);
          ele->getHessShapeFunctions(u,v,w,_ihess);
          double detJ = ele->getJacobian(u, v, w, jac);
          numver = ele->getNumVertices();
        }
        inv3x3(jac, invjac);

        STensor33 graduvwJ(0.);
        for (int i=0; i<numver; i++)
        {
          MVertex* v = ele->getVertex(i);
          SVector3 vec(v->x(),v->y(),v->z());
          for (int m=0; m<3; m++){
            for (int n=0; n<3; n++){
              for (int q=0; q<3; q++){
                graduvwJ(m,q,n) +=_ihess[i][m][n]*vec(q);
              };
            };
          };
        };

        for(int i=0;i<nbFF;i++)
        {
          STensorOperation::zero(hesst);
          for (int k=0; k<3; k++){
            for (int l=0; l<3; l++){
              for (int m=0; m<3; m++){
                for (int n=0; n<3; n++){
                  hesst(k,l)+= invjac[k][m]*invjac[l][n]*_ihess[i][m][n];
                  for (int q=0; q<3; q++){
                    for (int p=0; p<3; p++){
                      hesst(k,l) -= invjac[k][m]*invjac[l][n]*graduvwJ(m,q,n)*invjac[q][p]*_igrad[i][p];
                    };
                  };
                };
              };
            };
          };
          hess[hesssize + i+lastTotalnbFF] = hesst;
        };
        lastTotalnbFF += nbFF;
      };
    };
    virtual void thirdDevfuvw(MElement *ele, double u, double v, double w,std::vector<ThirdDevType> &third) const
    {
      int thirdsize = third.size();
      int totalnbFF = getTotalNumShapeFunctions(ele);
      third.resize(thirdsize + totalnbFF);
      
      int lastTotalnbFF = 0;
      for(int l=0;l<_ncomp; l++)
      {
        int cc = comp[l];
        int nbFF = getNumShapeFunctions(ele,cc);
        if (withPrimaryShapeFunction(cc))
        {
          MElement* elePrimary = VertexBasedLagrangeFunctionSpace::getPrimaryElement(ele);
          elePrimary->getThirdDerivativeShapeFunctions(u,v,w,_ithird);
        }
        else
        {
          ele->getThirdDerivativeShapeFunctions(u,v,w,_ithird);
        }
        
        for(int i=0;i<nbFF;i++)
        {
          for (int p=0; p<3; p++)
            for (int q=0; q<3; q++)
              for (int r=0; r<3; r++)
                thirdt(p,q,r) = _ithird[i][p][q][r];
        
          third[thirdsize + i+ lastTotalnbFF] = thirdt;
        }
        lastTotalnbFF += nbFF;
      }
    }; //need to high order fem
    virtual void thirdDevf(MElement *ele, double u, double v, double w,std::vector<ThirdDevType> &third) const
    {
      
      int thirdsize = third.size();
      int totalnbFF = getTotalNumShapeFunctions(ele);
      third.resize(thirdsize + totalnbFF);
      
      double jac[3][3];
      double invjac[3][3];
      
      int lastTotalnbFF = 0;
      for(int l=0;l<_ncomp; l++)
      {
        int cc = comp[l];
        int nbFF = getNumShapeFunctions(ele,cc);
        int numver = 0;
        if (withPrimaryShapeFunction(cc))
        {
          MElement* elePrimary = VertexBasedLagrangeFunctionSpace::getPrimaryElement(ele);
          elePrimary->getGradShapeFunctions(u,v,w,_igrad);
          elePrimary->getHessShapeFunctions(u,v,w,_ihess);
          elePrimary->getThirdDerivativeShapeFunctions(u,v,w,_ithird);
          double detJ = elePrimary->getJacobian(u, v, w, jac);
          numver = elePrimary->getNumVertices();
        }
        else
        {
          ele->getGradShapeFunctions(u,v,w,_igrad);
          ele->getHessShapeFunctions(u,v,w,_ihess);
          ele->getThirdDerivativeShapeFunctions(u,v,w,_ithird);
          double detJ = ele->getJacobian(u, v, w, jac);
          numver = ele->getNumVertices();
        }
        inv3x3(jac, invjac);

        STensor33 B(0.);
      
        for (int i=0; i<numver; i++){
          MVertex* v = ele->getVertex(i);
          SVector3 vec(v->x(),v->y(),v->z());
          for (int m=0; m<3; m++){
            for (int n=0; n<3; n++){
              for (int q=0; q<3; q++){
                B(m,q,n) +=_ihess[i][m][n]*vec(q);
              };
            };
          };
        };

        STensor43 C(0.);
        for (int i=0; i<numver; i++){
          MVertex* v = ele->getVertex(i);
          SVector3 vec(v->x(),v->y(),v->z());
          for (int m=0; m<3; m++){
            for (int n=0; n<3; n++){
              for (int q=0; q<3; q++){
                for (int r=0; r<3; r++ ){
                  C(m,q,n,r) +=_ithird[i][m][n][r]*vec(q);
                }
              };
            };
          };
        };

        STensor33 A(0);
        for (int i=0; i<3; i++)
          for (int j=0; j<3; j++)
            for (int k=0; k<3; k++)
              for (int l=0; l<3; l++)
                for (int m=0; m<3; m++)
                  A(i,j,k) -= B(l,m,k)*invjac[i][l]*invjac[m][j];

        for(int i=0;i<nbFF;i++){
          STensorOperation::zero(thirdt);
          for(int j=0; j<3; j++)
            for (int k=0; k<3; k++)
              for (int l=0; l<3; l++)
                for (int m=0; m<3; m++){
                  for (int s=0; s<3; s++)
                    for (int r=0; r<3; r++){
                      double temp = invjac[l][s]*(A(j,m,s)*invjac[k][r]+ A(k,r,s)*invjac[j][m]);
                      double temp2 = invjac[j][m]*invjac[k][r]*invjac[l][s];
                      thirdt(j,k,l) += temp* _ihess[i][m][r]+
                                       temp2* _ithird[i][m][r][s];

                      for (int q=0; q<3; q++)
                        for (int p=0; p<3; p++){
                          thirdt(j,k,l) -= temp*B(m,q,r)*invjac[q][p]*_igrad[i][p]+
                                           temp2*(C(m,q,r,s)*invjac[q][p]*_igrad[i][p]+
                                                  B(m,q,r)*A(q,p,s)*_igrad[i][p]+
                                                  B(m,q,r)*invjac[q][p]*_ihess[i][s][p]);
                        }
                    }
                }
        
          third[thirdsize + i+lastTotalnbFF] = thirdt;
        };
        lastTotalnbFF += nbFF;
      }
    }; //need to high order fem
    
  private: //cache to avoid constant reallocation
     mutable double _ival[256];
     mutable double _igrad[256][3];
     mutable double _ihess [256][3][3];
     mutable double _ithird[256][3][3][3];
     mutable HessType hesst;
     mutable GradType gradt;
     mutable ThirdDevType thirdt;
};


// Lagrange function space
class ThreeDLagrangeFunctionSpace : public VertexBasedLagrangeFunctionSpace
{
  protected:
    virtual void getKeysOnElement(MElement *ele, std::vector<Dof> &keys) const=0;
    
  public:
    ThreeDLagrangeFunctionSpace(int id, int ncomp,bool hesscompute, bool thirdcompute);
    ThreeDLagrangeFunctionSpace(int id, int ncomp, int comp1_, bool hesscompute, bool thirdcompute);
    ThreeDLagrangeFunctionSpace(int id, int ncomp, int comp1_, int comp2_, const bool hessc, const bool thirdc);
    ThreeDLagrangeFunctionSpace(int id, int ncomp, int comp1_, int comp2_, int comp3_,
                              const bool hessc, const bool thirdc) ;
    ThreeDLagrangeFunctionSpace(int id, int ncomp, int comp1_, int comp2_, int comp3_,
                                int comp4_,const bool hessc, const bool thirdc);
    ThreeDLagrangeFunctionSpace(int id, int ncomp, int comp1_, int comp2_, int comp3_,
                                int comp4_, int comp5_,const bool hessc, const bool thirdc);
    ThreeDLagrangeFunctionSpace(int id, int ncomp, int comp1_, int comp2_, int comp3_,
                                int comp4_, int comp5_, int comp6_, const bool hessc, const bool thirdc);
    virtual ~ThreeDLagrangeFunctionSpace(){}
    
    virtual std::string getFunctionSpaceName() const 
    {
      return "ThreeDLagrangeFunctionSpace";
    }
    
    virtual FunctionSpaceBase* clone(const int id) const 
    {
      Msg::Error("define ThreeDLagrangeFunctionSpace::clone!!!");
      return NULL;
    }
};

// used by dgshell and by dG3D for interface
class IsoparametricLagrangeFunctionSpace : public VertexBasedLagrangeFunctionSpace
{  
  protected:
    virtual void getKeysOnElement(MElement *ele, std::vector<Dof> &keys) const = 0;
 
  public:
    IsoparametricLagrangeFunctionSpace(int id);
    IsoparametricLagrangeFunctionSpace(int id, int comp1_);
    IsoparametricLagrangeFunctionSpace(int id, int comp1_, int comp2_) ;
    virtual ~IsoparametricLagrangeFunctionSpace()
    {
    }
    
    virtual std::string getFunctionSpaceName() const 
    {
      return "IsoparametricLagrangeFunctionSpace";
    }
    
    virtual FunctionSpaceBase* clone(const int id) const 
    {
      Msg::Error("define IsoparametricLagrangeFunctionSpace::clone");
      return NULL;
    };
    
    // ==fuvw
    virtual void f(MElement *ele, double u, double v, double w, std::vector<ValType> &vals) const
    {
      this->fuvw(ele,u,v,w,vals);
    }
    // == gradfuvw
    virtual void gradf(MElement *ele, double u, double v, double w,std::vector<GradType> &grads) const
    {
      this->gradfuvw(ele,u,v,w,grads);
    }

    // == hessfuvw
    virtual void hessf(MElement *ele, double u, double v, double w,std::vector<HessType> &hess) const
    {
      this->hessfuvw(ele,u,v,w,hess);
    }
    
    virtual void thirdDevfuvw(MElement *ele, double u, double v, double w, std::vector<ThirdDevType> &third) const 
    {
      Msg::Error("IsoparametricLagrangeFunctionSpace::thirdDevfuvw cannot be called!!!");
    }
    virtual void thirdDevf(MElement *ele, double u, double v, double w, std::vector<ThirdDevType> &third) const
    {
      this->thirdDevfuvw(ele,u,v,w,third);
    }
};


#endif // THREEDLAGRANGEFUNCTIONSPACE_H_
