//
// C++ Interface: Function Space
//
// Description: FunctionSpace used (scalar lagragian function space in 3D)
//
//
// Author:  <V-D Nguyen >, (C) 2020
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "ThreeDHierarchicalFunctionSpace.h"
#include "HierarchicalBasisH1Tria.h"
#include "HierarchicalBasisH1Tetra.h"
#include "HierarchicalBasisH1Quad.h"
#include "HierarchicalBasisH1Brick.h"
#include "HierarchicalBasisH1Pri.h"
#include "HierarchicalBasisH1Line.h"
#include "HierarchicalBasisH1Point.h"

std::map<std::pair<int,int>, HierarchicalBasis*> HierarchicalFunctionSpace::HierarchicalBasisMap;
HierarchicalBasis* HierarchicalFunctionSpace::getHierarchicalBasis(int order, MElement* ele)
{
  int eleType = ele->getType();
  std::map<std::pair<int,int>, HierarchicalBasis*>::iterator itF = HierarchicalBasisMap.find(std::pair<int,int>(eleType,order));
  if (itF != HierarchicalBasisMap.end())
  {
    return itF->second;
  }
  else
  {
    HierarchicalBasis* basis = NULL;
    if (eleType == TYPE_PNT)
    {
      basis = new HierarchicalBasisH1Point();
    }
    else if (eleType == TYPE_LIN)
    {
      basis = new HierarchicalBasisH1Line(order);
    }
    else if (eleType == TYPE_TRI)
    {
      basis = new HierarchicalBasisH1Tria(order);
    }
    else if (eleType == TYPE_TET)
    {
      basis = new HierarchicalBasisH1Tetra(order);
    }
    else if (eleType == TYPE_QUA)
    {
      basis = new HierarchicalBasisH1Quad(order);
    }
    else if (eleType == TYPE_HEX)
    {
      basis = new HierarchicalBasisH1Brick(order);
    }
    else if (eleType == TYPE_PRI)
    {
      basis = new HierarchicalBasisH1Pri(order);
    }
    else
    {
      Msg::Error("element type = %d has not been implemented",eleType);
    }
    HierarchicalBasisMap[std::pair<int,int>(eleType,order)] = basis;
    return basis;
  };
};

void HierarchicalFunctionSpace::setOrder(int cc, int order)
{
  bool found = false;
  for (int i=0; i< comp.size(); i++)
  {
    if (cc == comp[i])
    {
      found = true;
      _orderBasis[i] = order;
      Msg::Info("Order %d is used for comp %d in %s",order,cc,getFunctionSpaceName().c_str());
      break;
    }
  }
  if (!found)
  {
    Msg::Error("comp %d does not exist in %s !!!",cc,getFunctionSpaceName().c_str());
  }
};

int HierarchicalFunctionSpace::getNumShapeFunctions(MElement* ele, int c) const
{
  int orderComp = getOrderComp(c);
  HierarchicalBasis* basis = HierarchicalFunctionSpace::getHierarchicalBasis(orderComp,ele);
  const int vSize = basis->getnVertexFunction();
  const int eSize = basis->getnEdgeFunction();
  const int fSize = basis->getnTriFaceFunction() + basis->getnQuadFaceFunction();
  const int bSize = basis->getnBubbleFunction();  
  return vSize+eSize+fSize+bSize;
};
int HierarchicalFunctionSpace::getShapeFunctionsIndex(MElement* ele, int c) const
{
  int num = 0;
  bool found = false;
  for (int i=0; i< comp.size(); i++)
  {
    if (c == comp[i])
    {
      found = true;
      break;
    }
    num += getNumShapeFunctions(ele,comp[i]);
  }
  
  if (!found)
  {
    Msg::Error("field index %d does not exist",c);
  };
  
  return num;
}
int HierarchicalFunctionSpace::getTotalNumShapeFunctions(MElement* ele) const
{
  int num = 0;
  for (int i=0; i< comp.size(); i++)
  {
    num += getNumShapeFunctions(ele,comp[i]);
  }
  return num;
};

int HierarchicalFunctionSpace::getOrderComp(int c) const
{
  int fieldIndex = -1;
  bool found = false;
  for (int i=0; i< comp.size(); i++)
  {
    if (comp[i] == c)
    {
      fieldIndex = i;
      found = true;
      break;
    }
  }
  if (!found)
  {
    Msg::Error("comp %d can not be found in comp list",c);
    return -1;
  }
  return _orderBasis[fieldIndex];
};

int HierarchicalFunctionSpace::getIdComp(int c) const
{
  int fieldIndex = -1;
  bool found = false;
  for (int i=0; i< comp.size(); i++)
  {
    if (comp[i] == c)
    {
      fieldIndex = i;
      found = true;
      break;
    }
  }
  if (!found)
  {
    Msg::Error("comp %d can not be found in comp list",c);
    return -1;
  }
  return ifield[fieldIndex];
}