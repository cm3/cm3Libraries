//
// C++ Interface: space functions
//
// Description: Define a common functional space based on the new genTerm (see cadxfem)
//              Calls one for all the Gauss Points. Allow caching and avoid to recompute
//              the shape function each time which seems very costly
//
//
// Author:  <Gauthier BECKER>, (C) 2012
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef NLSFUNCTIONSPACE_H_
#define NLSFUNCTIONSPACE_H_
#include "functionSpace.h"
#include "GaussIntegration.h"
#include "STensorOperations.h"

// Structure to store the value of a Gauss point
template<class T> struct nlsContainerTraits
{
  typedef T ContainerValType;
  typedef T ContainerGradType;
  typedef T ContainerHessType;
  typedef T ContainerThirdDevType;
};

template<> struct nlsContainerTraits<double>
{
  typedef std::vector<TensorialTraits<double>::ValType> ContainerValType;
  typedef std::vector<TensorialTraits<double>::GradType> ContainerGradType;
  typedef std::vector<TensorialTraits<double>::HessType> ContainerHessType;
  typedef std::vector<TensorialTraits<double>::ThirdDevType> ContainerThirdDevType;
};


template<class T>
struct GaussPointSpaceValues
{
  public:
    typedef typename nlsContainerTraits<T>::ContainerValType ContainerValType;
    typedef typename nlsContainerTraits<T>::ContainerGradType ContainerGradType;
    typedef typename nlsContainerTraits<T>::ContainerHessType ContainerHessType;
    typedef typename nlsContainerTraits<T>::ContainerThirdDevType ContainerThirdDevType;

    ContainerValType _vvals;
    ContainerGradType _vgrads;
    ContainerHessType _vhess;
    ContainerThirdDevType _vthird;
    GaussPointSpaceValues(const FunctionSpace<T> *sp, MElement *ele,const double u, const double v, const double w,
                          const bool hessian, const bool thirdDev)
    {
      // compute the value
      sp->fuvw(ele,u,v,w,_vvals);
      sp->gradfuvw(ele,u,v,w,_vgrads);
      if(hessian)
        sp->hessfuvw(ele,u,v,w,_vhess);
      if(thirdDev)
        sp->thirdDevfuvw(ele,u,v,w,_vthird);
    }
    GaussPointSpaceValues(const GaussPointSpaceValues &source)
    {
      _vvals.assign(source._vvals.begin(),source._vvals.end());
      _vgrads.assign(source._vgrads.begin(),source._vgrads.end());
      _vhess.assign(source._vhess.begin(),source._vhess.end());
      _vthird.assign(source._vthird.begin(),source._vthird.end());
    }
    virtual ~GaussPointSpaceValues()
    {
      _vvals.clear();
      _vgrads.clear();
      _vhess.clear();
      _vthird.clear();
    }
};


template<> struct GaussPointSpaceValues <double>
{
  public:
    typedef typename nlsContainerTraits<double>::ContainerValType ContainerValType;
    typedef typename nlsContainerTraits<double>::ContainerGradType ContainerGradType;
    typedef typename nlsContainerTraits<double>::ContainerHessType ContainerHessType;
    typedef typename nlsContainerTraits<double>::ContainerThirdDevType ContainerThirdDevType;
    ContainerValType _vvals;
    ContainerGradType _vgrads;
    ContainerHessType _vhess;
    ContainerThirdDevType _vthird;
    GaussPointSpaceValues(const FunctionSpace<double> *sp, MElement *ele,const double u, const double v, const double w,
                          const bool hessian, const bool thirdDev)
    {
      int nbFF = ele->getNumShapeFunctions();
      //
      _vvals.resize(3*nbFF);
      _vgrads.resize(3*nbFF);
      if (hessian)
      {
        _vhess.resize(3*nbFF);
      }
      if (thirdDev)
      {
        _vthird.resize(3*nbFF);
      }
      // val
      double _ival[256];
      ele->getShapeFunctions(u,v,w,_ival);
      for(int i=0;i<nbFF;i++)
      {
        _vvals[i] = _ival[i];
        _vvals[i+nbFF] = _ival[i];
        _vvals[i+2*+nbFF] = _ival[i];
      }
      // grad
      double _igrad[256][3];
      ele->getGradShapeFunctions(u,v,w,_igrad);
      for(int i=0;i<nbFF;i++)
      {
        _vgrads[i](0) = _igrad[i][0];
        _vgrads[i](1) = _igrad[i][1];
        _vgrads[i](2) = _igrad[i][2];
        _vgrads[i+nbFF] = _vgrads[i];
        _vgrads[i+2*nbFF] = _vgrads[i];
      }
      
      // hess
      if (hessian)
      {
        double _ihess [256][3][3];
        ele->getHessShapeFunctions(u,v,w,_ihess);
        for(int i=0;i<nbFF;i++)
        {
          _vhess[i](0,0) = _ihess[i][0][0]; _vhess[i](0,1) = _ihess[i][0][1]; _vhess[i](0,2) = _ihess[i][0][2];
          _vhess[i](1,0) = _ihess[i][1][0]; _vhess[i](1,1) = _ihess[i][1][1]; _vhess[i](1,2) = _ihess[i][1][2];
          _vhess[i](2,0) = _ihess[i][2][0]; _vhess[i](2,1) = _ihess[i][2][1]; _vhess[i](2,2) = _ihess[i][2][2];
          
          _vhess[i+nbFF] = _vhess[i];
          _vhess[i+2*nbFF] = _vhess[i];
        }
      }
      
      // third
      if(thirdDev)
      {
        double _ithird[256][3][3][3];
        ele->getThirdDerivativeShapeFunctions(u,v,w,_ithird);
        for(int i=0;i<nbFF;i++)
        {
          for (int p=0; p<3; p++)
            for (int q=0; q<3; q++)
              for (int r=0; r<3; r++)
                _vthird[i](p,q,r) = _ithird[i][p][q][r];
                
          _vthird[i+nbFF] = _vthird[i];
          _vthird[i+2*nbFF] = _vthird[i];
        }
      }
    }
    GaussPointSpaceValues(const GaussPointSpaceValues &source)
    {
      _vvals.assign(source._vvals.begin(),source._vvals.end());
      _vgrads.assign(source._vgrads.begin(),source._vgrads.end());
      _vhess.assign(source._vhess.begin(),source._vhess.end());
      _vthird.assign(source._vthird.begin(),source._vthird.end());
    }
    virtual ~GaussPointSpaceValues()
    {
      _vvals.clear();
      _vgrads.clear();
      _vhess.clear();
      _vthird.clear();
    }
};


template<class T> 
class  nlsFunctionSpace : public FunctionSpace<T>
{
  public:
    typedef typename TensorialTraits<T>::ValType ValType;
    typedef typename TensorialTraits<T>::GradType GradType;
    typedef typename TensorialTraits<T>::HessType HessType;
    typedef typename TensorialTraits<T>::ThirdDevType ThirdDevType;
    
  public:
    virtual ~nlsFunctionSpace()
    {
      for(typename std::map<IntPt*,std::vector<GaussPointSpaceValues<T>*> >::iterator itG=_mapGaussValues.begin(); itG!=_mapGaussValues.end();++itG)
      {
        std::vector<GaussPointSpaceValues<T>*> &vgvals = itG->second;
        for(int j=0;j<vgvals.size();j++)
          delete vgvals[j];
      }
      _mapGaussValues.clear();
    }
    virtual int getNumShapeFunctions(MElement* ele, int c) const = 0;
    virtual int getShapeFunctionsIndex(MElement* ele, int c) const = 0; // location for comp c
    virtual int getTotalNumShapeFunctions(MElement* ele) const = 0;
    //some new function
    virtual bool withHessianComputation() const = 0;
    virtual bool withThirdDevComputation() const =0;
    
    virtual int getNumComp() const =0;
    virtual void getComp(std::vector<int> &comp) const = 0; // get all comp
    virtual void setNewIdForComp(int cc, int type) = 0;
    
    // functions from FunctionSpace
    virtual int getNumKeys(MElement *ele) const = 0;
    virtual void getKeys(MElement *ele, std::vector<Dof> &keys) const = 0;
    virtual void getKeysOnVertex(MElement *ele, MVertex *v, const std::vector<int> &comp, std::vector<Dof> &keys) const = 0;
    virtual FunctionSpaceBase *clone(const int id) const = 0;
    // shape functions
    virtual void f(MElement *ele, double u, double v, double w, std::vector<ValType> &vals) const = 0;
    virtual void fuvw(MElement *ele, double u, double v, double w,  std::vector<ValType> &vals) const = 0;
    virtual void gradf(MElement *ele, double u, double v, double w, std::vector<GradType> &grads) const = 0;
    virtual void gradfuvw(MElement *ele, double u, double v, double w, std::vector<GradType> &grads) const = 0;
    virtual void hessfuvw(MElement *ele, double u, double v, double w, std::vector<HessType> &hess) const = 0;
    virtual void hessf(MElement *ele, double u, double v, double w, std::vector<HessType> &hess) const = 0;
    virtual void thirdDevfuvw(MElement *ele, double u, double v, double w, std::vector<ThirdDevType> &third) const = 0;
    virtual void thirdDevf(MElement *ele, double u, double v, double w, std::vector<ThirdDevType> &third) const  = 0;
    
    
   protected:
    mutable std::map<IntPt*,std::vector<GaussPointSpaceValues<T>*> > _mapGaussValues; // cache in order to assess everywhere
    // function to allocate the map
    typename std::map<IntPt*,std::vector<GaussPointSpaceValues<T>*> >::iterator allocatePoints(MElement *ele,int npts, IntPt *GP) const
    {
      std::vector<GaussPointSpaceValues<T>*> allval;
      for(int i=0;i<npts;i++)
      {
        GaussPointSpaceValues<T>* gpsv = new GaussPointSpaceValues<T>(this,ele,GP[i].pt[0],GP[i].pt[1],GP[i].pt[2],withHessianComputation(),withThirdDevComputation());
        allval.push_back(gpsv);
      }
      std::pair<typename std::map<IntPt*,std::vector<GaussPointSpaceValues<T>*> >::iterator,bool> pib = _mapGaussValues.insert(std::pair<IntPt*,std::vector<GaussPointSpaceValues<T>*> >(GP,allval));
      return pib.first;
    }

  public:
    // only values in uvw space are stored as they are represented in the isoparametric space
    virtual void get(MElement *ele, int npts, IntPt *GP, std::vector< GaussPointSpaceValues<T>*> &vall) const
    {
      typename std::map<IntPt*,std::vector<GaussPointSpaceValues<T>*> >::iterator itG = _mapGaussValues.find(GP);
      // fill the value if not allocated
      if(itG == _mapGaussValues.end())
      {
        itG = this->allocatePoints(ele,npts,GP);
      }
      std::vector<GaussPointSpaceValues<T>*>& allc = itG->second;
      vall.assign(allc.begin(),allc.end());
    }
    
};
#endif // NLSFUNCTIONSPACE_H_
