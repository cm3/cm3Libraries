//
// C++ Interface: Function Space
//
// Description: interface for mixed function space 
//
//
// Author:  <VD Nguyen>, (C) 2020
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef _MIXEDFUNCTIONSPACE_H_
#define _MIXEDFUNCTIONSPACE_H_

#ifndef SWIG
#include "nlsFunctionSpace.h"
#include "CurlFunctionSpace.h"
#endif //SWIG
class mixedFunctionSpaceBase
{
  public:
    enum DofType{DOF_STANDARD=0,DOF_CURL=1}; // to 
  
  #ifndef SWIG
  public:
    virtual ~mixedFunctionSpaceBase(){}
    virtual int getNumKeysByType(mixedFunctionSpaceBase::DofType type, MElement *ele) const = 0; 
    virtual void getKeysByType(mixedFunctionSpaceBase::DofType type, MElement *ele, std::vector<Dof> &keys) const = 0;
  #endif //SWIG
};

#ifndef SWIG
template<class T>
class CurlMixedFunctionSpace : public mixedFunctionSpaceBase,  public nlsFunctionSpace<T>,  public CurlFunctionSpaceBase<T>
{
  public:
    typedef typename TensorialTraits<T>::ValType ValType;
    typedef typename TensorialTraits<T>::GradType GradType;
    typedef typename TensorialTraits<T>::HessType HessType;
    typedef typename TensorialTraits<T>::ThirdDevType ThirdDevType;
    typedef typename CurlTraits<T>::CurlType CurlType;
    typedef typename CurlTraits<T>::CurlCurlType CurlCurlType;
  
  protected:
    nlsFunctionSpace<T>* _space;
    CurlFunctionSpaceBase<T>* _curlSpace;
    
  public:
    CurlMixedFunctionSpace(nlsFunctionSpace<T>* space1, CurlFunctionSpaceBase<T>* space2): mixedFunctionSpaceBase(),
                      nlsFunctionSpace<T>(), CurlFunctionSpaceBase<T>(),
                      _space(space1),_curlSpace(space2){}
                      
    virtual ~CurlMixedFunctionSpace(){}
    
    nlsFunctionSpace<T>* getSpace() {return _space;};
    const nlsFunctionSpace<T>* getSpace() const {return _space;};
    CurlFunctionSpaceBase<T>* getCurlSpace() {return _curlSpace;};
    const CurlFunctionSpaceBase<T>* getCurlSpace() const {return _curlSpace;};
    
    virtual FunctionSpaceBase *clone(const int id) const
    {
      return new CurlMixedFunctionSpace(_space,_curlSpace);
    }; // copy space with new Id
    
    virtual int getId(void) const { return _space->getId();}
    
    virtual int getNumComp() const {return _space->getNumComp() + _curlSpace->getNumComp();}
    virtual void getComp(std::vector<int> &comp) const 
    {
      std::vector<int> standardComp, curlComp;
      _space->getComp(standardComp);
      _curlSpace->getComp(curlComp);
      
      comp.resize(getNumComp());
      for (int i=0; i< standardComp.size(); i++)
      {
        comp[i] = standardComp[i];
      }
      for (int i=0; i< curlComp.size(); i++)
      {
        comp[standardComp.size()+i] = curlComp[i];
      }
    }
    
    virtual int getNumShapeFunctions(MElement* ele, int c) const 
    {
      std::vector<int> standardComp, curlComp;
      _space->getComp(standardComp);
      _curlSpace->getComp(curlComp);
      if (std::find(standardComp.begin(),standardComp.end(),c) != standardComp.end())
      {
        return _space->getNumShapeFunctions(ele,c);
      }
      else if (std::find(curlComp.begin(),curlComp.end(),c) != curlComp.end())
      {
        return _curlSpace->getNumShapeFunctions(ele,c);
      }
      else
      {
        Msg::Error("comp %d does not exist in CurlMixedFunctionSpace::getNumShapeFunctions", c);
				return 0;
      }
    };
    virtual int getShapeFunctionsIndex(MElement* ele, int c) const 
    {
      std::vector<int> standardComp, curlComp;
      _space->getComp(standardComp);
      _curlSpace->getComp(curlComp);
      if (std::find(standardComp.begin(),standardComp.end(),c) != standardComp.end())
      {
        return _space->getShapeFunctionsIndex(ele,c);
      }
      else if (std::find(curlComp.begin(),curlComp.end(),c) != curlComp.end())
      {
        return _curlSpace->getShapeFunctionsIndex(ele,c);
      }
      else
      {
        Msg::Error("comp %d does not exist in CurlMixedFunctionSpace::getShapeFunctionsIndex",c);
				return -1;
      }
      
    }; // location of comp c
    virtual int getTotalNumShapeFunctions(MElement* ele) const 
    {
      return _space->getTotalNumShapeFunctions(ele)+_curlSpace->getTotalNumShapeFunctions(ele);
    };
    
    virtual bool withHessianComputation() const {return _space->withHessianComputation();};
    virtual bool withThirdDevComputation() const {return _space->withThirdDevComputation();}
    
    virtual void setNewIdForComp(int cc, int type)
    {
      std::vector<int> standardComp, curlComp;
      _space->getComp(standardComp);
      if (std::find(standardComp.begin(),standardComp.end(),cc) != standardComp.end())
      { 
        _space->setNewIdForComp(cc,type);
      }
      else
      {
        Msg::Error("CurlMixedFunctionSpace::setNewIdForComp with comp %d cannot be called !!!");
      }
    };
    
    // functions from FunctionSpace
    virtual void getKeysOnVertex(MElement *ele, MVertex *v, const std::vector<int> &comp, std::vector<Dof> &keys) const
    {
      std::vector<int> standardComp;
      _space->getComp(standardComp);
      std::vector<int> nodeComp;
      for (int i=0; i< comp.size(); i++)
      {
        if (std::find(standardComp.begin(),standardComp.end(),comp[i]) != standardComp.end())
        {
          nodeComp.push_back(comp[i]);
        }
      }
      _space->getKeysOnVertex(ele,v,nodeComp,keys);
    }
    
    virtual int getNumKeys(MElement *ele) const 
    {
      int numK = 0;
      if (_space!=NULL)
      {
        numK += _space->getNumKeys(ele);
      }
      if (_curlSpace != NULL)
      {
        numK += _curlSpace->getNumKeys(ele);
      }
      return numK;
    }
    virtual void getKeys(MElement *ele, std::vector<Dof> &keys) const 
    {
      if (_space!=NULL)
      {
        _space->getKeys(ele,keys);
      }
      if (_curlSpace != NULL)
      {
        _curlSpace->getKeys(ele,keys);
      }
    };
    
    virtual void get(MElement *ele, int npts, IntPt *GP, std::vector< GaussPointSpaceValues<T>*> &vall) const
    {
      if (_space != NULL)
      {
        _space->get(ele,npts,GP,vall);
      }
    }
    
  
    virtual void f(MElement *ele, double u, double v, double w,  std::vector<ValType> &vals) const
    {
      if (_space != NULL)
      {
        _space->f(ele,u,v,w,vals);
      }
      else
      {
        Msg::Error("space is null");
      }
    }
    virtual void fuvw(MElement *ele, double u, double v, double w, std::vector<ValType> &vals) const
    {
      if (_space != NULL)
      {
        _space->fuvw(ele,u,v,w,vals);
      }
      else
      {
        Msg::Error("space is null");
      }
    }
    
    virtual void gradf(MElement *ele, double u, double v, double w, std::vector<GradType> &grads) const
    {
      if (_space != NULL)
      {
        _space->gradf(ele,u,v,w,grads);
      }
      else
      {
        Msg::Error("space is null");
      }
    }
    virtual void gradfuvw(MElement *ele, double u, double v, double w, std::vector<GradType> &grads) const
    {
      if (_space != NULL)
      {
        _space->gradfuvw(ele,u,v,w,grads);
      }
      else
      {
        Msg::Error("space is null");
      }
    }
    virtual void hessfuvw(MElement *ele, double u, double v, double w, std::vector<HessType> &hess) const
    {
      if (_space != NULL)
      {
        _space->hessfuvw(ele,u,v,w,hess);
      }
      else
      {
        Msg::Error("space is null");
      }
    }
    virtual void hessf(MElement *ele, double u, double v, double w, std::vector<HessType> &hess) const
    {
      if (_space != NULL)
      {
        _space->hessf(ele,u,v,w,hess);
      }
      else
      {
        Msg::Error("space is null");
      }
    }
    virtual void thirdDevfuvw( MElement *ele, double u, double v, double w, std::vector<ThirdDevType> &third) const
    {
      if (_space != NULL)
      {
        _space->thirdDevfuvw(ele,u,v,w,third);
      }
      else
      {
        Msg::Error("space is null");
      }
    }
    virtual void thirdDevf( MElement *ele, double u, double v, double w, std::vector<ThirdDevType> &third) const
    {
      if (_space != NULL)
      {
        _space->thirdDevf(ele,u,v,w,third);
      }
      else
      {
        Msg::Error("space is null");
      }
    };
    
    virtual void fcurluvw(MElement *ele, double u, double v, double w, const std::string &typeFunction,std::vector<CurlType> &curlvals) const
    {
      if (_curlSpace!=NULL)
      {
        _curlSpace->fcurluvw(ele,u,v,w,typeFunction,curlvals);
      }
      else
      {
        Msg::Error("_curlSpace is null");
      }
    }
    virtual void fcurl(MElement *ele, double u, double v, double w, const std::string &typeFunction, std::vector<CurlType> &curlvals) const
    {
      if (_curlSpace!=NULL)
      {
        _curlSpace->fcurl(ele,u,v,w,typeFunction,curlvals);
      }
      else
      {
        Msg::Error("_curlSpace is null");
      }
    }
    
    virtual void Curlfcurl(MElement *ele, double u, double v, double w, const std::string &typeFunction,std::vector<CurlCurlType> &Curlcurlvals) const
    {
      if (_curlSpace!=NULL)
      {
        _curlSpace->Curlfcurl(ele,u,v,w,typeFunction,Curlcurlvals);
      }
      else
      {
        Msg::Error("_curlSpace is null");
      }
    }
    virtual void Curlfcurluvw(MElement *ele, double u, double v, double w, const std::string &typeFunction,std::vector<CurlCurlType> &Curlcurlvals) const
    {
      if (_curlSpace!=NULL)
      {
        _curlSpace->Curlfcurluvw(ele,u,v,w,typeFunction,Curlcurlvals);
      }
      else
      {
        Msg::Error("_curlSpace is null");
      }
    }
    
    virtual int getNumKeysByType(mixedFunctionSpaceBase::DofType type, MElement *ele) const
    {
      if (type == mixedFunctionSpaceBase::DOF_STANDARD)
      {
        if (_space!=NULL)
          return _space->getNumKeys(ele);
        else
          return 0;
      }
      else if (type== mixedFunctionSpaceBase::DOF_CURL)
      {
        if (_curlSpace!=NULL)
          return _curlSpace->getNumKeys(ele);
        else
          return 0;
      }
      else
      {
        Msg::Error("Dof type %d has not been defined !!!",type);
        return 0;
      }
    } 
    virtual void getKeysByType(mixedFunctionSpaceBase::DofType type, MElement *ele, std::vector<Dof> &keys) const
    {
      if (type == mixedFunctionSpaceBase::DOF_STANDARD)
      {
        if (_space!=NULL)
        {
          _space->getKeys(ele,keys);
        }
      }
      else if (type== mixedFunctionSpaceBase::DOF_CURL)
      {
        if (_curlSpace != NULL)
        {
          _curlSpace->getKeys(ele,keys);
        }
      }
      else
      {
        Msg::Error("Dof type %d has not been defined !!!",type);
      }
    }
};
#endif //SWIG
#endif // _MIXEDFUNCTIONSPACE_H_
