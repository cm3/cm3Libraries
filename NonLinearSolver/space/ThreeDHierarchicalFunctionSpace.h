//
// C++ Interface: Function Space
//
// Description: FunctionSpace used (scalar lagragian function space in 3D)
//
//
// Author:  <V-D Nguyen >, (C) 2020
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef THREEDHIERARCHICALFUNCTIONSPACE_H_
#define THREEDHIERARCHICALFUNCTIONSPACE_H_

#include "HierarchicalBasis.h"
#include "nlsFunctionSpace.h"
#include "MInterfaceElement.h"

class HierarchicalFunctionSpace : public nlsFunctionSpace<double>
{
  public:
    static std::map<std::pair<int,int>, HierarchicalBasis*> HierarchicalBasisMap;
    static HierarchicalBasis* getHierarchicalBasis(int order, MElement* ele);
  
  protected:    
    std::vector<int> comp; // all fields
    std::vector<int> ifield; // id field for each field
    // number of comp;
    int _ncomp;
    std::vector<int> _orderBasis;
    
  protected: 
    virtual void getKeysOnElement(MElement *ele, std::vector<Dof> &keys) const=0;
    virtual void getShapeFunctions(int c, MElement *ele, double u, double v, double w,  std::vector<double>& vals) const=0;
    virtual void getGradShapeFunctions(int c, MElement *ele, double u, double v, double w,  std::vector<std::vector<double> >& grads) const = 0;
    
  public:
    HierarchicalFunctionSpace(int id, int ord, int ncomp):_ncomp(ncomp)
    {
      _iField = id;
      ifield.resize(_ncomp);
      _orderBasis.resize(_ncomp);
      for (int i=0; i<_ncomp; i++)
      {
        ifield[i] = _iField;
        _orderBasis[i] = ord;
      }
    };
    virtual ~HierarchicalFunctionSpace(){};
    
    virtual std::string getFunctionSpaceName() const 
    {
      return "HierarchicalFunctionSpace";
    }
    
    virtual FunctionSpaceBase *clone(const int id) const = 0;
    virtual void setOrder(int c, int order);
    virtual int getOrderComp(int c) const;
    virtual int getIdComp(int c) const;
    
    virtual int getMaxOrderBasis() const
    {
      int cc = _orderBasis[0];
      for (int i=1; i< _ncomp; i++)
      {
        if (_orderBasis[i] > cc)
          cc = _orderBasis[i];
      }
      return cc;
    }
    
    virtual int getNumShapeFunctions(MElement* ele, int c) const;
    virtual int getShapeFunctionsIndex(MElement* ele, int c) const; // location for comp c
    virtual int getTotalNumShapeFunctions(MElement* ele) const;
    //some new function
    virtual bool withHessianComputation() const {return false;};
    virtual bool withThirdDevComputation() const {return false;};
    
    virtual int getNumComp() const {return _ncomp;};
    virtual void getComp(std::vector<int> &comp_) const
    {
      comp_.resize(comp.size());
      std::copy(comp.begin(),comp.end(),comp_.begin());
    }
    virtual void setNewIdForComp(int c, int type)
    {
      for (int i=0; i< comp.size(); i++)
      {
        if (c == comp[i])
        {
          ifield[i] = type;
          break;
        }
      }
    };
    
    virtual void getKeysOnLowerDimensionPart(MElement *ele, MElement* lowEle, const std::vector<int> &vercomp, std::vector<Dof> &keys) const = 0;
    
    // functions from FunctionSpace
    virtual int getNumKeys(MElement *ele) const
    {
      MInterfaceElement *iele = dynamic_cast<MInterfaceElement*>(ele);
      if (iele)
      {
        int numK = getNumKeys(iele->getElem(0));
        if (iele->getElem(0) != iele->getElem(1))
        {
          numK += getNumKeys(iele->getElem(1));
        }
        return numK;
      }
      else
        return getTotalNumShapeFunctions(ele);
    };
    virtual void getKeys(MElement *ele, std::vector<Dof> &keys) const 
    {
      MInterfaceElement *iele = dynamic_cast<MInterfaceElement*>(ele);
      if (iele)
      {
        this->getKeysOnElement(iele->getElem(0),keys);
        if(iele->getElem(0) != iele->getElem(1))
        {
          this->getKeysOnElement(iele->getElem(1),keys);
        }
      }
      else
      {
        this->getKeysOnElement(ele,keys);
      }
    }
    virtual void getKeysOnVertex(MElement *ele, MVertex *v, const std::vector<int> &vercomp, std::vector<Dof> &keys) const
    {
      // all Keys on this elements
      std::vector<Dof> elekeys;
      this->getKeys(ele,elekeys);
      if (elekeys.size() == 0)
      {
        Msg::Error("cannot get Dof from ele = %d in %s",ele->getNum(),getFunctionSpaceName().c_str());
        return;
      }
     
      // find position of vextex in list vertex of element
      int numVer =ele->getNumVertices();
      int idx = -1;
      // find vertex index in element
      bool found = false;
      for (int iv = 0; iv<numVer; iv++){
        if (ele->getVertex(iv)  == v){
          found = true;
          idx = iv;
          break;
        }
      }
      if (found == false){
        Msg::Error("Vertex %d does not belong to element %d",v->getNum(),ele->getNum());
        return;
      }
      
      std::vector<int> spaceComp;
      this->getComp(spaceComp);
      
      for (int ic = 0; ic < vercomp.size(); ic++)
      {
        // check if comp belong to comp list in space
        if (std::find(spaceComp.begin(),spaceComp.end(),vercomp[ic]) == spaceComp.end())
        {
          Msg::Error("comp %d does not exist in %s",vercomp[ic], getFunctionSpaceName().c_str());
        }
        else
        {
          int nbFF = getNumShapeFunctions(ele,vercomp[ic]);
          int nbFFTotalLastIndex  = getShapeFunctionsIndex(ele,vercomp[ic]);
          if (idx < nbFF)
          {
            keys.push_back(elekeys[nbFFTotalLastIndex+idx]);
          }
          #ifdef _DEBUG
          else
          {
            Msg::Warning("Dof comp %d cannot be found",vercomp[ic]);
          }
          #endif //_DEBUG
        }
      }
    };
    
    // shape functions
    virtual void f(MElement *ele, double u, double v, double w, std::vector<ValType> &vals) const
    {
      this->fuvw(ele,u,v,w,vals);
    }
    
    virtual void fuvw(MElement *ele, double u, double v, double w,  std::vector<ValType> &vals) const
    {
      int valssize = vals.size();
      int totalnbFF = getTotalNumShapeFunctions(ele);
      vals.resize(valssize + totalnbFF);
      int lastTotalnbFF = 0;
      for(int l=0;l<_ncomp;l++)
      {
        int cc = comp[l];
        int nbFF = getNumShapeFunctions(ele,cc);
        std::vector<double> _vvals;
        getShapeFunctions(cc,ele,u,v,w,_vvals);
        for(int i=0;i<nbFF;i++)          
        {
          vals[valssize+i+lastTotalnbFF] = _vvals[i];
        }
        lastTotalnbFF += nbFF;
      }
    };
    virtual void gradf(MElement *ele, double u, double v, double w, std::vector<GradType> &grads) const
    {
      GradType gradt;
      int gradssize = grads.size();
      int totalnbFF = getTotalNumShapeFunctions(ele);
      grads.resize(gradssize + totalnbFF);
      int lastTotalnbFF = 0;
      double jac[3][3];
      double invjac[3][3];
      for(int l=0;l<_ncomp; l++)
      {
        int cc = comp[l];
        int nbFF = getNumShapeFunctions(ele,cc);
        std::vector<std::vector<double> > _vgrads;
        getGradShapeFunctions(cc,ele,u,v,w,_vgrads);
        double detJ = ele->getJacobian(u, v, w, jac);
        inv3x3(jac, invjac);
      
        for(int i=0;i<nbFF;i++)
        {
          gradt(0)=invjac[0][0] * _vgrads[i][0] + invjac[0][1] * _vgrads[i][1] + invjac[0][2] * _vgrads[i][2];
          gradt(1)=invjac[1][0] * _vgrads[i][0] + invjac[1][1] * _vgrads[i][1] + invjac[1][2] * _vgrads[i][2];
          gradt(2)=invjac[2][0] * _vgrads[i][0] + invjac[2][1] * _vgrads[i][1] + invjac[2][2] * _vgrads[i][2];
          grads[gradssize + i + lastTotalnbFF] = gradt;
        }
        lastTotalnbFF += nbFF;
      }
    }
    virtual void gradfuvw(MElement *ele, double u, double v, double w, std::vector<GradType> &grads) const
    {
      GradType gradt;
      int gradssize = grads.size();
      int totalnbFF = getTotalNumShapeFunctions(ele);
      grads.resize(gradssize + totalnbFF);
      int lastTotalnbFF = 0;
      for(int l=0;l<_ncomp; l++)
      {
        int cc = comp[l];
        int nbFF = getNumShapeFunctions(ele,cc);
        std::vector<std::vector<double> > _vgrads;
        getGradShapeFunctions(cc,ele,u,v,w,_vgrads);
        //
        for(int i=0;i<nbFF;i++)
        {
          gradt(0) = _vgrads[i][0];
          gradt(1) = _vgrads[i][1];
          gradt(2) = _vgrads[i][2];
          grads[gradssize + i + lastTotalnbFF] = gradt;
        }
        lastTotalnbFF += nbFF;
      }
    }
    virtual void hessfuvw(MElement *ele, double u, double v, double w, std::vector<HessType> &hess) const
    {
      Msg::Error("HierarchicalFunctionSpace::hessfuvw has not been implemented yet !!!");
    }
    virtual void hessf(MElement *ele, double u, double v, double w, std::vector<HessType> &hess) const
    {
      Msg::Error("HierarchicalFunctionSpace::hessf has not been implemented yet !!!");
    };
    virtual void thirdDevfuvw(MElement *ele, double u, double v, double w, std::vector<ThirdDevType> &third) const
    {
      Msg::Error("HierarchicalFunctionSpace::thirdDevfuvw has not been implemented yet !!!");
    };
    virtual void thirdDevf(MElement *ele, double u, double v, double w, std::vector<ThirdDevType> &third) const 
    {
      Msg::Error("HierarchicalFunctionSpace::thirdDevf has not been implemented yet !!!");
    };
};

#endif // THREEDHIERARCHICALFUNCTIONSPACE_H_