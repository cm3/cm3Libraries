//
// function space on an interface domain
//
// Description: When you define an interface domain your space has derived from this space (add special functions)
//
// Author:  <Gauthier BECKER>, (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef _INTERFUNCTIONSPACE_H_
#define _INTERFUNCTIONSPACE_H_
#include "functionSpace.h"
class interFunctionSpace
{
  public:
    virtual FunctionSpaceBase* getMinusSpace() =0;
    virtual FunctionSpaceBase* getPlusSpace() =0;
    virtual const FunctionSpaceBase* getMinusSpace() const=0;
    virtual const FunctionSpaceBase* getPlusSpace() const=0;
    virtual void getNumKeys(MInterfaceElement *ele, int &numMinus, int &numPlus) const= 0; // if one needs the number of dofs
    virtual void getKeys(MInterfaceElement *ele, std::vector<Dof> &Rminus,std::vector<Dof> &Rplus) const=0;
};

#endif //_INTERFUNCTIONSPACE_H_
