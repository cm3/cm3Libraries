//
// C++ Interface: Function Space
//
// Description: curl function space 
//
//
// Author:  <VD Nguyen>, (C) 2020
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef CURLFUNCTIONSPACE_H_
#define CURLFUNCTIONSPACE_H_

#include "functionSpace.h"
#include "HierarchicalBasis.h"
#include "HierarchicalBasisHcurlTria.h"
#include "HierarchicalBasisHcurlTetra.h"
#include "HierarchicalBasisHcurlQuad.h"
#include "HierarchicalBasisHcurlBrick.h"
#include "HierarchicalBasisHcurlPri.h"
#include "GModel.h"
#include "STensorOperations.h"
#include "gmsh.h"

template <class T> struct CurlTraits 
{
  typedef T CurlType[3];
  typedef T CurlCurlType[3];
};

template <> struct CurlTraits<double> {
  typedef SVector3 CurlType;
  typedef SVector3 CurlCurlType;
};


template<class T>
class CurlFunctionSpaceBase 
{
  public:
    typedef typename CurlTraits<T>::CurlType CurlType;
    typedef typename CurlTraits<T>::CurlCurlType CurlCurlType;
    
  public:
    virtual ~CurlFunctionSpaceBase(){}
    
    virtual int getNumComp() const =0;
    virtual void getComp(std::vector<int> &comp) const = 0; // get all comp
    
    virtual int getNumShapeFunctions(MElement* ele, int c) const =0;
    virtual int getShapeFunctionsIndex(MElement* ele, int c) const =0;
    virtual int getTotalNumShapeFunctions(MElement* ele) const = 0;

    virtual int getNumKeys(MElement *ele) const = 0;
    virtual void getKeys(MElement *ele, std::vector<Dof> &keys) const = 0;

    virtual void fcurluvw(MElement *ele, double u, double v, double w, const std::string &typeFunction,std::vector<CurlType> &curlvals) const = 0;
    virtual void fcurl(MElement *ele, double u, double v, double w, const std::string &typeFunction, std::vector<CurlType> &curlvals) const = 0;
    
    virtual void Curlfcurl(MElement *ele, double u, double v, double w, const std::string &typeFunction,std::vector<CurlCurlType> &Curlcurlvals) const =0;
    virtual void Curlfcurluvw(MElement *ele, double u, double v, double w, const std::string &typeFunction,std::vector<CurlCurlType> &Curlcurlvals) const = 0;
};


class ScalarHierarchicalCurlFunctionSpace : public CurlFunctionSpaceBase<double>
{
  public:

    // cache to avoid reevaluation of shape functions
    // due to expensive gmsh api calls
    // Store for each elem, sf val evaluated for each GP
    mutable std::map<size_t, std::map<std::vector<double>, std::vector<CurlType> > > _precomputedCurlVals;
    mutable std::map<size_t, std::map<std::vector<double>, std::vector<CurlCurlType> > > _precomputedCurlCurlVals;
    mutable std::vector<int> _precomputedCurlBasisFunctionsOrientation;
    mutable std::vector<int> _precomputedCurlUniqueOrientations;
    mutable std::vector<int> _precomputedCurlCurlBasisFunctionsOrientation;
    mutable std::vector<int> _precomputedCurlCurlUniqueOrientations;
    mutable std::vector<size_t> _filledElemKeys;
  
   //
    mutable std::map<int, HierarchicalBasis*> _eleCurlBasis; // cache to avoid reallocate
    int _orderCurlBasis;
    
  public:
    ScalarHierarchicalCurlFunctionSpace(int order): CurlFunctionSpaceBase<double>(), _orderCurlBasis(order)
    {
    }
    
    virtual ~ScalarHierarchicalCurlFunctionSpace()
    {
      // clear cache
      for (std::map<int, HierarchicalBasis*>::iterator it = _eleCurlBasis.begin(); it != _eleCurlBasis.end(); it++)
      {
        delete it->second;
      }
      _eleCurlBasis.clear();
    };
    
    // set and get
    void setOrderCurlBasis(const int o) {_orderCurlBasis = o;}
    int getOrderCurlBasis() const {return _orderCurlBasis;}

    // Function to remove repeated orientation numbers from the input vector
    // and return a vector of unique orientation numbers
    const std::vector<int> getUniqueOrientations(std::vector<int> &basisFunctionsOrientation) const
    {
        std::vector<int> uniqueOrientationsNumbering;
        std::pair< int, int > previousPair(-1, -1);
        for(unsigned int i = 0; i < basisFunctionsOrientation.size(); ++i)
        {
            if(basisFunctionsOrientation[i] == previousPair.first)
            {
                basisFunctionsOrientation[i] = previousPair.second;
            }
            else
            {
                auto itFind = std::find(uniqueOrientationsNumbering.begin(), uniqueOrientationsNumbering.end(), basisFunctionsOrientation[i]);
                if(itFind == uniqueOrientationsNumbering.end())
                {
                    uniqueOrientationsNumbering.push_back(basisFunctionsOrientation[i]);
                    previousPair = std::make_pair(basisFunctionsOrientation[i], uniqueOrientationsNumbering.size() - 1);
                    basisFunctionsOrientation[i] = previousPair.second;
                }
                else
                {
                    previousPair = std::make_pair(basisFunctionsOrientation[i], itFind - uniqueOrientationsNumbering.begin());
                    basisFunctionsOrientation[i] = previousPair.second;
                }
            }
        }
        return uniqueOrientationsNumbering;
    }
    
    virtual void fcurl(MElement *ele, double u, double v, double w, const std::string &typeFunction,std::vector<CurlType> &curlvals) const
    {
      if (typeFunction != "HcurlLegendre")
      {
        Msg::Error("Wrong function invoked: use Curlfcurl() instead");
      }

      const std::size_t elemTag = ele->getNum();
      std::vector<double> localCoord{u, v, w};
      if ((_precomputedCurlVals.find(elemTag) == _precomputedCurlVals.end()) ||
          ((_precomputedCurlVals.find(elemTag))->second.find(localCoord) == (_precomputedCurlVals.find(elemTag))->second.end()) )
      {
          int eleType = ele->getType();
          if (_eleCurlBasis.find(eleType) == _eleCurlBasis.end())
          {
              if (eleType == TYPE_TRI)
              {
                  _eleCurlBasis[eleType] = new HierarchicalBasisHcurlTria(_orderCurlBasis);
              }
              else if (eleType == TYPE_TET)
              {
                  _eleCurlBasis[eleType] = new HierarchicalBasisHcurlTetra(_orderCurlBasis);
              }
              else if (eleType == TYPE_QUA)
              {
                  _eleCurlBasis[eleType] = new HierarchicalBasisHcurlQuad(_orderCurlBasis);
              }
              else if (eleType == TYPE_HEX)
              {
                  _eleCurlBasis[eleType] = new HierarchicalBasisHcurlBrick(_orderCurlBasis);
              }
              else if (eleType == TYPE_PRI)
              {
                  _eleCurlBasis[eleType] = new HierarchicalBasisHcurlPri(_orderCurlBasis);
              }
              else
              {
                  Msg::Error("missing cases");
              }
          }

          HierarchicalBasis *basis = _eleCurlBasis[eleType];
          int vSize = basis->getnVertexFunction();
          int bSize = basis->getnBubbleFunction();
          int eSize = basis->getnEdgeFunction();
          int fSize = basis->getnTriFaceFunction() + basis->getnQuadFaceFunction();
          int numFunPerEle = vSize + bSize + eSize + fSize;

          int numedges = ele->getNumEdges();
          int numvert = ele->getNumVertices();
          int numfaces = ele->getNumFaces();
          double jac[3][3];
          double invjac[3][3];
          const double detJ = ele->getJacobian(u, v, w, jac);
          inv3x3(jac, invjac);

          //int curlvalssize = curlvals.size();
          curlvals.resize(/*curlvalssize +*/ this->getOrderCurlBasis() * numedges, SVector3(0.0));

          const int elemType = ele->getTypeForMSH();
          int numComponents = 0;
          int numOrientations = 0;
          std::vector<double> gmsh_curlVals;

          static int _initialized = 0;
          if (!_initialized)
          {
              gmsh::initialize();
              gmsh::option::setNumber("General.Terminal",0);
              _initialized = 1;
          }
          std::vector<std::pair<int, int> > entities;
          const int dim = 3;
          gmsh::model::getEntities(entities, dim);

          // loop for multiple entities of same dim
          // possible with multiple fields defined
          if (_precomputedCurlBasisFunctionsOrientation.empty())
          {
              std::vector<int> basisFunctionsOrientation;
              for (unsigned int entitie = 0; entitie < entities.size(); ++entitie)
              {
                  const int tag = entities[entitie].second;
                  std::vector<int> tempOrientations;
                  gmsh::model::mesh::getBasisFunctionsOrientation(elemType, typeFunction, tempOrientations,
                                                                             tag);

                  for (unsigned int k = 0; k < tempOrientations.size(); ++k)
                      basisFunctionsOrientation.push_back(tempOrientations[k]);
              }
              _precomputedCurlBasisFunctionsOrientation = basisFunctionsOrientation;
              const std::vector<int> uniqueOrientationsNumbering = this->getUniqueOrientations(_precomputedCurlBasisFunctionsOrientation);
              _precomputedCurlUniqueOrientations = uniqueOrientationsNumbering;
          }

          const int numberOfOrientations = gmsh::model::mesh::getNumberOfOrientations(elemType, typeFunction);

          if (_filledElemKeys.empty())
          {
              int familyType = ElementType::getParentType(elemType);
              std::vector<GEntity *> ENTITIES;
              for (unsigned int entitie = 0; entitie < entities.size(); ++entitie)
              {
                  const int tag = entities[entitie].second;
                  if (dim >= 0 && tag >= 0)
                  {
                      GEntity *ge = GModel::current()->getEntityByTag(dim, tag);
                      ENTITIES.push_back(ge);
                  }
                  else
                      GModel::current()->getEntities(ENTITIES, dim);
              }

              for (std::size_t i = 0; i < ENTITIES.size(); i++)
              {
                  GEntity *ge = ENTITIES[i];
                  std::size_t numElementsInEntitie = ge->getNumMeshElementsByType(familyType);

                  for (std::size_t j = 0; j < numElementsInEntitie; j++)
                  {
                      MElement *e = ge->getMeshElementByType(familyType, j);
                      _filledElemKeys.push_back(e->getNum());
                  }
              }
          }

          const auto foundElem = std::find(std::begin(_filledElemKeys), std::end(_filledElemKeys), elemTag);
          if (foundElem == std::end(_filledElemKeys))
              Msg::Error("Element with tag %d not found", elemTag);
          const std::size_t elemPos = std::distance(std::begin(_filledElemKeys), foundElem);

          std::vector<int> wantedOrienForElem{_precomputedCurlUniqueOrientations[_precomputedCurlBasisFunctionsOrientation[elemPos]]};
          gmsh::model::mesh::getBasisFunctions(elemType, localCoord, typeFunction, numComponents, gmsh_curlVals,
                                               numOrientations, wantedOrienForElem);

          SVector3 curlvaluvw(0.0);
          for (int i = 0; i < numedges; ++i)
          {
              STensorOperation::zero(curlvaluvw);
              for (int j = 0; j < 3; ++j)
                  for (int k = 0; k < 3; ++k)
                      curlvaluvw[k] += invjac[k][j] * gmsh_curlVals[i * 3 + j];

              curlvals[/*curlvalssize +*/ i].operator=(curlvaluvw);
          }

          // push back sf value in cache data structure
          _precomputedCurlVals[elemTag].insert(std::make_pair(localCoord, curlvals));
      }
      else
      {
          // find elemTag in cached sf data structure from
          // earlier computations in initialization
          auto search = _precomputedCurlVals.find(elemTag);
          if (search != _precomputedCurlVals.end())
          {
              // find GP corresponding to elemTag and
              // check if CurlVals sf is filled and not empty
              auto GP = search->second.find(localCoord);
              if (GP != search->second.end())
                  curlvals = GP->second;
          }
          if(curlvals.empty())
              Msg::Error("Failed to get cached CurlVals for %d element", elemTag);
      }
    }
    
    virtual void fcurluvw(MElement *ele, double u, double v, double w, const std::string &typeFunction,std::vector<CurlType> &curlvals) const
    {
      if (typeFunction != "HcurlLegendre")
      {
        Msg::Error("Wrong function invoked: use Curlfcurluvw() instead");
      }

        const std::size_t elemTag = ele->getNum();
        std::vector<double> localCoord{u, v, w};
        if ((_precomputedCurlVals.find(elemTag) == _precomputedCurlVals.end()) ||
            ((_precomputedCurlVals.find(elemTag))->second.find(localCoord) == (_precomputedCurlVals.find(elemTag))->second.end()) )
        {
            int eleType = ele->getType();
            if  (_eleCurlBasis.find(eleType) == _eleCurlBasis.end())
            {
                if (eleType == TYPE_TRI)
                {
                    _eleCurlBasis[eleType] = new HierarchicalBasisHcurlTria(_orderCurlBasis);
                }
                else if (eleType == TYPE_TET)
                {
                    _eleCurlBasis[eleType] = new HierarchicalBasisHcurlTetra(_orderCurlBasis);
                }
                else if (eleType == TYPE_QUA)
                {
                    _eleCurlBasis[eleType] = new HierarchicalBasisHcurlQuad(_orderCurlBasis);
                }
                else if (eleType == TYPE_HEX)
                {
                    _eleCurlBasis[eleType] = new HierarchicalBasisHcurlBrick(_orderCurlBasis);
                }
                else if (eleType == TYPE_PRI)
                {
                    _eleCurlBasis[eleType] = new HierarchicalBasisHcurlPri(_orderCurlBasis);
                }
                else
                {
                    Msg::Error("missing cases");
                }
            }

            HierarchicalBasis* basis = _eleCurlBasis[eleType];
            int vSize = basis->getnVertexFunction();
            int bSize = basis->getnBubbleFunction();
            int eSize = basis->getnEdgeFunction();
            int fSize = basis->getnTriFaceFunction() + basis->getnQuadFaceFunction();
            int numFunPerEle = vSize + bSize + eSize + fSize;

            int numedges = ele->getNumEdges();
            int numvert = ele->getNumVertices();
            int numfaces = ele->getNumFaces();
            //int curlvalssize = curlvals.size();
            curlvals.resize(/*curlvalssize +*/ this->getOrderCurlBasis() * numedges, SVector3(0.0));

            const int elemType = ele->getTypeForMSH();
            int numComponents = 0;
            int numOrientations = 0;
            std::vector<double> gmsh_curlVals;

            static int _initialized = 0;
            if (!_initialized)
            {
                gmsh::initialize();
                gmsh::option::setNumber("General.Terminal",0);
                _initialized = 1;
            }
            std::vector<std::pair<int, int> > entities;
            const int dim = 3;
            gmsh::model::getEntities(entities, dim);

            // loop for multiple entities of same dim
            // possible with multiple fields defined
            if (_precomputedCurlBasisFunctionsOrientation.empty())
            {
                std::vector<int> basisFunctionsOrientation;
                for (unsigned int entitie = 0; entitie < entities.size(); ++entitie)
                {
                    const int tag = entities[entitie].second;
                    std::vector<int> tempOrientations;
                    gmsh::model::mesh::getBasisFunctionsOrientation(elemType, typeFunction, tempOrientations,
                                                                               tag);

                    for (unsigned int k = 0; k < tempOrientations.size(); ++k)
                        basisFunctionsOrientation.push_back(tempOrientations[k]);
                }
                _precomputedCurlBasisFunctionsOrientation = basisFunctionsOrientation;
                const std::vector<int> uniqueOrientationsNumbering = this->getUniqueOrientations(_precomputedCurlBasisFunctionsOrientation);
                _precomputedCurlUniqueOrientations = uniqueOrientationsNumbering;
            }

            const int numberOfOrientations = gmsh::model::mesh::getNumberOfOrientations(elemType, typeFunction);

            if (_filledElemKeys.empty())
            {
                int familyType = ElementType::getParentType(elemType);
                std::vector<GEntity *> ENTITIES;
                for (unsigned int entitie = 0; entitie < entities.size(); ++entitie)
                {
                    const int tag = entities[entitie].second;
                    if (dim >= 0 && tag >= 0)
                    {
                        GEntity *ge = GModel::current()->getEntityByTag(dim, tag);
                        ENTITIES.push_back(ge);
                    }
                    else
                        GModel::current()->getEntities(ENTITIES, dim);
                }

                for(std::size_t i = 0; i < ENTITIES.size(); i++)
                {
                    GEntity *ge = ENTITIES[i];
                    std::size_t numElementsInEntitie = ge->getNumMeshElementsByType(familyType);

                    for(std::size_t j = 0; j < numElementsInEntitie; j++)
                    {
                        MElement *e = ge->getMeshElementByType(familyType, j);
                        _filledElemKeys.push_back(e->getNum());
                    }
                }
            }

            const auto foundElem = std::find(std::begin(_filledElemKeys), std::end(_filledElemKeys), elemTag);
            if (foundElem == std::end(_filledElemKeys))
                Msg::Error("Element with tag %d not found", elemTag);
            const std::size_t elemPos = std::distance(std::begin(_filledElemKeys), foundElem);

            std::vector<int> wantedOrienForElem{_precomputedCurlUniqueOrientations[_precomputedCurlBasisFunctionsOrientation[elemPos]]};
            gmsh::model::mesh::getBasisFunctions(elemType,localCoord,typeFunction,numComponents,gmsh_curlVals,
                                                 numOrientations,wantedOrienForElem);

            SVector3 curlvaluvw(0.0);
            for (int i = 0; i < numedges; ++i)
            {
                STensorOperation::zero(curlvaluvw);
                for (int j = 0; j < 3; ++j)
                    curlvaluvw[j] = gmsh_curlVals[i * 3 + j];

                curlvals[/*curlvalssize +*/ i].operator=(curlvaluvw);
            }

            // push back sf value in cache data structure
            _precomputedCurlVals[elemTag].insert(std::make_pair(localCoord, curlvals));
        }
        else
        {
            // find elemTag in cached sf data structure from
            // earlier computations in initialization
            auto search = _precomputedCurlVals.find(elemTag);
            if (search != _precomputedCurlVals.end())
            {
                // find GP corresponding to elemTag and
                // check if CurlVals sf is filled and not empty
                auto GP = search->second.find(localCoord);
                if (GP != search->second.end())
                    curlvals = GP->second;
            }
            if(curlvals.empty())
                Msg::Error("Failed to get cached CurlValsuvw for %d element", elemTag);
        }
    }
    
    virtual void Curlfcurl(MElement *ele, double u, double v, double w, const std::string &typeFunction,std::vector<CurlCurlType> &Curlcurlvals) const
    {
      if (typeFunction != "CurlHcurlLegendre")
      {
        Msg::Error("Wrong function invoked: use fcurl() instead");
      }

        const std::size_t elemTag = ele->getNum();
        std::vector<double> localCoord{u, v, w};
        if ((_precomputedCurlCurlVals.find(elemTag) == _precomputedCurlCurlVals.end()) ||
            ((_precomputedCurlCurlVals.find(elemTag))->second.find(localCoord) == (_precomputedCurlCurlVals.find(elemTag))->second.end()) )
        {
            int eleType = ele->getType();
            if  (_eleCurlBasis.find(eleType) == _eleCurlBasis.end())
            {
                if (eleType == TYPE_TRI)
                {
                    _eleCurlBasis[eleType] = new HierarchicalBasisHcurlTria(_orderCurlBasis);
                }
                else if (eleType == TYPE_TET)
                {
                    _eleCurlBasis[eleType] = new HierarchicalBasisHcurlTetra(_orderCurlBasis);
                }
                else if (eleType == TYPE_QUA)
                {
                    _eleCurlBasis[eleType] = new HierarchicalBasisHcurlQuad(_orderCurlBasis);
                }
                else if (eleType == TYPE_HEX)
                {
                    _eleCurlBasis[eleType] = new HierarchicalBasisHcurlBrick(_orderCurlBasis);
                }
                else if (eleType == TYPE_PRI)
                {
                    _eleCurlBasis[eleType] = new HierarchicalBasisHcurlPri(_orderCurlBasis);
                }
                else
                {
                    Msg::Error("missing cases");
                }
            }

            HierarchicalBasis* basis= _eleCurlBasis[eleType];
            int vSize = basis->getnVertexFunction();
            int bSize = basis->getnBubbleFunction();
            int eSize = basis->getnEdgeFunction();
            int fSize = basis->getnTriFaceFunction() + basis->getnQuadFaceFunction();
            int numFunPerEle = vSize + bSize + eSize + fSize;

            int numedges = ele->getNumEdges();
            int numvert = ele->getNumVertices();
            int numfaces = ele->getNumFaces();
            double jac[3][3];
            const double detJ = ele->getJacobian(u, v, w, jac);
            const double invdetJ = 1.0/detJ;
            //int Curlcurlvalssize = Curlcurlvals.size();
            Curlcurlvals.resize(/*Curlcurlvalssize +*/ this->getOrderCurlBasis() * numedges, SVector3(0.0));

            const int elemType = ele->getTypeForMSH();
            int numComponents = 0;
            int numOrientations = 0;
            std::vector<double> gmsh_CurlcurlVals;

            static int _initialized = 0;
            if (!_initialized)
            {
                gmsh::initialize();
                gmsh::option::setNumber("General.Terminal",0);
                _initialized = 1;
            }
            std::vector<std::pair<int, int> > entities;
            const int dim = 3;
            gmsh::model::getEntities(entities, dim);

            // loop for multiple entities of same dim
            // possible with multiple fields defined
            if (_precomputedCurlCurlBasisFunctionsOrientation.empty())
            {
                std::vector<int> basisFunctionsOrientation;
                for (unsigned int entitie = 0; entitie < entities.size(); ++entitie)
                {
                    const int tag = entities[entitie].second;
                    std::vector<int> tempOrientations;
                    gmsh::model::mesh::getBasisFunctionsOrientation(elemType, typeFunction, tempOrientations, tag);

                    for (unsigned int k = 0; k < tempOrientations.size(); ++k)
                        basisFunctionsOrientation.push_back(tempOrientations[k]);
                }
                _precomputedCurlCurlBasisFunctionsOrientation = basisFunctionsOrientation;
                const std::vector< int > uniqueOrientationsNumbering = this->getUniqueOrientations(_precomputedCurlCurlBasisFunctionsOrientation);
                _precomputedCurlCurlUniqueOrientations = uniqueOrientationsNumbering;
            }

            const int numberOfOrientations = gmsh::model::mesh::getNumberOfOrientations(elemType, typeFunction);

            if (_filledElemKeys.empty())
            {
                int familyType = ElementType::getParentType(elemType);
                std::vector<GEntity *> ENTITIES;
                for (unsigned int entitie = 0; entitie < entities.size(); ++entitie)
                {
                    const int tag = entities[entitie].second;
                    if (dim >= 0 && tag >= 0)
                    {
                        GEntity *ge = GModel::current()->getEntityByTag(dim, tag);
                        ENTITIES.push_back(ge);
                    }
                    else
                        GModel::current()->getEntities(ENTITIES, dim);
                }

                for(std::size_t i = 0; i < ENTITIES.size(); i++)
                {
                    GEntity *ge = ENTITIES[i];
                    std::size_t numElementsInEntitie = ge->getNumMeshElementsByType(familyType);

                    for(std::size_t j = 0; j < numElementsInEntitie; j++)
                    {
                        MElement *e = ge->getMeshElementByType(familyType, j);
                        _filledElemKeys.push_back(e->getNum());
                    }
                }
            }

            const auto foundElem = std::find(std::begin(_filledElemKeys), std::end(_filledElemKeys), elemTag);
            if (foundElem == std::end(_filledElemKeys))
                Msg::Error("Element with tag %d not found", elemTag);
            const std::size_t elemPos = std::distance(std::begin(_filledElemKeys), foundElem);

            std::vector<int> wantedOrienForElem{_precomputedCurlCurlUniqueOrientations[_precomputedCurlCurlBasisFunctionsOrientation[elemPos]]};
            gmsh::model::mesh::getBasisFunctions(elemType,localCoord,typeFunction,numComponents,gmsh_CurlcurlVals,
                                                 numOrientations,wantedOrienForElem);

            SVector3 Curlcurlvaluvw(0.0);
            for (int i = 0; i < numedges; ++i)
            {
                STensorOperation::zero(Curlcurlvaluvw);
                for (int j = 0; j < 3; ++j)
                    for (int k = 0; k < 3; ++k)
                        Curlcurlvaluvw[k] += jac[j][k] * invdetJ * gmsh_CurlcurlVals[i * 3 + j];

                Curlcurlvals[/*Curlcurlvalssize +*/ i].operator=(Curlcurlvaluvw);
            }

            // push back sf value in cache data structure
            _precomputedCurlCurlVals[elemTag].insert(std::make_pair(localCoord, Curlcurlvals));
        }
        else
        {
            // find elemTag in cached sf data structure from
            // earlier computations in initialization
            auto search = _precomputedCurlCurlVals.find(elemTag);
            if (search != _precomputedCurlCurlVals.end())
            {
                // find GP corresponding to elemTag and
                // check if CurlCurlVals sf is filled and not empty
                auto GP = search->second.find(localCoord);
                if (GP != search->second.end())
                    Curlcurlvals = GP->second;
            }
            if(Curlcurlvals.empty())
                Msg::Error("Failed to get cached CurlCurlVals for %d element", elemTag);
        }
    }
    
    virtual void Curlfcurluvw(MElement *ele, double u, double v, double w, const std::string &typeFunction,std::vector<CurlCurlType> &Curlcurlvals) const
    {
      if (typeFunction != "CurlHcurlLegendre")
      {
        Msg::Error("Wrong function invoked: use fcurluvw() instead");
      }

        const std::size_t elemTag = ele->getNum();
        std::vector<double> localCoord{u, v, w};
        if ((_precomputedCurlCurlVals.find(elemTag) == _precomputedCurlCurlVals.end()) ||
            ((_precomputedCurlCurlVals.find(elemTag))->second.find(localCoord) == (_precomputedCurlCurlVals.find(elemTag))->second.end()) )
        {
            int eleType = ele->getType();
            if  (_eleCurlBasis.find(eleType) == _eleCurlBasis.end())
            {
                if (eleType == TYPE_TRI)
                {
                    _eleCurlBasis[eleType] = new HierarchicalBasisHcurlTria(_orderCurlBasis);
                }
                else if (eleType == TYPE_TET)
                {
                    _eleCurlBasis[eleType] = new HierarchicalBasisHcurlTetra(_orderCurlBasis);
                }
                else if (eleType == TYPE_QUA)
                {
                    _eleCurlBasis[eleType] = new HierarchicalBasisHcurlQuad(_orderCurlBasis);
                }
                else if (eleType == TYPE_HEX)
                {
                    _eleCurlBasis[eleType] = new HierarchicalBasisHcurlBrick(_orderCurlBasis);
                }
                else if (eleType == TYPE_PRI)
                {
                    _eleCurlBasis[eleType] = new HierarchicalBasisHcurlPri(_orderCurlBasis);
                }
                else
                {
                    Msg::Error("missing cases");
                }
            }

            HierarchicalBasis* basis= _eleCurlBasis[eleType];
            int vSize = basis->getnVertexFunction();
            int bSize = basis->getnBubbleFunction();
            int eSize = basis->getnEdgeFunction();
            int fSize = basis->getnTriFaceFunction() + basis->getnQuadFaceFunction();
            int numFunPerEle = vSize + bSize + eSize + fSize;

            int numedges = ele->getNumEdges();
            int numvert = ele->getNumVertices();
            int numfaces = ele->getNumFaces();
            //int Curlcurlvalssize = Curlcurlvals.size();
            Curlcurlvals.resize(/*Curlcurlvalssize +*/ this->getOrderCurlBasis() * numedges, SVector3(0.0));

            const int elemType = ele->getTypeForMSH();
            int numComponents = 0;
            int numOrientations = 0;
            std::vector<double> gmsh_CurlcurlVals;

            static int _initialized = 0;
            if (!_initialized)
            {
                gmsh::initialize();
                gmsh::option::setNumber("General.Terminal",0);
                _initialized = 1;
            }
            std::vector<std::pair<int, int> > entities;
            const int dim = 3;
            gmsh::model::getEntities(entities, dim);

            // loop for multiple entities of same dim
            // possible with multiple fields defined
            if (_precomputedCurlCurlBasisFunctionsOrientation.empty())
            {
                std::vector<int> basisFunctionsOrientation;
                for (unsigned int entitie = 0; entitie < entities.size(); ++entitie)
                {
                    const int tag = entities[entitie].second;
                    std::vector<int> tempOrientations;
                    gmsh::model::mesh::getBasisFunctionsOrientation(elemType, typeFunction, tempOrientations, tag);

                    for (unsigned int k = 0; k < tempOrientations.size(); ++k)
                        basisFunctionsOrientation.push_back(tempOrientations[k]);
                }
                _precomputedCurlCurlBasisFunctionsOrientation = basisFunctionsOrientation;
                const std::vector< int > uniqueOrientationsNumbering = this->getUniqueOrientations(_precomputedCurlCurlBasisFunctionsOrientation);
                _precomputedCurlCurlUniqueOrientations = uniqueOrientationsNumbering;
            }

            const int numberOfOrientations = gmsh::model::mesh::getNumberOfOrientations(elemType, typeFunction);

            if (_filledElemKeys.empty())
            {
                int familyType = ElementType::getParentType(elemType);
                std::vector<GEntity *> ENTITIES;
                for (unsigned int entitie = 0; entitie < entities.size(); ++entitie)
                {
                    const int tag = entities[entitie].second;
                    if (dim >= 0 && tag >= 0)
                    {
                        GEntity *ge = GModel::current()->getEntityByTag(dim, tag);
                        ENTITIES.push_back(ge);
                    }
                    else
                        GModel::current()->getEntities(ENTITIES, dim);
                }

                for(std::size_t i = 0; i < ENTITIES.size(); i++)
                {
                    GEntity *ge = ENTITIES[i];
                    std::size_t numElementsInEntitie = ge->getNumMeshElementsByType(familyType);

                    for(std::size_t j = 0; j < numElementsInEntitie; j++)
                    {
                        MElement *e = ge->getMeshElementByType(familyType, j);
                        _filledElemKeys.push_back(e->getNum());
                    }
                }
            }

            const auto foundElem = std::find(std::begin(_filledElemKeys), std::end(_filledElemKeys), elemTag);
            if (foundElem == std::end(_filledElemKeys))
                Msg::Error("Element with tag %d not found", elemTag);
            const std::size_t elemPos = std::distance(std::begin(_filledElemKeys), foundElem);

            std::vector<int> wantedOrienForElem{_precomputedCurlCurlUniqueOrientations[_precomputedCurlCurlBasisFunctionsOrientation[elemPos]]};
            gmsh::model::mesh::getBasisFunctions(elemType,localCoord,typeFunction,numComponents,gmsh_CurlcurlVals,
                                                 numOrientations,wantedOrienForElem);

            SVector3 Curlcurlvaluvw(0.0);
            for (int i = 0; i < numedges; ++i)
            {
                STensorOperation::zero(Curlcurlvaluvw);
                for (int j = 0; j < 3; ++j)
                    Curlcurlvaluvw[j] = gmsh_CurlcurlVals[i * 3 + j];

                Curlcurlvals[/*Curlcurlvalssize +*/ i].operator=(Curlcurlvaluvw);
            }

            // push back sf value in cache data structure
            _precomputedCurlCurlVals[elemTag].insert(std::make_pair(localCoord, Curlcurlvals));
        }
        else
        {
            // find elemTag in cached sf data structure from
            // earlier computations in initialization
            auto search = _precomputedCurlCurlVals.find(elemTag);
            if (search != _precomputedCurlCurlVals.end())
            {
                // find GP corresponding to elemTag and
                // check if CurlCurlVals sf is filled and not empty
                auto GP = search->second.find(localCoord);
                if (GP != search->second.end())
                    Curlcurlvals = GP->second;
            }
            if(Curlcurlvals.empty())
                Msg::Error("Failed to get cached CurlCurlValsuvw for %d element", elemTag);
        }
    }
};

#endif //CURLFUNCTIONSPACE_H_


