//
// C++ Interface: Function Space
//
// Description: FunctionSpace used (scalar lagragian function space in 3D)
//
//
// Author:  <G. Becker & L. Noels>, (C) 2012
//
// Copyright: See COPYING file that comes with this distribution
//
//

#include "ThreeDLagrangeFunctionSpace.h"
#include "MElement.h"
#include "MPoint.h"

std::map<int,MElement*> VertexBasedLagrangeFunctionSpace::primaryElementMap;
MElement* VertexBasedLagrangeFunctionSpace::getPrimaryElement(MElement* ele)
{
  if (ele->getPolynomialOrder() == 1) return ele;
  std::map<int,MElement*>::iterator itF = primaryElementMap.find(ele->getNum());
  if (itF != primaryElementMap.end())
  {
    return itF->second;
  }
  else
  {
    int type = ele->getType();
    std::vector<MVertex*> vver;
    for(int j=0;j<ele->getNumPrimaryVertices();j++)
    {
      vver.push_back(ele->getVertex(j));
    }
    MElement *eletmp = NULL;
    MElementFactory factory;
    switch(ele->getType())
    {
      case TYPE_PNT:
        eletmp = new MPoint(vver[0]); break;
      case TYPE_LIN:
        eletmp = factory.create(MSH_LIN_2,vver); break;
      case TYPE_TRI:
        eletmp = factory.create(MSH_TRI_3,vver); break;
      case TYPE_QUA:
        eletmp = factory.create(MSH_QUA_4,vver); break;
      case TYPE_TET:
        eletmp = factory.create(MSH_TET_4,vver); break;
      case TYPE_PYR:
        eletmp = factory.create(MSH_PYR_5,vver); break;
      case TYPE_PRI:
        eletmp = factory.create(MSH_PRI_6,vver); break;
      case TYPE_HEX:
        eletmp = factory.create(MSH_HEX_8,vver); break;
      case TYPE_POLYG:
        eletmp = factory.create(MSH_POLYG_,vver); break;
      case TYPE_POLYH:
        eletmp = factory.create(MSH_POLYH_,vver); break;
      default:
        Msg::Error("unknown element type %d",ele->getType());
    }
    primaryElementMap.insert(std::pair<int,MElement*>(ele->getNum(),eletmp));
    return eletmp;
  }
}

ThreeDLagrangeFunctionSpace::ThreeDLagrangeFunctionSpace(int id, int ncomp,bool hesscompute, bool thirdcompute) : 
    VertexBasedLagrangeFunctionSpace(id,ncomp,hesscompute,thirdcompute)
{
  comp.resize(_ncomp);
  for(int i=0; i < _ncomp; i++)
  {
    comp[i] = i;
  }
}
ThreeDLagrangeFunctionSpace::ThreeDLagrangeFunctionSpace(int id, int ncomp, int comp1_, bool hesscompute, bool thirdcompute): 
  VertexBasedLagrangeFunctionSpace(id,1,hesscompute,thirdcompute)
{
  comp.resize(1);
  comp[0] = comp1_;
};

ThreeDLagrangeFunctionSpace::ThreeDLagrangeFunctionSpace(int id, int ncomp, int comp1_, int comp2_, const bool hessc, const bool thirdc) : 
  VertexBasedLagrangeFunctionSpace(id,2,hessc,thirdc)
{
  comp.resize(2);
  comp[0] = comp1_;
  comp[1] = comp2_;
  
};

ThreeDLagrangeFunctionSpace::ThreeDLagrangeFunctionSpace(int id, int ncomp, int comp1_, int comp2_, int comp3_,
                              const bool hessc, const bool thirdc) :
                              VertexBasedLagrangeFunctionSpace(id,3,hessc,thirdc)
{
  comp.clear();
  comp.push_back(comp1_); comp.push_back(comp2_); comp.push_back(comp3_);
}
ThreeDLagrangeFunctionSpace::ThreeDLagrangeFunctionSpace(int id, int ncomp, int comp1_, int comp2_, int comp3_,
                            int comp4_,const bool hessc, const bool thirdc) : 
                            VertexBasedLagrangeFunctionSpace(id,4,hessc,thirdc)
{
  comp.clear();
  comp.push_back(comp1_); comp.push_back(comp2_); comp.push_back(comp3_); comp.push_back(comp4_);
}
ThreeDLagrangeFunctionSpace::ThreeDLagrangeFunctionSpace(int id, int ncomp, int comp1_, int comp2_, int comp3_,
                            int comp4_, int comp5_,const bool hessc, const bool thirdc) : 
                            VertexBasedLagrangeFunctionSpace(id,5,hessc,thirdc)
{
  comp.clear();
  comp.push_back(comp1_); comp.push_back(comp2_); comp.push_back(comp3_); comp.push_back(comp4_); comp.push_back(comp5_);
}
ThreeDLagrangeFunctionSpace::ThreeDLagrangeFunctionSpace(int id, int ncomp, int comp1_, int comp2_, int comp3_,
                            int comp4_, int comp5_, int comp6_, const bool hessc, const bool thirdc) : 
                            VertexBasedLagrangeFunctionSpace(id,6,hessc,thirdc)
{
  comp.clear();
  comp.push_back(comp1_); comp.push_back(comp2_); comp.push_back(comp3_); comp.push_back(comp4_); comp.push_back(comp5_); comp.push_back(comp6_);
}

IsoparametricLagrangeFunctionSpace::IsoparametricLagrangeFunctionSpace(int id) : VertexBasedLagrangeFunctionSpace(id,3,true,false)
{
  comp.clear();
  comp.push_back(0),comp.push_back(1),comp.push_back(2);
}
IsoparametricLagrangeFunctionSpace::IsoparametricLagrangeFunctionSpace(int id, int comp1_) : VertexBasedLagrangeFunctionSpace(id,1,true,false)
{
  comp.clear();
  comp.push_back(comp1_);
}
IsoparametricLagrangeFunctionSpace::IsoparametricLagrangeFunctionSpace(int id, int comp1_, int comp2_) : VertexBasedLagrangeFunctionSpace(id,2,true,false)
{
  comp.clear();
  comp.push_back(comp1_); comp.push_back(comp2_);
};