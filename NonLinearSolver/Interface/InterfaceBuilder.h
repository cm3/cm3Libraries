//
// C++ Interface: terms
//
// Description: Class to manage the creation of interface
//
//
// Author:  <Gauthier BECKER>, (C) 2011
// Author:  <Van Dung Nguyen>, (C) 2017
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef _INTERFACEBUILDER_H_
#define _INTERFACEBUILDER_H_
#include "mlaw.h"
#include "IElement.h"
class partDomain;
class manageInterface{
 public:
  typedef std::map<const interfaceKeys,IElement*> IelementContainer;
 protected:
  IelementContainer mapinter;
  std::vector<partDomain*> *_vdom;
 public:
  static unsigned long int getKey(const int npdom1, const int npdom2){
    unsigned long int i;
    if(npdom1 == npdom2){
      return npdom1;
    }
    else{
      npdom1<npdom2 ? i = npdom2*100000+npdom1 : i = npdom1*100000+npdom2;
      return i;
    }
  }
  static void getPhysDom(const unsigned long int key,int &phys1,int &phys2){
    phys2 = key%100000;
    phys1 = key/100000;
  }

  unsigned long int getKey(partDomain* dom1, partDomain *dom2);
  
  void insert(IElement *iele,partDomain *dom);
  void createinter(MElement *ele, IElement *ie, const int phys2);

  manageInterface(std::vector<partDomain*> *vdom) : _vdom(vdom){}
  ~manageInterface()
  {
    for(IelementContainer::iterator it = mapinter.begin(); it!=mapinter.end(); ++it){
      delete it->second;
    }
  }
  void erase(IElement *ie);

  IelementContainer::iterator begin(){return mapinter.begin();}
  IelementContainer::iterator end(){return mapinter.end();}

};
#endif //_INTERFACEBUILDER_H_
