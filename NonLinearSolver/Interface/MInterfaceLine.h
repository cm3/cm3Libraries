//
// C++ Interface: terms
//
// Description: Class of interface element of line used for DG
//
//
// Author:  <Gauthier BECKER>, (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
//
// Has to be merge with interface element defined in dg project HOW ??

#ifndef _MINTERFACELINE_H_
#define _MINTERFACELINE_H_
#include "MLine.h"
#include "MVertex.h"
#include "MInterfaceElement.h"
class MInterfaceLine : public MLineN, public MInterfaceElement{ // or don't derivate but in this case a vector with the vertices of interface element has to be save ??
  protected :
    // table of pointer on the two elements linked to the interface element
    MElement *_numElem[2];
    // edge's number linked to interface element of minus and plus element
    int _numEdge[2];
    // dir = true if the edge and the interface element are defined in the same sens and dir = false otherwise
    bool _dir[2];
    // constant characteristic size --> compute it at initialization
    double _hs;

  public :

    MInterfaceLine(std::vector<MVertex*> &v, int num = 0, int part = 0, MElement *e_minus = 0, MElement *e_plus = 0);

    // Destructor
    virtual ~MInterfaceLine(){}

    // Give the number of minus 0 or plus 1 element
    virtual MElement* getElem(int index) const {return _numElem[index];}

    virtual void getuvwOnElem(const double u, const double v, double &uem, double &vem, double &wem, double &uep, double &vep, double &wep) const;
    // Return the edge number of element
    virtual int getEdgeOrFaceNumber(const int i) const {return _numEdge[i];}

    // Return the local vertex number of interface
    virtual void getLocalVertexNum(const int i,std::vector<int> &vn) const;
    // Compute the characteristic size of the side h_s = max_e (area_e/perimeter_e)
    virtual double getCharacteristicSize() const {return _hs;}
    virtual bool isSameDirection(const int i) const {return _dir[i];}
    virtual int getPermutation(const int i) const{return 0;}
};
#endif // _MINTERFACELINE_H_
