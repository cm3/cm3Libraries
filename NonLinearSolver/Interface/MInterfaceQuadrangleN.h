//
// C++ Interface: terms
//
// Description: Class of interface element of triangle used for DG
//
//
// Author:  <L. Noels>, (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
//
// Has to be merge with interface element defined in dg project HOW ??

#ifndef _MINTERFACEQUADRANGLEN_H_
#define _MINTERFACEQUADRANGLEN_H_
#include "MQuadrangle.h"
#include "MLine.h"
#include "MVertex.h"
#include "MInterfaceElement.h"
class MInterfaceQuadrangleN : public MQuadrangleN, public MInterfaceElement{ // or don't derivate but in this case a vector with the vertices of interface element has to be save ??
  protected :
    // table of pointer on the two elements linked to the interface element
    MElement *_numElem[2];
    // edge's number linked to interface element of minus and plus element
    int _numFace[2];
    // dir = true if the edge and the interface element are defined in the same sens and dir = false otherwise
    bool _dir[2];

    int _permutation[2]; //element 0 should be 0 and element 1 will be 0, 1, 2 or 3 to get matches vertices
    
    double _hs;

  public :

    MInterfaceQuadrangleN(std::vector<MVertex*> &v, char order, int num = 0, int part = 0, MElement *e_minus = 0, MElement *e_plus = 0);

    // Destructor
    virtual ~MInterfaceQuadrangleN(){}

    // Give the number of minus 0 or plus 1 element
    virtual MElement* getElem(int index) const {return _numElem[index];}
    
    virtual void getuvwOnElem(const double u, const double v, double &uem, double &vem, double &wem, double &uep, double &vep, double &wep) const;

    // Return the edge number of element
    virtual int getEdgeOrFaceNumber(const int i) const {return _numFace[i];}

    // Return the local vertex number of interface
    virtual void getLocalVertexNum(const int i,std::vector<int> &vn) const;
    // Compute the characteristic size of the side h_s = max_e (area_e/perimeter_e)
    virtual double getCharacteristicSize() const {return _hs;};
    virtual bool isSameDirection(const int i) const {return _dir[i];}
    virtual int  getPermutation(const int i) const {return _permutation[i];}

};
#endif // _MINTERFACEQUADRANGLEN_H_
