//
// Description: Keys for interface creation
//
//
// Author:  < Gauthier Becker, Van Dung NGUYEN>, (C) 2017
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef _INTERFACEKEYS_H_
#define _INTERFACEKEYS_H_
#include <vector>
class interfaceKeys{
  private:
    int dim;
    std::vector<int> comp; // comp[0] >= comp[1] >= ... >= comp[dim-1]
 public:
  interfaceKeys(): dim(0),comp(0){}
  ~interfaceKeys(){}
  void setValues(const int i1, const int i2);
  void setValues(const int i1, const int i2, const int i3);
  void setValues(const int i1, const int i2, const int i3, const int i4);

  bool operator==(const interfaceKeys &key)const;
  bool operator < (const interfaceKeys& key) const;
};

#endif // _INTERFACEKEYS_H_
