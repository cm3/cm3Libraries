//
//
// Description: compute interface localb basis
//
//
// Author:  <Van Dung NGUYEN>, (C) 2017
//
// Copyright: See COPYING file that comes with this distribution
//
//

#include "MElement.h"
#include "SVector3.h"


void computeInterfaceReferenceLocalBasis(const MElement* ele, const IntPt* GP, SVector3& n, SVector3& t, SVector3& b);