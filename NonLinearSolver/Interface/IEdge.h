//
// C++ Interface: terms
//
// Description: Class to bluid an interface line (used at initialization)
//
//
// Author:  <Gauthier BECKER>, (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef _IEDEGE_H_
#define _IEDEGE_H_
#include "IElement.h"
#include "MVertex.h"
#include "MElement.h"
// Class used to build 2D interface element
class IEdge : public IElement{
  public :
    IEdge(std::vector<MVertex*> &v,MElement *e, partDomain *dom) : IElement(v,e,dom){
      _key.setValues(v[0]->getNum(),v[1]->getNum());
    };
    ~IEdge(){};
    virtual IElement::IEtype getType()const{return IElement::Edge;}
    // To build a set of IEdge (needed for creation on interface in MPI case)
    bool operator<(const IEdge &_other) const{ return _key < _other._key;}
};
#endif // _IEDEGE_H_
