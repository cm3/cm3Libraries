//
// Description: Keys for interface creation
//
//
// Author:  <Gauthier BECKER, Van Dung NGUYEN>, (C) 2017
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "InterfaceKeys.h"
#include <functional>
#include <algorithm>
#include "GmshMessage.h"

void interfaceKeys::setValues(const int i1, const int i2){
  dim = 2;
  comp.resize(2);
  if (i1 > i2) {
    comp[0] = i1; 
    comp[1] = i2;
  }
  else{
    comp[0] = i2;
    comp[1] = i1;
  }
};
void interfaceKeys::setValues(const int i1, const int i2, const int i3){
  dim = 3;  
  comp.resize(3);
  comp[0] = i1;
  comp[1] = i2;
  comp[2] = i3;
  std::sort(comp.begin(),comp.end(),std::greater<int>());
};
void interfaceKeys::setValues(const int i1, const int i2, const int i3, const int i4){
  dim = 4;  
  comp.resize(4);
  comp[0] = i1;
  comp[1] = i2;
  comp[2] = i3;
  comp[3] = i4;
  std::sort(comp.begin(),comp.end(),std::greater<int>());
};

bool interfaceKeys::operator==(const interfaceKeys &key)const{
 if (dim == 2){
   return ((comp[0] == key.comp[0]) && (comp[1] == key.comp[1]));
 } 
 else if (dim == 3){
   return ((comp[0] == key.comp[0]) && (comp[1] == key.comp[1]) && (comp[2] == key.comp[2]));
 }
 else if (dim == 4){
    return ((comp[0] == key.comp[0]) && (comp[1] == key.comp[1]) && (comp[2] == key.comp[2]) && (comp[3] == key.comp[3]));
 }
 else
   Msg::Error("Missing case in interfaceKeys::operator = \n");
 return false;
};

bool interfaceKeys::operator < (const interfaceKeys& key) const{
  if (dim == 2){
    if (comp[0] < key.comp[0]) return true;
    if (comp[0] > key.comp[0]) return false;
    if (comp[1] < key.comp[1]) return true;
    return false;
  }
  else if (dim == 3){
    if (comp[0] < key.comp[0]) return true;
    if (comp[0] > key.comp[0]) return false;
    if (comp[1] < key.comp[1]) return true;
    if (comp[1] > key.comp[1]) return false;
    if (comp[2] < key.comp[2]) return true;
    return false;
  }
  else if (dim == 4){
    if (comp[0] < key.comp[0]) return true;
    if (comp[0] > key.comp[0]) return false;
    if (comp[1] < key.comp[1]) return true;
    if (comp[1] > key.comp[1]) return false;
    if (comp[2] < key.comp[2]) return true;
    if (comp[2] > key.comp[2]) return false;
    if (comp[3] < key.comp[3]) return true; 
    return false;
  }
  else
     printf("Missing case in interfaceKeys::operator < \n");
  return false;
};

