//
// C++ Interface: terms
//
// Description: Class of interface element of line used for DG
//
//
// Author:  <L. Noels>, (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
//
// Has to be merge with interface element defined in dg project HOW ??

#include "MInterfaceTriangleN.h"
MInterfaceTriangleN::MInterfaceTriangleN(std::vector<MVertex*> &v, char order, int num, int part,
                               MElement *e_minus, MElement *e_plus) : MTriangleN(v, order, num, part), MInterfaceElement()
{
  _numElem[0]=e_minus;
  _numElem[1]=e_plus;
  // Face of element linked to interface element
  std::vector<MVertex*> vv;
  for(int jj=0;jj<2;jj++){
    for(int i = 0; i < _numElem[jj]->getNumFaces(); i++) // Loop on the four faces
    {
      vv.clear();
      _numElem[jj]->getFaceVertices(i,vv);
      if (vv[0] == v[0] and vv[1] == v[1] and vv[2] == v[2])
      {
        _numFace[jj] = i;
        _dir[jj] = true;
        _permutation[jj] = 0;
      }
      else if (vv[1] == v[0] and vv[2] == v[1] and vv[0] == v[2])
      {
        _numFace[jj] = i;
        _dir[jj] = true;
        _permutation[jj] = 1;
      }
      else if (vv[2] == v[0] and vv[0] == v[1] and vv[1] == v[2])
      {
        _numFace[jj] = i;
        _dir[jj] = true;
        _permutation[jj] = 2;
      }
      else if (vv[0] == v[0] and vv[1] == v[2] and vv[2] == v[1])
      {
        _numFace[jj] = i;
        _dir[jj] = false;
        _permutation[jj] = 0;
      }
      else if (vv[1] == v[0] and vv[2] == v[2] and vv[0] == v[1])
      {
        _numFace[jj] = i;
        _dir[jj] = false;
        _permutation[jj] = 1;
      }
      else if (vv[2] == v[0] and vv[0] == v[2] and vv[1] == v[1])
      {
        _numFace[jj] = i;
        _dir[jj] = false;
        _permutation[jj] = 2;
      }
    }
  }
  
  _hs = MTriangleN::getInnerRadius()/(MTriangleN::getPolynomialOrder()*MTriangleN::getPolynomialOrder());
}

void MInterfaceTriangleN::getLocalVertexNum(const int i,std::vector<int> &vn) const
{

  std::vector<MVertex*> vvelem;
  _numElem[i]->getVertices(vvelem);
  int faceNb = _numFace[i];
  std::vector<MVertex*> vvface;
  /*if(_numElem[i]->getTypeForMSH() == MSH_TET_10)
  {
    std::vector<MVertex*> vvv;
    _numElem[i]->getFaceVertices(faceNb,vvv);
    vvface.push_back(vvv[0]);
    vvface.push_back(vvv[1]);
    vvface.push_back(vvv[2]);
    if(faceNb==0)
    {
      vvface.push_back(vvv[5]);
      vvface.push_back(vvv[4]);
      vvface.push_back(vvv[3]);
    }
    else if(faceNb==1)
    {
      vvface.push_back(vvv[3]);
      vvface.push_back(vvv[5]);
      vvface.push_back(vvv[4]);
    }
    else if(faceNb==2)
    {
      vvface.push_back(vvv[4]);
      vvface.push_back(vvv[5]);
      vvface.push_back(vvv[3]);
    }
    else
    {
      vvface.push_back(vvv[5]);
      vvface.push_back(vvv[3]);
      vvface.push_back(vvv[4]);
    }
  }
  else*/
    _numElem[i]->getFaceVertices(faceNb,vvface);
  int nbVertex = vvface.size();
  for( int k = 0; k< nbVertex ; k++)
  {
     for( int l =0; l < vvelem.size(); l++)
     {
        if(vvelem[l] == vvface[k])
        {
          vn[k] = l;
          break;
        }
     }
  }
}

  // Get the u v w value on element for abscissa u v on the interface element
void MInterfaceTriangleN::getuvwOnElem(const double u, const double v, double &uem, double &vem, double &wem, double &uep, double &vep, double &wep) const
{
  for(int jj=0;jj<2;jj++)
  {
    SPoint3 p;
    this->pnt(u,v, 0., p);
    double xyz[3];
    p.getPosition(&xyz[0],&xyz[1],&xyz[2]);
    double uvw[3];
    _numElem[jj]->xyz2uvw(xyz,uvw);

    if(jj==0){uem=uvw[0];vem=uvw[1];wem=uvw[2];}
    else {uep=uvw[0];vep=uvw[1];wep=uvw[2];}
  }
}
