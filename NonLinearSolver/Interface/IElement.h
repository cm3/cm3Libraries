//
// C++ Interface: terms
//
// Description: Class to bluid the interface element (used at initialization)
//
//
// Author:  <Gauthier BECKER>, (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef _IELEMENT_H_
#define _IELEMENT_H_
#include "MVertex.h"
#include "MElement.h"
#include "InterfaceKeys.h"
class partDomain;
class IElement{
 public:
  enum IEtype{Edge, Face};
 protected:
  std::vector<MVertex*> vertex;
  MElement *elem;
  partDomain *_dom;
  interfaceKeys _key;
 public:
  IElement(std::vector<MVertex*> &v,MElement *e, partDomain *dom) : vertex(v), elem(e), _dom(dom),_key(){};
  virtual ~IElement(){};
  const interfaceKeys& getkey() const {return _key;};
  virtual IEtype getType() const=0;
  // As for now only IEdge exists I don't know if these method are virtual pure
  // virtual or are only for IEdge
  virtual std::vector<MVertex*> getVertices() const{return vertex;}
  virtual MElement* getElement() const{return elem;}
  virtual int getPhys() const;
  virtual partDomain* getDomain() const{return _dom;}

};
#endif // _IELEMENT_H_
