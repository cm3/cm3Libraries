//
// Description: Class of interface element of line used for DG
//
//
// Author:  <Van Dung NGUYEN>, (C) 2016
//
// Copyright: See COPYING file that comes with this distribution
//
//
// THIS CLASS IS NOT COMPLETE !
// ALL FUNCTIONS NEED TO BE REDEFINED
//

#ifndef _MINTERFACEPOINT_H_
#define _MINTERFACEPOINT_H_
#include "MPoint.h"
#include "MInterfaceElement.h"
class MInterfacePoint : public MPoint, public MInterfaceElement{ // or don't derivate but in this case a vector with the vertices of interface element has to be save ??
  protected :
    // table of pointer on the two elements linked to the interface element
    MElement *_numElem[2];
    // constant characteristic size --> compute it at initialization
    double _hs;

  public :

    MInterfacePoint(MVertex* v, int num = 0, int part = 0, MElement *e_minus = NULL, MElement *e_plus = NULL):
      MPoint(v,num,part),MInterfaceElement(){
      _numElem[0] = e_minus;
      _numElem[1] = e_plus;


      if (e_minus!=NULL and e_plus!=NULL){
        _hs = std::min(e_minus->getVolume(),e_plus->getVolume());
      }

    };

    // Destructor
    virtual ~MInterfacePoint(){}

    // Give the number of minus 0 or plus 1 element
    virtual MElement* getElem(int index) const {return _numElem[index];}

    virtual void getuvwOnElem(const double u, const double v, double &uem, double &vem, double &wem, double &uep, double &vep, double &wep) const{
      Msg::Error("MInterfacePoint::getuvwOnElem has not been implemented");
    };
    // Return the edge number of element
    virtual int getEdgeOrFaceNumber(const int i) const {
      Msg::Error("MInterfacePoint::getEdgeOrFaceNumber has not been implemented");
      return 0;
    }

    // Return the local vertex number of interface
    virtual void getLocalVertexNum(const int i,std::vector<int> &vn) const{};
    // Compute the characteristic size of the side h_s = max_e (area_e/perimeter_e)
    virtual double getCharacteristicSize() const {return _hs;}
    virtual bool isSameDirection(const int i) const {
      Msg::Error("MInterfacePoint::isSameDirection has not been implemented");
      return true;
    }
    virtual int getPermutation(const int i) const{
      Msg::Error("MInterfacePoint::getPermutation has not been implemented");
      return 0;
    }
};
#endif // _MINTERFACEPOINT_H_
