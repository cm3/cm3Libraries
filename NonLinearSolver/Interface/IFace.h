//
// C++ Interface: terms
//
// Description: Class to bluid an interface face (used at initialization)
//
//
// Author:  <L. Noels>, (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef _IFACE_H_
#define _IFACE_H_
#include "IElement.h"
#include "MVertex.h"
#include "MElement.h"
#include "InterfaceKeys.h"
// Class used to build 3D interface element
class IFace : public IElement{
  public :
    IFace(std::vector<MVertex*> &v,MElement *e, partDomain *dom);
    ~IFace(){
    };
    virtual IElement::IEtype getType()const{return IElement::Face;}
    bool operator<(const IFace &_other) const{ return _key < _other._key;};
};
#endif // _IFACE_H_
