//
// C++ Interface: terms
//
// Description: Class to bluid an interface face (used at initialization)
//
//
// Author:  <L. Noels>, (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "IFace.h"

IFace::IFace(std::vector<MVertex*> &v,MElement *e, partDomain *dom) : IElement(v,e,dom)
{
      int i1,i2,i3,i4;
      if(e->getType() == TYPE_TET)
      {
        i1 = vertex[0]->getNum();
        i2 = vertex[1]->getNum();
        i3 = vertex[2]->getNum();
        _key.setValues(i1,i2,i3);
      }
      else if(e->getType() == TYPE_HEX)
      {
        i1 = vertex[0]->getNum();
        i2 = vertex[1]->getNum();
        i3 = vertex[2]->getNum();
        i4 = vertex[3]->getNum();
        _key.setValues(i1,i2,i3,i4);
      }
      else if (e->getType() == TYPE_PRI){
        if (v.size() == 3 or v.size() == 6){
          i1 = vertex[0]->getNum();
          i2 = vertex[1]->getNum();
          i3 = vertex[2]->getNum();
          _key.setValues(i1,i2,i3);
        }
        else if (v.size() == 4 or v.size() == 9){
          i1 = vertex[0]->getNum();
          i2 = vertex[1]->getNum();
          i3 = vertex[2]->getNum();
          i4 = vertex[3]->getNum();
          _key.setValues(i1,i2,i3,i4);
        }
        else{
          Msg::Error("missing case in IFace with TYPE_PRI");
        }
      }
      else 
      {
		Msg::Error("missing case in IFace");
      }
}



