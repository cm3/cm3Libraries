//
// C++ Interface: terms
//
// Description: Class to bluid the interface element (used at initialization)
//
//
// Author:  <Gauthier BECKER>, (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "IElement.h"
#include "partDomain.h"
int IElement::getPhys() const {return _dom->getPhysical();}
