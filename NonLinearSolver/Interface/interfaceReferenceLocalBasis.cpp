//
//
// Description: compute interface localb basis
//
//
// Author:  <Van Dung NGUYEN>, (C) 2017
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "interfaceReferenceLocalBasis.h"
#include "MInterfaceElement.h"
#include "functionSpace.h"

void computeInterfaceReferenceLocalBasis(const MElement* ele, const IntPt* GP, SVector3& n, SVector3& t, SVector3& b){
	//printf("begin computation local basis \n");
	
	const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
	int nbvertexInter = ele->getNumVertices();
	
	std::vector<TensorialTraits<double>::GradType> Grads;
	double gradsuvw[256][3];
	ele->getGradShapeFunctions((*GP).pt[0], (*GP).pt[1], (*GP).pt[2], gradsuvw);
	for(int i = 0; i < nbvertexInter; ++i){
		Grads.push_back(TensorialTraits<double>::GradType(gradsuvw[i][0], gradsuvw[i][1], gradsuvw[i][2]));
	}
	
	SVector3 referencePhi0[2];
	if (ele->getDim() == 2){
		for(int k=0;k<nbvertexInter;k++)
		{
			double x = ele->getVertex(k)->x();
			double y = ele->getVertex(k)->y();
			double z = ele->getVertex(k)->z();
			//evaulate displacements at interface
			for(int i=0;i<2;i++)
			{
			 referencePhi0[i](0) += Grads[k](i)*x;
			 referencePhi0[i](1) += Grads[k](i)*y;
			 referencePhi0[i](2) += Grads[k](i)*z;
			}
		}
	}
	else if (ele->getDim() == 1){
		for(int k=0;k<nbvertexInter;k++)
		{
			double x = ele->getVertex(k)->x();
			double y = ele->getVertex(k)->y();
			double z = ele->getVertex(k)->z();
			 referencePhi0[0](0) += Grads[k](0)*x;
			 referencePhi0[0](1) += Grads[k](0)*y;
			 referencePhi0[0](2) += Grads[k](0)*z;
		}

		SVector3 phi0[2];
		
		SPoint3 P;
		ele->pnt((*GP).pt[0], (*GP).pt[1], (*GP).pt[2],P);		
		const MElement* em = iele->getElem(0);
		double uvw[3];
		em->xyz2uvw(P.data(),uvw);
		std::vector<TensorialTraits<double>::GradType> gradmUVW;
		em->getGradShapeFunctions(uvw[0], uvw[1], uvw[2], gradsuvw);
		int nbFFm = em->getNumShapeFunctions();
		for(int i = 0; i < nbFFm; ++i){
			gradmUVW.push_back(TensorialTraits<double>::GradType(gradsuvw[i][0], gradsuvw[i][1], gradsuvw[i][2]));
		}
		
		for(int k=0;k<nbFFm;k++)
		{
			double x = em->getVertex(k)->x();
			double y = em->getVertex(k)->y();
			double z = em->getVertex(k)->z();
			//evaulate displacements at interface
			for(int i=0;i<2;i++)
			{
			 phi0[i](0) += gradmUVW[k](i)*x;
			 phi0[i](1) += gradmUVW[k](i)*y;
			 phi0[i](2) += gradmUVW[k](i)*z;
			}
		}
		referencePhi0[1] = crossprod(phi0[0],phi0[1]);
		referencePhi0[1].normalize();
	}
	
	n =  crossprod(referencePhi0[0],referencePhi0[1]); n.normalize();
	t = referencePhi0[0]; t.normalize();
	b = crossprod(n,t); b.normalize();
	
	//printf("local basis em = %d, ep = %d: n = [%e %e %e], t = [%e %e %e] ; b =[%e %e %e]\n",
	//			iele->getElem(0)->getNum(),iele->getElem(1)->getNum(),n[0],n[1],n[2],t[0],t[1],t[2],b[0],b[1],b[2]);
};