//
// Description: special assembly function for mpi communication
//
//
// Author:  <Gauthier BECKER>, (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef NLMPIALGORITHMS_H_
#define NLMPIALGORITHMS_H_
#include "interFunctionSpace.h"
#include "GmshConfig.h"
#if defined(HAVE_MPI)

template<class Assembler> void NumberInterfaceDofsMPI(interFunctionSpace *space, MInterfaceElement *iele,Assembler* pAssembler)
{
  int rankMinus = iele->getElem(0)->getPartition()-1; // because rank == partitionNumber -1
  int rankPlus =  iele->getElem(1)->getPartition()-1; // idem
  if(rankMinus!= rankPlus) // otherwise it is an interface domain located in the same partition and there is noting to do
  {
    // get the keys of the two elements
    std::vector<Dof> Rminus;
    std::vector<Dof> Rplus;
    space->getKeys(iele,Rminus,Rplus);
    if(rankMinus == Msg::GetCommRank())
    {
      //pAssembler->manageMPIComm(Rminus,rankPlus,Rplus);
      pAssembler->manageMPIComm(rankPlus,Rplus);
    }
    else
    {
      //pAssembler->manageMPIComm(Rplus,rankMinus,Rminus);
      pAssembler->manageMPIComm(rankMinus,Rminus);
    }

  }
}

template<class Iterator, class Assembler> void SparsityInterfaceDofsMPI(FunctionSpaceBase *space, Iterator itbegin, Iterator itend,
                                                                        Assembler* pAssembler)
{
  interFunctionSpace *ispace = dynamic_cast<interFunctionSpace*>(space);
  #ifdef _DEBUG
  if(ispace == NULL){
    Msg::Error("derive your space from interFunctionSpace to use NumberInterfaceDofsMPI");
    return;
  }
  #endif // _DEBUG
  for (Iterator it=itbegin;it!=itend;++it)
  {
    MInterfaceElement *iele = dynamic_cast<MInterfaceElement*>(it->second);
    if(!iele) return;
    int rankMinus = iele->getElem(0)->getPartition()-1; // because rank == partitionNumber -1
    int rankPlus =  iele->getElem(1)->getPartition()-1; // idem
    if(rankMinus!= rankPlus) // otherwise it is an interface domain located in the same partition and there is noting to do
    {
      // get the keys of the two elements
      std::vector<Dof> Rminus;
      std::vector<Dof> Rplus;
      ispace->getKeys(iele,Rminus,Rplus);
      if(rankMinus == Msg::GetCommRank())
      {
        pAssembler->manageMPISparsity(Rminus,rankPlus,Rplus);
        //pAssembler->manageMPIComm(rankPlus,Rplus);
      }
      else
      {
        pAssembler->manageMPISparsity(Rplus,rankMinus,Rminus);
        //pAssembler->manageMPIComm(rankMinus,Rminus);
      }
    }
  }
}

template<class Iterator, class Assembler> void NumberInterfaceDofsMPI(FunctionSpaceBase *space, Iterator itbegin, Iterator itend,
                                                                      Assembler* pAssembler)
{
  interFunctionSpace *ispace = dynamic_cast<interFunctionSpace*>(space);
  #ifdef _DEBUG
  if(ispace == NULL){
    Msg::Error("derive your space from interFunctionSpace to use NumberInterfaceDofsMPI");
  }
  else
  #endif // _DEBUG
  {
    for (Iterator it=itbegin;it!=itend;++it)
    {
      MInterfaceElement *iele = dynamic_cast<MInterfaceElement*>(it->second);
      if(iele != NULL)
        NumberInterfaceDofsMPI(ispace,iele,pAssembler);
      #ifdef _DEBUG
      else{
        Msg::Error("Can't create interface dof for MPI because the iterator is not on interface element");
      }
      #endif
    }
  }
}
#endif // HAVE_MPI
#endif //NLMPIALGORITHMS_H_
