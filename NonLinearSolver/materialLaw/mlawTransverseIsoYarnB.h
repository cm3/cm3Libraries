//
// C++ Interface: material law
//              Transverse Isotropic law for large strains it is a "virtual implementation" you have to define a law
//              in your project which derive from this law. you have to do THE SAME for your IPVariable
//              which have to derive from IPIPTransverseIsotropic
//              Implementation follows J. Bonet, AJ Burton, CMAME 1998, 162, 151-164
//
//              Rotation Angles and defined by the location of Gauss Point
// Author:  <Ling Wu , Xing Liu>, (C) 2016
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef MLAWTRANSVERSEISOTROPICYarnB_H_
#define MLAWTRANSVERSEISOTROPICYarnB_H_
#include "mlaw.h"
#include "STensor3.h"
#include "STensor43.h"
//#include "MatrixTools.h"
#include "ipTransverseIsoYarnB.h"


class mlawTransverseIsoYarnB: public materialLaw
{
 protected:
  int _physicnum; //Yarn index
  double _rho;
  Yarn _YD; //Yarn
  vector<int> _Eid_Physicnum_table; //The position is the index of the element and
                                        //the value is the physical number.


  double _CriticalrDSvDT;
  /*In the model, the expression Exp(-DT/DS)*Exp(DT) is used due to the strain
   ratio related constitutive law. When zero strain happens somewhere, the term
   DT/DS make an infinity error. Fortunately, Exp(-DT/DS) converges to 0 when
   DT/DS goes to infinity. The variable _CriticalrDSvDT is the critical number
   such that when DS/DT < CriticalrDSvDT, the computation DT/DS will be avoid
   and Exp(-DT/DS) is set to 0.*/

  double _MaxStrainRate; //The maximal value of the strain rate

  int _IsUniDir;
  vector<double> _UniDir;
 //*********Fitted parameters*********
 //---Detailed information, could refer to the article:
 //'Nonlinear dynamic constitutive model for carbon/yarn textile composites'

  //fitted data
  double _Cxx11;
  double _Cxx21;
  double _Cxx22;
  double _Cxx31;
  double _Cxx32;
  double _Cyy11;
  double _Cyy21;
  double _Cyy22;
  double _Cyy31;
  double _Cyy32;
  double _Cxy11;
  double _Cxy21;
  double _Cxy22;
  double _Cxy31;
  double _Cxy32;
  double _Cyz11;
  double _Cyz21;
  double _Cyz22;
  double _Cyz31;
  double _Cyz32;

  //Poisson's ratio
  double _nuxy;
  double _nuxz;
  double _nuyz;

  double _CArray[9][5];//This array stores all the values above
 /*The x,y,z are local coordinates. x is the tangent direction of the center
   line of the yarn, y and z are two directions on the cross section plane
   perpendicular to x. The parameter Czzmn is the same as Cyymn, Cxymn is the
   same as Cxzmn, thus we do not repeat these parameters.*/


 public:
 //Constructor for yarn
  mlawTransverseIsoYarnB(int lawnum,          //tag of the material law
                         double rho,          //material density
                         int physicnum,       //physic number of
                         string yarnInfo,     //vector container of YarnDirections
                         int isunidir,  //
                         double rDSvDT,       //To initialize '_CriticalrDSvDT'
                         double MaxStrainRate, //The maximal value of the strain rate
			 const double Cxx11,
		         const double Cxx21,
			 const double Cxx22,
			 const double Cxx31,
			 const double Cxx32,
			 const double Cyy11,
			 const double Cyy21,
			 const double Cyy22,
			 const double Cyy31,
			 const double Cyy32,
			 const double Cxy11,
			 const double Cxy21,
			 const double Cxy22,
			 const double Cxy31,
			 const double Cxy32,
			 const double Cyz11,
			 const double Cyz21,
			 const double Cyz22,
			 const double Cyz31,
			 const double Cyz32,
                         const double nuxy,
                         const double nuxz,
                         const double nuyz);


 //Constructor for matrix
  mlawTransverseIsoYarnB(int lawnum,          //tag of the material law
                         double rho,          //material density
                         int physicnum,       //physic number of
                         double rDSvDT,       //To initialize '_CriticalrDSvDT'
                         double MaxStrainRate, //The maximal value of the strain rate
			 const double Cxx11,
		         const double Cxx21,
			 const double Cxx22,
			 const double Cxx31,
			 const double Cxx32,
			 const double Cyy11,
			 const double Cyy21,
			 const double Cyy22,
			 const double Cyy31,
			 const double Cyy32,
			 const double Cxy11,
			 const double Cxy21,
			 const double Cxy22,
			 const double Cxy31,
			 const double Cxy32,
			 const double Cyz11,
			 const double Cyz21,
			 const double Cyz22,
			 const double Cyz31,
			 const double Cyz32,
                         const double nuxy,
                         const double nuxz,
                         const double nuyz);
 #ifndef SWIG

 ~mlawTransverseIsoYarnB(){}
  mlawTransverseIsoYarnB(const mlawTransverseIsoYarnB &source);
  mlawTransverseIsoYarnB& operator=(const materialLaw &source);
  int get_PhysicNum() const{return _physicnum;}
  // function of materialLaw
  virtual materialLaw* clone() const {return new mlawTransverseIsoYarnB(*this);};
  virtual void checkInternalState(IPVariable* ipv, const IPVariable* ipvprev) const{}; // do nothing
  virtual bool withEnergyDissipation() const {return false;};

  virtual Yarn get_Yarn() const{return _YD;}
  virtual vector<double> get_UniDir() const{return _UniDir;}
  virtual int get_IsUniDir() const{return _IsUniDir;}

  virtual void createIPState(IPStateBase* &ips,bool hasBodyForce, const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const IntPt *GP=NULL, const int gpt =0) const;
  virtual void initLaws(const std::map<int,materialLaw*> &maplaw){}; // this law is initialized so nothing to do
  virtual double soundSpeed() const; // default but you can redefine it for your case
  virtual matname getType() const {return materialLaw::transverseIsoYarnB;}

  //specific function
  virtual void constitutive(const STensor3 &F0,
                            const STensor3 &Fn1,            // updated deformation gradient (input @ time n+1)
                            STensor3 &P,                    // 1st Piola-Kirchoff stress
			    const IPVariable *q0,       // internal point variable @ time n
		            IPVariable *q1,       // internal point will be updated @ time n+1
                            STensor43 &Tangent,
                            const bool stiff,                // if true compute the tangents
                            STensor43* elasticTangent = NULL, 
                            const bool dTangent =false,
                            STensor63* dCalgdeps = NULL) const;

 #endif // SWIG
};

#endif // MLAWTRANSVERSEISOTROPICYarnB_H_
