/*********************************************************************************************/
/** Copyright (C) 2022 - See COPYING file that comes with this distribution	                **/
/** Author: Mohamed HADDAD (MEMA-iMMC-UCLouvain)							                              **/
/** Contact: mohamed.haddad@uclouvain.be					                                      		**/
/**											                                                                  	**/
/** C++ Interface: material law                                                             **/
/** Description: ViscoElastic-ViscoPlastic material law (small strains)             				**/
/** See B.Miled et I.Doghri 2011                                                            **/
/**********************************************************************************************/

#include <math.h>
#include "MInterfaceElement.h"
#include <fstream>
#include "STensorOperations.h"
#include <cstring>
#include "nonLinearMechSolver.h"

#include "mlawVEVPMFH.h"
#include "UsefulFunctionsVEVPMFH.h"

mlawVEVPMFH::mlawVEVPMFH(const int num, const double rho, const char *propName) : materialLaw(num,true), _rho(rho)
{
        
    std::ifstream in(propName);
      if(!in)
      {
          Msg::Error("Cannot open the file %s! Maybe is missing",propName);
          std::exit(EXIT_FAILURE);
      }
      
    inputfilename = propName;
    VEVPMFHSPACE::readinput(&matrprop, &inclprop, propName);
};
  
  
mlawVEVPMFH::mlawVEVPMFH(const mlawVEVPMFH &source) : materialLaw(source), _rho(source._rho)
{
     _rho = source._rho;
     inputfilename = source.inputfilename;
     
     VEVPMFHSPACE::readinput (&matrprop, &inclprop, source.inputfilename);
     matrprop.E0 = (source.matrprop).E0;
     matrprop.nu = (source.matrprop).nu;
    
     matrprop.yldstrs = (source.matrprop).yldstrs;
     matrprop.hardmodel = (source.matrprop).hardmodel;
     matrprop.hardmodulus1 = (source.matrprop).hardmodulus1;
     matrprop.hardmodulus2 = (source.matrprop).hardmodulus2;
     matrprop.hardexp = (source.matrprop).hardexp;
     
     matrprop.viscmodel = (source.matrprop).viscmodel;
     matrprop.viscexp1 = (source.matrprop).viscexp1;
     matrprop.viscexp2 = (source.matrprop).viscexp2;
     matrprop.visccoef = (source.matrprop).visccoef;
     
     matrprop.v = (source.matrprop).v;
     matrprop.ar = (source.matrprop).ar;
     
     int i;
     for(i=0; i<3; i++){
       matrprop.orient[i] = (source.matrprop).orient[i];
     }
     
     matrprop.G0 = (source.matrprop).G0;
     matrprop.G_inf = (source.matrprop).G_inf;
     matrprop.nbrGi = (source.matrprop).nbrGi;
     
     for(i=0; i< matrprop.nbrGi; i++){
       matrprop.Gi[i] = (source.matrprop).Gi[i];
       matrprop.gi[i] = (source.matrprop).gi[i];
     }
     
     matrprop.K0 = (source.matrprop).K0;
     matrprop.K_inf = (source.matrprop).K_inf;
     matrprop.nbrKj = (source.matrprop).nbrKj;
     
     for(i=0; i<matrprop.nbrKj ; i++){
       matrprop.Kj[i] = (source.matrprop).Kj[i];
       matrprop.kaj[i] = (source.matrprop).kaj[i];
     }
     
};


mlawVEVPMFH& mlawVEVPMFH::operator=(const materialLaw &source)
{

     materialLaw::operator=(source);
     const mlawVEVPMFH* src =static_cast<const mlawVEVPMFH*>(&source);
     
     //copy material structures
     _rho = src->_rho;
     matrprop.E0 = (src->matrprop).E0;
     matrprop.nu = (src->matrprop).nu;
    
     matrprop.yldstrs = (src->matrprop).yldstrs;
     matrprop.hardmodel = (src->matrprop).hardmodel;
     matrprop.hardmodulus1 = (src->matrprop).hardmodulus1;
     matrprop.hardmodulus2 = (src->matrprop).hardmodulus2;
     matrprop.hardexp = (src->matrprop).hardexp;
     
     matrprop.viscmodel = (src->matrprop).viscmodel;
     matrprop.viscexp1 = (src->matrprop).viscexp1;
     matrprop.viscexp2 = (src->matrprop).viscexp2;
     matrprop.visccoef = (src->matrprop).visccoef;
     
     matrprop.v = (src->matrprop).v;
     matrprop.ar = (src->matrprop).ar;
     
     int i;
     for(i=0; i<3; i++){
       matrprop.orient[i] = (src->matrprop).orient[i];
     }
     
     matrprop.G0 = (src->matrprop).G0;
     matrprop.G_inf = (src->matrprop).G_inf;
     matrprop.nbrGi = (src->matrprop).nbrGi;
     
     for(i=0; i< matrprop.nbrGi; i++){
       matrprop.Gi[i] = (src->matrprop).Gi[i];
       matrprop.gi[i] = (src->matrprop).gi[i];
     }
     
     matrprop.K0 = (src->matrprop).K0;
     matrprop.K_inf = (src->matrprop).K_inf;
     matrprop.nbrKj = (src->matrprop).nbrKj;
     
     for(i=0; i<matrprop.nbrKj ; i++){
       matrprop.Kj[i] = (src->matrprop).Kj[i];
       matrprop.kaj[i] = (src->matrprop).kaj[i];
     }
  

    return *this;
};


mlawVEVPMFH::~mlawVEVPMFH()
{
    //Destructor
    free(matrprop.Gi);
    free(matrprop.gi);
    free(matrprop.Kj);
    free(matrprop.kaj);
};


  
void mlawVEVPMFH::createIPState(IPStateBase* &ips,bool hasBodyForce,const bool* state_,const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
  IPVariable* ivi = new IPVEVPMFH();
  IPVariable* iv1 = new IPVEVPMFH();
  IPVariable* iv2 = new IPVEVPMFH();
     
  if(ips != NULL) delete ips;
  ips = new IP3State(state_,ivi,iv1,iv2);
};


double mlawVEVPMFH::soundSpeed() const
{
  double nu = matrprop.nu;
  double E = matrprop.E0;
  double mu = (E*nu)/((1+nu)*(1-2*nu));
  double factornu = (1.-nu)/((1.+nu)*(1.-2.*nu));
  
  return sqrt(E*factornu/_rho);

};



void mlawVEVPMFH::constitutive(const STensor3& F0,const STensor3& Fn,STensor3 &P,const IPVariable *ipvprevi,
                                      IPVariable *ipvcuri,STensor43 &Tangent,const bool stiff,STensor43* elasticTangent, 
                                      const bool dTangent,
                                      STensor63* dCalgdeps) const{
                                      
  
  double **Calgo; 	VEVPMFHSPACE::mallocmatrix(&Calgo,6,6);
  double *dstrn; 	dstrn = (double*)malloc(6*sizeof(double));
  double dt;
  
  if(elasticTangent!=NULL) Msg::Error(" mlawVEVPMFH STensor43* elasticTangent not defined");
  const IPVEVPMFH *ipvprev = dynamic_cast<const IPVEVPMFH *> (ipvprevi);
  IPVEVPMFH *ipvcur = dynamic_cast<IPVEVPMFH *> (ipvcuri);
  const VEVPMFHSPACE::STATE* statetn= ipvprev->getSTATE();
  VEVPMFHSPACE::STATE* statetau= ipvcur->getSTATE();
  
  //time step
  dt = _timeStep;
  
  //true strain from the deformation gradient
  for(int i=0;i<3;i++){
    dstrn[i]=Fn(i,i)-F0(i,i);
  }
  
  dstrn[3]=0.5*sqrt(2)*(Fn(0,1)-F0(0,1)+Fn(1,0)-F0(1,0));
  dstrn[4]=0.5*sqrt(2)*(Fn(1,2)-F0(1,2)+Fn(2,1)-F0(2,1));
  dstrn[5]=0.5*sqrt(2)*(Fn(0,2)-F0(0,2)+Fn(2,0)-F0(2,0));
  
  //case t=0
  if (dt == 0){
    //strs(t=0)
    double *dstrs; 	dstrs = (double*)malloc(6*sizeof(double)); 
    VEVPMFHSPACE::compute_Hooke(matrprop.E0, matrprop.nu, Calgo);
    VEVPMFHSPACE::contraction42(Calgo, dstrn, dstrs);
    VEVPMFHSPACE::addtens2(1, statetn->strs, 1, dstrs, statetau->strs);
    
    VEVPMFHSPACE::addtens2(1, statetn->strn, 1, dstrn, statetau->strn);
    VEVPMFHSPACE::copystate(statetau, &(ipvcur->_state));
    
    //convert strs(t=0) to P(t=0)  
    for(int i=0;i<3;i++){
      P(i,i) = statetau->strs[i];
    }
    P(0,1) = statetau->strs[3]/ sqrt(2);
    P(1,0) = P(0,1);
    P(0,2) = statetau->strs[5]/ sqrt(2);
    P(2,0) = P(0,2);
    P(1,2) = statetau->strs[4]/ sqrt(2);
    P(2,1) = P(1,2);
    
    //convert calgo(t=0) to Tangent(t=0) 
   if(stiff){
      Tangent(0,0,0,0) = Calgo[0][0];
      Tangent(0,0,0,1) = Calgo[0][3]/sqrt(2);
      Tangent(0,0,0,2) = Calgo[0][5]/sqrt(2);
      Tangent(0,0,1,0) = Calgo[0][3]/sqrt(2);
      Tangent(0,0,1,1) = Calgo[0][1];
      Tangent(0,0,1,2) = Calgo[0][4]/sqrt(2);
      Tangent(0,0,2,0) = Calgo[0][5]/sqrt(2);
      Tangent(0,0,2,1) = Calgo[0][4]/sqrt(2);
      Tangent(0,0,2,2) = Calgo[0][2];

      Tangent(1,1,0,0) = Calgo[1][0];  
      Tangent(1,1,0,1) = Calgo[1][3]/sqrt(2);
      Tangent(1,1,0,2) = Calgo[1][5]/sqrt(2);
      Tangent(1,1,1,0) = Calgo[1][3]/sqrt(2);
      Tangent(1,1,1,1) = Calgo[1][1];
      Tangent(1,1,1,2) = Calgo[1][4]/sqrt(2);
      Tangent(1,1,2,0) = Calgo[1][5]/sqrt(2);
      Tangent(1,1,2,1) = Calgo[1][4]/sqrt(2);
      Tangent(1,1,2,2) = Calgo[1][2];

      Tangent(2,2,0,0) = Calgo[2][0];  
      Tangent(2,2,0,1) = Calgo[2][3]/sqrt(2);
      Tangent(2,2,0,2) = Calgo[2][5]/sqrt(2);
      Tangent(2,2,1,0) = Calgo[2][3]/sqrt(2);
      Tangent(2,2,1,1) = Calgo[2][1];
      Tangent(2,2,1,2) = Calgo[2][4]/sqrt(2);
      Tangent(2,2,2,0) = Calgo[2][5]/sqrt(2);
      Tangent(2,2,2,1) = Calgo[2][4]/sqrt(2);
      Tangent(2,2,2,2) = Calgo[2][2];

      Tangent(0,1,0,0) = Calgo[3][0]/sqrt(2);
      Tangent(0,1,0,1) = Calgo[3][3]/2;
      Tangent(0,1,0,2) = Calgo[3][5]/2;
      Tangent(0,1,1,0) = Calgo[3][3]/2;
      Tangent(0,1,1,1) = Calgo[3][1]/sqrt(2);
      Tangent(0,1,1,2) = Calgo[3][4]/2;
      Tangent(0,1,2,0) = Calgo[3][5]/2;
      Tangent(0,1,2,1) = Calgo[3][4]/2;
      Tangent(0,1,2,2) = Calgo[3][2]/sqrt(2);

      Tangent(1,0,0,0) = Calgo[3][0]/sqrt(2);
      Tangent(1,0,0,1) = Calgo[3][3]/2;
      Tangent(1,0,0,2) = Calgo[3][5]/2;
      Tangent(1,0,1,0) = Calgo[3][3]/2;
      Tangent(1,0,1,1) = Calgo[3][1]/sqrt(2);
      Tangent(1,0,1,2) = Calgo[3][4]/2;
      Tangent(1,0,2,0) = Calgo[3][5]/2;
      Tangent(1,0,2,1) = Calgo[3][4]/2;
      Tangent(1,0,2,2) = Calgo[3][2]/sqrt(2);

      Tangent(0,2,0,0) = Calgo[5][0]/sqrt(2);
      Tangent(0,2,0,1) = Calgo[5][3]/2;
      Tangent(0,2,0,2) = Calgo[5][5]/2;
      Tangent(0,2,1,0) = Calgo[5][3]/2;
      Tangent(0,2,1,1) = Calgo[5][1]/sqrt(2);
      Tangent(0,2,1,2) = Calgo[5][4]/2;
      Tangent(0,2,2,0) = Calgo[5][5]/2;
      Tangent(0,2,2,1) = Calgo[5][4]/2;
      Tangent(0,2,2,2) = Calgo[5][2]/sqrt(2);

      Tangent(2,0,0,0) = Calgo[5][0]/sqrt(2);
      Tangent(2,0,0,1) = Calgo[5][3]/2;
      Tangent(2,0,0,2) = Calgo[5][5]/2;
      Tangent(2,0,1,0) = Calgo[5][3]/2;
      Tangent(2,0,1,1) = Calgo[5][1]/sqrt(2);
      Tangent(2,0,1,2) = Calgo[5][4]/2;
      Tangent(2,0,2,0) = Calgo[5][5]/2;
      Tangent(2,0,2,1) = Calgo[5][4]/2;
      Tangent(2,0,2,2) = Calgo[5][2]/sqrt(2);

      Tangent(2,1,0,0) = Calgo[4][0]/sqrt(2);
      Tangent(2,1,0,1) = Calgo[4][3]/2;
      Tangent(2,1,0,2) = Calgo[4][5]/2;
      Tangent(2,1,1,0) = Calgo[4][3]/2;
      Tangent(2,1,1,1) = Calgo[4][1]/sqrt(2);
      Tangent(2,1,1,2) = Calgo[4][4]/2;
      Tangent(2,1,2,0) = Calgo[4][5]/2;
      Tangent(2,1,2,1) = Calgo[4][4]/2;
      Tangent(2,1,2,2) = Calgo[4][2]/sqrt(2);

      Tangent(1,2,0,0) = Calgo[4][0]/sqrt(2);
      Tangent(1,2,0,1) = Calgo[4][3]/2;
      Tangent(1,2,0,2) = Calgo[4][5]/2;
      Tangent(1,2,1,0) = Calgo[4][3]/2;
      Tangent(1,2,1,1) = Calgo[4][1]/sqrt(2);
      Tangent(1,2,1,2) = Calgo[4][4]/2;
      Tangent(1,2,2,0) = Calgo[4][5]/2;
      Tangent(1,2,2,1) = Calgo[4][4]/2;
      Tangent(1,2,2,2) = Calgo[4][2]/sqrt(2);
      
      }
   
    //freeing variables at t=0
    free(dstrs);
  }
  
  //case t!=0
  else{
    //update the stress, p, Calgo
    int error= VEVPMFHSPACE::ConstitutiveBoxViscoElasticViscoPlasticSmallStrains(&matrprop, statetn, dt, dstrn, statetau, Calgo);

    if(error){//converged 
      //update viscous stresses  
      double *dstrnp; dstrnp = (double*)malloc(6*sizeof(double));
      double *dstrn_ve; dstrn_ve = (double*)malloc(6*sizeof(double));
      double *dstrn_ve_vol; dstrn_ve_vol = (double*)malloc(6*sizeof(double));
      double *dstrn_ve_dev; dstrn_ve_dev = (double*)malloc(6*sizeof(double));
    
      VEVPMFHSPACE::addtens2(-1, statetn->pstrn, 1, statetau->pstrn, dstrnp); 
      VEVPMFHSPACE::addtens2(-1, dstrnp, 1, dstrn, dstrn_ve); 	    

      VEVPMFHSPACE::decomposition_dstrn_ve(dstrn_ve, dstrn_ve_vol, dstrn_ve_dev);
     
      VEVPMFHSPACE::compute_SigmaHj(&matrprop, statetn, dt, dstrn_ve_vol, statetau->Sigma_Hj);
      VEVPMFHSPACE::compute_Si(&matrprop, statetn, dt, dstrn_ve_dev, statetau->Si);
    
      VEVPMFHSPACE::addtens2(1, statetn->strn, 1, dstrn, statetau->strn);
      VEVPMFHSPACE::copystate(statetau, &(ipvcur->_state));
      
      //convert strs to P 
      for(int i=0;i<3;i++) {
        P(i,i) = statetau->strs[i];
      }
      P(0,1) = statetau->strs[3]/ sqrt(2);
      P(1,0) = P(0,1);
      P(0,2) = statetau->strs[5]/ sqrt(2);
      P(2,0) = P(0,2);
      P(1,2) = statetau->strs[4]/ sqrt(2);
      P(2,1) = P(1,2);
    
     //convert Calgo to Tangent 
     if(stiff){
        Tangent(0,0,0,0) = Calgo[0][0];
        Tangent(0,0,0,1) = Calgo[0][3]/sqrt(2);
        Tangent(0,0,0,2) = Calgo[0][5]/sqrt(2);
        Tangent(0,0,1,0) = Calgo[0][3]/sqrt(2);
        Tangent(0,0,1,1) = Calgo[0][1];
        Tangent(0,0,1,2) = Calgo[0][4]/sqrt(2);
        Tangent(0,0,2,0) = Calgo[0][5]/sqrt(2);
        Tangent(0,0,2,1) = Calgo[0][4]/sqrt(2);
        Tangent(0,0,2,2) = Calgo[0][2];

        Tangent(1,1,0,0) = Calgo[1][0];  
        Tangent(1,1,0,1) = Calgo[1][3]/sqrt(2);
        Tangent(1,1,0,2) = Calgo[1][5]/sqrt(2);
        Tangent(1,1,1,0) = Calgo[1][3]/sqrt(2);
        Tangent(1,1,1,1) = Calgo[1][1];
        Tangent(1,1,1,2) = Calgo[1][4]/sqrt(2);
        Tangent(1,1,2,0) = Calgo[1][5]/sqrt(2);
        Tangent(1,1,2,1) = Calgo[1][4]/sqrt(2);
        Tangent(1,1,2,2) = Calgo[1][2];

        Tangent(2,2,0,0) = Calgo[2][0];  
        Tangent(2,2,0,1) = Calgo[2][3]/sqrt(2);
        Tangent(2,2,0,2) = Calgo[2][5]/sqrt(2);
        Tangent(2,2,1,0) = Calgo[2][3]/sqrt(2);
        Tangent(2,2,1,1) = Calgo[2][1];
        Tangent(2,2,1,2) = Calgo[2][4]/sqrt(2);
        Tangent(2,2,2,0) = Calgo[2][5]/sqrt(2);
        Tangent(2,2,2,1) = Calgo[2][4]/sqrt(2);
        Tangent(2,2,2,2) = Calgo[2][2];

        Tangent(0,1,0,0) = Calgo[3][0]/sqrt(2);
        Tangent(0,1,0,1) = Calgo[3][3]/2;
        Tangent(0,1,0,2) = Calgo[3][5]/2;
        Tangent(0,1,1,0) = Calgo[3][3]/2;
        Tangent(0,1,1,1) = Calgo[3][1]/sqrt(2);
        Tangent(0,1,1,2) = Calgo[3][4]/2;
        Tangent(0,1,2,0) = Calgo[3][5]/2;
        Tangent(0,1,2,1) = Calgo[3][4]/2;
        Tangent(0,1,2,2) = Calgo[3][2]/sqrt(2);

        Tangent(1,0,0,0) = Calgo[3][0]/sqrt(2);
        Tangent(1,0,0,1) = Calgo[3][3]/2;
        Tangent(1,0,0,2) = Calgo[3][5]/2;
        Tangent(1,0,1,0) = Calgo[3][3]/2;
        Tangent(1,0,1,1) = Calgo[3][1]/sqrt(2);
        Tangent(1,0,1,2) = Calgo[3][4]/2;
        Tangent(1,0,2,0) = Calgo[3][5]/2;
        Tangent(1,0,2,1) = Calgo[3][4]/2;
        Tangent(1,0,2,2) = Calgo[3][2]/sqrt(2);

        Tangent(0,2,0,0) = Calgo[5][0]/sqrt(2);
        Tangent(0,2,0,1) = Calgo[5][3]/2;
        Tangent(0,2,0,2) = Calgo[5][5]/2;
        Tangent(0,2,1,0) = Calgo[5][3]/2;
        Tangent(0,2,1,1) = Calgo[5][1]/sqrt(2);
        Tangent(0,2,1,2) = Calgo[5][4]/2;
        Tangent(0,2,2,0) = Calgo[5][5]/2;
        Tangent(0,2,2,1) = Calgo[5][4]/2;
        Tangent(0,2,2,2) = Calgo[5][2]/sqrt(2);

        Tangent(2,0,0,0) = Calgo[5][0]/sqrt(2);
        Tangent(2,0,0,1) = Calgo[5][3]/2;
        Tangent(2,0,0,2) = Calgo[5][5]/2;
        Tangent(2,0,1,0) = Calgo[5][3]/2;
        Tangent(2,0,1,1) = Calgo[5][1]/sqrt(2);
        Tangent(2,0,1,2) = Calgo[5][4]/2;
        Tangent(2,0,2,0) = Calgo[5][5]/2;
        Tangent(2,0,2,1) = Calgo[5][4]/2;
        Tangent(2,0,2,2) = Calgo[5][2]/sqrt(2);

        Tangent(2,1,0,0) = Calgo[4][0]/sqrt(2);
        Tangent(2,1,0,1) = Calgo[4][3]/2;
        Tangent(2,1,0,2) = Calgo[4][5]/2;
        Tangent(2,1,1,0) = Calgo[4][3]/2;
        Tangent(2,1,1,1) = Calgo[4][1]/sqrt(2);
        Tangent(2,1,1,2) = Calgo[4][4]/2;
        Tangent(2,1,2,0) = Calgo[4][5]/2;
        Tangent(2,1,2,1) = Calgo[4][4]/2;
        Tangent(2,1,2,2) = Calgo[4][2]/sqrt(2);

        Tangent(1,2,0,0) = Calgo[4][0]/sqrt(2);
        Tangent(1,2,0,1) = Calgo[4][3]/2;
        Tangent(1,2,0,2) = Calgo[4][5]/2;
        Tangent(1,2,1,0) = Calgo[4][3]/2;
        Tangent(1,2,1,1) = Calgo[4][1]/sqrt(2);
        Tangent(1,2,1,2) = Calgo[4][4]/2;
        Tangent(1,2,2,0) = Calgo[4][5]/2;
        Tangent(1,2,2,1) = Calgo[4][4]/2;
        Tangent(1,2,2,2) = Calgo[4][2]/sqrt(2);
      }

      // compute VE & VP energies
      //double* ve_strn; ve_strn=(double*)malloc(6*sizeof(double));
      //VEVPMFHSPACE::addtens2(1,statetau->strn,-1,statetau->pstrn,ve_strn);
      //ipvcur->_elasticEne=0.5*VEVPMFHSPACE::contraction22(statetau->strs,ve_strn);
      //ipvcur->_plasticEne=0.5*VEVPMFHSPACE::contraction22(statetau->strs,statetau->pstrn);
      //free(ve_strn);

      free(dstrnp); 
      free(dstrn_ve); 
      free(dstrn_ve_vol); 
      free(dstrn_ve_dev);
        
    }
    
    else{//no convergence of the constitutive box
      // to exist NR and performed next step with a smaller incremenet
      P(0,0) = P(1,1) = P(2,2) = sqrt(-1.); 
    }

  }
  
  //freeing
  VEVPMFHSPACE::freematrix(Calgo,6);
  free(dstrn);
  
}


const double mlawVEVPMFH::bulkModulus() const{
  return matrprop.K0;
}

const double mlawVEVPMFH::YoungModulus() const{
  return matrprop.E0;
}

const double mlawVEVPMFH::shearModulus() const{
  return matrprop.G0 ;
}


const double mlawVEVPMFH::poissonRatio() const{
  return matrprop.nu;

}

