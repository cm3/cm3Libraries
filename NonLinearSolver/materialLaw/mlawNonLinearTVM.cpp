//
// C++ Interface: Material Law
//
// Description: Non-Linear Thermo-Visco-Mechanics (Thermo-ViscoElasto-ViscoPlasto Law) with Non-Local Damage Interface (soon.....)
//
// Author: <Ujwal Kishore J - FLE_Knight>, (C) 2023;
//
// Copyright: See COPYING file that comes with this distribution
//
//

#include "mlawHyperelastic.h"
#include "STensorOperations.h"
#include "nonLinearMechSolver.h"
#include "mlawNonLinearTVM.h"

// Constructor
mlawNonLinearTVM::mlawNonLinearTVM(const int num,const double E,const double nu, const double rho, const double tol,
                                    const double Tinitial, const double Alpha, const double KThCon, const double Cp,
                                    const bool matrixbyPerturbation, const double pert, const bool thermalEstimationPreviousConfig):
     mlawPowerYieldHyper(num, E, nu, rho, tol, matrixbyPerturbation, pert),
     _Tinitial(Tinitial),_scalarAlpha(Alpha),_scalarK(KThCon),_Cp(Cp),
     _thermalEstimationPreviousConfig(thermalEstimationPreviousConfig), _mullinsEffect(NULL), _elasticPotential(NULL), 
     _useExtraBranch(false), _useExtraBranch_TVE(false), _ExtraBranch_TVE_option(2), _V0(0.),_V1(0.),_V2(0.),_D0(0.),_D1(0.),_D2(0.),_Ci(0.),
	 _V3(0.),_V4(0.),_V5(0.),_D3(0.),_D4(0.),_D5(0.),_useRotationCorrection(false),_rotationCorrectionScheme(0){


  _G = _mu;

  _Tref = 298.;
  _C1 = 0.;
  _C2 = 0.;

  // _scalarAlpha = 0.; _scalarK = 0.; _Cp = 0.;
  _tensorAlpha = 0.;
  _tensorAlpha(0,0)= _scalarAlpha; _tensorAlpha(1,1)= _scalarAlpha; _tensorAlpha(2,2)= _scalarAlpha;

  // to be unifom in DG3D q = k' gradT with k'=-k instead of q=-kgradT
  _tensorK = 0.;
  _tensorK(0,0)= - _scalarK; _tensorK(1,1)= - _scalarK; _tensorK(2,2)= - _scalarK;

  _temFunc_K = new constantScalarFunction(1.);
  _temFunc_G = new constantScalarFunction(1.);
  _temFunc_Alpha = new constantScalarFunction(1.);
  _temFunc_KThCon = new constantScalarFunction(1.);
  _temFunc_Cp = new constantScalarFunction(1.);
  _temFunc_elasticCorrection_Bulk = new constantScalarFunction(1.);
  _temFunc_elasticCorrection_Shear = new constantScalarFunction(1.);

};
     
// Copy Constructor
mlawNonLinearTVM::mlawNonLinearTVM(const mlawNonLinearTVM& src): mlawPowerYieldHyper(src),
      _Tinitial(src._Tinitial),_scalarAlpha(src._scalarAlpha),_scalarK(src._scalarK),_Cp(src._Cp),
      _thermalEstimationPreviousConfig(src._thermalEstimationPreviousConfig){

  _G = src._G;
  _Tref = src._Tref;
  _C1 = src._C1;
  _C2 = src._C2;
  _tensorAlpha = src._tensorAlpha;
  _tensorK = src._tensorK;
  _useExtraBranch = src._useExtraBranch;
  _useExtraBranch_TVE = src._useExtraBranch_TVE;
  _ExtraBranch_TVE_option = src._ExtraBranch_TVE_option;
  _V0 = src._V0; _V1 = src._V1; _V2 = src._V2; _D0 = src._D0; _D1 = src._D1; _D2 = src._D2; _Ci = src._Ci;
  _V3 = src._V3; _V4 = src._V4; _V5 = src._V5; _D3 = src._D3; _D4 = src._D4; _D5 = src._D5;
  _useRotationCorrection = src._useRotationCorrection;
  _rotationCorrectionScheme = src._rotationCorrectionScheme;

  _temFunc_K = NULL;                                                                    // bulk modulus
  if (src._temFunc_K != NULL){ _temFunc_K = src._temFunc_K->clone();}

  _temFunc_G = NULL;                                                                    // shear modulus
  if (src._temFunc_G!=NULL){ _temFunc_G = src._temFunc_G->clone();}

  _temFunc_Alpha = NULL;                                                                // thermal dilatation coeffecient
  if (src._temFunc_Alpha!=NULL){ _temFunc_Alpha = src._temFunc_Alpha->clone();}

  _temFunc_KThCon = NULL;                                                               // thermal conductivity
  if (src._temFunc_KThCon!=NULL){ _temFunc_KThCon = src._temFunc_KThCon->clone();}

  _temFunc_Cp = NULL;                                                                   // specific heat
  if (src._temFunc_Cp!=NULL){ _temFunc_Cp = src._temFunc_Cp->clone();}

  _temFunc_elasticCorrection_Bulk = NULL;                                                    // elastic correction
  if (src._temFunc_elasticCorrection_Bulk!=NULL){ _temFunc_elasticCorrection_Bulk = src._temFunc_elasticCorrection_Bulk->clone();}
  
  _temFunc_elasticCorrection_Shear = NULL;                                                    // elastic correction
  if (src._temFunc_elasticCorrection_Shear!=NULL){ _temFunc_elasticCorrection_Shear = src._temFunc_elasticCorrection_Shear->clone();}
  
  _mullinsEffect= NULL;
  if (src._mullinsEffect) _mullinsEffect = src._mullinsEffect->clone();
  
  _elasticPotential = NULL;
  if (src._elasticPotential) _elasticPotential = src._elasticPotential->clone();
  
};  


mlawNonLinearTVM& mlawNonLinearTVM::operator=(const materialLaw &source){
    
  mlawPowerYieldHyper::operator=(source);
  const mlawNonLinearTVM* src =dynamic_cast<const mlawNonLinearTVM*>(&source);
  if(src != NULL){

      _Tinitial = src->_Tinitial; _scalarAlpha = src->_scalarAlpha; _scalarK = src->_scalarK; _Cp = src->_Cp;
      _thermalEstimationPreviousConfig = src->_thermalEstimationPreviousConfig;

    _G = src->_G;
    _Tref = src->_Tref;
    _C1 = src->_C1;
    _C2 = src->_C2;
    _tensorAlpha = src->_tensorAlpha;
    _tensorK = src->_tensorK;
    _useExtraBranch = src->_useExtraBranch;
    _useExtraBranch_TVE = src->_useExtraBranch_TVE;
    _ExtraBranch_TVE_option = src->_ExtraBranch_TVE_option;
    _V0 = src->_V0; _V1 = src->_V1; _V2 = src->_V2; _D0 = src->_D0; _D1 = src->_D1; _D2 = src->_D2; _Ci = src->_Ci;
    _V3 = src->_V3; _V4 = src->_V4; _V5 = src->_V5; _D3 = src->_D3; _D4 = src->_D4; _D5 = src->_D5;
    _useRotationCorrection = src->_useRotationCorrection;
    _rotationCorrectionScheme = src->_rotationCorrectionScheme;

    if(_temFunc_K != NULL) delete _temFunc_K;                                                   // bulk modulus
    if (src->_temFunc_K != NULL){ _temFunc_K = src->_temFunc_K->clone();}

    if(_temFunc_G != NULL) delete _temFunc_G;                                                   // shear modulus
    if (src->_temFunc_G!=NULL){ _temFunc_G = src->_temFunc_G->clone();}

    if(_temFunc_Alpha != NULL) delete _temFunc_Alpha;                                           // thermal dilatation coeffecient
    if (src->_temFunc_Alpha!=NULL){ _temFunc_Alpha = src->_temFunc_Alpha->clone();}

    if(_temFunc_KThCon != NULL) delete _temFunc_KThCon;                                         // thermal conductivity
    if (src->_temFunc_KThCon!=NULL){ _temFunc_KThCon = src->_temFunc_KThCon->clone();}

    if(_temFunc_Cp != NULL) delete _temFunc_Cp;                                                 // specific heat
    if (src->_temFunc_Cp!=NULL){ _temFunc_Cp = src->_temFunc_Cp->clone();}
    
    if(_temFunc_elasticCorrection_Bulk != NULL) delete _temFunc_elasticCorrection_Bulk;                   // elastic correction
    if (src->_temFunc_elasticCorrection_Bulk!=NULL){ _temFunc_elasticCorrection_Bulk = src->_temFunc_elasticCorrection_Bulk->clone();}
    
    if(_temFunc_elasticCorrection_Shear != NULL) delete _temFunc_elasticCorrection_Shear;                   // elastic correction
    if (src->_temFunc_elasticCorrection_Shear!=NULL){ _temFunc_elasticCorrection_Shear = src->_temFunc_elasticCorrection_Shear->clone();}

    if(_mullinsEffect != NULL) delete _mullinsEffect;
    if (src->_mullinsEffect!=NULL){ _mullinsEffect = src->_mullinsEffect->clone();}
    
    if(_elasticPotential != NULL) delete _elasticPotential;
    if (src->_elasticPotential!=NULL){ _elasticPotential = src->_elasticPotential->clone();}

  }
  return *this;
}

mlawNonLinearTVM::~mlawNonLinearTVM(){
    if(_temFunc_K != NULL) delete _temFunc_K; _temFunc_K = NULL;                    // bulk modulus;
    if(_temFunc_G != NULL) delete _temFunc_G; _temFunc_G = NULL;                    // shear modulus;
    if(_temFunc_Alpha != NULL) delete _temFunc_Alpha; _temFunc_Alpha = NULL;        // thermal dilatation coeff;
    if(_temFunc_KThCon != NULL) delete _temFunc_KThCon; _temFunc_KThCon = NULL;     // thermal conductivity;
    if(_temFunc_Cp != NULL) delete _temFunc_Cp; _temFunc_Cp = NULL;                 // specific heat;
    if(_temFunc_elasticCorrection_Bulk != NULL) delete _temFunc_elasticCorrection_Bulk; _temFunc_elasticCorrection_Bulk = NULL;                 // elastic correction
    if(_temFunc_elasticCorrection_Shear != NULL) delete _temFunc_elasticCorrection_Shear; _temFunc_elasticCorrection_Shear = NULL;                 // elastic correction
    if (_mullinsEffect) delete _mullinsEffect; _mullinsEffect = NULL;
    if (_elasticPotential) delete _elasticPotential; _elasticPotential = NULL;
};

void mlawNonLinearTVM::setViscoElasticNumberOfElement(const int N){
  _N = N;
  Msg::Info("Numer of Spring/Dashpot for viscoelastic model: %d",_N);
  _Ki.clear(); _ki.clear();
  _Gi.clear(); _gi.clear();
  _Ki.resize(_N,0.); _ki.resize(_N,0.);
  _Gi.resize(_N,0.); _gi.resize(_N,0.);
  _V0.clear(); _V1.clear(); _V2.clear(); _D0.clear(); _D1.clear(); _D2.clear(); _Ci.clear(); _V3.clear(); _V4.clear(); _V5.clear(); _D3.clear(); _D4.clear(); _D5.clear();
  _V0.resize(_N,0.); _V1.resize(_N,0.); _V2.resize(_N,0.); _D0.resize(_N,0.); _D1.resize(_N,0.); _D2.resize(_N,0.); _Ci.resize(_N,0.);
  _V3.resize(_N,0.); _V4.resize(_N,0.); _V5.resize(_N,0.); _D3.resize(_N,0.); _D4.resize(_N,0.); _D5.resize(_N,0.);
};

void mlawNonLinearTVM::setViscoElasticData(const int i, const double Ei, const double taui){
  if (i> _N or i<1)
    Msg::Error("This setting is invalid %d > %d",i,_N);
  else{
    double _nu = (3.*_K-2.*_mu)/2./(3.*_K+_mu);
    double KK = Ei/(3.*(1.-2.*_nu));
    double GG = Ei/(2.*(1.+_nu));

    _Ki[i-1] = KK;
    _ki[i-1] = Ei*taui/KK; // /(3.*(1.-2.*_nu));
    _Gi[i-1] = GG;
    _gi[i-1] = Ei*taui/(2*GG); // /(2.*(1.+_nu));

    Msg::Info("setting: element=%d Ki= %e ki= %e, Gi= %e, gi= %e",i-1,KK,taui,GG,taui);
  }
};

void mlawNonLinearTVM::setCorrectionsAllBranchesTVE(const int i, const double V0, const double V1, const double V2, const double D0, const double D1, const double D2){
  if (i> _N or i<1)
    Msg::Error("This setting is invalid %d > %d",i,_N);
  else{
	  _V0[i-1] = V0; _V1[i-1] = V1; _V2[i-1] = V2; _D0[i-1] = D0; _D1[i-1] = D1; _D2[i-1] = D2;

    // Msg::Info("setting: element=%d, V0 = %e, V1 = %e, V2 = %e, D0 = %e, D1 = %e, D2 = %e",i-1,V0,V1,V2,D0,D1,D2);
  }
};

void mlawNonLinearTVM::setAdditionalCorrectionsAllBranchesTVE(const int i, const double V3, const double D3){
  if (i> _N or i<1)
    Msg::Error("This setting is invalid %d > %d",i,_N);
  else{
    _V3[i-1] = V3; _D3[i-1] = D3;

    //  Msg::Info("setting: element=%d, V3 = %e, D3 = %e",i-1,V3,D3);
  }
};

void mlawNonLinearTVM::setAdditionalCorrectionsAllBranchesTVE(const int i, const double V3, const double V4, const double V5, const double D3, const double D4, const double D5){
  if (i> _N or i<1)
    Msg::Error("This setting is invalid %d > %d",i,_N);
  else{
    _V3[i-1] = V3; _V4[i-1] = V4; _V5[i-1] = V5; _D3[i-1] = D3; _D4[i-1] = D4; _D5[i-1] = D5;

    // Msg::Info("setting: element=%d, V3 = %e, V4 = %e, V5 = %e, D3 = %e, D4 = %e, D5 = %e",i-1,V3,V4,V5,D3,D4,D5);
  }
};

void mlawNonLinearTVM::setCompressionCorrectionsAllBranchesTVE(const int i, const double Ci){
  if (i> _N or i<1)
    Msg::Error("This setting is invalid %d > %d",i,_N);
  else{
    _Ci[i-1] = Ci;

    // Msg::Info("setting: element=%d, Ci = %e",i-1,Ci);
  }
};

void mlawNonLinearTVM::setViscoElasticData_Bulk(const int i, const double Ki, const double ki){
  if (i> _N or i<1)
    Msg::Error("This setting is invalid %d > %d",i,_N);
  else{
    _Ki[i-1] = Ki;
    _ki[i-1] = ki;

    Msg::Info("setting: element=%d Ki = %e ki = %e, ",i-1,Ki,ki);
  }
};

void mlawNonLinearTVM::setViscoElasticData_Shear(const int i, const double Gi, const double gi){
  if (i> _N or i<1)
    Msg::Error("This setting is invalid %d > %d",i,_N);
  else{
    _Gi[i-1] = Gi;
    _gi[i-1] = gi;

    Msg::Info("setting: element=%d Gi = %e gi = %e, ",i-1,Gi,gi);
  }
};

void mlawNonLinearTVM::setViscoElasticData(const std::string filename){
  FILE* file = fopen(filename.c_str(),"r");
  if (file == NULL) Msg::Error("file: %s does not exist",filename.c_str());
  _Ki.clear(); _ki.clear();
  _Gi.clear(); _gi.clear();
  _N = 0;
  Msg::Info("reading viscoelastic input data");

  while (!feof(file)){
    int ok = fscanf(file,"%d",&_N);
    Msg::Info("Numer of Maxwell elements: %d",_N);
    double KK, k, GG, g;

    for (int i=0; i<_N; i++){
      ok = fscanf(file,"%lf %lf %lf %lf",&KK,&k,&GG,&g);
      _Ki.push_back(KK);
      _ki.push_back(k);
      _Gi.push_back(GG);
      _gi.push_back(g);
      Msg::Info("Maxwell element %d: K[%d] = %e, k[%d] = %e, G[%d] = %e, g[%d] = %e",
                i,i,KK,i,k,i,GG,i,g);
    }
  }
  fclose(file);
};

double mlawNonLinearTVM::ShiftFactorWLF(const double T, double *DaDT, double *DDaDDT) const{
    
    double a_WLF = exp(- (_C1*(T - _Tref)) / (_C2 + T - _Tref) );
    
    if(DaDT!=NULL){
        *DaDT = 1/a_WLF * (_C1*_C2) / pow( (_C2 + T-_Tref) ,2);
    }
    if(DDaDDT!=NULL){
        *DDaDDT = 1/a_WLF * (_C1*_C2) * (_C1*_C2 - 2*(_C2+ T-_Tref)) / pow( (_C2 + T-_Tref) ,4);
    }
    return a_WLF;
}

void mlawNonLinearTVM::shiftedTime(const double t, const double T0, const double T1, double *t_shift, double *Ddt_shiftDT, double *DDdt_shiftDT,
                                    const int opt_time, const int opt_order) const{

// Shift t -> t is either delta_t or t (final time).  This function returns either t^* or delta_t^*.

// opt_time -> 0 or 1 (default), depending on full-step or mid-step integration approximation for shifted time step
// opt_order -> 1 or 2 (default) or 4, order of Guassian quadrature  (1 doesnt work, 4 probably works)

    double dt = this->getTimeStep();
    double t1 = this->getTime();
    double t0 = t1 - dt;

    // Linearly Interpolate Temperture -> This means dT/dt is constant between tn and tn+1
    double ts(0.), t_x1(0.), t_x2(0.);
    if (opt_time == 0){
        ts = 2;
        t_x1 = (t1+t0)/2 - dt/2 * (1/sqrt(3));
        t_x2 = (t1+t0)/2 + dt/2 * (1/sqrt(3));

    } else {
        ts = 4;
        t_x1 = (3*t1+t0)/4 - dt/4 * (1/sqrt(3));
        t_x2 = (3*t1+t0)/4 + dt/4 * (1/sqrt(3));
    }

    double T_x1 = linearInterpTemp(t_x1, t0, t1, T0, T1);
    double T_x2 = linearInterpTemp(t_x2, t0, t1, T0, T1);

    if (t_shift!=NULL){
        if (opt_order == 2){
            // 2 point Gauss rule
            double a_x1 = ShiftFactorWLF(T_x1);
            double a_x2 = ShiftFactorWLF(T_x2);
            *t_shift = t/ts * (1/a_x1 + 1/a_x2);
        } 
    }

    if (Ddt_shiftDT!=NULL){

        if (dt > 0.){
           if (opt_order == 2){
                // 2 point Gauss rule
                double DaDT_x1(0.), DaDT_x2(0.);
                double a_x1 = ShiftFactorWLF(T_x1,&DaDT_x1);
                double a_x2 = ShiftFactorWLF(T_x2,&DaDT_x2);
                *Ddt_shiftDT = t/ts * (DaDT_x1 * (t_x1-t0)/dt + DaDT_x2 * (t_x2-t0)/dt);
             } 
        } else { *Ddt_shiftDT = 0.;}
    }

    if (DDdt_shiftDT!=NULL){

        if (dt > 0.){
           if (opt_order == 2){
                // 2 point Gauss rule
                double DDaDT_x1(0.), DDaDT_x2(0.);
                double a_x1 = ShiftFactorWLF(T_x1,NULL,&DDaDT_x1);
                double a_x2 = ShiftFactorWLF(T_x2,NULL,&DDaDT_x2);
                *DDdt_shiftDT = t/ts * ( DDaDT_x1 * pow( ((t_x1-t0)/dt) ,2 ) + DDaDT_x2 * pow( ((t_x2-t0)/dt) ,2 ) );
            }
        } else { *DDdt_shiftDT = 0.;}
    }
}

double mlawNonLinearTVM::linearInterpTemp(const double t_interp, const double t0, const double t1, const double T0, const double T1) const{
    
    // This function linearly interpolates temperature for the given intermediate time -> t_interp is tn + fraction of step (i.e., midstep = tn + 0.5*dt)
        // Also, linear interp -> constant gradients
    double dt = t1 - t0;
    double T_interp;
    if (dt > 0.){
        T_interp = 1/dt*( (t1 - t_interp) )*T0 + 1/dt*( (t_interp - t0) )*T1;
    }
    else { T_interp = T0;}
    return T_interp;
}

double mlawNonLinearTVM::setTemp(const double T0, const double T1, const int para) const{
    
    double T_set = 0.;

    if (para == 1){ // @T1
        T_set = T1;

    } else if (para == 2){ // @Tmid
        double dt = this->getTimeStep();
        double t1 = this->getTime();
        double t0 = t1 - dt;
        double t_mid = t1 - dt/2;
        double T_mid = linearInterpTemp(t_mid, t0, t1, T0, T1);
        T_set = T_mid;

    } else if (para == 3){ // @Tmidshift
        double dt = this->getTimeStep();
        double t1 = this->getTime();
        double dt_shift, t1_shift;
        shiftedTime(dt,T0,T1,&dt_shift); shiftedTime(t1,T0,T1,&t1_shift);
        double t0_shift = t1_shift - dt_shift;
        double t_mid_shift = t1_shift - dt_shift/2;
        double aT_0 = ShiftFactorWLF(T0);
        double aT_1 = ShiftFactorWLF(T1);
        double T_mid_shift = linearInterpTemp(t_mid_shift, t0_shift, t1_shift, T0, T1);
        T_set = T_mid_shift;
    }

    return T_set;
}

double mlawNonLinearTVM::freeEnergyMechanical(const IPNonLinearTVM& q0, IPNonLinearTVM& q, const double T0, const double T, double* DpsiDT, STensor3* DpsiDE) const{

    // Update the Properties to the current temperature (see below which ones are being used)
    double KT, GT, AlphaT0, AlphaT, dKdT, dGdT, dAlphaTdT; 
    getK(&q,KT,T,&dKdT); getG(&q,GT,T,&dGdT); getAlpha(AlphaT,T,&dAlphaTdT); getAlpha(AlphaT0,T);

    double Psy_mech = 0.;

    // Get trEe and devEe
    double trEe0, trEe;
    static STensor3 devEe0, devEe, devDE;
    STensorOperation::decomposeDevTr(q._Ee,devEe,trEe);
    STensorOperation::decomposeDevTr(q0._Ee,devEe0,trEe0);
    devDE = devEe;
    devDE -= devEe0;

    // Get thermal strain
    double Eth = 3.*AlphaT*(T-_Tinitial);
    double eff_trDE = q._trDE - 3.*(AlphaT*(T-_Tinitial) - AlphaT0*(T0-_Tinitial));
    double Deff_trDE_DT = -3.*(dAlphaTdT*(T-_Tinitial) + AlphaT);
        
    // Get Equilibrium freeEnergy_mech
    Psy_mech = KT*0.5*(trEe-Eth)*(trEe-Eth) + GT*STensorOperation::doubledot(devEe,devEe);
    
    // Get ExtraBranch freeEnergy
    if (_useExtraBranch){
        Psy_mech += KT*q._intA + 2.*GT*q._intB;
    }

    // Add Branch freeEnergy_mech
    if ((_Ki.size() > 0) or (_Gi.size() > 0)){
        for (int i=0; i<_N; i++){
            
            if (!_useExtraBranch_TVE){
                Psy_mech += _Ki[i]*0.5*(q._B[i])*(q._B[i]) + _Gi[i]*STensorOperation::doubledot(q._A[i],q._A[i]);                
            }
            else{
                if (_ExtraBranch_TVE_option == 1){
                    
                    /* static STensor3 temp100, temp200, temp300;
                    temp100 = q0._A[i] + q._A[i];
                    temp200 = q._A[i];
                    temp200 -= q0._A[i];
                
                    for (int j=0; j<3; j++)
                        for (int k=0; k<3; k++)
                            temp300(j,k) = q0._A[i](j,k)*1./q0._Bd_TVE + q._A[i](j,k)*1./q._Bd_TVE;
                
                    // cumulative trapz rule
                    // q._psi_branch[i] = q0._psi_branch[i] + _Ki[i]*0.5*(trEe-trEe0)*(q0._B[i]+q._B[i]) + 0.5*2.*_Gi[i]*STensorOperation::doubledot(devDE,temp100);
                    q._psi_branch[i] = q0._psi_branch[i] + _Ki[i]*0.5*(q._B[i]-q0._B[i])*(q0._B[i]/q0._Av_TVE + q._B[i]/q._Av_TVE) 
                            - _Ki[i]*0.5*(q._Av_TVE-q0._Av_TVE)*(pow((q._B[i]/q._Av_TVE),2)+pow((q0._B[i]/q0._Av_TVE),2))
                            + 0.5*2.*_Gi[i]*STensorOperation::doubledot(temp200,temp300)
                            - 0.5*2.*_Gi[i]*(q._Bd_TVE-q0._Bd_TVE)*( STensorOperation::doubledot(q._A[i],q._A[i])/pow(1/q._Bd_TVE,2) 
                                                                + STensorOperation::doubledot(q0._A[i],q0._A[i])/pow(1/q0._Bd_TVE,2));
                
                    // Psy_mech += q._psi_branch[i];*/
                    Msg::Info("setting: _ExtraBranch_TVE_option=%d integrates the viscoelastic stress and currently has no definition for deformation energy. Don't use it with     mullin's effect",_ExtraBranch_TVE_option);
                }
                else if (_ExtraBranch_TVE_option == 2 || _ExtraBranch_TVE_option == 3 || _ExtraBranch_TVE_option == 4 || _ExtraBranch_TVE_option == 5){
                    Psy_mech += _Ki[i]*q._intAv_TVE_vector[i] + 2*_Gi[i]*q._intBd_TVE_vector[i];
                }
            }
        }
    }
    
    // If mullins, then calculate psi_VE derivatives
    if (_mullinsEffect != NULL && q._ipMullinsEffect != NULL){
        *DpsiDT = 0.;
        STensorOperation::zero(*DpsiDE);
        
        // infinite terms
        *DpsiDT += 0.5*dKdT*(trEe-Eth)*(trEe-Eth) + KT*(trEe-Eth)*(-3*dAlphaTdT*(T-_Tinitial)-3*AlphaT) + dGdT*STensorOperation::doubledot(devEe,devEe);
        for (int j=0; j<3; j++)
            for (int k=0; k<3; k++)
                (*DpsiDE)(j,k) += KT*(trEe-Eth)*_I(j,k) + 2*GT*devEe(j,k);
                
        // extraBranch terms
        if (_useExtraBranch){
            // q._elasticBulkPropertyScaleFactor = -1 + Av // Same applies to intA, etc
            
            if(_extraBranchNLType != TensionCompressionRegularisedType && _extraBranchNLType != hyper_exp_TCasymm_Type){
                (*DpsiDT) += dKdT*q._intA + KT* (q._elasticBulkPropertyScaleFactor) *(trEe-Eth)* (-3*dAlphaTdT*(T-_Tinitial)-3*AlphaT) + 
                            2.*dGdT*q._intB;
                (*DpsiDE) += q._corKirExtra;
            }
            else{
                // _intA, _intB have CTasymm
    
                // Regularising function
                // double m = _tensionCompressionRegularisation;
                // double expmtr = exp(-m*(trEe-Eth));
                // double sigmoid = 1/(1.+expmtr);
                //  double CTasymm = sigmoid + _compCorrection*(1.-sigmoid);
                // double dCTasymmDtrE = m*expmtr/pow((1.+expmtr),2.)*(1.-_compCorrection);
                // double psy_inf = (KT*0.5*(trEe-Eth)*(trEe-Eth) + GT*STensorOperation::doubledot(devEe,devEe) + KT*q._intA + 2.*GT*q._intB)/CTasymm;
                // retains only the A and B integrands assuming integrands1 = integrands2 -> cant divide by CTasymm if integrands1 != integrands2
                
                (*DpsiDT) += dKdT*q._intA + (KT*(q._elasticBulkPropertyScaleFactor) *(trEe-Eth) * (-3*dAlphaTdT*(T-_Tinitial)-3*AlphaT)) + 
                            2.*dGdT*q._intB;
                            
                (*DpsiDT) += (KT*q._DintA + 2*GT*q._DintB)*(-3*dAlphaTdT*(T-_Tinitial)-3*AlphaT); // psy_inf*dCTasymmDtrE*(-3*dAlphaTdT*(T-_Tinitial)-3*AlphaT);
                
                (*DpsiDE) += q._corKirExtra; // already has CTasymm
                for (int j=0; j<3; j++)
                    for (int k=0; k<3; k++)
                        (*DpsiDE)(j,k) += (KT*q._DintA + 2*GT*q._DintB)*_I(j,k); //psy_inf*dCTasymmDtrE*_I(j,k);
            }
        }
        
        // viscoelastic terms
    
        // Initialize if needed
        // Get ShiftFactor and shifted time increment
        double dt = this->getTimeStep();
        double dt_shift_rec(0.), dt_shift_mid(0.);
        double Ddt_shiftDT_rec(0.), Ddt_shiftDT_mid(0.), DDdt_shiftDT_rec(0.), DDdt_shiftDT_mid(0.);
        shiftedTime(dt, T0, T, &dt_shift_rec,&Ddt_shiftDT_rec,&DDdt_shiftDT_rec,0);
        shiftedTime(dt, T0, T, &dt_shift_mid,&Ddt_shiftDT_mid,&DDdt_shiftDT_mid);
        
        double Dexp_rec_k_DT(0.), Dexp_mid_k_DT(0.);  
        double Dexp_rec_g_DT(0.), Dexp_mid_g_DT(0.);  
        double dtk(0.), dtk2(0.), exp_rec_k(0.), exp_mid_k(0.);
        double dtg(0.), dtg2(0.), exp_rec_g(0.), exp_mid_g(0.);
        
        static STensor43 temp;
        if ((_Ki.size() > 0) or (_Gi.size() > 0)){
                
            for (int i=0; i<_N; i++){
                
                dtk = dt_shift_rec/(_ki[i]);
                dtk2 = dt_shift_mid/(_ki[i]);
                exp_rec_k = exp(-dtk);     // rec
                exp_mid_k = exp(-dtk2); // mid
                
                dtg = dt_shift_rec/(_gi[i]);
                dtg2 = dt_shift_mid/(_gi[i]);
                exp_rec_g = exp(-dtg);     // rec
                exp_mid_g = exp(-dtg2); // mid
            
                Dexp_rec_k_DT = -1/(_ki[i]) * Ddt_shiftDT_rec*exp_rec_k;
                Dexp_mid_k_DT = -1/(_ki[i]) * Ddt_shiftDT_mid*exp_mid_k;
                Dexp_rec_g_DT = -1/(_gi[i]) * Ddt_shiftDT_rec*exp_rec_g;
                Dexp_mid_g_DT = -1/(_gi[i]) * Ddt_shiftDT_mid*exp_mid_g;
                
              if (!_useExtraBranch_TVE){
                *DpsiDT += _Ki[i]*q._B[i]*q._DB_DT[i] + 2*_Gi[i]*STensorOperation::doubledot(q._A[i],q._DA_DT[i]);
                STensorOperation::multSTensor43(q._DA_DdevE[i],_Idev,temp);
                for (int j=0; j<3; j++)
                    for (int k=0; k<3; k++){
                        (*DpsiDE)(j,k) += _Ki[i]*q._B[i]*q._DB_DtrE[i]*_I(j,k);
                        for (int p=0; p<3; p++)
                            for (int r=0; r<3; r++){
                                (*DpsiDE)(j,k) += 2*_Gi[i]*q._A[i](p,r)*q._DA_DdevE[i](p,r,j,k);
                            }
                    }
              }
              else{
                if (_ExtraBranch_TVE_option == 1){
                    /* 
                    // derivatives of trapz
                    *DpsiDT += _Ki[i]*0.5*(trEe-trEe0)*q._DB_DT[i] + _Gi[i]*0.5*2.*STensorOperation::doubledot(devDE,q._DA_DT[i]);
                    STensorOperation::multSTensor43(q._DA_DdevE[i],_Idev,temp);
                    for (int j=0; j<3; j++)
                        for (int k=0; k<3; k++){
                            (*DpsiDE)(j,k) += _Ki[i]*0.5*(q0._B[i] + q._B[i] + (trEe-trEe0)*q._DB_DtrE[i])*_I(j,k);
                            for (int p=0; p<3; p++)
                                for (int r=0; r<3; r++){
                                    (*DpsiDE)(j,k) += 0.5*2*_Gi[i]*_Idev(p,r,j,k)*( q0._A[i](p,r) + q._A[i](p,r) ) ; 
                                    for (int m=0; m<3; m++)
                                        for (int o=0; o<3; o++){
                                            (*DpsiDE)(j,k) += 0.5*2*_Gi[i]*devDE(p,r)*q._DA_DdevE[i](p,r,m,o)*_Idev(m,o,j,k);
                                        }
                                }
                        }
                    }*/
                    Msg::Info("setting: _ExtraBranch_TVE_option=%d integrates the viscoelastic stress and currently has no definition for deformation energy. Don't use it with mullin's effect",_ExtraBranch_TVE_option);
                }
                else if (_ExtraBranch_TVE_option == 2 || _ExtraBranch_TVE_option == 3 || _ExtraBranch_TVE_option == 4 || _ExtraBranch_TVE_option == 5){
                    
                    double dTrEei_DT(0.); // branchElasticStrain trace
                    static STensor3 dDevEei_DT;
                    dTrEei_DT = q0._B[i] * Dexp_rec_k_DT + Dexp_mid_k_DT*eff_trDE + exp_mid_k*Deff_trDE_DT; // trDE; //NEW
                    for (int k=0; k<3; k++)
                        for (int l=0; l<3; l++)
                            dDevEei_DT(k,l) = q0._A[i](k,l) * Dexp_rec_g_DT + Dexp_mid_g_DT*q._devDE(k,l);
                    
                    if (_ExtraBranch_TVE_option == 2){
                        *DpsiDT += _Ki[i]*q._Av_TVE_vector[i]*q._B[i]*dTrEei_DT + 2.*_Gi[i]*q._Bd_TVE_vector[i]*STensorOperation::doubledot(q._A[i],dDevEei_DT);
                        for (int j=0; j<3; j++)
                            for (int k=0; k<3; k++){
                                (*DpsiDE)(j,k) += _Ki[i]*q._Av_TVE_vector[i]*q._B[i]*exp_mid_k*_I(j,k);
                                for (int p=0; p<3; p++)
                                    for (int r=0; r<3; r++)
                                        (*DpsiDE)(j,k) += 2*_Gi[i]*q._Bd_TVE_vector[i]*q._A[i](p,r)*exp_mid_g*_Idev(p,r,j,k); 
                        }
                    }
                    else { // _ExtraBranch_TVE_option == 3 || _ExtraBranch_TVE_option == 4 || _ExtraBranch_TVE_option == 5
                    
                        // NOTE : _Av_TVE_vector, _Bd_TVE_vector, _intAv_TVE_vector, _intBd_TVE_vector have CTasymm already
                        //          Therefore, to derive CTasymm, first divide to remove it and then derive.
                            
                        // Regularising function
                    	/*
                        double m = _tensionCompressionRegularisation;
                        double expmtr = exp(-m*q._B[i]);
                        double sigmoid = 1/(1.+expmtr);
                        double CTasymm = sigmoid + _compCorrection*(1.-sigmoid);
                        double dCTasymmDtrE = m*expmtr/pow((1.+expmtr),2.)*(1.-_compCorrection);*/
                        
                        (*DpsiDT) += (_Ki[i]*q._Av_TVE_vector[i]*q._B[i]*dTrEei_DT + 
                                        2.*_Gi[i]*q._Bd_TVE_vector[i]*STensorOperation::doubledot(q._A[i],dDevEei_DT)); // * CTasymm;
                                    
                        // (*DpsiDT) += (_Ki[i]*q._intAv_TVE_vector[i] + 2*_Gi[i]*q._intBd_TVE_vector[i])/CTasymm * dCTasymmDtrE * dTrEei_DT;
                        (*DpsiDT) += (_Ki[i]*q._DintAv_TVE_vector[i] + 2*_Gi[i]*q._DintBd_TVE_vector[i]) * dTrEei_DT;
                                    
                        for (int j=0; j<3; j++)
                            for (int k=0; k<3; k++){
                                (*DpsiDE)(j,k) += _Ki[i]*q._Av_TVE_vector[i]*q._B[i]*exp_mid_k*_I(j,k); // * CTasymm;
                                // (*DpsiDE)(j,k) += (_Ki[i]*q._intAv_TVE_vector[i] + 2.*_Gi[i]*q._intBd_TVE_vector[i])/CTasymm * dCTasymmDtrE * exp_mid_k*_I(j,k);
                                (*DpsiDE)(j,k) += (_Ki[i]*q._DintAv_TVE_vector[i] + 2.*_Gi[i]*q._DintBd_TVE_vector[i])* exp_mid_k*_I(j,k);
                                for (int p=0; p<3; p++)
                                    for (int r=0; r<3; r++){
                                        (*DpsiDE)(j,k) += 2.*_Gi[i]*q._Bd_TVE_vector[i]*q._A[i](p,r)*exp_mid_g*_Idev(p,r,j,k); // * CTasymm; 

                                // NEW EIGEN
                                		if ( _useRotationCorrection == true && _rotationCorrectionScheme == 1){
                                            for (int l=0; l<3; l++)
                                              for (int s=0; s<3; s++){
                                            	(*DpsiDE)(j,k) += 2.*_Gi[i]*q._Bd_TVE_vector[i]*q._A[i](p,r)*( exp_rec_g*( q._dRdEe(p,l,j,k)*q0._A[i](l,s)*q._R(s,r) + q._R(p,l)*q0._A[i](l,s)*q._dRtdEe(s,r,j,k) )
                                            							+ exp_mid_g*( - q._dRdEe(p,l,j,k)*devEe0(l,s)*q._R(s,r) - q._R(p,l)*devEe0(l,s)*q._dRtdEe(s,r,j,k) ) );
                                              }
                                		}
                                // NEW EIGEN
                                    }
                        } // for loop

                    } // if
                }
              }

            }
        }

        // *DpsiDE *= 1/_rho;
        // *DpsiDT *= 1/_rho;
    }

    // Psy_mech *= 1/_rho;
    return Psy_mech;
}

void mlawNonLinearTVM::calculateMullinsEffectScaler(const IPNonLinearTVM* q0, IPNonLinearTVM* q1, const double T,  const double* dPsiDT) const{
    
    // Initialise
    double eta = 1.;
    
    // update eta - Mullins Effect Scalar
    if (_mullinsEffect != NULL && q1->_ipMullinsEffect != NULL){
        if (dPsiDT!=NULL){
            _mullinsEffect->mullinsEffectScaling(q0->_elasticEnergy, q1->_elasticEnergy, *q0->_ipMullinsEffect, *q1->_ipMullinsEffect, T, *dPsiDT); 
        }
        else{
            _mullinsEffect->mullinsEffectScaling(q0->_elasticEnergy, q1->_elasticEnergy, *q0->_ipMullinsEffect, *q1->_ipMullinsEffect);
        }
    }
    
    // get eta - Mullins Effect Scalar (at unloading intitiation-> eta = 1.; unloading end-> eta = eta_min; reloading -> eta = eta_min;)
    if (q1->_ipMullinsEffect != NULL){
        eta = q1->_ipMullinsEffect->getEta(); 
        q1->_DmullinsDamage_Dpsi_cap = q1->_ipMullinsEffect->getDetaDpsi();
        if (dPsiDT!=NULL){
            q1->_DmullinsDamage_DT = q1->_ipMullinsEffect->getDetaDT();
        }
    }
    
    // update mullinsDamage scalar
    q1->_mullinsDamage = eta;
};

void mlawNonLinearTVM::corKirInfinity(const IPNonLinearTVM *q1, const STensor3& devEe,  const double& trEe, const double T1, STensor3& CorKirDevInf, double& CorKirTrInf) const{
    double KT, GT, AlphaT;
    getK(q1,KT,T1); getG(q1,GT,T1); getAlpha(AlphaT,T1);
    CorKirDevInf = 2*GT*devEe;
    CorKirTrInf = KT*(trEe - 3*AlphaT*(T1-_Tinitial));
}

void mlawNonLinearTVM::rotationTensor_N_to_Nplus1(const STensor3& Ee00, const STensor3& Fe0, const STensor3& Fe, const STensor3& Ce, const STensor3& Ee, const STensor3& Ee0,
													STensor3& R, STensor43& dRdEe, STensor43& dRtdEe) const{

	STensorOperation::zero(R);

	// Eigenvalue check
	double e1, e2, e3;
	static STensor3 E1, E2, E3;
	STensorOperation::getEigenDecomposition(Ee,e1,e2,e3,E1,E2,E3);

	if (_rotationCorrectionScheme == 0){
		STensorOperation::getBasisRotationTensor(Ee00,Ee0,R); // EIGEN 2
		STensorOperation::zero(dRdEe); // EIGEN 2
		STensorOperation::zero(dRtdEe); // EIGEN 2
	}
	else if (_rotationCorrectionScheme == 1){

		double pert = _perturbationfactor/1.e-5;
		static STensor3 Ee_plus, R_plus;
		static STensor43 dRdEe_plus, dRtdEe_plus;
		/*
		for(int i=0; i<3; i++)
		  for(int j=0; j<3; j++){
		    Ee_plus = Ee;
			Ee_plus(i,j) += 0.5*pert;
			Ee_plus(j,i) += 0.5*pert;
			STensorOperation::getBasisRotationTensor(Ee0,Ee_plus,R_plus);

			for(int k=0; k<3; k++)
			  for(int l=0; l<3; l++){
				dRdEe_plus(k,l,i,j) = (R_plus(k,l)-R(k,l))/pert;
				dRtdEe_plus(k,l,i,j) = (R_plus(l,k)-R(l,k))/pert;
	     	  }
		  }*/
		/*
		if (e1==e2 || e1==e3 || e2==e3){
			STensorOperation::zero(dRdEe_plus); // EIGEN 2
			STensorOperation::zero(dRtdEe_plus); // EIGEN 2
		}
		else{
			STensorOperation::getBasisRotationTensor(Ee0,Ee,R,dRdEe_plus,dRtdEe_plus);
		}*/

		STensorOperation::getBasisRotationTensor(Ee0,Ee,R,dRdEe_plus,dRtdEe_plus); // R rotates the bases of Ee0 to that of E
		dRdEe = dRdEe_plus;
		dRtdEe = dRtdEe_plus;
	}

    // static STensor3 Ue, Re, Ue0, Re0, Vp0, Vp, Rp0, Rp;
    // static STensor43 dEedFe, dEedCe, dRedEe;
	// mlawNonLinearTVM::getdRedEe(Fe,Ce,dEedFe,dEedCe,Ue,Re,dRedEe);
	// STensorOperation::VRDecomposition(Fe0,Vp0,Rp0); //EIGEN 2 - remove
	// STensorOperation::VRDecomposition(Fe,Vp,Rp); //EIGEN 2 - remove
    // STensorOperation::RUDecomposition(Fe0,Ue0,Re0);
}

void mlawNonLinearTVM::getdRedEe(const STensor3& Fe, const STensor3& Ce, const STensor43& dEedFe, const STensor43& dEedCe,
									STensor3& Ue, STensor3& Re, STensor43& dRedEe) const{
	STensorOperation::zero(Re);
	STensorOperation::zero(Ue);
	STensorOperation::RUDecomposition(Fe,Ue,Re);

	static STensor43 dFedEe, dCedEe;
	STensorOperation::inverseSTensor43(dEedFe,dFedEe);
	STensorOperation::inverseSTensor43(dEedCe,dCedEe);

	static STensor43 dFedFe;
	STensorOperation::zero(dFedFe);
    for(int i=0; i<3; i++)
      for(int j=0; j<3; j++)
        for(int k=0; k<3; k++)
          for(int l=0; l<3; l++)
        	  dFedFe(i,j,k,l) += _I(i,k)*_I(j,l);

	static STensor3 Ueinv;
	STensorOperation::inverseSTensor3(Ue,Ueinv);
	static STensor43 dUedCe, dUeinvdCe;
	mlawNonLinearTVM::getdUedCe(Ce,Ue,dUedCe);
    for (int i=0; i<3; i++)
        for (int s=0; s<3; s++)
            for (int k=0; k<3; k++)
                for (int l=0; l<3; l++){
                	dUeinvdCe(i,s,k,l) = 0.;
                    for (int m=0; m<3; m++)
                        for (int j=0; j<3; j++)
                        	dUeinvdCe(i,s,k,l) -= Ueinv(i,m)*dUedCe(m,j,k,l)*Ueinv(j,s);
      }

    static STensor43 dUeinvdEe;
    STensorOperation::multSTensor43(dUeinvdCe,dCedEe,dUeinvdEe);

    STensorOperation::zero(dRedEe);
    for(int i=0; i<3; i++)
      for(int j=0; j<3; j++)
        for(int k=0; k<3; k++)
          for(int l=0; l<3; l++)
        	for(int r=0; r<3; r++)
              dRedEe(i,j,k,l) += dFedEe(i,r,k,l)*Ueinv(r,j) + Fe(i,r)*dUeinvdEe(r,j,k,l);

}
void mlawNonLinearTVM::getdUedCe(const STensor3& Ce, const STensor3& Ue, STensor43& dUedCe) const{

	double c1,c2,c3,x1,x2,x3; // eigenvalues
	static STensor3 C1,C2,C3,E1,E2,E3; // bases
	static STensor3 dc1da,dc2da,dc3da,dx1da,dx2da,dx3da; // eigenvalue derivatives
	static STensor43 dC1da,dC2da,dC3da,dE1da,dE2da,dE3da; // bases derivatives

	STensorOperation::getEigenDecomposition(Ce,c1,c2,c3,C1,C2,C3,dc1da,dc2da,dc3da,dC1da,dC2da,dC3da);
	STensorOperation::getEigenDecomposition(Ue,x1,x2,x3,E1,E2,E3,dx1da,dx2da,dx3da,dE1da,dE2da,dE3da);

	static STensor43 dCedCe;
	STensorOperation::zero(dCedCe);
	STensorOperation::zero(dUedCe);
    for(int i=0; i<3; i++)
      for(int j=0; j<3; j++)
        for(int k=0; k<3; k++)
          for(int l=0; l<3; l++){
        	  dCedCe(i,j,k,l) += dc1da(i,j)*C1(k,l) + dc2da(i,j)*C2(k,l) + dc3da(i,j)*C3(k,l) + c1*dC1da(i,j,k,l) + c2*dC2da(i,j,k,l) + c3*dC3da(i,j,k,l); // Should be equal to _I4
        	  dUedCe(i,j,k,l) += dx1da(i,j)*E1(k,l) + dx2da(i,j)*E2(k,l) + dx3da(i,j)*E3(k,l) + x1*dE1da(i,j,k,l) + x2*dE2da(i,j,k,l) + x3*dE3da(i,j,k,l);
          }

    static STensor43 check;
    check = dCedCe;
    check -= _I4;
}

void mlawNonLinearTVM::getdEe0dEe(const STensor3& Ce, const STensor3& Ee, const STensor3& Ee0, STensor43& dEe0sdEe) const{

	// Ee0 becomes Ee0_star expressed in the same configuration as Ee

	double e01,e02,e03,c1,c2,c3; // eigenvalues
	static STensor3 E01,E02,E03,C1,C2,C3; // bases
	static STensor3 dc1da,dc2da,dc3da; // eigenvalue derivatives
	static STensor43 dC1da,dC2da,dC3da; // bases derivatives

	STensorOperation::getEigenDecomposition(Ee0,e01,e02,e03,E01,E02,E03);
	STensorOperation::getEigenDecomposition(Ce,c1,c2,c3,C1,C2,C3,dc1da,dc2da,dc3da,dC1da,dC2da,dC3da);

	// #####
	// Align the eigenVals of Ee0 with Ee
	static fullMatrix<double> m1(3, 3), m2(3, 3);
	static fullVector<double> eigenValReal1(3), eigenValReal2(3), AlignedEigenValReal1(3);
	static fullVector<double> eigenValImag1(3), eigenValImag2(3);
	static fullMatrix<double> leftEigenVect1(3,3), leftEigenVect2(3,3);
	static fullMatrix<double> rightEigenVect1(3,3), rightEigenVect2(3,3), AlignedRightEigenVect1(3,3);
	m1.setAll(0.); m2.setAll(0.);
	eigenValReal1.setAll(0.); eigenValReal2.setAll(0.); AlignedEigenValReal1.setAll(0.);
	eigenValImag1.setAll(0.); eigenValImag2.setAll(0.);
	leftEigenVect1.setAll(0.); leftEigenVect2.setAll(0.);
	rightEigenVect1.setAll(0.); rightEigenVect2.setAll(0.); AlignedRightEigenVect1.setAll(0.);

	// Get eigen values and vectors
	Ee0.getMat(m1); Ee.getMat(m2);
	m1.eig(eigenValReal1,eigenValImag1,leftEigenVect1,rightEigenVect1,false);
	m2.eig(eigenValReal2,eigenValImag2,leftEigenVect2,rightEigenVect2,false);

	// for printing - Debug
	e01 = eigenValReal1(0);
	e02 = eigenValReal1(1);
	e03 = eigenValReal1(2);

	// Make sure rightEigenVect1 is aligned with rightEigenVect2
	int align;
	STensorOperation::alignEigenDecomposition_NormBased(Ee0,Ee,eigenValReal1,eigenValReal2,rightEigenVect1,rightEigenVect2, e01, e02, e03, AlignedRightEigenVect1,align);
	// eigenValReal1 = AlignedEigenValReal1;
	rightEigenVect1 = AlignedRightEigenVect1;
	// e01 = AlignedEigenValReal1(0);
	// e02 = AlignedEigenValReal1(1);
	// e03 = AlignedEigenValReal1(2);
	// #####

	if(c1==0.) {STensorOperation::zero(dC1da);}
	if(c2==0.) {STensorOperation::zero(dC2da);}
	if(c3==0.) {STensorOperation::zero(dC3da);}

	static STensor43 dEe0sdCe, dCedCe;
	STensorOperation::zero(dEe0sdCe);
	STensorOperation::zero(dCedCe);
	if(align = 1){
    for(int i=0; i<3; i++)
      for(int j=0; j<3; j++)
        for(int k=0; k<3; k++)
          for(int l=0; l<3; l++){
        	  dEe0sdCe(i,j,k,l) += e01*dC1da(i,j,k,l) + e02*dC2da(i,j,k,l) + e03*dC3da(i,j,k,l);
        	  dCedCe(i,j,k,l) += dc1da(i,j)*C1(k,l) + dc2da(i,j)*C2(k,l) + dc3da(i,j)*C3(k,l) + c1*dC1da(i,j,k,l) + c2*dC2da(i,j,k,l) + c3*dC3da(i,j,k,l);  // Should be equal to _I4
          }
	}
    static STensor43 check;
    check = dCedCe;
    check -= _I4;

    // convert to dEe0sdEe
    static STensor3 Ee2, exp_Ee2;
    static STensor43 dCedEe;
	STensorOperation::zero(dCedEe);
    Ee2 = Ee;
    Ee2 *= 2;
    STensorOperation::expSTensor3(Ee2,_order,exp_Ee2,&dCedEe);
    STensorOperation::multSTensor43(dEe0sdCe,dCedEe,dEe0sdEe);
}

void mlawNonLinearTVM::evaluateElasticCorrection(const double trE, const STensor3 &devE, const double& T, 
                                                 double &A_v, double &dA_vdE, double &intA, double &dAdT,
                                                 double &B_d, STensor3 &dB_vddev, double &intB, double &dBdT,
                                                 double *ddAdTT, double *ddBdTT, double *ddA_vdTdE, STensor3 *ddB_vdTdevE,
                                                 double *dB_dTrEe, double *dB_dTdTrEe, double* psiInfCorrector, double* DintA, double* DintB) const{
    
    if (_extraBranchNLType != TensionCompressionRegularisedType && _extraBranchNLType != hyper_exp_TCasymm_Type){
        mlawHyperViscoElastic::evaluatePhiPCorrection(trE,devE,A_v,dA_vdE,intA,B_d,dB_vddev,intB,NULL,psiInfCorrector,DintA,DintB);
    }
    else{
        mlawHyperViscoElastic::evaluatePhiPCorrection(trE,devE,A_v,dA_vdE,intA,B_d,dB_vddev,intB,dB_dTrEe,psiInfCorrector,DintA,DintB);
        // *dB_dTrEe *= _temFunc_elasticCorrection_Shear->getVal(T);
        // if(dB_dTdTrEe!=NULL){
            // *dB_dTdTrEe = (*dB_dTrEe)*_temFunc_elasticCorrection_Bulk->getDiff(T);
        // }
    }
    
    // TBD - tempfuncs for extraBranch
    /*
    A_v *= _temFunc_elasticCorrection_Bulk->getVal(T);
    dA_vdE *= _temFunc_elasticCorrection_Bulk->getVal(T);
    intA *= _temFunc_elasticCorrection_Bulk->getVal(T);
    if (psiInfCorrector != NULL){
        (*psiInfCorrector) *= _temFunc_elasticCorrection_Bulk->getVal(T);
    }
    
    dAdT = A_v*_temFunc_elasticCorrection_Bulk->getDiff(T);
    if(ddAdTT!=NULL){
        *ddAdTT = A_v*_temFunc_elasticCorrection_Bulk->getDoubleDiff(T);
    }
    if(ddA_vdTdE!=NULL){
        *ddA_vdTdE = dA_vdE*_temFunc_elasticCorrection_Bulk->getDiff(T)/_temFunc_elasticCorrection_Bulk->getVal(T);
    }
    
    B_d *= _temFunc_elasticCorrection_Shear->getVal(T);
    dB_vddev *= _temFunc_elasticCorrection_Shear->getVal(T);
    intB *= _temFunc_elasticCorrection_Shear->getVal(T);
    dBdT = B_d*_temFunc_elasticCorrection_Shear->getDiff(T);
    if(ddBdTT!=NULL){
        *ddBdTT = B_d*_temFunc_elasticCorrection_Shear->getDoubleDiff(T);
    }
    if(ddB_vdTdevE!=NULL){
        *ddB_vdTdevE = dB_vddev*_temFunc_elasticCorrection_Shear->getDiff(T);
        *ddB_vdTdevE *= 1/_temFunc_elasticCorrection_Shear->getVal(T);
    }
    */
}

void mlawNonLinearTVM::extraBranchLaw(const STensor3& Ee, const double& T, const IPNonLinearTVM *q0, IPNonLinearTVM *q1, 
                        STensor3& sig, const bool stiff, STensor43* DsigDEe, STensor3* DsigDT, 
                        double* DsigV_dTrEe, STensor43* DsigD_dDevEe,
                        double* DsigV_dT, STensor3* DsigD_dT,
                        double* DDsigV_dTT, STensor3* DDsigD_dTT,
                        double* DDsigV_dTdTrEe, STensor43* DDsigD_dTdDevEe,
                        STensor3* DsigD_dTrEe, STensor3* DsigD_dTdTrEe) const
{
    // DsigDEe = DcorKirDE
    
    // NOTE: Ee = eff_trEe = trEe - 3*AlphaT*(T1-_Tinitial)
    double AlphaT, DAlphaDT, DDAlphaDT;
    getAlpha(AlphaT,T,&DAlphaDT,&DDAlphaDT);
    double DtrEeDT = -3*(AlphaT + DAlphaDT*(T-_Tinitial));
    double DDtrEeDTT = -3*(2*DAlphaDT + DDAlphaDT*(T-_Tinitial));
    
  if (_extraBranchType == Bilogarithmic)
  {
    static STensor3 devEe;
    double trEe;
    STensorOperation::decomposeDevTr(Ee,devEe,trEe);
    
    double A(0.), B(0.);
    double dA_dTrEe(0.), dA_dTdTrEe(0.), dB_dTrEe(0.), dB_dTdTrEe(0.), psiInfCorrector(0.);
    STensor3 dB_dDevEe, ddB_dTdDevEe;
    double intA(0.), intB(0.), dA_dT(0.), dB_dT(0.), ddA_dTT(0.), ddB_dTT(0.), DintA(0.), DintB(0.);
    
    if (_extraBranchNLType != TensionCompressionRegularisedType && _extraBranchNLType != hyper_exp_TCasymm_Type){
        evaluateElasticCorrection(trEe, devEe, T, A, dA_dTrEe, intA, dA_dT, B, dB_dDevEe, intB, dB_dT, &ddA_dTT, &ddB_dTT, &dA_dTdTrEe, &ddB_dTdDevEe, 
                                        NULL, NULL, &psiInfCorrector, &DintA, &DintB);
    }
    else {
        evaluateElasticCorrection(trEe, devEe, T, A, dA_dTrEe, intA, dA_dT, B, dB_dDevEe, intB, dB_dT, &ddA_dTT, &ddB_dTT, &dA_dTdTrEe, &ddB_dTdDevEe, 
                                        &dB_dTrEe, &dB_dTdTrEe, &psiInfCorrector, &DintA, &DintB);
    }
    
    if (A <= -1. + 1.e-5){ // saturated
        double trEe_0 = q0->_Ee.trace();
        intA = q0->_intA;
        intA += A*0.5 *(trEe*trEe-trEe_0*trEe_0);
    }
    q1->_intA = intA;
    q1->_DintA = DintA;
    
    if (B <= -1. + 1.e-5){ // saturated
        static STensor3 devEe_0;
        double trEe_0;
        STensorOperation::decomposeDevTr(q0->_Ee,devEe_0,trEe_0);
        intA = q0->_intB;
        intA += B*0.5 *(devEe.dotprod()-devEe_0.dotprod());
    }
    q1->_intB = intB;
    q1->_DintB = DintB;
    
    q1->_psiInfCorrector = psiInfCorrector; // Additional term to correct thermal expansion term in free energy
    
    // double check_1 = (1+A)*_K;
    // double check_2 = (1+B)*_G;
    // Msg::Error(" Inside extraBranchLaw, Kinf = %e, Ginf = %e !!", _K, _G);
    // Msg::Error(" Inside extraBranchLaw, (1+A)*Kinf = %e, (1+B)*Ginf = %e !!", check_1, check_2);
    
    q1->_elasticBulkPropertyScaleFactor = A; 
    q1->_elasticShearPropertyScaleFactor = B; 
    
    double Gextra, dGex_dT, ddGex_dTT; 
    getG(q1,Gextra,T, &dGex_dT, &ddGex_dTT); // getUpdatedShearModulus(q1); // We dont update here anymore
    double Kextra, dKex_dT, ddKex_dTT;  
    getK(q1,Kextra,T, &dKex_dT, &ddKex_dTT); // getUpdatedBulkModulus(q1);  // We dont update here anymore

    sig = devEe;
    sig *= (2.*Gextra*B);  // deviatoric part
    double p = trEe*Kextra*A; // pressure
    sig(0,0) += p;
    sig(1,1) += p;
    sig(2,2) += p;
    
    if (stiff)
    {
        // DsigDE
        if(DsigDEe!=NULL){
            STensorOperation::zero(*DsigDEe);
            *DsigDEe = _Idev;
            *DsigDEe *= (2.*Gextra*B);
            STensorOperation::prodAdd(devEe, dB_dDevEe, 2.*Gextra,  *DsigDEe);            
            STensorOperation::prodAdd(_I,_I,Kextra*(A+trEe*dA_dTrEe),*DsigDEe);
            
            if (_extraBranchNLType == TensionCompressionRegularisedType || _extraBranchNLType == hyper_exp_TCasymm_Type){
                STensorOperation::prodAdd(devEe, _I, 2.*Gextra*dB_dTrEe, *DsigDEe);
            }
        }
        
        // DsigDT
        if(DsigDT!=NULL){
            STensorOperation::zero(*DsigDT);
        }
        
        // These can be used for plasticity
        if(DsigV_dTrEe!=NULL){
            *DsigV_dTrEe = Kextra*(A + trEe*dA_dTrEe);
        }
        if(DsigD_dDevEe!=NULL){
            STensorOperation::zero(*DsigD_dDevEe);
            *DsigD_dDevEe = _I4;
            *DsigD_dDevEe *= (2.*Gextra*B);
            STensorOperation::prodAdd(devEe, dB_dDevEe, 2.*Gextra, *DsigD_dDevEe);
        }
        
        if(DsigV_dT!=NULL){
            dA_dT = dA_dTrEe*DtrEeDT;
            *DsigV_dT = dKex_dT*A*trEe + Kextra*(dA_dT*trEe + A*DtrEeDT);
        }
        if(DsigD_dT!=NULL){
            STensorOperation::zero(*DsigD_dT);
            double temp = 2*dGex_dT*B;
            if (_extraBranchNLType == TensionCompressionRegularisedType || _extraBranchNLType == hyper_exp_TCasymm_Type){
                temp += 2*Gextra*dB_dTrEe*DtrEeDT;
            }
            *DsigD_dT = devEe;
            *DsigD_dT *= temp;
        }
        
        if(DDsigV_dTT!=NULL){
            dA_dT = dA_dTrEe*DtrEeDT;
            ddA_dTT = dA_dTdTrEe*DtrEeDT + dA_dTrEe*DDtrEeDTT;
            *DDsigV_dTT = ddKex_dTT*A*trEe + 2*dKex_dT*(dA_dT*trEe + A*DtrEeDT) + Kextra*(ddA_dTT*trEe + dA_dT*DtrEeDT + A*DDtrEeDTT); // ddA_dTdTrEe = 0. 
        }
        if(DDsigD_dTT!=NULL){
            STensorOperation::zero(*DDsigD_dTT);
            double temp = 2*ddGex_dTT*B;
            if (_extraBranchNLType == TensionCompressionRegularisedType || _extraBranchNLType == hyper_exp_TCasymm_Type){
                temp += 2*dGex_dT*(2*dB_dTrEe*DtrEeDT) + 2*Gextra*dB_dTrEe*DDtrEeDTT; // Assuming ddB_ddTrEe, ddB_dTdTrEe = 0.
            }
            *DDsigD_dTT = devEe;
            *DDsigD_dTT *= temp;
        }
        if(DDsigV_dTdTrEe!=NULL){
            dA_dT = dA_dTrEe*DtrEeDT;
            *DDsigV_dTdTrEe = dKex_dT*(A + trEe*dA_dTrEe) + Kextra*(dA_dT + dA_dTrEe*DtrEeDT); // ddA_dTrEedTrEe = 0.
        }
        if(DDsigD_dTdDevEe!=NULL){
            STensorOperation::zero(*DDsigD_dTdDevEe);
            *DDsigD_dTdDevEe = _I4;
            double temp = (2.*dGex_dT*B);
            if (_extraBranchNLType == TensionCompressionRegularisedType || _extraBranchNLType == hyper_exp_TCasymm_Type){
                temp += 2*Gextra*dB_dTrEe*DtrEeDT;
            }
            *DDsigD_dTdDevEe *= temp;
            STensorOperation::prodAdd(devEe, dB_dDevEe, 2.*dGex_dT, *DDsigD_dTdDevEe);
        }
        if(DsigD_dTrEe!=NULL){
            STensorOperation::zero(*DsigD_dTrEe);
            *DsigD_dTrEe = devEe;
            *DsigD_dTrEe *= (2.*Gextra*dB_dTrEe);
        }
        if(DsigD_dTdTrEe!=NULL){
            STensorOperation::zero(*DsigD_dTdTrEe);
            *DsigD_dTdTrEe = devEe;
            *DsigD_dTdTrEe *= (2.*dGex_dT*dB_dTrEe); // Assuming ddB_ddTrEe = 0.
        } 
    };
  }
  else
  {
    Msg::Error("extra branch type %d is not defined",_extraBranchType);
  }
};

void mlawNonLinearTVM::extraBranch_nonLinearTVE(const int i, const STensor3& Ee,  const double& T,
                        double& Av, double& dA_dTrEe, double& intA, double& intA1, double& intA2, double& dA_dT, double& ddA_dTT, double& ddA_dTdTrEe,
                        double& Bd, STensor3& dB_dDevEe, double &intB, STensor3 &intB1, STensor3 &intB2, double &dB_dT, double &ddB_dTT, STensor3& ddB_dTdDevEe,
						double& DintA, double& DintB, double* dB_dTrEe) const{

    // Similar code to previous implementations of extraBranch except we dont do -1 + Av, have to make this a unique implementation.
    
    // Ki(trEe) = Av*Ki
    // Gi(devEe) = Bd*Gi
    // dKi/dTrEe = dA_dTrEe*Ki
    // dGi/dDevEe = dB_dDevEe*Gi
    // int(Ki*Av*trEe dTrEe) = Ki*intA         // For energy
    // int(Gi*Bd*devEe dDevEe) = Gi*intB       // For energy 
    // int(Ki*dA_dTrEe dTrEe) = Ki*intA1       // For 1st convolution integral 
    // int(Gi*dB_dDevEe dDevEe) = Gi*intB1     // For 1st convolution integral 
    // int(1/Av*dA_dTrEe dTrEe) = Ki*intA2       // For 2nd convolution integral and stiffness  --> actually it is, int(1/Ki * dKi/dTrEe dTrEe)
    // int(1/Bd*dB_dDevEe dDevEe) = Gi*intB2     // For 2nd convolution integral and stiffness  --> actually it is, int(1/Gi * dGi/dDevEe dDevEe)
    // ddA_dTdTrEe - mechSrc??
    // ddB_dTdDevEe - mechSrc??
	// DintA, DintB - the additional derivatives of the sigmoid in the free energy approximation associated with tension-compression assymmetry
    
    static STensor3 dev;
    double tr;
    STensorOperation::decomposeDevTr(Ee,dev,tr);
    
     
    int method = 3; // consistent with the member function in mlawHyperelastic evaluatePhiPCorrection(..) - combine some day
    if(_ExtraBranch_TVE_option == 1){
        
        // Standard form => A_v = a/(sqrt(b*tr*tr+c))
        
        double x = getXiVolumeCorrection()*tr*tr + getZetaVolumeCorrection();
        Av = getVolumeCorrection()* 1/sqrt(x) ;
        if (tr < 0.){
            Av = getVolumeCorrection()* 1/sqrt(x) * (1+_compCorrection/getVolumeCorrection());
        }
        
        dA_dTrEe = - getVolumeCorrection()*getXiVolumeCorrection()*tr/pow(x,1.5);
        intA = getVolumeCorrection()*sqrt(x)/getXiVolumeCorrection(); // integral of A_v * trEe
        intA -= (getVolumeCorrection()*sqrt(getZetaVolumeCorrection())/getXiVolumeCorrection()); // value at trEe = 0.
            
        intA1 = getVolumeCorrection()/sqrt(getXiVolumeCorrection()) * asinh(sqrt(getXiVolumeCorrection()/getZetaVolumeCorrection())*tr); // integral of A_v
        // intA1 = getVolumeCorrection()/sqrt(getXiVolumeCorrection()) * atanh(sqrt(getXiVolumeCorrection())*tr* 1/sqrt(x)); // integral of A_v
        
        intA2 = -1./2.*log(getXiVolumeCorrection()*tr*tr + getZetaVolumeCorrection()); // integral of 1/A_v*dA_dTrEe
            
            if (tr < 0.){
                dA_dTrEe = - getVolumeCorrection()*getXiVolumeCorrection()*tr/pow(x,1.5) * (1+_compCorrection/getVolumeCorrection());
                intA = getVolumeCorrection()*sqrt(x)/getXiVolumeCorrection() * (1+_compCorrection/getVolumeCorrection()); 
                intA -= getVolumeCorrection()*sqrt(getZetaVolumeCorrection())/getXiVolumeCorrection() * (1+_compCorrection/getVolumeCorrection()); 
                
                intA1 = getVolumeCorrection()/sqrt(getXiVolumeCorrection()) * asinh(sqrt(getXiVolumeCorrection()/getZetaVolumeCorrection())*tr)
                            * (1+_compCorrection/getVolumeCorrection());
                intA2 = -1./2.*log(getXiVolumeCorrection()*tr*tr + getZetaVolumeCorrection())
                            * (1+_compCorrection/getVolumeCorrection());
            }
        
        double y = getThetaDevCorrection()*dev.dotprod() + getPiDevCorrection();
        Bd = getDevCorrection()*1/sqrt(y);
        if(Bd > 0.){ 
            STensorOperation::zero(dB_dDevEe);
            dB_dDevEe = dev;
            dB_dDevEe *= (-getDevCorrection()*getThetaDevCorrection()/pow(y,1.5));
            intB = getDevCorrection()*sqrt(y)/getThetaDevCorrection();  // integral of B_d * devEe
            intB -= (getDevCorrection()*sqrt(getPiDevCorrection())/getThetaDevCorrection());  // value at devEe = 0.
            
            intB1 = 0.; // integral of B_d -> cannot be done
            intB2 = 0.; // integral of 1/B_d*dB_dDevEe -> cannot be done
        }
        else{
            Bd = 0. + 1.e-5; 
            STensorOperation::zero(dB_dDevEe);
            intB = 0.;
            intB1 = 0.;
            intB2 = 0.;
        }        
    }
    else if (_ExtraBranch_TVE_option == 2){
        
        // Standard form => A_v = a/(sqrt(b*tr*tr+c))
        
        // Av
        double x = getXiVolumeCorrection()*tr*tr + getZetaVolumeCorrection();
        Av = getVolumeCorrection()* 1/sqrt(x) ;
        if (tr < 0.){
            Av = getVolumeCorrection()* 1/sqrt(x); // * (1. + _compCorrection/getVolumeCorrection());
            // Av += (_compCorrection*tanh(_compCorrection_2*tr*tr+_compCorrection_3)) ;
        }
        
        dA_dTrEe = - getVolumeCorrection()*getXiVolumeCorrection()*tr/pow(x,1.5);
        if(getXiVolumeCorrection()>0.){
            intA = getVolumeCorrection()/getXiVolumeCorrection()*sqrt(x); // integral of A_v * trEe
            intA -= ( getVolumeCorrection()/getXiVolumeCorrection()*sqrt(getZetaVolumeCorrection()) ); // value at trEe = 0.
        }
        else{
            intA = 1.;
        }
        if (tr < 0.){
            dA_dTrEe = - getVolumeCorrection()*getXiVolumeCorrection()*tr/pow(x,1.5); //  * (1. + _compCorrection/getVolumeCorrection());
            if(getXiVolumeCorrection()>0.){
                intA = getVolumeCorrection()/getXiVolumeCorrection()*sqrt(x); //  * (1. + _compCorrection/getVolumeCorrection()); 
                intA -= ( getVolumeCorrection()/getXiVolumeCorrection()*sqrt(getZetaVolumeCorrection()) ); // * (1. + _compCorrection/getVolumeCorrection()); 
            }
            else{
                intA = 1.;
            }
        }
        
        // Bd    
        double y = getThetaDevCorrection()*dev.dotprod() + getPiDevCorrection();
        Bd = getDevCorrection()*1/sqrt(y);
        STensorOperation::zero(dB_dDevEe);
        dB_dDevEe = dev;
        dB_dDevEe *= (-getDevCorrection()*getThetaDevCorrection()/pow(y,1.5));
        if(getThetaDevCorrection()>0.){
            intB = getDevCorrection()/getThetaDevCorrection()*sqrt(y);  // integral of B_d * devEe
            intB -= ( getDevCorrection()/getThetaDevCorrection()*sqrt(getPiDevCorrection()) );  // value at devEe = 0.  
        }
        else{
            intB = 1.;
        }   
        
    }
    else if (_ExtraBranch_TVE_option == 3){
        
        // Tension-Compression Asymmetry 
        // Standard form => A_v = a/(sqrt(b*tr*tr+c))
        
        // Regularising function
        double m = _tensionCompressionRegularisation;
        double expmtr = exp(-m*tr);
        // if (exp(-m*tr)<1.e+10){ expmtr = exp(-m*tr);}
        double sigmoid = 1/(1.+expmtr);
        
        // Av
        double x = getXiVolumeCorrection()*tr*tr + getZetaVolumeCorrection();
        Av = sigmoid*1./sqrt(x) + (1.-sigmoid)*(_compCorrection/sqrt(x));
        Av *= getVolumeCorrection();
        
        dA_dTrEe = - sigmoid*getXiVolumeCorrection()*tr/pow(x,1.5) - (1.-sigmoid)*(getXiVolumeCorrection()*_compCorrection*tr/pow(x,1.5)) 
                   + (m*expmtr/pow((1.+expmtr),2.))*1./sqrt(x) - (_compCorrection*m*expmtr/pow((1.+expmtr),2.))*1./sqrt(x);
        dA_dTrEe *= getVolumeCorrection();
        
        if(getXiVolumeCorrection()>0.){
            double integrand = 1./getXiVolumeCorrection()*sqrt(x); // integral of A_v * trEe
            integrand -= ( 1./getXiVolumeCorrection()*sqrt(getZetaVolumeCorrection()) ); // value at trEe = 0.
            intA = sigmoid*integrand + _compCorrection*(1.-sigmoid)*integrand;
            intA *= getVolumeCorrection();

            DintA = (m*expmtr/pow((1.+expmtr),2.))*integrand - _compCorrection*(m*expmtr/pow((1.+expmtr),2.))*integrand;
            DintA *= getVolumeCorrection();
        }
        else{
            intA = 1.;
        }
        
        // Bd    
        double y = getThetaDevCorrection()*dev.dotprod() + getPiDevCorrection();
        Bd = sigmoid*1./sqrt(y) + (1.-sigmoid)*(_compCorrection/sqrt(y)); 
        Bd *= getDevCorrection();
        
        STensorOperation::zero(dB_dDevEe);
        dB_dDevEe = dev;
        dB_dDevEe *= (-sigmoid*getThetaDevCorrection()/pow(y,1.5) - (1.-sigmoid)*(getThetaDevCorrection()*_compCorrection/pow(y,1.5)));
        dB_dDevEe *= getDevCorrection();
        
        if(dB_dTrEe!=NULL){
            *dB_dTrEe = (m*expmtr/pow((1.+expmtr),2.))*1./sqrt(y) - (_compCorrection*m*expmtr/pow((1.+expmtr),2.))*1./sqrt(y);
            *dB_dTrEe *= getDevCorrection();
        }    
        if(getThetaDevCorrection()>0.){
            double integrand = 1./getThetaDevCorrection()*sqrt(y);  // integral of B_d * devEe
            integrand -= (1./getThetaDevCorrection()*sqrt(getPiDevCorrection()) );  // value at devEe = 0.   
            intB = sigmoid*integrand + _compCorrection*(1.-sigmoid)*integrand;
            intB *= getDevCorrection();

            DintB = (m*expmtr/pow((1.+expmtr),2.))*integrand - _compCorrection*(m*expmtr/pow((1.+expmtr),2.))*integrand;
            DintB *= getDevCorrection();
        }
        else{
            intB = 1.;
        }   
    }
    else if (_ExtraBranch_TVE_option == 4){

            // Tension-Compression Asymmetry
            // Standard form => A_v = a/(sqrt(b*tr*tr+c))

            // Regularising function
            double m = _tensionCompressionRegularisation;
            double expmtr = exp(-m*tr);
            // if (exp(-m*tr)<1.e+10){ expmtr = exp(-m*tr);}
            double sigmoid = 1/(1.+expmtr);

            // Av
            double x1 = _V1[i]*tr*tr + _V2[i];
            double x2 = _V4[i]*tr*tr + _V5[i];
            Av = sigmoid*(1./sqrt(x1)+_V0[i]) + (1.-sigmoid)*_Ci[i]*(1/sqrt(x2)+_V3[i]);

            dA_dTrEe = - sigmoid*_V1[i]*tr/pow(x1,1.5) - (1.-sigmoid)*(_V4[i]*_Ci[i]*tr/pow(x2,1.5))
                       + (m*expmtr/pow((1.+expmtr),2.))*(1./sqrt(x1)+_V0[i]) - (_Ci[i]*m*expmtr/pow((1.+expmtr),2.))*(1/sqrt(x2)+_V3[i]);

            if(_V1[i]>0. && _V4[i]>0.){
                double integrand1 = 1./_V1[i]*sqrt(x1) + _V0[i]*tr*tr/2.; // integral of A_v * trEe
                double integrand2 = 1./_V4[i]*sqrt(x2) + _V3[i]*tr*tr/2.;
                integrand1 -= ( 1./_V1[i]*sqrt(_V2[i]) ); // value at trEe = 0.
                integrand2 -= ( 1./_V4[i]*sqrt(_V5[i]) ); // value at trEe = 0.
                intA = sigmoid*integrand1 + _Ci[i]*(1.-sigmoid)*integrand2;

                DintA = (m*expmtr/pow((1.+expmtr),2.))*integrand1 - _Ci[i]*(m*expmtr/pow((1.+expmtr),2.))*integrand2;
            }
            else{
                intA = 1.;
                // Msg::Error(" Inside extraBranch_nonLinearTVE Type 4, _V1 = 0. or _V4 = 0., incompatibility with Mullin's effect");
            }

            // Bd
            double y1 = _D1[i]*dev.dotprod() + _D2[i];
            double y2 = _D4[i]*dev.dotprod() + _D5[i];
            Bd = sigmoid*(1./sqrt(y1)+_D0[i]) + (1.-sigmoid)*_Ci[i]*(1/sqrt(y2)+_D3[i]);

            STensorOperation::zero(dB_dDevEe);
            dB_dDevEe = dev;
            dB_dDevEe *= (-sigmoid*_D1[i]/pow(y1,1.5) - (1.-sigmoid)*(_D4[i]*_Ci[i]/pow(y2,1.5)));

            if(dB_dTrEe!=NULL){
                *dB_dTrEe = ( (m*expmtr/pow((1.+expmtr),2.))*(1./sqrt(y1)+_D0[i]) - (_Ci[i]*m*expmtr/pow((1.+expmtr),2.))*(1/sqrt(y2)+_D3[i]) );
            }
            if(_D1[i]>0. && _D4[i]>0.){
                double integrand1 = 1./_D1[i]*sqrt(y1) + _D0[i]*dev.dotprod()/2.;  // integral of B_d * devEe
                double integrand2 = 1./_D4[i]*sqrt(y2) + _D3[i]*dev.dotprod()/2.;
                integrand1 -= ( 1./_D1[i]*sqrt(_D2[i]) );  // value at devEe = 0.
                integrand2 -= ( 1./_D4[i]*sqrt(_D5[i]) );  // value at devEe = 0.
                intB = sigmoid*integrand1 + _Ci[i]*(1.-sigmoid)*integrand2;

                DintB = (m*expmtr/pow((1.+expmtr),2.))*integrand1 - _Ci[i]*(m*expmtr/pow((1.+expmtr),2.))*integrand2;
            }
            else{
                intB = 1.;
                // Msg::Error(" Inside extraBranch_nonLinearTVE Type 4, _D1 = 0. or _D4 = 0., incompatibility with Mullin's effect");
            }
        }
    else if (_ExtraBranch_TVE_option == 5){

            // Tension-Compression Asymmetry
            // Standard form => A_v = a/(sqrt(b*tr*tr+c)) + d

    		// Msg::Error("_extraBran_ExtraBranch_TVE_optionchNLType = %d need a 3rd Parameter. Define _V3 and _D3",_extraBranchNLType);

            // Regularising function
            double m = _tensionCompressionRegularisation;
            double expmtr = exp(-m*tr);
            double sigmoid = 1/(1.+expmtr);

            // Av
            double x1 = _V1[i]*tr*tr + _V2[i];
            double x2 = _V4[i]*tr*tr;
            Av = sigmoid*(1./sqrt(x1)+ _V3[i]*(_V5[i]+tanh(x2))) + (1.-sigmoid)*(_Ci[i]*(1./sqrt(x1) + _V0[i]));
            // Av = sigmoid*(1./sqrt(x1)+_V0[i]) + (1.-sigmoid)*_Ci[i]*(1/sqrt(x2)+_V3[i]); -> Type4
            // Av *= getVolumeCorrection();

            dA_dTrEe = -sigmoid*(_V1[i]*tr/pow(x1,1.5) - _V3[i]*(2*_V4[i]*tr*pow(1/cosh(x2),2))) - (1.-sigmoid)*(_V1[i]*_Ci[i]*tr/pow(x1,1.5))
                       + (m*expmtr/pow((1.+expmtr),2.))*(1./sqrt(x1) + _V3[i]*(_V5[i]+tanh(x2)))- (_Ci[i]*m*expmtr/pow((1.+expmtr),2.))*(1./sqrt(x1) + _V0[i]);
            // dA_dTrEe *= getVolumeCorrection();

            if(_V1[i]>0.){
                double integrand1 = 1./_V1[i]*sqrt(x1) + _V3[i]*_V5[i]*tr*tr/2.; // integral of A_v * trEe
                double integrand2 = 0.;
                double integrand3 = 1./_V1[i]*sqrt(x1) + _V0[i]*tr*tr/2.;
                if(_V4[i] != 0.){
                	integrand2 = _V3[i]/(2.*_V4[i])*log(cosh(x2));
                }
                integrand1 -= ( 1./_V1[i]*sqrt(_V2[i]) ); // value at trEe = 0.
                integrand3 -= ( 1./_V1[i]*sqrt(_V2[i]) ); // value at trEe = 0.
                intA = sigmoid*(integrand1 + integrand2) + _Ci[i]*(1.-sigmoid)*integrand3;
                // intA *= getVolumeCorrection();

                DintA = (m*expmtr/pow((1.+expmtr),2.))*(integrand1 + integrand2) - _Ci[i]*(m*expmtr/pow((1.+expmtr),2.))*integrand3;
            }
            else{
                intA = 1.;
            }

            // Bd
            double y1 = _D1[i]*dev.dotprod() + _D2[i];
            double y2 = _D4[i]*dev.dotprod();
            Bd = sigmoid* (1./sqrt(y1) + _D3[i]*(_D5[i] + tanh(y2))) + (1.-sigmoid)*(_Ci[i]*(1./sqrt(y1) + _D0[i])) ;
            // Bd *= getDevCorrection();

            STensorOperation::zero(dB_dDevEe);
            dB_dDevEe = dev;
            dB_dDevEe *= (-sigmoid*(_D1[i]/pow(y1,1.5) - _D3[i]*(2*_D4[i]*pow(1/cosh(x2),2))) - (1.-sigmoid)*(_D1[i]*_Ci[i]/pow(y1,1.5)));
            // dB_dDevEe *= getDevCorrection();

            if(dB_dTrEe!=NULL){
                *dB_dTrEe = (m*expmtr/pow((1.+expmtr),2.))*(1./sqrt(y1) + _D3[i]*(_D5[i] + tanh(y2))) - (m*expmtr/pow((1.+expmtr),2.))*(_Ci[i]*(1./sqrt(y1) + _D0[i]));
                // *dB_dTrEe *= getDevCorrection();
            }
            if(_D1[i]>0.){
                double integrand1 = 1./_D1[i]*sqrt(y1) + _D3[i]*_D5[i]*dev.dotprod()/2.;  // integral of B_d * devEe
                double integrand2 = 0.;
                double integrand3 = 1./_D1[i]*sqrt(y1) + _D0[i]*dev.dotprod()/2.;
                if(_D4[i] != 0.){
                	integrand2 = _D3[i]/(2.*_D4[i])*log(cosh(y2));
                }
                integrand1 -= (1./_D1[i]*sqrt(_D2[i]) );  // value at devEe = 0.
                integrand3 -= (1./_D1[i]*sqrt(_D2[i]) );  // value at devEe = 0.
                intB = sigmoid*(integrand1 + integrand2) + _Ci[i]*(1.-sigmoid)*integrand3;
                // intB *= getDevCorrection();

                DintB = (m*expmtr/pow((1.+expmtr),2.))*(integrand1 + integrand2) - _Ci[i]*(m*expmtr/pow((1.+expmtr),2.))*integrand3;
            }
            else{
                intB = 1.;
            }
        }
    else{ // Test
        double x = getXiVolumeCorrection()*tr*tr + getZetaVolumeCorrection();
        Av = getVolumeCorrection()*(tanh(getXiVolumeCorrection()/3.*tr*tr-getZetaVolumeCorrection())+tanh(getZetaVolumeCorrection()));
        dA_dTrEe = getVolumeCorrection()*getXiVolumeCorrection()*2./3.*(1.-tanh(getXiVolumeCorrection()/3.*tr*tr-getZetaVolumeCorrection())*tanh(getXiVolumeCorrection()/3.*tr*tr-getZetaVolumeCorrection()));
        intA = 1.;

        Bd = getDevCorrection()*(tanh(getThetaDevCorrection()*dev.dotprod()-getPiDevCorrection())+tanh(getPiDevCorrection()));
        STensorOperation::zero(dB_dDevEe);
        dB_dDevEe=dev;
        dB_dDevEe*=2.*getPiDevCorrection()*getDevCorrection()*(1.-tanh(getThetaDevCorrection()*dev.dotprod()-getPiDevCorrection())*tanh(getThetaDevCorrection()*dev.dotprod()-getPiDevCorrection()) );
        intB = 1.;
    }
    // TBD - tempfuncs
    /*        
    Av *= (_temFunc_elasticCorrection_Bulk->getVal(T));
    dA_dTrEe *= (_temFunc_elasticCorrection_Bulk->getVal(T));
    intA *= (_temFunc_elasticCorrection_Bulk->getVal(T));
    intA1 *= (_temFunc_elasticCorrection_Bulk->getVal(T));
    
    dA_dT = (Av*_temFunc_elasticCorrection_Bulk->getDiff(T));
    ddA_dTT = (Av*_temFunc_elasticCorrection_Bulk->getDoubleDiff(T));
    ddA_dTdTrEe = (dA_dTrEe*_temFunc_elasticCorrection_Bulk->getDiff(T)/_temFunc_elasticCorrection_Bulk->getVal(T));
    
    Bd *= (_temFunc_elasticCorrection_Shear->getVal(T));
    dB_dDevEe *= (_temFunc_elasticCorrection_Shear->getVal(T));
    intB *= (_temFunc_elasticCorrection_Shear->getVal(T));
    intB1 *= (_temFunc_elasticCorrection_Shear->getVal(T));
        
    dB_dT = (Bd*_temFunc_elasticCorrection_Shear->getDiff(T));
    ddB_dTT = (Bd*_temFunc_elasticCorrection_Shear->getDoubleDiff(T));
    ddB_dTdDevEe = (dB_dDevEe*_temFunc_elasticCorrection_Shear->getDiff(T));
    ddB_dTdDevEe *= (1/_temFunc_elasticCorrection_Shear->getVal(T));*/
        
    // Msg::Error(" Inside evaluatePhiPCorrection, A_v = %e, B_d = %e !!", A_v, B_d);
                            
};
                        
void mlawNonLinearTVM::ThermoViscoElasticPredictor(const STensor3& Ee, const STensor3& Ee0,
          const IPNonLinearTVM *q0, IPNonLinearTVM *q1,
          double& Ke, double& Ge,
          const double T0, const double T1, const bool stiff, STensor43& Bd_stiffnessTerm, STensor43* Bd_stiffnessTerm2) const{

    // NEW
    STensorOperation::zero(Bd_stiffnessTerm);
    // if(_extraBranchNLType == TensionCompressionRegularisedType && Bd_stiffnessTerm2!= NULL){
    if(Bd_stiffnessTerm2!= NULL){
        STensorOperation::zero(*Bd_stiffnessTerm2);
    }
    
    // Set Temperature to calculate temperature updated properties
    double T_set = setTemp(T0,T1,1);  // 1->T1, 2->T_mid, 3->T_midshift

    // Update the Properties to the current temperature (see below which ones are being used)
    double KT, GT, AlphaT0, AlphaT, DKDT, DGDT, DAlpha0DT, DAlphaDT, DDAlphaDTT;
    getK(q1,KT,T_set,&DKDT); getG(q1,GT,T_set,&DGDT); getAlpha(AlphaT,T_set,&DAlphaDT,&DDAlphaDTT); getAlpha(AlphaT0,T0);

    // Get LogStrain (E) and put DEe in IP
    STensor3& DE = q1->getRefToDE();
    STensor3& devDE = q1->getRefToDevDE();
    double& trDE = q1->getRefToTrDE();
    static STensor3 devEe;
    double trEe(0.);
    DE =  Ee;
    DE -= Ee0;
    STensorOperation::decomposeDevTr(DE,devDE,trDE);
    STensorOperation::decomposeDevTr(Ee,devEe,trEe);
    
    // Initialise effective trEe - include thermal expansion
    double eff_trEe(0.), eff_trDE(0.);
    eff_trEe = trEe - 3*AlphaT*(T1-_Tinitial);
    eff_trDE = trDE - 3*(AlphaT*(T1-_Tinitial) - AlphaT0*(T0-_Tinitial));

    // Initialise CorKirStress - Dev
    static STensor3 devK;
    STensorOperation::zero(devK);
    devK = 2*GT*devEe;
    
    // Initialise CorKirStress - Tr
    double p(0.);
    p = KT*eff_trEe;
    
    // Initialise Moduli
    Ke = KT; Ge = GT;
    
    if(_useExtraBranch){
        
        double A(0.), B(0.), dA_dTrEe(0.), psiInfCorrector(0.);
        static STensor3 dB_dDevEe;
        double intA(0.), intB(0.), dA_dT(0.), dB_dT(0.), DintA(0.), DintB(0.);
        
        // Here, A, B and related terms first calculated as (1+A) and (1+B)
        if(_extraBranchNLType != TensionCompressionRegularisedType && _extraBranchNLType != hyper_exp_TCasymm_Type){
            evaluateElasticCorrection(eff_trEe, devEe, T1, A, dA_dTrEe, intA, dA_dT, B, dB_dDevEe, intB, dB_dT, NULL ,NULL, NULL, NULL, NULL, &psiInfCorrector, &DintA, &DintB);
        }
        else{
            double dB_dTrEe(0.);
            evaluateElasticCorrection(eff_trEe, devEe, T1, A, dA_dTrEe, intA, dA_dT, B, dB_dDevEe, intB, dB_dT, NULL ,NULL, NULL, NULL, &dB_dTrEe, &psiInfCorrector, &DintA, &DintB);
            q1->_dBd_dTrEe = dB_dTrEe;
        }
    
        q1->_intA = intA;
        q1->_intB = intB;
        q1->_DintA = DintA;
        q1->_DintB = DintB;
        q1->_psiInfCorrector = psiInfCorrector; // Additional term to correct thermal expansion term in free energy
        q1->_elasticBulkPropertyScaleFactor = A; 
        q1->_elasticShearPropertyScaleFactor = B; 
        q1->_dAv_dTrEe = dA_dTrEe;
        q1->_dBd_dDevEe = dB_dDevEe;
        
        
        // Add to stress
        static STensor3 sigExtra;
        sigExtra = devEe;
        sigExtra *= 2.*GT*B;
        devK += sigExtra;
        sigExtra += _I*(eff_trEe*KT*A);
        p += eff_trEe*KT*A;
        q1->_corKirExtra = sigExtra; 
        
        // Add to Ke
        Ke += KT*A + KT*dA_dTrEe*eff_trEe;
        
        // Add to Ge (1st term) and Bd_stiffnessTerm (2nd term)
        Ge += GT*B; // don't multiply 2
        for (int k=0; k<3; k++)
            for (int l=0; l<3; l++)
                for (int p=0; p<3; p++)
                    for (int q=0; q<3; q++){
                        Bd_stiffnessTerm(k,l,p,q) +=  2*GT*devEe(k,l)*dB_dDevEe(p,q);
                        if(_extraBranchNLType == TensionCompressionRegularisedType || _extraBranchNLType == hyper_exp_TCasymm_Type){
                            (*Bd_stiffnessTerm2)(k,l,p,q) +=  2*GT*devEe(k,l)*_I(p,q)*q1->_dBd_dTrEe;
                        }
                    }
                        
    }
    
    // Main Loop for Maxwell Branches
    if ((_Ki.size() > 0) or (_Gi.size() > 0)){

        // Get ShiftFactor and shifted time increment
        double dt = this->getTimeStep();
        double dt_shift_rec(0.), dt_shift_mid(0.);
        double Ddt_shiftDT_rec(0.), Ddt_shiftDT_mid(0.), DDdt_shiftDT_rec(0.), DDdt_shiftDT_mid(0.);
        shiftedTime(dt, T0, T1, &dt_shift_rec,&Ddt_shiftDT_rec,&DDdt_shiftDT_rec,0);
        shiftedTime(dt, T0, T1, &dt_shift_mid,&Ddt_shiftDT_mid,&DDdt_shiftDT_mid);

                    
        // Do non-linear TVE extraBranch here
        double Av(1.), Bd(1.);
        double dA_dTrEe(0.),intA(0.),intA1(0.),intA2(0.),dA_dT(0.),ddA_dTT(0.),ddA_dTdTrEe(0.),intB(0.),dB_dT(0.),ddB_dTT(0.),DintA(0.),DintB(0.);
        static STensor3 intB1,intB2,dB_dDevEe,ddB_dTdDevEe;
        STensorOperation::zero(intB1);
        STensorOperation::zero(intB2);
        STensorOperation::zero(dB_dDevEe);
        STensorOperation::zero(ddB_dTdDevEe);
        
        if (_useExtraBranch_TVE){
            
            // NEW
            // STensorOperation::zero(Bd_stiffnessTerm); //This term is for the additional term in Ge that is a tensor
            // NEW
            
            if(_ExtraBranch_TVE_option == 1){
                mlawNonLinearTVM::extraBranch_nonLinearTVE(0,Ee,T1,Av,dA_dTrEe,intA,intA1,intA2,dA_dT,ddA_dTT,ddA_dTdTrEe,Bd,dB_dDevEe,intB,intB1,intB2,dB_dT,ddB_dTT,ddB_dTdDevEe,DintA,DintB);
                q1->_Av_TVE = Av; q1->_Bd_TVE = Bd; q1->_dAv_dTrEe_TVE = dA_dTrEe; q1->_dBd_dDevEe_TVE = dB_dDevEe;
                q1->_intAv_TVE = intA; q1->_intBd_TVE = intB; q1->_intAv_1_TVE = intA1; q1->_intBd_1_TVE = intB1;  // this is zero
                q1->_intAv_2_TVE = intA2; q1->_intBd_2_TVE = intB2;  // this is zero
            }
            
            // NEW
            /*if(_ExtraBranch_TVE_option == 3){
                STensorOperation::zero(*Bd_stiffnessTerm2); //This term is for the additional term in Ge because Bd is a function of trEe
            }*/
            // NEW
        }
        
        for (int i=0; i<_Gi.size(); i++){
                        
            // Dev Q_i and  Tr Q_i
            
            double dtg = dt_shift_rec/(_gi[i]);
            double dtg2 = dt_shift_mid/(_gi[i]);
            double exp_rec_g = exp(-dtg);     // rec
            double exp_mid_g = exp(-dtg2); // mid
            double dtk = dt_shift_rec/(_ki[i]);
            double dtk2 = dt_shift_mid/(_ki[i]);
            double exp_rec_k = exp(-dtk);      // rec
            double exp_mid_k = exp(-dtk2);  // mid
            
            if (!_useExtraBranch_TVE){
                
                // Ge for DcorKirDE
                Ge += _Gi[i]*exp_mid_g; // 2* (..) - Dont multiply by 2 if using lambda and mu in Hooks tensor function
            
                // Ke for DcorKirDE
                Ke += _Ki[i]*exp_mid_k;
            
                // Single Convolution
                for (int k=0; k<3; k++)
                    for (int l=0; l<3; l++){
                        q1->_A[i](k,l) = exp_rec_g*q0->_A[i](k,l) + exp_mid_g*devDE(k,l);
                    }
            
                q1->_B[i] = exp_rec_k*q0->_B[i] + exp_mid_k*eff_trDE ; //*trDE; NEW
            
                // Add to deviatoric stress
                devK += 2*_Gi[i]*q1->_A[i];
            
                // Add to volumetric stress
                p += _Ki[i]*q1->_B[i];
            }
            else{ // all extraBranches
                
                if(_ExtraBranch_TVE_option == 1){
                
                    // Evaluate Bd_stiffnessTerm
                    for (int k=0; k<3; k++)
                        for (int l=0; l<3; l++)
                            for (int p=0; p<3; p++)
                                for (int q=0; q<3; q++)
                                    Bd_stiffnessTerm(k,l,p,q) +=  2*_Gi[i]*q0->_A[i](k,l)*q0->_dBd_dDevEe_TVE(p,q) * 1/q0->_Bd_TVE * exp_mid_g;
                
                    // Ge for DcorKirDE
                    Ge += ( exp_mid_g *_Gi[i] * q0->_Bd_TVE ); // 2* (..) - Dont multiply by 2 if using lambda and mu in Hooks tensor function
            
                    // Ke for DcorKirDE
                    Ke += ( exp_mid_k *_Ki[i] * Av ) ; 
                    Ke += ( exp_mid_k *_Ki[i]* q0->_B[i]/Av * dA_dTrEe );
            
                    // Single Convolution
                    double temp2(0.);
                    STensorOperation::doubleContractionSTensor3(q0->_dBd_dDevEe_TVE,devDE,temp2);
                    for (int k=0; k<3; k++)
                        for (int l=0; l<3; l++)
                            q1->_A[i](k,l) = exp_rec_g*q0->_A[i](k,l) + exp_mid_g*( q0->_Bd_TVE*devDE(k,l) + q0->_A[i](k,l) * 1/q0->_Bd_TVE * temp2);
            
                    q1->_B[i] = ( exp_rec_k*q0->_B[i] );
                    q1->_B[i] += ( exp_mid_k *( q1->_intAv_1_TVE - q0->_intAv_1_TVE ) ); 
                    q1->_B[i] += ( exp_mid_k*( q0->_B[i]*(q1->_intAv_2_TVE - q0->_intAv_2_TVE) ) );
                    
                    // Add to deviatoric stress
                    devK += ( 2.*_Gi[i]*q1->_A[i] );
            
                    // Add to volumetric stress
                    p += ( _Ki[i]*q1->_B[i] );
                    
                }
                else if (_ExtraBranch_TVE_option == 2 || _ExtraBranch_TVE_option == 3 || _ExtraBranch_TVE_option == 4 || _ExtraBranch_TVE_option == 5){
                    
                    // Single Convolution - get branchElasticStrain
                    for (int k=0; k<3; k++)
                        for (int l=0; l<3; l++)
                            q1->_A[i](k,l) = exp_rec_g*q0->_A[i](k,l) + exp_mid_g*devDE(k,l);
            
                    q1->_B[i] = exp_rec_k*q0->_B[i] + exp_mid_k*eff_trDE; // trDE; // NEW
                    
                    static STensor3 branchElasticStrain;
                    branchElasticStrain = q1->_A[i];
                    branchElasticStrain(0,0) += 1./3.*q1->_B[i];
                    branchElasticStrain(1,1) += 1./3.*q1->_B[i];
                    branchElasticStrain(2,2) += 1./3.*q1->_B[i];
                    if (_ExtraBranch_TVE_option == 2){
                        mlawNonLinearTVM::extraBranch_nonLinearTVE(i,branchElasticStrain,T1,Av,dA_dTrEe,intA,intA1,intA2,dA_dT,ddA_dTT,ddA_dTdTrEe,
                                                                Bd,dB_dDevEe,intB,intB1,intB2,dB_dT,ddB_dTT,ddB_dTdDevEe,DintA,DintB);
                    }
                    else{
                        double dB_dTrEe(0.);
                        mlawNonLinearTVM::extraBranch_nonLinearTVE(i,branchElasticStrain,T1,Av,dA_dTrEe,intA,intA1,intA2,dA_dT,ddA_dTT,ddA_dTdTrEe,
                                                                Bd,dB_dDevEe,intB,intB1,intB2,dB_dT,ddB_dTT,ddB_dTdDevEe,DintA,DintB,&dB_dTrEe);
                        q1->_dBd_dTrEe_TVE_vector[i] = dB_dTrEe;
                    }
                    
                    q1->_Av_TVE_vector[i] = Av;
                    q1->_Bd_TVE_vector[i] = Bd;
                    q1->_dAv_dTrEe_TVE_vector[i] = dA_dTrEe;
                    q1->_dBd_dDevEe_TVE_vector[i] = dB_dDevEe;
                    q1->_intAv_TVE_vector[i] = intA;
                    q1->_intBd_TVE_vector[i] = intB;
                    q1->_DintAv_TVE_vector[i] = DintA;
                    q1->_DintBd_TVE_vector[i] = DintB;
                    
                    // Evaluate Bd_stiffnessTerm and/or Bd_stiffnessTerm2
                    for (int k=0; k<3; k++)
                        for (int l=0; l<3; l++)
                            for (int p=0; p<3; p++)
                                for (int q=0; q<3; q++){
                                    Bd_stiffnessTerm(k,l,p,q) +=  2*_Gi[i]*q1->_A[i](k,l)*dB_dDevEe(p,q) * exp_mid_g; 
                                    if (_ExtraBranch_TVE_option == 3 || _ExtraBranch_TVE_option == 4 || _ExtraBranch_TVE_option == 5){
                                        (*Bd_stiffnessTerm2)(k,l,p,q) += 2*_Gi[i]*q1->_dBd_dTrEe_TVE_vector[i]*q1->_A[i](k,l)*_I(p,q) * exp_mid_k;
                                            // exp_mid_k = d(tr branchElasticStrain)_d(trEe)
                                    }
                                }
                                
                    // Ge for DcorKirDE
                    Ge += ( exp_mid_g *_Gi[i] * Bd); // 2* (..) - Dont multiply by 2 if using lambda and mu in Hooks tensor function
                            // exp_mid_g = d(dev branchElasticStrain)_d(devEe)
                    
                    // Ke for DcorKirDE
                    Ke += ( exp_mid_k *_Ki[i] * (dA_dTrEe*q1->_B[i] + Av) );  // exp_mid_k = d(tr branchElasticStrain)_d(trEe)
                    
                    // Add to deviatoric stress
                    devK += ( 2.*_Gi[i]*Bd*q1->_A[i] );
            
                    // Add to volumetric stress
                    p += ( _Ki[i]*Av*q1->_B[i] );
                }

            }
        }
        
        // Calculate derivatives here and put them in IP
        if(stiff){
            std::vector<double>& DB_DT = q1->_DB_DT;
            std::vector<double>& DB_DtrE = q1->_DB_DtrE;
            std::vector<double>& DDB_DTT = q1->_DDB_DTT;
            std::vector<double>& DDB_DTDtrE = q1->_DDB_DTDtrE;
            std::vector<STensor3>& DA_DT = q1->_DA_DT;
            std::vector<STensor3>& DDA_DTT = q1->_DDA_DTT;
            std::vector<STensor3>& DA_DtrE = q1->_DA_DtrE;
            std::vector<STensor3>& DDA_DTDtrE = q1->_DDA_DTDtrE;
            std::vector<STensor43>& DA_DdevE = q1->_DA_DdevE;
            std::vector<STensor43>& DDA_DTDdevE = q1->_DDA_DTDdevE;
                    
            double Dexp_rec_k_DT(0.), Dexp_mid_k_DT(0.), DDexp_rec_k_DTT(0.), DDexp_mid_k_DTT(0.);  
            double Dexp_rec_g_DT(0.), Dexp_mid_g_DT(0.), DDexp_rec_g_DTT(0.), DDexp_mid_g_DTT(0.);  
            
            double dtk(0.), dtk2(0.), exp_rec_k(0.), exp_mid_k(0.);
            double dtg(0.), dtg2(0.), exp_rec_g(0.), exp_mid_g(0.);
            
            // NEW
            double Deff_trDE_DT = -3.*(DAlphaDT*(T1-_Tinitial) + AlphaT);
            double DDeff_trDE_DTT = -3.*(DDAlphaDTT*(T1-_Tinitial) + 2*DAlphaDT);
            // NEW
            
            for (int i=0; i<_Gi.size(); i++){   
                
                dtk = dt_shift_rec/(_ki[i]);
                dtk2 = dt_shift_mid/(_ki[i]);
                exp_rec_k = exp(-dtk);     // rec
                exp_mid_k = exp(-dtk2); // mid
                
                dtg = dt_shift_rec/(_gi[i]);
                dtg2 = dt_shift_mid/(_gi[i]);
                exp_rec_g = exp(-dtg);     // rec
                exp_mid_g = exp(-dtg2); // mid
            
                Dexp_rec_k_DT = -1/(_ki[i]) * Ddt_shiftDT_rec*exp_rec_k;
                Dexp_mid_k_DT = -1/(_ki[i]) * Ddt_shiftDT_mid*exp_mid_k;
                Dexp_rec_g_DT = -1/(_gi[i]) * Ddt_shiftDT_rec*exp_rec_g;
                Dexp_mid_g_DT = -1/(_gi[i]) * Ddt_shiftDT_mid*exp_mid_g;
                
                DDexp_rec_k_DTT = -1/(_ki[i]) * (DDdt_shiftDT_rec - pow(Ddt_shiftDT_rec,2)/(_ki[i]))*exp_rec_k;
                DDexp_mid_k_DTT = -1/(_ki[i]) * (DDdt_shiftDT_mid - pow(Ddt_shiftDT_mid,2)/(_ki[i]))*exp_mid_k;
                DDexp_rec_g_DTT = -1/(_gi[i]) * (DDdt_shiftDT_rec - pow(Ddt_shiftDT_rec,2)/(_gi[i]))*exp_rec_g;
                DDexp_mid_g_DTT = -1/(_gi[i]) * (DDdt_shiftDT_mid - pow(Ddt_shiftDT_mid,2)/(_gi[i]))*exp_mid_g;
                
                if (!_useExtraBranch_TVE){
                    DB_DtrE[i] = exp_mid_k;
                    DB_DT[i] = q0->_B[i] * Dexp_rec_k_DT + Dexp_mid_k_DT*eff_trDE + exp_rec_k*Deff_trDE_DT; // trDE; // NEW
                    DDB_DTT[i] = q0->_B[i] * DDexp_rec_k_DTT + DDexp_mid_k_DTT*eff_trDE + 2*Dexp_mid_k_DT*Deff_trDE_DT + exp_mid_k*DDeff_trDE_DTT; // trDE; //NEW
                    DDB_DTDtrE[i] = Dexp_mid_k_DT;
                    for (int k=0; k<3; k++)
                        for (int l=0; l<3; l++){
                            DA_DT[i](k,l) = q0->_A[i](k,l) * Dexp_rec_g_DT + Dexp_mid_g_DT*devDE(k,l); 
                            DDA_DTT[i](k,l) = q0->_A[i](k,l) * DDexp_rec_g_DTT + DDexp_mid_g_DTT*devDE(k,l); 
                            for (int r=0; r<3; r++)
                                for (int s=0; s<3; s++){
                                    // _Idev is pre-multiplied !!
                                    DA_DdevE[i](k,l,r,s) =  exp_mid_g*_Idev(k,l,r,s);  
                                    DDA_DTDdevE[i](k,l,r,s) =  Dexp_mid_g_DT*_Idev(k,l,r,s);  
                                }
                        }
                }
                
                else{ // all extraBranches
                
                 if(_ExtraBranch_TVE_option == 1){
                     
                    DB_DtrE[i] = exp_mid_k * ( q1->_Av_TVE + q0->_B[i]/q1->_Av_TVE * dA_dTrEe );
                    DB_DT[i] = q0->_B[i] * Dexp_rec_k_DT + Dexp_mid_k_DT*( (q1->_intAv_1_TVE - q0->_intAv_1_TVE) +
                                                            q0->_B[i]*(q1->_intAv_2_TVE - q0->_intAv_2_TVE) );
                    DDB_DTT[i] = q0->_B[i] * DDexp_rec_k_DTT + DDexp_mid_k_DTT*( (q1->_intAv_1_TVE - q0->_intAv_1_TVE) +
                                                            q0->_B[i]*(q1->_intAv_2_TVE - q0->_intAv_2_TVE) );
                    DDB_DTDtrE[i] = Dexp_mid_k_DT * ( q1->_Av_TVE + q0->_B[i]/q1->_Av_TVE * dA_dTrEe );
                                
                    double temp2(0.);
                    STensorOperation::doubleContractionSTensor3(q0->_dBd_dDevEe_TVE,devDE,temp2);
                    for (int k=0; k<3; k++)
                        for (int l=0; l<3; l++){
                            DA_DT[i](k,l) = q0->_A[i](k,l) * Dexp_rec_g_DT + Dexp_mid_g_DT*( q0->_Bd_TVE*devDE(k,l) + 
                                                                                q0->_A[i](k,l) * 1/q0->_Bd_TVE * temp2); 
                            DDA_DTT[i](k,l) = q0->_A[i](k,l) * DDexp_rec_g_DTT + DDexp_mid_g_DTT*( q0->_Bd_TVE*devDE(k,l) + 
                                                                                q0->_A[i](k,l) * 1/q0->_Bd_TVE * temp2); 
                            for (int r=0; r<3; r++)
                                for (int s=0; s<3; s++){
                                    // _Idev is not pre-multiplied !! 
                                    DA_DdevE[i](k,l,r,s) =  exp_mid_g * ( q0->_Bd_TVE * _I4(k,l,r,s) + 1/q0->_Bd_TVE * q0->_A[i](k,l)*q0->_dBd_dDevEe_TVE(r,s) );  
                                    DDA_DTDdevE[i](k,l,r,s) =  Dexp_mid_g_DT * ( q0->_Bd_TVE * _I4(k,l,r,s) + 1/q0->_Bd_TVE * q0->_A[i](k,l)*q0->_dBd_dDevEe_TVE(r,s) );  
                                }
                        }
                 }
                 else if (_ExtraBranch_TVE_option == 2 || _ExtraBranch_TVE_option == 3 || _ExtraBranch_TVE_option == 4 || _ExtraBranch_TVE_option == 5){
                    // Here the derivatives are made as: B[i] = Av[i]*B[i]; A[i] = Bd[i]*A[i]; Above, BranchElasticStrain = A[i] + 1/3*B[i]*_I
                    // 2nd order derivatives of _Av_TVE_vector and _Bd_TVE_vector are ignored
                    
                    double dTrEei_DT(0.), ddTrEei_DTT(0.); // branchElasticStrain trace
                    static STensor3 dDevEei_DT, ddDevEei_DTT; // branchElasticStrain deviator
                    dTrEei_DT = q0->_B[i] * Dexp_rec_k_DT + Dexp_mid_k_DT*eff_trDE + exp_mid_k*Deff_trDE_DT; // trDE; //NEW
                    ddTrEei_DTT = q0->_B[i] * DDexp_rec_k_DTT + DDexp_mid_k_DTT*eff_trDE + 2.*Dexp_mid_k_DT*Deff_trDE_DT + exp_mid_k*DDeff_trDE_DTT; // trDE; //NEW
                    for (int k=0; k<3; k++)
                        for (int l=0; l<3; l++){
                            dDevEei_DT(k,l) = q0->_A[i](k,l) * Dexp_rec_g_DT + Dexp_mid_g_DT*devDE(k,l);
                            ddDevEei_DTT(k,l) = q0->_A[i](k,l) * DDexp_rec_g_DTT + DDexp_mid_g_DTT*devDE(k,l);
                        }
                            
                    DB_DtrE[i] = exp_mid_k * ( q1->_dAv_dTrEe_TVE_vector[i] * q1->_B[i] + q1->_Av_TVE_vector[i] );
                    DB_DT[i] = q1->_Av_TVE_vector[i]*dTrEei_DT + q1->_dAv_dTrEe_TVE_vector[i]*dTrEei_DT* q1->_B[i];
                    DDB_DTT[i] = q1->_Av_TVE_vector[i]* ddTrEei_DTT + 2.*q1->_dAv_dTrEe_TVE_vector[i]*pow(dTrEei_DT,2) + q1->_dAv_dTrEe_TVE_vector[i]*ddTrEei_DTT*q1->_B[i];
                    DDB_DTDtrE[i] = Dexp_mid_k_DT * ( q1->_dAv_dTrEe_TVE_vector[i] * q1->_B[i] + q1->_Av_TVE_vector[i] ) 
                                    + exp_mid_k * ( 2.*q1->_dAv_dTrEe_TVE_vector[i] * dTrEei_DT );
                                
                    for (int k=0; k<3; k++)
                        for (int l=0; l<3; l++){
                            DA_DT[i](k,l) = q1->_Bd_TVE_vector[i]*dDevEei_DT(k,l); 
                            DDA_DTT[i](k,l) = q1->_Bd_TVE_vector[i]*ddDevEei_DTT(k,l); 
                            
                            if (_ExtraBranch_TVE_option == 3 || _ExtraBranch_TVE_option == 4 || _ExtraBranch_TVE_option == 5){
                                DA_DT[i](k,l) += q1->_A[i](k,l)*q1->_dBd_dTrEe_TVE_vector[i]*dTrEei_DT;
                                DDA_DTT[i](k,l) += q1->_A[i](k,l)*q1->_dBd_dTrEe_TVE_vector[i]*ddTrEei_DTT +
                                                    2*q1->_dBd_dTrEe_TVE_vector[i]*dTrEei_DT*dDevEei_DT(k,l);  
                                
                                // _Idev is not required !! _I is not pre-multiplied !!
                                DA_DtrE[i](k,l) = q1->_dBd_dTrEe_TVE_vector[i] * exp_mid_k * q1->_A[i](k,l); // *_I(r,s);
                                DDA_DTDtrE[i](k,l) = q1->_dBd_dTrEe_TVE_vector[i] * (exp_mid_k*dDevEei_DT(k,l) + Dexp_mid_k_DT*q1->_A[i](k,l)); // *_I(r,s);
                            }
                                                                
                            for (int r=0; r<3; r++)
                                for (int s=0; s<3; s++){
                                    DA_DT[i](k,l) += q1->_A[i](k,l)*q1->_dBd_dDevEe_TVE_vector[i](r,s)*dDevEei_DT(r,s); 
                                    DDA_DTT[i](k,l) += 2.*dDevEei_DT(k,l)*q1->_dBd_dDevEe_TVE_vector[i](r,s)*dDevEei_DT(r,s) 
                                                        + q1->_A[i](k,l) * q1->_dBd_dDevEe_TVE_vector[i](r,s)*ddDevEei_DTT(r,s);
                                    
                                    // _Idev is not pre-multiplied !! 
                                    DA_DdevE[i](k,l,r,s) =  exp_mid_g * ( q1->_A[i](k,l)*q1->_dBd_dDevEe_TVE_vector[i](r,s) + q1->_Bd_TVE_vector[i] * _I4(k,l,r,s) );  
                                    DDA_DTDdevE[i](k,l,r,s) =  Dexp_mid_g_DT * ( q1->_A[i](k,l)*q1->_dBd_dDevEe_TVE_vector[i](r,s) + q1->_Bd_TVE_vector[i] * _I4(k,l,r,s) ) 
                                                                + exp_mid_g * ( dDevEei_DT(k,l)* q1->_dBd_dDevEe_TVE_vector[i](r,s));  
                                    for (int p=0; p<3; p++)
                                        for (int q=0; q<3; q++){
                                            DDA_DTDdevE[i](k,l,r,s) += exp_mid_g * q1->_dBd_dDevEe_TVE_vector[i](p,q)*dDevEei_DT(p,q)*_I4(k,l,r,s);
                                        }

                                    if (_ExtraBranch_TVE_option == 3 || _ExtraBranch_TVE_option == 4 || _ExtraBranch_TVE_option == 5){
                                    	 DDA_DTDdevE[i](k,l,r,s) += exp_mid_g*q1->_dBd_dTrEe_TVE_vector[i]*dTrEei_DT*_I4(k,l,r,s);
                                    }

                                }
                        }
                 } // if - extraBranchTVE_option
                } // if - extraBranchTVE
            } // for
        } // stiff
        else{ // if stiff = false, we still need DA_DT, DB_DT for DcorKirDT in mechSource
            std::vector<double>& DB_DT = q1->_DB_DT;
            std::vector<STensor3>& DA_DT = q1->_DA_DT;
                    
            double Dexp_rec_k_DT(0.), Dexp_mid_k_DT(0.);  
            double Dexp_rec_g_DT(0.), Dexp_mid_g_DT(0.);  
            
            double dtk(0.), dtk2(0.), exp_rec_k(0.), exp_mid_k(0.);
            double dtg(0.), dtg2(0.), exp_rec_g(0.), exp_mid_g(0.);
            
            // NEW
            double Deff_trDE_DT = -3.*(DAlphaDT*(T1-_Tinitial) + AlphaT);
            // NEW
            
            for (int i=0; i<_Gi.size(); i++){   
                
                dtk = dt_shift_rec/(_ki[i]);
                dtk2 = dt_shift_mid/(_ki[i]);
                exp_rec_k = exp(-dtk);     // rec
                exp_mid_k = exp(-dtk2); // mid
                
                dtg = dt_shift_rec/(_gi[i]);
                dtg2 = dt_shift_mid/(_gi[i]);
                exp_rec_g = exp(-dtg);     // rec
                exp_mid_g = exp(-dtg2); // mid
            
                Dexp_rec_k_DT = -1/(_ki[i]) * Ddt_shiftDT_rec*exp_rec_k;
                Dexp_mid_k_DT = -1/(_ki[i]) * Ddt_shiftDT_mid*exp_mid_k;
                Dexp_rec_g_DT = -1/(_gi[i]) * Ddt_shiftDT_rec*exp_rec_g;
                Dexp_mid_g_DT = -1/(_gi[i]) * Ddt_shiftDT_mid*exp_mid_g;
                
                if (!_useExtraBranch_TVE){
                    DB_DT[i] = q0->_B[i] * Dexp_rec_k_DT + Dexp_mid_k_DT*eff_trDE + exp_rec_k*Deff_trDE_DT; // trDE; // NEW
                    for (int k=0; k<3; k++)
                        for (int l=0; l<3; l++){
                            DA_DT[i](k,l) = q0->_A[i](k,l) * Dexp_rec_g_DT + Dexp_mid_g_DT*devDE(k,l); 
                        }
                }
                else{ // all extraBranches
                
                 if(_ExtraBranch_TVE_option == 1){
                    DB_DT[i] = q0->_B[i] * Dexp_rec_k_DT + Dexp_mid_k_DT*( (q1->_intAv_1_TVE - q0->_intAv_1_TVE) +
                                                            q0->_B[i]*(q1->_intAv_2_TVE - q0->_intAv_2_TVE) );
                                                            
                    double temp2(0.);
                    STensorOperation::doubleContractionSTensor3(q0->_dBd_dDevEe_TVE,devDE,temp2);
                    for (int k=0; k<3; k++)
                        for (int l=0; l<3; l++){
                            DA_DT[i](k,l) = q0->_A[i](k,l) * Dexp_rec_g_DT + Dexp_mid_g_DT*( q0->_Bd_TVE*devDE(k,l) + 
                                                                                q0->_A[i](k,l) * 1/q0->_Bd_TVE * temp2); 
                        }
                 }
                 else if (_ExtraBranch_TVE_option == 2 || _ExtraBranch_TVE_option == 3 || _ExtraBranch_TVE_option == 4 || _ExtraBranch_TVE_option == 5){
                    // Here the derivative are made as: B[i] = Av[i]*B[i]; A[i] = Bd[i]*A[i]; Above, BranchElasticStrain = A[i] + 1/3*B[i]*_I
                    // 2nd order derivatives of _Av_TVE_vector and _Bd_TVE_vector are ignored
                    
                    double dTrEei_DT(0.); // branchElasticStrain trace
                    static STensor3 dDevEei_DT;
                    dTrEei_DT = q0->_B[i] * Dexp_rec_k_DT + Dexp_mid_k_DT*eff_trDE + exp_mid_k*Deff_trDE_DT; // trDE; //NEW
                    for (int k=0; k<3; k++)
                        for (int l=0; l<3; l++)
                            dDevEei_DT(k,l) = q0->_A[i](k,l) * Dexp_rec_g_DT + Dexp_mid_g_DT*devDE(k,l);
                            
                    DB_DT[i] = q1->_Av_TVE_vector[i]*dTrEei_DT + q1->_dAv_dTrEe_TVE_vector[i]*dTrEei_DT* q1->_B[i];
                    for (int k=0; k<3; k++)
                        for (int l=0; l<3; l++){
                            DA_DT[i](k,l) = q1->_Bd_TVE_vector[i]*dDevEei_DT(k,l); 
                            
                            if (_ExtraBranch_TVE_option == 3 || _ExtraBranch_TVE_option == 4 || _ExtraBranch_TVE_option == 5){
                                DA_DT[i](k,l) += q1->_A[i](k,l)*q1->_dBd_dTrEe_TVE_vector[i]*dTrEei_DT;
                            }
                            
                            for (int r=0; r<3; r++)
                                for (int s=0; s<3; s++){
                                    DA_DT[i](k,l) += q1->_A[i](k,l)*q1->_dBd_dDevEe_TVE_vector[i](r,s)*dDevEei_DT(r,s); 
                                }
                        }
            
                 }         
                }
            }
        }
        
    }

    // Put Stress in IP
    // Msg::Error(" Inside TVE_predictorCorrector, Ke = %e, Ge = %e !!", Ke, Ge);
    q1->_kirchhoff = devK;
    q1->_kirchhoff(0,0) += p;
    q1->_kirchhoff(1,1) += p;
    q1->_kirchhoff(2,2) += p;
    q1->_trCorKirinf_TVE = p;
}

void mlawNonLinearTVM::isotropicHookTensor(const double K, const double G, STensor43& L) const{
  double lambda = K - 2.*G/3.;
  static STensor3 I(1.);
  for (int i=0; i<3; i++){
    for (int j=0; j<3; j++){
      for (int k=0; k<3; k++){
        for (int l=0; l<3; l++){
          L(i,j,k,l) = lambda*I(i,j)*I(k,l)+ G*(I(i,k)*I(j,l)+I(i,l)*I(j,k));
        }
      }
    }
  }
}

void mlawNonLinearTVM::getMechSource_TVE_term(const IPNonLinearTVM *q0, IPNonLinearTVM *q1, const double& T0, const double& T, 
                                        double& Wm, const bool stiff, STensor3& dWmdE, double& dWmdT) const{
                                          
    // Initialize
    Wm = 0.;

    // Ee
    static STensor3 devEe, devEe0;
    static double trEe, trEe0;
    STensorOperation::decomposeDevTr(q0->_Ee,devEe0,trEe0);
    STensorOperation::decomposeDevTr(q1->_Ee,devEe,trEe);

    // DEe
    const STensor3& DEe = q1->getConstRefToDE();
    const STensor3& devDE = q1->getConstRefToDevDE();
    const double& trDE = q1->getConstRefToTrDE();

    // Add visco terms
    if ((_Ki.size() > 0) or (_Gi.size() > 0)){
        
        // TVE Mechanical Source Term
        static std::vector<STensor3> Qi, DQiDT, dot_Gammai; // Qi -> Viscous Stress Tensor, Gammai -> Viscous Strain Tensor
        Qi.clear();
        DQiDT.clear();
        dot_Gammai.clear();
        STensor3 allZero(0.);
        for (int i=0; i < _N; i++){
            Qi.push_back(allZero);
            DQiDT.push_back(allZero);
            dot_Gammai.push_back(allZero);
        }
    
        for (int i=0; i<_Gi.size(); i++){
            
            STensorOperation::zero(Qi[i]);   
            STensorOperation::zero(DQiDT[i]);   
            STensorOperation::zero(dot_Gammai[i]);   
            for (int k=0; k<3; k++){
                for (int l=0; l<3; l++){
                    Qi[i](k,l) = _Ki[i]*q1->_B[i]*_I(k,l) + 2*_Gi[i]*q1->_A[i](k,l); 
                    DQiDT[i](k,l) = _Ki[i]*q1->_DB_DT[i]*_I(k,l) + 2*_Gi[i]*q1->_DA_DT[i](k,l); 
                    dot_Gammai[i](k,l) = 1/3*(trEe-q1->_B[i] - trEe0 + q0->_B[i])*_I(k,l) + (devEe(k,l)-q1->_A[i](k,l) - devEe0(k,l) + q0->_A[i](k,l) );
                }
            }
            if (this->getTimeStep() > 0){
                dot_Gammai[i] *= 1/this->getTimeStep();
                Wm -= STensorOperation::doubledot(Qi[i],dot_Gammai[i]) - T*STensorOperation::doubledot(DQiDT[i],dot_Gammai[i]);
            }
        }
        
        // TVE Mechanical Source Term derivatives
        if(stiff){
            
            // Initialise
            dWmdT = 0.;
            STensorOperation::zero(dWmdE);
            
            // Initialise vectors
            static std::vector<STensor3> DDQiDTT, dot_DGammaiDT; 
            static std::vector<STensor43> DQiDEe, DDQiDTDEe, dot_DGammaiDEe; 
            DDQiDTT.clear();
            dot_DGammaiDT.clear();
            DQiDEe.clear();
            DDQiDTDEe.clear();
            dot_DGammaiDEe.clear();
            STensor43 allZero43(0.);
            for (int i=0; i < _N; i++){
                DDQiDTT.push_back(allZero);
                dot_DGammaiDT.push_back(allZero);
                DQiDEe.push_back(allZero43);
                DDQiDTDEe.push_back(allZero43);
                dot_DGammaiDEe.push_back(allZero43);
            }
            
            for (int i=0; i<_Gi.size(); i++){
             
                STensorOperation::zero(DDQiDTT[i]);   
                STensorOperation::zero(dot_DGammaiDT[i]);   
                STensorOperation::zero(DQiDEe[i]);   
                STensorOperation::zero(DDQiDTDEe[i]);   
                STensorOperation::zero(dot_DGammaiDEe[i]);   
                for (int k=0; k<3; k++){
                    for (int l=0; l<3; l++){
                        DDQiDTT[i](k,l) = _Ki[i]*q1->_DDB_DTT[i]*_I(k,l) + 2*_Gi[i]*q1->_DDA_DTT[i](k,l); 
                        dot_DGammaiDT[i](k,l) = - 1/3*q1->_DB_DT[i]*_I(k,l) - q1->_DA_DT[i](k,l);
                        for (int r=0; r<3; r++){
                            for (int s=0; s<3; s++){
                                DQiDEe[i](k,l,r,s) = _Ki[i]*q1->_DB_DtrE[i]*_I(k,l)*_I(r,s);
                                DDQiDTDEe[i](k,l,r,s) = _Ki[i]*q1->_DDB_DTDtrE[i]*_I(k,l)*_I(r,s);
                                dot_DGammaiDEe[i](k,l,r,s) = 1/3*(_I(k,l)*_I(r,s) - q1->_DB_DtrE[i]*_I(k,l)*_I(r,s)) + _Idev(k,l,r,s);
                                for (int p=0; p<3; p++){
                                  for (int q=0; q<3; q++){
                                      DQiDEe[i](k,l,r,s) += 2*_Gi[i]*q1->_DA_DdevE[i](k,l,p,q)*_I4(p,q,r,s);
                                      DDQiDTDEe[i](k,l,r,s) += 2*_Gi[i]*q1->_DDA_DTDdevE[i](k,l,p,q)*_I4(p,q,r,s);
                                      dot_DGammaiDEe[i](k,l,r,s) -= q1->_DA_DdevE[i](k,l,p,q)*_I4(p,q,r,s);
                                  }
                                }
                            }
                        }
                    }
                }
                if (this->getTimeStep() > 0){
                    dot_DGammaiDT[i] *= 1/this->getTimeStep();
                    dot_DGammaiDEe[i] *= 1/this->getTimeStep();
                
                    dWmdT -= (STensorOperation::doubledot(Qi[i],dot_DGammaiDT[i]) - T*STensorOperation::doubledot(DQiDT[i],dot_DGammaiDT[i]) 
                            -  T*STensorOperation::doubledot(DDQiDTT[i],dot_Gammai[i]));
                        
                    for (int k=0; k<3; k++)
                        for (int l=0; l<3; l++)
                            for (int r=0; r<3; r++)
                                for (int s=0; s<3; s++){
                                    dWmdE(r,s) -= ((DQiDEe[i](k,l,r,s) - T*DDQiDTDEe[i](k,l,r,s))*dot_Gammai[i](k,l) + (Qi[i](k,l) - T*DQiDT[i](k,l))*dot_DGammaiDEe[i](k,l,r,s));
                                }
                }
            }
            
        }
    }
                                          
};

void mlawNonLinearTVM::getMechSource_nonLinearTVE_term(const IPNonLinearTVM *q0, IPNonLinearTVM *q1, const double& T0, const double& T, 
                                        double& Wm, const bool stiff, STensor3& dWmdE, double& dWmdT) const{
                                          
    // Initialize
    Wm = 0.;
    dWmdT = 0.;
    STensorOperation::zero(dWmdE);
    
    // Ee
    static STensor3 devEe, devEe0;
    static double trEe, trEe0;
    STensorOperation::decomposeDevTr(q0->_Ee,devEe0,trEe0);
    STensorOperation::decomposeDevTr(q1->_Ee,devEe,trEe);

    // DEe
    const STensor3& devDE = q1->getConstRefToDevDE();
    const double& trDE = q1->getConstRefToTrDE();
    
    // NEW
    double T_set = setTemp(T0,T,1);  // 1->T1, 2->T_mid, 3->T_midshift
    double AlphaT0, AlphaT, DAlphaDT;
    getAlpha(AlphaT,T_set,&DAlphaDT); getAlpha(AlphaT0,T0);
    double eff_trEe = trEe - 3.*(AlphaT*(T-_Tinitial));
    double eff_trEe0 = trEe0 - 3.*(AlphaT0*(T0-_Tinitial));
    double Deff_trEe_DT = -3.*(DAlphaDT*(T-_Tinitial) + AlphaT);
    double eff_trDE = q1->_trDE - 3.*(AlphaT*(T-_Tinitial) - AlphaT0*(T0-_Tinitial));
    double Deff_trDE_DT = -3.*(DAlphaDT*(T-_Tinitial) + AlphaT);
    // NEW
            
    // Initialize if needed
    // Get ShiftFactor and shifted time increment
    double dt = this->getTimeStep();
    double dt_shift_rec(0.), dt_shift_mid(0.);
    double Ddt_shiftDT_rec(0.), Ddt_shiftDT_mid(0.), DDdt_shiftDT_rec(0.), DDdt_shiftDT_mid(0.);
    shiftedTime(dt, T0, T, &dt_shift_rec,&Ddt_shiftDT_rec,&DDdt_shiftDT_rec,0);
    shiftedTime(dt, T0, T, &dt_shift_mid,&Ddt_shiftDT_mid,&DDdt_shiftDT_mid);
        
    double Dexp_rec_k_DT(0.), Dexp_mid_k_DT(0.);  
    double Dexp_rec_g_DT(0.), Dexp_mid_g_DT(0.);  
    double dtk(0.), dtk2(0.), exp_rec_k(0.), exp_mid_k(0.);
    double dtg(0.), dtg2(0.), exp_rec_g(0.), exp_mid_g(0.);

    // Add visco terms
    if ((_Ki.size() > 0) or (_Gi.size() > 0)){
        
        if (_ExtraBranch_TVE_option != 1){
        
            // TVE Mechanical Source Term
            
            // Initialise vectors
            static std::vector<STensor3> Qi, DQiDT, dot_Gammai; // Qi -> Viscous Stress Tensor, Gammai -> Viscous Strain Tensor
            static std::vector<STensor3> DDQiDTT, dot_DGammaiDT; 
            static std::vector<STensor43> DQiDEe, DDQiDTDEe, dot_DGammaiDEe; 
            Qi.clear();
            DQiDT.clear();
            dot_Gammai.clear();
            DDQiDTT.clear();
            dot_DGammaiDT.clear();
            DQiDEe.clear();
            DDQiDTDEe.clear();
            dot_DGammaiDEe.clear();
            STensor3 allZero(0.);
            STensor43 allZero43(0.);
            for (int i=0; i < _N; i++){
                Qi.push_back(allZero);
                DQiDT.push_back(allZero);
                dot_Gammai.push_back(allZero);
                DDQiDTT.push_back(allZero);
                dot_DGammaiDT.push_back(allZero);
                DQiDEe.push_back(allZero43);
                DDQiDTDEe.push_back(allZero43);
                dot_DGammaiDEe.push_back(allZero43);
            }
                
            for (int i=0; i<_Gi.size(); i++){
            
                STensorOperation::zero(Qi[i]);   
                STensorOperation::zero(DQiDT[i]);   
                STensorOperation::zero(dot_Gammai[i]);   
                for (int k=0; k<3; k++){
                    for (int l=0; l<3; l++){
                        Qi[i](k,l) = _Ki[i]*q1->_Av_TVE_vector[i]*q1->_B[i]*_I(k,l) + 2*_Gi[i]*q1->_Bd_TVE_vector[i]*q1->_A[i](k,l); 
                        DQiDT[i](k,l) = _Ki[i]*q1->_DB_DT[i]*_I(k,l) + 2*_Gi[i]*q1->_DA_DT[i](k,l); 
                        dot_Gammai[i](k,l) = 1/3*(eff_trEe - q1->_B[i] - eff_trEe0 + q0->_B[i])*_I(k,l) + (devEe(k,l)-q1->_A[i](k,l) - devEe0(k,l) + q0->_A[i](k,l) ); // NEW
                    }
                }
                if (this->getTimeStep() > 0){
                    dot_Gammai[i] *= 1/this->getTimeStep();
                    Wm += STensorOperation::doubledot(Qi[i],dot_Gammai[i]); //  - T*STensorOperation::doubledot(DQiDT[i],dot_Gammai[i]); // OLD
                }
        
                // TVE Mechanical Source Term derivatives
                if(stiff){

                    dtk = dt_shift_rec/(_ki[i]);
                    dtk2 = dt_shift_mid/(_ki[i]);
                    exp_rec_k = exp(-dtk);     // rec
                    exp_mid_k = exp(-dtk2); // mid
                
                    dtg = dt_shift_rec/(_gi[i]);
                    dtg2 = dt_shift_mid/(_gi[i]);
                    exp_rec_g = exp(-dtg);     // rec
                    exp_mid_g = exp(-dtg2); // mid
            
                    Dexp_rec_k_DT = -1/(_ki[i]) * Ddt_shiftDT_rec*exp_rec_k;
                    Dexp_mid_k_DT = -1/(_ki[i]) * Ddt_shiftDT_mid*exp_mid_k;
                    Dexp_rec_g_DT = -1/(_gi[i]) * Ddt_shiftDT_rec*exp_rec_g;
                    Dexp_mid_g_DT = -1/(_gi[i]) * Ddt_shiftDT_mid*exp_mid_g;
            
                    double dTrEei_DT(0.); // branchElasticStrain
                    static STensor3 dDevEei_DT;
                    dTrEei_DT = q0->_B[i] * Dexp_rec_k_DT + Dexp_mid_k_DT*eff_trDE + exp_mid_k*Deff_trDE_DT; // trDE; //NEW
                    for (int k=0; k<3; k++)
                        for (int l=0; l<3; l++)
                            dDevEei_DT(k,l) = q0->_A[i](k,l) * Dexp_rec_g_DT + Dexp_mid_g_DT*devDE(k,l);
             
                    STensorOperation::zero(DDQiDTT[i]);   
                    STensorOperation::zero(dot_DGammaiDT[i]);   
                    STensorOperation::zero(DQiDEe[i]);   
                    STensorOperation::zero(DDQiDTDEe[i]);   
                    STensorOperation::zero(dot_DGammaiDEe[i]);   
                    for (int k=0; k<3; k++){
                        for (int l=0; l<3; l++){
                            DDQiDTT[i](k,l) = _Ki[i]*q1->_DDB_DTT[i]*_I(k,l) + 2*_Gi[i]*q1->_DDA_DTT[i](k,l); 
                            dot_DGammaiDT[i](k,l) = 1/3*(Deff_trEe_DT-dTrEei_DT)*_I(k,l) - dDevEei_DT(k,l); // NEW
                            for (int r=0; r<3; r++){
                                for (int s=0; s<3; s++){
                                    DQiDEe[i](k,l,r,s) = _Ki[i]*q1->_DB_DtrE[i]*_I(k,l)*_I(r,s);
                                    DDQiDTDEe[i](k,l,r,s) = _Ki[i]*q1->_DDB_DTDtrE[i]*_I(k,l)*_I(r,s);
                                    dot_DGammaiDEe[i](k,l,r,s) = 1/3*(1.- exp_mid_k)*_I(k,l)*_I(r,s)  + _Idev(k,l,r,s);
                                    
                                    if (_ExtraBranch_TVE_option == 3 || _ExtraBranch_TVE_option == 4 || _ExtraBranch_TVE_option == 5){
                                        DQiDEe[i](k,l,r,s) +=  2*_Gi[i]*q1->_DA_DtrE[i](k,l)*_I(r,s);
                                        DDQiDTDEe[i](k,l,r,s) +=  2*_Gi[i]*q1->_DDA_DTDtrE[i](k,l)*_I(r,s);
                                    }
                                    
                                    for (int p=0; p<3; p++){
                                        for (int q=0; q<3; q++){
                                            DQiDEe[i](k,l,r,s) += 2*_Gi[i]*q1->_DA_DdevE[i](k,l,p,q)*_Idev(p,q,r,s); // _Idev is not pre-multiplied in this case
                                            DDQiDTDEe[i](k,l,r,s) += 2*_Gi[i]*q1->_DDA_DTDdevE[i](k,l,p,q)*_Idev(p,q,r,s); // _Idev is not pre-multiplied in this case
                                            dot_DGammaiDEe[i](k,l,r,s) -= exp_mid_g*_I4(k,l,p,q)*_Idev(p,q,r,s);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    if (this->getTimeStep() > 0){
                        dot_DGammaiDT[i] *= 1/this->getTimeStep();
                        dot_DGammaiDEe[i] *= 1/this->getTimeStep();
                        
                        
                        // dWmdT += (STensorOperation::doubledot(Qi[i],dot_DGammaiDT[i]) - T*STensorOperation::doubledot(DQiDT[i],dot_DGammaiDT[i]) 
                        //        -  T*STensorOperation::doubledot(DDQiDTT[i],dot_Gammai[i])); // OLD
                        
                        dWmdT += (STensorOperation::doubledot(DQiDT[i],dot_Gammai[i]) + STensorOperation::doubledot(Qi[i],dot_DGammaiDT[i]));
                        
                        for (int k=0; k<3; k++)
                            for (int l=0; l<3; l++)
                                for (int r=0; r<3; r++)
                                    for (int s=0; s<3; s++){
                                    // dWmdE(r,s) += ((DQiDEe[i](k,l,r,s) - T*DDQiDTDEe[i](k,l,r,s))*dot_Gammai[i](k,l) + (Qi[i](k,l) - T*DQiDT[i](k,l))*dot_DGammaiDEe[i](k,l,r,s)); // OLD
                                        dWmdE(r,s) += (DQiDEe[i](k,l,r,s)*dot_Gammai[i](k,l) + Qi[i](k,l)*dot_DGammaiDEe[i](k,l,r,s));
                                    }
                    }
                } // if stiff
            
             
            } // i
         
        }
    }
                                          
};

void mlawNonLinearTVM::getTVEdCorKirDT(const IPNonLinearTVM *q0, IPNonLinearTVM *q1, const double& T0, const double& T) const{
                                          
    // DcorKirDT in the IP
    STensor3& DcorKirDT = q1->getRefToDcorKirDT();
    STensor3& DDcorKirDT = q1->getRefToDDcorKirDTT();
    STensor43& DDcorKirDTDEe = q1->getRefToDDcorKirDTDE();
    STensorOperation::zero(DcorKirDT); 
    STensorOperation::zero(DDcorKirDT); 
    STensorOperation::zero(DDcorKirDTDEe); 
    
    // Ee
    static STensor3 devEe, devEe0;
    double trEe, trEe0;
    STensorOperation::decomposeDevTr(q0->_Ee,devEe0,trEe0);
    STensorOperation::decomposeDevTr(q1->_Ee,devEe,trEe);

    // DEe
    const STensor3& DEe = q1->getConstRefToDE();
    const STensor3& devDE = q1->getConstRefToDevDE();
    const double& trDE = q1->getConstRefToTrDE();

    // Update the Properties to the current temperature
    double KT, GT, AlphaT, DKDT, DGDT, DAlphaDT, DDKDT, DDGDT, DDAlphaDT;
    getK(q1,KT,T,&DKDT,&DDKDT); getG(q1,GT,T,&DGDT,&DDGDT); getAlpha(AlphaT,T,&DAlphaDT,&DDAlphaDT);
    
    // Add Infinity terms
    for (int i=0; i<3; i++){
        DcorKirDT(i,i) += DKDT*(trEe - 3*AlphaT*(T-_Tinitial))- 3*KT*(DAlphaDT*(T-_Tinitial) + AlphaT);
        DDcorKirDT(i,i) += DDKDT*(trEe - 3*AlphaT*(T-_Tinitial)) - 6*DKDT*(DAlphaDT*(T-_Tinitial) + AlphaT)- 3*KT*(DDAlphaDT*(T-_Tinitial) + 2*DAlphaDT);
        for (int j=0; j<3; j++){
            DcorKirDT(i,j) += 2*DGDT*devEe(i,j);
            DDcorKirDT(i,j) += 2*DDGDT*devEe(i,j);
            for (int k=0; k<3; k++)
                for (int l=0; l<3; l++)
                    DDcorKirDTDEe(i,j,k,l) += DKDT*_I(i,j)*_I(k,l) + 2*DGDT*_Idev(i,j,k,l);
        }
    }
    
    // Add hyperelastic extrabranch terms
    if (_useExtraBranch){
        double DsigV_dT(0.), DDsigV_dTT(0.), DDsigV_dTdTrEe(0.);
        static STensor3 sigExtra, DsigD_dT, DDsigD_dTT, DDsigD_dTdTrEe;
        static STensor43 DDsigD_dTdDevEe;
        STensorOperation::zero(sigExtra); STensorOperation::zero(DsigD_dT); STensorOperation::zero(DDsigD_dTT); 
        STensorOperation::zero(DDsigD_dTdDevEe);
        STensorOperation::zero(DDsigD_dTdTrEe); 
        
        static STensor3 eff_Ee;
        STensorOperation::zero(eff_Ee);
        eff_Ee = -1.*_I;
        eff_Ee *= 3.*AlphaT*(T-_Tinitial);
        eff_Ee += q1->_Ee;
        
        if (_extraBranchNLType != TensionCompressionRegularisedType && _extraBranchNLType != hyper_exp_TCasymm_Type){
            extraBranchLaw(eff_Ee,T,q0,q1,sigExtra,true,NULL,NULL,NULL,NULL,
                                            &DsigV_dT, &DsigD_dT, &DDsigV_dTT, &DDsigD_dTT, &DDsigV_dTdTrEe, &DDsigD_dTdDevEe);
        }
        else{
            extraBranchLaw(eff_Ee,T,q0,q1,sigExtra,true,NULL,NULL,NULL,NULL,
                                            &DsigV_dT, &DsigD_dT, &DDsigV_dTT, &DDsigD_dTT, &DDsigV_dTdTrEe, &DDsigD_dTdDevEe, NULL, &DDsigD_dTdTrEe);
        }
        DcorKirDT += DsigV_dT*_I;
        DcorKirDT += DsigD_dT;
        DDcorKirDT += DDsigV_dTT*_I;
        DDcorKirDT += DDsigD_dTT;
        for (int i=0; i<3; i++)
            for (int j=0; j<3; j++)
                for (int k=0; k<3; k++)
                    for (int l=0; l<3; l++){
                        DDcorKirDTDEe(i,j,k,l) += DDsigV_dTdTrEe*_I(i,j)*_I(k,l);
                        
                        if (_extraBranchNLType == TensionCompressionRegularisedType || _extraBranchNLType == hyper_exp_TCasymm_Type){
                            DDcorKirDTDEe(i,j,k,l) += DDsigD_dTdTrEe(i,j)*_I(k,l);
                        }
                        for (int r=0; r<3; r++)
                            for (int s=0; s<3; s++){
                                DDcorKirDTDEe(i,j,k,l) += DDsigD_dTdDevEe(i,j,r,s)*_Idev(r,s,k,l);
                            }
                    }
    }
    
    // Add visco terms
    if ((_Ki.size() > 0) or (_Gi.size() > 0)){
        for (int i=0; i<_Gi.size(); i++){
            for (int j=0; j<3; j++)
                for (int k=0; k<3; k++){
                    DcorKirDT(j,k) += _Ki[i]*q1->_DB_DT[i]*_I(j,k) + 2*_Gi[i]*q1->_DA_DT[i](j,k);
                    DDcorKirDT(j,k) += _Ki[i]*q1->_DDB_DTT[i]*_I(j,k) + 2*_Gi[i]*q1->_DDA_DTT[i](j,k);
                    for (int r=0; r<3; r++)
                        for (int s=0; s<3; s++){
                            if (!_useExtraBranch_TVE){ // _Idev is multiplied to _DDA_DTDdevE
                                DDcorKirDTDEe(j,k,r,s) += _Ki[i]*q1->_DDB_DTDtrE[i]*_I(j,k)*_I(r,s) + 2*_Gi[i]*q1->_DDA_DTDdevE[i](j,k,r,s);
                            }
                            else{ // all extraBranches, because _Idev was not multiplied to _DDA_DTDdevE in the predictorCorrector function
                                DDcorKirDTDEe(j,k,r,s) += _Ki[i]*q1->_DDB_DTDtrE[i]*_I(j,k)*_I(r,s);
                                
                                if (_ExtraBranch_TVE_option == 3 || _ExtraBranch_TVE_option == 4 || _ExtraBranch_TVE_option == 5){
                                    DDcorKirDTDEe(j,k,r,s) += 2*_Gi[i]*q1->_DDA_DTDtrE[i](j,k)*_I(r,s);
                                }
                                
                                for (int p=0; p<3; p++)
                                    for (int q=0; q<3; q++){
                                        DDcorKirDTDEe(j,k,r,s) += 2*_Gi[i]*q1->_DDA_DTDdevE[i](j,k,p,q)*_Idev(p,q,r,s);
                                    }
                            }
                        }
                }
        }
    }
    
}

void mlawNonLinearTVM::constitutive(const STensor3& F0,
                                    const STensor3& F,
                                    STensor3& P,
                                    const IPVariable *q0i,
                                    IPVariable *q1i,
                                    STensor43& Tangent,
                                    const bool stiff,
                                    STensor43* elasticTangent,
                                    const bool dTangent,
                                    STensor63* dCalgdeps) const{

  const IPNonLinearTVM *q0=dynamic_cast<const IPNonLinearTVM *>(q0i);
  IPNonLinearTVM *q1 = dynamic_cast<IPNonLinearTVM *>(q1i);

  double Tc = _Tref;
  static SVector3 gradT, temp2;
  static STensor3 temp3;
  static STensor33 temp33;
  static double tempVal;
  static STensor43 dFpdF, dFedF;
  static STensor3 dFpdT, dFedT, dGammadF;
  double dGammadT;

  if(elasticTangent!=NULL) Msg::Error("mlawNonLinearTVM elasticTangent not defined");
  predictorCorrector_ThermoViscoElastic(F0,F,P,q0,q1,Tangent,dFedF,dFedT,_Tref,_Tref,gradT,gradT,temp2,temp3,temp3,temp2,temp33,tempVal,
                      tempVal,temp3,tempVal,tempVal,temp3,stiff);
};

void mlawNonLinearTVM::constitutive(
                            const STensor3& F0,                             // initial deformation gradient (input @ time n)
                            const STensor3& F,                              // updated deformation gradient (input @ time n+1)
                                  STensor3& P,                              // updated 1st Piola-Kirchhoff stress tensor (output)
                            const IPVariable *q0i,                          // array of initial internal variable
                                  IPVariable *q1i,                          // updated array of internal variable (in ipvcur on output),
                                  STensor43& Tangent,                       // mechanical tangents (output)
                            const double& T0,                               // previous temperature
                            const double& T,                                // temperature
                            const SVector3& gradT0,                         // previous temperature gradient
                            const SVector3& gradT,                          // temperature gradient
                                  SVector3& fluxT,                          // temperature flux
                                  STensor3& dPdT,                           // mechanical-thermal coupling
                                  STensor3& dfluxTdgradT,                   // thermal tengent
                                  SVector3& dfluxTdT,
                                  STensor33& dfluxTdF,                      // thermal-mechanical coupling
                                  double& thermalSource,                    // - Cp*DTdt
                                  double& dthermalSourcedT,                 // thermal source
                                  STensor3& dthermalSourcedF,
                                  double& mechanicalSource,                 // mechanical source --> convert to heat
                                  double& dmechanicalSourcedT,
                                  STensor3& dmechanicalSourceF,
                            const bool stiff,
                                  STensor43* elasticTangent) const{

  const IPNonLinearTVM *q0 = dynamic_cast<const IPNonLinearTVM *>(q0i);
        IPNonLinearTVM *q1 = dynamic_cast<IPNonLinearTVM *>(q1i);
  if(elasticTangent!=NULL) Msg::Error("mlawNonLinearTVM elasticTangent not defined");

  if (!_tangentByPerturbation){
    static STensor43 dFedF;
    static STensor3 dFedT;
    STensorOperation::unity(dFedF);
    STensorOperation::zero(dFedT);
    predictorCorrector_ThermoViscoElastic(F0,F,P,q0,q1,Tangent,dFedF,dFedT,T0,T,gradT0,gradT,fluxT,dPdT,dfluxTdgradT,dfluxTdT,dfluxTdF,
                      thermalSource,dthermalSourcedT,dthermalSourcedF,
                      mechanicalSource,dmechanicalSourcedT,dmechanicalSourceF,stiff);
  }
   else{
    predictorCorrector_ThermoViscoElastic(F0,F,P,q0,q1,T0,T,gradT0,gradT,fluxT,thermalSource,mechanicalSource);

    static STensor3 Fplus, Pplus;
    static SVector3 fluxTPlus, gradTplus;
    static double thermalSourcePlus;
    static double mechanicalSourcePlus;
    static IPNonLinearTVM qPlus(*q0);

    // perturb F
    for (int i=0; i<3; i++){
      for (int j=0; j<3; j++){
        Fplus = (F);
        Fplus(i,j) += _perturbationfactor;
        predictorCorrector_ThermoViscoElastic(F0,Fplus,Pplus,q0,&qPlus,T0,T,gradT0,gradT,fluxTPlus,thermalSourcePlus,mechanicalSourcePlus);
        for (int k=0; k<3; k++){
          for (int l=0; l<3; l++){
            Tangent(k,l,i,j) = (Pplus(k,l) - P(k,l))/(_perturbationfactor);
          }
          dfluxTdF(k,i,j) = (fluxTPlus(k) - fluxT(k))/_perturbationfactor;
        }
        dthermalSourcedF(i,j) = (thermalSourcePlus - thermalSource)/_perturbationfactor;
        dmechanicalSourceF(i,j) = (mechanicalSourcePlus - mechanicalSource)/_perturbationfactor;
      }
    }

    // perturb gradT
    double gradTpert = _perturbationfactor*T0/1e-3;
    for (int i=0; i<3; i++){
      gradTplus = (gradT);
      gradTplus(i) += (gradTpert);
      predictorCorrector_ThermoViscoElastic(F0,F,Pplus,q0,&qPlus,T0,T,gradT0,gradTplus,fluxTPlus,thermalSourcePlus,mechanicalSourcePlus);
      for (int k=0; k<3; k++){
        dfluxTdgradT(k,i) = (fluxTPlus(k) - fluxT(k))/gradTpert;
      }
    }

    // perturb on T
    double Tplus = T+ _perturbationfactor*T0;
    predictorCorrector_ThermoViscoElastic(F0,F,Pplus,q0,&qPlus,T0,Tplus,gradT0,gradT,fluxTPlus,thermalSourcePlus,mechanicalSourcePlus);
    for (int k=0; k<3; k++){
      for (int l=0; l<3; l++){
        dPdT(k,l) = (Pplus(k,l) - P(k,l))/(_perturbationfactor*T0);
      }
      dfluxTdT(k) = (fluxTPlus(k) - fluxT(k))/(_perturbationfactor*T0);
    }

    dthermalSourcedT = (thermalSourcePlus - thermalSource)/(_perturbationfactor*T0);
    dmechanicalSourcedT = (mechanicalSourcePlus - mechanicalSource)/(_perturbationfactor*T0);
   }
};

void mlawNonLinearTVM::predictorCorrector_ThermoViscoElastic(
                                        const STensor3& F0,                             // initial deformation gradient (input @ time n)
                                        const STensor3& F,                              // updated deformation gradient (input @ time n+1)
                                              STensor3& P,                              // updated 1st Piola-Kirchhoff stress tensor (output)
                                        const IPNonLinearTVM *q0,                       // array of initial internal variables
                                              IPNonLinearTVM *q1,                       // updated array of internal variables (in ipvcur on output),
                                        const double& T0,                               // previous temperature
                                        const double& T,                                // temperature
                                        const SVector3& gradT0,                         // previous temperature gradient
                                        const SVector3& gradT,                          // temperature gradient
                                              SVector3& fluxT,                          // temperature flux
                                              double& thermalSource,
                                              double& mechanicalSource) const{
  // temp variables
  static STensor43 Tangent, dFedF;
  static STensor3 dPdT, dfluxTdgradT, dthermalSourcedF, dmechanicalSourceF, dFedT;
  static STensor33 dfluxTdF;
  static SVector3 dfluxTdT;
  static double dthermalSourcedT, dmechanicalSourcedT;
  predictorCorrector_ThermoViscoElastic(F0,F,P,q0,q1,Tangent,dFedF,dFedT,T0,T,gradT0,gradT,fluxT,dPdT,dfluxTdgradT,dfluxTdT,dfluxTdF,
                      thermalSource,dthermalSourcedT,dthermalSourcedF,
                      mechanicalSource,dmechanicalSourcedT,dmechanicalSourceF,false);
}
void mlawNonLinearTVM::predictorCorrector_ThermoViscoElastic(
                                        const STensor3& F0,                             // initial deformation gradient (input @ time n)
                                        const STensor3& F,                              // updated deformation gradient (input @ time n+1)
                                              STensor3& P,                              // updated 1st Piola-Kirchhoff stress tensor (output)
                                        const IPNonLinearTVM *q0,                       // array of initial internal variables
                                              IPNonLinearTVM *q1,                       // updated array of internal variableS (in ipvcur on output),
                                              STensor43& Tangent,                       // mechanical tangents (output)
                                              STensor43& dFedF,                         // elastic tangent
                                              STensor3& dFedT,                          // elastic tangent
                                        const double& T0,                               // previous temperature
                                        const double& T,                                // temperature
                                        const SVector3& gradT0,                         // previous temperature gradient
                                        const SVector3& gradT,                          // temperature gradient
                                              SVector3& fluxT,                          // temperature flux
                                              STensor3& dPdT,                           // mechanical-thermal coupling
                                              STensor3& dfluxTdgradT,                   // thermal tengent
                                              SVector3& dfluxTdT,
                                              STensor33& dfluxTdF,                      // thermal-mechanical coupling
                                              double& thermalSource,                    // - Cp*dTdt
                                              double& dthermalSourcedT,                 // thermal source
                                              STensor3& dthermalSourcedF,
                                              double& mechanicalSource,                 // mechanical source --> convert to heat
                                              double& dmechanicalSourcedT,
                                              STensor3& dmechanicalSourceF,
                                        const bool stiff) const{

   
    // Get time step
    double dt = this->getTimeStep();
    double T_set = setTemp(T0,T,1);  // 1->T1, 2->T_mid, 3->T_midshift

    // Make Identity Tensors
    // static const STensor3 I3(1.);
    // static const STensor43 I4(1.,1.);

    // Update the Properties to the current temperature (see below which ones are being used)         
    double KT, GT, AlphaT, KThConT, CpT, DKDT, DGDT, DAlphaDT, DKThConDT, DCpDT, DDKDT, DDGDT, DDAlphaDT;
    getK(q1,KT,T_set,&DKDT,&DDKDT); getG(q1,GT,T_set,&DGDT,&DDGDT); getAlpha(AlphaT,T_set,&DAlphaDT,&DDAlphaDT); getKTHCon(KThConT,T_set,&DKThConDT); getCp(CpT,T_set,&DCpDT); 
    
    // Calculate/update Log Strain
    static STensor3 C;
    static STensor43 dlnCdC;
    static STensor63 ddlnCddC;

    STensor3& E = q1->getRefToElasticStrain();

    STensorOperation::multSTensor3FirstTranspose(F,F,C);
    if (_order == 1){
        bool ok=STensorOperation::logSTensor3(C,_order,E,&dlnCdC);  // as ddlogCddC = 0
        if(!ok)
        {
          P(0,0) = P(1,1) = P(2,2) = sqrt(-1.);
          return; 
        }
    }
    else{
        bool ok=STensorOperation::logSTensor3(C,_order,E,&dlnCdC,&ddlnCddC);
        if(!ok)
        {
          P(0,0) = P(1,1) = P(2,2) = sqrt(-1.);
          return; 
        }
    }
    // STensorOperation::logSTensor3(C,_order,E,&dlnCdC,&ddlnCddC);
    E *= 0.5; // strain
    
    // Stresses and Effective Moduli
    double Ke(0.), Ge(0.);
    static STensor43 Bd_stiffnessTerm, Bd_stiffnessTerm2;
    STensorOperation::zero(Bd_stiffnessTerm);
    STensorOperation::zero(Bd_stiffnessTerm2);


    if(_extraBranchNLType == TensionCompressionRegularisedType || _extraBranchNLType == hyper_exp_TCasymm_Type || _ExtraBranch_TVE_option == 3 || _ExtraBranch_TVE_option == 4 || _ExtraBranch_TVE_option == 5){
        ThermoViscoElasticPredictor(E,q0->_Ee,q0,q1,Ke,Ge,T0,T,stiff,Bd_stiffnessTerm,&Bd_stiffnessTerm2);
                // Updates the values of moduli to the current temperatures and calculate stresses.
    }
    else{
        ThermoViscoElasticPredictor(E,q0->_Ee,q0,q1,Ke,Ge,T0,T,stiff,Bd_stiffnessTerm);
    }
    

    static fullMatrix<double> m1(3, 3), m2(3, 3);
    static fullVector<double> eigenValReal1(3), eigenValReal2(3);
    static fullVector<double> eigenValImag1(3), eigenValImag2(3);
    static fullMatrix<double> leftEigenVect1(3,3), leftEigenVect2(3,3);
    static fullMatrix<double> rightEigenVect1(3,3), rightEigenVect2(3,3);
    m1.setAll(0.); m2.setAll(0.);
    eigenValReal1.setAll(0.); eigenValReal2.setAll(0.);
    eigenValImag1.setAll(0.); eigenValImag2.setAll(0.);
    leftEigenVect1.setAll(0.); leftEigenVect2.setAll(0.);
    rightEigenVect1.setAll(0.); rightEigenVect2.setAll(0.);

    // Get eigen values and vectors
    q1->_kirchhoff.getMat(m1); C.getMat(m2);
    m1.eig(eigenValReal1,eigenValImag1,leftEigenVect1,rightEigenVect1,false);
    m2.eig(eigenValReal2,eigenValImag2,leftEigenVect2,rightEigenVect2,false);

    double eig1, eig2, eig3, eig10,eig20,eig30;
    static STensor3 E1,E2,E3,E10,E20,E30;
    STensorOperation::getEigenDecomposition(q1->_kirchhoff,eig1,eig2,eig3,E1,E2,E3);
    STensorOperation::getEigenDecomposition(C,eig10,eig20,eig30,E10,E20,E30);
    
    const STensor3& corKir = q1->getConstRefToCorotationalKirchhoffStress();
    static STensor3 devT;
    double pT(0.);  
    STensorOperation::decomposeDevTr(corKir,devT,pT);
    q1->_pressure = pT/3.;
    
    static STensor3 secondPK;
    STensorOperation::multSTensor3STensor43(q1->_kirchhoff,dlnCdC,secondPK);
    STensorOperation::multSTensor3(F,secondPK,P);                  // 1st PK
    
    static STensor3 M;
    STensorOperation::multSTensor3(C,secondPK,M);
    q1->_ModMandel = M;

    // commuteChecker -> for commuting tensors Ce and Se, Me has to be symmetric ->
    // 			For two cases, both in the case of viscoplasticity if Een and Ee dont share eigenvectors: 1. Due to plasticity (Rp)  2. Due to non-proportional loading
    static STensor3 commuteChecker, MT;
    STensorOperation::transposeSTensor3(M,MT);
    commuteChecker = M;
    commuteChecker -= MT;
    q1->_mandelCommuteChecker = commuteChecker.norm0();
    if(q1->_mandelCommuteChecker > 1.e-6){
    	// Msg::Error("Mandel does not commute in mlawNonLinearTVM, norm = %e, tol = %e !!",q1->_mandelCommuteChecker,1.e-6);
    }

    // elastic energy and derivatives
    double& Dpsi_DT = q1->getRefTo_Dpsi_DT();
    STensor3& Dpsi_DE = q1->getRefTo_Dpsi_DE();
    q1->_elasticEnergy = freeEnergyMechanical(*q0,*q1,T0,T,&Dpsi_DT,&Dpsi_DE); 
    
    // Mullins Effect Scaler 
    if (_mullinsEffect != NULL && q1->_ipMullinsEffect != NULL){
        mlawNonLinearTVM::calculateMullinsEffectScaler(q0, q1, T, &Dpsi_DT);
    }
    double eta_mullins = 1.;
    if (q1->_ipMullinsEffect != NULL){
        
        // Put Stress in IP
        q1->_P_cap = P;
        
        // Modify Stress for mullins
        eta_mullins = q1->_ipMullinsEffect->getEta(); 
        P *= eta_mullins;
        // q1->_elasticEnergy *= eta_mullins;
    }
    // Msg::Error(" Inside TVE predCorr, after updating q1->_elasticEnergy = %e!!",q1->_elasticEnergy);
    
    // thermal energy
    q1->_thermalEnergy = CpT*T;

    // fluxT
    double J  = 1.;
    STensor3 Finv(0.);
    if (_thermalEstimationPreviousConfig){                                            // ADD  _thermalEstimationPreviousConfig -- WHAT?
        STensorOperation::inverseSTensor3(F0,Finv);
        J = STensorOperation::determinantSTensor3(F0);
    }
    else{
        STensorOperation::inverseSTensor3(F,Finv);
        J = STensorOperation::determinantSTensor3(F);
    }
    static STensor3 Cinv;
    STensorOperation::multSTensor3SecondTranspose(Finv,Finv,Cinv);
    STensorOperation::multSTensor3SVector3(Cinv,gradT,fluxT);
    fluxT *= (-KThConT*J);

    // thermalSource
    if (this->getTimeStep() > 0.){
        thermalSource = -CpT*(T-T0)/this->getTimeStep();
    }
    else
        thermalSource = 0.;
    
    // CorKir derivatives wrt T    
    getTVEdCorKirDT(q0,q1,T0,T);
    const STensor3& DcorKirDT = q1->getRefToDcorKirDT();
    const STensor3& DDcorKirDT = q1->getRefToDDcorKirDTT(); // for mech source derivatives
    const STensor43& DDcorKirDTDEe = q1->getRefToDDcorKirDTDE();
    const STensor3& DEe = q1->getConstRefToDE();
    
    // mechanicalSource
    mechanicalSource = 0.;
    double mechanicalSource_cap = 0.; // in case of mullins, undamaged mechanicalSource

    // 1) ThermoElastic Heat
    if (this->getTimeStep() > 0){
        mechanicalSource += (STensorOperation::doubledot(DcorKirDT,DEe)*T/this->getTimeStep());
    }
    // 2) Viscous Contribution to mechSrc
    double Wm_TVE(0.), dWmdT_TVE(0.);
    static STensor3 dWmdE_TVE;
    STensorOperation::zero(dWmdE_TVE);
    if (!_useExtraBranch_TVE){
        getMechSource_TVE_term(q0,q1,T0,T,Wm_TVE,stiff,dWmdE_TVE,dWmdT_TVE);
        q1->_mechSrcTVE = Wm_TVE;
    }
    else{
        getMechSource_nonLinearTVE_term(q0,q1,T0,T,Wm_TVE,stiff,dWmdE_TVE,dWmdT_TVE);
        Wm_TVE *= 1.;
        dWmdE_TVE *= 1.;
        dWmdT_TVE *= 1.;
        q1->_mechSrcTVE = Wm_TVE;
    }
    // mechanicalSource += Wm_TVE; // TVE term
    
    // Compute mechanicalSource for Mullin's Effect
    if (_mullinsEffect != NULL && q1->_ipMullinsEffect != NULL){
    	mechanicalSource_cap = mechanicalSource;
        mechanicalSource *= q1->_ipMullinsEffect->getEta();
        if (this->getTimeStep() > 0){
            mechanicalSource -= q1->_ipMullinsEffect->getDpsiNew_DpsiMax()*(q1->_ipMullinsEffect->getpsiMax() - q0->_ipMullinsEffect->getpsiMax())/this->getTimeStep();
        }
    }
    
    // calculate viscous dissipated energy -> only in IP
    q1->_viscousDissipatedEnergy = q0->_viscousDissipatedEnergy + 0.5*dt*(q1->_mechSrcTVE + q0->_mechSrcTVE);

    if (stiff){
     
        // Compute Mechanical Tangents - dSdC and dPdF 
    
        static STensor43 DcorKDE;
        isotropicHookTensor(Ke,Ge,DcorKDE);
        // DcorKDE += DsigExtraDE; // NEW
        if (_useExtraBranch || _useExtraBranch_TVE){
            for (int i=0; i<3; i++)
                for (int j=0; j<3; j++)
                    for (int k=0; k<3; k++)
                        for (int l=0; l<3; l++)
                            for (int p=0; p<3; p++)
                                for (int q=0; q<3; q++){
                                    DcorKDE(i,j,k,l) += Bd_stiffnessTerm(i,j,p,q)*_Idev(p,q,k,l);
                                }
           DcorKDE += Bd_stiffnessTerm2; // for _ExtraBranch_TVE_option == 3 || _extraBranchNLType = TensionCompression
        }
        
        
        static STensor43 DCeDFe;
        static STensor43 DEeDFe;
        STensorOperation::zero(DEeDFe);
        STensorOperation::unity(dFedF);
        for (int i=0; i<3; i++){
         for (int j=0; j<3; j++){
          for (int k=0; k<3; k++){
            for (int l=0; l<3; l++){
                DCeDFe(i,j,k,l) = 0.;
              for (int p=0; p<3; p++){
                DCeDFe(i,j,k,l) += 2*F(p,i)*dFedF(p,j,k,l);
              }
            }
          }
         }
        }
        STensorOperation::multSTensor43(dlnCdC,DCeDFe,DEeDFe);
        DEeDFe *= 0.5;
        
        static STensor43 DsecondPKdC;
        for (int i=0; i<3; i++){
            for (int j=0; j<3; j++){
                for (int k=0; k<3; k++){
                    for (int l=0; l<3; l++){
                        DsecondPKdC(i,j,k,l) = 0.;
                            for (int p=0; p<3; p++){
                                for (int q=0; q<3; q++){
                                    if (_order != 1){
                                        DsecondPKdC(i,j,k,l) += corKir(p,q)*ddlnCddC(p,q,i,j,k,l);
                                    }
                                    for (int r=0; r<3; r++){
                                        for (int s=0; s<3; s++){
                                            DsecondPKdC(i,j,k,l) += 0.5*DcorKDE(p,q,r,s)*dlnCdC(p,q,i,j)*dlnCdC(r,s,k,l);
                                        }
                                    }
                                }
                            }
                    }
                }
            }
        }

        STensorOperation::zero(Tangent);
        for (int i=0; i<3; i++){
            for (int j=0; j<3; j++){
                for (int p=0; p<3; p++){
                    Tangent(i,j,i,p) += secondPK(p,j);
                    for (int q=0; q<3; q++){
                        for (int s=0; s<3; s++){
                            for (int k=0; k<3; k++){
                                Tangent(i,j,p,q) += 2.*F(i,k)*DsecondPKdC(k,j,q,s)*F(p,s);
                            }
                        }
                    }
                }
            }
        }
        
        // Compute Mechano-thermal Tangents - dSdT and dPdT
        static STensor3 DSDT;
        STensorOperation::zero(DSDT);
        for (int i=0; i<3; i++){
            for (int j=0; j<3; j++){
                for (int r=0; r<3; r++){
                    for (int s=0; s<3; s++){
                        DSDT(i,j) += DcorKirDT(r,s)*dlnCdC(r,s,i,j);
                    }
                }
            }
        }

        STensorOperation::zero(dPdT);
        STensorOperation::multSTensor3(F,DSDT,dPdT); 

        // fluxT  derivatives
        dfluxTdT = fluxT;
        dfluxTdT *= (DKThConDT/KThConT);
        dfluxTdgradT = Cinv;
        dfluxTdgradT *= (-KThConT*J);
        STensorOperation::zero(dfluxTdF);

        if (!_thermalEstimationPreviousConfig){
            static const STensor43 I4(1.,1.);
            static STensor3 DJDF;
            static STensor43 DCinvDF;
            for (int i=0; i<3; i++){
                for (int j=0; j<3; j++){
                    DJDF(i,j) = J*Finv(j,i);
                        for (int k=0; k<3; k++){
                            for (int l=0; l<3; l++){
                                DCinvDF(i,j,k,l) = 0.;
                                for (int p=0; p<3; p++){
                                    for (int a=0; a<3; a++){
                                        for (int b=0; b<3; b++){
                                            DCinvDF(i,j,k,l) -= 2.*F(k,p)*Cinv(i,a)*Cinv(j,b)*I4(a,b,p,l);
                                        }
                                    }
                                }
                            }
                        }
                }
            }

            for (int i=0; i<3; i++){
                for (int j=0; j<3; j++){
                    for (int k=0; k<3; k++){
                        dfluxTdF(i,j,k) += (DJDF(j,k)*fluxT(i)/J);
                        for (int l=0; l<3; l++){
                            dfluxTdF(i,j,k) -= (J*DCinvDF(i,l,j,k)*gradT(l)*KThConT);
                        }
                    }
                }
            }
        }

        // thermal source derivatives
        static STensor3 DCpDF;
        STensorOperation::zero(DCpDF);                                                       // CHANGE for DCpDF 
        if (this->getTimeStep() > 0){
            dthermalSourcedT = -CpT/this->getTimeStep() - DCpDT*(T-T0)/this->getTimeStep();       
            for(int i = 0; i< 3; i++){
                for(int j = 0; j< 3; j++){
                    dthermalSourcedF(i,j) = -DCpDF(i,j)*(T-T0)/this->getTimeStep();          // --- See below for perturbation implementation
                }
            }
        }
        else{
            dthermalSourcedT = 0.;
            STensorOperation::zero(dthermalSourcedF); 
        }
                                    

        // mechanical source derivatives                                                 --- CHANGE for TVE terms
        static STensor3 dmechanicalSourceE;
        STensorOperation::zero(dmechanicalSourceE);
        dmechanicalSourcedT = 0;
        STensorOperation::zero(dmechanicalSourceF);
        
        // dmechanicalSourceE -> DcorKirDT terms
        static const STensor43 I4_2(1.,1.);
        static STensor3 DcorKirDT_I;
        STensorOperation::multSTensor3STensor43(DcorKirDT,I4_2,DcorKirDT_I);

        if (this->getTimeStep() > 0){
            for (int i=0; i<3; i++){
                for (int j=0; j<3; j++){
                    dmechanicalSourceE(i,j) += T*DcorKirDT_I(i,j)/this->getTimeStep();
                    for (int k=0; k<3; k++)
                        for (int l=0; l<3; l++){
                            dmechanicalSourceE(i,j) += T*DDcorKirDTDEe(k,l,i,j)*DEe(k,l)/this->getTimeStep();
                        }
                }
            }
            dmechanicalSourceE += dWmdE_TVE; // TVE term
            STensorOperation::multSTensor3STensor43(dmechanicalSourceE,DEeDFe,dmechanicalSourceF);
        
            dmechanicalSourcedT += (STensorOperation::doubledot(DcorKirDT,DEe)/this->getTimeStep());
            dmechanicalSourcedT += T*(STensorOperation::doubledot(DDcorKirDT,DEe)/this->getTimeStep());
            dmechanicalSourcedT += dWmdT_TVE; // TVE term
        }
        
        // Compute Tangents for Mullin's Effect
        if (_mullinsEffect != NULL && q1->_ipMullinsEffect != NULL){
            
            // 1st Term
            Tangent *= q1->_ipMullinsEffect->getEta();
            dPdT *= q1->_ipMullinsEffect->getEta();
            dmechanicalSourceF *= q1->_ipMullinsEffect->getEta();
            dmechanicalSourcedT *= q1->_ipMullinsEffect->getEta();
            
            // 2nd Term
            static STensor3 Deta_mullins_DF;
            STensorOperation::multSTensor3STensor43(q1->_DpsiDE,DEeDFe,Deta_mullins_DF);
            Deta_mullins_DF *= q1->_DmullinsDamage_Dpsi_cap;

            double Deta_mullins_DT(0.);
            Deta_mullins_DT = q1->_DmullinsDamage_DT; // + q1->_DmullinsDamage_Dpsi_cap*q1->_DpsiDT;

            for (int i=0; i<3; i++)
                for (int j=0; j<3; j++){
                    dPdT(i,j) += q1->_P_cap(i,j) * Deta_mullins_DT;
                    dmechanicalSourceF(i,j) += mechanicalSource_cap*Deta_mullins_DF(i,j);
                        for (int k=0; k<3; k++)
                            for (int l=0; l<3; l++)
                                Tangent(i,j,k,l) += q1->_P_cap(i,j) * Deta_mullins_DF(k,l);
                }
                
            static STensor3 DDpsiNew_DpsiMaxDF, DpsiMax_DF;
            STensorOperation::multSTensor3STensor43(q1->_DpsiDE,DEeDFe,DDpsiNew_DpsiMaxDF);
            STensorOperation::multSTensor3STensor43(q1->_DpsiDE,DEeDFe,DpsiMax_DF);
            DDpsiNew_DpsiMaxDF *= q1->_ipMullinsEffect->getDDpsiNew_DpsiMaxDpsi();
            DpsiMax_DF *= q1->_ipMullinsEffect->getDpsiMax_Dpsi();
            
            dmechanicalSourcedT += mechanicalSource_cap*Deta_mullins_DT; // q1->_DmullinsDamage_DT;
            if (this->getTimeStep() > 0){
                dmechanicalSourcedT -= (q1->_ipMullinsEffect->getDDpsiNew_DpsiMaxDT()*(q1->_ipMullinsEffect->getpsiMax() - q0->_ipMullinsEffect->getpsiMax())/this->getTimeStep()
                							+ q1->_ipMullinsEffect->getDpsiNew_DpsiMax()*q1->_ipMullinsEffect->getDpsiMax_Dpsi()*q1->_DpsiDT/this->getTimeStep());
                for (int i=0; i<3; i++)
                    for (int j=0; j<3; j++){
                        dmechanicalSourceF(i,j) -= DDpsiNew_DpsiMaxDF(i,j)*(q1->_ipMullinsEffect->getpsiMax() - q0->_ipMullinsEffect->getpsiMax())/this->getTimeStep();
                        dmechanicalSourceF(i,j) -= q1->_ipMullinsEffect->getDpsiNew_DpsiMax()*DpsiMax_DF(i,j)/this->getTimeStep();
                    }
            }

        }
    }
};

void mlawNonLinearTVM::getEe0s(const STensor3& Ce, const STensor3& Ee, const STensor3& Ee0, STensor3& Ee0s, STensor43& dEe0sdEe) const{

	// Ee0 becomes Ee0_star expressed in the same configuration as Ee

	double e01,e02,e03, e1,e2,e3, c1,c2,c3; // eigenvalues
	static STensor3 E01,E02,E03,E1,E2,E3,C1,C2,C3; // bases
	static STensor3 dc1da,dc2da,dc3da; // eigenvalue derivatives
	static STensor43 dC1da,dC2da,dC3da; // bases derivatives

	STensorOperation::getEigenDecomposition(Ee0,e01,e02,e03,E01,E02,E03);
	STensorOperation::getEigenDecomposition(Ee,e1,e2,e3,E1,E2,E3);
	STensorOperation::getEigenDecomposition(Ce,c1,c2,c3,C1,C2,C3,dc1da,dc2da,dc3da,dC1da,dC2da,dC3da);

	// #####
	// Align the eigenVals of Ee0 with Ee
	static fullMatrix<double> m1(3, 3), m2(3, 3);
	static fullVector<double> eigenValReal1(3), eigenValReal2(3), AlignedEigenValReal1(3);
	static fullVector<double> eigenValImag1(3), eigenValImag2(3);
	static fullMatrix<double> leftEigenVect1(3,3), leftEigenVect2(3,3);
	static fullMatrix<double> rightEigenVect1(3,3), rightEigenVect2(3,3), AlignedRightEigenVect1(3,3);
	m1.setAll(0.); m2.setAll(0.);
	eigenValReal1.setAll(0.); eigenValReal2.setAll(0.); AlignedEigenValReal1.setAll(0.);
	eigenValImag1.setAll(0.); eigenValImag2.setAll(0.);
	leftEigenVect1.setAll(0.); leftEigenVect2.setAll(0.);
	rightEigenVect1.setAll(0.); rightEigenVect2.setAll(0.); AlignedRightEigenVect1.setAll(0.);

	// Get eigen values and vectors
	Ee0.getMat(m1); Ce.getMat(m2);
	m1.eig(eigenValReal1,eigenValImag1,leftEigenVect1,rightEigenVect1,false);
	m2.eig(eigenValReal2,eigenValImag2,leftEigenVect2,rightEigenVect2,false);

	// for printing - Debug
	e01 = eigenValReal1(0);
	e02 = eigenValReal1(1);
	e03 = eigenValReal1(2);

	// Make sure rightEigenVect1 is aligned with rightEigenVect2
	int align =0;
	STensorOperation::alignEigenDecomposition_NormBased(Ee0,Ce,eigenValReal1,eigenValReal2,rightEigenVect1,rightEigenVect2, e01, e02, e03, AlignedRightEigenVect1, align);
	// eigenValReal1 = AlignedEigenValReal1;
	rightEigenVect1 = AlignedRightEigenVect1;
	// e01 = AlignedEigenValReal1(0);
	// e02 = AlignedEigenValReal1(1);
	// e03 = AlignedEigenValReal1(2);
	// #####

	if(c1==0.) {STensorOperation::zero(dC1da);}
	if(c2==0.) {STensorOperation::zero(dC2da);}
	if(c3==0.) {STensorOperation::zero(dC3da);}

	static STensor43 dEe0sdCe, dCedCe;
	STensorOperation::zero(dEe0sdCe);
	STensorOperation::zero(dCedCe);
	STensorOperation::zero(Ee0s);
	if(align == 1){
    for(int i=0; i<3; i++)
      for(int j=0; j<3; j++){
    	Ee0s(i,j) += e01*C1(i,j) + e02*C2(i,j) + e03*C3(i,j);
        for(int k=0; k<3; k++)
          for(int l=0; l<3; l++){
        	  dEe0sdCe(i,j,k,l) += e01*dC1da(i,j,k,l) + e02*dC2da(i,j,k,l) + e03*dC3da(i,j,k,l);
        	  dCedCe(i,j,k,l) += dc1da(i,j)*C1(k,l) + dc2da(i,j)*C2(k,l) + dc3da(i,j)*C3(k,l) + c1*dC1da(i,j,k,l) + c2*dC2da(i,j,k,l) + c3*dC3da(i,j,k,l);  // Should be equal to _I4
          }
      }
	}
    else{
    	Ee0s = Ee0;
    }

    static STensor43 check;
    check = dCedCe;
    check -= _I4;

    // convert to dEe0sdEe
    static STensor3 Ee2, exp_Ee2;
    static STensor43 dCedEe;
	STensorOperation::zero(dCedEe);
    Ee2 = Ee;
    Ee2 *= 2;
    STensorOperation::expSTensor3(Ee2,_order,exp_Ee2,&dCedEe);
    STensorOperation::multSTensor43(dEe0sdCe,dCedEe,dEe0sdEe);
}

void mlawNonLinearTVM::getEe0s2(const STensor3& Ce, const STensor3& Ee, const STensor3& Ee0, STensor3& Ee0s, STensor43& dEe0sdEe) const{

	// Ee0 becomes Ee0_star expressed in the same configuration as Ee

	double e01,e02,e03, e1,e2,e3, c1,c2,c3; // eigenvalues
	static STensor3 E01,E02,E03,E1,E2,E3,C1,C2,C3; // bases
	static STensor3 de1da,de2da,de3da; // eigenvalue derivatives
	static STensor43 dE1da,dE2da,dE3da; // bases derivatives

	STensorOperation::getEigenDecomposition(Ee0,e01,e02,e03,E01,E02,E03);

	double det = STensorOperation::determinantSTensor3(Ee);
	if(det!=0.){
		STensorOperation::getEigenDecomposition(Ee,e1,e2,e3,E1,E2,E3,de1da,de2da,de3da,dE1da,dE2da,dE3da);
	}
	else{
		STensorOperation::zero(de1da); STensorOperation::zero(de2da); STensorOperation::zero(de3da); STensorOperation::zero(dE1da); STensorOperation::zero(dE2da); STensorOperation::zero(dE3da);
	}

	// #####
	// Align the eigenVals of Ee0 with Ee
	static fullMatrix<double> m1(3, 3), m2(3, 3);
	static fullVector<double> eigenValReal1(3), eigenValReal2(3), AlignedEigenValReal1(3);
	static fullVector<double> eigenValImag1(3), eigenValImag2(3);
	static fullMatrix<double> leftEigenVect1(3,3), leftEigenVect2(3,3);
	static fullMatrix<double> rightEigenVect1(3,3), rightEigenVect2(3,3), AlignedRightEigenVect1(3,3);
	m1.setAll(0.); m2.setAll(0.);
	eigenValReal1.setAll(0.); eigenValReal2.setAll(0.); AlignedEigenValReal1.setAll(0.);
	eigenValImag1.setAll(0.); eigenValImag2.setAll(0.);
	leftEigenVect1.setAll(0.); leftEigenVect2.setAll(0.);
	rightEigenVect1.setAll(0.); rightEigenVect2.setAll(0.); AlignedRightEigenVect1.setAll(0.);

	// Get eigen values and vectors
	Ee0.getMat(m1); Ee.getMat(m2);
	m1.eig(eigenValReal1,eigenValImag1,leftEigenVect1,rightEigenVect1,false);
	m2.eig(eigenValReal2,eigenValImag2,leftEigenVect2,rightEigenVect2,false);

	// for printing - Debug
	e01 = eigenValReal1(0);
	e02 = eigenValReal1(1);
	e03 = eigenValReal1(2);

	// Make sure rightEigenVect1 is aligned with rightEigenVect2
	int align = 0;
	STensorOperation::alignEigenDecomposition_NormBased(Ee0,Ee,eigenValReal1,eigenValReal2,rightEigenVect1,rightEigenVect2, e01, e02, e03, AlignedRightEigenVect1,align);
	rightEigenVect1 = AlignedRightEigenVect1;

	static STensor43 dEedEe;
	STensorOperation::zero(dEe0sdEe);
	STensorOperation::zero(dEedEe);
	STensorOperation::zero(Ee0s);
	if(align == 1){
    for(int i=0; i<3; i++)
      for(int j=0; j<3; j++){
    	Ee0s(i,j) += e01*E1(i,j) + e02*E2(i,j) + e03*E3(i,j);
        for(int k=0; k<3; k++)
          for(int l=0; l<3; l++){
        	  dEe0sdEe(i,j,k,l) += e01*dE1da(i,j,k,l) + e02*dE2da(i,j,k,l) + e03*dE3da(i,j,k,l);
        	  dEedEe(i,j,k,l) += de1da(i,j)*E1(k,l) + de2da(i,j)*E2(k,l) + de3da(i,j)*E3(k,l) + e1*dE1da(i,j,k,l) + e2*dE2da(i,j,k,l) + e3*dE3da(i,j,k,l);  // Should be equal to _I4
          }
      }
	}
    else{
    	Ee0s = Ee0;
    }
	
    static STensor43 check;
    check = dEedEe;
    check -= _I4;
}

void mlawNonLinearTVM::getAi0s(const STensor3& Ce, const STensor3& Ee, const std::vector<STensor3>& Ai0, std::vector<STensor3>& Ai0s, std::vector<STensor43>& dAi0sdEe) const{

	// Ee0 becomes Ee0_star expressed in the same configuration as Ee

	double e01,e02,e03, e1,e2,e3, c1,c2,c3; // eigenvalues
	static STensor3 E01,E02,E03,E1,E2,E3,C1,C2,C3; // bases
	static STensor3 dc1da,dc2da,dc3da; // eigenvalue derivatives
	static STensor43 dC1da,dC2da,dC3da; // bases derivatives

	STensorOperation::getEigenDecomposition(Ee,e1,e2,e3,E1,E2,E3);
	STensorOperation::getEigenDecomposition(Ce,c1,c2,c3,C1,C2,C3,dc1da,dc2da,dc3da,dC1da,dC2da,dC3da);
	if(c1==0.) {STensorOperation::zero(dC1da);}
	if(c2==0.) {STensorOperation::zero(dC2da);}
	if(c3==0.) {STensorOperation::zero(dC3da);}


	// #####
	// Align the eigenVals of Ee0 with Ee
	static fullMatrix<double> m1(3, 3), m2(3, 3);
	static fullVector<double> eigenValReal1(3), eigenValReal2(3), AlignedEigenValReal1(3);
	static fullVector<double> eigenValImag1(3), eigenValImag2(3);
	static fullMatrix<double> leftEigenVect1(3,3), leftEigenVect2(3,3);
	static fullMatrix<double> rightEigenVect1(3,3), rightEigenVect2(3,3), AlignedRightEigenVect1(3,3);
	m1.setAll(0.); m2.setAll(0.);
	eigenValReal1.setAll(0.); eigenValReal2.setAll(0.); AlignedEigenValReal1.setAll(0.);
	eigenValImag1.setAll(0.); eigenValImag2.setAll(0.);
	leftEigenVect1.setAll(0.); leftEigenVect2.setAll(0.);
	rightEigenVect1.setAll(0.); rightEigenVect2.setAll(0.); AlignedRightEigenVect1.setAll(0.);

	// Get eigen values and vectors
	Ce.getMat(m2); m2.eig(eigenValReal2,eigenValImag2,leftEigenVect2,rightEigenVect2,false);

	// conversion tensor to dEe0sdEe
    static STensor3 Ee2, exp_Ee2;
    static STensor43 dCedEe;
	STensorOperation::zero(dCedEe);
    Ee2 = Ee;
    Ee2 *= 2;
    STensorOperation::expSTensor3(Ee2,_order,exp_Ee2,&dCedEe);

    // Loop
    for (int i=0; i<_Gi.size(); i++){

    	// Get eigen values and vectors
    	m1.setAll(0.); eigenValReal1.setAll(0.); eigenValImag1.setAll(0.); leftEigenVect1.setAll(0.); rightEigenVect1.setAll(0.); AlignedRightEigenVect1.setAll(0.);
    	STensorOperation::getEigenDecomposition(Ai0[i],e01,e02,e03,E01,E02,E03);
    	Ai0[i].getMat(m1);
    	m1.eig(eigenValReal1,eigenValImag1,leftEigenVect1,rightEigenVect1,false);


    	// eigenvalues
    	e01 = eigenValReal1(0);
    	e02 = eigenValReal1(1);
    	e03 = eigenValReal1(2);

    	// Make sure rightEigenVect1 is aligned with rightEigenVect2
    	int align = 0;
    	STensorOperation::alignEigenDecomposition_NormBased(Ai0[i],Ce,eigenValReal1,eigenValReal2,rightEigenVect1,rightEigenVect2, e01, e02, e03, AlignedRightEigenVect1, align);
        rightEigenVect1 = AlignedRightEigenVect1;

    	static STensor43 dAi0sdCe; // , dCedCe;
    	STensorOperation::zero(dAi0sdCe);
    	// STensorOperation::zero(dCedCe);
    	STensorOperation::zero(Ai0s[i]);
    	if(align == 1){
    	for(int p=0; p<3; p++)
    		for(int j=0; j<3; j++){
    			Ai0s[i](p,j) += e01*C1(p,j) + e02*C2(p,j) + e03*C3(p,j);
    			for(int k=0; k<3; k++)
    				for(int l=0; l<3; l++){
    					dAi0sdCe(p,j,k,l) += e01*dC1da(p,j,k,l) + e02*dC2da(p,j,k,l) + e03*dC3da(p,j,k,l);
    					// dCedCe(p,j,k,l) += dc1da(p,j)*C1(k,l) + dc2da(p,j)*C2(k,l) + dc3da(p,j)*C3(k,l) + c1*dC1da(p,j,k,l) + c2*dC2da(p,j,k,l) + c3*dC3da(p,j,k,l);  // Should be equal to _I4
    				}
    		}
    	}
        else{
        	Ai0s[i] = Ai0[i];
        }
    	
    	// debug
    	STensorOperation::getEigenDecomposition(Ai0s[i],e01,e02,e03,E01,E02,E03);

    	/*
    	static STensor43 check;
    	check = dCedCe;
    	check -= _I4;*/

    	STensorOperation::multSTensor43(dAi0sdCe,dCedEe,dAi0sdEe[i]);
    }
}

void mlawNonLinearTVM::getAi0s2(const STensor3& Ce, const STensor3& Ee, const std::vector<STensor3>& Ai0, std::vector<STensor3>& Ai0s, std::vector<STensor43>& dAi0sdEe) const{

	// Ee0 becomes Ee0_star expressed in the same configuration as Ee

	double e01,e02,e03, e1,e2,e3, c1,c2,c3; // eigenvalues
	static STensor3 E01,E02,E03,E1,E2,E3,C1,C2,C3; // bases
	static STensor3 de1da,de2da,de3da; // eigenvalue derivatives
	static STensor43 dE1da,dE2da,dE3da; // bases derivatives

	double det = STensorOperation::determinantSTensor3(Ee);
	if(det!=0.){
		STensorOperation::getEigenDecomposition(Ee,e1,e2,e3,E1,E2,E3,de1da,de2da,de3da,dE1da,dE2da,dE3da);
	}
	else{
		STensorOperation::zero(de1da); STensorOperation::zero(de2da); STensorOperation::zero(de3da); STensorOperation::zero(dE1da); STensorOperation::zero(dE2da); STensorOperation::zero(dE3da);
	}


	// #####
	// Align the eigenVals of Ee0 with Ee
	static fullMatrix<double> m1(3, 3), m2(3, 3);
	static fullVector<double> eigenValReal1(3), eigenValReal2(3), AlignedEigenValReal1(3);
	static fullVector<double> eigenValImag1(3), eigenValImag2(3);
	static fullMatrix<double> leftEigenVect1(3,3), leftEigenVect2(3,3);
	static fullMatrix<double> rightEigenVect1(3,3), rightEigenVect2(3,3), AlignedRightEigenVect1(3,3);
	m1.setAll(0.); m2.setAll(0.);
	eigenValReal1.setAll(0.); eigenValReal2.setAll(0.); AlignedEigenValReal1.setAll(0.);
	eigenValImag1.setAll(0.); eigenValImag2.setAll(0.);
	leftEigenVect1.setAll(0.); leftEigenVect2.setAll(0.);
	rightEigenVect1.setAll(0.); rightEigenVect2.setAll(0.); AlignedRightEigenVect1.setAll(0.);

	// Get eigen values and vectors
	Ee.getMat(m2); m2.eig(eigenValReal2,eigenValImag2,leftEigenVect2,rightEigenVect2,false);

    // Loop
    for (int i=0; i<_Gi.size(); i++){

    	// Get eigen values and vectors
    	m1.setAll(0.); eigenValReal1.setAll(0.); eigenValImag1.setAll(0.); leftEigenVect1.setAll(0.); rightEigenVect1.setAll(0.); AlignedRightEigenVect1.setAll(0.);
    	STensorOperation::getEigenDecomposition(Ai0[i],e01,e02,e03,E01,E02,E03);
    	Ai0[i].getMat(m1);
    	m1.eig(eigenValReal1,eigenValImag1,leftEigenVect1,rightEigenVect1,false);


    	// eigenvalues
    	e01 = eigenValReal1(0);
    	e02 = eigenValReal1(1);
    	e03 = eigenValReal1(2);

    	// Make sure rightEigenVect1 is aligned with rightEigenVect2
    	int align = 0;
    	STensorOperation::alignEigenDecomposition_NormBased(Ai0[i],Ee,eigenValReal1,eigenValReal2,rightEigenVect1,rightEigenVect2, e01, e02, e03, AlignedRightEigenVect1, align);
    	// STensorOperation::alignEigenDecomposition_EigenVectorDotProductBased(eigenValReal1,eigenValReal2,rightEigenVect1,rightEigenVect2, e01, e02, e03, AlignedRightEigenVect1);
        rightEigenVect1 = AlignedRightEigenVect1;

    	STensorOperation::zero(dAi0sdEe[i]);
    	// STensorOperation::zero(dCedCe);
    	STensorOperation::zero(Ai0s[i]);
    if(align == 1){
    	for(int p=0; p<3; p++)
    		for(int j=0; j<3; j++){
    			Ai0s[i](p,j) += e01*E1(p,j) + e02*E2(p,j) + e03*E3(p,j);
    			for(int k=0; k<3; k++)
    				for(int l=0; l<3; l++){
    					dAi0sdEe[i](p,j,k,l) += e01*dE1da(p,j,k,l) + e02*dE2da(p,j,k,l) + e03*dE3da(p,j,k,l);
    					// dCedCe(p,j,k,l) += dc1da(p,j)*C1(k,l) + dc2da(p,j)*C2(k,l) + dc3da(p,j)*C3(k,l) + c1*dC1da(p,j,k,l) + c2*dC2da(p,j,k,l) + c3*dC3da(p,j,k,l);  // Should be equal to _I4
    				}
    		}
    }
    else{
    	Ai0s[i] = Ai0[i];
    }

    	// debug
    	STensorOperation::getEigenDecomposition(Ai0s[i],e01,e02,e03,E01,E02,E03);

    }
}

void mlawNonLinearTVM::ThermoViscoElasticPredictor_forTVP(const STensor3& Ce, const STensor3& Ee, const STensor3& Ee0,
          const IPNonLinearTVM *q0, IPNonLinearTVM *q1,
          double& Ke, double& Ge,
          const double T0, const double T1, const bool stiff, STensor43& Bd_stiffnessTerm, STensor43* Bd_stiffnessTerm2, STensor43* rotationStiffness) const{

	// Caution!! -> For _rotationCorrectionScheme == 0 and 1,  Ee0 is already rotated. _R and its derivatives are in the IP

    //
    STensorOperation::zero(Bd_stiffnessTerm);
    // if(_extraBranchNLType == TensionCompressionRegularisedType && Bd_stiffnessTerm2!= NULL){
    if(Bd_stiffnessTerm2!= NULL){
        STensorOperation::zero(*Bd_stiffnessTerm2);
    }


    // NEW EIGEN
    static STensor3 devEe0s;
    static STensor43 dDevEe0sdEe;
    STensorOperation::zero(dDevEe0sdEe);
    STensorOperation::zero(devEe0s);
    
    static STensor3 devEe0; double trEe0(0.);
    static STensor3 devEe; double trEe(0.);
    STensorOperation::decomposeDevTr(q0->_Ee,devEe0,trEe0);
    STensorOperation::decomposeDevTr(Ee,devEe,trEe);
    if (_rotationCorrectionScheme == 2){
    	// mlawNonLinearTVM::getEe0s2(Ce,Ee,devEe0,devEe0,dDevEe0sdEe);
    		// mlawNonLinearTVM::getdEe0dEe(Ce,Ee,devEe0,dDevEe0sdEe);
    	STensorOperation::alignEigenDecomposition_NormBased_withDerivative(Ee,devEe0,devEe0s,dDevEe0sdEe);
    	static STensor43 check;
    	STensorOperation::zero(check);
        for (int k=0; k<3; k++)
            for (int l=0; l<3; l++)
                for (int p=0; p<3; p++)
                    for (int q=0; q<3; q++)
                        for (int r=0; r<3; r++)
                          for (int s=0; s<3; s++)
                        	  check(k,l,p,q) += dDevEe0sdEe(k,l,r,s)*_I(r,s)*_I(p,q);
        
        double checkers = 0;
    }
    std::vector<STensor43> rotationDer1, rotationDer2, dAi0sdEe;
    rotationDer1.clear(); rotationDer2.clear(); dAi0sdEe.clear();
    for (int i=0; i<_Gi.size(); i++){
      STensor43 el43(0.);
      dAi0sdEe.push_back(el43);
      rotationDer1.push_back(el43);
      rotationDer2.push_back(el43);
    }

    if (_rotationCorrectionScheme == 2){
    	q1->_A_rot = q0->_A;
    	// mlawNonLinearTVM::getAi0s2(Ce,Ee,q0->_A,q1->_A_rot,dAi0sdEe);
    	STensorOperation::alignEigenDecomposition_NormBased_withDerivative_Vector(Ee,q0->_A,q1->_A_rot,dAi0sdEe);
    }
 
    if ((_Ki.size() > 0) or (_Gi.size() > 0)){
  	  for (int i=0; i<_Gi.size(); i++){
  		 if (_rotationCorrectionScheme == 0 && _rotationCorrectionScheme == 1){
  		  for(int k=0; k<3; k++)
  		 	for(int l=0; l<3; l++){
  			   q1->_A_rot[i](k,l) = 0.;
     		  	 for(int p=0; p<3; p++)
     		  		 for(int q=0; q<3; q++)
     		  			 q1->_A_rot[i](k,l) += q1->_R(k,p)*q0->_A[i](p,q)*q1->_R(l,q);
  		 	   }
  		 }
  	  }
    }

    if(rotationStiffness!= NULL){
        STensorOperation::zero(*rotationStiffness);
    }
    // NEW EIGEN
    
    // Set Temperature to calculate temperature updated properties
    double T_set = setTemp(T0,T1,1);  // 1->T1, 2->T_mid, 3->T_midshift

    // Update the Properties to the current temperature (see below which ones are being used)
    double KT, GT, AlphaT0, AlphaT, DKDT, DGDT, DAlpha0DT, DAlphaDT, DDAlphaDTT;
    getK(q1,KT,T_set,&DKDT); getG(q1,GT,T_set,&DGDT); getAlpha(AlphaT,T_set,&DAlphaDT,&DDAlphaDTT); getAlpha(AlphaT0,T0);

    // Get LogStrain (E) andput DEe in IP
    STensor3& DE = q1->getRefToDE();
    STensor3& devDE = q1->getRefToDevDE();
    double& trDE = q1->getRefToTrDE();
    DE =  Ee;
    DE -= Ee0;
    STensorOperation::decomposeDevTr(DE,devDE,trDE);
    if (_rotationCorrectionScheme == 2){
    	devDE = devEe;
    	devDE -= devEe0s;
    }

    // Initialise effective trEe - include thermal expansion
    double eff_trEe(0.), eff_trDE(0.);
    eff_trEe = trEe - 3*AlphaT*(T1-_Tinitial);
    eff_trDE = trDE - 3.*(AlphaT*(T1-_Tinitial) - AlphaT0*(T0-_Tinitial));
        
    // Initialise CorKirStress - Dev
    static STensor3 devK;
    STensorOperation::zero(devK);
    devK = 2*GT*devEe;
    
    // Initialise CorKirStress - Tr
    double p(0.);
    p = KT*eff_trEe;
    
    // Initialise Moduli
    Ke = KT; Ge = GT;

    if(_useExtraBranch){
        
        double A(0.), B(0.), dA_dTrEe(0.), psiInfCorrector(0.);
        static STensor3 dB_dDevEe;
        double intA(0.), intB(0.), dA_dT(0.), dB_dT(0.), DintA(0.), DintB(0.);
        
        if(_extraBranchNLType != TensionCompressionRegularisedType && _extraBranchNLType != hyper_exp_TCasymm_Type){
            evaluateElasticCorrection(eff_trEe, devEe, T1, A, dA_dTrEe, intA, dA_dT, B, dB_dDevEe, intB, dB_dT, NULL ,NULL, NULL, NULL, NULL, &psiInfCorrector, &DintA, &DintB);
        }
        else{
            double dB_dTrEe(0.);
            evaluateElasticCorrection(eff_trEe, devEe, T1, A, dA_dTrEe, intA, dA_dT, B, dB_dDevEe, intB, dB_dT, NULL ,NULL, NULL, NULL, &dB_dTrEe, &psiInfCorrector, &DintA, &DintB);
            q1->_dBd_dTrEe = dB_dTrEe;
        }
    
        q1->_intA = intA;
        q1->_intB = intB;
        q1->_DintA = DintA;
        q1->_DintB = DintB;
        q1->_psiInfCorrector = psiInfCorrector; // Additional term to correct thermal expansion term in free energy
        q1->_elasticBulkPropertyScaleFactor = A; 
        q1->_elasticShearPropertyScaleFactor = B; 
        q1->_dAv_dTrEe = dA_dTrEe;
        q1->_dBd_dDevEe = dB_dDevEe;
        
        
        // Add to stress
        static STensor3 sigExtra;
        sigExtra = devEe;
        sigExtra *= 2.*GT*B;
        devK += sigExtra;
        sigExtra += _I*(eff_trEe*KT*A);
        p += eff_trEe*KT*A;
        q1->_corKirExtra = sigExtra; 
        
        // Add to Ke
        Ke += KT*A + KT*dA_dTrEe*eff_trEe;
        
        // Add to Ge (1st term) and Bd_stiffnessTerm (2nd term)
        Ge += GT*B; // don't multiply 2
        for (int k=0; k<3; k++)
            for (int l=0; l<3; l++)
                for (int p=0; p<3; p++)
                    for (int q=0; q<3; q++){
                        Bd_stiffnessTerm(k,l,p,q) +=  2*GT*devEe(k,l)*dB_dDevEe(p,q);
                        if(_extraBranchNLType == TensionCompressionRegularisedType || _extraBranchNLType == hyper_exp_TCasymm_Type){
                            (*Bd_stiffnessTerm2)(k,l,p,q) +=  2*GT*devEe(k,l)*_I(p,q)*q1->_dBd_dTrEe;
                        }
                    }
                        
    }
    
    // Main Loop for Maxwell Branches
    if ((_Ki.size() > 0) or (_Gi.size() > 0)){

        // Get ShiftFactor and shifted time increment
        double dt = this->getTimeStep();
        double dt_shift_rec(0.), dt_shift_mid(0.);
        double Ddt_shiftDT_rec(0.), Ddt_shiftDT_mid(0.), DDdt_shiftDT_rec(0.), DDdt_shiftDT_mid(0.);
        shiftedTime(dt, T0, T1, &dt_shift_rec,&Ddt_shiftDT_rec,&DDdt_shiftDT_rec,0);
        shiftedTime(dt, T0, T1, &dt_shift_mid,&Ddt_shiftDT_mid,&DDdt_shiftDT_mid);

                    
        // Do non-linear TVE extraBranch here
        double Av(1.), Bd(1.);
        double dA_dTrEe(0.),intA(0.),intA1(0.),intA2(0.),dA_dT(0.),ddA_dTT(0.),ddA_dTdTrEe(0.),intB(0.),dB_dT(0.),ddB_dTT(0.),DintA(0.),DintB(0.);
        static STensor3 intB1,intB2,dB_dDevEe,ddB_dTdDevEe;
        STensorOperation::zero(intB1);
        STensorOperation::zero(intB2);
        STensorOperation::zero(dB_dDevEe);
        STensorOperation::zero(ddB_dTdDevEe);
        
        if (_useExtraBranch_TVE){                            
            if(_ExtraBranch_TVE_option == 1){
                mlawNonLinearTVM::extraBranch_nonLinearTVE(0,Ee,T1,Av,dA_dTrEe,intA,intA1,intA2,dA_dT,ddA_dTT,ddA_dTdTrEe,Bd,dB_dDevEe,intB,intB1,intB2,dB_dT,ddB_dTT,ddB_dTdDevEe,DintA,DintB);
                q1->_Av_TVE = Av; q1->_Bd_TVE = Bd; q1->_dAv_dTrEe_TVE = dA_dTrEe; q1->_dBd_dDevEe_TVE = dB_dDevEe;
                q1->_intAv_TVE = intA; q1->_intBd_TVE = intB;
                q1->_intAv_1_TVE = intA1; q1->_intBd_1_TVE = intB1;  // this is zero
                q1->_intAv_2_TVE = intA2; q1->_intBd_2_TVE = intB2;  // this is zero
            }
        }
        
        for (int i=0; i<_Gi.size(); i++){
                        
            // Dev Q_i and  Tr Q_i
            
            double dtg = dt_shift_rec/(_gi[i]);
            double dtg2 = dt_shift_mid/(_gi[i]);
            double exp_rec_g = exp(-dtg);     // rec
            double exp_mid_g = exp(-dtg2); // mid
            double dtk = dt_shift_rec/(_ki[i]);
            double dtk2 = dt_shift_mid/(_ki[i]);
            double exp_rec_k = exp(-dtk);      // rec
            double exp_mid_k = exp(-dtk2);  // mid
            
            if (!_useExtraBranch_TVE){
                
                // Ge for DcorKirDE
                Ge += _Gi[i]*exp_mid_g; // 2* (..) - Dont multiply by 2 if using lambda and mu in Hooks tensor function
            
                // Ke for DcorKirDE
                Ke += _Ki[i]*exp_mid_k;
            
                // Single Convolution
                for (int k=0; k<3; k++)
                    for (int l=0; l<3; l++){
                    	// q1->_A[i](k,l) = exp_rec_g*q0->_A[i](k,l) + exp_mid_g*devDE(k,l);
                        q1->_A[i](k,l) = exp_rec_g*q1->_A_rot[i](k,l) + exp_mid_g*devDE(k,l); // NEW EIGEN
                    }
            
                q1->_B[i] = exp_rec_k*q0->_B[i] + exp_mid_k*eff_trDE ; //*trDE; NEW
            
                // Add to deviatoric stress
                devK += 2*_Gi[i]*q1->_A[i];
            
                // Add to volumetric stress
                p += _Ki[i]*q1->_B[i];

                for (int k=0; k<3; k++)
                  for (int l=0; l<3; l++)
                    for (int p=0; p<3; p++)
                      for (int q=0; q<3; q++){
                    	if (_rotationCorrectionScheme == 2){
                    		rotationDer1[i](k,l,p,q) += dAi0sdEe[i](k,l,p,q);
                    		rotationDer2[i](k,l,p,q) += (-dDevEe0sdEe(k,l,p,q));
                    	}
                        for (int r=0; r<3; r++)
                          for (int s=0; s<3; s++){
                            if (_rotationCorrectionScheme == 1){
                            	rotationDer1[i](k,l,p,q) +=  ( q1->_dRdEe(k,r,p,q)*q0->_A[i](r,s)*q1->_R(l,s) + q1->_R(k,r)*q0->_A[i](r,s)*q1->_dRtdEe(s,l,p,q) );
								rotationDer2[i](k,l,p,q) +=  ( - q1->_dRdEe(k,r,p,q)*devEe0(r,s)*q1->_R(l,s) - q1->_R(k,r)*devEe0(r,s)*q1->_dRtdEe(s,l,p,q) ) ;
                            }
                          }
                      }
                for (int k=0; k<3; k++)
                  for (int l=0; l<3; l++)
                    for (int p=0; p<3; p++)
                      for (int q=0; q<3; q++)
                    	(*rotationStiffness)(k,l,p,q) +=  2*_Gi[i]*( exp_rec_g*rotationDer1[i](k,l,p,q) + exp_mid_g*rotationDer2[i](k,l,p,q) );
                // NEW EIGEN

            }
            else{ // all extraBranches
                
             if(_ExtraBranch_TVE_option == 1){
                // Evaluate Bd_stiffnessTerm
                for (int k=0; k<3; k++)
                    for (int l=0; l<3; l++)
                        for (int p=0; p<3; p++)
                            for (int q=0; q<3; q++)
                                Bd_stiffnessTerm(k,l,p,q) +=  2*_Gi[i]*q0->_A[i](k,l)*q0->_dBd_dDevEe_TVE(p,q) * 1/q0->_Bd_TVE * exp_mid_g;
                
                // Ge for DcorKirDE
                Ge += ( exp_mid_g *_Gi[i] * q0->_Bd_TVE ); // 2* (..) - Dont multiply by 2 if using lambda and mu in Hooks tensor function
            
                // Ke for DcorKirDE
                Ke += ( exp_mid_k *_Ki[i] * Av ) ; 
                Ke += ( exp_mid_k *_Ki[i]* q0->_B[i]/Av * dA_dTrEe );
            
                // Single Convolution
                double temp2(0.);
                STensorOperation::doubleContractionSTensor3(q0->_dBd_dDevEe_TVE,devDE,temp2);
                for (int k=0; k<3; k++)
                    for (int l=0; l<3; l++)
                        q1->_A[i](k,l) = exp_rec_g*q0->_A[i](k,l) + exp_mid_g*( q0->_Bd_TVE*devDE(k,l) + q0->_A[i](k,l) * 1/q0->_Bd_TVE * temp2);
            
                q1->_B[i] = ( exp_rec_k*q0->_B[i] );
                q1->_B[i] += ( exp_mid_k *( q1->_intAv_1_TVE - q0->_intAv_1_TVE ) ); 
                q1->_B[i] += ( exp_mid_k*( q0->_B[i]*(q1->_intAv_2_TVE - q0->_intAv_2_TVE) ) );
            
                // Add to deviatoric stress
                devK += ( 2.*_Gi[i]*q1->_A[i] );
            
                // Add to volumetric stress
                p += ( _Ki[i]*q1->_B[i] );
             }
             else if (_ExtraBranch_TVE_option == 2 || _ExtraBranch_TVE_option == 3 || _ExtraBranch_TVE_option == 4 || _ExtraBranch_TVE_option == 5){
                    
                // Single Convolution - get branchElasticStrain
                for (int k=0; k<3; k++)
                    for (int l=0; l<3; l++){
                    	// q1->_A[i](k,l) = exp_rec_g*q0->_A[i](k,l) + exp_mid_g*devDE(k,l);
                        q1->_A[i](k,l) = exp_rec_g*q1->_A_rot[i](k,l) + exp_mid_g*devDE(k,l); // NEW EIGEN
                    }
            
                q1->_B[i] = exp_rec_k*q0->_B[i] + exp_mid_k*eff_trDE; // trDE; // NEW
                    
                static STensor3 branchElasticStrain;
                branchElasticStrain = q1->_A[i];
                branchElasticStrain(0,0) += 1./3.*q1->_B[i];
                branchElasticStrain(1,1) += 1./3.*q1->_B[i];
                branchElasticStrain(2,2) += 1./3.*q1->_B[i];
                if (_ExtraBranch_TVE_option == 2){
                    mlawNonLinearTVM::extraBranch_nonLinearTVE(i,branchElasticStrain,T1,Av,dA_dTrEe,intA,intA1,intA2,dA_dT,ddA_dTT,ddA_dTdTrEe,
                                                                Bd,dB_dDevEe,intB,intB1,intB2,dB_dT,ddB_dTT,ddB_dTdDevEe,DintA,DintB);
                }
                else{
                    double dB_dTrEe(0.);
                    mlawNonLinearTVM::extraBranch_nonLinearTVE(i,branchElasticStrain,T1,Av,dA_dTrEe,intA,intA1,intA2,dA_dT,ddA_dTT,ddA_dTdTrEe,
                                                                Bd,dB_dDevEe,intB,intB1,intB2,dB_dT,ddB_dTT,ddB_dTdDevEe,DintA,DintB,&dB_dTrEe);
                    q1->_dBd_dTrEe_TVE_vector[i] = dB_dTrEe;
                }
                    
                q1->_Av_TVE_vector[i] = Av;
                q1->_Bd_TVE_vector[i] = Bd;
                q1->_dAv_dTrEe_TVE_vector[i] = dA_dTrEe;
                q1->_dBd_dDevEe_TVE_vector[i] = dB_dDevEe;
                q1->_intAv_TVE_vector[i] = intA;
                q1->_intBd_TVE_vector[i] = intB;
                q1->_DintAv_TVE_vector[i] = DintA;
                q1->_DintBd_TVE_vector[i] = DintB;

                // Evaluate Bd_stiffnessTerm
                for (int k=0; k<3; k++)
                    for (int l=0; l<3; l++)
                        for (int p=0; p<3; p++)
                            for (int q=0; q<3; q++){
                                Bd_stiffnessTerm(k,l,p,q) += 2*_Gi[i]*q1->_A[i](k,l)*dB_dDevEe(p,q) * exp_mid_g;
                                if (_ExtraBranch_TVE_option == 3 || _ExtraBranch_TVE_option == 4 || _ExtraBranch_TVE_option == 5){
                                    (*Bd_stiffnessTerm2)(k,l,p,q) += 2*_Gi[i]*q1->_dBd_dTrEe_TVE_vector[i]*q1->_A[i](k,l)*_I(p,q) * exp_mid_k;
                                } 
                            }
                                
                // Ge for DcorKirDE
                Ge += ( exp_mid_g *_Gi[i] * Bd); // 2* (..) - Dont multiply by 2 if using lambda and mu in Hooks tensor function
                    
                // Ke for DcorKirDE
                Ke += ( exp_mid_k *_Ki[i] * (dA_dTrEe*q1->_B[i] + Av) ); 
                    
                // Add to deviatoric stress
                devK += ( 2.*_Gi[i]*Bd*q1->_A[i] );
            
                // Add to volumetric stress
                p += ( _Ki[i]*Av*q1->_B[i] );

                for (int k=0; k<3; k++)
                  for (int l=0; l<3; l++)
                    for (int p=0; p<3; p++)
                      for (int q=0; q<3; q++){
                    	if (_rotationCorrectionScheme == 2){
                    		rotationDer1[i](k,l,p,q) += dAi0sdEe[i](k,l,p,q);
                    		rotationDer2[i](k,l,p,q) += (-dDevEe0sdEe(k,l,p,q));
                    	}
                        for (int r=0; r<3; r++)
                          for (int s=0; s<3; s++){
                        	if (_rotationCorrectionScheme == 1){
                            	rotationDer1[i](k,l,p,q) +=  ( q1->_dRdEe(k,r,p,q)*q0->_A[i](r,s)*q1->_R(l,s) + q1->_R(k,r)*q0->_A[i](r,s)*q1->_dRtdEe(s,l,p,q) );
								rotationDer2[i](k,l,p,q) +=  ( - q1->_dRdEe(k,r,p,q)*devEe0(r,s)*q1->_R(l,s) - q1->_R(k,r)*devEe0(r,s)*q1->_dRtdEe(s,l,p,q) ) ;
                        	}

                        	if (_rotationCorrectionScheme == 2){
                        		// Ignore this term - ADDED on 20/06
                        		// (*rotationStiffness)(k,l,p,q) += 2.*_Gi[i]*q1->_A[i](k,l)*dB_dDevEe(r,s) * ( exp_rec_g*dAi0sdEe[i](r,s,p,q) - exp_mid_g*dDevEe0sdEe(r,s,p,q) );
                        	}
                            for (int m=0; m<3; m++)
                              for (int n=0; n<3; n++){
                            	if (_rotationCorrectionScheme == 1){
                            		// Ignore this term - ADDED on 20/06
                            		/*(*rotationStiffness)(k,l,p,q) += 2.*_Gi[i]*q1->_A[i](k,l)*dB_dDevEe(r,s) * (exp_rec_g*( q1->_dRdEe(r,m,p,q)*q0->_A[i](m,n)*q1->_R(s,n) + q1->_R(r,m)*q0->_A[i](m,n)*q1->_dRtdEe(n,s,p,q) )
        								+  exp_mid_g*( - q1->_dRdEe(r,m,p,q)*devEe0(m,n)*q1->_R(s,n) - q1->_R(r,m)*devEe0(m,n)*q1->_dRtdEe(n,s,p,q) ));*/
                            	}
                              }
                          }
                      }

                for (int k=0; k<3; k++)
                  for (int l=0; l<3; l++)
                    for (int r=0; r<3; r++)
                      for (int s=0; s<3; s++){
                      	(*rotationStiffness)(k,l,r,s) +=  2*_Gi[i]*Bd*( exp_rec_g*rotationDer1[i](k,l,r,s) + exp_mid_g*rotationDer2[i](k,l,r,s) );
                    	for (int p=0; p<3; p++)
                          for (int q=0; q<3; q++){
                        	(*rotationStiffness)(k,l,r,s) += 2.*_Gi[i]*q1->_A[i](k,l)*dB_dDevEe(p,q)*( exp_rec_g*rotationDer1[i](p,q,r,s) + exp_mid_g*rotationDer2[i](p,q,r,s) );
                          }
                      }
                // NEW EIGEN
             }
             
            } // all extrabranches
        }
        
        // Calculate derivatives here and put them in IP
        if(stiff){
            std::vector<double>& DB_DT = q1->_DB_DT;
            std::vector<double>& DB_DtrE = q1->_DB_DtrE;
            std::vector<double>& DDB_DTT = q1->_DDB_DTT;
            std::vector<double>& DDB_DTDtrE = q1->_DDB_DTDtrE;
            std::vector<STensor3>& DA_DT = q1->_DA_DT;
            std::vector<STensor3>& DDA_DTT = q1->_DDA_DTT;
            std::vector<STensor3>& DA_DtrE = q1->_DA_DtrE;
            std::vector<STensor3>& DDA_DTDtrE = q1->_DDA_DTDtrE;
            std::vector<STensor43>& DA_DdevE = q1->_DA_DdevE;
            std::vector<STensor43>& DDA_DTDdevE = q1->_DDA_DTDdevE;
                    
            double Dexp_rec_k_DT(0.), Dexp_mid_k_DT(0.), DDexp_rec_k_DTT(0.), DDexp_mid_k_DTT(0.);  
            double Dexp_rec_g_DT(0.), Dexp_mid_g_DT(0.), DDexp_rec_g_DTT(0.), DDexp_mid_g_DTT(0.);  
            
            double dtk(0.), dtk2(0.), exp_rec_k(0.), exp_mid_k(0.);
            double dtg(0.), dtg2(0.), exp_rec_g(0.), exp_mid_g(0.);
            
            // NEW
            double Deff_trDE_DT = -3.*(DAlphaDT*(T1-_Tinitial) + AlphaT);
            double DDeff_trDE_DTT = -3.*(DDAlphaDTT*(T1-_Tinitial) + 2*DAlphaDT);
            // NEW
            
            for (int i=0; i<_Gi.size(); i++){   
                
                dtk = dt_shift_rec/(_ki[i]);
                dtk2 = dt_shift_mid/(_ki[i]);
                exp_rec_k = exp(-dtk);     // rec
                exp_mid_k = exp(-dtk2); // mid
                
                dtg = dt_shift_rec/(_gi[i]);
                dtg2 = dt_shift_mid/(_gi[i]);
                exp_rec_g = exp(-dtg);     // rec
                exp_mid_g = exp(-dtg2); // mid
            
                Dexp_rec_k_DT = -1/(_ki[i]) * Ddt_shiftDT_rec*exp_rec_k;
                Dexp_mid_k_DT = -1/(_ki[i]) * Ddt_shiftDT_mid*exp_mid_k;
                Dexp_rec_g_DT = -1/(_gi[i]) * Ddt_shiftDT_rec*exp_rec_g;
                Dexp_mid_g_DT = -1/(_gi[i]) * Ddt_shiftDT_mid*exp_mid_g;
                
                DDexp_rec_k_DTT = -1/(_ki[i]) * (DDdt_shiftDT_rec - pow(Ddt_shiftDT_rec,2)/(_ki[i]))*exp_rec_k;
                DDexp_mid_k_DTT = -1/(_ki[i]) * (DDdt_shiftDT_mid - pow(Ddt_shiftDT_mid,2)/(_ki[i]))*exp_mid_k;
                DDexp_rec_g_DTT = -1/(_gi[i]) * (DDdt_shiftDT_rec - pow(Ddt_shiftDT_rec,2)/(_gi[i]))*exp_rec_g;
                DDexp_mid_g_DTT = -1/(_gi[i]) * (DDdt_shiftDT_mid - pow(Ddt_shiftDT_mid,2)/(_gi[i]))*exp_mid_g;
                
                if (!_useExtraBranch_TVE){
                    DB_DtrE[i] = exp_mid_k;
                    DB_DT[i] = q0->_B[i] * Dexp_rec_k_DT + Dexp_mid_k_DT*eff_trDE + exp_rec_k*Deff_trDE_DT; // trDE; // NEW
                    DDB_DTT[i] = q0->_B[i] * DDexp_rec_k_DTT + DDexp_mid_k_DTT*eff_trDE + 2*Dexp_mid_k_DT*Deff_trDE_DT + exp_mid_k*DDeff_trDE_DTT; // trDE; //NEW
                    DDB_DTDtrE[i] = Dexp_mid_k_DT;
                    for (int k=0; k<3; k++)
                        for (int l=0; l<3; l++){
                            DA_DT[i](k,l) = q0->_A[i](k,l) * Dexp_rec_g_DT + Dexp_mid_g_DT*devDE(k,l); 
                            DDA_DTT[i](k,l) = q0->_A[i](k,l) * DDexp_rec_g_DTT + DDexp_mid_g_DTT*devDE(k,l); 
                            for (int r=0; r<3; r++)
                                for (int s=0; s<3; s++){
                                    // _Idev is pre-multiplied !!
                                    DA_DdevE[i](k,l,r,s) =  exp_mid_g*_Idev(k,l,r,s);  
                                    DDA_DTDdevE[i](k,l,r,s) =  Dexp_mid_g_DT*_Idev(k,l,r,s);  

                                    // NEW EIGEN
                                    if ( _useRotationCorrection == true){
                                    	if(_rotationCorrectionScheme == 1 && _rotationCorrectionScheme == 2){
                                    		DA_DdevE[i](k,l,r,s) += ( exp_rec_g*rotationDer1[i](k,l,r,s) + exp_mid_g*rotationDer2[i](k,l,r,s) );
                                    		DDA_DTDdevE[i](k,l,r,s) += ( Dexp_rec_g_DT*rotationDer1[i](k,l,r,s) + Dexp_mid_g_DT*rotationDer2[i](k,l,r,s) );
                                    	}
                                    }
                                    // NEW EIGEN
                                }
                        }
                }
                else{ // all extraBranches
                
                 if(_ExtraBranch_TVE_option == 1){
                    DB_DtrE[i] = exp_mid_k * ( q1->_Av_TVE + q0->_B[i]/q1->_Av_TVE * dA_dTrEe );
                    DB_DT[i] = q0->_B[i] * Dexp_rec_k_DT + Dexp_mid_k_DT*( (q1->_intAv_1_TVE - q0->_intAv_1_TVE) +
                                                            q0->_B[i]*(q1->_intAv_2_TVE - q0->_intAv_2_TVE) );
                    DDB_DTT[i] = q0->_B[i] * DDexp_rec_k_DTT + DDexp_mid_k_DTT*( (q1->_intAv_1_TVE - q0->_intAv_1_TVE) +
                                                            q0->_B[i]*(q1->_intAv_2_TVE - q0->_intAv_2_TVE) );
                    DDB_DTDtrE[i] = Dexp_mid_k_DT * ( q1->_Av_TVE + q0->_B[i]/q1->_Av_TVE * dA_dTrEe );
                                
                    double temp2(0.);
                    STensorOperation::doubleContractionSTensor3(q0->_dBd_dDevEe_TVE,devDE,temp2);
                    for (int k=0; k<3; k++)
                        for (int l=0; l<3; l++){
                            DA_DT[i](k,l) = q0->_A[i](k,l) * Dexp_rec_g_DT + Dexp_mid_g_DT*( q0->_Bd_TVE*devDE(k,l) + 
                                                                                q0->_A[i](k,l) * 1/q0->_Bd_TVE * temp2); 
                            DDA_DTT[i](k,l) = q0->_A[i](k,l) * DDexp_rec_g_DTT + DDexp_mid_g_DTT*( q0->_Bd_TVE*devDE(k,l) + 
                                                                                q0->_A[i](k,l) * 1/q0->_Bd_TVE * temp2); 
                            for (int r=0; r<3; r++)
                                for (int s=0; s<3; s++){
                                    // _Idev is not pre-multiplied !! 
                                    DA_DdevE[i](k,l,r,s) =  exp_mid_g * ( q0->_Bd_TVE * _I4(k,l,r,s) + 1/q0->_Bd_TVE * q0->_A[i](k,l)*q0->_dBd_dDevEe_TVE(r,s) );  
                                    DDA_DTDdevE[i](k,l,r,s) =  Dexp_mid_g_DT * ( q0->_Bd_TVE * _I4(k,l,r,s) + 1/q0->_Bd_TVE * q0->_A[i](k,l)*q0->_dBd_dDevEe_TVE(r,s) );  
                                }
                        }
                 }
                 else if (_ExtraBranch_TVE_option == 2 || _ExtraBranch_TVE_option == 3 || _ExtraBranch_TVE_option == 4 || _ExtraBranch_TVE_option == 5){
                    // Here the derivative are made as: B[i] = Av[i]*B[i]; A[i] = Bd[i]*A[i]; Above, BranchElasticStrain = A[i] + 1/3*B[i]*_I
                    
                    double dTrEei_DT(0.), ddTrEei_DTT(0.); // branchElasticStrain trace
                    static STensor3 dDevEei_DT, ddDevEei_DTT;
                    dTrEei_DT = q0->_B[i] * Dexp_rec_k_DT + Dexp_mid_k_DT*eff_trDE + exp_mid_k*Deff_trDE_DT; // trDE; //NEW
                    ddTrEei_DTT = q0->_B[i] * DDexp_rec_k_DTT + DDexp_mid_k_DTT*eff_trDE + 2.*Dexp_mid_k_DT*Deff_trDE_DT + exp_mid_k*DDeff_trDE_DTT; // trDE; //NEW
                    for (int k=0; k<3; k++)
                        for (int l=0; l<3; l++){
                            dDevEei_DT(k,l) = q0->_A[i](k,l) * Dexp_rec_g_DT + Dexp_mid_g_DT*devDE(k,l);
                            ddDevEei_DTT(k,l) = q0->_A[i](k,l) * DDexp_rec_g_DTT + DDexp_mid_g_DTT*devDE(k,l);
                        }
                            
                    DB_DtrE[i] = exp_mid_k * ( q1->_dAv_dTrEe_TVE_vector[i] * q1->_B[i] + q1->_Av_TVE_vector[i] );
                    DB_DT[i] = q1->_Av_TVE_vector[i]*dTrEei_DT + q1->_dAv_dTrEe_TVE_vector[i]*dTrEei_DT* q1->_B[i];
                    DDB_DTT[i] = q1->_Av_TVE_vector[i]* ddTrEei_DTT + 2.*q1->_dAv_dTrEe_TVE_vector[i]*pow(dTrEei_DT,2) + q1->_dAv_dTrEe_TVE_vector[i]*ddTrEei_DTT*q1->_B[i];
                    DDB_DTDtrE[i] = Dexp_mid_k_DT * ( q1->_dAv_dTrEe_TVE_vector[i] * q1->_B[i] + q1->_Av_TVE_vector[i] ) 
                                    + exp_mid_k * ( 2.*q1->_dAv_dTrEe_TVE_vector[i] * dTrEei_DT );
                                
                    for (int k=0; k<3; k++)
                        for (int l=0; l<3; l++){
                            DA_DT[i](k,l) = q1->_Bd_TVE_vector[i]*dDevEei_DT(k,l); 
                            DDA_DTT[i](k,l) = q1->_Bd_TVE_vector[i]*ddDevEei_DTT(k,l); 
                            
                            if (_ExtraBranch_TVE_option == 3 || _ExtraBranch_TVE_option == 4 || _ExtraBranch_TVE_option == 5){
                                DA_DT[i](k,l) += q1->_A[i](k,l)*q1->_dBd_dTrEe_TVE_vector[i]*dTrEei_DT;
                                DDA_DTT[i](k,l) += q1->_A[i](k,l)*q1->_dBd_dTrEe_TVE_vector[i]*ddTrEei_DTT +
                                                    2*q1->_dBd_dTrEe_TVE_vector[i]*dTrEei_DT*dDevEei_DT(k,l);  
                                
                                // _Idev is not required !! _I is not pre-multiplied !!
                                DA_DtrE[i](k,l) = q1->_dBd_dTrEe_TVE_vector[i] * exp_mid_k * q1->_A[i](k,l); // *_I(r,s);
                                DDA_DTDtrE[i](k,l) = q1->_dBd_dTrEe_TVE_vector[i] * (exp_mid_k*dDevEei_DT(k,l) + Dexp_mid_k_DT*q1->_A[i](k,l)); // *_I(r,s);
                            }                                   
                                    
                            for (int r=0; r<3; r++)
                                for (int s=0; s<3; s++){
                                    DA_DT[i](k,l) += q1->_A[i](k,l)*q1->_dBd_dDevEe_TVE_vector[i](r,s)*dDevEei_DT(r,s); 
                                    DDA_DTT[i](k,l) += 2.*dDevEei_DT(k,l)*q1->_dBd_dDevEe_TVE_vector[i](r,s)*dDevEei_DT(r,s) 
                                                        + q1->_A[i](k,l) * q1->_dBd_dDevEe_TVE_vector[i](r,s)*ddDevEei_DTT(r,s);
                                    
                                    // _Idev is not pre-multiplied !! 
                                    DA_DdevE[i](k,l,r,s) =  exp_mid_g * ( q1->_A[i](k,l)*q1->_dBd_dDevEe_TVE_vector[i](r,s) + q1->_Bd_TVE_vector[i] * _I4(k,l,r,s) );  
                                    DDA_DTDdevE[i](k,l,r,s) =  Dexp_mid_g_DT * ( q1->_A[i](k,l)*q1->_dBd_dDevEe_TVE_vector[i](r,s) + q1->_Bd_TVE_vector[i] * _I4(k,l,r,s) ) 
                                                                + exp_mid_g * ( dDevEei_DT(k,l)* q1->_dBd_dDevEe_TVE_vector[i](r,s));  
                                    for (int p=0; p<3; p++)
                                        for (int q=0; q<3; q++){
                                            DDA_DTDdevE[i](k,l,r,s) += exp_mid_g * q1->_dBd_dDevEe_TVE_vector[i](p,q)*dDevEei_DT(p,q)*_I4(k,l,r,s);


                                            // NEW EIGEN
                                            if ( _useRotationCorrection == true){
                                            	if(_rotationCorrectionScheme == 1 && _rotationCorrectionScheme == 2){
                                            		DA_DdevE[i](k,l,r,s) +=  ( q1->_A[i](k,l)*q1->_dBd_dDevEe_TVE_vector[i](p,q) + q1->_Bd_TVE_vector[i] * _I4(k,l,p,q) ) *
                                                									( exp_rec_g*rotationDer1[i](p,q,r,s) + exp_mid_g*rotationDer2[i](p,q,r,s) );
                                            			DDA_DTDdevE[i](k,l,r,s) += ( q1->_A[i](k,l)*q1->_dBd_dDevEe_TVE_vector[i](p,q) + q1->_Bd_TVE_vector[i] * _I4(k,l,p,q) ) *
                                                    								( Dexp_rec_g_DT*rotationDer1[i](p,q,r,s) + Dexp_mid_g_DT*rotationDer2[i](p,q,r,s) );
                                            	}
                                            }
                                            // NEW EIGEN
                                        }
                                }
                        }
                 } // if - extraBranchTVE_option
                } // all extraBranches
            }
        }
        else{ // if stiff = false, we still need DA_DT, DB_DT for DcorKirDT in mechSource
            std::vector<double>& DB_DT = q1->_DB_DT;
            std::vector<STensor3>& DA_DT = q1->_DA_DT;
                    
            double Dexp_rec_k_DT(0.), Dexp_mid_k_DT(0.);  
            double Dexp_rec_g_DT(0.), Dexp_mid_g_DT(0.);  
            
            double dtk(0.), dtk2(0.), exp_rec_k(0.), exp_mid_k(0.);
            double dtg(0.), dtg2(0.), exp_rec_g(0.), exp_mid_g(0.);
            
            // NEW
            double Deff_trDE_DT = -3.*(DAlphaDT*(T1-_Tinitial) + AlphaT);
            // NEW
            
            for (int i=0; i<_Gi.size(); i++){   
                
                dtk = dt_shift_rec/(_ki[i]);
                dtk2 = dt_shift_mid/(_ki[i]);
                exp_rec_k = exp(-dtk);     // rec
                exp_mid_k = exp(-dtk2); // mid
                
                dtg = dt_shift_rec/(_gi[i]);
                dtg2 = dt_shift_mid/(_gi[i]);
                exp_rec_g = exp(-dtg);     // rec
                exp_mid_g = exp(-dtg2); // mid
            
                Dexp_rec_k_DT = -1/(_ki[i]) * Ddt_shiftDT_rec*exp_rec_k;
                Dexp_mid_k_DT = -1/(_ki[i]) * Ddt_shiftDT_mid*exp_mid_k;
                Dexp_rec_g_DT = -1/(_gi[i]) * Ddt_shiftDT_rec*exp_rec_g;
                Dexp_mid_g_DT = -1/(_gi[i]) * Ddt_shiftDT_mid*exp_mid_g;
                
                if (!_useExtraBranch_TVE){
                    DB_DT[i] = q0->_B[i] * Dexp_rec_k_DT + Dexp_mid_k_DT*eff_trDE + exp_rec_k*Deff_trDE_DT; // trDE; // NEW
                    for (int k=0; k<3; k++)
                        for (int l=0; l<3; l++){
                            DA_DT[i](k,l) = q0->_A[i](k,l) * Dexp_rec_g_DT + Dexp_mid_g_DT*devDE(k,l); 
                        }
                }
                else{ // all extraBranches
                
                 if(_ExtraBranch_TVE_option == 1){
                    DB_DT[i] = q0->_B[i] * Dexp_rec_k_DT + Dexp_mid_k_DT*( (q1->_intAv_1_TVE - q0->_intAv_1_TVE) +
                                                            q0->_B[i]*(q1->_intAv_2_TVE - q0->_intAv_2_TVE) );
                                                            
                    double temp2(0.);
                    STensorOperation::doubleContractionSTensor3(q0->_dBd_dDevEe_TVE,devDE,temp2);
                    for (int k=0; k<3; k++)
                        for (int l=0; l<3; l++){
                            DA_DT[i](k,l) = q0->_A[i](k,l) * Dexp_rec_g_DT + Dexp_mid_g_DT*( q0->_Bd_TVE*devDE(k,l) + 
                                                                                q0->_A[i](k,l) * 1/q0->_Bd_TVE * temp2); 
                        }
                 }
                 else if (_ExtraBranch_TVE_option == 2 || _ExtraBranch_TVE_option == 3 || _ExtraBranch_TVE_option == 4 || _ExtraBranch_TVE_option == 5){
                    // Here the derivative are made as: B[i] = Av[i]*B[i]; A[i] = Bd[i]*A[i]; Above, BranchElasticStrain = A[i] + 1/3*B[i]*_I
                     
                    double dTrEei_DT(0.); // branchElasticStrain trace
                    static STensor3 dDevEei_DT;
                    dTrEei_DT = q0->_B[i] * Dexp_rec_k_DT + Dexp_mid_k_DT*eff_trDE + exp_mid_k*Deff_trDE_DT; // trDE; //NEW
                    for (int k=0; k<3; k++)
                        for (int l=0; l<3; l++)
                            dDevEei_DT(k,l) = q0->_A[i](k,l) * Dexp_rec_g_DT + Dexp_mid_g_DT*devDE(k,l);
                            
                    DB_DT[i] = q1->_Av_TVE_vector[i]*dTrEei_DT + q1->_dAv_dTrEe_TVE_vector[i]*dTrEei_DT* q1->_B[i];
                    for (int k=0; k<3; k++)
                        for (int l=0; l<3; l++){
                            DA_DT[i](k,l) = q1->_Bd_TVE_vector[i]*dDevEei_DT(k,l); 
                            
                            if (_ExtraBranch_TVE_option == 3 || _ExtraBranch_TVE_option == 4 || _ExtraBranch_TVE_option == 5){
                                DA_DT[i](k,l) += q1->_A[i](k,l)*q1->_dBd_dTrEe_TVE_vector[i]*dTrEei_DT;
                            }
                            
                            for (int r=0; r<3; r++)
                                for (int s=0; s<3; s++){
                                    DA_DT[i](k,l) += q1->_A[i](k,l)*q1->_dBd_dDevEe_TVE_vector[i](r,s)*dDevEei_DT(r,s); 
                                }
                        }
            
                 }
       
                } // all extraBranches
            }
        }
        
    }

    // Put Stress in IP
    // Msg::Error(" Inside TVE_predictorCorrector, Ke = %e, Ge = %e !!", Ke, Ge);
    q1->_kirchhoff = devK;
    q1->_kirchhoff(0,0) += p;
    q1->_kirchhoff(1,1) += p;
    q1->_kirchhoff(2,2) += p;
    q1->_trCorKirinf_TVE = p;
}

// ========================================================================================================================================
// Unused Member Functions

double mlawNonLinearTVM::soundSpeed() const {   // Correct this for current temperature
    return sqrt((_K+4.*_G/3.)/_rho);
}
