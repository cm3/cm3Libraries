//
// C++ Interface: material law
//
// Description: mlawThermal Condutivity law
//
//
// Author:  <L. Homsy>, (C) 2015
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "mlawThermalConducter.h"

#include <math.h>
#include "MInterfaceElement.h"


mlawThermalConducter::mlawThermalConducter(const int num,const double alpha, const double beta, const double gamma,
						     const double Kx,const double Ky,const double Kz):
				materialLaw(num,true),_alpha(alpha),_beta(beta),_gamma(gamma),_Kx(Kx),_Ky(Ky),_Kz(Kz)
{
	
    	STensor3 R;		//3x3 rotation matrix

	double c1,c2,c3,s1,s2,s3;
	double s1c2, c1c2;
	double pi(3.14159265359);
    	double fpi = pi/180.;

	c1 = cos(_alpha*fpi);
	s1 = sin(_alpha*fpi);

	c2 = cos(_beta*fpi);
	s2 = sin(_beta*fpi);

	c3 = cos(_gamma*fpi);
	s3 = sin(_gamma*fpi);

	s1c2 = s1*c2;
	c1c2 = c1*c2;

	R(0,0) = c3*c1 - s1c2*s3;
	R(0,1) = c3*s1 + c1c2*s3;
	R(0,2) = s2*s3;

	R(1,0) = -s3*c1 - s1c2*c3;
	R(1,1) = -s3*s1 + c1c2*c3;
	R(1,2) = s2*c3;

	R(2,0) = s1*s2;
	R(2,1) = -c1*s2;
	R(2,2) = c2;

  
		
	STensor3 k;
	// to be unifom in DG3D q= k' gradT with k'=-k instead of q=-kgradT
	k(0,0)  = -_Kx;
	k(1,1)  = -_Ky;
	k(2,2)  = -_Kz;
  	for(int i=0;i<3;i++)
  	 {
    	  for(int j=0;j<3;j++)
    	   {
             _k(i,j)=0.;
             for(int m=0;m<3;m++)
             {
               for(int n=0;n<3;n++)
            	{
              	  _k(i,j)+=R(m,i)*R(n,j)*k(m,n);
       	        }
     	      }
   	    }
	  } 
	
} 
mlawThermalConducter::mlawThermalConducter(const mlawThermalConducter &source) : materialLaw(source),_alpha(source._alpha),_beta(source._beta),_gamma(source._gamma)
                                                                             ,_Kx(source. _Kx),_Ky(source._Ky),_Kz (source._Kz),  _k(source._k)
{

}


mlawThermalConducter& mlawThermalConducter::operator=(const materialLaw &source)
{
  materialLaw::operator=(source);
  const mlawThermalConducter* src =static_cast<const mlawThermalConducter*>(&source);
  if(src !=NULL)
  {
    _alpha = src->_alpha;
    _beta  = src->_beta;
    _gamma = src->_gamma;
    _Kx = src->_Kx;
    _Ky = src->_Ky;
    _Kz = src->_Kz;
    _k = src->_k;
  
  }  
  return *this;
}

void mlawThermalConducter::createIPState(IPStateBase* &ips, bool hasBodyForce,const bool* state_,const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
 /* bool inter=true;
  const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
  if(iele==NULL) inter=false;
  IPVariable* ipvi = new IPThermalConducter();
  IPVariable* ipv1 = new IPThermalConducter();
  IPVariable* ipv2 = new IPThermalConducter();
  if(ips != NULL) delete ips;
  ips = new IP3State(state_,ipvi,ipv1,ipv2);*/
}






