	subroutine KshearB( nn, S, W_ltc, Dtot, Wtot, tau_c
     &			, MF, NR, nn111, gamrlx, Arlx, Brlx, BB1
     &			, alpha_R, sq2, nrlx, icrys, i6, first )
	implicit none
c	===================================================
	include 'LD_param_size.f'
c	===================================================
c	-- 'arguments'
	real*8 nn(3), S(6), W_ltc(3), Dtot(6), Wtot(3)
	real*8 tau_c(2), MF, NR(mxneq,mxneq), nn111(6)
	real*8 gamrlx, Arlx(6), Brlx(6), BB1(9), alpha_R, sq2
	integer icrys
	integer*2 nrlx, i6(3,3)
	logical first
c	::::  'Slip system geometry'
	real*8 A1(6,mxslip,mxcstt), B1(3,mxslip,mxcstt)
	real*8 miller(7,mxslip,mxcstt)
	common /KLP0/ A1, B1, miller
c	-- 'local variables'
	real*8 dSdW(6,3), dSdD(6,6), dWltcdW(3,3), dWltcdD(3,6)
	real*8 Sr(6), dDrdD(6,6), WltcM(3,3), SM(6,3), DtotM(6,3)
	real*8 tmp, tmp2, tmp3, gg(3), vec6(6)
	real*8 tau_sb, W_sb(3), gam_sb, MF_sb, xx(10,2)
	real*8 q(4), R(3,3), sq3, fpi
	integer i, j, k, l
	integer*2 ii, i3(3,3)
c

	end
c
c	**************************************************************
c	******* RICE & ASARO criterion for shear banding
	subroutine K_init_SB( i, xx, S, D, dSdD, dSdW
     &				, tau_c, sq2, i6, i3 )
	implicit none
	real*8 nn(3), gg(3), S(6), D(6)
	real*8 dSdD(6,6), dSdW(6,3), tau_c, sq2
	real*8 ang0(2), ang(2), tt(3), tau_sb, dtau(2), v6(6)
	real*8 Det0, Det, dDet(2), pi, tmp, tmp2, tol, D_c
	real*8 Misf0, Misf, dX(2), D_sb_mx, tau_sb_mx, xx(10,2)
	integer iter, iter2, icrys, nx, nc, iloop
	integer*2 i6(3,3), i3(3,3), i, j, k, l, m, n, nrsb
	real*8 detmin, tsbmx
	common /detmi/ detmin, tsbmx
	logical first, last, deriv, notyet

	return
	end

c	////////////////////////////////
	subroutine Kshear2c( nn, gg, Det, dDet, tau_sb, dtau
     &					, ang, S, dSdD, dSdW
     &					, tol, deriv, sq2, i6, i3 )
	implicit none
	real*8 nn(3), gg(3), Det, dDet(2), tau_sb, dtau(2), tau1
	real*8 ang(2), S(6), dSdD(6,6), dSdW(6,3), tol, sq2, v6(6)
	real*8 sn(2), cn(2), AA(2,2), MM(2,2), dMM(2,2), dsig(6,2)
	real*8 tt(3,2), dtt(3,2), A1(6,2), B1(3,2), dA1(6,2), dB1(3,2)
	real*8 tmp, tmp2, tmp3, Det1, dnn(3), dgg(3), gg1(3), norm_g
	integer*2 i6(3,3), i3(3,3), i, j, k, l, m, n
	logical deriv, last, chkderiv, vlast


	return
	end
c
c	////////////////////////////////
	subroutine Kasaro_rice( nn, gg, Det, dDet, tau_sb, dtau
     &					, ang, S, dSdD, dSdW
     &					, tol, deriv, sq2, i6, i3 )
	implicit none
	real*8 nn(3), gg(3), Det, dDet(2), tau_sb, dtau(2)
	real*8 ang(2), S(6), dSdD(6,6), dSdW(6,3), tol, sq2
	real*8 AA(3,3), dAA(3,3,3), dsig(6)
	real*8 tmp, tmp2, Det1, dDet3(3), sn(2), cn(2), BB(6,3)
	integer*2 i6(3,3), i3(3,3), i, j, k, l, m, n
	logical deriv, last, chkderiv, vlast


	return
	end
c
c	//////////////
	subroutine Kcofac( AA )
	implicit none
	real*8 AA(3,3), MM(3,3)
	integer i, j
	return
	end
c
	subroutine Kalam_fx( nrlx1, vec6, n_alam, R, q, S, S_cr_b
     &				, co20, si20, co30, sq2
     &				, kinc, icrys, ineb, i6, ii )
	implicit none
	include 'LD_param_size.f'
	integer*2 nrlx1
	real*8 vec6(6), n_alam(3), R(3,3,mxneb), q(4,mxneb), S(6,mxneb)
	real*8 S_cr_b(6,mxneb), R_cr2sa(3,3), co20, si20, co30, sq2
	real*8 tmp, tmp2, tmp3
	integer kinc, icrys, ineb, i6(3,3), ii, i, j
	real*8 sq3

	return
	end
