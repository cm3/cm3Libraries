#ifndef HOMOGENIZATION_2RD_H
#define HOMOGENIZATION_2RD_H 1

#ifdef NONLOCALGMSH
#include "material.h"

int homogenize_2rd(int sch, double* DE, MFH::Material* mtx_mat, MFH::Material* icl_mat, double vfM, double vfI, double* ar, double* angles, double* statev_n, double* statev, int nsdv, int idsdv_m, int idsdv_i, MFH::Lcc* LCC, double** Calgo, double* dpdE, double* strs_dDdp_bar,double *Sp_bar, int kinc, int kstep, double dt);


int preNR(double* DE, double* mu0, MFH::Material* mtx_mat, MFH::Material* icl_mat, double vfI, double* ar, double* angles, double* statev_n, double* statev, int idsdv_m, int idsdv_i, MFH::Lcc* LCC, int last, double* F,double** J, int* indx, int* neq, int solver, double** Calgo, double* dpdE, double* strs_dDdp_bar, double *Sp_bar, int kinc, int kstep, double dt);

#else
int homogenize_2rd(int sch, double* DE, Material* mtx_mat, Material* icl_mat, double vfM, double vfI, double* ar, double* angles, double* statev_n, double* statev, int nsdv, int idsdv_m, int idsdv_i, Lcc* LCC, double** Calgo, double* dpdE, double* strs_dDdp_bar,double *Sp_bar, int kinc, int kstep, double dt);


int preNR(double* DE, double* mu0, Material* mtx_mat, Material* icl_mat, double vfI, double* ar, double* angles, double* statev_n, double* statev, int idsdv_m, int idsdv_i, Lcc* LCC, int last, double* F,double** J, int* indx, int* neq, int solver, double** Calgo, double* dpdE, double* strs_dDdp_bar, double *Sp_bar, int kinc, int kstep, double dt);
#endif

double scalar_res(double* F,int neq, double norm);
void solveNR(double* F,double** J, double** invJ, double* DX, int* indx, int Nmax, int neq);
void updateNR(double* X, double* dX, int N, double factor);


#endif
