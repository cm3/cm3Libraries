//
// C++ Interface: material law
//
// Author:  <Van Dung Nguyen>, (C) 2017
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef COHESIVEELASTICPOTENTIAL_H_
#define COHESIVEELASTICPOTENTIAL_H_

#ifndef SWIG
#include "STensor3.h"
#endif //SWIG

class cohesiveElasticPotential{
  #ifndef SWIG
  protected:
    int _num;
  
  public:
    cohesiveElasticPotential(const int num): _num(num){}
    cohesiveElasticPotential(const cohesiveElasticPotential& src): _num(src._num){}
    virtual ~cohesiveElasticPotential(){}
    
    virtual double get(const SVector3& ujump, const SVector3& normal, const double Kp, const double beta) const = 0;
    virtual double getPositiveValue(const SVector3& ujump, const SVector3& normal, const double Kp, const double beta) const = 0;
    virtual double getNegativeValue(const SVector3& ujump, const SVector3& normal, const double Kp, const double beta) const = 0;
    virtual void constitutivePositive(const SVector3& ujump, const SVector3& normal, const double Kp, const double beta, SVector3& T, const bool sitff, STensor3* L) const = 0;
    virtual void constitutiveNegative(const SVector3& ujump, const SVector3& normal, const double Kp, const double beta, SVector3& T, const bool sitff, STensor3* L) const = 0;
    virtual void getEffectiveJump(const SVector3& ujump, const SVector3& normal, const double beta, double& effJump, 
                                    const bool stiff, SVector3* deffJumpdUjump) const = 0;
    
    virtual cohesiveElasticPotential* clone() const = 0;
  #endif //SWIG
};

class linearCohesiveElasticPotential : public cohesiveElasticPotential{
  protected:
  
  public:
    linearCohesiveElasticPotential(const int num);
    #ifndef SWIG
    linearCohesiveElasticPotential(const linearCohesiveElasticPotential& src):cohesiveElasticPotential(src) {}
    virtual ~linearCohesiveElasticPotential(){}
    
    virtual double get(const SVector3& ujump, const SVector3& normal, const double Kp, const double beta) const;
    virtual double getPositiveValue(const SVector3& ujump, const SVector3& normal, const double Kp, const double beta) const;
    virtual double getNegativeValue(const SVector3& ujump, const SVector3& normal, const double Kp, const double beta) const;
    virtual void constitutivePositive(const SVector3& ujump, const SVector3& normal, const double Kp, const double beta, SVector3& T, const bool sitff, STensor3* L) const;
    virtual void constitutiveNegative(const SVector3& ujump, const SVector3& normal, const double Kp, const double beta, SVector3& T, const bool sitff, STensor3* L) const;
    virtual void getEffectiveJump(const SVector3& ujump, const SVector3& normal, const double beta, double& effJump, 
                                  const bool stiff, SVector3* deffJumpdUjump) const;
    
    virtual cohesiveElasticPotential* clone() const {return new linearCohesiveElasticPotential(*this);};
    #endif //SWIG
};


#endif //COHESIVEELASTICPOTENTIAL_H_
