//
// C++ Interface: material law
//              Linear Thermo Mechanical law
// Author:  <L. Homsy>, (C) 2013
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef MLAWSMP_H_
#define MLAWSMP_H_
#include "mlawThermalConducter.h"
#include "../internalPoints/ipSMP.h"
#include "mlaw.h"
#include "STensor3.h"
#include "STensor43.h"
#include "STensor63.h"
#include "ipSMP.h"


class mlawSMP : public  mlawThermalConducter
{
 protected:
 const STensor43 I4;
 const STensor3  I2;
 double _mu_groundState3, _Im3;
 double _pi;
 double _Tr,_mu_groundState2,_Nmu_groundState2, _Im2,_Sgl2,_Sr2,_Delta,_m2, _epsilonr,_n, _epsilonp02 ;
 double _t0;
 double _alphar1,_alphagl1,_Ggl1,_Gr1,_Mgl1,_Mr1,_Mugl1,_Mur1;
 double  _epsilon01, _QglOnKb1, _QrOnKb1, _epsygl1 , _d1, _m1,_VOnKb1, _alphap, _Sa0,_ha1, _b1, _g1, _phia01, _Z1,
	_r1,_s1, _Sb01, _Hgl1, _Lgl1, _Hr1, _Lr1, _l1,_Kb,_be1,_wp,_c0,_c1;
  STensor3 _k0;
  
  STensor3  _alphaDilatation;
  STensor3 _Stiff_alphaDilatation;
  int _order;
  bool _mechanism2;

 public:
   mlawSMP(const int num,const double rho,const double alpha, const double beta, const double gamma ,const double t0, const double Kx,const double Ky, const double Kz,
       const double mu_groundState3,const double Im3, const double pi,const double Tr, const double Nmu_groundState2 ,const double mu_groundState2 ,
       const double Im2,const double Sgl2,const double Sr2,const double Delta,const double m2, const double epsilonr ,const double n,const double epsilonp02,
       const double alphar1,const double alphagl1,const double Ggl1,const double Gr1 ,const double Mgl1,const double Mr1,const double Mugl1,
       const double Mur1,const double epsilon01,const double Qgl1,const double Qr1,const double epsygl1 ,const double d1,
      const double m1,const double V1,const double alphap, const double  Sa0,const double ha1,const double b1,const double g1,
       const double phia01,const double Z1, const double r1,const double s1,const double Sb01,const double Hgl1,const double Lgl1,const double Hr1,
       const double Lr1,const double l1,const double Kb,const double be1,const double c0,const double wp,const double c1);
	#ifndef SWIG
	mlawSMP(const mlawSMP &source);
	mlawSMP& operator=(const materialLaw &source);
	virtual ~mlawSMP(){};
	virtual materialLaw* clone() const {return new mlawSMP(*this);};
  virtual bool withEnergyDissipation() const {return false;};
 //mlawSMP& operator=(const mlawLinearThermoMechanics &source);
  // function of materialLaw
  virtual matname getType() const{return materialLaw::SMP;}  //***** I don't know if it is right??
  // virtual matname getType() const{return mlawLinearThermoMechanics::SMP;}  //***** I don't know if it is right??
  virtual void createIPState(IPStateBase* &ips, bool hasBodyForce,const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const IntPt *GP=NULL, const int gpt =0) const;
  

public:
  void setStrainOrder(const int order){_order = order;};
  virtual void setMechanism2(bool mechanism2) {_mechanism2=mechanism2;};
  double soundSpeed() const;
  virtual double getSa0() const { return _Sa0;}
  virtual double getPhia0() const { return _phia01;}
  virtual double getSastar0() const { return -_b1*_phia01;}
  double getInitialTemperature() const {return _t0;};
  double getExtraDofStoredEnergyPerUnitField(double T) const;
  virtual double getInitialExtraDofStoredEnergyPerUnitField() const {return getExtraDofStoredEnergyPerUnitField(_t0);}
//virtual  void rightcauchydis(const STensor3& Fn,STensor3& Cedis);
  virtual void constitutive(
                            const STensor3& F0,         // initial deformation gradient (input @ time n)
                            const STensor3& Fn,         // updated deformation gradient (input @ time n+1)
                            STensor3 &P,                // updated 1st Piola-Kirchhoff stress tensor (output)
                            const IPSMP *q0,       // array of initial internal variable
                            IPSMP *q1,              // updated array of internal variable (in ipvcur on output),
			    STensor43 &Tangent,         // constitutive tangents (output)
			    const bool stiff, 
                            STensor43* elasticTangent = NULL, 
                            const bool dTangent =false,
                            STensor63* dCalgdeps = NULL
			    ) const;
  virtual void constitutive(
                            const STensor3& F0,         // initial deformation gradient (input @ time n)
                            const STensor3& Fn,         // updated deformation gradient (input @ time n+1)
                            STensor3 &P,                // updated 1st Piola-Kirchhoff stress tensor (output)
                                               // contains the initial values on input
                            const IPSMP *q0,       // array of initial internal variable
                            IPSMP *q1,              // updated array of internal variable (in ipvcur on output),
			    STensor43 &Tangent,         // constitutive tangents (output)
                            double T0,
			    double T,
  			    const SVector3 &gradT,
                            SVector3 &fluxT,
                            STensor3 &dPdT,
                            STensor3 &dqdgradT,
                            SVector3 &dqdT,
                            STensor33 &dqdF,
			    const bool stiff,
			    double &w,
			    double &dwdt,
			    STensor3 &dwdf
			    ) const;
 void constitutive1(
                            const STensor3& F0,         // initial deformation gradient (input @ time n)
                            const STensor3& Fn,         // updated deformation gradient (input @ time n+1)
                            STensor3 &P1,                // updated 1st Piola-Kirchhoff stress tensor (output)
                                               // contains the initial values on input
                            const IPSMP *q0,       // array of initial internal variable
                            IPSMP *q1,              // updated array of internal variable (in ipvcur on output),
			    STensor43 &Tangent1,         // constitutive tangents (output)
                            double T0,
			    double T,
                            STensor3 &dPdT,
			    const bool stiff,
			    const double Tg,
		             const STensor3 &dTgdF , double &depsitau1,STensor3 &dtauepsilon2dF,double &ddepsilon1dt
			    ) const;
 void constitutive2(
                            const STensor3& F0,         // initial deformation gradient (input @ time n)
                            const STensor3& Fn,         // updated deformation gradient (input @ time n+1)
                            STensor3 &P2,                // updated 1st Piola-Kirchhoff stress tensor (output)
                                                           // contains the initial values on input
                            const IPSMP *q0,       // array of initial internal variable
                            IPSMP *q1,              // updated array of internal variable (in ipvcur on output),
			    STensor43 &Tangent2,         // constitutive tangents (output)
                            double T,
                            STensor3 &dPdT,
			    const bool stiff,
			    const double Tg,
		            const STensor3 &dTgdF,double &depsitau2,STensor3 &dtauepsilon2dF,double &dtauepsilon2dt
			    ) const ;
 void constitutive3(
                            const STensor3& F0,         // initial deformation gradient (input @ time n)
                            const STensor3& Fn,         // updated deformation gradient (input @ time n+1)
                            STensor3 &P3,                // updated 1st Piola-Kirchhoff stress tensor (output)
                                                         // contains the initial values on input
                            const IPSMP *q0,       // array of initial internal variable
                            IPSMP *q1,              // updated array of internal variable (in ipvcur on output),
			    STensor43 &Tangent3,         // constitutive tangents (output)
                            double T,
                            STensor3 &dPdT,
			    const bool stiff
			    ) const;
			    //const STensor3&  getConductivityTensor() const { return _k;};
			    const STensor3&  getInitialConductivityTensor() const { return _k0;};
			    			    				
			    virtual const STensor3& getStiff_alphaDilatation()const { return _Stiff_alphaDilatation;};


 protected:
  virtual double deformationEnergy(double y) const;
  virtual double deformationEnergy(const STensor3& Cebar2) const;
  virtual double deformationEnergy(const STensor3& lnCepr1, const double G1,const double K1,const double alalphath,double T)const;
 /* virtual double deformationEnergy(STensor3 defo) const ;
  virtual double deformationEnergy(STensor3 defo, const STensor3& sigma) const ;*/
  virtual void  Gmu_2(const double T,const double Tg, double &Gmu_groundState2_) const;
  virtual void Secpiola2(const STensor3 &Ce_,const double Gmu_groundState2_,const double invJ2third_, STensor3 &SP2_) const;
  virtual void Mandel2(const STensor3 &Ce,const STensor3 &SP2, STensor3 &M2) const;
  virtual void Kirco(const STensor3 &Fn,const STensor3 &SP2, STensor3 &Kir2) const;
  virtual void Tau2(const STensor3 &M2,double &Ta2) const;
  virtual void delta_epsilonp2( const double &Ta2_,double dh_, double St2_,double &depsilon2_) const;
  virtual void delta_DP2(const STensor3 &M2_,const double &Ta2_,const double &depsilon2_,STensor3 &dDp2_) const;
  virtual void fSt2(const double T,const double Tg, double &St2_,double &dSt2dT,STensor3 &dSt2dF, const STensor3 &dTgdF)const;
  //virtual void glasstrans(const STensor3& F0,const STensor3& Fn,double T,double &Tg)const;
  //virtual void logSTensor3(const STensor3& a, STensor3 &loga, STensor43 *dloga=NULL, STensor63 *ddloga=NULL) const;
  //virtual void expSTensor3(const STensor3 &a,STensor3 &expa,STensor43 *dexpa=NULL,STensor63 *ddexpa=NULL) const;
  // Use STensorOperation:: instead
  virtual void matricetovectoromega(const STensor3 &om, fullVector<double> &Omega2) const;
  virtual void vectortomatriceomega(STensor3 &o, const fullVector<double> &Omega2) const;
  virtual void vectortomatricedomega( STensor43 &domega2dM24ord,const fullMatrix<double> &domega2dM2)const;

  virtual void matricetovectordomega(const STensor43 &domega2dM24ord,fullMatrix<double> &domega2dM2)const;
  virtual void computeResidual2( STensor3 &ome2,const STensor3 &Cebar2,const STensor3 &M2,
				const STensor3 &dDp2,const STensor3 &Cepr2,const double Gmu_groundState2/*,const STensor3 expdDp2*/) const;
  virtual void computeJacobian2(STensor43 &domega2dM24ord, const STensor3 &Ce2,const STensor3 &M2,
				const STensor3 &dDp2,const STensor3 &Cepr2,const double Gmu_groundState2,const double St2,
				double dh,const double invJ2third/*,const STensor3 expdDp2,const STensor43 &dexpdDp2*/) const;
  virtual void predictorCorrector2(  fullVector<double> &Omega2, fullMatrix<double> &domega2dM2inv,  STensor3 &M2,STensor3 &SP2,double &Ta2,double &depsilon2,
				    STensor3 &dDp2,double &invJ2third, const STensor3& F0, const STensor3& Fn, STensor3 &P2, const IPSMP *q0, IPSMP *q1,
				    double T,const double Tg,double &Gmu_groundState2,bool stiff /* ,STensor43 &Le    */ ,STensor3 &Fe2
				    ,STensor3 &Cebar2,STensor3 &Cebarpr2,const double dh, STensor3 &Fp2,const double St2/*,STensor3 &expdDp2,STensor43 *dexpdDp2*/) const;
  virtual void tangent2(STensor43 &Tangent2,/*STensor43 &dFp2invdF,*/ const STensor3 &Fn, const STensor3 &Fp2,const STensor3 &SP2,const double Gmu_groundState2
			, const STensor43 &dFp2dF, const STensor3 &Fe2,  const STensor3 &Cebar2, const double invJ2third,const double T,const double Tg,const STensor43 &dSP2dF) const;
  virtual void computeInternalVariabledfpdf2(STensor43 &dFp2dF,const fullMatrix<double> &domega2dM2inv, const STensor3 &Fn, const STensor3 &Fppr2,
				const STensor3  &SP2,const double Gmu_groundState2, const STensor3 &M2,const STensor3 &dDp2 ,  const STensor3 &Cebar2,
				const double invJ2third,const STensor3 &Cebarpr2, const double dh,const double St2,const double Ta2,const double depsilon2,STensor3 &dtauepsilon2dF,
				const STensor3 &dTgdF,const double T, const double Tg,const STensor3 &dSt2dF,STensor43 &dSP2dF) const;
  virtual void computedPdT2(STensor3 &dp2dt,const fullMatrix<double> &domega2dM2inv, const STensor3 &Fn,/*,const STensor3 &Fp0,*/ const STensor3 &Fp2
			  ,const STensor3 &Fp0,const STensor3  &SP2, const double Gmu_groundState2, const STensor3 &M2,const STensor3 &dDp2 ,  const STensor3 &Cebar2,
			    const STensor3 &Ceprbar2,const double dh,const double St2,const double T,const double Tg,const double invJ2third
			    ,double &dtauepsilon2dt,const double Ta2,const double depsilon2,double dSt2dT ) const;

  virtual void predictorCorrector1( double &j1,  double &TauE1,double &depsilon1,STensor3 &dDp1, double &Sa1,double &Sb1, double &phia1, double &phiastar1,
				   double &Sastar1,const double G1,const double K1 ,const double Mu1,const STensor3& F0,const STensor3& Fn, STensor3 &P1, const IPSMP *q0,
				   IPSMP *q1,const double T0,const double T,const double Tg,bool stiff,STensor3 &Fe1,STensor3 &Ce1,STensor3 &Cepr1,const double dh, STensor3 &Fp1,
				   STensor3 &C1,const double Hb,STensor43 &dexpdDp1,STensor3 &N1,double &Tau1 ,STensor3 &Me1,const double Q1,const double L1/*,STensor43 &dFp1dF,
				   STensor43 &dMe1dF*/,const double alalphath, double &dSa1dTg, double &dSa1dT) const;
  virtual void deltaepsilon1(const double TauE1,const double Q1,const double T,const double Tg,const double dh,double &depsilon1) const;
  virtual void computeResidual1( double &ome1,const STensor3 &lnCepr1,const double L1,const double Sa1,const double Sb1,const double depsilon1,const double T,
				  const double Tg,const double K1,const double G1,const double Q1,const double dh,const double alalphath) const;
  virtual void computeSaphi(const double depsilon1 ,const double Sa0,const double phai0,const double T,const double Tg,
			      const double dh,double &phaistar0,double &Sastar0,double &Sa1,double &Sastar1,double &phai1,double &phaistar1,double &dSa1depsilon, double &dSa1dTg, double &dSa1dT)const;
  virtual void computeJacobian1(double &j1 , const double depsilon1,const double T, const double Tg,
				const double G1,const double L1, const double dSa1depsilon) const;
  virtual void computeM1(const STensor3& devlnCepr1,const STensor3& lnCepr1,const double depsilon1, const STensor3 &N1,const double T ,const double Tg,
			const double G1,const double Mu1, const double K1,STensor3 &Me1,const double dh,const double alalphath)const;
  virtual void computeTauE(const STensor3 &Me1,const STensor3 &devlnCepr1,const STensor3 &lnCepr1,const double depsilon1,const double G1,const double K1,const double Sa1,
			  const double Sb1,const double T ,const double Tg, double &TauE1,double &tau1,const double alalphath)const;
  virtual void tangent1(STensor43 &Tangent1,const STensor43 &dMe1dF,const STensor3 &Fn, const STensor3 &Fp1,const STensor3 &Me1,
			 const STensor43 &dFp1dF, const STensor3 &Fe1,  const STensor3 &Ce1) const;
  virtual void computeInternalVariabledfpdf1(STensor43 &dFp1dF,STensor43 &dMe1dF,const double j1, const STensor3 &Fn, const STensor3 &Fppr1, const STensor3 &Cepr1 ,const STensor3 &N1,
				const double dh,const double depsilon1,const STensor43 &dexpdDp1,const double T, const double Tg,const double G1,const double K1,
				const STensor3 &C1,const double Hb,STensor3 &dtauepsilondF,const double Tau1,const STensor3& F0,const double alalphath,const double Q1,
				 const double TauE1,const STensor3 &dTgdF,const STensor3 &dG1dF,const STensor3 &dMu1dF,const STensor3 &dK1dF,const STensor3 &dQ1dF
				 ,const STensor3 &dHbdF,const STensor3 &dthermalalphadF,const double L1,const STensor3 &dL1dF,const STensor3 &Me1, const double dSa1dTg) const;
 virtual void computedPdT1(STensor3 &dp1dt,const double j1, const STensor3 &Fn,const STensor3 &Fp1,const STensor3 &Fppr1,const STensor3  &Me1, const STensor3 &dDp1 ,
			   const STensor3 &Ce1,const STensor3 &Cepr1,const double dh,const double T,const double Tg,const double depsilon1,const STensor43 &dexpdDp1,
			   const double G1,const double K1,const double Mu1,const STensor3 C1,const double Hb ,const double Q1,/*const double L1,*/
			   const STensor3 &N1,const double alalphath,double &dtauepsilon1dt,const double Tau1,const double dG1dt,const double dMu1dt,const double dK1dt,const double dHbdt,
			   const double dalphadt,const double L1,const double dQ1dt,const double dL1dt,const STensor3 &Fe1, const double dSa1dT) const;
  virtual void computeThermalAlpha(const double T ,const double Tg, double &thermalalpha,double &dthermalalphadt, double &dthermalalphadtg)const;
  //virtual void derivativethermalalpha(const double T ,const double Tg, double &dthermalalphadt)const;
  virtual void heatcapacity(double &d2epsi1dt2, const STensor3 &N1,const STensor3 &Cepr1,const double T,const double Tg,
			   const double G1,const double K1,const double Mu1,const double depsilon1 ) const {};
  virtual void glassTrans(const STensor3& F0,const STensor3& Fn,const double T,double &Tg,STensor3& dTgdF)const;
  //virtual void derivativethermalalphadtg(const double T ,const double Tg, double &dthermalalphadtg)const;
  virtual void computeParameter1(const double T ,const double Tg,const STensor3 &dTgdF,bool stiff,double &G1,double &Mu1, double &K1,double &Hb,double &Q1,STensor3 &dG1dF,STensor3 &dMu1dF,
				STensor3 &dK1dF,STensor3 &dQ1dF,STensor3 &dHbdF,double &dG1dt,double &dMu1dt,double &dK1dt,double &dHbdt,const double dthermalalphadtg
				,STensor3 &dthermalalphaddF,double &dQ1dt,double &L1,double &dL1dt,STensor3 &dL1dF,const double dh)const;

 #endif // SWIG
};

#endif // MLAWSMP_H_
