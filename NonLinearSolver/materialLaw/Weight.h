#ifndef WEIGHT_PHASE_H  
#define WEIGHT_PHASE_H 1

void getClosure(double a1, double a2, double a3, double A[3][3][3][3]);
void getLinearClosure(double a[3][3], double Al[3][3][3][3], bool is2D);
void getQuadraticClosure(double a[3][3], double Aq[3][3][3][3]);
void getHybridClosure(double a[3][3], double Al[3][3][3][3], bool is2D);
double getODF(double p[3], double bofa[3][3], double bigBofa[3][3][3][3], bool is2D);
double generateWeight(int N, double **a, double **probDensityList, double **facetList,  double **thetaList, double **phiList);
double evaluateErrorGaussian(int fixAngleInd, double fixAngleVar, bool theta, double va, double esp, double **odf, double *angleList, double *dangleList, int nbFacetGIden);
void getARWeight(double theta, double phi, int Total_number_of_AR_per_phase, double *weightAR, double *ARList);

#endif

// Add by MOHAMEDOU Mohamed 
