//
// C++ Interface: Nucleation criterion
//
// Description: Provide criterion class for nucleation 
//
//
// Author:  <J. Leclerc>, (C) 2019
//
// Copyright: See COPYING file that comes with this distribution
//
//
//
#ifndef _NUCLEATIONCRITERIONLAW_H_
#define _NUCLEATIONCRITERIONLAW_H_
#ifndef SWIG
#include "ipNucleationCriterion.h"
#include "STensor3.h"
class IPNonLocalPorosity;
class mlawNonLocalPorosity;
#endif




/*! \class NucleationCriterionLaw
 * This mother class defines the interface for the nucleation criterion.
 * If the criterion is implicit, the nucleation activation is verified during the step.
 * Otherwise, if it is explicit, the nucleation cirterion is evaluated at the end of the time step. 
 */
class NucleationCriterionLaw
{
public :
  //! type enumeration of the nucleation criterion
  enum nucleationCriterionName{ trivialCriterion,        //!< Trivial case (always true)
                                plasticBasedCriterion,   //!< Activation once a plastic strain value is reached
                                porosityBasedCriterion,  //!< Activation once a porosity threeshold value is reached
                                bereminCriterion         //!< Activation following the Beremin criterion
                              };
#ifndef SWIG
protected :
  //! identifier of the internal law in the vector of the nucleation manager.
  int num_;
  
  //! is true once the internal law is correctly initialised.
  bool isInitialized_;
  
  //! vector containing the index of law that can be inhibited.
  //! Every element should be positive and different of num.
  std::vector<int> index_inihibitableLaw_;
  
  //! pointer to the corresponding mlawPorous.
  //! (but the real object is not contained inside the law).
  const mlawNonLocalPorosity* mlawPorous_;


  //! @name Constructors and destructors
private :
  //! Default constructor is always forbidden.
  NucleationCriterionLaw();
  
public :
  NucleationCriterionLaw( const int num );
  NucleationCriterionLaw( const NucleationCriterionLaw& source );
  NucleationCriterionLaw& operator=( const NucleationCriterionLaw& source );
  virtual ~NucleationCriterionLaw(){};
  virtual NucleationCriterionLaw* clone() const = 0;
  //! @}
  
  //! @name General function of materialLaw
  virtual int getNum() const{ return num_; };
  virtual const bool isInitialized() { return isInitialized_;};
  virtual nucleationCriterionName getType() const = 0;
  virtual void createIPVariable( IPNucleationCriterion* &ipNuclCriterion ) const = 0;
  //! @}


  //! @name Setting functions used in internal interface
  //! It corresponds to non-const functions used outside SWIG interface,
  //! to set or exchange internal parameters or initialise the laws.
  
  //! Set the number of the nucleation law.
  virtual void setNum( const int num);

  //! Initialise the criterion (set mlaw pointer and check for inconsitencies).
  virtual void initCriterionLaw( const mlawNonLocalPorosity* const mlawPorous );
  //! @}
  
#endif // SWIG
  //! @name Setting functions used in user interface
  //! It corresponds to non-const functions used inside SWIG interface,
  //! Add an exclusion rule.
  virtual void addAnExclusionRule( const int new_index );
  //! @}
#ifndef SWIG
  
  //! @name Access functions
  //! return the vector containing the index of law to block
  virtual const std::vector<int>& getIndexOfExclusionRules() const;
  //! @}


  //! @name Specific functions
protected :
  //! Check the parameters integrity. If an inconsistency is detected, send back an error and a boolean == false.
  //! Executed at the initialisation.
  virtual const bool checkParametersIntegrity() const;
  
  //! Evaluate directly and simply (but only) the criterion value (a positive value means that the criterion is about to be onset or activated)
  //! regardless the IPv. However, the IPV is yet not modified. (the nucleation manager will have the final decision).
  //! "criterionValue" value should be the order of magnitude of the plastic strain increment.
  virtual void assessNucleationOnset( double& criterionValue, IPNonLocalPorosity& q1Porous ) const = 0;
  
public :
  //! Send back the suited criterion value at the end of a time step (positive value = the criterion should be activated) 
  //! by taking into account the IPV status.
  virtual void assessNucleationActivation( double& criterionValue, IPNonLocalPorosity& q1Porous, const IPNonLocalPorosity& q0Porous ) const = 0;
  //! Update the IPV when the nucleation has to end. (i.e. when the end criterion is reached).
  virtual void updateIfNucleationEnds( IPNucleationCriterion& q1i_Criterion, const IPNonLocalPorosity& q1Porous ) const = 0;
  //! Apply the effects of this criterion activtion on other criterion.
  virtual void applyCriterionOnsetInIP( IPNonLocalPorosity& q1Porous ) const;
  //! Reset the variables / refresh the ipv to the adequate values at the begining of a time step / iteration.
  virtual void previousBasedRefresh( IPNucleationCriterion& q1i_Criterion, const IPNucleationCriterion& q0i_Criterion ) const = 0;
  //! @}
  
#endif // SWIG
};


/*! \class TrivialNucleationCriterionLaw
 * This class is the default criterion: nucleation is always activated.
 */
class TrivialNucleationCriterionLaw : public NucleationCriterionLaw
{
protected :
  // Nothing is needed

public :
  //! @name Constructors and destructors
  TrivialNucleationCriterionLaw( const int num );
#ifndef SWIG
  TrivialNucleationCriterionLaw( const TrivialNucleationCriterionLaw& source );
  TrivialNucleationCriterionLaw& operator=( const NucleationCriterionLaw& source );
  virtual ~TrivialNucleationCriterionLaw(){};
  virtual NucleationCriterionLaw * clone() const { return new TrivialNucleationCriterionLaw( *this ); };
  //! @}

  //! @name General function of materialLaw
  virtual nucleationCriterionName getType() const{ return NucleationCriterionLaw::trivialCriterion; };
  virtual void createIPVariable( IPNucleationCriterion* &ipNuclCriterion ) const;
  //! @}
  
  //! @name Specific functions
protected :
  virtual const bool checkParametersIntegrity() const;
  
  //! This function should is not called by the actual version of "assessNucleationActivation" to save time.
  virtual void assessNucleationOnset( double& criterionValue, IPNonLocalPorosity& q1Porous ) const;

public :
  virtual void assessNucleationActivation( double& criterionValue, IPNonLocalPorosity& q1Porous, const IPNonLocalPorosity& q0Porous ) const;
  virtual void updateIfNucleationEnds( IPNucleationCriterion& q1i_Criterion, const IPNonLocalPorosity& q1Porous ) const;
  virtual void previousBasedRefresh( IPNucleationCriterion& q1i_Criterion, const IPNucleationCriterion& q0i_Criterion ) const;
  //! @}
  
#endif // SWIG
};




/*! \class TrivialNucleationCriterionLaw
 * This class is the default criterion: nucleation is always activated.
 */
class NonTrivialNucleationCriterionLaw : public NucleationCriterionLaw
{
protected :
  // Nothing is needed
  
#ifndef SWIG

public :
  //! @name Constructors and destructors
  NonTrivialNucleationCriterionLaw( const int num );
  NonTrivialNucleationCriterionLaw( const NonTrivialNucleationCriterionLaw& source );
  NonTrivialNucleationCriterionLaw& operator=( const NucleationCriterionLaw& source );
  virtual ~NonTrivialNucleationCriterionLaw(){};
  virtual NucleationCriterionLaw * clone() const = 0;
  //! @}

  //! @name General function of materialLaw
  virtual nucleationCriterionName getType() const = 0;
  virtual void createIPVariable( IPNucleationCriterion* &ipNuclCriterion ) const = 0;
  //! @}
  
  //! @name Specific functions
protected :
  virtual const bool checkParametersIntegrity() const = 0;
  virtual void assessNucleationOnset( double& criterionValue, IPNonLocalPorosity& q1Porous ) const = 0;
  
public :
  virtual void assessNucleationActivation( double& criterionValue, IPNonLocalPorosity& q1Porous, const IPNonLocalPorosity& q0Porous ) const;
  virtual void updateIfNucleationEnds( IPNucleationCriterion& q1i_Criterion, const IPNonLocalPorosity& q1Porous ) const = 0;
  inline virtual void previousBasedRefresh( IPNucleationCriterion& q1i_Criterion, const IPNucleationCriterion& q0i_Criterion ) const
  {
    // Transfer data commun to each derived-ipv.
    q1i_Criterion.setNucleationActivation( q0i_Criterion.hasNucleationActivated() );
    q1i_Criterion.setNucleationOnset( q0i_Criterion.hasNucleationOnset() );
    q1i_Criterion.setNucleationInhibition( q0i_Criterion.isInhibited() );
  };
  //! @}

#endif // SWIG
};






/*! \class PlasticBasedNucleationCriterionLaw
 * This class is plastic strain based criterion: 
 * nucleation is activated during an interval defined 
 * in terms of equivalent plastic strain.
 */
class PlasticBasedNucleationCriterionLaw : public NonTrivialNucleationCriterionLaw
{
#ifndef SWIG
protected :
  //! Strain value at which nucleation starts
  double startingNuclStrain_;
  //! Strain value at which nucleation ends
  double endingNuclStrain_;
  
#endif // SWIG


public :
  //! @name Constructors and destructors
  PlasticBasedNucleationCriterionLaw( const int num, const double startingNuclStrain, const double endingNuclStrain);
#ifndef SWIG
  PlasticBasedNucleationCriterionLaw( const PlasticBasedNucleationCriterionLaw& source );
  PlasticBasedNucleationCriterionLaw& operator=( const NucleationCriterionLaw& source );
  virtual ~PlasticBasedNucleationCriterionLaw(){};
  virtual NucleationCriterionLaw * clone() const { return new PlasticBasedNucleationCriterionLaw( *this ); };
  //! @}
  
  //! @name General function of materialLaw
  virtual nucleationCriterionName getType() const{ return NucleationCriterionLaw::plasticBasedCriterion; };
  virtual void createIPVariable( IPNucleationCriterion* &ipNuclCriterion ) const;
  //! @}
  
  //! @name Specific functions
protected :
  virtual const bool checkParametersIntegrity() const;
  virtual void assessNucleationOnset( double& criterionValue, IPNonLocalPorosity& q1Porous ) const;
public :
  virtual void updateIfNucleationEnds( IPNucleationCriterion& q1i_Criterion, const IPNonLocalPorosity& q1Porous ) const;
  
  //! @}
  
#endif // SWIG
};




/*! \class PorosityBasedNucleationCriterionLaw
 * This class is plastic strain based criterion: 
 * nucleation is activated during an interval defined 
 * in terms of porosity value.
 */
class PorosityBasedNucleationCriterionLaw : public NonTrivialNucleationCriterionLaw
{
#ifndef SWIG
protected :
  //! Porosity value at which nucleation starts
  double startingNuclPorosity_;
  //! Porosity value at which nucleation ends
  double endingNuclPorosity_;

#endif // SWIG


public :
  //! @name Constructors and destructors
  PorosityBasedNucleationCriterionLaw( const int num, const double startingNuclPorosity, const double endingNuclPorosity);
#ifndef SWIG
  PorosityBasedNucleationCriterionLaw( const PorosityBasedNucleationCriterionLaw& source );
  PorosityBasedNucleationCriterionLaw& operator=( const NucleationCriterionLaw& source );
  virtual ~PorosityBasedNucleationCriterionLaw(){};
  virtual NucleationCriterionLaw * clone() const { return new PorosityBasedNucleationCriterionLaw( *this ); };
  //! @}
  
  //! @name General function of materialLaw
  virtual nucleationCriterionName getType() const{ return NucleationCriterionLaw::porosityBasedCriterion; };
  virtual void createIPVariable( IPNucleationCriterion* &ipNuclCriterion ) const;
  //! @}
  
  //! @name Specific functions
protected :
  virtual const bool checkParametersIntegrity() const;
  virtual void assessNucleationOnset( double& criterionValue, IPNonLocalPorosity& q1Porous ) const;
public :
  virtual void updateIfNucleationEnds( IPNucleationCriterion& q1i_Criterion, const IPNonLocalPorosity& q1Porous ) const;
  //! @}
  
#endif // SWIG
};




/*! \class BereminNucleationCriterionLaw
 * This class is based on Beremin criterion: 
 * Nucleation is allowed if n.(sigma + k(sigma_eq - sigma_y0)).n > n.sigma_c.n
 * (This criterion is identical either in Kirchhoff and in Sigma stress and 
 * is compatible with both formulations).
 */
class BereminNucleationCriterionLaw : public NonTrivialNucleationCriterionLaw
{
#ifndef SWIG
protected :
  //! Stress threshold value at which nucleation occurs
  STensor3 _criticalStress;
  
  //! Stress concentration factor represented under a tensor form
  //! in initial material axis (an arbitrary rotation can be applied using the setting options)
  STensor3 _stressConcentrationTensor;
  
  STensor3 dirAmplification_;

#endif // SWIG


public :
  //! @name Constructors and destructors
  BereminNucleationCriterionLaw( const int num, 
                                 const double sigma_cx, const double sigma_cy, const double sigma_cz, 
                                 const double kx, const double ky, const double kz );
  
#ifndef SWIG
  BereminNucleationCriterionLaw( const BereminNucleationCriterionLaw& source );
  BereminNucleationCriterionLaw& operator=( const NucleationCriterionLaw& source );
  virtual ~BereminNucleationCriterionLaw(){};
  virtual NucleationCriterionLaw* clone() const;
  //! @}
  
  //! @name General function of materialLaw
  virtual nucleationCriterionName getType() const{ return NucleationCriterionLaw::bereminCriterion;};
  virtual void createIPVariable(IPNucleationCriterion* &ipNuclCriterion ) const;
  //! @}
  
  //! @name Setting functions used in user interface
#endif // SWIG
  virtual void setdirAmplification(const double kx, const double ky, const double kz);

  //! Applied a rotation on the material tensor using euler angles (in degrees)
  virtual void rotateMaterialAxes( const double alpha, const double beta, const double gamma );
#ifndef SWIG
  //! @}
  
  //! @name Specific functions
protected :
  virtual const bool checkParametersIntegrity() const;
  virtual void assessNucleationOnset( double& criterionValue, IPNonLocalPorosity& q1Porous ) const;
public :
  virtual void updateIfNucleationEnds( IPNucleationCriterion& q1i_Criterion, const IPNonLocalPorosity& q1Porous ) const;
  
  inline virtual void previousBasedRefresh( IPNucleationCriterion& q1i_Criterion, const IPNucleationCriterion& q0i_Criterion ) const
  {
    // Transfer data commun to each derived-ipv.
    q1i_Criterion.setNucleationActivation( q0i_Criterion.hasNucleationActivated() );
    q1i_Criterion.setNucleationOnset( q0i_Criterion.hasNucleationOnset() );
    q1i_Criterion.setNucleationInhibition( q0i_Criterion.isInhibited() );
    q1i_Criterion.setAmpli( q0i_Criterion.getAmpli()) ;
  };
  
  //! @}
  
  
#endif // SWIG
};


#endif //_NUCLEATIONCRITERIONLAW_H_
