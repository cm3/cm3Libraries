#ifndef CLENGTH_H
#define CLENGTH_H 1

#ifdef NONLOCALGMSH
namespace MFH {
#endif

//Generic Characteristic length
class Clength{

	protected:
		int clengthmodel;
		int nclprops;
		int nsdv;
		
	public:
		
		Clength(double* props, int idclength){
			clengthmodel = (int) props[idclength];
		}
		virtual ~Clength(){ ; }
		inline int get_clengthmodel(){  return clengthmodel; }
		inline int get_nsdv() {	return nsdv; } 
		
		//print the clength properties
		virtual void print()=0;

		//compute the characteristic length
		//INPUT: strs_n, pos_p 
		//OUTPUT: cg characteristic length matrix
		virtual void creat_cg(double *statev_n, int pos_p, double** cg)=0;

};

//Zero Characteristic length, local model
class Zero_Cl : public Clength{

	private:
		
	public:
		
		Zero_Cl(double* props, int idclength);
		~Zero_Cl();
		
		virtual void print();
		virtual void creat_cg(double *statev_n, int pos_p, double** cg);

		
		
}; 



//Isotropic charateristic length
class Iso_Cl : public Clength{

	private:
		double cl;

	public:
		
		Iso_Cl(double* props, int idclength);
		~Iso_Cl();
		
		virtual void print();
		virtual void creat_cg(double *statev_n, int pos_p, double** cg);

		
		
}; 



//Anisotropic characteristic length
class Aniso_Cl : public Clength{

	private:
		double* cl;
                double* euler;

	public:
		
		Aniso_Cl(double* props, int idclength);
		~Aniso_Cl();
		
		virtual void print();
		virtual void creat_cg(double *statev_n, int pos_p, double** cg);
		
		
}; 




//changable Isotroipic charateristic length 
// moled description according to Marc Grees (CMAME 1998)

class IsoV1_Cl : public Clength{

	private:
		double cl;
                double e_l, n_l;

	public:
		
		IsoV1_Cl(double* props, int idclength);
		~IsoV1_Cl();
		
		virtual void print();
		virtual void creat_cg(double *statev_n, int pos_p, double** cg);
		
		
}; 



//changable anisotroipic charateristic length 
class AnisoV1_Cl : public Clength{

	private:
		double* cl;
                double* euler;
                double e_l, n_l;

	public:
		
		AnisoV1_Cl(double* props, int idclength);
		~AnisoV1_Cl();
		
		virtual void print();
		virtual void creat_cg(double *statev_n, int pos_p, double** cg);
		
		
}; 

//changable anisotroipic charateristic length 
class Stoch_Cl : public Clength{

	private:
		double* cl;

	public:
		
		Stoch_Cl(double* props, int idclength);
		~Stoch_Cl();
		
		virtual void print();
		virtual void creat_cg(double *statev_n, int pos_p, double** cg);
		
		
}; 
////////////////////////////////////////////////
//////////end of new defined ling Wu 25/02/2013.
////////////////////////////////////////////////




#ifdef NONLOCALGMSH
}
#endif


#endif
