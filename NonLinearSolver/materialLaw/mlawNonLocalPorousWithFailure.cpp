// C++ Interface: material law
//
// Description: general class for nonlocal porous material law
//
// Author:  <V.D. Nguyen>, (C) 2018
//
// Copyright: See COPYING file that comes with this distribution
//
//

#include "mlawNonLocalPorousWithFailure.h"
#include "ipNonLocalPorosity.h"
#include "nonLinearMechSolver.h"

mlawNonLocalPorousWithCleavageFailure::mlawNonLocalPorousWithCleavageFailure(mlawNonLocalPorosity& law, const CLengthLaw &cLLaw,
                                const DamageLaw &damLaw, const double sigmac): materialLaw(law.getNum(),true){
  _nonlocalPorousLaw = &law;
  _failureCriterion = new maximalPrincipleStressWithIPNonLocalPorosity(law.getNum(),sigmac);
  _cLLawCleavage = cLLaw.clone();
  _damLawCleavage = damLaw.clone();
};

mlawNonLocalPorousWithCleavageFailure::mlawNonLocalPorousWithCleavageFailure(mlawNonLocalPorosity& law, const CLengthLaw &cLLaw,
                                const DamageLaw &damLaw, const FailureCriterionBase& failureLaw): materialLaw(law.getNum(),true){
  _nonlocalPorousLaw = &law;
  _failureCriterion = failureLaw.clone();
  _cLLawCleavage = cLLaw.clone();
  _damLawCleavage = damLaw.clone();
};

mlawNonLocalPorousWithCleavageFailure::mlawNonLocalPorousWithCleavageFailure(const mlawNonLocalPorousWithCleavageFailure& src): materialLaw(src),_nonlocalPorousLaw(src._nonlocalPorousLaw){
  if (src._failureCriterion != NULL){
    _failureCriterion = src._failureCriterion->clone();
  }
  else{
    _failureCriterion = NULL;
  }
  if(src._cLLawCleavage != NULL) {
    _cLLawCleavage=src._cLLawCleavage->clone();
  }
  else{
    _cLLawCleavage = NULL;
  }
  if(src._damLawCleavage != NULL){
    _damLawCleavage=src._damLawCleavage->clone();
  }
  else{
    _damLawCleavage = NULL;
  }
}
mlawNonLocalPorousWithCleavageFailure::~mlawNonLocalPorousWithCleavageFailure(){
  if (_failureCriterion != NULL) {delete _failureCriterion; _failureCriterion = NULL;};
  if (_damLawCleavage!=NULL) {delete _damLawCleavage; _damLawCleavage=NULL;};
  if (_cLLawCleavage!=NULL) {delete _cLLawCleavage; _cLLawCleavage=NULL;}
}

void mlawNonLocalPorousWithCleavageFailure::createIPState(IPStateBase* &ips,bool hasBodyForce, const bool* state_,const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
  Msg::Error("this function does not used");
};

void mlawNonLocalPorousWithCleavageFailure::createIPVariable(const double fInit, IPNonLocalPorosity* &ipv) const
{
  if (ipv!=NULL) delete ipv;
  ipv = new IPNonLocalPorosityWithCleavage(fInit,_nonlocalPorousLaw->getJ2IsotropicHardening(),
                                              _nonlocalPorousLaw->getCLengthLawVector(),
                                              _nonlocalPorousLaw->getNucleationLaw(),
                                              _nonlocalPorousLaw->getCoalescenceLaw(),
                                              _nonlocalPorousLaw->getVoidEvolutionLaw(),
                                              getCleavageCLengthLaw(),getCleavageDamageLaw());
}

void mlawNonLocalPorousWithCleavageFailure::initLaws(const std::map<int,materialLaw*> &maplaw) {
  _nonlocalPorousLaw->initLaws(maplaw);
};
    

void mlawNonLocalPorousWithCleavageFailure::checkInternalState(IPVariable* ipv, const IPVariable* ipvprev) const {
  _nonlocalPorousLaw->checkInternalState(ipv,ipvprev);


  IPNonLocalPorosityWithCleavage* porousipv = dynamic_cast<IPNonLocalPorosityWithCleavage*>(ipv);
  const IPNonLocalPorosityWithCleavage* porousipvprev = dynamic_cast<const IPNonLocalPorosityWithCleavage*>(ipvprev);
  if (porousipvprev->getConstRefToPassOnsetCleavageState()){
    porousipv->getRefToPassOnsetCleavageState() = true;
  }
  else{
    // checck cleavage
    if (_failureCriterion->isFailed(ipvprev,ipv)){
      porousipv->getRefToPassOnsetCleavageState() = true;
    }
    else{
      porousipv->getRefToPassOnsetCleavageState() = false;
    }
  }
};

void mlawNonLocalPorousWithCleavageFailure::predictorCorrector(const STensor3& F0,         // initial deformation gradient (input @ time n)
                              const STensor3& Fn,         // updated deformation gradient (input @ time n+1)
                              STensor3 &P,                // updated 1st Piola-Kirchhoff stress tensor (output)
                                                          // contains the initial values on input
                              const IPNonLocalPorosityWithCleavage *q0,       // array of initial internal variable
                              IPNonLocalPorosityWithCleavage *q1,             // updated array of internal variable (in ipvcur on output),
                              STensor43 &Tangent,         // constitutive tangents (output)
                              std::vector<STensor3>  &dLocalVarDF,
                              std::vector<STensor3>  &dStressDNonLocalVar,
                              fullMatrix<double>    &dLocalVarDNonLocalVar,
                              const bool stiff) const
                              {
/// \todo Why the predictor and not the constitutive function of porous law ? should be better no ?
                                
  const int numNonlocalPorous = _nonlocalPorousLaw->getNumNonLocalVariables();
  static STensor43 dFedF;
  static std::vector<STensor3> dFeDNonLocalVar(numNonlocalPorous);
  static STensor3 Peff;
  _nonlocalPorousLaw->predictorCorrector(F0,Fn,Peff,q0,q1,Tangent,dLocalVarDF,dStressDNonLocalVar,dLocalVarDNonLocalVar,stiff,dFedF,dFeDNonLocalVar);

  static STensor3 DplasticMatrixDF;
  static std::vector<double> DplasticMatrixDNonLocalVar(numNonlocalPorous);

  // from plasticEnergy_current = plasticEnergy_previous+ (1-f0)*yield*DeltaHatP
  double f0 = q1->getInitialPorosity();
  double DeltaHatP = q1->getLocalMatrixPlasticStrain()- q0->getLocalMatrixPlasticStrain();
  // no viscoplastic effect
  double DplasticEnergyDHatP = q1->getConstRefToDPlasticEnergyDMatrixPlasticStrain();

  DplasticMatrixDF = q1->getConstRefToDPlasticEnergyDF();
  DplasticMatrixDF *= (1./DplasticEnergyDHatP);
  for (int i=0; i< numNonlocalPorous; i++){
    DplasticMatrixDNonLocalVar[i] = q1->getConstRefToDPlasticEnergyDNonLocalVariable(i)/DplasticEnergyDHatP;
  };

  // compute local var
  q1->getRefToCleavageMatrixPlasticStrain() = q0->getConstRefToCleavageMatrixPlasticStrain();
  if (q1->getConstRefToPassOnsetCleavageState()){
    q1->getRefToCleavageMatrixPlasticStrain() += DeltaHatP;
  }

  // compute clength
  double p0 = q0->getLocalMatrixPlasticStrain();
  _cLLawCleavage->computeCL(p0, q1->getRefToIPCLengthCleavage());

  // compute Fe
  static STensor3 Fe, Fpinv;
  const STensor3 &Fp  = q1->getConstRefToFp();
  STensorOperation::inverseSTensor3(Fp,Fpinv);
  STensorOperation::multSTensor3(Fn,Fpinv,Fe);

  double ene = q1->defoEnergy();

  // compute Damage
  _damLawCleavage->computeDamage(q1->getConstRefToNonLocalCleavageMatrixPlasticStrain(),
                        q0->getConstRefToNonLocalCleavageMatrixPlasticStrain(),
                        ene, Fe, Fp, Peff, _nonlocalPorousLaw->getCel(),
                        q0->getConstRefToIPDamageCleavage(),q1->getRefToIPDamageCleavage());

  // stress
  double D = q1->getConstRefToIPDamageCleavage().getDamage();
  double D0 = q0->getConstRefToIPDamageCleavage().getDamage();
	P = Peff;
  P*=(1.-D);

  q1->getRefToElasticEnergy()*= (1.-D);
  double dPlasticDisp = q1->plasticEnergy() - q0->plasticEnergy();
  q1->getRefToPlasticEnergy() = q0->plasticEnergy() + (1.-D)*dPlasticDisp;
  q1->getRefToDamageEnergy() =  q0->damageEnergy() + ene*(D-D0);

  if (stiff){
    static STensor3 DDamageDF;
    static std::vector<double> DDamageDNonLocalVar(numNonlocalPorous+1);
    STensorOperation::zero(DDamageDF);
    for (int i=0; i<3; i++){
      for (int j=0; j<3; j++){
        for (int k=0; k<3; k++){
          for (int l=0; l<3; l++){
            DDamageDF(i,j) += q1->getConstRefToIPDamageCleavage().getConstRefToDDamageDFe()(k,l)*dFedF(k,l,i,j);
          }
        }
      }
    }

    for (int inl=0; inl< numNonlocalPorous; inl++){
      DDamageDNonLocalVar[inl] = 0.;
      for (int k=0; k<3; k++){
        for (int l=0; l<3; l++){
          DDamageDNonLocalVar[inl] += q1->getConstRefToIPDamageCleavage().getConstRefToDDamageDFe()(k,l)*dFeDNonLocalVar[inl](k,l);
        }
      }
    }
    DDamageDNonLocalVar[numNonlocalPorous] = q1->getConstRefToIPDamageCleavage().getDDamageDp();


    // tangent stress
    Tangent*=(1.-D);
    for(int i=0;i<3;i++){
      for(int j=0;j<3;j++){
        for(int k=0;k<3;k++){
          for(int l=0;l<3;l++){
            Tangent(i,j,k,l)-=Peff(i,j)*DDamageDF(k,l);
          }
        }
      }
    }

    // tangent nonlocal stress
    STensorOperation::zero(dStressDNonLocalVar[numNonlocalPorous]);
    for (int inl=0; inl< numNonlocalPorous+1; inl++){
      dStressDNonLocalVar[inl]*= (1-D);
      STensorOperation::axpy(dStressDNonLocalVar[inl],Peff,-DDamageDNonLocalVar[inl]);
    }
    // tangent local
    if (q1->getConstRefToPassOnsetCleavageState()){
      dLocalVarDF[numNonlocalPorous] = DplasticMatrixDF;
    }
    else{
      STensorOperation::zero(dLocalVarDF[numNonlocalPorous]);
    }

    // tangent local nonlocal
    if (q1->getConstRefToPassOnsetCleavageState()){
      for (int i=0; i< numNonlocalPorous; i++){
        dLocalVarDNonLocalVar(numNonlocalPorous,i) = DplasticMatrixDNonLocalVar[i];
      }
    }
    else{
      for (int i=0; i< numNonlocalPorous; i++){
        dLocalVarDNonLocalVar(numNonlocalPorous,i) = 0.;
      }
    }
  }
};
