//
//
// Description: elastic constitutive behavior
//
// Author:  <Van Dung NGUYEN>, (C) 2010
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef ELASTICLAW_H_
#define ELASTICLAW_H_
#include "STensor3.h"
#include "STensor43.h"
#include "STensor33.h"
#include "STensor63.h"
#include "STensor53.h"


class elasticLawBase{
  public:
		enum ELASTIC_TYPE{NONE = -1, LINEAR_ELASTIC=0,NEO_HOOKEAN =1,BI_LOGARITHMIC=2,
                      LINEAR_ELASTIC_SECOND_ORDER=3, LINEAR_MINDLIN=4,
                      LINEAR_FIRST_SECOND=5};
  #ifndef SWIG
  public:
    elasticLawBase(){};
    elasticLawBase(const elasticLawBase& src){};
    virtual elasticLawBase& operator =( const elasticLawBase& src) {return *this;};

    virtual ~elasticLawBase(){};
    virtual void setParameter(const std::string parameter, const double mu) = 0;
    virtual double getParameter(const std::string parameter) const = 0;
		virtual elasticLawBase::ELASTIC_TYPE getType()const =0;
		virtual elasticLawBase* clone() const = 0;
  #endif
};

class elasticLaw : public elasticLawBase {
	#ifndef SWIG
  protected:
    mutable std::map<std::string,double> _matData;
    mutable bool _isComputed, _isModified;

  public:
    elasticLaw() : _isComputed(false), _isModified(false),elasticLawBase(){}
    elasticLaw(const elasticLaw& src) :elasticLawBase(src),_isComputed(src._isComputed),
                  _isModified(src._isModified),_matData(src._matData){}
    virtual elasticLaw& operator= (const elasticLawBase& src){
      elasticLawBase::operator=(src);
      const elasticLaw* psrc = dynamic_cast<const elasticLaw*>(&src);
      if (psrc != NULL){
        _isComputed =psrc->_isComputed;
        _isModified = psrc->_isModified;
        _matData = psrc->_matData;
      }
      return *this;
    }
    virtual ~elasticLaw(){}
    virtual void setParameter(const std::string parameter, const double mu);
    virtual double getParameter(const std::string parameter) const;
  	virtual elasticLawBase::ELASTIC_TYPE getType()const =0;
  	virtual elasticLawBase* clone() const = 0;

  	// elastic constitutive law
  	virtual void constitutive(const STensor3& F, STensor3& P, STensor43& L, const bool stiff, const bool dTangent=false, STensor63 *dCm=NULL) const=0;

	#endif
};

class elasticSecondOrderLaw: public elasticLawBase{
  #ifndef SWIG
  protected:
    mutable std::map<std::string,double> _matData;

  public:
    elasticSecondOrderLaw():elasticLawBase(){}
    elasticSecondOrderLaw(const elasticSecondOrderLaw& src):elasticLawBase(src),_matData(src._matData){}
    virtual elasticSecondOrderLaw& operator =(const elasticLawBase& src){
      elasticLawBase::operator=(src);
      const elasticSecondOrderLaw* psrc = dynamic_cast<const elasticSecondOrderLaw*>(&src);
      if (psrc){
        _matData = psrc->_matData;
      }
      return *this;
    };

    virtual ~elasticSecondOrderLaw(){}
    virtual void setParameter(const std::string parameter, const double val);
    virtual double getParameter(const std::string parameter) const;
		virtual elasticLawBase::ELASTIC_TYPE getType()const =0;
		virtual elasticLawBase* clone() const=0;

		// elastic constitutive law
		virtual void constitutive(const STensor33& G, STensor33& Q, STensor63& J, const bool stiff) const = 0;
  #endif
};

class elasticFirstSecondLaw : public elasticLawBase{
  #ifndef SWIG
  protected:
    mutable std::map<std::string,double> _matData;

  public:
    elasticFirstSecondLaw():elasticLawBase(){}
    elasticFirstSecondLaw(const elasticFirstSecondLaw& src):elasticLawBase(src),_matData(src._matData){}
    virtual elasticFirstSecondLaw& operator =(const elasticLawBase& src){
      elasticLawBase::operator=(src);
      const elasticFirstSecondLaw* psrc = dynamic_cast<const elasticFirstSecondLaw*>(&src);
      if (psrc){
        _matData = psrc->_matData;
      }
      return *this;
    };

    virtual ~elasticFirstSecondLaw(){}
    virtual void setParameter(const std::string parameter, const double val);
    virtual double getParameter(const std::string parameter) const;
		virtual elasticLawBase::ELASTIC_TYPE getType()const =0;
		virtual elasticLawBase* clone() const=0;

		virtual void constitutive(const STensor3& F, const STensor33& G,
                              STensor3& P, STensor33& Q,
                              STensor53& JFG, STensor53& JGF, const bool stiff) const = 0;

  #endif
};


class linearElasticLaw : public elasticLaw{
  #ifndef SWIG
  public:
    linearElasticLaw();
    linearElasticLaw(const double K, const double mu);
    linearElasticLaw(const linearElasticLaw& src);
    virtual linearElasticLaw& operator = (const elasticLawBase& src){
      elasticLaw::operator=(src);
      return *this;
    }
    virtual ~linearElasticLaw(){};
		virtual elasticLawBase::ELASTIC_TYPE getType()const{return elasticLawBase::LINEAR_ELASTIC;};
		virtual elasticLawBase* clone() const{return new linearElasticLaw(*this);};

		 virtual void constitutive(const STensor3& F, STensor3& P, STensor43& L, const bool stiff, const bool dTangent=false, STensor63 *dCm=NULL) const;
  #endif
};

class NeoHookeanElasticLaw : public elasticLaw{
  #ifndef SWIG
  protected:
    void getStress(const STensor3& F,STensor3& P) const;
		void getConstitutiveTangent(const STensor3& F, STensor43& L) const;

  public:
    NeoHookeanElasticLaw();
    NeoHookeanElasticLaw(const double K, const double mu, const double tol = 1e-6);
    NeoHookeanElasticLaw(const NeoHookeanElasticLaw& src);
    virtual NeoHookeanElasticLaw& operator=(const elasticLawBase& src){
      elasticLaw::operator=(src);
      return *this;
    }
    virtual ~NeoHookeanElasticLaw(){}
		virtual elasticLawBase::ELASTIC_TYPE getType()const {return elasticLawBase::NEO_HOOKEAN;};
		virtual elasticLawBase* clone() const{return new NeoHookeanElasticLaw(*this);};

		virtual void constitutive(const STensor3& F, STensor3& P, STensor43& L, const bool stiff, const bool dTangent=false, STensor63 *dCm=NULL) const;
  #endif
};

class biLogarithmicElasticLaw : public elasticLaw{
  #ifndef SWIG
  public:
    biLogarithmicElasticLaw();
    biLogarithmicElasticLaw(const double K, const double mu);
    biLogarithmicElasticLaw(const biLogarithmicElasticLaw& src);
    virtual biLogarithmicElasticLaw& operator =( const elasticLawBase& src){
      elasticLaw::operator=(src);
      return *this;
    }
    virtual ~biLogarithmicElasticLaw(){};
		virtual elasticLawBase::ELASTIC_TYPE getType()const{return elasticLawBase::BI_LOGARITHMIC;};
		virtual elasticLawBase* clone() const{return new biLogarithmicElasticLaw(*this);};

		virtual void constitutive(const STensor3& F, STensor3& P, STensor43& L, const bool stiff, const bool dTangent=false, STensor63 *dCm=NULL) const;
	#endif
};

/**
// linear elastic second order law
// energy density: w = a1*Gjji*Gkki
                      +a2*Gkii*Gjjk
                      +a3*Gkii*Gkjj
                      +a4*Gijk*Gijk
                      +a5*Gijk*(Gjki+Gkji)

 **/

class linearElasticSecondOrderLaw : public elasticSecondOrderLaw{
  #ifndef SWIG
  inline int delta(const int i, const int j) const {if (i==j) return 1; else return 0;}

  public:
    linearElasticSecondOrderLaw();
    linearElasticSecondOrderLaw(const double a1, const double a2, const double a3, const double a4, const double a5);
    linearElasticSecondOrderLaw(const linearElasticSecondOrderLaw& src);
    virtual linearElasticSecondOrderLaw& operator =(const elasticLawBase& src){
      elasticSecondOrderLaw::operator=(src);
      return *this;
    }
    virtual ~linearElasticSecondOrderLaw(){};
		virtual elasticLawBase::ELASTIC_TYPE getType()const{return elasticLawBase::LINEAR_ELASTIC_SECOND_ORDER;};
		virtual elasticLawBase* clone() const {return new linearElasticSecondOrderLaw(*this);};

		virtual void constitutive(const STensor33& G, STensor33& Q, STensor63& J, const bool stiff) const;
  #endif
};

class linearElasticMindlinLaw : public elasticSecondOrderLaw{
  #ifndef SWIG
  inline int d(const int i, const int j) const {if (i==j) return 1; else return 0;}
  inline int e(const int i, const int j, const int k) const {
    if (i==0 and j==1 and k==2) return 1;
    else if (i==1 and j==2 and k==0) return 1;
    else if (i==2 and j==0 and k==1) return 1;
    else if (i==0 and j==2 and k==1) return -1;
    else if (i==1 and j==0 and k==2) return -1;
    else if (i==2 and j==1 and k==0) return -1;
    else return 0;
  }

  public:
    linearElasticMindlinLaw();
    linearElasticMindlinLaw(const double E, const double le);
    linearElasticMindlinLaw(const linearElasticMindlinLaw& src);
    virtual linearElasticMindlinLaw& operator=(const elasticLawBase& src){
      elasticSecondOrderLaw::operator=(src);
      return *this;
    }
    virtual ~linearElasticMindlinLaw(){};

    virtual elasticLawBase::ELASTIC_TYPE getType()const{return elasticLawBase::LINEAR_MINDLIN;};
		virtual elasticLawBase* clone() const {return new linearElasticMindlinLaw(*this);};

		virtual void constitutive(const STensor33& G, STensor33& Q, STensor63& J, const bool stiff) const;

  #endif
};

/**
  elastic first-second law
  w = bi*Ejk*Gijk
  Eij = 0.5*(Fij+Fji) - delta_ij
  bk is a vector related to length scale
**/

class linearElasticFirstSecondLaw : public elasticFirstSecondLaw{
   #ifndef SWIG
  protected:
    inline int delta(int i, int j) const {if (i==j) return 1; else return 0;}
  public:
    linearElasticFirstSecondLaw() :elasticFirstSecondLaw(){};
    linearElasticFirstSecondLaw(const linearElasticFirstSecondLaw& src): elasticFirstSecondLaw(src){};
    virtual linearElasticFirstSecondLaw& operator=(const elasticLawBase& src){
      elasticFirstSecondLaw::operator=(src);
      return *this;
    }
    virtual ~linearElasticFirstSecondLaw(){};

    virtual elasticLawBase::ELASTIC_TYPE getType()const{return elasticLawBase::LINEAR_FIRST_SECOND;};
		virtual elasticLawBase* clone() const {return new linearElasticFirstSecondLaw(*this);};


    virtual void constitutive(const STensor3& F, const STensor33& G,
                              STensor3& P, STensor33& Q,
                              STensor53& JFG, STensor53& JGF, const bool stiff) const;
   #endif
};

#endif // ELASTICLAW_H_
