// C++ Interface: material law
//              Generic Residual Strain law
// Author:  <mohib>, (C) 2024
//
// Copyright: See COPYING file that comes with this distribution
//
#include "mlawGenericRM.h"
#include <math.h>
#include "MInterfaceElement.h"
#include "extraVariablesAtGaussPoints.h"


mlawGenericRM::mlawGenericRM(const int num, const char* inputname, const bool init) :
//mlawGenericRM::mlawGenericRM(const int num, const std::string inputname, const bool init) :
        materialLaw(num, init), _extraVariablesAtGaussPoints()
{
    // Mechanical constitutive law @Mohib
    _mecaLaw=NULL;
    // Name of file containing Fres to be imposed @Mohib
    //const std::string empty = " ";
    inputfilename = inputname;
    setFileExtraInputsGp();
    fillExtraInputAtGp();
    // Residual Deformation Grad Field stored as a scaler function
    // Enables applying Fres gradually as a Ramp or any other suitable 
    // function. Here its initialized to a unit step function @Mohib
    _FRes = new constantScalarFunction(1.);

}

void mlawGenericRM::setLawForFRes(const scalarFunction& funcFRes)
{
    if (_FRes != NULL) delete _FRes;
    _FRes = funcFRes.clone();
}

mlawGenericRM::mlawGenericRM(const mlawGenericRM& source) : materialLaw(source), _mecaLaw(NULL),
_extraVariablesAtGaussPoints(source._extraVariablesAtGaussPoints)
{
    inputfilename = source.inputfilename;
    if(source._mecaLaw!=NULL)
    {
        if(_mecaLaw != NULL)
        {
            delete _mecaLaw;
            _mecaLaw = NULL;
        }
        _mecaLaw=source._mecaLaw->clone();
    }
    _FRes = NULL;
    if(source._FRes)
        _FRes = source._FRes->clone();
}

mlawGenericRM& mlawGenericRM::operator=(const materialLaw& source)
{
    //mlawCoupledThermoMechanics::operator=(source); //TODO: @Mohib
    const mlawGenericRM* src =static_cast<const mlawGenericRM*>(&source);
    if(src !=NULL)
    {
        if(src->_mecaLaw!=NULL)
        {
            if(_mecaLaw != NULL)
            {
                _mecaLaw->operator=(dynamic_cast<const materialLaw& >(*(src->_mecaLaw)));
            }
            else
              _mecaLaw=src->_mecaLaw->clone();
        }

        if (_FRes!= NULL) delete _FRes; _FRes = NULL;
        if (src->_FRes != NULL) _FRes = src->_FRes->clone();
        inputfilename = src->inputfilename; //TODO @Mohib
        //inputfilename = src->inputname;
        _extraVariablesAtGaussPoints=src->_extraVariablesAtGaussPoints;
    }
    return *this;
}

//createIPVariable cf mlawTFA
void mlawGenericRM::createIPState(IPStateBase* &ips,bool hasBodyForce, const bool* state_,const MElement *ele, const int nbFF_,
                   const IntPt *GP, const int gpt) const
{

  bool inter=true;
  const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
  if(iele==NULL) inter=false;

  static SPoint3 pt_Global;
  ele->pnt(GP->pt[0],GP->pt[1],GP->pt[2],pt_Global);
  static SVector3 GaussP;
  GaussP[0] = pt_Global[0];
  GaussP[1] = pt_Global[1];
  GaussP[2] = pt_Global[2];
  

  // get the value? Reads the file inputfilename and get the closest point @mohib do it in a function 
  std::vector<double> value_ExtraInput;
  bool assign_flag = true;
  for(int i=0; i<9;i++)
  {
    if(_extraVariablesAtGaussPoints.isStored(i))
    {
      value_ExtraInput.push_back(_extraVariablesAtGaussPoints.returnFieldAtClosestLocation(i,GaussP));
      assign_flag = true;
    }
    else
    {
      assign_flag = false;
    }

  }
  
  STensor3 FRes;
  if(assign_flag)
  {
    int counter = 0;
    for(int i=0; i<3; i++){
        for(int j=0; j<3; j++){
            FRes(i,j) = value_ExtraInput[counter];
            counter++;
        }
    }
  }
  else
  {
    FRes(0,0) = 1.0;
    FRes(1,1) = 1.0;
    FRes(2,2) = 1.0;
  }


  if(ips != NULL) delete ips;
  IPGenericRM *ipvi=new IPGenericRM(FRes);
  IPGenericRM *ipv1=new IPGenericRM(FRes);
  IPGenericRM *ipv2=new IPGenericRM(FRes);

  IPStateBase* ipsmeca=NULL;
  getConstRefMechanicalMaterialLaw().createIPState(ipsmeca,hasBodyForce, state_,ele, nbFF_,GP, gpt);
  std::vector<IPVariable*> allIP;
  ipsmeca->getAllIPVariable(allIP);

  ipvi->setIpMeca(*allIP[0]->clone());
  ipv1->setIpMeca(*allIP[1]->clone());
  ipv2->setIpMeca(*allIP[2]->clone());

  ips = new IP3State(state_,ipvi,ipv1,ipv2);
  delete ipsmeca;
}

void mlawGenericRM::createIPVariable(IPGenericRM* &ipv, bool hasBodyForce, const MElement *ele, const int nbFF,
                                     const IntPt *GP, const int gpt) const
{
  bool inter=true;
  const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
  if(iele==NULL) inter=false;

  static SPoint3 pt_Global;
  ele->pnt(GP->pt[0],GP->pt[1],GP->pt[2],pt_Global);
  static SVector3 GaussP;
  GaussP[0] = pt_Global[0];
  GaussP[1] = pt_Global[1];
  GaussP[2] = pt_Global[2];
  

  // get the value? Reads the file     inputfilename and get the closest point @mohib do it in a function
  std::vector<double> value_ExtraInput;
  bool assign_flag = true;
  for(int i=0; i<9;i++)
  {
    if(_extraVariablesAtGaussPoints.isStored(i))
    {
      value_ExtraInput.push_back(_extraVariablesAtGaussPoints.returnFieldAtClosestLocation(i,GaussP));
      assign_flag = true;
    }
    else
    {
      assign_flag = false;
    }

  }

  STensor3 FRes;
  if (assign_flag)
  {
    int counter = 0;
    for(int i=0; i<3; i++){
        for(int j=0; j<3; j++){
            FRes(i,j) = value_ExtraInput[counter];
            counter++;
        }
    }
  }
  else
  {
    FRes(0,0) = 1.0;
    FRes(1,1) = 1.0;
    FRes(2,2) = 1.0;
  }

  if(ipv != NULL)
      delete ipv;
  ipv = new IPGenericRM(FRes);

  IPStateBase* ipsmeca=NULL;
  const bool state_ = true;
  getConstRefMechanicalMaterialLaw().createIPState(ipsmeca,hasBodyForce, &state_,ele, nbFF,GP, gpt);

  std::vector<IPVariable*> allIP;
  ipsmeca->getAllIPVariable(allIP);

  ipv->setIpMeca(*allIP[0]->clone());
  delete ipsmeca;
}

void mlawGenericRM::constitutive(            
            const STensor3      &F0,                   // initial deformation gradient (input @ time n)
            const STensor3      &Fn,                   // updated deformation gradient (input @ time n+1)
            STensor3            &P,                    // updated 1st Piola-Kirchhoff stress tensor (output)
            const IPVariable    *q0,                   // array of initial internal variable
            IPVariable          *q1,                   // updated array of internal variable (in ipvcur on output),
            STensor43           &Tangent,              // constitutive tangents (output)
            const bool          stiff,
            STensor43           *elasticTangent,//TODO:@Mohib
            const bool dTangent,
            STensor63 *dCalgdeps //TODO:@Mohib

                               ) const
{
    if(_mecaLaw == NULL)
        Msg::Error("Mechanic law is null");

    STensorOperation::zero(Tangent);

    const double dh= getTimeStep();
    const double time=getTime();


    const IPGenericRM &qRM0= dynamic_cast<const IPGenericRM &> (*q0);
    IPGenericRM &qRM1= dynamic_cast<IPGenericRM &> (*q1);

    static STensor3 FRes,FRes0,FResInv,FResInvT,FRes0Inv;
    static STensor43 dFResdFn;
    STensorOperation::zero(FRes);
    STensorOperation::zero(FResInv);
    STensorOperation::zero(FResInvT);
    STensorOperation::zero(dFResdFn);
    STensorOperation::zero(FRes0);
    STensorOperation::zero(FRes0Inv);

    const STensor3 I2(1.0);
    const STensor43 I4(2.0,0.0);

    FRes = qRM1.getConstRefToFRes();
    double scaleFactor=_FRes->getVal(time);//TODO: @Mohib
    FRes-= I2;
    FRes*=scaleFactor;
    FRes+=I2;
    STensorOperation::inverseSTensor3(FRes, FResInv);
    STensorOperation::transposeSTensor3(FResInv, FResInvT);

    FRes0 = qRM0.getConstRefToFRes();
    double time0=time;
    if(getTimeStep()>0)
     time0 -= getTimeStep();    
    double scaleFactor0=_FRes->getVal(time0);//TODO: @Mohib
    FRes0-= I2;
    FRes0*=scaleFactor0;
    FRes0+=I2;
    STensorOperation::inverseSTensor3(FRes0, FRes0Inv);


    for (int i=0; i<3; i++){
        for (int j=0; j<3; j++){
            for (int k=0; k<3; k++){
                for (int m=0; m<3; m++){
                    for (int n=0; n<3; n++){
                        dFResdFn(i,k,m,n) += FRes(i,j) * I2(j,m) * I2(k,n); /// to chnage to reflect order
                    }
                }
            }
        }
    }
    // need to updqte kt etc...
    const double JacRes = FRes.determinant();


    const IPVariableMechanics& qMeca0 = dynamic_cast<const IPGenericRM&>(*q0).getConstRefToIpMeca();
    IPVariableMechanics& qMecan = dynamic_cast<IPGenericRM&>(*q1).getRefToIpMeca();


    //Fmeca from F and Fthermal (possible using reference F?) for 0 and n
    static STensor3 Fmeca, Fmeca0;
 
    // Fmeca from F and FRes
    STensorOperation::multSTensor3(Fn,FRes, Fmeca);
    // Fmeca from F and FRes
    STensorOperation::multSTensor3(F0,FRes0, Fmeca0);

    static STensor43 dFmecadF;
    STensorOperation::zero(dFmecadF);

    for (int i=0; i<3; i++){
        for (int j=0; j<3; j++){
            for (int k=0; k<3; k++){
                for(int l = 0; l < 3; ++l){
                    dFmecadF(i,j,k,l)=I2(i,k)*FResInv(l,j);
                }
            }
        }
    }

    static STensor3 Pmeca;
    static STensor43 TangentMeca;
    static STensor43 elasticTangentMeca;
    static STensor63 dTangentMecadF;

    STensorOperation::zero(Pmeca);
    STensorOperation::zero(TangentMeca);
    STensorOperation::zero(elasticTangentMeca);
    STensorOperation::zero(dTangentMecadF);

    _mecaLaw->constitutive(Fmeca0, Fmeca, Pmeca,
                           &qRM0.getConstRefToIpMeca(),
                           &qRM1.getRefToIpMeca(),
                           TangentMeca, stiff,
                           &elasticTangentMeca, dTangent,  &dTangentMecadF);


    // P, Tangent etc..; from Pmeca and Fth and Fref

    // get P
    STensorOperation::multSTensor3(Pmeca, FResInvT, P);

    // get Tangent
    if(stiff){
        for(int i=0; i<3; i++)
        {
            for(int J=0; J<3; J++)
            {
                for(int k=0; k<3; k++)
                {
                    for(int L=0; L<3; L++)
                    {
                        Tangent(i,J,k,L)=0.0;
                        // if (*elasticTangent != 0)
                        //   elasticTangent(i,J,k,L)=0.0;
                          
                        for(int M=0; M<3; M++)
                        {
                            for(int o=0; o<3; o++)
                            {
                                for(int P=0; P<3; P++)
                                {
                                    Tangent(i,J,k,L)+=TangentMeca(i,M,o,P)*dFmecadF(o,P,k,L)*FResInvT(M,J);
                                    // if (elasticTangent!=0)
                                    //    elasticTangent(i,J,k,L)=*elasticTangentMeca(i,M,o,P)*dFmecadF(o,P,k,L)*FResInvT(M,J);;

                                }
                            }
                        }
                    }
                }
            }
        }
    }
        // if(dTangent)
        // {
        //   for(int i=0; i<3; i++)
        //   {
        //     for(int J=0; J<3; J++)
        //     {
        //         for(int k=0; k<3; k++)
        //         {
        //             for(int L=0; L<3; L++)
        //             {

        //               for(int r=0; r<3; r++)
        //               {
        //                  for(int S=0; S<3; S++)
        //                  {
        //                     *dCalgdeps(i,J,k,L,r,S)=0.;

                          
        //                 for(int M=0; M<3; M++)
        //                 {
        //                     for(int o=0; o<3; o++)
        //                     {
        //                         for(int P=0; P<3; P++)
        //                         {
        //                             *dCalgdeps(i,J,k,L,r,S)+=dTangentMecadF(i,M,k,L,o,P)*dFmecadF(o,P,r,S)*FResinvT(M,J); //to check
 
        //                         }
        //                     }
        //                 }
        //             }
        //         }
        //     }
        //   }
        // }
 
    }

// }
// }

void mlawGenericRM::ElasticStiffness(STensor43 *elasticStiffness) const {
    getConstRefMechanicalMaterialLaw().ElasticStiffness(elasticStiffness);
}
