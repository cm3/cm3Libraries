//
// C++ Interface: material law
//
// Description: UMAT interface
//
// Author:  <L. Noels>, (C) 2019
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef MLAWUMATINTERFACE_H_
#define MLAWUMATINTERFACE_H_
#include "mlaw.h"
#include "STensor3.h"
#include "STensor43.h"
#include "ipUMATInterface.h"
#include "material.h"

class mlawUMATInterface : public materialLaw
{

 protected:
  double _rho,_nu0,_mu0; //maximal mu for anisotropic

  mutable double _temperature, _deltaTemperature; // temperature is at the start of time step (previous step)
  // to be compatible with umat: use same variable
  mutable double* strn_n, *strs_n, *dstrn, *strs,*dStrsdT; //dStrdT varation of stress with Temp
  mutable double* statev_n, *statev_1;
  mutable double** Calgo;
  mutable double **Ftmp;
  mutable double **Ftmp0;
  mutable double **dR;

  double *props;
  int nprops1;

  int nsdv;

 public:
  mlawUMATInterface(const int num, const double rho, const char *propName);

 #ifndef SWIG
  mlawUMATInterface(const mlawUMATInterface &source);
  mlawUMATInterface& operator=(const materialLaw &source);
  virtual ~mlawUMATInterface();
  virtual materialLaw* clone() const {return new mlawUMATInterface(*this);};
  virtual void checkInternalState(IPVariable* ipv, const IPVariable* ipvprev) const{}; // do nothing
  // function of materialLaw
  virtual matname getType() const{return materialLaw::UMATInterface;}

  virtual void createIPState(IPStateBase* &ips, bool hasBodyForce,const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const IntPt *GP=NULL, const int gpt = 0) const
  {
     Msg::Error("Cannot be called");
  }

  virtual bool withEnergyDissipation() const {return true;};
  virtual void createIPState(IPUMATInterface *ivi, IPUMATInterface *iv1, IPUMATInterface *iv2) const;
  virtual void createIPVariable(IPUMATInterface *ipv, bool hasBodyForce, const MElement *ele,const int nbFF, const IntPt *GP, const int gpt) const;
  virtual void initLaws(const std::map<int,materialLaw*> &maplaw){}; // this law is initialized so nothing to do
  virtual double soundSpeed() const; // default but you can redefine it for your case
  virtual const double bulkModulus() const;
  virtual const double shearModulus() const;
  virtual const double poissonRatio() const;

  virtual void callUMAT(double *stress, double *statev, double **ddsdde, double &sse, double &spd, double &scd, double &rpl, 
                                 double *ddsddt, double *drplde, double &drpldt, double *stran, double *dtsran,
                                 double *tim, double timestep, double temperature, double deltaTemperature, double *predef, double *dpred,
                                 const char *CMNAME, int &ndi, int &nshr, int tensorSize, 
                                 int statevSize, double *prs, int matPropSize, double *coords, double **dRot,
                                 double &pnewdt, double &celent, double **F0, double **F1, 
                                 int &noel, int &npt, int &layer, int &kspt, int &kstep, int &kinc) const;    

  virtual const char* getCMNAME() const {return "undef";}
  void convert3x3To9(const double **mat, double *v) const;
  void convert6x6To36(const double **mat, double *v) const;
  void convert9To3x3(const double *v, double **mat) const;
  void convert36To6x6(const double *v, double **mat) const;
 public:
  virtual void constitutive(
                            const STensor3& F0,         // initial deformation gradient (input @ time n)
                            const STensor3& Fn,         // updated deformation gradient (input @ time n+1)
                            STensor3 &P,                // updated 1st Piola-Kirchhoff stress tensor (output)
                                                        // contains the initial values on input
                            const IPVariable *q0,       // array of initial internal variable
                            IPVariable *q1,             // updated array of internal variable (in ipvcur on output),
                            STensor43 &Tangent,         // constitutive tangents (output)
                            const bool stiff,            // if true compute the tangents
                            STensor43* elasticTangent = NULL, 
                            const bool dTangent =false,
                            STensor63* dCalgdeps = NULL) const;

  int getNsdv() const { return nsdv;}


 #endif // SWIG
};

#endif // MLAWUMATINTERFACE_H_
