// C++ Interface: material law
// Source: Generic Residual Strain law
// mlawGenericRM is a generic (wrapper) material law that lets
// impose a residual deformation gradient, to 
// apply the mech constitutive law on a pre strained configuration.
//
// Author:  <mohib>, (C) 2024
//
// Copyright: See COPYING file that comes with this distribution
//

#ifndef MLAWGENERICRM_H_
#define MLAWGENERICRM_H_

#include "mlaw.h"
#include "STensor3.h"
#include "STensor43.h"
#include "STensor63.h"
#include "scalarFunction.h"
#include "ipGenericRM.h"
#include "mlawCoupledThermoMechanics.h"


#include "extraVariablesAtGaussPoints.h"
// Class definition for reading values at GP

class mlawGenericRM : public  materialLaw {
protected:
    // Mechanical constitutive law @Mohib
    materialLaw *_mecaLaw;
    // Name of file containing Fres to be imposed @Mohib
    //const std::string inputfilename;
    const char* inputfilename;
    // Residual Deformation Grad Field stored as a scaler function
    // Enables applying Fres gradually as a Ramp or any other suitable 
    // function. @Mohib
    scalarFunction* _FRes;
    // Object for values read at GP @Mohib
    ExtraVariablesAtGaussPoints _extraVariablesAtGaussPoints;

public:
    mlawGenericRM(const int num, const char* inputfilename, const bool init = true);
    //mlawGenericRM(const int num, const std::string inputfilename, const bool init = true);
    const materialLaw &getConstRefMechanicalMaterialLaw() const
    {
        if(_mecaLaw == NULL)
            Msg::Error("getConstRefMechanicalMaterialLaw: Mechanic law is null");
        return *_mecaLaw;
    }
    materialLaw &getRefMechanicalMaterialLaw()
    {
        if(_mecaLaw == NULL)
            Msg::Error("getRefMechanicalMaterialLaw: Mechanic law is null");
        return *_mecaLaw;
    }
    virtual void setMechanicalMaterialLaw(const materialLaw *mlaw)
    {
        if(_mecaLaw != NULL)
        {
            delete _mecaLaw;
            _mecaLaw = NULL;
        }
        _mecaLaw=mlaw->clone();

    }
    virtual void setTime(const double ctime,const double dtime){
	materialLaw::setTime(ctime,dtime);
        // mlawCoupledThermoMechanics::setTime(ctime,dtime); //TODO: @Mohib
        if(_mecaLaw == NULL)
            Msg::Error("setTime: Mechanic law is null");
        _mecaLaw->setTime(ctime,dtime);
    }
    virtual void setMacroSolver(const nonLinearMechSolver* sv)
    {
        materialLaw::setMacroSolver( sv);
        if(_mecaLaw == NULL)
            Msg::Error("setMacroSolver: Mechanic law is null");
        _mecaLaw->setMacroSolver( sv);
    }


#ifndef SWIG
    virtual ~mlawGenericRM()
    {
        if(_mecaLaw != NULL)
        {
            delete _mecaLaw;
            _mecaLaw = NULL;
        }
    }
    mlawGenericRM(const mlawGenericRM& source);
    
    void setFileExtraInputsGp()
    {
        _extraVariablesAtGaussPoints.fill(inputfilename);
    }

    void fillExtraInputAtGp() {
        _extraVariablesAtGaussPoints.fillExtraInputAtGp(0,0);
        _extraVariablesAtGaussPoints.fillExtraInputAtGp(1,1);
        _extraVariablesAtGaussPoints.fillExtraInputAtGp(2,2);
        _extraVariablesAtGaussPoints.fillExtraInputAtGp(3,3);
        _extraVariablesAtGaussPoints.fillExtraInputAtGp(4,4);
        _extraVariablesAtGaussPoints.fillExtraInputAtGp(5,5);
        _extraVariablesAtGaussPoints.fillExtraInputAtGp(6,6);
        _extraVariablesAtGaussPoints.fillExtraInputAtGp(7,7);
        _extraVariablesAtGaussPoints.fillExtraInputAtGp(8,8);
    }
    
    virtual mlawGenericRM& operator=(const materialLaw& source);
    virtual void initLaws(const std::map<int,materialLaw*> &maplaw){}; // this law is initialized so nothing to do
    virtual double soundSpeed() const{
        if(_mecaLaw == NULL)
            Msg::Error("soundSpeed: Mechanic law is null");
        return _mecaLaw->soundSpeed();
    };
    virtual double density() const{
        if(_mecaLaw == NULL)
            Msg::Error("density: Mechanic law is null");
        return _mecaLaw->density();
    };

    virtual double ElasticShearModulus() const{
        if(_mecaLaw == NULL)
            Msg::Error("ElasticShearModulus: Mechanic law is null");
        return _mecaLaw->getConstNonLinearSolverMaterialLaw()->ElasticShearModulus();
    }

    virtual void checkInternalState(IPVariable* ipv, const IPVariable* ipvprev) const{}; // do nothing
    virtual materialLaw* clone() const {return new mlawGenericRM(*this);}
    virtual bool withEnergyDissipation() const {
            if(_mecaLaw == NULL)
            Msg::Error("ElasticShearModulus: Mechanic law is null");
        return _mecaLaw->withEnergyDissipation();

    }
    virtual matname getType() const{return materialLaw::GenericResidualStrain;}
    virtual void createIPState(IPStateBase* &ips,bool hasBodyForce, const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0,
                               const IntPt *GP=NULL, const int gpt =0) const;
    virtual void createIPVariable(IPGenericRM* &ipv, bool hasBodyForce, const MElement *ele, const int nbFF,
                                  const IntPt *GP, const int gpt) const;

    virtual void constitutive(
            const STensor3      &F0,                   // initial deformation gradient (input @ time n)
            const STensor3      &Fn,                   // updated deformation gradient (input @ time n+1)
            STensor3            &P,                    // updated 1st Piola-Kirchhoff stress tensor (output)
            const IPVariable    *q0,                   // array of initial internal variable
            IPVariable          *q1,                   // updated array of internal variable (in ipvcur on output),
            STensor43           &Tangent,              // constitutive tangents (output)
            const bool          stiff,
            STensor43           *elasticTangent, //TODO:@Mohib
            const bool dTangent,
            STensor63 *dCalgdeps //TODO:@Mohib
    ) const;

    virtual void setLawForFRes(const scalarFunction& funcRes);
    //const STensor43& getElasticityTensor() const { return _ElasticityTensor;};
    //virtual const STensor3&  getConductivityTensor() const { return _k;}; //TODO @Mohib
    //virtual const STensor3& getAlphaDilatation() const { return _alphaDilatation;}; //TODO @Mohib
    //virtual void getStiff_alphaDilatation(STensor3 &stiff_alphaDilatation) const;
    virtual void ElasticStiffness(STensor43 *elasticStiffness) const;

#endif //SWIG
};

#endif //MLAWGENERICRM_H_
