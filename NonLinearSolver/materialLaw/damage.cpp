#ifndef DAMAGE_CC
#define DAMAGE_CC 1

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <random>

#include "damage.h"
#include "matrix_operations.h"
#ifdef NONLOCALGMSH
using namespace MFH;
#endif

//****************************************
//LEMAITRE-CHABOCHE DAMAGE MODEL (1985)
//for a pseudo-grain of MT type 
//PROPS: ID, S0, n
//STATEV: D, p, plastic strn (6)
//****************************************
//constructor
LC_Damage::LC_Damage(double* props, int iddam): Damage(props, iddam){

	nsdv = 9;
	S0 = props[iddam+1];
	n = props[iddam+2];
        pos_maxD=nsdv-1;
}

//destructor
LC_Damage::~LC_Damage(){
	;
}

//print
void LC_Damage::print(){

	printf("Lemaitre-Chaboche damage law\n");
	printf("S0: %lf, n: %lf\n", S0, n);
	
}

//damagebox
void LC_Damage::damagebox(double* statev_n, double* statev, int pos_strn, int pos_strs, int pos_dam, double** Calgo,double* dDdE, double *dDdp_bar, double alpha, int kinc, int kstep){

//empty
}




///******************************************************************************************************************************************************************
////////////////////////////////////////////////
/////////////new defined ling Wu 6/4/2011.
////////////////////////////////////////////////


//LEMAITRE-CHABOCHE DAMAGE MODEL (nonlocal)
//for nonlocal MT type 
//PROPS: ID, S0, n
//STATEV: D,dD,p_bar,dp_bar, plastic strn (6)
//
// output: dD increment of D in statev
//         dDdE, dDdp_bar D w.r.t strain and p_bar respectivly
//****************************************
//constructor
LCN_Damage::LCN_Damage(double* props, int iddam):Damage(props, iddam){

	nsdv = 6;                                     //p_bar dp_bar D dD, Max_p_bar,         Max_p_bar keep the highest history value of p_bar
	S0 = props[iddam+1];
	n = props[iddam+2];
        p0 = props[iddam+3];
        pos_maxD=nsdv-1;
}

//destructor
LCN_Damage::~LCN_Damage(){
	;
}

//print
void LCN_Damage::print(){

	printf("Lemaitre-Chaboche nonlocaldamage law\n");
	printf("S0: %lf, n: %lf\n", S0, n);
	
}

//damagebox
void LCN_Damage::damagebox(double* statev_n, double* statev, int pos_strn, int pos_strs, int pos_dam, double** Calgo,double* dDdE, double *dDdp_bar, double alpha, int kinc, int kstep){


}







// Stochastic LEMAITRE-CHABOCHE DAMAGE MODEL (nonlocal)
//PROPS: ID
//STATEV: D,dD,p_bar,dp_bar, plastic strn (6)
//
// output: dD increment of D in statev
//         dDdE, dDdp_bar D w.r.t strain and p_bar respectivly
//****************************************
//constructor
LCN_Damage_Stoch::LCN_Damage_Stoch(double* props, int iddam):Damage(props, iddam){

	nsdv = 8;                                     //p_bar dp_bar D dD, Max_p_bar,         Max_p_bar keep the highest history value of p_bar
	pos_DamParm1 = nsdv-3;
        pos_DamParm2 = nsdv-2;
        p0 = props[iddam+3];
        pos_maxD=nsdv-1;

}

//destructor
LCN_Damage_Stoch::~LCN_Damage_Stoch(){
	;
}

//print
void LCN_Damage_Stoch::print(){

	printf("Stochastic Lemaitre-Chaboche nonlocaldamage law\n");
	
}

//damagebox
void LCN_Damage_Stoch::damagebox(double* statev_n, double* statev, int pos_strn, int pos_strs, int pos_dam, double** Calgo,double* dDdE, double *dDdp_bar, double alpha, int kinc, int kstep){


        
}



///******************************************************************************************************************************************************************
//Linear DAMAGE MODEL (nonlocal)
//for nonlocal MT type 
//PROPS: ID, p0, pc
//STATEV: D,dD,p_bar,dp_bar, plastic strn (6)
//
// output: dD increment of D in statev
//         dDdE, dDdp_bar D w.r.t strain and p_bar respectivly
//****************************************
//constructor
LINN_Damage::LINN_Damage(double* props, int iddam):Damage(props, iddam){

	nsdv = 6;                                     //p_bar dp_bar D dD, Max_p_bar,         Max_p_bar keep the highest history value of p_bar
	p0 = props[iddam+1];
	pc = props[iddam+2];
        pos_maxD=nsdv-1;
}

//destructor
LINN_Damage::~LINN_Damage(){
	;
}

//print
void LINN_Damage::print(){

	printf("Linear nonlocaldamage law\n");
	printf("p0: %lf, pc: %lf\n", p0, pc);
	
}
//damagebox

void LINN_Damage::damagebox(double* statev_n, double* statev, int pos_strn, int pos_strs, int pos_dam, double** Calgo,double* dDdE, double *dDdp_bar, double alpha, int kinc, int kstep){


         
}




///******************************************************************************************************************************************************************
//Exponential DAMAGE MODEL (nonlocal)
//for nonlocal MT type 
//PROPS: ID, Beta
//STATEV: D,dD,p_bar,dp_bar, plastic strn (6)
//
// output: dD increment of D in statev
//         dDdE, dDdp_bar D w.r.t strain and p_bar respectivly
//****************************************
//constructor
EXPN_Damage::EXPN_Damage(double* props, int iddam):Damage(props,iddam){

	nsdv = 6;                                     //p_bar dp_bar D dD,  Max_p_bar,         Max_p_bar keep the highest history value of p_bar
	Beta = props[iddam+1];
        pos_maxD=nsdv-1;

}

//destructor
EXPN_Damage::~EXPN_Damage(){
	;
}
//print
void EXPN_Damage::print(){

	printf("Exponential nonlocaldamage law\n");
	printf("Beta: %lf\n", Beta);
	
}
//damagebox

void EXPN_Damage::damagebox(double* statev_n, double* statev, int pos_strn, int pos_strs, int pos_dam, double** Calgo,double* dDdE, double *dDdp_bar, double alpha, int kinc, int kstep){

//  statev for damage  p_bar, dp_bar ,D, dD

  
}

///******************************************************************************************************************************************************************
//Sigmoid DAMAGE MODEL (nonlocal)
//for nonlocal MT type 
//PROPS: ID, Beta,pc
//STATEV: D,dD,p_bar,dp_bar, plastic strn (6)
//
// output: dD increment of D in statev
//         dDdE, dDdp_bar D w.r.t strain and p_bar respectivly
//****************************************
//constructor
Sigmoid_Damage::Sigmoid_Damage(double* props, int iddam):Damage(props,iddam){

	nsdv = 6;                                     //p_bar dp_bar D dD,  Max_p_bar,         Max_p_bar keep the highest history value of p_bar
	Beta = props[iddam+1];
        pc = props[iddam+2];
        pos_maxD=nsdv-1;
        D0 = 1.0/(1.0+exp(-Beta*(-pc)));

}

//destructor
Sigmoid_Damage::~Sigmoid_Damage(){
	;
}
//print
void Sigmoid_Damage::print(){

	printf("Sigmoid nonlocaldamage law\n");
	printf("Beta: , %lf, pc %lf\n", Beta, pc);
	
}
//damagebox

void Sigmoid_Damage::damagebox(double* statev_n, double* statev, int pos_strn, int pos_strs, int pos_dam, double** Calgo,double* dDdE, double *dDdp_bar, double alpha, int kinc, int kstep){


               
}
	
	

////////////////////////////////////////////////
//////////end of new defined ling Wu 11/4/2011.
////////////////////////////////////////////////


//********************************************************************************************************************************
//Power_law DAMAGE MODEL (nonlocal) ling Wu 30/11/2012
// PROPS: ID, Alpha, Beta, p0,pc
//STATEV: D,dD,p_bar,dp_bar, p (equivalent strain)
// **here, p is no longer the accumulate plastic strian, it is a defined equivalent strain. 
//
// output: dD increment of D in statev
//         dDdE, dDdp_bar D w.r.t strain and p_bar respectivly
//****************************************
//Note:
//  statev order for damage: p_bar, dp_bar ,D, dD, p
//  for this damage law, the special equivalent strain p is computered in this box, 
//  and its nonlocal value p_bar is used for damage.
//***Here we use dDdE to give back the derivatives of dpdE, which is only for this special defined damage law
//constructor
PLN_Damage::PLN_Damage(double* props, int iddam):Damage(props,iddam){

	nsdv = 7;                                     //p_bar dp_bar D dD, p, Max_p_bar,         Max_p_bar keep the highest history value of p_bar
	Alpha = props[iddam+1];
        Beta = props[iddam+2];
        p0 = props[iddam+3];
        pc = props[iddam+4];
	Dc = props[iddam+5];
	if(Dc>1.) Dc=0.999;
        pos_maxD=nsdv-1;
}

//destructor
PLN_Damage::~PLN_Damage(){
	;
}
//print
void PLN_Damage::print(){

	printf("Power_Law nonlocaldamage law\n");
	printf("Alpha: %lf,Beta: %lf,p0: %lf,pc: %lf\n", Alpha, Beta, p0,pc);
	
}
//damagebox

void PLN_Damage::damagebox(double* statev_n, double* statev, int pos_strn, int pos_strs, int pos_dam, double** Calgo,double* dDdE, double *dDdp_bar, double alpha, int kinc, int kstep){

 
}

//****************************************************************************************
// Weibull Damage model for fibers -***********************
//*********************************************************
//constructor
Weibull_Damage::Weibull_Damage(double* props, int iddam):Damage(props,iddam){


}

//destructor
Weibull_Damage::~Weibull_Damage(){
	;
}
//print
void Weibull_Damage::print(){

	printf("Weibull_Damage nonlocaldamage law\n");
	printf("L: %lf,L0: %lf, S0: %lf, Alpha: %lf,m: %lf\n", L, L0, S0, Alpha, m);
	
}
//damagebox


void Weibull_Damage::damagebox(double* statev_n, double* statev, int pos_strn, int pos_strs, int pos_dam, double** Calgo,double* dDdE, double *dDdFd_bar, double alpha, int kinc, int kstep){

 
}




// JMC JMC JMC NEW

//LEMAITRE-CHABOCHE DAMAGE MODEL (nonlocal)
//for nonlocal MT type 
//PROPS: ID, S0, n
//STATEV: D,dD,p_bar,dp_bar, plastic strn (6)
//
// output: dD increment of D in statev
//         dDdE, dDdp_bar D w.r.t strain and p_bar respectivly
//****************************************
//constructor
BILIN_Damage::BILIN_Damage(double* props, int iddam):Damage(props, iddam){

	nsdv = 6;                                     //p_bar dp_bar D dD, Max_p_bar,         Max_p_bar keep the highest history value of p_bar
	S0 = props[iddam+1];
	n = props[iddam+2];
        p0 = props[iddam+3];
        pos_maxD=nsdv-1;
}

//destructor
BILIN_Damage::~BILIN_Damage(){
	;
}

//print
void BILIN_Damage::print(){

	printf("Lemaitre-Chaboche nonlocaldamage law\n");
	printf("S0: %lf, n: %lf\n", S0, n);
	
}

//damagebox
void BILIN_Damage::damagebox(double* statev_n, double* statev, int pos_strn, int pos_strs, int pos_dam, double** Calgo,double* dDdE, double *dDdp_bar, double alpha, int kinc, int kstep){
        		
}

BILIN_Damage_Stoch::BILIN_Damage_Stoch(double* props, int iddam):Damage(props, iddam){

        nsdv = 6;                                     //p_bar dp_bar D dD, Max_p_bar,         Max_p_bar keep the highest history value of p_bar
        S0 = props[iddam+1];
        n = props[iddam+2];
        p0 = props[iddam+3];
        pos_maxD=nsdv-1;
}

//destructor
BILIN_Damage_Stoch::~BILIN_Damage_Stoch(){
        ;
}

//print
void BILIN_Damage_Stoch::print(){

        printf("Lemaitre-Chaboche nonlocaldamage law\n");
        printf("S0: %lf, n: %lf\n", S0, n);

}

//damagebox
void BILIN_Damage_Stoch::damagebox(double* statev_n, double* statev, int pos_strn, int pos_strs, int pos_dam, double** Calgo,double* dDdE, double *dDdp_bar, double alpha, int kinc, int kstep){

}

// JMC JMC JMC NEW


#endif
