//
// C++ Interface: material law
//
// Description: linear isotropic elastic law with non local damage interface
//
// Author:  <J. Leclerc>, (C) 2015
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef MLAWNONLOCALDAMAGEISOTROPICELASTICITY_H_
#define MLAWNONLOCALDAMAGEISOTROPICELASTICITY_H_
#include "mlaw.h"
#include "STensor3.h"
#include "STensor43.h"
#include "ipNonLocalDamageIsotropicElasticity.h"
#include "CLengthLaw.h"
#include "DamageLaw.h"




class mlawNonLocalDamageIsotropicElasticity : public materialLaw
{
protected:
    double _rho;                        // Density
    double _E;                          // Young modulus
    double _nu;                         // Poisson ratio
    STensor43 _ElasticityTensor;        // Elasticity tensor
    CLengthLaw *_cLLaw;                 // Law for non-local length
    DamageLaw *_damLaw;                 // Law for damage evolution
    int _nonLocalLimitingMethod;        // Identify non-local limiting method

public:
    // Constructors & Destructors
    mlawNonLocalDamageIsotropicElasticity(const int num, const double rho, const double E,const double nu,
                                          const CLengthLaw &cLLaw,const DamageLaw &damLaw);

    #ifndef SWIG
    mlawNonLocalDamageIsotropicElasticity(const mlawNonLocalDamageIsotropicElasticity &source);
    mlawNonLocalDamageIsotropicElasticity& operator=(const materialLaw &source);
    virtual ~mlawNonLocalDamageIsotropicElasticity()
    {
        if(_cLLaw != NULL) delete _cLLaw; _cLLaw = NULL;
        if(_damLaw != NULL) delete _damLaw; _damLaw = NULL;
    }
		virtual materialLaw* clone() const {return new mlawNonLocalDamageIsotropicElasticity(*this);};

		virtual void checkInternalState(IPVariable* ipv, const IPVariable* ipvprev) const{}; // do nothing

    // General functions
    virtual matname getType() const {return materialLaw::nonLocalDamageIsotropicElasticity;}
    virtual bool withEnergyDissipation() const {return true;};
    virtual void createIPState(IPStateBase* &ips, bool hasBodyForce, const bool* state_ = NULL,const MElement *ele = NULL, const int nbFF_ = 0, const IntPt *GP=NULL, const int gpt=0) const;
    virtual void createIPState(IPNonLocalDamageIsotropicElasticity *ivi, IPNonLocalDamageIsotropicElasticity *iv1, IPNonLocalDamageIsotropicElasticity *iv2) const;
    virtual void createIPVariable(IPNonLocalDamageIsotropicElasticity *&ipv) const;
    virtual void initLaws(const std::map<int,materialLaw*> &maplaw) {}; // this law is initialized so nothing to do
    virtual double density() const {return _rho;};
    virtual double soundSpeed() const;
		virtual double scaleFactor() const{return _E/(2.*(1.+_nu));}
    virtual const CLengthLaw *getCLengthLaw() const {return _cLLaw;};
    virtual const DamageLaw *getDamageLaw() const{return _damLaw;};

    //Specific functions
      // Specify limiting method for non-local spread
      // = 0 : nothing happens
      // = 1 : if damage is blocked, maximal values of local strains (send inside non-local equations)
      // is restrained to its maximal value ever reached before
    virtual void setNonLocalLimitingMethod(const int num){_nonLocalLimitingMethod = num;};

      // Constitutive law
    virtual void constitutive(
                            const STensor3& F0,         // initial deformation gradient (input @ time n)
                            const STensor3& Fn,         // updated deformation gradient (input @ time n+1)
                            STensor3 &P,                // updated 1st Piola-Kirchhoff stress tensor (output)
                                                        // contains the initial values on input
                            const IPVariable *q0,       // array of initial internal variable
                            IPVariable *q1,             // updated array of internal variable (in ipvcur on output),
                            STensor43 &Tangent,         // constitutive tangents (output)
                            const bool stiff,            // if true compute the tangents
                            STensor43* elasticTangent = NULL, 
                            const bool dTangent =false,
                            STensor63* dCalgdeps = NULL) const
    {
       Msg::Error(" mlawNonLocalDamageIsotropicElasticity: local constituve not defined");
    }
    virtual void constitutive(
        const STensor3& F0,                             // initial deformation gradient (input @ time n)
        const STensor3& F1,                             // updated deformation gradient (input @ time n+1)
        STensor3 &P,                                    // updated 1st Piola-Kirchhoff stress tensor (output)
        const IPVariable *q0,  // array of initial internal variable (in ipvprev on input)
        IPVariable *q1,        // updated array of internal variable (in ipvcur on output),
        STensor43 &Tangent,                             // constitutive tangents (output) // partial stress / partial strains
        STensor3 &dLocalEffectiveStrainsDStrains,       //                                // partial Local Effective Strains / partial Strains
        STensor3 &dStressDNonLocalEffectiveStrains,     //                                // partial stress / partial Non-Local Effective strains
        double &dLocalEffectiveStrainsDNonLocalStrains, //                                // partial Local Effective Strains / partial Non Local effective strains
        const bool stiff,                                // if true: compute the tangents analytically
        STensor43* elasticTangent = NULL

    ) const;

protected:
    virtual double deformationEnergy(const STensor3 &epsilon_strains, const STensor3 &sigma) const;   // Compute elastic energy

    #endif // SWIG
};

#endif // MLAWNONLOCALDAMAGEISOTROPICELASTICITY_H_


