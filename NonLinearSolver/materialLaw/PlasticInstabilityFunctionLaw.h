//
// C++ Interface: Plastic instability function law
//
//
// Author:  <L. Noels>, (C) 2020
//
// Copyright: See COPYING file that comes with this distribution
//


#ifndef _PLASTICINSTABILITYFUNCTIONLAW_H_
#define _PLASTICINSTABILITYFUNCTIONLAW_H_


/*! \class PlasticInstabilityFunctionLaw
 * This mother class defines the commun interface for the different plastic instabilities functions.
 * They compute the yield stress reduction from the non-local matrix plastic strain
 */
class PlasticInstabilityFunctionLaw
{
public :
  //! type enumeration for the nucleation function
  enum PlasticInstabilityFunctionLawName{ TrivialPlasticInstability, ExponentialPlasticInstability
                              };
#ifndef SWIG
protected :
  //! identifier of the internal law
  int num_;
  
private :
  //! default constructor is always forbidden
  PlasticInstabilityFunctionLaw();
  
public :
  PlasticInstabilityFunctionLaw( const int num )
  { num_=num; }
  PlasticInstabilityFunctionLaw( const PlasticInstabilityFunctionLaw& source )
  {
    num_=source.num_;
  }
  virtual PlasticInstabilityFunctionLaw& operator= ( const PlasticInstabilityFunctionLaw& source )
  {
    num_=source.num_;
    return *this;
  }
  virtual ~PlasticInstabilityFunctionLaw(){};
  virtual PlasticInstabilityFunctionLaw* clone() const 
  {
    return new PlasticInstabilityFunctionLaw(*this);
  }
  //! @}

  
  //! @name General functions
  virtual int getNum() const {return num_;};
  virtual PlasticInstabilityFunctionLawName getType() { return PlasticInstabilityFunctionLaw::TrivialPlasticInstability;}
  virtual void setNum( const int num)
  {
    num_ = num;
  }
    
public :
  virtual void getPlasticInstability(double hatpm, double &wp, double &dwp) const
  {
    wp=0.;
    dwp=0.;
  };
    
#endif // SWIG
};

class ExponentialPlasticInstabilityFunctionLaw : public PlasticInstabilityFunctionLaw
{
#ifndef SWIG
protected :
  //! total quantity to be reduced
  double deltaY_;
  //! exponential rate
  double eps0_;
#endif // SWIG


  //! @name Constructors and destructors
public :
  ExponentialPlasticInstabilityFunctionLaw( const int num, const double eps0, const double deltaY);
#ifndef SWIG
  ExponentialPlasticInstabilityFunctionLaw( const ExponentialPlasticInstabilityFunctionLaw& source );
  ExponentialPlasticInstabilityFunctionLaw& operator= ( const PlasticInstabilityFunctionLaw& source );
  virtual ~ExponentialPlasticInstabilityFunctionLaw(){};
  virtual PlasticInstabilityFunctionLaw* clone() const { return new ExponentialPlasticInstabilityFunctionLaw( *this ); };
  //! @}
  
  //! @name General functions of materialLaw
  virtual PlasticInstabilityFunctionLawName getType() const { return PlasticInstabilityFunctionLaw::ExponentialPlasticInstability; };
  virtual void getPlasticInstability(double hatpm, double &wp, double &dwp) const
  {
    wp=0.;
    dwp=0.;
    if(hatpm>0.)
    {
      wp= deltaY_*(1.-exp(-hatpm/eps0_));
      dwp = deltaY_*exp(-hatpm/eps0_)/eps0_;
    }
  };
  
  //! @}
#endif // SWIG
};







#endif // _PLASTICINSTABILITYFUNCTIONLAW_H_


