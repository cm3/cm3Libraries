// C++ Interface: material law
//
// Description: general class for nonlocal porous material law
//
// Author:  <V.D. Nguyen>, (C) 2018
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef MLAWNONLOCALPOROUS_H_
#define MLAWNONLOCALPOROUS_H_

// Utilitary includes
#include "STensor43.h"
#include "STensor63.h"
#include "scalarFunction.h"
#include "mlaw.h"
#include "j2IsotropicHardening.h"
#include "CoalescenceLaw.h"
#include "voidStateEvolutionLaw.h"
#include "CLengthLaw.h"
#include "PlasticInstabilityFunctionLaw.h"
#include "viscosityLaw.h"



class NucleationLaw;
class NucleationFunctionLaw;
class NucleationCriterionLaw;

class mlawNonLocalPorosity: public materialLaw
{
  public:
     //! Type of non-local implementation (with respect to the DG term!).
    enum nonLocalMethod{  LOCAL = 0,                     //!< the local model is used (option = 0).
                          NONLOCAL_POROSITY = 1,         //!< the non-local model uses the non-local porosity (option = 1).
                          NONLOCAL_FULL_THREEVAR = 2,    //!< the non-local model uses 3 non-local plastic increments (option = 2).
                          NONLOCAL_LOG_POROSITY = 3      //!< the non-local model uses the non-local ln(porosity) (option = 3).
                       };
    //! Type of stress tensor used (coro Cauchy or Kirchhoff) in the yield surface and in the nucleation criterion.
    enum stressFormumation{ CORO_CAUCHY = 0,    //!< Corotational Cauchy is used (option 0).
                            CORO_KIRCHHOFF = 1  //!< Corotational Kirchhoff is used (option 1).
                          };
    //! Requested behavior once the damage is blocked inside the element.
    enum postBlockBehaviour{ ELASTIC = 1,       //!< 1 - elastic behavior only.
                           ELASTOPLASTIC = 2  //!< 2 - purely elasto-plastic behavior only.
                         };
                         
  #ifndef SWIG
  protected:
    //! Non-local method used
    nonLocalMethod nonlocalMethod_;
    
    int _numNonLocalVar;  // number of non-local variables
    //! (option) Stress formulation used.
    stressFormumation _stressFormulation;
        
    //! (option) Activate nucleation after coalescence onset if true (default = false).
    bool _nucleationDuringCoalesIsON;
    
    //! (transition) Behavior after transition. (default = elasto-plastic = 2)
    postBlockBehaviour _postDamageBlockingBehavior;
    
    //! (thermo-mech) Activate the thermomechanical coupling if true (default = false).
    bool _thermomechanicsCoupling;
    //! (thermo-mech) Base the thermomechanical coupling on the previous configuration if true (default = true).
    bool _thermalEstimationPreviousConfig;

    //! Define all the included internal laws.
    //! Vector of non-local length laws
    std::vector< CLengthLaw* > _cLLaw;
    
    //! Isotropic hardening law.
    J2IsotropicHardening* _j2IH;
  
    //! Viscoplastic law (Perzyna-type law).
    viscosityLaw* _viscoplastlaw; // 
    
    //! Plastic instability law
    PlasticInstabilityFunctionLaw* _plasticInstabilityFunctionLaw;
  
    //! Nucleation law.
    NucleationLaw* _nucleationLaw;
      
    //! (Elasticity) Intiial density [kg/m^3]
    double _rho;
    //! (Elasticity - thermal part) Temperature dependence of density (i.e. f such as rho(T) = f(T)*_rho ).
    scalarFunction* _tempFunc_rho; 
    //! (Elasticity) Young Modulus [Pa].
    double _E;
    //! (Elasticity - thermal part) Temperature dependence of Young Modulus (i.e. f such as E(T) = f(T)*_E ).
    scalarFunction* _tempFunc_E;
    //! (Elasticity) Poisson ratio [-].
    double _nu;
    //! (Elasticity - thermal part) Temperature dependence of Poisson ratio (i.e. f such as nu(T) = f(T)*_nu ).
    scalarFunction* _tempFunc_nu;
    //! (Elasticity) 1st Lame parameter.
    double _lambda;
    //! (Elasticity) 2nd Lame parameter / shear modulus (= G ).
    double _mu;
    //! (Elasticity) 2*Shear modulus ( = 2.0*_G ).
    double _mu2;
    //! (Elasticity) Bulk modulus.
    double _K;
    //! (Elasticity) 3*Bulk modulus ( = 3.0*_K ).
    double _K3;
    
    //! (Elasticity) Elasticity Hook tensor
    STensor43 _Cel;
    
    
    //! (Thermal properties) Reference temperature for thermal expansion computation.
    double _Tref;
    //! (Thermal properties) The heat capacity value at the reference temperature (i.e. cp_ref = cp(Tref)). 
    double _cp;
    //! (Thermal properties) The temperature-dependency function of the thermal capacity (i.e. f such as cp = f(T)*cp_ref ).
    scalarFunction* _tempFunc_cp;
    //! (Thermal properties) Taylor-Quiney coefficient - ratio of plastic work conversion into heat generation. 
    double _taylorQuineyFactor;
    //! (Thermal properties) The thermal expansion value at the reference temperature.
    double _alphaDilatation;
    //! (Thermal properties) The temperature-dependency function of the thermal expansion (i.e. f such as alpha = f(T)*alpha_ref ).
    scalarFunction* _tempFunc_alphaDilatation;
    //! (Thermal properties) ????????????????.
    STensor3 Stiff_alphaDilatation;
    //! (Thermal properties) The thermal conductivity value at the reference temperature.
    double _thermalConductivity;
    //! (Thermal properties) The temperature-dependency function of the thermal conductivity (i.e. f such as k = f(T)*k_ref ).
    scalarFunction* _tempFunc_thermalConductivity;
    //! (Thermal properties) The thermal conductivity tensor (Fourier Law).
    STensor3 linearK;
    
    //! (Micro-mechanics) Initial porosity [-].
    double _fVinitial;
    //! (Micro-mechanics) Min. bound applied on the initial microstructure [-].
    double _randMinbound;
    //! (Micro-mechanics) Max. bound applied on the initial microstructure [-].
    double _randMaxbound;
    
    //! (Micro-mechanics - numerical parameter) Max. porosity value allowed in the yield surface 
    //! (value for which f(_fV_failure) = 0) which depends on the yield surface used. (default = 1) [-].
    double _fV_failure;
    //! (Micro-mechanics - numerical parameter) Max. local porosity value (< 1) [-]. \todo double employ ???? in the future, improve the numerical robustness.
    double _localfVFailure;
    //! (Micro-mechanics - numerical parameter) Min. allowed value for the porosity in order to avoid numerical problem.
    double _localfVMin;
    //! (Micro-mechanics - numerical parameter) Regularization function for the yield/effective value of porosity.
    scalarFunction* _regularisationFunc_porosityEff;

    //! (Micro-mechanics) The shear-induced porosity growth factor of Nashon-Hutchinson kw 
    //! such as \f$ \Delta f_{shear} = kw * f(T) * f(L) * \Delta d \f$.
    double _kw;
    //! (Micro-mechanics) The shear-induced porosity growth function of stress triaxiality T = p / tau_eq 
    //! such as \f$ \Delta f_{shear} = kw * f(T) * f(L) * \Delta d \f$.
    scalarFunction* _shearGrowth_triaxFunc;
    //! (Micro-mechanics) The shear-induced porosity growth function of Lode variable L = xi = 27 det(tau_dev)/2 tau_eq^3 
    //! such as \f$ \Delta f_{shear} = kw * f(T) * f(L) * \Delta d \f$.
    scalarFunction* _shearGrowth_LodeVarFunc;
    
    //! Polynomial approximation order for tensorial log. and exp. computation.
    //! Should be uneven (= 1 by default).
    int _orderlogexp;
    //! (Numerical options) Tolerance on the yield surface (dimensionless).
    double _tol;
    //! (Numerical options) Maximal number of iterations for the Newton-Raphson iin the plastic corrector.
    int _maxite;
    //! (Numerical options) Activate tangent computation by perturbation if true (false by default).
    bool _tangentByPerturbation;
    //! (Numerical options) Factor applied for perturbation (dimensionless).
    double _perturbationfactor;
    //! (Numerical options) Failure tolerance on the yield/effective value of porosity.
    double _failedTol; //!< \todo improve its use.
    
    //! (Sub-stepping parameters) Activate the sub-stepping scheme if true (false by default).
    bool _withSubstepping;
    //! (Sub-stepping parameters) Maximal number of attempts to do a sub-stepping. (i.e. times on which the step is divided by 2, default = 1).
    int _maxAttemptSubstepping;
    //! (Sub-stepping parameters) Number of sub-steps at the first attempts (?????????? by default).
    int _numofSubStep;
    
    //! Second order identity tensor: \f$ I_{ij} = \delta_{ij} \f$.
    STensor3  _I;
    //! 4th-order symmetric identity tensor: \f$ I4_{ijkl} =  0.5 * ( \delta_{ik} \delta_{jl} + \delta_{il} \delta_{jk} ) \f$.
    STensor43 _I4;
    //! 4-th order volumic tensor: \f$ I4vol_{ijkl} =  1/3 * \delta_{ij} \delta_{kl} \f$.
    STensor43 _I4vol;
    //! 4-th order deviatoric tensor: \f$ I4dev_{ijkl} =  I4_{ijkl} - I4vol_{ijkl} \f$.
    STensor43 _I4dev;
    
    //! Derivatives of the pressure in terms of stress components \f$ 1/3 I_{ij} \f$.
    STensor3 _dpressure_dStress;
    
  protected:  
    virtual inline double density( const double T ) const{
      return _rho*_tempFunc_rho->getVal(T);
    };
    virtual inline double youngModulus( const double T ) const{ 
      return _E*_tempFunc_E->getVal(T);
    };
    virtual inline double youngModulusDerivative( const double T ) const{
      return _E*_tempFunc_E->getDiff(T);
    };
    virtual inline double poissonRatio( const double T ) const{ 
      return _nu*_tempFunc_nu->getVal(T);
    };
    virtual inline double poissonRatioDerivative( const double T ) const{
      return _nu*_tempFunc_nu->getDiff(T);
    };
    virtual inline double ThermalExpansion( const double T) const{ 
      return _alphaDilatation*_tempFunc_alphaDilatation->getVal(T);
    };
    virtual inline double ThermalExpansionDerivative( const double T ) const{
      return _alphaDilatation*_tempFunc_alphaDilatation->getDiff(T);
    };
    virtual inline double bulkModulus( const double T ) const{ 
      return youngModulus(T)/(3.*(1.-2.*poissonRatio(T)));
    };
    virtual inline double bulkModulusDerivative( const double T ) const{
      return ( youngModulusDerivative(T)*3.0*(1.-2.*poissonRatio(T))+ 6.0 * youngModulus(T)*poissonRatioDerivative(T)) / (3.*(1.-2.*poissonRatio(T))) / (3.*(1.-2.*poissonRatio(T)));
    };
    virtual inline double shearModulus( const double T ) const{ 
      return 0.5*youngModulus(T)/(1+poissonRatio(T));
    };
    virtual inline double shearModulusDerivative( const double T ) const{
      return (0.5*youngModulusDerivative(T)*(1.+poissonRatio(T))-0.5*youngModulus(T)*poissonRatioDerivative(T))/(1.+poissonRatio(T))/(1.+poissonRatio(T));
    };
    virtual inline double lambda( double T ) const{
      return (youngModulus(T)*poissonRatio(T))/(1.+poissonRatio(T))/(1.-2.*poissonRatio(T));
    };


    //! Elastic predictor.
    /// From the deformation gradient and the given plastic deformation gradient (usually, the predictor one),
    /// compute the stress state in terms of the corKir, the elastic strain tensors and the related derivatives.
    virtual bool elasticPredictor( const STensor3& F1, const STensor3& Fppr,
                                   STensor3& corKir,  STensor3& corKirDev, double& pKir_pr,
                                   STensor43 &dcorKirdEe, STensor43& dcorKirDevdEe, STensor3& dpDEe,
                                   STensor3& Fepr, STensor3& Cepr, STensor3& Eepr,
                                   STensor43 &Le, STensor63 &dLe, const bool stiff, 
                                   const double* T = NULL, STensor3 *dcorKirdT = NULL, STensor3* dcorKirDevdT = NULL, double* dpDT = NULL
                                  ) const;
  #endif //SWIG

  public :
    // Option settings
    virtual void setNonLocalMethod(const int i);
    virtual void clearCLengthLaw();
    virtual void setCLengthLaw(const CLengthLaw& clength);

    //! (option setting) Set stress formulation method.
    virtual void setStressFormulation( const int stress_formu ); // add
    //! (option setting) Choose if the nucleation has to be active during the coalescence phase or not.
    virtual void activateNucleationDuringCoalescence( const bool nuclAfterCoalesIsON = true );
    //! (option setting) Choose the material behavior after damage blocking.
    virtual void setPostBlockedDissipationBehavior( const int method ); 
    
    //! (internal law) Set the viscoplastic law (no viscoplastic effects by default).
    virtual void setViscoPlasticLaw( const viscosityLaw& viscoPlasticLaw ); // add
    //! (internal law) Set the plastic instability law (no instability by default).
    virtual void setPlasticInstabilityFunctionLaw( const PlasticInstabilityFunctionLaw& plIns ); // add
    //! (internal law) Set the void evolution law.
    //! (internal law) Set the nucleation law.
    virtual void setNucleationLaw( const NucleationFunctionLaw& added_function, const NucleationCriterionLaw* added_criterion = NULL );
    //! (internal law) Set the nucleation law (deprecated).
    virtual void setNucleationLaw( const NucleationLaw& nuclLaw );

    //! (option setting - thermal part) Choose to activate the thermo-mechanical coupling.
    virtual void setThermomechanicsCoupling( const bool thermoMechCouplingIsON = true ); // add
    //! (option setting - thermal part) 
    virtual void setThermalEstimationPreviousConfig( const bool thermoMechIsBasedOnPrevious );// add
    //! (material param. - thermal part) Set the temperature-dependency function of the density.
    virtual void setTemperatureFunction_rho( const scalarFunction& Tfunc ); // add
    //! (material param. - thermal part) Set the temperature-dependency function of the Young modulus.
    virtual void setTemperatureFunction_E( const scalarFunction& Tfunc ); // add
    //! (material param. - thermal part) Set the temperature-dependency function of the poison ratio.
    virtual void setTemperatureFunction_nu( const scalarFunction& Tfunc ); // add
    //! (material param. - thermal part) Set the thermal capacity value at the reference temperature (i.e. cp_ref = cp(Tref)).
    virtual void setReferenceCp( const double cp );
    //! (material param. - thermal part) Set the temperature-dependency function of the thermal capacity (i.e. f such as cp = f(T)*cp_ref ).
    virtual void setTemperatureFunction_cp( const scalarFunction& Tfunc );
    //! (material param. - thermal part) Set the reference temperature Tref [K] (at which reference values are defined).
    virtual void setReferenceT( const double referenceT );
    //! (material param. - thermal part) Set the value of the Taylor Quiney factor.
    virtual void setTaylorQuineyFactor( const double taylorQuineyFactor );
    //! (material param. - thermal part) Set the thermal expansion coefficient.
    virtual void setThermalExpansionCoefficient( const double thermalExpansionCoefficient );
    //! (material param. - thermal part) Set the temperature-dependency function of the thermal expansion coefficient.
    virtual void setTemperatureFunction_alphaDilatation( const scalarFunction& Tfunc );
    //! (material param. - thermal part) Set the thermal conductivity value at the reference temperature (i.e. k_ref = k(Tref)).
    virtual void setReferenceThermalConductivity( const double k );
    //! (material param. - thermal part) Set the temperature-dependency function of the thermal conductivity (i.e. f such as k = f(T)*k_ref ).
    virtual void setTemperatureFunction_ThermalConductivity( const scalarFunction& Tfunc );

    //! (material param. - micromech part) Set the random law bounds to applied on the initial porosity.
    //! (The bounds determine the span of a constant density probability function. The initial porosity is then multiply by this function).
    virtual void setScatterredInitialPorosity( const double f0min, const double f0max );
    //! (material param. - micromech part) Set the shear-induced void growth term parameters: the growth coefficient.
    virtual void setShearPorosityGrowth_Factor( const double kw );
    //! (material param. - micromech part) Set the shear-induced void growth term parameters: the triaxiality-dependent function.
    virtual void setShearPorosityGrowth_StressTriaxialityFunction( const scalarFunction& fct );
    //! (material param. - micromech part) Set the shear-induced void growth term parameters: the Lode-dependent function.
    virtual void setShearPorosityGrowth_LodeFunction( const scalarFunction& fct );
    //! (numerical parameter) Set the tolerance value applied on the damage parameters after which the material has failed.
    virtual void setFailureTolerance( const double NewFailureTol );
    //! (numerical parameter) Set the maximal allowed porosity value.
    virtual void setLocalFailurePorosity( const double newFvMax );
    //! (numerical parameter) Set the regularization function applied on the damage parameters.
    virtual void setCorrectedRegularizedFunction( const scalarFunction& fct );

    //! (option setting - numerical parameter) Set the order for Taylor approximation of log/exp operators.
    virtual void setOrderForLogExp( const int newOrder );
    //! (option setting - numerical parameter) Allow the computation of the tangent by perturbation (and set the perturbation factor).
    //! Can be also done thanks to the constructor.
    virtual void setTangentByPerturbation( const bool tangentByPertIsON = true, const double perturbationfactor = 1.0e-8 );
    //! (option setting - numerical parameter) Allow the sub-stepping and set the number of reduction attempt.
    /// i.e. the maximal total allowed number of sub-step is equal to 2^maxNumStep.
    virtual void setSubStepping( const bool subSteppingIsON = true, const int maxNumOfStepReductions = 10 );
    
  #ifndef SWIG
    mlawNonLocalPorosity(const int num,const double E,const double nu, const double rho,
                           const double fVinitial, const J2IsotropicHardening &j2IH, const CLengthLaw &cLLaw,
                           const double tol=1.e-8, const bool matrixbyPerturbation = false, const double pert = 1e-8);
    mlawNonLocalPorosity(const mlawNonLocalPorosity &source);
    virtual ~mlawNonLocalPorosity();
    
       // Function of materialLaw
    virtual matname getType() const = 0;
    virtual void createIPState(IPStateBase* &ips, bool hasBodyForce,const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const IntPt *GP=NULL, const int gpt = 0) const = 0;
    virtual void createIPVariable(const double fInit, IPNonLocalPorosity* &ipv) const = 0;
    virtual void initLaws(const std::map<int,materialLaw*> &maplaw) = 0;

    virtual materialLaw* clone() const  = 0;
    
    virtual void setVoidEvolutionLaw( const voidStateEvolutionLaw& voidStateLaw ) = 0;
    virtual void setCoalescenceLaw( const CoalescenceLaw& added_coalsLaw ) =0;
    
    virtual const voidStateEvolutionLaw* getVoidEvolutionLaw() const =0;
    virtual const CoalescenceLaw* getCoalescenceLaw() const =0;
    
    //! Inform that these laws are dissipative.
    virtual bool withEnergyDissipation() const { return true; };
    //! Send back the sound speed (at the temperature reference).
    virtual double soundSpeed() const;
    //! Send back the density.
    virtual double density() const { return _rho; };
    
    //! (options) Send back which stress formulation is used.
    virtual const stressFormumation getStressFormulation() const { return _stressFormulation; };
    //! (options) Inform if the law use thermo-mech coupling
    virtual const bool isThermomechanicallyCoupled() const { return _thermomechanicsCoupling; };
    
    virtual bool getTangentByPerturbation() const {return _tangentByPerturbation;};

    //! (mat. properties) Send back the porosity at which failure is considered.
    virtual double getFailureYieldPorosityValue() const  { return _fV_failure; };
    //! (mat. properties) Send back the initial elastic tensor.
    virtual const STensor43& getCel() const { return _Cel;};
    //! (mat. properties) Send back the initial tensor of conductivity.
    virtual const STensor3& getInitialConductivityTensor() const { return linearK; };
    //! (mat. properties) Send back the porosity at which failure is considered.
    virtual const STensor3& getStiff_alphaDilatation() const  { return Stiff_alphaDilatation; };
    
    //! Access to isotropic law.
    virtual const J2IsotropicHardening* getJ2IsotropicHardening() const { return _j2IH; };
    //! Access to viscoplastic law.
    virtual const viscosityLaw* getViscoplasticLaw() const { return _viscoplastlaw; };
    //! Access to plastic instability law.
    virtual const PlasticInstabilityFunctionLaw* getPlasticInstabilityFunctionLaw() const { return _plasticInstabilityFunctionLaw; };
    //! Access to nucleation law.
    virtual const NucleationLaw* getNucleationLaw() const { return _nucleationLaw; };
    
    virtual const nonLocalMethod getNonLocalMethod() const;
    
    virtual double generateARandomInitialPorosityValue() const { return _fVinitial*frand(_randMinbound,_randMaxbound); };
    static double frand(const double a,const double b){ return (rand()/(double)RAND_MAX) * (b-a) + a;}

    virtual double scaleFactor() const { return _mu;};
    virtual double bulkModulus() const {return _K;}
    // Access functions
    const std::vector<CLengthLaw*>* getCLengthLawVector() const {return &_cLLaw; };

    virtual double getShearFactor(const STensor3& kcor, bool stiff=false, STensor3* DkDkcor=NULL) const;

    double poissonRatio() const{return _nu;}
    virtual const int getNumNonLocalVariables() const {return _numNonLocalVar;}
  public :
    // Local case
    virtual void constitutive(
                              const STensor3& F0,         // initial deformation gradient (input @ time n)
                              const STensor3& Fn,         // updated deformation gradient (input @ time n+1)
                              STensor3 &P,                // updated 1st Piola-Kirchhoff stress tensor (output)
                              const IPVariable *q0,       // array of initial internal variable
                              IPVariable *q1,             // updated array of internal variable (in ipvcur on output),
                              STensor43 &Tangent,         // constitutive tangents (output)
                              const bool stiff,           // if true compute the tangents
                              STensor43* elasticTangent = NULL, 
                              const bool dTangent =false,
                              STensor63* dCalgdeps = NULL) const;

    virtual void constitutive(
                              const STensor3& F0,         // initial deformation gradient (input @ time n)
                              const STensor3& Fn,         // updated deformation gradient (input @ time n+1)
                              STensor3 &P,                // updated 1st Piola-Kirchhoff stress tensor (output)
                                                          // contains the initial values on input
                              const IPNonLocalPorosity *q0,       // array of initial internal variable
                              IPNonLocalPorosity *q1,             // updated array of internal variable (in ipvcur on output),
                              STensor43 &Tangent,         // constitutive tangents (output)
                              std::vector<STensor3>  &dLocalVarDStrain,
                              std::vector<STensor3>  &dStressDNonLocalVar,
                              fullMatrix<double>    &dLocalVarDNonLocalVar,
                              const bool stiff,
                              STensor43* elasticTangent = NULL) const;
    
     // Local case
    virtual void I1J2J3_constitutive(
                              const STensor3& F0,         // initial deformation gradient (input @ time n)
                              const STensor3& Fn,         // updated deformation gradient (input @ time n+1)
                              STensor3 &P,                // updated 1st Piola-Kirchhoff stress tensor (output)
                              const IPVariable *q0,       // array of initial internal variable
                              IPVariable *q1,             // updated array of internal variable (in ipvcur on output),
                              STensor43 &Tangent,         // constitutive tangents (output)
                              const bool stiff,           // if true compute the tangents
                              STensor43* elasticTangent = NULL, 
                              const bool dTangent =false,
                              STensor63* dCalgdeps = NULL) const;
                              
    virtual void I1J2J3_constitutive(
                              const STensor3& F0,         // initial deformation gradient (input @ time n)
                              const STensor3& Fn,         // updated deformation gradient (input @ time n+1)
                              STensor3 &P,                // updated 1st Piola-Kirchhoff stress tensor (output)
                                                          // contains the initial values on input
                              const IPNonLocalPorosity *q0,       // array of initial internal variable
                              IPNonLocalPorosity *q1,             // updated array of internal variable (in ipvcur on output),
                              STensor43 &Tangent,         // constitutive tangents (output)
                              std::vector<STensor3>  &dLocalVarDStrain,
                              std::vector<STensor3>  &dStressDNonLocalVar,
                              fullMatrix<double>    &dLocalVarDNonLocalVar,
                              const bool stiff,
                              STensor43* elasticTangent = NULL) const;

    virtual void constitutive(const STensor3& F0,         // initial deformation gradient (input @ time n)
                              const STensor3& Fn,         // updated deformation gradient (input @ time n+1)
                              STensor3 &P,                // updated 1st Piola-Kirchhoff stress tensor (output)
                              const IPNonLocalPorosity *q0,       // array of initial internal variable
                              IPNonLocalPorosity *q1,             // updated array of internal variable (in ipvcur on output),
                              STensor43 &Tangent,         // constitutive tangents (output)
                              std::vector<STensor3>  &dLocalVarDStrain,
                              std::vector<STensor3>  &dStressDNonLocalVar,
                              fullMatrix<double>    &dLocalVarDNonLocalVar,
                              const bool stiff,
                              STensor3& dPdT,
                              STensor33 &dPdgradT,
                              fullMatrix<double>  &dLocalVarDT,
                              const double& T0, // previous temperature
                              const double& Tn, // current temperature
                              const SVector3 &gradT0, // previous temperature gradient
                              const SVector3 &gradT, // current temperature gradient
                              SVector3 &fluxT, // temperature flux
                              STensor33 &dfluxTdF, // thermal-mechanical coupling
                              std::vector<SVector3> &dfluxTdNonLocalVar,
                              SVector3 &dfluxTdT,
                              STensor3 &dfluxTdgradT, // thermal tangent
                              double &thermalSource,   // - cp*dTdt
                              STensor3 &dthermalSourcedF,
                              fullMatrix<double> &dthermalSourcedNonLocalVar,
                              double &dthermalSourcedT, // thermal source
                              SVector3 &dthermalSourcedgradT,
                              double& mechanicalSource, // mechanical source--> convert to heat
                              STensor3 & dmechanicalSourceF,
                              fullMatrix<double> & dmechanicalSourcedNonLocalVar,
                              double & dmechanicalSourcedT,
                              SVector3 &dmechanicalSourcedgradT,
                              STensor43* elasticTangent = NULL) const;


    virtual void predictorCorrector(const STensor3& F0,         // initial deformation gradient (input @ time n)
                              const STensor3& Fn,         // updated deformation gradient (input @ time n+1)
                              STensor3 &P,                // updated 1st Piola-Kirchhoff stress tensor (output)
                              const IPNonLocalPorosity *q0,       // array of initial internal variable
                              IPNonLocalPorosity *q1,             // updated array of internal variable (in ipvcur on output),
                              STensor43 &Tangent,         // constitutive tangents (output)
                              std::vector<STensor3>  &dLocalVarDStrain,
                              std::vector<STensor3>  &dStressDNonLocalVar,
                              fullMatrix<double>    &dLocalVarDNonLocalVar,
                              const bool stiff,
                              STensor43& dFedF, std::vector<STensor3>& dFeDNonlocalVar,
                              STensor3& dPdT,
                              STensor33 &dPdgradT,
                              fullMatrix<double>  &dLocalVarDT,
                              const double& T0, // previous temperature
                              const double& Tn, // current temperature
                              const SVector3 &gradT0, // previous temperature gradient
                              const SVector3 &gradT, // current temperature gradient
                              SVector3 &fluxT, // temperature flux
                              STensor33 &dfluxTdF, // thermal-mechanical coupling
                              std::vector<SVector3> &dfluxTdNonLocalVar,
                              SVector3 &dfluxTdT,
                              STensor3 &dfluxTdgradT, // thermal tangent
                              double &thermalSource,   // - cp*dTdt
                              STensor3 &dthermalSourcedF,
                              fullMatrix<double> &dthermalSourcedNonLocalVar,
                              double &dthermalSourcedT, // thermal source
                              SVector3 &dthermalSourcedgradT,
                              double& mechanicalSource, // mechanical source--> convert to heat
                              STensor3 & dmechanicalSourceF,
                              fullMatrix<double> & dmechanicalSourcedNonLocalVar,
                              double & dmechanicalSourcedT,
                              SVector3 &dmechanicalSourcedgradT
                              ) const;

    virtual void predictorCorrector(const STensor3& F0,         // initial deformation gradient (input @ time n)
                              const STensor3& Fn,         // updated deformation gradient (input @ time n+1)
                              STensor3 &P,                // updated 1st Piola-Kirchhoff stress tensor (output)
                                                          // contains the initial values on input
                              const IPNonLocalPorosity *q0,       // array of initial internal variable
                              IPNonLocalPorosity *q1,             // updated array of internal variable (in ipvcur on output),
                              STensor43 &Tangent,         // constitutive tangents (output)
                              std::vector<STensor3>  &dLocalVarDF,
                              std::vector<STensor3>  &dStressDNonLocalVar,
                              fullMatrix<double>    &dLocalVarDNonLocalVar,
                              const bool stiff,                   // if true compute the tangents
                              STensor43& dFedF, std::vector<STensor3>& dFeDNonlocalVar,
                              STensor3*  dStressDT = NULL,
                              fullMatrix<double>* dLocalVarDT = NULL,
                              const double T0 = 0.,const double T = 0.
                             ) const;

    // Case-dependent functions
    // void characteristic Y = [fV Chi W]
    virtual double yieldFunction(const double kcorEq, const double pcor, const double R, const double yieldfV,
                                const IPNonLocalPorosity* q0, const IPNonLocalPorosity* q1, const double* T = NULL,
                                bool diff=false, fullVector<double>* grad = NULL,
                                bool withthermic = false, double* dfdT = NULL) const = 0;

    virtual void plasticFlow(double & Ns, double& Nv,
                              const double kcorEq, const double pcor, const double R,
                              const IPNonLocalPorosity* q0, const IPNonLocalPorosity* q1, const double* T = NULL,
                              bool diff=false, fullVector<double>* gradNs=NULL, // grad of Ns with respect to [kcorEq pcor R fV Chi W]
                              fullVector<double>* gradNv = NULL, // grad of Nv with respect to  [kcorEq pcor R fV Chi W] )
                              bool withTher = false, double* dNsDT = NULL, double* dNvDT=NULL) const = 0;

    // Internal state and failure management
      // Function from mlaw that gathers checks for material status (i.e. for nucleation criterion, coalescence and failure)
    virtual void checkInternalState(IPVariable* ipv, const IPVariable* ipvprev) const;
      /// update nucleation criterion status
    virtual void checkNucleationCriterion(IPNonLocalPorosity* q1, const IPNonLocalPorosity *q0) const;
      /// update coalescence status
    virtual void checkCoalescence(IPNonLocalPorosity* q1, const IPNonLocalPorosity *q0, const double* T) const = 0;
    virtual void forceCoalescence(IPNonLocalPorosity* q1, const IPNonLocalPorosity *q0, const double* T) const = 0;
    virtual bool checkCrackCriterion(IPNonLocalPorosity* q1, const IPNonLocalPorosity *q0, const STensor3& Fcur, const SVector3& normal, double delayFactor, const double* T) const = 0;
      /// update failure status
    virtual void checkFailed(IPNonLocalPorosity* q1, const IPNonLocalPorosity *q0) const = 0;

    virtual double getOnsetCriterion(const IPNonLocalPorosity* q1) const = 0;


    // Void nucleation
    // compute the increment of nucleated porosity
    //! \fixme argument f0 shloud be deleted.
    virtual void nucleation(const double p1, const double p0, const double f0, IPNonLocalPorosity* q1, const IPNonLocalPorosity *q0) const;

    // Void expansion from growth, nucleation and shear
    // in the integrated form, it is a function of
    // plastic defos as DeltaHatD=deviatoric plastic defo, DeltaHatQ = volumetric plastic defor, DeltaHatP=matrixPlasticDefo
    // and kcor
    virtual void localPorosityGrowth(IPNonLocalPorosity *q1,
                      const double DeltaHatD, const double DeltaHatQ, const double DeltaHatP,
                      const STensor3& Kcor, const IPNonLocalPorosity *q0,
                      bool stiff = false,
                      double* DDeltafVDDeltaHatD=NULL, double* DDeltafVDDeltaHatQ=NULL, double* DDeltafVDDeltaHatP=NULL, STensor3* DDeltafVDKcor=NULL) const;
    // void state beside porosity
    virtual void voidStateGrowth(const double yieldfV0, const double yieldfV1, // yield porosity
                        const double DeltahatD, const double DeltahatQ, const double DeltahatP, // internal variables
                        const IPNonLocalPorosity *q0, IPNonLocalPorosity *q1) const = 0;
    
    // void characteristic evolutions
     virtual void voidCharacteristicsEvolution_NonLocalPorosity(const STensor3& kcor,
                                                const IPNonLocalPorosity *q0, IPNonLocalPorosity* q1,
                                                const double DeltaHatD, const double DeltaHatQ, const double DeltaHatP,
                                                double& DfVDDeltaHatD, double& DfVDDeltaHatQ, double& DfVDDeltaHatP,
                                                STensor3& DfVDkcorpr) const;
    
    virtual void voidCharacteristicsEvolutionLocal(const STensor3& kcor,
                                                const IPNonLocalPorosity *q0, IPNonLocalPorosity* q1,
                                                const double DeltaHatD, const double DeltaHatQ, const double DeltaHatP,
                                                const double DtildefVstarDDeltafV,
                                                double& DyieldfVDDeltafV,
                                                double& DDeltafVDDeltaHatD, double& DDeltafVDDeltaHatQ, double& DDeltafVDDeltaHatP,
                                                STensor3& DDeltafVDKcor) const;
    virtual void voidCharacteristicsEvolution_multipleNonLocalVariables(const STensor3& kcor,
                                                const IPNonLocalPorosity *q0, IPNonLocalPorosity* q1,
                                                std::vector<double>& DyieldfVDNonlocalVars,
                                                STensor3& DyieldfVDKcor, const bool blockVoidGrowth
                                                ) const;
     // Constitutive functions
  protected:
    // local and nonlocal vars depend on nonlocal method
    virtual double getLocalVariable(const IPNonLocalPorosity* q, const int idex) const;
    virtual double& getRefToLocalVariable(IPNonLocalPorosity* q, const int idex) const;
    virtual double getNonLocalVariable(const IPNonLocalPorosity* q, const int idex) const;
    virtual double& getRefToNonLocalVariable(IPNonLocalPorosity* q, const int idex) const;

    // Non-local case
    virtual void constitutive_NonLocalPorosity(
                              const STensor3& F0,         // initial deformation gradient (input @ time n)
                              const STensor3& Fn,         // updated deformation gradient (input @ time n+1)
                              STensor3 &P,                // updated 1st Piola-Kirchhoff stress tensor (output)
                                                          // contains the initial values on input
                              const IPNonLocalPorosity *q0,       // array of initial internal variable
                              IPNonLocalPorosity *q1,             // updated array of internal variable (in ipvcur on output),
                              STensor43 &Tangent,         // constitutive tangents (output)
                              STensor3  &dLocalPorosityDStrain,
                              STensor3  &dStressDNonLocalPorosity,
                              double    &dLocalPorosityDNonLocalPorosity,
                              const bool stiff,     // if true compute the tangents
                              STensor43* elasticTangent = NULL) const;

    // Non-local case
    virtual void constitutive_NonLocalLogarithmPorosity(
                              const STensor3& F0,         // initial deformation gradient (input @ time n)
                              const STensor3& Fn,         // updated deformation gradient (input @ time n+1)
                              STensor3 &P,                // updated 1st Piola-Kirchhoff stress tensor (output)
                                                          // contains the initial values on input
                              const IPNonLocalPorosity *q0,       // array of initial internal variable
                              IPNonLocalPorosity *q1,             // updated array of internal variable (in ipvcur on output),
                              STensor43 &Tangent,         // constitutive tangents (output)
                              STensor3  &dLocalPorosityDStrain,
                              STensor3  &dStressDNonLocalPorosity,
                              double    &dLocalPorosityDNonLocalPorosity,
                              const bool stiff,     // if true compute the tangents
                              STensor43* elasticTangent = NULL) const;

    // Multiple non-local var. cases
    virtual void constitutive_multipleNonLocalVariables(
                              const STensor3& F0,         // initial deformation gradient (input @ time n)
                              const STensor3& Fn,         // updated deformation gradient (input @ time n+1)
                              STensor3 &P,                // updated 1st Piola-Kirchhoff stress tensor (output)
                                                          // contains the initial values on input
                              const IPNonLocalPorosity *q0,       // array of initial internal variable
                              IPNonLocalPorosity *q1,             // updated array of internal variable (in ipvcur on output),
                              STensor43 &Tangent,         // constitutive tangents (output)
                              std::vector<STensor3>  &dLocalVarDStrain,
                              std::vector<STensor3>  &dStressDNonLocalVar,
                              fullMatrix<double>    &dLocalVarDNonLocalVar,
                              const bool stiff,
                              STensor43* elasticTangent = NULL) const;

    // Elasticity-related functions
    virtual double deformationEnergy(const STensor3 &C,const double* T = NULL) const;
    virtual void getDHookTensorDT(STensor43& DHDT, double T) const;


    // Update internal variables during corrector iterations
    virtual void updateInternalVariables(IPNonLocalPorosity *q1, const IPNonLocalPorosity* q0,
                              const double DeltaHatD, const double DeltaHatQ, const double DeltaHatP, bool withPlasticInstability,
                              const double* T = NULL) const;

    // Update plastic state at the end of a plastic corrector step (and check for coalescence)
    virtual void updatePlasticState(IPNonLocalPorosity *q1, const IPNonLocalPorosity* q0,  const STensor3& F1,
                              const double DeltaHatD, const double DeltaHatQ, const double DeltaHatP,
                              const STensor3& devKcorpr, const double kcorEqpr, const double pcorpr, const double yield,
                              STensor3& Fe1, STensor3& Ce, STensor3& Ee, STensor43& Le, STensor63& dLe,
                              STensor3& DGNp, STensor3& devDGNp, double& trDGNp, STensor43& Dexp,
                              const double* T=NULL) const;


    virtual void predictorCorrector_NonLocalLogarithmPorosity(const STensor3& F0,         // initial deformation gradient (input @ time n)
                              const STensor3& Fn,         // updated deformation gradient (input @ time n+1)
                              STensor3 &P,                // updated 1st Piola-Kirchhoff stress tensor (output)
                                                          // contains the initial values on input
                              const IPNonLocalPorosity *q0,       // array of initial internal variable
                              IPNonLocalPorosity *q1,             // updated array of internal variable (in ipvcur on output),
                              STensor43 &Tangent,         // constitutive tangents (output)
                              STensor3  &dLocalPorosityDStrain,
                              STensor3  &dStressDNonLocalPorosity,
                              double    &dLocalPorosityDNonLocalPorosity,
                              const bool stiff,                   // if true compute the tangents
                              STensor43& dFedF, STensor3& dFeDNonlocalVar,
                              STensor3*  dStressDT = NULL,
                              double* dLocalPorosityDT = NULL,
                              const double T0 = 0.,const double T = 0.
                             ) const;

     // when considereing nonlocal porosity,
    virtual void computeResidual_NonLocalPorosity(SVector3& res, STensor3& J, const STensor3& F,
                  const double kcorEqpr, const double pcorpr, const double R, const double H,
                  const double yieldfV,
                  const IPNonLocalPorosity *q0, const IPNonLocalPorosity* q1,
                  const double DeltaHatD, const double DeltaHatQ, const double DeltaHatP, // 3 unknonwn
                  const double* T = NULL
                ) const;

    virtual void computeDResidual_NonLocalPorosity(STensor3 &dres0dEpr, STensor3 &dres1dEpr, STensor3 &dres2dEpr,
                                    double &dres0dtildefV, double &dres1dtildefV, double &dres2dtildefV,
                                    const STensor3& F,
                                    const STensor3& devKcorpr, const double kcorEqpr, const double pcorpr, const double R,
                                    const double yieldfV, const STensor3& DyieldfVDKcorpr, const double DyieldfVDtildefV,
                                    const STensor43& DKcorprDEpr, const STensor43& DdevKcorprDEpr, const STensor3& DpcorprDEpr,
                                    const IPNonLocalPorosity *q0, const IPNonLocalPorosity* q1,
                                    const double DeltaHatD, const double DeltaHatQ, const double DeltaHatP,
                                    const double* T = NULL, const double* DRDT= NULL,
                                    const STensor3* DdevKcorprDT = NULL, const double* DpcorprDT = NULL, const STensor3* DKcorprDT = NULL,
                                    double *dres0dT= NULL, double *dres1dT= NULL, double *dres2dT = NULL
                                    ) const;


    // Predictor-corrector and plastic corrector for non-local case
    virtual void predictorCorrector_NonLocalPorosity(const STensor3& F0,         // initial deformation gradient (input @ time n)
                              const STensor3& Fn,         // updated deformation gradient (input @ time n+1)
                              STensor3 &P,                // updated 1st Piola-Kirchhoff stress tensor (output)
                                                          // contains the initial values on input
                              const IPNonLocalPorosity *q0,       // array of initial internal variable
                              IPNonLocalPorosity *q1,             // updated array of internal variable (in ipvcur on output),
                              STensor43 &Tangent,         // constitutive tangents (output)
                              STensor3  &dLocalPorosityDStrain,
                              STensor3  &dStressDNonLocalPorosity,
                              double    &dLocalPorosityDNonLocalPorosity,
                              const bool stiff,                   // if true compute the tangents
                              STensor43& dFedF, STensor3& dFeDtildefV,
                              STensor3*  dStressDT = NULL,
                              double* dLocalPorosityDT = NULL,
                              const double T0 = 0.,const double T = 0.
                             ) const;

   
    virtual bool plasticCorrector_NonLocalPorosity(const STensor3& F1,
                              const double& yieldfV,const double& DyieldfVDtildefV,
                              const STensor3& Kcorpr, const STensor3& devKcorpr,  const double& pcorpr,
                              const IPNonLocalPorosity *q0, IPNonLocalPorosity *q1,
                              STensor3& Fe, STensor3& Ce, STensor3& Ee, STensor43& Le, STensor63& dLe,
                              const bool stiff,
                              const STensor43& DKcorprDEpr, const STensor43& DdevKcorprDEpr, const STensor3& DpcorprDEpr, // predictor elastic tangent
                              STensor43& DKcorDEpr, STensor3& DKcorDtildefV,
                              STensor43& DFpDEpr, STensor3& DFpDtildefV,
                              STensor3& DfVDEpr, double& DfVDtildefV,
                              const double* T = NULL,
                              const STensor3* DKcorprDT = NULL, const STensor3* DdevKcorprDT = NULL, const double* DpcorprDT = NULL,
                              STensor3* DKcorDT= NULL, STensor3* DFpDT = NULL, double* DfVDT = NULL
                            ) const;
    virtual void computeResidualLocal(SVector3& res, STensor3& J, const STensor3& F,
                                    const double kcorEqpr, const double pcorpr, const double R, const double H,
                                    const double yieldfV,  const double DyieldfVDfV,
                                    const IPNonLocalPorosity *q0, const IPNonLocalPorosity* q1,
                                    const double DeltaHatD, const double DeltaHatQ, const double DeltaHatP, // 3 unknonwn
                                    const double DDeltafVDDeltaHatD, const double DDeltafVDDeltaHatQ, const double DDeltafVDDeltaHatP,
                                    const double* T = NULL
                                  ) const;

    virtual void computeDResidualLocal(STensor3 &dres0dEpr, STensor3 &dres1dEpr, STensor3 &dres2dEpr, const STensor3& F,
                                    const STensor3& devKcorpr, const double kcorEqpr, const double pcorpr, const double R,
                                    const double yieldfV, const double DyieldfVDfV,
                                    const STensor3& DDeltafVDEpr, const STensor43& DdevKcorprDEpr, const STensor3& DpcorprDEpr,
                                    const IPNonLocalPorosity *q0, const IPNonLocalPorosity* q1,
                                    const double DeltaHatD, const double DeltaHatQ, const double DeltaHatP,
                                    const double* T = NULL, const double* DRDT= NULL,
                                    const STensor3* DdevKcorprDT = NULL, const double* DpcorprDT = NULL, const double* DDeltafVDT = NULL,
                                    double *dres0dT= NULL, double *dres1dT= NULL, double *dres2dT = NULL
                                    ) const;

    // Predictor-corrector and plastic corrector for local case
    virtual void predictorCorrectorLocal(const STensor3& F0,         // initial deformation gradient (input @ time n)
                              const STensor3& Fn,         // updated deformation gradient (input @ time n+1)
                              STensor3 &P,                // updated 1st Piola-Kirchhoff stress tensor (output)
                                                          // contains the initial values on input
                              const IPNonLocalPorosity *q0,       // array of initial internal variable
                              IPNonLocalPorosity *q1,             // updated array of internal variable (in ipvcur on output),
                              STensor43 &Tangent,         // constitutive tangents (output)
                              const bool stiff           // if true compute the tangents
                             ) const;

    virtual bool plasticCorrectorLocal(const STensor3& F1, const double& tildefVstarPrev,const double& DtildefVstarDDeltafV,
                                    const STensor3& Kcorpr, const STensor3& devKcorpr,  const double& pcorpr,
                                    const IPNonLocalPorosity *q0, IPNonLocalPorosity *q1,
                                    STensor3& Fe, STensor3& Ce, STensor3& Ee, STensor43& Le, STensor63& dLe,
                                    const bool stiff,
                                    const STensor43& DKcorprDEpr, const STensor43& DdevKcorprDEpr, const STensor3& DpcorprDEpr, // predictor elastic tangent
                                    STensor43& DKcorDEpr,
                                    STensor43& DFpDEpr,
                                    const double* T = NULL,
                                    const STensor3* DKcorprDT = NULL, const STensor3* DdevKcorprDT = NULL, const double* DpcorprDT = NULL,
                                    STensor3* DKcorDT= NULL, STensor3* DFpDT = NULL
                                  ) const;


    // Tangent-related functions
    virtual void computePlasticTangentLocal(const IPNonLocalPorosity *q1, const IPNonLocalPorosity* q0,
                const STensor3& devKcorpr, const double kcorEqpr, const double pcorpr,
                const STensor3& DGNp, const STensor3& devDGNp, const double trDGNp, const STensor43& Dexp,
                const double DeltaHatD, const double DeltaHatQ, const double DeltaHatP,
                const STensor43& DKcorprDEpr, const STensor43& DdevKcorprDEpr, const STensor3& DpcorprDEpr,
                const STensor3& DDeltaHatDDEpr, const STensor3& DDeltaHatQDEpr, const STensor3& DDeltaHatPDEpr,
                STensor43& DKcorDEpr, STensor43& DFpDEpr,
                const double* T = NULL,
                const STensor3* DKcorprDT= NULL, const STensor3* DdevKcorprDT= NULL, const double* DpcorprDT= NULL,
                const double* DDeltaHatDDT= NULL, const double* DDeltaHatQDT= NULL, const double* DDeltaHatPDT= NULL,
                STensor3* DKcorDT= NULL, STensor3* DFpDT= NULL
              ) const;

    virtual void computePlasticTangentNonLocal(const IPNonLocalPorosity *q1, const IPNonLocalPorosity* q0,
                const STensor3& devKcorpr, const double kcorEqpr, const double pcorpr,
                const STensor3& DGNp, const STensor3& devDGNp, const double trDGNp, const STensor43& Dexp,
                const double DeltaHatD, const double DeltaHatQ, const double DeltaHatP,
                const STensor43& DKcorprDEpr, const STensor43& DdevKcorprDEpr, const STensor3& DpcorprDEpr,
                const STensor3& DDeltaHatDDEpr, const STensor3& DDeltaHatQDEpr, const STensor3& DDeltaHatPDEpr,
                STensor43& DKcorDEpr, STensor43& DFpDEpr,
                const double DDeltaHatDDtildefV, const double DDeltaHatQDtildefV, const double DDeltaHatPDtildefV,
                STensor3& DKcorDtildefV, STensor3& DFpDtildefV,
                const double* T= NULL, const STensor3* DKcorprDT= NULL, const STensor3* DdevKcorprDT= NULL, const double* DpcorprDT= NULL,
                const double* DDeltaHatDDT= NULL, const double* DDeltaHatQDT= NULL, const double* DDeltaHatPDT= NULL,
                STensor3* DKcorDT= NULL, STensor3* DFpDT= NULL
              ) const;


    virtual void tangentComputationLocal(STensor43& dStressDF,
                                const bool plastic,
                                const STensor3& F,  // current F
                                const STensor3& corKir,  // cor Kir
                                const STensor3& S, //  second PK
                                const STensor3& Fepr, const STensor3& Fppr, // predictor
                                const STensor43& Lpr,
                                const STensor3& Fe, const STensor3& Fp, // corrector
                                const STensor43& L, const STensor63& dL, // corrector value
                                const STensor43& DcorKirDEepr,
                                const STensor43& dFpDEepr,
                                const STensor43& EprToF, const STensor3& Fpinv
                                ) const;
      //----------------------added
    virtual void tangentComputationLocalWithTemperature(STensor43& dStressDF,STensor3& dStressDT,
                                const bool plastic,
                                const STensor3& F,  // current F
                                const STensor3& corKir,  // cor Kir
                                const STensor3& S, //  second PK
                                const STensor3& Fepr, const STensor3& Fppr, // predictor
                                const STensor43& Lpr,
                                const STensor3& Fe, const STensor3& Fp, // corrector
                                const STensor43& L, const STensor63& dL, // corrector value
                                const STensor43& DcorKirDEepr, const STensor3& DcorKirDT, // small strain tangents
                                const STensor43& dFpDEepr, const STensor3& DFpDT,
                                const STensor43& EprToF, const STensor3& Fpinv
                                ) const;

      //----------------------end


    virtual void tangentComputation(STensor43& dFedF, STensor3& dFeDtildefV, STensor43& dStressDF, STensor3& dStressDtildefV,
                                STensor3& DLocalPorositydF,
                                const bool plastic,
                                const STensor3& F,  // current F
                                const STensor3& corKir,  // cor Kir
                                const STensor3& S, //  second PK
                                const STensor3& Fepr, const STensor3& Fppr, // predictor
                                const STensor43& Lpr,
                                const STensor3& Fe, const STensor3& Fp, // corrector
                                const STensor43& L, const STensor63& dL, // corrector value
                                const STensor43& DcorKirDEepr, const STensor3& DcorKirDtildefV,  // small strain tangents
                                const STensor43& dFpDEepr, const STensor3& DFpDtildefV,
                                const STensor3& DfVDEpr,
                                const STensor43& EprToF, const STensor3& Fpinv
                                ) const;

    virtual void tangentComputation(STensor43& dFedF, std::vector<STensor3>& dFeDNonlocalVar, STensor43& dStressDF,
                                std::vector<STensor3>& dStressDNonlocalVar,
                                std::vector<STensor3>& DLocalVardF,
                                const bool plastic,
                                const STensor3& F,  // current F
                                const STensor3& corKir,  // cor Kir
                                const STensor3& S, //  second PK
                                const STensor3& Fepr, const STensor3& Fppr, // predictor
                                const STensor43& Lpr,
                                const STensor3& Fe, const STensor3& Fp, // corrector
                                const STensor43& L, const STensor63& dL, // corrector value
                                const STensor43& DcorKirDEepr, const std::vector<STensor3>& DcorKirDNonlocalVar,  // small strain tangents
                                const STensor43& dFpDEepr, const std::vector<STensor3>& DFpDNonlocalVar,
                                const std::vector<STensor3>& DLocalVarDEpr,
                                const STensor43& EprToF, const STensor3& Fpinv
                                ) const;
  //---------------------------------------------------------------------------------------------------------added
    virtual void tangentComputationWithTemperature(STensor43& dFedF, STensor3& dFeDtildefV, STensor43& dStressDF,
                              STensor3& dStressDtildefVstar, STensor3& dStressDT,
                                STensor3& DLocalPorositydF,
                                const bool plastic,
                                const STensor3& F,  // current F
                                const STensor3& corKir,  // cor Kir
                                const STensor3& S, //  second PK
                                const STensor3& Fepr, const STensor3& Fppr, // predictor
                                const STensor43& Lpr,
                                const STensor3& Fe, const STensor3& Fp, // corrector
                                const STensor43& L, const STensor63& dL, // corrector value
                                const STensor43& DcorKirDEepr, const STensor3& DcorKirDtildefVstar, const STensor3& DcorKirDT, // small strain tangents
                                const STensor43& dFpDEepr, const STensor3& DFpDtildefVstar, const STensor3& DFpDT,
                                const STensor3& DfVDEpr,
                                const STensor43& EprToF, const STensor3& Fpinv
                                ) const;


    virtual void tangentComputationWithTemperature(STensor43& dFedF, std::vector<STensor3>& dFeDNonlocalVar, STensor43& dStressDF,
                                std::vector<STensor3>& dStressDNonlocalVar, STensor3& dStressDT,
                                std::vector<STensor3>& DLocalVardF,
                                const bool plastic,
                                const STensor3& F,  // current F
                                const STensor3& corKir,  // cor Kir
                                const STensor3& S, //  second PK
                                const STensor3& Fepr, const STensor3& Fppr, // predictor
                                const STensor43& Lpr,
                                const STensor3& Fe, const STensor3& Fp, // corrector
                                const STensor43& L, const STensor63& dL, // corrector value
                                const STensor43& DcorKirDEepr, const std::vector<STensor3>& DcorKirDNonlocalVar, const STensor3& DcorKirDT, // small strain tangents
                                const STensor43& dFpDEepr, const std::vector<STensor3>& DFpDNonlocalVar, const STensor3& DFpDT,
                                const std::vector<STensor3>& DLocalVarDEpr,
                                const STensor43& EprToF, const STensor3& Fpinv
                                ) const;

    virtual void computePlasticTangentNonLocal_multipleNonLocalVariables(const IPNonLocalPorosity *q1, const IPNonLocalPorosity* q0,
                const STensor3& devKcorpr, const double kcorEqpr, const double pcorpr,
                const STensor3& DGNp, const STensor3& devDGNp, const double trDGNp, const STensor43& Dexp,
                const double DeltaHatD, const double DeltaHatQ, const double DeltaHatP,
                const STensor43& DKcorprDEpr, const STensor43& DdevKcorprDEpr, const STensor3& DpcorprDEpr,
                const STensor3& DDeltaHatDDEpr, const STensor3& DDeltaHatQDEpr, const STensor3& DDeltaHatPDEpr,
                STensor43& DKcorDEpr, STensor43& DFpDEpr,
                const std::vector<double>& DDeltaHatDDNonLocalVar, const std::vector<double>& DDeltaHatQDNonLocalVar, const std::vector<double>& DDeltaHatPDNonLocalVar,
                std::vector<STensor3>& DKcorDNonLocalVar, std::vector<STensor3>& DFpDNonLocalVar,
                const double* T= NULL, const STensor3* DKcorprDT= NULL, const STensor3* DdevKcorprDT= NULL, const double* DpcorprDT= NULL,
                const double* DDeltaHatDDT= NULL, const double* DDeltaHatQDT= NULL, const double* DDeltaHatPDT= NULL,
                STensor3* DKcorDT= NULL, STensor3* DFpDT= NULL
              ) const;

     // Multiple nonlocal version
    virtual void computeResidual_multipleNonLocalVariables(SVector3& res, STensor3& J, const STensor3& F,
                                  const double kcorEqpr, const double pcorpr, const double R, const double H,
                                  const double yieldfV,
                                  const IPNonLocalPorosity *q0, const IPNonLocalPorosity* q1,
                                  const double DeltaHatD, const double DeltaHatQ, const double DeltaHatP, // 3 unknonwn
                                  const double* T = NULL
                                ) const;


    virtual void computeDResidual_multipleNonLocalVariables(STensor3 &dres0dEpr, STensor3 &dres1dEpr, STensor3 &dres2dEpr,const STensor3& F,
                                  std::vector<double> &dres0dNonlocalVars, std::vector<double> &dres1dNonlocalVars, std::vector<double> &dres2dNonlocalVars,
                                  const STensor3& devKcorpr, const double kcorEqpr, const double pcorpr, const double R,
                                  const double yieldfV, const STensor3& DyieldfVDkcorpr, const std::vector<double>& DyieldfVDNonlocalVars,
                                  const STensor43& DkcorprDEpr, const STensor43& DdevKcorprDEpr, const STensor3& DpcorprDEpr,
                                  const IPNonLocalPorosity *q0, const IPNonLocalPorosity* q1,
                                  const double DeltaHatD, const double DeltaHatQ, const double DeltaHatP,
                                  const double* T = NULL, const double* DRDT= NULL,
                                  const STensor3* DdevKcorprDT = NULL, const double* DpcorprDT = NULL, const STensor3* DKcorprVDT = NULL,
                                  double *dres0dT= NULL, double *dres1dT= NULL, double *dres2dT = NULL
                                  ) const;
    
    
    virtual bool plasticCorrector_multipleNonLocalVariables(const STensor3& F1,
                                    const double& yieldfV,const std::vector<double>& DyieldfVDNonlocalVar, const STensor3& DyieldfVDKcorpr,
                                    const STensor3& Kcorpr, const STensor3& devKcorpr,  const double& pcorpr,
                                    const IPNonLocalPorosity *q0, IPNonLocalPorosity *q1,
                                    STensor3& Fe, STensor3& Ce, STensor3& Ee, STensor43& Le, STensor63& dLe,
                                    const bool stiff,
                                    const STensor43& DKcorprDEpr, const STensor43& DdevKcorprDEpr, const STensor3& DpcorprDEpr, // predictor elastic tangent
                                    STensor43& DKcorDEpr, std::vector<STensor3>& DKcorDnonlocalVar,
                                    STensor43& DFpDEpr, std::vector<STensor3>& DFpDnonlocalVar,
                                    std::vector<STensor3>& DlocalVarDEpr, fullMatrix<double>& DlocalVarDnonlocalVar,
                                    const double* T = NULL,
                                    const STensor3* DKcorprDT = NULL, const STensor3* DdevKcorprDT = NULL, const double* DpcorprDT = NULL,
                                    STensor3* DKcorDT= NULL, STensor3* DFpDT = NULL, fullMatrix<double>* DlocalVarDT = NULL
                                  ) const;

    virtual void copyInternalVariableForElasticState_multipleNonLocalVariables(IPNonLocalPorosity* q1, const IPNonLocalPorosity* q0, const double T) const;
    virtual void predictorCorrector_multipleNonLocalVariables(const STensor3& F0,         // initial deformation gradient (input @ time n)
                              const STensor3& Fn,         // updated deformation gradient (input @ time n+1)
                              STensor3 &P,                // updated 1st Piola-Kirchhoff stress tensor (output)
                                                          // contains the initial values on input
                              const IPNonLocalPorosity *q0,       // array of initial internal variable
                              IPNonLocalPorosity *q1,             // updated array of internal variable (in ipvcur on output),
                              STensor43 &Tangent,         // constitutive tangents (output)
                              std::vector<STensor3>  &dLocalVarDF,
                              std::vector<STensor3>  &dStressDNonLocalVar,
                              fullMatrix<double>    &dLocalVarDNonLocalVar,
                              const bool stiff,                   // if true compute the tangents
                              STensor43& dFedF, std::vector<STensor3>& dFeDNonLocalVar,
                              STensor3*  dStressDT = NULL,
                              fullMatrix<double>* dLocalVarDT = NULL,
                              const double T0 = 0.,const double T = 0.
                             ) const;

  public:
    virtual int getNumOfYieldSurfaces() const;
    virtual int getNumOfVoidCharacteristics() const;
    // Y = [yieldfV Chi W gamma]
    virtual double I1J2J3_yieldFunction(const STensor3& kcor, const double R, const IPNonLocalPorosity* q0, const IPNonLocalPorosity* q1, const double* T = NULL,
                                        bool diff=false, STensor3* DFDkcor=NULL, double* DFDR=NULL, std::vector<double>* DFDY=NULL,
                                        bool withThermic=false, double *DFDT=NULL) const=0;
    virtual void I1J2J3_getYieldNormal(STensor3& Np, const STensor3& kcor, const double R, const IPNonLocalPorosity* q0, const IPNonLocalPorosity* q1, const double* T = NULL,
                                        bool diff=false, STensor43* DNpDkcor=NULL, STensor3* DNpDR=NULL, std::vector<STensor3>* DNpDY=NULL,
                                        bool withThermic=false, STensor3* DNpDT=NULL
                                        ) const=0;
  public:
    virtual bool I1J2J3_withPlastic(const STensor3& F, const STensor3& kcor, const double R, const IPNonLocalPorosity* q0, const IPNonLocalPorosity* q1, const double* T = NULL) const;

    virtual void I1J2J3_computeResidual(const STensor3& DeltaEp, const std::vector<double>& DeltaPlasticMult, const double DeltaHatP, // unknown value
                                        const STensor3& sig,  const double R, const IPNonLocalPorosity* q0, const IPNonLocalPorosity* q1,
                                        const std::vector<double>& Y, // void parameters
                                        std::vector<double>& res0, STensor3& res1, double& res2,
                                        bool stiff,
                                        const STensor43& DsigDDeltaEp, const double H,
                                        const std::vector<STensor3>& DYDsig, // stress -dependence
                                        std::vector<STensor3>& Dres0DDeltaEp, STensor43& Dres1DDeltaEp, STensor3& Dres2DDeltaEp,
                                        std::vector<double>& Dres0DDeltaPlasticMult,std::vector<STensor3>& Dres1DDeltaPlasticMult, std::vector<double>& Dres2DDeltaPlasticMult,
                                        std::vector<double>& Dres0DDeltaHatP, STensor3& Dres1DDeltaHatP, double& Dres2DDeltaHatP,
                                        const double* T=NULL) const;
     virtual void I1J2J3_computeResidualLocal(const STensor3& DeltaEp, const std::vector<double>& DeltaPlasticMult, const double DeltaHatP, // unknown value
                                        const STensor3& sig,  const double R, const IPNonLocalPorosity* q0, const IPNonLocalPorosity* q1,
                                        const std::vector<double>& Y, // void parameters
                                        std::vector<double>& res0, STensor3& res1, double& res2,
                                        bool stiff,
                                        const STensor43& DsigDDeltaEp, const double H,
                                        const std::vector<STensor3>& DYDsig, // stress -dependence
                                        const std::vector<STensor3>& DYDDeltaEp,
                                        const std::vector<double>& DYDDeltaHatP,
                                        std::vector<STensor3>& Dres0DDeltaEp, STensor43& Dres1DDeltaEp, STensor3& Dres2DDeltaEp,
                                        std::vector<double>& Dres0DDeltaPlasticMult,std::vector<STensor3>& Dres1DDeltaPlasticMult, std::vector<double>& Dres2DDeltaPlasticMult,
                                        std::vector<double>& Dres0DDeltaHatP, STensor3& Dres1DDeltaHatP, double& Dres2DDeltaHatP,
                                        const double* T=NULL) const;
    virtual void I1J2J3_computeDResidual(const STensor3& DeltaEp, const std::vector<double>& DeltaPlasticMult, const double DeltaHatP, // unknown value
                                const STensor3& sig, const double R,  const IPNonLocalPorosity* q0, const IPNonLocalPorosity* q1,
                                const STensor43& DsigDEepr, const double H, const double dwp,
                                const std::vector<double>& Y, // void parameters
                                const std::vector<STensor3>& DYDsig,
                                const std::vector<std::vector<double> >& DYDnonlocalVars,
                                std::vector<std::vector<double> >& Dres0DNonlocalVars, std::vector<STensor3>& Dres0DEepr,
                                std::vector<STensor3>& Dres1DNonlocalVars, STensor43& Dres1DEepr,
                                std::vector<double>& Dres2DNonlocalVars, STensor3& Dres2DEepr,
                                const double* T=NULL, const double* DRDT=NULL, const STensor3* DsigDT=NULL,
                                std::vector<double>* Dres0DT=NULL, STensor3* Dres1DT=NULL, double* Dres2DT=NULL) const;
                                
    virtual void I1J2J3_computeDResidualLocal(const STensor3& DeltaEp, const std::vector<double>& DeltaPlasticMult, const double DeltaHatP, // unknown value
                                const STensor3& sig, const double R,  const IPNonLocalPorosity* q0, const IPNonLocalPorosity* q1,
                                const STensor43& DsigDEepr, const double H,
                                const std::vector<double>& Y, // void parameters
                                const std::vector<STensor3>& DYDsig,
                                std::vector<STensor3>& Dres0DEepr, STensor43& Dres1DEepr, STensor3& Dres2DEepr,
                                const double* T=NULL, const double* DRDT=NULL, const STensor3* DsigDT=NULL,
                                std::vector<double>* Dres0DT=NULL, STensor3* Dres1DT=NULL, double* Dres2DT=NULL) const;

    virtual bool I1J2J3_plasticCorrector_NonLocal(const STensor3& F1,
                                  const STensor3& Kcorpr,
                                  const IPNonLocalPorosity *q0, IPNonLocalPorosity *q1,
                                  STensor3& Fe, STensor3& Ce, STensor3& Ee, STensor43& Le, STensor63& dLe,
                                  const bool stiff,
                                  const STensor43& DKcorprDEepr, // predictor elastic tangent
                                  STensor43& DKcorDEepr, std::vector<STensor3>& DKcorDnonlocalVar,
                                  STensor43& DFpDEepr, std::vector<STensor3>& DFpDnonlocalVar,
                                  std::vector<STensor3>& DlocalVarDEepr, fullMatrix<double>& DlocalVarDnonlocalVar,
                                  const double* T,
                                  const STensor3* DKcorprDT,
                                  STensor3* DKcorDT, STensor3* DFpDT, fullMatrix<double>* DlocalVarDT
                                  ) const;
    virtual bool I1J2J3_plasticCorrectorLocal(const STensor3& F1,
                                  const STensor3& Kcorpr,
                                  const IPNonLocalPorosity *q0, IPNonLocalPorosity *q1,
                                  STensor3& Fe, STensor3& Ce, STensor3& Ee, STensor43& Le, STensor63& dLe,
                                  const bool stiff,
                                  const STensor43& DKcorprDEepr, // predictor elastic tangent
                                  STensor43& DKcorDEepr, STensor43& DFpDEepr, 
                                  const double* T,
                                  const STensor3* DKcorprDT,
                                  STensor3* DKcorDT, STensor3* DFpDT
                                  ) const;
    virtual void I1J2J3_copyInternalVariableForElasticState(IPNonLocalPorosity* q1, const IPNonLocalPorosity* q0, const double T) const;
    virtual void I1J2J3_predictorCorrector_NonLocal(const STensor3& F0,         // initial deformation gradient (input @ time n)
                            const STensor3& F1,         // updated deformation gradient (input @ time n+1)
                            STensor3 &P,                // updated 1st Piola-Kirchhoff stress tensor (output)
                            const IPNonLocalPorosity *ipvprev,       // array of initial internal variable
                            IPNonLocalPorosity *ipvcur,             // updated array of internal variable (in ipvcur on output),
                            STensor43 &Tangent,         // constitutive tangents (output)
                            std::vector<STensor3>  &dLocalVarDF,
                            std::vector<STensor3>  &dStressDNonLocalVar,
                            fullMatrix<double>    &dLocalVarDNonLocalVar,
                            const bool stiff,                   // if true compute the tangents
                            STensor43& dFedF, std::vector<STensor3>& dFeDNonLocalVar,
                            STensor3*  dStressDT=NULL,
                            fullMatrix<double>* dLocalVarDT=NULL,
                            const double T0 =0,const double T=0
                           ) const;
    virtual void I1J2J3_copyInternalVariableForElasticStateLocal(IPNonLocalPorosity* q1, const IPNonLocalPorosity* q0, const double T) const;
    virtual void I1J2J3_predictorCorrectorLocal(
                            const STensor3& F0,         // initial deformation gradient (input @ time n)
                            const STensor3& F1,         // updated deformation gradient (input @ time n+1)
                            STensor3 &P,                // updated 1st Piola-Kirchhoff stress tensor (output)
                            const IPNonLocalPorosity *ipvprev,       // array of initial internal variable
                            IPNonLocalPorosity *ipvcur,             // updated array of internal variable (in ipvcur on output),
                            STensor43 &Tangent,         // constitutive tangents (output)
                            const bool stiff            // if true compute the tangents
                           ) const;

    virtual void I1J2J3_constitutive_NonLocal(
                              const STensor3& F0,         // initial deformation gradient (input @ time n)
                              const STensor3& F1,         // updated deformation gradient (input @ time n+1)
                              STensor3 &P,                // updated 1st Piola-Kirchhoff stress tensor (output)
                              const IPNonLocalPorosity *ipvprev,       // array of initial internal variable
                              IPNonLocalPorosity *ipvcur,             // updated array of internal variable (in ipvcur on output),
                              STensor43 &Tangent,         // constitutive tangents (output)
                              std::vector<STensor3>  &dLocaldVarDStrain,
                              std::vector<STensor3>  &dStressDNonLocalVar,
                              fullMatrix<double>    &dLocalVarDNonLocalVar,
                              const bool stiff,            // if true compute the tangents
                             STensor43* elasticTangent = NULL) const;
    
                              
    virtual void I1J2J3_voidCharacteristicsEvolution_NonLocal(const STensor3& sig,
                                                  const IPNonLocalPorosity *q0, IPNonLocalPorosity* q1,
                                                  bool stiff = false,
                                                  std::vector<double>* Y=NULL,
                                                  std::vector<STensor3>* DYDsig = NULL,
                                                  std::vector<std::vector<double> >* DYDNonlocalVars = NULL
                                                  ) const;
    virtual void I1J2J3_voidCharacteristicsEvolution_Local(const STensor3& sig, const STensor3& DeltaEp, const double DeltaHatP,
                                                  const IPNonLocalPorosity *q0, IPNonLocalPorosity* q1,
                                                  bool stiff = false,
                                                  std::vector<double>* Y = NULL,
                                                  std::vector<STensor3>* DYDsig = NULL,
                                                  std::vector<STensor3>* DYDDeltaEp = NULL,
                                                  std::vector<double>* DYDDeltaHatP = NULL
                                                  ) const;
  protected:
    virtual void I1J2J3_updateInternalVariables(IPNonLocalPorosity *q1, const IPNonLocalPorosity* q0,
                            const STensor3& DeltaEp, const double DeltaHatP, const double* T=NULL) const;
                            
    virtual void I1J2J3_voidCharacteristicsEvolution(const STensor3& sig, double DeltaHatQ, double DeltaHatP, double DeltaHatD,
                                                  const IPNonLocalPorosity *q0, IPNonLocalPorosity* q1,
                                                  bool stiff = false,
                                                  std::vector<double>* DyieldfVDVars = NULL,
                                                  STensor3* DyieldfVDsig = NULL
                                                  ) const;

    virtual void I1J2J3_getVoidCharacteristics(std::vector<double>& Y,
                                                std::vector<STensor3>& DYDsig,
                                                std::vector<std::vector<double> >& DYDnonlocalVars,
                                                const IPNonLocalPorosity *q0,  const IPNonLocalPorosity* q1,
                                                const std::vector<double>& DyieldfVDNonlocalVars,
                                                const STensor3& DyieldfVDsig) const;

    virtual void I1J2J3_getResAndJacobian(fullVector<double>& res, fullMatrix<double>& Jac,
                                const std::vector<double>& res0, const STensor3& res1, const double& res2,
                                const std::vector<STensor3>& Dres0DDeltaEp, const STensor43& Dres1DDeltaEp, const STensor3& Dres2DDeltaEp,
                                const std::vector<double>& Dres0DDeltaPlasticMult, const std::vector<STensor3>& Dres1DDeltaPlasticMult, const std::vector<double>& Dres2DDeltaPlasticMult,
                                const std::vector<double>& Dres0DDeltaHatP, const STensor3& Dres1DDeltaHatP,const double& Dres2DDeltaHatP) const;

    virtual void I1J2J3_updatePlasticState(IPNonLocalPorosity *q1, const IPNonLocalPorosity* q0,  const STensor3& F1, const STensor3& kcor,
                            const double R,  const STensor3& DeltaEp, const double DeltaHatP, const double H,
                            STensor3& Fe1, STensor3& Ce, STensor3& Ee, STensor43& Le, STensor63& dLe, STensor43& DFpDDeltaEp, const double* T=NULL) const;
    virtual void I1J2J3_getNonLocalVars(std::vector<double>& Vars,  const IPNonLocalPorosity* q) const;
    
    
    // anisotropy
    virtual void I1J2J3_voidCharacteristicsEvolution_NonLocal_aniso(const STensor3& sig,
                                                const IPNonLocalPorosity *q0, IPNonLocalPorosity* q1,
                                                bool stiff = false,
                                                std::vector<double>* Y = NULL,
                                                std::vector<STensor3>* DYDsig = NULL,
                                                std::vector<std::vector<double> >* DYDNonlocalVars = NULL
                                                ) const
    {
       Msg::Error("mlawNonLocalPorosity::I1J2J3_voidCharacteristicsEvolution_NonLocal_aniso has not been implemented");     
    };
    virtual const STensor43& getAnisotropicTensorReferenceConfiguration() const 
    {
        Msg::Error("mlawNonLocalPorosity::getAnisotropicTensorReferenceConfiguration has not been implemented");  
    };
    virtual bool computeRotatedAnisotropicTensor(const STensor3& F, const STensor3& DeltaEp,  const IPNonLocalPorosity* q0, IPNonLocalPorosity* q1, bool stiff) const;
    virtual bool computeAnisotropyRotation(STensor3& Rtotal, const STensor3& F, const STensor3& DeltaEp, const STensor3& Fp0, 
                                                bool stiff=false, STensor43* DRtotalDF=NULL, STensor43* DRtotalDDeltaEp=NULL) const;
    
    virtual double I1J2J3_yieldFunction_aniso(const STensor3& kcor, const double R, const IPNonLocalPorosity* q0, const IPNonLocalPorosity* q1, const double* T = NULL,
                                        bool diff=false, STensor3* DFDkcor=NULL, double* DFDR=NULL, std::vector<double>* DFDY=NULL, 
                                        STensor3* DFDDeltaEp = NULL, STensor3* DFDFdefo = NULL, 
                                        bool withThermic=false, double *DFDT=NULL) const
    {
      Msg::Error("mlawNonLocalPorosity::I1J2J3_yieldFunction_aniso has not been implemented");
      return 0;
    };
    virtual void I1J2J3_getYieldNormal_aniso(STensor3& Np, const STensor3& kcor, const double R, const IPNonLocalPorosity* q0, const IPNonLocalPorosity* q1, const double* T = NULL,
                                        bool diff=false, STensor43* DNpDkcor=NULL, STensor3* DNpDR=NULL, std::vector<STensor3>* DNpDY=NULL,
                                        STensor43* DNpDDeltaEp=NULL, STensor43* DNpDFdefo=NULL,
                                        bool withThermic=false, STensor3* DNpDT=NULL  
                                        ) const
    {
      Msg::Error("mlawNonLocalPorosity::I1J2J3_getYieldNormal_aniso has not been implemented");
    };
                                                
    virtual bool I1J2J3_withPlastic_aniso(const STensor3& F, const STensor3& kcor, const double R, const IPNonLocalPorosity* q0, const IPNonLocalPorosity* q1, const double* T = NULL) const;
    virtual void I1J2J3_computeResidual_aniso(const STensor3& DeltaEp, const std::vector<double>& DeltaPlasticMult, const double DeltaHatP, // unknown value
                                        const STensor3& sig,  const double R, const IPNonLocalPorosity* q0, const IPNonLocalPorosity* q1,
                                        const std::vector<double>& Y, // void parameters
                                        std::vector<double>& res0, STensor3& res1, double& res2,
                                        bool stiff,
                                        const STensor43& DsigDDeltaEp, const double H,
                                        const std::vector<STensor3>& DYDsig, // stress -dependence
                                        std::vector<STensor3>& Dres0DDeltaEp, STensor43& Dres1DDeltaEp, STensor3& Dres2DDeltaEp,
                                        std::vector<double>& Dres0DDeltaPlasticMult,std::vector<STensor3>& Dres1DDeltaPlasticMult, std::vector<double>& Dres2DDeltaPlasticMult,
                                        std::vector<double>& Dres0DDeltaHatP, STensor3& Dres1DDeltaHatP, double& Dres2DDeltaHatP,
                                        const double* T=NULL) const;
                                        
    
    virtual void I1J2J3_computeDResidual_aniso(const STensor3& DeltaEp, const std::vector<double>& DeltaPlasticMult, const double DeltaHatP, // unknown value
                                const STensor3& sig, const double R,  const IPNonLocalPorosity* q0, const IPNonLocalPorosity* q1,
                                const STensor43& DsigDEepr, const double H, const double dwp,
                                const std::vector<double>& Y, // void parameters
                                const std::vector<STensor3>& DYDsig,
                                const std::vector<std::vector<double> >& DYDnonlocalVars, const STensor43& EprToF,
                                std::vector<std::vector<double> >& Dres0DNonlocalVars, std::vector<STensor3>& Dres0DFdefo,
                                std::vector<STensor3>& Dres1DNonlocalVars, STensor43& Dres1DFdefo,
                                std::vector<double>& Dres2DNonlocalVars, STensor3& Dres2DFdefo,
                                const double* T=NULL, const double* DRDT=NULL, const STensor3* DsigDT=NULL,
                                std::vector<double>* Dres0DT=NULL, STensor3* Dres1DT=NULL, double* Dres2DT=NULL) const;
    
                                                
    virtual bool I1J2J3_plasticCorrector_NonLocal_aniso(const STensor3& F1,
                                  const STensor3& Kcorpr,
                                  const IPNonLocalPorosity *q0, IPNonLocalPorosity *q1,
                                  STensor3& Fe, STensor3& Ce, STensor3& Ee, STensor43& Le, STensor63& dLe,
                                  const STensor43& EprToF,
                                  const bool stiff,
                                  const STensor43& DKcorprDEepr, // predictor elastic tangent
                                  STensor43& DKcorDF, std::vector<STensor3>& DKcorDnonlocalVar,
                                  STensor43& DFpDFdefo, std::vector<STensor3>& DFpDnonlocalVar,
                                  std::vector<STensor3>& DlocalVarDFdefo, fullMatrix<double>& DlocalVarDnonlocalVar,
                                  const double* T,
                                  const STensor3* DKcorprDT,
                                  STensor3* DKcorDT, STensor3* DFpDT, fullMatrix<double>* DlocalVarDT
                                  ) const;
    
    virtual void tangentComputation_aniso(STensor43& dFedF, std::vector<STensor3>& dFeDNonlocalVar, STensor43& dStressDF,
                                std::vector<STensor3>& dStressDNonlocalVar,
                                std::vector<STensor3>& DLocalVardF,
                                const bool plastic,
                                const STensor3& F,  // current F
                                const STensor3& corKir,  // cor Kir
                                const STensor3& S, //  second PK
                                const STensor3& Fepr, const STensor3& Fppr, // predictor
                                const STensor43& Lpr,
                                const STensor3& Fe, const STensor3& Fp, // corrector
                                const STensor43& L, const STensor63& dL, // corrector value
                                const STensor43& DcorKirDFdefo, const std::vector<STensor3>& DcorKirDNonlocalVar,  // small strain tangents
                                const STensor43& dFpDFdefo, const std::vector<STensor3>& DFpDNonlocalVar,
                                const std::vector<STensor3>& DLocalVarDFdefo,
                                const STensor3& Fpinv
                                ) const;
    virtual void tangentComputationWithTemperature_aniso(STensor43& dFedF, std::vector<STensor3>& dFeDNonlocalVar, STensor43& dStressDF,
                                std::vector<STensor3>& dStressDNonlocalVar, STensor3& dStressDT,
                                std::vector<STensor3>& DLocalVardF,
                                const bool plastic,
                                const STensor3& F,  // current F
                                const STensor3& corKir,  // cor Kir
                                const STensor3& S, //  second PK
                                const STensor3& Fepr, const STensor3& Fppr, // predictor
                                const STensor43& Lpr,
                                const STensor3& Fe, const STensor3& Fp, // corrector
                                const STensor43& L, const STensor63& dL, // corrector value
                                const STensor43& DcorKirDFdefo, const std::vector<STensor3>& DcorKirDNonlocalVar, const STensor3& DcorKirDT, // small strain tangents
                                const STensor43& dFpDFdefo, const std::vector<STensor3>& DFpDNonlocalVar, const STensor3& DFpDT,
                                const std::vector<STensor3>& DLocalVarDFdefo,
                                const STensor3& Fpinv
                                ) const;
                                  
    virtual void I1J2J3_predictorCorrector_NonLocal_aniso(const STensor3& F0,         // initial deformation gradient (input @ time n)
                            const STensor3& F1,         // updated deformation gradient (input @ time n+1)
                            STensor3 &P,                // updated 1st Piola-Kirchhoff stress tensor (output)
                            const IPNonLocalPorosity *ipvprev,       // array of initial internal variable
                            IPNonLocalPorosity *ipvcur,             // updated array of internal variable (in ipvcur on output),
                            STensor43 &Tangent,         // constitutive tangents (output)
                            std::vector<STensor3>  &dLocalVarDF,
                            std::vector<STensor3>  &dStressDNonLocalVar,
                            fullMatrix<double>    &dLocalVarDNonLocalVar,
                            const bool stiff,                   // if true compute the tangents
                            STensor43& dFedF, std::vector<STensor3>& dFeDNonLocalVar,
                            STensor3*  dStressDT=NULL,
                            fullMatrix<double>* dLocalVarDT=NULL,
                            const double T0 =0,const double T=0
                           ) const;
    

#endif // SWIG

    virtual void getYieldDecrease(double hatpm, double &wp, double &dwp) const ;
};



class combinedPorousLaw : public mlawNonLocalPorosity
{
  #ifndef SWIG
  protected:
    std::vector<mlawNonLocalPorosity*> _multipleLaws; // cache for all relying laws


  public:    // Default constructor
    combinedPorousLaw(const int num,const double E,const double nu, const double rho, const double fVinitial,
                        const J2IsotropicHardening &j2IH, const CLengthLaw &cLLaw,
                        const double tol=1.e-8, const bool matrixbyPerturbation = false, const double pert = 1e-8);
    // Constructors & destructor
    combinedPorousLaw(const combinedPorousLaw &source);
    virtual ~combinedPorousLaw();
    virtual materialLaw* clone() const =0;
    virtual matname getType() const =0;
    
    virtual void setVoidEvolutionLaw( const voidStateEvolutionLaw& voidStateLaw ) = 0;
    virtual void setCoalescenceLaw( const CoalescenceLaw& added_coalsLaw ) =0;
    
    virtual void setNucleationLaw(const NucleationFunctionLaw& added_function, const NucleationCriterionLaw* added_criterion = NULL) override;
    virtual void setNucleationLaw(const NucleationLaw& nuclLaw) override;

    // Function of materialLaw
    virtual void createIPState(IPStateBase* &ips, bool hasBodyForce, const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const IntPt *GP=NULL, const int gpt = 0) const{};
    virtual void createIPState(IPNonLocalPorosity *ivi, IPNonLocalPorosity *iv1, IPNonLocalPorosity *iv2) const {};
    virtual void createIPVariable(IPNonLocalPorosity* &ipv, bool hasBodyForce, const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const {};
    virtual void createIPVariable(IPNonLocalPorosity *&ipv) const {};
    virtual void initLaws(const std::map<int,materialLaw*> &maplaw) override;
    
    virtual void setMacroSolver(const nonLinearMechSolver* sv) override;

    // Option settings
    virtual void setStressFormulation(const int fm) override;
    virtual void activateNucleationDuringCoalescence(const bool flg = true) override;
    virtual void setNonLocalMethod(const int i) override;
    virtual void setOrderForLogExp(const int newOrder) override;
    virtual void setViscoPlasticLaw(const viscosityLaw& vico) override;
    virtual void setPlasticInstabilityFunctionLaw(const PlasticInstabilityFunctionLaw &plIns) override;
    virtual void setScatterredInitialPorosity(double f0min, double f0max) override;
    virtual void clearCLengthLaw() override;
    virtual void setCLengthLaw(const CLengthLaw& clength) override;

    virtual void setShearPorosityGrowth_Factor(const double k);
    virtual void setShearPorosityGrowth_StressTriaxialityFunction(const scalarFunction& fct);
    virtual void setShearPorosityGrowth_LodeFunction(const scalarFunction& fct);
    virtual void setFailureTolerance(const double NewFailureTol);
    virtual void setCorrectedRegularizedFunction(const scalarFunction& fct);
    virtual void setPostBlockedDissipationBehavior(const int method);

    virtual void setSubStepping(const bool fl, const int maxNumStep);

    // Options setting -- temperature part
    virtual void setThermomechanicsCoupling(const bool fl);
    virtual void setReferenceT(const double referenceT);
    virtual void setThermalExpansionCoefficient(const double alp);
    virtual void setReferenceThermalConductivity(const double KK);
    virtual void setTemperatureFunction_ThermalConductivity(const scalarFunction& Tfunc);
    virtual void setReferenceCp(const double Cp);
    virtual void setTemperatureFunction_cp(const scalarFunction& Tfunc);
    virtual void setThermalEstimationPreviousConfig(const bool flag);
    virtual void setTaylorQuineyFactor(const double f);
    //
    virtual void setTemperatureFunction_rho(const scalarFunction& Tfunc);
    virtual void setTemperatureFunction_E(const scalarFunction& Tfunc);
    virtual void setTemperatureFunction_nu(const scalarFunction& Tfunc);
    virtual void setTemperatureFunction_alphaDilatation(const scalarFunction& Tfunc);
    virtual void setTangentByPerturbation(const bool fl,const double pr = 1e-8);

  public:
    static void LpNorm(const double power, const fullVector<double>& X, double& F,
                                     bool diff = false, fullVector<double>* DFDX = NULL,
                                     bool hess = false, fullMatrix<double>* DDFDXDX = NULL);

    void regulizationYieldFunctionBase(const double power, double p, const double s, double& f,
                          bool diff=false, double* dfdp=NULL , double* dfds=NULL,
                          bool hess=false, double* ddfdpdp=NULL, double* ddfdpds=NULL,double* ddfdsdp=NULL,double* ddfdsds=NULL) const;
    void regulizationYieldFunctionBase(const double power, double p, const double s, const double t, double& f,
                          bool diff=false, double* dfdp=NULL , double* dfds=NULL, double *dfdt = NULL,
                          bool hess=false, double* ddfdpdp=NULL, double* ddfdpds=NULL, double* ddfdpdt=NULL,
                          double* ddfdsdp=NULL,double* ddfdsds=NULL,double* ddfdsdt=NULL,
                          double* ddfdtdp=NULL,double* ddfdtds=NULL,double* ddfdtdt=NULL) const;
  #endif //SWIG
};


#endif //MLAWNONLOCALPOROUS_H_
