//
// C++ Interface: material law
//
// Description: VUMAT Interface law
//
//
// Author:  <Antoine JERUSALEM>, (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "mlawVUMATinterface.h"
#include <math.h>
#include <fstream>
#include "MInterfaceElement.h"
#include "fullMatrix.h"
#include "Numeric.h"
#include "GmshMessage.h"
#include "matrix_operations.h"

#if !defined(F77NAME)
#define F77NAME(x) (x##_)
#endif

extern "C" {
  void F77NAME(vumat)(int* nblock, int* ndir, int* nshr, int* nstatev, int* nfieldv, int* nprops, int* lanneal,
		      double* stepTime, double* totalTime, double* dt, char* cmname, double* coordMp, double* charLength,
		      double* props, double* density, double* strainInc, double* relSpinInc,
		      double* tempOld, double* stretchOld, double* defgradOld, double* fieldOld,
		      double* stressOld, double* stateOld, double* enerInternOld, double* enerInelasOld,
		      double* tempNew, double* stretchNew, double* defgradNew, double* fieldNew,
		      double* stressNew, double* stateNew, double* enerInternNew, double* enerInelasNew);
  void F77NAME(vumat_soundspeed)(int* nblock, int* nprops, double* props, double* density, double* soundSpeed);
}

mlawVUMATinterface::mlawVUMATinterface(const int num, const double rho,
				       const char *propName) : materialLaw(num,true), _rho(rho)
{
  std::ifstream in(propName);
  in >> _nprops;
  in.ignore(256,'\n');
  mallocvector(&_props,_nprops);
  for (int i=0;i < _nprops; i++)
    {
      in >> _props[i];
      in.ignore(256,'\n');
    }
  in >> _nsdv;
  in.ignore(256,'\n');
}
mlawVUMATinterface::mlawVUMATinterface(const mlawVUMATinterface &source) : materialLaw(source), _rho(source._rho) , _nsdv(source._nsdv), _nprops(source._nprops)
{
         _nprops = source._nprops;
         mallocvector(&_props,_nprops);
         for (int i=0;i < _nprops; i++)
         {
           _props[i] = source._props[i];
         }
}

mlawVUMATinterface& mlawVUMATinterface::operator=(const materialLaw &source)
{
  materialLaw::operator=(source);
  const mlawVUMATinterface* src =static_cast<const mlawVUMATinterface*>(&source);
  _rho = src->_rho;
  _nprops = src->_nprops;
  for (int i=0;i < _nprops; i++)
    {
      _props[i] = src->_props[i];
    }
  _nsdv = src->_nsdv;
  return *this;
}
 mlawVUMATinterface::~mlawVUMATinterface()
  {
    free(_props);
  }
void mlawVUMATinterface::createIPState(IPStateBase* &ips, bool hasBodyForce, const bool* state_,const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
  bool inter=true;
  const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
  if(iele==NULL) inter=false;
  IPVariable* ipvi = new IPVUMATinterface(_nsdv, 2.*(( const_cast<MElement*>(ele) )->getInnerRadius()));
  IPVariable* ipv1 = new IPVUMATinterface(_nsdv, 2.*(( const_cast<MElement*>(ele) )->getInnerRadius()));
  IPVariable* ipv2 = new IPVUMATinterface(_nsdv, 2.*(( const_cast<MElement*>(ele) )->getInnerRadius()));
  if(ips != NULL) delete ips;
  ips = new IP3State(state_,ipvi,ipv1,ipv2);
}

double mlawVUMATinterface::soundSpeed() const
{
  double soundSpeed, density;
  int nblock,nprops;

  nblock  = 1; // 1 Gauss point
  nprops  = _nprops;
  density   = _rho;

  F77NAME(vumat_soundspeed)(&nblock, &nprops, _props, &density, &soundSpeed);   // TO DEFINE IN VUMAT.F

  return soundSpeed;

}


void mlawVUMATinterface::constitutive(const STensor3& F0,
                                      const STensor3& Fn,
                                      STensor3 &P,
                                      const IPVariable *q0i,
                                      IPVariable *q1i,
                                      STensor43 &Tangent,
                                      const bool stiff,
			              STensor43* elasticTangent, 
                                      const bool dTangent,
                                      STensor63* dCalgdeps) const
{
  const IPVUMATinterface *q0 = dynamic_cast< const IPVUMATinterface * > (q0i);
  IPVUMATinterface *q1 = dynamic_cast< IPVUMATinterface * > (q1i);

  int nblock,ndir,nshr,nstatev,nprops;
  double totalTime, dt, density, charLength;
  double defgradOld[9], defgradNew[9], stretchOld[6], stretchNew[6],
    stressOld[6], stressNew[6];
  double * strainInc, * relSpinInc;
  double enerInternOld, enerInelasOld, enerInternNew, enerInelasNew;
  int * ndummy;
  double * dummy;
  char * cdummy;

  fullMatrix<double> Rmat0(3,3), Rmatn(3,3),
    Umat0(3,3), Umatn(3,3),
    Wmat0(3,3), Wmatn(3,3),
    Vmat0(3,3), Vmatn(3,3),
    VTmat0(3,3), VTmatn(3,3),
    sigmat0(3,3), sigmatn(3,3),
    Fmat0(3,3), FTmat0(3,3),
    Pmat0(3,3), cauchymat0(3,3),
    Pmatn(3,3), cauchymatn(3,3),
    tempmat(3,3), tempmat2(3,3),
    Fmatn(3,3);
  fullVector<double> sigvec0(3), sigvecn(3);
  STensor3 U0, Un, cauchy0, cauchyn;

  bool strainSpinIncNeeded     = false; // strainInc and relSpinInc requirement flag
  bool internalEnergyFromVUMAT = false; // Use the internal energy calculated by the VUMAT flag


  if ( stiff )
      Msg::Error("VUMAT stiffness matrix not available (can be implemented in mlawVUMATinterface.cpp using numerical tangent moduli)");
  if ( strainSpinIncNeeded )
    {
      Msg::Error("strainInc and relSpinInc calculations not implemented yet in mlawVUMATinterface.cpp (very costly operation anyway)");
    } else {
      strainInc  = (double*) calloc (6,sizeof(double));
      relSpinInc = (double*) calloc (3,sizeof(double));
    }

  /* 3D tensor sizes for VUMAT */
  nblock  = 1; // 1 Gauss point at each time
  ndir    = 3;
  nshr    = 3;
  nstatev = _nsdv;
  nprops  = _nprops;

  /* Double parameters initialization */
  totalTime      = _currentTime;
  dt             = _timeStep;
  density        = _rho;
  charLength     = q0->_elementSize; // CAREFUL THIS IS THE INITIAL ELEMENT SIZE
  enerInternOld  = q0->_internalEnergy;
  enerInelasOld  = q0->_inelasticEnergy;

  /* Transfer gradient of deformation (VUMAT Convention) */
  for (int i=0;i<3;i++)
    {
      defgradOld[i] = F0(i,i);
      defgradNew[i] = Fn(i,i);
    }
  defgradOld[3] = F0(0,1);
  defgradOld[4] = F0(1,2);
  defgradOld[5] = F0(2,0);
  defgradOld[6] = F0(1,0);
  defgradOld[7] = F0(2,1);
  defgradOld[8] = F0(0,2);
  defgradNew[3] = Fn(0,1);
  defgradNew[4] = Fn(1,2);
  defgradNew[5] = Fn(2,0);
  defgradNew[6] = Fn(1,0);
  defgradNew[7] = Fn(2,1);
  defgradNew[8] = Fn(0,2);

  /* Polar decomposition of F0 and Fn and transfer to VUMAT */
  F0.getMat(Fmat0); // Transform in fullMatrix class
  Wmat0=Fmat0;
  Fn.getMat(Wmatn);

  Wmat0.svd(Vmat0,sigvec0); // Do Singular Value Decomposition
  Wmatn.svd(Vmatn,sigvecn);
  VTmat0=Vmat0.transpose();
  VTmatn=Vmatn.transpose();
  sigmat0(0,0)=sigvec0(0); // Transform the vector of sigma in a matrix
  sigmat0(1,1)=sigvec0(1);
  sigmat0(2,2)=sigvec0(2);
  sigmatn(0,0)=sigvecn(0);
  sigmatn(1,1)=sigvecn(1);
  sigmatn(2,2)=sigvecn(2);


  Vmat0.mult(sigmat0,tempmat); // Use the regular relation between SVD and polar decomposition
  tempmat.mult(VTmat0,Umat0);
  Vmatn.mult(sigmatn,tempmat);
  tempmat.mult(VTmatn,Umatn);
  Wmat0.mult(VTmat0,Rmat0);
  Wmatn.mult(VTmatn,Rmatn);

  U0.setMat(Umat0); // Go back in STensor3 class
  Un.setMat(Umatn);

  /* Transfer stretch tensor (VUMAT Convention) */
  for (int i=0;i<3;i++)
    {
      stretchOld[i] = U0(i,i);
      stretchNew[i] = Un(i,i);
    }
  stretchOld[3] = U0(0,1);
  stretchOld[4] = U0(1,2);
  stretchOld[5] = U0(2,0);
  stretchNew[3] = Un(0,1);
  stretchNew[4] = Un(1,2);
  stretchNew[5] = Un(2,0);


  /* Transform 1st PK stress into corotated Cauchy stress (sigma=1/J * P * F^T) */
  P.getMat(Pmat0);
  Pmat0.mult(Fmat0.transpose(),tempmat);
  tempmat.scale(1/Fmat0.determinant()); // Cauchy stress
  tempmat.mult(Rmat0,tempmat2);
  (Rmat0.transpose()).mult(tempmat2,cauchymat0);
  cauchy0.setMat(cauchymat0); // corotated Cauchy stress

  /* Transfer corotated Cauchy stress (VUMAT Convention) */
  for (int i=0;i<3;i++)
    {
      stressOld[i] = cauchy0(i,i);
    }
  stressOld[3] = cauchy0(0,1);
  stressOld[4] = cauchy0(1,2);
  stressOld[5] = cauchy0(2,1);


  /* Call the vumat (name of the file needs to be changed to vumat.f) */
  /* IMPORTANT NOTE: Not all the fields are actually defined; if the vumat you are plugging in
                     needs them, its definition will need to be implemented */
  F77NAME(vumat)(&nblock, &ndir, &nshr, &nstatev, ndummy, &nprops, ndummy,
		 dummy, &totalTime, &dt, cdummy, dummy, &charLength,
		 _props, &density, strainInc, relSpinInc,
		 dummy, stretchOld, defgradOld, dummy,
		 stressOld, q0->_vumatStatev, &enerInternOld, &enerInelasOld,
		 dummy, stretchNew, defgradNew, dummy,
		 stressNew, q1->_vumatStatev, &enerInternNew, &enerInelasNew);


 /* Transfer corotated Cauchy stress (VUMAT Convention) */
  for (int i=0;i<3;i++)
    {
      cauchyn(i,i) = stressNew[i];
    }
  cauchyn(0,1) = stressNew[3];
  cauchyn(1,0) = stressNew[3];
  cauchyn(1,2) = stressNew[4];
  cauchyn(2,1) = stressNew[4];
  cauchyn(0,2) = stressNew[5];
  cauchyn(2,0) = stressNew[5];



 /* Transform corotated Cauchy stress into 1st PK stress (P=J sigma F^-T) */
  cauchyn.getMat(cauchymatn);
  Rmatn.mult(cauchymatn,tempmat);
  tempmat.mult(Rmatn.transpose(),tempmat2); // Cauchy stress
  Fn.getMat(Fmatn);
  tempmat2.scale(Fmatn.determinant());
  (Fmatn.transpose()).invert(tempmat);
  tempmat2.mult(tempmat,Pmatn);
  P.setMat(Pmatn); // 1st PK stress


  /* Get back the internal (from VUMAT or not) and inelastic energies (from VUMAT only) */
  if ( internalEnergyFromVUMAT )
    {
      q1->_internalEnergy = enerInternNew;
    } else {
      q1->_internalEnergy = enerInternOld;
      for (int i=0;i<3;i++)
	{
	  for (int j=0;j<3;j++)
	    {
	      q1->_internalEnergy = q1->_internalEnergy +
		0.5 * P(i,j) * ( Fn(i,j) - F0(i,j) );
	    }
	}
    }
  q1->_inelasticEnergy = enerInelasNew;


  free(strainInc);
  free(relSpinInc);

}

