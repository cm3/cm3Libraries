#ifndef CLENGTH_CC
#define CLENGTH_CC 1

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "clength.h"
#include "matrix_operations.h"
#include "rotations.h"
#ifdef NONLOCALGMSH
using namespace MFH;
#endif

//****************************************
//Zero charatiristic length , local model
//****************************************
//constructor
Zero_Cl::Zero_Cl(double* props, int idclength): Clength(props, idclength){

	nsdv = 0;
}

//destructor
Zero_Cl::~Zero_Cl(){
	;
}

//print
void Zero_Cl::print(){

	printf("Clength is zero, local model\n");
	
}

//creat_cg box
void Zero_Cl::creat_cg(double *statev_n, int pos_p, double** cg){

   int i, j;
     for(i=0;i<3;i++){
             for(j=0;j<3;j++){
                    cg[i][j]=0.0;
             }
             cg[i][i] += 1.e-10;
     }
}



//****************************************
//Isotropic charatiristic length 
//Input: cl
//Output: cg
//****************************************
//constructor
Iso_Cl::Iso_Cl(double* props, int idclength):Clength(props, idclength){

	nsdv = 0;                                     
	cl = props[idclength+1];

}

//destructor
Iso_Cl::~Iso_Cl(){
	;
}

//print
void Iso_Cl::print(){

	printf("isotropic charateristic length\n");
	printf("clength: %lf\n",cl);
	
}

void Iso_Cl::creat_cg(double *statev_n, int pos_p, double** cg){

   int i, j;
     for(i=0;i<3;i++){
             for(j=0;j<3;j++){
                    cg[i][j]=0.0;
             }
             cg[i][i] = cl;
             if(cl < 1.e-10) cg[i][i] = 1.e-10;
     }
}



//****************************************
//Anisotropic charatiristic length 
//Input: cl[i], euler[i]
//Output: cg
//****************************************
//constructor
Aniso_Cl::Aniso_Cl(double* props, int idclength):Clength(props, idclength){

       int  k;

	nsdv = 0;                                     

	//allocate memory
	mallocvector(&cl,3);
	mallocvector(&euler,3);

        k = idclength+1;

        cl[0]= props[k++];
        cl[1]= props[k++];
        cl[2]= props[k++];

        euler[0]= props[k++];
        euler[1]= props[k++];
        euler[2]= props[k++];
}

//destructor
Aniso_Cl::~Aniso_Cl(){

	free(cl);
	free(euler);

}

//print
void Aniso_Cl::print(){

	printf("Anisotropic characteristic length\n");
	printf("cl: %lf, %lf, %lf\n", cl[0], cl[1], cl[2]);
	printf("euler: %lf, %lf, %lf\n", euler[0], euler[1], euler[2]);
}


void Aniso_Cl::creat_cg(double *statev_n, int pos_p, double** cg){

      int i;
      static double** P;
      static double** T;
      
      static bool initialized = false;
      if(!initialized)
      {
        mallocmatrix(&P,3,3);
        mallocmatrix(&T,3,3);
        initialized = true;
      }
      for(i=0;i<3;i++){
               T[i][i]= cl[i];
             if(cl[i]<1e-10) T[i][i] = 1.e-10;
      } 
     eul_mat33(euler, P);
     transpose(P,P,3,3);  
     rot33(P,T, cg);
}




//****************************************
//changable Isotropic charatiristic length 
//Input: cl, Parameter for changabl length model : e_l, n_l
//Output: cg
//****************************************
//constructor
IsoV1_Cl::IsoV1_Cl(double* props, int idclength):Clength(props, idclength){

	nsdv = 0;                                     
	cl = props[idclength+1];
        e_l = props[idclength+2];
        n_l = props[idclength+3];
}

//destructor
IsoV1_Cl::~IsoV1_Cl(){
	;
}
//print
void IsoV1_Cl::print(){

	printf("Changable characteristic length\n");
	printf("cl: %lf, e_l: %lf, n_l: %lf\n", cl, e_l, n_l);
	
}

void IsoV1_Cl::creat_cg(double *statev_n, int pos_p, double** cg){

   int i, j;
   double local_pn, lcurrent;

      local_pn = statev_n[pos_p];

      if (local_pn <= e_l){
          lcurrent=cl*pow(local_pn/e_l, n_l);
      }
      else{
         lcurrent=cl;
      }

      for(i=0;i<3;i++){
             for(j=0;j<3;j++){
                    cg[i][j]=0.0;
             }
              cg[i][i] = lcurrent;
              if(lcurrent <1.e-10) cg[i][i] = 1.e-10;
      }
}


	

//****************************************
//Anisotropic charatiristic length 
//Input: cl[i], euler[i], e_l, n_l
//Output: cg
//****************************************
//constructor
AnisoV1_Cl::AnisoV1_Cl(double* props, int idclength):Clength(props, idclength){

	int  k;

	nsdv = 0;                                     

	//allocate memory
	mallocvector(&cl,3);
	mallocvector(&euler,3);

        k = idclength+1;

        cl[0]= props[k++];
        cl[1]= props[k++];
        cl[2]= props[k++];

        euler[0]= props[k++];
        euler[1]= props[k++];
        euler[2]= props[k++];

        e_l = props[k++];
        n_l = props[k++];
}

//destructor
AnisoV1_Cl::~AnisoV1_Cl(){

	free(cl);
	free(euler);

}
//print
void AnisoV1_Cl::print(){

	printf("changable anisotropic charaateristic length\n");
	printf("cl: %lf, %lf, %lf\n", cl[0], cl[1], cl[2]);
	printf("euler: %lf, %lf, %lf\n", euler[0], euler[1], euler[2]);
        printf("e_l: %lf, n_l: %lf\n", e_l, n_l);
	
}

void AnisoV1_Cl::creat_cg(double *statev_n, int pos_p, double** cg){


      int i, j;
      double local_pn;
      static double** P;
      static double** T;
      
      static bool initialized = false;
      if(!initialized)
      {
        mallocmatrix(&P,3,3);
        mallocmatrix(&T,3,3);
        initialized = true;
      }

      local_pn = statev_n[pos_p];

      if (local_pn <= e_l){
            for(i=0;i<3;i++){
                for(j=0;j<3;j++){
                   T[i][j]=0.0;
                }
                T[i][i]= cl[i]*pow(local_pn/e_l, n_l);
                if(T[i][i] <1.e-10) T[i][i] = 1.e-10;
            } 
      }
      else{
            for(i=0;i<3;i++){
                for(j=0;j<3;j++){
                   T[i][j]=0.0;
                }
                T[i][i]= cl[i];
                if(T[i][i] <1.e-10) T[i][i] = 1.e-10;
            } 
      }

     eul_mat33(euler, P);
     transpose(P,P,3,3);  
     rot33(P,T, cg);
}

//****************************************
//Anisotropic charatiristic length for stochastic Euler: 
//Create a fix direction taht will be rotated later
//Input: cl[i]
//Output: cg
//****************************************
//constructor
Stoch_Cl::Stoch_Cl(double* props, int idclength):Clength(props, idclength){

       int  k;

	nsdv = 0;                                     

	//allocate memory
	mallocvector(&cl,3);
        k = idclength+1;

        cl[0]= props[k++];
        cl[1]= props[k++];
        cl[2]= props[k++];

}

//destructor
Stoch_Cl::~Stoch_Cl(){

	free(cl);
}

//print
void Stoch_Cl::print(){

	printf("Anisotropic Stochastic characteristic length\n");
	printf("cl: %lf, %lf, %lf\n", cl[0], cl[1], cl[2]);
}


void Stoch_Cl::creat_cg(double *statev_n, int pos_p, double** cg){

      int i,j;
      for(i=0;i<3;i++){
        for(j=0;j<3;j++){
          cg[i][j] = 0.;
        }
      }
      for(i=0;i<3;i++){
        cg[i][i]= cl[i];
        if(cl[i]<1e-10) cg[i][i] = 1.e-10;
      } 
}

#endif
