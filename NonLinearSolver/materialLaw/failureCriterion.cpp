//
// C++ Interface: failure criterions
//
// Author:  <Van Dung Nguyen>, (C) 2015
//
// Copyright: See COPYING file that comes with this distribution
//
//


#include "failureCriterion.h"
#include "ipField.h"
#include "ipNonLocalPorosity.h"

TrivialFailureCriterion::TrivialFailureCriterion(const int num): FailureCriterionBase(num,true){};

CriticalDamageCriterion::CriticalDamageCriterion(const int num, const double Dc): FailureCriterionBase(num,true),_Dc(Dc){};

bool CriticalDamageCriterion::isFailed(const IPVariable* ipvprev, const IPVariable* ipv) const{
	double D = ipv->get(IPField::DAMAGE);
	if (D > _Dc) return true;
	else return false;
};

GeneralCritialCriterion::GeneralCritialCriterion(const int num, const double cval, const int ipval, const int type):
     FailureCriterionBase(num,true), _cricalValue(cval),_ipVal((IPField::Output)ipval),_type((EXTREME_TYPE)type){};

bool GeneralCritialCriterion::isFailed(const IPVariable* ipvprev, const IPVariable* ipv) const{
	double curval = ipv->get(_ipVal);
  if (_type == GeneralCritialCriterion::UPPER_BOUND){
    if (curval >= _cricalValue) return true;
    else return false;
  }
  else if (_type == GeneralCritialCriterion::LOWER_BOUND){
    if (curval <= _cricalValue) return true;
    else return false;
  }
  else{
    Msg::Error("GeneralCritialCriterion::_type is not correctly defined GeneralCritialCriterion::isFailed");
    return false;
  }
};

maximalPrincipleStressWithIPNonLocalPorosity::maximalPrincipleStressWithIPNonLocalPorosity(const int num, const double sig): FailureCriterionBase(num,true),_sigmac(sig){};

bool maximalPrincipleStressWithIPNonLocalPorosity::isFailed(const IPVariable* ipvprev, const IPVariable* ipv) const{
  const IPNonLocalPorosity* porousipv = dynamic_cast<const IPNonLocalPorosity*>(ipv);
  if (!porousipv) {
    Msg::Error("IPNonLocalPorosity must be used in maximalPrincipleStressWithIPNonLocalPorosity");
    return false;
  }

  const IPNonLocalPorosity* porousipvprev = dynamic_cast<const IPNonLocalPorosity*>(ipvprev);

  const STensor3& kCor = porousipv->getConstRefToCorotationalKirchhoffStress();
  const STensor3& F = porousipv->getConstRefToDeformationGradient();
  double J = STensorOperation::determinantSTensor3(F);

  // cauchy stress sig = kcor/J
  static fullMatrix<double> eigVec(3,3);
  static fullVector<double> eigVal(3);
  kCor.eig(eigVec,eigVal);

  double maxEigVal = std::max(std::max(eigVal(0),eigVal(1)),eigVal(2));
  if (maxEigVal/J < _sigmac) return false;
  else return true;
};

ThomasonCoalescenceConditionWithIPNonLocalPorosity::ThomasonCoalescenceConditionWithIPNonLocalPorosity(const int num, const double kk, const double lam0):
  FailureCriterionBase(num,true),_alpha(0.1), _beta(1.24),_kappa(kk),_lambda0(lam0){}

bool ThomasonCoalescenceConditionWithIPNonLocalPorosity::isFailed(const IPVariable* ipvprev, const IPVariable* ipv) const{
  const IPNonLocalPorosity* porousipv = dynamic_cast<const IPNonLocalPorosity*>(ipv);
  if (!porousipv) {
    Msg::Error("IPNonLocalPorosity must be used in ThomasonCoalescenceConditionWithIPNonLocalPorosity");
    return false;
  }

  const IPNonLocalPorosity* porousipvprev = dynamic_cast<const IPNonLocalPorosity*>(ipvprev);

  double tauY = porousipv->getCurrentViscoplasticYieldStress();
  const STensor3& kCor = porousipv->getConstRefToCorotationalKirchhoffStress();
  const STensor3& F = porousipv->getConstRefToDeformationGradient();

  double f = porousipv->getYieldPorosity();
  double p = porousipv->getLocalMatrixPlasticStrain();

  double XCube = 1.5*f*_lambda0*exp(_kappa*p);
  double X = pow(XCube,1./3.);
  if (f<=0.) X = 1e-10;
  if (X < 1e-10) X = 1e-10;
  if (X >0.999999) X= 0.999999;

  double XSq = X*X;
  double C1 = (1.-XSq);
  double C2 = _alpha*(1.-X)*(1.-X)/(XSq)+ _beta/sqrt(X);
  double CTf = C1*C2;

  // cauchy stress sig = kcor/J
  static fullMatrix<double> eigVec(3,3);
  static fullVector<double> eigVal(3);
  kCor.eig(eigVec,eigVal);

  double maxEigVal = std::max(std::max(eigVal(0),eigVal(1)),eigVal(2));


  if (maxEigVal < CTf*tauY){
    return false;
  }
  else{
    return true;
  }

};

MultipleFailureCriterion::MultipleFailureCriterion(const int num): FailureCriterionBase(num,true){};
MultipleFailureCriterion::MultipleFailureCriterion(const MultipleFailureCriterion& src): FailureCriterionBase(src){
  _allCriterion.clear();
  for (int i=0; i< src._allCriterion.size(); i++){
    _allCriterion.push_back(src._allCriterion[i]->clone());
  }
};
MultipleFailureCriterion::~MultipleFailureCriterion(){
  for (int i=0; i< _allCriterion.size(); i++){
    delete _allCriterion[i];
  }
  _allCriterion.clear();
};

void MultipleFailureCriterion::setFailureCriterion(const FailureCriterionBase& cr){
  _allCriterion.push_back(cr.clone());
};

// failure check based on stress state tensor and internal variable
bool MultipleFailureCriterion::isFailed(const IPVariable* ipvprev, const IPVariable* ipv) const{
  for (int i=0; i< _allCriterion.size(); i++){
    if (_allCriterion[i]->isFailed(ipvprev,ipv)) return true;
  }
  return false;
};
