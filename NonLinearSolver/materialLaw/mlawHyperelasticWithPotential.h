//
// C++ Interface: material law with hyperelastic potential
//
// Author:  <Van Dung Nguyen>, (C) 2023
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef MLAWHYPERELASTICWITHPOTENTIAL_H_
#define MLAWHYPERELASTICWITHPOTENTIAL_H_

#include "mlaw.h"
#include "elasticPotential.h"

class mlawHyperelasticWithPotential : public materialLaw
{
  protected:
    #ifndef SWIG
    elasticPotential* _elasticPotential;
    double _rho;
    #endif
  
  public:
    mlawHyperelasticWithPotential(const int num, const double rho, const elasticPotential& potential);
    #ifndef SWIG
    mlawHyperelasticWithPotential(const mlawHyperelasticWithPotential& src);
    virtual mlawHyperelasticWithPotential& operator=(const materialLaw &source);
    virtual ~mlawHyperelasticWithPotential();
    
    const elasticPotential* getElasticPotential() const {return _elasticPotential;};
    elasticPotential* getElasticPotential() {return _elasticPotential;};

    virtual double ElasticShearModulus() const{
        const double _E = _elasticPotential->getYoungModulus();
        const double _nu = _elasticPotential->getPoissonRatio();
        const double _G = _E/(2.*(1.+_nu));
        return _G;
    }
    
    virtual matname getType() const {return materialLaw::hyperelastic;};
    virtual void createIPState(IPStateBase* &ips,bool hasBodyForce, const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const IntPt *GP=NULL, const int gpt = 0) const;

    //allows getting the real material law for dG3DMaterialLaw etc
    virtual const materialLaw *getConstNonLinearSolverMaterialLaw() const      {
        return this;
        } // =0;
    virtual materialLaw *getNonLinearSolverMaterialLaw() {
        return this;
        }; //=0;

    virtual void initLaws(const std::map<int,materialLaw*> &maplaw){};
    // for explicit scheme it must return sqrt(E/rho) adapted to your case
    virtual double soundSpeed() const;
    virtual double density() const{return _rho;}
    // for multiscale analysis
    virtual double scaleFactor() const;

    virtual bool withEnergyDissipation() const {return false;};
    // function to define when one IP is broken for block dissipation
    virtual bool brokenCheck(const IPVariable* ipv) const { return false;}
    virtual void checkInternalState(IPVariable* ipv, const IPVariable* ipvprev) const {};
    virtual materialLaw* clone() const {return new mlawHyperelasticWithPotential(*this);};

    virtual void constitutive(const STensor3& F0,         // initial deformation gradient (input @ time n)
                              const STensor3& Fn,         // updated deformation gradient (input @ time n+1)
                              STensor3 &P,                // updated 1st Piola-Kirchhoff stress tensor (output)
                                                          // contains the initial values on input
                              const IPVariable *q0,       // array of initial internal variable
                              IPVariable *q1,              // updated array of internal variable (in ipvcur on output),
                              STensor43 &Tangent,         // constitutive tangents (output)
                              const bool stiff,
                              STensor43* elasticTangent = NULL,
                              const bool dTangent=false,
                              STensor63* dCalgdeps = NULL) const;
    #endif //SWIG
};

#endif  // MLAWHYPERELASTICWITHPOTENTIAL_H_