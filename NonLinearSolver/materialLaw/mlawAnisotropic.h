//
// C++ Interface: material law
//              Transverse Isotropic law for large strains it is a "virtual implementation" you have to define a law
//              in your project which derive from this law. you have to do THE SAME for your IPVariable
//              which have to derive from IPIPTransverseIsotropic
//              Implementation follows J. Bonet, AJ Burton, CMAME 1998, 162, 151-164
// Author:  <Gauthier BECKER>, (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef MLAWANISOTROPIC_H_
#define MLAWANISOTROPIC_H_
#include "mlaw.h"
#include "STensor3.h"
#include "STensor43.h"
#include "ipAnisotropic.h"

class mlawAnisotropic : public materialLaw
{
 protected:
  // can't be const due to operator= constructor (HOW TO CHANGE THIS ??)
  double _rho; // density
  STensor43 _ElasticityTensor;
  double _poissonMax;
  double _alpha; // parameter of anisotropy
  double _beta; // parameter of anisotropy
  double _gamma; // parameter of anisotropy


#ifndef SWIG
 private: // cache data for constitutive function
  //mutable STensor3 defo_g,defo_l,sigma_g,sigma_l;
 private: // cache data for update function
  mutable STensor3 S,Str,Siso;
 private: // cache data for tangent function
  mutable STensor43 M,Mtr,Miso;
#endif // SWIG
 public:
  mlawAnisotropic(const int num,const double rho, const double Ex, const double Ey, const double Ez,
						  const double Vxy, const double Vxz, const double Vyz,
						  const double MUxy, const double MUxz, const double MUyz,
						  const double alpha, const double beta, const double gamma);
 #ifndef SWIG
  mlawAnisotropic(const mlawAnisotropic &source);
  mlawAnisotropic& operator=(const materialLaw &source);
	virtual ~mlawAnisotropic(){}
	virtual materialLaw* clone() const {return new mlawAnisotropic(*this);}
	virtual void checkInternalState(IPVariable* ipv, const IPVariable* ipvprev) const{}; // do nothing
  virtual bool withEnergyDissipation() const {return false;};
  // function of materialLaw
  virtual matname getType() const{return materialLaw::Anisotropic;}  // WTF IS THIS ??
  virtual void createIPState(IPStateBase* &ips, bool hasBodyForce, const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const IntPt *GP=NULL, const int gpt =0) const;
  virtual void initLaws(const std::map<int,materialLaw*> &maplaw){}; // this law is initialized so nothing to do
  virtual double poissonRatio() const;
  virtual double shearModulus() const;
  virtual double scaleFactor() const {return shearModulus();};
  virtual double soundSpeed() const; // default but you can redefine it for your case
public:
  virtual void constitutive(
                            const STensor3& F0,         // initial deformation gradient (input @ time n)
                            const STensor3& Fn,         // updated deformation gradient (input @ time n+1)
                            STensor3 &P,                // updated 1st Piola-Kirchhoff stress tensor (output)
                                                        // contains the initial values on input
                            const IPVariable *q0,       // array of initial internal variable
                            IPVariable *q1,              // updated array of internal variable (in ipvcur on output),
			    STensor43 &Tangent,         // constitutive tangents (output)
			    const bool stiff,
			    STensor43* elasticTangent = NULL, 
                            const bool dTangent =false,
                            STensor63* dCalgdeps = NULL) const;
                            
  virtual void constitutiveTFA(const STensor3& F0,         // previous deformation gradient (input @ time n)
                            const STensor3& Fn,         // current deformation gradient (input @ time n+1)
                            STensor3 &P,                // current 1st Piola-Kirchhoff stress tensor (output)
                            const IPVariable *q0,       // array of previous internal variables
                            IPVariable *q1,             // current array of internal variable (in ipvcur on output),
                            STensor43 &Tangent,         // tangents (output)
                            STensor3 &epsEig,
                            STensor43 &depsEigdeps) const;
                            
  const STensor43& getElasticityTensor() const { return _ElasticityTensor;};
  virtual void ElasticStiffness(STensor43* elasticTensor) const;
 protected:
  virtual double deformationEnergy(const STensor3 &defo) const ;
  virtual double deformationEnergy(const STensor3 &defo, const STensor3& sigma) const ;

 #endif // SWIG
};

#endif // MLAWANISOTROPIC_H_
