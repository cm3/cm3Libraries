//
// C++ Interface: material law
//
// Description: EoS
//
//
// Author:  <Dongli Li, Antoine Jerusalem>, (C) 2014
//
// Copyright: See COPYING file that comes with this distribution
//
//

#include <math.h>
#include "MInterfaceElement.h"
#include "mlawEOS.h"
#include <iostream>
#include <fstream>
#include <iomanip>


void mlawEOS::Write_Matrix_To_File(std::ofstream& my_file, const STensor3 &X, std::string matrix_name) const
{
    my_file << matrix_name<<"\n";
    my_file << std::scientific << std::setprecision(14) << X(0,0) << " "<<X(0,1) << " " <<X(0,2) << "\n";
    my_file << X(1,0) << " "<<X(1,1) << " " << X(1,2)<< "\n";
    my_file << X(2,0) << " " <<X(2,1) << " " << X(2,2) << "\n";
}

void mlawEOS::multSTensor3(const STensor3 &a, const STensor3 &b, STensor3 &c) const
{
  for(int i=0;i<3;i++)
    for(int j=0;j<3;j++)
  {
    c(i,j) = a(i,0)*b(0,j)+a(i,1)*b(1,j)+a(i,2)*b(2,j);
  }
}

void mlawEOS::multSTensor3SecondTranspose(const STensor3 &a, const STensor3 &b, STensor3 &c) const
{
  for(int i=0;i<3;i++)
    for(int j=0;j<3;j++)
      c(i,j) = a(i,0)*b(j,0)+a(i,1)*b(j,1)+a(i,2)*b(j,2);
}

STensor3 mlawEOS::devSTensor3(STensor3 &a) const
{
    STensor3 I(1.);
    STensor3 deva(0.);
    double trace = a(0,0)+a(1,1)+a(2,2);
    for(int i=0; i<3; i++)
    {
        for(int j=0; j<3; j++)
        {
            deva(i,j) = a(i,j)-1./3.*trace*I(i,j);
        }
    }
    return deva;
}


STensor3 mlawEOS::symSTensor3(STensor3 &a) const
{
    STensor3 syms_a;
    STensor3 trans_a = a.transpose();
    syms_a = 0.5 * (a + trans_a);
    return syms_a;
}

mlawEOS::mlawEOS(const int num, const double rho,const double K, const double Pressure0) : materialLaw(num,true), _KEOS(K), _rho(rho), _Pressure0(Pressure0)
{
        _method = 0; //method=0 for accoustic EOS, method =1 for Hugoniot EOS
        _flagAV = 0;
}


mlawEOS::mlawEOS(int flag, const int num,
                 const double rho,const double P1, double P2, double P3, const double Pressure0) : materialLaw(num,true),_flagAV(flag), _rho(rho),_Pressure0(Pressure0)
{
    //P1 = s, P2 = C0, P3 = Gamma for non-artificial viscosity case;
    //P1 = K, P2 = c1, P3 = cl for artificial viscosity case;
        if(_flagAV == 0)
        {
            _method = 1;
            _s = P1;
            _C0 = P2;
            _Gamma = P3;
            _KEOS = pow(_C0, 2) * _rho;
        }
        else
        {
            _method = 0;
            _KEOS = P1;
            _c1 = P2;
            _cl = P3;
        }
}

mlawEOS::mlawEOS(int flag, const int num,
        const double rho,const double s, const double C0, double c1, double cl, const double Gamma) :  materialLaw(num,true),_flagAV(flag), _rho(rho), _s(s),_C0(C0),_Gamma(Gamma),_c1(c1),_cl(cl)
{
    _method = 1;
    _KEOS = pow(_C0, 2) * _rho;
}


mlawEOS::mlawEOS(const mlawEOS &source) : materialLaw(source), _rho(source._rho),
                                                         _KEOS(source._KEOS),_Pressure0(source._Pressure0), _tol(source._tol),
                                                         _perturbationfactor(source._perturbationfactor),_c1(source._c1),_cl(source._cl),_method(source._method),_flagAV(source._flagAV),
                                                         _tangentByPerturbation(source._tangentByPerturbation)
{

}



mlawEOS& mlawEOS::operator=(const materialLaw &source)
{
  materialLaw::operator=(source);
  const mlawEOS* src =static_cast<const mlawEOS*>(&source);
  if(src != NULL)
  {
    _rho = src->_rho;
    _KEOS = src->_KEOS;
    _Pressure0 = src->_Pressure0;
    _tol = src->_tol;
    _perturbationfactor = src->_perturbationfactor;
    _tangentByPerturbation = src->_tangentByPerturbation;
    _c1 = src->_c1;
    _cl = src->_cl;
    _method = src->_method;
    _flagAV = src->_flagAV;

  }
  return *this;
}

void mlawEOS::createIPState(IPStateBase* &ips,bool hasBodyForce,const bool* state_,const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{

    bool inter=true;
    const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
    if(iele==NULL) inter=false;
    IPVariable* ipvi = new IPEOS(2.*(( const_cast<MElement*>(ele) )->getInnerRadius())); //input the original element size
    IPVariable* ipv1 = new IPEOS(2.*(( const_cast<MElement*>(ele) )->getInnerRadius()));
    IPVariable* ipv2 = new IPEOS(2.*(( const_cast<MElement*>(ele) )->getInnerRadius()));
    if(ips != NULL) delete ips;
    ips = new IP3State(state_,ipvi,ipv1,ipv2);

}

double mlawEOS::soundSpeed() const
{
    double C0 = sqrt(_KEOS/_rho);
    return C0;
}


void mlawEOS::constitutive(const STensor3& F0,const STensor3& Fn,STensor3 &P,STensor3 &Piola_vis,STensor3 &P_b4AV, IPEOS *ipEOS,const IPEOS *ipEOSprev,STensor3 &Fdot)
{
  // update
  this->update(F0, Fn,P, Piola_vis, P_b4AV, ipEOS,ipEOSprev,Fdot);
}


void mlawEOS::update(const STensor3& F0,const STensor3& Fn,STensor3&P,STensor3 &Piola_vis,STensor3 &P_b4AV, IPEOS *ipEOS,const IPEOS *ipEOSprev,STensor3 &Fdot) //with AV
{

    double J = Fn.determinant();

//-----------------------------------------------------output first PK stress for deviatoric response -----------------------------------------//

   STensor3 Cauchy_v(0.);
   this->volumetricPart(Cauchy_v,Fn); //get the volumetric part from EOS

   STensor3 PK1_Orig = P;

   double invJ = 1./J;
   STensor3 Cauchy_orig(0.);
   multSTensor3SecondTranspose(PK1_Orig,Fn,Cauchy_orig);
   Cauchy_orig*=invJ; //get the original Cauchy stress Sig = PK1*F'/J

   double trace = Cauchy_orig(0,0) + Cauchy_orig(1,1) + Cauchy_orig(2,2);
   STensor3 subt_vol(0.);
   subt_vol(0,0) = -1./3.*trace;
   subt_vol(1,1) = -1./3.*trace;
   subt_vol(2,2) = -1./3.*trace;

   STensor3 Cauchy_dev = Cauchy_orig +  subt_vol; //get the deviatoric part from the original cauchy stress Sig_dev = Sig - Sig_vol

   STensor3 Cauchy = Cauchy_dev + Cauchy_v; //the new Cauchy stress

   STensor3 Finv = Fn.invert();

   STensor3 Pk1_new(0.);
   multSTensor3SecondTranspose(Cauchy,Finv,Pk1_new);
   P_b4AV = Pk1_new * J; //Cauchy->PK1

   STensor3 ZEROS(0.);


   if(_flagAV == 0) //without AV
   {
       Piola_vis = ZEROS;
   }
   else            //with AV
   {
       Piola_vis = ZEROS;

       artificialViscosity(ipEOS->_elementSize, Fn, F0, Piola_vis,Fdot);
//       bulkViscosity(ipEOS->_elementSize, Fn, F0, Piola_vis,Fdot);
   }

   P = P_b4AV + Piola_vis;
}

double mlawEOS::eos(const STensor3 &Fn)
{
    double Jn = Fn.determinant();
    double Rho_n = _rho/Jn;
    double result_P = 0.;

    if(_method == 0) //accoustic EOS
    {
        double diff = Rho_n - _rho;

        result_P = _Pressure0 + _KEOS/_rho * diff + 4 *_KEOS / (_rho * _rho) * diff * diff;
    }
    else            //Hugoniot
    {
        double Em = 0.;
        _KEOS= _rho * _C0 * _C0;
        double Ph = _KEOS * (1.-Jn)/((1.-_s * (1.-Jn)) * (1.-_s * (1.-Jn)));
        result_P = Ph * (1. - _Gamma/2.*(1.-_rho/Rho_n)) + _rho * _Gamma * Em;
    }

    return result_P;
}

//-----------------------------------volumetricPart ---------------------------------------//

void mlawEOS::volumetricPart(STensor3 &Sig_v, const STensor3 &Fn)
{
   double Pressure = this->eos(Fn);
   Sig_v(0,0)-= Pressure;
   Sig_v(1,1)-= Pressure;
   Sig_v(2,2)-= Pressure;

}

//-----------------------------------Arttificial viscosity with the input of FDot-------------------------------------//
void mlawEOS::artificialViscosity(double elementSize, const STensor3 &F1, const STensor3 &F0, STensor3 &Piola_vis,STensor3 &Fdot)
{
    double dt = _timeStep;

//    std::cout<<"dt\t"<<dt<<"\n";

    double J1 = F1.determinant();
    double J0 = F0.determinant();
    double elementSize1 = pow(J1, 1./.3) * elementSize;

    double du = elementSize1 * (log10(J1) - log10(J0))/dt; // dun+1 = hn+1*(logJn+1 - logJn)/dt
    double deta = 0;
    double Rho_1 = _rho/J1;
    double C0 = sqrt(_KEOS/_rho);

    if(du <0)
    {
        deta = std::max(0., -3./4.*Rho_1*elementSize1*(_c1 * du - _cl * C0));
    }
    else
    {
        deta = 0;
    }

    STensor3 Cauchy_vis(0.);

    Fdot = 1/dt * (F1 + (-1. * F0));
    STensor3 inv_F = F1.invert();
    STensor3 multi(0.);
    multSTensor3(Fdot,inv_F,multi);
    STensor3 sym_multi = symSTensor3(multi);
    Cauchy_vis = 2. * deta * devSTensor3(sym_multi); //Sig = 2* deta* dev[sym(F dot * F^-1)]
    STensor3 inter_Piola_vis(0.);
    multSTensor3SecondTranspose(Cauchy_vis,inv_F,inter_Piola_vis);
    Piola_vis = J1 * inter_Piola_vis; //Cauchy->PK1
}

//--------------------------------Bulk viscosity ----------------------------------------------------//
void mlawEOS::bulkViscosity(double elementSize1, STensor3 F1, const STensor3 F0, STensor3 &Piola_vis,STensor3 &Fdot)
{
    double dt = _timeStep;
    if(dt ==0)
        Fdot = 0;
    else
        Fdot = 1 /dt * (F1 + (-1) * F0); //strain rate tensor

    double trD = Fdot.trace();

    double C0 = sqrt(_KEOS/_rho);
    double J1 = F1.determinant();
    double Rho_1 = _rho/J1;

    double qBV = Rho_1 * elementSize1 * (_c1 * _c1 * elementSize1 * abs(trD) * std::min(0.,trD) + _cl * C0 * trD); //c1 qrd coef, cl linear coef
    STensor3 Cauchy_vis(0.);
    Cauchy_vis(0,0) = -qBV;
    Cauchy_vis(1,1) = -qBV;
    Cauchy_vis(2,2) = -qBV;

    STensor3 inv_F = F1.invert();
    STensor3 inter_Piola_vis(0.);
    multSTensor3SecondTranspose(Cauchy_vis,inv_F,inter_Piola_vis);
    Piola_vis = -1 * J1 * inter_Piola_vis; //Cauchy->PK1
}
