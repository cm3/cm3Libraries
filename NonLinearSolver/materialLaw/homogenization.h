#ifndef HOMOGENIZATION_H
#define HOMOGENIZATION_H 1

#ifdef NONLOCALGMSH
#include "material.h"
#include "elasticlcc.h"

int impl_localization(int sch, double* DE, MFH::Material* mtx_mat, MFH::Material** icl_mat, double vfM, double* vfI, double** ar, double** angles,
	double* statev_n, double* statev, int nsdv, int idsdv_m, int* idsdv_i, int mtx, int Nicl, double** C, double*** dC, double** Calgo, double* dpdE, double* strs_dDdp_bar, double *Sp_bar, double* dFd_d_bar, double* dstrs_dFd_bar, double *dFd_dE, double** c_gF, int kinc, int kstep, double dt);


int expl_localization(int sch, double* DE, MFH::Material* mtx_mat, MFH::Material** icl_mat, double vfM, double* vfI, double** ar, double** angles,
	double* statev_n, double* statev, int nsdv, int idsdv_m, int* idsdv_i, int mtx, int Nicl, double** C, double*** dC, double** Calgo, int kinc, int kstep, double dt);

int equationsNR(double* DE, double* DeM, double** DeI, MFH::Material* mtx_mat, MFH::Material** icl_mat, double vfM, double* vfI, 
	double* statev_n, double* statev, int idsdv_m, int* idsdv_i, double*** S0, int sch, int mtx, int Nicl, int kinc, int kstep, double dt, int last, 
	int neq, double* F, double** J, double** C, double*** dC, double** Calgo, double* dpdE, double* strs_dDdp_bar,double *Sp_bar, double* dFd_d_bar, double* dstrs_dFd_bar, double *dFd_dE, double** c_gF, double alpha1);

int impl_SecLocal(int sch, double* DE,  MFH::Material* mtx_mat, MFH::Material** icl_mat, MFH::ELcc *LCC, double vfM, double* vfI, double** ar, double** angles, double** _R66,
	double* statev_n, double* statev, int nsdv, int idsdv_m, int* idsdv_i, int mtx, int Nicl, double** C, double*** dC, double** Calgo, double* dpdE, double* dstrsdp_bar,double *Sp_bar, double* dFd_d_bar, double* dstrs_dFd_bar, double *dFd_dE, double** c_gF, double* dpdFd, double* dFddp, int kinc, int kstep, double dt);

int equationsNR_Sec(int sch, double* DE, double* DeM, double** DeI,  MFH::Material* mtx_mat, MFH::Material** icl_mat, MFH::ELcc *LCC, double vfM, double* vfI, 
	double* statev_n, double* statev, int idsdv_m, int* idsdv_i, int mtx, int Nicl, double** ar, double** angles, double** _R66, int kinc, int kstep, double dt, int last, 
	int neq, double* F, double** J, double** C, double*** dC, double** Calgo,double* dpdE, double* dstrsdp_bar, double *Sp_bar, double* dFd_d_bar, double* dstrs_dFd_bar, double *dFd_dE, double** c_gF, double* dpdFd, double* dFddp, double alpha1);


#else

int impl_localization(int sch, double* DE, Material* mtx_mat, Material** icl_mat, double vfM, double* vfI, double** ar, double** angles,
	double* statev_n, double* statev, int nsdv, int idsdv_m, int* idsdv_i, int mtx, int Nicl, double** C, double*** dC, double** Calgo, double* dpdE, double* strs_dDdp_bar, double *Sp_bar, double* dFd_d_bar, double* dstrs_dFd_bar, double *dFd_dE, double** c_gF, int kinc, int kstep, double dt);


int expl_localization(int sch, double* DE, Material* mtx_mat, Material** icl_mat, double vfM, double* vfI, double** ar, double** angles,
	double* statev_n, double* statev, int nsdv, int idsdv_m, int* idsdv_i, int mtx, int Nicl, double** C, double*** dC, double** Calgo, int kinc, int kstep, double dt);

int equationsNR(double* DE, double* DeM, double** DeI, Material* mtx_mat, Material** icl_mat, double vfM, double* vfI, 
	double* statev_n, double* statev, int idsdv_m, int* idsdv_i, double*** S0, int sch, int mtx, int Nicl, int kinc, int kstep, double dt, int last, 
	int neq, double* F, double** J, double** C, double*** dC, double** Calgo, double* dpdE, double* strs_dDdp_bar, double *Sp_bar, double* dFd_d_bar, double* dstrs_dFd_bar, double *dFd_dE, double** c_gF, double alpha1);

int impl_SecLocal(int sch, double* DE, Material* mtx_mat, Material** icl_mat, ELcc *LCC, double vfM, double* vfI, double** ar, double** angles, double** _R66,
	double* statev_n, double* statev, int nsdv, int idsdv_m, int* idsdv_i, int mtx, int Nicl, double** C, double*** dC, double** Calgo, double* dpdE, double* dstrsdp_bar,double *Sp_bar, double* dFd_d_bar, double* dstrs_dFd_bar, double *dFd_dE, double** c_gF, double* dpdFd, double* dFddp, int kinc, int kstep, double dt);


int equationsNR_Sec(int sch, double* DE, double* DeM, double** DeI, Material* mtx_mat, Material** icl_mat, ELcc *LCC, double vfM, double* vfI, 
	double* statev_n, double* statev, int idsdv_m, int* idsdv_i, int mtx, int Nicl, double** ar, double** angles, double** _R66, int kinc, int kstep, double dt, int last, 
	int neq, double* F, double** J, double** C, double*** dC, double** Calgo, double* dpdE, double* dstrsdp_bar, double *Sp_bar, double* dFd_d_bar, double* dstrs_dFd_bar, double *dFd_dE, double** c_gF, double* dpdFd, double* dFddp, double alpha1);

#endif
void updateNR(double* DE, double** De1, double* DeM1, double* vf, double vfM, double** C, double* corrNR, int sch, int mtx, int Nicl);
double scalar_res(double* vec, int sch, int Nicl, double NORM);

void eshelby_iso (double **C0, double ar, double **Esh);
void eshelby(double** dSdE, double ar1, double ar2, double**Esh);

void eshelby_iso(double **C0, double ar, double **S, double **dS, double ** ddS, double *dnu);
void eshelby_aniso(double **C0, double ar1, double ar2, double **S);
void eshelby_aniso(double **C0, double ar1, double ar2, double **S, double **dS, double ** ddS, double *dnu);

void Eshelby(double **C0, double* ar, double *euler, double **S, double **dS, double ** ddS, double *dnu);

#endif
