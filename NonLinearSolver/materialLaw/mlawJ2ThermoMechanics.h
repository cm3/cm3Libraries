//
// C++ Interface: material law
//              J2 Thermo Mechanical law: this is a first fast attempt. Not fully coupled.
// Author:  <L. Noels>, (C) 2013
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef MLAWJ2THERMOMECHANICS_H_
#define MLAWJ2THERMOMECHANICS_H_
#include "mlawJ2linear.h"
#include "STensor3.h"
#include "STensor43.h"
#include "ipJ2ThermoMechanics.h"

class mlawJ2ThermoMechanics : public mlawJ2linear
{
 protected:
  double _cv;
  double _t0;
  double _Kther;
  double _alphather;
  double _cp;

  STensor3 _k;
  STensor3 _alphaDilatation;
  double _TaylorQuineyFactor; // percentage of lpla
  
 // STensor3  _alphaDilatation;
  STensor43 K_;//fill elastic stiffenss
  STensor3 _Stiff_alphaDilatation;

 public:
  mlawJ2ThermoMechanics(const int num,const double E,const double nu, const double rho,const double sy0,const double h,
              const double t0,const double Kther, const double alphather, const double cp,const double tol=1.e-6,
              const bool matrixbyPerturbation = false, const double pert = 1e-8);

  mlawJ2ThermoMechanics(const int num,const double E,const double nu, const double rho,const J2IsotropicHardening &_j2IH,
                        const double t0,const double Kther, const double alphather, const double cp,
                        const double tol=1.e-6, const bool matrixbyPerturbation = false, const double pert = 1e-8);

  void setTaylorQuineyFactor(const double tqf) {_TaylorQuineyFactor = tqf;};
 #ifndef SWIG
  mlawJ2ThermoMechanics(const mlawJ2ThermoMechanics &source);
  mlawJ2ThermoMechanics& operator=(const materialLaw &source);
	virtual ~mlawJ2ThermoMechanics(){};
	virtual materialLaw* clone() const {return new mlawJ2ThermoMechanics(*this);};
  virtual bool withEnergyDissipation() const {return true;};
  // function of materialLaw
  virtual matname getType() const{return materialLaw::J2ThermoMechanics;}
  virtual void createIPState(IPStateBase* &ips, bool hasBodyForce, const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const IntPt *GP=NULL, const int gpt =0) const;
  virtual void initLaws(const std::map<int,materialLaw*> &maplaw){}; // this law is initialized so nothing to do
public:
  virtual void constitutive(
                            const STensor3& F0,         // initial deformation gradient (input @ time n)
                            const STensor3& Fn,         // updated deformation gradient (input @ time n+1)
                            STensor3 &P,                // updated 1st Piola-Kirchhoff stress tensor (output)
                                                        // contains the initial values on input
                            const IPJ2ThermoMechanics *q0,       // array of initial internal variable
                            IPJ2ThermoMechanics *q1,              // updated array of internal variable (in ipvcur on output),
			    STensor43 &Tangent,         // constitutive tangents (output)
			    const bool stiff,
                            STensor43* elasticTangent = NULL, 
                            const bool dTangent =false,
                            STensor63* dCalgdeps = NULL
			    ) const;
  virtual void constitutive(
                            const STensor3& F0,         // initial deformation gradient (input @ time n)
                            const STensor3& Fn,         // updated deformation gradient (input @ time n+1)
                            STensor3 &P,                // updated 1st Piola-Kirchhoff stress tensor (output)
                                                        // contains the initial values on input
                            const IPJ2ThermoMechanics *q0,       // array of initial internal variable
                            IPJ2ThermoMechanics *q1,             // updated array of internal variable (in ipvcur on output),
                            STensor43 &Tangent,         // constitutive tangents (output)
                            double T0,
			    double T,
  			    const SVector3 &gradT,
                            SVector3 &fluxT,
                            STensor3 &dPdT,
                            STensor3 &dqdgradT,
                            SVector3 &dqdT,
                            STensor33 &dqdF,
			    double &w,   //cp*dTdh
			    double &dwdt,
			    STensor3 &dwdf,
                            const bool stiff            // if true compute the tangents
                           ) const;
  virtual void constitutive(
                            const STensor3& F0,         // initial deformation gradient (input @ time n)
                            const STensor3& Fn,         // updated deformation gradient (input @ time n+1)
                            STensor3 &P,                // updated 1st Piola-Kirchhoff stress tensor (output)
                                                        // contains the initial values on input
                            const IPJ2ThermoMechanics *q0,       // array of initial internal variable
                            IPJ2ThermoMechanics *q1,             // updated array of internal variable (in ipvcur on output),
                            STensor43 &Tangent,         // constitutive tangents (output)
                            double T0,
			                      double T,
  			                    const SVector3 &gradT,
                            SVector3 &fluxT,
                            STensor3 &dPdT,
                            STensor3 &dqdgradT,
                            SVector3 &dqdT,
                            STensor33 &dqdF,
                            double &w,   //cp*dTdh
                            double &dwdt,
                            STensor3 &dwdf,
                            double& mecaSour,
                            double& dmecaSourdT,
                            STensor3& dmecaSourdF,
                            const bool stiff            // if true compute the tangents
                           ) const;

  virtual const STensor3&  getConductivityTensor() const { return _k;};
  double getInitialTemperature() const {return _t0;};
  virtual double getInitialExtraDofStoredEnergyPerUnitField() const {return getExtraDofStoredEnergyPerUnitField(_t0);}
  double getExtraDofStoredEnergyPerUnitField(double T) const {return _cp;};
  virtual const STensor3& getStiff_alphaDilatation()const { return _Stiff_alphaDilatation;};

 #endif // SWIG
};

#endif // MLAWJ2THERMOMECHANICS_H_
