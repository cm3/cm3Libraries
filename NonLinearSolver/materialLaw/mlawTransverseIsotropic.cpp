//
// C++ Interface: material law
//
// Description: TransverseIsotropic law
//
//
// Author:  <L. Noels>, (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "mlawTransverseIsotropic.h"
#include <math.h>
#include "MInterfaceElement.h"
#include "STensorOperations.h"

mlawTransverseIsotropic::mlawTransverseIsotropic(const int num,const double E,const double nu,
                           const double rho, const double EA, const double GA, const double nu_minor,
                           const double Ax, const double Ay, const double Az) :
                                materialLaw(num,true), _E(E), _nu(nu), _nu_minor(nu_minor), _rho(rho), _EA(EA),
                                _GA(GA), _A(Ax,Ay,Az), _n(EA/E)
{
  if(_A.norm()!=0.) _A.normalize();
  //Msg::Info("%f %f %f %f %f",_E,_EA,_n,_nu_minor,_nu);
  _m = 1.-nu-2.*_nu_minor*_nu_minor*_n;
  if(_m<=0.)
    Msg::Error("Wrong parameter m = 1-nu-2*EA/E*nu*nu %f",_m);

  _lambda = _E*(_nu+_n*_nu_minor*_nu_minor)/(1.+_nu)/_m;
  _mu = 0.5*_E/(1.+_nu);
  _K =_lambda+2.*_mu/3.;
  _alpha = _mu-_GA;
  _beta  = _E*(_n*_nu_minor*(1.+_nu-_nu_minor)-_nu)/4./_m/(1.+_nu);
  _gamma = EA*(1.-_nu)/8./_m-(_lambda+2.*_mu)/8.+_alpha/2.-_beta;

}
mlawTransverseIsotropic::mlawTransverseIsotropic(const mlawTransverseIsotropic &source) : materialLaw(source),_E(source._E), _nu(source._nu),
                                                       _rho(source._rho),_EA(source._EA), _GA(source._GA),
                                                       _nu_minor(source._nu_minor),
                                                       _A(source._A), _n(source._n), _m(source._m),
                                                       _lambda(source._lambda), _mu(source._mu),
                                                       _alpha(source._alpha),_beta(source._beta),
                                                       _gamma(source._gamma), _K(source._K) {}
mlawTransverseIsotropic& mlawTransverseIsotropic::operator=(const materialLaw &source)
{
  materialLaw::operator=(source);
  const mlawTransverseIsotropic* src =static_cast<const mlawTransverseIsotropic*>(&source);
  _E = src->_E;
  _nu = src->_nu;
  _nu_minor = src->_nu_minor;
  _rho = src->_rho;
  _EA = src->_EA;
  _GA = src->_GA;
  _A = src->_A;
  _n = src->_n;
  _m = src->_m;
  _lambda = src->_lambda;
  _mu = src->_mu;
  _K  = src->_K;
  _alpha = src->_alpha;
  _beta = src->_beta;
  _gamma = src->_gamma;
  return *this;
}

void mlawTransverseIsotropic::createIPState(IPStateBase* &ips,bool hasBodyForce, const bool* state_,const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
  bool inter=true;
  const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
  if(iele==NULL) inter=false;
  IPVariable* ipvi = new IPTransverseIsotropic();
  IPVariable* ipv1 = new IPTransverseIsotropic();
  IPVariable* ipv2 = new IPTransverseIsotropic();
  if(ips != NULL) delete ips;
  ips = new IP3State(state_,ipvi,ipv1,ipv2);
}

double mlawTransverseIsotropic::soundSpeed() const
{
  double factornu = (1.-_nu)/((1.+_nu)*(1.-2.*_nu));
  if(_E>_EA)  return sqrt(_E*factornu/_rho);
  else   return sqrt(_EA*factornu/_rho);
}


void mlawTransverseIsotropic::constitutive(const STensor3& F0,
                                           const STensor3& Fn,
                                           STensor3 &P,
                                           const IPVariable *q0i, 
                                           IPVariable *q1i,
                                           STensor43 &Tangent,
                                           const bool stiff, 
                                           STensor43* elasticTangent, 
                                           const bool dTangent,
                                           STensor63* dCalgdeps) const
{
  const IPTransverseIsotropic *q0 = dynamic_cast<const IPTransverseIsotropic *>(q0i);
  IPTransverseIsotropic *q1 = dynamic_cast<IPTransverseIsotropic *>(q1i);
  static SVector3 A;
  getTransverseDirection( A, q0);
  A.normalize();

  /* compute gradient of deformation */
  double Jac= (Fn(0,0) * (Fn(1,1) * Fn(2,2) - Fn(1,2) * Fn(2,1)) -
          Fn(0,1) * (Fn(1,0) * Fn(2,2) - Fn(1,2) * Fn(2,0)) +
          Fn(0,2) * (Fn(1,0) * Fn(2,1) - Fn(1,1) * Fn(2,0)));
  double lnJ = log(Jac);
  static STensor3 C;
  STensorOperation::zero(C);
  for(int I=0; I<3; I++)
    for(int J=0; J<3; J++)
      for(int k=0; k<3; k++)
        C(I,J)+=Fn(k,I)*Fn(k,J);

//  double I1= C.trace();
  static STensor3 Cinv;
  STensorOperation::inverseSTensor3(C,Cinv);

  static STensor3 Siso;
  Siso=Cinv;
  Siso*= (_lambda*lnJ-_mu);
  for(int I=0; I<3; I++)
    Siso(I,I) +=_mu;

  static SVector3 CA;
  STensorOperation::zero(CA);
  for(int I=0; I<3; I++)
    for(int J=0; J<3; J++)
      CA(I) += C(I,J)*A(J);
  double I4= dot(A,CA);
  static STensor3 AA,ACA,CAA;
  tensprod(A,A,AA);
  tensprod(A,CA,ACA);
  tensprod(CA,A,CAA);

  static STensor3 Str;
  Str = Cinv;
  Str *= (2.*_beta*(I4-1.));
  Str.daxpy(AA,(2.*(_alpha+2.*_beta*lnJ+2.*_gamma*(I4-1))));
  Str.daxpy(CAA,-1.*_alpha);
  Str.daxpy(ACA,-1.*_alpha);

  STensorOperation::zero(P);
  for(int i=0; i<3; i++)
    for(int J=0; J<3; J++)
      for(int K=0; K<3; K++)
        P(i,J)+=Fn(i,K)*(Str(K,J)+Siso(K,J));

  q1->_elasticEnergy=deformationEnergy(C,A);

  /* tangents */
  if(stiff)
  {
    static STensor43 Miso;
    static STensor43 Mtr;
    for(int I=0; I<3; I++)
      for(int J=0; J<3; J++)
        for(int K=0; K<3; K++)
          for(int L=0; L<3; L++)
          {


	    Miso(I,J,K,L)=(Cinv(I,J)*Cinv(K,L)*(_lambda))/2.-((_lambda*lnJ-_mu)*(Cinv(I,K)*Cinv(J,L)+Cinv(I,L)*Cinv(J,K)))/2.;
	    Mtr(I,J,K,L)=-_beta*(I4-1.)*(Cinv(I,K)*Cinv(J,L)+Cinv(I,L)*Cinv(J,K))+2.*_beta*(Cinv(I,J)*A(K)*A(L))+4.*_beta *A(I)*A(J)*Cinv(K,L)/2.
	                  +4.*_gamma*A(I)*A(J)*A(K)*A(L);
            if(J==K) Mtr(I,J,K,L) -= _alpha*A(I)*A(L);
            if(I==K) Mtr(I,J,K,L) -= _alpha*A(J)*A(L);  //new
          }
    for(int i=0; i<3; i++)
      for(int J=0; J<3; J++)
        for(int k=0; k<3; k++)
          for(int L=0; L<3; L++)
          {
            Tangent(i,J,k,L) = 0.;
            for(int M=0; M<3; M++)
              for(int N=0; N<3; N++)
               Tangent(i,J,k,L) += (Miso(M,J,N,L)+Mtr(M,J,N,L))*Fn(i,M)*Fn(k,N);// just one was before
            for(int M=0; M<3; M++)
              for(int N=0; N<3; N++)
                Tangent(i,J,k,L) += (Miso(M,J,L,N)+Mtr(M,J,L,N))*Fn(i,M)*Fn(k,N);//new


          if(i==k) Tangent(i,J,k,L) += (Siso(J,L)+Str(J,L));

          }
    if (elasticTangent!= NULL){
        (*elasticTangent) = Tangent;
    }
  }

}

double mlawTransverseIsotropic::deformationEnergy(const STensor3 &C, const SVector3 &A) const
{
  double Jac= sqrt((C(0,0) * (C(1,1) * C(2,2) - C(1,2) * C(2,1)) -
          C(0,1) * (C(1,0) * C(2,2) - C(1,2) * C(2,0)) +
          C(0,2) * (C(1,0) * C(2,1) - C(1,1) * C(2,0))));
  double lnJ = log(Jac)/2.;
  double I1= C.trace();

  static SVector3 CA;
  STensorOperation::zero(CA);
  for(int I=0; I<3; I++)
    for(int J=0; J<3; J++)
      CA(I) += C(I,J)*A(J);

  double I4= dot(A,CA);
  static STensor3 CC;
  CC=C;
  CC*=C;

  static SVector3 CCA;
  STensorOperation::zero(CCA);
  for(int I=0; I<3; I++)
    for(int J=0; J<3; J++)
      CCA(I) += CC(I,J)*A(J);
  double I5= dot(A,CCA);

  double PsyIso = (_alpha+_GA)*((I1-3)*0.5-lnJ)+0.5*_lambda*lnJ*lnJ;
  double PsyTr = (_alpha+2*_beta*lnJ+_gamma*(I4-1.))*(I4-1)-0.5*_alpha*(I5-1.);
  return (PsyIso+PsyTr);

}


