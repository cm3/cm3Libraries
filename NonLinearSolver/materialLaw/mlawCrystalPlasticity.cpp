//
// C++ Interface: material law
//
// Description: UMAT interface for crystal plasticity
//
//
// Author:  <L. NOELS>, (C) 2019
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include <math.h>
#include "MInterfaceElement.h"
#include <fstream>
#include "mlawCrystalPlasticity.h"
#include "STensorOperations.h"

#if !defined(F77NAME)
#define F77NAME(x) (x##_)
#endif

extern "C" {
  void F77NAME(umat_cp)(double *STRESS, double *STATEV, double *DDSDDE, double *SSE, double *SPD, double *SCD,
              double *RPL, double *DDSDDT, double *DRPLDE, double *DRPLDT,
              double *STRAN, double *DSTRAN, double *TIM, double *DTIME, double *TEMP, double *DTEMP, double *PREDEF, double *DPRED, const char *CMNAME,
              int *NDI, int*NSHR, int* NTENS, int*NSTATV, double *PROPS, int *NPROPS, double *COORDS, double *DROT, double *PNEWDT,
              double *CELENT, double *DFGRD0, double *DFGRD1, int *NOEL, int *NPT, int *LAYER, int *KSPT, int *KSTEP, int *KINC);
}



mlawCrystalPlasticity::mlawCrystalPlasticity(const int num, const double rho, const double temperature,
                   const char *propName, const char*matName) : mlawUMATInterface(num,rho,propName)
  {
	_matName=(char *)malloc(sizeof(char)*(strlen(matName)+1)); 
	strcpy(_matName,matName);
        _temperature=temperature;
        //should depend on the umat: here we read the grains.inp file with 3 euler angle, 2 parameters equal to 1, and the number of internal variables (23)
        std::ifstream in(propName);
        if(!in) Msg::Error("Cannot open the file %s! Maybe is missing   ",propName);
        nprops1=5;
        in.ignore(256,'\n');
        in.ignore(256,'\n');
        in.ignore(256,'\n');
        props=new double[nprops1];
        int i=0;
        std::string line;
        while (std::getline(in, line,',') and i<4) 
        {
          //std::cout << line << std::endl;
          props[i]=atof(line.c_str());
          std::cout << "property "<< i << ": "<< props[i] << std::endl;
          i++;
        }
        //std::cout << "1" << line << std::endl;
        props[i]=atof(line.c_str());
        std::cout << "property "<< i << ": "<< props[i] << std::endl;
        std::getline(in, line,'\n');
        //std::cout << "2" << line<< std::endl;
        std::getline(in, line,'\n');
        //std::cout << "3" << line<< std::endl;
        std::getline(in, line,'\n');
        //std::cout << "4" << line<< std::endl;
        nsdv= atoi(line.c_str());
        std::cout << "nsdv: "<< nsdv << std::endl;
        in.close();
       
       //elastic coefficient are in 'material.i01'

       std::ifstream inmat(_matName);
       if(!inmat) Msg::Error("Cannot open the file %s! Maybe is missing   ",_matName);
       inmat.ignore(256,'\n');
       inmat.ignore(256,'\n');
       inmat.ignore(256,'\n');
       inmat.ignore(256,'\n');
       double C1111,C1122,C1212;
       std::string x;
       i=0;
       while (inmat >> x && i<3) 
       {
         //std::cout << x <<std::endl;
         double val =atof(x.c_str());
         int j=0;
         int dec=0.;
         std::stringstream test(x.c_str());
         while(std::getline(test, line,'d'))
         {
           //std::cout << line <<std::endl;
           if(j==1) dec=atoi(line.c_str());
           j++;
         }
         val*=pow(10.,dec);
         if(i==0) C1111=val;
         if(i==1) C1122=val;
         if(i==2) C1212=val;
         i++;
       }

       std::cout << "C1111 "<< C1111 << ", C1122 "<< C1122<< ", C1212 "<< C1212 << std::endl;
       inmat.close();
       _mu0 = C1212/2.;
       _nu0 = C1111/2./(_mu0+C1111);
  }
  mlawCrystalPlasticity::mlawCrystalPlasticity(const mlawCrystalPlasticity &source) :
                                        mlawUMATInterface(source)
  {

  }

  mlawCrystalPlasticity& mlawCrystalPlasticity::operator=(const materialLaw &source)
  {
     mlawUMATInterface::operator=(source);
     const mlawCrystalPlasticity* src =static_cast<const mlawCrystalPlasticity*>(&source);

    return *this;
  }

  mlawCrystalPlasticity::~mlawCrystalPlasticity()
  {
     free(_matName);

  }
void mlawCrystalPlasticity::createIPState(IPCrystalPlasticity *ivi, IPCrystalPlasticity *iv1, IPCrystalPlasticity *iv2) const
{
  mlawUMATInterface::createIPState(ivi, iv1, iv2);
}

void mlawCrystalPlasticity::createIPVariable(IPCrystalPlasticity *ipv,bool hasBodyForce,const MElement *ele,const int nbFF,const IntPt *GP, const int gpt) const
{
  mlawUMATInterface::createIPVariable(ipv, hasBodyForce, ele, nbFF,GP, gpt);
}


void mlawCrystalPlasticity::callUMAT(double *stress, double *statev, double **ddsdde, double &sse, double &spd, double &scd, double &rpl, 
                                 double *ddsddt, double *drplde, double &drpldt, double *stran, double *dtsran,
                                 double *tim, double timestep, double temperature, double deltaTemperature, double *predef, double *dpred,
                                 const char *CMNAME, int &ndi, int &nshr, int tensorSize, 
                                 int statevSize, double *prs, int matPropSize, double *coords, double **dRot,
                                 double &pnewdt, double &celent, double **F0, double **F1, 
                                 int &noel, int &npt, int &layer, int &kspt, int &kstep, int &kinc) const
{
  
  double dRtmp[9];
  double F0tmp[9];
  double F1tmp[9];

  double ddsddetmp[36]; 

  convert3x3To9((const double**)dRot, dRtmp);
  convert3x3To9((const double**)F0, F0tmp);
  convert3x3To9((const double**)F1, F1tmp);

  convert6x6To36((const double**)ddsdde,ddsddetmp);

  F77NAME(umat_cp)(stress, statev, ddsddetmp, &sse, &spd, &scd, &rpl,
                   ddsddt, drplde, &drpldt, stran, dtsran,
                   tim, &timestep, &temperature, &deltaTemperature, predef, dpred,
                   CMNAME, &ndi, &nshr, &tensorSize, &statevSize, prs, &matPropSize, coords, dRtmp,
                   &pnewdt, &celent, F0tmp, F1tmp, 
                   &noel, &npt, &layer, &kspt, &kstep, &kinc);

  convert9To3x3((const double*)dRtmp,dRot);
  convert36To6x6((const double*)ddsddetmp,ddsdde);

} 

