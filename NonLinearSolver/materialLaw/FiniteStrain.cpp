//
//  FiniteStrain.c
//  m
//
//  Created by Marieme Imene EL GHEZAL on 22/11/16.
//  Copyright (c) 2016 Marieme Imene EL GHEZAL. All rights reserved.
//
#ifndef __m__FiniteStrain__c
#define __m__FiniteStrain__c

#include "FiniteStrain.h"
#include "matrix_operations.h"
#include "ID.h"

/******************************************************************************/
/* Get macroscopic logarithmic strain from macroscopic deformation gradient  */
/*input: n: increment number, Fn, dF*/
/*output: Logarithmic strain stored in vector & Fn1: F at t_n+1 */
/******************************************************************************/

int Getstrnln(double** Fn1, double* strnln)
{
    static double** b = NULL;
    static double* lambdasqr = NULL;
    static double** eA = NULL;
    static double** Fn1t; // F transpose at t_n+1
    static double** epln= NULL; //epsilonlogarithmic at t_n+1
    
    if(b==NULL)
    {
      mallocmatrix(&b,3,3);
      mallocvector(&lambdasqr,3);
      mallocmatrix(&eA,3,3);
      mallocmatrix(&Fn1t,3,3);
      mallocmatrix(&epln,3,3);
    }
    for(int i=0;i<3;i++)
    {
      lambdasqr[i]=0.;
      for(int j=0;j<3;j++)
      {
        b[i][j]=0.;
        eA[i][j]=0.;
        Fn1t[i][j]=0.;
        epln[i][j]=0.;
      }
    }
    transpose(Fn1,Fn1t,3,3);
    contraction1(Fn1, Fn1t, b);// Get macroscopic is b_(n+1) called bn1
        
//** Eigendecomposition of b (which returns lambdasqr and e(A)) **//
    
    Jacobian_Eigen( 3, b, lambdasqr, eA);
    
//** Compute logarithmic strain from lambda and e(A) **//
    GetEpsilonln(eA, lambdasqr, epln);
    
// **save the logarithmic strain in a vector strnln  **//

        for (int i=0; i<3; i++){
            strnln[i]=epln[i][i];
        }
        strnln[3]=epln[0][1]*2.0;
        strnln[4]=epln[0][2]*2.0;
        strnln[5]=epln[1][2]*2.0;
    
        return 0;
}

/******************************************************************************/
/* Get Rotation matrix R from macroscopic deformation gradient  */
/*input: n: F*/
/*output: dR,  F = VR F0=V0R0,dR=RR0^T */
/******************************************************************************/
int GetdRot(double** F0, double** Fn, double** dR)
{
    static double** b = NULL;
    static double* lambdasqr = NULL;
    static double** eA = NULL;
    static double** F; 
    static double** Ft; // F transpose 
    static double** Vinv= NULL; 
    int error;
    
    if(b==NULL)
    {
      mallocmatrix(&b,3,3);
      mallocvector(&lambdasqr,3);
      mallocmatrix(&eA,3,3);
      mallocmatrix(&F,3,3);
      mallocmatrix(&Ft,3,3);
      mallocmatrix(&Vinv,3,3);
    }
    for(int i=0;i<3;i++)
    {
      lambdasqr[i]=0.;
      for(int j=0;j<3;j++)
      {
        b[i][j]=0.;
        eA[i][j]=0.;
        F[i][j]=0.;
        Ft[i][j]=0.;
        Vinv[i][j]=0.;
      }
    }
    inverse(F0, Ft, &error,3);
    contraction1(Fn, Ft, F);   // F=FnF0^-1

    transpose(F,Ft,3,3);
    contraction1(F, Ft, b);// Get macroscopic is b_(n+1) called bn1
        
//** Eigendecomposition of b (which returns lambdasqr and e(A)) **//    
    Jacobian_Eigen( 3, b, lambdasqr, eA);
    for (int i=0; i<3; i++) {
        for (int j=0; j<3; j++) {            
            Vinv[i][j]=1.0/sqrt(lambdasqr[0])*eA[i][0]*eA[j][0]+1.0/sqrt(lambdasqr[1])*eA[i][1]*eA[j][1]+1.0/sqrt(lambdasqr[2])*eA[i][2]*eA[j][2];
        }
    }
    contraction1(Vinv, F, dR);
      
    return 0;
}

/************************************************************************************************/
//*** Comput V from strn */
/***********************************************a*************************************************/

int GetStretchTensor(double* strn, double** Vr, double shearRatio){

    static double** eplnalpha = NULL; // epsilon logarithmic per phase alpha
    static double* lnlambdaalpha = NULL; //eigen values of eplnalpha
    static double** ealphaA = NULL; // eigenvectors of eplnalpha
    
    if (eplnalpha == NULL){
        mallocmatrix(&eplnalpha,3,3);
        mallocvector(&lnlambdaalpha,3);
        mallocmatrix(&ealphaA,3,3);
    }
    for(int i=0;i<3;i++)
    {
      lnlambdaalpha[i]=0.;
      for(int j=0;j<3;j++)
      {
        eplnalpha[i][j]=0.;
        ealphaA[i][j]=0.;
      }
    }
    
    for (int i=0; i<3; i++){
      eplnalpha[i][i] = strn[i];
    }
    
    eplnalpha[0][1]= eplnalpha[1][0] = (strn[3])/shearRatio;
    eplnalpha[0][2]= eplnalpha[2][0] = (strn[4])/shearRatio;
    eplnalpha[1][2]= eplnalpha[2][1] = (strn[5])/shearRatio;
    
    Jacobian_Eigen( 3, eplnalpha, lnlambdaalpha, ealphaA);
    for (int i=0; i<3; i++) {
        for (int j=0; j<3; j++) {
            
            Vr[i][j]=(exp(lnlambdaalpha[0]))*ealphaA[i][0]*ealphaA[j][0]+(exp(lnlambdaalpha[1]))*ealphaA[i][1]*ealphaA[j][1]+(exp(lnlambdaalpha[2]))*ealphaA[i][2]*ealphaA[j][2];
        }
    }    
    return 0;
}

/************************************************************************************************/
//*** Get Updated Tau trial per phase alpha from updated  logarithmic strain */
//***This function takes as input unrotated residual strain, dR, Vr, and eigen base */
//*** and returns dTau trial (to be used in Ling Wu code as input)
//**** ntr==mtr in P.409 Issam Doghri 2000
//***********************************************************************************************/

int GetTauRes(double** dr, double* strn, double *Vr, double E, double Nu, double* strs_tr, double** Basenp1, double shearRatio1, double shearRatio2)
{
  static double** betr = NULL;  // b^e trial of phase alpha
  static double** be = NULL;  // rotated residual b^e  phase alpha
  static double** v = NULL;  // V^r
  static double** drT = NULL;  // transposed dR
  static double** dsr = NULL;  // VR under matrix form
  static double** dse = NULL;  // elastic residual strain under matrix form
  static double** drdse = NULL;  // elastic residual strain under matrix form
  static double** Basenp1tmp =NULL;
  static double** Basentmp =NULL;
  static double* lambdasqr = NULL; //eigenvalues of betr
  static double* lnlambdaalpha = NULL;
  static double* lnlambdaalphael = NULL;

  static double* eplntr = NULL; //principal values of logarithmic elastic trial strain
  static double* bettatr = NULL; // principal values of Tau trial
  static double** Tautr = NULL;


  int error, i , j;

  if (betr == NULL){
    mallocmatrix(&betr,3,3);
    mallocmatrix(&be,3,3);
    mallocmatrix(&v,3,3);
    mallocmatrix(&drT,3,3);
    mallocmatrix(&dsr,3,3);
    mallocmatrix(&dse,3,3);
    mallocmatrix(&drdse,3,3);
    mallocmatrix(&Basenp1tmp,3,3);
    mallocmatrix(&Basentmp,3,3);
    mallocmatrix(&Tautr,3,3);

    mallocvector(&lambdasqr,3);
    mallocvector(&lnlambdaalpha,3);
    mallocvector(&lnlambdaalphael,3);
    mallocvector(&eplntr,3);
    mallocvector(&bettatr,3);

  }
  for(int i=0;i<3;i++)
  {
    lambdasqr[i]=0.;
    lnlambdaalpha[i]=0.;
    lnlambdaalphael[i]=0.;
    eplntr[i]=0.;
    bettatr[i]=0.;

    for(int j=0;j<3;j++)
    {
        betr[i][j]=0.;
        be[i][j]=0.;
        v[i][j]=0.;
        drT[i][j]=0.;
        dsr[i][j]=0.;
        dse[i][j]=0.;
        drdse[i][j]=0.;
        Basenp1tmp[i][j]=0.;
        Basentmp[i][j]=0.;
        Tautr[i][j]=0.;
    }
  }
  be[0][0]=1.;
  be[1][1]=1.;
  be[2][2]=1.;

  for (i=0; i<3; i++)
  {
    dsr[i][i] = Vr[i];
    dse[i][i]=  strn[i];
  }   
  dsr[0][1]= dsr[1][0] = (Vr[3])/shearRatio2;
  dsr[0][2]= dsr[2][0] = (Vr[4])/shearRatio2;
  dsr[1][2]= dsr[2][1] = (Vr[5])/shearRatio2;
  dse[0][1]= dse[1][0] = (strn[3])/shearRatio1; // /2.?
  dse[0][2]= dse[2][0] = (strn[4])/shearRatio1; // /2.?
  dse[1][2]= dse[2][1] = (strn[5])/shearRatio1; // /2.?


  Jacobian_Eigen( 3, dsr, lnlambdaalpha, Basenp1tmp);
  for (int i=0; i<3; i++){
    for (int j=0; j<3; j++){
      v[i][j]=exp(lnlambdaalpha[0])*Basenp1tmp[i][0]*Basenp1tmp[j][0]+exp(lnlambdaalpha[1])*Basenp1tmp[i][1]*Basenp1tmp[j][1]+exp(lnlambdaalpha[2])*Basenp1tmp[i][2]*Basenp1tmp[j][2];//Basenp1tmp should be equal to Basenp1
    }
  }
  Jacobian_Eigen( 3, dse, lnlambdaalphael, Basentmp);
#if RESIDUAL_FROM_STRAIN_LD==1
  for (int i=0; i<3; i++){
    for (int j=0; j<3; j++){
      be[i][j]=exp(2.*lnlambdaalphael[0])*Basentmp[i][0]*Basentmp[j][0]+exp(2.*lnlambdaalphael[1])*Basentmp[i][1]*Basentmp[j][1]+exp(2.*lnlambdaalphael[2])*Basentmp[i][2]*Basentmp[j][2];//Basentmp should be equal to Basen
    }
  }
#endif

  transpose(dr, drT, 3, 3);
  contraction1(dr, be, drdse);
  contraction1(drdse, drT, betr);
  contraction1(v, betr, drdse);
//  contraction1(v, be, drdse);
  contraction1(drdse, v, betr); //Vr tildeR beres tilde R^T Vr



  for(int i=0;i<3;i++)
  {
    for(int j=0;j<3;j++)
    {
        Basenp1tmp[i][j]=0.;
    }
  }
  Jacobian_Eigen( 3, betr, lambdasqr, Basenp1tmp); //Basenp1tmp should be equal to Basenp1


  double Kappa;
  double Mu;
  Kappa = E/(3.*(1.-2.*Nu));
  Mu = E/(2.*(1.+Nu));

  for (i=0; i<3; i++){
      eplntr[i]=log(sqrt(lambdasqr[i]));
  }
  for (i=0; i<3; i++){
      bettatr[i]=(Kappa-2./3.*Mu)*(eplntr[0]+eplntr[1]+eplntr[2])+2.*Mu*eplntr[i];
  }
  for (i=0; i<3; i++){
    for (j=0; j<3; j++){
      Tautr[i][j]=bettatr[0]*Basenp1tmp[i][0]*Basenp1tmp[j][0]+bettatr[1]*Basenp1tmp[i][1]*Basenp1tmp[j][1]+bettatr[2]*Basenp1tmp[i][2]*Basenp1tmp[j][2];
    }
  }

  double sq2 = sqrt(2.);
  for(i=0;i<3;i++){
     strs_tr[i] = Tautr[i][i];
  }
  strs_tr[3] = Tautr[0][1]*sq2;
  strs_tr[4] = Tautr[0][2]*sq2;
  strs_tr[5] = Tautr[1][2]*sq2;
  return 0;
}

/************************************************************************************************/
//*** Get Tau per phase alpha from updated  logarithmic strain */
//***This function takes as input unrotated dR, Vr, and eigen base */
//*** and returns dTau (to be used in Ling Wu code as input)
//**** ntr==mtr in P.409 Issam Doghri 2000
//***********************************************************************************************/

int GetTau(double** dr, double* strn, double E, double Nu, double* strs_tr, double** Basenp1, double shearRatio1, double shearRatio2)
{
  static double** be = NULL;  // rotated residual b^e  phase alpha
  static double** drT = NULL;  // transposed dR
  static double** dse = NULL;  // elastic residual strain under matrix form
  static double** drdse = NULL;  // elastic residual strain under matrix form
  static double** Basenp1tmp =NULL;
  static double** Basentmp =NULL;
  static double* lambdasqr = NULL; //eigenvalues of betr
  static double* lnlambdaalphael = NULL;

  static double* eplntr = NULL; //principal values of logarithmic elastic trial strain
  static double* bettatr = NULL; // principal values of Tau trial
  static double** Tautr = NULL;


  int error, i , j;

  if (be == NULL){
    mallocmatrix(&be,3,3);
    mallocmatrix(&drT,3,3);
    mallocmatrix(&dse,3,3);
    mallocmatrix(&drdse,3,3);
    mallocmatrix(&Basenp1tmp,3,3);
    mallocmatrix(&Basentmp,3,3);
    mallocmatrix(&Tautr,3,3);

    mallocvector(&lambdasqr,3);
    mallocvector(&lnlambdaalphael,3);
    mallocvector(&eplntr,3);
    mallocvector(&bettatr,3);

  }
  for(int i=0;i<3;i++)
  {
    lambdasqr[i]=0.;
    lnlambdaalphael[i]=0.;
    eplntr[i]=0.;
    bettatr[i]=0.;

    for(int j=0;j<3;j++)
    {
        be[i][j]=0.;
        drT[i][j]=0.;
        dse[i][j]=0.;
        drdse[i][j]=0.;
        Basenp1tmp[i][j]=0.;
        Basentmp[i][j]=0.;
        Tautr[i][j]=0.;
    }
  }
  be[0][0]=1.;
  be[1][1]=1.;
  be[2][2]=1.;

  for (i=0; i<3; i++)
  {
    dse[i][i]=  strn[i];
  }   
  dse[0][1]= dse[1][0] = (strn[3])/shearRatio1; // /2.?
  dse[0][2]= dse[2][0] = (strn[4])/shearRatio1; // /2.?
  dse[1][2]= dse[2][1] = (strn[5])/shearRatio1; // /2.?


  Jacobian_Eigen( 3, dse, lnlambdaalphael, Basentmp);
  for (int i=0; i<3; i++){
    for (int j=0; j<3; j++){
      be[i][j]=exp(2.*lnlambdaalphael[0])*Basentmp[i][0]*Basentmp[j][0]+exp(2.*lnlambdaalphael[1])*Basentmp[i][1]*Basentmp[j][1]+exp(2.*lnlambdaalphael[2])*Basentmp[i][2]*Basentmp[j][2];//Basentmp should be equal to Basen
    }
  }

  transpose(dr, drT, 3, 3);
  contraction1(dr, be, drdse);
  contraction1(drdse, drT, be);

  for(int i=0;i<3;i++)
  {
    for(int j=0;j<3;j++)
    {
        Basenp1tmp[i][j]=0.;
    }
  }
  Jacobian_Eigen( 3, be, lambdasqr, Basenp1tmp); //Basenp1tmp should be equal to Basenp1


  double Kappa;
  double Mu;
  Kappa = E/(3.*(1.-2.*Nu));
  Mu = E/(2.*(1.+Nu));

  for (i=0; i<3; i++){
      eplntr[i]=log(sqrt(lambdasqr[i]));
  }
  for (i=0; i<3; i++){
      bettatr[i]=(Kappa-2./3.*Mu)*(eplntr[0]+eplntr[1]+eplntr[2])+2.*Mu*eplntr[i];
  }
  for (i=0; i<3; i++){
    for (j=0; j<3; j++){
      Tautr[i][j]=bettatr[0]*Basenp1tmp[i][0]*Basenp1tmp[j][0]+bettatr[1]*Basenp1tmp[i][1]*Basenp1tmp[j][1]+bettatr[2]*Basenp1tmp[i][2]*Basenp1tmp[j][2];
    }
  }

  double sq2 = sqrt(2.);
  for(i=0;i<3;i++){
     strs_tr[i] = Tautr[i][i];
  }
  strs_tr[3] = Tautr[0][1]*sq2;
  strs_tr[4] = Tautr[0][2]*sq2;
  strs_tr[5] = Tautr[1][2]*sq2;
  return 0;
}


void Updatefrac(double JI,double JM,double VI0,double VM0, double*VI, double*VM){
    *VI=(JI*VI0)/(JI*VI0+JM*VM0);
    *VM=(JM*VM0)/(JI*VI0+JM*VM0);
}

//*************************************************************
//Get Epsilonln from eigendecomposition of b
//*************************************************************
void GetEpsilonln(double** v, double* e, double** epln){
    for (int i=0; i<3; i++) {
        for (int j=0; j<3; j++) {
            
            epln[i][j]=log(sqrt(e[0]))*v[i][0]*v[j][0]+log(sqrt(e[1]))*v[i][1]*v[j][1]+log(sqrt(e[2]))*v[i][2]*v[j][2];
        }
        }
}


//****************************************************************************************
//Update Inclusions orientation and shape
//** Input: Initial (t=0) Aspect ratios, Fn1 (F at tn+1 in inclusion phase) & Euler angles
//** Output: updated aspect ratios ar1 (tn+1) (approximated in order to get a spheroid)
//**and updated Rotation matrix R66
//*****************************************************************************************
void UpdateIncl(double *ar0, double *euler, double**Fn1, double *ar1, double**R){
    int i,j,ind;
    double tmp;
    double perturb = 1.0e-3;
    double a,b,c; //geometric parameters of ellipsoid
    
    int order_index[3];
    
    
    double c1,c2,c3,s1,s2,s3;
    double s1c2, c1c2;
    double sq2;
    double fpi = 3.1415927/180.;
    int error;
    sq2 = sqrt(2.);
    
    static double** An1= NULL; //updated A (for ellipsoid equation x^T A x=1)
    static double** A0 = NULL;
    static double** Fn1t = NULL;
    static double** Fn1A = NULL;
    static double** Fn1inv= NULL;
    static double** Fn1invt= NULL;
    
    
    static double* alpha = NULL;
    static double** n = NULL;

    static double **P0;//3x3 rotation matrix at t=0
    static double **Pn1; // updated rotation matrix at tn+1

    double maxx;
    
    if(A0==NULL)
    {
      mallocmatrix(&A0,3,3);
      mallocmatrix(&An1,3,3);
    
      mallocmatrix(&Fn1t,3,3);
      mallocmatrix(&Fn1A,3,3);
      mallocmatrix(&Fn1inv,3,3);
      mallocmatrix(&Fn1invt,3,3);
    
      mallocvector(&alpha,3);
      mallocmatrix(&n,3,3);
      mallocmatrix(&P0,3,3);
      mallocmatrix(&Pn1,3,3);
    }
    
    for(int i=0;i<3;i++)
    {
      alpha[i]=0.;
      for(int j=0;j<3;j++)
      {
        A0[i][j]=0.;
        An1[i][j]=0.;
        Fn1t[i][j]=0.;
        Fn1A[i][j]=0.;
        Fn1inv[i][j]=0.;
        Fn1invt[i][j]=0.;
        n[i][j]=0.;
        P0[i][j]=0.;
        Pn1[i][j]=0.;
      }
    }


    a=1.0;
    b=(ar0[0]+ perturb)/ar0[1] ;
    c=ar0[0];
    //printf("initial demi-axis: %f %f %f: \n ", a, b, c);
    
    c1 = cos(euler[0]*fpi);
    s1 = sin(euler[0]*fpi);
    
    c2 = cos(euler[1]*fpi);
    s2 = sin(euler[1]*fpi);
    
    c3 = cos(euler[2]*fpi);
    s3 = sin(euler[2]*fpi);
    
    s1c2 = s1*c2;
    c1c2 = c1*c2;
    
    P0[0][0] = c3*c1 - s1c2*s3;
    P0[0][1] = c3*s1 + c1c2*s3;
    P0[0][2] = s2*s3;
    
    P0[1][0] = -s3*c1 - s1c2*c3;
    P0[1][1] = -s3*s1 + c1c2*c3;
    P0[1][2] = s2*c3;
    
    P0[2][0] = s1*s2;
    P0[2][1] = -c1*s2;
    P0[2][2] = c2;
    
    //Calcul de A0 from initial data (t=0)
    for (int i=0; i<3; i++) {
        for (int j=0; j<3; j++) {
            
        //    A0[i][j]=(1./(a*a))*P0[i][0]*P0[j][0]+(1./(b*b))*P0[i][1]*P0[j][1]+(1./(c*c))*P0[i][2]*P0[j][2];
            A0[i][j]=(1./(a*a))*P0[0][i]*P0[0][j]+(1./(b*b))*P0[1][i]*P0[1][j]+(1./(c*c))*P0[2][i]*P0[2][j];
        }
    }
   /*  printf("A0:");
    for(int i=0; i<3;i++) {
      for(int j=0; j<3;j++) {
         printf(" %f \t",A0[i][j]);
        }
        printf("\n");
     }*/
    
    //Calcul de An+1 updated from An+1=Fn+1^-T*A0*Fn+1-1
    inverse(Fn1, Fn1inv,&error,3);
    transpose(Fn1inv,Fn1invt,3,3);
    
    contraction1(Fn1invt, A0, Fn1A);
    contraction1(Fn1A, Fn1inv, An1);
    //Diagonaliser An1 and get principal directions of ellipsoid and new demi axis 1/a^2,1/b^2,1/c^2
    Jacobian_Eigen( 3, An1, alpha, n);

   // tmp = fmax(fmax(alpha[0],alpha[1]),alpha[2]);
    for (i=0;i<3;i++) order_index[i]=i;
    
    for (i=0;i<3;i++){
        for (j=i+1;j<3;j++){
            if(alpha[i] > alpha[j]){
               tmp = alpha[i];
               ind = order_index[i];
               alpha[i] = alpha[j];
               order_index[i] = order_index[j]; 
               alpha[j] = tmp;
               order_index[j] = ind;
             }
        }
    }
    if(alpha[1]-alpha[0] < alpha[2]-alpha[1]){
       a=1./sqrt(alpha[0]);
       b=1./sqrt(alpha[1]);
       c=1./sqrt(alpha[2]);
    } 
    else{     
       a=1./sqrt(alpha[2]);
       b=1./sqrt(alpha[1]);
       c=1./sqrt(alpha[0]);
       ind = order_index[0];
       order_index[0] = order_index[2];
       order_index[2] = ind;  
    }

    //new real semi-axis
    //printf("New demi-axis: %f %f %f: \n ", a, b, c);
      
    //compute new approximate aspect ratio of a spheroid
    ar1[0]=c/sqrt(a*b);
    ar1[1]=c/sqrt(a*b);
    
    //Compute Pn1 (P at tn+1)
   
    for (int i=0; i<3; i++) {
        for (int j=0; j<3; j++) {
            
         Pn1[i][j]=n[j][order_index[i]];
            
        }
    }
    
    
    // Compute updated R66 matrix
    for(i=0;i<6;i++){
        for( j=0;j<6;j++){
            R[i][j]=0;
        }
    }
    
    for(int i=0;i<3;i++){
        
        R[i][3]=Pn1[i][0]*Pn1[i][1]*sq2;
        R[i][4]=Pn1[i][0]*Pn1[i][2]*sq2;
        R[i][5]=Pn1[i][1]*Pn1[i][2]*sq2;
        
        R[3][i] = Pn1[0][i]*Pn1[1][i]*sq2;
        R[4][i] = Pn1[0][i]*Pn1[2][i]*sq2;
        R[5][i] = Pn1[1][i]*Pn1[2][i]*sq2;
        
        for(int j=0;j<3;j++){
            R[i][j] = Pn1[i][j]*Pn1[i][j];
        }
        
    }
    
    R[3][3] = Pn1[0][0]*Pn1[1][1] + Pn1[0][1]*Pn1[1][0];
    R[3][4] = Pn1[0][0]*Pn1[1][2] + Pn1[0][2]*Pn1[1][0];
    R[3][5] = Pn1[0][1]*Pn1[1][2] + Pn1[0][2]*Pn1[1][1];
    R[4][3] = Pn1[0][0]*Pn1[2][1] + Pn1[0][1]*Pn1[2][0];
    R[4][4] = Pn1[0][0]*Pn1[2][2] + Pn1[0][2]*Pn1[2][0];
    R[4][5] = Pn1[0][1]*Pn1[2][2] + Pn1[0][2]*Pn1[2][1];
    R[5][3] = Pn1[1][0]*Pn1[2][1] + Pn1[1][1]*Pn1[2][0];
    R[5][4] = Pn1[1][0]*Pn1[2][2] + Pn1[1][2]*Pn1[2][0];
    R[5][5] = Pn1[1][1]*Pn1[2][2] + Pn1[1][2]*Pn1[2][1];
   
}
/******************************************************************************/
/* Get Rotation matrix dR from Vr and oldBase  */
/*input: Vr, Basen/
/*output: dR=sum_e e_n+1 x en  and Basenp1*/
/******************************************************************************/
int GetdR(double** Basen, double* Vr, double** dR, double **Basenp1, double shearRatio)
{
#if 1
    static double** ds = NULL; // increment of strn
    static double** dRp = NULL; // increment of strn
    static double* dep = NULL; //eigen values of ds
    static double** eA = NULL; // eigenvectors of ds
    int i,j,k, order1, order2, order3;
    double sign1,sign2,sign3;
    double norm_A = 0.0;

    if (ds == NULL){
        mallocmatrix(&ds,3,3);
        mallocmatrix(&dRp,3,3);
        mallocvector(&dep,3);
        mallocmatrix(&eA,3,3);
    }
    for (i=0; i<6; i++)
    {
       norm_A +=  Vr[i]*Vr[i];
    } 

   if(norm_A>1.0e-12){
      for (i=0; i<3; i++)
      {
          ds[i][i] = Vr[i];
      }   
    
      ds[0][1]= ds[1][0] = (Vr[3])/shearRatio;
      ds[0][2]= ds[2][0] = (Vr[4])/shearRatio;
      ds[1][2]= ds[2][1] = (Vr[5])/shearRatio;

      for (i=0; i<3; i++)
          for (j=0; j<3; j++)
            Basenp1[i][j]=0.;
      Jacobian_Eigen(3, ds, dep, Basenp1);
      //find order 1
      if(fabs(Basenp1[0][0]*Basen[0][0]+Basenp1[1][0]*Basen[1][0]+Basenp1[2][0]*Basen[2][0])>=fabs(Basenp1[0][0]*Basen[0][1]+Basenp1[1][0]*Basen[1][1]+Basenp1[2][0]*Basen[2][1]) &&
         fabs(Basenp1[0][0]*Basen[0][0]+Basenp1[1][0]*Basen[1][0]+Basenp1[2][0]*Basen[2][0])>=fabs(Basenp1[0][0]*Basen[0][2]+Basenp1[1][0]*Basen[1][2]+Basenp1[2][0]*Basen[2][2]))
      {
         order1=0;
         if(Basenp1[0][0]*Basen[0][0]+Basenp1[1][0]*Basen[1][0]+Basenp1[2][0]*Basen[2][0]>0.)
           sign1=1.;
         else
           sign1=-1.;
      }
      else if(fabs(Basenp1[0][0]*Basen[0][1]+Basenp1[1][0]*Basen[1][1]+Basenp1[2][0]*Basen[2][1])>=fabs(Basenp1[0][0]*Basen[0][0]+Basenp1[1][0]*Basen[1][0]+Basenp1[2][0]*Basen[2][0]) &&
              fabs(Basenp1[0][0]*Basen[0][1]+Basenp1[1][0]*Basen[1][1]+Basenp1[2][0]*Basen[2][1])>=fabs(Basenp1[0][0]*Basen[0][2]+Basenp1[1][0]*Basen[1][2]+Basenp1[2][0]*Basen[2][2]))
      {
         order1=1;
         if(Basenp1[0][0]*Basen[0][1]+Basenp1[1][0]*Basen[1][1]+Basenp1[2][0]*Basen[2][1]>0.)
           sign1=1.;
         else
           sign1=-1.;
      }
      else
      { 
         if(Basenp1[0][0]*Basen[0][2]+Basenp1[1][0]*Basen[1][2]+Basenp1[2][0]*Basen[2][2]>0.)
           sign1=1.;
         else
           sign1=-1.;
         order1=2;
      }
      //find order 2
      if(fabs(Basenp1[0][1]*Basen[0][0]+Basenp1[1][1]*Basen[1][0]+Basenp1[2][1]*Basen[2][0])>=fabs(Basenp1[0][1]*Basen[0][1]+Basenp1[1][1]*Basen[1][1]+Basenp1[2][1]*Basen[2][1]) &&
         fabs(Basenp1[0][1]*Basen[0][0]+Basenp1[1][1]*Basen[1][0]+Basenp1[2][1]*Basen[2][0])>=fabs(Basenp1[0][1]*Basen[0][2]+Basenp1[1][1]*Basen[1][2]+Basenp1[2][1]*Basen[2][2]) && order1!=0)
      {
        order2=0;
        if(Basenp1[0][1]*Basen[0][0]+Basenp1[1][1]*Basen[1][0]+Basenp1[2][1]*Basen[2][0]>0.)
          sign2=1.;
        else
          sign2=-1.;
      }
      else if(fabs(Basenp1[0][1]*Basen[0][1]+Basenp1[1][1]*Basen[1][1]+Basenp1[2][1]*Basen[2][1])>=fabs(Basenp1[0][1]*Basen[0][0]+Basenp1[1][1]*Basen[1][0]+Basenp1[2][1]*Basen[2][0]) &&
              fabs(Basenp1[0][1]*Basen[0][1]+Basenp1[1][1]*Basen[1][1]+Basenp1[2][1]*Basen[2][1])>=fabs(Basenp1[0][1]*Basen[0][2]+Basenp1[1][1]*Basen[1][2]+Basenp1[2][1]*Basen[2][2]) && order1!=1)
      {
        order2=1;
        if(Basenp1[0][1]*Basen[0][1]+Basenp1[1][1]*Basen[1][1]+Basenp1[2][1]*Basen[2][1]>0.)
          sign2=1.;
        else
          sign2=-1.;
      }
      else 
      {
        order2=2;
        if(Basenp1[0][1]*Basen[0][2]+Basenp1[1][1]*Basen[1][2]+Basenp1[2][1]*Basen[2][2]>0.)
          sign2=1.;
        else
          sign2=-1.;
      }
      if((order1==0 && order2==1) || (order1==1 && order2==0))
      {
        if(Basenp1[0][2]*Basen[0][2]+Basenp1[1][2]*Basen[1][2]+Basenp1[2][2]*Basen[2][2]>0.)
          sign3=1.;
        else
          sign3=-1.;
        order3=2;
      }
      else if((order1==0 && order2==2) || (order1==2 && order2==0))
      {
        if(Basenp1[0][2]*Basen[0][1]+Basenp1[1][2]*Basen[1][1]+Basenp1[2][2]*Basen[2][1]>0.)
          sign3=1.;
        else
          sign3=-1.;
        order3=1;
      }
      else
      {
        if(Basenp1[0][2]*Basen[0][0]+Basenp1[1][2]*Basen[1][0]+Basenp1[2][2]*Basen[2][0]>0.)
          sign3=1.;
        else
          sign3=-1.;
        order3=0;
      }
      for (i=0; i<3; i++){
          for (j=0; j<3; j++){
            dR[i][j] = sign1*Basenp1[i][0]*Basen[j][order1]+sign2*Basenp1[i][1]*Basen[j][order2]+sign3*Basenp1[i][2]*Basen[j][order3];
          }
      }
    }
    else{
       for (i=0; i<3; i++){
          for (j=0; j<3; j++){
            dR[i][j] = 0.0;
            Basenp1[i][j] = Basen[i][j];
          }
          dR[i][i] = 1.0;
       }
    }
    return 0;
#else
    static double** ds = NULL; // increment of strn
    static double** dRp = NULL; // increment of strn
    static double* dep = NULL; //eigen values of ds
    static double* mu = NULL; //eigen values of RRtilde
    static double** eA = NULL; // eigenvectors of ds
    static double** a = NULL; // eigenvectors of RRtilde
    static double** b = NULL; // RA
    static double** bigR = NULL; // increment of strn
    static double** bigRtilde = NULL; // increment of strn
    static double** bigS = NULL; // increment of strn
    int i,j,k;
    double norm_A = 0.0;

    if (ds == NULL){
        mallocmatrix(&ds,3,3);
        mallocmatrix(&dRp,3,3);
        mallocvector(&dep,3);
        mallocvector(&mu,3);
        mallocmatrix(&eA,3,3);
        mallocmatrix(&a,3,3);
        mallocmatrix(&b,3,3);
        mallocmatrix(&bigR,3,3);
        mallocmatrix(&bigRtilde,3,3);
        mallocmatrix(&bigS,3,3);
    }
    for (i=0; i<6; i++)
    {
       norm_A +=  Vr[i]*Vr[i];
    } 

   if(norm_A>1.0e-12){
      for (i=0; i<3; i++)
      {
          ds[i][i] = Vr[i];
      }   
    
      ds[0][1]= ds[1][0] = (Vr[3])/shearRatio;
      ds[0][2]= ds[2][0] = (Vr[4])/shearRatio;
      ds[1][2]= ds[2][1] = (Vr[5])/shearRatio;

      for (i=0; i<3; i++)
          for (j=0; j<3; j++)
            Basenp1[i][j]=0.;
      Jacobian_Eigen(3, ds, dep, Basenp1);

      for (i=0; i<3; i++){
          for (j=0; j<3; j++){
            bigR[i][j] = Basenp1[i][0]*Basen[j][0]+Basenp1[i][1]*Basen[j][1]+Basenp1[i][2]*Basen[j][2];
            bigS[i][j] = Basenp1[i][0]*Basenp1[j][0]+Basenp1[i][1]*Basenp1[j][1]+Basenp1[i][2]*Basenp1[j][2];
          }
      }
      transpose(bigR, bigRtilde, 3, 3);
      contraction1(bigRtilde, bigR, dRp);
      Jacobian_Eigen(3, dRp, mu, a);
      for (i=0; i<3; i++){
         for (j=0; j<3; j++){
            b[i][j]=0.;
            for (k=0; k<3; k++){
               b[i][j]+=bigR[i][k]*a[k][j]/sqrt(mu[j]);
            }
         }
      }       

      for (i=0; i<3; i++){
          for (j=0; j<3; j++){
            dR[i][j] = b[i][0]*a[j][0]+b[i][1]*a[j][1]+b[i][2]*a[j][2];
          }
      }
    }
    else{
       for (i=0; i<3; i++){
          for (j=0; j<3; j++){
            dR[i][j] = 0.0;
            Basenp1[i][j] = Basen[i][j];
          }
          dR[i][i] = 1.0;
       }
    }
    return 0;

#endif
}
/******************************************************************************/
/* Update F from UNLOADED STATE  */
/*output: Fnp1=Vr tilde R Fn  an Rnp1=tilde R R*/
/******************************************************************************/
int UpdateFandR(double** Fn, double** Rn, double* Vr, double** dR, double **Fnp1, double **Rnp1, double shearRatio)

{
    static double** ds = NULL; // increment of strn
    static double** drFn = NULL; 
    static double** v = NULL; 
    static double* lnlambdaalpha = NULL;
    static double** ealpha = NULL;
    int i,j;

    if (ds == NULL){
      mallocmatrix(&ds,3,3);
      mallocmatrix(&drFn,3,3);
      mallocmatrix(&v,3,3);
      mallocmatrix(&ealpha, 3, 3);
      mallocvector(&lnlambdaalpha, 3);
    }
    for(int i=0;i<3;i++)
    {
      lnlambdaalpha[i]=0.;
      for(int j=0;j<3;j++)
      {
        ealpha[i][j]=0.;
      }
    }

    for (i=0; i<3; i++)
    {
      ds[i][i] = Vr[i];
    }   
    
    ds[0][1]= ds[1][0] = (Vr[3])/shearRatio;
    ds[0][2]= ds[2][0] = (Vr[4])/shearRatio;
    ds[1][2]= ds[2][1] = (Vr[5])/shearRatio;
    Jacobian_Eigen( 3, ds, lnlambdaalpha, ealpha);
    for (int i=0; i<3; i++){
      for (int j=0; j<3; j++){
        v[i][j]=exp(lnlambdaalpha[0])*ealpha[i][0]*ealpha[j][0]+exp(lnlambdaalpha[1])*ealpha[i][1]*ealpha[j][1]+exp(lnlambdaalpha[2])*ealpha[i][2]*ealpha[j][2];
        }
    }

    contraction1(dR, Fn, drFn);
    contraction1(dR, Rn, Rnp1);
    contraction1(v, drFn, Fnp1);
    return 0;
}
/******************************************************************************/
/* Update F   */
/*output: Fnp1=Vr Fn */
/******************************************************************************/
int UpdateF(double** Fn, double* Vr, double **Fnp1, double shearRatio)

{
   static double** ds = NULL; // increment of strn
   static double** v = NULL; 
   static double* lnlambdaalpha = NULL;
   static double** ealpha = NULL;
   int i,j;

    if (ds == NULL){
      mallocmatrix(&ds,3,3);
      mallocmatrix(&v,3,3);
      mallocmatrix(&ealpha, 3, 3);
      mallocvector(&lnlambdaalpha, 3);
    }
    for(int i=0;i<3;i++)
    {
      lnlambdaalpha[i]=0.;
      for(int j=0;j<3;j++)
      {
        ealpha[i][j]=0.;
      }
    }

    for (i=0; i<3; i++)
    {
      ds[i][i] = Vr[i];
    }   
    
    ds[0][1]= ds[1][0] = (Vr[3])/shearRatio;
    ds[0][2]= ds[2][0] = (Vr[4])/shearRatio;
    ds[1][2]= ds[2][1] = (Vr[5])/shearRatio;

    Jacobian_Eigen( 3, ds, lnlambdaalpha, ealpha);
    for (int i=0; i<3; i++){
      for (int j=0; j<3; j++){
        v[i][j]=exp(lnlambdaalpha[0])*ealpha[i][0]*ealpha[j][0]+exp(lnlambdaalpha[1])*ealpha[i][1]*ealpha[j][1]+exp(lnlambdaalpha[2])*ealpha[i][2]*ealpha[j][2];
        }
    }
    contraction1(v, Fn, Fnp1);

    return 0;
}
#endif
