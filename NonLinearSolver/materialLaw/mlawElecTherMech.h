//
// C++ Interface: material law
//             Electrical SMP law
// Author:  <L. Homsy>, (C) 2015
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef MLAWELECTHERMECH_H_
#define MLAWELECTHERMECH_H_
#include "STensor3.h"
#include "STensor43.h"
#include "mlawTransverseIsotropic.h"
#include "ipElecTherMech.h"


  
class mlawElecTherMech :public mlawTransverseIsotropic
{
 protected:
  // can't be const due to operator= constructor (HOW TO CHANGE THIS ??)
 mlawLinearElecTherMech *_lawLinearETM;

 
 
  STensor3 _l0;
  STensor3 _k10;
  STensor3 _k20;
  STensor3 _l10;
  STensor3 _l20;

 // double _seebeck;*/
  

#ifndef SWIG
 private: 
   
#endif // SWIG
 public:
  mlawElecTherMech(int num,const double rho,  const double alpha, const double beta, const double gamma  ,
         const double t0,const double Kx,const double Ky, const double Kz,
	 const  double lx,const  double ly,const  double lz,const double seebeck, const double EA, const double GA, const double nu_minor,
                           const double Ax, const double Ay, const double Az);
   #ifndef SWIG
	mlawElecTherMech(const mlawElecTherMech &source);
	mlawElecTherMech& operator=(const  materialLaw &source);
	virtual materialLaw* clone() const {return new mlawElecTherMech(*this);};
  virtual bool withEnergyDissipation() const {return false;};
  // function of materialLaw
  virtual matname getType() const{return materialLaw::ElecTherMech;}  
  virtual void createIPState(IPStateBase* &ips, bool hasBodyForce, const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const IntPt *GP=NULL, const int gpt =0) const;
 
public:
   virtual void constitutive(
                            const STensor3& F0,         // initial deformation gradient (input @ time n)
                            const STensor3& Fn,         // updated deformation gradient (input @ time n+1)
                            STensor3 &P,                // updated 1st Piola-Kirchhoff stress tensor (output)
                                                        // contains the initial values on input
                            const IPElecTherMech *q0,       // array of initial internal variable
                            IPElecTherMech *q1,             // updated array of internal variable (in ipvcur on output),
                            STensor43 &Tangent,         // constitutive tangents (output)
                            double T,
			    double V,
  			    const SVector3 &gradT,
			    const SVector3 &gradV,
                            SVector3 &fluxT,
			    SVector3 &fluxV,
                            STensor3 &dPdT,
                            STensor3 &dPdV,
                            STensor3 &dqdgradT,
                            SVector3 &dqdT,
			    STensor3 &dqdgradV,
                            SVector3 &dqdV,
			    SVector3 &dedV,
			    STensor3 &dedgradV,
			    SVector3 &dedT,
			    STensor3 &dedgradT,
                            STensor33 &dqdF,
			    STensor33 &dedF,
                            const bool stiff  
                           ) const;

    const STensor3&  getInitialElecConductivityTensor() const { return _l10;};
    const STensor3&  getInitialElecCrossConductivityTensor() const { return _l20;};
    const STensor3&  getInitialConductivityTensor() const { return _k10;};
    const STensor3&  getInitialCrossConductivityTensor() const { return _k20; };
 //protected:
  //virtual double deformationEnergy(STensor3 defo) const ;
  //virtual double deformationEnergy(STensor3 defo, const STensor3& sigma) const ;
      virtual ~mlawElecTherMech() {
				if (_lawLinearETM != NULL) delete _lawLinearETM;
			};

 #endif // SWIG
};

#endif // MLAWELECTHERMOMECH_H_
