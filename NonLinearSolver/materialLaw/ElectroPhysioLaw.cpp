//
//
//
// Description: Define electrophysiology law
//
//
// Author:  <A. Jerusalem>, (C) 2013
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "ElectroPhysioLaw.h"
#include <math.h>

ElectroPhysioLaw::ElectroPhysioLaw(const int num, const bool init): _num(num), _initialized(init)
{

}

ElectroPhysioLaw::ElectroPhysioLaw(const ElectroPhysioLaw &source)
{
  _num = source._num;
  _initialized = source._initialized;
  
}

ElectroPhysioLaw& ElectroPhysioLaw::operator=(const ElectroPhysioLaw &source)
{
  _num = source._num;
  _initialized = source._initialized;
  return *this;
}



CytoConductionLaw::CytoConductionLaw(const int num, const double _rhoc, const double _rhotilde,
                                                    const bool init) : 
				ElectroPhysioLaw(num,init), rhoc(_rhoc), rhotilde(_rhotilde)  
{

}
CytoConductionLaw::CytoConductionLaw(const CytoConductionLaw &source) :
					ElectroPhysioLaw(source)  
{
 
  rhoc     =  source.rhoc;
  rhotilde =  source.rhotilde;

}

CytoConductionLaw& CytoConductionLaw::operator=(const ElectroPhysioLaw &source) 
{
  ElectroPhysioLaw::operator=(source);
  const CytoConductionLaw* src = dynamic_cast<const CytoConductionLaw*>(&source);
  if(src != NULL)
  {
    rhoc     =  src->rhoc;
    rhotilde =  src->rhotilde;
  }
  return *this;
}

void CytoConductionLaw::createIPVariable(IPElectroPhysio* &ipcl) const
{
  if(ipcl != NULL) delete ipcl;
  ipcl = new IPCytoConductionEP();
}


ElectroPhysioLaw * CytoConductionLaw::clone() const
{
  return new CytoConductionLaw(*this);
}


CableTheoryLaw::CableTheoryLaw(const int num, const double _p0, const double _pc,
                                                    const double _n,
                                                    const double _alpha, const bool init) : 
				ElectroPhysioLaw(num,init), p0(_p0), pc(_pc), n(_n), alpha(_alpha)  // TO BE MODIFIED
{

}
CableTheoryLaw::CableTheoryLaw(const CableTheoryLaw &source) :
					ElectroPhysioLaw(source)  // TO BE MODIFIED
{
 
  // p0     =  source.p0;
  // pc     =  source.pc;
  // n      =  source.n;
  // alpha  =  source.alpha;
}

CableTheoryLaw& CableTheoryLaw::operator=(const ElectroPhysioLaw &source)  // TO BE MODIFIED
{
  // ElectroPhysioLaw::operator=(source);
  // const CableTheoryLaw* src = dynamic_cast<const CableTheoryLaw*>(&source);
  // if(src != NULL)
  // {
  //   p0     =  src->p0;
  //   pc     =  src->pc;
  //   n      =  src->n;
  //   alpha  =  src->alpha;

  }
  return *this;
}
void CableTheoryLaw::createIPVariable(IPElectroPhysio* &ipcl) const
{
  if(ipcl != NULL) delete ipcl;
  ipcl = new IPCableTheoryEP();
}

void CableTheoryLaw::computePotential(double p1, double p0, double phiel, 
                                              const STensor3 &Fe, const STensor3 &Fp, 
                                              const STensor3 &Peff,
                                              const STensor43 &Cel, 
                                              const IPElectroPhysio &ipvprev, IPElectroPhysio &ipvcur) const  // TO BE MODIFIED
{
  // double D0     = ipvprev.getDamage();
  // double maxp0  = ipvprev.getMaximalP();
  // double DeltaD = 0.;
  // double dDdp   = 0.;
  // static STensor3 dDdFe;
  // dDdFe =0;

  // double D=D0;
  // double maxp = maxp0;
  // if(p1>pc) p1=pc;
  // if(p1>p0 and p1>p0)
  // {
  //   DeltaD = pow(phiel/alpha,n)*(p1-p0);
  //   dDdp   = pow(phiel/alpha,n);
  //   for(int i=0; i<3; i++)
  //   {
  //     for(int j=0; j<3; j++)
  //     {
  //       dDdFe(i,j)=0.;
  //       for(int k=0; k<3; k++)
  //         dDdFe(i,j)+=Peff(i,k)*Fp(j,k);
  //      }
  //    }
  //    dDdFe *=(pow(phiel/alpha,n-1)*(p1-p0)*n/alpha);
  // }
  // D = D0+DeltaD;
  // if(D>0.99999999999)
  // {
  //    double oldDD=DeltaD;
  //    DeltaD      = 0.99999999999-D0;
  //    double rat = DeltaD/oldDD;
  //    D = D0+DeltaD;
  //    dDdp   *= rat;
  //    dDdFe  *= rat;
  // }
  // if(p1>maxp) maxp=p1;
  // ipvcur.setValues(D, maxp, DeltaD, dDdp, dDdFe);
}

ElectroPhysioLaw * CableTheoryLaw::clone() const
{
  return new CableTheoryLaw(*this);
}

HodgkinHuxleyLaw::HodgkinHuxleyLaw(const int num, const double _p0, const double _pc,
                                                    const double _alpha,
                                                    const double _beta, const bool init) : 
				ElectroPhysioLaw(num,init), p0(_p0), pc(_pc), alpha(_alpha), beta(_beta) // TO BE MODIFIED
{

}
HodgkinHuxleyLaw::HodgkinHuxleyLaw(const HodgkinHuxleyLaw &source) :
					ElectroPhysioLaw(source) // TO BE MODIFIED
{
 
  // p0     =  source.p0;
  // pc     =  source.pc;
  // alpha  =  source.alpha;
  // beta   =  source.beta;
}

HodgkinHuxleyLaw& HodgkinHuxleyLaw::operator=(const ElectroPhysioLaw &source) // TO BE MODIFIED
{
  // ElectroPhysioLaw::operator=(source);
  // const HodgkinHuxleyLaw* src = dynamic_cast<const HodgkinHuxleyLaw*>(&source);
  // if(src != NULL)
  // {
  //   p0     =  src->p0;
  //   pc     =  src->pc;
  //   alpha  =  src->alpha;
  //   beta   =  src->beta;
  // }
  // return *this;
}
void HodgkinHuxleyLaw::createIPVariable(IPElectroPhysio* &ipcl) const
{
  if(ipcl != NULL) delete ipcl;
  ipcl = new IPHodgkinHuxleyEP();
}

void HodgkinHuxleyLaw::computePotential(double p1, double p0, double phiel, 
                                              const STensor3 &Fe, 
                                              const STensor3 &Fp, const STensor3 &Peff,
                                              const STensor43 &Cel, 
                                    const IPElectroPhysio &ipvprev, IPElectroPhysio &ipvcur) const // TO BE MODIFIED
{

}

ElectroPhysioLaw * HodgkinHuxleyLaw::clone() const
{
  return new HodgkinHuxleyLaw(*this);
}

