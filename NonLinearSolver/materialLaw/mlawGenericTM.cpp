// C++ Interface: material law
//              Generic Thermo-Mechanical law
// Author:  <Vinayak GHOLAP>, (C) 2022
//
// Copyright: See COPYING file that comes with this distribution
//
#include "mlawGenericTM.h"
#include <math.h>
#include "MInterfaceElement.h"

mlawGenericTM::mlawGenericTM(const int num, const bool init) :
        mlawCoupledThermoMechanics(num, init), applyReferenceF(false)
{
    Msg::Error("mlawGenericTM: Cannot use the default constructor");
    _mecaLaw=NULL;
}

mlawGenericTM::mlawGenericTM(const int num,
                             const double alpha, const double beta, const double gamma,
                             const double t0, const double Kx, const double Ky,
                             const double Kz, const double alphax, const double alphay,
                             const double alphaz, const double cp) :
                             mlawCoupledThermoMechanics(num),
                             _alpha(alpha), _beta(beta), _gamma(gamma), _t0(t0), _Kx(Kx), _Ky(Ky),
                             _Kz(Kz), _alphax(alphax), _alphay(alphay), _alphaz(alphaz), applyReferenceF(false)
{
    _mecaLaw=NULL;

    _cp = new constantScalarFunction(cp);

    STensor3 R;		//3x3 rotation matrix
    double c1,c2,c3,s1,s2,s3;
    double s1c2, c1c2;
    double pi(3.14159265359);
    double fpi = M_PI/180.;

    c1 = std::cos(_alpha*fpi);
    s1 = std::sin(_alpha*fpi);

    c2 = std::cos(_beta*fpi);
    s2 = std::sin(_beta*fpi);

    c3 = std::cos(_gamma*fpi);
    s3 = std::sin(_gamma*fpi);

    s1c2 = s1*c2;
    c1c2 = c1*c2;

    R(0,0) = c3*c1 - s1c2*s3;
    R(0,1) = c3*s1 + c1c2*s3;
    R(0,2) = s2*s3;

    R(1,0) = -s3*c1 - s1c2*c3;
    R(1,1) = -s3*s1 + c1c2*c3;
    R(1,2) = s2*c3;

    R(2,0) = s1*s2;
    R(2,1) = -c1*s2;
    R(2,2) = c2;


    STensor3 k,alphaDilatation;
    // to be unifom in DG3D q= k' gradT with k'=-k instead of q=-kgradT
    k(0,0)  = -_Kx;
    k(1,1)  = -_Ky;
    k(2,2)  = -_Kz;
    alphaDilatation(0,0)  = _alphax;
    alphaDilatation(1,1)  = _alphay;
    alphaDilatation(2,2)  = _alphaz;

    for(int i=0;i<3;i++)
    {
        for(int j=0;j<3;j++)
        {
            _k(i,j)=0.;
            _alphaDilatation(i,j)=0.;
            for(int m=0;m<3;m++)
            {
                for(int n=0;n<3;n++)
                {
                    _k(i,j)+=R(m,i)*R(n,j)*k(m,n);
                    _alphaDilatation(i,j)+=R(m,i)*R(n,j)*alphaDilatation(m,n);
                }
            }
        }
    }

}

void mlawGenericTM::setLawForCp(const scalarFunction& funcCp)
{
    if (_cp != NULL) delete _cp;
    _cp = funcCp.clone();
}

mlawGenericTM::mlawGenericTM(const mlawGenericTM& source) : mlawCoupledThermoMechanics(source),
_alpha(source._alpha), _beta(source._beta), _gamma(source._gamma), _cv(source._cv), _t0(source._t0),
_Kx(source._Kx), _Ky(source._Ky), _Kz(source._Kz), _alphax(source._alphax), _alphay(source._alphay),
_alphaz(source._alphaz), _k(source._k), _alphaDilatation(source._alphaDilatation),  applyReferenceF(source.applyReferenceF),
_mecaLaw(NULL)
{
    if(source._mecaLaw!=NULL)
    {
        if(_mecaLaw != NULL)
        {
            delete _mecaLaw;
            _mecaLaw = NULL;
        }
        _mecaLaw=source._mecaLaw->clone();
    }
    _cp = NULL;
    if(source._cp)
        _cp = source._cp->clone();
}

mlawGenericTM& mlawGenericTM::operator=(const materialLaw& source)
{
    mlawCoupledThermoMechanics::operator=(source);
    const mlawGenericTM* src =static_cast<const mlawGenericTM*>(&source);
   if(src !=NULL)
    {
        applyReferenceF=src->applyReferenceF;
        if(src->_mecaLaw!=NULL)
        {
            if(_mecaLaw != NULL)
            {
                _mecaLaw->operator=(dynamic_cast<const materialLaw& >(*(src->_mecaLaw)));
            }
            else
              _mecaLaw=src->_mecaLaw->clone();
        }
        _alpha = src->_alpha;
        _beta = src->_beta;
        _gamma = src->_gamma;
        _cv = src->_cv;
        _t0 = src->_t0,
        _Kx = src->_Kx;
        _Ky = src->_Ky;
        _Kz  = src->_Kz;
        _alphax = src->_alphax;
        _alphay = src-> _alphay;
        _alphaz = src->_alphaz;
        _k = src->_k;
        _alphaDilatation=src->_alphaDilatation;

        if (_cp!= NULL) delete _cp; _cp = NULL;
        if (src->_cp != NULL) _cp = src->_cp->clone();

    }
    return *this;
}

//createIPVariable cf mlawTFA
void mlawGenericTM::createIPState(IPStateBase* &ips,bool hasBodyForce, const bool* state_,const MElement *ele, const int nbFF_,
                   const IntPt *GP, const int gpt) const
{
    if(ips != NULL) delete ips;
    IPGenericTM *ipvi=new IPGenericTM();
    IPGenericTM *ipv1=new IPGenericTM();
    IPGenericTM *ipv2=new IPGenericTM();

    IPStateBase* ipsmeca=NULL;
    getConstRefMechanicalMaterialLaw().createIPState(ipsmeca,hasBodyForce, state_,ele, nbFF_,GP, gpt);
    std::vector<IPVariable*> allIP;
    ipsmeca->getAllIPVariable(allIP);

    ipvi->setIpMeca(*allIP[0]->clone());
    ipv1->setIpMeca(*allIP[1]->clone());
    ipv2->setIpMeca(*allIP[2]->clone());

    ips = new IP3State(state_,ipvi,ipv1,ipv2);
    delete ipsmeca;
}

void mlawGenericTM::createIPVariable(IPGenericTM* &ipv, bool hasBodyForce, const MElement *ele, const int nbFF,
                                     const IntPt *GP, const int gpt) const
{
    if(ipv != NULL)
        delete ipv;
    ipv = new IPGenericTM();

    IPStateBase* ipsmeca=NULL;
    const bool state_ = true;
    getConstRefMechanicalMaterialLaw().createIPState(ipsmeca,hasBodyForce, &state_,ele, nbFF,GP, gpt);

    std::vector<IPVariable*> allIP;
    ipsmeca->getAllIPVariable(allIP);

    ipv->setIpMeca(*allIP[0]->clone());
    delete ipsmeca;
}

void mlawGenericTM::constitutive(const STensor3   &F0,
                                 const STensor3   &Fn,
                                 STensor3         &P,
                                 const STensor3   &P0,
                                 const IPVariable *q0,
                                 IPVariable       *q1,
                                 STensor43        &Tangent,
                                 const double     T0,
                                 double           T,
                                 const SVector3   &gradT,
                                 SVector3         &fluxT,
                                 STensor3         &dPdT,
                                 STensor3         &dqdgradT,
                                 SVector3         &dqdT,
                                 STensor33        &dqdF,
                                 const bool       stiff,
                                 double           &wth,
                                 double           &dwthdt,
                                 STensor3         &dwthdF,
                                 double           &mechSource,
                                 double           &dmechSourcedT,
                                 STensor3         &dmechSourcedF,
                                 STensor43        *elasticTangent
                               ) const
{
    if(_mecaLaw == NULL)
        Msg::Error("Mechanic law is null");

    //initilisation of variables to zero
    wth=0.;
    dwthdt=0.;
    STensorOperation::zero(dwthdF);
    mechSource=0.;
    dmechSourcedT=0.;
    STensorOperation::zero(dmechSourcedF);
    STensorOperation::zero(fluxT);
    STensorOperation::zero(dqdT);
    STensorOperation::zero(dqdF);
    STensorOperation::zero(dqdgradT);
    STensorOperation::zero(P);
    STensorOperation::zero(dPdT);
    STensorOperation::zero(Tangent);

    const double dh= getTimeStep();

    static STensor3 _kt;
    STensorOperation::zero(_kt);
    static STensor3 dktdT,dkdT;
    static STensor43 dktdF, dkdF;
    STensorOperation::zero(dkdT);
    STensorOperation::zero(dktdT);
    STensorOperation::zero(dktdF);
    STensorOperation::zero(dkdF);

    const IPGenericTM &qTM0= dynamic_cast<const IPGenericTM &> (*q0);
    IPGenericTM &qTM1= dynamic_cast<IPGenericTM &> (*q1);

    static STensor3 FRef,FRefInv;
    static STensor43 dFRefdFn;
    STensorOperation::zero(FRef);
    STensorOperation::zero(FRefInv);
    STensorOperation::zero(dFRefdFn);

    const STensor3 I2(1.0);
    const STensor43 I4(2.0,0.0);

    if(applyReferenceF)
    {
       //FRef = Fn; // here we have Fn identity in any case but I would leave it to be correct
       FRef = qTM1.getRefToReferenceF();
        for (int i=0; i<3; i++){
            for (int j=0; j<3; j++){
                for (int k=0; k<3; k++){
                    for (int m=0; m<3; m++){
                        for (int n=0; n<3; n++){
                            dFRefdFn(i,k,m,n) += qTM1.getRefToReferenceF()(i,j) * I2(j,m) * I2(k,n); /// to chnage to reflect order
                        }
                    }
                }
            }
        }
    }
    else
    {
        FRef = Fn;
        dFRefdFn = I4;
    }
    // need to updqte kt etc...
    STensorOperation::zero(FRefInv);
    STensorOperation::inverseSTensor3(FRef, FRefInv);
    const double JacRef = FRef.determinant();

    // Large deformations

    for (unsigned int K = 0; K < 3; ++K)
        for (unsigned int L = 0; L < 3; ++L)
            for (unsigned int i = 0; i < 3; ++i)
                for (unsigned int j = 0; j < 3; ++j)
                {
                    _kt(K, L) += FRefInv(K, i) * _k(i, j) * FRefInv(L, j);
                    dktdT(K, L) += FRefInv(K, i) * dkdT(i, j) * FRefInv(L, j);

                    for (unsigned int m = 0; m < 3; ++m)
                        for(unsigned int n = 0; n < 3; ++n)
                        {
                            // Need to check this
                            dktdF(K,L,m,n) += - FRefInv(K,n) * _kt(m,L)
                                                        - _kt(K,m) * FRefInv(L,n)
                                                        + _kt(K,L) * FRefInv(m,n);
                        }

                }

    _kt *= JacRef;
    dktdT *= JacRef;
    dktdF *= JacRef;

    //thermal flux like Lina but from kt
    STensorOperation::zero(fluxT);
    for(int i=0;i<3;i++)
    {
        for(int j=0;j<3;j++)
        {
            fluxT(i)+=_kt(i,j)*gradT(j);
        }
    }

    if(stiff)
    {
        for(int i=0;i<3;i++)
        {
            dqdT(i)=0.;
            for(int j=0;j<3;j++)
            {
                dqdT(i) += dktdT(i,j)*gradT(j);
            }
        }

        dqdgradT=_kt;

        // dependency of flux with deformation gradient
        for(int K = 0; K< 3; K++)
        {
            for(int m = 0; m< 3; m++)
            {
                for(int N = 0; N< 3; N++)
                {
                    for(int L = 0; L< 3; L++)
                    {
                        dqdF(K,m,N) -= FRefInv(K,m)*_kt(N,L)*gradT(L);
                        dqdF(K,m,N) -= _kt(K,N)*FRefInv(L,m)*gradT(L);
                        dqdF(K,m,N) += _kt(K,L)*gradT(L)*FRefInv(N,m);
                    }
                }
            }
        }

    }

    const IPVariableMechanics& qMeca0 = dynamic_cast<const IPGenericTM&>(*q0).getConstRefToIpMeca();
    IPVariableMechanics& qMecan = dynamic_cast<IPGenericTM&>(*q1).getRefToIpMeca();

    // wth & mecaSource
    double dcpdT = _cp->getDiff(T);
    static STensor3 dcpdF;
    STensorOperation::zero(dcpdF);
    if(dh>0.0)
    {
        wth = -_cp->getVal(T)*(T-T0)/dh;
        mechSource = 0.0; // Plas+Visc Energy/dh;
    }
    else
    {
        wth=0.;
        mechSource = 0.0;
    }

    if(dh>0.0)
    {
        dwthdt = -dcpdT*(T-T0)/dh - _cp->getVal(T)/dh;
        dmechSourcedT = 0.0; // dPlasdT + dViscdT Energy / dh;
        for(int K = 0; K< 3; K++){
            for(int i = 0; i< 3; i++){
                dwthdF(K,i) += (-dcpdF(K,i))*(T-T0)/dh;
                dmechSourcedF(K,i) += 0.0; // dPlasdF(K,i) + dViscdF(K,i) / dh;
            }
        }
    }
    else
    {
        dwthdt=0.;
        STensorOperation::zero(dwthdF);
        dmechSourcedT = 0.0;
        STensorOperation::zero(dmechSourcedF);
    }

    qTM1._thermalEnergy = _cp->getVal(T0)*T; /// ????

    //Fthermal see my SMP for 0 and n
    STensor3 aux, dauxdT, lamC;
    STensor43 dlamC;
    STensorOperation::zero(aux);
    STensorOperation::zero(dauxdT);
    STensorOperation::zero(lamC);
    STensorOperation::zero(dlamC);

    for(int i=0; i<3; i++)
    {
        for(int j=0; j<3; j++)
        {
            aux(i,j)    += _alphaDilatation(i,j) * (T - T0);
            dauxdT(i,j) += _alphaDilatation(i,j);
        }
    }

    STensorOperation::expSTensor3(aux,-1,lamC,&dlamC);

    const STensor3 &FthI0 =qTM0.getConstRefToFthI(); // Need to check this ????
    STensor3& FthI = qTM1.getRefToFthI();
    STensor3 dFthIdT;
    STensor43 dFthIdF;

    STensorOperation::zero(FthI);
    STensorOperation::zero(dFthIdT);
    STensorOperation::zero(dFthIdF);

    if(getTime()-_timeStep<0.)
    {
        FthI= FthI0;
        dFthIdT*=0.0;
    }
    else
    {
        for(int i=0; i<3; i++)
        {
            for(int j=0; j<3; j++)
            {
                for(int k=0; k<3; k++)
                {
                    FthI(i,k) += lamC(i,j)*FthI0(j,k);
                    for(int l=0; l<3; l++)
                    {
                        for(int m=0; m<3; m++)
                        {
                            dFthIdT(i,k) += dlamC(i,j,l,m)*dauxdT(l,m)*FthI0(j,k);
                        }
                    }
                }
            }
        }
    }

    STensor3 FthIinv, FthIinvT;
    STensor43 dFthIinvdF;
    STensor3 dFthIinvdT;

    STensorOperation::zero(FthIinv);
    STensorOperation::zero(FthIinvT);
    STensorOperation::zero(dFthIinvdF);
    STensorOperation::zero(dFthIinvdT);

    STensorOperation::inverseSTensor3(FthI, FthIinv);
    STensorOperation::transposeSTensor3(FthIinv, FthIinvT);

    for (int i=0; i<3; i++){
        for (int j=0; j<3; j++){
            for (int p=0; p<3; p++){
                for (int q=0; q<3; q++){
                    dFthIinvdT(i,j) -= FthIinv(i,p) * dFthIdT(p,q) * FthIinv(q,j);
                    for (int m=0; m<3; m++){
                        for (int n=0; n<3; n++){
                            dFthIinvdF(i,j,m,n) -= FthIinv(i,p)*dFthIdF(p,q,m,n)*FthIinv(q,j);
                        }
                    }
                }
            }
        }
    }

    //Fmeca from F and Fthermal (possible using reference F?) for 0 and n
    STensor3 Fmeca;
    const STensor3& Fmeca0 = qTM0.getConstRefToReferenceF(); // Need to check this ????
    STensorOperation::zero(Fmeca);


    // Fmeca from F and Fth
    STensorOperation::multSTensor3(FRef, FthIinv, Fmeca);

    STensor43 dFmecadF;
    STensor3 dFmecadT;
    STensorOperation::zero(dFmecadF);
    STensorOperation::zero(dFmecadT);

    for (int i=0; i<3; i++){
        for (int j=0; j<3; j++){
            for (int k=0; k<3; k++){
                dFmecadT(i,k) +=FRef(i,j)*dFthIinvdT(j,k);
                for(int l = 0; l < 3; ++l){
                    dFmecadF(i,j,k,l)=I2(i,k)*FthIinv(l,j);
                    for (int m=0; m<3; m++){
                        for (int o=0; o<3; o++) {
                            dFmecadF(i, j, k, l) += FRef(i, m) * dFthIinvdF(m, o, k, l) * I2(o, j);
                            //dFmecadF(i,k,m,o) += I2(i,m)*FthIinv(j,k)*I2(j,o) + FRef(i,j) * dFthIinvdF(j,k,m,o);
                        }
                    }
                }
            }
        }
    }

    STensor3 Pmeca;
    STensor43 TangentMeca;
    STensor43 elasticTangentMeca;

    STensorOperation::zero(Pmeca);
    STensorOperation::zero(TangentMeca);
    STensorOperation::zero(elasticTangentMeca);

    const bool dTangentMeca(true);

    _mecaLaw->constitutive(Fmeca0, Fmeca, Pmeca,
                           &qTM0.getConstRefToIpMeca(),
                           &qTM1.getRefToIpMeca(),
                           TangentMeca, stiff,
                           &elasticTangentMeca, dTangentMeca);
     if(STensorOperation::isnan(Pmeca))
     {
       Msg::Info("mlawGenericTM::constitutive did not converge");
       P(0,0) = P(1,1) = P(2,2) = sqrt(-1.); // to exist NR and performed next step with a smaller incremene
       return;
     }

    STensor3 dPmecadT;
    STensor43 dPmecadF;

    STensorOperation::zero(dPmecadT);
    STensorOperation::zero(dPmecadF);

    // P, Tangent etc..; from Pmeca and Fth and Fref

    // get P
    //STensorOperation::multSTensor3STensor43(Pmeca, dFmecadF, P);
    // or
    STensorOperation::multSTensor3(Pmeca, FthIinvT, P);

    // dPdT
    if(stiff){
        for(int i=0; i<3; i++)
        {
            for(int J=0; J<3; J++)
            {
                dPdT(i,J)=0.0;
                for(int M=0; M<3; M++)
                {
                    dPdT(i,J)+=dPmecadT(i,M)*FthIinvT(M,J);
                    dPdT(i,J)+= Pmeca(i,M)*dFthIinvdT(M,J);
                    for(int o=0; o<3; o++)
                    {
                        for(unsigned int p = 0; p < 3; ++p)
                            dPdT(i,J)+= TangentMeca(i, M, o,p)*dFmecadT(o,p)*FthIinvT(M,J);
                    }
                }
            }
        }
    }

    // get Tangent
    if(stiff){
        for(int i=0; i<3; i++)
        {
            for(int J=0; J<3; J++)
            {
                for(int k=0; k<3; k++)
                {
                    for(int L=0; L<3; L++)
                    {
                        Tangent(i,J,k,L)=0.0;
                        for(int M=0; M<3; M++)
                        {

                            for(int o=0; o<3; o++)
                            {
                                for(int P=0; P<3; P++)
                                {
                                    Tangent(i,J,k,L)+=TangentMeca(i,M,o,P)*dFmecadF(o,P,k,L)*FthIinvT(M,J);
                                }
                                Tangent(i,J,k,L)+= Pmeca(i,M)*I2(M,o)*dFthIinvdF(J,o,k,L);
                            }
                        }
                    }
                }
            }
        }
    }

}

/*void mlawGenericTM::getStiff_alphaDilatation(STensor3 &stiff_alphaDilatation) const

{
    STensor43 elasticStiffness;
    STensor3 linearK;
    getConstRefMechanicalMaterialLaw().ElasticStiffness(&elasticStiffness);
    linearK     =  getConductivityTensor();
   // TO DO THE PRODUCT
}*/
void mlawGenericTM::ElasticStiffness(STensor43 *elasticStiffness) const {
    getConstRefMechanicalMaterialLaw().ElasticStiffness(elasticStiffness);
}
