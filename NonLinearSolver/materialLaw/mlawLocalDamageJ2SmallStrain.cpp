//
// C++ Interface: material law
//
// Description: local damage law
//
// Author:  <V-D Nguyen>, (C) 2021
//
// Copyright: See COPYING file that comes with this distribution
//
//

#include "mlawLocalDamageJ2SmallStrain.h"

mlawLocalDamageJ2SmallStrain::mlawLocalDamageJ2SmallStrain(const int num,const double E,const double nu,
                           const double rho, const J2IsotropicHardening &_j2IH,
                           const DamageLaw &_damLaw,
			   const double tol, const bool pert, const double eps)
				: mlawJ2VMSmallStrain(num,E,nu,rho,_j2IH,tol,pert,eps)
{
  damLaw = _damLaw.clone();
}
mlawLocalDamageJ2SmallStrain::mlawLocalDamageJ2SmallStrain(const mlawLocalDamageJ2SmallStrain &source) : mlawJ2VMSmallStrain(source)
{
  damLaw  = NULL;
  if(source.damLaw != NULL)
  {
    damLaw=source.damLaw->clone();
  }
}

void mlawLocalDamageJ2SmallStrain::createIPState(IPStateBase* &ips, bool hasBodyForce, const bool* state_,const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
  IPVariable* ipvi = new IPLocalDamageJ2Hyper(_j2IH,damLaw);
  IPVariable* ipv1 = new IPLocalDamageJ2Hyper(_j2IH,damLaw);
  IPVariable* ipv2 = new IPLocalDamageJ2Hyper(_j2IH,damLaw);
  if(ips != NULL) delete ips;
  ips = new IP3State(state_,ipvi,ipv1,ipv2);
}

double mlawLocalDamageJ2SmallStrain::soundSpeed() const
{
  return mlawJ2VMSmallStrain::soundSpeed();
}


void mlawLocalDamageJ2SmallStrain::constitutive(const STensor3& F0,
                                          const STensor3& Fn,
                                          STensor3 &P,
                                          const IPVariable *_q0, 
                                          IPVariable *_q1,
                                          STensor43 &Tangent, 
                                          const bool stiff, 
                                          STensor43* elasticTangent,
                                          const bool dTangent,
                                          STensor63* dCalgdeps) const
{

  const IPLocalDamageJ2Hyper *q0=dynamic_cast<const IPLocalDamageJ2Hyper *> (_q0); 
  IPLocalDamageJ2Hyper *q1=dynamic_cast<IPLocalDamageJ2Hyper *> (_q1);

  static STensor43 depsEldF;
  static STensor3 Peff;
  static STensor3 dpdF;
  static const STensor3 I(1.); // identity tensor

  mlawJ2VMSmallStrain::predictorCorector(F0,Fn,Peff,q0,q1,Tangent,depsEldF,dpdF,stiff,elasticTangent);

  const STensor3& epsPl = q1->_j2lepsp;
  const STensor3& epsEl = q1->_Ee;
  double ene = q1->defoEnergy();

  if (q1->dissipationIsBlocked()){
  //if (1){
    // at bulk elelent when damage is blocked
    // damage stop increasing
    IPDamage& curDama = q1->getRefToIPDamage();
    curDama.getRefToDamage() = q0->getDamage();
    curDama.getRefToDDamageDp() = 0.;
    STensorOperation::zero(curDama.getRefToDDamageDFe());
    curDama.getRefToDeltaDamage() = 0;
    curDama.getRefToMaximalP() = q1->getMaximalP();
    //Msg::Info("bulk damage stops increasing = %e",q1->getDamage());

    q1->getRefToDissipationActive() = (false);
  }
  else{
    // compute damage for ipcur                        
    damLaw->computeDamage(q1->getConstRefToEquivalentPlasticStrain(),
                        q0->getConstRefToEquivalentPlasticStrain(),
                        ene, epsEl, epsPl, Peff, _Cel,
                        q0->getConstRefToIPDamage(),q1->getRefToIPDamage());
                                                
    // check active damage
    if ((q1->getDamage() > q0->getDamage()) and (q1->getConstRefToEquivalentPlasticStrain() > q0->getConstRefToEquivalentPlasticStrain()))
    {
      q1->getRefToDissipationActive()  = (true);
    }
    else{
      q1->getRefToDissipationActive() = (false);
    }
  }

  // get true PK1 stress from damage and effective PK1 stress
  double D = q1->getDamage();
	P = Peff;
  P*=(1.-D);
  double elEne = (1-D)*ene;
  q1->_elasticEnergy=elEne;
  
  q1->getRefToPlasticPower() *= (1.-D);

  double dPlasticDisp = q1->plasticEnergy() - q0->plasticEnergy();
  q1->getRefToPlasticEnergy() = q0->plasticEnergy() + (1-D)*dPlasticDisp;
  q1->getRefToDamageEnergy() =  q0->damageEnergy() + ene*(D-q0->getDamage());
  
  //computation of the elastic tangent with damage
  STensor43 elTang;
  elTang = (1.-D)*_Cel;
  if(elasticTangent !=NULL) elasticTangent->operator=(elTang);

  if(stiff)
  {
    static STensor3 DDamageDF;

    for (int i=0; i<3; i++){
      for (int j=0; j<3; j++){
        DDamageDF(i,j) = 0.;
        for (int k=0; k<3; k++){
          for (int l=0; l<3; l++){
            DDamageDF(i,j) += q1->getConstRefToDDamageDFe()(k,l)*depsEldF(k,l,i,j);
          }
        }
      }
    }

    for (int i=0; i<3; i++){
      for (int j=0; j<3; j++){
        DDamageDF(i,j) +=q1->getDDamageDp()*dpdF(i,j);
      }
    }

    Tangent*=(1.-D);
    for(int i=0;i<3;i++)
    {
      for(int j=0;j<3;j++)
      {
        for(int k=0;k<3;k++)
        {
          for(int l=0;l<3;l++)
          {
            Tangent(i,j,k,l)-=Peff(i,j)*DDamageDF(k,l);
          }
        }
      }
    }

  }
}
