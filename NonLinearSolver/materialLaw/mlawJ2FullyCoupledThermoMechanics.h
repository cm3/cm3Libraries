//
// C++ Interface: material law
//
// Description: j2 thermo elasto-plastic law

// Author:  <Van Dung NGUYEN>, (C) 2016
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef MLAWJ2FULLYCOUPLEDTHERMOMECHANICS_H_
#define MLAWJ2FULLYCOUPLEDTHERMOMECHANICS_H_

#include "mlawJ2linear.h"
#include "scalarFunction.h"
#include "ipJ2ThermoMechanics.h"

class mlawJ2FullyCoupledThermoMechanics : public materialLaw {
  protected:
    // hardening at reference temperature
    J2IsotropicHardening *_j2IH; // isotropic hardening
    // temperature dependence of hardening
    scalarFunction* _temFunc_Sy0; // temperature dependence of initial yield stress
    scalarFunction* _temFunc_H; // temperature dependence of hardening stress

    // temperature dependent elastic behavior
    double  _K, _mu;
    scalarFunction* _temFunc_K; // bulk modulus;
    scalarFunction* _temFunc_mu; // shear modulus;

    double _alp; // thermal expansion parameter
    scalarFunction* _tempFunc_alp; // temperature dependent
    double _Tref; // reference tempereture;


    double _tol; // tolerace of plastic corrector
    double _rho; // density

    double _perturbationfactor; // perturbation factor
    bool _tangentByPerturbation; // flag for tangent by perturbation

    //
    int _order; // order for log and exp approximation

    STensor43 _I4dev;

    double _TaylorQuineyFactor; // for heat conversion

    double _ThermalConductivity;
    double _cp; // heat capacity per unit field
    scalarFunction* _temFunc_ThermalConductivity;
    scalarFunction* _temFunc_cp;

    bool _thermalEstimationPreviousConfig;

		STensor3  linearK;
		STensor3	Stiff_alphaDilatation;

  public:
    mlawJ2FullyCoupledThermoMechanics(const int num, const double E, const double nu,const double rho,
                               const double sy0,const double h,const double tol=1.e-6,
                               const bool matrixbyPerturbation = false, const double pert = 1e-8,
                               const bool init =true);

    mlawJ2FullyCoupledThermoMechanics(const mlawJ2FullyCoupledThermoMechanics& src);
    mlawJ2FullyCoupledThermoMechanics& operator=(const materialLaw &source);
    virtual ~mlawJ2FullyCoupledThermoMechanics();

    virtual materialLaw* clone() const {return new mlawJ2FullyCoupledThermoMechanics(*this);};
    virtual void checkInternalState(IPVariable* ipv, const IPVariable* ipvprev) const{}; // do nothing
    virtual bool withEnergyDissipation() const {return true;};

    const STensor3& getConductivityTensor() const {return linearK;};
    const STensor3& getStiff_alphaDilatation() const {return Stiff_alphaDilatation;};

    void setReferenceTemperature(const double Tr){_Tref = Tr;};
    virtual const J2IsotropicHardening * getJ2IsotropicHardening() const {return _j2IH;}
    void setIsotropicHardeningLaw(const J2IsotropicHardening& isoHard){
      if (_j2IH != NULL) delete _j2IH;
      _j2IH = isoHard.clone();
    }
    void setReferenceThermalExpansionCoefficient(const double al){
			_alp = al;
			STensorOperation::zero(Stiff_alphaDilatation);
			Stiff_alphaDilatation(0,0) = al;
			Stiff_alphaDilatation(1,1) = al;
			Stiff_alphaDilatation(2,2) = al;
		};
    void setTemperatureFunction_ThermalExpansionCoefficient(const scalarFunction& Tfunc){
      if (_tempFunc_alp != NULL) delete _tempFunc_alp;
      _tempFunc_alp = Tfunc.clone();
    }

    void setReferenceThermalConductivity(const double KK){
			_ThermalConductivity = KK;
			STensorOperation::zero(linearK);
			linearK(0,0) = KK;
			linearK(1,1) = KK;
			linearK(2,2) = KK;
		};
    void setTemperatureFunction_ThermalConductivity(const scalarFunction& Tfunc){
      if (_temFunc_ThermalConductivity != NULL) delete _temFunc_ThermalConductivity;
      _temFunc_ThermalConductivity = Tfunc.clone();
    }

    void setReferenceCp(const double Cp) {_cp = Cp;};
    void setTemperatureFunction_Cp(const scalarFunction& Tfunc){
      if (_temFunc_cp != NULL) delete _temFunc_cp;
      _temFunc_cp = Tfunc.clone();
    }

    void setTemperatureFunction_Hardening(const scalarFunction& Tfunc){
      if (_temFunc_H != NULL) delete _temFunc_H;
      _temFunc_H = Tfunc.clone();
    }
    void setTemperatureFunction_InitialYieldStress(const scalarFunction& Tfunc){
      if (_temFunc_Sy0 != NULL) delete _temFunc_Sy0;
      _temFunc_Sy0 = Tfunc.clone();
    }
    void setTemperatureFunction_BulkModulus(const scalarFunction& Tfunc){
      if (_temFunc_K != NULL) delete _temFunc_K;
      _temFunc_K = Tfunc.clone();
    }
    void setTemperatureFunction_ShearModulus(const scalarFunction& Tfunc){
      if (_temFunc_mu != NULL) delete _temFunc_mu;
      _temFunc_mu = Tfunc.clone();
    }
    void setThermalEstimationPreviousConfig(const bool flag){
      _thermalEstimationPreviousConfig = flag;
    }

    void setStrainOrder(const int i){_order = i;};
    void setTaylorQuineyFactor(const double f){_TaylorQuineyFactor = f;};

    virtual matname getType() const{return materialLaw::J2;}
    virtual void createIPState(IPStateBase* &ips,bool hasBodyForce, const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const IntPt *GP=NULL, const int gpt = 0) const;
    virtual void initLaws(const std::map<int,materialLaw*> &maplaw){};
    virtual double soundSpeed() const;
    virtual double density() const{return _rho;};

    virtual double shearModulus() const {return _mu;}
    virtual double getInitialExtraDofStoredEnergyPerUnitField() const {return _cp;};
    virtual double getExtraDofStoredEnergyPerUnitField(double T) const {return _cp*_temFunc_cp->getVal(T);};

    // predictor-corrector estimation
    virtual void constitutive(const STensor3& F0,         // previous deformation gradient (input @ time n)
                            const STensor3& Fn,         // current deformation gradient (input @ time n+1)
                            STensor3 &P,                // current 1st Piola-Kirchhoff stress tensor (output)
                            const IPVariable *q0,       // array of previous internal variables
                            IPVariable *q1,             // current array of internal variable (in ipvcur on output),
                            STensor43 &Tangent,         // tangents (output)
                            const bool stiff,            // if true compute the tangents
                            STensor43* elasticTangent = NULL, 
                            const bool dTangent =false,
                            STensor63* dCalgdeps = NULL) const;
    virtual void constitutive(const STensor3& F0,         // initial deformation gradient (input @ time n)
                            const STensor3& F1,         // updated deformation gradient (input @ time n+1)
                            STensor3 &P1,                // updated 1st Piola-Kirchhoff stress tensor (output)
                            const IPVariable *q0,       // array of initial internal variable
                            IPVariable *q1,             // updated array of internal variable (in ipvcur on output),
                            STensor43 &Tangent,         // mechanical tangents (output)
                            const double& T0, // previous temperature
			                      const double& T, // temperature
			                      const SVector3 &gradT0, // previoustemeprature gradient
  			                    const SVector3 &gradT, // temeprature gradient
                            SVector3 &fluxT, // temperature flux
                            STensor3 &dPdT, // mechanical-thermal coupling
                            STensor3 &dfluxTdgradT, // thermal tengent
                            SVector3 &dfluxTdT,
                            STensor33 &dfluxTdF, // thermal-mechanical coupling
                            double &thermalSource,   // - cp*dTdt
                            double &dthermalSourcedT, // thermal source
                            STensor3 &dthermalSourcedF,
                            double &mechanicalSource, // mechanical source--> convert to heat
                            double &dmechanicalSourcedT,
                            STensor3 & dmechanicalSourceF,
                            const bool stiff,
                            STensor43* elasticTangent = NULL) const;

  protected:
    void stress(const STensor3& Ee, const double & KT, const double& muT, const double& alpT, const double& Tr, const double& T, double& p, STensor3& corKir) const;
    void HookeTensor(const double K, const double mu, STensor43& C) const;
    double deformationEnergy(const STensor3& Ce, const double& Tr, const double& T) const;

    void predictorCorector(const STensor3& F0,         // initial deformation gradient (input @ time n)
                            const STensor3& F,         // updated deformation gradient (input @ time n+1)
                            STensor3 &P,                // updated 1st Piola-Kirchhoff stress tensor (output)
                            const IPJ2ThermoMechanics *q0,       // array of initial internal variable
                            IPJ2ThermoMechanics *q,             // updated array of internal variable (in ipvcur on output),
                            const double& T0, // previous temperature
			                      const double& T, // temperature
			                      const SVector3 &gradT0, // previous temeprature gradient
  			                    const SVector3 &gradT, // temeprature gradient
                            SVector3 &fluxT, // temperature flux)
                            double &thermalSource,
                            double& mechanicalSource
                            ) const;

    void predictorCorector(const STensor3& F0,         // initial deformation gradient (input @ time n)
                            const STensor3& F1,         // updated deformation gradient (input @ time n+1)
                            STensor3 &P1,                // updated 1st Piola-Kirchhoff stress tensor (output)
                            const IPJ2ThermoMechanics *q0,       // array of initial internal variable
                            IPJ2ThermoMechanics *q1,             // updated array of internal variable (in ipvcur on output),
                            STensor43 &Tangent,         // mechanical tangents (output)
                            STensor43 &dFpdF, // plastic tangent
                            STensor3 &dFpdT, // plastic tangent
                            STensor43 &dFedF, // elastic tangent
                            STensor3 &dFedT, // elastic tangent
                            STensor3 &dGammadF,  //d plastic strain dF
                            double &dGammadT,     //d plastic strain dT
                            const double& T0, // previous temperature
                            const double& T, // temperature
                            const SVector3 &gradT0, // previoustemeprature gradient
                            const SVector3 &gradT, // temeprature gradient
                            SVector3 &fluxT, // temperature flux
                            STensor3 &dPdT, // mechanical-thermal coupling
                            STensor3 &dfluxTdgradT, // thermal tengent
                            SVector3 &dfluxTdT,
                            STensor33 &dfluxTdF, // thermal-mechanical coupling
                            double &thermalSource,   // - cp*dTdt
                            double &dthermalSourcedT, // thermal source
                            STensor3 &dthermalSourcedF,
                            double &mechanicalSource, // mechanical source--> convert to heat
                            double &dmechanicalSourcedT,
                            STensor3 & dmechanicalSourceF,
                            const bool stiff
                            ) const; // tangent is used by pointer, if NULL->no compute
};

#endif // MLAWJ2FULLYCOUPLEDTHERMOMECHANICS_H_
