//
// C++ Interface: material law
//
// Author:  <Van Dung Nguyen>, (C) 2017
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef ELASTICPOTENTIAL_H_
#define ELASTICPOTENTIAL_H_

#include "mlaw.h"
#include "STensor3.h"
#include "STensor43.h"
#include "STensorOperations.h"
#include <limits>

class elasticPotential{
	#ifndef SWIG
	protected:
		int _num;
		
	public:
		elasticPotential(const int num):_num(num){};
		elasticPotential(const elasticPotential& src): _num(src._num){}
		virtual ~elasticPotential(){};
    virtual const STensor43& getElasticTensor() const = 0;
		virtual double getYoungModulus() const = 0;
		virtual double getPoissonRatio() const = 0;
		virtual double get(const STensor3& F) const  = 0; // elastic potential
		virtual void constitutive(const STensor3& F, STensor3& P, const bool sitff, STensor43* L,
                            const bool dTangent=false, STensor63* dCalgdeps = NULL) const = 0; // constitutive law
    
		virtual void getLocalValForNonLocalEquation(const STensor3& F, double& val, const bool stiff, STensor3* DvalDF) const = 0;
		virtual elasticPotential* clone() const = 0;
    // for anisotropic
    virtual double getPositiveValue(const STensor3& F) const = 0;
    virtual double getNegativeValue(const STensor3& F) const = 0;
    virtual void constitutivePositive(const STensor3& F, STensor3& P, const bool sitff, STensor43* L) const = 0;
    virtual void constitutiveNegative(const STensor3& F, STensor3& P, const bool sitff, STensor43* L) const = 0;
    virtual void getLocalPositiveValForNonLocalEquation(const STensor3& F, double& val, const bool stiff, STensor3* DvalDF) const = 0;
	#endif //SWIG
};

class smallStrainLinearElasticPotential : public elasticPotential{
	protected:
		double _ERef, _nuRef; // elastic constant
		STensor43 _Cel;
		
	public:
		smallStrainLinearElasticPotential(const int num, const double E, const double nu);
    smallStrainLinearElasticPotential(const int num,
                                    const double Ex, const double Ey, const double Ez,
                                    const double Vxy, const double Vxz, const double Vyz,
                                    const double MUxy, const double MUxz, const double MUyz,
                                    const double alpha, const double beta, const double gamma);
    smallStrainLinearElasticPotential(const int num, const STensor43& C);
    smallStrainLinearElasticPotential(const int num, const fullMatrix<double>& C);
    smallStrainLinearElasticPotential(const int num,
                                      const double C00, const double C11, const double C22, const double C33, const double C44, const double C55,
                                      const double C01, const double C02, const double C03, const double C04, const double C05,
                                      const double C12, const double C13, const double C14, const double C15,
                                      const double C23, const double C24, const double C25,
                                      const double C34, const double C35,
                                      const double C45);
		#ifndef SWIG
		smallStrainLinearElasticPotential(const smallStrainLinearElasticPotential& src);
		virtual ~smallStrainLinearElasticPotential(){};
    virtual const STensor43& getElasticTensor() const {return _Cel;};
		virtual double getYoungModulus() const {return _ERef;};
		virtual double getPoissonRatio() const {return _nuRef;};
		virtual double get(const STensor3& F) const; // elastic potential
		virtual void constitutive(const STensor3& F, STensor3& P, const bool sitff, STensor43* L,
                            const bool dTangent=false, STensor63* dCalgdeps = NULL) const; // constitutive law
		virtual void getLocalValForNonLocalEquation(const STensor3& F, double& val, const bool stiff, STensor3* DvalDF) const;
		virtual elasticPotential* clone() const {
			return new smallStrainLinearElasticPotential(*this);
		};
    // for anisotropic
    virtual double getPositiveValue(const STensor3& F) const;
    virtual double getNegativeValue(const STensor3& F) const;
    virtual void constitutivePositive(const STensor3& F, STensor3& P, const bool sitff, STensor43* L) const;
    virtual void constitutiveNegative(const STensor3& F, STensor3& P, const bool sitff, STensor43* L) const;
    virtual void getLocalPositiveValForNonLocalEquation(const STensor3& F, double& val, const bool stiff, STensor3* DvalDF) const;
    
  public:
    static void createElasticTensor(const double Ex, const double Ey, const double Ez,
                                    const double Vxy, const double Vxz, const double Vyz,
                                    const double MUxy, const double MUxz, const double MUyz,
                                    const double alpha, const double beta, const double gamma, STensor43& C);
                                    
    static void createElasticTensor(const double C00, const double C11, const double C22, const double C33, const double C44, const double C55,
                                    const double C01, const double C02, const double C03, const double C04, const double C05,
                                    const double C12, const double C13, const double C14, const double C15,
                                    const double C23, const double C24, const double C25,
                                    const double C34, const double C35,
                                    const double C45, 
                                    STensor43& C);
		#endif //SWIG
};

class largeStrainAnisotropicPotential : public elasticPotential{
   // eps*H*eps
  protected:
    STensor43 _Cel;
    double _Emax;
    double _poissonMax;
    
  public:
    largeStrainAnisotropicPotential(const int num, const double Ex, const double Ey, const double Ez, 
                                    const double Vxy, const double Vxz, const double Vyz,
                                    const double MUxy, const double MUxz, const double MUyz,
                                    const double alpha, const double beta, const double gamma);
    largeStrainAnisotropicPotential(const int num, const STensor43& C);
		#ifndef SWIG
		largeStrainAnisotropicPotential(const largeStrainAnisotropicPotential& src);
		virtual ~largeStrainAnisotropicPotential(){};
    virtual const STensor43& getElasticTensor() const {return _Cel;};
		virtual double getYoungModulus() const {return _Emax;};
		virtual double getPoissonRatio() const {return _poissonMax;};
		virtual double get(const STensor3& F) const; // elastic potential
		virtual void constitutive(const STensor3& F, STensor3& P, const bool sitff, STensor43* L,
                            const bool dTangent=false, STensor63* dCalgdeps = NULL) const; // constitutive law
		virtual void getLocalValForNonLocalEquation(const STensor3& F, double& val, const bool stiff, STensor3* DvalDF) const;
		virtual elasticPotential* clone() const {
			return new largeStrainAnisotropicPotential(*this);
		};
    // for anisotropic
    virtual double getPositiveValue(const STensor3& F) const;
    virtual double getNegativeValue(const STensor3& F) const;
    virtual void constitutivePositive(const STensor3& F, STensor3& P, const bool sitff, STensor43* L) const;
    virtual void constitutiveNegative(const STensor3& F, STensor3& P, const bool sitff, STensor43* L) const;
    virtual void getLocalPositiveValForNonLocalEquation(const STensor3& F, double& val, const bool stiff, STensor3* DvalDF) const;
		#endif //SWIG
};

class largeStrainTransverseIsotropicPotential : public elasticPotential{
   // eps*H*eps
  protected:
    STensor43 _Cel;
    double _Emax;
    double _poissonMax;
    
  public:
    largeStrainTransverseIsotropicPotential(const int num, 
                                    const double ETran, // inplane modulus
                                    const double ELong,  // longitudinal modulus 
                                    const double nuTT, // inplane Poisson ratio
                                    const double nuLT,  // inplane-longitudinal Possion ratio
                                    const double MUTT,  // inplane shear
                                    const double MUTL, // out-plane shear
                                    const double alpha, const double beta, const double gamma); 
		#ifndef SWIG
		largeStrainTransverseIsotropicPotential(const largeStrainTransverseIsotropicPotential& src);
		virtual ~largeStrainTransverseIsotropicPotential(){};
    virtual const STensor43& getElasticTensor() const {return _Cel;};
		virtual double getYoungModulus() const {return _Emax;};
		virtual double getPoissonRatio() const {return _poissonMax;};
		virtual double get(const STensor3& F) const; // elastic potential
		virtual void constitutive(const STensor3& F, STensor3& P, const bool sitff, STensor43* L,
                            const bool dTangent=false, STensor63* dCalgdeps = NULL) const; // constitutive law
		virtual void getLocalValForNonLocalEquation(const STensor3& F, double& val, const bool stiff, STensor3* DvalDF) const;
		virtual elasticPotential* clone() const {
			return new largeStrainTransverseIsotropicPotential(*this);
		};
    // for anisotropic
    virtual double getPositiveValue(const STensor3& F) const;
    virtual double getNegativeValue(const STensor3& F) const;
    virtual void constitutivePositive(const STensor3& F, STensor3& P, const bool sitff, STensor43* L) const;
    virtual void constitutiveNegative(const STensor3& F, STensor3& P, const bool sitff, STensor43* L) const;
    virtual void getLocalPositiveValForNonLocalEquation(const STensor3& F, double& val, const bool stiff, STensor3* DvalDF) const;
		#endif //SWIG
};


class biLogarithmicElasticPotential : public elasticPotential{
	protected:
		double _K, _mu, _E, _nu; // elastic constant
		int _order; // for log approx
		STensor43 _Cel;
		bool _ByPerturbation;
		double _tol;
	
	public:
		biLogarithmicElasticPotential(const int num, const double E, const double nu, const int order, const bool pert = false, const double tol =1e-6);
		#ifndef SWIG
		biLogarithmicElasticPotential(const biLogarithmicElasticPotential& src);
		virtual ~biLogarithmicElasticPotential(){}
    virtual const STensor43& getElasticTensor() const {return _Cel;};
		virtual double getYoungModulus() const {return _E;};
		virtual double getPoissonRatio() const {return _nu;};
		virtual double get(const STensor3& F) const; // elastic potential
		virtual void constitutive(const STensor3& F, STensor3& P, const bool sitff, STensor43* L,
                            const bool dTangent=false, STensor63* dCalgdeps = NULL) const; // constitutive law
		virtual void getLocalValForNonLocalEquation(const STensor3& F, double& val, const bool stiff, STensor3* DvalDF) const;
		virtual elasticPotential* clone() const {
			return new biLogarithmicElasticPotential(*this);
		}
    // for anisotropic
    virtual double getPositiveValue(const STensor3& F) const;
    virtual double getNegativeValue(const STensor3& F) const;
    virtual void constitutivePositive(const STensor3& F, STensor3& P, const bool sitff, STensor43* L) const;
    virtual void constitutiveNegative(const STensor3& F, STensor3& P, const bool sitff, STensor43* L) const;
    virtual void getLocalPositiveValForNonLocalEquation(const STensor3& F, double& val, const bool stiff, STensor3* DvalDF) const;
		#endif //SWIG
};

class NeoHookeanElasticPotential : public elasticPotential{
  protected:
    #ifndef SWIG
    double _K, _mu, _E, _nu;
    STensor43 _Cel;
		bool _ByPerturbation;
		double _tol;
    #endif //SWIG
  public:
    NeoHookeanElasticPotential(const int num, const double E, const double nu, const bool pert = false, const double tol =1e-6);
		#ifndef SWIG
		NeoHookeanElasticPotential(const NeoHookeanElasticPotential& src);
		virtual ~NeoHookeanElasticPotential(){}	
    virtual const STensor43& getElasticTensor() const {return _Cel;};
		virtual double getYoungModulus() const {return _E;};
		virtual double getPoissonRatio() const {return _nu;};
		virtual double get(const STensor3& F) const; // elastic potential
		virtual void constitutive(const STensor3& F, STensor3& P, const bool sitff, STensor43* L,
                            const bool dTangent=false, STensor63* dCalgdeps = NULL) const; // constitutive law
		virtual void getLocalValForNonLocalEquation(const STensor3& F, double& val, const bool stiff, STensor3* DvalDF) const;
		virtual elasticPotential* clone() const {
			return new NeoHookeanElasticPotential(*this);
		}
    // for anisotropic
    virtual double getPositiveValue(const STensor3& F) const;
    virtual double getNegativeValue(const STensor3& F) const;
    virtual void constitutivePositive(const STensor3& F, STensor3& P, const bool sitff, STensor43* L) const;
    virtual void constitutiveNegative(const STensor3& F, STensor3& P, const bool sitff, STensor43* L) const;
    virtual void getLocalPositiveValForNonLocalEquation(const STensor3& F, double& val, const bool stiff, STensor3* DvalDF) const;
    #endif //SWIG
};

class CompressiveNeoHookeanPotential : public elasticPotential{
  protected:
    double _E, _nu, _lambda, _mu;
    STensor43 _Cel;
    
  public:
    CompressiveNeoHookeanPotential(const int num, const double E, const double nu);
		#ifndef SWIG
		CompressiveNeoHookeanPotential(const CompressiveNeoHookeanPotential& src);
		virtual ~CompressiveNeoHookeanPotential(){}	
    virtual const STensor43& getElasticTensor() const {return _Cel;};
		virtual double getYoungModulus() const {return _E;};
		virtual double getPoissonRatio() const {return _nu;};
		virtual double get(const STensor3& F) const; // elastic potential
		virtual void constitutive(const STensor3& F, STensor3& P, const bool sitff, STensor43* L,
                            const bool dTangent=false, STensor63* dCalgdeps = NULL) const; // constitutive law
		virtual void getLocalValForNonLocalEquation(const STensor3& F, double& val, const bool stiff, STensor3* DvalDF) const;
		virtual elasticPotential* clone() const {
			return new CompressiveNeoHookeanPotential(*this);
		}
    // for anisotropic
    virtual double getPositiveValue(const STensor3& F) const;
    virtual double getNegativeValue(const STensor3& F) const;
    virtual void constitutivePositive(const STensor3& F, STensor3& P, const bool sitff, STensor43* L) const;
    virtual void constitutiveNegative(const STensor3& F, STensor3& P, const bool sitff, STensor43* L) const;
    virtual void getLocalPositiveValForNonLocalEquation(const STensor3& F, double& val, const bool stiff, STensor3* DvalDF) const;
    #endif //SWIG
};

// See https://en.wikipedia.org/wiki/Polynomial_(hyperelastic_model)
class HyperElasticPolynomialPotential: public elasticPotential
{
protected:
  #ifndef SWIG
  double _K, _mu, _E, _nu;
  std::vector<std::vector<double> > _A; // Material constants (incompressible part).
  std::vector<double> _D; // Material constants (compressible part).
  STensor43 _Cel;
  bool _ByPerturbation;
  double _tol;
  #endif // SWIG

public:
  HyperElasticPolynomialPotential(const int num, const double E, const double nu, const bool pert = false, const double tol = 1e-6);

  void setSizeDistortionalParameters(int m, int n);
  void setDistortionalParameter(int i, int j, double val);
  void setSizeVolumetricParameters(int n);
  void setVolumetricParameter(int i, double val);
  
#ifndef SWIG
  HyperElasticPolynomialPotential(const HyperElasticPolynomialPotential &src);
  virtual ~HyperElasticPolynomialPotential() {}
  virtual const STensor43& getElasticTensor() const { return _Cel; } // Hook tensor
  virtual double getYoungModulus() const { return _E; }
  virtual double getPoissonRatio() const { return _nu; }
  virtual double get(const STensor3 &F) const; // elastic potential
  virtual void constitutive(const STensor3 &F, STensor3 &P, const bool stiff, STensor43 *L,
                            const bool dTangent=false, STensor63* dCalgdeps = NULL) const;

  virtual elasticPotential* clone() const
  { return new HyperElasticPolynomialPotential(*this); }

  virtual void getLocalValForNonLocalEquation(const STensor3& F, double& val, const bool stiff, STensor3* DvalDF) const {}; // do nothing
  // for anisotropric
  virtual double getPositiveValue(const STensor3 &F) const { return std::numeric_limits<double>::quiet_NaN(); }
  virtual double getNegativeValue(const STensor3 &F) const { return std::numeric_limits<double>::quiet_NaN(); }
  virtual void constitutivePositive(const STensor3 &F, STensor3 &P, const bool stiff, STensor43 *L) const {} // do nothing
  virtual void constitutiveNegative(const STensor3 &F, STensor3 &P, const bool stiff, STensor43 *L) const {} // do nothing
  virtual void getLocalPositiveValForNonLocalEquation(const STensor3 &F, double &val, const bool stiff, STensor3 *DvalDF) const {} // do nothing
#endif /* SWIG */

private:
#ifndef SWIG
  void computeB(const STensor3 &F, STensor3 &B) const;
  void computeI1(const STensor3 &B, double &I1) const;
  void computeI2(const STensor3 &B, const double &I1, double &I2) const;
  void computedJdF(const double J, const STensor3 &Finv, STensor3 &dJdF) const;
  void computedIbar1dF(const double J2over3, const double I1, const STensor3 &F, const STensor3 &Finv, STensor3 &dIbar1dF) const;
  void computedIbar2dF(const double J4over3, const double I1, const double I2, const STensor3 &F, const STensor3 &Finv, const STensor3 &B, STensor3 &dIbar2dF) const;
  void computedPsidIbar1(const double Ibar1, const double Ibar2, double &dPsidIbar1, double &d2PsidIbar1, double &d3PsidIbar1) const;
  void computedPsidIbar2(const double Ibar1, const double Ibar2, double &dPsidIbar2, double &d2PsidIbar2, double &d3PsidIbar2) const;
  void computedPsidJ(const double J, double &dPsidJ,double &d2PsidJ,double &d3PsidJ) const;
  void computed2JdFdF(const double J, const STensor3 &Finv, STensor43 &d2J) const;
  void computed3JdF3(const double J, const STensor3 &Finv, STensor63 &d3J) const;
  void computed2Ibar1dFdF(const double J2over3, const double Ibar1, const STensor3 &F, const STensor3 &Finv, const STensor3 &dIbar1dF, STensor43 &d2Ibar1) const;
  void computed3Ibar1dF3(const double J, const double Ibar1, const STensor3 &F, const STensor3 &Finv, const STensor3 &dIbar1dF, const STensor43 &d2Ibar1, STensor63 &d3Ibar1) const;
  void computed2Ibar2dFdF(const double J4over3, const double I1, const double I2, const STensor3 &F, const STensor3 &Finv, const STensor3 &B, const STensor3 &dIbar2dF, STensor43
  &d2Ibar2) const;
  void computed3Ibar2dF3(const double J, const double Ibar1, const double Ibar2, const STensor3 &F, const STensor3 &Finv, const STensor3 &B, const STensor3 &dIbar1dF, 
       const STensor43 &d2Ibar1, const STensor3 &dIbar2dF, const STensor43 &d2Ibar2, STensor63 &d3Ibar2) const;
  void computed2Psi(const double J, const double Ibar1, const double Ibar2, const STensor3 &dJdF, const STensor3 &dIbar1dF, const STensor3 &dIbar2dF, STensor3 &d2PsidFdIbar1, STensor3 &d2PsidFdIbar2, STensor3 &d2PsidFdJ) const;
#endif /* SWIG */
};



// See https://www.sciencedirect.com/science/article/abs/pii/S0022509608000434 (hyperelastic_model)
class SuperElasticPotential: public elasticPotential
{
protected:
  #ifndef SWIG
  double _K, _mu, _E, _nu;
  double _C1, _C2;
  STensor43 _Cel;
  bool _ByPerturbation;
  double _tol;
  #endif // SWIG

public:
  SuperElasticPotential(const int num, const double E, const double nu, const bool pert = false, const double tol = 1e-6);

  void setParameters_C1_C2_K(double val_C1,double val_C2,double val_K);
  
#ifndef SWIG
  SuperElasticPotential(const SuperElasticPotential &src);
  virtual ~SuperElasticPotential() {}
  virtual const STensor43& getElasticTensor() const { return _Cel; } // Hook tensor
  virtual double getYoungModulus() const { return _E; }
  virtual double getPoissonRatio() const { return _nu; }
  virtual double get(const STensor3 &F) const; // elastic potential
  virtual void constitutive(const STensor3 &F, STensor3 &P, const bool stiff, STensor43 *L,
                            const bool dTangent=false, STensor63* dCalgdeps = NULL) const;

  virtual elasticPotential* clone() const
  { return new SuperElasticPotential(*this); }

  virtual void getLocalValForNonLocalEquation(const STensor3& F, double& val, const bool stiff, STensor3* DvalDF) const {}; // do nothing
  // for anisotropric
  virtual double getPositiveValue(const STensor3 &F) const { return std::numeric_limits<double>::quiet_NaN(); }
  virtual double getNegativeValue(const STensor3 &F) const { return std::numeric_limits<double>::quiet_NaN(); }
  virtual void constitutivePositive(const STensor3 &F, STensor3 &P, const bool stiff, STensor43 *L) const {} // do nothing
  virtual void constitutiveNegative(const STensor3 &F, STensor3 &P, const bool stiff, STensor43 *L) const {} // do nothing
  virtual void getLocalPositiveValForNonLocalEquation(const STensor3 &F, double &val, const bool stiff, STensor3 *DvalDF) const {} // do nothing
#endif /* SWIG */

private:
#ifndef SWIG
  void computedFinvdF(const STensor3 &Finv, STensor43 &dFinvdF) const;
  void computedJdF(const double J, const STensor3 &Finv, STensor3 &dJdF) const;
  void computed2JdFdF(const double J, const STensor3 &Finv, STensor43 &d2J) const;
#endif /* SWIG */
};


#endif //ELASTICPOTENTIAL_H_
