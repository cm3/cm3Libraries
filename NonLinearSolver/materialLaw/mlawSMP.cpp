//
// C++ Interface: material law
//
// Description: SMP law
//
//
// Author:  <L. Homsy>, (C) 2013
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "mlawSMP.h"
#include <math.h>
#include <iostream>
#include "mlawThermalConducter.h"
#include "MInterfaceElement.h"
#include "ipSMP.h"
//#include "STensor63.h"
#include <vector>


mlawSMP::mlawSMP(const int num,const double rho,const double alpha, const double beta, const double gamma,const double t0,
			const double Kx,const double Ky,const double Kz, const double mu_groundState3,const double Im3, const double pi  ,const double Tr,
		          const double Nmu_groundState2,const double mu_groundState2, const double Im2,const double Sgl2,const double Sr2,const double Delta,const double m2,
		         const double epsilonr,const double n, const double epsilonp02,const double alphar1,const double alphagl1,const double Ggl1,const double Gr1,
		          const double Mgl1,const double Mr1,const double Mugl1,const double Mur1, const double epsilon01,const double QglOnKb1,
		          const double QrOnKb1,const double epsygl1 ,const double d1,const double m1,const double VOnKb1, const double alphap,
		 const double  Sa0,const double ha1,const double b1,const double g1,const double phia01,const double Z1,
			 const double r1,const double s1,const double Sb01,const double Hgl1,const double Lgl1,const double Hr1,const double Lr1,const double l1,
		 const double Kb,const double be1,const double c0,const double wp,const double c1)
                                   :mlawThermalConducter(num,alpha, beta, gamma, Kx, Ky,Kz) , _mu_groundState3(mu_groundState3),_Im3(Im3),_pi(pi),_Tr(Tr),_Nmu_groundState2(Nmu_groundState2),
			                 _mu_groundState2(mu_groundState2),_Im2(Im2),_Sgl2(Sgl2),_Sr2(Sr2),_Delta(Delta),_m2(m2),
			                 _epsilonr(epsilonr),_n(n), _epsilonp02( epsilonp02),_alphar1(alphar1),_alphagl1(alphagl1),_Ggl1(Ggl1),_Gr1(Gr1),
			                 _Mgl1(Mgl1),_Mr1(Mr1), _Mugl1(Mugl1),_Mur1(Mur1),_epsilon01( epsilon01), _QglOnKb1(QglOnKb1/Kb), _QrOnKb1(QrOnKb1/Kb), _epsygl1(epsygl1) ,
			                 _d1(d1),_m1(m1),_VOnKb1(VOnKb1/Kb), _alphap(alphap), _Sa0(Sa0),_ha1(ha1),_b1(b1),_g1(g1),_phia01(phia01),_Z1(Z1),
					_r1(r1),_s1(s1),_Sb01(Sb01),_Hgl1(Hgl1),_Lgl1(Lgl1),_Hr1(Hr1),_Lr1(Lr1),_l1(l1),_Kb(Kb),_be1(be1),_c0(c0),_wp(wp),_c1(c1),_t0(t0),_order(-1),  
                                        I4(1.,1.), I2(1.), _mechanism2(false)

					 
{
  _k0=_k;
  STensor3 R;		//3x3 rotation matrix

  double co1,co2,co3,si1,si2,si3;
  double s1c2, c1c2;
  double _pi(3.14159265359);
  double fpi = _pi/180.;

  co1 = cos(_alpha*fpi);
  si1 = sin(_alpha*fpi);

  co2 = cos(_beta*fpi);
  si2 = sin(_beta*fpi);

  co3 = cos(_gamma*fpi);
  si3 = sin(_gamma*fpi);

  s1c2 = si1*co2;
  c1c2 = co1*co2;

  R(0,0) = co3*co1 - s1c2*si3;
  R(0,1) = co3*si1 + c1c2*si3;
  R(0,2) = si2*si3;

  R(1,0) = -si3*co1 - s1c2*co3;
  R(1,1) = -si3*si1 + c1c2*co3;
  R(1,2) = si2*co3;

  R(2,0) = si1*si2;
  R(2,1) = -co1*si2;
  R(2,2) = co2;
 
  STensor3 alphaDilatation;
  alphaDilatation(0,0)= _alphagl1;
  alphaDilatation(1,1)= _alphagl1;
  alphaDilatation(2,2)= _alphagl1;
  for(int i=0;i<3;i++)
  {
    for(int j=0;j<3;j++)
    {
      _alphaDilatation(i,j)=0.;
      for(int m=0;m<3;m++)
      {
        for(int n=0;n<3;n++)
    	{
      	  _alphaDilatation(i,j)+=R(m,i)*R(n,j)*alphaDilatation(m,n);
        }
      }
    }
  } 
 
  double nu = Mr1;
  double mu = Gr1;
  double E = 2.*mu*(1.+nu);
  //double mu = 0.5*E/(1.+nu);
  double lambda = (E*nu)/(1.+nu)/(1.-2.*nu);
  double twicemu = mu+mu;

  STensor43 K_;
  STensorOperation::zero(K_);
  K_(0,0,0,0) = lambda + twicemu;
  K_(1,1,0,0) = lambda;
  K_(2,2,0,0) = lambda;
  K_(0,0,1,1) = lambda;
  K_(1,1,1,1) = lambda + twicemu;
  K_(2,2,1,1) = lambda;
  K_(0,0,2,2) = lambda;
  K_(1,1,2,2) = lambda;
  K_(2,2,2,2) = lambda + twicemu;

  if(lambda>=1000.*mu)
  {
    mu = lambda + mu;
  }

  K_(1,0,1,0) = mu;
  K_(2,0,2,0) = mu;
  K_(0,1,0,1) = mu;
  K_(2,1,2,1) = mu;
  K_(0,2,0,2) = mu;
  K_(1,2,1,2) = mu;

  K_(0,1,1,0) = mu;
  K_(0,2,2,0) = mu;
  K_(1,0,0,1) = mu;
  K_(1,2,2,1) = mu;
  K_(2,0,0,2) = mu;
  K_(2,1,1,2) = mu;


  for(int i=0;i<3;i++)
  {
    for(int j=0;j<3;j++)
    {
      _Stiff_alphaDilatation(i,j)=0.;
      for(int k=0;k<3;k++)
      {
        for(int l=0;l<3;l++)
        {
          _Stiff_alphaDilatation(i,j)+=K_(i,j,k,l)*_alphaDilatation(k,l);
        }
      }
    }
  }
}
double mlawSMP::soundSpeed() const
{
  double nu = _Mgl1;
  double mu = _Mugl1;
  double E = 2.*mu*(1.+nu);

  double factornu = (1.-nu)/((1.+nu)*(1.-2.*nu));
  return sqrt(E*factornu/_rho);
}
 
mlawSMP::mlawSMP(const mlawSMP &source) : mlawThermalConducter(source),_mu_groundState3(source._mu_groundState3),_Im3(source._Im3),
_pi(source._pi),   _k0(source._k0)
,_Tr(source._Tr),_Nmu_groundState2(source._Nmu_groundState2),_Im2(source._Im2),_mu_groundState2(source._mu_groundState2),
_Sgl2(source._Sgl2),_Sr2(source._Sr2),_Delta(source._Delta),_m2(source._m2),_epsilonr(source._epsilonr),_n(source._n), _epsilonp02(source. _epsilonp02),
_alphar1(source._alphar1),_alphagl1(source._alphagl1),_Ggl1(source._Ggl1),_Gr1 (source._Gr1),_Mgl1(source._Mgl1),_Mr1(source._Mr1),_Mugl1(source._Mugl1),
_Mur1(source._Mur1),_epsilon01( source._epsilon01), _QglOnKb1(source._QglOnKb1), _QrOnKb1(source._QrOnKb1), _epsygl1(source._epsygl1) ,  _d1(source._d1),
 _m1(source._m1),_VOnKb1(source._VOnKb1), _alphap( source._alphap), _Sa0( source._Sa0),_ha1( source._ha1),_b1( source._b1),_g1( source._g1),
_phia01( source._phia01),_Z1(source._Z1),_r1(source._r1),_s1(source._s1),_Sb01(source._Sb01),_Hgl1(source._Hgl1),_Lgl1(source._Lgl1),_Hr1(source._Hr1)
,_Lr1(source._Lr1),_l1(source._l1),_Kb(source._Kb),_be1(source._be1),_wp(source._wp),_c0(source._c0),_c1(source._c1),_t0(source._t0)
,_Stiff_alphaDilatation(source._Stiff_alphaDilatation),_alphaDilatation(source._alphaDilatation), _order(source._order), _mechanism2(source._mechanism2), I4(1.,1.), I2(1.)

{

}


//mlawSMP& mlawSMP::operator=(const  mlawLinearThermoMechanics &source)
mlawSMP& mlawSMP::operator=(const  materialLaw &source)
{
  mlawThermalConducter::operator=(source);
  const mlawSMP* src =dynamic_cast<const mlawSMP*>(&source);
	if (src != NULL)
{
  _mu_groundState3=src->_mu_groundState3;
  _Im3=src->_Im3;
  _pi=src->_pi;
  _k0=src->_k0;
  _Nmu_groundState2=src->_Nmu_groundState2;
  _mu_groundState2=src->_mu_groundState2;
  _Im2=src->_Im2;
  _Tr=src->_Tr;
  _Sgl2=src->_Sgl2;
  _Sr2=src->_Sr2;
  _Delta=src->_Delta;
  _m2=src->_m2;
  _epsilonr=src->_epsilonr;
  _n=src->_n;
  _epsilonp02=src->_epsilonp02;
  _alphar1=src->_alphar1;
  _alphagl1=src->_alphagl1;
  _Ggl1=src->_Ggl1;
  _Gr1 =src->_Gr1;
  _Mgl1=src->_Mgl1;
  _Mr1=src->_Mr1;
  _Mugl1=src->_Mugl1;
  _Mur1=src->_Mur1;
  _epsilon01=src->_epsilon01;
  _QglOnKb1=src->_QglOnKb1;
  _QrOnKb1=src->_QrOnKb1;
  _epsygl1=src->_epsygl1 ;
  _d1=src->_d1 ;
  _m1=src->_m1;
  _VOnKb1=src->_VOnKb1;
  _alphap=src->_alphap;
  _Sa0=src->_Sa0;
  _ha1=src->_ha1;
  _b1=src->_b1;
  _g1=src->_g1;
  _phia01=src->_phia01;
  _Z1=src->_Z1;
  _r1=src->_r1;
  _s1=src->_s1;
  _Sb01=src->_Sb01;
  _Hgl1=src->_Hgl1; 
  _Lgl1=src->_Lgl1;
  _Hr1=src->_Hr1;
  _Lr1=src->_Lr1; 
  _l1=src->_l1;
  _Kb=src->_Kb;
  _be1=src->_be1;
  _wp=src->_wp;
  _c0=src->_c0;
  _c1=src->_c1;
  _t0=src->_t0;
  _Stiff_alphaDilatation=src->_Stiff_alphaDilatation;
  _alphaDilatation=src->_alphaDilatation;
  _order          =src->_order;
  _mechanism2     =src->_mechanism2;
}
return *this;
}

double mlawSMP::getExtraDofStoredEnergyPerUnitField(double T) const
{
  //here is an approximation for constant
  double Tg=_Tr;
  double specificheat=_c0;
  if(T<Tg)
  {
    specificheat=_c0-_c1*(T-Tg); 
  }
  return specificheat;
}

void mlawSMP::createIPState(IPStateBase* &ips, bool hasBodyForce, const bool* state_,const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
  bool inter=true;
  const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
  if(iele==NULL) inter=false;
  IPVariable* ipvi = new IPSMP(getSa0(),getPhia0(),getSastar0());
  IPVariable* ipv1 = new IPSMP(getSa0(),getPhia0(),getSastar0());
  IPVariable* ipv2 = new IPSMP(getSa0(),getPhia0(),getSastar0());
  if(ips != NULL) delete ips; 
  ips = new IP3State(state_,ipvi,ipv1,ipv2);
}


void mlawSMP::constitutive(const STensor3& F0,
                           const STensor3& Fn,
                           STensor3 &P,
                           const IPSMP *q0, 
                           IPSMP *q1,
                           STensor43 &Tangent,
                           const bool stiff, 
                           STensor43* elasticTangent,
                           const bool dTangent,
                           STensor63* dCalgdeps) const
{
  

}

void mlawSMP::glassTrans(const STensor3& F0,const STensor3& Fn, const double T, double &Tg,STensor3& dTgdF)const
{
  //double dh= _timeStep;
  double dh=getTimeStep();
  static STensor3 F0T;
  STensorOperation::transposeSTensor3(F0, F0T);
  static STensor3 FnT;
  STensorOperation::transposeSTensor3(Fn, FnT);
  static STensor3 F0inv;
  STensorOperation::inverseSTensor3(F0, F0inv);
  static STensor3 F0invT;
  STensorOperation::transposeSTensor3(F0inv, F0invT);
  static STensor3 Fninv;
  STensorOperation::inverseSTensor3(Fn, Fninv);
  static STensor3 L2;
  STensorOperation::multSTensor3(Fn, F0inv, L2);
  static STensor3 LT2;
  STensorOperation::multSTensor3(F0invT, FnT, LT2);
  static STensor43 dlogaL;
  STensorOperation::zero(dlogaL);
  static STensor3 L,logL,Dsym,devDsym;
  STensorOperation::zero(L); STensorOperation::zero(logL); STensorOperation::zero(Dsym); STensorOperation::zero(devDsym);
  double normdevDsym=0.,epsi=0;
  if(dh>0.)
  {
    L  = L2;
    L += LT2;
    bool ok=STensorOperation::logSTensor3(L, _order, logL, &dlogaL);
    Dsym=logL;
    Dsym*=1./(2.*dh);
  }
  double trD;
  STensorOperation::decomposeDevTr(Dsym,devDsym, trD);
  normdevDsym=devDsym.norm2();
  epsi=normdevDsym*sqrt(2.);
  Tg=_Tr;
  STensorOperation::zero(dTgdF);
#if 0
  if(epsi>_epsilonr and _epsilonr!=0.)
  {    
    Tg=_Tr+_n*log(epsi/_epsilonr);
    if(dh>0.)
    {
      static STensor3 depsidF;
      static STensor43 dDdF;
      for(int i = 0; i< 3; i++){
        for(int x = 0; x< 3; x++){
          for(int j = 0; j< 3; j++){
	    for(int C = 0; C< 3; C++){
	      dDdF(i,x,j,C)=0.;
   	      for(int y = 0; y< 3; y++){
	        for(int z = 0; z< 3; z++){
	          dDdF(i,x,j,C)+=1./(2.*dh)*dlogaL(i,x,z,y)*(I2(z,j)*F0inv(C,y)+I2(y,j)*F0inv(C,z) ) ; 
	        }
              }
            }
          }
        }
      }
	    
      for(int j = 0; j< 3; j++){
	for(int C = 0; C< 3; C++){
          depsidF(j,C)=0.;
          for(int i = 0; i< 3; i++){
            for(int X = 0; X< 3; X++){
	      depsidF(j,C)+=sqrt(2.)*devDsym(i,X)/ normdevDsym*dDdF(i,X,j,C)  ;
	      for(int y = 0; y< 3; y++){
	        for(int z = 0; z< 3; z++){
	          depsidF(j,C)-=sqrt(2.)/3.*devDsym(i,X)/ normdevDsym*I2(y,z)*dDdF(y,z,j,C)*I2(i,X)  ;
                }
              }
            }
          }
        }
      }
      dTgdF=depsidF;
      dTgdF*=_n/epsi;
    }	     
  }
#endif
}



void mlawSMP::constitutive( const STensor3& F0,         // initial deformation gradient (input @ time n)
                            const STensor3& Fn,         // updated deformation gradient (input @ time n+1)
                                                        // updated 1st Piola-Kirchhoff stress tensor (output)
                            STensor3 &P,                        // contains the initial values on input
                            const IPSMP *q0,       // array of initial internal variable
                            IPSMP *q1,             // updated array of internal variable (in ipvcur on output),
                            STensor43 &Tangent,         // constitutive tangents (output)
                            double T0,
			    double T,
  			    const SVector3 &gradT,
                            SVector3 &fluxT,
                            STensor3 &dPdT,
                            STensor3 &dqdgradT,
                            SVector3 &dqdT,
                            STensor33 &dqdF,
			    const bool stiff,// if true compute the tangents
			    double &w,
			    double &dwdT,
			    STensor3 &dwdF
                           ) const
{
  w=0.;dwdT=0.; STensorOperation::zero(dwdF); STensorOperation::zero(dqdF); STensorOperation::zero(dqdT);   STensorOperation::zero(dqdgradT); STensorOperation::zero(dPdT); STensorOperation::zero(fluxT);
  // _k0=_k;
  double Tg=0.;;
  // const double pi=3.14159;
  static STensor3 P1,P2,P3;
  STensorOperation::zero(P1); STensorOperation::zero(P2); STensorOperation::zero(P3);
  static STensor43 Tangent1,Tangent2,Tangent3;  
  STensorOperation::zero(Tangent1); STensorOperation::zero(Tangent2); STensorOperation::zero(Tangent3);
  static STensor3 Cedis3;
  STensorOperation::zero(Cedis3);
  static STensor3 dPdT1,dPdT2,dPdT3;
  STensorOperation::zero(dPdT1); STensorOperation::zero(dPdT2); STensorOperation::zero(dPdT3);
  static STensor3 Fninv;
  STensorOperation::zero(Fninv);
  static STensor3 _Kref,dtauepsilon2dF,dtauepsilon1dF,dTgdF,dcpdF;
  STensorOperation::zero(_Kref); STensorOperation::zero(dtauepsilon2dF); STensorOperation::zero(dtauepsilon1dF); STensorOperation::zero(dTgdF); STensorOperation::zero(dcpdF);
  double specificheat=0.,depsitau1=0.,depsitau2=0.,dtauepsilon1dt=0.,dtauepsilon2dt=0.;

  //Msg::Error(" temp: %f",T);
  glassTrans(F0,Fn,T,Tg, dTgdF); 
  constitutive1(F0,Fn,P1, q0, q1, Tangent1,T0, T,dPdT1, stiff,Tg,dTgdF,depsitau1,dtauepsilon1dF,dtauepsilon1dt);
  if(_mechanism2)
    constitutive2(F0,Fn,P2, q0, q1, Tangent2, T,dPdT2, stiff,Tg, dTgdF,depsitau2,dtauepsilon2dF,dtauepsilon2dt);
  constitutive3(F0,Fn,P3, q0, q1, Tangent3, T,dPdT3, stiff) ;
 

  double dh;
  double dcpdt=0.;
  dh = getTimeStep();
  //cp is not constant
  specificheat=0.;
  if(T<Tg)
  {
   specificheat=_c0-_c1*(T-Tg); 
  }
  else
   specificheat=_c0;
 
  if(dh>0.)
  {  
    w=-specificheat*(T-T0)/dh;// the - sign become from the term of thermal source 
    w+=(depsitau1)*_wp/dh;
    w+=(depsitau2)*_wp/dh;
  }
  else
    w=0.;
 // q1->_thermalEnergy = specificheat*T; //(T-_t0);
 
  if (T<Tg)
  {
    dcpdt=-_c1;  
    dcpdF=dTgdF;
    dcpdF*=_c1;
  }
 
  else 
  {
    dcpdt=0.;
    STensorOperation::zero(dcpdF);
  }
  
  if(dh>0.)
  {  
    dwdT=-dcpdt*(T-T0)/dh-specificheat/dh;
    dwdT+=(dtauepsilon1dt)*_wp/dh;
    dwdT+=(dtauepsilon2dt)*_wp/dh;  

 
    for(int K = 0; K< 3; K++){
      for(int i = 0; i< 3; i++){
        dwdF(K,i)=-dcpdF(K,i)*(T-T0)/dh;
        dwdF(K,i)+=(dtauepsilon1dF(K,i))*_wp/dh;
        dwdF(K,i)+=(dtauepsilon2dF(K,i))*_wp/dh;    
      }
    }
  }
  else
  {
    dwdT=0.;
    STensorOperation::zero(dwdF);
  }

  P =P3;
  P+=P2;
  P+=P1;
  //P.print("p");
  //put flux in reference configuration
  STensorOperation::inverseSTensor3(Fn, Fninv);
  double J= Fn.determinant(); 
  for(int K = 0; K< 3; K++)
  {
    for(int L = 0; L< 3; L++)
    {
      for(int i = 0; i< 3; i++)
      {
        for(int j = 0; j< 3; j++)
        {
          _Kref(K,L) += Fninv(K,i)*_k(i,j)*Fninv(L,j);//*J; //put in reference configuration
        }
      }
    }
  }
  STensorOperation::multSTensor3SVector3(_Kref, gradT, fluxT);
  fluxT*=J;

  static STensor3 dkdT;
  STensorOperation::zero(dkdT);
  if(stiff)
  {

   dPdT=dPdT3;
   dPdT+=dPdT2;
   dPdT+=dPdT1;
   Tangent=Tangent3;
   Tangent+=Tangent2;
   Tangent+=Tangent1;
   STensorOperation::multSTensor3SVector3(dkdT, gradT, dqdT);
   dqdT*=J;

   dqdgradT = _Kref;
   dqdgradT*=J;

   
    for(int K = 0; K< 3; K++)
    {
      for(int m = 0; m< 3; m++)
      {
        for(int N = 0; N< 3; N++)
        {
          for(int L = 0; L< 3; L++)
          {
            dqdF(K,m,N) -= Fninv(K,m)*_Kref(N,L)*gradT(L)*J;
            dqdF(K,m,N) -= _Kref(K,N)*Fninv(L,m)*gradT(L)*J;
            dqdF(K,m,N) += _Kref(K,L)*gradT(L)*Fninv(N,m)*J;	    
          }
        }
      }
    }
  }  
  // Tangent.print("tangent=");
}
  
  
void mlawSMP::constitutive3(const STensor3& F0, const STensor3& Fn, STensor3 &P3, const IPSMP *q0, IPSMP *q1, STensor43 &Tangent3, double T,
			      STensor3 &dPdT, const bool stiff) const
{
  // rightcauchydis(Fn,Cedis);
  STensorOperation::zero(P3); STensorOperation::zero(Tangent3); STensorOperation::zero(dPdT);
  static  STensor3 FnT,Fninv,FnTinv,Cedis,invCedis,Fndis,S3,Fndisinv,c,cinv;
  STensorOperation::zero(FnT); STensorOperation::zero(Fninv); STensorOperation::zero(FnTinv); STensorOperation::zero(Cedis); STensorOperation::zero(invCedis); 
  STensorOperation::zero(Fndis); STensorOperation::zero(S3); STensorOperation::zero(Fndisinv); STensorOperation::zero(c); STensorOperation::zero(cinv);
  double J=0.,invJ2third=0.,invJ1third=0.,traceCedis=0.,traceC=0.;
   
  STensorOperation::transposeSTensor3(Fn, FnT);
  STensorOperation::inverseSTensor3(Fn, Fninv);
  STensorOperation::transposeSTensor3(Fninv, FnTinv);
      
  J=Fn.determinant();
  invJ2third =1./ pow(J,2./3.);

  STensorOperation::multSTensor3(FnT,Fn,c);    
  traceC=c.trace();
  STensorOperation::multSTensor3(Fninv,FnTinv,cinv);
     
  Cedis=c; 
  Cedis*=invJ2third;
  traceCedis=Cedis.trace();

  invCedis=cinv; 
  invCedis*=1./invJ2third;

  invJ1third =1./ pow(J,1./3.);	      
  Fndis=Fn;
  Fndis*=invJ1third;
  Fndisinv=Fninv;
  Fndisinv*=1./invJ1third;

       
  if (traceCedis<3.+_Im3) 
  {
    S3=	invCedis;
    S3*=-(traceCedis/3.);
    S3+=I2;
    S3*=invJ2third*_mu_groundState3*(1./(1.-((traceCedis-3.)/_Im3))); 
  }
  else 
  {
    STensorOperation::zero(S3);
  }
	 
  //q1->_SMPEnergy=-1./2.*_Im3*_mu_groundState3*log10(y);  //energ // if without calling the deformationEnergy function
      
  //  q1->_SMPEnergy=deformationEnergy(y);//right
  STensorOperation::multSTensor3(Fn,S3,P3);
  if(stiff)
  {
    if (traceCedis<3.+_Im3)
    {
      for(int i=0;i<3;i++)
      {
        for(int B=0;B<3;B++)
        {
          for(int j=0;j<3;j++)
          {
            for(int C=0;C<3;C++)
            {
              Tangent3(i,B,j,C)= I2(i,j)*S3(C,B);
	      Tangent3(i,B,j,C)+=2./(_Im3)* invJ2third*_mu_groundState3*(1./(1.-((traceCedis-3.)/_Im3)))*(1./(1.-((traceCedis-3.)/_Im3)))*(Fndis(i,B)*Fndis(j,C)
                                  -1./3.*traceCedis*Fndisinv(B,i)*Fndis(j,C));
              Tangent3(i,B,j,C)-=2./(3.)* invJ2third*_mu_groundState3*(1./(1.-((traceCedis-3.)/_Im3)))*Fndisinv(B,i)*Fndis(j,C);
	      Tangent3(i,B,j,C)+=1./(3.)* invJ2third*_mu_groundState3*(1./(1.-((traceCedis-3.)/_Im3)))*traceCedis*(I2(i,j)*invCedis(B,C)+
                      Fndisinv(C,i)*Fndisinv(B,j));
	      for(int A=0;A<3;A++)
              {	     
                Tangent3(i,B,j,C)-=2./3.* Fn(i,A)*S3(A,B)*Fninv(C,j);
              }
            }
          }
        }
      }
    }
  }
}
     



void mlawSMP:: constitutive2(const STensor3& F0, const STensor3& Fn, STensor3 &P2, const IPSMP *q0, IPSMP *q1, STensor43 &Tangent2, double T,
			           STensor3 &dPdT2, const bool stiff,const double Tg,
		             const STensor3 &dTgdF, double &depsitau2,STensor3 &dtauepsilon2dF,double &dtauepsilon2dT) const
{
  STensorOperation::zero(P2);
  STensorOperation::zero(Tangent2);
  STensorOperation::zero(dPdT2);
  STensorOperation::zero(dtauepsilon2dF);
  depsitau2=0.; dtauepsilon2dT=0.;
  

  static fullVector<double> Omega2(6);
  Omega2.setAll(0.);
  static fullMatrix<double> domega2dM2inv(6,6);
  domega2dM2inv.setAll(0.);

  static STensor3  M2,dDp2,SP2,Fe2,Cebar2,Cebarpr2,dSt2dF;
  STensorOperation::zero(M2); STensorOperation::zero(dDp2); STensorOperation::zero(SP2); STensorOperation::zero(Fe2); 
  STensorOperation::zero(Cebar2); STensorOperation::zero(Cebarpr2); STensorOperation::zero(dSt2dF);
  static STensor43 dFp2dF,dSP2dF;
  STensorOperation::zero(dFp2dF); STensorOperation::zero(dSP2dF);

  double invJ2third=0.,depsilon2=0.,Ta2=0.,Gmu_groundState2=0.,St2=0.,dSt2dT=0.;

  double dh = getTimeStep();
 
  STensor3 &Fp2= q1->getRefToFp2();
  const STensor3 &Fp0= q0->getConstRefToFp2();
  
  fSt2( T,Tg, St2,dSt2dT,dSt2dF, dTgdF);
   

  predictorCorrector2(  Omega2,  domega2dM2inv, M2, SP2, Ta2, depsilon2, dDp2, invJ2third,  F0,  Fn, P2, q0, q1, 
				  T,   Tg, Gmu_groundState2 , stiff,Fe2,Cebar2,Cebarpr2,dh,Fp2,St2) ;
				  
  depsitau2=depsilon2*Ta2;  //for cp
							 
  if(stiff)
  {
    
    computeInternalVariabledfpdf2(dFp2dF,domega2dM2inv,  Fn,  Fp0,  SP2, Gmu_groundState2,   M2,  dDp2 ,Cebar2,  invJ2third, Cebarpr2, dh, St2,
	                          Ta2,depsilon2, dtauepsilon2dF,dTgdF,T,Tg,dSt2dF, dSP2dF) ;
   
    
    tangent2(Tangent2, Fn,Fp2,SP2, Gmu_groundState2,dFp2dF, Fe2,  Cebar2,  invJ2third ,T,Tg,dSP2dF) ; 
     
        
    computedPdT2(dPdT2,domega2dM2inv, Fn,Fp2 ,Fp0,SP2,  Gmu_groundState2,M2,dDp2 , Cebar2,Cebarpr2,dh, St2, T, Tg, invJ2third,dtauepsilon2dT, Ta2, depsilon2,dSt2dT);
			
  
  }
   
}
  
  
void mlawSMP::Gmu_2(const double T,const double Tg, double &Gmu_groundState2_) const
{
  Gmu_groundState2_= _mu_groundState2*exp(-_Nmu_groundState2 *(T-Tg));
}
  

void mlawSMP:: fSt2(const double T,const double Tg, double &St2_,double &dSt2dT,STensor3 &dSt2dF, const STensor3 &dTgdF)const
{
  St2_=0.; dSt2dT=0.;
  STensorOperation::zero(dSt2dF);
  double dSt2dTg=0.;
  if(_Delta==0) Msg::Error("mlawSMP: _Delta =0");
  St2_=1./2.*(_Sgl2+_Sr2)-1./2.*(_Sgl2-_Sr2)*tanh(20./_Delta*(T-Tg));
  dSt2dT=-20./(2.*_Delta)*(_Sgl2-_Sr2)*(1.-pow(tanh(20./_Delta*(T-Tg)),2.));
  dSt2dTg=20./(2.*_Delta)*(_Sgl2-_Sr2)*(1.-pow(tanh(20./_Delta*(T-Tg)),2.));
  dSt2dF=dTgdF;
  dSt2dF*=dSt2dTg;   
}
  
void mlawSMP::Secpiola2(const STensor3 &Cebar2,const double Gmu_groundState2,const double invJ2third, STensor3 &SP2) const
{
  STensorOperation::zero(SP2);
  double traceCe=Cebar2.trace();
  static STensor3 invCe;
  STensorOperation::inverseSTensor3(Cebar2, invCe);
  if(_Im2==0) Msg::Error("mlawSMP: _Im2 =0");
  if (traceCe<3.+_Im2)
  {
    double X=1./(1.-((traceCe-3.)/_Im2));
    for(int i = 0; i < 3; i++) 
    {
      for(int j = 0; j < 3; j++) 
      {
        SP2(i,j)= invJ2third*Gmu_groundState2*X*(I2(i,j)-1./3.*traceCe*invCe(i,j));
      }    
    }
  }
}

void mlawSMP::Mandel2(const STensor3 &Ce_,const STensor3 &SP_, STensor3 &M2_) const
{
  STensorOperation::multSTensor3(Ce_,SP_,M2_);
}  

void mlawSMP::Kirco(const STensor3 &Fn_,const STensor3 &SP_, STensor3 &Kir2_) const
{
  STensorOperation::multSTensor3(Fn_,SP_,Kir2_);
}

void mlawSMP::Tau2(const STensor3 &M2_,double &Ta2_) const
{
  Ta2_=1./sqrt(2.)*M2_.norm2();
}


void mlawSMP::delta_epsilonp2( const double &Ta2_,double dh_,double St2_,double &depsilon2_) const
{
  if(_m2==0) Msg::Error("mlawSMP: _m2 =0");
  if(St2_==0) Msg::Error("mlawSMP: St2_ =0");
  
  depsilon2_=_epsilonp02*pow(Ta2_/St2_,1./_m2)*dh_;
} 
void mlawSMP::delta_DP2(const STensor3 &M2_,const double &Ta2_,const double &depsilon2_,STensor3 &dDp2_) const
{
  STensorOperation::zero(dDp2_);
  if(Ta2_>0.)
  {
    dDp2_=M2_;
    dDp2_*=depsilon2_/2./Ta2_;
  }
}

void mlawSMP::matricetovectoromega(const STensor3 &om, fullVector<double> &Omega) const
{
  //xx yy zz  yz xz  XY
  Omega.resize(6);
  Omega(0) = om(0,0);
  Omega(1) = om(1,1);
  Omega(2) = om(2,2);
  Omega(3) = om(1,2);
  Omega(4) = om(0,2);
  Omega(5) = om(0,1);
}

void mlawSMP::matricetovectordomega(const STensor43 &domega2dM24ord,fullMatrix<double> &domega2dM2)const
{
  
  // XXXX,YYYY,ZZZZ,YZYZ,XZXZ,XYXY     0000,1111,2222,1212,0202,0101;
  domega2dM2(0,0)=domega2dM24ord(0,0,0,0);
  domega2dM2(1,1)=domega2dM24ord(1,1,1,1);
  domega2dM2(2,2)=domega2dM24ord(2,2,2,2);
  domega2dM2(3,3)=domega2dM24ord(1,2,1,2)+domega2dM24ord(1,2,2,1);
  domega2dM2(4,4)=domega2dM24ord(0,2,0,2)+domega2dM24ord(0,2,2,0);
  domega2dM2(5,5)=domega2dM24ord(0,1,0,1)+domega2dM24ord(1,0,1,0);
  domega2dM2(0,1)=domega2dM24ord(0,0,1,1);
  domega2dM2(0,2)=domega2dM24ord(0,0,2,2);
  domega2dM2(0,3)=domega2dM24ord(0,0,1,2)+domega2dM24ord(0,0,2,1);
  domega2dM2(0,4)=domega2dM24ord(0,0,0,2)+domega2dM24ord(0,0,2,0);
  domega2dM2(0,5)=domega2dM24ord(0,0,0,1)+domega2dM24ord(0,0,1,0);
  domega2dM2(1,0)=domega2dM24ord(1,1,0,0);
  domega2dM2(1,2)=domega2dM24ord(1,1,2,2);
  domega2dM2(1,3)=domega2dM24ord(1,1,1,2)+domega2dM24ord(1,1,2,1);
  domega2dM2(1,4)=domega2dM24ord(1,1,0,2)+domega2dM24ord(1,1,2,0);
  domega2dM2(1,5)=domega2dM24ord(1,1,0,1)+domega2dM24ord(1,1,1,0);
  domega2dM2(2,0)=domega2dM24ord(2,2,0,0);
  domega2dM2(2,1)=domega2dM24ord(2,2,1,1);
  domega2dM2(2,3)=domega2dM24ord(2,2,1,2)+domega2dM24ord(2,2,2,1);
  domega2dM2(2,4)=domega2dM24ord(2,2,0,2)+domega2dM24ord(2,2,2,0);
  domega2dM2(2,5)=domega2dM24ord(2,2,0,1)+domega2dM24ord(2,2,1,0);
  domega2dM2(3,0)=domega2dM24ord(1,2,0,0);
  domega2dM2(3,1)=domega2dM24ord(1,2,1,1);
  domega2dM2(3,2)=domega2dM24ord(1,2,2,2);
  domega2dM2(3,4)=domega2dM24ord(1,2,0,2)+domega2dM24ord(1,2,2,0);
  domega2dM2(3,5)=domega2dM24ord(1,2,0,1)+domega2dM24ord(1,2,1,0);
  domega2dM2(4,0)=domega2dM24ord(0,2,0,0);
  domega2dM2(4,1)=domega2dM24ord(0,2,1,1);
  domega2dM2(4,2)=domega2dM24ord(0,2,2,2);
  domega2dM2(4,3)=domega2dM24ord(0,2,1,2)+domega2dM24ord(0,2,2,1);
  domega2dM2(4,5)=domega2dM24ord(0,2,0,1)+domega2dM24ord(0,2,1,0);
  domega2dM2(5,0)=domega2dM24ord(0,1,0,0);
  domega2dM2(5,1)=domega2dM24ord(0,1,1,1);
  domega2dM2(5,2)=domega2dM24ord(0,1,2,2);
  domega2dM2(5,3)=domega2dM24ord(0,1,1,2)+domega2dM24ord(0,1,2,1);
  domega2dM2(5,4)=domega2dM24ord(0,1,0,2)+domega2dM24ord(0,1,2,0);
}

void mlawSMP::vectortomatriceomega(STensor3 &o, const fullVector<double> &Omega2) const
{//xx yy zz  yz xz  XY

  o(0,0)=Omega2(0);
  o(0,1)=Omega2(5);
  o(0,2)=Omega2(4);

  o(1,0)=Omega2(5);
  o(1,1)=Omega2(1);
  o(1,2)=Omega2(3);
   
  o(2,0)=Omega2(4);
  o(2,1)=Omega2(3);
  o(2,2)=Omega2(2);
   
}

void mlawSMP::  vectortomatricedomega( STensor43 &domega2dM24ord,const fullMatrix<double> &domega2dM2)const
{   // XXXX,YYYY,ZZZZ,YZYZ,XZXZ,XYXY     0000,1111,2222,1212,0202,0101;
		 
  domega2dM24ord(0,0,0,0)=domega2dM2(0,0);
  domega2dM24ord(0,0,1,1)=domega2dM2(0,1);
  domega2dM24ord(0,0,2,2)=domega2dM2(0,2);
  domega2dM24ord(0,0,1,2)=domega2dM2(0,3)/2.;
  domega2dM24ord(0,0,2,1)=domega2dM2(0,3)/2.;
  domega2dM24ord(0,0,0,2)=domega2dM2(0,4)/2.;
  domega2dM24ord(0,0,2,0)=domega2dM2(0,4)/2.;
  domega2dM24ord(0,0,0,1)=domega2dM2(0,5)/2.;
  domega2dM24ord(0,0,1,0)=domega2dM2(0,5)/2.;
		 
  domega2dM24ord(1,1,0,0)=domega2dM2(1,0);
  domega2dM24ord(1,1,1,1)=domega2dM2(1,1);
  domega2dM24ord(1,1,2,2)=domega2dM2(1,2);
  domega2dM24ord(1,1,1,2)=domega2dM2(1,3)/2.;
  domega2dM24ord(1,1,0,2)=domega2dM2(1,4)/2.;
  domega2dM24ord(1,1,0,1)=domega2dM2(1,5)/2.;
  domega2dM24ord(1,1,2,1)=domega2dM2(1,3)/2.;
  domega2dM24ord(1,1,2,0)=domega2dM2(1,4)/2.;
  domega2dM24ord(1,1,1,0)=domega2dM2(1,5)/2.;
		 
  domega2dM24ord(2,2,0,0)=domega2dM2(2,0);
  domega2dM24ord(2,2,1,1)=domega2dM2(2,1);
  domega2dM24ord(2,2,2,2)=domega2dM2(2,2);
  domega2dM24ord(2,2,1,2)=domega2dM2(2,3)/2.;
  domega2dM24ord(2,2,0,2)=domega2dM2(2,4)/2.;
  domega2dM24ord(2,2,0,1)=domega2dM2(2,5)/2.;
  domega2dM24ord(2,2,2,1)=domega2dM2(2,3)/2.;
  domega2dM24ord(2,2,2,0)=domega2dM2(2,4)/2.;
  domega2dM24ord(2,2,1,0)=domega2dM2(2,5)/2.;
		 
  domega2dM24ord(1,2,0,0)=domega2dM2(3,0);
  domega2dM24ord(1,2,1,1)=domega2dM2(3,1);
  domega2dM24ord(1,2,2,2)=domega2dM2(3,2);
  domega2dM24ord(1,2,1,2)=domega2dM2(3,3)/2.;
  domega2dM24ord(1,2,0,2)=domega2dM2(3,4)/2.;
  domega2dM24ord(1,2,0,1)=domega2dM2(3,5)/2.;
  domega2dM24ord(1,2,2,0)=domega2dM2(3,4)/2.;
  domega2dM24ord(1,2,2,1)=domega2dM2(3,3)/2.;
  domega2dM24ord(1,2,1,0)=domega2dM2(3,5)/2.;

  domega2dM24ord(0,2,0,0)=domega2dM2(4,0);
  domega2dM24ord(0,2,1,1)=domega2dM2(4,1);
  domega2dM24ord(0,2,1,2)=domega2dM2(4,2);
  domega2dM24ord(0,2,1,2)=domega2dM2(4,3)/2.;
  domega2dM24ord(0,2,0,2)=domega2dM2(4,4)/2.;
  domega2dM24ord(0,2,0,1)=domega2dM2(4,5)/2.;
  domega2dM24ord(0,2,2,1)=domega2dM2(4,3)/2.;
  domega2dM24ord(0,2,2,0)=domega2dM2(4,4)/2.;
  domega2dM24ord(0,2,1,0)=domega2dM2(4,5)/2.;
		 
  domega2dM24ord(0,1,0,0)=domega2dM2(5,0);
  domega2dM24ord(0,1,1,1)=domega2dM2(5,1);
  domega2dM24ord(0,1,2,2)=domega2dM2(5,2);
  domega2dM24ord(0,1,1,2)=domega2dM2(5,3)/2.;
  domega2dM24ord(0,1,0,2)=domega2dM2(5,4)/2.;
  domega2dM24ord(0,1,0,1)=domega2dM2(5,5)/2.;
  domega2dM24ord(0,1,2,1)=domega2dM2(5,3)/2.;
  domega2dM24ord(0,1,2,0)=domega2dM2(5,4)/2.;
  domega2dM24ord(0,1,1,0)=domega2dM2(5,5)/2.;
		 
  domega2dM24ord(2,1,0,0)=domega2dM2(3,0);
  domega2dM24ord(2,1,1,1)=domega2dM2(3,1);
  domega2dM24ord(2,1,2,2)=domega2dM2(3,2);
  domega2dM24ord(2,1,1,2)=domega2dM2(3,3)/2.;
  domega2dM24ord(2,1,0,2)=domega2dM2(3,4)/2.;
  domega2dM24ord(2,1,0,1)=domega2dM2(3,5)/2.;
  domega2dM24ord(2,1,2,1)=domega2dM2(3,3)/2.;
  domega2dM24ord(2,1,2,0)=domega2dM2(3,4)/2.;
  domega2dM24ord(2,1,1,0)=domega2dM2(3,5)/2.;
		 
  domega2dM24ord(2,0,0,0)=domega2dM2(4,0);
  domega2dM24ord(2,0,1,1)=domega2dM2(4,1);
  domega2dM24ord(2,0,2,2)=domega2dM2(4,2);
  domega2dM24ord(2,0,1,2)=domega2dM2(4,3)/2.;
  domega2dM24ord(2,0,0,2)=domega2dM2(4,4)/2.;
  domega2dM24ord(2,0,0,1)=domega2dM2(4,5)/2.;
  domega2dM24ord(2,0,2,1)=domega2dM2(4,3)/2.;
  domega2dM24ord(2,0,2,0)=domega2dM2(4,4)/2.;
  domega2dM24ord(2,0,1,0)=domega2dM2(4,5)/2.;
		
  domega2dM24ord(1,0,0,0)=domega2dM2(5,0);
  domega2dM24ord(1,0,1,1)=domega2dM2(5,1);
  domega2dM24ord(1,0,2,2)=domega2dM2(5,2);
  domega2dM24ord(1,0,1,2)=domega2dM2(5,3)/2.;
  domega2dM24ord(1,0,0,2)=domega2dM2(5,4)/2.;
  domega2dM24ord(1,0,0,1)=domega2dM2(5,5)/2.;
  domega2dM24ord(1,0,2,1)=domega2dM2(5,3)/2.;
  domega2dM24ord(1,0,2,0)=domega2dM2(5,4)/2.;
  domega2dM24ord(1,0,1,0)=domega2dM2(5,5)/2.; 
}
   
   
void mlawSMP::predictorCorrector2(fullVector<double> &Omega2, fullMatrix<double> &domega2dM2inv,  STensor3 &M2, STensor3 &SP2, double &Ta2, double &depsilon2,
				STensor3 &dDp2, double &invJ2third, const STensor3& F0, const STensor3& Fn, STensor3 &P2, const IPSMP *q0, IPSMP *q1, 
				double T, const double Tg, double &Gmu_groundState2, bool stiff, STensor3 &Fe2,
				STensor3 &Cebar2, STensor3 &Cebarpr2, const double dh, STensor3 &Fp2, const double St2) const
{ 
  Omega2.setAll(0.); domega2dM2inv.setAll(0.);  
  STensorOperation::zero(M2); STensorOperation::zero(SP2); STensorOperation::zero(dDp2); STensorOperation::zero(P2); 
  STensorOperation::zero(Fe2); STensorOperation::zero(Cebar2); STensorOperation::zero(Cebarpr2); STensorOperation::zero(Fp2);
  Ta2=0.; depsilon2=0.; invJ2third=0.; Gmu_groundState2=0.;
    
  /* compute elastic predictor */
  double J=  Fn.determinant();
  static STensor3 Fp0;
  Fp0=q0->getConstRefToFp2();
  Fp2=Fp0;
  static fullMatrix<double> domega2dM2(6,6);
  domega2dM2.setAll(0.);

  //evaluate A from previous plastic strain 
  static STensor3 Fp2inv;
  STensorOperation::inverseSTensor3(Fp2,Fp2inv);

  static STensor3 Fe2pr;
  STensorOperation::multSTensor3(Fn,Fp2inv,Fe2pr);

  static STensor3 Cepr2;
  STensorOperation::multSTensor3FirstTranspose(Fe2pr,Fe2pr,Cepr2);
  double j=Fn.determinant();
  invJ2third=1./ pow(j,2./3.);
  Cebarpr2=Cepr2;
  Cebarpr2*= invJ2third;

  Fe2=Fe2pr;
  static STensor3 Ce2;
  Ce2=Cepr2;
  Cebar2=Cebarpr2;

  Gmu_2(T, Tg, Gmu_groundState2) ;
  Secpiola2(Cebar2,  Gmu_groundState2,  invJ2third, SP2);
  Mandel2(Ce2,SP2,M2);
  Tau2( M2,Ta2) ;

  //fSt2(T,Tg,St2);  
  delta_epsilonp2( Ta2,dh,St2, depsilon2);
  delta_DP2(M2,Ta2,depsilon2,dDp2) ;  
  static STensor3 ome2;
  STensorOperation::zero(ome2);
  
  int ite = 0, maxite = 1000;

  computeResidual2(ome2, Cebar2, M2, dDp2, Cebarpr2, Gmu_groundState2);

  double tolNR=1.e-6;
  double refnorm=M2.norm2();
  if(refnorm==0.) refnorm=1.;
  double f=ome2.norm2();
  static STensor43 domega2dM24ord;
  STensorOperation::zero(domega2dM24ord);
  static fullVector<double> deltaM2vec(6);
  deltaM2vec.setAll(0.);
  static STensor3 deltaM2;
  STensorOperation::zero(deltaM2);

  while(fabs(f)/refnorm > tolNR or ite <1)
  {    
    computeJacobian2 (domega2dM24ord, Cebar2,  M2, dDp2, Cebarpr2,Gmu_groundState2, St2,  dh,  invJ2third) ;
    matricetovectordomega( domega2dM24ord,domega2dM2);    
    matricetovectoromega( ome2, Omega2 );

    // if(ite>10) Msg::Info("SMP itereration %d, residual %f, norm %f,time step %f ", ite, f, refnorm  ,dh);
    
    bool solvelu = domega2dM2.luSolve(Omega2,  deltaM2vec);
    if(!solvelu)   Msg::Error("No inversion of J in plastic correction in mech 2  ");
    
    vectortomatriceomega(deltaM2,deltaM2vec);
    M2-=deltaM2;
    Tau2( M2,Ta2) ;
    delta_epsilonp2( Ta2, dh,St2, depsilon2);
    delta_DP2(M2,Ta2,depsilon2,dDp2) ;  
   
    computeResidual2(ome2, Cebar2, M2, dDp2, Cebarpr2, Gmu_groundState2);
    f = ome2.norm2();
    if(fabs(f)/refnorm < tolNR)
    {
      //recompute Jacobian for stiffness
      computeJacobian2(domega2dM24ord,  Cebar2, M2,dDp2,Cebarpr2, Gmu_groundState2, St2, dh,invJ2third);
      matricetovectordomega( domega2dM24ord,domega2dM2);
      bool solve = domega2dM2.invert(domega2dM2inv);
    }
    ite++;    
    if(ite > maxite)
    {
      Msg::Error("SMP itereration %d, residual %f, norm %f,time step %f ", ite, f, refnorm  ,dh);
      break;
    }
  }
  // final state
  static STensor3 expdDp2;
  STensorOperation::zero(expdDp2);
  static STensor43 dexpdDp2;
  STensorOperation::zero(dexpdDp2);
  STensorOperation::expSTensor3(dDp2,_order,expdDp2,&dexpdDp2);
  STensorOperation::multSTensor3(expdDp2,Fp0,Fp2);
  static STensor3 Fp2invT;
  STensorOperation::inverseSTensor3(Fp2,Fp2invT);
  STensorOperation::multSTensor3(Fn,Fp2inv,Fe2);
  STensorOperation::multSTensor3FirstTranspose(Fe2,Fe2,Ce2);
  Cebar2=Ce2 ;
  Cebar2*=invJ2third;
  Secpiola2(Cebar2,  Gmu_groundState2,  invJ2third, SP2);
  Mandel2(Ce2,SP2,M2);
    
  for( int I=0; I<3; I++){
    for( int A=0; A<3; A++){
      P2(I,A)= 0.;
      for( int W=0; W<3; W++){
        for( int D=0; D<3; D++){ 
          for( int B=0; B<3; B++){
            P2(I,A)+=Fn(I,W)* Fp2inv(W,D)*SP2(D,B)*Fp2inv(A,B);
	  }
        }
      }
    }
  }
  /*  double traCebar2=Cebar2.trace();
  double XX=1./(1.-((traCebar2-3.)/_Im2));
  q1->_SMPEnergy+=-1./2.*_Im2*_mu_groundState2*log10(XX);*/  //if without calling the deformationEnergy function
  //    q1->_SMPEnergy+=deformationEnergy(Cebar2);  //right
}

  
void mlawSMP::computeResidual2( STensor3 &ome2,const STensor3 &Cebar2,const STensor3 &M2,
                                const STensor3 &dDp2,const STensor3 &Cepr2,const double Gmu_groundState2) const
{
  STensorOperation::zero(ome2);
  double trCebar2=Cebar2.trace();
  static STensor3 expdDp2; 
  STensorOperation::zero(expdDp2);
  static STensor43 dexpdDp2;
  STensorOperation::zero(dexpdDp2);
  STensorOperation::expSTensor3(dDp2,_order, expdDp2,&dexpdDp2);
  
  static STensor3 expdDp2inv;
  STensorOperation::inverseSTensor3(expdDp2, expdDp2inv);
  if (trCebar2<3.+_Im2)
  {
    ome2=M2;
    for( int C=0; C<3; C++){
      for( int D=0; D<3; D++){
        ome2(C,D)-=Gmu_groundState2*(1./(1.-((trCebar2-3.)/_Im2)	))*(-1./3.*trCebar2*I2(C,D));
        for( int R=0; R<3; R++){
          for( int Q=0; Q<3; Q++){
            ome2(C,D)-=Gmu_groundState2*(1./(1.-((trCebar2-3.)/_Im2)))*(expdDp2inv(R,C)*Cepr2(R,Q)*expdDp2inv(Q,D));
          }   
        }
      }
    }
  }
  else
  {  
    // ???????? check ome2*=0.;
  }
}   
void mlawSMP::computeJacobian2(STensor43 &domega2dM24ord, const STensor3 &Cebar2,const STensor3 &M2,
				    const STensor3 &dDp2,const STensor3 &Cebarpr2,const double Gmu_groundState2,const double St2,
				    const double dh,const double invJ2third) const
{
  STensorOperation::zero(domega2dM24ord);

  double trCebar2=Cebar2.trace();

  static STensor43 dexpdDp2;
  STensorOperation::zero(dexpdDp2);
  static STensor3 dtrce2dM2, expdDp2;
  STensorOperation::zero(dtrce2dM2);   STensorOperation::zero(expdDp2);
  STensorOperation::expSTensor3(dDp2,_order,expdDp2, &dexpdDp2);

  double W=_epsilonp02*dh/(pow(St2,1./_m2)*pow(sqrt(2.),_m2+1./_m2));

  static STensor3 expdDp2inv;
  STensorOperation::inverseSTensor3(expdDp2,expdDp2inv);

  double M2norm=M2.norm2();
  for( int C=0; C<3; C++){
    for( int D=0; D<3; D++){
      dtrce2dM2(C,D)=0.;
      for( int Z=0; Z<3; Z++){
        for( int A=0; A<3; A++){
          for( int B=0; B<3; B++){
            for( int K=0; K<3; K++){
              for( int L=0; L<3; L++){
                for( int I=0; I<3; I++){
                  for( int J=0; J<3;J++){	
                    dtrce2dM2(C,D)-=W*expdDp2inv(B,Z)*expdDp2inv(I,A)*dexpdDp2(A,B,K,L)*((1.-_m2)/_m2*pow(M2norm,(1.-3.*_m2)/(_m2))*M2(C,D)*M2(K,L)+
                                    pow(M2norm,(1.-_m2)/(_m2))*I4(K,L,C,D))*Cebarpr2(I,J)*expdDp2inv(J,Z);
                    dtrce2dM2(C,D)-=W*expdDp2inv(I,Z)*Cebarpr2(I,J)*expdDp2inv(J,A)*expdDp2inv(B,Z)*dexpdDp2(A,B,K,L)*((1.-_m2)/_m2*pow(M2norm,(1.-3.*_m2)/(_m2))
                                    *M2(C,D)*M2(K,L)+pow(M2norm,(1.-_m2)/(_m2))*I4(K,L,C,D));
                  }
                }
              }
            }
          }
        } 
      }
    }
  }
  if (trCebar2<3.+_Im2)
  {
    domega2dM24ord=I4;
    for( int U=0; U<3; U++){
      for( int S=0; S<3; S++){	   
        for( int C=0; C<3; C++){
          for( int D=0; D<3; D++){
            domega2dM24ord(U,S,C,D)-=Gmu_groundState2/_Im2*(1./(1.-((trCebar2-3)/_Im2)))*(1./(1.-((trCebar2-3.)/_Im2)))*dtrce2dM2(C,D)*(-1./3.*trCebar2*I2(U,S));
            domega2dM24ord(U,S,C,D)-=Gmu_groundState2*(1./(1.-((trCebar2-3.)/_Im2)))*(-1./3.*dtrce2dM2(C,D)*I2(U,S));
            for( int R=0; R<3; R++){
              for( int Q=0; Q<3; Q++){
                domega2dM24ord(U,S,C,D)-=Gmu_groundState2/_Im2*(1./(1.-((trCebar2-3)/_Im2)))*(1./(1.-((trCebar2-3.)/_Im2)))*
                                         dtrce2dM2(C,D)*(expdDp2inv(R,U)*Cebarpr2(R,Q)*expdDp2inv(Q,S));
                for( int A=0; A<3; A++){
                  for( int B=0; B<3; B++){
                    for( int K=0; K<3; K++){
                      for( int L=0; L<3; L++){	
                        domega2dM24ord(U,S,C,D)-=Gmu_groundState2*(1./(1.-((trCebar2-3.)/_Im2)))*(-W*expdDp2inv(B,U)*
                                                 expdDp2inv(R,A)*dexpdDp2(A,B,K,L)*((1.-_m2)/_m2*pow(M2norm,(1.-3.*_m2)/(_m2))*M2(C,D)*M2(K,L)+pow(M2norm,(1.-_m2)/(_m2))
                                                 *I4(K,L,C,D)) *Cebarpr2(R,Q)*expdDp2inv(Q,S));
                        domega2dM24ord(U,S,C,D)-=Gmu_groundState2*(1./(1.-((trCebar2-3.)/_Im2)))*(-W*expdDp2inv(R,U)*Cebarpr2(R,Q)*expdDp2inv(Q,A)*
                                                 expdDp2inv(B,S)*dexpdDp2(A,B,K,L)*((1.-_m2)/_m2*pow(M2norm,(1.-3.*_m2)/(_m2))*M2(C,D)*M2(K,L)+pow(M2norm,(1.-_m2)/(_m2))*I4(K,L,C,D)));
		      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }   
  }
}


void mlawSMP::computeInternalVariabledfpdf2(STensor43 &dFp2dF, const fullMatrix<double> &domega2dM2inv, const STensor3 &Fn, const STensor3 &Fppr2,
				const STensor3  &SP2, const double Gmu_groundState2, const STensor3 &M2,const STensor3 &dDp2, const STensor3 &Cebar2, 
				const double invJ2third, const STensor3 &Cebarpr2, const double dh, const double St2, const double Ta2, const double depsilon2, 
                                STensor3 &dtauepsilon2dF, const STensor3 &dTgdF,const double T, const double Tg, const STensor3 &dSt2dF, STensor43 &dSP2dF)  const
{
  STensorOperation::zero(dFp2dF); STensorOperation::zero(dtauepsilon2dF); STensorOperation::zero(dSP2dF); 
  
  static STensor3  dTau2dF, dtraCebardF;
  STensorOperation::zero(dTau2dF); STensorOperation::zero(dtraCebardF);
  static STensor43 domega2dM24ordinv2, dM2dF;
  vectortomatricedomega(domega2dM24ordinv2, domega2dM2inv);
  STensorOperation::zero(dM2dF);

  double tracCebar2=Cebar2.trace();
  static STensor3 Fppr2inv;
  STensorOperation::inverseSTensor3(Fppr2,Fppr2inv);

  static STensor3 invCebar2;
  STensorOperation::inverseSTensor3(Cebar2, invCebar2);

  static STensor43 dM2dcbar2, dexpdDp2, domegadF, dCebardF;
  STensorOperation::zero(dM2dcbar2); STensorOperation::zero(dexpdDp2); STensorOperation::zero(domegadF); STensorOperation::zero(dCebardF);

  static STensor3 expdDp2, expdDp2inv;
  STensorOperation::expSTensor3(dDp2,_order,expdDp2, &dexpdDp2);
  STensorOperation::inverseSTensor3(expdDp2,expdDp2inv);

  double dSt2dtg=0.;
  static STensor3 Z;
  STensorOperation::zero(Z);
  double normM2=M2.norm2();
  if(normM2!=0.)
  {
    Z=M2;
    Z*=_epsilonp02*dh*pow(normM2/sqrt(2.),1./_m2)/(sqrt(2.)*normM2);
  }
  for( int D=0; D<3; D++){
    for( int B=0; B<3; B++){
      for( int j=0; j<3; j++){
        for( int C=0; C<3; C++){
          dCebardF(D,B,j,C)=0.;
          for( int N=0; N<3; N++){
            for( int O=0; O<3; O++){
              for( int R=0; R<3; R++){
                for( int K=0; K<3; K++){
                  for( int L=0; L<3; L++){
                    for( int Q=0; Q<3; Q++){
                      dCebardF(D,B,j,C)+=1./_m2*expdDp2inv(O,D)*expdDp2inv(R,N)* dexpdDp2(N,O,K,L)*Z(K,L)*pow(St2,(-1.-_m2)/_m2)*dSt2dF(j,C)*Cebarpr2(R,Q)*expdDp2inv(Q,B);
                      dCebardF(D,B,j,C)+=1./_m2*expdDp2inv(R,D)*Cebarpr2(R,Q)*expdDp2inv(Q,N)*expdDp2inv(O,B) *dexpdDp2(N,O,K,L)*Z(K,L)*pow(St2,(-1.-_m2)/_m2)*dSt2dF(j,C);
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }
  for( int j=0; j<3; j++){
    for( int C=0; C<3; C++){
      dtraCebardF(j,C)=0.;
      for( int D=0; D<3; D++){	
        for( int B=0; B<3; B++){
          dtraCebardF(j,C)+=dCebardF(D,B,j,C)*I2(D,B);
        }
      }
    }
  }

  if (tracCebar2<3.+_Im2)
  {      
    for( int U=0; U<3; U++){
      for( int S=0; S<3; S++){
        for( int j=0; j<3; j++){
          for( int X=0; X<3; X++){
            domegadF(U,S,j,X)=-((Gmu_groundState2*1./_Im2*(1./(1.-((tracCebar2-3.)/_Im2)))*(1./(1.-((tracCebar2-3.)/_Im2)))*dtraCebardF(j,X)
                               * (-1./3.*tracCebar2*I2(U,S))));	
            domegadF(U,S,j,X)-=Gmu_groundState2*(1./(1.-((tracCebar2-3.)/_Im2)))*(-1./3.*dtraCebardF(j,X)*I2(U,S));    
            domegadF(U,S,j,X)-=_Nmu_groundState2*Gmu_groundState2*(1./(1.-((tracCebar2-3.)/_Im2)))*(-1./3.*tracCebar2*I2(U,S))*dTgdF(j,X);
            for( int A=0; A<3; A++){
              for( int B=0; B<3; B++){
                domegadF(U,S,j,X)-=(Gmu_groundState2*1./_Im2*(1./(1.-((tracCebar2-3.)/_Im2)))*(1./(1.-((tracCebar2-3.)/_Im2)))*dtraCebardF(j,X)
                                   *(expdDp2inv(A,U)*Cebarpr2(A,B)*expdDp2inv(B,S)));
                domegadF(U,S,j,X)-=_Nmu_groundState2*Gmu_groundState2*(1./(1.-((tracCebar2-3.)/_Im2)))*(expdDp2inv(A,U)
                                   *Cebarpr2(A,B)*expdDp2inv(B,S))*dTgdF(j,X);
                for( int H=0; H<3; H++){
                  for( int M=0; M<3; M++){
                    for( int K=0; K<3; K++){
                      for( int L=0; L<3; L++){
                        domegadF(U,S,j,X)-=1./_m2*Gmu_groundState2*(1./(1.-((tracCebar2-3.)/_Im2)))*(Z(K,L)*expdDp2inv(B,U)*expdDp2inv(H,A)*
                                           dexpdDp2(A,B,K,L)*pow(St2,(-1.-_m2)/_m2)*dSt2dF(j,X)* Cebarpr2(H,M)*expdDp2inv(M,S));
                        domegadF(U,S,j,X)-=1./_m2*Gmu_groundState2*(1./(1.-((tracCebar2-3.)/_Im2)))*(Z(K,L)*expdDp2inv(H,U)*Cebarpr2(H,M)*expdDp2inv(M,A)
                                           *expdDp2inv(B,S)* dexpdDp2(A,B,K,L)*pow(St2,(-1.-_m2)/_m2)*dSt2dF(j,X));
                        for( int T=0; T<3; T++){
                          for( int W=0; W<3; W++){
                            domegadF(U,S,j,X)-=Fppr2inv(A,H)*(Gmu_groundState2*1./_Im2*(1./(1.-((tracCebar2-3.)/_Im2)))*(1./(1.-((tracCebar2-3.)/_Im2)))
                                                 *(expdDp2inv(K,U)*Cebarpr2(K,L)*expdDp2inv(L,S)))*Fppr2inv(B,T)*invJ2third*I4(A,B,W,M)*(Fn(j,M)*I2(W,X)+Fn(j,W)*I2(M,X));
                            for( int V=0;V<3; V++){
                              domegadF(U,S,j,X)-=Fppr2inv(A,H)*(Gmu_groundState2*1./_Im2*(1./(1.-((tracCebar2-3.)/_Im2)))*(1./(1.-((tracCebar2-3.)/_Im2)))
                                                 *expdDp2inv(K,M)*I4(K,L,H,T)*expdDp2inv(L,M)* (-1./3.*tracCebar2*I2(U,S)))*Fppr2inv(B,T)*invJ2third*I4(A,B,W,V)*(Fn(j,V)*I2(W,X)+Fn(j,W)*I2(V,X));
                              domegadF(U,S,j,X)-=Fppr2inv(A,H)*Gmu_groundState2*(1./(1.-((tracCebar2-3.)/_Im2)))*(-1./3.*expdDp2inv(K,M)*I4(K,L,H,T)
                                                 *expdDp2inv(L,M)*I2(U,S))*Fppr2inv(B,T)*invJ2third*I4(A,B,W,V)*(Fn(j,V)*I2(W,X)+Fn(j,W)*I2(V,X));
                              for( int R=0; R<3; R++){
                                for( int Q=0; Q<3; Q++){
                                  domegadF(U,S,j,X)-=Fppr2inv(A,H)*(Gmu_groundState2*1./_Im2*(1./(1.-((tracCebar2-3.)/_Im2)))*(1./(1.-((tracCebar2-3.)/_Im2)))
                                                     *expdDp2inv(K,M)*I4(K,L,H,T)*expdDp2inv(L,M)*(expdDp2inv(R,U)*Cebarpr2(R,Q)*expdDp2inv(Q,S)))*Fppr2inv(B,T)*invJ2third*I4(A,B,W,V)
                                                     *(Fn(j,V)*I2(W,X)+Fn(j,W)*I2(V,X));
                                }
                              }
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    } 
  }
  for( int C=0; C<3; C++){
    for( int D=0; D<3; D++){
      for( int j=0; j<3; j++){
        for( int X=0; X<3; X++){
          dM2dF(C,D,j,X)=0.;
          for( int U=0; U<3; U++){
            for( int S=0; S<3; S++){
              dM2dF(C,D,j,X)-=domega2dM24ordinv2(C,D,U,S)*domegadF(U,S,j,X);
            }
          }
        }
      }
    }
  }

  static STensor43 dN2dF;
  static STensor3 ddelatepsilon2dF;
  STensorOperation::zero(dN2dF); STensorOperation::zero(ddelatepsilon2dF);
  double M2norm=M2.norm2();

  for( int j=0; j<3; j++){
    for( int X=0; X<3; X++){
      ddelatepsilon2dF(j,X)=-_epsilonp02*dh/_m2*pow(M2norm/sqrt(2.),1./_m2)*pow(St2,-1-_m2/_m2)*dSt2dF(j,X);
      for( int C=0; C<3; C++){
        for( int D=0; D<3; D++){
          ddelatepsilon2dF(j,X)+=1./_m2*(_epsilonp02*dh/(pow(sqrt(2.)*St2,1./_m2))*pow(M2norm,(1.-2.*_m2)/_m2)* dM2dF(C,D,j,X)*M2(C,D));
        }
      }
    }
  }
  
  if (M2norm!=0.)
  {
    dN2dF=dM2dF;
    dN2dF*=1./sqrt(2.)/M2norm;
    for( int O=0; O<3; O++){
      for( int P=0; P<3; P++){
        for( int j=0; j<3; j++){
          for( int X=0; X<3; X++){
            for( int C=0; C<3; C++){ 
              for( int D=0; D<3; D++){
                dN2dF(O,P,j,X)-=1./sqrt(2.)* pow(M2norm,-3.)*M2(O,P)*dM2dF(C,D,j,X)*M2(C,D);
              }
            }
          }
        }
      }
    }	  
    for( int E=0; E<3; E++){
      for( int Z=0; Z<3; Z++){  
        for( int j=0; j<3; j++){
          for( int C=0; C<3; C++){
            for( int I=0; I<3; I++){
              for( int O=0; O<3; O++){
                for( int P=0; P<3; P++){
                  dFp2dF(E,Z,j,C)+=dexpdDp2(I,E,O,P)*(M2(O,P)/(sqrt(2.)*M2norm)*ddelatepsilon2dF(j,C))*Fppr2(I,Z);
                  dFp2dF(E,Z,j,C)+=dexpdDp2(I,E,O,P)*(depsilon2)*dN2dF(O,P,j,C)*Fppr2(I,Z);
                }
              }
            }
          }
        } 
      }
    }
  }
  if (tracCebar2<3.+_Im2)
  {
    for( int D=0; D<3; D++){
      for( int B=0; B<3; B++){
        for( int j=0; j<3; j++){
          for( int C=0; C<3; C++){
            dSP2dF(D,B,j,C)+=invJ2third*_Nmu_groundState2*Gmu_groundState2*(1./(1.-((tracCebar2-3.)/_Im2)))*(I2(D,B))*dTgdF(j,C);
            dSP2dF(D,B,j,C)+=invJ2third*_Nmu_groundState2*Gmu_groundState2*(1./(1.-((tracCebar2-3.)/_Im2)))*(-1./3.*tracCebar2*invCebar2(D,B))*dTgdF(j,C);
            dSP2dF(D,B,j,C)+=1./_Im2*invJ2third*Gmu_groundState2*(1./(1.-((tracCebar2-3.)/_Im2)))*(1./(1.-((tracCebar2-3.)/_Im2)))*dtraCebardF(j,C)*(I2(D,B));
            dSP2dF(D,B,j,C)+=1./_Im2*invJ2third*Gmu_groundState2*(1./(1.-((tracCebar2-3.)/_Im2)))*(1./(1.-((tracCebar2-3.)/_Im2)))*dtraCebardF(j,C)*(-1./3.
                             *tracCebar2*invCebar2(D,B));
            for( int F=0; F<3; F++){
              for( int G=0; G<3; G++){
                dSP2dF(D,B,j,C)-=1./3.*invJ2third*Gmu_groundState2*(1./(1.-((tracCebar2-3.)/_Im2)))*(dtraCebardF(j,C)*invCebar2(D,B));
                dSP2dF(D,B,j,C)-=1./3.*invJ2third*Gmu_groundState2*(1./(1.-((tracCebar2-3.)/_Im2)))*(-tracCebar2*invCebar2(D,F)*invCebar2(G,B)*dCebardF(F,G,j,C));
              }
            }
          }
        }
      }
    }
  }
  if (M2norm!=0.)
  {				
    for( int j=0; j<3; j++){
      for( int C=0; C<3; C++){
        for( int E=0; E<3; E++){
	  for( int D=0; D<3; D++){
	    dTau2dF(j,C)+=1./sqrt(2.)*M2(E,D)*dM2dF(E,D,j,C)/M2norm;
	  }
        }
      }
    } 				
    for( int j=0; j<3; j++){
      for( int C=0; C<3; C++){
	dtauepsilon2dF(j,C)=ddelatepsilon2dF(j,C)*Ta2+depsilon2*dTau2dF(j,C);
      }
    }
  }
}


void mlawSMP::tangent2(STensor43 &Tangent2, const STensor3 &Fn, const STensor3 &Fp2,const STensor3 &SP2,const double Gmu_groundState2, 
                       const STensor43 &dFp2dF, const STensor3 &Fe2,  const STensor3 &Cebar2, const double invJ2third ,const double T,const double Tg,const STensor43 &dSP2dF) const
{  
  STensorOperation::zero(Tangent2);
  // d Fp-1 / d
  static STensor3 Fp2inv;
  STensorOperation::inverseSTensor3(Fp2,Fp2inv);
  double J1=  Fn.determinant();
  static STensor3 Fninv;
  STensorOperation::inverseSTensor3(Fn,Fninv);
  double invJ1third =1./ pow(J1,1./3.);
  static STensor43 dFe2dF,dFp2invdF;
  STensorOperation::zero(dFe2dF); STensorOperation::zero(dFp2invdF);
   // dFpinv / d 
  for( int i=0; i<3; i++)
  {
    for( int j=0; j<3; j++)
    {
      for( int k=0; k<3; k++)
      {
        for( int l=0; l<3; l++)
        {
          for( int m=0; m<3; m++)
          {
            for( int n=0; n<3; n++)
            {
              dFp2invdF(i,j,k,l)-= Fp2inv(i,m)*dFp2dF(m,n,k,l)*Fp2inv(n,j);
            }
          }
        }
      }
    }
  }
  // dFe / d 
  for( int q=0; q<3; q++){
    for( int M=0; M<3; M++){
      for( int j=0; j<3; j++){
        for( int C=0; C<3; C++)
        {
          dFe2dF(q,M,j,C) = I2(q,j)*Fp2inv(C,M);
          for( int G=0; G<3; G++){
            dFe2dF(q,M,j,C) += Fn(q,G)*dFp2invdF(G,M,j,C);
          }
        }
      }
    }
  }
             
  double tracCebar2=Cebar2.trace();
  static STensor3 invCebar2;
  STensorOperation::inverseSTensor3(Cebar2,invCebar2);
  static STensor3 Fe2bar;
  Fe2bar= Fe2;
  Fe2bar*=invJ1third;
  static STensor3 Fe2barinv;
  STensorOperation::inverseSTensor3(Fe2bar,Fe2barinv);
  	
  if (tracCebar2<3.+_Im2)
  {
    for( int i=0; i<3; i++){
      for( int A=0; A<3; A++){
        for( int j=0; j<3; j++){
          for( int C=0; C<3; C++){
            for( int B=0; B<3; B++){
              for( int D=0; D<3; D++){
                Tangent2(i,A,j,C)+=I2(i,j)*Fp2inv(C,D)*SP2(D,B)*Fp2inv(A,B);
                for( int W=0; W<3; W++){
                  Tangent2(i,A,j,C)+=Fn(i,W)*dFp2invdF(W,D,j,C)*SP2(D,B)*Fp2inv(A,B);
                  Tangent2(i,A,j,C)+=Fn(i,W)*Fp2inv(W,D)*SP2(D,B)*dFp2invdF(A,B,j,C);
                  Tangent2(i,A,j,C)+=Fn(i,W)*Fp2inv(W,D)*(dSP2dF(D,B,j,C))*Fp2inv(A,B);
                  Tangent2(i,A,j,C)-=Fn(i,W)*Fp2inv(W,D)*2./3.* SP2(D,B)*Fninv(C,j)*Fp2inv(A,B);
                  for( int q=0; q<3; q++){
                    for( int M=0; M<3; M++){
                      Tangent2(i,A,j,C)+= Fn(i,W)*Fp2inv(W,D)*(2./(_Im2* J1 )*Gmu_groundState2*(1./(1.-((tracCebar2-3.)/_Im2)))*(1./(1.-((tracCebar2-3.)/_Im2)))
                          *Fe2bar(q,M)*I2(D,B))*dFe2dF(q,M,j,C)*Fp2inv(A,B);
                      Tangent2(i,A,j,C)+= Fn(i,W)*Fp2inv(W,D)*(2./(_Im2* J1 )*Gmu_groundState2*(1./(1.-((tracCebar2-3.)/_Im2)))*(1./(1.-((tracCebar2-3.)/_Im2)))
                          *Fe2bar(q,M)*( -1./3.)*tracCebar2*invCebar2(D,B))*dFe2dF(q,M,j,C)*Fp2inv(A,B);      
                      Tangent2(i,A,j,C)-= Fn(i,W)*Fp2inv(W,D)*(1./(3.* J1) *Gmu_groundState2*(1./(1.-((tracCebar2-3.)/_Im2)))
                          *2.*Fe2bar(q,M)*invCebar2(D,B))*dFe2dF(q,M,j,C)*Fp2inv(A,B);
                      Tangent2(i,A,j,C)-= Fn(i,W)*Fp2inv(W,D)*(2./(3.*_Im2)* invJ2third *Gmu_groundState2*(1./(1.-((tracCebar2-3.)/_Im2)))*(1./(1.-((tracCebar2-3.)/_Im2)))
                                      *Fe2bar(q,M)*I2(D,B))*Fe2bar(q,M)*Fninv(C,j)*Fp2inv(A,B);
                      Tangent2(i,A,j,C)-= Fn(i,W)*Fp2inv(W,D)*(2./(3.*_Im2)* invJ2third *Gmu_groundState2*(1./(1.-((tracCebar2-3.)/_Im2)))*(1./(1.-((tracCebar2-3.)/_Im2)))
                                      *Fe2bar(q,M)*(-1./3.)*tracCebar2*invCebar2(D,B))*Fe2bar(q,M)*Fninv(C,j)*Fp2inv(A,B);
                      Tangent2(i,A,j,C)+=Fn(i,W)*Fp2inv(W,D)*1./9.* invJ2third *Gmu_groundState2*(1./(1.-((tracCebar2-3.)/_Im2)))
                                      *(2.*Fe2bar(q,M)*invCebar2(D,B))*Fe2bar(q,M)*Fninv(C,j)*Fp2inv(A,B);
                      for( int K=0; K<3; K++){
                        Tangent2(i,A,j,C)-= Fn(i,W)*Fp2inv(W,D)*(1./(3.* J1) *Gmu_groundState2*(1./(1.-((tracCebar2-3.)/_Im2)))
                             *(-tracCebar2*(Fe2barinv(D,q)*Fe2barinv(M,K)*Fe2barinv(B,K)+Fe2barinv(D,K)*Fe2barinv(B,q)*Fe2barinv(M,K))))
                             *dFe2dF(q,M,j,C)*Fp2inv(A,B);
                        Tangent2(i,A,j,C)+=Fn(i,W)*Fp2inv(W,D)*1./9.* invJ2third *Gmu_groundState2*(1./(1.-((tracCebar2-3.)/_Im2)))
                                      *(-tracCebar2*(Fe2barinv(D,q)*Fe2barinv(M,K)*Fe2barinv(B,K)+Fe2barinv(D,K)*Fe2barinv(B,q)*Fe2barinv(M,K)))*
                                      Fe2bar(q,M)*Fninv(C,j)*Fp2inv(A,B);
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }
}

void mlawSMP:: computedPdT2(STensor3 &dP2dT,const fullMatrix<double> &domega2dM2inv, const STensor3 &Fn, const STensor3 &Fp2,
		            const STensor3 &Fp0,const STensor3  &SP2, const double Gmu_groundState2, const STensor3 &M2,const STensor3 &dDp2,  const STensor3 &Cebar2,
			    const STensor3 &Ceprbar2,const double dh,const double St2,const double T,const double Tg,const double invJ2third,
                            double &dtauepsilon2dT,const double Ta2,const double depsilon2 ,const double dSt2dT) const
{
  STensorOperation::zero(dP2dT); 
  dtauepsilon2dT=0.;
  static STensor3 Fp2inv;
  STensorOperation::inverseSTensor3(Fp2, Fp2inv);
  static STensor3 expdDP2, dFp2invdT, dFp2dT, dSP2dT, domegadT;
  STensorOperation::zero(expdDP2); STensorOperation::zero(dFp2invdT); STensorOperation::zero(dFp2dT); STensorOperation::zero(dSP2dT); STensorOperation::zero(domegadT);
  double  dTau2dT=0.; 
  double trCebar2=Cebar2.trace();
  static STensor3 invCebar2;
  STensorOperation::inverseSTensor3(Cebar2, invCebar2);
  double dtrCebar2dT=0.;
  static STensor43 dexpdDP2;
  STensorOperation::expSTensor3(dDp2,_order,expdDP2, &dexpdDP2);
  static STensor3 expdDP2inv;
  STensorOperation::inverseSTensor3(expdDP2, expdDP2inv);
  double normM2(M2.norm2());
  static STensor43 domega2dM24ordinv2;
  vectortomatricedomega(domega2dM24ordinv2, domega2dM2inv);
  static STensor3 W;
  STensorOperation::zero(W); 
  		
  if(normM2!=0.)
  {
    W=M2;
    W*=_epsilonp02*dh*pow(normM2/sqrt(2.),1./_m2)/(sqrt(2.)*normM2);
  }		    
  for( int K=0; K<3; K++){
    for( int L=0; L<3; L++){
      for( int Z=0; Z<3; Z++){
        for( int B=0; B<3; B++){
          for( int A=0; A<3; A++){
            for( int I=0; I<3; I++){
              for( int J=0; J<3; J++){
	        dtrCebar2dT+=1./_m2*W(K,L)*expdDP2inv(B,Z)*expdDP2inv(I,A)*dexpdDP2(A,B,K,L)*pow(St2,(-1.-_m2)/_m2)*dSt2dT*Ceprbar2(I,J)*expdDP2inv(Z,J);
                dtrCebar2dT+=1./_m2*W(K,L)*expdDP2inv(I,Z)* Ceprbar2(I,J)*expdDP2inv(J,A)*expdDP2inv(B,Z)* dexpdDP2(A,B,K,L)*
                             pow(St2,(-1.-_m2)/_m2) *dSt2dT;
	      }
            }
          }
        }
      }
    }
  }				 
				 
  if (trCebar2<3.+_Im2)
  {   
    for( int U=0; U<3; U++){
      for( int S=0; S<3; S++){
        domegadT(U,S)=+_Nmu_groundState2*Gmu_groundState2*(1./(1.-((trCebar2-3.)/_Im2)))*(-1./3.*trCebar2*I2(U,S));
        domegadT(U,S)-=Gmu_groundState2/_Im2*(1./(1.-((trCebar2-3.)/_Im2)))*(1./(1.-((trCebar2-3.)/_Im2)))*dtrCebar2dT*(-1./3.*trCebar2*I2(U,S));
        domegadT(U,S)-=Gmu_groundState2*(1./(1.-((trCebar2-3.)/_Im2)))* (-1./3.*dtrCebar2dT*I2(U,S));
        for( int R=0; R<3; R++){
          for( int Q=0; Q<3; Q++){
            domegadT(U,S)+=_Nmu_groundState2*Gmu_groundState2*(1./(1.-((trCebar2-3.)/_Im2)))*(expdDP2inv(R,U)*Ceprbar2(R,Q)*expdDP2inv(Q,S));
            domegadT(U,S)-=Gmu_groundState2/_Im2*(1./(1.-((trCebar2-3.)/_Im2)))*(1./(1.-((trCebar2-3.)/_Im2)))*dtrCebar2dT*(expdDP2inv(R,U)*
                           Ceprbar2(R,Q)*expdDP2inv(Q,S));
            for( int A=0; A<3; A++){
              for( int B=0; B<3; B++){
                for( int K=0; K<3; K++){
                  for( int L=0; L<3; L++){
                    domegadT(U,S)-=1./_m2*Gmu_groundState2*(1./(1.-((trCebar2-3.)/_Im2)))*(W(K,L)*expdDP2inv(B,U)*expdDP2inv(R,A)*
                                   dexpdDP2(A,B,K,L)*pow(St2,(-1.-_m2)/_m2)*dSt2dT* Ceprbar2(R,Q)*expdDP2inv(Q,S));
                    domegadT(U,S)-=1./_m2*Gmu_groundState2*(1./(1.-((trCebar2-3.)/_Im2)))*(W(K,L))*expdDP2inv(R,U)*Ceprbar2(R,Q)*expdDP2inv(Q,A)
                                   *expdDP2inv(B,S)* dexpdDP2(A,B,K,L)*pow(St2,(-1.-_m2)/_m2)*dSt2dT;
                  }
                }
              }
            }
          }
        }
      }
    }
  }
			         			
  static STensor3 dM2dT;
  STensorOperation::zero(dM2dT);	 
  for( int C=0; C<3; C++){
    for( int D=0; D<3; D++){
      for( int U=0; U<3; U++){
        for( int S=0; S<3; S++){
          dM2dT(C,D)-=domega2dM24ordinv2(C,D,U,S)*domegadT(U,S);
        }
      }
    }
  }
  double depsilon2dT=-1./_m2*_epsilonp02*dh*pow(normM2/sqrt(2.),1./_m2)*pow(St2,(-1.-_m2)/_m2)*dSt2dT;		  
  depsilon2dT+=1./_m2*(_epsilonp02*dh/(pow(St2*sqrt(2.),1./_m2)))*pow(normM2,(1.-2.*_m2)/_m2)*dot(dM2dT,M2);
  if(normM2!=0.)
  {
    for( int E=0; E<3; E++){
      for( int Z=0; Z<3; Z++){
        for( int I=0; I<3; I++){
          for( int O=0; O<3; O++){
            for( int P=0; P<3; P++){
              dFp2dT(E,Z)+=dexpdDP2(E,I,O,P)*(M2(O,P)/(sqrt(2.)*normM2)*( depsilon2dT) )*Fp0(I,Z);
              dFp2dT(E,Z)+=dexpdDP2(E,I,O,P)*depsilon2*(dM2dT(O,P))*Fp0(I,Z);
              for( int C=0; C<3; C++){
                for( int D=0; D<3; D++){
                  dFp2dT(E,Z)-=dexpdDP2(E,I,O,P)*depsilon2*(pow(normM2,-3.)*M2(O,P)*dM2dT(C,D)*M2(C,D))*Fp0(I,Z);
                }
              }
            }
          }
        }
      }
    }
  }
  for(int X=0; X<3; X++){
    for(int Y=0; Y<3; Y++){
      for( int E=0; E<3; E++){
        for( int Z=0; Z<3; Z++){
          dFp2invdT(X,Y)-= Fp2inv(X,E)*dFp2dT(E,Z)*Fp2inv(Z,Y);
        }
      }
    }
  }
  static STensor3 dCebar2SdT;
  STensorOperation::zero(dCebar2SdT);	 
  double dtrCebar2SdT=0.;
  double v=_epsilonp02*dh/pow(sqrt(2.),(1.+_m2)/_m2);
  // dtrace ce bar s dt	

  for( int D=0; D<3; D++){
    for( int B=0; B<3; B++){
      for( int N=0; N<3; N++){
        for( int O=0; O<3; O++){
          for( int R=0; R<3; R++){
            for( int K=0; K<3; K++){
              for( int L=0; L<3; L++){
                for( int Q=0; Q<3; Q++){
                  dCebar2SdT(D,B)-=expdDP2inv(O,D)*expdDP2inv(R,N)* dexpdDP2(N,O,K,L)*v*pow(normM2,(1.-_m2)/_m2)*dM2dT(K,L)*pow(St2,-1./_m2)*Ceprbar2(R,Q)*expdDP2inv(Q,B);
                  dCebar2SdT(D,B)+=1./_m2*expdDP2inv(O,D)*expdDP2inv(R,N)* dexpdDP2(N,O,K,L)*v*(pow(normM2,(1.-_m2)/_m2)*M2(K,L)*
                                    pow(St2,(-1.-_m2)/_m2)*dSt2dT)*Ceprbar2(R,Q)*expdDP2inv(Q,B);
                  dCebar2SdT(D,B)-=expdDP2inv(R,D)*Ceprbar2(R,Q)*expdDP2inv(Q,N)*expdDP2inv(O,B) *dexpdDP2(N,O,K,L)*v*
                                    (pow(normM2,(1.-_m2)/_m2)*dM2dT(K,L))*pow(St2,-1./_m2);
                  dCebar2SdT(D,B)+=1./_m2*expdDP2inv(R,D)*Ceprbar2(R,Q)*expdDP2inv(Q,N)*expdDP2inv(O,B) *dexpdDP2(N,O,K,L)*v*
                                   (pow(normM2,(1.-_m2)/_m2)*M2(K,L)*pow(St2,(-1.-_m2)/_m2)*dSt2dT);
                  for( int A=0; A<3; A++){
                    for( int P=0; P<3; P++){
                      dCebar2SdT(D,B)-=expdDP2inv(O,D)*expdDP2inv(R,N)* dexpdDP2(N,O,K,L)*v*((1.-_m2)/_m2*pow(normM2,(1.-3.*_m2)/_m2)
                                       *dM2dT(A,P)*M2(A,P)*M2(K,L))*pow(St2,-1./_m2)*Ceprbar2(R,Q)*expdDP2inv(Q,B);
                      dCebar2SdT(D,B)-=expdDP2inv(R,D)*Ceprbar2(R,Q)*expdDP2inv(Q,N)*expdDP2inv(O,B) *dexpdDP2(N,O,K,L)*v*
                                       ((1.-_m2)/_m2*pow(normM2,(1.-3.*_m2)/_m2)*dM2dT(A,P)*M2(A,P)*M2(K,L))*pow(St2,-1./_m2);
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }
  dtrCebar2SdT+=dCebar2SdT.trace();
  	     			
  if (trCebar2<3.+_Im2)
  {
    dSP2dT=I2;
    dSP2dT*=1./_Im2*invJ2third*Gmu_groundState2*(1./(1.-((trCebar2-3.)/_Im2)))*(1./(1.-((trCebar2-3.)/_Im2)))*dtrCebar2SdT -
            invJ2third*_Nmu_groundState2*Gmu_groundState2*(1./(1.-((trCebar2-3.)/_Im2)));
    for( int D=0; D<3; D++){
      for( int B=0; B<3; B++){
        dSP2dT(D,B)-=invJ2third*_Nmu_groundState2*Gmu_groundState2*(1./(1.-((trCebar2-3.)/_Im2)))*(-1./3.*trCebar2*invCebar2(D,B));
        dSP2dT(D,B)+=1./_Im2*invJ2third*Gmu_groundState2*(1./(1.-((trCebar2-3.)/_Im2)))*(1./(1.-((trCebar2-3.)/_Im2)))*dtrCebar2SdT
                    *(-1./3.*trCebar2*invCebar2(D,B));
        dSP2dT(D,B)-=1./3.*invJ2third*Gmu_groundState2*(1./(1.-((trCebar2-3.)/_Im2)))*(dtrCebar2SdT*invCebar2(D,B));
        for( int F=0; F<3; F++){
          for( int G=0; G<3; G++){
            dSP2dT(D,B)-=1./3.*invJ2third*Gmu_groundState2*(1./(1.-((trCebar2-3.)/_Im2)))*(-trCebar2*invCebar2(D,F)*invCebar2(G,B)*dCebar2SdT(F,G));
          }
        }
      }
    }
  }
  for( int i=0; i<3; i++){
    for( int A=0; A<3; A++){
      for( int W=0; W<3; W++){
        for( int D=0; D<3; D++){
          for( int B=0; B<3; B++){
            dP2dT(i,A)+=Fn(i,W)*dFp2invdT(W,D)*SP2(D,B)*Fp2inv(A,B);
            dP2dT(i,A)+=Fn(i,W)*Fp2inv(W,D)*dSP2dT(D,B)*Fp2inv(A,B);
            dP2dT(i,A)+=Fn(i,W)*Fp2inv(W,D)*SP2(D,B)*dFp2invdT(A,B);
          }
        }
      }
    }
  }
  //for cp
  if(normM2!=0.)
  {
    dTau2dT=1./sqrt(2.)/normM2*dot(M2,dM2dT);
  }
  dtauepsilon2dT=dTau2dT*depsilon2+Ta2*depsilon2dT  ;
}

void mlawSMP:: constitutive1(const STensor3& F0, const STensor3& Fn, STensor3 &P1, const IPSMP *q0, IPSMP *q1, STensor43 &Tangent1, double T0, double T,
			      STensor3 &dPdT1, const bool stiff, const double Tg,            
		           const STensor3 &dTgdF, double &depsitau1,STensor3 &dtauepsilon1dF,double &dtauepsilon1dT) const
{
  STensorOperation::zero(P1); STensorOperation::zero(Tangent1); STensorOperation::zero(dPdT1); STensorOperation::zero(dtauepsilon1dF);
  depsitau1=0.; dtauepsilon1dT=0.;

  static STensor3 dDp1,Fe1,Ce1,Cepr1,Dp1,C1,N1,Me1;
  STensorOperation::zero(dDp1); STensorOperation::zero(Fe1); STensorOperation::zero(Ce1); STensorOperation::zero(Cepr1); STensorOperation::zero(Dp1); 
  STensorOperation::zero(C1); STensorOperation::zero(N1); STensorOperation::zero(Me1);
  static STensor3 dG1dF,dMu1dF,dK1dF,dQ1dF,dHbdF, dthermalphaddF,dL1dF;
  STensorOperation::zero(dG1dF); STensorOperation::zero(dMu1dF); STensorOperation::zero(dK1dF); STensorOperation::zero(dQ1dF); 
  STensorOperation::zero(dHbdF); STensorOperation::zero( dthermalphaddF); STensorOperation::zero(dL1dF);
  double dG1dT=0.,dMu1dT=0.,dK1dT=0.,dHbdT=0.,thermalpha=0.,dthermalphadT=0.,dthermalphadTg=0.,dQ1dT=0.,L1=0.,dL1dT=0.;
  double dSa1dTg=0.,dSa1dT=0.;
  double depsilon1=0.,TauE1=0., K1=0.,Mue1=0.,G1=0.,j1=0.,Sb1=0.,Hb=0.,tau1=0.,Q1=0.;
  static STensor43 dFp1dF,dexpdDp1,dMe1dF;
  STensorOperation::zero(dFp1dF);   STensorOperation::zero(dexpdDp1);   STensorOperation::zero(dMe1dF);

  double dh=0.;
  
  STensor3 &Fp1= q1->getRefToFp1();
  const STensor3 &Fp0= q0->getConstRefToFp1();
  double &Sa1= q1->getRefToSa1();
  double &phia1=q1->getRefToPhia1(); 
  double &phiastar1=q1->getRefToPhiastar1();
  double &Sastar1=q1->getRefToSastar1();
  
  dh = getTimeStep();
  //std::cout<<"Sa1="<<Sa1<<std::endl;
 
  computeThermalAlpha( T , Tg, thermalpha,dthermalphadT,dthermalphadTg);
  computeParameter1( T , Tg, dTgdF, stiff,G1,Mue1,K1,Hb,Q1,dG1dF,dMu1dF,dK1dF,dQ1dF,dHbdF,dG1dT,dMu1dT,dK1dT,dHbdT,dthermalphadTg,dthermalphaddF,dQ1dT,L1,dL1dT,dL1dF,dh); 
  predictorCorrector1  ( j1, TauE1,depsilon1,dDp1,Sa1,Sb1,phia1,phiastar1,Sastar1,G1,K1 ,Mue1, F0,Fn, P1, q0,q1, T0,T, Tg, stiff,Fe1,Ce1,Cepr1,
			   dh, Fp1,C1,Hb,dexpdDp1,N1,tau1 ,Me1,Q1,L1/*,dFp1dF,dMe1dF*/,thermalpha,dSa1dTg,dSa1dT);   
  //  depsitau1=0.;
  depsitau1=tau1*depsilon1;
  q1->getRefToEquivalentPlasticDefo1() = q0->getConstRefToEquivalentPlasticDefo1()+depsilon1 ;
							 
  if(stiff)
  {
    computeInternalVariabledfpdf1(dFp1dF,dMe1dF, j1, Fn, Fp0, Cepr1 ,N1, dh, depsilon1,dexpdDp1, T, Tg, G1, K1,C1,Hb,dtauepsilon1dF,tau1,F0,thermalpha,Q1,
                       TauE1,dTgdF,dG1dF,dMu1dF,dK1dF,dQ1dF,dHbdF,dthermalphaddF,L1,dL1dF,Me1,dSa1dTg); 
    tangent1(Tangent1,dMe1dF,Fn, Fp1,Me1,dFp1dF, Fe1,  Ce1);
    computedPdT1(dPdT1, j1, Fn,Fp1,Fp0,Me1, dDp1 ,  Ce1,Cepr1, dh, T, Tg, depsilon1, dexpdDp1, G1, K1, Mue1, C1, Hb , Q1, N1,thermalpha,dtauepsilon1dT,
	      tau1,dG1dT,dMu1dT,dK1dT,dHbdT,dthermalphadT,L1,dQ1dT,dL1dT,Fe1,dSa1dT) ;
  }   
}
  
  
void mlawSMP::deltaepsilon1(const double TauE1,const double Q1,const double T,const double Tg,const double dh,double &depsilon1)const
{
  if  (TauE1>0.)
  {
     if(T<=Tg)
       depsilon1=_epsilon01*dh*exp(-1./_epsygl1)*exp(-Q1/(T))*pow(sinh(TauE1*_VOnKb1/(2.*T)),1./_m1);
     else 
       depsilon1=_epsilon01*dh*exp(-1./(_epsygl1+_d1*(T-Tg)))*exp(-Q1/(T))*pow(sinh(TauE1*_VOnKb1/(2.*T)),1./_m1);
  } 
  else 
     depsilon1=0.;
}
  
void mlawSMP::computeTauE(const STensor3 &Me1,const STensor3 &devlnCepr1,const STensor3 &lnCepr1,const double depsilon1,const double G1,const double K1,const double Sa1,
			    const double Sb1,const double T ,const double Tg, double &TauE1,double &Tau1,const double alalphath)const
{
  double P1;//,Tau11;
  double normdevlnCepr1=devlnCepr1.norm2();
  double trlnCepr1=lnCepr1.trace();
  static STensor3 devMe1;
  double trMe1;
  STensorOperation::decomposeDevTr(Me1, devMe1, trMe1);
  double normdevMe1=devMe1.norm2();
  if (depsilon1==0.)
    Tau1=G1/sqrt(2)*normdevlnCepr1;
  else
    Tau1=G1/sqrt(2)*normdevlnCepr1-G1*depsilon1;
    
  P1=-1./2.*K1*trlnCepr1+3.*K1*alalphath;
  P1*=_alphap;
  TauE1=Tau1-(Sa1+Sb1+P1); 
}  
 
   
void mlawSMP::computeThermalAlpha(const double T ,const double Tg, double &thermalpha,double &dthermalphadT, double &dthermalphadTg)const
{
     
     //to compare the result with AnisotropicETM!! 
   /*  for( int C=0; C<3; C++){
	 for( int D=0; D<3; D++){
	   thermalpha= _alphaDilatation(C,D)*I2(C,D);
	   }}*/
	 
     /* thermalpha= _alphax*(T-_t0);
      dthermalphadt=_alphax;
      dthermalphadtg=0.;*/
  if(_t0<=Tg )
  {
    if(T<=Tg )
    {
      thermalpha=_alphagl1*(T-_t0);
      dthermalphadT=_alphagl1;
      dthermalphadTg=0.;
    }
    else
    {  
      thermalpha=(_alphagl1*(T-_t0)+(_alphar1-_alphagl1)*(T-Tg));  
      dthermalphadT=_alphar1;
      dthermalphadTg=-_alphar1+_alphagl1;
    }         
  }
  else
  { 
    if(T<=Tg )
    {
      thermalpha=(_alphar1*(T-_t0)+(_alphagl1-_alphar1)*(T-Tg));
      dthermalphadT=_alphagl1;
      dthermalphadTg=_alphar1-_alphagl1;
    }
    else
    {  
      thermalpha=_alphar1*(T-_t0);
      dthermalphadT=_alphar1;
      dthermalphadTg=0.;
    }  
  }
     //Msg::Info("alpha DT %f",_t0);
}
    
  
void mlawSMP:: computeSaphi(const double depsilon1 ,const double Sa0,const double phia0,const double T,const double Tg,
			    const double dh,double &phiastar0,double &Sastar0,double &Sa1,double &Sastar1,double &phia1,double &phiastar1, 
			    double &dSa1depsilon, double &dSa1dTg, double &dSa1dT)const
{
	
  double _htg =1.e-9;//pow(0.001,_r1);
#if 0
  if(T<=Tg  && depsilon1>0.)
  {
    phiastar1=_Z1*(pow(1.-T/Tg,_r1)+_htg)*pow(depsilon1/(dh*_epsilonr),_s1);
  }
  else if (T>Tg  && depsilon1>0.)//check in the serever
  { 
    phiastar1=_Z1*_htg*pow(depsilon1/(dh*_epsilonr),_s1);
  }
  else
  {
    phiastar1=0.;
  }	
#endif
#if 1
  double tmp=(pow(1.+depsilon1/(dh*_epsilonr),_s1)-1.+tanh(depsilon1/(dh*_epsilonr)));
  double Dtmp=1./(dh*_epsilonr)*(_s1*pow(1.+depsilon1/(dh*_epsilonr),_s1-1.)+1./cosh(depsilon1/(dh*_epsilonr))/cosh(depsilon1/(dh*_epsilonr)));
  bool oldlaw=true;
  if(oldlaw)
  {
    tmp=pow(depsilon1/(dh*_epsilonr),_s1);
    Dtmp=_s1/(dh*_epsilonr)*pow(depsilon1/(dh*_epsilonr),_s1-1.);
  }

  if(T<=Tg  && depsilon1>0.)
  {
    phiastar1=_Z1*(pow(1.-T/Tg,_r1)+_htg)*tmp;
  }
  else if (T>Tg  && depsilon1>0.)//check in the serever
  {
    phiastar1=_Z1*_htg*tmp;
  }
  else
  {
    phiastar1=0.;
  }
#endif
  
  phia1=(phia0 +_g1*_be1* phiastar1*depsilon1+_g1*(1.-_be1)*(phiastar0-phia0)*depsilon1)/(1.+_be1*_g1*depsilon1);	
  Sastar1=_b1*(phiastar1-phia1);
  Sa1=(Sa0 +_ha1*_be1* Sastar1*depsilon1+_ha1*(1.-_be1)*(Sastar0-Sa0)*depsilon1)/(1.+_be1*_ha1*depsilon1);
	   
  dSa1depsilon=0.;dSa1dTg=0.;dSa1dT=0.;
	
  dSa1depsilon=_ha1*(1.-_be1)*(Sastar0-Sa0)/(1.+_be1*_ha1*depsilon1);
  dSa1depsilon-=_be1*_ha1*(Sa0+_ha1*(1.-_be1)*(Sastar0-Sa0)*depsilon1)/pow((1.+_be1*_ha1*depsilon1),2.);
  dSa1depsilon-=_be1*_ha1*_b1*(phia0+_g1*(1.-_be1)*(phiastar0-phia0)*depsilon1)/((1.+_be1*_ha1*depsilon1)*(1.+_be1*_g1*depsilon1));
  dSa1depsilon-=_be1*_ha1*_b1*(_g1*(1.-_be1)*(phiastar0-phia0)*depsilon1)/((1.+_be1*_ha1*depsilon1)*(1.+_be1*_g1*depsilon1));
  dSa1depsilon+=(pow(_be1,2.)*_ha1*(1.+_be1*_g1*depsilon1)+pow(_be1,2.)*_g1*(1.+_be1*_ha1*depsilon1))*_ha1*_b1*phia0*depsilon1/(pow((1.+_be1*_ha1*depsilon1),2.)*pow((1.+_be1*_g1*depsilon1),2.));
  dSa1depsilon+=(pow(_be1,2.)*_ha1*(1.+_be1*_g1*depsilon1)+pow(_be1,2.)*_g1*(1.+_be1*_ha1*depsilon1))*_ha1*_b1*(_g1*(1.-_be1)*(phiastar0-phia0)*depsilon1)*depsilon1/(pow((1.+_be1*_ha1*depsilon1),2.)*pow((1.+_be1*_g1*depsilon1),2.));
     
#if 0
  if(T<=Tg  && depsilon1>0.)
  {
    dSa1depsilon+=(_be1*_ha1*_b1*_Z1*(pow(1.-T/Tg,_r1)+_htg)*pow(depsilon1/(dh*_epsilonr),_s1))/((1.+_be1*_ha1*depsilon1)*(1.+_be1*_g1*depsilon1));
    dSa1depsilon-=(pow(_be1,2.)*_ha1*(1.+_be1*_g1*depsilon1)+pow(_be1,2.)*_g1*(1.+_be1*_ha1*depsilon1))*_ha1*_b1*_Z1*(pow(1.-T/Tg,_r1)+_htg)*pow(depsilon1/(dh*_epsilonr),_s1)*depsilon1/(pow((1.+_be1*_ha1*depsilon1),2.)*pow((1.+_be1*_g1*depsilon1),2.));
	    
    dSa1depsilon+=_s1*_be1*_ha1*_b1*_Z1/(dh*_epsilonr)*(pow(1.-T/Tg,_r1)+_htg)*pow(depsilon1/(dh*_epsilonr),_s1-1.)*depsilon1/((1.+_be1*_ha1*depsilon1)*(1.+_be1*_g1*depsilon1));
     
    dSa1dTg=(_r1*_be1*_ha1*_b1*_Z1*T/(Tg*Tg)*(pow(1.-T/Tg,_r1-1.)+_htg)*pow(depsilon1/(dh*_epsilonr),_s1)*depsilon1)/((1.+_be1*_ha1*depsilon1)*(1.+_be1*_g1*depsilon1));	
    dSa1dT=-_r1*_be1*_ha1*_b1*_Z1/Tg*(pow(1.-T/Tg,_r1-1.)+_htg)*pow(depsilon1/(dh*_epsilonr),_s1)*depsilon1/((1.+_be1*_ha1*depsilon1)*(1.+_be1*_g1*depsilon1));
  }
  else if (T>Tg  && depsilon1>0.)
  { 
    dSa1depsilon+=(_be1*_ha1*_b1*_Z1*(_htg)*pow(depsilon1/(dh*_epsilonr),_s1))/((1.+_be1*_ha1*depsilon1)*(1.+_be1*_g1*depsilon1));
    dSa1depsilon-=(pow(_be1,2.)*_ha1*(1.+_be1*_g1*depsilon1)+pow(_be1,2.)*_g1*(1.+_be1*_ha1*depsilon1))*_ha1*_b1*_Z1*(_htg)*pow(depsilon1/(dh*_epsilonr),_s1)*depsilon1/(pow((1.+_be1*_ha1*depsilon1),2.)*pow((1.+_be1*_g1*depsilon1),2.));	    
    dSa1depsilon+=_s1*_be1*_ha1*_b1*_Z1/(dh*_epsilonr)*(_htg)*pow(depsilon1/(dh*_epsilonr),_s1-1.)*depsilon1/((1.+_be1*_ha1*depsilon1)*(1.+_be1*_g1*depsilon1)); 
    dSa1dTg=0.;	
    dSa1dT =0.;
	  
  }
  else
  {
	  
  }
#endif
#if 1
  if(T<=Tg  && depsilon1>0.)
  {
    dSa1depsilon+=(_be1*_ha1*_b1*_Z1*(pow(1.-T/Tg,_r1)+_htg)*tmp)/((1.+_be1*_ha1*depsilon1)*(1.+_be1*_g1*depsilon1));
    dSa1depsilon-=(pow(_be1,2.)*_ha1*(1.+_be1*_g1*depsilon1)+pow(_be1,2.)*_g1*(1.+_be1*_ha1*depsilon1))*_ha1*_b1*_Z1*(pow(1.-T/Tg,_r1)+_htg)*tmp*depsilon1/(pow((1.+_be1*_ha1*depsilon1),2.)*pow((1.+_be1*_g1*depsilon1),2.));
	    
    dSa1depsilon+=_be1*_ha1*_b1*_Z1*(pow(1.-T/Tg,_r1)+_htg)*Dtmp*depsilon1/((1.+_be1*_ha1*depsilon1)*(1.+_be1*_g1*depsilon1));
     
    dSa1dTg=(_r1*_be1*_ha1*_b1*_Z1*T/(Tg*Tg)*(pow(1.-T/Tg,_r1-1.)+_htg)*tmp*depsilon1)/((1.+_be1*_ha1*depsilon1)*(1.+_be1*_g1*depsilon1));	
    dSa1dT=-_r1*_be1*_ha1*_b1*_Z1/Tg*(pow(1.-T/Tg,_r1-1.)+_htg)*tmp*depsilon1/((1.+_be1*_ha1*depsilon1)*(1.+_be1*_g1*depsilon1));
  }
  else if (T>Tg  && depsilon1>0.)
  { 
    dSa1depsilon+=(_be1*_ha1*_b1*_Z1*(_htg)*tmp)/((1.+_be1*_ha1*depsilon1)*(1.+_be1*_g1*depsilon1));
    dSa1depsilon-=(pow(_be1,2.)*_ha1*(1.+_be1*_g1*depsilon1)+pow(_be1,2.)*_g1*(1.+_be1*_ha1*depsilon1))*_ha1*_b1*_Z1*(_htg)*tmp*depsilon1/(pow((1.+_be1*_ha1*depsilon1),2.)*pow((1.+_be1*_g1*depsilon1),2.));	    
    dSa1depsilon+=_be1*_ha1*_b1*_Z1*(_htg)*Dtmp*depsilon1/((1.+_be1*_ha1*depsilon1)*(1.+_be1*_g1*depsilon1)); 
    dSa1dTg=0.;	
    dSa1dT =0.;
	  
  }
  else
  {
	  
  }
#endif


}
        
    
        
void mlawSMP::computeParameter1(const double T ,const double Tg,const STensor3 &dTgdF,bool stiff,double &G1,double &Mue1, double &K1,double &Hb,double &Q1,STensor3 &dG1dF,STensor3 &dMu1dF,
				   STensor3 &dK1dF,STensor3 &dQ1dF,STensor3 &dHbdF,double &dG1dT,double &dMu1dT,double &dK1dT,double &dHbdT,const double dthermalalphadTg,
				   STensor3 &dthermalalphaddF,double &dQ1dT,double &L1,double &dL1dT,STensor3 &dL1dF,const double dh)const
	   
{
  STensorOperation::zero(dG1dF); STensorOperation::zero(dMu1dF); STensorOperation::zero(dK1dF); STensorOperation::zero(dQ1dF), STensorOperation::zero(dHbdF); STensorOperation::zero(dthermalalphaddF);
  G1=0.; Mue1=0.; K1=0; Hb=0.; Q1=0.;
 
  double dG1dTg=0.,dMu1dTg=0.,dHbdTg=0.,dK1dTg=0.,dQ1dTg=0.,dL1dTg=0.;


  dthermalalphaddF=dTgdF;
  dthermalalphaddF*=dthermalalphadTg;

  Q1=0.5*(_QglOnKb1+_QrOnKb1)-0.5*(_QglOnKb1-_QrOnKb1)*tanh(1./_Delta*(T-Tg));
  Mue1=0.5*(_Mugl1+_Mur1)-0.5*(_Mugl1-_Mur1)*tanh(1./_Delta*(T-Tg));
      
  dQ1dTg =0.5/_Delta*(_QglOnKb1-_QrOnKb1)*(1.-pow(tanh(1./_Delta*(T-Tg)),2.));
  dMu1dTg=0.5/_Delta*(_Mugl1-_Mur1)*(1.-pow(tanh(1./_Delta*(T-Tg)),2.));  
  dMu1dT =-0.5/_Delta*(_Mugl1-_Mur1)*(1.-pow(tanh(1./_Delta*(T-Tg)),2.));
  dQ1dT  =-0.5/_Delta*(_QglOnKb1-_QrOnKb1)*(1.-pow(tanh(1./_Delta*(T-Tg)),2.));

  dQ1dF  = dTgdF;
  dQ1dF *=dQ1dTg;
  dMu1dF=dTgdF;
  dMu1dF*=dMu1dTg;
   
  if(T<=Tg)
  {
    // *_pi/180.
    G1=0.5*(_Ggl1+_Gr1)-0.5*(_Ggl1-_Gr1)*tanh(1./_Delta*(T-Tg))-_Mgl1*(T-Tg);	
    K1=G1*(2.*(1.+Mue1))/(3.*(1.-2.*Mue1));
    Hb=0.5*(_Hgl1+_Hr1)-0.5*(_Hgl1-_Hr1)*tanh(1./_Delta*(T-Tg))-_Lgl1*(T-Tg);	
    L1=_epsilon01*dh*exp(-1./_epsygl1)*exp(-Q1/(T));
		
    dG1dTg=0.5/_Delta*(_Ggl1-_Gr1)*(1.-pow(tanh(1./_Delta*(T-Tg)),2.))+_Mgl1;
    dHbdTg=0.5/_Delta*(_Hgl1-_Hr1)*(1.-pow(tanh(1./_Delta*(T-Tg)),2.))+_Lgl1;
    dK1dTg=dG1dTg*(2.*(1.+Mue1))/(3.*(1.-2.*Mue1))+G1*(2.*dMu1dTg*(1.-2.*Mue1)+4.*dMu1dTg*(1.+Mue1))/(3.*pow(1.-2.*Mue1,2.));
    dL1dTg=-_epsilon01*dh/T*exp(-1./_epsygl1)*exp(-Q1/(T))*(dQ1dTg);

    dG1dF=dTgdF;
    dG1dF*=dG1dTg;	
    dHbdF=dTgdF;
    dHbdF*=dHbdTg*dTgdF;
    dK1dF=dTgdF;
    dK1dF*=dK1dTg;
    dL1dF=dTgdF;
    dL1dF*=dL1dTg;
	
    dG1dT=-0.5/_Delta*(_Ggl1-_Gr1)*(1.-pow(tanh(1./_Delta*(T-Tg)),2.))-_Mgl1;	
    dK1dT=dG1dT*(2.*(1.+Mue1))/(3.*(1.-2.*Mue1))+G1*(2.*dMu1dT*(1.-2.*Mue1)+4.*dMu1dT*(1.+Mue1))/(3.*pow(1.-2.*Mue1,2.));
    dHbdT=-0.5/_Delta*(_Hgl1-_Hr1)*(1.-pow(tanh(1./_Delta*(T-Tg)),2.))-_Lgl1;
    dL1dT=_epsilon01*dh*exp(-1./_epsygl1)*exp(-Q1/(T))*(-dQ1dT*T+Q1)/(T*T);
  }		
  else
  {
    G1=0.5*(_Ggl1+_Gr1)-0.5*(_Ggl1-_Gr1)*tanh(1./_Delta*(T-Tg))-_Mr1*(T-Tg);
    K1=G1*(2.*(1.+ Mue1))/(3.*(1.-2.* Mue1));
    Hb=0.5*(_Hgl1+_Hr1)-0.5*(_Hgl1-_Hr1)*tanh(1./_Delta*(T-Tg))-_Lr1*(T-Tg);
    L1=(_epsilon01*dh*exp(-1./(_epsygl1+_d1*(T-Tg)))*exp(-Q1/(T)));	

    dG1dTg=0.5/_Delta*(_Ggl1-_Gr1)*(1.-pow(tanh(1./_Delta*(T-Tg)),2.))+_Mr1;
    dHbdTg=0.5/_Delta*(_Hgl1-_Hr1)*(1.-pow(tanh(1./_Delta*(T-Tg)),2.))+_Lr1;
    dK1dTg=dG1dTg*(2.*(1.+Mue1))/(3.*(1.-2.*Mue1))+G1*(2.*dMu1dTg*(1.-2.*Mue1)+4.*dMu1dTg*(1.+Mue1))/(3.*pow(1.-2.*Mue1,2.));
    dL1dTg=-_epsilon01*dh/T*exp(-1./(_epsygl1+_d1*(T-Tg)))*exp(-Q1/(T))*(dQ1dTg)
	   -_epsilon01*dh*exp(-1./(_epsygl1+_d1*(T-Tg)))*exp(-Q1/(T))*_d1/(pow(_epsygl1+_d1*(T-Tg),2.));
    dG1dF=dTgdF;
    dG1dF*=dG1dTg;
    dHbdF=dTgdF;
    dHbdF*=dHbdTg;
    dK1dF=dTgdF;	
    dK1dF*=dK1dTg;	
    dL1dF=dTgdF;		      
    dL1dF*=dL1dTg;		      

    dG1dT=-0.5/_Delta*(_Ggl1-_Gr1)*(1.-pow(tanh(1./_Delta*(T-Tg)),2.))-_Mr1;
    dK1dT=dG1dT*(2.*(1.+Mue1))/(3.*(1.-2.*Mue1))+G1*(2.*dMu1dT*(1.-2.*Mue1)+4.*dMu1dT*(1.+Mue1))/(3.*pow(1.-2.*Mue1,2.));
    dHbdT=-0.5/_Delta*(_Hgl1-_Hr1)*(1.-pow(tanh(1./_Delta*(T-Tg)),2.))-_Lr1;	
    dL1dT= _epsilon01*dh*exp(-1./(_epsygl1+_d1*(T-Tg)))*exp(-Q1/(T))*_d1/(pow(_epsygl1+_d1*(T-Tg),2.))
	   +_epsilon01*dh*exp(-1./(_epsygl1+_d1*(T-Tg)))*exp(-Q1/(T))*(-dQ1dT*T+Q1)/(T*T);
  }	
}    

void mlawSMP::computeM1(const STensor3& devlnCepr1,const STensor3& lnCepr1,const double depsilon1, const STensor3 &N1,const double T ,const double Tg,
		const double G1,const double Mu1, const double K1,STensor3 &Me1,const double dh,const double alalphath)const
	   
{
  double trlnCepr1=lnCepr1.trace();
  for( int A=0; A<3; A++){
    for( int B=0; B<3; B++){
      Me1(A,B)=G1*devlnCepr1(A,B) -2.*G1*N1(A,B)*depsilon1+1./2.*K1*trlnCepr1*I2(A,B)-3.*K1*alalphath*I2(A,B);
    }
  }		 
}
	  
void mlawSMP::predictorCorrector1( double &j1,  double &TauE1,double &depsilon1,STensor3 &dDp1, double &Sa1,double &Sb1, double &phia1, double &phiastar1,
				   double &Sastar1,const double G1,const double K1 ,const double Mu1,const STensor3& F0,const STensor3& Fn, STensor3 &P1, const IPSMP *q0,
				   IPSMP *q1,const double T0,const double T,const double Tg,bool stiff,STensor3 &Fe1,STensor3 &Ce1,STensor3 &Cepr1,const double dh, STensor3 &Fp1,
				   STensor3 &C1,const double Hb,STensor43 &dexpdDp1,STensor3 &N1,double &Tau1 ,STensor3 &Me1,const double Q1,const double L1
				   ,const double alalphath, double &dSa1dTg, double &dSa1dT) const
{
   /* initialize terms */
   //  double J=  Fn.determinant();
  STensorOperation::zero(dDp1); STensorOperation::zero(P1); STensorOperation::zero(Fe1); STensorOperation::zero(Ce1); STensorOperation::zero(Cepr1); 
  STensorOperation::zero(Fp1); STensorOperation::zero(C1); STensorOperation::zero(dexpdDp1); STensorOperation::zero(N1); STensorOperation::zero(Me1);
  Sa1=0.; Sb1=0.; phia1=0.; phiastar1-=0.; Sastar1=0.; Tau1=0.; dSa1dTg=0.; dSa1dT=0.;
  
  static STensor3 devM1;
  STensorOperation::zero(devM1); 
  double epsilon1=0.,trM1=0.,trC1=0.;
  double lambda=0.;
  double dSa1depsilon=0.;
  
  double Sa0=q0->getSa1();
  Sa1=Sa0;
  double phia0=q0->getPhia1();
  phia1=phia0;
  double phiastar0=q0->getPhiastar1();
  phiastar1=phiastar0;
  double Sastar0=q0->getSastar1();
  Sastar1=Sastar0;
         
  /* compute elastic predictor */
  const STensor3 &Fp0=q0->getConstRefToFp1();
  Fp1=Fp0;

  STensorOperation::multSTensor3FirstTranspose(Fn, Fn, C1);
  trC1=C1.trace();

  static STensor3 Fp1inv;
  STensorOperation::inverseSTensor3(Fp1, Fp1inv); 
 
  static STensor3 Fe1pr;
  STensorOperation::multSTensor3(Fn, Fp1inv, Fe1pr);
 
  static STensor3 lnCepr1,devlnCepr1,Bp1;
  STensorOperation::multSTensor3FirstTranspose(Fe1pr, Fe1pr, Cepr1);
  STensorOperation::multSTensor3SecondTranspose(Fp1, Fp1, Bp1);
 
  Fe1=Fe1pr;
 
  static STensor43 dlnCepr1;
  bool ok=STensorOperation::logSTensor3(Cepr1,_order,lnCepr1,&dlnCepr1);
  if(!ok)
  {
       P1(0,0) = P1(1,1) = P1(2,2) = sqrt(-1.);
       return; 
  }
  double trlnCepr1;
  STensorOperation::decomposeDevTr(lnCepr1, devlnCepr1, trlnCepr1);  
  double normdevlnCepr1;
  normdevlnCepr1=devlnCepr1.norm2();
  if (normdevlnCepr1==0.)
    N1=I2;
  else
  {   
    N1= devlnCepr1;
    N1*=1./(sqrt(2)*normdevlnCepr1);
  }
     
  
  static STensor3 expdDp1;
  STensorOperation::zero(expdDp1);
  depsilon1 =0.;
  j1=0.;
  computeM1(devlnCepr1,lnCepr1,depsilon1,N1,T ,Tg,G1, Mu1, K1,Me1,dh,alalphath);
  lambda=sqrt(trC1/3.);
  if(lambda<1.0)  lambda=1.;
  Sb1=_Sb01+Hb*pow(lambda-1.,_l1);
  	  
  computeTauE( Me1,devlnCepr1,lnCepr1,depsilon1, G1, K1, Sa1,Sb1, T ,Tg, TauE1,Tau1,alalphath);
    
  //  if(TauE1/(_Ggl1+_Gr1)>1.e-3)
  if(TauE1>0.)
  { 
    int ite = 0, maxite = 10000; 
    double ome1=0.;
	 
    deltaepsilon1(TauE1,Q1,T,Tg,dh,depsilon1); //derivatives do not exist for 0
    computeSaphi(depsilon1 ,Sa0,phia0, T, Tg,dh,phiastar0,Sastar0,Sa1,Sastar1,phia1,phiastar1,dSa1depsilon,dSa1dTg,dSa1dT);
    computeResidual1(ome1, lnCepr1,L1,Sa1,Sb1,depsilon1,T,Tg,K1,G1,Q1,dh,alalphath);
    // deltaepsilon1( TauE1 ,Q1, T, Tg,depsilon1);         
    
    double tolNR=1.e-7;
    //Msg::Info("SMP itereration %d, residual %f, j %f,depsilin %e ,temp %f,time step %f", ite, ome1/(_Ggl1+_Gr1),j1,depsilon1,T,dh);
    double  deltadeltadepsilon1 =0.;
		   
    while(fabs(ome1/(0.5*_Ggl1+0.5*_Gr1)) > tolNR or ite <1)	 
    {
      computeJacobian1(j1, depsilon1,T, Tg,G1,L1,dSa1depsilon);
      //if(ite>0)
      //  Msg::Info("SMP itereration %d, residual %f, j %f,depsilin %e ,temp %f,time step %f", ite, ome1/(_Ggl1+_Gr1),j1,depsilon1,T,dh);
      //if(ite>10)     Msg::Info("time step %f ,temperature %f", dh,T);
					
      if (j1==0.)
        deltadeltadepsilon1==0.;
      else 
        deltadeltadepsilon1=ome1/j1;
      if(depsilon1-deltadeltadepsilon1 > 0.)
        depsilon1-= deltadeltadepsilon1;
      else 
        depsilon1/=2.;
	  
      computeSaphi(depsilon1 ,Sa0,phia0, T, Tg,dh,phiastar0,Sastar0,Sa1,Sastar1,phia1,phiastar1,dSa1depsilon,dSa1dTg,dSa1dT);
      computeResidual1(ome1,  lnCepr1,L1,Sa1,Sb1,depsilon1,T,Tg,K1,G1,Q1,dh,alalphath);
 
      ite++;	       
      //if (depsilon1<1.e-10) break;
    
//     #ifdef _DEBUG
      if(ite > maxite)
      {
        Msg::Error("SMP itereration %d, residual %f, j %f,depsilin %e ,temp %f,time step %f,TG%f", ite, ome1/(_Ggl1+_Gr1),j1,depsilon1,T,dh,Tg);
        break;
      }
//     #endif // _DEBUG
    }
    computeJacobian1(j1, depsilon1,T, Tg,G1,L1,dSa1depsilon);
  }
      
	// final state
  computeSaphi(depsilon1 ,Sa0,phia0, T, Tg,dh,phiastar0,Sastar0,Sa1,Sastar1,phia1,phiastar1,dSa1depsilon,dSa1dTg,dSa1dT);
  computeM1(devlnCepr1,lnCepr1,depsilon1,N1,T ,Tg,G1, Mu1, K1,Me1,dh,alalphath);
  computeTauE(Me1,devlnCepr1,lnCepr1, depsilon1, G1,K1,Sa1, Sb1, T,Tg, TauE1,Tau1,alalphath);
  double trMe1;
  STensorOperation::decomposeDevTr(Me1, devM1, trMe1);
  //Me1.print("Me1=");
  double normdevlnMe1;
  normdevlnMe1=devM1.norm2();
  if(normdevlnMe1==0.)
    N1=I2;
  else
  {
    N1=devM1;
    N1*=1./(sqrt(2.)*normdevlnMe1);
  }
  STensorOperation::zero(dDp1); 
  if(Tau1!=0.)
  {
    dDp1=devM1;
    dDp1*=1./2.*(depsilon1)/Tau1;
  }

  STensorOperation::expSTensor3(dDp1,_order,expdDp1,&dexpdDp1);
  STensorOperation::multSTensor3( expdDp1, Fp0, Fp1);
  STensorOperation::inverseSTensor3(Fp1,Fp1inv);
  STensorOperation::multSTensor3(Fn, Fp1inv, Fe1);
  STensorOperation::multSTensor3FirstTranspose(Fe1,Fe1,Ce1); 
	 
  static STensor3 lnCe1;
  static STensor43 dlnCe1;
  ok=STensorOperation::logSTensor3(Ce1,_order,lnCe1,&dlnCe1);
  if(!ok)
  {
       P1(0,0) = P1(1,1) = P1(2,2) = sqrt(-1.);
       return; 
  }
   
  /*  if(stiff)
  {   
   //  computintvardfpdf1(dFp1dF,dMe1dF, j1, Fn, Fp0, Cepr1 ,N1, dh, depsilon1,dexpdDp1, T, Tg, G1, K1,C1,Hb,dtauepsilondF,tau1);
   //  Msg::Info("SMPd  dFp1dF %f,dMe1dF %f ,dFp1dF,dMe1dF);
   //  dFp1dF.print("dFp1dF");
  }*/
        
  for( int I=0; I<3; I++){
    for( int A=0; A<3; A++){
      P1(I,A)= 0.;
      for( int M=0; M<3; M++){
        for( int K=0; K<3; K++){ 
	  for( int L=0; L<3; L++){
            for( int S=0; S<3; S++){
              P1(I,A)+=Fe1(I,M)*dlnCe1(K,L,M,S)* Me1(K,L)*Fp1inv(A,S);
            }
          }
        }
      }
    }
  } 
  /* double trlnCepr1=lnCepr1.trace();
  double normlnCepr1=lnCepr1.norm2();
  q1->_SMPEnergy+= G1*normlnCepr1*normlnCepr1 +1./2.*K1*trlnCepr1*trlnCepr1-3.*K1*alalphath*trlnCepr1*(T-T0); */ //energy if without calling the deformationEnergy function	    
            
  //   q1->_SMPEnergy+=deformationEnergy(lnCepr1,G1,K1,alalphath,T,T0); //right
 
} 
 
 
void mlawSMP::computeResidual1( double &ome1,const STensor3 &lnCepr1,const double L1,const double Sa1,const double Sb1,const double depsilon1,const double T,
				  const double Tg,const double K1,const double G1,const double Q1,const double dh,const double alalphath) const
				
{
  double trlnCepr1,normdevlnCepr1;
  static STensor3 devlnCepr1;
  STensorOperation::decomposeDevTr(lnCepr1, devlnCepr1, trlnCepr1);
 
  normdevlnCepr1=devlnCepr1.norm2();
  ome1=2.*T/_VOnKb1*(asinh(pow(depsilon1/L1,_m1)))-G1/sqrt(2.)*normdevlnCepr1+G1*depsilon1-1./2.*_alphap*K1*trlnCepr1
	  +3.*_alphap*K1*alalphath+Sa1+Sb1; 
               
}
   		
void mlawSMP::computeJacobian1(double &j1 , const double depsilon1,const double T, const double Tg,
				      const double G1,const double L1, const double dSa1depsilon) const
{ 

  j1=G1;
  j1+=dSa1depsilon;
  j1+=2.*T*_m1/(_VOnKb1*L1)*1./sqrt(pow(depsilon1/(L1),2.*_m1)+1.)*pow(depsilon1/(L1),(_m1-1.));
  
}
  
  
void mlawSMP::computeInternalVariabledfpdf1(STensor43 &dFp1dF,STensor43 &dMe1dF,const double j1, const STensor3 &Fn, const STensor3 &Fppr1, const STensor3 &Cepr1 ,const STensor3 &N1,
				const double dh,const double depsilon1,const STensor43 &dexpdDp1,const double T, const double Tg,const double G1,const double K1,
				const STensor3 &C1,const double Hb,STensor3 &dtauepsilondF,const double Tau1,const STensor3& F0,const double alalphath,const double Q1,
				 const double TauE1,const STensor3 &dTgdF,const STensor3 &dG1dF,const STensor3 &dMu1dF,const STensor3 &dK1dF,const STensor3 &dQ1dF,
				 const STensor3 &dHbdF,const STensor3 &dthermalalphadF,const double L1,const STensor3 &dL1dF,const STensor3 &Me1, const double dSa1dTg) const
				
				
{
   STensorOperation::zero(dFp1dF); STensorOperation::zero(dMe1dF); STensorOperation::zero(dtauepsilondF);

   double  trC1=0., normdevlnCepr1=0.,trlnCepr1=0.,trMe1=0.,normM1=0.,normdevMe1=0.;
   static STensor3  devlnCepr1 ,lnCepr1, depsilon1dF, devMe1, dTaudF, Fppr1inv;
   STensorOperation::zero(devlnCepr1); STensorOperation::zero(lnCepr1); STensorOperation::zero(depsilon1dF); STensorOperation::zero(devMe1); 
   STensorOperation::zero(dTaudF); STensorOperation::zero(Fppr1inv);
   static STensor43 dlnCepr1,dN1dC;
   STensorOperation::zero(dlnCepr1); STensorOperation::zero(dN1dC);

   STensorOperation::decomposeDevTr(Me1, devMe1, trMe1);
   normdevMe1=devMe1.norm2();
   bool ok=STensorOperation::logSTensor3(Cepr1,_order,lnCepr1,&dlnCepr1);

   STensorOperation::decomposeDevTr(lnCepr1, devlnCepr1, trlnCepr1);
   normdevlnCepr1=devlnCepr1.norm2();
   STensorOperation::inverseSTensor3(Fppr1,Fppr1inv);

   trC1=C1.trace();
   normM1=Me1.norm2();
   
   if(normdevlnCepr1!=0.)
   {
     for( int O=0; O<3; O++){
       for( int P=0; P<3; P++){
         for( int W=0; W<3; W++){
           for( int V=0; V<3; V++){
             dN1dC(O,P,W,V)=0.;
             for( int H=0; H<3; H++){
               for( int X=0; X<3; X++){
                 dN1dC(O,P,W,V)+=1./sqrt(2.)*(dlnCepr1(O,P,H,X))/normdevlnCepr1*Fppr1inv(W,H)*Fppr1inv(V,X);
                 for( int Y=0; Y<3; Y++){
                   for( int Z=0; Z<3; Z++){
                     dN1dC(O,P,W,V)-=1./sqrt(2.)*1./3.*I2(Y,Z)*dlnCepr1(Y,Z,H,X)*I2(O,P)/normdevlnCepr1*Fppr1inv(W,H)*Fppr1inv(V,X);
                   }
                 }
                 for( int Q=0; Q<3; Q++){
	           for( int M=0; M<3; M++){
                     dN1dC(O,P,W,V)-=N1(Q,M)/(pow(normdevlnCepr1,2))*dlnCepr1(Q,M,H,X)*devlnCepr1(O,P)*Fppr1inv(W,H)*Fppr1inv(V,X);
                   }
                 }
               }
             }
           }
         }
       }
     }
   } 
	
   static STensor3 dSa1dF;
   dSa1dF=dTgdF;
   dSa1dF*=dSa1dTg;
	
   if( j1==0.)
   {
     STensorOperation::zero(depsilon1dF);
   }				 
   else
   { 
     for( int j=0; j<3; j++){
       for( int C=0; C<3; C++){
         depsilon1dF(j,C)=(dG1dF(j,C)/sqrt(2.)*normdevlnCepr1-dG1dF(j,C)*depsilon1+1./2.*_alphap*dK1dF(j,C)*trlnCepr1  -3.*_alphap*dK1dF(j,C)*alalphath
				 -3.*_alphap*K1*dthermalalphadF(j,C)-dSa1dF(j,C))/j1;
       }
     }
     if( depsilon1!=0.)
     {
       for( int j=0; j<3; j++){
         for( int C=0; C<3; C++){  
           depsilon1dF(j,C)+= (2.*T*_m1/_VOnKb1*1./sqrt(pow(depsilon1/L1,2.*_m1)+1.)*pow(L1,-_m1-1.)*pow(depsilon1,_m1)*dL1dF(j,C))/j1;	   
	 }
       }
     }
   
     double tmpvar=sqrt(trC1/3.)-1.;
     if(tmpvar>0.)
     { 			
       for( int j=0; j<3; j++){
         for( int C=0; C<3; C++){ 
           depsilon1dF(j,C)-=dHbdF(j,C)*pow(tmpvar,_l1)/j1;
           for( int W=0; W<3; W++){
             for( int V=0; V<3; V++){
               depsilon1dF(j,C)-=_l1/2.*Hb*pow(tmpvar,_l1-1.)*I2(W,V)/(sqrt(3.*trC1))*(Fn(j,V)*I2(W,C)+Fn(j,W)*I2(V,C))/j1;
             }
           }
         }
       }
     }		
     for( int j=0; j<3; j++){
       for( int C=0; C<3; C++){  
         for( int W=0; W<3; W++){
           for( int V=0; V<3; V++){
	     for( int Y=0; Y<3; Y++){
	       for( int Z=0; Z<3; Z++){
	         for( int H=0; H<3; H++){
	           for( int X=0; X<3; X++){
		     depsilon1dF(j,C)+=1./2.*_alphap*K1*I2(Y,Z)*dlnCepr1(Y,Z,H,X)*Fppr1inv(W,H)*Fppr1inv(V,X)*(Fn(j,V)*I2(W,C)+Fn(j,W)*I2(V,C))/j1;
                   }
                 }
	       }
	     }
           }
         }
       }
     }		

     if(normdevlnCepr1!=0. && normdevMe1!=0.)
     {
       for( int j=0; j<3; j++){
         for( int C=0; C<3; C++){ 
           for( int W=0; W<3; W++){ 
             for( int V=0; V<3; V++){
               for( int O=0; O<3; O++){
                 for( int M=0; M<3; M++){
                   for( int H=0; H<3; H++){
                     for( int X=0; X<3; X++){
                       depsilon1dF(j,C)+=G1*dlnCepr1(O,M,H,X)*N1(O,M)*Fppr1inv(W,H)*Fppr1inv(V,X)*(Fn(j,V)*I2(W,C)+Fn(j,W)*I2(V,C))/j1;		
                     }
                   }
                 }
               }
             }
           }
         }
       }			
     }
   }
   for( int K=0; K<3; K++){
     for( int L=0; L<3; L++){
       for( int j=0; j<3; j++){
         for( int C=0; C<3; C++){ 
           dMe1dF(K,L,j,C)+=dG1dF(j,C)*devlnCepr1(K,L) -2.*dG1dF(j,C)*depsilon1*N1(K,L) +1./2.*dK1dF(j,C)*trlnCepr1*I2(K,L);
		-3.*dK1dF(j,C)*alalphath*I2(K,L)-3.*K1*dthermalalphadF(j,C)*I2(K,L);
	   dMe1dF(K,L,j,C)-= 2.*G1*N1(K,L)*depsilon1dF(j,C);
	   for( int W=0; W<3; W++){
	     for( int V=0; V<3; V++){
	       dMe1dF(K,L,j,C)-= 2*G1*depsilon1*dN1dC(K,L,W,V)*(Fn(j,V)*I2(W,C)+Fn(j,W)*I2(V,C));
               for( int H=0; H<3; H++){
                 for( int X=0; X<3; X++){
                   dMe1dF(K,L,j,C)+= G1*(dlnCepr1(K,L,H,X))*Fppr1inv(W,H)*Fppr1inv(V,X)*(Fn(j,V)*I2(W,C)+Fn(j,W)*I2(V,C));
                   for( int Y=0; Y<3; Y++){
                     for( int Z=0; Z<3; Z++){
                       dMe1dF(K,L,j,C)-= G1*(1./3.*I2(Y,Z)*dlnCepr1(Y,Z,H,X)*I2(K,L))*Fppr1inv(W,H)*Fppr1inv(V,X)*(Fn(j,V)*I2(W,C)+Fn(j,W)*I2(V,C));
                       dMe1dF(K,L,j,C)+= 1./2.*K1*I2(Y,Z)*dlnCepr1(Y,Z,H,X)*I2(K,L)
                                         *Fppr1inv(W,H)*Fppr1inv(V,X)*(Fn(j,V)*I2(W,C)+Fn(j,W)*I2(V,C));
                     }
                   }			
                 }
               }
	     }
	   }
         }
       }
     }
   }

   if(depsilon1!=0.)
   {
     for( int E=0; E<3; E++){
       for( int Z=0; Z<3; Z++){  
         for( int W=0; W<3; W++){
           for( int V=0; V<3; V++){
             dFp1dF(E,Z,W,V)=0.;
             for( int I=0; I<3; I++){
               for( int O=0; O<3; O++){
                 for( int P=0; P<3; P++){
                   dFp1dF(E,Z,W,V)+=dexpdDp1(E,I,O,P)*(N1(O,P)*depsilon1dF(W,V))*Fppr1(I,Z);
                   for( int j=0; j<3; j++){
                     for( int C=0; C<3; C++){
                       dFp1dF(E,Z,W,V)+=dexpdDp1(E,I,O,P)*depsilon1*dN1dC(O,P,W,V)*(Fn(j,V)*I2(W,C)+Fn(j,W)*I2(V,C))*Fppr1(I,Z);
                     }
                   }
                 }
               }
             }
           }
         }
       }
     }
   }
			  
   if(normdevMe1!=0.)
   {
     for( int j=0; j<3; j++){
       for( int C=0; C<3; C++){ 
         dTaudF(j,C)=0.;
         for( int A=0; A<3; A++){
           for( int B=0; B<3; B++){
             dTaudF(j,C)+=1./sqrt(2.)*dMe1dF(A,B,j,C)*devMe1(A,B)/normdevMe1 ;
             for( int E=0; E<3; E++){
               for( int D=0; D<3; D++){
                 dTaudF(j,C)-=1./(3.*sqrt(2.))*devMe1(A,B)/normdevMe1*I2(E,D)*dMe1dF(E,D,j,C)*I2(A,B) ;
			     
               }
             }
           }
         }
       }
     }
     for( int j=0; j<3; j++){
       for( int C=0; C<3; C++){ 
         dtauepsilondF(j,C)=dTaudF(j,C)*depsilon1+Tau1*depsilon1dF(j,C);
       }
     }	
   }	
}
		   
void mlawSMP::tangent1(STensor43 &Tangent1,const STensor43 &dMe1dF,const STensor3 &Fn, const STensor3 &Fp1,const STensor3 &Me1,
			 const STensor43 &dFp1dF, const STensor3 &Fe1,  const STensor3 &Ce1) const
{  
  STensorOperation::zero(Tangent1);

  // d Fp-1 / d
  static STensor3 Fp1inv,lnCe1;
  STensorOperation::zero(Fp1inv); STensorOperation::zero(lnCe1);
  STensorOperation::inverseSTensor3(Fp1,Fp1inv);
 
  static STensor43 dFe1dF,dFp1invdF,dlnCe1;
  STensorOperation::zero(dFe1dF); STensorOperation::zero(dFp1invdF); STensorOperation::zero(dlnCe1);
  static STensor63 ddlnCe1;
  STensorOperation::zero(ddlnCe1);
  bool ok=STensorOperation::logSTensor3(Ce1,_order,lnCe1,&dlnCe1,&ddlnCe1);
 
  for( int X=0; X<3; X++){
    for( int Y=0; Y<3; Y++){
      for( int j=0; j<3; j++){
        for( int C=0; C<3; C++){
          dFp1invdF(X,Y,j,C)   = 0.;
          for( int E=0; E<3; E++){
            for( int Z=0; Z<3; Z++){
              dFp1invdF(X,Y,j,C)-= Fp1inv(X,E)*dFp1dF(E,Z,j,C)*Fp1inv(Z,Y);
            }
          }
        }
      }
    }
  }
 
  // dFe / d 
  for( int i=0; i<3; i++){
    for( int M=0; M<3; M++){
      for( int j=0; j<3; j++){
        for( int C=0; C<3; C++){
          dFe1dF(i,M,j,C) = I2(i,j)*Fp1inv(C,M);
          for( int G=0; G<3; G++){
            dFe1dF(i,M,j,C) += Fn(i,G)*dFp1invdF(G,M,j,C);
          }
        }
      }
    }
  }
	
  for( int i=0; i<3; i++){ 
    for( int A=0; A<3; A++){
      for( int j=0; j<3; j++){
        for( int C=0; C<3; C++){
          Tangent1(i,A,j,C) = 0.;
          for( int M=0; M<3; M++){
            for( int K=0; K<3; K++){
              for( int L=0; L<3; L++){
                for( int S=0; S<3; S++){
                  Tangent1(i,A,j,C)+=dFe1dF(i,M,j,C)*dlnCe1(K,L,M,S)*Me1(K,L)*Fp1inv(A,S);
                  Tangent1(i,A,j,C)+=Fe1(i,M)*dlnCe1(K,L,M,S)*dMe1dF(K,L,j,C)*Fp1inv(A,S);
                  Tangent1(i,A,j,C)+=Fe1(i,M)*dlnCe1(K,L,M,S)*Me1(K,L)*dFp1invdF(A,S,j,C);
                  for( int Q=0; Q<3; Q++){
                    for( int V=0; V<3; V++){
                      for( int E=0; E<3; E++){
                        for( int U=0; U<3; U++){
			  Tangent1(i,A,j,C)+=Fe1(i,M)*ddlnCe1(K,L,M,S,Q,V)*(I2(Q,U)*Fe1(E,V)+Fe1(E,Q)*I2(V,U))
			      *dFe1dF(E,U,j,C)*Me1(K,L)*Fp1inv(A,S);
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }
}
 
void mlawSMP:: computedPdT1(STensor3 &dP1dT,const double j1, const STensor3 &Fn,const STensor3 &Fp1,const STensor3 &Fppr1,const STensor3  &Me1, const STensor3 &dDp1 , 
			   const STensor3 &Ce1,const STensor3 &Cepr1,const double dh,const double T,const double Tg,const double depsilon1,const STensor43 &dexpdDp1,
			   const double G1,const double K1,const double Mu1,const STensor3 C1,const double Hb ,const double Q1,
			   const STensor3 &N1,const double alalphath,double &dtauepsilon1dT,const double Tau1,const double dG1dT,const double dMu1dT,const double dK1dT,const double dHbdT,
			   const double dalphadT,const double L1,const double dQ1dT,const double dL1dT,const STensor3 &Fe1, const double dSa1dT) const
		
{
  STensorOperation::zero(dP1dT); 
  dtauepsilon1dT=0.; 
  double ddepsilon1dT=0.,domega1dT=0.,trlnCepr1=0.,normM1=0.,trMe1=0.,normdevMe1=0.;
  double  trC1=0., normdevlnCepr1=0., dTau1dT=0.;
  static STensor3  devlnCepr1 ,lnCe1,dMe1dT,dFp1invdT, dFp1dT,lnCepr1,devMe1;
  STensorOperation::zero(devlnCepr1); STensorOperation::zero(lnCe1); STensorOperation::zero(dMe1dT); STensorOperation::zero(dFp1invdT); STensorOperation::zero(dFp1dT); 
  STensorOperation::zero(lnCepr1); STensorOperation::zero(devMe1);
  static STensor43 dlnCe1, dlnCepr1;
  STensorOperation::zero(dlnCe1); STensorOperation::zero(dlnCepr1);
  static STensor63 ddlnCe1dce;
  STensorOperation::zero(ddlnCe1dce);


  bool ok=STensorOperation::logSTensor3(Ce1,_order,lnCe1,&dlnCe1,&ddlnCe1dce);
  ok=STensorOperation::logSTensor3(Cepr1,_order,lnCepr1,&dlnCepr1);
  STensorOperation::decomposeDevTr(lnCepr1, devlnCepr1, trlnCepr1);
  STensorOperation::decomposeDevTr(Me1, devMe1, trMe1);
  normdevMe1=devMe1.norm2();			    
  normdevlnCepr1=devlnCepr1.norm2();
  trC1=C1.trace(); 
  static STensor3 Fp1inv;
  STensorOperation::inverseSTensor3(Fp1,Fp1inv);

  normM1=Me1.norm2();

  domega1dT=2./_VOnKb1*(asinh(pow(depsilon1/L1,_m1)))-dG1dT/sqrt(2.)*normdevlnCepr1+dG1dT*depsilon1-1./2.*_alphap*dK1dT*trlnCepr1 
             +3.*_alphap*dK1dT*alalphath +3.*_alphap*K1*dalphadT+dSa1dT; 

  if (trC1>3.)
  {
    domega1dT+=dHbdT*pow(sqrt(trC1/3.)-1.,_l1); 
  }
  if(depsilon1>0.)
  {
    domega1dT+=- 2.*T*_m1/_VOnKb1*1./sqrt(pow(depsilon1/L1,2.*_m1)+1.)*pow(L1,-_m1-1.)*pow(depsilon1,_m1)*dL1dT;
  }

  if( j1!=0.)
    ddepsilon1dT=-domega1dT/j1;

  for( int A=0; A<3; A++){
    for( int B=0; B<3; B++){
      dMe1dT(A,B)=dG1dT*devlnCepr1(A,B)-2.*dG1dT*depsilon1*N1(A,B) -2.*G1*N1(A,B)*ddepsilon1dT+1./2.*dK1dT*trlnCepr1*I2(A,B)
                     -3.*dK1dT*alalphath*I2(A,B)-3.*K1*dalphadT*I2(A,B);
    }
  }

  if(normM1!=0.&&normdevMe1!=0.)
  { 
    dTau1dT=dot(dMe1dT,devMe1)/sqrt(2.)/normdevMe1 ;
    dTau1dT-=1./(3.*sqrt(2.))*trMe1/normdevMe1*dMe1dT.trace();
  }
  dtauepsilon1dT= ddepsilon1dT*Tau1+depsilon1*dTau1dT;

  for( int E=0; E<3; E++){
    for( int Z=0; Z<3; Z++){
      dFp1dT(E,Z)=0.;
      for( int I=0; I<3; I++){
        for( int O=0; O<3; O++){
          for( int P=0; P<3; P++){
            dFp1dT(E,Z)+=dexpdDp1(E,I,O,P)*N1(O,P)*ddepsilon1dT*Fppr1(I,Z); 
          }
        }
      }
    }
  }
			
  for( int X=0; X<3; X++){
    for( int Y=0; Y<3; Y++){
      dFp1invdT(X,Y)=0.;
      for( int E=0; E<3; E++){
        for( int Z=0; Z<3; Z++){
          dFp1invdT(X,Y)-= Fp1inv(X,E)*dFp1dT(E,Z)*Fp1inv(Z,Y);
        }
      }
    }
  }

  for( int i=0; i<3; i++){
    for( int A=0; A<3; A++){
      dP1dT(i,A)=0.;
      for( int N=0; N<3; N++){
        for( int M=0; M<3; M++){
          for( int K=0; K<3; K++){
            for( int L=0; L<3; L++){
              for( int S=0; S<3; S++){
                dP1dT(i,A)+=Fn(i,N)*dFp1invdT(N,M)*dlnCe1(K,L,M,S)*Me1(K,L)*Fp1inv(A,S);
                dP1dT(i,A)+=Fn(i,N)*Fp1inv(N,M)*dlnCe1(K,L,M,S)*dMe1dT(K,L)*Fp1inv(A,S);
                dP1dT(i,A)+=Fn(i,N)*Fp1inv(N,M)*dlnCe1(K,L,M,S)*Me1(K,L)*dFp1invdT(A,S);
                for( int Q=0; Q<3; Q++){
                  for( int V=0; V<3; V++){
                    for( int E=0; E<3; E++){
                      for( int U=0; U<3; U++){
                        for( int W=0; W<3; W++){
                          dP1dT(i,A)+=Fn(i,N)*Fp1inv(N,M)*ddlnCe1dce(K,L,M,S,Q,V)*(I2(Q,U)*Fe1(E,V)+Fe1(E,Q)*I2(V,U))
                                      *Fn(E,W)*dFp1invdT(W,V)*Me1(K,L)*Fp1inv(A,S);
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }
}
 	  
 
 
double mlawSMP::deformationEnergy(double y) const
{
 double j3;
 j3 =-1./2.*_Im3*_mu_groundState3*log10(y);
 return j3;			// W=0.5*trace(epsilon^T*sigma)
 
}
 
double mlawSMP::deformationEnergy(const STensor3& Cebar2) const 
{
  double j2;
  double traCebar2=Cebar2.trace();
  double XX=1./(1.-((traCebar2-3.)/_Im2));
  j2=-1./2.*_Im2*_mu_groundState2*log10(XX);
  return j2; 			
}

double mlawSMP::deformationEnergy(const STensor3& lnCepr1,const double G1,const double K1,const double alalphath,double T) const 
{
  double j1;
  double trlnCepr1=lnCepr1.trace();
  double normlnCepr1=lnCepr1.norm2();
  j1= G1*normlnCepr1*normlnCepr1 +1./2.*K1*trlnCepr1*trlnCepr1-3.*K1*alalphath*trlnCepr1*(T-_t0);  //energy
  return j1; 			
}
