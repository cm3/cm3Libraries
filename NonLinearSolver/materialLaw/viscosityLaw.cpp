//
// Description: Define viscosity laws for  plasticity
//
//
// Author:  <V.D. Nguyen>, (C) 2014
//
// Copyright: See COPYING file that comes with this distribution
//
//

#include "viscosityLaw.h"
#include "math.h"
#include "GmshMessage.h"

viscosityLaw::viscosityLaw(const int num, const bool init):_num(num),_initialized(init){};
viscosityLaw::viscosityLaw(const viscosityLaw& src):_num(src._num),_initialized(src._initialized){}
viscosityLaw& viscosityLaw::operator = (const viscosityLaw& src){
  _num = src._num;
  _initialized = src._initialized;
  return *this;
};



constantViscosityLaw::constantViscosityLaw(const int num, const double val, const bool init):viscosityLaw(num,init),_eta0(val){
    _temFunc = new constantScalarFunction(1.);
};

constantViscosityLaw::constantViscosityLaw(const constantViscosityLaw& src):viscosityLaw(src),_eta0(src._eta0){
    _temFunc = NULL;
    if (src._temFunc != NULL){
        _temFunc = src._temFunc->clone();
    }
};

constantViscosityLaw& constantViscosityLaw::operator = (const viscosityLaw& src){
  viscosityLaw::operator=(src);
  const constantViscosityLaw* ps = dynamic_cast<const constantViscosityLaw*>(&src);
  if (ps != NULL){
    _eta0 = ps->_eta0;
    
  if (ps->_temFunc != NULL){
      if (_temFunc != NULL){
        _temFunc->operator=(*ps->_temFunc);
      }
      else{
        _temFunc = ps->_temFunc->clone();
      }
  }
 }
  return *this;
};

constantViscosityLaw::~constantViscosityLaw(){
  if (_temFunc != NULL) {delete _temFunc; _temFunc = NULL;};
};

void constantViscosityLaw::setTemperatureFunction(const scalarFunction& Tfunc){
  if (_temFunc != NULL) delete _temFunc;
  _temFunc = Tfunc.clone();
}

void constantViscosityLaw::get(const double p, const double T, double& R, double& dR, double& dRdT) const {
    R = _eta0*_temFunc->getVal(T);
    dR = 0.;
    dRdT = _eta0*_temFunc->getDiff(T);    
};

void constantViscosityLaw::get(const double p, const double T, double& R, double& dR, double& dRdT, double& ddRdTT) const {
    R = _eta0*_temFunc->getVal(T);
    dR = 0.;
    dRdT = _eta0*_temFunc->getDiff(T);    
    ddRdTT = _eta0*_temFunc->getDoubleDiff(T);    
};

void constantViscosityLaw::get(const double p, const double T, double& R, double& dR, double& dRdT, double& ddRdTT, double& ddRdT) const {
    R = _eta0*_temFunc->getVal(T);
    dR = 0.;
    dRdT = _eta0*_temFunc->getDiff(T);    
    ddRdTT = _eta0*_temFunc->getDoubleDiff(T);    
    ddRdT = 0.;
};

saturatePowerViscosityLaw::saturatePowerViscosityLaw(const int num, const double eta0, const double q, const bool init):
                viscosityLaw(num,init), _eta0(eta0), _q(q){
                _temFunc_eta0 = new constantScalarFunction(1.);
                _temFunc_q = new constantScalarFunction(1.);
};

saturatePowerViscosityLaw::saturatePowerViscosityLaw(const saturatePowerViscosityLaw& src):viscosityLaw(src),_eta0(src._eta0),_q(src._q){
    _temFunc_eta0 = NULL;
    if (src._temFunc_eta0 != NULL){
        _temFunc_eta0 = src._temFunc_eta0->clone();
    }
    _temFunc_q = NULL;
    if (src._temFunc_q != NULL){
        _temFunc_q = src._temFunc_q->clone();
    }
};

saturatePowerViscosityLaw& saturatePowerViscosityLaw::operator = (const viscosityLaw& src){
  viscosityLaw::operator=(src);
  const saturatePowerViscosityLaw* ps = dynamic_cast<const saturatePowerViscosityLaw*>(&src);
  if (ps != NULL){
    _eta0 = ps->_eta0;
    _q = ps->_q;
    
  if (ps->_temFunc_eta0 != NULL){
      if (_temFunc_eta0 != NULL){
        _temFunc_eta0->operator=(*ps->_temFunc_eta0);
      }
      else{
        _temFunc_eta0 = ps->_temFunc_eta0->clone();
      }
  }
  if (ps->_temFunc_q != NULL){
      if (_temFunc_q != NULL){
        _temFunc_q->operator=(*ps->_temFunc_q);
      }
      else{
        _temFunc_q = ps->_temFunc_q->clone();
      }
  }
  }
  return *this;
};

saturatePowerViscosityLaw::~saturatePowerViscosityLaw(){
  if (_temFunc_eta0 != NULL) {delete _temFunc_eta0; _temFunc_eta0 = NULL;};
  if (_temFunc_q != NULL) {delete _temFunc_q; _temFunc_q = NULL;};
};

void saturatePowerViscosityLaw::setTemperatureFunction_eta0(const scalarFunction& Tfunc){
  if (_temFunc_eta0 != NULL) delete _temFunc_eta0;
  _temFunc_eta0 = Tfunc.clone();
}

void saturatePowerViscosityLaw::setTemperatureFunction_q(const scalarFunction& Tfunc){
  if (_temFunc_q != NULL) delete _temFunc_q;
  _temFunc_q = Tfunc.clone();
}

void saturatePowerViscosityLaw::get(const double p, double& R, double& dR) const{
  R = _eta0 * pow(p,_q);
  if(p <1.e-6 and _q < 1.)
  {
    R =  _eta0 * pow(1.e-6,_q)*(p/1.e-6); 
    dR = _eta0 * pow(1.e-6,_q)   /1.e-6;
  }
  else
  {
    R = _eta0 * pow(p,_q);
    dR = _eta0 * pow(p,_q-1.) * _q;
  }
};

void saturatePowerViscosityLaw::get(const double p, const double T, double& R, double& dR, double& dRdT) const{
  
  double eta = _eta0*_temFunc_eta0->getVal(T);
  double DetaDT = _eta0*_temFunc_eta0->getDiff(T);
  double q = _q*_temFunc_q->getVal(T);
  double DqDT = _q*_temFunc_q->getDiff(T);
  R = eta * pow(p,q);
  if(p <1.e-6 and q < 1.)
  {
    R =  eta * pow(1.e-6,q)*(p/1.e-6); 
    dR = eta * pow(1.e-6,q)   /1.e-6;
    dRdT = DetaDT * pow(1.e-6,q)*(p/1.e-6) + eta *log(1.e-6) *pow(1.e-6,_q)*(p/1.e-6)*DqDT;
  }
  else
  {
    R = eta * pow(p,q);
    dR = eta * pow(p,q-1.) * q;
    dRdT = DetaDT * pow(p,q) + eta*log(p)*pow(p,_q)*DqDT;
  }
  
};

void saturatePowerViscosityLaw::get(const double p, const double T, double& R, double& dR, double& dRdT, double& ddRdTT) const{
  
  double eta = _eta0*_temFunc_eta0->getVal(T);
  double DetaDT = _eta0*_temFunc_eta0->getDiff(T);
  double DDetaDTT = _eta0*_temFunc_eta0->getDoubleDiff(T);
  double q = _q*_temFunc_q->getVal(T);
  double DqDT = _q*_temFunc_q->getDiff(T);
  double DDqDTT = _q*_temFunc_q->getDoubleDiff(T);
  
  R = eta * pow(p,q);
  if(p <1.e-6 and q < 1.)
  {
    R =  eta * pow(1.e-6,q)*(p/1.e-6); 
    dR = eta * pow(1.e-6,q)   /1.e-6;
    dRdT = DetaDT * pow(1.e-6,q)*(p/1.e-6) + eta * pow(1.e-6,_q)*(p/1.e-6)*DqDT*log(1.e-6);
    ddRdTT = DDetaDTT * pow(1.e-6,q)*(p/1.e-6) + 2*DetaDT * pow(1.e-6,_q)*(p/1.e-6)*DqDT*log(1.e-6)
            + eta*(p/1.e-6)*pow(1.e-6,_q)*(pow(log(1.e-6)*DqDT,2) + log(1.e-6)*DDqDTT);
  }
  else
  {
    R = eta * pow(p,q);
    dR = eta * pow(p,q-1.) * q;
    dRdT = DetaDT * pow(p,q) + eta *pow(p,_q)*DqDT*log(p);
    ddRdTT = DDetaDTT * pow(p,q) + 2*DetaDT*pow(p,_q)*DqDT*log(p)
            + eta*pow(p,_q)*(pow(log(p)*DqDT,2) + log(p)*DDqDTT);
  }
  
};

void saturatePowerViscosityLaw::get(const double p, const double T, double& R, double& dR, double& dRdT, double& ddRdTT, double& ddRdT) const{
  
  double eta = _eta0*_temFunc_eta0->getVal(T);
  double DetaDT = _eta0*_temFunc_eta0->getDiff(T);
  double DDetaDTT = _eta0*_temFunc_eta0->getDoubleDiff(T);
  double q = _q*_temFunc_q->getVal(T);
  double DqDT = _q*_temFunc_q->getDiff(T);
  double DDqDTT = _q*_temFunc_q->getDoubleDiff(T);
  
  R = eta * pow(p,q);
  if(p <1.e-6 and q < 1.)
  {
    R =  eta * pow(1.e-6,q)*(p/1.e-6); 
    dR = eta * pow(1.e-6,q)   /1.e-6;
    dRdT = DetaDT * pow(1.e-6,q)*(p/1.e-6) + eta * pow(1.e-6,_q)*(p/1.e-6)*DqDT*log(1.e-6);
    ddRdTT = DDetaDTT * pow(1.e-6,q)*(p/1.e-6) + 2*DetaDT * pow(1.e-6,_q)*(p/1.e-6)*DqDT*log(1.e-6)
            + eta*(p/1.e-6)*pow(1.e-6,_q)*(pow(log(1.e-6)*DqDT,2) + log(1.e-6)*DDqDTT);
    ddRdT = DetaDT * pow(1.e-6,q)   /1.e-6;
  }
  else
  {
    R = eta * pow(p,q);
    dR = eta * pow(p,q-1.) * q;
    dRdT = DetaDT * pow(p,q) + eta *pow(p,_q)*DqDT*log(p);
    ddRdTT = DDetaDTT * pow(p,q) + 2*DetaDT*pow(p,_q)*DqDT*log(p)
            + eta*pow(p,_q)*(pow(log(p)*DqDT,2) + log(p)*DDqDTT);
    ddRdT = DetaDT * pow(p,q-1.) * q + eta * (DqDT*pow(p,q-1.) + q*pow(p,q-1.)*DqDT*log(p));
  }
  
};

saturateSinhViscosityLaw::saturateSinhViscosityLaw(const int num, const double eta0, const double eta1, const double g0, const bool init):
                viscosityLaw(num,init),_eta0(eta0),_eta1(eta1),_g0(g0){
            _temFunc_eta0 = new constantScalarFunction(1.);
            _temFunc_eta1 = new constantScalarFunction(1.);
            _temFunc_g0 = new constantScalarFunction(1.);
};

saturateSinhViscosityLaw::saturateSinhViscosityLaw(const saturateSinhViscosityLaw& src):viscosityLaw(src),_eta0(src._eta0),_eta1(src._eta1),_g0(src._g0){
    _temFunc_eta0 = NULL;
    if (src._temFunc_eta0 != NULL){
        _temFunc_eta0 = src._temFunc_eta0->clone();
    }
    _temFunc_eta1 = NULL;
    if (src._temFunc_eta1 != NULL){
        _temFunc_eta1 = src._temFunc_eta1->clone();
    }
    _temFunc_g0 = NULL;
    if (src._temFunc_g0 != NULL){
        _temFunc_g0 = src._temFunc_g0->clone();
    }
};

saturateSinhViscosityLaw& saturateSinhViscosityLaw::operator = (const viscosityLaw& src){
  viscosityLaw::operator=(src);
  const saturateSinhViscosityLaw* ps = dynamic_cast<const saturateSinhViscosityLaw*>(&src);
  if (ps != NULL){
    _eta0 = ps->_eta0;
    _eta1 = ps->_eta1;
    _g0 = ps->_g0;
    
  if (ps->_temFunc_eta0 != NULL){
      if (_temFunc_eta0 != NULL){
        _temFunc_eta0->operator=(*ps->_temFunc_eta0);
      }
      else{
        _temFunc_eta0 = ps->_temFunc_eta0->clone();
      }
  }
  if (ps->_temFunc_eta1 != NULL){
      if (_temFunc_eta1 != NULL){
        _temFunc_eta1->operator=(*ps->_temFunc_eta1);
      }
      else{
        _temFunc_eta1 = ps->_temFunc_eta1->clone();
      }
  }
  if (ps->_temFunc_g0 != NULL){
      if (_temFunc_g0 != NULL){
        _temFunc_g0->operator=(*ps->_temFunc_g0);
      }
      else{
        _temFunc_g0 = ps->_temFunc_g0->clone();
      }
  }
    
  }
  return *this;
};

saturateSinhViscosityLaw::~saturateSinhViscosityLaw(){
  if (_temFunc_eta0 != NULL) {delete _temFunc_eta0; _temFunc_eta0 = NULL;};
  if (_temFunc_g0 != NULL) {delete _temFunc_g0; _temFunc_g0 = NULL;};
  if (_temFunc_eta1 != NULL) {delete _temFunc_eta1; _temFunc_eta1 = NULL;};
};

void saturateSinhViscosityLaw::setTemperatureFunction_eta0(const scalarFunction& Tfunc){
  if (_temFunc_eta0 != NULL) delete _temFunc_eta0;
  _temFunc_eta0 = Tfunc.clone();
}

void saturateSinhViscosityLaw::setTemperatureFunction_g0(const scalarFunction& Tfunc){
  if (_temFunc_g0 != NULL) delete _temFunc_g0;
  _temFunc_g0 = Tfunc.clone();
}

void saturateSinhViscosityLaw::setTemperatureFunction_eta1(const scalarFunction& Tfunc){
  if (_temFunc_eta1 != NULL) delete _temFunc_eta1;
  _temFunc_eta1 = Tfunc.clone();
}

void saturateSinhViscosityLaw::get(const double p, double& R, double& dR) const{
  double pa = p/_g0;
  if (pa< 1e-6){
    R = _eta0;
    dR = 0.;
  }
  else
  {
    R = _eta1 + (_eta0-_eta1)*pa/sinh(pa);
    dR = (_eta0-_eta1)*(1./sinh(pa) - (pa*cosh(pa))/(sinh(pa)*sinh(pa)))/_g0 ;
  }
};
void saturateSinhViscosityLaw::get(const double p, const double T, double& R, double& dR, double& dRdT) const{
  double pa = p/_g0;
  if (pa< 1e-6){
    R = _eta0;
    dR = 0.;
    dRdT = 0.; // correct it by yourselves
  }
  else
  {
    R = _eta1 + (_eta0-_eta1)*pa/sinh(pa);
    dR = (_eta0-_eta1)*(1./sinh(pa) - (pa*cosh(pa))/(sinh(pa)*sinh(pa)))/_g0 ;
    dRdT = 0.; // correct it yourselves
  }
};
void saturateSinhViscosityLaw::get(const double p, const double T, double& R, double& dR, double& dRdT, double& ddRdTT) const{
  double pa = p/_g0;
  if (pa< 1e-6){
    R = _eta0;
    dR = 0.;
    dRdT = 0.; // correct it by yourselves
    ddRdTT = 0.; // correct it by yourselves
  }
  else
  {
    R = _eta1 + (_eta0-_eta1)*pa/sinh(pa);
    dR = (_eta0-_eta1)*(1./sinh(pa) - (pa*cosh(pa))/(sinh(pa)*sinh(pa)))/_g0 ;
    dRdT = 0.; // correct it yourselves
    ddRdTT = 0.; // correct it yourselves
  }
};
void saturateSinhViscosityLaw::get(const double p, const double T, double& R, double& dR, double& dRdT, double& ddRdTT, double& ddRdT) const{
  double pa = p/_g0;
  if (pa< 1e-6){
    R = _eta0;
    dR = 0.;
    dRdT = 0.; // correct it by yourselves
    ddRdTT = 0.; // correct it by yourselves
    ddRdT = 0.; // correct it by yourselves
  }
  else
  {
    R = _eta1 + (_eta0-_eta1)*pa/sinh(pa);
    dR = (_eta0-_eta1)*(1./sinh(pa) - (pa*cosh(pa))/(sinh(pa)*sinh(pa)))/_g0 ;
    dRdT = 0.; // correct it yourselves
    ddRdTT = 0.; // correct it yourselves
    ddRdT = 0.; // correct it yourselves
  }
};