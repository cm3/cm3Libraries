//
// C++ Interface: material law
//
// Description: TransverseIsotropic law
//
//
// Author:  <L. Noels>, (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "mlawTransverseIsoCurvature.h"
#include <math.h>
#include "MInterfaceElement.h"

mlawTransverseIsoCurvature::mlawTransverseIsoCurvature(const int num,const double E,const double nu,
                           const double rho, const double EA, const double GA, const double nu_minor,
                           const double Centroid_x, const double Centroid_y, const double Centroid_z, const double PointStart1_x, const double PointStart1_y, 
                           const double PointStart1_z, const double PointStart2_x, const double PointStart2_y, 
                           const double PointStart2_z,const double init_Angle) : mlawTransverseIsotropic(num,E,nu, rho, EA, GA, nu_minor, 0., 0., 0.), 
                           _Centroid(Centroid_x, Centroid_y, Centroid_z), _PointStart1(PointStart1_x, PointStart1_y, PointStart1_z), _PointStart2(PointStart2_x, PointStart2_y, PointStart2_z),_init_Angle(init_Angle)
{

}
mlawTransverseIsoCurvature::mlawTransverseIsoCurvature(const mlawTransverseIsoCurvature &source) : mlawTransverseIsotropic(source),
                                                       _Centroid(source._Centroid), _PointStart1(source._PointStart1), _PointStart2(source._PointStart2),_init_Angle(source._init_Angle) {}
mlawTransverseIsoCurvature& mlawTransverseIsoCurvature::operator=(const materialLaw &source)
{
  mlawTransverseIsotropic::operator=(source);
  const mlawTransverseIsoCurvature* src =static_cast<const mlawTransverseIsoCurvature*>(&source);
  _Centroid = src->_Centroid;
  _PointStart1 = src->_PointStart1;
  _PointStart2 = src->_PointStart2;
  _init_Angle = src->_init_Angle;
  return *this;
}

void mlawTransverseIsoCurvature::createIPState(IPStateBase* &ips, bool hasBodyForce, const bool* state_,const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
  bool inter=true;
  const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
  if(iele==NULL) inter=false;

  SPoint3 pt_Global;
  ele->pnt(GP->pt[0],GP->pt[1],GP->pt[2],pt_Global);
  SVector3 GaussP(pt_Global);

  IPVariable* ipvi = new IPTransverseIsoCurvature(GaussP, _Centroid, _PointStart1, _PointStart2, _init_Angle);
  IPVariable* ipv1 = new IPTransverseIsoCurvature(GaussP, _Centroid, _PointStart1, _PointStart2, _init_Angle);
  IPVariable* ipv2 = new IPTransverseIsoCurvature(GaussP, _Centroid, _PointStart1, _PointStart2, _init_Angle);
  if(ips != NULL) delete ips;
  ips = new IP3State(state_,ipvi,ipv1,ipv2);
}



