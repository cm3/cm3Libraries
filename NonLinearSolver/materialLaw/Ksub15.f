c	*******************************************************************
c	********   Computes CRSS and backstress based on disloc densities
c	********   the latter are stored in statev (here called n_pu and nmx_pu)
c	********   Variables are normalized as follows:
c	********   rho ~ (mu * Burgers)**2 * disloc_density   (stored in rho(1) for transfer to Kevp)
c	********   rho(2) is the rate of increase of rho(1) with platsic slip
c	********   muBn ~ mu * Burgers * n_disloc
c	*******************************************************************
	Subroutine KcalcMtc( tau_c, rho, muBn, statev, MF0, gam
     &				, v_twin, weight, dMFmx, gam0, expo
     &				, imods, kcstt, istat, ntau )
	implicit none
c	===================================================
	include 'LD_param_size.f'
c	=====  'ARGUMENTS'
	integer istat, ntau
	real*8 tau_c, rho(mxs6,mxs1), muBn(mxs6)
	real*8 gam(mxslip), statev(ntau), MF0
	real*8 v_twin(13), weight, dMFmx, gam0(mxmods), expo
	integer*4 imods, kcstt
c	=====  'COMMON BLOCKS'
	real*8 Emod(6,6,mxcstt), Ecpl(6,6,mxcstt)
	real*8 twinS(mxmods,mxcstt)
	integer*4 nslip(mxmods,mxcstt), nmods(mxcstt)
	integer*4 nslipT(mxcstt), idtwin(3,mxmods,mxcstt)
	logical incrH(mxcstt), latentH(mxcstt)
	common /KmatLD/ Emod, Ecpl, twinS, idtwin, incrH
     &			, latentH, nslip, nmods, nslipT
c	::::  'slip systems geometry'
	real*8 A1(6,mxslip,mxcstt), B1(3,mxslip,mxcstt)
	real*8 miller(7,mxslip,mxcstt)
	common /KLP0/ A1, B1, miller
c	::::  'strain hardening'
	real*8 hard_p(mxHP,mxmods,mxcstt), hard_M(mxs2,mxs2)
	integer*4 IDhard(mxmods,mxcstt)
	common /KhardLD/ hard_p, hard_M, IDhard
c	::::  'Kinematic hardening'
	real*8 AB(2,mxcstt)
	logical kinH(mxcstt)
	common /KkinHLD/ AB, kinH
c	=====  'LOCAL VARIABLES'
	real*8 dtau, sumgam, tauc1, tauc2, tmp, tmp2
	integer*4 islip, islip2, jslip, imods2, i, j, k
    	real*8 Bgh(12,4), h_recov, alpha_forest, k_forest
	real*8 e_twin, r_stack, b_burgers, Rho_0, mu2b, tmp3
	real*8 twinVFtot, Mu, Lambda, lambda0, gam1, v_tw(13)
	real*8 hcp, hncp, b_param, muBn0, invDgr, Mub
	real*8 tau_b, n_pu, nmx_pu, tau_c0
	integer noel

	return
	end


c	*******************************************************************
c	********   'Routine that is called during preprocessing'
c	********   'initializes state variables and material parameters'
c	*******************************************************************
	SUBROUTINE SDVINI(STATEV,COORDS,NSTATV,NCRDS,NOEL,NPT,
     1  LAYER,KSPT)
	implicit none
c	===================================================
	include 'LD_param_size.f'
c	===================================================
c	=====  ARGUMENTS:
	integer nstatv, ncrds, noel, npt, layer, kspt
	real*8 STATEV(NSTATV),COORDS(NCRDS) 
c	===================================================
c	=====  COMMON BLOCKS:
c	::::  Constants
	real*8 sq2, C1, C2, C3, C4, C12
	integer*4 i6(3,3)
	common /KcstLD/ sq2, C1, C2, C3, C4, C12, i6
c	::::  1st set of material parameters
	real*8 gam0dt(mxmods,mxcstt), expo(mxcstt), Sadim
	real*8 Drlxmod(6,mxrlx), Wrlxmod(3,mxrlx), ellps_ax(3)
	integer*4 rlxmod(3,2*mxrlx), nbrlx(3), ncstt, nneb
	integer ncrys1, nelt, ngauss
	common /KrlxLD/ gam0dt, expo, Sadim, Drlxmod, Wrlxmod
     &			, ellps_ax, ncrys1, nelt, ngauss
     &			, rlxmod, nbrlx, ncstt, nneb
c	::::  'Strain hardening parameters'
	real*8 hard_p(mxHP,mxmods,mxcstt), hard_M(mxs2,mxs2)
	integer*4 IDhard(mxmods,mxcstt)
	common /KhardLD/ hard_p, hard_M, IDhard
c	::::  'Kinematic hardening'
	real*8 AB(2,mxcstt)
	logical kinH(mxcstt)
	common /KkinHLD/ AB, kinH
c	::::  Other material parameters
	real*8 Emod(6,6,mxcstt), Ecpl(6,6,mxcstt)
	real*8 twinS(mxmods,mxcstt)
	integer*4 nslip(mxmods,mxcstt), nmods(mxcstt)
	integer*4 nslipT(mxcstt), idtwin(3,mxmods,mxcstt)
	logical incrH(mxcstt), latentH(mxcstt)
	common /KmatLD/ Emod, Ecpl, twinS, idtwin, incrH
     &			, latentH, nslip, nmods, nslipT
c	::::  variables used to read texture and for debug:
	real*8 ebpmx
	integer NOEL1, NPT1, KKSTEP, KKINC, ELT1, submx
	common /tempLD/ ebpmx, submx
     &		, NOEL1, NPT1, KKSTEP, KKINC, ELT1
c
c	===================================================
c       =====  LOCAL VARIABLES:
	real*8 phi(3), q(4), q2(4), tmp, tmp2, co_fi, phi1, x(3)
	real*8 V0, twinR(2), isq3, R(3,3), tmp3, visco_p(2)
	real*8 n_alam(3,2)
	integer i, j, k, istat, fil16, j1, icrys, ncrys, nn, n_trf
	integer istat3
	integer*4 icstt(mxneb), ineb, nbgr(1,1), nlayer, IDtw(10)
	integer*4 jj, imods, islip, kcstt, nrlx, irlx
	logical chkpr, var_nbcr, reorder, polyX, jeroen, fx
	character*10 line
c

	RETURN
	END

c       ************************************************************
c       ***   Reads material.i01 at the onset of the simulation    
c	***   Prepares elastic moduli & slip system description
c       ************************************************************
	subroutine KinitLD( fil16, chkpr )
	implicit none
c	===================================================
	include 'LD_param_size.f'
c	===================================================
c	=====  ARGUMENTS:
	integer fil16
	logical chkpr
c	===================================================
c	=====  COMMON BLOCKS:
c	::::  Constants
	real*8 sq2, C1, C2, C3, C4, CC12, sq3, pi, fpi
	integer*4 i6(3,3)
	common /KcstLD/ sq2, C1, C2, C3, C4, CC12, i6
	common /Kquater/ pi, fpi
c	::::  1st set of material parameters
	real*8 gam0dt(mxmods,mxcstt), expo(mxcstt), Sadim
	real*8 Drlxmod(6,mxrlx), Wrlxmod(3,mxrlx), ellps_ax(3)
	integer*4 rlxmod(3,2*mxrlx), nbrlx(3), ncstt, nneb
	integer ncrys1, nelt, ngauss
	common /KrlxLD/ gam0dt, expo, Sadim, Drlxmod, Wrlxmod
     &			, ellps_ax, ncrys1, nelt, ngauss
     &			, rlxmod, nbrlx, ncstt, nneb
c	::::  'Slip system geometry'
	real*8 A1(6,mxslip,mxcstt), B1(3,mxslip,mxcstt)
	real*8 miller(7,mxslip,mxcstt)
	common /KLP0/ A1, B1, miller
c	::::  'Strain hardening parameters'
	real*8 hard_p(mxHP,mxmods,mxcstt), hard_M(mxs2,mxs2)
	integer*4 IDhard(mxmods,mxcstt)
	common /KhardLD/ hard_p, hard_M, IDhard
c	::::  'Kinematic hardening'
	real*8 AB(2,mxcstt)
	logical kinH(mxcstt)
	common /KkinHLD/ AB, kinH
c	::::  Other material parameters
	real*8 Emod(6,6,mxcstt), Ecpl(6,6,mxcstt)
	real*8 twinS(mxmods,mxcstt)
	integer*4 nslip(mxmods,mxcstt), nmods(mxcstt)
	integer*4 nslipT(mxcstt), idtwin(3,mxmods,mxcstt)
	logical incrH(mxcstt), latentH(mxcstt)
	common /KmatLD/ Emod, Ecpl, twinS, idtwin, incrH
     &			, latentH, nslip, nmods, nslipT
c	==================================================
c	=====  LOCAL VARIABLES:
	real*8 C11, C12, C13, C33, C44, tmp1, tmp2, S_cr(6), vec6(6)
	real*8 N(3), B(3), normB, normN, NHex(4), BHex(4), c_sur_a
	real*8 phi(3), q(4), mat3(3,3), SH, tmp, MF0, tau_c(mxslip)
	real*8 mu, Burgers, alpha
	real*8 accslp(24), ElW, PlW
	integer*4 i, i1, ii, j, islip, idum, actmod(mxmods)
	integer*4 imods, eltyp, icstt, nslip1, nrlx
	integer ineb, icrys, ncrys, igauss, irlx, ielt, k, k2
	integer n_elt_gr, n_gr_l, n_gr_f
	logical previous, hexago, peierls


	end

c
c	************************************************
	subroutine Kdefcst
	implicit none
	real*8 sq2, C1, C2, C3, C4, C12, pi, fpi, sq3
	integer*4 i6(3,3)
	common /KcstLD/ sq2, C1, C2, C3, C4, C12, i6
	common /Kquater/ pi, fpi
c	 ::  for quaternion calculations
	pi= acos(-1.d0)
	fpi= pi / 180.D0
c	 ::  for 3x3 => 5
	sq2= sqrt(2.d0)
	sq3=  sqrt(3.d0)
	C1= 0.5d0 * (sq3+1.d0)
	C2= 0.5d0 * (sq3-1.d0)
	C3= (sq3+3.d0) / 6.d0
	C4= (3.d0-sq3) / 6.d0
	C12= C1 + C2
	return
	end

c
c       ************************************************
	subroutine Krotw3( rot, w1, w2, i3 )
c       !! Rotation of a symmetric matrix
	implicit none
	real*8 tmp, rot(3,3), w1(3), w2(3)
	integer*4 i, j, k, l, i3(3,3)
	w1(2)= -w1(2)
	do 1 i= 1,3
	  w2(i)= 0.D0
  1     continue
	do 2 i= 1,3
	do 2 k= 1,3
	  do 2 j= i+1,3
	  do 2 l= k+1,3
c	 (Only the upper-right part of the tensor is computed)
		tmp= rot(i,k) * rot(j,l) - rot(i,l) * rot(j,k)
c	(this is because w1(kl)= -w1(lk))
		w2(i3(i,j))= w2(i3(i,j)) + w1(i3(k,l)) * tmp
  2     continue
	w1(2)= -w1(2)
	w2(2)= -w2(2)
	return
	end
c
c       ************************************************
	subroutine Krots3w( rot, Dmat, Wmat, D, W, i6 )
c       !! Rotation of symmetric & asym matrices
	implicit none
	real*8 rot(3,3), Dmat(6), Wmat(3), D(6), W(3)
	real*8 tmp, tmp2, tmp3, A(3,3), A1(3,3)
	integer*4 i, j, k, l, i6(3,3), i3
	if ( wmat(1).eq.100.D0 ) then
	do i= 1,3
	  do j= 1,3
	    A(i,j)= 0.D0
	    A1(i,j)= 0.D0
	  end do
	end do
	A(1,2)= wmat(3)
	A(2,1)= -wmat(3)
	A(1,3)= -wmat(2)
	A(3,1)= wmat(2)
	A(2,3)= wmat(1)
	A(3,2)= -wmat(1)
	do i= 1,3
	  do j= 1,3
	    A1(i,j)= 0.D0
	    do k= 1,3
	      do l= 1,3
		A1(i,j)= A1(i,j) + rot(i,k)*rot(j,l)*A(k,l)
	      end do
	    end do
	  end do
	end do
	write(*,*) rot
	write(*,*)
	write(*,*) A1(2,3), A1(3,1), A1(1,2)
	end if
	do 1 i= 1,6
	  D(i)= 0.D0
	  if ( i.le.3 ) w(i)= 0.D0
  1	continue
	Wmat(2)= -Wmat(2)
	do 2 i= 1,3
	do 2 k= 1,3
	  do 2 j= i,3
	  do 2 l= k,3
c	 (Only the upper-right part of the tensor is computed)
		tmp= rot(i,k) * rot(j,l)
		tmp3= tmp
		if ( k.ne.l ) then
		  tmp2= rot(i,l) * rot(j,k)
		  tmp= tmp + tmp2
c		 (this is because Dmat(kl)= Dmat(lk))
		  if ( i.ne.j ) then
		    tmp3= tmp3 - tmp2
c		   (this is because Wmat(kl)= -Wmat(lk))
		    i3= 6-i-j
		    w(i3)= w(i3) + Wmat(6-k-l) * tmp3
		  end if
		end if
		D(i6(i,j))= D(i6(i,j)) + Dmat(i6(k,l)) * tmp
  2	continue
	Wmat(2)= -Wmat(2)
	w(2)= -w(2)
	if ( wmat(1).eq.100.D0 ) then
	write(*,*) w
	stop
	end if
	return
	end

c
c       ************************************************
	subroutine Krotmats( rot, vec1, vec2, neq, i6 )
c       !! Rotation of a symmetric matrix
	implicit none
	real*8 rot(3,3), vec1(6), vec6(6), vec2(6), tmp
	integer*4 i, j, k, l, i6(3,3), ij, neq
	logical check
	do i= 1,6
	  vec2(i)= 0.D0
	  vec6(i)= vec1(i)
	end do
	do 2 i= 1,3
	do 2 j= i,3
	  ij= i6(i,j)
	  do 2 k= 1,3
	  do 2 l= k,3
		tmp= rot(i,k) * rot(j,l)
		if ( k.ne.l ) tmp= tmp + rot(i,l) * rot(j,k)
c	(this is because mat(kl)= mat(lk))
		vec2(ij)= vec2(ij) + vec6(i6(k,l)) * tmp
  2     continue
	return
	end
c
c	***************************************************************
	subroutine KR_Dsa( D_sa, q, dStrdE, i6 )
c       !!!!  To take lattice spin effect on strain into account
	implicit none
c       ::::  Arguments
	real*8 D_sa(6), q(4), Rot(3,3), dStrdE(6,6)
	integer*4 i6(3,3)
	integer neq
c       ::::  Other variables
	integer*4 i, j, k, l, ij, m

	return
	end

c
c
c       ***************************************************************
	subroutine KRmodM1( Ar, A, Rot, neq, i6 )
c       !!!!  Rotation of a symmetric 4th order tensor
	implicit none
c       ::::  Arguments
	real*8 Ar(6,6), Rot(3,3), A(6,6) 
c       ::::  Other variables
	real*8 tmp1, tmp2, tmp3, tmp4
	integer*4 i, j, k, l, m, n, o, p, ij, mn, i6(3,3), neq
c
	do i= 4,6
          A(i,i)= A(i,i) / 2.d0
        end do
	do 1 i= 1,6
	  do 1 j= 1,6
	    Ar(i,j)= 0.D0
   1    continue
	do 2 i= 1,3
	  do 2 j= i,3
	    ij= i6(i,j)
	    do 2 k= 1,3
	      do 2 l= k,3
		do 2 m= 1,3
		  do 2 n= 1,3
		    mn= i6(m,n)
		    do 2 o= 1,3
		      do 2 p= 1,3
			tmp4= rot(i,m)*rot(j,n)*rot(k,o)*rot(l,p)
			Ar(ij,i6(k,l))= Ar(ij,i6(k,l))
     &					+ A(mn,i6(o,p)) * tmp4
   2    continue
	do i= 4,6
          A(i,i)= A(i,i) * 2.d0
        end do
	return
	end

c
c       ***************************************************************
	subroutine KRmodM( Ar, A, Rot, neq, i6 )
c       !!!!  Rotation of a symmetric 4th order tensor
	implicit none
c       ::::  Arguments
	real*8 Ar(6,6), Rot(3,3), A(6,6) 
c       ::::  Other variables
	real*8 tmp1, tmp2, tmp3, tmp4
	integer*4 i, j, k, l, m, n, o, p, ij, mn, i6(3,3), neq
c
	do 1 i= 1,6
	  do 1 j= 1,6
		Ar(i,j)= 0.D0
   1    continue
	do 2 i= 1,3
	do 2 j= i,3
	  ij= i6(i,j)
	  do 2 m= 1,3
	  do 2 n= m,3
		mn= i6(m,n)
c	(Only the upper-right part of the tensor is computed)
		tmp1= rot(i,m) * rot(j,n)
		if ( m.ne.n ) tmp1= tmp1 + rot(i,n) * rot(j,m)
c	(this is because Cmnop = Cnmop)
		do 2 k= 1,3
		do 2 o= 1,3
		  tmp2= tmp1 * rot(k,o)
		  tmp3= tmp2 * A(mn,i6(o,o))
		  do l= k,3
		    Ar(ij,i6(k,l))= Ar(ij,i6(k,l)) + tmp3 * rot(l,o)
		  end do
		  do 2 p= o+1,3
		    tmp3= tmp1 * rot(k,p)
		    do 2 l= k,3
		      tmp4= tmp2 * rot(l,p) + tmp3 * rot(l,o)
c		      (this is because Cmnpo = Cmnop)
		      Ar(ij,i6(k,l))= Ar(ij,i6(k,l))
     &					+ A(mn,i6(o,p)) * tmp4
   2	continue
	return
	end
c
c       ***************************************************************
	subroutine KRmodM2( dScrdE, RotS, dSdE_sa, i6 )
c       !!!!  Multicrystal version: rotation matrices are transposed
c       !!!!  & are different respectively for S and for E
c       !!!!  & tking lattice spin into account
	implicit none
c       ::::  Arguments
	real*8 dScrdE(6,6), RotS(3,3), dSdE_sa(6,6) 
	integer*4 i6(3,3)
c       ::::  Other variables
	real*8 tmp
	integer*4 i, j, k, l, m, n, ij, mn
c
c       >>>>  Perform rotation
	do 1 i= 1,6
	  do 1 j= 1,6
	    dSdE_sa(i,j)= 0.D0
   1    continue
	do 2 i= 1,3
	do 2 j= i,3
	  ij= i6(i,j)
	  do 2 m= 1,3
	  do 2 n= m,3
		mn= i6(m,n)
c	(Only the upper-right part of the tensor is computed)
		tmp= rotS(m,i) * rotS(n,j)
		if ( m.ne.n ) tmp= tmp + rotS(n,i) * rotS(m,j)
c	(this is because Cmnop = Cnmop)
		do 2 k= 1,3
		  do 2 l= k,3
		    dSdE_sa(ij,i6(k,l))= dSdE_sa(ij,i6(k,l))
     &		                  + dScrdE(mn,i6(k,l)) * tmp
   2    continue


	return
	end

c
c       ************************************************
	Subroutine Kdmat( q0, q2, w, dmatdw )
	implicit none
	real*8 q0(4), q2(4), w(3), dmatdw(3,3,3)
	real*8 q(4), dqdw(4,3), dq2dw(4,3)
	real*8 dmatdq(3,3,4), ang, tmp, tmp2, co, si
	integer*4 i, j, k, l
	do j= 1,4
	  q(j)= 2.D0 * q0(j)
	end do
	tmp= 2.D0*q(1)
	do i= 1,3
	  do j= 1,4
	    dmatdq(i,i,j)= 0.D0
	  end do
	  dmatdq(i,i,i+1)= 2.D0*q(i+1)
	  dmatdq(i,i,1)= tmp
	end do
	dmatdq(1,2,1)= -q(4)
	dmatdq(1,2,2)= q(3)
	dmatdq(1,2,3)= q(2)
	dmatdq(1,2,4)= -q(1)
	dmatdq(2,1,1)= q(4)
	dmatdq(2,1,2)= q(3)
	dmatdq(2,1,3)= q(2)
	dmatdq(2,1,4)= q(1)
c
	dmatdq(1,3,1)= q(3)
	dmatdq(1,3,2)= q(4)
	dmatdq(1,3,3)= q(1)
	dmatdq(1,3,4)= q(2)
	dmatdq(3,1,1)= -q(3)
	dmatdq(3,1,2)= q(4)
	dmatdq(3,1,3)= -q(1)
	dmatdq(3,1,4)= q(2)
c
	dmatdq(2,3,1)= -q(2)
	dmatdq(2,3,2)= -q(1)
	dmatdq(2,3,3)= q(4)
	dmatdq(2,3,4)= q(3)
	dmatdq(3,2,1)= q(2)
	dmatdq(3,2,2)= q(1)
	dmatdq(3,2,3)= q(4)
	dmatdq(3,2,4)= q(3)
c
	tmp= w(1)**2 + w(2)**2 + w(3)**2
	ang= dsqrt( tmp ) / 2.D0
	co= dcos(ang)
	si= dsqrt(1.D0 - co**2)
	si= si / ang 
	tmp= (co-si) / tmp / 2.D0
	si= si / 4.D0
	do j= 1,3
	  dqdw(1,j)= -w(j) * si
	  tmp2= tmp * w(j) 
	  do k= 2,4
	    dqdw(k,j)= w(k-1) * tmp2 
	  end do
	end do
	si= si * 2.D0
	dqdw(2,1)= dqdw(2,1) + si
	dqdw(3,2)= dqdw(3,2) + si
	dqdw(4,3)= dqdw(4,3) + si
c
	if ( q2(1).ne.100.D0 ) then
	  do i= 1,3
	    do j= 1,4
	      dq2dw(j,i)= 0.D0
	    end do
	    dq2dw(1,i)= q2(1) * dqdw(1,i)	
	    do j= 2,4
	      dq2dw(1,i)= dq2dw(1,i) - q2(j) * dqdw(j,i)
	      dq2dw(j,i)= dq2dw(j,i) + q2(j) * dqdw(1,i)
	      dq2dw(j,i)= dq2dw(j,i) + q2(1) * dqdw(j,i)
	    end do
	    dq2dw(2,i)= dq2dw(2,i) - q2(4) * dqdw(3,i)
	    dq2dw(2,i)= dq2dw(2,i) + q2(3) * dqdw(4,i)
	    dq2dw(3,i)= dq2dw(3,i) - q2(2) * dqdw(4,i)
	    dq2dw(3,i)= dq2dw(3,i) + q2(4) * dqdw(2,i)
	    dq2dw(4,i)= dq2dw(4,i) - q2(3) * dqdw(2,i)
	    dq2dw(4,i)= dq2dw(4,i) + q2(2) * dqdw(3,i)
	  end do
	  do i= 1,3
	    do j= 1,4
	      dqdw(j,i)= dq2dw(j,i)
	    end do
	  end do
	end if
c
	do i=1,3
	  do j= 1,3
	    do l= 1,3
	      dmatdw(i,j,l)= 0.d0 
	      do k= 1,4
		dmatdw(i,j,l)= dmatdw(i,j,l) 
     &			+ dmatdq(i,j,k) * dqdw(k,l)
	      end do
	    end do
	  end do
	end do
	return
	end

c
c       ************************************************
	Subroutine Kmat_Q4( mat, q )
	implicit none
	real*8 q(4), mat(3,3), tmp, tmp2
	integer*4 j
	q(1)= mat(1,1) + mat(2,2) + mat(3,3) + 1.d0 
	if ( q(1).le.1.d-10 ) then
	  tmp= ( mat(1,1) + 1.D0 ) / 2.D0
	  tmp2= ( mat(2,2) + 1.D0 ) / 2.D0
	  if ( tmp.lt.0.d0 .or. tmp2.lt.0.D0
     &	     .or. tmp+tmp2 .gt. 1.D0 ) then
	    write(*,*) 'Prob in mat_Q4', tmp, tmp2
	    call xit
	  end if
	  q(1)= 0.D0
	  q(2)= sqrt( tmp )
	  q(3)= sqrt( tmp2 )
	  q(4)= sqrt( 1.D0 - tmp - tmp2 )
	  if ( mat(1,2).lt.0.D0 ) q(3)= -q(3)
	  if ( mat(1,3).lt.0.D0 ) q(4)= -q(4)
	  return
	end if
	q(1)= dsqrt( q(1) ) / 2.d0
	tmp= q(1) * 4.d0
	q(2)= ( mat(3,2)-mat(2,3) ) / tmp
	q(3)= ( mat(1,3)-mat(3,1) ) / tmp
	q(4)= ( mat(2,1)-mat(1,2) ) / tmp
	return
	end

c     ********************************************************
      subroutine KQ4mult( Res, q_1, q_2)
      implicit none
      integer i
      integer Res
      real*8 q_1(4), q_2(4), q_tmp(4)
c
      q_tmp(1)= q_1(1) * q_2(1)
      do i= 2,4
	q_tmp(1)= q_tmp(1) - q_1(i) * q_2(i)
	q_tmp(i)= q_1(1) * q_2(i) + q_2(1) * q_1(i) 
      end do
      q_tmp(2)= q_tmp(2) - ( q_1(3)*q_2(4) - q_1(4)*q_2(3) )
      q_tmp(3)= q_tmp(3) - ( q_1(4)*q_2(2) - q_1(2)*q_2(4) ) 
      q_tmp(4)= q_tmp(4) - ( q_1(2)*q_2(3) - q_1(3)*q_2(2) )
c
      if (Res.eq.1) then
	do i= 1,4
	  q_1(i)= q_tmp(i)
	end do
      else
	do i= 1,4
	  q_2(i)= q_tmp(i)
	end do
      end if
c
      return
      end

c
c     ****************************************************
      subroutine Keul_Q4( euler, q)
c
      implicit none
      real*8 q(4), euler(3), fi1, fi2, Fi, pi, fpi, fpi2
      integer j
	common /Kquater/ pi, fpi
c
	fpi2= fpi / 2.d0
      fi1= euler(1) * fpi2 
      Fi= euler(2) * fpi2
      fi2= euler(3) * fpi2 
c
c     :::::::::::::   corresponding quaternion:  :::::::::::: 
      q(1)= dcos(fi) * dcos( fi1+fi2 )
      q(2)= dsin(fi) * dcos( fi1-fi2 )
      q(3)= dsin(fi) * dsin( fi1-fi2 )
      q(4)= dcos(fi) * dsin( fi1+fi2 )
      if ( q(1).lt.0.d0 ) then
	do j= 1,4
	  q(j)= -q(j)
	end do
      end if
c
      return
      end
c
c     ********************************************************
      subroutine KQ4_eul( r, ang )
c
      implicit none
      integer i
      real*8 r(4), ang(3), pi, tmp, Karc, msin, mcos, fpi
      external Karc
      common /Kquater/ pi, fpi
c
      if (r(1).lt.0.d0) then
	do i= 1,4
	  r(i)= -r(i)
	end do
      end if
c
      tmp= r(1)**2 + r(4)**2
      if (abs(1.d0-tmp).lt.1.d-7) then
	mcos= r(1)
	msin= r(4)
	ang(1)= 2.D0*Karc(msin,mcos) - pi/4.d0
	ang(2)= 0.d0
	ang(3)= pi/4.d0
      else if (abs(tmp).lt.1.d-7) then
	mcos= r(2)
	msin= r(3)
	ang(1)= 2.D0*Karc(msin,mcos) + pi/4.d0
	ang(2)= pi
	ang(3)= pi/4.d0
      else
	ang(2)= dacos( 2.d0*tmp - 1.d0 )
	msin= r(2)*r(4) + r(1)*r(3)
	mcos= -r(3)*r(4) + r(1)*r(2)
	ang(1)= Karc(msin,mcos)
	msin= r(2)*r(4) - r(1)*r(3)
	mcos= r(3)*r(4) + r(1)*r(2)
	ang(3)= Karc(msin,mcos)
      end if
      do i= 1,3
	ang(i)= ang(i) / fpi
      end do
c	
      return
      end
c
c     =====================================================
      function Karc(si,co)
c
      implicit none
      real*8 si, co, tmp, fpi, pi, Karc
c
	common /Kquater/ pi, fpi
c
      if (abs(co).le.1.d-10) then
	if (si.gt.0) tmp= pi/2.d0
	if (si.lt.0) tmp= -pi/2.d0
      else
	if (co.gt.0.d0) tmp= atan(si/co)
	if (co.lt.0.d0) tmp= atan(si/co) + pi
      end if
      if (tmp.gt.pi) tmp= tmp - 2.d0*pi
c	
      Karc= tmp
      return
      end 
c
c	************************************************
	SUBROUTINE KMINV (A1,N,NP, D)
	implicit none
	integer nmax, n, np
	parameter(nmax=100)
	real*8 A1, A, D, biga, hold
	integer L, M, i, j, k, nk, kk, iz, ij, ki, ji, jp
	integer jk, ik, kj, jq, jr 
	DIMENSION A1(NP,NP),A(Nmax*Nmax),L(Nmax),M(Nmax)


	if ( n.gt.nmax .or. n.gt.np ) then 
	  write(*,*) ' Prob at minv:', n, np, nmax
	  call xit
	end if
	k= 0
	do j=1,N
	do i=1,N
	k= k + 1
	A(k)= A1(i,j)
	end do
	end do
C
C       SEARCH FOR LARGEST ELEMENT
C
      D=1.d0
      NK=-N
      DO 180 K=1,N
      NK=NK+N
      L(K)=K
      M(K)=K
      KK=NK+K
      BIGA=A(KK)
      DO 20 J=K,N
      IZ=N*(J-1)
      DO 20 I=K,N
      IJ=IZ+I
      IF (dABS(BIGA)-dABS(A(IJ))) 10,20,20
   10 BIGA=A(IJ)
      L(K)=I
      M(K)=J
   20 CONTINUE
C
C       INTERCHANGE ROWS
C
      J=L(K)
      IF (J-K) 50,50,30
   30 KI=K-N
      DO 40 I=1,N
      KI=KI+N
      HOLD=-A(KI)
      JI=KI-K+J
      A(KI)=A(JI)
   40 A(JI)=HOLD
C
C       INTERCHANGE COLUMNS
C
   50 I=M(K)
      IF (I-K) 80,80,60
   60 JP=N*(I-1)
      DO 70 J=1,N
      JK=NK+J
      JI=JP+J
      HOLD=-A(JK)
      A(JK)=A(JI)
   70 A(JI)=HOLD
C
C       DIVIDE COLUMN BY MINUS PIVOT (BIGA)
C
   80 IF (dABS(BIGA).LT.1.d-30) THEN
      D=0.d0
      go to 260
      ENDIF
      DO 120 I=1,N
      IF (I-K) 110,120,110
  110 IK=NK+I
      A(IK)=A(IK)/(-BIGA)
  120 CONTINUE
C
C       REDUCE MATRIX
C
      DO 150 I=1,N
      IK=NK+I
      HOLD=A(IK)
      IJ=I-N
      DO 150 J=1,N
      IJ=IJ+N
      IF (I-K) 130,150,130
  130 IF (J-K) 140,150,140
  140 KJ=IJ-I+K
      A(IJ)=HOLD*A(KJ)+A(IJ)
  150 CONTINUE
C
C       DIVIDE ROW BY PIVOT
C
      KJ=K-N
      DO 170 J=1,N
      KJ=KJ+N
      IF (J-K) 160,170,160
  160 A(KJ)=A(KJ)/BIGA
  170 CONTINUE
C
C       PRODUCT OF PIVOTS
C
      D=D*BIGA
C
C       REPLACE PIVOT BY RECIPROCAL
C
      A(KK)=1.d0/BIGA
  180 CONTINUE
C
C       FINAL ROW AND COLUMN INTERCHANGE
C
      K=N
  190 K=(K-1)
      IF (K) 260,260,200
  200 I=L(K)
      IF (I-K) 230,230,210
  210 JQ=N*(K-1)
      JR=N*(I-1)
      DO 220 J=1,N
      JK=JQ+J
      HOLD=A(JK)
      JI=JR+J
      A(JK)=-A(JI)
  220 A(JI)=HOLD
  230 J=M(K)
      IF (J-K) 190,190,240
  240 KI=K-N
      DO 250 I=1,N
      KI=KI+N
      HOLD=A(KI)
      JI=KI-K+J
      A(KI)=-A(JI)
  250 A(JI)=HOLD
      GO TO 190
  260 k= 0
	do j=1,N
	do i=1,N
	k= k + 1
	A1(i,j)= A(k)
	end do
	end do
C
      RETURN
      END
c
c ----------------------------------------------------------
c     Decomposition polaire F=RU: calcul des matrices R et U
c     par une méthode de type Newton-Raphson.
c     Modifs Phil. Thibaux pour debug + otpim mars 1999

      subroutine FRU(F,R,U)
	
      implicit none

      integer i,j,k,flag
	
      real*8 fx(6),x(6),dfdx(6,6),Invdfdx(6,6),dx(6), const(6), 
     &     invpu(6),det

      real*8 F(3,3),R(3,3),U(3,3),InvU(3,3),determinant, det33

      real*8 residu,aux,tol,IT


      return
      end

c
	subroutine Kxmat( vec1, vec4, i6 )
	implicit none
	real*8 vec1(6), vec2(6), vec3(6), vec4(6)
	integer*4 ideg, i6(3,3), i, j, k
	
	do j= 1,6
	  vec2(j)= vec1(j)
	  vec4(j)= vec1(j)
	  if ( j.le.3 ) vec4(j)= vec4(j) + 1.D0
	end do
	do ideg= 2,12
	  do i= 1,3
	    do j= i,3
	vec3(i6(i,j))= 0.D0
	      do k= 1,3
		 vec3(i6(i,j))= vec3(i6(i,j))
     &			+ vec2(i6(i,k)) * vec1(i6(j,k))
	      end do
	    end do
	  end do
	  do i= 1,6
	    vec2(i)= vec3(i) / float(ideg)
	    vec4(i)= vec4(i) + vec2(i)
	  end do
	end do
	return
	end
c
c
c	*****************************************************
	subroutine Kellps( n, qlamel )
	implicit none
	real*8 qlamel(4), n(3), tmp, tmp2
	integer i, j
	if ( n(3).lt.0.D0 ) then
	  do i= 1,3
	    n(i)= -n(i)
	  end do
	end if
c	write(20,*) n(1)/(1.+n(3)), n(2)/(1.+n(3))
c
c	>> Bring new PHI into quaternion
c	>>  n(3)=cos(PHI)
c	>>  q1=cos(PHI/2)...
	if ( n(3).lt.0.999999d0 ) then
	  tmp= ( 1.D0 + n(3) ) / 2.D0
	  qlamel(1)= sqrt( tmp )
	  qlamel(2)= sqrt( 1.D0 - tmp )
	  qlamel(3)= qlamel(2)
	  qlamel(4)= qlamel(1)
	else
	  qlamel(1)= 1.D0
	  qlamel(2)= 0.D0
	  qlamel(3)= 0.D0
	  qlamel(4)= 0.D0
	  return
	end if
	
c	>> Bring new phi1 into quaternion
	tmp2= -n(2) / sqrt( 1.D0 - n(3)**2 )
	tmp= max( 0.D0, ( 1.D0 + tmp2 ) / 2.D0 )
	tmp2= sqrt( tmp )
	if ( n(1).lt.0.D0 ) tmp2= -tmp2
	qlamel(1)= qlamel(1) * tmp2
	qlamel(2)= qlamel(2) * tmp2
	tmp= max( 0.D0, 1.D0-tmp )
	tmp2= sqrt( tmp )
	qlamel(3)= qlamel(3) * tmp2
	qlamel(4)= qlamel(4) * tmp2

	return
	end
c
c	*******************************************************************
c	********   'Computes the equivalent plastic strain'
c	********   '(MF is actually the sum of the slip increments)'
c	********   'based on the current value of the CRSS'
c	*******************************************************************
	Subroutine KcalcMF( MF, tau_c, nmods, icstt )
	implicit none
c	===================================================
	include 'LD_param_size.f'
c	===================================================
c	=====  'ARGUMENTS:'
	real*8 MF, tau_c(mxmods)
	integer*4 icstt, imods, nmods
c	===================================================
c	=====  'COMMON BLOCKS:'
c	::::  'Hardening parameters'
	real*8 hard_p(mxHP,mxmods,mxcstt), hard_M(mxs2,mxs2)
	integer*4 IDhard(mxmods,mxcstt)
	common /KhardLD/ hard_p, hard_M, IDhard
c
c	do imods= 1,nmods
c	  tau_c(imods)= max( tau_c(imods), hard_p(1,imods,icstt) )
c	end do
	imods= 1
	if ( IDhard(imods,icstt).eq.0 ) then
          write(*,*) 'IDhard=0 is not supported'
          call xit
cc 	  if ( tau_c(imods).le.hard_p(1,imods,icstt) ) then
cc 	    MF= 0.D0
cc 	  else if ( tau_c(imods)
cc      &			.le.0.99999 * hard_p(2,imods,icstt) ) then
cc 	    MF= log( (hard_p(2,imods,icstt)-hard_p(1,imods,icstt))
cc      &			/ ( hard_p(2,imods,icstt)-tau_c(imods)) )
cc      &			/ hard_p(4,imods,icstt)
cc 	  else
cc 	    MF= log( (hard_p(2,imods,icstt)-hard_p(1,imods,icstt))
cc      &			/ (0.00001*hard_p(2,imods,icstt)) )
cc      &			/ hard_p(4,imods,icstt)
cc 	  end if
	else if ( IDhard(imods,icstt).eq.1 ) then
          write(*,*) 'IDhard=1 is not supported'
          call xit
cc 	  MF= hard_p(2,imods,icstt) * (
cc      &		( tau_c(imods) / hard_p(1,imods,icstt)
cc      &			 )**hard_p(4,imods,icstt) - 1.D0 )
cc c	  (rem: hard_p(4) = 1/hard_p(3))
	else if ( IDhard(imods,icstt).eq.2 ) then
	  write(*,*) 'calcMF not available when idhard=2'
	  stop
	else if ( tau_c(imods).gt.hard_p(1,imods,icstt) ) then
	  MF= ( ( tau_c(imods) - hard_p(1,imods,icstt) )
     &			/hard_p(2,imods,icstt) )**hard_p(4,imods,icstt)
c	  (rem: hard_p(4) = 1/hard_p(3))
c          write(*,*) 'Ksub:2199 hard_p(2)=', hard_p(2,imods,icstt),
c     &      'MF=', MF
c 'This part is not reached in simplified thermal simulations with IDhard=7 ?
	else
	  MF= 0.D0
	end if
	return
	end
c
