//
// C++ Interface: material law
//
// Description: Coalescence elasto-plastic law with non-local damage
//
// Author:  <J. Leclerc, V.D - Nguyen>, (C) 2017
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "mlawNonLocalPorousCoalescence.h"
#include <math.h>
#include "MInterfaceElement.h"
#include "nonLinearMechSolver.h"
#include "ipNonLocalPorosity.h"

mlawNonLocalPorousThomasonLaw::mlawNonLocalPorousThomasonLaw(const int num,const double E,const double nu, const double rho,
            const double fVinitial, const double lambda0, const J2IsotropicHardening &j2IH, const CLengthLaw &cLLaw,
            const double tol, const bool matrixbyPerturbation, const double pert) :
                    mlawNonLocalPorosity(num,E,nu,rho,fVinitial,j2IH,cLLaw,tol,matrixbyPerturbation,pert),
                    _n(10.)
{
  _coalescenceLaw = new ThomasonCoalescenceLaw(num);          // no coalesnce law by default / trivial one
  _voidEvolutionLaw = new SphericalVoidStateEvolutionLawWithMatrixPlasticDeformation(num,lambda0,1.);
  _failedTol = 0.98;
};

mlawNonLocalPorousThomasonLaw::mlawNonLocalPorousThomasonLaw(const int num,const double E,const double nu, const double rho,
            const double fVinitial, const double lambda0, const double kappa, const J2IsotropicHardening &j2IH, const CLengthLaw &cLLaw,
            const double tol, const bool matrixbyPerturbation, const double pert) :
                    mlawNonLocalPorosity(num,E,nu,rho,fVinitial,j2IH,cLLaw,tol,matrixbyPerturbation,pert),
                    _n(10.)
{
  // modify _coalescenceLaw from default type created by mlawNonLocalPorosity::mlawNonLocalPorosity to adapted one
  _coalescenceLaw = new ThomasonCoalescenceLaw(num);          // no coalesnce law by default / trivial one
  _voidEvolutionLaw = new SphericalVoidStateEvolutionLawWithMatrixPlasticDeformation(num,lambda0,kappa);
  _failedTol = 0.98;
};



mlawNonLocalPorousThomasonLaw::mlawNonLocalPorousThomasonLaw(const mlawNonLocalPorousThomasonLaw &source) :
                    mlawNonLocalPorosity(source),
                     _n(source._n)
{


    // Coalescence law
  _coalescenceLaw = NULL;
  if(source._coalescenceLaw != NULL){
    _coalescenceLaw=source._coalescenceLaw->clone();
  }
    // Void state law
  _voidEvolutionLaw = NULL;
  if (source._voidEvolutionLaw != NULL){
    _voidEvolutionLaw = source._voidEvolutionLaw->clone();
  }
};

mlawNonLocalPorousThomasonLaw::~mlawNonLocalPorousThomasonLaw()
{
  if (_coalescenceLaw!=NULL) delete _coalescenceLaw; _coalescenceLaw = NULL;
  if (_voidEvolutionLaw!=NULL) delete _voidEvolutionLaw; _voidEvolutionLaw = NULL;
}


void mlawNonLocalPorousThomasonLaw::setYieldSurfaceExponent(const double newN)
{
  _n = newN;
};


void mlawNonLocalPorousThomasonLaw::setVoidEvolutionLaw(const voidStateEvolutionLaw& voidtateLaw)
{
  if (_voidEvolutionLaw != NULL) delete _voidEvolutionLaw;
  _voidEvolutionLaw = voidtateLaw.clone();
}
void mlawNonLocalPorousThomasonLaw::setCoalescenceLaw(const CoalescenceLaw& added_coalsLaw)
{
  if (_coalescenceLaw != NULL) delete _coalescenceLaw;
  _coalescenceLaw = added_coalsLaw.clone();
}


double mlawNonLocalPorousThomasonLaw::getOnsetCriterion(const IPNonLocalPorosity* q1) const {
  return q1->getConstRefToIPVoidState().getVoidLigamentRatio();
};


void mlawNonLocalPorousThomasonLaw::checkCoalescence(IPNonLocalPorosity* q1, const IPNonLocalPorosity *q0, const double* T) const{
  // do nothing
};


void mlawNonLocalPorousThomasonLaw::forceCoalescence(IPNonLocalPorosity* q1, const IPNonLocalPorosity *q0, const double* T) const
{
  // do nothing
}

bool mlawNonLocalPorousThomasonLaw::checkCrackCriterion(IPNonLocalPorosity* q1, const IPNonLocalPorosity *q0, const STensor3& Fcur, const SVector3& normal, double delayFactor, const double* T) const
{
  // do nothing
  return false;
}

void mlawNonLocalPorousThomasonLaw::checkFailed(IPNonLocalPorosity* q1, const IPNonLocalPorosity *q0) const{
  // Manage failure flag
  if (q0->isFailed()){
    q1->setFailed(true);
  }
  else
  {
    double Cft;
    _coalescenceLaw->computeConcentrationFactor(q0,q1,Cft);

    if (Cft < (1.-_failedTol)){
      q1->setFailed(true);
      #ifdef _DEBUG
      Msg::Info("mlawNonLocalPorousThomasonLaw::checkFailed: an ip has failed at Cft = %e", Cft);
      #endif //_DEBUG
    }
    else{
      q1->setFailed(false);
    }
  }
};

void mlawNonLocalPorousThomasonLaw::createIPState(IPStateBase* &ips, bool hasBodyForce, const bool* state_,const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
  Msg::Error("mlawNonLocalPorousThomasonLaw::createIPState was not defined!!!");
}

void mlawNonLocalPorousThomasonLaw::createIPVariable(const double fvInitial, IPNonLocalPorosity* &ipv) const
{
  if (ipv!=NULL) delete ipv;
  ipv = new IPNonLocalPorosity(fvInitial, _j2IH,  &_cLLaw, _nucleationLaw,_coalescenceLaw,_voidEvolutionLaw);
};


void mlawNonLocalPorousThomasonLaw::initLaws( const std::map< int, materialLaw* > &maplaw)
{
  // Initialise internal laws.
  _nucleationLaw->initNucleationLaw( this );
};


void mlawNonLocalPorousThomasonLaw::voidStateGrowth(const double yieldfV0, const double yieldfV1, // yield porosity
                        const double DeltahatD, const double DeltahatQ, const double DeltahatP, // internal variables
                        const IPNonLocalPorosity *q0, IPNonLocalPorosity *q1) const
{                        
  _voidEvolutionLaw->evolve(yieldfV0,yieldfV1,DeltahatD,DeltahatQ,DeltahatP,
                            q0,q1, q0->getConstRefToIPVoidState(),q1->getRefToIPVoidState());
}


double mlawNonLocalPorousThomasonLaw::yieldFunction(const double kcorEq, const double pcor, const double R, const double yieldfV,
                                const IPNonLocalPorosity* q0, const IPNonLocalPorosity* q1, const double* T,
                                bool diff, fullVector<double>* gradf,
                                bool withthermic, double* dfdT) const
{
  // Get values in ipvs
  double R0 = _j2IH->getYield0();
  double Cft, DCftDChi, DCftDW;
  _coalescenceLaw->computeConcentrationFactor(q0,q1,Cft,diff,&DCftDChi,&DCftDW);
  
  const IPCoalescence* ipcoales = &q1->getConstRefToIPCoalescence();
  double CftOffset = ipcoales->getCrackOffsetOnCft();
  double LodeOffset = ipcoales->getLodeParameterOffset();
  double yieldOffset = ipcoales->getYieldOffset();

  double s = (2./3.)*kcorEq*LodeOffset;
  double f = 0.;
  double dfdp, dfds;
  yieldFunctionBase(pcor,s,f,diff,&dfdp,&dfds); // already normalized with R0
  double factF = R0/(CftOffset*Cft*R);
  double yield = f*factF - 1. - yieldOffset;
  if (diff)
  {
    // with respect to [kcorEq pcor R fV Chi W]
    double dsdkcorEq = 2.*LodeOffset/3.;
    (*gradf)(0) = dfds*dsdkcorEq*factF;
    (*gradf)(1) = dfdp*factF;
    (*gradf)(2) = -f*factF/R;
    (*gradf)(3) = 0.;
    (*gradf)(4) = (-f*factF/Cft)*DCftDChi;
    (*gradf)(5) = (-f*factF/Cft)*DCftDW;

    if (withthermic)
    {
      *dfdT = 0.;
    }
   }
   return yield;
};

void mlawNonLocalPorousThomasonLaw::plasticFlow(double & Ns, double& Nv,
                              const double kcorEq, const double pcor, const double R,
                              const IPNonLocalPorosity* q0, const IPNonLocalPorosity* q1, const double* T,
                              bool diff, fullVector<double>* gradNs, // grad of Ns with respect to [kcorEq pcor R fV Chi W]
                              fullVector<double>* gradNv, // grad of Nv with respect to  [kcorEq pcor R fV Chi W]
                              bool withTher, double* dNsDT, double* dNvDT) const
{
  // associated flow rule
  double R0 = _j2IH->getYield0();
  const IPCoalescence* ipcoales = &q1->getConstRefToIPCoalescence();
  double CftOffset = ipcoales->getCrackOffsetOnCft();
  double LodeOffset = ipcoales->getLodeParameterOffset();
  
  double s = (2./3.)*kcorEq*LodeOffset;
  double dsdkcorEq = 2.*LodeOffset/3.;
  double f = 0.;
  double dfdp, dfds, DdfdpDp, DdfdpDs, DdfdsDp, DdfdsDs;
  yieldFunctionBase(pcor,s,f,true,&dfdp,&dfds,diff,&DdfdpDp,&DdfdpDs,&DdfdsDp,&DdfdsDs);
  
  double Cft, DCftDChi, DCftDW;
  _coalescenceLaw->computeConcentrationFactor(q0,q1,Cft,diff,&DCftDChi,&DCftDW);
  double factF = R0/(CftOffset*Cft*R);
  // yield = f*factF - 1
  Ns = dfds*dsdkcorEq*factF;
  Nv = dfdp*factF;

  if (diff)
  {
    // with respect to [kcorEq pcor R fV Chi W]
    (*gradNs)(0) = DdfdsDs*dsdkcorEq*dsdkcorEq*factF;
    (*gradNs)(1) = DdfdsDp*dsdkcorEq*factF;
    (*gradNs)(2) = -dfds*dsdkcorEq*factF/R;
    (*gradNs)(3) = 0.;
    (*gradNs)(4) = (-dfds*dsdkcorEq*factF/Cft)*DCftDChi;
    (*gradNs)(5) = (-dfds*dsdkcorEq*factF/Cft)*DCftDW;

    (*gradNv)(0) = DdfdpDs*dsdkcorEq*factF;
    (*gradNv)(1) = DdfdpDp*factF;
    (*gradNv)(2) = -dfdp*factF/R;
    (*gradNv)(3) = 0.;
    (*gradNv)(4) = (-dfdp*factF/Cft)*DCftDChi;
    (*gradNv)(5) = (-dfdp*factF/Cft)*DCftDW;
    if (withTher)
    {
      *dNsDT = 0.;
      *dNvDT = 0.;
    }
  }
};


void mlawNonLocalPorousThomasonLaw::yieldFunctionBase(double p, const double s, double& f,
                          bool diff, double* dfdp , double* dfds,
                          bool hess, double* ddfdpdp, double* ddfdpds,double* ddfdsdp,double* ddfdsds) const{
  double R0 = _j2IH->getYield0();
  // u = [p s]
  // x = x(u)
  fullVector<double> X(2);
  X(0) =(p + s)/R0;
  X(1) =(-p+s)/R0;

  static fullVector<double> DfDX(2);
  static fullMatrix<double> DDfDXDX(2,2);

  combinedPorousLaw::LpNorm(_n,X,f,diff,&DfDX,hess,&DDfDXDX);
  if (diff)
  {
    double DXDu[2][2];
    DXDu[0][0] = 1./R0;
    DXDu[0][1] = 1./R0;
    DXDu[1][0] = -1./R0;
    DXDu[1][1] = 1./R0;

    double dfdu[2];
    for (int i=0; i< 2; i++)
    {
      dfdu[i] = 0.;
      for (int q=0; q<2; q++)
      {
        dfdu[i] += DfDX(q)*DXDu[q][i];
      }
    }
    *dfdp = dfdu[0];
    *dfds = dfdu[1];
    if (hess)
    {
      double ddfdudu[2][2];
      for (int i=0; i< 2; i++)
      {
        for (int j=0; j<2; j++)
        {
          ddfdudu[i][j] = 0.;
          for (int q=0; q<2; q++)
          {
            for (int r=0; r< 2; r++)
            {
              ddfdudu[i][j] += DDfDXDX(q,r)*DXDu[r][j]*DXDu[q][i];
            }
          }
        }
      }

      *ddfdpdp = ddfdudu[0][0];
      *ddfdpds = ddfdudu[0][1];
      *ddfdsdp = ddfdudu[1][0];
      *ddfdsds = ddfdudu[1][1];
    }
  }


  /*
  double X[2];
  X[0] = (p + s)*(p+s)/(R0*R0);
  X[1] = (-p+s)*(-p+s)/(R0*R0);

  // function Z = Z(X)
  double halfN = 0.5*_n;
  double Z = 0.;
  if (X[0] > 0.)
  {
    Z += pow(X[0],halfN);
  }
  if (X[1] > 0.)
  {
    Z += pow(X[1],halfN);
  }
  // function f = f(Z)
  double oneOverN = 1./_n;
  f = 0.;
  if (Z >0.)
  {
    f = pow(Z,oneOverN);
  }

  if (diff)
  {
    if (Z > 0.)
    {
      // first derivatives DXDu
      double DXDu[2][2];
      DXDu[0][0] = 2.*(p+s)/(R0*R0);
      DXDu[0][1] = 2.*(p+s)/(R0*R0);
      DXDu[1][0] = -2.*(-p+s)/(R0*R0);
      DXDu[1][1] = 2.*(-p+s)/(R0*R0);

      //  first derivative of DZDX
      double DZDX[2];
      DZDX[0] = 0.;
      if (X[0] > 0.)
      {
        DZDX[0] += halfN*pow(X[0],halfN-1.);
      }
      DZDX[1] = 0.;
      if (X[1] > 0.)
      {
        DZDX[1] += halfN*pow(X[1],halfN-1.);
      }
      double dfdZ = oneOverN*pow(Z,oneOverN-1.);

      double dfdu[2];
      for (int i=0; i< 2; i++)
      {
        dfdu[i] = 0.;
        for (int q=0; q<2; q++)
        {
          dfdu[i] += dfdZ*DZDX[q]*DXDu[q][i];
        }
      }

      *dfdp = dfdu[0];
      *dfds = dfdu[1];

      if (hess)
      {
        // hess of X versus u DDXDuDu
        double DDXDuDu[2][2][2];
        DDXDuDu[0][0][0] = 2./(R0*R0);
        DDXDuDu[0][0][1] = 2./(R0*R0);
        DDXDuDu[0][1][0] = 2./(R0*R0);
        DDXDuDu[0][1][1] = 2./(R0*R0);
        DDXDuDu[1][0][0] = 2./(R0*R0);
        DDXDuDu[1][0][1] = -2./(R0*R0);
        DDXDuDu[1][1][0] = -2./(R0*R0);
        DDXDuDu[1][1][1] = 2./(R0*R0);

        double DDZDXDX[2][2];
        DDZDXDX[0][1] = 0.;
        DDZDXDX[1][0] = 0.;
        DDZDXDX[0][0] = 0.;
        if (X[0] > 0.)
        {
          DDZDXDX[0][0] += halfN*(halfN-1.)*pow(X[0],halfN-2.);
        }
        DDZDXDX[1][1] = 0.;
        if (X[1] > 0.)
        {
          DDZDXDX[1][1] += halfN*(halfN-1.)*pow(X[1],halfN-2.);
        }

        double ddfdZdZ = oneOverN*(oneOverN-1.)*pow(Z,oneOverN-2.);

        double ddfdudu[2][2];
        for (int i=0; i< 2; i++)
        {
          for (int j=0; j<2; j++)
          {
            ddfdudu[i][j] = 0.;
            for (int q=0; q<2; q++)
            {
              ddfdudu[i][j] += dfdZ*DZDX[q]*DDXDuDu[q][i][j];
              for (int r=0; r< 2; r++)
              {
                ddfdudu[i][j] += ddfdZdZ*DZDX[r]*DXDu[r][j]*DZDX[q]*DXDu[q][i] + dfdZ*DDZDXDX[q][r]*DXDu[r][j]*DXDu[q][i];
              }
            }
          }
        }


        *ddfdpdp = ddfdudu[0][0];
        *ddfdpds = ddfdudu[0][1];
        *ddfdsdp = ddfdudu[1][0];
        *ddfdsds = ddfdudu[1][1];
      }
    }
    else
    {
      *dfdp = 0.;
      *dfds = 0.;
      if (hess)
      {
        *ddfdpdp = 0.;
        *ddfdpds = 0.;
        *ddfdsdp = 0.;
        *ddfdsds = 0.;
      }
    }
  }
   */
};

double mlawNonLocalPorousThomasonLaw::I1J2J3_yieldFunction(const STensor3& kcor, const double R, const IPNonLocalPorosity* q0, const IPNonLocalPorosity* q1, const double* T,
                                bool diff, STensor3* DFDkcor, double* DFDR, std::vector<double>* DFDY,
                                bool withThermic, double *DFDT) const{
  //
  double R0 = _j2IH->getYield0();
  static STensor3 devKcor;
  static double pcor, kcorEq, kcorEqSq;

  const IPCoalescence* ipcoales = &q1->getConstRefToIPCoalescence();
  double CftOffset = ipcoales->getCrackOffsetOnCft();
  double LodeOffset = ipcoales->getLodeParameterOffset();
  double yieldOffset = ipcoales->getYieldOffset();

  STensorOperation::decomposeDevTr(kcor,devKcor,pcor);
  pcor /= 3.;
  kcorEqSq = 1.5*STensorOperation::doubledot(devKcor,devKcor);
  kcorEq = sqrt(kcorEqSq);
  double s = (2./3.)*kcorEq*LodeOffset;

  double f;
  yieldFunctionBase(pcor,s,f);

  double Cft, DCftDChi, DCftDW;
  _coalescenceLaw->computeConcentrationFactor(q0,q1,Cft,diff,&DCftDChi,&DCftDW);
  
  double factF = R0/(CftOffset*Cft*R);
  double yield = f*factF -1.- yieldOffset;

  if (diff)
  {
    this->I1J2J3_getYieldNormal(*DFDkcor,kcor,R,q0,q1,T);
    *DFDkcor *= (1./R0);
    //
    *DFDR = -f*factF/R;

    (*DFDY)[0] = 0.;
    (*DFDY)[1] = (-f*factF/Cft)*DCftDChi;
    (*DFDY)[2] = (-f*factF/Cft)*DCftDW;
    (*DFDY)[3] = 0.;

    if (withThermic){
      *DFDT = 0.;
    }
  }
  return yield;
};

void mlawNonLocalPorousThomasonLaw::I1J2J3_getYieldNormal(STensor3& Np, const STensor3& kcor, const double R,
                              const IPNonLocalPorosity* q0, const IPNonLocalPorosity* q1, const double* T,
                              bool diff, STensor43* DNpDkcor, STensor3* DNpDR, std::vector<STensor3>* DNpDY,
                              bool withThermic, STensor3* DNpDT) const
{
  // assosiative plastic flow
  double R0 = _j2IH->getYield0();
  static STensor3 devKcor;
  static double pcor, kcorEq, kcorEqSq;
  const IPCoalescence* ipcoales = &q1->getConstRefToIPCoalescence();
  double CftOffset = ipcoales->getCrackOffsetOnCft();
  double LodeOffset = ipcoales->getLodeParameterOffset();
  double Cft, DCftDChi, DCftDW;
  _coalescenceLaw->computeConcentrationFactor(q0,q1,Cft,diff,&DCftDChi,&DCftDW);

  STensorOperation::decomposeDevTr(kcor,devKcor,pcor);
  pcor /= 3.;
  kcorEqSq = 1.5*STensorOperation::doubledot(devKcor,devKcor);
  kcorEq = sqrt(kcorEqSq);
  double s = (2./3.)*kcorEq*LodeOffset;
  double DsDkcorEq = (2./3.)*LodeOffset;

  double f, DfDpcor, DfDs;
  double DDfDpcorDpcor, DDfDpcorDs, DDfDsDpcor, DDfDsDs;
  yieldFunctionBase(pcor,s,f,true,&DfDpcor,&DfDs,diff, &DDfDpcorDpcor, &DDfDpcorDs, &DDfDsDpcor, &DDfDsDs);
  
  // Np = R0*DyieldDKcor
  // yield = f*factF -1
  double factF = R0/(CftOffset*Cft*R);

  double DfDkcorEq = DfDs*DsDkcorEq;

  static STensor3 DpcorDkcor, DkcorEqDkcor;
  STensorOperation::diag(DpcorDkcor,1./3.);
  DkcorEqDkcor = devKcor;
  DkcorEqDkcor *= (1.5/kcorEq);

  STensorOperation::zero(Np);
  Np.daxpy(DpcorDkcor,DfDpcor);
  Np.daxpy(DkcorEqDkcor,DfDkcorEq);
  Np *= (R0*factF); // to obain a dimensionless tensor

  if (diff)
  {
    // to compute DNpDKcor
    // first term
    double DDfDpcorDkcorEq = DDfDpcorDs*DsDkcorEq;
    STensorOperation::zero(*DNpDkcor);
    STensorOperation::prodAdd(DpcorDkcor,DpcorDkcor,DDfDpcorDpcor,*DNpDkcor);
    STensorOperation::prodAdd(DpcorDkcor,DkcorEqDkcor,DDfDpcorDkcorEq,*DNpDkcor);


    // second term DfDkcorEq =  DfDs*DsDkcorEq
    double DDfDkcorEqDpcor = DDfDsDpcor*DsDkcorEq;
    double DDfDkcorEqDkcorEq = DDfDsDs*DsDkcorEq*DsDkcorEq;
    STensorOperation::prodAdd(DkcorEqDkcor,DpcorDkcor,DDfDkcorEqDpcor,*DNpDkcor);
    STensorOperation::prodAdd(DkcorEqDkcor,DkcorEqDkcor,DDfDkcorEqDkcorEq,*DNpDkcor);

    static STensor43 DDkcorEqDkcorDkcor;
    DDkcorEqDkcorDkcor = _I4dev;
    DDkcorEqDkcorDkcor *= (1.5/kcorEq);
    STensorOperation::prodAdd(devKcor,DkcorEqDkcor,-1.5/kcorEqSq,DDkcorEqDkcorDkcor);
    STensorOperation::axpy(*DNpDkcor,DDkcorEqDkcorDkcor,DfDs*DsDkcorEq);
    // scale
    (*DNpDkcor) *= (R0*factF);
    
    (*DNpDR) = Np;
    (*DNpDR) *= (-1./R);
    

    for (int i=0; i< DNpDY->size(); i++)
    {
      STensorOperation::zero((*DNpDY)[i]);
    }
    (*DNpDY)[1] = Np;
    (*DNpDY)[1] *= (-DCftDChi/Cft);
    
    (*DNpDY)[2] = Np;
    (*DNpDY)[2] *= (-DCftDW/Cft);
    
    if (withThermic)
    {
      STensorOperation::zero(*DNpDT);
    }
  }
};


// Default constructor


mlawNonLocalPorousThomasonLawWithMPS::mlawNonLocalPorousThomasonLawWithMPS(const int num,const double E,const double nu, const double rho,
                    const double fVinitial, const double lambda0, const J2IsotropicHardening &j2IH, const CLengthLaw &cLLaw,
                    const double tol, const bool matrixbyPerturbation, const double pert):
                    mlawNonLocalPorousThomasonLaw(num,E,nu,rho,fVinitial,lambda0,j2IH,cLLaw,tol,matrixbyPerturbation,pert), _cosSmmoothFactor(0.995)
{

};
mlawNonLocalPorousThomasonLawWithMPS::mlawNonLocalPorousThomasonLawWithMPS(const int num,const double E,const double nu, const double rho,
                    const double fVinitial, const double lambda0, const double kappa, const J2IsotropicHardening &j2IH, const CLengthLaw &cLLaw,
                    const double tol, const bool matrixbyPerturbation, const double pert):
                    mlawNonLocalPorousThomasonLaw(num,E,nu,rho,fVinitial,lambda0,kappa,j2IH,cLLaw,tol,matrixbyPerturbation,pert),_cosSmmoothFactor(0.995)
{

};

void mlawNonLocalPorousThomasonLawWithMPS::setSmoothFactor(double cs)
{
  _cosSmmoothFactor = cs;
  Msg::Info("smooth factor for cosinus of lode angle = %e",_cosSmmoothFactor);
};

mlawNonLocalPorousThomasonLawWithMPS::mlawNonLocalPorousThomasonLawWithMPS(const mlawNonLocalPorousThomasonLawWithMPS &source):
                  mlawNonLocalPorousThomasonLaw(source),_cosSmmoothFactor(source._cosSmmoothFactor){};


void mlawNonLocalPorousThomasonLawWithMPS::I1J2J3_getYieldNormal(STensor3& Np, const STensor3& kcor, const double R,
                              const IPNonLocalPorosity* q0, const IPNonLocalPorosity* q1, const double* T,
                              bool diff, STensor43* DNpDkcor, STensor3* DNpDR, std::vector<STensor3>* DNpDY,
                              bool withThermic, STensor3* DNpDT) const
{
  // assosiative plastic flow
  double R0 = _j2IH->getYield0();
  static STensor3 devKcor;
  static double pcor, kcorEq, kcorEqSq, J3;
  STensorOperation::decomposeDevTr(kcor,devKcor,pcor);
  pcor /= 3.;
  kcorEqSq = 1.5*STensorOperation::doubledot(devKcor,devKcor);
  kcorEq = sqrt(kcorEqSq);
  J3 = STensorOperation::determinantSTensor3(devKcor);

  // estimation DyieldDpcor, DyieldDkcorEq, DyieldDJ3
  double s, DsDkcorEq, DsDJ3;
  double xi, DxiDkcorEq,DxiDJ3;
  double costheta, DcosthetaDxi, DDcosthetaDxiDxi;
  if (kcorEq > 0)
  {
    xi = 27.*J3/(2.*kcorEq*kcorEqSq);
    DxiDkcorEq = -81.*J3/(2.*kcorEqSq*kcorEqSq);
    DxiDJ3 = 27./(2.*kcorEq*kcorEqSq);
    cosLodeAngle(_cosSmmoothFactor,xi,costheta,true,&DcosthetaDxi,diff,&DDcosthetaDxiDxi);
    s = (2./3.)*kcorEq*costheta;
    DsDkcorEq = (2./3.)*(costheta + kcorEq*DcosthetaDxi*DxiDkcorEq);
    DsDJ3 = (2./3.)*kcorEq*DcosthetaDxi*DxiDJ3;
  }
  else
  {
    s = 0.;
    DsDkcorEq = 0.;
    DsDJ3 = 0.;
  }
  double f, DfDpcor, DfDs;
  double DDfDpcorDpcor, DDfDpcorDs, DDfDsDpcor, DDfDsDs;
  yieldFunctionBase(pcor,s,f,true,&DfDpcor,&DfDs,diff, &DDfDpcorDpcor, &DDfDpcorDs, &DDfDsDpcor, &DDfDsDs);

  double DfDkcorEq = DfDs*DsDkcorEq;
  double DfDJ3 =DfDs*DsDJ3;

  static STensor3 DpcorDkcor, DkcorEqDkcor, DJ3Dkcor;
  STensorOperation::diag(DpcorDkcor,1./3.);
  DkcorEqDkcor = devKcor;
  DkcorEqDkcor *= (1.5/kcorEq);
  STensorOperation::multSTensor3(devKcor,devKcor,DJ3Dkcor);
  DJ3Dkcor(0,0) -= (2.*kcorEqSq/9.);
  DJ3Dkcor(1,1) -= (2.*kcorEqSq/9.);
  DJ3Dkcor(2,2) -= (2.*kcorEqSq/9.);
  
  double Cft, DCftDChi, DCftDW;
  _coalescenceLaw->computeConcentrationFactor(q0,q1,Cft,diff,&DCftDChi,&DCftDW);
  
  const IPCoalescence* ipcoales = &q1->getConstRefToIPCoalescence();
  double CftOffset = ipcoales->getCrackOffsetOnCft();
  double yieldOffset = ipcoales->getYieldOffset();
  double factF = R0/(CftOffset*Cft*R);

  STensorOperation::zero(Np);
  Np.daxpy(DpcorDkcor,DfDpcor);
  Np.daxpy(DkcorEqDkcor,DfDkcorEq);
  Np.daxpy(DJ3Dkcor,DfDJ3);
  
  Np *= (R0*factF); // to obain a dimensionless tensor

  if (diff)
  {
    // estimation DyieldDpcor, DyieldDkcorEq, DyieldDJ3
    double DDsDkcorEqDkcorEq, DDsDkcorEqDJ3, DDsDJ3DkcorEq, DDsDJ3DJ3;
    if (kcorEq > 0)
    {
      // second derivatives
      double DDxiDkcorEqDkcorEq, DDxiDkcorEqDJ3, DDxiDJ3DkcorEq, DDxiDJ3DJ3;
      DDxiDkcorEqDkcorEq = 324.*J3/(2.*kcorEqSq*kcorEqSq*kcorEq);
      DDxiDkcorEqDJ3 = -81./(2.*kcorEqSq*kcorEqSq);
      DDxiDJ3DkcorEq = -81./(2.*kcorEqSq*kcorEqSq);
      DDxiDJ3DJ3 = 0.;

      // second derivatives
      DDsDkcorEqDkcorEq = (2./3.)*(DcosthetaDxi*DxiDkcorEq+ DcosthetaDxi*DxiDkcorEq +kcorEq*DDcosthetaDxiDxi*DxiDkcorEq*DxiDkcorEq + kcorEq*DcosthetaDxi*DDxiDkcorEqDkcorEq);
      DDsDkcorEqDJ3 = (2./3.)*(DcosthetaDxi*DxiDJ3 + kcorEq*DDcosthetaDxiDxi*DxiDJ3*DxiDkcorEq + kcorEq*DcosthetaDxi*DDxiDkcorEqDJ3);
      DDsDJ3DkcorEq = (2./3.)*(DcosthetaDxi*DxiDJ3 + kcorEq*DDcosthetaDxiDxi*DxiDkcorEq*DxiDJ3 + kcorEq*DcosthetaDxi*DDxiDJ3DkcorEq);
      DDsDJ3DJ3 = (2./3.)*kcorEq*(DDcosthetaDxiDxi*DxiDJ3*DxiDJ3 + DcosthetaDxi*DDxiDJ3DJ3);
    }
    else
    {
      DDsDkcorEqDkcorEq = 0.;
      DDsDkcorEqDJ3 = 0.;
      DDsDJ3DkcorEq = 0.;
      DDsDJ3DJ3 = 0.;
    }

    // we have Np = R0*factF*(DfDpcor*DpcorDkcor + DfDs*DsDkcorEq*DkcorEqDkcor + DfDs*DsDJ3*DJ3Dkcor)
    // to compute DNpDKcor
    // first term
    double DDfDpcorDkcorEq = DDfDpcorDs*DsDkcorEq;
    double DDfDpcorDJ3 = DDfDpcorDs*DsDJ3;
    STensorOperation::zero(*DNpDkcor);
    STensorOperation::prodAdd(DpcorDkcor,DpcorDkcor,DDfDpcorDpcor,*DNpDkcor);
    STensorOperation::prodAdd(DpcorDkcor,DkcorEqDkcor,DDfDpcorDkcorEq,*DNpDkcor);
    STensorOperation::prodAdd(DpcorDkcor,DJ3Dkcor,DDfDpcorDJ3,*DNpDkcor);


    // second term DfDkcorEq =  DfDs*DsDkcorEq
    double DDfDkcorEqDpcor = DDfDsDpcor*DsDkcorEq;
    double DDfDkcorEqDkcorEq = DDfDsDs*DsDkcorEq*DsDkcorEq + DfDs*DDsDkcorEqDkcorEq;
    double DDfDkcorEqDJ3 = DDfDsDs*DsDJ3*DsDkcorEq + DfDs*DDsDkcorEqDJ3;
    STensorOperation::prodAdd(DkcorEqDkcor,DpcorDkcor,DDfDkcorEqDpcor,*DNpDkcor);
    STensorOperation::prodAdd(DkcorEqDkcor,DkcorEqDkcor,DDfDkcorEqDkcorEq,*DNpDkcor);
    STensorOperation::prodAdd(DkcorEqDkcor,DJ3Dkcor,DDfDkcorEqDJ3,*DNpDkcor);

    static STensor43 DDkcorEqDkcorDkcor;
    DDkcorEqDkcorDkcor = _I4dev;
    DDkcorEqDkcorDkcor *= (1.5/kcorEq);
    STensorOperation::prodAdd(devKcor,DkcorEqDkcor,-1.5/kcorEqSq,DDkcorEqDkcorDkcor);
    STensorOperation::axpy(*DNpDkcor,DDkcorEqDkcorDkcor,DfDs*DsDkcorEq);

    // third term DfDJ3 = DfDs*DsDJ3
    double DDfDJ3Dpcor = DDfDsDpcor*DsDJ3;
    double DDfDJ3DkcorEq = DDfDsDs*DsDkcorEq*DsDJ3 + DfDs*DDsDJ3DkcorEq;
    double DDfDJ3DJ3 = DDfDsDs*DsDJ3*DsDJ3 + DfDs*DDsDJ3DJ3;
    STensorOperation::prodAdd(DJ3Dkcor,DpcorDkcor,DDfDJ3Dpcor,*DNpDkcor);
    STensorOperation::prodAdd(DJ3Dkcor,DkcorEqDkcor,DDfDJ3DkcorEq,*DNpDkcor);
    STensorOperation::prodAdd(DJ3Dkcor,DJ3Dkcor,DDfDJ3DJ3,*DNpDkcor);
    static STensor43 DDJ3DkcorDKcor;
    for (int i=0; i<3; i++)
    {
      for (int j=0; j<3; j++)
      {
        for (int p=0; p<3; p++)
        {
          for (int q=0; q<3; q++)
          {
            DDJ3DkcorDKcor(i,j,p,q) = 0.;
            for (int r=0; r<3; r++)
            {
              DDJ3DkcorDKcor(i,j,p,q) += _I4dev(i,r,p,q)*devKcor(r,j) + devKcor(i,r)*_I4dev(r,j,p,q);
            }
          }
        }
      }
    }
    STensorOperation::prodAdd(_I,DkcorEqDkcor,-4.*kcorEq/9.,DDJ3DkcorDKcor);
    STensorOperation::axpy(*DNpDkcor,DDJ3DkcorDKcor,DfDs*DsDJ3);
    (*DNpDkcor) *= (R0*factF);
    
    //
    (*DNpDR) = Np;
    (*DNpDR) *= (-1./R);

    for (int i=0; i< getNumOfVoidCharacteristics(); i++)
    {
      STensorOperation::zero((*DNpDY)[i]);
    }
    (*DNpDY)[1] = Np;
    (*DNpDY)[1] *= (-DCftDChi/Cft);
    
    (*DNpDY)[2] = Np;
    (*DNpDY)[2] *= (-DCftDW/Cft);

    
    if (withThermic)
    {
      STensorOperation::zero(*DNpDT);
    }
  }
};

double mlawNonLocalPorousThomasonLawWithMPS::I1J2J3_yieldFunction(const STensor3& kcor, const double R, const IPNonLocalPorosity* q0, const IPNonLocalPorosity* q1, const double* T,
                            bool diff, STensor3* DFDkcor, double* DFDR, std::vector<double>* DFDY,
                            bool withThermic, double *DFDT) const{
  //
  double R0 = _j2IH->getYield0();
  static STensor3 devKcor;
  static double pcor, kcorEq, kcorEqSq, J3, s;

  STensorOperation::decomposeDevTr(kcor,devKcor,pcor);
  pcor /= 3.;
  kcorEqSq = 1.5*STensorOperation::doubledot(devKcor,devKcor);
  kcorEq = sqrt(kcorEqSq);
  J3 = STensorOperation::determinantSTensor3(devKcor);

  if (kcorEq > 0)
  {
    double xi, costheta;
    xi = 27.*J3/(2.*kcorEq*kcorEqSq);
    cosLodeAngle(_cosSmmoothFactor,xi,costheta);
    s = (2./3.)*kcorEq*costheta;
  }
  else
  {
    s = 0.;
  }
  double f;
  yieldFunctionBase(pcor,s,f);

  double Cft, DCftDChi, DCftDW;
  _coalescenceLaw->computeConcentrationFactor(q0,q1,Cft,diff,&DCftDChi,&DCftDW);
  
  const IPCoalescence* ipcoales = &q1->getConstRefToIPCoalescence();
  double CftOffset = ipcoales->getCrackOffsetOnCft();
  double yieldOffset = ipcoales->getYieldOffset();
  //
  double factF = R0/(CftOffset*Cft*R);
  double yield = f*factF -1. - yieldOffset;
  if (diff)
  {
    ////
    this->I1J2J3_getYieldNormal(*DFDkcor,kcor,R,q0,q1,T);
    *DFDkcor *= (1./R0);

    *DFDR = -f*factF/R;

    for (int i=0; i< DFDY->size(); i++)
    {
      (*DFDY)[i] = 0.;
    }
    (*DFDY)[1] = (-f*factF/Cft)*DCftDChi;
    (*DFDY)[2] = (-f*factF/Cft)*DCftDW;

    if (withThermic){
      *DFDT = 0.;
    }
  }

  return yield;
};

void mlawNonLocalPorousThomasonLawWithMPS::cosFunc(double xi, double& F, bool diff, double* dF, bool hess, double* ddF) const
{
  //
  // to avoid limits at xi =1
  // F=cos(acos(xi)/3), using xi = 2*(x+1)^2-1 or x = sqrt(0.5*(xi+1))-1
  // it will compute F by taylor approximation as function of x
  // matlab code
  /**
  clear all
  syms x
  format long
  coeff = double(coeffs(taylor(cos(acos(2*(x+1)^2-1)/3),'Order',10)))
  */
  // taylor expansion of polynomial order 9
  double N = 10;
  static double coeff[10] = {1.,0.44444,-0.041152,0.0097546,-0.0029806,0.0010303,-0.00038334,0.00014978,-6.0604e-05,2.5175e-05};
  double x[10];
  x[0] = 1.;
  x[1] = sqrt(0.5*(xi+1.))-1.;
  for (int i=2; i< N; i++)
  {
    x[i] = x[i-1]*x[1];
  }

  F = 0.;
  for (int i=0; i< N; i++)
  {
    F += coeff[i]*x[i];
  }

  if (diff)
  {
    double dFdx = 0.;
    for (int i=1; i< N; i++)
    {
      dFdx += i*coeff[i]*x[i-1];
    }

    double fact = 0.5/(sqrt(2.));
    double DxDxi = fact/(sqrt(xi+1.));
    *dF = dFdx*DxDxi;

    if (hess)
    {
      double DDxDxiDxi = -fact/(2.*(1.+xi)*sqrt(xi+1.));
      double ddFdxdx = 0.;
      for (int i=2; i< N; i++)
      {
        ddFdxdx += i*(i-1)*coeff[i]*x[i-2];
      }
      *ddF = dFdx*DDxDxiDxi + ddFdxdx*DxDxi*DxDxi;
    }
  }
};

void mlawNonLocalPorousThomasonLawWithMPS::cosLodeAngle(double regFact, double xi, double& F, bool diff, double* dF, bool hess, double* ddF) const{
  // function F = cos((acos(regFact*(xi-1)+1))/3)
  /*
  double x = regFact*(xi-1.) +1.;
  double G = (acos(x))/3.;
  F = cos(G);
  if (diff)
  {
    double DFDG = -sin(G);
    double DGDx = -1./(3.*sqrt(1.-x*x));
    double DxDxi = regFact;
    *dF = DFDG*DGDx*DxDxi;
    if (hess)
    {
      double DDFDGDG = -cos(G);
      double DDGDxDx = DGDx*x/(1.-x*x);
      *ddF = DDFDGDG*DGDx*DxDxi*DGDx*DxDxi + DFDG*DDGDxDx*DxDxi*DxDxi;
    }
  }
   */
   // resolve problem at xi = 1 and -1
  double xiReg = regFact*(xi-1.) +1.;
  cosFunc(xiReg,F,diff,dF,hess,ddF);
  if (diff)
  {
    (*dF) *=  regFact;
    if (hess)
    {
      (*ddF) *= (regFact*regFact);
    }
  }
};


mlawNonLocalPorousThomasonLawWithMSS::mlawNonLocalPorousThomasonLawWithMSS(const int num,const double E,const double nu, const double rho,
                    const double fVinitial, const double lambda0, const J2IsotropicHardening &j2IH, const CLengthLaw &cLLaw,
                    const double tol, const bool matrixbyPerturbation, const double pert):
                    mlawNonLocalPorousThomasonLaw(num,E,nu,rho,fVinitial,lambda0,j2IH,cLLaw,tol,matrixbyPerturbation,pert), _sinSmmoothFactor(0.995)
{
  if (_coalescenceLaw) delete _coalescenceLaw;
  _coalescenceLaw = new simpleShearCoalescenceLaw(num);
};
mlawNonLocalPorousThomasonLawWithMSS::mlawNonLocalPorousThomasonLawWithMSS(const int num,const double E,const double nu, const double rho,
                    const double fVinitial, const double lambda0, const double kappa, const J2IsotropicHardening &j2IH, const CLengthLaw &cLLaw,
                    const double tol, const bool matrixbyPerturbation, const double pert):
                    mlawNonLocalPorousThomasonLaw(num,E,nu,rho,fVinitial,lambda0,kappa,j2IH,cLLaw,tol,matrixbyPerturbation,pert),_sinSmmoothFactor(0.995)
{
  if (_coalescenceLaw) delete _coalescenceLaw;
   _coalescenceLaw = new simpleShearCoalescenceLaw(num);
};

mlawNonLocalPorousThomasonLawWithMSS::mlawNonLocalPorousThomasonLawWithMSS(const mlawNonLocalPorousThomasonLawWithMSS& src):
  mlawNonLocalPorousThomasonLaw(src),_sinSmmoothFactor(src._sinSmmoothFactor){};

void mlawNonLocalPorousThomasonLawWithMSS::setSmoothFactor(double cs)
{
  _sinSmmoothFactor = cs;
  Msg::Info("smooth factor of lode angle = %e",_sinSmmoothFactor);

};

void mlawNonLocalPorousThomasonLawWithMSS::setShearFactor(double g)
{
  simpleShearCoalescenceLaw* shearL = dynamic_cast<simpleShearCoalescenceLaw*>(_coalescenceLaw);
  if (shearL)
  {
    shearL->setShearFactor(g);
  }
  else
  {
    Msg::Error("simpleShearCoalescenceLaw must be used in mlawNonLocalPorousThomasonLawWithMSS::setShearFactor");
  }
};

void mlawNonLocalPorousThomasonLawWithMSS::setVoidLigamentExponent(double g)
{
  simpleShearCoalescenceLaw* shearL = dynamic_cast<simpleShearCoalescenceLaw*>(_coalescenceLaw);
  if (shearL)
  {
    shearL->setVoidLigamentExponent(g);
  }
  else
  {
    Msg::Error("simpleShearCoalescenceLaw must be used in mlawNonLocalPorousThomasonLawWithMSS::setVoidLigamentExponent");
  }
};

void mlawNonLocalPorousThomasonLawWithMSS::sinLodeAngle_shear(double regFact, double xi, double& F, bool diff, double* dF, bool hess, double* ddF) const
{
  double xiReg = regFact*(xi);
  sinFunc_acos(xiReg,F,diff,dF,hess,ddF);
  if (diff)
  {
    (*dF) *=  regFact;
    if (hess)
    {
      (*ddF) *= (regFact*regFact);
    }
  }

};

void mlawNonLocalPorousThomasonLawWithMSS::cosLodeAngle_shear(double regFact, double xi, double& F, bool diff, double* dF, bool hess, double* ddF) const
{
  double xiReg = regFact*(xi);
  cosFunc_acos(xiReg,F,diff,dF,hess,ddF);
  if (diff)
  {
    (*dF) *=  regFact;
    if (hess)
    {
      (*ddF) *= (regFact*regFact);
    }
  }

};


void mlawNonLocalPorousThomasonLawWithMSS::sinFunc_acos(double x, double& F, bool diff, double* dF, bool hess, double* ddF) const
{
  double G = (acos(x))/3.;
  F = sin(G);
  if (diff)
  {
    double DFDG = cos(G);
    double DGDx = -1./(3.*sqrt(1.-x*x));
    *dF = DFDG*DGDx;
    if (hess)
    {
      double DDFDGDG = -sin(G);
      double DDGDxDx = DGDx*x/(1.-x*x);
      *ddF = DDFDGDG*DGDx*DGDx + DFDG*DDGDxDx;
    }
  }
};

void mlawNonLocalPorousThomasonLawWithMSS::cosFunc_acos(double x, double& F, bool diff, double* dF, bool hess, double* ddF) const
{
  double G = (acos(x))/3.;
  F = cos(G);
  if (diff)
  {
    double DFDG = -sin(G);
    double DGDx = -1./(3.*sqrt(1.-x*x));
    *dF = DFDG*DGDx;
    if (hess)
    {
      double DDFDGDG = -cos(G);
      double DDGDxDx = DGDx*x/(1.-x*x);
      *ddF = DDFDGDG*DGDx*DGDx + DFDG*DDGDxDx;
    }
  }
};

void mlawNonLocalPorousThomasonLawWithMSS::I1J2J3_getYieldNormal(STensor3& Np, const STensor3& kcor, const double R,
                              const IPNonLocalPorosity* q0, const IPNonLocalPorosity* q1, const double* T,
                              bool diff, STensor43* DNpDkcor, STensor3* DNpDR, std::vector<STensor3>* DNpDY,
                              bool withThermic, STensor3* DNpDT) const
{
  // assosiative plastic flow
  double R0 = _j2IH->getYield0();
  static STensor3 devKcor;
  static double pcor, kcorEq, kcorEqSq, J3;
  STensorOperation::decomposeDevTr(kcor,devKcor,pcor);
  pcor /= 3.;
  kcorEqSq = 1.5*STensorOperation::doubledot(devKcor,devKcor);
  kcorEq = sqrt(kcorEqSq);
  J3 = STensorOperation::determinantSTensor3(devKcor);

  // estimation DyieldDpcor, DyieldDkcorEq, DyieldDJ3
  double s, DsDkcorEq, DsDJ3;
  double xi, DxiDkcorEq,DxiDJ3;
  double fact, DfactDxi, DDfactDxiDxi;
  if (kcorEq > 0)
  {
    xi = 27.*J3/(2.*kcorEq*kcorEqSq);
    DxiDkcorEq = -81.*J3/(2.*kcorEqSq*kcorEqSq);
    DxiDJ3 = 27./(2.*kcorEq*kcorEqSq);

    double costheta, DcosthetaDxi, DDcosthetaDxiDxi;
    double sintheta, DsinthetaDxi, DDsinthetaDxiDxi;
    cosLodeAngle_shear(_sinSmmoothFactor,xi,costheta,true,&DcosthetaDxi,diff,&DDcosthetaDxiDxi);
    sinLodeAngle_shear(_sinSmmoothFactor,xi,sintheta,true,&DsinthetaDxi,diff,&DDsinthetaDxiDxi);
    fact = (sintheta/2. + costheta*sqrt(3.)/2.);
    DfactDxi = DsinthetaDxi/2. + DcosthetaDxi*sqrt(3.)/2.;
    DDfactDxiDxi = DDsinthetaDxiDxi/2. + DDcosthetaDxiDxi*sqrt(3.)/2.;
    s = kcorEq*fact;
    DsDkcorEq = fact + kcorEq*DfactDxi*DxiDkcorEq;
    DsDJ3 =  kcorEq*DfactDxi*DxiDJ3;
  }
  else
  {
    s = 0.;
    DsDkcorEq = 0.;
    DsDJ3 = 0.;
  }
  
   double Cft, DCftDChi, DCftDW;
  _coalescenceLaw->computeConcentrationFactor(q0,q1,Cft,diff,&DCftDChi,&DCftDW);
  const IPCoalescence* ipcoales = &q1->getConstRefToIPCoalescence();
  double CftOffset = ipcoales->getCrackOffsetOnCft();
  double yieldOffset = ipcoales->getYieldOffset();
  
  double factF = 1./(CftOffset*Cft*R);

  static STensor3 DkcorEqDkcor, DJ3Dkcor;
  DkcorEqDkcor = devKcor;
  DkcorEqDkcor *= (1.5/kcorEq);
  STensorOperation::multSTensor3(devKcor,devKcor,DJ3Dkcor);
  DJ3Dkcor(0,0) -= (2.*kcorEqSq/9.);
  DJ3Dkcor(1,1) -= (2.*kcorEqSq/9.);
  DJ3Dkcor(2,2) -= (2.*kcorEqSq/9.);

  STensorOperation::zero(Np);
  Np.daxpy(DkcorEqDkcor,DsDkcorEq);
  Np.daxpy(DJ3Dkcor,DsDJ3);
  Np *=(R0*factF);

  if (diff)
  {
    // estimation DyieldDpcor, DyieldDkcorEq, DyieldDJ3
    double DDsDkcorEqDkcorEq, DDsDkcorEqDJ3, DDsDJ3DkcorEq, DDsDJ3DJ3;
    if (kcorEq > 0)
    {
      // second derivatives
      double DDxiDkcorEqDkcorEq, DDxiDkcorEqDJ3, DDxiDJ3DkcorEq, DDxiDJ3DJ3;
      DDxiDkcorEqDkcorEq = 324.*J3/(2.*kcorEqSq*kcorEqSq*kcorEq);
      DDxiDkcorEqDJ3 = -81./(2.*kcorEqSq*kcorEqSq);
      DDxiDJ3DkcorEq = -81./(2.*kcorEqSq*kcorEqSq);
      DDxiDJ3DJ3 = 0.;

      // second derivatives
      DDsDkcorEqDkcorEq = (DfactDxi*DxiDkcorEq+ DfactDxi*DxiDkcorEq +kcorEq*DDfactDxiDxi*DxiDkcorEq*DxiDkcorEq + kcorEq*DfactDxi*DDxiDkcorEqDkcorEq);
      DDsDkcorEqDJ3 = (DfactDxi*DxiDJ3 + kcorEq*DDfactDxiDxi*DxiDJ3*DxiDkcorEq + kcorEq*DfactDxi*DDxiDkcorEqDJ3);
      DDsDJ3DkcorEq = (DfactDxi*DxiDJ3 + kcorEq*DDfactDxiDxi*DxiDkcorEq*DxiDJ3 + kcorEq*DfactDxi*DDxiDJ3DkcorEq);
      DDsDJ3DJ3 = kcorEq*(DDfactDxiDxi*DxiDJ3*DxiDJ3 + DfactDxi*DDxiDJ3DJ3);
    }
    else
    {
      DDsDkcorEqDkcorEq = 0.;
      DDsDkcorEqDJ3 = 0.;
      DDsDJ3DkcorEq = 0.;
      DDsDJ3DJ3 = 0.;
    }


    STensorOperation::zero(*DNpDkcor);
    STensorOperation::prodAdd(DkcorEqDkcor,DkcorEqDkcor,DDsDkcorEqDkcorEq,*DNpDkcor);
    STensorOperation::prodAdd(DkcorEqDkcor,DJ3Dkcor,DDsDkcorEqDJ3,*DNpDkcor);

    static STensor43 DDkcorEqDkcorDkcor;
    DDkcorEqDkcorDkcor = _I4dev;
    DDkcorEqDkcorDkcor *= (1.5/kcorEq);
    STensorOperation::prodAdd(devKcor,DkcorEqDkcor,-1.5/kcorEqSq,DDkcorEqDkcorDkcor);
    STensorOperation::axpy(*DNpDkcor,DDkcorEqDkcorDkcor,DsDkcorEq);

    STensorOperation::prodAdd(DJ3Dkcor,DkcorEqDkcor,DDsDJ3DkcorEq,*DNpDkcor);
    STensorOperation::prodAdd(DJ3Dkcor,DJ3Dkcor,DDsDJ3DJ3,*DNpDkcor);
    static STensor43 DDJ3DkcorDKcor;
    for (int i=0; i<3; i++)
    {
      for (int j=0; j<3; j++)
      {
        for (int p=0; p<3; p++)
        {
          for (int q=0; q<3; q++)
          {
            DDJ3DkcorDKcor(i,j,p,q) = 0.;
            for (int r=0; r<3; r++)
            {
              DDJ3DkcorDKcor(i,j,p,q) += _I4dev(i,r,p,q)*devKcor(r,j) + devKcor(i,r)*_I4dev(r,j,p,q);
            }
          }
        }
      }
    }
    STensorOperation::prodAdd(_I,DkcorEqDkcor,-4.*kcorEq/9.,DDJ3DkcorDKcor);
    STensorOperation::axpy(*DNpDkcor,DDJ3DkcorDKcor,DsDJ3);
    *DNpDkcor *= (R0*factF);
    
    (*DNpDR) = Np;
    (*DNpDR) *= (-1./R);
    
    for (int i=0; i< getNumOfVoidCharacteristics(); i++)
    {
      STensorOperation::zero((*DNpDY)[i]);
    }
    (*DNpDY)[1] = Np;
    (*DNpDY)[1] *= (-DCftDChi/Cft);
    
    (*DNpDY)[2] = Np;
    (*DNpDY)[2] *= (-DCftDW/Cft);
    
    if (withThermic)
    {
      STensorOperation::zero(*DNpDT);
    }
  }
};

double mlawNonLocalPorousThomasonLawWithMSS::I1J2J3_yieldFunction(const STensor3& kcor, const double R, const IPNonLocalPorosity* q0, const IPNonLocalPorosity* q1, const double* T,
                            bool diff, STensor3* DFDkcor, double* DFDR, std::vector<double>* DFDY,
                            bool withThermic, double *DFDT) const{
  //
  double R0 = _j2IH->getYield0();
  static STensor3 devKcor;
  static double pcor, kcorEq, kcorEqSq, J3, s;

  STensorOperation::decomposeDevTr(kcor,devKcor,pcor);
  pcor /= 3.;
  kcorEqSq = 1.5*STensorOperation::doubledot(devKcor,devKcor);
  kcorEq = sqrt(kcorEqSq);
  J3 = STensorOperation::determinantSTensor3(devKcor);

  if (kcorEq > 0)
  {
    double xi, costheta, sintheta;
    xi = 27.*J3/(2.*kcorEq*kcorEqSq);
    cosLodeAngle_shear(_sinSmmoothFactor,xi,costheta);
    sinLodeAngle_shear(_sinSmmoothFactor,xi,sintheta);
    s = kcorEq*(sintheta/2. + costheta*sqrt(3.)/2.);
  }
  else
  {
    s = 0.;
  }

  double Cft, DCftDChi, DCftDW;
  _coalescenceLaw->computeConcentrationFactor(q0,q1,Cft,diff,&DCftDChi,&DCftDW);
  
  const IPCoalescence* ipcoales = &q1->getConstRefToIPCoalescence();
  double CftOffset = ipcoales->getCrackOffsetOnCft();
  double yieldOffset = ipcoales->getYieldOffset();
  
  double factF = 1./(CftOffset*Cft*R);

  if (diff)
  {
    ////
    this->I1J2J3_getYieldNormal(*DFDkcor,kcor,R,q0,q1,T);
    *DFDkcor *= (1./R0);

    *DFDR = -s*factF/R;
    for (int i=0; i< DFDY->size(); i++)
    {
      (*DFDY)[i] = 0.;
    }
    (*DFDY)[1] = (-s*factF/Cft)*DCftDChi;
    (*DFDY)[2] = (-s*factF/Cft)*DCftDW;

    if (withThermic)
    {
      *DFDT = 0.;
    }
  }
  return s*factF- 1. - yieldOffset;
};
