//
// Created by vinayak on 24.08.22.
//
//
// C++ Interface: material law for inductor region in ElecMag problem
// computing normal direction to a gauss point to impose current
// density js0 in inductor
//
// Author:  <Vinayak GHOLAP>, (C) 2022
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef MLAWELECMAGINDUCTOR_H_
#define MLAWELECMAGINDUCTOR_H_
#include "mlawElecMagGenericThermoMech.h"
#include "ipElecMagInductor.h"

class mlawElecMagInductor : public mlawElecMagGenericThermoMech
{
protected:
    SVector3 _Centroid;
    SVector3 _CentralAxis;

public:
    mlawElecMagInductor(const int num,
                        const double alpha, const double beta, const double gamma,
                        const double lx,const double ly,const double lz,
                        const double seebeck,const double v0,
                        const double mu_x, const double mu_y, const double mu_z,
                        const double A0_x, const double A0_y, const double A0_z,
                        const double Irms, const double freq, const unsigned int nTurnsCoil,
                        const double coilLength_x, const double coilLength_y, const double coilLength_z,
                        const double coilWidth,
                        const double Centroid_x, const double Centroid_y, const double Centroid_z,
                        const double CentralAxis_x, const double CentralAxis_y, const double CentralAxis_z,
                        const bool useFluxT, const bool evaluateCurlField = true);
#ifndef SWIG
    mlawElecMagInductor(const mlawElecMagInductor &source);
    mlawElecMagInductor& operator=(const materialLaw &source);
    virtual ~mlawElecMagInductor(){}

    virtual materialLaw* clone() const {return new mlawElecMagInductor(*this);}
    virtual bool withEnergyDissipation() const {return false;}

    // function of materialLaw
    virtual matname getType() const{return materialLaw::ElecMagInductor;}
    virtual void createIPState(IPStateBase* &ips, bool hasBodyForce, const bool* state_=NULL,const MElement *ele=NULL,
                               const int nbFF_=0, const IntPt *GP=NULL, const int gpt =0) const;

    virtual void createIPVariable(IPElecMagInductor* &ipv, bool hasBodyForce, const MElement *ele, const int nbFF,
                                  const IntPt *GP, const int gpt) const;

    virtual void getNormalDirection(SVector3 &normal, const IPElecMagGenericThermoMech *q1) const
    {
        const IPElecMagInductor *q = dynamic_cast<const IPElecMagInductor*>(q1);
        if(q != NULL)
        {
            q->getNormalDirection(normal);
        }
    }

    SVector3 getCentroid() const { return _Centroid;}
    SVector3 getCentralAxis() const { return _CentralAxis;}
    virtual bool isInductor() const {return true;}

    double getInitialVoltage() const {return mlawElecMagGenericThermoMech::getInitialVoltage();}
    SVector3 getInitialMagPotential() const {return mlawElecMagGenericThermoMech::getInitialMagPotential();}
#endif //SWIG
};

#endif //MLAWELECMAGINDUCTOR_H_
