//
// C++ Interface: material law
//
// Description: j2 thermo-elasto-plastic law with non local damage interface
//
// Author:  <L. Noels>, (C) 2021
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef MLAWNONLOCALDAMAGEJ2FULLYCOUPLEDTHERMOMECHANICS_H_
#define MLAWNONLOCALDAMAGEJ2FULLYCOUPLEDTHERMOMECHANICS_H_
#include "mlawJ2FullyCoupledThermoMechanics.h"
#include "ipNonLocalDamageJ2FullyCoupledThermoMechanics.h"
#include "j2IsotropicHardening.h"
#include "CLengthLaw.h"
#include "DamageLaw.h"

class mlawNonLocalDamageJ2FullyCoupledThermoMechanics : public mlawJ2FullyCoupledThermoMechanics
{
 protected:
  CLengthLaw *cLLaw;
  DamageLaw  *damLaw;
  //STensor43   Cel;
 public:
  mlawNonLocalDamageJ2FullyCoupledThermoMechanics(const int num,const double E,const double nu, const double rho,
				const double sy0,const double h, const CLengthLaw &_cLLaw,
                                const DamageLaw &_damLaw,
				const double tol=1.e-6, const bool matrixbyPerturbation = false, const double pert = 1e-8);
 #ifndef SWIG
  mlawNonLocalDamageJ2FullyCoupledThermoMechanics(const mlawNonLocalDamageJ2FullyCoupledThermoMechanics &source);
  mlawNonLocalDamageJ2FullyCoupledThermoMechanics& operator=(const materialLaw &source);
  virtual ~mlawNonLocalDamageJ2FullyCoupledThermoMechanics()
  {
    if (cLLaw!= NULL) delete cLLaw;
    if (damLaw!= NULL) delete damLaw;

  }
  virtual materialLaw* clone() const {return new mlawNonLocalDamageJ2FullyCoupledThermoMechanics(*this);}
  // function of materialLaw
  virtual matname getType() const{return materialLaw::nonLocalDamageJ2FullyCoupledThermoMechanics;}
  virtual bool withEnergyDissipation() const {return true;};
  virtual void createIPState(IPNonLocalDamageJ2FullyCoupledThermoMechanics *ivi, IPNonLocalDamageJ2FullyCoupledThermoMechanics *iv1, 
                                                                                 IPNonLocalDamageJ2FullyCoupledThermoMechanics *iv2) const;
  virtual void createIPState(IPStateBase* &ips, bool hasBodyForce, const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const IntPt *GP=NULL, const int gpt = 0) const;
  virtual void createIPVariable(IPNonLocalDamageJ2FullyCoupledThermoMechanics *&ipv) const;

  virtual void initLaws(const std::map<int,materialLaw*> &maplaw){}; // this law is initialized so nothing to do
  virtual double soundSpeed() const; // default but you can redefine it for your case
  virtual const CLengthLaw *getCLengthLaw() const {return cLLaw; };
  virtual const DamageLaw *getDamageLaw() const {return damLaw; };
  virtual double scaleFactor() const;
  virtual void evaluateCel(STensor43 &Cel, double T) const;
  // specific function
 public:
  
/**
   * @brief Material constitutive interface function associated with the non-local thermomechanical case in the dG3D interface.
   * whatever the number of the non-local variables.
   * @param F0 (input @ time n)     the initial deformation gradient.
   * @param F1 (input @ time n+1)   the updated deformation gradient.
   * @param P (output)              the 1st Piola-Kirchhoff stress tensor updated after computation.
   * @param q0 (input @ tine n+1)   the initial array of internal variables.
   * @param q1 (output)             the updated array of internal variables updated after computation (including the output local variables).
   * @param Tangent (output)               the tangent quantities d_P/dF.
   * @param dLocalVarDStrain (output)      the tangent quantities d_v_loc/dF.
   * @param dStressDNonLocalVar (output)   the tangent quantities d_P/dv_nonloc.
   * @param dLocalVarDNonLocalVar (output) the tangent quantities d_v_loc/dv_nonloc.
   * @param stiff (input options)          is true if the tangent computation is required.
   * 
   * @param dPdT (output)                  the tangent quantities d_P/dT.
   * @param dPdgradT (output)              the tangent quantities d_P/dgradT.
   * @param dLocalVarDT (output)           the tangent quantities d_v_loc/dT.
   * @param T0 (input @ time n)            the initial temperature.
   * @param T1 (input @ time n+1)          the updated temperature.
   * @param gradT0 (input @ time n)        the initial temperature gradient.
   * @param gradT1 (input @ time n+1)      the updated temperature gradient.
   * @param fluxT (output)                 the heat flux
   * @param dfluxTdF (output)              the tangent quantities d_heatflux/dF.
   * @param dfluxTdNonLocalVar (output)    the tangent quantities d_heatflux/dv_nonloc.
   * @param dfluxTdT (output)              the tangent quantities d_heatflux/dT.
   * @param dfluxTdgradT (output)          the tangent quantities d_heatflux/dgradT.
   * @param thermalSource (output)                  the thermal source ?????????????.
   * @param dthermalSourcedF (output)               the tangent quantities d_termsource/dF.
   * @param dthermalSourcedNonLocalVar (output)     the tangent quantities d_termsource/dv_nonloc.
   * @param dthermalSourcedT (output)               the tangent quantities d_termsource/dT.
   * @param dthermalSourcedgradT (output)           the tangent quantities d_termsource/dgradT.
   * @param mechanicalSource (output)               the mechanical dissipation converted into heat source.
   * @param dmechanicalSourceF (output)             the tangent quantities d_dissip/dF.
   * @param dmechanicalSourcedNonLocalVar (output)  the tangent quantities d_dissip/dv_nonloc.
   * @param dmechanicalSourcedT (output)            the tangent quantities d_dissip/dT.
   * @param dmechanicalSourcedgradT (output)        the tangent quantities d_dissip/dgradT.
   * @param elasticTangent (optional output)        
   */
  virtual void constitutive(  const STensor3 &F0, const STensor3 &Fn, STensor3 &P,
                              const IPVariable * q0,IPVariable * q1,                          
                              STensor43 &Tangent, STensor3 &dLocalVarDStrain,
                              STensor3 &dStressDNonLocalVar,
                              double &dLocalVarDNonLocalVar,
                              const bool stiff,
                              STensor3 &dPdT, STensor33 &dPdgradT,  double &dLocalVarDT,
                              
                              const double &T0, const double &Tn, 
                              const SVector3 &gradT0, const SVector3 &gradT,
                              
                              SVector3 &fluxT,
                              STensor33 &dfluxTdF, SVector3 &dfluxTdNonLocalVar,
                              SVector3  &dfluxTdT, STensor3 &dfluxTdgradT,
                              
                              double &thermalSource, 
                              STensor3 &dthermalSourcedF, double &dthermalSourcedNonLocalVar,
                              double &dthermalSourcedT, SVector3 &dthermalSourcedgradT,
          
                              double &mechanicalSource,
                              STensor3 &dmechanicalSourceF, double &dmechanicalSourcedNonLocalVar,
                              double &dmechanicalSourcedT, SVector3 &dmechanicalSourcedgradT,
                           
                              STensor43* elasticTangent = NULL
                          ) const;

  virtual void predictorCorrector(
                              const STensor3 &F0, const STensor3 &Fn, STensor3 &P,
                              const IPVariable * q0i,IPVariable * q1i,                          
                              STensor43 &Tangent, 
                              STensor43 &dFpdF, // plastic tangent
                              STensor3 &dFpdT, // plastic tangent
                              STensor43 &dFedF, // elastic tangent
                              STensor3 &dFedT, // elastic tangent
                              STensor3 &dLocalPlasticStrainDStrain,
                              STensor3 &dStressDNonLocalPlasticStrain,
                              double &dLocalPlasticStrainDNonLocalPlasticStrain,
                              const bool stiff,
                              STensor3 &dPdT, STensor33 &dPdgradT,  double &dLocalPlasticStrainDT,
                           
                              const double &T0, const double &Tn, 
                              const SVector3 &gradT0, const SVector3 &gradT,
                              
                              SVector3 &fluxT,
                              STensor33 &dfluxTdF, SVector3 &dfluxTdNonLocalPlasticStrain,
                              SVector3  &dfluxTdT, STensor3 &dfluxTdgradT,
                              
                              double &thermalSource, 
                              STensor3 &dthermalSourcedF, double &dthermalSourcedNonLocalPlasticStrain,
                              double &dthermalSourcedT, SVector3 &dthermalSourcedgradT,
          
                              double &mechanicalSource,
                              STensor3 &dmechanicalSourceF, double &dmechanicalSourcedNonLocalPlasticStrain,
                              double &dmechanicalSourcedT, SVector3 &dmechanicalSourcedgradT,
                              STensor43* elasticTangent 
                           ) const;

  protected:

 #endif // SWIG
  
};

#endif //MLAWNONLOCALDAMAGEJ2FULLYCOUPLEDTHERMOMECHANICS_H_  
