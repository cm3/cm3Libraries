//
// C++ Interface: material law
//
// Description: UMAT interface for VEVP model
//
//
// Author:  <V.D. NGUYEN>, (C) 2023
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef MLAWVEVPUMAT_H_
#define MLAWVEVPUMAT_H_
#include "mlawUMATInterface.h"
#include "ipVEVPUMAT.h"

class mlawVEVPUMAT : public mlawUMATInterface
{

 public:
  mlawVEVPUMAT(const int num, const char *propName);

 #ifndef SWIG
  mlawVEVPUMAT(const mlawVEVPUMAT &source);
  mlawVEVPUMAT& operator=(const materialLaw &source);
  virtual ~mlawVEVPUMAT();
  virtual materialLaw* clone() const {return new mlawVEVPUMAT(*this);};
  virtual void checkInternalState(IPVariable* ipv, const IPVariable* ipvprev) const{}; // do nothing
  // function of materialLaw
  virtual matname getType() const{return materialLaw::VEVPUMAT;}

  virtual void createIPState(IPStateBase* &ips, bool hasBodyForce, const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const IntPt *GP=NULL, const int gpt = 0) const
  {
     Msg::Error("Cannot be called");
  }

  virtual void createIPState(IPVEVPUMAT *ivi, IPVEVPUMAT *iv1, IPVEVPUMAT *iv2) const;
  virtual void createIPVariable(IPVEVPUMAT *ipv,bool hasBodyForce, const MElement *ele,const int nbFF, const IntPt *GP, const int gpt) const;
  virtual void initLaws(const std::map<int,materialLaw*> &maplaw){}; // this law is initialized so nothing to do

  virtual void callUMAT(double *stress, double *statev, double **ddsdde, double &sse, double &spd, double &scd, double &rpl,
                                 double *ddsddt, double *drplde, double &drpldt, double *stran, double *dtsran,
                                 double *tim, double timestep, double temperature, double deltaTemperature, double *predef, double *dpred,
                                 const char *CMNAME, int &ndi, int &nshr, int tensorSize,
                                 int statevSize, double *prs, int matPropSize, double *coords, double **dRot,
                                 double &pnewdt, double &celent, double **F0, double **F1,
                                 int &noel, int &npt, int &layer, int &kspt, int &kstep, int &kinc) const;

  virtual const char* getCMNAME() const {return "VEVP";}
  virtual double getDensity() const {return _rho;}

 #endif // SWIG
};

#endif // MLAWVEVPUMAT_H_
