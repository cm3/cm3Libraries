//
// C++ Interface: material law
//              Anisotropic law for large strains it is a "virtual implementation" you have to define a law
//              in your project which derive from this law. you have to do THE SAME for your IPVariable
//              which have to derive from IPAnisotropic
//
//              Material properties defined by the location of Gauss Point
// Author:  <Ling Wu >, (C) 2017
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef MLAWANISOTROPICSTOCH_H_
#define MLAWANISOTROPICSTOCH_H_
#include "mlaw.h"
#include "fullMatrix.h"
#include "STensor3.h"
#include "STensor43.h"
#include "ipAnisotropicStoch.h"


class mlawAnisotropicStoch : public materialLaw
{
 protected:
  double _rho;    // density
  double _poissonMax;
  double _alpha;   // parameter of anisotropy
  double _beta;   // parameter of anisotropy
  double _gamma;   // parameter of anisotropy
  int _intpl;
  STensor43 _ElasticityTensor;

  fullMatrix<double> _ExMat;
  fullMatrix<double> _EyMat;
  fullMatrix<double> _EzMat;

  fullMatrix<double> _VxyMat;
  fullMatrix<double> _VxzMat;
  fullMatrix<double> _VyzMat;

  fullMatrix<double> _MUxyMat;
  fullMatrix<double> _MUxzMat;
  fullMatrix<double> _MUyzMat;

  double _dx, _dy;
  double _OrigX, _OrigY;

 protected:
  virtual void AnisoElasticTensor(const double Ex, const double Ey, const double Ez, const double Vxy, const double Vxz, const double Vyz,
                                 const double MUxy, const double MUxz, const double MUyz, const double alpha, const double beta,
                                 const double gamma, STensor43 &ElastTensor) const;

 public:
  mlawAnisotropicStoch(const int num, const double rho, const double alpha, const double beta, const double gamma, const double OrigX, const double OrigY, const char *propName, const int intpl);

 #ifndef SWIG
  mlawAnisotropicStoch(const mlawAnisotropicStoch &source);
  mlawAnisotropicStoch& operator=(const materialLaw &source);
  virtual ~mlawAnisotropicStoch(){}

  virtual materialLaw* clone() const {return new mlawAnisotropicStoch(*this);}
  virtual void checkInternalState(IPVariable* ipv, const IPVariable* ipvprev) const{}; // do nothing
  virtual bool withEnergyDissipation() const {return false;};

  // function of materialLaw
  virtual matname getType() const{return materialLaw::AnisotropicStoch;}
  virtual void createIPState(IPStateBase* &ips,bool hasBodyForce, const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const IntPt *GP=NULL, const int gpt =0) const;
  virtual void initLaws(const std::map<int,materialLaw*> &maplaw){}; // this law is initialized so nothing to do
  virtual double soundSpeed() const; // default but you can redefine it for your case
  virtual double poissonRatio() const;
  virtual double shearModulus() const;
  virtual double scaleFactor() const {return shearModulus();};

  const fullMatrix<double> & getExMat() const {return _ExMat;}
  const fullMatrix<double> & getEyMat() const {return _EyMat;}
  const fullMatrix<double> & getEzMat() const {return _EzMat;}

  const fullMatrix<double> & getVxyMat() const {return _VxyMat;}
  const fullMatrix<double> & getVxzMat() const {return _VxzMat;}
  const fullMatrix<double> & getVyzMat() const {return _VyzMat;}

  const fullMatrix<double> & getMUxyMat() const {return _MUxyMat;}
  const fullMatrix<double> & getMUxzMat() const {return _MUxzMat;}
  const fullMatrix<double> & getMUyzMat() const {return _MUyzMat;}

  int getInterpoly() const {return _intpl;}

  double getOrigX() const { return _OrigX;}
  double getOrigY() const { return _OrigY;}
  double getDeltaX() const { return _dx;}
  double getDeltaY() const { return _dy;}

  virtual void constitutive(
                            const STensor3& F0,         // initial deformation gradient (input @ time n)
                            const STensor3& Fn,         // updated deformation gradient (input @ time n+1)
                            STensor3 &P,                // updated 1st Piola-Kirchhoff stress tensor (output)
                                                        // contains the initial values on input
                            const IPVariable *q0,       // array of initial internal variable
                            IPVariable *q1,              // updated array of internal variable (in ipvcur on output),
			    STensor43 &Tangent,         // constitutive tangents (output)
			    const bool stiff,
			    STensor43* elasticTangent = NULL, 
                            const bool dTangent =false,
                            STensor63* dCalgdeps = NULL) const;

  const STensor43& getElasticityTensor() const { return _ElasticityTensor;};

 protected:
  virtual double deformationEnergy(const STensor3 &defo, const STensor43& ElastTensor) const ;
  virtual double deformationEnergy(const STensor3 &defo, const STensor3& sigma) const ;

 #endif // SWIG
};

#endif // MLAWANISOTROPICSTOCH_H_
