// C++ Interface: material law
//              Generic Thermo-Mechanical law
// Author:  <Vinayak GHOLAP>, (C) 2022
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef MLAWGENERICTM_H_
#define MLAWGENERICTM_H_

#include "mlaw.h"
#include "STensor3.h"
#include "STensor43.h"
#include "STensor63.h"
#include "scalarFunction.h"
#include "ipGenericTM.h"
#include "mlawCoupledThermoMechanics.h"


class mlawGenericTM : public  mlawCoupledThermoMechanics {
protected:
   //mechanical
    materialLaw *_mecaLaw;

  //the Thermal and TM
    double _alpha; // parameter of anisotropy
    double _beta; // parameter of anisotropy
    double _gamma; // parameter of anisotropy
    double _cv;
    double _t0;
    double _Kx;
    double _Ky;
    double _Kz;
    double _alphax;
    double _alphay;
    double _alphaz;

    scalarFunction* _cp; // function of temperature

    STensor3 _k,_alphaDilatation;

    bool applyReferenceF;

public:
    mlawGenericTM(const int num, const bool init = true);
    mlawGenericTM(const int num,
                  const double alpha, const double beta, const double gamma,
                  const double t0, const double Kx, const double Ky,
                  const double Kz, const double alphax, const double alphay,
                  const double alphaz, const double cp);
    virtual void setLawForCp(const scalarFunction& funcCp);
    const materialLaw &getConstRefMechanicalMaterialLaw() const
    {
        if(_mecaLaw == NULL)
            Msg::Error("getConstRefMechanicalMaterialLaw: Mechanic law is null");
        return *_mecaLaw;
    }
    materialLaw &getRefMechanicalMaterialLaw()
    {
        if(_mecaLaw == NULL)
            Msg::Error("getRefMechanicalMaterialLaw: Mechanic law is null");
        return *_mecaLaw;
    }
    virtual void setMechanicalMaterialLaw(const materialLaw *mlaw)
    {
        if(_mecaLaw != NULL)
        {
            delete _mecaLaw;
            _mecaLaw = NULL;
        }
        _mecaLaw=mlaw->clone();

    }
    void setApplyReferenceF(const bool rf)
    {
        applyReferenceF=rf;
    }
    virtual void setTime(const double ctime,const double dtime){
        mlawCoupledThermoMechanics::setTime(ctime,dtime);
        if(_mecaLaw == NULL)
            Msg::Error("setTime: Mechanic law is null");
        _mecaLaw->setTime(ctime,dtime);
    }
    virtual void setMacroSolver(const nonLinearMechSolver* sv)
    {
        mlawCoupledThermoMechanics::setMacroSolver(sv);
        if(_mecaLaw == NULL)
            Msg::Error("setMacroSolver: Mechanic law is null");
        _mecaLaw->setMacroSolver( sv);
    }
#ifndef SWIG
    virtual ~mlawGenericTM()
    {
        if(_mecaLaw != NULL)
        {
            delete _mecaLaw;
            _mecaLaw = NULL;
        }
        if (_cp != NULL)
        {
            delete _cp;
            _cp = NULL;
        }
    }
    mlawGenericTM(const mlawGenericTM& source);
    virtual mlawGenericTM& operator=(const materialLaw& source);
    virtual void initLaws(const std::map<int,materialLaw*> &maplaw){}; // this law is initialized so nothing to do
    virtual double soundSpeed() const{
        if(_mecaLaw == NULL)
            Msg::Error("soundSpeed: Mechanic law is null");
        return _mecaLaw->soundSpeed();
    };
    virtual double density() const{
        if(_mecaLaw == NULL)
            Msg::Error("density: Mechanic law is null");
        return _mecaLaw->density();
    };

    virtual double ElasticShearModulus() const{
        if(_mecaLaw == NULL)
            Msg::Error("ElasticShearModulus: Mechanic law is null");
        return _mecaLaw->getConstNonLinearSolverMaterialLaw()->ElasticShearModulus();
    }
    virtual void checkInternalState(IPVariable* ipv, const IPVariable* ipvprev) const{}; // do nothing
    virtual materialLaw* clone() const {return new mlawGenericTM(*this);}
    virtual bool withEnergyDissipation() const {return false;}
    virtual matname getType() const{return materialLaw::GenericThermoMechanics;}
    virtual void createIPState(IPStateBase* &ips,bool hasBodyForce, const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0,
                               const IntPt *GP=NULL, const int gpt =0) const;
    virtual void createIPVariable(IPGenericTM* &ipv, bool hasBodyForce, const MElement *ele, const int nbFF,
                                  const IntPt *GP, const int gpt) const;

    virtual void constitutive(
            const STensor3      &F0,                   // initial deformation gradient (input @ time n)
            const STensor3      &Fn,                   // updated deformation gradient (input @ time n+1)
            STensor3            &P,                    // updated 1st Piola-Kirchhoff stress tensor (output)
            const IPVariable    *q0,                   // array of initial internal variable
            IPVariable          *q1,                   // updated array of internal variable (in ipvcur on output),
            STensor43           &Tangent,              // constitutive tangents (output)
            const bool          stiff,
            STensor43           *elasticTangent=NULL,
            const bool dTangent =false,
            STensor63* dCalgdeps = NULL
    ) const
    {
        if(_mecaLaw == NULL)
            Msg::Error("Mechanic law is null");
        _mecaLaw->constitutive(F0, Fn, P, &(dynamic_cast<const IPGenericTM*>(q0)->getConstRefToIpMeca()),
                               &(dynamic_cast<IPGenericTM*>(q1)->getRefToIpMeca()),
                               Tangent, stiff, elasticTangent, dTangent, dCalgdeps);
    };

    virtual void constitutive(
            const STensor3   &F0,
            const STensor3   &Fn,
            STensor3         &P,
            const STensor3   &P0,
            const IPVariable *q0,
            IPVariable       *q1,
            STensor43        &Tangent,
            const double     T0,
            double           T,
            const SVector3   &gradT,
            SVector3         &fluxT,
            STensor3         &dPdT,
            STensor3         &dqdgradT,
            SVector3         &dqdT,
            STensor33        &dqdF,
            const bool       stiff,
            double           &wth,
            double           &dwthdt,
            STensor3         &dwthdF,
            double           &mechSource,
            double           &dmechSourcedT,
            STensor3         &dmechSourcedF,
            STensor43        *elasticTangent=NULL
    ) const;

    //const STensor43& getElasticityTensor() const { return _ElasticityTensor;};
    virtual const STensor3&  getConductivityTensor() const { return _k;};
    virtual const STensor3& getAlphaDilatation() const { return _alphaDilatation;};
    //virtual void getStiff_alphaDilatation(STensor3 &stiff_alphaDilatation) const;
    virtual void ElasticStiffness(STensor43 *elasticStiffness) const;

    virtual double getInitialTemperature() const {return _t0;};
    virtual double getInitialExtraDofStoredEnergyPerUnitField() const {return getExtraDofStoredEnergyPerUnitField(_t0);}
    virtual double getExtraDofStoredEnergyPerUnitField(double T) const {return _cp->getVal(T);};
#endif //SWIG
};

#endif //MLAWGENERICTM_H_
