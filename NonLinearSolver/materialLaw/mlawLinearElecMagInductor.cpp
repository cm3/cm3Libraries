//
// C++ Interface: material law for inductor region in ElecMag problem
// computing normal direction to a gauss point to impose current
// density js0 in inductor
//
//
// Author:  <Vinayak GHOLAP>, (C) 2020
//
// Copyright: See COPYING file that comes with this distribution
//
//

#include "mlawLinearElecMagInductor.h"
#include "MInterfaceElement.h"

mlawLinearElecMagInductor::mlawLinearElecMagInductor(int num,const double rho, const double Ex, const double Ey, const double Ez,
  const double Vxy, const double Vxz, const double Vyz, const double MUxy, const double MUxz,
  const double MUyz,  const double alpha, const double beta, const double gamma,
  const double t0,const double Kx,const double Ky, const double Kz,const double alphax,
  const double alphay,const double alphaz, const  double lx,const  double ly,
  const  double lz,const double seebeck,const double cp,const double v0,
  const double mu_x, const double mu_y, const double mu_z, const double A0_x, const double A0_y,
  const double A0_z, const double Irms, const double freq, const unsigned int nTurnsCoil, 
  const double coilLength_x, const double coilLength_y, const double coilLength_z, const double coilWidth, 
  const double Centroid_x, const double Centroid_y, const double Centroid_z, 
  const double CentralAxis_x, const double CentralAxis_y, const double CentralAxis_z, const bool useFluxT,
  const bool evaluateCurlField)
: mlawLinearElecMagTherMech(num, rho, Ex, Ey, Ez, Vxy, Vxz, Vyz, MUxy, MUxz, MUyz,
  alpha, beta, gamma, t0, Kx, Ky, Kz, alphax, alphay, alphaz, lx, ly, lz,
  seebeck, cp, v0, mu_x, mu_y, mu_z, A0_x, A0_y, A0_z, Irms, freq, nTurnsCoil,
  coilLength_x, coilLength_y, coilLength_z, coilWidth, useFluxT,
  evaluateCurlField),
  _Centroid(Centroid_x, Centroid_y, Centroid_z), _CentralAxis(CentralAxis_x, CentralAxis_y, CentralAxis_z),
  _useFluxT(useFluxT), _evaluateCurlField(evaluateCurlField)
{
  // Compute the magnitude of imposed current density in inductor
  // js0 = sqrt(2)*Irms*nTurns/crossSecArea
  const double crossSectionCoil = coilLength_y * coilWidth; // Assumed coil cross section is rectangular block
  if(crossSectionCoil != 0.0)  this->_js0 = std::sqrt(2) * Irms * double(nTurnsCoil)/crossSectionCoil;
  else this->_js0 = 0.0;
}

mlawLinearElecMagInductor::mlawLinearElecMagInductor(const mlawLinearElecMagInductor &source) 
: mlawLinearElecMagTherMech(source), _Centroid(source._Centroid), _CentralAxis(source._CentralAxis),
_useFluxT(source._useFluxT),_evaluateCurlField(source._evaluateCurlField){}

mlawLinearElecMagInductor& mlawLinearElecMagInductor::operator=(const materialLaw &source)
{
  mlawLinearElecMagTherMech::operator=(source);
  const mlawLinearElecMagInductor* src =static_cast<const mlawLinearElecMagInductor*>(&source);
  _Centroid = src->_Centroid;
  _CentralAxis = src->_CentralAxis;
  _useFluxT = src->_useFluxT;
  _evaluateCurlField = src->_evaluateCurlField;
  return *this;
}

void mlawLinearElecMagInductor::createIPState(IPStateBase* &ips, bool hasBodyForce,const bool* state_,const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
  bool inter=true;
  const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
  if(iele==NULL) inter=false;

  SPoint3 pt_Global;
  ele->pnt(GP->pt[0],GP->pt[1],GP->pt[2],pt_Global);
  SVector3 GaussP(pt_Global);

  IPVariable* ipvi = new IPLinearElecMagInductor(GaussP, _Centroid, _CentralAxis);
  IPVariable* ipv1 = new IPLinearElecMagInductor(GaussP, _Centroid, _CentralAxis);
  IPVariable* ipv2 = new IPLinearElecMagInductor(GaussP, _Centroid, _CentralAxis);
  if(ips != NULL) delete ips;
  ips = new IP3State(state_,ipvi,ipv1,ipv2);
}

