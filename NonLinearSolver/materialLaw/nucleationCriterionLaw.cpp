//
//
// Description: Provide criterion class for nucleation 
//
//
// Author:  <J. Leclerc>, (C) 2019
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "nucleationCriterionLaw.h"
#include "mlawNonLocalPorous.h"
#include "ipNonLocalPorosity.h"

// NucleationCriterionLaw
// -------------------------------------------
// Constructors and destructors
// ------------
NucleationCriterionLaw::NucleationCriterionLaw() : 
  num_(-1), isInitialized_(false),
  mlawPorous_(NULL)
{
  index_inihibitableLaw_.clear();
  Msg::Error( "NucleationCriterionLaw :: default constructor should never be called." );
};


NucleationCriterionLaw::NucleationCriterionLaw( const int num ) :
  num_(num), isInitialized_(false),
  mlawPorous_(NULL)
{
  index_inihibitableLaw_.clear();
  // Nothing more to do
};


NucleationCriterionLaw::NucleationCriterionLaw( const NucleationCriterionLaw &source ) :
  num_( source.num_ ), isInitialized_( source.isInitialized_ ),
  mlawPorous_( source.mlawPorous_ ), index_inihibitableLaw_( source.index_inihibitableLaw_ )
{
  // Nothing more to do.
};


NucleationCriterionLaw& NucleationCriterionLaw::operator=( const NucleationCriterionLaw& source )
{
  /// Copy this class.
  num_ = source.num_;
  isInitialized_ = source.isInitialized_;
  index_inihibitableLaw_ = source.index_inihibitableLaw_;
  mlawPorous_ = source.mlawPorous_;
  
  return *this;
};





// Setting functions (non-const functions)
// ---------------

void NucleationCriterionLaw::setNum( const int num) 
{
  num_ = num;
  Msg::Warning( "NucleationCriterionLaw:: the index of the law now n. ' %d ' have been modified !", num_ );
};


void NucleationCriterionLaw::initCriterionLaw( const mlawNonLocalPorosity* const mlawPorous )
{
  // Add mlaw pointer and check for inconstencies before setting the initialisaiton flag to true.
  
  /// Check if the mlaw pointer is not null before adding it
  if ( mlawPorous == NULL ){
    Msg::Error( "NucleationCriterionLaw::initCriterionLaw : mlawPorous pointer is NULL" );
    isInitialized_ = false;
    return;
  }
  mlawPorous_ = mlawPorous;
  
  
  /// Check for inconsistencies in the parameters.
  isInitialized_ = checkParametersIntegrity();
  
  if ( !isInitialized_ ){
    Msg::Error( "NucleationCriterionLaw::initCriterionLaw: Inconsistencies detected." );
  };
  
};


const bool NucleationCriterionLaw::checkParametersIntegrity() const
{
  // Determine if the law is correctly initialised. 
  // Nothing has to be done inside the base class for now.
  return true;
};




// User-setting functions (non-const functions)
// ---------------
void NucleationCriterionLaw::addAnExclusionRule( const int new_index )
{
  // Check for admissibles datas before adding the value inside the vector.
  if ( new_index < 0 ){
    Msg::Error( "NucleationCriterionLaw::addAnExclusionRule:"
                "index of newly added exclusion rule %d should be >= 0."
                , new_index );
  }
  else if ( new_index == this->getNum() ) {
    Msg::Error( "NucleationCriterionLaw::addAnExclusionRule:" 
                "index of newly added exclusion rule %d should be different "
                "than the identifier of this law.", new_index );
  }
  else if ( this->getType()  == NucleationCriterionLaw::trivialCriterion ){
    Msg::Error( "NucleationCriterionLaw::addAnExclusionRule:" 
                "an exclusion rule is added to the trivial criterion %d, "
                "which has no sense.", new_index );
  }
  else{
    index_inihibitableLaw_.push_back(new_index);
  }
  
};


// Access functions
// ------------
const std::vector<int>& NucleationCriterionLaw::getIndexOfExclusionRules() const 
{
  return index_inihibitableLaw_;
};





// Specific functions
// ------------
void NucleationCriterionLaw::applyCriterionOnsetInIP( IPNonLocalPorosity& q1Porous ) const
{
  // Apply effects of the activation onset of the nucleation in the IPVs which are:
  //  - update the flags (activation and onset) if nothing has already forbidden 
  //    at this time step the activation.
  //  - forbid the nucleation activation of laws gathered in "index_inihibitableLaw_"
  //    excepted if the nucleation was already activated at this time step.
  IPNucleationCriterion& q1i_criterion = q1Porous.getRefToIPNucleation().getRefToIPNucleationCriterion( num_ );
  
  if ( q1i_criterion.isInhibited() ) {
    q1i_criterion.setNucleationOnset( false );
    q1i_criterion.setNucleationActivation( false );
  }
  else {
    q1i_criterion.setNucleationOnset( true );
    q1i_criterion.setNucleationActivation( true );
    
    for (int i = 0; i < index_inihibitableLaw_.size(); i++ ) {
      /// get the i-th ipv
      int index_to_inhibit = index_inihibitableLaw_[i];
      IPNucleationCriterion& q1i_criterion_toblock = q1Porous.getRefToIPNucleation().getRefToIPNucleationCriterion(index_to_inhibit);

      if ( !q1i_criterion_toblock.hasNucleationOnset() ) {
        Msg::Info("Nucleation criterion %d inhibits criterion %d.", num_, index_to_inhibit);
        q1i_criterion_toblock.setNucleationInhibition( true );
      }
      else {
        Msg::Warning( "Nucleation criterion %d wants to inhibit criterion %d but is already activated.", num_, index_to_inhibit );
      }
    }
    
  }
  
};









// TrivialNucleationCriterionLaw
// -------------------------------------------
// Constructors and destructors
// ------------
TrivialNucleationCriterionLaw::TrivialNucleationCriterionLaw( const int num ) :
  NucleationCriterionLaw(num)
{
  
};


TrivialNucleationCriterionLaw::TrivialNucleationCriterionLaw( const TrivialNucleationCriterionLaw& source ) :
  NucleationCriterionLaw(source)
{
  // Nothing more to do.
};


TrivialNucleationCriterionLaw& TrivialNucleationCriterionLaw::operator=( const NucleationCriterionLaw& source )
{
  /// Copy the mother class
  NucleationCriterionLaw::operator=(source);
  
   /// Copy this class
  const TrivialNucleationCriterionLaw* source_casted = dynamic_cast<const TrivialNucleationCriterionLaw*>(&source);
  if ( source_casted != NULL )
  {
    /// Nothing more to do.
  }
  return *this;
};




// General function of materialLaw
// ------------
void TrivialNucleationCriterionLaw::createIPVariable( IPNucleationCriterion* &ipNuclCriterion ) const
{
  // Cleaning the ipv in case of, then create the correct one
  if ( ipNuclCriterion != NULL ) delete ipNuclCriterion;
  ipNuclCriterion = new IPTrivialNucleationCriterion();
  
};




// Specific functions
// ------------
const bool TrivialNucleationCriterionLaw::checkParametersIntegrity() const
{
  return NucleationCriterionLaw::checkParametersIntegrity();
}

void TrivialNucleationCriterionLaw::assessNucleationOnset( double& criterionValue, IPNonLocalPorosity& q1Porous ) const
{
  // Nothing has to be done. The value of the criterion is always negative as it will never switch its state.
  criterionValue = -1.0;
};


void TrivialNucleationCriterionLaw::assessNucleationActivation( double& criterionValue, IPNonLocalPorosity& q1Porous, const IPNonLocalPorosity& q0Porous ) const
{
  // Nothing has to be done. The value of the criterion is always negative as it will never switch its state.
  // IPTrivialNucleationCriterion->hasNucleationActivated() will always be true
  // IPTrivialNucleationCriterion->hasNucleationOnset() will always be true
  // IPTrivialNucleationCriterion->isInhibited() will always be false
  criterionValue = -1.0;
};


void TrivialNucleationCriterionLaw::previousBasedRefresh( IPNucleationCriterion& q1i_Criterion, const IPNucleationCriterion& q0i_Criterion ) const
{
  // Nothing has to be done or updated as nothing is really updated:
  // IPTrivialNucleationCriterion->hasNucleationActivated() will always be true
  // IPTrivialNucleationCriterion->hasNucleationOnset() will always be true
  // IPTrivialNucleationCriterion->isInhibited() will always be false
};


void TrivialNucleationCriterionLaw::updateIfNucleationEnds( IPNucleationCriterion& q1i_Criterion, const IPNonLocalPorosity& q1Porous ) const
{
  // Nothing has to be done.
  // IPTrivialNucleationCriterion->hasNucleationActivated() will always be true
};







// NonTrivialNucleationCriterionLaw
// -------------------------------------------
// Constructors and destructors
// ------------
NonTrivialNucleationCriterionLaw::NonTrivialNucleationCriterionLaw( const int num ) :
  NucleationCriterionLaw(num)
{
  // Nothing more to do.
};


NonTrivialNucleationCriterionLaw::NonTrivialNucleationCriterionLaw( const NonTrivialNucleationCriterionLaw& source ) :
  NucleationCriterionLaw(source)
{
  // Nothing more to do.
};


NonTrivialNucleationCriterionLaw& NonTrivialNucleationCriterionLaw::operator=( const NucleationCriterionLaw& source )
{
  /// Copy mother class.
  NucleationCriterionLaw::operator=(source);
  
  /// Copy this class.
  // const NonTrivialNucleationCriterionLaw* source_casted = dynamic_cast< const NonTrivialNucleationCriterionLaw* >(&source);
  // if ( source_casted != NULL ) {
  //  
  // }
  return *this;
};


void NonTrivialNucleationCriterionLaw::assessNucleationActivation( double& criterionValue, IPNonLocalPorosity& q1Porous, const IPNonLocalPorosity& q0Porous ) const
{

  /// Get the ipvs
  const IPNucleationCriterion& q0i_criterion = q0Porous.getConstRefToIPNucleation().getConstRefToIPNucleationCriterion( num_ );
  
  /// Nucleation criterion or onset needs to be assessed only 
  /// when it is not inhibited and not already onset.
  if ( q0i_criterion.isInhibited() ) {
    criterionValue = -1.0;
    return;
  }
  else if ( q0i_criterion.hasNucleationOnset() ) {
    criterionValue = -1.0;
    return;
  }
  else{
    assessNucleationOnset( criterionValue, q1Porous );
  }
};



// PlasticBasedNucleationCriterionLaw
// -------------------------------------------
// Constructors and destructors
// ------------
PlasticBasedNucleationCriterionLaw::PlasticBasedNucleationCriterionLaw( const int num, const double startingNuclStrain, const double endingNuclStrain) :
  NonTrivialNucleationCriterionLaw(num),
  startingNuclStrain_(startingNuclStrain), endingNuclStrain_(endingNuclStrain)
{
  // Check if values are meaningfull.
  bool isOk = checkParametersIntegrity();
  
  if ( !isOk ) {
    Msg::Error( "PlasticBasedNucleationCriterionLaw:: checkParametersIntegrity has failed." );
  }
};


PlasticBasedNucleationCriterionLaw::PlasticBasedNucleationCriterionLaw( const PlasticBasedNucleationCriterionLaw& source ) :
  NonTrivialNucleationCriterionLaw(source),
  startingNuclStrain_(source.startingNuclStrain_), endingNuclStrain_(source.endingNuclStrain_)
{
  // Nothing more to do.
};


PlasticBasedNucleationCriterionLaw& PlasticBasedNucleationCriterionLaw::operator=( const NucleationCriterionLaw& source )
{
  /// Copy mother class.
  NonTrivialNucleationCriterionLaw::operator=(source);
  
  ///Copy this class.
  const PlasticBasedNucleationCriterionLaw* source_casted = dynamic_cast<const PlasticBasedNucleationCriterionLaw*>(&source);
  if ( source_casted != NULL ) {
    startingNuclStrain_ = source_casted->startingNuclStrain_;
    endingNuclStrain_ = source_casted->endingNuclStrain_;
  }
  return *this;
};



// General function of materialLaw
// ------------
void PlasticBasedNucleationCriterionLaw::createIPVariable( IPNucleationCriterion* &ipNuclCriterion ) const
{
  if( ipNuclCriterion != NULL ) delete ipNuclCriterion;
  ipNuclCriterion = new IPPlasticBasedNucleationCriterion();
};



// Specific functions
// ------------
const bool PlasticBasedNucleationCriterionLaw::checkParametersIntegrity() const 
{
  // Check if values are meaningfull (return false if a problem is encountered)
  // (i.e check from parent classes are satisfied 
  // and values are postive and the starting value should be higher than the ending one).
  if( !NucleationCriterionLaw::checkParametersIntegrity() ){
    return false;
  }
  
  if ( startingNuclStrain_ < 0. ){
    Msg::Error( "PlasticBasedNucleationCriterionLaw::checkParametersIntegrity " 
                "incorrect parameter values: startingNuclStrain_ %e." , startingNuclStrain_);
    return false;
  };
  
  if ( startingNuclStrain_ >= endingNuclStrain_ ){
    Msg::Error( "PlasticBasedNucleationCriterionLaw::checkParametersIntegrity "
                "incorrect parameter values: startingNuclStrain_ >= endingNuclStrain_" );
    return false;
  };
  
  // Return true as no error is detected so far.
  return true;
};


void PlasticBasedNucleationCriterionLaw::assessNucleationOnset( double& criterionValue, IPNonLocalPorosity& q1Porous ) const
{
  // The nucleation starts if p1 is higher than the startingNuclStrain_ threehold value.
  
  //! \fixme Add a line to choose which variables, depending on the nonlocal scheme.
  double p1 = q1Porous.getLocalMatrixPlasticStrain();
  criterionValue = p1 - startingNuclStrain_;
  
};

void PlasticBasedNucleationCriterionLaw::updateIfNucleationEnds( IPNucleationCriterion& q1i_Criterion, const IPNonLocalPorosity& q1Porous ) const
{
  // The nucleation ends if p1 is higher than the endingNuclStrain_ threehold value.
  
  //! \fixme Add a line to choose which variables, depending on the nonlocal scheme.
  double p1 = q1Porous.getLocalMatrixPlasticStrain();
  bool nucleationHasToEnd = ( p1 > endingNuclStrain_ );
  
  if ( nucleationHasToEnd ) {
    q1i_Criterion.setNucleationActivation( false );
  }
  
};





// PorosityBasedNucleationCriterionLaw
// -------------------------------------------
PorosityBasedNucleationCriterionLaw::PorosityBasedNucleationCriterionLaw( const int num, const double startingNuclPorosity, const double endingNuclPorosity) :
  NonTrivialNucleationCriterionLaw(num),
  startingNuclPorosity_(startingNuclPorosity), endingNuclPorosity_(endingNuclPorosity)
{
  // Check if values are meaningfull.
  bool isOk = checkParametersIntegrity();
  
  if ( !isOk ) {
    Msg::Error( "PorosityBasedNucleationCriterionLaw:: checkParametersIntegrity has failed." );
  }
  
};


PorosityBasedNucleationCriterionLaw::PorosityBasedNucleationCriterionLaw( const PorosityBasedNucleationCriterionLaw& source ) :
  NonTrivialNucleationCriterionLaw(source),
  startingNuclPorosity_(source.startingNuclPorosity_), endingNuclPorosity_(source.endingNuclPorosity_)
{
  // Nothing more to do.
};


PorosityBasedNucleationCriterionLaw& PorosityBasedNucleationCriterionLaw::operator=( const NucleationCriterionLaw& source )
{
  /// Copy mother class
  NonTrivialNucleationCriterionLaw::operator=(source);
  
  /// Copy this class
  const PorosityBasedNucleationCriterionLaw* source_casted = dynamic_cast<const PorosityBasedNucleationCriterionLaw*>(&source);
  if( source_casted != NULL )
  {
    startingNuclPorosity_ = source_casted->startingNuclPorosity_;
    endingNuclPorosity_ = source_casted->endingNuclPorosity_;
  }
  return *this;
};


void PorosityBasedNucleationCriterionLaw::createIPVariable( IPNucleationCriterion* &ipNuclCriterion ) const
{
  if ( ipNuclCriterion != NULL ) delete ipNuclCriterion;
  ipNuclCriterion = new IPPorosityBasedNucleationCriterion();
};


// Specific functions
// ------------
const bool PorosityBasedNucleationCriterionLaw::checkParametersIntegrity() const 
{
  // Check if values are meaningfull (return false if a problem is encountered)
  // (i.e check from parent classes are satisfied 
  // and values are postive and the starting value should be higher than the ending one).
  if( !NucleationCriterionLaw::checkParametersIntegrity() ){
    return false;
  }
  
  if ( startingNuclPorosity_ < 0. ){
    Msg::Error( "PorosityBasedNucleationCriterionLaw::checkParametersIntegrity " 
                "incorrect parameter values: startingNuclPorosity_ %e." , startingNuclPorosity_);
    return false;
  };
  
  if ( startingNuclPorosity_ >= endingNuclPorosity_ ){
    Msg::Error( "PorosityBasedNucleationCriterionLaw::checkParametersIntegrity "
                "incorrect parameter values: startingNuclPorosity_ >= endingNuclPorosity_" );
    return false;
  };
  
  // Return true as no error is detected so far.
  return true;
};


void PorosityBasedNucleationCriterionLaw::assessNucleationOnset( double& criterionValue, IPNonLocalPorosity& q1Porous ) const
{
  // The nucleation starts if fv1 is higher than the startingNuclStrain_ threehold value. 
  // The adequate value is provided by mlawPorous.
  double fV1 = q1Porous.getYieldPorosity();
  criterionValue = fV1 - startingNuclPorosity_;
};

void PorosityBasedNucleationCriterionLaw::updateIfNucleationEnds( IPNucleationCriterion& q1i_Criterion, const IPNonLocalPorosity& q1Porous ) const
{
  // The nucleation ends if fv1 is higher than the endingNuclPorosity_ threehold value.
  double fV1 = q1Porous.getYieldPorosity();
  bool nucleationHasToEnd = ( fV1 > endingNuclPorosity_ );
  if ( nucleationHasToEnd ) {
    q1i_Criterion.setNucleationActivation( false );
  }
};








// BereminNucleationCriterionLaw
// -------------------------------------------
BereminNucleationCriterionLaw::BereminNucleationCriterionLaw(const int num, 
                  const double sigma_cx, const double sigma_cy, const double sigma_cz, 
                  const double kx, const double ky, const double kz ):
    NonTrivialNucleationCriterionLaw(num),
    _criticalStress(0.),
    _stressConcentrationTensor(0.),
    dirAmplification_(0.)
{
  // Set the stress concentration tensor to 0 and set the diagonal values.
  _criticalStress *= 0.;
  _criticalStress(0,0) = sigma_cx;
  _criticalStress(1,1) = sigma_cy;
  _criticalStress(2,2) = sigma_cz;
  
  _stressConcentrationTensor *= 0.;
  _stressConcentrationTensor(0,0) = kx;
  _stressConcentrationTensor(1,1) = ky;
  _stressConcentrationTensor(2,2) = kz;
  
  dirAmplification_ *= 0.0;
  dirAmplification_(0,0) = 1.0;
  dirAmplification_(1,1) = 1.0;
  dirAmplification_(2,2) = 1.0;
};


BereminNucleationCriterionLaw::BereminNucleationCriterionLaw( const BereminNucleationCriterionLaw &source ) :
    NonTrivialNucleationCriterionLaw(source),
    _criticalStress(source._criticalStress),
    _stressConcentrationTensor(source._stressConcentrationTensor),
    dirAmplification_(source.dirAmplification_)
{

};


BereminNucleationCriterionLaw& BereminNucleationCriterionLaw::operator=( const NucleationCriterionLaw & source )
{
  /// Copy mother class.
  NonTrivialNucleationCriterionLaw::operator=( source );
  
  /// Copy this class.
  const BereminNucleationCriterionLaw* source_casted = dynamic_cast< const BereminNucleationCriterionLaw* >( &source );
  if( source_casted != NULL )
  {
    _criticalStress = source_casted->_criticalStress;
    _stressConcentrationTensor = source_casted->_stressConcentrationTensor;
    dirAmplification_ = source_casted->dirAmplification_;
    
  }
  return *this;
};


NucleationCriterionLaw* BereminNucleationCriterionLaw::clone() const
{
  return new BereminNucleationCriterionLaw(*this);
};



void BereminNucleationCriterionLaw::setdirAmplification(const double kx, const double ky, const double kz)
{
  dirAmplification_ *= 0.0;
  dirAmplification_(0,0) = kx;
  dirAmplification_(1,1) = ky;
  dirAmplification_(2,2) = kz;
}


void BereminNucleationCriterionLaw::rotateMaterialAxes(const double alpha, const double beta, const double gamma)
{
  // Rotate the tensor of stress concentration factor following the rotation defined by the euler angles in arguments.
  // Angles are in degrees in input !
  
  // The rotation from the initial frame (need to be computed) to the body/material frame (given in input by constructor) 
  // is described as follows:
    // 1. a rotation along z0 of amplitude alpha = precession angle
    // 2. a rotation along x1 of amplitude beta = nutation angle
    // 3. a rotation along z2 of amplitude gamma = spin angle
  // R defines the rotation from the initial frame (need to be computed) to the body frame (given in input by constructor)
  // i.e.  X_body = R * X_initial =>    K_body = R * K_initital * R^T
  // and   X_initial = R^T * X_body =>  K_initital = R^T * K_body * R
  
  // Compute the rotation tensor R(alpha, beta, gamma)
    // Get values in rad
  double c1, c2, c3, s1, s2, s3;
  double s1c2, c1c2;
  double pi(3.14159265359);
  double fpi = pi / 180.;

    // Compute components
  c1 = cos( alpha * fpi );
  s1 = sin( alpha * fpi );

  c2 = cos( beta * fpi );
  s2 = sin( beta * fpi );

  c3 = cos( gamma * fpi );
  s3 = sin( gamma * fpi );

  s1c2 = s1 * c2;
  c1c2 = c1 * c2;
  
  STensor3 R;
  R(0,0) = c3*c1 - s1c2*s3;
  R(0,1) = c3*s1 + c1c2*s3;
  R(0,2) = s2*s3;

  R(1,0) = -s3*c1 - s1c2*c3;
  R(1,1) = -s3*s1 + c1c2*c3;
  R(1,2) =  s2*c3;

  R(2,0) =  s1*s2;
  R(2,1) = -c1*s2;
  R(2,2) =  c2;
  
  // Apply the rotation
  STensor3 criticalStressInOldAxes = _criticalStress; // This tensor is created for cache
  STensor3 stressFactorInOldAxes = _stressConcentrationTensor; // This tensor is created for cache
  STensor3 dirAmplificationInOldAxes = dirAmplification_; // This tensor is created for cache
  for( int i = 0; i < 3; i++ ){
    for( int j = 0; j < 3; j++ ){
      _stressConcentrationTensor(i,j) = 0.;
      _criticalStress(i,j) = 0.;
      dirAmplification_(i,j) = 0.;
      for ( int k = 0; k < 3; k++ ){
        for ( int l = 0; l < 3; l++ ){
          /// K_(initial) = R^T * K_(material) * R
          _stressConcentrationTensor(i,j) += R(k,i) * stressFactorInOldAxes(k,l) * R(l,j);
          _criticalStress(i,j) += R(k,i) * criticalStressInOldAxes(k,l) * R(l,j);
          dirAmplification_(i,j) += R(k,i) * dirAmplificationInOldAxes(k,l) * R(l,j);
        }
      }
    }
  }
};






void BereminNucleationCriterionLaw::createIPVariable( IPNucleationCriterion* &ipNuclCriterion ) const
{
  if(ipNuclCriterion != NULL) delete ipNuclCriterion;
  ipNuclCriterion = new IPBereminNucleationCriterion();
};



// ------------
const bool BereminNucleationCriterionLaw::checkParametersIntegrity() const 
{
  // Check if values are meaningfull (return false if a problem is encountered)
  // (i.e check from parent classes are satisfied)
  if( !NucleationCriterionLaw::checkParametersIntegrity() ){
    return false;
  }

  //! \todo add a check for the tensor values.
  
  
  // Return true as no error is detected so far.
  return true;
};




void BereminNucleationCriterionLaw::assessNucleationOnset( double& criterionValue, IPNonLocalPorosity& q1Porous ) const
{
  // To evaluate the criterion, get the maximal eigen values of the criterion under a tensorial form.
  
  /// To do so, compute the maximal effective stress transmitted to the particles, which
  /// corresponds to the eigenvalues of the criterion rewritten under a "tensorial" form:
  /// criterion_tensor = stress + R*(k*(sigma_eq - sigma_y0) - sigma_nc)*R^T (in the current configuration)
  static STensor3 criterion_tensor;
  
  /// Get 2nd and 3rd term := k*(sigma_eq - sigma_y0) - sigma_nc
  static STensor3 K_minus_sigma_nc;
  K_minus_sigma_nc = _stressConcentrationTensor;
  K_minus_sigma_nc *= (q1Porous.getCurrentViscoplasticYieldStress() - q1Porous.getConstRefToIPJ2IsotropicHardening().getR0());
  K_minus_sigma_nc -= _criticalStress;
  
  
  //! \fixme One day, this should be done once at the material level and not here. (i.e. compute the rotation matrix and the stress tensor).

  /// Apply the rotation R from the initial to the current configuration:
  /// i.e. k(current) = R * k(initial) * R^T
    /// Get the rotation R from deformation gradient
  static STensor3 R, U; /// cache for rotation and RU decomposition
  static STensor3 cache_R; /// cache for rotation computation
  STensorOperation::RUDecomposition( q1Porous.getConstRefToDeformationGradient(), U, R);
    
    /// apply the rotation k(current) = R * k(initial) * R^T
  STensorOperation::multSTensor3(R, K_minus_sigma_nc, cache_R);
  STensorOperation::multSTensor3SecondTranspose(cache_R, R, K_minus_sigma_nc);


  /// Get stress tensor in current frame from corrotationnal frame
    /// Compute Kirchhoff Stress from CorKir stress following: Stress = Fe^-T * StressCor * Fe^T
  static STensor3 invFp, Fe, invFe, invFe_x_CorStress, Stress; /// cache
      /// - get Fe = F * Fp^-1 and Fe^-1
  STensorOperation::inverseSTensor3(q1Porous.getConstRefToFp(), invFp);
  STensorOperation::multSTensor3(q1Porous.getConstRefToDeformationGradient(), invFp, Fe);
  STensorOperation::inverseSTensor3(Fe, invFe);
      /// - apply the rotation Stress = Fe^-T * StressCor * Fe^T
  STensorOperation::multSTensor3FirstTranspose(invFe, q1Porous.getConstRefToCorotationalKirchhoffStress(), invFe_x_CorStress);
  STensorOperation::multSTensor3SecondTranspose(invFe_x_CorStress, Fe, Stress);
      /// - transform, following the stress formulation, Kirchhoff stress to Cauchy stress = Kirchhoff / Jacobian
  if (mlawPorous_->getStressFormulation() == mlawNonLocalPorosity::CORO_CAUCHY){
    double detF = STensorOperation::determinantSTensor3(q1Porous.getConstRefToDeformationGradient());
    Stress *= 1./detF;
  }
    

  /// Get criterion_tensor
  criterion_tensor = Stress;
  criterion_tensor += K_minus_sigma_nc;
  
  /// Get the max eigen values for the direction that maximises the criterion
  static fullMatrix<double> eigVec(3,3);
  static fullVector<double> eigVal(3);
  criterion_tensor.eig(eigVec,eigVal,true);
  
  criterionValue = eigVal(2) / q1Porous.getConstRefToIPJ2IsotropicHardening().getR0();
  
  if ( criterionValue > 0.0 ){
    Msg::Info("Nucleation criterion %d satisfied in direction [%e, %e, %e]", num_, eigVec(0,2), eigVec(1,2), eigVec(2,2));
    
    IPNucleationCriterion& q1i_criterion = q1Porous.getRefToIPNucleation().getRefToIPNucleationCriterion(num_);
    double ampli = 0.0;
    
    for( int i=0; i < 3; i++ ){
      for( int j=0; j < 3; j++ ){
        ampli += eigVec(i,2)*dirAmplification_(i,j)*eigVec(j,2);
      }
    }
    q1i_criterion.setAmpli(ampli);
    Msg::Info("Ampli factor = %e ", ampli);
  }
  else{
    IPNucleationCriterion& q1i_criterion = q1Porous.getRefToIPNucleation().getRefToIPNucleationCriterion(num_);
    q1i_criterion.setAmpli(1.0);
  }
  
};


void BereminNucleationCriterionLaw::updateIfNucleationEnds( IPNucleationCriterion& q1i_Criterion, const IPNonLocalPorosity& q1Porous ) const
{
  // Nothing to do : this criterion has no end foreseen.
};





