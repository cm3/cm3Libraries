//
// Description: Define kinematic hardening laws for  plasticity
//
//
// Author:  <V.D. Nguyen>, (C) 2014
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef KINEMATICHARDENING_H_
#define KINEMATICHARDENING_H_

#include "ipKinematicHardening.h"
#include "fullMatrix.h"
#include "scalarFunction.h"

class kinematicHardening {
  public:
    enum hardeningname{polynomialKinematicHardening, powerKinematicHardening, coshKinematicHardening, exponentialKinematicHardening};

  protected:
    int _num; // number of law (must be unique !)
    bool _initialized; // to initialize law

  public:
  #ifndef SWIG
    kinematicHardening(const int num, const bool init=true);
    virtual ~kinematicHardening(){}
    kinematicHardening(const kinematicHardening &source);
    virtual kinematicHardening& operator=(const kinematicHardening &source);
    virtual int getNum() const{return _num;}
    virtual hardeningname getType() const=0;
    virtual void createIPVariable(IPKinematicHardening* &ipv) const=0;
    virtual void initLaws(const std::map<int,kinematicHardening*> &maplaw)=0;
    virtual const bool isInitialized() {return _initialized;}
    virtual void hardening(double p0, const IPKinematicHardening &ipvprev, double _p, IPKinematicHardening &ipv) const=0;
    virtual void hardening(double p0, const IPKinematicHardening &ipvprev, double _p, double T, IPKinematicHardening &ipv) const=0;
    virtual kinematicHardening * clone() const=0;
  #endif

};

class PolynomialKinematicHardening : public kinematicHardening{
  #ifndef SWIG
  protected:
    int _order; // order of polymonial
    fullVector<double> _coefficients; // coeff of polymonial _coefficients(0)+_coefficients(1)*eps+...
    scalarFunction* _temFunc_K;
  #endif // SWIG

  public:
    PolynomialKinematicHardening(const int num, int order, bool init = true);
    void setCoefficients(const int i, const double val);
    void setTemperatureFunction_K(const scalarFunction& Tfunc);
    #ifndef SWIG
    virtual ~PolynomialKinematicHardening();
    PolynomialKinematicHardening(const PolynomialKinematicHardening& src);
    virtual PolynomialKinematicHardening& operator =(const kinematicHardening& src);
    virtual hardeningname getType() const{return kinematicHardening::polynomialKinematicHardening; };
    virtual void createIPVariable(IPKinematicHardening* &ipv) const;
    virtual void initLaws(const std::map<int,kinematicHardening*> &maplaw) {}; //nothing now, we will see if we use the mapping
    virtual void hardening(double p0, const IPKinematicHardening &ipvprev, double _p, IPKinematicHardening &ipv) const;
    virtual void hardening(double p0, const IPKinematicHardening &ipvprev, double _p, double T, IPKinematicHardening &ipv) const;
    virtual kinematicHardening * clone() const;
    #endif // SWIG
};

class PowerKinematicHardening : public kinematicHardening{
  // R = h p^exp
 protected :
  double _h, _exp;

 public:
    PowerKinematicHardening(const int num, const double h, const double exp,  bool init = true);
    #ifndef SWIG
    virtual ~PowerKinematicHardening(){}
    PowerKinematicHardening(const PowerKinematicHardening& src);
    virtual PowerKinematicHardening& operator =(const kinematicHardening& src);
    virtual hardeningname getType() const{return kinematicHardening::powerKinematicHardening; };
    virtual void createIPVariable(IPKinematicHardening* &ipv) const;
    virtual void initLaws(const std::map<int,kinematicHardening*> &maplaw) {}; //nothing now, we will see if we use the mapping
    virtual void hardening(double p0, const IPKinematicHardening &ipvprev, double _p, IPKinematicHardening &ipv) const;
    virtual void hardening(double p0, const IPKinematicHardening &ipvprev, double _p, double T, IPKinematicHardening &ipv) const;
    virtual kinematicHardening * clone() const;
    #endif // SWIG
};

class CoshKinematicHardening : public kinematicHardening{
  // R = K*(cosh(p/p0)-1)
 protected :
  double _K, _p0;

 public:
    CoshKinematicHardening(const int num, const double KK, const double p0,  bool init = true);
    #ifndef SWIG
    virtual ~CoshKinematicHardening(){}
    CoshKinematicHardening(const CoshKinematicHardening& src);
    virtual CoshKinematicHardening& operator =(const kinematicHardening& src);
    virtual hardeningname getType() const{return kinematicHardening::coshKinematicHardening; };
    virtual void createIPVariable(IPKinematicHardening* &ipv) const;
    virtual void initLaws(const std::map<int,kinematicHardening*> &maplaw) {}; //nothing now, we will see if we use the mapping
    virtual void hardening(double p0, const IPKinematicHardening &ipvprev, double _p, IPKinematicHardening &ipv) const;
    virtual void hardening(double p0, const IPKinematicHardening &ipvprev, double _p, double T, IPKinematicHardening &ipv) const;
    virtual kinematicHardening * clone() const;
    #endif // SWIG
};

class exponentialKinematicHardeningLaw : public kinematicHardening{
  // R = K*(1-exp(-h p^alp))
  protected:
    double _K, _h, _alpha;
    scalarFunction* _temFunc_K;
    
  public:
    exponentialKinematicHardeningLaw(const int num, const double KK, const double h, const double alp);
    void setTemperatureFunction_K(const scalarFunction& Tfunc);
    
    #ifndef SWIG
    virtual ~exponentialKinematicHardeningLaw();
    exponentialKinematicHardeningLaw(const exponentialKinematicHardeningLaw& src);
    virtual exponentialKinematicHardeningLaw& operator =(const kinematicHardening& src);
    virtual hardeningname getType() const{return kinematicHardening::exponentialKinematicHardening; };
    virtual void createIPVariable(IPKinematicHardening* &ipv) const;
    virtual void initLaws(const std::map<int,kinematicHardening*> &maplaw) {}; //nothing now, we will see if we use the mapping
    virtual void hardening(double p0, const IPKinematicHardening &ipvprev, double _p, IPKinematicHardening &ipv) const;
    virtual kinematicHardening * clone() const;
    virtual void hardening(double p0, const IPKinematicHardening &ipvprev, double _p, double T, IPKinematicHardening &ipv) const;
    #endif 
};

#endif // KINEMATICHARDENING_H_
