//
// C++ Interface: material law
//
// Description: j2 linear elasto-plastic law with non local damage interface
//
// Author:  <L. Noels>, (C) 2013
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef MLAWNONLOCALDAMAGEJ2HYPER_H_
#define MLAWNONLOCALDAMAGEJ2HYPER_H_
#include "mlawJ2linear.h"
#include "ipNonLocalDamageJ2Hyper.h"
#include "j2IsotropicHardening.h"
#include "CLengthLaw.h"
#include "DamageLaw.h"

class mlawNonLocalDamageJ2Hyper : public mlawJ2linear
{
 protected:
  CLengthLaw *cLLaw;
  DamageLaw  *damLaw;
  STensor43   Cel;
 public:
  mlawNonLocalDamageJ2Hyper(const int num,const double E,const double nu, const double rho,
				const J2IsotropicHardening &_j2IH, const CLengthLaw &_cLLaw,
                                const DamageLaw &_damLaw,
				const double tol=1.e-6, const bool matrixbyPerturbation = false, const double pert = 1e-8);
 #ifndef SWIG
  mlawNonLocalDamageJ2Hyper(const mlawNonLocalDamageJ2Hyper &source);
  mlawNonLocalDamageJ2Hyper& operator=(const materialLaw &source);
  virtual ~mlawNonLocalDamageJ2Hyper()
  {
    if (cLLaw!= NULL) delete cLLaw;
    if (damLaw!= NULL) delete damLaw;

  }
	virtual materialLaw* clone() const {return new mlawNonLocalDamageJ2Hyper(*this);}
  // function of materialLaw
  virtual matname getType() const{return materialLaw::nonLocalDamageJ2Hyper;}
  virtual bool withEnergyDissipation() const {return true;};
  virtual void createIPState(IPNonLocalDamageJ2Hyper *ivi, IPNonLocalDamageJ2Hyper *iv1, IPNonLocalDamageJ2Hyper *iv2) const;
  virtual void createIPState(IPStateBase* &ips, bool hasBodyForce, const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const IntPt *GP=NULL, const int gpt = 0) const;
  virtual void createIPVariable(IPNonLocalDamageJ2Hyper *&ipv) const;

  virtual void initLaws(const std::map<int,materialLaw*> &maplaw){}; // this law is initialized so nothing to do
  virtual double soundSpeed() const; // default but you can redefine it for your case
  virtual const CLengthLaw *getCLengthLaw() const {return cLLaw; };
  virtual const DamageLaw *getDamageLaw() const {return damLaw; };
	virtual double scaleFactor() const{return Cel(0,1,0,1);};
  // specific function
 public:
  virtual void constitutive(
                            const STensor3& F0,         // initial deformation gradient (input @ time n)
                            const STensor3& Fn,         // updated deformation gradient (input @ time n+1)
                            STensor3 &P,                // updated 1st Piola-Kirchhoff stress tensor (output)
                                                        // contains the initial values on input
                            const IPNonLocalDamageJ2Hyper *q0,       // array of initial internal variable
                            IPNonLocalDamageJ2Hyper *q1,             // updated array of internal variable (in ipvcur on output),
                            STensor43 &Tangent,         // constitutive tangents (output)
                            const bool stiff , 
                            const bool dTangentdeps,
                            STensor63* dCalgdeps = NULL           // if true compute the tangents
                           ) const;

  virtual void constitutive(
                            const STensor3& F0,         // initial deformation gradient (input @ time n)
                            const STensor3& Fn,         // updated deformation gradient (input @ time n+1)
                            STensor3 &P,                // updated 1st Piola-Kirchhoff stress tensor (output)
                                                        // contains the initial values on input
                            const IPNonLocalDamageJ2Hyper *q0,       // array of initial internal variable
                            IPNonLocalDamageJ2Hyper *q1,             // updated array of internal variable (in ipvcur on output),
                            STensor43 &Tangent,         // constitutive tangents (output)
                            STensor3  &dLocalPlasticStrainDStrain,
  			                STensor3  &dStressDNonLocalPlasticStrain,
                            double   &dLocalPlasticStrainDNonLocalPlasticStrain,
                            const bool stiff,            // if true compute the tangents
                            STensor43* elasticTangent = NULL
                           ) const;

  protected:

 #endif // SWIG
};

#endif // MLAWNONLOCALDAMAGEJ2HYPER_H_
