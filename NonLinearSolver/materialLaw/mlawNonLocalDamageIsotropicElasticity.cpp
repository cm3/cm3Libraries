//
// C++ Interface: material law
//
// Description: linear isotropic elastic law with non local damage interface
//
// Author:  <J. Leclerc>, (C) 2015
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "mlawNonLocalDamageIsotropicElasticity.h"
#include <math.h>
#include "MInterfaceElement.h"
#include "nonLinearMechSolver.h"


mlawNonLocalDamageIsotropicElasticity::mlawNonLocalDamageIsotropicElasticity(const int num,const double rho, const double E,
        const double nu, const CLengthLaw& cLLaw, const DamageLaw& damLaw)
    :materialLaw(num, true), _E(E),_nu(nu), _rho(rho), _ElasticityTensor(0.), _nonLocalLimitingMethod(1)
{
    _cLLaw = cLLaw.clone();
    _damLaw = damLaw.clone();

    _ElasticityTensor(0,0,0,0) = (1.-_nu)*_E/(1.+_nu)/(1.-2.*_nu);
    _ElasticityTensor(1,1,1,1) = _ElasticityTensor(0,0,0,0);
    _ElasticityTensor(2,2,2,2) = _ElasticityTensor(0,0,0,0);

    _ElasticityTensor(0,0,1,1) = nu*_E/(1.+_nu)/(1.-2.*_nu);
    _ElasticityTensor(0,0,2,2) = _ElasticityTensor(0,0,1,1);
    _ElasticityTensor(1,1,0,0) = _ElasticityTensor(0,0,1,1);
    _ElasticityTensor(1,1,2,2) = _ElasticityTensor(0,0,1,1);
    _ElasticityTensor(2,2,0,0) = _ElasticityTensor(0,0,1,1);
    _ElasticityTensor(2,2,1,1) = _ElasticityTensor(0,0,1,1);

    double mu = 0.5*_E/(1.+_nu);
    _ElasticityTensor(0,1,0,1) = mu;
    _ElasticityTensor(0,1,1,0) = mu;
    _ElasticityTensor(1,0,0,1) = mu;
    _ElasticityTensor(1,0,1,0) = mu;
    _ElasticityTensor(0,2,0,2) = mu;
    _ElasticityTensor(0,2,2,0) = mu;
    _ElasticityTensor(2,0,0,2) = mu;
    _ElasticityTensor(2,0,2,0) = mu;

    _ElasticityTensor(1,2,1,2) = mu;
    _ElasticityTensor(1,2,2,1) = mu;
    _ElasticityTensor(2,1,1,2) = mu;
    _ElasticityTensor(2,1,2,1) = mu;
}


mlawNonLocalDamageIsotropicElasticity::mlawNonLocalDamageIsotropicElasticity(const mlawNonLocalDamageIsotropicElasticity& source)
    : materialLaw(source)
{
    _rho = source._rho;
    _E = source._E;
    _nu = source._nu;
    _ElasticityTensor = source._ElasticityTensor;
    _nonLocalLimitingMethod = source._nonLocalLimitingMethod;

		_cLLaw = NULL;
    if(source._cLLaw != NULL)
        {
            _cLLaw=source._cLLaw->clone();
        }
		_damLaw = NULL;
    if(source._damLaw != NULL)
        {
            _damLaw=source._damLaw->clone();
        }
}


mlawNonLocalDamageIsotropicElasticity& mlawNonLocalDamageIsotropicElasticity::operator=(const materialLaw& source)
{
    materialLaw::operator=(source);
    const mlawNonLocalDamageIsotropicElasticity* src = dynamic_cast<const mlawNonLocalDamageIsotropicElasticity*>(&source);
    if(src != NULL)
    {
        _rho = src->_rho;
        _E = src->_E;
        _nu = src->_nu;
        _ElasticityTensor = src->_ElasticityTensor;
        _nonLocalLimitingMethod = src->_nonLocalLimitingMethod;

        if(_cLLaw != NULL) {delete _cLLaw;};
        if(src->_cLLaw != NULL)
            {
                _cLLaw = src->_cLLaw->clone();
            }

        if(_damLaw != NULL){delete _damLaw;};
        if(src->_damLaw != NULL)
            {
                _damLaw=src->_damLaw->clone();
            }
    }
    return *this;
}

void mlawNonLocalDamageIsotropicElasticity::createIPState(IPStateBase* &ips, bool hasBodyForce, const bool* state_,const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
    bool inter=true;
    const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
    if(iele==NULL)
        {
            inter=false;
        };
    IPVariable* ipvi = new IPNonLocalDamageIsotropicElasticity();
    IPVariable* ipv1 = new IPNonLocalDamageIsotropicElasticity();
    IPVariable* ipv2 = new IPNonLocalDamageIsotropicElasticity();
    if(ips != NULL) delete ips;
    ips = new IP3State(state_,ipvi,ipv1,ipv2);

}

void mlawNonLocalDamageIsotropicElasticity::createIPState(IPNonLocalDamageIsotropicElasticity* ivi, IPNonLocalDamageIsotropicElasticity* iv1,
        IPNonLocalDamageIsotropicElasticity* iv2) const
{

}

void mlawNonLocalDamageIsotropicElasticity::createIPVariable(IPNonLocalDamageIsotropicElasticity*& ipv) const
{

}

double mlawNonLocalDamageIsotropicElasticity::soundSpeed() const
{
    double factornu = (1.-_nu)/((1.+_nu)*(1.-2.*_nu));
    return sqrt(_E*factornu/_rho);
}


double mlawNonLocalDamageIsotropicElasticity::deformationEnergy(const STensor3 &epsilon_strains, const STensor3 &sigma) const
{
    // psi = 0.5 sigma_ij epsilon_ij
    double En(0.);
    for(int i=0; i<3; i++)
    {
        for(int j=0; j<3; j++)
        {
            En += sigma(i,j)*epsilon_strains(i,j);
        }
    }
    En *= 0.5;
    return En;
}


void  mlawNonLocalDamageIsotropicElasticity::constitutive(
            const STensor3& F0,                             // initial deformation gradient (input @ time n)
            const STensor3& F1,                             // updated deformation gradient (input @ time n+1)
            STensor3 &P,                                    // updated 1st Piola-Kirchhoff stress tensor (output)
            const IPVariable *q0i,  // array of initial internal variable (in ipvprev on input)
            IPVariable *q1i,        // updated array of internal variable (in ipvcur on output),
            STensor43 &Tangent,                             // constitutive tangents (output) // partial stress / partial strains
            STensor3 &dLocalEffectiveStrainsDStrains,       //                                // partial Local Effective Strains / partial Strains
            STensor3 &dStressDNonLocalEffectiveStrains,     //                                // partial stress / partial Non-Local Effective strains
            double &dLocalEffectiveStrainsDNonLocalStrains, //                                // partial Local Effective Strains / partial Non Local effective strains
            const bool stiff,                                // if true: compute the tangents
            STensor43* elasticTangent) const
{
  const IPNonLocalDamageIsotropicElasticity *q0 = dynamic_cast<const IPNonLocalDamageIsotropicElasticity *>(q0i);
        IPNonLocalDamageIsotropicElasticity *q1 = dynamic_cast<IPNonLocalDamageIsotropicElasticity *>(q1i);
// - Strains computation
  static STensor3 epsilon_strains;
  for(int i=0; i<3; i++)
  {
    for(int j=0; j<3; j++)
    {
      epsilon_strains(i,j) = F1(j,i);
    }
  }
  epsilon_strains += F1;
  epsilon_strains *= 0.5;
  epsilon_strains(0,0) -= 1;
  epsilon_strains(1,1) -= 1;
  epsilon_strains(2,2) -= 1;

// - Effective stress computation
  static STensor3 Peff;
  STensorOperation::zero(Peff);

  for(int i=0; i<3; i++)
  {
    for(int j=0; j<3; j++)
    {
      for(int k=0; k<3; k++)
      {
        for(int l=0; l<3; l++)
        {
          Peff(i,j)+=_ElasticityTensor(i,j,k,l)*epsilon_strains(k,l);
        }
      }
    }
  }

// - Local effective strains
  static fullMatrix<double> m(3, 3);
  static fullVector<double> eigenValReal(3), eigenValImag(3);
  static fullMatrix<double> leftEigenVect(3, 3), rightEigenVect(3, 3);
  epsilon_strains.getMat(m);

  // solving eigen problem
  m.eig(eigenValReal,eigenValImag,leftEigenVect,rightEigenVect,0);

  // norm computation including positive eigenvalues only
  double equivalentStrainsSquared(0.);
  for (int i=0; i<3; i++)
  {
    if (eigenValReal(i) > 0.)
    {
      equivalentStrainsSquared += eigenValReal(i)*eigenValReal(i);
    }
  }
  // modify ipv value
  q1->getRefToLocalEffectiveStrains() = sqrt(equivalentStrainsSquared);



// - Elastic energy release rate
  double effEner = deformationEnergy(epsilon_strains,Peff);

// - Damage computation and transition managing
  if (q1->dissipationIsBlocked())
  {
  // If damage evolution is blocked (ie. bulk near a crack):
    // ==> keep damage constant
    IPDamage& curDama = q1->getRefToIPDamage();
    curDama.getRefToDamage() = q0->getDamage();
    curDama.getRefToDDamageDp() = 0.;
    STensorOperation::zero(curDama.getRefToDDamageDFe());
    curDama.getRefToDeltaDamage() = 0;
    curDama.getRefToMaximalP() = q0->getMaximalP();
    q1->activeDamage(false);
  }
  else
  {
    // If local transition is allowed: (ie cracked interface)
    if (q1->getNonLocalToLocal())
    {
      // If transition from non-local to local is allowed (ie. at the iterface of a crack): effective values computed with local increment
      q1->getRefToNonLocalEffectiveStrains() = q0->getNonLocalEffectiveStrains() + (q1->getLocalEffectiveStrains() - q0->getLocalEffectiveStrains());
      _damLaw->computeDamage(q1->getNonLocalEffectiveStrains(), q0->getNonLocalEffectiveStrains(),
                             effEner, F1, 0., Peff, _ElasticityTensor,
                             q0->getConstRefToIPDamage(), q1->getRefToIPDamage());
    }
    else
    {
      // While critical damage not reached, damage is comp. with non-local value (as usual)
      _damLaw->computeDamage(q1->getNonLocalEffectiveStrains(), q0->getNonLocalEffectiveStrains(),
                             effEner, F1, 0., Peff, _ElasticityTensor,
                             q0->getConstRefToIPDamage(), q1->getRefToIPDamage());
    }


    if (q1->getDamage() > q0->getDamage())
    {
      q1->activeDamage(true);
    }
    else
    {
      q1->activeDamage(false);
    }
  }

// - Non-local managing during transition
  bool isLocalValueCorrected(false);  // Flag to see if local value has been corrected (for stiffness comp.)

  // If nonlocal diffusion is restrained (ie. bulk near a crack after crack insertion)
  if (_nonLocalLimitingMethod == 1)
  {
    if (q1->dissipationIsBlocked())
    {
      // Recopying previous max/critical value and restrained local value if needed
      q1->getRefToCriticalEffectiveStrains() = q0->getCriticalEffectiveStrains();
      if (q1->getLocalEffectiveStrains() > q0->getCriticalEffectiveStrains())
      {
        q1->getRefToLocalEffectiveStrains() = q0->getCriticalEffectiveStrains();
        isLocalValueCorrected = true;
      }
    }
    else    // Before transition and blocked nonlocal diffusion (classical case)
    {
      // Keep in memory maximal value as critical effective strain value
      if (q1->getLocalEffectiveStrains() < q0->getCriticalEffectiveStrains())
      {
        q1->getRefToCriticalEffectiveStrains() = q0->getCriticalEffectiveStrains();
      }
      else
      {
        q1->getRefToCriticalEffectiveStrains() = q1->getLocalEffectiveStrains();
      }
    }
  }



  double D = q1->getDamage();
  P = Peff;
  P *= (1.-D);



// - Stored energy computation
  q1->getRefToElasticEnergy() = effEner*(1-D);


// - Tangent data computation
  if (stiff)
  {
    // partial stress / partial strains
    Tangent = _ElasticityTensor * (1.-D);
    if(elasticTangent !=NULL) elasticTangent->operator=(Tangent);
    // partial stress / partial Non-Local Effective strains
    if (q1->getNonLocalToLocal())
    {
      STensorOperation::zero(dStressDNonLocalEffectiveStrains);
    }
    else
    {
      dStressDNonLocalEffectiveStrains = -q1->getDDamageDp() * Peff;
    }


    // partial Local Effective Strains / partial Strains
    STensorOperation::zero(dLocalEffectiveStrainsDStrains);
    if (isLocalValueCorrected)
    {
      //STensorOperation::zero(dLocalEffectiveStrainsDStrains);// Best solution ?
    }
    else
    {
      for (int k=0; k<3; k++)
      {
        if (eigenValReal(k) > 0.0)
        {
          double norm_V  = (leftEigenVect(0,k)*rightEigenVect(0,k) + leftEigenVect(1,k)*rightEigenVect(1,k)+leftEigenVect(2,k)*rightEigenVect(2,k));
          for (int i=0; i<3; i++)
          {
            for (int j=0; j<3; j++)
            {
              dLocalEffectiveStrainsDStrains(i,j) += leftEigenVect(i,k)*rightEigenVect(j,k)*eigenValReal(k)/norm_V/q1->getLocalEffectiveStrains();
            }
          }
        }
      }
    }


    // partial stress / partial strains: adding 1 additionnal term in case of local damage (to replace dStressDNonLocalEffectiveStrains)
    // and suppress dLocalEffectiveStrainsDStrains
    if (q1->getNonLocalToLocal())
    {
      for (int i=0; i<3; i++)
      {
        for (int j=0; j<3; j++)
        {
          for (int k=0; k<3; k++)
          {
            for (int l=0; l<3; l++)
            {
              Tangent(i,j,k,l) += -q1->getDDamageDp() * Peff(i,j) * dLocalEffectiveStrainsDStrains(k,l);
            }
          }
        }
      }
      //STensorOperation::zero(dLocalEffectiveStrainsDStrains);
    }


    // partial Local Effective Strains / partial Non Local effective strains
    STensorOperation::zero(dLocalEffectiveStrainsDNonLocalStrains);

  }

  q1->getRefToDamageEnergy() = q0->damageEnergy() + effEner* (D-q0->getDamage());

  // Path following only - irreversible energy
  // Dissipated energy = previous dissipated energy + Y delta_D
  if (this->getMacroSolver()->getPathFollowingLocalIncrementType() == pathFollowingManager::DEFO_ENERGY){
    q1->getRefToIrreversibleEnergy() = q1->defoEnergy();
  }
  else if ((this->getMacroSolver()->getPathFollowingLocalIncrementType() == pathFollowingManager::DISSIPATION_ENERGY) or
          (this->getMacroSolver()->getPathFollowingLocalIncrementType() == pathFollowingManager::DAMAGE_ENERGY)){
    q1->getRefToIrreversibleEnergy() = q1->damageEnergy();

  }
  else{
    q1->getRefToIrreversibleEnergy() = 0.;
  }


  if (stiff)
  {
    STensor3& DIrrevEnergyDF = q1->getRefToDIrreversibleEnergyDF();
    double& DIrrevEnergyDNonlocalVar = q1->getRefToDIrreversibleEnergyDNonLocalVariable();

    DIrrevEnergyDF = Peff;

    if (this->getMacroSolver()->getPathFollowingLocalIncrementType() == pathFollowingManager::DEFO_ENERGY){
      DIrrevEnergyDF*= (1.-D);
      if (q1->getNonLocalToLocal()){
        DIrrevEnergyDF.daxpy(dLocalEffectiveStrainsDStrains,-q1->getDDamageDp()*effEner);
        DIrrevEnergyDNonlocalVar = 0.;
      }
      else
      {
        DIrrevEnergyDNonlocalVar =  -effEner*q1->getDDamageDp();
      }
    }
    else if ((this->getMacroSolver()->getPathFollowingLocalIncrementType() == pathFollowingManager::DISSIPATION_ENERGY) or
          (this->getMacroSolver()->getPathFollowingLocalIncrementType() == pathFollowingManager::DAMAGE_ENERGY)){
      DIrrevEnergyDF *= (D - q0->getDamage());
      if (q1->getNonLocalToLocal()){
        DIrrevEnergyDF.daxpy(dLocalEffectiveStrainsDStrains,q1->getDDamageDp()*effEner);
        DIrrevEnergyDNonlocalVar = 0.;
      }
      else
      {
        DIrrevEnergyDNonlocalVar =  effEner*q1->getDDamageDp();
      }
    }
    else{
      DIrrevEnergyDF = 0.;
      DIrrevEnergyDNonlocalVar = 0.;
    }
  }


}



