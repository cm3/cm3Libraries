//
// C++ Interface: material law
//
// Author:  <Van Dung Nguyen>, (C) 2017
//
// Copyright: See COPYING file that comes with this distribution
//
//

#include "mlawHyperelasticDamage.h"
#include "nonLinearMechSolver.h"

mlawLocalDamageHyperelastic::mlawLocalDamageHyperelastic(const int num, const double rho, const elasticPotential& elaw, const DamageLaw &damLaw):
materialLaw(num,true), _rho(rho)
{
	_elasticLaw = elaw.clone();
	_damLaw = damLaw.clone();
};

mlawLocalDamageHyperelastic::mlawLocalDamageHyperelastic(const mlawLocalDamageHyperelastic& src): materialLaw(src){
	_elasticLaw = NULL;
	if (src._elasticLaw!=NULL) _elasticLaw = src._elasticLaw->clone();

	_damLaw = NULL;
	if (src._damLaw != NULL) _damLaw = src._damLaw->clone();

};

mlawLocalDamageHyperelastic::~mlawLocalDamageHyperelastic(){
	if (_elasticLaw) delete _elasticLaw; _elasticLaw = NULL;
	if (_damLaw) delete _damLaw; _damLaw = NULL;
};

double mlawLocalDamageHyperelastic::soundSpeed() const{
	double nu = _elasticLaw->getPoissonRatio();
	double E = _elasticLaw->getYoungModulus();
	 double factornu = (1.-nu)/((1.+nu)*(1.-2.*nu));
	return sqrt(E*factornu/_rho);
};
double mlawLocalDamageHyperelastic::scaleFactor() const{
	double nu = _elasticLaw->getPoissonRatio();
	double E = _elasticLaw->getYoungModulus();
	return E/(2.*(1.+nu));
}

void mlawLocalDamageHyperelastic::constitutive(
		const STensor3& F0,                             // initial deformation gradient (input @ time n)
		const STensor3& F,                             // updated deformation gradient (input @ time n+1)
		STensor3 &P,                                    // updated 1st Piola-Kirchhoff stress tensor (output)
		const IPVariable *q0i,  // array of initial internal variable (in ipvprev on input)
		IPVariable *qi,        // updated array of internal variable (in ipvcur on output),
		STensor43 &dPdF,                             // constitutive tangents (output) // partial stress / partial strains
		const bool stiff,                                // if true: compute the tangents analytically
                STensor43* elasticTangent, 
                const bool dTangent,
                STensor63* dCalgdeps) const{
	// elastic law
        const IPLocalDamageIsotropicElasticity *q0 =dynamic_cast<const IPLocalDamageIsotropicElasticity *> (q0i);
	IPLocalDamageIsotropicElasticity *q=dynamic_cast<IPLocalDamageIsotropicElasticity *> (qi);
	if(elasticTangent!=NULL) Msg::Error("mlawLocalDamageHyperelastic::constitutive elasticTangent not defined");

	static STensor3 effP, dLocalVardF;
	STensorOperation::zero(effP);
	if (_elasticLaw == NULL){
		Msg::Error("elastic potential is NULL");
	}
	_elasticLaw->constitutive(F,effP,stiff,&dPdF); // compute

	// elastic potential
	double ene =  _elasticLaw->get(F);

	// local strain
	_elasticLaw->getLocalValForNonLocalEquation(F,q->getRefToLocalEffectiveStrains(),stiff,&dLocalVardF);

  static STensor3 Fp(1.);
  _damLaw->computeDamage(q->getLocalEffectiveStrains(), q0->getLocalEffectiveStrains(),
                         ene, F, Fp, effP, dPdF,q0->getConstRefToIPDamage(), q->getRefToIPDamage());

  if (q->getDamage() > q0->getDamage()){
    q->activeDamage(true);
  }
  else{
    q->activeDamage(false);
  }

  double D = q->getDamage();
  P = effP;
  P *= (1.-D);

  // elastic energy
  q->getRefToElasticEnergy() = ene*(1.-D);
  q->getRefToDamageEnergy() = q0->damageEnergy()+ ene*(D-q0->getDamage());

  if(stiff){
    dPdF *= (1.-D);
    for (int i=0; i<3; i++){
      for (int j=0; j<3; j++) {
        for (int k=0; k<3; k++){
          for (int l=0; l<3; l++){
            dPdF(i,j,k,l) -= (q->getDDamageDp() * effP(i,j) * dLocalVardF(k,l));
          }
        }
      }
    }
  }

  if (this->getMacroSolver()->getPathFollowingLocalIncrementType() == pathFollowingManager::DEFO_ENERGY){
    q->getRefToIrreversibleEnergy() = q->defoEnergy();
  }
  else if ((this->getMacroSolver()->getPathFollowingLocalIncrementType() == pathFollowingManager::DISSIPATION_ENERGY) or
           (this->getMacroSolver()->getPathFollowingLocalIncrementType() == pathFollowingManager::DAMAGE_ENERGY)){
    q->getRefToIrreversibleEnergy() = q->damageEnergy();
  }
  else{
    q->getRefToIrreversibleEnergy() = 0.;
  }

  if (stiff){
    STensor3& DIrrevEnergyDF = q->getRefToDIrreversibleEnergyDF();
    if (this->getMacroSolver()->getPathFollowingLocalIncrementType() == pathFollowingManager::DEFO_ENERGY){
      for (int i=0; i<3; i++){
        for (int j=0; j<3; j++){
          DIrrevEnergyDF(i,j) = effP(i,j)*(1-D) - ene*q->getDDamageDp()*dLocalVardF(i,j);
        }
      }
    }
    else if ((this->getMacroSolver()->getPathFollowingLocalIncrementType() == pathFollowingManager::DISSIPATION_ENERGY) or
             (this->getMacroSolver()->getPathFollowingLocalIncrementType() == pathFollowingManager::DAMAGE_ENERGY)){
      for (int i=0; i<3; i++){
        for (int j=0; j<3; j++){
          DIrrevEnergyDF(i,j) = effP(i,j)*(D-q0->getDamage()) + ene*q->getDDamageDp()*dLocalVardF(i,j);
        }
      }
    }
    else{
      STensorOperation::zero(DIrrevEnergyDF);
    }
  }


};

void mlawLocalAnisotropicDamageHyperelastic::constitutive(
		const STensor3& F0,                             // initial deformation gradient (input @ time n)
		const STensor3& F,                             // updated deformation gradient (input @ time n+1)
		STensor3 &P,                                    // updated 1st Piola-Kirchhoff stress tensor (output)
		const IPVariable *q0i,  // array of initial internal variable (in ipvprev on input)
		IPVariable *qi,        // updated array of internal variable (in ipvcur on output),
		STensor43 &dPdF,                             // constitutive tangents (output) // partial stress / partial strains
		const bool stiff,                                // if true: compute the tangents analytically
                STensor43* elasticTangent, 
                const bool dTangentdeps,
                STensor63* dCalgdeps) const{
	// elastic law
        const IPLocalDamageIsotropicElasticity *q0 =dynamic_cast<const IPLocalDamageIsotropicElasticity *> (q0i);
	IPLocalDamageIsotropicElasticity *q=dynamic_cast<IPLocalDamageIsotropicElasticity *> (qi);
	if(elasticTangent!=NULL) Msg::Error("mlawLocalAnisotropicDamageHyperelastic::constitutive elasticTangent not defined");

// elastic law
	if (_elasticLaw == NULL){
		Msg::Error("elastic potential is NULL");
	}
  static STensor3 effPp, effPm;
  static STensor43 effdPdFp, effdPdFm;
  static STensor3 dLocalVardF;

	_elasticLaw->constitutivePositive(F,effPp,stiff,&effdPdFp); // compute
  _elasticLaw->constitutiveNegative(F,effPm,stiff,&effdPdFm); // compute

	// elastic potential
  double effEnergPos = _elasticLaw->getPositiveValue(F);
  double effEnergNeg = _elasticLaw->getNegativeValue(F);

	// local strain
	_elasticLaw->getLocalPositiveValForNonLocalEquation(F,q->getRefToLocalEffectiveStrains(),stiff,&dLocalVardF);

  static STensor3 Fp(1.);
  _damLaw->computeDamage(q->getLocalEffectiveStrains(), q0->getLocalEffectiveStrains(),
                             effEnergPos, F, Fp, effPp, effdPdFp,q0->getConstRefToIPDamage(), q->getRefToIPDamage());

  if (q->getDamage() > q0->getDamage()){
    q->activeDamage(true);
  }
  else
  {
    q->activeDamage(false);
  }

  double D = q->getDamage();
  P = effPp;
  P *= (1.-D);
  P += effPm;

  // elastic energy
  q->getRefToElasticEnergy() = (1.-D)*effEnergPos+ effEnergNeg;
  q->getRefToDamageEnergy() = q0->damageEnergy()+effEnergPos*(D - q0->getDamage());


  if(stiff){
    dPdF = effdPdFp;
    dPdF*= (1.-D);
    dPdF += effdPdFm;

    for (int i=0; i<3; i++){
      for (int j=0; j<3; j++) {
        for (int k=0; k<3; k++){
          for (int l=0; l<3; l++){
            dPdF(i,j,k,l) -= (q->getDDamageDp() * effPp(i,j) * dLocalVardF(k,l));
          }
        }
      }
    }
  }

  if (this->getMacroSolver()->getPathFollowingLocalIncrementType() == pathFollowingManager::DEFO_ENERGY){
    q->getRefToIrreversibleEnergy() = q->defoEnergy();
  }
  else if ((this->getMacroSolver()->getPathFollowingLocalIncrementType() == pathFollowingManager::DISSIPATION_ENERGY) or
          (this->getMacroSolver()->getPathFollowingLocalIncrementType() == pathFollowingManager::DAMAGE_ENERGY)){
    q->getRefToIrreversibleEnergy() = q->damageEnergy();
  }
  else{
    q->getRefToIrreversibleEnergy() = 0.;
  }

  if (stiff){
    STensor3& DIrrevEnergyDF = q->getRefToDIrreversibleEnergyDF();
    if (this->getMacroSolver()->getPathFollowingLocalIncrementType() == pathFollowingManager::DEFO_ENERGY){
      for (int i=0; i<3; i++){
        for (int j=0; j<3; j++){
          DIrrevEnergyDF(i,j) = effPp(i,j)*(1-D) - effEnergPos*q->getDDamageDp()*dLocalVardF(i,j) + effPm(i,j);
        }
      }
    }
    else if ((this->getMacroSolver()->getPathFollowingLocalIncrementType() == pathFollowingManager::DISSIPATION_ENERGY) or
            (this->getMacroSolver()->getPathFollowingLocalIncrementType() == pathFollowingManager::DAMAGE_ENERGY)){
      for (int i=0; i<3; i++){
        for (int j=0; j<3; j++){
          DIrrevEnergyDF(i,j) = effPp(i,j)*(D-q0->getDamage()) + effEnergPos*q->getDDamageDp()*dLocalVardF(i,j);
        }
      }
    }
    else{
      STensorOperation::zero(DIrrevEnergyDF);
    }
  }

};



mlawNonlocalDamageHyperelastic::mlawNonlocalDamageHyperelastic(const int num, const double rho, const elasticPotential& elaw, const CLengthLaw &cLLaw,const DamageLaw &damLaw):
materialLaw(num,true), _rho(rho)
{
	_elasticLaw = elaw.clone();
	_cLLaw = cLLaw.clone();
	_damLaw = damLaw.clone();
};

mlawNonlocalDamageHyperelastic::mlawNonlocalDamageHyperelastic(const mlawNonlocalDamageHyperelastic& src): materialLaw(src){
	_elasticLaw = NULL;
	if (src._elasticLaw!=NULL) _elasticLaw = src._elasticLaw->clone();

	_cLLaw = NULL;
	if (src._cLLaw != NULL) _cLLaw = src._cLLaw->clone();

	_damLaw = NULL;
	if (src._damLaw != NULL) _damLaw = src._damLaw->clone();

};

mlawNonlocalDamageHyperelastic::~mlawNonlocalDamageHyperelastic(){
	if (_elasticLaw) delete _elasticLaw; _elasticLaw = NULL;
	if (_cLLaw) delete _cLLaw; _cLLaw = NULL;
	if (_damLaw) delete _damLaw; _damLaw = NULL;
};

double mlawNonlocalDamageHyperelastic::soundSpeed() const{
	double nu = _elasticLaw->getPoissonRatio();
	double E = _elasticLaw->getYoungModulus();
	 double factornu = (1.-nu)/((1.+nu)*(1.-2.*nu));
	return sqrt(E*factornu/_rho);
};
double mlawNonlocalDamageHyperelastic::scaleFactor() const{
	double nu = _elasticLaw->getPoissonRatio();
	double E = _elasticLaw->getYoungModulus();
	return E/(2.*(1.+nu));
}

void mlawNonlocalDamageHyperelastic::constitutive(
		const STensor3& F0,                             // initial deformation gradient (input @ time n)
		const STensor3& F,                             // updated deformation gradient (input @ time n+1)
		STensor3 &P,                                    // updated 1st Piola-Kirchhoff stress tensor (output)
		const IPVariable *q0i,  // array of initial internal variable (in ipvprev on input)
		IPVariable *qi,        // updated array of internal variable (in ipvcur on output),
		STensor43 &dPdF,                             // constitutive tangents (output) // partial stress / partial strains
		STensor3 &dLocalVardF,       //                                // partial Local Effective Strains / partial Strains
		STensor3 &dPdNonlocalVar,     //                                // partial stress / partial Non-Local Effective strains
		double &dLocalVardNonlocalVar, //                                // partial Local Effective Strains / partial Non Local effective strains
		const bool stiff,                                // if true: compute the tangents analytically
                STensor43* elasticTangent) const{
	// elastic law
        const IPNonLocalDamageIsotropicElasticity *q0 =dynamic_cast<const IPNonLocalDamageIsotropicElasticity *> (q0i);
	IPNonLocalDamageIsotropicElasticity *q=dynamic_cast<IPNonLocalDamageIsotropicElasticity *> (qi);
	if(elasticTangent!=NULL) Msg::Error("mlawNonlocalDamageHyperelastic::constitutive elasticTangent not defined");


	// compute current nonlocal lengths matrix
	double p0 = q0->getLocalEffectiveStrains();
  _cLLaw->computeCL(p0, q->getRefToIPCLength());

	// elastic law
	static STensor3 effP;
	STensorOperation::zero(effP);
	if (_elasticLaw == NULL){
		Msg::Error("elastic potential is NULL");
	}
	_elasticLaw->constitutive(F,effP,stiff,&dPdF); // compute

	// elastic potential
	q->getRefToElasticEnergy() = _elasticLaw->get(F);

	// local strain
	_elasticLaw->getLocalValForNonLocalEquation(F,q->getRefToLocalEffectiveStrains(),stiff,&dLocalVardF);


	if (q->dissipationIsBlocked()){
    IPDamage& curDama = q->getRefToIPDamage();
    curDama.getRefToDamage() = q0->getDamage();
    curDama.getRefToDDamageDp() = 0.;
    STensorOperation::zero(curDama.getRefToDDamageDFe());
    curDama.getRefToDeltaDamage() = 0;
    curDama.getRefToMaximalP() = q0->getMaximalP();
		q->activeDamage(false);
  }
  else
  {
		static STensor3 Fp(1.);

    if (q->getNonLocalToLocal()){
      q->getRefToNonLocalEffectiveStrains() = q0->getNonLocalEffectiveStrains() + (q->getLocalEffectiveStrains() - q0->getLocalEffectiveStrains());
      _damLaw->computeDamage(q->getNonLocalEffectiveStrains(), q0->getNonLocalEffectiveStrains(),
                             q->defoEnergy(), F, Fp, effP, dPdF,q0->getConstRefToIPDamage(), q->getRefToIPDamage());
    }
    else
    {
        _damLaw->computeDamage(q->getNonLocalEffectiveStrains(), q0->getNonLocalEffectiveStrains(),
                             q->defoEnergy(), F, Fp, effP, dPdF, q0->getConstRefToIPDamage(), q->getRefToIPDamage());
    }


    if (q->getDamage() > q0->getDamage())
    {
      q->activeDamage(true);
    }
    else
    {
      q->activeDamage(false);
    }
  }

  double D = q->getDamage();

  P = effP;
  P *= (1.-D);

  // elastic energy
  q->getRefToElasticEnergy() *= (1.-D);


  if(stiff){
    dPdF *= (1.-D);

    if (q->getNonLocalToLocal()){
      STensorOperation::zero(dPdNonlocalVar);
    }
    else{
      dPdNonlocalVar = effP;
      dPdNonlocalVar *= (-q->getDDamageDp());
    }

    if (q->getNonLocalToLocal()){
      for (int i=0; i<3; i++){
        for (int j=0; j<3; j++) {
          for (int k=0; k<3; k++){
            for (int l=0; l<3; l++){
              dPdF(i,j,k,l) -= (q->getDDamageDp() * effP(i,j) * dLocalVardF(k,l));
            }
          }
        }
      }
    }

    STensorOperation::zero(dLocalVardNonlocalVar);
  }

  q->getRefToIrreversibleEnergy() = q0->irreversibleEnergy() + q->defoEnergy()/(1.-q->getDamage()) * (D-q0->getDamage());
  if (stiff){
    if (q->getNonLocalToLocal()){
      STensor3& DIrrevEnergyDF = q->getRefToDIrreversibleEnergyDF();
      for (int i=0; i<3; i++)
      {
        for (int j=0; j<3; j++)
        {
          DIrrevEnergyDF(i,j) = effP(i,j)*(D-q0->getDamage()) + q->defoEnergy()/(1.-q->getDamage())*q->getDDamageDp()*dLocalVardF(i,j);
        }
      }
      q->getRefToDIrreversibleEnergyDNonLocalVariable() = 0.;
    }
    else
    {
      q->getRefToDIrreversibleEnergyDF() = effP;
      q->getRefToDIrreversibleEnergyDF() *= (D-q0->getDamage());
      q->getRefToDIrreversibleEnergyDNonLocalVariable() = q->defoEnergy()/(1.-q->getDamage()) * q->getDDamageDp();
    }
  }

};


void mlawNonlocalAnisotropicDamageHyperelastic::constitutive(
		const STensor3& F0,                             // initial deformation gradient (input @ time n)
		const STensor3& F,                             // updated deformation gradient (input @ time n+1)
		STensor3 &P,                                    // updated 1st Piola-Kirchhoff stress tensor (output)
		const IPVariable *q0i,  // array of initial internal variable (in ipvprev on input)
		IPVariable *qi,        // updated array of internal variable (in ipvcur on output),
		STensor43 &dPdF,                             // constitutive tangents (output) // partial stress / partial strains
		STensor3 &dLocalVardF,       //                                // partial Local Effective Strains / partial Strains
		STensor3 &dPdNonlocalVar,     //                                // partial stress / partial Non-Local Effective strains
		double &dLocalVardNonlocalVar, //                                // partial Local Effective Strains / partial Non Local effective strains
		const bool stiff,                                // if true: compute the tangents analytically
                STensor43* elasticTangent) const{
	// elastic law
        const IPNonLocalDamageIsotropicElasticity *q0 =dynamic_cast<const IPNonLocalDamageIsotropicElasticity *> (q0i);
	IPNonLocalDamageIsotropicElasticity *q=dynamic_cast<IPNonLocalDamageIsotropicElasticity *> (qi);
	if(elasticTangent!=NULL) Msg::Error("mlawNonlocalAnisotropicDamageHyperelastic::constitutive elasticTangent not defined");


	// compute current nonlocal lengths matrix
	double p0 = q0->getLocalEffectiveStrains();
  _cLLaw->computeCL(p0, q->getRefToIPCLength());

	// elastic law
	if (_elasticLaw == NULL){
		Msg::Error("elastic potential is NULL");
	}
  static STensor3 effPp, effPm;
  static STensor43 effdPdFp, effdPdFm;

	_elasticLaw->constitutivePositive(F,effPp,stiff,&effdPdFp); // compute
  _elasticLaw->constitutiveNegative(F,effPm,stiff,&effdPdFm); // compute

	// elastic potential
  double effEnergPos = _elasticLaw->getPositiveValue(F);
  double effEnergNeg = _elasticLaw->getNegativeValue(F);

	// local strain
	_elasticLaw->getLocalPositiveValForNonLocalEquation(F,q->getRefToLocalEffectiveStrains(),stiff,&dLocalVardF);

	if (q->dissipationIsBlocked()){
    IPDamage& curDama = q->getRefToIPDamage();
    curDama.getRefToDamage() = q0->getDamage();
    curDama.getRefToDDamageDp() = 0.;
    STensorOperation::zero(curDama.getRefToDDamageDFe());
    curDama.getRefToDeltaDamage() = 0;
    curDama.getRefToMaximalP() = q0->getMaximalP();
		q->activeDamage(false);
  }
  else
  {
		static STensor3 Fp(1.);

    if (q->getNonLocalToLocal()){
      q->getRefToNonLocalEffectiveStrains() = q0->getNonLocalEffectiveStrains() + (q->getLocalEffectiveStrains() - q0->getLocalEffectiveStrains());
      _damLaw->computeDamage(q->getNonLocalEffectiveStrains(), q0->getNonLocalEffectiveStrains(),
                             effEnergPos, F, Fp, effPp, effdPdFp,q0->getConstRefToIPDamage(), q->getRefToIPDamage());
    }
    else
    {
        _damLaw->computeDamage(q->getNonLocalEffectiveStrains(), q0->getNonLocalEffectiveStrains(),
                             effEnergPos, F, Fp, effPp, effdPdFp, q0->getConstRefToIPDamage(), q->getRefToIPDamage());
    }


    if (q->getDamage() > q0->getDamage())
    {
      q->activeDamage(true);
    }
    else
    {
      q->activeDamage(false);
    }
  }

  double D = q->getDamage();

  P = effPp;
  P *= (1.-D);
  P += effPm;

  // elastic energy
  q->getRefToElasticEnergy() = (1.-D)*effEnergPos+ effEnergNeg;


  if(stiff){
    dPdF = effdPdFp;
    dPdF*= (1.-D);
    dPdF += effdPdFm;

    if (q->getNonLocalToLocal()){
      STensorOperation::zero(dPdNonlocalVar);
    }
    else{
      dPdNonlocalVar = effPp;
      dPdNonlocalVar *= (-q->getDDamageDp());
    }

    if (q->getNonLocalToLocal()){
      for (int i=0; i<3; i++){
        for (int j=0; j<3; j++) {
          for (int k=0; k<3; k++){
            for (int l=0; l<3; l++){
              dPdF(i,j,k,l) -= (q->getDDamageDp() * effPp(i,j) * dLocalVardF(k,l));
            }
          }
        }
      }
    }

    STensorOperation::zero(dLocalVardNonlocalVar);
  }

  q->getRefToIrreversibleEnergy() = q0->irreversibleEnergy() + effEnergPos*(D-q0->getDamage());
  if (stiff){
    if (q->getNonLocalToLocal()){
      STensor3& DIrrevEnergyDF = q->getRefToDIrreversibleEnergyDF();
      for (int i=0; i<3; i++)
      {
        for (int j=0; j<3; j++)
        {
          DIrrevEnergyDF(i,j) = effPp(i,j)*(D-q0->getDamage()) + effEnergPos*q->getDDamageDp()*dLocalVardF(i,j);
        }
      }
      q->getRefToDIrreversibleEnergyDNonLocalVariable() = 0.;
    }
    else
    {
      q->getRefToDIrreversibleEnergyDF() = effPp;
      q->getRefToDIrreversibleEnergyDF() *= (D-q0->getDamage());
      q->getRefToDIrreversibleEnergyDNonLocalVariable() = effEnergPos* q->getDDamageDp();
    }
  }

};
