//
// C++ Interface: material law
//
// Description: material law for plastic anisotropy
//
// Author:  <V.D. Nguyen>, (C) 2023
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "mlawAnisotropicPlasticity.h"
#include "nonLinearMechSolver.h"

mlawAnisotropicPlasticity::mlawAnisotropicPlasticity(const int num,const double E,const double nu, const double rho,const J2IsotropicHardening &j2IH,
                           const double tol, const bool pert, const double eps) : materialLaw(num,true), _E(E), _nu(nu), _rho(rho),
                                               _lambda((E*nu)/(1.+nu)/(1.-2.*nu)),
                                               _mu(0.5*E/(1.+nu)),_K(E/3./(1.-2.*nu)), _K3(3.*_K), _mu3(3.*_mu),
                                               _mu2(2.*_mu), _tol(tol), _maxite(1000),_perturbationfactor(eps),_tangentByPerturbation(pert),_order(1),
                                                _stressFormulation(CORO_CAUCHY)
{
  _j2IH=j2IH.clone();

  STensorOperation::zero(_Cel);
  _Cel(0,0,0,0) = _lambda + _mu2;
  _Cel(1,1,0,0) = _lambda;
  _Cel(2,2,0,0) = _lambda;
  _Cel(0,0,1,1) = _lambda;
  _Cel(1,1,1,1) = _lambda + _mu2;
  _Cel(2,2,1,1) = _lambda;
  _Cel(0,0,2,2) = _lambda;
  _Cel(1,1,2,2) = _lambda;
  _Cel(2,2,2,2) = _lambda + _mu2;

  _Cel(1,0,1,0) = _mu;
  _Cel(2,0,2,0) = _mu;
  _Cel(0,1,0,1) = _mu;
  _Cel(2,1,2,1) = _mu;
  _Cel(0,2,0,2) = _mu;
  _Cel(1,2,1,2) = _mu;

  _Cel(0,1,1,0) = _mu;
  _Cel(0,2,2,0) = _mu;
  _Cel(1,0,0,1) = _mu;
  _Cel(1,2,2,1) = _mu;
  _Cel(2,0,0,2) = _mu;
  _Cel(2,1,1,2) = _mu;
  
   STensor3 I(1.);
  for (int i=0; i<3; i++){
    for (int j=0; j<3; j++){
      for (int k=0; k<3; k++){
        for (int l=0; l<3; l++){
          _I4(i,j,k,l) = 0.5*(I(i,k)*I(j,l)+I(i,l)*I(j,k));
          _I4dev(i,j,k,l) = 0.5*(I(i,k)*I(j,l)+I(i,l)*I(j,k)) - I(i,j)*I(k,l)/3.;
        }
      }
    }
  }
}
mlawAnisotropicPlasticity::mlawAnisotropicPlasticity(const mlawAnisotropicPlasticity &source) : materialLaw(source),_E(source._E), _nu(source._nu), _rho(source._rho),
                                                         _lambda(source._lambda),
                                                         _mu(source._mu), _K(source._K),_K3(source._K3), _mu3(source._mu3),
                                                         _mu2(source._mu2), _tol(source._tol), _maxite(source._maxite),
                                                         _perturbationfactor(source._perturbationfactor),
                                                         _tangentByPerturbation(source._tangentByPerturbation),_order(source._order),
                                                         _Cel(source._Cel),
                                                         _stressFormulation(source._stressFormulation),
                                                         _I4(source._I4), _I4dev(source._I4dev)
{
	_j2IH = NULL;
  if(source._j2IH != NULL)
  {
    _j2IH=source._j2IH->clone();
  }
}

mlawAnisotropicPlasticity& mlawAnisotropicPlasticity::operator=(const materialLaw &source)
{
  materialLaw::operator=(source);
  const mlawAnisotropicPlasticity* src =static_cast<const mlawAnisotropicPlasticity*>(&source);
  if(src != NULL)
  {
    _E = src->_E;
    _nu = src->_nu;
    _rho = src->_rho;
    _lambda = src->_lambda;
    _mu = src->_mu;
    _K = src->_K;
    _K3 = src->_K3;
    _mu3 = src->_mu3;
    _mu2 = src->_mu2;
    _tol = src->_tol;
    _maxite = src->_maxite;
    _perturbationfactor = src->_perturbationfactor;
    _tangentByPerturbation = src->_tangentByPerturbation;
    _Cel = src->_Cel;
    _stressFormulation  = src->_stressFormulation;
    _I4 = src->_I4;
    _I4dev = src->_I4dev;
    if(_j2IH != NULL) delete _j2IH;
    if(src->_j2IH != NULL)
    {
      _j2IH=src->_j2IH->clone();
    }

  }
  return *this;
}

mlawAnisotropicPlasticity::~mlawAnisotropicPlasticity()
{
  if(_j2IH!=NULL) {delete _j2IH; _j2IH = NULL;};
}



void mlawAnisotropicPlasticity::setStressFormulation(int s)
{
  _stressFormulation = (stressFormulation)s;
  if (_stressFormulation == CORO_CAUCHY)
  {
    Msg::Info("The corotational cauchy stress is used");
  }
  else if (_stressFormulation == CORO_KIRCHHOFF)
  {
    Msg::Info("The corotational Kirchoff stress is used");
  }
  else
  {
    Msg::Error("The formulation %d is not implemented, using corotational Kirchoff instead");
    _stressFormulation = CORO_KIRCHHOFF;
  }
}

void mlawAnisotropicPlasticity::createIPState(IPStateBase* &ips,bool hasBodyForce,const bool* state_,const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
  //bool inter=true;
  //const MInterfaceElement *iele = static_cast<const MInterfaceElement*>(ele);
  //if(iele==NULL) inter=false;
  IPVariable* ipvi = new IPAnisotropicPlasticity(_j2IH);
  IPVariable* ipv1 = new IPAnisotropicPlasticity(_j2IH);
  IPVariable* ipv2 = new IPAnisotropicPlasticity(_j2IH);
  if(ips != NULL) delete ips;
  ips = new IP3State(state_,ipvi,ipv1,ipv2);
}

double mlawAnisotropicPlasticity::soundSpeed() const
{
  double factornu = (1.-_nu)/((1.+_nu)*(1.-2.*_nu));
  return sqrt(_E*factornu/_rho);
}



double mlawAnisotropicPlasticity::deformationEnergy(const STensor3 &C) const
{
  STensor3 logCdev;
  bool ok=STensorOperation::logSTensor3(C,_order,logCdev,NULL);
  double trace = logCdev.trace();

  double lnJ = 0.5*trace;
  logCdev(0,0)-=trace/3.;
  logCdev(1,1)-=trace/3.;
  logCdev(2,2)-=trace/3.;

  double Psy = _K*0.5*lnJ*lnJ+_mu*0.25*dot(logCdev,logCdev);
  return Psy;;
}



void mlawAnisotropicPlasticity::computeResidual(const STensor3& DeltaEp, std::vector<double>& DeltaPlasticMult, const double DeltaHatP, // unknown value 
                                        const STensor3& sig,  const double R, 
                                        const IPAnisotropicPlasticity* q0, const IPAnisotropicPlasticity* q1, 
                                        std::vector<double>& res0, STensor3& res1, double& res2,  
                                        bool stiff, 
                                        const STensor43& DsigDDeltaEp, const double H,
                                        std::vector<STensor3>& Dres0DDeltaEp, STensor43& Dres1DDeltaEp, STensor3& Dres2DDeltaEp,
                                        std::vector<double>& Dres0DDeltaPlasticMult, std::vector<STensor3>& Dres1DDeltaPlasticMult, std::vector<double>& Dres2DDeltaPlasticMult,
                                        std::vector<double>& Dres0DDeltaHatP, STensor3& Dres1DDeltaHatP, double& Dres2DDeltaHatP) const
{
  int numberYield = getNumOfYieldSurfaces();
  double R0 = _j2IH->getYield0();
  static std::vector<STensor3> DyieldDsig(numberYield,STensor3(0.));
  static std::vector<double> DyieldDR(numberYield, 0);
  static std::vector<STensor3> DyieldDDeltaEp(numberYield,STensor3(0.));
  static std::vector<STensor3> DyieldDFdefo(numberYield,STensor3(0.));;
  
  yieldFunction(res0,sig,R,q0,q1,stiff,&DyieldDsig,&DyieldDR,&DyieldDDeltaEp,&DyieldDFdefo);
  
  static std::vector<STensor3> Np(numberYield,STensor3(0.));
  static std::vector<STensor43> DNpDsig(numberYield,STensor43(0.));
  static std::vector<STensor3> DNpDR(numberYield, STensor3(0.));
  static std::vector<STensor43> DNpDDeltaEp(numberYield,STensor43(0.));
  static std::vector<STensor43> DNpDFdefo(numberYield,STensor43(0.));
  
  getYieldNormal(Np,sig,R,q0,q1,stiff,&DNpDsig,&DNpDR, &DNpDDeltaEp, &DNpDFdefo);
    
  // plastic energy balance
  res1 = DeltaEp;
  for (int i=0; i< numberYield; i++)
  {
    res1.daxpy(Np[i],-DeltaPlasticMult[i]);
  }
  //
  res2 = (STensorOperation::doubledot(sig,DeltaEp) - R*DeltaHatP)/R0;
  //
  if (stiff)
  {
    // res0
    for (int i=0; i< numberYield; i++)
    {
      STensorOperation::multSTensor3STensor43(DyieldDsig[i],DsigDDeltaEp,Dres0DDeltaEp[i]);
      Dres0DDeltaEp[i] += DyieldDDeltaEp[i];
      Dres0DDeltaPlasticMult[i] = 0.;
      Dres0DDeltaHatP[i] = DyieldDR[i]*H;
    }
    // res1
    Dres1DDeltaEp = _I4;
    STensorOperation::zero(Dres1DDeltaHatP);
    for (int i=0; i< numberYield; i++)
    {
      STensorOperation::multSTensor43Add(DNpDsig[i],DsigDDeltaEp,-DeltaPlasticMult[i],Dres1DDeltaEp);
      Dres1DDeltaEp.axpy(-DeltaPlasticMult[i], DNpDDeltaEp[i]);
      
      Dres1DDeltaPlasticMult[i] = Np[i];
      Dres1DDeltaPlasticMult[i] *= (-1.);
      Dres1DDeltaHatP.daxpy(DNpDR[i],-DeltaPlasticMult[i]*H);
    }
    
    // res2
    STensorOperation::scale(sig,1./R0,Dres2DDeltaEp);
    STensorOperation::multSTensor3STensor43Add(DeltaEp,DsigDDeltaEp,1./R0,Dres2DDeltaEp);
    for (int i=0; i< numberYield; i++)
    {
      Dres2DDeltaPlasticMult[i] = 0.;
    }
    Dres2DDeltaHatP = -(R+H*DeltaHatP)/R0;
  }
};

void mlawAnisotropicPlasticity::computeDResidual(const STensor3& DeltaEp, const std::vector<double>& DeltaPlasticMult, const double DeltaHatP, // unknown value 
                                  const STensor3& sig, const double R,  const IPAnisotropicPlasticity* q0, const IPAnisotropicPlasticity* q1,
                                  const STensor43& DsigDEepr, const double H, const STensor43& EprToF,
                                  std::vector<STensor3>& Dres0DFdefo, STensor43& Dres1DFdefo, STensor3& Dres2DFdefo) const
{
  int numberYield = getNumOfYieldSurfaces();
  double R0 = _j2IH->getYield0();
  
  static std::vector<STensor3> Dres0DEepr(numberYield, STensor3(0.));
  static STensor43 Dres1DEepr;
  static  STensor3 Dres2DEepr;
  
  
  static std::vector<double> yf(numberYield,0.);
  static std::vector<STensor3> DyieldDsig(numberYield,STensor3(0.)) ;
  static std::vector<double> DyieldDR(numberYield, 0);
  static std::vector<STensor3> DyieldDDeltaEp(numberYield,STensor3(0.));
  static std::vector<STensor3> DyieldDFdefo(numberYield,STensor3(0.));;
  
  yieldFunction(yf,sig,R,q0,q1,true,&DyieldDsig,&DyieldDR,&DyieldDDeltaEp, &DyieldDFdefo);
  for (int i=0;  i< numberYield; i++)
  {
    STensorOperation::multSTensor3STensor43(DyieldDsig[i],DsigDEepr,Dres0DEepr[i]);
  }
  
  static std::vector<STensor3> Np(numberYield,STensor3(0.));
  static std::vector<STensor43> DNpDsig(numberYield,STensor43(0.));
  static std::vector<STensor3> DNpDR(numberYield, STensor3(0.));
  static std::vector<STensor43> DNpDDeltaEp(numberYield,STensor43(0.));
  static std::vector<STensor43> DNpDFdefo(numberYield,STensor43(0.));
  
  getYieldNormal(Np,sig,R,q0,q1,true,&DNpDsig,&DNpDR, &DNpDDeltaEp, &DNpDFdefo);
  STensorOperation::zero(Dres1DEepr);
  for (int i=0;  i< numberYield; i++)
  {
    STensorOperation::multSTensor43Add(DNpDsig[i],DsigDEepr, -DeltaPlasticMult[i], Dres1DEepr);
  }
  // plastic energy balance
  STensorOperation::multSTensor3STensor43(DeltaEp,DsigDEepr,Dres2DEepr);
  Dres2DEepr *= (1./R0);
  
  // compute with respect to F
  for (int i=0; i< numberYield; i++)
  {
    STensorOperation::multSTensor3STensor43(Dres0DEepr[i],EprToF,Dres0DFdefo[i]);
    Dres0DFdefo[i] += DyieldDFdefo[i];
  }
  STensorOperation::multSTensor43(Dres1DEepr, EprToF, Dres1DFdefo);
  for (int i=0; i< numberYield; i++)
  {
    Dres1DFdefo.axpy(-DeltaPlasticMult[i], DNpDFdefo[i]);
  }
  STensorOperation::multSTensor3STensor43(Dres2DEepr, EprToF, Dres2DFdefo);
}; 

void mlawAnisotropicPlasticity::getResAndJacobian(fullVector<double>& res, fullMatrix<double>& J, 
                                const std::vector<double>& res0, const STensor3& res1, const double& res2,
                                const std::vector<STensor3>& Dres0DDeltaEp, const STensor43& Dres1DDeltaEp, const STensor3& Dres2DDeltaEp,
                                const std::vector<double>& Dres0DDeltaPlasticMult, const std::vector<STensor3>& Dres1DDeltaPlasticMult, const std::vector<double>& Dres2DDeltaPlasticMult,
                                const std::vector<double>& Dres0DDeltaHatP, const STensor3& Dres1DDeltaHatP,const double& Dres2DDeltaHatP) const{
  // unknown DeltaEp, DeltaPlasticMult_i, DeltaHatP
  // phi_i = 0, 
  // DeltaEp - sum_i DeltaPlasticMult_i * N_i = 0,
  // res2 = 0
  int numberYield = getNumOfYieldSurfaces();
  if (res.size() != 7+numberYield){
    res.resize(7+numberYield);
    J.resize(7+numberYield,7+numberYield);
  }
  // residual
  for (int i=0; i< numberYield; i++)
  {
    res(i) = res0[i];
  }
  res(numberYield) = res1(0,0);
  res(numberYield+1) = res1(1,1);
  res(numberYield+2) = res1(2,2);
  res(numberYield+3) = res1(0,1);
  res(numberYield+4) = res1(0,2);
  res(numberYield+5) = res1(1,2);
  res(numberYield+6) = res2;
  
  //
  J.setAll(0.);
  // jacobian for res0
  for (int i=0; i< numberYield; i++)
  {
    J(i,0) = Dres0DDeltaEp[i](0,0);
    J(i,1) = Dres0DDeltaEp[i](1,1);
    J(i,2) = Dres0DDeltaEp[i](2,2);
    J(i,3) = Dres0DDeltaEp[i](0,1)+Dres0DDeltaEp[i](1,0);
    J(i,4) = Dres0DDeltaEp[i](0,2)+Dres0DDeltaEp[i](2,0);
    J(i,5) = Dres0DDeltaEp[i](1,2)+Dres0DDeltaEp[i](2,1);
    J(i,6+i) = Dres0DDeltaPlasticMult[i]; // no cross term in yield
    J(i,6+numberYield) = Dres0DDeltaHatP[i];
  }
    
  // jacobian for res1
  J(numberYield,0) = Dres1DDeltaEp(0,0,0,0);
  J(numberYield,1) = Dres1DDeltaEp(0,0,1,1);
  J(numberYield,2) = Dres1DDeltaEp(0,0,2,2);
  J(numberYield,3) = Dres1DDeltaEp(0,0,0,1)+Dres1DDeltaEp(0,0,1,0);
  J(numberYield,4) = Dres1DDeltaEp(0,0,0,2)+Dres1DDeltaEp(0,0,2,0);
  J(numberYield,5) = Dres1DDeltaEp(0,0,1,2)+Dres1DDeltaEp(0,0,2,1);
  for (int i=0; i< numberYield; i++)
  {
    J(numberYield,6+i) = Dres1DDeltaPlasticMult[i](0,0);
  }
  J(numberYield,6+numberYield) = Dres1DDeltaHatP(0,0);
  
  J(numberYield+1,0) = Dres1DDeltaEp(1,1,0,0);
  J(numberYield+1,1) = Dres1DDeltaEp(1,1,1,1);
  J(numberYield+1,2) = Dres1DDeltaEp(1,1,2,2);
  J(numberYield+1,3) = Dres1DDeltaEp(1,1,0,1)+Dres1DDeltaEp(1,1,1,0);
  J(numberYield+1,4) = Dres1DDeltaEp(1,1,0,2)+Dres1DDeltaEp(1,1,2,0);
  J(numberYield+1,5) = Dres1DDeltaEp(1,1,1,2)+Dres1DDeltaEp(1,1,2,1);
  for (int i=0; i< numberYield; i++)
  {
    J(numberYield+1,6+i) = Dres1DDeltaPlasticMult[i](1,1);
  }
  J(numberYield+1,6+numberYield) = Dres1DDeltaHatP(1,1);
  
  J(numberYield+2,0) = Dres1DDeltaEp(2,2,0,0);
  J(numberYield+2,1) = Dres1DDeltaEp(2,2,1,1);
  J(numberYield+2,2) = Dres1DDeltaEp(2,2,2,2);
  J(numberYield+2,3) = Dres1DDeltaEp(2,2,0,1)+Dres1DDeltaEp(2,2,1,0);
  J(numberYield+2,4) = Dres1DDeltaEp(2,2,0,2)+Dres1DDeltaEp(2,2,2,0);
  J(numberYield+2,5) = Dres1DDeltaEp(2,2,1,2)+Dres1DDeltaEp(2,2,2,1);
  for (int i=0; i< numberYield; i++)
  {
    J(numberYield+2,6+i) = Dres1DDeltaPlasticMult[i](2,2);
  }
  J(numberYield+2,6+numberYield) = Dres1DDeltaHatP(2,2);
  
  J(numberYield+3,0) = Dres1DDeltaEp(0,1,0,0);
  J(numberYield+3,1) = Dres1DDeltaEp(0,1,1,1);
  J(numberYield+3,2) = Dres1DDeltaEp(0,1,2,2);
  J(numberYield+3,3) = Dres1DDeltaEp(0,1,0,1)+Dres1DDeltaEp(0,1,1,0);
  J(numberYield+3,4) = Dres1DDeltaEp(0,1,0,2)+Dres1DDeltaEp(0,1,2,0);
  J(numberYield+3,5) = Dres1DDeltaEp(0,1,1,2)+Dres1DDeltaEp(0,1,2,1);
  for (int i=0; i< numberYield; i++)
  {
    J(numberYield+3,6+i) = Dres1DDeltaPlasticMult[i](0,1);
  }
  J(numberYield+3,6+numberYield) = Dres1DDeltaHatP(0,1);
  
  J(numberYield+4,0) = Dres1DDeltaEp(0,2,0,0);
  J(numberYield+4,1) = Dres1DDeltaEp(0,2,1,1);
  J(numberYield+4,2) = Dres1DDeltaEp(0,2,2,2);
  J(numberYield+4,3) = Dres1DDeltaEp(0,2,0,1)+Dres1DDeltaEp(0,2,1,0);
  J(numberYield+4,4) = Dres1DDeltaEp(0,2,0,2)+Dres1DDeltaEp(0,2,2,0);
  J(numberYield+4,5) = Dres1DDeltaEp(0,2,1,2)+Dres1DDeltaEp(0,2,2,1);
  for (int i=0; i< numberYield; i++)
  {
    J(numberYield+4,6+i) = Dres1DDeltaPlasticMult[i](0,2);
  }
  J(numberYield+4,6+numberYield) = Dres1DDeltaHatP(0,2);
  
  J(numberYield+5,0) = Dres1DDeltaEp(1,2,0,0);
  J(numberYield+5,1) = Dres1DDeltaEp(1,2,1,1);
  J(numberYield+5,2) = Dres1DDeltaEp(1,2,2,2);
  J(numberYield+5,3) = Dres1DDeltaEp(1,2,0,1)+Dres1DDeltaEp(1,2,1,0);
  J(numberYield+5,4) = Dres1DDeltaEp(1,2,0,2)+Dres1DDeltaEp(1,2,2,0);
  J(numberYield+5,5) = Dres1DDeltaEp(1,2,1,2)+Dres1DDeltaEp(1,2,2,1);
  for (int i=0; i< numberYield; i++)
  {
    J(numberYield+5,6+i) = Dres1DDeltaPlasticMult[i](1,2);
  }
  J(numberYield+5,6+numberYield) = Dres1DDeltaHatP(1,2);
  
  // jacobian for res2
  J(numberYield+6,0) = Dres2DDeltaEp(0,0);
  J(numberYield+6,1) = Dres2DDeltaEp(1,1);
  J(numberYield+6,2) = Dres2DDeltaEp(2,2);
  J(numberYield+6,3) = Dres2DDeltaEp(0,1)+Dres2DDeltaEp(1,0);
  J(numberYield+6,4) = Dres2DDeltaEp(0,2)+Dres2DDeltaEp(2,0);
  J(numberYield+6,5) = Dres2DDeltaEp(1,2)+Dres2DDeltaEp(2,1);
  for (int i=0; i< numberYield; i++)
  {
    J(numberYield+6,6+i) = Dres2DDeltaPlasticMult[i];
  }
  J(numberYield+6,6+numberYield) = Dres2DDeltaHatP;
  
  //res.print("res");
  //J.print("jacobian");
};

bool mlawAnisotropicPlasticity::withPlastic(const STensor3& F, const STensor3& kcor, const double R, const IPAnisotropicPlasticity* q0, const IPAnisotropicPlasticity* q1) const
{
  int numberYield= getNumOfYieldSurfaces();
  if (numberYield > 1)
  {
    Msg::Error("multiple yield surface does not implemented in mlawAnisotropicPlasticity");
  }
  std::vector<double> yf(numberYield, 0);
  if (_stressFormulation == CORO_KIRCHHOFF)
  {
    yieldFunction(yf, kcor, R, q0, q1);
  }
  else if (_stressFormulation == CORO_CAUCHY)
  {
    double J = STensorOperation::determinantSTensor3(F);
    static STensor3 sig;
    sig = kcor;
    sig *= (1./J);
    yieldFunction(yf, sig, R, q0, q1);
  }
  else
  {
    Msg::Error("stress formuation is not correctly defined in mlawAnisotropicPlasticity!!!");
    return false;
  }
  return *(std::min_element(yf.begin(), yf.end())) > _tol; 
}


void mlawAnisotropicPlasticity::constitutive(const STensor3& F0,
                                const STensor3& Fn,
                                STensor3 &P,
                                const IPVariable *q0,
                                IPVariable *q1,
                                STensor43 &Tangent,
                                const bool stiff,
                                STensor43* elasticTangent, 
                                const bool dTangent,
                                STensor63* dCalgdeps
                                ) const
{
  const IPAnisotropicPlasticity* q0Ani= dynamic_cast<const IPAnisotropicPlasticity*>(q0);
        IPAnisotropicPlasticity* q1Ani= dynamic_cast<IPAnisotropicPlasticity*>(q1);
  static STensor43 dFpdF,dFedF;
  static STensor3 dpdF;
  bool ok = predictorCorector(F0,Fn,P,q0Ani,q1Ani,Tangent,dFpdF,dFedF,dpdF,stiff);
}

bool mlawAnisotropicPlasticity::predictorCorector(const STensor3& F0,         // initial deformation gradient (input @ time n)
                            const STensor3& F,         // updated deformation gradient (input @ time n+1)
                            STensor3 &P,                // updated 1st Piola-Kirchhoff stress tensor (output)
                            const IPAnisotropicPlasticity *q0,       // array of initial internal variable
                            IPAnisotropicPlasticity *q,             // updated array of internal variable (in ipvcur on output),
                            STensor43& Tangent,         // mechanical tangents (output)
                            STensor43& dFpdF, // plastic tangent
                            STensor43& dFedF,
                            STensor3& dpdF,
                            const bool stiff
                            ) const 
{  
  q->setRefToDeformationGradient(F);
  const STensor3& Fp0 = q0->getConstRefToFp();
  STensor3& Fp = q->getRefToFp();
  
  const STensor3& Ep0 = q0->getConstRefToPlasticDeformationTensor();
  STensor3& Ep = q->getRefToPlasticDeformationTensor();

  double& eps = q->getRefToEquivalentPlasticStrain();
  const double& eps0  = q0->getConstRefToEquivalentPlasticStrain();
  // elastic predictor
  Fp = Fp0;
  eps = eps0;
  Ep =  Ep0;
  static STensor3 invFp0, Fepr;
  STensorOperation::inverseSTensor3(Fp0,invFp0);
  STensorOperation::multSTensor3(F,invFp0,Fepr);

  static STensor3 Fpinv, Fe, Ce;
  Fpinv = invFp0;
  Fe = Fepr;
  STensorOperation::multSTensor3FirstTranspose(Fe,Fe,Ce);
  // compute strain by logarithmic operator
  static STensor43 Lpr; // dlogCepr/dCepr
  static STensor63 dLDCe; // ddlogCepr/ddCepr
  static STensor3 Eepr; // 0.5*log(Cepr)
  if (_order == 1)
  {
    bool ok=STensorOperation::logSTensor3(Ce,_order,Eepr,&Lpr);
    if(!ok)
    {
       P(0,0) = P(1,1) = P(2,2) = sqrt(-1.);
       return false; 
    }
  }
  else
  {
    bool ok=STensorOperation::logSTensor3(Ce,_order,Eepr,&Lpr,&dLDCe);
    if(!ok)
    {
       P(0,0) = P(1,1) = P(2,2) = sqrt(-1.);
       return false; 
    }
  }
  Eepr *= 0.5;

  static STensor43 L; // dlogCe/dCe
  L = Lpr; // equal to dlogCepr/dCepr

  STensor3& Ee  = q->getRefToElasticDeformationTensor();
  Ee = Eepr; // current elatic part of total strain

  // compute corotational Kirchhoff stress
  double pcorpr = _K*Ee.trace();
  static STensor3 kcorpr, kcor; 
  kcorpr = Ee.dev();
  kcorpr *= (2.*_mu);
  kcorpr(0,0) += pcorpr;
  kcorpr(1,1) += pcorpr;
  kcorpr(2,2) += pcorpr;

  kcor = kcorpr;
  
  /* Test plasticity */
  _j2IH->hardening(eps0, q0->getConstRefToIPJ2IsotropicHardening(),eps, q->getRefToIPJ2IsotropicHardening());
  double R0 = _j2IH->getYield0();
  double R   = q->getConstRefToIPJ2IsotropicHardening().getR();
  double H    = q->getConstRefToIPJ2IsotropicHardening().getDR();
  
  int nyield = getNumOfYieldSurfaces();
  static STensor43 DFpDDeltaEp;
  static fullVector<double> res(7+nyield), sol(7+nyield);
  static fullMatrix<double> Jac(7+nyield,7+nyield);
  double detF = 1.;
  double detFinv = 1.;
  
  static STensor3 DeltaEp;
  STensorOperation::zero(DeltaEp);
  static std::vector<double> DeltaPlasticMult(nyield);
  for (int i=0; i< nyield; i++)
  {
    DeltaPlasticMult[i] = 0.;
  }
  double DeltaHatP = 0;
  
  static STensor3 sig;
  static STensor43 DsigDDeltaEp;
  sig = kcor;
  DsigDDeltaEp = _Cel;
  DsigDDeltaEp *= (-1.);
  if (_stressFormulation == CORO_CAUCHY)
  {
    // update with deformation Jacobian
    detF = STensorOperation::determinantSTensor3(F);
    detFinv = 1./detF;
    sig *= detFinv;
    DsigDDeltaEp *= detFinv;
  }
  
  
  if (q->dissipationIsBlocked())
  {
    q->getRefToDissipationActive() = false;
  }
  else
  {
    computeRotatedAnisotropicTensor(F, DeltaEp, q0, q, true);
    // corotational stress
    if (withPlastic(F,kcor,R,q0,q))
    {
      // plastic occurs
      q->getRefToDissipationActive() = true;
      // plastic corrector
      // unknowns
      
                                            
      // static data
      static std::vector<double> res0(nyield);
      static std::vector<STensor3> Dres0DDeltaEp(nyield);
      static std::vector<double> Dres0DDeltaPlasticMult(nyield);
      static std::vector<double> Dres0DDeltaHatP(nyield);
      
      static STensor3 res1;
      static STensor43 Dres1DDeltaEp;
      static std::vector<STensor3> Dres1DDeltaPlasticMult(nyield);
      static STensor3 Dres1DDeltaHatP;
      
      static double res2;
      static STensor3 Dres2DDeltaEp;
      static std::vector<double> Dres2DDeltaPlasticMult(nyield);
      static double Dres2DDeltaHatP;
      
      computeResidual(DeltaEp,DeltaPlasticMult,DeltaHatP,sig,R,q0,q,
            res0,res1,res2,
            true,DsigDDeltaEp,H,
            Dres0DDeltaEp,Dres1DDeltaEp,Dres2DDeltaEp,
            Dres0DDeltaPlasticMult,Dres1DDeltaPlasticMult,Dres2DDeltaPlasticMult,
            Dres0DDeltaHatP, Dres1DDeltaHatP,Dres2DDeltaHatP);
            
      // compute solution
      getResAndJacobian(res,Jac,res0,res1,res2,
                      Dres0DDeltaEp,Dres1DDeltaEp,Dres2DDeltaEp,
                      Dres0DDeltaPlasticMult,Dres1DDeltaPlasticMult,Dres2DDeltaPlasticMult,
                      Dres0DDeltaHatP,Dres1DDeltaHatP,Dres2DDeltaHatP);
      //Jac.print("Jac");
      int ite = 0;
      double f = res.norm();
      // Start iterations
      //Msg::Info("plastic corrector iter=%d f=%e",ite,f);
      while(f > _tol or ite <1)
      {
                                        
        bool ok = Jac.luSolve(res,sol);
        
        if (!ok) 
        {
          Msg::Error("lu solve does not work in mlawAnisotropicPlasticity::predictorCorector!!!");
          return false;
        }
        static STensor3 dDeltaEp; 
        dDeltaEp(0,0) = sol(0);
        dDeltaEp(1,1) = sol(1);
        dDeltaEp(2,2) = sol(2);
        dDeltaEp(0,1) = sol(3);
        dDeltaEp(1,0) = sol(3);
        dDeltaEp(0,2) = sol(4);
        dDeltaEp(2,0) = sol(4);
        dDeltaEp(1,2) = sol(5);
        dDeltaEp(2,1) = sol(5);
        
        if (dDeltaEp.norm0() > 10.){
          DeltaEp*=0.1;
        }
        else{
          DeltaEp -= dDeltaEp;
        }
        
        for (int i=0; i< nyield; i++)
        {
          if (DeltaPlasticMult[i]-sol(6+i) <0.){
            DeltaPlasticMult[i] *=0.1;
          }
          else{
            DeltaPlasticMult[i] -= sol(6+i);
          }
        }
        
        if (DeltaHatP-sol(6+nyield) < 0.){
          DeltaHatP *= 0.1;
        }
        else{
          DeltaHatP -= sol(6+nyield);
        }
      
        // update Kcor
        // Kcor = Kcorpr - H:DeltaEp
        kcor = kcorpr;
        STensorOperation::multSTensor43STensor3Add(_Cel,DeltaEp,-1.,kcor);
        STensorOperation::scale(kcor,detFinv,sig);
        
        Ep = Ep0 + DeltaEp;
        eps = eps0 + DeltaHatP;
        _j2IH->hardening(eps0, q0->getConstRefToIPJ2IsotropicHardening(),eps, q->getRefToIPJ2IsotropicHardening());
        // update internal variables        
        R = q->getConstRefToIPJ2IsotropicHardening().getR();
        H = q->getConstRefToIPJ2IsotropicHardening().getDR();
        
        computeRotatedAnisotropicTensor(F, DeltaEp, q0, q, true);
        
        computeResidual(DeltaEp,DeltaPlasticMult,DeltaHatP,sig,R,q0,q,
            res0,res1,res2,
            true,DsigDDeltaEp,H,
            Dres0DDeltaEp,Dres1DDeltaEp,Dres2DDeltaEp,
            Dres0DDeltaPlasticMult,Dres1DDeltaPlasticMult,Dres2DDeltaPlasticMult,
            Dres0DDeltaHatP, Dres1DDeltaHatP,Dres2DDeltaHatP);
            
        getResAndJacobian(res,Jac,res0,res1,res2,
                        Dres0DDeltaEp,Dres1DDeltaEp,Dres2DDeltaEp,
                        Dres0DDeltaPlasticMult,Dres1DDeltaPlasticMult,Dres2DDeltaPlasticMult,
                        Dres0DDeltaHatP,Dres1DDeltaHatP,Dres2DDeltaHatP);
        
        f = res.norm();
        //printf("iter = %d error = %e tol = %e \n ",ite,f,_tol);
        if (f < _tol)  break;
        
        ite++;
        if((ite > _maxite) or STensorOperation::isnan(f))
        {
          P(0,0) = P(1,1) = P(2,2) = sqrt(-1.);
          printf("no convergence for plastic correction  with ite = %d maxite = %d norm = %e !!!\n", ite,_maxite,f);
          return false;
        }
      }
      
      // update plastic deformation
       // Plastic increment
      static STensor3 dFp;
      static STensor43 Dexp;
      // dFp = exp(DeltaEp)
      STensorOperation::expSTensor3(DeltaEp,_order,dFp, &Dexp);
      // Fp1 = dFp * Fp0
      STensorOperation::multSTensor3(dFp,Fp0,Fp);
      //
      for (int i=0; i<3; i++)
      {
        for (int j=0; j<3; j++)
        {
          for (int p=0; p<3; p++)
          {
            for (int q=0; q<3; q++)
            {
              DFpDDeltaEp(i,j,p,q) = 0.;
              for (int r=0; r<3; r++)
              {
                DFpDDeltaEp(i,j,p,q) += Dexp(i,r,p,q)*Fp0(r,j);
              }
            }
          }
        }
      }
      
      // Fe = F * Fp^-1
      STensorOperation::inverseSTensor3(Fp, Fpinv);
      STensorOperation::multSTensor3(F,Fpinv,Fe);
      // Ce = Fe^T * Fe
      STensorOperation::multSTensor3FirstTranspose(Fe,Fe,Ce);
      // Ee = ln(sqrt(Ce))
      if (_order == 1)
      {
        bool ok=STensorOperation::logSTensor3(Ce,_order,Ee,&L);
        if(!ok)
        {
           P(0,0) = P(1,1) = P(2,2) = sqrt(-1.);
           return false; 
        }
      }
      else
      {
        bool ok=STensorOperation::logSTensor3(Ce,_order,Ee,&L,&dLDCe);
        if(!ok)
        {
           P(0,0) = P(1,1) = P(2,2) = sqrt(-1.);
           return false; 
        }
      }
      Ee *= 0.5;      
    }
    else
    {
      q->getRefToDissipationActive() = false;
    }
  }

  // estimation of PK stress
  static STensor3 S, FeS;
  STensorOperation::multSTensor3STensor43(kcor,L,S);
  STensorOperation::multSTensor3(Fe,S,FeS);
  STensorOperation::multSTensor3SecondTranspose(FeS,Fpinv,P);
  
  effectivePresureVMS(F, kcor, q0, q, q->getRefToEffectivePressure(), q->getRefToEffectiveSVM());
  

  // elastic energy
  q->getRefToElasticEnergy()= this->deformationEnergy(Ce);
  // plastic energy
  q->getRefToPlasticEnergy() = q0->plasticEnergy();
  q->getRefToPlasticPower() = 0.;
    // plastic power (Wp1- Wp0)/dt
  if (eps-eps0 > 0.){
    q->getRefToPlasticEnergy() += detF*DeltaHatP*R;
    q->getRefToPlasticPower() = detF*DeltaHatP*R/this->getTimeStep();
  }

  // irreversible energy
  if (this->getMacroSolver()->getPathFollowingLocalIncrementType() == pathFollowingManager::DEFO_ENERGY){
    //
    q->getRefToIrreversibleEnergy() = q->defoEnergy();
  }
  else if ((this->getMacroSolver()->getPathFollowingLocalIncrementType() == pathFollowingManager::PLASTIC_ENERGY) or
           (this->getMacroSolver()->getPathFollowingLocalIncrementType() == pathFollowingManager::DISSIPATION_ENERGY)) {
    q->getRefToIrreversibleEnergy() = q->plasticEnergy();
  }
  else{
    q->getRefToIrreversibleEnergy() = 0.;
  }
  
  if (stiff)
  {
    
    if (_tangentByPerturbation)
    {
      static STensor3 plusF, plusP, plusFe, plusFpinv;
      static STensor43 tmp43;
      static IPAnisotropicPlasticity qTmp(*q0);

      for (int k=0; k<3; k++){
        for (int l=0; l<3; l++){
          plusF = F;
          plusF(k,l) += _perturbationfactor;
          this->constitutive(F0,plusF,plusP,q0,&qTmp,tmp43,false);
          STensorOperation::inverseSTensor3(qTmp._j2lepsp,plusFpinv);
          STensorOperation::multSTensor3(plusF,plusFpinv,plusFe);

          for (int i=0; i<3; i++){
            for (int j=0; j<3; j++){
              Tangent(i,j,k,l) = (plusP(i,j) - P(i,j))/_perturbationfactor;
              dFpdF(i,j,k,l) = (qTmp._j2lepsp(i,j) - q->_j2lepsp(i,j))/_perturbationfactor;
              dFedF(i,j,k,l) = (plusFe(i,j) - Fe(i,j))/_perturbationfactor;
            }
          }

          dpdF(k,l) = (qTmp._j2lepspbarre - eps)/_perturbationfactor;
          q->getRefToDIrreversibleEnergyDF()(k,l) = (qTmp.irreversibleEnergy() - q->irreversibleEnergy())/_perturbationfactor;
        }
      }

      STensor3& dplasticpowerDF = q->getRefToDPlasticPowerDF();
      dplasticpowerDF = q->getConstRefToDPlasticEnergyDF();
      dplasticpowerDF *= (1./this->getTimeStep());
    }
    else
    {
      static STensor43 EprToF;
      for (int i=0; i<3; i++){
        for (int j=0; j<3; j++){
          for (int k=0; k<3; k++){
            for (int l=0; l<3; l++){
              EprToF(i,j,k,l) = 0.;
              for (int p=0; p<3; p++){
                for (int q=0; q<3; q++){
                  EprToF(i,j,k,l) += Lpr(i,j,p,q)*Fepr(k,p)*invFp0(l,q);
                }
              }
            }
          }
        }
      }
      
      static STensor43 DKcorDFdefo, DFpDFdefo;
      STensorOperation::zero(DKcorDFdefo);
      STensorOperation::zero(DFpDFdefo);
      STensor3& DplasticEnergyDF = q->getRefToDPlasticEnergyDF();
      STensorOperation::zero(DplasticEnergyDF);
      
      // Kcor = Kcorpr - Cel:DeltaEp, 
      // DkcorDEepr = DKcorprDEepr
      STensorOperation::multSTensor43(_Cel, EprToF, DKcorDFdefo);
      if (q->dissipationIsActive())
      {
        
        /* Compute internal variables derivatives from residual derivatives */
        static std::vector<STensor3> Dres0DFdefo(nyield);
        static STensor43 Dres1DFdefo;
        static STensor3 Dres2DFdefo;
        
        
        // sig = Kcor*invdetF
        static STensor43 correctedDsigDEepr;
        STensorOperation::scale(_Cel,detFinv,correctedDsigDEepr);
        if (_stressFormulation == CORO_CAUCHY)
        {
          // J = Jepr*Jppr --> ln(J) = tr(Eepr) + ln(ppr) --> DJDEepr = J*I
          static STensor3 DdetFDEepr;
          STensorOperation::diag(DdetFDEepr,detF);
          STensorOperation::prodAdd(kcor,DdetFDEepr,-detFinv*detFinv,correctedDsigDEepr);
        }
        computeDResidual(DeltaEp,DeltaPlasticMult,DeltaHatP,sig, R,q0,q,correctedDsigDEepr, H, EprToF,
                        Dres0DFdefo,Dres1DFdefo,Dres2DFdefo);
                        
        static STensor43 DDeltaEPDFdefo;
        static STensor3 DDeltaHatPDFdefo;
        
        fullMatrix<double> invJac(8,8);
        bool isInverted = Jac.invert(invJac);
        if (!isInverted) 
        {
          Msg::Error("Jacobian cannot be inverted");
          return false;
        }
        for(int i=0; i<3; i++)
        {
          for(int j=0; j<3; j++)
          {
            for (int k=0; k< nyield; k++)
            {
              res(k) = -Dres0DFdefo[k](i,j);
            }
            res(nyield) = -Dres1DFdefo(0,0,i,j);
            res(nyield+1) = -Dres1DFdefo(1,1,i,j); 
            res(nyield+2) = -Dres1DFdefo(2,2,i,j);
            res(nyield+3) = -Dres1DFdefo(0,1,i,j);
            res(nyield+4) = -Dres1DFdefo(0,2,i,j);
            res(nyield+5) = -Dres1DFdefo(1,2,i,j);
            res(nyield+6) = -Dres2DFdefo(i,j);
            invJac.mult(res,sol);
            DDeltaEPDFdefo(0,0,i,j) = sol(0);
            DDeltaEPDFdefo(1,1,i,j) = sol(1);
            DDeltaEPDFdefo(2,2,i,j) = sol(2);
            DDeltaEPDFdefo(0,1,i,j) = sol(3);
            DDeltaEPDFdefo(1,0,i,j) = sol(3);
            DDeltaEPDFdefo(0,2,i,j) = sol(4);
            DDeltaEPDFdefo(2,0,i,j) = sol(4);
            DDeltaEPDFdefo(1,2,i,j) = sol(5);
            DDeltaEPDFdefo(2,1,i,j) = sol(5);
            DDeltaHatPDFdefo(i,j) = sol(6+nyield);
          }
        }
        
        // compute tangents
        // kcor = kcorpr - H:DeltaEp 
        STensorOperation::multSTensor43Add(_Cel,DDeltaEPDFdefo,-1.,DKcorDFdefo);
        
        // Fp = exp(DeltaEp)*Fp0
        STensorOperation::multSTensor43(DFpDDeltaEp,DDeltaEPDFdefo,DFpDFdefo);
        
        // plastic energy DplasticEnegy = R*DeltaHatP
        STensorOperation::scale(DDeltaHatPDFdefo,detF*(H*DeltaHatP+R),DplasticEnergyDF);
      };
    
      

      tangentComputation(Tangent, q->dissipationIsActive(), F, kcor, S, Fepr, Fp0, Lpr,
                        Fe, Fp, L, dLDCe,
                        DKcorDFdefo, DFpDFdefo, Fpinv);

      //update tangent of plastic energy

      if (_stressFormulation == CORO_CAUCHY)
      {
        static STensor3 invF;
        STensorOperation::inverseSTensor3(F, invF);
        for (int i=0; i<3; i++)
        {
          for (int j=0; j< 3; j++)
          {
            DplasticEnergyDF(i,j) += detF*invF(j,i)*DeltaHatP*R;
          }
        }
      }
      if (this->getMacroSolver()->withPathFollowing()){
        // irreversible energy
        if ((this->getMacroSolver()->getPathFollowingLocalIncrementType() == pathFollowingManager::PLASTIC_ENERGY) or
                 (this->getMacroSolver()->getPathFollowingLocalIncrementType() == pathFollowingManager::DISSIPATION_ENERGY)) {
          q->getRefToDIrreversibleEnergyDF() = q->getConstRefToDPlasticEnergyDF();
        }
        else{
          Msg::Error("Path following method is only contructed with dissipation based on plastic energy");
        }
      };
    }
  }
  return true;
};


void mlawAnisotropicPlasticity::tangentComputation(STensor43& dStressDF,
                              const bool plastic,
                              const STensor3& F,  // current F
                              const STensor3& corKir,  // cor Kir
                              const STensor3& S, //  second PK
                              const STensor3& Fepr, const STensor3& Fppr, // predictor
                              const STensor43& Lpr,
                              const STensor3& Fe, const STensor3& Fp, // corrector
                              const STensor43& L, const STensor63& dL, // corrector value
                              const STensor43& DcorKirDF,
                              const STensor43& dFpdF, 
                              const STensor3& Fpinv
                              ) const{
  // P = Fe. (Kcor:Le) . invFp
  // need to compute dFedF, dKcordF, dinvFpdF, dLedF

  // done DcorKirDF, DFpDF

  static STensor43 DinvFpdF;
  STensorOperation::zero(DinvFpdF);
  if (plastic){
    for (int i=0; i<3; i++){
      for (int j=0; j<3; j++){
        for (int p=0; p<3; p++){
          for (int q=0; q<3; q++){
            for (int k=0; k<3; k++){
              for (int l=0; l<3; l++){
                DinvFpdF(i,j,k,l) -= Fpinv(i,p)*dFpdF(p,q,k,l)*Fpinv(q,j);
              }
            }
          }
        }
      }
    }
  }

  static STensor43 dFedF;
  STensorOperation::zero(dFedF);
  for (int i=0; i<3; i++){
    for (int j=0; j<3; j++){
      for (int k=0; k<3; k++){
        dFedF(i,j,i,k) += Fpinv(k,j);
        if (plastic){
          for (int l=0; l<3; l++){
            for (int p=0; p<3; p++){
              dFedF(i,j,k,l) += F(i,p)*DinvFpdF(p,j,k,l);
            }
          }
        }
      }
    }
  }
  static STensor63 DLDF;
  STensorOperation::zero(DLDF);
  for (int i=0; i<3; i++){
    for (int j=0; j<3; j++){
      for (int k=0; k<3; k++){
        for (int l=0; l<3; l++){
          for (int r=0; r<3; r++){
            for (int s=0; s<3; s++){
              for (int a=0; a<3; a++){
                for (int p=0; p<3; p++){
                  for (int q=0; q<3; q++){
                    DLDF(i,j,k,l,p,q) += dL(i,j,k,l,r,s)*2.*Fe(a,r)*dFedF(a,s,p,q);
                  }
                }
              }
            }
          }
        }
      }
    }
  }
  //

  static STensor43 DSDF; // S = corKir:L
  STensorOperation::zero(DSDF);
  for (int i=0; i<3; i++){
    for (int j=0; j<3; j++){
      for (int r=0; r<3; r++){
        for (int s=0; s<3; s++){
          for (int k=0; k<3; k++){
            for (int l=0; l<3; l++){
              DSDF(i,j,k,l) += DcorKirDF(r,s,k,l)*L(r,s,i,j) + corKir(r,s)*DLDF(r,s,i,j,k,l);
            }
          }
        }
      }
    }
  }


  // compute mechanical tengent
  STensorOperation::zero(dStressDF);
  for (int i=0; i<3; i++){
    for (int j=0; j<3; j++){
      for (int k=0; k<3; k++){
        for (int l=0; l<3; l++){
          for (int p=0; p<3; p++){
            for (int q=0; q<3; q++){
              dStressDF(i,j,p,q) += (dFedF(i,k,p,q)*S(k,l)*Fpinv(j,l) + Fe(i,k)*DSDF(k,l,p,q)*Fpinv(j,l)+Fe(i,k)*S(k,l)*DinvFpdF(j,l,p,q));
            }
          }
        }
      }
    }
  }
}


mlawAnisotropicPlasticityJ2::mlawAnisotropicPlasticityJ2(const int num,const double E,const double nu, const double rho,const J2IsotropicHardening &_j2IH,const double tol,
              const bool matrixbyPerturbation, const double pert):
              mlawAnisotropicPlasticity(num, E, nu, rho, _j2IH, tol, matrixbyPerturbation,pert)
{
};
mlawAnisotropicPlasticityJ2::mlawAnisotropicPlasticityJ2(const mlawAnisotropicPlasticityJ2 &source): mlawAnisotropicPlasticity(source){};
mlawAnisotropicPlasticityJ2& mlawAnisotropicPlasticityJ2::operator=(const materialLaw &source)
{
  mlawAnisotropicPlasticity::operator =(source);
  
  return *this;
}

void mlawAnisotropicPlasticityJ2::effectivePresureVMS(const STensor3& F, const STensor3& sig, const IPAnisotropicPlasticity* q0, const IPAnisotropicPlasticity* q1, 
                        double& effPress, double& effSVM) const
{
  double J = F.determinant();
  effPress = (sig(0,0)+sig(1,1)+sig(2,2))/(3.*J);
  STensor3 s = sig.dev();
  effSVM = sqrt(1.5*s.dotprod())/J;
};

void mlawAnisotropicPlasticityJ2::yieldFunction(std::vector<double>& yf, const STensor3& sig, const double R, const IPAnisotropicPlasticity* q0, const IPAnisotropicPlasticity* q1,
                                  bool diff, std::vector<STensor3>* DFDsig, std::vector<double>* DFDR, std::vector<STensor3>* DFDDeltaEp, std::vector<STensor3>* DFDdefo) const
{
  double R0 = _j2IH->getYield0();
  STensor3 s = sig.dev();
  double sigEq = sqrt(1.5*s.dotprod());
  yf.resize(1);
  yf[0] = (sigEq - R)/R0;
  if (diff)
  {
    STensorOperation::scale(s, 1.5/(sigEq*R0), (*DFDsig)[0]);
    (*DFDR)[0] = -1./R0;
  }
};
void mlawAnisotropicPlasticityJ2::getYieldNormal(std::vector<STensor3>& Np, const STensor3& sig, const double R, const IPAnisotropicPlasticity* q0, const IPAnisotropicPlasticity* q1,
                                  bool diff, std::vector<STensor43>* DNpDsig, std::vector<STensor3>* DNpDR, std::vector<STensor43>* DFNpDeltaEp, std::vector<STensor43>* DNpDdefo) const
{
  int numberYield = getNumOfYieldSurfaces();
  double R0 = _j2IH->getYield0();
  STensor3 s = sig.dev();
  STensorOperation::scale(s, 1.5/R, Np[0]);
  if (diff)
  {
    STensorOperation::scale(_I4dev, 1.5/R, (*DNpDsig)[0]);
    STensorOperation::scale(s, -1.5/(R*R), (*DNpDR)[0]);
    STensorOperation::zero((*DFNpDeltaEp)[0]);
    STensorOperation::zero((*DNpDdefo)[0]);
  }
};

mlawAnisotropicPlasticityHill48::mlawAnisotropicPlasticityHill48(const int num,const double E,const double nu, const double rho,const J2IsotropicHardening &_j2IH,
              const double tol,
              const bool matrixbyPerturbation, const double pert):
              mlawAnisotropicPlasticity(num, E, nu, rho, _j2IH, tol, matrixbyPerturbation,pert)
{
    _aniM = _I4dev;
};
mlawAnisotropicPlasticityHill48::mlawAnisotropicPlasticityHill48(const mlawAnisotropicPlasticityHill48 &source): mlawAnisotropicPlasticity(source), _aniM(source._aniM){};
mlawAnisotropicPlasticityHill48& mlawAnisotropicPlasticityHill48::operator=(const materialLaw &source)
{
  mlawAnisotropicPlasticity::operator =(source);
  const mlawAnisotropicPlasticityHill48* psrc = dynamic_cast<const mlawAnisotropicPlasticityHill48*>(&source);
  if (psrc != NULL)
  {
    _aniM = psrc->_aniM;
  }
  
  return *this;
}

void mlawAnisotropicPlasticityHill48::setYieldParameters(const std::vector<double>& Mvec)
{
  if (Mvec.size() != 6)
  {
    Msg::Error("Hill48 requires 6 parameters to define yield surface");
    Msg::Exit(0);
  }
  STensorOperation::zero(_aniM);
  
  Msg::Info("-----\nHill parameters");
  for (int i=0; i< Mvec.size(); i++)
  {
    printf("%.16g ", Mvec[i]);
  }
  Msg::Info("\n------");
    
  // [F, G, H, L, M, N] = Mvec
  double F = Mvec[0];
  double G = Mvec[1];
  double H = Mvec[2];
  double L = Mvec[3];
  double M = Mvec[4];
  double N = Mvec[5];
  // define the equivalent stress under the form
  // sigEq**2 = F*0.5*(s11-s22)**2 
  //           +G*0.5*(s22-s00)**2
  //           +H*0.5*(s00-s11)**2 
  //           +L*3*sig12**2 + M*3*sig02**2 + N*3*s01**2) = 1.5*s:aniMSqr:s
  
  STensor43 aniMSqr;
  STensorOperation::zero(aniMSqr);
  aniMSqr(0,0,0,0) = (G+H)/3.;
  aniMSqr(0,0,1,1) = -H/3;
  aniMSqr(0,0,2,2) = -G/3.;
  
  aniMSqr(1,1,0,0) = -H/3.;
  aniMSqr(1,1,1,1) = (H+F)/3.;
  aniMSqr(1,1,2,2) = -F/3.;
  
  aniMSqr(2,2,0,0) = -G/3.;
  aniMSqr(2,2,1,1) = -F/3.;
  aniMSqr(2,2,2,2) = (F+G)/3.;
  
  aniMSqr(1,0,1,0) = N*0.5;
  aniMSqr(0,1,0,1) = N*0.5;
  aniMSqr(0,1,1,0) = N*0.5;
  aniMSqr(1,0,0,1) = N*0.5;
  
  aniMSqr(2,0,2,0) = M*0.5;
  aniMSqr(0,2,2,0) = M*0.5;
  aniMSqr(0,2,0,2) = M*0.5;
  aniMSqr(2,0,0,2) = M*0.5;
  
  aniMSqr(2,1,2,1) = L*0.5;
  aniMSqr(1,2,1,2) = L*0.5;
  aniMSqr(1,2,2,1) = L*0.5;
  aniMSqr(2,1,1,2) = L*0.5; 
  
  STensorOperation::sqrtSTensor43(aniMSqr, _aniM);
  _aniM.print("anisotropic tensor");
  
  fullMatrix<double> tmp(6,6);
  STensorOperation::fromSTensor43ToFullMatrix_ConsistentForm(aniMSqr,tmp);
  tmp.print("aniMSqr");
  STensorOperation::fromSTensor43ToFullMatrix_ConsistentForm(_aniM,tmp);
  tmp.print("_aniM");
  
  fullMatrix<double> test(6,6);
  tmp.mult(tmp, test);
  test.print("test");
};

void mlawAnisotropicPlasticityHill48::computeAnisotropyRotation(STensor3& Rtotal, const STensor3& F, const STensor3& DeltaEp, const STensor3& Fp0, 
                                                bool stiff, STensor43* DRtotalDF, STensor43* DRtotalDDeltaEp) const
{
  static STensor3 dFp, Fp, Fpinv, Fe, Ce, Ue, UeInv, Re, C,  U, UInv, R;
  STensorOperation::expSTensor3(DeltaEp,_order,dFp);
  // Fp1 = dFp * Fp0
  STensorOperation::multSTensor3(dFp,Fp0,Fp);
  // Fe = F * Fp^-1
  STensorOperation::inverseSTensor3(Fp, Fpinv);
  STensorOperation::multSTensor3(F,Fpinv,Fe);
  STensorOperation::multSTensor3FirstTranspose(Fe,Fe,Ce);
  STensorOperation::sqrtSTensor3(Ce,Ue);
  STensorOperation::inverseSTensor3(Ue,UeInv);
  STensorOperation::multSTensor3(Fe,UeInv,Re);
  
  STensorOperation::multSTensor3FirstTranspose(F,F,C);
  STensorOperation::sqrtSTensor3(C,U);
  STensorOperation::inverseSTensor3(U,UInv);
  STensorOperation::multSTensor3(F,UInv,R);

  STensorOperation::multSTensor3FirstTranspose(R,Re,Rtotal);
  
  if (stiff)
  {
    STensorOperation::zero(*DRtotalDDeltaEp);
    //
    static STensor3 plus_R;
    double ff = 1e-8;
    for (int i=0; i< 3; i++)
    {
      for (int j=i; j<3; j++)
      {
        static STensor3 plus_DeltaEp;
        plus_DeltaEp = DeltaEp;
        plus_DeltaEp(i,j) += 0.5*ff;
        plus_DeltaEp(j,i) += 0.5*ff;
        computeAnisotropyRotation(plus_R, F, plus_DeltaEp, Fp0);
        for (int k=0; k< 3; k++)
        {
          for (int l=0; l<3; l++)
          {
            (*DRtotalDDeltaEp)(k,l,i,j) = (plus_R(k,l) -Rtotal(k,l))/(ff);
            (*DRtotalDDeltaEp)(k,l,j,i) = (*DRtotalDDeltaEp)(k,l,i,j);
          }
        }
      }
    }
    
    STensorOperation::zero(*DRtotalDF);
    for (int i=0; i< 3; i++)
    {
      for (int j=0; j<3; j++)
      {
        static STensor3 plus_F;
        plus_F = F;
        plus_F(i,j) += ff;
        computeAnisotropyRotation(plus_R, plus_F, DeltaEp, Fp0);
        for (int k=0; k< 3; k++)
        {
          for (int l=0; l<3; l++)
          {
            
            (*DRtotalDF)(k,l,i,j) = (plus_R(k,l) -Rtotal(k,l))/(ff);
          }
        }
      }
    }
  }
  
};
                                                

void mlawAnisotropicPlasticityHill48::computeRotatedAnisotropicTensor(const STensor3& F, const STensor3& DeltaEp,  const IPAnisotropicPlasticity* q0, IPAnisotropicPlasticity* q1, bool stiff) const
{
  const STensor3& Fp0 = q0->getConstRefToFp();
  static STensor3 Rtotal;
  static STensor43 DRtotalDF, DRtotalDDeltaEp;
  computeAnisotropyRotation(Rtotal,F,DeltaEp,Fp0,stiff, &DRtotalDF, &DRtotalDDeltaEp);
  
  STensor43& M = q1->getRefToAnisotropicTensor();
  STensor63& DMDF =q1->getRefToDAnisotropicTensorDF();
  STensor63& DMDDeltaEp = q1->getRefToDAnisotropicTensorDEp();
  
  STensorOperation::zero(M);
  if (stiff)
  {
    STensorOperation::zero(DMDF);
    STensorOperation::zero(DMDDeltaEp);
  }
  
  for (int i=0; i<3; i++)
  {
    for (int j=0; j<3; j++)
    {
      for (int k=0; k<3; k++)
      {
        for (int l=0; l<3; l++)
        {
          for (int p=0; p<3; p++)
          {
            for (int q=0; q<3; q++)
            {
              for (int r=0; r<3; r++)
              {
                for (int s=0; s<3; s++)
                {
                  M(i,j,k,l) += Rtotal(p,i)*Rtotal(q,j)*_aniM(p,q,r,s)*Rtotal(r,k)*Rtotal(s,l);
                  if (stiff)
                  {
                    for (int a=0; a<3; a++)
                    {
                      for (int b=0; b<3; b++)
                      {
                        DMDF(i,j,k,l,a,b) += DRtotalDF(p,i,a,b)*Rtotal(q,j)*_aniM(p,q,r,s)*Rtotal(r,k)*Rtotal(s,l) +
                                                 Rtotal(p,i)*DRtotalDF(q,j,a,b)*_aniM(p,q,r,s)*Rtotal(r,k)*Rtotal(s,l) +
                                                  Rtotal(p,i)*Rtotal(q,j)*_aniM(p,q,r,s)*DRtotalDF(r,k,a,b)*Rtotal(s,l)+ 
                                                   Rtotal(p,i)*Rtotal(q,j)*_aniM(p,q,r,s)*Rtotal(r,k)*DRtotalDF(s,l,a,b);
                                                   
                        DMDDeltaEp(i,j,k,l,a,b) += DRtotalDDeltaEp(p,i,a,b)*Rtotal(q,j)*_aniM(p,q,r,s)*Rtotal(r,k)*Rtotal(s,l) +
                                                 Rtotal(p,i)*DRtotalDDeltaEp(q,j,a,b)*_aniM(p,q,r,s)*Rtotal(r,k)*Rtotal(s,l) +
                                                  Rtotal(p,i)*Rtotal(q,j)*_aniM(p,q,r,s)*DRtotalDDeltaEp(r,k,a,b)*Rtotal(s,l)+ 
                                                   Rtotal(p,i)*Rtotal(q,j)*_aniM(p,q,r,s)*Rtotal(r,k)*DRtotalDDeltaEp(s,l,a,b);
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }
};

void mlawAnisotropicPlasticityHill48::effectivePresureVMS(const STensor3& F, const STensor3& sig, const IPAnisotropicPlasticity* q0, const IPAnisotropicPlasticity* q1, 
                      double& effPress, double& effSVM) const
{
  double J = F.determinant();
  const STensor43& M = q1->getConstRefToAnisotropicTensor();
  const STensor63& DMDFdefo = q1->getConstRefToDAnisotropicTensorDF();
  const STensor63& DMDDeltaEp = q1->getConstRefToDAnisotropicTensorDEp();
    
  static STensor3 SigEff;
  STensorOperation::multSTensor43STensor3(M, sig, SigEff);
  static STensor3 sEff;
  sEff = SigEff.dev();
  effPress = (sig(0,0)+sig(1,1)+sig(2,2))/(3.*J);
  effSVM =  sqrt(1.5*sEff.dotprod())/J;
};


void mlawAnisotropicPlasticityHill48::yieldFunction(std::vector<double>& yf, const STensor3& sig, const double R, const IPAnisotropicPlasticity* q0, const IPAnisotropicPlasticity* q1,
                                  bool diff, std::vector<STensor3>* DFDsig, std::vector<double>* DFDR, std::vector<STensor3>* DFDDeltaEp, std::vector<STensor3>* DFDdefo) const
{  
  const STensor43& M = q1->getConstRefToAnisotropicTensor();
  const STensor63& DMDFdefo = q1->getConstRefToDAnisotropicTensorDF();
  const STensor63& DMDDeltaEp = q1->getConstRefToDAnisotropicTensorDEp();
  
  static STensor3 SigEff;
  STensorOperation::multSTensor43STensor3(M, sig, SigEff);

  static STensor3 sEff;
  sEff = SigEff.dev();

  double R0 = _j2IH->getYield0();
  double sigEq = sqrt(1.5*sEff.dotprod());
  yf.resize(1);
  yf[0] = (sigEq - R)/R0;
  if (diff)
  {
    STensorOperation::multSTensor3STensor43(sEff,M,(*DFDsig)[0]);
    (*DFDsig)[0] *= (1.5/(sigEq*R0));
    
    (*DFDR)[0] = -1./R0;
   
    static STensor43 temp;
    STensorOperation::multSTensor3STensor63(sig, DMDDeltaEp, temp);
    STensorOperation::multSTensor3STensor43(sigEq,temp,(*DFDDeltaEp)[0]);
    (*DFDDeltaEp)[0] *= (1.5/(sigEq*R0));

    STensorOperation::multSTensor3STensor63(sig, DMDFdefo, temp);
    STensorOperation::multSTensor3STensor43(sigEq,temp,(*DFDdefo)[0]);
    (*DFDdefo)[0] *= (1.5/(sigEq*R0));
  }
};
void mlawAnisotropicPlasticityHill48::getYieldNormal(std::vector<STensor3>& Np, const STensor3& sig, const double R, const IPAnisotropicPlasticity* q0, const IPAnisotropicPlasticity* q1,
                                  bool diff, std::vector<STensor43>* DNpDsig, std::vector<STensor3>* DNpDR, std::vector<STensor43>* DNpDDeltaEp, std::vector<STensor43>* DNpDdefo) const
{
  const STensor43& M = q1->getConstRefToAnisotropicTensor();
  const STensor63& DMDFdefo = q1->getConstRefToDAnisotropicTensorDF();
  const STensor63& DMDDeltaEp = q1->getConstRefToDAnisotropicTensorDEp();
  
  
  int numberYield = getNumOfYieldSurfaces();
  double R0 = _j2IH->getYield0();
  static STensor3 SigEff;
  STensorOperation::multSTensor43STensor3(M, sig, SigEff);
  static STensor3 sEff;
  sEff = SigEff.dev();
  STensorOperation::multSTensor3STensor43(sEff,M,Np[0]);
  Np[0]*= (1.5/R0);
  if (diff)
  {
    STensorOperation::multSTensor43(M,M,(*DNpDsig)[0]);
    (*DNpDsig)[0]*= (1.5/R0);
    
    STensorOperation::zero((*DNpDR)[0]);
    
    static STensor43 tmp;
    STensorOperation::multSTensor3STensor63(sig, DMDDeltaEp, tmp);
    STensorOperation::multSTensor43(M,tmp,(*DNpDDeltaEp)[0]);
    STensorOperation::multSTensor3STensor63Add(sEff, DMDDeltaEp, (*DNpDDeltaEp)[0]);
    (*DNpDDeltaEp)[0] *= (1.5/R0);

    STensorOperation::multSTensor3STensor63(sig, DMDFdefo, tmp);
    STensorOperation::multSTensor43(M,tmp,(*DNpDdefo)[0]);
    STensorOperation::multSTensor3STensor63Add(sEff, DMDFdefo, (*DNpDdefo)[0]);
    (*DNpDdefo)[0] *= (1.5/R0);
  }
};
