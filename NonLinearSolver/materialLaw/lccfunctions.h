#ifndef LCC_FUNCTIONS_H
#define LCC_FUNCTIONS_H 1

#ifdef NONLOCALGMSH
#include "matrix_operations.h"
namespace MFH {
#endif

typedef struct lcctool {

	int Nph;
        double v1;
	double* mu, *kappa;	//elastic bulk and shear moduli of phases in the LCC

	double** A, **dAdmu0, **dAdmu1, **ddAddmu0, **ddAddmu1, **ddAdmu0dmu1; //strain concentration tensor and derivatives w.r.t. mu
	double** C, **C0, **C1, **dCdmu0, **dCdmu1, **ddCddmu0, **ddCddmu1, **ddCdmu0dmu1; //effective stiffness and derivatives w.r.t. mu
        double **dCdp_bar, **ddCdmu0dp_bar, **ddCdmu1dp_bar;   //effective stiffness derivatives w.r.t. mu , p_bar
        double*** dCdstrn,  ***ddCdmu0dstrn, ***ddCdmu1dstrn;  //effective stiffness derivatives w.r.t. marco_epsilon
} Lcc;


typedef struct YieldingFunc {

	double trial;                   // trial= 3*mu_el*eqstrs-(R(p)-Y-3*mu_el*tiny_p) is used to check plasticity
        double f, dfdp_bar, dpdp_bar;		// value of yielding function
	double *dfdmu;	//derivatives w.r. mu0 mu1 and dp_bar(if with damage)
	double *dfdstrn;            //  derivatives w.r. incremental strain of composite
      
	double *dpdmu;    //if there is damage	
	double *dpdstrn;            //  derivatives w.r. incremental strain of composite
}  YieldF ;
#ifdef NONLOCALGMSH
}
#endif


#ifdef NONLOCALGMSH
void malloc_lcc(MFH::Lcc* LCC, int N, double vf_i);
void set_elprops(MFH::Lcc* LCC, double* mu, double* kappa);
void free_lcc(MFH::Lcc* LCC);

void malloc_YF(MFH::YieldF* YFdf);
void free_YF(MFH::YieldF* YFdf);
void CA_dCdA_MT(MFH::Lcc* LCC, double v1, double **S, double **dS, double *dnu);
void CA_2ndCdA_MT(MFH::Lcc* LCC, double v1, double **S, double **dS, double **ddS, double *dnu);

void eqstrn2_order2(int mtx, double* e, MFH::Lcc* LCC,double* es2, double* des2de, double* des2dmu0, double* des2dmu1);

#else
void malloc_lcc(Lcc* LCC, int N, double vf_i);
void set_elprops(Lcc* LCC, double* mu, double* kappa);
void free_lcc(Lcc* LCC);

void malloc_YF(YieldF* YFdf);
void free_YF(YieldF* YFdf);
void CA_dCdA_MT(Lcc* LCC, double v1, double **S, double **dS, double *dnu);
void CA_2ndCdA_MT(Lcc* LCC, double v1, double **S, double **dS, double **ddS, double *dnu);

void eqstrn2_order2(int mtx, double* e, Lcc* LCC,double* es2, double* des2de, double* des2dmu0, double* des2dmu1);
#endif
//
#endif
