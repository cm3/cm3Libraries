//
// C++ Interface: material law
//
// Description: Porous material law with Gurson void growth and coalescence
//
// Author:  <J. Leclerc, V-D Nguyen>, (C) 2017
//
// Copyright: See COPYING file that comes with this distribution
//
//

#include "mlawNonLocalPorousCoupled.h"
#include "nonLinearMechSolver.h"
#include "ipNonLocalPorosity.h"

mlawNonLocalPorousCoupledLaw::mlawNonLocalPorousCoupledLaw(const int num,const double E,const double nu, const double rho,
                const double q1, const double q2, const double q3,
                const double fVinitial, const double lambda0, const J2IsotropicHardening &j2IH, const CLengthLaw &cLLaw,
                const double tol, const bool matrixbyPerturbation, const double pert) :
                    combinedPorousLaw(num, E, nu, rho, fVinitial, j2IH, cLLaw, tol, matrixbyPerturbation, pert),
                    _checkCoalescenceWithNormal(false),_beta(0.), _coalesTol(1.), 
                    _useTwoYieldRegularization(false),_yieldRegExponent(50.),
                    _withCftOffset(true), _withLodeOffset(false), _withYieldOffset(false),
                    _withBothYieldSurfacesInBulk(true)
{
  _mlawGrowth = new mlawNonLocalDamageGurson(num, E, nu, rho, q1, q2, q3, fVinitial, j2IH, cLLaw, tol, matrixbyPerturbation, pert);
  _mlawCoales = new mlawNonLocalPorousThomasonLaw(num, E, nu, rho, fVinitial, lambda0, j2IH, cLLaw, tol, matrixbyPerturbation, pert);
  _multipleLaws.resize(2);
  _multipleLaws[0] = _mlawGrowth;
  _multipleLaws[1] = _mlawCoales;

  // growth law
  SphericalVoidStateEvolutionLawWithMatrixPlasticDeformation voidEvolLaw(num,lambda0,1.);
  NoCoalescenceLaw noCoalesLaw(num);
  _mlawGrowth->setVoidEvolutionLaw(voidEvolLaw);
  _mlawGrowth->setCoalescenceLaw(noCoalesLaw);
  
  // coalescence law
  ThomasonCoalescenceLaw thomasonLaw(num); 
  _mlawCoales->setVoidEvolutionLaw(voidEvolLaw);
  _mlawCoales->setCoalescenceLaw(thomasonLaw);
 
  _failedTol = 0.99;
};

mlawNonLocalPorousCoupledLaw::mlawNonLocalPorousCoupledLaw(const int num,const double E,const double nu, const double rho,
                const double q1, const double q2, const double q3,
                const double fVinitial, const double lambda0, const double kappa, const J2IsotropicHardening &j2IH, const CLengthLaw &cLLaw,
                const double tol, const bool matrixbyPerturbation, const double pert) :
                    combinedPorousLaw(num, E, nu, rho, fVinitial, j2IH, cLLaw, tol, matrixbyPerturbation, pert),
                    _checkCoalescenceWithNormal(false),_beta(0.), _coalesTol(1.),
                    _useTwoYieldRegularization(false),_yieldRegExponent(50.),
                    _withCftOffset(true), _withLodeOffset(false), _withYieldOffset(false),
                    _withBothYieldSurfacesInBulk(true)
{
  _mlawGrowth = new mlawNonLocalDamageGurson(num, E, nu, rho, q1, q2, q3, fVinitial, j2IH, cLLaw, tol, matrixbyPerturbation, pert);
  _mlawCoales = new mlawNonLocalPorousThomasonLaw(num, E, nu, rho, fVinitial, lambda0, kappa, j2IH, cLLaw, tol, matrixbyPerturbation, pert);
  _multipleLaws.resize(2);
  _multipleLaws[0] = _mlawGrowth;
  _multipleLaws[1] = _mlawCoales;
  
  // growth law
  SphericalVoidStateEvolutionLawWithMatrixPlasticDeformation voidEvolLaw(num,lambda0,kappa);
  NoCoalescenceLaw noCoalesLaw(num);
  _mlawGrowth->setVoidEvolutionLaw(voidEvolLaw);
  _mlawGrowth->setCoalescenceLaw(noCoalesLaw);
  
  // coalescence law
  ThomasonCoalescenceLaw thomasonLaw(num); 
  _mlawCoales->setVoidEvolutionLaw(voidEvolLaw);
  _mlawCoales->setCoalescenceLaw(thomasonLaw);
 
  _failedTol = 0.99;
};



mlawNonLocalPorousCoupledLaw::mlawNonLocalPorousCoupledLaw(const mlawNonLocalPorousCoupledLaw &source) :
      combinedPorousLaw(source),
      _checkCoalescenceWithNormal(source._checkCoalescenceWithNormal), _beta(source._beta), _coalesTol(source._coalesTol),
      _useTwoYieldRegularization(source._useTwoYieldRegularization),
      _yieldRegExponent(source._yieldRegExponent),
      _withCftOffset(source._withCftOffset), _withLodeOffset(source._withLodeOffset), _withYieldOffset(source._withYieldOffset),
      _withBothYieldSurfacesInBulk(source._withBothYieldSurfacesInBulk)
{
  _mlawGrowth = dynamic_cast<mlawNonLocalDamageGurson*>(source._mlawGrowth->clone());
  _mlawCoales = dynamic_cast<mlawNonLocalPorousThomasonLaw*>(source._mlawCoales->clone());
   _multipleLaws.resize(2);
  _multipleLaws[0] = _mlawGrowth;
  _multipleLaws[1] = _mlawCoales;

  _failedTol = 0.99;
};

 mlawNonLocalPorousCoupledLaw::~mlawNonLocalPorousCoupledLaw()
{
  if (_mlawGrowth != NULL) {delete _mlawGrowth; _mlawGrowth = NULL;};
  if (_mlawCoales != NULL) {delete _mlawCoales; _mlawCoales = NULL;};
};


void mlawNonLocalPorousCoupledLaw::setVoidEvolutionLaw(const voidStateEvolutionLaw& voidstateLaw)
{
  // mus be specify the one of coalescence or the one of void growth
  Msg::Error("setVoidEvolutionLaw cannot be called, use setGrowthVoidEvolutionLaw or setCoalescenceVoidEvolutionLaw instead");
}
void mlawNonLocalPorousCoupledLaw::setGrowthVoidEvolutionLaw(const voidStateEvolutionLaw& voidstateLaw)
{
  Msg::Info("set growth void evolution law:");
  voidstateLaw.printInfo();
  _mlawGrowth->setVoidEvolutionLaw(voidstateLaw);
};
void mlawNonLocalPorousCoupledLaw::setCoalescenceVoidEvolutionLaw(const voidStateEvolutionLaw& voidstateLaw)
{
  Msg::Info("set coalescence void evolution law:");
  voidstateLaw.printInfo();
  _mlawCoales->setVoidEvolutionLaw(voidstateLaw);
};

void mlawNonLocalPorousCoupledLaw::setCoalescenceLaw(const CoalescenceLaw& added_coalsLaw)
{
  Msg::Info("set coalescescence law:");
  added_coalsLaw.printInfo();
  _mlawCoales->setCoalescenceLaw(added_coalsLaw);
};

// new options
void mlawNonLocalPorousCoupledLaw::useTwoYieldRegularization(const bool fl, const double n)
{
  _useTwoYieldRegularization = fl;
  _yieldRegExponent = n;
  if (_useTwoYieldRegularization)
  {
    Msg::Info("regularized two yield function is considered with exponent %e",n);
  }
}

void  mlawNonLocalPorousCoupledLaw::setYieldSurfaceExponent(const double newN)
{
  dynamic_cast<mlawNonLocalPorousThomasonLaw*>(_mlawCoales)->setYieldSurfaceExponent(newN);
};

void mlawNonLocalPorousCoupledLaw::setLodeOffset(const bool fl){
  _withLodeOffset=fl;
  if (_withLodeOffset)
  {
    Msg::Info("Lode offset is activated");
  }
};

void mlawNonLocalPorousCoupledLaw::setCftOffset(const bool fl){
  _withCftOffset=fl;
  if (_withCftOffset)
  {
    Msg::Info("Cft offset is activated");
  }
}; 

void mlawNonLocalPorousCoupledLaw::setYieldOffset(const bool fl){
  _withYieldOffset=fl;
  if (_withYieldOffset)
  {
    Msg::Info("yield offset is activated");
  }
}; 

void mlawNonLocalPorousCoupledLaw::setCoalescenceTolerance(const double t){
	Msg::Info("modify coalescence tolerance = %e",t);
	_coalesTol = t;
};

void mlawNonLocalPorousCoupledLaw::setBulkCouplingBehaviour(const bool fl)
{
  _withBothYieldSurfacesInBulk = fl;
  if (fl)
  {
    Msg::Info("both yield surface are considered in bulk elements");
  }
};

void mlawNonLocalPorousCoupledLaw::setCrackTransition(const bool fl, const double beta){
  _checkCoalescenceWithNormal = fl;
  _beta = beta;
};

void mlawNonLocalPorousCoupledLaw::createIPState(IPStateBase* &ips,bool hasBodyForce, const bool* state_,const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
  Msg::Error("mlawNonLocalPorousCoupledLaw::createIPState was not defined!!!");
}

void mlawNonLocalPorousCoupledLaw::createIPVariable(const double fvInitial, IPNonLocalPorosity* &ipv) const
{
  // create by coalescence law
  _mlawGrowth->createIPVariable(fvInitial,ipv);
};

void mlawNonLocalPorousCoupledLaw::voidCharacteristicsEvolution_NonLocalPorosity(const STensor3& kcor,
                                                const IPNonLocalPorosity *q0, IPNonLocalPorosity* q1,
                                                const double DeltaHatD, const double DeltaHatQ, const double DeltaHatP,
                                                double& DfVDDeltaHatD, double& DfVDDeltaHatQ, double& DfVDDeltaHatP,
                                                STensor3& DfVDkcorpr) const
{
  bool coalesFlag = q1->getConstRefToIPCoalescence().getCoalescenceOnsetFlag();
  if (coalesFlag)
  {
    _mlawCoales->voidCharacteristicsEvolution_NonLocalPorosity(kcor,q0,q1,DeltaHatD,DeltaHatQ,DeltaHatP,DfVDDeltaHatD,DfVDDeltaHatQ,DfVDDeltaHatP,DfVDkcorpr);
  }
  else
  {
    _mlawGrowth->voidCharacteristicsEvolution_NonLocalPorosity(kcor,q0,q1,DeltaHatD,DeltaHatQ,DeltaHatP,DfVDDeltaHatD,DfVDDeltaHatQ,DfVDDeltaHatP,DfVDkcorpr);
  }
}
void mlawNonLocalPorousCoupledLaw::voidCharacteristicsEvolutionLocal(const STensor3& kcor,
                                            const IPNonLocalPorosity *q0, IPNonLocalPorosity* q1,
                                            const double DeltaHatD, const double DeltaHatQ, const double DeltaHatP,
                                            const double DtildefVstarDDeltafV,
                                            double& DyieldfVDDeltafV,
                                            double& DDeltafVDDeltaHatD, double& DDeltafVDDeltaHatQ, double& DDeltafVDDeltaHatP,
                                            STensor3& DDeltafVDKcor) const
{
  bool coalesFlag = q1->getConstRefToIPCoalescence().getCoalescenceOnsetFlag();
  if (coalesFlag)
  {
    _mlawCoales->voidCharacteristicsEvolutionLocal(kcor,q0,q1,DeltaHatD,DeltaHatQ,DeltaHatP,
                                            DtildefVstarDDeltafV,DyieldfVDDeltafV,
                                            DDeltafVDDeltaHatD,DDeltafVDDeltaHatQ,DDeltafVDDeltaHatP,DDeltafVDKcor);
  }
  else
  {
    _mlawGrowth->voidCharacteristicsEvolutionLocal(kcor,q0,q1,DeltaHatD,DeltaHatQ,DeltaHatP,
                                            DtildefVstarDDeltafV,DyieldfVDDeltafV,
                                            DDeltafVDDeltaHatD,DDeltafVDDeltaHatQ,DDeltafVDDeltaHatP,DDeltafVDKcor);
  }
  
}

void mlawNonLocalPorousCoupledLaw::voidCharacteristicsEvolution_multipleNonLocalVariables(const STensor3& kcor,
                                            const IPNonLocalPorosity *q0, IPNonLocalPorosity* q1,
                                            std::vector<double>& DyieldfVDNonlocalVars,
                                            STensor3& DyieldfVDKcor, const bool blockVoidGrowth
                                            ) const
{
  bool coalesFlag = q1->getConstRefToIPCoalescence().getCoalescenceOnsetFlag();
  if (coalesFlag)
  {
    _mlawCoales->voidCharacteristicsEvolution_multipleNonLocalVariables(kcor,q0,q1,DyieldfVDNonlocalVars,DyieldfVDKcor,blockVoidGrowth);
  }
  else
  {
    _mlawGrowth->voidCharacteristicsEvolution_multipleNonLocalVariables(kcor,q0,q1,DyieldfVDNonlocalVars,DyieldfVDKcor,blockVoidGrowth);
  }
}

void mlawNonLocalPorousCoupledLaw::voidStateGrowth(const double yieldfV0, const double yieldfV1, // yield porosity
                        const double DeltahatD, const double DeltahatQ, const double DeltahatP, // internal variables
                        const IPNonLocalPorosity *q0, IPNonLocalPorosity *q1) const
{
  bool coalesFlag = q1->getConstRefToIPCoalescence().getCoalescenceOnsetFlag();
  if (coalesFlag)
  {
    _mlawCoales->voidStateGrowth(yieldfV0,yieldfV1,DeltahatD,DeltahatQ,DeltahatP,q0,q1);
                        
  }
  else
  {
    _mlawGrowth->voidStateGrowth(yieldfV0,yieldfV1,DeltahatD,DeltahatQ,DeltahatP,q0,q1);
  }
}


double mlawNonLocalPorousCoupledLaw::yieldFunction(const double kcorEq, const double pcor, const double R, const double yieldfV,
                                const IPNonLocalPorosity* q0, const IPNonLocalPorosity* q1, const double* T,
                                bool diff, fullVector<double>* grad,
                                bool withthermic, double* dfdT) const{
  bool coalesFlag = q1->getConstRefToIPCoalescence().getCoalescenceOnsetFlag();

  if (coalesFlag)
  {
    return _mlawCoales->yieldFunction(kcorEq,pcor,R,yieldfV,q0,q1,T,diff,grad,withthermic,dfdT);
  }
  else return _mlawGrowth->yieldFunction(kcorEq,pcor,R,yieldfV,q0,q1,T,diff,grad,withthermic,dfdT);
};


void mlawNonLocalPorousCoupledLaw::plasticFlow(double & Ns, double& Nv,
                              const double kcorEq, const double pcor, const double R,
                              const IPNonLocalPorosity* q0, const IPNonLocalPorosity* q1, const double* T,
                              bool diff, fullVector<double>* gradNs, // grad of Ns with respect to [kcorEq pcor R fV Chi W]
                              fullVector<double>* gradNv, // grad of Nv with respect to  [kcorEq pcor R fV Chi W]
                              bool withTher, double* dNsDT, double* dNvDT
                          ) const
{
  bool coalesFlag = q1->getConstRefToIPCoalescence().getCoalescenceOnsetFlag();
  if (coalesFlag)
  {
    _mlawCoales->plasticFlow(Ns,Nv,kcorEq,pcor,R,q0,q1,T,diff,gradNs,gradNv,withTher,dNsDT,dNvDT);
  }
  else
  {
    _mlawGrowth->plasticFlow(Ns,Nv,kcorEq,pcor,R,q0,q1,T,diff,gradNs,gradNv,withTher,dNsDT,dNvDT);;
  }
}


double mlawNonLocalPorousCoupledLaw::getOnsetCriterion(const IPNonLocalPorosity* q1) const{
  const IPCoalescence* ipcoales = &q1->getConstRefToIPCoalescence();
  if (_withCftOffset)
  {
    return ipcoales->getCrackOffsetOnCft();
  }
  else if (_withLodeOffset)
  {
    return 1./ipcoales->getLodeParameterOffset();
  }
  else if (_withYieldOffset)
  {
    return ipcoales->getYieldOffset();
  }
  else
  {
    return 1.;
  }
}

void mlawNonLocalPorousCoupledLaw::forceCoalescence(IPNonLocalPorosity* q1, const IPNonLocalPorosity *q0, const double* T) const
{
  const IPCoalescence* q0Thom = &q0->getConstRefToIPCoalescence();
  IPCoalescence* q1Thom = &q1->getRefToIPCoalescence();
  
  // force coalesccence without condittion
  q1Thom->getRefToCoalescenceOnsetFlag() = true;
  q1Thom->getRefToCoalescenceActiveFlag() = true;
  q1Thom->setIPvAtCoalescenceOnset(*q1);
  // if offset is used to satisfy Thomason yield surface at the onset of coalescence
  if (_useTwoYieldRegularization)
  {
    // never offset with yield regulation
    q1Thom->getRefToCrackOffsetOnCft() = 1.;
    q1Thom->getRefToLodeParameterOffset() = 1.;
    q1Thom->getRefToYieldOffset() = 0.;
  }
  else
  {
    const STensor3& kCor = q1->getConstRefToCorotationalKirchhoffStress();
    const double R = q1->getCurrentViscoplasticYieldStress();
    const double R0 = _j2IH->getYield0();
    static STensor3 corSig;
    corSig = kCor;
    if (_stressFormulation == mlawNonLocalPorosity::CORO_CAUCHY)
    {
      const STensor3& F = q1->getConstRefToDeformationGradient();
      double J = STensorOperation::determinantSTensor3(F);
      // get corotational Cauchy stress
      corSig *= (1./J);
    }
    
    double Cft;
    _mlawCoales->getCoalescenceLaw()->computeConcentrationFactor(q0,q1,Cft);
    
    if (_withCftOffset)
    {
      double yieldThomason = _mlawCoales->I1J2J3_yieldFunction(corSig,R,q0,q1,T); // always exists
      q1Thom->getRefToCrackOffsetOnCft() = 1. + yieldThomason;
      q1Thom->getRefToLodeParameterOffset() = 1.;
      q1Thom->getRefToYieldOffset() = 0.;
      #ifdef _DEBUG
      printf("force coalescence using Cft offset = %e\n",q1Thom->getCrackOffsetOnCft());
      #endif //_DEBUG
    }
    else if (_withLodeOffset)
    {
      static STensor3 devCorSig;
      static double corSigEq, pres;
      STensorOperation::decomposeDevTr(corSig,devCorSig,pres);
      pres /= 3.;
      corSigEq = sqrt(1.5*devCorSig.dotprod());
      
      q1Thom->getRefToCrackOffsetOnCft() = 1.;
      q1Thom->getRefToLodeParameterOffset()=  1.5*(Cft*R-fabs(pres))/corSigEq;
      q1Thom->getRefToYieldOffset() = 0.;
      #ifdef _DEBUG
      printf("force coalescence using Lode offset = %e\n",q1Thom->getLodeParameterOffset());
      #endif //_DEBUG
    }
    else if (_withYieldOffset)
    {
      q1Thom->getRefToCrackOffsetOnCft() = 1.;
      q1Thom->getRefToLodeParameterOffset() = 1.;
      q1Thom->getRefToYieldOffset() = _mlawCoales->I1J2J3_yieldFunction(corSig,R,q0,q1,T);
      #ifdef _DEBUG
      printf("force coalescence using yield offset = %e\n",q1Thom->getYieldOffset());
      #endif //_DEBUG
    }
    else
    {
      q1Thom->getRefToCrackOffsetOnCft() = 1.;
      q1Thom->getRefToLodeParameterOffset() = 1.;
      q1Thom->getRefToYieldOffset() = 0.;
      #ifdef _DEBUG
      printf("force coalescence , not offset is used\n");
      #endif //_DEBUG
    }
  }
};

bool mlawNonLocalPorousCoupledLaw::checkCrackCriterion(IPNonLocalPorosity* q1, const IPNonLocalPorosity *q0, const STensor3& Fcur, const SVector3& normal, double delayFactor, const double* T) const
{
  // crack is inserted when
 // coalescence occurs
 // and other condition
   if (q1->getConstRefToIPCoalescence().getCoalescenceOnsetFlag())
  {
    if (delayFactor >=1.)
    {
      return true;
    }
    const IPNonLocalPorosity* qOnset = q1->getConstRefToIPCoalescence().getIPvAtCoalescenceOnset();
    if (qOnset == NULL)
    {
      Msg::Error("IPNonLocalPorosity at the onset of coalescence must be introduced");
      Msg::Exit(0);
    }
    
    double Cftonset = 0.;
    _mlawCoales->getCoalescenceLaw()->computeConcentrationFactor(qOnset,qOnset,Cftonset);
    double Cftcurrent = 0.;
     _mlawCoales->getCoalescenceLaw()->computeConcentrationFactor(q0,q1,Cftcurrent);
    if (Cftcurrent <= Cftonset*delayFactor)
    {
      return true;
    }
    else
    {
      return false;
    }
  }
  else
  {
    return false;
  }
};

void mlawNonLocalPorousCoupledLaw::checkCoalescence(IPNonLocalPorosity* q1, const IPNonLocalPorosity *q0, const double* T) const
{
  // This function update the coalescence status of the considered GP.
  // In particular, it determines from the yield surface if the coalescence is reached.
  // It also manages which yield surface should be used.
  // And, if transition is used, the criterion is adapted to evaluate the "directional" 
  // form of the criterion interface. 

  
  // avoid case with several yield surfaces
  /// why ???
  if (getNumOfYieldSurfaces() > 1){
    Msg::Error("check coalescence has not implemented for num yield surface >1");
    return;
  }
  
   // Get ipvs
  const IPCoalescence* q0Thom = &q0->getConstRefToIPCoalescence();
  IPCoalescence* q1Thom = &q1->getRefToIPCoalescence();
  // check active
  if (q0Thom->getCoalescenceOnsetFlag() and q1->dissipationIsActive())
  {
    q1Thom->getRefToCoalescenceActiveFlag() = true;
  }
  else
  {
    q1Thom->getRefToCoalescenceActiveFlag() = false;
  }
  
  // Determine:
  // a. if the coales. state needs to be updated (otherwise, the function returns directly).
  // Namely,
  //  - the coales. state is never updated if coalescence is not allowed in the bulk
  //  - otherwise, the coales. state needs to be updated 
  // b. which criterion needs to be used to update the coalescence state. 
  // Namely, there are 2 cases:
  //  - criterion based on the coalescence criterion using a specific direction:
  //    -- used for interface IP when transition is used following the normal of the interface,
  //  or,
  //  - criterion directly based on the yield surface: 
  //    -- for every other cases.
  // Corresponding flag "willUseNormalToCheck" == true if the directionnal criterion is involved 
  // (false by default as it is the cases the most often)

  
  /// Make the decision...
  /// The flag "willUseNormalToCheck" will be true only if the transition is used and if we are at the interface.
  bool willUseNormalToCheck = false;
  
  if (_checkCoalescenceWithNormal){  /// _checkCoalescenceWithNormal == true if transition is used
    if (q1->getLocation() == IPVariable::INTERFACE_MINUS or q1->getLocation() == IPVariable::INTERFACE_PLUS){
      willUseNormalToCheck = true;
      
      // is this still needed ?
      const SVector3& normal = q1->getConstRefToCurrentOutwardNormal();
      if (normal.norm() <=0.)
      {
        Msg::Error("coalescence cannot check with zero normal!!!");
        return;
      };
      
    }
    else{
      if (!_withBothYieldSurfacesInBulk){
        return;
      }
    }
  }
  
  
  if (q0Thom->getCoalescenceOnsetFlag())
  {
    // if onset already occurs
    q1Thom->getRefToCoalescenceOnsetFlag() = true;    
    // copy data
    q1Thom->setIPvAtCoalescenceOnset(*q0Thom->getIPvAtCoalescenceOnset());

    q1Thom->getRefToCrackOffsetOnCft() = q0Thom->getCrackOffsetOnCft();
    q1Thom->getRefToLodeParameterOffset() = q0Thom->getLodeParameterOffset();
    q1Thom->getRefToYieldOffset() = q0Thom->getYieldOffset();
  }
  else if (q1->dissipationIsActive())
  {
    /// Get usefull data stored in IPV
    const STensor3& kCor = q1->getConstRefToCorotationalKirchhoffStress();
    const STensor3& F = q1->getConstRefToDeformationGradient();
    const double R = q1->getCurrentViscoplasticYieldStress();
    const double R0 = _j2IH->getYield0();
    const double yieldfV = q1->getYieldPorosity();

    double J = STensorOperation::determinantSTensor3(F);
    static STensor3 corSig;
    corSig = kCor;
    if (_stressFormulation == mlawNonLocalPorosity::CORO_CAUCHY){
      corSig *= (1./J);
    }
    
    static STensor3 devCorSig;
    static double corSigEq, pres;
    STensorOperation::decomposeDevTr(corSig,devCorSig,pres);
    pres /= 3.;
    corSigEq = sqrt(1.5*devCorSig.dotprod());
    
    double Cft;
    _mlawCoales->getCoalescenceLaw()->computeConcentrationFactor(q0,q1,Cft);
    
    // coalescence never occurs before
    // check at interface first
    bool interfaceActive = true;
    if (willUseNormalToCheck){
      // Evaluate the coalescence state from the current normal traction
      // force w.r.t. the current normal interface by
      // 1. using the stress tensor in the current/deformed configuration
      // 2. deducing from it the effective traction force
      // 3. and then, evaluating the criterion
      
  
      // 1. Compute the stress tensor Sigma (or Kir.) from the corrotational version
      // following: Sig = (Fe)^-T * sigCor * (Fe)^T = Fe * sigCor * Fe^-1
      /// Get cache tensor
      static STensor3 invFp, invFe, FeSigCor , Fe, Sig;
      STensorOperation::inverseSTensor3(q1->getConstRefToFp(), invFp);
      STensorOperation::multSTensor3(F, invFp, Fe);
      STensorOperation::inverseSTensor3(Fe, invFe);
      STensorOperation::multSTensor3(Fe, corSig, FeSigCor);
      STensorOperation::multSTensor3(FeSigCor, invFe, Sig);
      
      // 2. Compute the effective traction
      /// Get the normal and the traction vector
      SVector3 normal = q1->getConstRefToCurrentOutwardNormal();
      normal.normalize();
      static SVector3 tractionForce;
      STensorOperation::multSTensor3SVector3(Sig,normal,tractionForce);
      
      /// Get the normal and tangential part of the traction force vector 
      double tauN = STensorOperation::dot(tractionForce,normal);
      double tauTanSq = 0.;
      for (int i=0; i<3; i++){
        double temp = tractionForce(i)-tauN*normal(i);
        tauTanSq += temp*temp;
      }
      /// Get the effective stress using the beta coefficient
      double tauEff = 0.; 
      if (tauN > 0.){
        tauEff = sqrt(tauN*tauN + _beta*tauTanSq);
      }
      else{
        tauEff = sqrt(_beta*tauTanSq);
      }
      
      if (tauEff - Cft*R > 0.){
        interfaceActive = true;
      } 
      else{
        interfaceActive = false;
      }
    }
    
    bool activeCoales = false;
    
    // if not active with interface criterion
    // check with yield condition
    if (_useTwoYieldRegularization){
      // in case of two yield surface regulization
      double Gurson = _mlawGrowth->I1J2J3_yieldFunction(corSig,R,q0,q1,T);
      double Thomason = _mlawCoales->I1J2J3_yieldFunction(corSig,R,q0,q1,T);
      double Phi = this->I1J2J3_yieldFunction(corSig,R,q0,q1,T);
      //
      double FG = Gurson +1.;
      double FT = Thomason + 1;
      if (FT > FG)
      {
        double ratio = FG/FT;
        if (ratio < 0.)
        {
          Msg::Error("FT FG must be all positive but FT=%e, FG=%e",FT,FG);
        }
        if (pow(ratio,_yieldRegExponent) < _coalesTol && interfaceActive)
        {
          activeCoales = true;
          printf("coalescence occurs with ratio = %e\n",ratio);
        }
      }
    }
    else
    {
      // thomason yield condition
      double yieldThomason = _mlawCoales->yieldFunction(corSigEq,pres,R,yieldfV,q0,q1,T);
      if (yieldThomason > 0 && interfaceActive)
      {
        // coalescene occurs
        activeCoales = true;
        printf("coalescence occurs with Thomason yield surface = %e\n",yieldThomason);
      }
    }
    
    
    // if coalescence occurs
    if (activeCoales)
    {
      q1Thom->getRefToCoalescenceOnsetFlag() = true;
      q1Thom->setIPvAtCoalescenceOnset(*q1);
      // if offset is used to satisfy Thomason yield surface at the onset of coalescence
      if (_useTwoYieldRegularization)
      {
        // never offset with yield regulation
        q1Thom->getRefToCrackOffsetOnCft() = 1.;
        q1Thom->getRefToLodeParameterOffset() = 1.;
        q1Thom->getRefToYieldOffset() = 0.;
      }
      else
      {
        if (_withCftOffset)
        {
          double yieldThomason = _mlawCoales->yieldFunction(corSigEq,pres,R,yieldfV,q0,q1,T);
          q1Thom->getRefToCrackOffsetOnCft() = 1. + yieldThomason;
          q1Thom->getRefToLodeParameterOffset() = 1.;
          q1Thom->getRefToYieldOffset() = 0.;
          #ifdef _DEBUG
          printf("coalescence ocurrs using Cft offset = %e\n",q1Thom->getCrackOffsetOnCft());
          #endif //_DEBUG
        }
        else if (_withLodeOffset)
        {
          q1Thom->getRefToCrackOffsetOnCft() = 1.;
          q1Thom->getRefToLodeParameterOffset()=  1.5*(Cft*R-fabs(pres))/corSigEq;
          q1Thom->getRefToYieldOffset() = 0.;
          #ifdef _DEBUG
          printf("coalescence ocurrs using Lode offset = %e\n",q1Thom->getLodeParameterOffset());
          #endif //_DEBUG
        }
        else if (_withYieldOffset)
        {
          q1Thom->getRefToCrackOffsetOnCft() = 1.;
          q1Thom->getRefToLodeParameterOffset() = 1.;
          q1Thom->getRefToYieldOffset() = _mlawCoales->yieldFunction(corSigEq,pres,R,yieldfV,q0,q1,T);
          #ifdef _DEBUG
          printf("coalescence ocurrs using yield offset = %e\n",q1Thom->getYieldOffset());
          #endif //_DEBUG
        }
        else
        {
          q1Thom->getRefToCrackOffsetOnCft() = 1.;
          q1Thom->getRefToLodeParameterOffset() = 1.;
          q1Thom->getRefToYieldOffset() = 0.;
          #ifdef _DEBUG
          printf("coalescence ocurrs, not offset is used\n");
          #endif //_DEBUG
        }
      }
    }
    else
    {
      q1Thom->getRefToCoalescenceOnsetFlag() = false;
      q1Thom->getRefToCrackOffsetOnCft() = 1.;
      q1Thom->getRefToLodeParameterOffset() = 1.;
      q1Thom->getRefToYieldOffset() = 0.;
    }
  }
};


void mlawNonLocalPorousCoupledLaw::checkFailed(IPNonLocalPorosity* q1, const IPNonLocalPorosity *q0) const
{
  // Function to manage the flag to assess failure status:
    // The evaluation of the function is based on the "_failedTol" parameter (close to 1. but < 1.).
    // When a condition for failure is met, the flag is set to true. And once it is considered as failed,
    // it stays failed.
  
  // Assess the flag status:
  // If a IPVs is previously failed, it always be failed.
  // Otherwise, check if one conditions for failure is met or not to eventually change the status
  if (q0->isFailed()){
    q1->setFailed(true);
  }
  else{
    q1->setFailed(false);
    
    // Get correct ipvs to obtain the needed datas for conditions evaluation
    const IPCoalescence* q0Thom = &q0->getConstRefToIPCoalescence();
    IPCoalescence* q1Thom =&q1->getRefToIPCoalescence();
        
    // Get data for condition evaluation
    double yieldfV = q1->getYieldPorosity();
    double Chi = q1->getConstRefToIPVoidState().getVoidLigamentRatio();
    double Cft;
    _mlawCoales->getCoalescenceLaw()->computeConcentrationFactor(q0,q1,Cft);
    double CftOffest = q1Thom->getCrackOffsetOnCft();
    
    // Evaluate each condition, and if one is true, the IPV is considered as failed
    if (Cft*CftOffest < (1.-_failedTol)*3.){
      // 1st case: the used concentration factor has a low value close to its min. possible (=0)
      q1->setFailed(true);
      #ifdef _DEBUG
      Msg::Info("mlawNonLocalPorousCoupledLaw::checkFailed: an ip failed with Cft at Cft= %e, chi= %e, yieldfV= %e", Cft, Chi, yieldfV);
      #endif //_DEBUG
    }
    else if (Chi > _failedTol){
      // 2nd case: the effective concentration factor has a high value near its max. possible (=1)
      q1->setFailed(true);
      #ifdef _DEBUG
      Msg::Info("mlawNonLocalPorousCoupledLaw::checkFailed: an ip failed with Chi at Cft= %e, chi= %e, yieldfV= %e", Cft, Chi, yieldfV);
      #endif //_DEBUG
    }
    else if (yieldfV > _failedTol*_mlawGrowth->getFailureYieldPorosityValue()){
      // 3rd case: the used yield porosity has a high value near its max. possible 
      // (= fV_failure i.e. fV for which the yeild surface vanishes)
      q1->setFailed(true);
      #ifdef _DEBUG
      Msg::Info("mlawNonLocalPorousCoupledLaw::checkFailed: an ip failed with fVy at Cft= %e, chi= %e, yieldfV= %e", Cft, Chi, yieldfV);
      #endif //_DEBUG
    }
  }
};



void mlawNonLocalPorousCoupledLaw::I1J2J3_voidCharacteristicsEvolution_NonLocal(const STensor3& sig,
                                                  const IPNonLocalPorosity *q0, IPNonLocalPorosity* q1,
                                                  bool stiff,
                                                  std::vector<double>* Y,
                                                  std::vector<STensor3>* DYDsig,
                                                  std::vector<std::vector<double> >* DYDNonlocalVars
                                                  ) const
{
  bool coalesFlag = q1->getConstRefToIPCoalescence().getCoalescenceOnsetFlag();
  if (coalesFlag)
  {
    // growth phase
    _mlawCoales->I1J2J3_voidCharacteristicsEvolution_NonLocal(sig,q0,q1,stiff,Y,DYDsig,DYDNonlocalVars);
  }
  else
  {
    _mlawGrowth->I1J2J3_voidCharacteristicsEvolution_NonLocal(sig,q0,q1,stiff,Y,DYDsig,DYDNonlocalVars);
  }
};

void mlawNonLocalPorousCoupledLaw::I1J2J3_voidCharacteristicsEvolution_Local(const STensor3& sig, const STensor3& DeltaEp, const double DeltaHatP,
                                                  const IPNonLocalPorosity *q0, IPNonLocalPorosity* q1,
                                                  bool stiff,
                                                  std::vector<double>* Y,
                                                  std::vector<STensor3>* DYDsig,
                                                  std::vector<STensor3>* DYDDeltaEp,
                                                  std::vector<double>* DYDDeltaHatP
                                                  ) const
{
  bool coalesFlag = q1->getConstRefToIPCoalescence().getCoalescenceOnsetFlag();
  if (coalesFlag)
  {
    // growth phase
    _mlawCoales->I1J2J3_voidCharacteristicsEvolution_Local(sig,DeltaEp,DeltaHatP,q0,q1,stiff,Y,DYDsig,DYDDeltaEp,DYDDeltaHatP);
  }
  else
  {
    _mlawGrowth->I1J2J3_voidCharacteristicsEvolution_Local(sig,DeltaEp,DeltaHatP,q0,q1,stiff,Y,DYDsig,DYDDeltaEp,DYDDeltaHatP);
  }
};

double mlawNonLocalPorousCoupledLaw::I1J2J3_yieldFunction(const STensor3& kcor, const double R, const IPNonLocalPorosity* q0, const IPNonLocalPorosity* q1, const double* T,
                                                          bool diff, STensor3* DFDkcor, double* DFDR, std::vector<double>* DFDY,
                                                          bool withThermic, double *DFDT) const
{
  if (_useTwoYieldRegularization)
  {
    int Ysize = getNumOfVoidCharacteristics();
    static STensor3 DGursonDkcor;
    double DGursonDR;
    static std::vector<double> DGursonDY(Ysize,0.);
    double DGursonDT;
    double Gurson = _mlawGrowth->I1J2J3_yieldFunction(kcor,R,q0,q1,T,diff,&DGursonDkcor,&DGursonDR,&DGursonDY,withThermic,&DGursonDT);

    static STensor3 DThomasonDkcor;
    double DThomasonDR;
    static std::vector<double> DThomasonDY(Ysize,0.);
    double DThomasonDT;
    double Thomason = _mlawCoales->I1J2J3_yieldFunction(kcor,R,q0,q1,T,diff,&DThomasonDkcor,&DThomasonDR,&DThomasonDY,withThermic,&DThomasonDT);

    double FG = Gurson +1.;
    double FT = Thomason +1.;

    double Phi,DPhiDFG,DPhiDFT;
    regulizationYieldFunctionBase(_yieldRegExponent,FG,FT,Phi,diff,&DPhiDFG,&DPhiDFT);
    //Msg::Info("Gurson = %e Thomason = %e Phi = %e FG = %e, FT = %e",Gurson,Thomason,Phi-1.,FG,FT);
    if (diff)
    {
      double DFGDGurson = 1.;
      double DFTDThomason = 1;
      //
      STensorOperation::zero(*DFDkcor);
      DFDkcor->daxpy(DGursonDkcor,DPhiDFG*DFGDGurson);
      DFDkcor->daxpy(DThomasonDkcor,DPhiDFT*DFTDThomason);

      //
      *DFDR = DPhiDFG*DFGDGurson*DGursonDR + DPhiDFT*DFTDThomason*DThomasonDR;
      //
      for (int i=0; i< getNumOfVoidCharacteristics(); i++){
        (*DFDY)[i] = DPhiDFG*DFGDGurson*DGursonDY[i] + DPhiDFT*DFTDThomason*DThomasonDY[i];
      }

      //
      if (withThermic)
      {
        (*DFDT) = DPhiDFG*DFGDGurson*DGursonDT + DPhiDFT*DFTDThomason*DThomasonDT;
      }
    }
    return Phi-1.;
  }
  else
  {
    bool coalesFlag = q1->getConstRefToIPCoalescence().getCoalescenceOnsetFlag();
    if (coalesFlag)
    {
      return _mlawCoales->I1J2J3_yieldFunction(kcor,R,q0,q1,T,diff,DFDkcor,DFDR,DFDY,withThermic,DFDT);
    }
    else
    {
      return _mlawGrowth->I1J2J3_yieldFunction(kcor,R,q0,q1,T,diff,DFDkcor,DFDR,DFDY,withThermic,DFDT);
    }
  }
};



void mlawNonLocalPorousCoupledLaw::I1J2J3_getYieldNormal(STensor3& Np, const STensor3& kcor, const double R, const IPNonLocalPorosity* q0, const IPNonLocalPorosity* q1, const double* T,
          bool diff, STensor43* DNpDkcor, STensor3* DNpDR, std::vector<STensor3>* DNpDY,
          bool withThermic, STensor3* DNpDT) const{
  if (_useTwoYieldRegularization)
  {
    int Ysize =getNumOfVoidCharacteristics();
    static STensor3 DGursonDkcor;
    double DGursonDR;
    static std::vector<double> DGursonDY(Ysize,0.);
    double DGursonDT;
    double Gurson = _mlawGrowth->I1J2J3_yieldFunction(kcor,R,q0,q1,T,diff,&DGursonDkcor,&DGursonDR,&DGursonDY,withThermic,&DGursonDT);

    static STensor43 DNpGDkcor;
    static std::vector<STensor3> DNpGDY(Ysize,STensor3(0.));
    static STensor3 DNpGDR;
    static STensor3 DNpGDT;
    static STensor3 NpG;
    _mlawGrowth->I1J2J3_getYieldNormal(NpG,kcor,R,q0,q1,T,diff,&DNpGDkcor,&DNpGDR,&DNpGDY,withThermic,&DNpGDT);

    static STensor3 DThomasonDkcor;
    double DThomasonDR;
    static std::vector<double> DThomasonDY(Ysize,0.);
    double DThomasonDT;
    double Thomason = _mlawCoales->I1J2J3_yieldFunction(kcor,R,q0,q1,T,diff,&DThomasonDkcor,&DThomasonDR,&DThomasonDY,withThermic,&DThomasonDT);

    static STensor43 DNpTDkcor;
    static std::vector<STensor3> DNpTDY(Ysize,STensor3(0.));
    static STensor3 DNpTDR;
    static STensor3 DNpTDT;
    static STensor3 NpT;
    _mlawCoales->I1J2J3_getYieldNormal(NpT,kcor,R,q0,q1,T,diff,&DNpTDkcor,&DNpTDR,&DNpTDY,withThermic,&DNpTDT);

    double FG = Gurson +1.;
    double FT = Thomason+1.;

    double Phi,DPhiDFG,DPhiDFT, DDPhiDFG_DFG, DDPhiDFG_DFT, DDPhiDFT_DFG, DDPhiDFT_DFT;
    regulizationYieldFunctionBase(_yieldRegExponent,FG,FT,Phi,true,&DPhiDFG,&DPhiDFT,diff, &DDPhiDFG_DFG, &DDPhiDFG_DFT, &DDPhiDFT_DFG, &DDPhiDFT_DFT);

    // Np = R0*DPhiDkcor
    double DFGDGurson = 1.;
    double DFTDThomason = 1.;

    STensorOperation::zero(Np);
    Np.daxpy(NpG,DPhiDFG*DFGDGurson);
    Np.daxpy(NpT,DPhiDFT*DFTDThomason);

    //Msg::Info("Gurson = %e, Thomason = %e, factGurson = %e, FactThomason = %e",Gurson,Thomason,DPhiDFG*DFGDGurson,DPhiDFT*DFTDThomason);
    if (diff)
    {
      // first term
      // Np = R0*DPhiDkcor = DPhiDGurson*NpG + DPhiDThomason*NpT;
      double DPhiDGurson = DPhiDFG*DFGDGurson;
      double DPhiDThomason = DPhiDFT*DFTDThomason;

      //
      *DNpDkcor = DNpGDkcor;
      *DNpDkcor *= DPhiDGurson;
      DNpDkcor->axpy(DPhiDThomason,DNpTDkcor);

      //
      *DNpDR = DNpGDR;
      *DNpDR *= DPhiDGurson;
      DNpDR->daxpy(DNpTDR,DPhiDThomason);

      //
      for (int i=0; i< Ysize; i++)
      {
        (*DNpDY)[i] = DNpGDY[i];
        (*DNpDY)[i] *= DPhiDGurson;
        (*DNpDY)[i].daxpy(DNpTDY[i],DPhiDThomason);
      }
      if (withThermic)
      {
        *DNpDT = DNpGDT;
        *DNpDT *= (DPhiDGurson);
        DNpDT->daxpy(DNpTDT,DPhiDThomason);
      }

      // other part
      double DDPhiDGursonDGurson = DDPhiDFG_DFG*DFGDGurson*DFGDGurson;
      double DDPhiDGursonDThomason = DDPhiDFG_DFT*DFTDThomason*DFGDGurson;
      double DDPhiDThomasonDGurson = DDPhiDFT_DFG*DFGDGurson*DFTDThomason;
      double DDPhiDThomasonDThomason = DDPhiDFT_DFT*DFTDThomason*DFTDThomason;

      //
      STensorOperation::prodAdd(NpG,DGursonDkcor,DDPhiDGursonDGurson,*DNpDkcor);
      STensorOperation::prodAdd(NpG,DThomasonDkcor,DDPhiDGursonDThomason,*DNpDkcor);
      STensorOperation::prodAdd(NpT,DGursonDkcor,DDPhiDThomasonDGurson,*DNpDkcor);
      STensorOperation::prodAdd(NpT,DThomasonDkcor,DDPhiDThomasonDThomason,*DNpDkcor);

      //
      DNpDR->daxpy(NpG,DDPhiDGursonDGurson*DGursonDR);
      DNpDR->daxpy(NpG,DDPhiDGursonDThomason*DThomasonDR);
      DNpDR->daxpy(NpT,DDPhiDThomasonDGurson*DGursonDR);
      DNpDR->daxpy(NpT,DDPhiDThomasonDThomason*DThomasonDR);


      for (int i=0; i< Ysize; i++)
      {
        (*DNpDY)[i].daxpy(NpG,DDPhiDGursonDGurson*DGursonDY[i]);
        (*DNpDY)[i].daxpy(NpG,DDPhiDGursonDThomason*DThomasonDY[i]);
        (*DNpDY)[i].daxpy(NpT,DDPhiDThomasonDGurson*DGursonDY[i]);
        (*DNpDY)[i].daxpy(NpT,DDPhiDThomasonDThomason*DThomasonDY[i]);
      }

      if (withThermic)
      {
        DNpDT->daxpy(NpG,DDPhiDGursonDGurson*DGursonDT);
        DNpDT->daxpy(NpG,DDPhiDGursonDThomason*DThomasonDT);
        DNpDT->daxpy(NpT,DDPhiDThomasonDGurson*DGursonDT);
        DNpDT->daxpy(NpT,DDPhiDThomasonDThomason*DThomasonDT);
      }
    }
  }
  else
  {
    bool coalesFlag = q1->getConstRefToIPCoalescence().getCoalescenceOnsetFlag();
    if (coalesFlag)
    {
      _mlawCoales->I1J2J3_getYieldNormal(Np,kcor,R,q0,q1,T,diff,DNpDkcor,DNpDR,DNpDY,withThermic,DNpDT);
    }
    else
    {
      _mlawGrowth->I1J2J3_getYieldNormal(Np,kcor,R,q0,q1,T,diff,DNpDkcor,DNpDR,DNpDY,withThermic,DNpDT);
    }
  }
};


mlawNonLocalPorousCoupledLawWithMPS::mlawNonLocalPorousCoupledLawWithMPS(const int num,const double E,const double nu, const double rho,
                const double q1, const double q2, const double q3,
                const double fVinitial, const double lambda0, const J2IsotropicHardening &j2IH, const CLengthLaw &cLLaw,
                const double tol, const bool matrixbyPerturbation, const double pert) :
                    mlawNonLocalPorousCoupledLaw(num, E, nu, rho, q1,q2,q3, fVinitial,lambda0, j2IH, cLLaw, tol, matrixbyPerturbation, pert)
{
  if (_mlawGrowth != NULL) delete _mlawGrowth;
  _mlawGrowth = new mlawNonLocalDamageGurson(num, E, nu, rho, q1, q2, q3, fVinitial, j2IH, cLLaw, tol, matrixbyPerturbation, pert);
  if (_mlawCoales != NULL) delete _mlawCoales;
  _mlawCoales = new mlawNonLocalPorousThomasonLawWithMPS(num, E, nu, rho, fVinitial, lambda0, j2IH, cLLaw, tol, matrixbyPerturbation, pert);
  _multipleLaws.resize(2);
  _multipleLaws[0] = _mlawGrowth;
  _multipleLaws[1] = _mlawCoales;
};

mlawNonLocalPorousCoupledLawWithMPS::mlawNonLocalPorousCoupledLawWithMPS(const int num,const double E,const double nu, const double rho,
                const double q1, const double q2, const double q3,
                const double fVinitial, const double lambda0, const double kappa, const J2IsotropicHardening &j2IH, const CLengthLaw &cLLaw,
                const double tol, const bool matrixbyPerturbation, const double pert) :
                    mlawNonLocalPorousCoupledLaw(num, E, nu, rho, q1,q2,q3, fVinitial,lambda0,kappa, j2IH, cLLaw, tol, matrixbyPerturbation, pert)
{
  if (_mlawGrowth != NULL) delete _mlawGrowth;
  _mlawGrowth = new mlawNonLocalDamageGurson(num, E, nu, rho, q1, q2, q3, fVinitial, j2IH, cLLaw, tol, matrixbyPerturbation, pert);
  if (_mlawCoales) delete _mlawCoales;
  _mlawCoales = new mlawNonLocalPorousThomasonLawWithMPS(num, E, nu, rho, fVinitial, lambda0, kappa, j2IH, cLLaw, tol, matrixbyPerturbation, pert);
  _multipleLaws.resize(2);
  _multipleLaws[0] = _mlawGrowth;
  _multipleLaws[1] = _mlawCoales;
};

void mlawNonLocalPorousCoupledLawWithMPS::checkCoalescence(IPNonLocalPorosity* q1, const IPNonLocalPorosity *q0, const double* T) const
{
  // check condition if check coalescence is carried out
  if (getNumOfYieldSurfaces() > 1) {
    Msg::Error("check coalescenece has not implemented for num yield surface >1");
    return;
  }
  
  // Get ipvs
  const IPCoalescence* q0Thom = &q0->getConstRefToIPCoalescence();
  IPCoalescence* q1Thom = &q1->getRefToIPCoalescence();
  // check active
  if (q0Thom->getCoalescenceOnsetFlag() and q1->dissipationIsActive())
  {
    q1Thom->getRefToCoalescenceActiveFlag() = true;
  }
  else
  {
    q1Thom->getRefToCoalescenceActiveFlag() = false;
  }
  
  
  bool willUseNormalToCheck = false;
  if (_checkCoalescenceWithNormal)
  {
    if (q1->getLocation() == IPVariable::INTERFACE_MINUS or q1->getLocation() == IPVariable::INTERFACE_PLUS)
    {
      
      willUseNormalToCheck = true;
      const SVector3& normal = q1->getConstRefToCurrentOutwardNormal();
      if (normal.norm() <=0.)
      {
        Msg::Error("coalescence cannot check with zero normal!!!");
        return;
      };
    }
    else
    {
      willUseNormalToCheck = false;
      if (!_withBothYieldSurfacesInBulk)
      {
        return;
      }
    }  
  }
  
  if (q0Thom->getCoalescenceOnsetFlag())
  {
    // if onset already occurs
    q1Thom->getRefToCoalescenceOnsetFlag() = true;
    // copy data
    q1Thom->setIPvAtCoalescenceOnset(*q0Thom->getIPvAtCoalescenceOnset());

    q1Thom->getRefToCrackOffsetOnCft() = q0Thom->getCrackOffsetOnCft();
    q1Thom->getRefToLodeParameterOffset() = q0Thom->getLodeParameterOffset();
    q1Thom->getRefToYieldOffset() = q0Thom->getYieldOffset();
  }
  else if (q1->dissipationIsActive())
  {
    const STensor3& kCor = q1->getConstRefToCorotationalKirchhoffStress();
    const STensor3& F = q1->getConstRefToDeformationGradient();
    const double R = q1->getCurrentViscoplasticYieldStress();
    const double R0 = _j2IH->getYield0();
    const double yieldfV = q1->getYieldPorosity();

    double J = STensorOperation::determinantSTensor3(F);
    static STensor3 corSig;
    corSig = kCor;
    if (_stressFormulation == mlawNonLocalPorosity::CORO_CAUCHY)
    {
      // get corotational Cauchy stress
      corSig *= (1./J);
    }
    
    
    bool interfaceActive = true;
    if (willUseNormalToCheck)
    {
      double Cft;
      _mlawCoales->getCoalescenceLaw()->computeConcentrationFactor(q0,q1,Cft);
    
    
      SVector3 normal = q1->getConstRefToCurrentOutwardNormal();
      normal.normalize();
  
      // Compute Sig from Kcor following: Sig = invFeT * sigCor * FeT = Fe*sigCor*invFe
      static STensor3 invFep, FeSigCor , Fe, Sig;
      STensorOperation::inverseSTensor3(q1->getConstRefToFp(), invFep);
      STensorOperation::multSTensor3(F, invFep, Fe);
      STensorOperation::inverseSTensor3(Fe, invFep);
      STensorOperation::multSTensor3(Fe, corSig, FeSigCor);
      STensorOperation::multSTensor3SecondTranspose(FeSigCor, invFep, Sig);
      
      // traction vector
      static SVector3 tractionForce;
      STensorOperation::multSTensor3SVector3(Sig,normal,tractionForce);
      double tauN = STensorOperation::dot(tractionForce,normal);
      // Deduce tangent part
      double tauTanSq = 0.;
      for (int i=0; i<3; i++)
      {
        double temp = tractionForce(i)-tauN*normal(i);
        tauTanSq += temp*temp;
      }
      // Compute effective stress
      double tauEff = 0.; 
      if (tauN > 0.)
      {
        tauEff = sqrt(tauN*tauN + _beta*tauTanSq);
      }
      else
      {
        tauEff = sqrt(_beta*tauTanSq);
      }
      
      if (tauEff - Cft*R >0)
      {
        interfaceActive = true;
      } 
      else
      {
        interfaceActive = false;
      }
    }
    
    // coalescence never occurs before
    // check at interface first
    bool activeCoales = false;
    
    if (_useTwoYieldRegularization)
    {
      // in case of two yield surface regulization
      double Gurson = _mlawGrowth->I1J2J3_yieldFunction(corSig,R,q0,q1,T);
      double Thomason = _mlawCoales->I1J2J3_yieldFunction(corSig,R,q0,q1,T);
      double Phi = this->I1J2J3_yieldFunction(corSig,R,q0,q1,T);
      //
      double FG = Gurson +1.;
      double FT = Thomason +1.;
      if (FT > FG)
      {
        double ratio = FG/FT;
        if (ratio < 0.)
        {
          Msg::Error("FT FG must be all positive but FT=%e, FG=%e",FT,FG);
        }
        if (pow(ratio,_yieldRegExponent) < _coalesTol && interfaceActive)
        {
          activeCoales = true;
          printf("coalescence occurs with ratio = %e\n",ratio);
        }
      }
    }
    else
    {
      // thomason yield condition
      double yieldThomason = _mlawCoales->I1J2J3_yieldFunction(corSig,R,q0,q1,T);
      if (yieldThomason > 0 && interfaceActive)
      {
        // coalescene occurs
        activeCoales = true;
        printf("coalescence occurs with Thomason yield surface = %e\n",yieldThomason);
      }
    }
    
    
    // if coalesnce occurs
    if (activeCoales)
    {
      q1Thom->getRefToCoalescenceOnsetFlag() = true;
      q1Thom->setIPvAtCoalescenceOnset(*q1);
      // if offset is used to satisfy Thomason yield surface at the onset of coalescence
      if (_useTwoYieldRegularization)
      {
        // never offset with yield regulation
        q1Thom->getRefToCrackOffsetOnCft() = 1.;
        q1Thom->getRefToLodeParameterOffset() = 1.;
        q1Thom->getRefToYieldOffset() = 0.;
      }
      else
      {
        if (_withCftOffset)
        {
          double yieldThomason = _mlawCoales->I1J2J3_yieldFunction(corSig,R,q0,q1,T);
          q1Thom->getRefToCrackOffsetOnCft() = 1. + yieldThomason;
          q1Thom->getRefToLodeParameterOffset() = 1.;
          q1Thom->getRefToYieldOffset() = 0.;
          #ifdef _DEBUG
          printf("coalescence ocurrs using Cft offset = %e\n",q1Thom->getCrackOffsetOnCft());
          #endif //_DEBUG
        }
        else if (_withLodeOffset)
        {
          // Lode offset is not used for material law, because law already accounts for Lode variable
          q1Thom->getRefToCrackOffsetOnCft() = 1.;
          q1Thom->getRefToLodeParameterOffset()=  1.;
          q1Thom->getRefToYieldOffset() = 0.;
        }
        else if (_withYieldOffset)
        {
          q1Thom->getRefToCrackOffsetOnCft() = 1.;
          q1Thom->getRefToLodeParameterOffset() = 1.;
          q1Thom->getRefToYieldOffset() = _mlawCoales->I1J2J3_yieldFunction(corSig,R,q0,q1,T);
          #ifdef _DEBUG
          printf("coalescence ocurrs using yield offset = %e\n",q1Thom->getYieldOffset());
          #endif //_DEBUG
        }
        else
        {
          q1Thom->getRefToCrackOffsetOnCft() = 1.;
          q1Thom->getRefToLodeParameterOffset() = 1.;
          q1Thom->getRefToYieldOffset() = 0.;
          #ifdef _DEBUG
          printf("coalescence ocurrs, not offset is used\n");
          #endif //_DEBUG
        }
      }
    }
    else
    {
      q1Thom->getRefToCoalescenceOnsetFlag() = false;
      q1Thom->getRefToCrackOffsetOnCft() = 1.;
      q1Thom->getRefToLodeParameterOffset() = 1.;
      q1Thom->getRefToYieldOffset() = 0.;
    }
  }
};


void mlawNonLocalPorousCoupledHill48LawWithMPS::setYieldParameters(const std::vector<double>& Mvec)
{
  if (Mvec.size() != 9)
  {
    Msg::Error("mlawNonLocalDamageGursonHill48 requires 9 parameters to define yield surface");
    Msg::Exit(0);
  }
  STensorOperation::zero(_aniM);
  
  Msg::Info("-----\nHill parameters");
  for (int i=0; i< Mvec.size(); i++)
  {
    printf("%.16g ", Mvec[i]);
  }
  Msg::Info("\n------");
    
  // [F, G, H, L, M, N] = Mvec
  double F = Mvec[0];
  double G = Mvec[1];
  double H = Mvec[2];
  double L = Mvec[3];
  double M = Mvec[4];
  double N = Mvec[5];
  double P = Mvec[6];
  double Q = Mvec[7];
  double R = Mvec[8];
  // define the equivalent stress under the form
  // sigEq**2 = F*0.5*(s11-s22)**2 
  //           +G*0.5*(s22-s00)**2
  //           +H*0.5*(s00-s11)**2 
  //           +L*3*sig12**2 + M*3*sig02**2 + N*3*s01**2) = 1.5*s:aniMSqr:s
  // p = (P*sig00 + Q*sig11 + R*sig22)/3
  
  STensor43 aniMSqr;
  STensorOperation::zero(aniMSqr);
  aniMSqr(0,0,0,0) = (G+H)/3.;
  aniMSqr(0,0,1,1) = -H/3;
  aniMSqr(0,0,2,2) = -G/3.;
  
  aniMSqr(1,1,0,0) = -H/3.;
  aniMSqr(1,1,1,1) = (H+F)/3.;
  aniMSqr(1,1,2,2) = -F/3.;
  
  aniMSqr(2,2,0,0) = -G/3.;
  aniMSqr(2,2,1,1) = -F/3.;
  aniMSqr(2,2,2,2) = (F+G)/3.;
  
  aniMSqr(1,0,1,0) = N*0.5;
  aniMSqr(0,1,0,1) = N*0.5;
  aniMSqr(0,1,1,0) = N*0.5;
  aniMSqr(1,0,0,1) = N*0.5;
  
  aniMSqr(2,0,2,0) = M*0.5;
  aniMSqr(0,2,2,0) = M*0.5;
  aniMSqr(0,2,0,2) = M*0.5;
  aniMSqr(2,0,0,2) = M*0.5;
  
  aniMSqr(2,1,2,1) = L*0.5;
  aniMSqr(1,2,1,2) = L*0.5;
  aniMSqr(1,2,2,1) = L*0.5;
  aniMSqr(2,1,1,2) = L*0.5; 
  
  STensorOperation::sqrtSTensor43(aniMSqr, _aniM);
  _aniM.print("anisotropic tensor");
  fullMatrix<double> tmp(6,6);
  STensorOperation::fromSTensor43ToFullMatrix_ConsistentForm(aniMSqr,tmp);
  tmp.print("aniMSqr");
  STensorOperation::fromSTensor43ToFullMatrix_ConsistentForm(_aniM,tmp);
  tmp.print("_aniM");
  
  fullMatrix<double> test(6,6);
  tmp.mult(tmp, test);
  test.print("test");
  STensor3 I(1.);
  STensor3 D(0);
  D(0,0) = P;
  D(1,1) = Q;
  D(2,2) = R;
  STensorOperation::prodAdd(I,D,1./3.,_aniM);
  _aniM.print("anisotropic tensor");
};


void mlawNonLocalPorousCoupledHill48LawWithMPS::I1J2J3_voidCharacteristicsEvolution_NonLocal_aniso(const STensor3& sig,
                                                const IPNonLocalPorosity *q0, IPNonLocalPorosity* q1,
                                                bool stiff,
                                                std::vector<double>* Y ,
                                                std::vector<STensor3>* DYDsig ,
                                                std::vector<std::vector<double> >* DYDNonlocalVars
                                                  ) const
{
  const STensor43& M = q1->getConstRefToAnisotropicTensor();
  static STensor3 sigEff;
  static std::vector<STensor3> DYDsigEff(4, STensor3());
  STensorOperation::multSTensor43STensor3(M, sig, sigEff);
  mlawNonLocalPorousCoupledLawWithMPS::I1J2J3_voidCharacteristicsEvolution_NonLocal(sigEff, q0, q1, stiff, Y, &DYDsigEff, DYDNonlocalVars);
  if (stiff)
  {
    for (int i=0; i< 4; i++)
    {
      STensorOperation::multSTensor3STensor43(DYDsigEff[i],M,(*DYDsig)[i]);
    }
  }
};

double mlawNonLocalPorousCoupledHill48LawWithMPS::I1J2J3_yieldFunction_aniso(const STensor3& kcor, const double R, const IPNonLocalPorosity* q0, const IPNonLocalPorosity* q1, const double* T,
                                        bool diff, STensor3* DFDkcor, double* DFDR, std::vector<double>* DFDY, 
                                        STensor3* DFDDeltaEp, STensor3* DFDFdefo, 
                                        bool withThermic, double *DFDT) const
{
  const STensor43& M = q1->getConstRefToAnisotropicTensor();
  const STensor63& DMDFdefo = q1->getConstRefToDAnisotropicTensorDF();
  const STensor63& DMDDeltaEp = q1->getConstRefToDAnisotropicTensorDEp();
  
  static STensor3 sigEff;
  static STensor3 DFDsigEff;
  STensorOperation::multSTensor43STensor3(M, kcor, sigEff);
  
  double fval = mlawNonLocalPorousCoupledLawWithMPS::I1J2J3_yieldFunction(sigEff, R, q0, q1, T, diff,&DFDsigEff,DFDR, DFDY,withThermic,DFDT);
  if (diff)
  {
    STensorOperation::multSTensor3STensor43(DFDsigEff, M,*DFDkcor);
    
    static STensor43 temp;
    STensorOperation::multSTensor3STensor63(kcor, DMDDeltaEp, temp);
    STensorOperation::multSTensor3STensor43(DFDsigEff,temp,*DFDDeltaEp);
    
    STensorOperation::multSTensor3STensor63(kcor, DMDFdefo, temp);
    STensorOperation::multSTensor3STensor43(DFDsigEff,temp,*DFDFdefo);
  }
  return fval;
};
void mlawNonLocalPorousCoupledHill48LawWithMPS::I1J2J3_getYieldNormal_aniso(STensor3& Np, const STensor3& kcor, const double R, const IPNonLocalPorosity* q0, const IPNonLocalPorosity* q1, const double* T,
                                  bool diff, STensor43* DNpDkcor, STensor3* DNpDR, std::vector<STensor3>* DNpDY,
                                  STensor43* DNpDDeltaEp, STensor43* DNpDFdefo,
                                  bool withThermic, STensor3* DNpDT  
                                  ) const
{
  const STensor43& M = q1->getConstRefToAnisotropicTensor();
  const STensor63& DMDFdefo = q1->getConstRefToDAnisotropicTensorDF();
  const STensor63& DMDDeltaEp = q1->getConstRefToDAnisotropicTensorDEp();
  
  static STensor3 sigEff, NpEff, DNpEffDR, DNpEffDT; 
  static std::vector<STensor3> DNpEffDY;
  static STensor43 DNpEffDsigEff;
  if (diff)
  {
    DNpEffDY.resize(DNpDY->size(), STensor3(0.));
  }
  STensorOperation::multSTensor43STensor3(M, kcor, sigEff);
  mlawNonLocalPorousCoupledLawWithMPS::I1J2J3_getYieldNormal(NpEff, sigEff, R, q0, q1, T, diff, &DNpEffDsigEff, &DNpEffDR, &DNpEffDY, withThermic, &DNpEffDT);
  STensorOperation::multSTensor3STensor43(NpEff, M, Np);
  if (diff)
  {
    static STensor43 DNpDsigEff;
    STensorOperation::multSTensor43FirstIndexes(M, DNpEffDsigEff, DNpDsigEff);
    STensorOperation::multSTensor43(DNpDsigEff, M, *DNpDkcor);
    
    STensorOperation::multSTensor3STensor43(DNpEffDR, M, *DNpDR);
    for (int i=0; i< DNpEffDY.size(); i++)
    {
      STensorOperation::multSTensor3STensor43(DNpEffDY[i],M, (*DNpDY)[i]);
    }
    
    static STensor43 tmp;
    STensorOperation::multSTensor3STensor63(kcor, DMDDeltaEp, tmp);    
    STensorOperation::multSTensor43(DNpDsigEff, tmp, *DNpDDeltaEp);
    STensorOperation::multSTensor3STensor63Add(NpEff, DMDDeltaEp, *DNpDDeltaEp);
    
    STensorOperation::multSTensor3STensor63(kcor, DMDFdefo, tmp);
    STensorOperation::multSTensor43(DNpDsigEff,tmp,*DNpDFdefo);
    STensorOperation::multSTensor3STensor63Add(NpEff, DMDFdefo, *DNpDFdefo);
    
    if (withThermic)
    {
      STensorOperation::multSTensor3STensor43(DNpEffDT, M, *DNpDT);
    }
  }
};

void mlawNonLocalPorousCoupledHill48LawWithMPS::checkCoalescence(IPNonLocalPorosity* q1, const IPNonLocalPorosity *q0, const double* T) const
{
  // check condition if check coalescence is carried out
  if (getNumOfYieldSurfaces() > 1) {
    Msg::Error("check coalescenece has not implemented for num yield surface >1");
    return;
  }
  
  // Get ipvs
  const IPCoalescence* q0Thom = &q0->getConstRefToIPCoalescence();
  IPCoalescence* q1Thom = &q1->getRefToIPCoalescence();
  // check active
  if (q0Thom->getCoalescenceOnsetFlag() and q1->dissipationIsActive())
  {
    q1Thom->getRefToCoalescenceActiveFlag() = true;
  }
  else
  {
    q1Thom->getRefToCoalescenceActiveFlag() = false;
  }
  
  
  bool willUseNormalToCheck = false;
  if (_checkCoalescenceWithNormal)
  {
    if (q1->getLocation() == IPVariable::INTERFACE_MINUS or q1->getLocation() == IPVariable::INTERFACE_PLUS)
    {
      
      willUseNormalToCheck = true;
      const SVector3& normal = q1->getConstRefToCurrentOutwardNormal();
      if (normal.norm() <=0.)
      {
        Msg::Error("coalescence cannot check with zero normal!!!");
        return;
      };
    }
    else
    {
      willUseNormalToCheck = false;
      if (!_withBothYieldSurfacesInBulk)
      {
        return;
      }
    }  
  }
  
  if (q0Thom->getCoalescenceOnsetFlag())
  {
    // if onset already occurs
    q1Thom->getRefToCoalescenceOnsetFlag() = true;
    // copy data
    q1Thom->setIPvAtCoalescenceOnset(*q0Thom->getIPvAtCoalescenceOnset());

    q1Thom->getRefToCrackOffsetOnCft() = q0Thom->getCrackOffsetOnCft();
    q1Thom->getRefToLodeParameterOffset() = q0Thom->getLodeParameterOffset();
    q1Thom->getRefToYieldOffset() = q0Thom->getYieldOffset();
  }
  else if (q1->dissipationIsActive())
  {
    const STensor3& kCor = q1->getConstRefToCorotationalKirchhoffStress();
    const STensor3& F = q1->getConstRefToDeformationGradient();
    const double R = q1->getCurrentViscoplasticYieldStress();
    const double R0 = _j2IH->getYield0();
    const double yieldfV = q1->getYieldPorosity();

    double J = STensorOperation::determinantSTensor3(F);
    static STensor3 corSig;
    corSig = kCor;
    if (_stressFormulation == mlawNonLocalPorosity::CORO_CAUCHY)
    {
      // get corotational Cauchy stress
      corSig *= (1./J);
    }
    
    
    bool interfaceActive = true;
    if (willUseNormalToCheck)
    {
      double Cft;
      _mlawCoales->getCoalescenceLaw()->computeConcentrationFactor(q0,q1,Cft);
    
    
      SVector3 normal = q1->getConstRefToCurrentOutwardNormal();
      normal.normalize();
  
      // Compute Sig from Kcor following: Sig = invFeT * sigCor * FeT = Fe*sigCor*invFe
      static STensor3 invFep, FeSigCor , Fe, Sig;
      STensorOperation::inverseSTensor3(q1->getConstRefToFp(), invFep);
      STensorOperation::multSTensor3(F, invFep, Fe);
      STensorOperation::inverseSTensor3(Fe, invFep);
      STensorOperation::multSTensor3(Fe, corSig, FeSigCor);
      STensorOperation::multSTensor3SecondTranspose(FeSigCor, invFep, Sig);
      
      // traction vector
      static SVector3 tractionForce;
      STensorOperation::multSTensor3SVector3(Sig,normal,tractionForce);
      double tauN = STensorOperation::dot(tractionForce,normal);
      // Deduce tangent part
      double tauTanSq = 0.;
      for (int i=0; i<3; i++)
      {
        double temp = tractionForce(i)-tauN*normal(i);
        tauTanSq += temp*temp;
      }
      // Compute effective stress
      double tauEff = 0.; 
      if (tauN > 0.)
      {
        tauEff = sqrt(tauN*tauN + _beta*tauTanSq);
      }
      else
      {
        tauEff = sqrt(_beta*tauTanSq);
      }
      
      if (tauEff - Cft*R >0)
      {
        interfaceActive = true;
      } 
      else
      {
        interfaceActive = false;
      }
    }
    
    // coalescence never occurs before
    // check at interface first
    bool activeCoales = false;
    static STensor3 sigEff;
    const STensor43& M = q1->getConstRefToAnisotropicTensor();
    STensorOperation::multSTensor43STensor3(M, corSig, sigEff);
    if (_useTwoYieldRegularization)
    {
      // in case of two yield surface regulization
      double Gurson = _mlawGrowth->I1J2J3_yieldFunction(sigEff,R,q0,q1,T);
      double Thomason = _mlawCoales->I1J2J3_yieldFunction(sigEff,R,q0,q1,T);
      //
      double FG = Gurson +1.;
      double FT = Thomason +1.;
      if (FT > FG)
      {
        double ratio = FG/FT;
        if (ratio < 0.)
        {
          Msg::Error("FT FG must be all positive but FT=%e, FG=%e",FT,FG);
        }
        if (pow(ratio,_yieldRegExponent) < _coalesTol && interfaceActive)
        {
          activeCoales = true;
          printf("coalescence occurs with ratio = %e\n",ratio);
        }
      }
    }
    else
    {
      // thomason yield condition
      double yieldThomason = _mlawCoales->I1J2J3_yieldFunction(sigEff,R,q0,q1,T);
      if (yieldThomason > 0 && interfaceActive)
      {
        // coalescene occurs
        activeCoales = true;
        printf("coalescence occurs with Thomason yield surface = %e\n",yieldThomason);
      }
    }
    
    
    // if coalesnce occurs
    if (activeCoales)
    {
      q1Thom->getRefToCoalescenceOnsetFlag() = true;
      q1Thom->setIPvAtCoalescenceOnset(*q1);
      // if offset is used to satisfy Thomason yield surface at the onset of coalescence
      if (_useTwoYieldRegularization)
      {
        // never offset with yield regulation
        q1Thom->getRefToCrackOffsetOnCft() = 1.;
        q1Thom->getRefToLodeParameterOffset() = 1.;
        q1Thom->getRefToYieldOffset() = 0.;
      }
      else
      {
        if (_withCftOffset)
        {
          double yieldThomason = _mlawCoales->I1J2J3_yieldFunction(sigEff,R,q0,q1,T);
          q1Thom->getRefToCrackOffsetOnCft() = 1. + yieldThomason;
          q1Thom->getRefToLodeParameterOffset() = 1.;
          q1Thom->getRefToYieldOffset() = 0.;
          #ifdef _DEBUG
          printf("coalescence ocurrs using Cft offset = %e\n",q1Thom->getCrackOffsetOnCft());
          #endif //_DEBUG
        }
        else if (_withLodeOffset)
        {
          // Lode offset is not used for material law, because law already accounts for Lode variable
          q1Thom->getRefToCrackOffsetOnCft() = 1.;
          q1Thom->getRefToLodeParameterOffset()=  1.;
          q1Thom->getRefToYieldOffset() = 0.;
        }
        else if (_withYieldOffset)
        {
          q1Thom->getRefToCrackOffsetOnCft() = 1.;
          q1Thom->getRefToLodeParameterOffset() = 1.;
          q1Thom->getRefToYieldOffset() = _mlawCoales->I1J2J3_yieldFunction(sigEff,R,q0,q1,T);
          #ifdef _DEBUG
          printf("coalescence ocurrs using yield offset = %e\n",q1Thom->getYieldOffset());
          #endif //_DEBUG
        }
        else
        {
          q1Thom->getRefToCrackOffsetOnCft() = 1.;
          q1Thom->getRefToLodeParameterOffset() = 1.;
          q1Thom->getRefToYieldOffset() = 0.;
          #ifdef _DEBUG
          printf("coalescence ocurrs, not offset is used\n");
          #endif //_DEBUG
        }
      }
    }
    else
    {
      q1Thom->getRefToCoalescenceOnsetFlag() = false;
      q1Thom->getRefToCrackOffsetOnCft() = 1.;
      q1Thom->getRefToLodeParameterOffset() = 1.;
      q1Thom->getRefToYieldOffset() = 0.;
    }
  }
};

void mlawNonLocalPorousCoupledHill48LawWithMPS::forceCoalescence(IPNonLocalPorosity* q1, const IPNonLocalPorosity *q0, const double* T) const
{
  Msg::Error("mlawNonLocalPorousCoupledHill48LawWithMPS::forceCoalescence is not implemented!!!");
};
bool mlawNonLocalPorousCoupledHill48LawWithMPS::checkCrackCriterion(IPNonLocalPorosity* q1, const IPNonLocalPorosity *q0, const STensor3& Fcur, const SVector3& normal, double delayFactor, const double* T) const
{
  Msg::Error("mlawNonLocalPorousCoupledHill48LawWithMPS::checkCrackCriterion is not implemented!!!");
};


mlawNonLocalPorousCoupledLawWithMPSAndMSS::mlawNonLocalPorousCoupledLawWithMPSAndMSS(const int num,const double E,const double nu, const double rho,
                const double q1, const double q2, const double q3,
                const double fVinitial, const double lambda0, const J2IsotropicHardening &j2IH, const CLengthLaw &cLLaw,
                const double tol, const bool matrixbyPerturbation, const double pert) :
                    mlawNonLocalPorousCoupledLawWithMPS(num, E, nu, rho, q1,q2,q3, fVinitial,lambda0, j2IH, cLLaw, tol, matrixbyPerturbation, pert),
                    _freezeCoalescenceState(false)
{
  _mlawShear = new mlawNonLocalPorousThomasonLawWithMSS(num, E, nu, rho, fVinitial, lambda0,  j2IH, cLLaw, tol, matrixbyPerturbation, pert);
  _multipleLaws.resize(3);
  _multipleLaws[0] = _mlawGrowth;
  _multipleLaws[1] = _mlawCoales;
  _multipleLaws[2] = _mlawShear;
};

mlawNonLocalPorousCoupledLawWithMPSAndMSS::mlawNonLocalPorousCoupledLawWithMPSAndMSS(const int num,const double E,const double nu, const double rho,
                const double q1, const double q2, const double q3,
                const double fVinitial, const double lambda0, const double kappa, const J2IsotropicHardening &j2IH, const CLengthLaw &cLLaw,
                const double tol, const bool matrixbyPerturbation, const double pert) :
                    mlawNonLocalPorousCoupledLawWithMPS(num, E, nu, rho, q1,q2,q3, fVinitial,lambda0,kappa, j2IH, cLLaw, tol, matrixbyPerturbation, pert),
                    _freezeCoalescenceState(false)
{
   _mlawShear = new mlawNonLocalPorousThomasonLawWithMSS(num, E, nu, rho, fVinitial, lambda0, kappa, j2IH, cLLaw, tol, matrixbyPerturbation, pert);
  _multipleLaws.resize(3);
  _multipleLaws[0] = _mlawGrowth;
  _multipleLaws[1] = _mlawCoales;
  _multipleLaws[2] = _mlawShear;
};

mlawNonLocalPorousCoupledLawWithMPSAndMSS::mlawNonLocalPorousCoupledLawWithMPSAndMSS(const mlawNonLocalPorousCoupledLawWithMPSAndMSS& src):
mlawNonLocalPorousCoupledLawWithMPS(src), _freezeCoalescenceState(src._freezeCoalescenceState)
{
  _mlawShear =  NULL;
  if (src._mlawShear != NULL)
  {
    _mlawShear = dynamic_cast<mlawNonLocalPorousThomasonLawWithMSS*>(src._mlawShear->clone());
  }
  _multipleLaws.resize(3);
  _multipleLaws[0] = _mlawGrowth;
  _multipleLaws[1] = _mlawCoales;
  _multipleLaws[2] = _mlawShear;
};


mlawNonLocalPorousCoupledLawWithMPSAndMSS::~mlawNonLocalPorousCoupledLawWithMPSAndMSS(){
  if (_mlawShear != NULL)
  {
    delete _mlawShear;
    _mlawShear = NULL;
  }
};

void mlawNonLocalPorousCoupledLawWithMPSAndMSS::setThomasonSmoothFactor(const double fact)
{
  dynamic_cast<mlawNonLocalPorousThomasonLawWithMPS*>(_mlawCoales)->setSmoothFactor(fact);
}
void mlawNonLocalPorousCoupledLawWithMPSAndMSS::setShearSmoothFactor(const double fact)
{
  _mlawShear->setSmoothFactor(fact);
}

void mlawNonLocalPorousCoupledLawWithMPSAndMSS::setShearFactor(double g) 
{
  _mlawShear->setShearFactor(g);
};
void mlawNonLocalPorousCoupledLawWithMPSAndMSS::setVoidLigamentExponent(double e) 
{
  _mlawShear->setVoidLigamentExponent(e);
}

void mlawNonLocalPorousCoupledLawWithMPSAndMSS::freezeCoalescenceState(const bool fl)
{
  _freezeCoalescenceState = fl;
}


void mlawNonLocalPorousCoupledLawWithMPSAndMSS::setCoalescenceLaw(const CoalescenceLaw& added_coalsLaw)
{
  Msg::Error("setCoalescenceLaw cannot be used, use setNeckingCoalescenceLaw or setShearCoalescenceLaw instead!!!");
}

void mlawNonLocalPorousCoupledLawWithMPSAndMSS::setNeckingCoalescenceLaw(const CoalescenceLaw& added_coalsLaw)
{
  Msg::Info("set necking coalescence law:");
  added_coalsLaw.printInfo();
  _mlawCoales->setCoalescenceLaw(added_coalsLaw);
}
void mlawNonLocalPorousCoupledLawWithMPSAndMSS::setShearCoalescenceLaw(const CoalescenceLaw& added_coalsLaw)
{
  Msg::Info("set shear coalescence law:");
  added_coalsLaw.printInfo();
  _mlawShear->setCoalescenceLaw(added_coalsLaw);
}

void mlawNonLocalPorousCoupledLawWithMPSAndMSS::setShearVoidEvolutionLaw(const voidStateEvolutionLaw& voidtateLaw)
{
  Msg::Info("set shear void evolution law:");
  voidtateLaw.printInfo();
  _mlawShear->setVoidEvolutionLaw(voidtateLaw);
}

void mlawNonLocalPorousCoupledLawWithMPSAndMSS::voidStateGrowth(const double yieldfV0, const double yieldfV1, // yield porosity
                        const double DeltahatD, const double DeltahatQ, const double DeltahatP, // internal variables
                        const IPNonLocalPorosity *q0, IPNonLocalPorosity *q1) const
{
  bool CoalesFlag = q1->getConstRefToIPCoalescence().getCoalescenceOnsetFlag();
  if (CoalesFlag)
  {
    // coalescence mode
    int coalesMode = q1->getConstRefToIPCoalescence().getCoalescenceMode();
    if (coalesMode == 1)
    {
      // necking
      _mlawCoales->voidStateGrowth(yieldfV0,yieldfV1,DeltahatD,DeltahatQ,DeltahatP,q0,q1);
    }
    else if (coalesMode == 2)
    {
      _mlawShear->voidStateGrowth(yieldfV0,yieldfV1,DeltahatD,DeltahatQ,DeltahatP,q0,q1);
    }
    else
    {
      Msg::Error("coalescence mode %d does not defined",coalesMode);
    }
  }
  else
  {
    // void growth
    _mlawGrowth->voidStateGrowth(yieldfV0,yieldfV1,DeltahatD,DeltahatQ,DeltahatP,q0,q1);
  }
}

void mlawNonLocalPorousCoupledLawWithMPSAndMSS::I1J2J3_voidCharacteristicsEvolution_NonLocal(const STensor3& sig,
                                                const IPNonLocalPorosity *q0, IPNonLocalPorosity* q1,
                                                bool stiff,
                                                std::vector<double>* Y,
                                                std::vector<STensor3>* DYDsig,
                                                std::vector<std::vector<double> >* DYDNonlocalVars) const
{
  bool CoalesFlag = q1->getConstRefToIPCoalescence().getCoalescenceOnsetFlag();
  if (CoalesFlag)
  {
    // coalescence mode
    int coalesMode = q1->getConstRefToIPCoalescence().getCoalescenceMode();
    if (coalesMode == 1)
    {
      _mlawCoales->I1J2J3_voidCharacteristicsEvolution_NonLocal(sig,q0,q1,stiff,Y,DYDsig,DYDNonlocalVars);
    }
    else if (coalesMode == 2)
    {
      _mlawShear->I1J2J3_voidCharacteristicsEvolution_NonLocal(sig,q0,q1,stiff,Y,DYDsig,DYDNonlocalVars);
    }
    else
    {
      Msg::Error("coalescence mode %d does not defined",coalesMode);
    }
  }
  else
  {
    _mlawGrowth->I1J2J3_voidCharacteristicsEvolution_NonLocal(sig,q0,q1,stiff,Y,DYDsig,DYDNonlocalVars);
  }
};

void mlawNonLocalPorousCoupledLawWithMPSAndMSS::I1J2J3_voidCharacteristicsEvolution_Local(const STensor3& sig, const STensor3& DeltaEp, const double DeltaHatP,
                                                const IPNonLocalPorosity *q0, IPNonLocalPorosity* q1,
                                                bool stiff,
                                                std::vector<double>* Y,
                                                std::vector<STensor3>* DYDsig,
                                                std::vector<STensor3>* DYDDeltaEp,
                                                std::vector<double>* DYDDeltaHatP) const{
  bool CoalesFlag = q1->getConstRefToIPCoalescence().getCoalescenceOnsetFlag();      
  if (CoalesFlag)
  {
      // coalescence mode
    int coalesMode = q1->getConstRefToIPCoalescence().getCoalescenceMode();
    if (coalesMode == 1)
    {  
      _mlawCoales->I1J2J3_voidCharacteristicsEvolution_Local(sig,DeltaEp,DeltaHatP,q0,q1,stiff,Y,DYDsig,DYDDeltaEp,DYDDeltaHatP);
    }
    else if (coalesMode == 2)
    {
      _mlawShear->I1J2J3_voidCharacteristicsEvolution_Local(sig,DeltaEp,DeltaHatP,q0,q1,stiff,Y,DYDsig,DYDDeltaEp,DYDDeltaHatP);
    }
    else
    {
      Msg::Error("coalescence mode %d does not defined",coalesMode);
    }
  }
  else
  {
    _mlawGrowth->I1J2J3_voidCharacteristicsEvolution_Local(sig,DeltaEp,DeltaHatP,q0,q1,stiff,Y,DYDsig,DYDDeltaEp,DYDDeltaHatP);
  }
};


double mlawNonLocalPorousCoupledLawWithMPSAndMSS::I1J2J3_yieldFunction(const STensor3& kcor, const double R, const IPNonLocalPorosity* q0, const IPNonLocalPorosity* q1, const double* T,
                                                          bool diff, STensor3* DFDkcor, double* DFDR, std::vector<double>* DFDY,
                                                          bool withThermic, double *DFDT) const
{
  bool neckCoalesFlag = q1->getConstRefToIPCoalescence().getCoalescenceOnsetFlag() and (q1->getConstRefToIPCoalescence().getCoalescenceMode() == 1);
  bool shearCoalesFlag = q1->getConstRefToIPCoalescence().getCoalescenceOnsetFlag() and (q1->getConstRefToIPCoalescence().getCoalescenceMode() == 2);
  const IPNonLocalPorosity* ipvCoales = q1->getConstRefToIPCoalescence().getIPvAtCoalescenceOnset();
  if (_useTwoYieldRegularization)
  {
    int Ysize = getNumOfVoidCharacteristics();
    static STensor3 DGursonDkcor;
    double DGursonDR;
    static std::vector<double> DGursonDY(Ysize,0.);
    double DGursonDT;
    double Gurson = _mlawGrowth->I1J2J3_yieldFunction(kcor,R,q0,q1,T,diff,&DGursonDkcor,&DGursonDR,&DGursonDY,withThermic,&DGursonDT);

    static STensor3 DThomasonDkcor;
    double DThomasonDR;
    static std::vector<double> DThomasonDY(Ysize,0.);
    double DThomasonDT;
    double Thomason;
    
    if (shearCoalesFlag && _freezeCoalescenceState)
    {
      Thomason = _mlawCoales->I1J2J3_yieldFunction(kcor,R,ipvCoales,ipvCoales,T,diff,&DThomasonDkcor,&DThomasonDR,&DThomasonDY,withThermic,&DThomasonDT);
      for (int i=0; i< DThomasonDY.size(); i++)
      {
        DThomasonDY[i] = 0.;
      }
    }
    else
    {
      Thomason = _mlawCoales->I1J2J3_yieldFunction(kcor,R,q0,q1,T,diff,&DThomasonDkcor,&DThomasonDR,&DThomasonDY,withThermic,&DThomasonDT);
    }

    static STensor3 DShearDkcor;
    double DShearDR;
    static std::vector<double> DShearDY(Ysize,0.);
    double DShearDT;
    double Shear;
    
    if (neckCoalesFlag && _freezeCoalescenceState)
    {
      Shear = _mlawShear->I1J2J3_yieldFunction(kcor,R,ipvCoales,ipvCoales,T,diff,&DShearDkcor,&DShearDR,&DShearDY,withThermic,&DShearDT);
      for (int i=0; i< DShearDY.size(); i++)
      {
        DShearDY[i] = 0.;
      }
    }
    else
    {
      Shear = _mlawShear->I1J2J3_yieldFunction(kcor,R,q0,q1,T,diff,&DShearDkcor,&DShearDR,&DShearDY,withThermic,&DShearDT);
    }

    double FG = Gurson +1.;
    double FT = Thomason +1.;
    double FS = Shear+1.;
  

    double Phi,DPhiDFG,DPhiDFT,DPhiDFS;
    regulizationYieldFunctionBase(_yieldRegExponent, FG,FT,FS,Phi,diff,&DPhiDFG,&DPhiDFT,&DPhiDFS);
    //Msg::Info("Gurson = %e Thomason = %e Shear =%e Phi = %e FG = %e, FT = %e FS = %e",Gurson,Thomason,Shear,Phi-1.,FG,FT,FS);
    if (diff)
    {
      double DFGDGurson = 1.;
      double DFTDThomason = 1.;
      double DFSDShear = 1.;
     
      //
      STensorOperation::zero(*DFDkcor);
      DFDkcor->daxpy(DGursonDkcor,DPhiDFG*DFGDGurson);
      DFDkcor->daxpy(DThomasonDkcor,DPhiDFT*DFTDThomason);
      DFDkcor->daxpy(DShearDkcor,DPhiDFS*DFSDShear);

      //    
      *DFDR = DPhiDFG*DFGDGurson*DGursonDR + DPhiDFT*DFTDThomason*DThomasonDR + DPhiDFS*DFSDShear*DShearDR;

      //
      for (int i=0; i< Ysize; i++){
        (*DFDY)[i] = DPhiDFG*DFGDGurson*DGursonDY[i]+DPhiDFT*DFTDThomason*DThomasonDY[i] +DPhiDFS*DFSDShear*DShearDY[i];
      }

      //
      if (withThermic)
      {
        (*DFDT) = DPhiDFG*DFGDGurson*DGursonDT + DPhiDFT*DFTDThomason*DThomasonDT + DPhiDFS*DFSDShear*DShearDT;
      }
    }
    return Phi-1.;
  }
  else
  {
    if (neckCoalesFlag)
    {
      return _mlawCoales->I1J2J3_yieldFunction(kcor,R,q0,q1,T,diff,DFDkcor,DFDR,DFDY,withThermic,DFDT);
    }
    else if (shearCoalesFlag)
    {
      return _mlawShear->I1J2J3_yieldFunction(kcor,R,q0,q1,T,diff,DFDkcor,DFDR,DFDY,withThermic,DFDT);
    }
    else
    {
      return _mlawGrowth->I1J2J3_yieldFunction(kcor,R,q0,q1,T,diff,DFDkcor,DFDR,DFDY,withThermic,DFDT);
    }
  }
};



void mlawNonLocalPorousCoupledLawWithMPSAndMSS::I1J2J3_getYieldNormal(STensor3& Np, const STensor3& kcor, const double R, const IPNonLocalPorosity* q0, const IPNonLocalPorosity* q1, const double* T,
          bool diff, STensor43* DNpDkcor, STensor3* DNpDR, std::vector<STensor3>* DNpDY,
          bool withThermic, STensor3* DNpDT) const{
  bool neckCoalesFlag = q1->getConstRefToIPCoalescence().getCoalescenceOnsetFlag() and (q1->getConstRefToIPCoalescence().getCoalescenceMode() == 1);
  bool shearCoalesFlag = q1->getConstRefToIPCoalescence().getCoalescenceOnsetFlag() and (q1->getConstRefToIPCoalescence().getCoalescenceMode() == 2);
  const IPNonLocalPorosity* ipvCoales = q1->getConstRefToIPCoalescence().getIPvAtCoalescenceOnset();
  if (_useTwoYieldRegularization)
  {
    int Ysize = getNumOfVoidCharacteristics();
    static STensor3 DGursonDkcor;
    double DGursonDR;
    static std::vector<double> DGursonDY(Ysize,0.);
    double DGursonDT;
    double Gurson = _mlawGrowth->I1J2J3_yieldFunction(kcor,R,q0,q1,T,diff,&DGursonDkcor,&DGursonDR,&DGursonDY,withThermic,&DGursonDT);

    static STensor43 DNpGDkcor;
    static std::vector<STensor3> DNpGDY(Ysize,STensor3(0.));
    static STensor3 DNpGDR;
    static STensor3 DNpGDT;
    static STensor3 NpG;
    _mlawGrowth->I1J2J3_getYieldNormal(NpG,kcor,R,q0,q1,T,diff,&DNpGDkcor,&DNpGDR,&DNpGDY,withThermic,&DNpGDT);

    static STensor3 DThomasonDkcor;
    double DThomasonDR;
    static std::vector<double> DThomasonDY(Ysize,0.);
    double DThomasonDT;
    double Thomason;

    static STensor43 DNpTDkcor;
    static std::vector<STensor3> DNpTDY(Ysize,STensor3(0.));
    static STensor3 DNpTDR;
    static STensor3 DNpTDT;
    static STensor3 NpT;
    
    
    if (shearCoalesFlag && _freezeCoalescenceState)
    {
      Thomason= _mlawCoales->I1J2J3_yieldFunction(kcor,R,ipvCoales,ipvCoales,T,diff,&DThomasonDkcor,&DThomasonDR,&DThomasonDY,withThermic,&DThomasonDT);
      _mlawCoales->I1J2J3_getYieldNormal(NpT,kcor,R,ipvCoales,ipvCoales,T,diff,&DNpTDkcor,&DNpTDR,&DNpTDY,withThermic,&DNpTDT);
      for (int i=0; i< DThomasonDY.size(); i++)
      {
        DThomasonDY[i] = 0.;
        STensorOperation::zero(DNpTDY[i]);
      }
    }
    else
    {
      Thomason= _mlawCoales->I1J2J3_yieldFunction(kcor,R,q0,q1,T,diff,&DThomasonDkcor,&DThomasonDR,&DThomasonDY,withThermic,&DThomasonDT);
      _mlawCoales->I1J2J3_getYieldNormal(NpT,kcor,R,q0,q1,T,diff,&DNpTDkcor,&DNpTDR,&DNpTDY,withThermic,&DNpTDT);
    }
    static STensor3 DShearDkcor;
    double DShearDR;
    static std::vector<double> DShearDY(Ysize,0.);
    double DShearDT;
    double Shear; 

    static STensor43 DNpSDkcor;
    static std::vector<STensor3> DNpSDY(Ysize,STensor3(0.));
    static STensor3 DNpSDR;
    static STensor3 DNpSDT;
    static STensor3 NpS;
    
    if (neckCoalesFlag && _freezeCoalescenceState)
    {
       Shear= _mlawShear->I1J2J3_yieldFunction(kcor,R,ipvCoales,ipvCoales,T,diff,&DShearDkcor,&DShearDR,&DShearDY,withThermic,&DShearDT);
      _mlawShear->I1J2J3_getYieldNormal(NpS,kcor,R,ipvCoales,ipvCoales,T,diff,&DNpSDkcor,&DNpSDR,&DNpSDY,withThermic,&DNpSDT);
      
      for (int i=0; i< DShearDY.size(); i++)
      {
        DShearDY[i] = 0.;
        STensorOperation::zero(DNpSDY[i]);
      }
    }
    else
    {
      Shear= _mlawShear->I1J2J3_yieldFunction(kcor,R,q0,q1,T,diff,&DShearDkcor,&DShearDR,&DShearDY,withThermic,&DShearDT);
      _mlawShear->I1J2J3_getYieldNormal(NpS,kcor,R,q0,q1,T,diff,&DNpSDkcor,&DNpSDR,&DNpSDY,withThermic,&DNpSDT);

    }

    double FG = Gurson +1.;
    double FT = Thomason+1.;
    double FS = Shear +1.;


    double Phi,DPhiDFG,DPhiDFT,DPhiDFS, DDPhiDFG_DFG, DDPhiDFG_DFT, DDPhiDFG_DFS, DDPhiDFT_DFG, DDPhiDFT_DFT, DDPhiDFT_DFS, DDPhiDFS_DFG, DDPhiDFS_DFT, DDPhiDFS_DFS;
    regulizationYieldFunctionBase(_yieldRegExponent, FG,FT,FS,Phi,true,&DPhiDFG,&DPhiDFT,&DPhiDFS, diff,
                      &DDPhiDFG_DFG, &DDPhiDFG_DFT,&DDPhiDFG_DFS,
                      &DDPhiDFT_DFG, &DDPhiDFT_DFT,&DDPhiDFT_DFS,
                      &DDPhiDFS_DFG, &DDPhiDFS_DFT,&DDPhiDFS_DFS);

    // Np = R0*DPhiDkcor
    double DFGDGurson = 1.;
    double DFTDThomason = 1.;
    double DFSDShear = 1.;
      

    double DPhiDGurson = DPhiDFG*DFGDGurson;
    double DPhiDThomason = DPhiDFT*DFTDThomason;
    double DPhiDShear = DPhiDFS*DFSDShear;

    STensorOperation::zero(Np);
    Np.daxpy(NpG,DPhiDGurson);
    Np.daxpy(NpT,DPhiDThomason);
    Np.daxpy(NpS,DPhiDShear);

    if (diff)
    {
      // first term
      // Np = R0*DPhiDkcor = DPhiDGurson*NpG + DPhiDThomason*NpT+DPhiDShear*NpS;
      //
      STensorOperation::zero(*DNpDkcor);
      DNpDkcor->axpy(DPhiDGurson,DNpGDkcor);
      DNpDkcor->axpy(DPhiDThomason,DNpTDkcor);
      DNpDkcor->axpy(DPhiDShear,DNpSDkcor);

      //
      STensorOperation::zero(*DNpDR);
      DNpDR->daxpy(DNpGDR,DPhiDGurson);
      DNpDR->daxpy(DNpTDR,DPhiDThomason);
      DNpDR->daxpy(DNpSDR,DPhiDShear);

      //
      for (int i=0; i< Ysize; i++)
      {
        STensorOperation::zero((*DNpDY)[i]);
        (*DNpDY)[i].daxpy(DNpGDY[i],DPhiDGurson);
        (*DNpDY)[i].daxpy(DNpTDY[i],DPhiDThomason);
        (*DNpDY)[i].daxpy(DNpSDY[i],DPhiDShear);
      }
      if (withThermic)
      {
        STensorOperation::zero(*DNpDT);
        DNpDT->daxpy(DNpGDT,DPhiDGurson);
        DNpDT->daxpy(DNpTDT,DPhiDThomason);
        DNpDT->daxpy(DNpSDT,DPhiDShear);
      }

      // other part
      double DDPhiDGursonDGurson = DDPhiDFG_DFG*DFGDGurson*DFGDGurson;
      double DDPhiDGursonDThomason = DDPhiDFG_DFT*DFTDThomason*DFGDGurson;
      double DDPhiDGursonDShear = DDPhiDFG_DFS*DFSDShear*DFGDGurson;

      double DDPhiDThomasonDGurson = DDPhiDFT_DFG*DFGDGurson*DFTDThomason;
      double DDPhiDThomasonDThomason = DDPhiDFT_DFT*DFTDThomason*DFTDThomason;
      double DDPhiDThomasonDShear = DDPhiDFT_DFS*DFSDShear*DFTDThomason;

      double DDPhiDShearDGurson = DDPhiDFS_DFG*DFGDGurson*DFSDShear;
      double DDPhiDShearDThomason = DDPhiDFS_DFT*DFTDThomason*DFSDShear;
      double DDPhiDShearDShear = DDPhiDFS_DFS*DFSDShear*DFSDShear;

      //
      STensorOperation::prodAdd(NpG,DGursonDkcor,DDPhiDGursonDGurson,*DNpDkcor);
      STensorOperation::prodAdd(NpG,DThomasonDkcor,DDPhiDGursonDThomason,*DNpDkcor);
      STensorOperation::prodAdd(NpG,DShearDkcor,DDPhiDGursonDShear,*DNpDkcor);

      STensorOperation::prodAdd(NpT,DGursonDkcor,DDPhiDThomasonDGurson,*DNpDkcor);
      STensorOperation::prodAdd(NpT,DThomasonDkcor,DDPhiDThomasonDThomason,*DNpDkcor);
      STensorOperation::prodAdd(NpT,DShearDkcor,DDPhiDThomasonDShear,*DNpDkcor);

      STensorOperation::prodAdd(NpS,DGursonDkcor,DDPhiDShearDGurson,*DNpDkcor);
      STensorOperation::prodAdd(NpS,DThomasonDkcor,DDPhiDShearDThomason,*DNpDkcor);
      STensorOperation::prodAdd(NpS,DShearDkcor,DDPhiDShearDShear,*DNpDkcor);

      //
      DNpDR->daxpy(NpG,DDPhiDGursonDGurson*DGursonDR);
      DNpDR->daxpy(NpG,DDPhiDGursonDThomason*DThomasonDR);
      DNpDR->daxpy(NpG,DDPhiDGursonDShear*DShearDR);

      DNpDR->daxpy(NpT,DDPhiDThomasonDGurson*DGursonDR);
      DNpDR->daxpy(NpT,DDPhiDThomasonDThomason*DThomasonDR);
      DNpDR->daxpy(NpT,DDPhiDThomasonDShear*DShearDR);

      DNpDR->daxpy(NpS,DDPhiDShearDGurson*DGursonDR);
      DNpDR->daxpy(NpS,DDPhiDShearDThomason*DThomasonDR);
      DNpDR->daxpy(NpS,DDPhiDShearDShear*DShearDR);

      for (int i=0; i< getNumOfVoidCharacteristics(); i++)
      {
        (*DNpDY)[i].daxpy(NpG,DDPhiDGursonDGurson*DGursonDY[i]);
        (*DNpDY)[i].daxpy(NpG,DDPhiDGursonDThomason*DThomasonDY[i]);
        (*DNpDY)[i].daxpy(NpG,DDPhiDGursonDShear*DShearDY[i]);

        (*DNpDY)[i].daxpy(NpT,DDPhiDThomasonDGurson*DGursonDY[i]);
        (*DNpDY)[i].daxpy(NpT,DDPhiDThomasonDThomason*DThomasonDY[i]);
        (*DNpDY)[i].daxpy(NpT,DDPhiDThomasonDShear*DShearDY[i]);

        (*DNpDY)[i].daxpy(NpS,DDPhiDShearDGurson*DGursonDY[i]);
        (*DNpDY)[i].daxpy(NpS,DDPhiDShearDThomason*DThomasonDY[i]);
        (*DNpDY)[i].daxpy(NpS,DDPhiDShearDShear*DShearDY[i]);
      }

      if (withThermic)
      {
        DNpDT->daxpy(NpG,DDPhiDGursonDGurson*DGursonDT);
        DNpDT->daxpy(NpG,DDPhiDGursonDThomason*DThomasonDT);
        DNpDT->daxpy(NpG,DDPhiDGursonDShear*DShearDT);

        DNpDT->daxpy(NpT,DDPhiDThomasonDGurson*DGursonDT);
        DNpDT->daxpy(NpT,DDPhiDThomasonDThomason*DThomasonDT);
        DNpDT->daxpy(NpT,DDPhiDThomasonDShear*DShearDT);

        DNpDT->daxpy(NpS,DDPhiDShearDGurson*DGursonDT);
        DNpDT->daxpy(NpS,DDPhiDShearDThomason*DThomasonDT);
        DNpDT->daxpy(NpS,DDPhiDShearDShear*DShearDT);
      };
    }
  }
  else
  {
    if (neckCoalesFlag)
    {
      _mlawCoales->I1J2J3_getYieldNormal(Np,kcor,R,q0,q1,T,diff,DNpDkcor,DNpDR,DNpDY,withThermic,DNpDT);
    }
    else if (shearCoalesFlag)
    {
      _mlawShear->I1J2J3_getYieldNormal(Np,kcor,R,q0,q1,T,diff,DNpDkcor,DNpDR,DNpDY,withThermic,DNpDT);
    }
    else
    {
      _mlawGrowth->I1J2J3_getYieldNormal(Np,kcor,R,q0,q1,T,diff,DNpDkcor,DNpDR,DNpDY,withThermic,DNpDT);
    }
  }
};

void mlawNonLocalPorousCoupledLawWithMPSAndMSS::checkCoalescence(IPNonLocalPorosity* q1, const IPNonLocalPorosity *q0, const double* T) const
{
  // check condition if check coalescence is carried out
  if (getNumOfYieldSurfaces() > 1)
  {
    Msg::Error("check coalescenece has not implemented for num yield surface >1");
    return;
  }
    
  // Get ipvs
  const IPCoalescence* q0Thom = &q0->getConstRefToIPCoalescence();
  IPCoalescence* q1Thom = &q1->getRefToIPCoalescence();

  // check active
  if (q0Thom->getCoalescenceOnsetFlag() and q1->dissipationIsActive())
  {
    q1Thom->getRefToCoalescenceActiveFlag() = true;
  }
  else
  {
    q1Thom->getRefToCoalescenceActiveFlag() = false;
  }
  
  bool willUseNormalToCheck = false;
  if (_checkCoalescenceWithNormal)
  {
    if (q1->getLocation() == IPVariable::INTERFACE_MINUS or q1->getLocation() == IPVariable::INTERFACE_PLUS)
    {
      
      willUseNormalToCheck = true;
      const SVector3& normal = q1->getConstRefToCurrentOutwardNormal();
      if (normal.norm() <=0.)
      {
        Msg::Error("coalescence cannot check with zero normal!!!");
        return;
      };
    }
    else
    {
      willUseNormalToCheck = false;
      if (!_withBothYieldSurfacesInBulk)
      {
        return;
      }
    }  
  }
  
  
  if (q0Thom->getCoalescenceOnsetFlag())
  {
    // if onset already occurs
    q1Thom->getRefToCoalescenceOnsetFlag() = q0Thom->getCoalescenceOnsetFlag();
    q1Thom->getRefToCoalescenceMode() = q0Thom->getCoalescenceMode();
    // copy data
    q1Thom->setIPvAtCoalescenceOnset(*q0Thom->getIPvAtCoalescenceOnset());

    q1Thom->getRefToCrackOffsetOnCft() = q0Thom->getCrackOffsetOnCft();
    q1Thom->getRefToLodeParameterOffset() = q0Thom->getLodeParameterOffset();
    q1Thom->getRefToYieldOffset() = q0Thom->getYieldOffset();
  }
  else if (q1->dissipationIsActive())
  {
    const STensor3& kCor = q1->getConstRefToCorotationalKirchhoffStress();
    const STensor3& F = q1->getConstRefToDeformationGradient();
    const double R = q1->getCurrentViscoplasticYieldStress();    
    double J = STensorOperation::determinantSTensor3(F);
    static STensor3 corSig;
    corSig = kCor;
    if (_stressFormulation == CORO_CAUCHY)
    {
      corSig *= (1./J);
    }

    // check at interface 
    bool interfaceNeckActive = true;
    bool interfaceShearActive = true;
    if (willUseNormalToCheck)
    {
       double CftThomason;
      _mlawCoales->getCoalescenceLaw()->computeConcentrationFactor(q0,q1,CftThomason);
      double CftShear;
      _mlawShear->getCoalescenceLaw()->computeConcentrationFactor(q0,q1,CftShear);
      
      SVector3 normal = q1->getConstRefToCurrentOutwardNormal();
      normal.normalize();

      // Compute Sig from Kcor following: Sig = invFeT * sigCor * FeT = Fe*sigCor*invFe
      static STensor3 invFep, FeSigCor , Fe, Sig;
      STensorOperation::inverseSTensor3(q1->getConstRefToFp(), invFep);
      STensorOperation::multSTensor3(F, invFep, Fe);
      STensorOperation::inverseSTensor3(Fe, invFep);
      STensorOperation::multSTensor3(Fe, corSig, FeSigCor);
      STensorOperation::multSTensor3SecondTranspose(FeSigCor, invFep, Sig);
      
      // traction vector
      static SVector3 tractionForce;
      STensorOperation::multSTensor3SVector3(Sig,normal,tractionForce);
      double tauN = STensorOperation::dot(tractionForce,normal);
      // Deduce tangent part
      double tauTanSq = 0.;
      for (int i=0; i<3; i++)
      {
        double temp = tractionForce(i)-tauN*normal(i);
        tauTanSq += temp*temp;
      }
      // Compute effective stress
      double tauEff = 0.; 
      if (tauN > 0.)
      {
        tauEff = sqrt(tauN*tauN + _beta*tauTanSq);
      }
      else
      {
        tauEff = sqrt(_beta*tauTanSq);
      }
      
      if (tauEff - CftThomason*R >0)
      {
        interfaceNeckActive = true;
      } 
      else
      {
        interfaceNeckActive = false;
      }
      if (sqrt(3.*tauTanSq) - CftShear*R >0)
      {
        interfaceShearActive = true;
      }
      else
      {
        interfaceShearActive = false;
      }
    }
    
    double Gurson = _mlawGrowth->I1J2J3_yieldFunction(corSig,R,q0,q1,T);
    double Thomason = _mlawCoales->I1J2J3_yieldFunction(corSig,R,q0,q1,T);
    double Shear = _mlawShear->I1J2J3_yieldFunction(corSig,R,q0,q1,T);
    
    // active plasticity
    // check coalescence is active
    bool& CoalesFlag = q1Thom->getRefToCoalescenceOnsetFlag();
    int& CoalesMode = q1Thom->getRefToCoalescenceMode();
    CoalesFlag = false;
    CoalesMode = 0;
    
    if (_useTwoYieldRegularization)
    {
      double FG = Gurson +1.;
      double FT = Thomason +1.;
      double FS = Shear +1.;
      if (FG < 0. or FT < 0. or FS < 0)
      {
        Msg::Error("FT,FS FG must be all positive but FT=%e, FS = %e, FG=%e",FT,FS,FG);
      }
      // check necking
      if (std::max(FT,FS)> FG)
      {
        double ratio = FG/FT;
        if (pow(ratio,_yieldRegExponent) < _coalesTol  && interfaceNeckActive)
        {
          CoalesFlag = true;
          CoalesMode = 1;
          printf("Necking coalescence at Gurson = %e, Thomason =%e, Shear = %e\n",
                        Gurson,Thomason,Shear);
        }
        else
        {
          ratio = FG/FS;
          if (pow(ratio,_yieldRegExponent) < _coalesTol  && interfaceShearActive)
          {
            CoalesFlag = true; 
            CoalesMode = 2;
            printf("Shear coalescence at Gurson = %e, Thomason =%e, Shear = %e\n",
                                  Gurson,Thomason,Shear);
          }
          else
          {
            CoalesFlag = false;
            CoalesMode = 0;
          }
        }
      }
    }
    else
    {
      if (Thomason > std::max(Gurson,Shear) && interfaceNeckActive)
      {
        CoalesFlag = true;
        CoalesMode = 1;
        printf("Necking coalescence at Gurson = %e, Thomason =%e, Shear = %e\n",
                        Gurson,Thomason,Shear);
      }
      else if (Shear > std::max(Gurson,Thomason) && interfaceShearActive)
      {
        CoalesFlag = true;
        CoalesMode = 2;
        printf("Shear coalescence at Gurson = %e, Thomason =%e, Shear = %e\n",
                                Gurson,Thomason,Shear);
      }
      else
      {
        CoalesFlag = false;
        CoalesMode = 0;
      }
    }
       
   // if coalesnce occurs
    if (CoalesFlag)
    {
      q1Thom->setIPvAtCoalescenceOnset(*q1);
      // if offset is used to satisfy Thomason yield surface at the onset of coalescence
      if (_useTwoYieldRegularization)
      {
        // never offset with yield regulation
        q1Thom->getRefToCrackOffsetOnCft() = 1.;
        q1Thom->getRefToLodeParameterOffset() = 1.;
        q1Thom->getRefToYieldOffset() = 0.;
      }
      else
      {
        if (_withCftOffset)
        {
          if (CoalesMode==1)
          {
            q1Thom->getRefToCrackOffsetOnCft() = 1. + Thomason;
          }
          else if (CoalesMode==2)
          {
            q1Thom->getRefToCrackOffsetOnCft() = 1. + Shear;
          }
          q1Thom->getRefToLodeParameterOffset() = 1.;
          q1Thom->getRefToYieldOffset() = 0.;
          #ifdef _DEBUG
          printf("coalescence ocurrs using Cft offset = %e\n",q1Thom->getCrackOffsetOnCft());
          #endif //_DEBUG
        }
        else if (_withLodeOffset)
        {
          // Lode offset is not used for material law, because law already accounts for Lode variable
          q1Thom->getRefToCrackOffsetOnCft() = 1.;
          q1Thom->getRefToLodeParameterOffset()=  1.;
          q1Thom->getRefToYieldOffset() = 0.;
        }
        else if (_withYieldOffset)
        {
          q1Thom->getRefToCrackOffsetOnCft() = 1.;
          q1Thom->getRefToLodeParameterOffset() = 1.;
          if (CoalesMode==1)
          {
            q1Thom->getRefToYieldOffset() = Thomason;
          }
          else if (CoalesMode==2)
          {
            q1Thom->getRefToYieldOffset() = Shear;
          }
          #ifdef _DEBUG
          printf("coalescence ocurrs using yield offset = %e\n",q1Thom->getYieldOffset());
          #endif //_DEBUG
        }
        else
        {
          q1Thom->getRefToCrackOffsetOnCft() = 1.;
          q1Thom->getRefToLodeParameterOffset() = 1.;
          q1Thom->getRefToYieldOffset() = 0.;
          #ifdef _DEBUG
          printf("coalescence ocurrs, not offset is used\n");
          #endif //_DEBUG
        }
      }
    }
    else
    {
      q1Thom->getRefToCrackOffsetOnCft() = 1.;
      q1Thom->getRefToLodeParameterOffset() = 1.;
      q1Thom->getRefToYieldOffset() = 0.;
    }
  }
};

void mlawNonLocalPorousCoupledLawWithMPSAndMSS::forceCoalescence(IPNonLocalPorosity* q1, const IPNonLocalPorosity *q0, const double* T) const
{
  const IPCoalescence* q0Thom = &q0->getConstRefToIPCoalescence();
  IPCoalescence* q1Thom = &q1->getRefToIPCoalescence();
  
  // force coalesccence without condittion
  q1Thom->getRefToCoalescenceOnsetFlag() = true;
  q1Thom->setIPvAtCoalescenceOnset(*q1);
  //
  const STensor3& kCor = q1->getConstRefToCorotationalKirchhoffStress();
  const STensor3& F = q1->getConstRefToDeformationGradient();
  const double R = q1->getCurrentViscoplasticYieldStress();    
  double J = STensorOperation::determinantSTensor3(F);
  static STensor3 corSig;
  corSig = kCor;
  if (_stressFormulation == CORO_CAUCHY)
  {
    corSig *= (1./J);
  }
  
  double Gurson = _mlawGrowth->I1J2J3_yieldFunction(corSig,R,q0,q1,T);
  double Thomason = _mlawCoales->I1J2J3_yieldFunction(corSig,R,q0,q1,T);
  double Shear = _mlawShear->I1J2J3_yieldFunction(corSig,R,q0,q1,T);
  
  int& CoalesMode = q1Thom->getRefToCoalescenceMode(); // should be set before calling this fuction
  if (CoalesMode != 1 || CoalesMode !=2)
  {
    if (Thomason >= Shear)
    {
      Msg::Info("force necking coalescence");
      CoalesMode = 1;
    }
    else 
    {
      Msg::Info("force shear coalescence");
      CoalesMode = 2;
    }
  }
  
  if (_useTwoYieldRegularization)
  {
    // never offset with yield regulation
    q1Thom->getRefToCrackOffsetOnCft() = 1.;
    q1Thom->getRefToLodeParameterOffset() = 1.;
    q1Thom->getRefToYieldOffset() = 0.;
  }
  else
  {
    // if offset is used to satisfy Thomason yield surface at the onset of coalescence
    if (_useTwoYieldRegularization)
    {
      // never offset with yield regulation
      q1Thom->getRefToCrackOffsetOnCft() = 1.;
      q1Thom->getRefToLodeParameterOffset() = 1.;
      q1Thom->getRefToYieldOffset() = 0.;
    }
    else
    {
      if (_withCftOffset)
      {
        if (CoalesMode==1)
        {
          q1Thom->getRefToCrackOffsetOnCft() = 1. + Thomason;
        }
        else if (CoalesMode==2)
        {
          q1Thom->getRefToCrackOffsetOnCft() = 1. + Shear;
        }
        q1Thom->getRefToLodeParameterOffset() = 1.;
        q1Thom->getRefToYieldOffset() = 0.;
      }
      else if (_withLodeOffset)
      {
        // Lode offset is not used for material law, because law already accounts for Lode variable
        q1Thom->getRefToCrackOffsetOnCft() = 1.;
        q1Thom->getRefToLodeParameterOffset()=  1.;
        q1Thom->getRefToYieldOffset() = 0.;
      }
      else if (_withYieldOffset)
      {
        q1Thom->getRefToCrackOffsetOnCft() = 1.;
        q1Thom->getRefToLodeParameterOffset() = 1.;
        if (CoalesMode==1)
        {
          q1Thom->getRefToYieldOffset() = Thomason;
        }
        else if (CoalesMode==2)
        {
          q1Thom->getRefToYieldOffset() = Shear;
        }
      }
      else
      {
        q1Thom->getRefToCrackOffsetOnCft() = 1.;
        q1Thom->getRefToLodeParameterOffset() = 1.;
        q1Thom->getRefToYieldOffset() = 0.;
      }
    }
  }
}

bool mlawNonLocalPorousCoupledLawWithMPSAndMSS::checkCrackCriterion(IPNonLocalPorosity* q1, const IPNonLocalPorosity *q0,  const STensor3& Fcur, const SVector3& normal, double delayFactor, const double* T) const
{
 // crack is inserted when
 // coalescence occurs
 // and other condition
   if (q1->getConstRefToIPCoalescence().getCoalescenceOnsetFlag())
  {
    if (delayFactor >=1.)
    {
      return true;
    }
    const IPNonLocalPorosity* qOnset = q1->getConstRefToIPCoalescence().getIPvAtCoalescenceOnset();
    if (qOnset == NULL)
    {
      Msg::Error("IPNonLocalPorosity at the onset of coalescence must be introduced");
      Msg::Exit(0);
    }
    
    double Cftonset = 0.;
    int onsetMode = qOnset->getConstRefToIPCoalescence().getCoalescenceMode();
    if (onsetMode == 1)
    {
      // necking
      _mlawCoales->getCoalescenceLaw()->computeConcentrationFactor(qOnset,qOnset,Cftonset);
    }
    else if (onsetMode == 2)
    {
      // shearing
      _mlawShear->getCoalescenceLaw()->computeConcentrationFactor(qOnset,qOnset,Cftonset);
    }
    else
    {
      Msg::Error("coalescence mode %d has not been defined",onsetMode);
    }
    double Cftcurrent = 0.;
    int currentMode = q1->getConstRefToIPCoalescence().getCoalescenceMode();
    if (currentMode == 1)
    {
      // necking
      _mlawCoales->getCoalescenceLaw()->computeConcentrationFactor(q0,q1,Cftcurrent);
    }
    else if (currentMode == 2)
    {
      // shearing
      _mlawShear->getCoalescenceLaw()->computeConcentrationFactor(q0,q1,Cftcurrent);
    }
    else
    {
      Msg::Error("coalescence mode %d has not been defined",currentMode);
    }
   
    if (Cftcurrent <= Cftonset*delayFactor)
    {
      return true;
    }
    else
    {
      return false;
    }
  }
  else
  {
    return false;
  }
};


void mlawNonLocalPorousCoupledHill48LawWithMPSAndMSS::setYieldParameters(const std::vector<double>& Mvec)
{
  if (Mvec.size() != 9)
  {
    Msg::Error("mlawNonLocalDamageGursonHill48 requires 9 parameters to define yield surface");
    Msg::Exit(0);
  }
  STensorOperation::zero(_aniM);
  
  Msg::Info("-----\nHill parameters");
  for (int i=0; i< Mvec.size(); i++)
  {
    printf("%.16g ", Mvec[i]);
  }
  Msg::Info("\n------");
    
  // [F, G, H, L, M, N] = Mvec
  double F = Mvec[0];
  double G = Mvec[1];
  double H = Mvec[2];
  double L = Mvec[3];
  double M = Mvec[4];
  double N = Mvec[5];
  double P = Mvec[6];
  double Q = Mvec[7];
  double R = Mvec[8];
  // define the equivalent stress under the form
  // sigEq**2 = F*0.5*(s11-s22)**2 
  //           +G*0.5*(s22-s00)**2
  //           +H*0.5*(s00-s11)**2 
  //           +L*3*sig12**2 + M*3*sig02**2 + N*3*s01**2) = 1.5*s:aniMSqr:s
  // p = (P*sig00 + Q*sig11 + R*sig22)/3
  
  STensor43 aniMSqr;
  STensorOperation::zero(aniMSqr);
  aniMSqr(0,0,0,0) = (G+H)/3.;
  aniMSqr(0,0,1,1) = -H/3;
  aniMSqr(0,0,2,2) = -G/3.;
  
  aniMSqr(1,1,0,0) = -H/3.;
  aniMSqr(1,1,1,1) = (H+F)/3.;
  aniMSqr(1,1,2,2) = -F/3.;
  
  aniMSqr(2,2,0,0) = -G/3.;
  aniMSqr(2,2,1,1) = -F/3.;
  aniMSqr(2,2,2,2) = (F+G)/3.;
  
  aniMSqr(1,0,1,0) = N*0.5;
  aniMSqr(0,1,0,1) = N*0.5;
  aniMSqr(0,1,1,0) = N*0.5;
  aniMSqr(1,0,0,1) = N*0.5;
  
  aniMSqr(2,0,2,0) = M*0.5;
  aniMSqr(0,2,2,0) = M*0.5;
  aniMSqr(0,2,0,2) = M*0.5;
  aniMSqr(2,0,0,2) = M*0.5;
  
  aniMSqr(2,1,2,1) = L*0.5;
  aniMSqr(1,2,1,2) = L*0.5;
  aniMSqr(1,2,2,1) = L*0.5;
  aniMSqr(2,1,1,2) = L*0.5; 
  
  STensorOperation::sqrtSTensor43(aniMSqr, _aniM);
  _aniM.print("anisotropic tensor");
  fullMatrix<double> tmp(6,6);
  STensorOperation::fromSTensor43ToFullMatrix_ConsistentForm(aniMSqr,tmp);
  tmp.print("aniMSqr");
  STensorOperation::fromSTensor43ToFullMatrix_ConsistentForm(_aniM,tmp);
  tmp.print("_aniM");
  
  fullMatrix<double> test(6,6);
  tmp.mult(tmp, test);
  test.print("test");
  STensor3 I(1.);
  STensor3 D(0);
  D(0,0) = P;
  D(1,1) = Q;
  D(2,2) = R;
  STensorOperation::prodAdd(I,D,1./3.,_aniM);
  _aniM.print("anisotropic tensor");
};


void mlawNonLocalPorousCoupledHill48LawWithMPSAndMSS::I1J2J3_voidCharacteristicsEvolution_NonLocal_aniso(const STensor3& sig,
                                                const IPNonLocalPorosity *q0, IPNonLocalPorosity* q1,
                                                bool stiff,
                                                std::vector<double>* Y ,
                                                std::vector<STensor3>* DYDsig ,
                                                std::vector<std::vector<double> >* DYDNonlocalVars
                                                  ) const
{
  const STensor43& M = q1->getConstRefToAnisotropicTensor();
  static STensor3 sigEff;
  static std::vector<STensor3> DYDsigEff(4, STensor3());
  STensorOperation::multSTensor43STensor3(M, sig, sigEff);
  mlawNonLocalPorousCoupledLawWithMPSAndMSS::I1J2J3_voidCharacteristicsEvolution_NonLocal(sigEff, q0, q1, stiff, Y, &DYDsigEff, DYDNonlocalVars);
  if (stiff)
  {
    for (int i=0; i< 4; i++)
    {
      STensorOperation::multSTensor3STensor43(DYDsigEff[i],M,(*DYDsig)[i]);
    }
  }
};

void mlawNonLocalPorousCoupledHill48LawWithMPSAndMSS::computeEffectivePressureSVM(const STensor3& F, const IPNonLocalPorosity* q0, const IPNonLocalPorosity* q1,
                                             double& effPressure, double& effSVM) const
{
  double J = F.determinant();
  const STensor43& M = q1->getConstRefToAnisotropicTensor();
  const STensor63& DMDFdefo = q1->getConstRefToDAnisotropicTensorDF();
  const STensor63& DMDDeltaEp = q1->getConstRefToDAnisotropicTensorDEp();
  const STensor3& kcor = q1->getConstRefToCorotationalKirchhoffStress();
  static STensor3 sigEff;
  STensorOperation::multSTensor43STensor3(M, kcor, sigEff);
  static STensor3 s;
  s = sigEff.dev();
  effPressure = (sigEff(0,0)+sigEff(1,1)+sigEff(2,2))/(3.*J);
  effSVM = sqrt(1.5*s.dotprod())/J;
};

double mlawNonLocalPorousCoupledHill48LawWithMPSAndMSS::I1J2J3_yieldFunction_aniso(const STensor3& kcor, const double R, const IPNonLocalPorosity* q0, const IPNonLocalPorosity* q1, const double* T,
                                        bool diff, STensor3* DFDkcor, double* DFDR, std::vector<double>* DFDY, 
                                        STensor3* DFDDeltaEp, STensor3* DFDFdefo, 
                                        bool withThermic, double *DFDT) const
{
  const STensor43& M = q1->getConstRefToAnisotropicTensor();
  const STensor63& DMDFdefo = q1->getConstRefToDAnisotropicTensorDF();
  const STensor63& DMDDeltaEp = q1->getConstRefToDAnisotropicTensorDEp();
  
  static STensor3 sigEff;
  static STensor3 DFDsigEff;
  STensorOperation::multSTensor43STensor3(M, kcor, sigEff);
  
  double fval = mlawNonLocalPorousCoupledLawWithMPSAndMSS::I1J2J3_yieldFunction(sigEff, R, q0, q1, T, diff,&DFDsigEff,DFDR, DFDY,withThermic,DFDT);
  if (diff)
  {
    STensorOperation::multSTensor3STensor43(DFDsigEff, M,*DFDkcor);
    
    static STensor43 temp;
    STensorOperation::multSTensor3STensor63(kcor, DMDDeltaEp, temp);
    STensorOperation::multSTensor3STensor43(DFDsigEff,temp,*DFDDeltaEp);
    
    STensorOperation::multSTensor3STensor63(kcor, DMDFdefo, temp);
    STensorOperation::multSTensor3STensor43(DFDsigEff,temp,*DFDFdefo);
  }
  return fval;
};
void mlawNonLocalPorousCoupledHill48LawWithMPSAndMSS::I1J2J3_getYieldNormal_aniso(STensor3& Np, const STensor3& kcor, const double R, const IPNonLocalPorosity* q0, const IPNonLocalPorosity* q1, const double* T,
                                  bool diff, STensor43* DNpDkcor, STensor3* DNpDR, std::vector<STensor3>* DNpDY,
                                  STensor43* DNpDDeltaEp, STensor43* DNpDFdefo,
                                  bool withThermic, STensor3* DNpDT  
                                  ) const
{
  const STensor43& M = q1->getConstRefToAnisotropicTensor();
  const STensor63& DMDFdefo = q1->getConstRefToDAnisotropicTensorDF();
  const STensor63& DMDDeltaEp = q1->getConstRefToDAnisotropicTensorDEp();
  
  static STensor3 sigEff, NpEff, DNpEffDR, DNpEffDT; 
  static std::vector<STensor3> DNpEffDY;
  static STensor43 DNpEffDsigEff;
  if (diff)
  {
    DNpEffDY.resize(DNpDY->size(), STensor3(0.));
  }
  STensorOperation::multSTensor43STensor3(M, kcor, sigEff);
  mlawNonLocalPorousCoupledLawWithMPSAndMSS::I1J2J3_getYieldNormal(NpEff, sigEff, R, q0, q1, T, diff, &DNpEffDsigEff, &DNpEffDR, &DNpEffDY, withThermic, &DNpEffDT);
  STensorOperation::multSTensor3STensor43(NpEff, M, Np);
  if (diff)
  {
    static STensor43 DNpDsigEff;
    STensorOperation::multSTensor43FirstIndexes(M, DNpEffDsigEff, DNpDsigEff);
    STensorOperation::multSTensor43(DNpDsigEff, M, *DNpDkcor);
    
    STensorOperation::multSTensor3STensor43(DNpEffDR, M, *DNpDR);
    for (int i=0; i< DNpEffDY.size(); i++)
    {
      STensorOperation::multSTensor3STensor43(DNpEffDY[i],M, (*DNpDY)[i]);
    }
    
    static STensor43 tmp;
    STensorOperation::multSTensor3STensor63(kcor, DMDDeltaEp, tmp);    
    STensorOperation::multSTensor43(DNpDsigEff, tmp, *DNpDDeltaEp);
    STensorOperation::multSTensor3STensor63Add(NpEff, DMDDeltaEp, *DNpDDeltaEp);
    
    STensorOperation::multSTensor3STensor63(kcor, DMDFdefo, tmp);
    STensorOperation::multSTensor43(DNpDsigEff,tmp,*DNpDFdefo);
    STensorOperation::multSTensor3STensor63Add(NpEff, DMDFdefo, *DNpDFdefo);
    
    if (withThermic)
    {
      STensorOperation::multSTensor3STensor43(DNpEffDT, M, *DNpDT);
    }
  }
};


void mlawNonLocalPorousCoupledHill48LawWithMPSAndMSS::checkCoalescence(IPNonLocalPorosity* q1, const IPNonLocalPorosity *q0, const double* T) const
{
  // check condition if check coalescence is carried out
  if (getNumOfYieldSurfaces() > 1)
  {
    Msg::Error("check coalescenece has not implemented for num yield surface >1");
    return;
  }
    
  // Get ipvs
  const IPCoalescence* q0Thom = &q0->getConstRefToIPCoalescence();
  IPCoalescence* q1Thom = &q1->getRefToIPCoalescence();

  // check active
  if (q0Thom->getCoalescenceOnsetFlag() and q1->dissipationIsActive())
  {
    q1Thom->getRefToCoalescenceActiveFlag() = true;
  }
  else
  {
    q1Thom->getRefToCoalescenceActiveFlag() = false;
  }
  
  bool willUseNormalToCheck = false;
  if (_checkCoalescenceWithNormal)
  {
    if (q1->getLocation() == IPVariable::INTERFACE_MINUS or q1->getLocation() == IPVariable::INTERFACE_PLUS)
    {
      
      willUseNormalToCheck = true;
      const SVector3& normal = q1->getConstRefToCurrentOutwardNormal();
      if (normal.norm() <=0.)
      {
        Msg::Error("coalescence cannot check with zero normal!!!");
        return;
      };
    }
    else
    {
      willUseNormalToCheck = false;
      if (!_withBothYieldSurfacesInBulk)
      {
        return;
      }
    }  
  }
  
  
  if (q0Thom->getCoalescenceOnsetFlag())
  {
    // if onset already occurs
    q1Thom->getRefToCoalescenceOnsetFlag() = q0Thom->getCoalescenceOnsetFlag();
    q1Thom->getRefToCoalescenceMode() = q0Thom->getCoalescenceMode();
    // copy data
    q1Thom->setIPvAtCoalescenceOnset(*q0Thom->getIPvAtCoalescenceOnset());

    q1Thom->getRefToCrackOffsetOnCft() = q0Thom->getCrackOffsetOnCft();
    q1Thom->getRefToLodeParameterOffset() = q0Thom->getLodeParameterOffset();
    q1Thom->getRefToYieldOffset() = q0Thom->getYieldOffset();
  }
  else if (q1->dissipationIsActive())
  {
    const STensor3& kCor = q1->getConstRefToCorotationalKirchhoffStress();
    const STensor3& F = q1->getConstRefToDeformationGradient();
    const double R = q1->getCurrentViscoplasticYieldStress();    
    double J = STensorOperation::determinantSTensor3(F);
    static STensor3 corSig;
    corSig = kCor;
    if (_stressFormulation == CORO_CAUCHY)
    {
      corSig *= (1./J);
    }

    // check at interface 
    bool interfaceNeckActive = true;
    bool interfaceShearActive = true;
    if (willUseNormalToCheck)
    {
       double CftThomason;
      _mlawCoales->getCoalescenceLaw()->computeConcentrationFactor(q0,q1,CftThomason);
      double CftShear;
      _mlawShear->getCoalescenceLaw()->computeConcentrationFactor(q0,q1,CftShear);
      
      SVector3 normal = q1->getConstRefToCurrentOutwardNormal();
      normal.normalize();

      // Compute Sig from Kcor following: Sig = invFeT * sigCor * FeT = Fe*sigCor*invFe
      static STensor3 invFep, FeSigCor , Fe, Sig;
      STensorOperation::inverseSTensor3(q1->getConstRefToFp(), invFep);
      STensorOperation::multSTensor3(F, invFep, Fe);
      STensorOperation::inverseSTensor3(Fe, invFep);
      STensorOperation::multSTensor3(Fe, corSig, FeSigCor);
      STensorOperation::multSTensor3SecondTranspose(FeSigCor, invFep, Sig);
      
      // traction vector
      static SVector3 tractionForce;
      STensorOperation::multSTensor3SVector3(Sig,normal,tractionForce);
      double tauN = STensorOperation::dot(tractionForce,normal);
      // Deduce tangent part
      double tauTanSq = 0.;
      for (int i=0; i<3; i++)
      {
        double temp = tractionForce(i)-tauN*normal(i);
        tauTanSq += temp*temp;
      }
      // Compute effective stress
      double tauEff = 0.; 
      if (tauN > 0.)
      {
        tauEff = sqrt(tauN*tauN + _beta*tauTanSq);
      }
      else
      {
        tauEff = sqrt(_beta*tauTanSq);
      }
      
      if (tauEff - CftThomason*R >0)
      {
        interfaceNeckActive = true;
      } 
      else
      {
        interfaceNeckActive = false;
      }
      if (sqrt(3.*tauTanSq) - CftShear*R >0)
      {
        interfaceShearActive = true;
      }
      else
      {
        interfaceShearActive = false;
      }
    }
    
    static STensor3 sigEff;
    const STensor43& M = q1->getConstRefToAnisotropicTensor();
    STensorOperation::multSTensor43STensor3(M, corSig, sigEff);
    double Gurson = _mlawGrowth->I1J2J3_yieldFunction(sigEff,R,q0,q1,T);
    double Thomason = _mlawCoales->I1J2J3_yieldFunction(sigEff,R,q0,q1,T);
    double Shear = _mlawShear->I1J2J3_yieldFunction(sigEff,R,q0,q1,T);
    
    // active plasticity
    // check coalescence is active
    bool& CoalesFlag = q1Thom->getRefToCoalescenceOnsetFlag();
    int& CoalesMode = q1Thom->getRefToCoalescenceMode();
    CoalesFlag = false;
    CoalesMode = 0;
    
    if (_useTwoYieldRegularization)
    {
      double FG = Gurson +1.;
      double FT = Thomason +1.;
      double FS = Shear +1.;
      if (FG < 0. or FT < 0. or FS < 0)
      {
        Msg::Error("FT,FS FG must be all positive but FT=%e, FS = %e, FG=%e",FT,FS,FG);
      }
      // check necking
      if (std::max(FT,FS)> FG)
      {
        double ratio = FG/FT;
        if (pow(ratio,_yieldRegExponent) < _coalesTol  && interfaceNeckActive)
        {
          CoalesFlag = true;
          CoalesMode = 1;
          printf("Necking coalescence at Gurson = %e, Thomason =%e, Shear = %e\n",
                        Gurson,Thomason,Shear);
        }
        else
        {
          ratio = FG/FS;
          if (pow(ratio,_yieldRegExponent) < _coalesTol  && interfaceShearActive)
          {
            CoalesFlag = true; 
            CoalesMode = 2;
            printf("Shear coalescence at Gurson = %e, Thomason =%e, Shear = %e\n",
                                  Gurson,Thomason,Shear);
          }
          else
          {
            CoalesFlag = false;
            CoalesMode = 0;
          }
        }
      }
    }
    else
    {
      if (Thomason > std::max(Gurson,Shear) && interfaceNeckActive)
      {
        CoalesFlag = true;
        CoalesMode = 1;
        printf("Necking coalescence at Gurson = %e, Thomason =%e, Shear = %e\n",
                        Gurson,Thomason,Shear);
      }
      else if (Shear > std::max(Gurson,Thomason) && interfaceShearActive)
      {
        CoalesFlag = true;
        CoalesMode = 2;
        printf("Shear coalescence at Gurson = %e, Thomason =%e, Shear = %e\n",
                                Gurson,Thomason,Shear);
      }
      else
      {
        CoalesFlag = false;
        CoalesMode = 0;
      }
    }
       
   // if coalesnce occurs
    if (CoalesFlag)
    {
      q1Thom->setIPvAtCoalescenceOnset(*q1);
      // if offset is used to satisfy Thomason yield surface at the onset of coalescence
      if (_useTwoYieldRegularization)
      {
        // never offset with yield regulation
        q1Thom->getRefToCrackOffsetOnCft() = 1.;
        q1Thom->getRefToLodeParameterOffset() = 1.;
        q1Thom->getRefToYieldOffset() = 0.;
      }
      else
      {
        if (_withCftOffset)
        {
          if (CoalesMode==1)
          {
            q1Thom->getRefToCrackOffsetOnCft() = 1. + Thomason;
          }
          else if (CoalesMode==2)
          {
            q1Thom->getRefToCrackOffsetOnCft() = 1. + Shear;
          }
          q1Thom->getRefToLodeParameterOffset() = 1.;
          q1Thom->getRefToYieldOffset() = 0.;
          #ifdef _DEBUG
          printf("coalescence ocurrs using Cft offset = %e\n",q1Thom->getCrackOffsetOnCft());
          #endif //_DEBUG
        }
        else if (_withLodeOffset)
        {
          // Lode offset is not used for material law, because law already accounts for Lode variable
          q1Thom->getRefToCrackOffsetOnCft() = 1.;
          q1Thom->getRefToLodeParameterOffset()=  1.;
          q1Thom->getRefToYieldOffset() = 0.;
        }
        else if (_withYieldOffset)
        {
          q1Thom->getRefToCrackOffsetOnCft() = 1.;
          q1Thom->getRefToLodeParameterOffset() = 1.;
          if (CoalesMode==1)
          {
            q1Thom->getRefToYieldOffset() = Thomason;
          }
          else if (CoalesMode==2)
          {
            q1Thom->getRefToYieldOffset() = Shear;
          }
          #ifdef _DEBUG
          printf("coalescence ocurrs using yield offset = %e\n",q1Thom->getYieldOffset());
          #endif //_DEBUG
        }
        else
        {
          q1Thom->getRefToCrackOffsetOnCft() = 1.;
          q1Thom->getRefToLodeParameterOffset() = 1.;
          q1Thom->getRefToYieldOffset() = 0.;
          #ifdef _DEBUG
          printf("coalescence ocurrs, not offset is used\n");
          #endif //_DEBUG
        }
      }
    }
    else
    {
      q1Thom->getRefToCrackOffsetOnCft() = 1.;
      q1Thom->getRefToLodeParameterOffset() = 1.;
      q1Thom->getRefToYieldOffset() = 0.;
    }
  }
};
void mlawNonLocalPorousCoupledHill48LawWithMPSAndMSS::forceCoalescence(IPNonLocalPorosity* q1, const IPNonLocalPorosity *q0, const double* T) const
{
  Msg::Error("mlawNonLocalPorousCoupledHill48LawWithMPSAndMSS::forceCoalescence is not implemented!!!");
};
bool mlawNonLocalPorousCoupledHill48LawWithMPSAndMSS::checkCrackCriterion(IPNonLocalPorosity* q1, const IPNonLocalPorosity *q0, const STensor3& Fcur, const SVector3& normal, double delayFactor, const double* T) const
{
  Msg::Error("mlawNonLocalPorousCoupledHill48LawWithMPSAndMSS::checkCrackCriterion is not implemented!!!");
};


