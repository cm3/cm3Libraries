//
// C++ Interface: material law
//              Transverse Isotropic law for large strains it is a "virtual implementation" you have to define a law
//              in your project which derive from this law. you have to do THE SAME for your IPVariable
//              which have to derive from IPIPTransverseIsotropic
//              Implementation follows J. Bonet, AJ Burton, CMAME 1998, 162, 151-164
// Author:  <Gauthier BECKER>, (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef MLAWTRANSVERSEISOTROPIC_H_
#define MLAWTRANSVERSEISOTROPIC_H_
#include "mlaw.h"
#include "STensor3.h"
#include "STensor43.h"
#include "ipTransverseIsotropic.h"

class mlawTransverseIsotropic : public materialLaw
{
 protected:
  // can't be const due to operator= constructor (HOW TO CHANGE THIS ??)
  double _rho; // density
  double _E; // young modulus
  double _nu; // Poisson ratio
  double _nu_minor; //minor transverse Poisson ratio
  double _EA; // young modulus along A
  double _GA; // Shear modulud along A
  SVector3 _A; // direction of anisotropy
  double _lambda; // 1st lame parameter
  double _mu; // 2nd lame parameter (=G)
  double _K;  // lambda+2/3_mu
  double _n; // parameter of anisotropy (EA/E)
  double _m; // parameter of anisotropy (1-nu-2 n nu*nu)
  double _alpha; // parameter of anisotropy
  double _beta; // parameter of anisotropy
  double _gamma; // parameter of anisotropy

 public:
  mlawTransverseIsotropic(const int num,const double E,const double nu, const double rho, const double EA, const double GA, const double _nu_minor, const double Ax, const double Ay, const double Az);
 #ifndef SWIG
  mlawTransverseIsotropic(const mlawTransverseIsotropic &source);
  mlawTransverseIsotropic& operator=(const materialLaw &source);
	virtual ~mlawTransverseIsotropic(){};
	virtual materialLaw* clone() const {return new mlawTransverseIsotropic(*this);};
	virtual void checkInternalState(IPVariable* ipv, const IPVariable* ipvprev) const{}; // do nothing
  virtual bool withEnergyDissipation() const {return false;};
  // function of materialLaw
  virtual matname getType() const{return materialLaw::transverseIsotropic;}
  virtual void createIPState(IPStateBase* &ips, bool hasBodyForce, const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const IntPt *GP=NULL, const int gpt =0) const;
  virtual void initLaws(const std::map<int,materialLaw*> &maplaw){}; // this law is initialized so nothing to do
  virtual double soundSpeed() const; // default but you can redefine it for your case
	virtual double scaleFactor() const{return _mu;};
  virtual void getTransverseDirection(SVector3 &A, const IPTransverseIsotropic *q1) const
  {
     A=_A;
  }
  // specific function
 public:
  virtual void constitutive(
                            const STensor3& F0,         // initial deformation gradient (input @ time n)
                            const STensor3& Fn,         // updated deformation gradient (input @ time n+1)
                            STensor3 &P,                // updated 1st Piola-Kirchhoff stress tensor (output)
                                                        // contains the initial values on input
                            const IPVariable *q0,       // array of initial internal variable
                            IPVariable *q1,             // updated array of internal variable (in ipvcur on output),
                            STensor43 &Tangent,         // constitutive tangents (output)
                            const bool stiff, 
                            STensor43* elasticTangent =NULL,           // if true compute the tangents, 
                            const bool dTangent =false,
                            STensor63* dCalgdeps = NULL
                            ) const;
 protected:
  virtual double deformationEnergy(const STensor3 &C, const SVector3 &A) const ;
 #endif // SWIG
};

#endif // MLAWTRANSVERSEISOTROPIC_H_
