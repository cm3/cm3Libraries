//

//
// C++ Interface: material law
//
// Description: AnIsotropicElecTherMech law
//
//
// Author:  <L. Homsy>, (C) 2014
//
// Copyright: See COPYING file that comes with this distribution
//
//
//#include "mlawLinearElecTherMech.h"
#include <math.h>
#include "MInterfaceElement.h"
#include "mlawAnIsotropicElecTherMech.h"
#include "mlawTransverseIsotropic.h"


mlawAnIsotropicElecTherMech::mlawAnIsotropicElecTherMech(const int num,const double E, const double nu,const double rho,const double EA, const double GA, const double nu_minor,
                           const double Ax, const double Ay, const double Az, const double alpharot, const double betarot, const double gammarot,const double t0,const  double lx,
			   const  double ly,const  double lz,const double seebeck,const double kx,const double ky,
			   const double kz,const double cp,const double alphath,const double v0): 
                                 mlawAnIsotropicTherMech(num, E, nu, rho, EA, GA, nu_minor, Ax, Ay, Az, alpharot, betarot, gammarot, t0, kx, ky, kz, cp, alphath),
                                 _lx(lx),_ly(ly),_lz(lz),_seebeck(seebeck),_v0(v0)
{
	STensor3 l(0.);
	// to be unifom in DG3D e= l' gradV with l'=-l instead of e=-l gradV
	l(0,0)  = -_lx;
	l(1,1)  = -_ly;
	l(2,2)  = -_lz;
  	for(int i=0;i<3;i++)
  	{
    	  for(int j=0;j<3;j++)
    	  {
             _l0(i,j)=0.;
             for(int m=0;m<3;m++)
             {
               for(int n=0;n<3;n++)
            	{
              	  _l0(i,j)+=_rotationMatrix(m,i)*_rotationMatrix(n,j)*l(m,n);
       	        }
     	     }
   	   }
	}	  
} 

mlawAnIsotropicElecTherMech::mlawAnIsotropicElecTherMech(const mlawAnIsotropicElecTherMech &source) : mlawAnIsotropicTherMech(source), _lx(source._lx),
						      _ly(source._ly),_lz(source._lz),_seebeck(source._seebeck), _v0(source._v0), _l0(source._l0)
{  
					  
}
mlawAnIsotropicElecTherMech& mlawAnIsotropicElecTherMech::operator=(const materialLaw &source)
{
  mlawAnIsotropicTherMech::operator=(source);
  const mlawAnIsotropicElecTherMech* src =dynamic_cast<const mlawAnIsotropicElecTherMech*>(&source);
  if(src!=NULL)
  {
    _lx = src->_lx;
    _ly = src->_ly;
    _lz = src->_lz;
    _seebeck=src->_seebeck;
    _l0 =src->_l0;
     _v0 =src->_v0;
   }
   return *this;
}

void mlawAnIsotropicElecTherMech::createIPState(IPStateBase* &ips,  bool hasBodyForce, const bool* state_,const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
  bool inter=true;
  const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
  if(iele==NULL) inter=false;

  IPVariable* ipvi = new IPAnIsotropicElecTherMech();
  IPVariable* ipv1 = new IPAnIsotropicElecTherMech();
  IPVariable* ipv2 = new IPAnIsotropicElecTherMech();
  if(ips != NULL) delete ips;
  ips = new IP3State(state_,ipvi,ipv1,ipv2);
}

void mlawAnIsotropicElecTherMech::constitutive(
                            const STensor3& F0,         // initial deformation gradient (input @ time n)
                            const STensor3& Fn,         // updated deformation gradient (input @ time n+1)
                            STensor3 &P,                // updated 1st Piola-Kirchhoff stress tensor (output)
                                                        // contains the initial values on input
                            const IPAnIsotropicElecTherMech *q0,       // array of initial internal variable
                            IPAnIsotropicElecTherMech *q1,             // updated array of internal variable (in ipvcur on output),
                            STensor43 &Tangent,         // constitutive tangents (output)
                            double T0,			// initial Temperature
			    double T,			// updated Temperature
			    double V0,			// updated Voltage
			    double V,			// updated Voltage
  			    const SVector3 &gradT,
			    const SVector3 &gradV,
                            SVector3 &fluxT,            // Thermal flux
			    SVector3 &fluxV,		// Electric current flux
                            STensor3 &dPdT,
                            STensor3 &dPdV,
                            STensor3 &dqdgradT,
                            SVector3 &dqdT,
			    STensor3 &dqdgradV,
                            SVector3 &dqdV,
			    SVector3 &djedV,
			    STensor3 &djedgradV,
			    SVector3 &djedT,
			    STensor3 &djedgradT,
                            STensor33 &dqdF,
			    STensor33 &djedF,
			    double &w,			// Thermal source (ex. heat cpacity)
			    double &dwdt,
			    STensor3 &dwdf,
			    STensor3 &l10,
			    STensor3 &l20,
			    STensor3 &k10,
			    STensor3 &k20,
			    STensor3 &dl10dT,
			    STensor3 &dl20dT,
			    STensor3 &dk10dT,
			    STensor3 &dk20dT,
			    STensor3 &dl10dv,
			    STensor3 &dl20dv,
			    STensor3 &dk10dv,
			    STensor3 &dk20dv,
			    STensor43 &dl10dF,
			    STensor43 &dl20dF,
			    STensor43 &dk10dF,
			    STensor43 &dk20dF,
			    SVector3  &fluxjy,     	 // Energy flux
			    SVector3  &djydV,
			    STensor3  &djydgradV,
			    SVector3  &djydT,
			    STensor3  &djydgradT,
                            STensor33 &djydF,
			    STensor3 &djydgradVdT,
			    STensor3 &djydgradVdV,
			    STensor43 &djydgradVdF,	
			    STensor3 &djydgradTdT,
			    STensor3 &djydgradTdV,
			    STensor43 &djydgradTdF,	
                            const bool stiff
                           )const
{
    mlawAnIsotropicTherMech::constitutive(F0, Fn, P, q0, q1, Tangent, T0, T, gradT, fluxT, dPdT, dqdgradT, dqdT, dqdF, w,	dwdt, dwdf, stiff);
    //these ones will be recomputed
    STensorOperation::zero(fluxT);
    STensorOperation::zero(dqdgradT);
    STensorOperation::zero(dqdT);
    STensorOperation::zero(dqdF);

    /* compute gradient of deformation */
    double Jac=  STensorOperation::determinantSTensor3(Fn);
    double lnJ = log(Jac);
    static STensor3 Fninv;
    STensorOperation::inverseSTensor3(Fn, Fninv);
  

    double dseebeckdT=0.;
    static STensor3 dLdT,dKdT;
    STensorOperation::zero(dLdT);
    STensorOperation::zero(dKdT);
    
    STensorOperation::zero(dseebeckdT);
    STensorOperation::zero(djedV);
    STensorOperation::zero(djedT);
    STensorOperation::zero(dqdV);
    STensorOperation::zero(dqdF); // dependency of flux with deformation gradient
    STensorOperation::zero(djedF);
    STensorOperation::zero(dPdV);
    STensorOperation::zero(dqdgradT);
    STensorOperation::zero(dqdgradV);
    STensorOperation::zero(fluxjy);
    STensorOperation::zero(djedF); STensorOperation::zero(dqdF);STensorOperation::zero(djydF);
    STensorOperation::zero(djydgradT);STensorOperation::zero(djydgradV); STensorOperation::zero(djydV); STensorOperation::zero(djydT);
  //STensorOperation::zero(jy1);STensorOperation::zero(djy1dT);STensorOperation::zero(djy1dV); STensorOperation::zero(djy1dF);
    
    STensorOperation::zero(l10);
    STensorOperation::zero(l20);
    STensorOperation::zero(k10);
    STensorOperation::zero(k20);
    STensorOperation::zero(dl10dT);
    STensorOperation::zero(dl20dT);
    STensorOperation::zero(dk10dT);
    STensorOperation::zero(dk20dT);
    STensorOperation::zero(dl10dv);
    STensorOperation::zero(dl20dv);
    STensorOperation::zero(dk10dv);
    STensorOperation::zero(dk20dv);
    STensorOperation::zero(dl10dF);
    STensorOperation::zero(dl20dF);
    STensorOperation::zero(dk10dF);
    STensorOperation::zero(dk20dF);

    STensorOperation::zero(djydgradVdT);STensorOperation::zero(djydgradVdV);STensorOperation::zero(djydgradVdF);
    STensorOperation::zero(djydgradTdT);STensorOperation::zero(djydgradTdV);STensorOperation::zero(djydgradTdF);
	   
    //large defo
    static STensor3 _Lref0,_Kref0;
    STensorOperation::zero(_Lref0);
    STensorOperation::zero(_Kref0);
    
    for(int K = 0; K< 3; K++)
    {
      for(int L = 0; L< 3; L++)
      {
        for(int i = 0; i< 3; i++)
        {
          for(int j = 0; j< 3; j++)
          {
	    _Kref0(K,L)  += Fninv(K,i)*_k0(i,j)*Fninv(L,j);
	    _Lref0(K,L)  += Fninv(K,i)*_l0(i,j)*Fninv(L,j);
          }
        }
      }
    }   
  
    //energy conjugated fluxes
    for(int i = 0; i< 3; i++)
    { 
      for(int j = 0; j< 3; j++)
      {	 
        l10(i,j) = -_Lref0(i,j)*T*Jac;
	l20(i,j) = -_Lref0(i,j)*(V*T+_seebeck*T*T)*Jac;
	//k10(i,j) = -_Kref0(i,j)*T*T*Jac-_seebeck*_Lref0(i,j)*T*T*V*Jac-pow(_seebeck,2)*_Lref0(i,j)*pow(T,3)*Jac;
	//k20(i,j) = -_seebeck*T*T*_Lref0(i,j)*Jac ; 
        k20(i,j) = l20(i,j); 	
      //jy1(i,j) = k10(i,j) -_seebeck*_Lref0(i,j)*T*T*V*Jac-_Lref0(i,j)*T*V*V*Jac;
      // jy1(i,j) = -_Kref0(i,j)*T*T*Jac-2.*_seebeck*_Lref0(i,j)*T*T*V*Jac-pow(_seebeck,2)*_Lref0(i,j)*pow(T,3)*Jac-_Lref0(i,j)*T*V*V*Jac;	
        k10(i,j) = -_Kref0(i,j)*T*T*Jac-2.*_seebeck*_Lref0(i,j)*T*T*V*Jac-pow(_seebeck,2)*_Lref0(i,j)*pow(T,3)*Jac-_Lref0(i,j)*T*V*V*Jac;
      }
   }      
   //_Lref0.print("_Lref0");
   //_Kref0.print("_Kref0"); 

   //classical flux
   for(int i=0;i<3;i++)
    {
      fluxV(i)=0.;
      for(int j=0;j<3;j++)
      {
       fluxV(i)+=_Lref0(i,j)*gradV(j)*Jac+_Lref0(i,j)*_seebeck*gradT(j)*Jac;
      }
    }  
    for(int i=0;i<3;i++)
    {
      fluxT(i)=0.;
      fluxjy(i)=0.;
      for(int j=0;j<3;j++)
      {
        double val=(_Kref0(i,j)+_Lref0(i,j)*_seebeck*_seebeck*T)*gradT(j)*Jac+_Lref0(i,j)*_seebeck*T*gradV(j)*Jac;
	fluxT(i)+=val;
	fluxjy(i)+=val+ (_Lref0(i,j)*_seebeck*V)*gradT(j)*Jac+(_Lref0(i,j)*V)*gradV(j)*Jac;
      }
    }
  
  if(stiff)
  {

   STensor43 _I4(1.,1.), DinvFndF, dLdF, dKdF;
   STensor3 dJacdF;
   STensorOperation::zero(DinvFndF);STensorOperation::zero(dLdF);STensorOperation::zero(dJacdF);

      for (int i=0; i<3; i++){
        for (int j=0; j<3; j++){
          for (int p=0; p<3; p++){
            for (int q=0; q<3; q++){
              for (int k=0; k<3; k++){
                for (int l=0; l<3; l++){
                  DinvFndF(i,j,k,l) -= Fninv(i,p)*_I4(p,q,k,l)*Fninv(q,j);
                }
              }
            }
          }
        }
      }


     for(int i = 0; i< 3; i++)
     {
       for(int j = 0; j< 3; j++)
       {
         dJacdF(i,j)=Jac*Fninv(j,i);
         for(int k = 0; k< 3; k++)
         {
           for(int l = 0; l< 3; l++)
           {
	     for(int o = 0; o< 3; o++)
              {
                for(int n = 0; n< 3; n++)
                 {
                  dLdF(i,j,k,l) += DinvFndF(i,n,k,l)*_l0(n,o)*Fninv(j,o);
                  dLdF(i,j,k,l) += Fninv(i,n)*_l0(n,o)*DinvFndF(j,o,k,l);

                  dKdF(i,j,k,l) += DinvFndF(i,n,k,l)*_k0(n,o)*Fninv(j,o);
                  dKdF(i,j,k,l) += Fninv(i,n)*_k0(n,o)*DinvFndF(j,o,k,l);
                }
              }
            }
          }
        }
      }


    //classical flux
    for(int K = 0; K< 3; K++)
    {
      for(int m = 0; m< 3; m++)
      {
        for(int N = 0; N< 3; N++)
        {
          djedF(K,m,N)=0.;
          dqdF(K,m,N)=0.;
          djydF(K,m,N)=0.;
          for(int L = 0; L< 3; L++)
          {
            djedF(K,m,N) -= Fninv(K,m)*_Lref0(N,L)*gradV(L)*Jac;
            djedF(K,m,N) -= _Lref0(K,N)*Fninv(L,m)*gradV(L)*Jac;
            djedF(K,m,N) += _Lref0(K,L)*gradV(L)*Fninv(N,m)*Jac;
	    djedF(K,m,N) -= Fninv(K,m)*_seebeck*_Lref0(N,L)*gradT(L)*Jac;
            djedF(K,m,N) -= _Lref0(K,N)*_seebeck*Fninv(L,m)*gradT(L)*Jac;
            djedF(K,m,N) += _Lref0(K,L)*_seebeck*gradT(L)*Fninv(N,m)*Jac;
	    
            dqdF(K,m,N) -= Fninv(K,m)*(_Kref0(N,L)+_Lref0(N,L)*_seebeck*_seebeck*T)*gradT(L)*Jac;
            dqdF(K,m,N) -= (_Kref0(K,N)+_Lref0(K,N)*_seebeck*_seebeck*T)*Fninv(L,m)*gradT(L)*Jac;
            dqdF(K,m,N) += (_Kref0(K,L)+_Lref0(K,L)*_seebeck*_seebeck*T)*gradT(L)*Fninv(N,m)*Jac;
	    dqdF(K,m,N) -= Fninv(K,m)*(_Lref0(N,L)*_seebeck*T)*gradV(L)*Jac;
            dqdF(K,m,N) -= (_Lref0(K,N)*_seebeck*T)*Fninv(L,m)*gradV(L)*Jac;
            dqdF(K,m,N) += (_Lref0(K,L)*_seebeck*T)*gradV(L)*Fninv(N,m)*Jac;
	    
	    djydF(K,m,N) -= Fninv(K,m)*(_Kref0(N,L)+_Lref0(N,L)*_seebeck*_seebeck*T+_Lref0(N,L)*_seebeck*V)*gradT(L)*Jac;
            djydF(K,m,N) -= (_Kref0(K,N)+_Lref0(K,N)*_seebeck*_seebeck*T+_Lref0(K,N)*_seebeck*V)*Fninv(L,m)*gradT(L)*Jac;
            djydF(K,m,N) += (_Kref0(K,L)+_Lref0(K,L)*_seebeck*_seebeck*T+_Lref0(K,L)*_seebeck*V)*gradT(L)*Fninv(N,m)*Jac;
	    djydF(K,m,N) -= Fninv(K,m)*(_Lref0(N,L)*V+_Lref0(N,L)*_seebeck*T)*gradV(L)*Jac;
            djydF(K,m,N) -= (_Lref0(K,N)*V+_Lref0(K,N)*_seebeck*T)*Fninv(L,m)*gradV(L)*Jac;
            djydF(K,m,N) += (_Lref0(K,L)*V+_Lref0(K,L)*_seebeck*T)*gradV(L)*Fninv(N,m)*Jac;

            djydgradVdF(K,m,N,L) += dLdF(K,m,N,L)*_seebeck*T*Jac;
            djydgradVdF(K,m,N,L) += dLdF(K,m,N,L)*V*Jac;
            djydgradVdF(K,m,N,L) += _Lref0(K,m)*dJacdF(N,L)*_seebeck*T;
            djydgradVdF(K,m,N,L) += _Lref0(K,m)*dJacdF(N,L)*V;

            djydgradTdF(K,m,N,L) += dKdF(K,m,N,L)*Jac;
            djydgradTdF(K,m,N,L) += _Kref0(K,m)*dJacdF(N,L);
            djydgradTdF(K,m,N,L) += dLdF(K,m,N,L)*_seebeck*_seebeck*T*Jac;
            djydgradTdF(K,m,N,L) += dLdF(K,m,N,L)*_seebeck*V*Jac;
            djydgradTdF(K,m,N,L) += _Lref0(K,m)*_seebeck*_seebeck*T*dJacdF(N,L);
            djydgradTdF(K,m,N,L) += _Lref0(K,m)*_seebeck*V*dJacdF(N,L);
          }
        }
      }
    }

     for(int i = 0; i< 3; i++)
     {
      for(int j = 0; j< 3; j++)
      {
	dl10dT(i,j) = -_Lref0(i,j)*Jac;
	dl20dT(i,j) = -_Lref0(i,j)*(V+2*_seebeck*T)*Jac;
	//dk10dT(i,j) = -2*_Kref0(i,j)*T*Jac- 2*_seebeck*_Lref0(i,j)*T*V*Jac-3*pow(_seebeck,2)*_Lref0(i,j)*pow(T,2)*Jac;
	//dk20dT(i,j) = -2*_seebeck*T*_Lref0(i,j)*Jac;
        dk20dT(i,j) = dl20dT(i,j);
	 
	dl20dv(i,j)= -_Lref0(i,j)*(T)*Jac;
	dk20dv(i,j)= dl20dv(i,j);
	//dk10dv(i,j)= -_seebeck*_Lref0(i,j)*T*T*Jac;

        dk10dT(i,j) = -2.*_Kref0(i,j)*T*Jac- 4.*_seebeck*_Lref0(i,j)*T*V*Jac-3*pow(_seebeck,2)*_Lref0(i,j)*pow(T,2)*Jac-_Lref0(i,j)*V*V*Jac;
        dk10dv(i,j) = -2.*_seebeck*_Lref0(i,j)*T*T*Jac-2.*_Lref0(i,j)*T*V*Jac; 
	
	//djy1dT(i,j) = dk10dT(i,j)-2.*_seebeck*_Lref0(i,j)*T*V*Jac-_Lref0(i,j)*V*V*Jac;
	//djy1dV(i,j) = dk10dv(i,j)-1.*_seebeck*_Lref0(i,j)*T*T*Jac-2.*_Lref0(i,j)*T*V*Jac;
      }
    }

    static STensor43 dldF,dkdF;
    STensorOperation::zero(dldF);
    STensorOperation::zero(dkdF);
    
    for(int K = 0; K< 3; K++)
    {
      for(int L = 0; L< 3; L++)
      {
	for(int m = 0; m< 3; m++)
	  {
	    for(int N = 0; N< 3; N++)
	      {
		dldF(K,L,m,N) -= Fninv(K,m)*_Lref0(N,L)*Jac;
		dldF(K,L,m,N) -= _Lref0(K,N)*Fninv(L,m)*Jac;
		dldF(K,L,m,N) += _Lref0(K,L)*Fninv(N,m)*Jac;
		
		dkdF(K,L,m,N) -= Fninv(K,m)*_Kref0(N,L)*Jac;
		dkdF(K,L,m,N) -= _Kref0(K,N)*Fninv(L,m)*Jac;
		dkdF(K,L,m,N) += _Kref0(K,L)*Fninv(N,m)*Jac;
	      }
	  }
      }
    }
    for(int i = 0; i< 3; i++)
      {
	for(int j = 0; j< 3; j++)
	 {
	   for(int N = 0; N< 3; N++)
	     {
	      for(int L = 0; L< 3; L++)
		{		  		   
		  dl10dF(i,j,N,L) = -dldF(i,j,N,L)*T;
 		  dl20dF(i,j,N,L) = -dldF(i,j,N,L)*(V*T+_seebeck*T*T);
		  //dk10dF(i,j,N,L) = -dkdF(i,j,N,L)*T*T- _seebeck*dldF(i,j,N,L)*T*T*V-pow(_seebeck,2)*dldF(i,j,N,L)*pow(T,3);
		  dk20dF(i,j,N,L) = dl20dF(i,j,N,L);
                  dk10dF(i,j,N,L) = -dkdF(i,j,N,L)*T*T- 2.*_seebeck*dldF(i,j,N,L)*T*T*V-pow(_seebeck,2)*dldF(i,j,N,L)*pow(T,3)-dldF(i,j,N,L)*T*V*V;
		 // djy1dF(i,j,N,L) = -dkdF(i,j,N,L)*T*T- 2.*_seebeck*dldF(i,j,N,L)*T*T*V-pow(_seebeck,2)*dldF(i,j,N,L)*pow(T,3)-dldF(i,j,N,L)*T*V*V;
		}
	    }
	 }
      }
      
    // dqdF.print("dqdF");
     for(int i=0;i<3;i++)
    {
      djedT(i)=0.;
      for(int j=0;j<3;j++)
      {
         djedT(i)+=dseebeckdT*_Lref0(i,j)*gradT(j)*Jac+_seebeck*dLdT(i,j)*gradT(j)*Jac+dLdT(i,j)*gradV(j)*Jac;
       }
    }
    
    djedgradT=_Lref0;
    djedgradT*=_seebeck*Jac;
    djedgradV =_Lref0;
    djedgradV*=Jac;
    
   // djedgradT.print("djedgradT");
  //  djedgradV.print("djedgradV");

    for(int i=0;i<3;i++)
    {
      dqdT(i)=0.;djydT(i)=0.;djydV(i)=0.;
      for(int j=0;j<3;j++)
      {
         dqdT(i)+=dKdT(i,j)*gradT(j)*Jac+2*_seebeck*dseebeckdT*_Lref0(i,j)*T*gradT(j)*Jac+_seebeck*_seebeck*dLdT(i,j)*T*gradT(j)*Jac+_seebeck*_seebeck*_Lref0(i,j)*gradT(j)*Jac
	 +dseebeckdT*_Lref0(i,j)*T*gradV(j)*Jac+_seebeck*dLdT(i,j)*T*gradV(j)*Jac+_seebeck*_Lref0(i,j)*gradV(j)*Jac;
	 
	 djydT(i)+=_seebeck*_seebeck*_Lref0(i,j)*gradT(j)*Jac+_seebeck*_Lref0(i,j)*gradV(j)*Jac;
	 djydV(i)+=_seebeck*_Lref0(i,j)*gradT(j)*Jac+_Lref0(i,j)*gradV(j)*Jac;
       }
    }

    for(int i=0;i<3;i++)
    {
      for(int k=0;k<3;k++)
      {
	//dqdgradT(i,k)=_Kref0(i,k)+_Lref0(i,k)*_seebeck*_seebeck*T;
	dqdgradT(i,k) =_Kref0(i,k)*Jac;
	dqdgradT(i,k)+=_Lref0(i,k)*_seebeck*_seebeck*T*Jac;
	dqdgradV(i,k) =_Lref0(i,k)*_seebeck*T*Jac;
	
	djydgradT(i,k) =_Kref0(i,k)*Jac;
	djydgradT(i,k)+=_Lref0(i,k)*_seebeck*_seebeck*T*Jac+_Lref0(i,k)*_seebeck*V*Jac;
	djydgradV(i,k) =_Lref0(i,k)*_seebeck*T*Jac+_Lref0(i,k)*V*Jac;
	djydgradVdT(i,k) =_Lref0(i,k)*_seebeck*Jac;
	djydgradVdV(i,k) =_Lref0(i,k)*Jac;
	djydgradTdT(i,k)+=_Lref0(i,k)*_seebeck*_seebeck*Jac;
	djydgradTdV(i,k)+=_Lref0(i,k)*_seebeck*Jac;
      }
   }    
    
  }
  q1->_ElecThermEnergy=deformationEnergy(gradV,gradT,fluxV,fluxjy,T,V);
  //q1->_elasticEnergy=deformationEnergy(gradV,gradT,fluxV,fluxjy,T,V);
}
  
double mlawAnIsotropicElecTherMech::deformationEnergy(const SVector3 &gradV,const SVector3 &gradT,const SVector3 &fluxV,
							const SVector3 &fluxjy,const double T, const double V) const // If i have sigma ...
{
  double fz=0.;
  for(int i=0;i<3;i++)
  {
   fz+=-fluxV(i)*gradV(i)/T+fluxV(i)*V*gradT(i)/(T*T)-fluxjy(i)*gradT(i)/(T*T);
   //  fz+=fluxV(i)*(-gradV(i)/T+V*gradT(i)/(T*T))+ fluxjy(i)*(-gradT(i)/(T*T));
  }
  //return sqrt(fabs(fz));  		
  return fabs(fz);  		
}
  
    
    

