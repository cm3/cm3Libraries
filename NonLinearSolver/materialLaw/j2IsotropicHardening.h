//
// C++ Interface: terms
//
// Description: Define isotropic hardening laws for j" plasticity
//
//
// Author:  <L. Noels>, (C) 2013
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef _J2ISOTROPICHARDENING_H_
#define _J2ISOTROPICHARDENING_H_
#ifndef SWIG
#include "ipstate.h"
#include "MElement.h"
#include "ipHardening.h"
#include "scalarFunction.h"
#include "fullMatrix.h"
#include "generalMapping.h"
#endif
class J2IsotropicHardening{
 public :
  enum hardeningname{perfectlyPlasticJ2IsotropicHardening,powerLawJ2IsotropicHardening, exponentialJ2IsotropicHardening,
                     swiftJ2IsotropicHardening, linearExponentialJ2IsotropicHardening,
                     linearFollowedByExponentialJ2IsotropicHardening, linearFollowedByPowerLawJ2IsotropicHardening,
                     polynomialJ2IsotropicHardening,
                     twoExpJ2IsotropicHaderning, tanhJ2IsotropicHaderning,gursonJ2IsotropicHardening, multipleSwiftJ2IsotropicHardening,
                     linearExponentialFollowedBySwiftLawJ2IsotropicHardening,LinearFollowedByExponentialFollowedByMultiplePowerLawJ2IsotropicHardening,LinearFollowedByMultipleSwiftJ2IsotropicHardening,
                     UserIsoLaw};
 protected :
  int _num; // number of law (must be unique !)
  bool _initialized; // to initialize law
  double _yield0;
  scalarFunction* _temFunc_Sy0;

 public:
  void setTemperatureFunction_Sy0(const scalarFunction& Tfunc);

#ifndef SWIG
  // constructor
  J2IsotropicHardening(const int num, double yield0, const bool init=true);
  virtual ~J2IsotropicHardening();
  J2IsotropicHardening(const J2IsotropicHardening &source);
  virtual J2IsotropicHardening& operator=(const J2IsotropicHardening &source);
  virtual int getNum() const{return _num;}

  virtual const bool isInitialized() {return _initialized;}
  virtual double getYield0() const {return _yield0;}
  virtual double getYield0(double T) const {return _yield0*_temFunc_Sy0->getVal(T);}
  virtual double getDYield0DT(double T) const {return _yield0*_temFunc_Sy0->getDiff(T);}
  virtual double getDDYield0DTT(double T) const {return _yield0*_temFunc_Sy0->getDoubleDiff(T);}

  virtual hardeningname getType() const=0;
  virtual void createIPVariable(IPJ2IsotropicHardening* &ipv) const=0;
  virtual void initLaws(const std::map<int,J2IsotropicHardening*> &maplaw)=0;

  virtual void hardening(double p0, const IPJ2IsotropicHardening &ipvprev, double p, IPJ2IsotropicHardening &ipv) const=0;
  virtual J2IsotropicHardening * clone() const=0;
  virtual void hardening(double p0, const IPJ2IsotropicHardening &ipvprev, double p, double T, IPJ2IsotropicHardening &ipv) const=0;
  #endif
};
class PerfectlyPlasticJ2IsotropicHardening : public J2IsotropicHardening
{
 public :
  // constructor
  PerfectlyPlasticJ2IsotropicHardening(const int num, double yield0, const bool init=true);
	#ifndef SWIG
  virtual ~PerfectlyPlasticJ2IsotropicHardening(){}
  PerfectlyPlasticJ2IsotropicHardening(const PerfectlyPlasticJ2IsotropicHardening &source);
  virtual PerfectlyPlasticJ2IsotropicHardening& operator=(const J2IsotropicHardening &source);
  virtual int getNum() const{return _num;}
  virtual hardeningname getType() const{return J2IsotropicHardening::perfectlyPlasticJ2IsotropicHardening; };
  virtual void createIPVariable(IPJ2IsotropicHardening* &ipv) const;
  virtual void initLaws(const std::map<int,J2IsotropicHardening*> &maplaw) {}; //nothing now, we will see if we use the mapping
  virtual void hardening(double p0, const IPJ2IsotropicHardening &ipvprev, double p, IPJ2IsotropicHardening &ipv) const;
  virtual J2IsotropicHardening * clone() const;
  virtual void hardening(double p0, const IPJ2IsotropicHardening &ipvprev, double p, double T, IPJ2IsotropicHardening &ipv) const;
  #endif
};

class UserFunctionIsotropicHardening : public J2IsotropicHardening
{
  protected:
    generalMapping* _isoFuncUser; 
    // function return R, dR, ddR, intR if only plastic strain as input
    // function return R, dR, ddR, intR, dRdT,ddRdTT,ddRdT if only plastic strain and temperature as inputs
    
  public:
    UserFunctionIsotropicHardening(const int num, double yield0, const generalMapping& isoFunc, const bool init=true);
    #ifndef SWIG
    virtual ~UserFunctionIsotropicHardening();
    UserFunctionIsotropicHardening(const UserFunctionIsotropicHardening &source);
    virtual UserFunctionIsotropicHardening& operator=(const J2IsotropicHardening &source);
    virtual int getNum() const{return _num;}
    virtual hardeningname getType() const{return J2IsotropicHardening::UserIsoLaw; };
    virtual void createIPVariable(IPJ2IsotropicHardening* &ipv) const;
    virtual void initLaws(const std::map<int,J2IsotropicHardening*> &maplaw) {}; //nothing now, we will see if we use the mapping
    virtual void hardening(double p0, const IPJ2IsotropicHardening &ipvprev, double p, IPJ2IsotropicHardening &ipv) const;
    virtual J2IsotropicHardening * clone() const;
    virtual void hardening(double p0, const IPJ2IsotropicHardening &ipvprev, double p, double T, IPJ2IsotropicHardening &ipv) const;
    #endif
};

class PowerLawJ2IsotropicHardening : public J2IsotropicHardening
{
 // R = yield0 + h p^hexp if p>pth
 // R = yield0 + (h pth^hexp)/pth p if p<=pth
 protected :
  double _h, _hexp, _pth;
  scalarFunction* _temFunc_h;
  scalarFunction* _temFunc_hexp;

 public:
  // constructor
  PowerLawJ2IsotropicHardening(const int num,  double yield0, double h, double hexp, double pth=1.e-16, bool init=true);
  void setTemperatureFunction_h(const scalarFunction& Tfunc);
  void setTemperatureFunction_hexp(const scalarFunction& Tfunc);

#ifndef SWIG
  virtual ~PowerLawJ2IsotropicHardening();
  PowerLawJ2IsotropicHardening(const PowerLawJ2IsotropicHardening &source);
  virtual PowerLawJ2IsotropicHardening& operator=(const J2IsotropicHardening &source);
  virtual int getNum() const{return _num;}
  virtual hardeningname getType() const{return J2IsotropicHardening::UserIsoLaw; };
  virtual void createIPVariable(IPJ2IsotropicHardening* &ipv) const;
  virtual void initLaws(const std::map<int,J2IsotropicHardening*> &maplaw) {}; //nothing now, we will see if we use the mapping
  virtual void hardening(double p0, const IPJ2IsotropicHardening &ipvprev, double p, IPJ2IsotropicHardening &ipv) const;
  virtual J2IsotropicHardening * clone() const;
  virtual void hardening(double p0, const IPJ2IsotropicHardening &ipvprev, double p, double T, IPJ2IsotropicHardening &ipv) const;
#endif
};

class ExponentialJ2IsotropicHardening : public J2IsotropicHardening
{
 // R = yield0 +h * (1-exp (-hexp *p))
 protected :
  double _h, _hexp;
  scalarFunction* _temFunc_h;
  scalarFunction* _temFunc_hexp;

 public:
  // constructor
  ExponentialJ2IsotropicHardening(const int num,  double yield0, double h, double hexp, bool init=true);
  void setTemperatureFunction_h(const scalarFunction& Tfunc);
  void setTemperatureFunction_hexp(const scalarFunction& Tfunc);

#ifndef SWIG
  virtual ~ExponentialJ2IsotropicHardening();
  ExponentialJ2IsotropicHardening(const ExponentialJ2IsotropicHardening &source);
  virtual ExponentialJ2IsotropicHardening& operator=(const J2IsotropicHardening &source);
  virtual int getNum() const{return _num;}
  virtual hardeningname getType() const{return J2IsotropicHardening::exponentialJ2IsotropicHardening; };
  virtual void createIPVariable(IPJ2IsotropicHardening* &ipv) const;
  virtual void initLaws(const std::map<int,J2IsotropicHardening*> &maplaw) {}; //nothing now, we will see if we use the mapping
  virtual void hardening(double p0, const IPJ2IsotropicHardening &ipvprev, double p, IPJ2IsotropicHardening &ipv) const;
  virtual J2IsotropicHardening * clone() const;
  virtual void hardening(double p0, const IPJ2IsotropicHardening &ipvprev, double p, double T, IPJ2IsotropicHardening &ipv) const;
#endif
};

class EnhancedExponentialJ2IsotropicHardening : public J2IsotropicHardening
{
 // R = yield0 +h * (1-exp (-hexp *p))*(1+c*p)^n
 protected :
  double _h, _hexp,_c, _n;
  scalarFunction* _temFunc_h;
  scalarFunction* _temFunc_hexp;

 public:
  // constructor
  EnhancedExponentialJ2IsotropicHardening(const int num,  double yield0, double h, double hexp, double c, double n);
  void setTemperatureFunction_h(const scalarFunction& Tfunc);
  void setTemperatureFunction_hexp(const scalarFunction& Tfunc);

#ifndef SWIG
  virtual ~EnhancedExponentialJ2IsotropicHardening();
  EnhancedExponentialJ2IsotropicHardening(const EnhancedExponentialJ2IsotropicHardening &source);
  virtual EnhancedExponentialJ2IsotropicHardening& operator=(const J2IsotropicHardening &source);
  virtual int getNum() const{return _num;}
  virtual hardeningname getType() const{return J2IsotropicHardening::exponentialJ2IsotropicHardening; };
  virtual void createIPVariable(IPJ2IsotropicHardening* &ipv) const;
  virtual void initLaws(const std::map<int,J2IsotropicHardening*> &maplaw) {}; //nothing now, we will see if we use the mapping
  virtual void hardening(double p0, const IPJ2IsotropicHardening &ipvprev, double p, IPJ2IsotropicHardening &ipv) const;
  virtual J2IsotropicHardening * clone() const;
  virtual void hardening(double p0, const IPJ2IsotropicHardening &ipvprev, double p, double T, IPJ2IsotropicHardening &ipv) const;
#endif
};

class ExponentialSeriesJ2IsotropicHardening : public J2IsotropicHardening
{
 // R = yield0 + \sum_i h[i] * (1-exp (-hexp[i] *p))
 protected :
  std::vector<double> _h, _hexp;
  std::vector<scalarFunction*> _temFunc_h;
  std::vector<scalarFunction*> _temFunc_hexp;
 public:
  // constructor
  ExponentialSeriesJ2IsotropicHardening(const int num, const int el, double yield0);
  void set(int i, double h, double hexp, const scalarFunction* func_h = NULL, const scalarFunction* func_hexp= NULL);
#ifndef SWIG
  virtual ~ExponentialSeriesJ2IsotropicHardening();
  ExponentialSeriesJ2IsotropicHardening(const ExponentialSeriesJ2IsotropicHardening &source);
  virtual ExponentialSeriesJ2IsotropicHardening& operator=(const J2IsotropicHardening &source);
  virtual int getNum() const{return _num;}
  virtual hardeningname getType() const{return J2IsotropicHardening::exponentialJ2IsotropicHardening; };
  virtual void createIPVariable(IPJ2IsotropicHardening* &ipv) const;
  virtual void initLaws(const std::map<int,J2IsotropicHardening*> &maplaw) {}; //nothing now, we will see if we use the mapping
  virtual void hardening(double p0, const IPJ2IsotropicHardening &ipvprev, double p, IPJ2IsotropicHardening &ipv) const;
  virtual J2IsotropicHardening * clone() const;
  virtual void hardening(double p0, const IPJ2IsotropicHardening &ipvprev, double p, double T, IPJ2IsotropicHardening &ipv) const;
#endif
};

class SwiftJ2IsotropicHardening : public J2IsotropicHardening
{
  // R =  yield0 if p <p0
  // R = yield0 * (1+ h*(p-p0))^hexp if p >= p0
  protected :
    double _p0;
    double _h, _hexp;
    scalarFunction* _temFunc_h;
    scalarFunction* _temFunc_hexp;

 public:
  // constructor
  SwiftJ2IsotropicHardening(const int num,  double yield0, double h, double hexp, double p0=0., bool init=true);
  void setTemperatureFunction_h(const scalarFunction& Tfunc);
  void setTemperatureFunction_hexp(const scalarFunction& Tfunc);

#ifndef SWIG
  virtual ~SwiftJ2IsotropicHardening();
  SwiftJ2IsotropicHardening(const SwiftJ2IsotropicHardening &source);
  virtual SwiftJ2IsotropicHardening& operator=(const J2IsotropicHardening &source);
  virtual int getNum() const{return _num;}
  virtual hardeningname getType() const{return J2IsotropicHardening::swiftJ2IsotropicHardening; };
  virtual void createIPVariable(IPJ2IsotropicHardening* &ipv) const;
  virtual void initLaws(const std::map<int,J2IsotropicHardening*> &maplaw) {}; //nothing now, we will see if we use the mapping
  virtual void hardening(double p0, const IPJ2IsotropicHardening &ipvprev, double p, IPJ2IsotropicHardening &ipv) const;
  virtual J2IsotropicHardening * clone() const;
  virtual void hardening(double p0, const IPJ2IsotropicHardening &ipvprev, double p, double T, IPJ2IsotropicHardening &ipv) const;
#endif
};

class MultipleSwiftJ2IsotropicHardening : public J2IsotropicHardening
{
 // R = yield0 * (1+ h0*p)^n0 if p< p1
 //   = yield1 * (1+h1*(p-p1))^n1 if p> p1
 // yield1 = yield0 * (1+ h0*p1)^n0

 protected :
  double _h0, _h1, _p1;
  double _n0, _n1;

 public:
  // constructor
  MultipleSwiftJ2IsotropicHardening(const int num,  double yield0, double h0, double n0, double p1, double h1, double n1);

#ifndef SWIG
  virtual ~MultipleSwiftJ2IsotropicHardening();
  MultipleSwiftJ2IsotropicHardening(const MultipleSwiftJ2IsotropicHardening &source);
  virtual MultipleSwiftJ2IsotropicHardening& operator=(const J2IsotropicHardening &source);
  virtual int getNum() const{return _num;}
  virtual hardeningname getType() const{return J2IsotropicHardening::multipleSwiftJ2IsotropicHardening; };
  virtual void createIPVariable(IPJ2IsotropicHardening* &ipv) const;
  virtual void initLaws(const std::map<int,J2IsotropicHardening*> &maplaw) {}; //nothing now, we will see if we use the mapping
  virtual void hardening(double p0, const IPJ2IsotropicHardening &ipvprev, double p, IPJ2IsotropicHardening &ipv) const;
  virtual J2IsotropicHardening * clone() const;
  virtual void hardening(double p0, const IPJ2IsotropicHardening &ipvprev, double p, double T, IPJ2IsotropicHardening &ipv) const;
#endif
};

//---------------------------------------------------------------------------end
class LinearExponentialJ2IsotropicHardening : public J2IsotropicHardening
{
 // R = yield0 +h1 * p + h2* (1- exp(-hexp*p))
 protected :
  double _h1, _h2, _hexp;
  scalarFunction* _temFunc_h1;
  scalarFunction* _temFunc_h2;
  scalarFunction* _temFunc_hexp;

 public:
  // constructor
  LinearExponentialJ2IsotropicHardening(const int num,  double yield0, double h1, double h2, double hexp, bool init=true);
  void setTemperatureFunction_h1(const scalarFunction& Tfunc);
  void setTemperatureFunction_h2(const scalarFunction& Tfunc);
  void setTemperatureFunction_hexp(const scalarFunction& Tfunc);

#ifndef SWIG
  virtual ~LinearExponentialJ2IsotropicHardening();
  LinearExponentialJ2IsotropicHardening(const LinearExponentialJ2IsotropicHardening &source);
  virtual LinearExponentialJ2IsotropicHardening& operator=(const J2IsotropicHardening &source);
  virtual int getNum() const{return _num;}
  virtual hardeningname getType() const{return J2IsotropicHardening::linearExponentialJ2IsotropicHardening; };
  virtual void createIPVariable(IPJ2IsotropicHardening* &ipv) const;
  virtual void initLaws(const std::map<int,J2IsotropicHardening*> &maplaw) {}; //nothing now, we will see if we use the mapping
  virtual void hardening(double p0, const IPJ2IsotropicHardening &ipvprev, double p, IPJ2IsotropicHardening &ipv) const;
  virtual J2IsotropicHardening * clone() const;
  virtual void hardening(double p0, const IPJ2IsotropicHardening &ipvprev, double p, double T, IPJ2IsotropicHardening &ipv) const;
#endif
};


class LinearExponentialFollowedBySwiftLawJ2IsotropicHardening : public LinearExponentialJ2IsotropicHardening{
  // R = yield0 +h1 * p + h2* (1- exp(-hexp*p)) if p < p3
  // R = syp1*(1+h3*(p-p3))^n3  p3 < p
  // sy1 = yield0 +h1*p1 + h2* (1- exp(-hexp*p1))
 protected :
  double _h3, _p3, _n3;

 public:
  // constructor
  LinearExponentialFollowedBySwiftLawJ2IsotropicHardening(const int num,  double yield0, double h1, double h2, double hexp, double h3, double p3, double n3);

#ifndef SWIG
  virtual ~LinearExponentialFollowedBySwiftLawJ2IsotropicHardening();
  LinearExponentialFollowedBySwiftLawJ2IsotropicHardening(const LinearExponentialFollowedBySwiftLawJ2IsotropicHardening &source);
  virtual LinearExponentialFollowedBySwiftLawJ2IsotropicHardening& operator=(const J2IsotropicHardening &source);
  virtual int getNum() const{return _num;}
  virtual hardeningname getType() const{return J2IsotropicHardening::linearExponentialFollowedBySwiftLawJ2IsotropicHardening; };
  virtual void createIPVariable(IPJ2IsotropicHardening* &ipv) const;
  virtual void initLaws(const std::map<int,J2IsotropicHardening*> &maplaw) {}; //nothing now, we will see if we use the mapping
  virtual void hardening(double p0, const IPJ2IsotropicHardening &ipvprev, double p, IPJ2IsotropicHardening &ipv) const;
  virtual J2IsotropicHardening * clone() const;
  virtual void hardening(double p0, const IPJ2IsotropicHardening &ipvprev, double p, double T, IPJ2IsotropicHardening &ipv) const;
#endif

};


class LinearFollowedByExponentialJ2IsotropicHardening : public J2IsotropicHardening
{
 // R = yield0 + h1 * p
 // Then when p > p_exp
 // R = yield0 + h1 * p + h2* (1- exp(-(p-pexp)/hexp2))
 // when p> _p3
 // R = sy3*(p/p3)^3

 protected :
  double _h1, _pexp, _h2, _hexp2, _p3,_n3;
  scalarFunction* _temFunc_pexp;
  scalarFunction* _temFunc_hexp2;
  scalarFunction* _temFunc_h1;
  scalarFunction* _temFunc_h2;

 public:
  // constructor
  LinearFollowedByExponentialJ2IsotropicHardening(const int num,  double yield0, double h1, double pexp, double h2, double hexp2);
  LinearFollowedByExponentialJ2IsotropicHardening(const int num,  double yield0, double h1, double pexp, double h2, double hexp2, double p3, double n3);
  void setTemperatureFunction_h1(const scalarFunction& Tfunc);
  void setTemperatureFunction_h2(const scalarFunction& Tfunc);
  void setTemperatureFunction_pexp(const scalarFunction& Tfunc);
  void setTemperatureFunction_hexp2(const scalarFunction& Tfunc);

#ifndef SWIG
  virtual ~LinearFollowedByExponentialJ2IsotropicHardening();
  LinearFollowedByExponentialJ2IsotropicHardening(const LinearFollowedByExponentialJ2IsotropicHardening &source);
  virtual LinearFollowedByExponentialJ2IsotropicHardening& operator=(const J2IsotropicHardening &source);
  virtual int getNum() const{return _num;}
  virtual hardeningname getType() const{return J2IsotropicHardening::linearFollowedByExponentialJ2IsotropicHardening; };
  virtual void createIPVariable(IPJ2IsotropicHardening* &ipv) const;
  virtual void initLaws(const std::map<int,J2IsotropicHardening*> &maplaw) {}; //nothing now, we will see if we use the mapping
  virtual void hardening(double p0, const IPJ2IsotropicHardening &ipvprev, double p, IPJ2IsotropicHardening &ipv) const;
  virtual J2IsotropicHardening * clone() const;
  virtual void hardening(double p0, const IPJ2IsotropicHardening &ipvprev, double p, double T, IPJ2IsotropicHardening &ipv) const;
#endif
};


class LinearFollowedByPowerLawJ2IsotropicHardening : public J2IsotropicHardening
{
 // R = yield0 + h1 * p
 // Then when p > pexp
 // R = yield0 * (1 + h1 * pexp) * (p/pexp) ^ h_exp
 protected :
  double _h1, _pexp, _hexp;
  scalarFunction* _temFunc_pexp;
  scalarFunction* _temFunc_hexp;
  scalarFunction* _temFunc_h1;

 public:
  // constructor
  LinearFollowedByPowerLawJ2IsotropicHardening(const int num,  double yield0, double h, double pexp, double hexp, bool init=true);
  void setTemperatureFunction_h1(const scalarFunction& Tfunc);
  void setTemperatureFunction_pexp(const scalarFunction& Tfunc);
  void setTemperatureFunction_hexp(const scalarFunction& Tfunc);

#ifndef SWIG
  virtual ~LinearFollowedByPowerLawJ2IsotropicHardening();
  LinearFollowedByPowerLawJ2IsotropicHardening(const LinearFollowedByPowerLawJ2IsotropicHardening &source);
  virtual LinearFollowedByPowerLawJ2IsotropicHardening& operator=(const J2IsotropicHardening &source);
  virtual int getNum() const{return _num;}
  virtual hardeningname getType() const{return J2IsotropicHardening::linearFollowedByPowerLawJ2IsotropicHardening; };
  virtual void createIPVariable(IPJ2IsotropicHardening* &ipv) const;
  virtual void initLaws(const std::map<int,J2IsotropicHardening*> &maplaw) {}; //nothing now, we will see if we use the mapping
  virtual void hardening(double p0, const IPJ2IsotropicHardening &ipvprev, double p, IPJ2IsotropicHardening &ipv) const;
  virtual J2IsotropicHardening * clone() const;
  virtual void hardening(double p0, const IPJ2IsotropicHardening &ipvprev, double p, double T, IPJ2IsotropicHardening &ipv) const;
#endif
};

class LinearFollowedByMultiplePowerLawJ2IsotropicHardening: public J2IsotropicHardening{
	// R = yield0 + H*p if p < p1
	// R = syp1*(p/p1)^n1  p1 < p < p2
	// R = syp2*(p/p2)^n2 p2 < p
	protected:
		double _H;
		double _p1, _p2;
    double _n1, _n2;

	public:
		LinearFollowedByMultiplePowerLawJ2IsotropicHardening(const int num,  double yield0, double h, double p1, double n1, double p2, double n2);
		#ifndef SWIG
    virtual ~LinearFollowedByMultiplePowerLawJ2IsotropicHardening();
    LinearFollowedByMultiplePowerLawJ2IsotropicHardening(const LinearFollowedByMultiplePowerLawJ2IsotropicHardening &source);
    LinearFollowedByMultiplePowerLawJ2IsotropicHardening& operator=(const J2IsotropicHardening &source);
    virtual int getNum() const{return _num;}
    virtual hardeningname getType() const{return J2IsotropicHardening::linearFollowedByPowerLawJ2IsotropicHardening; };
    virtual void createIPVariable(IPJ2IsotropicHardening* &ipv) const;
    virtual void initLaws(const std::map<int,J2IsotropicHardening*> &maplaw) {}; //nothing now, we will see if we use the mapping
    virtual void hardening(double p0, const IPJ2IsotropicHardening &ipvprev, double p, IPJ2IsotropicHardening &ipv) const;
    virtual J2IsotropicHardening * clone() const;
    virtual void hardening(double p0, const IPJ2IsotropicHardening &ipvprev, double p, double T, IPJ2IsotropicHardening &ipv) const;
		#endif //SWIG
};

class PolynomialJ2IsotropicHardening : public J2IsotropicHardening{
  #ifndef SWIG
  protected:
    int _order; // order of polymonial
    fullVector<double> _coefficients; // coeff of polymonial _coefficients(0)+_coefficients(1)*eps+...
  #endif // SWIG

  public:
  PolynomialJ2IsotropicHardening(const int num, double yield0, int order,  bool init = true);
  void setCoefficients(const int i, const double val);
  #ifndef SWIG
  virtual ~PolynomialJ2IsotropicHardening(){}
  PolynomialJ2IsotropicHardening(const PolynomialJ2IsotropicHardening& src);
  PolynomialJ2IsotropicHardening& operator =(const J2IsotropicHardening& src);
  virtual int getNum() const{return _num;}
  virtual hardeningname getType() const{return J2IsotropicHardening::polynomialJ2IsotropicHardening; };
  virtual void createIPVariable(IPJ2IsotropicHardening* &ipv) const;
  virtual void initLaws(const std::map<int,J2IsotropicHardening*> &maplaw) {}; //nothing now, we will see if we use the mapping
  virtual void hardening(double p0, const IPJ2IsotropicHardening &ipvprev, double p, IPJ2IsotropicHardening &ipv) const;
  virtual J2IsotropicHardening * clone() const;
  virtual void hardening(double p0, const IPJ2IsotropicHardening &ipvprev, double p, double T, IPJ2IsotropicHardening &ipv) const;
  #endif // SWIG
};

class TwoExpJ2IsotropicHaderning : public J2IsotropicHardening{
  // R = K(exp(p)-exp(-2*p))
  #ifndef SWIG
  protected:
    double _K;
    scalarFunction* _temFunc_K;

  #endif // SWIG
  public:
    TwoExpJ2IsotropicHaderning(const int num,  double yield0, double K, bool init=true);
    void setTemperatureFunction_K(const scalarFunction& Tfunc);
  #ifndef SWIG
    virtual ~TwoExpJ2IsotropicHaderning();
    TwoExpJ2IsotropicHaderning(const TwoExpJ2IsotropicHaderning& src);
    virtual TwoExpJ2IsotropicHaderning& operator =(const J2IsotropicHardening& src);
    virtual int getNum() const{return _num;}
    virtual hardeningname getType() const{return J2IsotropicHardening::twoExpJ2IsotropicHaderning; };
    virtual void createIPVariable(IPJ2IsotropicHardening* &ipv) const;
    virtual void initLaws(const std::map<int,J2IsotropicHardening*> &maplaw) {}; //nothing now, we will see if we use the mapping
    virtual void hardening(double p0, const IPJ2IsotropicHardening &ipvprev, double p, IPJ2IsotropicHardening &ipv) const;
    virtual J2IsotropicHardening * clone() const;
    virtual void hardening(double p0, const IPJ2IsotropicHardening &ipvprev, double p, double T, IPJ2IsotropicHardening &ipv) const;
  #endif // SWIG
};

class TanhJ2IsotropicHardening : public J2IsotropicHardening{
  // R = K*tang(p/p0);
  protected:
    double _K;
    double _p0;
    scalarFunction* _temFunc_K;
    scalarFunction* _temFunc_p0;

  public:
    TanhJ2IsotropicHardening(const int num, double y0, double K, double p0, bool init = true);
    void setTemperatureFunction_K(const scalarFunction& Tfunc);
    void setTemperatureFunction_p0(const scalarFunction& Tfunc);
    #ifndef SWIG
    virtual ~TanhJ2IsotropicHardening();
    TanhJ2IsotropicHardening(const TanhJ2IsotropicHardening& src);
    virtual TanhJ2IsotropicHardening& operator =(const J2IsotropicHardening& src);
    virtual int getNum() const{return _num;}
    virtual hardeningname getType() const{return J2IsotropicHardening::tanhJ2IsotropicHaderning; };
    virtual void createIPVariable(IPJ2IsotropicHardening* &ipv) const;
    virtual void initLaws(const std::map<int,J2IsotropicHardening*> &maplaw) {}; //nothing now, we will see if we use the mapping
    virtual void hardening(double p0, const IPJ2IsotropicHardening &ipvprev, double p, IPJ2IsotropicHardening &ipv) const;
    virtual J2IsotropicHardening * clone() const;
    virtual void hardening(double p0, const IPJ2IsotropicHardening &ipvprev, double p, double T, IPJ2IsotropicHardening &ipv) const;
  #endif // SWIG
};

class TanhSeriesJ2IsotropicHardening : public J2IsotropicHardening{
  // R = sum_k K_k*tang(p/pk);
  protected:
    int _Ksize;
    std::vector<double> _K;
    std::vector<double> _p0;

  public:
    TanhSeriesJ2IsotropicHardening(const int num, double y0, int Ksize);
    void set(int i, double K, double p0);
    #ifndef SWIG
    virtual ~TanhSeriesJ2IsotropicHardening();
    TanhSeriesJ2IsotropicHardening(const TanhSeriesJ2IsotropicHardening& src);
    virtual TanhSeriesJ2IsotropicHardening& operator =(const J2IsotropicHardening& src);
    virtual int getNum() const{return _num;}
    virtual hardeningname getType() const{return J2IsotropicHardening::tanhJ2IsotropicHaderning; };
    virtual void createIPVariable(IPJ2IsotropicHardening* &ipv) const;
    virtual void initLaws(const std::map<int,J2IsotropicHardening*> &maplaw) {}; //nothing now, we will see if we use the mapping
    virtual void hardening(double p0, const IPJ2IsotropicHardening &ipvprev, double p, IPJ2IsotropicHardening &ipv) const;
    virtual J2IsotropicHardening * clone() const;
    virtual void hardening(double p0, const IPJ2IsotropicHardening &ipvprev, double p, double T, IPJ2IsotropicHardening &ipv) const;
  #endif // SWIG
};

class LinearFollowedByExponentialFollowedByMultiplePowerLawJ2IsotropicHardening: public J2IsotropicHardening{
	// R = yield0 + H*p if p < p1
	// R = syp1*(p/p1)^n1  p1 < p < p2
	// R = syp2*(p/p2)^n2 p2 < p 
	protected:
	  double _h1, _pexp, _h2, _hexp2, _ppo, _npo;
                scalarFunction* _temFunc_pexp;
  		scalarFunction* _temFunc_hexp2;
  		scalarFunction* _temFunc_h1;
  		scalarFunction* _temFunc_h2;
  		scalarFunction* _temFunc_ppo;
  		scalarFunction* _temFunc_npo;

    
	public:
		LinearFollowedByExponentialFollowedByMultiplePowerLawJ2IsotropicHardening(const int num,  double yield0, double h1, double pexp, double h2, double hexp2, double ppo, double npo, bool init=true);
      void setTemperatureFunction_h1(const scalarFunction& Tfunc);
      void setTemperatureFunction_h2(const scalarFunction& Tfunc);
      void setTemperatureFunction_pexp(const scalarFunction& Tfunc);
      void setTemperatureFunction_hexp2(const scalarFunction& Tfunc);
      void setTemperatureFunction_ppo(const scalarFunction& Tfunc);
      void setTemperatureFunction_npo(const scalarFunction& Tfunc);
		#ifndef SWIG
    virtual ~LinearFollowedByExponentialFollowedByMultiplePowerLawJ2IsotropicHardening();
    LinearFollowedByExponentialFollowedByMultiplePowerLawJ2IsotropicHardening(const LinearFollowedByExponentialFollowedByMultiplePowerLawJ2IsotropicHardening &source);
    LinearFollowedByExponentialFollowedByMultiplePowerLawJ2IsotropicHardening& operator=(const J2IsotropicHardening &source);
    virtual int getNum() const{return _num;}
    virtual hardeningname getType() const{return J2IsotropicHardening::LinearFollowedByExponentialFollowedByMultiplePowerLawJ2IsotropicHardening; };
    virtual void createIPVariable(IPJ2IsotropicHardening* &ipv) const;
    virtual void initLaws(const std::map<int,J2IsotropicHardening*> &maplaw) {}; //nothing now, we will see if we use the mapping
    virtual void hardening(double p0, const IPJ2IsotropicHardening &ipvprev, double p, IPJ2IsotropicHardening &ipv) const;
    virtual J2IsotropicHardening * clone() const;
    virtual void hardening(double p0, const IPJ2IsotropicHardening &ipvprev, double p, double T, IPJ2IsotropicHardening &ipv) const;
		#endif //SWIG
};

class LinearFollowedByMultipleSwiftJ2IsotropicHardening: public J2IsotropicHardening{
	// R = yield0 + h0*p if p < p1
	// R = syp1*(1+h1*(p-p1))^n1 if p1 < p < p2
	// R = syp2*(1+h2*(p-p2))^n2 p2 < p
	protected:
	  double _h0, _h1, _h2, _p1, _p2, _n1, _n2;
        scalarFunction* _temFunc_h0;
  		scalarFunction* _temFunc_h1;
  		scalarFunction* _temFunc_h2;
  		scalarFunction* _temFunc_n1;
  		scalarFunction* _temFunc_n2;


	public:
  	  LinearFollowedByMultipleSwiftJ2IsotropicHardening(const int num,  double yield0, double h0, double p1, double h1, double n1, double p2, double h2, double n2, bool init=true);
  	  void setTemperatureFunction_h0(const scalarFunction& Tfunc);
      void setTemperatureFunction_h1(const scalarFunction& Tfunc);
      void setTemperatureFunction_h2(const scalarFunction& Tfunc);
      void setTemperatureFunction_n1(const scalarFunction& Tfunc);
      void setTemperatureFunction_n2(const scalarFunction& Tfunc);
		#ifndef SWIG
    virtual ~LinearFollowedByMultipleSwiftJ2IsotropicHardening();
    LinearFollowedByMultipleSwiftJ2IsotropicHardening(const LinearFollowedByMultipleSwiftJ2IsotropicHardening &source);
    LinearFollowedByMultipleSwiftJ2IsotropicHardening& operator=(const J2IsotropicHardening &source);
    virtual int getNum() const{return _num;}
    virtual hardeningname getType() const{return J2IsotropicHardening::LinearFollowedByMultipleSwiftJ2IsotropicHardening; };
    virtual void createIPVariable(IPJ2IsotropicHardening* &ipv) const;
    virtual void initLaws(const std::map<int,J2IsotropicHardening*> &maplaw) {}; //nothing now, we will see if we use the mapping
    virtual void hardening(double p0, const IPJ2IsotropicHardening &ipvprev, double p, IPJ2IsotropicHardening &ipv) const;
    virtual J2IsotropicHardening * clone() const;
    virtual void hardening(double p0, const IPJ2IsotropicHardening &ipvprev, double p, double T, IPJ2IsotropicHardening &ipv) const;
		#endif //SWIG
};
#endif //J2ISOTROPICHARDENING_H_
