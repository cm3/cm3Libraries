//
// Description: Define mullins effect for softening in polymers in (pseudo-)hyperelastic regime 
//
//
// Author:  <Ujwal Kishore J.>, (C) 2023
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "mullinsEffect.h"

mullinsEffect::mullinsEffect(const int num,const bool init): _num(num), _initialized(init){}

mullinsEffect::mullinsEffect(const mullinsEffect &source)
{
  _num = source._num;
  _initialized = source._initialized;
}

mullinsEffect& mullinsEffect::operator=(const mullinsEffect &source)
{
  _num = source._num;
  _initialized = source._initialized;
  return *this;
}


negativeExponentialScaler::negativeExponentialScaler(const int num, double r, double m, bool init):
  mullinsEffect(num,init),_r(r),_m(m){
      
    _temFunc_r= new constantScalarFunction(1.);
    _temFunc_m= new constantScalarFunction(1.);
};

negativeExponentialScaler::negativeExponentialScaler(const negativeExponentialScaler& src):
  mullinsEffect(src), _r(src._r), _m(src._m){
      
    _temFunc_r = NULL;
    if (src._temFunc_r != NULL){
        _temFunc_r = src._temFunc_r->clone();
    }
    
    _temFunc_m = NULL;
    if (src._temFunc_m != NULL){
        _temFunc_m = src._temFunc_m->clone();
    }
};

negativeExponentialScaler& negativeExponentialScaler::operator =(const mullinsEffect& src){
  mullinsEffect::operator=(src);
  const negativeExponentialScaler* psrc = dynamic_cast<const negativeExponentialScaler*>(&src);
  if(psrc != NULL)
  {
    _r = psrc->_r;
    _m = psrc->_m;
    
    if (psrc->_temFunc_r != NULL){
      if (_temFunc_r != NULL){
        _temFunc_r->operator=(*psrc->_temFunc_r);
      }
      else{
        _temFunc_r = psrc->_temFunc_r->clone();
      }
    }
    
    if (psrc->_temFunc_m != NULL){
      if (_temFunc_m != NULL){
        _temFunc_m->operator=(*psrc->_temFunc_m);
      }
      else{
        _temFunc_m = psrc->_temFunc_m->clone();
      }
    }
  }
  return *this;
};

negativeExponentialScaler::~negativeExponentialScaler(){
  if (_temFunc_r != NULL) {delete _temFunc_r; _temFunc_r = NULL;};
  if (_temFunc_m != NULL) {delete _temFunc_m; _temFunc_m = NULL;};
};

void negativeExponentialScaler::setTemperatureFunction_r(const scalarFunction& Tfunc){
  if (_temFunc_r != NULL) delete _temFunc_r;
  _temFunc_r = Tfunc.clone();
}

void negativeExponentialScaler::setTemperatureFunction_m(const scalarFunction& Tfunc){
  if (_temFunc_m != NULL) delete _temFunc_m;
  _temFunc_m = Tfunc.clone();
}

void negativeExponentialScaler::createIPVariable(IPMullinsEffect* &ipv) const
{
  if(ipv != NULL) delete ipv;
  ipv = new IPMullinsEffect();
}

void negativeExponentialScaler::mullinsEffectScaling(double _psi0, double _psi, const IPMullinsEffect &ipvprev, IPMullinsEffect &ipv) const{
    
  // at unloading intitiation-> eta = 1.; unloading end-> eta = eta_min; reloading -> eta = eta_min;  
    
	  double r = _r;
	  double m = _m;

	  double eta0 = ipvprev.getEta();
	  double _psi_max0 = ipvprev.getpsiMax();
	  double psi_cap = _psi;
	  double eta = 1., DetaDpsi_cap = 0., DDetaDpsipsi = 0., DetaDT = 0., DDetaDTT = 0.;
	  double DpsiNew_DpsiMax(0.), DDpsiNew_DpsiMaxDpsi(0.), DDpsiNew_DpsiMaxDT(0.);
	  double Dpsi_max_Dpsi_cap(0.), Dpsi_max_DT(0.);

	  double _psi_max = std::max(_psi,_psi_max0);
	  // Msg::Error(" Inside mullinsEffectScaling, _psi = %e, _psi_max = %e !!", _psi, _psi_max);

	  if (_psi_max>0.){

		// eta
		double temp = m*(_psi_max-psi_cap);
		eta = (1-r*sqrt(tanh(temp)));

	    // derivatives
	    if (psi_cap >= _psi_max){
	        Dpsi_max_Dpsi_cap = 1.;
	    }

	    DetaDpsi_cap = m*r/2.*pow( 1/cosh(temp) ,2)/sqrt(tanh(temp)) * (1- Dpsi_max_Dpsi_cap);

	    double DetaDpsi_max = -m*r/2.*pow( 1/cosh(temp) ,2)/sqrt(tanh(temp));
	    DpsiNew_DpsiMax = r*( sqrt(tanh(temp)) - sqrt(tanh(m*_psi_max)) ); // definition
	    DDpsiNew_DpsiMaxDpsi = DetaDpsi_max + r*m/2.*(pow( 1/cosh(temp) ,2)/sqrt(tanh(temp)) - pow( 1/cosh(m*_psi_max) ,2)/sqrt(tanh(m*_psi_max)) ) * Dpsi_max_Dpsi_cap;
	  }

	  ipv.set(eta,DetaDpsi_cap,DDetaDpsipsi,DetaDT,DDetaDTT,DpsiNew_DpsiMax, DDpsiNew_DpsiMaxDpsi, DDpsiNew_DpsiMaxDT);
	  ipv.setPsiMax(_psi_max);
	  ipv.setDpsiMax_Dpsi(Dpsi_max_Dpsi_cap);
}

void negativeExponentialScaler::mullinsEffectScaling(double _psi0, double _psi, const IPMullinsEffect &ipvprev, IPMullinsEffect &ipv, const double T, const double& dPsi_capDT) const{
   
  double r = _r*_temFunc_r->getVal(T);
  double m = _m*_temFunc_m->getVal(T);
  double drdT = _r*_temFunc_r->getDiff(T);
  double dmdT = _m*_temFunc_m->getDiff(T);
  
  double eta0 = ipvprev.getEta();
  double _psi_max0 = ipvprev.getpsiMax();
  double psi_cap = _psi;
  double eta = 1., DetaDpsi_cap = 0., DDetaDpsipsi = 0., DetaDT = 0., DDetaDTT = 0.;
  double DpsiNew_DpsiMax(0.), DDpsiNew_DpsiMaxDpsi(0.), DDpsiNew_DpsiMaxDT(0.);
  double Dpsi_max_Dpsi_cap(0.), Dpsi_max_DT(0.);
  
  double _psi_max = std::max(_psi,_psi_max0);
  // Msg::Error(" Inside mullinsEffectScaling, _psi = %e, _psi_max = %e !!", _psi, _psi_max);
  
  if (_psi_max>0.){

	// eta
	double temp = m*(_psi_max-psi_cap);
	eta = (1-r*sqrt(tanh(temp)));

    // derivatives
    if (psi_cap >= _psi_max){
        Dpsi_max_Dpsi_cap = 1.;
        Dpsi_max_DT = dPsi_capDT;
    }
    
    DetaDpsi_cap = m*r/2.*pow( 1/cosh(temp) ,2)/sqrt(tanh(temp)) * (1- Dpsi_max_Dpsi_cap);
    DetaDT = -drdT*(sqrt(tanh(temp))) - dmdT*r/2.*(_psi_max-psi_cap)*pow( 1/cosh(temp) ,2)/sqrt(tanh(temp))
    		+ m*r/2.*pow( 1/cosh(temp) ,2)/sqrt(tanh(temp)) * (1- Dpsi_max_Dpsi_cap)*dPsi_capDT
			- m*r/2.*pow( 1/cosh(temp) ,2)/sqrt(tanh(temp)) * Dpsi_max_DT;
    
    double DetaDpsi_max = -m*r/2.*pow( 1/cosh(temp) ,2)/sqrt(tanh(temp));
    DpsiNew_DpsiMax = r*( sqrt(tanh(temp)) - sqrt(tanh(m*_psi_max)) ); // definition
    DDpsiNew_DpsiMaxDpsi = DetaDpsi_max + r*m/2.*(pow( 1/cosh(temp) ,2)/sqrt(tanh(temp)) - pow( 1/cosh(m*_psi_max) ,2)/sqrt(tanh(m*_psi_max)) ) * Dpsi_max_Dpsi_cap;
    DDpsiNew_DpsiMaxDT = DDpsiNew_DpsiMaxDpsi*dPsi_capDT - drdT*( sqrt(tanh(temp)) - sqrt(tanh(m*_psi_max)) )
    						+ r*dmdT/2.*( (_psi_max-psi_cap)*pow( 1/cosh(temp) ,2)/sqrt(tanh(temp)) - (_psi_max)*pow( 1/cosh(m*_psi_max) ,2)/sqrt(tanh(m*_psi_max)) )
							+ r*m/2.*(pow( 1/cosh(temp) ,2)/sqrt(tanh(temp)) - pow( 1/cosh(m*_psi_max) ,2)/sqrt(tanh(m*_psi_max)) ) * Dpsi_max_DT;
  }
   
  ipv.set(eta,DetaDpsi_cap,DDetaDpsipsi,DetaDT,DDetaDTT,DpsiNew_DpsiMax, DDpsiNew_DpsiMaxDpsi, DDpsiNew_DpsiMaxDT);
  ipv.setPsiMax(_psi_max);
  ipv.setDpsiMax_Dpsi(Dpsi_max_Dpsi_cap);
}

mullinsEffect *negativeExponentialScaler::clone() const
{
  return new negativeExponentialScaler(*this);
}

// #############

hyperbolicScaler::hyperbolicScaler(const int num, double r, bool init):
  mullinsEffect(num,init),_r(r){
      
    _temFunc_r= new constantScalarFunction(1.);
};

hyperbolicScaler::hyperbolicScaler(const hyperbolicScaler& src):
  mullinsEffect(src), _r(src._r){
      
    _temFunc_r = NULL;
    if (src._temFunc_r != NULL){
        _temFunc_r = src._temFunc_r->clone();
    }
};

hyperbolicScaler& hyperbolicScaler::operator =(const mullinsEffect& src){
  mullinsEffect::operator=(src);
  const hyperbolicScaler* psrc = dynamic_cast<const hyperbolicScaler*>(&src);
  if(psrc != NULL)
  {
    _r = psrc->_r;
    
    if (psrc->_temFunc_r != NULL){
      if (_temFunc_r != NULL){
        _temFunc_r->operator=(*psrc->_temFunc_r);
      }
      else{
        _temFunc_r = psrc->_temFunc_r->clone();
      }
    }
  }
  return *this;
};

hyperbolicScaler::~hyperbolicScaler(){
  if (_temFunc_r != NULL) {delete _temFunc_r; _temFunc_r = NULL;};
};

void hyperbolicScaler::setTemperatureFunction_r(const scalarFunction& Tfunc){
  if (_temFunc_r != NULL) delete _temFunc_r;
  _temFunc_r = Tfunc.clone();
}

void hyperbolicScaler::createIPVariable(IPMullinsEffect* &ipv) const
{
  if(ipv != NULL) delete ipv;
  ipv = new IPMullinsEffect();
}

void hyperbolicScaler::mullinsEffectScaling(double _psi0, double _psi, const IPMullinsEffect &ipvprev, IPMullinsEffect &ipv) const{
    
  // at unloading intitiation-> eta = 1.; unloading end-> eta = eta_min; reloading -> eta = eta_min;  
    
  double eta0 = ipvprev.getEta();
  double _psi_max0 = ipvprev.getpsiMax();
  double psi_cap = _psi;
  double eta = 1., DetaDpsi_cap = 0., DDetaDpsipsi = 0., DetaDT = 0., DDetaDTT = 0.;
  double DpsiNew_DpsiMax(0.), DDpsiNew_DpsiMaxDpsi(0.), DDpsiNew_DpsiMaxDT(0.);
  double Dpsi_max_Dpsi_cap(0.);
  
  double _psi_max = std::max(_psi,_psi_max0);
  // Msg::Error(" Inside mullinsEffectScaling, _psi = %e, _psi_max = %e !!", _psi, _psi_max);

  if (_psi_max>0.){
    // eta
    eta = (1-_r)/(1-_r*psi_cap/_psi_max);

    // derivatives
    if (psi_cap >= _psi_max) Dpsi_max_Dpsi_cap = 1.;
    DetaDpsi_cap = -pow(eta,2)/(1-_r) * (-_r/_psi_max + _r*psi_cap/pow(_psi_max,2)*Dpsi_max_Dpsi_cap);
  }
  
  ipv.set(eta,DetaDpsi_cap,DDetaDpsipsi,DetaDT,DDetaDTT,DpsiNew_DpsiMax, DDpsiNew_DpsiMaxDpsi, DDpsiNew_DpsiMaxDT);
  ipv.setPsiMax(_psi_max);
  ipv.setDpsiMax_Dpsi(Dpsi_max_Dpsi_cap);
}

void hyperbolicScaler::mullinsEffectScaling(double _psi0, double _psi, const IPMullinsEffect &ipvprev, IPMullinsEffect &ipv, const double T, const double& dPsi_capDT) const{
   
  double r = _r*_temFunc_r->getVal(T);
  double drdT = _r*_temFunc_r->getDiff(T);
  
  double eta0 = ipvprev.getEta();
  double _psi_max0 = ipvprev.getpsiMax();
  double psi_cap = _psi;
  double eta = 1., DetaDpsi_cap = 0., DDetaDpsipsi = 0., DetaDT = 0., DDetaDTT = 0.;
  double DpsiNew_DpsiMax(0.), DDpsiNew_DpsiMaxDpsi(0.), DDpsiNew_DpsiMaxDT(0.);
  double Dpsi_max_Dpsi_cap(0.), Dpsi_max_DT(0.);
  
  double _psi_max = std::max(_psi,_psi_max0);
  // Msg::Error(" Inside mullinsEffectScaling, _psi = %e, _psi_max = %e !!", _psi, _psi_max);
  
  if (_psi_max>0.){
    // eta
    eta = (1-r)/(1-r*psi_cap/_psi_max);
   
    // derivatives 
    if (psi_cap >= _psi_max){
        Dpsi_max_Dpsi_cap = 1.;
        Dpsi_max_DT = dPsi_capDT;
    }
    DetaDpsi_cap = -pow(eta,2)/(1-r) * (-r/_psi_max + r*psi_cap/pow(_psi_max,2)*Dpsi_max_Dpsi_cap); 
    DetaDT = -drdT*eta/(1-r) - pow(eta,2)/(1-r)*(-drdT*psi_cap/_psi_max - r/_psi_max*dPsi_capDT + r*psi_cap/pow(_psi_max,2)*Dpsi_max_DT);
  }
  
  ipv.set(eta,DetaDpsi_cap,DDetaDpsipsi,DetaDT,DDetaDTT,DpsiNew_DpsiMax, DDpsiNew_DpsiMaxDpsi, DDpsiNew_DpsiMaxDT);
  ipv.setPsiMax(_psi_max);
  ipv.setDpsiMax_Dpsi(Dpsi_max_Dpsi_cap);
}

mullinsEffect *hyperbolicScaler::clone() const
{
  return new hyperbolicScaler(*this);
}

// ######

linearScaler::linearScaler(const int num, double r, bool init):
  mullinsEffect(num,init),_r(r){
      
    _temFunc_r= new constantScalarFunction(1.);
};

linearScaler::linearScaler(const linearScaler& src):
  mullinsEffect(src), _r(src._r){
      
    _temFunc_r = NULL;
    if (src._temFunc_r != NULL){
        _temFunc_r = src._temFunc_r->clone();
    }
};

linearScaler& linearScaler::operator =(const mullinsEffect& src){
  mullinsEffect::operator=(src);
  const linearScaler* psrc = dynamic_cast<const linearScaler*>(&src);
  if(psrc != NULL)
  {
    _r = psrc->_r;
    
    if (psrc->_temFunc_r != NULL){
      if (_temFunc_r != NULL){
        _temFunc_r->operator=(*psrc->_temFunc_r);
      }
      else{
        _temFunc_r = psrc->_temFunc_r->clone();
      }
    }
  }
  return *this;
};

linearScaler::~linearScaler(){
  if (_temFunc_r != NULL) {delete _temFunc_r; _temFunc_r = NULL;};
};

void linearScaler::setTemperatureFunction_r(const scalarFunction& Tfunc){
  if (_temFunc_r != NULL) delete _temFunc_r;
  _temFunc_r = Tfunc.clone();
}

void linearScaler::createIPVariable(IPMullinsEffect* &ipv) const
{
  if(ipv != NULL) delete ipv;
  ipv = new IPMullinsEffect();
}

void linearScaler::mullinsEffectScaling(double _psi0, double _psi, const IPMullinsEffect &ipvprev, IPMullinsEffect &ipv) const{
    
  // at unloading intitiation-> eta = 1.; unloading end-> eta = eta_min; reloading -> eta = eta_min;  
    
  double eta0 = ipvprev.getEta();
  double _psi_max0 = ipvprev.getpsiMax();
  double psi_cap = _psi;
  double eta = 1., DetaDpsi_cap = 0., DDetaDpsipsi = 0., DetaDT = 0., DDetaDTT = 0.;
  double DpsiNew_DpsiMax(0.), DDpsiNew_DpsiMaxDpsi(0.), DDpsiNew_DpsiMaxDT(0.);
  double Dpsi_max_Dpsi_cap(0.);
  
  double _psi_max = std::max(_psi,_psi_max0);
  // Msg::Error(" Inside mullinsEffectScaling, _psi = %e, _psi_max = %e !!", _psi, _psi_max);

  if (_psi_max>0.){
    // eta
    eta = (1-_r*(1-psi_cap/_psi_max));

    // derivatives
    if (psi_cap >= _psi_max) Dpsi_max_Dpsi_cap = 1.;
    
    DetaDpsi_cap = _r/_psi_max + _r*psi_cap/pow(_psi_max,2) * (-Dpsi_max_Dpsi_cap);
    
    double DetaDpsi_max = -_r*psi_cap/pow(_psi_max,2);
    DpsiNew_DpsiMax = -_r*pow(psi_cap,2)/(2.*pow(_psi_max,2));
    DDpsiNew_DpsiMaxDpsi = DetaDpsi_max;
  }
  
  ipv.set(eta,DetaDpsi_cap,DDetaDpsipsi,DetaDT,DDetaDTT,DpsiNew_DpsiMax, DDpsiNew_DpsiMaxDpsi, DDpsiNew_DpsiMaxDT);
  ipv.setPsiMax(_psi_max);
  ipv.setDpsiMax_Dpsi(Dpsi_max_Dpsi_cap);
}

void linearScaler::mullinsEffectScaling(double _psi0, double _psi, const IPMullinsEffect &ipvprev, IPMullinsEffect &ipv, const double T, const double& dPsi_capDT) const{
   
  double r = _r*_temFunc_r->getVal(T);
  double drdT = _r*_temFunc_r->getDiff(T);
  
  double eta0 = ipvprev.getEta();
  double _psi_max0 = ipvprev.getpsiMax();
  double psi_cap = _psi;
  double eta = 1., DetaDpsi_cap = 0., DDetaDpsipsi = 0., DetaDT = 0., DDetaDTT = 0.;
  double DpsiNew_DpsiMax(0.), DDpsiNew_DpsiMaxDpsi(0.), DDpsiNew_DpsiMaxDT(0.);
  double Dpsi_max_Dpsi_cap(0.), Dpsi_max_DT(0.);
  
  double _psi_max = std::max(_psi,_psi_max0);
  // Msg::Error(" Inside mullinsEffectScaling, _psi = %e, _psi_max = %e !!", _psi, _psi_max);
  
  if (_psi_max>0.){
    // eta
    eta = (1-r*(1-psi_cap/_psi_max));

    // derivatives 
    if (psi_cap >= _psi_max){
        Dpsi_max_Dpsi_cap = 1.;
        Dpsi_max_DT = dPsi_capDT;
    }
    
    DetaDpsi_cap = r/_psi_max + r*psi_cap/pow(_psi_max,2) * (-Dpsi_max_Dpsi_cap);
    DetaDT = -drdT*(1.-psi_cap/_psi_max) + r*(dPsi_capDT*1/_psi_max - psi_cap/pow(_psi_max,2)*Dpsi_max_DT);

    // DetaDT = -drdT*(1.-psi_cap/_psi_max) + r*(- psi_cap/pow(_psi_max,2)*Dpsi_max_Dpsi_cap*dPsi_capDT); // NEW - In paper
    
    double DetaDpsi_max = -r*psi_cap/pow(_psi_max,2);
    DpsiNew_DpsiMax = -r*pow(psi_cap,2)/(2.*pow(_psi_max,2)); // definition
    DDpsiNew_DpsiMaxDpsi = DetaDpsi_max + r*pow(psi_cap,2)/pow(_psi_max,3)*Dpsi_max_Dpsi_cap;
    DDpsiNew_DpsiMaxDT = DDpsiNew_DpsiMaxDpsi*dPsi_capDT - drdT*(pow(psi_cap,2)/(2.*pow(_psi_max,2)));
  }
  
  ipv.set(eta,DetaDpsi_cap,DDetaDpsipsi,DetaDT,DDetaDTT,DpsiNew_DpsiMax, DDpsiNew_DpsiMaxDpsi, DDpsiNew_DpsiMaxDT);
  ipv.setPsiMax(_psi_max);
  ipv.setDpsiMax_Dpsi(Dpsi_max_Dpsi_cap);
}

mullinsEffect *linearScaler::clone() const
{
  return new linearScaler(*this);
}

// ######

positiveExponentialScaler::positiveExponentialScaler(const int num, double r, double m, bool init):
  mullinsEffect(num,init),_r(r),_m(m){
      
    _temFunc_r= new constantScalarFunction(1.);
    _temFunc_m= new constantScalarFunction(1.);
};

positiveExponentialScaler::positiveExponentialScaler(const positiveExponentialScaler& src):
  mullinsEffect(src), _r(src._r), _m(src._m){
      
    _temFunc_r = NULL;
    if (src._temFunc_r != NULL){
        _temFunc_r = src._temFunc_r->clone();
    }
    
    _temFunc_m = NULL;
    if (src._temFunc_m != NULL){
        _temFunc_m = src._temFunc_m->clone();
    }
};

positiveExponentialScaler& positiveExponentialScaler::operator =(const mullinsEffect& src){
  mullinsEffect::operator=(src);
  const positiveExponentialScaler* psrc = dynamic_cast<const positiveExponentialScaler*>(&src);
  if(psrc != NULL)
  {
    _r = psrc->_r;
    _m = psrc->_m;
    
    if (psrc->_temFunc_r != NULL){
      if (_temFunc_r != NULL){
        _temFunc_r->operator=(*psrc->_temFunc_r);
      }
      else{
        _temFunc_r = psrc->_temFunc_r->clone();
      }
    }
    
    if (psrc->_temFunc_m != NULL){
      if (_temFunc_m != NULL){
        _temFunc_m->operator=(*psrc->_temFunc_m);
      }
      else{
        _temFunc_m = psrc->_temFunc_m->clone();
      }
    }
  }
  return *this;
};

positiveExponentialScaler::~positiveExponentialScaler(){
  if (_temFunc_r != NULL) {delete _temFunc_r; _temFunc_r = NULL;};
  if (_temFunc_m != NULL) {delete _temFunc_m; _temFunc_m = NULL;};
};

void positiveExponentialScaler::setTemperatureFunction_r(const scalarFunction& Tfunc){
  if (_temFunc_r != NULL) delete _temFunc_r;
  _temFunc_r = Tfunc.clone();
}

void positiveExponentialScaler::setTemperatureFunction_m(const scalarFunction& Tfunc){
  if (_temFunc_m != NULL) delete _temFunc_m;
  _temFunc_m = Tfunc.clone();
}

void positiveExponentialScaler::createIPVariable(IPMullinsEffect* &ipv) const
{
  if(ipv != NULL) delete ipv;
  ipv = new IPMullinsEffect();
}

void positiveExponentialScaler::mullinsEffectScaling(double _psi0, double _psi, const IPMullinsEffect &ipvprev, IPMullinsEffect &ipv) const{
    
  // at unloading intitiation-> eta = 1.; unloading end-> eta = eta_min; reloading -> eta = eta_min;  
    
  double eta0 = ipvprev.getEta();
  double _psi_max0 = ipvprev.getpsiMax();
  double psi_cap = _psi;
  double eta = 1., DetaDpsi_cap = 0., DDetaDpsipsi = 0., DetaDT = 0., DDetaDTT = 0.;
  double DpsiNew_DpsiMax(0.), DDpsiNew_DpsiMaxDpsi(0.), DDpsiNew_DpsiMaxDT(0.);
  
  double _psi_max = std::max(_psi,_psi_max0);
  // Msg::Error(" Inside mullinsEffectScaling, _psi = %e, _psi_max = %e !!", _psi, _psi_max);
  
  double Dpsi_maxDpsi_cap = 1.; // Dpsi_maxDpsi_cap = 1. if _psi_max = _psi
  double Term = (_m*(1.-eta*psi_cap/_psi_max));  // Term = 0. if _psi_max = _psi
  
  if (Term != 0.){  
    
    int ite = 0;
    int maxite = 100; 
    double _tol = 1e-8;
    double DfDeta(0.), Deta(0.);
    double f = eta - _r*(1-exp(Term)) - 1.;
    
    while (fabs(f) >_tol or ite <1){
      
      DfDeta = 1 - _r*_m*exp(Term)*psi_cap/_psi_max;
      Deta = - f/DfDeta;
      
      if (eta + Deta > 1.){
          eta = 0.99;                // NR
        }
        else
          eta += Deta;
          
      // Msg::Error(" Inside iterator: ite = %d, f = %e, DfDeta = %e, Deta = %e, eta = %e !!", ite, f, DfDeta, Deta, eta);
  
      Term = (_m*(1.-eta*psi_cap/_psi_max));  
      f = eta - _r*(1-exp(Term)) - 1.;
      
      ite++;
      if (fabs(f) <_tol) break;
      
      if(ite > maxite){
          // Msg::Error("No convergence for eta in mullinsEffectScaling: ite= %d, f = %e!!",ite,f);
          eta = sqrt(-1.);
          return;
      }
    }
    
    // eta = 1+_r*(1-exp(_m*(1 - psi_cap/_psi_max)));
    
    Dpsi_maxDpsi_cap = 0.;
    
    // DetaDpsi_cap = ;
  }
  else{
    eta = 1.; 
    DetaDpsi_cap = 0.;
  }

  ipv.set(eta,DetaDpsi_cap,DDetaDpsipsi,DetaDT,DDetaDTT,DpsiNew_DpsiMax, DDpsiNew_DpsiMaxDpsi, DDpsiNew_DpsiMaxDT);
  ipv.setPsiMax(_psi_max);
}

void positiveExponentialScaler::mullinsEffectScaling(double _psi0, double _psi, const IPMullinsEffect &ipvprev, IPMullinsEffect &ipv, const double T, const double& dPsi_capDT) const{
   
  double r = _r*_temFunc_r->getVal(T);
  double m = _m*_temFunc_m->getVal(T);
  
  double eta0 = ipvprev.getEta();
  double _psi_max0 = ipvprev.getpsiMax();
  double psi_cap = _psi;
  double eta = 1., DetaDpsi_cap = 0., DDetaDpsipsi = 0., DetaDT = 0., DDetaDTT = 0.;
  double DpsiNew_DpsiMax(0.), DDpsiNew_DpsiMaxDpsi(0.), DDpsiNew_DpsiMaxDT(0.);
  
  double _psi_max = std::max(_psi,_psi_max0);
  // Msg::Error(" Inside mullinsEffectScaling, _psi = %e, _psi_max = %e !!", _psi, _psi_max);
  
  double Dpsi_maxDpsi_cap = 1.; // Dpsi_maxDpsi_cap = 1. if _psi_max = _psi
  double Term = sqrt(m*(_psi_max-eta*psi_cap));  // Term = 0. if _psi_max = _psi
  
  if (Term!=0){
    
    int ite = 0;
    int maxite = 100; 
    double _tol = 1e-8;
    double DfDeta(0.), Deta(0.);
    double f = eta + r*(1-exp(-Term)) - 1.;

    while (fabs(f) >_tol or ite <1){
      
      DfDeta = 1 + r*(-m*psi_cap*exp(-Term)/(2*Term));
      Deta = - f/DfDeta;
      
      if (eta + Deta > 1.){
            eta = 0.99;                // NR
        }
        else
          eta += Deta;
          
      // Msg::Error(" Inside iterator, ite = %d, f = %e, DfDeta = %e, Deta = %e, eta = %e !!", ite, f, DfDeta, Deta, eta);
  
      Term = sqrt(m*(_psi_max-eta*psi_cap));
      f = eta + r*(1-exp(-Term)) - 1.;
      
      ite++;
      if (fabs(f) <_tol) break;
      
      if(ite > maxite){
          // Msg::Error("No convergence for eta in mullinsEffectScaling: ite= %d, f = %e!!",ite,f);
          eta = sqrt(-1.);
          return;
      }
    }
    // eta = 1+_r*(1-exp(_m*(1 - psi_cap/_psi_max)));
    Dpsi_maxDpsi_cap = 0.;
    
    DetaDpsi_cap = (-r*m*exp(-Term)/(2*Term)) * (Dpsi_maxDpsi_cap - eta) / (1 - r*m*exp(-Term)/(2*Term)*psi_cap);
  }
  else{
    eta = 1.; 
    DetaDpsi_cap = 0.;
  }
  
  ipv.set(eta,DetaDpsi_cap,DDetaDpsipsi,DetaDT,DDetaDTT,DpsiNew_DpsiMax, DDpsiNew_DpsiMaxDpsi, DDpsiNew_DpsiMaxDT);
  ipv.setPsiMax(_psi_max);
}

mullinsEffect *positiveExponentialScaler::clone() const
{
  return new positiveExponentialScaler(*this);
}

// #############
