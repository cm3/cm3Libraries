#ifndef ELASTIC_LCC_H
#define ELASTIC_LCC_H 1

#ifdef NONLOCALGMSH
#include "matrix_operations.h"
namespace MFH {
#endif

typedef struct Lccsimple {

	int Nph;
        double v1;
	double* mu, *kappa;	//elastic bulk and shear moduli of phases in the LCC

	double** C, **C0, **C1, **dCdmu0;
        double **A;     //effective stiffness and derivatives w.r.t. mu

        double *strs_n;
        double *dstrnf;
        double *dstrn_m;
} ELcc;


typedef struct YieldFunc2nd {

        double f, dfdp;		// value of yielding function

	double *dfdstrn_m, *dfdstrn_c;            //  derivatives w.r. incremental strain of matrix and composite
	double *dpdstrn_m, *dpdstrn_c;            
}  FY2nd ;

typedef struct Epresult {
     
        double **Calgo, **Csd;
        double ***dCsd, ***dCsddE;              // dCsd - dCsddstrain_m, dCsddE - dCsddstrain_c
        double **dCsdp_bar;
        double *dnu, *dnudE, *dnudp_bar;
        double *dp, *dpdE; 
        double *dstrsdp_bar;
} EPR;


#ifdef NONLOCALGMSH
}
#endif

#ifdef NONLOCALGMSH
void malloc_lcc(MFH::ELcc* LCC, int N, double vf_i);
void set_elprops(MFH::ELcc* LCC, double* mu, double* kappa);
void free_lcc(MFH::ELcc* LCC);

void malloc_YF(MFH::FY2nd* YF);
void free_YF(MFH::FY2nd* YF);
void CA_dCdA_MT(MFH::ELcc* LCC, double v1, double **S, double **dS, double *dnu);

void malloc_Epresult(MFH::EPR* EpResult);
void free_Epresult(MFH::EPR* EpResult);
#else
void malloc_lcc(ELcc* LCC, int N, double vf_i);
void set_elprops(ELcc* LCC, double* mu, double* kappa);
void free_lcc(ELcc* LCC);

void malloc_YF(FY2nd* YF);
void free_YF(FY2nd* YF);
void CA_dCdA_MT(ELcc* LCC, double v1, double **S, double **dS, double *dnu);

void malloc_Epresult(EPR* EpResult);
void free_Epresult(EPR* EpResult);
#endif


#endif
