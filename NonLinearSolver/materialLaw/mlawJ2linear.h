//
// C++ Interface: material law
//
// Description: j2 linear elasto-plastic law it is a "virtual implementation" you have to define a law
//              in your project which derive from this law. you have to do THE SAME for your IPVariable
//              which have to derive from IPJ2linear (all data in this ipvarible have name beggining by _j2...
//
// Author:  <Gauthier BECKER>, (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef MLAWJ2LINEAR_H_
#define MLAWJ2LINEAR_H_
#include "mlaw.h"
#include "STensor3.h"
#include "STensor43.h"
#include "ipJ2linear.h"
#include "viscosityLaw.h"

class mlawJ2linear : public materialLaw
{
 public:
  enum stressFormumation{ CORO_CAUCHY = 0,    // cauchy stress in yield surface
                            CORO_KIRCHHOFF = 1  // Kirchhoff stress in yield surface
                          };
 protected:
  J2IsotropicHardening *_j2IH;
  viscosityLaw* _viscosityLaw;
  double _p; // power law of viscoplasticity
  // can't be const due to operator= constructor (HOW TO CHANGE THIS ??)
  double _rho; // density
  double _E; // young modulus
  double _nu; // Poisson ratio
  double _lambda; // 1st lame parameter
  double _mu; // 2nd lame parameter (=G)
  double _K; // bulk modulus
  double _K3; // 3*bulk modulus
  double _mu3; // 3*_mu = 3*G
  double _mu2; // 2*_mu = 2*G
  double _tol; // tolerance for iterative process
  double _perturbationfactor; // perturbation factor
  bool _tangentByPerturbation; // flag for tangent by perturbation
  int _order;
  stressFormumation _stressFormulation;

  STensor43 _Cel; // elastic hook tensor
  STensor43 _I4dev; // dev part of _I4

 public:
  mlawJ2linear(const int num,const double E,const double nu, const double rho,const double sy0,const double h,const double tol=1.e-6,
              const bool matrixbyPerturbation = false, const double pert = 1e-8);
  mlawJ2linear(const int num,const double E,const double nu, const double rho,const J2IsotropicHardening &_j2IH,const double tol=1.e-6,
              const bool matrixbyPerturbation = false, const double pert = 1e-8);
 #ifndef SWIG
  mlawJ2linear(const mlawJ2linear &source);
  mlawJ2linear& operator=(const materialLaw &source);

  void setViscosityEffect(const viscosityLaw& v, const double p);
  void setStressFormulation(int s);

  virtual ~mlawJ2linear();

	virtual materialLaw* clone() const {return new mlawJ2linear(*this);};
  virtual bool withEnergyDissipation() const {return true;};
  // function of materialLaw
  virtual matname getType() const{return materialLaw::J2;}
  virtual void createIPState(IPStateBase* &ips,bool hasBodyForce, const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const IntPt *GP=NULL, const int gpt = 0) const;
  virtual void initLaws(const std::map<int,materialLaw*> &maplaw){}; // this law is initialized so nothing to do
  virtual double soundSpeed() const; // default but you can redefine it for your case
  virtual double density()const{return _rho;}
  virtual const double bulkModulus() const{return _K;}
  virtual const double shearModulus() const{return _mu;}
  virtual const double poissonRatio() const{return _nu;}
	virtual double scaleFactor() const{return _mu;};
  virtual const J2IsotropicHardening * getJ2IsotropicHardening() const {return _j2IH;}
  virtual void setStrainOrder(const int i) {_order = i; Msg::Info("order %d is used to approximate log and exp operator ",_order);};
  // specific function
 public:
  virtual void constitutive(
                            const STensor3& F0,         // initial deformation gradient (input @ time n)
                            const STensor3& Fn,         // updated deformation gradient (input @ time n+1)
                            STensor3 &P,                // updated 1st Piola-Kirchhoff stress tensor (output)
                                                        // contains the initial values on input
                            const IPVariable *q0,       // array of initial internal variable
                            IPVariable *q1,             // updated array of internal variable (in ipvcur on output),
                            STensor43 &Tangent,         // constitutive tangents (output)
                            const bool stiff,            // if true compute the tangents
                            STensor43* elasticTangent = NULL, 
                            const bool dTangent =false,
                            STensor63* dCalgdeps = NULL
                           ) const;
  virtual void tangent_full(
                             STensor43 &T_,
                             const STensor3 &P_,
                             const STensor3 &F_,
                             const STensor3 &Fp_,
                             const STensor3 &Fp1_,
                             const IPJ2linear* q0,
                             const IPJ2linear* q1
                           ) const;

  virtual void checkInternalState(IPVariable* ipv, const IPVariable* ipvprev) const{};
 protected:
    void tangent(
                        STensor43 &K_,
                        STensor3 &Epre_,
                        const IPJ2linear *q0_,
                        const IPJ2linear *q1_,
                        STensor43 &dFpdE_
                      ) const;

    double deformationEnergy(const STensor3 &C) const ;
    void J2flow(STensor3 &M_,const STensor3 &Ee_) const;

    void predictorCorector(const STensor3& F0,         // initial deformation gradient (input @ time n)
                            const STensor3& F,         // updated deformation gradient (input @ time n+1)
                            STensor3 &P,                // updated 1st Piola-Kirchhoff stress tensor (output)
                            const IPJ2linear *q0,       // array of initial internal variable
                            IPJ2linear *q,             // updated array of internal variable (in ipvcur on output),

                            STensor43& Tangent,         // mechanical tangents (output)
                            STensor43& dFpdF, // plastic tangent
                            STensor43& dFedF,
                            STensor3& dpdF,
                            const bool stiff,
                            STensor43* elasticTangent = NULL, 
                            const bool dTangent =false,
                            STensor63* dCalgdeps = NULL
                            ) const;
    virtual void ElasticStiffness(STensor43* elasticTensor) const;
 #endif // SWIG
};

#endif // MLAWJ2LINEAR_H_
