//
// C++ Interface: material law
//
// Description: UMAT interface
//
//
// Author:  <L. NOELS>, (C) 2019
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include <math.h>
#include "MInterfaceElement.h"
#include <fstream>
#include "mlawUMATInterface.h"
#include "STensorOperations.h"


mlawUMATInterface::mlawUMATInterface(const int num, const double rho,
                   const char *propName) : materialLaw(num,true), _rho(rho), props(NULL), nprops1(0), _temperature(273.), _deltaTemperature(0.)
  {

	//allocate memory
        strn_n =new double[6];
	strs_n =new double[6];
	dstrn=new double[6];
	strs=new double[6];
        dStrsdT=new double[6];	
        for(int i=0;i<6; i++)
        {
          strn_n[i]=0.;
	  strs_n[i]=0.;
	  dstrn[i]=0.;
	  strs[i]=0.;
          dStrsdT[i]=0.;	
        }       

 	Ftmp=new double*[3];
	Ftmp0=new double*[3];
        dR=new double*[3];
        for(int i=0; i<3; i++)
        {
          Ftmp[i]=new double[3];
	  Ftmp0[i]=new double[3];
          dR[i]=new double[3];
          for(int j=0;j<3; j++)
          {
            if(i==j)
            {
              Ftmp[i][j]=1.;
	      Ftmp0[i][j]=1.;
              dR[i][j]=1.;
            }
            else
            {
              Ftmp[i][j]=0.;
	      Ftmp0[i][j]=0.;
              dR[i][j]=0.;
            }
          }
        }        
	Calgo=new double*[6];
        for(int i=0; i<6; i++)
        {
	  Calgo[i]=new double[6];
          for(int j=0; j<6; j++)
          {
            Calgo[i][j]=0.;
          }
        }
        //should depend on the umat
        /*std::ifstream in(propName);
        if(!in) Msg::Error("Cannot open the file %s! Maybe is missing   ",propName);
        in >> nprops1;
        in.ignore(256,'\n');
        props=new double[nprops1];
        for (int i=0;i < nprops1; i++)
        {
          in >> props[i];
          in.ignore(256,'\n');
        }*/
       // should depend on the UMAT 
       //nsdv = ???;

       //_mu0 = C1212/2.;
       //_nu0 = C1111/2./(_mu0+C1111);
  }
  mlawUMATInterface::mlawUMATInterface(const mlawUMATInterface &source) :
                                        materialLaw(source), _rho(source._rho), _mu0(source._mu0), _nu0(source._nu0), 
                                        _temperature(source._temperature), _deltaTemperature(source._deltaTemperature)
  {

	//allocate memory
	strn_n =new double[6];
	strs_n =new double[6];
	dstrn=new double[6];
	strs=new double[6];
        dStrsdT=new double[6];	
	
 	Ftmp=new double*[3];
	Ftmp0=new double*[3];
        dR=new double*[3];
        for(int i=0; i<3; i++)
        {
          Ftmp[i]=new double[3];
	  Ftmp0[i]=new double[3];
          dR[i]=new double[3];
        }        
	Calgo=new double*[6];
        for(int i=0; i<6; i++)
        {
	  Calgo[i]=new double[6];
        }
        //copy
        for(int i=0; i< 3; i++)
        {
           for(int j=0; j<3; j++)
           {
              Ftmp[i][j] = source.Ftmp[i][j];
              Ftmp0[i][j] = source.Ftmp0[i][j];
              dR[i][j] = source.dR[i][j];
           }
        }
        for(int i=0; i< 6; i++)
        {
           strn_n[i] = source.strn_n[i];
           strs_n[i] = source.strs_n[i];
           dstrn[i]  = source.dstrn[i];
           strs[i]   = source.strs[i];

           for(int j=0; j<6; j++)
           {
              Calgo[i][j] = source.Calgo[i][j];
           }
         }

         nprops1 = source.nprops1;
         if(nprops1>0)
         {
           props=new double[nprops1];
  
           for (int i=0;i < nprops1; i++)
           {
             props[i] = source.props[i];
           }
        }
        nsdv = source.nsdv;
  }

  mlawUMATInterface& mlawUMATInterface::operator=(const materialLaw &source)
  {
     materialLaw::operator=(source);
     const mlawUMATInterface* src =static_cast<const mlawUMATInterface*>(&source);
     _rho = src->_rho;
     _mu0 = src->_mu0;
     _nu0 = src->_nu0;
     _temperature = src->_temperature;
     _deltaTemperature = src->_deltaTemperature;

     for(int i=0; i< 3; i++)
     {
       for(int j=0; j<3; j++)
       {
         Ftmp[i][j] = src->Ftmp[i][j];
         Ftmp0[i][j] = src->Ftmp0[i][j];
         dR[i][j] = src->dR[i][j];
       }
     }

     for(int i=0; i< 6; i++)
     {
       strn_n[i] = src->strn_n[i];
       strs_n[i] = src->strs_n[i];
       dstrn[i]  = src->dstrn[i];
       strs[i]   = src->strs[i];
       dStrsdT[i]= src->dStrsdT[i];	

       for(int j=0; j<6; j++)
       {
         Calgo[i][j] = src->Calgo[i][j];
       }
     }
     nprops1 = src->nprops1;
     for (int i=0;i < nprops1; i++)
     {
       props[i] = src->props[i];
     }
     nsdv = src->nsdv;

    return *this;
  }

  mlawUMATInterface::~mlawUMATInterface()
  {
    delete []strn_n;
    delete []strs_n;
    delete []dstrn;
    delete []strs;
    delete []dStrsdT;
    for(int i=0; i<3; i++)
    {
      delete []Ftmp[i];
      delete []Ftmp0[i];
      delete []dR[i];
    }
    delete []Ftmp;
    delete []Ftmp0;
    delete []dR;
    for(int i=0; i<6; i++)
    {
      delete []Calgo[i];
    }
    delete []Calgo;
    if(props!=NULL)
    {
      delete []props;
    }
  }
void mlawUMATInterface::createIPState(IPUMATInterface *ivi, IPUMATInterface *iv1, IPUMATInterface *iv2) const
{
  //initialize Eshelby tensor
  int kstep = 1;
  int kinc  = 1;
  double *stvi = ivi->_statev;
  double *stv1 = iv1->_statev;
  double *stv2 = iv2->_statev;
  //here call umat with only stv1 to initialize 
  double &sse=ivi->getRefToDefoEnergy();
  double &spd=ivi->getRefToPlasticEnergy(); 
  double &scd=ivi->getRefToDamageEnergy();
  double rpl=0.; //related to tangential flow in open cohesive elements
  double timestep=this->getTimeStep();
  if(timestep<1.e-16) timestep=1.e-7;
  double tim[2];
  tim[0]=this->getTime()-timestep;
  tim[1]=this->getTime()-timestep; //not sure, to check
  double predef[1], dpred[1];
  predef[0]=0.; dpred[0]=0.;
  int ndi=0, nshr=0,noel=1,npt=1, layer=1,kspt=0;  
  double coords[3];
  double drplde[6];
  for(int i=0; i<6; i++) drplde[i]=0.; 
  double pnewdt=1., drpldt=0.; //celent = characteristic element length to be changed
  double celent=ivi->elementSize();

  callUMAT(strs, stv1, Calgo, sse, spd, scd, rpl, dStrsdT, drplde, drpldt, strn_n, dstrn,
           tim, timestep, _temperature, _deltaTemperature, predef, dpred,
           getCMNAME(), ndi, nshr, 6, getNsdv(), props,nprops1, coords,dR, pnewdt, celent, Ftmp0, Ftmp, 
           noel, npt, layer, kspt, kstep, kinc);

  for(int i=1; i<nsdv; i++)
  {
    stvi[i]=stv1[i];
    stv2[i]=stv1[i];
  }
}

void mlawUMATInterface::createIPVariable(IPUMATInterface *ipv, bool hasBodyForce, const MElement *ele,const int nbFF,const IntPt *GP, const int gpt) const
{
  //initialize Eshelby tensor
  int kstep = 1;
  int kinc  = 1;
  double *stvi = ipv->_statev;
 
  //here call umat with only stvi to initialize 
  //here call umat with only stv1 to initialize 
  double &sse=ipv->getRefToDefoEnergy();
  double &spd=ipv->getRefToPlasticEnergy();
  double &scd=ipv->getRefToDamageEnergy();

  double rpl=0.; //related to tangential flow in open cohesive elements
  double timestep=this->getTimeStep();
  if(timestep<1.e-16) timestep=1.e-7;
  double tim[2];
  tim[0]=this->getTime()-timestep;
  tim[1]=this->getTime()-timestep; //not sure, to check
  double predef[1], dpred[1];
  predef[0]=0.; dpred[0]=0.;
  int ndi=0, nshr=0,noel=1,npt=1, layer=1,kspt=0;  
  double coords[3];
  for(int i=0; i<3; i++) coords[i]=0.; 
  double drplde[6];
  for(int i=0; i<6; i++) drplde[i]=0.; 
  double pnewdt=1., drpldt=0.; 
  double celent=ipv->elementSize();

  callUMAT(strs, stvi, Calgo, sse, spd, scd, rpl, dStrsdT, drplde, drpldt, strn_n, dstrn,
           tim, timestep, _temperature, _deltaTemperature, predef, dpred,
           getCMNAME(), ndi, nshr, 6, getNsdv(), props, nprops1, coords,dR, pnewdt, celent, Ftmp0, Ftmp, 
           noel, npt, layer, kspt, kstep, kinc);
}

double mlawUMATInterface::soundSpeed() const
{
  double nu = poissonRatio();
  double mu = shearModulus();
  double E = 2.*mu*(1.+nu);
  double factornu = (1.-nu)/((1.+nu)*(1.-2.*nu));
  return sqrt(E*factornu/_rho);
}


void mlawUMATInterface::constitutive(const STensor3& F0,
                                     const STensor3& Fn,
                                     STensor3 &P,
                                     const IPVariable *ipvprevi,
                                     IPVariable *ipvcuri,
                                     STensor43 &Tangent,
                                     const bool stiff,
                                     STensor43* elasticTangent, 
                                     const bool dTangent,
                                     STensor63* dCalgdeps) const
{
  if(elasticTangent!=NULL) Msg::Error(" mlawUMATInterface:: STensor43* elasticTangent not defined");
  const IPUMATInterface *ipvprev = dynamic_cast<const IPUMATInterface *> (ipvprevi);
  IPUMATInterface *ipvcur = dynamic_cast<IPUMATInterface *> (ipvcuri);

  for(int i=0;i<3;i++)
  {
    for(int j=0;j<3;j++)
    {
      Ftmp[i][j] = Fn(i,j);
      Ftmp0[i][j] = F0(i,j);
    }
  }
  static STensor3 Rn, Un, dFn, F0inv, dRn, dUn;
#if 0
  STensorOperation::RUDecomposition(Fn,Un,Rn);
  STensorOperation::inverseSTensor3(F0, F0inv);
  STensorOperation::multSTensor3(Fn,F0inv,dFn);
  STensorOperation::RUDecomposition(dFn,dUn,dRn);

  //dR
  for(int i=0; i<3; i++)
    for(int j=0; j<3; j++)
      dR[i][j]=dRn(i,j);
  //strain increment with gamma
  dstrn[0]=dUn(0,0)-1.;
  dstrn[1]=dUn(1,1)-1.;
  dstrn[2]=dUn(2,2)-1.;
  dstrn[3] = dUn(0,1)+dUn(1,0);
  dstrn[4] = dUn(0,2)+dUn(2,0);
  dstrn[5] = dUn(1,2)+dUn(2,1);
#else
  STensorOperation::unity(dRn);
  dUn=Fn;
  STensorOperation::inverseSTensor3(F0, F0inv);
  //dUn-=F0;
  dUn*=F0inv;
  dstrn[0]=dUn(0,0)-1.;
  dstrn[1]=dUn(1,1)-1.;
  dstrn[2]=dUn(2,2)-1.;
  dstrn[3] = dUn(0,1)+dUn(1,0);
  dstrn[4] = dUn(0,2)+dUn(2,0);
  dstrn[5] = dUn(1,2)+dUn(2,1);
  static STensor3 dW;
  STensorOperation::zero(dW);
  dW(0,1)=(dUn(0,1)-dUn(1,0))/2.;
  dW(1,0)=-dW(0,1);
  dW(0,2)=(dUn(0,2)-dUn(2,0))/2.;
  dW(2,0)=-dW(0,2);
  dW(1,2)=(dUn(1,2)-dUn(2,1))/2.;
  dW(2,1)=-dW(1,2);
  static STensor3 ImdWHalf,ImdWHalfinv, IpdWHalf;
  STensorOperation::unity(ImdWHalf);
  STensorOperation::unity(IpdWHalf);
  ImdWHalf*=(2.);
  IpdWHalf*=(2.);
  ImdWHalf-=dW;
  IpdWHalf+=dW;
  ImdWHalf*=(1./2.);
  IpdWHalf*=(1./2.);
  STensorOperation::inverseSTensor3(ImdWHalf, ImdWHalfinv);
  dRn=ImdWHalfinv;
  dRn*=IpdWHalf;
  for(int i=0; i<3; i++)
    for(int j=0; j<3; j++)
      dR[i][j]=dRn(i,j);
  //here we should rotate previous stress as dR sigma dRT with sigma the cauchy
#endif

  //get previous strain and stress
  for(int i=0;i<6;i++){
    strn_n[i]=(ipvprev->_strain)(i);
  }
  //current strain
  for(int i=0;i<6;i++){
    (ipvcur->_strain)(i)=dstrn[i]+(ipvprev->_strain)(i);
  }
  //rotate previous stress
  double rotStress[3][3],rotStressTmp[3][3];
  rotStressTmp[0][0]=(ipvprev->_stress)(0);
  rotStressTmp[1][1]=(ipvprev->_stress)(1);
  rotStressTmp[2][2]=(ipvprev->_stress)(2);
  rotStressTmp[0][1]=rotStressTmp[1][0]=(ipvprev->_stress)(3);
  rotStressTmp[0][2]=rotStressTmp[2][0]=(ipvprev->_stress)(4);
  rotStressTmp[1][2]=rotStressTmp[2][1]=(ipvprev->_stress)(5);
  for(int i=0;i<3;i++)
  {
    for(int j=0;j<3;j++)
    {
      rotStress[i][j]=0.;
      for(int k=0;k<3;k++)
      {
        for(int l=0;l<3;l++)
        { 
          rotStress[i][j]+= dR[i][k]* rotStressTmp[k][l]*dR[j][l];
        }
      }
    }
  }
  for(int i=0;i<3;i++){
    strs_n[i]=rotStress[i][i];
  }
  strs_n[3]=rotStress[0][1];
  strs_n[4]=rotStress[0][2];
  strs_n[5]=rotStress[1][2];

  statev_n = ipvprev->_statev;
  statev_1 = ipvcur->_statev;

  // to required to use Eshelby tensor of previous step?
  for(int i=0; i<nsdv; i++)
  {
    statev_1[i]=statev_n[i];
  }
  int kinc = 1;
  int kstep = 2;
  //call umat
  double &sse=ipvcur->getRefToDefoEnergy();
  sse=ipvprev->defoEnergy();
  double &spd=ipvcur->getRefToPlasticEnergy(); 
  spd=ipvprev->plasticEnergy();
  double &scd=ipvcur->getRefToDamageEnergy();
  scd=ipvprev->damageEnergy();
  double rpl=0.; //related to tangential flow in open cohesive elements 
  double timestep=this->getTimeStep();
  if(timestep<1.e-16) timestep=1.e-7;
  double tim[2];
  tim[0]=this->getTime()-timestep;
  if(tim[0]<0.)
    tim[0]=0.;
  tim[1]=this->getTime()-timestep; //not sure, to check
  if(tim[1]<0.)
    tim[1]=0.;
  double predef[1], dpred[1]; //array of intrpolated values of field variables
  predef[0]=0.; dpred[0]=0.;
  int ndi=0, nshr=0;  //ndi: number of direct stress components, nshr number of engineeing shear stress component
  int noel=1,npt=1, layer=1,kspt=0;  //noel: element number, npt: integration point nb, layer only for composite shells, kspt point number in layer

  double coords[3]; //coordinate of point if needed
  for(int i=0; i<3; i++) coords[i]=0.; 
  double drplde[6]; //drplde varation of rpl with strain
  for(int i=0; i<6; i++) drplde[i]=0.; 
  double pnewdt=1., drpldt=0.;  //pnewdt ratio of time increment suggested, drpldt varation of rpl with Temp
  double celent=ipvcur->elementSize();

  callUMAT(strs, statev_1, Calgo, sse, spd, scd, rpl, dStrsdT, drplde, drpldt, strn_n, dstrn,
           tim, timestep, _temperature, _deltaTemperature, predef, dpred,
           getCMNAME(), ndi, nshr, 6, getNsdv(), props, nprops1, coords,dR, pnewdt, celent, Ftmp0, Ftmp, 
           noel, npt, layer, kspt, kstep, kinc);
  //HERE CREATE A VIRTUAL FUNCTION TO BE CALLED IN DERIVED CLASS
  //update stress and tangent operator
  for(int i=0;i<3;i++)
  {
    (ipvcur->_stress)(i)=strs[i];
    for(int j=0;j<3;j++){
      (ipvcur->_materialTensor)(i,j)=Calgo[i][j];
    }
  }
  for(int i=3;i<6;i++)
  {
    (ipvcur->_stress)(i)=strs[i];
    for(int j=0;j<3;j++)
    {
      (ipvcur->_materialTensor)(i,j)=Calgo[i][j];
      (ipvcur->_materialTensor)(j,i)=Calgo[j][i];
    }
    for(int j=3;j<6;j++)
    {
      (ipvcur->_materialTensor)(i,j)=Calgo[i][j];
    }
  }

  ipvcur->_elasticEne = 0.;
  ipvcur->_plasticEne = 0.;
  //convert str to P  :order xx yy zz xy xz yz
  //What we have is actually sigma -> send to P
  static STensor3 tau;
  for(int i=0;i<3;i++)
  {
    tau(i,i) = ipvcur->_stress(i);
  }
  tau(0,1) = ipvcur->_stress(3);
  tau(1,0) = ipvcur->_stress(3);
  tau(0,2) = ipvcur->_stress(4);
  tau(2,0) = ipvcur->_stress(4);
  tau(1,2) = ipvcur->_stress(5);
  tau(2,1) = ipvcur->_stress(5);
  double J=Fn.determinant();
  tau*=J;
  static STensor3 Finv, FinvT;
  STensorOperation::inverseSTensor3(Fn, Finv);
  STensorOperation::transposeSTensor3(Finv, FinvT);
  P=tau;
  P*=FinvT;
  //convert MaterialTensor to Tangent  :order xx yy zz xy xz yz
  if(stiff)
  {
    static STensor43 tmpTangent;
    static STensor3 _I(1.);
    tmpTangent(0,0,0,0) = ipvcur->_materialTensor(0, 0);
    tmpTangent(0,0,0,1) = ipvcur->_materialTensor(0, 3);
    tmpTangent(0,0,0,2) = ipvcur->_materialTensor(0, 4);
    tmpTangent(0,0,1,0) = ipvcur->_materialTensor(0, 3);
    tmpTangent(0,0,1,1) = ipvcur->_materialTensor(0, 1);
    tmpTangent(0,0,1,2) = ipvcur->_materialTensor(0, 5);
    tmpTangent(0,0,2,0) = ipvcur->_materialTensor(0, 4);
    tmpTangent(0,0,2,1) = ipvcur->_materialTensor(0, 5);
    tmpTangent(0,0,2,2) = ipvcur->_materialTensor(0, 2);

    tmpTangent(1,1,0,0) = ipvcur->_materialTensor(1, 0);
    tmpTangent(1,1,0,1) = ipvcur->_materialTensor(1, 3);
    tmpTangent(1,1,0,2) = ipvcur->_materialTensor(1, 4);
    tmpTangent(1,1,1,0) = ipvcur->_materialTensor(1, 3);
    tmpTangent(1,1,1,1) = ipvcur->_materialTensor(1, 1);
    tmpTangent(1,1,1,2) = ipvcur->_materialTensor(1, 5);
    tmpTangent(1,1,2,0) = ipvcur->_materialTensor(1, 4);
    tmpTangent(1,1,2,1) = ipvcur->_materialTensor(1, 5);
    tmpTangent(1,1,2,2) = ipvcur->_materialTensor(1, 2);

    tmpTangent(2,2,0,0) = ipvcur->_materialTensor(2, 0);
    tmpTangent(2,2,0,1) = ipvcur->_materialTensor(2, 3);
    tmpTangent(2,2,0,2) = ipvcur->_materialTensor(2, 4);
    tmpTangent(2,2,1,0) = ipvcur->_materialTensor(2, 3);
    tmpTangent(2,2,1,1) = ipvcur->_materialTensor(2, 1);
    tmpTangent(2,2,1,2) = ipvcur->_materialTensor(2, 5);
    tmpTangent(2,2,2,0) = ipvcur->_materialTensor(2, 4);
    tmpTangent(2,2,2,1) = ipvcur->_materialTensor(2, 5);
    tmpTangent(2,2,2,2) = ipvcur->_materialTensor(2, 2);

    tmpTangent(0,1,0,0) = ipvcur->_materialTensor(3, 0);
    tmpTangent(0,1,0,1) = ipvcur->_materialTensor(3, 3);
    tmpTangent(0,1,0,2) = ipvcur->_materialTensor(3, 4);
    tmpTangent(0,1,1,0) = ipvcur->_materialTensor(3, 3);
    tmpTangent(0,1,1,1) = ipvcur->_materialTensor(3, 1);
    tmpTangent(0,1,1,2) = ipvcur->_materialTensor(3, 5);
    tmpTangent(0,1,2,0) = ipvcur->_materialTensor(3, 4);
    tmpTangent(0,1,2,1) = ipvcur->_materialTensor(3, 5);
    tmpTangent(0,1,2,2) = ipvcur->_materialTensor(3, 2);

    tmpTangent(1,0,0,0) = ipvcur->_materialTensor(3, 0);
    tmpTangent(1,0,0,1) = ipvcur->_materialTensor(3, 3);
    tmpTangent(1,0,0,2) = ipvcur->_materialTensor(3, 4);
    tmpTangent(1,0,1,0) = ipvcur->_materialTensor(3, 3);
    tmpTangent(1,0,1,1) = ipvcur->_materialTensor(3, 1);
    tmpTangent(1,0,1,2) = ipvcur->_materialTensor(3, 5);
    tmpTangent(1,0,2,0) = ipvcur->_materialTensor(3, 4);
    tmpTangent(1,0,2,1) = ipvcur->_materialTensor(3, 5);
    tmpTangent(1,0,2,2) = ipvcur->_materialTensor(3, 2);

    tmpTangent(0,2,0,0) = ipvcur->_materialTensor(4, 0);
    tmpTangent(0,2,0,1) = ipvcur->_materialTensor(4, 3);
    tmpTangent(0,2,0,2) = ipvcur->_materialTensor(4, 4);
    tmpTangent(0,2,1,0) = ipvcur->_materialTensor(4, 3);
    tmpTangent(0,2,1,1) = ipvcur->_materialTensor(4, 1);
    tmpTangent(0,2,1,2) = ipvcur->_materialTensor(4, 5);
    tmpTangent(0,2,2,0) = ipvcur->_materialTensor(4, 4);
    tmpTangent(0,2,2,1) = ipvcur->_materialTensor(4, 5);
    tmpTangent(0,2,2,2) = ipvcur->_materialTensor(4, 2);

    tmpTangent(2,0,0,0) = ipvcur->_materialTensor(4, 0);
    tmpTangent(2,0,0,1) = ipvcur->_materialTensor(4, 3);
    tmpTangent(2,0,0,2) = ipvcur->_materialTensor(4, 4);
    tmpTangent(2,0,1,0) = ipvcur->_materialTensor(4, 3);
    tmpTangent(2,0,1,1) = ipvcur->_materialTensor(4, 1);
    tmpTangent(2,0,1,2) = ipvcur->_materialTensor(4, 5);
    tmpTangent(2,0,2,0) = ipvcur->_materialTensor(4, 4);
    tmpTangent(2,0,2,1) = ipvcur->_materialTensor(4, 5);
    tmpTangent(2,0,2,2) = ipvcur->_materialTensor(4, 2);

    tmpTangent(2,1,0,0) = ipvcur->_materialTensor(5, 0);
    tmpTangent(2,1,0,1) = ipvcur->_materialTensor(5, 3);
    tmpTangent(2,1,0,2) = ipvcur->_materialTensor(5, 4);
    tmpTangent(2,1,1,0) = ipvcur->_materialTensor(5, 3);
    tmpTangent(2,1,1,1) = ipvcur->_materialTensor(5, 1);
    tmpTangent(2,1,1,2) = ipvcur->_materialTensor(5, 5);
    tmpTangent(2,1,2,0) = ipvcur->_materialTensor(5, 4);
    tmpTangent(2,1,2,1) = ipvcur->_materialTensor(5, 5);
    tmpTangent(2,1,2,2) = ipvcur->_materialTensor(5, 2);

    tmpTangent(1,2,0,0) = ipvcur->_materialTensor(5, 0);
    tmpTangent(1,2,0,1) = ipvcur->_materialTensor(5, 3);
    tmpTangent(1,2,0,2) = ipvcur->_materialTensor(5, 4);
    tmpTangent(1,2,1,0) = ipvcur->_materialTensor(5, 3);
    tmpTangent(1,2,1,1) = ipvcur->_materialTensor(5, 1);
    tmpTangent(1,2,1,2) = ipvcur->_materialTensor(5, 5);
    tmpTangent(1,2,2,0) = ipvcur->_materialTensor(5, 4);
    tmpTangent(1,2,2,1) = ipvcur->_materialTensor(5, 5);
    tmpTangent(1,2,2,2) = ipvcur->_materialTensor(5, 2);
    //tmpTangent is actually such that Jaumann(Kirchhoff) = J tmpTangent: d with d the stretch tensor, see Lucarni & Segurado, Computational Mechanics 2018
    // delta d = 0.5 delta F F^-1 + 0.5 (delta F F^-1)^T 
    // delta w = 0.5 delta F F^-1 - 0.5 (delta F F^-1)^T 
    for(int i=0; i<3; i++)
    {
      for(int j=0; j<3; j++)
      {   
        for(int k=0; k<3; k++)
        {   
          for(int l=0; l<3; l++)
          {   
            Tangent(i,j,k,l) = 0.;
            for(int p=0; p<3; p++)
            {
              for(int m=0; m<3; m++)
              {
                Tangent(i,j,k,l)+= J*tmpTangent(i,p,k,m)*Finv(l,m)*Finv(j,p)+0.5*_I(i,k)*Finv(l,m)*tau(m,p)*Finv(j,p)+0.5*_I(p,k)*Finv(l,m)*tau(i,m)*Finv(j,p);   
              }
              Tangent(i,j,k,l)-=0.5*Finv(l,i)*tau(k,p)*Finv(j,p)+0.5*Finv(l,p)*tau(i,k)*Finv(j,p);
              Tangent(i,j,k,l)-= tau(i,p)*Finv(l,p)*Finv(j,k);
            }
          }
        }
      }
    }
  }
}


const double  mlawUMATInterface::bulkModulus() const
{
  return 2.*_mu0*(1+_nu0)/3./(1.-2.*_nu0);
}
const double  mlawUMATInterface::shearModulus() const
{
  return _mu0;
}

const double  mlawUMATInterface::poissonRatio() const
{
  return _nu0;
}
void mlawUMATInterface::convert3x3To9(const double **mat, double *v) const
{
  v[0]=mat[0][0];
  v[1]=mat[1][0];
  v[2]=mat[2][0];

  v[3]=mat[0][1];
  v[4]=mat[1][1];
  v[5]=mat[2][1];

  v[6]=mat[0][2];
  v[7]=mat[1][2];
  v[8]=mat[2][2];

}
void mlawUMATInterface::convert6x6To36(const double **mat, double *v) const
{
  v[0]=mat[0][0];
  v[1]=mat[1][0];
  v[2]=mat[2][0];
  v[3]=mat[3][0];
  v[4]=mat[4][0];
  v[5]=mat[5][0];

  v[6]=mat[0][1];
  v[7]=mat[1][1];
  v[8]=mat[2][1];
  v[9]=mat[3][1];
  v[10]=mat[4][1];
  v[11]=mat[5][1];

  v[12]=mat[0][2];
  v[13]=mat[1][2];
  v[14]=mat[2][2];
  v[15]=mat[3][2];
  v[16]=mat[4][2];
  v[17]=mat[5][2];

  v[18]=mat[0][3];
  v[19]=mat[1][3];
  v[20]=mat[2][3];
  v[21]=mat[3][3];
  v[22]=mat[4][3];
  v[23]=mat[5][3];

  v[24]=mat[0][4];
  v[25]=mat[1][4];
  v[26]=mat[2][4];
  v[27]=mat[3][4];
  v[28]=mat[4][4];
  v[29]=mat[5][4];

  v[30]=mat[0][5];
  v[31]=mat[1][5];
  v[32]=mat[2][5];
  v[33]=mat[3][5];
  v[34]=mat[4][5];
  v[35]=mat[5][5];

}

void mlawUMATInterface::convert9To3x3(const double *v,double **mat) const
{
  mat[0][0] = v[0];
  mat[1][0] = v[1];
  mat[2][0] = v[2];

  mat[0][1] = v[3];
  mat[1][1] = v[4];
  mat[2][1] = v[5];

  mat[0][2] = v[6];
  mat[1][2] = v[7];
  mat[2][2] = v[8];
}

void mlawUMATInterface::convert36To6x6(const double *v,double **mat) const
{
  mat[0][0] = v[0];
  mat[1][0] = v[1];
  mat[2][0] = v[2];
  mat[3][0] = v[3];
  mat[4][0] = v[4];
  mat[5][0] = v[5];

  mat[0][1] = v[6];
  mat[1][1] = v[7];
  mat[2][1] = v[8];
  mat[3][1] = v[9];
  mat[4][1] = v[10];
  mat[5][1] = v[11];

  mat[0][2] = v[12];
  mat[1][2] = v[13];
  mat[2][2] = v[14];
  mat[3][2] = v[15];
  mat[4][2] = v[16];
  mat[5][2] = v[17];

  mat[0][3] = v[18];
  mat[1][3] = v[19];
  mat[2][3] = v[20];
  mat[3][3] = v[21];
  mat[4][3] = v[22];
  mat[5][3] = v[23];

  mat[0][4] = v[24];
  mat[1][4] = v[25];
  mat[2][4] = v[26];
  mat[3][4] = v[27];
  mat[4][4] = v[28];
  mat[5][4] = v[29];

  mat[0][5] = v[30];
  mat[1][5] = v[31];
  mat[2][5] = v[32];
  mat[3][5] = v[33];
  mat[4][5] = v[34];
  mat[5][5] = v[35];

}

void mlawUMATInterface::callUMAT(double *stress, double *statev, double **ddsdde, double &sse, double &spd, double &scd, double &rpl, 
                                 double *ddsddt, double *drplde, double &drpldt, double *stran, double *dtsran,
                                 double *tim, double timestep, double temperature, double deltaTemperature, double *predef, double *dpred,
                                 const char *CMNAME, int &ndi, int &nshr, int tensorSize, 
                                 int statevSize, double *prs, int matPropSize, double *coords, double **dRot,
                                 double &pnewdt, double &celent, double **F0, double **F1, 
                                 int &noel, int &npt, int &layer, int &kspt, int &kstep, int &kinc) const
{
  Msg::Error("mlawUMATInterface::callUMAT has to be particularized for each law");
} 

