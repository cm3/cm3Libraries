//
// C++ Interface: J2 material in small strains
//
//
// Author:  <Van Dung NGUYEN>, (C) 2016
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef MLAWJ2VMSMALLSTRAIN_H_
#define MLAWJ2VMSMALLSTRAIN_H_

#include "mlaw.h"
#include "STensor3.h"
#include "STensor43.h"
#include "ipJ2linear.h"

class mlawJ2VMSmallStrain : public materialLaw {
  protected:
    // hardening at reference temperature
    J2IsotropicHardening *_j2IH; // isotropic hardening
    double  _K, _mu, _E, _nu, _lambda;
    double _tol; // tolerace of plastic corrector
    double _rho; // density
    double _perturbationfactor; // perturbation factor
    bool _tangentByPerturbation; // flag for tangent by perturbation
    //
    STensor43 _Cel, _I4dev, _I4; // elastic hook tensor

  public:
    mlawJ2VMSmallStrain(const int num, const double E, const double nu,const double rho,
                               const double sy0,const double h,const double tol=1.e-6,
                               const bool matrixbyPerturbation = false, const double pert = 1e-8,
                               const bool init =true);
    mlawJ2VMSmallStrain(const int num, const double E, const double nu,const double rho,
                               const J2IsotropicHardening& j2IH,const double tol=1.e-6,
                               const bool matrixbyPerturbation = false, const double pert = 1e-8,
                               const bool init =true);

    mlawJ2VMSmallStrain(const mlawJ2VMSmallStrain& src);
    virtual ~mlawJ2VMSmallStrain();

		virtual materialLaw* clone() const {
			return new mlawJ2VMSmallStrain(*this);
		}
		virtual void checkInternalState(IPVariable* ipv, const IPVariable* ipvprev) const{}; // do nothing
    virtual bool withEnergyDissipation() const {return true;};

    virtual const J2IsotropicHardening * getJ2IsotropicHardening() const {return _j2IH;}
    void setIsotropicHardeningLaw(const J2IsotropicHardening& isoHard){
      if (_j2IH != NULL) delete _j2IH;
      _j2IH = isoHard.clone();
    }
    virtual matname getType() const{return materialLaw::J2;}
    virtual void createIPState(IPStateBase* &ips, bool hasBodyForce, const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const IntPt *GP=NULL, const int gpt = 0) const;
    virtual void initLaws(const std::map<int,materialLaw*> &maplaw){};
    virtual double soundSpeed() const;
    virtual double density() const{return _rho;};
    virtual double shearModulus() const {return _mu;}
    virtual double bulkModulus() const {return _K;}
    virtual double scaleFactor() const{return _mu;};
    virtual void ElasticStiffness(STensor43* elasticTensor) const;
    virtual double ElasticYoungModulus() const {return _E;}
    virtual double ElasticShearModulus() const {return _mu;}
    virtual double ElasticBulkModulus() const {return _K;}

    virtual double TangentShearModulus(const IPVariable *q0, const IPVariable *q1) const;
    virtual double SecantShearModulus(const IPVariable *q1) const;
    virtual double ResidualSecantShearModulus(const IPVariable *q_unloaded, const IPVariable *q1) const;
    virtual double ZeroResidualSecantShearModulus(const IPVariable *q_unloaded, const IPVariable *q1) const;

    virtual void ElasticStiffnessIsotropic(STensor43& ElasticTangent) const;
    virtual void TangentStiffnessIsotropic(const IPVariable *q0i, const IPVariable *q1i, STensor43& Tangent) const;
    virtual void SecantStiffnessIsotropic(const IPVariable *q1i, STensor43& Secant) const;
    virtual void ResidualSecantStiffnessIsotropic(const IPVariable *q_unloadedi, const IPVariable *q1i, STensor43& Secant) const;
    virtual void ZeroResidualSecantStiffnessIsotropic(const IPVariable *q_unloadedi, const IPVariable *q1i, STensor43& Secant) const;

    virtual double getH(const IPVariable *q0, const IPVariable *q1) const;

    // predictor-corrector estimation
    virtual void constitutive(const STensor3& F0,         // previous deformation gradient (input @ time n)
                            const STensor3& Fn,         // current deformation gradient (input @ time n+1)
                            STensor3 &P,                // current 1st Piola-Kirchhoff stress tensor (output)
                            const IPVariable *q0,       // array of previous internal variables
                            IPVariable *q1,             // current array of internal variable (in ipvcur on output),
                            STensor43 &Tangent,         // tangents (output)
                            const bool stiff,            // if true compute the tangents
                            STensor43* elasticTangent = NULL, 
                            const bool dTangent =false,
                            STensor63* dCalgdeps = NULL) const;
                            
    virtual void constitutiveTFA(const STensor3& F0,         // previous deformation gradient (input @ time n)
                            const STensor3& Fn,         // current deformation gradient (input @ time n+1)
                            STensor3 &P,                // current 1st Piola-Kirchhoff stress tensor (output)
                            const IPVariable *q0,       // array of previous internal variables
                            IPVariable *q1,             // current array of internal variable (in ipvcur on output),
                            STensor43 &Tangent,         // tangents (output)
                            STensor3 &epsEig,
                            STensor43 &depsEigdeps) const;
                            
    virtual void constitutiveTFA_corr(
                            const STensor3& F0,         // previous deformation gradient (input @ time n)
                            const STensor3& Fn,         // current deformation gradient (input @ time n+1)
                            const STensor3& Fn_min,         // current deformation gradient (input @ time n+1)
                            const STensor3& Fn_max,         // current deformation gradient (input @ time n+1)
                            STensor3 &P,                // current 1st Piola-Kirchhoff stress tensor (output)
                            const IPVariable *q0,       // array of previous internal variables
                            IPVariable *q1,             // current array of internal variable (in ipvcur on output),
                            STensor43 &Tangent,         // tangents (output)
                            STensor3 &epsEig,
                            STensor43 &depsEigdeps) const;

    virtual void constitutiveIsotropicTangentTFA(const STensor3& F0,         // previous deformation gradient (input @ time n)
                            const STensor3& Fn,         // current deformation gradient (input @ time n+1)
                            STensor3 &P,                // current 1st Piola-Kirchhoff stress tensor (output)
                            const IPVariable *q0,       // array of previous internal variables
                            IPVariable *q1,             // current array of internal variable (in ipvcur on output),
                            STensor43 &Tangent,         // tangents (output)
                            STensor3 &epsEig,
                            STensor43 &depsEigdeps) const;

    virtual void constitutiveResidualSecantTFA( 
                            const STensor3& F0,         // previous deformation gradient (input @ time n)
                            const STensor3& Fn,         // current deformation gradient (input @ time n+1)
                            STensor3 &P,                // current 1st Piola-Kirchhoff stress tensor (output)
                            const IPVariable *q_unloaded,       // array of previous internal variables
                            IPVariable *q1,             // current array of internal variable (in ipvcur on output),
                            STensor43 &Secant,         // tangents (output)
                            STensor3 &epsEig,
                            STensor43 &depsEigdeps,
                            const bool tangent_compute =false,
                            STensor43* Tangent=NULL) const;


    virtual void constitutiveResidualSecant( 
                            const STensor3& F0,         // previous deformation gradient (input @ time n)
                            const STensor3& Fn,         // current deformation gradient (input @ time n+1)
                            STensor3 &P,                // current 1st Piola-Kirchhoff stress tensor (output)
                            const IPVariable *q_unloaded,       // array of previous internal variables
                            IPVariable *q1,             // current array of internal variable (in ipvcur on output),
                            STensor43 &Secant,         // tangents (output)
                            const bool stiff,            // if true compute the tangents
                            STensor43* elasticTangent = NULL, 
                            const bool dSecant =false,
                            STensor63* dCsecdeps = NULL,
                            const bool Calg_compute =false,
                            STensor43* C_algo = NULL) const;

    virtual void constitutiveZeroSecant( 
                            const STensor3& F0,         // previous deformation gradient (input @ time n)
                            const STensor3& Fn,         // current deformation gradient (input @ time n+1)
                            STensor3 &P,                // current 1st Piola-Kirchhoff stress tensor (output)
                            const IPVariable *q_unloaded,       // array of previous internal variables
                            IPVariable *q1,             // current array of internal variable (in ipvcur on output),
                            STensor43 &Secant,         // tangents (output)
                            const bool stiff,            // if true compute the tangents
                            STensor43* elasticTangent = NULL, 
                            const bool dSecant =false,
                            STensor63* dCsecdeps = NULL,
                            const bool Calg_compute =false,
                            STensor43* C_algo = NULL) const;


    virtual void constitutiveVirtualUnloading(
                            const STensor3& F0,         // previous deformation gradient (input @ time n)
                            const STensor3& F_res,         // current deformation gradient (input @ time n+1)
                            STensor3 &P_res,                // current 1st Piola-Kirchhoff stress tensor (output)
                            const IPVariable *q0,       // array of previous internal variables
                            IPVariable *q_unloaded        // current array of internal variable (in ipvcur on output),
                            ) const;

  protected:
      double deformationEnergy(const STensor3 &Ee) const;

      void predictorCorector(const STensor3& F0,         // initial deformation gradient (input @ time n)
                            const STensor3& F,         // updated deformation gradient (input @ time n+1)
                            STensor3 &P,                // updated 1st Piola-Kirchhoff stress tensor (output)
                            const IPJ2linear *q0,       // array of initial internal variable
                            IPJ2linear *q,             // updated array of internal variable (in ipvcur on output),
                            STensor43& Tangent,         // mechanical tangents (output)
                            STensor43& depsEldF,
                            STensor3& dpdF,
                            const bool stiff,
                            STensor43* elasticTangent = NULL, 
                            const bool dTangent =false,
                            STensor63* dCalgdeps = NULL) const;
                            
      void predictorCorectorTFA(const STensor3& F0,         // initial deformation gradient (input @ time n)
                            const STensor3& F,         // updated deformation gradient (input @ time n+1)
                            STensor3 &P,                // updated 1st Piola-Kirchhoff stress tensor (output)
                            const IPJ2linear *q0,       // array of initial internal variable
                            IPJ2linear *q,             // updated array of internal variable (in ipvcur on output),
                            STensor43& Tangent,         // mechanical tangents (output)      
                            STensor3 &epsEig,
                            STensor43 &depsEigdeps
                            ) const;
                            
      void predictorCorectorTFA_corr(const STensor3& F0,         // initial deformation gradient (input @ time n)
                            const STensor3& F,         // updated deformation gradient (input @ time n+1)
                            const STensor3& F_min,         // updated deformation gradient (input @ time n+1)
                            const STensor3& F_max,         // updated deformation gradient (input @ time n+1)
                            STensor3 &P,                // updated 1st Piola-Kirchhoff stress tensor (output)
                            const IPJ2linear *q0,       // array of initial internal variable
                            IPJ2linear *q,             // updated array of internal variable (in ipvcur on output),
                            STensor43& Tangent,         // mechanical tangents (output)      
                            STensor3 &epsEig,
                            STensor43 &depsEigdeps
                            ) const;

      void predictorCorectorIsotropicTangentTFA(const STensor3& F0,         // initial deformation gradient (input @ time n)
                            const STensor3& F,         // updated deformation gradient (input @ time n+1)
                            STensor3 &P,                // updated 1st Piola-Kirchhoff stress tensor (output)
                            const IPJ2linear *q0,       // array of initial internal variable
                            IPJ2linear *q,             // updated array of internal variable (in ipvcur on output),
                            STensor43& Tangent,         // mechanical tangents (output)      
                            STensor3 &epsEig,
                            STensor43 &depsEigdeps
                            ) const;

      void predictorCorectorResidualSecantTFA(const STensor3& F0,         // initial deformation gradient (input @ time n)
                            const STensor3& F,         // updated deformation gradient (input @ time n+1)
                            STensor3 &P,                // updated 1st Piola-Kirchhoff stress tensor (output)
                            const IPJ2linear *q_unloaded,       // array of initial internal variable
                            IPJ2linear *q,             // updated array of internal variable (in ipvcur on output),
                            STensor43& Secant,         // mechanical tangents (output)      
                            STensor3 &epsEig,
                            STensor43 &depsEigdeps,
                            const bool tangent_compute =false,
                            STensor43* Tangent = NULL) const;

      void predictorCorectorResidualSecant(const STensor3& F0,         // initial deformation gradient (input @ time n)
                            const STensor3& F,         // updated deformation gradient (input @ time n+1)
                            STensor3 &P,                // updated 1st Piola-Kirchhoff stress tensor (output)
                            const IPJ2linear *q_unloaded,       // array of initial internal variable
                            IPJ2linear *q,             // updated array of internal variable (in ipvcur on output),
                            STensor43& Secant,         // mechanical tangents (output)
                            STensor43& depsEldF,
                            STensor3& dpdF,
                            const bool stiff,
                            STensor43* elasticTangent = NULL, 
                            const bool dSecant =false,
                            STensor63* dCsecdeps = NULL,
                            const bool Calg_compute =false,
                            STensor43* C_algo = NULL) const;

      void predictorCorectorZeroSecant(const STensor3& F0,         // initial deformation gradient (input @ time n)
                            const STensor3& F,         // updated deformation gradient (input @ time n+1)
                            STensor3 &P,                // updated 1st Piola-Kirchhoff stress tensor (output)
                            const IPJ2linear *q_unloaded,       // array of initial internal variable
                            IPJ2linear *q,             // updated array of internal variable (in ipvcur on output),
                            STensor43& Secant,         // mechanical tangents (output)
                            STensor43& depsEldF,
                            STensor3& dpdF,
                            const bool stiff,
                            STensor43* elasticTangent = NULL, 
                            const bool dSecant =false,
                            STensor63* dCsecdeps = NULL,
                            const bool Calg_compute =false,
                            STensor43* C_algo = NULL
                            ) const;


      void elasticUnloading(const STensor3& F0,         // initial deformation gradient (input @ time n)
                            const STensor3& F,         // updated deformation gradient (input @ time n+1)
                            STensor3 &P,                // updated 1st Piola-Kirchhoff stress tensor (output)
                            const IPJ2linear *q0,       // array of initial internal variable
                            IPJ2linear *q_unloadedi             // updated array of internal variable (in ipvcur on output),
                            ) const;

};


#endif // MLAWJ2VMSMALLSTRAIN_H_
