//
// C++ Interface: material law
//
// Description: clustering material law
//
//
// Author:  <Kevin SPILKER>, (C) 2019
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include <math.h>
#include "MInterfaceElement.h"
#include <fstream>
#include "mlawTFAMaterialLaws.h"
#include "ipField.h"
#include "STensorOperations.h"
#include "FiniteStrain.h"
#include "matrix_operations.h"
#include <iostream>
using namespace std;


mlawGenericTFAMaterialLaws::mlawGenericTFAMaterialLaws(const int num, double rho) : materialLaw(num,true), _rho(rho), _reductionModel(NULL), _ownData(false)
{
  _mu=0.; //to be modified
  _nu =0.3;
}
mlawGenericTFAMaterialLaws::mlawGenericTFAMaterialLaws(const mlawGenericTFAMaterialLaws &source) :
                                        materialLaw(source)
{
  _ownData = false;
  _reductionModel = NULL;
  if (source._reductionModel != NULL)
  {
    _reductionModel = source._reductionModel;
  }
  _mu=source._mu;
  _nu=source._nu;
  _rho=source._rho;
}


mlawGenericTFAMaterialLaws& mlawGenericTFAMaterialLaws::operator=(const materialLaw &source)
{
  materialLaw::operator=(source);
  const mlawGenericTFAMaterialLaws* src =static_cast<const mlawGenericTFAMaterialLaws*>(&source);
  if(src!=NULL)
  {
    if (_ownData)
    {
      delete _reductionModel;
      _reductionModel = NULL;
    }
    _ownData = false;
    if (src->_reductionModel)
    {
      _reductionModel = src->_reductionModel;
    }

    _mu=src->_mu;
    _nu=src->_nu;
    _rho=src->_rho;
  }
  return *this;
}

mlawGenericTFAMaterialLaws::~mlawGenericTFAMaterialLaws()
{
  if (_ownData)
  {
    _ownData = false;
    if (_reductionModel) delete _reductionModel;
    _reductionModel = NULL;
  }
}

void mlawGenericTFAMaterialLaws::initLaws(const std::map<int,materialLaw*> &maplaw)
{
  if (_reductionModel!=NULL)
  {
    _reductionModel->initializeClusterMaterialMap(maplaw);
  }
}

void mlawGenericTFAMaterialLaws::addClusterMaterialLaw(int clusterID, materialLaw* clusterLaw)
{
  _reductionModel->addClusterLawToMap(clusterID, clusterLaw);
}


void mlawGenericTFAMaterialLaws::loadClusterSummaryAndInteractionTensorsFromFiles(const std::string clusterSummaryFileName, 
                                        const std::string interactionTensorsFileName)
{
  if (_reductionModel!=NULL)
  {
    _reductionModel->loadClusterSummaryAndInteractionTensorsFromFiles(clusterSummaryFileName,interactionTensorsFileName);
  }
  else
  {
    Msg::Error("reduction model must be allocated by the function setTFAMethod first !!!");
    Msg::Exit(0);
  }
}

void mlawGenericTFAMaterialLaws::loadClusterSummaryAndInteractionTensorsHomogenizedFrameFromFiles(const std::string clusterSummaryFileName, 
                                        const std::string interactionTensorsFileName)
{
  if (_reductionModel!=NULL)
  {
    _reductionModel->loadClusterSummaryAndInteractionTensorsHomogenizedFrameFromFiles(clusterSummaryFileName,interactionTensorsFileName);
  }
  else
  {
    Msg::Error("reduction model must be allocated by the function setTFAMethod first !!!");
    Msg::Exit(0);
  }
}

void mlawGenericTFAMaterialLaws::loadPlasticEqStrainConcentrationFromFile(const std::string PlasticEqStrainConcentrationFileName, 
                                                                   const std::string PlasticEqStrainConcentrationGeometricFileName,
                                                                   const std::string PlasticEqStrainConcentrationHarmonicFileName)
{
  if (_reductionModel!=NULL)
  {
    _reductionModel->loadPlasticEqStrainConcentrationFromFile(PlasticEqStrainConcentrationFileName,PlasticEqStrainConcentrationGeometricFileName,
                                                              PlasticEqStrainConcentrationHarmonicFileName);
  }
  else
  {
    Msg::Error("reduction model must be allocated by the function setTFAMethod first !!!");
    Msg::Exit(0);
  }
}



void mlawGenericTFAMaterialLaws::loadEshelbyInteractionTensorsFromFile(const std::string EshelbyInteractionTensorsFileName)
{
  if (_reductionModel!=NULL)
  {
    _reductionModel->loadEshelbyInteractionTensorsFromFile(EshelbyInteractionTensorsFileName);
  }
  else
  {
    Msg::Error("reduction model must be allocated by the function setTFAMethod first !!!");
    Msg::Exit(0);
  }
}

void mlawGenericTFAMaterialLaws::loadInteractionTensorsMatrixFrameELFromFile(const std::string InteractionTensorsMatrixFrameELFileName)
{
  if (_reductionModel!=NULL)
  {
    _reductionModel->loadInteractionTensorsMatrixFrameELFromFile(InteractionTensorsMatrixFrameELFileName);
  }
  else
  {
    Msg::Error("reduction model must be allocated by the function setTFAMethod first !!!");
    Msg::Exit(0);
  }
}

void mlawGenericTFAMaterialLaws::loadInteractionTensorsHomogenizedFrameELFromFile(const std::string InteractionTensorsHomogenizedFrameELFileName)
{
  if (_reductionModel!=NULL)
  {
    _reductionModel->loadInteractionTensorsHomogenizedFrameELFromFile(InteractionTensorsHomogenizedFrameELFileName);
  }
  else
  {
    Msg::Error("reduction model must be allocated by the function setTFAMethod first !!!");
    Msg::Exit(0);
  }
}





void mlawGenericTFAMaterialLaws::loadClusterStandardDeviationFromFile(const std::string ClusterStrainConcentrationSTD)
{
  if (_reductionModel!=NULL)
  {
    _reductionModel->loadClusterStandardDeviationFromFile(ClusterStrainConcentrationSTD);
  }
  else
  {
    Msg::Error("reduction model must be allocated by the function setTFAMethod first !!!");
    Msg::Exit(0);
  }
}


void mlawGenericTFAMaterialLaws::loadElasticStrainConcentrationDerivativeFromFile(const std::string ElasticStrainConcentrationDerivative)
{
  if (_reductionModel!=NULL)
  {
    _reductionModel->loadElasticStrainConcentrationDerivativeFromFile(ElasticStrainConcentrationDerivative);
  }
  else
  {
    Msg::Error("reduction model must be allocated by the function setTFAMethod first !!!");
    Msg::Exit(0);
  }
}


void mlawGenericTFAMaterialLaws::loadReferenceStiffnessFromFile(const std::string ReferenceStiffnessFileName)
{
  if (_reductionModel!=NULL)
  {
    _reductionModel->loadReferenceStiffnessFromFile(ReferenceStiffnessFileName);
  }
  else
  {
    Msg::Error("reduction model must be allocated by the function setTFAMethod first !!!");
    Msg::Exit(0);
  }
}

void mlawGenericTFAMaterialLaws::loadReferenceStiffnessIsoFromFile(const std::string ReferenceStiffnessIsoFileName)
{
  if (_reductionModel!=NULL)
  {
    _reductionModel->loadReferenceStiffnessIsoFromFile(ReferenceStiffnessIsoFileName);
  }
  else
  {
    Msg::Error("reduction model must be allocated by the function setTFAMethod first !!!");
    Msg::Exit(0);
  }
}

void mlawGenericTFAMaterialLaws::loadInteractionTensorIsoFromFile(const std::string InteractionTensorIsoFileName)
{
  if (_reductionModel!=NULL)
  {
    _reductionModel->loadInteractionTensorIsoFromFile(InteractionTensorIsoFileName);
  }
  else
  {
    Msg::Error("reduction model must be allocated by the function setTFAMethod first !!!");
    Msg::Exit(0);
  }
}


void mlawGenericTFAMaterialLaws::loadClusterFiberOrientationFromFile(const std::string OrientationDataFileName)
{
  if (_reductionModel!=NULL)
  {
    _reductionModel->loadClusterFiberOrientationFromFile(OrientationDataFileName);
  }
  else
  {
    Msg::Error("reduction model must be allocated by the function setTFAMethod first !!!");
    Msg::Exit(0);
  }
}


void mlawGenericTFAMaterialLaws::setTFAMethod(int TFAMethodNum, int dim, int correction, int solverType, int tag)
{
  if (_ownData)
  {
    if (_reductionModel) delete _reductionModel;
    _reductionModel = NULL;
  }
  
  _ownData = true;
  bool withFrame = false;
  bool polarization = false;
  if (TFAMethodNum == 0)
  {
    Msg::Info("IncrementalTangent is used !!!");
    _reductionModel = new IncrementalTangentTFA(solverType, dim, correction, withFrame, polarization, tag);
  }
  else if(TFAMethodNum == 1)
  {
    withFrame = true;
    Msg::Info("IncrementalTangentWithFrame is used !!!");
    _reductionModel = new IncrementalTangentTFAWithFrame(solverType, dim, correction, withFrame, polarization, tag);
  }
  else if(TFAMethodNum == 2)
  {
    polarization = true;
    Msg::Info("IncrementalTangentHS is used !!!");
    _reductionModel = new IncrementalTangentHS(solverType, dim, correction, withFrame, polarization, tag);
  }
  else if(TFAMethodNum == 3)
  {
    polarization = true;
    Msg::Info("IncrementalTangentPFA is used !!!");
    _reductionModel = new IncrementalTangentPFA(solverType, dim, correction, withFrame, polarization, tag);
  }
  else
  {
    Msg::Error("TFA method has not been implemented in mlawGenericTFAMaterialLaws");
  }
}

void mlawGenericTFAMaterialLaws::setClusterIncOrientation()
{
  if (_reductionModel!=NULL)
  {
    _reductionModel->setClusterIncOrientation();
  }
  else
  {
    Msg::Error("reduction model must be allocated by the function setTFAMethod first !!!");
    Msg::Exit(0);
  }
}

void mlawGenericTFAMaterialLaws::constitutive(
            const STensor3& F0,         // initial deformation gradient (input @ time n)
            const STensor3& Fn,         // updated deformation gradient (input @ time n+1)
            STensor3 &P,                // updated 1st Piola-Kirchhoff stress tensor (output)
            const IPVariable *q0i,       // array of initial internal variable
            IPVariable *q1i,             // updated array of internal variable (in ipvcur on output),
            STensor43 &Tangent,         // constitutive tangents (output)
            const bool stiff,         // if true compute the tangents
            STensor43* elasticTangent,
            const bool dTangent,
            STensor63* dCalgdeps) const
{

  if (_reductionModel != NULL)
  {
    const IPTFA* q0 = static_cast<const IPTFA*>(q0i);
    IPTFA* q1 = static_cast<IPTFA*>(q1i);
    _reductionModel->evaluate(F0,Fn,P,q0->getClusteringData(),q1->getClusteringData(),Tangent,true,elasticTangent);
    if (dTangent)
    {
      Msg::Error("mlawGenericTFAMaterialLaws::constitutive with dTangent option has not been implemented");
    }
  }
  else
  {
    Msg::Error("mlawGenericTFAMaterialLaws: reduction model has not been created !!!");
  }
}


void mlawGenericTFAMaterialLaws::createIPState(IPTFA *ivi, IPTFA *iv1, IPTFA *iv2) const
{
  Msg::Error("mlawGenericTFAMaterialLaws::createIPState(IPTFA *ivi, IPTFA *iv1, IPTFA *iv2 cannot be used without Ele");
}

void mlawGenericTFAMaterialLaws::createIPVariable(IPTFA *&ipv, bool hasBodyForce, const MElement *ele,const int nbFF, const IntPt *GP, const int gpt) const
{

  //find the clusters nb
  int clustersNb = _reductionModel->getNumberOfClusters();

  static std::vector<int> allCls;
  _reductionModel->getAllClusterIndices(allCls);
  
  static std::vector<double> allVolumes;
  _reductionModel->getAllClusterVolumes(allVolumes);
  //
  if (ipv != NULL)
    delete ipv;
  ipv=new IPTFA(allCls, allVolumes);

  for(int i=0; i<clustersNb; i++)
  {
    IPStateBase* ipslaw=NULL;
    const bool state_=true;
    _reductionModel->getMaterialLaw(allCls[i])->createIPState(ipslaw, hasBodyForce, &state_,ele, nbFF, GP, gpt);
    std::vector<IPVariable*> allIP;
    ipslaw->getAllIPVariable(allIP);
    ipv->getClusteringData()->getRefToIPv(allCls[i]) = allIP[0]->clone();
    delete ipslaw;
  }

}

void mlawGenericTFAMaterialLaws::createIPState(IPStateBase* &ips,bool hasBodyForce, const bool* state_,const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
  //find the clusters nb
  int clustersNb=_reductionModel->getNumberOfClusters();
  static std::vector<int> allCls;
  _reductionModel->getAllClusterIndices(allCls);
  static std::vector<double> allVolumes;
  _reductionModel->getAllClusterVolumes(allVolumes);
  
  if(ips != NULL) delete ips;
  IPTFA *ipvi=new IPTFA(allCls,allVolumes);
  IPTFA *ipv1=new IPTFA(allCls,allVolumes);
  IPTFA *ipv2=new IPTFA(allCls,allVolumes);

  for(int i=0; i<clustersNb; i++)
  {
    IPStateBase* ipslaw=NULL;
    _reductionModel->getMaterialLaw(allCls[i])->createIPState(ipslaw, hasBodyForce, state_,ele, nbFF_, GP, gpt);
    std::vector<IPVariable*> allIP;
    ipslaw->getAllIPVariable(allIP);

    ipvi->getClusteringData()->getRefToIPv(allCls[i]) = allIP[0]->clone();
    ipv1->getClusteringData()->getRefToIPv(allCls[i]) = allIP[1]->clone();
    ipv2->getClusteringData()->getRefToIPv(allCls[i]) = allIP[2]->clone();
    delete ipslaw;
  }
  ips = new IP3State(state_,ipvi,ipv1,ipv2);
}


double mlawGenericTFAMaterialLaws::soundSpeed() const
{
  double E = 2.*_mu*(1.+_nu);
  double factornu = (1.-_nu)/((1.+_nu)*(1.-2.*_nu));
  return sqrt(E*factornu/_rho);
};



mlawTFAMaterialLaws::~mlawTFAMaterialLaws()
{
  if (_ownData)
  {
    _ownData = false;
    if (_reductionModel) delete _reductionModel;
    _reductionModel = NULL;
  }
}

mlawTFAMaterialLaws& mlawTFAMaterialLaws::operator=(const materialLaw &source)
{
  materialLaw::operator=(source);
  const mlawTFAMaterialLaws* src =static_cast<const mlawTFAMaterialLaws*>(&source);
  if(src!=NULL)
  {
    if (_ownData)
    {
      delete _reductionModel;
      _reductionModel = NULL;
    }
    _ownData = false;
    if (src->_reductionModel)
    {
      _reductionModel = src->_reductionModel;
    }

    _mu=src->_mu;
    _nu=src->_nu;
    _rho=src->_rho;
  }
  return *this;
}


void mlawTFAMaterialLaws::createIPState(IPTFA *ivi, IPTFA *iv1, IPTFA *iv2) const
{
  Msg::Error("mlawTFAMaterialLaws::createIPState(IPTFA *ivi, IPTFA *iv1, IPTFA *iv2 cannot be used without Ele");
}

void mlawTFAMaterialLaws::createIPVariable(IPTFA *&ipv, bool hasBodyForce, const MElement *ele,const int nbFF, const IntPt *GP, const int gpt) const
{
  //find the clusters nb
  int clustersNb = _reductionModel->getNumberOfClusters();

  static std::vector<int> allCls;
  _reductionModel->getAllClusterIndices(allCls);
  
  static std::vector<double> allVolumes;
  _reductionModel->getAllClusterVolumes(allVolumes);
  //
  if (ipv != NULL)
    delete ipv;
  ipv=new IPTFA(allCls, allVolumes);

  for(int i=0; i<clustersNb; i++)
  {
    IPStateBase* ipslaw=NULL;
    const bool state_=true;
    _reductionModel->getMaterialLaw(allCls[i])->createIPState(ipslaw, hasBodyForce, &state_,ele, nbFF, GP, gpt);
    std::vector<IPVariable*> allIP;
    ipslaw->getAllIPVariable(allIP);
    ipv->getClusteringData()->getRefToIPv(allCls[i]) = allIP[0]->clone();
    delete ipslaw;
  }
}

void mlawTFAMaterialLaws::createIPState(IPStateBase* &ips,bool hasBodyForce, const bool* state_,const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
  //find the clusters nb
  int clustersNb=_reductionModel->getNumberOfClusters();
  static std::vector<int> allCls;
  _reductionModel->getAllClusterIndices(allCls);
  static std::vector<double> allVolumes;
  _reductionModel->getAllClusterVolumes(allVolumes);
  
  if(ips != NULL) delete ips;
  IPTFA *ipvi=new IPTFA(allCls,allVolumes);
  IPTFA *ipv1=new IPTFA(allCls,allVolumes);
  IPTFA *ipv2=new IPTFA(allCls,allVolumes);

  for(int i=0; i<clustersNb; i++)
  {
    IPStateBase* ipslaw=NULL;
    _reductionModel->getMaterialLaw(allCls[i])->createIPState(ipslaw, hasBodyForce, state_,ele, nbFF_, GP, gpt);
    std::vector<IPVariable*> allIP;
    ipslaw->getAllIPVariable(allIP);

    ipvi->getClusteringData()->getRefToIPv(allCls[i]) = allIP[0]->clone();
    ipv1->getClusteringData()->getRefToIPv(allCls[i]) = allIP[1]->clone();
    ipv2->getClusteringData()->getRefToIPv(allCls[i]) = allIP[2]->clone();
    delete ipslaw;
  }
  ips = new IP3State(state_,ipvi,ipv1,ipv2);
};



void mlawTFAHierarchicalMaterialLaws::setTFAMethod(int TFAMethodNum, int dim, int correction, int solverType, int tag)
{
  if (_ownData)
  {
    if (_reductionModel) delete _reductionModel;
    _reductionModel = NULL;
  }  
  _ownData = true;  
  
  bool polarization = false;
  if(TFAMethodNum == 0)
  {
    Msg::Info("HierarchicalIncrementalTangentTFA is used !!!");
    _reductionModel = new HierarchicalIncrementalTangentTFA(solverType, dim, 0, false, false, tag);
  }
}

mlawTFAHierarchicalMaterialLaws::~mlawTFAHierarchicalMaterialLaws()
{
  if (_ownData)
  {
    _ownData = false;
    if (_reductionModel) delete _reductionModel;
    _reductionModel = NULL;
  }
}

mlawTFAHierarchicalMaterialLaws& mlawTFAHierarchicalMaterialLaws::operator=(const materialLaw &source)
{
  materialLaw::operator=(source);
  const mlawTFAHierarchicalMaterialLaws* src =static_cast<const mlawTFAHierarchicalMaterialLaws*>(&source);
  if(src!=NULL)
  {
    if (_ownData)
    {
      delete _reductionModel;
      _reductionModel = NULL;
    }
    _ownData = false;
    if (src->_reductionModel)
    {
      _reductionModel = src->_reductionModel;
    }

    _mu=src->_mu;
    _nu=src->_nu;
    _rho=src->_rho;
  }
  return *this;
}


void mlawTFAHierarchicalMaterialLaws::initLaws(const std::map<int,materialLaw*> &maplaw)
{
  _nbLevels = dynamic_cast<HierarchicalReductionTFA*>(_reductionModel)->getNumberOfLevels();
  if (_reductionModel!=NULL)
  {
    if(_nbLevels==2)
    {
      dynamic_cast<HierarchicalReductionTFA*>(_reductionModel)->initializeClusterTFAMaterialMapLevel0(maplaw,_tfaLawID);
      dynamic_cast<HierarchicalReductionTFA*>(_reductionModel)->initializeClusterMaterialMapLevel1(maplaw);
    }
  }
}

void mlawTFAHierarchicalMaterialLaws::createIPState(IPTFA2Levels *ivi, IPTFA2Levels *iv1, IPTFA2Levels *iv2) const
{
  Msg::Error("mlawTFAMaterialLaws::createIPState(IPTFA *ivi, IPTFA *iv1, IPTFA *iv2 cannot be used without Ele");
}

void mlawTFAHierarchicalMaterialLaws::createIPVariable(IPTFA2Levels *&ipv, bool hasBodyForce, const MElement *ele,const int nbFF, const IntPt *GP, const int gpt) const
{

  //find the clusters nb
  int clustersNbLv0 = _reductionModel->getNumberOfClusters();

  static std::vector<int> allClsLevel0; 
  _reductionModel->getAllClusterIndices(allClsLevel0);  
  static std::vector<double> allVolumesLevel0;
  _reductionModel->getAllClusterVolumes(allVolumesLevel0);
  
  static std::map<int,std::vector<int>> allClsLevel1; 
  dynamic_cast<HierarchicalIncrementalTangentTFA*>(_reductionModel)->getAllClusterIndicesLevel1(allClsLevel1);  
  static std::map<int,std::vector<double>> allVolumesLevel1;
  dynamic_cast<HierarchicalIncrementalTangentTFA*>(_reductionModel)->getAllClusterVolumesLevel1(allVolumesLevel1);
  
  if (ipv != NULL)
    delete ipv;
  ipv=new IPTFA2Levels(allClsLevel0, allVolumesLevel0,
                       allClsLevel1, allVolumesLevel1);
  

  for(int itLv0=0; itLv0<clustersNbLv0; itLv0++)
  {
    int clLv0 = allClsLevel0[itLv0];
    
    IPVariable *ipvi=new IPTFA(allClsLevel0,allVolumesLevel0);
    IPVariable *ipv1=new IPTFA(allClsLevel0,allVolumesLevel0);
    IPVariable *ipv2=new IPTFA(allClsLevel0,allVolumesLevel0);
    IPStateBase* ipsTFA=NULL;
    const bool state_=true;
    ipsTFA = new IP3State(&state_,ipvi,ipv1,ipv2);
    std::vector<IPVariable*> allIP;
    ipsTFA->getAllIPVariable(allIP);
    dynamic_cast<IPTFA2Levels*>(ipv)->getClusteringDataLevel0()->getRefToIPv(clLv0) = allIP[0]->clone();
    delete ipsTFA;
        
    std::vector<int> clIDsUpper;
    clIDsUpper.push_back(clLv0);
    int clustersNbLv1 = dynamic_cast<HierarchicalIncrementalTangentTFA*>(_reductionModel)->getNumberOfSubClusters(clIDsUpper);
    
    std::vector<int> allClsOnLevel1 = allClsLevel1[clLv0];
    
    for(int itLv1=0; itLv1<clustersNbLv1; itLv1++)
    {
      int clLv1 = allClsOnLevel1[itLv1];
      std::vector<int> allClsHierarchical;
      allClsHierarchical.push_back(clLv0);
      allClsHierarchical.push_back(clLv1);
    
      IPStateBase* ipslaw=NULL;
      const bool state_=true;
      dynamic_cast<HierarchicalIncrementalTangentTFA*>(_reductionModel)->getMaterialLaw(allClsHierarchical)->createIPState(ipslaw, hasBodyForce, &state_,ele, nbFF, GP, gpt);
      std::vector<IPVariable*> allIP;
      ipslaw->getAllIPVariable(allIP);
      dynamic_cast<IPTFA2Levels*>(ipv)->getClusteringDataOnLevel1(clLv0)->getRefToIPv(clLv1) = allIP[0]->clone();
      
      delete ipslaw;
    }    
    IPVariable*& iptfaLv0  = dynamic_cast<IPTFA2Levels*>(ipv)->getClusteringDataLevel0()->getRefToIPv(clLv0);
    ClusteringData*& clustDataLv0 = dynamic_cast<IPTFA*>(iptfaLv0)->getRefToClusteringData();
    clustDataLv0 = dynamic_cast<IPTFA2Levels*>(ipv)->getClusteringDataOnLevel1(clLv0);
  }
}

void mlawTFAHierarchicalMaterialLaws::createIPState(IPStateBase* &ips,bool hasBodyForce, const bool* state_,const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
  //find the clusters nb
  int clustersNb=_reductionModel->getNumberOfClusters();
  
  static std::vector<int> allClsLevel0; 
  _reductionModel->getAllClusterIndices(allClsLevel0);  
  static std::vector<double> allVolumesLevel0;
  _reductionModel->getAllClusterVolumes(allVolumesLevel0);
  
  static std::map<int,std::vector<int>> allClsLevel1; 
  dynamic_cast<HierarchicalIncrementalTangentTFA*>(_reductionModel)->getAllClusterIndicesLevel1(allClsLevel1);  
  static std::map<int,std::vector<double>> allVolumesLevel1;
  dynamic_cast<HierarchicalIncrementalTangentTFA*>(_reductionModel)->getAllClusterVolumesLevel1(allVolumesLevel1);
  
  if(ips != NULL) delete ips;
  IPTFA2Levels *ipvi=new IPTFA2Levels(allClsLevel0,allVolumesLevel0,allClsLevel1,allVolumesLevel1);
  IPTFA2Levels *ipv1=new IPTFA2Levels(allClsLevel0,allVolumesLevel0,allClsLevel1,allVolumesLevel1);
  IPTFA2Levels *ipv2=new IPTFA2Levels(allClsLevel0,allVolumesLevel0,allClsLevel1,allVolumesLevel1);

  for(int itLv0=0; itLv0<clustersNb; itLv0++)
  {
    int clLv0 = allClsLevel0[itLv0];
    IPStateBase* ipslaw=NULL;
    _reductionModel->getMaterialLaw(clLv0)->createIPState(ipslaw, hasBodyForce, state_,ele, nbFF_, GP, gpt);
    std::vector<IPVariable*> allIP;
    ipslaw->getAllIPVariable(allIP);

    ipvi->getClusteringData()->getRefToIPv(clLv0) = allIP[0]->clone();
    ipv1->getClusteringData()->getRefToIPv(clLv0) = allIP[1]->clone();
    ipv2->getClusteringData()->getRefToIPv(clLv0) = allIP[2]->clone();
    delete ipslaw;
    
    std::vector<int> clIDsUpper;
    clIDsUpper.push_back(clLv0);
    int clustersNbLv1 = dynamic_cast<HierarchicalIncrementalTangentTFA*>(_reductionModel)->getNumberOfSubClusters(clIDsUpper);
    
    std::vector<int> allClsOnLevel1 = allClsLevel1[clLv0];
    
    for(int itLv1=0; itLv1<clustersNbLv1; itLv1++)
    {
      int clLv1 = allClsOnLevel1[itLv1];
      std::vector<int> allClsHierarchical;
      allClsHierarchical.push_back(clLv0);
      allClsHierarchical.push_back(clLv1);
      
      //IPStateBase* ipslaw=NULL;
      ipslaw=NULL;
      dynamic_cast<HierarchicalIncrementalTangentTFA*>(_reductionModel)->getMaterialLaw(allClsHierarchical)->createIPState(ipslaw, hasBodyForce, state_,ele, nbFF_, GP, gpt);
      std::vector<IPVariable*> allIP;
      ipslaw->getAllIPVariable(allIP);
      
      ipvi->getClusteringDataOnLevel1(clLv0)->getRefToIPv(clLv1) = allIP[0]->clone();
      ipv1->getClusteringDataOnLevel1(clLv0)->getRefToIPv(clLv1) = allIP[1]->clone();
      ipv2->getClusteringDataOnLevel1(clLv0)->getRefToIPv(clLv1) = allIP[2]->clone();
      
      delete ipslaw;
    } 
  }
  ips = new IP3State(state_,ipvi,ipv1,ipv2);
}


void mlawTFAHierarchicalMaterialLaws::loadClusterSummaryAndInteractionTensorsLevel1FromFiles(const std::string clusterSummaryFileName,  
                                                                                  const std::string interactionTensorsFileName)
{
  if (_reductionModel!=NULL)
  {
    dynamic_cast<HierarchicalIncrementalTangentTFA*>(_reductionModel)->loadClusterSummaryAndInteractionTensorsLevel1FromFiles(clusterSummaryFileName,interactionTensorsFileName);
  }
  else
  {
    Msg::Error("reduction model must be allocated by the function setTFAMethod first !!!");
    Msg::Exit(0);
  }
}


void mlawTFAHierarchicalMaterialLaws::constitutive(
            const STensor3& F0,         // initial deformation gradient (input @ time n)
            const STensor3& Fn,         // updated deformation gradient (input @ time n+1)
            STensor3 &P,                // updated 1st Piola-Kirchhoff stress tensor (output)
            const IPVariable *q0i,       // array of initial internal variable
            IPVariable *q1i,             // updated array of internal variable (in ipvcur on output),
            STensor43 &Tangent,         // constitutive tangents (output)
            const bool stiff,         // if true compute the tangents
            STensor43* elasticTangent,
            const bool dTangent,
            STensor63* dCalgdeps) const
{

  if (_reductionModel != NULL)
  {
    int nbLevels = dynamic_cast<HierarchicalIncrementalTangentTFA*>(_reductionModel)->getNumberOfLevels();
    if(nbLevels < 2)
    {
      Msg::Error("reduction model must have at least two level hierarchy !!!");
      Msg::Exit(0);
    }
    else if(nbLevels == 2)
    {
      const IPTFA2Levels* q0 = static_cast<const IPTFA2Levels*>(q0i);
      IPTFA2Levels* q1 = static_cast<IPTFA2Levels*>(q1i);
      dynamic_cast<HierarchicalIncrementalTangentTFA*>(_reductionModel)->evaluate(F0,Fn,P,
                                                                                         q0->getClusteringDataLevel0(),
                                                                                         q0->getClusteringDataLevel1(),
                                                                                         q1->getClusteringDataLevel0(),
                                                                                         q1->getClusteringDataLevel1(),
                                                                                         Tangent,true,elasticTangent);
    }
  }
  else
  {
    Msg::Error("reduction model has not been created !!!");
  }
}
;




/*mlawTFAMaterialLaws::mlawTFAMaterialLaws(const int num, double rho) : materialLaw(num,true), _rho(rho), _reductionModel(NULL), _ownData(false)
{
  _mu=0.; //to be modified
  _nu =0.3;
}
mlawTFAMaterialLaws::mlawTFAMaterialLaws(const mlawTFAMaterialLaws &source) :
                                        materialLaw(source)
{
  _ownData = false;
  _reductionModel = NULL;
  if (source._reductionModel != NULL)
  {
    _reductionModel = source._reductionModel;
  }
  _mu=source._mu;
  _nu=source._nu;
  _rho=source._rho;

}


mlawTFAMaterialLaws& mlawTFAMaterialLaws::operator=(const materialLaw &source)
{
  materialLaw::operator=(source);
  const mlawTFAMaterialLaws* src =static_cast<const mlawTFAMaterialLaws*>(&source);
  if(src!=NULL)
  {
    if (_ownData)
    {
      delete _reductionModel;
      _reductionModel = NULL;
    }
    _ownData = false;
    if (src->_reductionModel)
    {
      _reductionModel = src->_reductionModel;
    }

    _mu=src->_mu;
    _nu=src->_nu;
    _rho=src->_rho;
  }
  return *this;
}

mlawTFAMaterialLaws::~mlawTFAMaterialLaws()
{
  if (_ownData)
  {
    _ownData = false;
    if (_reductionModel) delete _reductionModel;
    _reductionModel = NULL;
  }
}

void mlawTFAMaterialLaws::initLaws(const std::map<int,materialLaw*> &maplaw)
{
  if (_reductionModel!=NULL)
  {
    _reductionModel->initializeClusterMaterialMap(maplaw);
  }
}

void mlawTFAMaterialLaws::addClusterMaterialLaw(int clusterID, materialLaw* clusterLaw)
{
  _reductionModel->addClusterLawToMap(clusterID, clusterLaw);
}


void mlawTFAMaterialLaws::loadClusterSummaryAndInteractionTensorsFromFiles(const std::string clusterSummaryFileName, 
                                        const std::string interactionTensorsFileName)
{
  if (_reductionModel!=NULL)
  {
    _reductionModel->loadClusterSummaryAndInteractionTensorsFromFiles(clusterSummaryFileName,interactionTensorsFileName);
  }
  else
  {
    Msg::Error("reduction model must be allocated by the function setTFAMethod first !!!");
    Msg::Exit(0);
  }
}

void mlawTFAMaterialLaws::loadClusterSummaryAndInteractionTensorsHomogenizedFrameFromFiles(const std::string clusterSummaryFileName, 
                                        const std::string interactionTensorsFileName)
{
  if (_reductionModel!=NULL)
  {
    _reductionModel->loadClusterSummaryAndInteractionTensorsHomogenizedFrameFromFiles(clusterSummaryFileName,interactionTensorsFileName);
  }
  else
  {
    Msg::Error("reduction model must be allocated by the function setTFAMethod first !!!");
    Msg::Exit(0);
  }
}

void mlawTFAMaterialLaws::loadPlasticEqStrainConcentrationFromFile(const std::string PlasticEqStrainConcentrationFileName, 
                                                                   const std::string PlasticEqStrainConcentrationGeometricFileName,
                                                                   const std::string PlasticEqStrainConcentrationHarmonicFileName,
                                                                   const std::string PlasticEqStrainConcentrationPowerFileName)
{
  if (_reductionModel!=NULL)
  {
    _reductionModel->loadPlasticEqStrainConcentrationFromFile(PlasticEqStrainConcentrationFileName,PlasticEqStrainConcentrationGeometricFileName,
                                                              PlasticEqStrainConcentrationHarmonicFileName,PlasticEqStrainConcentrationPowerFileName);
  }
  else
  {
    Msg::Error("reduction model must be allocated by the function setTFAMethod first !!!");
    Msg::Exit(0);
  }
}



void mlawTFAMaterialLaws::loadEshelbyInteractionTensorsFromFile(const std::string EshelbyInteractionTensorsFileName)
{
  if (_reductionModel!=NULL)
  {
    _reductionModel->loadEshelbyInteractionTensorsFromFile(EshelbyInteractionTensorsFileName);
  }
  else
  {
    Msg::Error("reduction model must be allocated by the function setTFAMethod first !!!");
    Msg::Exit(0);
  }
}

void mlawTFAMaterialLaws::loadInteractionTensorsMatrixFrameELFromFile(const std::string InteractionTensorsMatrixFrameELFileName)
{
  if (_reductionModel!=NULL)
  {
    _reductionModel->loadInteractionTensorsMatrixFrameELFromFile(InteractionTensorsMatrixFrameELFileName);
  }
  else
  {
    Msg::Error("reduction model must be allocated by the function setTFAMethod first !!!");
    Msg::Exit(0);
  }
}

void mlawTFAMaterialLaws::loadInteractionTensorsHomogenizedFrameELFromFile(const std::string InteractionTensorsHomogenizedFrameELFileName)
{
  if (_reductionModel!=NULL)
  {
    _reductionModel->loadInteractionTensorsHomogenizedFrameELFromFile(InteractionTensorsHomogenizedFrameELFileName);
  }
  else
  {
    Msg::Error("reduction model must be allocated by the function setTFAMethod first !!!");
    Msg::Exit(0);
  }
}





void mlawTFAMaterialLaws::loadClusterStandardDeviationFromFile(const std::string ClusterStrainConcentrationSTD)
{
  if (_reductionModel!=NULL)
  {
    _reductionModel->loadClusterStandardDeviationFromFile(ClusterStrainConcentrationSTD);
  }
  else
  {
    Msg::Error("reduction model must be allocated by the function setTFAMethod first !!!");
    Msg::Exit(0);
  }
}


void mlawTFAMaterialLaws::loadElasticStrainConcentrationDerivativeFromFile(const std::string ElasticStrainConcentrationDerivative)
{
  if (_reductionModel!=NULL)
  {
    _reductionModel->loadElasticStrainConcentrationDerivativeFromFile(ElasticStrainConcentrationDerivative);
  }
  else
  {
    Msg::Error("reduction model must be allocated by the function setTFAMethod first !!!");
    Msg::Exit(0);
  }
}


void mlawTFAMaterialLaws::loadReferenceStiffnessFromFile(const std::string ReferenceStiffnessFileName)
{
  if (_reductionModel!=NULL)
  {
    _reductionModel->loadReferenceStiffnessFromFile(ReferenceStiffnessFileName);
  }
  else
  {
    Msg::Error("reduction model must be allocated by the function setTFAMethod first !!!");
    Msg::Exit(0);
  }
}

void mlawTFAMaterialLaws::loadReferenceStiffnessIsoFromFile(const std::string ReferenceStiffnessIsoFileName)
{
  if (_reductionModel!=NULL)
  {
    _reductionModel->loadReferenceStiffnessIsoFromFile(ReferenceStiffnessIsoFileName);
  }
  else
  {
    Msg::Error("reduction model must be allocated by the function setTFAMethod first !!!");
    Msg::Exit(0);
  }
}

void mlawTFAMaterialLaws::loadInteractionTensorIsoFromFile(const std::string InteractionTensorIsoFileName)
{
  if (_reductionModel!=NULL)
  {
    _reductionModel->loadInteractionTensorIsoFromFile(InteractionTensorIsoFileName);
  }
  else
  {
    Msg::Error("reduction model must be allocated by the function setTFAMethod first !!!");
    Msg::Exit(0);
  }
}


void mlawTFAMaterialLaws::loadClusterFiberOrientationFromFile(const std::string OrientationDataFileName)
{
  if (_reductionModel!=NULL)
  {
    _reductionModel->loadClusterFiberOrientationFromFile(OrientationDataFileName);
  }
  else
  {
    Msg::Error("reduction model must be allocated by the function setTFAMethod first !!!");
    Msg::Exit(0);
  }
}


void mlawTFAMaterialLaws::setTFAMethod(int TFAMethodNum, int dim, int correction, int solverType, int tag)
{
  if (_ownData)
  {
    if (_reductionModel) delete _reductionModel;
    _reductionModel = NULL;
  }
  
  _ownData = true;
  bool withFrame = false;
  bool polarization = false;
  if (TFAMethodNum == 0)
  {
    Msg::Info("IncrementalTangent is used !!!");
    _reductionModel = new IncrementalTangentTFA(solverType, dim, correction, withFrame, polarization, tag);
  }
  else if(TFAMethodNum == 1)
  {
    withFrame = true;
    Msg::Info("IncrementalTangentWithFrame is used !!!");
    _reductionModel = new IncrementalTangentTFAWithFrame(solverType, dim, correction, withFrame, polarization, tag);
  }
  else if(TFAMethodNum == 2)
  {
    polarization = true;
    Msg::Info("IncrementalTangentTFAPolarization is used !!!");
    _reductionModel = new IncrementalTangentTFAPolarization(solverType, dim, correction, withFrame, polarization, tag);
  }
  else
  {
    Msg::Error("TFA method has not been implemented in mlawTFAMaterialLaws");
  }
}

void mlawTFAMaterialLaws::setClusterIncOrientation()
{
  if (_reductionModel!=NULL)
  {
    _reductionModel->setClusterIncOrientation();
  }
  else
  {
    Msg::Error("reduction model must be allocated by the function setTFAMethod first !!!");
    Msg::Exit(0);
  }
}


void mlawTFAMaterialLaws::constitutive(
            const STensor3& F0,         // initial deformation gradient (input @ time n)
            const STensor3& Fn,         // updated deformation gradient (input @ time n+1)
            STensor3 &P,                // updated 1st Piola-Kirchhoff stress tensor (output)
            const IPVariable *q0i,       // array of initial internal variable
            IPVariable *q1i,             // updated array of internal variable (in ipvcur on output),
            STensor43 &Tangent,         // constitutive tangents (output)
            const bool stiff,         // if true compute the tangents
            STensor43* elasticTangent,
            const bool dTangent,
            STensor63* dCalgdeps) const
{

  if (_reductionModel != NULL)
  {
    const IPTFA* q0 = static_cast<const IPTFA*>(q0i);
    IPTFA* q1 = static_cast<IPTFA*>(q1i);
    _reductionModel->evaluate(F0,Fn,P,q0->getClusteringData(),q1->getClusteringData(),Tangent,true,elasticTangent);
    if (dTangent)
    {
      Msg::Error("mlawTFAMaterialLaws::constitutive with dTangent option has not been implemented");
    }
  }
  else
  {
    Msg::Error("reduction model has not been created !!!");
  }
}

void mlawTFAMaterialLaws::createIPState(IPTFA *ivi, IPTFA *iv1, IPTFA *iv2) const
{
  Msg::Error("mlawTFAMaterialLaws::createIPState(IPTFA *ivi, IPTFA *iv1, IPTFA *iv2 cannot be used without Ele");
}

void mlawTFAMaterialLaws::createIPVariable(IPTFA *&ipv, bool hasBodyForce, const MElement *ele,const int nbFF, const IntPt *GP, const int gpt) const
{

  //find the clusters nb
  int clustersNb = _reductionModel->getNumberOfClusters();

  static std::vector<int> allCls;
  _reductionModel->getAllClusterIndices(allCls);
  
  static std::vector<double> allVolumes;
  _reductionModel->getAllClusterVolumes(allVolumes);
  //
  if (ipv != NULL)
    delete ipv;
  ipv=new IPTFA(allCls, allVolumes);

  for(int i=0; i<clustersNb; i++)
  {
    IPStateBase* ipslaw=NULL;
    const bool state_=true;
    _reductionModel->getMaterialLaw(allCls[i])->createIPState(ipslaw, hasBodyForce, &state_,ele, nbFF, GP, gpt);
    std::vector<IPVariable*> allIP;
    ipslaw->getAllIPVariable(allIP);
    ipv->getClusteringData()->getRefToIPv(allCls[i]) = allIP[0]->clone();
    delete ipslaw;
  }
}


void mlawTFAMaterialLaws::createIPState(IPStateBase* &ips,bool hasBodyForce, const bool* state_,const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
  //find the clusters nb
  int clustersNb=_reductionModel->getNumberOfClusters();
  static std::vector<int> allCls;
  _reductionModel->getAllClusterIndices(allCls);
  static std::vector<double> allVolumes;
  _reductionModel->getAllClusterVolumes(allVolumes);
  
  if(ips != NULL) delete ips;
  IPTFA *ipvi=new IPTFA(allCls,allVolumes);
  IPTFA *ipv1=new IPTFA(allCls,allVolumes);
  IPTFA *ipv2=new IPTFA(allCls,allVolumes);

  for(int i=0; i<clustersNb; i++)
  {
    IPStateBase* ipslaw=NULL;
    _reductionModel->getMaterialLaw(allCls[i])->createIPState(ipslaw, hasBodyForce, state_,ele, nbFF_, GP, gpt);
    std::vector<IPVariable*> allIP;
    ipslaw->getAllIPVariable(allIP);

    ipvi->getClusteringData()->getRefToIPv(allCls[i]) = allIP[0]->clone();
    ipv1->getClusteringData()->getRefToIPv(allCls[i]) = allIP[1]->clone();
    ipv2->getClusteringData()->getRefToIPv(allCls[i]) = allIP[2]->clone();
    delete ipslaw;
  }
  ips = new IP3State(state_,ipvi,ipv1,ipv2);
}


double mlawTFAMaterialLaws::soundSpeed() const
{
  double E = 2.*_mu*(1.+_nu);
  double factornu = (1.-_nu)/((1.+_nu)*(1.-2.*_nu));
  return sqrt(E*factornu/_rho);
}*/



