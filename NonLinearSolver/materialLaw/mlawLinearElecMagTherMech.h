//
// C++ Interface: material law
//              Linear Electro-Magneto-Thermo-Mechanical law
// Author:  <Vinayak GHOLAP>, (C) 2019
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef MLAWLINEARELECMAGTHERMECH_H_
#define MLAWLINEARELECMAGTHERMECH_H_
#include "SVector3.h"
#include "STensor3.h"
#include "STensor43.h"

#include "mlawLinearElecTherMech.h"
#include "ipLinearElecMagTherMech.h"



class mlawLinearElecMagTherMech : public mlawLinearElecTherMech
{
 protected:
  // Magnetic permeability directional components
  double _mu_x;
  double _mu_y;
  double _mu_z;
  // Initial magnetic vector potential components
  double _A0_x;
  double _A0_y;
  double _A0_z;
  
  // Magnitude of source current density imposed in inductor
  double _js0;
  double _freqEM;       // Frequency of input current in source inductor

  STensor3 _mu0;        // Magnetic permeability tensor
  STensor3 _nu0;        // Magnetic reluctivity tensor = inv(mu)

  bool _useFluxT;        // if true thermal problem formulated using heat flux q
                        // else using thermal energy flux jy

  // designed to be used for coupled simulations
  // between EM and TM solvers with internal state data transfer
  bool _evaluateCurlField; // if true curl field and relevant variables
                          // are computed (eg ThermalEMFieldSource, fluxje etc)
                          // else imported values from other solver used

  virtual double deformationEnergy(STensor3 defo,const SVector3 &gradV,const SVector3 &gradT,
  const double T, const double V) const ;

 public:
  mlawLinearElecMagTherMech(int num,const double rho, const double Ex, const double Ey, const double Ez,
  const double Vxy, const double Vxz, const double Vyz, const double MUxy, const double MUxz,
  const double MUyz,  const double alpha, const double beta, const double gamma,
  const double t0,const double Kx,const double Ky, const double Kz,const double alphax,
  const double alphay,const double alphaz, const  double lx,const  double ly,
  const  double lz,const double seebeck,const double cp,const double v0,
  const double mu_x, const double mu_y, const double mu_z, const double A0_x, const double A0_y,
  const double A0_z, const double Irms, const double freq, const unsigned int nTurnsCoil, 
  const double coilLength_x, const double coilLength_y, const double coilLength_z, const double coilWidth,
  const bool useFluxT,const bool evaluateCurlField = true);

   #ifndef SWIG
	mlawLinearElecMagTherMech(const mlawLinearElecMagTherMech &source);
	mlawLinearElecMagTherMech& operator=(const  materialLaw &source);
	virtual ~mlawLinearElecMagTherMech(){}
	virtual materialLaw* clone() const {return new mlawLinearElecMagTherMech(*this);}
	virtual bool withEnergyDissipation() const {return false;}
  // function of materialLaw
    virtual matname getType() const{return materialLaw::LinearElecMagTherMech;}
    virtual void createIPState(IPStateBase* &ips,bool hasBodyForce, const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0,
    const IntPt *GP=NULL, const int gpt =0) const;
    
  virtual void getNormalDirection(SVector3 &normal, const IPLinearElecMagTherMech *q1) const 
  {
    normal[0] = 0.0;
    normal[1] = 0.0;
    normal[2] = -1.0;
  }

   virtual void constitutive(
                            const STensor3& F0,         // initial deformation gradient (input @ time n)
                            const STensor3& Fn,         // updated deformation gradient (input @ time n+1)
                            STensor3 &P,                // updated 1st Piola-Kirchhoff stress tensor (output)
                                                        // contains the initial values on input
                            const IPLinearElecMagTherMech *q0,       // array of initial internal variable
                            IPLinearElecMagTherMech *q1,             // updated array of internal variable (in ipvcur on output),
                            STensor43 &Tangent,         // constitutive tangents (output)
                            double T0,
                            double T,
                            double V0,
                            double V,
                            const SVector3 &gradT,
                            const SVector3 &gradV,
                            SVector3 &fluxT,
                            SVector3 &fluxje,
                            STensor3 &dPdT,
                            STensor3 &dPdV,
                            STensor3 &dqdgradT,
                            SVector3 &dqdT,
                            STensor3 &dqdgradV,
                            SVector3 &dqdV,
                            SVector3 &dedV,
                            STensor3 &dedgradV,
                            SVector3 &dedT,
                            STensor3 &dedgradT,
                            STensor33 &dqdF,
                            STensor33 &dedF,
                            double &w_T,    //related to cp*dT/dt
                            double &dw_Tdt,
                            double &dw_TdV,
                            STensor3 &dw_Tdf,
                            double &wV,  //related to dV/dt
                            double &dwVdt,
                            double &dwVdV,
                            STensor3 &dwVdF,
                            STensor3 &l10,
                            STensor3 &l20,
                            STensor3 &k10,
                            STensor3 &k20,
                            STensor3 &dl10dT,
                            STensor3 &dl20dT,
                            STensor3 &dk10dT,
                            STensor3 &dk20dT,
                            STensor3 &dl10dv,
                            STensor3 &dl20dv,
                            STensor3 &dk10dv,
                            STensor3 &dk20dv,
                            STensor43 &dl10dF,
                            STensor43 &dl20dF,
                            STensor43 &dk10dF,
                            STensor43 &dk20dF,
                            SVector3 &fluxjy,
                            SVector3 &djydV,
                            STensor3 &djydgradV,
                            SVector3 &djydT,
                            STensor3 &djydgradT,
                            STensor33 &djydF,
                            STensor3 & djydA,
                            STensor3 & djydB,
                            STensor3 &djydgradVdT,
                            STensor3 &djydgradVdV,
                            STensor43 &djydgradVdF,
                            STensor3 &djydgradTdT,
                            STensor3 &djydgradTdV,
                            STensor43 &djydgradTdF,
                            const bool stiff,
                            const SVector3 & A0, // magnetic potential at time n
                            const SVector3 & An, // magnetic potential at time n+1
                            const SVector3 & B, // magneticinduction B = Curl A
                            SVector3 & H, // magnetic field H
                            SVector3 & js0, // current density applied in inductor
                            SVector3 & dBdT,
                            STensor3 & dBdGradT,
                            SVector3 & dBdV,
                            STensor3 & dBdGradV,
                            STensor33 & dBdF,
                            STensor3 & dBdA, // A magnetic vector potential
                            STensor3 & dfluxTdA,
                            STensor3 & dfluxjedA,
                            STensor3 & dfluxTdB,
                            STensor3 & dfluxjedB,
                            SVector3 &dw_TdA,
                            SVector3 & dw_TdB,
                            SVector3 & dmechSourcedB,
                            SVector3 & sourceVectorField, // Magnetic source vector field (T, grad V, F)
                            SVector3 & dsourceVectorFielddt,
                            double & ThermalEMFieldSource,
                            double & VoltageEMFieldSource,
                            STensor3 & dHdA,
                            STensor3 & dHdB,
                            STensor33 & dPdB,
                            STensor33 & dHdF,
                            STensor33 & dsourceVectorFielddF,
                            SVector3 & mechFieldSource, // Electromagnetic force
                            STensor3 & dmechFieldSourcedB,
                            STensor33 & dmechFieldSourcedF,
                            SVector3 & dHdT,
                            STensor3 & dHdgradT,
                            SVector3 & dHdV,
                            STensor3 & dHdgradV,
                            SVector3 & dsourceVectorFielddT,
                            STensor3 & dsourceVectorFielddgradT,
                            SVector3 & dsourceVectorFielddV,
                            STensor3 & dsourceVectorFielddgradV,
                            STensor3 & dsourceVectorFielddA,
                            STensor3 & dsourceVectorFielddB,
                            SVector3 & dmechFieldSourcedT,
                            STensor3 & dmechFieldSourcedgradT,
                            SVector3 & dmechFieldSourcedV,
                            STensor3 & dmechFieldSourcedgradV,
                            SVector3 & dThermalEMFieldSourcedA,
                            SVector3 & dThermalEMFieldSourcedB,
                            STensor3 & dThermalEMFieldSourcedF,
                            double & dThermalEMFieldSourcedT,
                            SVector3 & dThermalEMFieldSourcedGradT,
                            double & dThermalEMFieldSourcedV,
                            SVector3 & dThermalEMFieldSourcedGradV,
                            SVector3 & dVoltageEMFieldSourcedA,
                            SVector3 & dVoltageEMFieldSourcedB,
                            STensor3 & dVoltageEMFieldSourcedF,
                            double & dVoltageEMFieldSourcedT,
                            SVector3 & dVoltageEMFieldSourcedGradT,
                            double & dVoltageEMFieldSourcedV,
                            SVector3 & dVoltageEMFieldSourcedGradV
                            ) const;
    double getInitialTemperature() const {return _t0;}
    double getInitialVoltage()     const {return _v0;}
    SVector3 getInitialMagPotential() const {return SVector3(_A0_x, _A0_y, _A0_z);}
    virtual bool isInductor() const {return false;}
 #endif // SWIG
};

#endif // MLAWELECMAGTHERMOMECH_H_

