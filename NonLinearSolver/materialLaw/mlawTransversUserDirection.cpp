//
// C++ Interface: material law for user direction
//
// Author:  <Van Dung NGUYEN (C) 2021>
//
// Copyright: See COPYING file that comes with this distribution
//


#include "mlawTransversUserDirection.h"
#include "ipTransverseIsoUserDirection.h"

mlawTransverseIsoUserDirection::mlawTransverseIsoUserDirection(const int num,const double E,const double nu, const double rho, 
                  const double EA, const double GA, const double nu_minor, const UserDirectionGenerator& dirGen): mlawTransverseIsotropic(num,E,nu, rho, EA, GA, nu_minor, 0., 0., 0.)
{
  _dirGenerator = dirGen.clone();
};

mlawTransverseIsoUserDirection::mlawTransverseIsoUserDirection(const mlawTransverseIsoUserDirection& src): mlawTransverseIsotropic(src)
{
  _dirGenerator = NULL;
  if (src._dirGenerator != NULL)
  {
    _dirGenerator = src._dirGenerator->clone();
  }
}

mlawTransverseIsoUserDirection::~mlawTransverseIsoUserDirection()
{
  if (_dirGenerator != NULL)
  {
    delete _dirGenerator;
    _dirGenerator = NULL;
  }
};
    
    

void mlawTransverseIsoUserDirection::createIPState(IPStateBase* &ips, bool hasBodyForce, const bool* state_, const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
  /*
  bool inter=true;
  const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
  if(iele==NULL) inter=false;
  */
  SPoint3 pt_Global;
  ele->pnt(GP->pt[0],GP->pt[1],GP->pt[2],pt_Global);
  SVector3 GaussP(pt_Global);
  
  SVector3 DIR;
  if (_dirGenerator == NULL)
  {
    Msg::Error("direction generator must be defined in mlawTransverseIsoUserDirection !!!");
    Msg::Exit(0);
  }
  else
  {
    _dirGenerator->getTransverseDirection(DIR,GaussP);
  }
  DIR.normalize();
  
  IPVariable* ipvi = new IPTransverseIsoUserDirection(DIR);
  IPVariable* ipv1 = new IPTransverseIsoUserDirection(DIR);
  IPVariable* ipv2 = new IPTransverseIsoUserDirection(DIR);
  if(ips != NULL) delete ips;
  ips = new IP3State(state_,ipvi,ipv1,ipv2);
};
