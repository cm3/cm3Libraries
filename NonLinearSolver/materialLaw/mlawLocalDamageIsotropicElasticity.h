//
// C++ Interface: material law
//
// Description: local damage law
//
// Author:  <V-D Nguyen>, (C) 2021
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef MLAWLOCALDAMAGEISOTROPICELASTICITY_H_
#define MLAWLOCALDAMAGEISOTROPICELASTICITY_H_
#include "mlaw.h"
#include "STensor3.h"
#include "STensor43.h"
#include "ipNonLocalDamageIsotropicElasticity.h"
#include "DamageLaw.h"


class mlawLocalDamageIsotropicElasticity : public materialLaw
{
  protected:
    double _rho;                        // Density
    double _E;                          // Young modulus
    double _nu;                         // Poisson ratio
    STensor43 _ElasticityTensor;        // Elasticity tensor
    DamageLaw *_damLaw;                 // Law for damage evolution

  public:
    // Constructors & Destructors
    mlawLocalDamageIsotropicElasticity(const int num, const double rho, const double E,const double nu, const DamageLaw &damLaw);

    #ifndef SWIG
    mlawLocalDamageIsotropicElasticity(const mlawLocalDamageIsotropicElasticity &source);
    virtual ~mlawLocalDamageIsotropicElasticity()
    {
      if(_damLaw != NULL) delete _damLaw; _damLaw = NULL;
    }
		virtual materialLaw* clone() const {return new mlawLocalDamageIsotropicElasticity(*this);};

		virtual void checkInternalState(IPVariable* ipv, const IPVariable* ipvprev) const{}; // do nothing

    // General functions
    virtual matname getType() const {return materialLaw::localDamageIsotropicElasticity;}
    virtual bool withEnergyDissipation() const {return true;};
    virtual void createIPState(IPStateBase* &ips,bool hasBodyForce,const bool* state_ = NULL,const MElement *ele = NULL, const int nbFF_ = 0, const IntPt *GP=NULL, const int gpt=0) const;
    virtual void createIPState(IPLocalDamageIsotropicElasticity *ivi, IPLocalDamageIsotropicElasticity *iv1, IPLocalDamageIsotropicElasticity *iv2) const;
    virtual void createIPVariable(IPLocalDamageIsotropicElasticity *&ipv) const;
    virtual void initLaws(const std::map<int,materialLaw*> &maplaw) {}; // this law is initialized so nothing to do
    virtual double density() const {return _rho;};
    virtual double soundSpeed() const;
		virtual double scaleFactor() const{return _E/(2.*(1.+_nu));}
    virtual const DamageLaw *getDamageLaw() const{return _damLaw;};
      // Constitutive law
    virtual void constitutive(
                            const STensor3& F0,         // initial deformation gradient (input @ time n)
                            const STensor3& Fn,         // updated deformation gradient (input @ time n+1)
                            STensor3 &P,                // updated 1st Piola-Kirchhoff stress tensor (output)
                                                        // contains the initial values on input
                            const IPVariable *q0,       // array of initial internal variable
                            IPVariable *q1,             // updated array of internal variable (in ipvcur on output),
                            STensor43 &Tangent,         // constitutive tangents (output)
                            const bool stiff,            // if true compute the tangents
                            STensor43* elasticTangent = NULL, 
                            const bool dTangent =false,
                            STensor63* dCalgdeps = NULL) const;
   
  protected:
    virtual double deformationEnergy(const STensor3 &epsilon_strains, const STensor3 &sigma) const;   // Compute elastic energy

    #endif // SWIG
};


#endif //MLAWLOCALDAMAGEISOTROPICELASTICITY_H_
