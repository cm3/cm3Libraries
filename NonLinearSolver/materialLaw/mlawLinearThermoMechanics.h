//
// C++ Interface: material law
//              Linear Thermo Mechanical law
// Author:  <L. Homsy>, (C) 2013
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef MLAWLINEARTHERMOMECHANICS_H_
#define MLAWLINEARTHERMOMECHANICS_H_
#include "mlaw.h"
#include "STensor3.h"
#include "STensor43.h"
#include "ipLinearThermoMechanics.h"
#include "scalarFunction.h"

class mlawLinearThermoMechanics : public materialLaw
{
 protected:
  // can't be const due to operator= constructor (HOW TO CHANGE THIS ??)
  double _rho; // density
  STensor43 _ElasticityTensor;
  double _poissonMax;
  double _alpha; // parameter of anisotropy
  double _beta; // parameter of anisotropy
  double _gamma; // parameter of anisotropy
  double _cv;
  double _t0;
  double _Kx;
  double _Ky;
  double _Kz;
  double _alphax;
  double _alphay;
  double _alphaz;

  scalarFunction* _cp; // function of temperature

  STensor3 _k;
  STensor3 _alphaDilatation;
  STensor3 _Stiff_alphaDilatation;


#ifndef SWIG
 private: // cache data for constitutive function
  //mutable STensor3 defo_g,defo_l,sigma_g,sigma_l;
 private: // cache data for update function
  mutable STensor3 S,Str,Siso;
 private: // cache data for tangent function
  mutable STensor43 M,Mtr,Miso;
#endif // SWIG
 public:
  mlawLinearThermoMechanics(const int num,const double rho, const double Ex, const double Ey, const double Ez,
						  const double Vxy, const double Vxz, const double Vyz,
						  const double MUxy, const double MUxz, const double MUyz,
			 			  const double alpha, const double beta, const double gamma  ,
			                   const double t0,const double Kx,const double Ky,
			                  const double Kz,const double alphax,const double alphay,const double alphaz,const double cp);
  virtual void setLawForCp(const scalarFunction& funcCp);
 #ifndef SWIG
  mlawLinearThermoMechanics(const mlawLinearThermoMechanics &source);
  mlawLinearThermoMechanics& operator=(const materialLaw &source);
	virtual ~mlawLinearThermoMechanics(){
		if (_cp != NULL){
			delete _cp; _cp = NULL;
		}
	}
	virtual materialLaw* clone() const {return new mlawLinearThermoMechanics(*this);};
	virtual void checkInternalState(IPVariable* ipv, const IPVariable* ipvprev) const{}; // do nothing
  virtual bool withEnergyDissipation() const {return false;};
  // function of materialLaw
  virtual matname getType() const{return materialLaw::LinearThermoMechanics;}
  virtual void createIPState(IPStateBase* &ips, bool hasBodyForce, const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const IntPt *GP=NULL, const int gpt =0) const;
  virtual void initLaws(const std::map<int,materialLaw*> &maplaw){}; // this law is initialized so nothing to do
  virtual double poissonRatio() const;
  virtual double shearModulus() const;
  virtual double soundSpeed() const; // default but you can redefine it for your case
public:
  virtual void constitutive(
                            const STensor3& F0,         // initial deformation gradient (input @ time n)
                            const STensor3& Fn,         // updated deformation gradient (input @ time n+1)
                            STensor3 &P,                // updated 1st Piola-Kirchhoff stress tensor (output)
                                                        // contains the initial values on input
                            const IPVariable *q0,       // array of initial internal variable
                            IPVariable *q1,              // updated array of internal variable (in ipvcur on output),
			    STensor43 &Tangent,         // constitutive tangents (output)
			    const bool stiff,
			    STensor43* elasticTangent = NULL, 
                            const bool dTangent =false,
                            STensor63* dCalgdeps = NULL) const;
  virtual void constitutive(
                            const STensor3& F0,         // initial deformation gradient (input @ time n)
                            const STensor3& Fn,         // updated deformation gradient (input @ time n+1)
                            STensor3 &P,                // updated 1st Piola-Kirchhoff stress tensor (output)
                                                        // contains the initial values on input
                            const IPVariable *q0,       // array of initial internal variable
                            IPVariable *q1,             // updated array of internal variable (in ipvcur on output),
                            STensor43 &Tangent,         // constitutive tangents (output)
                            double T0,
			    double T,
  			    const SVector3 &gradT,
                            SVector3 &fluxT,
                            STensor3 &dPdT,
                            STensor3 &dqdgradT,
                            SVector3 &dqdT,
                            STensor33 &dqdF,
			   // double h,   //time
			    double &w,   //cp*dTdh
			    double &dwdt,
			    STensor3 &dwdf,
			    //STensor3 &Stiff_alphaDilatation,
                            const bool stiff,            // if true compute the tangents
                            STensor43* elasticTangent = NULL) const;

  const STensor43& getElasticityTensor() const { return _ElasticityTensor;};
  virtual const STensor3&  getConductivityTensor() const { return _k;};
  virtual const STensor3& getStiff_alphaDilatation()const { return _Stiff_alphaDilatation;};
  virtual double getInitialTemperature() const {return _t0;};
  virtual double getInitialExtraDofStoredEnergyPerUnitField() const {return getExtraDofStoredEnergyPerUnitField(_t0);}
  virtual double getExtraDofStoredEnergyPerUnitField(double T) const {return _cp->getVal(T);};
 protected:
  virtual double deformationEnergy(STensor3 defo) const ;
  virtual double deformationEnergy(STensor3 defo, const STensor3& sigma) const ;
  virtual double deformationEnergy(STensor3 defo,const SVector3 &gradT) const ;

 #endif // SWIG
};

#endif // MLAWLINEARTHERMOMECHANICS_H_
