//
// C++ Interface: terms
//
// Description: Define Gurson nucleation
//
//
// Author:  <L. Noels>, (C) 2013
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef _NUCLEATIONLAW_H_
#define _NUCLEATIONLAW_H_
#ifndef SWIG
#include "ipstate.h"
#include "MElement.h"
#include "ipNucleation.h"
#endif

#include "STensorOperations.h"
#include "nucleationFunctionLaw.h"
#include "nucleationCriterionLaw.h"


/*! \class NucleationLaw
 * This class manages the different nucleation function and criterion
 * for the strain-controlled nucleation and used in controlled.
 * It determines which nucleation function law should be activated
 * depending on the corresponding criterion. Then, the contributions are added.
 * 
 * The nucleation components are first empty and are then added by the user through the 
 * mlawPorous interface. Exclusion rules are added by the user before adding the criterion law.
 * 
 * Once all the components are added, the nucleation needs to be initialised 
 * (and at the same time, the integrety of the data are checked.)
 * 
 * 
 */

class NucleationLaw
{
#ifndef SWIG
protected :
  //! identifier of the internal law
  int num_;
  
  //! is true once the internal law is correctly initialised.
  bool isInitialized_;
  
  //! number of nucleation components.
  //! It corresponds to the length of both container vectors
  int nNucleationLaw_;
  
  //! vector gathering the nucleation function pointers.
  //! Its length is always equal to nNucleationLaw_
  std::vector< NucleationFunctionLaw* > nuclFunctionVector_;
  
  //! vector gathering the nucleation criteiron pointers
  //! Its length is always equal to nNucleationLaw_
  std::vector< NucleationCriterionLaw* > nuclCriterionVector_;

#endif // SWIG


public:
  //! @name Constructors and destructors
  NucleationLaw( const int num );
  //! @name Setting functions used in Python interface
  //! It corresponds to non-const functions used to set internal law parameters,
  //! using the python user interface.

  //! Clean all the nucleation components
  virtual void clearAllNucleationComponents();
  //! Add one nucleation function with an associated criterion.
  //! If the criterion is not provided, it is replaced by a trivial one (which is always true).
  virtual void addOneNucleationComponent( const NucleationFunctionLaw& nuclFunction, const NucleationCriterionLaw* const nuclCriterion = NULL );
  //! @}
  
#ifndef SWIG
  
  NucleationLaw( const NucleationLaw &source );
  virtual NucleationLaw& operator=(const NucleationLaw &source);
  virtual ~NucleationLaw();
  virtual NucleationLaw* clone() const { return new NucleationLaw(*this); };
  //! @}
  
  
  //! @name General functions of materialLaw
  virtual int getNum() const { return num_; };
  virtual void createIPVariable( IPNucleation* &ipNucl ) const;
  virtual const bool isInitialized() const { return isInitialized_; };
 
  //! @name Setting functions used in internal interface
  //! It corresponds to non-const functions used outside SWIG interface,
  //! to set or exchange internal parameters or initialise the laws.

  //! Check the integrity of the parameters and finalise the initialisation
  virtual void initNucleationLaw( const mlawNonLocalPorosity * const mlawPorous );
  
  //! @}
  
  //! @name Access functions
  //! Get the number of nucleation components
  virtual int getNumberOfNucleationComponents() const;
  
  //! Get the i-th NucleationFunctionLaw
  virtual const NucleationFunctionLaw&  getConstRefToNucleationFunctionLaw( const int i ) const;
  //! Get the i-th NucleationCriterionLaw
  virtual const NucleationCriterionLaw& getConstRefToNucleationCriterionLaw( const int i ) const;

  // These functions should not used because nothing has to modify the components ouside the nucleation manager
  // virtual NucleationFunctionLaw&       getRefToNucleationFunctionLaw( const int i) const{};
  // virtual NucleationCriterionLaw&       getRefToNucleationCriterionLaw( const int i) const{};
  
  //! @}
  
  
  //! @name Specific functions
  //! Compute the nucleated porosity from the plastic strain evolution
  virtual void nucleate( const double p, const double p0, IPNonLocalPorosity& q1Porous, const IPNonLocalPorosity& q0Porous ) const;
  
  //! Update the state of each nucleation criterion
  virtual void checkNucleationActivation( IPNonLocalPorosity& q1Porous, const IPNonLocalPorosity& q0Porous ) const;
  
  //! Reset the variables / refresh the ipv to the adequate values at the begining of a time step / iteration.
  inline virtual void previousBasedRefresh( IPNucleation& q1Nucl, const IPNucleation& q0Nucl ) const
  {
    q1Nucl.getRefToNucleatedPorosity() = q0Nucl.getNucleatedPorosity();
    q1Nucl.getRefToNucleatedPorosityIncrement() = 0.0;
    q1Nucl.getRefToDNucleatedPorosityDMatrixPlasticDeformation() = 0.0;
    
    for (int it_law = 0; it_law < nNucleationLaw_; it_law++ ){
      nuclFunctionVector_[it_law]->previousBasedRefresh( q1Nucl.getRefToIPNucleationFunction( it_law ),
                                                         q0Nucl.getConstRefToIPNucleationFunction( it_law ) );
      
      nuclCriterionVector_[it_law]->previousBasedRefresh( q1Nucl.getRefToIPNucleationCriterion( it_law ),
                                                          q0Nucl.getConstRefToIPNucleationCriterion( it_law ) );
    };
  };
  
protected :
  //! Check it the number of nucleation function corresponds to the number of criterion.
  //! This function is called almost each time for non-const function.
  virtual void checkNumberOfNucleationLaw() const;
  //! @}
#endif
};


/*! \class LinearNucleationLaw
 * Class-function to preserve the old interface. 
 * Involves a constant nucleation rate limited by the porosity value.
 */
class LinearNucleationLaw : public NucleationLaw
{
  public:
    LinearNucleationLaw(const int num, const double A0, const double fmin=0., const double fmax=1.);
    #ifndef SWIG
    LinearNucleationLaw(const LinearNucleationLaw& src) : NucleationLaw(src){};
    virtual ~LinearNucleationLaw(){};
    virtual NucleationLaw* clone() const { return new LinearNucleationLaw(*this); };
    #endif //SWIG
};



/*! \class ExponentialNucleationLaw
 * Class-function to preserve the old interface. 
 * Involves a Gaussian rate law.
 */
class ExponentialNucleationLaw: public NucleationLaw
{
  public:
    ExponentialNucleationLaw(const int num, double fn, double sn, double epsilonn);
    #ifndef SWIG
    ExponentialNucleationLaw(const ExponentialNucleationLaw& src):NucleationLaw(src){};
    virtual ~ExponentialNucleationLaw(){};
    virtual NucleationLaw* clone() const { return new ExponentialNucleationLaw(*this); };
    #endif //SWIG
};

#endif //_NUCLEATIONLAW_H_
