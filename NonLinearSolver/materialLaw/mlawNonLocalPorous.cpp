// C++ Interface: material law
//
// Description: general class for nonlocal porous material law
//
// Author:  <V.D. Nguyen>, (C) 2018
//
// Copyright: See COPYING file that comes with this distribution
//
//

#include "mlawNonLocalPorous.h"
#include "STensorOperations.h"
#include "nonLinearMechSolver.h"
#include "ipNonLocalPorosity.h"

mlawNonLocalPorosity::mlawNonLocalPorosity(const int num,const double E,const double nu, const double rho,
                           const double fVinitial, const J2IsotropicHardening &j2IH, const CLengthLaw &cLLaw,
                           const double tol, const bool matrixbyPerturbation, const double pert) :
   materialLaw(num, false),
   nonlocalMethod_(NONLOCAL_POROSITY),  _numNonLocalVar(1),
   _stressFormulation(CORO_CAUCHY), _nucleationDuringCoalesIsON(false), 
  _postDamageBlockingBehavior(ELASTOPLASTIC),
  _thermomechanicsCoupling(false), _thermalEstimationPreviousConfig(true),

  _j2IH(NULL), _viscoplastlaw(NULL), _plasticInstabilityFunctionLaw(NULL), _nucleationLaw(NULL),
  
  _rho( rho ), _tempFunc_rho(NULL), _E( E ), _tempFunc_E(NULL), _nu( nu ), _tempFunc_nu(NULL),
  _lambda( ( _E*_nu ) / ( 1.0+_nu ) / ( 1.0-2.0*_nu ) ),
  _mu( 0.5*_E/( 1.0+_nu ) ),  _mu2( 2.0*_mu ), _K( _E/ 3.0 / (1.0-2.0*_nu) ), _K3( 3.0*_K ),
  _Tref(298.), _cp(0.0), _tempFunc_cp(NULL), _taylorQuineyFactor(0.9), 
  _alphaDilatation(0.0), _tempFunc_alphaDilatation(NULL), Stiff_alphaDilatation(0.0), 
  _thermalConductivity(0.0), _tempFunc_thermalConductivity(NULL), linearK(0.0),
  
  _fVinitial(fVinitial), _randMinbound(1.0), _randMaxbound(1.0),
  _fV_failure(1.0), _localfVFailure(0.99), _localfVMin( fVinitial * 0.1 ), _regularisationFunc_porosityEff(NULL),
  
  _kw(0.0), _shearGrowth_triaxFunc(NULL), _shearGrowth_LodeVarFunc(NULL),
  
  _orderlogexp(1), _tol(tol), _maxite(50), _tangentByPerturbation(matrixbyPerturbation), _perturbationfactor(pert),
  _failedTol(0.999),
  _withSubstepping(false), _maxAttemptSubstepping(1), _numofSubStep(10),  
  
  _I(1.0), _I4(1.0, 1.0), _I4vol(0.0,0.0), _I4dev(0.0, 0.0),
  _dpressure_dStress(0.0)
       
{

  _cLLaw.clear();               // clear cl law before adding one
  _cLLaw.push_back(cLLaw.clone());

  /// 1. Construction of the internal objects:
  /// - The j2-hardening law (a copy of the argument is performed).
  _j2IH  = j2IH.clone();
  
  /// - The visco-plastic law (no viscoplastic effect considered by default).
  _viscoplastlaw = NULL;
  
  /// - The plastic instability function (no plastic instability considered by default).
  _plasticInstabilityFunctionLaw = NULL;
  
  /// - The nucleation law (by default, no nucleation is considered through the default case).
  _nucleationLaw = new NucleationLaw( num );
  
  /// 2. Initialise the temperature dependence function (constant function by default)
  _tempFunc_rho = new constantScalarFunction( 1.0 );
  _tempFunc_E   = new constantScalarFunction( 1.0 );
  _tempFunc_nu  = new constantScalarFunction( 1.0 );
  _tempFunc_cp  = new constantScalarFunction( 1.0 );
  _tempFunc_alphaDilatation = new constantScalarFunction( 1.0 );
  _tempFunc_thermalConductivity = new constantScalarFunction( 1.0 );

  /// 3. Initialise the (not yet initialised) numerical tools.
  STensorOperation::diag( _dpressure_dStress, 1.0/3.0 );
  _I4dev = _I4;
  tensprod( _I, _dpressure_dStress, _I4vol );
  
  _I4dev -= _I4vol;
  
  
  /// 4. Initialise other parameters.
  // Fill the elastic tensor. (Set everything to zero then add elastic contributions).
  _Cel = 0.0;
  _Cel(0,0,0,0) = _lambda + _mu2;
  _Cel(1,1,0,0) = _lambda;
  _Cel(2,2,0,0) = _lambda;
  _Cel(0,0,1,1) = _lambda;
  _Cel(1,1,1,1) = _lambda + _mu2;
  _Cel(2,2,1,1) = _lambda;
  _Cel(0,0,2,2) = _lambda;
  _Cel(1,1,2,2) = _lambda;
  _Cel(2,2,2,2) = _lambda + _mu2;

  _Cel(1,0,1,0) = _mu;
  _Cel(2,0,2,0) = _mu;
  _Cel(0,1,0,1) = _mu;
  _Cel(2,1,2,1) = _mu;
  _Cel(0,2,0,2) = _mu;
  _Cel(1,2,1,2) = _mu;

  _Cel(0,1,1,0) = _mu;
  _Cel(0,2,2,0) = _mu;
  _Cel(1,0,0,1) = _mu;
  _Cel(1,2,2,1) = _mu;
  _Cel(2,0,0,2) = _mu;
  _Cel(2,1,1,2) = _mu;
  
  /// Set regularisation function for the yield porosity
  _regularisationFunc_porosityEff = new fourVariableExponentialSaturationScalarFunction( _localfVMin * 2.0, _localfVMin, 0.9*_fV_failure,0.999*_fV_failure);
  
  
  /// 5. Initialise the default shear-induced growth.
  /// By default, the function proposed by [Nashon2008] is used:
  /// \f$  \f$ \dot f_{V,shear} = _kw * (1-xi^2) * \dot d \f$. \f$,
  /// (i.e., with no triaxiality dependence, contribution at max. in shear, plane strain and min. in axisym. conditions).
  _shearGrowth_LodeVarFunc = new polynomialScalarFunction(2); 
  polynomialScalarFunction* polyL = static_cast<polynomialScalarFunction*>(_shearGrowth_LodeVarFunc);
  polyL->setCoefficient(0,1.);
  polyL->setCoefficient(1,0.);
  polyL->setCoefficient(2,-1.);

  _shearGrowth_triaxFunc = new constantScalarFunction(1.);
}

mlawNonLocalPorosity::mlawNonLocalPorosity( const mlawNonLocalPorosity& source ) :
        materialLaw(source),
  nonlocalMethod_(source.nonlocalMethod_), _numNonLocalVar(source._numNonLocalVar),
  _stressFormulation(source._stressFormulation), 
  _nucleationDuringCoalesIsON(source._nucleationDuringCoalesIsON),
  _postDamageBlockingBehavior(source._postDamageBlockingBehavior), _thermomechanicsCoupling(source._thermomechanicsCoupling),
  _thermalEstimationPreviousConfig(source._thermalEstimationPreviousConfig),
  _rho( source._rho ), _E( source._E ), _nu( source._nu ),
  _lambda( source._lambda ),
  _mu( source._mu ), _mu2( source._mu2 ), _K( source._K ), _K3( source._K3 ), _Cel(source._Cel),
  _Tref( source._Tref ), _cp (source._cp ), _taylorQuineyFactor( source._taylorQuineyFactor ), 
  _alphaDilatation(source._alphaDilatation), Stiff_alphaDilatation( source.Stiff_alphaDilatation ),
  _thermalConductivity( source._thermalConductivity ), linearK( source.linearK ),
  
  _fVinitial( source._fVinitial ), _randMinbound( source._randMinbound ), _randMaxbound( source._randMaxbound ),
  _fV_failure( source._fV_failure ), _localfVFailure( source._localfVFailure ), _localfVMin( source._localfVMin ),
  _kw( source._kw ),
  
  _orderlogexp( source._orderlogexp), _tol( source._tol), _maxite( source._maxite), _tangentByPerturbation( source._tangentByPerturbation), _perturbationfactor( source._perturbationfactor),
  _failedTol(source._failedTol),
  _withSubstepping( source._withSubstepping), _maxAttemptSubstepping( source._maxAttemptSubstepping), _numofSubStep( source._numofSubStep),
    
  _I(source._I), _I4(source._I4), _I4vol(source._I4vol), _I4dev(source._I4dev),
  _dpressure_dStress(source._dpressure_dStress)                                                                          
{
   
  _cLLaw.resize(source._cLLaw.size());
  for (int i=0; i< source._cLLaw.size(); i++){
    if (source._cLLaw[i] != NULL){
      _cLLaw[i] = source._cLLaw[i]->clone();
    }
  }
  
  _j2IH = NULL;
  if ( source._j2IH != NULL ) _j2IH = source._j2IH->clone();
  
  _viscoplastlaw = NULL;
  if ( source._viscoplastlaw != NULL ) _viscoplastlaw = source._viscoplastlaw->clone();
  
  _nucleationLaw = NULL;
  if ( source._nucleationLaw != NULL ) _nucleationLaw = source._nucleationLaw->clone();
  
  _plasticInstabilityFunctionLaw = NULL;
  if ( source._plasticInstabilityFunctionLaw != NULL ) _plasticInstabilityFunctionLaw = source._plasticInstabilityFunctionLaw->clone();
  
  _tempFunc_rho = NULL;
  if (source._tempFunc_rho!= NULL)  _tempFunc_rho = source._tempFunc_rho->clone();
  
  _tempFunc_E = NULL;
  if ( source._tempFunc_E != NULL ) _tempFunc_E = source._tempFunc_E->clone();
  
  _tempFunc_nu = NULL;
  if ( source._tempFunc_nu != NULL ) _tempFunc_nu = source._tempFunc_nu->clone();
  
  _tempFunc_cp = NULL;
  if ( source._tempFunc_cp != NULL ) _tempFunc_cp = source._tempFunc_cp->clone();
  
  _tempFunc_alphaDilatation = NULL;
  if ( source._tempFunc_alphaDilatation != NULL ) _tempFunc_alphaDilatation = source._tempFunc_alphaDilatation->clone();
  
  _tempFunc_thermalConductivity = NULL;
  if ( source._tempFunc_thermalConductivity != NULL ) _tempFunc_thermalConductivity = source._tempFunc_thermalConductivity->clone();
  
  _shearGrowth_triaxFunc = NULL;
  if ( source._shearGrowth_triaxFunc != NULL ) _shearGrowth_triaxFunc = source._shearGrowth_triaxFunc->clone();
  
  _shearGrowth_LodeVarFunc = NULL;
  if ( source._shearGrowth_LodeVarFunc != NULL ) _shearGrowth_LodeVarFunc = source._shearGrowth_LodeVarFunc->clone();

  _regularisationFunc_porosityEff = NULL;
  if ( source._regularisationFunc_porosityEff != NULL ) _regularisationFunc_porosityEff = source._regularisationFunc_porosityEff->clone();
};

mlawNonLocalPorosity::~mlawNonLocalPorosity()
{
  /// Destructor: delete all included objects:
  if ( _j2IH != NULL ) delete _j2IH; _j2IH = NULL;
  if ( _viscoplastlaw != NULL ) delete _viscoplastlaw; _viscoplastlaw = NULL;
  if ( _nucleationLaw != NULL ) delete _nucleationLaw; _nucleationLaw = NULL;
  if ( _plasticInstabilityFunctionLaw != NULL ) delete _plasticInstabilityFunctionLaw; _plasticInstabilityFunctionLaw = NULL;
  
  if ( _tempFunc_rho != NULL ) delete _tempFunc_rho; _tempFunc_rho = NULL;
  if ( _tempFunc_E != NULL )   delete _tempFunc_E; _tempFunc_E = NULL;
  if ( _tempFunc_nu != NULL )  delete _tempFunc_nu; _tempFunc_nu = NULL;
  if ( _tempFunc_cp != NULL )  delete _tempFunc_cp; _tempFunc_cp = NULL;
  if ( _tempFunc_alphaDilatation != NULL )  delete _tempFunc_alphaDilatation; _tempFunc_alphaDilatation = NULL;
  if ( _tempFunc_thermalConductivity != NULL )  delete _tempFunc_thermalConductivity; _tempFunc_thermalConductivity = NULL;
  
  if ( _shearGrowth_triaxFunc != NULL )  delete _shearGrowth_triaxFunc; _shearGrowth_triaxFunc = NULL;
  if ( _shearGrowth_LodeVarFunc != NULL )  delete _shearGrowth_LodeVarFunc; _shearGrowth_LodeVarFunc = NULL;
  if ( _regularisationFunc_porosityEff != NULL )  delete _regularisationFunc_porosityEff; _regularisationFunc_porosityEff = NULL;
  // Internal laws
  for (int i=0; i< _cLLaw.size(); i++){
    if(_cLLaw[i]!= NULL) delete _cLLaw[i]; _cLLaw[i] = NULL;
  }
}

double mlawNonLocalPorosity::soundSpeed() const
{
  double factornu = (1.-_nu)/((1.+_nu)*(1.-2.*_nu));
  return sqrt(_E*factornu/_rho);
};






void mlawNonLocalPorosity::setStressFormulation( const int stress_formu )
{
  _stressFormulation = ( stressFormumation ) stress_formu;
  
  if ( _stressFormulation == CORO_CAUCHY )
    Msg::Info( "generalmlawPorous: Corotational Cauchy stress is used for the yield surface." );
  else if ( _stressFormulation == CORO_KIRCHHOFF )
    Msg::Info( "generalmlawPorous: Corotational Kirchhoff stress is used for the yield surface." );
  else
    Msg::Error( "generalmlawPorous: Wrong setting in generalmlawPorous::setStressFormulation." );
};

void mlawNonLocalPorosity::setPostBlockedDissipationBehavior( const int method )
{
  _postDamageBlockingBehavior = (postBlockBehaviour) method;
  if ( method == ELASTIC ) {
     Msg::Info("generalmlawPorous::setPostBlockedDissipationBehavior: after blocking damage, elastic behaviour is used.");
  }
  else if ( method == ELASTOPLASTIC ) {
     Msg::Info("generalmlawPorous::setPostBlockedDissipationBehavior: after blocking damage, elastoplastic behaviour is used.");
  }
  else {
    Msg::Error("generalmlawPorous::setPostBlockedDissipationBehavior: method %d is not implemented !",method);
  }
};


void mlawNonLocalPorosity::activateNucleationDuringCoalescence( const bool nuclAfterCoalesIsON )
{
  _nucleationDuringCoalesIsON = nuclAfterCoalesIsON;
  if ( _nucleationDuringCoalesIsON ) Msg::Info( "activate void nucleation during void coalescence" );
  else Msg::Info( "no void nucleation during void coalescence" );
};


void mlawNonLocalPorosity::setViscoPlasticLaw( const viscosityLaw& viscoPlasticLaw )
{
  if ( _viscoplastlaw != NULL ) delete _viscoplastlaw;
  _viscoplastlaw = viscoPlasticLaw.clone();
};

void mlawNonLocalPorosity::setPlasticInstabilityFunctionLaw( const PlasticInstabilityFunctionLaw &plIns )
{
 if ( _plasticInstabilityFunctionLaw != NULL ) delete _plasticInstabilityFunctionLaw;
 _plasticInstabilityFunctionLaw = plIns.clone();
};


void mlawNonLocalPorosity::setNucleationLaw( const NucleationFunctionLaw& added_function, const NucleationCriterionLaw* added_criterion )
{
  _nucleationLaw->addOneNucleationComponent(added_function, added_criterion);
};

void mlawNonLocalPorosity::setNucleationLaw( const NucleationLaw& nuclLaw )
{
  if ( _nucleationLaw !=NULL ) delete _nucleationLaw;
  _nucleationLaw = nuclLaw.clone();
}

void mlawNonLocalPorosity::setThermomechanicsCoupling( const bool thermoMechCouplingIsON )
{
  _thermomechanicsCoupling = thermoMechCouplingIsON;
  if ( _thermomechanicsCoupling ) Msg::Info( "generalmlawPorous::setThermomechanicsCoupling: the thermomechanical coupling is turned ON." );
  else Msg::Info( "generalmlawPorous::setThermomechanicsCoupling: the thermomechanical coupling is turned OFF." );
};

void mlawNonLocalPorosity::setThermalEstimationPreviousConfig( const bool thermoMechIsBasedOnPrevious )
{
  _thermalEstimationPreviousConfig = thermoMechIsBasedOnPrevious;
  if ( _thermalEstimationPreviousConfig ) Msg::Info( "generalmlawPorous::setThermalEstimationPreviousConfig: the thermomechanical coupling is based on the previous config." );
  else Msg::Info( "generalmlawPorous::setThermalEstimationPreviousConfig: the thermomechanical coupling is based on the current config." );
};


void mlawNonLocalPorosity::setTemperatureFunction_rho( const scalarFunction& Tfunc )
{
  if ( _tempFunc_rho != NULL ) delete _tempFunc_rho;
  _tempFunc_rho = Tfunc.clone();
};

void mlawNonLocalPorosity::setTemperatureFunction_E( const scalarFunction& Tfunc )
{
  if ( _tempFunc_E != NULL ) delete _tempFunc_E;
  _tempFunc_E = Tfunc.clone();
};

void mlawNonLocalPorosity::setTemperatureFunction_nu( const scalarFunction& Tfunc )
{
  if ( _tempFunc_nu != NULL ) delete _tempFunc_nu;
  _tempFunc_nu = Tfunc.clone();
};

void mlawNonLocalPorosity::setReferenceCp( const double cp )
{
  _cp = cp;
};

void mlawNonLocalPorosity::setTemperatureFunction_cp( const scalarFunction& Tfunc )
{
  if ( _tempFunc_cp != NULL ) delete _tempFunc_cp;
  _tempFunc_cp = Tfunc.clone();
};

void mlawNonLocalPorosity::setReferenceT( const double referenceT )
{
  _Tref = referenceT;
};

void mlawNonLocalPorosity::setTaylorQuineyFactor( const double taylorQuineyFactor )
{
  _taylorQuineyFactor = taylorQuineyFactor;
};

void mlawNonLocalPorosity::setThermalExpansionCoefficient( const double thermalExpansionCoefficient )
{
  _alphaDilatation = thermalExpansionCoefficient;
  STensorOperation::zero(Stiff_alphaDilatation);
  Stiff_alphaDilatation(0,0) = _alphaDilatation;
  Stiff_alphaDilatation(1,1) = _alphaDilatation;
  Stiff_alphaDilatation(2,2) = _alphaDilatation;
};

void mlawNonLocalPorosity::setTemperatureFunction_alphaDilatation( const scalarFunction& Tfunc )
{
  if ( _tempFunc_alphaDilatation != NULL ) delete _tempFunc_alphaDilatation;
  _tempFunc_alphaDilatation = Tfunc.clone();
};

void mlawNonLocalPorosity::setReferenceThermalConductivity( const double k )
{
  _thermalConductivity = k;
  STensorOperation::zero(linearK);
  linearK(0,0) = k;
  linearK(1,1) = k;
  linearK(2,2) = k;
};

void mlawNonLocalPorosity::setTemperatureFunction_ThermalConductivity( const scalarFunction& Tfunc )
{
  if (_tempFunc_thermalConductivity != NULL) delete _tempFunc_thermalConductivity;
  _tempFunc_thermalConductivity = Tfunc.clone();
};

void mlawNonLocalPorosity::setScatterredInitialPorosity(const double randMinbound, const double randMaxbound )
{
  _randMinbound = randMinbound;
  _randMaxbound = randMaxbound;
  
  Msg::Info("generalmlawPorous::setScatterredInitialPorosity: Add a scatter in initial parameters");

  if ( _randMinbound < 0. ) Msg::Error("generalmlawPorous::setScatterredInitialPorosity: Wrong scatter parameter randMinbound !");
  if ( _randMaxbound < _randMinbound ) Msg::Error("generalmlawPorous::setScatterredInitialPorosity: Wrong scatter parameter randMaxbound < randMinbound !");
};


void mlawNonLocalPorosity::setFailureTolerance( const double NewFailureTol )
{
  if ( (NewFailureTol >= 1.0) or (NewFailureTol < 0.) )
    Msg::Error("generalmlawPorous::setFailureTolerance: Uncorrect value of tolerance failure");

  _failedTol = NewFailureTol;
};

void mlawNonLocalPorosity::setLocalFailurePorosity( const double newFvMax )
{
  if ( (newFvMax >= 1.0) or (newFvMax <= _fVinitial) )
    Msg::Error("generalmlawPorous::setLocalFailurePorosity: Uncorrect value of maximal local porosity");

  _localfVFailure = newFvMax;
};

void mlawNonLocalPorosity::setCorrectedRegularizedFunction( const scalarFunction& fct )
{
  if ( _regularisationFunc_porosityEff != NULL ) delete _regularisationFunc_porosityEff;
  _regularisationFunc_porosityEff = fct.clone();
};


void mlawNonLocalPorosity::setShearPorosityGrowth_Factor( const double kw )
{
  if (kw < 0.0) Msg::Error("generalmlawPorous::setShearPorosityGrowth_Factor: kw can not be negative");
  _kw = kw;
};

void mlawNonLocalPorosity::setShearPorosityGrowth_StressTriaxialityFunction( const scalarFunction& fct )
{
  if (_shearGrowth_triaxFunc != NULL) delete _shearGrowth_triaxFunc;
  _shearGrowth_triaxFunc = fct.clone();
};

void mlawNonLocalPorosity::setShearPorosityGrowth_LodeFunction( const scalarFunction& fct )
{
  if (_shearGrowth_LodeVarFunc != NULL) delete _shearGrowth_LodeVarFunc;
  _shearGrowth_LodeVarFunc = fct.clone();
};



void mlawNonLocalPorosity::setOrderForLogExp( const int newOrder )
{
  _orderlogexp = newOrder;
  Msg::Info("generalmlawPorous::setOrderForLogExp: polynomial order = %d is used to approximate log. and exp. functions");
};

void mlawNonLocalPorosity::setTangentByPerturbation( const bool tangentByPertIsON, const double perturbationfactor )
{
  _tangentByPerturbation = tangentByPertIsON;
  _perturbationfactor = perturbationfactor;  
}

void mlawNonLocalPorosity::setSubStepping( const bool subSteppingIsON, const int maxNumOfStepReductions )
{
  _withSubstepping = subSteppingIsON;
  _maxAttemptSubstepping = maxNumOfStepReductions;
  if (_maxAttemptSubstepping < 1) Msg::Error( "generalmlawPorous::setSubStepping: at least 1 attemp must be allowed for the substepping instead of %d", _maxAttemptSubstepping );
  if ( _withSubstepping ) Msg::Info( "generalmlawPorous::setSubStepping: Substepping is ON with max. %d step reductions.", _maxAttemptSubstepping );
  else Msg::Info( "generalmlawPorous::setSubStepping: Substepping is OFF." );
};



bool mlawNonLocalPorosity::elasticPredictor(  const STensor3& F1, const STensor3& Fppr,
                                           STensor3& corKir,  STensor3& corKirDev, double& ppr,
                                           STensor43 &dcorKirdEe, STensor43& dcorKirDevdEe, STensor3& dpDEe,
                                           STensor3& Fepr, STensor3& Cepr, STensor3& Eepr,
                                           STensor43 &Le, STensor63 &dLe, const bool stiff, 
                                           const double* T, STensor3 *dcorKirdT, STensor3* dcorKirDevdT, double* dpDT
                                        ) const
{
  // Get elastic constants.
  double KT = _K;
  double muT = _mu;
  double alpT = _alphaDilatation;
  if ( isThermomechanicallyCoupled() ){
    KT = bulkModulus(*T);
    muT = shearModulus(*T);
    alpT = ThermalExpansion(*T);
  }

  // Compute elastic strain tensors Fepr, Cepr, Eepr and derivatives Le and dLe.
  static STensor3 invFp0;
  STensorOperation::inverseSTensor3(Fppr, invFp0);
  STensorOperation::multSTensor3(F1, invFp0, Fepr);
  STensorOperation::multSTensor3FirstTranspose(Fepr, Fepr, Cepr);
  bool ok=STensorOperation::logSTensor3(Cepr, _orderlogexp, Eepr, &Le, &dLe);
  if(!ok)
  {
    return false;
  }
  Eepr *= 0.5;

  // Split Ee in dev and trace part for stress computation.
  static STensor3 devEe;
  double trEe;
  STensorOperation::decomposeDevTr(Eepr, devEe, trEe);

  // Compute kcor dev and pressure
  corKirDev = devEe;
  double muT_times2 = 2.0 * muT;
  corKirDev *= muT_times2;

  ppr = KT * trEe;
  if ( isThermomechanicallyCoupled() ){
   ppr -= 3.0 * alpT * KT * ( *T - _Tref);
  }

  corKir = corKirDev;
  corKir(0,0) += ppr;
  corKir(1,1) += ppr;
  corKir(2,2) += ppr;

  if ( stiff )
  {
    dpDEe = _I;
    dpDEe *= KT;

    dcorKirDevdEe = _I4dev;
    dcorKirDevdEe *= muT_times2;

    dcorKirdEe = dcorKirDevdEe;
    //! \todo a more efficient function exist ???
    STensorOperation::prodAdd(_I, dpDEe, 1.0, dcorKirdEe);
    
    if ( isThermomechanicallyCoupled() ){
      double DKDT = bulkModulusDerivative(*T);
      double DmuDT = shearModulusDerivative(*T);
      double DalpDT = ThermalExpansionDerivative(*T);

      //static STensor3 devEe;
      //double trEe;
      //STensorOperation::decomposeDevTr(Eepr,devEe,trEe);

      (*dcorKirDevdT) = devEe;
      (*dcorKirDevdT) *= (2.*DmuDT);

      (*dpDT) = DKDT*trEe -3.*DalpDT*KT*(*T-_Tref) - 3.*alpT*DKDT*(*T-_Tref) - 3.*alpT*KT;

      (*dcorKirdT) = (*dcorKirDevdT);
      dcorKirdT->daxpy(_I,*dpDT);
    }
  }
  return true;
};




void mlawNonLocalPorosity::setNonLocalMethod(const int i){
  nonlocalMethod_ = (nonLocalMethod)i;
  if (nonlocalMethod_ == LOCAL){
    _numNonLocalVar = 0;
    Msg::Info("local formulation is used");
  }
  else if (nonlocalMethod_ == NONLOCAL_POROSITY){
    _numNonLocalVar = 1;
    Msg::Info("nonlocal porosity");
  }
  else if (nonlocalMethod_ == NONLOCAL_LOG_POROSITY){
    _numNonLocalVar = 1;
    Msg::Info("nonlocal logarithm of porosity");
  }
  else if (nonlocalMethod_ == NONLOCAL_FULL_THREEVAR){
    _numNonLocalVar = 3;
    if (_cLLaw.size() < 3 and _cLLaw.size() > 0)
    {
      for (int i=_cLLaw.size(); i<3; i++)
      {
        this->setCLengthLaw(*_cLLaw[0]);
      }
    }
    Msg::Info("nonlocal deviatoric plasticity, volumetric plasticity, and matrix plasticity");
  }
  else{
    Msg::Error("nonlocal Method hs not correctly defined");
  }
};

const mlawNonLocalPorosity::nonLocalMethod mlawNonLocalPorosity::getNonLocalMethod() const{
  return nonlocalMethod_;
};



void mlawNonLocalPorosity::clearCLengthLaw(){
  for (int i=0; i< _cLLaw.size(); i++){
    if (_cLLaw[i] != NULL) delete _cLLaw[i];
  }
  _cLLaw.resize(0);
};

void mlawNonLocalPorosity::setCLengthLaw(const CLengthLaw& clength){
  _cLLaw.push_back(clength.clone());
};

double mlawNonLocalPorosity::getShearFactor(const STensor3& kcor, bool stiff, STensor3* DkDkcor) const
{
  // kw is a function of stress triaxiality and Lode
  static STensor3 devKcor;
  double pcor;
  STensorOperation::decomposeDevTr(kcor,devKcor,pcor);
  pcor /= 3.;
  double kcorEqSq = 1.5*devKcor.dotprod();
  double kcorEq = sqrt(kcorEqSq);
  if (kcorEq >0)
  {
    double J3 = STensorOperation::determinantSTensor3(devKcor);
    double T = pcor/kcorEq;
    double Xi = 27.*J3/(2.*kcorEq*kcorEq*kcorEq);
    
    double funcT = _shearGrowth_triaxFunc->getVal(T);
    double funcXi = _shearGrowth_LodeVarFunc->getVal(Xi);
    
    double realKw = _kw*funcT*funcXi;
    if (stiff)
    {
      // ompute DTDkcor, DXiDkcor
      static STensor3 DpcorDkcor, DkcorEqDkcor, DJ3Dkcor;
      STensorOperation::diag(DpcorDkcor,1./3.);
      DkcorEqDkcor = devKcor;
      DkcorEqDkcor *= (1.5/kcorEq);
      STensorOperation::multSTensor3(devKcor,devKcor,DJ3Dkcor);
      DJ3Dkcor(0,0) -= (2.*kcorEqSq/9.);
      DJ3Dkcor(1,1) -= (2.*kcorEqSq/9.);
      DJ3Dkcor(2,2) -= (2.*kcorEqSq/9.);
      
      double DTDpcor = 1./kcorEq;
      double DTDkcorEq = -T/kcorEq;
      
      double DXiDJ3 = Xi/J3;
      double DXiDkcorEq = -3.*Xi/kcorEq;
      
      double DifffuncT = _shearGrowth_triaxFunc->getDiff(T);
      double DifffuncXi = _shearGrowth_LodeVarFunc->getDiff(Xi);
      
      double DrealKwDpcor = _kw*DifffuncT*DTDpcor*funcXi;
      double DrealKwDkcorEq = _kw*DifffuncT*DTDkcorEq*funcXi + _kw*funcT*DifffuncXi*DXiDkcorEq;
      double DrealKwDJ3 = _kw*funcT*DifffuncXi*DXiDJ3;
      
      STensorOperation::zero(*DkDkcor);
      DkDkcor->daxpy(DpcorDkcor,DrealKwDpcor);
      DkDkcor->daxpy(DkcorEqDkcor,DrealKwDkcorEq);
      DkDkcor->daxpy(DJ3Dkcor,DrealKwDJ3);
    }
    return realKw;
  }
  else
  {
    //Msg::Warning("stress equivalent is zero, kw effect is neglected");
    if (stiff)
    {
      STensorOperation::zero(*DkDkcor);
    }
    return 0.;
  }
};


void mlawNonLocalPorosity::checkInternalState(IPVariable* ipv, const IPVariable* ipvprev) const{
  // Update the state of the material (nucleation criterion, coalescence and failure) at the end of the NR scheme
  // from the ip porous
  /// Get IP porous
  IPNonLocalPorosity* q1 = dynamic_cast<IPNonLocalPorosity*>(ipv);
  const IPNonLocalPorosity* q0 = static_cast<const IPNonLocalPorosity*>(ipvprev);
  /// Call corresponding functions of each parts (nucleation criterion, coalescence and failure)
  if (q1 != NULL){
    this->checkNucleationCriterion(q1, q0);
    double T = q1->getTemperature();
    this->checkCoalescence(q1, q0, &T);
    this->checkFailed(q1, q0);
    // saturate when void state is saturated
    q1->getRefToIPJ2IsotropicHardening().getRefToSaturationState() = q1->getConstRefToIPVoidState().isSaturated();
  };
};

void mlawNonLocalPorosity::checkNucleationCriterion(IPNonLocalPorosity* q1, const IPNonLocalPorosity *q0) const
{
  // Update nucleation criterion by calling the internal law
  getNucleationLaw()->checkNucleationActivation( *q1, *q0 );
};

//-------------------------------------------------------added
double mlawNonLocalPorosity::deformationEnergy(const STensor3 &C,const double* T) const
{

  double Jac= sqrt((C(0,0) * (C(1,1) * C(2,2) - C(1,2) * C(2,1)) -
          C(0,1) * (C(1,0) * C(2,2) - C(1,2) * C(2,0)) +
          C(0,2) * (C(1,0) * C(2,1) - C(1,1) * C(2,0))));
  double lnJ = log(Jac);
  static STensor3 logCdev;
  bool ok=STensorOperation::logSTensor3(C,_orderlogexp,logCdev);
  double trace = logCdev.trace();
  logCdev(0,0)-=trace/3.;
  logCdev(1,1)-=trace/3.;
  logCdev(2,2)-=trace/3.;
  double Psy;
  if(isThermomechanicallyCoupled()){
    double KT=bulkModulus(*T);
    double muT=shearModulus(*T);
    double alpT=ThermalExpansion(*T);
    Psy = KT*0.5*lnJ*lnJ+muT*0.25*dot(logCdev,logCdev)-3.*KT*alpT*(*T-_Tref)*lnJ;
  }
  else{
    Psy = _K*0.5*lnJ*lnJ+_mu*0.25*dot(logCdev,logCdev);
  }
  return Psy;
}
//--------------------------------------------------------end

void mlawNonLocalPorosity::getDHookTensorDT(STensor43& DHDT, double T) const{
  if (isThermomechanicallyCoupled())
  {
    // H = 2*G*I4dev + 3K*I4vol
    double DKDT=bulkModulusDerivative(T);
    double DmuDT=shearModulusDerivative(T);
    DHDT = _I4vol;
    DHDT *= (3.*DKDT);
    DHDT.axpy(2.*DmuDT,_I4dev);
  }
  else
  {
    STensorOperation::zero(DHDT);
  }
};






void mlawNonLocalPorosity::updateInternalVariables(IPNonLocalPorosity *q1, const IPNonLocalPorosity* q0,
                            const double DeltaHatD, const double DeltaHatQ, const double DeltaHatP,
                            bool withPlasticInstability, const double* T) const{
  double hatPn = q0->getLocalMatrixPlasticStrain();
  double hatQn = q0->getLocalVolumetricPlasticStrain();
  double hatDn = q0->getLocalDeviatoricPlasticStrain();

  double& hatP = q1->getRefToLocalMatrixPlasticStrain();
  hatP = hatPn + DeltaHatP;

  double& hatQ = q1->getRefToLocalVolumetricPlasticStrain();
  hatQ = hatQn + DeltaHatQ;

  double& hatD = q1->getRefToLocalDeviatoricPlasticStrain();
  hatD = hatDn + DeltaHatD;

  // update hardening
  if (isThermomechanicallyCoupled()){
    _j2IH->hardening(hatPn,q0->getConstRefToIPJ2IsotropicHardening(),hatP,*T, q1->getRefToIPJ2IsotropicHardening());
  }
  else{
    _j2IH->hardening(hatPn,q0->getConstRefToIPJ2IsotropicHardening(),hatP,q1->getRefToIPJ2IsotropicHardening());
  }
  if(withPlasticInstability)
  {
    double wp=0.;
    double dwp=0.;
    getYieldDecrease(q1->getNonLocalMatrixPlasticStrain(), wp, dwp);
    q1->getRefToIPJ2IsotropicHardening().setScaleFactor(wp, dwp);
  }
};



void mlawNonLocalPorosity::updatePlasticState(IPNonLocalPorosity *q1, const IPNonLocalPorosity* q0,  const STensor3& F1,
                            const double DeltaHatD, const double DeltaHatQ, const double DeltaHatP,
                            const STensor3& devKcorpr, const double kcorEqpr, const double pcorpr, const double yield,
                            STensor3& Fe1, STensor3& Ce, STensor3& Ee, STensor43& Le, STensor63& dLe,
                            STensor3& DGNp, STensor3& devDGNp, double& trDGNp, STensor43& Dexp, const double* T) const{
  double KT=_K;
  double muT= _mu;
  if (isThermomechanicallyCoupled()){
    KT = bulkModulus(*T);
    muT = shearModulus(*T);
  }
  double f0 = q0->getInitialPorosity();

  // update plastic normal
  devDGNp = devKcorpr;
  devDGNp *= (1.5*DeltaHatD/kcorEqpr);
  // trace
  trDGNp = DeltaHatQ;

  //
  DGNp = devDGNp;
  DGNp(0,0) += (trDGNp/3.);
  DGNp(1,1) += (trDGNp/3.);
  DGNp(2,2) += (trDGNp/3.);
  //
  // update stress
  STensor3& kCor = q1->getRefToCorotationalKirchhoffStress();
  kCor = devKcorpr;
  kCor.daxpy(devDGNp,-2.*muT);
  double pcor = pcorpr- KT*DeltaHatQ;
  kCor(0,0) += pcor;
  kCor(1,1) += pcor;
  kCor(2,2) += pcor;

  // update plastic deformation
  STensor3& Fp1 = q1->getRefToFp();
   // Plastic increment
  static STensor3 dFp;
  // dFp = exp(DeltaGamma*Np)
  STensorOperation::expSTensor3(DGNp,_orderlogexp,dFp, &Dexp);
  // Fp1 = dFp * Fp0
  const STensor3& Fp0 = q0->getConstRefToFp();
  STensorOperation::multSTensor3(dFp,Fp0,Fp1);
  // Fe1 = F1 * Fp1^-T
  static STensor3 Fp1_inv;
  STensorOperation::inverseSTensor3(Fp1, Fp1_inv);
  STensorOperation::multSTensor3(F1,Fp1_inv,Fe1);
  // Ce = Fe1^T * Fe1
  STensorOperation::multSTensor3FirstTranspose(Fe1,Fe1,Ce);
  // Ee = ln(sqrt(Ce))
  bool ok=STensorOperation::logSTensor3(Ce,_orderlogexp,Ee,&Le,&dLe);
  Ee *= 0.5;

  // dissipation energy
  double& plasticEnergy = q1->getRefToPlasticEnergy();
  plasticEnergy = q0->plasticEnergy()+ (1-f0)*yield*DeltaHatP;

};

void mlawNonLocalPorosity::predictorCorrector_NonLocalLogarithmPorosity(
                            const STensor3& F0,         // initial deformation gradient (input @ time n)
                            const STensor3& F1,         // updated deformation gradient (input @ time n+1)
                            STensor3 &P,                // updated 1st Piola-Kirchhoff stress tensor (output)
                            const IPNonLocalPorosity *ipvprev,       // array of initial internal variable
                            IPNonLocalPorosity *ipvcur,             // updated array of internal variable (in ipvcur on output),
                            STensor43 &Tangent,         // constitutive tangents (output)
                            STensor3  &dLocalVarDStrain,
                            STensor3  &dStressDNonLocalVar,
                            double    &dLocalVarDNonLocalVar,
                            const bool stiff,   // if true compute the tangents
                            STensor43& dFedF, STensor3& dFeDNonlocalVar,
                            STensor3* dStressDT ,//------added
                            double* dLocalVarDT,//-----added
                            const double T0,const double T   //temperatures @ time n and @ time n+1
                            ) const{
  // starts from previous
  ipvcur->getRefToLocalLogarithmPorosity() = ipvprev->getLocalLogarithmPorosity();

  //  update from logarithmic nonlocal variable
  ipvcur->getRefToNonLocalPorosity() = exp(ipvcur->getNonLocalLogarithmPorosity());

  // compute with usual nonlocal variables
  predictorCorrector_NonLocalPorosity(F0,F1,P,ipvprev,ipvcur,Tangent,dLocalVarDStrain,dStressDNonLocalVar,dLocalVarDNonLocalVar,stiff,
                    dFedF,dFeDNonlocalVar, dStressDT,dLocalVarDT,T0,T);


  // update current value
  ipvcur->getRefToLocalLogarithmPorosity() = log(ipvcur->getLocalPorosity());

  if (stiff){
    // change tangents

    double DNonlocalPoroDlogNonlocalPoro = ipvcur->getRefToNonLocalPorosity();

    dLocalVarDStrain *= (1./ipvcur->getLocalPorosity());
    dStressDNonLocalVar *= DNonlocalPoroDlogNonlocalPoro;
    dLocalVarDNonLocalVar *= (DNonlocalPoroDlogNonlocalPoro/ipvcur->getLocalPorosity());


    if (isThermomechanicallyCoupled()){
      (*dLocalVarDT) /= (ipvcur->getLocalPorosity());
    }

    ipvcur->getRefToDPlasticEnergyDNonLocalVariable(0) *= DNonlocalPoroDlogNonlocalPoro;
    dFeDNonlocalVar *= DNonlocalPoroDlogNonlocalPoro;

    if (getMacroSolver()->withPathFollowing()){
      ipvcur->getRefToDIrreversibleEnergyDNonLocalVariable(0) *= DNonlocalPoroDlogNonlocalPoro;
    }
  }
};

void mlawNonLocalPorosity::computeResidual_NonLocalPorosity(SVector3& res, STensor3& J, const STensor3& F,
                  const double kcorEqpr, const double pcorpr, const double R, const double H,
                  const double yieldfV,
                  const IPNonLocalPorosity *q0, const IPNonLocalPorosity* q1,
                  const double DeltaHatD, const double DeltaHatQ, const double DeltaHatP, // 3 unknonwn
                  const double* T
                ) const
{
  // Get ipv
  const IPVoidState* voidState = &q1->getConstRefToIPVoidState();

  // Get values
  double Chi = voidState->getVoidLigamentRatio();
  double DChiDyieldfV  = voidState->getDVoidLigamentRatioDYieldPorosity();
  double DChiDDeltaHatD = voidState->getDVoidLigamentRatioDDeviatoricPlasticDeformation();
  double DChiDDeltaHatQ = voidState->getDVoidLigamentRatioDVolumetricPlasticDeformation();
  double DChiDDeltaHatP = voidState->getDVoidLigamentRatioDMatrixPlasticDeformation();

  double W = voidState->getVoidAspectRatio();
  double DWDyieldfV  = voidState->getDVoidAspectRatioDYieldPorosity();
  double DWDDeltaHatD = voidState->getDVoidAspectRatioDDeviatoricPlasticDeformation();
  double DWDDeltaHatQ = voidState->getDVoidAspectRatioDVolumetricPlasticDeformation();
  double DWDDeltaHatP = voidState->getDVoidAspectRatioDMatrixPlasticDeformation();

  double f0 = q1->getInitialPorosity();
  double R0 = _j2IH->getYield0();

  // Get elastic parameters
  double KT= _K;
  double muT = _mu;
  if (isThermomechanicallyCoupled()){
    KT = bulkModulus(*T);
    muT =shearModulus(*T);
  };

  // Corrected stress invariants
  double kcorEq =  kcorEqpr - 3.*muT*DeltaHatD;
  double pcor = pcorpr - KT*DeltaHatQ;
  double detF = 1.;
  if (_stressFormulation == CORO_CAUCHY)
  {
    detF = STensorOperation::determinantSTensor3(F);
    kcorEq /= detF;
    pcor /= detF;
  }

  // Residues
  // 1. yield function
  // grad f  with respect to [kcorEq pcor R fV Chi W]
  static fullVector<double> gradf(6); 
  res(0) = yieldFunction(kcorEq,pcor,R,yieldfV,q0,q1,T,true,&gradf);


  // 2. plastic flow normality
  // grad Ns, Nf with respect to [kcorEq pcor R fV Chi W]
  static fullVector<double> gradNs(6), gradNv(6);
  double Ns, Nv; // plastic flow components
  plasticFlow(Ns,Nv,kcorEq,pcor,R,q0,q1,T,true,&gradNs,&gradNv);
  res(1) = R0*(DeltaHatD*Nv- DeltaHatQ*Ns);

  double Jp = 1.;
  if (_stressFormulation == CORO_KIRCHHOFF)
  {
    double Jp0 = STensorOperation::determinantSTensor3(q0->getConstRefToFp());
    Jp = Jp0*exp(DeltaHatQ);
  }
  // plastic energy balance
  res(2) = ((1.-yieldfV)*Jp*R*DeltaHatP - kcorEq*DeltaHatD - pcor*DeltaHatQ)/R0;
 
  double DkcorEqDDeltaHatD = -3.*muT/detF;
  double DpcorDDeltaHatQ = -KT/detF;

  // Dres0DDeltaHatD
  J(0,0) = gradf(0)*DkcorEqDDeltaHatD+ gradf(4)*DChiDDeltaHatD + gradf(5)*DWDDeltaHatD;
  // Dres0DDeltaHatQ
  J(0,1) = gradf(1)*DpcorDDeltaHatQ + gradf(4)*DChiDDeltaHatQ + gradf(5)*DWDDeltaHatQ;
  // Dres0DDeltaHatP
  J(0,2) = gradf(2)*H+ + gradf(4)*DChiDDeltaHatP + gradf(5)*DWDDeltaHatP;

  // Dres1DDeltaHatD
  J(1,0) = R0*(Nv + DeltaHatD*gradNv(0)*DkcorEqDDeltaHatD - DeltaHatQ*gradNs(0)*DkcorEqDDeltaHatD) +
           R0*(DeltaHatD*gradNv(4)- DeltaHatQ*gradNs(4))*DChiDDeltaHatD +
           R0*(DeltaHatD*gradNv(5)- DeltaHatQ*gradNs(5))*DWDDeltaHatD;
  // Dres1DDeltaHatQ
  J(1,1) = R0*(DeltaHatD*gradNv(1)*DpcorDDeltaHatQ- Ns - DeltaHatQ*gradNs(1)*DpcorDDeltaHatQ) +
           R0*(DeltaHatD*gradNv(4)- DeltaHatQ*gradNs(4))*DChiDDeltaHatQ +
           R0*(DeltaHatD*gradNv(5)- DeltaHatQ*gradNs(5))*DWDDeltaHatQ;
  // Dres1DDeltaHatP
  J(1,2) = R0*(DeltaHatD*gradNv(2) - DeltaHatQ*gradNs(2))*H+
           R0*(DeltaHatD*gradNv(4)- DeltaHatQ*gradNs(4))*DChiDDeltaHatP+
           R0*(DeltaHatD*gradNv(5)- DeltaHatQ*gradNs(5))*DWDDeltaHatP;

  // Dres2DDeltaHatd
  J(2,0) = (-kcorEq - DkcorEqDDeltaHatD*DeltaHatD)/R0;
  // Dres2DDeltaHatq
  J(2,1) = (-pcor - DpcorDDeltaHatQ*DeltaHatQ)/R0;
  if (_stressFormulation == CORO_KIRCHHOFF)
  {
    double DJpDDeltaHatQ = Jp;
    J(2,1) += ((1.-yieldfV)*DJpDDeltaHatQ*R*DeltaHatP)/R0;
  }
  // Dres2DDeltaHatp
  J(2,2) = (1.-yieldfV)*Jp*(R + H*DeltaHatP)/R0;
};

void mlawNonLocalPorosity::computeDResidual_NonLocalPorosity(STensor3 &dres0dEpr, STensor3 &dres1dEpr, STensor3 &dres2dEpr,
                                  double &dres0dtildefV, double &dres1dtildefV, double &dres2dtildefV,
                                  const STensor3& F,
                                  const STensor3& devKcorpr, const double kcorEqpr, const double pcorpr, const double R,
                                  const double yieldfV, const STensor3& DyieldfVDKcorpr, const double DyieldfVDtildefV,
                                  const STensor43& DKcorprDEpr, const STensor43& DdevKcorprDEpr, const STensor3& DpcorprDEpr,
                                  const IPNonLocalPorosity *q0, const IPNonLocalPorosity* q1,
                                  const double DeltaHatD, const double DeltaHatQ, const double DeltaHatP,
                                  const double* T, const double* DRDT,
                                  const STensor3* DdevKcorprDT , const double* DpcorprDT, const STensor3* DKcorprDT,
                                  double *dres0dT, double *dres1dT, double *dres2dT
                                  ) const
{

  const IPVoidState* voidState = &q1->getConstRefToIPVoidState();

  double Chi  = voidState->getVoidLigamentRatio();
  double DChiDyieldfV  = voidState->getDVoidLigamentRatioDYieldPorosity();

  double W  = voidState->getVoidAspectRatio();
  double DWDyieldfV  = voidState->getDVoidAspectRatioDYieldPorosity();

  double gamma = voidState->getVoidShapeFactor();
  double DgammaDyieldfV  = voidState->getDVoidShapeFactorDYieldPorosity();

  double f0 = q1->getInitialPorosity();
  double R0 = _j2IH->getYield0(); // reference value

  double KT = _K;
  double muT = _mu;
  if (isThermomechanicallyCoupled()){
    KT=bulkModulus(*T);
    muT=shearModulus(*T);
  }
  
  static STensor3 DyieldfVDEpr;
  STensorOperation::multSTensor3STensor43(DyieldfVDKcorpr,DKcorprDEpr,DyieldfVDEpr);
  
  static STensor3 DkcorEqprDEpr;
  STensorOperation::multSTensor3STensor43(devKcorpr,DdevKcorprDEpr,DkcorEqprDEpr);
  DkcorEqprDEpr *= (1.5/kcorEqpr);

  // corrected values
  double kcorEq =  kcorEqpr - 3.*muT*DeltaHatD;
  double pcor = pcorpr - KT*DeltaHatQ;

  static STensor3 DkcorEqDEpr, DpcorDEpr;
  DkcorEqDEpr = DkcorEqprDEpr;
  DpcorDEpr = DpcorprDEpr;
  double detF = 1.;
  if (_stressFormulation == CORO_CAUCHY)
  {
    detF = STensorOperation::determinantSTensor3(F);
    kcorEq /= detF;
    pcor /= detF;  
    
    DkcorEqDEpr *= (1./detF);
    DpcorDEpr *= (1./detF);
    
    static STensor3 DdetFDEepr;
    STensorOperation::diag(DdetFDEepr,detF);
    DkcorEqDEpr.daxpy(DdetFDEepr,-kcorEq/detF);
    DpcorDEpr.daxpy(DdetFDEepr,-pcor/detF);
  }
  
  // grad f, Ns, Nf with respect to [kcorEq pcor R fV Chi W]
  static fullVector<double> gradf(6), gradNs(6), gradNv(6);
  double DfDT, DNsDT, DNvDT;
  double f, Ns, Nv;
  f = yieldFunction(kcorEq,pcor,R,yieldfV,q0,q1,T,true,&gradf,isThermomechanicallyCoupled(),&DfDT);
  plasticFlow(Ns,Nv,kcorEq,pcor,R,q0,q1,T,true,&gradNs,&gradNv,isThermomechanicallyCoupled(),&DNsDT,&DNvDT);
  
  // compute Dres0DEpr
  double Dres0DkcorEq = gradf(0);
  double Dres0Dpcor = gradf(1);
  double Dres0DyieldfV = gradf(3) + gradf(4)*DChiDyieldfV+gradf(5)*DWDyieldfV;
  dres0dEpr = DkcorEqDEpr;
  dres0dEpr*= Dres0DkcorEq;
  dres0dEpr.daxpy(DpcorDEpr,Dres0Dpcor);
  dres0dEpr.daxpy(DyieldfVDEpr,Dres0DyieldfV);

  // compute Dres1DEpr
  double Dres1DkcorEq = R0*(DeltaHatD*gradNv(0)- DeltaHatQ*gradNs(0));
  double Dres1Dpcor = R0*(DeltaHatD*gradNv(1)- DeltaHatQ*gradNs(1));
  double Dres1DyieldfV = R0*(DeltaHatD*gradNv(3)- DeltaHatQ*gradNs(3))+
                         R0*(DeltaHatD*gradNv(4)- DeltaHatQ*gradNs(4))*DChiDyieldfV +
                         R0*(DeltaHatD*gradNv(5)- DeltaHatQ*gradNs(5))*DWDyieldfV;

  dres1dEpr = DkcorEqDEpr;
  dres1dEpr *= Dres1DkcorEq;
  dres1dEpr.daxpy(DpcorDEpr,Dres1Dpcor);
  dres1dEpr.daxpy(DyieldfVDEpr,Dres1DyieldfV);

  double Jp = 1.;
  if (_stressFormulation == CORO_KIRCHHOFF)
  {
    double Jp0 = STensorOperation::determinantSTensor3(q0->getConstRefToFp());
    Jp = Jp0*exp(DeltaHatQ);
  }
  
  // compute Dres2DEpr
  double Dres2DkcorEq = (-DeltaHatD)/R0;
  double Dres2Dpcor = (-DeltaHatQ)/R0;
  double Dres2DyieldfV = (-Jp*R*DeltaHatP)/R0;

  dres2dEpr = DkcorEqDEpr;
  dres2dEpr *= Dres2DkcorEq;
  dres2dEpr.daxpy(DpcorDEpr,Dres2Dpcor);


  // compute nonlocal derivative
  dres0dtildefV = Dres0DyieldfV*DyieldfVDtildefV;
  dres1dtildefV = Dres1DyieldfV*DyieldfVDtildefV;
  dres2dtildefV = Dres2DyieldfV*DyieldfVDtildefV;
  
  if (isThermomechanicallyCoupled()){
    double dKdT=bulkModulusDerivative(*T);
    double dmudT=shearModulusDerivative(*T);

    static double DkcorEqDT, DpcorDT;
    DkcorEqDT = STensorOperation::doubledot(devKcorpr,*DdevKcorprDT)*1.5/kcorEqpr - 3.*dmudT*DeltaHatD;
    DpcorDT = *DpcorprDT -dKdT* DeltaHatQ;
    if (_stressFormulation == CORO_CAUCHY)
    {
      DkcorEqDT /= detF;
      DpcorDT /= detF;
    }

    double partDres0DT = gradf(2)*(*DRDT) + DfDT;  // yield temperature-dependent
    *dres0dT = Dres0DkcorEq*DkcorEqDT + Dres0Dpcor*DpcorDT + partDres0DT;

    double partDres1DT = R0*(DeltaHatD*gradNv(2)- DeltaHatQ*gradNs(2))*(*DRDT) + R0*(DeltaHatD*DNvDT- DeltaHatQ*DNsDT);
    *dres1dT = Dres1DkcorEq*DkcorEqDT + Dres1Dpcor*DpcorDT + partDres1DT;

    double partDres2DT = ((1-yieldfV)*Jp*(*DRDT)*DeltaHatP)/R0;
    *dres2dT = Dres2DkcorEq*DkcorEqDT + Dres2Dpcor*DpcorDT+ partDres2DT;
  }
};


//
// note that mlaw considers previous step as reference
// when modifying internal variables, the related condition must be respective
//

void mlawNonLocalPorosity::predictorCorrector_NonLocalPorosity(
                            const STensor3& F0,         // initial deformation gradient (input @ time n)
                            const STensor3& F1,         // updated deformation gradient (input @ time n+1)
                            STensor3 &P,                // updated 1st Piola-Kirchhoff stress tensor (output)
                            const IPNonLocalPorosity *ipvprev,       // array of initial internal variable
                            IPNonLocalPorosity *ipvcur,             // updated array of internal variable (in ipvcur on output),
                            STensor43 &Tangent,         // constitutive tangents (output)
                            STensor3  &dLocaldPorosityDStrain,
                            STensor3  &dStressDNonLocalPorosity,
                            double    &dLocalPorosityDNonLocalPorosity,
                            const bool stiff,   // if true compute the tangents
                            STensor43& dFedF, STensor3& dFeDtildefV,
                            STensor3* dStressDT ,//------added
                            double* dLocalPorosityDT,//-----added
                            const double T0,const double T   //temperatures @ time n and @ time n+1
                            ) const
{
  // Get variables, set them to previous state and update internal laws
  // (excepted porosity and coalescence which depend on transition/damage blocking)
    // Plasticity (local) variables
  ipvcur->getRefToLocalMatrixPlasticStrain() = ipvprev->getLocalMatrixPlasticStrain();
  ipvcur->getRefToLocalVolumetricPlasticStrain() = ipvprev->getLocalVolumetricPlasticStrain();
  ipvcur->getRefToLocalDeviatoricPlasticStrain() = ipvprev->getLocalDeviatoricPlasticStrain();
  ipvcur->getRefToPlasticEnergy() = ipvprev->plasticEnergy();

  // so copy-paste previous values
  ipvcur->getRefToLocalPorosity() = ipvprev->getLocalPorosity();
  ipvcur->getRefToCorrectedPorosity() = ipvprev->getCorrectedPorosity();
  ipvcur->getRefToYieldPorosity() = ipvprev->getYieldPorosity();

    // Fp
  ipvcur->getRefToFp() = ipvprev->getConstRefToFp();
  const STensor3* Fp0 = &(ipvprev->getConstRefToFp()); // Pointer is used as pointed object will changed if substepping
  STensor3& Fp1 = ipvcur->getRefToFp();

    // Non-local length law
  _cLLaw[0]->computeCL(ipvprev->getNonLocalPorosity(), ipvcur->getRefToIPCLength(0));

    // Hardening law
  if (isThermomechanicallyCoupled())
  {
    _j2IH->hardening(ipvprev->getLocalMatrixPlasticStrain(),ipvprev->getConstRefToIPJ2IsotropicHardening(), ipvcur->getLocalMatrixPlasticStrain(),T, ipvcur->getRefToIPJ2IsotropicHardening());
  }
  else
  {
    _j2IH->hardening(ipvprev->getLocalMatrixPlasticStrain(),ipvprev->getConstRefToIPJ2IsotropicHardening(), ipvcur->getLocalMatrixPlasticStrain(),ipvcur->getRefToIPJ2IsotropicHardening());
  }
    // Yield stress
  double& R = ipvcur->getRefToCurrentViscoplasticYieldStress();
  R = ipvcur->getConstRefToIPJ2IsotropicHardening().getR();

  // no nucleation
  getNucleationLaw()->previousBasedRefresh( ipvcur->getRefToIPNucleation() , ipvprev->getConstRefToIPNucleation() );
  
  // copy void state
  ipvcur->getRefToIPVoidState() = ipvprev->getConstRefToIPVoidState();
  // coalescene from previous
  ipvcur->getRefToIPCoalescence() = ipvprev->getConstRefToIPCoalescence();

  // failure Flags governs by this laws
  ipvcur->setFailed(ipvprev->isFailed());

  // Init other variables/objects
  STensor3& Ee = ipvcur->getRefToElasticDeformation();
  STensor3& corKir = ipvcur->getRefToCorotationalKirchhoffStress();

  static STensor3 kcorprDev, Fe, Fepr, Ce, Cepr, Epr;
  static double ppr;
  static STensor43 Lepr, Le;
  static STensor63 dLepr, dLe;

  static STensor3 corKirpr;
  static STensor43 DcorKirprDEpr,DkcorprDevDEpr,DcorKirDEpr, DFpDEpr;
  static STensor3 DcorKirDtildefV,DpprDEpr, DFpDtildefV, DfVDEpr;
  static STensor3 DcorKirDT, DcorKirprDT, DkcorprDevDT;
  static STensor3 DFpDT;
  static double dpprDT;

  
  ipvcur->getRefToConstitutiveSuccessFlag() = true;
  // Compute kcor, Fp and fv with an elastic predictor and eventually a plastic corrector
  // Elastic predictor
  bool okElast = elasticPredictor(F1,*Fp0,corKirpr,kcorprDev,ppr,DcorKirprDEpr,DkcorprDevDEpr,DpprDEpr,Fepr,Cepr,Epr,Lepr,dLepr,stiff,
                  &T,&DcorKirprDT,&DkcorprDevDT,&dpprDT);
  if (!okElast)
  {
    P(0,0) = P(1,1) = P(2,2)= sqrt(-1.); // to exist NR and performed next step with a smaller incremenet
    ipvcur->getRefToConstitutiveSuccessFlag() = false;
    return;
  }
  double kCorPrEq = sqrt(1.5*kcorprDev.dotprod());

  // Elastic behaviour: set values to elastic predictor
  corKir = corKirpr;
  Fe = Fepr;
  Ce = Cepr;
  Ee = Epr;
  Le = Lepr;
  dLe = dLepr;

  if (stiff)
  {
    STensorOperation::zero(DFpDEpr);
    STensorOperation::zero(DcorKirDtildefV);
    STensorOperation::zero(DFpDtildefV);
    STensorOperation::zero(DfVDEpr);
    STensorOperation::zero(DFpDT);//--------added
    DcorKirDEpr = DcorKirprDEpr;
    DcorKirDT = DcorKirprDT;
    dLocalPorosityDNonLocalPorosity = 0.;

    ipvcur->getRefToDPlasticEnergyDMatrixPlasticStrain() = 0.;
    STensorOperation::zero(ipvcur->getRefToDPlasticEnergyDF());
    if (isThermomechanicallyCoupled())
    {
      (*dLocalPorosityDT)=0.;
      STensorOperation::zero(ipvcur->getRefToDPlasticEnergyDT());
    }
    for (int i=0; i< _numNonLocalVar; i++)
    {
      STensorOperation::zero(ipvcur->getRefToDPlasticEnergyDNonLocalVariable(i));
    }
  }

  // Transition and plastic corrector management
  bool plastic = false;     // = true if plastic correction occured
  bool correctorOK = true;  // = true if corrector is successfull

  if (ipvcur->getNonLocalToLocal())
  {
    double yieldF = 0.;
    if (_stressFormulation == CORO_KIRCHHOFF)
    {
      yieldF = yieldFunction(kCorPrEq,ppr,R,ipvprev->getYieldPorosity(),ipvprev,ipvcur,&T);
    }
    else if (_stressFormulation == CORO_CAUCHY)
    {
      double detF = STensorOperation::determinantSTensor3(F1);
      double sigCorPrEq = kCorPrEq/detF;
      double presCorPr  = ppr/detF;
      yieldF =  yieldFunction(sigCorPrEq,presCorPr,R,ipvprev->getYieldPorosity(),ipvprev,ipvcur,&T);
    }
    else
    {
      Msg::Error("stress method %d is not correctly defined",_stressFormulation);
    }
    plastic = (yieldF > _tol);

    if (plastic)
    {
      double DfVtildeStarDtildefV = ipvprev->getConstRefToIPCoalescence().getAccelerateRate();
      correctorOK = plasticCorrectorLocal(F1,ipvprev->getCorrectedPorosity(),DfVtildeStarDtildefV,corKirpr,kcorprDev,ppr,ipvprev,ipvcur,
                        Fe,Ce,Ee,Le,dLe,
                        stiff,DcorKirprDEpr,DkcorprDevDEpr,DpprDEpr,
                        DcorKirDEpr, DFpDEpr,
                        &T, &DcorKirprDT,&DkcorprDevDT,&dpprDT,
                        &DcorKirDT,&DFpDT);

      if (!correctorOK and _withSubstepping){
        // Start substepping
        int numSubStep = 4;
        int numAttempt = 0;
        static STensor3 dF, Fcur;
        dF = F1;
        dF -= F0;
        //! \todo check this line : it seems to bring full bullshit !!!!!!!
        static IPNonLocalPorosity ipvTemp(*dynamic_cast<const IPNonLocalPorosity*>(ipvprev));
        double dT,Tcur;//----temperature for substepping
        dT = T-T0;//--------added to substep the temperature
        while (true){
          bool success = true;
          ipvTemp.operator =(*dynamic_cast<const IPVariable*>(ipvprev)); // take privious value
          int iter = 0;

          while (iter < numSubStep){

            iter++;
            double fact = ((double)iter)/(double)numSubStep;
            //Msg::Info("substepping step %d of %d fact = %e",iter,numSubStep,fact);
            bool estimateStiff = false;
            if (iter == numSubStep){
              estimateStiff = stiff;
            }
            // kinematic variables between current and previous states
            Fcur = F0;
            Fcur.daxpy(dF,fact);
            Tcur = T0+ fact*dT;

            //elastic predictor
            Fp0 = &ipvTemp.getConstRefToFp();
						// Fp
						ipvcur->getRefToFp() = ipvTemp.getConstRefToFp();
						// start
            okElast= elasticPredictor(Fcur,*Fp0,corKirpr,kcorprDev,ppr,DcorKirprDEpr,DkcorprDevDEpr,DpprDEpr,Fepr,Cepr,Epr,Lepr,dLepr,estimateStiff,
                             &Tcur, &DcorKirprDT,&DkcorprDevDT,&dpprDT);
            if (!okElast)
            {
              P(0,0) = P(1,1) = P(2,2)= sqrt(-1.); // to exist NR and performed next step with a smaller incremenet
              ipvcur->getRefToConstitutiveSuccessFlag() = false;
              return;
            }
            //check yield condition
            kCorPrEq = sqrt(1.5*kcorprDev.dotprod());
            if (STensorOperation::isnan(kCorPrEq)){
              success = false;  // shortcut plastic return if tauEqPr is a nan.
              break;
            }

            R = ipvTemp.getConstRefToIPJ2IsotropicHardening().getR();
            if (_stressFormulation == CORO_KIRCHHOFF)
            {
              yieldF = yieldFunction(kCorPrEq,ppr,R,ipvTemp.getYieldPorosity(),&ipvTemp,ipvcur,&Tcur);
            }
            else if (_stressFormulation == CORO_CAUCHY)
            {
              double detF = STensorOperation::determinantSTensor3(F1);
              double sigCorPrEq = kCorPrEq/detF;
              double presCorPr  = ppr/detF;
              yieldF =  yieldFunction(sigCorPrEq,presCorPr,R,ipvTemp.getYieldPorosity(),&ipvTemp,ipvcur,&Tcur);
            }
            else
            {
              Msg::Error("stress method %d is not correctly defined",_stressFormulation);
            }
            
            plastic = (yieldF > _tol);

            if (plastic){
              //Msg::Info("plastic occurs");
              correctorOK = plasticCorrectorLocal(Fcur,ipvTemp.getCorrectedPorosity(),DfVtildeStarDtildefV,corKirpr,kcorprDev,ppr,&ipvTemp,ipvcur,
                        Fe,Ce,Ee,Le,dLe,
                        estimateStiff,DcorKirprDEpr,DkcorprDevDEpr,DpprDEpr,
                        DcorKirDEpr, DFpDEpr,
                        &Tcur, &DcorKirprDT,&DkcorprDevDT,&dpprDT,
                        &DcorKirDT,&DFpDT);
							if (!correctorOK){
								success = false;
              	break;
							}
            }

            // next step
            if (iter < numSubStep){
              ipvTemp.operator =(*dynamic_cast<const IPVariable*>(ipvcur));
            }
          }
          if (!success){
            numSubStep *= 2;
            numAttempt++;
            //Msg::Warning("substeping is not converged, decrease number of step");
          }
          else{
            #ifdef _DEBUG
            Msg::Info("mlawNonLocalPorous::predictorCorrector: successful substepping with %d substeps", numSubStep);
            #endif //_DEBUG
            correctorOK = true;
            //plastic = true; // not sure that the last step is purely plastic
            break;
          }
          if (numAttempt > _maxAttemptSubstepping){
            #ifdef _DEBUG
            Msg::Warning("mlawNonLocalPorous::predictorCorrector: failed substepping with %d substeps", numSubStep/2);
            #endif //_DEBUG
            correctorOK = false;
            plastic = true;
            break;
          }
        }
      }
    }
  }
  else
  {
    // Manage damage evolution and blocking behaviour
    bool elasticFollowedBlocked = false;
    bool plasticFollowedBlocked = false;

    if (ipvcur->dissipationIsBlocked())
    {
      if (_postDamageBlockingBehavior == ELASTIC){
        elasticFollowedBlocked = true;  // Elastic behaviour only
        plasticFollowedBlocked = false;
      }
      else if (_postDamageBlockingBehavior == ELASTOPLASTIC){
        elasticFollowedBlocked = false; // Plastic behaviour without damaging
        plasticFollowedBlocked = true;
      }
      else{
        Msg::Error("method %d has not been implemented",_postDamageBlockingBehavior);
      }
    }

    if (!elasticFollowedBlocked){
        // non-local porosity (do nothing as it is an input)
      double tildefV  = ipvcur->getNonLocalPorosity();
      double tildefVPrev = ipvprev->getNonLocalPorosity();
        // corrected value: applied non-local increment
      double fVtildeStarPrev = ipvprev->getCorrectedPorosity();
      double& fVtildeStar = ipvcur->getRefToCorrectedPorosity();
      double DfVtildeStarDtildefVPrev = ipvprev->getConstRefToIPCoalescence().getAccelerateRate();
      fVtildeStar = fVtildeStarPrev + (DfVtildeStarDtildefVPrev*(tildefV - tildefVPrev));

        // Effective value
      double& yieldfV = ipvcur->getRefToYieldPorosity(); // value
      double DyieldfVDDtildefV; // rate
      if (plasticFollowedBlocked)
      {
        // If damage is blocked but not plasticity: copy-paste previous values
        yieldfV = ipvprev->getYieldPorosity();
        DyieldfVDDtildefV = 0.;
      }
      else
      {
        // If damage is allowed: get regularised evolution
        yieldfV = _regularisationFunc_porosityEff->getVal(fVtildeStar);
        DyieldfVDDtildefV = _regularisationFunc_porosityEff->getDiff(fVtildeStar)*DfVtildeStarDtildefVPrev;
        // re update coalescene data after update yield porosity
        voidStateGrowth(ipvprev->getYieldPorosity(),ipvcur->getYieldPorosity(),0.,0.,0.,ipvprev,ipvcur);
      }

      double yieldF = 0.;
      if (_stressFormulation == CORO_KIRCHHOFF)
      {
        yieldF = yieldFunction(kCorPrEq,ppr,R,yieldfV,ipvprev,ipvcur,&T);
      }
      else if (_stressFormulation == CORO_CAUCHY)
      {
        double detF = STensorOperation::determinantSTensor3(F1);
        double sigCorPrEq = kCorPrEq/detF;
        double presCorPr  = ppr/detF;
        yieldF =  yieldFunction(sigCorPrEq,presCorPr,R,yieldfV,ipvprev,ipvcur,&T);
      }
      else
      {
        Msg::Error("stress method %d is not correctly defined",_stressFormulation);
      }
      plastic = (yieldF > _tol);

      if (plastic){
        correctorOK = plasticCorrector_NonLocalPorosity(F1,yieldfV,DyieldfVDDtildefV,corKirpr,kcorprDev,ppr,ipvprev,ipvcur,
                        Fe,Ce,Ee,Le,dLe,stiff,DcorKirprDEpr,DkcorprDevDEpr,DpprDEpr,
                        DcorKirDEpr,DcorKirDtildefV,
                        DFpDEpr,DFpDtildefV,
                        DfVDEpr,dLocalPorosityDNonLocalPorosity,
                        &T,&DcorKirprDT,&DkcorprDevDT,&dpprDT,&DcorKirDT,&DFpDT,dLocalPorosityDT);

        if (!correctorOK and _withSubstepping){
          //Msg::Info("start substepping");
          // substepping devide by 0 untile converge
          int numSubStep = 2;
          int numAttempt = 0;
          static STensor3 dF, Fcur;
          dF = F1;
          dF -= F0;
          double dNonlocalVar = ipvcur->getNonLocalPorosity() - ipvprev->getNonLocalPorosity();
          static IPNonLocalPorosity ipvTemp(*dynamic_cast<const IPNonLocalPorosity*>(ipvprev));
          double dT,Tcur;//---------added for substepping temperature
          dT = T - T0;
          while (true){
            bool success = true;
            int iter = 0;
            ipvTemp.operator =(*dynamic_cast<const IPVariable*>(ipvprev)); // take privious value

            while (iter < numSubStep){
              iter++;
              double fact = ((double)iter)/(double)numSubStep;
              //Msg::Info("substepping step %d of %d fact = %e",iter,numSubStep,fact);
              bool estimateStiff = false;
              if (iter == numSubStep){
                estimateStiff = stiff;
              }
              // kinematic variables between current and previous states
              Fcur = F0;
              Fcur.daxpy(dF,fact);
              ipvcur->getRefToNonLocalPorosity() = ipvprev->getNonLocalPorosity()+ fact*dNonlocalVar;
              Tcur = T0 + fact*dT;//------for temperature substepping

              //elastic predictor
              Fp0 = &ipvTemp.getConstRefToFp();
              // Fp
							ipvcur->getRefToFp() = ipvTemp.getConstRefToFp();
              okElast = elasticPredictor(Fcur,*Fp0,corKirpr,kcorprDev,ppr,DcorKirprDEpr,DkcorprDevDEpr,DpprDEpr,Fepr,Cepr,Epr,Lepr,dLepr,estimateStiff,
                              &Tcur,&DcorKirprDT,&DkcorprDevDT,&dpprDT);
              if (!okElast)
              {
                ipvcur->getRefToConstitutiveSuccessFlag() = false;
                P(0,0) = P(1,1) = P(2,2)= sqrt(-1.); // to exist NR and performed next step with a smaller incremenet
                return;
              }
              //check yield condition
              kCorPrEq = sqrt(1.5*kcorprDev.dotprod());
              if (STensorOperation::isnan(kCorPrEq)){
                success = false; // shortcut plastic return if tauEqPr is a nan.
                break;
              }
              tildefV  = ipvcur->getNonLocalPorosity();
              fVtildeStar = fVtildeStarPrev+ (DfVtildeStarDtildefVPrev*(tildefV - tildefVPrev));
              // update true yield porosity and coalescene data if damage is not blocked
              if (plasticFollowedBlocked){
                yieldfV = ipvprev->getYieldPorosity();
                DyieldfVDDtildefV = 0.;
              }
              else{
                yieldfV = _regularisationFunc_porosityEff->getVal(fVtildeStar);
                DyieldfVDDtildefV = _regularisationFunc_porosityEff->getDiff(fVtildeStar)*DfVtildeStarDtildefVPrev;
                voidStateGrowth(ipvTemp.getYieldPorosity(),ipvcur->getYieldPorosity(),0.,0.,0.,&ipvTemp,ipvcur);
              }

              R = ipvTemp.getConstRefToIPJ2IsotropicHardening().getR();
              if (_stressFormulation == CORO_KIRCHHOFF)
              {
                yieldF = yieldFunction(kCorPrEq,ppr,R,yieldfV,&ipvTemp,ipvcur,&Tcur);
              }
              else if (_stressFormulation == CORO_CAUCHY)
              {
                double detF = STensorOperation::determinantSTensor3(F1);
                double sigCorPrEq = kCorPrEq/detF;
                double presCorPr  = ppr/detF;
                yieldF =  yieldFunction(sigCorPrEq,presCorPr,R,yieldfV,&ipvTemp,ipvcur,&Tcur);
              }
              else
              {
                Msg::Error("stress method %d is not correctly defined",_stressFormulation);
              }
              plastic = (yieldF > _tol);

              if (plastic){
                correctorOK = plasticCorrector_NonLocalPorosity(Fcur,yieldfV,DyieldfVDDtildefV,corKirpr,kcorprDev,ppr,&ipvTemp,ipvcur,
                        Fe,Ce,Ee,Le,dLe,estimateStiff,DcorKirprDEpr,DkcorprDevDEpr,DpprDEpr,
                        DcorKirDEpr,DcorKirDtildefV,
                        DFpDEpr,DFpDtildefV,
                        DfVDEpr,dLocalPorosityDNonLocalPorosity,
                        &Tcur,&DcorKirprDT,&DkcorprDevDT,&dpprDT,&DcorKirDT,&DFpDT,dLocalPorosityDT);

								if (!correctorOK){
									success = false;
                	break;
								}
              }

             // next step
              if (iter < numSubStep){
                ipvTemp.operator =(*dynamic_cast<const IPVariable*>(ipvcur));
              }
            }

            if (!success){
              numSubStep *= 2;
              numAttempt++;
              //Msg::Warning("substeping is not converged, decrease number of step");
            }
            else{
              #ifdef _DEBUG
              Msg::Info("mlawNonLocalPorous::predictorCorrector: successful substepping with %d substeps", numSubStep);
              #endif //_DEBUG
              correctorOK = true;
              //plastic = true; // not sure that the last substep is plastic
              break;
            }

            if (numAttempt > _maxAttemptSubstepping){
              #ifdef _DEBUG
              Msg::Warning("mlawNonLocalPorous::predictorCorrector: failed substepping with %d substeps", numSubStep/2);
              #endif //_DEBUG
              correctorOK = false;
              plastic = true;
              break;
            }
          }
        }
      }
    }
  }

  if (plastic and !correctorOK){
    // In case of failed plastic correction
    ipvcur->getRefToConstitutiveSuccessFlag() = false;
    P(0,0) = sqrt(-1.); // to exist NR and performed next step with a smaller incremenet
    return;
  }



  // Post traitment based on new values of kcor, Fp and fV.
  // update first PK stress from kcor and Fp
  static STensor3 S, FeS, invFp;
  STensorOperation::inverseSTensor3(Fp1,invFp);
  STensorOperation::multSTensor3STensor43(corKir,Le,S);
  STensorOperation::multSTensor3(Fe,S,FeS);
  STensorOperation::multSTensor3SecondTranspose(FeS,invFp,P);


  // update elastic energy
  ipvcur->getRefToElasticEnergy()= this->deformationEnergy(Ce,&T);
  if (this->getMacroSolver()->withPathFollowing()){
    // irreversible energy
    if ((this->getMacroSolver()->getPathFollowingLocalIncrementType() == pathFollowingManager::PLASTIC_ENERGY) or
             (this->getMacroSolver()->getPathFollowingLocalIncrementType() == pathFollowingManager::DISSIPATION_ENERGY)) {
      ipvcur->getRefToIrreversibleEnergy() = ipvprev->irreversibleEnergy()+ ipvcur->plasticEnergy() - ipvprev->plasticEnergy();
    }
    else{
      ipvcur->getRefToIrreversibleEnergy() = 0.;
    }
  };

  ipvcur->getRefToDissipationActive() = plastic;

  // Stiffness computation
  if (stiff)
  {
    static STensor3 invFp0;
    STensorOperation::inverseSTensor3(*Fp0,invFp0);
    static STensor43 EprToF;
    for (int i=0; i<3; i++){
      for (int j=0; j<3; j++){
        for (int k=0; k<3; k++){
          for (int l=0; l<3; l++){
            EprToF(i,j,k,l) = 0.;
            for (int p=0; p<3; p++){
              for (int q=0; q<3; q++){
                EprToF(i,j,k,l) += Lepr(i,j,p,q)*Fepr(k,p)*invFp0(l,q);
              }
            }
          }
        }
      }
    }

    if (ipvcur->getNonLocalToLocal())
    {
      if(isThermomechanicallyCoupled()){
        tangentComputationLocalWithTemperature(Tangent,*dStressDT, plastic, F1, corKir, S, Fepr, *Fp0, Lepr,
                      Fe, Fp1, Le, dLe,
                      DcorKirDEpr,DcorKirDT, DFpDEpr,DFpDT, EprToF, invFp); //-----------added

      }
      else{
        tangentComputationLocal(Tangent, plastic,F1, corKir, S, Fepr, *Fp0, Lepr,
                      Fe, Fp1, Le, dLe,
                      DcorKirDEpr, DFpDEpr, EprToF, invFp);
      }

      STensorOperation::zero(dStressDNonLocalPorosity);
      dLocalPorosityDNonLocalPorosity = 0.;
    }
    else{
      if(isThermomechanicallyCoupled()){
         tangentComputationWithTemperature(dFedF,dFeDtildefV,Tangent,dStressDNonLocalPorosity,*dStressDT, dLocaldPorosityDStrain,
                                           plastic,F1,corKir, S, Fepr,  *Fp0, Lepr,
                                           Fe,Fp1,Le, dLe,
                                           DcorKirDEpr, DcorKirDtildefV,DcorKirDT,DFpDEpr, DFpDtildefV,DFpDT, DfVDEpr,
                                           EprToF, invFp);//--------------------added
      }
      else{
         tangentComputation(dFedF,dFeDtildefV,Tangent,dStressDNonLocalPorosity, dLocaldPorosityDStrain,
                                           plastic,F1,corKir, S, Fepr,  *Fp0, Lepr,
                                           Fe,Fp1,Le, dLe,
                                           DcorKirDEpr, DcorKirDtildefV,DFpDEpr, DFpDtildefV, DfVDEpr,
                                           EprToF, invFp);
      }

    }

    //update tangent of plastic energy
    static STensor3 DplEnergyDEpr;
    DplEnergyDEpr = ipvcur->getConstRefToDPlasticEnergyDF();
    STensor3& DplEnergyDF = ipvcur->getRefToDPlasticEnergyDF();
    STensorOperation::multSTensor3STensor43(DplEnergyDEpr,EprToF,DplEnergyDF);

    if (this->getMacroSolver()->withPathFollowing()){
      // irreversible energy
      if ((this->getMacroSolver()->getPathFollowingLocalIncrementType() == pathFollowingManager::PLASTIC_ENERGY) or
               (this->getMacroSolver()->getPathFollowingLocalIncrementType() == pathFollowingManager::DISSIPATION_ENERGY)) {
        ipvcur->getRefToDIrreversibleEnergyDF() = ipvcur->getConstRefToDPlasticEnergyDF();
        for (int i=0; i< _numNonLocalVar; i++){
          if (ipvprev->getNonLocalToLocal()){
            ipvcur->getRefToDIrreversibleEnergyDNonLocalVariable(i) = 0.;
          }
          else{
            ipvcur->getRefToDIrreversibleEnergyDNonLocalVariable(i) = ipvcur->getConstRefToDPlasticEnergyDNonLocalVariable(0);
          }
        }
      }
      else{
        Msg::Error("Path following method is only contructed with dissipation based on plastic energy");
      }

    };

  }
}





 void mlawNonLocalPorosity::voidCharacteristicsEvolution_NonLocalPorosity(const STensor3& kcor,
                                                const IPNonLocalPorosity *q0, IPNonLocalPorosity* q1,
                                                const double DeltaHatD, const double DeltaHatQ, const double DeltaHatP,
                                                double& DfVDDeltaHatD, double& DfVDDeltaHatQ, double& DfVDDeltaHatP,
                                                STensor3& DfVDkcorpr) const
{
  if (getVoidEvolutionLaw()->useYieldPorosityDuringCoalescence())
  {
    double hatPn = q0->getLocalMatrixPlasticStrain(); // previous converge value
    double fn = q0->getLocalPorosity();
    
      // Update nucleation state
    nucleation(hatPn+DeltaHatP,hatPn,fn,q1,q0);

    // void evolution
    localPorosityGrowth(q1, DeltaHatD,DeltaHatQ,DeltaHatP,
                      kcor, q0,
                      true, &DfVDDeltaHatD,&DfVDDeltaHatQ,&DfVDDeltaHatP,&DfVDkcorpr);
    
    voidStateGrowth(q0->getYieldPorosity(),q1->getYieldPorosity(),DeltaHatD,DeltaHatQ,DeltaHatP,q0,q1);    
  }
  else
  {
    // void characteristics are evaluated first, then porosity are evaluated
    // other void characteristic evolutions are called first
    // then void evolution is estimated
    // update void state
    STensorOperation::zero(DfVDkcorpr);

    voidStateGrowth(q0->getYieldPorosity(),q1->getYieldPorosity(),DeltaHatD,DeltaHatQ,DeltaHatP,q0,q1);    
    
    // local porosity
    const IPVoidState* ipvoid = &(q1->getConstRefToIPVoidState());

    double lambda = ipvoid->getVoidSpacingRatio();
    double W = ipvoid->getVoidAspectRatio();
    double Chi = ipvoid->getVoidLigamentRatio();
    double gamma = ipvoid->getVoidShapeFactor();
    
    double& fV = q1->getRefToLocalPorosity();

    fV = Chi*Chi*Chi*W/(3.*gamma*lambda); // always corrects
    // Regularise porosity growth and rates
    double regularizedRate = 1.;
    if (fV > _localfVFailure)
    {
      double fVn = q0->getLocalPorosity();
      double DeltafV = fV - fVn;
      fV = _localfVFailure;
      regularizedRate = 0.;
      if (fabs(DeltafV)> 0)
      {
        regularizedRate = (_localfVFailure - fVn)/DeltafV;
      }
    }
    
    
    // nonlocal var = [volumetric, matrix, deviatoric]
    double DfVDChi = 3.*fV*regularizedRate/Chi;
    double DfVDlambda = -fV*regularizedRate/lambda;
    double DfVDW = fV*regularizedRate/W;
    double DfVDgamma = -fV*regularizedRate/gamma;

    DfVDDeltaHatQ =  DfVDChi*ipvoid->getDVoidLigamentRatioDVolumetricPlasticDeformation() +
                                   DfVDW*ipvoid->getDVoidAspectRatioDVolumetricPlasticDeformation()+
                                   DfVDgamma*ipvoid->getDVoidShapeFactorDVolumetricPlasticDeformation()+
                                   DfVDlambda*ipvoid->getDVoidSpacingRatioDVolumetricPlasticDeformation();
    DfVDDeltaHatP =  DfVDChi*ipvoid->getDVoidLigamentRatioDMatrixPlasticDeformation() +
                                   DfVDW*ipvoid->getDVoidAspectRatioDMatrixPlasticDeformation()+
                                   DfVDgamma*ipvoid->getDVoidShapeFactorDMatrixPlasticDeformation()+
                                   DfVDlambda*ipvoid->getDVoidSpacingRatioDMatrixPlasticDeformation();
    DfVDDeltaHatD =  DfVDChi*ipvoid->getDVoidLigamentRatioDDeviatoricPlasticDeformation() +
                                   DfVDW*ipvoid->getDVoidAspectRatioDDeviatoricPlasticDeformation()+
                                   DfVDgamma*ipvoid->getDVoidShapeFactorDDeviatoricPlasticDeformation()+
                                   DfVDlambda*ipvoid->getDVoidSpacingRatioDDeviatoricPlasticDeformation();
  };
};



bool mlawNonLocalPorosity::plasticCorrector_NonLocalPorosity(const STensor3& F1, 
                                  const double& yieldfV,const double& DyieldfVDtildefV,
                                  const STensor3& Kcorpr, const STensor3& devKcorpr,  const double& pcorpr,
                                  const IPNonLocalPorosity *q0, IPNonLocalPorosity *q1,
                                  STensor3& Fe, STensor3& Ce, STensor3& Ee, STensor43& Le, STensor63& dLe,
                                  const bool stiff,
                                  const STensor43& DKcorprDEpr, const STensor43& DdevKcorprDEpr, const STensor3& DpcorprDEpr, // predictor elastic tangent
                                  STensor43& DKcorDEpr, STensor3& DKcorDtildefV,
                                  STensor43& DFpDEpr, STensor3& DFpDtildefV,
                                  STensor3& DfVDEpr, double& DfVDtildefV,
                                  const double* T,
                                  const STensor3* DKcorprDT, const STensor3* DdevKcorprDT, const double* DpcorprDT,
                                  STensor3* DKcorDT, STensor3* DFpDT, double* DfVDT
                                ) const{
  // 
  // yieldfV depends only on nonlocal porosity
  //
  double hatPn = q0->getLocalMatrixPlasticStrain(); // previous converge value
  double fn = q0->getLocalPorosity();
  // unknowns
  double DeltaHatD = 1.e-8;
  double DeltaHatQ = 0.;
  double DeltaHatP = 1.e-8;
  
  // dependence of local porosity on unknowns
  double DfVDDeltaHatD = 0.;
  double DfVDDeltaHatQ = 0.;
  double DfVDDeltaHatP = 0.;
  // and stress tensor due to kw
  static STensor3 DfVDkcorpr;

  voidCharacteristicsEvolution_NonLocalPorosity(Kcorpr,q0,q1,DeltaHatD,DeltaHatQ,DeltaHatP,
                        DfVDDeltaHatD,DfVDDeltaHatQ,DfVDDeltaHatP,DfVDkcorpr);
  
  // First residual
  static SVector3 res;
  static SVector3 sol;
  static STensor3 J, invJ;

  double & yield = q1->getRefToCurrentViscoplasticYieldStress();
  yield = q1->getConstRefToIPJ2IsotropicHardening().getR();
  double H = q1->getConstRefToIPJ2IsotropicHardening().getDR();
  
  double kcorEqpr = sqrt(1.5*devKcorpr.dotprod());
  computeResidual_NonLocalPorosity(res,J,F1,kcorEqpr,pcorpr,yield,H,yieldfV,q0,q1,
                  DeltaHatD,DeltaHatQ,DeltaHatP,T);

  double muT = _mu;
  if (isThermomechanicallyCoupled()){
    muT=shearModulus(*T);
  }


  double f = res.norm();

  int ite = 0;
  // Start iterations
  //Msg::Info("plastic corrector");
  while(f > _tol or ite <1)
  {
    // Jacobian
    if (fabs(STensorOperation::determinantSTensor3(J)) > 0)
    {
      STensorOperation::inverseSTensor3(J,invJ);
      STensorOperation::multSTensor3SVector3(invJ,res,sol);

      if (DeltaHatD -sol(0) <= 0.){
        DeltaHatD *= 0.5;
      }
      else if (DeltaHatD-sol(0) > kcorEqpr/3./muT){
        DeltaHatD = kcorEqpr/3./muT;
      }
      else{
        DeltaHatD -= sol(0);
      }

      if (-sol(1) > 1.){
        DeltaHatQ += 1.;
      }
      else if (-sol(1) < -1.){
        DeltaHatQ += -1.;
      }
      else
        DeltaHatQ -= sol(1);


      if (DeltaHatP -sol(2) < 0.){
        DeltaHatP *= 0.5;
      }
      else
        DeltaHatP -= sol(2);

      voidCharacteristicsEvolution_NonLocalPorosity(Kcorpr,q0,q1,DeltaHatD,DeltaHatQ,DeltaHatP,
                          DfVDDeltaHatD,DfVDDeltaHatQ,DfVDDeltaHatP,DfVDkcorpr);
      // update internal variable
      updateInternalVariables(q1,q0,DeltaHatD,DeltaHatQ,DeltaHatP,false,T);
   
      yield = q1->getConstRefToIPJ2IsotropicHardening().getR();
      H = q1->getConstRefToIPJ2IsotropicHardening().getDR();
      // Add strain rate effects
      if (_viscoplastlaw != NULL)
      {
        double R, dR;
        double delta_t = getTimeStep();
        _viscoplastlaw->get(DeltaHatP/delta_t,R,dR);
        yield += R;
        H += dR/delta_t;
      }

      computeResidual_NonLocalPorosity(res,J,F1,kcorEqpr,pcorpr,yield,H,yieldfV,q0,q1,
                    DeltaHatD,DeltaHatQ,DeltaHatP,T);

      f = res.norm();
      //Msg::Info("ite = %d maxite = %d norm = %e ,DeltaHatD = %e,DeltaHatQ = %e,DeltaHatP = %e !!",ite,_maxite,f,DeltaHatD,DeltaHatQ,DeltaHatP);
      if (f < _tol)  break;

      ite++;
      if((ite > _maxite) or STensorOperation::isnan(f))
      {
        #ifdef _DEBUG
        Msg::Warning("No convergence in NON-local plast. correct. ite= %d: norm= %e, DeltaHatD= %e, DeltaHatQ= %e, DeltaHatP= %e, fv_y = %e ",
                      ite,f,DeltaHatD,DeltaHatQ,DeltaHatP, q1->getYieldPorosity());
        #endif //_DEBUG
        return false;
      }
    }
    else
    {
      #ifdef _DEBUG
      Msg::Warning("No convergence at iteration %d in plasic corrector as sigulier Jacobian",ite);
      J.print("Jacobian");
      #endif //_DEBUG
      return false;
    }
  }


  static STensor3 DGNp, devDGNp;
  double trDGNp;
  static STensor43 ENp;

  updatePlasticState(q1,q0,F1,DeltaHatD,DeltaHatQ,DeltaHatP,devKcorpr,kcorEqpr,pcorpr,
                    yield,Fe,Ce,Ee,Le,dLe,DGNp,devDGNp,trDGNp,ENp,T);
  double f0=q0->getInitialPorosity();
  q1->getRefToDPlasticEnergyDMatrixPlasticStrain() =(1.-f0)*H*DeltaHatP+(1.-f0)*yield;
  /* Stiffness computation*/
  if(stiff)
  {
    double dRdT= 0.;
    if (isThermomechanicallyCoupled()){
      dRdT = q1->getConstRefToIPJ2IsotropicHardening().getDRDT();
    }

    /* Compute internal variables derivatives from residual derivatives */
    static STensor3 dres0dEpr, dres1dEpr, dres2dEpr;
    double dres0dtildefV, dres1dtildefV, dres2dtildefV;
    double dres0dT, dres1dT,dres2dT;
    
    static STensor3 DyieldfVDKcorpr(0.); // fV

    computeDResidual_NonLocalPorosity(dres0dEpr,dres1dEpr,dres2dEpr,
                  dres0dtildefV,dres1dtildefV,dres2dtildefV, F1,
                  devKcorpr,kcorEqpr,pcorpr,yield,
                  yieldfV,DyieldfVDKcorpr,DyieldfVDtildefV,
                  DKcorprDEpr,DdevKcorprDEpr,DpcorprDEpr,
                  q0,q1,
                  DeltaHatD,DeltaHatQ,DeltaHatP,
                  T,&dRdT,DdevKcorprDT,DpcorprDT,DKcorprDT,
                  &dres0dT,&dres1dT,&dres2dT);

    static STensor3 dDeltaHatDdEpr, dDeltaHatQdEpr, dDeltaHatPdEpr;

    STensorOperation::inverseSTensor3(J,invJ); // inverse last tengent operator

    for(int i=0; i<3; i++)
    {
      for(int j=0; j<3; j++)
      {
        res(0) = -dres0dEpr(i,j);
        res(1) = -dres1dEpr(i,j);
        res(2) = -dres2dEpr(i,j);
        STensorOperation::multSTensor3SVector3(invJ,res,sol);
        dDeltaHatDdEpr(i,j) = sol(0);
        dDeltaHatQdEpr(i,j) = sol(1);
        dDeltaHatPdEpr(i,j) = sol(2);
      }
    }

    // dtilde_fv
    res(0) = -dres0dtildefV;
    res(1) = -dres1dtildefV;
    res(2) = -dres2dtildefV;
    STensorOperation::multSTensor3SVector3(invJ,res,sol);
    double dDeltaHatDdtildefV = sol(0);
    double dDeltaHatQdtildefV = sol(1);
    double dDeltaHatPdtildefV = sol(2);

    double dDeltaHatDdT = 0.;
    double dDeltaHatQdT = 0.;
    double dDeltaHatPdT = 0.;

    STensor3 &DplEnergyDEpr=q1->getRefToDPlasticEnergyDF();
    STensorOperation::zero(DplEnergyDEpr);

    double &DplEnergyDtildefV=q1->getRefToDPlasticEnergyDNonLocalVariable(0);
    DplEnergyDtildefV=0.;
    DplEnergyDtildefV=(1.-f0)*H*dDeltaHatPdtildefV*DeltaHatP+(1.-f0)*yield*dDeltaHatPdtildefV;
    for(int i=0;i<3;i++){
      for(int j=0;j<3;j++){
        DplEnergyDEpr(i,j)=(1.-f0)*H*dDeltaHatPdEpr(i,j)*DeltaHatP+(1.-f0)*yield*dDeltaHatPdEpr(i,j);
      }
    }

    if(isThermomechanicallyCoupled()){
      res(0) = -dres0dT;
      res(1) = -dres1dT;
      res(2) = -dres2dT;
      STensorOperation::multSTensor3SVector3(invJ,res,sol);
      dDeltaHatDdT = sol(0);
      dDeltaHatQdT = sol(1);
      dDeltaHatPdT = sol(2);
      *DfVDT = STensorOperation::doubledot(DfVDkcorpr,*DKcorprDT)+ DfVDDeltaHatD*dDeltaHatDdT+DfVDDeltaHatQ*dDeltaHatQdT+DfVDDeltaHatP*dDeltaHatPdT;

      double &DplEnergyDT=q1->getRefToDPlasticEnergyDT();
      DplEnergyDT=(1.-f0)*(H*dDeltaHatPdT+dRdT)*DeltaHatP+(1.-f0)*yield*dDeltaHatPdT;
    }

    // compute DfVDEpr
    STensorOperation::multSTensor3STensor43(DfVDkcorpr,DKcorprDEpr,DfVDEpr);
    DfVDEpr.daxpy(dDeltaHatDdEpr,DfVDDeltaHatD);
    DfVDEpr.daxpy(dDeltaHatQdEpr,DfVDDeltaHatQ);
    DfVDEpr.daxpy(dDeltaHatPdEpr,DfVDDeltaHatP);

    // compute DfVDtildefV
    DfVDtildefV =  DfVDDeltaHatD*dDeltaHatDdtildefV+DfVDDeltaHatQ*dDeltaHatQdtildefV+DfVDDeltaHatP*dDeltaHatPdtildefV;

    computePlasticTangentNonLocal(q1,q0,devKcorpr,kcorEqpr,pcorpr,DGNp,devDGNp,trDGNp,ENp,
        DeltaHatD,DeltaHatQ,DeltaHatP,
        DKcorprDEpr,DdevKcorprDEpr,DpcorprDEpr,
        dDeltaHatDdEpr,dDeltaHatQdEpr,dDeltaHatPdEpr,
        DKcorDEpr,DFpDEpr,
        dDeltaHatDdtildefV,dDeltaHatQdtildefV,dDeltaHatPdtildefV,
        DKcorDtildefV,DFpDtildefV,
        T,DKcorprDT,DdevKcorprDT,DpcorprDT,
        &dDeltaHatDdT,&dDeltaHatQdT,&dDeltaHatPdT,
        DKcorDT,DFpDT);
  }

  return true;
};








void mlawNonLocalPorosity::predictorCorrectorLocal(
                            const STensor3& F0,         // initial deformation gradient (input @ time n)
                            const STensor3& F1,         // updated deformation gradient (input @ time n+1)
                            STensor3 &P,                // updated 1st Piola-Kirchhoff stress tensor (output)
                            const IPNonLocalPorosity *ipvprev,       // array of initial internal variable
                            IPNonLocalPorosity *ipvcur,             // updated array of internal variable (in ipvcur on output),
                            STensor43 &Tangent,         // constitutive tangents (output)
                            const bool stiff            // if true compute the tangents
                           ) const
{
  // Porosity
  ipvcur->getRefToLocalPorosity() = ipvprev->getLocalPorosity();
  // Fp
  ipvcur->getRefToFp() = ipvprev->getConstRefToFp();
  ipvcur->getRefToLocalMatrixPlasticStrain() = ipvprev->getLocalMatrixPlasticStrain();
  ipvcur->getRefToPlasticEnergy() = ipvprev->plasticEnergy();
  ipvcur->getRefToCorrectedPorosity() =  ipvprev->getCorrectedPorosity();
  ipvcur->getRefToYieldPorosity() = ipvprev->getYieldPorosity();
  ipvcur->getRefToLocalVolumetricPlasticStrain() = ipvprev->getLocalVolumetricPlasticStrain();
  ipvcur->getRefToLocalDeviatoricPlasticStrain() = ipvprev->getLocalDeviatoricPlasticStrain();

  // starting hardening data
	if (isThermomechanicallyCoupled()){
    Msg::Error("local model mlawNonLocalPorosity::predictorCorrectorLocal has not been implemented with thermomechanics");
  }
  else{
    _j2IH->hardening(ipvprev->getLocalMatrixPlasticStrain(),ipvprev->getConstRefToIPJ2IsotropicHardening(), ipvcur->getLocalMatrixPlasticStrain(),ipvcur->getRefToIPJ2IsotropicHardening());
  }

  // no nucleation
  getNucleationLaw()->previousBasedRefresh( ipvcur->getRefToIPNucleation() , ipvprev->getConstRefToIPNucleation() );

  ipvcur->getRefToIPVoidState() = (ipvprev->getConstRefToIPVoidState());
  // reset to previous state
  ipvcur->getRefToIPVoidState() = (ipvprev->getConstRefToIPVoidState());
  ipvcur->getRefToIPCoalescence() = (ipvprev->getConstRefToIPCoalescence());
  // failure state
  ipvcur->setFailed(ipvprev->isFailed());

	double& R = ipvcur->getRefToCurrentViscoplasticYieldStress();
	R = ipvcur->getConstRefToIPJ2IsotropicHardening().getR();

  const STensor3* Fp0 = &ipvprev->getConstRefToFp();
  STensor3& Fp1 = ipvcur->getRefToFp();
  double& fV1 = ipvcur->getRefToLocalPorosity();
  STensor3& Ee = ipvcur->getRefToElasticDeformation();
	STensor3& corKir = ipvcur->getRefToCorotationalKirchhoffStress();

  // elastic predictor
  static STensor3 kcorprDev, Fe, Fepr, Ce, Cepr, Epr;
  static double ppr;
  static STensor43 Lepr, Le;
  static STensor63 dLepr, dLe;

  static STensor3 corKirpr, DpprDEpr;
  static STensor43 DcorKirprDEpr,DkcorprDevDEpr,DcorKirDEpr, DFpDEpr;

  bool correctorOK = false;
  bool plastic = false;
  ipvcur->getRefToConstitutiveSuccessFlag() = true;

  // elastic predictor
  bool okElast = this->elasticPredictor(F1,*Fp0,corKirpr,kcorprDev,ppr,DcorKirprDEpr,DkcorprDevDEpr,DpprDEpr,Fepr,Cepr,Epr,Lepr,dLepr,stiff);
  if (!okElast)
  {
    ipvcur->getRefToConstitutiveSuccessFlag() = false;
    P(0,0) = P(1,1) = P(2,2)= sqrt(-1.); // to exist NR and performed next step with a smaller incremenet
    return;
  }
  // elastic only
  corKir = corKirpr;
  Fe = Fepr;
  Ce = Cepr;
  Ee = Epr;
  Le = Lepr;
  dLe = dLepr;

  if (stiff){
    STensorOperation::zero(DFpDEpr);
    DcorKirDEpr = DcorKirprDEpr;

    ipvcur->getRefToDPlasticEnergyDMatrixPlasticStrain() = 0.;
    STensorOperation::zero(ipvcur->getRefToDPlasticEnergyDF());
    if (isThermomechanicallyCoupled()){
      STensorOperation::zero(ipvcur->getRefToDPlasticEnergyDT());
    }
  }

  if (!ipvcur->dissipationIsBlocked()){

    double kCorPrEq = sqrt(1.5*kcorprDev.dotprod());
    // compute correctted value for yield surface
    double fVStarPrev = ipvprev->getCorrectedPorosity();
    double DfVStarDtildefVPrev = ipvprev->getConstRefToIPCoalescence().getAccelerateRate();

    // check with previous value
    double yieldfV = ipvprev->getYieldPorosity();
    double yieldF = 0.;
    if (_stressFormulation == CORO_KIRCHHOFF)
    {
      yieldF = yieldFunction(kCorPrEq,ppr,R,yieldfV,ipvprev,ipvcur);
    }
    else if (_stressFormulation == CORO_CAUCHY)
    {
      double detF = STensorOperation::determinantSTensor3(F1);
      double sigCorPrEq = kCorPrEq/detF;
      double presCorPr  = ppr/detF;
      yieldF =  yieldFunction(sigCorPrEq,presCorPr,R,yieldfV,ipvprev,ipvcur);
    }
    else
    {
      Msg::Error("stress method %d is not correctly defined",_stressFormulation);
    }
    plastic = (yieldF > _tol);

    if (plastic){
      correctorOK = plasticCorrectorLocal(F1,fVStarPrev,DfVStarDtildefVPrev,corKirpr,kcorprDev,ppr,ipvprev,ipvcur,
                        Fe,Ce,Ee,Le,dLe,stiff,DcorKirprDEpr,DkcorprDevDEpr,DpprDEpr,
                        DcorKirDEpr, DFpDEpr);


      if (!correctorOK and _withSubstepping){
        // Msg::Info("start substepping");
        // substepping devide by 0 untile converge
        int numSubStep = 2.;
        int numAttempt = 0;
        static STensor3 dF, Fcur;
        dF = F1;
        dF -= F0;
        static IPNonLocalPorosity ipvTemp(*dynamic_cast<const IPNonLocalPorosity*>(ipvprev));

        while (true){
          bool success = true;
          int iter = 0;
          ipvTemp.operator =(*dynamic_cast<const IPVariable*>(ipvprev)); // take privious value

          while (iter < numSubStep){

            iter++;
            double fact = ((double)iter)/(double)numSubStep;
            //Msg::Info("substepping step %d of %d fact = %e",iter,numSubStep,fact);
            bool estimateStiff = false;
            if (iter == numSubStep){
              estimateStiff = stiff;
            }
            // kinematic varaible
            Fcur = F0;
            Fcur.daxpy(dF,fact);

            //elastic predictor
            Fp0 = &ipvTemp.getConstRefToFp();
						// Fp
						ipvcur->getRefToFp() = ipvTemp.getConstRefToFp();
            okElast =  elasticPredictor(Fcur,*Fp0,corKirpr,kcorprDev,ppr,DcorKirprDEpr,DkcorprDevDEpr,DpprDEpr,Fepr,Cepr,Epr,Lepr,dLepr,estimateStiff);
            if (!okElast)
            {
              ipvcur->getRefToConstitutiveSuccessFlag() = false;
              P(0,0) = P(1,1) = P(2,2)= sqrt(-1.); // to exist NR and performed next step with a smaller incremenet
              return;
            }
            //check yield condition
            kCorPrEq = sqrt(1.5*kcorprDev.dotprod());

            kCorPrEq = sqrt(1.5*kcorprDev.dotprod());
            R = ipvTemp.getConstRefToIPJ2IsotropicHardening().getR();
            if (_stressFormulation == CORO_KIRCHHOFF)
            {
              yieldF = yieldFunction(kCorPrEq,ppr,R,ipvTemp.getYieldPorosity(),&ipvTemp,ipvcur);
            }
            else if (_stressFormulation == CORO_CAUCHY)
            {
              double detF = STensorOperation::determinantSTensor3(F1);
              double sigCorPrEq = kCorPrEq/detF;
              double presCorPr  = ppr/detF;
              yieldF =  yieldFunction(sigCorPrEq,presCorPr,R,ipvTemp.getYieldPorosity(),&ipvTemp,ipvcur);
            }
            else
            {
              Msg::Error("stress method %d is not correctly defined",_stressFormulation);
            }
            
            plastic = (yieldF > _tol);

            if (plastic){
              //Msg::Info("plastic occurs");
              correctorOK = plasticCorrectorLocal(Fcur,ipvTemp.getCorrectedPorosity(),DfVStarDtildefVPrev,corKirpr,kcorprDev,ppr,&ipvTemp,ipvcur,
                        Fe,Ce,Ee,Le,dLe,
                        estimateStiff,DcorKirprDEpr,DkcorprDevDEpr,DpprDEpr,
                        DcorKirDEpr, DFpDEpr);
							if (!correctorOK){
								success = false;
              	break;
							}
            }

						// next step
						if (iter < numSubStep){
              ipvTemp.operator =(*dynamic_cast<const IPVariable*>(ipvcur));
            }
          }
          if (!success){
            numSubStep *= 2;
            numAttempt++;
            // Msg::Warning("substeping is not converged, decrease number of step");
          }
          else{
            #ifdef _DEBUG
            Msg::Info("mlawNonLocalPorous::predictorCorrector: successful substepping with %d substeps", numSubStep);
            #endif //_DEBUG
						correctorOK = true;
            plastic = true;
            break;
          }
          if (numAttempt > _maxAttemptSubstepping){
            #ifdef _DEBUG
            Msg::Warning("mlawNonLocalPorous::predictorCorrector: failed substepping with %d substeps", numSubStep);
            #endif //_DEBUG
            correctorOK = false;
            plastic = true;
            break;
          }
        }
      }
    }
    if (plastic and !correctorOK){
      ipvcur->getRefToConstitutiveSuccessFlag() = false;
      P(0,0) = sqrt(-1.); // to exist NR and performed next step with a smaller incremenet
      return;
    }
  }

  // update first PK stress
  static STensor3 S, FeS, invFp;
  STensorOperation::inverseSTensor3(Fp1,invFp);
  STensorOperation::multSTensor3STensor43(corKir,Le,S);
  STensorOperation::multSTensor3(Fe,S,FeS);
  STensorOperation::multSTensor3SecondTranspose(FeS,invFp,P);

  // elastic energy
  ipvcur->getRefToElasticEnergy()= this->deformationEnergy(Ce);
  if (this->getMacroSolver()->withPathFollowing()){
    // irreversible energy
    if ((this->getMacroSolver()->getPathFollowingLocalIncrementType() == pathFollowingManager::PLASTIC_ENERGY) or
             (this->getMacroSolver()->getPathFollowingLocalIncrementType() == pathFollowingManager::DISSIPATION_ENERGY)) {
      ipvcur->getRefToIrreversibleEnergy() = ipvprev->irreversibleEnergy()+ ipvcur->plasticEnergy() - ipvprev->plasticEnergy();
    }
    else{
      ipvcur->getRefToIrreversibleEnergy() = 0.;
    }
  };

  ipvcur->getRefToDissipationActive() = plastic;

  if (stiff){
    static STensor3 invFp0;
    STensorOperation::inverseSTensor3(*Fp0,invFp0);
    static STensor43 EprToF;
    for (int i=0; i<3; i++){
      for (int j=0; j<3; j++){
        for (int k=0; k<3; k++){
          for (int l=0; l<3; l++){
            EprToF(i,j,k,l) = 0.;
            for (int p=0; p<3; p++){
              for (int q=0; q<3; q++){
                EprToF(i,j,k,l) += Lepr(i,j,p,q)*Fepr(k,p)*invFp0(l,q);
              }
            }
          }
        }
      }
    }

    tangentComputationLocal(Tangent, plastic,F1,corKir, S, Fepr,  *Fp0, Lepr,
                      Fe,Fp1,Le, dLe,
                      DcorKirDEpr, DFpDEpr, EprToF, invFp);


    //update tangent of plastic energy
    static STensor3 DplEnergyDEpr;
    DplEnergyDEpr = ipvcur->getConstRefToDPlasticEnergyDF();
    STensor3& DplEnergyDF = ipvcur->getRefToDPlasticEnergyDF();
    STensorOperation::multSTensor3STensor43(DplEnergyDEpr,EprToF,DplEnergyDF);

    if (this->getMacroSolver()->withPathFollowing()){
      // irreversible energy
      if ((this->getMacroSolver()->getPathFollowingLocalIncrementType() == pathFollowingManager::PLASTIC_ENERGY) or
               (this->getMacroSolver()->getPathFollowingLocalIncrementType() == pathFollowingManager::DISSIPATION_ENERGY)) {
        ipvcur->getRefToDIrreversibleEnergyDF() = ipvcur->getConstRefToDPlasticEnergyDF();
      }
      else{
        Msg::Error("Path following method is only contructed with dissipation based on plastic energy");
      }
    };
  }
};

void mlawNonLocalPorosity::computeResidualLocal(SVector3& res, STensor3& J, const STensor3& F,
                const double kcorEqpr, const double pcorpr, const double R, const double H,
                const double yieldfV,  const double DyieldfVDfV,
                const IPNonLocalPorosity *q0, const IPNonLocalPorosity* q1,
                const double DeltaHatD, const double DeltaHatQ, const double DeltaHatP, // 3 unknonwn
                const double DDeltafVDDeltaHatD, const double DDeltafVDDeltaHatQ, const double DDeltafVDDeltaHatP,
                const double* T
              ) const
{
  // Get ipv
  const IPVoidState* voidState = &q1->getConstRefToIPVoidState();

  // Get values
  double Chi = voidState->getVoidLigamentRatio();
  double DChiDDeltafV  = voidState->getDVoidLigamentRatioDYieldPorosity()*DyieldfVDfV;
  double DChiDDeltaHatD = voidState->getDVoidLigamentRatioDDeviatoricPlasticDeformation();
  double DChiDDeltaHatQ = voidState->getDVoidLigamentRatioDVolumetricPlasticDeformation();
  double DChiDDeltaHatP = voidState->getDVoidLigamentRatioDMatrixPlasticDeformation();

  double W = voidState->getVoidAspectRatio();
  double DWDDeltafV  = voidState->getDVoidAspectRatioDYieldPorosity()*DyieldfVDfV;
  double DWDDeltaHatD = voidState->getDVoidAspectRatioDDeviatoricPlasticDeformation();
  double DWDDeltaHatQ = voidState->getDVoidAspectRatioDVolumetricPlasticDeformation();
  double DWDDeltaHatP = voidState->getDVoidAspectRatioDMatrixPlasticDeformation();
  

  double f0 = q1->getInitialPorosity();
  double R0 = _j2IH->getYield0();


  // Get elastic parameters
  double KT= _K;
  double muT = _mu;
  if (isThermomechanicallyCoupled()){
    KT = bulkModulus(*T);
    muT =shearModulus(*T);
  };

  // Corrected stress invariants
  double kcorEq =  kcorEqpr - 3.*muT*DeltaHatD;
  double pcor = pcorpr - KT*DeltaHatQ;
  double detF = 1.;
  if (_stressFormulation == CORO_CAUCHY)
  {
    detF = STensorOperation::determinantSTensor3(F);
    kcorEq /= detF;
    pcor /= detF;
  }

  // grad f, Ns, Nf with respect to [kcorEq pcor R fV Chi W]
  static fullVector<double> gradf(6), gradNs(6), gradNv(6);
  
  // Residues
  // 1. yield function
  res(0) = yieldFunction(kcorEq,pcor,R,yieldfV,q0,q1,T,true,&gradf);


  // 2. plastic flow normality
  double Ns, Nv; // plastic flow components
  plasticFlow(Ns,Nv,kcorEq,pcor,R,q0,q1,T,true,&gradNs,&gradNv);
  res(1) = R0*(DeltaHatD*Nv- DeltaHatQ*Ns);
  
  
  double Jp = 1.;
  if (_stressFormulation == CORO_KIRCHHOFF)
  {
    double Jp0 = STensorOperation::determinantSTensor3(q0->getConstRefToFp());
    Jp = Jp0*exp(DeltaHatQ);
  }
  // plastic energy balance
  res(2) = ((1-yieldfV)*Jp*R*DeltaHatP - kcorEq*DeltaHatD - pcor*DeltaHatQ)/R0;
 

  double DkcorEqDDeltaHatD = -3.*muT/detF;
  double DpcorDDeltaHatQ = -KT/detF;

  double Dres0DfV = gradf(3)*DyieldfVDfV+ gradf(4)*DChiDDeltafV + gradf(5)*DWDDeltafV;
  // Dres0DDeltaHatD
  J(0,0) = gradf(0)*DkcorEqDDeltaHatD+ gradf(4)*DChiDDeltaHatD + gradf(5)*DWDDeltaHatD + Dres0DfV*DDeltafVDDeltaHatD;
  // Dres0DDeltaHatQ
  J(0,1) = gradf(1)*DpcorDDeltaHatQ + gradf(4)*DChiDDeltaHatQ + gradf(5)*DWDDeltaHatQ+  Dres0DfV*DDeltafVDDeltaHatQ;
  // Dres0DDeltaHatP
  J(0,2) = gradf(2)*H+ + gradf(4)*DChiDDeltaHatP + gradf(5)*DWDDeltaHatP +  Dres0DfV*DDeltafVDDeltaHatP;

  double Dres1DfV = R0*(DeltaHatD*gradNv(3)- DeltaHatQ*gradNs(3))*DyieldfVDfV +
                    R0*(DeltaHatD*gradNv(4)- DeltaHatQ*gradNs(4))*DChiDDeltafV +
                    R0*(DeltaHatD*gradNv(5)- DeltaHatQ*gradNs(5))*DWDDeltafV;


  // Dres1DDeltaHatD
  J(1,0) = R0*(Nv + DeltaHatD*gradNv(0)*DkcorEqDDeltaHatD - DeltaHatQ*gradNs(0)*DkcorEqDDeltaHatD) +
           R0*(DeltaHatD*gradNv(4)- DeltaHatQ*gradNs(4))*DChiDDeltaHatD +
           R0*(DeltaHatD*gradNv(5)- DeltaHatQ*gradNs(5))*DWDDeltaHatD +
           Dres1DfV*DDeltafVDDeltaHatD;
  // Dres1DDeltaHatQ
  J(1,1) = R0*(DeltaHatD*gradNv(1)*DpcorDDeltaHatQ- Ns - DeltaHatQ*gradNs(1)*DpcorDDeltaHatQ) +
           R0*(DeltaHatD*gradNv(4)- DeltaHatQ*gradNs(4))*DChiDDeltaHatQ +
           R0*(DeltaHatD*gradNv(5)- DeltaHatQ*gradNs(5))*DWDDeltaHatQ +
           Dres1DfV*DDeltafVDDeltaHatQ;
  // Dres1DDeltaHatP
  J(1,2) = R0*(DeltaHatD*gradNv(2) - DeltaHatQ*gradNs(2))*H  +
           R0*(DeltaHatD*gradNv(4)- DeltaHatQ*gradNs(4))*DChiDDeltaHatP+
           R0*(DeltaHatD*gradNv(5)- DeltaHatQ*gradNs(5))*DWDDeltaHatP+
           Dres1DfV*DDeltafVDDeltaHatP;
           
  double Dres2DfV = (-Jp*R*DeltaHatP/R0)*DyieldfVDfV;

  // Dres2DDeltaHatd
  J(2,0) = (-kcorEq - DkcorEqDDeltaHatD*DeltaHatD)/R0 + Dres2DfV*DDeltafVDDeltaHatD;
  // Dres2DDeltaHatq
  J(2,1) = (-pcor - DpcorDDeltaHatQ*DeltaHatQ)/R0+ Dres2DfV*DDeltafVDDeltaHatQ;
  if (_stressFormulation == CORO_KIRCHHOFF)
  {
    double DJpDDeltaHatQ = Jp;
    J(2,1) += ((1.-yieldfV)*DJpDDeltaHatQ*R*DeltaHatP)/R0;
  }
  // Dres2DDeltaHatp
  J(2,2) = (1-yieldfV)*Jp*(R + H*DeltaHatP)/R0+ Dres2DfV*DDeltafVDDeltaHatP;
};

void mlawNonLocalPorosity::computeDResidualLocal(STensor3 &dres0dEpr, STensor3 &dres1dEpr, STensor3 &dres2dEpr, const STensor3& F,
                                  const STensor3& devKcorpr, const double kcorEqpr, const double pcorpr, const double R,
                                  const double yieldfV, const double DyieldfVDfV,
                                  const STensor3& DDeltafVDEpr, const STensor43& DdevKcorprDEpr, const STensor3& DpcorprDEpr,
                                  const IPNonLocalPorosity *q0, const IPNonLocalPorosity* q1,
                                  const double DeltaHatD, const double DeltaHatQ, const double DeltaHatP,
                                  const double* T, const double* DRDT,
                                  const STensor3* DdevKcorprDT , const double* DpcorprDT, const double* DDeltafVDT,
                                  double *dres0dT, double *dres1dT, double *dres2dT
                                  ) const{
  const IPVoidState* voidState = &q1->getConstRefToIPVoidState();
  
  double Chi  = voidState->getVoidLigamentRatio();
  double DChiDDeltafV  = voidState->getDVoidLigamentRatioDYieldPorosity()*DyieldfVDfV;

  double W  = voidState->getVoidAspectRatio();
  double DWDDeltafV  = voidState->getDVoidAspectRatioDYieldPorosity()*DyieldfVDfV;

  double f0 = q1->getInitialPorosity();
  double R0 = _j2IH->getYield0(); // reference value

  double KT = _K;
  double muT = _mu;
  if (isThermomechanicallyCoupled()){
    KT=bulkModulus(*T);
    muT=shearModulus(*T);
  }

  static STensor3 DkcorEqprDEpr;
  STensorOperation::multSTensor3STensor43(devKcorpr,DdevKcorprDEpr,DkcorEqprDEpr);
  DkcorEqprDEpr *= (1.5/kcorEqpr);

  // corrected values
  double kcorEq =  kcorEqpr - 3.*muT*DeltaHatD;
  double pcor = pcorpr - KT*DeltaHatQ;

  static STensor3 DkcorEqDEpr, DpcorDEpr;
  DkcorEqDEpr = DkcorEqprDEpr;
  DpcorDEpr = DpcorprDEpr;
  double detF = 1.;
  if (_stressFormulation == CORO_CAUCHY)
  {
    detF = STensorOperation::determinantSTensor3(F);
    kcorEq /= detF;
    pcor /= detF;  
    
    DkcorEqDEpr *= (1./detF);
    DpcorDEpr *= (1./detF);
    
    static STensor3 DdetFDEepr;
    STensorOperation::diag(DdetFDEepr,detF);
    DkcorEqDEpr.daxpy(DdetFDEepr,-kcorEq/detF);
    DpcorDEpr.daxpy(DdetFDEepr,-pcor/detF);
  }
  
  // grad f, Ns, Nf with respect to [kcorEq pcor R fV Chi W]
  static fullVector<double> gradf(6), gradNs(6), gradNv(6);
  double DFDT, DNsDT, DNvDT;
  double f, Ns, Nv;
  f = yieldFunction(kcorEq,pcor,R,yieldfV,q0,q1,T,true,&gradf,isThermomechanicallyCoupled(),&DFDT);
  plasticFlow(Ns,Nv,kcorEq,pcor,R,q0,q1,T,true,&gradNs,&gradNv,isThermomechanicallyCoupled(),&DNsDT,&DNvDT);
  
  // compute Dres0DEpr
  double Dres0DkcorEq = gradf(0);
  double Dres0Dpcor = gradf(1);
  double Dres0DDeltafV = gradf(3)*DyieldfVDfV + gradf(4)*DChiDDeltafV+gradf(5)*DWDDeltafV;
  dres0dEpr = DkcorEqDEpr;
  dres0dEpr*= Dres0DkcorEq;
  dres0dEpr.daxpy(DpcorDEpr,Dres0Dpcor);
  dres0dEpr.daxpy(DDeltafVDEpr,Dres0DDeltafV);

  // compute Dres1DEpr
  double Dres1DkcorEq = R0*(DeltaHatD*gradNv(0)- DeltaHatQ*gradNs(0));
  double Dres1Dpcor = R0*(DeltaHatD*gradNv(1)- DeltaHatQ*gradNs(1));
  double Dres1DDeltafV =R0*(DeltaHatD*gradNv(3)- DeltaHatQ*gradNs(3))*DyieldfVDfV+
                        R0*(DeltaHatD*gradNv(4)- DeltaHatQ*gradNs(4))*DChiDDeltafV+
                        R0*(DeltaHatD*gradNv(5)- DeltaHatQ*gradNs(5))*DWDDeltafV;

  dres1dEpr = DkcorEqDEpr;
  dres1dEpr *= Dres1DkcorEq;
  dres1dEpr.daxpy(DpcorDEpr,Dres1Dpcor);
  dres1dEpr.daxpy(DDeltafVDEpr,Dres1DDeltafV);
  
  double Jp = 1.;
  if (_stressFormulation == CORO_KIRCHHOFF)
  {
    double Jp0 = STensorOperation::determinantSTensor3(q0->getConstRefToFp());
    Jp = Jp0*exp(DeltaHatQ);
  }

  // compute Dres2DEpr
  double Dres2DkcorEq = (-DeltaHatD)/R0;
  double Dres2Dpcor = (-DeltaHatQ)/R0;
  double Dres2DDeltafV = (-Jp*R*DeltaHatP/R0)*DyieldfVDfV;;

  dres2dEpr = DkcorEqDEpr;
  dres2dEpr *= Dres2DkcorEq;
  dres2dEpr.daxpy(DpcorDEpr,Dres2Dpcor);
  dres1dEpr.daxpy(DDeltafVDEpr,Dres2DDeltafV);


  if (isThermomechanicallyCoupled()){
    double dKdT=bulkModulusDerivative(*T);
    double dmudT=shearModulusDerivative(*T);

    static double DkcorEqDT, DpcorDT;
    DkcorEqDT = STensorOperation::doubledot(devKcorpr,*DdevKcorprDT)*1.5/kcorEqpr - 3.*dmudT*DeltaHatD;
    DpcorDT = *DpcorprDT -dKdT* DeltaHatQ;
    
    if (_stressFormulation == CORO_CAUCHY)
    {
      DkcorEqDT /= detF;
      DpcorDT /= detF;
    }

    double partDres0DT = gradf(2)*(*DRDT) + DFDT;  // yield temperature-dependent
    *dres0dT = Dres0DkcorEq*DkcorEqDT + Dres0Dpcor*DpcorDT + Dres0DDeltafV*(*DDeltafVDT) + partDres0DT;

    double partDres1DT = R0*(DeltaHatD*gradNv(2)- DeltaHatQ*gradNs(2))*(*DRDT) + Dres1DDeltafV*(*DDeltafVDT)+ R0*(DeltaHatD*DNvDT- DeltaHatQ*DNsDT);
    *dres1dT = Dres1DkcorEq*DkcorEqDT + Dres1Dpcor*DpcorDT + partDres1DT;

    double partDres2DT = ((1-f0)*(*DRDT)*DeltaHatP)/R0;
    *dres2dT = Dres2DkcorEq*DkcorEqDT + Dres2Dpcor*DpcorDT + Dres2DDeltafV*(*DDeltafVDT)+ partDres2DT;
  }
};


void mlawNonLocalPorosity::voidCharacteristicsEvolutionLocal(const STensor3& kcor,
                                                const IPNonLocalPorosity *q0, IPNonLocalPorosity* q1,
                                                const double DeltaHatD, const double DeltaHatQ, const double DeltaHatP,
                                                const double DtildefVstarDDeltafV,
                                                double& DyieldfVDDeltafV,
                                                double& DDeltafVDDeltaHatD, double& DDeltafVDDeltaHatQ, double& DDeltafVDDeltaHatP,
                                                STensor3& DDeltafVDKcor) const
{
  if (getVoidEvolutionLaw()->useYieldPorosityDuringCoalescence())
  {
    double hatPn = q0->getLocalMatrixPlasticStrain();
    // nucleatino
    nucleation(hatPn+DeltaHatP,hatPn,q0->getLocalPorosity(),q1,q0);

    // local void growth
    localPorosityGrowth(q1, DeltaHatD,DeltaHatQ,DeltaHatP,
                      kcor,q0,true,&DDeltafVDDeltaHatD,&DDeltafVDDeltaHatQ,&DDeltafVDDeltaHatP,&DDeltafVDKcor); 
    
    // yield porosity
    double & tildefVstar = q1->getRefToCorrectedPorosity();
    double& yieldfV = q1->getRefToYieldPorosity();
    
    tildefVstar = q0->getCorrectedPorosity() + DtildefVstarDDeltafV*(q1->getLocalPorosity() - q0->getLocalPorosity());
    yieldfV = _regularisationFunc_porosityEff->getVal(tildefVstar);
    DyieldfVDDeltafV  = _regularisationFunc_porosityEff->getDiff(tildefVstar)*DtildefVstarDDeltafV;

    // update void
    voidStateGrowth(q0->getYieldPorosity(),q1->getYieldPorosity(),DeltaHatD,DeltaHatQ,DeltaHatP,q0,q1);
  }
  else
  {
    // void characteristics are evaluated first, then porosity are evaluated
    // other void characteristic evolutions are called first
    // then void evolution is estimated
    // update void state
    STensorOperation::zero(DDeltafVDKcor);
    // yield porosity does not effect other void characteristics
    voidStateGrowth(q0->getYieldPorosity(),q1->getYieldPorosity(),DeltaHatD,DeltaHatQ,DeltaHatP,q0,q1);    
    
    // local porosity
    const IPVoidState* ipvoid = &(q1->getConstRefToIPVoidState());

    double lambda = ipvoid->getVoidSpacingRatio();
    double W = ipvoid->getVoidAspectRatio();
    double Chi = ipvoid->getVoidLigamentRatio();
    double gamma = ipvoid->getVoidShapeFactor();
    
    double& fV = q1->getRefToLocalPorosity();

    fV = Chi*Chi*Chi*W/(3.*gamma*lambda); // always corrects
    // Regularise porosity growth and rates
    double regularizedRate = 1.;
    if (fV > _localfVFailure)
    {
      double fVn = q0->getLocalPorosity();
      double DeltafV = fV - fVn;
      fV = _localfVFailure;
      regularizedRate = 0.;
      if (fabs(DeltafV)> 0)
      {
        regularizedRate = (_localfVFailure - fVn)/DeltafV;
      }
    }
    
    
    // nonlocal var = [volumetric, matrix, deviatoric]
    double DfVDChi = 3.*fV*regularizedRate/Chi;
    double DfVDlambda = -fV*regularizedRate/lambda;
    double DfVDW = fV*regularizedRate/W;
    double DfVDgamma = -fV*regularizedRate/gamma;

    DDeltafVDDeltaHatQ =  DfVDChi*ipvoid->getDVoidLigamentRatioDVolumetricPlasticDeformation() +
                                   DfVDW*ipvoid->getDVoidAspectRatioDVolumetricPlasticDeformation()+
                                   DfVDgamma*ipvoid->getDVoidShapeFactorDVolumetricPlasticDeformation()+
                                   DfVDlambda*ipvoid->getDVoidSpacingRatioDVolumetricPlasticDeformation();
    DDeltafVDDeltaHatP =  DfVDChi*ipvoid->getDVoidLigamentRatioDMatrixPlasticDeformation() +
                                   DfVDW*ipvoid->getDVoidAspectRatioDMatrixPlasticDeformation()+
                                   DfVDgamma*ipvoid->getDVoidShapeFactorDMatrixPlasticDeformation()+
                                   DfVDlambda*ipvoid->getDVoidSpacingRatioDMatrixPlasticDeformation();
    DDeltafVDDeltaHatD =  DfVDChi*ipvoid->getDVoidLigamentRatioDDeviatoricPlasticDeformation() +
                                   DfVDW*ipvoid->getDVoidAspectRatioDDeviatoricPlasticDeformation()+
                                   DfVDgamma*ipvoid->getDVoidShapeFactorDDeviatoricPlasticDeformation()+
                                   DfVDlambda*ipvoid->getDVoidSpacingRatioDDeviatoricPlasticDeformation();
                                   
    // yield porosity
    double & tildefVstar = q1->getRefToCorrectedPorosity();
    double& yieldfV = q1->getRefToYieldPorosity();
    
    tildefVstar = q0->getCorrectedPorosity() + DtildefVstarDDeltafV*(q1->getLocalPorosity() - q0->getLocalPorosity());
    yieldfV = _regularisationFunc_porosityEff->getVal(tildefVstar);
    DyieldfVDDeltafV  = _regularisationFunc_porosityEff->getDiff(tildefVstar)*DtildefVstarDDeltafV;
  }
};




bool mlawNonLocalPorosity::plasticCorrectorLocal(const STensor3& F1, const double& tildefVstarPrev,const double& DtildefVstarDDeltafV,
                                  const STensor3& Kcorpr, const STensor3& devKcorpr,  const double& pcorpr,
                                  const IPNonLocalPorosity *q0, IPNonLocalPorosity *q1,
                                  STensor3& Fe, STensor3& Ce, STensor3& Ee, STensor43& Le, STensor63& dLe,
                                  const bool stiff,
                                  const STensor43& DKcorprDEpr, const STensor43& DdevKcorprDEpr, const STensor3& DpcorprDEpr, // predictor elastic tangent
                                  STensor43& DKcorDEpr, STensor43& DFpDEpr,
                                  const double* T,
                                  const STensor3* DKcorprDT, const STensor3* DdevKcorprDT, const double* DpcorprDT,
                                  STensor3* DKcorDT, STensor3* DFpDT
                                ) const{
  // previous converged state
  double hatPn = q0->getLocalMatrixPlasticStrain();
  double hatQn = q0->getLocalVolumetricPlasticStrain();
  double hatDn = q0->getLocalDeviatoricPlasticStrain();
  double fVn = q0->getLocalPorosity();

  double DeltaHatD = 1.e-8;
  double DeltaHatQ = 0.;
  double DeltaHatP = 1.e-8;

  double DDeltafVDDeltaHatD = 0.;
  double DDeltafVDDeltaHatQ = 0.;
  double DDeltafVDDeltaHatP = 0.;  
  static STensor3 DDeltafVDKcorpr;
  double DyieldfVDDeltafV = 1.;
  
  voidCharacteristicsEvolutionLocal(Kcorpr,q0,q1,DeltaHatD,DeltaHatQ,DeltaHatP,
                        DtildefVstarDDeltafV,
                        DyieldfVDDeltafV, DDeltafVDDeltaHatD, DDeltafVDDeltaHatQ, DDeltafVDDeltaHatP,DDeltafVDKcorpr);

  double kcorEqpr = sqrt(1.5*devKcorpr.dotprod());
 
  // First residual
  static SVector3 res;
  static SVector3 sol;
  static STensor3 J, invJ;
  
 double & yield = q1->getRefToCurrentViscoplasticYieldStress();
  yield = q1->getConstRefToIPJ2IsotropicHardening().getR();
  double H = q1->getConstRefToIPJ2IsotropicHardening().getDR();
  
  double yieldfV = q1->getYieldPorosity();
  computeResidualLocal(res,J,F1,kcorEqpr,pcorpr,yield,H,yieldfV,DyieldfVDDeltafV,q0,q1,
                  DeltaHatD,DeltaHatQ,DeltaHatP,
                  DDeltafVDDeltaHatD,DDeltafVDDeltaHatQ,DDeltafVDDeltaHatP,T);

  double muT = _mu;
  if (isThermomechanicallyCoupled()){
    muT=shearModulus(*T);
  }

  int ite = 0;
  double f = res.norm();
  // Start iterations
  //Msg::Info("plastic corrector");
  while(f > _tol or ite <1)
  {
    // Jacobian
  	STensorOperation::inverseSTensor3(J,invJ);
  	STensorOperation::multSTensor3SVector3(invJ,res,sol);

    if (DeltaHatD -sol(0) <= 0.){
      DeltaHatD *= 0.5;
    }
    else if (DeltaHatD-sol(0) > kcorEqpr/3./muT){
      DeltaHatD = kcorEqpr/3./muT;
    }
    else{
      DeltaHatD -= sol(0);
    }

    if (-sol(1) > 1.){
      DeltaHatQ += 1.;
    }
    else if (-sol(1) < -1.){
      DeltaHatQ += -1.;
    }
    else
      DeltaHatQ -= sol(1);


    if (DeltaHatP -sol(2) < 0.){
      DeltaHatP *= 0.5;
    }
    else
      DeltaHatP -= sol(2);

    voidCharacteristicsEvolutionLocal(Kcorpr,q0,q1,DeltaHatD,DeltaHatQ,DeltaHatP,
                        DtildefVstarDDeltafV,
                        DyieldfVDDeltafV, DDeltafVDDeltaHatD, DDeltafVDDeltaHatQ, DDeltafVDDeltaHatP,DDeltafVDKcorpr);

    updateInternalVariables(q1,q0,DeltaHatD,DeltaHatQ,DeltaHatP,false,T);
    
    yield = q1->getConstRefToIPJ2IsotropicHardening().getR();
    H = q1->getConstRefToIPJ2IsotropicHardening().getDR();
    // Add strain rate effects
    if (_viscoplastlaw != NULL)
    {
      double R, dR;
      double delta_t = getTimeStep();
      _viscoplastlaw->get(DeltaHatP/delta_t,R,dR);
      yield += R;
      H += dR/delta_t;
    }
    
    yieldfV = q1->getYieldPorosity();
    computeResidualLocal(res,J,F1,kcorEqpr,pcorpr,yield,H,yieldfV,DyieldfVDDeltafV,q0,q1,
                  DeltaHatD,DeltaHatQ,DeltaHatP,
                  DDeltafVDDeltaHatD,DDeltafVDDeltaHatQ,DDeltafVDDeltaHatP,T);

    f = res.norm();
    //Msg::Info("ite = %d maxite = %d norm = %e ,DeltaHatD = %e,DeltaHatQ = %e,DeltaHatP = %e,DeltafV =%e !!",ite,_maxite,f,DeltaHatD,DeltaHatQ,DeltaHatP,DeltafV);
    if (f < _tol)  break;

    ite++;
    if((ite > _maxite) or STensorOperation::isnan(f))
    {
      #ifdef _DEBUG
      if (q1->getConstRefToIPCoalescence().getCoalescenceOnsetFlag()){
        Msg::Warning("No convergence in LOCAL plast. correct. ite= %d: active coales, norm= %e, DeltaHatD= %e, DeltaHatQ= %e, DeltaHatP= %e,  fv_y= %e",
                    ite,f,DeltaHatD,DeltaHatQ,DeltaHatP, q1->getYieldPorosity());
      }
      else{
        Msg::Warning("No convergence in LOCAL plast. correct. ite= %d: inactive coa., norm= %e, DeltaHatD= %e, DeltaHatQ= %e, DeltaHatP= %e, fv_y= %e",
                    ite,f,DeltaHatD,DeltaHatQ,DeltaHatP, q1->getYieldPorosity());
        double J3 = STensorOperation::determinantSTensor3(devKcorpr);
        Msg::Warning("res=[%e; %e; %e;] fVloc= %e, tauEqpr= %e , ppr = %e, J3= %e, DyieldfVDDeltafV= %e, DDeltafVDDeltaHatV= [%e %e %e], yieldfG = %e",
                    res(0),res(1),res(2),q0->getLocalPorosity(), kcorEqpr/_j2IH->getYield0(), pcorpr/_j2IH->getYield0(), 27.*J3/(2.*kcorEqpr*kcorEqpr*kcorEqpr),
                    DyieldfVDDeltafV, DDeltafVDDeltaHatD,DDeltafVDDeltaHatQ,DDeltafVDDeltaHatP, q0->getConstRefToIPCoalescence().getYieldOffset());
      }
      #endif //_DEBUG
      return false;
    }
  }


  static STensor3 DGNp, devDGNp;
  double trDGNp;
  static STensor43 ENp;
  updatePlasticState(q1,q0,F1,DeltaHatD,DeltaHatQ,DeltaHatP,devKcorpr,kcorEqpr,pcorpr,
                    yield,Fe,Ce,Ee,Le,dLe,DGNp,devDGNp,trDGNp,ENp,T);
  double f0=q0->getInitialPorosity();
  q1->getRefToDPlasticEnergyDMatrixPlasticStrain() = (1.-f0)*H*DeltaHatP + (1.-f0)*yield;
  /* Stiffness computation*/
  if(stiff)
  {
    static STensor3 DDeltafVDEpr; // partial derivatives
    STensorOperation::multSTensor3STensor43(DDeltafVDKcorpr,DKcorprDEpr,DDeltafVDEpr);
    
    double dRdT= 0.;
    double DDeltafVDT =0.; // partial derivatives
    
    if (isThermomechanicallyCoupled()){
      dRdT = q1->getConstRefToIPJ2IsotropicHardening().getDRDT();
      DDeltafVDT = STensorOperation::doubledot(DDeltafVDKcorpr,*DKcorprDT);
    }

    /* Compute internal variables derivatives from residual derivatives */
    static STensor3 dres0dEpr, dres1dEpr, dres2dEpr;
    double dres0dT, dres1dT,dres2dT;

    computeDResidualLocal(dres0dEpr,dres1dEpr,dres2dEpr,F1,
                  devKcorpr,kcorEqpr,pcorpr,yield,
                  yieldfV,DyieldfVDDeltafV,
                  DDeltafVDEpr,DdevKcorprDEpr,DpcorprDEpr,
                  q0,q1,
                  DeltaHatD,DeltaHatQ,DeltaHatP,
                  T,&dRdT,DdevKcorprDT,DpcorprDT,&DDeltafVDT,
                  &dres0dT,&dres1dT,&dres2dT);

    static STensor3 dDeltaHatDdEpr, dDeltaHatQdEpr, dDeltaHatPdEpr;

    STensorOperation::inverseSTensor3(J,invJ); // inverse last tengent operator

    for(int i=0; i<3; i++)
    {
      for(int j=0; j<3; j++)
      {
        res(0) = -dres0dEpr(i,j);
        res(1) = -dres1dEpr(i,j);
        res(2) = -dres2dEpr(i,j);
        STensorOperation::multSTensor3SVector3(invJ,res,sol);
        dDeltaHatDdEpr(i,j) = sol(0);
        dDeltaHatQdEpr(i,j) = sol(1);
        dDeltaHatPdEpr(i,j) = sol(2);
      }
    }

    STensor3 &DplEnergyDEpr=q1->getRefToDPlasticEnergyDF();
    STensorOperation::zero(DplEnergyDEpr);
    for(int i=0;i<3;i++){
      for(int j=0;j<3;j++){
        DplEnergyDEpr(i,j)=(1.-f0)*H*dDeltaHatPdEpr(i,j)*DeltaHatP+(1.-f0)*yield*dDeltaHatPdEpr(i,j);
      }
    }

    double dDeltaHatDdT = 0.;
    double dDeltaHatQdT = 0.;
    double dDeltaHatPdT = 0.;
    if(isThermomechanicallyCoupled()){
      res(0) = -dres0dT;
      res(1) = -dres1dT;
      res(2) = -dres2dT;
      STensorOperation::multSTensor3SVector3(invJ,res,sol);
      dDeltaHatDdT = sol(0);
      dDeltaHatQdT = sol(1);
      dDeltaHatPdT = sol(2);

      double &DplEnergyDT=q1->getRefToDPlasticEnergyDT();
      DplEnergyDT=(1.-f0)*(H*dDeltaHatPdT+dRdT)*DeltaHatP+(1.-f0)*yield*dDeltaHatPdT;
    }


    computePlasticTangentLocal(q1,q0,devKcorpr,kcorEqpr,pcorpr,DGNp,devDGNp,trDGNp,ENp,
        DeltaHatD,DeltaHatQ,DeltaHatP,
        DKcorprDEpr,DdevKcorprDEpr,DpcorprDEpr,
        dDeltaHatDdEpr,dDeltaHatQdEpr,dDeltaHatPdEpr,
        DKcorDEpr,DFpDEpr,
        T,DKcorprDT,DdevKcorprDT,DpcorprDT,
        &dDeltaHatDdT,&dDeltaHatQdT,&dDeltaHatPdT,
        DKcorDT,DFpDT);
  }

  return true;
};



void mlawNonLocalPorosity::computePlasticTangentLocal(const IPNonLocalPorosity *q1, const IPNonLocalPorosity* q0,
              const STensor3& devKcorpr, const double kcorEqpr, const double pcorpr,
              const STensor3& DGNp, const STensor3& devDGNp, const double trDGNp, const STensor43& Dexp,
              const double DeltaHatD, const double DeltaHatQ, const double DeltaHatP,
              const STensor43& DKcorprDEpr, const STensor43& DdevKcorprDEpr, const STensor3& DpcorprDEpr,
              const STensor3& DDeltaHatDDEpr, const STensor3& DDeltaHatQDEpr, const STensor3& DDeltaHatPDEpr,
              STensor43& DKcorDEpr, STensor43& DFpDEpr,
              const double* T, const STensor3* DKcorprDT, const STensor3* DdevKcorprDT, const double* DpcorprDT,
              const double* DDeltaHatDDT, const double* DDeltaHatQDT, const double* DDeltaHatPDT,
              STensor3* DKcorDT, STensor3* DFpDT
            ) const{

  double KT=_K;
  double muT= _mu;
  if (isThermomechanicallyCoupled()){
    KT = bulkModulus(*T);
    muT = shearModulus(*T);
  }

  //compute DkcorEqprDEpr
  static STensor3 DkcorEqprDEpr;
  STensorOperation::multSTensor3STensor43(devKcorpr,DdevKcorprDEpr,DkcorEqprDEpr);
  DkcorEqprDEpr *= (1.5/kcorEqpr);

   // from devDGNp = DeltaHatD*1.5*devKcorpr/kcorEqpr
   // compute DdevDGNpDEpr
   static STensor43 DdevDGNpDEpr;
   STensorOperation::prod(devKcorpr,DDeltaHatDDEpr,1.5/kcorEqpr,DdevDGNpDEpr);
   STensorOperation::axpy(DdevDGNpDEpr,DdevKcorprDEpr,1.5*DeltaHatD/kcorEqpr);
   STensorOperation::prodAdd(devKcorpr,DkcorEqprDEpr,-DeltaHatD*1.5/(kcorEqpr*kcorEqpr),DdevDGNpDEpr);

   // trDGN = DeltaHatQ
   // compute
   static STensor3 DtrDGNpDEpr;
   DtrDGNpDEpr = DDeltaHatQDEpr;

   static STensor43 DDGNpDEpr;
   DDGNpDEpr = DdevDGNpDEpr;
   STensorOperation::prodAdd(_I,DtrDGNpDEpr,1./3.,DDGNpDEpr);


   DKcorDEpr =   DKcorprDEpr;
   STensorOperation::axpy(DKcorDEpr,DdevDGNpDEpr,-2.*muT);
   STensorOperation::prodAdd(_I,DtrDGNpDEpr,-KT,DKcorDEpr);

   // plastic tangent
   const STensor3& Fp0 = q0->getConstRefToFp();
   static STensor43 DexpFp0;
   for (int i=0; i<3; i++){
     for (int j=0; j<3; j++){
       for (int k=0; k<3; k++){
         for (int l=0; l<3; l++){
           DexpFp0(i,j,k,l) = 0.;
           for (int p=0; p<3; p++){
             DexpFp0(i,j,k,l) += Dexp(i,p,k,l)* Fp0(p,j);
           }
         }
       }
     }
   }
   STensorOperation::multSTensor43(DexpFp0,DDGNpDEpr,DFpDEpr);


  if (isThermomechanicallyCoupled()){
    double DkcorEqprDT = STensorOperation::doubledot(devKcorpr,*DdevKcorprDT)*(1.5/kcorEqpr);
    // from devDGNp = DeltaHatD*1.5*devKcorpr/kcorEqpr
    // compute DdevDGNpDT
    static STensor3 DdevDGNpDT;
    DdevDGNpDT = devKcorpr;
    DdevDGNpDT *= ((*DDeltaHatDDT)*1.5/kcorEqpr);
    STensorOperation::axpy(DdevDGNpDT,*DdevKcorprDT,DeltaHatD*1.5/kcorEqpr);
    STensorOperation::axpy(DdevDGNpDT,devKcorpr,-DeltaHatD*1.5*DkcorEqprDT/(kcorEqpr*kcorEqpr));

    // trDGN = DeltaHatQ
    // compute DtrDGNDT
    double DtrDGNpDT = *DDeltaHatQDT;

    static STensor3 DDGNpDT;
    DDGNpDT = DdevDGNpDT;
    STensorOperation::axpy(DDGNpDT,_I,DtrDGNpDT/3.);

    (*DKcorDT) = (*DKcorprDT);
    STensorOperation::axpy(*DKcorDT,DdevDGNpDT,-2.*muT);
    STensorOperation::axpy(*DKcorDT,_I,-DtrDGNpDT*KT);

    double dKdT=bulkModulusDerivative(*T);
    double dmudT=shearModulusDerivative(*T);
    STensorOperation::axpy(*DKcorDT,devDGNp,-2.*dmudT);
    STensorOperation::axpy(*DKcorDT,_I,-dKdT*trDGNp);

    STensorOperation::multSTensor43STensor3(DexpFp0,DDGNpDT,*DFpDT);
  }

};



void mlawNonLocalPorosity::computePlasticTangentNonLocal(const IPNonLocalPorosity *q1, const IPNonLocalPorosity* q0,
              const STensor3& devKcorpr, const double kcorEqpr, const double pcorpr,
              const STensor3& DGNp, const STensor3& devDGNp, const double trDGNp, const STensor43& Dexp,
              const double DeltaHatD, const double DeltaHatQ, const double DeltaHatP,
              const STensor43& DKcorprDEpr, const STensor43& DdevKcorprDEpr, const STensor3& DpcorprDEpr,
              const STensor3& DDeltaHatDDEpr, const STensor3& DDeltaHatQDEpr, const STensor3& DDeltaHatPDEpr,
              STensor43& DKcorDEpr, STensor43& DFpDEpr,
              const double DDeltaHatDDtildefV, const double DDeltaHatQDtildefV, const double DDeltaHatPDtildefV,
              STensor3& DKcorDtildefV, STensor3& DFpDtildefV,
              const double* T, const STensor3* DKcorprDT, const STensor3* DdevKcorprDT, const double* DpcorprDT,
              const double* DDeltaHatDDT, const double* DDeltaHatQDT, const double* DDeltaHatPDT,
              STensor3* DKcorDT, STensor3* DFpDT
            ) const{

  // compute local tangent
  computePlasticTangentLocal(q1,q0,
              devKcorpr,kcorEqpr,pcorpr,
              DGNp,devDGNp,trDGNp,Dexp,
              DeltaHatD,DeltaHatQ,DeltaHatP,
              DKcorprDEpr, DdevKcorprDEpr, DpcorprDEpr,
              DDeltaHatDDEpr, DDeltaHatQDEpr, DDeltaHatPDEpr,
              DKcorDEpr, DFpDEpr,
              T, DKcorprDT, DdevKcorprDT, DpcorprDT,
              DDeltaHatDDT, DDeltaHatQDT, DDeltaHatPDT,
              DKcorDT, DFpDT);

  double KT=_K;
  double muT= _mu;
  if (isThermomechanicallyCoupled()){
    KT = bulkModulus(*T);
    muT = shearModulus(*T);
  }

  // from devDGNp = DeltaHatD*1.5*devKcorpr/kcorEqpr
  static STensor3 DdevDGNpDtildefV;
  DdevDGNpDtildefV = devKcorpr;
  DdevDGNpDtildefV *= (DDeltaHatDDtildefV*1.5/kcorEqpr);

  double DtrDGNpDtildefV = DDeltaHatQDtildefV;

  static STensor3 DDGNpDtildefV;
  DDGNpDtildefV = DdevDGNpDtildefV;
  STensorOperation::axpy(DDGNpDtildefV,_I,DtrDGNpDtildefV/3.);

  STensorOperation::zero(DKcorDtildefV);
  STensorOperation::axpy(DKcorDtildefV,DdevDGNpDtildefV,-2.*muT);
  STensorOperation::axpy(DKcorDtildefV,_I,-DtrDGNpDtildefV*KT);

  const STensor3& Fp0 = q0->getConstRefToFp();
  static STensor43 DexpFp0;
  for (int i=0; i<3; i++){
   for (int j=0; j<3; j++){
     for (int k=0; k<3; k++){
       for (int l=0; l<3; l++){
         DexpFp0(i,j,k,l) = 0.;
         for (int p=0; p<3; p++){
           DexpFp0(i,j,k,l) += Dexp(i,p,k,l)* Fp0(p,j);
         }
       }
     }
   }
  }
  STensorOperation::multSTensor43STensor3(DexpFp0,DDGNpDtildefV,DFpDtildefV);
};




void mlawNonLocalPorosity::computePlasticTangentNonLocal_multipleNonLocalVariables(const IPNonLocalPorosity *q1, const IPNonLocalPorosity* q0,
              const STensor3& devKcorpr, const double kcorEqpr, const double pcorpr,
              const STensor3& DGNp, const STensor3& devDGNp, const double trDGNp, const STensor43& Dexp,
              const double DeltaHatD, const double DeltaHatQ, const double DeltaHatP,
              const STensor43& DKcorprDEpr, const STensor43& DdevKcorprDEpr, const STensor3& DpcorprDEpr,
              const STensor3& DDeltaHatDDEpr, const STensor3& DDeltaHatQDEpr, const STensor3& DDeltaHatPDEpr,
              STensor43& DKcorDEpr, STensor43& DFpDEpr,
              const std::vector<double>& DDeltaHatDDNonLocalVar, const std::vector<double>& DDeltaHatQDNonLocalVar, const std::vector<double>& DDeltaHatPDNonLocalVar,
              std::vector<STensor3>& DKcorDNonLocalVar, std::vector<STensor3>& DFpDNonLocalVar,
              const double* T, const STensor3* DKcorprDT, const STensor3* DdevKcorprDT, const double* DpcorprDT,
              const double* DDeltaHatDDT, const double* DDeltaHatQDT, const double* DDeltaHatPDT,
              STensor3* DKcorDT, STensor3* DFpDT
            ) const{

  // compute local tangent
  computePlasticTangentLocal(q1,q0,
              devKcorpr,kcorEqpr,pcorpr,
              DGNp,devDGNp,trDGNp,Dexp,
              DeltaHatD,DeltaHatQ,DeltaHatP,
              DKcorprDEpr, DdevKcorprDEpr, DpcorprDEpr,
              DDeltaHatDDEpr, DDeltaHatQDEpr, DDeltaHatPDEpr,
              DKcorDEpr, DFpDEpr,
              T, DKcorprDT, DdevKcorprDT, DpcorprDT,
              DDeltaHatDDT, DDeltaHatQDT, DDeltaHatPDT,
              DKcorDT, DFpDT);

  double KT=_K;
  double muT= _mu;
  if (isThermomechanicallyCoupled()){
    KT = bulkModulus(*T);
    muT = shearModulus(*T);
  }

  const STensor3& Fp0 = q0->getConstRefToFp();
  static STensor43 DexpFp0;
  for (int i=0; i<3; i++){
   for (int j=0; j<3; j++){
     for (int k=0; k<3; k++){
       for (int l=0; l<3; l++){
         DexpFp0(i,j,k,l) = 0.;
         for (int p=0; p<3; p++){
           DexpFp0(i,j,k,l) += Dexp(i,p,k,l)* Fp0(p,j);
         }
       }
     }
   }
  }

  for (int nl=0; nl < _numNonLocalVar; nl++){
    // from devDGNp = DeltaHatD*1.5*devKcorpr/kcorEqpr
    static STensor3 DdevDGNpDNonLocalVar;
    DdevDGNpDNonLocalVar = devKcorpr;
    DdevDGNpDNonLocalVar *= (DDeltaHatDDNonLocalVar[nl]*1.5/kcorEqpr);

    double DtrDGNpDNonLocalVar = DDeltaHatQDNonLocalVar[nl];

    static STensor3 DDGNpDNonLocalVar;
    DDGNpDNonLocalVar = DdevDGNpDNonLocalVar;
    STensorOperation::axpy(DDGNpDNonLocalVar,_I,DtrDGNpDNonLocalVar/3.);

    STensorOperation::zero(DKcorDNonLocalVar[nl]);
    STensorOperation::axpy(DKcorDNonLocalVar[nl],DdevDGNpDNonLocalVar,-2.*muT);
    STensorOperation::axpy(DKcorDNonLocalVar[nl],_I,-DtrDGNpDNonLocalVar*KT);

    STensorOperation::multSTensor43STensor3(DexpFp0,DDGNpDNonLocalVar,DFpDNonLocalVar[nl]);
  }
};



void mlawNonLocalPorosity::tangentComputationLocal(STensor43& dStressDF,
                              const bool plastic,
                              const STensor3& F,  // current F
                              const STensor3& corKir,  // cor Kir
                              const STensor3& S, //  second PK
                              const STensor3& Fepr, const STensor3& Fppr, // predictor
                              const STensor43& Lpr,
                              const STensor3& Fe, const STensor3& Fp, // corrector
                              const STensor43& L, const STensor63& dL, // corrector value
                              const STensor43& DcorKirDEepr,
                              const STensor43& dFpDEepr,
                              const STensor43& EprToF, const STensor3& Fpinv
                              ) const{
  // P = Fe. (Kcor:Le) . invFp
  // need to compute dFedF, dKcordF, dinvFpdF, dLedF

  static STensor43 DcorKirDF, dFpdF;
  STensorOperation::multSTensor43(DcorKirDEepr,EprToF,DcorKirDF);
  if (plastic){
    STensorOperation::multSTensor43(dFpDEepr,EprToF,dFpdF);
  }
  else{
    STensorOperation::zero(dFpdF);
  }

  // done DcorKirDF, DFpDF

  static STensor43 DinvFpdF;
  STensorOperation::zero(DinvFpdF);
  if (plastic){
    for (int i=0; i<3; i++){
      for (int j=0; j<3; j++){
        for (int p=0; p<3; p++){
          for (int q=0; q<3; q++){
            for (int k=0; k<3; k++){
              for (int l=0; l<3; l++){
                DinvFpdF(i,j,k,l) -= Fpinv(i,p)*dFpdF(p,q,k,l)*Fpinv(q,j);
              }
            }
          }
        }
      }
    }
  }

  static STensor43 dFedF;
  STensorOperation::zero(dFedF);
  for (int i=0; i<3; i++){
    for (int j=0; j<3; j++){
      for (int k=0; k<3; k++){
        dFedF(i,j,i,k) += Fpinv(k,j);
        if (plastic){
          for (int l=0; l<3; l++){
            for (int p=0; p<3; p++){
              dFedF(i,j,k,l) += F(i,p)*DinvFpdF(p,j,k,l);
            }
          }
        }
      }
    }
  }
  static STensor63 DLDF;
  STensorOperation::zero(DLDF);
  for (int i=0; i<3; i++){
    for (int j=0; j<3; j++){
      for (int k=0; k<3; k++){
        for (int l=0; l<3; l++){
          for (int r=0; r<3; r++){
            for (int s=0; s<3; s++){
              for (int a=0; a<3; a++){
                for (int p=0; p<3; p++){
                  for (int q=0; q<3; q++){
                    DLDF(i,j,k,l,p,q) += dL(i,j,k,l,r,s)*2.*Fe(a,r)*dFedF(a,s,p,q);
                  }
                }
              }
            }
          }
        }
      }
    }
  }
  //

  static STensor43 DSDF; // S = corKir:L
  STensorOperation::zero(DSDF);
  for (int i=0; i<3; i++){
    for (int j=0; j<3; j++){
      for (int r=0; r<3; r++){
        for (int s=0; s<3; s++){
          for (int k=0; k<3; k++){
            for (int l=0; l<3; l++){
              DSDF(i,j,k,l) += DcorKirDF(r,s,k,l)*L(r,s,i,j) + corKir(r,s)*DLDF(r,s,i,j,k,l);
            }
          }
        }
      }
    }
  }


  // compute mechanical tengent
  STensorOperation::zero(dStressDF);
  for (int i=0; i<3; i++){
    for (int j=0; j<3; j++){
      for (int k=0; k<3; k++){
        for (int l=0; l<3; l++){
          for (int p=0; p<3; p++){
            for (int q=0; q<3; q++){
              dStressDF(i,j,p,q) += (dFedF(i,k,p,q)*S(k,l)*Fpinv(j,l) + Fe(i,k)*DSDF(k,l,p,q)*Fpinv(j,l)+Fe(i,k)*S(k,l)*DinvFpdF(j,l,p,q));
            }
          }
        }
      }
    }
  }
}

//-------------------------------------------------------------------------------------added
void mlawNonLocalPorosity::tangentComputationLocalWithTemperature(STensor43& dStressDF,STensor3& dStressDT,
                              const bool plastic,
                              const STensor3& F,  // current F
                              const STensor3& corKir,  // cor Kir
                              const STensor3& S, //  second PK
                              const STensor3& Fepr, const STensor3& Fppr, // predictor
                              const STensor43& Lpr,
                              const STensor3& Fe, const STensor3& Fp, // corrector
                              const STensor43& L, const STensor63& dL, // corrector value
                              const STensor43& DcorKirDEepr, const STensor3& DcorKirDT, // small strain tangents
                              const STensor43& dFpDEepr, const STensor3& DFpDT,
                              const STensor43& EprToF, const STensor3& Fpinv
                              ) const
{
  // P = Fe. (Kcor:Le) . invFp
  // need to compute dFedF, dKcordF, dinvFpdF, dLedF
  //DfVstarDT = DfVDT;
  static STensor3 DinvFpDT;
  STensorOperation::zero(DinvFpDT);//---added

  if(plastic){
    for (int i=0; i<3; i++){
      for (int j=0; j<3; j++){
        for (int p=0; p<3; p++){
          for (int q=0; q<3; q++){
             DinvFpDT(i,j) -= Fpinv(i,p)*DFpDT(p,q)*Fpinv(q,j);
          }
        }
      }
    }
  }

  static STensor3 dFeDT;
  STensorOperation::zero(dFeDT);


  if(plastic){
    for (int i=0; i<3; i++){
      for (int j=0; j<3; j++){
        for (int k=0; k<3; k++){
          dFeDT(i,j) += F(i,k)*DinvFpDT(k,j);
        }
      }
    }
  }

  static STensor43 DLDT;
  STensorOperation::zero(DLDT);
  for (int i=0; i<3; i++){
    for (int j=0; j<3; j++){
      for (int k=0; k<3; k++){
        for (int l=0; l<3; l++){
          for (int r=0; r<3; r++){
            for (int s=0; s<3; s++){
              for (int a=0; a<3; a++){
                DLDT(i,j,k,l) += dL(i,j,k,l,r,s)*2.*Fe(a,r)*dFeDT(a,s);
              }
            }
          }
        }
      }
    }
  }

  static STensor3 DSDT;
  STensorOperation::zero(DSDT);
  for (int i=0; i<3; i++){
    for (int j=0; j<3; j++){
      for (int k=0; k<3; k++){
        for (int l=0; l<3; l++){
          for (int r=0; r<3; r++){
            for (int s=0; s<3; s++){
              for (int a=0; a<3; a++){
                 DSDT(i,j) += DcorKirDT(r,s)*L(r,s,i,j) + corKir(r,s)*DLDT(r,s,i,j);
              }
            }
          }
        }
      }
    }
  }

  STensorOperation::zero(dStressDT);
  for (int i=0; i<3; i++){
    for (int j=0; j<3; j++){
      for (int k=0; k<3; k++){
        for (int l=0; l<3; l++){
           dStressDT(i,j) += (dFeDT(i,k)*S(k,l)*Fpinv(j,l) + Fe(i,k)*DSDT(k,l)*Fpinv(j,l)+Fe(i,k)*S(k,l)*DinvFpDT(j,l));
        }
      }
    }
  }

  tangentComputationLocal(dStressDF,plastic,F,corKir,S,Fepr,Fppr,Lpr,Fe,Fp,L,dL,DcorKirDEepr,dFpDEepr,EprToF,Fpinv);


}
//-------------------------------------------------------------------------------------------end
void mlawNonLocalPorosity::tangentComputation(STensor43& dFedF, STensor3& dFeDtildefV, STensor43& dStressDF,
                              STensor3& dStressDtildefV, STensor3& DfVdF,
                              const bool plastic,
                              const STensor3& F,  // current F
                              const STensor3& corKir,  // cor Kir
                              const STensor3& S, //  second PK
                              const STensor3& Fepr, const STensor3& Fppr, // predictor
                              const STensor43& Lpr,
                              const STensor3& Fe, const STensor3& Fp, // corrector
                              const STensor43& L, const STensor63& dL, // corrector value
                              const STensor43& DcorKirDEepr, const STensor3& DcorKirDtildefV,  // small strain tangents
                              const STensor43& dFpDEepr, const STensor3& DFpDtildefV,
                              const STensor3& DfVDEpr,
                              const STensor43& EprToF, const STensor3& Fpinv
                              ) const{
  // P = Fe. (Kcor:Le) . invFp
  // need to compute dFedF, dKcordF, dinvFpdF, dLedF

  STensorOperation::multSTensor3STensor43(DfVDEpr,EprToF,DfVdF);

  static STensor43 DcorKirDF, dFpdF;
  STensorOperation::multSTensor43(DcorKirDEepr,EprToF,DcorKirDF);
  if (plastic){
    STensorOperation::multSTensor43(dFpDEepr,EprToF,dFpdF);
  }
  else{
    STensorOperation::zero(dFpdF);
  }

  // done DcorKirDF, DcorKirDtildefV, DFpDF, DFpDtildefV

  static STensor43 DinvFpdF;
  static STensor3 DinvFpDtildefV;
  STensorOperation::zero(DinvFpdF);
  STensorOperation::zero(DinvFpDtildefV);
  if (plastic){
    for (int i=0; i<3; i++){
      for (int j=0; j<3; j++){
        for (int p=0; p<3; p++){
          for (int q=0; q<3; q++){
            DinvFpDtildefV(i,j) -= Fpinv(i,p)*DFpDtildefV(p,q)*Fpinv(q,j);
            for (int k=0; k<3; k++){
              for (int l=0; l<3; l++){
                DinvFpdF(i,j,k,l) -= Fpinv(i,p)*dFpdF(p,q,k,l)*Fpinv(q,j);
              }
            }
          }
        }
      }
    }
  }

  STensorOperation::zero(dFedF);
  STensorOperation::zero(dFeDtildefV);
  for (int i=0; i<3; i++){
    for (int j=0; j<3; j++){
      for (int k=0; k<3; k++){
        dFedF(i,j,i,k) += Fpinv(k,j);
        if (plastic){
          dFeDtildefV(i,j) += F(i,k)*DinvFpDtildefV(k,j);
          for (int l=0; l<3; l++){
            for (int p=0; p<3; p++){
              dFedF(i,j,k,l) += F(i,p)*DinvFpdF(p,j,k,l);
            }
          }
        }
      }
    }
  }
  static STensor63 DLDF;
  static STensor43 DLDtildefV;
  STensorOperation::zero(DLDF);
  STensorOperation::zero(DLDtildefV);
  for (int i=0; i<3; i++){
    for (int j=0; j<3; j++){
      for (int k=0; k<3; k++){
        for (int l=0; l<3; l++){
          for (int r=0; r<3; r++){
            for (int s=0; s<3; s++){
              for (int a=0; a<3; a++){
                DLDtildefV(i,j,k,l) += dL(i,j,k,l,r,s)*2.*Fe(a,r)*dFeDtildefV(a,s);
                for (int p=0; p<3; p++){
                  for (int q=0; q<3; q++){
                    DLDF(i,j,k,l,p,q) += dL(i,j,k,l,r,s)*2.*Fe(a,r)*dFedF(a,s,p,q);
                  }
                }
              }
            }
          }
        }
      }
    }
  }
  //

  static STensor43 DSDF; // S = corKir:L
  static STensor3 DSDtildefV;
  STensorOperation::zero(DSDF);
  STensorOperation::zero(DSDtildefV);
  for (int i=0; i<3; i++){
    for (int j=0; j<3; j++){
      for (int r=0; r<3; r++){
        for (int s=0; s<3; s++){
          DSDtildefV(i,j) += DcorKirDtildefV(r,s)*L(r,s,i,j) + corKir(r,s)*DLDtildefV(r,s,i,j);
          for (int k=0; k<3; k++){
            for (int l=0; l<3; l++){
              DSDF(i,j,k,l) += DcorKirDF(r,s,k,l)*L(r,s,i,j) + corKir(r,s)*DLDF(r,s,i,j,k,l);
            }
          }
        }
      }
    }
  }


  // compute mechanical tengent
  STensorOperation::zero(dStressDF);
  STensorOperation::zero(dStressDtildefV);
  for (int i=0; i<3; i++){
    for (int j=0; j<3; j++){
      for (int k=0; k<3; k++){
        for (int l=0; l<3; l++){
          dStressDtildefV(i,j) += (dFeDtildefV(i,k)*S(k,l)*Fpinv(j,l) + Fe(i,k)*DSDtildefV(k,l)*Fpinv(j,l)+Fe(i,k)*S(k,l)*DinvFpDtildefV(j,l));
          for (int p=0; p<3; p++){
            for (int q=0; q<3; q++){
              dStressDF(i,j,p,q) += (dFedF(i,k,p,q)*S(k,l)*Fpinv(j,l) + Fe(i,k)*DSDF(k,l,p,q)*Fpinv(j,l)+Fe(i,k)*S(k,l)*DinvFpdF(j,l,p,q));
            }
          }
        }
      }
    }
  }
};

void mlawNonLocalPorosity::tangentComputation(STensor43& dFedF, std::vector<STensor3>& dFeDNonlocalVar, STensor43& dStressDF,
                              std::vector<STensor3>& dStressDNonlocalVar, std::vector<STensor3>& DLocalVardF,
                              const bool plastic,
                              const STensor3& F,  // current F
                              const STensor3& corKir,  // cor Kir
                              const STensor3& S, //  second PK
                              const STensor3& Fepr, const STensor3& Fppr, // predictor
                              const STensor43& Lpr,
                              const STensor3& Fe, const STensor3& Fp, // corrector
                              const STensor43& L, const STensor63& dL, // corrector value
                              const STensor43& DcorKirDEepr, const std::vector<STensor3>& DcorKirDNonlocalVar,  // small strain tangents
                              const STensor43& dFpDEepr, const std::vector<STensor3>& DFpDNonlocalVar,
                              const std::vector<STensor3>& DLocalVarDEpr,
                              const STensor43& EprToF, const STensor3& Fpinv
                              ) const{
  // P = Fe. (Kcor:Le) . invFp
  // need to compute dFedF, dKcordF, dinvFpdF, dLedF
  for (int i=0; i< _numNonLocalVar; i++){
    STensorOperation::multSTensor3STensor43(DLocalVarDEpr[i],EprToF,DLocalVardF[i]);
  }

  static STensor43 DcorKirDF, dFpdF;
  STensorOperation::multSTensor43(DcorKirDEepr,EprToF,DcorKirDF);
  if (plastic){
    STensorOperation::multSTensor43(dFpDEepr,EprToF,dFpdF);
  }
  else{
    STensorOperation::zero(dFpdF);
  }

  // done DcorKirDF, DcorKirDtildefV, DFpDF, DFpDtildefV

  static STensor43 DinvFpdF;
  static std::vector<STensor3> DinvFpDNonlocalVar;
  if (DinvFpDNonlocalVar.size() != _numNonLocalVar)
    DinvFpDNonlocalVar.resize(_numNonLocalVar);

  STensorOperation::zero(DinvFpdF);
  for (int il=0; il< _numNonLocalVar; il++){
    STensorOperation::zero(DinvFpDNonlocalVar[il]);
  }
  if (plastic){
    for (int i=0; i<3; i++){
      for (int j=0; j<3; j++){
        for (int p=0; p<3; p++){
          for (int q=0; q<3; q++){
            for (int il=0; il< _numNonLocalVar; il++){
              DinvFpDNonlocalVar[il](i,j) -= Fpinv(i,p)*DFpDNonlocalVar[il](p,q)*Fpinv(q,j);
            }

            for (int k=0; k<3; k++){
              for (int l=0; l<3; l++){
                DinvFpdF(i,j,k,l) -= Fpinv(i,p)*dFpdF(p,q,k,l)*Fpinv(q,j);
              }
            }
          }
        }
      }
    }
  }

  STensorOperation::zero(dFedF);
  for (int il=0; il < _numNonLocalVar; il++){
    STensorOperation::zero(dFeDNonlocalVar[il]);
  }
  for (int i=0; i<3; i++){
    for (int j=0; j<3; j++){
      for (int k=0; k<3; k++){
        dFedF(i,j,i,k) += Fpinv(k,j);
        if (plastic){
          for (int il=0; il<_numNonLocalVar; il++){
            dFeDNonlocalVar[il](i,j) += F(i,k)*DinvFpDNonlocalVar[il](k,j);
          }
          for (int l=0; l<3; l++){
            for (int p=0; p<3; p++){
              dFedF(i,j,k,l) += F(i,p)*DinvFpdF(p,j,k,l);
            }
          }
        }
      }
    }
  }
  static STensor63 DLDF;
  static std::vector<STensor43> DLDNonlocalVar;
  if (DLDNonlocalVar.size() != _numNonLocalVar){
    DLDNonlocalVar.resize(_numNonLocalVar);
  }
  STensorOperation::zero(DLDF);
  for (int il=0; il< _numNonLocalVar; il++){
    STensorOperation::zero(DLDNonlocalVar[il]);
  }
  for (int i=0; i<3; i++){
    for (int j=0; j<3; j++){
      for (int k=0; k<3; k++){
        for (int l=0; l<3; l++){
          for (int r=0; r<3; r++){
            for (int s=0; s<3; s++){
              for (int a=0; a<3; a++){
                for (int il=0; il< _numNonLocalVar; il++){
                  DLDNonlocalVar[il](i,j,k,l) += dL(i,j,k,l,r,s)*2.*Fe(a,r)*dFeDNonlocalVar[il](a,s);
                }
                for (int p=0; p<3; p++){
                  for (int q=0; q<3; q++){
                    DLDF(i,j,k,l,p,q) += dL(i,j,k,l,r,s)*2.*Fe(a,r)*dFedF(a,s,p,q);
                  }
                }
              }
            }
          }
        }
      }
    }
  }
  //

  static STensor43 DSDF; // S = corKir:L
  static std::vector<STensor3> DSDNonlocalVar;
  if (DSDNonlocalVar.size() != _numNonLocalVar){
    DSDNonlocalVar.resize(_numNonLocalVar);
  }
  STensorOperation::zero(DSDF);
  for (int il=0; il<_numNonLocalVar; il++){
    STensorOperation::zero(DSDNonlocalVar[il]);
  }
  for (int i=0; i<3; i++){
    for (int j=0; j<3; j++){
      for (int r=0; r<3; r++){
        for (int s=0; s<3; s++){
          for (int il=0; il<_numNonLocalVar; il++){
            DSDNonlocalVar[il](i,j) += DcorKirDNonlocalVar[il](r,s)*L(r,s,i,j) + corKir(r,s)*DLDNonlocalVar[il](r,s,i,j);
          }
          for (int k=0; k<3; k++){
            for (int l=0; l<3; l++){
              DSDF(i,j,k,l) += DcorKirDF(r,s,k,l)*L(r,s,i,j) + corKir(r,s)*DLDF(r,s,i,j,k,l);
            }
          }
        }
      }
    }
  }


  // compute mechanical tengent
  STensorOperation::zero(dStressDF);
  for (int il=0; il<_numNonLocalVar; il++){
    STensorOperation::zero(dStressDNonlocalVar[il]);
  }
  for (int i=0; i<3; i++){
    for (int j=0; j<3; j++){
      for (int k=0; k<3; k++){
        for (int l=0; l<3; l++){
          for (int il=0; il<_numNonLocalVar; il++){
            dStressDNonlocalVar[il](i,j) += (dFeDNonlocalVar[il](i,k)*S(k,l)*Fpinv(j,l) + Fe(i,k)*DSDNonlocalVar[il](k,l)*Fpinv(j,l)+Fe(i,k)*S(k,l)*DinvFpDNonlocalVar[il](j,l));
          }
          for (int p=0; p<3; p++){
            for (int q=0; q<3; q++){
              dStressDF(i,j,p,q) += (dFedF(i,k,p,q)*S(k,l)*Fpinv(j,l) + Fe(i,k)*DSDF(k,l,p,q)*Fpinv(j,l)+Fe(i,k)*S(k,l)*DinvFpdF(j,l,p,q));
            }
          }
        }
      }
    }
  }
};

//-------------------------added
void mlawNonLocalPorosity::tangentComputationWithTemperature(STensor43& dFedF, STensor3& dFeDtildefV, STensor43& dStressDF,
                              STensor3& dStressDtildefVstar,STensor3& dStressDT, STensor3& DfVdF,
                              const bool plastic,
                              const STensor3& F,  // current F
                              const STensor3& corKir,  // cor Kir
                              const STensor3& S, //  second PK
                              const STensor3& Fepr, const STensor3& Fppr, // predictor
                              const STensor43& Lpr,
                              const STensor3& Fe, const STensor3& Fp, // corrector
                              const STensor43& L, const STensor63& dL, // corrector value
                              const STensor43& DcorKirDEepr, const STensor3& DcorKirDtildefVstar, const STensor3& DcorKirDT, // small strain tangents
                              const STensor43& dFpDEepr, const STensor3& DFpDtildefVstar, const STensor3& DFpDT,
                              const STensor3& DfVDEpr,
                              const STensor43& EprToF, const STensor3& Fpinv
                              ) const
{
  // P = Fe. (Kcor:Le) . invFp
  // need to compute dFedF, dKcordF, dinvFpdF, dLedF
  static STensor3 DinvFpDT;
  STensorOperation::zero(DinvFpDT);//---added

  if(plastic){
    for (int i=0; i<3; i++){
      for (int j=0; j<3; j++){
        for (int p=0; p<3; p++){
          for (int q=0; q<3; q++){
             DinvFpDT(i,j) -= Fpinv(i,p)*DFpDT(p,q)*Fpinv(q,j);
          }
        }
      }
    }
  }

  static STensor3 dFeDT;
  STensorOperation::zero(dFeDT);


  if(plastic){
    for (int i=0; i<3; i++){
      for (int j=0; j<3; j++){
        for (int k=0; k<3; k++){
          dFeDT(i,j) += F(i,k)*DinvFpDT(k,j);
        }
      }
    }
  }

  static STensor43 DLDT;
  STensorOperation::zero(DLDT);
  for (int i=0; i<3; i++){
    for (int j=0; j<3; j++){
      for (int k=0; k<3; k++){
        for (int l=0; l<3; l++){
          for (int r=0; r<3; r++){
            for (int s=0; s<3; s++){
              for (int a=0; a<3; a++){
                DLDT(i,j,k,l) += dL(i,j,k,l,r,s)*2.*Fe(a,r)*dFeDT(a,s);
              }
            }
          }
        }
      }
    }
  }

  static STensor3 DSDT;
  STensorOperation::zero(DSDT);
  for (int i=0; i<3; i++){
    for (int j=0; j<3; j++){
      for (int r=0; r<3; r++){
        for (int s=0; s<3; s++){
          DSDT(i,j) += DcorKirDT(r,s)*L(r,s,i,j) + corKir(r,s)*DLDT(r,s,i,j);
        }
      }
    }
  }

  STensorOperation::zero(dStressDT);
  for (int i=0; i<3; i++){
    for (int j=0; j<3; j++){
      for (int k=0; k<3; k++){
        for (int l=0; l<3; l++){
           dStressDT(i,j) += (dFeDT(i,k)*S(k,l)*Fpinv(j,l) + Fe(i,k)*DSDT(k,l)*Fpinv(j,l)+Fe(i,k)*S(k,l)*DinvFpDT(j,l));
        }
      }
    }
  }

  tangentComputation(dFedF,dFeDtildefV,dStressDF,dStressDtildefVstar,DfVdF,plastic,F,corKir,
                   S,Fepr,Fppr,Lpr,Fe,Fp,L,dL,DcorKirDEepr,DcorKirDtildefVstar,dFpDEepr,
                   DFpDtildefVstar,DfVDEpr,EprToF,Fpinv);

//L.print("L");DLDT.print("DLDT");corKir.print("corKir");DcorKirDT.print("DcorKirDT");
}
//-------------------------end

void mlawNonLocalPorosity::tangentComputationWithTemperature(STensor43& dFedF, std::vector<STensor3>& dFeDNonlocalVar, STensor43& dStressDF,
                                std::vector<STensor3>& dStressDNonlocalVar, STensor3& dStressDT,
                                std::vector<STensor3>& DLocalVardF,
                                const bool plastic,
                                const STensor3& F,  // current F
                                const STensor3& corKir,  // cor Kir
                                const STensor3& S, //  second PK
                                const STensor3& Fepr, const STensor3& Fppr, // predictor
                                const STensor43& Lpr,
                                const STensor3& Fe, const STensor3& Fp, // corrector
                                const STensor43& L, const STensor63& dL, // corrector value
                                const STensor43& DcorKirDEepr, const std::vector<STensor3>& DcorKirDNonlocalVar, const STensor3& DcorKirDT, // small strain tangents
                                const STensor43& dFpDEepr, const std::vector<STensor3>& DFpDNonlocalVar, const STensor3& DFpDT,
                                const std::vector<STensor3>& DLocalVarDEpr,
                                const STensor43& EprToF, const STensor3& Fpinv
                                ) const{

  static STensor3 DinvFpDT;
  STensorOperation::zero(DinvFpDT);//---added

  if(plastic){
    for (int i=0; i<3; i++){
      for (int j=0; j<3; j++){
        for (int p=0; p<3; p++){
          for (int q=0; q<3; q++){
             DinvFpDT(i,j) -= Fpinv(i,p)*DFpDT(p,q)*Fpinv(q,j);
          }
        }
      }
    }
  }

  static STensor3 dFeDT;
  STensorOperation::zero(dFeDT);


  if(plastic){
    for (int i=0; i<3; i++){
      for (int j=0; j<3; j++){
        for (int k=0; k<3; k++){
          dFeDT(i,j) += F(i,k)*DinvFpDT(k,j);
        }
      }
    }
  }

  static STensor43 DLDT;
  STensorOperation::zero(DLDT);
  for (int i=0; i<3; i++){
    for (int j=0; j<3; j++){
      for (int k=0; k<3; k++){
        for (int l=0; l<3; l++){
          for (int r=0; r<3; r++){
            for (int s=0; s<3; s++){
              for (int a=0; a<3; a++){
                DLDT(i,j,k,l) += dL(i,j,k,l,r,s)*2.*Fe(a,r)*dFeDT(a,s);
              }
            }
          }
        }
      }
    }
  }

  static STensor3 DSDT;
  STensorOperation::zero(DSDT);
  for (int i=0; i<3; i++){
    for (int j=0; j<3; j++){
      for (int r=0; r<3; r++){
        for (int s=0; s<3; s++){
          DSDT(i,j) += DcorKirDT(r,s)*L(r,s,i,j) + corKir(r,s)*DLDT(r,s,i,j);
        }
      }
    }
  }

  STensorOperation::zero(dStressDT);
  for (int i=0; i<3; i++){
    for (int j=0; j<3; j++){
      for (int k=0; k<3; k++){
        for (int l=0; l<3; l++){
           dStressDT(i,j) += (dFeDT(i,k)*S(k,l)*Fpinv(j,l) + Fe(i,k)*DSDT(k,l)*Fpinv(j,l)+Fe(i,k)*S(k,l)*DinvFpDT(j,l));
        }
      }
    }
  }

  tangentComputation(dFedF,dFeDNonlocalVar,dStressDF,dStressDNonlocalVar,DLocalVardF,plastic,F,corKir,
                   S,Fepr,Fppr,Lpr,Fe,Fp,L,dL,DcorKirDEepr,DcorKirDNonlocalVar,dFpDEepr,
                   DFpDNonlocalVar,DLocalVarDEpr,EprToF,Fpinv);

};

void mlawNonLocalPorosity::constitutive(
                            const STensor3& F0,         // initial deformation gradient (input @ time n)
                            const STensor3& F1,         // updated deformation gradient (input @ time n+1)
                            STensor3 &P,                // updated 1st Piola-Kirchhoff stress tensor (output)
                            const IPNonLocalPorosity *ipvprev,       // array of initial internal variable
                            IPNonLocalPorosity *ipvcur,             // updated array of internal variable (in ipvcur on output),
                            STensor43 &Tangent,         // constitutive tangents (output)
                            std::vector<STensor3>  &dLocaldVarDStrain,
                            std::vector<STensor3>  &dStressDNonLocalVar,
                            fullMatrix<double>    &dLocalVarDNonLocalVar,
                            const bool stiff,            // if true compute the tangents
                           STensor43* elasticTangent) const
{
  if (getNonLocalMethod() == NONLOCAL_POROSITY){
    this->constitutive_NonLocalPorosity(F0,F1,P,ipvprev,ipvcur,Tangent,dLocaldVarDStrain[0],dStressDNonLocalVar[0],dLocalVarDNonLocalVar(0,0),stiff,elasticTangent);
  }
  else if (getNonLocalMethod() == NONLOCAL_LOG_POROSITY){
    this->constitutive_NonLocalLogarithmPorosity(F0,F1,P,ipvprev,ipvcur,Tangent,dLocaldVarDStrain[0],dStressDNonLocalVar[0],dLocalVarDNonLocalVar(0,0),stiff,elasticTangent);
  }
  else if (getNonLocalMethod() == NONLOCAL_FULL_THREEVAR){
    this->constitutive_multipleNonLocalVariables(F0,F1,P,ipvprev,ipvcur,Tangent,dLocaldVarDStrain,dStressDNonLocalVar,dLocalVarDNonLocalVar,stiff,elasticTangent);
  }
  else{
    Msg::Error("nonlocal method has not been implemented mlawNonLocalPorosity::constitutive");
  }
};

void mlawNonLocalPorosity::I1J2J3_constitutive(
                            const STensor3& F0,         // initial deformation gradient (input @ time n)
                            const STensor3& F1,         // updated deformation gradient (input @ time n+1)
                            STensor3 &P,                // updated 1st Piola-Kirchhoff stress tensor (output)
                            const IPNonLocalPorosity *ipvprev,       // array of initial internal variable
                            IPNonLocalPorosity *ipvcur,             // updated array of internal variable (in ipvcur on output),
                            STensor43 &Tangent,         // constitutive tangents (output)
                            std::vector<STensor3>  &dLocaldVarDStrain,
                            std::vector<STensor3>  &dStressDNonLocalVar,
                            fullMatrix<double>    &dLocalVarDNonLocalVar,
                            const bool stiff,            // if true compute the tangents
                           STensor43* elasticTangent) const
{
 if (getNonLocalMethod() == NONLOCAL_FULL_THREEVAR){
    this->I1J2J3_constitutive_NonLocal(F0,F1,P,ipvprev,ipvcur,Tangent,dLocaldVarDStrain,dStressDNonLocalVar,dLocalVarDNonLocalVar,stiff,elasticTangent);
  }
  else{
    Msg::Error("nonlocal method has not been implemented mlawNonLocalPorosity::constitutive");
  }
};

void mlawNonLocalPorosity::constitutive(const STensor3& F0,         // initial deformation gradient (input @ time n)
                              const STensor3& F1,         // updated deformation gradient (input @ time n+1)
                              STensor3 &P,                // updated 1st Piola-Kirchhoff stress tensor (output)
                              const IPNonLocalPorosity *q0,       // array of initial internal variable
                              IPNonLocalPorosity *q1,             // updated array of internal variable (in ipvcur on output),
                              STensor43 &Tangent,         // constitutive tangents (output)
                              std::vector<STensor3>  &dLocalVarDStrain,
                              std::vector<STensor3>  &dStressDNonLocalVar,
                              fullMatrix<double>    &dLocalVarDNonLocalVar,
                              const bool stiff,
                              STensor3& dPdT,
                              STensor33 &dPdgradT,
                              fullMatrix<double>  &dLocalVarDT,
                              const double& T0, // previous temperature
                              const double& T1, // current temperature
                              const SVector3 &gradT0, // previous temperature gradient
                              const SVector3 &gradT, // current temperature gradient
                              SVector3 &fluxT, // temperature flux
                              STensor33 &dfluxTdF, // thermal-mechanical coupling
                              std::vector<SVector3> &dfluxTdNonLocalVar,
                              SVector3 &dfluxTdT,
                              STensor3 &dfluxTdgradT, // thermal tangent
                              double &thermalSource,   // - cp*dTdt
                              STensor3 &dthermalSourcedF,
                              fullMatrix<double> &dthermalSourcedNonLocalVar,
                              double &dthermalSourcedT, // thermal source
                              SVector3 &dthermalSourcedgradT,
                              double& mechanicalSource, // mechanical source--> convert to heat
                              STensor3 & dmechanicalSourceF,
                              fullMatrix<double> & dmechanicalSourcedNonLocalVar,
                              double & dmechanicalSourcedT,
                              SVector3 &dmechanicalSourcedgradT,
                              STensor43* elasticTangent
                              ) const{
  if(elasticTangent!=NULL) Msg::Error("mlawNonLocalPorosity elasticTangent not computed");
  static STensor43 dFedF;
  static std::vector<STensor3> dFeDNonlocalVar(this->getNumNonLocalVariables());
  if (stiff and _tangentByPerturbation){
    predictorCorrector(F0,F1,P,q0,q1,Tangent,dLocalVarDStrain,dStressDNonLocalVar,dLocalVarDNonLocalVar,false,
                       dFedF,dFeDNonlocalVar,dPdT,dPdgradT,dLocalVarDT,
                       T0,T1,gradT0,gradT,fluxT,dfluxTdF,dfluxTdNonLocalVar,dfluxTdT,dfluxTdgradT,
                       thermalSource,dthermalSourcedF,dthermalSourcedNonLocalVar,dthermalSourcedT,dthermalSourcedgradT,
                       mechanicalSource,dmechanicalSourceF,dmechanicalSourcedNonLocalVar,dmechanicalSourcedT,dmechanicalSourcedgradT);
    static STensor3 Fplus, Pplus;
    static SVector3 fluxTPlus, gradTplus;
    static double thermalSourcePlus;
    static double mechanicalSourcePlus;
    
    static IPNonLocalPorosity q1Plus(*dynamic_cast<const IPNonLocalPorosity*>(q0));
        
    // perturbe F
    q1Plus.operator =(*dynamic_cast<const IPVariable*>(q1));
    for (int i=0; i<3; i++){
      for (int j=0; j<3; j++){
        Fplus = (F1);
        Fplus(i,j) += _perturbationfactor;
        predictorCorrector(F0,Fplus,Pplus,q0,&q1Plus,Tangent,dLocalVarDStrain,dStressDNonLocalVar,dLocalVarDNonLocalVar,false,
                       dFedF,dFeDNonlocalVar,dPdT,dPdgradT,dLocalVarDT,
                       T0,T1,gradT0,gradT,fluxTPlus,dfluxTdF,dfluxTdNonLocalVar,dfluxTdT,dfluxTdgradT,
                       thermalSourcePlus,dthermalSourcedF,dthermalSourcedNonLocalVar,dthermalSourcedT,dthermalSourcedgradT,
                       mechanicalSourcePlus,dmechanicalSourceF,dmechanicalSourcedNonLocalVar,dmechanicalSourcedT,dmechanicalSourcedgradT);
        for (int k=0; k<3; k++){
          for (int l=0; l<3; l++){
            Tangent(k,l,i,j) = (Pplus(k,l) - P(k,l))/(_perturbationfactor);
          }
          dfluxTdF(k,i,j) = (fluxTPlus(k)- fluxT(k))/_perturbationfactor;
        }
        for (int inl=0; inl < _numNonLocalVar; inl++){
          double localVarPlus = this->getLocalVariable(&q1Plus,inl);
          double localVar = this->getLocalVariable(q1,inl);
          dLocalVarDStrain[inl](i,j) = (localVarPlus-localVar)/_perturbationfactor;
        }
        
        q1->getRefToDPlasticEnergyDF()(i,j) = (q1Plus.plasticEnergy() - q1->plasticEnergy())/_perturbationfactor;
        
        dthermalSourcedF(i,j) = (thermalSourcePlus-thermalSource)/_perturbationfactor;
        dmechanicalSourceF(i,j) = (mechanicalSourcePlus - mechanicalSource)/_perturbationfactor;
      }
    }
    
    // nonlocal variables
    for (int inl = 0; inl < _numNonLocalVar; inl++){
      q1Plus.operator =(*dynamic_cast<const IPVariable*>(q1));
      double& nonlocalVar = this->getRefToNonLocalVariable(&q1Plus,inl);
      nonlocalVar += _perturbationfactor;
      
      predictorCorrector(F0,F1,Pplus,q0,&q1Plus,Tangent,dLocalVarDStrain,dStressDNonLocalVar,dLocalVarDNonLocalVar,false,
                       dFedF,dFeDNonlocalVar,dPdT,dPdgradT,dLocalVarDT,
                       T0,T1,gradT0,gradT,fluxTPlus,dfluxTdF,dfluxTdNonLocalVar,dfluxTdT,dfluxTdgradT,
                       thermalSourcePlus,dthermalSourcedF,dthermalSourcedNonLocalVar,dthermalSourcedT,dthermalSourcedgradT,
                       mechanicalSourcePlus,dmechanicalSourceF,dmechanicalSourcedNonLocalVar,dmechanicalSourcedT,dmechanicalSourcedgradT);
      for (int k=0; k<3; k++){
        for (int l=0; l<3; l++){
          dStressDNonLocalVar[inl](k,l) = (Pplus(k,l)-P(k,l))/_perturbationfactor;
        }
        dfluxTdNonLocalVar[inl](k) = (fluxTPlus(k) - fluxT(k))/_perturbationfactor;
      }
      
      for (int jj=0; jj < _numNonLocalVar; jj++){
        double localVarPlus = this->getLocalVariable(&q1Plus,jj);
        double localVar = this->getLocalVariable(q1,jj);
        dLocalVarDNonLocalVar(jj,inl) = (localVarPlus-localVar)/_perturbationfactor;
      }
      q1->getRefToDPlasticEnergyDNonLocalVariable(inl) = (q1Plus.plasticEnergy() - q1->plasticEnergy())/_perturbationfactor;
      dthermalSourcedNonLocalVar(0,inl) = (thermalSourcePlus-thermalSource)/_perturbationfactor;
      dmechanicalSourcedNonLocalVar(0,inl) = (mechanicalSourcePlus-mechanicalSource)/_perturbationfactor;
    }
    
    // gradient temperature
    double gradTpert = _perturbationfactor;
    q1Plus.operator =(*dynamic_cast<const IPVariable*>(q1));
    for (int i=0; i<3; i++){
      gradTplus = (gradT);
      gradTplus(i) += (gradTpert);
      predictorCorrector(F0,F1,Pplus,q0,&q1Plus,Tangent,dLocalVarDStrain,dStressDNonLocalVar,dLocalVarDNonLocalVar,false,
                       dFedF,dFeDNonlocalVar,dPdT,dPdgradT,dLocalVarDT,
                       T0,T1,gradT0,gradTplus,fluxTPlus,dfluxTdF,dfluxTdNonLocalVar,dfluxTdT,dfluxTdgradT,
                       thermalSourcePlus,dthermalSourcedF,dthermalSourcedNonLocalVar,dthermalSourcedT,dthermalSourcedgradT,
                       mechanicalSourcePlus,dmechanicalSourceF,dmechanicalSourcedNonLocalVar,dmechanicalSourcedT,dmechanicalSourcedgradT);
      for (int k=0; k<3; k++){
        for (int l=0; l<3; l++){
          dPdgradT(k,l,i) = (Pplus(k,l)-P(k,l))/gradTpert;
        }
        dfluxTdgradT(k,i) = (fluxTPlus(k) - fluxT(k))/gradTpert;
      }
      dthermalSourcedgradT(i) = (thermalSourcePlus-thermalSource)/gradTpert;
      dmechanicalSourcedgradT(i) = (mechanicalSourcePlus-mechanicalSource)/gradTpert;
    }
    // temperature
    double Tplus = T1+ _perturbationfactor*T0;
    q1Plus.operator =(*dynamic_cast<const IPVariable*>(q1));
    predictorCorrector(F0,F1,Pplus,q0,&q1Plus,Tangent,dLocalVarDStrain,dStressDNonLocalVar,dLocalVarDNonLocalVar,false,
                       dFedF,dFeDNonlocalVar,dPdT,dPdgradT,dLocalVarDT,
                       T0,Tplus,gradT0,gradT,fluxTPlus,dfluxTdF,dfluxTdNonLocalVar,dfluxTdT,dfluxTdgradT,
                       thermalSourcePlus,dthermalSourcedF,dthermalSourcedNonLocalVar,dthermalSourcedT,dthermalSourcedgradT,
                       mechanicalSourcePlus,dmechanicalSourceF,dmechanicalSourcedNonLocalVar,dmechanicalSourcedT,dmechanicalSourcedgradT);
    for (int k=0; k<3; k++){
      for (int l=0; l<3; l++){
        dPdT(k,l) = (Pplus(k,l)-P(k,l))/(_perturbationfactor*T0);
      }
      dfluxTdT(k) = (fluxTPlus(k) - fluxT(k))/(_perturbationfactor*T0);
    }
    for (int inl=0; inl< _numNonLocalVar; inl++){
      double localVarPlus = this->getLocalVariable(&q1Plus,inl);
      double localVar = this->getLocalVariable(q1,inl);
      dLocalVarDT(inl,0) = (localVarPlus-localVar)/(_perturbationfactor*T0);
    }
    dthermalSourcedT = (thermalSourcePlus-thermalSource)/(_perturbationfactor*T0);
    dmechanicalSourcedT = (mechanicalSourcePlus-mechanicalSource)/(_perturbationfactor*T0);
  }
  else{
    predictorCorrector(F0,F1,P,q0,q1,Tangent,dLocalVarDStrain,dStressDNonLocalVar,dLocalVarDNonLocalVar,stiff,
                       dFedF,dFeDNonlocalVar,dPdT,dPdgradT,dLocalVarDT,
                       T0,T1,gradT0,gradT,fluxT,dfluxTdF,dfluxTdNonLocalVar,dfluxTdT,dfluxTdgradT,
                       thermalSource,dthermalSourcedF,dthermalSourcedNonLocalVar,dthermalSourcedT,dthermalSourcedgradT,
                       mechanicalSource,dmechanicalSourceF,dmechanicalSourcedNonLocalVar,dmechanicalSourcedT,dmechanicalSourcedgradT);
  }
};


void mlawNonLocalPorosity::predictorCorrector(const STensor3& F0,         // initial deformation gradient (input @ time n)
                              const STensor3& F,         // updated deformation gradient (input @ time n+1)
                              STensor3 &P,                // updated 1st Piola-Kirchhoff stress tensor (output)
                              const IPNonLocalPorosity *q0,       // array of initial internal variable
                              IPNonLocalPorosity *q,             // updated array of internal variable (in ipvcur on output),
                              STensor43 &Tangent,         // constitutive tangents (output)
                              std::vector<STensor3>  &dLocalVarDStrain,
                              std::vector<STensor3>  &dStressDNonLocalVar,
                              fullMatrix<double>    &dLocalVarDNonLocalVar,
                              const bool stiff,
                              STensor43& dFedF, std::vector<STensor3>& dFeDNonlocalVar,
                              STensor3& dPdT,
                              STensor33 &dPdgradT,
                              fullMatrix<double>  &dLocalVarDT,
                              const double& T0, // previous temperature
                              const double& T, // current temperature
                              const SVector3 &gradT0, // previous temperature gradient
                              const SVector3 &gradT, // current temperature gradient
                              SVector3 &fluxT, // temperature flux
                              STensor33 &dfluxTdF, // thermal-mechanical coupling
                              std::vector<SVector3> &dfluxTdNonLocalVar,
                              SVector3 &dfluxTdT,
                              STensor3 &dfluxTdgradT, // thermal tangent
                              double &thermalSource,   // - cp*dTdt
                              STensor3 &dthermalSourcedF,
                              fullMatrix<double> &dthermalSourcedNonLocalVar,
                              double &dthermalSourcedT, // thermal source
                              SVector3 &dthermalSourcedgradT,
                              double& mechanicalSource, // mechanical source--> convert to heat
                              STensor3 & dmechanicalSourceF,
                              fullMatrix<double> & dmechanicalSourcedNonLocalVar,
                              double & dmechanicalSourcedT,
                              SVector3 &dmechanicalSourcedgradT
                              ) const{
  // mechanics part
  this->predictorCorrector(F0,F,P,q0,q,Tangent,dLocalVarDStrain,dStressDNonLocalVar,dLocalVarDNonLocalVar,stiff,dFedF,dFeDNonlocalVar,
                     &dPdT,&dLocalVarDT,T0,T);
  // P not depend on gradT
  STensorOperation::zero(dPdgradT);

  double KT = bulkModulus(T);
  double muT = shearModulus(T);
  double alpT = ThermalExpansion(T);
  double conductT = _thermalConductivity*_tempFunc_thermalConductivity->getVal(T);
  double cpT = _cp*_tempFunc_cp->getVal(T0); // predious --> derivatives 0
  double DKDT = bulkModulusDerivative(T);
  double DmuDT = shearModulusDerivative(T);
  double DalpDT = ThermalExpansionDerivative(T);
  double DconductDT = _thermalConductivity*_tempFunc_thermalConductivity->getDiff(T);
  double DcpdT = 0.;

  // thermal energy
  q->getRefToThermalEnergy() = cpT*T;


  if (this->getTimeStep() <= 0.){
    #ifdef _DEBUG
    Msg::Warning("Time step is zero in mlawNonLocalPorosity::predictorCorector");
    #endif // _DEBUG
    mechanicalSource = 0.;

    // tangent
    if (stiff){
      STensorOperation::zero(dmechanicalSourceF);
      dmechanicalSourcedNonLocalVar.setAll(0.);
      dmechanicalSourcedT =0.;
      STensorOperation::zero(dmechanicalSourcedgradT);
    }

    thermalSource  = 0.;
    // tangent
    if (stiff){
      STensorOperation::zero(dthermalSourcedF);
      STensorOperation::zero(dthermalSourcedgradT);
      dthermalSourcedT = 0.;
      dthermalSourcedNonLocalVar.setAll(0.);
    }
  }
  else{

    // thermoplastic heat
    mechanicalSource = _taylorQuineyFactor*(q->plasticEnergy()-q0->plasticEnergy())/(this->getTimeStep());

    // tangent
    if (stiff){
      dmechanicalSourceF = q->getConstRefToDPlasticEnergyDF();
      dmechanicalSourceF *= (_taylorQuineyFactor/this->getTimeStep());
      for (int inl = 0; inl< _numNonLocalVar; inl++){
        dmechanicalSourcedNonLocalVar(0,inl) = q->getConstRefToDPlasticEnergyDNonLocalVariable(inl)*_taylorQuineyFactor/this->getTimeStep();
      }
      dmechanicalSourcedT = q->getConstRefToDPlasticEnergyDT()*_taylorQuineyFactor/this->getTimeStep();
      STensorOperation::zero(dmechanicalSourcedgradT);
    };

    thermalSource  = -cpT*(T-T0)/this->getTimeStep();
    // tangent
    if (stiff){
      STensorOperation::zero(dthermalSourcedF);
      STensorOperation::zero(dthermalSourcedgradT);
      dthermalSourcedT =-cpT/this->getTimeStep()-DcpdT*(T-T0)/this->getTimeStep();
      for (int inl = 0; inl< _numNonLocalVar; inl++){
        dthermalSourcedNonLocalVar(0,inl) = 0.;
      }      
    }
  }

  // fluxT
  double J  = 1.;
  static STensor3 Finv(0.);
  if (_thermalEstimationPreviousConfig){
    STensorOperation::inverseSTensor3(F0,Finv);
    J = STensorOperation::determinantSTensor3(F0);
  }
  else{
    STensorOperation::inverseSTensor3(F,Finv);
    J = STensorOperation::determinantSTensor3(F);
  }
  static STensor3 Cinv;
  STensorOperation::multSTensor3SecondTranspose(Finv,Finv,Cinv);
  STensorOperation::multSTensor3SVector3(Cinv,gradT,fluxT);
  fluxT *= (-conductT*J);
  
  if (stiff){
    for (int inl = 0; inl< _numNonLocalVar; inl++){
      STensorOperation::zero(dfluxTdNonLocalVar[inl]);
    }
    dfluxTdT = fluxT;
    dfluxTdT *= (DconductDT/conductT);
    dfluxTdgradT = Cinv;
    dfluxTdgradT *= (-conductT*J);
    STensorOperation::zero(dfluxTdF);
    if (!_thermalEstimationPreviousConfig){
      static const STensor43 I4(1.,1.);
      static STensor3 DJDF;
      static STensor43 DCinvDF;
      for (int i=0; i<3; i++){
        for (int j=0; j<3; j++){
          DJDF(i,j) = J*Finv(j,i);
          for (int k=0; k<3; k++){
            for (int l=0; l<3; l++){
              DCinvDF(i,j,k,l) = 0.;
              for (int p=0; p<3; p++){
                for (int a=0; a<3; a++){
                  for (int b=0; b<3; b++){
                    DCinvDF(i,j,k,l) -= 2.*F(k,p)*Cinv(i,a)*Cinv(j,b)*I4(a,b,p,l);
                  }
                }
              }
            }
          }
        }
      }

      for (int i=0; i<3; i++){
        for (int j=0; j<3; j++){
          for (int k=0; k<3; k++){
            dfluxTdF(i,j,k) += (DJDF(j,k)*fluxT(i)/J);
            for (int l=0; l<3; l++){
              dfluxTdF(i,j,k) -= (J*DCinvDF(i,l,j,k)*gradT(l)*conductT);
            }
          }
        };
      }
    }
    
  }
};

void mlawNonLocalPorosity::predictorCorrector(const STensor3& F0,         // initial deformation gradient (input @ time n)
                            const STensor3& F1,         // updated deformation gradient (input @ time n+1)
                            STensor3 &P,                // updated 1st Piola-Kirchhoff stress tensor (output)
                                                        // contains the initial values on input
                            const IPNonLocalPorosity *ipvprev,       // array of initial internal variable
                            IPNonLocalPorosity *ipvcur,             // updated array of internal variable (in ipvcur on output),
                            STensor43 &Tangent,         // constitutive tangents (output)
                            std::vector<STensor3>  &dLocaldVarDStrain,
                            std::vector<STensor3>  &dStressDNonLocalVar,
                            fullMatrix<double>    &dLocalVarDNonLocalVar,
                            const bool stiff,                   // if true compute the tangents
                            STensor43& dFedF, std::vector<STensor3>& dFeDNonlocalVar,
                            STensor3*  dStressDT,
                            fullMatrix<double>* dLocalVarDT,
                            const double T0 ,const double T
                           ) const{

  if (getNonLocalMethod() == NONLOCAL_POROSITY){
    if (isThermomechanicallyCoupled()){
      this->predictorCorrector_NonLocalPorosity(F0,F1,P,ipvprev,ipvcur,Tangent,
                  dLocaldVarDStrain[0],dStressDNonLocalVar[0],dLocalVarDNonLocalVar(0,0),stiff,dFedF,dFeDNonlocalVar[0],dStressDT,&(*dLocalVarDT)(0,0),T0,T);
    }
    else{
      this->predictorCorrector_NonLocalPorosity(F0,F1,P,ipvprev,ipvcur,Tangent,
                  dLocaldVarDStrain[0],dStressDNonLocalVar[0],dLocalVarDNonLocalVar(0,0),stiff,dFedF,dFeDNonlocalVar[0]);
    }
  }
  else if (getNonLocalMethod() == NONLOCAL_LOG_POROSITY){
    if (isThermomechanicallyCoupled()){
      this->predictorCorrector_NonLocalLogarithmPorosity(F0,F1,P,ipvprev,ipvcur,Tangent,
                  dLocaldVarDStrain[0],dStressDNonLocalVar[0],dLocalVarDNonLocalVar(0,0),stiff,dFedF,dFeDNonlocalVar[0],dStressDT,&(*dLocalVarDT)(0,0),T0,T);
    }
    else{
      this->predictorCorrector_NonLocalLogarithmPorosity(F0,F1,P,ipvprev,ipvcur,Tangent,
                  dLocaldVarDStrain[0],dStressDNonLocalVar[0],dLocalVarDNonLocalVar(0,0),stiff,dFedF,dFeDNonlocalVar[0]);
    }
  }
  else if (getNonLocalMethod() == NONLOCAL_FULL_THREEVAR){
    if (isThermomechanicallyCoupled()){
      this->predictorCorrector_multipleNonLocalVariables(F0,F1,P,ipvprev,ipvcur,Tangent,
                  dLocaldVarDStrain,dStressDNonLocalVar,dLocalVarDNonLocalVar,stiff,dFedF,dFeDNonlocalVar,dStressDT,dLocalVarDT,T0,T);
    }
    else{
      this->predictorCorrector_multipleNonLocalVariables(F0,F1,P,ipvprev,ipvcur,Tangent,
                  dLocaldVarDStrain,dStressDNonLocalVar,dLocalVarDNonLocalVar,stiff,dFedF,dFeDNonlocalVar);
    }
  }
  else{
     Msg::Error("nonlocal method has not been implemented mlawNonLocalPorosity::predictorCorrector");
  }
};

void mlawNonLocalPorosity::constitutive_multipleNonLocalVariables(
                            const STensor3& F0,         // initial deformation gradient (input @ time n)
                            const STensor3& F1,         // updated deformation gradient (input @ time n+1)
                            STensor3 &P,                // updated 1st Piola-Kirchhoff stress tensor (output)
                            const IPNonLocalPorosity *ipvprev,       // array of initial internal variable
                            IPNonLocalPorosity *ipvcur,             // updated array of internal variable (in ipvcur on output),
                            STensor43 &Tangent,         // constitutive tangents (output)
                            std::vector<STensor3>  &dLocaldVarDStrain,
                            std::vector<STensor3>  &dStressDNonLocalVar,
                            fullMatrix<double>    &dLocalVarDNonLocalVar,
                            const bool stiff,            // if true compute the tangents
                              STensor43* elasticTangent
                              ) const
{
  if(elasticTangent!=NULL) Msg::Error("mlawNonLocalPorosity elasticTangent not computed");
  static STensor43 dFedF;
  static std::vector<STensor3> dFeDNonLocalVar(this->getNumNonLocalVariables());

  if (stiff and _tangentByPerturbation){
    predictorCorrector_multipleNonLocalVariables(F0,F1,P,ipvprev,ipvcur,Tangent,dLocaldVarDStrain,dStressDNonLocalVar,dLocalVarDNonLocalVar,false,dFedF,dFeDNonLocalVar);
    static STensor3 Fplus, Pplus;
    static IPNonLocalPorosity ipvcurPlus(*dynamic_cast<const IPNonLocalPorosity*>(ipvprev));

    ipvcurPlus.operator =(*dynamic_cast<const IPVariable*>(ipvcur));
    // perturbe F
    for (int i=0; i<3; i++){
      for (int j=0; j<3; j++){
        Fplus = (F1);
        Fplus(i,j) += _perturbationfactor;
        predictorCorrector_multipleNonLocalVariables(F0,Fplus,Pplus,ipvprev,&ipvcurPlus,Tangent,dLocaldVarDStrain,dStressDNonLocalVar,dLocalVarDNonLocalVar,false,dFedF,dFeDNonLocalVar);
        for (int k=0; k<3; k++){
          for (int l=0; l<3; l++){
            Tangent(k,l,i,j) = (Pplus(k,l) - P(k,l))/(_perturbationfactor);
          }
        }
        dLocaldVarDStrain[0](i,j) = (ipvcurPlus.getLocalVolumetricPlasticStrain() - ipvcur->getLocalVolumetricPlasticStrain())/_perturbationfactor;
        dLocaldVarDStrain[1](i,j) = (ipvcurPlus.getLocalMatrixPlasticStrain() - ipvcur->getLocalMatrixPlasticStrain())/_perturbationfactor;
        dLocaldVarDStrain[2](i,j) = (ipvcurPlus.getLocalDeviatoricPlasticStrain() - ipvcur->getLocalDeviatoricPlasticStrain())/_perturbationfactor;

        // energy
				if (this->getMacroSolver()->withPathFollowing()){
					ipvcur->getRefToDIrreversibleEnergyDF()(i,j) = (ipvcurPlus.plasticEnergy() - ipvcur->plasticEnergy())/_perturbationfactor;
				}
      }
    }

    // perturb on nonlocal var
    ipvcurPlus.operator =(*dynamic_cast<const IPVariable*>(ipvcur));
    ipvcurPlus.getRefToNonLocalVolumetricPlasticStrain() += _perturbationfactor;
    predictorCorrector_multipleNonLocalVariables(F0,F1,Pplus,ipvprev,&ipvcurPlus,Tangent,dLocaldVarDStrain,dStressDNonLocalVar,dLocalVarDNonLocalVar,false,dFedF,dFeDNonLocalVar);
    for (int k=0; k<3; k++){
      for (int l=0; l<3; l++){
        dStressDNonLocalVar[0](k,l) = (Pplus(k,l) - P(k,l))/(_perturbationfactor);
      }
    }
    dLocalVarDNonLocalVar(0,0) = (ipvcurPlus.getLocalVolumetricPlasticStrain() - ipvcur->getLocalVolumetricPlasticStrain())/(_perturbationfactor);
    dLocalVarDNonLocalVar(1,0) = (ipvcurPlus.getLocalMatrixPlasticStrain() - ipvcur->getLocalMatrixPlasticStrain())/(_perturbationfactor);
    dLocalVarDNonLocalVar(2,0) = (ipvcurPlus.getLocalDeviatoricPlasticStrain() - ipvcur->getLocalDeviatoricPlasticStrain())/(_perturbationfactor);

		if (this->getMacroSolver()->withPathFollowing()){
			ipvcur->getRefToDIrreversibleEnergyDNonLocalVariable(0) =  (ipvcurPlus.plasticEnergy() - ipvcur->plasticEnergy())/(_perturbationfactor);
		}

    // perturb on nonlocal var
    ipvcurPlus.operator =(*dynamic_cast<const IPVariable*>(ipvcur));
    ipvcurPlus.getRefToNonLocalMatrixPlasticStrain() += _perturbationfactor;
    predictorCorrector_multipleNonLocalVariables(F0,F1,Pplus,ipvprev,&ipvcurPlus,Tangent,dLocaldVarDStrain,dStressDNonLocalVar,dLocalVarDNonLocalVar,false,dFedF,dFeDNonLocalVar);
    for (int k=0; k<3; k++){
      for (int l=0; l<3; l++){
        dStressDNonLocalVar[1](k,l) = (Pplus(k,l) - P(k,l))/(_perturbationfactor);
      }
    }
    dLocalVarDNonLocalVar(0,1) = (ipvcurPlus.getLocalVolumetricPlasticStrain() - ipvcur->getLocalVolumetricPlasticStrain())/(_perturbationfactor);
    dLocalVarDNonLocalVar(1,1) = (ipvcurPlus.getLocalMatrixPlasticStrain() - ipvcur->getLocalMatrixPlasticStrain())/(_perturbationfactor);
    dLocalVarDNonLocalVar(2,1) = (ipvcurPlus.getLocalDeviatoricPlasticStrain() - ipvcur->getLocalDeviatoricPlasticStrain())/(_perturbationfactor);

		if (this->getMacroSolver()->withPathFollowing()){
			ipvcur->getRefToDIrreversibleEnergyDNonLocalVariable(1) =  (ipvcurPlus.plasticEnergy() - ipvcur->plasticEnergy())/(_perturbationfactor);
		}

    // perturb on nonlocal var
    ipvcurPlus.operator =(*dynamic_cast<const IPVariable*>(ipvcur));
    ipvcurPlus.getRefToNonLocalDeviatoricPlasticStrain() += _perturbationfactor;
    predictorCorrector_multipleNonLocalVariables(F0,F1,Pplus,ipvprev,&ipvcurPlus,Tangent,dLocaldVarDStrain,dStressDNonLocalVar,dLocalVarDNonLocalVar,false,dFedF,dFeDNonLocalVar);
    for (int k=0; k<3; k++){
      for (int l=0; l<3; l++){
        dStressDNonLocalVar[2](k,l) = (Pplus(k,l) - P(k,l))/(_perturbationfactor);
      }
    }
    dLocalVarDNonLocalVar(0,2) = (ipvcurPlus.getLocalVolumetricPlasticStrain() - ipvcur->getLocalVolumetricPlasticStrain())/(_perturbationfactor);
    dLocalVarDNonLocalVar(1,2) = (ipvcurPlus.getLocalMatrixPlasticStrain() - ipvcur->getLocalMatrixPlasticStrain())/(_perturbationfactor);
    dLocalVarDNonLocalVar(2,2) = (ipvcurPlus.getLocalDeviatoricPlasticStrain() - ipvcur->getLocalDeviatoricPlasticStrain())/(_perturbationfactor);

		if (this->getMacroSolver()->withPathFollowing()){
			ipvcur->getRefToDIrreversibleEnergyDNonLocalVariable(2) =  (ipvcurPlus.plasticEnergy() - ipvcur->plasticEnergy())/(_perturbationfactor);
		}

  }
  else{
    //F1.print("F1");
    predictorCorrector_multipleNonLocalVariables(F0,F1,P,ipvprev,ipvcur,Tangent,dLocaldVarDStrain,dStressDNonLocalVar,dLocalVarDNonLocalVar,stiff,dFedF,dFeDNonLocalVar);
    //Tangent.print("K");
  }
};

void mlawNonLocalPorosity::constitutive_NonLocalLogarithmPorosity(
                            const STensor3& F0,         // initial deformation gradient (input @ time n)
                            const STensor3& F1,         // updated deformation gradient (input @ time n+1)
                            STensor3 &P,                // updated 1st Piola-Kirchhoff stress tensor (output)
                            const IPNonLocalPorosity *ipvprev,       // array of initial internal variable
                            IPNonLocalPorosity *ipvcur,             // updated array of internal variable (in ipvcur on output),
                            STensor43 &Tangent,         // constitutive tangents (output)
                            STensor3  &dLocaldPorosityDStrain,
                            STensor3  &dStressDNonLocalPorosity,
                            double    &dLocalPorosityDNonLocalPorosity,
                            const bool stiff,
                            STensor43* elasticTangent) const
{
  if(elasticTangent!=NULL) Msg::Error("mlawNonLocalPorosity elasticTangent not computed");
  static STensor43 dFedF;
  static STensor3 dFeDtildefV;
  if (stiff and _tangentByPerturbation){
    predictorCorrector_NonLocalLogarithmPorosity(F0,F1,P,ipvprev,ipvcur,Tangent,dLocaldPorosityDStrain,dStressDNonLocalPorosity,dLocalPorosityDNonLocalPorosity,false,dFedF,dFeDtildefV);
    static STensor3 Fplus, Pplus;
    
    static IPNonLocalPorosity ipvcurPlus(*dynamic_cast<const IPNonLocalPorosity*>(ipvprev));

    // perturbe F
    ipvcurPlus.getRefToNonLocalLogarithmPorosity() = ipvcur->getNonLocalLogarithmPorosity();
    for (int i=0; i<3; i++){
      for (int j=0; j<3; j++){
        Fplus = (F1);
        Fplus(i,j) += _perturbationfactor;
        predictorCorrector_NonLocalLogarithmPorosity(F0,Fplus,Pplus,ipvprev,&ipvcurPlus,Tangent,dLocaldPorosityDStrain,dStressDNonLocalPorosity,dLocalPorosityDNonLocalPorosity,false,dFedF,dFeDtildefV);
        for (int k=0; k<3; k++){
          for (int l=0; l<3; l++){
            Tangent(k,l,i,j) = (Pplus(k,l) - P(k,l))/(_perturbationfactor);
          }
        }
        dLocaldPorosityDStrain(i,j) = (ipvcurPlus.getLocalLogarithmPorosity() - ipvcur->getLocalLogarithmPorosity())/_perturbationfactor;
				if (this->getMacroSolver()->withPathFollowing()){
					ipvcur->getRefToDIrreversibleEnergyDF()(i,j) = (ipvcurPlus.plasticEnergy() - ipvcur->plasticEnergy())/_perturbationfactor;
				}
      }
    }
    // perturb on nonlocal var
    ipvcurPlus.getRefToNonLocalLogarithmPorosity() = ipvcur->getNonLocalLogarithmPorosity()+_perturbationfactor*log(_fV_failure);
    predictorCorrector_NonLocalLogarithmPorosity(F0,F1,Pplus,ipvprev,&ipvcurPlus,Tangent,dLocaldPorosityDStrain,dStressDNonLocalPorosity,dLocalPorosityDNonLocalPorosity,false,dFedF,dFeDtildefV);
    for (int k=0; k<3; k++){
      for (int l=0; l<3; l++){
        dStressDNonLocalPorosity(k,l) = (Pplus(k,l) - P(k,l))/(_perturbationfactor*log(_fV_failure));
      }
    }
    dLocalPorosityDNonLocalPorosity = (ipvcurPlus.getLocalLogarithmPorosity() - ipvcur->getLocalLogarithmPorosity())/(_perturbationfactor*log(_fV_failure));
		if (this->getMacroSolver()->withPathFollowing()){
			ipvcur->getRefToDIrreversibleEnergyDNonLocalVariable(0) =  (ipvcurPlus.plasticEnergy() - ipvcur->plasticEnergy())/(_perturbationfactor*log(_fV_failure));
		}

  }
  else{
    //F1.print("F1");
    predictorCorrector_NonLocalLogarithmPorosity(F0,F1,P,ipvprev,ipvcur,Tangent,dLocaldPorosityDStrain,dStressDNonLocalPorosity,dLocalPorosityDNonLocalPorosity,stiff,dFedF,dFeDtildefV);
    //Tangent.print("K");
  }
};


void mlawNonLocalPorosity::nucleation(const double p1, const double p0, const double f0, IPNonLocalPorosity* q1, const IPNonLocalPorosity *q0) const{
  // Update nucleation state
  if (getNucleationLaw()!=NULL)
  {
    // Get IpNucl
    const IPNucleation& ipvnucprev = q0->getConstRefToIPNucleation();
    IPNucleation& ipvnuc = q1->getRefToIPNucleation();
    if (q1->dissipationIsBlocked())
    {
      getNucleationLaw()->previousBasedRefresh( q1->getRefToIPNucleation() , q0->getConstRefToIPNucleation() );
    }
    else
    {
      // Compute...
      if (_nucleationDuringCoalesIsON)
      {
        getNucleationLaw()->nucleate(p1,p0,*q1,*q0);
      }
      else
      {
        if (q1->getConstRefToIPCoalescence().getCoalescenceOnsetFlag()) 
        {
          getNucleationLaw()->previousBasedRefresh( q1->getRefToIPNucleation() , q0->getConstRefToIPNucleation() );
        }
        else
        {
          getNucleationLaw()->nucleate(p1,p0,*q1,*q0);
        }
      }
    }
  }
  else
  {
    Msg::Error("nucleation law does not exist");
  }
};

void mlawNonLocalPorosity::localPorosityGrowth(IPNonLocalPorosity *q1,
                      const double DeltaHatD, const double DeltaHatQ, const double DeltaHatP,
                      const STensor3& Kcor,
                      const IPNonLocalPorosity *q0, 
                      bool stiff,  double* DDeltafVDDeltaHatD, double* DDeltafVDDeltaHatQ, double* DDeltafVDDeltaHatP, STensor3* DDeltafVDKcor) const
{
  // Compute local porosity growth if damage is not blocked (otherwise : no more growth)
  if (q1->dissipationIsBlocked()){
    // No more growth as dissipation is blocked
    q1->getRefToLocalPorosity() = q0->getLocalPorosity();
    if (stiff){
      *DDeltafVDDeltaHatD = 0.;
      *DDeltafVDDeltaHatQ = 0.;
      *DDeltafVDDeltaHatP = 0.;
      STensorOperation::zero(*DDeltafVDKcor);
    }
  }
  else
  {
    // implicit approximation
    // fn+ DeltafV = (1-fn-DeltafV)*DeltaHatQ + DeltafVNucleation+ kw*(1-Xi^2)*(fn+DeltafV)*DeltaHatD
    // lead to DeltafV = u/v
    // with u = (1-fn)*DeltaHatQ + DeltafVNucleation + kw*w*fn*DeltaHatD
    // with v = 1+ DeltaHatQ - kw*(1-Xi^2)*DeltaHatD
    // with Xi = (27/2)*(J3/kcorEq^3), J3 = det(devKcor)
    
    // Compute growth terms...
    double fVn = q0->getLocalPorosity();
  
    // Get nucleation part
    const IPNucleation& ipvnucprev = q0->getConstRefToIPNucleation();
    const IPNucleation& ipvnuc = q1->getConstRefToIPNucleation();
    
    double DeltafVNucleation = ipvnuc.getNucleatedPorosityIncrement();
    
    double DDeltafVNucleationDhatP = ipvnuc.getDNucleatedPorosityDMatrixPlasticDeformation();
    
     // Compute growth and nucleation
    double u = (1- fVn)*DeltaHatQ + DeltafVNucleation;
    double v = 1.+DeltaHatQ;

    // Get shear part
    static STensor3 DkwDkcor;
    double kw = getShearFactor(Kcor,stiff,&DkwDkcor);
    if (DeltaHatD > 0.)
    {
      u += kw*fVn*DeltaHatD;
      v -= kw*DeltaHatD;
    }
    // get local porosity
    double DeltafV = u/v;
    
    /// Regularise porosity growth and rates
    /// local fV value has to be included between _localfVMin and _localfVFailure.
    double regularizedRate( 0.0 );
    double fV1_trial = fVn + DeltafV;
    
    if ( fV1_trial > _localfVFailure ) {
      /// If the value is too high (and DeltafV > 0)
      q1->getRefToLocalPorosity() = _localfVFailure;
      
      /// \fixme this condition is proably not necessary as long as the variable is not updated eelsewhere.
      if ( DeltafV  != 0.0 ) {
        regularizedRate = (_localfVFailure - fVn) / DeltafV;
      }
      else {
        regularizedRate = 0.0;
      }
    }
    else if (fV1_trial < _localfVMin) {
      /// If value is too low (and DeltafV < 0)
      q1->getRefToLocalPorosity() = _localfVMin;
      
      /// \fixme this condition is proably not necessary as long as the variable is not updated elsewhere.
      if ( DeltafV  != 0.0 ) {
        regularizedRate = (_localfVMin - fVn) / DeltafV;
      }
      else {
        regularizedRate = 0.0;
      }
    }
    else {
      /// Otherwise, nothing special.
      q1->getRefToLocalPorosity() = fV1_trial;
      regularizedRate = 1.0;
    }
    
    if (stiff){
      // Compute derivatives
      double DuDDeltaHatD = 0.;
      double DuDDeltaHatQ = (1- fVn);
      double DuDDeltaHatP = DDeltafVNucleationDhatP;

      double DvDDeltaHatD = 0.;
      double DvDDeltaHatQ = 1.;
      double DvDDeltaHatP = 0.;

      if (DeltaHatD >0.)
      {
        DuDDeltaHatD = kw*fVn;
        DvDDeltaHatD = -kw;
      }

      *DDeltafVDDeltaHatD = (DuDDeltaHatD/v - DeltafV*DvDDeltaHatD/v);
      *DDeltafVDDeltaHatQ = (DuDDeltaHatQ/v - DeltafV*DvDDeltaHatQ/v);
      *DDeltafVDDeltaHatP = (DuDDeltaHatP/v - DeltafV*DvDDeltaHatP/v);
      
      STensorOperation::zero(*DDeltafVDKcor);
      if (DeltaHatD > 0.)
      {
        double DuDkw = fVn*DeltaHatD;
        double DvDkw = -DeltaHatD;
        
        double DDeltafVDkw = DuDkw/v - DeltafV*DvDkw/v;
        DDeltafVDKcor->daxpy(DkwDkcor,DDeltafVDkw);
      }
      
      // add the regularisation derivatives
      *DDeltafVDDeltaHatD *= regularizedRate;
      *DDeltafVDDeltaHatQ *= regularizedRate;
      *DDeltafVDDeltaHatP *= regularizedRate;
      *DDeltafVDKcor *= regularizedRate;
    }
  }
};


double mlawNonLocalPorosity::getLocalVariable(const IPNonLocalPorosity* q, const int idex) const{
  if (_numNonLocalVar > 0){
    if (nonlocalMethod_ == NONLOCAL_POROSITY){
      if (idex == 0){
        return q->getLocalPorosity();
      }
      else{
        Msg::Error("the non-local variable %d is not defined",idex);
      }
    }
    else if (nonlocalMethod_ == NONLOCAL_LOG_POROSITY){
      if (idex == 0){
        return q->getLocalLogarithmPorosity();
      }
      else{
        Msg::Error("the non-local variable %d is not defined",idex);
      }
    }
    else if (nonlocalMethod_ == NONLOCAL_FULL_THREEVAR){
      if (idex == 0){
        return q->getLocalVolumetricPlasticStrain();
      }
      else if (idex == 1){
        return q->getLocalMatrixPlasticStrain();
      }
      else if (idex == 2){
        return q->getLocalDeviatoricPlasticStrain();
      }
      else{
        Msg::Error("the non-local variable %d is not defined",idex);
      }
    }
    else{
      Msg::Error("nonlocal method has not correctly defined");
    }

  }
  return 0.;
};
double& mlawNonLocalPorosity::getRefToLocalVariable(IPNonLocalPorosity* q, const int idex) const{
  if (_numNonLocalVar > 0){
    if (nonlocalMethod_ == NONLOCAL_POROSITY){
      if (idex == 0){
        return q->getRefToLocalPorosity();
      }
      else{
        Msg::Error("the non-local variable %d is not defined",idex);
      }
    }
    else if (nonlocalMethod_ == NONLOCAL_LOG_POROSITY){
      if (idex == 0){
        return q->getRefToLocalLogarithmPorosity();
      }
      else{
        Msg::Error("the non-local variable %d is not defined",idex);
      }
    }
    else if (nonlocalMethod_ == NONLOCAL_FULL_THREEVAR){
      if (idex == 0){
        return q->getRefToLocalVolumetricPlasticStrain();
      }
      else if (idex == 1){
        return q->getRefToLocalMatrixPlasticStrain();
      }
      else if (idex == 2){
        q->getRefToLocalDeviatoricPlasticStrain();
      }
      else{
        Msg::Error("the non-local variable %d is not defined",idex);
      }
    }
    else{
      Msg::Error("nonlocal method has not correctly defined");
    }
  }
  else{
    Msg::Error("reference to nonlocal var does not exist");
  }
  static double a(0.);
  return a;
};
double mlawNonLocalPorosity::getNonLocalVariable(const IPNonLocalPorosity* q, const int idex) const{
  if (_numNonLocalVar > 0){
    if (nonlocalMethod_ == NONLOCAL_POROSITY){
      if (idex == 0){
        return q->getNonLocalPorosity();
      }
      else{
        Msg::Error("the non-local variable %d is not defined",idex);
      }
    }
    else if (nonlocalMethod_ == NONLOCAL_LOG_POROSITY){
      if (idex == 0){
        return q->getNonLocalLogarithmPorosity();
      }
      else{
        Msg::Error("the non-local variable %d is not defined",idex);
      }
    }
    else if (nonlocalMethod_ == NONLOCAL_FULL_THREEVAR){
      if (idex == 0){
        return q->getNonLocalVolumetricPlasticStrain();
      }
      else if (idex == 1){
        return q->getNonLocalMatrixPlasticStrain();
      }
      else if (idex == 2){
        return q->getNonLocalDeviatoricPlasticStrain();
      }
      else{
        Msg::Error("the non-local variable %d is not defined",idex);
      }
    }
    else{
      Msg::Error("nonlocal method has not correctly defined");
    }
  }
  return 0.;
};
double& mlawNonLocalPorosity::getRefToNonLocalVariable(IPNonLocalPorosity* q, const int idex) const{
  if (_numNonLocalVar > 0){
    if (nonlocalMethod_ == NONLOCAL_POROSITY){
      if (idex == 0){
        return q->getRefToNonLocalPorosity();
      }
      else{
        Msg::Error("the non-local variable %d is not defined",idex);
      }
    }
    else if (nonlocalMethod_ == NONLOCAL_LOG_POROSITY){
      if (idex == 0){
        return q->getRefToNonLocalLogarithmPorosity();
      }
      else{
        Msg::Error("the non-local variable %d is not defined",idex);
      }
    }
    else if (nonlocalMethod_ == NONLOCAL_FULL_THREEVAR){
      if (idex == 0){
        return q->getRefToNonLocalVolumetricPlasticStrain();
      }
      else if (idex == 1){
        return q->getRefToNonLocalMatrixPlasticStrain();
      }
      else if (idex == 2){
        return q->getRefToNonLocalDeviatoricPlasticStrain();
      }
      else{
        Msg::Error("the non-local variable %d is not defined",idex);
      }
    }
    else{
      Msg::Error("nonlocal method has not correctly defined");
    }
  }
  Msg::Error("reference to nonlocal var does not exist");
  static double tmp(0.);
  return tmp;
};

void mlawNonLocalPorosity::constitutive_NonLocalPorosity(
                            const STensor3& F0,         // initial deformation gradient (input @ time n)
                            const STensor3& F1,         // updated deformation gradient (input @ time n+1)
                            STensor3 &P,                // updated 1st Piola-Kirchhoff stress tensor (output)
                            const IPNonLocalPorosity *ipvprev,       // array of initial internal variable
                            IPNonLocalPorosity *ipvcur,             // updated array of internal variable (in ipvcur on output),
                            STensor43 &Tangent,         // constitutive tangents (output)
                            STensor3  &dLocaldPorosityDStrain,
                            STensor3  &dStressDNonLocalPorosity,
                            double    &dLocalPorosityDNonLocalPorosity,
                            const bool stiff,            // if true compute the tangents
                            STensor43* elasticTangent                              ) const
{
  if(elasticTangent!=NULL) Msg::Error("mlawNonLocalPorosity elasticTangent not computed");
  static STensor43 dFedF;
  static STensor3 dFeDtildefV;
  if (stiff and _tangentByPerturbation){
    predictorCorrector_NonLocalPorosity(F0,F1,P,ipvprev,ipvcur,Tangent,dLocaldPorosityDStrain,dStressDNonLocalPorosity,dLocalPorosityDNonLocalPorosity,false,dFedF,dFeDtildefV);
    static STensor3 Fplus, Pplus;
    static IPNonLocalPorosity ipvcurPlus(*dynamic_cast<const IPNonLocalPorosity*>(ipvprev));

    // perturbe F
    ipvcurPlus.getRefToNonLocalPorosity() = ipvcur->getRefToNonLocalPorosity();
    for (int i=0; i<3; i++){
      for (int j=0; j<3; j++){
        Fplus = (F1);
        Fplus(i,j) += _perturbationfactor;
        predictorCorrector_NonLocalPorosity(F0,Fplus,Pplus,ipvprev,&ipvcurPlus,Tangent,dLocaldPorosityDStrain,dStressDNonLocalPorosity,dLocalPorosityDNonLocalPorosity,false,dFedF,dFeDtildefV);
        for (int k=0; k<3; k++){
          for (int l=0; l<3; l++){
            Tangent(k,l,i,j) = (Pplus(k,l) - P(k,l))/(_perturbationfactor);
          }
        }
        dLocaldPorosityDStrain(i,j) = (ipvcurPlus.getLocalPorosity() - ipvcur->getLocalPorosity())/_perturbationfactor;
				if (this->getMacroSolver()->withPathFollowing()){
					ipvcur->getRefToDIrreversibleEnergyDF()(i,j) = (ipvcurPlus.plasticEnergy() - ipvcur->plasticEnergy())/_perturbationfactor;
				}
      }
    }
    // perturb on nonlocal var
    ipvcurPlus.getRefToNonLocalPorosity() = ipvcur->getNonLocalPorosity()+_perturbationfactor*_fV_failure;
    predictorCorrector_NonLocalPorosity(F0,F1,Pplus,ipvprev,&ipvcurPlus,Tangent,dLocaldPorosityDStrain,dStressDNonLocalPorosity,dLocalPorosityDNonLocalPorosity,false,dFedF,dFeDtildefV);
    for (int k=0; k<3; k++){
      for (int l=0; l<3; l++){
        dStressDNonLocalPorosity(k,l) = (Pplus(k,l) - P(k,l))/(_perturbationfactor*_fV_failure);
      }
    }
    dLocalPorosityDNonLocalPorosity = (ipvcurPlus.getLocalPorosity() - ipvcur->getLocalPorosity())/(_perturbationfactor*_fV_failure);
		if (this->getMacroSolver()->withPathFollowing()){
			ipvcur->getRefToDIrreversibleEnergyDNonLocalVariable(0) =  (ipvcurPlus.plasticEnergy() - ipvcur->plasticEnergy())/(_perturbationfactor*_fV_failure);
		}

  }
  else{
    //F1.print("F1");
    predictorCorrector_NonLocalPorosity(F0,F1,P,ipvprev,ipvcur,Tangent,dLocaldPorosityDStrain,dStressDNonLocalPorosity,dLocalPorosityDNonLocalPorosity,stiff,dFedF,dFeDtildefV);
    //Tangent.print("K");
  }
};


void mlawNonLocalPorosity::constitutive(
                            const STensor3& F0,         // initial deformation gradient (input @ time n)
                            const STensor3& F1,         // updated deformation gradient (input @ time n+1)
                            STensor3 &P,                // updated 1st Piola-Kirchhoff stress tensor (output)
                            const IPVariable *_ipvprev,       // array of initial internal variable
                            IPVariable *_ipvcur,             // updated array of internal variable (in ipvcur on output),
                            STensor43 &Tangent,         // constitutive tangents (output)
                            const bool stiff ,
                            STensor43* elasticTangent, 
                            const bool dTangentdeps,
                            STensor63* dCalgdeps) const
{
  if(elasticTangent!=NULL) Msg::Error("mlawNonLocalPorosity elasticTangent not computed");
  const IPNonLocalPorosity *ipvprev = dynamic_cast<const IPNonLocalPorosity *> (_ipvprev);       // array of initial internal variable
  IPNonLocalPorosity *ipvcur = dynamic_cast<IPNonLocalPorosity *> (_ipvcur);
 
  if (stiff and _tangentByPerturbation){
    predictorCorrectorLocal(F0,F1,P,ipvprev,ipvcur,Tangent,false);
    static STensor3 Fplus, Pplus;
    static IPNonLocalPorosity ipvcurPlus(*dynamic_cast<const IPNonLocalPorosity*>(ipvprev));
    
    ipvcurPlus.operator =(*dynamic_cast<const IPVariable*>(ipvcur));
    // perturbe F
    for (int i=0; i<3; i++){
      for (int j=0; j<3; j++){
        Fplus = (F1);
        Fplus(i,j) += _perturbationfactor;
        predictorCorrectorLocal(F0,Fplus,Pplus,ipvprev,&ipvcurPlus,Tangent,false);
        for (int k=0; k<3; k++){
          for (int l=0; l<3; l++){
            Tangent(k,l,i,j) = (Pplus(k,l) - P(k,l))/(_perturbationfactor);
          }
        }
				if (this->getMacroSolver()->withPathFollowing()){
					ipvcur->getRefToDIrreversibleEnergyDF()(i,j) = (ipvcurPlus.plasticEnergy() - ipvcur->plasticEnergy())/_perturbationfactor;
				}
      }
    }
  }
  else{
    predictorCorrectorLocal(F0,F1,P,ipvprev,ipvcur,Tangent,stiff);
  }
};



bool mlawNonLocalPorosity::plasticCorrector_multipleNonLocalVariables(const STensor3& F1, 
                                  const double& yieldfV,const std::vector<double>& DyieldfVDnonlocalVar, const STensor3& DyieldfVDKcorpr,
                                  const STensor3& Kcorpr, const STensor3& devKcorpr,  const double& pcorpr,
                                  const IPNonLocalPorosity *q0, IPNonLocalPorosity *q1,
                                  STensor3& Fe, STensor3& Ce, STensor3& Ee, STensor43& Le, STensor63& dLe,
                                  const bool stiff,
                                  const STensor43& DKcorprDEpr, const STensor43& DdevKcorprDEpr, const STensor3& DpcorprDEpr, // predictor elastic tangent
                                  STensor43& DKcorDEpr, std::vector<STensor3>& DKcorDnonlocalVar,
                                  STensor43& DFpDEpr, std::vector<STensor3>& DFpDnonlocalVar,
                                  std::vector<STensor3>& DlocalVarDEpr, fullMatrix<double>& DlocalVarDnonlocalVar,
                                  const double* T,
                                  const STensor3* DKcorprDT, const STensor3* DdevKcorprDT, const double* DpcorprDT,
                                  STensor3* DKcorDT, STensor3* DFpDT, fullMatrix<double>* DlocalVarDT
                                ) const{
  
  // unknowns
  double DeltaHatD = 0.;
  double DeltaHatQ = 0.;
  double DeltaHatP = 0.;


  // First residual
  static SVector3 res;
  static SVector3 sol;
  static STensor3 J, invJ;

  double & yield = q1->getRefToCurrentViscoplasticYieldStress();
  yield = q1->getConstRefToIPJ2IsotropicHardening().getR();
  double H = q1->getConstRefToIPJ2IsotropicHardening().getDR();
  
  double kcorEqpr = sqrt(1.5*STensorOperation::doubledot(devKcorpr,devKcorpr));
  computeResidual_multipleNonLocalVariables(res,J,F1,kcorEqpr,pcorpr,yield,H,yieldfV,q0,q1,
                  DeltaHatD,DeltaHatQ,DeltaHatP,T);

  int ite = 0;
  double f = res.norm();
  // Start iterations
  //Msg::Info("plastic corrector");
  while(f > _tol or ite <1)
  {
    // Jacobian
  	STensorOperation::inverseSTensor3(J,invJ);
  	STensorOperation::multSTensor3SVector3(invJ,res,sol);
    if (DeltaHatD -sol(0) < 0){
			DeltaHatD *= 0.1;
		}
		else{
    	DeltaHatD -= sol(0);
		}

    if (-sol(1) > 0.5){
			DeltaHatQ += 0.5;
		}
		else if (-sol(1) < -0.5){
			DeltaHatQ -= 0.5;
		}
		else
			DeltaHatQ -= sol(1);


		if (DeltaHatP -sol(2) <0){
			DeltaHatP *= 0.1;
		}
		else
	    DeltaHatP -= sol(2);

    updateInternalVariables(q1,q0,DeltaHatD,DeltaHatQ,DeltaHatP,true,T);
    yield = q1->getConstRefToIPJ2IsotropicHardening().getR();
    H = q1->getConstRefToIPJ2IsotropicHardening().getDR();
    // Add strain rate effects
    if (_viscoplastlaw != NULL)
    {
      double R, dR;
      double delta_t = getTimeStep();
      _viscoplastlaw->get(DeltaHatP/delta_t,R,dR);
      yield += R;
      H += dR/delta_t;
    }

    computeResidual_multipleNonLocalVariables(res,J,F1,kcorEqpr,pcorpr,yield,H,yieldfV,q0,q1,
                  DeltaHatD,DeltaHatQ,DeltaHatP,T);

    f = res.norm();
    //printf("ite = %d maxite = %d norm = %e ,DeltaHatD = %e,DeltaHatQ = %e,DeltaHatP = %e,DeltafV =%e !! \n",ite,_maxite,f,DeltaHatD,DeltaHatQ,DeltaHatP,DeltafV);
    if (f < _tol)  break;

    ite++;
    if((ite > _maxite) or STensorOperation::isnan(f))
    {
      #ifdef _DEBUG
      printf("No convergence for plastic correction  with ite = %d maxite = %d norm = %e ,DeltaHatD = %e,DeltaHatQ = %e,DeltaHatP = %e!!\n",ite,_maxite,f,DeltaHatD,DeltaHatQ,DeltaHatP);
      #endif //_DEBUG
      return false;
    }
  }

  static STensor3 DGNp, devDGNp;
  double trDGNp;
  static STensor43 ENp;

  // update internal value first
  updateInternalVariables(q1,q0,DeltaHatD,DeltaHatQ,DeltaHatP,true,T);

  updatePlasticState(q1,q0,F1,DeltaHatD,DeltaHatQ,DeltaHatP,devKcorpr,kcorEqpr,pcorpr,
                     yield,Fe,Ce,Ee,Le,dLe,DGNp,devDGNp,trDGNp,ENp,T);
  
  double DplasticEnergyDHatP = (1-q0->getInitialPorosity())*(yield +H*DeltaHatP);
  q1->getRefToDPlasticEnergyDMatrixPlasticStrain() = DplasticEnergyDHatP;
    
  /* Stiffness computation*/
  if(stiff)
  {
    double dRdT= 0.;
    if (isThermomechanicallyCoupled()){
      dRdT = q1->getConstRefToIPJ2IsotropicHardening().getDRDT();
    }
    /* Compute internal variables derivatives from residual derivatives */
    static STensor3 dres0dEpr, dres1dEpr, dres2dEpr;
    static std::vector<double> Dres0DNonLocalVar(_numNonLocalVar,0.), Dres1DNonLocalVar(_numNonLocalVar,0.), Dres2DNonLocalVar(_numNonLocalVar,0.);
    
    double dres0dT, dres1dT,dres2dT;

    computeDResidual_multipleNonLocalVariables(dres0dEpr,dres1dEpr,dres2dEpr,F1, 
                  Dres0DNonLocalVar,Dres1DNonLocalVar,Dres2DNonLocalVar,
                  devKcorpr,kcorEqpr,pcorpr,yield,
                  yieldfV,DyieldfVDKcorpr,DyieldfVDnonlocalVar,
                  DKcorprDEpr,DdevKcorprDEpr,DpcorprDEpr,
                  q0,q1,
                  DeltaHatD,DeltaHatQ,DeltaHatP,
                  T,&dRdT,DdevKcorprDT,DpcorprDT,DKcorprDT,
                  &dres0dT,&dres1dT,&dres2dT);

    static STensor3 dDeltaHatDdEpr, dDeltaHatQdEpr, dDeltaHatPdEpr;

    STensorOperation::inverseSTensor3(J,invJ); // inverse last tengent operator

    for(int i=0; i<3; i++)
    {
      for(int j=0; j<3; j++)
      {
        res(0) = -dres0dEpr(i,j);
        res(1) = -dres1dEpr(i,j);
        res(2) = -dres2dEpr(i,j);
        STensorOperation::multSTensor3SVector3(invJ,res,sol);
        dDeltaHatDdEpr(i,j) = sol(0);
        dDeltaHatQdEpr(i,j) = sol(1);
        dDeltaHatPdEpr(i,j) = sol(2);
      }
    }
    // local variables
    DlocalVarDEpr[0] = dDeltaHatQdEpr;
    DlocalVarDEpr[1] = dDeltaHatPdEpr;
    DlocalVarDEpr[2] = dDeltaHatDdEpr;

    static std::vector<double> dDeltaHatDDNonLocalVar(_numNonLocalVar,0.), dDeltaHatQDNonLocalVar(_numNonLocalVar,0.), dDeltaHatPDNonLocalVar(_numNonLocalVar,0.);

    for (int i=0; i< _numNonLocalVar; i++){
      // dtilde_fv
      res(0) = -Dres0DNonLocalVar[i];
      res(1) = -Dres1DNonLocalVar[i];
      res(2) = -Dres2DNonLocalVar[i];
      STensorOperation::multSTensor3SVector3(invJ,res,sol);
      dDeltaHatDDNonLocalVar[i] = sol(0);
      dDeltaHatQDNonLocalVar[i] = sol(1);
      dDeltaHatPDNonLocalVar[i] = sol(2);

      DlocalVarDnonlocalVar(0,i) = dDeltaHatQDNonLocalVar[i];
      DlocalVarDnonlocalVar(1,i) = dDeltaHatPDNonLocalVar[i];
      DlocalVarDnonlocalVar(2,i) = dDeltaHatDDNonLocalVar[i];
    }

    double dDeltaHatDdT = 0.;
    double dDeltaHatQdT = 0.;
    double dDeltaHatPdT = 0.;

    if(isThermomechanicallyCoupled()){
      res(0) = -dres0dT;
      res(1) = -dres1dT;
      res(2) = -dres2dT;
      STensorOperation::multSTensor3SVector3(invJ,res,sol);
      dDeltaHatDdT = sol(0);
      dDeltaHatQdT = sol(1);
      dDeltaHatPdT = sol(2);

      (*DlocalVarDT)(0,0) = dDeltaHatQdT;
      (*DlocalVarDT)(1,0) = dDeltaHatPdT;
      (*DlocalVarDT)(2,0) = dDeltaHatDdT;
    }


    computePlasticTangentNonLocal_multipleNonLocalVariables(q1,q0,devKcorpr,kcorEqpr,pcorpr,DGNp,devDGNp,trDGNp,ENp,
        DeltaHatD,DeltaHatQ,DeltaHatP,
        DKcorprDEpr,DdevKcorprDEpr,DpcorprDEpr,
        dDeltaHatDdEpr,dDeltaHatQdEpr,dDeltaHatPdEpr,
        DKcorDEpr,DFpDEpr,
        dDeltaHatDDNonLocalVar,dDeltaHatQDNonLocalVar,dDeltaHatPDNonLocalVar,
        DKcorDnonlocalVar,DFpDnonlocalVar,
        T,DKcorprDT,DdevKcorprDT,DpcorprDT,
        &dDeltaHatDdT,&dDeltaHatQdT,&dDeltaHatPdT,
        DKcorDT,DFpDT);


    STensor3& DplasticEnergyDF = q1->getRefToDPlasticEnergyDF();
    DplasticEnergyDF = dDeltaHatPdEpr;
    DplasticEnergyDF *= DplasticEnergyDHatP;
    for (int nl=0; nl < _numNonLocalVar; nl++){
      q1->getRefToDPlasticEnergyDNonLocalVariable(nl) = DplasticEnergyDHatP*dDeltaHatPDNonLocalVar[nl];
      if(nl==1)
      {
        double DplasticEnergyWp = (1-q0->getInitialPorosity())*(-1.*DeltaHatP);
        double dwp=q1->getConstRefToIPJ2IsotropicHardening().getDWp();
        q1->getRefToDPlasticEnergyDNonLocalVariable(nl)+=DplasticEnergyWp*dwp;
      }
    };

    if(isThermomechanicallyCoupled()){
      double& DplasticEnergyDT = q1->getRefToDPlasticEnergyDT();
      DplasticEnergyDT =  DplasticEnergyDHatP*dDeltaHatPdT + (1-q0->getInitialPorosity())*dRdT*DeltaHatP;
    }
  };
  return true;
};

void mlawNonLocalPorosity::computeResidual_multipleNonLocalVariables(SVector3& res, STensor3& J, const STensor3& F,
                const double kcorEqpr, const double pcorpr, const double R, const double H,
                const double yieldfV, 
                const IPNonLocalPorosity *q0, const IPNonLocalPorosity* q1,
                const double DeltaHatD, const double DeltaHatQ, const double DeltaHatP, // 3 unknonwn
                const double* T
              ) const{
  // note that yieldfV Chi, W, are nonlocal
  const IPVoidState* voidState = &(q1->getConstRefToIPVoidState());
  const IPVoidState* voidStatePrev = &(q0->getConstRefToIPVoidState());
  
  double Chi  = voidState->getVoidLigamentRatio();
  double W = voidState->getVoidAspectRatio();

  double f0 = q1->getInitialPorosity();
  double R0 = _j2IH->getYield0(); // reference value

  double KT= _K;
  double muT = _mu;
  if (isThermomechanicallyCoupled()){
    KT = bulkModulus(*T);
    muT =shearModulus(*T);
  };

  // corrected value
  double kcorEq =  kcorEqpr - 3.*muT*DeltaHatD;
  double pcor = pcorpr - KT*DeltaHatQ;
  double detF = 1.;
  if (_stressFormulation == CORO_CAUCHY)
  {
    detF = STensorOperation::determinantSTensor3(F);
    kcorEq /= detF;
    pcor /= detF;
  }
  
  // grad f, Ns, Nf with respect to [kcorEq pcor R fV Chi W]
  static fullVector<double> gradf(6), gradNs(6), gradNv(6);

  // yield
  res(0) = yieldFunction(kcorEq,pcor,R,yieldfV,q0,q1,T,true,&gradf);

  double Ns, Nv; // plastic flow components
  plasticFlow(Ns,Nv,kcorEq,pcor,R,q0,q1,T,true,&gradNs,&gradNv);
  // normality
  res(1) = R0*(DeltaHatD*Nv- DeltaHatQ*Ns);
  
  double Jp = 1.;
  if (_stressFormulation == CORO_KIRCHHOFF)
  {
    double Jp0 = STensorOperation::determinantSTensor3(q0->getConstRefToFp());
    Jp = Jp0*exp(DeltaHatQ);
  }
  // plastic energy balance
  res(2) = ((1.-yieldfV)*Jp*R*DeltaHatP - kcorEq*DeltaHatD - pcor*DeltaHatQ)/R0;

  double DkcorEqDDeltaHatD = -3.*muT/detF;
  double DpcorDDeltaHatQ = -KT/detF;

  // Dres0DDeltaHatD
  J(0,0) = gradf(0)*DkcorEqDDeltaHatD;
  // Dres0DDeltaHatQ
  J(0,1) = gradf(1)*DpcorDDeltaHatQ;
  // Dres0DDeltaHatP
  J(0,2) = gradf(2)*H;

  // Dres1DDeltaHatD
  J(1,0) = R0*(Nv + DeltaHatD*gradNv(0)*DkcorEqDDeltaHatD - DeltaHatQ*gradNs(0)*DkcorEqDDeltaHatD);
  // Dres1DDeltaHatQ
  J(1,1) = R0*(DeltaHatD*gradNv(1)*DpcorDDeltaHatQ- Ns - DeltaHatQ*gradNs(1)*DpcorDDeltaHatQ);
  // Dres1DDeltaHatP
  J(1,2) = R0*(DeltaHatD*gradNv(2) - DeltaHatQ*gradNs(2))*H;

  // Dres2DDeltaHatd
  J(2,0) = (-kcorEq - DkcorEqDDeltaHatD*DeltaHatD)/R0;
  // Dres2DDeltaHatq
  J(2,1) = (-pcor - DpcorDDeltaHatQ*DeltaHatQ)/R0;
  if (_stressFormulation == CORO_KIRCHHOFF)
  {
    double DJpDDeltaHatQ = Jp;
    J(2,1) += ((1.-yieldfV)*DJpDDeltaHatQ*R*DeltaHatP)/R0;
  }
  // Dres2DDeltaHatp
  J(2,2) = (1.-yieldfV)*Jp*(R + H*DeltaHatP)/R0;
};


void mlawNonLocalPorosity::computeDResidual_multipleNonLocalVariables(STensor3 &dres0dEpr, STensor3 &dres1dEpr, STensor3 &dres2dEpr, const STensor3& F,
                                  std::vector<double> &dres0dNonlocalVars, std::vector<double> &dres1dNonlocalVars, std::vector<double> &dres2dNonlocalVars,
                                  const STensor3& devKcorpr, const double kcorEqpr, const double pcorpr, const double R,
                                  const double yieldfV, const STensor3& DyieldfVDkcorpr, const std::vector<double>& DyieldfVDNonlocalVars,
                                  const STensor43& DkcorprDEpr, const STensor43& DdevKcorprDEpr, const STensor3& DpcorprDEpr,
                                  const IPNonLocalPorosity *q0, const IPNonLocalPorosity* q1,
                                  const double DeltaHatD, const double DeltaHatQ, const double DeltaHatP,
                                  const double* T, const double* DRDT,
                                  const STensor3* DdevKcorprDT , const double* DpcorprDT, const STensor3* DKcorprVDT,
                                  double *dres0dT, double *dres1dT, double *dres2dT
                                  ) const{
  // note that yieldfV Chi, W, are nonlocal
  const IPVoidState* voidState = &q1->getConstRefToIPVoidState();
  
  double Chi  = voidState->getVoidLigamentRatio();
  double DChiDyieldfV  = voidState->getDVoidLigamentRatioDYieldPorosity();

  double W  = voidState->getVoidAspectRatio();
  double DWDyieldfV  = voidState->getDVoidAspectRatioDYieldPorosity();

  double gamma = voidState->getVoidShapeFactor();
  double DgammaDyieldfV = voidState->getDVoidShapeFactorDYieldPorosity();

  double f0 = q1->getInitialPorosity();
  double R0 = _j2IH->getYield0(); // reference value

  double KT = _K;
  double muT = _mu;
  if (isThermomechanicallyCoupled()){
    KT=bulkModulus(*T);
    muT=shearModulus(*T);
  }
  
  static STensor3 DyieldfVDEpr;
  STensorOperation::multSTensor3STensor43(DyieldfVDkcorpr,DkcorprDEpr,DyieldfVDEpr);

  static STensor3 DkcorEqprDEpr;
  STensorOperation::multSTensor3STensor43(devKcorpr,DdevKcorprDEpr,DkcorEqprDEpr);
  DkcorEqprDEpr *= (1.5/kcorEqpr);

  // corrected values
  double kcorEq =  kcorEqpr - 3.*muT*DeltaHatD;
  double pcor = pcorpr - KT*DeltaHatQ;

  static STensor3 DkcorEqDEpr, DpcorDEpr;
  DkcorEqDEpr = DkcorEqprDEpr;
  DpcorDEpr = DpcorprDEpr;
  double detF = 1.;
  if (_stressFormulation == CORO_CAUCHY)
  {
    detF = STensorOperation::determinantSTensor3(F);
    kcorEq /= detF;
    pcor /= detF;  
    
    DkcorEqDEpr *= (1./detF);
    DpcorDEpr *= (1./detF);
    
    static STensor3 DdetFDEepr;
    STensorOperation::diag(DdetFDEepr,detF);
    DkcorEqDEpr.daxpy(DdetFDEepr,-kcorEq/detF);
    DpcorDEpr.daxpy(DdetFDEepr,-pcor/detF);
  }
  
  // grad f, Ns, Nf with respect to [kcorEq pcor R fV Chi W]
  static fullVector<double> gradf(6), gradNs(6), gradNv(6);
  double DFDT, DNsDT, DNvDT;
  double f, Ns, Nv;
  f = yieldFunction(kcorEq,pcor,R,yieldfV,q0,q1,T,true,&gradf,isThermomechanicallyCoupled(),&DFDT);
  plasticFlow(Ns,Nv,kcorEq,pcor,R,q0,q1,T,true,&gradNs,&gradNv,isThermomechanicallyCoupled(),&DNsDT,&DNvDT);

  // compute Dres0DEpr
  double Dres0DkcorEq = gradf(0);
  double Dres0Dpcor = gradf(1);
  double Dres0DyieldfV = gradf(3)+gradf(4)*DChiDyieldfV+gradf(5)*DWDyieldfV;
  dres0dEpr = DkcorEqDEpr;
  dres0dEpr*= Dres0DkcorEq;
  dres0dEpr.daxpy(DpcorDEpr,Dres0Dpcor);
  dres0dEpr.daxpy(DyieldfVDEpr,Dres0DyieldfV);

  // compute Dres1DEpr
  double Dres1DkcorEq = R0*(DeltaHatD*gradNv(0)- DeltaHatQ*gradNs(0));
  double Dres1Dpcor = R0*(DeltaHatD*gradNv(1)- DeltaHatQ*gradNs(1));
  double Dres1DyieldfV = R0*(DeltaHatD*gradNv(3)- DeltaHatQ*gradNs(3)) +
                         R0*(DeltaHatD*gradNv(4)- DeltaHatQ*gradNs(4))*DChiDyieldfV+
                         R0*(DeltaHatD*gradNv(5)- DeltaHatQ*gradNs(5))*DWDyieldfV;

  dres1dEpr = DkcorEqDEpr;
  dres1dEpr *= Dres1DkcorEq;
  dres1dEpr.daxpy(DpcorDEpr,Dres1Dpcor);
  dres1dEpr.daxpy(DyieldfVDEpr,Dres1DyieldfV);

  // compute Dres2DEpr
  double Jp = 1.;
  if (_stressFormulation == CORO_KIRCHHOFF)
  {
    double Jp0 = STensorOperation::determinantSTensor3(q0->getConstRefToFp());
    Jp = Jp0*exp(DeltaHatQ);
  }  
  // compute Dres2DEpr
  double Dres2DkcorEq = (-DeltaHatD)/R0;
  double Dres2Dpcor = (-DeltaHatQ)/R0;
  double Dres2DyieldfV = (-Jp*R*DeltaHatP)/R0;
  
  
  dres2dEpr = DkcorEqDEpr;
  dres2dEpr *= Dres2DkcorEq;
  dres2dEpr.daxpy(DpcorDEpr,Dres2Dpcor);
  dres2dEpr.daxpy(DyieldfVDEpr,Dres2DyieldfV);

  static std::vector<double> DChiDNonlocalVars(3,0.), DWDNonlocalVars(3,0.);

  double DChiDDeltaHatDNonLocal = voidState->getDVoidLigamentRatioDDeviatoricPlasticDeformation();
  double DChiDDeltaHatQNonLocal = voidState->getDVoidLigamentRatioDVolumetricPlasticDeformation();
  double DChiDDeltaHatPNonLocal = voidState->getDVoidLigamentRatioDMatrixPlasticDeformation();

  double DWDDeltaHatDNonLocal = voidState->getDVoidAspectRatioDDeviatoricPlasticDeformation();
  double DWDDeltaHatQNonLocal = voidState->getDVoidAspectRatioDVolumetricPlasticDeformation();
  double DWDDeltaHatPNonLocal = voidState->getDVoidAspectRatioDMatrixPlasticDeformation();

  DChiDNonlocalVars[0] = DChiDDeltaHatQNonLocal;
  DChiDNonlocalVars[1] = DChiDDeltaHatPNonLocal;
  DChiDNonlocalVars[2] = DChiDDeltaHatDNonLocal;

  DWDNonlocalVars[0] = DWDDeltaHatQNonLocal;
  DWDNonlocalVars[1] = DWDDeltaHatPNonLocal;
  DWDNonlocalVars[2] = DWDDeltaHatDNonLocal;

  for (int i=0; i< _numNonLocalVar; i++){
    // compute nonlocal derivative
    dres0dNonlocalVars[i] = Dres0DyieldfV*DyieldfVDNonlocalVars[i]+ gradf(4)*DChiDNonlocalVars[i]+gradf(5)*DWDNonlocalVars[i];
    dres1dNonlocalVars[i] = Dres1DyieldfV*DyieldfVDNonlocalVars[i]+ 
                    R0*(DeltaHatD*gradNv(4)- DeltaHatQ*gradNs(4))*DChiDNonlocalVars[i] +
                    R0*(DeltaHatD*gradNv(5)- DeltaHatQ*gradNs(5))*DWDNonlocalVars[i];
    dres2dNonlocalVars[i] = Dres2DyieldfV*DyieldfVDNonlocalVars[i];
  };
  //HERE contribution from wp FOR NONLVAR 1
  double dwp=q1->getConstRefToIPJ2IsotropicHardening().getDWp();
  dres0dNonlocalVars[1] -= gradf(2)*dwp;
  dres1dNonlocalVars[1] -= (R0*(DeltaHatD*gradNv(2) - DeltaHatQ*gradNs(2))*dwp);
  dres2dNonlocalVars[1] -= (1.-yieldfV)*Jp*dwp*DeltaHatP/R0;
           
  if (isThermomechanicallyCoupled()){
    double dKdT=bulkModulusDerivative(*T);
    double dmudT=shearModulusDerivative(*T);

    static double DkcorEqDT, DpcorDT, DyieldfVDT;
    DkcorEqDT = STensorOperation::doubledot(devKcorpr,*DdevKcorprDT)*1.5/kcorEqpr - 3.*dmudT*DeltaHatD;
    DpcorDT = *DpcorprDT -dKdT* DeltaHatQ;
    if (_stressFormulation == CORO_CAUCHY)
    {
      DkcorEqDT /= detF;
      DpcorDT /= detF;
    }
    
    DyieldfVDT = STensorOperation::doubledot(DyieldfVDkcorpr,*DdevKcorprDT);

    double partDres0DT = gradf(2)*(*DRDT) + DFDT;  // yield temperature-dependent
    *dres0dT = Dres0DkcorEq*DkcorEqDT + Dres0Dpcor*DpcorDT + Dres0DyieldfV*DyieldfVDT + partDres0DT;

    double partDres1DT = R0*(DeltaHatD*gradNv(2)- DeltaHatQ*gradNs(2))*(*DRDT) + R0*(DeltaHatD*DNvDT- DeltaHatQ*DNsDT);
    *dres1dT = Dres1DkcorEq*DkcorEqDT + Dres1Dpcor*DpcorDT + Dres1DyieldfV*DyieldfVDT + partDres1DT;

    double partDres2DT = ((1-f0)*(*DRDT)*DeltaHatP)/R0;
    *dres2dT = Dres2DkcorEq*DkcorEqDT + Dres2Dpcor*DpcorDT+ partDres2DT;
  
  }
};

void mlawNonLocalPorosity::voidCharacteristicsEvolution_multipleNonLocalVariables(const STensor3& kcor,
                                                const IPNonLocalPorosity *q0, IPNonLocalPorosity* q1,
                                                std::vector<double>& DyieldfVDNonlocalVars,
                                                STensor3& DyieldfVDKcor, const bool blockVoidGrowth
                                                ) const
{
  if (blockVoidGrowth)
  {
    // purely plastic problem without void growth
    q1->getRefToLocalPorosity() = q0->getLocalPorosity();
    q1->getRefToNonLocalPorosity() = q0->getNonLocalPorosity();
    q1->getRefToCorrectedPorosity() = q0->getCorrectedPorosity();
    q1->getRefToLocalLogarithmPorosity() = q0->getLocalLogarithmPorosity();
    q1->getRefToYieldPorosity() = q0->getYieldPorosity();
    // 
    for (int i=0; i< _numNonLocalVar; i++)
    {
      DyieldfVDNonlocalVars[i] = 0.;
    }
    STensorOperation::zero(DyieldfVDKcor);
  }
  else
  {
    // update internal data with nonlocal variable
    // nonlocal increment
    double DeltaHatQNonLocal = q1->getNonLocalVolumetricPlasticStrain() - q0->getNonLocalVolumetricPlasticStrain();
    double DeltaHatPNonLocal = q1->getNonLocalMatrixPlasticStrain() - q0->getNonLocalMatrixPlasticStrain();
    double DeltaHatDNonLocal = q1->getNonLocalDeviatoricPlasticStrain() - q0->getNonLocalDeviatoricPlasticStrain();
    
    // 
    double& fV = q1->getRefToLocalPorosity();
    double& fVtilde = q1->getRefToNonLocalPorosity();
    double& fVtildeStar = q1->getRefToCorrectedPorosity();
    double& yieldfV = q1->getRefToYieldPorosity();
    
    if(getVoidEvolutionLaw()->useYieldPorosityDuringCoalescence())
    {
      // note the computation order
      // nucleation--> porosities (local, nonlocal, yield ...) -> void state -> coalescence
     
   // Update nucleation state first
      nucleation(q1->getNonLocalMatrixPlasticStrain(),q0->getNonLocalMatrixPlasticStrain(),q0->getYieldPorosity(),q1,q0);
      // note that nonlocal var = [hatQ hatP hatD]
      // void growth --> yield porosity = local porosity
      localPorosityGrowth(q1,DeltaHatDNonLocal,DeltaHatQNonLocal,DeltaHatPNonLocal,
                          kcor,q0,true,
                          &DyieldfVDNonlocalVars[2],&DyieldfVDNonlocalVars[0],&DyieldfVDNonlocalVars[1],&DyieldfVDKcor);
      // nonlocal
      fVtilde = fV;
      // corrected porosity
      double DfVtildeStarDtildefV = q1->getConstRefToIPCoalescence().getAccelerateRate(); // current rate 
      fVtildeStar = q0->getCorrectedPorosity() + DfVtildeStarDtildefV*(q1->getNonLocalPorosity() - q0->getNonLocalPorosity());
      // yield
      yieldfV = _regularisationFunc_porosityEff->getVal(fVtildeStar);
      
      // derivatives of yield porosity
      double DyieldfVDDtildefV = _regularisationFunc_porosityEff->getDiff(fVtildeStar)*DfVtildeStarDtildefV;
      for (int i=0; i< 3; i++)
      {
        DyieldfVDNonlocalVars[i] *= DyieldfVDDtildefV;
      }
      DyieldfVDKcor *= (DyieldfVDDtildefV);
      
      // update void state
      voidStateGrowth(q0->getYieldPorosity(),q1->getYieldPorosity(),DeltaHatDNonLocal,DeltaHatQNonLocal,DeltaHatPNonLocal,q0,q1);
    }
    else
    {
      // void characteristics are evaluated first, then porosity are evaluated
      // other void characteristic evolutions are called first
      // then void evolution is estimated
      // update void state
      voidStateGrowth(q0->getYieldPorosity(),q1->getYieldPorosity(),DeltaHatDNonLocal,DeltaHatQNonLocal,DeltaHatPNonLocal,q0,q1);

      // local porosity
      const IPVoidState* ipvoid = &(q1->getConstRefToIPVoidState());

      double lambda = ipvoid->getVoidSpacingRatio();
      double W = ipvoid->getVoidAspectRatio();
      double Chi = ipvoid->getVoidLigamentRatio();
      double gamma = ipvoid->getVoidShapeFactor();

      fV = Chi*Chi*Chi*W/(3.*gamma*lambda); // always corrects
      // Regularise porosity growth and rates
      double regularizedRate = 1.;
      if (fV > _localfVFailure)
      {
        double fVn = q0->getLocalPorosity();
        double DeltafV = fV - fVn;
        fV = _localfVFailure;
        regularizedRate = 0.;
        if (fabs(DeltafV)> 0)
        {
          regularizedRate = (_localfVFailure - fVn)/DeltafV;
        }
      }
      // update void
      fVtilde = fV;
      // update yield porosity as it depends on nonlocal variable
      double DfVtildeStarDtildefV = 1.;
      fVtildeStar = q0->getCorrectedPorosity() + DfVtildeStarDtildefV*(q1->getLocalPorosity() - q0->getLocalPorosity());
      yieldfV = _regularisationFunc_porosityEff->getVal(fVtildeStar);
      double DyieldfVDDfV= _regularisationFunc_porosityEff->getDiff(fVtildeStar)*DfVtildeStarDtildefV;

      STensorOperation::zero(DyieldfVDKcor);
      // nonlocal var = [volumetric, matrix, deviatoric]
      double DfVDChi = 3.*fV*regularizedRate/Chi;
      double DfVDlambda = -fV*regularizedRate/lambda;
      double DfVDW = fV*regularizedRate/W;
      double DfVDgamma = -fV*regularizedRate/gamma;

      DyieldfVDNonlocalVars[0] =  DfVDChi*ipvoid->getDVoidLigamentRatioDVolumetricPlasticDeformation() +
                                     DfVDW*ipvoid->getDVoidAspectRatioDVolumetricPlasticDeformation()+
                                     DfVDgamma*ipvoid->getDVoidShapeFactorDVolumetricPlasticDeformation()+
                                     DfVDlambda*ipvoid->getDVoidSpacingRatioDVolumetricPlasticDeformation();
      DyieldfVDNonlocalVars[1] =  DfVDChi*ipvoid->getDVoidLigamentRatioDMatrixPlasticDeformation() +
                                     DfVDW*ipvoid->getDVoidAspectRatioDMatrixPlasticDeformation()+
                                     DfVDgamma*ipvoid->getDVoidShapeFactorDMatrixPlasticDeformation()+
                                     DfVDlambda*ipvoid->getDVoidSpacingRatioDMatrixPlasticDeformation();
      DyieldfVDNonlocalVars[2] =  DfVDChi*ipvoid->getDVoidLigamentRatioDDeviatoricPlasticDeformation() +
                                     DfVDW*ipvoid->getDVoidAspectRatioDDeviatoricPlasticDeformation()+
                                     DfVDgamma*ipvoid->getDVoidShapeFactorDDeviatoricPlasticDeformation()+
                                     DfVDlambda*ipvoid->getDVoidSpacingRatioDDeviatoricPlasticDeformation();
    }
  }                                       
};

void mlawNonLocalPorosity::copyInternalVariableForElasticState_multipleNonLocalVariables(IPNonLocalPorosity* ipvcur, const IPNonLocalPorosity* ipvprev, const double T) const
{
  // starting from elastic state
  ipvcur->getRefToLocalPorosity() = ipvprev->getLocalPorosity();
  ipvcur->getRefToNonLocalPorosity() = ipvprev->getNonLocalPorosity();
  ipvcur->getRefToCorrectedPorosity() = ipvprev->getCorrectedPorosity();
  ipvcur->getRefToYieldPorosity() = ipvprev->getYieldPorosity();
  ipvcur->getRefToLocalLogarithmPorosity() = ipvprev->getLocalLogarithmPorosity();
  ipvcur->getRefToNonLocalLogarithmPorosity() = ipvprev->getNonLocalLogarithmPorosity();
  
  // Fp
  ipvcur->getRefToFp() = ipvprev->getConstRefToFp();
  // local 
  ipvcur->getRefToLocalMatrixPlasticStrain() = ipvprev->getLocalMatrixPlasticStrain();
  ipvcur->getRefToLocalVolumetricPlasticStrain() = ipvprev->getLocalVolumetricPlasticStrain();
  ipvcur->getRefToLocalDeviatoricPlasticStrain() = ipvprev->getLocalDeviatoricPlasticStrain();
  
  ipvcur->getRefToPlasticEnergy() = ipvprev->plasticEnergy();
  ipvcur->getRefToDissipationActive() = false;
  ipvcur->setFailed(ipvprev->isFailed());

    /* Non-local length law */
  if (_cLLaw.size() != 3){
    Msg::Error("three nonlocal length must be used");
  }

  _cLLaw[0]->computeCL(ipvprev->getNonLocalPorosity(), ipvcur->getRefToIPCLength(0));
  _cLLaw[1]->computeCL(ipvprev->getNonLocalPorosity(), ipvcur->getRefToIPCLength(1));
  _cLLaw[2]->computeCL(ipvprev->getNonLocalPorosity(), ipvcur->getRefToIPCLength(2));

  // update hardening
  if (isThermomechanicallyCoupled()){
    _j2IH->hardening(ipvprev->getLocalMatrixPlasticStrain(),ipvprev->getConstRefToIPJ2IsotropicHardening(),ipvcur->getLocalMatrixPlasticStrain(),T, ipvcur->getRefToIPJ2IsotropicHardening());
  }
  else{
    _j2IH->hardening(ipvprev->getLocalMatrixPlasticStrain(),ipvprev->getConstRefToIPJ2IsotropicHardening(),ipvcur->getLocalMatrixPlasticStrain(),ipvcur->getRefToIPJ2IsotropicHardening());
  }
  double wp=0.;
  double dwp=0.;
  getYieldDecrease(ipvcur->getNonLocalMatrixPlasticStrain(), wp, dwp);
  ipvcur->getRefToIPJ2IsotropicHardening().setScaleFactor(wp, dwp);
  
  // copy void state
  ipvcur->getRefToIPVoidState() = (ipvprev->getConstRefToIPVoidState());

  // coalescence from previous state
  ipvcur->getRefToIPCoalescence() = (ipvprev->getConstRefToIPCoalescence());
  //
  // no nucleation
  getNucleationLaw()->previousBasedRefresh( ipvcur->getRefToIPNucleation() , ipvprev->getConstRefToIPNucleation() );
}

void mlawNonLocalPorosity::predictorCorrector_multipleNonLocalVariables(const STensor3& F0,         // initial deformation gradient (input @ time n)
                            const STensor3& F1,         // updated deformation gradient (input @ time n+1)
                            STensor3 &P,                // updated 1st Piola-Kirchhoff stress tensor (output)
                                                        // contains the initial values on input
                            const IPNonLocalPorosity *ipvprev,       // array of initial internal variable
                            IPNonLocalPorosity *ipvcur,             // updated array of internal variable (in ipvcur on output),
                            STensor43 &Tangent,         // constitutive tangents (output)
                            std::vector<STensor3>  &dLocalVarDF,
                            std::vector<STensor3>  &dStressDNonLocalVar,
                            fullMatrix<double>    &dLocalVarDNonLocalVar,
                            const bool stiff,                   // if true compute the tangents
                            STensor43& dFedF, std::vector<STensor3>& dFeDNonLocalVar,
                            STensor3*  dStressDT,
                            fullMatrix<double>* dLocalVarDT,
                            const double T0 ,const double T
                           ) const{

  copyInternalVariableForElasticState_multipleNonLocalVariables(ipvcur,ipvprev,T);

  double& R  = ipvcur->getRefToCurrentViscoplasticYieldStress();
  R= ipvcur->getConstRefToIPJ2IsotropicHardening().getR();
  
  //
  const STensor3* Fp0 = &(ipvprev->getConstRefToFp());
  STensor3& Fp1 = ipvcur->getRefToFp();
  double& fV1 = ipvcur->getRefToLocalPorosity();
  STensor3& Ee = ipvcur->getRefToElasticDeformation();
  STensor3& corKir = ipvcur->getRefToCorotationalKirchhoffStress();

  // elastic predictor
  static STensor3 kcorprDev, Fe, Fepr, Ce, Cepr, Epr;
  static double ppr;
  static STensor43 Lepr, Le;
  static STensor63 dLepr, dLe;


  static STensor3 corKirpr;
  static STensor43 DcorKirprDEpr,DkcorprDevDEpr,DcorKirDEpr, DFpDEpr;
  static STensor3 DpprDEpr;
  static STensor3 DcorKirDT, DcorKirprDT, DkcorprDevDT;
  static STensor3 DFpDT;
  static double dpprDT;

  static std::vector<STensor3> DcorKirDNonLocalVar, DFpDNonLocalVar, DLocalVarDEpr;
  if (DcorKirDNonLocalVar.size() != _numNonLocalVar){
    DcorKirDNonLocalVar.resize(_numNonLocalVar);
    DFpDNonLocalVar.resize(_numNonLocalVar);
    DLocalVarDEpr.resize(_numNonLocalVar);
  };
  
  ipvcur->getRefToConstitutiveSuccessFlag() = true;
  // elastic predictor
  bool okElast = elasticPredictor(F1,*Fp0,corKirpr,kcorprDev,ppr,DcorKirprDEpr,DkcorprDevDEpr,DpprDEpr,Fepr,Cepr,Epr,Lepr,dLepr,stiff,
                  &T,&DcorKirprDT,&DkcorprDevDT,&dpprDT);
  if (!okElast)
  {
    ipvcur->getRefToConstitutiveSuccessFlag() = false;
    P(0,0) = P(1,1) = P(2,2)= sqrt(-1.); // to exist NR and performed next step with a smaller incremenet
    return;
  }
  // elastic only
  corKir = corKirpr;
  Fe = Fepr;
  Ce = Cepr;
  Ee = Epr;
  Le = Lepr;
  dLe = dLepr;

  if (stiff){
    STensorOperation::zero(DFpDEpr);
    for (int il = 0; il < _numNonLocalVar; il++){
      STensorOperation::zero(DcorKirDNonLocalVar[il]);
      STensorOperation::zero(DFpDNonLocalVar[il]);
      STensorOperation::zero(DLocalVarDEpr[il]);
      if (isThermomechanicallyCoupled()){
        (*dLocalVarDT)(il,0) = 0.;
      }
    }
    STensorOperation::zero(DFpDT);//--------added
    DcorKirDEpr = DcorKirprDEpr;
    DcorKirDT = DcorKirprDT;
    dLocalVarDNonLocalVar.setAll(0.);

    ipvcur->getRefToDPlasticEnergyDMatrixPlasticStrain() = 0.;
    STensorOperation::zero(ipvcur->getRefToDPlasticEnergyDF());
    if (isThermomechanicallyCoupled()){
      STensorOperation::zero(ipvcur->getRefToDPlasticEnergyDT());
    }
    for (int i=0; i< _numNonLocalVar; i++){
      STensorOperation::zero(ipvcur->getRefToDPlasticEnergyDNonLocalVariable(i));
    }
  }

  // predictor
  double kCorPrEq = sqrt(1.5*kcorprDev.dotprod());

  bool correctorOK = false;
  bool plastic = false;

  if (ipvcur->getNonLocalToLocal()){
    // check with previous value
    double yieldfVprev = ipvprev->getYieldPorosity();
    double yieldF = 0.;
    if (_stressFormulation == CORO_KIRCHHOFF)
    {
      yieldF =  yieldFunction(kCorPrEq,ppr,R,yieldfVprev,ipvprev,ipvcur,&T);
    }
    else if (_stressFormulation == CORO_CAUCHY)
    {
      double detF = STensorOperation::determinantSTensor3(F1);
      double sigCorPrEq = kCorPrEq/detF;
      double presCorPr  = ppr/detF;
      yieldF =  yieldFunction(sigCorPrEq,presCorPr,R,yieldfVprev,ipvprev,ipvcur,&T);
    }
    else
    {
      Msg::Error("stress method %d is not correctly defined",_stressFormulation);
    }
    
    plastic = (yieldF > _tol);
    //printf("plastic=%d\n",plastic);
    if (plastic){
      //Msg::Info("plastic occurs");
      double DfVtildeStarDtildefVPrev = ipvprev->getConstRefToIPCoalescence().getAccelerateRate();
      correctorOK = plasticCorrectorLocal(F1, ipvprev->getCorrectedPorosity(),DfVtildeStarDtildefVPrev,corKirpr,kcorprDev,ppr,ipvprev,ipvcur,
                        Fe,Ce,Ee,Le,dLe,
                        stiff,DcorKirprDEpr,DkcorprDevDEpr,DpprDEpr,
                        DcorKirDEpr, DFpDEpr,
                        &T, &DcorKirprDT,&DkcorprDevDT,&dpprDT,
                        &DcorKirDT,&DFpDT);

      if (!correctorOK and _withSubstepping){
        #ifdef _DEBUG
        Msg::Info("start substepping");
        #endif //_DEBUG
        // substepping devide by 0 untile converge
        int numSubStep = 2.;
        int numAttempt = 0;
        static STensor3 dF, Fcur;
        dF = F1;
        dF -= F0;
        static IPNonLocalPorosity ipvTemp(*dynamic_cast<const IPNonLocalPorosity*>(ipvprev));
        
        double dT,Tcur;//----temperature for substepping
        dT = T-T0;//--------added to substep the temperature
        while (true){
          bool success = true;
          int iter = 0;
          ipvTemp.operator =(*dynamic_cast<const IPVariable*>(ipvprev)); // take privious value

          while (iter < numSubStep){

            iter++;
            double fact = ((double)iter)/(double)numSubStep;
            //Msg::Info("substepping step %d of %d fact = %e",iter,numSubStep,fact);
            bool estimateStiff = false;
            if (iter == numSubStep){
              estimateStiff = stiff;
            }
            //
            Fcur = F0;
            Fcur.daxpy(dF,fact);
            Tcur = T0+ fact*dT;

            //elastic predictor
            Fp0 = &ipvTemp.getConstRefToFp();
            // Fp
						ipvcur->getRefToFp() = ipvTemp.getConstRefToFp();

            okElast =  elasticPredictor(Fcur,*Fp0,corKirpr,kcorprDev,ppr,DcorKirprDEpr,DkcorprDevDEpr,DpprDEpr,Fepr,Cepr,Epr,Lepr,dLepr,estimateStiff,
                             &Tcur, &DcorKirprDT,&DkcorprDevDT,&dpprDT);
            if (!okElast)
            {
              P(0,0) = P(1,1) = P(2,2)= sqrt(-1.); // to exist NR and performed next step with a smaller incremenet
              ipvcur->getRefToConstitutiveSuccessFlag() = false;
              return;
            }
            //check yield condition
            kCorPrEq = sqrt(1.5*kcorprDev.dotprod());
            R = ipvTemp.getConstRefToIPJ2IsotropicHardening().getR();
            if (_stressFormulation == CORO_KIRCHHOFF)
            {
              yieldF =  yieldFunction(kCorPrEq,ppr, R,ipvTemp.getYieldPorosity(),&ipvTemp,ipvcur,&Tcur);
            }
            else if (_stressFormulation == CORO_CAUCHY)
            {
              double detF = STensorOperation::determinantSTensor3(F1);
              double sigCorPrEq = kCorPrEq/detF;
              double presCorPr  = ppr/detF;
              yieldF =  yieldFunction(sigCorPrEq,presCorPr, R,ipvTemp.getYieldPorosity(),&ipvTemp,ipvcur,&Tcur);
            }
            else
            {
              Msg::Error("stress method %d is not correctly defined",_stressFormulation);
            }
            
            plastic = (yieldF > _tol);

            if (plastic){
              //Msg::Info("plastic occurs");
              correctorOK = plasticCorrectorLocal(Fcur,ipvTemp.getCorrectedPorosity(),DfVtildeStarDtildefVPrev,corKirpr,kcorprDev,ppr,&ipvTemp,ipvcur,
                        Fe,Ce,Ee,Le,dLe,
                        estimateStiff,DcorKirprDEpr,DkcorprDevDEpr,DpprDEpr,
                        DcorKirDEpr, DFpDEpr,
                        &Tcur, &DcorKirprDT,&DkcorprDevDT,&dpprDT,
                        &DcorKirDT,&DFpDT);
							if (!correctorOK){
								success = false;
              	break;
							}
            }
            if (iter < numSubStep){
              ipvTemp.operator =(*dynamic_cast<const IPVariable*>(ipvcur));
            }
          }
          if (!success){
            numSubStep *= 2;
            numAttempt++;
            #ifdef _DEBUG
            Msg::Warning("substeping is not converged, decrease number of step");
            #endif //_DEBUG
          }
          else{
            #ifdef _DEBUG
            Msg::Info("substeping with success");
            #endif //_DEBUG
						correctorOK = true;
            plastic = true;
            break;
          }
          if (numAttempt > _maxAttemptSubstepping){
            #ifdef _DEBUG
            Msg::Warning("maximal attemp substepping is reached");
            #endif //_DEBUG
            correctorOK = false;
            plastic = true;
            break;
          }
        }
      }
    }
  }
  else{
    bool elasticFollowedBlocked = false;
    bool plasticFollowedBlocked = false;

    if (ipvcur->dissipationIsBlocked()){
      if (_postDamageBlockingBehavior == ELASTIC){
        elasticFollowedBlocked = true;
        plasticFollowedBlocked = false;
      }
      else if (_postDamageBlockingBehavior == ELASTOPLASTIC){
        elasticFollowedBlocked = false;
        plasticFollowedBlocked = true;
      }
      else{
        Msg::Error("method %d has not been implemented",_postDamageBlockingBehavior);
      }
    }

    if (!elasticFollowedBlocked){
      static std::vector<double> DyieldfVDNonlocalVar(3,0.);
      static STensor3 DyieldfVDKcorpr;
      voidCharacteristicsEvolution_multipleNonLocalVariables(corKirpr,ipvprev,ipvcur,DyieldfVDNonlocalVar,DyieldfVDKcorpr,plasticFollowedBlocked);
      // check with previous value
      double yieldfV = ipvcur->getYieldPorosity(); // does not change  during corrector
      double yieldF = 0.;
      if (_stressFormulation == CORO_KIRCHHOFF)
      {
        yieldF = yieldFunction(kCorPrEq,ppr, R,yieldfV,ipvprev,ipvcur,&T);
      }
      else if (_stressFormulation == CORO_CAUCHY)
      {
        double detF = STensorOperation::determinantSTensor3(F1);
        double sigCorPrEq = kCorPrEq/detF;
        double presCorPr  = ppr/detF;
        yieldF =  yieldFunction(sigCorPrEq,presCorPr, R,yieldfV,ipvprev,ipvcur,&T);
      }
      else
      {
        Msg::Error("stress method %d is not correctly defined",_stressFormulation);
      }
      
      plastic = (yieldF > _tol);
      if (plastic){
        correctorOK = plasticCorrector_multipleNonLocalVariables(F1,
                        yieldfV,DyieldfVDNonlocalVar,DyieldfVDKcorpr,
                        corKirpr,kcorprDev,ppr,ipvprev,ipvcur,
                        Fe,Ce,Ee,Le,dLe,stiff,DcorKirprDEpr,DkcorprDevDEpr,DpprDEpr,
                        DcorKirDEpr,DcorKirDNonLocalVar,
                        DFpDEpr,DFpDNonLocalVar,
                        DLocalVarDEpr,dLocalVarDNonLocalVar,
                        &T,&DcorKirprDT,&DkcorprDevDT,&dpprDT,&DcorKirDT,&DFpDT,dLocalVarDT);

        if (!correctorOK and _withSubstepping){
          #ifdef _DEBUG
          printf("start substepping\n");
          #endif //_DEBUG
          // substepping devide by 0 untile converge
          int numSubStep = 2.;
          int numAttempt = 0;
          
          static STensor3 dF;
          dF = F1;
          dF -= F0;
          double DeltaHatQNonLocal = ipvcur->getNonLocalVolumetricPlasticStrain() - ipvprev->getNonLocalVolumetricPlasticStrain();
          double DeltaHatPNonLocal = ipvcur->getNonLocalMatrixPlasticStrain() - ipvprev->getNonLocalMatrixPlasticStrain();
          double DeltaHatDNonLocal = ipvcur->getNonLocalDeviatoricPlasticStrain() - ipvprev->getNonLocalDeviatoricPlasticStrain();
          double dT = T - T0;
          
          static IPNonLocalPorosity* ipvTemp = dynamic_cast<IPNonLocalPorosity*>(ipvprev->clone()); // do not known type of ip a priori
          
          while (true){
            bool success = true;
            int iter = 0;
            ipvTemp->operator =(*dynamic_cast<const IPVariable*>(ipvprev)); // take privious value

            while (iter < numSubStep){

              iter++;
              double fact = ((double)iter)/(double)numSubStep;
              //Msg::Info("substepping step %d of %d fact = %e",iter,numSubStep,fact);
              bool estimateStiff = false;
              if (iter == numSubStep){
                estimateStiff = stiff;
              }

              //update current kinematic variables
              static STensor3 Fcur;
              Fcur = F0;
              Fcur.daxpy(dF,fact);
							ipvcur->getRefToNonLocalVolumetricPlasticStrain() = ipvprev->getNonLocalVolumetricPlasticStrain() + fact*DeltaHatQNonLocal;
							ipvcur->getRefToNonLocalDeviatoricPlasticStrain() = ipvprev->getNonLocalDeviatoricPlasticStrain() + fact*DeltaHatDNonLocal;
							ipvcur->getRefToNonLocalMatrixPlasticStrain() = ipvprev->getNonLocalMatrixPlasticStrain() + fact*DeltaHatPNonLocal;
              double Tcur = T0 + fact*dT;//------for temperature substepping
              ipvcur->setTemperature(Tcur); // some dependence on temperature must be followed
              
              copyInternalVariableForElasticState_multipleNonLocalVariables(ipvcur,ipvTemp,Tcur);
              
              //elastic predictor
              Fp0 = &ipvTemp->getConstRefToFp();
              okElast =  elasticPredictor(Fcur,*Fp0,corKirpr,kcorprDev,ppr,DcorKirprDEpr,DkcorprDevDEpr,DpprDEpr,Fepr,Cepr,Epr,Lepr,dLepr,estimateStiff,
                              &Tcur,&DcorKirprDT,&DkcorprDevDT,&dpprDT);
              if (!okElast)
              {
                ipvcur->getRefToConstitutiveSuccessFlag() = false;
                P(0,0) = P(1,1) = P(2,2)= sqrt(-1.); // to exist NR and performed next step with a smaller incremenet
                return;
              }
              //check yield condition
              kCorPrEq = sqrt(1.5*kcorprDev.dotprod());
              R = ipvTemp->getConstRefToIPJ2IsotropicHardening().getR();
              voidCharacteristicsEvolution_multipleNonLocalVariables(corKirpr,ipvTemp,ipvcur,DyieldfVDNonlocalVar,DyieldfVDKcorpr,plasticFollowedBlocked);
              
              yieldfV = ipvcur->getYieldPorosity(); // does not change  during corrector
              if (_stressFormulation == CORO_KIRCHHOFF)
              {
                yieldF =  yieldFunction(kCorPrEq,ppr, R,yieldfV,ipvTemp,ipvcur,&Tcur);
              }
              else if (_stressFormulation == CORO_CAUCHY)
              {
                double detF = STensorOperation::determinantSTensor3(F1);
                double sigCorPrEq = kCorPrEq/detF;
                double presCorPr  = ppr/detF;
                yieldF =  yieldFunction(sigCorPrEq,presCorPr, R,yieldfV,ipvTemp,ipvcur,&Tcur);
              }
              else
              {
                Msg::Error("stress method %d is not correctly defined",_stressFormulation);
              }
              plastic = (yieldF > _tol);

              if (plastic){
                correctorOK = plasticCorrector_multipleNonLocalVariables(Fcur,
                        yieldfV,DyieldfVDNonlocalVar,DyieldfVDKcorpr,
                        corKirpr,kcorprDev,ppr,ipvTemp,ipvcur,
                        Fe,Ce,Ee,Le,dLe,estimateStiff,DcorKirprDEpr,DkcorprDevDEpr,DpprDEpr,
                        DcorKirDEpr,DcorKirDNonLocalVar,
                        DFpDEpr,DFpDNonLocalVar,
                        DLocalVarDEpr,dLocalVarDNonLocalVar,
                        &Tcur,&DcorKirprDT,&DkcorprDevDT,&dpprDT,&DcorKirDT,&DFpDT,dLocalVarDT);

								if (!correctorOK){
									success = false;
                	break;
								}
              }
              // next step
              if (iter < numSubStep){
                ipvTemp->operator =(*static_cast<const IPVariable*>(ipvcur));
              }
            }
            if (!success){
              numSubStep *= 2;
              numAttempt++;
              #ifdef _DEBUG
              printf("substeping is not converged, decrease number of step\n");
              #endif //_DEBUG
            }
            else{
              #ifdef _DEBUG
              printf("substeping with success\n");
              #endif //_DEBUG
							correctorOK = true;
							plastic = true;
              break;
            }
            if (numAttempt > _maxAttemptSubstepping){
              #ifdef _DEBUG
              printf("maximal attemp substepping is reached\n");
              #endif //_DEBUG
              correctorOK = false;
              plastic = true;
              break;
            }
          }
        }
      }
    }
  }

  if (plastic and !correctorOK){
    P(0,0) = sqrt(-1.); // to exist NR and performed next step with a smaller incremenet
    ipvcur->getRefToConstitutiveSuccessFlag() = false;
    return;
  }


  // update first PK stress
  static STensor3 S, FeS, invFp;
  STensorOperation::inverseSTensor3(Fp1,invFp);
  STensorOperation::multSTensor3STensor43(corKir,Le,S);
  STensorOperation::multSTensor3(Fe,S,FeS);
  STensorOperation::multSTensor3SecondTranspose(FeS,invFp,P);

  // elastic energy
  ipvcur->getRefToElasticEnergy()= this->deformationEnergy(Ce,&T);
  if (this->getMacroSolver()->withPathFollowing()){
    // irreversible energy
    if ((this->getMacroSolver()->getPathFollowingLocalIncrementType() == pathFollowingManager::PLASTIC_ENERGY) or
             (this->getMacroSolver()->getPathFollowingLocalIncrementType() == pathFollowingManager::DISSIPATION_ENERGY)) {
      ipvcur->getRefToIrreversibleEnergy() = ipvprev->irreversibleEnergy()+ ipvcur->plasticEnergy() - ipvprev->plasticEnergy();
    }
    else{
      ipvcur->getRefToIrreversibleEnergy() = 0.;
    }
  };

  ipvcur->getRefToDissipationActive() = plastic;

  if (stiff){
    static STensor3 invFp0;
    STensorOperation::inverseSTensor3(*Fp0,invFp0);
    static STensor43 EprToF;
    for (int i=0; i<3; i++){
      for (int j=0; j<3; j++){
        for (int k=0; k<3; k++){
          for (int l=0; l<3; l++){
            EprToF(i,j,k,l) = 0.;
            for (int p=0; p<3; p++){
              for (int q=0; q<3; q++){
                EprToF(i,j,k,l) += Lepr(i,j,p,q)*Fepr(k,p)*invFp0(l,q);
              }
            }
          }
        }
      }
    }

    if (ipvcur->getNonLocalToLocal()){
      if(!isThermomechanicallyCoupled()){
        tangentComputationLocal(Tangent, plastic,F1,corKir, S, Fepr,  *Fp0, Lepr,
                      Fe,Fp1,Le, dLe,
                      DcorKirDEpr, DFpDEpr, EprToF, invFp);
      }
      else {

        tangentComputationLocalWithTemperature(Tangent,*dStressDT, plastic,F1,corKir, S, Fepr,  *Fp0, Lepr,
                      Fe,Fp1,Le, dLe,
                      DcorKirDEpr,DcorKirDT, DFpDEpr,DFpDT, EprToF, invFp); //-----------added
      }
      for (int i=0; i< _numNonLocalVar; i++){
        STensorOperation::zero(dStressDNonLocalVar[i]);
      }
      dLocalVarDNonLocalVar.setAll(0.);
    }
    else{
      if(isThermomechanicallyCoupled()){
        tangentComputationWithTemperature(dFedF,dFeDNonLocalVar,Tangent,dStressDNonLocalVar, *dStressDT, dLocalVarDF,
                             plastic,F1,corKir, S, Fepr,  *Fp0, Lepr,
                             Fe,Fp1,Le, dLe,
                             DcorKirDEpr, DcorKirDNonLocalVar, DcorKirDT,DFpDEpr, DFpDNonLocalVar, DFpDT, DLocalVarDEpr,
                             EprToF, invFp);
      }
      else{
         tangentComputation(dFedF,dFeDNonLocalVar,Tangent,dStressDNonLocalVar, dLocalVarDF,
                             plastic,F1,corKir, S, Fepr,  *Fp0, Lepr,
                             Fe,Fp1,Le, dLe,
                             DcorKirDEpr, DcorKirDNonLocalVar,DFpDEpr, DFpDNonLocalVar, DLocalVarDEpr,
                             EprToF, invFp);
      }
    }

    //update tangent of plastic energy
    static STensor3 DplEnergyDEpr;
    DplEnergyDEpr = ipvcur->getConstRefToDPlasticEnergyDF();
    STensor3& DplEnergyDF = ipvcur->getRefToDPlasticEnergyDF();
    STensorOperation::multSTensor3STensor43(DplEnergyDEpr,EprToF,DplEnergyDF);

    if (this->getMacroSolver()->withPathFollowing()){
      // irreversible energy
      if ((this->getMacroSolver()->getPathFollowingLocalIncrementType() == pathFollowingManager::PLASTIC_ENERGY) or
               (this->getMacroSolver()->getPathFollowingLocalIncrementType() == pathFollowingManager::DISSIPATION_ENERGY)) {
        ipvcur->getRefToDIrreversibleEnergyDF() = ipvcur->getConstRefToDPlasticEnergyDF();
        for (int i=0; i< _numNonLocalVar; i++){
          if (ipvcur->getNonLocalToLocal()){
            ipvcur->getRefToDIrreversibleEnergyDNonLocalVariable(i) = 0.;
          }
          else{
            ipvcur->getRefToDIrreversibleEnergyDNonLocalVariable(i) = ipvcur->getConstRefToDPlasticEnergyDNonLocalVariable(i);
          }
        }
      }
      else{
        Msg::Error("Path following method is only contructed with dissipation based on plastic energy");
      }
    };
  }
};

void mlawNonLocalPorosity::I1J2J3_getNonLocalVars(std::vector<double>& Vars,  const IPNonLocalPorosity* q) const{
  // compatible with nonlocal formulation
  if (Vars.size() != 3) Vars.resize(3,0.);
  Vars[0] = q->getNonLocalVolumetricPlasticStrain();
  Vars[1] = q->getNonLocalMatrixPlasticStrain();
  Vars[2] = q->getNonLocalDeviatoricPlasticStrain();
};

void mlawNonLocalPorosity::I1J2J3_updateInternalVariables(IPNonLocalPorosity *q1, const IPNonLocalPorosity* q0, 
                            const STensor3& DeltaEp, const double DeltaHatP, const double* T) const{
   // update internal deviatoric and volumetric plastic strain
  double DeltaHatQ, DeltaHatD;
  static STensor3 devDeltaEp;
  STensorOperation::decomposeDevTr(DeltaEp, devDeltaEp, DeltaHatQ);
  DeltaHatD = sqrt(2.*STensorOperation::doubledot(devDeltaEp,devDeltaEp)/3.);
  
  updateInternalVariables(q1,q0,DeltaHatD,DeltaHatQ,DeltaHatP,true,T);
};

void mlawNonLocalPorosity::I1J2J3_voidCharacteristicsEvolution(const STensor3& sig, double DeltaHatQ, double DeltaHatP, double DeltaHatD,
                                                  const IPNonLocalPorosity *q0, IPNonLocalPorosity* q1,
                                                  bool stiff,
                                                  std::vector<double>* DyieldfVDVars,
                                                  STensor3* DyieldfVDsig
                                                  ) const
{
  // 
  double& fV = q1->getRefToLocalPorosity();
  double& fVtilde = q1->getRefToNonLocalPorosity();
  double& fVtildeStar = q1->getRefToCorrectedPorosity();
  double& yieldfV = q1->getRefToYieldPorosity();
 
  if (getVoidEvolutionLaw()->useYieldPorosityDuringCoalescence())
  {
    // nucleation as DeltaHatPNonLocal changed
    nucleation(q1->getNonLocalMatrixPlasticStrain(),q0->getNonLocalMatrixPlasticStrain(),q0->getYieldPorosity(),q1,q0);
    
    double DDeltafVtildeDDeltaHatD, DDeltafVtildeDDeltaHatQ, DDeltafVtildeDDeltaHatP;
    static STensor3 DDeltafVtildeDsig;
    // nonlocal void growth
    localPorosityGrowth(q1, DeltaHatD,DeltaHatQ,DeltaHatP,
                        sig,q0, stiff, 
                        &DDeltafVtildeDDeltaHatD, &DDeltafVtildeDDeltaHatQ,&DDeltafVtildeDDeltaHatP,
                        &DDeltafVtildeDsig);
    // update void
    fVtilde = fV;
    
    // update yield porosity as it depends on nonlocal variable
    double DfVtildeStarDtildefV = q1->getConstRefToIPCoalescence().getAccelerateRate();
    fVtildeStar = q0->getCorrectedPorosity() + DfVtildeStarDtildefV*(q1->getLocalPorosity() - q0->getLocalPorosity());
    yieldfV = _regularisationFunc_porosityEff->getVal(fVtildeStar);
    double DyieldfVDDfVtilde = _regularisationFunc_porosityEff->getDiff(fVtildeStar)*DfVtildeStarDtildefV;
        
    // update void state
    voidStateGrowth(q0->getYieldPorosity(),q1->getYieldPorosity(),DeltaHatD,DeltaHatQ,DeltaHatP,q0,q1);
    //
    if (stiff)
    {
      *DyieldfVDsig = DDeltafVtildeDsig;
      *DyieldfVDsig *= DyieldfVDDfVtilde;
      
      if (DyieldfVDVars->size() != 3) Msg::Error("DyieldfVDVars is not correctly allocated");
      (*DyieldfVDVars)[0] =  DyieldfVDDfVtilde*DDeltafVtildeDDeltaHatQ;
      (*DyieldfVDVars)[1] =  DyieldfVDDfVtilde*DDeltafVtildeDDeltaHatP;
      (*DyieldfVDVars)[2] =  DyieldfVDDfVtilde*DDeltafVtildeDDeltaHatD;
    }
  }
  else
  {
    // void characteristics are evaluated first, then porosity are evaluated
    // other void characteristic evolutions are called first
    // then void evolution is estimated
    // update void state
    voidStateGrowth(q0->getYieldPorosity(),q1->getYieldPorosity(),DeltaHatD,DeltaHatQ,DeltaHatP,q0,q1);

    // local porosity
    const IPVoidState* ipvoid = &(q1->getConstRefToIPVoidState());

    double lambda = ipvoid->getVoidSpacingRatio();
    double W = ipvoid->getVoidAspectRatio();
    double Chi = ipvoid->getVoidLigamentRatio();
    double gamma = ipvoid->getVoidShapeFactor();

    fV = Chi*Chi*Chi*W/(3.*gamma*lambda); // always corrects
    // Regularise porosity growth and rates
    double regularizedRate = 1.;
    if (fV > _localfVFailure)
    {
      double fVn = q0->getLocalPorosity();
      double DeltafV = fV - fVn;
      fV = _localfVFailure;
      regularizedRate = 0.;
      if (fabs(DeltafV)> 0)
      {
        regularizedRate = (_localfVFailure - fVn)/DeltafV;
      }
    }
    // update void
    fVtilde = fV;
    // update yield porosity as it depends on nonlocal variable
    double DfVtildeStarDtildefV = 1.;
    fVtildeStar = q0->getCorrectedPorosity() + DfVtildeStarDtildefV*(q1->getLocalPorosity() - q0->getLocalPorosity());
    yieldfV = _regularisationFunc_porosityEff->getVal(fVtildeStar);
    double DyieldfVDDfV= _regularisationFunc_porosityEff->getDiff(fVtildeStar)*DfVtildeStarDtildefV;

    if (stiff)
    {
      STensorOperation::zero(*DyieldfVDsig);
      if (DyieldfVDVars->size() != 3) Msg::Error("DyieldfVDVars is not correctly allocated");
      // nonlocal var = [volumetric, matrix, deviatoric]
      double DfVDChi = 3.*fV*regularizedRate/Chi;
      double DfVDlambda = -fV*regularizedRate/lambda;
      double DfVDW = fV*regularizedRate/W;
      double DfVDgamma = -fV*regularizedRate/gamma;

      (*DyieldfVDVars)[0] =  DfVDChi*ipvoid->getDVoidLigamentRatioDVolumetricPlasticDeformation() +
                                     DfVDW*ipvoid->getDVoidAspectRatioDVolumetricPlasticDeformation()+
                                     DfVDgamma*ipvoid->getDVoidShapeFactorDVolumetricPlasticDeformation()+
                                     DfVDlambda*ipvoid->getDVoidSpacingRatioDVolumetricPlasticDeformation();
      (*DyieldfVDVars)[1] =  DfVDChi*ipvoid->getDVoidLigamentRatioDMatrixPlasticDeformation() +
                                     DfVDW*ipvoid->getDVoidAspectRatioDMatrixPlasticDeformation()+
                                     DfVDgamma*ipvoid->getDVoidShapeFactorDMatrixPlasticDeformation()+
                                     DfVDlambda*ipvoid->getDVoidSpacingRatioDMatrixPlasticDeformation();
      (*DyieldfVDVars)[2] =  DfVDChi*ipvoid->getDVoidLigamentRatioDDeviatoricPlasticDeformation() +
                                     DfVDW*ipvoid->getDVoidAspectRatioDDeviatoricPlasticDeformation()+
                                     DfVDgamma*ipvoid->getDVoidShapeFactorDDeviatoricPlasticDeformation()+
                                     DfVDlambda*ipvoid->getDVoidSpacingRatioDDeviatoricPlasticDeformation();
    };
  };

}

void mlawNonLocalPorosity::I1J2J3_voidCharacteristicsEvolution_NonLocal(const STensor3& sig,
                                                const IPNonLocalPorosity *q0, IPNonLocalPorosity* q1,
                                                bool stiff,
                                                std::vector<double>* Y,
                                                std::vector<STensor3>* DYDsig,
                                                std::vector<std::vector<double> >* DYDNonlocalVars
                                                ) const
{ 
  static std::vector<double> nonlocalVars(3,0.), nonlocalVarsPrev(3,0.);
  I1J2J3_getNonLocalVars(nonlocalVars,q1);
  I1J2J3_getNonLocalVars(nonlocalVarsPrev,q0);
  double DeltaHatQNonLocal = nonlocalVars[0] - nonlocalVarsPrev[0];
  double DeltaHatPNonLocal = nonlocalVars[1] - nonlocalVarsPrev[1];
  double DeltaHatDNonLocal = nonlocalVars[2] - nonlocalVarsPrev[2];
  
  static std::vector<double> DyieldfVDNonlocalVars(3,0.);
  static STensor3 DyieldfVDsig;
  I1J2J3_voidCharacteristicsEvolution(sig,DeltaHatQNonLocal,DeltaHatPNonLocal,DeltaHatDNonLocal,q0,q1,stiff,&DyieldfVDNonlocalVars,&DyieldfVDsig);
  if (stiff)
  {
    I1J2J3_getVoidCharacteristics(*Y,*DYDsig,*DYDNonlocalVars,q0,q1,DyieldfVDNonlocalVars,DyieldfVDsig);
  }
};

void mlawNonLocalPorosity::I1J2J3_voidCharacteristicsEvolution_Local(const STensor3& sig, const STensor3& DeltaEp, const double DeltaHatP,
                                                const IPNonLocalPorosity *q0, IPNonLocalPorosity* q1,
                                                bool stiff,
                                                std::vector<double>* Y,
                                                std::vector<STensor3>* DYDsig,
                                                std::vector<STensor3>* DYDDeltaEp,
                                                std::vector<double>* DYDDeltaHatP) const
{ 
  double DeltaHatQ, DeltaHatD;
  static STensor3 devDeltaEp;
  STensorOperation::decomposeDevTr(DeltaEp, devDeltaEp, DeltaHatQ);
  DeltaHatD = sqrt(2.*STensorOperation::doubledot(devDeltaEp,devDeltaEp)/3.);
  
  static std::vector<double> DyieldfVDlocalVars(3,0.);
  static STensor3 DyieldfVDsig;
  I1J2J3_voidCharacteristicsEvolution(sig,DeltaHatQ,DeltaHatP,DeltaHatD,q0,q1,stiff,&DyieldfVDlocalVars,&DyieldfVDsig);
  if (stiff)
  {
    static std::vector<std::vector<double> > DYDlocalVars(4,std::vector<double>(3,0));
    I1J2J3_getVoidCharacteristics(*Y,*DYDsig,DYDlocalVars,q0,q1,DyieldfVDlocalVars,DyieldfVDsig);
    static STensor3 dDeltaHatQdDeltaEp, dDeltaHatDdDeltaEp;
    if (DeltaHatD > 0)
    {
      STensorOperation::diag(dDeltaHatQdDeltaEp,1.);
      dDeltaHatDdDeltaEp = devDeltaEp;
      dDeltaHatDdDeltaEp*= (2./(3.*DeltaHatD));
    }
    else
    {
      STensorOperation::zero(dDeltaHatQdDeltaEp);
      STensorOperation::zero(dDeltaHatDdDeltaEp);
    }
    
    for (int i=0; i < getNumOfVoidCharacteristics(); i++)
    {
      (*DYDDeltaHatP)[i] = DYDlocalVars[i][1];
      STensorOperation::zero((*DYDDeltaEp)[i]);
      (*DYDDeltaEp)[i].daxpy(dDeltaHatQdDeltaEp,DYDlocalVars[i][0]);
      (*DYDDeltaEp)[i].daxpy(dDeltaHatDdDeltaEp,DYDlocalVars[i][2]);
    }
  }
};

void mlawNonLocalPorosity::I1J2J3_getVoidCharacteristics(std::vector<double>& Y, 
                                                std::vector<STensor3>& DYDsig,
                                                std::vector<std::vector<double> >& DYDnonlocalVars, 
                                                const IPNonLocalPorosity *q0,  const IPNonLocalPorosity* q1,
                                                const std::vector<double>& DyieldfVDNonlocalVars,
                                                const STensor3& DyieldfVDsig) const{
  //
  // Y = [yieldfV Chi W gamma]
  //
  const IPVoidState&  voidState = q1->getConstRefToIPVoidState();
  if (Y.size() != 4) Y.resize(4,0.);
  Y[0] = q1->getYieldPorosity();
  Y[1] = voidState.getVoidLigamentRatio();
  Y[2] = voidState.getVoidAspectRatio();
  Y[3] = voidState.getVoidShapeFactor();
  
  // stress dependence
  if (DYDsig.size() != 4) Msg::Error("DYDsig is not correctly allocated");
  DYDsig[0]= DyieldfVDsig;

  DYDsig[1] = DYDsig[0];
  DYDsig[1] *= voidState.getDVoidLigamentRatioDYieldPorosity();
  
  DYDsig[2] = DYDsig[0];
  DYDsig[2] *= voidState.getDVoidAspectRatioDYieldPorosity();
  
  DYDsig[3] =DYDsig[0];
  DYDsig[3] *= voidState.getDVoidShapeFactorDYieldPorosity();
  
  // nonlocal dependence
  if (DYDnonlocalVars.size() != 4) Msg::Error("DYDnonlocalVars is not correctly allocated");
  DYDnonlocalVars[0][0] = DyieldfVDNonlocalVars[0];
  DYDnonlocalVars[0][1] = DyieldfVDNonlocalVars[1];
  DYDnonlocalVars[0][2] = DyieldfVDNonlocalVars[2];
  
  std::vector<double>& DChiDNonlocalVars = DYDnonlocalVars[1];
  DChiDNonlocalVars[0] = voidState.getDVoidLigamentRatioDYieldPorosity()*DyieldfVDNonlocalVars[0] + voidState.getDVoidLigamentRatioDVolumetricPlasticDeformation();
  DChiDNonlocalVars[1] = voidState.getDVoidLigamentRatioDYieldPorosity()*DyieldfVDNonlocalVars[1] + voidState.getDVoidLigamentRatioDMatrixPlasticDeformation();
  DChiDNonlocalVars[2] = voidState.getDVoidLigamentRatioDYieldPorosity()*DyieldfVDNonlocalVars[2] + voidState.getDVoidLigamentRatioDDeviatoricPlasticDeformation();
  
  std::vector<double>& DWDNonlocalVars = DYDnonlocalVars[2];
  DWDNonlocalVars[0] = voidState.getDVoidAspectRatioDYieldPorosity()*DyieldfVDNonlocalVars[0] + voidState.getDVoidAspectRatioDVolumetricPlasticDeformation();
  DWDNonlocalVars[1] = voidState.getDVoidAspectRatioDYieldPorosity()*DyieldfVDNonlocalVars[1] + voidState.getDVoidAspectRatioDMatrixPlasticDeformation();
  DWDNonlocalVars[2] = voidState.getDVoidAspectRatioDYieldPorosity()*DyieldfVDNonlocalVars[2] + voidState.getDVoidAspectRatioDDeviatoricPlasticDeformation();
  
  std::vector<double>& DgammaDNonlocalVars = DYDnonlocalVars[3];
  DgammaDNonlocalVars[0] = voidState.getDVoidShapeFactorDYieldPorosity()*DyieldfVDNonlocalVars[0] + voidState.getDVoidShapeFactorDVolumetricPlasticDeformation();
  DgammaDNonlocalVars[1] = voidState.getDVoidShapeFactorDYieldPorosity()*DyieldfVDNonlocalVars[1] + voidState.getDVoidShapeFactorDMatrixPlasticDeformation();
  DgammaDNonlocalVars[2] = voidState.getDVoidShapeFactorDYieldPorosity()*DyieldfVDNonlocalVars[2] + voidState.getDVoidShapeFactorDDeviatoricPlasticDeformation();
                                                  
};

void mlawNonLocalPorosity::I1J2J3_getResAndJacobian(fullVector<double>& res, fullMatrix<double>& J, 
                                const std::vector<double>& res0, const STensor3& res1, const double& res2,
                                const std::vector<STensor3>& Dres0DDeltaEp, const STensor43& Dres1DDeltaEp, const STensor3& Dres2DDeltaEp,
                                const std::vector<double>& Dres0DDeltaPlasticMult, const std::vector<STensor3>& Dres1DDeltaPlasticMult, const std::vector<double>& Dres2DDeltaPlasticMult,
                                const std::vector<double>& Dres0DDeltaHatP, const STensor3& Dres1DDeltaHatP,const double& Dres2DDeltaHatP) const{
  // unknown DeltaEp, DeltaPlasticMult_i, DeltaHatP
  // phi_i = 0, 
  // DeltaEp - sum_i DeltaPlasticMult_i * N_i = 0,
  // res2 = 0
  
  int numberYield = getNumOfYieldSurfaces();
  if (res.size() != 7+numberYield){
    res.resize(7+numberYield);
    J.resize(7+numberYield,7+numberYield);
  }
  // residual
  for (int i=0; i< numberYield; i++)
  {
    res(i) = res0[i];
  }
  res(numberYield) = res1(0,0);
  res(numberYield+1) = res1(1,1);
  res(numberYield+2) = res1(2,2);
  res(numberYield+3) = res1(0,1);
  res(numberYield+4) = res1(0,2);
  res(numberYield+5) = res1(1,2);
  res(numberYield+6) = res2;
  
  //
  J.setAll(0.);
  // jacobian for res0
  for (int i=0; i< numberYield; i++)
  {
    J(i,0) = Dres0DDeltaEp[i](0,0);
    J(i,1) = Dres0DDeltaEp[i](1,1);
    J(i,2) = Dres0DDeltaEp[i](2,2);
    J(i,3) = Dres0DDeltaEp[i](0,1)+Dres0DDeltaEp[i](1,0);
    J(i,4) = Dres0DDeltaEp[i](0,2)+Dres0DDeltaEp[i](2,0);
    J(i,5) = Dres0DDeltaEp[i](1,2)+Dres0DDeltaEp[i](2,1);
    J(i,6+i) = Dres0DDeltaPlasticMult[i]; // no cross term in yield
    J(i,6+numberYield) = Dres0DDeltaHatP[i];
  }
    
  // jacobian for res1
  J(numberYield,0) = Dres1DDeltaEp(0,0,0,0);
  J(numberYield,1) = Dres1DDeltaEp(0,0,1,1);
  J(numberYield,2) = Dres1DDeltaEp(0,0,2,2);
  J(numberYield,3) = Dres1DDeltaEp(0,0,0,1)+Dres1DDeltaEp(0,0,1,0);
  J(numberYield,4) = Dres1DDeltaEp(0,0,0,2)+Dres1DDeltaEp(0,0,2,0);
  J(numberYield,5) = Dres1DDeltaEp(0,0,1,2)+Dres1DDeltaEp(0,0,2,1);
  for (int i=0; i< numberYield; i++)
  {
    J(numberYield,6+i) = Dres1DDeltaPlasticMult[i](0,0);
  }
  J(numberYield,6+numberYield) = Dres1DDeltaHatP(0,0);
  
  J(numberYield+1,0) = Dres1DDeltaEp(1,1,0,0);
  J(numberYield+1,1) = Dres1DDeltaEp(1,1,1,1);
  J(numberYield+1,2) = Dres1DDeltaEp(1,1,2,2);
  J(numberYield+1,3) = Dres1DDeltaEp(1,1,0,1)+Dres1DDeltaEp(1,1,1,0);
  J(numberYield+1,4) = Dres1DDeltaEp(1,1,0,2)+Dres1DDeltaEp(1,1,2,0);
  J(numberYield+1,5) = Dres1DDeltaEp(1,1,1,2)+Dres1DDeltaEp(1,1,2,1);
  for (int i=0; i< numberYield; i++)
  {
    J(numberYield+1,6+i) = Dres1DDeltaPlasticMult[i](1,1);
  }
  J(numberYield+1,6+numberYield) = Dres1DDeltaHatP(1,1);
  
  J(numberYield+2,0) = Dres1DDeltaEp(2,2,0,0);
  J(numberYield+2,1) = Dres1DDeltaEp(2,2,1,1);
  J(numberYield+2,2) = Dres1DDeltaEp(2,2,2,2);
  J(numberYield+2,3) = Dres1DDeltaEp(2,2,0,1)+Dres1DDeltaEp(2,2,1,0);
  J(numberYield+2,4) = Dres1DDeltaEp(2,2,0,2)+Dres1DDeltaEp(2,2,2,0);
  J(numberYield+2,5) = Dres1DDeltaEp(2,2,1,2)+Dres1DDeltaEp(2,2,2,1);
  for (int i=0; i< numberYield; i++)
  {
    J(numberYield+2,6+i) = Dres1DDeltaPlasticMult[i](2,2);
  }
  J(numberYield+2,6+numberYield) = Dres1DDeltaHatP(2,2);
  
  J(numberYield+3,0) = Dres1DDeltaEp(0,1,0,0);
  J(numberYield+3,1) = Dres1DDeltaEp(0,1,1,1);
  J(numberYield+3,2) = Dres1DDeltaEp(0,1,2,2);
  J(numberYield+3,3) = Dres1DDeltaEp(0,1,0,1)+Dres1DDeltaEp(0,1,1,0);
  J(numberYield+3,4) = Dres1DDeltaEp(0,1,0,2)+Dres1DDeltaEp(0,1,2,0);
  J(numberYield+3,5) = Dres1DDeltaEp(0,1,1,2)+Dres1DDeltaEp(0,1,2,1);
  for (int i=0; i< numberYield; i++)
  {
    J(numberYield+3,6+i) = Dres1DDeltaPlasticMult[i](0,1);
  }
  J(numberYield+3,6+numberYield) = Dres1DDeltaHatP(0,1);
  
  J(numberYield+4,0) = Dres1DDeltaEp(0,2,0,0);
  J(numberYield+4,1) = Dres1DDeltaEp(0,2,1,1);
  J(numberYield+4,2) = Dres1DDeltaEp(0,2,2,2);
  J(numberYield+4,3) = Dres1DDeltaEp(0,2,0,1)+Dres1DDeltaEp(0,2,1,0);
  J(numberYield+4,4) = Dres1DDeltaEp(0,2,0,2)+Dres1DDeltaEp(0,2,2,0);
  J(numberYield+4,5) = Dres1DDeltaEp(0,2,1,2)+Dres1DDeltaEp(0,2,2,1);
  for (int i=0; i< numberYield; i++)
  {
    J(numberYield+4,6+i) = Dres1DDeltaPlasticMult[i](0,2);
  }
  J(numberYield+4,6+numberYield) = Dres1DDeltaHatP(0,2);
  
  J(numberYield+5,0) = Dres1DDeltaEp(1,2,0,0);
  J(numberYield+5,1) = Dres1DDeltaEp(1,2,1,1);
  J(numberYield+5,2) = Dres1DDeltaEp(1,2,2,2);
  J(numberYield+5,3) = Dres1DDeltaEp(1,2,0,1)+Dres1DDeltaEp(1,2,1,0);
  J(numberYield+5,4) = Dres1DDeltaEp(1,2,0,2)+Dres1DDeltaEp(1,2,2,0);
  J(numberYield+5,5) = Dres1DDeltaEp(1,2,1,2)+Dres1DDeltaEp(1,2,2,1);
  for (int i=0; i< numberYield; i++)
  {
    J(numberYield+5,6+i) = Dres1DDeltaPlasticMult[i](1,2);
  }
  J(numberYield+5,6+numberYield) = Dres1DDeltaHatP(1,2);
  
  // jacobian for res2
  J(numberYield+6,0) = Dres2DDeltaEp(0,0);
  J(numberYield+6,1) = Dres2DDeltaEp(1,1);
  J(numberYield+6,2) = Dres2DDeltaEp(2,2);
  J(numberYield+6,3) = Dres2DDeltaEp(0,1)+Dres2DDeltaEp(1,0);
  J(numberYield+6,4) = Dres2DDeltaEp(0,2)+Dres2DDeltaEp(2,0);
  J(numberYield+6,5) = Dres2DDeltaEp(1,2)+Dres2DDeltaEp(2,1);
  for (int i=0; i< numberYield; i++)
  {
    J(numberYield+6,6+i) = Dres2DDeltaPlasticMult[i];
  }
  J(numberYield+6,6+numberYield) = Dres2DDeltaHatP;
  
  //res.print("res");
  //J.print("jacobian");
};

void mlawNonLocalPorosity::I1J2J3_updatePlasticState(IPNonLocalPorosity *q1, const IPNonLocalPorosity* q0,  const STensor3& F1, const STensor3& kcor,
                            const double R, const STensor3& DeltaEp,  const double DeltaHatP, const double H,
                            STensor3& Fe1, STensor3& Ce, STensor3& Ee, STensor43& Le, STensor63& dLe, STensor43& DFpDDeltaEp, const double* T) const{
                              
  // update plastic deformation
  STensor3& Fp1 = q1->getRefToFp();
   // Plastic increment
  static STensor3 dFp;
  static STensor43 Dexp;
  // dFp = exp(DeltaEp)
  STensorOperation::expSTensor3(DeltaEp,_orderlogexp,dFp, &Dexp);
  // Fp1 = dFp * Fp0
  const STensor3& Fp0 = q0->getConstRefToFp();
  STensorOperation::multSTensor3(dFp,Fp0,Fp1);
  //
  for (int i=0; i<3; i++)
  {
    for (int j=0; j<3; j++)
    {
      for (int p=0; p<3; p++)
      {
        for (int q=0; q<3; q++)
        {
          DFpDDeltaEp(i,j,p,q) = 0.;
          for (int r=0; r<3; r++)
          {
            DFpDDeltaEp(i,j,p,q) += Dexp(i,r,p,q)*Fp0(r,j);
          }
        }
      }
    }
  }
  
  // Fe1 = F1 * Fp1^-T
  static STensor3 Fp1_inv;
  STensorOperation::inverseSTensor3(Fp1, Fp1_inv);
  STensorOperation::multSTensor3(F1,Fp1_inv,Fe1);
  // Ce = Fe1^T * Fe1
  STensorOperation::multSTensor3FirstTranspose(Fe1,Fe1,Ce);
  // Ee = ln(sqrt(Ce))
  STensorOperation::logSTensor3(Ce,_orderlogexp,Ee,&Le,&dLe);
  Ee *= 0.5;

  // dissipation energy
  q1->getRefToPlasticEnergy() = q0->plasticEnergy()+ STensorOperation::doubledot(kcor,DeltaEp);
};

bool mlawNonLocalPorosity::I1J2J3_plasticCorrector_NonLocal(const STensor3& F1, 
                                  const STensor3& Kcorpr,  // predictor 
                                  const IPNonLocalPorosity *q0, IPNonLocalPorosity *q1,
                                  STensor3& Fe, STensor3& Ce, STensor3& Ee, STensor43& Le, STensor63& dLe,
                                  const bool stiff,
                                  const STensor43& DKcorprDEepr, // predictor elastic tangent
                                  STensor43& DKcorDEepr, std::vector<STensor3>& DKcorDnonlocalVar,
                                  STensor43& DFpDEepr, std::vector<STensor3>& DFpDnonlocalVar,
                                  std::vector<STensor3>& DlocalVarDEepr, fullMatrix<double>& DlocalVarDnonlocalVar,
                                  const double* T,
                                  const STensor3* DKcorprDT, 
                                  STensor3* DKcorDT, STensor3* DFpDT, fullMatrix<double>* DlocalVarDT
                                ) const{
                                  
  STensor3& Kcor = q1->getRefToCorotationalKirchhoffStress();
  Kcor = Kcorpr;
  
  static STensor3 sig;
  static STensor43 DsigDDeltaEp;
  sig = Kcor;
  STensorOperation::scale(DKcorprDEepr,-1.,DsigDDeltaEp);
  double detF = 1.;
  double detFinv = 1.;
  if (_stressFormulation == CORO_CAUCHY)
  {
    // update with deformation Jacobian
    detF = STensorOperation::determinantSTensor3(F1);
    detFinv = 1./detF;
    sig *= detFinv;
    DsigDDeltaEp *= detFinv;
  }
                                  
  // get all void characteristic
  static std::vector<double> Y(4,0.);
  static std::vector<STensor3> DYDsig(4,STensor3(0.));
  static std::vector<std::vector<double> > DYDNonlocalVars(4,std::vector<double>(3,0.));
  I1J2J3_voidCharacteristicsEvolution_NonLocal(sig,q0,q1,true,&Y,&DYDsig,&DYDNonlocalVars);
  
  double & R = q1->getRefToCurrentViscoplasticYieldStress();
  R = q1->getConstRefToIPJ2IsotropicHardening().getR();
  double H = q1->getConstRefToIPJ2IsotropicHardening().getDR();
  
  // unknowns
  int nyield = getNumOfYieldSurfaces();
  static STensor3 DeltaEp;
  STensorOperation::zero(DeltaEp);
  static std::vector<double> DeltaPlasticMult(nyield);
  for (int i=0; i< nyield; i++)
  {
    DeltaPlasticMult[i] = 0.;
  }
  double DeltaHatP = 0;
  
  
  static std::vector<double> res0(nyield);
  static std::vector<STensor3> Dres0DDeltaEp(nyield);
  static std::vector<double> Dres0DDeltaPlasticMult(nyield);
  static std::vector<double> Dres0DDeltaHatP(nyield);
  
  static STensor3 res1;
  static STensor43 Dres1DDeltaEp;
  static std::vector<STensor3> Dres1DDeltaPlasticMult(nyield);
  static STensor3 Dres1DDeltaHatP;
  
  static double res2;
  static STensor3 Dres2DDeltaEp;
  static std::vector<double> Dres2DDeltaPlasticMult(nyield);
  static double Dres2DDeltaHatP;
    

  static fullVector<double> res(7+nyield), sol(7+nyield);
  static fullMatrix<double> Jac(7+nyield,7+nyield);
  
  I1J2J3_computeResidual(DeltaEp,DeltaPlasticMult,DeltaHatP,sig,R,q0,q1,Y,
        res0,res1,res2,
        true,DsigDDeltaEp,H,DYDsig,
        Dres0DDeltaEp,Dres1DDeltaEp,Dres2DDeltaEp,
        Dres0DDeltaPlasticMult,Dres1DDeltaPlasticMult,Dres2DDeltaPlasticMult,
        Dres0DDeltaHatP, Dres1DDeltaHatP,Dres2DDeltaHatP,T);
        
  // compute solution
  I1J2J3_getResAndJacobian(res,Jac,res0,res1,res2,
                  Dres0DDeltaEp,Dres1DDeltaEp,Dres2DDeltaEp,
                  Dres0DDeltaPlasticMult,Dres1DDeltaPlasticMult,Dres2DDeltaPlasticMult,
                  Dres0DDeltaHatP,Dres1DDeltaHatP,Dres2DDeltaHatP);
  //Jac.print("Jac");
  
  int ite = 0;
  int lsMax = 10;
  
  double f = res.norm();
  // Start iterations
  //Msg::Info("plastic corrector f=%e",f);
  while(f > _tol or ite <1)
  {
                                    
    bool ok = Jac.luSolve(res,sol);
    
    if (!ok) {
      #ifdef _DEBUG
      Msg::Error("lu solve does not work !!!");
      #endif //_DEBUG
      return false;
    }
    
    double alph=1.;
    static STensor3 DeltaEp_prev;
    DeltaEp_prev = DeltaEp;
    static std::vector<double> DeltaPlasticMult_prev(nyield);
    DeltaPlasticMult_prev = DeltaPlasticMult;    
    double DeltaHatP_prev = DeltaHatP;
  
    for (int ls =0;  ls < lsMax; ls++)
    {
    
      static STensor3 dDeltaEp; 
      dDeltaEp(0,0) = alph*sol(0);
      dDeltaEp(1,1) = alph*sol(1);
      dDeltaEp(2,2) = alph*sol(2);
      dDeltaEp(0,1) = alph*sol(3);
      dDeltaEp(1,0) = alph*sol(3);
      dDeltaEp(0,2) = alph*sol(4);
      dDeltaEp(2,0) = alph*sol(4);
      dDeltaEp(1,2) = alph*sol(5);
      dDeltaEp(2,1) = alph*sol(5);
      
      if (dDeltaEp.norm0() > 1.){
        DeltaEp*=0.1;
      }
      else{
        DeltaEp -= dDeltaEp;
      }
      
      for (int i=0; i< nyield; i++)
      {
        if (DeltaPlasticMult[i]-alph*sol(6+i) <0.){
          DeltaPlasticMult[i] *=0.1;
        }
        else{
          DeltaPlasticMult[i] -= alph*sol(6+i);
        }
      }
      
      if (DeltaHatP-alph*sol(6+nyield) < 0.){
        DeltaHatP *= 0.1;
      }
      else{
        DeltaHatP -= alph*sol(6+nyield);
      }
    
      // update Kcor
      // Kcor = Kcorpr - H:DeltaEp
      Kcor = Kcorpr;
      STensorOperation::multSTensor43STensor3Add(DKcorprDEepr,DeltaEp,-1.,Kcor);
      STensorOperation::scale(Kcor,detFinv,sig);
      
      // update with new sig
      // update all void characteristics
      I1J2J3_voidCharacteristicsEvolution_NonLocal(sig,q0,q1,true,&Y,&DYDsig,&DYDNonlocalVars);    
      // update internal variables
      I1J2J3_updateInternalVariables(q1,q0,DeltaEp,DeltaHatP,T);
      
      R = q1->getConstRefToIPJ2IsotropicHardening().getR();
      H = q1->getConstRefToIPJ2IsotropicHardening().getDR();
      
      I1J2J3_computeResidual(DeltaEp,DeltaPlasticMult,DeltaHatP,sig,R,q0,q1,Y,
          res0,res1,res2,
          true,DsigDDeltaEp,H,DYDsig,
          Dres0DDeltaEp,Dres1DDeltaEp,Dres2DDeltaEp,
          Dres0DDeltaPlasticMult,Dres1DDeltaPlasticMult,Dres2DDeltaPlasticMult,
          Dres0DDeltaHatP, Dres1DDeltaHatP,Dres2DDeltaHatP,T);
          
      // compute solution
      I1J2J3_getResAndJacobian(res,Jac,res0,res1,res2,
                      Dres0DDeltaEp,Dres1DDeltaEp,Dres2DDeltaEp,
                      Dres0DDeltaPlasticMult,Dres1DDeltaPlasticMult,Dres2DDeltaPlasticMult,
                      Dres0DDeltaHatP,Dres1DDeltaHatP,Dres2DDeltaHatP);   
      
      double newf = res.norm();
      if (newf < f)
      {	
        //if (ls > 0)
        //{
         // Msg::Info("ls ite = %d, alp = %e new = %e, f=%e",ls, alph, newf, f);
        //}
        f = newf;
        break;
      }
      else
      {
        alph *= 0.75;
        DeltaEp = DeltaEp_prev;
        DeltaPlasticMult = DeltaPlasticMult_prev;    
        DeltaHatP = DeltaHatP_prev;
      }
    }
      
    //printf("iter = %d error = %e \n ",ite,f);
    if (f < _tol)  break;
    
    ite++;
    if((ite > _maxite) or STensorOperation::isnan(f))
    {
      #ifdef _DEBUG
      printf("in mlawNonLocalPorosity::I1J2J3_plasticCorrector_NonLocal: no convergence for plastic correction  with ite = %d maxite = %d norm = %e chiPrev= %e chi = %e !!!\n",ite,_maxite,f,q0->getConstRefToIPVoidState().getVoidLigamentRatio(),q1->getConstRefToIPVoidState().getVoidLigamentRatio());
      #endif //_DEBUG
      return false;
    }
  }

  static STensor43 DFpDDeltaEp;
  I1J2J3_updatePlasticState(q1,q0,F1, Kcor, R, DeltaEp,DeltaHatP, H, Fe,Ce,Ee,Le,dLe,DFpDDeltaEp,T);
  /* Stiffness computation*/
  if(stiff)
  {
    double DRDT= 0.;
    if (isThermomechanicallyCoupled())
    {
      DRDT = q1->getConstRefToIPJ2IsotropicHardening().getDRDT();
    }
    double dwp=q1->getConstRefToIPJ2IsotropicHardening().getDWp();
    /* Compute internal variables derivatives from residual derivatives */
    static std::vector<STensor3> Dres0DEepr(nyield);
    static STensor43 Dres1DEepr;
    static STensor3 Dres2DEepr;
    static std::vector<std::vector<double> > Dres0DNonlocalVars(nyield, std::vector<double>(_numNonLocalVar,0.));
    static std::vector<STensor3> Dres1DNonlocalVars(_numNonLocalVar,STensor3(0.));
    static std::vector<double> Dres2DNonlocalVars(_numNonLocalVar,0.);
    
    static std::vector<double> Dres0DT(nyield);
    static STensor3 Dres1DT;
    static double Dres2DT;
    
    // Kcor = Kcorpr - H:DeltaEp, 
    // DkcorDEepr = DKcorprDEepr
    // partDKcorDT = DKcorprDT - DHDT:DeltaEp
    // sig = Kcor*invdetF
    static STensor43 correctedDsigDEepr;
    STensorOperation::scale(DKcorprDEepr,detFinv,correctedDsigDEepr);
    if (_stressFormulation == CORO_CAUCHY)
    {
      // J = Jepr*Jppr --> ln(J) = tr(Eepr) + ln(ppr) --> DJDEepr = J*I
      static STensor3 DdetFDEepr;
      STensorOperation::diag(DdetFDEepr,detF);
      STensorOperation::prodAdd(Kcor,DdetFDEepr,-detFinv*detFinv,correctedDsigDEepr);
    }
    
    
    static STensor3 correctedDKcorDT, correctedDsigDT;
    if (isThermomechanicallyCoupled())
    {
      static STensor43 DHDT;
      getDHookTensorDT(DHDT,*T);
      correctedDKcorDT = *DKcorprDT;
      STensorOperation::multSTensor43STensor3Add(DHDT,DeltaEp,-1.,correctedDKcorDT);
      correctedDsigDT = correctedDKcorDT;
      correctedDsigDT *= (detFinv);
    }
    
    I1J2J3_computeDResidual(DeltaEp,DeltaPlasticMult,DeltaHatP,sig, R,q0,q1,correctedDsigDEepr, H, dwp,Y,DYDsig,DYDNonlocalVars,
                    Dres0DNonlocalVars,Dres0DEepr,
                    Dres1DNonlocalVars,Dres1DEepr,
                    Dres2DNonlocalVars,Dres2DEepr,
                    T,&DRDT,&correctedDsigDT,
                    &Dres0DT,&Dres1DT,&Dres2DT);
                    
    
    static STensor43 DDeltaEPDEepr;
    static STensor3 DDeltaHatPDEepr;
    
    fullMatrix<double> invJac(8,8);
    bool isInverted = Jac.invert(invJac);
    if (!isInverted) 
    {
      #ifdef _DEBUG
      Msg::Error("Jacobian cannot be inverted");
      #endif //_DEBUG
      return false;
    }
    for(int i=0; i<3; i++)
    {
      for(int j=0; j<3; j++)
      {
        for (int k=0; k< nyield; k++)
        {
          res(k) = -Dres0DEepr[k](i,j);
        }
        res(nyield) = -Dres1DEepr(0,0,i,j);
        res(nyield+1) = -Dres1DEepr(1,1,i,j); 
        res(nyield+2) = -Dres1DEepr(2,2,i,j);
        res(nyield+3) = -Dres1DEepr(0,1,i,j);
        res(nyield+4) = -Dres1DEepr(0,2,i,j);
        res(nyield+5) = -Dres1DEepr(1,2,i,j);
        res(nyield+6) = -Dres2DEepr(i,j);
        invJac.mult(res,sol);
        DDeltaEPDEepr(0,0,i,j) = sol(0);
        DDeltaEPDEepr(1,1,i,j) = sol(1);
        DDeltaEPDEepr(2,2,i,j) = sol(2);
        DDeltaEPDEepr(0,1,i,j) = sol(3);
        DDeltaEPDEepr(1,0,i,j) = sol(3);
        DDeltaEPDEepr(0,2,i,j) = sol(4);
        DDeltaEPDEepr(2,0,i,j) = sol(4);
        DDeltaEPDEepr(1,2,i,j) = sol(5);
        DDeltaEPDEepr(2,1,i,j) = sol(5);
        DDeltaHatPDEepr(i,j) = sol(6+nyield);
      }
    }
    // local variables
    // double DeltaHatQ, DeltaHatD , DeltaHatP
    // DeltaHatQ = trace(DeltaEp)
    // DeltaHatD = sqrt(2*devDeltaEp*DevDeltaEp/3)
    
    static STensor3 devDeltaEp;
    double DeltaHatQ, DeltaHatD; // volumetric part pf DeltaEp
    STensorOperation::decomposeDevTr(DeltaEp,devDeltaEp,DeltaHatQ);
    DeltaHatD = sqrt(2.*STensorOperation::doubledot(devDeltaEp,devDeltaEp)/3.);

    // local Var are [DeltaHatQ DeltaHatP DeltaHatD]
    STensorOperation::multSTensor3STensor43(_I,DDeltaEPDEepr,DlocalVarDEepr[0]);
    DlocalVarDEepr[1] = DDeltaHatPDEepr;
    STensorOperation::multSTensor3STensor43(devDeltaEp,DDeltaEPDEepr,DlocalVarDEepr[2]);
    DlocalVarDEepr[2] *= (2./(3.*DeltaHatD));
    
    static std::vector<STensor3> DDeltaEpDNonlocalVars(_numNonLocalVar);
    static std::vector<double> DDeltaHatPDNonlocalVars(_numNonLocalVar);
    
    // nonlocal derivatives
    for (int i=0; i< _numNonLocalVar; i++){
      for (int k=0; k< nyield; k++){
        res(k) = -Dres0DNonlocalVars[k][i];
      }
      res(nyield) = -Dres1DNonlocalVars[i](0,0);
      res(nyield+1) = -Dres1DNonlocalVars[i](1,1);
      res(nyield+2) = -Dres1DNonlocalVars[i](2,2);
      res(nyield+3) = -Dres1DNonlocalVars[i](0,1);
      res(nyield+4) = -Dres1DNonlocalVars[i](0,2);
      res(nyield+5) = -Dres1DNonlocalVars[i](1,2);
      res(nyield+6) = -Dres2DNonlocalVars[i];
      
      invJac.mult(res,sol);
    
      DDeltaEpDNonlocalVars[i](0,0) = sol(0);
      DDeltaEpDNonlocalVars[i](1,1) = sol(1);
      DDeltaEpDNonlocalVars[i](2,2) = sol(2);
      DDeltaEpDNonlocalVars[i](0,1) = sol(3);
      DDeltaEpDNonlocalVars[i](1,0) = sol(3);
      DDeltaEpDNonlocalVars[i](0,2) = sol(4);
      DDeltaEpDNonlocalVars[i](2,0) = sol(4);
      DDeltaEpDNonlocalVars[i](1,2) = sol(5);
      DDeltaEpDNonlocalVars[i](2,1) = sol(5);
      DDeltaHatPDNonlocalVars[i] = sol(6+nyield);
      
      DlocalVarDnonlocalVar(0,i) = STensorOperation::doubledot(_I,DDeltaEpDNonlocalVars[i]);
      DlocalVarDnonlocalVar(1,i) = DDeltaHatPDNonlocalVars[i];
      DlocalVarDnonlocalVar(2,i) = STensorOperation::doubledot(devDeltaEp,DDeltaEpDNonlocalVars[i])*2./(3.*DeltaHatD);
    };
    
    // compute tangents
    // kcor = kcorpr - H:DeltaEp 
    DKcorDEepr = DKcorprDEepr;
    STensorOperation::multSTensor43Add(DKcorprDEepr,DDeltaEPDEepr,-1.,DKcorDEepr);
    for (int i=0; i< _numNonLocalVar; i++){
      STensorOperation::multSTensor43STensor3(DKcorprDEepr,DDeltaEpDNonlocalVars[i],DKcorDnonlocalVar[i],-1.);
    }
    // Fp = exp(DeltaEp)*Fp0
    STensorOperation::multSTensor43(DFpDDeltaEp,DDeltaEPDEepr,DFpDEepr);
    for (int i=0; i< _numNonLocalVar; i++){
      STensorOperation::multSTensor43STensor3(DFpDDeltaEp,DDeltaEpDNonlocalVars[i],DFpDnonlocalVar[i]);
    }
    
    // plastic energy DplasticEnegy = Kcor:DeltaEp
    STensor3& DplasticEnergyDF = q1->getRefToDPlasticEnergyDF();
    STensorOperation::multSTensor43STensor3(DKcorDEepr,DeltaEp,DplasticEnergyDF);
    STensorOperation::multSTensor3STensor43Add(Kcor,DDeltaEPDEepr,1.,DplasticEnergyDF);
    
    for (int nl=0; nl < _numNonLocalVar; nl++){
      q1->getRefToDPlasticEnergyDNonLocalVariable(nl) = STensorOperation::doubledot(DKcorDnonlocalVar[nl],DeltaEp)+STensorOperation::doubledot(Kcor,DDeltaEpDNonlocalVars[nl]);
    };
    
    if (isThermomechanicallyCoupled()){
      static STensor3 DDeltaEpDT;
      static double DDeltaHatPDT;
      
      // dtilde_fv
      for (int k=0; k< nyield; k++)
      {
        res(k) = -Dres0DT[k];
      }
      res(nyield) = -Dres1DT(0,0);
      res(nyield+1) = -Dres1DT(1,1);
      res(nyield+2) = -Dres1DT(2,2);
      res(nyield+3) = -Dres1DT(0,1);
      res(nyield+4) = -Dres1DT(0,2);
      res(nyield+5) = -Dres1DT(1,2);
      res(nyield+6) = -Dres2DT;
      
      invJac.mult(res,sol);
    
      DDeltaEpDT(0,0) = sol(0);
      DDeltaEpDT(1,1) = sol(1);
      DDeltaEpDT(2,2) = sol(2);
      DDeltaEpDT(0,1) = sol(3);
      DDeltaEpDT(1,0) = sol(3);
      DDeltaEpDT(0,2) = sol(4);
      DDeltaEpDT(2,0) = sol(4);
      DDeltaEpDT(1,2) = sol(5);
      DDeltaEpDT(2,1) = sol(5);
      DDeltaHatPDT = sol(6+nyield);
      
      (*DlocalVarDT)(0,0) = STensorOperation::doubledot(_I,DDeltaEpDT);
      (*DlocalVarDT)(1,0) = DDeltaHatPDT;
      (*DlocalVarDT)(2,0) = STensorOperation::doubledot(devDeltaEp,DDeltaEpDT)*2./(3.*DeltaHatD);
      
      // using Kcor = Kcorpr - HT:DeltaEp
      *DKcorDT = correctedDKcorDT;
      STensorOperation::multSTensor43STensor3Add(DKcorprDEepr,DDeltaEpDT,-1.,*DKcorDT);
      // for Fp
      STensorOperation::multSTensor43STensor3(DFpDDeltaEp,DDeltaEpDT,*DFpDT);
      // for plastic energy
      double& DplasticEnergyDT = q1->getRefToDPlasticEnergyDT();
      DplasticEnergyDT =  STensorOperation::doubledot(*DKcorDT,DeltaEp)+ STensorOperation::doubledot(Kcor,DDeltaEpDT);
    }
  };
  return true;
};

bool mlawNonLocalPorosity::I1J2J3_plasticCorrectorLocal(const STensor3& F1, 
                                  const STensor3& Kcorpr,  // predictor 
                                  const IPNonLocalPorosity *q0, IPNonLocalPorosity *q1,
                                  STensor3& Fe, STensor3& Ce, STensor3& Ee, STensor43& Le, STensor63& dLe,
                                  const bool stiff,
                                  const STensor43& DKcorprDEepr, // predictor elastic tangent
                                  STensor43& DKcorDEepr, STensor43& DFpDEepr,
                                  const double* T,
                                  const STensor3* DKcorprDT, 
                                  STensor3* DKcorDT, STensor3* DFpDT
                                ) const
{                                  
  STensor3& Kcor = q1->getRefToCorotationalKirchhoffStress();
  Kcor = Kcorpr;
  
  static STensor3 sig;
  static STensor43 DsigDDeltaEp;
  sig = Kcor;
  STensorOperation::scale(DKcorprDEepr,-1.,DsigDDeltaEp);
  double detF = 1.;
  double detFinv = 1.;
  if (_stressFormulation == CORO_CAUCHY)
  {
    // update with deformation Jacobian
    detF = STensorOperation::determinantSTensor3(F1);
    detFinv = 1./detF;
    sig *= detFinv;
    DsigDDeltaEp *= detFinv;
  }
  
  // unknowns
  int nyield = getNumOfYieldSurfaces();
  static STensor3 DeltaEp;
  STensorOperation::zero(DeltaEp);
  static std::vector<double> DeltaPlasticMult(nyield);
  for (int i=0; i< nyield; i++)
  {
    DeltaPlasticMult[i] = 0.;
  }
  double DeltaHatP = 0;
  
                                  
  // get all void characteristic
  static std::vector<double> Y(4,0.);
  static std::vector<STensor3> DYDsig(4,STensor3(0.));
  static std::vector<STensor3> DYDDeltaEp(4,STensor3(0.));
  static std::vector<double> DYDDeltaHatP(4,0.);
  
  I1J2J3_voidCharacteristicsEvolution_Local(sig,DeltaEp,DeltaHatP,q0,q1,true,&Y,&DYDsig,&DYDDeltaEp,&DYDDeltaHatP);
  
  double & R = q1->getRefToCurrentViscoplasticYieldStress();
  R = q1->getConstRefToIPJ2IsotropicHardening().getR();
  double H = q1->getConstRefToIPJ2IsotropicHardening().getDR();
  
  
  
  static std::vector<double> res0(nyield);
  static std::vector<STensor3> Dres0DDeltaEp(nyield);
  static std::vector<double> Dres0DDeltaPlasticMult(nyield);
  static std::vector<double> Dres0DDeltaHatP(nyield);
  
  static STensor3 res1;
  static STensor43 Dres1DDeltaEp;
  static std::vector<STensor3> Dres1DDeltaPlasticMult(nyield);
  static STensor3 Dres1DDeltaHatP;
  
  static double res2;
  static STensor3 Dres2DDeltaEp;
  static std::vector<double> Dres2DDeltaPlasticMult(nyield);
  static double Dres2DDeltaHatP;
    

  static fullVector<double> res(7+nyield), sol(7+nyield);
  static fullMatrix<double> Jac(7+nyield,7+nyield);
  
  I1J2J3_computeResidualLocal(DeltaEp,DeltaPlasticMult,DeltaHatP,sig,R,q0,q1,Y,
        res0,res1,res2,
        true,DsigDDeltaEp,H,DYDsig, DYDDeltaEp, DYDDeltaHatP,
        Dres0DDeltaEp,Dres1DDeltaEp,Dres2DDeltaEp,
        Dres0DDeltaPlasticMult,Dres1DDeltaPlasticMult,Dres2DDeltaPlasticMult,
        Dres0DDeltaHatP, Dres1DDeltaHatP,Dres2DDeltaHatP,T);
        
  // compute solution
  I1J2J3_getResAndJacobian(res,Jac,res0,res1,res2,
                  Dres0DDeltaEp,Dres1DDeltaEp,Dres2DDeltaEp,
                  Dres0DDeltaPlasticMult,Dres1DDeltaPlasticMult,Dres2DDeltaPlasticMult,
                  Dres0DDeltaHatP,Dres1DDeltaHatP,Dres2DDeltaHatP);
  //Jac.print("Jac");
  
  int ite = 0;
  
  double f = res.norm();
  // Start iterations
  //Msg::Info("plastic corrector f=%e",f);
  while(f > _tol or ite <1)
  {
                                    
    bool ok = Jac.luSolve(res,sol);
    
    if (!ok) {
      #ifdef _DEBUG
      Msg::Error("lu solve does not work !!!");
      #endif //_DEBUG
      return false;
    }
    static STensor3 dDeltaEp; 
    dDeltaEp(0,0) = sol(0);
    dDeltaEp(1,1) = sol(1);
    dDeltaEp(2,2) = sol(2);
    dDeltaEp(0,1) = sol(3);
    dDeltaEp(1,0) = sol(3);
    dDeltaEp(0,2) = sol(4);
    dDeltaEp(2,0) = sol(4);
    dDeltaEp(1,2) = sol(5);
    dDeltaEp(2,1) = sol(5);
    
    if (dDeltaEp.norm0() > 1.){
      DeltaEp*=0.1;
    }
    else{
      DeltaEp -= dDeltaEp;
    }
    
    for (int i=0; i< nyield; i++)
    {
      if (DeltaPlasticMult[i]-sol(6+i) <0.){
        DeltaPlasticMult[i] *=0.1;
      }
      else{
        DeltaPlasticMult[i] -= sol(6+i);
      }
    }
    
    if (DeltaHatP-sol(6+nyield) < 0.){
      DeltaHatP *= 0.1;
    }
    else{
      DeltaHatP -= sol(6+nyield);
    }
  
    // update Kcor
    // Kcor = Kcorpr - H:DeltaEp
    Kcor = Kcorpr;
    STensorOperation::multSTensor43STensor3Add(DKcorprDEepr,DeltaEp,-1.,Kcor);
    STensorOperation::scale(Kcor,detFinv,sig);
    
    // update with new sig
    // update all void characteristics
    I1J2J3_voidCharacteristicsEvolution_Local(sig,DeltaEp,DeltaHatP,q0,q1,true,&Y,&DYDsig,&DYDDeltaEp,&DYDDeltaHatP);    
    // update internal variables
    I1J2J3_updateInternalVariables(q1,q0,DeltaEp,DeltaHatP,T);
    
    R = q1->getConstRefToIPJ2IsotropicHardening().getR();
    H = q1->getConstRefToIPJ2IsotropicHardening().getDR();
    
    I1J2J3_computeResidualLocal(DeltaEp,DeltaPlasticMult,DeltaHatP,sig,R,q0,q1,Y,
        res0,res1,res2,
        true,DsigDDeltaEp,H,DYDsig,DYDDeltaEp,DYDDeltaHatP,
        Dres0DDeltaEp,Dres1DDeltaEp,Dres2DDeltaEp,
        Dres0DDeltaPlasticMult,Dres1DDeltaPlasticMult,Dres2DDeltaPlasticMult,
        Dres0DDeltaHatP, Dres1DDeltaHatP,Dres2DDeltaHatP,T);
        
    // compute solution
    I1J2J3_getResAndJacobian(res,Jac,res0,res1,res2,
                    Dres0DDeltaEp,Dres1DDeltaEp,Dres2DDeltaEp,
                    Dres0DDeltaPlasticMult,Dres1DDeltaPlasticMult,Dres2DDeltaPlasticMult,
                    Dres0DDeltaHatP,Dres1DDeltaHatP,Dres2DDeltaHatP);   
    
    f = res.norm();
    //printf("iter = %d error = %e \n ",ite,f);
    if (f < _tol)  break;
    
    ite++;
    if((ite > _maxite) or STensorOperation::isnan(f))
    {
      #ifdef _DEBUG
      printf("in mlawNonLocalPorosity::I1J2J3_plasticCorrector_NonLocal: no convergence for plastic correction  with ite = %d maxite = %d norm = %e chiPrev= %e chi = %e !!!\n",ite,_maxite,f,q0->getConstRefToIPVoidState().getVoidLigamentRatio(),q1->getConstRefToIPVoidState().getVoidLigamentRatio());
      #endif 
      return false;
    }
  }

  static STensor43 DFpDDeltaEp;
  I1J2J3_updatePlasticState(q1,q0,F1, Kcor, R, DeltaEp,DeltaHatP, H, Fe,Ce,Ee,Le,dLe,DFpDDeltaEp,T);
  /* Stiffness computation*/
  if(stiff)
  {
    double DRDT= 0.;
    if (isThermomechanicallyCoupled())
    {
      DRDT = q1->getConstRefToIPJ2IsotropicHardening().getDRDT();
    }
    double dwp=q1->getConstRefToIPJ2IsotropicHardening().getDWp();
    /* Compute internal variables derivatives from residual derivatives */
    static std::vector<STensor3> Dres0DEepr(nyield);
    static STensor43 Dres1DEepr;
    static STensor3 Dres2DEepr;
    static std::vector<double> Dres0DT(nyield);
    static STensor3 Dres1DT;
    static double Dres2DT;
    
    // Kcor = Kcorpr - H:DeltaEp, 
    // DkcorDEepr = DKcorprDEepr
    // partDKcorDT = DKcorprDT - DHDT:DeltaEp
    // sig = Kcor*invdetF
    static STensor43 correctedDsigDEepr;
    STensorOperation::scale(DKcorprDEepr,detFinv,correctedDsigDEepr);
    if (_stressFormulation == CORO_CAUCHY)
    {
      // J = Jepr*Jppr --> ln(J) = tr(Eepr) + ln(ppr) --> DJDEepr = J*I
      static STensor3 DdetFDEepr;
      STensorOperation::diag(DdetFDEepr,detF);
      STensorOperation::prodAdd(Kcor,DdetFDEepr,-detFinv*detFinv,correctedDsigDEepr);
    }
    
    
    static STensor3 correctedDKcorDT, correctedDsigDT;
    if (isThermomechanicallyCoupled())
    {
      static STensor43 DHDT;
      getDHookTensorDT(DHDT,*T);
      correctedDKcorDT = *DKcorprDT;
      STensorOperation::multSTensor43STensor3Add(DHDT,DeltaEp,-1.,correctedDKcorDT);
      correctedDsigDT = correctedDKcorDT;
      correctedDsigDT *= (detFinv);
    }
    
    I1J2J3_computeDResidualLocal(DeltaEp,DeltaPlasticMult,DeltaHatP,sig, R,q0,q1,correctedDsigDEepr, H,Y,DYDsig,
                    Dres0DEepr,Dres1DEepr,Dres2DEepr,
                    T,&DRDT,&correctedDsigDT,
                    &Dres0DT,&Dres1DT,&Dres2DT);
                    
    
    static STensor43 DDeltaEPDEepr;
    static STensor3 DDeltaHatPDEepr;
    
    fullMatrix<double> invJac(8,8);
    bool isInverted = Jac.invert(invJac);
    if (!isInverted) 
    {
      #ifdef _DEBUG
      Msg::Error("Jacobian cannot be inverted");
      #endif //_DEBUG
      return false;
    }
    for(int i=0; i<3; i++)
    {
      for(int j=0; j<3; j++)
      {
        for (int k=0; k< nyield; k++)
        {
          res(k) = -Dres0DEepr[k](i,j);
        }
        res(nyield) = -Dres1DEepr(0,0,i,j);
        res(nyield+1) = -Dres1DEepr(1,1,i,j); 
        res(nyield+2) = -Dres1DEepr(2,2,i,j);
        res(nyield+3) = -Dres1DEepr(0,1,i,j);
        res(nyield+4) = -Dres1DEepr(0,2,i,j);
        res(nyield+5) = -Dres1DEepr(1,2,i,j);
        res(nyield+6) = -Dres2DEepr(i,j);
        invJac.mult(res,sol);
        DDeltaEPDEepr(0,0,i,j) = sol(0);
        DDeltaEPDEepr(1,1,i,j) = sol(1);
        DDeltaEPDEepr(2,2,i,j) = sol(2);
        DDeltaEPDEepr(0,1,i,j) = sol(3);
        DDeltaEPDEepr(1,0,i,j) = sol(3);
        DDeltaEPDEepr(0,2,i,j) = sol(4);
        DDeltaEPDEepr(2,0,i,j) = sol(4);
        DDeltaEPDEepr(1,2,i,j) = sol(5);
        DDeltaEPDEepr(2,1,i,j) = sol(5);
        DDeltaHatPDEepr(i,j) = sol(6+nyield);
      }
    }
    
    // compute tangents
    // kcor = kcorpr - H:DeltaEp 
    DKcorDEepr = DKcorprDEepr;
    STensorOperation::multSTensor43Add(DKcorprDEepr,DDeltaEPDEepr,-1.,DKcorDEepr);
    // Fp = exp(DeltaEp)*Fp0
    STensorOperation::multSTensor43(DFpDDeltaEp,DDeltaEPDEepr,DFpDEepr);
    
    // plastic energy DplasticEnegy = Kcor:DeltaEp
    STensor3& DplasticEnergyDF = q1->getRefToDPlasticEnergyDF();
    STensorOperation::multSTensor43STensor3(DKcorDEepr,DeltaEp,DplasticEnergyDF);
    STensorOperation::multSTensor3STensor43Add(Kcor,DDeltaEPDEepr,1.,DplasticEnergyDF);
        
    if (isThermomechanicallyCoupled()){
      static STensor3 DDeltaEpDT;
      static double DDeltaHatPDT;
      
      // dtilde_fv
      for (int k=0; k< nyield; k++)
      {
        res(k) = -Dres0DT[k];
      }
      res(nyield) = -Dres1DT(0,0);
      res(nyield+1) = -Dres1DT(1,1);
      res(nyield+2) = -Dres1DT(2,2);
      res(nyield+3) = -Dres1DT(0,1);
      res(nyield+4) = -Dres1DT(0,2);
      res(nyield+5) = -Dres1DT(1,2);
      res(nyield+6) = -Dres2DT;
      
      invJac.mult(res,sol);
    
      DDeltaEpDT(0,0) = sol(0);
      DDeltaEpDT(1,1) = sol(1);
      DDeltaEpDT(2,2) = sol(2);
      DDeltaEpDT(0,1) = sol(3);
      DDeltaEpDT(1,0) = sol(3);
      DDeltaEpDT(0,2) = sol(4);
      DDeltaEpDT(2,0) = sol(4);
      DDeltaEpDT(1,2) = sol(5);
      DDeltaEpDT(2,1) = sol(5);
      DDeltaHatPDT = sol(6+nyield);
            
      // using Kcor = Kcorpr - HT:DeltaEp
      *DKcorDT = correctedDKcorDT;
      STensorOperation::multSTensor43STensor3Add(DKcorprDEepr,DDeltaEpDT,-1.,*DKcorDT);
      // for Fp
      STensorOperation::multSTensor43STensor3(DFpDDeltaEp,DDeltaEpDT,*DFpDT);
      // for plastic energy
      double& DplasticEnergyDT = q1->getRefToDPlasticEnergyDT();
      DplasticEnergyDT =  STensorOperation::doubledot(*DKcorDT,DeltaEp)+ STensorOperation::doubledot(Kcor,DDeltaEpDT);
    }
  };
  return true;
};

int mlawNonLocalPorosity::getNumOfYieldSurfaces() const
{
  return 1;
}

int mlawNonLocalPorosity::getNumOfVoidCharacteristics() const
{
  return 4; //fV, X, W, gamma
}
bool mlawNonLocalPorosity::I1J2J3_withPlastic(const STensor3& F, const STensor3& kcor, const double R, const IPNonLocalPorosity* q0, const IPNonLocalPorosity* q1, const double* T) const{
  if (getNumOfYieldSurfaces() > 1)
  {
    Msg::Error("multiple yield surface does not implemented with this law");
  }
  if (_stressFormulation == CORO_KIRCHHOFF)
  {
    return I1J2J3_yieldFunction(kcor,R,q0,q1,T) > _tol; 
  }
  else if (_stressFormulation == CORO_CAUCHY)
  {
    double J = STensorOperation::determinantSTensor3(F);
    static STensor3 sig;
    sig = kcor;
    sig *= (1./J);
    return I1J2J3_yieldFunction(sig,R,q0,q1,T) > _tol; 
  }
  else
  {
    Msg::Error("stress formuation is not correctly defined !!!");
    return false;
  }
};


void mlawNonLocalPorosity::I1J2J3_computeResidual(const STensor3& DeltaEp, const std::vector<double>& DeltaPlasticMult, const double DeltaHatP, // unknown value 
                                        const STensor3& sig,  const double R, const IPNonLocalPorosity* q0, const IPNonLocalPorosity* q1, 
                                        const std::vector<double>& Y, // void parameters                                                   
                                        std::vector<double>& res0, STensor3& res1, double& res2,  
                                        bool stiff, 
                                        const STensor43& DsigDDeltaEp, const double H,
                                        const std::vector<STensor3>& DYDsig, // stress -dependence
                                        std::vector<STensor3>& Dres0DDeltaEp, STensor43& Dres1DDeltaEp, STensor3& Dres2DDeltaEp,
                                        std::vector<double>& Dres0DDeltaPlasticMult, std::vector<STensor3>& Dres1DDeltaPlasticMult, std::vector<double>& Dres2DDeltaPlasticMult,
                                        std::vector<double>& Dres0DDeltaHatP, STensor3& Dres1DDeltaHatP, double& Dres2DDeltaHatP,
                                        const double* T) const{
  if (getNumOfYieldSurfaces() > 1)
  {
    Msg::Error("multiple yield surface does not implemented with this law");
  }
  double f0 = q1->getInitialPorosity();
  double R0 = _j2IH->getYield0();
  
  static STensor3 DyieldDsig;
  double DyieldDR;
  std::vector<double> dyieldDY(Y.size());
                                  
  res0[0] = I1J2J3_yieldFunction(sig,R,q0,q1,T,stiff,&DyieldDsig,&DyieldDR,&dyieldDY);
  
  static STensor3 Np;
  static STensor43 DNpDsig;
  static STensor3 DNpDR;
  static std::vector<STensor3> DNpDY(Y.size());
  I1J2J3_getYieldNormal(Np,sig,R,q0,q1,T,stiff,&DNpDsig,&DNpDR,&DNpDY);
    
  // plastic energy balance
  res1 = DeltaEp;
  res1.daxpy(Np,-DeltaPlasticMult[0]);
  
  double Jp = 1.;
  static STensor3 DJpDDeltaEp;
  // plastic energy balance
  if (_stressFormulation == CORO_KIRCHHOFF)
  {
    // we have
    // dot(Jp) = (Jp* Fp^{-T}):dot{Fp} = (Jp* Fp^{-T}): (Dp*Fp) = Jp*trace(Dp)
    // ln(Jp/Jp_0) = trace(DeltaEp)
    // Jp = Jp_0*exp(trace(DeltaEp))
    // note that R_{Kirchhoff} = Je*R_{Cauchy}
    // if no nucleation and shear
    // (1-f)Jp = 1-f0
    double Jp0 = STensorOperation::determinantSTensor3(q0->getConstRefToFp());
    double traceDeltaEp = DeltaEp(0,0)+DeltaEp(1,1)+DeltaEp(2,2);
    Jp = Jp0*exp(traceDeltaEp);
    if (stiff)
    {
      STensorOperation::diag(DJpDDeltaEp,Jp);
    }
  }
  
  double yieldf = q1->getYieldPorosity();
  res2 = (STensorOperation::doubledot(sig,DeltaEp) - (1.-yieldf)*Jp*R*DeltaHatP)/R0;
  
  if (stiff){
    // res0
    static STensor3 fullDFDsig;
    fullDFDsig = DyieldDsig;
    for (int i=0; i< Y.size(); i++)
    {
      fullDFDsig.daxpy(DYDsig[i],dyieldDY[i]);
    }
    STensorOperation::multSTensor3STensor43(fullDFDsig,DsigDDeltaEp,Dres0DDeltaEp[0]);
    Dres0DDeltaPlasticMult[0] = 0.;
    Dres0DDeltaHatP[0] = DyieldDR*H;
    
    // res1
    static STensor43 fullDNpDsig;
    fullDNpDsig = DNpDsig;
    for (int i=0; i< Y.size(); i++)
    {
      STensorOperation::prodAdd(DNpDY[i],DYDsig[i],1.,fullDNpDsig);
    }
    Dres1DDeltaEp = _I4;
    STensorOperation::multSTensor43Add(fullDNpDsig,DsigDDeltaEp,-DeltaPlasticMult[0],Dres1DDeltaEp);
    Dres1DDeltaPlasticMult[0] = Np;
    Dres1DDeltaPlasticMult[0] *= (-1.);
    Dres1DDeltaHatP = DNpDR;
    Dres1DDeltaHatP *= (-DeltaPlasticMult[0]*H);
    
    // res2 is  (sig:DeltaEp - (1.-yieldf)*Jp*R*DeltaHatP)/R0
    STensorOperation::scale(sig,1./R0,Dres2DDeltaEp);
    if (_stressFormulation == CORO_KIRCHHOFF)
    {
      double partDres2DJp = -(1.-yieldf)*R*DeltaHatP/R0;
      Dres2DDeltaEp.daxpy(DJpDDeltaEp,partDres2DJp);
    }
    
    double partDres2Dyieldf =Jp*R*DeltaHatP/R0;
    static STensor3 partDres2Dsig;
    STensorOperation::scale(DeltaEp,1./R0,partDres2Dsig);
    partDres2Dsig.daxpy(DYDsig[0],partDres2Dyieldf);
    STensorOperation::multSTensor3STensor43Add(partDres2Dsig,DsigDDeltaEp,1.,Dres2DDeltaEp);
    Dres2DDeltaPlasticMult[0] = 0.;
    Dres2DDeltaHatP = -(1.-yieldf)*Jp*(R+H*DeltaHatP)/R0;

  }
};

void mlawNonLocalPorosity::I1J2J3_computeResidualLocal(const STensor3& DeltaEp, const std::vector<double>& DeltaPlasticMult, const double DeltaHatP, // unknown value 
                                        const STensor3& sig,  const double R, const IPNonLocalPorosity* q0, const IPNonLocalPorosity* q1, 
                                        const std::vector<double>& Y, // void parameters                                                   
                                        std::vector<double>& res0, STensor3& res1, double& res2,  
                                        bool stiff, 
                                        const STensor43& DsigDDeltaEp, const double H,
                                        const std::vector<STensor3>& DYDsig, // stress -dependence
                                        const std::vector<STensor3>& DYDDeltaEp,
                                        const std::vector<double>& DYDDeltaHatP,
                                        std::vector<STensor3>& Dres0DDeltaEp, STensor43& Dres1DDeltaEp, STensor3& Dres2DDeltaEp,
                                        std::vector<double>& Dres0DDeltaPlasticMult, std::vector<STensor3>& Dres1DDeltaPlasticMult, std::vector<double>& Dres2DDeltaPlasticMult,
                                        std::vector<double>& Dres0DDeltaHatP, STensor3& Dres1DDeltaHatP, double& Dres2DDeltaHatP,
                                        const double* T) const{
  if (getNumOfYieldSurfaces() > 1)
  {
    Msg::Error("multiple yield surface does not implemented with this law");
  }
  double f0 = q1->getInitialPorosity();
  double R0 = _j2IH->getYield0();
  
  static STensor3 DyieldDsig;
  double DyieldDR;
  std::vector<double> dyieldDY(Y.size());
                                  
  res0[0] = I1J2J3_yieldFunction(sig,R,q0,q1,T,stiff,&DyieldDsig,&DyieldDR,&dyieldDY);
  
  static STensor3 Np;
  static STensor43 DNpDsig;
  static STensor3 DNpDR;
  static std::vector<STensor3> DNpDY(Y.size());
  I1J2J3_getYieldNormal(Np,sig,R,q0,q1,T,stiff,&DNpDsig,&DNpDR,&DNpDY);
    
  // plastic energy balance
  res1 = DeltaEp;
  res1.daxpy(Np,-DeltaPlasticMult[0]);
  
  double Jp = 1.;
  static STensor3 DJpDDeltaEp;
  // plastic energy balance
  if (_stressFormulation == CORO_KIRCHHOFF)
  {
    // we have
    // dot(Jp) = (Jp* Fp^{-T}):dot{Fp} = (Jp* Fp^{-T}): (Dp*Fp) = Jp*trace(Dp)
    // ln(Jp/Jp_0) = trace(DeltaEp)
    // Jp = Jp_0*exp(trace(DeltaEp))
    // note that R_{Kirchhoff} = Je*R_{Cauchy}
    // if no nucleation and shear
    // (1-f)Jp = 1-f0
    double Jp0 = STensorOperation::determinantSTensor3(q0->getConstRefToFp());
    double traceDeltaEp = DeltaEp(0,0)+DeltaEp(1,1)+DeltaEp(2,2);
    Jp = Jp0*exp(traceDeltaEp);
    if (stiff)
    {
      STensorOperation::diag(DJpDDeltaEp,Jp);
    }
  }
  
  double yieldf = q1->getYieldPorosity();
  res2 = (STensorOperation::doubledot(sig,DeltaEp) - (1.-yieldf)*Jp*R*DeltaHatP)/R0;
  
  if (stiff){
    // res0
    static STensor3 fullDFDsig;
    fullDFDsig = DyieldDsig;
    for (int i=0; i< Y.size(); i++)
    {
      fullDFDsig.daxpy(DYDsig[i],dyieldDY[i]);
    }
    STensorOperation::multSTensor3STensor43(fullDFDsig,DsigDDeltaEp,Dres0DDeltaEp[0]);
    for (int i=0; i< Y.size(); i++)
    {
      Dres0DDeltaEp[0].daxpy(DYDDeltaEp[i],dyieldDY[i]);
    }
    Dres0DDeltaPlasticMult[0] = 0.;
    Dres0DDeltaHatP[0] = DyieldDR*H;
    for (int i=0; i< Y.size(); i++)
    {
      Dres0DDeltaHatP[0] += (dyieldDY[i]*DYDDeltaHatP[i]);
    }
    
    // res1
    static STensor43 fullDNpDsig;
    fullDNpDsig = DNpDsig;
    for (int i=0; i< Y.size(); i++)
    {
      STensorOperation::prodAdd(DNpDY[i],DYDsig[i],1.,fullDNpDsig);
    }
    Dres1DDeltaEp = _I4;
    STensorOperation::multSTensor43Add(fullDNpDsig,DsigDDeltaEp,-DeltaPlasticMult[0],Dres1DDeltaEp);
    for (int i=0; i< Y.size(); i++)
    {
      STensorOperation::prodAdd(DNpDY[i],DYDDeltaEp[i],-DeltaPlasticMult[0],Dres1DDeltaEp);
    }
    Dres1DDeltaPlasticMult[0] = Np;
    Dres1DDeltaPlasticMult[0] *= (-1.);
    Dres1DDeltaHatP = DNpDR;
    Dres1DDeltaHatP *= (-DeltaPlasticMult[0]*H);
    for (int i=0; i< Y.size(); i++)
    {
      double ff=-DeltaPlasticMult[0]*DYDDeltaHatP[i];
      Dres1DDeltaHatP.daxpy(DNpDY[i],ff);
    }
    
    // res2 is  (sig:DeltaEp - (1.-yieldf)*Jp*R*DeltaHatP)/R0
    STensorOperation::scale(sig,1./R0,Dres2DDeltaEp);
    if (_stressFormulation == CORO_KIRCHHOFF)
    {
      double partDres2DJp = -(1.-yieldf)*R*DeltaHatP/R0;
      Dres2DDeltaEp.daxpy(DJpDDeltaEp,partDres2DJp);
    }
    
    double partDres2Dyieldf =Jp*R*DeltaHatP/R0;
    static STensor3 partDres2Dsig;
    STensorOperation::scale(DeltaEp,1./R0,partDres2Dsig);
    partDres2Dsig.daxpy(DYDsig[0],partDres2Dyieldf);
    STensorOperation::multSTensor3STensor43Add(partDres2Dsig,DsigDDeltaEp,1.,Dres2DDeltaEp);
    Dres2DDeltaEp.daxpy(DYDDeltaEp[0],partDres2Dyieldf);
    
    Dres2DDeltaPlasticMult[0] = 0.;
    Dres2DDeltaHatP = -(1.-yieldf)*Jp*(R+H*DeltaHatP)/R0;
    Dres2DDeltaHatP += partDres2Dyieldf*DYDDeltaHatP[0];
  }
};



void mlawNonLocalPorosity::I1J2J3_computeDResidual(const STensor3& DeltaEp, const std::vector<double>& DeltaPlasticMult, const double DeltaHatP, // unknown value 
                                  const STensor3& sig, const double R,  const IPNonLocalPorosity* q0, const IPNonLocalPorosity* q1,
                                  const STensor43& DsigDEepr, const double H, const double dwp,
                                  const std::vector<double>& Y, // void parameters  
                                  const std::vector<STensor3>& DYDsig,
                                  const std::vector<std::vector<double> >& DYDnonlocalVars, 
                                  std::vector<std::vector<double> >& Dres0DNonlocalVars, std::vector<STensor3>& Dres0DEepr,
                                  std::vector<STensor3>& Dres1DNonlocalVars, STensor43& Dres1DEepr,
                                  std::vector<double>& Dres2DNonlocalVars, STensor3& Dres2DEepr,
                                  const double* T, const double* DRDT, const STensor3* DsigDT,
                                  std::vector<double>* Dres0DT, STensor3* Dres1DT, double* Dres2DT) const{
  if (getNumOfYieldSurfaces() > 1)
  {
    Msg::Error("multiple yield surface does not implemented with this law");
  }
  double f0 = q1->getInitialPorosity();
  double R0 = _j2IH->getYield0();
  
  static STensor3 DyieldDsig;
  double DyieldDR, DyieldDT;
  std::vector<double> DyieldDY(Y.size(),0.);
  double yield = I1J2J3_yieldFunction(sig,R,q0,q1,T,true,&DyieldDsig,&DyieldDR,&DyieldDY,isThermomechanicallyCoupled(),&DyieldDT);
  
  // res0= F(sig,R,Y,T) = 0

  static STensor3 fullDyieldDsig;
  fullDyieldDsig = DyieldDsig;
  for (int i=0; i< Y.size(); i++)
  {
    fullDyieldDsig.daxpy(DYDsig[i],DyieldDY[i]);
  }
  STensorOperation::multSTensor3STensor43(fullDyieldDsig,DsigDEepr,Dres0DEepr[0]);
  for (int i=0; i< _numNonLocalVar; i++)
  {
    Dres0DNonlocalVars[0][i] = 0.;
    for (int j=0; j< Y.size(); j++)
    {
      Dres0DNonlocalVars[0][i] += DyieldDY[j]*DYDnonlocalVars[j][i];
    }
    if(i==1)
    {
      Dres0DNonlocalVars[0][i]-=DyieldDR*dwp;
    }
  }
  if (isThermomechanicallyCoupled()){
    (*Dres0DT)[0] = STensorOperation::doubledot(fullDyieldDsig,*DsigDT) + DyieldDR*(*DRDT) + DyieldDT;
  }
  
  static STensor3 Np;
  static STensor43 DNpDsig;
  static STensor3 DNpDR, DNpDT;
  static std::vector<STensor3> DNpDY(4,STensor3(0.));
  I1J2J3_getYieldNormal(Np,sig,R,q0,q1,T,true,&DNpDsig,&DNpDR,&DNpDY,isThermomechanicallyCoupled(),&DNpDT);
  
  // res1 = DeltaEp - DeltaPlasticMult*Np(kcor,R,Y,T) = 0
  static STensor43 fullDNpDsig;
  fullDNpDsig = DNpDsig;
  for (int i=0; i< Y.size(); i++)
  {
    STensorOperation::prodAdd(DNpDY[i],DYDsig[i],1.,fullDNpDsig);
  }  
  STensorOperation::multSTensor43(fullDNpDsig,DsigDEepr,Dres1DEepr);
  Dres1DEepr *= (-DeltaPlasticMult[0]);
    
  for (int i=0; i< _numNonLocalVar; i++){
    STensorOperation::zero(Dres1DNonlocalVars[i]);
    for (int j=0; j< Y.size(); j++)
    {
      Dres1DNonlocalVars[i].daxpy(DNpDY[j],-DeltaPlasticMult[0]*DYDnonlocalVars[j][i]);
    }
    if(i==1)
    {
      Dres1DNonlocalVars[i]+=(DNpDR*(DeltaPlasticMult[0]*dwp));
    }
  }

  if (isThermomechanicallyCoupled()){
    STensorOperation::multSTensor43STensor3(fullDNpDsig,*DsigDT,*Dres1DT);
    Dres1DT->daxpy(DNpDR,*DRDT);
    *Dres1DT += DNpDT;
    *Dres1DT *= (-DeltaPlasticMult[0]);
  };
  
  // res2 = (sig:DeltaEp - (1-f)*Jp*R*DeltaHatP)/R0 = 0
  double Jp = 1.;
  // plastic energy balance
  if (_stressFormulation == CORO_KIRCHHOFF)
  {
    double Jp0 = STensorOperation::determinantSTensor3(q0->getConstRefToFp());
    double traceDeltaEp = DeltaEp(0,0)+DeltaEp(1,1)+DeltaEp(2,2);
    Jp = Jp0*exp(traceDeltaEp);
  }
  
  double partDres2Dyieldf = Jp*R*DeltaHatP/R0;
  static STensor3 partDres2Dsig;
  STensorOperation::scale(DeltaEp,1./R0,partDres2Dsig);
  partDres2Dsig.daxpy(DYDsig[0],partDres2Dyieldf);
  double yieldf = q1->getYieldPorosity();
  
  STensorOperation::multSTensor3STensor43(partDres2Dsig,DsigDEepr,Dres2DEepr);
  for (int i=0; i< _numNonLocalVar; i++){
    Dres2DNonlocalVars[i] = partDres2Dyieldf*DYDnonlocalVars[0][i];
    if(i==1)
    {
      Dres2DNonlocalVars[i]-=(1.-yieldf)*Jp*(DeltaHatP)/R0*dwp;

    }
  }

  if (isThermomechanicallyCoupled()){
    *Dres2DT = STensorOperation::doubledot(partDres2Dsig,*DsigDT) - (1.-yieldf)*Jp*(*DRDT)*DeltaHatP/R0;;
  }
}; 

void mlawNonLocalPorosity::I1J2J3_computeDResidualLocal(const STensor3& DeltaEp, const std::vector<double>& DeltaPlasticMult, const double DeltaHatP, // unknown value 
                                  const STensor3& sig, const double R,  const IPNonLocalPorosity* q0, const IPNonLocalPorosity* q1,
                                  const STensor43& DsigDEepr, const double H, 
                                  const std::vector<double>& Y, // void parameters  
                                  const std::vector<STensor3>& DYDsig,
                                  std::vector<STensor3>& Dres0DEepr, STensor43& Dres1DEepr, STensor3& Dres2DEepr,
                                  const double* T, const double* DRDT, const STensor3* DsigDT,
                                  std::vector<double>* Dres0DT, STensor3* Dres1DT, double* Dres2DT) const{
  if (getNumOfYieldSurfaces() > 1)
  {
    Msg::Error("multiple yield surface does not implemented with this law");
  }
  double f0 = q1->getInitialPorosity();
  double R0 = _j2IH->getYield0();
  
  static STensor3 DyieldDsig;
  double DyieldDR, DyieldDT;
  std::vector<double> DyieldDY(Y.size(),0.);
  double yield = I1J2J3_yieldFunction(sig,R,q0,q1,T,true,&DyieldDsig,&DyieldDR,&DyieldDY,isThermomechanicallyCoupled(),&DyieldDT);
  
  // res0= F(sig,R,Y,T) = 0

  static STensor3 fullDyieldDsig;
  fullDyieldDsig = DyieldDsig;
  for (int i=0; i< Y.size(); i++)
  {
    fullDyieldDsig.daxpy(DYDsig[i],DyieldDY[i]);
  }
  STensorOperation::multSTensor3STensor43(fullDyieldDsig,DsigDEepr,Dres0DEepr[0]);
  if (isThermomechanicallyCoupled()){
    (*Dres0DT)[0] = STensorOperation::doubledot(fullDyieldDsig,*DsigDT) + DyieldDR*(*DRDT) + DyieldDT;
  }
  
  static STensor3 Np;
  static STensor43 DNpDsig;
  static STensor3 DNpDR, DNpDT;
  static std::vector<STensor3> DNpDY(4,STensor3(0.));
  I1J2J3_getYieldNormal(Np,sig,R,q0,q1,T,true,&DNpDsig,&DNpDR,&DNpDY,isThermomechanicallyCoupled(),&DNpDT);
  
  // res1 = DeltaEp - DeltaPlasticMult*Np(kcor,R,Y,T) = 0
  static STensor43 fullDNpDsig;
  fullDNpDsig = DNpDsig;
  for (int i=0; i< Y.size(); i++)
  {
    STensorOperation::prodAdd(DNpDY[i],DYDsig[i],1.,fullDNpDsig);
  }  
  STensorOperation::multSTensor43(fullDNpDsig,DsigDEepr,Dres1DEepr);
  Dres1DEepr *= (-DeltaPlasticMult[0]);

  if (isThermomechanicallyCoupled()){
    STensorOperation::multSTensor43STensor3(fullDNpDsig,*DsigDT,*Dres1DT);
    Dres1DT->daxpy(DNpDR,*DRDT);
    *Dres1DT += DNpDT;
    *Dres1DT *= (-DeltaPlasticMult[0]);
  };
  
  // res2 = (sig:DeltaEp - (1-f)*Jp*R*DeltaHatP)/R0 = 0
  double Jp = 1.;
  // plastic energy balance
  if (_stressFormulation == CORO_KIRCHHOFF)
  {
    double Jp0 = STensorOperation::determinantSTensor3(q0->getConstRefToFp());
    double traceDeltaEp = DeltaEp(0,0)+DeltaEp(1,1)+DeltaEp(2,2);
    Jp = Jp0*exp(traceDeltaEp);
  }
  
  double partDres2Dyieldf = Jp*R*DeltaHatP/R0;
  static STensor3 partDres2Dsig;
  STensorOperation::scale(DeltaEp,1./R0,partDres2Dsig);
  partDres2Dsig.daxpy(DYDsig[0],partDres2Dyieldf);
  double yieldf = q1->getYieldPorosity();
  STensorOperation::multSTensor3STensor43(partDres2Dsig,DsigDEepr,Dres2DEepr);
  if (isThermomechanicallyCoupled()){
    *Dres2DT = STensorOperation::doubledot(partDres2Dsig,*DsigDT) - (1.-yieldf)*Jp*(*DRDT)*DeltaHatP/R0;;
  }
}; 

void mlawNonLocalPorosity::I1J2J3_copyInternalVariableForElasticState(IPNonLocalPorosity *ipvcur, const IPNonLocalPorosity *ipvprev, const double T) const
{
  ipvcur->getRefToLocalPorosity() = ipvprev->getLocalPorosity();
  ipvcur->getRefToNonLocalPorosity() = ipvprev->getNonLocalPorosity();
  ipvcur->getRefToCorrectedPorosity() = ipvprev->getCorrectedPorosity();
  ipvcur->getRefToYieldPorosity() = ipvprev->getYieldPorosity();
  ipvcur->getRefToLocalLogarithmPorosity() = ipvprev->getLocalLogarithmPorosity();
  ipvcur->getRefToNonLocalLogarithmPorosity() = ipvprev->getNonLocalLogarithmPorosity();
  
  // Fp
  ipvcur->getRefToFp() = ipvprev->getConstRefToFp();
  // local 
  ipvcur->getRefToLocalMatrixPlasticStrain() = ipvprev->getLocalMatrixPlasticStrain();
  ipvcur->getRefToLocalVolumetricPlasticStrain() = ipvprev->getLocalVolumetricPlasticStrain();
  ipvcur->getRefToLocalDeviatoricPlasticStrain() = ipvprev->getLocalDeviatoricPlasticStrain();
  
  ipvcur->getRefToPlasticEnergy() = ipvprev->plasticEnergy();
  ipvcur->getRefToDissipationActive() = false;
  ipvcur->setFailed(ipvprev->isFailed());

    /* Non-local length law */
  if (_cLLaw.size() != 3){
    Msg::Error("three nonlocal length must be used");
  }

  _cLLaw[0]->computeCL(ipvprev->getNonLocalPorosity(), ipvcur->getRefToIPCLength(0));
  _cLLaw[1]->computeCL(ipvprev->getNonLocalPorosity(), ipvcur->getRefToIPCLength(1));
  _cLLaw[2]->computeCL(ipvprev->getNonLocalPorosity(), ipvcur->getRefToIPCLength(2));

  // update hardening
  if (isThermomechanicallyCoupled()){
    _j2IH->hardening(ipvprev->getLocalMatrixPlasticStrain(),ipvprev->getConstRefToIPJ2IsotropicHardening(),ipvcur->getLocalMatrixPlasticStrain(),T, ipvcur->getRefToIPJ2IsotropicHardening());
  }
  else{
    _j2IH->hardening(ipvprev->getLocalMatrixPlasticStrain(),ipvprev->getConstRefToIPJ2IsotropicHardening(),ipvcur->getLocalMatrixPlasticStrain(),ipvcur->getRefToIPJ2IsotropicHardening());
  }
  double wp=0.;
  double dwp=0.;
  getYieldDecrease(ipvcur->getNonLocalMatrixPlasticStrain(), wp, dwp);
  ipvcur->getRefToIPJ2IsotropicHardening().setScaleFactor(wp, dwp);
  
  // copy void state
  ipvcur->getRefToIPVoidState() = (ipvprev->getConstRefToIPVoidState());

  // coalescence from previous state
  ipvcur->getRefToIPCoalescence() = (ipvprev->getConstRefToIPCoalescence());
  //
   // no nucleation
  getNucleationLaw()->previousBasedRefresh( ipvcur->getRefToIPNucleation() , ipvprev->getConstRefToIPNucleation() );
}


void mlawNonLocalPorosity::I1J2J3_predictorCorrector_NonLocal(const STensor3& F0,         // initial deformation gradient (input @ time n)
                            const STensor3& F1,         // updated deformation gradient (input @ time n+1)
                            STensor3 &P,                // updated 1st Piola-Kirchhoff stress tensor (output)
                                                        // contains the initial values on input
                            const IPNonLocalPorosity *ipvprev,       // array of initial internal variable
                            IPNonLocalPorosity *ipvcur,             // updated array of internal variable (in ipvcur on output),
                            STensor43 &Tangent,         // constitutive tangents (output)
                            std::vector<STensor3>  &dLocalVarDF,
                            std::vector<STensor3>  &dStressDNonLocalVar,
                            fullMatrix<double>    &dLocalVarDNonLocalVar,
                            const bool stiff,                   // if true compute the tangents
                            STensor43& dFedF, std::vector<STensor3>& dFeDNonLocalVar,
                            STensor3*  dStressDT,
                            fullMatrix<double>* dLocalVarDT,
                            const double T0 ,const double T
                           ) const{

  // starting from elastic state
  I1J2J3_copyInternalVariableForElasticState(ipvcur,ipvprev,T);
  //
  double& R  = ipvcur->getRefToCurrentViscoplasticYieldStress();
  R= ipvcur->getConstRefToIPJ2IsotropicHardening().getR();
 
  const STensor3* Fp0 = &(ipvprev->getConstRefToFp());
  STensor3& Fp1 = ipvcur->getRefToFp();
  double& fV1 = ipvcur->getRefToLocalPorosity();
  STensor3& Ee = ipvcur->getRefToElasticDeformation();
  STensor3& corKir = ipvcur->getRefToCorotationalKirchhoffStress();

  // elastic predictor
  static STensor3 kcorprDev, Fe, Fepr, Ce, Cepr, Epr;
  static double ppr;
  static STensor43 Lepr, Le;
  static STensor63 dLepr, dLe;


  static STensor3 corKirpr;
  static STensor43 DcorKirprDEpr,DkcorprDevDEpr,DcorKirDEpr, DFpDEpr;
  static STensor3 DpprDEpr;
  static STensor3 DcorKirDT, DcorKirprDT, DkcorprDevDT;
  static STensor3 DFpDT;
  static double dpprDT;

  static std::vector<STensor3> DcorKirDNonLocalVar, DFpDNonLocalVar, DLocalVarDEpr;
  if (DcorKirDNonLocalVar.size() != _numNonLocalVar){
    DcorKirDNonLocalVar.resize(_numNonLocalVar);
    DFpDNonLocalVar.resize(_numNonLocalVar);
    DLocalVarDEpr.resize(_numNonLocalVar);
  };
  
  ipvcur->getRefToConstitutiveSuccessFlag() = true;

  // elastic predictor
  bool okElast = elasticPredictor(F1,*Fp0,corKirpr,kcorprDev,ppr,DcorKirprDEpr,DkcorprDevDEpr,DpprDEpr,Fepr,Cepr,Epr,Lepr,dLepr,true,
                  &T,&DcorKirprDT,&DkcorprDevDT,&dpprDT);
  if (!okElast)
  {
    P(0,0) = P(1,1) = P(2,2)= sqrt(-1.); // to exist NR and performed next step with a smaller incremenet
    ipvcur->getRefToConstitutiveSuccessFlag() = false;
    return;
  }
  // elastic only
  corKir = corKirpr;
  Fe = Fepr;
  Ce = Cepr;
  Ee = Epr;
  Le = Lepr;
  dLe = dLepr;

  if (stiff){
    STensorOperation::zero(DFpDEpr);
    for (int il = 0; il < _numNonLocalVar; il++){
      STensorOperation::zero(DcorKirDNonLocalVar[il]);
      STensorOperation::zero(DFpDNonLocalVar[il]);
      STensorOperation::zero(DLocalVarDEpr[il]);
      if (isThermomechanicallyCoupled()){
        (*dLocalVarDT)(il,0) = 0.;
      }
    }
    STensorOperation::zero(DFpDT);//--------added
    DcorKirDEpr = DcorKirprDEpr;
    DcorKirDT = DcorKirprDT;
    dLocalVarDNonLocalVar.setAll(0.);

    ipvcur->getRefToDPlasticEnergyDMatrixPlasticStrain() = 0.;
    STensorOperation::zero(ipvcur->getRefToDPlasticEnergyDF());
    if (isThermomechanicallyCoupled()){
      STensorOperation::zero(ipvcur->getRefToDPlasticEnergyDT());
    }
    for (int i=0; i< _numNonLocalVar; i++){
      STensorOperation::zero(ipvcur->getRefToDPlasticEnergyDNonLocalVariable(i));
    }
  }

  // predictor
  double kCorPrEq = sqrt(1.5*kcorprDev.dotprod());

  bool correctorOK = false;
  bool plastic = false;

  if (ipvcur->getNonLocalToLocal())
  {
    plastic = I1J2J3_withPlastic(F1,corKirpr,R,ipvprev,ipvcur,&T);
    if (plastic)
    {
      correctorOK = I1J2J3_plasticCorrectorLocal(F1,corKirpr,ipvprev,ipvcur,
                        Fe,Ce,Ee,Le,dLe,
                        stiff,DcorKirprDEpr,
                        DcorKirDEpr,DFpDEpr,
                        &T,&DcorKirprDT,&DcorKirDT,&DFpDT);


      if (!correctorOK and _withSubstepping){
        // Msg::Info("start substepping");
        // substepping devide by 0 untile converge
        int numSubStep = 2.;
        int numAttempt = 0;
        static STensor3 dF, Fcur;
        dF = F1;
        dF -= F0;
        double dT,Tcur; 
        dT = T - T0;
        static IPNonLocalPorosity ipvTemp(*dynamic_cast<const IPNonLocalPorosity*>(ipvprev));

        while (true){
          bool success = true;
          int iter = 0;
          ipvTemp.operator =(*dynamic_cast<const IPVariable*>(ipvprev)); // take privious value

          while (iter < numSubStep){

            iter++;
            double fact = ((double)iter)/(double)numSubStep;
            //Msg::Info("substepping step %d of %d fact = %e",iter,numSubStep,fact);
            bool estimateStiff = false;
            if (iter == numSubStep){
              estimateStiff = stiff;
            }
            // kinematic varaible
            Fcur = F0;
            Fcur.daxpy(dF,fact);
            Tcur = T0 + fact*dT;//------for temperature substepping
            ipvcur->setTemperature(Tcur);

            //elastic predictor
            Fp0 = &ipvTemp.getConstRefToFp();
            //elastic predictor
            okElast =  elasticPredictor(Fcur,*Fp0,corKirpr,kcorprDev,ppr,DcorKirprDEpr,DkcorprDevDEpr,DpprDEpr,Fepr,Cepr,Epr,Lepr,dLepr,estimateStiff,
                            &Tcur,&DcorKirprDT,&DkcorprDevDT,&dpprDT);
            if (!okElast)
            {
              ipvcur->getRefToConstitutiveSuccessFlag() = false;
              P(0,0) = P(1,1) = P(2,2)= sqrt(-1.); // to exist NR and performed next step with a smaller incremenet
              return;
            }
            //check yield condition
            plastic  =I1J2J3_withPlastic(Fcur,corKirpr,R,&ipvTemp,ipvcur,&T);

            if (plastic){
              //Msg::Info("plastic occurs");
              correctorOK = I1J2J3_plasticCorrectorLocal(F1,corKirpr,&ipvTemp,ipvcur,
                        Fe,Ce,Ee,Le,dLe,
                        estimateStiff,DcorKirprDEpr,
                        DcorKirDEpr,DFpDEpr,
                        &_Tref,&DcorKirprDT,&DcorKirDT,&DFpDT);
                        
							if (!correctorOK){
								success = false;
              	break;
							}
            }

						// next step
						if (iter < numSubStep){
              ipvTemp.operator =(*dynamic_cast<const IPVariable*>(ipvcur));
            }
          }
          if (!success){
            numSubStep *= 2;
            numAttempt++;
            // Msg::Warning("substeping is not converged, decrease number of step");
          }
          else{
            #ifdef _DEBUG
            Msg::Info("mlawNonLocalPorous::predictorCorrector: successful substepping with %d substeps", numSubStep);
            #endif //_DEBUG
						correctorOK = true;
            plastic = true;
            break;
          }
          if (numAttempt > _maxAttemptSubstepping){
            #ifdef _DEBUG
            Msg::Warning("mlawNonLocalPorous::predictorCorrector: failed substepping with %d substeps", numSubStep);
            #endif //_DEBUG
            correctorOK = false;
            plastic = true;
            break;
          }
        }
      }
    }
  }
  else{
    bool elasticFollowedBlocked = false;
    bool plasticFollowedBlocked = false;

    if (ipvcur->dissipationIsBlocked()){
      if (_postDamageBlockingBehavior == ELASTIC){
        elasticFollowedBlocked = true;
        plasticFollowedBlocked = false;
      }
      else if (_postDamageBlockingBehavior == ELASTOPLASTIC){
        elasticFollowedBlocked = false;
        plasticFollowedBlocked = true;
      }
      else{
        Msg::Error("method %d has not been implemented",_postDamageBlockingBehavior);
      }
    }

    if (!elasticFollowedBlocked){
      I1J2J3_voidCharacteristicsEvolution_NonLocal(corKirpr,ipvprev,ipvcur);
      // check with previous value
      plastic = I1J2J3_withPlastic(F1,corKirpr,R,ipvprev,ipvcur,&T);

      if (plastic){
        correctorOK = I1J2J3_plasticCorrector_NonLocal(F1,corKirpr,ipvprev,ipvcur,
                        Fe,Ce,Ee,Le,dLe,
                        stiff,DcorKirprDEpr,
                        DcorKirDEpr,DcorKirDNonLocalVar,
                        DFpDEpr,DFpDNonLocalVar,
                        DLocalVarDEpr,dLocalVarDNonLocalVar,
                        &T,&DcorKirprDT,&DcorKirDT,&DFpDT,dLocalVarDT);
        // subsetepping
        if (!correctorOK and _withSubstepping){
          #ifdef _DEBUG
          printf("start substepping\n");
          #endif //_DEBUG
          // substepping devide by 0 untile converge
          int numSubStep = 2.;
          int numAttempt = 0;
          static STensor3 dF, Fcur;
          dF = F1;
          dF -= F0;
          
          double DeltaHatQNonLocal = ipvcur->getNonLocalVolumetricPlasticStrain() - ipvprev->getNonLocalVolumetricPlasticStrain();
          double DeltaHatDNonLocal = ipvcur->getNonLocalDeviatoricPlasticStrain() - ipvprev->getNonLocalDeviatoricPlasticStrain();
          double DeltaHatPNonLocal = ipvcur->getNonLocalMatrixPlasticStrain() - ipvprev->getNonLocalMatrixPlasticStrain();

          double dT,Tcur; 
          dT = T - T0;
          static IPNonLocalPorosity* ipvTemp = static_cast<IPNonLocalPorosity*>(ipvprev->clone());
          
          while (true){
            bool success = true;
            int iter = 0;
            ipvTemp->operator =(*dynamic_cast<const IPVariable*>(ipvprev)); // take privious value

            while (iter < numSubStep){

              iter++;
              double fact = ((double)iter)/(double)numSubStep;
              //Msg::Info("substepping step %d of %d fact = %e",iter,numSubStep,fact);
              bool estimateStiff = false;
              if (iter == numSubStep){
                estimateStiff = stiff;
              }

              // modify all kinematic variable
              Fcur = F0;
              Fcur.daxpy(dF,fact);
							ipvcur->getRefToNonLocalVolumetricPlasticStrain() = ipvprev->getNonLocalVolumetricPlasticStrain() + fact*DeltaHatQNonLocal;
							ipvcur->getRefToNonLocalDeviatoricPlasticStrain() = ipvprev->getNonLocalDeviatoricPlasticStrain() + fact*DeltaHatDNonLocal;
							ipvcur->getRefToNonLocalMatrixPlasticStrain() = ipvprev->getNonLocalMatrixPlasticStrain() + fact*DeltaHatPNonLocal;
              Tcur = T0 + fact*dT;//------for temperature substepping
              ipvcur->setTemperature(Tcur);
              
              I1J2J3_copyInternalVariableForElasticState(ipvcur,ipvTemp,Tcur);
          
              //elastic predictor
              Fp0 = &ipvTemp->getConstRefToFp();
              okElast =  elasticPredictor(Fcur,*Fp0,corKirpr,kcorprDev,ppr,DcorKirprDEpr,DkcorprDevDEpr,DpprDEpr,Fepr,Cepr,Epr,Lepr,dLepr,estimateStiff,
                              &Tcur,&DcorKirprDT,&DkcorprDevDT,&dpprDT);
              if (!okElast)
              {
                ipvcur->getRefToConstitutiveSuccessFlag() = false;
                P(0,0) = P(1,1) = P(2,2)= sqrt(-1.); // to exist NR and performed next step with a smaller incremenet
                return;
              }
              //check yield condition
              I1J2J3_voidCharacteristicsEvolution_NonLocal(corKirpr,ipvTemp,ipvcur);

              plastic =I1J2J3_withPlastic(Fcur,corKirpr,R,ipvTemp,ipvcur,&T);

              if (plastic){
                correctorOK = I1J2J3_plasticCorrector_NonLocal(F1,corKirpr,ipvTemp,ipvcur,
                        Fe,Ce,Ee,Le,dLe,
                        stiff,DcorKirprDEpr,
                        DcorKirDEpr,DcorKirDNonLocalVar,
                        DFpDEpr,DFpDNonLocalVar,
                        DLocalVarDEpr,dLocalVarDNonLocalVar,
                        &T,&DcorKirprDT,&DcorKirDT,&DFpDT,dLocalVarDT);

								if (!correctorOK){
									success = false;
                	break;
								}
              }

              // next step
              if (iter < numSubStep){
                ipvTemp->operator =(*dynamic_cast<const IPVariable*>(ipvcur));
              }
            }
            if (!success){
              numSubStep *= 2;
              numAttempt++;
              #ifdef _DEBUG
              printf("substeping is not converged, decrease number of step\n");
              #endif //_DEBUG
            }
            else{
              #ifdef _DEBUG
              printf("substeping with success\n");
              #endif //DEBUG
							correctorOK = true;
							plastic = true;
              break;
            }
            if (numAttempt > _maxAttemptSubstepping){
              #ifdef _DEBUG
              printf("maximal attemp substepping is reached\n");
              #endif //_DEBUG
              correctorOK = false;
              plastic = true;
              break;
            }
          }
        }
      }
    }
  }

  if (plastic and !correctorOK){
    P(0,0) = sqrt(-1.); // to exist NR and performed next step with a smaller incremenet
    ipvcur->getRefToConstitutiveSuccessFlag() = false;
    return;
  }


  // update first PK stress
  static STensor3 S, FeS, invFp;
  STensorOperation::inverseSTensor3(Fp1,invFp);
  STensorOperation::multSTensor3STensor43(corKir,Le,S);
  STensorOperation::multSTensor3(Fe,S,FeS);
  STensorOperation::multSTensor3SecondTranspose(FeS,invFp,P);

  // elastic energy
  ipvcur->getRefToElasticEnergy()= this->deformationEnergy(Ce,&T);
  if (this->getMacroSolver()->withPathFollowing()){
    // irreversible energy
    if ((this->getMacroSolver()->getPathFollowingLocalIncrementType() == pathFollowingManager::PLASTIC_ENERGY) or
             (this->getMacroSolver()->getPathFollowingLocalIncrementType() == pathFollowingManager::DISSIPATION_ENERGY)) {
      ipvcur->getRefToIrreversibleEnergy() = ipvprev->irreversibleEnergy()+ ipvcur->plasticEnergy() - ipvprev->plasticEnergy();
    }
    else{
      ipvcur->getRefToIrreversibleEnergy() = 0.;
    }
  };

  ipvcur->getRefToDissipationActive() = plastic;

  if (stiff){
    static STensor3 invFp0;
    STensorOperation::inverseSTensor3(*Fp0,invFp0);
    static STensor43 EprToF;
    for (int i=0; i<3; i++){
      for (int j=0; j<3; j++){
        for (int k=0; k<3; k++){
          for (int l=0; l<3; l++){
            EprToF(i,j,k,l) = 0.;
            for (int p=0; p<3; p++){
              for (int q=0; q<3; q++){
                EprToF(i,j,k,l) += Lepr(i,j,p,q)*Fepr(k,p)*invFp0(l,q);
              }
            }
          }
        }
      }
    }

    if (ipvcur->getNonLocalToLocal()){
      if(!isThermomechanicallyCoupled()){
        tangentComputationLocal(Tangent, plastic,F1,corKir, S, Fepr,  *Fp0, Lepr,
                      Fe,Fp1,Le, dLe,
                      DcorKirDEpr, DFpDEpr, EprToF, invFp);
      }
      else {

        tangentComputationLocalWithTemperature(Tangent,*dStressDT, plastic,F1,corKir, S, Fepr,  *Fp0, Lepr,
                      Fe,Fp1,Le, dLe,
                      DcorKirDEpr,DcorKirDT, DFpDEpr,DFpDT, EprToF, invFp); //-----------added
      }
      for (int i=0; i< _numNonLocalVar; i++){
        STensorOperation::zero(dStressDNonLocalVar[i]);
      }
      dLocalVarDNonLocalVar.setAll(0.);
    }
    else{
      if(isThermomechanicallyCoupled()){
        tangentComputationWithTemperature(dFedF,dFeDNonLocalVar,Tangent,dStressDNonLocalVar, *dStressDT, dLocalVarDF,
                             plastic,F1,corKir, S, Fepr,  *Fp0, Lepr,
                             Fe,Fp1,Le, dLe,
                             DcorKirDEpr, DcorKirDNonLocalVar, DcorKirDT,DFpDEpr, DFpDNonLocalVar, DFpDT, DLocalVarDEpr,
                             EprToF, invFp);
      }
      else{
         tangentComputation(dFedF,dFeDNonLocalVar,Tangent,dStressDNonLocalVar, dLocalVarDF,
                             plastic,F1,corKir, S, Fepr,  *Fp0, Lepr,
                             Fe,Fp1,Le, dLe,
                             DcorKirDEpr, DcorKirDNonLocalVar,DFpDEpr, DFpDNonLocalVar, DLocalVarDEpr,
                             EprToF, invFp);
      }
    }

    //update tangent of plastic energy
    static STensor3 DplEnergyDEpr;
    DplEnergyDEpr = ipvcur->getConstRefToDPlasticEnergyDF();
    STensor3& DplEnergyDF = ipvcur->getRefToDPlasticEnergyDF();
    STensorOperation::multSTensor3STensor43(DplEnergyDEpr,EprToF,DplEnergyDF);

    if (this->getMacroSolver()->withPathFollowing()){
      // irreversible energy
      if ((this->getMacroSolver()->getPathFollowingLocalIncrementType() == pathFollowingManager::PLASTIC_ENERGY) or
               (this->getMacroSolver()->getPathFollowingLocalIncrementType() == pathFollowingManager::DISSIPATION_ENERGY)) {
        ipvcur->getRefToDIrreversibleEnergyDF() = ipvcur->getConstRefToDPlasticEnergyDF();
        for (int i=0; i< _numNonLocalVar; i++){
          if (ipvcur->getNonLocalToLocal()){
            ipvcur->getRefToDIrreversibleEnergyDNonLocalVariable(i) = 0.;
          }
          else{
            ipvcur->getRefToDIrreversibleEnergyDNonLocalVariable(i) = ipvcur->getConstRefToDPlasticEnergyDNonLocalVariable(i);
          }
        }
      }
      else{
        Msg::Error("Path following method is only contructed with dissipation based on plastic energy");
      }
    };
  }
};

void mlawNonLocalPorosity::I1J2J3_copyInternalVariableForElasticStateLocal(IPNonLocalPorosity *ipvcur, const IPNonLocalPorosity *ipvprev, const double T) const
{
  ipvcur->getRefToLocalPorosity() = ipvprev->getLocalPorosity();
  ipvcur->getRefToNonLocalPorosity() = ipvprev->getNonLocalPorosity();
  ipvcur->getRefToCorrectedPorosity() = ipvprev->getCorrectedPorosity();
  ipvcur->getRefToYieldPorosity() = ipvprev->getYieldPorosity();
  ipvcur->getRefToLocalLogarithmPorosity() = ipvprev->getLocalLogarithmPorosity();
  ipvcur->getRefToNonLocalLogarithmPorosity() = ipvprev->getNonLocalLogarithmPorosity();
  
  // Fp
  ipvcur->getRefToFp() = ipvprev->getConstRefToFp();
  // local 
  ipvcur->getRefToLocalMatrixPlasticStrain() = ipvprev->getLocalMatrixPlasticStrain();
  ipvcur->getRefToLocalVolumetricPlasticStrain() = ipvprev->getLocalVolumetricPlasticStrain();
  ipvcur->getRefToLocalDeviatoricPlasticStrain() = ipvprev->getLocalDeviatoricPlasticStrain();
  
  ipvcur->getRefToPlasticEnergy() = ipvprev->plasticEnergy();
  ipvcur->getRefToDissipationActive() = false;
  ipvcur->setFailed(ipvprev->isFailed());

  // update hardening
  if (isThermomechanicallyCoupled()){
    _j2IH->hardening(ipvprev->getLocalMatrixPlasticStrain(),ipvprev->getConstRefToIPJ2IsotropicHardening(),ipvcur->getLocalMatrixPlasticStrain(),T, ipvcur->getRefToIPJ2IsotropicHardening());
  }
  else{
    _j2IH->hardening(ipvprev->getLocalMatrixPlasticStrain(),ipvprev->getConstRefToIPJ2IsotropicHardening(),ipvcur->getLocalMatrixPlasticStrain(),ipvcur->getRefToIPJ2IsotropicHardening());
  }
  
  // copy void state
  ipvcur->getRefToIPVoidState() = (ipvprev->getConstRefToIPVoidState());

  // coalescence from previous state
  ipvcur->getRefToIPCoalescence() = (ipvprev->getConstRefToIPCoalescence());
  //
   // no nucleation
  getNucleationLaw()->previousBasedRefresh( ipvcur->getRefToIPNucleation() , ipvprev->getConstRefToIPNucleation() );
}

void mlawNonLocalPorosity::I1J2J3_predictorCorrectorLocal(
                            const STensor3& F0,         // initial deformation gradient (input @ time n)
                            const STensor3& F1,         // updated deformation gradient (input @ time n+1)
                            STensor3 &P,                // updated 1st Piola-Kirchhoff stress tensor (output)
                            const IPNonLocalPorosity *ipvprev,       // array of initial internal variable
                            IPNonLocalPorosity *ipvcur,             // updated array of internal variable (in ipvcur on output),
                            STensor43 &Tangent,         // constitutive tangents (output)
                            const bool stiff            // if true compute the tangents
                           ) const
{
  if (isThermomechanicallyCoupled()){
    Msg::Error("local model mlawNonLocalPorosity::I1J2J3_predictorCorrectorLocal has not been implemented with thermomechanics");
    Msg::Exit(0);
  }
  
   // starting from elastic state
  I1J2J3_copyInternalVariableForElasticStateLocal(ipvcur,ipvprev,_Tref);

	double& R = ipvcur->getRefToCurrentViscoplasticYieldStress();
	R = ipvcur->getConstRefToIPJ2IsotropicHardening().getR();

  const STensor3* Fp0 = &ipvprev->getConstRefToFp();
  STensor3& Fp1 = ipvcur->getRefToFp();
  double& fV1 = ipvcur->getRefToLocalPorosity();
  STensor3& Ee = ipvcur->getRefToElasticDeformation();
	STensor3& corKir = ipvcur->getRefToCorotationalKirchhoffStress();

  // elastic predictor
  static STensor3 kcorprDev, Fe, Fepr, Ce, Cepr, Epr;
  static double ppr;
  static STensor43 Lepr, Le;
  static STensor63 dLepr, dLe;

  static STensor3 corKirpr, DpprDEpr;
  static STensor43 DcorKirprDEpr,DkcorprDevDEpr,DcorKirDEpr, DFpDEpr;
  
  static STensor3 DcorKirDT, DcorKirprDT;
  static STensor3 DFpDT;

  bool correctorOK = false;
  bool plastic = false;
  ipvcur->getRefToConstitutiveSuccessFlag() = true;

  // elastic predictor
  bool okElast = this->elasticPredictor(F1,*Fp0,corKirpr,kcorprDev,ppr,DcorKirprDEpr,DkcorprDevDEpr,DpprDEpr,Fepr,Cepr,Epr,Lepr,dLepr,true);
  if (!okElast)
  {
    P(0,0) = P(1,1) = P(2,2)= sqrt(-1.); // to exist NR and performed next step with a smaller incremenet
    ipvcur->getRefToConstitutiveSuccessFlag() = false;
    return;
  }
  // elastic only
  corKir = corKirpr;
  Fe = Fepr;
  Ce = Cepr;
  Ee = Epr;
  Le = Lepr;
  dLe = dLepr;

  if (stiff){
    STensorOperation::zero(DFpDEpr);
    DcorKirDEpr = DcorKirprDEpr;

    ipvcur->getRefToDPlasticEnergyDMatrixPlasticStrain() = 0.;
    STensorOperation::zero(ipvcur->getRefToDPlasticEnergyDF());
    if (isThermomechanicallyCoupled()){
      STensorOperation::zero(ipvcur->getRefToDPlasticEnergyDT());
    }
  }

  if (!ipvcur->dissipationIsBlocked())
  {
    plastic = I1J2J3_withPlastic(F1,corKirpr,R,ipvprev,ipvcur,&_Tref);

    if (plastic)
    {
      correctorOK = I1J2J3_plasticCorrectorLocal(F1,corKirpr,ipvprev,ipvcur,
                        Fe,Ce,Ee,Le,dLe,
                        stiff,DcorKirprDEpr,
                        DcorKirDEpr,DFpDEpr,
                        &_Tref,&DcorKirprDT,&DcorKirDT,&DFpDT);


      if (!correctorOK and _withSubstepping){
        // Msg::Info("start substepping");
        // substepping devide by 0 untile converge
        int numSubStep = 2.;
        int numAttempt = 0;
        static STensor3 dF, Fcur;
        dF = F1;
        dF -= F0;
        static IPNonLocalPorosity ipvTemp(*dynamic_cast<const IPNonLocalPorosity*>(ipvprev));

        while (true){
          bool success = true;
          int iter = 0;
          ipvTemp.operator =(*dynamic_cast<const IPVariable*>(ipvprev)); // take privious value

          while (iter < numSubStep){

            iter++;
            double fact = ((double)iter)/(double)numSubStep;
            //Msg::Info("substepping step %d of %d fact = %e",iter,numSubStep,fact);
            bool estimateStiff = false;
            if (iter == numSubStep){
              estimateStiff = stiff;
            }
            // kinematic varaible
            Fcur = F0;
            Fcur.daxpy(dF,fact);

            //elastic predictor
            Fp0 = &ipvTemp.getConstRefToFp();
						// Fp
						ipvcur->getRefToFp() = ipvTemp.getConstRefToFp();
            okElast =  elasticPredictor(Fcur,*Fp0,corKirpr,kcorprDev,ppr,DcorKirprDEpr,DkcorprDevDEpr,DpprDEpr,Fepr,Cepr,Epr,Lepr,dLepr,estimateStiff);
            if (!okElast)
            {
              ipvcur->getRefToConstitutiveSuccessFlag() = false;
              P(0,0) = P(1,1) = P(2,2)= sqrt(-1.); // to exist NR and performed next step with a smaller incremenet
              return;
            }
            //check yield condition
            plastic  =I1J2J3_withPlastic(Fcur,corKirpr,R,&ipvTemp,ipvcur,&_Tref);

            if (plastic){
              //Msg::Info("plastic occurs");
              correctorOK = I1J2J3_plasticCorrectorLocal(F1,corKirpr,&ipvTemp,ipvcur,
                        Fe,Ce,Ee,Le,dLe,
                        estimateStiff,DcorKirprDEpr,
                        DcorKirDEpr,DFpDEpr,
                        &_Tref,&DcorKirprDT,&DcorKirDT,&DFpDT);
                        
							if (!correctorOK){
								success = false;
              	break;
							}
            }

						// next step
						if (iter < numSubStep){
              ipvTemp.operator =(*dynamic_cast<const IPVariable*>(ipvcur));
            }
          }
          if (!success){
            numSubStep *= 2;
            numAttempt++;
            // Msg::Warning("substeping is not converged, decrease number of step");
          }
          else{
            #ifdef _DEBUG
            Msg::Info("mlawNonLocalPorous::predictorCorrector: successful substepping with %d substeps", numSubStep);
            #endif //_DEBUG
						correctorOK = true;
            plastic = true;
            break;
          }
          if (numAttempt > _maxAttemptSubstepping){
            #ifdef _DEBUG
            Msg::Warning("mlawNonLocalPorous::predictorCorrector: failed substepping with %d substeps", numSubStep);
            #endif //_DEBUG
            correctorOK = false;
            plastic = true;
            break;
          }
        }
      }
    }
    if (plastic and !correctorOK){
      P(0,0) = sqrt(-1.); // to exist NR and performed next step with a smaller incremenet
      ipvcur->getRefToConstitutiveSuccessFlag() = false;
      return;
    }
  }

  // update first PK stress
  static STensor3 S, FeS, invFp;
  STensorOperation::inverseSTensor3(Fp1,invFp);
  STensorOperation::multSTensor3STensor43(corKir,Le,S);
  STensorOperation::multSTensor3(Fe,S,FeS);
  STensorOperation::multSTensor3SecondTranspose(FeS,invFp,P);

  // elastic energy
  ipvcur->getRefToElasticEnergy()= this->deformationEnergy(Ce);
  if (this->getMacroSolver()->withPathFollowing()){
    // irreversible energy
    if ((this->getMacroSolver()->getPathFollowingLocalIncrementType() == pathFollowingManager::PLASTIC_ENERGY) or
             (this->getMacroSolver()->getPathFollowingLocalIncrementType() == pathFollowingManager::DISSIPATION_ENERGY)) {
      ipvcur->getRefToIrreversibleEnergy() = ipvprev->irreversibleEnergy()+ ipvcur->plasticEnergy() - ipvprev->plasticEnergy();
    }
    else{
      ipvcur->getRefToIrreversibleEnergy() = 0.;
    }
  };

  ipvcur->getRefToDissipationActive() = plastic;

  if (stiff){
    static STensor3 invFp0;
    STensorOperation::inverseSTensor3(*Fp0,invFp0);
    static STensor43 EprToF;
    for (int i=0; i<3; i++){
      for (int j=0; j<3; j++){
        for (int k=0; k<3; k++){
          for (int l=0; l<3; l++){
            EprToF(i,j,k,l) = 0.;
            for (int p=0; p<3; p++){
              for (int q=0; q<3; q++){
                EprToF(i,j,k,l) += Lepr(i,j,p,q)*Fepr(k,p)*invFp0(l,q);
              }
            }
          }
        }
      }
    }

    tangentComputationLocal(Tangent, plastic,F1,corKir, S, Fepr,  *Fp0, Lepr,
                      Fe,Fp1,Le, dLe,
                      DcorKirDEpr, DFpDEpr, EprToF, invFp);


    //update tangent of plastic energy
    static STensor3 DplEnergyDEpr;
    DplEnergyDEpr = ipvcur->getConstRefToDPlasticEnergyDF();
    STensor3& DplEnergyDF = ipvcur->getRefToDPlasticEnergyDF();
    STensorOperation::multSTensor3STensor43(DplEnergyDEpr,EprToF,DplEnergyDF);

    if (this->getMacroSolver()->withPathFollowing()){
      // irreversible energy
      if ((this->getMacroSolver()->getPathFollowingLocalIncrementType() == pathFollowingManager::PLASTIC_ENERGY) or
               (this->getMacroSolver()->getPathFollowingLocalIncrementType() == pathFollowingManager::DISSIPATION_ENERGY)) {
        ipvcur->getRefToDIrreversibleEnergyDF() = ipvcur->getConstRefToDPlasticEnergyDF();
      }
      else{
        Msg::Error("Path following method is only contructed with dissipation based on plastic energy");
      }
    };
  }
};


void mlawNonLocalPorosity::I1J2J3_constitutive_NonLocal(
                            const STensor3& F0,         // initial deformation gradient (input @ time n)
                            const STensor3& F1,         // updated deformation gradient (input @ time n+1)
                            STensor3 &P,                // updated 1st Piola-Kirchhoff stress tensor (output)
                            const IPNonLocalPorosity *ipvprev,       // array of initial internal variable
                            IPNonLocalPorosity *ipvcur,             // updated array of internal variable (in ipvcur on output),
                            STensor43 &Tangent,         // constitutive tangents (output)
                            std::vector<STensor3>  &dLocaldVarDStrain,
                            std::vector<STensor3>  &dStressDNonLocalVar,
                            fullMatrix<double>    &dLocalVarDNonLocalVar,
                            const bool stiff ,
                              STensor43* elasticTangent
                              ) const
{
  if(elasticTangent!=NULL) Msg::Error("mlawNonLocalPorosity elasticTangent not computed");
  static STensor43 dFedF;
  static std::vector<STensor3> dFeDNonLocalVar(this->getNumNonLocalVariables());

  if (stiff and _tangentByPerturbation){
    I1J2J3_predictorCorrector_NonLocal(F0,F1,P,ipvprev,ipvcur,Tangent,dLocaldVarDStrain,dStressDNonLocalVar,dLocalVarDNonLocalVar,false,dFedF,dFeDNonLocalVar);
    static STensor3 Fplus, Pplus;
    static IPNonLocalPorosity ipvcurPlus(*dynamic_cast<const IPNonLocalPorosity*>(ipvprev));
    
    ipvcurPlus.operator =(*dynamic_cast<const IPVariable*>(ipvcur));
    // perturbe F
    for (int i=0; i<3; i++){
      for (int j=0; j<3; j++){
        Fplus = (F1);
        Fplus(i,j) += _perturbationfactor;
        I1J2J3_predictorCorrector_NonLocal(F0,Fplus,Pplus,ipvprev,&ipvcurPlus,Tangent,dLocaldVarDStrain,dStressDNonLocalVar,dLocalVarDNonLocalVar,false,dFedF,dFeDNonLocalVar);
        for (int k=0; k<3; k++){
          for (int l=0; l<3; l++){
            Tangent(k,l,i,j) = (Pplus(k,l) - P(k,l))/(_perturbationfactor);
            dLocaldVarDStrain[0](i,j) = (ipvcurPlus.getLocalVolumetricPlasticStrain() - ipvcur->getLocalVolumetricPlasticStrain())/_perturbationfactor;
            dLocaldVarDStrain[1](i,j) = (ipvcurPlus.getLocalMatrixPlasticStrain() - ipvcur->getLocalMatrixPlasticStrain())/_perturbationfactor;
            dLocaldVarDStrain[2](i,j) = (ipvcurPlus.getLocalDeviatoricPlasticStrain() - ipvcur->getLocalDeviatoricPlasticStrain())/_perturbationfactor;
            if (this->getMacroSolver()->withPathFollowing()){
              ipvcur->getRefToDIrreversibleEnergyDF()(i,j) = (ipvcurPlus.plasticEnergy() - ipvcur->plasticEnergy())/_perturbationfactor;
            }
          }
        }
      }
    }

    // perturb on nonlocal var
    ipvcurPlus.operator =(*dynamic_cast<const IPVariable*>(ipvcur));
    ipvcurPlus.getRefToNonLocalVolumetricPlasticStrain() += _perturbationfactor;
    I1J2J3_predictorCorrector_NonLocal(F0,F1,Pplus,ipvprev,&ipvcurPlus,Tangent,dLocaldVarDStrain,dStressDNonLocalVar,dLocalVarDNonLocalVar,false,dFedF,dFeDNonLocalVar);
    for (int k=0; k<3; k++){
      for (int l=0; l<3; l++){
        dStressDNonLocalVar[0](k,l) = (Pplus(k,l) - P(k,l))/(_perturbationfactor);
        dLocalVarDNonLocalVar(0,0) = (ipvcurPlus.getLocalVolumetricPlasticStrain() - ipvcur->getLocalVolumetricPlasticStrain())/(_perturbationfactor);
        dLocalVarDNonLocalVar(1,0) = (ipvcurPlus.getLocalMatrixPlasticStrain() - ipvcur->getLocalMatrixPlasticStrain())/(_perturbationfactor);
        dLocalVarDNonLocalVar(2,0) = (ipvcurPlus.getLocalDeviatoricPlasticStrain() - ipvcur->getLocalDeviatoricPlasticStrain())/(_perturbationfactor);
        if (this->getMacroSolver()->withPathFollowing()){
          ipvcur->getRefToDIrreversibleEnergyDNonLocalVariable(0) =  (ipvcurPlus.plasticEnergy() - ipvcur->plasticEnergy())/(_perturbationfactor);
        }
      }
    }

    // perturb on nonlocal var
    ipvcurPlus.operator =(*dynamic_cast<const IPVariable*>(ipvcur));
    ipvcurPlus.getRefToNonLocalMatrixPlasticStrain() += _perturbationfactor;
    I1J2J3_predictorCorrector_NonLocal(F0,F1,Pplus,ipvprev,&ipvcurPlus,Tangent,dLocaldVarDStrain,dStressDNonLocalVar,dLocalVarDNonLocalVar,false,dFedF,dFeDNonLocalVar);
    for (int k=0; k<3; k++){
      for (int l=0; l<3; l++){
        dStressDNonLocalVar[1](k,l) = (Pplus(k,l) - P(k,l))/(_perturbationfactor);
        dLocalVarDNonLocalVar(0,1) = (ipvcurPlus.getLocalVolumetricPlasticStrain() - ipvcur->getLocalVolumetricPlasticStrain())/(_perturbationfactor);
        dLocalVarDNonLocalVar(1,1) = (ipvcurPlus.getLocalMatrixPlasticStrain() - ipvcur->getLocalMatrixPlasticStrain())/(_perturbationfactor);
        dLocalVarDNonLocalVar(2,1) = (ipvcurPlus.getLocalDeviatoricPlasticStrain() - ipvcur->getLocalDeviatoricPlasticStrain())/(_perturbationfactor);
        if (this->getMacroSolver()->withPathFollowing()){
          ipvcur->getRefToDIrreversibleEnergyDNonLocalVariable(1) =  (ipvcurPlus.plasticEnergy() - ipvcur->plasticEnergy())/(_perturbationfactor);
        }
      }
    }

    // perturb on nonlocal var
    ipvcurPlus.operator =(*dynamic_cast<const IPVariable*>(ipvcur));
    ipvcurPlus.getRefToNonLocalDeviatoricPlasticStrain() += _perturbationfactor;
    I1J2J3_predictorCorrector_NonLocal(F0,F1,Pplus,ipvprev,&ipvcurPlus,Tangent,dLocaldVarDStrain,dStressDNonLocalVar,dLocalVarDNonLocalVar,false,dFedF,dFeDNonLocalVar);
    for (int k=0; k<3; k++){
      for (int l=0; l<3; l++){
        dStressDNonLocalVar[2](k,l) = (Pplus(k,l) - P(k,l))/(_perturbationfactor);
        dLocalVarDNonLocalVar(0,2) = (ipvcurPlus.getLocalVolumetricPlasticStrain() - ipvcur->getLocalVolumetricPlasticStrain())/(_perturbationfactor);
        dLocalVarDNonLocalVar(1,2) = (ipvcurPlus.getLocalMatrixPlasticStrain() - ipvcur->getLocalMatrixPlasticStrain())/(_perturbationfactor);
        dLocalVarDNonLocalVar(2,2) = (ipvcurPlus.getLocalDeviatoricPlasticStrain() - ipvcur->getLocalDeviatoricPlasticStrain())/(_perturbationfactor);
        if (this->getMacroSolver()->withPathFollowing()){
          ipvcur->getRefToDIrreversibleEnergyDNonLocalVariable(2) =  (ipvcurPlus.plasticEnergy() - ipvcur->plasticEnergy())/(_perturbationfactor);
        }
      }
    }
  }
  else{
    //F1.print("F1");
    I1J2J3_predictorCorrector_NonLocal(F0,F1,P,ipvprev,ipvcur,Tangent,dLocaldVarDStrain,dStressDNonLocalVar,dLocalVarDNonLocalVar,stiff,dFedF,dFeDNonLocalVar);
    //Tangent.print("K");
  }
};

void mlawNonLocalPorosity::I1J2J3_constitutive(
                            const STensor3& F0,         // initial deformation gradient (input @ time n)
                            const STensor3& F1,         // updated deformation gradient (input @ time n+1)
                            STensor3 &P,                // updated 1st Piola-Kirchhoff stress tensor (output)
                            const IPVariable *_ipvprev,       // array of initial internal variable
                            IPVariable *_ipvcur,             // updated array of internal variable (in ipvcur on output),
                            STensor43 &Tangent,         // constitutive tangents (output)
                            const bool stiff ,
                            STensor43* elasticTangent, 
                            const bool dTangentdeps,
                            STensor63* dCalgdeps) const
{
  if(elasticTangent!=NULL) Msg::Error("mlawNonLocalPorosity elasticTangent not computed");
  const IPNonLocalPorosity *ipvprev = dynamic_cast<const IPNonLocalPorosity *> (_ipvprev);       // array of initial internal variable
  IPNonLocalPorosity *ipvcur = dynamic_cast<IPNonLocalPorosity *> (_ipvcur);
 
  if (stiff and _tangentByPerturbation){
    I1J2J3_predictorCorrectorLocal(F0,F1,P,ipvprev,ipvcur,Tangent,false);
    static STensor3 Fplus, Pplus;
    static IPNonLocalPorosity ipvcurPlus(*dynamic_cast<const IPNonLocalPorosity*>(ipvprev));
    
    ipvcurPlus.operator =(*dynamic_cast<const IPVariable*>(ipvprev));
    // perturbe F
    for (int i=0; i<3; i++){
      for (int j=0; j<3; j++){
        Fplus = (F1);
        Fplus(i,j) += _perturbationfactor;
        I1J2J3_predictorCorrectorLocal(F0,Fplus,Pplus,ipvprev,&ipvcurPlus,Tangent,false);
        for (int k=0; k<3; k++){
          for (int l=0; l<3; l++){
            Tangent(k,l,i,j) = (Pplus(k,l) - P(k,l))/(_perturbationfactor);
          }
        }
				if (this->getMacroSolver()->withPathFollowing()){
					ipvcur->getRefToDIrreversibleEnergyDF()(i,j) = (ipvcurPlus.plasticEnergy() - ipvcur->plasticEnergy())/_perturbationfactor;
				}
      }
    }
  }
  else{
    I1J2J3_predictorCorrectorLocal(F0,F1,P,ipvprev,ipvcur,Tangent,stiff);
  }
};

bool mlawNonLocalPorosity::I1J2J3_withPlastic_aniso(const STensor3& F, const STensor3& kcor, const double R, const IPNonLocalPorosity* q0, const IPNonLocalPorosity* q1, const double* T) const
{
  if (getNumOfYieldSurfaces() > 1)
  {
    Msg::Error("multiple yield surface does not implemented with this law");
  }
  if (_stressFormulation == CORO_KIRCHHOFF)
  {
    return I1J2J3_yieldFunction_aniso(kcor,R,q0,q1,T) > _tol; 
  }
  else if (_stressFormulation == CORO_CAUCHY)
  {
    double J = STensorOperation::determinantSTensor3(F);
    static STensor3 sig;
    sig = kcor;
    sig *= (1./J);
    return I1J2J3_yieldFunction_aniso(sig,R,q0,q1,T) > _tol; 
  }
  else
  {
    Msg::Error("stress formuation is not correctly defined !!!");
    return false;
  }
}

void mlawNonLocalPorosity::I1J2J3_computeResidual_aniso(const STensor3& DeltaEp, const std::vector<double>& DeltaPlasticMult, const double DeltaHatP, // unknown value 
                                        const STensor3& sig,  const double R, const IPNonLocalPorosity* q0, const IPNonLocalPorosity* q1, 
                                        const std::vector<double>& Y, // void parameters                                                   
                                        std::vector<double>& res0, STensor3& res1, double& res2,  
                                        bool stiff, 
                                        const STensor43& DsigDDeltaEp, const double H,
                                        const std::vector<STensor3>& DYDsig, // stress -dependence
                                        std::vector<STensor3>& Dres0DDeltaEp, STensor43& Dres1DDeltaEp, STensor3& Dres2DDeltaEp,
                                        std::vector<double>& Dres0DDeltaPlasticMult, std::vector<STensor3>& Dres1DDeltaPlasticMult, std::vector<double>& Dres2DDeltaPlasticMult,
                                        std::vector<double>& Dres0DDeltaHatP, STensor3& Dres1DDeltaHatP, double& Dres2DDeltaHatP,
                                        const double* T) const{
  if (getNumOfYieldSurfaces() > 1)
  {
    Msg::Error("multiple yield surface does not implemented with this law");
  }
  double f0 = q1->getInitialPorosity();
  double R0 = _j2IH->getYield0();
  
  static STensor3 DyieldDsig;
  double DyieldDR;
  std::vector<double> dyieldDY(Y.size());
  static STensor3 DyieldDDeltaEp;
  static STensor3 DyieldDFdefo;
                                  
  res0[0] = I1J2J3_yieldFunction_aniso(sig,R,q0,q1,T,stiff,&DyieldDsig,&DyieldDR,&dyieldDY,&DyieldDDeltaEp,&DyieldDFdefo);
  
  static STensor3 Np;
  static STensor43 DNpDsig;
  static STensor3 DNpDR;
  static std::vector<STensor3> DNpDY(Y.size());
  static STensor43 DNpDDeltaEp;
  static STensor43 DNpDFdefo;
  I1J2J3_getYieldNormal_aniso(Np,sig,R,q0,q1,T,stiff,&DNpDsig,&DNpDR,&DNpDY,&DNpDDeltaEp,&DNpDFdefo);
    
  // plastic energy balance
  res1 = DeltaEp;
  res1.daxpy(Np,-DeltaPlasticMult[0]);
  
  double Jp = 1.;
  static STensor3 DJpDDeltaEp;
  // plastic energy balance
  if (_stressFormulation == CORO_KIRCHHOFF)
  {
    // we have
    // dot(Jp) = (Jp* Fp^{-T}):dot{Fp} = (Jp* Fp^{-T}): (Dp*Fp) = Jp*trace(Dp)
    // ln(Jp/Jp_0) = trace(DeltaEp)
    // Jp = Jp_0*exp(trace(DeltaEp))
    // note that R_{Kirchhoff} = Je*R_{Cauchy}
    // if no nucleation and shear
    // (1-f)Jp = 1-f0
    double Jp0 = STensorOperation::determinantSTensor3(q0->getConstRefToFp());
    double traceDeltaEp = DeltaEp(0,0)+DeltaEp(1,1)+DeltaEp(2,2);
    Jp = Jp0*exp(traceDeltaEp);
    if (stiff)
    {
      STensorOperation::diag(DJpDDeltaEp,Jp);
    }
  }
  
  double yieldf = q1->getYieldPorosity();
  res2 = (STensorOperation::doubledot(sig,DeltaEp) - (1.-yieldf)*Jp*R*DeltaHatP)/R0;
  
  if (stiff){
    // res0
    static STensor3 fullDFDsig;
    fullDFDsig = DyieldDsig;
    for (int i=0; i< Y.size(); i++)
    {
      fullDFDsig.daxpy(DYDsig[i],dyieldDY[i]);
    }
    STensorOperation::multSTensor3STensor43(fullDFDsig,DsigDDeltaEp,Dres0DDeltaEp[0]);
    Dres0DDeltaEp[0] += DyieldDDeltaEp;
    Dres0DDeltaPlasticMult[0] = 0.;
    Dres0DDeltaHatP[0] = DyieldDR*H;
    
    // res1
    static STensor43 fullDNpDsig;
    fullDNpDsig = DNpDsig;
    for (int i=0; i< Y.size(); i++)
    {
      STensorOperation::prodAdd(DNpDY[i],DYDsig[i],1.,fullDNpDsig);
    }
    Dres1DDeltaEp = _I4;
    STensorOperation::multSTensor43Add(fullDNpDsig,DsigDDeltaEp,-DeltaPlasticMult[0],Dres1DDeltaEp);
    Dres1DDeltaEp.axpy(-DeltaPlasticMult[0], DNpDDeltaEp);
    
    Dres1DDeltaPlasticMult[0] = Np;
    Dres1DDeltaPlasticMult[0] *= (-1.);
    Dres1DDeltaHatP = DNpDR;
    Dres1DDeltaHatP *= (-DeltaPlasticMult[0]*H);
    
    // res2 is  (sig:DeltaEp - (1.-yieldf)*Jp*R*DeltaHatP)/R0
    STensorOperation::scale(sig,1./R0,Dres2DDeltaEp);
    if (_stressFormulation == CORO_KIRCHHOFF)
    {
      double partDres2DJp = -(1.-yieldf)*R*DeltaHatP/R0;
      Dres2DDeltaEp.daxpy(DJpDDeltaEp,partDres2DJp);
    }
    
    double partDres2Dyieldf =Jp*R*DeltaHatP/R0;
    static STensor3 partDres2Dsig;
    STensorOperation::scale(DeltaEp,1./R0,partDres2Dsig);
    partDres2Dsig.daxpy(DYDsig[0],partDres2Dyieldf);
    STensorOperation::multSTensor3STensor43Add(partDres2Dsig,DsigDDeltaEp,1.,Dres2DDeltaEp);
    Dres2DDeltaPlasticMult[0] = 0.;
    Dres2DDeltaHatP = -(1.-yieldf)*Jp*(R+H*DeltaHatP)/R0;

  }
};

void mlawNonLocalPorosity::I1J2J3_computeDResidual_aniso(const STensor3& DeltaEp, const std::vector<double>& DeltaPlasticMult, const double DeltaHatP, // unknown value 
                                  const STensor3& sig, const double R,  const IPNonLocalPorosity* q0, const IPNonLocalPorosity* q1,
                                  const STensor43& DsigDEepr, const double H, const double dwp,
                                  const std::vector<double>& Y, // void parameters  
                                  const std::vector<STensor3>& DYDsig,
                                  const std::vector<std::vector<double> >& DYDnonlocalVars,  const STensor43& EprToF,
                                  std::vector<std::vector<double> >& Dres0DNonlocalVars, std::vector<STensor3>& Dres0DFdefo,
                                  std::vector<STensor3>& Dres1DNonlocalVars, STensor43& Dres1DFdefo,
                                  std::vector<double>& Dres2DNonlocalVars, STensor3& Dres2DFdefo,
                                  const double* T, const double* DRDT, const STensor3* DsigDT,
                                  std::vector<double>* Dres0DT, STensor3* Dres1DT, double* Dres2DT) const{
  if (getNumOfYieldSurfaces() > 1)
  {
    Msg::Error("multiple yield surface does not implemented with this law");
  }
  double f0 = q1->getInitialPorosity();
  double R0 = _j2IH->getYield0();
  int numberYield = getNumOfYieldSurfaces();
  
  static std::vector<STensor3> Dres0DEepr(numberYield, STensor3(0));
  static STensor43 Dres1DEepr;
  static STensor3 Dres2DEepr;
  
  static STensor3 DyieldDsig;
  double DyieldDR, DyieldDT;
  std::vector<double> DyieldDY(Y.size(),0.);
  static STensor3 DyieldDDeltaEp;
  static STensor3 DyieldDFdefo;
  
  double yield = I1J2J3_yieldFunction_aniso(sig,R,q0,q1,T,true,&DyieldDsig,&DyieldDR,&DyieldDY,&DyieldDDeltaEp,&DyieldDFdefo,isThermomechanicallyCoupled(),&DyieldDT);
  
  // res0= F(sig,R,Y,T) = 0

  static STensor3 fullDyieldDsig;
  fullDyieldDsig = DyieldDsig;
  for (int i=0; i< Y.size(); i++)
  {
    fullDyieldDsig.daxpy(DYDsig[i],DyieldDY[i]);
  }
  STensorOperation::multSTensor3STensor43(fullDyieldDsig,DsigDEepr,Dres0DEepr[0]);
  for (int i=0; i< _numNonLocalVar; i++)
  {
    Dres0DNonlocalVars[0][i] = 0.;
    for (int j=0; j< Y.size(); j++)
    {
      Dres0DNonlocalVars[0][i] += DyieldDY[j]*DYDnonlocalVars[j][i];
    }
    if(i==1)
    {
      Dres0DNonlocalVars[0][i]-=DyieldDR*dwp;
    }
  }
  if (isThermomechanicallyCoupled()){
    (*Dres0DT)[0] = STensorOperation::doubledot(fullDyieldDsig,*DsigDT) + DyieldDR*(*DRDT) + DyieldDT;
  }
  
  static STensor3 Np;
  static STensor43 DNpDsig;
  static STensor3 DNpDR, DNpDT;
  static std::vector<STensor3> DNpDY(Y.size(),STensor3(0.));
  static STensor43 DNpDDeltaEp;
  static STensor43 DNpDFdefo;
  
  I1J2J3_getYieldNormal_aniso(Np,sig,R,q0,q1,T,true,&DNpDsig,&DNpDR,&DNpDY,&DNpDDeltaEp, &DNpDFdefo, isThermomechanicallyCoupled(),&DNpDT);
  
  // res1 = DeltaEp - DeltaPlasticMult*Np(kcor,R,Y,T) = 0
  static STensor43 fullDNpDsig;
  fullDNpDsig = DNpDsig;
  for (int i=0; i< Y.size(); i++)
  {
    STensorOperation::prodAdd(DNpDY[i],DYDsig[i],1.,fullDNpDsig);
  }  
  STensorOperation::multSTensor43(fullDNpDsig,DsigDEepr,Dres1DEepr);
  Dres1DEepr *= (-DeltaPlasticMult[0]);
    
  for (int i=0; i< _numNonLocalVar; i++){
    STensorOperation::zero(Dres1DNonlocalVars[i]);
    for (int j=0; j< Y.size(); j++)
    {
      Dres1DNonlocalVars[i].daxpy(DNpDY[j],-DeltaPlasticMult[0]*DYDnonlocalVars[j][i]);
    }
    if(i==1)
    {
      Dres1DNonlocalVars[i]+=(DNpDR*(DeltaPlasticMult[0]*dwp));
    }
  }

  if (isThermomechanicallyCoupled()){
    STensorOperation::multSTensor43STensor3(fullDNpDsig,*DsigDT,*Dres1DT);
    Dres1DT->daxpy(DNpDR,*DRDT);
    *Dres1DT += DNpDT;
    *Dres1DT *= (-DeltaPlasticMult[0]);
  };
  
  // res2 = (sig:DeltaEp - (1-f)*Jp*R*DeltaHatP)/R0 = 0
  double Jp = 1.;
  // plastic energy balance
  if (_stressFormulation == CORO_KIRCHHOFF)
  {
    double Jp0 = STensorOperation::determinantSTensor3(q0->getConstRefToFp());
    double traceDeltaEp = DeltaEp(0,0)+DeltaEp(1,1)+DeltaEp(2,2);
    Jp = Jp0*exp(traceDeltaEp);
  }
  
  double partDres2Dyieldf = Jp*R*DeltaHatP/R0;
  static STensor3 partDres2Dsig;
  STensorOperation::scale(DeltaEp,1./R0,partDres2Dsig);
  partDres2Dsig.daxpy(DYDsig[0],partDres2Dyieldf);
  double yieldf = q1->getYieldPorosity();
  
  STensorOperation::multSTensor3STensor43(partDres2Dsig,DsigDEepr,Dres2DEepr);
  for (int i=0; i< _numNonLocalVar; i++){
    Dres2DNonlocalVars[i] = partDres2Dyieldf*DYDnonlocalVars[0][i];
    if(i==1)
    {
      Dres2DNonlocalVars[i]-=(1.-yieldf)*Jp*(DeltaHatP)/R0*dwp;

    }
  }

  if (isThermomechanicallyCoupled()){
    *Dres2DT = STensorOperation::doubledot(partDres2Dsig,*DsigDT) - (1.-yieldf)*Jp*(*DRDT)*DeltaHatP/R0;;
  }
  
  
  // compute with respect to F
  for (int i=0; i< numberYield; i++)
  {
    STensorOperation::multSTensor3STensor43(Dres0DEepr[i],EprToF,Dres0DFdefo[i]);
    Dres0DFdefo[i] += DyieldDFdefo[i];
  }
  STensorOperation::multSTensor43(Dres1DEepr, EprToF, Dres1DFdefo);
  Dres1DFdefo.axpy(-DeltaPlasticMult[0], DNpDFdefo);
  STensorOperation::multSTensor3STensor43(Dres2DEepr, EprToF, Dres2DFdefo);
}; 

bool mlawNonLocalPorosity::computeRotatedAnisotropicTensor(const STensor3& F, const STensor3& DeltaEp,  const IPNonLocalPorosity* q0, IPNonLocalPorosity* q1, bool stiff) const
{
  const STensor3& Fp0 = q0->getConstRefToFp();
  static STensor3 Rtotal;
  static STensor43 DRtotalDF, DRtotalDDeltaEp;
  bool ok = computeAnisotropyRotation(Rtotal,F,DeltaEp,Fp0,stiff, &DRtotalDF, &DRtotalDDeltaEp);
  if (not(ok))
  {
    return false;
  }
  
  STensor43& M = q1->getRefToAnisotropicTensor();
  STensor63& DMDF =q1->getRefToDAnisotropicTensorDF();
  STensor63& DMDDeltaEp = q1->getRefToDAnisotropicTensorDEp();
  const STensor43& aniM = getAnisotropicTensorReferenceConfiguration();
  STensorOperation::zero(M);
  if (stiff)
  {
    STensorOperation::zero(DMDF);
    STensorOperation::zero(DMDDeltaEp);
  }
  
  for (int i=0; i<3; i++)
  {
    for (int j=0; j<3; j++)
    {
      for (int k=0; k<3; k++)
      {
        for (int l=0; l<3; l++)
        {
          for (int p=0; p<3; p++)
          {
            for (int q=0; q<3; q++)
            {
              for (int r=0; r<3; r++)
              {
                for (int s=0; s<3; s++)
                {
                  M(i,j,k,l) += Rtotal(p,i)*Rtotal(q,j)*aniM(p,q,r,s)*Rtotal(r,k)*Rtotal(s,l);
                  if (stiff)
                  {
                    for (int a=0; a<3; a++)
                    {
                      for (int b=0; b<3; b++)
                      {
                        DMDF(i,j,k,l,a,b) += DRtotalDF(p,i,a,b)*Rtotal(q,j)*aniM(p,q,r,s)*Rtotal(r,k)*Rtotal(s,l) +
                                                 Rtotal(p,i)*DRtotalDF(q,j,a,b)*aniM(p,q,r,s)*Rtotal(r,k)*Rtotal(s,l) +
                                                  Rtotal(p,i)*Rtotal(q,j)*aniM(p,q,r,s)*DRtotalDF(r,k,a,b)*Rtotal(s,l)+ 
                                                   Rtotal(p,i)*Rtotal(q,j)*aniM(p,q,r,s)*Rtotal(r,k)*DRtotalDF(s,l,a,b);
                                                   
                        DMDDeltaEp(i,j,k,l,a,b) += DRtotalDDeltaEp(p,i,a,b)*Rtotal(q,j)*aniM(p,q,r,s)*Rtotal(r,k)*Rtotal(s,l) +
                                                 Rtotal(p,i)*DRtotalDDeltaEp(q,j,a,b)*aniM(p,q,r,s)*Rtotal(r,k)*Rtotal(s,l) +
                                                  Rtotal(p,i)*Rtotal(q,j)*aniM(p,q,r,s)*DRtotalDDeltaEp(r,k,a,b)*Rtotal(s,l)+ 
                                                   Rtotal(p,i)*Rtotal(q,j)*aniM(p,q,r,s)*Rtotal(r,k)*DRtotalDDeltaEp(s,l,a,b);
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }
  return true;
};

bool mlawNonLocalPorosity::computeAnisotropyRotation(STensor3& Rtotal, const STensor3& F, const STensor3& DeltaEp, const STensor3& Fp0, 
                                                bool stiff, STensor43* DRtotalDF, STensor43* DRtotalDDeltaEp) const
{
  static STensor3 dFp, Fp, Fpinv, Fe, Ce, Ue, UeInv, Re, C,  U, UInv, R;
  bool ok = STensorOperation::expSTensor3(DeltaEp, _orderlogexp ,dFp);
  if (not(ok))
  {
    return false;
  }
  // Fp1 = dFp * Fp0
  STensorOperation::multSTensor3(dFp,Fp0,Fp);
  // Fe = F * Fp^-1
  STensorOperation::inverseSTensor3(Fp, Fpinv);
  STensorOperation::multSTensor3(F,Fpinv,Fe);
  STensorOperation::multSTensor3FirstTranspose(Fe,Fe,Ce);
  ok = STensorOperation::sqrtSTensor3(Ce,Ue);
  if (not(ok))
  {
    return false;
  }
  STensorOperation::inverseSTensor3(Ue,UeInv);
  STensorOperation::multSTensor3(Fe,UeInv,Re);
  
  STensorOperation::multSTensor3FirstTranspose(F,F,C);
  ok = STensorOperation::sqrtSTensor3(C,U);
  if (not(ok))
  {
    return false;
  }
  STensorOperation::inverseSTensor3(U,UInv);
  STensorOperation::multSTensor3(F,UInv,R);

  STensorOperation::multSTensor3FirstTranspose(R,Re,Rtotal);
  
  if (stiff)
  {
    STensorOperation::zero(*DRtotalDDeltaEp);
    //
    static STensor3 plus_R;
    double ff = 1e-8;
    for (int i=0; i< 3; i++)
    {
      for (int j=i; j<3; j++)
      {
        static STensor3 plus_DeltaEp;
        plus_DeltaEp = DeltaEp;
        plus_DeltaEp(i,j) += 0.5*ff;
        plus_DeltaEp(j,i) += 0.5*ff;
        ok = computeAnisotropyRotation(plus_R, F, plus_DeltaEp, Fp0);
        if (not(ok))
        {
          return false;
        }
        for (int k=0; k< 3; k++)
        {
          for (int l=0; l<3; l++)
          {
            (*DRtotalDDeltaEp)(k,l,i,j) = (plus_R(k,l) -Rtotal(k,l))/(ff);
            (*DRtotalDDeltaEp)(k,l,j,i) = (*DRtotalDDeltaEp)(k,l,i,j);
          }
        }
      }
    }
    
    STensorOperation::zero(*DRtotalDF);
    for (int i=0; i< 3; i++)
    {
      for (int j=0; j<3; j++)
      {
        static STensor3 plus_F;
        plus_F = F;
        plus_F(i,j) += ff;
        ok = computeAnisotropyRotation(plus_R, plus_F, DeltaEp, Fp0);
        if (not(ok))
        {
          return false;
        }
        for (int k=0; k< 3; k++)
        {
          for (int l=0; l<3; l++)
          {
            
            (*DRtotalDF)(k,l,i,j) = (plus_R(k,l) -Rtotal(k,l))/(ff);
          }
        }
      }
    }
  }
  return true;
};


bool mlawNonLocalPorosity::I1J2J3_plasticCorrector_NonLocal_aniso(const STensor3& F1, 
                                  const STensor3& Kcorpr,  // predictor 
                                  const IPNonLocalPorosity *q0, IPNonLocalPorosity *q1,
                                  STensor3& Fe, STensor3& Ce, STensor3& Ee, STensor43& Le, STensor63& dLe,
                                  const STensor43& EprToF,
                                  const bool stiff,
                                  const STensor43& DKcorprDEepr, // predictor elastic tangent
                                  STensor43& DKcorDFdefo, std::vector<STensor3>& DKcorDnonlocalVar,
                                  STensor43& DFpDFdefo, std::vector<STensor3>& DFpDnonlocalVar,
                                  std::vector<STensor3>& DlocalVarDFdefo, fullMatrix<double>& DlocalVarDnonlocalVar,
                                  const double* T,
                                  const STensor3* DKcorprDT, 
                                  STensor3* DKcorDT, STensor3* DFpDT, fullMatrix<double>* DlocalVarDT
                                ) const{
                                  
  STensor3& Kcor = q1->getRefToCorotationalKirchhoffStress();
  Kcor = Kcorpr;
  
  static STensor3 sig;
  static STensor43 DsigDDeltaEp;
  sig = Kcor;
  STensorOperation::scale(DKcorprDEepr,-1.,DsigDDeltaEp);
  double detF = 1.;
  double detFinv = 1.;
  if (_stressFormulation == CORO_CAUCHY)
  {
    // update with deformation Jacobian
    detF = STensorOperation::determinantSTensor3(F1);
    detFinv = 1./detF;
    sig *= detFinv;
    DsigDDeltaEp *= detFinv;
  }
  
  // unknowns
  int nyield = getNumOfYieldSurfaces();
  static STensor3 DeltaEp;
  STensorOperation::zero(DeltaEp);
  static std::vector<double> DeltaPlasticMult(nyield);
  for (int i=0; i< nyield; i++)
  {
    DeltaPlasticMult[i] = 0.;
  }
  double DeltaHatP = 0;
                                  
  // get all void characteristic
  static std::vector<double> Y(4,0.);
  static std::vector<STensor3> DYDsig(4,STensor3(0.));
  static std::vector<std::vector<double> > DYDNonlocalVars(4,std::vector<double>(3,0.));
  I1J2J3_voidCharacteristicsEvolution_NonLocal_aniso(sig,q0,q1,true,&Y,&DYDsig,&DYDNonlocalVars);
  
  double & R = q1->getRefToCurrentViscoplasticYieldStress();
  R = q1->getConstRefToIPJ2IsotropicHardening().getR();
  double H = q1->getConstRefToIPJ2IsotropicHardening().getDR();
  
  
  
  static std::vector<double> res0(nyield);
  static std::vector<STensor3> Dres0DDeltaEp(nyield);
  static std::vector<double> Dres0DDeltaPlasticMult(nyield);
  static std::vector<double> Dres0DDeltaHatP(nyield);
  
  static STensor3 res1;
  static STensor43 Dres1DDeltaEp;
  static std::vector<STensor3> Dres1DDeltaPlasticMult(nyield);
  static STensor3 Dres1DDeltaHatP;
  
  static double res2;
  static STensor3 Dres2DDeltaEp;
  static std::vector<double> Dres2DDeltaPlasticMult(nyield);
  static double Dres2DDeltaHatP;
    

  static fullVector<double> res(7+nyield), sol(7+nyield);
  static fullMatrix<double> Jac(7+nyield,7+nyield);
  
  
  I1J2J3_computeResidual_aniso(DeltaEp,DeltaPlasticMult,DeltaHatP,sig,R,q0,q1,Y,
        res0,res1,res2,
        true,DsigDDeltaEp,H,DYDsig,
        Dres0DDeltaEp,Dres1DDeltaEp,Dres2DDeltaEp,
        Dres0DDeltaPlasticMult,Dres1DDeltaPlasticMult,Dres2DDeltaPlasticMult,
        Dres0DDeltaHatP, Dres1DDeltaHatP,Dres2DDeltaHatP,T);
        
  // compute solution
  I1J2J3_getResAndJacobian(res,Jac,res0,res1,res2,
                  Dres0DDeltaEp,Dres1DDeltaEp,Dres2DDeltaEp,
                  Dres0DDeltaPlasticMult,Dres1DDeltaPlasticMult,Dres2DDeltaPlasticMult,
                  Dres0DDeltaHatP,Dres1DDeltaHatP,Dres2DDeltaHatP);
  //Jac.print("Jac");
  
  int ite = 0;
  int lsMax = 10;
  
  double f = res.norm();
  // Start iterations
  //Msg::Info("plastic corrector f=%e",f);
  while(f > _tol or ite <1)
  {
                                    
    bool ok = Jac.luSolve(res,sol);
    
    if (!ok) {
      #ifdef _DEBUG
      Msg::Error("lu solve does not work !!!");
      #endif //_DEBUG
      return false;
    }
    
    double alph=1.;
    static STensor3 DeltaEp_prev;
    DeltaEp_prev = DeltaEp;
    static std::vector<double> DeltaPlasticMult_prev(nyield);
    DeltaPlasticMult_prev = DeltaPlasticMult;    
    double DeltaHatP_prev = DeltaHatP;
  
    for (int ls =0;  ls < lsMax; ls++)
    {
    
      static STensor3 dDeltaEp; 
      dDeltaEp(0,0) = alph*sol(0);
      dDeltaEp(1,1) = alph*sol(1);
      dDeltaEp(2,2) = alph*sol(2);
      dDeltaEp(0,1) = alph*sol(3);
      dDeltaEp(1,0) = alph*sol(3);
      dDeltaEp(0,2) = alph*sol(4);
      dDeltaEp(2,0) = alph*sol(4);
      dDeltaEp(1,2) = alph*sol(5);
      dDeltaEp(2,1) = alph*sol(5);
      
      if (dDeltaEp.norm0() > 1.){
        DeltaEp*=0.1;
      }
      else{
        DeltaEp -= dDeltaEp;
      }
      
      for (int i=0; i< nyield; i++)
      {
        if (DeltaPlasticMult[i]-alph*sol(6+i) <0.){
          DeltaPlasticMult[i] *=0.1;
        }
        else{
          DeltaPlasticMult[i] -= alph*sol(6+i);
        }
      }
      
      if (DeltaHatP-alph*sol(6+nyield) < 0.){
        DeltaHatP *= 0.1;
      }
      else{
        DeltaHatP -= alph*sol(6+nyield);
      }
    
      // update Kcor
      // Kcor = Kcorpr - H:DeltaEp
      Kcor = Kcorpr;
      STensorOperation::multSTensor43STensor3Add(DKcorprDEepr,DeltaEp,-1.,Kcor);
      STensorOperation::scale(Kcor,detFinv,sig);
      
      // state anisotropic tensor
      bool ok = computeRotatedAnisotropicTensor(F1, DeltaEp, q0, q1, true);
      if (not(ok))
      {
        return false;
      }
      
      // update with new sig
      // update all void characteristics
      I1J2J3_voidCharacteristicsEvolution_NonLocal_aniso(sig,q0,q1,true,&Y,&DYDsig,&DYDNonlocalVars);    
      // update internal variables
      I1J2J3_updateInternalVariables(q1,q0,DeltaEp,DeltaHatP,T);
      
      R = q1->getConstRefToIPJ2IsotropicHardening().getR();
      H = q1->getConstRefToIPJ2IsotropicHardening().getDR();
            
      I1J2J3_computeResidual_aniso(DeltaEp,DeltaPlasticMult,DeltaHatP,sig,R,q0,q1,Y,
          res0,res1,res2,
          true,DsigDDeltaEp,H,DYDsig,
          Dres0DDeltaEp,Dres1DDeltaEp,Dres2DDeltaEp,
          Dres0DDeltaPlasticMult,Dres1DDeltaPlasticMult,Dres2DDeltaPlasticMult,
          Dres0DDeltaHatP, Dres1DDeltaHatP,Dres2DDeltaHatP,T);
          
      // compute solution
      I1J2J3_getResAndJacobian(res,Jac,res0,res1,res2,
                      Dres0DDeltaEp,Dres1DDeltaEp,Dres2DDeltaEp,
                      Dres0DDeltaPlasticMult,Dres1DDeltaPlasticMult,Dres2DDeltaPlasticMult,
                      Dres0DDeltaHatP,Dres1DDeltaHatP,Dres2DDeltaHatP);   
      
      double newf = res.norm();
      if (newf < f)
      {	
        //if (ls > 0)
        //{
         // Msg::Info("ls ite = %d, alp = %e new = %e, f=%e",ls, alph, newf, f);
        //}
        f = newf;
        break;
      }
      else
      {
        alph *= 0.75;
        DeltaEp = DeltaEp_prev;
        DeltaPlasticMult = DeltaPlasticMult_prev;    
        DeltaHatP = DeltaHatP_prev;
      }
    }
      
    //printf("iter = %d error = %e \n ",ite,f);
    if (f < _tol)  break;
    
    ite++;
    if((ite > _maxite) or STensorOperation::isnan(f))
    {
      #ifdef _DEBUG
      printf("in mlawNonLocalPorosity::I1J2J3_plasticCorrector_NonLocal: no convergence for plastic correction  with ite = %d maxite = %d norm = %e chiPrev= %e chi = %e !!!\n",ite,_maxite,f,q0->getConstRefToIPVoidState().getVoidLigamentRatio(),q1->getConstRefToIPVoidState().getVoidLigamentRatio());
      #endif //_DEBUG
      return false;
    }
  }

  static STensor43 DFpDDeltaEp;
  I1J2J3_updatePlasticState(q1,q0,F1, Kcor, R, DeltaEp,DeltaHatP, H, Fe,Ce,Ee,Le,dLe,DFpDDeltaEp,T);
  /* Stiffness computation*/
  if(stiff)
  {
    double DRDT= 0.;
    if (isThermomechanicallyCoupled())
    {
      DRDT = q1->getConstRefToIPJ2IsotropicHardening().getDRDT();
    }
    double dwp=q1->getConstRefToIPJ2IsotropicHardening().getDWp();
    /* Compute internal variables derivatives from residual derivatives */
    static std::vector<STensor3> Dres0DFdefo(nyield);
    static STensor43 Dres1DFdefo;
    static STensor3 Dres2DFdefo;
    static std::vector<std::vector<double> > Dres0DNonlocalVars(nyield, std::vector<double>(_numNonLocalVar,0.));
    static std::vector<STensor3> Dres1DNonlocalVars(_numNonLocalVar,STensor3(0.));
    static std::vector<double> Dres2DNonlocalVars(_numNonLocalVar,0.);
    
    static std::vector<double> Dres0DT(nyield);
    static STensor3 Dres1DT;
    static double Dres2DT;
    
    // Kcor = Kcorpr - H:DeltaEp, 
    // DkcorDEepr = DKcorprDEepr
    // partDKcorDT = DKcorprDT - DHDT:DeltaEp
    // sig = Kcor*invdetF
    static STensor43 correctedDsigDEepr;
    STensorOperation::scale(DKcorprDEepr,detFinv,correctedDsigDEepr);
    if (_stressFormulation == CORO_CAUCHY)
    {
      // J = Jepr*Jppr --> ln(J) = tr(Eepr) + ln(ppr) --> DJDEepr = J*I
      static STensor3 DdetFDEepr;
      STensorOperation::diag(DdetFDEepr,detF);
      STensorOperation::prodAdd(Kcor,DdetFDEepr,-detFinv*detFinv,correctedDsigDEepr);
    }
    
    
    static STensor3 correctedDKcorDT, correctedDsigDT;
    if (isThermomechanicallyCoupled())
    {
      static STensor43 DHDT;
      getDHookTensorDT(DHDT,*T);
      correctedDKcorDT = *DKcorprDT;
      STensorOperation::multSTensor43STensor3Add(DHDT,DeltaEp,-1.,correctedDKcorDT);
      correctedDsigDT = correctedDKcorDT;
      correctedDsigDT *= (detFinv);
    }
    
    I1J2J3_computeDResidual_aniso(DeltaEp,DeltaPlasticMult,DeltaHatP,sig, 
                    R,q0,q1,correctedDsigDEepr, H, dwp,Y,DYDsig,DYDNonlocalVars,EprToF,
                    Dres0DNonlocalVars,Dres0DFdefo,
                    Dres1DNonlocalVars,Dres1DFdefo,
                    Dres2DNonlocalVars,Dres2DFdefo,
                    T,&DRDT,&correctedDsigDT,
                    &Dres0DT,&Dres1DT,&Dres2DT);
                    
    
    static STensor43 DDeltaEPDFdefo;
    static STensor3 DDeltaHatPDFdefo;
    
    fullMatrix<double> invJac(8,8);
    bool isInverted = Jac.invert(invJac);
    if (!isInverted) 
    {
      #ifdef _DEBUG
      Msg::Error("Jacobian cannot be inverted");
      #endif //_DEBUG
      return false;
    }
    for(int i=0; i<3; i++)
    {
      for(int j=0; j<3; j++)
      {
        for (int k=0; k< nyield; k++)
        {
          res(k) = -Dres0DFdefo[k](i,j);
        }
        res(nyield) = -Dres1DFdefo(0,0,i,j);
        res(nyield+1) = -Dres1DFdefo(1,1,i,j); 
        res(nyield+2) = -Dres1DFdefo(2,2,i,j);
        res(nyield+3) = -Dres1DFdefo(0,1,i,j);
        res(nyield+4) = -Dres1DFdefo(0,2,i,j);
        res(nyield+5) = -Dres1DFdefo(1,2,i,j);
        res(nyield+6) = -Dres2DFdefo(i,j);
        invJac.mult(res,sol);
        DDeltaEPDFdefo(0,0,i,j) = sol(0);
        DDeltaEPDFdefo(1,1,i,j) = sol(1);
        DDeltaEPDFdefo(2,2,i,j) = sol(2);
        DDeltaEPDFdefo(0,1,i,j) = sol(3);
        DDeltaEPDFdefo(1,0,i,j) = sol(3);
        DDeltaEPDFdefo(0,2,i,j) = sol(4);
        DDeltaEPDFdefo(2,0,i,j) = sol(4);
        DDeltaEPDFdefo(1,2,i,j) = sol(5);
        DDeltaEPDFdefo(2,1,i,j) = sol(5);
        DDeltaHatPDFdefo(i,j) = sol(6+nyield);
      }
    }
    // local variables
    // double DeltaHatQ, DeltaHatD , DeltaHatP
    // DeltaHatQ = trace(DeltaEp)
    // DeltaHatD = sqrt(2*devDeltaEp*DevDeltaEp/3)
    
    static STensor3 devDeltaEp;
    double DeltaHatQ, DeltaHatD; // volumetric part pf DeltaEp
    STensorOperation::decomposeDevTr(DeltaEp,devDeltaEp,DeltaHatQ);
    DeltaHatD = sqrt(2.*STensorOperation::doubledot(devDeltaEp,devDeltaEp)/3.);

    // local Var are [DeltaHatQ DeltaHatP DeltaHatD]
    STensorOperation::multSTensor3STensor43(_I,DDeltaEPDFdefo,DlocalVarDFdefo[0]);
    DlocalVarDFdefo[1] = DDeltaHatPDFdefo;
    STensorOperation::multSTensor3STensor43(devDeltaEp,DDeltaEPDFdefo,DlocalVarDFdefo[2]);
    DlocalVarDFdefo[2] *= (2./(3.*DeltaHatD));
    
    static std::vector<STensor3> DDeltaEpDNonlocalVars(_numNonLocalVar);
    static std::vector<double> DDeltaHatPDNonlocalVars(_numNonLocalVar);
    
    // nonlocal derivatives
    for (int i=0; i< _numNonLocalVar; i++){
      for (int k=0; k< nyield; k++){
        res(k) = -Dres0DNonlocalVars[k][i];
      }
      res(nyield) = -Dres1DNonlocalVars[i](0,0);
      res(nyield+1) = -Dres1DNonlocalVars[i](1,1);
      res(nyield+2) = -Dres1DNonlocalVars[i](2,2);
      res(nyield+3) = -Dres1DNonlocalVars[i](0,1);
      res(nyield+4) = -Dres1DNonlocalVars[i](0,2);
      res(nyield+5) = -Dres1DNonlocalVars[i](1,2);
      res(nyield+6) = -Dres2DNonlocalVars[i];
      
      invJac.mult(res,sol);
    
      DDeltaEpDNonlocalVars[i](0,0) = sol(0);
      DDeltaEpDNonlocalVars[i](1,1) = sol(1);
      DDeltaEpDNonlocalVars[i](2,2) = sol(2);
      DDeltaEpDNonlocalVars[i](0,1) = sol(3);
      DDeltaEpDNonlocalVars[i](1,0) = sol(3);
      DDeltaEpDNonlocalVars[i](0,2) = sol(4);
      DDeltaEpDNonlocalVars[i](2,0) = sol(4);
      DDeltaEpDNonlocalVars[i](1,2) = sol(5);
      DDeltaEpDNonlocalVars[i](2,1) = sol(5);
      DDeltaHatPDNonlocalVars[i] = sol(6+nyield);
      
      DlocalVarDnonlocalVar(0,i) = STensorOperation::doubledot(_I,DDeltaEpDNonlocalVars[i]);
      DlocalVarDnonlocalVar(1,i) = DDeltaHatPDNonlocalVars[i];
      DlocalVarDnonlocalVar(2,i) = STensorOperation::doubledot(devDeltaEp,DDeltaEpDNonlocalVars[i])*2./(3.*DeltaHatD);
    };
    
    // compute tangents
    // kcor = kcorpr - H:DeltaEp 
    STensorOperation::multSTensor43(DKcorprDEepr, EprToF, DKcorDFdefo);
    STensorOperation::multSTensor43Add(DKcorprDEepr,DDeltaEPDFdefo,-1.,DKcorDFdefo);
    for (int i=0; i< _numNonLocalVar; i++){
      STensorOperation::multSTensor43STensor3(DKcorprDEepr,DDeltaEpDNonlocalVars[i],DKcorDnonlocalVar[i],-1.);
    }
    // Fp = exp(DeltaEp)*Fp0
    STensorOperation::multSTensor43(DFpDDeltaEp,DDeltaEPDFdefo,DFpDFdefo);
    for (int i=0; i< _numNonLocalVar; i++){
      STensorOperation::multSTensor43STensor3(DFpDDeltaEp,DDeltaEpDNonlocalVars[i],DFpDnonlocalVar[i]);
    }
    
    // plastic energy DplasticEnegy = Kcor:DeltaEp
    STensor3& DplasticEnergyDF = q1->getRefToDPlasticEnergyDF();
    STensorOperation::multSTensor43STensor3(DKcorDFdefo,DeltaEp,DplasticEnergyDF);
    STensorOperation::multSTensor3STensor43Add(Kcor,DDeltaEPDFdefo,1.,DplasticEnergyDF);
    
    for (int nl=0; nl < _numNonLocalVar; nl++){
      q1->getRefToDPlasticEnergyDNonLocalVariable(nl) = STensorOperation::doubledot(DKcorDnonlocalVar[nl],DeltaEp)+STensorOperation::doubledot(Kcor,DDeltaEpDNonlocalVars[nl]);
    };
    
    if (isThermomechanicallyCoupled()){
      static STensor3 DDeltaEpDT;
      static double DDeltaHatPDT;
      
      // dtilde_fv
      for (int k=0; k< nyield; k++)
      {
        res(k) = -Dres0DT[k];
      }
      res(nyield) = -Dres1DT(0,0);
      res(nyield+1) = -Dres1DT(1,1);
      res(nyield+2) = -Dres1DT(2,2);
      res(nyield+3) = -Dres1DT(0,1);
      res(nyield+4) = -Dres1DT(0,2);
      res(nyield+5) = -Dres1DT(1,2);
      res(nyield+6) = -Dres2DT;
      
      invJac.mult(res,sol);
    
      DDeltaEpDT(0,0) = sol(0);
      DDeltaEpDT(1,1) = sol(1);
      DDeltaEpDT(2,2) = sol(2);
      DDeltaEpDT(0,1) = sol(3);
      DDeltaEpDT(1,0) = sol(3);
      DDeltaEpDT(0,2) = sol(4);
      DDeltaEpDT(2,0) = sol(4);
      DDeltaEpDT(1,2) = sol(5);
      DDeltaEpDT(2,1) = sol(5);
      DDeltaHatPDT = sol(6+nyield);
      
      (*DlocalVarDT)(0,0) = STensorOperation::doubledot(_I,DDeltaEpDT);
      (*DlocalVarDT)(1,0) = DDeltaHatPDT;
      (*DlocalVarDT)(2,0) = STensorOperation::doubledot(devDeltaEp,DDeltaEpDT)*2./(3.*DeltaHatD);
      
      // using Kcor = Kcorpr - HT:DeltaEp
      *DKcorDT = correctedDKcorDT;
      STensorOperation::multSTensor43STensor3Add(DKcorprDEepr,DDeltaEpDT,-1.,*DKcorDT);
      // for Fp
      STensorOperation::multSTensor43STensor3(DFpDDeltaEp,DDeltaEpDT,*DFpDT);
      // for plastic energy
      double& DplasticEnergyDT = q1->getRefToDPlasticEnergyDT();
      DplasticEnergyDT =  STensorOperation::doubledot(*DKcorDT,DeltaEp)+ STensorOperation::doubledot(Kcor,DDeltaEpDT);
    }
  };
  return true;
};

void mlawNonLocalPorosity::tangentComputation_aniso(STensor43& dFedF, std::vector<STensor3>& dFeDNonlocalVar, STensor43& dStressDF,
                              std::vector<STensor3>& dStressDNonlocalVar, std::vector<STensor3>& DLocalVardF,
                              const bool plastic,
                              const STensor3& F,  // current F
                              const STensor3& corKir,  // cor Kir
                              const STensor3& S, //  second PK
                              const STensor3& Fepr, const STensor3& Fppr, // predictor
                              const STensor43& Lpr,
                              const STensor3& Fe, const STensor3& Fp, // corrector
                              const STensor43& L, const STensor63& dL, // corrector value
                              const STensor43& DcorKirDF, const std::vector<STensor3>& DcorKirDNonlocalVar,  // small strain tangents
                              const STensor43& dFpdF, const std::vector<STensor3>& DFpDNonlocalVar,
                              const std::vector<STensor3>& DLocalVarDFdefo,
                              const STensor3& Fpinv
                              ) const{
  // P = Fe. (Kcor:Le) . invFp
  // need to compute dFedF, dKcordF, dinvFpdF, dLedF
  for (int i=0; i< _numNonLocalVar; i++){
    DLocalVardF[i] = DLocalVarDFdefo[i];
  }


  // done DcorKirDF, DcorKirDtildefV, DFpDF, DFpDtildefV

  static STensor43 DinvFpdF;
  static std::vector<STensor3> DinvFpDNonlocalVar;
  if (DinvFpDNonlocalVar.size() != _numNonLocalVar)
    DinvFpDNonlocalVar.resize(_numNonLocalVar);

  STensorOperation::zero(DinvFpdF);
  for (int il=0; il< _numNonLocalVar; il++){
    STensorOperation::zero(DinvFpDNonlocalVar[il]);
  }
  if (plastic){
    for (int i=0; i<3; i++){
      for (int j=0; j<3; j++){
        for (int p=0; p<3; p++){
          for (int q=0; q<3; q++){
            for (int il=0; il< _numNonLocalVar; il++){
              DinvFpDNonlocalVar[il](i,j) -= Fpinv(i,p)*DFpDNonlocalVar[il](p,q)*Fpinv(q,j);
            }

            for (int k=0; k<3; k++){
              for (int l=0; l<3; l++){
                DinvFpdF(i,j,k,l) -= Fpinv(i,p)*dFpdF(p,q,k,l)*Fpinv(q,j);
              }
            }
          }
        }
      }
    }
  }

  STensorOperation::zero(dFedF);
  for (int il=0; il < _numNonLocalVar; il++){
    STensorOperation::zero(dFeDNonlocalVar[il]);
  }
  for (int i=0; i<3; i++){
    for (int j=0; j<3; j++){
      for (int k=0; k<3; k++){
        dFedF(i,j,i,k) += Fpinv(k,j);
        if (plastic){
          for (int il=0; il<_numNonLocalVar; il++){
            dFeDNonlocalVar[il](i,j) += F(i,k)*DinvFpDNonlocalVar[il](k,j);
          }
          for (int l=0; l<3; l++){
            for (int p=0; p<3; p++){
              dFedF(i,j,k,l) += F(i,p)*DinvFpdF(p,j,k,l);
            }
          }
        }
      }
    }
  }
  static STensor63 DLDF;
  static std::vector<STensor43> DLDNonlocalVar;
  if (DLDNonlocalVar.size() != _numNonLocalVar){
    DLDNonlocalVar.resize(_numNonLocalVar);
  }
  STensorOperation::zero(DLDF);
  for (int il=0; il< _numNonLocalVar; il++){
    STensorOperation::zero(DLDNonlocalVar[il]);
  }
  for (int i=0; i<3; i++){
    for (int j=0; j<3; j++){
      for (int k=0; k<3; k++){
        for (int l=0; l<3; l++){
          for (int r=0; r<3; r++){
            for (int s=0; s<3; s++){
              for (int a=0; a<3; a++){
                for (int il=0; il< _numNonLocalVar; il++){
                  DLDNonlocalVar[il](i,j,k,l) += dL(i,j,k,l,r,s)*2.*Fe(a,r)*dFeDNonlocalVar[il](a,s);
                }
                for (int p=0; p<3; p++){
                  for (int q=0; q<3; q++){
                    DLDF(i,j,k,l,p,q) += dL(i,j,k,l,r,s)*2.*Fe(a,r)*dFedF(a,s,p,q);
                  }
                }
              }
            }
          }
        }
      }
    }
  }
  //

  static STensor43 DSDF; // S = corKir:L
  static std::vector<STensor3> DSDNonlocalVar;
  if (DSDNonlocalVar.size() != _numNonLocalVar){
    DSDNonlocalVar.resize(_numNonLocalVar);
  }
  STensorOperation::zero(DSDF);
  for (int il=0; il<_numNonLocalVar; il++){
    STensorOperation::zero(DSDNonlocalVar[il]);
  }
  for (int i=0; i<3; i++){
    for (int j=0; j<3; j++){
      for (int r=0; r<3; r++){
        for (int s=0; s<3; s++){
          for (int il=0; il<_numNonLocalVar; il++){
            DSDNonlocalVar[il](i,j) += DcorKirDNonlocalVar[il](r,s)*L(r,s,i,j) + corKir(r,s)*DLDNonlocalVar[il](r,s,i,j);
          }
          for (int k=0; k<3; k++){
            for (int l=0; l<3; l++){
              DSDF(i,j,k,l) += DcorKirDF(r,s,k,l)*L(r,s,i,j) + corKir(r,s)*DLDF(r,s,i,j,k,l);
            }
          }
        }
      }
    }
  }


  // compute mechanical tengent
  STensorOperation::zero(dStressDF);
  for (int il=0; il<_numNonLocalVar; il++){
    STensorOperation::zero(dStressDNonlocalVar[il]);
  }
  for (int i=0; i<3; i++){
    for (int j=0; j<3; j++){
      for (int k=0; k<3; k++){
        for (int l=0; l<3; l++){
          for (int il=0; il<_numNonLocalVar; il++){
            dStressDNonlocalVar[il](i,j) += (dFeDNonlocalVar[il](i,k)*S(k,l)*Fpinv(j,l) + Fe(i,k)*DSDNonlocalVar[il](k,l)*Fpinv(j,l)+Fe(i,k)*S(k,l)*DinvFpDNonlocalVar[il](j,l));
          }
          for (int p=0; p<3; p++){
            for (int q=0; q<3; q++){
              dStressDF(i,j,p,q) += (dFedF(i,k,p,q)*S(k,l)*Fpinv(j,l) + Fe(i,k)*DSDF(k,l,p,q)*Fpinv(j,l)+Fe(i,k)*S(k,l)*DinvFpdF(j,l,p,q));
            }
          }
        }
      }
    }
  }
};

void mlawNonLocalPorosity::tangentComputationWithTemperature_aniso(STensor43& dFedF, std::vector<STensor3>& dFeDNonlocalVar, STensor43& dStressDF,
                                std::vector<STensor3>& dStressDNonlocalVar, STensor3& dStressDT,
                                std::vector<STensor3>& DLocalVardF,
                                const bool plastic,
                                const STensor3& F,  // current F
                                const STensor3& corKir,  // cor Kir
                                const STensor3& S, //  second PK
                                const STensor3& Fepr, const STensor3& Fppr, // predictor
                                const STensor43& Lpr,
                                const STensor3& Fe, const STensor3& Fp, // corrector
                                const STensor43& L, const STensor63& dL, // corrector value
                                const STensor43& DcorKirDFdefo, const std::vector<STensor3>& DcorKirDNonlocalVar, const STensor3& DcorKirDT, // small strain tangents
                                const STensor43& dFpDFdefo, const std::vector<STensor3>& DFpDNonlocalVar, const STensor3& DFpDT,
                                const std::vector<STensor3>& DLocalVarDFdefo,
                                const STensor3& Fpinv
                                ) const{

  static STensor3 DinvFpDT;
  STensorOperation::zero(DinvFpDT);//---added

  if(plastic){
    for (int i=0; i<3; i++){
      for (int j=0; j<3; j++){
        for (int p=0; p<3; p++){
          for (int q=0; q<3; q++){
             DinvFpDT(i,j) -= Fpinv(i,p)*DFpDT(p,q)*Fpinv(q,j);
          }
        }
      }
    }
  }

  static STensor3 dFeDT;
  STensorOperation::zero(dFeDT);


  if(plastic){
    for (int i=0; i<3; i++){
      for (int j=0; j<3; j++){
        for (int k=0; k<3; k++){
          dFeDT(i,j) += F(i,k)*DinvFpDT(k,j);
        }
      }
    }
  }

  static STensor43 DLDT;
  STensorOperation::zero(DLDT);
  for (int i=0; i<3; i++){
    for (int j=0; j<3; j++){
      for (int k=0; k<3; k++){
        for (int l=0; l<3; l++){
          for (int r=0; r<3; r++){
            for (int s=0; s<3; s++){
              for (int a=0; a<3; a++){
                DLDT(i,j,k,l) += dL(i,j,k,l,r,s)*2.*Fe(a,r)*dFeDT(a,s);
              }
            }
          }
        }
      }
    }
  }

  static STensor3 DSDT;
  STensorOperation::zero(DSDT);
  for (int i=0; i<3; i++){
    for (int j=0; j<3; j++){
      for (int r=0; r<3; r++){
        for (int s=0; s<3; s++){
          DSDT(i,j) += DcorKirDT(r,s)*L(r,s,i,j) + corKir(r,s)*DLDT(r,s,i,j);
        }
      }
    }
  }

  STensorOperation::zero(dStressDT);
  for (int i=0; i<3; i++){
    for (int j=0; j<3; j++){
      for (int k=0; k<3; k++){
        for (int l=0; l<3; l++){
           dStressDT(i,j) += (dFeDT(i,k)*S(k,l)*Fpinv(j,l) + Fe(i,k)*DSDT(k,l)*Fpinv(j,l)+Fe(i,k)*S(k,l)*DinvFpDT(j,l));
        }
      }
    }
  }

  tangentComputation_aniso(dFedF,dFeDNonlocalVar,dStressDF,dStressDNonlocalVar,DLocalVardF,plastic,F,corKir,
                   S,Fepr,Fppr,Lpr,Fe,Fp,L,dL,DcorKirDFdefo,DcorKirDNonlocalVar,dFpDFdefo,
                   DFpDNonlocalVar,DLocalVarDFdefo,Fpinv);

};

void mlawNonLocalPorosity::I1J2J3_predictorCorrector_NonLocal_aniso(const STensor3& F0,         // initial deformation gradient (input @ time n)
                            const STensor3& F1,         // updated deformation gradient (input @ time n+1)
                            STensor3 &P,                // updated 1st Piola-Kirchhoff stress tensor (output)
                                                        // contains the initial values on input
                            const IPNonLocalPorosity *ipvprev,       // array of initial internal variable
                            IPNonLocalPorosity *ipvcur,             // updated array of internal variable (in ipvcur on output),
                            STensor43 &Tangent,         // constitutive tangents (output)
                            std::vector<STensor3>  &dLocalVarDF,
                            std::vector<STensor3>  &dStressDNonLocalVar,
                            fullMatrix<double>    &dLocalVarDNonLocalVar,
                            const bool stiff,                   // if true compute the tangents
                            STensor43& dFedF, std::vector<STensor3>& dFeDNonLocalVar,
                            STensor3*  dStressDT,
                            fullMatrix<double>* dLocalVarDT,
                            const double T0 ,const double T
                           ) const{

  // starting from elastic state
  I1J2J3_copyInternalVariableForElasticState(ipvcur,ipvprev,T);
  //
  double& R  = ipvcur->getRefToCurrentViscoplasticYieldStress();
  R= ipvcur->getConstRefToIPJ2IsotropicHardening().getR();
 
  const STensor3* Fp0 = &(ipvprev->getConstRefToFp());
  STensor3& Fp1 = ipvcur->getRefToFp();
  double& fV1 = ipvcur->getRefToLocalPorosity();
  STensor3& Ee = ipvcur->getRefToElasticDeformation();
  STensor3& corKir = ipvcur->getRefToCorotationalKirchhoffStress();

  // elastic predictor
  static STensor3 kcorprDev, Fe, Fepr, Ce, Cepr, Epr;
  static double ppr;
  static STensor43 Lepr, Le;
  static STensor63 dLepr, dLe;


  static STensor3 corKirpr;
  static STensor43 DcorKirprDEpr,DkcorprDevDEpr,DcorKirDEpr;
  static STensor43 DcorKirDFdefo, DFpDFdefo;
  static STensor3 DpprDEpr;
  static STensor3 DcorKirDT, DcorKirprDT, DkcorprDevDT;
  static STensor3 DFpDT;
  static double dpprDT;

  static std::vector<STensor3> DcorKirDNonLocalVar, DFpDNonLocalVar, DLocalVarDFdefo;
  if (DcorKirDNonLocalVar.size() != _numNonLocalVar){
    DcorKirDNonLocalVar.resize(_numNonLocalVar);
    DFpDNonLocalVar.resize(_numNonLocalVar);
    DLocalVarDFdefo.resize(_numNonLocalVar);
  };
  
  ipvcur->getRefToConstitutiveSuccessFlag() = true;

  // elastic predictor
  bool okElast = elasticPredictor(F1,*Fp0,corKirpr,kcorprDev,ppr,DcorKirprDEpr,DkcorprDevDEpr,DpprDEpr,Fepr,Cepr,Epr,Lepr,dLepr,true,
                  &T,&DcorKirprDT,&DkcorprDevDT,&dpprDT);
                  
  if (!okElast)
  {
    P(0,0) = P(1,1) = P(2,2)= sqrt(-1.); // to exist NR and performed next step with a smaller incremenet
    ipvcur->getRefToConstitutiveSuccessFlag() = false;
    return;
  }
  // elastic only
  corKir = corKirpr;
  Fe = Fepr;
  Ce = Cepr;
  Ee = Epr;
  Le = Lepr;
  dLe = dLepr;
  
  static STensor43 EprToF;
  if (stiff){
    static STensor3 invFp0;
    STensorOperation::inverseSTensor3(*Fp0,invFp0);
    for (int i=0; i<3; i++){
      for (int j=0; j<3; j++){
        for (int k=0; k<3; k++){
          for (int l=0; l<3; l++){
            EprToF(i,j,k,l) = 0.;
            for (int p=0; p<3; p++){
              for (int q=0; q<3; q++){
                EprToF(i,j,k,l) += Lepr(i,j,p,q)*Fepr(k,p)*invFp0(l,q);
              }
            }
          }
        }
      }
    }
  
    STensorOperation::zero(DFpDFdefo);
    for (int il = 0; il < _numNonLocalVar; il++){
      STensorOperation::zero(DcorKirDNonLocalVar[il]);
      STensorOperation::zero(DFpDNonLocalVar[il]);
      STensorOperation::zero(DLocalVarDFdefo[il]);
      if (isThermomechanicallyCoupled()){
        (*dLocalVarDT)(il,0) = 0.;
      }
    }
    STensorOperation::zero(DFpDT);//--------added
    STensorOperation::multSTensor43(DcorKirprDEpr, EprToF, DcorKirDFdefo);
    DcorKirDT = DcorKirprDT;
    dLocalVarDNonLocalVar.setAll(0.);

    ipvcur->getRefToDPlasticEnergyDMatrixPlasticStrain() = 0.;
    STensorOperation::zero(ipvcur->getRefToDPlasticEnergyDF());
    if (isThermomechanicallyCoupled()){
      STensorOperation::zero(ipvcur->getRefToDPlasticEnergyDT());
    }
    for (int i=0; i< _numNonLocalVar; i++){
      STensorOperation::zero(ipvcur->getRefToDPlasticEnergyDNonLocalVariable(i));
    }
  }

  // predictor
  double kCorPrEq = sqrt(1.5*kcorprDev.dotprod());

  I1J2J3_voidCharacteristicsEvolution_NonLocal_aniso(corKirpr,ipvprev,ipvcur);
  // state anisotropic tensor
  static STensor3 zero(0.);
  computeRotatedAnisotropicTensor(F1, zero,ipvprev,ipvcur, true);

  // check with previous value
  bool plastic = I1J2J3_withPlastic_aniso(F1,corKirpr,R,ipvprev,ipvcur,&T);
  bool correctorOK = false;
  if (plastic){
    correctorOK = I1J2J3_plasticCorrector_NonLocal_aniso(F1,corKirpr,ipvprev,ipvcur,
                    Fe,Ce,Ee,Le,dLe,EprToF,
                    stiff,DcorKirprDEpr,
                    DcorKirDFdefo,DcorKirDNonLocalVar,
                    DFpDFdefo,DFpDNonLocalVar,
                    DLocalVarDFdefo,dLocalVarDNonLocalVar,
                    &T,&DcorKirprDT,&DcorKirDT,&DFpDT,dLocalVarDT);
    // subsetepping
    if (!correctorOK and _withSubstepping){
      #ifdef _DEBUG
      printf("start substepping\n");
      #endif //_DEBUG
      // substepping devide by 0 untile converge
      int numSubStep = 2.;
      int numAttempt = 0;
      static STensor3 dF, Fcur;
      dF = F1;
      dF -= F0;
      
      double DeltaHatQNonLocal = ipvcur->getNonLocalVolumetricPlasticStrain() - ipvprev->getNonLocalVolumetricPlasticStrain();
      double DeltaHatDNonLocal = ipvcur->getNonLocalDeviatoricPlasticStrain() - ipvprev->getNonLocalDeviatoricPlasticStrain();
      double DeltaHatPNonLocal = ipvcur->getNonLocalMatrixPlasticStrain() - ipvprev->getNonLocalMatrixPlasticStrain();

      double dT,Tcur; 
      dT = T - T0;
      static IPNonLocalPorosity* ipvTemp = static_cast<IPNonLocalPorosity*>(ipvprev->clone());
      
      while (true){
        bool success = true;
        int iter = 0;
        ipvTemp->operator =(*dynamic_cast<const IPVariable*>(ipvprev)); // take privious value

        while (iter < numSubStep){

          iter++;
          double fact = ((double)iter)/(double)numSubStep;
          //Msg::Info("substepping step %d of %d fact = %e",iter,numSubStep,fact);
          bool estimateStiff = false;
          if (iter == numSubStep){
            estimateStiff = stiff;
          }

          // modify all kinematic variable
          Fcur = F0;
          Fcur.daxpy(dF,fact);
          ipvcur->getRefToNonLocalVolumetricPlasticStrain() = ipvprev->getNonLocalVolumetricPlasticStrain() + fact*DeltaHatQNonLocal;
          ipvcur->getRefToNonLocalDeviatoricPlasticStrain() = ipvprev->getNonLocalDeviatoricPlasticStrain() + fact*DeltaHatDNonLocal;
          ipvcur->getRefToNonLocalMatrixPlasticStrain() = ipvprev->getNonLocalMatrixPlasticStrain() + fact*DeltaHatPNonLocal;
          Tcur = T0 + fact*dT;//------for temperature substepping
          ipvcur->setTemperature(Tcur);
          
          I1J2J3_copyInternalVariableForElasticState(ipvcur,ipvTemp,Tcur);
      
          //elastic predictor
          Fp0 = &ipvTemp->getConstRefToFp();
          okElast =  elasticPredictor(Fcur,*Fp0,corKirpr,kcorprDev,ppr,DcorKirprDEpr,DkcorprDevDEpr,DpprDEpr,Fepr,Cepr,Epr,Lepr,dLepr,estimateStiff,
                          &Tcur,&DcorKirprDT,&DkcorprDevDT,&dpprDT);
          if (!okElast)
          {
            ipvcur->getRefToConstitutiveSuccessFlag() = false;
            P(0,0) = P(1,1) = P(2,2)= sqrt(-1.); // to exist NR and performed next step with a smaller incremenet
            return;
          }
          //check yield condition
          I1J2J3_voidCharacteristicsEvolution_NonLocal_aniso(corKirpr,ipvTemp,ipvcur);

          plastic =I1J2J3_withPlastic_aniso(Fcur,corKirpr,R,ipvTemp,ipvcur,&T);

          if (plastic){
            correctorOK = I1J2J3_plasticCorrector_NonLocal_aniso(F1,corKirpr,ipvTemp,ipvcur,
                    Fe,Ce,Ee,Le,dLe,EprToF,
                    stiff,DcorKirprDEpr,
                    DcorKirDFdefo,DcorKirDNonLocalVar,
                    DFpDFdefo,DFpDNonLocalVar,
                    DLocalVarDFdefo,dLocalVarDNonLocalVar,
                    &T,&DcorKirprDT,&DcorKirDT,&DFpDT,dLocalVarDT);

            if (!correctorOK){
              success = false;
              break;
            }
          }

          // next step
          if (iter < numSubStep){
            ipvTemp->operator =(*dynamic_cast<const IPVariable*>(ipvcur));
          }
        }
        if (!success){
          numSubStep *= 2;
          numAttempt++;
          #ifdef _DEBUG
          printf("substeping is not converged, decrease number of step\n");
          #endif //_DEBUG
        }
        else{
          #ifdef _DEBUG
          printf("substeping with success\n");
          #endif //DEBUG
          correctorOK = true;
          plastic = true;
          break;
        }
        if (numAttempt > _maxAttemptSubstepping){
          #ifdef _DEBUG
          printf("maximal attemp substepping is reached\n");
          #endif //_DEBUG
          correctorOK = false;
          plastic = true;
          break;
        }
      }
    }
  }

  if (plastic and !correctorOK){
    P(0,0) = sqrt(-1.); // to exist NR and performed next step with a smaller incremenet
    ipvcur->getRefToConstitutiveSuccessFlag() = false;
    return;
  }


  // update first PK stress
  static STensor3 S, FeS, invFp;
  STensorOperation::inverseSTensor3(Fp1,invFp);
  STensorOperation::multSTensor3STensor43(corKir,Le,S);
  STensorOperation::multSTensor3(Fe,S,FeS);
  STensorOperation::multSTensor3SecondTranspose(FeS,invFp,P);

  // elastic energy
  ipvcur->getRefToElasticEnergy()= this->deformationEnergy(Ce,&T);
  if (this->getMacroSolver()->withPathFollowing()){
    // irreversible energy
    if ((this->getMacroSolver()->getPathFollowingLocalIncrementType() == pathFollowingManager::PLASTIC_ENERGY) or
             (this->getMacroSolver()->getPathFollowingLocalIncrementType() == pathFollowingManager::DISSIPATION_ENERGY)) {
      ipvcur->getRefToIrreversibleEnergy() = ipvprev->irreversibleEnergy()+ ipvcur->plasticEnergy() - ipvprev->plasticEnergy();
    }
    else{
      ipvcur->getRefToIrreversibleEnergy() = 0.;
    }
  };

  ipvcur->getRefToDissipationActive() = plastic;

  if (stiff)
  {
    if(isThermomechanicallyCoupled()){
      tangentComputationWithTemperature_aniso(dFedF,dFeDNonLocalVar,Tangent,dStressDNonLocalVar, *dStressDT, dLocalVarDF,
                           plastic,F1,corKir, S, Fepr,  *Fp0, Lepr,
                           Fe,Fp1,Le, dLe,
                           DcorKirDFdefo, DcorKirDNonLocalVar, DcorKirDT,DFpDFdefo, DFpDNonLocalVar, DFpDT, DLocalVarDFdefo,
                           invFp);
    }
    else{
       tangentComputation_aniso(dFedF,dFeDNonLocalVar,Tangent,dStressDNonLocalVar, dLocalVarDF,
                           plastic,F1,corKir, S, Fepr,  *Fp0, Lepr,
                           Fe,Fp1,Le, dLe,
                           DcorKirDFdefo, DcorKirDNonLocalVar,DFpDFdefo, DFpDNonLocalVar, DLocalVarDFdefo,
                           invFp);
    }
    

    if (this->getMacroSolver()->withPathFollowing()){
      // irreversible energy
      if ((this->getMacroSolver()->getPathFollowingLocalIncrementType() == pathFollowingManager::PLASTIC_ENERGY) or
               (this->getMacroSolver()->getPathFollowingLocalIncrementType() == pathFollowingManager::DISSIPATION_ENERGY)) {
        ipvcur->getRefToDIrreversibleEnergyDF() = ipvcur->getConstRefToDPlasticEnergyDF();
        for (int i=0; i< _numNonLocalVar; i++){
          if (ipvcur->getNonLocalToLocal()){
            ipvcur->getRefToDIrreversibleEnergyDNonLocalVariable(i) = 0.;
          }
          else{
            ipvcur->getRefToDIrreversibleEnergyDNonLocalVariable(i) = ipvcur->getConstRefToDPlasticEnergyDNonLocalVariable(i);
          }
        }
      }
      else{
        Msg::Error("Path following method is only contructed with dissipation based on plastic energy");
      }
    };
  }
};

combinedPorousLaw::combinedPorousLaw(const int num,const double E,const double nu, const double rho,
                const double fVinitial, const J2IsotropicHardening &j2IH, const CLengthLaw &cLLaw,
                const double tol, const bool matrixbyPerturbation, const double pert) :
                    mlawNonLocalPorosity(num, E, nu, rho, fVinitial, j2IH, cLLaw, tol, matrixbyPerturbation, pert),
                    _multipleLaws(0)
{

};

combinedPorousLaw::combinedPorousLaw(const combinedPorousLaw &source):mlawNonLocalPorosity(source)
{

};
combinedPorousLaw::~combinedPorousLaw()
{

};

void combinedPorousLaw::setNucleationLaw(const NucleationFunctionLaw& added_function, const NucleationCriterionLaw* added_criterion)
{
  _nucleationLaw->addOneNucleationComponent(added_function, added_criterion);
  // same for all laws
  for (int i = 0; i < _multipleLaws.size(); i++) {
    _multipleLaws[i]->setNucleationLaw(added_function,added_criterion);
  }
} 

void combinedPorousLaw::setNucleationLaw(const NucleationLaw& nuclLaw)
{
  if (_nucleationLaw !=NULL) delete _nucleationLaw;
  _nucleationLaw = nuclLaw.clone();
  
   // same for all laws
  for (int i = 0; i < _multipleLaws.size(); i++) {
    _multipleLaws[i]->setNucleationLaw(nuclLaw);
  }
}


void combinedPorousLaw::initLaws( const std::map< int, materialLaw* > &maplaw )
{
  for (int i = 0; i < _multipleLaws.size(); i++) {
    _multipleLaws[i]->initLaws(maplaw);
  }

}



void combinedPorousLaw::setMacroSolver(const nonLinearMechSolver* sv)
{
  mlawNonLocalPorosity::setMacroSolver(sv);
  for (int i=0; i< _multipleLaws.size(); i++)
  {
    _multipleLaws[i]->setMacroSolver(sv);
  }
};
// Option settings
void combinedPorousLaw::setStressFormulation(const int fm)
{
  mlawNonLocalPorosity::setStressFormulation(fm);
  for (int i=0; i< _multipleLaws.size(); i++)
  {
    _multipleLaws[i]->setStressFormulation(fm);
  }
};
void combinedPorousLaw::activateNucleationDuringCoalescence(const bool flag)
{
  mlawNonLocalPorosity::activateNucleationDuringCoalescence(flag);
  for (int i=0; i< _multipleLaws.size(); i++)
  {
    _multipleLaws[i]->activateNucleationDuringCoalescence(flag);
  }
};

void combinedPorousLaw::setNonLocalMethod(const int meth)
{
  mlawNonLocalPorosity::setNonLocalMethod(meth);
  for (int i=0; i< _multipleLaws.size(); i++)
  {
    _multipleLaws[i]->setNonLocalMethod(meth);
  }
};
void combinedPorousLaw::setOrderForLogExp(const int newOrder)
{
  mlawNonLocalPorosity::setOrderForLogExp(newOrder);
  for (int i=0; i< _multipleLaws.size(); i++)
  {
    _multipleLaws[i]->setOrderForLogExp(newOrder);
  }
};
void combinedPorousLaw::setViscoPlasticLaw(const viscosityLaw& vico)
{
  mlawNonLocalPorosity::setViscoPlasticLaw(vico);
  for (int i=0; i< _multipleLaws.size(); i++)
  {
    _multipleLaws[i]->setViscoPlasticLaw(vico);
  }
}
void combinedPorousLaw::setPlasticInstabilityFunctionLaw(const PlasticInstabilityFunctionLaw &plIns)
{
  mlawNonLocalPorosity::setPlasticInstabilityFunctionLaw(plIns);
  for (int i=0; i< _multipleLaws.size(); i++)
  {
    _multipleLaws[i]->setPlasticInstabilityFunctionLaw(plIns);
  }
}
void combinedPorousLaw::setScatterredInitialPorosity(double f0min, double f0max)
{
  mlawNonLocalPorosity::setScatterredInitialPorosity(f0min,f0max);
  for (int i=0; i< _multipleLaws.size(); i++)
  {
    _multipleLaws[i]->setScatterredInitialPorosity(f0min,f0max);
  }
};
void combinedPorousLaw::clearCLengthLaw()
{
  mlawNonLocalPorosity::clearCLengthLaw();
  for (int i=0; i< _multipleLaws.size(); i++)
  {
    _multipleLaws[i]->clearCLengthLaw();
  }
}
void combinedPorousLaw::setCLengthLaw(const CLengthLaw& clength)
{
  mlawNonLocalPorosity::setCLengthLaw(clength);
  for (int i=0; i< _multipleLaws.size(); i++)
  {
    _multipleLaws[i]->setCLengthLaw(clength);
  }
}

void combinedPorousLaw::setShearPorosityGrowth_Factor(const double k)
{
  mlawNonLocalPorosity::setShearPorosityGrowth_Factor(k);
  for (int i=0; i< _multipleLaws.size(); i++)
  {
    _multipleLaws[i]->setShearPorosityGrowth_Factor(k);
  }
};
void combinedPorousLaw::setShearPorosityGrowth_StressTriaxialityFunction(const scalarFunction& fct)
{
  mlawNonLocalPorosity::setShearPorosityGrowth_StressTriaxialityFunction(fct);
  for (int i=0; i< _multipleLaws.size(); i++)
  {
    _multipleLaws[i]->setShearPorosityGrowth_StressTriaxialityFunction(fct);
  }
};
void combinedPorousLaw::setShearPorosityGrowth_LodeFunction(const scalarFunction& fct)
{
  mlawNonLocalPorosity::setShearPorosityGrowth_LodeFunction(fct);
  for (int i=0; i< _multipleLaws.size(); i++)
  {
    _multipleLaws[i]->setShearPorosityGrowth_LodeFunction(fct);
  }
};
void combinedPorousLaw::setFailureTolerance(const double NewFailureTol)
{
  mlawNonLocalPorosity::setFailureTolerance(NewFailureTol);
  for (int i=0; i< _multipleLaws.size(); i++)
  {
    _multipleLaws[i]->setFailureTolerance(NewFailureTol);
  }
};
void combinedPorousLaw::setCorrectedRegularizedFunction(const scalarFunction& fct)
{
  mlawNonLocalPorosity::setCorrectedRegularizedFunction(fct);
  for (int i=0; i< _multipleLaws.size(); i++)
  {
    _multipleLaws[i]->setCorrectedRegularizedFunction(fct);
  }
};
void combinedPorousLaw::setPostBlockedDissipationBehavior(const int method)
{
  mlawNonLocalPorosity::setPostBlockedDissipationBehavior(method);
  for (int i=0; i< _multipleLaws.size(); i++)
  {
    _multipleLaws[i]->setPostBlockedDissipationBehavior(method);
  }
};

void combinedPorousLaw::setSubStepping(const bool fl, const int maxNumStep)
{
  mlawNonLocalPorosity::setSubStepping(fl,maxNumStep);
  for (int i=0; i< _multipleLaws.size(); i++)
  {
    _multipleLaws[i]->setSubStepping(fl,maxNumStep);
  }
};

// Options setting -- temperature part
void combinedPorousLaw::setThermomechanicsCoupling(const bool fl)
{
  mlawNonLocalPorosity::setThermomechanicsCoupling(fl);
  for (int i=0; i< _multipleLaws.size(); i++)
  {
    _multipleLaws[i]->setThermomechanicsCoupling(fl);
  }
};
void combinedPorousLaw::setReferenceT(const double referenceT)
{
  mlawNonLocalPorosity::setReferenceT(referenceT);
  for (int i=0; i< _multipleLaws.size(); i++)
  {
    _multipleLaws[i]->setReferenceT(referenceT);
  }
};
void combinedPorousLaw::setThermalExpansionCoefficient(const double alp)
{
  mlawNonLocalPorosity::setThermalExpansionCoefficient(alp);
  for (int i=0; i< _multipleLaws.size(); i++)
  {
    _multipleLaws[i]->setThermalExpansionCoefficient(alp);
  }
};
void combinedPorousLaw::setReferenceThermalConductivity(const double KK)
{
  mlawNonLocalPorosity::setReferenceThermalConductivity(KK);
  for (int i=0; i< _multipleLaws.size(); i++)
  {
    _multipleLaws[i]->setReferenceThermalConductivity(KK);
  }
};
void combinedPorousLaw::setTemperatureFunction_ThermalConductivity(const scalarFunction& Tfunc)
{
  mlawNonLocalPorosity::setTemperatureFunction_ThermalConductivity(Tfunc);
  for (int i=0; i< _multipleLaws.size(); i++)
  {
    _multipleLaws[i]->setTemperatureFunction_ThermalConductivity(Tfunc);
  }
};
void combinedPorousLaw::setReferenceCp(const double Cp)
{
  mlawNonLocalPorosity::setReferenceCp(Cp);
  for (int i=0; i< _multipleLaws.size(); i++)
  {
    _multipleLaws[i]->setReferenceCp(Cp);
  }
};
void combinedPorousLaw::setTemperatureFunction_cp(const scalarFunction& Tfunc)
{
  mlawNonLocalPorosity::setTemperatureFunction_cp(Tfunc);
  for (int i=0; i< _multipleLaws.size(); i++)
  {
    _multipleLaws[i]->setTemperatureFunction_cp(Tfunc);
  }
};
void combinedPorousLaw::setThermalEstimationPreviousConfig(const bool flag)
{
  mlawNonLocalPorosity::setThermalEstimationPreviousConfig(flag);
  for (int i=0; i< _multipleLaws.size(); i++)
  {
    _multipleLaws[i]->setThermalEstimationPreviousConfig(flag);
  }
};
void combinedPorousLaw::setTaylorQuineyFactor(const double f)
{
  mlawNonLocalPorosity::setTaylorQuineyFactor(f);
  for (int i=0; i< _multipleLaws.size(); i++)
  {
    _multipleLaws[i]->setTaylorQuineyFactor(f);
  }
};
//
void combinedPorousLaw::setTemperatureFunction_rho(const scalarFunction& Tfunc)
{
  mlawNonLocalPorosity::setTemperatureFunction_rho(Tfunc);
  for (int i=0; i< _multipleLaws.size(); i++)
  {
    _multipleLaws[i]->setTemperatureFunction_rho(Tfunc);
  }
};
void combinedPorousLaw::setTemperatureFunction_E(const scalarFunction& Tfunc)
{
  mlawNonLocalPorosity::setTemperatureFunction_E(Tfunc);
  for (int i=0; i< _multipleLaws.size(); i++)
  {
    _multipleLaws[i]->setTemperatureFunction_E(Tfunc);
  }
};
void combinedPorousLaw::setTemperatureFunction_nu(const scalarFunction& Tfunc)
{
  mlawNonLocalPorosity::setTemperatureFunction_nu(Tfunc);
  for (int i=0; i< _multipleLaws.size(); i++)
  {
    _multipleLaws[i]->setTemperatureFunction_nu(Tfunc);
  }
};
void combinedPorousLaw::setTemperatureFunction_alphaDilatation(const scalarFunction& Tfunc)
{
  mlawNonLocalPorosity::setTemperatureFunction_alphaDilatation(Tfunc);
  for (int i=0; i< _multipleLaws.size(); i++)
  {
    _multipleLaws[i]->setTemperatureFunction_alphaDilatation(Tfunc);
  }
};

void combinedPorousLaw::setTangentByPerturbation(bool fl, double pr)
{
  mlawNonLocalPorosity::setTangentByPerturbation(fl,pr);
  for (int i=0; i< _multipleLaws.size(); i++)
  {
    _multipleLaws[i]->setTangentByPerturbation(fl,pr);
  }
}

void combinedPorousLaw::LpNorm(const double power, const fullVector<double>& X, double& F, 
                                     bool diff, fullVector<double>* DFDX,
                                     bool hess, fullMatrix<double>* DDFDXDX)
{
  double halfN = 0.5*power;
  double n = X.size();
  double Z = 0.;
  static fullVector<double> Xsq(n);
  for (int i=0; i< n; i++)
  {
    Xsq(i) = X(i)*X(i);
    if (Xsq(i) > 0.)
    {
      Z += pow(Xsq(i),halfN);
    }
  }
  double oneOverN = 1./power;
  F = 0.;
  if (Z >0.)
  {
    F = pow(Z,oneOverN);
  }
  if (diff)
  {
    if (Z > 0.)
    {
      //  first derivative of DZDX
      static fullVector<double> DZDX(n);
      for (int i=0; i< n; i++)
      {
        if (Xsq(i) > 0.)
        {
          DZDX(i) = halfN*pow(Xsq(i),halfN-1.)*(2.*X(i));
        }
        else
        {
          DZDX(i) = 0.;
        }
      }
      double dFdZ = oneOverN*pow(Z,oneOverN-1.);
      for (int i=0; i< n; i++)
      {
        (*DFDX)(i) = dFdZ*DZDX(i);
      }

      if (hess)
      {
        static fullVector<double> DDZDXDX(n);
        for (int i=0; i< n; i++)
        {
          if (Xsq(i) > 0.)
          {
            DDZDXDX(i) = halfN*(halfN-1.)*pow(Xsq(i),halfN-2.)*(2.*X(i))*(2.*X(i)) + halfN*pow(Xsq(i),halfN-1.)*2.; 
          }
          else
          {
            DDZDXDX(i) = 0.;
          }
        }
        double ddFdZdZ = oneOverN*(oneOverN-1.)*pow(Z,oneOverN-2.);
        
        DDFDXDX->setAll(0.);
        for (int i=0; i< n; i++)
        {
          (*DDFDXDX)(i,i) += dFdZ*DDZDXDX(i);
          for (int j=0; j< n; j++)
          {
            (*DDFDXDX)(i,j) += ddFdZdZ*DZDX(j)*DZDX(i);
          }
        }
      }
    }
    else
    {
      DFDX->setAll(0.);
      if (hess)
      {
        DDFDXDX->setAll(0.);
      }
    }
  }
};

void combinedPorousLaw::regulizationYieldFunctionBase(const double power, double p, const double s, double& f,
                          bool diff, double* dfdp , double* dfds,
                          bool hess, double* ddfdpdp, double* ddfdpds,double* ddfdsdp,double* ddfdsds) const{

  static fullVector<double> X(2);
  X(0) = p;
  X(1) = s;

  static fullVector<double> DFDX(2);
  static fullMatrix<double> DDFDXDX(2,2);

  combinedPorousLaw::LpNorm(power,X,f,diff,&DFDX,hess,&DDFDXDX);
  if (diff)
  {
    *dfdp = DFDX(0);
    *dfds = DFDX(1);
    if (hess)
    {
      *ddfdpdp  = DDFDXDX(0,0);
      *ddfdpds  = DDFDXDX(0,1);
      *ddfdsdp  = DDFDXDX(1,0);
      *ddfdsds  = DDFDXDX(1,1);
    }
  }
};

void combinedPorousLaw::regulizationYieldFunctionBase(const double power, double p, const double s, const double t, double& f,
                          bool diff, double* dfdp , double* dfds, double *dfdt,
                          bool hess, double* ddfdpdp, double* ddfdpds, double* ddfdpdt,
                          double* ddfdsdp,double* ddfdsds,double* ddfdsdt,
                          double* ddfdtdp,double* ddfdtds,double* ddfdtdt) const
{
  static fullVector<double> X(3);
  X(0) = p;
  X(1) = s;
  X(2) = t;

  static fullVector<double> DFDX(3);
  static fullMatrix<double> DDFDXDX(3,3);

  combinedPorousLaw::LpNorm(power,X,f,diff,&DFDX,hess,&DDFDXDX);
  if (diff)
  {
    *dfdp = DFDX(0);
    *dfds = DFDX(1);
    *dfdt = DFDX(2);
    if (hess)
    {
      *ddfdpdp  = DDFDXDX(0,0);
      *ddfdpds  = DDFDXDX(0,1);
      *ddfdpdt  = DDFDXDX(0,2);
      *ddfdsdp  = DDFDXDX(1,0);
      *ddfdsds  = DDFDXDX(1,1);
      *ddfdsdt  = DDFDXDX(1,2);
      *ddfdtdp  = DDFDXDX(2,0);
      *ddfdtds  = DDFDXDX(2,1);
      *ddfdtdt  = DDFDXDX(2,2);
    }
  }
}

void mlawNonLocalPorosity::getYieldDecrease(double hatpm, double &wp, double &dwp) const
{
  wp=0.;
  dwp=0.;
  if(getPlasticInstabilityFunctionLaw()!=NULL)
  {
    getPlasticInstabilityFunctionLaw()->getPlasticInstability(hatpm,wp,dwp);
  }
}

