//
// Description: Define damage law for cohesive layer
//
// Author:  <V.D. Nguyen>, (C) 2017
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef COHESIVEDAMAGELAW_H_
#define COHESIVEDAMAGELAW_H_

#include "ipCohesive.h"

class cohesiveDamageLaw{
	public:
		enum cohesiveDamageLawName{Zero=0, Bilinear=1, Exponential=2};
	protected:
		int _num;
 
	public:
		#ifndef SWIG
		cohesiveDamageLaw(const int num):_num(num) {};
		cohesiveDamageLaw(const cohesiveDamageLaw& src): _num(src._num){};
		virtual ~cohesiveDamageLaw(){};
		virtual int getNum() const{return _num;};
    virtual void createIPVariable(IPCohesiveDamage* &ipv) const=0;
		virtual void computeDamage(const double pi, const double pc,  // initial and critical effective jump
															 const double p0, const double p1,  // previous and current maximal effective jump
															 const IPCohesiveDamage& ipprev, IPCohesiveDamage& ipcur,
															 const bool stiff) const = 0; // derivative in respecto reffective jump
		virtual cohesiveDamageLaw::cohesiveDamageLawName getType() const = 0;
		virtual cohesiveDamageLaw* clone() const = 0;
		#endif // SWIG
};

class ZeroCohesiveDamageLaw : public cohesiveDamageLaw{
	
	public:
		ZeroCohesiveDamageLaw(const int num);
		#ifndef SWIG
		ZeroCohesiveDamageLaw(const ZeroCohesiveDamageLaw& src);
		virtual ~ZeroCohesiveDamageLaw(){};
    virtual void createIPVariable(IPCohesiveDamage* &ipv) const 
    { 
      if (ipv) delete ipv;
      ipv = new IPZeroCohesiveDamage();
    }
		virtual void computeDamage(const double pi, const double pc, 
															 const double p0, const double p1, 
															 const IPCohesiveDamage& ipprev, IPCohesiveDamage& ipcur,
															 const bool stiff) const;
		virtual cohesiveDamageLaw::cohesiveDamageLawName getType() const {return cohesiveDamageLaw::Zero;};
		virtual cohesiveDamageLaw* clone() const {return new ZeroCohesiveDamageLaw(*this);};
		#endif // SWIG
}; 


class BilinearCohesiveDamageLaw : public cohesiveDamageLaw{
	
	public:
		BilinearCohesiveDamageLaw(const int num);
		#ifndef SWIG
		BilinearCohesiveDamageLaw(const BilinearCohesiveDamageLaw& src);
		virtual ~BilinearCohesiveDamageLaw(){};
    virtual void createIPVariable(IPCohesiveDamage* &ipv) const 
    { 
      if (ipv) delete ipv;
      ipv = new IPBiLinearCohesiveDamage();
    }
		virtual void computeDamage(const double pi, const double pc, 
															 const double p0, const double p1, 
															 const IPCohesiveDamage& ipprev, IPCohesiveDamage& ipcur,
															 const bool stiff) const;
		virtual cohesiveDamageLaw::cohesiveDamageLawName getType() const {return cohesiveDamageLaw::Bilinear;};
		virtual cohesiveDamageLaw* clone() const {return new BilinearCohesiveDamageLaw(*this);};
		#endif // SWIG
};

class ExponentialCohesiveDamageLaw  : public cohesiveDamageLaw{
  protected:
    double _c;
	public:
		ExponentialCohesiveDamageLaw(const int num, double cc=1.);
    double getPowerParameter() const {return _c;};
		#ifndef SWIG
		ExponentialCohesiveDamageLaw(const ExponentialCohesiveDamageLaw& src);
		virtual ~ExponentialCohesiveDamageLaw(){};
    virtual void createIPVariable(IPCohesiveDamage* &ipv) const 
    { 
      if (ipv) delete ipv;
      ipv = new IPExponentialCohesiveDamage(_c);
    }
		virtual void computeDamage(const double pi, const double pc, 
															 const double p0, const double p1, 
															 const IPCohesiveDamage& ipprev, IPCohesiveDamage& ipcur, 
															 const bool stiff) const;
		virtual cohesiveDamageLaw::cohesiveDamageLawName getType() const {return cohesiveDamageLaw::Exponential;};
		virtual cohesiveDamageLaw* clone() const {return new ExponentialCohesiveDamageLaw(*this);};
		#endif // SWIG
};


#endif //COHESIVEDAMAGELAW_H_