#ifndef J2PLAST_C
#define J2PLAST_C 1

#ifndef max
#define max(a,b) ( ((a) > (b)) ? (a) : (b) )
#endif

#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "j2plast.h"
#include "matrix_operations.h"
#include "elasticlcc.h"

#ifdef NONLOCALGMSH
#include "ID.h"
using namespace MFH;
#endif

#define DP_MIN 1e-60


//ELASTO-PLASTIC CONSTITUTIVE BOX
int constbox_ep(double E, double nu, double sy0, int htype, double hmod1, double hmod2, double hp0,
	double hexp, double *dstrn, double *strs_n, double *strs, double* pstrn_n, double* pstrn,
	double p_n, double *p, double *twomu_iso, double* dtwomu_iso,  double** Calgo, double*** dCalgo, double* dpdE, bool residual)

{
	return(0);
}

//****************************************************
/**********J2 plasticity for large deformation***/
//******************************************************
int constbox_ep(double E, double nu, double sy0, int htype, double hmod1, double hmod2, double hp0, double hexp, double *strstr, double *strs, double* pstrn_n, double* pstrn, double p_n, double *p,  double** Calgo, double*** dCalgo, double* dpdE, bool residual)

{

	return(0);
}

//PRESSURE SENSITIVE ELASTO-PLASTIC CONSTITUTIVE BOX
int constbox_ep(double E, double nu, double sy0, int htype, double hmod1, double hmod2, double hp0, double alpha_DP, double m_DP, double nup, double hexp, double *dstrn, double *strs_n, double *strs, double* pstrn_n, double* pstrn, double p_n, double *p, double *twomu_iso, double* dtwomu_iso, double *twok_iso, double* dtwok_iso, double** Calgo, double*** dCalgo, double* dpdE, bool residual)
{
  return (0);

}

//end elastoPlastic for large deformation


// EVALUATION OF THE HARDENING FUNCTION 
void j2hard (double p, double sy0, int htype, double hmod1, double hmod2, double hp0, double hexp, double* R, double* dR, double* ddR){

 
}

/***********************************************
 * COMPUTATION OF ALGORITHMIC TANGENT OPERATOR
 * FROM STATE VARIABLES 
 * ********************************************/
void algoJ2(double E, double nu, double sy0, int htype, double hmod1, double hmod2,  double hp0, double hexp, 
	double* strs, double p, double dp, double** Calgo){



}



//ELASTO-PLASTIC CONSTITUTIVE BOX for second moment mehtod
int constbox_ep(double E, double nu, double sy0, int htype, double hmod1, double hmod2, double hp0, double hexp, double *DE, double *dstrn, double *strs_n, double eq2strs_n, double *eq2strs, double dstrseq_tr, double *dstrseq_trdDE, double *strs, double* pstrn_n, double* pstrn, double p_n, double *p, FY2nd *FY2){
return 0;
}



/////**********************************************///////
//****  ELASTO-VISCOPLASTIC CONSTITUTIVE BOX  ************
/////*******  07.2016, By Ling Wu    **************///////
/////**********************************************///////

int constbox_evp(double E, double nu, double sy0, int htype, double hmod1, double hmod2, double hp0,
	double hexp, int viscmodel, double vmod, double vstressy, double vexp, double *dstrn, double *strs_n, double *strs, double* pstrn_n, 
        double* pstrn, double p_n, double *p, double *twomu_iso, double* dtwomu_iso,  double** Calgo, double*** dCalgo, 
        double* dpdE, bool residual, double dt)

{
  return (0);
}


//ELASTO-VISCOPLASTIC CONSTITUTIVE BOX for second moment mehtod
int constbox_evp(double E, double nu, double sy0, int htype, double hmod1, double hmod2, double hp0, double hexp, int viscmodel, double vmod, double vstressy, double vexp, double *DE, double *dstrn, double *strs_n, double eq2strs_n, double *eq2strs, double dstrseq_tr, double *dstrseq_trdDE, double *strs, double* pstrn_n, double* pstrn, double p_n, double *p, FY2nd *FY2, double dt, ELcc *LCC){

	return(0);
}

//****************************************************
/**********J2 viscoplasticity for large deformation***/
//******************************************************
int constbox_evp(double E, double nu, double sy0, int htype, double hmod1, double hmod2, double hp0,
	double hexp, int viscmodel, double vmod, double vstressy, double vexp, double *strs_tr, double *strs, double* pstrn_n, 
        double* pstrn, double p_n, double *p, double** Calgo, double*** dCalgo, double* dpdE, bool residual, double dt)

{
	return(0);
}


/***********************************************************
 * SOLVE VISCOPLASTIC FUNCTION gv *
 **********************************************************/

void solve_gv(int viscmodel, double vmod, double vstressy, double vexp, double mu, double strseq, double yldstrs, double dt, double R, double dR, double ddR, double *gv, double *hv, double *dgdf, double *dhvdSeq, double *dhvdp, double eq2strs_n){


	return;
}
	




#endif








