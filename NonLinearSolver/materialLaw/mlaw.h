//
// C++ Interface: terms
//
// Description: Define material law
//
//
// Author:  <Gauthier BECKER>, (C) 2010
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef _MLAW_H_
#define _MLAW_H_
#ifndef SWIG
#include "ipstate.h"
#include "MElement.h"
#include "STensorOperations.h"
#endif

class nonLinearMechSolver;

class materialLaw{
 public :


    enum matname{linearElasticPlaneStress,linearElasticPlaneStressWithDamage, linearElasticOrthotropicPlaneStress,
                 cohesiveLaw, fracture, nonLinearShell, J2, viscoelastic, EOS, transverseIsotropic, transverseIsoCurvature, transverseIsoUserDirection,
                 transverseIsoYarnB, Anisotropic, AnisotropicStoch, nonLocalDamage, vumat, UMATInterface, crystalPlasticity, gursonUMAT,
                 numeric, secondOrderElastic, j2smallstrain, porousPlasticDamagelaw,
                 nonLocalDamageGurson, nonLocalDamageThomason, nonLocalDamageGursonThomason,
                 nonLocalDamageJ2Hyper, nonLocalDamageIsotropicElasticity, localDamageIsotropicElasticity,
                 LinearThermoMechanics,J2ThermoMechanics,nonLocalDamageJ2FullyCoupledThermoMechanics,SMP,PhenomenologicalSMP,LinearElecTherMech,AnIsotropicElecTherMech,
                 hyperelastic, powerYieldLaw, powerYieldLawWithFailure,nonLocalDamagePowerYieldHyper,
                 localDamagePowerYieldHyperWithFailure,nonLocalDamagePowerYieldHyperWithFailure,ElecSMP,
                 ThermalConducter,AnIsotropicTherMech, localDamageJ2Hyper,linearElastic,nonLocalDamageGursonThermoMechanics,
                 localDamageJ2SmallStrain,nonLocalDamageJ2SmallStrain,cluster,tfa,ANN, DMN, torchANN, NonlocalDamageTorchANN, StochDMN, LinearElecMagTherMech, LinearElecMagInductor, hyperviscoelastic, GenericResidualStrain,
                 GenericThermoMechanics, ElecMagGenericThermoMechanics, ElecMagInductor,
                 nonlineartvm,nonlinearTVE,nonlinearTVP,vevpmfh, VEVPUMAT, IMDEACPUMAT, Hill48,nonlinearTVEnonlinearTVP,nonlinearTVEnonlinearTVP2,
				 TVEVPwithFailure,nonLocalDamageTVEVP,localDamageTVEVPWithFailure,nonLocalDamageTVEVPWithFailure,
                 MultipleLaws, genericCrackPhaseField};


 protected :
  int _num; // number of law (must be unique !)
  bool _initialized; // to initialize law
  double _timeStep; // for law which works on increment. (Use only in so no getTimeStep function)
  double _currentTime; // To save results vs time



#ifndef SWIG
	const nonLinearMechSolver* _macroSolver; // solver that this material belongs to

 public:
  // constructor
  materialLaw(const int num, const bool init=true);
  virtual ~materialLaw(){}
  materialLaw(const materialLaw &source);
  virtual materialLaw& operator=(const materialLaw &source);
  virtual int getNum() const{return _num;}
  virtual void setTime(const double ctime,const double dtime){_timeStep = dtime; _currentTime = ctime;}
  virtual matname getType() const=0;
  virtual void createIPState(IPStateBase* &ips,bool hasBodyForce, const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const IntPt *GP=NULL, const int gpt = 0) const=0;

  //allows getting the real material law for dG3DMaterialLaw etc
  virtual const materialLaw *getConstNonLinearSolverMaterialLaw() const      {
      return this;
      } // =0;
  virtual materialLaw *getNonLinearSolverMaterialLaw() {
      return this;
      }; //=0;

  virtual void initLaws(const std::map<int,materialLaw*> &maplaw)=0;
  virtual bool isInitialized() const {return _initialized;}
  // for explicit scheme it must return sqrt(E/rho) adapted to your case
  virtual double soundSpeed() const=0;
  virtual double density() const{return 0.;}
  virtual double nLDensity() const{return 0.;}
  // for multiscale analysis
  virtual double scaleFactor() const { return 1.;};
  virtual bool isHighOrder() const { return false;};
  virtual bool isNumeric() const {return false;};
  virtual double getTimeStep() const {return _timeStep;}
  virtual double getTime() const{return _currentTime;}
  virtual int getNumberOfExtraDofsDiffusion() const {return 0;}
  virtual double getInitialExtraDofStoredEnergyPerUnitField() const {return 0.;}
  virtual double getExtraDofStoredEnergyPerUnitField(double T) const {return 0.;}
  virtual double getCharacteristicLength() const {return 0.;};
	virtual void setMacroSolver(const nonLinearMechSolver* sv){_macroSolver = sv;};
	virtual const nonLinearMechSolver* getMacroSolver() const {return _macroSolver;};

  virtual bool withEnergyDissipation() const = 0;
  // function to define when one IP is broken for block dissipation
  virtual bool brokenCheck(const IPVariable* ipv) const { return false;}
  virtual void checkInternalState(IPVariable* ipv, const IPVariable* ipvprev) const = 0;
	virtual materialLaw* clone() const = 0;

  virtual void constitutive(const STensor3& F0,         // initial deformation gradient (input @ time n)
                            const STensor3& Fn,         // updated deformation gradient (input @ time n+1)
                            STensor3 &P,                // updated 1st Piola-Kirchhoff stress tensor (output)
                                                        // contains the initial values on input
                            const IPVariable *q0,       // array of initial internal variable
                            IPVariable *q1,              // updated array of internal variable (in ipvcur on output),
			     STensor43 &Tangent,         // constitutive tangents (output)
			     const bool stiff,
                            STensor43* elasticTangent = NULL,
                            const bool dTangent=false,
                            STensor63* dCalgdeps = NULL) const=0;

  virtual void constitutiveTFA(const STensor3& F0,         // initial deformation gradient (input @ time n)
                            const STensor3& Fn,         // updated deformation gradient (input @ time n+1)
                            STensor3 &P,                // updated 1st Piola-Kirchhoff stress tensor (output)
                                                        // contains the initial values on input
                            const IPVariable *q0,       // array of initial internal variable
                            IPVariable *q1,              // updated array of internal variable (in ipvcur on output),
			     STensor43 &Tangent,         // constitutive tangents (output)
                            STensor3 &epsEig,
                            STensor43 &depsEigdeps) const
                            {
                               Msg::Error("constitutiveTFA not implemented for this material law");
                               Msg::Exit(0);
                            };

  virtual void constitutiveTFA_corr(
                            const STensor3& F0,         // initial deformation gradient (input @ time n)
                            const STensor3& Fn,         // updated deformation gradient (input @ time n+1)
                            const STensor3& Fn_min,         // updated deformation gradient (input @ time n+1)
                            const STensor3& Fn_max,         // updated deformation gradient (input @ time n+1)
                            STensor3 &P,                // updated 1st Piola-Kirchhoff stress tensor (output)
                                                        // contains the initial values on input
                            const IPVariable *q0,       // array of initial internal variable
                            IPVariable *q1,              // updated array of internal variable (in ipvcur on output),
			     STensor43 &Tangent,         // constitutive tangents (output)
                            STensor3 &epsEig,
                            STensor43 &depsEigdeps) const
                            {
                               Msg::Error("constitutiveTFA not implemented for this material law");
                               Msg::Exit(0);
                            };

  virtual void constitutiveTFA_incOri(const STensor3& F0,         // initial deformation gradient (input @ time n)
                            const STensor3& Fn,         // updated deformation gradient (input @ time n+1)
                            const fullVector<double>& euler,
                            STensor3 &P,                // updated 1st Piola-Kirchhoff stress tensor (output)
                                                        // contains the initial values on input
                            const IPVariable *q0,       // array of initial internal variable
                            IPVariable *q1,              // updated array of internal variable (in ipvcur on output),
			     STensor43 &Tangent,         // constitutive tangents (output)
                            STensor3 &epsEig,
                            STensor43 &depsEigdeps) const
                            {
                               Msg::Error("constitutiveTFA_incOri not implemented for this material law");
                               Msg::Exit(0);
                            };

  virtual void constitutiveIsotropicTangentTFA(const STensor3& F0,         // initial deformation gradient (input @ time n)
                            const STensor3& Fn,         // updated deformation gradient (input @ time n+1)
                            STensor3 &P,                // updated 1st Piola-Kirchhoff stress tensor (output)
                                                        // contains the initial values on input
                            const IPVariable *q0,       // array of initial internal variable
                            IPVariable *q1,              // updated array of internal variable (in ipvcur on output),
			     STensor43 &Tangent,         // constitutive tangents (output)
                            STensor3 &epsEig,
                            STensor43 &depsEigdeps) const
                            {
                               Msg::Error("constitutiveIsotropicTangentTFA not implemented for this material law");
                               Msg::Exit(0);
                            };

  virtual void constitutiveResidualSecantTFA(
                            const STensor3& F0,         // previous deformation gradient (input @ time n)
                            const STensor3& Fn,         // current deformation gradient (input @ time n+1)
                            STensor3 &P,                // current 1st Piola-Kirchhoff stress tensor (output)
                            const IPVariable *q_unloaded,       // array of previous internal variables
                            IPVariable *q1,             // current array of internal variable (in ipvcur on output),
                            STensor43 &Secant,         // constitutive tangents (output)
                            STensor3 &epsEig,
                            STensor43 &depsEigdeps,
                            const bool tangent_compute =false,
                            STensor43* Tangent = NULL) const
                            {
                               Msg::Error("constitutiveResidualSecantTFA not implemented for this material law");
                               Msg::Exit(0);
                            };

  virtual void constitutiveResidualSecant(
                            const STensor3& F0,         // previous deformation gradient (input @ time n)
                            const STensor3& Fn,         // current deformation gradient (input @ time n+1)
                            STensor3 &P,                // current 1st Piola-Kirchhoff stress tensor (output)
                            const IPVariable *q_unloaded,       // array of previous internal variables
                            IPVariable *q1,             // current array of internal variable (in ipvcur on output),
                            STensor43 &Secant,         // tangents (output)
                            const bool stiff,            // if true compute the tangents
                            STensor43* elasticTangent = NULL,
                            const bool dSecant =false,
                            STensor63* dCsecdeps = NULL,
                            const bool Calg_compute =false,
                            STensor43* C_algo = NULL) const
                            {
                               Msg::Error("constitutiveResidualSecant not implemented for this material law");
                               Msg::Exit(0);
                            };

  virtual void constitutiveZeroSecant(
                            const STensor3& F0,         // previous deformation gradient (input @ time n)
                            const STensor3& Fn,         // current deformation gradient (input @ time n+1)
                            STensor3 &P,                // current 1st Piola-Kirchhoff stress tensor (output)
                            const IPVariable *q_unloaded,       // array of previous internal variables
                            IPVariable *q1,             // current array of internal variable (in ipvcur on output),
                            STensor43 &Secant,         // tangents (output)
                            const bool stiff,            // if true compute the tangents
                            STensor43* elasticTangent = NULL,
                            const bool dSecant =false,
                            STensor63* dCsecdeps = NULL,
                            const bool Calg_compute =false,
                            STensor43* C_algo = NULL) const
                            {
                               Msg::Error("constitutiveZeroSecant not implemented for this material law");
                               Msg::Exit(0);
                            };


  virtual void constitutiveVirtualUnloading(
                            const STensor3& F0,         // previous deformation gradient (input @ time n)
                            const STensor3& F_res,         // current deformation gradient (input @ time n+1)
                            STensor3 &P_res,                // current 1st Piola-Kirchhoff stress tensor (output)
                            const IPVariable *q0,       // array of previous internal variables
                            IPVariable *q_unloaded        // current array of internal variable (in ipvcur on output),
                            ) const
                            {
                               Msg::Error("constitutiveVirtualUnloading not implemented for this material law");
                               Msg::Exit(0);
                            };
  virtual void ElasticStiffness(STensor43* elasticTensor) const
                            {
                               Msg::Error("ElasticStiffness not implemented for this material law");
                               Msg::Exit(0);
                            };

  virtual double ElasticYoungModulus() const {return 0.;};
  virtual double ElasticShearModulus() const {return 0.;};
  virtual double ElasticBulkModulus() const {return 0.;};
  virtual double TangentShearModulus(const IPVariable *q0, const IPVariable *q1) const {return 0.;};
  virtual double SecantShearModulus(const IPVariable *q1) const {return 0.;};
  virtual double ResidualSecantShearModulus(const IPVariable *q_unloaded, const IPVariable *q1) const {return 0.;};
  virtual double ZeroResidualSecantShearModulus(const IPVariable *q_unloaded, const IPVariable *q1) const {return 0.;};

  virtual void ElasticStiffnessIsotropic(STensor43& ElasticStiffness) const
                            {
                               Msg::Error("ElasticStiffnessIsotropic not implemented for this material law");
                               Msg::Exit(0);
                            };
  virtual void TangentStiffnessIsotropic(const IPVariable *q0i, const IPVariable *q1i, STensor43& Tangent) const
                            {
                               Msg::Error("TangentStiffnessIsotropic not implemented for this material law");
                               Msg::Exit(0);
                            };
  virtual void SecantStiffnessIsotropic(const IPVariable *q1i, STensor43& Secant) const
                            {
                               Msg::Error("SecantStiffnessIsotropic not implemented for this material law");
                               Msg::Exit(0);
                            };
  virtual void ResidualSecantStiffnessIsotropic(const IPVariable *q_unloadedi, const IPVariable *q1i, STensor43& Secant) const
                            {
                               Msg::Error("ResidualSecantStiffnessIsotropic not implemented for this material law");
                               Msg::Exit(0);
                            };
  virtual void ZeroResidualSecantStiffnessIsotropic(const IPVariable *q_unloadedi, const IPVariable *q1i, STensor43& Secant) const
                            {
                               Msg::Error("ZeroResidualSecantStiffnessIsotropic not implemented for this material law");
                               Msg::Exit(0);
                            };

  virtual double getH(const IPVariable *q0, const IPVariable *q1) const {return 0.;};
  virtual double getPlasticEqStrainIncrement(const IPVariable *q0i, const IPVariable *q1i) const {return 0.;};
  virtual double getPlasticEqStrain(const IPVariable *qi) const {return 0.;};
  virtual bool materialLawHasLocalOrientation() const {return false;}
  virtual void getLocalOrientation(fullVector<double> &GaussP, fullVector<double> &vecEulerAngles) const
                            {
                               Msg::Error("getLocalOrientation not implemented for this material law");
                            };



#endif
};
class materialLaw2LawsInitializer{
#ifndef SWIG
 public:
  materialLaw2LawsInitializer(){}
  virtual ~materialLaw2LawsInitializer(){}
  materialLaw2LawsInitializer(const materialLaw2LawsInitializer &source){}
  virtual void initialBroken(IPStateBase *ips) const=0;
  virtual int bulkLawNumber() const=0;
  virtual int fractureLawNumber() const=0;
  virtual bool fullBroken(const IPVariable *ipv) const=0; // has to return true if the fracture process is completed
                                                    // for a given state so it is a ipvariable and not a IPState
                                                    // used for crack tracking algorithms
  virtual double timeInitBroken(const IPVariable *ipv)const{return 0.;} // To known the time when crack has initiate at the Gauss point
#endif // SWIG
};
// If you used two laws your class has to be derived from this one
template<class Tbulk, class Tfrac>class fractureBy2Laws : public materialLaw2LawsInitializer{
#ifndef SWIG
 protected:
  const int _nbulk;
  const int _nfrac;
  Tbulk *_mbulk;
  Tfrac *_mfrac;
 public:
  fractureBy2Laws(const int num, const int nbulk, const int nfrac) : materialLaw2LawsInitializer(),
                                                                       _nbulk(nbulk), _nfrac(nfrac),
                                                                       _mbulk(NULL), _mfrac(NULL){}
  fractureBy2Laws(const fractureBy2Laws &source) : materialLaw2LawsInitializer(source), _nbulk(source._nbulk),
                                                    _nfrac(source._nfrac),
                                                    _mbulk(source._mbulk), _mfrac(source._mfrac){}
  virtual ~fractureBy2Laws(){};
  virtual int bulkLawNumber() const{return _nbulk;}
  virtual int fractureLawNumber() const{return _nfrac;}
  virtual const Tbulk* getBulkLaw() const{return _mbulk;}
  virtual Tbulk* getBulkLaw(){return _mbulk;}
  virtual const Tfrac* getFractureLaw() const{return _mfrac;}
  virtual Tfrac* getFractureLaw(){return _mfrac;}
  virtual void initLaws(const std::map<int,materialLaw*> &maplaw)
  {
    bool findb=false;
    bool findc=false;
    for(std::map<int,materialLaw*>::const_iterator it = maplaw.begin(); it != maplaw.end(); ++it)
    {
      int num = it->first;
      if(num == _nbulk){
        findb=true;
        it->second->initLaws(maplaw);
        _mbulk = static_cast<Tbulk*>(it->second);
      }
      if(num == _nfrac){
        findc=true;
        it->second->initLaws(maplaw);
        _mfrac =NULL;
        _mfrac = dynamic_cast<Tfrac *> (it->second);
        if(_mfrac==NULL) Msg::Error("The material law %d is not a cohesive law",_nfrac);
      }
    }
    if(!findb) Msg::Error("The bulk law is not initialize for fracture2Law number %d",_nbulk);
    if(!findc and _nfrac!=-10000000) Msg::Error("The cohesive law is not initialize for fractureBy2Law number %d",_nfrac); // for old damage law change this
  }
  virtual void initialBroken(IPStateBase *ips) const=0;
  virtual bool fullBroken(const IPVariable *ipv)const=0;
#endif // SWIG
};
#endif //MLAW_H_
