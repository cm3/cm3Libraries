//
// C++ Interface: Material Law
//
// Description: Non-Linear Thermo-Visco-Mechanics (Thermo-ViscoElasto-ViscoPlasto Law) with Non-Local Damage Interface (soon.....)
//
// Author: <Ujwal Kishore J - FLE_Knight>, (C) 2024
//
// Copyright: See COPYING file that comes with this distribution
//
//

#include "nonLinearMechSolver.h"
#include "mlawNonLinearTVENonLinearTVP.h"


mlawNonLinearTVENonLinearTVP::mlawNonLinearTVENonLinearTVP(const int num,const double E,const double nu, const double rho, const double tol,
                                   const double Tinitial, const double Alpha, const double KThCon, const double Cp,
                                   const bool matrixbyPerturbation, const double pert, const double stressIteratorTol, const bool thermalEstimationPreviousConfig):
                                   mlawNonLinearTVP(num, E, nu, rho, tol, Tinitial, Alpha, KThCon, Cp, matrixbyPerturbation, pert, thermalEstimationPreviousConfig), _stressIteratorTol(stressIteratorTol) {
};

mlawNonLinearTVENonLinearTVP::mlawNonLinearTVENonLinearTVP(const mlawNonLinearTVENonLinearTVP& src): mlawNonLinearTVP(src), _stressIteratorTol(src._stressIteratorTol) {
};

mlawNonLinearTVENonLinearTVP& mlawNonLinearTVENonLinearTVP::operator=(const materialLaw& source){

    mlawNonLinearTVP::operator=(source);
    const mlawNonLinearTVENonLinearTVP* src =dynamic_cast<const mlawNonLinearTVENonLinearTVP*>(&source);
    if(src != NULL){
    	_stressIteratorTol = src->_stressIteratorTol;
    }
    return *this;
};

mlawNonLinearTVENonLinearTVP::~mlawNonLinearTVENonLinearTVP(){
};

double mlawNonLinearTVENonLinearTVP::freeEnergyPlasticity(const IPNonLinearTVP *q0, IPNonLinearTVP *q1, const double& T0, const double& T) const{

	double Psy_TVP(0.);

	// 1. Kinematic Hardening
    double Hb(0.);
    if (q1->_ipKinematic != NULL){
       Hb = q1->_ipKinematic->getR(); // kinematic hardening parameter
   	}

    double kk = 1./sqrt(1.+2.*q1->_nup*q1->_nup);
    static STensor3 alpha;
    STensorOperation::zero(alpha);
    if(Hb>0.){
    	alpha = q1->_backsig;
    	alpha *= 1/(pow(kk,1)*Hb); //pow(kk,2) debug
    }
	STensorOperation::doubleContractionSTensor3(q1->_backsig,alpha,Psy_TVP);

	// 2. Isotropic Hardening
	double intR = q1->_ipCompression->getIntegR();
	double sigc0 = q1->_ipCompression->getR0();
	intR -= sigc0*q1->_epspbarre;

	Psy_TVP += intR;

    // If mullins, then calculate psi_VP derivatives
    // if (_mullinsEffect != NULL && q._ipMullinsEffect != NULL){
    //    *DpsiDT = 0.;
    //    STensorOperation::zero(*DpsiDE);
    // }
    return Psy_TVP;
}

void mlawNonLinearTVENonLinearTVP::freeEnergyPlasticityDerivatives(const IPNonLinearTVP *q0, IPNonLinearTVP *q1, const double& T0, const double& T,
                              STensor3& dPsy_TVPdF, double& dPsy_TVPdT) const{

	STensorOperation::zero(dPsy_TVPdF);
	dPsy_TVPdT = 0.;

	// 1. Kinematic Hardening
	double Hb(0.), dHb(0.), dHbdT(0.);
	if (q1->_ipKinematic != NULL){
	  Hb = q1->_ipKinematic->getR(); // kinematic hardening parameter
	  dHb = q1->_ipKinematic->getDR(); // kinematic hardening parameter derivative (dHb/dgamma)
	  dHbdT = q1->_ipKinematic->getDRDT();  // make sure to normalise DRDT while defining in python file
	}

    double kk = 1./sqrt(1.+2.*q1->_nup*q1->_nup);
    if(Hb>0.){

    	static STensor3 term1, term2;
    	STensorOperation::multSTensor3STensor43(q1->_backsig,q1->_DbackSigDF,term1);
    	term2 = q1->_DgammaDF;
    	term2 *= (-1./(2*Hb)*dHb*q1->_backsig.dotprod());

    	dPsy_TVPdF = term1;
    	dPsy_TVPdF += term2;
    	dPsy_TVPdF *= 1./(pow(kk,1)*Hb); //pow(kk,2) debug

    	double term3, term4;
    	STensorOperation::doubleContractionSTensor3(q1->_backsig,q1->_DbackSigDT,term3);
    	term4 = -(dHbdT + dHb*q1->_DgammaDT)/(2*Hb)*q1->_backsig.dotprod();
    	dPsy_TVPdT = (term3+term4)/(pow(kk,1)*Hb); //pow(kk,2) debug

    }


	// 2. Isotropic Hardening

    // Get Hc, etc.
	double intR = q1->_ipCompression->getIntegR();
	double sigc0 = q1->_ipCompression->getR0();  // Initial yield stress
	intR -= sigc0*q1->_epspbarre;

    double R(0.), dsigc0dT(0.), sigc(0.);
    dsigc0dT = sigc0*_temFunc_Sy0->getDiff(T);
    sigc = q1->_ipCompression->getR();

    // Get R
    R = sigc - sigc0;

    dPsy_TVPdF += R*q1->_DgammaDF;
    dPsy_TVPdT += (R*q1->_DgammaDT + _temFunc_Sy0->getDiff(T)/ _temFunc_Sy0->getVal(T)*intR - dsigc0dT*q1->_epspbarre);
}

void mlawNonLinearTVENonLinearTVP::constitutive( // This function is just a placeholder, defined in the pure virtual class -> does nothing, its never called.
                            const STensor3& F0,         // previous deformation gradient (input @ time n)
                            const STensor3& F1,         // current deformation gradient (input @ time n+1)
                                  STensor3 &P1,                // current 1st Piola-Kirchhoff stress tensor (output)
                            const IPVariable *q0i,       // array of previous internal variables
                                  IPVariable *q1i,             // current array of internal variable (in ipvcur on output),
                                  STensor43 &Tangent,         // tangents (output)
                            const bool stiff,            // if true compute the tangents
                                  STensor43* elasticTangent,
                            const bool dTangent,
                                  STensor63* dCalgdeps) const{
    static SVector3 gradT, temp2;
    static STensor3 temp3;
    static STensor33 temp33;
    static double tempVal;
    static STensor43 dFpdF, dFedF;
    static STensor3 dFpdT, dFedT;

    const IPNonLinearTVP *q0 = dynamic_cast<const IPNonLinearTVP *> (q0i);
          IPNonLinearTVP *q1 = dynamic_cast<IPNonLinearTVP *>(q1i);

    if(elasticTangent != NULL) Msg::Error("mlawNonLinearTVENonLinearTVP elasticTangent not defined");
    predictorCorrector_ThermoViscoPlastic(F0,F1,P1,q0,q1,Tangent,dFpdF, dFpdT, dFedF, dFedT,
                                          _Tref,_Tref,gradT,gradT,temp2,temp3,temp3,temp2,temp33,
                                          tempVal,tempVal,temp3,
                                          tempVal,tempVal,temp3,
                                          stiff, NULL);
};

void mlawNonLinearTVENonLinearTVP::constitutive(
                            const STensor3& F0,                     // initial deformation gradient (input @ time n)
                            const STensor3& F1,                     // updated deformation gradient (input @ time n+1)
                                  STensor3& P,                      // updated 1st Piola-Kirchhoff stress tensor (output)
                            const IPVariable *q0i,                   // array of initial internal variable
                                  IPVariable *q1i,                   // updated array of internal variable (in ipvcur on output),
                                  STensor43 &Tangent,               // constitutive mechanical tangents (output)
                            const double& T0,                       // previous temperature
                            const double& T,                        // temperature
                            const SVector3 &gradT0,                 // previous temperature gradient
                            const SVector3 &gradT,                  // temperature gradient
                                  SVector3 &fluxT,                  // temperature flux
                                  STensor3 &dPdT,                   // mechanical-thermal coupling
                                  STensor3 &dfluxTdgradT,           // thermal tengent
                                  SVector3 &dfluxTdT,
                                  STensor33 &dfluxTdF,              // thermal-mechanical coupling
                                  double &thermalSource,            // - Cp*dTdt
                                  double &dthermalSourcedT,         // thermal source
                                  STensor3 &dthermalSourcedF,
                                  double &mechanicalSource,         // mechanical source--> convert to heat
                                  double &dmechanicalSourcedT,
                                  STensor3 &dmechanicalSourceF,
                            const bool stiff,
                                  STensor43* elasticTangent) const{

    const IPNonLinearTVP *q0 = dynamic_cast<const IPNonLinearTVP *>(q0i);
          IPNonLinearTVP *q1 = dynamic_cast<IPNonLinearTVP *>(q1i);

    // Declaring here, coz I dont know better.
    static STensor43 dFedF, dFpdF;
    static STensor3 dFpdT, dFedT;

    if (!_tangentByPerturbation){

        this->predictorCorrector_ThermoViscoPlastic(F0, F1, P, q0, q1, Tangent, dFpdF, dFpdT, dFedF, dFedT,
                                                    T0, T, gradT0, gradT, fluxT, dPdT, dfluxTdgradT, dfluxTdT, dfluxTdF,
                                                    thermalSource, dthermalSourcedT, dthermalSourcedF,
                                                    mechanicalSource, dmechanicalSourcedT, dmechanicalSourceF,
                                                    stiff, elasticTangent);
    }

    else{

        this->predictorCorrector_ThermoViscoPlastic(F0, F1, P, q0, q1, T0, T, gradT0, gradT, fluxT, thermalSource, mechanicalSource, elasticTangent);
        if (stiff)
            this->tangent_full_perturbation(F0,F1,P,q0,q1,T0, T, gradT0, gradT, fluxT, thermalSource, mechanicalSource,
                                            Tangent, dFpdF, dFpdT, dFedF, dFedT,
                                            dPdT, dfluxTdgradT, dfluxTdT, dfluxTdF,
                                            dthermalSourcedT, dthermalSourcedF,
                                            dmechanicalSourcedT, dmechanicalSourceF);
    }
};


void mlawNonLinearTVENonLinearTVP::predictorCorrector_ThermoViscoPlastic(
                                                    const STensor3& F0,         // initial deformation gradient (input @ time n)
                                                    const STensor3& F1,         // updated deformation gradient (input @ time n+1)
                                                          STensor3 &P,                // updated 1st Piola-Kirchhoff stress tensor (output)
                                                    const IPNonLinearTVP *q0,       // array of initial internal variable
                                                          IPNonLinearTVP *q1,             // updated array of internal variable (in ipvcur on output),
                                                    const double& T0, // previous temperature
                                                    const double& T, // temperature
                                                    const SVector3 &gradT0, // previous temeprature gradient
                                                    const SVector3 &gradT, // temeprature gradient
                                                          SVector3 &fluxT, // temperature flux)
                                                          double &thermalSource,
                                                          double& mechanicalSource,
                                                          STensor43* elasticTangent) const{
  // temp variables
  static STensor43 Tangent, dFpdF, dFedF;
  static STensor3 dPdT, dfluxTdgradT, dthermalSourcedF, dmechanicalSourceF, dFpdT, dFedT;
  static STensor33 dfluxTdF;
  static SVector3 dfluxTdT;
  static double dthermalSourcedT, dmechanicalSourcedT;
  predictorCorrector_ThermoViscoPlastic(F0,F1,P,q0,q1,Tangent,dFpdF,dFpdT,dFedF,dFedT,
                                        T0,T,gradT0,gradT,fluxT,dPdT,dfluxTdgradT,dfluxTdT,dfluxTdF,
                                        thermalSource,dthermalSourcedT,dthermalSourcedF,
                                        mechanicalSource,dmechanicalSourcedT,dmechanicalSourceF,false,elasticTangent);
};

void mlawNonLinearTVENonLinearTVP::tangent_full_perturbation(
                            const STensor3& F0,
                            const STensor3& F1,         // updated deformation gradient (input @ time n+1)
                                  STensor3& P,                // updated 1st Piola-Kirchhoff stress tensor (output)
                            const IPNonLinearTVP *q0,       // array of initial internal variable
                                  IPNonLinearTVP *q1,             // updated array of internal variable (in ipvcur on output),
                            const double& T0, // previous temperature
                            const double& T, // temperature
                            const SVector3 &gradT0, // previous temeprature gradient
                            const SVector3 &gradT, // temeprature gradient
                                  SVector3 &fluxT, // temperature flux)
                                  double &thermalSource,
                                  double &mechanicalSource,
                                  STensor43 &Tangent,         // mechanical tangents (output)
                                  STensor43 &dFpdF, // plastic tangent
                                  STensor3 &dFpdT, // plastic tangent
                                  STensor43 &dFedF, // elastic tangent
                                  STensor3 &dFedT, // elastic tangent
                                  STensor3 &dPdT, // mechanical-thermal coupling
                                  STensor3 &dfluxTdgradT, // thermal tengent
                                  SVector3 &dfluxTdT,
                                  STensor33 &dfluxTdF, // thermal-mechanical coupling
                                  double &dthermalSourcedT, // thermal source
                                  STensor3 &dthermalSourcedF,
                                  double &dmechanicalSourcedT,
                                  STensor3 &dmechanicalSourceF) const{

    static STensor3 Fplus, Pplus;
    static SVector3 fluxTPlus, gradTplus;
    static double thermalSourcePlus;
    static double mechanicalSourcePlus;
    static IPNonLinearTVP qPlus(*q0);

    // perturb F
    for (int i=0; i<3; i++){
      for (int j=0; j<3; j++){
        Fplus = F1;
        Fplus(i,j) += _perturbationfactor;

        predictorCorrector_ThermoViscoPlastic(F0,Fplus,Pplus,q0,&qPlus,T0,T,gradT0,gradT,fluxTPlus,thermalSourcePlus,mechanicalSourcePlus, NULL);

        q1->_DgammaDF(i,j) = (qPlus._epspbarre - q1->_epspbarre)/_perturbationfactor;
        q1->_DirreversibleEnergyDF(i,j) = (qPlus._irreversibleEnergy - q1->_irreversibleEnergy)/_perturbationfactor;

        for (int k=0; k<3; k++){
          for (int l=0; l<3; l++){
            Tangent(k,l,i,j) = (Pplus(k,l) - P(k,l))/(_perturbationfactor);
            dFpdF(k,l,i,j) = (qPlus._Fp(k,l)-q1->_Fp(k,l))/_perturbationfactor;
            dFedF(k,l,i,j) = (qPlus._Fe(k,l)-q1->_Fe(k,l))/_perturbationfactor;
          }
          dfluxTdF(k,i,j) = (fluxTPlus(k) - fluxT(k))/_perturbationfactor;
        }

        q1->getRefToDElasticEnergyPartdF()(i,j)=(qPlus.getConstRefToElasticEnergyPart()-q1->getConstRefToElasticEnergyPart())/_perturbationfactor;
        q1->getRefToDViscousEnergyPartdF()(i,j)=(qPlus.getConstRefToViscousEnergyPart()-q1->getConstRefToViscousEnergyPart())/_perturbationfactor;
        q1->getRefToDPlasticEnergyPartdF()(i,j)=(qPlus.getConstRefToPlasticEnergyPart()-q1->getConstRefToPlasticEnergyPart())/_perturbationfactor;

        dthermalSourcedF(i,j) = (thermalSourcePlus - thermalSource)/_perturbationfactor;
        dmechanicalSourceF(i,j) = (mechanicalSourcePlus - mechanicalSource)/_perturbationfactor;
      }
    }

    // perturb gradT
    double gradTpert = _perturbationfactor*T0/1e-3;
    for (int i=0; i<3; i++){
      gradTplus = gradT;
      gradTplus(i) += gradTpert;
      predictorCorrector_ThermoViscoPlastic(F0,F1,Pplus,q0,&qPlus,T0,T,gradT0,gradTplus,fluxTPlus,thermalSourcePlus,mechanicalSourcePlus, NULL);
      for (int k=0; k<3; k++){
        dfluxTdgradT(k,i) = (fluxTPlus(k) - fluxT(k))/gradTpert;
      }
    }

    // perturb T
    double Tplus = T+ _perturbationfactor*T0;
    predictorCorrector_ThermoViscoPlastic(F0,F1,Pplus,q0,&qPlus,T0,Tplus,gradT0,gradT,fluxTPlus,thermalSourcePlus,mechanicalSourcePlus, NULL);

    q1->_DgammaDT = (qPlus._epspbarre - q1->_epspbarre)/(_perturbationfactor*T0);
    q1->_DirreversibleEnergyDT = (qPlus._irreversibleEnergy - q1->_irreversibleEnergy)/(_perturbationfactor*T0);

    for (int k=0; k<3; k++){
      for (int l=0; l<3; l++){
        dFpdT(k,l) = (qPlus._Fp(k,l) - q1->_Fp(k,l))/(_perturbationfactor*T0);
        dFedT(k,l) = (qPlus._Fe(k,l) - q1->_Fe(k,l))/(_perturbationfactor*T0);
        dPdT(k,l) = (Pplus(k,l) - P(k,l))/(_perturbationfactor*T0);
      }
      dfluxTdT(k) = (fluxTPlus(k) - fluxT(k))/(_perturbationfactor*T0);
    }

    q1->getRefToDElasticEnergyPartdT() = (qPlus.getConstRefToElasticEnergyPart()-q1->getConstRefToElasticEnergyPart())/(_perturbationfactor*T0);
    q1->getRefToDViscousEnergyPartdT() = (qPlus.getConstRefToViscousEnergyPart()-q1->getConstRefToViscousEnergyPart())/(_perturbationfactor*T0);
    q1->getRefToDPlasticEnergyPartdT() = (qPlus.getConstRefToPlasticEnergyPart()-q1->getConstRefToPlasticEnergyPart())/(_perturbationfactor*T0);

    dthermalSourcedT = (thermalSourcePlus - thermalSource)/(_perturbationfactor*T0);
    dmechanicalSourcedT = (mechanicalSourcePlus - mechanicalSource)/(_perturbationfactor*T0);

};

void mlawNonLinearTVENonLinearTVP::predictorCorrector_ThermoViscoPlastic(
                                                    const STensor3& F0,         // initial deformation gradient (input @ time n)
                                                    const STensor3& F1,         // updated deformation gradient (input @ time n+1)
                                                          STensor3& P,                // updated 1st Piola-Kirchhoff stress tensor (output)
                                                    const IPNonLinearTVP *q0,       // array of initial internal variable
                                                          IPNonLinearTVP *q1,             // updated array of internal variable (in ipvcur on output),
                                                          STensor43 &Tangent,         // mechanical tangents (output)
                                                          STensor43 &dFpdF, // plastic tangent
                                                          STensor3 &dFpdT, // plastic tangent
                                                          STensor43 &dFedF, // elastic tangent
                                                          STensor3 &dFedT, // elastic tangent
                                                    const double& T0, // previous temperature
                                                    const double& T, // temperature
                                                    const SVector3 &gradT0, // previoustemeprature gradient
                                                    const SVector3 &gradT, // temeprature gradient
                                                          SVector3 &fluxT, // temperature flux
                                                          STensor3 &dPdT, // mechanical-thermal coupling
                                                          STensor3 &dfluxTdgradT, // thermal tengent
                                                          SVector3 &dfluxTdT,
                                                          STensor33 &dfluxTdF, // thermal-mechanical coupling
                                                          double &thermalSource,   // - cp*dTdt
                                                          double &dthermalSourcedT, // thermal source
                                                          STensor3 &dthermalSourcedF,
                                                          double &mechanicalSource, // mechanical source--> convert to heat
                                                          double &dmechanicalSourcedT,
                                                          STensor3 &dmechanicalSourceF,
                                                    const bool stiff,
                                                          STensor43* elasticTangent) const{

  if (_tangentByPerturbation){
    if (_nonAssociatedFlow){
      this->predictorCorrector_TVP_nonAssociatedFlow_nonLinearTVE(F0,F1,q0,q1,P,false,Tangent,dFedF,dFpdF,dFedT,dFpdT,
                                                        T0,T,gradT0,gradT,fluxT,dPdT,dfluxTdgradT,dfluxTdT,dfluxTdF,
                                                        thermalSource,dthermalSourcedT,dthermalSourcedF,
                                                        mechanicalSource,dmechanicalSourcedT,dmechanicalSourceF);
    }
    else{
      this->mlawNonLinearTVP::predictorCorrector_TVP_AssociatedFlow(F1,q0,q1,P,false,Tangent,dFedF,dFpdF,T0,T);
    }

  }
  else{
    if (_nonAssociatedFlow){
      this->predictorCorrector_TVP_nonAssociatedFlow_nonLinearTVE(F0,F1,q0,q1,P,stiff,Tangent,dFedF,dFpdF,dFedT,dFpdT,
                                                        T0,T,gradT0,gradT,fluxT,dPdT,dfluxTdgradT,dfluxTdT,dfluxTdF,
                                                        thermalSource,dthermalSourcedT,dthermalSourcedF,
                                                        mechanicalSource,dmechanicalSourcedT,dmechanicalSourceF);
    }
    else{
      this->mlawNonLinearTVP::predictorCorrector_TVP_AssociatedFlow(F1,q0,q1,P,stiff,Tangent,dFedF,dFpdF,T0,T);
    }
  }

};


void mlawNonLinearTVENonLinearTVP::predictorCorrector_TVP_nonAssociatedFlow_nonLinearTVE(const STensor3& F0, const STensor3& F, const IPNonLinearTVP *q0, IPNonLinearTVP *q1,
                            STensor3&P, const bool stiff, STensor43& Tangent, STensor43& dFedF, STensor43& dFpdF, STensor3& dFedT, STensor3& dFpdT,
                            const double T0,
                            const double T,
                            const SVector3 &gradT0,                 // previous temperature gradient
                            const SVector3 &gradT,                  // temperature gradient
                                  SVector3 &fluxT,                  // temperature flux
                                  STensor3 &dPdT,                   // mechanical-thermal coupling
                                  STensor3 &dfluxTdgradT,           // thermal tengent
                                  SVector3 &dfluxTdT,
                                  STensor33 &dfluxTdF,              // thermal-mechanical coupling
                                  double &thermalSource,            // - Cp*dTdt
                                  double &dthermalSourcedT,         // thermal source
                                  STensor3 &dthermalSourcedF,
                                  double &mechanicalSource,         // mechanical source--> convert to heat
                                  double &dmechanicalSourcedT,
                                  STensor3 &dmechanicalSourceF) const{

  // compute elastic predictor
  STensor3& Fp1 = q1->_Fp;
  const STensor3& Fp0 = q0->_Fp;

  // Update the Properties to the current temperature (see below which ones are being used)
  double CpT, DCpDT; getCp(CpT,T,&DCpDT);

  // Initialise predictor values
  Fp1 = Fp0; // plastic deformation tensor
  q1->_epspbarre = q0->_epspbarre; // plastic equivalent strain
  q1->_epspCompression = q0->_epspCompression;
  q1->_epspTraction = q0->_epspTraction;
  q1->_epspShear = q0->_epspShear;
  q1->_backsig = q0->_backsig; // backstress tensor
  q1->_DbackSigDT = q0->_DbackSigDT; // DbackSigDT
  q1->_DgammaDt = 0.;

  // Get hardening parameters
  this->mlawNonLinearTVP::hardening(q0,q1,T);
  static fullVector<double> a(3), Da(3); // yield coefficients and derivatives with respect to plastic deformation
  this->mlawNonLinearTVP::getYieldCoefficients(q1,a);
  //a.print("a init");

  double Hb(0.), dHb(0.), ddHb(0.), dHbdT(0.), ddHbdgammadT(0.), ddHbddT(0.);
  if (q1->_ipKinematic != NULL){
    Hb = q1->_ipKinematic->getR(); // kinematic hardening parameter
    dHb = q1->_ipKinematic->getDR(); // kinematic hardening parameter derivative (dHb/dgamma)
    dHbdT = q1->_ipKinematic->getDRDT();  // make sure to normalise DRDT while defining in python file
    ddHbddT = q1->_ipKinematic->getDDRDTT();
    ddHbdgammadT = q1->_ipKinematic->getDDRDT();
  }

  double eta(0.),Deta(0.),DetaDT(0.);
  if (_viscosity != NULL)
    _viscosity->get(q1->_epspbarre,T,eta,Deta,DetaDT);

  // Get Cepr, Ceinvpr
  static STensor3 Fpinv, Ce, Fepr;
  STensorOperation::inverseSTensor3(Fp1,Fpinv);
  STensorOperation::multSTensor3(F,Fpinv,Fepr);
  STensorOperation::multSTensor3FirstTranspose(Fepr,Fepr,Ce);

  static STensor3 Eepr, Cepr, Ceinvpr;
  Cepr = Ce;
  STensorOperation::inverseSTensor3(Cepr,Ceinvpr);

  static STensor3 invFp0; // plastic predictor
  invFp0= Fpinv;
  STensor3& Fe = q1->_Fe;
  Fe = Fepr;

  // Predictor - TVE
  static STensor43 DlnDCepr, DlnDCe;
  static STensor63 DDlnDDCe;
  static STensor3 expGN, GammaN;
  static STensor43 dexpAdA; // estimation of dexpA/dA
  expGN = _I; // if Gamma = 0, expGN = _I
  // dexpAdA = _I4;
  STensorOperation::zero(dexpAdA); // CHECKERS

  STensor3& Ee = q1->_Ee;
  // STensorOperation::logSTensor3(Ce,_order,Ee,&DlnDCepr,&DDlnDDCe);
  bool ok=STensorOperation::logSTensor3(Ce,_order,Ee,&DlnDCepr,&DDlnDDCe);
  if(!ok)
  {
     P(0,0) = P(1,1) = P(2,2) = sqrt(-1.);
     return;
  }
  Ee *= 0.5;
  Eepr = Ee;
  DlnDCe = DlnDCepr;

  // update A, B -> ExtraBranch calculations are within
  double Ke(0.), Ge(0.), Ke_pr(0.), Ge_pr(0.);
  static STensor43 Ge_Tensor_pr, Ge_Tensor, Bd_stiffnessTerm, Ge_TrEeTensor_pr, Ge_TrEeTensor;
  STensorOperation::zero(Ge_Tensor_pr); STensorOperation::zero(Ge_Tensor);
  STensorOperation::zero(Ge_TrEeTensor_pr); STensorOperation::zero(Ge_TrEeTensor);

  // NEW EIGEN
  STensor3& R = q1->_R;
  STensor43& dRdEe = q1->_dRdEe;
  STensor43& dRtdEe = q1->_dRtdEe;
  static STensor3 Ee0_rot, devXns;
  static STensor43 rotationStiffness_pr ,rotationStiffness, dEe0sdEe, backStressRotationStiffness;
  STensorOperation::zero(rotationStiffness_pr); STensorOperation::zero(rotationStiffness); STensorOperation::zero(backStressRotationStiffness);
  q1->_Ee0 = q0->_Ee;
  if (_rotationCorrectionScheme == 0 || _rotationCorrectionScheme == 1){
	  mlawNonLinearTVM::rotationTensor_N_to_Nplus1(q0->_Ee0,q0->_Fp,Fp1,Ce,Ee,q0->_Ee,R,dRdEe,dRtdEe);
	  for(int i=0; i<3; i++)
		  for(int j=0; j<3; j++){
			  Ee0_rot(i,j) = 0.;
			  for(int k=0; k<3; k++)
				  for(int l=0; l<3; l++)
					  Ee0_rot(i,j) += R(i,k)*q0->_Ee(k,l)*R(j,l);
		  }
  }
  else if(_rotationCorrectionScheme == 2){
	  Ee0_rot = q0->_Ee;
	  // mlawNonLinearTVM::getEe0s(Ce,Ee,q0->_Ee,Ee0_rot,dEe0sdEe); // Dont do it here
  }

  q1->_detEe = STensorOperation::determinantSTensor3(Ee);
  // NEW EIGEN

  if (_extraBranchNLType == TensionCompressionRegularisedType  || _extraBranchNLType == hyper_exp_TCasymm_Type || _ExtraBranch_TVE_option == 3 || _ExtraBranch_TVE_option == 4 || _ExtraBranch_TVE_option == 5){
      // TC asymmetry -> for either case of TensionCompressionRegularisedType and _ExtraBranch_TVE_option == 3
	  if (!_useRotationCorrection){
		  mlawNonLinearTVM::ThermoViscoElasticPredictor(Ee,q0->_Ee,q0,q1,Ke,Ge,T0,T,stiff,Bd_stiffnessTerm,&Ge_TrEeTensor);
	  }
	  else{
		  // mlawNonLinearTVM::ThermoViscoElasticPredictor(Ee,q0->_Ee,q0,q1,Ke,Ge,T0,T,stiff,Bd_stiffnessTerm,&Ge_TrEeTensor);
		  mlawNonLinearTVM::ThermoViscoElasticPredictor_forTVP(Ce,Ee,Ee0_rot,q0,q1,Ke,Ge,T0,T,stiff,Bd_stiffnessTerm,&Ge_TrEeTensor,&rotationStiffness); // NEW EIGEN
	  }
      Ge_TrEeTensor_pr = Ge_TrEeTensor;
      rotationStiffness_pr = rotationStiffness;
  }
  else{
	  if (!_useRotationCorrection){
		  mlawNonLinearTVM::ThermoViscoElasticPredictor(Ee,q0->_Ee,q0,q1,Ke,Ge,T0,T,stiff,Bd_stiffnessTerm);
	  }
	  else{
		  // mlawNonLinearTVM::ThermoViscoElasticPredictor(Ee,q0->_Ee,q0,q1,Ke,Ge,T0,T,stiff,Bd_stiffnessTerm);
	      mlawNonLinearTVM::ThermoViscoElasticPredictor_forTVP(Ce,Ee,Ee0_rot,q0,q1,Ke,Ge,T0,T,stiff,Bd_stiffnessTerm,NULL,&rotationStiffness); // NEW EIGEN
	  }
	  rotationStiffness_pr = rotationStiffness;
  }

  Ge_Tensor = _I4;
  Ge_Tensor *= Ge*2; // Because the function does not do it
  Ge_Tensor += Bd_stiffnessTerm;

  // Keep the predictor values of Ke and Ge for tangents later
  Ke_pr = Ke;
  Ge_pr = Ge;
  Ge_Tensor_pr = _I4;
  Ge_Tensor_pr *= Ge_pr*2; // Because the function does not do it
  Ge_Tensor_pr += Bd_stiffnessTerm;

  // get predictor - dKeprDT
  mlawNonLinearTVM::getTVEdCorKirDT(q0,q1,T0,T);
  const STensor3& dKeprDT = q1->_DcorKirDT; // This has all extrabranches now

  // Initialise Dho
  static STensor43 Dho, Dho2inv, Dho2_u_inv;  // These will be important tensors.
  double Dho2_v_inv(0.);

  // Get Kepr
  static STensor3 Kepr;  // Ke is corKir
  Kepr = q1->_kirchhoff;

  // Get Xn; Initialise X
  static STensor3 devXn, devX;
  static double trXn, pXn, eXn, trX;
  STensorOperation::decomposeDevTr(q0->_backsig,devXn,trXn); // needed for chaboche model
  devXns = devXn; // initialise here for backstress rotation correction
  pXn = 1./3.*trXn;  // 1st invariant of Xn
  eXn = sqrt(1.5*devXn.dotprod()); // J2 invariant of Xn

  // Initialise Phipr
  static STensor3 PhiPr, Phi;
  PhiPr = q1->_kirchhoff;
  PhiPr -= q1->_backsig;
  Phi = PhiPr;

  static STensor3 devPhipr, devPhi; // effective dev stress predictor
  double ptildepr(0.), ptilde(0.);
  STensorOperation::decomposeDevTr(PhiPr,devPhipr,ptildepr);
  ptildepr/= 3.;

  // Initialise Normal
  static STensor3 devN, N; // dev part of yield normal
  double trN = 0.; // trace part of yield normal
  STensorOperation::zero(devN);
  STensorOperation::zero(N);

  // Get plastic poisson ratio
  q1->_nup = (9.-2.*_b)/(18.+2.*_b);
  double kk = (1./sqrt(1.+2.*q1->_nup*q1->_nup));

  // Initialise variables
  double& Gamma = q1->_Gamma;   // flow rule parameter
  Gamma = 0.;
  double dDgammaDGamma = 0.;
  double Dgamma = 0.; // eqplastic strain increment
  static fullVector<double> m(2);
  mlawNonLinearTVP::getChabocheCoeffs(m,0,q1);

  double DfDGamma(0.), dfdDgamma(0.), dfdGamma(0.), dAdDgamma(0.), dAdGamma(0.);

  // Initialise Cx
  double Cxdev = 1. + 3.*m(1)*pow(kk,1)*Hb;        // pow(kk,2) DEBUG
  double Cxtr = 1. + 2.*_b/3. *m(1)*pow(kk,1)*Hb;  // pow(kk,2) DEBUG
  double dCxdevdDgamma(0.), dCxtrdDgamma(0.);
  if (Hb>0.){
    Cxdev -= m(0)*dHb*Dgamma/Hb + 1./Hb * dHbdT * (T-T0);
    Cxtr -= m(0)*dHb*Dgamma/Hb + 1./Hb * dHbdT * (T-T0);
  }

  // Initialise ptilde and devPhi
  ptilde = ptildepr;
  devPhi = devPhipr;

  // Initialise the rest
  if (_extraBranchNLType == TensionCompressionRegularisedType  || _extraBranchNLType == hyper_exp_TCasymm_Type || _ExtraBranch_TVE_option == 3 || _ExtraBranch_TVE_option == 4 || _ExtraBranch_TVE_option == 5){
    // TC asymmetry -> for either case of TensionCompressionRegularisedType and _ExtraBranch_TVE_option == 3
    getDho(Gamma,Cepr,Ceinvpr,Kepr,Ke,Ge_Tensor,kk,Hb,Cxtr,Cxdev,expGN,dexpAdA,Dho,Dho2inv,Dho2_u_inv,Dho2_v_inv,&Ge_TrEeTensor,&rotationStiffness,&backStressRotationStiffness);
  }
  else{
    getDho(Gamma,Cepr,Ceinvpr,Kepr,Ke,Ge_Tensor,kk,Hb,Cxtr,Cxdev,expGN,dexpAdA,Dho,Dho2inv,Dho2_u_inv,Dho2_v_inv,NULL,&rotationStiffness,&backStressRotationStiffness);
  }

  double PhiEqpr2 = 1.5*devPhi.dotprod();
  double PhiEqpr = sqrt(PhiEqpr2);      // -> second invariant
  double PhiEq = PhiEqpr;   // current effective deviator

  // Get f and A
  double f = a(2)*pow(PhiEq,_n) - (a(1)*ptilde+a(0));
  double A = sqrt(6.*PhiEq*PhiEq+4.*_b*_b/3.*ptilde*ptilde);

  // Initialise g
  double dgdDgamma(0.), dgdGamma(0.);
  double g = Dgamma - kk*Gamma*A;

  // NR Loop - Gamma and Dgamma
    if (q1->dissipationIsBlocked()){
        q1->getRefToDissipationActive() = false;
    }
    else{
        if (f>_tol){
        	// Msg::Error("Plasticity");
            q1->getRefToDissipationActive() = true;

            // plasticity
            int ite = 0;
            int maxite = 100; // 5000; // maximal number of iters
            // Msg::Error("plasticity occurs f = %e",f);
            //double f0 = fabs(f);

            while (fabs(f) >_tol or ite <1){

                // Viscosity term
                double etaOverDt = eta/this->getTimeStep();

                // Get dCxdevdDgamma, dCxtrdDgamma
                dCxdevdDgamma = 0.; dCxtrdDgamma = 0.;
                if (Hb>0.){
                    dCxdevdDgamma = -m(0)*1./Hb*dHb - m(0)*Dgamma*(-1./pow(Hb,2.)*pow(dHb,2.) + 1./Hb*ddHb) - (T-T0)*(-1./pow(Hb,2.)*dHb*dHbdT + 1./Hb*ddHbdgammadT);
                    dCxtrdDgamma = dCxdevdDgamma;
                }
                dCxdevdDgamma += 3.*m(1)*pow(kk,1)*dHb;        // pow(kk,2) DEBUG
                dCxtrdDgamma += (2.*_b/3.)*m(1)*pow(kk,1)*dHb; // pow(kk,2) DEBUG

                // Get Hp = dPhiPdDgamma, dPhiPdGamma
                double Hp =  ( 2*_b*pow(kk,1)*Gamma/3.*ptilde*(Hb/pow(Cxtr,2)*dCxtrdDgamma-1./Cxtr*dHb) + 1./3.*trXn/pow(Cxtr,2)*dCxtrdDgamma ) * Dho2_v_inv; // pow(kk,2) DEBUG
                double dPhiPdGamma = -2*_b*ptilde*(Ke + pow(kk,1)*Hb/(3.*Cxtr)) * Dho2_v_inv; // pow(kk,2) DEBUG

                // Get He = dPhiEdGamma
                // Get DdevPhidGamma
                static STensor3 DdevPhidDgamma, DdevPhidDgamma_RHS;
                static STensor3 DdevPhidGamma, DdevPhidGamma_RHS;
                STensorOperation::zero(DdevPhidDgamma); STensorOperation::zero(DdevPhidGamma);
                STensorOperation::zero(DdevPhidDgamma_RHS); STensorOperation::zero(DdevPhidGamma_RHS);

                for (int i=0; i<3; i++)
                    for (int j=0; j<3; j++){
                        DdevPhidDgamma_RHS(i,j) += ( 3*Gamma*pow(kk,1)*( Hb/pow(Cxdev,2.)*dCxdevdDgamma - dHb/Cxdev ) * devPhi(i,j)
                                                    + devXns(i,j)/pow(Cxdev,2.)*dCxdevdDgamma ); // pow(kk,2) DEBUG
                        DdevPhidGamma_RHS(i,j) += (- 3*pow(kk,1)*Hb/Cxdev*devPhi(i,j) ); // pow(kk,2) DEBUG
                        for (int k=0; k<3; k++)
                            for (int l=0; l<3; l++){
                                DdevPhidDgamma_RHS(i,j) += 2.*_b/3.*Gamma*( - Ge_TrEeTensor(i,j,k,l) - rotationStiffness(i,j,k,l))*_I(k,l)*Hp; // EIGEN
                                DdevPhidGamma_RHS(i,j) += ( -3.*Ge_Tensor(i,j,k,l)*devPhi(k,l) + 2.*_b/3.*Gamma*(- Ge_TrEeTensor(i,j,k,l) - rotationStiffness(i,j,k,l) )*_I(k,l)*dPhiPdGamma ); // EIGEN
                                DdevPhidGamma_RHS(i,j) += (-Ge_TrEeTensor(i,j,k,l) - rotationStiffness(i,j,k,l) )*N(k,l); // DEBUG // EIGEN
                            }
                    }
                for (int i=0; i<3; i++)
                    for (int j=0; j<3; j++)
                        for (int k=0; k<3; k++)
                            for (int l=0; l<3; l++){
                                DdevPhidDgamma(i,j) += Dho2_u_inv(i,j,k,l)*DdevPhidDgamma_RHS(k,l);
                                DdevPhidGamma(i,j) += Dho2_u_inv(i,j,k,l)*DdevPhidGamma_RHS(k,l);
                            }

                double Stemp1 = STensorOperation::doubledot(devPhi,DdevPhidDgamma);
                double Stemp2 = STensorOperation::doubledot(devPhi,DdevPhidGamma);
                double He = 1.5*Stemp1/PhiEq;
                double dPhiEdGamma = 1.5*Stemp2/PhiEq;


                // dAdGamma and dDgammaDGamma
                dAdDgamma = (12.*PhiEq*He + 8.*_b*_b*ptilde*Hp/3.)/(2.*A);
                dAdGamma = (12.*PhiEq*dPhiEdGamma + 8.*_b*_b*ptilde*dPhiPdGamma/3.)/(2.*A);

                dDgammaDGamma = pow(kk,1)*(A+Gamma*dAdGamma);  // mistake in the paper (VD 2016) // pow(kk,2) DEBUG

                this->getYieldCoefficientDerivatives(q1,q1->_nup,Da);

                dfdDgamma = Da(2)*pow(PhiEq,_n) - Da(1)*ptilde -Da(0);
                dfdDgamma += a(2)*_n*pow(PhiEq,(_n-1.))*He - a(1)*Hp;

                if (Gamma>0 and etaOverDt>0)
                    dfdDgamma -= _p*pow(etaOverDt,_p-1.)*Deta/this->getTimeStep()*pow(Gamma,_p);  // THIS term is absent in the paper!!

                dfdGamma = _n*a(2)*pow(PhiEq,(_n-1.))*dPhiEdGamma - a(1)*dPhiPdGamma;
                if (Gamma>0 and etaOverDt>0)
                    dfdGamma -= pow(etaOverDt,_p)*_p*pow(Gamma,(_p-1.));

                DfDGamma = dfdDgamma*dDgammaDGamma + dfdGamma; // unused

                dgdDgamma = 1. - kk*Gamma*dAdDgamma;
                dgdGamma = - dDgammaDGamma;

                double dGamma = (-f + dfdDgamma*g/dgdDgamma)/(-dfdDgamma*dgdGamma/dgdDgamma + dfdGamma); // NR
                double dDgamma = -(g+dgdGamma*dGamma)/dgdDgamma; // NR

                if (Gamma + dGamma <=0.){
                    Gamma /= 2.;                // NR
                }
                else
                    Gamma += dGamma;

                // Dgammma_test += dDgamma;
                if (Dgamma + dDgamma <=0.){
                    Dgamma /= 2.;
                }
                else
                    Dgamma += dDgamma;

                //Msg::Error("Gamma = %e",Gamma);
                //Msg::Error("Dgamma = %e",Dgamma);


                // UPDATE FOR NEXT ITERATION

                // Update gamma and all Hardening moduli
                mlawNonLinearTVP::updateEqPlasticDeformation(q1,q0,q1->_nup,Dgamma);
                mlawNonLinearTVP::hardening(q0,q1,T);
                mlawNonLinearTVP::getYieldCoefficients(q1,a);
                if (q1->_ipKinematic != NULL){
                    Hb = q1->_ipKinematic->getR(); // kinematic hardening parameter
                    dHb = q1->_ipKinematic->getDR(); // kinematic hardening parameter derivative (dHb/dgamma ??)
                    dHbdT = q1->_ipKinematic->getDRDT();  // make sure to normalise DRDT while defining in python file //NEW
                    ddHbddT = q1->_ipKinematic->getDDRDTT();
                    ddHbdgammadT = q1->_ipKinematic->getDDRDT();
                }
                //a.print("a update");

                // Update Viscosity -> NEW 23rd May,2024
                if (_viscosity != NULL)
                    _viscosity->get(q1->_epspbarre,T,eta,Deta,DetaDT);

                // Update Cx
                Cxdev = 1. + 3.*m(1)*pow(kk,1)*Hb;  // pow(kk,2) DEBUG
                Cxtr = 1. + 2.*_b/3. *m(1)*pow(kk,1)*Hb; // pow(kk,2) DEBUG
                if (Hb>0.){
                    Cxdev -= m(0)*dHb*Dgamma/Hb - 1./Hb * dHbdT * (T-T0);
                    Cxtr -= m(0)*dHb*Dgamma/Hb - 1./Hb * dHbdT * (T-T0);
                }

                // Update Phi
                if (_extraBranchNLType == TensionCompressionRegularisedType  || _extraBranchNLType == hyper_exp_TCasymm_Type || _ExtraBranch_TVE_option == 3 || _ExtraBranch_TVE_option == 4 || _ExtraBranch_TVE_option == 5){
                    // TC asymmetry -> for either case of TensionCompressionRegularisedType and _ExtraBranch_TVE_option == 3
                    getIterated_DPhi(F,T0,T,q0,q1,Gamma,Cxtr,Cxdev,Cepr,Eepr,trXn,devXn,Ke,Ge,Ge_Tensor,ptilde,devPhi,Phi,N,expGN,dexpAdA,
                                    Dho,Dho2inv,Dho2_u_inv,Dho2_v_inv,Fp1,&Ge_TrEeTensor,&rotationStiffness,&devXns,&backStressRotationStiffness);
                }
                else{
                    getIterated_DPhi(F,T0,T,q0,q1,Gamma,Cxtr,Cxdev,Cepr,Eepr,trXn,devXn,Ke,Ge,Ge_Tensor,ptilde,devPhi,Phi,N,expGN,dexpAdA,
                                    Dho,Dho2inv,Dho2_u_inv,Dho2_v_inv,Fp1,NULL,&rotationStiffness,&devXns,&backStressRotationStiffness);
                }

                PhiEq = sqrt(1.5*devPhi.dotprod());

                // Update A
                A = sqrt(6.*PhiEq*PhiEq+4.*_b*_b/3.*ptilde*ptilde);

                // Update f, g
                f = a(2)*pow(PhiEq,_n) - (a(1)*ptilde+a(0));
                double viscoTerm = etaOverDt*Gamma;
                if (Gamma>0. and etaOverDt>0.) f-= pow(viscoTerm,_p);

                g = Dgamma - kk*Gamma*A;

                ite++;
                // if (ite> maxite-5)
                // Msg::Error("it = %d, DfDGamma = %e error = %e dGamma = %e, Gamma = %e",ite,DfDGamma,f,dGamma,Gamma);

                if (fabs(f) <_tol) break;

                if(ite > maxite){
                    Msg::Error("No convergence for plastic correction in mlawNonLinearTVENonLinearTVP nonAssociatedFlow iter = %d, f = %e!!",ite,f);
                    P(0,0) = P(1,1) = P(2,2) = sqrt(-1.);
                    return;
                }

            } // while

            q1->_DgammaDt = Dgamma/this->getTimeStep();

            // Correct Cx
            Cxdev = 1. + 3.*m(1)*pow(kk,1)*Hb;  // pow(kk,2) DEBUG
            Cxtr = 1. + 2.*_b/3. *m(1)*pow(kk,1)*Hb; // pow(kk,2) DEBUG
            if (Hb>0.){
                Cxdev -= m(0)*dHb*Dgamma/Hb - 1./Hb * dHbdT * (T-T0);
                Cxtr -= m(0)*dHb*Dgamma/Hb - 1./Hb * dHbdT * (T-T0);
            }

            // Correct Phi
            if (_extraBranchNLType == TensionCompressionRegularisedType || _extraBranchNLType == hyper_exp_TCasymm_Type || _ExtraBranch_TVE_option == 3 || _ExtraBranch_TVE_option == 4 || _ExtraBranch_TVE_option == 5){
                // TC asymmetry -> for either case of TensionCompressionRegularisedType and _ExtraBranch_TVE_option == 3
                getIterated_DPhi(F,T0,T,q0,q1,Gamma,Cxtr,Cxdev,Cepr,Eepr,trXn,devXn,Ke,Ge,Ge_Tensor,ptilde,devPhi,Phi,N,expGN,dexpAdA,
                    Dho,Dho2inv,Dho2_u_inv,Dho2_v_inv,Fp1,&Ge_TrEeTensor,&rotationStiffness,&devXns,&backStressRotationStiffness);
            }
            else{
                getIterated_DPhi(F,T0,T,q0,q1,Gamma,Cxtr,Cxdev,Cepr,Eepr,trXn,devXn,Ke,Ge,Ge_Tensor,ptilde,devPhi,Phi,N,expGN,dexpAdA,
                    Dho,Dho2inv,Dho2_u_inv,Dho2_v_inv,Fp1,NULL,&rotationStiffness,&devXns,&backStressRotationStiffness);
            }
            PhiEq = sqrt(1.5*devPhi.dotprod());

            // Correct Normal, H = expGN
            STensorOperation::decomposeDevTr(N,devN,trN);

            // Correct plastic deformation tensor
            STensorOperation::multSTensor3(expGN,Fp0,Fp1);

            // Correct IP gamma
            updateEqPlasticDeformation(q1,q0,q1->_nup,Dgamma);
                // Msg::Info("setting: gamma=%e ",q1->_epspbarre);

            // Correct elastic deformation tensor, corotational stress
            STensorOperation::inverseSTensor3(Fp1,Fpinv);
            STensorOperation::multSTensor3(F,Fpinv,Fe);
            STensorOperation::multSTensor3FirstTranspose(Fe,Fe,Ce);

            bool ok=STensorOperation::logSTensor3(Ce,_order,Ee,&DlnDCe,&DDlnDDCe);
            if(!ok)
            {
              P(0,0) = P(1,1) = P(2,2) = sqrt(-1.);
              return;
            }
            Ee *= 0.5;

        } // 2nd if
        else{
            q1->getRefToDissipationActive() = false;
        	// Msg::Error("Elasticity");
        }
    } // 1st if

    // corKir
    const STensor3& KS = q1->_kirchhoff;

    // second Piola Kirchhoff stress
    static STensor3 S, Ceinv;
    STensorOperation::inverseSTensor3(Ce,Ceinv);
    STensorOperation::multSTensor3STensor43(KS,DlnDCe,S);

    // mandel_e
    static STensor3 MS;
    for(int i=0; i<3; i++)
        for(int j=0; j<3; j++){
        	MS(i,j) = 0.;
            for(int k=0; k<3; k++)
              MS(i,j) += Ce(i,k)*S(k,j);
        }
    q1->_ModMandel = MS;

    // commuteChecker -> for commuting tensors Ce and Se, Me has to be symmetric ->
    // 			For two cases, both in the case of viscoplasticity if Een and Ee dont share eigenvectors: 1. Due to plasticity (Rp)  2. Due to non-proportional loading
    static STensor3 commuteChecker, MST;
    STensorOperation::transposeSTensor3(MS,MST);
    commuteChecker = MS;
    commuteChecker -= MST;
    q1->_mandelCommuteChecker = commuteChecker.norm0();
    if(q1->_mandelCommuteChecker > 1.e-6){
    	// Msg::Error("Mandel does not commute in mlawNonLinearTVENonLinearTVP, norm = %e, tol = %e !!",q1->_mandelCommuteChecker,1.e-6);
    }

    // first PK
    for(int i=0; i<3; i++)
        for(int j=0; j<3; j++){
            P(i,j) = 0.;
            for(int k=0; k<3; k++)
                for(int l=0; l<3; l++)
                    P(i,j) += Fe(i,k)*S(k,l)*Fpinv(j,l);
        }

    // Msg::Error(" Inside TVP, after correction, after updating q1->_elasticEnergy = %e, eta = %e !!",q1->_elasticEnergy, eta_mullins);

    // q1->getRefToElasticEnergy()=deformationEnergy(*q1);
    q1->getRefToViscousEnergyPart()=viscousEnergy(*q0,*q1)+q0->getConstRefToViscousEnergyPart();
    q1->getRefToPlasticEnergy() = q0->plasticEnergy();
    if (Gamma > 0.){
        double dotKSN = dot(KS,N);
        q1->getRefToPlasticEnergy() += Gamma*dotKSN; // = Dp:Ke
    }

    // fluxT
    double KThConT, DKThConDT;
    mlawNonLinearTVM::getKTHCon(KThConT,T,&DKThConDT);
    double J  = 1.;
    STensor3 Finv(0.);
    if (_thermalEstimationPreviousConfig){                                            // ADD  _thermalEstimationPreviousConfig
        STensorOperation::inverseSTensor3(F0,Finv);
        J = STensorOperation::determinantSTensor3(F0);
    }
    else{
        STensorOperation::inverseSTensor3(F,Finv);
        J = STensorOperation::determinantSTensor3(F);
    }

    static STensor3 Cinv;
    STensorOperation::multSTensor3SecondTranspose(Finv,Finv,Cinv);
    STensorOperation::multSTensor3SVector3(Cinv,gradT,fluxT);
    fluxT *= (-KThConT*J);

    // ThermSrc and MechSrc after dPdF and dPdT - But need the following tensors for the mechSrcTVP function call
    static STensor3 DphiPDF;
    STensorOperation::zero(DphiPDF);
    STensorOperation::zero(dFpdF); //dFpdT
    static STensor43 CeprToF, DEeDCepr, DEeDF, dGammaNdCepr; // conversion tensors
    STensorOperation::zero(CeprToF);
    STensorOperation::zero(DEeDCepr);
    STensorOperation::zero(DEeDF);
    STensorOperation::zero(dGammaNdCepr);

    // I didnt make this
    if (this->getMacroSolver()->getPathFollowingLocalIncrementType() == pathFollowingManager::DEFO_ENERGY){
        q1->getRefToIrreversibleEnergy() = q1->defoEnergy();
    }
    else if ((this->getMacroSolver()->getPathFollowingLocalIncrementType() == pathFollowingManager::PLASTIC_ENERGY) or
             (this->getMacroSolver()->getPathFollowingLocalIncrementType() == pathFollowingManager::DISSIPATION_ENERGY)){
        q1->getRefToIrreversibleEnergy() = q1->plasticEnergy();
    }
    else{
        q1->getRefToIrreversibleEnergy() = 0.;
    }


    if (stiff){

        // dPdF

        // 1. get Dp_pr_DCepr, DpDCepr
        static STensor3 Dp_pr_DCepr, DpDCepr;
        STensorOperation::multSTensor3STensor43(_I,DlnDCepr,Dp_pr_DCepr);
        STensorOperation::multSTensor3STensor43(_I,DlnDCepr,DpDCepr);
        Dp_pr_DCepr*= (0.5*Ke_pr);
        DpDCepr*= (0.5*Ke);

        // Ke and Ge_Tensor are already corrected if Gamma>0. because of the NR loop on Phi - Reuse DcorKirDEe later
        static STensor43 DcorKirDEe_pr, DcorKirDEe;
        STensorOperation::zero(DcorKirDEe_pr);
        STensorOperation::zero(DcorKirDEe);
        for (int i=0; i<3; i++)
            for (int j=0; j<3; j++)
                for (int k=0; k<3; k++)
                    for (int l=0; l<3; l++){
                        DcorKirDEe(i,j,k,l) += Ke*_I(i,j)*_I(k,l);
                        DcorKirDEe_pr(i,j,k,l) += Ke_pr*_I(i,j)*_I(k,l);
                        for (int p=0; p<3; p++)
                            for (int q=0; q<3; q++){
                                DcorKirDEe_pr(i,j,k,l) += Ge_Tensor_pr(i,j,p,q)*_Idev(p,q,k,l);
                                DcorKirDEe(i,j,k,l) += Ge_Tensor(i,j,p,q)*_Idev(p,q,k,l);
                            }
                    }
        DcorKirDEe_pr += Ge_TrEeTensor_pr; // TC Assymmetry
        DcorKirDEe += Ge_TrEeTensor; // TC Assymmetry

        // NEW EIGEN
        DcorKirDEe_pr += rotationStiffness_pr;
        DcorKirDEe += rotationStiffness;
        // NEW EIGEN

        static STensor43 DcorKir_pr_DCepr, DcorKirDCepr;
        STensorOperation::multSTensor43(DcorKirDEe_pr,DlnDCepr,DcorKir_pr_DCepr);
        STensorOperation::multSTensor43(DcorKirDEe,DlnDCepr,DcorKirDCepr);
        DcorKir_pr_DCepr *= 0.5; // use for DMeprDCepr
        DcorKirDCepr *= 0.5; // correct this one later

        // 2. get DCeprDCepr = _I4 and DCeinvprDCepr
        static STensor43 DCeinvprDCepr;

        for (int i=0; i<3; i++)
            for (int s=0; s<3; s++)
                for (int k=0; k<3; k++)
                    for (int l=0; l<3; l++){
                        DCeinvprDCepr(i,s,k,l) = 0.;
                        for (int m=0; m<3; m++)
                            for (int j=0; j<3; j++)
                                DCeinvprDCepr(i,s,k,l) -= Ceinvpr(i,m)*_I4(m,j,k,l)*Ceinvpr(j,s);
          }

        //
        static STensor43 DdevCorKir_pr_DCepr;
        STensorOperation::zero(DdevCorKir_pr_DCepr);
        for (int i=0; i<3; i++)
          for (int s=0; s<3; s++)
            for (int k=0; k<3; k++)
              for (int l=0; l<3; l++)
            	 DdevCorKir_pr_DCepr(i,s,k,l) += DcorKir_pr_DCepr(i,s,k,l) - _I(i,s)*Dp_pr_DCepr(k,l);

        //4. get DdevphiDCepr and DphiPprDCepr; and DphiEprDCepr
        static STensor3 DphiPDCepr, DphiEDCepr, DphiEDdevPhi;
        static STensor43 DdevphiDCepr, DphiDCepr;

        DphiPDCepr = Dp_pr_DCepr;
        DdevphiDCepr = DdevCorKir_pr_DCepr;

        if (PhiEq >0.){
            DphiEDdevPhi = devPhi;
            DphiEDdevPhi *= 1.5/(PhiEq);
        }

        STensorOperation::multSTensor3STensor43(DphiEDdevPhi,DdevphiDCepr,DphiEDCepr);

        DphiDCepr = DdevphiDCepr;
        for (int i=0; i<3; i++)
          for (int s=0; s<3; s++)
            for (int k=0; k<3; k++)
              for (int l=0; l<3; l++)
            	  DphiDCepr(i,s,k,l) += _I(i,s)*DphiPDCepr(k,l);

        // 6. to 11. (inside if loop-> Gamma > 0.)
        static STensor3 dAdCepr, dfDCepr, dgDCepr, DGDCepr, DgammaDCepr, DtrNDCepr, dTrXdCepr;
        static STensor43 DdevNDCepr, dFpDCepr, dDevXdCepr, dXdCepr;

        if (Gamma >0.){

            //5.1 update DphiPDCepr to include the Gamma term
            STensorOperation::zero(DphiPDCepr);
            STensorOperation::multSTensor3STensor43(_I,DlnDCepr,DphiPDCepr);
            DphiPDCepr*= (0.5*Ke);
            DphiPDCepr *= Dho2_v_inv;

            // update DpDCepr (needed for DdevphiDCepr)
            for (int i=0; i<3; i++)
              for (int j=0; j<3; j++)
                DpDCepr(i,j) -= Ke*2*_b*(Gamma*DphiPDCepr(i,j));

            //5.2 update DdevphiDCepr to include the Gamma term
            STensorOperation::zero(DdevphiDCepr);

            // get RHS
            static STensor43 DdevphiDCepr_RHS;
            STensorOperation::zero(DdevphiDCepr_RHS);
            for (int i=0; i<3; i++)
              for (int j=0; j<3; j++)
                for (int k=0; k<3; k++)
                  for (int l=0; l<3; l++){

                    for (int p=0; p<3; p++)
                      for (int q=0; q<3; q++){

                        DdevphiDCepr_RHS(i,j,k,l) +=  ( (Ge_TrEeTensor(i,j,p,q) + rotationStiffness(i,j,p,q) )*0.5*DlnDCepr(p,q,k,l) +
                                        (-Ge_TrEeTensor(i,j,p,q) - rotationStiffness(i,j,p,q))* ( 2.*_b*Gamma/3.*_I(p,q)*DphiPDCepr(k,l) ) );  // EIGEN

                        for (int r=0; r<3; r++)
                          for (int s=0; s<3; s++){

                            DdevphiDCepr_RHS(i,j,k,l) +=  ( Ge_Tensor(i,j,p,q)*_Idev(p,q,r,s)*0.5*DlnDCepr(r,s,k,l));
                          }

                      }
                  }

            STensorOperation::multSTensor43(Dho2_u_inv,DdevphiDCepr_RHS,DdevphiDCepr);
            STensorOperation::multSTensor3STensor43(DphiEDdevPhi,DdevphiDCepr,DphiEDCepr);

            //6. get dAdCepr, dfDCepr
            double fact = a(2)*_n*pow(PhiEq,_n-1.);
            for (int i=0; i<3; i++)
              for (int j=0; j<3; j++){
                dAdCepr(i,j) = ( 4./3.*_b*_b*ptilde*DphiPDCepr(i,j) + 6.*PhiEq*DphiEDCepr(i,j) )/A;
                dfDCepr(i,j) = fact*DphiEDCepr(i,j)-a(1)*DphiPDCepr(i,j);
                // dgDCepr(i,j) = -kk*Gamma*dAdCepr(i,j);
              }
            dgDCepr = dAdCepr;
            dgDCepr *= (-kk*Gamma);

            //7. get DGDCepr, DgammaDCepr
            for (int i=0; i<3; i++)
                for (int j=0; j<3; j++){
                    DGDCepr(i,j) = (-dfDCepr(i,j) + dfdDgamma*dgDCepr(i,j)/dgdDgamma)/(dfdGamma - dfdDgamma*dgdGamma/dgdDgamma);
                    DgammaDCepr(i,j) = (-dgdGamma*DGDCepr(i,j) - dgDCepr(i,j))/dgdDgamma;
                }

            //8.1 update DphiPDCepr
            STensorOperation::zero(DphiPDCepr);
            STensorOperation::multSTensor3STensor43(_I,DlnDCepr,DphiPDCepr);
            DphiPDCepr*= (0.5*Ke);
            for (int i=0; i<3; i++)
              for (int j=0; j<3; j++){
                DphiPDCepr(i,j) += ( - Ke* 2.*_b*ptilde*DGDCepr(i,j)
                                    - 2.*_b*pow(kk,1)/3. * ( (dHb/Cxtr - Hb/pow(Cxtr,2)*dCxtrdDgamma)*DgammaDCepr(i,j)*Gamma*ptilde + Hb/Cxtr*DGDCepr(i,j)*ptilde )
                                    + 1./3.*trXn/pow(Cxtr,2)*dCxtrdDgamma*DgammaDCepr(i,j));
                                    // DEBUG pow(kk,2)
              }
            DphiPDCepr *= Dho2_v_inv;

            // update DpDCepr (needed for DdevphiDCepr)
            STensorOperation::zero(DpDCepr);
            STensorOperation::multSTensor3STensor43(_I,DlnDCepr,DpDCepr);
            DpDCepr*= (0.5*Ke);
            for (int i=0; i<3; i++)
              for (int j=0; j<3; j++)
                DpDCepr(i,j) -= Ke*2*_b*( DGDCepr(i,j)*ptilde + Gamma*DphiPDCepr(i,j));

            //8.2 update DdevphiDCepr and DphiEDCepr
            STensorOperation::zero(DdevphiDCepr);

            // get RHS
            STensorOperation::zero(DdevphiDCepr_RHS);
            for (int i=0; i<3; i++)
              for (int j=0; j<3; j++)
                for (int k=0; k<3; k++)
                  for (int l=0; l<3; l++){

                    DdevphiDCepr_RHS(i,j,k,l) += ( - 3.*pow(kk,1)*( ( dHb/Cxdev - Hb/pow(Cxdev,2)*dCxdevdDgamma )*Gamma*devPhi(i,j)*DgammaDCepr(k,l) + Hb/Cxdev*devPhi(i,j)*DGDCepr(k,l) )
                            + 1./pow(Cxdev,2)*dCxdevdDgamma*devXns(i,j)*DgammaDCepr(k,l)); // DEBUG pow(kk,2)

                    for (int p=0; p<3; p++)
                      for (int q=0; q<3; q++){

                        DdevphiDCepr_RHS(i,j,k,l) +=  ( - 3.*Ge_Tensor(i,j,p,q)*devPhi(p,q)*DGDCepr(k,l)
                                                    + (-Ge_TrEeTensor(i,j,p,q) -rotationStiffness(i,j,p,q)) * ( N(p,q)*DGDCepr(k,l) + 2.*_b*Gamma/3.*_I(p,q)*DphiPDCepr(k,l) )
                                                    +  (Ge_TrEeTensor(i,j,p,q) + rotationStiffness(i,j,p,q)) *0.5*DlnDCepr(p,q,k,l) ) ; // EIGEN
                        for (int r=0; r<3; r++)
                          for (int s=0; s<3; s++){

                            DdevphiDCepr_RHS(i,j,k,l) +=  ( Ge_Tensor(i,j,p,q)*_Idev(p,q,r,s)*0.5*DlnDCepr(r,s,k,l) );
                          }

                      }
                  }

            STensorOperation::multSTensor43(Dho2_u_inv,DdevphiDCepr_RHS,DdevphiDCepr);
            STensorOperation::multSTensor3STensor43(DphiEDdevPhi,DdevphiDCepr,DphiEDCepr);

            DphiDCepr = DdevphiDCepr;
            for (int i=0; i<3; i++)
              for (int s=0; s<3; s++)
                for (int k=0; k<3; k++)
                  for (int l=0; l<3; l++)
                	  DphiDCepr(i,s,k,l) += _I(i,s)*DphiPDCepr(k,l);

            //9. get DtrNDCepr DdevNdCepr
            DtrNDCepr = DphiPDCepr;
            DtrNDCepr *= (2.*_b);

            DdevNDCepr = DdevphiDCepr;
            DdevNDCepr *= 3.;

            // 10. get dFpDCepr
            STensorOperation::zero(dGammaNdCepr);
            for (int i=0; i<3; i++)
              for (int j=0; j<3; j++)
                for (int k=0; k<3; k++)
                  for (int l=0; l<3; l++)
                    dGammaNdCepr(i,j,k,l) = N(i,j)*DGDCepr(k,l) + Gamma*DdevNDCepr(i,j,k,l) + Gamma/3.*_I(i,j)*DtrNDCepr(k,l);

            static STensor43 CeprFp0;
            for (int i=0; i<3; i++)
              for (int j=0; j<3; j++)
                for (int k=0; k<3; k++)
                  for (int l=0; l<3; l++){
                    CeprFp0(i,j,k,l) = 0.;
                    for (int s=0; s<3; s++){
                      CeprFp0(i,j,k,l) += dexpAdA(i,s,k,l)*Fp0(s,j);
                    }
                  }

            STensorOperation::multSTensor43(CeprFp0,dGammaNdCepr,dFpDCepr);

            // 10.1 get dXDCepr
            for (int i=0; i<3; i++)
              for (int j=0; j<3; j++){
                dTrXdCepr(i,j) = pow(kk,1.)*( (dHb/Cxtr - Hb/pow(Cxtr,2)*dCxtrdDgamma)*Gamma*trN*DgammaDCepr(i,j) +
                                        Hb/Cxtr*( trN*DGDCepr(i,j) + Gamma*DtrNDCepr(i,j) ) )
                                        - trXn/pow(Cxtr,2)*dCxtrdDgamma*DgammaDCepr(i,j); // pow(kk,2.) DEBUG
                for (int k=0; k<3; k++)
                  for (int l=0; l<3; l++){
                    dDevXdCepr(i,j,k,l) = pow(kk,1.)*( (dHb/Cxdev - Hb/pow(Cxdev,2)*dCxdevdDgamma)*Gamma*devN(i,j)*DgammaDCepr(k,l) +
                                                Hb/Cxdev*( devN(i,j)*DGDCepr(k,l) + Gamma*DdevNDCepr(i,j,k,l) ) )
                                                - 1./pow(Cxdev,2)*dCxdevdDgamma*devXns(i,j)*DgammaDCepr(k,l); // pow(kk,2.) DEBUG
                  }
                }

            if(_useRotationCorrection){
                for (int i=0; i<3; i++)
                  for (int j=0; j<3; j++)
                     for (int k=0; k<3; k++)
                        for (int l=0; l<3; l++)
                            for (int p=0; p<3; p++)
                              for (int q=0; q<3; q++)
                            	  dDevXdCepr(i,j,k,l) += 3./Cxdev*backStressRotationStiffness(i,j,p,q)*DdevphiDCepr(p,q,k,l);
            }

            STensorOperation::zero(dXdCepr);
            dXdCepr = dDevXdCepr;
            for (int i=0; i<3; i++)
              for (int j=0; j<3; j++)
                for (int k=0; k<3; k++)
                  for (int l=0; l<3; l++)
                    dXdCepr(i,j,k,l) += 1./3. * _I(i,j)*dTrXdCepr(k,l);

            // 11. update DcorKirDCepr
            for (int i=0; i<3; i++)
              for (int j=0; j<3; j++)
                for (int k=0; k<3; k++)
                  for (int l=0; l<3; l++)
                    for (int p=0; p<3; p++)
                      for (int q=0; q<3; q++)
                        DcorKirDCepr(i,j,k,l) -= DcorKirDEe(i,j,p,q)*dGammaNdCepr(p,q,k,l);
        } // if Gamma
        else{
            // elastic
            STensorOperation::zero(DgammaDCepr); STensorOperation::zero(DGDCepr); STensorOperation::zero(DtrNDCepr);
            STensorOperation::zero(dFpDCepr); STensorOperation::zero(dTrXdCepr); STensorOperation::zero(DdevNDCepr);
            STensorOperation::zero(dDevXdCepr); STensorOperation::zero(dXdCepr); STensorOperation::zero(dGammaNdCepr);
        }

        // 12. get  CeprToF conversion tensor
        for (int i=0; i<3; i++)
          for (int j=0; j<3; j++)
            for (int k=0; k<3; k++)
              for (int l=0; l<3; l++)
                CeprToF(i,j,k,l) = 2.*Fepr(k,i)*invFp0(j,l);

        // 13. get DcorKirDF
        static STensor43 DKcorDF;
        STensorOperation::multSTensor43(DcorKirDCepr,CeprToF,DKcorDF);

        // 14. dXdF, dGammaNdF, DphiPDF for mechSrc
        STensor43& dXdF = q1->getRefToDbackStressdF();
        STensorOperation::zero(dXdF);
        STensorOperation::multSTensor43(dXdCepr,CeprToF,dXdF);

        STensor43& dGammaNdF = q1->_dGammaNdF;
        STensorOperation::zero(dGammaNdF);
        STensorOperation::multSTensor43(dGammaNdCepr,CeprToF,dGammaNdF);

        // DphiPDCepr - use this for the pressure term in R
        STensorOperation::zero(DphiPDF);
        for (int i=0; i<3; i++)
          for (int j=0; j<3; j++)
            for (int k=0; k<3; k++)
              for (int l=0; l<3; l++)
                DphiPDF(i,j) = DphiPDCepr(k,l)*CeprToF(k,l,i,j);

        // 15. get DgammaDF
        STensor3& DgammaDF = q1->_DgammaDF;
        STensor3& DGammaDF = q1->_DGammaDF;
        STensor43& DphiDF = q1->_DphiDF;
        STensorOperation::zero(DgammaDF);
        STensorOperation::zero(DGammaDF);
        STensorOperation::zero(DphiDF);
        if (Gamma > 0){
          STensorOperation::multSTensor3STensor43(DgammaDCepr,CeprToF,DgammaDF);
          STensorOperation::multSTensor3STensor43(DGDCepr,CeprToF,DGammaDF);
          STensorOperation::multSTensor43(dFpDCepr,CeprToF,dFpdF);
        }
        STensorOperation::multSTensor43(DphiDCepr,CeprToF,DphiDF);

        // 16. Everything else - Conversion Tensors
        static STensor43 DinvFpDF, dCedF, dCeinvDF;
        for (int i=0; i<3; i++)
          for (int s=0; s<3; s++)
            for (int k=0; k<3; k++)
              for (int l=0; l<3; l++){
                DinvFpDF(i,s,k,l) = 0.;
                for (int m=0; m<3; m++)
                  for (int j=0; j<3; j++)
                    DinvFpDF(i,s,k,l) -= Fpinv(i,m)*dFpdF(m,j,k,l)*Fpinv(j,s);
              }

        for (int m=0; m<3; m++)
          for (int j=0; j<3; j++)
            for (int k=0; k<3; k++)
              for (int l=0; l<3; l++){
                dFedF(m,j,k,l) = _I(m,k)*Fpinv(l,j);
                for (int s=0; s<3; s++)
                  dFedF(m,j,k,l) += F(m,s)*DinvFpDF(s,j,k,l);
              }

        for (int i=0; i<3; i++)
          for (int j=0; j<3; j++)
            for (int k=0; k<3; k++)
              for (int l=0; l<3; l++){
                dCedF(i,j,k,l) = 0.;
                for (int p=0; p<3; p++){
                    dCedF(i,j,k,l) += Fe(p,i)*dFedF(p,j,k,l) + dFedF(p,i,k,l)*Fe(p,j); // This Works!
                }
              }

        for (int i=0; i<3; i++)
          for (int s=0; s<3; s++)
            for (int k=0; k<3; k++)
              for (int l=0; l<3; l++){
                dCeinvDF(i,s,k,l) = 0.;
                for (int m=0; m<3; m++)
                  for (int j=0; j<3; j++)
                    dCeinvDF(i,s,k,l) -= Ceinv(i,m)*dCedF(m,j,k,l)*Ceinv(j,s);
              }

        // 17. Tangent - dPdF
        static STensor43 dSdCepr, dSdF, dMedCepr;
        STensor43& dMedF = q1->getRefToDModMandelDF();
        STensorOperation::zero(dMedF);

        for (int i=0; i<3; i++)
          for (int j=0; j<3; j++)
            for (int k=0; k<3; k++)
              for (int l=0; l<3; l++)
                dMedF(i,j,k,l) = DKcorDF(i,j,k,l);  // KS is corKir


        static STensor63 DlnDF;
        if (_order != 1){
          for (int i=0; i<3; i++){
            for (int j=0; j<3; j++){
              for (int k=0; k<3; k++){
                for (int l=0; l<3; l++){
                  for (int p=0; p<3; p++){
                    for (int q=0; q<3; q++){
                      DlnDF(i,j,k,l,p,q) = 0.;
                      for (int r=0; r<3; r++){
                        for (int s=0; s<3; s++){
                          for (int a=0; a<3; a++){
                            DlnDF(i,j,k,l,p,q) += DDlnDDCe(i,j,k,l,r,s)*2.*Fe(a,r)*dFedF(a,s,p,q);
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }

        }
        else{
          STensorOperation::zero(DlnDF);
        }

        STensorOperation::zero(dSdF);
        for (int i=0; i<3; i++)
          for (int j=0; j<3; j++)
            for (int k=0; k<3; k++)
              for (int l=0; l<3; l++)
                for (int m=0; m<3; m++)
                  for (int n=0; n<3; n++){
                    dSdF(i,j,k,l) += DKcorDF(m,n,k,l)*DlnDCe(m,n,i,j);
                    dSdF(i,j,k,l) += KS(m,n)*DlnDF(m,n,i,j,k,l);
                  }

        for (int i=0; i<3; i++)
          for (int j=0; j<3; j++)
            for (int k=0; k<3; k++)
              for (int l=0; l<3; l++){
                Tangent(i,j,k,l) = 0.;
                for (int m=0; m<3; m++){
                  for (int n=0; n<3; n++){
                    Tangent(i,j,k,l) += dFedF(i,m,k,l)*S(m,n)*Fpinv(j,n);
                    Tangent(i,j,k,l) += Fe(i,m)*dSdF(m,n,k,l)*Fpinv(j,n);
                    Tangent(i,j,k,l) += Fe(i,m)*S(m,n)*DinvFpDF(j,n,k,l);
                  }
                }
              }


        // dP/dT

        // 1. get dMeprDT from dKeprDT  -> DcorKirprDT from TVE
        static STensor3 dDevKeprDT, dDevCorKirDT, DcorKirDT;
        double DpDT(0.), dpKeprDT(0.);

        STensorOperation::decomposeDevTr(dKeprDT,dDevKeprDT,dpKeprDT);
        dpKeprDT = dKeprDT.trace()/3.;

        DcorKirDT = dKeprDT; // update later

        // 2. initialise dXdT - > need this for DmechsourceDT -> initialise here (X is backStress)
        STensor3& dXdT = q1->getRefToDbackStressdT();
        STensorOperation::zero(dXdT);
        STensor3& dGammaNdT =  q1->_dGammaNdT;
        STensorOperation::zero(dGammaNdT);

        // 3. get dPhipprDT and dDevPhiprDT
        double dPhiPdT(0.);
        static STensor3 dDevPhiDT;
        STensor3& DphiDT = q1->_DphiDT;

        dPhiPdT = dpKeprDT;
        dDevPhiDT = dDevKeprDT;

        DphiDT = dDevPhiDT;
        for (int i=0; i<3; i++)
          for (int j=0; j<3; j++)
        	  DphiDT(i,j) +=  dPhiPdT*_I(i,j);

        // 4. get dPhiEdT
        double dPhiEdT(0.);
        STensorOperation::doubleContractionSTensor3(DphiEDdevPhi,dDevPhiDT,dPhiEdT);

        // 5. to 11. (inside if loop-> Gamma > 0.)
        double& dGammaDT = q1->_DGammaDT;
        double& DgammaDT = q1->_DgammaDT;
        dGammaDT = 0.; DgammaDT =0.;

        double dAdT(0.), dfdT(0.), dgdT(0.), DtrNdT(0.), DtrXdT(0.);
        static STensor3 DdevNdT, dFpDT, DdevXdT;

        // eta(0.), Deta(0.), DetaDT(0.);
        if (_viscosity != NULL)
            _viscosity->get(q1->_epspbarre,T,eta,Deta,DetaDT);
        double etaOverDt = eta/this->getTimeStep();

        if (Gamma >0){
        // plastic

            // =============================== ROUND 1 - Updates for dAdT, dfdT, dgdT ================================================== //

            // 5.1 update dCxdT
            double dCxdevdT(0.), dCxtrdT(0.);
            // dHbdT+dHb*DgammaDT
            if (Hb>0.){
                dCxdevdT = -m(0)*( -1./pow(Hb,2.)*dHb*Dgamma*(dHbdT) + 1./Hb*(ddHbdgammadT*Dgamma + dHb*DgammaDT))
                      + (1./pow(Hb,2.)*(dHbdT)*(dHbdT) - 1./Hb*ddHbddT)*(T-T0) -1./Hb*dHbdT;
            }
            dCxtrdT = dCxdevdT;
            dCxdevdT += 3.*m(1)*pow(kk,1.)*(dHbdT); // pow(kk,2.) DEBUG
            dCxtrdT += 2.*_b/3.*m(1)*pow(kk,1.)*(dHbdT); // pow(kk,2.) DEBUG

            // 5.2 update DcorKirDT for shift factor derivatives - // dAdT and dBdT for maxwell elements are updated inside the loop for Phi
            mlawNonLinearTVM::getTVEdCorKirDT(q0,q1,T0,T);
            DcorKirDT = q1->_DcorKirDT;
            STensorOperation::decomposeDevTr(DcorKirDT,dDevCorKirDT,DpDT);
            DpDT = DcorKirDT.trace()/3.;

            // 5.2 update dPhiPdT, dDevPhiDT to get dAdT, dfdT, dgdT
            dPhiPdT = DpDT + Ke*(-2*_b*ptilde*dGammaDT)
                        - 2.*_b*pow(kk,1)/3.*( (dHbdT/Cxtr - Hb/pow(Cxtr,2)*dCxtrdT ) *Gamma*ptilde + Hb/Cxtr*dGammaDT*ptilde )
                        + 1./3.*trXn/pow(Cxtr,2)*dCxtrdT ; // pow(kk,2.) DEBUG
            dPhiPdT *= Dho2_v_inv;

            // 5.3 update DpDT
            DpDT += 2*_b*Ke*(-dGammaDT*ptilde - Gamma*dPhiPdT);

            // 5.4 update DdevphiDT
            static STensor3 DdevphiDT_RHS;
            for (int i=0; i<3; i++)
              for (int j=0; j<3; j++){
                DdevphiDT_RHS(i,j) = ( dDevCorKirDT(i,j) )
                                        - 3*pow(kk,1)*( (dHbdT/Cxdev - Hb/pow(Cxdev,2)*dCxdevdT)*Gamma*devPhi(i,j) + Hb/Cxdev*dGammaDT*devPhi(i,j) )
                                        + 1./pow(Cxdev,2)*dCxdevdT*devXns(i,j);  // pow(kk,2.) DEBUG
                for (int k=0; k<3; k++)
                  for (int l=0; l<3; l++){
                    DdevphiDT_RHS(i,j) += ( -3*dGammaDT*Ge_Tensor(i,j,k,l)*devPhi(k,l)
                                                + (-Ge_TrEeTensor(i,j,k,l) -rotationStiffness(i,j,k,l))*( dGammaDT*N(k,l) + 2.*_b/3.*dPhiPdT*_I(k,l) ) ); // EIGEN
                  }
              }

            STensorOperation::multSTensor3STensor43(DdevphiDT_RHS,Dho2_u_inv,dDevPhiDT);
            STensorOperation::doubleContractionSTensor3(DphiEDdevPhi,dDevPhiDT,dPhiEdT);

            // =============================== ROUND 1 - END ========================================================================= //

            // 6. get dAdT, dfdT, dgdT
            fullVector<double> DaDT(3), DDaDTT(3);
            getYieldCoefficientTempDers(q1,T,DaDT,DDaDTT);

            dAdT = (4./3.*_b*_b*ptilde*dPhiPdT + 6.*PhiEq*dPhiEdT)/A;
            dfdT = DaDT(2)*pow(PhiEq,_n) + a(2)*_n*pow(PhiEq,_n-1.)*dPhiEdT - DaDT(1)*ptilde - a(1)*dPhiPdT - DaDT(0);
            if (this->getTimeStep()>0){
                dfdT -= (DetaDT*Gamma/this->getTimeStep())*_p*pow((eta*Gamma/this->getTimeStep()),(_p-1.));
            }

            dgdT = -kk*Gamma*dAdT;

            // 7. get dGammaDT, DgammaDT
            dGammaDT = (-dfdT + dfdDgamma*dgdT/dgdDgamma)/(dfdGamma - dfdDgamma*dgdGamma/dgdDgamma);
            DgammaDT = (-dgdGamma*dGammaDT - dgdT)/dgdDgamma;

            // =============================== ROUND 2 - Updates for dPhiPdT, DdevphiDT ================================================== //
            // 8. update dCxdT
            // dHbdT+dHb*DgammaDT
            if (Hb>0.){
                dCxdevdT = -m(0)*( -1./pow(Hb,2.)*dHb*Dgamma*(dHbdT) + 1./Hb*(ddHbdgammadT*Dgamma + dHb*DgammaDT))
                      + (1./pow(Hb,2.)*(dHbdT)*(dHbdT) - 1./Hb*ddHbddT)*(T-T0) -1./Hb*dHbdT;
            }
            dCxtrdT = dCxdevdT;
            dCxdevdT += 3.*m(1)*pow(kk,1.)*(dHbdT); // pow(kk,2.) DEBUG
            dCxtrdT += 2.*_b/3.*m(1)*pow(kk,1.)*(dHbdT); // pow(kk,2.) DEBUG

            // 9. update DpDT 1 - shift factor derivatives
            DpDT = DcorKirDT.trace()/3.;

            // 10. update dPhiPdT
            dPhiPdT = DpDT + Ke*(-2*_b*ptilde*dGammaDT)
                        - 2.*_b*pow(kk,1)/3.*( (dHbdT/Cxtr - Hb/pow(Cxtr,2)*dCxtrdT ) *Gamma*ptilde + Hb/Cxtr*dGammaDT*ptilde )
                        + 1./3.*trXn/pow(Cxtr,2)*dCxtrdT ; // pow(kk,2.) DEBUG
            dPhiPdT *= Dho2_v_inv;

            // 10.1 update DpDT 2
            DpDT += 2*_b*Ke*(-dGammaDT*ptilde - Gamma*dPhiPdT);

            // 11. update DdevphiDT
            STensorOperation::zero(DdevphiDT_RHS);
            for (int i=0; i<3; i++)
              for (int j=0; j<3; j++){
                DdevphiDT_RHS(i,j) = ( dDevCorKirDT(i,j) )
                                    - 3*pow(kk,1)*( (dHbdT/Cxdev - Hb/pow(Cxdev,2)*dCxdevdT)*Gamma*devPhi(i,j) + Hb/Cxdev*dGammaDT*devPhi(i,j) )
                                    + 1./pow(Cxdev,2)*dCxdevdT*devXns(i,j);   // pow(kk,2.) DEBUG
                for (int k=0; k<3; k++)
                  for (int l=0; l<3; l++){
                    DdevphiDT_RHS(i,j) += ( -3*dGammaDT*Ge_Tensor(i,j,k,l)*devPhi(k,l)
                                                + (-Ge_TrEeTensor(i,j,k,l) - rotationStiffness(i,j,k,l))*( dGammaDT*N(k,l) + 2.*_b/3.*Gamma*dPhiPdT*_I(k,l) ) ); // EIGEN
                  }
              }

            STensorOperation::multSTensor3STensor43(DdevphiDT_RHS,Dho2_u_inv,dDevPhiDT);

            DphiDT = dDevPhiDT;
            for (int i=0; i<3; i++)
              for (int j=0; j<3; j++)
            	  DphiDT(i,j) +=  dPhiPdT*_I(i,j);

            // =============================== ROUND 2 - END ========================================================================= //

            // 12. get DtrNdT, DdevNdT
            DtrNdT = dPhiPdT;
            DtrNdT *= (2.*_b);

            DdevNdT = dDevPhiDT;
            DdevNdT *= 3.;

            // 13. get dFpdT
            STensorOperation::zero(dGammaNdT);
            for (int i=0; i<3; i++)
              for (int j=0; j<3; j++)
                dGammaNdT(i,j) += N(i,j)*dGammaDT + Gamma*DdevNdT(i,j) + Gamma/3.*_I(i,j)*DtrNdT;

            static STensor43 CeprFp0;
            for (int i=0; i<3; i++)
              for (int j=0; j<3; j++)
                for (int k=0; k<3; k++)
                  for (int l=0; l<3; l++){
                    CeprFp0(i,j,k,l) = 0.;
                    for (int s=0; s<3; s++)
                      CeprFp0(i,j,k,l) += dexpAdA(i,s,k,l)*Fp0(s,j);
                  }

            for (int i=0; i<3; i++)
              for (int j=0; j<3; j++){
                dFpdT(i,j) = 0.;
                for (int k=0; k<3; k++)
                  for (int l=0; l<3; l++)
                    dFpdT(i,j) += CeprFp0(i,j,k,l)*dGammaNdT(k,l);
                }

            // 14. dXdT - backstress temperature derivative -> correction - CHECK!!!
            DtrXdT = pow(kk,1.)*( (dHbdT/Cxtr - Hb/pow(Cxtr,2)*dCxtrdT ) *Gamma*trN + Hb/Cxtr*(dGammaDT*trN + Gamma*DtrNdT) ) - trXn/pow(Cxtr,2)*dCxtrdT; // pow(kk,2.) DEBUG
            for (int i=0; i<3; i++)
              for (int j=0; j<3; j++)
                DdevXdT(i,j) = pow(kk,1.)*( (dHbdT/Cxdev - Hb/pow(Cxdev,2)*dCxdevdT)*Gamma*devN(i,j) + Hb/Cxdev*( dGammaDT*devN(i,j) + Gamma*DdevNdT(i,j) ) )
                                - 1./pow(Cxdev,2)*dCxdevdT * devXns(i,j);  // pow(kk,2.) DEBUG

            if(_useRotationCorrection){
                for (int i=0; i<3; i++)
                  for (int j=0; j<3; j++)
                     for (int k=0; k<3; k++)
                        for (int l=0; l<3; l++)
                           DdevXdT(i,j) += 3/Cxdev*backStressRotationStiffness(i,j,k,l)*dDevPhiDT(k,l);
            }

            STensorOperation::zero(dXdT);
            dXdT = DdevXdT;
            dXdT(0,0) = DtrXdT/3.;
            dXdT(1,1) = DtrXdT/3.;
            dXdT(2,2) = DtrXdT/3.;

            // 15. update DcorKirDT
            for (int i=0; i<3; i++)
              for (int j=0; j<3; j++)
                for (int k=0; k<3; k++)
                  for (int l=0; l<3; l++)
                      DcorKirDT(i,j) -= DcorKirDEe(i,j,k,l)*dGammaNdT(k,l);

            q1->_DcorKirDT = DcorKirDT; // update in IP

        } // if Gamma
        else{
            // elastic
            STensorOperation::zero(dFpDT); STensorOperation::zero(DdevXdT); STensorOperation::zero(DdevNdT);
            }

        // 16. get dKcorDT
        // done above

        // 17. get DinvFpdT, dFedT, dCedT, dCeinvDT
        static STensor3 DinvFpdT, dFedT, dCedT, dCeinvdT;
        STensorOperation::zero(DinvFpdT);
        for (int i=0; i<3; i++)
          for (int j=0; j<3; j++)
            for (int p=0; p<3; p++)
              for (int q=0; q<3; q++)
                DinvFpdT(i,j) -= Fpinv(i,p)*dFpdT(p,q)*Fpinv(q,j);

        STensorOperation::zero(dFedT);
        for (int i=0; i<3; i++)
          for (int j=0; j<3; j++)
            for (int k=0; k<3; k++)
              dFedT(i,j) += F(i,k)*DinvFpdT(k,j);

        STensorOperation::zero(dCedT);
        for (int i=0; i<3; i++)
          for (int j=0; j<3; j++)
            for (int p=0; p<3; p++)
              dCedT(i,j) += Fe(p,i)*dFedT(p,j) + dFedT(p,i)*Fe(p,j); // This Works!

        STensorOperation::zero(dCeinvdT);
        for (int i=0; i<3; i++)
          for (int s=0; s<3; s++)
            for (int k=0; k<3; k++)
              for (int l=0; l<3; l++)
                dCeinvdT(i,s) -= Ceinv(i,k)*dCedT(k,l)*Ceinv(l,s);

        // 18. get dMeDT -> needed for DmechSourceDT
        STensor3& dMeDT = q1->getRefToDModMandelDT();
        STensorOperation::zero(dMeDT);

        for (int i=0; i<3; i++)
          for (int j=0; j<3; j++)
            dMeDT(i,j) = DcorKirDT(i,j);

        // 19. get dSdT

        static STensor43 DLDT;
        STensorOperation::zero(DLDT);
        for (int i=0; i<3; i++)
          for (int j=0; j<3; j++)
            for (int k=0; k<3; k++)
              for (int l=0; l<3; l++)
                for (int r=0; r<3; r++)
                  for (int s=0; s<3; s++)
                    for (int a=0; a<3; a++)
                      DLDT(i,j,k,l) += DDlnDDCe(i,j,k,l,r,s)*2.*Fe(a,r)*dFedT(a,s);

        static STensor3 dSdT;
        for (int i=0; i<3; i++)
          for (int j=0; j<3; j++){
            dSdT(i,j) = 0.;
            for (int r=0; r<3; r++)
              for (int s=0; s<3; s++){
            	dSdT(i,j) += DcorKirDT(r,s)*DlnDCe(r,s,i,j) + KS(r,s)*DLDT(r,s,i,j);
            }
         }

         // 20. Finally, get dPdT
        for (int i=0; i<3; i++)
          for (int j=0; j<3; j++){
            dPdT(i,j) = 0.;
            for (int k=0; k<3; k++)
              for (int l=0; l<3; l++){
                dPdT(i,j) += (dFedT(i,k)*S(k,l)*Fpinv(j,l) + Fe(i,k)*dSdT(k,l)*Fpinv(j,l) + Fe(i,k)*S(k,l)*DinvFpdT(j,l));
              }
          }

        // TVP - flux derivatives
        // fluxT
        dfluxTdT = fluxT;
        dfluxTdT *= (DKThConDT/KThConT);
        dfluxTdgradT = Cinv;
        dfluxTdgradT *= (-KThConT*J);
        STensorOperation::zero(dfluxTdF);

        if (!_thermalEstimationPreviousConfig){

            static STensor3 DJDF;
            static STensor43 DCinvDF;
            for (int i=0; i<3; i++){
              for (int j=0; j<3; j++){
                DJDF(i,j) = J*Finv(j,i);
                for (int k=0; k<3; k++){
                  for (int l=0; l<3; l++){
                    DCinvDF(i,j,k,l) = 0.;
                    for (int p=0; p<3; p++){
                      for (int a=0; a<3; a++){
                        for (int b=0; b<3; b++){
                          DCinvDF(i,j,k,l) -= 2.*F(k,p)*Cinv(i,a)*Cinv(j,b)*_I4(a,b,p,l);
                        }
                      }
                    }
                  }
                }
              }
            }

            for (int i=0; i<3; i++){
              for (int j=0; j<3; j++){
                for (int k=0; k<3; k++){
                  dfluxTdF(i,j,k) += (DJDF(j,k)*fluxT(i)/J);
                  for (int l=0; l<3; l++){
                    dfluxTdF(i,j,k) -= (J*DCinvDF(i,l,j,k)*gradT(l)*KThConT);
                  }
                }
              }
            }
        }

    } // if stiff


    // thermalEnergy
    if (stiff){
        double& DDpsiTVMdTT = q1->getRefToDDFreeEnergyTVMdTT();
        // getFreeEnergyTVM(q0,q1,T0,T,NULL,NULL,&DDpsiTVMdTT);
        // CpT = -T*DDpsiTVMdTT;
        getCp(CpT,T);
    }
    else{
        getCp(CpT,T);
    }

    q1->_thermalEnergy = CpT*T;

    // thermalSource
    if (this->getTimeStep() > 0.){
        thermalSource = -CpT*(T-T0)/this->getTimeStep();
    }
    else
        thermalSource = 0.;

    // mechanical Source
    double& Wm_TVP = q1->getRefToMechSrcTVP(); // TVP
    double& Wm_TVE = q1->getRefToMechSrcTVE(); // TVE

    mechanicalSource = 0.;
    double mechanicalSource_cap = 0.; // in case of mullins, undamaged mechanicalSource

    // 1) ThermoElastic Heat -> _DcorKirDT needs to corrected for plasticity ( DcorKirDT = dcorKir_dT + dCorKirDEe * dEeDT )
    static STensor3 DEe;
    DEe = q1->_Ee;
    DEe -= q0->_Ee;
    if (this->getTimeStep() > 0){
        // mechanicalSource += (STensorOperation::doubledot(q1->_DcorKirDT,DEe)*T/this->getTimeStep());
    }

    // 2) Viscoelastic Contribution to mechSrc
    double& dWmdT_TVE = q1->getRefTodMechSrcTVEdT();
    STensor3& dWmdF_TVE = q1->getRefTodMechSrcTVEdF();
    static STensor3 dWmdE_TVE;
    mlawNonLinearTVM::getMechSource_nonLinearTVE_term(q0,q1,T0,T,Wm_TVE,stiff,dWmdE_TVE,dWmdT_TVE);
    // mechanicalSource += Wm_TVE;

    // 3) Viscoplastic Contribution to mechSrc
    double& dWmdT_TVP = q1->getRefTodMechSrcTVPdT();
    STensor3& dWmdF_TVP = q1->getRefTodMechSrcTVPdF();
    mlawNonLinearTVP::getMechSourceTVP(F0,F,q0,q1,T0,T,Fepr,Cepr,DphiPDF,Wm_TVP,dWmdF_TVP,dWmdT_TVP);
    mechanicalSource += Wm_TVP;

    // --------------------------------------------------------------------------------------------
    // TVE energy and derivatives
    double& Dpsi_DT = q1->getRefTo_Dpsi_DT();
    STensor3& Dpsi_TVEdE = q1->getRefTo_Dpsi_DE();
    q1->_elasticEnergy = mlawNonLinearTVM::freeEnergyMechanical(*q0,*q1,T0,T,&Dpsi_DT,&Dpsi_TVEdE);

    // TVP energy and derivatives
    double psi_TVP = freeEnergyPlasticity(q0,q1,T0,T);
    double Dpsi_TVPdT(0.);
    static STensor3 Dpsi_TVPdF;
    freeEnergyPlasticityDerivatives(q0,q1,T0,T,Dpsi_TVPdF,Dpsi_TVPdT);
    // q1->_elasticEnergy += psi_TVP; // Mullins TVP
    // Dpsi_DT += Dpsi_TVPdT;  // add plastic part // Mullins TVP
    Dpsi_DT -= dot(Dpsi_TVEdE,q1->_dGammaNdT); // add plastic dependency of elastic strain // Mullins TVP

    // Mullins Effect
    if (_mullinsEffect != NULL && q1->_ipMullinsEffect != NULL){
        mlawNonLinearTVM::calculateMullinsEffectScaler(q0, q1, T, &Dpsi_DT);
    }
    double eta_mullins = 1.;

    if (q1->_ipMullinsEffect != NULL){

        // Put Stress in IP
        q1->_P_cap = P;

        // Modify Stress for mullins
        eta_mullins = q1->_ipMullinsEffect->getEta();
        P *= eta_mullins;
        // q1->_elasticEnergy *= eta_mullins;

    	mechanicalSource_cap = mechanicalSource;
        mechanicalSource *= q1->_ipMullinsEffect->getEta();
        if (this->getTimeStep() > 0){
            mechanicalSource -= q1->_ipMullinsEffect->getDpsiNew_DpsiMax()*(q1->_ipMullinsEffect->getpsiMax() - q0->_ipMullinsEffect->getpsiMax())/this->getTimeStep();
        }
    }


    if(stiff){

    	// --------------------------------------------------------------------------------------------
        // Important conversion tensors
        STensorOperation::zero(DEeDCepr);
        STensorOperation::zero(DEeDF);

        // DEeDCepr to DEeDF
        // STensorOperation::multSTensor43(_I4, DlnDCepr, DEeDCepr);
        DEeDCepr = DlnDCepr;
        DEeDCepr *= 0.5;
        DEeDCepr -= dGammaNdCepr;
        STensorOperation::multSTensor43(DEeDCepr, CeprToF, DEeDF);

        // Note: DEeDT = - dGammaNdT
        // --------------------------------------------------------------------------------------------

        // thermSrc and MechSrc Derivatives

        // thermal source derivatives
        double DCpDT(0.);
        mlawNonLinearTVM::getCp(CpT,T,&DCpDT);
        static STensor3 DCpDF;
        STensorOperation::zero(DCpDF);                                                       // CHANGE for DCpDF

        if (this->getTimeStep() > 0){
            dthermalSourcedT = -DCpDT*(T-T0)/this->getTimeStep() - CpT/this->getTimeStep();
            // dthermalSourcedT = -CpT/this->getTimeStep() - (CpT-CpT_0)/this->getTimeStep();
            for(int i = 0; i< 3; i++){
              for(int j = 0; j< 3; j++){
                dthermalSourcedF(i,j) = -DCpDF(i,j)*(T-T0)/this->getTimeStep();
              }
            }
        }
        else{
            dthermalSourcedT = 0.;
            STensorOperation::zero(dthermalSourcedF);
        }

        // mechSourceTVP derivatives
        dmechanicalSourcedT = 0.;
        STensorOperation::zero(dmechanicalSourceF);
        static STensor3 dmechanicalSourceE;
        STensorOperation::zero(dmechanicalSourceE);

          // DcorKirDT Term
        STensor3& DDcorKirDTT = q1->getRefToDDcorKirDTT();
        STensor43& DDcorKirDTDEe = q1->getRefToDDcorKirDTDE();

        static STensor3 DcorKirDT_I;
        STensorOperation::multSTensor3STensor43(q1->_DcorKirDT,_I4,DcorKirDT_I);
        if (this->getTimeStep() > 0){
            for (int i=0; i<3; i++){
                for (int j=0; j<3; j++){
                    dmechanicalSourceE(i,j) += T*DcorKirDT_I(i,j)/this->getTimeStep();
                    for (int k=0; k<3; k++)
                        for (int l=0; l<3; l++){
                            dmechanicalSourceE(i,j) += T*DDcorKirDTDEe(k,l,i,j)*DEe(k,l)/this->getTimeStep();
                            DDcorKirDTT(i,j) += - DDcorKirDTDEe(i,j,k,l)*q1->_dGammaNdT(k,l); // additional term due to TVP
                        }
                }
            }
            // STensorOperation::multSTensor3STensor43(dmechanicalSourceE,DEeDF,dmechanicalSourceF);

            // dmechanicalSourcedT += (STensorOperation::doubledot(q1->_DcorKirDT,DEe)/this->getTimeStep());
            // dmechanicalSourcedT += T*(STensorOperation::doubledot(DDcorKirDTT,DEe)/this->getTimeStep());
            // dmechanicalSourcedT -= T*(STensorOperation::doubledot(q1->_DcorKirDT,q1->_dGammaNdT)/this->getTimeStep());
        }

        // TVE Term
        STensorOperation::multSTensor3STensor43(dWmdE_TVE, DEeDF, dWmdF_TVE);
        for (int i=0; i<3; i++)
            for (int j=0; j<3; j++)
                dWmdT_TVE += - dWmdE_TVE(i,j) *  q1->_dGammaNdT(i,j); // the second term in dWmdT_TVE from dependency of Ee on plasticity

        // dmechanicalSourceF += dWmdF_TVE;
        // dmechanicalSourcedT += dWmdT_TVE;


        // TVP Term
        dmechanicalSourceF += dWmdF_TVP;
        dmechanicalSourcedT += dWmdT_TVP;

    	// Mullins Effect Derivatives

        // Compute Tangents for Mullin's Effect
        if (_mullinsEffect != NULL && q1->_ipMullinsEffect != NULL){

            // 1st Term
            Tangent *= q1->_ipMullinsEffect->getEta();
            dPdT *= q1->_ipMullinsEffect->getEta();
            dmechanicalSourceF *= q1->_ipMullinsEffect->getEta();
            dmechanicalSourcedT *= q1->_ipMullinsEffect->getEta();


            // 2nd Term
            double Deta_mullins_DT(0.);
            static STensor3 Deta_mullins_DF, Deta_mullins_DEe;

                // Deta_mullins_DF
            STensorOperation::multSTensor3STensor43(q1->_DpsiDE,DEeDF,Deta_mullins_DF);
            Deta_mullins_DF *= q1->_DmullinsDamage_Dpsi_cap; // elastic part
            // Deta_mullins_DF += Dpsi_TVPdF*q1->_DmullinsDamage_Dpsi_cap; // plastic part // Mullins TVP

                // Deta_mullins_DT
            // STensorOperation::doubleContractionSTensor3(q1->_DpsiDE,q1->_dGammaNdT,Deta_mullins_DT); // Remove - Mullins TVP
            // Deta_mullins_DT *= -q1->_DmullinsDamage_Dpsi_cap; // Remove - Mullins TVP
            // Deta_mullins_DT += q1->_DmullinsDamage_DT; // Remove - Mullins TVP
            Deta_mullins_DT = q1->_DmullinsDamage_DT; // + q1->_DmullinsDamage_Dpsi_cap*q1->_DpsiDT; // Mullins TVP

            // 2nd Term - 1st PK
            for (int i=0; i<3; i++)
                for (int j=0; j<3; j++){
                    dPdT(i,j) += q1->_P_cap(i,j) * Deta_mullins_DT;
                        for (int k=0; k<3; k++)
                            for (int l=0; l<3; l++)
                                Tangent(i,j,k,l) += q1->_P_cap(i,j) * Deta_mullins_DF(k,l);
                }


            // 2nd Term - MechSrc
            dmechanicalSourcedT += mechanicalSource_cap*Deta_mullins_DT;
            for (int i=0; i<3; i++)
                for (int j=0; j<3; j++){
                    dmechanicalSourceF(i,j) += mechanicalSource_cap*Deta_mullins_DF(i,j);
                }

            static STensor3 DDpsiNew_DpsiMaxDF, DpsiMax_DF;
            STensorOperation::multSTensor3STensor43(q1->_DpsiDE,DEeDF,DDpsiNew_DpsiMaxDF);
            STensorOperation::multSTensor3STensor43(q1->_DpsiDE,DEeDF,DpsiMax_DF);
            DDpsiNew_DpsiMaxDF *= q1->_ipMullinsEffect->getDDpsiNew_DpsiMaxDpsi();
            DpsiMax_DF *= q1->_ipMullinsEffect->getDpsiMax_Dpsi();

            if (this->getTimeStep() > 0){
                dmechanicalSourcedT -= (q1->_ipMullinsEffect->getDDpsiNew_DpsiMaxDT()*(q1->_ipMullinsEffect->getpsiMax() - q0->_ipMullinsEffect->getpsiMax())/this->getTimeStep()
                							+ q1->_ipMullinsEffect->getDpsiNew_DpsiMax()*q1->_ipMullinsEffect->getDpsiMax_Dpsi()*q1->_DpsiDT/this->getTimeStep());
                for (int i=0; i<3; i++)
                    for (int j=0; j<3; j++){
                        dmechanicalSourceF(i,j) -= DDpsiNew_DpsiMaxDF(i,j)*(q1->_ipMullinsEffect->getpsiMax() - q0->_ipMullinsEffect->getpsiMax())/this->getTimeStep();
                        dmechanicalSourceF(i,j) -= q1->_ipMullinsEffect->getDpsiNew_DpsiMax()*DpsiMax_DF(i,j)/this->getTimeStep();
                    }
            }

        } // mullin's
    }

}

void mlawNonLinearTVENonLinearTVP::get_G1_Tensor(const STensor3& Cepr, const STensor3& expGN,
                                    const STensor43& DCeinvprDCepr, const STensor3& KS, STensor43& G1) const{

  static STensor3 Hinv;
  STensorOperation::inverseSTensor3(expGN,Hinv);

  static STensor3 Ceinvpr;
  STensorOperation::inverseSTensor3(Cepr,Ceinvpr);

  static STensor43 term1, term2;

  for (int i=0; i<3; i++)
    for (int j=0; j<3; j++)
      for (int k=0; k<3; k++)
        for (int l=0; l<3; l++){
          term1(i,j,k,l) = 0.;
          term2(i,j,k,l) = 0.;
          for (int p=0; p<3; p++)
            for (int q=0; q<3; q++)
              for (int r=0; r<3; r++)
                for (int s=0; s<3; s++)
                  for (int m=0; m<3; m++)
                    for (int o=0; o<3; o++){
                      term1(i,j,k,l) += Hinv(i,p)*Hinv(p,q)*_I4(q,r,k,l)*KS(r,s)*Ceinvpr(s,m)*expGN(m,o)*expGN(o,j);
                      term2(i,j,k,l) += Hinv(i,p)*Hinv(p,q)*Cepr(q,r)*KS(r,s)*DCeinvprDCepr(s,m,k,l)*expGN(m,o)*expGN(o,j);
                    }
         }
  STensorOperation::zero(G1);
  G1 = term1+term2;
}

void mlawNonLinearTVENonLinearTVP::getDho(const double& Gamma, const STensor3& Cepr, const STensor3& Ceinvpr, const STensor3& KS,
                                  const double& Ke, const STensor43& Ge_Tensor,
                                  const double& kk, const double& Hb, const double& Cxtr, const double& Cxdev,
                                  const STensor3& expGN, const STensor43& dexpAdA,
                                  STensor43& Dho, STensor43& Dho2inv, STensor43& Dho2_u_inv, double& Dho2_v_inv,
								  STensor43* Ge_TrEeTensor, STensor43* rotationStiffness, STensor43* backStressRotationStiffness) const{

  // Ge_TrEeTensor -> Dependence of devCorKir on TrEe. DO NOT CHANGE ITS VALUE!

 // Get DcorKirDEe for Dho 2nd term
 static STensor43 DcorKirDEe;
 STensorOperation::zero(DcorKirDEe);
 for (int i=0; i<3; i++)
    for (int j=0; j<3; j++)
      for (int k=0; k<3; k++)
        for (int l=0; l<3; l++){
            DcorKirDEe(i,j,k,l) += Ke*_I(i,j)*_I(k,l);
            for (int p=0; p<3; p++)
                for (int q=0; q<3; q++)
                    DcorKirDEe(i,j,k,l) += Ge_Tensor(i,j,p,q)*_Idev(p,q,k,l);
        }
 if (Ge_TrEeTensor!= NULL){
    DcorKirDEe += (*Ge_TrEeTensor);
 }

 if (rotationStiffness!= NULL){
    DcorKirDEe += (*rotationStiffness);
 }

  //
  // Get Dho2 -> dJdPhi, Dho2_u -> LHS of dDevPhidDgamma, dDevPhidGamma, Dho2_v -> LHS
  //

  // Get dMeDphi
  static STensor43 dMedGN, dMeDphi;
  STensorOperation::zero(dMedGN);
  STensorOperation::zero(dMeDphi);

  for (int i=0; i<3; i++)
    for (int j=0; j<3; j++)
      for (int k=0; k<3; k++)
        for (int l=0; l<3; l++)
          for (int p=0; p<3; p++)
            for (int q=0; q<3; q++)
                dMedGN(i,j,k,l) += DcorKirDEe(i,j,p,q)*(-_I4(p,q,k,l));

  for (int i=0; i<3; i++)
    for (int j=0; j<3; j++)
      for (int k=0; k<3; k++)
        for (int l=0; l<3; l++)
          for (int p=0; p<3; p++)
            for (int q=0; q<3; q++)
                dMeDphi(i,j,k,l) += dMedGN(i,j,p,q)*Gamma*(3.*_Idev(p,q,k,l) + 2.*_b/3 * 1./3 * _I(p,q)*_I(k,l));

  // Get dXdPhi
  static STensor43 dXdPhi;
  STensorOperation::zero(dXdPhi);
  for (int i=0; i<3; i++)
    for (int j=0; j<3; j++)
      for (int k=0; k<3; k++)
        for (int l=0; l<3; l++)
            dXdPhi(i,j,k,l) = pow(kk,1)*Hb*Gamma*(3./Cxdev*_Idev(i,j,k,l) + 2.*_b/(3.*Cxtr) * 1./3 * _I(i,j)*_I(k,l)); // DEBUG pow(kk,2)

  if (rotationStiffness!= NULL){
	  for (int i=0; i<3; i++)
	    for (int j=0; j<3; j++)
	      for (int k=0; k<3; k++)
	        for (int l=0; l<3; l++)
		      for (int p=0; p<3; p++)
		        for (int q=0; q<3; q++)
		        	dXdPhi(i,j,k,l) += ( 3/Cxdev* (*backStressRotationStiffness)(i,j,p,q) * _Idev(p,q,k,l));
  }

  // Get Dho2, Dho2inv
  static STensor43 Dho2;
  Dho2 = _I4;
  Dho2 -= dMeDphi;
  Dho2 += dXdPhi;
  STensorOperation::inverseSTensor43(Dho2,Dho2inv);

  // Get Dho2_u, Dho2_u_inv
  static STensor43 Dho2_u;

  Dho2_u = _I4;
  for (int i=0; i<3; i++)
    for (int j=0; j<3; j++)
      for (int k=0; k<3; k++)
        for (int l=0; l<3; l++){
          Dho2_u(i,j,k,l) += 3*Gamma*pow(kk,1)*Hb/Cxdev * _I4(i,j,k,l); // DEBUG pow(kk,2)
          for (int p=0; p<3; p++)
            for (int q=0; q<3; q++){
                Dho2_u(i,j,k,l) += 3.*Gamma*(Ge_Tensor(i,j,p,q)*_Idev(p,q,k,l)); // Ge_Tensor(i,j,p,q)*_I4(p,q,k,l) - CHECKERS
        }
      }
  if (Ge_TrEeTensor!= NULL){
    static STensor43 Ge_TrEeTensor_temp;
    STensorOperation::zero(Ge_TrEeTensor_temp);
    Ge_TrEeTensor_temp = (*Ge_TrEeTensor);
    Ge_TrEeTensor_temp *= (3*Gamma);
    Dho2_u += Ge_TrEeTensor_temp;
  }
  if (rotationStiffness!= NULL){
	static STensor43 rotationStiffness_Temp, backStressRotationStiffness_Temp;

	STensorOperation::zero(rotationStiffness_Temp);
	rotationStiffness_Temp = (*rotationStiffness);
	rotationStiffness_Temp *= (3*Gamma);
	Dho2_u += rotationStiffness_Temp;

	/*backStressRotationStiffness_Temp = *backStressRotationStiffness;
	backStressRotationStiffness_Temp *= (3/Cxdev);
	Dho2_u += backStressRotationStiffness_Temp;*/
	for (int i=0; i<3; i++)
	  for (int j=0; j<3; j++)
	    for (int k=0; k<3; k++)
	      for (int l=0; l<3; l++)
	        for (int p=0; p<3; p++)
	          for (int q=0; q<3; q++)
	            Dho2_u(i,j,k,l) += 3./Cxdev*((*backStressRotationStiffness)(i,j,p,q)*_I4(p,q,k,l));
  }
  STensorOperation::inverseSTensor43(Dho2_u,Dho2_u_inv);

  // Get Dho2_v, Dho2_v_inv
  double Dho2_v = 1. + 2.*_b*Gamma*(Ke + pow(kk,1)*Hb/(3.*Cxtr)); // pow(kk,2) DEBUG
  Dho2_v_inv = 1./Dho2_v;
}

void mlawNonLinearTVENonLinearTVP::getIterated_DPhi(const STensor3& F, const double& T0, const double& T, const IPNonLinearTVP *q0, IPNonLinearTVP *q1,
                                            double& Gamma, const double& Cxtr, const double& Cxdev,
                                            const STensor3& Cepr, const STensor3& Eepr,
                                            const double& trXn, const STensor3& devXn,
                                            double& Ke, double& Ge, STensor43& Ge_Tensor,
                                            double& ptilde, STensor3& devPhi,
                                            STensor3& Phi, STensor3& N, STensor3& expGN, STensor43& dexpAdA,
                                            STensor43& Dho, STensor43& Dho2inv, STensor43& Dho2_u_inv , double& Dho2_v_inv, STensor3& Fp1,
                                            STensor43* Ge_TrEeTensor, STensor43* rotationStiffness, STensor3* devXns, STensor43* backStressRotationStiffness) const{

  // This function calculates Phi iteratively.
    // At every iteration it will calculate Ee, corKir and N

  // EIGEN NEW - Initialise Fe
  static STensor3 Fpinv;
  STensor3& Fe = q1->_Fe;
  STensorOperation::multSTensor3(expGN,q0->_Fp,Fp1);
  STensorOperation::inverseSTensor3(Fp1,Fpinv);
  STensorOperation::multSTensor3(F,Fpinv,Fe);
  // EIGEN NEW - Initialise Fe

  // Initialise GammaN in IP -> for mechSrc later
  STensor3& GammaN = q1->getRefToGammaN();

  // Initialise Hinv
  static STensor3 Hinv, Ceinvpr;
  STensorOperation::inverseSTensor3(expGN,Hinv);
  STensorOperation::inverseSTensor3(Cepr,Ceinvpr);

  // Initialise Ce, Ceinv
  static STensor3 Ce, Ceinv; // Temporary Local Variable
  for (int i=0; i<3; i++)
    for (int j=0; j<3; j++){
      Ce(i,j) = 0.;
      Ceinv(i,j) = 0.;
      for (int k=0; k<3; k++)
        for (int l=0; l<3; l++){
          Ce(i,j) += Hinv(i,k)*Hinv(k,l)*Cepr(l,j);
          Ceinv(i,j) += Ceinvpr(i,k)*expGN(k,l)*expGN(l,j);
     }
   }

  // Initialise Ee
  static STensor3 Ee;
  for (int i=0; i<3; i++)
    for (int j=0; j<3; j++)
      Ee(i,j) = Eepr(i,j) - Gamma*N(i,j);

  q1->_detEe = STensorOperation::determinantSTensor3(Ee);
  /*
  bool ok=STensorOperation::logSTensor3(Ce,_order,Ee);
  if(!ok)
  {
    Phi(0,0) = Phi(1,1) = Phi(2,2) = sqrt(-1.);
    return;
  }
  Ee *= 0.5;*/

  // Initialise corKir
  static STensor3 KS;
  static STensor43 Bd_stiffnessTerm;

  // NEW EIGEN
  static STensor3 Ee0_rot;
  if (_rotationCorrectionScheme == 0 && _rotationCorrectionScheme == 1){
	  for(int i=0; i<3; i++)
		  for(int j=0; j<3; j++){
			  Ee0_rot(i,j) = 0.;
			  for(int k=0; k<3; k++)
				  for(int l=0; l<3; l++)
					  Ee0_rot(i,j) += q1->_R(i,k)*q0->_Ee(k,l)*q1->_R(j,l);
		  }
  }
  else if(_rotationCorrectionScheme == 2){
	  Ee0_rot = q0->_Ee;
	  // mlawNonLinearTVM::getEe0s(Ce,Ee,q0->_Ee,Ee0_rot,dEe0sdEe); // Dont do it here
  }
  // NEW EIGEN

  if (Ge_TrEeTensor == NULL){
	  if (!_useRotationCorrection){
		  mlawNonLinearTVM::ThermoViscoElasticPredictor(Ee,q0->_Ee,q0,q1,Ke,Ge,T0,T,true,Bd_stiffnessTerm);
	  }
	  else{
		  // mlawNonLinearTVM::ThermoViscoElasticPredictor(Ee,q0->_Ee,q0,q1,Ke,Ge,T0,T,true,Bd_stiffnessTerm);
		  mlawNonLinearTVM::ThermoViscoElasticPredictor_forTVP(Ce,Ee,Ee0_rot,q0,q1,Ke,Ge,T0,T,true,Bd_stiffnessTerm,NULL,rotationStiffness); // NEW EIGEN
	  }
  }
  else{ // TC asymmetry
	  if (!_useRotationCorrection){
		  mlawNonLinearTVM::ThermoViscoElasticPredictor(Ee,q0->_Ee,q0,q1,Ke,Ge,T0,T,true,Bd_stiffnessTerm,Ge_TrEeTensor);
	  }
	  else{
		  // mlawNonLinearTVM::ThermoViscoElasticPredictor(Ee,q0->_Ee,q0,q1,Ke,Ge,T0,T,true,Bd_stiffnessTerm,Ge_TrEeTensor);
		  mlawNonLinearTVM::ThermoViscoElasticPredictor_forTVP(Ce,Ee,Ee0_rot,q0,q1,Ke,Ge,T0,T,true,Bd_stiffnessTerm,Ge_TrEeTensor,rotationStiffness); // NEW EIGEN
	  }
  }

  Ge_Tensor = _I4;
  Ge_Tensor *= Ge*2; // *2 because the function doesnt do it
  Ge_Tensor += Bd_stiffnessTerm;
  KS = q1->_kirchhoff;

  // Get Hb
  double Hb(0.);
  if (q1->_ipKinematic != NULL)
        Hb = q1->_ipKinematic->getR(); // kinematic hardening parameter
  double kk = (1./sqrt(1.+2.*q1->_nup*q1->_nup));

  // Initialise backStress
  static STensor3 devX, devN;
  double trX, trN;
  STensorOperation::decomposeDevTr(N,devN,trN);
  devX = pow(kk,1)*Hb*Gamma*devN + devXn;      // pow(kk,2) DEBUG

  if (_useRotationCorrection){
	  devX -= devXn;
	  STensorOperation::zero(*devXns);
	  STensorOperation::zero(*backStressRotationStiffness);
	  STensorOperation::alignEigenDecomposition_NormBased_withDerivative(devN,devXn,*devXns,*backStressRotationStiffness);
	  devX += (*devXns);

  	static STensor43 check;
  	STensorOperation::zero(check);
      for (int k=0; k<3; k++)
          for (int l=0; l<3; l++)
              for (int p=0; p<3; p++)
                  for (int q=0; q<3; q++)
                      for (int r=0; r<3; r++)
                        for (int s=0; s<3; s++)
                      	  check(k,l,p,q) += (*backStressRotationStiffness)(k,l,r,s)*_I(r,s)*_I(p,q);

      double checkers = 0;
  }

  devX *= 1./Cxdev;
  trX = (pow(kk,1)*Hb*Gamma*trN + trXn)*1./Cxtr; // pow(kk,2) DEBUG
  q1->_backsig = devX;
  q1->_backsig(0,0) += trX/3.;
  q1->_backsig(1,1) += trX/3.;
  q1->_backsig(2,2) += trX/3.;

  // Initialise J
  static STensor3 J;
  STensorOperation::zero(J);
  for (int i=0; i<3; i++)
    for (int j=0; j<3; j++)
      J(i,j) += Phi(i,j) - KS(i,j) + q1->_backsig(i,j);

  // Initialise J_tol
  double J_tol = 0.;
  J_tol = J.norm0();
  J_tol/=(_K+_G);
  // for (int i=0; i<3; i++)
    // for (int j=0; j<3; j++)
      // J_tol += abs(J(i,j)) ;

  // Initialise Dho, Dho2, Dho2inv
  if (Ge_TrEeTensor == NULL){
    getDho(Gamma,Cepr,Ceinvpr,KS,Ke,Ge_Tensor,kk,Hb,Cxtr,Cxdev,expGN,dexpAdA,Dho,Dho2inv,Dho2_u_inv,Dho2_v_inv,NULL,rotationStiffness,backStressRotationStiffness);
  }
  else{ // TC asymmetry
    getDho(Gamma,Cepr,Ceinvpr,KS,Ke,Ge_Tensor,kk,Hb,Cxtr,Cxdev,expGN,dexpAdA,Dho,Dho2inv,Dho2_u_inv,Dho2_v_inv,Ge_TrEeTensor,rotationStiffness,backStressRotationStiffness);
  }

  // Initialise DPhi
  static STensor3 DPhi;

  int ite = 0;
  int maxite = 100; // 5000;

  double tol = _stressIteratorTol; // 1e-6;

  while (fabs(J_tol) > tol or ite <1){

     // update DPhi
     for (int i=0; i<3; i++)
       for (int j=0; j<3; j++){
         DPhi(i,j) = 0.;
         for (int k=0; k<3; k++)
           for (int l=0; l<3; l++){
             DPhi(i,j) -= Dho2inv(i,j,k,l)*J(k,l);
           }
         }

      // update devPhi
      for (int i=0; i<3; i++){
        for (int j=0; j<3; j++){
       		Phi(i,j) += DPhi(i,j);
       	}
      }

      // update devPhi, ptilde
      STensorOperation::decomposeDevTr(Phi,devPhi,ptilde);
      ptilde = Phi.trace()/3;

      // update N
      STensorOperation::zero(N);
      for (int i=0; i<3; i++)
        for (int j=0; j<3; j++)
          N(i,j) = 3.*devPhi(i,j) + 2.*_b/3.*ptilde*_I(i,j);

      // update H = exp(GN) = A in dexpAdA; Hinv
      GammaN = N;
      GammaN *= Gamma;
      STensorOperation::expSTensor3(GammaN,_order,expGN,&dexpAdA);
      STensorOperation::inverseSTensor3(expGN,Hinv);

      // update Ce, Ceinv
      for (int i=0; i<3; i++)
        for (int j=0; j<3; j++){
          Ce(i,j) = 0.;
          Ceinv(i,j) = 0.;
          for (int k=0; k<3; k++)
            for (int l=0; l<3; l++){
              Ce(i,j) += Hinv(i,k)*Hinv(k,l)*Cepr(l,j);
              Ceinv(i,j) += Ceinvpr(i,k)*expGN(k,l)*expGN(l,j);
          }
        }

      // update Ee
      STensorOperation::zero(Ee);
      for (int i=0; i<3; i++)
        for (int j=0; j<3; j++)
          Ee(i,j) = Eepr(i,j) - Gamma*N(i,j);

      q1->_detEe = STensorOperation::determinantSTensor3(Ee);

      // EIGEN NEW - update Fe
      STensorOperation::multSTensor3(expGN,q0->_Fp,Fp1);
      STensorOperation::inverseSTensor3(Fp1,Fpinv);
      STensorOperation::multSTensor3(F,Fpinv,Fe);
      // NEW EIGEN

      // update corKir
      if (Ge_TrEeTensor == NULL){
    	  if (!_useRotationCorrection){
    		  mlawNonLinearTVM::ThermoViscoElasticPredictor(Ee,q0->_Ee,q0,q1,Ke,Ge,T0,T,true,Bd_stiffnessTerm);
    	  }
    	  else{
    		  mlawNonLinearTVM::ThermoViscoElasticPredictor_forTVP(Ce,Ee,Ee0_rot,q0,q1,Ke,Ge,T0,T,true,Bd_stiffnessTerm,NULL,rotationStiffness); // NEW EIGEN
    	  }
      }
      else{ // TC asymmetry
    	  if (!_useRotationCorrection){
    		  mlawNonLinearTVM::ThermoViscoElasticPredictor(Ee,q0->_Ee,q0,q1,Ke,Ge,T0,T,true,Bd_stiffnessTerm,Ge_TrEeTensor);
    	  }
    	  else{
    		  mlawNonLinearTVM::ThermoViscoElasticPredictor_forTVP(Ce,Ee,Ee0_rot,q0,q1,Ke,Ge,T0,T,true,Bd_stiffnessTerm,Ge_TrEeTensor,rotationStiffness); // NEW EIGEN
    	  }
      }

      Ge_Tensor = _I4;
      Ge_Tensor *= Ge*2; // *2 because the function doesnt do it
      Ge_Tensor += Bd_stiffnessTerm;
      KS = q1->_kirchhoff;

      // update backStress
      STensorOperation::decomposeDevTr(N,devN,trN);
      devX = pow(kk,1)*Hb*Gamma*devN + devXn;           // pow(kk,2) DEBUG
      if (_useRotationCorrection){
    	  devX -= devXn;
    	  STensorOperation::zero(*devXns);
    	  STensorOperation::zero(*backStressRotationStiffness);
    	  STensorOperation::alignEigenDecomposition_NormBased_withDerivative(devN,devXn,*devXns,*backStressRotationStiffness);
    	  devX += (*devXns);

    	  	static STensor43 check;
    	  	STensorOperation::zero(check);
    	      for (int k=0; k<3; k++)
    	          for (int l=0; l<3; l++)
    	              for (int p=0; p<3; p++)
    	                  for (int q=0; q<3; q++)
    	                      for (int r=0; r<3; r++)
    	                        for (int s=0; s<3; s++)
    	                      	  check(k,l,p,q) += (*backStressRotationStiffness)(k,l,r,s)*_I(r,s)*_I(p,q);

    	      double checkers = 0;
      }
      devX *= 1./Cxdev;
      trX = (pow(kk,1)*Hb*Gamma*trN + trXn)*1./Cxtr;    // pow(kk,2) DEBUG
      q1->_backsig = devX;
      q1->_backsig(0,0) += trX/3.;
      q1->_backsig(1,1) += trX/3.;
      q1->_backsig(2,2) += trX/3.;

      // update J
      STensorOperation::zero(J);
      for (int i=0; i<3; i++)
        for (int j=0; j<3; j++)
          J(i,j) += ( Phi(i,j) - KS(i,j) + q1->_backsig(i,j) );

      // update Dho2, Dho2inv
      if (Ge_TrEeTensor == NULL){
        getDho(Gamma,Cepr,Ceinvpr,KS,Ke,Ge_Tensor,kk,Hb,Cxtr,Cxdev,expGN,dexpAdA,Dho,Dho2inv,Dho2_u_inv,Dho2_v_inv,NULL,rotationStiffness,backStressRotationStiffness);
      }
      else{ // TC asymmetry
        getDho(Gamma,Cepr,Ceinvpr,KS,Ke,Ge_Tensor,kk,Hb,Cxtr,Cxdev,expGN,dexpAdA,Dho,Dho2inv,Dho2_u_inv,Dho2_v_inv,Ge_TrEeTensor,rotationStiffness,backStressRotationStiffness);
      }

      J_tol = J.norm0();
      J_tol/=(_K+_G);
      // J_tol/=(1.e+10);

      ite++;

      // if (ite> maxite-5)
      // Msg::Info("ite = %d, tol %e, J_tol = %e, Gamma = %e",ite,tol,J_tol,Gamma);

      if (fabs(J_tol) <_tol) break;

      if(ite > maxite){
    	  Msg::Error("No convergence for iterated Phi mlawNonLinearTVENonLinearTVP nonAssociatedFlow iter = %d, J_tol = %e!!",ite,J_tol);
    	  // Msg::Error("No convergence for iterated Phi mlawNonLinearTVENonLinearTVP nonAssociatedFlow iter = %d, e1 = %e, e2 = %e, e3 = %e !!",ite,e1,e2,e3);
    	  // Msg::Error("Exiting the function Iterated Phi!!");
    	  // Phi(0,0) = Phi(1,1) = Phi(2,2) = sqrt(-1.);
    	  // Gamma *= 0.1;
       return;
       }
     } // while

}
