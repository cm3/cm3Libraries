//
// C++ Interface: material law
//
// Description: j2 elasto-plastic law with non local damage interface
//
//
// Author:  <L. Noels>, (C) 2013
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "mlawNonLocalDamageJ2Hyper.h"
#include <math.h>
#include "MInterfaceElement.h"
#include "nonLinearMechSolver.h"

mlawNonLocalDamageJ2Hyper::mlawNonLocalDamageJ2Hyper(const int num,const double E,const double nu,
                           const double rho, const J2IsotropicHardening &_j2IH,
                           const CLengthLaw &_clLaw, const DamageLaw &_damLaw,
			   const double tol, const bool pert, const double eps)
				: mlawJ2linear(num,E,nu,rho,_j2IH,tol,pert,eps)
{
  cLLaw = _clLaw.clone();
  damLaw = _damLaw.clone();
    Cel=0.;
    Cel(0,0,0,0) = _lambda + _mu2;
    Cel(1,1,0,0) = _lambda;
    Cel(2,2,0,0) = _lambda;
    Cel(0,0,1,1) = _lambda;
    Cel(1,1,1,1) = _lambda + _mu2;
    Cel(2,2,1,1) = _lambda;
    Cel(0,0,2,2) = _lambda;
    Cel(1,1,2,2) = _lambda;
    Cel(2,2,2,2) = _lambda + _mu2;

    Cel(1,0,1,0) = _mu;
    Cel(2,0,2,0) = _mu;
    Cel(0,1,0,1) = _mu;
    Cel(2,1,2,1) = _mu;
    Cel(0,2,0,2) = _mu;
    Cel(1,2,1,2) = _mu;

    Cel(0,1,1,0) = _mu;
    Cel(0,2,2,0) = _mu;
    Cel(1,0,0,1) = _mu;
    Cel(1,2,2,1) = _mu;
    Cel(2,0,0,2) = _mu;
    Cel(2,1,1,2) = _mu;

}
mlawNonLocalDamageJ2Hyper::mlawNonLocalDamageJ2Hyper(const mlawNonLocalDamageJ2Hyper &source) : mlawJ2linear(source)
{
  if(source.cLLaw != NULL)
  {
    cLLaw=source.cLLaw->clone();
  }
  if(source.damLaw != NULL)
  {
    damLaw=source.damLaw->clone();
  }
  Cel = source.Cel;
}

mlawNonLocalDamageJ2Hyper& mlawNonLocalDamageJ2Hyper::operator = (const materialLaw &source)
{
  mlawJ2linear::operator=(source);
  const mlawNonLocalDamageJ2Hyper* src = dynamic_cast<const mlawNonLocalDamageJ2Hyper*>(&source);
  if(src != NULL)
  {
    if(cLLaw != NULL) delete cLLaw;
    if(src->cLLaw != NULL)
    {
      cLLaw=src->cLLaw->clone();
    }
    if(damLaw != NULL) delete damLaw;
    if(src->damLaw != NULL)
    {
      damLaw=src->damLaw->clone();
    }
    Cel = src->Cel;
  }
  return *this;
}

void mlawNonLocalDamageJ2Hyper::createIPState(IPStateBase* &ips, bool hasBodyForce, const bool* state_,const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
  //bool inter=true;
  //const MInterfaceElement *iele = static_cast<const MInterfaceElement*>(ele);
  //if(iele==NULL) inter=false;
  IPVariable* ipvi = new IPNonLocalDamageJ2Hyper(_j2IH,cLLaw,damLaw);
  IPVariable* ipv1 = new IPNonLocalDamageJ2Hyper(_j2IH,cLLaw,damLaw);
  IPVariable* ipv2 = new IPNonLocalDamageJ2Hyper(_j2IH,cLLaw,damLaw);
  if(ips != NULL) delete ips;
  ips = new IP3State(state_,ipvi,ipv1,ipv2);
}
void mlawNonLocalDamageJ2Hyper::createIPState(IPNonLocalDamageJ2Hyper *ivi, IPNonLocalDamageJ2Hyper *iv1, IPNonLocalDamageJ2Hyper *iv2) const
{

}
void mlawNonLocalDamageJ2Hyper::createIPVariable(IPNonLocalDamageJ2Hyper *&ipv) const
{

}

double mlawNonLocalDamageJ2Hyper::soundSpeed() const
{
  return mlawJ2linear::soundSpeed();
}


void mlawNonLocalDamageJ2Hyper::constitutive(const STensor3& F0,
                                             const STensor3& Fn,
                                             STensor3 &P,
                                             const IPNonLocalDamageJ2Hyper *q0,
	                                     IPNonLocalDamageJ2Hyper *q1,
                                             STensor43 &Tangent,
                                             const bool stiff, 
                                             const bool dTangentdeps,
                                             STensor63* dCalgdeps) const
{
  mlawJ2linear::constitutive(F0,Fn,P,q0,q1,Tangent,stiff);
}

void mlawNonLocalDamageJ2Hyper::constitutive(
                            const STensor3& F0,         // initial deformation gradient (input @ time n)
                            const STensor3& Fn,         // updated deformation gradient (input @ time n+1)
                            STensor3 &P,                // updated 1st Piola-Kirchhoff stress tensor (output)
                                                        // contains the initial values on input
                            const IPNonLocalDamageJ2Hyper *ipvprev,       // array of initial internal variable
                            IPNonLocalDamageJ2Hyper *ipvcur,             // updated array of internal variable (in ipvcur on output),
                            STensor43 &Tangent,         // constitutive tangents (output)
                            STensor3  &dLocalPlasticStrainDStrain,
                            STensor3  &dStressDNonLocalPlasticStrain,
                            double   &dLocalPlasticStrainDNonLocalPlasticStrain,
                            const bool stiff,            // if true compute the tangents
                            STensor43* elasticTangent
                           ) const
{
  double p0 = ipvprev->getConstRefToEquivalentPlasticStrain();
  cLLaw->computeCL(p0, ipvcur->getRefToIPCLength());

  static STensor43 dFpdF, dFedF;
  static STensor3 Fe, Peff;
  static STensor3 Fpinv;
  static STensor3 dpdF;

  mlawJ2linear::predictorCorector(F0,Fn,Peff,ipvprev,ipvcur,Tangent,dFpdF,dFedF,dpdF,stiff,elasticTangent);

  const STensor3 &Fp  = ipvcur->getConstRefToFp();
  STensorOperation::inverseSTensor3(Fp,Fpinv);
  STensorOperation::multSTensor3(Fn,Fpinv,Fe);

  double ene = ipvcur->defoEnergy();

  if (ipvcur->dissipationIsBlocked()){
    // damage stop increasing
    IPDamage& curDama = ipvcur->getRefToIPDamage();
    curDama.getRefToDamage() = ipvprev->getDamage();
    curDama.getRefToDDamageDp() = 0.;
    STensorOperation::zero(curDama.getRefToDDamageDFe());
    curDama.getRefToDeltaDamage() = 0;
    curDama.getRefToMaximalP() = ipvprev->getMaximalP();
    // damage is not active
    ipvcur->getRefToDissipationActive() =  false;

  }
  else{
    // if damage is not blocked
    if (ipvcur->getNonLocalToLocal()){
      // when considering crack transition
      // increment of non-local plastic strain by true plastic strain
      // in order to obtain damage continuously developed
      ipvcur->getRefToEffectivePlasticStrain() = ipvprev->getEffectivePlasticStrain();
      ipvcur->getRefToEffectivePlasticStrain() += (ipvcur->getConstRefToEquivalentPlasticStrain() - ipvprev->getConstRefToEquivalentPlasticStrain());
      // computed damage with current effective plastic strain
      damLaw->computeDamage(ipvcur->getEffectivePlasticStrain(),
                        ipvprev->getEffectivePlasticStrain(),
                        ene, Fe, Fp, Peff, Cel,
                        ipvprev->getConstRefToIPDamage(),ipvcur->getRefToIPDamage());
     //Msg::Info("continue damage in interface = %e",ipvcur->getDamage());


    }
    else{
      //
      // compute damage for ipcur
      damLaw->computeDamage(ipvcur->getEffectivePlasticStrain(),
                        ipvprev->getEffectivePlasticStrain(),
                        ene, Fe, Fp, Peff, Cel,
                        ipvprev->getConstRefToIPDamage(),ipvcur->getRefToIPDamage());

    }

    if (ipvcur->getDamage() > ipvprev->getDamage() and ipvcur->getConstRefToEquivalentPlasticStrain() > ipvprev->getConstRefToEquivalentPlasticStrain()){
      ipvcur->getRefToDissipationActive() = true;
    }
    else{
      ipvcur->getRefToDissipationActive() = false;
    }
  }

  double D = ipvcur->getDamage();
  double D0 = ipvprev->getDamage();
  // check active damage

  // get true PK1 stress from damage and effective PK1 stress
	P = Peff;
  P*=(1.-D);

  ipvcur->getRefToElasticEnergy()*= (1.-D);
  ipvcur->getRefToPlasticPower() *= (1.-D);

  double dPlasticDisp = ipvcur->plasticEnergy() - ipvprev->plasticEnergy();
  ipvcur->getRefToPlasticEnergy() = ipvprev->plasticEnergy() + (1-D)*dPlasticDisp;
  ipvcur->getRefToDamageEnergy() =  ipvprev->damageEnergy() + ene*(D-D0);

  // irreversible energy
  double& irrEneg = ipvcur->getRefToIrreversibleEnergy();
  if (this->getMacroSolver()->getPathFollowingLocalIncrementType() == pathFollowingManager::DEFO_ENERGY){
    irrEneg = ipvcur->defoEnergy();
  }
  else if (this->getMacroSolver()->getPathFollowingLocalIncrementType() == pathFollowingManager::PLASTIC_ENERGY){
    irrEneg = ipvcur->plasticEnergy();
  }
  else if (this->getMacroSolver()->getPathFollowingLocalIncrementType() == pathFollowingManager::DAMAGE_ENERGY) {
    irrEneg = ipvcur->damageEnergy();
  }
  else if (this->getMacroSolver()->getPathFollowingLocalIncrementType() == pathFollowingManager::DISSIPATION_ENERGY){
    irrEneg = ipvcur->plasticEnergy()+ ipvcur->damageEnergy();
  }
  else{
    irrEneg = 0.;
  }


  if(stiff)
  {
    static STensor3 DDamageDF;

    for (int i=0; i<3; i++){
      for (int j=0; j<3; j++){
        DDamageDF(i,j) = 0.;
        if (ipvcur->getNonLocalToLocal()){
          DDamageDF(i,j) +=ipvcur->getDDamageDp()*dpdF(i,j);
        };
        for (int k=0; k<3; k++){
          for (int l=0; l<3; l++){
            DDamageDF(i,j) += ipvcur->getConstRefToDDamageDFe()(k,l)*dFedF(k,l,i,j);
          }
        }
      }
    }

    if (elasticTangent != NULL){
      // update with damage
      (*elasticTangent) *= (1.-D);
    }

    // we need to correct partial P/partial F: (1-D) partial P/partial F - Peff otimes partial D partial F
    Tangent*=(1.-D);
    for(int i=0;i<3;i++){
      for(int j=0;j<3;j++){
        for(int k=0;k<3;k++){
          for(int l=0;l<3;l++){
            Tangent(i,j,k,l)-=Peff(i,j)*DDamageDF(k,l);
          }
        }
      }
    }

    // partial p/partial F
    dLocalPlasticStrainDStrain = dpdF;

    // -hat{P} partial D/partial tilde p
    if (ipvcur->getNonLocalToLocal()){
      STensorOperation::zero(dStressDNonLocalPlasticStrain);
    }
    else{
      dStressDNonLocalPlasticStrain = Peff*(-1*ipvcur->getDDamageDp());
    }

    // partial p partial tilde p (0 if no MFH)
    dLocalPlasticStrainDNonLocalPlasticStrain = 0.;

    STensor3& DplasticPowerDF = ipvcur->getRefToDPlasticPowerDF();
    DplasticPowerDF *= (1-D);
    DplasticPowerDF.daxpy(DDamageDF,-ipvcur->getConstRefToPlasticPower()/(1.-D));

    // irreversible energy
    STensor3& DdissEnergDF = ipvcur->getRefToDIrreversibleEnergyDF();
    double& DdissEnergDNonlocalVar = ipvcur->getRefToDIrreversibleEnergyDNonLocalVariable();

    if (this->getMacroSolver()->getPathFollowingLocalIncrementType() == pathFollowingManager::DEFO_ENERGY){
      DdissEnergDF *= (1-D);
      DdissEnergDF.daxpy(DDamageDF,-ene);
      if (ipvcur->getNonLocalToLocal()){
        DdissEnergDF.daxpy(dpdF,-ipvcur->getDDamageDp()*ene);
        DdissEnergDNonlocalVar = 0.;
      }
      else{
        DdissEnergDNonlocalVar = -ipvcur->getDDamageDp()*ene;
      }
    }
    else if (this->getMacroSolver()->getPathFollowingLocalIncrementType() == pathFollowingManager::PLASTIC_ENERGY){
      DdissEnergDF *= (1-D);
      DdissEnergDF.daxpy(DDamageDF,-dPlasticDisp);

      if (ipvcur->getNonLocalToLocal()){
        DdissEnergDF.daxpy(dpdF,-ipvcur->getDDamageDp()*dPlasticDisp);
        DdissEnergDNonlocalVar = 0.;
      }
      else{
        DdissEnergDNonlocalVar = -ipvcur->getDDamageDp()*dPlasticDisp;
      }
    }
    else if (this->getMacroSolver()->getPathFollowingLocalIncrementType() == pathFollowingManager::DAMAGE_ENERGY) {
      for(int i=0;i<3;i++){
        for(int j=0;j<3;j++){
          DdissEnergDF(i,j) = 0.;
          for(int k=0;k<3;k++){
            for(int l=0;l<3;l++){
              for (int m=0; m<3; m++){
                DdissEnergDF(i,j) += Peff(k,m)*Fp(l,m)*dFedF(k,l,i,j)*(D-D0);
              }
            }
          }
        }
      }
      DdissEnergDF.daxpy(DDamageDF,ene);
      if (ipvcur->getNonLocalToLocal()){
        DdissEnergDF.daxpy(dpdF, ipvcur->getDDamageDp()*ene);
        DdissEnergDNonlocalVar = 0.;
      }
      else{
        DdissEnergDNonlocalVar = ipvcur->getDDamageDp()*ene;
      }

    }
    else if (this->getMacroSolver()->getPathFollowingLocalIncrementType() == pathFollowingManager::DISSIPATION_ENERGY){
      DdissEnergDF *= (1-D);
      for(int i=0;i<3;i++){
        for(int j=0;j<3;j++){
          for(int k=0;k<3;k++){
            for(int l=0;l<3;l++){
              for (int m=0; m<3; m++){
                DdissEnergDF(i,j) += Peff(k,m)*Fp(l,m)*dFedF(k,l,i,j)*(D-D0);
              }
            }
          }
        }
      }

      double DdissDD = ene - dPlasticDisp;
      DdissEnergDF.daxpy(DDamageDF,DdissDD);
      if (ipvcur->getNonLocalToLocal()){
        DdissEnergDF.daxpy(dpdF, ipvcur->getDDamageDp()*DdissDD);
        DdissEnergDNonlocalVar = 0.;
      }
      else{
        DdissEnergDNonlocalVar = ipvcur->getDDamageDp()*DdissDD;
      }
    }
    else{
      STensorOperation::zero(DdissEnergDF);
      STensorOperation::zero(DdissEnergDNonlocalVar);
    }

  }
}

