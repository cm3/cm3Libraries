//
// C++ Interface: material law
//
// Description: mlawNonLocalDamageQuadYieldHyper
//
// Author:  V.D. Nguyen, (C) 2014
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef MLAWNONLOCALDAMAGEHYPERELASTIC_H_
#define MLAWNONLOCALDAMAGEHYPERELASTIC_H_

#include "mlawHyperelastic.h"
#include "ipNonLocalDamageHyperelastic.h"
#include "j2IsotropicHardening.h"
#include "CLengthLaw.h"
#include "DamageLaw.h"

class mlawNonLocalDamagePowerYieldHyper : public mlawPowerYieldHyper{
  protected:
    CLengthLaw *cLLaw;
    DamageLaw  *damLaw;

  public:
    mlawNonLocalDamagePowerYieldHyper(const int num,const double E,const double nu, const double rho,
          const double tol=1.e-6, const bool matrixbyPerturbation = false, const double pert = 1e-8);
    void setCLengthLaw(const CLengthLaw &_cLLaw);
    void setDamageLaw(const DamageLaw &_damLaw);

   #ifndef SWIG
    mlawNonLocalDamagePowerYieldHyper(const mlawNonLocalDamagePowerYieldHyper &source);
    virtual ~mlawNonLocalDamagePowerYieldHyper();

		virtual materialLaw* clone() const { return new mlawNonLocalDamagePowerYieldHyper(*this);}

    // function of materialLaw
    virtual matname getType() const{return materialLaw::nonLocalDamagePowerYieldHyper;}
    virtual void createIPState(IPHyperViscoElastoPlasticNonLocalDamage *ivi, IPHyperViscoElastoPlasticNonLocalDamage *iv1, IPHyperViscoElastoPlasticNonLocalDamage *iv2) const;
    virtual void createIPState(IPStateBase* &ips,bool hasBodyForce,const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const IntPt *GP=NULL, const int gpt = 0) const;
    virtual void createIPVariable(IPHyperViscoElastoPlasticNonLocalDamage *&ipv,bool hasBodyForce, const MElement *ele,const int nbFF,const IntPt *GP, const int gpt) const;

    virtual void initLaws(const std::map<int,materialLaw*> &maplaw){}; // this law is initialized so nothing to do
    virtual double soundSpeed() const; // default but you can redefine it for your case
    virtual const CLengthLaw *getCLengthLaw() const {return cLLaw; };
    virtual const DamageLaw *getDamageLaw() const {return damLaw; };
    // specific function
   public:
    virtual void constitutive(
                              const STensor3& F0,         // initial deformation gradient (input @ time n)
                              const STensor3& Fn,         // updated deformation gradient (input @ time n+1)
                              STensor3 &P,                // updated 1st Piola-Kirchhoff stress tensor (output)
                                                          // contains the initial values on input
                              const IPHyperViscoElastoPlasticNonLocalDamage *q0,       // array of initial internal variable
                              IPHyperViscoElastoPlasticNonLocalDamage *q1,             // updated array of internal variable (in ipvcur on output),
                              STensor43 &Tangent,         // constitutive tangents (output)
                              const bool stiff, 
                              const bool dTangentdeps=false,
                              STensor63* dCalgdeps = NULL 
                             ) const;

    virtual void constitutive(
                              const STensor3& F0,         // initial deformation gradient (input @ time n)
                              const STensor3& Fn,         // updated deformation gradient (input @ time n+1)
                              STensor3 &P,                // updated 1st Piola-Kirchhoff stress tensor (output)
                                                          // contains the initial values on input
                              const IPHyperViscoElastoPlasticNonLocalDamage *q0,       // array of initial internal variable
                              IPHyperViscoElastoPlasticNonLocalDamage *q1,             // updated array of internal variable (in ipvcur on output),
                              STensor43 &Tangent,         // constitutive tangents (output)
                              STensor3  &dLocalPlasticStrainDStrain,
                          STensor3  &dStressDNonLocalPlasticStrain,
                              double   &dLocalPlasticStrainDNonLocalPlasticStrain,
                              const bool stiff,            // if true compute the tangents
                              STensor43 *elasticTangent,
                              const bool dTangentdeps=false,
                              STensor63* dCalgdeps = NULL ) const;


#endif // SWIG
};

class mlawLocalDamagePowerYieldHyperWithFailure : public mlawPowerYieldHyperWithFailure{
  protected:
    std::vector<DamageLaw*> damLaw;

  public:
    mlawLocalDamagePowerYieldHyperWithFailure(const int num,const double E,const double nu, const double rho,
          const double tol=1.e-6, const bool matrixbyPerturbation = false, const double pert = 1e-8);

    void clearAllDamageLaw();
    void setDamageLaw(const DamageLaw &_damLaw);

   #ifndef SWIG
    mlawLocalDamagePowerYieldHyperWithFailure(const mlawLocalDamagePowerYieldHyperWithFailure &source);
    virtual ~mlawLocalDamagePowerYieldHyperWithFailure();

		virtual materialLaw* clone() const { return new mlawLocalDamagePowerYieldHyperWithFailure(*this);}

    // function of materialLaw
    virtual matname getType() const{return materialLaw::localDamagePowerYieldHyperWithFailure;}
    virtual void createIPState(IPHyperViscoElastoPlasticMultipleLocalDamage *ivi, IPHyperViscoElastoPlasticMultipleLocalDamage *iv1, IPHyperViscoElastoPlasticMultipleLocalDamage *iv2) const;
    virtual void createIPState(IPStateBase* &ips,bool hasBodyForce,const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const IntPt *GP=NULL, const int gpt = 0) const;
    virtual void createIPVariable(IPHyperViscoElastoPlasticMultipleLocalDamage *&ipv,bool hasBodyForce, const MElement *ele,const int nbFF,const IntPt *GP, const int gpt) const;

    virtual void initLaws(const std::map<int,materialLaw*> &maplaw){}; // this law is initialized so nothing to do
    virtual double soundSpeed() const; // default but you can redefine it for your case
    virtual const std::vector<DamageLaw*>& getDamageLaw() const {return damLaw; };
    // specific function
   public:

    virtual void constitutive(
                              const STensor3& F0,         // initial deformation gradient (input @ time n)
                              const STensor3& Fn,         // updated deformation gradient (input @ time n+1)
                              STensor3 &P,                // updated 1st Piola-Kirchhoff stress tensor (output)
                              const IPHyperViscoElastoPlasticMultipleLocalDamage *q0,       // array of initial internal variable
                              IPHyperViscoElastoPlasticMultipleLocalDamage *q1,             // updated array of internal variable (in ipvcur on output),
                              STensor43 &Tangent,         // constitutive tangents (output)
                              const bool stiff,            // if true compute the tangents
                              const bool dTangentdeps=false,
                              STensor63* dCalgdeps = NULL          
                             ) const;
  #endif // SWIG
};


class mlawNonLocalDamagePowerYieldHyperWithFailure : public mlawPowerYieldHyperWithFailure{
  protected:
    std::vector<CLengthLaw*> cLLaw;
    std::vector<DamageLaw*> damLaw;
    double _criticalDamage; // hardening is saturated after damage attained this value
    bool _blockHardeningAfterFailure;
    bool _blockDamageAfterFailure;

  public:
    mlawNonLocalDamagePowerYieldHyperWithFailure(const int num,const double E,const double nu, const double rho,
          const double tol=1.e-6, const bool matrixbyPerturbation = false, const double pert = 1e-8);

    void clearAllCLengthLaw();
    void clearAllDamageLaw();
    void setCLengthLaw(const CLengthLaw &_cLLaw);
    void setDamageLaw(const DamageLaw &_damLaw);
    void setCritialDamage(const double cr);


   #ifndef SWIG
    mlawNonLocalDamagePowerYieldHyperWithFailure(const mlawNonLocalDamagePowerYieldHyperWithFailure &source);
    virtual ~mlawNonLocalDamagePowerYieldHyperWithFailure();

		virtual materialLaw* clone() const { return new mlawNonLocalDamagePowerYieldHyperWithFailure(*this);}

    // function of materialLaw
    virtual matname getType() const{return materialLaw::nonLocalDamagePowerYieldHyperWithFailure;}
    virtual void createIPState(IPHyperViscoElastoPlasticMultipleNonLocalDamage *ivi, IPHyperViscoElastoPlasticMultipleNonLocalDamage *iv1, IPHyperViscoElastoPlasticMultipleNonLocalDamage *iv2) const;
    virtual void createIPState(IPStateBase* &ips, bool hasBodyForce, const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const IntPt *GP=NULL, const int gpt = 0) const;
    virtual void createIPVariable(IPHyperViscoElastoPlasticMultipleNonLocalDamage *&ipv,bool hasBodyForce, const MElement *ele,const int nbFF,const IntPt *GP, const int gpt) const;

    virtual void initLaws(const std::map<int,materialLaw*> &maplaw){}; // this law is initialized so nothing to do
    virtual double soundSpeed() const; // default but you can redefine it for your case
    virtual const std::vector<CLengthLaw*>& getCLengthLaw() const {return cLLaw; };
    virtual const std::vector<DamageLaw*>& getDamageLaw() const {return damLaw; };
    // specific function
  public:
    virtual void checkInternalState(IPVariable* ipv, const IPVariable* ipvprev) const;
    virtual void predictorCorrector(
                              const STensor3& F0,         // initial deformation gradient (input @ time n)
                              const STensor3& Fn,         // updated deformation gradient (input @ time n+1)
                              STensor3 &P,                // updated 1st Piola-Kirchhoff stress tensor (output)
                              const IPHyperViscoElastoPlasticMultipleNonLocalDamage *q0,       // array of initial internal variable
                              IPHyperViscoElastoPlasticMultipleNonLocalDamage *q1,             // updated array of internal variable (in ipvcur on output),
                              STensor43 &Tangent,         // constitutive tangents (output)
                              std::vector<STensor3>  &dLocalVariableDStrain,
                              std::vector<STensor3>  &dStressDNonLocalVariable,
                              fullMatrix<double>  &dLocalVariableDNonLocalVariable,
                              const bool stiff,            // if true compute the tangents
                              STensor43* elasticTangent) const;

    virtual void constitutive(
                              const STensor3& F0,         // initial deformation gradient (input @ time n)
                              const STensor3& Fn,         // updated deformation gradient (input @ time n+1)
                              STensor3 &P,                // updated 1st Piola-Kirchhoff stress tensor (output)
                              const IPHyperViscoElastoPlasticMultipleNonLocalDamage *q0,       // array of initial internal variable
                              IPHyperViscoElastoPlasticMultipleNonLocalDamage *q1,             // updated array of internal variable (in ipvcur on output),
                              STensor43 &Tangent,         // constitutive tangents (output)
                              const bool stiff,           // if true compute the tangents
                              const bool dTangentdeps=false,
                              STensor63* dCalgdeps = NULL 
                             ) const;

    virtual void constitutive(
                              const STensor3& F0,         // initial deformation gradient (input @ time n)
                              const STensor3& Fn,         // updated deformation gradient (input @ time n+1)
                              STensor3 &P,                // updated 1st Piola-Kirchhoff stress tensor (output)
                              const IPHyperViscoElastoPlasticMultipleNonLocalDamage *q0,       // array of initial internal variable
                              IPHyperViscoElastoPlasticMultipleNonLocalDamage *q1,             // updated array of internal variable (in ipvcur on output),
                              STensor43 &Tangent,         // constitutive tangents (output)
                              std::vector<STensor3>  &dLocalVariableDStrain,
                              std::vector<STensor3>  &dStressDNonLocalVariable,
                              fullMatrix<double>  &dLocalVariableDNonLocalVariable,
                              const bool stiff,            // if true compute the tangents
                              STensor43* elasticTangent,
                              const bool dTangentdeps=false,
                              STensor63* dCalgdeps = NULL ) const;

  #endif // SWIG
};

#endif // MLAWNONLOCALDAMAGEHYPERELASTIC_H_
