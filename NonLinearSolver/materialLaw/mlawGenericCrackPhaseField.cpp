//
// C++ Interface: material law
//
// Description: Generic Crack Phase Field
//
// Mohib Mustafa
// 03/12/24

#include "mlawGenericCrackPhaseField.h"

mlawGenericCrackPhaseField::mlawGenericCrackPhaseField(const int num, const double rho,
  const CLengthLaw &cLLaw, const double gc) : materialLaw(num, true), _rho(rho), _gc(gc){

    _matlaw = NULL;
    _cLLaw = cLLaw.clone();

}

mlawGenericCrackPhaseField::mlawGenericCrackPhaseField(const mlawGenericCrackPhaseField& source) :
    materialLaw(source), _matlaw(NULL), _rho(source._rho), _cLLaw(NULL), _gc(source._gc) {

    if (source._matlaw != NULL) {
        if(_matlaw != NULL) {
            delete _matlaw;
            _matlaw = NULL;
        }

        _matlaw=source._matlaw->clone();
    }

    if (source._cLLaw != NULL)
    {
        if (_cLLaw != NULL)
        {
            delete _cLLaw;
            _cLLaw = NULL;
        }

        _cLLaw = source._cLLaw->clone();
    }
}

void mlawGenericCrackPhaseField::setMechanicalMaterialLaw(const materialLaw *matlaw) {
    if(_matlaw != NULL) {
        delete _matlaw;
        _matlaw = NULL;
    }
    _matlaw = matlaw->clone();
}

const materialLaw& mlawGenericCrackPhaseField::getConstRefMechanicalMaterialLaw() const {
    if(_matlaw == NULL) {
        Msg::Error("getConstRefMechanicalMaterialLaw: Mechanic law is null");
    }
    return *_matlaw;
}

materialLaw& mlawGenericCrackPhaseField::getRefMechanicalMaterialLaw() {
    if(_matlaw == NULL) {
        Msg::Error("getRefMechanicalMaterialLaw: Mechanic law is null");
    }
    return *_matlaw;
}

void mlawGenericCrackPhaseField::setTime(const double ctime, const double dtime) {
    materialLaw::setTime(ctime, dtime);
    if(_matlaw == NULL) {
        Msg::Error("setTime: Mechanic law is null");
    }
    _matlaw->setTime(ctime, dtime);
}

void mlawGenericCrackPhaseField::setMacroSolver(const nonLinearMechSolver *sv) {
    materialLaw::setMacroSolver(sv);
    if(_matlaw == NULL) {
        Msg::Error("setMacroSolver: Mechanic law is null");
    }
    _matlaw->setMacroSolver(sv);
}

mlawGenericCrackPhaseField& mlawGenericCrackPhaseField::operator=(const materialLaw &source){
    materialLaw::operator=(source);
    const mlawGenericCrackPhaseField* src = static_cast<const mlawGenericCrackPhaseField*>(&source);
    if(src !=NULL)
    {
        if(src->_matlaw!=NULL)
        {
            if(_matlaw != NULL)
            {
                _matlaw->operator=(dynamic_cast<const materialLaw& >(*(src->_matlaw)));
            }
            else
              _matlaw=src->_matlaw->clone();
        }

        if(src->_cLLaw!=NULL)
        {
            if(_cLLaw != NULL)
            {
                _cLLaw->operator=(dynamic_cast<const CLengthLaw& >(*(src->_cLLaw)));
            }
            else
                _cLLaw=src->_cLLaw->clone();
        }



        _rho = src->_rho;
        // _lc = src->_lc;
        _gc = src->_gc;

    }

    return *this;
}

double mlawGenericCrackPhaseField::soundSpeed() const {
    if(_matlaw == NULL) {
        Msg::Error("soundSpeed: Mechanic law is null");
    }
    return _matlaw->soundSpeed();
}

double mlawGenericCrackPhaseField::density() const {
    if(_matlaw == NULL) {
        Msg::Error("density: Mechanic law is null");
    }
    return _matlaw->density();
}

double mlawGenericCrackPhaseField::ElasticShearModulus() const {
    if(_matlaw == NULL) {
        Msg::Error("ElasticShearModulus: Mechanic law is null");
    }
    return _matlaw->ElasticShearModulus();
}

bool mlawGenericCrackPhaseField::withEnergyDissipation() const {
    if(_matlaw == NULL) {
        Msg::Error("withEnergyDissipation: Mechanic law is null");
    }
    return _matlaw->withEnergyDissipation();
}

void mlawGenericCrackPhaseField::ElasticStiffness(STensor43 *elasticStiffness) const {
    _matlaw->ElasticStiffness(elasticStiffness);
}

void mlawGenericCrackPhaseField::createIPState(IPStateBase *&ips, bool hasBodyForce, const bool *state_,
    const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const {
 
  bool inter=true;
  const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
  if(iele==NULL) inter=false;

  static SPoint3 pt_Global;
  ele->pnt(GP->pt[0],GP->pt[1],GP->pt[2],pt_Global);
  static SVector3 GaussP;
  GaussP[0] = pt_Global[0];
  GaussP[1] = pt_Global[1];
  GaussP[2] = pt_Global[2];
  
  if(ips != NULL) delete ips;
  IPGenericCrackPhaseField *ipvi=new IPGenericCrackPhaseField(_cLLaw);
  IPGenericCrackPhaseField *ipv1=new IPGenericCrackPhaseField(_cLLaw);
  IPGenericCrackPhaseField *ipv2=new IPGenericCrackPhaseField(_cLLaw);

  IPStateBase* ipsmeca=NULL;
  getConstRefMechanicalMaterialLaw().createIPState(ipsmeca,hasBodyForce, state_,ele, nbFF_,GP, gpt);
  std::vector<IPVariable*> allIP;
  ipsmeca->getAllIPVariable(allIP);

  ipvi->setIpMeca(*allIP[0]->clone());
  ipv1->setIpMeca(*allIP[1]->clone());
  ipv2->setIpMeca(*allIP[2]->clone());

  ips = new IP3State(state_,ipvi,ipv1,ipv2);
  delete ipsmeca;
}

void mlawGenericCrackPhaseField::createIPVariable(IPGenericCrackPhaseField *&ipv, bool hasBodyForce, const MElement *ele,
    const int nbFF, const IntPt *GP, const int gpt) const {
  bool inter=true;
  const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
  if(iele==NULL) inter=false;

  static SPoint3 pt_Global;
  ele->pnt(GP->pt[0],GP->pt[1],GP->pt[2],pt_Global);
  static SVector3 GaussP;
  GaussP[0] = pt_Global[0];
  GaussP[1] = pt_Global[1];
  GaussP[2] = pt_Global[2];
  
  if(ipv != NULL)
      delete ipv;
  ipv = new IPGenericCrackPhaseField(_cLLaw);

  IPStateBase* ipsmeca=NULL;
  const bool state_ = true;
  getConstRefMechanicalMaterialLaw().createIPState(ipsmeca,hasBodyForce, &state_,ele, nbFF,GP, gpt);

  std::vector<IPVariable*> allIP;
  ipsmeca->getAllIPVariable(allIP);

  ipv->setIpMeca(*allIP[0]->clone());
  delete ipsmeca;
}

mlawGenericCrackPhaseField::~mlawGenericCrackPhaseField() {
    if(_matlaw != NULL) {
        delete _matlaw;
        _matlaw = NULL;
    }

    if (_cLLaw != NULL)
    {
        delete _cLLaw;
        _cLLaw = NULL;
    }
}

void mlawGenericCrackPhaseField::constitutive(
                          const STensor3& F0,         // initial deformation gradient (input @ time n)
                          const STensor3& Fn,         // updated deformation gradient (input @ time n+1)
                          STensor3 &P,                // updated 1st Piola-Kirchhoff stress tensor (output)
                                                      // contains the initial values on input
                          const IPVariable *q0,       // array of initial internal variable
                          IPVariable *q1,             // updated array of internal variable (in ipvcur on output),
                          STensor43 &Tangent,         // constitutive tangents (output)
                          std::vector< STensor3 >  &dLocalPlasticStrainDStrain,
                          std::vector< STensor3 >  &dStressDNonLocalPlasticStrain,
                          fullMatrix<double>   &dLocalPlasticStrainDNonLocalPlasticStrain,
                          const bool stiff,            // if true compute the tangents
                          STensor43* elasticTangent //TODO @Mohib
                          ) const
{

    if(_matlaw == NULL)
        Msg::Error("Mechanic law is null");

    STensorOperation::zero(Tangent);

    const double dh= getTimeStep();
    const double time=getTime();

    const IPGenericCrackPhaseField &qPF0= dynamic_cast<const IPGenericCrackPhaseField &> (*q0);
    IPGenericCrackPhaseField &qPF1= dynamic_cast<IPGenericCrackPhaseField &> (*q1);

    // double &localVariable=qPF1.getRefTo
    const double &localVariable0=qPF0.getConstRefToLocalDamageVariable();
    double &localVariable1=qPF1.getRefToLocalDamageVariable();

    const double &nonlocalVariable0=qPF0.getConstRefToNonLocalDamageVariable();
    double &nonlocalVariable1=qPF1.getRefToNonLocalDamageVariable();

    const IPVariableMechanics& qMeca0 = dynamic_cast<const IPGenericCrackPhaseField&>(*q0).getConstRefToIpMeca();
    IPVariableMechanics& qMecan = dynamic_cast<IPGenericCrackPhaseField&>(*q1).getRefToIpMeca();
    
    static STensor3 Pmeca;
    STensorOperation::zero(Pmeca);

    static STensor43 TangentMeca;
    STensorOperation::zero(TangentMeca);

    static STensor43 elasticTangentMeca;
    STensorOperation::zero(elasticTangentMeca);

    static STensor63 dTangentMecadF;
    STensorOperation::zero(dTangentMecadF);



    const bool dTangent = false; //TODO @Mohib
    _matlaw->constitutive(F0, Fn, Pmeca,
                          &qPF0.getConstRefToIpMeca(),
                          &qPF1.getRefToIpMeca(),
                          TangentMeca, stiff, 
                          &elasticTangentMeca, dTangent, &dTangentMecadF);

    
    double degrade = (1.0 - nonlocalVariable1) * (1.0 - nonlocalVariable1);

    // Get deformation energy and 1PK stress
    double hh = qMecan.defoEnergy();
    STensor3 PMax = Pmeca;
    // Compare with effenergy of the last step and only update if its greater than it
    const double hh0 = qPF0.getConstRefToeffEner();
    const STensor3 PMax0 = qPF0.getConstRefToPMax();
    if( hh <= hh0){
        hh = hh0;
        PMax = PMax0;
    }
    // also update the max values of effEner and PMax if they have increased 
    else{
        qPF1.seteffEner(hh);
        qPF1.setPMax(PMax);
    }

    // Compute total P
    STensorOperation::scale(Pmeca, degrade, P);

    // Compute total Tangent
    STensorOperation::scale(TangentMeca, degrade, Tangent);

    // get length scale from Prescribed CLengthLaw
    _cLLaw->computeCL(localVariable0, qPF1.getRefToIPCLength());
    
    double _lc = qPF1.getRefToCharacteristicLength()[0];

    // Update the local damage variable
    // qPF1.setlocalDamageVariable(((-2.0 * _lc) / _gc) * (1.0 - nonlocalVariable1) * hh);
    qPF1.setlocalDamageVariable(((2.0 * _lc) / _gc) * (1.0 - nonlocalVariable1) * hh);

    if(getNbNonLocalVariables() == 1){

        double factor = ((2.0 * _lc) / _gc) * (1.0 - nonlocalVariable1);
        STensorOperation::scale(PMax, factor, dLocalPlasticStrainDStrain[0]);

        double factor1 = 2.0 * (1.0 - nonlocalVariable1);
        STensorOperation::scale(Pmeca, factor1, dStressDNonLocalPlasticStrain[0]);

        dLocalPlasticStrainDNonLocalPlasticStrain(0,0) = ((-2.0 * _lc) / _gc) * hh;
    }

    //TODO: 

    // q1->getRefToDamageEnergy() = q0->damageEnergy() + effEner* (D-q0->getDamage());
    // q1->getRefToDefoEnergy() = qMecan->defoEnergy() * degrade * DamageEnergy // TODO Fix 
    // q1->getRefToplastic() = ... compute from mecha
    
    double defoEnergy = qMecan.defoEnergy() * degrade; 
    qPF1.setdefoEnergy(defoEnergy);

    double plasticEnergy = qMecan.plasticEnergy();
    qPF1.setplasticEnergy(plasticEnergy);
    
    double damageEnergy = (1.0 - degrade) * hh;
    qPF1.setdamageEnergy(damageEnergy);
    
    // Path following only - irreversible energy

    // Dissipated energy = previous dissipated energy + Y delta_D

    if (this->getMacroSolver()->getPathFollowingLocalIncrementType() == pathFollowingManager::DEFO_ENERGY){

        qPF1.getRefToIrreversibleEnergy() = qPF1.defoEnergy();

    }

    else if ((this->getMacroSolver()->getPathFollowingLocalIncrementType() == pathFollowingManager::DISSIPATION_ENERGY) or (this->getMacroSolver()->getPathFollowingLocalIncrementType() == pathFollowingManager::DAMAGE_ENERGY)){

        qPF1.getRefToIrreversibleEnergy() = qPF1.damageEnergy();

    }
    else{
        qPF1.getRefToIrreversibleEnergy() = 0.;

    }

    if (stiff)
    {
        STensor3& DIrrevEnergyDF = qPF1.getRefToDIrreversibleEnergyDF();

        double& DIrrevEnergyDNonlocalVar = qPF1.getRefToDIrreversibleEnergyDNonLocalVariable();

        DIrrevEnergyDF = Pmeca;

        if (this->getMacroSolver()->getPathFollowingLocalIncrementType() == pathFollowingManager::DEFO_ENERGY)
        {
            // DIrrevEnergyDF*= (1.-D);
            DIrrevEnergyDF*= degrade;
            // if (qPF1->getNonLocalToLocal()){

            //     DIrrevEnergyDF.daxpy(dLocalEffectiveStrainsDStrains,-q1->getDDamageDp()*effEner);

            //     DIrrevEnergyDNonlocalVar = 0.;

            // }

            // else{
            //     // DIrrevEnergyDNonlocalVar =  -effEner*q1->getDDamageDp();
            //     DIrrevEnergyDNonlocalVar =  qMecan.defoEnergy() * -2 * (1 - nonlocalVariable1);
            // }
            DIrrevEnergyDNonlocalVar =  qMecan.defoEnergy() * -2 * (1 - nonlocalVariable1);
        }

        else if ((this->getMacroSolver()->getPathFollowingLocalIncrementType() == pathFollowingManager::DISSIPATION_ENERGY) or (this->getMacroSolver()->getPathFollowingLocalIncrementType() == pathFollowingManager::DAMAGE_ENERGY))
        {

            // DIrrevEnergyDF *= (D - q0->getDamage());
            DIrrevEnergyDF = (1.0 - degrade) * PMax;
    
            // if (qPF1->getNonLocalToLocal()){

            //     DIrrevEnergyDF.daxpy(dLocalEffectiveStrainsDStrains,q1->getDDamageDp()*effEner);
            //     DIrrevEnergyDNonlocalVar = 0.;

            // }
            // else{
            //     // DIrrevEnergyDNonlocalVar =  effEner*q1->getDDamageDp();
            //     DIrrevEnergyDNonlocalVar =  hh * 2.0 * (1.0 - nonlocalVariable1);

            // }
            DIrrevEnergyDNonlocalVar =  hh * 2.0 * (1.0 - nonlocalVariable1);

        }

        else
        {
            DIrrevEnergyDF = 0.;
            DIrrevEnergyDNonlocalVar = 0.;
        }

    }
        
}
                        