//
// C++ Interface: material law
//
// Description: nonlinear viscoelastic (Simo J.C., Computational Inelasticity, Springer)
//
//
// Author:  <Dongli Li, Antoine Jerusalem>, (C) 2014
//
// Copyright: See COPYING file that comes with this distribution
//
//

#include <math.h>
#include "MInterfaceElement.h"
#include "mlawViscoelastic.h"
#include <iostream>
#include <fstream>
#include <iomanip>
#include <vector>

std::vector<double> mlawViscoelastic::read_file(const char* file_name, int file_size)
{
    std::ifstream file(file_name);
    std::vector<double> output;

    if(file.is_open())
    {
        for(int i = 0; i < file_size; ++i)
        {
            std::string curr_string;
            file >> curr_string;
            double curr_number = std::atof(curr_string.c_str());
            output.push_back(curr_number);
        }
    }

    return output;
}

void mlawViscoelastic::Write_Matrix_To_File(std::ofstream& my_file, const STensor3 &X, std::string matrix_name)
{
    my_file << matrix_name<<"\n";
    my_file << std::scientific << std::setprecision(14) << X(0,0) << " "<<X(0,1) << " " <<X(0,2) << "\n";
    my_file << X(1,0) << " "<<X(1,1) << " " << X(1,2)<< "\n";
    my_file << X(2,0) << " " <<X(2,1) << " " << X(2,2) << "\n";

}

double mlawViscoelastic::determinantSTensor3(const STensor3 &a) const
{
    return (a(0,0) * (a(1,1) * a(2,2) - a(1,2) * a(2,1)) -
            a(0,1) * (a(1,0) * a(2,2) - a(1,2) * a(2,0)) +
            a(0,2) * (a(1,0) * a(2,1) - a(1,1) * a(2,0)));
}

void mlawViscoelastic::inverseSTensor3(const STensor3 &a, STensor3 &ainv) const
{
    double udet = 1./determinantSTensor3(a);
    ainv(0,0) =  (a(1,1) * a(2,2) - a(1,2) * a(2,1))* udet;
    ainv(1,0) = -(a(1,0) * a(2,2) - a(1,2) * a(2,0))* udet;
    ainv(2,0) =  (a(1,0) * a(2,1) - a(1,1) * a(2,0))* udet;
    ainv(0,1) = -(a(0,1) * a(2,2) - a(0,2) * a(2,1))* udet;
    ainv(1,1) =  (a(0,0) * a(2,2) - a(0,2) * a(2,0))* udet;
    ainv(2,1) = -(a(0,0) * a(2,1) - a(0,1) * a(2,0))* udet;
    ainv(0,2) =  (a(0,1) * a(1,2) - a(0,2) * a(1,1))* udet;
    ainv(1,2) = -(a(0,0) * a(1,2) - a(0,2) * a(1,0))* udet;
    ainv(2,2) =  (a(0,0) * a(1,1) - a(0,1) * a(1,0))* udet;
}

void mlawViscoelastic::multSTensor3(const STensor3 &a, const STensor3 &b, STensor3 &c) const
{
    for(int i=0;i<3;i++)
        for(int j=0;j<3;j++)
        {
            c(i,j) = a(i,0)*b(0,j)+a(i,1)*b(1,j)+a(i,2)*b(2,j);
        }
}

void mlawViscoelastic::multSTensor3FirstTranspose(const STensor3 &A, const STensor3 &B, STensor3 &C) const
{
    for(int i=0;i<3;i++)
        for(int j=0;j<3;j++)
            C(i,j) = A(0,i)*B(0,j) + A(1,i)*B(1,j) + A(2,i)*B(2,j);
}

void mlawViscoelastic::multSTensor3SecondTranspose(const STensor3 &a, const STensor3 &b, STensor3 &c) const
{
    for(int i=0;i<3;i++)
        for(int j=0;j<3;j++)
            c(i,j) = a(i,0)*b(j,0)+a(i,1)*b(j,1)+a(i,2)*b(j,2);
}

STensor3 mlawViscoelastic::devSTensor3(STensor3 &a) const
{
    STensor3 I(1.);
    STensor3 deva(0.);
    double trace = a(0,0)+a(1,1)+a(2,2);
    for(int i=0; i<3; i++)
    {
        for(int j=0; j<3; j++)
        {
            deva(i,j) = a(i,j)-1./3.*trace*I(i,j);
        }
    }

    return deva;
}

mlawViscoelastic::mlawViscoelastic(const int num,const double K, const double rho, const std::string &array_eta, int array_size_eta, const std::string &array_mu,
                                   const double tol, const bool pert, const double eps) : materialLaw(num,true),
    _K(K), _rho(rho),_N(array_size_eta),
    _tol(tol),_perturbationfactor(eps),_tangentByPerturbation(pert)
{

    _mu.resize(0);
    _eta.resize(0);
    _tau.resize(0);

    _mu = read_file(array_mu.c_str(),array_size_eta+1);
    _eta = read_file(array_eta.c_str(),array_size_eta);

    double sum_mu = 0.;

    for(int i=0;i<_mu.size();++i)
    {
        sum_mu += _mu.at(i);
    }

    _E = 9.*_K*sum_mu/(3.*_K+sum_mu);

    _nu = (3.*_K - 2.*sum_mu)/2./(3.*_K + sum_mu);

    for(int i = 0; i<_eta.size();++i)
    {
        _tau.push_back(_eta.at(i)/_mu.at(i+1));
    }

}

mlawViscoelastic::mlawViscoelastic(const mlawViscoelastic &source) : materialLaw(source),_E(source._E), _nu(source._nu), _rho(source._rho),_N(source._N),
    _mu(source._mu),_eta(source._eta),_tau(source._tau),
    _tol(source._tol),
    _perturbationfactor(source._perturbationfactor),
    _tangentByPerturbation(source._tangentByPerturbation)
{
    _K = source._K;
}


mlawViscoelastic& mlawViscoelastic::operator=(const materialLaw &source)
{
    materialLaw::operator=(source);
    const mlawViscoelastic* src =static_cast<const mlawViscoelastic*>(&source);
    if(src != NULL)
    {

        _E = src->_E;
        _nu = src->_nu;
        _rho = src->_rho;
        _mu = src->_mu;
        _eta = src->_eta;
        _tau = src->_tau;
        _N = src->_N;
        _K = src->_K;
        _tol = src->_tol;
        _perturbationfactor = src->_perturbationfactor;
        _tangentByPerturbation = src->_tangentByPerturbation;
    }
    return *this;
}

materialLaw * mlawViscoelastic::clone() const
{

    return new mlawViscoelastic(*this);
}


void mlawViscoelastic::createIPState(IPStateBase* &ips,bool hasBodyForce, const bool* state_,const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{

    IPVariable* ipvi = new IPViscoelastic(_N);
    IPVariable* ipv1 = new IPViscoelastic(_N);
    IPVariable* ipv2 = new IPViscoelastic(_N);
    if(ips != NULL) delete ips;
    ips = new IP3State(state_,ipvi,ipv1,ipv2);
}

double mlawViscoelastic::soundSpeed() const
{

    double factornu = (1.-_nu)/((1.+_nu)*(1.-2.*_nu));
    return sqrt(_E*factornu/_rho);
}

//----------------------------------------initial elastic stress Kirchhoff tensor-----------------------------------------//
void mlawViscoelastic::initialKirchhoffStressTensor(const STensor3 &Fn, STensor3 &Kirchhoff_) const
{

    double Jn = determinantSTensor3(Fn);
    double frac = 1./3.;
    STensor3 Fn_ = pow(Jn, -frac) * Fn; // volume-preserving deformation gradient

    double sum_mu = 0.;

    for(int i=0;i<_mu.size();++i)
    {
        sum_mu += _mu.at(i);
    }

    double derivative =1./2.*sum_mu;
    STensor3 total(0.);
    multSTensor3SecondTranspose(Fn_, Fn_,total);
    total = 2.0*derivative*total;
    Kirchhoff_ = devSTensor3(total); // T = dev[mu * F_*F_']

}


void mlawViscoelastic::constitutive(const STensor3& F0,
                                    const STensor3& Fn,
                                    STensor3 &P, 
                                    const IPVariable *IPvisprevi,
                                    IPVariable *IPvisi, 
                                    STensor43 &Tangent, 
                                    const bool stiff,
                                    STensor43* elasticTangent, 
                                    const bool dTangent,
                                    STensor63* dCalgdeps) const
{
    IPViscoelastic *IPvis = dynamic_cast<IPViscoelastic *>(IPvisi);
    const IPViscoelastic *IPvisprev=dynamic_cast<const IPViscoelastic *>(IPvisprevi);
    if(stiff || elasticTangent!=NULL)
        Msg::Error("Stiffness matrix not available");
    // update
    this->update(F0,Fn,P,IPvis,IPvisprev);
}


   // update internal variables
void mlawViscoelastic::update_IP(const STensor3 &F1, STensor3 &kirchhoff1_,IPViscoelastic *IPvis, const IPViscoelastic *IPvisprev, double &g,STensor3 &hn,const std::vector<double> &r) const
{
    double dt = _timeStep;
    double Jn1 = determinantSTensor3(F1);
    double frac = 1./3.;
    STensor3 F1_ = pow(Jn1, -frac) * F1;
    STensor3 Inv_F1_(0.);
    inverseSTensor3(F1_,Inv_F1_);
    STensor3 inter1(0.);
    std::vector<STensor3> Hn_c;
    Hn_c.resize(_N,STensor3(0.));

    for(int i=0; i<_N; i++) // for all the internal variables
    {

        Hn_c.at(i) = exp(-dt/_tau.at(i))*IPvisprev->_Hn.at(i)+(-exp(-dt/(2.*_tau.at(i)))*IPvisprev->_Sn); //Hn_c(i) = exp(-dt/tau(i))Hn(i) - exp(-dt/2*tau(i))Sn

        multSTensor3(Inv_F1_,kirchhoff1_,inter1);
        multSTensor3SecondTranspose(inter1,Inv_F1_,IPvis->_Sn); //Sn+1 = F_^-1* T * F_^-T

        IPvis->_Hn.at(i) = Hn_c.at(i)+exp(-dt/(2.*_tau.at(i)))*IPvis->_Sn; //Hn+1(i) = Hn_c(i) + exp(-dt/2*tau(i))Sn+1

        g = g+r.at(i)*exp(-dt/(2*_tau.at(i))); //g = r_inf + sum(r(i)*exp(-dt/2*tau(i)))
        STensor3 total(0.);
        STensor3 intertotal(0.);
        multSTensor3(F1_,Hn_c.at(i), intertotal);

        multSTensor3SecondTranspose(intertotal, F1_,total);
        hn = hn +r.at(i)* devSTensor3(total); //hn = sum(r(i)*dev[F_*Hn_c(i)*F_'])
    }
}


void mlawViscoelastic::update(const STensor3& F0, const STensor3& F, STensor3&P,IPViscoelastic *IPvis, const IPViscoelastic *IPvisprev) const
{
    STensor3 Finv(0.);
    inverseSTensor3(F,Finv);

    STensor3 T(0.);
    this->stress(T,F,IPvis,IPvisprev); // calculate Kirchhoff stress

    multSTensor3SecondTranspose(T,Finv,P);

}

// Kirchhoff stress
void mlawViscoelastic::stress(STensor3 &Sig_,const STensor3 &Fn,IPViscoelastic *IPvis, const IPViscoelastic *IPvisprev) const
{
    double Jn = determinantSTensor3(Fn);

    // volumetric part ( this is for a free energy function Psi=1/2 k (1/2 J^2 -1) - ln(J) )
    double P = 0.5*_K*(Jn - 1./Jn);
    STensor3 I(1.);
    STensor3 Part1 = Jn*P * I; //volumetric part = JPI

    double SumMu = 0.;
    std::vector<double> r;

    for (int i=0; i<_mu.size(); i++)
    {
        SumMu += _mu.at(i); //_mu.at(0) = mu0 for t=inf
    }
    for(int i=0; i<_N; i++)
    {
        r.push_back(_mu.at(i+1)/SumMu); //r(i) = mu(i)/sumMu

    }

    double r_inf = _mu.at(0)/SumMu; //r_inf = mu0/sumMu
    double g = r_inf;
    STensor3 IniKirchhoff_(0.);
    this->initialKirchhoffStressTensor(Fn,IniKirchhoff_);

    STensor3 hn(0.);

    update_IP(Fn, IniKirchhoff_,IPvis,IPvisprev, g,hn,r); //update algorithmic internal variables

    STensor3 Part2 = g* IniKirchhoff_;
    Sig_ = Part1 + Part2 + hn; //Kirchhoff stress

}
