//
// C++ Interface: material law
//
// Description: material law for plastic anisotropy 
//
// Author:  <V.D. Nguyen>, (C) 2023
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef MLAWANISOTROPICPLASTICITY_H_
#define MLAWANISOTROPICPLASTICITY_H_

#include "mlaw.h"
#include "STensor3.h"
#include "STensor43.h"
#include "ipAnisotropicPlasticity.h"


class mlawAnisotropicPlasticity : public materialLaw
{
  public:
    enum stressFormulation{ CORO_CAUCHY = 0,    // cauchy stress in yield surface
                            CORO_KIRCHHOFF = 1  // Kirchhoff stress in yield surface
                          };
  protected:
    J2IsotropicHardening *_j2IH;
    // can't be const due to operator= constructor (HOW TO CHANGE THIS ??)
    double _rho; // density
    double _E; // young modulus
    double _nu; // Poisson ratio
    double _lambda; // 1st lame parameter
    double _mu; // 2nd lame parameter (=G)
    double _K; // bulk modulus
    double _K3; // 3*bulk modulus
    double _mu3; // 3*_mu = 3*G
    double _mu2; // 2*_mu = 2*G
    double _tol; // tolerance for iterative process
    int _maxite;
    double _perturbationfactor; // perturbation factor
    bool _tangentByPerturbation; // flag for tangent by perturbation
    STensor43 _Cel, _I4, _I4dev; // elastic hook tensor
    int _order;
    stressFormulation _stressFormulation;

  public:
    mlawAnisotropicPlasticity(const int num,const double E,const double nu, const double rho,const J2IsotropicHardening &_j2IH,const double tol=1.e-6,
              const bool matrixbyPerturbation = false, const double pert = 1e-8);
  #ifndef SWIG
    mlawAnisotropicPlasticity(const mlawAnisotropicPlasticity &source);
    mlawAnisotropicPlasticity& operator=(const materialLaw &source);
    
    virtual ~mlawAnisotropicPlasticity();
    
    virtual bool withEnergyDissipation() const {return true;};
    // function of materialLaw
    virtual materialLaw* clone() const =0;
    virtual matname getType() const = 0;
    virtual void setYieldParameters(const std::vector<double>& params)=0;
    virtual void checkInternalState(IPVariable* ipv, const IPVariable* ipvprev) const{};
    virtual void createIPState(IPStateBase* &ips,bool hasBodyForce, const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const IntPt *GP=NULL, const int gpt = 0) const;
    virtual void initLaws(const std::map<int,materialLaw*> &maplaw){}; // this law is initialized so nothing to do
    virtual double soundSpeed() const; // default but you can redefine it for your case
    virtual double density()const{return _rho;}
    virtual const double bulkModulus() const{return _K;}
    virtual const double shearModulus() const{return _mu;}
    virtual const double poissonRatio() const{return _nu;}
    virtual double scaleFactor() const{return _mu;};
    virtual const J2IsotropicHardening * getJ2IsotropicHardening() const {return _j2IH;}
    void setStressFormulation(int s);
    virtual void setStrainOrder(const int i) {_order = i; Msg::Info("order %d is used to approximate log and exp operator ",_order);};
    // specific function
  public:
    virtual void constitutive(
                            const STensor3& F0,         // initial deformation gradient (input @ time n)
                            const STensor3& Fn,         // updated deformation gradient (input @ time n+1)
                            STensor3 &P,                // updated 1st Piola-Kirchhoff stress tensor (output)
                                                        // contains the initial values on input
                            const IPVariable *q0,       // array of initial internal variable
                            IPVariable *q1,             // updated array of internal variable (in ipvcur on output),
                            STensor43 &Tangent,         // constitutive tangents (output)
                            const bool stiff,            // if true compute the tangents
                            STensor43* elasticTangent = NULL, 
                            const bool dTangent =false,
                            STensor63* dCalgdeps = NULL
                           ) const;
  protected:
    virtual int getNumOfYieldSurfaces() const {return 1;}
    double deformationEnergy(const STensor3 &C) const ;
    virtual void effectivePresureVMS(const STensor3& F, const STensor3& sig, const IPAnisotropicPlasticity* q0, const IPAnisotropicPlasticity* q1, double& effPress, double& effSVM) const = 0;
    virtual void computeRotatedAnisotropicTensor(const STensor3& F, const STensor3& DeltaEp,  const IPAnisotropicPlasticity* q0, IPAnisotropicPlasticity* q1, bool stiff) const=0;
    virtual bool withPlastic(const STensor3& F, const STensor3& kcor, const double R, const IPAnisotropicPlasticity* q0, const IPAnisotropicPlasticity* q1) const;
    virtual void yieldFunction(std::vector<double>& yf, const STensor3& sig, const double R, const IPAnisotropicPlasticity* q0, const IPAnisotropicPlasticity* q1,
                                        bool diff=false, std::vector<STensor3>* DFDsig=NULL, std::vector<double>* DFDR=NULL,
                                        std::vector<STensor3>* DFDDeltaEp=NULL, std::vector<STensor3>* DFDdefo=NULL) const=0;
    virtual void getYieldNormal(std::vector<STensor3>& Np, const STensor3& sig, const double R, const IPAnisotropicPlasticity* q0, const IPAnisotropicPlasticity* q1,
                                        bool diff=false, std::vector<STensor43>* DNpDsig=NULL, std::vector<STensor3>* DNpDR=NULL,
                                        std::vector<STensor43>* DFNpDeltaEp=NULL, std::vector<STensor43>* DNpDdefo=NULL) const=0;
    virtual void computeResidual(const STensor3& DeltaEp, std::vector<double>& DeltaPlasticMult, const double DeltaHatP, // unknown value 
                                        const STensor3& sig,  const double R, 
                                        const IPAnisotropicPlasticity* q0, const IPAnisotropicPlasticity* q1, 
                                        std::vector<double>& res0, STensor3& res1, double& res2,  
                                        bool stiff, 
                                        const STensor43& DsigDDeltaEp, const double H,
                                        std::vector<STensor3>& Dres0DDeltaEp, STensor43& Dres1DDeltaEp, STensor3& Dres2DDeltaEp,
                                        std::vector<double>& Dres0DDeltaPlasticMult, std::vector<STensor3>& Dres1DDeltaPlasticMult, std::vector<double>& Dres2DDeltaPlasticMult,
                                        std::vector<double>& Dres0DDeltaHatP, STensor3& Dres1DDeltaHatP, double& Dres2DDeltaHatP) const;
    virtual void computeDResidual(const STensor3& DeltaEp, const std::vector<double>& DeltaPlasticMult, const double DeltaHatP, // unknown value 
                                  const STensor3& sig, const double R,  const IPAnisotropicPlasticity* q0, const IPAnisotropicPlasticity* q1,
                                  const STensor43& DsigDEepr, const double H, const STensor43& EprToF,
                                  std::vector<STensor3>& Dres0DEepr, STensor43& Dres1DEepr, STensor3& Dres2DEepr) const;
                                  
    virtual void getResAndJacobian(fullVector<double>& res, fullMatrix<double>& J, 
                                const std::vector<double>& res0, const STensor3& res1, const double& res2,
                                const std::vector<STensor3>& Dres0DDeltaEp, const STensor43& Dres1DDeltaEp, const STensor3& Dres2DDeltaEp,
                                const std::vector<double>& Dres0DDeltaPlasticMult, const std::vector<STensor3>& Dres1DDeltaPlasticMult, const std::vector<double>& Dres2DDeltaPlasticMult,
                                const std::vector<double>& Dres0DDeltaHatP, const STensor3& Dres1DDeltaHatP,const double& Dres2DDeltaHatP) const;
                                
    virtual void tangentComputation(STensor43& dStressDF,
                              const bool plastic,
                              const STensor3& F,  // current F
                              const STensor3& corKir,  // cor Kir
                              const STensor3& S, //  second PK
                              const STensor3& Fepr, const STensor3& Fppr, // predictor
                              const STensor43& Lpr,
                              const STensor3& Fe, const STensor3& Fp, // corrector
                              const STensor43& L, const STensor63& dL, // corrector value
                              const STensor43& DcorKirDF,
                              const STensor43& dFpDF,
                              const STensor3& Fpinv
                              ) const;
                                    
    virtual bool predictorCorector(const STensor3& F0,         // initial deformation gradient (input @ time n)
                            const STensor3& F,         // updated deformation gradient (input @ time n+1)
                            STensor3 &P,                // updated 1st Piola-Kirchhoff stress tensor (output)
                            const IPAnisotropicPlasticity *q0,       // array of initial internal variable
                            IPAnisotropicPlasticity *q,             // updated array of internal variable (in ipvcur on output),
                            STensor43& Tangent,         // mechanical tangents (output)
                            STensor43& dFpdF, // plastic tangent
                            STensor43& dFedF,
                            STensor3& dpdF,
                            const bool stiff
                            ) const;
 #endif // SWIG
}; 


class mlawAnisotropicPlasticityJ2 : public mlawAnisotropicPlasticity
{
  // to test the implementation
  public:
    mlawAnisotropicPlasticityJ2(const int num,const double E,const double nu, const double rho,const J2IsotropicHardening &_j2IH,const double tol=1.e-6,
              const bool matrixbyPerturbation = false, const double pert = 1e-8);
  #ifndef SWIG
    mlawAnisotropicPlasticityJ2(const mlawAnisotropicPlasticityJ2 &source);
    mlawAnisotropicPlasticityJ2& operator=(const materialLaw &source);
    virtual ~mlawAnisotropicPlasticityJ2(){};
    virtual materialLaw* clone() const {return new mlawAnisotropicPlasticityJ2(*this);};
    // function of materialLaw
    virtual matname getType() const {return materialLaw::J2;};
    virtual void setYieldParameters(const std::vector<double>& params){};
  
  protected:
    virtual void computeRotatedAnisotropicTensor(const STensor3& F, const STensor3& DeltaEp,  const IPAnisotropicPlasticity* q0, IPAnisotropicPlasticity* q1, bool stiff) const{};
    virtual void effectivePresureVMS(const STensor3& F, const STensor3& sig, const IPAnisotropicPlasticity* q0, const IPAnisotropicPlasticity* q1, double& effPress, double& effSVM) const;
    virtual void yieldFunction(std::vector<double>& yf, const STensor3& sig, const double R, const IPAnisotropicPlasticity* q0, const IPAnisotropicPlasticity* q1,
                                        bool diff=false, std::vector<STensor3>* DFDsig=NULL, std::vector<double>* DFDR=NULL,
                                        std::vector<STensor3>* DFDDeltaEp=NULL, std::vector<STensor3>* DFDdefo=NULL) const;
    virtual void getYieldNormal(std::vector<STensor3>& Np, const STensor3& sig, const double R, const IPAnisotropicPlasticity* q0, const IPAnisotropicPlasticity* q1,
                                        bool diff=false, std::vector<STensor43>* DNpDsig=NULL, std::vector<STensor3>* DNpDR=NULL,
                                        std::vector<STensor43>* DFNpDeltaEp=NULL, std::vector<STensor43>* DNpDdefo=NULL) const;
  #endif // SWIG
};



class mlawAnisotropicPlasticityHill48 : public mlawAnisotropicPlasticity
{
  protected:
    STensor43 _aniM;
  public:
    mlawAnisotropicPlasticityHill48(const int num,const double E,const double nu, const double rho,const J2IsotropicHardening &_j2IH, const double tol=1.e-6,
              const bool matrixbyPerturbation = false, const double pert = 1e-8);
  #ifndef SWIG
    mlawAnisotropicPlasticityHill48(const mlawAnisotropicPlasticityHill48 &source);
    mlawAnisotropicPlasticityHill48& operator=(const materialLaw &source);
    virtual ~mlawAnisotropicPlasticityHill48(){};
    virtual materialLaw* clone() const {return new mlawAnisotropicPlasticityHill48(*this);};
    // function of materialLaw
    virtual matname getType() const {return materialLaw::Hill48;};
    virtual void setYieldParameters(const std::vector<double>& params);
    
  protected:
    virtual void computeAnisotropyRotation(STensor3& Rtotal, const STensor3& F, const STensor3& DeltaEp, const STensor3& Fp0, 
                                                bool stiff=false, STensor43* DMDF=NULL, STensor43* DMDDeltaEp=NULL) const;
    
    virtual void effectivePresureVMS(const STensor3& F, const STensor3& sig, const IPAnisotropicPlasticity* q0, const IPAnisotropicPlasticity* q1, double& effPress, double& effSVM) const;
    virtual void computeRotatedAnisotropicTensor(const STensor3& F, const STensor3& DeltaEp,  const IPAnisotropicPlasticity* q0, IPAnisotropicPlasticity* q1, bool stiff) const;
    virtual void yieldFunction(std::vector<double>& yf, const STensor3& sig, const double R, const IPAnisotropicPlasticity* q0, const IPAnisotropicPlasticity* q1,
                                        bool diff=false, std::vector<STensor3>* DFDsig=NULL, std::vector<double>* DFDR=NULL,
                                        std::vector<STensor3>* DFDDeltaEp=NULL, std::vector<STensor3>* DFDdefo=NULL) const;
    virtual void getYieldNormal(std::vector<STensor3>& Np, const STensor3& sig, const double R, const IPAnisotropicPlasticity* q0, const IPAnisotropicPlasticity* q1,
                                        bool diff=false, std::vector<STensor43>* DNpDsig=NULL, std::vector<STensor3>* DNpDR=NULL,
                                        std::vector<STensor43>* DNpDDeltaEp=NULL, std::vector<STensor43>* DNpDdefo=NULL) const;
  #endif // SWIG
};



#endif //MLAWANISOTROPICPLASTICITY_H_