#ifndef ELASTIC_LCC_C
#define ELASTIC_LCC_C 1

#include "elasticlcc.h"
#include "matrix_operations.h"
#include <stdio.h>
#include <stdlib.h>


#ifdef NONLOCALGMSH
using namespace MFH;
#endif


//*************************************************************************
void malloc_lcc(ELcc* LCC, int N, double vf_i){

	LCC->Nph=N;
        LCC->v1=vf_i;
	//shear and bulk moduli of the phases in the LCC
	mallocvector(&(LCC->mu),N);
	mallocvector(&(LCC->kappa),N);

	
	//effective stiffness of the LCC and derivatives w.r.t. mu0 and mu1
	//should be generalized for more than 2 phases
	mallocmatrix(&(LCC->C),6,6);
	mallocmatrix(&(LCC->C0),6,6);
	mallocmatrix(&(LCC->C1),6,6);
	mallocmatrix(&(LCC->dCdmu0),6,6);
	mallocmatrix(&(LCC->A),6,6);

        mallocvector(&(LCC->strs_n),6);
        mallocvector(&(LCC->dstrnf),6);
        mallocvector(&(LCC->dstrn_m),6);
}

void set_elprops(ELcc* LCC, double* mu, double* kappa){
	
	if(LCC->Nph==1){
		LCC->mu[0]=mu[0];
                LCC->kappa[0]=kappa[0];
		makeiso(LCC->kappa[0],LCC->mu[0],LCC->C0);
	}

	else if(LCC->Nph==2){
		LCC->mu[0]=mu[0];
		LCC->mu[1]=mu[1];
		LCC->kappa[0]=kappa[0];
		LCC->kappa[1]=kappa[1];
		makeiso(LCC->kappa[0],LCC->mu[0],LCC->C0);
		makeiso(LCC->kappa[1],LCC->mu[1],LCC->C1);
	}
	else{
		printf("Case of LCC with more than two phase not allowed!\n");
	}
	
}


void free_lcc(ELcc* LCC){

	int Nph=LCC->Nph;

	free(LCC->mu);
	free(LCC->kappa);
	
	freematrix(LCC->C,6);
	freematrix(LCC->C0,6);
	freematrix(LCC->C1,6);
	freematrix(LCC->dCdmu0,6);
	freematrix(LCC->A,6);
	
        free(LCC->strs_n);
        free(LCC->dstrnf);
        free(LCC->dstrn_m);
}

void malloc_YF(FY2nd* YF){

	//derivatives w.r. strain
	mallocvector(&(YF->dfdstrn_m),6);
        mallocvector(&(YF->dfdstrn_c),6);
	mallocvector(&(YF->dpdstrn_m),6);
	mallocvector(&(YF->dpdstrn_c),6);
}

void free_YF(FY2nd* YF){

	free(YF->dfdstrn_m);
	free(YF->dfdstrn_c);
	free(YF->dpdstrn_m);
	free(YF->dpdstrn_c);
}


void malloc_Epresult(EPR* EpResult){

        mallocvector(&(EpResult->dnu),6);             // dCsd - dCsddstrain_m, dCsddE - dCsddstrain_c
	mallocvector(&(EpResult->dnudE),6);
        mallocvector(&(EpResult->dnudp_bar),6);

        mallocvector(&(EpResult->dp),6);
        mallocvector(&(EpResult->dpdE),6);
        mallocvector(&(EpResult->dstrsdp_bar),6);
	

	mallocmatrix(&(EpResult->Calgo),6,6);
	mallocmatrix(&(EpResult->Csd),6,6);
	mallocmatrix(&(EpResult->dCsdp_bar),6,6);

        malloctens3(&(EpResult->dCsd),6,6,6);
        malloctens3(&(EpResult->dCsddE),6,6,6);

}


void free_Epresult(EPR* EpResult){

        free(EpResult->dnu);                  // dCsd - dCsddstrain_m, dCsddE - dCsddstrain_c
	free(EpResult->dnudE);              
	free(EpResult->dnudp_bar);

        free(EpResult->dp);
        free(EpResult->dpdE);              
        free(EpResult->dstrsdp_bar);
	
	freematrix(EpResult->Calgo,6);
	freematrix(EpResult->Csd,6);
	freematrix(EpResult->dCsdp_bar,6);

        freetens3(EpResult->dCsd,6,6);
        freetens3(EpResult->dCsddE,6,6);
}


//************************************************************************
// COMPUTE MAROSTIFFNESS AND STRAIN CONCENTRATION TENSORS AND THEIR FIRST DERIVATIVES
//************************************************************************
void CA_dCdA_MT(ELcc* LCC, double v1, double **S, double **dS, double *dnu){

	int i,j;
	int error;
	double v0;
	double** diffC, **invA;
	double** mat1, **mat2;
	double** Idev, **Ivol, **I;
	double** Sdev;
	double** invC;
	double** dinvAdmu0, **dAdmu0;
	double** devA;

	double mu0,mu1;
	
	mallocmatrix(&diffC,6,6);
	mallocmatrix(&invA,6,6);
	mallocmatrix(&mat1,6,6);
	mallocmatrix(&mat2,6,6);
	mallocmatrix(&Idev,6,6);
	mallocmatrix(&Ivol,6,6);
	mallocmatrix(&I,6,6);


	mallocmatrix(&invC,6,6);
	mallocmatrix(&Sdev,6,6);
	mallocmatrix(&dinvAdmu0,6,6);
	mallocmatrix(&dAdmu0,6,6);

	mallocmatrix(&devA,6,6);

	v0 = 1.-v1;

	mu0=LCC->mu[0];
	mu1=LCC->mu[1];

	for(i=0;i<3;i++){
		Idev[i][i]=1.;
		Idev[i+3][i+3]=1.;
		I[i][i]=1.;
		I[i+3][i+3]=1.;
		for(j=0;j<3;j++){
			Ivol[i][j]=1./3.;
			Idev[i][j]=Idev[i][j]-1./3.;
		}
	}	

      //*** Compute A*******
	inverse(LCC->C0,invC,&error,6);
	contraction44(S,Idev,Sdev);
			
	contraction44(invC,LCC->C1,mat1); //mat1 = invC0:C1 - 1
	for(i=0;i<6;i++){
		mat1[i][i] -= 1.;
	}
	contraction44(S,mat1,invA);
	addtens4(1.,I, v0,invA,invA);

	inverse(invA,LCC->A,&error,6);
	

        //1st derivatives of A
	//dAdmu0:

        addtens4(0.0,I,dnu[0],dS,dS);      //dsdmu=dsdnu*dnudmu

	contraction44(dS,mat1,mat2);
	
	addtens4(-v0,mat2,v0*mu1/(mu0*mu0),Sdev,dinvAdmu0);  //warning: opposite sign
	
	contraction44(dinvAdmu0,LCC->A,mat2); 
	contraction44(LCC->A,mat2, dAdmu0);
	
	
     //***** Compute C **********
        addtens4(1.,LCC->C1,-1.,LCC->C0,diffC);
	contraction44(diffC, LCC->A,mat1);
	addtens4(1.,LCC->C0,v1,mat1,LCC->C);


        contraction44(Idev,LCC->A,devA);
	
        //1st derivatives of C
	//dCdmu0:
	contraction44(diffC, dAdmu0,mat1);
	addtens4(2.,Idev,-2.*v1,devA,LCC->dCdmu0);
	addtens4(1.,LCC->dCdmu0,v1,mat1,LCC->dCdmu0);
	
		
        freematrix(invA,6);
	freematrix(mat1,6);
	freematrix(mat2,6);
	freematrix(Idev,6);
	freematrix(Ivol,6);
	freematrix(I,6);
	freematrix(invC,6);
	freematrix(Sdev,6);
	freematrix(diffC,6);

	freematrix(devA,6);
	freematrix(dinvAdmu0,6);
	freematrix(dAdmu0,6);
	
}

#endif
