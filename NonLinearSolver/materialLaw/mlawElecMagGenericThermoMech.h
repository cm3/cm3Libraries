//
// Created by vinayak on 17.08.22.
//
// C++ Interface: material law
//              Electro-Magnetic with Generic Thermo-Mechanical law
// Author:  <Vinayak GHOLAP>, (C) 2022
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef MLAWELECMAGGENERICTHERMOMECH_H_
#define MLAWELECMAGGENERICTHERMOMECH_H_

#include "ipElecMagGenericThermoMech.h"
#include "mlawCoupledThermoMechanics.h"

class mlawElecMagGenericThermoMech: public materialLaw{
protected:
    // generic thermo-mech
    mlawCoupledThermoMechanics *_genericThermoMechLaw;

    // EM material parameters
    double _alpha; // parameter of anisotropy
    double _beta; // parameter of anisotropy
    double _gamma; // parameter of anisotropy

    // Electric conductivity directional components
    double _lx;
    double _ly;
    double _lz;
    // Initial electric scalar potential
    double _v0;
    // electric dissipation 
    double _cVoltage;
    
    // Magnetic permeability directional components
    double _mu_x;
    double _mu_y;
    double _mu_z;
    // Constants to formulate electric permittivity tensor
    // from constitutive model using invariants I_4,I_5,I_6
    // I_4 = E \otimes E : I, I_5 = E \otimes E : C^{-1}, I_6 = E \otimes E : C^{-2}
    // W_elec + W_elec-mech = 0.5 * epsilon_4 I_4 + 0.5 * epsilon_5 I_5 + 0.5 * epsilon_6 I_6
    double _epsilon_4;
    double _epsilon_5;
    double _epsilon_6;
    // Constants to formulate magnetic permeability tensor
    // from constitutive model using invariants I_7,I_8,I_9
    // I_7 = B \otimes B : I, I_8 = B \otimes B : C, I_9 = B \otimes B : C^2
    // W_mag + W_mag-mech = 0.5 * mu_7^{-1} I_7 + 0.5 * mu_8^{-1} I_8 + 0.5 * mu_9^{-1} I_9
    double _mu_7;
    double _mu_8;
    double _mu_9;
    // shear modulus due to EM fields responsive nature

    // Initial magnetic vector potential components
    double _A0_x;
    double _A0_y;
    double _A0_z;

    // Magnitude of source current density imposed in inductor
    double _js0;
    double _freqEM;       // Frequency of input current in source inductor

    STensor3 _l0;         // Electric conductivity tensor
    STensor3 _mu0;        // Magnetic permeability tensor
    STensor3 _nu0;        // Magnetic reluctivity tensor = inv(mu)

    double _seebeck;
    bool _useFluxT;        // if true thermal problem formulated using heat flux q
                           // else using thermal energy flux jy

    // designed to be used for coupled simulations
    // between EM and TM solvers with internal state data transfer
    bool _evaluateCurlField; // if true curl field and relevant variables
                            // are computed (eg ThermalEMFieldSource, fluxje etc)
                            // else imported values from other solver used

    bool _evaluateTemperature;

    bool applyReferenceF;

    // use EM stress: Elec-Mech, Mag-Mech, and Maxwell (free space) stresses
    // compute EM stress contributions to 1st PK stress and its tangents
    bool _useEMStress;
    // compute stress due to Shear modulus dependent on EM fields
    // c.f. Mehnert and Steinmann for thermo-elec-mech and thermo-mag-mech problems
    bool _EMFieldDependentShearModulus;
    // Mehnert et al 2017, Saxena et al 2013 test parameters for EM field-dependent shear modulus
    // with tanh function g(I7) = g(1+alpha_e tanh(I7/m_e)), invariant I7=B*B:I2
    // psiMagMeca = (1+alpha_e tanh(I7/m_e)) psiMeca + c1 I7 + c2 I8
    double _alpha_e; // dimensionless positive parameter for scaling
    double _m_e; // for non-dimensionalisation [T²]
    // Mehnert et al 2016, Bustamante 2010
    // with linear function g(I4) = g(1 + g1 I4/g), invariant I4=E*E:I2
    // psiElecMeca = (1 + g1 I4/g) psiMeca + c1 I4 + c2 I5
    double _g1; // elec-mech shear modulus in kPa/(kVm^-1)^2 // Mehnert 2016

    /*virtual double deformationEnergy(STensor3 defo,const SVector3 &gradV,const SVector3 &gradT,
                                     const double T, const double V) const ;*/
public:
    mlawElecMagGenericThermoMech(const int num, const bool init = true);
    mlawElecMagGenericThermoMech(const int num,
                                 const double alpha, const double beta, const double gamma,
                                 const double lx,const double ly,const double lz,
                                 const double seebeck,const double v0,
                                 const double mu_x, const double mu_y, const double mu_z,
                                 const double epsilon_4,const double epsilon_5,const double epsilon_6, // for constitutive modeling
                                 const double mu_7,const double mu_8,const double mu_9, // for constitutive modeling
                                 const double A0_x, const double A0_y, const double A0_z,
                                 const double Irms, const double freq, const unsigned int nTurnsCoil,
                                 const double coilLength_x, const double coilLength_y, const double coilLength_z,
                                 const double coilWidth, const bool useFluxT,
                                 const bool evaluateCurlField = true,
                                 const bool evaluateTemperature = true);
    const mlawCoupledThermoMechanics &getConstRefToThermoMechanicalMaterialLaw() const
    {
        if(_genericThermoMechLaw == NULL)
            Msg::Error("getConstRefToThermoMechanicalMaterialLaw: Thermo-Mechanical law is null");
        return *_genericThermoMechLaw;
    }
    mlawCoupledThermoMechanics &getRefToThermoMechanicalMaterialLaw()
    {
        if(_genericThermoMechLaw == NULL)
            Msg::Error("getRefToThermoMechanicalMaterialLaw: Thermo-Mechanical law is null");
        return *_genericThermoMechLaw;
    }
    virtual void setThermoMechanicalMaterialLaw(const materialLaw *mlaw)
    {
        if(_genericThermoMechLaw != NULL)
        {
            delete _genericThermoMechLaw;
            _genericThermoMechLaw = NULL;
        }
        _genericThermoMechLaw=dynamic_cast<mlawCoupledThermoMechanics*>(mlaw->clone());
    }
    virtual void setTime(const double ctime,const double dtime){
        materialLaw::setTime(ctime,dtime);
        if(_genericThermoMechLaw == NULL)
            Msg::Error("setTime: Thermo-Mechanic law is null");
        _genericThermoMechLaw->setTime(ctime,dtime);
    }
    virtual void setMacroSolver(const nonLinearMechSolver* sv)
    {
        materialLaw::setMacroSolver( sv);
        if(_genericThermoMechLaw == NULL)
            Msg::Error("setMacroSolver: Thermo-Mechanic law is null");
        _genericThermoMechLaw->setMacroSolver( sv);
    }
    void setApplyReferenceF(const bool rf)
    {
        applyReferenceF=rf;
    }
    void setUseEMStress(const bool emStress)
    {
        _useEMStress=emStress;
    }
    void setEMFieldDependentShearModulus(const bool EM_ShearModulus)
    {
        _EMFieldDependentShearModulus=EM_ShearModulus;
    }
    void setEMFieldDependentShearModulusTestParameters(const double alpha_e,const double m_e,const double g1)
    {
        _alpha_e = alpha_e;
        _m_e = m_e;
        _g1 = g1;
    }
    void setCVoltage(double cVoltage){   _cVoltage=cVoltage;} 




#ifndef SWIG
    virtual ~mlawElecMagGenericThermoMech()
    {
        if(_genericThermoMechLaw != NULL)
        {
            delete _genericThermoMechLaw;
            _genericThermoMechLaw = NULL;
        }
    }

    mlawElecMagGenericThermoMech(const mlawElecMagGenericThermoMech& source);
    virtual mlawElecMagGenericThermoMech& operator=(const materialLaw& source);
    virtual void initLaws(const std::map<int,materialLaw*> &maplaw){}; // this law is initialized so nothing to do
    virtual double soundSpeed() const{
        if(_genericThermoMechLaw == NULL)
            Msg::Error("soundSpeed: Thermo-Mechanic law is null");
        return _genericThermoMechLaw->soundSpeed();
    };
    virtual double density() const{
        if(_genericThermoMechLaw == NULL)
            Msg::Error("density: Thermo-Mechanic law is null");
        return _genericThermoMechLaw->density();
    };

    virtual void checkInternalState(IPVariable* ipv, const IPVariable* ipvprev) const{}; // do nothing
    virtual materialLaw* clone() const {return new mlawElecMagGenericThermoMech(*this);}
    virtual bool withEnergyDissipation() const {return false;}
    virtual matname getType() const{return materialLaw::ElecMagGenericThermoMechanics;}
    virtual void createIPState(IPStateBase* &ips,bool hasBodyForce, const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0,
                               const IntPt *GP=NULL, const int gpt =0) const;
    virtual void createIPVariable(IPElecMagGenericThermoMech* &ipv, bool hasBodyForce, const MElement *ele, const int nbFF,
                                  const IntPt *GP, const int gpt) const;

    virtual void getNormalDirection(SVector3 &normal, const IPElecMagGenericThermoMech *q1) const
    {
        normal[0] = 0.0;
        normal[1] = 0.0;
        normal[2] = -1.0;
    }

    virtual void constitutive(
            const STensor3      &F0,                   // initial deformation gradient (input @ time n)
            const STensor3      &Fn,                   // updated deformation gradient (input @ time n+1)
            STensor3            &P,                    // updated 1st Piola-Kirchhoff stress tensor (output)
            const IPVariable    *q0,                   // array of initial internal variable
            IPVariable          *q1,                   // updated array of internal variable (in ipvcur on output),
            STensor43           &Tangent,              // constitutive tangents (output)
            const bool          stiff,
            STensor43           *elasticTangent=NULL,
            const bool dTangent =false,
            STensor63* dCalgdeps = NULL
    ) const
    {
        if(_genericThermoMechLaw == NULL)
            Msg::Error("constitutive: Thermo-Mechanical law is null");
        _genericThermoMechLaw->constitutive(F0, Fn, P,
                                            &(dynamic_cast<const IPElecMagGenericThermoMech*>(q0)->getConstRefToIpThermoMech()),
                                            &(dynamic_cast<IPElecMagGenericThermoMech*>(q1)->getRefToIpThermoMech()),
                                            Tangent, stiff, elasticTangent, dTangent, dCalgdeps);
    };

    virtual void constitutive(
            const STensor3   &F0,
            const STensor3   &Fn,
            STensor3         &P,
            const STensor3   &P0,
            const IPVariable *q0,
            IPVariable       *q1,
            STensor43        &Tangent,
            const double     T0,
            double           T,
            double           V0,
            double           V,
            const SVector3   &gradT,
            const SVector3   &gradV,
            SVector3         &fluxT,
            SVector3         &fluxje,
            SVector3         &D, // Electric displacement current
            STensor3         &dPdT,
            STensor3         &dPdV,
            STensor33        &dPdE,
            STensor3         &dqdgradT,
            SVector3         &dqdT,
            STensor3         &dqdgradV,
            SVector3         &dqdV,
            STensor33        &dqdF,
            STensor3         &dedgradT,
            SVector3         &dedT,
            STensor3         &dedgradV,
            SVector3         &dedV,
            STensor33        &dedF,
            STensor3         &dDdgradT,
            SVector3         &dDdT,
            STensor3         &dDdgradV,
            SVector3         &dDdV,
            STensor33        &dDdF,
            const bool       stiff,
            double           &wth,  //related to cp*dT/dt
            double           &dwthdt,
            double           &dwthdV,
            STensor3         &dwthdF,
            double           &wV,  //related to dV/dt
            double           &dwVdt,
            double           &dwVdV,
            STensor3         &dwVdF,
            STensor3 &l10,
            STensor3 &l20,
            STensor3 &k10,
            STensor3 &k20,
            STensor3 &dl10dT,
            STensor3 &dl20dT,
            STensor3 &dk10dT,
            STensor3 &dk20dT,
            STensor3 &dl10dv,
            STensor3 &dl20dv,
            STensor3 &dk10dv,
            STensor3 &dk20dv,
            STensor43 &dl10dF,
            STensor43 &dl20dF,
            STensor43 &dk10dF,
            STensor43 &dk20dF,
            double           &mechSource,
            double           &dmechSourcedT,
            double           &dmechSourcedV,
            STensor3         &dmechSourcedF,
            SVector3         &fluxjy,
            SVector3         &djydV,
            STensor3         &djydgradV,
            SVector3         &djydT,
            STensor3         &djydgradT,
            STensor33        &djydF,
            STensor3 & djydA,
            STensor3 & djydB,
            STensor3 &djydgradVdT,
            STensor3 &djydgradVdV,
            STensor43 &djydgradVdF,
            STensor3 &djydgradTdT,
            STensor3 &djydgradTdV,
            STensor43 &djydgradTdF,
            const SVector3 & A0, // magnetic potential at time n
            const SVector3 & An, // magnetic potential at time n+1
            const SVector3 & B, // magneticinduction B = Curl A
            SVector3 & H, // magnetic field H
            SVector3 & js0, // current density applied in inductor
            SVector3 & dBdT,
            STensor3 & dBdGradT,
            SVector3 & dBdV,
            STensor3 & dBdGradV,
            STensor33 & dBdF,
            STensor3 & dBdA, // A magnetic vector potential
            STensor3 & dfluxTdA,
            STensor3 & dfluxjedA,
            STensor3 & dDdA,
            STensor3 & dfluxTdB,
            STensor3 & dfluxjedB,
            STensor3 & dDdB,
            SVector3 &dw_TdA,
            SVector3 & dw_TdB,
            SVector3 & dmechSourcedB,
            SVector3 & sourceVectorField, // Magnetic source vector field (T, grad V, F)
            SVector3 & dsourceVectorFielddt,
            double & ThermalEMFieldSource,
            double & VoltageEMFieldSource,
            STensor3 & dHdA,
            STensor3 & dHdB,
            STensor33 & dPdB,
            STensor33 & dHdF,
            STensor33 & dsourceVectorFielddF,
            SVector3 & mechFieldSource, // Electromagnetic force
            STensor3 & dmechFieldSourcedB,
            STensor33 & dmechFieldSourcedF,
            SVector3 & dHdT,
            STensor3 & dHdgradT,
            SVector3 & dHdV,
            STensor3 & dHdgradV,
            SVector3 & dsourceVectorFielddT,
            STensor3 & dsourceVectorFielddgradT,
            SVector3 & dsourceVectorFielddV,
            STensor3 & dsourceVectorFielddgradV,
            STensor3 & dsourceVectorFielddA,
            STensor3 & dsourceVectorFielddB,
            SVector3 & dmechFieldSourcedT,
            STensor3 & dmechFieldSourcedgradT,
            SVector3 & dmechFieldSourcedV,
            STensor3 & dmechFieldSourcedgradV,
            SVector3 & dThermalEMFieldSourcedA,
            SVector3 & dThermalEMFieldSourcedB,
            STensor3 & dThermalEMFieldSourcedF,
            double & dThermalEMFieldSourcedT,
            SVector3 & dThermalEMFieldSourcedGradT,
            double & dThermalEMFieldSourcedV,
            SVector3 & dThermalEMFieldSourcedGradV,
            SVector3 & dVoltageEMFieldSourcedA,
            SVector3 & dVoltageEMFieldSourcedB,
            STensor3 & dVoltageEMFieldSourcedF,
            double & dVoltageEMFieldSourcedT,
            SVector3 & dVoltageEMFieldSourcedGradT,
            double & dVoltageEMFieldSourcedV,
            SVector3 & dVoltageEMFieldSourcedGradV,
            STensor43 *elasticTangent=NULL
    ) const;

    double getInitialTemperature() const {
        if(_genericThermoMechLaw == NULL)
            Msg::Error("getInitialTemperature: Thermo-Mechanic law is null");
        return getConstRefToThermoMechanicalMaterialLaw().getInitialTemperature();
    }
    virtual const STensor3&  getConductivityTensor() const {
        if(_genericThermoMechLaw == NULL)
            Msg::Error("getConductivityTensor: Thermo-Mechanic law is null");
        return getConstRefToThermoMechanicalMaterialLaw().getConductivityTensor();
    }
    virtual const STensor3& getAlphaDilatation() const {
        if(_genericThermoMechLaw == NULL)
            Msg::Error("getAlphaDilatation: Thermo-Mechanic law is null");
        return getConstRefToThermoMechanicalMaterialLaw().getAlphaDilatation();
    }

    virtual void ElasticStiffness(STensor43 *elasticStiffness) const{
        if(_genericThermoMechLaw == NULL)
            Msg::Error("getAlphaDilatation: Thermo-Mechanic law is null");
        return getConstRefToThermoMechanicalMaterialLaw().ElasticStiffness(elasticStiffness);
    }
    virtual double getInitialExtraDofStoredEnergyPerUnitField() const {
        if(_genericThermoMechLaw == NULL)
            Msg::Error("getInitialExtraDofStoredEnergyPerUnitField: Thermo-Mechanic law is null");
        return getConstRefToThermoMechanicalMaterialLaw().getInitialExtraDofStoredEnergyPerUnitField();
    }
    virtual double getExtraDofStoredEnergyPerUnitField(double T) const {
        if(_genericThermoMechLaw == NULL)
            Msg::Error("getExtraDofStoredEnergyPerUnitField: Thermo-Mechanic law is null");
        return getConstRefToThermoMechanicalMaterialLaw().getExtraDofStoredEnergyPerUnitField(T);
    }

    double getInitialVoltage() const {return _v0;}
    SVector3 getInitialMagPotential() const {return SVector3(_A0_x, _A0_y, _A0_z);}
    virtual bool isInductor() const {return false;}

#endif //SWIG
};

#endif //MLAWELECMAGGENERICTHERMOMECH_H_
