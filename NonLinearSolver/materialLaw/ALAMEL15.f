        SUBROUTINE umat_cp(STRESS,STATEV,DDSDDE,SSE,SPD,SCD,
     &        RPL,DDSDDT,DRPLDE,DRPLDT,
     &        STRAN,DSTRAN,TIM,DTIME,TEMP,DTEMP,PREDEF,DPRED, CMNAME,
     &        NDI,NSHR,NTENS,NSTATV,PROPS,NPROPS,COORDS,DROT,PNEWDT,
     &        CELENT,DFGRD0,DFGRD1,NOEL,NPT,LAYER,KSPT,KSTEP,KINC)
C
C        INCLUDE 'ABA_PARAM.INC'
        implicit none
c        ===================================================
        include 'LD_param_size.f'
c        ===================================================
c        =====  ARGUMENTS:
        integer ntens, nstatv, nprops
        CHARACTER*8 CMNAME
        REAL*8 STRESS(NTENS),STATEV(NSTATV),
     &        DDSDDE(NTENS,NTENS),DDSDDT(NTENS),DRPLDE(NTENS),
     &        STRAN(NTENS),DSTRAN(NTENS),TIM(2),PREDEF(1),DPRED(1),
     &        PROPS(NPROPS),COORDS(3),DROT(3,3),
     &        DFGRD0(3,3),DFGRD1(3,3)
        real*8 SSE, SPD, SCD, RPL, DTIME, TEMP, CELENT, DRPLDT
        real*8 PNEWDT, DTEMP
        integer NDI, NSHR, NOEL, NPT, LAYER, KSPT, KSTEP, KINC
C
c        ===================================================
c        =====  'COMMON BLOCKS:'
c        ::::  'Constants'
        real*8 sq2, C1, C2, C3, C4, C12
        integer*4 i6(3,3), i3(3,3)
        common /KcstLD/ sq2, C1, C2, C3, C4, C12, i6
c        ::::  '1st set of material parameters'
        real*8 gam0dt(mxmods,mxcstt), expo0(mxcstt), Sadim
        real*8 Drlxmod(6,mxrlx), Wrlxmod(3,mxrlx), ellps_ax(3)
        integer*4 rlxmod(3,2*mxrlx), nbrlx(3), ncstt, nneb00
        integer ncrys1, nelt, ngauss
        common /KrlxLD/ gam0dt, expo0, Sadim, Drlxmod, Wrlxmod
     &                        , ellps_ax, ncrys1, nelt, ngauss
     &                        , rlxmod, nbrlx, ncstt, nneb00
c        ::::  Hardening parameters
        real*8 hard_p(mxHP,mxmods,mxcstt), hard_M(mxs2,mxs2)
        integer*4 IDhard(mxmods,mxcstt)
        common /KhardLD/ hard_p, hard_M, IDhard
c        ::::  Kinematic hardening
        real*8 AB(2,mxcstt)
        logical kinH(mxcstt)
        common /KkinHLD/ AB, kinH
c        ::::  'Slip system geometry'
        real*8 A1(6,mxslip,mxcstt), B1(3,mxslip,mxcstt)
        real*8 miller(7,mxslip,mxcstt)
        common /KLP0/ A1, B1, miller
c        ::::  'variables used to read texture and for debug:'
        real*8 ebpmx
        integer NOEL1, NPT1, KKSTEP, KKINC, ELT1, submx
        common /tempLD/ ebpmx, submx
     &                        , NOEL1, NPT1, KKSTEP, KKINC, ELT1
c        ::::  'Other material parameters'
        real*8 Emod(6,6,mxcstt), Ecpl(6,6,mxcstt)
        real*8 twinS(mxmods,mxcstt)
        integer*4 nslip(mxmods,mxcstt), nmods(mxcstt)
        integer*4 nslipT(mxcstt), idtwin(3,mxmods,mxcstt)
        logical incrH(mxcstt), latentH(mxcstt)
        common /KmatLD/ Emod, Ecpl, twinS, idtwin, incrH
     &                        , latentH, nslip, nmods, nslipT
        integer icrys, ntau(mxcstt)
c        ===================================================
c        =====  'LOCAL VARIABLES:'
c        ::::  'Integration of the material law:'
c        ::::  'variables used in the loop over crystals'
c        ::::  '(mxneb is the maximum number of crystals within a cluster)'
        real*8 q_cr(4,mxneb), S_cr(6,mxneb), MF0(mxneb), MF(mxneb)
        real*8 D_sa(6), S_cr_b(6,mxneb), D_cr(6,mxneb), W_cr(3,mxneb)
        real*8 NR(mxneq,mxneq), rho(mxs6,mxmods,mxneb), avgMF(mxmods)
        real*8 gam(mxslip,mxneb), muBn(mxslip,mxneb)
        real*8 gam0(mxmods,mxcstt), dScrdE(6,6,mxneb), S_cr0(6,mxneb)
        real*8 weight(mxneb), v_twin(13,mxneb), tau_c(mxslip,mxneb)
        real*8 v_trf(mxneb), S_trf(6,mxneb), rho_trf(mxneb), isq3
        real*8 sumgam(mxmods,mxneb), M_tc(mxmods,mxmods,mxneb)
        real*8 q_spin(4), q2(4), expo(mxcstt), dMFmx(mxcstt)
        real*8 q(4), q_cr0(4,mxneb), R_sa2cr(3,3,mxneb), R_cr2sa(3,3)
        real*8 R_rlx(3,3), qlamel(4), n_alam(3)
        real*8 Brlx(3,mmxrlx), Arlx(6,mmxrlx), gamrlx(mmxrlx)
        real*8 dSdE_sa(6,6), Savg_sa(6), SH0, dSH(6), dEH, EH, wgt_tot
        real*8 Strain1(6), SH, Savg_b(6), Savg_b1(6)
        integer*4 ineb, itw, ntw, id_trf(4,mxneb)
c        ::::  'Lattice spin effects'
        real*8 dWdE(6,3,mxneb), invDF(3,3), dsdw(6,3)
        real*8 dgxdE_cr(6,mxrlx,mxneb), dgxdE(6,mxrlx)
        real*8 dEcdEs(6,6), dmatdw(3,3,3,mxneb), W_sa(3), W_sa0(3)
        logical first, RotD
c        ::::  'Other variables'
        character*8 word
        real*8 vec6(6), mat66(6,6), avgrlx(3)
        real*8 angle, si, Karc, BB1(6), ebp, vonmises
        real*8 tmp, tmp2, tmp3, statev1(1), v2(6), v_min, k_2(mxmods)
        real*8 taucmoy, Vtw, Vtot, avgDp(6), phi(3), ntb, avtb, avtc
        integer incr, ncrys, nvar, i1, k, k1, ii, istat1
        integer istat, istat2(mxneb), istat3, istat4, istat5, d_istat
        integer*4 imods, imods2, islip, islip2, ineb2
        integer*4 icstt(mxneb), kcstt, isub, nsub, nebj, nneb0, nneb1
        integer*4 i, j, m, n, l, jj, nbtwin(mxcstt)
        integer*4 krlx, irlx, nrlx, nrlx1, nebrlx(2,7,mxneb), nneb
        logical check, onlyjac, last, chkpr, cpfem, chkderiv, guerric
        logical xpr, polyX, report_slip, newinc, jeroen, defconfig, fx
        logical backstress, zinov
        real*8 ERRE, ERRS, avD, nD
        logical Augm_L, FFT
        common /K_augm_L/ ERRE, ERRS, Augm_L
        integer idd
        common /K_idd/ idd
        real*8 TaylFac, fpi, co20, si20, co30
c        ::::variables needed for the phenomenological model
        real*8 phen_Taylor, phen_k2, phen_A
c

        return
        end
c
c
c        *******************************************************
c        *** Print out texture
c        *******************************************************
        subroutine textOut( statev, stress, nstatv )
        implicit none
        include 'LD_param_size.f'
        integer nstatv
        real*8 statev(nstatv), stress(6)
c        =====  COMMON BLOCKS:
c        ::::  Constants
        real*8 sq2, C1, C2, C3, C4, C12
        integer*4 i6(3,3)
        common /KcstLD/ sq2, C1, C2, C3, C4, C12, i6
c        ::::  1st set of material parameters
        real*8 gam0dt(mxmods,mxcstt), expo0(mxcstt), Sadim
        real*8 Drlxmod(6,mxrlx), Wrlxmod(3,mxrlx), ellps_ax(3)
        integer*4 rlxmod(3,2*mxrlx), nbrlx(3), ncstt, nneb
        integer ncrys1, nelt, ngauss
        common /KrlxLD/ gam0dt, expo0, Sadim, Drlxmod, Wrlxmod
     &                        , ellps_ax, ncrys1, nelt, ngauss
     &                        , rlxmod, nbrlx, ncstt, nneb
c        ::::  'Slip system geometry'
        real*8 A1(6,mxslip,mxcstt), B1(3,mxslip,mxcstt)
        real*8 miller(7,mxslip,mxcstt)
        common /KLP0/ A1, B1, miller
c        ::::  Other material parameters
        real*8 Emod(6,6,mxcstt), Ecpl(6,6,mxcstt)
        real*8 twinS(mxmods,mxcstt)
        integer*4 nslip(mxmods,mxcstt), nmods(mxcstt)
        integer*4 nslipT(mxcstt), idtwin(3,mxmods,mxcstt)
        logical incrH(mxcstt), latentH(mxcstt)
        common /KmatLD/ Emod, Ecpl, twinS, idtwin, incrH
     &                        , latentH, nslip, nmods, nslipT
c        ::::  'Kinematic hardening'
        real*8 AB(2,mxcstt)
        logical kinH(mxcstt)
        common /KkinHLD/ AB, kinH
        integer ineb, icrys
c        ====  variables used for printing output
        real*8 phi(3), q(4,mxneb), alamel(mxneb), qlamel(4), MF(mxneb)
        real*8 w_rlx(3,mxrlx,mxneb), Arlx(6,mxrlx,mxneb), avgMF
        real*8 S_cr(6,mxneb), vec6(6), avgst5, TaylFac(mxneb)
        real*8 expo(mxcstt), isq3, v_twin(13,mxneb), Vtw, Vtot, v_min
        real*8 nn(3), q2(4), weight(mxneb), F0(3,3), tmp
        real*8 tau_c(mxmods,mxneb), sumgam(mxmods,mxneb), avSG(mxmods)
        real*8 eps_el(6), R(3,3)
        integer*4 i, j, i1, jj
        integer*4 krlx, irlx, nrlx, kcstt, icstt(mxneb), imods
        integer istat3, istat2(mxneb), ntw, k, ncrys, n_trf, nstep
        integer id(13), ii
        logical polyX


        return
        end
c
c        include 'Kevpm15.f'
c        include 'Ksub15.f'
c        include 'LD_utils.f'
c        include 'ShearB.f'
