//
// C++ Interface: material law
//
// Description: Define Gurson coalescence models to be used inside the Gurson law
//
//
// Author:  <J. Leclerc>, (C) 2017
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "CoalescenceLaw.h"
#include "ipNonLocalPorosity.h"


// CoalescenceLaw
// -------------------------------------------
CoalescenceLaw::CoalescenceLaw(const int num, const bool init): _num(num), _initialized(init)
{

};

CoalescenceLaw::CoalescenceLaw(const CoalescenceLaw &source)
{
  _num = source._num;
  _initialized = source._initialized;
};


// NoCoalescenceLaw
// -------------------------------------------
NoCoalescenceLaw::NoCoalescenceLaw(const int num) : CoalescenceLaw(num,true)
{

};

NoCoalescenceLaw::NoCoalescenceLaw(const NoCoalescenceLaw &source) : CoalescenceLaw(source)
{

};

CoalescenceLaw* NoCoalescenceLaw::clone() const
{
  return new NoCoalescenceLaw(*this);
};

void NoCoalescenceLaw::createIPVariable(IPCoalescence* &ipv) const
{
  if(ipv != NULL) delete ipv;
  ipv = new IPNoCoalescence();
};

void NoCoalescenceLaw::computeConcentrationFactor(const  IPNonLocalPorosity* q0Porous, const  IPNonLocalPorosity* q1Porous, 
                                              double& CfT, bool stiff, double* DCfTDX, double* DCfTDW) const 
{
  CfT = 1.;
  if (stiff)
  {
    *DCfTDX = 0.;
    *DCfTDW = 0.;
  }
}


// FstarCoalescenceLaw
// -------------------------------------------
FstarCoalescenceLaw::FstarCoalescenceLaw(const int num, const double fC, const double r) :
  CoalescenceLaw(num,true),_acceleratedRate(r), _fC(fC)
{

};


FstarCoalescenceLaw::FstarCoalescenceLaw(const FstarCoalescenceLaw &source) : CoalescenceLaw(source),
  _acceleratedRate(source._acceleratedRate), _fC(source._fC)
{

};

CoalescenceLaw* FstarCoalescenceLaw::clone() const
{
  return new FstarCoalescenceLaw(*this);
};

void FstarCoalescenceLaw::createIPVariable(IPCoalescence* &ipv) const
{
  if(ipv != NULL) delete ipv;
  ipv = new IPFstarCoalescence();
};



void FstarCoalescenceLaw::checkCoalescenceGTN(const IPNonLocalPorosity* q0Porous,  IPNonLocalPorosity* q1Porous) const
{
  // _fC is set by user
  // when computing, coalescene occurs when fV> _fC
  // current fV is used as true onset value (not _fC)


  const  IPCoalescence* q0Coal = &(q0Porous->getConstRefToIPCoalescence());
  IPCoalescence* q1Coal = &(q1Porous->getRefToIPCoalescence());
  double fV = q1Porous->getYieldPorosity();
  // Checking...
  if (!q0Coal->getCoalescenceOnsetFlag())
  {
    // If coalescence never met before
    if ((fV >= _fC) and q1Porous->dissipationIsActive())
    {
      // Coalescence occurs for the first time ==> rate and onset values are modified
      q1Coal->getRefToCoalescenceOnsetFlag() = true;
      q1Coal->getRefToCoalescenceActiveFlag() = true; // active 
      q1Coal->setIPvAtCoalescenceOnset(*q1Porous);
      q1Coal->getRefToAccelerateRate() = _acceleratedRate;
    }
    else
    {
      // Still no coalescence ==> copy-paste default values
      q1Coal->getRefToCoalescenceOnsetFlag() = false;
      q1Coal->getRefToCoalescenceActiveFlag() = false;
      q1Coal->deleteIPvAtCoalescenceOnset();
      q1Coal->getRefToAccelerateRate() = 1.;
    }
  }
  else
  {
    // If coalescence already met before: copy onset values
    q1Coal->getRefToCoalescenceOnsetFlag() = true;
    q1Coal->setIPvAtCoalescenceOnset(*q0Coal->getIPvAtCoalescenceOnset());
    if ((fV > q0Coal->getIPvAtCoalescenceOnset()->getYieldPorosity()) and q1Porous->dissipationIsActive())
    {
      // if fV increase and active plasticity
      q1Coal->getRefToAccelerateRate() = _acceleratedRate;
      q1Coal->getRefToCoalescenceActiveFlag() = true;
    }
    else
    {
      // unloading
      q1Coal->getRefToAccelerateRate() = 1.;
      q1Coal->getRefToCoalescenceActiveFlag() = false;
    }
  }
};

void FstarCoalescenceLaw::checkCoalescenceGTNWithNormal(const SVector3& normal, const IPNonLocalPorosity* q0Porous, IPNonLocalPorosity* q1Porous) const
{
  checkCoalescenceGTN(q0Porous,q1Porous);
}


void FstarCoalescenceLaw::forceCoalescenceGTN(const IPNonLocalPorosity* q0Porous, IPNonLocalPorosity* q1Porous) const
{
  // force coalescence without condition
  IPCoalescence* q1Coal = &(q1Porous->getRefToIPCoalescence());
  // Coalescence occurs for the first time ==> rate and onset values are modified
  q1Coal->getRefToCoalescenceOnsetFlag() = true;
  q1Coal->getRefToCoalescenceActiveFlag() = true;
  q1Coal->setIPvAtCoalescenceOnset(*q1Porous);
  q1Coal->getRefToAccelerateRate() = _acceleratedRate;
};


LoadConcentrationFactorBasedCoalescenceLaw::LoadConcentrationFactorBasedCoalescenceLaw(const int num, const double R):
    CoalescenceLaw(num,true),_acceleratedRate(R)
{};
LoadConcentrationFactorBasedCoalescenceLaw::LoadConcentrationFactorBasedCoalescenceLaw(const LoadConcentrationFactorBasedCoalescenceLaw& src):
   CoalescenceLaw(src),_acceleratedRate(src._acceleratedRate)
{}

void LoadConcentrationFactorBasedCoalescenceLaw::checkCoalescenceGTNWithNormal(const SVector3& norm, const IPNonLocalPorosity* q0Porous, IPNonLocalPorosity* q1Porous) const
{  
  // Get IPV
  const IPCoalescence* q0Thom = &(q0Porous->getConstRefToIPCoalescence());
  IPCoalescence* q1Thom = &(q1Porous->getRefToIPCoalescence());

  double fV = q1Porous->getYieldPorosity();    
  if (!q0Thom->getCoalescenceOnsetFlag())
  {
    // if coalescence never met before
    /* replace by kirchoof stress*/
    const STensor3& corKir = q1Porous->getConstRefToCorotationalKirchhoffStress();
    double tauY = q1Porous->getCurrentViscoplasticYieldStress(); // include rate effects
    // concentration factor
    // computeConcentrationFactor(q1Thom,false);
    double CTf;
    this->computeConcentrationFactor(q0Porous,q1Porous,CTf);
    double sigmax = 0.;
    for (int i=0; i<3; i++){
      for (int j=0; j<3; j++){
        sigmax += norm(i)*corKir(i,j)*norm(j);
      }
    }


    if ((sigmax >= CTf*tauY) and q1Porous->dissipationIsActive())
    {
      // Coalescence occurs for the first time ==> rate and onset values are modified
      q1Thom->getRefToCoalescenceOnsetFlag() = true;
      q1Thom->getRefToCoalescenceActiveFlag() = true;
      q1Thom->setIPvAtCoalescenceOnset(*q1Porous);
      q1Thom->getRefToAccelerateRate() = _acceleratedRate;
    }
    else
    {
      // Still no coalescence ==> copy-paste default values
      q1Thom->getRefToCoalescenceOnsetFlag() = false;
      q1Thom->getRefToCoalescenceActiveFlag() = false;
      q1Thom->deleteIPvAtCoalescenceOnset();
      q1Thom->getRefToAccelerateRate() = 1.;
    }
  }
  else
  {
    // If coalescence already met before: copy onset values
    q1Thom->getRefToCoalescenceOnsetFlag() = true;
    q1Thom->setIPvAtCoalescenceOnset(*q0Thom->getIPvAtCoalescenceOnset());
    
    // Then check if there will be coalescence now
    if ((fV > q0Thom->getIPvAtCoalescenceOnset()->getYieldPorosity()) and q1Porous->dissipationIsActive())
    {
      // if fV increase and active plasticity
      q1Thom->getRefToAccelerateRate() = _acceleratedRate;
      q1Thom->getRefToCoalescenceActiveFlag() = true;
    }
    else
    {
      // unloading
      q1Thom->getRefToAccelerateRate() = 1;
      q1Thom->getRefToCoalescenceActiveFlag() = false;
    }
  }
};

void LoadConcentrationFactorBasedCoalescenceLaw::checkCoalescenceGTN(const IPNonLocalPorosity* q0Porous, IPNonLocalPorosity* q1Porous) const
{
  // Get IPV
  const IPCoalescence* q0Thom = &(q0Porous->getConstRefToIPCoalescence());
  IPCoalescence* q1Thom = &(q1Porous->getRefToIPCoalescence());

  double fV = q1Porous->getYieldPorosity();    
  if (!q0Thom->getCoalescenceOnsetFlag())
  {
    // if coalescence never met before
    /* replace by kirchoof stress*/
    const STensor3& corKir = q1Porous->getConstRefToCorotationalKirchhoffStress();
    double tauY = q1Porous->getCurrentViscoplasticYieldStress(); // include rate effects
    // concentration factor
    // computeConcentrationFactor(q1Thom,false);
    double CTf;
    this->computeConcentrationFactor(q0Porous,q1Porous,CTf);
    // maximal eigen value
    static fullMatrix<double> eigVec(3,3);
    static fullVector<double> eigVal(3);
    corKir.eig(eigVec,eigVal,true);
    double sigmax = std::max(eigVal(2),std::max(eigVal(0),eigVal(1)));

    if ((sigmax >= CTf*tauY) and q1Porous->dissipationIsActive())
    {
      // Coalescence occurs for the first time ==> rate and onset values are modified
      q1Thom->getRefToCoalescenceOnsetFlag() = true;
      q1Thom->getRefToCoalescenceActiveFlag() = true;
      q1Thom->setIPvAtCoalescenceOnset(*q1Porous);
      q1Thom->getRefToAccelerateRate() = _acceleratedRate;
    }
    else
    {
      // Still no coalescence ==> copy-paste default values
      q1Thom->getRefToCoalescenceOnsetFlag() = false;
      q1Thom->getRefToCoalescenceActiveFlag() = false;
      q1Thom->deleteIPvAtCoalescenceOnset();
      q1Thom->getRefToAccelerateRate() = 1.;
    }
  }
  else
  {
    // If coalescence already met before: copy onset values
    q1Thom->getRefToCoalescenceOnsetFlag() = true;
    q1Thom->setIPvAtCoalescenceOnset(*q0Thom->getIPvAtCoalescenceOnset());
    
    // Then check if there will be coalescence now
    if ((fV > q0Thom->getIPvAtCoalescenceOnset()->getYieldPorosity()) and q1Porous->dissipationIsActive())
    {
      // if fV increase and active plasticity
      q1Thom->getRefToAccelerateRate() = _acceleratedRate;
      q1Thom->getRefToCoalescenceActiveFlag() = true;
    }
    else
    {
      // unloading
      q1Thom->getRefToAccelerateRate() = 1;
      q1Thom->getRefToCoalescenceActiveFlag() = false;
    }
  }
};


void LoadConcentrationFactorBasedCoalescenceLaw::forceCoalescenceGTN(const IPNonLocalPorosity* q0Porous, IPNonLocalPorosity* q1Porous) const 
{

  double fV = q1Porous->getYieldPorosity();
  // Coalescence occurs for the first time ==> rate and onset values are modified
    // get thomason ip
  IPCoalescence* q1Thom = &(q1Porous->getRefToIPCoalescence());
    // set onset values
  q1Thom->getRefToCoalescenceOnsetFlag() = true;
  q1Thom->getRefToCoalescenceActiveFlag() = true;
  q1Thom->setIPvAtCoalescenceOnset(*q1Porous);
    // set rate values
  q1Thom->getRefToAccelerateRate() = _acceleratedRate;
};

ThomasonCoalescenceLaw::ThomasonCoalescenceLaw(const int num, const double R): 
      LoadConcentrationFactorBasedCoalescenceLaw(num,R),_alpha(0.1),_beta(1.24)
{
  
};
//
ThomasonCoalescenceLaw::ThomasonCoalescenceLaw(const ThomasonCoalescenceLaw& src): 
              LoadConcentrationFactorBasedCoalescenceLaw(src), _alpha(src._alpha), _beta(src._beta)
{
};

ThomasonCoalescenceLaw::~ThomasonCoalescenceLaw(){
};

void ThomasonCoalescenceLaw::createIPVariable(IPCoalescence* &qCoal) const
{
  if(qCoal != NULL) delete qCoal;
  qCoal = new IPLoadConcentrationFactorBasedCoalescence();
};

void ThomasonCoalescenceLaw::setThomasonCoefficient(const double a, const double b){
  Msg::Info("modifying alpha = %e beta = %e to alpha = %e beta = %e",_alpha,_beta,a,b);
  _alpha = a;
  _beta = b;
};

void ThomasonCoalescenceLaw::computeConcentrationFactor(const  IPNonLocalPorosity* q0Porous, const  IPNonLocalPorosity* q1Porous, 
                                              double& CfT, bool stiff, double* DCfTDX, double* DCfTDW) const
{
  double X = q1Porous->getConstRefToIPVoidState().getVoidLigamentRatio();
  double W = q1Porous->getConstRefToIPVoidState().getVoidAspectRatio();
  // Get variables
  if (X < 1e-8){
    // Correction to avoid division by zero
    X = 1e-8;
  }
  if (W < 1e-8){
    // Correction to avoid division by zero
    W = 1e-8;
  }

  // Compute Cft = C1*C2
  double XSq = X*X;
  double WSq = W*W;

  double C1 = (1.-XSq);
  double C2 = _alpha*(1.-X)*(1.-X)/(XSq*WSq)+ _beta/sqrt(X);

  CfT = C1*C2;

  // Compute derivatives
  if(stiff)
  {
    double invX = 1./X;
    double DC2DX = (_alpha/WSq)*2.*(invX -1)*(-invX*invX) - 0.5*_beta/(X*sqrt(X));
    (*DCfTDX) = -2.*X*C2 + C1*DC2DX;

    double DC2DW = _alpha*(invX-1)*(invX-1)*(-2./WSq/W);
    (*DCfTDW) = C1*DC2DW;
  }
};



simpleShearCoalescenceLaw::simpleShearCoalescenceLaw(const int num, const double R): 
    LoadConcentrationFactorBasedCoalescenceLaw(num,R),_gamma(1.),_exponent(1.){
};
//
simpleShearCoalescenceLaw::simpleShearCoalescenceLaw(const simpleShearCoalescenceLaw& src): LoadConcentrationFactorBasedCoalescenceLaw(src),
_gamma(src._gamma),_exponent(src._exponent) {
};

void simpleShearCoalescenceLaw::setShearFactor(double g) {
  _gamma = g;
  Msg::Info("set shear factor in simpleShearCoalescenceLaw = %e",g);
};

void simpleShearCoalescenceLaw::setVoidLigamentExponent(double e) {
  _exponent = e;
  Msg::Info("set void ligament exponent in simpleShearCoalescenceLaw = %e",e);
};


void simpleShearCoalescenceLaw::createIPVariable(IPCoalescence* &qCoal) const
{
  if(qCoal != NULL) delete qCoal;
  qCoal = new IPLoadConcentrationFactorBasedCoalescence();
};

void simpleShearCoalescenceLaw::computeConcentrationFactor(const  IPNonLocalPorosity* q0Porous, const  IPNonLocalPorosity* q1Porous, 
                                              double& CfT, bool stiff, double* DCfTDX, double* DCfTDW) const
{
  double X = q1Porous->getConstRefToIPVoidState().getVoidLigamentRatio();
  double W = q1Porous->getConstRefToIPVoidState().getVoidAspectRatio();
  
  if (X < 1e-8) X = 1e-8;
  double Xeff = pow(X,_exponent);
  CfT =_gamma*(1.-Xeff*Xeff);

  // Compute derivatives
  if(stiff)
  {
    double DXeffDX = _exponent*pow(X,_exponent-1.);
    (*DCfTDX) = -2.*_gamma*Xeff*DXeffDX;
    (*DCfTDW) = 0.;
  }
};


BenzergaLeblondCoalescenceLaw::BenzergaLeblondCoalescenceLaw(const int num, const double R): 
    LoadConcentrationFactorBasedCoalescenceLaw(num,R){
};
//
BenzergaLeblondCoalescenceLaw::BenzergaLeblondCoalescenceLaw(const BenzergaLeblondCoalescenceLaw& src): LoadConcentrationFactorBasedCoalescenceLaw(src) {
};


void BenzergaLeblondCoalescenceLaw::createIPVariable(IPCoalescence* &qCoal) const
{
  if(qCoal != NULL) delete qCoal;
  qCoal = new IPLoadConcentrationFactorBasedCoalescence();
};

void BenzergaLeblondCoalescenceLaw::computeConcentrationFactor(const  IPNonLocalPorosity* q0Porous, const  IPNonLocalPorosity* q1Porous, 
                                              double& CfT, bool stiff, double* DCfTDX, double* DCfTDW) const
{
  double X = q1Porous->getConstRefToIPVoidState().getVoidLigamentRatio();
  double W = q1Porous->getConstRefToIPVoidState().getVoidAspectRatio();
  if (W < 1e-8)
  {
    W = 1e-8;
  };

  double X2 = X*X;
  double X4 = X2*X2;
  double a = sqrt(1.+3.*X4);
  double fact = (1.+a)/(3.*X2);
  double Cvol = (2.-a + log(fact))/sqrt(3.);
  double Csur =  (X2*X - 3.*X+2.)/(3.*sqrt(3.)*X*W);
  CfT = Cvol+Csur;

  // Compute derivatives
  if(stiff)
  {
    double DaDX = (6.*X2*X)/a;
    double DfactDX = DaDX/(3*X2) - (1+a)*2./(3.*X2*X);
    (*DCfTDX) = (-DaDX + DfactDX/fact)/sqrt(3.)+(2.*X - 2./X2)/(3.*sqrt(3.)*W);
    (*DCfTDW) = -Csur/W;
  }
};