//
// C++ Interface: material law
//
// Description: Define Gurson coalescence models to be used inside the Gurson law
//
//
// Author:  <J. Leclerc>, (C) 2017
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef _COALESCENCELAW_H_
#define _COALESCENCELAW_H_
#ifndef SWIG
#include "ipstate.h"
#include "ipCoalescence.h"
#include "STensor43.h"
#include "STensor63.h"
#include "scalarFunction.h"
#include "ipVoidState.h"
class IPNonLocalPorosity;
#endif // SWIG

class CoalescenceLaw
{
  // Basic class
  public:
    enum coalescenceType{NoCoalescence, FstarCoalescence, ThomasonCoalescence,BenzergaLeblondCoalescence};

  protected:
    int _num; // number of law
    bool _initialized; // to initialize law


  public:
    #ifndef SWIG
    // Constructors & destructor
    CoalescenceLaw(const int num, const bool init=true);
    virtual ~CoalescenceLaw(){};
    CoalescenceLaw(const CoalescenceLaw &source);

    // Standard functions
    int getNum() const{return _num;}
    bool isInitialized() const {return _initialized;}

    virtual coalescenceType getType() const=0;
    virtual CoalescenceLaw* clone() const=0;
    
    virtual void initPorousInternalLaw(){_initialized = true;};
    
    // Specific functions
      // Initialise variables for ipv
    virtual void createIPVariable(IPCoalescence* &ipv) const=0;
      // Check if coalescence criterion is satisfied at the end of the plastic update 
    virtual void checkCoalescenceGTN(const IPNonLocalPorosity* q0Porous, IPNonLocalPorosity* q1Porous) const = 0;
    virtual void checkCoalescenceGTNWithNormal(const SVector3& normal, const IPNonLocalPorosity* q0Porous, IPNonLocalPorosity* q1Porous) const = 0;
      // at the end of an iteration or a converged step (not used somewhere else !)
    virtual void forceCoalescenceGTN(const IPNonLocalPorosity* q0Porous, IPNonLocalPorosity* q1Porous) const = 0;
    
    // compute load concentration factor following Thomason criterion
    virtual void computeConcentrationFactor(const  IPNonLocalPorosity* q0Porous, const  IPNonLocalPorosity* q1Porous, 
                                              double& CfT, bool stiff=false, double* DCfTDX=NULL, double* DCfTDW=NULL) const = 0;
    virtual void printInfo() const = 0;
    #endif // SWIG
};



class NoCoalescenceLaw : public CoalescenceLaw
{
  // Default coalescence law (nothing happens)
  public:
    // Constructors & destructor
    NoCoalescenceLaw(const int num);
    #ifndef SWIG
    virtual ~NoCoalescenceLaw(){};
    NoCoalescenceLaw(const NoCoalescenceLaw &source);

    // Standard functions
    virtual coalescenceType getType() const {return CoalescenceLaw::NoCoalescence;};
    virtual CoalescenceLaw* clone() const;

    // Specific functions
    virtual void createIPVariable(IPCoalescence* &qCoal) const;
    virtual void checkCoalescenceGTN(const IPNonLocalPorosity* q0Porous, IPNonLocalPorosity* q1Porous) const {};
    virtual void checkCoalescenceGTNWithNormal(const SVector3& normal, const IPNonLocalPorosity* q0Porous, IPNonLocalPorosity* q1Porous) const{}
    virtual void forceCoalescenceGTN(const IPNonLocalPorosity* q0Porous, IPNonLocalPorosity* q1Porous) const {};
    virtual void computeConcentrationFactor(const  IPNonLocalPorosity* q0Porous, const  IPNonLocalPorosity* q1Porous, 
                                              double& CfT, bool stiff=false, double* DCfTDX=NULL, double* DCfTDW=NULL) const ;
    virtual void printInfo() const {Msg::Info("using NoCoalescenceLaw");};                                          
    #endif // SWIG
};


class FstarCoalescenceLaw : public CoalescenceLaw
{
  // Approach using an effective porosity growth f_V^star with an accelerate growth rate
  protected:
    // Phenomenological coalescence - Acceleration rate
    double _acceleratedRate;  // Acceleration rate after satifying the criterion
    double _fC;               // Critical porosity at coalescencenset of accelerate onset

  public:
    // Constructors & destructor
    FstarCoalescenceLaw(const int num, const double fC, const double R);
    #ifndef SWIG
    virtual ~FstarCoalescenceLaw(){};
    FstarCoalescenceLaw(const FstarCoalescenceLaw &source);

    // Standard functions
    virtual coalescenceType getType() const {return CoalescenceLaw::FstarCoalescence;};
    virtual CoalescenceLaw* clone() const;

    // Specific functions
    virtual void createIPVariable(IPCoalescence* &qCoal) const;
    virtual void checkCoalescenceGTN(const IPNonLocalPorosity* q0Porous, IPNonLocalPorosity* q1Porous) const;
    virtual void checkCoalescenceGTNWithNormal(const SVector3& normal, const IPNonLocalPorosity* q0Porous, IPNonLocalPorosity* q1Porous) const;
    virtual void forceCoalescenceGTN(const IPNonLocalPorosity* q0Porous, IPNonLocalPorosity* q1Porous) const;
    virtual void computeConcentrationFactor(const  IPNonLocalPorosity* q0Porous, const  IPNonLocalPorosity* q1Porous, 
                                              double& CfT, bool stiff=false, double* DCfTDX=NULL, double* DCfTDW=NULL) const 
                                              {
                                                Msg::Error("FstarCoalescenceLaw::computeConcentrationFactor cannot be called");
                                              }
    virtual void printInfo() const {Msg::Info("using FstarCoalescenceLaw");};
    #endif // SWIG
};


// Approach using a Thomason-based model with load concentration factor (CTf) to compute the coalescence variables, concentration factor, and check for coalescence state
// Also able to be included in an effective porosity growth f_V^star approach
class LoadConcentrationFactorBasedCoalescenceLaw : public CoalescenceLaw 
{
   #ifndef SWIG
  protected:
  // Phenomenological coalescence - Acceleration rate
    double _acceleratedRate; // Acceleration rate after satifying the criterion
 
 public:
    LoadConcentrationFactorBasedCoalescenceLaw(const int num, const double R);
    LoadConcentrationFactorBasedCoalescenceLaw(const LoadConcentrationFactorBasedCoalescenceLaw& src);
    virtual ~LoadConcentrationFactorBasedCoalescenceLaw(){};
     
     // Standard functions
    virtual coalescenceType getType() const =0;
    virtual CoalescenceLaw* clone() const =0;
    virtual void createIPVariable(IPCoalescence* &qCoal) const = 0;
    
    virtual void checkCoalescenceGTN(const IPNonLocalPorosity* q0Porous, IPNonLocalPorosity* q1Porous) const;
    virtual void checkCoalescenceGTNWithNormal(const SVector3& normal, const IPNonLocalPorosity* q0Porous, IPNonLocalPorosity* q1Porous) const;
    virtual void forceCoalescenceGTN(const IPNonLocalPorosity* q0Porous, IPNonLocalPorosity* q1Porous) const;
    
    virtual void computeConcentrationFactor(const  IPNonLocalPorosity* q0Porous, const  IPNonLocalPorosity* q1Porous, 
                                              double& CfT, bool stiff=false, double* DCfTDX=NULL, double* DCfTDW=NULL) const =0;
    virtual void printInfo() const = 0;
   #endif //SWIG
};

// from Pardoen-Hutchinson 2000
class ThomasonCoalescenceLaw : public LoadConcentrationFactorBasedCoalescenceLaw{
  protected:
    double _alpha, _beta;             // Hardening-dependent parameters in Cft computation

  public:
    ThomasonCoalescenceLaw(const int num, const double R=1.);
    void setThomasonCoefficient(const double a, const double b);
    #ifndef SWIG
    ThomasonCoalescenceLaw(const ThomasonCoalescenceLaw& src);
    virtual ~ThomasonCoalescenceLaw();
     
     // Standard functions
    virtual coalescenceType getType() const {return CoalescenceLaw::ThomasonCoalescence;};
    
    virtual CoalescenceLaw* clone() const {return new ThomasonCoalescenceLaw(*this);};
    
    virtual void createIPVariable(IPCoalescence* &qCoal) const;
    virtual void computeConcentrationFactor(const  IPNonLocalPorosity* q0Porous, const  IPNonLocalPorosity* q1Porous, 
                                              double& CfT, bool stiff=false, double* DCfTDX=NULL, double* DCfTDW=NULL) const;
    virtual void printInfo() const {Msg::Info("using ThomasonCoalescenceLaw");};
    #endif
};

// from Torki-Benzerga-Leblond 2015
class simpleShearCoalescenceLaw : public LoadConcentrationFactorBasedCoalescenceLaw
{
  protected:    
    double _gamma; // damage factro
    double _exponent; // 
  
  public:
    simpleShearCoalescenceLaw(const int num, const double R=1.);
    void setShearFactor(double g);
    void setVoidLigamentExponent(double e);
    #ifndef SWIG
    simpleShearCoalescenceLaw(const simpleShearCoalescenceLaw& src);
    virtual ~simpleShearCoalescenceLaw(){};
     
     // Standard functions
    virtual coalescenceType getType() const {return CoalescenceLaw::ThomasonCoalescence;};
    
    virtual CoalescenceLaw* clone() const {return new simpleShearCoalescenceLaw(*this);};
    
    virtual void createIPVariable(IPCoalescence* &qCoal) const;
    virtual void computeConcentrationFactor(const  IPNonLocalPorosity* q0Porous, const  IPNonLocalPorosity* q1Porous, 
                                              double& CfT, bool stiff=false, double* DCfTDX=NULL, double* DCfTDW=NULL) const;
    virtual void printInfo() const {Msg::Info("using simpleShearCoalescenceLaw");};
    #endif
};

// from Benzerga and Leblond (2014) 
class BenzergaLeblondCoalescenceLaw  : public LoadConcentrationFactorBasedCoalescenceLaw
{
  public:
    BenzergaLeblondCoalescenceLaw(const int num, const double R=1.);
     #ifndef SWIG
    BenzergaLeblondCoalescenceLaw(const BenzergaLeblondCoalescenceLaw& src);
    virtual ~BenzergaLeblondCoalescenceLaw(){};
     
     // Standard functions
    virtual coalescenceType getType() const {return CoalescenceLaw::BenzergaLeblondCoalescence;};
    
    virtual CoalescenceLaw* clone() const {return new BenzergaLeblondCoalescenceLaw(*this);};
    
    virtual void createIPVariable(IPCoalescence* &qCoal) const;
    virtual void computeConcentrationFactor(const  IPNonLocalPorosity* q0Porous, const  IPNonLocalPorosity* q1Porous, 
                                              double& CfT, bool stiff=false, double* DCfTDX=NULL, double* DCfTDW=NULL) const;
    virtual void printInfo() const {Msg::Info("using BenzergaLeblondCoalescenceLaw");};
    #endif
};

#endif // _COALESCENCELAW_H_
