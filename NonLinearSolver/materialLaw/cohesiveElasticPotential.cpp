//
// C++ Interface: material law
//
// Author:  <Van Dung Nguyen>, (C) 2017
//
// Copyright: See COPYING file that comes with this distribution
//
//

#include "cohesiveElasticPotential.h"
#include "STensorOperations.h"

linearCohesiveElasticPotential::linearCohesiveElasticPotential(const int num): cohesiveElasticPotential(num){};

double linearCohesiveElasticPotential::get(const SVector3& ujump, const SVector3& normal, const double Kp, const double beta) const{
  double deltan = dot(ujump,normal);
  double Psi = 0.5*Kp*deltan*deltan;
  
  static SVector3 deltat;
  deltat=ujump;
  for (int i=0; i< 3; i++){
    deltat(i) -= deltan*normal(i);
  }
  
  Psi += 0.5*beta*beta*Kp*dot(deltat,deltat);
  
  return Psi;
};
double linearCohesiveElasticPotential::getPositiveValue(const SVector3& ujump, const SVector3& normal, const double Kp, const double beta) const{
  double deltan = dot(ujump,normal);
  double PoPlus = 0.;
  if (deltan >=0.){
    PoPlus += 0.5*Kp*deltan*deltan;
  }
  static SVector3 deltat;
  deltat = (ujump);
  for (int i=0; i< 3; i++){
    deltat(i) -= deltan*normal(i);
  }
  PoPlus += 0.5*Kp*beta*beta*dot(deltat,deltat);
  return PoPlus;
};
double linearCohesiveElasticPotential::getNegativeValue(const SVector3& ujump, const SVector3& normal, const double Kp, const double beta) const{
  double deltan = dot(ujump,normal);
  double PoNeg = 0.;
  if (deltan < 0.){
    PoNeg += 0.5*Kp*deltan*deltan;
  }
  return PoNeg;
};

void linearCohesiveElasticPotential::constitutivePositive(const SVector3& ujump, const SVector3& normal, const double Kp, const double beta, SVector3& T, const bool stiff, STensor3* L) const{
  double deltan = dot(ujump,normal);
  static SVector3 deltat;
  deltat=ujump;
  for (int i=0; i< 3; i++){
    deltat(i) -= deltan*normal(i);
  }
  
  for (int i=0; i<3; i++){
    T(i) = beta*beta*Kp*deltat(i);
    if (deltan >=0.){
      T(i) += Kp*deltan*normal(i);
    }
  }
  
  if (stiff){
    static STensor3 I(1.);
    for (int i=0; i<3; i++){
      for (int j=0; j<3; j++){
        (*L)(i,j) = beta*beta*Kp*(I(i,j) -normal(i)*normal(j));
        if (deltan >=0.){
          (*L)(i,j) += Kp*normal(i)*normal(j);
        }
      }
    }
  }
};
void linearCohesiveElasticPotential::constitutiveNegative(const SVector3& ujump, const SVector3& normal,const double Kp, const double beta, SVector3& T, const bool stiff, STensor3* L) const{
  double deltan = dot(ujump,normal);
  if (deltan <0.){
    for (int i=0; i<3; i++){
      T(i) = Kp*deltan*normal(i);
    }
  }
  else{
    STensorOperation::zero(T);
  }
  
  if (stiff){
    if (deltan <0.){
      for (int i=0; i<3; i++){
        for (int j=0; j<3; j++){
          (*L)(i,j) = Kp*normal(i)*normal(j);
        }
      }
    }
    else{
      STensorOperation::zero(*L);
    }
  }
};

void linearCohesiveElasticPotential::getEffectiveJump(const SVector3& ujump, const SVector3& normal, const double beta, double& effJump, const bool stiff, SVector3* deffJumpdUjump) const{
  double deltan = dot(ujump,normal);
  static SVector3 deltat;
  deltat=ujump;
  for (int i=0; i< 3; i++){
    deltat(i) -= deltan*normal(i);
  }
  
  effJump = 0.;
  if (deltan >=0.){
    effJump = sqrt(deltan*deltan+ beta*beta*dot(deltat,deltat));
  }
  else{
    effJump = sqrt(beta*beta*dot(deltat,deltat));
  }
  
  if (stiff){
    STensorOperation::zero(*deffJumpdUjump);
    for (int i=0; i<3; i++){
      (*deffJumpdUjump)(i) += beta*beta*deltat(i)/effJump;
      if (deltan >=0.){
        (*deffJumpdUjump)(i) += deltan*normal(i)/effJump;
      }
    }
  }
};
    
