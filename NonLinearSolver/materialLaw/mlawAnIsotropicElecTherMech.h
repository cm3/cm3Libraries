//
// C++ Interface: material law
//              AnIsotropicElecTherMech law
// Author:  <L. Homsy>, (C) 2016
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef MLAWANISOTROPICELECTHERMECH_H_
#define MLAWANISOTROPICELECTHERMECH_H_
#include "STensor3.h"
#include "STensor43.h"
#include "mlawAnIsotropicTherMech.h"
#include "ipAnIsotropicElecTherMech.h"




class mlawAnIsotropicElecTherMech : public mlawAnIsotropicTherMech
{
  protected:
  double _lx;
  double _ly;
  double _lz;
  double _v0;
  STensor3 _l0;
   
  // function of materialLaw
  double _seebeck;

  
 public:
  mlawAnIsotropicElecTherMech(const int num,const double E, const double nu,const double rho,const double EA, const double GA, const double nu_minor,
                           const double Ax, const double Ay, const double Az, const double alpharot, const double betarot, const double gammarot,const double t0,const  double lx,
			   const  double ly,const  double lz,const double seebeck,const double kx,const double ky, const double kz,const double cp,const double alphath,const double v0);
#ifndef SWIG
  
  mlawAnIsotropicElecTherMech(const mlawAnIsotropicElecTherMech &source);
  mlawAnIsotropicElecTherMech& operator=(const materialLaw &source);
  virtual ~mlawAnIsotropicElecTherMech(){}
  virtual materialLaw* clone() const {return new mlawAnIsotropicElecTherMech(*this);};
  virtual bool withEnergyDissipation() const {return false;};
  // function of materialLaw
  virtual matname getType() const{return materialLaw::AnIsotropicElecTherMech;}
  virtual void createIPState(IPStateBase* &ips,  bool hasBodyForce, const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const IntPt *GP=NULL, const int gpt =0) const;
  
public:
    virtual void constitutive(
                            const STensor3& F0,         // initial deformation gradient (input @ time n)
                            const STensor3& Fn,         // updated deformation gradient (input @ time n+1)
                            STensor3 &P,                // updated 1st Piola-Kirchhoff stress tensor (output)
                                                        // contains the initial values on input
                            const IPAnIsotropicElecTherMech *q0,       // array of initial internal variable
                            IPAnIsotropicElecTherMech *q1,             // updated array of internal variable (in ipvcur on output),
                            STensor43 &Tangent,         // constitutive tangents (output)
                            double T0,
			    double T,
			    double V0,
			    double V,
  			    const SVector3 &gradT,
			    const SVector3 &gradV,
                            SVector3 &fluxT,
			    SVector3 &fluxV,
                            STensor3 &dPdT,
                            STensor3 &dPdV,
                            STensor3 &dqdgradT,
                            SVector3 &dqdT,
			    STensor3 &dqdgradV,
                            SVector3 &dqdV,
			    SVector3 &dedV,
			    STensor3 &dedgradV,
			    SVector3 &dedT,
			    STensor3 &dedgradT,
                            STensor33 &dqdF,
			    STensor33 &dedF,
			    double &w,
			    double &dwdt,
			    STensor3 &dwdf,
			    STensor3 &l10,
			    STensor3 &l20,
			    STensor3 &k10,
			    STensor3 &k20,
			    STensor3 &dl10dT,
			    STensor3 &dl20dT,
			    STensor3 &dk10dT,
			    STensor3 &dk20dT,
			    STensor3 &dl10dv,
			    STensor3 &dl20dv,
			    STensor3 &dk10dv,
			    STensor3 &dk20dv,
			    STensor43 &dl10dF,
			    STensor43 &dl20dF,
			    STensor43 &dk10dF,
			    STensor43 &dk20dF,
			    SVector3 &fluxjy,
			    SVector3 &djydV,
			    STensor3 &djydgradV,
			    SVector3 &djydT,
			    STensor3 &djydgradT,
                            STensor33 &djydF,
			    STensor3 &djydgradVdT,
			    STensor3 &djydgradVdV,
			    STensor43 &djydgradVdF,
			    STensor3 &djydgradTdT,
			    STensor3 &djydgradTdV,
			    STensor43 &djydgradTdF,
                            const bool stiff
                           ) const;
			   
    double getInitialVoltage() const {return _v0;};


  protected:
  virtual double deformationEnergy(const SVector3 &gradV,const SVector3 &gradT,const SVector3 &fluxV,const SVector3 &fluxjy,double T, double V) const ;


 #endif // SWIG
};

#endif // MLAWANISOTROPICELECTHERMECH_H_
