/*************************************************************************************************/
/** Copyright (C) 2022 - See COPYING file that comes with this distribution	                    **/
/** Author: Mohamed HADDAD (MEMA-iMMC-UCLouvain)							                    **/
/** Contact: mohamed.haddad@uclouvain.be					                                    **/
/**											                                                    **/
/** Description: namespace for storing functions needed for VE-VP material law (small strains)  **/
/** VE-VP material law : See B.Miled et I.Doghri 2011                                           **/
/*************************************************************************************************/
#ifndef USEFULFUNCTIONSVEVPMFH_H
#define USEFULFUNCTIONSVEVPMFH_H 1

namespace VEVPMFHSPACE{

  const int max_nbrGi = 4;  
  const int max_nbrKj = 4;
  
  // Structure containing the mechanical variables
  typedef struct State {
    double *strn;  		    /* Strain tensor */
    double *strs;  		    /* Stress tensor */
    double p;      		    /* Accumulated plasticity - scalar */
    double *pstrn;  		  /* Plastic strain */
    double **Si;        	/* matrix nbrGi*6 : deviatoric part of viscous stress relative to Gi */
    double **Sigma_Hj;  	/* matrix nbrGi*6 : volumetric part of viscous stress relative to kj */
  } STATE;

  // Structure containing the macro mechanical variables (Only for MFH)
  typedef struct Statemacro {
    double *strn;       	  /* Macroscopic strain tensor */
    double *strs;        	  /* Macroscopic stress tensor */
    double **Cmacro;		    /* Macroscopic  elastic stiffness (or equivalent) */
    double *Beta_macro;    	/* Macroscopic thermal stress (or equivalent) */
  } STATEMACRO;

  // Structure containing the VE-VP material properties
  typedef struct Matprop{ 
    double E0;              /* Initial relaxation modulus */
    double nu;              /* Poisson's ratio */
    double yldstrs;         /* Yield stress */
    int hardmodel;          /* Hardening model type */
    double hardmodulus1;
    double hardmodulus2;
    double hardexp;
    int viscmodel;          /* Viscous model type */
    double visccoef;
    double viscexp1;
    double viscexp2;
    double v;               /* Volume fraction of the phase */
    double ar;              /* Aspect ratio */
    double orient[3];       /* Orientation of inclusions  */
    
    // VE properties 
    double G0;
    double G_inf;
    int nbrGi;  		        /* number of shear relaxation times */
    double *Gi; 		        /* shear relaxation weights */ 
    double *gi; 		        /* shear relaxation times */
  
    double K0;
    double K_inf;
    int nbrKj;   		        /* number of bulk relaxation times */ 
    double *Kj;  		        /* bulk relaxation weights */
    double *kaj; 		        /* bulk relaxation times */ 
  }MATPROP;


  void convertmatrvect (double **matr, double *vect);
  void convertvectmatr (double *vect, double **matr);

  void addtens4 (double a, double **A, double b, double **B, double **res);
  void addtens2 (double a, double *A, double b, double *B, double *res);
  double contraction22 (const double *, const double *);
  void contraction42 (double **T4, double *T2, double *Sol);
  void contraction44 (double **, double **, double **);
  double dcontr44 (double **TA, double **TB);
  double contraction444 (double **A, double **B, double **C);
  void contraction442 (double **A, double **B, double *C, double *Tres);
  double contr444_reduced (double **A, double **B, double **C);
  void produit_tensoriel (double *TA, double *TB, double **Tres);

  void ludcmp(double **a, int n, int *indx, double *d, int *error);
  void lubksb (double **a, int n, int *indx, double b[]);
  void inverse (double **a, double **y, int *error, int n);
  void extractiso (double **tensani, double **tensiso);

  void mallocmatrix (double ***A, int m, int n);
  void freematrix (double **A, int m);
  void mallocmatrix3D (double ****A, int p, int m, int n);
  void freematrix3D (double ***A, int p, int m);
  void mallocMATPROP(MATPROP* matr, MATPROP* incl, const char* inputfilename);
  
  void compute_Hooke(double E, double nu, double **C);

  void printvect(double *vect,int  n);
  void fprintvect(FILE *pfile, double *vect, int n);
  void printmatr(double **matr4, int m, int n);
  void fprintmatr(FILE *pfile, double **matr4, int m, int n);

  double max(double a, double b);
  int isnull(double *A, int size);

  double trace(double *tens2);
  void dev(double *tens2, double *dev_tens2);
  double vonmises(double *tens2);
  double eqstrn(double *tens2);

  int isisotropic(double**A);
  void copyvect(double *A, double *B, int size);

  void compute_I(double **I);
  void compute_Ivol(double **I_vol);
  void compute_Idev(double **I_dev);


  void readinput (MATPROP *matr, MATPROP *incl, const char* inputfilename);
  void computestrslinel(double young, double nu, double *strn, double *strs);
  void computestrnlinel(double young, double nu, double *strs, double *strn);

  void compute_R (double p, const MATPROP *mat, double *Rp, double *dRdp);
  void computevisc (double strseq, double p, const MATPROP *mat, double *gv, double *dgvdstrseq, double *dgvdp);

  void mallocstate(STATE *st);
  void freestate(STATE st);
  void copystate(const STATE *A, STATE *B);
  void fprintstate(FILE *pfile, STATE *A);
  void printstate(STATE *A);

  void fileprintresults(FILE *pfile, double time, STATE *st);

  void printmatprop(MATPROP *A);
  void fprintmatprop(FILE *pfile, MATPROP *A);

  double compute_Gtilde(const MATPROP *material_prop, double dt);
  double compute_Ktilde(const MATPROP *material_prop, double dt);
  void compute_Etilde(const MATPROP *material_prop, double dt, double **E_tilde);
  void compute_Einf(const MATPROP *material_prop, double **E_inf);
  
  void decomposition_dstrn_ve(double *dstrn_ve, double *dstrn_ve_volpart, double *dstrn_ve_devpart);
  
  void compute_Si(const MATPROP *material_prop, const STATE *tn, double dt, double *dstrn_ve, double **Si);
  void compute_SigmaHj(const MATPROP *material_prop, const STATE *tn, double dt, double *dstrn_ve, double **Sigma_Hj);

  void compute_sum_Siexp(const MATPROP *material_prop, double dt, double **Si, double *sum_Siexp);
  void compute_sum_SigmaHjexp(const MATPROP *material_prop, double dt, double **Sigma_Hj, double *sum_SigmaHjexp);
  
  void CalgoUsingPerturbation(const MATPROP *matrprop, STATE *matrtn, STATE *matrtau, double dt, double *dstrn, double **Calgo);

  int ConstitutiveBoxViscoElasticViscoPlasticSmallStrains(const MATPROP *material, const STATE *tn, double dt, double *dstrn, STATE *tau, double **Calgo);

}
 
#endif