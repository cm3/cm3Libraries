//
// C++ Interface: material law
//
// Description: non-local damage elasto-plastic law
//
//
// Author:  <L. NOELS>, (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include <math.h>
#include "MInterfaceElement.h"
#include <fstream>
#include <stdio.h>
#include <stdlib.h>
#include "mlawNonLocalDamage.h"
#include "mlawNonLocalDamage_Stoch.h"
#include "rotations.h"

mlawNonLocalDamage_Stoch::mlawNonLocalDamage_Stoch(const int num, const double rho, const char *propName, const double Ori_x, const double Ori_y, const double Ori_z,const double Lx,const double Ly,const double Lz, const double dx,const double dy,const double dz, const double euler0, const double euler1, const double euler2, const char *Geo, const char *RandProp):mlawNonLocalDamage(num, rho, propName), _OrigX(Ori_x), _OrigY(Ori_y), _OrigZ(Ori_z), _Ldomain(Lx, Ly, Lz), _dx(dx), _dy(dy), _dz(dz), _intpl(0), Reuler(NULL), RMatProp(NULL), _Euler_mean(NULL), _Euler_tmp(NULL), _Euler_tmp2(NULL), _Euler_tmp3(NULL){
        pos_vfi = mat->get_pos_vfi();
        pos_euler = mat->get_pos_euler();
        pos_aspR = mat->get_pos_aspR();
        pos_ME = mat->get_pos_E();
        pos_Mnu = mat->get_pos_nu();
        pos_Msy0 = mat->get_pos_sy0();
        pos_Mhmod1 = mat->get_pos_hmod1();
        pos_Mhmod2 = mat->get_pos_hmod2();
        pos_Malpha_DP = mat->get_pos_alpha_DP();
        pos_Mm_DP = mat->get_pos_m_DP();
        pos_Mnup = mat->get_pos_nup();
        pos_Mhexp = mat->get_pos_hexp();  
        pos_DamParm1 = mat->get_pos_DamParm1();
        pos_DamParm2 = mat->get_pos_DamParm2();
        pos_DamParm3 = mat->get_pos_DamParm3();
        pos_INCDamParm1 = mat->get_pos_INCDamParm1();
        pos_INCDamParm2 = mat->get_pos_INCDamParm2();
        pos_INCDamParm3 = mat->get_pos_INCDamParm3();

        Randnum = 0;
        if(pos_vfi !=0) Randnum +=1;
        if(pos_aspR !=0) Randnum +=1;
        if(pos_ME !=0) Randnum +=1;
        if(pos_Mnu !=0) Randnum +=1;
        if(pos_Msy0 !=0) Randnum +=1;
        if(pos_Mhmod1 !=0) Randnum +=1;
        if(pos_Mhmod2 !=0) Randnum +=1;
        if(pos_Malpha_DP !=0) Randnum +=1;
        if(pos_Mm_DP !=0) Randnum +=1;
        if(pos_Mnup !=0) Randnum +=1;
        if(pos_Mhexp !=0) Randnum +=1; 
        if(pos_euler !=0) Randnum +=1; 
        if(pos_DamParm1 !=0) Randnum +=1; 
        if(pos_DamParm2 !=0) Randnum +=1; 
        if(pos_DamParm3 !=0) Randnum +=1; 
        if(pos_INCDamParm1 !=0) Randnum +=1; 
        if(pos_INCDamParm2 !=0) Randnum +=1; 
        if(pos_INCDamParm3 !=0) Randnum +=1;  

        //allocate memory if random variable are required
        if (pos_euler != 0){
	     mallocvector(&_Euler_mean,3);
	     //mallocvector(&_Euler_tmp,3);
             //mallocmatrix(&_Euler_tmp2,3,3);
             //mallocmatrix(&_Euler_tmp3,3,3);
             _Euler_mean[0] = euler0;
             _Euler_mean[1] = euler1;
             _Euler_mean[2] = euler2;

             Reuler = init_RandF(_Euler_mean, _Ldomain, _OrigX, _OrigY, _OrigZ, _dx, _dy, _dz, Geo);
             if ( Randnum > 1){
	         RMatProp = init_RandF(Randnum, _Ldomain, _OrigX, _OrigY,_OrigZ, _dx, _dy, _dz, RandProp);
             }
        }
        else if ( Randnum != 0){
	     RMatProp = init_RandF(Randnum, _Ldomain, _OrigX, _OrigY,_OrigZ, _dx, _dy, _dz, RandProp);
        }
}


mlawNonLocalDamage_Stoch::mlawNonLocalDamage_Stoch(const int num, const double rho, const char *propName, const double Ori_x, const double Ori_y, const double Ori_z,const double Lx,const double Ly,const double Lz, const char *RandProp, const int intpl): mlawNonLocalDamage(num, rho, propName), _OrigX(Ori_x), _OrigY(Ori_y), _OrigZ(Ori_z), _Ldomain(Lx, Ly, Lz), _intpl(intpl), Reuler(NULL), RMatProp(NULL), _Euler_mean(NULL), _Euler_tmp(NULL), _Euler_tmp2(NULL), _Euler_tmp3(NULL){

    double vfi_max = 0.0;
    double ME_max = 0.0;
    double nu_mtx = 0.3;

    pos_vfi = mat->get_pos_vfi();
    pos_euler = mat->get_pos_euler();
    pos_aspR = mat->get_pos_aspR();
    pos_ME = mat->get_pos_E();
    pos_Mnu = mat->get_pos_nu();
    pos_Msy0 = mat->get_pos_sy0();
    pos_Mhmod1 = mat->get_pos_hmod1();
    pos_Mhmod2 = mat->get_pos_hmod2();
    pos_Malpha_DP = mat->get_pos_alpha_DP();
    pos_Mm_DP = mat->get_pos_m_DP();
    pos_Mnup = mat->get_pos_nup();
    pos_Mhexp = mat->get_pos_hexp();  
    pos_DamParm1 = mat->get_pos_DamParm1();
    pos_DamParm2 = mat->get_pos_DamParm2();
    pos_DamParm3 = mat->get_pos_DamParm3();
    pos_INCDamParm1 = mat->get_pos_INCDamParm1();
    pos_INCDamParm2 = mat->get_pos_INCDamParm2();
    pos_INCDamParm3 = mat->get_pos_INCDamParm3();

    Randnum = 0;
    if(pos_vfi !=0) Randnum +=1;
    if(pos_aspR !=0) Randnum +=1;
    if(pos_ME !=0) Randnum +=1;
    if(pos_Mnu !=0) Randnum +=1;
    if(pos_Msy0 !=0) Randnum +=1;
    if(pos_Mhmod1 !=0) Randnum +=1;
    if(pos_Mhmod2 !=0) Randnum +=1;
    if(pos_Malpha_DP !=0) Randnum +=1;
    if(pos_Mm_DP !=0) Randnum +=1;
    if(pos_Mnup !=0) Randnum +=1;
    if(pos_Mhexp !=0) Randnum +=1; 
    if(pos_euler !=0) Randnum +=1; 
    if(pos_DamParm1 !=0) Randnum +=1; 
    if(pos_DamParm2 !=0) Randnum +=1; 
    if(pos_DamParm3 !=0) Randnum +=1; 
    if(pos_INCDamParm1 !=0) Randnum +=1; 
    if(pos_INCDamParm2 !=0) Randnum +=1;  
    if(pos_INCDamParm3 !=0) Randnum +=1; 

    //allocate memory if random variable are required
    // read random property variables from 
    if ( Randnum != 0){
       double Rprop[15];
       int nxyz[3];
       int k;


       FILE *Props = fopen(RandProp, "r");
       if ( Props != NULL ){
          int okf = fscanf(Props, "%d %d %d\n", &nxyz[0], &nxyz[1], &nxyz[2]);
          if (nxyz[2] == 1){
             bool resizeFlag;
             if(pos_vfi !=0) resizeFlag = _VfMat.resize(nxyz[0], nxyz[1], true);
             if(pos_aspR !=0) resizeFlag = _aspRMat.resize(nxyz[0], nxyz[1], true);
             if(pos_ME !=0) resizeFlag = _E_Mat.resize(nxyz[0], nxyz[1], true);
             if(pos_Mnu !=0) resizeFlag = _nu_Mat.resize(nxyz[0], nxyz[1], true);
             if(pos_Msy0 !=0) resizeFlag = _sy0_Mat.resize(nxyz[0], nxyz[1], true);
             if(pos_Mhmod1 !=0) resizeFlag = _hmod1_Mat.resize(nxyz[0], nxyz[1], true);
             if(pos_Mhmod2 !=0) resizeFlag = _hmod2_Mat.resize(nxyz[0], nxyz[1], true);
             if(pos_Malpha_DP !=0) resizeFlag = _alpha_DP_Mat.resize(nxyz[0], nxyz[1], true);
             if(pos_Mm_DP !=0) resizeFlag = _m_DP_Mat.resize(nxyz[0], nxyz[1], true);
             if(pos_Mnup !=0) resizeFlag = _nup_Mat.resize(nxyz[0], nxyz[1], true);
             if(pos_Mhexp !=0) resizeFlag = _hexp_Mat.resize(nxyz[0], nxyz[1], true);
             if(pos_euler !=0) resizeFlag = _euler_Mat.resize(nxyz[0], nxyz[1], true);
             if(pos_DamParm1 !=0) resizeFlag = _dam_Parm1_Mat.resize(nxyz[0], nxyz[1], true);
             if(pos_DamParm2 !=0) resizeFlag = _dam_Parm2_Mat.resize(nxyz[0], nxyz[1], true);
             if(pos_DamParm3 !=0) resizeFlag = _dam_Parm3_Mat.resize(nxyz[0], nxyz[1], true);

             okf = fscanf(Props, "%lf %lf %lf\n", &_dx, &_dy, &_dz);

             if (resizeFlag){
                for(int i=0; i<nxyz[0]; i++){
                   for(int j=0; j<nxyz[1]; j++){
                      okf = fscanf(Props, "%lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf\n", &Rprop[0],&Rprop[1],&Rprop[2],&Rprop[3],&Rprop[4],&Rprop[5],&Rprop[6],&Rprop[7],&Rprop[8],&Rprop[9],&Rprop[10],&Rprop[11],&Rprop[12],&Rprop[13],&Rprop[14]);
                      k = 0;
                      if(pos_vfi !=0){
                         _VfMat.set(i, j, Rprop[k++]);
                         if(vfi_max < _VfMat(i, j)) vfi_max= _VfMat(i, j);}

                      if(pos_aspR !=0) _aspRMat.set(i, j, Rprop[k++]);
                      if(pos_ME !=0){
                         _E_Mat.set(i, j, Rprop[k++]);
                         if(ME_max < _E_Mat(i, j)) ME_max= _E_Mat(i, j);}

                      if(pos_Mnu !=0) _nu_Mat.set(i, j, Rprop[k++]);
                      if(pos_Msy0 !=0) _sy0_Mat.set(i, j, Rprop[k++]);
                      if(pos_Mhmod1 !=0) _hmod1_Mat.set(i, j, Rprop[k++]);
                      if(pos_Mhmod2 !=0) _hmod2_Mat.set(i, j, Rprop[k++]);
                      if(pos_Malpha_DP !=0) _alpha_DP_Mat.set(i, j, Rprop[k++]);
                      if(pos_Mm_DP !=0) _m_DP_Mat.set(i, j, Rprop[k++]);
                      if(pos_Mnup !=0) _nup_Mat.set(i, j, Rprop[k++]);
                      if(pos_Mhexp !=0) _hexp_Mat.set(i, j, Rprop[k++]);
                      if(pos_euler !=0) _euler_Mat.set(i, j, Rprop[k++]);
                      if(pos_DamParm1 !=0) _dam_Parm1_Mat.set(i, j, Rprop[k++]);
                      if(pos_DamParm2 !=0) _dam_Parm2_Mat.set(i, j, Rprop[k++]);
                      if(pos_DamParm3 !=0) _dam_Parm3_Mat.set(i, j, Rprop[k]);
             
                  }
                }
             }
             else{
                  printf("Couldn't resize the property matrices.\n");
             }
         }
         else{printf("randomness along z direction is not implemented.\n");}
         fclose(Props);
      }
      else{
        printf("Property file is not usable.\n");
      } 
    }

    // compute a reference elastic tensor

	int l,error;
        double vf_i, vf_m; 
	static double **C0=NULL;
        static double **C1=NULL;
        static double **invC0=NULL;
        static double **C0_loc=NULL;
	static double **A=NULL;
        static double **invA=NULL;
	static double **S=NULL;
        static double **R66=NULL;
	static double **I=NULL;
	static double **mat1=NULL;
        static double **Cel=NULL;
  

        if(C0==NULL)
        {
          mallocmatrix(&C0,6,6);
          mallocmatrix(&C1,6,6);
          mallocmatrix(&invC0,6,6);
          mallocmatrix(&C0_loc,6,6);
          mallocmatrix(&A,6,6);
          mallocmatrix(&invA,6,6);
          mallocmatrix(&S,6,6);
          mallocmatrix(&R66,6,6);
          mallocmatrix(&I,6,6);
          mallocmatrix(&mat1,6,6);
          mallocmatrix(&Cel,6,6);
        }
        cleartens4(C0);
        cleartens4(C1);
        cleartens4(invC0);
        cleartens4(C0_loc);
        cleartens4(A);
        cleartens4(invA);
        cleartens4(S);
        cleartens4(R66);
        cleartens4(I);
        cleartens4(mat1);
     

	for(l=0;l<6;l++){
		I[l][l] = 1.;
	}

        if (vfi_max == 0.0){
            vf_i = mat->get_vfI();
            vf_m = 1.0 -vf_i;
        }
        else{
            vf_i = vfi_max;
            vf_m = 1.0 -vf_i;
        }
        
        if (ME_max == 0.0) mat->get_elOp_mtx(C0);
        else compute_Hooke(ME_max,nu_mtx,C0);
	mat->get_elOp_icl(C1);
	
	eshelby(C0,1000000.0,1000000.0,S);  
	
	inverse(C0,invC0,&error,6);
	if(error!=0) printf("problem while inversing C0 in compute_elOp of MT\n");

	contraction44(invC0,C1,mat1);
	addtens4(1.,mat1,-1.,I,mat1);	
	contraction44(S,mat1,invA);
	addtens4(1.,I,vf_m,invA,invA);
	inverse(invA,A,&error,6);
	if(error!=0) printf("problem while inversing invA in compute_elOp of MT\n");


	addtens4(1.,C1,-1.,C0,mat1);
	contraction44(mat1,A,Cel);
	addtens4(1.,C0,vf_i,Cel,Cel);

        double C1111 = Cel[0][0];
        double C2222 = Cel[1][1];
        double C3333 = Cel[2][2];
        double C1122 = Cel[0][1];
        double C1212 = Cel[3][3];
        double C1313 = Cel[4][4];
        double C2323 = Cel[5][5];
        // taka max value if anistotropy
        _mu0 = C1212/2.;
        _nu0 = C1111/2./(_mu0+C1111);
        if(C1313>_mu0)
        {
          _mu0 = C1313;
          _nu0 = C2222/2./(_mu0+C2222);
        }
        if(C2323>_mu0)
        {
          _mu0 = C2323;
          _nu0 = C3333/2./(_mu0+C3333);
        }
        if (_nu0 < 0 or _nu0 >0.5) Msg::Error("mlawNonLocalDamage: wrong Poisson coefficient");
	          
}



mlawNonLocalDamage_Stoch::mlawNonLocalDamage_Stoch(const mlawNonLocalDamage_Stoch &source):                                        
           mlawNonLocalDamage(source), pos_vfi(source.pos_vfi), pos_euler(source.pos_euler), pos_aspR(source.pos_aspR), 
           pos_ME(source.pos_ME), pos_Mnu(source.pos_Mnu), pos_Msy0(source.pos_Msy0), pos_Mhmod1(source.pos_Mhmod1), 
           pos_Mhmod2(source.pos_Mhmod2), pos_Malpha_DP(source.pos_Malpha_DP), pos_Mm_DP(source.pos_Mm_DP), pos_Mnup(source.pos_Mnup), pos_Mhexp(source.pos_Mhexp), pos_DamParm1(source.pos_DamParm1), pos_DamParm2(source.pos_DamParm2), pos_DamParm3(source.pos_DamParm3),  
           pos_INCDamParm1(source.pos_INCDamParm1), pos_INCDamParm2(source.pos_INCDamParm2), pos_INCDamParm3(source.pos_INCDamParm3), Randnum(source.Randnum), _intpl(source._intpl), 
           _dx(source._dx), _dy(source._dy), _dz(source._dz), _OrigX(source._OrigX), _OrigY(source._OrigY), 
           _OrigZ(source._OrigZ), _Ldomain(source._Ldomain), _VfMat(source._VfMat), _aspRMat(source._aspRMat), 
           _E_Mat(source._E_Mat), _nu_Mat(source._nu_Mat), _sy0_Mat(source._sy0_Mat), _hmod1_Mat(source._hmod1_Mat),
           _hmod2_Mat(source._hmod2_Mat), _alpha_DP_Mat(source._alpha_DP_Mat), _m_DP_Mat(source._m_DP_Mat),_nup_Mat(source._nup_Mat),_hexp_Mat(source._hexp_Mat),_euler_Mat(source._euler_Mat),
           _dam_Parm1_Mat(source._dam_Parm1_Mat),_dam_Parm2_Mat(source._dam_Parm2_Mat) ,_dam_Parm3_Mat(source._dam_Parm3_Mat){

     Reuler = NULL;
     RMatProp  = NULL;
     if(source.Reuler != NULL){
         Reuler=source.Reuler->clone();
     }
     if(source.RMatProp != NULL){
         RMatProp=source.RMatProp->clone();
     }
}

mlawNonLocalDamage_Stoch& mlawNonLocalDamage_Stoch::operator=(const materialLaw &source)
{
     mlawNonLocalDamage::operator=(source);
     const mlawNonLocalDamage_Stoch* src = static_cast<const mlawNonLocalDamage_Stoch*>(&source);

     pos_vfi = src->pos_vfi;
     pos_euler = src->pos_euler;
     pos_aspR = src->pos_aspR; 
     pos_ME = src->pos_ME;
     pos_Mnu = src->pos_Mnu;
     pos_Msy0 = src->pos_Msy0;
     pos_Mhmod1 = src->pos_Mhmod1;
     pos_Mhmod2 = src->pos_Mhmod2;
     pos_Malpha_DP = src->pos_Malpha_DP;
     pos_Mm_DP = src->pos_Mm_DP;
     pos_Mnup = src->pos_Mnup;
     pos_Mhexp = src->pos_Mhexp;
     pos_DamParm1 = src->pos_DamParm1;
     pos_DamParm2 = src->pos_DamParm2;
     pos_DamParm3 = src->pos_DamParm3;

     pos_INCDamParm1 = src->pos_INCDamParm1;
     pos_INCDamParm2 = src->pos_INCDamParm2;
     pos_INCDamParm3 = src->pos_INCDamParm3;

     Randnum = src->Randnum;

     _intpl = src->_intpl;
     _dx = src->_dx;
     _dy = src->_dy;
     _dz = src->_dz;

     _OrigX = src->_OrigX;
     _OrigY = src->_OrigY;
     _OrigZ = src->_OrigZ;
     _Ldomain = src->_Ldomain;

     _VfMat = src->_VfMat;
     _aspRMat = src->_aspRMat;
     _E_Mat = src->_E_Mat;
     _nu_Mat = src->_nu_Mat;

     _sy0_Mat = src->_sy0_Mat;
     _hmod1_Mat = src->_hmod1_Mat;
     _hmod2_Mat = src->_hmod2_Mat;

     _alpha_DP_Mat = src->_alpha_DP_Mat;
     _m_DP_Mat = src->_m_DP_Mat;
     _nup_Mat = src->_nup_Mat;


     _hexp_Mat = src->_hexp_Mat;
     _euler_Mat = src->_euler_Mat;

     _dam_Parm1_Mat = src->_dam_Parm1_Mat;
     _dam_Parm2_Mat = src->_dam_Parm2_Mat;
     _dam_Parm3_Mat = src->_dam_Parm3_Mat;


     RMatProp  = NULL;
     Reuler = NULL;
     if(src->RMatProp != NULL){
         RMatProp = src->RMatProp->clone();
     }
     if(src->Reuler != NULL){
         Reuler = src->Reuler->clone();
     }
     return *this;
}


  mlawNonLocalDamage_Stoch::~mlawNonLocalDamage_Stoch()
  {
        if(_Euler_mean != NULL){
          delete _Euler_mean;
          _Euler_mean=NULL;
        }
        if(_Euler_tmp != NULL){
          delete _Euler_tmp;
          _Euler_tmp=NULL;
        }
        if(_Euler_tmp2 != NULL){
          for(int i=0; i<3; i++)
            delete _Euler_tmp2[i];
          delete _Euler_tmp2;
          _Euler_tmp2=NULL;
        }
        if(_Euler_tmp3 != NULL){
          for(int i=0; i<3; i++)
            delete _Euler_tmp3[i];
          delete _Euler_tmp3;
          _Euler_tmp3=NULL;
        }  
  }



void mlawNonLocalDamage_Stoch::createIPState(const SVector3 &GaussP, IPNonLocalDamage *ivi, IPNonLocalDamage *iv1, IPNonLocalDamage *iv2) const
{

  //initialize Eshelby tensor
  int kstep = 1;
  int kinc  = 1;
  double *stvi = ivi->_nldStatev;
  double *stv1 = iv1->_nldStatev;
  double *stv2 = iv2->_nldStatev;
  double SpBartmp = 0.0;
  double dFd_d_bartmp = 0.0;
  double dpdFdtmp = 0.0;
  double dFddptmp = 0.0;

  ivi->setPosMaxD(mat->get_pos_maxD());
  ivi->setPosIncMaxD(mat->get_pos_inc_maxD());
  ivi->setPosMtxMaxD(mat->get_pos_mtx_maxD());
  iv1->setPosMaxD(mat->get_pos_maxD());
  iv1->setPosIncMaxD(mat->get_pos_inc_maxD());
  iv1->setPosMtxMaxD(mat->get_pos_mtx_maxD());
  iv2->setPosMaxD(mat->get_pos_maxD());
  iv2->setPosIncMaxD(mat->get_pos_inc_maxD());
  iv2->setPosMtxMaxD(mat->get_pos_mtx_maxD());

  ivi->setMaxD(_Dmax_Mtx);
  ivi->setIncMaxD(_Dmax_Inc);
  ivi->setMtxMaxD(_Dmax_Mtx);
  iv1->setMaxD(_Dmax_Mtx);
  iv1->setIncMaxD(_Dmax_Inc);
  iv1->setMtxMaxD(_Dmax_Mtx);
  iv2->setMaxD(_Dmax_Mtx);
  iv2->setIncMaxD(_Dmax_Inc);
  iv2->setMtxMaxD(_Dmax_Mtx);

  if(Randnum !=0){
      ivi->setPos_vfi(pos_vfi);
      ivi->setPos_euler(pos_euler);
      ivi->setPos_aspR(pos_aspR);
      ivi->setPos_ME(pos_ME);
      ivi->setPos_Mnu(pos_Mnu);
      ivi->setPos_Msy0(pos_Msy0);
      ivi->setPos_Mhmod1(pos_Mhmod1);
      ivi->setPos_Mhmod2(pos_Mhmod2);
      ivi->setPos_Malpha_DP(pos_Malpha_DP);
      ivi->setPos_Mm_DP(pos_Mm_DP);
      ivi->setPos_Mnup(pos_Mnup);
      ivi->setPos_Mhexp(pos_Mhexp);
      ivi->setPos_DamParm1(pos_DamParm1);
      ivi->setPos_DamParm2(pos_DamParm2);
      ivi->setPos_DamParm3(pos_DamParm3);
      ivi->setPos_INCDamParm1(pos_INCDamParm1);
      ivi->setPos_INCDamParm2(pos_INCDamParm2);
      ivi->setPos_INCDamParm3(pos_INCDamParm3);
      ivi->setRandnum(Randnum);

      iv1->setPos_vfi(pos_vfi);
      iv1->setPos_euler(pos_euler);
      iv1->setPos_aspR(pos_aspR);
      iv1->setPos_ME(pos_ME);
      iv1->setPos_Mnu(pos_Mnu);
      iv1->setPos_Msy0(pos_Msy0);
      iv1->setPos_Mhmod1(pos_Mhmod1);
      iv1->setPos_Mhmod2(pos_Mhmod2);
      iv1->setPos_Malpha_DP(pos_Malpha_DP);
      iv1->setPos_Mm_DP(pos_Mm_DP);
      iv1->setPos_Mnup(pos_Mnup);
      iv1->setPos_Mhexp(pos_Mhexp);
      iv1->setPos_DamParm1(pos_DamParm1);
      iv1->setPos_DamParm2(pos_DamParm2);
      iv1->setPos_DamParm3(pos_DamParm3);
      iv1->setPos_INCDamParm1(pos_INCDamParm1);
      iv1->setPos_INCDamParm2(pos_INCDamParm2);
      iv1->setPos_INCDamParm3(pos_INCDamParm3);
      iv1->setRandnum(Randnum);

      iv2->setPos_vfi(pos_vfi);
      iv2->setPos_euler(pos_euler);
      iv2->setPos_aspR(pos_aspR);
      iv2->setPos_ME(pos_ME);
      iv2->setPos_Mnu(pos_Mnu);
      iv2->setPos_Msy0(pos_Msy0);
      iv2->setPos_Mhmod1(pos_Mhmod1);
      iv2->setPos_Mhmod2(pos_Mhmod2);
      iv2->setPos_Malpha_DP(pos_Malpha_DP);
      iv2->setPos_Mm_DP(pos_Mm_DP);
      iv2->setPos_Mnup(pos_Mnup);
      iv2->setPos_Mhexp(pos_Mhexp);
      iv2->setPos_DamParm1(pos_DamParm1);
      iv2->setPos_DamParm2(pos_DamParm2);
      iv2->setPos_DamParm3(pos_DamParm3);
      iv2->setPos_INCDamParm1(pos_INCDamParm1);
      iv2->setPos_INCDamParm2(pos_INCDamParm2);
      iv2->setPos_INCDamParm3(pos_INCDamParm3);
      iv2->setRandnum(Randnum);   

    if(Reuler != NULL) Reuler->RandomGen(GaussP, &(stv1[pos_euler])); 

 
    if(RMatProp !=NULL){
      double Rprop[Randnum];
      int k=0;
      RMatProp->RandomGen(GaussP, Rprop);

      if(pos_vfi !=0) stv1[pos_vfi] = Rprop[k++];
      if(pos_aspR !=0) stv1[pos_aspR] = Rprop[k++];
      if(pos_ME !=0) stv1[pos_ME] = Rprop[k++];
      if(pos_Mnu !=0) stv1[pos_Mnu] = Rprop[k++];
      if(pos_Msy0 !=0) stv1[pos_Msy0] = Rprop[k++];
      if(pos_Mhmod1 !=0) stv1[pos_Mhmod1] = Rprop[k++];
      if(pos_Mhmod2 !=0) stv1[pos_Mhmod2] = Rprop[k++];
      if(pos_Malpha_DP !=0) stv1[pos_Malpha_DP] = Rprop[k++];
      if(pos_Mm_DP !=0) stv1[pos_Mm_DP] = Rprop[k++];
      if(pos_Mnup !=0) stv1[pos_Mnup] = Rprop[k++];
      if(pos_Mhexp !=0) stv1[pos_Mhexp] = Rprop[k++];
      if(pos_DamParm1 !=0) stv1[pos_DamParm1] = Rprop[k++];
      if(pos_DamParm2 !=0) stv1[pos_DamParm2] = Rprop[k++];
      if(pos_DamParm3 !=0) stv1[pos_DamParm3] = Rprop[k];

   } 
      
   else if(_intpl != 0){
     double param_x = (GaussP(0)-_OrigX)/_dx;
     double param_y = (GaussP(1)-_OrigY)/_dy;

     double x, y;
     double fnx, fny;
     x = modf(param_x , &fnx);
     y = modf(param_y , &fny);
     int nx = (int)fnx;
     int ny = (int)fny; 
     
     int nx_int = nx;
     int ny_int = ny;
     if(x > 0.5) nx_int +=1;
     if(y > 0.5) ny_int +=1;
       if(pos_Msy0 !=0) stv1[pos_Msy0] = _sy0_Mat(nx_int,ny_int);
       if(pos_Mhmod1 !=0) stv1[pos_Mhmod1] = _hmod1_Mat(nx_int,ny_int);
       if(pos_Mhmod2 !=0) stv1[pos_Mhmod2] = _hmod2_Mat(nx_int,ny_int);
       if(pos_Malpha_DP !=0) stv1[pos_Malpha_DP] = _alpha_DP_Mat(nx_int,ny_int);
       if(pos_Mm_DP !=0) stv1[pos_Mm_DP] = _m_DP_Mat(nx_int,ny_int);
       if(pos_Mnup !=0) stv1[pos_Mnup] = _nup_Mat(nx_int,ny_int);
       if(pos_Mhexp !=0)  stv1[pos_Mhexp] = _hexp_Mat(nx_int,ny_int);
       if(pos_DamParm1 !=0) stv1[pos_DamParm1] = _dam_Parm1_Mat(nx_int,ny_int);
       if(pos_DamParm2 !=0) stv1[pos_DamParm2] = _dam_Parm2_Mat(nx_int,ny_int);
       if(pos_DamParm3 !=0) stv1[pos_DamParm3] = _dam_Parm3_Mat(nx_int,ny_int);

     if (_intpl == 1){ 
       if(pos_vfi !=0){
         stv1[pos_vfi]=_VfMat(nx,ny)*(1.0-x)*(1.0-y)+_VfMat(nx+1,ny)*x*(1.0-y)+_VfMat(nx,ny+1)*(1.0-x)*y+_VfMat(nx+1,ny+1)*x*y;}
       if(pos_aspR !=0){ 
         stv1[pos_aspR]=_aspRMat(nx,ny)*(1.0-x)*(1.0-y)+_aspRMat(nx+1,ny)*x*(1.0-y)+_aspRMat(nx,ny+1)*(1.0-x)*y+_aspRMat(nx+1,ny+1)*x*y;}
       if(pos_ME !=0){
         stv1[pos_ME]=_E_Mat(nx,ny)*(1.0-x)*(1.0-y)+_E_Mat(nx+1,ny)*x*(1.0-y)+_E_Mat(nx,ny+1)*(1.0-x)*y+_E_Mat(nx+1,ny+1)*x*y;}
       if(pos_Mnu !=0){
         stv1[pos_Mnu]=_nu_Mat(nx,ny)*(1.0-x)*(1.0-y)+_nu_Mat(nx+1,ny)*x*(1.0-y)+_nu_Mat(nx,ny+1)*(1.0-x)*y+_nu_Mat(nx+1,ny+1)*x*y;}
    //   if(pos_Msy0 !=0){
    //     stv1[pos_Msy0]=_sy0_Mat(nx,ny)*(1.0-x)*(1.0-y)+_sy0_Mat(nx+1,ny)*x*(1.0-y)+_sy0_Mat(nx,ny+1)*(1.0-x)*y+_sy0_Mat(nx+1,ny+1)*x*y;}
    //   if(pos_Mhmod1 !=0){ 
    //     stv1[pos_Mhmod1]=_hmod1_Mat(nx,ny)*(1.0-x)*(1.0-y)+_hmod1_Mat(nx+1,ny)*x*(1.0-y)+_hmod1_Mat(nx,ny+1)*(1.0-x)*y+_hmod1_Mat(nx+1,ny+1)*x*y;}
    //   if(pos_Mhmod2 !=0){ 
    //     stv1[pos_Mhmod2]=_hmod2_Mat(nx,ny)*(1.0-x)*(1.0-y)+_hmod2_Mat(nx+1,ny)*x*(1.0-y)+_hmod2_Mat(nx,ny+1)*(1.0-x)*y+_hmod2_Mat(nx+1,ny+1)*x*y;}
   //    if(pos_Mhexp !=0){
    //     stv1[pos_Mhexp]=_hexp_Mat(nx,ny)*(1.0-x)*(1.0-y)+_hexp_Mat(nx+1,ny)*x*(1.0-y)+_hexp_Mat(nx,ny+1)*(1.0-x)*y+_hexp_Mat(nx+1,ny+1)*x*y;}
   //    if(pos_DamParm1 !=0){
   //      stv1[pos_DamParm1]=_dam_Parm1_Mat(nx,ny)*(1.0-x)*(1.0-y)+_dam_Parm1_Mat(nx+1,ny)*x*(1.0-y)+_dam_Parm1_Mat(nx,ny+1)*(1.0-x)*y+_dam_Parm1_Mat(nx+1,ny+1)*x*y;}
   //    if(pos_DamParm2 !=0){
   //      stv1[pos_DamParm2]=_dam_Parm2_Mat(nx,ny)*(1.0-x)*(1.0-y)+_dam_Parm2_Mat(nx+1,ny)*x*(1.0-y)+_dam_Parm2_Mat(nx,ny+1)*(1.0-x)*y+_dam_Parm2_Mat(nx+1,ny+1)*x*y;}
       if(pos_euler !=0){
         stv1[pos_euler]=_euler_Mat(nx,ny)*(1.0-x)*(1.0-y)+_euler_Mat(nx+1,ny)*x*(1.0-y)+_euler_Mat(nx,ny+1)*(1.0-x)*y+_euler_Mat(nx+1,ny+1)*x*y;
         stv1[pos_euler+1]= 0.0;
         stv1[pos_euler+2]= 0.0;



       }
     }
     else if (_intpl ==2 ){
       if(x >= 0.5){nx=nx+1;}
       if(y >= 0.5){ny=ny+1;}

       if(pos_vfi !=0)  stv1[pos_vfi] = _VfMat(nx,ny);
       if(pos_aspR !=0) stv1[pos_aspR] = _aspRMat(nx,ny);
       if(pos_ME !=0) stv1[pos_ME] = _E_Mat(nx,ny);
       if(pos_Mnu !=0) stv1[pos_Mnu] = _nu_Mat(nx,ny);
     //  if(pos_Msy0 !=0) stv1[pos_Msy0] = _sy0_Mat(nx,ny);
     //  if(pos_Mhmod1 !=0) stv1[pos_Mhmod1] = _hmod1_Mat(nx,ny);
     //  if(pos_Mhmod2 !=0) stv1[pos_Mhmod2] = _hmod2_Mat(nx,ny);
     //  if(pos_Mhexp !=0)  stv1[pos_Mhexp] = _hexp_Mat(nx,ny);
     //  if(pos_DamParm1 !=0) stv1[pos_DamParm1] = _dam_Parm1_Mat(nx,ny);
     //  if(pos_DamParm2 !=0) stv1[pos_DamParm2] = _dam_Parm2_Mat(nx,ny);

       if(pos_euler !=0) {
            stv1[pos_euler] = _euler_Mat(nx,ny);
            stv1[pos_euler+1]= 0.0;
            stv1[pos_euler+2]= 0.0;
       }
     }

     else if (_intpl == 3 ){
       double alpha = 0.1;
       double min = 0.001;
       double max = 0.999;
       if(x > 0.5){nx=nx+1;}
       if(y > 0.5){ny=ny+1;}

       if (x <=min or x >= max){
         if (y <=min or y >= max){
           if(pos_vfi !=0)  stv1[pos_vfi] = _VfMat(nx,ny);
           if(pos_aspR !=0) stv1[pos_aspR] = _aspRMat(nx,ny);
           if(pos_ME !=0) stv1[pos_ME] = _E_Mat(nx,ny);
           if(pos_Mnu !=0) stv1[pos_Mnu] = _nu_Mat(nx,ny);
          // if(pos_Msy0 !=0) stv1[pos_Msy0] = _sy0_Mat(nx,ny);
         //  if(pos_Mhmod1 !=0) stv1[pos_Mhmod1] = _hmod1_Mat(nx,ny);
         //  if(pos_Mhmod2 !=0) stv1[pos_Mhmod2] = _hmod2_Mat(nx,ny);
         //  if(pos_Mhexp !=0)  stv1[pos_Mhexp] = _hexp_Mat(nx,ny);
         //  if(pos_DamParm1 !=0) 
         //  {
         //    stv1[pos_DamParm1] = _dam_Parm1_Mat(nx,ny);
            // if(stv1[pos_DamParm1]<=0.)
             //  printf("Negative damage energy: %e\n", _dam_Parm1_Mat(nx,ny));
         //  } 
         //  if(pos_DamParm2 !=0)
        //   {
        //     stv1[pos_DamParm2] = _dam_Parm2_Mat(nx,ny);
           //  if(stv1[pos_DamParm2]<=0.)
            //   printf("Negative damage exponent: %e\n", _dam_Parm2_Mat(nx,ny));
       //    }
           if(pos_euler !=0) {
                stv1[pos_euler] = _euler_Mat(nx,ny);
                stv1[pos_euler+1]= 0.0;
                stv1[pos_euler+2]= 0.0;
           }
         }
         else{
             int ny0; 
             if( y >0.5 ){
                 ny0 = ny-1;
                 if(ny0 < 0){ny0=0;}
              }
             if( y <=0.5){
                 ny0 = ny;
                 ny = ny+1;
              }   
           double coef = 0.5*(1.0+tanh((y-0.5)/alpha));

           if(pos_vfi !=0)  stv1[pos_vfi] = _VfMat(nx,ny0) + (_VfMat(nx,ny)-_VfMat(nx,ny0))*coef;
           if(pos_aspR !=0) stv1[pos_aspR] = _aspRMat(nx,ny0) + (_aspRMat(nx,ny)-_aspRMat(nx,ny0))*coef;
           if(pos_ME !=0) stv1[pos_ME] = _E_Mat(nx,ny0) + (_E_Mat(nx,ny)-_E_Mat(nx,ny0))*coef;
           if(pos_Mnu !=0) stv1[pos_Mnu] = _nu_Mat(nx,ny0) + (_nu_Mat(nx,ny)-_nu_Mat(nx,ny0))*coef;
      //     if(pos_Msy0 !=0) stv1[pos_Msy0] = _sy0_Mat(nx,ny0) + (_sy0_Mat(nx,ny)-_sy0_Mat(nx,ny0))*coef;
      //     if(pos_Mhmod1 !=0) stv1[pos_Mhmod1] = _hmod1_Mat(nx,ny0) + (_hmod1_Mat(nx,ny)-_hmod1_Mat(nx,ny0))*coef;
      //     if(pos_Mhmod2 !=0) stv1[pos_Mhmod2] = _hmod2_Mat(nx,ny0) + (_hmod2_Mat(nx,ny)-_hmod2_Mat(nx,ny0))*coef;
      //     if(pos_Mhexp !=0)  stv1[pos_Mhexp] = _hexp_Mat(nx,ny0) + (_hexp_Mat(nx,ny)-_hexp_Mat(nx,ny0))*coef;
      //     if(pos_DamParm1 !=0)
     //      {
      //       stv1[pos_DamParm1] = _dam_Parm1_Mat(nx,ny0) + (_dam_Parm1_Mat(nx,ny)-_dam_Parm1_Mat(nx,ny0))*coef;
            // if(stv1[pos_DamParm1]<=0.)
             //  printf("Negative damage energy: %e, %e, %e\n", _dam_Parm1_Mat(nx,ny),_dam_Parm1_Mat(nx,ny0),coef);
      //     }
      //     if(pos_DamParm2 !=0) 
      //     {
      //       stv1[pos_DamParm2] = _dam_Parm2_Mat(nx,ny0) + (_dam_Parm2_Mat(nx,ny)-_dam_Parm2_Mat(nx,ny0))*coef;
         //    if(stv1[pos_DamParm2]<=0.)
             //  printf("Negative damage exponent: %e, %e, %e\n", _dam_Parm2_Mat(nx,ny),_dam_Parm2_Mat(nx,ny0),coef);
     //      }
           if(pos_euler !=0){
               stv1[pos_euler] = _euler_Mat(nx,ny0) + (_euler_Mat(nx,ny)-_euler_Mat(nx,ny0))*coef;
               stv1[pos_euler+1]= 0.0;
               stv1[pos_euler+2]= 0.0;
           }
         }
       }
       else{
         if (y <=min or y >= max){
            int nx0;
            if( x >0.5 ){
                nx0 = nx-1;
                if(nx0 < 0){nx0=0;}
            }
            if( x <=0.5){
                nx0 = nx;
                nx = nx+1;
            } 
            double coef = 0.5*(1.0+tanh((x-0.5)/alpha));

           if(pos_vfi !=0)  stv1[pos_vfi] = _VfMat(nx0,ny) + (_VfMat(nx,ny)-_VfMat(nx0,ny))*coef;
           if(pos_aspR !=0) stv1[pos_aspR] = _aspRMat(nx0,ny) + (_aspRMat(nx,ny)-_aspRMat(nx0,ny))*coef;
           if(pos_ME !=0) stv1[pos_ME] = _E_Mat(nx0,ny) + (_E_Mat(nx,ny)-_E_Mat(nx0,ny))*coef;
           if(pos_Mnu !=0) stv1[pos_Mnu] = _nu_Mat(nx0,ny) + (_nu_Mat(nx,ny)-_nu_Mat(nx0,ny))*coef;
      //     if(pos_Msy0 !=0) stv1[pos_Msy0] = _sy0_Mat(nx0,ny) + (_sy0_Mat(nx,ny)-_sy0_Mat(nx0,ny))*coef;
      //     if(pos_Mhmod1 !=0) stv1[pos_Mhmod1] = _hmod1_Mat(nx0,ny) + (_hmod1_Mat(nx,ny)-_hmod1_Mat(nx0,ny))*coef;
      //     if(pos_Mhmod2 !=0) stv1[pos_Mhmod2] = _hmod2_Mat(nx0,ny) + (_hmod2_Mat(nx,ny)-_hmod2_Mat(nx0,ny))*coef;
      //     if(pos_Mhexp !=0)  stv1[pos_Mhexp] = _hexp_Mat(nx0,ny) + (_hexp_Mat(nx,ny)-_hexp_Mat(nx0,ny))*coef;
      //     if(pos_DamParm1 !=0)
      //     {
      //       stv1[pos_DamParm1] = _dam_Parm1_Mat(nx0,ny) + (_dam_Parm1_Mat(nx,ny)-_dam_Parm1_Mat(nx0,ny))*coef;
            // if(stv1[pos_DamParm1]<=0.)
            //   printf("Negative damage energy: %e, %e, %e\n", _dam_Parm1_Mat(nx0,ny),_dam_Parm1_Mat(nx,ny),coef);
      //     }
       //    if(pos_DamParm2 !=0)
      //     { 
      //       stv1[pos_DamParm2] = _dam_Parm2_Mat(nx0,ny) + (_dam_Parm2_Mat(nx,ny)-_dam_Parm2_Mat(nx0,ny))*coef;
          //   if(stv1[pos_DamParm2]<=0.)
           //    printf("Negative damage exponent: %e, %e, %e\n", _dam_Parm2_Mat(nx0,ny),_dam_Parm2_Mat(nx,ny),coef);
        //   }
           if(pos_euler !=0)  {
                stv1[pos_euler] = _euler_Mat(nx0,ny) + (_euler_Mat(nx,ny)-_euler_Mat(nx0,ny))*coef;
                stv1[pos_euler+1]= 0.0;
                stv1[pos_euler+2]= 0.0;
           }
         }
         else{  
            int nx0, ny0;
            if( x >0.5 ){
                nx0 = nx-1;
                if(nx0 < 0){nx0=0;}
            }
            if( x <=0.5){
                nx0 = nx;
                nx = nx+1;
            } 
            if( y >0.5 ){
                ny0 = ny-1;
                if(ny0 < 0){ny0=0;}
            }
            if( y <=0.5){
                ny0 = ny;
                ny = ny+1;
            }  
            double cx,cy,s,t;
            cx = 0.5*(1.0+tanh((x-0.5)/alpha));
            cy = 0.5*(1.0+tanh((y-0.5)/alpha));
            s = (x-min)/(max-min); 
            t = (y-min)/(max-min); 
            double c00 = (1.0-cx)*(1.0-t) + (1.0-cy)*(1.0-s) - (1.0-s)*(1.0-t);
            double c01 = (1.0-cx)*t + cy*(1.0-s) - (1.0-s)*t;
            double c10 = cx*(1.0-t) + (1.0-cy)*s - s*(1.0-t);
            double c11 = cx*t + cy*s - s*t;
            //if(c00 <0.0) c00 = 0.00;
            //if(c10 <0.0) c10 = 0.00;        
            //if(c01 <0.0) c01 = 0.00;
            //if(c11 <0.0) c11 = 0.00;
            if(pos_vfi !=0){
               stv1[pos_vfi] = c00*_VfMat(nx0,ny0) + c01*_VfMat(nx0,ny) + c10*_VfMat(nx,ny0) + c11*_VfMat(nx,ny);}
            if(pos_aspR !=0){
               stv1[pos_aspR] = c00*_aspRMat(nx0,ny0) + c01*_aspRMat(nx0,ny) + c10*_aspRMat(nx,ny0) + c11*_aspRMat(nx,ny);}
            if(pos_ME !=0){
               stv1[pos_ME] = c00*_E_Mat(nx0,ny0) + c01*_E_Mat(nx0,ny) + c10*_E_Mat(nx,ny0) + c11*_E_Mat(nx,ny);}
            if(pos_Mnu !=0){
               stv1[pos_Mnu] = c00*_nu_Mat(nx0,ny0) + c01*_nu_Mat(nx0,ny) + c10*_nu_Mat(nx,ny0) + c11*_nu_Mat(nx,ny);}
     //       if(pos_Msy0 !=0){
     //          stv1[pos_Msy0] = c00*_sy0_Mat(nx0,ny0) + c01*_sy0_Mat(nx0,ny) + c10*_sy0_Mat(nx,ny0) + c11*_sy0_Mat(nx,ny);}
     //       if(pos_Mhmod1 !=0){
     //          stv1[pos_Mhmod1] = c00*_hmod1_Mat(nx0,ny0) + c01*_hmod1_Mat(nx0,ny) + c10*_hmod1_Mat(nx,ny0) + c11*_hmod1_Mat(nx,ny);}
     //       if(pos_Mhmod2 !=0){
      //         stv1[pos_Mhmod2] = c00*_hmod2_Mat(nx0,ny0) + c01*_hmod2_Mat(nx0,ny) + c10*_hmod2_Mat(nx,ny0) + c11*_hmod2_Mat(nx,ny);}
       //     if(pos_Mhexp !=0){
       //        stv1[pos_Mhexp] = c00*_hexp_Mat(nx0,ny0) + c01*_hexp_Mat(nx0,ny) + c10*_hexp_Mat(nx,ny0) + c11*_hexp_Mat(nx,ny);}

       //     if(pos_DamParm1 !=0){
      //        stv1[pos_DamParm1] = c00*_dam_Parm1_Mat(nx0,ny0) +c01*_dam_Parm1_Mat(nx0,ny)+ c10*_dam_Parm1_Mat(nx,ny0)+c11*_dam_Parm1_Mat(nx,ny);
            //  if(stv1[pos_DamParm1]<=0.)
           //      printf("Negative energy: %e,%e,%e,%e,%e,%e,%e,%e\n", c00,_dam_Parm1_Mat(nx0,ny0),c01,_dam_Parm1_Mat(nx0,ny),c10,_dam_Parm1_Mat(nx,ny0),c11,_dam_Parm1_Mat(nx,ny));
       //    }

      //     if(pos_DamParm2 !=0){
      //        stv1[pos_DamParm2] = c00*_dam_Parm2_Mat(nx0,ny0) + c01*_dam_Parm2_Mat(nx0,ny)+c10*_dam_Parm2_Mat(nx,ny0)+c11*_dam_Parm2_Mat(nx,ny);
           //   if(stv1[pos_DamParm2]<=0.)
           //      printf("Negative exponent: %e,%e,%e,%e,%e,%e,%e,%e\n", c00,_dam_Parm2_Mat(nx0,ny0),c01,_dam_Parm2_Mat(nx0,ny),c10,_dam_Parm2_Mat(nx,ny0),c11,_dam_Parm2_Mat(nx,ny));
      //      }

            if(pos_euler !=0){
               stv1[pos_euler] = c00*_euler_Mat(nx0,ny0) + c01*_euler_Mat(nx0,ny) + c10*_euler_Mat(nx,ny0) + c11*_euler_Mat(nx,ny);
               stv1[pos_euler+1]= 0.0;
               stv1[pos_euler+2]= 0.0;
            }
          }
        }
      }
      else {
         printf("Unknown interpolation method: %d\n", _intpl);
      }	

     if(pos_INCDamParm1 !=0){
      std::random_device rd; 
      std::mt19937 gen(rd()); 
      std::normal_distribution<double> d(0.0, 1.0); 
      // get random number with normal distribution using gen as random source
      if(pos_vfi != 0){
      stv1[pos_INCDamParm1] = int(stv1[pos_vfi]*0.25*0.43/3.0*1000000/3.141592/3.5/3.5); 
      }
      else{
      stv1[pos_INCDamParm1] = 100;
      }
      stv1[pos_INCDamParm2] =  d(gen);
       }
    }
  } 

  copyvect(stv1,stvi,nsdv);

  mat->constbox(dstrn, strs_n, strs, stvi, stv1, Cref, dCref, tau, dtau, Calgo, 1., dpdE, strs_dDdp_bar, &SpBartmp, chara_Length, &dFd_d_bartmp, dstrs_dFd_bar, dFd_dE, chara_Length_INC, &dpdFdtmp, &dFddptmp, kinc, kstep, _timeStep);

  //if(pos_vfi != 0) Rvfi->RandomGen(GaussP, &(stv1[pos_vfi]));
 // if(pos_euler != 0) Reuler->RandomGen(GaussP, &(stv1[pos_euler])); 
  ivi->setPosEuler(pos_euler);
  iv1->setPosEuler(pos_euler);
  iv2->setPosEuler(pos_euler);

  updateCharacteristicLengthTensor(chara_Length, stv1);
  updateCharacteristicLengthTensor(chara_Length_INC, stv1);

  for(int i=0;i<3;i++)
  {
    for(int j=0;j<3;j++){
      ivi->_nldCharacteristicLengthMatrix(i,j) = chara_Length[i][j];                 //by Wu Ling
      iv1->_nldCharacteristicLengthMatrix(i,j) = chara_Length[i][j];                 //by Wu Ling
      iv2->_nldCharacteristicLengthMatrix(i,j) = chara_Length[i][j];                 //by Wu Ling

      ivi->_nldCharacteristicLengthFiber(i,j) = chara_Length_INC[i][j];
      iv1->_nldCharacteristicLengthFiber(i,j) = chara_Length_INC[i][j];
      iv2->_nldCharacteristicLengthFiber(i,j) = chara_Length_INC[i][j];
    }
  }


  ivi->setPosStrMtx(mat->get_pos_mtx_stress());
  ivi->setPosStrInc(mat->get_pos_inc_stress());
  ivi->setPosStnMtx(mat->get_pos_mtx_strain());
  ivi->setPosStnInc(mat->get_pos_inc_strain());
  iv1->setPosStrMtx(mat->get_pos_mtx_stress());
  iv1->setPosStrInc(mat->get_pos_inc_stress());
  iv1->setPosStnMtx(mat->get_pos_mtx_strain());
  iv1->setPosStnInc(mat->get_pos_inc_strain());
  iv1->setPosIncDam(mat->get_pos_inc_Dam());
  iv1->setPosMtxDam(mat->get_pos_mtx_Dam());
  iv2->setPosStrMtx(mat->get_pos_mtx_stress());
  iv2->setPosStrInc(mat->get_pos_inc_stress());
  iv2->setPosStnMtx(mat->get_pos_mtx_strain());
  iv2->setPosStnInc(mat->get_pos_inc_strain());
  ivi->setPosIncVfi(mat->get_pos_vfi());
  ivi->setPosIncAR(mat->get_pos_aspR());
  ivi->setPosIncR(mat->get_pos_Ri());
  ivi->setPosIncJa(mat->get_pos_inc_Ja());
  ivi->setPosMtxJa(mat->get_pos_mtx_Ja());
  ivi->setPosIncDam(mat->get_pos_inc_Dam());
  ivi->setPosMtxDam(mat->get_pos_mtx_Dam());
  iv2->setPosIncVfi(mat->get_pos_vfi());
  iv2->setPosIncAR(mat->get_pos_aspR());
  iv2->setPosIncR(mat->get_pos_Ri());
  iv2->setPosIncJa(mat->get_pos_inc_Ja());
  iv2->setPosMtxJa(mat->get_pos_mtx_Ja());
  iv2->setPosIncDam(mat->get_pos_inc_Dam());
  iv2->setPosMtxDam(mat->get_pos_mtx_Dam());

  copyvect(stv1,stvi,nsdv);
  copyvect(stv1,stv2,nsdv);

}

void mlawNonLocalDamage_Stoch::createIPVariable(const SVector3 &GaussP, IPNonLocalDamage *&ipv,bool hasBodyForce, const MElement *ele,const int nbFF,const IntPt *GP, const int gpt) const
{
  //initialize Eshelby tensor
  int kstep = 1;
  int kinc  = 1;
  double *stv1 = ipv->_nldStatev;
  double SpBartmp = 0.0;
  double dFd_d_bartmp = 0.0;
  double dpdFdtmp = 0.0;
  double dFddptmp = 0.0;

  ipv->setPosMaxD(mat->get_pos_maxD());
  ipv->setPosIncMaxD(mat->get_pos_inc_maxD());
  ipv->setPosMtxMaxD(mat->get_pos_mtx_maxD());

  ipv->setMaxD(_Dmax_Mtx);
  ipv->setIncMaxD(_Dmax_Inc);
  ipv->setMtxMaxD(_Dmax_Mtx);

  if(Randnum !=0){
      ipv->setPos_vfi(pos_vfi);
      ipv->setPos_euler(pos_euler);
      ipv->setPos_aspR(pos_aspR);
      ipv->setPos_ME(pos_ME);
      ipv->setPos_Mnu(pos_Mnu);
      ipv->setPos_Msy0(pos_Msy0);
      ipv->setPos_Mhmod1(pos_Mhmod1);
      ipv->setPos_Mhmod2(pos_Mhmod2);
      ipv->setPos_Malpha_DP(pos_Malpha_DP);
      ipv->setPos_Mm_DP(pos_Mm_DP);
      ipv->setPos_Mnup(pos_Mnup);
      ipv->setPos_Mhexp(pos_Mhexp);
      ipv->setPos_DamParm1(pos_DamParm1);
      ipv->setPos_DamParm2(pos_DamParm2);
      ipv->setPos_DamParm3(pos_DamParm3);
      ipv->setPos_INCDamParm1(pos_INCDamParm1);
      ipv->setPos_INCDamParm2(pos_INCDamParm2);
      ipv->setPos_INCDamParm3(pos_INCDamParm3);
      ipv->setRandnum(Randnum);

    if(Reuler != NULL) Reuler->RandomGen(GaussP, &(stv1[pos_euler])); 

    if(RMatProp !=NULL){
      double Rprop[Randnum];
      int k=0;
      RMatProp->RandomGen(GaussP, Rprop);

      if(pos_vfi !=0) stv1[pos_vfi] = Rprop[k++];
      if(pos_aspR !=0) stv1[pos_aspR] = Rprop[k++];
      if(pos_ME !=0) stv1[pos_ME] = Rprop[k++];
      if(pos_Mnu !=0) stv1[pos_Mnu] = Rprop[k++];
      if(pos_Msy0 !=0) stv1[pos_Msy0] = Rprop[k++];
      if(pos_Mhmod1 !=0) stv1[pos_Mhmod1] = Rprop[k++];
      if(pos_Mhmod2 !=0) stv1[pos_Mhmod2] = Rprop[k++];
      if(pos_Malpha_DP !=0) stv1[pos_Malpha_DP] = Rprop[k++];
      if(pos_Mm_DP !=0) stv1[pos_Mm_DP] = Rprop[k++];
      if(pos_Mnup !=0) stv1[pos_Mnup] = Rprop[k++];
      if(pos_Mhexp !=0) stv1[pos_Mhexp] = Rprop[k++];
      if(pos_DamParm1 !=0) stv1[pos_DamParm1] = Rprop[k++];
      if(pos_DamParm2 !=0) stv1[pos_DamParm2] = Rprop[k++];
      if(pos_DamParm3 !=0) stv1[pos_DamParm3] = Rprop[k];
   } 
   else if(_intpl != 0){
     double param_x = (GaussP(0)-_OrigX)/_dx;
     double param_y = (GaussP(1)-_OrigY)/_dy;

     double x, y;
     double fnx, fny;
     x = modf(param_x , &fnx);
     y = modf(param_y , &fny);
     int nx = (int)fnx;
     int ny = (int)fny; 

     int nx_int = nx;
     int ny_int = ny;
     if(x > 0.5) nx_int +=1;
     if(y > 0.5) ny_int +=1;

       if(pos_Msy0 !=0) stv1[pos_Msy0] = _sy0_Mat(nx_int,ny_int);
       if(pos_Mhmod1 !=0) stv1[pos_Mhmod1] = _hmod1_Mat(nx_int,ny_int);
       if(pos_Mhmod2 !=0) stv1[pos_Mhmod2] = _hmod2_Mat(nx_int,ny_int);
       if(pos_Malpha_DP !=0) stv1[pos_Malpha_DP] = _alpha_DP_Mat(nx_int,ny_int);
       if(pos_Mm_DP !=0) stv1[pos_Mm_DP] = _m_DP_Mat(nx_int,ny_int);
       if(pos_Mnup !=0) stv1[pos_Mnup] = _nup_Mat(nx_int,ny_int);
       if(pos_Mhexp !=0)  stv1[pos_Mhexp] = _hexp_Mat(nx_int,ny_int);
       if(pos_DamParm1 !=0) stv1[pos_DamParm1] = _dam_Parm1_Mat(nx_int,ny_int);
       if(pos_DamParm2 !=0) stv1[pos_DamParm2] = _dam_Parm2_Mat(nx_int,ny_int);
       if(pos_DamParm3 !=0) stv1[pos_DamParm3] = _dam_Parm3_Mat(nx_int,ny_int);

     if (_intpl == 1){ 
       if(pos_vfi !=0){
         stv1[pos_vfi]=_VfMat(nx,ny)*(1.0-x)*(1.0-y)+_VfMat(nx+1,ny)*x*(1.0-y)+_VfMat(nx,ny+1)*(1.0-x)*y+_VfMat(nx+1,ny+1)*x*y;}
       if(pos_aspR !=0){ 
         stv1[pos_aspR]=_aspRMat(nx,ny)*(1.0-x)*(1.0-y)+_aspRMat(nx+1,ny)*x*(1.0-y)+_aspRMat(nx,ny+1)*(1.0-x)*y+_aspRMat(nx+1,ny+1)*x*y;}
       if(pos_ME !=0){
         stv1[pos_ME]=_E_Mat(nx,ny)*(1.0-x)*(1.0-y)+_E_Mat(nx+1,ny)*x*(1.0-y)+_E_Mat(nx,ny+1)*(1.0-x)*y+_E_Mat(nx+1,ny+1)*x*y;}
       if(pos_Mnu !=0){
         stv1[pos_Mnu]=_nu_Mat(nx,ny)*(1.0-x)*(1.0-y)+_nu_Mat(nx+1,ny)*x*(1.0-y)+_nu_Mat(nx,ny+1)*(1.0-x)*y+_nu_Mat(nx+1,ny+1)*x*y;}
   //    if(pos_Msy0 !=0){
   //     stv1[pos_Msy0]=_sy0_Mat(nx,ny)*(1.0-x)*(1.0-y)+_sy0_Mat(nx+1,ny)*x*(1.0-y)+_sy0_Mat(nx,ny+1)*(1.0-x)*y+_sy0_Mat(nx+1,ny+1)*x*y;}
   //    if(pos_Mhmod1 !=0){ 
   //      stv1[pos_Mhmod1]=_hmod1_Mat(nx,ny)*(1.0-x)*(1.0-y)+_hmod1_Mat(nx+1,ny)*x*(1.0-y)+_hmod1_Mat(nx,ny+1)*(1.0-x)*y+_hmod1_Mat(nx+1,ny+1)*x*y;}
   //    if(pos_Mhmod2 !=0){ 
   //      stv1[pos_Mhmod2]=_hmod2_Mat(nx,ny)*(1.0-x)*(1.0-y)+_hmod2_Mat(nx+1,ny)*x*(1.0-y)+_hmod2_Mat(nx,ny+1)*(1.0-x)*y+_hmod2_Mat(nx+1,ny+1)*x*y;}
   //    if(pos_Mhexp !=0){
   //      stv1[pos_Mhexp]=_hexp_Mat(nx,ny)*(1.0-x)*(1.0-y)+_hexp_Mat(nx+1,ny)*x*(1.0-y)+_hexp_Mat(nx,ny+1)*(1.0-x)*y+_hexp_Mat(nx+1,ny+1)*x*y;}
   //    if(pos_DamParm1 !=0){
    //     stv1[pos_DamParm1] = _dam_Parm1_Mat(nx,ny)*(1.0-x)*(1.0-y)+_dam_Parm1_Mat(nx+1,ny)*x*(1.0-y)+_dam_Parm1_Mat(nx,ny+1)*(1.0-x)*y+_dam_Parm1_Mat(nx+1,ny+1)*x*y;}
   //    if(pos_DamParm2 !=0){
  //       stv1[pos_DamParm2] = _dam_Parm2_Mat(nx,ny)*(1.0-x)*(1.0-y)+_dam_Parm2_Mat(nx+1,ny)*x*(1.0-y)+_dam_Parm2_Mat(nx,ny+1)*(1.0-x)*y+ _dam_Parm2_Mat(nx+1,ny+1)*x*y;}

       if(pos_euler !=0){
         stv1[pos_euler]=_euler_Mat(nx,ny)*(1.0-x)*(1.0-y)+_euler_Mat(nx+1,ny)*x*(1.0-y)+_euler_Mat(nx,ny+1)*(1.0-x)*y+_euler_Mat(nx+1,ny+1)*x*y;
         stv1[pos_euler+1]= 0.0;
         stv1[pos_euler+2]= 0.0;
       }
     }
     else if (_intpl ==2 ){
       if(x >= 0.5){nx=nx+1;}
       if(y >= 0.5){ny=ny+1;}

       if(pos_vfi !=0)  stv1[pos_vfi] = _VfMat(nx,ny);
       if(pos_aspR !=0) stv1[pos_aspR] = _aspRMat(nx,ny);
       if(pos_ME !=0) stv1[pos_ME] = _E_Mat(nx,ny);
       if(pos_Mnu !=0) stv1[pos_Mnu] = _nu_Mat(nx,ny);
     //  if(pos_Msy0 !=0) stv1[pos_Msy0] = _sy0_Mat(nx,ny);
    //   if(pos_Mhmod1 !=0) stv1[pos_Mhmod1] = _hmod1_Mat(nx,ny);
    //   if(pos_Mhmod2 !=0) stv1[pos_Mhmod2] = _hmod2_Mat(nx,ny);
    //   if(pos_Mhexp !=0)  stv1[pos_Mhexp] = _hexp_Mat(nx,ny);
    //   if(pos_DamParm1 !=0)  stv1[pos_DamParm1] = _dam_Parm1_Mat(nx,ny);
   //    if(pos_DamParm2 !=0)  stv1[pos_DamParm2] = _dam_Parm2_Mat(nx,ny);

       if(pos_euler !=0)  {
           stv1[pos_euler] = _euler_Mat(nx,ny);
           stv1[pos_euler+1]= 0.0;
           stv1[pos_euler+2]= 0.0;
       }
     }

     else if (_intpl == 3 ){
       double alpha = 0.1;
       double min = 0.001;
       double max = 0.999;
       if(x > 0.5){nx=nx+1;}
       if(y > 0.5){ny=ny+1;}
       if (x <=min or x >= max){
         if (y <=min or y >= max){
           if(pos_vfi !=0)  stv1[pos_vfi] = _VfMat(nx,ny);
           if(pos_aspR !=0) stv1[pos_aspR] = _aspRMat(nx,ny);
           if(pos_ME !=0) stv1[pos_ME] = _E_Mat(nx,ny);
           if(pos_Mnu !=0) stv1[pos_Mnu] = _nu_Mat(nx,ny);
    //       if(pos_Msy0 !=0) stv1[pos_Msy0] = _sy0_Mat(nx,ny);
    //       if(pos_Mhmod1 !=0) stv1[pos_Mhmod1] = _hmod1_Mat(nx,ny);
    //       if(pos_Mhmod2 !=0) stv1[pos_Mhmod2] = _hmod2_Mat(nx,ny);
    //       if(pos_Mhexp !=0)  stv1[pos_Mhexp] = _hexp_Mat(nx,ny);
    //       if(pos_DamParm1 !=0)
    //       {  
    //         stv1[pos_DamParm1] = _dam_Parm1_Mat(nx,ny);
           //  if(stv1[pos_DamParm1]<=0.)
           //      printf("Negative damage energy: %e\n", _dam_Parm1_Mat(nx,ny));
    //       }
      //     if(pos_DamParm2 !=0)
      //     {
     //        stv1[pos_DamParm2] = _dam_Parm2_Mat(nx,ny);
         //    if(stv1[pos_DamParm2]<=0.)
          //       printf("Negative damage exponent: %e\n", _dam_Parm2_Mat(nx,ny));
     //      }
           if(pos_euler !=0)  {
                 stv1[pos_euler] = _euler_Mat(nx,ny);
                 stv1[pos_euler+1]= 0.0;
                 stv1[pos_euler+2]= 0.0;
           } 
         }
         else{
             int ny0; 
             if( y >0.5 ){
                 ny0 = ny-1;
                 if(ny0 < 0){ny0=0;}
              }
             if( y <=0.5){
                 ny0 = ny;
                 ny = ny+1;
              }   
           double coef = 0.5*(1.0+tanh((y-0.5)/alpha));

           if(pos_vfi !=0)  stv1[pos_vfi] = _VfMat(nx,ny0) + (_VfMat(nx,ny)-_VfMat(nx,ny0))*coef;
           if(pos_aspR !=0) stv1[pos_aspR] = _aspRMat(nx,ny0) + (_aspRMat(nx,ny)-_aspRMat(nx,ny0))*coef;
           if(pos_ME !=0) stv1[pos_ME] = _E_Mat(nx,ny0) + (_E_Mat(nx,ny)-_E_Mat(nx,ny0))*coef;
           if(pos_Mnu !=0) stv1[pos_Mnu] = _nu_Mat(nx,ny0) + (_nu_Mat(nx,ny)-_nu_Mat(nx,ny0))*coef;
    //       if(pos_Msy0 !=0) stv1[pos_Msy0] = _sy0_Mat(nx,ny0) + (_sy0_Mat(nx,ny)-_sy0_Mat(nx,ny0))*coef;
     //      if(pos_Mhmod1 !=0) stv1[pos_Mhmod1] = _hmod1_Mat(nx,ny0) + (_hmod1_Mat(nx,ny)-_hmod1_Mat(nx,ny0))*coef;
     //      if(pos_Mhmod2 !=0) stv1[pos_Mhmod2] = _hmod2_Mat(nx,ny0) + (_hmod2_Mat(nx,ny)-_hmod2_Mat(nx,ny0))*coef;
     //      if(pos_Mhexp !=0)  stv1[pos_Mhexp] = _hexp_Mat(nx,ny0) + (_hexp_Mat(nx,ny)-_hexp_Mat(nx,ny0))*coef;

     //      if(pos_DamParm1 !=0)
    //       { 
    //         stv1[pos_DamParm1] = _dam_Parm1_Mat(nx,ny0) + (_dam_Parm1_Mat(nx,ny)-_dam_Parm1_Mat(nx,ny0))*coef;
            // if(stv1[pos_DamParm1]<=0.)
             //    printf("Negative damage energy: %e,%e,%e\n", _dam_Parm1_Mat(nx,ny0),_dam_Parm1_Mat(nx,ny),coef);
      //     }
       //    if(pos_DamParm2 !=0)
      //     {
      //       stv1[pos_DamParm2] = _dam_Parm2_Mat(nx,ny0) + (_dam_Parm2_Mat(nx,ny)-_dam_Parm2_Mat(nx,ny0))*coef;
         //    if(stv1[pos_DamParm2]<=0.)
          //     printf("Negative damage energy: %e,%e,%e\n", _dam_Parm2_Mat(nx,ny0),_dam_Parm2_Mat(nx,ny),coef);
      //     }
           if(pos_euler !=0) {
              stv1[pos_euler] = _euler_Mat(nx,ny0) + (_euler_Mat(nx,ny)-_euler_Mat(nx,ny0))*coef;
              stv1[pos_euler+1]= 0.0;
              stv1[pos_euler+2]= 0.0;
           }
         }
       }
       else{
         if (y <=min or y >= max){
            int nx0;
            if( x >0.5 ){
                nx0 = nx-1;
                if(nx0 < 0){nx0=0;}
            }
            if( x <=0.5){
                nx0 = nx;
                nx = nx+1;
            } 
            double coef = 0.5*(1.0+tanh((x-0.5)/alpha));

           if(pos_vfi !=0)  stv1[pos_vfi] = _VfMat(nx0,ny) + (_VfMat(nx,ny)-_VfMat(nx0,ny))*coef;
           if(pos_aspR !=0) stv1[pos_aspR] = _aspRMat(nx0,ny) + (_aspRMat(nx,ny)-_aspRMat(nx0,ny))*coef;
           if(pos_ME !=0) stv1[pos_ME] = _E_Mat(nx0,ny) + (_E_Mat(nx,ny)-_E_Mat(nx0,ny))*coef;
           if(pos_Mnu !=0) stv1[pos_Mnu] = _nu_Mat(nx0,ny) + (_nu_Mat(nx,ny)-_nu_Mat(nx0,ny))*coef;
     //      if(pos_Msy0 !=0) stv1[pos_Msy0] = _sy0_Mat(nx0,ny) + (_sy0_Mat(nx,ny)-_sy0_Mat(nx0,ny))*coef;
     //      if(pos_Mhmod1 !=0) stv1[pos_Mhmod1] = _hmod1_Mat(nx0,ny) + (_hmod1_Mat(nx,ny)-_hmod1_Mat(nx0,ny))*coef;
     //      if(pos_Mhmod2 !=0) stv1[pos_Mhmod2] = _hmod2_Mat(nx0,ny) + (_hmod2_Mat(nx,ny)-_hmod2_Mat(nx0,ny))*coef;
     //      if(pos_Mhexp !=0)  stv1[pos_Mhexp] = _hexp_Mat(nx0,ny) + (_hexp_Mat(nx,ny)-_hexp_Mat(nx0,ny))*coef;
     //      if(pos_DamParm1 !=0){ 
    //          stv1[pos_DamParm1] = _dam_Parm1_Mat(nx0,ny) + (_dam_Parm1_Mat(nx,ny)-_dam_Parm1_Mat(nx0,ny))*coef;
            //  if(stv1[pos_DamParm1]<=0.)
             //    printf("Negative damage energy: %e,%e,%e\n", _dam_Parm1_Mat(nx0,ny),_dam_Parm1_Mat(nx,ny),coef);
 
   //        }
   //        if(pos_DamParm2 !=0)
   //        { 
   //          stv1[pos_DamParm2] = _dam_Parm2_Mat(nx0,ny) + (_dam_Parm2_Mat(nx,ny)-_dam_Parm2_Mat(nx0,ny))*coef;
          //   if(stv1[pos_DamParm2]<=0.)
            //     printf("Negative damage exponent: %e,%e,%e\n", _dam_Parm2_Mat(nx0,ny),_dam_Parm2_Mat(nx,ny),coef);
   //        }
           if(pos_euler !=0)  {
                stv1[pos_euler] = _euler_Mat(nx0,ny) + (_euler_Mat(nx,ny)-_euler_Mat(nx0,ny))*coef;
                stv1[pos_euler+1]= 0.0;
                stv1[pos_euler+2]= 0.0;
           }
         }
         else{  
            int nx0, ny0;
            if( x >0.5 ){
                nx0 = nx-1;
                if(nx0 < 0){nx0=0;}
            }
            if( x <=0.5){
                nx0 = nx;
                nx = nx+1;
            } 
            if( y >0.5 ){
                ny0 = ny-1;
                if(ny0 < 0){ny0=0;}
            }
            if( y <=0.5){
                ny0 = ny;
                ny = ny+1;
            }  
            double cx,cy,s,t;
            cx = 0.5*(1.0+tanh((x-0.5)/alpha));
            cy = 0.5*(1.0+tanh((y-0.5)/alpha));
            s = (x-min)/(max-min); 
            t = (y-min)/(max-min); 
            double c00 = (1.0-cx)*(1.0-t) + (1.0-cy)*(1.0-s) - (1.0-s)*(1.0-t);
            double c01 = (1.0-cx)*t + cy*(1.0-s) - (1.0-s)*t;
            double c10 = cx*(1.0-t) + (1.0-cy)*s - s*(1.0-t);
            double c11 = cx*t + cy*s - s*t;
            if(c00 <0.0) c00 = 0.00;
            if(c10 <0.0) c10 = 0.00;        
            if(c01 <0.0) c01 = 0.00;
            if(c11 <0.0) c11 = 0.00;
            if(pos_vfi !=0){
               stv1[pos_vfi] = c00*_VfMat(nx0,ny0) + c01*_VfMat(nx0,ny) + c10*_VfMat(nx,ny0) + c11*_VfMat(nx,ny);}
            if(pos_aspR !=0){
               stv1[pos_aspR] = c00*_aspRMat(nx0,ny0) + c01*_aspRMat(nx0,ny) + c10*_aspRMat(nx,ny0) + c11*_aspRMat(nx,ny);}
            if(pos_ME !=0){
               stv1[pos_ME] = c00*_E_Mat(nx0,ny0) + c01*_E_Mat(nx0,ny) + c10*_E_Mat(nx,ny0) + c11*_E_Mat(nx,ny);}
            if(pos_Mnu !=0){
               stv1[pos_Mnu] = c00*_nu_Mat(nx0,ny0) + c01*_nu_Mat(nx0,ny) + c10*_nu_Mat(nx,ny0) + c11*_nu_Mat(nx,ny);}
    //        if(pos_Msy0 !=0){
     //          stv1[pos_Msy0] = c00*_sy0_Mat(nx0,ny0) + c01*_sy0_Mat(nx0,ny) + c10*_sy0_Mat(nx,ny0) + c11*_sy0_Mat(nx,ny);}
     //       if(pos_Mhmod1 !=0){
     //          stv1[pos_Mhmod1] = c00*_hmod1_Mat(nx0,ny0) + c01*_hmod1_Mat(nx0,ny) + c10*_hmod1_Mat(nx,ny0) + c11*_hmod1_Mat(nx,ny);}
       //     if(pos_Mhmod2 !=0){
       //        stv1[pos_Mhmod2] = c00*_hmod2_Mat(nx0,ny0) + c01*_hmod2_Mat(nx0,ny) + c10*_hmod2_Mat(nx,ny0) + c11*_hmod2_Mat(nx,ny);}
       //     if(pos_Mhexp !=0){
       //        stv1[pos_Mhexp] = c00*_hexp_Mat(nx0,ny0) + c01*_hexp_Mat(nx0,ny) + c10*_hexp_Mat(nx,ny0) + c11*_hexp_Mat(nx,ny);}

        //    if(pos_DamParm1 !=0){
        //      stv1[pos_DamParm1] = c00*_dam_Parm1_Mat(nx0,ny0) +c01*_dam_Parm1_Mat(nx0,ny)+ c10*_dam_Parm1_Mat(nx,ny0)+c11*_dam_Parm1_Mat(nx,ny);
            //  if(stv1[pos_DamParm1]<=0.)
            //     printf("Negative damage energy: %e,%e,%e,%e,%e,%e,%e,%e\n", c00,_dam_Parm1_Mat(nx0,ny0),c01,_dam_Parm1_Mat(nx0,ny),c10,_dam_Parm1_Mat(nx,ny0),c11,_dam_Parm1_Mat(nx,ny));
        //    }

       //    if(pos_DamParm2 !=0){
       //       stv1[pos_DamParm2] = c00*_dam_Parm2_Mat(nx0,ny0) + c01*_dam_Parm2_Mat(nx0,ny)+c10*_dam_Parm2_Mat(nx,ny0)+c11*_dam_Parm2_Mat(nx,ny);
             // if(stv1[pos_DamParm2]<=0.)
             //    printf("Negative exponent: %e,%e,%e,%e,%e,%e,%e,%e\n", c00,_dam_Parm2_Mat(nx0,ny0),c01,_dam_Parm2_Mat(nx0,ny),c10,_dam_Parm2_Mat(nx,ny0),c11,_dam_Parm2_Mat(nx,ny));

       //    }


            if(pos_euler !=0){
               stv1[pos_euler] = c00*_euler_Mat(nx0,ny0) + c01*_euler_Mat(nx0,ny) + c10*_euler_Mat(nx,ny0) + c11*_euler_Mat(nx,ny);
               stv1[pos_euler+1]= 0.0;
               stv1[pos_euler+2]= 0.0;
            }
          }
        }
      }
      else {
         printf("Unknown interpolation method: %d\n", _intpl);
      }	

    if(pos_INCDamParm1 !=0){
      std::random_device rd; 
      std::mt19937 gen(rd()); 
      std::normal_distribution<double> d(0.0, 1.0); 
      // get random number with normal distribution using gen as random source
      if(pos_vfi != 0){
      stv1[pos_INCDamParm1] = int(stv1[pos_vfi]*0.25*0.3/2.0*1000000/3.141592/3.5/3.5); 
      }
      else{
      stv1[pos_INCDamParm1] = 100;
      }  
      stv1[pos_INCDamParm2] =  d(gen);
      }

    }
  } 
                       
                
  mat->constbox(dstrn, strs_n, strs, stv1, stv1, Cref, dCref, tau, dtau, Calgo, 1., dpdE, strs_dDdp_bar, &SpBartmp, chara_Length, &dFd_d_bartmp, dstrs_dFd_bar, dFd_dE, chara_Length_INC, &dpdFdtmp, &dFddptmp, kinc, kstep, _timeStep);


  ipv->setPosEuler(pos_euler);

  updateCharacteristicLengthTensor(chara_Length, stv1);
  updateCharacteristicLengthTensor(chara_Length_INC, stv1);
  for(int i=0;i<3;i++)
  {
    for(int j=0;j<3;j++){
      ipv->_nldCharacteristicLengthMatrix(i,j) = chara_Length[i][j];                 //by Wu Ling
      ipv->_nldCharacteristicLengthFiber(i,j) = chara_Length_INC[i][j];
    }
  }

  ipv->setPosStrMtx(mat->get_pos_mtx_stress());
  ipv->setPosStrInc(mat->get_pos_inc_stress());
  ipv->setPosStnMtx(mat->get_pos_mtx_strain());
  ipv->setPosStnInc(mat->get_pos_inc_strain());
  ipv->setPosIncVfi(mat->get_pos_vfi());
  ipv->setPosIncAR(mat->get_pos_aspR());
  ipv->setPosIncR(mat->get_pos_Ri());
  ipv->setPosIncJa(mat->get_pos_inc_Ja());
  ipv->setPosMtxJa(mat->get_pos_mtx_Ja());
  ipv->setPosIncDam(mat->get_pos_inc_Dam());
  ipv->setPosMtxDam(mat->get_pos_mtx_Dam());

}
void mlawNonLocalDamage_Stoch::updateCharacteristicLengthTensor(double** chara_Length, double *stv) const 
{
  if(pos_euler != 0)
  {
    //Msg::Info("Non local lengthsi nitial %e, %e, %e, %e, %e, %e, %e, %e, %e", chara_Length[0][0], chara_Length[0][1], chara_Length[0][2], chara_Length[1][0], chara_Length[1][1], chara_Length[1][2], chara_Length[2][0], chara_Length[2][1], chara_Length[2][2]);
    //if(_Euler_tmp==NULL) Msg::Error("mlawNonLocalDamage_Stoch::updateCharacteristicLengthTensor: _Euler_tmp==NULL");
    
    mallocvector(&_Euler_tmp,3);
    mallocmatrix(&_Euler_tmp2,3,3);
    mallocmatrix(&_Euler_tmp3,3,3);
        
    _Euler_tmp[0]=stv[pos_euler];
    _Euler_tmp[1]=stv[pos_euler+1];
    _Euler_tmp[2]=stv[pos_euler+2];
    //Msg::Info("Euler angle for non local lengths %f, %f, %f", _Euler_tmp[0], _Euler_tmp[1], _Euler_tmp[2]);
    for(int i=0; i<3; i++)
    {
      for(int j=0; j<3; j++)
      {
         _Euler_tmp2[i][j]=chara_Length[i][j];
      }
    }

    eul_mat33(_Euler_tmp, _Euler_tmp3);
    transpose(_Euler_tmp3,_Euler_tmp3,3,3);  
    rot33(_Euler_tmp3,_Euler_tmp2, chara_Length);
    
    delete _Euler_tmp;
    _Euler_tmp = NULL;
    for(int i=0;i<3;i++)
    {
      delete _Euler_tmp2[i];
    }
    delete _Euler_tmp2;
    _Euler_tmp2 = NULL;
    for(int i=0;i<3;i++)
    {
      delete _Euler_tmp3[i];
    }
    delete _Euler_tmp3;
    _Euler_tmp3 = NULL;

    
    //Msg::Info("Non local lengths %e, %e, %e, %e, %e, %e, %e, %e, %e", chara_Length[0][0], chara_Length[0][1], chara_Length[0][2], chara_Length[1][0], chara_Length[1][1], chara_Length[1][2], chara_Length[2][0], chara_Length[2][1], chara_Length[2][2]);
  }
}

void mlawNonLocalDamage_Stoch::getLocalOrientation(fullVector<double> &GaussP, fullVector<double> &vecEulerAngles) const
{
  double* vec = NULL;
  mallocvector(&vec,3);
  
  SVector3 GP;
  GP(0) = GaussP(0);
  GP(1) = GaussP(1);
  GP(2) = GaussP(2);
  
  if(Reuler != NULL){
  Reuler->RandomGen(GP,vec);}
  else{
  vec[0] = 0.;
  vec[1] = 0.;
  vec[2] = 0.;
  }
  
  vecEulerAngles(0) = vec[0];
  vecEulerAngles(1) = vec[1];
  vecEulerAngles(2) = vec[2];
  
  delete vec;
}

