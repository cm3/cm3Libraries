//
// C++ Interface: material law
//
// Description: local damage law
//
// Author:  <V-D Nguyen>, (C) 2021
//
// Copyright: See COPYING file that comes with this distribution
//
//

#include "mlawLocalDamageIsotropicElasticity.h"

#include <math.h>
#include "MInterfaceElement.h"
#include "nonLinearMechSolver.h"


mlawLocalDamageIsotropicElasticity::mlawLocalDamageIsotropicElasticity(const int num,const double rho, const double E,
        const double nu,  const DamageLaw& damLaw)
    :materialLaw(num, true), _E(E),_nu(nu), _rho(rho), _ElasticityTensor(0.)
{
    _damLaw = damLaw.clone();
    _ElasticityTensor(0,0,0,0) = (1.-_nu)*_E/(1.+_nu)/(1.-2.*_nu);
    _ElasticityTensor(1,1,1,1) = _ElasticityTensor(0,0,0,0);
    _ElasticityTensor(2,2,2,2) = _ElasticityTensor(0,0,0,0);

    _ElasticityTensor(0,0,1,1) = nu*_E/(1.+_nu)/(1.-2.*_nu);
    _ElasticityTensor(0,0,2,2) = _ElasticityTensor(0,0,1,1);
    _ElasticityTensor(1,1,0,0) = _ElasticityTensor(0,0,1,1);
    _ElasticityTensor(1,1,2,2) = _ElasticityTensor(0,0,1,1);
    _ElasticityTensor(2,2,0,0) = _ElasticityTensor(0,0,1,1);
    _ElasticityTensor(2,2,1,1) = _ElasticityTensor(0,0,1,1);

    double mu = 0.5*_E/(1.+_nu);
    _ElasticityTensor(0,1,0,1) = mu;
    _ElasticityTensor(0,1,1,0) = mu;
    _ElasticityTensor(1,0,0,1) = mu;
    _ElasticityTensor(1,0,1,0) = mu;
    _ElasticityTensor(0,2,0,2) = mu;
    _ElasticityTensor(0,2,2,0) = mu;
    _ElasticityTensor(2,0,0,2) = mu;
    _ElasticityTensor(2,0,2,0) = mu;

    _ElasticityTensor(1,2,1,2) = mu;
    _ElasticityTensor(1,2,2,1) = mu;
    _ElasticityTensor(2,1,1,2) = mu;
    _ElasticityTensor(2,1,2,1) = mu;
}


mlawLocalDamageIsotropicElasticity::mlawLocalDamageIsotropicElasticity(const mlawLocalDamageIsotropicElasticity& source)
    : materialLaw(source)
{
  _rho = source._rho;
  _E = source._E;
  _nu = source._nu;
  _ElasticityTensor = source._ElasticityTensor;
  _damLaw = NULL;
  if(source._damLaw != NULL)
  {
    _damLaw=source._damLaw->clone();
  }
}


void mlawLocalDamageIsotropicElasticity::createIPState(IPStateBase* &ips, bool hasBodyForce, const bool* state_,const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
    bool inter=true;
    const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
    if(iele==NULL)
        {
            inter=false;
        };
    IPVariable* ipvi = new IPLocalDamageIsotropicElasticity(_damLaw);
    IPVariable* ipv1 = new IPLocalDamageIsotropicElasticity(_damLaw);
    IPVariable* ipv2 = new IPLocalDamageIsotropicElasticity(_damLaw);
    if(ips != NULL) delete ips;
    ips = new IP3State(state_,ipvi,ipv1,ipv2);

}

void mlawLocalDamageIsotropicElasticity::createIPState(IPLocalDamageIsotropicElasticity* ivi, IPLocalDamageIsotropicElasticity* iv1,
        IPLocalDamageIsotropicElasticity* iv2) const
{

}

void mlawLocalDamageIsotropicElasticity::createIPVariable(IPLocalDamageIsotropicElasticity*& ipv) const
{

}

double mlawLocalDamageIsotropicElasticity::soundSpeed() const
{
    double factornu = (1.-_nu)/((1.+_nu)*(1.-2.*_nu));
    return sqrt(_E*factornu/_rho);
}


double mlawLocalDamageIsotropicElasticity::deformationEnergy(const STensor3 &epsilon_strains, const STensor3 &sigma) const
{
    // psi = 0.5 sigma_ij epsilon_ij
    double En(0.);
    for(int i=0; i<3; i++)
    {
        for(int j=0; j<3; j++)
        {
            En += sigma(i,j)*epsilon_strains(i,j);
        }
    }
    En *= 0.5;
    return En;
}


void  mlawLocalDamageIsotropicElasticity::constitutive(
            const STensor3& F0,                             // initial deformation gradient (input @ time n)
            const STensor3& F1,                             // updated deformation gradient (input @ time n+1)
            STensor3 &P,                                    // updated 1st Piola-Kirchhoff stress tensor (output)
            const IPVariable *q0i,  // array of initial internal variable (in ipvprev on input)
            IPVariable *q1i,        // updated array of internal variable (in ipvcur on output),
            STensor43 &Tangent,                             // constitutive tangents (output) // partial stress / partial strains
            const bool stiff, 
            STensor43* elasticTangent, 
            const bool dTangent,
            STensor63* dCalgdeps) const
{
  const IPLocalDamageIsotropicElasticity *q0 = dynamic_cast<const IPLocalDamageIsotropicElasticity *>(q0i);
  IPLocalDamageIsotropicElasticity *q1 = dynamic_cast<IPLocalDamageIsotropicElasticity *>(q1i);
  // - Strains computation
  static STensor3 epsilon_strains;
  for(int i=0; i<3; i++)
  {
    for(int j=0; j<3; j++)
    {
      epsilon_strains(i,j) = F1(j,i);
    }
  }
  epsilon_strains += F1;
  epsilon_strains *= 0.5;
  epsilon_strains(0,0) -= 1;
  epsilon_strains(1,1) -= 1;
  epsilon_strains(2,2) -= 1;

  // - Effective stress computation
  static STensor3 Peff;
  STensorOperation::multSTensor43STensor3(_ElasticityTensor,epsilon_strains,Peff);


  // - Local effective strains
  static fullMatrix<double> m(3, 3);
  static fullVector<double> eigenValReal(3), eigenValImag(3);
  static fullMatrix<double> leftEigenVect(3, 3), rightEigenVect(3, 3);
  epsilon_strains.getMat(m);

  // solving eigen problem
  m.eig(eigenValReal,eigenValImag,leftEigenVect,rightEigenVect,0);

  // norm computation including positive eigenvalues only
  double equivalentStrainsSquared(0.);
  for (int i=0; i<3; i++)
  {
    if (eigenValReal(i) > 0.)
    {
      equivalentStrainsSquared += eigenValReal(i)*eigenValReal(i);
    }
  }
  // modify ipv value
  q1->getRefToLocalEffectiveStrains() = sqrt(equivalentStrainsSquared);

  // - Elastic energy release rate
  double effEner = deformationEnergy(epsilon_strains,Peff);

  // - Damage computation and transition managing
  if (q1->dissipationIsBlocked())
  {
  // If damage evolution is blocked (ie. bulk near a crack):
    // ==> keep damage constant
    IPDamage& curDama = q1->getRefToIPDamage();
    curDama.getRefToDamage() = q0->getDamage();
    curDama.getRefToDDamageDp() = 0.;
    STensorOperation::zero(curDama.getRefToDDamageDFe());
    curDama.getRefToDeltaDamage() = 0;
    curDama.getRefToMaximalP() = q0->getMaximalP();
    q1->activeDamage(false);
  }
  else
  {
    // While critical damage not reached, damage is comp. with non-local value (as usual)
    _damLaw->computeDamage(q1->getLocalEffectiveStrains(), q0->getLocalEffectiveStrains(),
                           effEner, F1, 0., Peff, _ElasticityTensor,
                           q0->getConstRefToIPDamage(), q1->getRefToIPDamage());
    if (q1->getDamage() > q0->getDamage())
    {
      q1->activeDamage(true);
    }
    else
    {
      q1->activeDamage(false);
    }
  }

  double D = q1->getDamage();
  P = Peff;
  P *= (1.-D);

  // - Stored energy computation
  q1->getRefToElasticEnergy() = effEner*(1-D);
  static STensor3 dLocalEffectiveStrainsDStrains;
  // - Tangent data computation
  if (stiff)
  {
    // partial stress / partial strains
    Tangent = _ElasticityTensor * (1.-D);
    if(elasticTangent !=NULL) elasticTangent->operator=(Tangent);
    // partial stress / partial Non-Local Effective strains

    // partial Local Effective Strains / partial Strains
    STensorOperation::zero(dLocalEffectiveStrainsDStrains);
    for (int k=0; k<3; k++)
    {
      if (eigenValReal(k) > 0.0)
      {
        double norm_V  = (leftEigenVect(0,k)*rightEigenVect(0,k) + leftEigenVect(1,k)*rightEigenVect(1,k)+leftEigenVect(2,k)*rightEigenVect(2,k));
        for (int i=0; i<3; i++)
        {
          for (int j=0; j<3; j++)
          {
            dLocalEffectiveStrainsDStrains(i,j) += leftEigenVect(i,k)*rightEigenVect(j,k)*eigenValReal(k)/norm_V/q1->getLocalEffectiveStrains();
          }
        }
      }
    }
    for (int i=0; i<3; i++)
    {
      for (int j=0; j<3; j++)
      {
        for (int k=0; k<3; k++)
        {
          for (int l=0; l<3; l++)
          {
            Tangent(i,j,k,l) += -q1->getDDamageDp() * Peff(i,j) * dLocalEffectiveStrainsDStrains(k,l);
          }
        }
      }
    }
  
    /*if (dTangent)
    {
      Msg::Error("computation with dTangent is not implemented!");
    }*/
  }

  q1->getRefToDamageEnergy() = q0->damageEnergy() + effEner* (D-q0->getDamage());

  // Path following only - irreversible energy
  // Dissipated energy = previous dissipated energy + Y delta_D
  /*if (this->getMacroSolver()->getPathFollowingLocalIncrementType() == pathFollowingManager::DEFO_ENERGY){
    q1->getRefToIrreversibleEnergy() = q1->defoEnergy();
  }
  else if ((this->getMacroSolver()->getPathFollowingLocalIncrementType() == pathFollowingManager::DISSIPATION_ENERGY) or
          (this->getMacroSolver()->getPathFollowingLocalIncrementType() == pathFollowingManager::DAMAGE_ENERGY)){
    q1->getRefToIrreversibleEnergy() = q1->damageEnergy();

  }
  else{
    q1->getRefToIrreversibleEnergy() = 0.;
  }


  if (stiff)
  {
    STensor3& DIrrevEnergyDF = q1->getRefToDIrreversibleEnergyDF();
    DIrrevEnergyDF = Peff;
    if (this->getMacroSolver()->getPathFollowingLocalIncrementType() == pathFollowingManager::DEFO_ENERGY){
      DIrrevEnergyDF*= (1.-D);
      DIrrevEnergyDF.daxpy(dLocalEffectiveStrainsDStrains,-q1->getDDamageDp()*effEner);
    }
    else if ((this->getMacroSolver()->getPathFollowingLocalIncrementType() == pathFollowingManager::DISSIPATION_ENERGY) or
          (this->getMacroSolver()->getPathFollowingLocalIncrementType() == pathFollowingManager::DAMAGE_ENERGY)){
      DIrrevEnergyDF *= (D - q0->getDamage());
      DIrrevEnergyDF.daxpy(dLocalEffectiveStrainsDStrains,q1->getDDamageDp()*effEner);
    }
    else{
      DIrrevEnergyDF = 0.;
    }
  }*/
};
