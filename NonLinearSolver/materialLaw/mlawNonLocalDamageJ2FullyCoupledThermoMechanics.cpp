//
// C++ Interface: material law
//
// Description: j2 thermo-elasto-plastic law with non local damage interface
//
// Author:  <L. Noels>, (C) 2021
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "mlawNonLocalDamageJ2FullyCoupledThermoMechanics.h"
#include <math.h>
#include "MInterfaceElement.h"
#include "nonLinearMechSolver.h"

mlawNonLocalDamageJ2FullyCoupledThermoMechanics::mlawNonLocalDamageJ2FullyCoupledThermoMechanics(const int num,const double E,const double nu,
                           const double rho, const double sy0,const double h,
                           const CLengthLaw &_clLaw, const DamageLaw &_damLaw,
			   const double tol, const bool pert, const double eps)
				: mlawJ2FullyCoupledThermoMechanics(num,E,nu,rho,sy0,h,tol,pert,eps)
{
  cLLaw = _clLaw.clone();
  damLaw = _damLaw.clone();


}
mlawNonLocalDamageJ2FullyCoupledThermoMechanics::mlawNonLocalDamageJ2FullyCoupledThermoMechanics(const mlawNonLocalDamageJ2FullyCoupledThermoMechanics &source) : mlawJ2FullyCoupledThermoMechanics(source)
{
  if(source.cLLaw != NULL)
  {
    cLLaw=source.cLLaw->clone();
  }
  if(source.damLaw != NULL)
  {
    damLaw=source.damLaw->clone();
  }
}

mlawNonLocalDamageJ2FullyCoupledThermoMechanics& mlawNonLocalDamageJ2FullyCoupledThermoMechanics::operator = (const materialLaw &source)
{
  mlawJ2FullyCoupledThermoMechanics::operator=(source);
  const mlawNonLocalDamageJ2FullyCoupledThermoMechanics* src = dynamic_cast<const mlawNonLocalDamageJ2FullyCoupledThermoMechanics*>(&source);
  if(src != NULL)
  {
    if(cLLaw != NULL) delete cLLaw;
    if(src->cLLaw != NULL)
    {
      cLLaw=src->cLLaw->clone();
    }
    if(damLaw != NULL) delete damLaw;
    if(src->damLaw != NULL)
    {
      damLaw=src->damLaw->clone();
    }
  }
  return *this;
}
void mlawNonLocalDamageJ2FullyCoupledThermoMechanics::evaluateCel(STensor43 &Cel, double T) const
{
  double muT = _mu; // remove temperature dependence on damage law at first *_temFunc_mu->getVal(T);
  double KT = _K; // remove temperature dependence on damage law at first *_temFunc_K->getVal(T);
  double mu2T=2*muT;
  double lambda= KT-2./3.*muT;
  Cel=0.;
  Cel(0,0,0,0) = lambda + mu2T;
  Cel(1,1,0,0) = lambda;
  Cel(2,2,0,0) = lambda;
  Cel(0,0,1,1) = lambda;
  Cel(1,1,1,1) = lambda + mu2T;
  Cel(2,2,1,1) = lambda;
  Cel(0,0,2,2) = lambda;
  Cel(1,1,2,2) = lambda;
  Cel(2,2,2,2) = lambda + mu2T;

  Cel(1,0,1,0) = muT;
  Cel(2,0,2,0) = muT;
  Cel(0,1,0,1) = muT;
  Cel(2,1,2,1) = muT;
  Cel(0,2,0,2) = muT;
  Cel(1,2,1,2) = muT;

  Cel(0,1,1,0) = muT;
  Cel(0,2,2,0) = muT;
  Cel(1,0,0,1) = muT;
  Cel(1,2,2,1) = muT;
  Cel(2,0,0,2) = muT;
  Cel(2,1,1,2) = muT;
}
double mlawNonLocalDamageJ2FullyCoupledThermoMechanics::scaleFactor() const{
  double muT = _mu; 
  return muT;
};

  
void mlawNonLocalDamageJ2FullyCoupledThermoMechanics::createIPState(IPStateBase* &ips, bool hasBodyForce, const bool* state_,const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
  //bool inter=true;
  //const MInterfaceElement *iele = static_cast<const MInterfaceElement*>(ele);
  //if(iele==NULL) inter=false;
  IPVariable* ipvi = new IPNonLocalDamageJ2FullyCoupledThermoMechanics(_j2IH,cLLaw,damLaw);
  IPVariable* ipv1 = new IPNonLocalDamageJ2FullyCoupledThermoMechanics(_j2IH,cLLaw,damLaw);
  IPVariable* ipv2 = new IPNonLocalDamageJ2FullyCoupledThermoMechanics(_j2IH,cLLaw,damLaw);
  if(ips != NULL) delete ips;
  ips = new IP3State(state_,ipvi,ipv1,ipv2);
}
void mlawNonLocalDamageJ2FullyCoupledThermoMechanics::createIPState(IPNonLocalDamageJ2FullyCoupledThermoMechanics *ivi, IPNonLocalDamageJ2FullyCoupledThermoMechanics *iv1, IPNonLocalDamageJ2FullyCoupledThermoMechanics *iv2) const
{
  //Msg::Error("this function is not used; IPVariable is not constructed by mlawNonLocalDamageJ2FullyCoupledThermoMechanics");
}
void mlawNonLocalDamageJ2FullyCoupledThermoMechanics::createIPVariable(IPNonLocalDamageJ2FullyCoupledThermoMechanics *&ipv) const
{
  //Msg::Error("this function is not used; IPVariable is not constructed by mlawNonLocalDamageJ2FullyCoupledThermoMechanics");
}

double mlawNonLocalDamageJ2FullyCoupledThermoMechanics::soundSpeed() const
{
  return mlawJ2FullyCoupledThermoMechanics::soundSpeed();
}

/**
   * @brief Material constitutive interface function associated with the non-local thermomechanical case in the dG3D interface.
   * whatever the number of the non-local variables.
   * @param F0 (input @ time n)     the initial deformation gradient.
   * @param F1 (input @ time n+1)   the updated deformation gradient.
   * @param P (output)              the 1st Piola-Kirchhoff stress tensor updated after computation.
   * @param q0 (input @ tine n+1)   the initial array of internal variables.
   * @param q1 (output)             the updated array of internal variables updated after computation (including the output local variables).
   * @param Tangent (output)               the tangent quantities d_P/dF.
   * @param dLocalPlasticStrainDStrain (output)      the tangent quantities d_v_loc/dF.
   * @param dStressDNonLocalPlasticStrain (output)   the tangent quantities d_P/dv_nonloc.
   * @param dLocalPlasticStrainDNonLocalPlasticStrain (output) the tangent quantities d_v_loc/dv_nonloc.
   * @param stiff (input options)          is true if the tangent computation is required.
   * 
   * @param dPdT (output)                  the tangent quantities d_P/dT.
   * @param dPdgradT (output)              the tangent quantities d_P/dgradT.
   * @param dLocalVarDT (output)           the tangent quantities d_v_loc/dT.
   * @param T0 (input @ time n)            the initial temperature.
   * @param T1 (input @ time n+1)          the updated temperature.
   * @param gradT0 (input @ time n)        the initial temperature gradient.
   * @param gradT1 (input @ time n+1)      the updated temperature gradient.
   * @param fluxT (output)                 the heat flux
   * @param dfluxTdF (output)              the tangent quantities d_heatflux/dF.
   * @param dfluxTdNonLocalPlasticStrain (output)    the tangent quantities d_heatflux/dv_nonloc.
   * @param dfluxTdT (output)              the tangent quantities d_heatflux/dT.
   * @param dfluxTdgradT (output)          the tangent quantities d_heatflux/dgradT.
   * @param thermalSource (output)                  the thermal source ?????????????.
   * @param dthermalSourcedF (output)               the tangent quantities d_termsource/dF.
   * @param dthermalSourcedNonLocalPlasticStrain (output)     the tangent quantities d_termsource/dv_nonloc.
   * @param dthermalSourcedT (output)               the tangent quantities d_termsource/dT.
   * @param dthermalSourcedgradT (output)           the tangent quantities d_termsource/dgradT.
   * @param mechanicalSource (output)               the mechanical dissipation converted into heat source.
   * @param dmechanicalSourceF (output)             the tangent quantities d_dissip/dF.
   * @param dmechanicalSourcedNonLocalVar (output)  the tangent quantities d_dissip/dv_nonloc.
   * @param dmechanicalSourcedT (output)            the tangent quantities d_dissip/dT.
   * @param dmechanicalSourcedgradT (output)        the tangent quantities d_dissip/dgradT.
   * @param elasticTangent (optional output)        
   */
void mlawNonLocalDamageJ2FullyCoupledThermoMechanics::constitutive(
                              const STensor3 &F0, const STensor3 &Fn, STensor3 &P,
                              const IPVariable * q0i,IPVariable * q1i,                          
                              STensor43 &Tangent, STensor3 &dLocalPlasticStrainDStrain,
                              STensor3 &dStressDNonLocalPlasticStrain,
                              double &dLocalPlasticStrainDNonLocalPlasticStrain,
                              const bool stiff,
                              STensor3 &dPdT, STensor33 &dPdgradT,  double &dLocalPlasticStrainDT,
                              
                              const double &T0, const double &Tn, 
                              const SVector3 &gradT0, const SVector3 &gradT,
                              
                              SVector3 &fluxT,
                              STensor33 &dfluxTdF, SVector3 &dfluxTdNonLocalPlasticStrain,
                              SVector3  &dfluxTdT, STensor3 &dfluxTdgradT,
                              
                              double &thermalSource, 
                              STensor3 &dthermalSourcedF, double &dthermalSourcedNonLocalPlasticStrain,
                              double &dthermalSourcedT, SVector3 &dthermalSourcedgradT,
          
                              double &mechanicalSource,
                              STensor3 &dmechanicalSourceF, double &dmechanicalSourcedNonLocalPlasticStrain,
                              double &dmechanicalSourcedT, SVector3 &dmechanicalSourcedgradT,
                              STensor43* elasticTangent 
                           ) const
{
  static STensor43 dFpdF, dFedF;
  static STensor3 dFpdT, dFedT;
  if(elasticTangent != NULL) Msg::Error("mlawNonLocalDamageJ2FullyCoupledThermoMechanics elasticTangent not defined"); 
  if (!_tangentByPerturbation){
          predictorCorrector(F0, Fn, P,q0i,q1i, Tangent, dFpdF, dFpdT, dFedF, dFedT, dLocalPlasticStrainDStrain,
                             dStressDNonLocalPlasticStrain,dLocalPlasticStrainDNonLocalPlasticStrain,stiff,
                             dPdT, dPdgradT,  dLocalPlasticStrainDT,T0, Tn, gradT0, gradT,
                              fluxT,dfluxTdF, dfluxTdNonLocalPlasticStrain,
                              dfluxTdT, dfluxTdgradT,thermalSource, dthermalSourcedF, dthermalSourcedNonLocalPlasticStrain,
                              dthermalSourcedT, dthermalSourcedgradT,mechanicalSource,dmechanicalSourceF, dmechanicalSourcedNonLocalPlasticStrain,
                              dmechanicalSourcedT, dmechanicalSourcedgradT, elasticTangent  );

  }
  else{
    Msg::Error("mlawNonLocalDamageJ2FullyCoupledThermoMechanics numerical tangent not implemented");
/*    mlawJ2FullyCoupledThermoMechanics::predictorCorector(F0,F,P,q0,q,T0,T,gradT0,gradT,fluxT,thermalSource, mechanicalSource);

    static STensor3 Fplus, Pplus;
    static SVector3 fluxTPlus, gradTplus;
    static double thermalSourcePlus;
    static double mechanicalSourcePlus;
    static IPJ2ThermoMechanics qPlus(*q0);

    // perturbe F
    for (int i=0; i<3; i++){
      for (int j=0; j<3; j++){
        Fplus = (F);
        Fplus(i,j) += _perturbationfactor;
        predictorCorector(F0,Fplus,Pplus,q0,&qPlus,T0,T,gradT0,gradT,fluxTPlus,thermalSourcePlus, mechanicalSourcePlus);
        for (int k=0; k<3; k++){
          for (int l=0; l<3; l++){
            Tangent(k,l,i,j) = (Pplus(k,l) - P(k,l))/(_perturbationfactor);
          }
          dfluxTdF(k,i,j) = (fluxTPlus(k) - fluxT(k))/_perturbationfactor;
        }
        dthermalSourcedF(i,j) = (thermalSourcePlus - thermalSource)/_perturbationfactor;
        dmechanicalSourceF(i,j) = (mechanicalSourcePlus - mechanicalSource)/_perturbationfactor;
      }
    }
    // perturbe gradT

    double gradTpert = _perturbationfactor*T0/1e-3;
    for (int i=0; i<3; i++){
      gradTplus = (gradT);
      gradTplus(i) += (gradTpert);
      predictorCorector(F0,F,Pplus,q0,&qPlus,T0,T,gradT0,gradTplus,fluxTPlus,thermalSourcePlus, mechanicalSourcePlus);
      for (int k=0; k<3; k++){
        dfluxTdgradT(k,i) = (fluxTPlus(k) - fluxT(k))/gradTpert;
      }
    }

    // perturb on T
    double Tplus = T+ _perturbationfactor*T0;
    predictorCorector(F0,F,Pplus,q0,&qPlus,T0,Tplus,gradT0,gradT,fluxTPlus,thermalSourcePlus, mechanicalSourcePlus);
    for (int k=0; k<3; k++){
      for (int l=0; l<3; l++){
        dPdT(k,l) = (Pplus(k,l) - P(k,l))/(_perturbationfactor*T0);
      }
      dfluxTdT(k) = (fluxTPlus(k) - fluxT(k))/(_perturbationfactor*T0);
    }
    dthermalSourcedT = (thermalSourcePlus - thermalSource)/(_perturbationfactor*T0);
    dmechanicalSourcedT = (mechanicalSourcePlus - mechanicalSource)/(_perturbationfactor*T0);
*/ 
 }
}
void mlawNonLocalDamageJ2FullyCoupledThermoMechanics::predictorCorrector(
                              const STensor3 &F0, const STensor3 &Fn, STensor3 &P,
                              const IPVariable * q0i,IPVariable * q1i,                          
                              STensor43 &Tangent, 
                              STensor43 &dFpdF, // plastic tangent
                              STensor3 &dFpdT, // plastic tangent
                              STensor43 &dFedF, // elastic tangent
                              STensor3 &dFedT, // elastic tangent                            
                              STensor3 &dLocalPlasticStrainDStrain,
                              STensor3 &dStressDNonLocalPlasticStrain,
                              double &dLocalPlasticStrainDNonLocalPlasticStrain,
                              const bool stiff,
                              STensor3 &dPdT, STensor33 &dPdgradT,  double &dLocalPlasticStrainDT,
                              
                              const double &T0, const double &Tn, 
                              const SVector3 &gradT0, const SVector3 &gradT,
                              
                              SVector3 &fluxT,
                              STensor33 &dfluxTdF, SVector3 &dfluxTdNonLocalPlasticStrain,
                              SVector3  &dfluxTdT, STensor3 &dfluxTdgradT,
                              
                              double &thermalSource, 
                              STensor3 &dthermalSourcedF, double &dthermalSourcedNonLocalPlasticStrain,
                              double &dthermalSourcedT, SVector3 &dthermalSourcedgradT,
          
                              double &mechanicalSource,
                              STensor3 &dmechanicalSourcedF, double &dmechanicalSourcedNonLocalPlasticStrain,
                              double &dmechanicalSourcedT, SVector3 &dmechanicalSourcedgradT,
                              STensor43* elasticTangent  
                           ) const
{

  static STensor3 Fe, Peff,dPeffdT;
  static STensor3 Fpinv;


  //
  const IPNonLocalDamageJ2FullyCoupledThermoMechanics *ipvprev = dynamic_cast<const IPNonLocalDamageJ2FullyCoupledThermoMechanics *> (q0i);
        IPNonLocalDamageJ2FullyCoupledThermoMechanics *ipvcur = dynamic_cast<IPNonLocalDamageJ2FullyCoupledThermoMechanics *>(q1i);
  double p0 = ipvprev->getConstRefToEquivalentPlasticStrain();
  cLLaw->computeCL(p0, ipvcur->getRefToIPCLength());
  mlawJ2FullyCoupledThermoMechanics::predictorCorector(F0,Fn,Peff,ipvprev,ipvcur,Tangent,dFpdF,dFpdT, dFedF,dFedT,dLocalPlasticStrainDStrain,dLocalPlasticStrainDT,T0,Tn,gradT0,gradT,fluxT,dPeffdT,dfluxTdgradT,dfluxTdT,dfluxTdF,
                      thermalSource,dthermalSourcedT,dthermalSourcedF,
                      mechanicalSource,dmechanicalSourcedT,dmechanicalSourcedF,stiff);


  const STensor3 &Fp  = ipvcur->getConstRefToFp();
  STensorOperation::inverseSTensor3(Fp,Fpinv);
  STensorOperation::multSTensor3(Fn,Fpinv,Fe);

  double ene = ipvcur->defoEnergy();

  if (ipvcur->dissipationIsBlocked()){
    // damage stop increasing
    IPDamage& curDama = ipvcur->getRefToIPDamage();
    curDama.getRefToDamage() = ipvprev->getDamage();
    curDama.getRefToDDamageDp() = 0.;
    STensorOperation::zero(curDama.getRefToDDamageDFe());
    curDama.getRefToDeltaDamage() = 0;
    curDama.getRefToMaximalP() = ipvprev->getMaximalP();
    // damage is not active
    ipvcur->getRefToDissipationActive() =  false;

  }
  else{
    static STensor43 Cel;
    evaluateCel(Cel, Tn); //in this version we assume it constant with respect to T
    // if damage is not blocked
    if (ipvcur->getNonLocalToLocal()){
      // when considering crack transition
      // increment of non-local plastic strain by true plastic strain
      // in order to obtain damage continuously developed
      ipvcur->getRefToEffectivePlasticStrain() = ipvprev->getEffectivePlasticStrain();
      ipvcur->getRefToEffectivePlasticStrain() += (ipvcur->getConstRefToEquivalentPlasticStrain() - ipvprev->getConstRefToEquivalentPlasticStrain());
      // computed damage with current effective plastic strain
      damLaw->computeDamage(ipvcur->getEffectivePlasticStrain(),
                        ipvprev->getEffectivePlasticStrain(),
                        ene, Fe, Fp, Peff, Cel,
                        ipvprev->getConstRefToIPDamage(),ipvcur->getRefToIPDamage());
     //Msg::Info("continue damage in interface = %e",ipvcur->getDamage());


    }
    else{
      //
      // compute damage for ipcur
      damLaw->computeDamage(ipvcur->getEffectivePlasticStrain(),
                        ipvprev->getEffectivePlasticStrain(),
                        ene, Fe, Fp, Peff, Cel,
                        ipvprev->getConstRefToIPDamage(),ipvcur->getRefToIPDamage());

    }

    if (ipvcur->getDamage() > ipvprev->getDamage() and ipvcur->getConstRefToEquivalentPlasticStrain() > ipvprev->getConstRefToEquivalentPlasticStrain()){
      ipvcur->getRefToDissipationActive() = true;
    }
    else{
      ipvcur->getRefToDissipationActive() = false;
    }
  }

  double D = ipvcur->getDamage();
  double D0 = ipvprev->getDamage();
  // check active damage

  // get true PK1 stress from damage and effective PK1 stress
  P = Peff;
  P*=(1.-D);

  mechanicalSource*=(1.-D);

  ipvcur->getRefToElasticEnergy()*= (1.-D);
  ipvcur->getRefToPlasticPower() *= (1.-D);

  double dPlasticDisp = ipvcur->plasticEnergy() - ipvprev->plasticEnergy();
  ipvcur->getRefToPlasticEnergy() = ipvprev->plasticEnergy() + (1-D)*dPlasticDisp;
  ipvcur->getRefToDamageEnergy() =  ipvprev->damageEnergy() + ene*(D-D0);

  // irreversible energy
  double& irrEneg = ipvcur->getRefToIrreversibleEnergy();
  if (this->getMacroSolver()->getPathFollowingLocalIncrementType() == pathFollowingManager::DEFO_ENERGY){
    irrEneg = ipvcur->defoEnergy();
  }
  else if (this->getMacroSolver()->getPathFollowingLocalIncrementType() == pathFollowingManager::PLASTIC_ENERGY){
    irrEneg = ipvcur->plasticEnergy();
  }
  else if (this->getMacroSolver()->getPathFollowingLocalIncrementType() == pathFollowingManager::DAMAGE_ENERGY) {
    irrEneg = ipvcur->damageEnergy();
  }
  else if (this->getMacroSolver()->getPathFollowingLocalIncrementType() == pathFollowingManager::DISSIPATION_ENERGY){
    irrEneg = ipvcur->plasticEnergy()+ ipvcur->damageEnergy();
  }
  else{
    irrEneg = 0.;
  }


  if(stiff)
  {
    static STensor3 DDamageDF;

    for (int i=0; i<3; i++){
      for (int j=0; j<3; j++){
        DDamageDF(i,j) = 0.;
        if (ipvcur->getNonLocalToLocal()){
          DDamageDF(i,j) +=ipvcur->getDDamageDp()*dLocalPlasticStrainDStrain(i,j);
        };
        for (int k=0; k<3; k++){
          for (int l=0; l<3; l++){
            DDamageDF(i,j) += ipvcur->getConstRefToDDamageDFe()(k,l)*dFedF(k,l,i,j);
          }
        }
      }
    }

    if (elasticTangent != NULL){
      // update with damage
      (*elasticTangent) *= (1.-D);
    }

    // we need to correct partial P/partial F: (1-D) partial P/partial F - Peff otimes partial D partial F
    Tangent*=(1.-D);
    for(int i=0;i<3;i++){
      for(int j=0;j<3;j++){
        for(int k=0;k<3;k++){
          for(int l=0;l<3;l++){
            Tangent(i,j,k,l)-=Peff(i,j)*DDamageDF(k,l);
          }
        }
      }
    }

    // -hat{P} partial D/partial tilde p
    if (ipvcur->getNonLocalToLocal()){
      STensorOperation::zero(dStressDNonLocalPlasticStrain);
    }
    else{
      dStressDNonLocalPlasticStrain = Peff*(-1*ipvcur->getDDamageDp());
    }

    // partial p partial tilde p (0 if no MFH)
    dLocalPlasticStrainDNonLocalPlasticStrain = 0.;

    STensor3& DplasticPowerDF = ipvcur->getRefToDPlasticPowerDF();
    DplasticPowerDF *= (1-D);
    DplasticPowerDF.daxpy(DDamageDF,-ipvcur->getConstRefToPlasticPower()/(1.-D));

    // irreversible energy
    STensor3& DdissEnergDF = ipvcur->getRefToDIrreversibleEnergyDF();
    double& DdissEnergDNonlocalVar = ipvcur->getRefToDIrreversibleEnergyDNonLocalVariable();

    if (this->getMacroSolver()->getPathFollowingLocalIncrementType() == pathFollowingManager::DEFO_ENERGY){
      DdissEnergDF *= (1-D);
      DdissEnergDF.daxpy(DDamageDF,-ene);
      if (ipvcur->getNonLocalToLocal()){
        DdissEnergDF.daxpy(dLocalPlasticStrainDStrain,-ipvcur->getDDamageDp()*ene);
        DdissEnergDNonlocalVar = 0.;
      }
      else{
        DdissEnergDNonlocalVar = -ipvcur->getDDamageDp()*ene;
      }
    }
    else if (this->getMacroSolver()->getPathFollowingLocalIncrementType() == pathFollowingManager::PLASTIC_ENERGY){
      DdissEnergDF *= (1-D);
      DdissEnergDF.daxpy(DDamageDF,-dPlasticDisp);

      if (ipvcur->getNonLocalToLocal()){
        DdissEnergDF.daxpy(dLocalPlasticStrainDStrain,-ipvcur->getDDamageDp()*dPlasticDisp);
        DdissEnergDNonlocalVar = 0.;
      }
      else{
        DdissEnergDNonlocalVar = -ipvcur->getDDamageDp()*dPlasticDisp;
      }
    }
    else if (this->getMacroSolver()->getPathFollowingLocalIncrementType() == pathFollowingManager::DAMAGE_ENERGY) {
      for(int i=0;i<3;i++){
        for(int j=0;j<3;j++){
          DdissEnergDF(i,j) = 0.;
          for(int k=0;k<3;k++){
            for(int l=0;l<3;l++){
              for (int m=0; m<3; m++){
                DdissEnergDF(i,j) += Peff(k,m)*Fp(l,m)*dFedF(k,l,i,j)*(D-D0);
              }
            }
          }
        }
      }
      DdissEnergDF.daxpy(DDamageDF,ene);
      if (ipvcur->getNonLocalToLocal()){
        DdissEnergDF.daxpy(dLocalPlasticStrainDStrain, ipvcur->getDDamageDp()*ene);
        DdissEnergDNonlocalVar = 0.;
      }
      else{
        DdissEnergDNonlocalVar = ipvcur->getDDamageDp()*ene;
      }

    }
    else if (this->getMacroSolver()->getPathFollowingLocalIncrementType() == pathFollowingManager::DISSIPATION_ENERGY){
      DdissEnergDF *= (1-D);
      for(int i=0;i<3;i++){
        for(int j=0;j<3;j++){
          for(int k=0;k<3;k++){
            for(int l=0;l<3;l++){
              for (int m=0; m<3; m++){
                DdissEnergDF(i,j) += Peff(k,m)*Fp(l,m)*dFedF(k,l,i,j)*(D-D0);
              }
            }
          }
        }
      }

      double DdissDD = ene - dPlasticDisp;
      DdissEnergDF.daxpy(DDamageDF,DdissDD);
      if (ipvcur->getNonLocalToLocal()){
        DdissEnergDF.daxpy(dLocalPlasticStrainDStrain, ipvcur->getDDamageDp()*DdissDD);
        DdissEnergDNonlocalVar = 0.;
      }
      else{
        DdissEnergDNonlocalVar = ipvcur->getDDamageDp()*DdissDD;
      }
    }
    else{
      STensorOperation::zero(DdissEnergDF);
      STensorOperation::zero(DdissEnergDNonlocalVar);
    }
    dPdT=dPeffdT;
    dPdT*=(1.-D);
    
    dmechanicalSourcedF*=(1.-D);
    dmechanicalSourcedF-=(mechanicalSource/(1.-D))*DDamageDF;
    dmechanicalSourcedT*=(1.-D);
    // we assume dDammage dT=0

    
    STensorOperation::zero(dPdgradT); 
    STensorOperation::zero(dfluxTdNonLocalPlasticStrain);
    dthermalSourcedNonLocalPlasticStrain=0.;
    dmechanicalSourcedNonLocalPlasticStrain=0.;

  }
}
