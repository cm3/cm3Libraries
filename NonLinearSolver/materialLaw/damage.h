#include <random>
#ifndef DAMAGE_H
#define DAMAGE_H 1

#ifdef NONLOCALGMSH
namespace MFH {
#endif

//Generic material
class Damage{

	protected:
		int dammodel;
		int ndamprops;
		int nsdv;
                int pos_maxD;
                bool localDamage;
		
	public:
		
		Damage(double* props, int iddam){
			dammodel = (int) props[iddam];
			localDamage = false;
		}
		virtual ~Damage(){ ; }
		inline int get_dammodel(){  return dammodel; }
		inline int get_nsdv() {	return nsdv; } 
		
		//print the damage properties
		virtual void print()=0;

		//compute the constitutive response of the material
		//INPUT: dstrn, strs_n, statev_n, kinc, kstep
		//OUTPUT: strs, statev, Cref, dCref, Calgo
		virtual void damagebox(double* statev_n, double* statev, int pos_strn, int pos_strs, int pos_dam, double** Calgo,double* dDdE, double *dDdp_bar, double alpha, int kinc, int kstep)=0;
		virtual int get_pos_DamParm1() const {  return 0; } 
		virtual int get_pos_DamParm2() const {  return 0; } 
		virtual int get_pos_DamParm3() const {  return 0; } 
		virtual int get_pos_Fd_bar() const {  return 0; }
		virtual int get_pos_dFd_bar() const {  return 0; }
		virtual int get_pos_locFd() const {  return 0; }
                virtual double get_Gc() const {return -1.0;}
                virtual double get_exp_n() const {return -1.0;}
                virtual int get_pos_maxD() const {return pos_maxD;}
                virtual double getMaxD(double *statev, int pos_dam) const {return statev[pos_dam+get_pos_maxD()];}
                virtual void setMaxD(double *statev, int pos_dam, double Dmax) { statev[pos_dam+get_pos_maxD()]=Dmax;}
                virtual int getNbNonLocalVariables() const {return 1;}
#ifdef NONLOCALGMSH
                virtual void setLocalDamage() {localDamage=true;}
                virtual void setNonLocalDamage() {localDamage=false;}
#endif
};

//Linear elastic material
class LC_Damage : public Damage{

	private:
		double S0,n;

	public:
		
		LC_Damage(double* props, int iddam);
		~LC_Damage();
		
		virtual void print();
		virtual void damagebox(double* statev_n, double* statev, int pos_strn, int pos_strs, int pos_dam, double** Calgo,double* dDdE, double *dDdp_bar, double alpha, int kinc, int kstep);


		
		
}; 


////////////////////////////////////////////////
/////////////new defined ling Wu 5/4/2011.
////////////////////////////////////////////////


//LEMAITRE-CHABOCHE DAMAGE MODEL (nonlocal)
//for nonlocal MT type 
//PROPS: ID, S0, n
//STATEV: D,dD,p_bar,dp_bar, plastic strn (6)
//
// output: dD increment of D in statev
//         dDdE, dDdp_bar D w.r.t strain and p_bar respectivly
//****************************************
class LCN_Damage : public Damage{

	private:
		double S0,n, p0;

                int pos_dDdp;
                int pos_pbarTrans;
                int pos_Dt;

	public:
		
		LCN_Damage(double* props, int iddam);
		~LCN_Damage();
		
		virtual void print();
		virtual void damagebox(double* statev_n, double* statev, int pos_strn, int pos_strs, int pos_dam, double** Calgo,double* dDdE, double *dDdp_bar, double alpha, int kinc, int kstep);

		
		
}; 



//BILIN DAMAGE MODEL (nonlocal)
//****************************************
class BILIN_Damage : public Damage{

	private:
		double S0,n, p0, D_crit, ponset, Alpha, Beta;

	public:
		
		BILIN_Damage(double* props, int iddam);
		~BILIN_Damage();
		
		virtual void print();
		virtual void damagebox(double* statev_n, double* statev, int pos_strn, int pos_strs, int pos_dam, double** Calgo,double* dDdE, double *dDdp_bar, double alpha, int kinc, int kstep);

		
		
}; 


//BILIN STOCHASTIC DAMAGE MODEL (nonlocal)
//****************************************
class BILIN_Damage_Stoch : public Damage{

	private:
		int pos_DamParm1;
		int pos_DamParm2;
		int pos_DamParm3;
		double S0,n, p0, Beta;


	public:
		
		BILIN_Damage_Stoch(double* props, int iddam);
		~BILIN_Damage_Stoch();
		
		virtual void print();
		virtual void damagebox(double* statev_n, double* statev, int pos_strn, int pos_strs, int pos_dam, double** Calgo,double* dDdE, double *dDdp_bar, double alpha, int kinc, int kstep);
		virtual int get_pos_DamParm1() const {  return pos_DamParm1; }
		virtual int get_pos_DamParm2() const {  return pos_DamParm2; }
		virtual int get_pos_DamParm3() const {  return pos_DamParm3; }

		
		
}; 




//Linear DAMAGE MODEL (nonlocal)
//for nonlocal MT type 
//PROPS: ID, p0, pc
//STATEV: D,dD,p_bar,dp_bar, plastic strn (6)
//
// output: dD increment of D in statev
//         dDdE, dDdp_bar D w.r.t strain and p_bar respectivly
//****************************************
class LINN_Damage : public Damage{

	private:
		double p0,pc;

	public:
		
		LINN_Damage(double* props, int iddam);
		~LINN_Damage();
		
		virtual void print();
		virtual void damagebox(double* statev_n, double* statev, int pos_strn, int pos_strs, int pos_dam, double** Calgo,double* dDdE, double *dDdp_bar, double alpha, int kinc, int kstep);

		
		
}; 




//Exponential DAMAGE MODEL (nonlocal)
//for nonlocal MT type 
//PROPS: ID, Beta
//STATEV: D,dD,p_bar,dp_bar, plastic strn (6)
//
// output: dD increment of D in statev
//         dDdE, dDdp_bar D w.r.t strain and p_bar respectivly
//****************************************
class EXPN_Damage : public Damage{

	private:
		double Beta;

	public:
		
		EXPN_Damage(double* props, int iddam);
		~EXPN_Damage();
		
		virtual void print();
		virtual void damagebox(double* statev_n, double* statev, int pos_strn, int pos_strs, int pos_dam, double** Calgo,double* dDdE, double *dDdp_bar, double alpha, int kinc, int kstep);

		
		
}; 



//Sigmoid DAMAGE MODEL (nonlocal)
//for nonlocal MT type 
//PROPS: ID, Beta, pc
//STATEV: D,dD,p_bar,dp_bar, plastic strn (6)
//
// output: dD increment of D in statev
//         dDdE, dDdp_bar D w.r.t strain and p_bar respectivly
//****************************************
class Sigmoid_Damage : public Damage{

	private:
		double Beta;
                double pc;
                double D0; 

	public:
		
		Sigmoid_Damage(double* props, int iddam);
		~Sigmoid_Damage();
		
		virtual void print();
		virtual void damagebox(double* statev_n, double* statev, int pos_strn, int pos_strs, int pos_dam, double** Calgo,double* dDdE, double *dDdp_bar, double alpha, int kinc, int kstep);

		
		
}; 

///ling Wu 5/4/2011.



//***********************************
//*******Power_law DAMAGE MODEL (nonlocal)
//for nonlocal MT type 
//PROPS: ID, Alpha, Beta, p0,pc, Dc
//STATEV: D,dD,p_bar,dp_bar, plastic strn (6)
// **here, p is no longer the accumulate plastic strian, it is a defined equvialent strain. 
// output: dD increment of D in statev
//         dDdE, dDdp_bar D w.r.t strain and p_bar respectivly
//****************************************
class PLN_Damage : public Damage{

	private:
		double Alpha, Beta;
                double p0, pc;
		double Dc;

	public:
		
		PLN_Damage(double* props, int iddam);
		~PLN_Damage();
		
		virtual void print();
		virtual void damagebox(double* statev_n, double* statev, int pos_strn, int pos_strs, int pos_dam, double** Calgo,double* dDdE, double *dDdp_bar, double alpha, int kinc, int kstep);

		
}; 


class LCN_Damage_Stoch : public Damage{

	private:
		int pos_DamParm1;
		int pos_DamParm2;
                double p0;

                int pos_dDdp;
                int pos_pbarTrans;
                int pos_Dt;

	public:
		
		LCN_Damage_Stoch(double* props, int iddam);
		~LCN_Damage_Stoch();
		
		virtual void print();
		virtual void damagebox(double* statev_n, double* statev, int pos_strn, int pos_strs, int pos_dam, double** Calgo,double* dDdE, double *dDdp_bar, double alpha, int kinc, int kstep);
		virtual int get_pos_DamParm1() const {  return pos_DamParm1; }
		virtual int get_pos_DamParm2() const {  return pos_DamParm2; }
};

//***********************************
//*******Weibull DAMAGE MODEL (nonlocal)
//for nonlocal MT type 
//PROPS: ID, Alpha, m,L, L0, S0, NFiber, Pdam
//STATEV: D,dD,p_bar,dp_bar, plastic strn (6)
// output: dD increment of D in statev
//         dDdE, dDdp_bar D w.r.t strain and p_bar respectivly
//****************************************
class Weibull_Damage : public Damage{

	private:
		double L, L0;
                double S0, Alpha, m;
		int NFiber;
                double Pdam;
                double Strs_cr;
                int pos_DamParm1, pos_DamParm2;
                int pos_Fd_bar, pos_dFd_bar;
                int pos_locFd;
                int pos_dDdS;
                int pos_StrsTrans;
                int pos_Dt;
                double Gc;
                double n;

	public:
		
		Weibull_Damage(double* props, int iddam);
		~Weibull_Damage();
		
		virtual void print();
		virtual int get_pos_DamParm1() const {  return pos_DamParm1; }
		virtual int get_pos_DamParm2() const {  return pos_DamParm2; }
		virtual int get_pos_Fd_bar() const {  return pos_Fd_bar; }
		virtual int get_pos_dFd_bar() const {  return pos_dFd_bar; }
		virtual int get_pos_locFd() const {  return pos_locFd; }
		virtual double get_Gc() const {  return Gc; }
		virtual double get_exp_n() const {  return n; }

		virtual void damagebox(double* statev_n, double* statev, int pos_strn, int pos_strs, int pos_dam, double** Calgo,double* dDdE, double *dDdFd_bar, double alpha, int kinc, int kstep);

                virtual int getNbNonLocalVariables() const {if (Gc>=1.0e-15) return 1; else return 0;}
		
}; 

#ifdef NONLOCALGMSH
}
#endif


#endif
