
//
// Description: Define damage law for cohesive layer
//
// Author:  <V.D. Nguyen>, (C) 2017
//
// Copyright: See COPYING file that comes with this distribution
//
//

#include "cohesiveDamageLaw.h"
#include <math.h>
#include "GmshMessage.h"

ZeroCohesiveDamageLaw::ZeroCohesiveDamageLaw(const int num):cohesiveDamageLaw(num){};
			
ZeroCohesiveDamageLaw::ZeroCohesiveDamageLaw(const ZeroCohesiveDamageLaw& src):cohesiveDamageLaw(src){}
			
void ZeroCohesiveDamageLaw::computeDamage(const double pi, const double pc, 
															 const double p0, const double p1,
                              const IPCohesiveDamage& ipprev, IPCohesiveDamage& ipcur, 
															 const bool stiff) const{
  double D0 = ipprev.getConstRefToCohesiveDamage();
  double& D = ipcur.getRefToCohesiveDamage();
  double& dDdp = ipcur.getRefToDCohesiveDamageDEffectiveJump();
  
  D = D0;
  dDdp = 0.;
};

BilinearCohesiveDamageLaw::BilinearCohesiveDamageLaw(const int num):cohesiveDamageLaw(num){};
			
BilinearCohesiveDamageLaw::BilinearCohesiveDamageLaw(const BilinearCohesiveDamageLaw& src):cohesiveDamageLaw(src){}
			
void BilinearCohesiveDamageLaw::computeDamage(const double pi, const double pc, 
															 const double p0, const double p1,
                              const IPCohesiveDamage& ipprev, IPCohesiveDamage& ipcur, 
															 const bool stiff) const{
  double D0 = ipprev.getConstRefToCohesiveDamage();
  double& D = ipcur.getRefToCohesiveDamage();
  double& dDdp = ipcur.getRefToDCohesiveDamageDEffectiveJump();
  
  D = D0;
  dDdp = 0.;
	 
  if ((p1> p0) and (p1 >pi)){
    if (p1 < pc){
			double fact = pc/(pc-pi);
			D = fact*(1. - pi/p1);
			if (stiff){
				dDdp = fact*pi/(p1*p1);
			}
		}
		else{
			D = 1.;
			dDdp = 0.;
		}
	}
};

ExponentialCohesiveDamageLaw::ExponentialCohesiveDamageLaw(const int num, double cc):
			cohesiveDamageLaw(num), _c(cc){};
			
ExponentialCohesiveDamageLaw::ExponentialCohesiveDamageLaw(const ExponentialCohesiveDamageLaw& src):
			cohesiveDamageLaw(src), _c(src._c){}

void ExponentialCohesiveDamageLaw::computeDamage(const double pi, const double pc, 
															 const double p0, const double p1, 
                               const IPCohesiveDamage& ipprev, IPCohesiveDamage& ipcur, 
															 const bool stiff) const{
  double D0 = ipprev.getConstRefToCohesiveDamage();
  double& D = ipcur.getRefToCohesiveDamage();
  double& dDdp = ipcur.getRefToDCohesiveDamageDEffectiveJump();
  
	D = D0;
	dDdp = 0.;
  
  const IPExponentialCohesiveDamage* ipdam = dynamic_cast<const IPExponentialCohesiveDamage*>(&ipcur);
  double I = (pc-pi)/(2.*pi);
  double xi = ipdam->getConstRefToPowerParameter();
  double Ga = tgamma(1./xi);
  double beta = pow(Ga/xi/I,xi);
  double alpha = -1.;
  
	if ((p1> p0) and (p1 >pi)){
		double refp = p1/pi;
		D = 1 - pow(refp,alpha)*exp(-beta*pow(refp-1.,xi));
		
		if (stiff){
      if (fabs(xi -1.) < 1e-6)
      {
        dDdp = (-alpha/refp+beta)*(1.-D)/pi;
      }
      else
      {
        dDdp = (-alpha/refp+beta*xi*pow(refp-1.,xi-1.))*(1.-D)/pi;
      }
		}
	}
};