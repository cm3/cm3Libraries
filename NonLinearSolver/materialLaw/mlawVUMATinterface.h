//
// C++ Interface: material law
//
// Description: VUMAT Interface law: it is a "virtual implementation" you have to define a law
//              in your project which derive from this law. you have to do THE SAME for your IPVariable
//              which have to derive from IPVUMATinterface (all data in this ipvariable have names beginning by _vumat...)
//
// Author:  <Antoine JERUSALEM>, (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef MLAWVUMATINTERFACE_H_
#define MLAWVUMATINTERFACE_H_
#include "mlaw.h"
#include "STensor3.h"
#include "STensor43.h"
#include "ipVUMATinterface.h"

class mlawVUMATinterface : public materialLaw
{
 protected:
  // can't be const due to operator= constructor (HOW TO CHANGE THIS ??)
  double _rho; // density
  double *_props; // properties
  int _nprops; // number of properties
  int _nsdv; // number of internal variables
 public:
  mlawVUMATinterface(const int num, const double rho, const char*propName);
 #ifndef SWIG
  mlawVUMATinterface(const mlawVUMATinterface &source);
  mlawVUMATinterface& operator=(const materialLaw &source);
  virtual ~mlawVUMATinterface();
	virtual materialLaw* clone() const {return new mlawVUMATinterface(*this);};
	virtual void checkInternalState(IPVariable* ipv, const IPVariable* ipvprev) const{}; // do nothing
  virtual bool withEnergyDissipation() const {return false;};
  // function of materialLaw
  virtual matname getType() const{return materialLaw::vumat;}
  virtual void createIPState(IPStateBase* &ips, bool hasBodyForce, const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const IntPt *GP=NULL, const int gpt = 0) const;
  virtual void initLaws(const std::map<int,materialLaw*> &maplaw){}; // this law is initialized so nothing to do
  virtual double soundSpeed() const; // default but you can redefine it for your case
  virtual double density()const{return _rho;}
  // specific function
 public:
  virtual void constitutive(
                            const STensor3& F0,         // initial deformation gradient (input @ time n)
                            const STensor3& Fn,         // updated deformation gradient (input @ time n+1)
                            STensor3 &P,                // updated 1st Piola-Kirchhoff stress tensor (output)
                                                        // contains the initial values on input
                            const IPVariable*q0,       // array of initial internal variable
                            IPVariable *q1,             // updated array of internal variable (in ipvcur on output),
                            STensor43 &Tangent,         // constitutive tangents (output)
                            const bool stiff ,           // if true compute the tangents
			    STensor43* elasticTangent = NULL, 
                            const bool dTangent =false,
                            STensor63* dCalgdeps = NULL) const;
  int getNsdv() const { return _nsdv;}
 #endif // SWIG
};

#endif // MLAWVUMAT_H_
