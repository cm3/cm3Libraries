//
// C++ Interface: numericalMaterial
//
// Description: Class creates microsolver from setting
//
//
// Author:  <Van Dung NGUYEN>, (C) 2012
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "numericalMaterial.h"
#include "nonLinearMechSolver.h"
#include "nonLinearBC.h"
#include "dirent.h"
#include "ipField.h"

void numericalMaterialBase::distributeMacroIP(const std::vector<int>& allIPNum, const std::vector<int>& allRanks,
                                    std::map<int,std::set<int> >& mapIP){
  int IPSize = allIPNum.size();
  mapIP.clear();

  int rankSize = allRanks.size();
  // init map
  /*
  for (int i=0; i<rankSize; i++){
    int rank = allRanks[i];
    std::set<int> numElOnRank;
    int startVal = IPSize*i/rankSize;
    int endVal = IPSize*(i+1)/rankSize;
    for (int j=startVal; j <endVal; j++){
      numElOnRank.insert(allIPNum[j]);
    }
    mapIP[rank] = numElOnRank;
  };
  */
  int numOnEachRank = IPSize/rankSize;
  for (int i=0; i<rankSize; i++){
    int rank = allRanks[i];
    std::set<int>& numElOnRank = mapIP[rank];
    for (int j=0; j<numOnEachRank; j++){
      int num = j+i*numOnEachRank;
      numElOnRank.insert(allIPNum[num]);
    };
  };
  int j=rankSize*numOnEachRank;
  while (j < IPSize){
    int i = j- rankSize*numOnEachRank;
    std::set<int>& numElOnRank = mapIP[allRanks[i]];
    numElOnRank.insert(allIPNum[j]);
    j++;
  };

  #ifdef _DEBUG
  for (std::map<int,std::set<int> >::iterator it = mapIP.begin(); it!= mapIP.end(); it++){
    int rank = it->first;
    std::set<int>& setIP = it->second;
    std::string fname = "IPonRank"+int2str(rank)+".csv";
    FILE* file = fopen(fname.c_str(),"a");
    for (std::set<int>::iterator it = setIP.begin(); it!= setIP.end(); it++){
      int num = *it;
      int e, g;
      numericalMaterialBase::getTwoIntsFromType(num,e,g);
      fprintf(file, "%d;%d;\n",e,g);
    }
    fclose(file);
  }
  #endif // _DEBUG
};

numericalMaterial::numericalMaterial(int tag) :
  numericalMaterialBase(),
  _isViewAll(false),
  _macroSolver(NULL)
{
  _microSolver = new nonLinearMechSolver(tag);
  // default options
  _microSolver->Scheme(1);
  _microSolver->Solver(2);
  _microSolver->setSystemType(1);
  _microSolver->setControlType(0);
  _microSolver->snlData(1,1.,1e-6,1e-10);
  _microSolver->stressAveragingFlag(true);
  _microSolver->setStressAveragingMethod(0);
  _microSolver->tangentAveragingFlag(true);
  _microSolver->setTangentAveragingMethod(2,1e-8);
  _microSolver->setSameStateCriterion(1e-10);
  _microSolver->setMicroSolverFlag(true);
  _microSolver->setMultiscaleFlag(true);
  
  // no view
  _microSolver->setHomogenizationPropertyArchiveFlag(false);
  _microSolver->setDisplacementAndIPArchiveFlag(false);
  _microSolver->setMessageView(false);
  _microSolver->setStrainArchiveFlag(false);
}
                
numericalMaterial::numericalMaterial(const numericalMaterial& src):
  numericalMaterialBase(src), 
  _meshOnIP(src._meshOnIP),
  _pertMaterialLawIP(src._pertMaterialLawIP),
  _allPertubationLaw(src._allPertubationLaw),
  _solverToView(src._solverToView),
  _isViewAll(src._isViewAll),
  _allGP(src._allGP),
  _macroSolver(NULL)
{
  _microSolver = NULL;
  if (src._microSolver)
  {
    _microSolver = new nonLinearMechSolver(*src._microSolver);
  }
};

numericalMaterial::~numericalMaterial(){
	// delete cache
  if (_microSolver) delete _microSolver;
	for (int i=0; i< _allocatedDomains.size(); i++){
		delete _allocatedDomains[i];
	}
	_allocatedDomains.clear();
	for (int i=0; i< _allocatedLaws.size(); i++){
		delete _allocatedLaws[i];
	}
	_allocatedLaws.clear();
};

nonLinearMechSolver* numericalMaterial::createMicroSolver(const int ele, const int gpt) const 
{
  /**create serial micro-solver **/
  int type = numericalMaterial::createTypeWithTwoInts(ele,gpt);
  std::string mshFileName = getMeshFileName(ele,gpt);
  std::map<int,std::string>::const_iterator itMestConstraint =  _meshOnIP.find(type);
  if (itMestConstraint != _meshOnIP.end())
  {
    mshFileName = itMestConstraint->second;
  };
  printf("create model from mesh file %s  \n",mshFileName.c_str());
  
  const std::vector<partDomain*>& allDomain = *(_microSolver->getDomainVector());
  std::vector<partDomain*> allDomainToMicroSolver;
  for (int i=0; i<allDomain.size(); i++)
  {
    partDomain* dom = allDomain[i]->clone();
    if (dom !=NULL) 
    {
      allDomainToMicroSolver.push_back(dom);
			_allocatedDomains.push_back(dom);
    }
    else
    {
      Msg::Error("partDomain::clone has not been defined");
      Msg::Exit(0);
    }
  };
  
  std::vector<materialLaw*> allLaw;
  if (_pertMaterialLawIP.find(type) == _pertMaterialLawIP.end())
  {
    const std::map<int,materialLaw*>& mapLaw = _microSolver->getMaplaw();
    for (std::map<int,materialLaw*>::const_iterator itlaw = mapLaw.begin(); itlaw != mapLaw.end(); itlaw++)
    {
			materialLaw* law = itlaw->second->clone();
			if (law != NULL)
      {
				allLaw.push_back(law);
				_allocatedLaws.push_back(law);
			}
			else
      {
				Msg::Error("materialLaw::clone must be defined");
        Msg::Exit(0);
			}
    };
  }
  else
  {
    Msg::Warning("perturbation in ele %d GP %d",ele,gpt);
    for (int i=0; i<_allPertubationLaw.size(); i++)
    {
			materialLaw* law = _allPertubationLaw[i]->clone();
			if (law != NULL)
      {
				allLaw.push_back(law);
				_allocatedLaws.push_back(law);
			}
			else
      {
				Msg::Error("materialLaw::clone must be defined");
        Msg::Exit(0);
			}
    };
  }
  
  nonLinearMechSolver* solver =  _microSolver->clone(_microSolver->getTag(),mshFileName,allDomainToMicroSolver,allLaw);
  /** set microsolver identification **/
  TwoNum tn(ele,ele);
  if (ele > 0)
  {
    if (_macroSolver==NULL) 
    {
      Msg::Error("macro solver is not correctly set");
      Msg::Exit(0);
    };
    
    const std::map<int,TwoNum>& interfaceMap = _macroSolver->getInterfaceElementsInverseMap();
    std::map<int,TwoNum>::const_iterator itInt = interfaceMap.find(ele);
    if (itInt == interfaceMap.end())
    {
      solver->setMicroProblemIndentification(ele,gpt);
    }
    else
    {
      tn = itInt->second;
      solver->setMicroProblemIndentification(tn.small,tn.large,gpt);
    }
  }
  else
  {
    solver->setMicroProblemIndentification(0,0);
  }
  
  bool willView = false;
  if (ele > 0)
  {
    if (_isViewAll and _allGP.find(gpt)!=_allGP.end())
    {
      willView = true;
    }
    else
    {
      std::map<TwoNum, std::set<int> >::const_iterator itV = _solverToView.find(tn);
      if (itV != _solverToView.end())
      {
        const std::set<int>& viewGP = itV->second;
        if (viewGP.find(gpt)!=viewGP.end())
        {
          willView = true;
        }
      } 
    }
  }
  

  /** for viewing**/
  if (willView)
  {
    solver->setMessageView(true);
    solver->setStrainArchiveFlag(true);
    solver->setHomogenizationPropertyArchiveFlag(true);
    solver->setDisplacementAndIPArchiveFlag(true);
  }
  else
  {
    solver->setMessageView(false);
    solver->setStrainArchiveFlag(false);
    solver->setHomogenizationPropertyArchiveFlag(false);
    solver->setDisplacementAndIPArchiveFlag(false);
  }
  return solver;
};

nonLinearMechSolver* numericalMaterial::getMicroSolver()
{
  return _microSolver;
}


void numericalMaterial::addPertutationalMeshIP(const int ele, const int ip, const std::string meshfile){
	Msg::Info("pert mesh it element %d ip %d",ele,ip);
	int type = numericalMaterial::createTypeWithTwoInts(ele,ip);
	_meshOnIP[type] = meshfile;
};

void numericalMaterial::setPerturbationMaterialLawIP(const int ele, const int ip){
  int num = numericalMaterial::createTypeWithTwoInts(ele,ip);
  _pertMaterialLawIP.insert(num);
};

void numericalMaterial::addPerturbationMaterialLaw(materialLaw* mlaw){
  _allPertubationLaw.push_back(mlaw);
}

// set view to this element
void numericalMaterial::addViewMicroSolver(const int e, const int gp){
  TwoNum tn(e,e);
  std::set<int>& saveGP = _solverToView[tn];
  saveGP.insert(gp);
};

void numericalMaterial::addViewMicroSolver(const int em, const int ep, const int gp){
  TwoNum tn(em,ep);
  std::set<int>& saveGP = _solverToView[tn];
  saveGP.insert(gp);
};

// set view all elements
void numericalMaterial::setViewAllMicroProblems(const bool flag, const int a){
  _allGP.insert(a);
  _isViewAll= flag;
};

void numericalMaterial::loadAllRVEMeshes(const std::string prefix){
  DIR *dir;
  struct dirent *ent;
  fileNameFilter* filter = new fileNameFilter(prefix,".msh");
  printf("read all file in current folder \n");
  if ((dir = opendir (".")) != NULL) {
    /* get all the files and directories within directory */
    while ((ent = readdir (dir)) != NULL) {
      std::string filename = ent->d_name;
      int num = filter->getNumber(filename);
      if (num>0){
        _allPertMesh[num] = filename;
        //printf("id: %d  -->filename: %s \n",num,filename.c_str());
      }
    }
    closedir (dir);
  }
  _idIterator = _allPertMesh.begin();
};

void numericalMaterial::printMeshIdMap() const{
  int rank = Msg::GetCommRank();
  std::string  filename = "idMap"+int2str(rank)+".txt";
  FILE* file =fopen(filename.c_str(),"a");
  for (std::map<int,int>::const_iterator it = _meshIdMap.begin(); it!= _meshIdMap.end(); it++){
    fprintf(file,"%d %d\n",it->first,it->second);
  }
  fclose(file);
};

void numericalMaterial::assignMeshId(const int e, const int gpt){
  if (_allPertMesh.size() == 0) return;
  int type = numericalMaterial::createTypeWithTwoInts(e, gpt);
  std::map<int,int>::iterator ittype = _meshIdMap.find(type);
  if (ittype!= _meshIdMap.end()) return;
  else{
    if (_idIterator == _allPertMesh.end())
      _idIterator = _allPertMesh.begin();

    _meshIdMap[type] = _idIterator->first;
    _idIterator++;
  }
};

void numericalMaterial::setMeshId(const int e, const int gpt, const int id) {
  if (_allPertMesh.size() == 0) return;
  int type = numericalMaterial::createTypeWithTwoInts(e, gpt);
  _meshIdMap[type] = id;
}

int numericalMaterial::getMeshId(const int e, const int gpt) const{
  int type = numericalMaterial::createTypeWithTwoInts(e,gpt);
  std::map<int,int>::const_iterator it = _meshIdMap.find(type);
  if (it !=_meshIdMap.end())
    return it->second;
  else{
    Msg::Warning("mesh id is not set: ELE %d GP %d",e,gpt);
    return 0;
  }
};

std::string numericalMaterial::getMeshFileName(const int e, const int gpt) const{
  if (_allPertMesh.size() ==0) return _microSolver->getMeshFileName();
  int id = this->getMeshId(e,gpt);
  std::map<int,std::string>::const_iterator it = _allPertMesh.find(id);
  if (it == _allPertMesh.end()){
    return _microSolver->getMeshFileName();
  }
  else{
    return it->second;
  }
};
#if defined(HAVE_MPI)
int numericalMaterial::getNumberValuesMPI() const {return _meshIdMap.size()*2;};
void numericalMaterial::fillValuesMPI(int* val) const{
  int iter=0;
  for (std::map<int,int>::const_iterator it = _meshIdMap.begin(); it!= _meshIdMap.end(); it++){
    val[iter] = it->first;
    val[iter+1] = it->second;
    iter+=2;
  }
};
void numericalMaterial::getValuesMPI(const int* val, const int size){
  for (int i=0; i<size; i+=2){
    _meshIdMap[val[i]] = val[i+1];
  }
};
#endif //HAVE_MPI



fileNameFilter::fileNameFilter(const std::string prefix, const std::string tag): _prefix(prefix),_fileTag(tag)  {}

int fileNameFilter::getNumber(const std::string& filename){
  if (filename.size() < _prefix.size() + _fileTag.size()) return 0;
  int count=0;
  for (int i=0; i<_prefix.size(); i++){
    if (_prefix[i]== filename[i])
      count++;
  }
  if (count != _prefix.size()) return 0;
  count=0;
  for (int i= _fileTag.size() -1; i>=0; i--){
    if (_fileTag[i] == filename[filename.size()-_fileTag.size()+i]){
      count++;
    }
  }
  if (count != _fileTag.size()) return 0;

  std::string temp;
  for (unsigned int i = 0; i<filename.size(); i++){
    if (isdigit(filename[i]))
      temp += filename[i];

  }

   std::istringstream stream(temp);
   int number;
   stream >> number;

   return number;
}


