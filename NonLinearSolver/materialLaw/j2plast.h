#ifndef J2PLAST_H
#define J2PLAST_H 1

#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "elasticlcc.h"
#include "matrix_operations.h"

#ifdef NONLOCALGMSH
#include "ID.h"
#endif


// Resolution of a constitutive model in elastoplasticity 
int constbox_ep(double E, double nu, double sy0, int htype, double hmod1, double hmod2, double hp0,
	double hexp, double *dstrn, double *strs_n, double *strs, double* pstrn_n, double* pstrn,
	double p_n, double *p, double *twomu_iso, double* dtwomu_iso, double** Calgo, double*** dCalgo, double* dpdE, bool residual);

// Resolution of a pressure sesitive constitutive model in elastoplasticity 
int constbox_ep(double E, double nu, double sy0, int htype, double hmod1, double hmod2, double hp0, double hexp, double alpha_DP, double m_DP, double nup,double *dstrn, double *strs_n, double *strs, double* pstrn_n, double* pstrn,	double p_n, double *p, double *twomu_iso, double* dtwomu_iso, double *twok_iso, double* dtwok_iso, double** Calgo, double*** dCalgo, double* dpdE, bool residual);

// epbox for large deformation
int constbox_ep(double E, double nu, double sy0, int htype, double hmod1, double hmod2, double hp0, double hexp, double *strstr, double *Rstrs_n, double *strs, double* pstrn_n, double* pstrn, double p_n, double *p,  double** Calgo, double*** dCalgo, double* dpdE, bool residual);

// Resolution of a constitutive model in elastoplasticity 
int constbox_evp(double E, double nu, double sy0, int htype, double hmod1, double hmod2, double hp0,
	double hexp, int viscmodel, double vmod, double vstressy, double vexp, double *dstrn, double *strs_n, double *strs, double* pstrn_n, 
        double* pstrn, double p_n, double *p, double *twomu_iso, double* dtwomu_iso,  double** Calgo, double*** dCalgo, 
        double* dpdE, bool residual, double dt);

int constbox_evp(double E, double nu, double sy0, int htype, double hmod1, double hmod2, double hp0,
	double hexp, int viscmodel, double vmod, double vstressy, double vexp, double *strs_tr, double *Rstrs_n, double *strs, double* pstrn_n, 
        double* pstrn, double p_n, double *p, double** Calgo, double*** dCalgo, double* dpdE, bool residual, double dt);

// Evaluation of the hardening function and its derivatives  
void j2hard (double p, double sy0, int htype, double hmod1, double hmod2, double hp0, double hexp, double* R, double* dR, double* ddR);

// Computation of J2 algorithmic tangent operator at tn 
void algoJ2(double E, double nu, double sy0, int htype, double hmod1, double hmod2, double hp0, double hexp, 
	double* strs, double p, double dp, double** Calgo);

// Evaluation of the viscoplastic function and its derivative 
void solve_gv(int viscmodel, double vmod, double vstressy, double vexp, double mu, double strseq, double yldstrs, double dt, double R, double dR, double ddR, double *gv, double *hv, double *dgdf, double *dhvdSeq, double *dhvdp, double eq2strs_n);

// Resolution of a constitutive model in elastoplasticity with second moment method
#ifdef NONLOCALGMSH
int constbox_ep(double E, double nu, double sy0, int htype, double hmod1, double hmod2, double hp0, double hexp, double *DE, double *dstrn, double *strs_n, double eq2strs_n, double *eq2strs, double dstrseq_tr, double *dstrseq_trdDE, double *strs, double* pstrn_n, double* pstrn, double p_n, double *p, MFH::FY2nd *FY2);

int constbox_evp(double E, double nu, double sy0, int htype, double hmod1, double hmod2, double hp0, double hexp, int viscmodel, double vmod, double vstressy, double vexp, double *DE, double *dstrn, double *strs_n, double eq2strs_n, double *eq2strs, double dstrseq_tr, double *dstrseq_trdDE, double *strs, double* pstrn_n, double* pstrn, double p_n, double *p, MFH::FY2nd *FY2, double dt, MFH::ELcc *LCC);
#else
int constbox_ep(double E, double nu, double sy0, int htype, double hmod1, double hmod2, double hp0, double hexp, double *DE, double *dstrn, double *strs_n, double eq2strs_n, double *eq2strs, double dstrseq_tr, double *dstrseq_trdDE, double *strs, double* pstrn_n, double* pstrn, double p_n, double *p, FY2nd *FY2);

int constbox_evp(double E, double nu, double sy0, int htype, double hmod1, double hmod2, double hp0, double hexp, int viscmodel, double vmod, double vstressy, double vexp, double *DE, double *dstrn, double *strs_n, double eq2strs_n, double *eq2strs, double dstrseq_tr, double *dstrseq_trdDE, double *strs, double* pstrn_n, double* pstrn, double p_n, double *p, FY2nd *FY2, double dt,ELcc *LCC);

#endif


#endif
