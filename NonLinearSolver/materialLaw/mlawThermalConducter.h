//
// C++ Interface: material law
//              thermal conductivity law
// Author:  <L. Homsy>, (C) 2015
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef MLAWTHERMALCONDUCTER_H_
#define MLAWTHERMALCONDUCTER_H_
#include "mlaw.h"
#include "STensor3.h"
#include "STensor43.h"
//#include "ipThermalConducter.h"

class mlawThermalConducter : public materialLaw
{
 protected:
  // can't be const due to operator= constructor (HOW TO CHANGE THIS ??)

  double _alpha; // parameter of anisotropy
  double _beta; // parameter of anisotropy
  double _gamma; // parameter of anisotropy
  double _rho;
  double _Kx;
  double _Ky;
  double _Kz;
  STensor3 _k;


#ifndef SWIG
  private: // cache data for constitutive function


#endif // SWIG
 public:
  mlawThermalConducter(const int num,const double alpha, const double beta, const double gamma,
						     const double Kx,const double Ky,const double Kz);
 #ifndef SWIG
  mlawThermalConducter(const mlawThermalConducter &source);
  mlawThermalConducter& operator=(const materialLaw &source);
	virtual ~mlawThermalConducter(){};
	virtual materialLaw* clone() const {return new mlawThermalConducter(*this);}
  virtual bool withEnergyDissipation() const {return false;};
  // function of materialLaw
  virtual matname getType() const{return materialLaw::ThermalConducter;}
  virtual void createIPState(IPStateBase* &ips, bool hasBodyForce, const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const IntPt *GP=NULL, const int gpt =0) const;
  virtual void initLaws(const std::map<int,materialLaw*> &maplaw){}; // this law is initialized so nothing to do
  virtual double soundSpeed() const {return 0;};
  virtual void checkInternalState(IPVariable* ipv, const IPVariable* ipvprev) const{};
  virtual void constitutive(const STensor3& F0,         // initial deformation gradient (input @ time n)
                            const STensor3& Fn,         // updated deformation gradient (input @ time n+1)
                            STensor3 &P,                // updated 1st Piola-Kirchhoff stress tensor (output)
                                                        // contains the initial values on input
                            const IPVariable *q0,       // array of initial internal variable
                            IPVariable *q1,              // updated array of internal variable (in ipvcur on output),
			    STensor43 &Tangent,         // constitutive tangents (output)
			    const bool stiff,
                            STensor43* elasticTangent = NULL, 
                            const bool dTangent =false,
                            STensor63* dCalgdeps = NULL
			    ) const
   {
     Msg::Error("mlawThermalConducter::constitutive is not defined");

   }
public:

  virtual const STensor3&  getConductivityTensor() const { return _k;};


 #endif // SWIG
};

#endif // MLAWTHERMALCONDUCTER_H_
