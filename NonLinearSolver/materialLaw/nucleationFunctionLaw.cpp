//
// C++ Interface: nucleation function law
//
// Description: Define the nucleation function law, 
// i.e. the relation between the nucleation and the plastic strain evolution
//
//
// Author:  <J. Leclerc>, (C) 2019
//
// Copyright: See COPYING file that comes with this distribution
//

#include "nucleationFunctionLaw.h"
#include "ipNonLocalPorosity.h"
#include "mlawNonLocalPorous.h"

// NucleationFunctionLaw : 
// -------------------------------------------
// Constructors and destructors
NucleationFunctionLaw::NucleationFunctionLaw( ) :
  num_(0), isInitialized_(false),
  mlawPorous_(NULL)
{
  Msg::Error( "NucleationFunctionLaw :: default constructor should never be called." );
};


NucleationFunctionLaw::NucleationFunctionLaw( const int num ) :
  num_(num), isInitialized_(false),
  mlawPorous_(NULL)
{
  // Nothing more to do. mlawPorous_ should be set later
};


NucleationFunctionLaw::NucleationFunctionLaw( const NucleationFunctionLaw& source ) :
  num_( source.num_ ), isInitialized_( source.isInitialized_ ),
  mlawPorous_( source.mlawPorous_ )
{
  // Nothing more to do.
};


NucleationFunctionLaw& NucleationFunctionLaw::operator=( const NucleationFunctionLaw& source )
{
  num_ = source.num_;
  isInitialized_ = source.isInitialized_;
  mlawPorous_ = source.mlawPorous_;
  return *this;
};


// General functions
// -------------- 
int NucleationFunctionLaw::getNum() const 
{
  return num_;
};




// Setting functions (non-const functions) used in internal interface
// --------------

void NucleationFunctionLaw::setNum( const int num) 
{
  num_ = num;
  Msg::Warning( "NucleationFunctionLaw:: the index of the law now n. ' %d ' have been modified !", num_ );
};



void NucleationFunctionLaw::initFunctionLaw( const mlawNonLocalPorosity * const mlawPorous )
{
  // Set the mlawPorous pointer Check for inconstencies before setting the initialisaiton flag to true.
  if ( mlawPorous == NULL ) {
    Msg::Error( "NucleationFunctionLaw::setmlawPorousPointer : mlawPorous pointer is NULL." );
  };
  mlawPorous_ = mlawPorous;
  
  isInitialized_ = checkParametersIntegrity();
  if ( !isInitialized_ ) {
    Msg::Error( "NucleationFunctionLaw::initialiseAndIntegrateCriterion: Inconsistencies detected." );
  };
  
};



// Specific functions
// --------------
const bool NucleationFunctionLaw::checkParametersIntegrity() const
{
  // Determine if the law is correctly initialised. 
  // Nothing has to be done inside the base class for now.
  return true;
};


// TrivialNucleationFunctionLaw
// -------------------------------------------
// Constructors and destructors
// -------------- 
TrivialNucleationFunctionLaw::TrivialNucleationFunctionLaw( const int num ) :
  NucleationFunctionLaw(num)
{
  // Nothing more to do.
};


TrivialNucleationFunctionLaw::TrivialNucleationFunctionLaw( const TrivialNucleationFunctionLaw& source ) :
  NucleationFunctionLaw(source)
{
  // Nothing more to do.
};


TrivialNucleationFunctionLaw& TrivialNucleationFunctionLaw::operator= ( const NucleationFunctionLaw& source )
{
  NucleationFunctionLaw::operator =( source );
  const TrivialNucleationFunctionLaw* source_casted = dynamic_cast< const TrivialNucleationFunctionLaw* >( &source );
  if( source_casted != NULL )
  {
    // Nothing to do.
  }
  return *this;
};


// General functions of materialLaw
// --------------
void TrivialNucleationFunctionLaw::createIPVariable( IPNucleationFunction* &ipNuclFunction ) const
{
  if( ipNuclFunction != NULL ) delete ipNuclFunction;
  ipNuclFunction = new IPTrivialNucleationFunction();
};


// Specific functions
// --------------
const bool TrivialNucleationFunctionLaw::checkParametersIntegrity() const
{
  return NucleationFunctionLaw::checkParametersIntegrity();
};

void TrivialNucleationFunctionLaw::nucleate( const double p, const double p0, IPNonLocalPorosity& q1Porous, const IPNonLocalPorosity& q0Porous ) const
{
  // Nothing has to be done or updated:
  // IPTrivialNucleationFunction->getNucleatedPorosity() and associated functions will always be 0.
};

void TrivialNucleationFunctionLaw::applyFunctionOnsetInIP( IPNonLocalPorosity& q1Porous  ) const
{
  // Nothing has to be done or updated as there is no onset
};

void TrivialNucleationFunctionLaw::previousBasedRefresh( IPNucleationFunction& q1function, const IPNucleationFunction& q0function ) const
{
  // Nothing has to be done as nothing is stored.
};


void TrivialNucleationFunctionLaw::computeNucleationRate( double& An, double& d_An_dp, const double& p, const IPNucleationFunction& q1function ) const
{
  // Nothing has to be done.
  Msg::Error( "TrivialNucleationFunctionLaw::computeNucleationRate : This function should not be used." );
};



// NonTrivialNucleationFunctionLaw
// -------------------------------------------
// Constructors and destructors
// -------------- 
NonTrivialNucleationFunctionLaw::NonTrivialNucleationFunctionLaw( const int num ) :
  NucleationFunctionLaw(num)
{
  // Nothing more to do.
};

NonTrivialNucleationFunctionLaw::NonTrivialNucleationFunctionLaw( const NonTrivialNucleationFunctionLaw& source ) :
  NucleationFunctionLaw(source)
{
  // Nothing more to do.
};

NonTrivialNucleationFunctionLaw& NonTrivialNucleationFunctionLaw::operator= ( const NucleationFunctionLaw& source )
{
  /// Copy mother class.
  NucleationFunctionLaw::operator =( source );
  
  /// Copy this class.
  const NonTrivialNucleationFunctionLaw* source_casted = dynamic_cast< const NonTrivialNucleationFunctionLaw* >( &source );
  if( source_casted != NULL )
  {
    // Nothing to do.
  }
  return *this;
};


// Specific functions
// --------------
void NonTrivialNucleationFunctionLaw::nucleate( const double p, const double p0, IPNonLocalPorosity& q1Porous, const IPNonLocalPorosity& q0Porous ) const
{
  // Perfom the generic operations done with all ipvs. 
  // Specific part (rate datas) are done in others virtual functions, proper to each derived-class.
  
  /// Get the different needed ipvs.
  const IPNucleationFunction& q0i_function = q0Porous.getConstRefToIPNucleation().getConstRefToIPNucleationFunction( num_ );
  IPNucleationFunction& q1i_function = q1Porous.getRefToIPNucleation().getRefToIPNucleationFunction( num_ );
  
  const IPNucleationCriterion& q0i_criterion = q0Porous.getConstRefToIPNucleation().getConstRefToIPNucleationCriterion( num_ );
  
  /// Determine if nucleation is activated or not and perform the adequate operation on the ipv.
  bool increasingPlasticStrain = (p >= p0);
  bool ipvHasNucleationActivated = q0i_criterion.hasNucleationActivated();
  
  bool hasNucleationAtThisStep = ( increasingPlasticStrain and ipvHasNucleationActivated );
  
  if ( hasNucleationAtThisStep ) {
    
    /// Compute the porosity nucleaiton and its derivatives from the nucleation rate
    double nuclRate, d_nuclRate_dp;
    this->computeNucleationRate( nuclRate, d_nuclRate_dp, p, q1i_function );

    double deltaFV = nuclRate * (p - p0);
    double fV =  q0i_function.getNucleatedPorosity() + deltaFV;
    double d_deltaFV_dp = nuclRate + d_nuclRate_dp * (p - p0);
    
    /// Set the ipv - variables
    q1i_function.setNucleatedPorosity( fV );
    q1i_function.setNucleatedPorosityIncrement( deltaFV );
    q1i_function.setNucleationRate( d_deltaFV_dp );
    
  }
  else{
    /// Restore the ipv-variables to their fresh values if no nucleation occured.
    previousBasedRefresh( q1i_function, q0i_function );
    
  }
  
};








// LinearNucleationFunctionLaw
// -------------------------------------------
// Constructors and destructors
// --------------
LinearNucleationFunctionLaw::LinearNucleationFunctionLaw( const int num, const double A0) :
  NonTrivialNucleationFunctionLaw(num),
  A0_(A0)
{
  // Check if values are meaningfull.
  bool isOk = checkParametersIntegrity();
  
  if ( !isOk ) {
    Msg::Error( "GaussianNucleationFunctionLaw:: checkParametersIntegrity has failed." );
  }
  
};


LinearNucleationFunctionLaw::LinearNucleationFunctionLaw( const LinearNucleationFunctionLaw& source ) :
  NonTrivialNucleationFunctionLaw(source),
  A0_(source.A0_)
{
  // Nothing more to do.
};


LinearNucleationFunctionLaw& LinearNucleationFunctionLaw::operator= ( const NucleationFunctionLaw& source )
{
  /// Copy mother class.
  NonTrivialNucleationFunctionLaw::operator =( source );
  
  /// Copy this class.
  const LinearNucleationFunctionLaw* source_casted = dynamic_cast< const LinearNucleationFunctionLaw* >( &source );
  if( source_casted != NULL )
  {
    A0_ = source_casted->A0_;
  }
  return *this;
};


// General functions of materialLaw
// --------------
void LinearNucleationFunctionLaw::createIPVariable( IPNucleationFunction* &ipNuclFunction ) const
{
  if( ipNuclFunction != NULL ) delete ipNuclFunction;
  ipNuclFunction = new IPLinearNucleationFunction();
};


// Specific functions
// --------------
const bool LinearNucleationFunctionLaw::checkParametersIntegrity() const
{
  // Check if values are meaningfull (return false if a problem is encountered)
  // (and check from parent classes are satisfied).
  if( !NucleationFunctionLaw::checkParametersIntegrity() ){
    return false;
  }
  
  if ( A0_ < 0. ){
    Msg::Error( "LinearNucleationFunctionLaw::checkParametersIntegrity "
                "uncorrect parameters A0 = %e < 0.0" , A0_ );
    return false;
  };
  
  /// Return true as no error is detected so far.
  return true;
};

void LinearNucleationFunctionLaw::applyFunctionOnsetInIP( IPNonLocalPorosity& q1Porous  ) const
{
  // Nothing has to be done or updated as there is no parameters to adapt at the onset.
};


void LinearNucleationFunctionLaw::computeNucleationRate( double& An, double& d_An_dp, const double& p, const IPNucleationFunction& q1function ) const
{
  An = A0_;
  d_An_dp = 0.0;
}



// GaussianNucleationFunctionLaw
// -------------------------------------------
// Constructors and destructors
// --------------
GaussianNucleationFunctionLaw::GaussianNucleationFunctionLaw( const int num, const double fn, const double sn, const double en ) :
  NonTrivialNucleationFunctionLaw(num),
  fn_(fn), sn_(sn), en_(en)
{
  // Check if values are meaningfull.
  bool isOk = checkParametersIntegrity();
  
  if ( !isOk ) {
    Msg::Error( "GaussianNucleationFunctionLaw:: checkParametersIntegrity has failed." );
  }
  
};


GaussianNucleationFunctionLaw::GaussianNucleationFunctionLaw( const GaussianNucleationFunctionLaw& source ) :
  NonTrivialNucleationFunctionLaw(source),
  fn_(source.fn_), sn_(source.sn_), en_(source.en_)
{
  // Nothing more to do.
};


GaussianNucleationFunctionLaw& GaussianNucleationFunctionLaw::operator= ( const NucleationFunctionLaw& source )
{
  /// Copy mother class.
  NonTrivialNucleationFunctionLaw::operator =( source );
  
  /// Copy this class.
  const GaussianNucleationFunctionLaw* source_casted = dynamic_cast< const GaussianNucleationFunctionLaw* >( &source );
  if( source_casted != NULL ) {
    fn_ = source_casted->fn_;
    sn_ = source_casted->sn_;
    en_ = source_casted->en_;
  }
  return *this;
};


// General functions of materialLaw
// --------------
void GaussianNucleationFunctionLaw::createIPVariable( IPNucleationFunction* &ipNuclFunction ) const
{
  if( ipNuclFunction != NULL ) delete ipNuclFunction;
  
  
  if ( mlawPorous_->getNucleationLaw()->getConstRefToNucleationCriterionLaw(num_).getType() == NucleationCriterionLaw::trivialCriterion) {
    ipNuclFunction = new IPGaussianNucleationFunction( en_, fn_ );
  }
  else{
    ipNuclFunction = new IPGaussianNucleationFunction( 0.0 , 0.0 );
  }
  
};



// Specific functions
// --------------
const bool GaussianNucleationFunctionLaw::checkParametersIntegrity() const
{
  // Check if values are meaningfull (return false if a problem is encountered)
  // (i.e check from parent classes are satisfied)
  if( !NucleationFunctionLaw::checkParametersIntegrity() ){
    return false;
  }

  if ( fn_ < 0. ){
    Msg::Info( "GaussianNucleationFunctionLaw::checkParametersIntegrity "
               "uncorrect parameters fn = %e < 0.0" , fn_ );
    return false;
  };
  
    if ( sn_ < 0. ){
    Msg::Info( "GaussianNucleationFunctionLaw::checkParametersIntegrity "
               "uncorrect parameters sn = %e < 0.0" , sn_ );
    return false;
  };
  
  if ( en_ <  3.0*sn_ ){
    /// This case is only a info / warning
    Msg::Info( "GaussianNucleationFunctionLaw::checkParametersIntegrity "
               "parameters en = %e < 3.*sn. Be sure of what you are doing !", en_);
  };
  
  
  /// Return true as no error is detected so far.
  return true;
};


void GaussianNucleationFunctionLaw::computeNucleationRate( double& An, double& d_An_dp, const double& p, const IPNucleationFunction& q1function ) const
{
  double en = q1function.getMeanNucleationStrain();
  double fn = q1function.getTotalNucleatedQuantity();
  An =  fn / sqrt( 2.0 *  3.141592653589793 ) / sn_ * exp( (-0.5)*( p - en )*( p - en ) / sn_ / sn_);
  d_An_dp = - An * (p - en) / sn_ / sn_;
}


void GaussianNucleationFunctionLaw::applyFunctionOnsetInIP( IPNonLocalPorosity& q1Porous  ) const
{
  // Compute the adequate peak value and then set it in the IPV.

  IPNucleationFunction& q1i_function = q1Porous.getRefToIPNucleation().getRefToIPNucleationFunction(num_);
  IPNucleationCriterion& q1i_criterion = q1Porous.getRefToIPNucleation().getRefToIPNucleationCriterion(num_);
  
  /// The maximal value should be at 3 times sigma from the starting point to ensure 99.865 % of the total value (without integration error)
  double meanNucleationValue = q1Porous.getLocalMatrixPlasticStrain() + 3.0 * sn_;
  q1i_function.setMeanNucleationStrain(meanNucleationValue);
  q1i_function.setTotalNucleatedQuantity( fn_ * q1i_criterion.getAmpli() );
  
};








