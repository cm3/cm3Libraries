//
// Created by vinayak on 24.08.22.
//
// C++ Interface: material law for inductor region in ElecMag problem
// computing normal direction to a gauss point to impose current
// density js0 in inductor
//
// Author:  <Vinayak GHOLAP>, (C) 2022
//
// Copyright: See COPYING file that comes with this distribution
//

#include "mlawElecMagInductor.h"
#include "MInterfaceElement.h"

mlawElecMagInductor::mlawElecMagInductor(const int num,
                                         const double alpha, const double beta, const double gamma,
                                         const double lx,const double ly,const double lz,
                                         const double seebeck,const double v0,
                                         const double mu_x, const double mu_y, const double mu_z,
                                         const double A0_x, const double A0_y, const double A0_z,
                                         const double Irms, const double freq, const unsigned int nTurnsCoil,
                                         const double coilLength_x, const double coilLength_y, const double coilLength_z,
                                         const double coilWidth,
                                         const double Centroid_x, const double Centroid_y, const double Centroid_z,
                                         const double CentralAxis_x, const double CentralAxis_y, const double CentralAxis_z,
                                         const bool useFluxT, const bool evaluateCurlField):
                                         mlawElecMagGenericThermoMech(num,alpha, beta,gamma,lx,ly,lz,seebeck,v0,mu_x, mu_y, mu_z,
                                                                      0.0,0.0,0.0,0.0,0.0,0.0,
                                                                      A0_x,A0_y, A0_z,Irms, freq,nTurnsCoil,coilLength_x, coilLength_y,
                                                                      coilLength_z,coilWidth, useFluxT,evaluateCurlField),
                                         _Centroid(Centroid_x, Centroid_y, Centroid_z),
                                         _CentralAxis(CentralAxis_x, CentralAxis_y, CentralAxis_z)
{
    // Compute the magnitude of imposed current density in inductor
    // js0 = sqrt(2)*Irms*nTurns/crossSecArea
    const double crossSectionCoil = coilLength_y * coilWidth; // Assumed coil cross section is rectangular block
    if(crossSectionCoil != 0.0)    this->_js0 = std::sqrt(2) * Irms * double(nTurnsCoil)/crossSectionCoil;
    else this->_js0 = 0.0;
}

mlawElecMagInductor::mlawElecMagInductor(const mlawElecMagInductor &source)
: mlawElecMagGenericThermoMech(source), _Centroid(source._Centroid), _CentralAxis(source._CentralAxis)
{}

mlawElecMagInductor& mlawElecMagInductor::operator=(const materialLaw &source)
{
    mlawElecMagGenericThermoMech::operator=(source);
    const mlawElecMagInductor* src =static_cast<const mlawElecMagInductor*>(&source);

    _Centroid = src->_Centroid;
    _CentralAxis = src->_CentralAxis;
    return *this;
}

void mlawElecMagInductor::createIPState(IPStateBase* &ips, bool hasBodyForce,const bool* state_,const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
    if(ips != NULL) delete ips;

    SPoint3 pt_Global;
    ele->pnt(GP->pt[0],GP->pt[1],GP->pt[2],pt_Global);
    SVector3 GaussP(pt_Global);

    IPElecMagInductor* ipvi = new IPElecMagInductor(GaussP, _Centroid, _CentralAxis);
    IPElecMagInductor* ipv1 = new IPElecMagInductor(GaussP, _Centroid, _CentralAxis);
    IPElecMagInductor* ipv2 = new IPElecMagInductor(GaussP, _Centroid, _CentralAxis);

    IPStateBase* ipsthermomeca=NULL;
    getConstRefToThermoMechanicalMaterialLaw().createIPState(ipsthermomeca,hasBodyForce, state_,ele, nbFF_,GP, gpt);
    std::vector<IPVariable*> allIP;
    ipsthermomeca->getAllIPVariable(allIP);

    ipvi->setIpThermoMech(*allIP[0]->clone());
    ipv1->setIpThermoMech(*allIP[1]->clone());
    ipv2->setIpThermoMech(*allIP[2]->clone());

    ips = new IP3State(state_,ipvi,ipv1,ipv2);
    delete ipsthermomeca;
}

void mlawElecMagInductor::createIPVariable(IPElecMagInductor* &ipv, bool hasBodyForce, const MElement *ele, const int nbFF,
                                           const IntPt *GP, const int gpt) const
{
    SPoint3 pt_Global;
    ele->pnt(GP->pt[0],GP->pt[1],GP->pt[2],pt_Global);
    SVector3 GaussP(pt_Global);

    if(ipv != NULL)
        delete ipv;
    ipv = new IPElecMagInductor(GaussP, _Centroid, _CentralAxis);

    IPStateBase* ipsthermomech=NULL;
    const bool state_ = true;
    getConstRefToThermoMechanicalMaterialLaw().createIPState(ipsthermomech,hasBodyForce, &state_,ele, nbFF,GP, gpt);

    std::vector<IPVariable*> allIP;
    ipsthermomech->getAllIPVariable(allIP);

    ipv->setIpThermoMech(*allIP[0]->clone());
    delete ipsthermomech;
}