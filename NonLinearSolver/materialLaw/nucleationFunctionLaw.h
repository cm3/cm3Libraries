//
// C++ Interface: nucleation function law
//
// Description: Define the nucleation function law, 
// i.e. the relation between the nucleation and the plastic strain evolution
//
//
// Author:  <J. Leclerc>, (C) 2019
//
// Copyright: See COPYING file that comes with this distribution
//


#ifndef _NUCLEATIONFUNCTIONLAW_H_
#define _NUCLEATIONFUNCTIONLAW_H_
#ifndef SWIG
#include "ipNucleationFunction.h"
class IPNonLocalPorosity;
class mlawNonLocalPorosity;
#endif // SWIG



/*! \class NucleationFunctionLaw
 * This mother class defines the commun interface for the different nucleation functions.
 * They compute the nucleation rates in terms of the plastic
 * strain increment once they are activated.
 * They send back the increment /\fv.
 */
class NucleationFunctionLaw
{
public :
  //! type enumeration for the nucleation function
  enum nucleationFunctionName{ GaussianNucleationRateFunction,  //!< Gaussian rate
                               LinearNucleationRateFunction,    //!< Constant linear rate
                               TrivialNucleationRateFunction    //!< Trivial function rate (no nucleation)
                              };
#ifndef SWIG
protected :
  //! identifier of the internal law
  int num_;
  
  //! is true once the internal law is correctly initialised.
  bool isInitialized_;
  
  //! pointer to the corresponding mlawPorous
  const mlawNonLocalPorosity* mlawPorous_;

  //! @name Constructors and destructors

private :
  //! default constructor is always forbidden
  NucleationFunctionLaw();
  
public :
  NucleationFunctionLaw( const int num );
  NucleationFunctionLaw( const NucleationFunctionLaw& source );
  virtual NucleationFunctionLaw& operator= ( const NucleationFunctionLaw& source );
  virtual ~NucleationFunctionLaw(){};
  virtual NucleationFunctionLaw* clone() const = 0;
  //! @}

  
  //! @name General functions
  virtual int getNum() const;
  virtual const bool isInitialized() const { return isInitialized_; };
  virtual nucleationFunctionName getType() const = 0;
  virtual void createIPVariable( IPNucleationFunction* &ipNuclFunction ) const = 0;
  //! @}

  //! @name Setting functions used in internal interface
  //! It corresponds to non-const functions used outside SWIG interface,
  //! to set or exchange internal parameters or initialise the laws.
  //! Set the number of the nucleation law.
  virtual void setNum( const int num);
  
  //! Initialise the criterion and execute checkParametersIntegrity()
  virtual void initFunctionLaw( const mlawNonLocalPorosity * const mlawPorous );
  //! @}
 
  //! @name Specific functions
protected:
  //! Compute the nucleation rate with the plastic strain and the nucleaiton state
  virtual void computeNucleationRate( double& An, double& d_An_dp, const double& p, const IPNucleationFunction& q1function ) const = 0;
  //! Check the parameters integrity before the simulation. If an inconsistency is detected, send back false.
  virtual const bool checkParametersIntegrity() const;
  
public :
  //! Compute the nucleation increment via "computeNucleationRate" and keep the variables up-to-date between each steps.
  virtual void nucleate( const double p, const double p0, IPNonLocalPorosity& q1Porous, const IPNonLocalPorosity& q0Porous ) const = 0;
  //! Set the onset values if needed when the onset occured (signal send by the criterionLaw)
  virtual void applyFunctionOnsetInIP( IPNonLocalPorosity& q1Porous ) const = 0;
  //! Reset the variables / refresh the ipv to the adequate values at the begining of a time step.
  virtual void previousBasedRefresh( IPNucleationFunction& q1function, const IPNucleationFunction& q0function ) const = 0;
  //! @}
  
  
#endif // SWIG
};




/*! \class TrivialNucleationFunctionLaw
 * This class corresponds to no nucleaiton at all.
 * /\fv is always equal to zero and nothing is stored.
 */
class TrivialNucleationFunctionLaw : public NucleationFunctionLaw
{
protected :
  // Nothing is needed


  //! @name Constructors and destructors
public :
  TrivialNucleationFunctionLaw( const int num );
#ifndef SWIG
  TrivialNucleationFunctionLaw( const TrivialNucleationFunctionLaw& source );
  TrivialNucleationFunctionLaw& operator= ( const NucleationFunctionLaw& source );
  virtual ~TrivialNucleationFunctionLaw(){};
  virtual NucleationFunctionLaw* clone() const { return new TrivialNucleationFunctionLaw(*this); };
  //! @}
  
  //! @name General functions of materialLaw
  virtual nucleationFunctionName getType() const { return NucleationFunctionLaw::TrivialNucleationRateFunction; };
  virtual void createIPVariable( IPNucleationFunction* &ipNuclFunction ) const;
  //! @}


  //! @name Specific functions
protected :
  virtual const bool checkParametersIntegrity() const;
  virtual void computeNucleationRate( double& An, double& d_An_dp, const double& p, const IPNucleationFunction& q1function ) const;

public :
  virtual void nucleate( const double p, const double p0, IPNonLocalPorosity& q1Porous, const IPNonLocalPorosity& q0Porous ) const;
  virtual void applyFunctionOnsetInIP( IPNonLocalPorosity& q1Porous  ) const;
  virtual void previousBasedRefresh( IPNucleationFunction& q1function, const IPNucleationFunction& q0function ) const;
  //! @}
#endif // SWIG
};




/*! \class NonTrivialNucleationFunctionLaw
 * This Virtual class corresponds to the basic class for non-zero nucleation.
 */
class NonTrivialNucleationFunctionLaw : public NucleationFunctionLaw
{
protected :
  // Nothing is needed (virtual class)

#ifndef SWIG
  //! @name Constructors and destructors
public :
  NonTrivialNucleationFunctionLaw( const int num );

  NonTrivialNucleationFunctionLaw( const NonTrivialNucleationFunctionLaw& source );
  NonTrivialNucleationFunctionLaw& operator= ( const NucleationFunctionLaw& source );
  virtual ~NonTrivialNucleationFunctionLaw(){};
  virtual NucleationFunctionLaw* clone() const = 0;
  //! @}
  
  //! @name General functions of materialLaw
  virtual nucleationFunctionName getType() const = 0;
  virtual void createIPVariable( IPNucleationFunction* &ipNuclFunction ) const = 0;
  //! @}


  //! @name Specific functions
protected :
  virtual const bool checkParametersIntegrity() const = 0;
  //! This function has to be kept virtual as it is case-dependent.
  virtual void computeNucleationRate( double& An, double& d_An_dp, const double& p, const IPNucleationFunction& q1function ) const = 0;

public :
  virtual void nucleate( const double p, const double p0, IPNonLocalPorosity& q1Porous, const IPNonLocalPorosity& q0Porous ) const;
  virtual void applyFunctionOnsetInIP( IPNonLocalPorosity& q1Porous  ) const = 0; //!< This function has to be kept virtual as it is case-dependent.
  inline virtual void previousBasedRefresh( IPNucleationFunction& q1function, const IPNucleationFunction& q0function ) const
  {
    // Transfer data commun to each derived-ipv.
    q1function.setNucleatedPorosity( q0function.getNucleatedPorosity() );
    q1function.setNucleatedPorosityIncrement( 0.0 );
    q1function.setNucleationRate( 0.0 );
  };
  //! @}
  
#endif // SWIG
};





/*! \class LinearNucleationFunctionLaw
 * This class uses a constant nucleation rate A0
 * to compute the nucleated porosity /\fv
 * following /\fv = A0 /\p
 */
class LinearNucleationFunctionLaw : public NonTrivialNucleationFunctionLaw
{
#ifndef SWIG
protected :
  //! (constant) nucleation rate
  double A0_;

#endif // SWIG


  //! @name Constructors and destructors
public :
  LinearNucleationFunctionLaw( const int num, const double A0 );
#ifndef SWIG
  LinearNucleationFunctionLaw( const LinearNucleationFunctionLaw& source );
  LinearNucleationFunctionLaw& operator= ( const NucleationFunctionLaw& source );
  virtual ~LinearNucleationFunctionLaw(){};
  virtual NucleationFunctionLaw* clone() const { return new LinearNucleationFunctionLaw( *this ); };
  //! @}
  
  //! @name General functions of materialLaw
  virtual nucleationFunctionName getType() const { return NucleationFunctionLaw::LinearNucleationRateFunction; };
  virtual void createIPVariable( IPNucleationFunction* &ipNuclFunction ) const;
  //! @}


  //! @name Specific functions
protected :
  virtual const bool checkParametersIntegrity() const;
  virtual void computeNucleationRate( double& An, double& d_An_dp, const double& p, const IPNucleationFunction& q1function ) const;
  
  
public :
  virtual void applyFunctionOnsetInIP( IPNonLocalPorosity& q1Porous  ) const;
  //! @}
  
  
#endif // SWIG
};




/*! \class GaussianNucleationFunctionLaw
 * This class uses a Guaussian function g(p) as nucleation function,
 * following /\fv = g(p_n + /\p) * /\p
 */

 class GaussianNucleationFunctionLaw : public NonTrivialNucleationFunctionLaw
{
#ifndef SWIG
protected :
  //! total quantity to be nucleated
  double fn_;
  //! standard deviation for the Gaussian function
  double sn_;
  //! cordinates of the maximal value / peak value of the Gaussian function or values of nucleation mean strain
  double en_;

#endif // SWIG


  //! @name Constructors and destructors
public :
  GaussianNucleationFunctionLaw( const int num, const double fn, const double sn, const double en);
#ifndef SWIG
  GaussianNucleationFunctionLaw( const GaussianNucleationFunctionLaw& source );
  GaussianNucleationFunctionLaw& operator= ( const NucleationFunctionLaw& source );
  virtual ~GaussianNucleationFunctionLaw(){};
  virtual NucleationFunctionLaw* clone() const { return new GaussianNucleationFunctionLaw( *this ); };
  //! @}
  
  //! @name General functions of materialLaw
  virtual nucleationFunctionName getType() const { return NucleationFunctionLaw::GaussianNucleationRateFunction; };
  virtual void createIPVariable( IPNucleationFunction* &ipNuclFunction ) const;
  //! @}

  //! @name Specific functions
protected :
  virtual const bool checkParametersIntegrity() const;
  virtual void computeNucleationRate( double& An, double& d_An_dp, const double& p, const IPNucleationFunction& q1function ) const;
  
public :
  virtual void applyFunctionOnsetInIP( IPNonLocalPorosity& q1Porous  ) const;
  inline virtual void previousBasedRefresh( IPNucleationFunction& q1function, const IPNucleationFunction& q0function ) const
  {
    NonTrivialNucleationFunctionLaw::previousBasedRefresh( q1function, q0function );
    q1function.setMeanNucleationStrain( q0function.getMeanNucleationStrain() );
    q1function.setTotalNucleatedQuantity( q0function.getTotalNucleatedQuantity() );
  };
  
  //! @}
#endif // SWIG
};







#endif // _NUCLEATIONFUNCTIONLAW_H_


