//
// C++ Interface: material law
//
// Description: j2 linear elasto-plastic law with electrophysiology interface
//
// Author:  <A. Jerusalem>, (C) 2013
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef MLAWELECTROPHYSIOJ2HYPER_H_
#define MLAWELECTROPHYSIOJ2HYPER_H_
#include "mlawJ2linear.h"
#include "ipElectroPhysioJ2Hyper.h"
#include "j2IsotropicHardening.h"
#include "ElectroPhysioLaw.h"

class mlawElectroPhysioJ2Hyper : public mlawJ2linear
{
 protected:
  ElectroPhysio  *epLaw;
  STensor43   Cel;
 public:
  mlawElectroPhysioJ2Hyper(const int num,const double E,const double nu, const double rho, 
				const J2IsotropicHardening &_j2IH, const ElectroPhysio &_epLaw,
				const double tol=1.e-6, const bool matrixbyPerturbation = false, const double pert = 1e-8);
 #ifndef SWIG
  mlawElectroPhysioJ2Hyper(const mlawElectroPhysioJ2Hyper &source);
  mlawElectroPhysioJ2Hyper& operator=(const materialLaw &source);
  virtual ~mlawElectroPhysioJ2Hyper() 
  { 
    if (epLaw!= NULL) delete epLaw;
  }
	virtual materialLaw* clone() const {return new mlawElectroPhysioJ2Hyper(*this);}
  virtual bool withEnergyDissipation() const {return true;};
  // function of materialLaw
  virtual matname getType() const{return materialLaw::electroPhysioJ2Hyper;}
  virtual void createIPState(IPElectroPhysioJ2Hyper *ivi, IPElectroPhysioJ2Hyper *iv1, IPElectroPhysioJ2Hyper *iv2) const;
  virtual void createIPState(IPStateBase* &ips, bool hasBodyForce, const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const IntPt *GP=NULL, const int gpt = 0) const;
  virtual void createIPVariable(IPElectroPhysioJ2Hyper *&ipv,bool hasBodyForce, const MElement *ele,const int nbFF,const IntPt *GP, const int gpt) const;

  virtual void initLaws(const std::map<int,materialLaw*> &maplaw){}; // this law is initialized so nothing to do
  virtual double soundSpeed() const;                                 // default but you can redefine it for your case
  virtual const ElectroPhysio *getElectroPhysio() const {return epLaw; };
  // specific function
 public:
  virtual void constitutive(
                            const STensor3& F0,                     // initial deformation gradient (input @ time n)
                            const STensor3& Fn,                     // updated deformation gradient (input @ time n+1)
                            STensor3 &P,                            // updated 1st Piola-Kirchhoff stress tensor (output)
                                                                    // contains the initial values on input
                            const IPElectroPhysioJ2Hyper *q0,       // array of initial internal variable
                            IPElectroPhysioJ2Hyper *q1,             // updated array of internal variable (in ipvcur on output),
                            STensor43 &Tangent,                     // constitutive tangents (output)
                            const bool stiff                        // if true compute the tangents
                           ) const;

  virtual void constitutive(
                            const STensor3& F0,                     // initial deformation gradient (input @ time n)
                            const STensor3& Fn,                     // updated deformation gradient (input @ time n+1)
                            STensor3 &P,                            // updated 1st Piola-Kirchhoff stress tensor (output)
                                                                    // contains the initial values on input
                            const IPElectroPhysioJ2Hyper *q0,       // array of initial internal variable
                            IPElectroPhysioJ2Hyper *q1,             // updated array of internal variable (in ipvcur on output),
                            STensor43 &Tangent,                     // constitutive tangents (output)
			    STensor3  &TangentVV,
			    double    &TangentVu,
                            const bool stiff                        // if true compute the tangents
                           ) const;

  protected:

 #endif // SWIG
};

#endif // MLAWELECTROPHYSIOJ2HYPER_H_
