//
// Created by vinayak on 17.08.22.
//
// C++ Interface: material law
//              Electro-Magnetic with Generic Thermo-Mechanical law
// Author:  <Vinayak GHOLAP>, (C) 2022
//
// Copyright: See COPYING file that comes with this distribution
//
#include "mlawElecMagGenericThermoMech.h"

mlawElecMagGenericThermoMech::mlawElecMagGenericThermoMech(const int num, const bool init) :
        materialLaw(num, init)
{
    Msg::Error("mlawElecMagGenericThermoMech: Cannot use the default constructor");
    _genericThermoMechLaw= NULL;
}

mlawElecMagGenericThermoMech::mlawElecMagGenericThermoMech(const int num,
                                                           const double alpha, const double beta, const double gamma,
                                                           const double lx,const double ly,const double lz,
                                                           const double seebeck,const double v0,
                                                           const double mu_x, const double mu_y, const double mu_z,
                                                           const double epsilon_4,const double epsilon_5,const double epsilon_6,
                                                           const double mu_7,const double mu_8,const double mu_9,
                                                           const double A0_x, const double A0_y, const double A0_z,
                                                           const double Irms, const double freq, const unsigned int nTurnsCoil,
                                                           const double coilLength_x, const double coilLength_y, const double coilLength_z,
                                                           const double coilWidth, const bool useFluxT,
                                                           const bool evaluateCurlField,
                                                           const bool evaluateTemperature):
        materialLaw(num, true),
        _alpha(alpha), _beta(beta), _gamma(gamma),
        _lx(lx),_ly(ly),_lz(lz),_v0(v0),_mu_x(mu_x), _mu_y(mu_y), _mu_z(mu_z),
        _epsilon_4(epsilon_4),_epsilon_5(epsilon_5),_epsilon_6(epsilon_6),
        _mu_7(mu_7),_mu_8(mu_8),_mu_9(mu_9),
        _A0_x(A0_x), _A0_y(A0_y), _A0_z(A0_z),_freqEM(freq),_seebeck(seebeck),_useFluxT(useFluxT),
        _evaluateCurlField(evaluateCurlField),
        _evaluateTemperature(evaluateTemperature),
        applyReferenceF(false),_useEMStress(false), _EMFieldDependentShearModulus(false),
        _alpha_e(0.0),_m_e(1.0),_g1(0.0), _cVoltage(0.)
{
    _genericThermoMechLaw = NULL;

    STensor3 R;		//3x3 rotation matrix
    double fpi = M_PI/180.;

    const double c1 = std::cos(_alpha*fpi);
    const double s1 = std::sin(_alpha*fpi);

    const double c2 = std::cos(_beta*fpi);
    const double s2 = std::sin(_beta*fpi);

    const double c3 = std::cos(_gamma*fpi);
    const double s3 = std::sin(_gamma*fpi);

    const double s1c2 = s1*c2;
    const double c1c2 = c1*c2;

    R(0,0) = c3*c1 - s1c2*s3;
    R(0,1) = c3*s1 + c1c2*s3;
    R(0,2) = s2*s3;

    R(1,0) = -s3*c1 - s1c2*c3;
    R(1,1) = -s3*s1 + c1c2*c3;
    R(1,2) = s2*c3;

    R(2,0) = s1*s2;
    R(2,1) = -c1*s2;
    R(2,2) = c2;

    STensor3 mu;
    STensor3 l;
    // to be uniform in DG3D j= l' dAdt + l' gradV with l'=-l
    // to be unifom in DG3D e= l' gradV with l'=-l instead of e=-l gradV
    l(0,0)  = -_lx;
    l(1,1)  = -_ly;
    l(2,2)  = -_lz;
    // h = mu^-1 b
    mu(0,0)  = _mu_x;
    mu(1,1)  = _mu_y;
    mu(2,2)  = _mu_z;

    for(unsigned int i=0; i<3; ++i)
    {
        for(unsigned int j=0; j<3; ++j)
        {
            _mu0(i,j) = 0.;
            _l0(i,j)=0.;

            for(unsigned int m=0; m<3; ++m)
                for(unsigned int n=0; n<3; ++n)
                {
                    _l0(i,j)+=R(m,i)*R(n,j)*l(m,n);
                    _mu0(i,j) += R(m,i)*R(n,j)*mu(m,n);
                }
        }
    }

    _nu0 = _mu0.invert();

    this->_js0 = 0.0; // Non-inductor domain: no imposed current density
}

mlawElecMagGenericThermoMech::mlawElecMagGenericThermoMech(const mlawElecMagGenericThermoMech& source):
        materialLaw(source),
        _alpha(source._alpha), _beta(source._beta), _gamma(source._gamma),
        _lx(source._lx), _ly(source._ly),_lz(source._lz),_v0(source._v0),
        _mu_x(source._mu_x), _mu_y(source._mu_y), _mu_z(source._mu_z),
        _epsilon_4(source._epsilon_4),_epsilon_5(source._epsilon_5),_epsilon_6(source._epsilon_6),
        _mu_7(source._mu_7), _mu_8(source._mu_8), _mu_9(source._mu_9),
        _A0_x(source._A0_x), _A0_y(source._A0_y), _A0_z(source._A0_z), _freqEM(source._freqEM),
        _seebeck(source._seebeck),_useFluxT(source._useFluxT),
        _evaluateCurlField(source._evaluateCurlField),
        _evaluateTemperature(source._evaluateTemperature),
        _l0(source._l0), _mu0(source._mu0), _nu0(source._nu0),_js0(source._js0),
        _genericThermoMechLaw(NULL),
        applyReferenceF(source.applyReferenceF),
        _useEMStress(source._useEMStress),_EMFieldDependentShearModulus(source._EMFieldDependentShearModulus),
        _alpha_e(source._alpha_e),_m_e(source._m_e),_g1(source._g1), _cVoltage(source._cVoltage)
{
    if(source._genericThermoMechLaw!=NULL)
    {
        if(_genericThermoMechLaw != NULL)
        {
            delete _genericThermoMechLaw;
            _genericThermoMechLaw = NULL;
        }
        _genericThermoMechLaw=dynamic_cast<mlawCoupledThermoMechanics*>((source._genericThermoMechLaw)->clone());
    }
}

mlawElecMagGenericThermoMech& mlawElecMagGenericThermoMech::operator=(const materialLaw& source)
{
    materialLaw::operator=(source);
    const mlawElecMagGenericThermoMech* src =static_cast<const mlawElecMagGenericThermoMech*>(&source);
    if(src !=NULL)
    {
        if(src->_genericThermoMechLaw!=NULL)
        {
            if(_genericThermoMechLaw != NULL)
            {
                _genericThermoMechLaw->operator=(dynamic_cast<const materialLaw& >(*(src->_genericThermoMechLaw)));
            }
            else
                _genericThermoMechLaw=dynamic_cast<mlawCoupledThermoMechanics*>((src->_genericThermoMechLaw)->clone());
        }
        _alpha = src->_alpha;
        _beta = src->_beta;
        _gamma = src->_gamma;
        _lx = src->_lx;
        _ly = src->_ly;
        _lz = src->_lz;
        _v0 = src->_v0;
        _l0 =src->_l0;
        _mu_x = src->_mu_x;
        _mu_y = src->_mu_y;
        _mu_z = src->_mu_z;
        _epsilon_4 = src->_epsilon_4;
        _epsilon_5 = src->_epsilon_5;
        _epsilon_6 = src->_epsilon_6;
        _mu_7 = src->_mu_7;
        _mu_8 = src->_mu_8;
        _mu_9 = src->_mu_9;
        _A0_x = src->_A0_x;
        _A0_y = src->_A0_y;
        _A0_z = src->_A0_z;
        _mu0 = src->_mu0;
        _nu0 = src->_nu0;
        _js0 = src->_js0;
        _freqEM = src->_freqEM;
        _seebeck=src->_seebeck;
        _useFluxT = src->_useFluxT;
        _evaluateCurlField = src->_evaluateCurlField;
        _evaluateTemperature = src->_evaluateTemperature;
        applyReferenceF = src->applyReferenceF;
        _useEMStress = src->_useEMStress;
        _EMFieldDependentShearModulus = src->_EMFieldDependentShearModulus;
        _alpha_e = src->_alpha_e;
        _m_e = src->_m_e;
        _g1 = src->_g1;
         _cVoltage= src->_cVoltage;
    }
    return *this;
}

void mlawElecMagGenericThermoMech::createIPState(IPStateBase* &ips,bool hasBodyForce, const bool* state_,const MElement *ele,
                                                 const int nbFF_, const IntPt *GP, const int gpt) const
{
    if(ips != NULL) delete ips;
    IPElecMagGenericThermoMech *ipvi=new IPElecMagGenericThermoMech();
    IPElecMagGenericThermoMech *ipv1=new IPElecMagGenericThermoMech();
    IPElecMagGenericThermoMech *ipv2=new IPElecMagGenericThermoMech();

    IPStateBase* ipsthermomeca=NULL;
    getConstRefToThermoMechanicalMaterialLaw().createIPState(ipsthermomeca,hasBodyForce, state_,ele, nbFF_,GP, gpt);
    std::vector<IPVariable*> allIP;
    ipsthermomeca->getAllIPVariable(allIP);

    ipvi->setIpThermoMech(*allIP[0]->clone());
    ipv1->setIpThermoMech(*allIP[1]->clone());
    ipv2->setIpThermoMech(*allIP[2]->clone());

    ips = new IP3State(state_,ipvi,ipv1,ipv2);
    delete ipsthermomeca;
}

void mlawElecMagGenericThermoMech::createIPVariable(IPElecMagGenericThermoMech* &ipv, bool hasBodyForce,
                                                    const MElement *ele, const int nbFF,
                                                    const IntPt *GP, const int gpt) const
{
    if(ipv != NULL)
        delete ipv;
    ipv = new IPElecMagGenericThermoMech();

    IPStateBase* ipsthermomech=NULL;
    const bool state_ = true;
    getConstRefToThermoMechanicalMaterialLaw().createIPState(ipsthermomech,hasBodyForce, &state_,ele, nbFF,GP, gpt);

    std::vector<IPVariable*> allIP;
    ipsthermomech->getAllIPVariable(allIP);

    ipv->setIpThermoMech(*allIP[0]->clone());
    delete ipsthermomech;
}

void mlawElecMagGenericThermoMech::constitutive(
        const STensor3   &F0,
        const STensor3   &Fn,
        STensor3         &P,
        const STensor3   &P0,
        const IPVariable *q0,
        IPVariable       *q1,
        STensor43        &Tangent,
        const double     T0,
        double           T,
        double           V0,
        double           V,
        const SVector3   &gradT,
        const SVector3   &gradV,
        SVector3         &fluxT,
        SVector3         &fluxje,
        SVector3         &D, // Electric displacement current
        STensor3         &dPdT,
        STensor3         &dPdV,
        STensor33        &dPdE,
        STensor3         &dqdgradT,
        SVector3         &dqdT,
        STensor3         &dqdgradV,
        SVector3         &dqdV,
        STensor33        &dqdF,
        STensor3         &dedgradT,
        SVector3         &dedT,
        STensor3         &dedgradV,
        SVector3         &dedV,
        STensor33        &dedF,
        STensor3         &dDdgradT,
        SVector3         &dDdT,
        STensor3         &dDdgradV,
        SVector3         &dDdV,
        STensor33        &dDdF,
        const bool       stiff,
        double           &wth,  //related to cp*dT/dt
        double           &dwthdt,
        double           &dwthdV,
        STensor3         &dwthdF,
        double           &wV,  //related to dV/dt
        double           &dwVdt,
        double           &dwVdV,
        STensor3         &dwVdF,
        STensor3 &l10,
        STensor3 &l20,
        STensor3 &k10,
        STensor3 &k20,
        STensor3 &dl10dT,
        STensor3 &dl20dT,
        STensor3 &dk10dT,
        STensor3 &dk20dT,
        STensor3 &dl10dv,
        STensor3 &dl20dv,
        STensor3 &dk10dv,
        STensor3 &dk20dv,
        STensor43 &dl10dF,
        STensor43 &dl20dF,
        STensor43 &dk10dF,
        STensor43 &dk20dF,
        double           &mechSource,
        double           &dmechSourcedT,
        double           &dmechSourcedV,
        STensor3         &dmechSourcedF,
        SVector3         &fluxjy,
        SVector3         &djydV,
        STensor3         &djydgradV,
        SVector3         &djydT,
        STensor3         &djydgradT,
        STensor33        &djydF,
        STensor3 & djydA,
        STensor3 & djydB,
        STensor3 &djydgradVdT,
        STensor3 &djydgradVdV,
        STensor43 &djydgradVdF,
        STensor3 &djydgradTdT,
        STensor3 &djydgradTdV,
        STensor43 &djydgradTdF,
        const SVector3 & A0, // magnetic potential at time n
        const SVector3 & An, // magnetic potential at time n+1
        const SVector3 & B, // magneticinduction B = Curl A
        SVector3 & H, // magnetic field H
        SVector3 & js0, // current density applied in inductor
        SVector3 & dBdT,
        STensor3 & dBdGradT,
        SVector3 & dBdV,
        STensor3 & dBdGradV,
        STensor33 & dBdF,
        STensor3 & dBdA, // A magnetic vector potential
        STensor3 & dfluxTdA,
        STensor3 & dfluxjedA,
        STensor3 & dDdA,
        STensor3 & dfluxTdB,
        STensor3 & dfluxjedB,
        STensor3 & dDdB,
        SVector3 &dw_TdA,
        SVector3 & dw_TdB,
        SVector3 & dmechSourcedB,
        SVector3 & sourceVectorField, // Magnetic source vector field (T, grad V, F)
        SVector3 & dsourceVectorFielddt,
        double & ThermalEMFieldSource,
        double & VoltageEMFieldSource,
        STensor3 & dHdA,
        STensor3 & dHdB,
        STensor33 & dPdB,
        STensor33 & dHdF,
        STensor33 & dsourceVectorFielddF,
        SVector3 & mechFieldSource, // Electromagnetic force
        STensor3 & dmechFieldSourcedB,
        STensor33 & dmechFieldSourcedF,
        SVector3 & dHdT,
        STensor3 & dHdgradT,
        SVector3 & dHdV,
        STensor3 & dHdgradV,
        SVector3 & dsourceVectorFielddT,
        STensor3 & dsourceVectorFielddgradT,
        SVector3 & dsourceVectorFielddV,
        STensor3 & dsourceVectorFielddgradV,
        STensor3 & dsourceVectorFielddA,
        STensor3 & dsourceVectorFielddB,
        SVector3 & dmechFieldSourcedT,
        STensor3 & dmechFieldSourcedgradT,
        SVector3 & dmechFieldSourcedV,
        STensor3 & dmechFieldSourcedgradV,
        SVector3 & dThermalEMFieldSourcedA,
        SVector3 & dThermalEMFieldSourcedB,
        STensor3 & dThermalEMFieldSourcedF,
        double & dThermalEMFieldSourcedT,
        SVector3 & dThermalEMFieldSourcedGradT,
        double & dThermalEMFieldSourcedV,
        SVector3 & dThermalEMFieldSourcedGradV,
        SVector3 & dVoltageEMFieldSourcedA,
        SVector3 & dVoltageEMFieldSourcedB,
        STensor3 & dVoltageEMFieldSourcedF,
        double & dVoltageEMFieldSourcedT,
        SVector3 & dVoltageEMFieldSourcedGradT,
        double & dVoltageEMFieldSourcedV,
        SVector3 & dVoltageEMFieldSourcedGradV,
        STensor43 *elasticTangent
) const
{
    if(_genericThermoMechLaw == NULL)
        Msg::Error("Thermo-Mech law is null");

    const IPElecMagGenericThermoMech &qEM0= dynamic_cast<const IPElecMagGenericThermoMech &> (*q0);
    IPElecMagGenericThermoMech &qEM1= dynamic_cast<IPElecMagGenericThermoMech &> (*q1);

    const IPCoupledThermoMechanics & qTM0 = qEM0.getConstRefToIpThermoMech();
    IPCoupledThermoMechanics & qTM1 = qEM1.getRefToIpThermoMech();

    // thermo-mechanical stress and tangents
    STensor3 P_TM, dP_TMdT;
    STensor43 dP_TMdF;

    STensorOperation::zero(P_TM);
    STensorOperation::zero(dP_TMdT);
    STensorOperation::zero(dP_TMdF);

    if(_evaluateTemperature)
    _genericThermoMechLaw->constitutive(F0,Fn,P_TM,P0,&qEM0.getConstRefToIpThermoMech(),
                                        &qEM1.getRefToIpThermoMech(),dP_TMdF,T0,T,gradT,
                                        fluxT,dP_TMdT,dqdgradT,dqdT,dqdF,stiff,
                                        wth,dwthdt,dwthdF,mechSource,dmechSourcedT,
                                        dmechSourcedF,elasticTangent);
     if(STensorOperation::isnan(P_TM))
     {
       Msg::Info("mlawGenericTM::constitutive did not converge");
       P(0,0) = P(1,1) = P(2,2) = sqrt(-1.); // to exist NR and performed next step with a smaller incremene
       return;
     }


    P = P_TM;
    if(stiff)
    {
        Tangent = dP_TMdF;
        dPdT = dP_TMdT;
    }

    wV=0.;  //related to dV/dt
    dwVdt=0.;
    dwVdV=0.;
    STensorOperation::zero(dwVdF);




    // if true then we solve EM problem and thus reevaluate Voltage field as well
    // else do not consider Voltage contributions -> TM problem
    if(_evaluateCurlField)
    {
        double dseebeckdT;
        static STensor3 dldT,dkdT,dnudT,dmudT,depsilondT;//,k1,k2,l10,l20;

        static STensor33 d_nu0dB;
        STensorOperation::zero(d_nu0dB);

        STensorOperation::zero(dldT);
        STensorOperation::zero(dkdT);
        STensorOperation::zero(dnudT);
        STensorOperation::zero(dmudT);
        STensorOperation::zero(dseebeckdT);
        STensorOperation::zero(depsilondT);

        STensorOperation::zero(fluxje);
        STensorOperation::zero(fluxjy);
        STensorOperation::zero(D);
        STensorOperation::zero(dedT);
        STensorOperation::zero(dedV);
        STensorOperation::zero(dedgradT);
        STensorOperation::zero(dedgradV);
        STensorOperation::zero(dDdT);
        STensorOperation::zero(dDdV);
        STensorOperation::zero(dDdgradT);
        STensorOperation::zero(dDdgradV);
        STensorOperation::zero(dqdV);
        STensorOperation::zero(dqdgradV);
        STensorOperation::zero(dedF);
        STensorOperation::zero(dDdF);
        STensorOperation::zero(dPdV);
        STensorOperation::zero(dPdE);

        STensorOperation::zero(djydT);
        STensorOperation::zero(djydV);
        STensorOperation::zero(djydgradT);
        STensorOperation::zero(djydgradV);
        STensorOperation::zero(djydgradTdV);
        STensorOperation::zero(djydgradTdT);
        STensorOperation::zero(djydgradVdV);
        STensorOperation::zero(djydgradVdT);

        STensorOperation::zero(l10);
        STensorOperation::zero(l20);
        STensorOperation::zero(k10);
        STensorOperation::zero(k20);
        STensorOperation::zero(dl10dT);
        STensorOperation::zero(dl20dT);
        STensorOperation::zero(dk10dT);
        STensorOperation::zero(dk20dT);
        STensorOperation::zero(dl10dv);
        STensorOperation::zero(dl20dv);
        STensorOperation::zero(dk10dv);
        STensorOperation::zero(dk20dv);
        STensorOperation::zero(dl10dF);
        STensorOperation::zero(dl20dF);
        STensorOperation::zero(dk10dF);
        STensorOperation::zero(dk20dF);

        STensorOperation::zero(H);
        STensorOperation::zero(dBdT);
        STensorOperation::zero(dBdGradT);
        STensorOperation::zero(dBdV);
        STensorOperation::zero(dBdGradV);
        STensorOperation::zero(dBdF);
        STensorOperation::zero(dBdA);
        STensorOperation::zero(dfluxTdA);
        STensorOperation::zero(dfluxjedA);
        STensorOperation::zero(dDdA);
        STensorOperation::zero(dfluxTdB);
        STensorOperation::zero(dfluxjedB);
        STensorOperation::zero(dDdB);
        STensorOperation::zero(dw_TdA);
        STensorOperation::zero(dw_TdB);
        STensorOperation::zero(dHdA);
        STensorOperation::zero(dHdB);
        STensorOperation::zero(dPdB);
        STensorOperation::zero(dHdF);
        STensorOperation::zero(dHdT);
        STensorOperation::zero(dHdgradT);
        STensorOperation::zero(dHdV);
        STensorOperation::zero(dHdgradV);
        STensorOperation::zero(sourceVectorField);
        STensorOperation::zero(dsourceVectorFielddT);
        STensorOperation::zero(dsourceVectorFielddgradT);
        STensorOperation::zero(dsourceVectorFielddV);
        STensorOperation::zero(dsourceVectorFielddgradV);
        STensorOperation::zero(dsourceVectorFielddA);
        STensorOperation::zero(dsourceVectorFielddB);
        STensorOperation::zero(dsourceVectorFielddt);
        STensorOperation::zero(dsourceVectorFielddF);
        STensorOperation::zero(djydA);
        STensorOperation::zero(djydB);
        ThermalEMFieldSource = 0.0;
        VoltageEMFieldSource = 0.0;
        STensorOperation::zero(dThermalEMFieldSourcedA);
        STensorOperation::zero(dThermalEMFieldSourcedB);
        STensorOperation::zero(dThermalEMFieldSourcedF);
        STensorOperation::zero(dThermalEMFieldSourcedT);
        STensorOperation::zero(dThermalEMFieldSourcedGradT);
        STensorOperation::zero(dThermalEMFieldSourcedV);
        STensorOperation::zero(dThermalEMFieldSourcedGradV);
        STensorOperation::zero(dVoltageEMFieldSourcedA);
        STensorOperation::zero(dVoltageEMFieldSourcedB);
        STensorOperation::zero(dVoltageEMFieldSourcedF);
        STensorOperation::zero(dVoltageEMFieldSourcedT);
        STensorOperation::zero(dVoltageEMFieldSourcedGradT);
        STensorOperation::zero(dVoltageEMFieldSourcedV);
        STensorOperation::zero(dVoltageEMFieldSourcedGradV);

        const STensor3 k_ = _genericThermoMechLaw->getConductivityTensor(); // Need to think for this
        const double _G = _genericThermoMechLaw->ElasticShearModulus();
        // to take in reference or current ?
        // for fluxT & fluxjy update in EM

        // Also need to think on transforming EM material params
        // mut, nut, lt ???
        // Also dltdF, dnutdF, dmutdF

        // Refer to mlawElecSMP, mlawElecMagTherMech for transformations
        static STensor3 lt, nut, mut, kt, epsilont;
        static STensor43 dltdF, dnutdF, dmutdF, dktdF, depsilontdF; // Not computed and used (instead used dXdFTot)

        STensorOperation::zero(lt);
        STensorOperation::zero(nut);
        STensorOperation::zero(mut);
        STensorOperation::zero(kt);
        STensorOperation::zero(epsilont);

        STensorOperation::zero(dltdF);
        STensorOperation::zero(dnutdF);
        STensorOperation::zero(dmutdF);
        STensorOperation::zero(dktdF);
        STensorOperation::zero(depsilontdF);

        static STensor3 FRef,FRefInv,FRefTranspose,FRefInvTranspose;
        static STensor43 dFRefdFn;
        STensorOperation::zero(FRef);
        STensorOperation::zero(FRefInv);
        STensorOperation::zero(FRefTranspose);
        STensorOperation::zero(FRefInvTranspose);
        STensorOperation::zero(dFRefdFn);
        static double JacRef;
        STensorOperation::zero(JacRef);

        const STensor3 I2(1.0);
        const STensor43 I4(2.0,0.0);
        static const double vacuum_permeability = 4.0 * M_PI * 1.e-7;
        static const double vacuum_permittivity = 8.854 * 1.e-12;
        static const double rho = this->_genericThermoMechLaw->density();

        // strong coupling: Ftot = Fn Fref (with Fref = I)
        // weak coupling: Ftot = Fn Fref (with FRef = F_TM, Fn = I)
        if(applyReferenceF)
        {
            //FRef =Fn; // here we have Fn identity in any case but I would leave it to be correct
            FRef = qTM1.getRefToReferenceF();
        }
        else
        {
            FRef = I2;
        }
        JacRef = FRef.determinant();
        STensorOperation::inverseSTensor3(FRef, FRefInv);
        STensorOperation::transposeSTensor3(FRef,FRefTranspose);
        STensorOperation::transposeSTensor3(FRefInv,FRefInvTranspose);
        dFRefdFn = I4;

        STensor3 Ftot(Fn);
        Ftot*=FRef;

        const double Jactot = Ftot.determinant();
        static STensor43 dFTotdFn;
        STensorOperation::zero(dFTotdFn);
        static STensor3 FtotInv, FtotTranspose, FtotInvTranspose;
        STensorOperation::zero(FtotInv);
        STensorOperation::zero(FtotTranspose);
        STensorOperation::zero(FtotInvTranspose);
        STensorOperation::inverseSTensor3(Ftot,FtotInv);
        STensorOperation::transposeSTensor3(Ftot,FtotTranspose);
        STensorOperation::transposeSTensor3(FtotInv,FtotInvTranspose);

        static STensor3 Ctot, Ctot_squared;
        static STensor3 CtotInv, CtotInv_squared;
        STensorOperation::zero(Ctot);
        STensorOperation::zero(Ctot_squared);
        STensorOperation::zero(CtotInv);
        STensorOperation::zero(CtotInv_squared);
        STensorOperation::multSTensor3(FtotTranspose,Ftot,Ctot);
        STensorOperation::multSTensor3(Ctot,Ctot,Ctot_squared);
        STensorOperation::inverseSTensor3(Ctot,CtotInv);
        STensorOperation::multSTensor3(CtotInv,CtotInv,CtotInv_squared);

        static STensor3 FnInv, FnTranspose, FnInvTranspose;
        STensorOperation::zero(FnInv);
        STensorOperation::zero(FnTranspose);
        STensorOperation::zero(FnInvTranspose);
        STensorOperation::inverseSTensor3(Fn,FnInv);
        STensorOperation::transposeSTensor3(Fn,FnTranspose);
        STensorOperation::transposeSTensor3(FnInv,FnInvTranspose);

        for (int i=0; i<3; i++){
            for (int j=0; j<3; j++){
                for (int k=0; k<3; k++){
                    for (int m=0; m<3; m++){
                        for (int n=0; n<3; n++){
                            dFTotdFn(i,k,m,n) += I2(i,m) * I2(j,n) * FRef(j,k);
                        }
                    }
                }
            }
        }

        //from HERE NO MORE applyReferenceFq

        // Magnetic permeability tensor and electric permittivity tensor from constitutive model
        // I_4 = E \otimes E : I, I_5 = E \otimes E : C^{-1}, I_6 = E \otimes E : C^{-2}
        // I_7 = B \otimes B : I, I_8 = B \otimes B : C, I_9 = B \otimes B : C^2
        // W_elec + W_elec-mech = 0.5 * epsilon_4 I_4 + 0.5 * epsilon_5 I_5 + 0.5 * epsilon_6 I_6
        // W_mag + W_mag-mech = 0.5 * mu_7^{-1} I_7 + 0.5 * mu_8^{-1} I_8 + 0.5 * mu_9^{-1} I_9
        for(int K = 0; K< 3; K++) {
            for (int L = 0; L < 3; L++) {
                for (int i = 0; i < 3; i++) {
                    for (int j = 0; j < 3; j++) {
                        for(int X = 0; X< 3; X++) {
                            for (int Y = 0; Y < 3; Y++) {
                                lt(K,L) += FRef(K,X) * FtotInv(X,i) * _l0(i,j) * FtotInv(Y,j) * FRef(L,Y);
                                kt(K,L) += FRef(K,X) * FtotInv(X,i) * k_(i,j) * FtotInv(Y,j) * FRef(L,Y);
                            }
                        }
                        nut(K,L) += FRefInv(i,K) * Ctot(i,j) * (1.0/vacuum_permeability) * FRefInv(j,L);
                        if(_mu_7 != 0.0)
                            nut(K,L) += FRefInv(i,K) * Jactot * rho * I2(i,j) * (1.0/_mu_7) * FRefInv(j,L);
                        if(_mu_8 != 0.0)
                            nut(K,L) += FRefInv(i,K) * Jactot * rho * Ctot(i,j) * (1.0/_mu_8) * FRefInv(j,L);
                        if(_mu_9 != 0.0)
                            nut(K,L) += FRefInv(i,K) * Jactot * rho * Ctot_squared(i,j) * (1.0/_mu_9) * FRefInv(j,L);

                        epsilont(K,L) += FRef(K,i) * CtotInv(i,j) * vacuum_permittivity * FRef(L, j);
                        if(_epsilon_4 != 0.0)
                            epsilont(K,L) -= FRef(K, i) * Jactot * rho * _epsilon_4 * I2(i,j) * FRef(L, j);
                        if(_epsilon_5 != 0.0)
                            epsilont(K,L) -= FRef(K, i) * Jactot * rho * _epsilon_5 * CtotInv(i,j) * FRef(L, j);
                        if(_epsilon_6 != 0.0)
                            epsilont(K,L) -= FRef(K, i) * Jactot * rho * _epsilon_6 * CtotInv_squared(i,j) * FRef(L, j);
                    }
                }
            }
        }

        lt *= (Jactot/JacRef);
        kt *= (Jactot/JacRef);
        nut *= (JacRef/Jactot);
        epsilont *= (Jactot/JacRef);
        STensorOperation::inverseSTensor3(nut,mut);

        static STensor43 dFTotdFTot,dFTotInvdFTot,dFTotInvTransposedFTot;
        static STensor43 dCTotdFTot, dCTot_squareddFTot;
        static STensor43 dCTotInvdFTot, dCTotInv_squareddFTot;
        STensorOperation::zero(dFTotdFTot);
        STensorOperation::zero(dFTotInvdFTot);
        STensorOperation::zero(dFTotInvTransposedFTot);
        STensorOperation::zero(dCTotdFTot);
        STensorOperation::zero(dCTot_squareddFTot);
        STensorOperation::zero(dCTotInvdFTot);
        STensorOperation::zero(dCTotInv_squareddFTot);

        for (int i=0; i<3; i++){
            for (int j=0; j<3; j++){
                for (int k=0; k<3; k++){
                    for(int l=0; l<3; ++l){
                        dFTotInvdFTot(i,j,k,l) -= FtotInv(i,k) * FtotInv(l,j);
                        dFTotInvTransposedFTot(i,j,k,l) -= FtotInv(i,l) * FtotInv(k,j);
                        dCTotdFTot(i,j,k,l) += I2(i,l) * Ftot(k,j);
                        dCTotdFTot(i,j,k,l) += I2(j,l) * Ftot(k,i);
                        dCTotInvdFTot(i,j,k,l) -= FtotInv(i,k) * CtotInv(l,j);
                        dCTotInvdFTot(i,j,k,l) -= CtotInv(i,l) * FtotInv(j,k);
                        dCTotInv_squareddFTot(i,j,k,l) -= FtotInv(i,k) * CtotInv_squared(l,j);
                        dCTotInv_squareddFTot(i,j,k,l) -= CtotInv_squared(i,l) * FtotInv(j,k);
                        for(int r=0; r<3; ++r){
                            dCTot_squareddFTot(i,j,k,l) += I2(i,l) * Ftot(k,r) * Ctot(r,j);
                            dCTot_squareddFTot(i,j,k,l) += I2(r,l) * Ftot(k,i) * Ctot(r,j);
                            dCTot_squareddFTot(i,j,k,l) += I2(r,l) * Ftot(k,j) * Ctot(i,r);
                            dCTot_squareddFTot(i,j,k,l) += I2(j,l) * Ftot(k,r) * Ctot(i,r);
                            dCTotInv_squareddFTot(i,j,k,l) -= CtotInv(i,l) * FtotInv(r,k) * CtotInv(r,j);
                            dCTotInv_squareddFTot(i,j,k,l) -= CtotInv(i,r) * FtotInv(r,k) * CtotInv(l,j);
                        }
                    }
                }
            }
        }

        static STensor43 dltdFTot, dktdFTot, dnutdFTot, depsilontdFTot;
        STensorOperation::zero(dltdFTot);
        STensorOperation::zero(dktdFTot);
        STensorOperation::zero(dnutdFTot);
        STensorOperation::zero(depsilontdFTot);

        for(int K = 0; K< 3; K++) {
            for (int L = 0; L < 3; L++) {
                for (int i = 0; i < 3; i++) {
                    for (int j = 0; j < 3; j++) {
                        dltdFTot(K,L,i,j) += lt(K,L) * FtotInv(j,i);
                        dktdFTot(K,L,i,j) += kt(K,L) * FtotInv(j,i);
                        dnutdFTot(K,L,i,j) -= nut(K,L) * FtotInv(j,i);
                        depsilontdFTot(K,L,i,j) += epsilont(K,L) * FtotInv(j,i);
                        for(int X = 0; X< 3; X++) {
                            for (int Y = 0; Y < 3; Y++) {
                                for(int p = 0; p < 3; ++p){
                                    for(int Q = 0; Q < 3; ++Q){
                                    dltdFTot(K,L,p,Q) -= (Jactot/JacRef) * FRef(K,X) * FtotInv(X,p) * FtotInv(Q,i) * _l0(i,j) * FtotInv(Y,j) * FRef(L,Y);
                                    dltdFTot(K,L,p,Q) -= (Jactot/JacRef) * FRef(K,X) * FtotInv(X,i) * _l0(i,j) * FtotInv(Y,p) * FtotInv(Q,j) * FRef(L,Y);

                                    dktdFTot(K,L,p,Q) -= (Jactot/JacRef) * FRef(K,X) * FtotInv(X,p) * FtotInv(Q,i) * k_(i,j) * FtotInv(Y,j) * FRef(L,Y);
                                    dktdFTot(K,L,p,Q) -= (Jactot/JacRef) * FRef(K,X) * FtotInv(X,i) * k_(i,j) * FtotInv(Y,p) * FtotInv(Q,j) * FRef(L,Y);
                                    }
                                }

                                dnutdFTot(K,L,X,Y) += (JacRef/Jactot) * FRefInv(i,K) * (1.0/vacuum_permeability) * dCTotdFTot(i,j,X,Y) * FRefInv(j,L);
                                if(_mu_7 != 0.0)
                                    dnutdFTot(K,L,X,Y) += (JacRef/Jactot) * FRefInv(i,K) * Jactot * rho * I2(i,j) * FtotInv(Y,X) * (1.0/_mu_7) * FRefInv(j,L);

                                if(_mu_8 != 0.0) {
                                    dnutdFTot(K, L, X, Y) += (JacRef / Jactot) * FRefInv(i, K) * Jactot * rho * Ctot(i, j) * FtotInv(Y, X) * (1.0 / _mu_8) * FRefInv(j, L);
                                    dnutdFTot(K, L, X, Y) += (JacRef / Jactot) * FRefInv(i, K) * Jactot * rho * dCTotdFTot(i, j, X, Y) * (1.0 / _mu_8) * FRefInv(j, L);
                                }
                                if(_mu_9 != 0.0) {
                                    dnutdFTot(K,L,X,Y) += (JacRef/Jactot) * FRefInv(i,K) * Jactot * rho * Ctot_squared(i,j) * FtotInv(Y,X) * (1.0/_mu_9) * FRefInv(j,L);
                                    dnutdFTot(K,L,X,Y) += (JacRef/Jactot) * FRefInv(i,K) * Jactot * rho * dCTot_squareddFTot(i,j,X,Y) * (1.0/_mu_9) * FRefInv(j,L);
                                }

                                depsilontdFTot(K,L,X,Y) -= (Jactot/JacRef) * FRef(K,i) * dCTotInvdFTot(i,j,X,Y) * vacuum_permittivity * FRef(L,j);
                                if(_epsilon_4 != 0.0)
                                    depsilontdFTot(K,L,X,Y) -= 2.0 * FRef(K,i) * (Jactot*Jactot/JacRef) * rho * _epsilon_4 * I2(i,j) * FRef(L,j) * FtotInv(Y,X);
                                if(_epsilon_5 != 0.0)
                                {
                                    depsilontdFTot(K,L,X,Y) -= 2.0 * FRef(K,i) * (Jactot*Jactot/JacRef) * rho * _epsilon_5 * CtotInv(i,j) * FRef(L,j) * FtotInv(Y,X);
                                    depsilontdFTot(K,L,X,Y) -= FRef(K,i) * (Jactot*Jactot/JacRef) * rho * _epsilon_5 * dCTotInvdFTot(i,j,X,Y) * FRef(L,j);
                                }
                                if(_epsilon_6 != 0.0)
                                {
                                    depsilontdFTot(K,L,X,Y) -= 2.0 * FRef(K,i) * (Jactot*Jactot/JacRef) * rho * _epsilon_6 * CtotInv_squared(i,j) * FRef(L,j) * FtotInv(Y,X);
                                    depsilontdFTot(K,L,X,Y) -= FRef(K,i) * (Jactot*Jactot/JacRef) * rho * _epsilon_6 * dCTotInv_squareddFTot(i,j,X,Y) * FRef(L,j);
                                }
                            }
                        }
                    }
                }
            }
        }

        // for DG only
        for(int i = 0; i< 3; i++)
        {
            for(int j = 0; j< 3; j++)
            {
                l10(i,j) = -_l0(i,j)*T;
                l20(i,j) = -_l0(i,j)*(V*T+_seebeck*T*T);

                k20(i,j) = l20(i,j);
                k10(i,j) = -k_(i,j)*T*T-2.*_seebeck*_l0(i,j)*T*T*V-pow(_seebeck,2)*_l0(i,j)*pow(T,3)-_l0(i,j)*T*V*V;
                //jy1(i,j) = -_k(i,j)*T*T-2.*_seebeck*_l0(i,j)*T*T*V-pow(_seebeck,2)*_l0(i,j)*pow(T,3)-_l0(i,j)*T*V*V;
            }
        }

        const double ctime = getTime();
        const double dh = getTimeStep();
        double invdh;
        if (dh < 0.0)
            Msg::Error("Negative time step size in mlawElecMagGenericThermoMech::constitutive()");
        else if (dh == 0.0)
            invdh = 0.0;
        else
            invdh = 1.0/dh;

        static SVector3 E_field; // Electric field in reference config
        STensorOperation::zero(E_field);

        // E = - gradV - dAdt
        for(unsigned int i =0; i <3; ++i)
            E_field(i) = -gradV(i) - (An(i) - A0(i)) * invdh;

        double invariant4 = 0.0; // (E \otimes E : I2)
        double invariant5 = 0.0; // (E \otimes E : C^-1), C:right Cauchy Green deformation tensor
        double invariant6 = 0.0; // (E \otimes E : C^-2)
        double invariant7 = 0.0; // (B \otimes B : I2)
        double invariant8 = 0.0; // (B \otimes B : C), C:right Cauchy Green deformation tensor
        double invariant9 = 0.0; // (B \otimes B : C^2)

        for(unsigned int K = 0; K < 3; K++)
        {
            invariant4 += E_field(K) * E_field(K);
            invariant7 += B(K) * B(K);
            for(unsigned int L = 0; L < 3; L++)
            {
                invariant8 += B(K) * B(L) * Ctot(K,L);
                invariant9 += B(K) * B(L) * Ctot_squared(K,L);
                invariant5 += E_field(K) * E_field(L) * CtotInv(K,L);
                invariant6 += E_field(K) * E_field(L) * CtotInv_squared(K,L);
            }
        }

        // for DG only
        if(stiff)
        {
            for(int i = 0; i< 3; i++)
            {
                for(int j = 0; j< 3; j++)
                {
                    dl10dT(i,j) = -_l0(i,j);
                    dl20dT(i,j) = -_l0(i,j)*(V+2*_seebeck*T);
                    //dk10dT(i,j) = -2*_k(i,j)*T- 2*_seebeck*_l0(i,j)*T*V-3*pow(_seebeck,2)*_l0(i,j)*pow(T,2);
                    //dK20dT(i,j) = -2*_seebeck*T*_l0(i,j);
                    dk20dT(i,j) = dl20dT(i,j);
                    dl20dv(i,j)= -_l0(i,j)*(T);
                    dk20dv(i,j) = dl20dT(i,j);
                    //dk10dv(i,j)= -_seebeck*_l0(i,j)*T*T;
                    // djy1dT(i,j) = -2.*_k(i,j)*T- 4.*_seebeck*_l0(i,j)*T*V-3*pow(_seebeck,2)*_l0(i,j)*pow(T,2)-_l0(i,j)*V*V;
                    // djy1dV(i,j) = -2.*_seebeck*_l0(i,j)*T*T-2.*_l0(i,j)*T*V;
                    dk10dT(i,j) = -2.*k_(i,j)*T- 4.*_seebeck*_l0(i,j)*T*V-3*pow(_seebeck,2)*_l0(i,j)*pow(T,2)-_l0(i,j)*V*V;
                    dk10dv(i,j) = -2.*_seebeck*_l0(i,j)*T*T-2.*_l0(i,j)*T*V;
                }
            }
        }

        // Electric contributions
        for(int i=0;i<3;i++)
        {
            for(int j=0;j<3;j++)
            {
                fluxje(i) += lt(i, j) * gradV(j)
                            + lt(i, j) * _seebeck * gradT(j);
                fluxT(i) += (lt(i,j)*_seebeck*_seebeck*T)*gradT(j)
                            +lt(i,j)*_seebeck*T*gradV(j);
                fluxjy(i) += (kt(i,j)+lt(i,j)*_seebeck*_seebeck*T+lt(i,j)*_seebeck*V)*gradT(j)
                             +(lt(i,j)*V+lt(i,j)*_seebeck*T)*gradV(j);
            }
        }

        if(stiff)
        {
            for(int i=0;i<3;i++)
            {
                for(int j=0;j<3;j++)
                {
                    dedT(i)+=dseebeckdT*lt(i,j)*gradT(j)
                             +_seebeck*dldT(i,j)*gradT(j)
                             +dldT(i,j)*gradV(j);
                    djydT(i)+=_seebeck*_seebeck*lt(i,j)*gradT(j)
                              +_seebeck*lt(i,j)*gradV(j);
                    djydV(i)+=_seebeck*lt(i,j)*gradT(j)
                              +lt(i,j)*gradV(j);
                    dqdT(i) +=2*_seebeck*dseebeckdT*lt(i,j)*T*gradT(j)
                              +_seebeck*_seebeck*dldT(i,j)*T*gradT(j)
                              +_seebeck*_seebeck*lt(i,j)*gradT(j)
                              +dseebeckdT*lt(i,j)*T*gradV(j)
                              +_seebeck*dldT(i,j)*T*gradV(j)
                              +_seebeck*lt(i,j)*gradV(j);

                    dedgradT(i,j) +=_seebeck*lt(i,j);
                    dedgradV(i,j) +=lt(i,j);
                    dqdgradV(i,j)+=lt(i,j)*_seebeck*T;
                    dqdgradT(i,j) += lt(i,j)*_seebeck*_seebeck*T;

                    djydgradT(i,j)+=kt(i,j)
                                    +lt(i,j)*_seebeck*_seebeck*T
                                    +lt(i,j)*_seebeck*V;
                    djydgradV(i,j) +=lt(i,j)*_seebeck*T
                                    +lt(i,j)*V;
                    djydgradVdT(i,j) +=lt(i,j)*_seebeck;
                    djydgradVdV(i,j) +=lt(i,j);
                    djydgradTdT(i,j)+=lt(i,j)*_seebeck*_seebeck;
                    djydgradTdV(i,j)+=lt(i,j)*_seebeck;

                    dHdT(i) += dnudT(i,j) * B(j);

                    for (unsigned int k = 0; k < 3; ++k)
                        dHdB(i,j) += (d_nu0dB(i,j,k) * B(k));

                    dHdB(i,j) += nut(i,j);
                }
            }
        }

        // Electric & Magnetic contributions

        const double psiMeca = qEM1.TMDefoEnergy(); // ipGenTM -> ipMeca::defoEnergy
        double & psiEMMeca = qEM1.getRefToDefoEnergy();

        static SVector3 Magnetization, Polarization;
        STensorOperation::zero(Magnetization);
        STensorOperation::zero(Polarization);

        // in case of shear modulus a tanh function
        // as in Mehnert et al. 2017
        // Gnew = G (1 + _alpha_e tanh(I7/_m_e)) for magneto-mechanics
        // Gnew = G (1 + _g1 I4/G) for elec-mechanics
        if(_EMFieldDependentShearModulus)
        {
            psiEMMeca = (1.0 + _alpha_e * std::tanh(invariant7/_m_e)) * (1.0 + (_g1 * invariant4 / _G)) * psiMeca;
            for(unsigned int K = 0; K < 3; ++K)
            {
                Magnetization(K) = (-2.0 * _alpha_e * psiMeca * (1.0 - (std::tanh(invariant7/_m_e) * std::tanh(invariant7/_m_e))) * (1.0 + (_g1 * invariant4 / _G)) * B(K))/(_m_e);
                Polarization(K) = (-2.0 * _g1 * psiMeca * (1.0 + _alpha_e * std::tanh(invariant7/_m_e)) * E_field(K)/_G);
            }
        }
        else
        {
            psiEMMeca = psiMeca;
        }

        if(_epsilon_4 != 0.0)
            psiEMMeca += (_epsilon_4 * invariant4)/ 2.0;
        if(_epsilon_5 != 0.0)
            psiEMMeca += (_epsilon_5 * invariant5)/ 2.0;
        if(_epsilon_6 != 0.0)
            psiEMMeca += (_epsilon_6 * invariant6)/ 2.0;
        if(_mu_7 != 0.0)
            psiEMMeca += invariant7/(2.0 * _mu_7);
        if(_mu_8 != 0.0)
            psiEMMeca += invariant8/(2.0 * _mu_8);
        if(_mu_9 != 0.0)
            psiEMMeca += invariant9/(2.0 * _mu_9);

        // Linear magnetic constitutive law H = (1/mu) * B
        for (unsigned int i = 0; i < 3; ++i)
        {
            H(i) = 0.0;
            D(i) = 0.0;
            for (unsigned int j = 0; j < 3; ++j)
            {
                H(i) += nut(i,j) * B(j);
                D(i) += epsilont(i,j) * E_field(j);
                if(_EMFieldDependentShearModulus)
                {
                    H(i) -= Magnetization(i);
                    D(i) += Polarization(i);
                }
            }
        }

        // phase angle for sin wave function
        // phi = M_PI / 2.0 if static analysis in EM
        // phi = 0.0 if time domain dynamic analysis
        const double phi = 0.0;
        const double js0_sin_wt_phi = this->_js0 * std::sin(2.0 * M_PI * _freqEM * ctime + phi);

        static SVector3 normalDir(0.0);
        this->getNormalDirection(normalDir, &qEM1);

        for(unsigned int i = 0; i < 3; ++i)
        {
            js0(i) = js0_sin_wt_phi * normalDir(i);
        }

        // Magnetic contributions
        for(int i=0;i<3;i++)
        {
            if (!isInductor())
            {
                for(int j=0;j<3;j++)
                {
                    fluxje(i)+= (lt(i, j) * (An(j) - A0(j)) * invdh);
                    fluxjy(i)+= ((lt(i, j) * V + lt(i, j) * _seebeck * T) * (An(j) - A0(j)) * invdh);
                    fluxT(i) += ((lt(i, j) * _seebeck * T) * (An(j) - A0(j)) * invdh);

                    if (stiff)
                    {
                        // derivatives of j_e
                        dedT(i) += dldT(i, j) * (An(j) - A0(j)) * invdh;
                        dfluxjedA(i, j) += lt(i, j) * invdh;

                        // derivatives of q or fluxT
                        dqdT(i) += dldT(i, j) * _seebeck * T * (An(j) - A0(j)) * invdh
                                + dseebeckdT * lt(i, j) * T * (An(j) - A0(j)) * invdh
                                + lt(i, j) * _seebeck * (An(j) - A0(j)) * invdh;
                        dfluxTdA(i, j) += lt(i, j) * _seebeck * T * invdh;

                        // derivatives of fluxjy
                        djydT(i) += (_seebeck * lt(i, j) + dseebeckdT * T * lt(i, j)
                                     + _seebeck * T * dldT(i,j) + V * dldT(i,j)) * (An(j) - A0(j)) * invdh;
                        djydV(i) += lt(i, j) * (An(j) - A0(j)) * invdh;
                        djydA(i, j) += (lt(i, j) * V + lt(i, j) * _seebeck * T) * invdh;

                        // derivatives of D
                        dDdgradV(i, j) -= epsilont(i,j);
                        dDdA(i, j) -= epsilont(i,j)*invdh;

                        // derivatives of sourceVectorField
                        dsourceVectorFielddgradV(i, j) += (-lt(i, j));
                        dsourceVectorFielddgradT(i, j) += (-_seebeck * lt(i ,j));

                        dsourceVectorFielddT(i) += (-dseebeckdT * lt(i, j) * gradT(j)
                                                    - _seebeck * dldT(i,j) * gradT(j)
                                                    - dldT(i,j) * (An(j) - A0(j)) * invdh
                                                    - dldT(i,j) * gradV(j));
                        dsourceVectorFielddA(i, j) += (-lt(i, j) * invdh);

                        dThermalEMFieldSourcedA(i) += (-dfluxjedA(i,j) * ((An(j) - A0(j)) * invdh));
                        dThermalEMFieldSourcedB(i) += /*(dHdB(i,j) * (Bn(j) - B0(j)) * invdh)*/ 0.0;
                        dThermalEMFieldSourcedB(i) += /*(H(i) * invdh)*/ 0.0;
                        dThermalEMFieldSourcedGradT(i) +=  (-dedgradT(i,j) * (An(j) - A0(j)) * invdh);
                        dThermalEMFieldSourcedGradV(i) += (-dedgradV(i,j) * (An(j) - A0(j)) * invdh);

                        if (_useFluxT)
                        {
                            dThermalEMFieldSourcedGradT(i) += (- dedgradT(i,j) * gradV(j));
                            dThermalEMFieldSourcedGradV(i) += (- dedgradV(i,j) * gradV(j));
                            dThermalEMFieldSourcedA(i) += (-dfluxjedA(i,j) * gradV(j));
                        }
                    }
                }

                if(stiff)
                {
                    dThermalEMFieldSourcedA(i) += (-fluxje(i) * invdh);
                    dThermalEMFieldSourcedB(i) += /*(H(i) * invdh)*/ 0.0;
                    dThermalEMFieldSourcedB(i) += /*(dHdB(i,j) * (Bn(j) - B0(j))* invdh)*/ 0.0;

                    if (_useFluxT)
                        dThermalEMFieldSourcedGradV(i) += ( - fluxje(i));

                    dThermalEMFieldSourcedT += (-dedT(i) * (An(i) - A0(i)) * invdh /*+ dnudT(i,j) * B(j) * (Bn(i) - B0(i)) * invdh*/);
                    dThermalEMFieldSourcedV += 0.0;
                }

                // -j_e
                sourceVectorField(i) = -fluxje(i);
                // w_AV = j_e \cdot -dadt + h \cdot dbdt
                ThermalEMFieldSource += (sourceVectorField(i) * ((An(i) - A0(i)) * invdh) /*+ H(i) * (Bn(i) - B0(i)) * invdh*/);

                // to debug weak vs strong coupling results by
                // using analytical EM thermal source in SMP only with
                // A²/2 being mean value that is calculated
                // in weak coupling for considered parameter values

                /*
                  if(this->_num == 1) // if SMP
                    // A² cos²(2 pi f t)
                    // A²/2 = Mean value over one period = 3559017.23076498
                    ThermalEMFieldSource = 2.0 * 2227580. * std::cos(2.0 * M_PI * _freqEM * ctime) *
                                            std::cos(2.0 * M_PI * _freqEM * ctime);
                else
                    ThermalEMFieldSource = 0.0;
                */

                // w_AV += je \cdot -gradV
                if (_useFluxT)
                    ThermalEMFieldSource += (sourceVectorField(i) * gradV(i));
            }

            if (isInductor())
            {
                fluxjy(i) += (_seebeck * T + V) * js0(i);
                // -j_e
                sourceVectorField(i) = - js0(i);
                // w_AV = j_e \cdot -dadt + h \cdot dbdt
                ThermalEMFieldSource += 0.0;

                // w_AV += je \cdot -gradV
                if (_useFluxT)
                    ThermalEMFieldSource += 0.0;

                if(stiff)
                {
                    djydT(i) += (_seebeck + dseebeckdT * T) * js0(i);
                    djydV(i) += js0(i);
                }

                // For test with unit cube to check
                // interpolation of curlVals
                //js0(0) = 1.0;
                //js0(1) = js0(2) = 0.0;
                //sourceVectorField = -js0;
            }
        }

        // to use in weak TM with transformation
        // because em source computed in intermediate config
        // but needs to be used in reference config in weak TM
        ThermalEMFieldSource *= JacRef;
        dThermalEMFieldSourcedA *= JacRef;
        dThermalEMFieldSourcedB *= JacRef;
        dThermalEMFieldSourcedGradT *= JacRef;
        dThermalEMFieldSourcedGradV *= JacRef;
        dThermalEMFieldSourcedT *= JacRef;
        dThermalEMFieldSourcedV *= JacRef;

        // derivatives with deformation gradient
        if(stiff)
        {
            for(int K = 0; K< 3; K++)
            {
                for(int m = 0; m< 3; m++)
                {
                    for(int N = 0; N< 3; N++)
                    {
                        for(int L = 0; L< 3; L++)
                        {
                            //dHdF(K,m,N) += nut(K,L) * dBdF(L,m,N);
                            //dDdF(K,m,N) += epsilont(K,L) * dEdF(L,m,N);
                            for(int p = 0; p < 3; ++p)
                            {
                                for(int Q = 0; Q < 3; ++Q)
                                {
                                    dedF(K,m,N) += dltdFTot(K,L,p,Q) * dFTotdFn(p,Q,m,N) * gradV(L);
                                    dedF(K,m,N) += dltdFTot(K,L,p,Q) * dFTotdFn(p,Q,m,N) * _alpha * gradT(L);
                                    dedF(K,m,N) += dltdFTot(K,L,p,Q)* dFTotdFn(p,Q,m,N) * ((An(L) - A0(L)) * invdh);

                                    dqdF(K,m,N) += dltdFTot(K,L,p,Q) * dFTotdFn(p,Q,m,N) * _seebeck * _seebeck * T * gradT(L);
                                    dqdF(K,m,N) += dltdFTot(K,L,p,Q) * dFTotdFn(p,Q,m,N) * _seebeck * T * gradV(L);
                                    dqdF(K,m,N) += dltdFTot(K,L,p,Q)* dFTotdFn(p,Q,m,N) * _seebeck * T * ((An(L) - A0(L)) * invdh);
                                    dqdF(K,m,N) += dktdFTot(K,L,p,Q) * dFTotdFn(p,Q,m,N) * gradT(L);

                                    djydF(K,m,N) += dktdFTot(K,L,p,Q) * dFTotdFn(p,Q,m,N) * gradT(L);
                                    djydF(K,m,N) += dltdFTot(K,L,p,Q) * dFTotdFn(p,Q,m,N) * _seebeck * _seebeck * T * gradT(L);
                                    djydF(K,m,N) += dltdFTot(K,L,p,Q) * dFTotdFn(p,Q,m,N) * _seebeck * V * gradT(L);
                                    djydF(K,m,N) += dltdFTot(K,L,p,Q) * dFTotdFn(p,Q,m,N) * V * gradV(L);
                                    djydF(K,m,N) += dltdFTot(K,L,p,Q) * dFTotdFn(p,Q,m,N) * _seebeck * T * gradV(L);
                                    djydF(K,m,N) += dltdFTot(K,L,p,Q) * dFTotdFn(p,Q,m,N) * V * ((An(L) - A0(L)) * invdh);
                                    djydF(K,m,N) += dltdFTot(K,L,p,Q) * dFTotdFn(p,Q,m,N) * _seebeck * T * ((An(L) - A0(L)) * invdh);

                                    dHdF(K,m,N) += dnutdFTot(K,L,p,Q) * dFTotdFn(p,Q,m,N) * B(L);
                                    dDdF(K,m,N) += depsilontdFTot(K,L,p,Q) * dFTotdFn(p,Q,m,N) * E_field(L);

                                    dsourceVectorFielddF(K,m,N) += -1.0 * dltdFTot(K,L,p,Q) * dFTotdFn(p,Q,m,N) * gradV(L);
                                    dsourceVectorFielddF(K,m,N) += -1.0 * dltdFTot(K,L,p,Q) * dFTotdFn(p,Q,m,N) * _alpha * gradT(L);
                                    dsourceVectorFielddF(K,m,N) += -1.0 * dltdFTot(K,L,p,Q)* dFTotdFn(p,Q,m,N) * ((An(L) - A0(L)) * invdh);
                                }
                            }
                        }
                        if(_EMFieldDependentShearModulus)
                        {
                            dHdF(K,m,N) += (2.0 * _alpha_e * (1.0 + (_g1 * invariant4 / _G)) * ( 1.0 - (std::tanh(invariant7/_m_e) * std::tanh(invariant7/_m_e)) ) * B(K) * P_TM(m,N)/(_m_e));

                            dDdF(K,m,N) -= (2.0 * _g1 * (1.0 + _alpha_e * std::tanh(invariant7/_m_e)) * E_field(K) * P_TM(m,N)/_G);
                        }
                    }
                }
                if(_EMFieldDependentShearModulus)
                {
                    for(int M = 0; M < 3; ++M)
                    {
                        dHdB(K,M) -= (8.0 * _alpha_e * psiMeca * std::tanh(invariant7/_m_e) * (1.0 - (std::tanh(invariant7/_m_e) * std::tanh(invariant7/_m_e))) * (1.0 + (_g1 * invariant4 / _G)) * B(K) * B(M)/(_m_e * _m_e));
                        dHdB(K,M) += (2.0 * _alpha_e * psiMeca * (1.0 - (std::tanh(invariant7/_m_e) * std::tanh(invariant7/_m_e))) * (1.0 + (_g1 * invariant4 / _G)) * I2(K,M)/(_m_e));

                        dHdgradV(K,M) -= ( 4.0 * _alpha_e * psiMeca * _g1 * (1.0 - (std::tanh(invariant7/_m_e) * std::tanh(invariant7/_m_e))) * B(K) * E_field(M)/(_m_e * _G));

                        dDdgradV(K,M) += (2.0 * _g1 * (1.0 + _alpha_e * std::tanh(invariant7/_m_e)) * psiMeca * I2(K,M)/_G);
                        dDdA(K,M) += (2.0 * _g1 * (1.0 + _alpha_e * std::tanh(invariant7/_m_e)) * psiMeca * invdh * I2(K,M)/_G);
                        dDdB(K,M) -= (4.0 * _g1 * _alpha_e * psiMeca * (1.0 - (std::tanh(invariant7/_m_e) * std::tanh(invariant7/_m_e))) * E_field(K) * B(M)/(_G * _m_e));
                    }
                }
            }

            for(int K = 0; K< 3; K++)
            {
                for(int m = 0; m< 3; m++)
                {
                    for(int N = 0; N< 3; N++)
                    {
                        dThermalEMFieldSourcedF(m,N) += JacRef * dsourceVectorFielddF(K,m,N) * ((An(K) - A0(K)) * invdh);

                        if (_useFluxT)
                            dThermalEMFieldSourcedF(m,N) += JacRef * dsourceVectorFielddF(K,m,N) * gradV(K);

                        // Magnetic hysteresis loss contribution
                        /*dThermalEMFieldSourcedF(m,N) += JacRef * dHdF(K,m,N) * (Bn(K) - B0(K)) * invdh;
                        dThermalEMFieldSourcedF(m,N) += JacRef * H(K) * dBdF(K,m,N) * invdh;*/
                    }
                }
            }
        }

        // Additional stress contribution in case of EM fields dependent Shear Modulus (c.f. Steinmann et al)
        if(_EMFieldDependentShearModulus)
        {
            // for values refer to Bustamante 2010, Saxena 2013, Mehnert et al. 2016, 2017

            // in case of shear modulus a linear function for Elec-Mechanics
            // as in Mehnert et al. 2016, Bustamante 2010
            // Gnew = G (1 + _g1 I4/G) for Elec-Mechanics
            // in case of shear modulus a tanh function for magneto-mechanics
            // as in Mehnert et al. 2017
            // Gnew = G (1 + _alpha_e tanh(I7/_m_e)) for magneto-mechanics

            // Assumption:
            // To simulate elec-mechanics _alpha_e = 0.
            // To simulate magneto-mechanics _g1 = 0.
            // For coupled elec-magn-mechanics use non-zero _alpha_e and _g1
            P *= ( (1.0 + _alpha_e * std::tanh(invariant7/_m_e))*(1.0 + (_g1 * invariant4 / _G)) );

            // Tangent
            if(stiff)
            {
                Tangent *= ( (1.0 + _alpha_e * std::tanh(invariant7/_m_e))*(1.0 + (_g1 * invariant4 / _G)) );
                dPdT *= ( (1.0 + _alpha_e * std::tanh(invariant7/_m_e))*(1.0 + (_g1 * invariant4 / _G)) );
                for(unsigned int i=0; i<3; i++)
                {
                    for(unsigned int J=0; J<3; J++)
                    {
                        for(unsigned int K = 0; K < 3; K++)
                        {
                            dPdB(i,J,K) += (2.0 * _alpha_e * (1.0 - (std::tanh(invariant7/_m_e) * std::tanh(invariant7/_m_e))) * (1.0 + (_g1 * invariant4 / _G)) * P_TM(i,J) * B(K)/(_m_e));
                            dPdE(i,J,K) += (2.0 * P_TM(i,J) * (1.0 + _alpha_e * std::tanh(invariant7/_m_e)) * _g1 * E_field(K)/_G);
                        }
                    }
                }
            }
        }
        // in case of no EM field dependent shear modulus
        // take into account TM stress and tangents -> already done before if(_evaluateCurlField)

        // Compute stresses Maxwell, Mag-mech and Elec-mech here
        if(_useEMStress)
        {
            static STensor3 CtotInv;
            STensorOperation::zero(CtotInv);

            STensorOperation::inverseSTensor3(Ctot,CtotInv);

            static STensor3 PelecMech, PmagMech, Pmaxwell;

            // Tangents
            static STensor33 dPelecMechdE, dPmagMechdE, dPmaxwelldE;
            static STensor33 dPelecMechdB, dPmagMechdB, dPmaxwelldB;
            static STensor43 dPelecMechdFtot, dPmagMechdFtot, dPmaxwelldFtot;

            STensorOperation::zero(PelecMech); STensorOperation::zero(PmagMech); STensorOperation::zero(Pmaxwell);
            STensorOperation::zero(dPelecMechdE); STensorOperation::zero(dPmagMechdE); STensorOperation::zero(dPmaxwelldE);
            STensorOperation::zero(dPelecMechdB); STensorOperation::zero(dPmagMechdB); STensorOperation::zero(dPmaxwelldB);
            STensorOperation::zero(dPelecMechdFtot); STensorOperation::zero(dPmagMechdFtot); STensorOperation::zero(dPmaxwelldFtot);

            for(unsigned int i = 0; i < 3; ++i)
            {
                for(unsigned int J = 0; J < 3; ++J)
                {
                    for(unsigned int K = 0; K < 3; ++K)
                    {
                        if(_mu_8 != 0.0)
                            PmagMech(i,J) += rho * (1.0/_mu_8) * Ftot(i,K) * B(K) * B(J);

                        Pmaxwell(i,J) += 1.0/(Jactot * vacuum_permeability) * FtotInv(K,i) * (Ctot(K,K) * B(K) * B(J));
                        for(unsigned int L = 0; L < 3; ++L)
                        {
                            if(_mu_9 != 0.0)
                                PmagMech(i,J) += rho * (1.0/_mu_9) * Ftot(i,K) * B(K) * Ctot(J,L) * B(L);

                            if(_mu_9 != 0.0)
                                PmagMech(i,J) += rho * (1.0/_mu_9) * Ftot(i,L) * Ctot(L,K) * B(K) * B(J);

                            if(_epsilon_5 != 0.0)
                                PelecMech(i,J) -= rho * _epsilon_5 * FtotInv(K,i) * E_field(K) * CtotInv(J,L) * E_field(L);

                            Pmaxwell(i,J) += Jactot * vacuum_permittivity * FtotInv(K,i) * (E_field(K) * CtotInv(L,J) * E_field(L));

                            for(unsigned int M = 0; M <3; ++M)
                            {
                                if(_epsilon_6 != 0.0)
                                    PelecMech(i,J) -= rho * _epsilon_6 * FtotInv(K,i) * E_field(K) * CtotInv(J,L) *CtotInv(L,M) * E_field(M);

                                if(_epsilon_6 != 0.0)
                                    PelecMech(i,J) -= rho * _epsilon_6 * FtotInv(K,i) * CtotInv(K,L) * E_field(L) * CtotInv(J,M) * E_field(M);

                                Pmaxwell(i,J) += 1.0/(Jactot * vacuum_permeability) * FtotInv(K,i) * (-0.5 * (B(L) * Ctot(L,M) * B(M)) * I2(K,J));
                                Pmaxwell(i,J) += Jactot * vacuum_permittivity * FtotInv(K,i) * (-0.5 * (E_field(L) * CtotInv(L,M) * E_field(M)) * I2(K,J));
                            }
                        }
                    }
                }
            }

            P += PmagMech;
            P += PelecMech;
            P += Pmaxwell;

            // Tangent
            if(stiff)
            {
                for(int i=0; i<3; i++)
                {
                    for(int J=0; J<3; J++)
                    {
                        for(int r=0; r<3; r++)
                        {
                            for(int S=0; S<3; S++)
                            {
                                dPelecMechdFtot(i,J,r,S)=0.0;
                                dPmagMechdFtot(i,J,r,S)=0.0;
                                dPmaxwelldFtot(i,J,r,S)=0.0;

                                for(int L=0; L<3; L++)
                                {
                                    for(int K=0; K<3; K++)
                                    {
                                        if(_epsilon_5 != 0.0)
                                        {
                                            dPelecMechdFtot(i,J,r,S) += rho * _epsilon_5 * (FtotInv(S,i) * FtotInv(L,r) * E_field(L) * CtotInv(J,K) * E_field(K));
                                            dPelecMechdFtot(i,J,r,S) += rho * _epsilon_5 * (FtotInv(L,i) * E_field(L) * FtotInv(J,r) * CtotInv(S,K) * E_field(K));
                                            dPelecMechdFtot(i,J,r,S) += rho * _epsilon_5 * (FtotInv(L,i) * E_field(L) * CtotInv(J,S) * FtotInv(K,r) * E_field(K));
                                        }

                                        dPmaxwelldFtot(i,J,r,S) += Jactot * vacuum_permittivity * FtotInv(L,i) * E_field(L) * CtotInv(K,J) * E_field(K) * FtotInv(S,r);
                                        dPmaxwelldFtot(i,J,r,S) -= Jactot * vacuum_permittivity * FtotInv(S,i) * FtotInv(L,r) * E_field(L) * CtotInv(K,J) * E_field(K);
                                        dPmaxwelldFtot(i,J,r,S) -= Jactot * vacuum_permittivity * FtotInv(L,i) * E_field(L) * FtotInv(K,r) * CtotInv(S,J) * E_field(K);
                                        dPmaxwelldFtot(i,J,r,S) -= Jactot * vacuum_permittivity * FtotInv(L,i) * E_field(L) * CtotInv(K,S) * FtotInv(J,r) * E_field(K);

                                        dPmaxwelldFtot(i,J,r,S) -= 0.5 * Jactot * vacuum_permittivity * E_field(L) * CtotInv(L,K) * E_field(K) * FtotInv(J,i) * FtotInv(S,r);
                                        dPmaxwelldFtot(i,J,r,S) += 0.5 * Jactot * vacuum_permittivity * E_field(L) * CtotInv(L,K) * E_field(K) * FtotInv(S,i) * FtotInv(J,r);
                                        dPmaxwelldFtot(i,J,r,S) += 0.5 * Jactot * vacuum_permittivity * E_field(L) * FtotInv(L,r) * CtotInv(S,K) * E_field(K) * FtotInv(J,i);
                                        dPmaxwelldFtot(i,J,r,S) += 0.5 * Jactot * vacuum_permittivity * E_field(L) * CtotInv(L,S) * FtotInv(K,r) * E_field(K) * FtotInv(J,i);

                                        dPmaxwelldFtot(i,J,r,S) += 0.5/(Jactot * vacuum_permeability) * B(L) * Ctot(L,K) * B(K) * FtotInv(J,i) * FtotInv(S,r);
                                        dPmaxwelldFtot(i,J,r,S) += 0.5/(Jactot * vacuum_permeability) * B(L) * Ctot(L,K) * B(K) * FtotInv(S,i) * FtotInv(J,r);

                                    }
                                    dPmaxwelldFtot(i,J,r,S) -= 1.0/(Jactot * vacuum_permeability) * FtotInv(L,i) * Ctot(L,L) * B(L) * B(J) * FtotInv(S,r);
                                    dPmaxwelldFtot(i,J,r,S) -= 1.0/(Jactot * vacuum_permeability) * FtotInv(S,i) * FtotInv(L,r) * Ctot(L,L) * B(L) * B(J);
                                    dPmaxwelldFtot(i,J,r,S) += 1.0/(Jactot * vacuum_permeability) * FtotInv(L,i) * I2(L,S) * Ftot(r,L) * B(L) * B(J);
                                    dPmaxwelldFtot(i,J,r,S) += 1.0/(Jactot * vacuum_permeability) * FtotInv(L,i) * Ftot(S,r) * B(L) * B(J);

                                    dPmaxwelldFtot(i,J,r,S) -= 0.5/(Jactot * vacuum_permeability) * B(S) * Ftot(r,L) * B(L) * FtotInv(J,i);
                                    dPmaxwelldFtot(i,J,r,S) -= 0.5/(Jactot * vacuum_permeability) * B(L) * Ftot(r,L) * B(S) * FtotInv(J,i);
                                }
                                if(_mu_8 != 0.0)
                                {
                                    dPmagMechdFtot(i,J,r,S) += rho * (1.0/_mu_8) * I2(i,r) * B(S) * B(J);
                                }

                            }
                        }
                    }
                }

                for(int i=0; i<3; i++)
                {
                    for(int J=0; J<3; J++)
                    {
                        for(int K=0; K<3; K++)
                        {
                            dPmagMechdB(i,J,K) = 0.0;
                            dPmaxwelldB(i,J,K) = 0.0;
                            dPelecMechdE(i,J,K) = 0.0;
                            dPmaxwelldE(i,J,K) = 0.0;

                            if(_mu_8 != 0.0)
                            {
                                dPmagMechdB(i,J,K) += rho * (1.0/_mu_8) * Ftot(i,K) * B(J);
                            }

                            for(int M=0; M<3; M++)
                            {
                                dPmaxwelldB(i,J,K) += 1.0/(Jactot * vacuum_permeability) * FtotInv(M,i) * Ctot(M,K) * B(J);
                                dPmaxwelldB(i,J,K) += 1.0/(Jactot * vacuum_permeability) * FtotInv(M,i) * Ctot(M,M) * B(M) * I2(J,K);
                                dPmaxwelldB(i,J,K) -= 1.0/(Jactot * vacuum_permeability) * FtotInv(J,i) * Ctot(K,M) * B(M);

                                dPmaxwelldE(i,J,K) += Jactot * vacuum_permittivity * FtotInv(K,i) * CtotInv(M,J) * E_field(M);
                                dPmaxwelldE(i,J,K) += Jactot * vacuum_permittivity * FtotInv(M,i) * E_field(M) * CtotInv(K,J);
                                dPmaxwelldE(i,J,K) -= Jactot * vacuum_permittivity * FtotInv(J,i) * CtotInv(K,M) * E_field(M);

                                if(_mu_8 != 0.0)
                                {
                                    dPmagMechdB(i,J,K) += rho * (1.0/_mu_8) * Ftot(i,M) * B(M) * I2(J,K);
                                }

                                if(_epsilon_5 != 0.0)
                                {
                                    dPelecMechdE(i,J,K) -= rho * _epsilon_5 * FtotInv(K,i) * CtotInv(J,M) * E_field(M);
                                    dPelecMechdE(i,J,K) -= rho * _epsilon_5 * FtotInv(M,i) * E_field(M) * CtotInv(J,K);
                                }

                            }

                        }
                    }
                }

                Tangent += dPmagMechdFtot;
                Tangent += dPelecMechdFtot;
                Tangent += dPmaxwelldFtot;

                dPdB += dPmagMechdB;
                dPdB += dPmaxwelldB;
                dPdB += dPelecMechdB;

                dPdE += dPelecMechdE;
                dPdE += dPmaxwelldE;
                dPdE += dPmagMechdE;

            }
        }
    }
    
    const double dh = getTimeStep();
    if(dh>0.0)// create some dissipation
    {
        wV = -_cVoltage*(V-V0)/dh;
        dwVdt = 0.;
        dwVdV = - _cVoltage/dh;
        STensorOperation::zero(dwVdV);
    }
    else
    {
        wV=0.;
        dwVdt = 0.;
        dwVdV = 0.;
        STensorOperation::zero(dwVdV);
    }

}
