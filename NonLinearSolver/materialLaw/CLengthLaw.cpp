//
//
//
// Description: Define clength for non local j2 plasticity
//
//
// Author:  <L. Noels>, (C) 2013
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "CLengthLaw.h"
#include <math.h>
#include "STensorOperations.h"

CLengthLaw::CLengthLaw(const int num, const bool init): _num(num), _initialized(init)
{

}

CLengthLaw::CLengthLaw(const CLengthLaw &source)
{
  _num = source._num;
  _initialized = source._initialized;
  
}

CLengthLaw& CLengthLaw::operator=(const CLengthLaw &source)
{
  _num = source._num;
  _initialized = source._initialized;
  return *this;
}

ZeroCLengthLaw::ZeroCLengthLaw(const int num, const bool init) : CLengthLaw(num,init)
{

}
ZeroCLengthLaw::ZeroCLengthLaw(const ZeroCLengthLaw &source) :
					CLengthLaw(source)
{

}

ZeroCLengthLaw& ZeroCLengthLaw::operator=(const CLengthLaw &source)
{
  CLengthLaw::operator=(source);
  const ZeroCLengthLaw* src = dynamic_cast<const ZeroCLengthLaw*>(&source);
  if(src != NULL)
  {
  }
  return *this;
}
void ZeroCLengthLaw::createIPVariable(IPCLength* &ipcl) const
{
  if(ipcl != NULL) delete ipcl;
  ipcl = new IPZeroCLength();
  computeCL(0,*ipcl);
}

void ZeroCLengthLaw::computeCL(double p, IPCLength &ipcl) const
{
  STensor3 &cl = ipcl.getRefToCL();
  STensorOperation::zero(cl);
  for(int i=0; i<3; i++) cl(i,i)=0.;
}
CLengthLaw * ZeroCLengthLaw::clone() const
{
  return new ZeroCLengthLaw(*this);
}

IsotropicCLengthLaw::IsotropicCLengthLaw(const int num, const double _cl, const bool init) : CLengthLaw(num,init),
											cl(_cl)
{
 double tol=1.e-12;
 if(cl < tol) cl=tol;
}
IsotropicCLengthLaw::IsotropicCLengthLaw(const IsotropicCLengthLaw &source) :
					CLengthLaw(source), cl(source.cl)
{
}

IsotropicCLengthLaw& IsotropicCLengthLaw::operator=(const CLengthLaw &source)
{
  CLengthLaw::operator=(source);
  const IsotropicCLengthLaw* src = dynamic_cast<const IsotropicCLengthLaw*>(&source);
  if(src != NULL)
  {
    cl = src->cl;
  }
  return *this;
}
void IsotropicCLengthLaw::createIPVariable(IPCLength* &ipcl) const
{
 
  if(ipcl != NULL) delete ipcl;
  ipcl = new IPIsotropicCLength();
  STensor3 &clm = ipcl->getRefToCL();
  computeCL(0.,*ipcl);
}

void IsotropicCLengthLaw::computeCL(double p, IPCLength &ipcl) const
{
  STensor3 &clm = ipcl.getRefToCL();
  STensorOperation::zero(clm);
  for(int i=0; i<3; i++) clm(i,i)=cl;
}
CLengthLaw * IsotropicCLengthLaw::clone() const
{
  return new IsotropicCLengthLaw(*this);
}

AnisotropicCLengthLaw::AnisotropicCLengthLaw(const int num, const double _cl1, const double _cl2, const double _cl3, 
					const double _euler1, const double _euler2, const double _euler3, 
					const bool init) : CLengthLaw(num,init), cl(_cl1,_cl2,_cl3), euler(_euler1,_euler2,_euler3)
{
 double tol=1.e-10;
 if(cl(0) < tol) cl(0)=tol;
 if(cl(1) < tol) cl(1)=tol;
 if(cl(2) < tol) cl(2)=tol;
}


AnisotropicCLengthLaw::AnisotropicCLengthLaw(const AnisotropicCLengthLaw &source) :
					CLengthLaw(source), cl(source.cl), euler(source.euler)
{
}

AnisotropicCLengthLaw& AnisotropicCLengthLaw::operator=(const CLengthLaw &source)
{
  CLengthLaw::operator=(source);
  const AnisotropicCLengthLaw* src = dynamic_cast<const AnisotropicCLengthLaw*>(&source);
  if(src != NULL)
  {
    cl    = src->cl;
    euler = src->euler;
  }
  return *this;
}
void AnisotropicCLengthLaw::createIPVariable(IPCLength* &ipcl) const
{
 
  if(ipcl != NULL) delete ipcl;
  ipcl = new IPAnisotropicCLength();
  computeCL(0.,*ipcl);
}

void AnisotropicCLengthLaw::computeCL(double p, IPCLength &ipcl) const
{
  STensor3 &clm = ipcl.getRefToCL();
  STensorOperation::zero(clm);
  
  static double fpi = M_PI/180.;
  static double sq2 = sqrt(2.);
  static double c1 = cos(euler(0)*fpi);
  static double s1 = sin(euler(0)*fpi);
  static double c2 = cos(euler(1)*fpi);
  static double s2 = sin(euler(1)*fpi);
  static double c3 = cos(euler(2)*fpi);
  static double s3 = sin(euler(2)*fpi);
  static double s1c2 = s1*c2;
  static double c1c2 = c1*c2;

 
  static STensor3 T(0);
  T(0,0) = cl(0);
  T(1,1) = cl(1);
  T(2,2) = cl(2);
  static STensor3 PT(0);
  PT(0,0) = (c3*c1 - s1c2*s3);
  PT(0,1) = (c3*s1 + c1c2*s3);
  PT(0,2) = (s2*s3);
  PT(1,0) = (-s3*c1 - s1c2*c3);
  PT(1,1) = (-s3*s1 + c1c2*c3);
  PT(1,2) = (s2*c3);
  PT(2,0) = (s1*s2);
  PT(2,1) = (-c1*s2);
  PT(2,2) = (c2);

  clm = PT.transpose();
  clm*=T;
  clm*=PT;
}

CLengthLaw * AnisotropicCLengthLaw::clone() const
{
  return new AnisotropicCLengthLaw(*this);
}

VariableIsotropicCLengthLaw::VariableIsotropicCLengthLaw(const int num, const double _cl, 
                                                         const double _el, const double _nl, const bool init) :
							 CLengthLaw(num,init), cl(_cl), el(_el), nl(_nl)
{
 double tol=1.e-10;
 if(cl < tol) cl=tol;
}
VariableIsotropicCLengthLaw::VariableIsotropicCLengthLaw(const VariableIsotropicCLengthLaw &source) :
					CLengthLaw(source), cl(source.cl), el(source.el), nl(source.nl)
{
}

VariableIsotropicCLengthLaw& VariableIsotropicCLengthLaw::operator=(const CLengthLaw &source)
{
  CLengthLaw::operator=(source);
  const VariableIsotropicCLengthLaw* src = dynamic_cast<const VariableIsotropicCLengthLaw*>(&source);
  if(src != NULL)
  {
    cl = src->cl;
    el = src->el;
    nl = src->nl;
  }
  return *this;
}
void VariableIsotropicCLengthLaw::createIPVariable(IPCLength* &ipcl) const
{
 
  if(ipcl != NULL) delete ipcl;
  ipcl = new IPVariableIsotropicCLength();
  computeCL(0.,*ipcl);
}

void VariableIsotropicCLengthLaw::computeCL(double p, IPCLength &ipcl) const
{
  double lcurrent=0.;
  STensor3 &clm = ipcl.getRefToCL();
  STensorOperation::zero(clm);

   if (p <= el){
     lcurrent=cl*pow(p/el, nl);
   }
   else
   {
     lcurrent=cl;
   }

   for(int i=0;i<3;i++)
   {
     clm(i,i) = lcurrent;
     if(clm(i,i) <1.e-10) clm(i,i) = 1.e-10;
   }

}
CLengthLaw * VariableIsotropicCLengthLaw::clone() const
{
  return new VariableIsotropicCLengthLaw(*this);
}

GeneralVariableIsotropicCLengthLaw::GeneralVariableIsotropicCLengthLaw(const int num, const scalarFunction& f): CLengthLaw(num,true){
  _fct = f.clone();
};
GeneralVariableIsotropicCLengthLaw::GeneralVariableIsotropicCLengthLaw(const GeneralVariableIsotropicCLengthLaw& src): CLengthLaw(src){
  _fct = NULL;
  if (src._fct != NULL){
    _fct = src._fct->clone();
  }
}
GeneralVariableIsotropicCLengthLaw::~GeneralVariableIsotropicCLengthLaw(){
  if (_fct) delete _fct;
};

GeneralVariableIsotropicCLengthLaw& GeneralVariableIsotropicCLengthLaw::operator =(const CLengthLaw& src){
  CLengthLaw::operator =(src);
  const GeneralVariableIsotropicCLengthLaw* psrc = dynamic_cast<const GeneralVariableIsotropicCLengthLaw*>(&src);
  if (psrc != NULL){
    if (psrc->_fct != NULL){
      if (_fct){
        _fct->operator=(*psrc->_fct);
      }
      else{
        _fct = psrc->_fct->clone();
      }
    }
  }
  return *this;
};

void GeneralVariableIsotropicCLengthLaw::createIPVariable(IPCLength* &ipcl) const
{
 
  if(ipcl != NULL) delete ipcl;
  ipcl = new IPVariableIsotropicCLength();
  this->computeCL(0.,*ipcl);
}

void GeneralVariableIsotropicCLengthLaw::computeCL(double p, IPCLength &ipcl) const
{
  STensor3 &clm = ipcl.getRefToCL();
  STensorOperation::zero(clm);
  double lcurrent= _fct->getVal(p);
  if (lcurrent < 1e-10) lcurrent = 1e-10;
  for(int i=0;i<3;i++){
    clm(i,i) = lcurrent;
  }
}

VariableAnisotropicCLengthLaw::VariableAnisotropicCLengthLaw(const int num, 
					const double _cl1, const double _cl2, const double _cl3, 
					const double _euler1, const double _euler2, const double _euler3, 
					const double _el, const double _nl,
					const bool init) : CLengthLaw(num,init), 
					cl(_cl1,_cl2,_cl3), euler(_euler1,_euler2,_euler3), el(_el), nl(_nl)
{
 double tol=1.e-10;
 if(cl(0) < tol) cl(0)=tol;
 if(cl(1) < tol) cl(1)=tol;
 if(cl(2) < tol) cl(2)=tol;
}


VariableAnisotropicCLengthLaw::VariableAnisotropicCLengthLaw(const VariableAnisotropicCLengthLaw &source) :
					CLengthLaw(source), cl(source.cl), euler(source.euler), 
					el(source.el), nl(source.nl)
{
}

VariableAnisotropicCLengthLaw& VariableAnisotropicCLengthLaw::operator=(const CLengthLaw &source)
{
  CLengthLaw::operator=(source);
  const VariableAnisotropicCLengthLaw* src = dynamic_cast<const VariableAnisotropicCLengthLaw*>(&source);
  if(src != NULL)
  {
    cl    = src->cl;
    euler = src->euler;
    el    = src->el;
    nl    = src->nl;
  }
  return *this;
}
void VariableAnisotropicCLengthLaw::createIPVariable(IPCLength* &ipcl) const
{
 
  if(ipcl != NULL) delete ipcl;
  ipcl = new IPVariableAnisotropicCLength();
  computeCL(0.,*ipcl);
}

void VariableAnisotropicCLengthLaw::computeCL(double p, IPCLength &ipcl) const
{
  double lcurrent=0.;

  static double fpi = M_PI/180.;
  static double sq2 = sqrt(2.);
  static double c1 = cos(euler(0)*fpi);
  static double s1 = sin(euler(0)*fpi);
  static double c2 = cos(euler(1)*fpi);
  static double s2 = sin(euler(1)*fpi);
  static double c3 = cos(euler(2)*fpi);
  static double s3 = sin(euler(2)*fpi);
  static double s1c2 = s1*c2;
  static double c1c2 = c1*c2;

 
  static STensor3 T(0);
  static STensor3 PT(0);
  PT(0,0) = (c3*c1 - s1c2*s3);
  PT(0,1) = (c3*s1 + c1c2*s3);
  PT(0,2) = (s2*s3);
  PT(1,0) = (-s3*c1 - s1c2*c3);
  PT(1,1) = (-s3*s1 + c1c2*c3);
  PT(1,2) = (s2*c3);
  PT(2,0) = (s1*s2);
  PT(2,1) = (-c1*s2);
  PT(2,2) = (c2);

  STensorOperation::zero(T);
  if (p <= el){
    for(int i=0;i<3;i++)
    {
      T(i,i)= cl(i)*pow(p/el, nl);
      if(T(i,i) <1.e-10) T(i,i) = 1.e-10;
    } 
  }
  else
  {
    for(int i=0;i<3;i++)
    {
      T(i,i)= cl(i);
      if(T(i,i) <1.e-10) T(i,i) = 1.e-10;
    }
  }

  STensor3 &clm = ipcl.getRefToCL();
  clm = PT.transpose();
  clm*=T;
  clm*=PT;
}

CLengthLaw * VariableAnisotropicCLengthLaw::clone() const
{
  return new VariableAnisotropicCLengthLaw(*this);
}


GeneralVariableAnisotropicCLengthLaw::GeneralVariableAnisotropicCLengthLaw(const int num, const scalarFunction& f1, const scalarFunction& f2, const scalarFunction& f3,
                                       const double _euler1, const double _euler2, const double _euler3):  
                                       CLengthLaw(num,true), euler(_euler1,_euler2,_euler3){
  _fct1 = f1.clone();
  _fct2 = f2.clone();
  _fct3 = f3.clone();
};

GeneralVariableAnisotropicCLengthLaw::~GeneralVariableAnisotropicCLengthLaw(){
  if (_fct1) delete _fct1;
  if (_fct2) delete _fct2;
  if (_fct3) delete _fct3;
};

GeneralVariableAnisotropicCLengthLaw::GeneralVariableAnisotropicCLengthLaw(const GeneralVariableAnisotropicCLengthLaw& src) : CLengthLaw(src),euler(src.euler){
  _fct1 = NULL;
  if (src._fct1) 
    _fct1 = src._fct1->clone();
  _fct2 = NULL;
  if (src._fct2) 
    _fct2 = src._fct2->clone();
  _fct3 = NULL;
  if (src._fct3) 
    _fct3 = src._fct3->clone();
};

GeneralVariableAnisotropicCLengthLaw& GeneralVariableAnisotropicCLengthLaw::operator =(const CLengthLaw& src){
  CLengthLaw::operator =(src);
  const GeneralVariableAnisotropicCLengthLaw* psrc = dynamic_cast<const GeneralVariableAnisotropicCLengthLaw*>(&src);
  if (psrc != NULL){
    euler = psrc->euler;
    if (psrc->_fct1){
      if (_fct1) 
        _fct1->operator=(*psrc->_fct1);
      else
        _fct1 = psrc->_fct1->clone();
    }
    if (psrc->_fct2){
      if (_fct2) 
        _fct2->operator=(*psrc->_fct2);
      else
        _fct2 = psrc->_fct2->clone();
    }
    if (psrc->_fct3){
      if (_fct3) 
        _fct3->operator=(*psrc->_fct3);
      else
        _fct3 = psrc->_fct3->clone();
    }
  };
  return *this;
};

void GeneralVariableAnisotropicCLengthLaw::createIPVariable(IPCLength* &ipcl) const
{
 
  if(ipcl != NULL) delete ipcl;
  ipcl = new IPVariableAnisotropicCLength();
  computeCL(0.,*ipcl);
}

void GeneralVariableAnisotropicCLengthLaw::computeCL(double p, IPCLength &ipcl) const
{

  static double fpi = M_PI/180.;
  static double sq2 = sqrt(2.);
  static double c1 = cos(euler(0)*fpi);
  static double s1 = sin(euler(0)*fpi);
  static double c2 = cos(euler(1)*fpi);
  static double s2 = sin(euler(1)*fpi);
  static double c3 = cos(euler(2)*fpi);
  static double s3 = sin(euler(2)*fpi);
  static double s1c2 = s1*c2;
  static double c1c2 = c1*c2;

 
  static STensor3 T(0);
  static STensor3 PT(0);
  PT(0,0) = (c3*c1 - s1c2*s3);
  PT(0,1) = (c3*s1 + c1c2*s3);
  PT(0,2) = (s2*s3);
  PT(1,0) = (-s3*c1 - s1c2*c3);
  PT(1,1) = (-s3*s1 + c1c2*c3);
  PT(1,2) = (s2*c3);
  PT(2,0) = (s1*s2);
  PT(2,1) = (-c1*s2);
  PT(2,2) = (c2);
  
  STensorOperation::zero(T);
  T(0,0) = _fct1->getVal(p);
  if (T(0,0) < 1e-10) T(0,0) = 1e-10;
  
  T(1,1) = _fct2->getVal(p);
  if (T(1,1) < 1e-10) T(1,1) = 1e-10;
  
  T(2,2) = _fct3->getVal(p);
  if (T(2,2) < 1e-10) T(2,2) = 1e-10;
  
  STensor3 &clm = ipcl.getRefToCL();
  clm = PT.transpose();
  clm*=T;
  clm*=PT;
}
