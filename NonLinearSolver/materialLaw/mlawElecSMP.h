//
// C++ Interface: material law
//              Linear Thermo Mechanical law
// Author:  <L. Homsy>, (C) 2013
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef MLAWELECSMP_H_
#define MLAWELECSMP_H_
#include "mlaw.h"
#include "STensor3.h"
#include "STensor43.h"
#include "material.h"

//#include "mlawLinearElecTherMech.h"
#include "mlawSMP.h"
#include "ipElecSMP.h"



class mlawElecSMP :public mlawSMP
{
 protected:
  // can't be const due to operator= constructor (HOW TO CHANGE THIS ??)

 mutable STensor3 _Kref10,_Kref20,_Lref10,_Lref20,_Lref0;

  double _lx;
  double _ly;
  double _lz;
  double _v0;
  STensor3 _l0;
 /* STensor3 _k10;
  STensor3 _k20;
  STensor3 _l10;
  STensor3 _l20;*/

  // function of materialLaw
  double _seebeck;
  


#ifndef SWIG
 private:

#endif // SWIG
 public:
  mlawElecSMP(const int num,const double rho, const double alpha, const double beta, const double gamma,const double t0,const double Kx,const double Ky,
			 const double Kz, const double mu_groundState3,const double Im3, const double pi  ,const double Tr,
		          const double Nmu_groundState2,const double mu_groundState2, const double Im2,const double Sgl2,const double Sr2,const double Delta,const double m2, const double epsilonr,
			  const double n, const double epsilonp02,const double alphar1,const double alphagl1,const double Ggl1,const double Gr1,const double Mgl1,const double Mr1,const double Mugl1
			  ,const double Mur1, const double epsilon01,const double QglOnKb1,const double QrOnKb1,const double epsygl1 ,const double d1,const double m1,const double VOnKb1,
			  const double alphap,const double  Sa0,const double ha1,const double b1,const double g1,const double phia01,const double Z1,const double r1,const double s1,const double Sb01
			  ,const double Hgl1,const double Lgl1,const double Hr1,const double Lr1,const double l1,const double Kb,const double be1,const double c0,const double wp,const double c1
			  ,const double lx,const double ly,const double lz,const double seebeck,const double v0);
   #ifndef SWIG
 mlawElecSMP(const mlawElecSMP &source);
 /*
 mlawElecSMP& operator=(const  materialLaw &source);
  // function of materialLaw
  virtual matname getType() const{return mlawElecSMP::ElecSMP;}  */
  mlawElecSMP& operator=(const  materialLaw &source);
	virtual ~mlawElecSMP(){};
	virtual materialLaw* clone() const {return new mlawElecSMP(*this);};
  virtual bool withEnergyDissipation() const {return false;};
  // function of materialLaw
  virtual matname getType() const{return materialLaw::ElecSMP;}  
  virtual void createIPState(IPStateBase* &ips, bool hasBodyForce,const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const IntPt *GP=NULL, const int gpt =0) const;

public:
    virtual void constitutive(
                            const STensor3& F0,         // initial deformation gradient (input @ time n)
                            const STensor3& Fn,         // updated deformation gradient (input @ time n+1)
                            STensor3 &P,                // updated 1st Piola-Kirchhoff stress tensor (output)
                                                        // contains the initial values on input
                            const IPElecSMP *q0,       // array of initial internal variable
                            IPElecSMP *q1,             // updated array of internal variable (in ipvcur on output),
                            STensor43 &Tangent,         // constitutive tangents (output)
                            double T0,
			    double T,
			    double V0,
			    double V,
  			    const SVector3 &gradT,
			    const SVector3 &gradV,
                            SVector3 &fluxT,
			    SVector3 &fluxV,
                            STensor3 &dPdT,
                            STensor3 &dPdV,
                            STensor3 &dqdgradT,
                            SVector3 &dqdT,
			    STensor3 &dqdgradV,
                            SVector3 &dqdV,
			    SVector3 &dedV,
			    STensor3 &dedgradV,
			    SVector3 &dedT,
			    STensor3 &dedgradT,
                            STensor33 &dqdF,
			    STensor33 &dedF,
			    double &w,
			    double &dwdt,
			    STensor3 &dwdf,
			    STensor3 &l10,
			    STensor3 &l20,
			    STensor3 &k10,
			    STensor3 &k20,
			    STensor3 &dl10dT,
			    STensor3 &dl20dT,
			    STensor3 &dk10dT,
			    STensor3 &dk20dT,
			    STensor3 &dl10dv,
			    STensor3 &dl20dv,
			    STensor3 &dk10dv,
			    STensor3 &dk20dv,
			    STensor43 &dl10dF,
			    STensor43 &dl20dF,
			    STensor43 &dk10dF,
			    STensor43 &dk20dF,
			    SVector3 &fluxjy,
			    SVector3 &djydV,
			    STensor3 &djydgradV,
			    SVector3 &djydT,
			    STensor3 &djydgradT,
                            STensor33 &djydF,
			    STensor3 &djydgradVdT,
			    STensor3 &djydgradVdV,
			    STensor43 &djydgradVdF,
			    STensor3 &djydgradTdT,
			    STensor3 &djydgradTdV,
			    STensor43 &djydgradTdF,
                            const bool stiff
                           ) const;
    double getInitialTemperature() const {return _t0;};
    double getInitialVoltage() const {return _v0;};
    virtual double deformationEnergy(const SVector3 &gradV,const SVector3 &gradT,const SVector3 &fluxV,const SVector3 &fluxjy,double T, double V) const ;
    virtual const STensor3& getStiff_alphaDilatation()const { return _Stiff_alphaDilatation;};
   /* const STensor3&  getInitialElecConductivityTensor() const { return _l10;};
    const STensor3&  getInitialElecCrossConductivityTensor() const { return _l20;};
    const STensor3&  getInitialConductivityTensor() const { return _k10;};
    const STensor3&  getInitialCrossConductivityTensor() const { return _k20; };*/
 //protected:
  //virtual double deformationEnergy(STensor3 defo) const ;
  //virtual double deformationEnergy(STensor3 defo, const STensor3& sigma) const ;


 #endif // SWIG
};

#endif // MLAWELECSMP_H_
