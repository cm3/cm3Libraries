
//
// C++ Interface: material law for user direction
//
// Author:  <Van Dung NGUYEN (C) 2021>
//
// Copyright: See COPYING file that comes with this distribution
//



#ifndef USERDIRECTIONGENERATOR_H_
#define USERDIRECTIONGENERATOR_H_

#ifndef SWIG
#include "SVector3.h"
#endif //SWIG

class UserDirectionGenerator
{
  public:
    #ifndef SWIG
    UserDirectionGenerator(){}
    UserDirectionGenerator(const UserDirectionGenerator& src){};
    virtual ~UserDirectionGenerator(){}
    virtual void getTransverseDirection(SVector3 &A, const SVector3 &pg) const = 0;
    virtual UserDirectionGenerator* clone() const = 0;
    #endif //SWIG  
};

class UniformDirectionGenerator : public UserDirectionGenerator
{
  protected:
    #ifndef SWIG
    SVector3 direction;
    #endif //SWIG
    
  public:
    UniformDirectionGenerator(double x, double y, double z);
    UniformDirectionGenerator(const UniformDirectionGenerator& src);
    virtual ~UniformDirectionGenerator(){}
    virtual void getTransverseDirection(SVector3 &A, const SVector3 &pg) const;
    virtual UserDirectionGenerator* clone() const{return new UniformDirectionGenerator(*this);}
};
#endif //USERDIRECTIONGENERATOR_H_