//
//
//
// Description: Define damage law for non local j2 plasticity
//
//
// Author:  <L. Noels>, (C) 2013
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "DamageLaw.h"
#include <math.h>
#include "STensorOperations.h"

void DamageLaw::setMinMaxFactor(const double ma, const double mi)
{
  _fscmax = ma;
  _fscmin = mi;
};
double DamageLaw::getFactor() const
{
  return (rand()/(double)RAND_MAX) * (_fscmax-_fscmin) + _fscmin;
}

DamageLaw::DamageLaw(const int num, const bool init): _num(num), _initialized(init), _fscmin(1.),_fscmax(1.)
{

}

DamageLaw::DamageLaw(const DamageLaw &source)
{
  _num = source._num;
  _initialized = source._initialized;
  _fscmin = source._fscmin;
  _fscmax = source._fscmax;
}

DamageLaw& DamageLaw::operator=(const DamageLaw &source)
{
  _num = source._num;
  _initialized = source._initialized;
  _fscmin = source._fscmin;
  _fscmax = source._fscmax;
  return *this;
}

LemaitreChabocheDamageLaw::LemaitreChabocheDamageLaw(const int num, const double _p0, const double _pc,
                                                    const double _n,
                                                    const double _alpha, const bool init) :
				DamageLaw(num,init), pInit(_p0), pc(_pc), n(_n), alpha(_alpha)
{

}
LemaitreChabocheDamageLaw::LemaitreChabocheDamageLaw(const LemaitreChabocheDamageLaw &source) :
					DamageLaw(source)
{

  pInit     =  source.pInit;
  pc     =  source.pc;
  n      =  source.n;
  alpha  =  source.alpha;
}

LemaitreChabocheDamageLaw& LemaitreChabocheDamageLaw::operator=(const DamageLaw &source)
{
  DamageLaw::operator=(source);
  const LemaitreChabocheDamageLaw* src = dynamic_cast<const LemaitreChabocheDamageLaw*>(&source);
  if(src != NULL)
  {
    pInit     =  src->pInit;
    pc     =  src->pc;
    n      =  src->n;
    alpha  =  src->alpha;

  }
  return *this;
}
void LemaitreChabocheDamageLaw::createIPVariable(IPDamage* &ipcl) const
{
  if(ipcl != NULL) delete ipcl;
  ipcl = new IPLemaitreChabocheDamage(pInit*getFactor());
}

void LemaitreChabocheDamageLaw::computeDamage(double p1, double p0, double phiel,
                                              const STensor3 &Fe, const STensor3 &Fp,
                                              const STensor3 &Peff,
                                              const STensor43 &Cel,
                                              const IPDamage &ipvprev, IPDamage &ipvcur) const
{
  double pinitial = ipvcur.getInitialP();
  double D0     = ipvprev.getDamage();
  double maxp0  = ipvprev.getMaximalP();
  double DeltaD = 0.;
  double dDdp   = 0.;
  static STensor3 dDdFe;
  STensorOperation::zero(dDdFe);

  double D=D0;
  double maxp = maxp0;
  if(p1>pc) p1=pc;
  if(p1>p0 and p1>pinitial)
  {
    DeltaD = pow(phiel/alpha,n)*(p1-p0);
    dDdp   = pow(phiel/alpha,n);
    for(int i=0; i<3; i++)
    {
      for(int j=0; j<3; j++)
      {
        dDdFe(i,j)=0.;
        for(int k=0; k<3; k++)
          dDdFe(i,j)+=Peff(i,k)*Fp(j,k);
       }
     }
     dDdFe *=(pow(phiel/alpha,n-1)*(p1-p0)*n/alpha);
  }
  D = D0+DeltaD;
  if(D>0.99999999999)
  {
     double oldDD=DeltaD;
     DeltaD      = 0.99999999999-D0;
     double rat = DeltaD/oldDD;
     D = D0+DeltaD;
     dDdp   *= rat;
     dDdFe  *= rat;
  }

  if(p1>maxp) maxp=p1;
  ipvcur.setValues(D, maxp, DeltaD, dDdp, dDdFe);
}

DamageLaw * LemaitreChabocheDamageLaw::clone() const
{
  return new LemaitreChabocheDamageLaw(*this);
}


SmallStrainLemaitreChabocheDamageLaw::SmallStrainLemaitreChabocheDamageLaw(const int num, const double _p0, const double _pc,
                                                    const double _n,
                                                    const double _alpha, const bool init) :
				DamageLaw(num,init), pInit(_p0), pc(_pc), n(_n), alpha(_alpha)
{

}
SmallStrainLemaitreChabocheDamageLaw::SmallStrainLemaitreChabocheDamageLaw(const SmallStrainLemaitreChabocheDamageLaw &source) :
					DamageLaw(source)
{

  pInit     =  source.pInit;
  pc     =  source.pc;
  n      =  source.n;
  alpha  =  source.alpha;
}

SmallStrainLemaitreChabocheDamageLaw& SmallStrainLemaitreChabocheDamageLaw::operator=(const DamageLaw &source)
{
  DamageLaw::operator=(source);
  const SmallStrainLemaitreChabocheDamageLaw* src = dynamic_cast<const SmallStrainLemaitreChabocheDamageLaw*>(&source);
  if(src != NULL)
  {
    pInit     =  src->pInit;
    pc     =  src->pc;
    n      =  src->n;
    alpha  =  src->alpha;

  }
  return *this;
}
void SmallStrainLemaitreChabocheDamageLaw::createIPVariable(IPDamage* &ipcl) const
{
  if(ipcl != NULL) delete ipcl;
  ipcl = new IPLemaitreChabocheDamage(pInit*getFactor());
}

void SmallStrainLemaitreChabocheDamageLaw::computeDamage(double p1, double p0, double phiel,
                                              const STensor3 &Fe, const STensor3 &Fp,
                                              const STensor3 &Peff,
                                              const STensor43 &Cel,
                                              const IPDamage &ipvprev, IPDamage &ipvcur) const
{
  double pinitial = ipvcur.getInitialP();
  double D0     = ipvprev.getDamage();
  double maxp0  = ipvprev.getMaximalP();
  double DeltaD = 0.;
  double dDdp   = 0.;
  static STensor3 dDdFe;
  STensorOperation::zero(dDdFe);

  double D=D0;
  double maxp = maxp0;
  if(p1>pc) p1=pc;
  if(p1>p0 and p1>pinitial)
  {
    DeltaD = pow(phiel/alpha,n)*(p1-p0);
    dDdp   = pow(phiel/alpha,n);
    dDdFe = Peff;
    dDdFe *=(pow(phiel/alpha,n-1)*(p1-p0)*n/alpha);
  }
  D = D0+DeltaD;
  if(D>0.99999999999)
  {
     double oldDD=DeltaD;
     DeltaD      = 0.99999999999-D0;
     double rat = DeltaD/oldDD;
     D = D0+DeltaD;
     dDdp   *= rat;
     dDdFe  *= rat;
  }

  if(p1>maxp) maxp=p1;
  ipvcur.setValues(D, maxp, DeltaD, dDdp, dDdFe);
}

DamageLaw * SmallStrainLemaitreChabocheDamageLaw::clone() const
{
  return new SmallStrainLemaitreChabocheDamageLaw(*this);
}



// ========== Power Damage Law ==========
PowerDamageLaw::PowerDamageLaw(const int num, const double pi, const double pc,
                               const double alpha,
                               const double beta, const bool init) :
    DamageLaw(num,init), _pi(pi), _pc(pc), _alpha(alpha), _beta(beta)
{
}


PowerDamageLaw::PowerDamageLaw(const PowerDamageLaw &source) :
    DamageLaw(source)
{
    _pi     =  source._pi;
    _pc     =  source._pc;
    _alpha  =  source._alpha;
    _beta   =  source._beta;
}


PowerDamageLaw& PowerDamageLaw::operator=(const DamageLaw &source)
{
    DamageLaw::operator=(source);
    const PowerDamageLaw* src = dynamic_cast<const PowerDamageLaw*>(&source);
    if(src != NULL)
        {
            _pi     =  src->_pi;
            _pc     =  src->_pc;
            _alpha  =  src->_alpha;
            _beta   =  src->_beta;
        }
    return *this;
}


void PowerDamageLaw::createIPVariable(IPDamage* &ipcl) const
{
    if(ipcl != NULL) delete ipcl;
    ipcl = new IPPowerLaw(_pi*getFactor());
}


DamageLaw * PowerDamageLaw::clone() const
{
    return new PowerDamageLaw(*this);
}


void PowerDamageLaw::computeDamage(const double p1, const double p0, double phiel, const STensor3 &Fe,
                                   const STensor3 &Fp, const STensor3 &Peff, const STensor43 &Cel,
                                   const IPDamage &ipvprev, IPDamage &ipvcur) const
{
    // Variable initialisation
    double pinitial = ipvcur.getInitialP();
    double D0 = ipvprev.getDamage();        // Previous damage
    double maxp0 = ipvprev.getMaximalP();   // Previous memory variable values
    double DeltaD(0.);                      // Variation of D
    double dDdp(0.);                        // Partial damage on partial memory variable
    static STensor3 dDdFe;                  // Partial damage on elastic strains ???
    STensorOperation::zero(dDdFe);

    double D = D0;                          // Current new values of damage
    double maxp = maxp0;                    // Current new values of memory variable

    // In case of memory variable increases
    if(p1>maxp)
        {
            maxp = p1;      // Updating of memory variable

            if(p1>pinitial)
                {
                    // Damage function: D(p) = 1-(pi/p)**beta * (pc-p/pc-pi)**alpha
                    D = 1. - pow(pinitial/p1, _beta) * pow((_pc-p1)/(_pc-pinitial), _alpha);
                    dDdp = _beta*pow(pinitial/p1, _beta-1)*(pinitial/p1/p1)*pow((_pc-p1)/(_pc-pinitial), _alpha) + pow(pinitial/p1, _beta)*_alpha*pow((_pc-p1)/(_pc-pinitial), _alpha-1)/(_pc-pinitial);

                    DeltaD = D-D0;
                }
        }
    // In order to avoid D = 1
    if(D>0.99999999999)
        {
            double oldDD=DeltaD;
            DeltaD      = 0.99999999999-D0;
            double rat = DeltaD/oldDD;
            D = D0+DeltaD;
            dDdp   *= rat;
            dDdFe  *= rat;
        }

    // Values saving
    ipvcur.setValues(D, maxp, DeltaD, dDdp, dDdFe);
}


//
SigmoidDamageLaw::SigmoidDamageLaw(const int num, const double pc, const double beta, const double Dmax, const bool init):
        DamageLaw(num,init),_pc(pc),_beta(beta),_Dmax(Dmax){};
SigmoidDamageLaw::SigmoidDamageLaw(const SigmoidDamageLaw &source):DamageLaw(source),_pc(source._pc),_beta(source._beta),_Dmax(source._Dmax){};
SigmoidDamageLaw& SigmoidDamageLaw::operator=(const DamageLaw &source){
  DamageLaw::operator=(source);
  const SigmoidDamageLaw* psrc = dynamic_cast<const SigmoidDamageLaw*>(&source);
  if (psrc != NULL){
    _pc = psrc->_pc;
    _beta = psrc->_beta;
    _Dmax = psrc->_Dmax;
  }
  return *this;
};
void SigmoidDamageLaw::createIPVariable(IPDamage* &ipcl) const{
  if(ipcl != NULL) delete ipcl;
  ipcl = new IPSigmoidDamageLaw(0.);
};

void SigmoidDamageLaw::computeDamage(double p1, double p0, double phiel, const STensor3 &Fe,
                         const STensor3 &Fp, const STensor3 &Peff,
                         const STensor43 &Cel, const IPDamage &ipvprev, IPDamage &ipvcur) const{
  double pinitial = ipvcur.getInitialP();
  double D0     = ipvprev.getDamage();
  double maxp0  = ipvprev.getMaximalP();
  double DeltaD = 0.;
  double dDdp   = 0.;
  static STensor3 dDdFe;
  STensorOperation::zero(dDdFe);

  double D=D0;
  double maxp = maxp0;
  if (p1> maxp and p1 >pinitial){
    double dp = p1-pinitial;
    double Dprim  = 1./(1.+exp(_beta*(_pc-p1) ) );
    double Dprimprim  = _beta*exp(_beta*(_pc-p1) )/(1.+exp(_beta*(_pc-p1) ) )/(1.+exp(_beta*(_pc-p1) ) );
    double D0prim = 1./(1.+exp(_beta*_pc ) );
    
    D = _Dmax*(Dprim-D0prim)/(1.-D0prim);
    dDdp = _Dmax*(Dprimprim)/(1.-D0prim);

    DeltaD = D-D0;
  }

  if(p1>maxp) maxp=p1;
  ipvcur.setValues(D, maxp, DeltaD, dDdp, dDdFe);
};

ExponentialDamageLaw::ExponentialDamageLaw(const int num, const double pinit, const double b, const double K, const double alp, const bool init):
        DamageLaw(num,init),_pi(pinit),_b(b),_K(K), _alpha(alp){};
ExponentialDamageLaw::ExponentialDamageLaw(const ExponentialDamageLaw &source):DamageLaw(source),_pi(source._pi),_b(source._b),_K(source._K), _alpha(source._alpha){};
ExponentialDamageLaw& ExponentialDamageLaw::operator=(const DamageLaw &source){
  DamageLaw::operator=(source);
  const ExponentialDamageLaw* psrc = dynamic_cast<const ExponentialDamageLaw*>(&source);
  if (psrc != NULL){
    _pi = psrc->_pi;
    _b = psrc->_b;
    _K = psrc->_K;
    _alpha = psrc->_alpha;
  }
  return *this;
};
void ExponentialDamageLaw::createIPVariable(IPDamage* &ipcl) const{
  if(ipcl != NULL) delete ipcl;
  ipcl = new IPExponentialDamageLaw(_pi*getFactor());
};

void ExponentialDamageLaw::computeDamage(double p1, double p0, double phiel, const STensor3 &Fe,
                         const STensor3 &Fp, const STensor3 &Peff,
                         const STensor43 &Cel, const IPDamage &ipvprev, IPDamage &ipvcur) const{
  double pinitial = ipvcur.getInitialP();
  double D0     = ipvprev.getDamage();
  double maxp0  = ipvprev.getMaximalP();
  double DeltaD = 0.;
  double dDdp   = 0.;
  static STensor3 dDdFe;
  STensorOperation::zero(dDdFe);

  double D=D0;
  double maxp = maxp0;
  if (p1> maxp and p1 >pinitial){
    double dp = p1-pinitial;
    double fact = (pinitial+_b)/(p1+_b);

    D = 1.-pow(fact,_alpha)*exp(-_K*dp);
    dDdp = (1.-D)*(_K+_alpha/(p1+_b));

    DeltaD = D-D0;
  }

  if(p1>maxp) maxp=p1;
  ipvcur.setValues(D, maxp, DeltaD, dDdp, dDdFe);
};
//

ThreeParametersExponentialDamageLaw::ThreeParametersExponentialDamageLaw(const int num, const double pi, const double a, const double b, const double g):
        DamageLaw(num,true),_pi(pi),_alpha(a),_beta(b),_gamma(g){};
ThreeParametersExponentialDamageLaw::ThreeParametersExponentialDamageLaw(const ThreeParametersExponentialDamageLaw &source):DamageLaw(source),_pi(source._pi),
_alpha(source._alpha),_beta(source._beta),_gamma(source._gamma){};
ThreeParametersExponentialDamageLaw& ThreeParametersExponentialDamageLaw::operator=(const DamageLaw &source){
  DamageLaw::operator=(source);
  const ThreeParametersExponentialDamageLaw* psrc = dynamic_cast<const ThreeParametersExponentialDamageLaw*>(&source);
  if (psrc != NULL){
    _pi = psrc->_pi;
    _alpha = psrc->_alpha;
    _beta = psrc->_beta;
		_gamma = psrc->_gamma;
  }
  return *this;
};
void ThreeParametersExponentialDamageLaw::createIPVariable(IPDamage* &ipcl) const{
  if(ipcl != NULL) delete ipcl;
  ipcl = new IPThreeParametersExponentialDamageLaw(_pi*getFactor());
};

void ThreeParametersExponentialDamageLaw::computeDamage(double p1, double p0, double phiel, const STensor3 &Fe,
                         const STensor3 &Fp, const STensor3 &Peff,
                         const STensor43 &Cel, const IPDamage &ipvprev, IPDamage &ipvcur) const{
  double pinitial = ipvcur.getInitialP();
  double D0     = ipvprev.getDamage();
  double maxp0  = ipvprev.getMaximalP();
  double DeltaD = 0.;
  double dDdp   = 0.;
  static STensor3 dDdFe;
  STensorOperation::zero(dDdFe);

  double D=D0;
  double maxp = maxp0;
  if (p1> maxp and p1 >pinitial){
    double dp = p1-pinitial;
    double expbe = exp(-_beta*dp);
		double piOverp = pinitial/p1;
		double powPiOverp = pow(piOverp,_gamma);

    D = 1.- powPiOverp*(1-_alpha + _alpha*expbe);

    dDdp = (1-_alpha + _alpha*expbe)*(_gamma*powPiOverp/p1) + powPiOverp*_alpha*_beta*expbe;

    DeltaD = D-D0;
  }

  if(D>0.99999999999)
  {
     double oldDD=DeltaD;
     DeltaD      = 0.99999999999-D0;
     double rat = DeltaD/oldDD;
     D = D0+DeltaD;
     dDdp   *= rat;
     dDdFe  *= rat;
  }

  if(p1>maxp) maxp=p1;
  ipvcur.setValues(D, maxp, DeltaD, dDdp, dDdFe);
};





SimpleSaturateDamageLaw::SimpleSaturateDamageLaw(const int num, const double Hh, const double Dinfty, const double pi, const double a, const bool init):
      DamageLaw(num,init), H(Hh),Dinf(Dinfty),pInit(pi),alpha(a){
};

SimpleSaturateDamageLaw::SimpleSaturateDamageLaw(const SimpleSaturateDamageLaw &source) :
					DamageLaw(source)
{
  H = source.H;
  Dinf = source.Dinf;
  pInit = source.pInit;
  alpha = source.alpha;
}

SimpleSaturateDamageLaw& SimpleSaturateDamageLaw::operator=(const DamageLaw &source)
{
  DamageLaw::operator=(source);
  const SimpleSaturateDamageLaw* src = dynamic_cast<const SimpleSaturateDamageLaw*>(&source);
  if(src != NULL)
  {
    H = src->H;
    Dinf = src->Dinf;
    pInit = src->pInit;
    alpha = src->alpha;
  }
  return *this;
}
void SimpleSaturateDamageLaw::createIPVariable(IPDamage* &ipcl) const
{
  if(ipcl != NULL) delete ipcl;
  ipcl = new IPSimpleSaturateDamageLaw(pInit*getFactor());
}

void SimpleSaturateDamageLaw::computeDamage(double p1, double p0, double phiel,
                                              const STensor3 &Fe,
                                              const STensor3 &Fp, const STensor3 &Peff,
                                              const STensor43 &Cel,
                                    const IPDamage &ipvprev, IPDamage &ipvcur) const
{
  double pinitial = ipvcur.getInitialP();
  double D0     = ipvprev.getDamage();
  double maxp0  = ipvprev.getMaximalP();
  double DeltaD = 0.;
  double dDdp   = 0.;
  STensor3 dDdFe;
  STensorOperation::zero(dDdFe);

  double D=D0;
  double maxp = maxp0;

  if (maxp < pinitial) maxp = pinitial;
  /*
  if (p1> p0 and p0> pinitial){
    double dp = p0 - pinitial;
    dDdp = H*alpha*pow(dp,alpha-1.)*(Dinf-D0);
    DeltaD = dDdp*(p1-p0);
    D = D0+DeltaD;
  }
  */

  if (p1> maxp and p1 > p0){
    double dp = p1-pinitial;
    double dpa = pow(dp,alpha);
    D = Dinf*(1.-exp(-H*dpa));
    DeltaD = D-D0;
    if ((alpha < 1.) and dp < 1e-5){
      dDdp = 1e10;
      Msg::Error("Pass Here");
    }
    else
      dDdp = H*alpha*pow(dp,alpha-1.)*(Dinf-D);

  }


  if(D>0.99999999999)
  {
     double oldDD=DeltaD;
     DeltaD      = 0.99999999999-D0;
     double rat = DeltaD/oldDD;
     D = D0+DeltaD;
     dDdp   *= rat;
     dDdFe  *= rat;
  }

  if(p1>maxp) maxp=p1;

  //Msg::Error("D = %e, dDdp = %e",D,dDdp);
  ipvcur.setValues(D, maxp, DeltaD, dDdp, dDdFe);
}

DamageLaw * SimpleSaturateDamageLaw::clone() const
{
  return new SimpleSaturateDamageLaw(*this);
}


PowerBrittleDamagelaw::PowerBrittleDamagelaw(const int n, const double pi, const double pf, const double m, const double nn, const bool init):
  DamageLaw(n,init),_pi(pi),_pf(pf),_m(m),_n(nn){
  _H = (_m+1.)/(_n+1.) /(pow(_pf,_m+1.) - pow(_pi,_m+1.));
	_Dc = (0.999);
	double Xf =  getP(_Dc);
	Msg::Info("critial value Xf/ pf= %e",Xf/_pf);
  _regFunction = new twoVariableExponentialSaturationScalarFunction(0.8*_pf,Xf);
};

void PowerBrittleDamagelaw::setRegulizedFunction(const scalarFunction& fct){
  if (_regFunction) delete _regFunction;
  _regFunction = fct.clone();
};

void PowerBrittleDamagelaw::setCriticalDamage(const double Dc){
  _Dc = Dc;
  double Xf =  getP(_Dc);
	Msg::Info("critial value Xf/ pf= %e",Xf/_pf);
  if (_regFunction) delete _regFunction;
  _regFunction = new twoVariableExponentialSaturationScalarFunction(0.8*_pf,Xf);
};



PowerBrittleDamagelaw::PowerBrittleDamagelaw(const PowerBrittleDamagelaw& src): DamageLaw(src),
  _pi(src._pi),_pf(src._pf),_m(src._m),_n(src._n),_H(src._H),_Dc(src._Dc){
  _regFunction = NULL;
  if (src._regFunction != NULL){
    _regFunction = src._regFunction->clone();
  }
};

PowerBrittleDamagelaw& PowerBrittleDamagelaw::operator=(const DamageLaw& src){
  DamageLaw::operator=(src);
  const PowerBrittleDamagelaw* psrc = dynamic_cast<const PowerBrittleDamagelaw*>(&src);
  if (psrc != NULL){
    _pi = psrc->_pi;
    _pf = psrc->_pf;
    _m = psrc->_m;
    _n = psrc->_n;
    _H = psrc->_H;
    _Dc = psrc->_Dc;
		if (_regFunction != NULL){
			delete _regFunction;
    	_regFunction = NULL;
		}
    if (psrc->_regFunction != NULL){
      _regFunction = psrc->_regFunction->clone();
    }
  }
  return *this;
};

void PowerBrittleDamagelaw::createIPVariable(IPDamage* &ipcl) const
{
    if(ipcl != NULL) delete ipcl;
    ipcl = new IPPowerBrittleDamagelaw(_pi*getFactor());
}


DamageLaw * PowerBrittleDamagelaw::clone() const
{
    return new PowerBrittleDamagelaw(*this);
}


void PowerBrittleDamagelaw::computeDamage(double p1, double p0, double phiel, const STensor3 &Fe,
                                   const STensor3 &Fp, const STensor3 &Peff, const STensor43 &Cel,
                                   const IPDamage &ipvprev, IPDamage &ipvcur) const
{
    // Variable initialisation
    double pinitial = ipvprev.getInitialP();
    double D0 = ipvprev.getDamage();        // Previous damage
    double maxp0 = ipvprev.getMaximalP();   // Previous memory variable values
    double DeltaD(0.);                      // Variation of D
    double dDdp(0.);                        // Partial damage on partial memory variable
    STensor3 dDdFe;
    STensorOperation::zero(dDdFe);
    ipvcur.getRefToInitialP() = pinitial;

    double D = D0;                          // Current new values of damage
    double maxp = maxp0;                    // Current new values of memory variable

    if ((p1>maxp)and (p1>pinitial)){
      double p1f = p1;
      double diffp1 = 1.;
			if (_regFunction != NULL){
				p1f = _regFunction->getVal(p1);
	      diffp1 = _regFunction->getDiff(p1);
			};
      double valt = 1. - _H*(_n+1.)/(_m+1.)*(pow(p1f,_m+1.) - pow(pinitial,_m+1.));
      double powval = 1./(_n+1.);
			if (valt > 0){
      	D = 1. - pow(valt,powval);
			}
			else
				D = 1.;

			if(D>_Dc)
		  {
		    D = _Dc;
		  }

      DeltaD = D - D0;
      dDdp = _H * pow(p1f,_m)*diffp1*pow(1.-D,-_n);
    }
    else{
      D = D0;
      DeltaD = 0.;
      dDdp = 0.;
    }



    if(p1>maxp) maxp=p1;
    // Values saving
    ipvcur.setValues(D, maxp, DeltaD, dDdp, dDdFe);
}

double PowerBrittleDamagelaw::getP(const double D) const{
  double intg = ((_m+1.)/((_n+1.)*_H))*(1.-pow(1.-D,_n+1.)) + pow(_pi,_m+1.);
  if (intg > 0)
    return pow(intg,1./(_m+1.));
  else return 0.;
};

