//
// Created by vinayak on 27.06.2023.
//
// C++ Interface: material law
//              Coupled Thermo-Mechanical law
//              with link to use any TM law
//              such as LinearTM, PhenomenologicalSMP etc.
// Author:  <Vinayak GHOLAP>, (C) 2023
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef MLAWCOUPLEDTHERMOMECHANICS_H_
#define MLAWCOUPLEDTHERMOMECHANICS_H_

#include "mlaw.h"
#include "STensor3.h"
#include "STensor43.h"
#include "STensor63.h"
#include "scalarFunction.h"
#include "ipCoupledThermoMechanics.h"

class mlawCoupledThermoMechanics: public materialLaw{
protected:

    bool applyReferenceF;

public:
    mlawCoupledThermoMechanics(const int num, const bool init = true);

    void setApplyReferenceF(const bool rf)
    {
        applyReferenceF=rf;
    }

    virtual void setTime(const double ctime,const double dtime){ materialLaw::setTime(ctime,dtime);}
    virtual void setMacroSolver(const nonLinearMechSolver* sv){ _macroSolver =sv;}

#ifndef SWIG
    mlawCoupledThermoMechanics(const mlawCoupledThermoMechanics &source);
    virtual mlawCoupledThermoMechanics& operator=(const materialLaw &source);
    virtual void initLaws(const std::map<int,materialLaw*> &maplaw)=0; // this law is initialized so nothing to do
    virtual double soundSpeed() const=0;
    virtual double density() const=0;
    virtual void checkInternalState(IPVariable* ipv, const IPVariable* ipvprev) const=0; // do nothing
    virtual bool withEnergyDissipation() const=0;
    virtual matname getType() const=0;

    virtual void constitutive(
            const STensor3      &F0,                   // initial deformation gradient (input @ time n)
            const STensor3      &Fn,                   // updated deformation gradient (input @ time n+1)
            STensor3            &P,                    // updated 1st Piola-Kirchhoff stress tensor (output)
            const IPVariable    *q0,                   // array of initial internal variable
            IPVariable          *q1,                   // updated array of internal variable (in ipvcur on output),
            STensor43           &Tangent,              // constitutive tangents (output)
            const bool          stiff,
            STensor43           *elasticTangent=NULL,
            const bool dTangent =false,
            STensor63* dCalgdeps = NULL
    ) const=0;

    virtual void constitutive(
            const STensor3   &F0,
            const STensor3   &Fn,
            STensor3         &P,
            const STensor3   &P0,
            const IPVariable *q0,
            IPVariable       *q1,
            STensor43        &Tangent,
            const double     T0,
            double           T,
            const SVector3   &gradT,
            SVector3         &fluxT,
            STensor3         &dPdT,
            STensor3         &dqdgradT,
            SVector3         &dqdT,
            STensor33        &dqdF,
            const bool       stiff,
            double           &wth,
            double           &dwthdt,
            STensor3         &dwthdF,
            double           &mechSource,
            double           &dmechSourcedT,
            STensor3         &dmechSourcedF,
            STensor43        *elasticTangent=NULL
    ) const=0;

    virtual double getInitialTemperature() const{Msg::Error("getInitialTemperature is not defined in mlawCoupledThermoMechanics"); return 0.0;};
    virtual double getInitialExtraDofStoredEnergyPerUnitField() const{Msg::Error("getInitialExtraDofStoredEnergyPerUnitField is not defined in mlawCoupledThermoMechanics"); return 0.0;};
    virtual const STensor3&  getConductivityTensor() const{Msg::Error("getConductivityTensor is not defined in mlawCoupledThermoMechanics"); static STensor3 dummy; return dummy;};
    virtual double getExtraDofStoredEnergyPerUnitField(double T) const {Msg::Error("getExtraDofStoredEnergyPerUnitField is not defined in mlawCoupledThermoMechanics"); return 0.0;};
    virtual const STensor3& getAlphaDilatation() const{Msg::Error("getAlphaDilatation is not defined in mlawCoupledThermoMechanics"); static STensor3 dummy; return dummy;};
    virtual void ElasticStiffness(STensor43 *elasticStiffness) const{Msg::Error("ElasticStiffness is not defined in mlawCoupledThermoMechanics");};

#endif //SWIG
};

#endif //MLAWCOUPLEDTHERMOMECHANICS_H_
