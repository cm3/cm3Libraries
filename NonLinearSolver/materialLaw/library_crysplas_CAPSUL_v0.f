C
c     CAPSUL: CrystAl Plasticity UtiLities tool set 
C     Multiscale Materials Modelling Group, IMDEA Materials
C
C     GENERAL SUBROUTINES FOR UMAT CRYSTAL PLASTICITY MODELS
c
c     Javier Segurado, last modification 15/01/2016


      SUBROUTINE STIFF6(c11,c12,c44,c13,c33,c66,stiff)

C     Generates the elastic stiffness matrix for an anisotropic crystal (i.e. HCP)
C     axis 3 is normal to isotropy plane (i.e. c-axis)
C     11-->1, 22-->2, 33-->3, 13-->4, 23-->5, 33-->6
      implicit none
      REAL*8 c11,c12,c44,c13,c33,c66
      REAL*8 stiff(3,3,3,3)
      integer ii,jj,kk,ll

      do,ii=1,3
         do,jj=1,3
            do,kk=1,3
               do,ll=1,3
                  STIFF(ii,jj,kk,ll)=0D0
               enddo
            enddo
         enddo
      enddo
              
      STIFF(1,1,1,1)=c11
      STIFF(2,2,2,2)=c11
      STIFF(3,3,3,3)=c33
c     
      STIFF(1,1,2,2)=c12
      STIFF(2,2,1,1)=c12
      STIFF(1,1,3,3)=c13
      STIFF(2,2,3,3)=c13
      STIFF(3,3,1,1)=c13
      STIFF(3,3,2,2)=c13
c     
      STIFF(1,2,1,2)=c66
      STIFF(2,1,2,1)=c66
      STIFF(1,2,2,1)=c66
      STIFF(2,1,1,2)=c66
c     
      STIFF(1,3,1,3)=c44
      STIFF(3,1,3,1)=c44
      STIFF(1,3,3,1)=c44
      STIFF(3,1,1,3)=c44
c     
      STIFF(2,3,2,3)=c44
      STIFF(3,2,3,2)=c44
      STIFF(2,3,3,2)=c44
      STIFF(3,2,2,3)=c44

      RETURN
      END

      SUBROUTINE STIFF4(c11,c12,c44,stiff)
      implicit none
      REAL*8 c11,c12,c44,stiff(3,3,3,3)
      integer ii,jj,kk,ll
      do,ii=1,3
         do,jj=1,3
            do,kk=1,3
               do,ll=1,3
                  STIFF(ii,jj,kk,ll)=0D0
               enddo
            enddo
         enddo
      enddo
                  
      STIFF(1,1,1,1)=c11
      STIFF(2,2,2,2)=c11
      STIFF(3,3,3,3)=c11
c     
      STIFF(1,1,2,2)=c12
      STIFF(1,1,3,3)=c12
      STIFF(2,2,1,1)=c12
      STIFF(2,2,3,3)=c12
      STIFF(3,3,1,1)=c12
      STIFF(3,3,2,2)=c12
c     
      STIFF(1,2,1,2)=c44
      STIFF(2,1,2,1)=c44
      STIFF(1,2,2,1)=c44
      STIFF(2,1,1,2)=c44
c     
      STIFF(1,3,1,3)=c44
      STIFF(3,1,3,1)=c44
      STIFF(1,3,3,1)=c44
      STIFF(3,1,1,3)=c44
c     
      STIFF(2,3,2,3)=c44
      STIFF(3,2,3,2)=c44
      STIFF(2,3,3,2)=c44
      STIFF(3,2,2,3)=c44
      RETURN
      END

      SUBROUTINE green_lagrange(FE,epsilon_el)
      implicit none
      real*8 FE(3,3),epsilon_el(3,3)
      real*8 FEt(3,3)
      integer ii,jj

      CALL TRANSPOSE(FEt,FE,3,3)
      CALL PMAT(epsilon_el,FEt,FE,3,3,3)
      
      do,ii=1,3
         do,jj=1,3
            epsilon_el(ii,jj)=epsilon_el(ii,jj)*.5d0
         enddo
         epsilon_el(ii,ii)=epsilon_el(ii,ii)-.5D0
      enddo
      return 
      end


      SUBROUTINE lagrangian_def(CC,epsilon_el)
      implicit none
      real*8 cc(3,3),epsilon_el(3,3)
      integer ii,jj
c     Lagrangian deformation

      do,ii=1,3
         do,jj=1,3
            epsilon_el(ii,jj)=cc(ii,jj)*.5d0
         enddo
         epsilon_el(ii,ii)=epsilon_el(ii,ii)-.5D0
      enddo
      RETURN 
      END


      FUNCTION secah(x)
      implicit none
      REAL*8 x,secah
       if(abs(x).GT.50)THEN
         secah=0d0
       else
          secah=2d0/(dexp(x)+dexp(-x))
       endif
      RETURN
      END



    
c *****************************************************************************
	subroutine euler (iopt,ph,th,tm,a)
c       
c     CALCULATE THE EULER ANGLES ASSOCIATED WITH THE TRANSFORMATION
c     MATRIX A(I,J) IF IOPT=1 AND VICEVERSA IF IOPT=2
c     A(i,j) TRANSFORMS FROM SYSTEM sa TO SYSTEM ca.
c     ph,th,om ARE THE EULER ANGLES (in degrees) OF ca REFERRED TO sa.
c     Copied from VPSC code (Lebensohn et at 1993)
c     *****************************************************************************
	real*8 ph,th,tm,a,pi,sph,cph,sth,cth,stm,ctm
	integer iopt
	dimension a(3,3)
	pi=4.*datan(1.d0)
	
	if(iopt.eq.1) then
	   if(abs(a(3,3)).ge.0.999999d0) then
              if(a(3,3).GT.0) th=0d0
              if(a(3,3).LT.0) th=180d0
	      tm=0d0
	      ph=datan2(a(1,2),a(1,1))
	   else
              th=dacos(a(3,3))
	      sth=dsin(th)
	      tm=datan2(a(1,3)/sth,a(2,3)/sth)
	      ph=datan2(a(3,1)/sth,-a(3,2)/sth)
	   endif
	   th=th*180d0/pi
	   ph=ph*180d0/pi
	   tm=tm*180d0/pi
	else if(iopt.eq.2) then
	   sph=dsin(ph*pi/180.)
	   cph=dcos(ph*pi/180.)
	   sth=dsin(th*pi/180.)
	   cth=dcos(th*pi/180.)
	   stm=dsin(tm*pi/180.)
	   ctm=dcos(tm*pi/180.)
	   a(1,1)=ctm*cph-sph*stm*cth
	   a(2,1)=-stm*cph-sph*ctm*cth
	   a(3,1)=sph*sth
	   a(1,2)=ctm*sph+cph*stm*cth
	   a(2,2)=-sph*stm+cph*ctm*cth
	   a(3,2)=-sth*cph
	   a(1,3)=sth*stm
	   a(2,3)=ctm*sth
	   a(3,3)=cth
	endif
	
	return
	end
	


c     SUBROUTINE to orientate a 4 order rank tensor using a rotation matrix

      SUBROUTINE ORIENTATE_TENSOR4(TENSOR_R,TENSOR,ROT)
      implicit none
      REAL*8 TENSOR_R(3,3,3,3),TENSOR(3,3,3,3),ROT(3,3)
      integer ii,jj,kk,ll,mm,nn,pp,qq
 
      
      do,ii=1,3
         do,jj=1,3
            do,kk=1,3
               do,ll=1,3
                  TENSOR_R(ii,jj,kk,ll)=0d0

                  do,mm=1,3
                     do,nn=1,3
                        do,pp=1,3
                           do,qq=1,3
                              TENSOR_R(ii,jj,kk,ll)=
     1                             TENSOR_R(ii,jj,kk,ll)+
     2                             ROT(mm,ii)*ROT(nn,jj)*
     3                             ROT(pp,kk)*ROT(qq,ll)*
     4                             TENSOR(mm,nn,pp,qq)
                           enddo
                        enddo
                     enddo
                  enddo

               enddo
            enddo
         enddo
      enddo

      RETURN 
      END
c

c
C     Polar decomposition by Simo 1991

      SUBROUTINE POLAR_DECOMP(UU,RR,CC,DEFGRAD)
      implicit none
      REAL*8 UU(3,3),UUINV(3,3),RR(3,3),DEFGRAD(3,3),DEFGRAD_T(3,3)
      REAL*8 CC(3,3),CC2(3,3),unit(3,3)
      REAL*8 I1,I2,I3,iu1,iu2,iu3
      REAL*8 x(3),lambda(3),lambda2(3)
      REAL*8 b,c,m,n,t,D,dd
      real*8 a,p,q,phi,temp1,y1,y2,y3,temp2,u,v,y2r,y2i
      INTEGER l,ii,jj,ndif
 
      real*8 pi


      PI=3.141592653589793238D0

      do,ii=1,3
         do,jj=1,3
            unit(ii,jj)=0d0
         enddo
         unit(ii,ii)=1d0
      enddo

c     Transpose F
      CALL TRANSPOSE(DEFGRAD_T,DEFGRAD,3,3)

c     CC=Ft*F
      CALL PMAT(CC,DEFGRAD_T,DEFGRAD,3,3,3)
c      write(*,*) 'cc',CC

C     Diagonalization of C (obtention of lambda(i))
      CALL INVARIANTS(I1,I2,I3,CC)

      b = I2 - I1*I1/3d0
      c = (-2d0/27d0)*I1*I1*I1 + I1*I2/3d0 - I3

       if(abs(b).LE.1D-15) then
         
         do,l=1,3
            x(l)=-c**(1D0/3d0) 
         enddo

c         write(*,*)'b<1e-12', x(1),x(2),x(3)
         
      else
         m  = 2d0*dsqrt(-b/3D0)
         n  = 3d0*c/(m*b)
         t  = datan2(dsqrt(abs(1d0-n*n)),n)/3D0
         do,l=1,3
            x(l) = m*dcos(t+2d0*(l-1)*PI/3d0)
         enddo
      endif

      do,l=1,3
         lambda(l)=dsqrt(x(l)+I1/3D0)
      enddo


c     Invariants of U

      iu1 = lambda(1) + lambda(2) + lambda(3)
      iu2 =  lambda(1)*lambda(2) + lambda(1)*lambda(3)
     1      +lambda(2)*lambda(3)
      iu3 = lambda(1) * lambda(2) * lambda(3)

      D= iu1*iu2 - iu3
      IF(ABS(D).le.1d-15) write(*,*)'ERROR D',D

      CALL PMAT(CC2,CC,CC,3,3,3)

      do,ii=1,3
         do,jj=1,3
            UU(ii,jj)=(1d0/D)*(-CC2(ii,jj)+(iu1*iu1-iu2)*CC(ii,jj)+
     1           iu1*iu3*unit(ii,jj))
            UUINV(ii,jj)=(1d0/iu3)*(CC(ii,jj)-iu1*UU(ii,jj)+
     1           iu2*unit(ii,jj))
         enddo
      enddo
      
C     ROTATION

      CALL PMAT(RR,DEFGRAD,UUINV,3,3,3)
            
c      write(*,*)'uu',uu
c      write(*,*)'rr',rr
      
      RETURN
      END


      SUBROUTINE DECOMP(Matrix,lambda,vec1,vec2,vec3)
      implicit none
      REAL*8 Matrix(3,3),lambda(3),vec1(3),vec2(3),vec3(3)
      REAL*8 I1,I2,I3
      real*8 a,d
      real*8 p,q,dd,phi,temp1,y1,y2,y3,temp2,u,v,y2r,y2i
      REAL*8 Matrix2(3,3)
      REAL*8 dnorm,det(3),detmax
      real*8 b,c,x(3),n,t,m,pi
      INTEGER ii,jj,ndif,nroot,i,ni,l



      PI=3.141592653589793238D0
C     Diagonalization of matrix (sym) (obtention of lambda(i))
      CALL INVARIANTS(I1,I2,I3,matrix)


c      write(*,*)'invariants',i1,i2,i3
      
      b = I2 - I1*I1/3.d0
      c = (-2/27.d0)*I1*I1*I1 + I1*I2/3.d0 - I3

c      write(*,*)'b,c',b,c

      if(abs(b).LE.1D-15) then
         
         do,l=1,3
            x(l)=-c**(1.D0/3.D0)
         enddo

         ndif=1
c         write(*,*) x(1),x(2),x(3)
         
      else
         
         m  = 2*dsqrt(-b/3.D0)
         n  = 3*c/(m*b)
         if(n.GT.1d0) THEN 
c            write(*,*)'RU error, SQRT() neg',1d0-n*n
            n=0d0
         endif
         t  = datan2(dsqrt(1d0-n*n),n)/3.D0
 
         if(abs(t).LE.1D-15) then
            ndif=2
         else
            ndif=3
         endif
c         write(*,*) m,n,t

         do,l=1,3
            x(l) = m*dcos(t+2*(l-1)*PI/3.d0)
         enddo

c         write(*,*) x(1),x(2),x(3)
         
      endif

      do,l=1,3
         lambda(l)=dsqrt(x(l)+I1/3D0)
      enddo

      write(*,*) 'lambdas',lambda(1),lambda(2),lambda(3)

      
c$$$
c$$$
c$$$
c$$$
c$$$
c$$$      CALL INVARIANTS(I1,I2,I3,Matrix)
c$$$
c$$$c      write(*,*)'invariants',i1,i2,i3
c$$$      
c$$$      a=1d0
c$$$      b=-i1
c$$$      c=i2
c$$$      d=-i3
c$$$c      PI=3.141592653589793238D0
c$$$      pi=4d0*datan(1.d0)
c$$$
c$$$c Step 1: Calculate p and q --------------------------------------------
c$$$      p  = c/a - b*b/a/a/3D0
c$$$      q  = (2D0*b*b*b/a/a/a - 9D0*b*c/a/a + 27D0*d/a) / 27D0
c$$$
c$$$c Step 2: Calculate DD (discriminant) ----------------------------------
c$$$      DD = p*p*p/27D0 + q*q/4D0
c$$$c      write(*,*)'DD',dd
c$$$
c$$$c Step 3: Branch to different algorithms based on DD -------------------
c$$$      if(DD.lt.0.and.abs(dd/i3).GT.1D-15) then
c$$$c       Step 3b:
c$$$c       3 real unequal roots -- use the trigonometric formulation
c$$$        phi = dacos(-q/2D0/dsqrt(dabs(p*p*p)/27D0))
c$$$        temp1=2D0*dsqrt(dabs(p)/3D0)
c$$$        y1 =  temp1*dcos(phi/3D0)
c$$$        y2 = -temp1*dcos((phi+pi)/3D0)
c$$$        y3 = -temp1*dcos((phi-pi)/3D0)
c$$$      else
c$$$        if(abs(DD/i3).LE.1D-15) DD=0d0
c$$$c       Step 3a:
c$$$c       1 real root & 2 conjugate complex roots OR 3 real roots (some are equal)
c$$$        temp1 = -q/2D0 + dsqrt(DD)
c$$$        temp2 = -q/2D0 - dsqrt(DD)
c$$$        u = dabs(temp1)**(1D0/3D0)
c$$$        v = dabs(temp2)**(1D0/3D0)
c$$$        if(temp1 .lt. 0D0) u=-u
c$$$        if(temp2 .lt. 0D0) v=-v
c$$$        y1  = u + v
c$$$        y2r = -(u+v)/2D0
c$$$        y2i =  (u-v)*dsqrt(3D0)/2D0
c$$$      endif
c$$$
c$$$c Step 4: Final transformation -----------------------------------------
c$$$      temp1 = b/a/3D0
c$$$      y1 = y1-temp1
c$$$      y2 = y2-temp1
c$$$      y3 = y3-temp1
c$$$      y2r=y2r-temp1
c$$$
c$$$c Assign answers and number of lambdas differents-------------------------------------------------------
c$$$      if(DD.lt.0.and.abs(dd/i3).GT.1D-15) then
c$$$
c$$$        lambda(1) = y1
c$$$        lambda(2) = y2
c$$$        lambda(3) = y3
c$$$        ndif=3
c$$$       
c$$$      elseif(abs(DD/i3).LE.1D-15) then
c$$$
c$$$         if(abs(y1-y2r).LE.1D-15) then
c$$$            ndif=1 
c$$$            lambda(1)=y1
c$$$            lambda(2)=y2r
c$$$            lambda(3)=y2r
c$$$         else
c$$$            ndif=2
c$$$            if(y1.GT.y2r) THEN
c$$$               lambda(1)=y1
c$$$               lambda(2)=y2r
c$$$               lambda(3)=y2r
c$$$            else
c$$$               lambda(1)=y2r
c$$$               lambda(2)=y2r
c$$$               lambda(3)=y1
c$$$            endif
c$$$
c$$$         endif
c$$$
c$$$      else
c$$$      
c$$$         write(*,*)'error:COMPLEx'
c$$$      
c$$$      endif 
c$$$
c$$$c      write(*,*)'NDIF',ndif
c$$$c      write(*,*)'LAMBDAS',lambda(1),lambda(2),lambda(3)
c$$$
C     EIGENVECTORS

C     three different
      IF(ndif.EQ.3) THEN

c     FIRST
      do,ii=1,3
         do,jj=1,3
            Matrix2(ii,jj)=Matrix(ii,jj)
            if(ii.EQ.jj) MATRIX2(ii,jj)=MATRIX2(ii,jj)-lambda(1)
         enddo
      enddo

      det(1)=Matrix2(1,1)*Matrix2(2,2)-Matrix2(1,2)*Matrix2(2,1)
      det(2)=Matrix2(2,2)*Matrix2(3,3)-Matrix2(2,3)*Matrix2(3,2)
      det(3)=Matrix2(1,1)*Matrix2(3,3)-Matrix2(1,3)*Matrix2(3,1)
      detmax=0d0
      do,i=1,3
         if(abs(det(i)).GT.detmax) THEN
            ni=i
            detmax=abs(det(i))
         endif
      enddo
c      write(*,*)'dets',det
      if(ni.EQ.1) THEN
c         write(*,*)'supuesto1',detmax
         a=(1/det(ni))*
     1        (-Matrix2(2,2)*Matrix2(1,3)+Matrix2(2,1)*Matrix2(2,3))
         b=(1/det(ni))*
     1        (Matrix2(1,2)*Matrix2(1,3)-Matrix2(1,1)*Matrix2(2,3))
         c=1d0
      elseif(ni.EQ.2) THEN  
c         write(*,*)'supuesto2',detmax
         a=1d0
         b=(1/det(ni))*
     1        (-Matrix2(3,3)*Matrix2(2,1)+Matrix2(3,2)*Matrix2(3,1))
         c=(1/det(ni))*
     1        (Matrix2(2,3)*Matrix2(2,1)-Matrix2(2,2)*Matrix2(3,1))
      else
c         write(*,*)'supuesto3',detmax 
         b=1d0
         a=(1/det(ni))*
     1        (-Matrix2(3,3)*Matrix2(1,2)+Matrix2(3,1)*Matrix2(2,3))
         c=(1/det(ni))*
     1        (Matrix2(1,3)*Matrix2(1,2)-Matrix2(1,1)*Matrix2(2,3))
      endif
      
      dnorm=dsqrt(a*a+b*b+c*c)
      vec1(1)=a/dnorm
      vec1(2)=b/dnorm
      vec1(3)=c/dnorm
c     SECOND

      do,ii=1,3
         do,jj=1,3
            Matrix2(ii,jj)=Matrix(ii,jj)
            if(ii.EQ.jj) MATRIX2(ii,jj)=MATRIX2(ii,jj)-lambda(2)
         enddo
      enddo
      detmax=0d0
      det(1)=Matrix2(1,1)*Matrix2(2,2)-Matrix2(1,2)*Matrix2(2,1)
      det(2)=Matrix2(2,2)*Matrix2(3,3)-Matrix2(2,3)*Matrix2(3,2)
      det(3)=Matrix2(1,1)*Matrix2(3,3)-Matrix2(1,3)*Matrix2(3,1)
c      write(*,*)'dets',det
      do,i=1,3
         if(abs(det(i)).GT.detmax) THEN
            ni=i
            detmax=abs(det(i))
         endif
      enddo
      if(ni.EQ.1) THEN
c         write(*,*)'supuesto1',detmax
         a=(1/det(ni))*
     1        (-Matrix2(2,2)*Matrix2(1,3)+Matrix2(2,1)*Matrix2(2,3))
         b=(1/det(ni))*
     1        (Matrix2(1,2)*Matrix2(1,3)-Matrix2(1,1)*Matrix2(2,3))
         c=1d0
      elseif(ni.EQ.2) THEN  
c         write(*,*)'supuesto2',detmax
         a=1d0
         b=(1/det(ni))*
     1        (-Matrix2(3,3)*Matrix2(2,1)+Matrix2(3,2)*Matrix2(3,1))
         c=(1/det(ni))*
     1        (Matrix2(2,3)*Matrix2(2,1)-Matrix2(2,2)*Matrix2(3,1))
      else
c         write(*,*)'supuesto3',detmax 
         b=1d0
         a=(1/det(ni))*
     1        (-Matrix2(3,3)*Matrix2(1,2)+Matrix2(3,1)*Matrix2(2,3))
         c=(1/det(ni))*
     1        (Matrix2(1,3)*Matrix2(1,2)-Matrix2(1,1)*Matrix2(2,3))
      endif
      
      dnorm=dsqrt(a*a+b*b+c*c)
      vec2(1)=a/dnorm
      vec2(2)=b/dnorm
      vec2(3)=c/dnorm
         
c     THIRD
      CALL PVECT(vec3,vec1,vec2)
c      write(*,*)'v1:',vec1
c      write(*,*)'v2:',vec2
c      write(*,*)'v3:',vec3
      
C     TWO DIFFERENT EIGENVALUES
      ELSE IF(ndif.EQ.2) THEN

         if(y2r.GT.y1) then
            do,ii=1,3
               do,jj=1,3
                  Matrix2(ii,jj)=Matrix(ii,jj)
                  if(ii.EQ.jj) MATRIX2(ii,jj)=MATRIX2(ii,jj)-lambda(3)
               enddo
            enddo

         else
            
            do,ii=1,3
               do,jj=1,3
                  Matrix2(ii,jj)=Matrix(ii,jj)
                  if(ii.EQ.jj) MATRIX2(ii,jj)=MATRIX2(ii,jj)-lambda(1)
               enddo
            enddo

         endif

         detmax=0d0
         det(1)=Matrix2(1,1)*Matrix2(2,2)-Matrix2(1,2)*Matrix2(2,1)
         det(2)=Matrix2(2,2)*Matrix2(3,3)-Matrix2(2,3)*Matrix2(3,2)
         det(3)=Matrix2(1,1)*Matrix2(3,3)-Matrix2(1,3)*Matrix2(3,1)
         do,i=1,3
            if(abs(det(i)).GT.detmax) THEN
               ni=i
               detmax=abs(det(i))
            endif
         enddo
         if(ni.EQ.1) THEN
c            write(*,*)'supuesto1',detmax
            a=(1/det(ni))*
     1           (-Matrix2(2,2)*Matrix2(1,3)+Matrix2(2,1)*Matrix2(2,3))
            b=(1/det(ni))*
     1           (Matrix2(1,2)*Matrix2(1,3)-Matrix2(1,1)*Matrix2(2,3))
            c=1d0
         elseif(ni.EQ.2) THEN  
c            write(*,*)'supuesto2',detmax
            a=1d0
            b=(1/det(ni))*
     1           (-Matrix2(3,3)*Matrix2(2,1)+Matrix2(3,2)*Matrix2(3,1))
            c=(1/det(ni))*
     1           (Matrix2(2,3)*Matrix2(2,1)-Matrix2(2,2)*Matrix2(3,1))
         else
c            write(*,*)'supuesto3',detmax 
            b=1d0
            a=(1/det(ni))*
     1           (-Matrix2(3,3)*Matrix2(1,2)+Matrix2(3,1)*Matrix2(2,3))
            c=(1/det(ni))*
     1           (Matrix2(1,3)*Matrix2(1,2)-Matrix2(1,1)*Matrix2(2,3))
         endif
         
         dnorm=dsqrt(a*a+b*b+c*c)

         if(y2r.GT.y1) then
            vec3(1)=a/dnorm
            vec3(2)=b/dnorm
            vec3(3)=c/dnorm
            if(abs(vec3(3)).GT.1D-15) THEN
               
               vec1(1)=-vec3(3)/dsqrt(vec3(1)**2+vec3(3)**2)
               vec1(2)=0d0       
               vec1(3)=vec3(1)/dsqrt(vec3(1)**2+vec3(3)**2)
               
            else 
               vec1(1)=0d0
               vec1(2)=0d0
               vec1(3)=1d0
            endif
            call pvect(vec2,vec3,vec1)
           

         else

            vec1(1)=a/dnorm
            vec1(2)=b/dnorm
            vec1(3)=c/dnorm
            if(abs(vec1(3)).GT.1D-15) THEN
               vec2(1)=-vec1(3)/dsqrt(vec1(1)**2+vec1(3)**2)
               vec2(2)=0d0
               vec2(3)=vec1(1)/dsqrt(vec1(1)**2+vec1(3)**2)
            else 
               vec2(1)=0d0
               vec2(2)=0d0
               vec2(3)=1d0
            endif
            call pvect(vec3,vec1,vec2)
           

            
         endif
         
c     lambda1=lambda2=lambda3
      ELSE
         vec1(1)=1d0
         vec1(2)=0d0
         vec1(3)=0d0
         vec2(1)=0d0
         vec2(2)=1d0
         vec2(3)=0d0
         vec3(1)=0d0
         vec3(2)=0d0
         vec3(3)=1d0
         
      ENDIF
      
      RETURN 
      END
      

      SUBROUTINE LOGtens(matrix,lambda,vec1,vec2,vec3)
      REAL*8 matrix(3,3),lambda(3),vec1(3),vec2(3),vec3(3)
      REAL*8 TENS1(3,3),TENS2(3,3),TENS3(3,3)
      do,ii=1,3
         lambda(ii)=log(lambda(ii))
      enddo
      CALL PTENS(TENS1,vec1,vec1)
      CALL PTENS(TENS2,vec2,vec2)
      CALL PTENS(TENS3,vec3,vec3)
      do,ii=1,3
         do,jj=1,3
            matrix(ii,jj)=lambda(1)*TENS1(ii,jj)+
     1           lambda(2)*TENS2(ii,jj)+
     1           lambda(3)*TENS3(ii,jj)
         enddo
      enddo
      RETURN
      END

      SUBROUTINE INVARIANTS(I1,I2,I3,CC)
c     Calculate invariants of a symmetric matrix
      implicit none
      REAL*8 I1,I2,I3
      REAL*8 CC(3,3),CC2(3,3)
      real*8 trac2
  

      I1=CC(1,1)+CC(2,2)+CC(3,3)
      
      CALL PMAT(CC2,CC,CC,3,3,3)
      
      trac2=CC2(1,1)+CC2(2,2)+CC2(3,3)
      
      I2=0.5d0*(I1*I1-trac2)

      I3=CC(1,1)*CC(2,2)*CC(3,3)+CC(1,2)*CC(2,3)*CC(3,1)
     1  +CC(1,3)*CC(2,1)*CC(3,2)
     2  -CC(1,3)*CC(2,2)*CC(3,1)-CC(2,3)*CC(3,2)*CC(1,1)
     3  -CC(3,3)*CC(1,2)*CC(2,1)

      RETURN
      END

      SUBROUTINE PMAT(matout,mat1,mat2,id,jd,kd)
C     Matrix product
      REAL*8 MAT1,MAT2,MATOUT
      DIMENSION mat1(id,kd),mat2(kd,jd),matout(id,jd)
      do,i=1,id
         Do,j=1,jd
            matout(i,j)=0D0
            do,k=1,kd
               matout(i,j)=matout(i,j)+mat1(i,k)*mat2(k,j)
            enddo
         enddo
      enddo
      RETURN
      END
c
      SUBROUTINE PTENS(matout,vectors1,vectors2)
c     Tensorial product of 2 vectors
       integer i,j
       real*8 vectors1(3),vectors2(3),matout(3,3)
       do,i=1,3
          do,j=1,3
             matout(i,j)=vectors1(i)*vectors2(j)
          enddo
       enddo
       RETURN
       END
      
      SUBROUTINE PTENS2(matout4,tens1,tens2)
      integer i,j,k,l
      real*8 tens1(3,3),tens2(3,3),matout4(3,3,3,3)
      do,i=1,3
         do,j=1,3
            do,k=1,3
               do,l=1,3
                  matout4(i,j,k,l)=tens1(i,j)*tens2(k,l)
               enddo
            enddo
         enddo
      enddo
      return
      end
      
      SUBROUTINE TRANSPOSE(Mat2,Mat1,ifil,icol)
      REAL*8 MAT1,MAT2
      DIMENSION MAT1(ifil,icol),MAT2(icol,ifil)
      DO,i=1,ifil
         do,j=1,icol
            MAT2(j,i)=0D0
            MAT2(j,i)=MAT1(i,j)
C     write(8,*) j,i,'-->',i,j,MAT2(i,j)
         enddo
      enddo
      RETURN
      END

      SUBROUTINE PCONTRACT(pcont,MAT1,MAT2,n)
      REAL*8 MAT1(n,n),MAT2(n,n),pcont
      INTEGER n,ii,jj
      pcont=0d0
      do,ii=1,n
         do,jj=1,n
            pcont=pcont+MAT1(ii,jj)*MAT2(ii,jj)
         enddo
      enddo
      RETURN 
      END

      SUBROUTINE PCONTRACT2(pcont,MAT1,MAT2,n)
      REAL*8 MAT1(n,n,n,n),MAT2(n,n),pcont(n,n)
      INTEGER n,ii,jj,kk,ll
      do,ii=1,n
         do,jj=1,n
            pcont(ii,jj)=0d0
            do,kk=1,n
               do,ll=1,n
                  pcont(ii,jj)=pcont(ii,jj)+MAT1(ii,jj,kk,ll)*
     1                 MAT2(kk,ll)
               enddo
            enddo
         enddo
      enddo


      RETURN 
      END


      SUBROUTINE PVECT(vecout,vectors1,vectors2)
C     Vectorial product
      REAL*8 vectors1,vectors2,vecout
      DIMENSION vectors1(3),vectors2(3),vecout(3)
      Dimension ii(3,2)
      ii(1,1)=2
      ii(1,2)=3
      ii(2,1)=1
      ii(2,2)=3
      ii(3,1)=1
      ii(3,2)=2
      Do,i=1,3
         vecout(i)=0d0
         vecout(i)=(-1)**(i+1)*( vectors1(ii(i,1))*vectors2(ii(i,2))-
     1   vectors2(ii(i,1))*vectors1(ii(i,2)) )
      ENDDO

      RETURN
      END

c
      SUBROUTINE MATINV3(CC2,CC,iflag)
      REAL*8 CC(3,3),CC2(3,3)
      REAL*8 I3
      integer iflag

      I3=CC(1,1)*CC(2,2)*CC(3,3)+CC(1,2)*CC(2,3)*CC(3,1)
     1     +CC(1,3)*CC(2,1)*CC(3,2)
     2     -CC(1,3)*CC(2,2)*CC(3,1)-CC(2,3)*CC(3,2)*CC(1,1)
     3     -CC(3,3)*CC(1,2)*CC(2,1)

      if(abs(I3).GT.1D-50) THEN
         iflag=0
         I3=1D0/I3
      else
         iflag=1
         return
      endif

      CC2(1,1)=I3*(cc(2,2)*cc(3,3)-cc(2,3)*cc(3,2))
      CC2(2,2)=I3*(cc(1,1)*cc(3,3)-cc(1,3)*cc(3,1))
      CC2(3,3)=I3*(cc(1,1)*cc(2,2)-cc(1,2)*cc(2,1))

      CC2(1,2)=-I3*(cc(1,2)*cc(3,3)-cc(3,2)*cc(1,3))
      CC2(2,1)=-I3*(cc(2,1)*cc(3,3)-cc(2,3)*cc(3,1))

      CC2(1,3)=I3*(cc(1,2)*cc(2,3)-cc(1,3)*cc(2,2))
      CC2(3,1)=I3*(cc(2,1)*cc(3,2)-cc(2,2)*cc(3,1))

      CC2(2,3)=-I3*(cc(1,1)*cc(2,3)-cc(1,3)*cc(2,1))
      CC2(3,2)=-I3*(cc(1,1)*cc(3,2)-cc(1,2)*cc(3,1))

      RETURN 
      END


      SUBROUTINE EXPONENTIAL3(EXPO,MATRIX)
      REAL*8 EXPO(3,3),MATRIX(3,3)
      REAL*8 AUX33(3,3),MATRIXn(3,3)
      REAL*8 dnorm,toler
      toler=1D-5
      do,ii=1,3
         do,jj=1,3
            EXPO(ii,jj)=0d0
            MATRIXn(ii,jj)=0d0
         enddo
         EXPO(ii,ii)=1d0 
         MATRIXn(ii,ii)=1d0
      enddo
      n=0
      nfact=1
      dnorm=1D0
      DO 100 WHILE(dnorm.GE.toler)
         n=n+1
         nfact=nfact*n
         CALL PMAT(AUX33,MATRIX,MATRIXn,3,3,3)
         do,ii=1,3
            do,jj=1,3
               MATRIXn(ii,jj)=AUX33(ii,jj)
               EXPO(ii,jj)=EXPO(ii,jj)+(1D0/nfact)*MATRIXn(ii,jj)
            enddo
         enddo

         dnorm=0d0
         do,ii=1,3
            do,jj=1,3
               dnorm=dnorm+MATRIXn(ii,jj)*MATRIXn(ii,jj)
            enddo
         enddo
         dnorm=dsqrt(dnorm)/nfact
c         write(*,*) dnorm
 100  CONTINUE
      RETURN
 102  FORMAT(3(3(E16.4,' '),/))
      
      END

      SUBROUTINE EXPMAP
     1     (   EXPX       ,NOCONV     ,X          )
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      PARAMETER
     1     (   NDIM=3     ,NDIM2=9    )
C     Arguments
      LOGICAL  NOCONV
      DIMENSION
     1     EXPX(NDIM,NDIM)           ,X(NDIM,NDIM)
C     Local arrays and variables
      DIMENSION
     1     XN(NDIM,NDIM)     ,XNM1(NDIM,NDIM)     ,XNM2(NDIM,NDIM)     ,
     2     XNM3(NDIM,NDIM)   ,X2(NDIM,NDIM)
      DATA
     1     R0   ,RP5  ,R1   ,R2   ,TOL    ,OVER    ,UNDER   /
     2     0.0D0,0.5D0,1.0D0,2.0D0,1.0D-5,1.0D+100,1.0D-100/
      DATA
     1     NMAX / 100 /
C***********************************************************************
C     COMPUTES THE EXPONENTIAL OF A (GENERALLY UNSYMMETRIC) 3-D TENSOR. USES
C     THE SERIES REPRESENTATION OF THE TENSOR EXPONENTIAL
C     
C     REFERENCE: Section B.1
C     Box B.1
C***********************************************************************
C     Initialise series convergence flag
      NOCONV=.FALSE.
C     Compute X square
      CALL RVZERO(X2,NDIM2)
      DO 30 I=1,NDIM
         DO 20 J=1,NDIM
            DO 10 K=1,NDIM
               X2(I,J)= X2(I,J)+X(I,K)*X(K,J)
 10         CONTINUE
 20      CONTINUE
 30   CONTINUE
C     Compute principal invariants of X
      C1=X(1,1)+X(2,2)+X(3,3)
      C2=RP5*(C1*C1-(X2(1,1)+X2(2,2)+X2(3,3)))
      C3=X(1,1)*X(2,2)*X(3,3)+X(1,2)*X(2,3)*X(3,1)+
     1     X(1,3)*X(2,1)*X(3,2)-X(1,2)*X(2,1)*X(3,3)-
     2     X(1,1)*X(2,3)*X(3,2)-X(1,3)*X(2,2)*X(3,1)
C     Start computation of exponential using its series definition
C     ============================================================
      DO 50 I=1,NDIM
         DO 40 J=1,NDIM
            XNM1(I,J)=X2(I,J)
            XNM2(I,J)=X(I,J)
 40      CONTINUE
 50   CONTINUE
      XNM3(1,1)=R1
      XNM3(1,2)=R0
      XNM3(1,3)=R0
      XNM3(2,1)=R0
      XNM3(2,2)=R1
      XNM3(2,3)=R0
      XNM3(3,1)=R0
      XNM3(3,2)=R0
      XNM3(3,3)=R1
C     Add first three terms of series
C     -------------------------------
      DO 70 I=1,NDIM
         DO 60 J=1,NDIM
            EXPX(I,J)=RP5*XNM1(I,J)+XNM2(I,J)+XNM3(I,J)
 60      CONTINUE
 70   CONTINUE
C     Add remaining terms (with X to the powers 3 to NMAX)
C     ----------------------------------------------------
      FACTOR=R2
      DO 140 N=3,NMAX
C     Use recursive formula to obtain X to the power N
         DO 90 I=1,NDIM
            DO 80 J=1,NDIM
               XN(I,J)=C1*XNM1(I,J)-C2*XNM2(I,J)+C3*XNM3(I,J)
 80         CONTINUE
 90      CONTINUE
C     Update factorial
         FACTOR=DBLE(N)*FACTOR
         R1DFAC=R1/FACTOR
C     Add Nth term of the series
         DO 110 I=1,NDIM
            DO 100 J=1,NDIM
               EXPX(I,J)=EXPX(I,J)+R1DFAC*XN(I,J)
 100        CONTINUE
 110     CONTINUE
C     Check convergence of series
         XNNORM=SQRT(SCAPRD(XN(1,1),XN(1,1),NDIM2))
         IF(XNNORM.GT.OVER.OR.(XNNORM.LT.UNDER.AND.XNNORM.GT.R0)
     1        .OR.R1DFAC.LT.UNDER)THEN
C...  first check possibility of overflow or underflow.
C...  numbers are to small or too big: Break (unconverged) loop and exit
            NOCONV=.TRUE.
            GOTO 999
         ELSEIF(XNNORM*R1DFAC.LT.TOL)THEN
C...  converged: Break series summation loop and exit with success
            GOTO 999
         ENDIF 
         DO 130 I=1,NDIM
            DO 120 J=1,NDIM
               XNM3(I,J)=XNM2(I,J)
               XNM2(I,J)=XNM1(I,J)
               XNM1(I,J)=XN(I,J)
 120        CONTINUE
 130     CONTINUE
 140  CONTINUE
C     Re-set convergence flag if series did not converge
      NOCONV=.TRUE.
C     
 999  CONTINUE
      RETURN
      END



      SUBROUTINE DEXPMP
     1(   DEXPX      ,NOCONV     ,X          )
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      PARAMETER
     1(   NDIM=3     ,NDIM2=9    ,NDIM4=81   ,MAXN=100   )
C Arguments
      LOGICAL  NOCONV
      DIMENSION
     1    DEXPX(NDIM,NDIM,NDIM,NDIM),X(NDIM,NDIM)
C Local arrays and variables
C...matrix of powers of X
      DIMENSION
     1    R1DFAC(MAXN)       ,XMATX(NDIM,NDIM,0:MAXN)
C...initialise identity matrix: X to the power 0
      DATA
     1    XMATX(1,1,0)  ,XMATX(1,2,0)  ,XMATX(1,3,0)  /
     2    1.D0          ,0.D0          ,0.D0          /
     3    XMATX(2,1,0)  ,XMATX(2,2,0)  ,XMATX(2,3,0)  /
     4    0.D0          ,1.D0          ,0.D0          /
     5    XMATX(3,1,0)  ,XMATX(3,2,0)  ,XMATX(3,3,0)  /
     6    0.D0          ,0.D0          ,1.D0          /
      DATA
     1    R0   ,RP5  ,R1   ,TOL    ,OVER    ,UNDER   /
     2    0.0D0,0.5D0,1.0D0,1.0D-10,1.0D+100,1.0D-100/
C***********************************************************************
C COMPUTES THE DERIVATIVE OF THE EXPONENTIAL OF A (GENERALLY
C UNSYMMETRIC) 3-D TENSOR. USES THE SERIES REPRESENTATION OF THE TENSOR
C EXPONENTIAL.
C
C REFERENCE: Section B.2
C            Box B.2
C***********************************************************************
C Initialise convergence flag
      NOCONV=.FALSE.
C X to the power 1
      DO 20 I=1,NDIM
        DO 10 J=1,NDIM
          XMATX(I,J,1)=X(I,J)
   10   CONTINUE
   20 CONTINUE
C Zero remaining powers of X
      CALL RVZERO(XMATX(1,1,2),NDIM*NDIM*(MAXN-1))
C Compute X square
      DO 50 I=1,NDIM
        DO 40 J=1,NDIM
          DO 30 K=1,NDIM
            XMATX(I,J,2)=XMATX(I,J,2)+X(I,K)*X(K,J)
   30     CONTINUE
   40   CONTINUE
   50 CONTINUE
C Compute principal invariants of X
      C1=X(1,1)+X(2,2)+X(3,3)
      C2=RP5*(C1*C1-(XMATX(1,1,2)+XMATX(2,2,2)+XMATX(3,3,2)))
      C3=X(1,1)*X(2,2)*X(3,3)+X(1,2)*X(2,3)*X(3,1)+
     1   X(1,3)*X(2,1)*X(3,2)-X(1,2)*X(2,1)*X(3,3)-
     2   X(1,1)*X(2,3)*X(3,2)-X(1,3)*X(2,2)*X(3,1)
C Compute X to the powers 3,4,...,NMAX using recursive formula
      R1DFAC(1)=R1
      R1DFAC(2)=RP5
      DO 80 N=3,MAXN
        R1DFAC(N)=R1DFAC(N-1)/DBLE(N)
        DO 70 I=1,NDIM
          DO 60 J=1,NDIM
            XMATX(I,J,N)=C1*XMATX(I,J,N-1)-C2*XMATX(I,J,N-2)+
     1                   C3*XMATX(I,J,N-3)
   60     CONTINUE
   70   CONTINUE
        XNNORM=SQRT(SCAPRD(XMATX(1,1,N),XMATX(1,1,N),NDIM2))
C...check number of terms required for series convergence
        IF(XNNORM.GT.OVER.OR.(XNNORM.LT.UNDER.AND.XNNORM.GT.R0)
     1                                  .OR.R1DFAC(N).LT.UNDER)THEN
C...numbers are to small or too big: Exit without computing derivative
          NOCONV=.TRUE.
          GOTO 999
        ELSEIF(XNNORM*R1DFAC(N).LT.TOL)THEN
C...series will converge with NMAX terms:
C   Carry on to derivative computation
          NMAX=N
          GOTO 90
        ENDIF
   80 CONTINUE
C...series will not converge for the currently prescribed tolerance
C   with the currently prescribed maximum number of terms MAXN:
C   Exit without computing derivative
      NOCONV=.TRUE.
      GOTO 999
   90 CONTINUE
C Compute the derivative of exponential map
      CALL RVZERO(DEXPX,NDIM4)
      DO 150 I=1,NDIM
        DO 140 J=1,NDIM
          DO 130 K=1,NDIM
            DO 120 L=1,NDIM
              DO 110 N=1,NMAX
                DO 100 M=1,N
                  DEXPX(I,J,K,L)=DEXPX(I,J,K,L)+
     1                           R1DFAC(N)*XMATX(I,K,M-1)*XMATX(L,J,N-M)
  100           CONTINUE
  110         CONTINUE
  120       CONTINUE
  130     CONTINUE
  140   CONTINUE
  150 CONTINUE
C
  999 CONTINUE
      RETURN
      END


      

      SUBROUTINE RVZERO
     1     (   V          ,N          )
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DIMENSION V(N)
      DATA R0/0.0D0/
C***********************************************************************
C     INITIALISES TO ZERO A DOUBLE PRECISION ARRAY OF DIMENSION N
C***********************************************************************
      DO 10 I=1,N
         V(I)=R0
 10   CONTINUE
      RETURN
      END


      DOUBLE PRECISION FUNCTION SCAPRD(U  ,V  ,N  ) 
C
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DIMENSION U(N), V(N)
      DATA  R0  / 0.0D0 /
C***********************************************************************
C SCALAR PRODUCT OF DOUBLE PRECISION VECTORS U AND V OF DIMENSION N
C***********************************************************************
      SCAPRD=R0
      DO 10 I=1,N
        SCAPRD=SCAPRD+U(I)*V(I)
  10  CONTINUE
      RETURN
      END
 



      SUBROUTINE norm(dnorm,MAT,m,n)
      REAL*8 MAT(m,n),dnorm
      integer ii,jj,m,n

      dnorm=0d0
      DO,ii=1,m
         do,jj=1,n
            dnorm=dnorm+MAT(ii,jj)*MAT(ii,jj)
         enddo
      enddo
      dnorm=dsqrt(dnorm)
      RETURN 
      END

      subroutine TENS3333_sym(A,A66)
      real*8 A(3,3,3,3)
      real*8 a66(6,6)
      integer indexi(6),indexj(6),iflag
      integer indexii(3,3)
      integer ii,jj
  

      do,ii=1,3
         indexi(ii)=ii
         indexj(ii)=ii
      enddo
      
      indexi(4)=1
      indexj(4)=2
      indexi(5)=1
      indexj(5)=3
      indexi(6)=2
      indexj(6)=3
      indexii(1,1)=1
      indexii(2,2)=2
      indexii(3,3)=3
      indexii(1,2)=4
      indexii(1,3)=5
      indexii(2,3)=6
      indexii(2,1)=4
      indexii(3,1)=5
      indexii(3,2)=6
     
      do,ii=1,6
         do,jj=1,6

            a66(ii,jj) = A( indexi(ii),indexj(ii),indexi(jj),indexj(jj))
         
         enddo        
      enddo

      RETURN 
 100  FORMAT( 6(E16.8,' ')) 
      END
      


      subroutine TENS3333(A,A99)
      real*8 A(3,3,3,3)
      real*8 a99(9,9)
      integer indexi(9),indexj(9),iflag
      integer indexii(3,3)
      
       do,ii=1,3
         indexi(ii)=ii
         indexj(ii)=ii
      enddo
      
      indexi(4)=1
      indexj(4)=2
      indexi(5)=1
      indexj(5)=3
      indexi(6)=2
      indexj(6)=3
      indexi(7)=2
      indexj(7)=1
      indexi(8)=3
      indexj(8)=1
      indexi(9)=3
      indexj(9)=2
      
      indexii(1,1)=1
      indexii(2,2)=2
      indexii(3,3)=3
      indexii(1,2)=4
      indexii(2,1)=7
      indexii(1,3)=5
      indexii(3,1)=8
      indexii(2,3)=6
      indexii(3,2)=9
      do,ii=1,9
         do,jj=1,9
            a99(ii,jj)=A(indexi(ii),indexj(ii),indexi(jj),indexj(jj))
         enddo
      enddo

      return
 100  FORMAT( 9(E16.8,' ')) 
      end
      
      integer function kroneker(i,j)
      integer i,j
      kroneker=0
      if(i.EQ.j) kroneker=1
      return 
      end


      SUBROUTINE ROTATE_TENS3(AROT,A,R)
c     INPUTS: A (second order tensor, in conf 2, i.e. crystal axes), R (rot matrix from 1 to 2)
C     OUTPUT: AROT (second order tensor, in conf 1, i.e. XYZ)
C     AROT=RT  A  R !!!
C     
c     A given in system ei
C     ei*=Aei
C     Arot=R^T A R
      REAL*8 A(3,3),R(3,3),AROT(3,3)
      REAL*8 aux33(3,3)
      
      do,i=1,3
         do,j=1,3
            aux33(i,j)=0d0
            do,k=1,3
               aux33(i,j)=R(k,i)*A(k,j)+aux33(i,j)
            enddo
         enddo
      enddo
      do,i=1,3
         do,j=1,3
            arot(i,j)=0d0
            do,k=1,3
               arot(i,j)=aux33(i,k)*R(k,j)+arot(i,j)
            enddo
         enddo
      enddo
      return
      end

      Subroutine LUDCMP_IMDEACP(A,N,INDX,D,CODE)
      PARAMETER(NMAX=100,TINY=1d-16)
      REAL*8  AMAX,DUM, SUM, A(N,N),VV(NMAX)
      INTEGER CODE, D, INDX(N)
      
      D=1; CODE=0

      DO I=1,N
         AMAX=0.d0
         DO J=1,N
            IF (DABS(A(I,J)).GT.AMAX) AMAX=DABS(A(I,J))
         END DO                 ! j loop
         IF(AMAX.LT.TINY) THEN
            CODE = 1
            RETURN
         END IF
         VV(I) = 1.d0 / AMAX
      END DO                    ! i loop
      
      DO J=1,N
         DO I=1,J-1
            SUM = A(I,J)
            DO K=1,I-1
               SUM = SUM - A(I,K)*A(K,J) 
            END DO              ! k loop
            A(I,J) = SUM
         END DO                 ! i loop
         AMAX = 0.d0
         DO I=J,N
            SUM = A(I,J)
            DO K=1,J-1
               SUM = SUM - A(I,K)*A(K,J) 
            END DO              ! k loop
            A(I,J) = SUM
            DUM = VV(I)*DABS(SUM)
            IF(DUM.GE.AMAX) THEN
               IMAX = I
               AMAX = DUM
            END IF
         END DO                 ! i loop  
         
         IF(J.NE.IMAX) THEN
            DO K=1,N
               DUM = A(IMAX,K)
               A(IMAX,K) = A(J,K)
               A(J,K) = DUM
            END DO              ! k loop
            D  = -D
            VV(IMAX) = VV(J)
         END IF
         
         INDX(J) = IMAX
         IF(DABS(A(J,J)) < TINY) A(J,J) = TINY
         
         IF(J.NE.N) THEN
            DUM = 1.d0 / A(J,J)
            DO I=J+1,N
               A(I,J) = A(I,J)*DUM
            END DO              ! i loop
         END IF 
      END DO                    ! j loop
      
      RETURN
      END subroutine LUDCMP_IMDEACP

c
      SUBROUTINE gauss_3(AA,BB,xx,nmax,n,iflag)
      integer iflag,d
      real*8 AA(nmax,nmax),bb(nmax),xx(nmax)
      real*8 A(n,n),b(n)
      integer n,nmax,i,j,indx(n)
      det=0.
     
      do,i=1,n
         b(i)=bb(i)
         do,j=1,n
            A(i,j)=AA(i,j)
         enddo
      enddo
      call ludcmp_IMDEACP(a,n,indx,d,iflag)
      call LUBKSB_IMDEACP(A,N,INDX,B)
      do,i=1,n
         xx(i)=b(i)
      enddo
      return
      end

      Subroutine LUBKSB_IMDEACP(A,N,INDX,B)
      REAL*8  SUM, A(N,N),B(N)
      INTEGER INDX(N)
      
      II = 0     

      DO I=1,N
         LL = INDX(I)
         SUM = B(LL)
         B(LL) = B(I)
         IF(II.NE.0) THEN
            DO J=II,I-1
               SUM = SUM - A(I,J)*B(J)
            END DO              ! j loop
         ELSE IF(SUM.NE.0.d0) THEN
            II = I
         END IF
         B(I) = SUM
      END DO                    ! i loop
      
      DO I=N,1,-1
         SUM = B(I)
         IF(I < N) THEN
            DO J=I+1,N
               SUM = SUM - A(I,J)*B(J)
            END DO              ! j loop
         END IF
         B(I) = SUM / A(I,I)
      END DO                    ! i loop
          

      RETURN
      END subroutine LUBKSB_IMDEACP


! end of file lu.f90


      subroutine dzeros_vec(A,i)
      real*8 A(i)
      integer ii,i
      do,ii=1,i
         A(ii)=0d0
      enddo
      return 
      end

      subroutine dzeros(A,i,j)
      real*8 A(i,j)
      integer i,j,ii,jj
      do,ii=1,i
         do,jj=1,j
            A(ii,jj)=0d0
         enddo
      enddo
      return
      end

      subroutine dzeros_tensor4(A,i,j,k,l)
      real*8 A(i,j,k,l)
      integer i,j,ii,jj
      do,ii=1,i
         do,jj=1,j
            do,kk=1,k
               do,ll=1,l
                  A(ii,jj,kk,ll)=0d0
               enddo
            enddo
         enddo
      enddo
      return
      end

      subroutine dunit(A,i,j)
      real*8 A(i,j)
      integer i,j,ii,jj
       do,ii=1,i
         do,jj=1,j
            A(ii,jj)=0d0
         enddo
         A(ii,ii)=1d0
      enddo
      return
      end


      subroutine min_location(min_value,min_loc,input_array,n)
      implicit none

      integer ii, n, min_loc
      real*8 min_value, input_array(n)

      min_value=input_array(1)
      min_loc=1

      do, ii=2,n
       if(min_value.gt.input_array(ii)) then
       min_value=input_array(ii)
       min_loc=ii
       endif
      enddo

      return
      end

      SUBROUTINE paralelization(init,init1,noel,numprocess,noelprocess)
      implicit NONE
      logical init(10),init1(10)
      INTEGER NUMPROCESS
      integer i,noel,noelprocess(10)
c      save init,init1
      numprocess=1
c      write(*,*)' numprocess,',numprocess
      DO,I=0,7
         if(.not. init(numprocess+1).and.numprocess==I) THEN
            noelprocess(numprocess+1)=noel
            write(*,*)'noel cluster node number and element',I,noel
            init(numprocess+1)=.true.
         endif
      enddo
      
      do,i=0,7
         if(.not. init1(numprocess+1).and.numprocess==I)  THEN
            if((noel.NE.noelprocess(numprocess+1))) then
               write(*,*)'noel waiting node number and element ',noel
               call sleep(1)
            endif
         endif
      enddo
      return
      end

      SUBROUTINE form_orient_MAT(props,orient_MAT,orient_MAT_tr)
      implicit none
      integer i
      real*8 props(*)
      real*8 orient1(3),orient2(3),orient3(3),dnorm
      real*8 orient_MAT(3,3),orient_mat_tr(3,3)
      
c      write(*,*)'I am in lybrary_crysplas_CAPSUL cm3Libraries--------'
c      orient1(1)=props(1)
c      orient1(2)=props(2)
c      orient1(3)=props(3)
      orient1(1)=0.494651
      orient1(2)=0.869092
      orient1(3)=0.000000   
c      write(*,*)'orient',orient1
      call norm(dnorm,orient1,3,1)
c      write(*,*)'dnorm',dnorm
      do,i=1,3
         orient1(i)=orient1(i)/dnorm
      enddo
c      write(*,*)'orient after dnorm',orient1
c      orient2(1)=props(4)
c      orient2(2)=props(5)
c      orient2(3)=props(6)
      orient2(1)=-0.869092
      orient2(2)=0.494651
      orient2(3)=0.000000
      call norm(dnorm,orient2,3,1)
      do,i=1,3
         orient2(i)=orient2(i)/dnorm
      enddo
      CALL pvect(orient3,orient1,orient2)
      call norm(dnorm,orient3,3,1)
      do,i=1,3
         orient3(i)=orient3(i)/dnorm
      enddo
      do,i=1,3
         orient_MAT(i,1)=orient1(i) ! Orient_MAT is the rotation matrix from crystal axis to reference configuration
         orient_MAT(i,2)=orient2(i) !
         orient_MAT(i,3)=orient3(i) ! Orient_MAT  u_crys= u_cart
      enddo

      call transpose(orient_MAT_tr,orient_mat,3,3)
c      write(*,*)'I leave lybrary_crysplas_CAPSUL cm3Libraries--------'
      return
      end
      
      SUBROUTINE  form_Mjacob(dtime,DFGRD_ELAS_PRED0_inv,Mjacob)
      implicit none
      real*8 dtime,DFGRD_ELAS_PRED0_inv(3,3),Mjacob(3,3,3,3)
      real*8 term
      integer ii,jj,kk,ll,kroneker

      CALL  dzeros_tensor4(Mjacob,3,3,3,3)
      
      do ii=1,3
         do jj=1,3
            do kk=1,3
               do ll=1,3
                  term=DFGRD_ELAS_PRED0_inv(ii,kk)*
     1                 kroneker(jj,ll)
                  Mjacob(ii,jj,kk,ll)=-(1/dtime)*term
               enddo
            enddo
         enddo
      enddo
      return
      end

      SUBROUTINE  form_partial_sigma_F(DFGRD_ELAS_PRED,stiff_orient,
     1     partial_sigma)
      implicit none
      REAL*8 DFGRD_ELAS_PRED(3,3),stiff_orient(3,3,3,3)
      REAL*8 partial_sigma(3,3,3,3),aux3333(3,3,3,3)
      integer ii,jj,nm,nn,kk,ll,kroneker

      CALL  dzeros_tensor4(partial_sigma,3,3,3,3)

c     partial Epsilon/Fe (rs,ss,nm,nn)
      do,ii=1,3
         do,jj=1,3
            do,nm=1,3
               do,nn=1,3
                  aux3333(ii,jj,nm,nn)=
     1                 .5d0*(kroneker(ii,nn)* 
     1                 DFGRD_ELAS_PRED(nm,jj)+
     1                 kroneker(jj,nn)* 
     1                 DFGRD_ELAS_PRED(nm,ii))
               enddo
            enddo
         enddo
      enddo  

    

c     partial sigmaij/Fmn
      
      do,ii=1,3
         do,jj=1,3
            do,nm=1,3
               do,nn=1,3
                  do,kk=1,3
                     do,ll=1,3
                        partial_sigma(ii,jj,nm,nn)=
     1                       partial_sigma(ii,jj,nm,nn)+
     1                       stiff_orient(ii,jj,kk,ll)*
     1                       aux3333(kk,ll,nm,nn)
                     enddo
                  enddo
               enddo
            enddo
         enddo
      enddo
      return
      end



