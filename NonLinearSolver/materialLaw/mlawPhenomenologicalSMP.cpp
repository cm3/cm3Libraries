// C++ Interface: material law
// Description: PhenomenologicalSMP law
// Author:  <H. Gulasik, M. Pareja>, (C) 2019
// Copyright: See COPYING file that comes with this distribution

#include "mlawPhenomenologicalSMP.h"
#include "ipPhenomenologicalSMP.h"
#include <iostream>
#include <math.h>
#include <vector>
#include "MInterfaceElement.h"
#include <fstream>

mlawPhenomenologicalSMP::mlawPhenomenologicalSMP(const int num, const double rho, const double alpha, const double beta, const double gamma, const double t0, 
                            const double Kxg, const double Kyg, const double Kzg, const double Kxr, const double Kyr, const double Kzr,
                            const double KxAM, const double KyAM, const double KzAM,
                            const double cfG, const double cfR, const double cfAM,
                            const double cg, const double cr, const double cAM, 
                            const double Tm0, const double Tc0, const double xi, const double wm0, const double wc0, 
                            const double alphag0, const double alphar0, const double alphaAM0, const double alphaAD0,
                            const double Eg, const double Er,const double EAM, 
                            const double nug, const double nur, const double nuAM,
                            const double TaylorQuineyG,const double TaylorQuineyR, const double TaylorQuineyAM, 
                            const double zAM) :mlawCoupledThermoMechanics(num),
     _rho(rho), _alpha(alpha), _beta(beta), _gamma(gamma), _t0(t0),
     _Kxg(Kxg), _Kyg(Kyg), _Kzg(Kzg), _Kxr(Kxr), _Kyr(Kyr), _Kzr(Kzr), _KxAM(KxAM), _KyAM(KyAM), _KzAM(KzAM),
     _cfG(cfG), _cfR(cfR), _cfAM(cfAM), _cg(cg), _cr(cr), _cAM(cAM),
     _Tm0(Tm0), _Tc0(Tc0), _xi(xi), _wm0(wm0), _wc0(wc0),
     _alphag0(alphag0), _alphar0(alphar0),_alphaAM0(alphaAM0), _alphaAD0(alphaAD0),
     _TaylorQuineyG(TaylorQuineyG), _TaylorQuineyR(TaylorQuineyR), _TaylorQuineyAM(TaylorQuineyAM), 
      _Tonsetg((6.90675/(2.*wm0))+Tm0), _Tonsetr((-6.90675/(2.*wc0))+Tc0), _I2(1.), _I4(1.,1.),
      _zAM(zAM)
//     _Gg(Eg/2./(1.+nug)), _Gr(Er/2./(1.+nur)), _Kg(Eg/3./(1.-2.*nug)), _Kr(Er/3./(1.-2.*nur)),_GAM(EAM/2./(1.+nuAM)), _KAM(EAM/3./(1.-2.*nuAM))

{

    lawG = new mlawPowerYieldHyper(num+123451,Eg,nug,rho);
    lawR = new mlawPowerYieldHyper(num+123452,Er,nur,rho);
    lawAM = new mlawPowerYieldHyper(num+123453,EAM,nuAM,rho);

    _TcTmWcWm       = {0., 0., 0., 0.,1.,1.,1.,1.};
    _alphaParam     = {0., 0., 0., 0., 0., 0., 0., 0.,1.,1.,1.,1.,1.,1.,1.,1.};


    _temFunc_kqG    = new constantScalarFunction(1.);
    _temFunc_cG     = new constantScalarFunction(1.);
    _temFunc_alphaG = new constantScalarFunction(1.);
    _temFunc_kqR    = new constantScalarFunction(1.);
    _temFunc_cR     = new constantScalarFunction(1.);
    _temFunc_alphaR = new constantScalarFunction(1.);
    _temFunc_kqAM   = new constantScalarFunction(1.);
    _temFunc_alphaAM= new constantScalarFunction(1.);
    _temFunc_cAM    = new constantScalarFunction(1.);
    
    _yieldCrystallinityG = new constantScalarFunction(1.);
    _yieldCrystallinityR = new constantScalarFunction(1.);
    _yieldCrystallinityAM = new constantScalarFunction(1.);

    _crystallizationVolumeRate = new constantScalarFunction(1.);

    _EPFunc_AM      = NULL;
    _EPFunc_R      = NULL;


    for (int i=0; i<3; i++){
        for (int j=0; j<3; j++){
            for (int k=0; k<3; k++){
                for (int l=0; l<3; l++){
                    _I4dev(i,j,k,l) = 0.5*(_I2(i,k)*_I2(j,l)+_I2(i,l)*_I2(j,k)) - _I2(i,j)*_I2(k,l)/3.;
                }
            }
        }
    }

    STensor3 R;
    double co1, co2, co3, si1, si2, si3;
    double s1c2, c1c2;
    double _pi(3.14159265359);
    double fpi = _pi/180.;
    co1  = cos(_alpha*fpi);
    si1  = sin(_alpha*fpi);
    co2  = cos(_beta*fpi);
    si2  = sin(_beta*fpi);
    co3  = cos(_gamma*fpi);
    si3  = sin(_gamma*fpi);
    s1c2 = si1*co2;
    c1c2 = co1*co2;

    R(0,0) = co3*co1 - s1c2*si3;
    R(0,1) = co3*si1 + c1c2*si3;
    R(0,2) = si2*si3;
    R(1,0) = -si3*co1 - s1c2*co3;
    R(1,1) = -si3*si1 + c1c2*co3;
    R(1,2) = si2*co3;
    R(2,0) = si1*si2;
    R(2,1) = -co1*si2;
    R(2,2) = co2;

    double a=25.e-5;
    STensor3 alphaDilatation;
    alphaDilatation(0,0)= a;
    alphaDilatation(1,1)= a;
    alphaDilatation(2,2)= a;
    for(int i=0; i<3; i++){
        for(int j=0; j<3; j++){
            _alphaDilatation(i,j)=0.;
            for(int m=0; m<3; m++){
                for(int n=0; n<3; n++){
                    _alphaDilatation(i,j) +=R(m,i)*R(n,j)*alphaDilatation(m,n);
                }
            }
        }
    }


 
    double _lambdag =(Eg*nug)/(1.+nug)/(1.-2.*nug);
    double _Gg=(Eg/2./(1.+nug));
    STensor43 K_;
    K_*=0.;
    K_(0,0,0,0) = _lambdag + 2.*_Gg;
    K_(1,1,0,0) = _lambdag;
    K_(2,2,0,0) = _lambdag;
    K_(0,0,1,1) = _lambdag;
    K_(1,1,1,1) = _lambdag + 2.*_Gg;
    K_(2,2,1,1) = _lambdag;
    K_(0,0,2,2) = _lambdag;
    K_(1,1,2,2) = _lambdag;
    K_(2,2,2,2) = _lambdag + 2.*_Gg;
    K_(1,0,1,0) = _Gg;
    K_(2,0,2,0) = _Gg;
    K_(0,1,0,1) = _Gg;
    K_(2,1,2,1) = _Gg;
    K_(0,2,0,2) = _Gg;
    K_(1,2,1,2) = _Gg;
    K_(0,1,1,0) = _Gg;
    K_(0,2,2,0) = _Gg;
    K_(1,0,0,1) = _Gg;
    K_(1,2,2,1) = _Gg;
    K_(2,0,0,2) = _Gg;
    K_(2,1,1,2) = _Gg;

    for(int i=0; i<3; i++){
        for(int j=0; j<3; j++){
            _Stiff_alphaDilatation(i,j) =0.;
            for(int k=0; k<3; k++){
                for(int l=0; l<3; l++){
                    _Stiff_alphaDilatation(i,j) +=K_(i,j,k,l)*_alphaDilatation(k,l);
                }
            }
        }
    }

    // to be unifom in DG3D q= k' gradT with k'=-k instead of q=-kgradT
    STensor3 kqg, kqr;
    STensor3 kqAM;
    kqg(0,0)   =-_Kxg;
    kqg(1,1)   =-_Kyg;
    kqg(2,2)   =-_Kzg;
    kqr(0,0)   =-_Kxr;
    kqr(1,1)   =-_Kyr;
    kqr(2,2)   =-_Kzr;
    kqAM(0,0) =-_KxAM;
    kqAM(1,1) =-_KyAM;
    kqAM(2,2) =-_KzAM;

    for(int i=0; i<3; i++){
        for(int j=0; j<3; j++){
            _kqg(i,j)    =0.;
            _kqr(i,j)    =0.;
            _kqAM(i,j)  =0.;
            for(int m=0; m<3; m++){
                for(int n=0; n<3; n++){
                    _kqg(i,j)  +=R(m,i)*R(n,j)*kqg(m,n);
                    _kqr(i,j)  +=R(m,i)*R(n,j)*kqr(m,n);
                    _kqAM(i,j) +=R(m,i)*R(n,j)*kqAM(m,n);
                }
            }
        }
    }
    _k0=_kqr;
}

mlawPhenomenologicalSMP::mlawPhenomenologicalSMP(const mlawPhenomenologicalSMP &source)
    :mlawCoupledThermoMechanics(source), _rho(source._rho), _alpha(source._alpha), _beta(source._beta), _gamma(source._gamma), _Kxg(source._Kxg), _Kyg(source._Kyg),
     _Kzg(source._Kzg), _Kxr(source._Kxr), _Kyr(source._Kyr), _Kzr(source._Kzr),  _KxAM(source._KxAM), _KyAM(source._KyAM), _KzAM(source._KzAM),
     _kqg(source._kqg), _kqr(source._kqr), _kqAM(source._kqAM), 
     _cfG(source._cfG), _cfR(source._cfR),_cfAM(source._cfAM), _cg(source._cg),  _cr(source._cr), _cAM(source._cAM),
      _t0(source._t0), _wm0(source._wm0), _wc0(source._wc0),  _Tm0(source._Tm0), _Tc0(source._Tc0), _xi(source._xi),
      _alphag0(source._alphag0), _alphar0(source._alphar0),_alphaAM0(source._alphaAM0), _alphaAD0(source._alphaAD0),
     _TaylorQuineyG(source._TaylorQuineyG),_TaylorQuineyR(source._TaylorQuineyR),_TaylorQuineyAM(source._TaylorQuineyAM),
     _zAM(source._zAM), _I2(1.), _I4(1.,1.), _I4dev(source._I4dev),     
     _Tonsetg(source._Tonsetg), _Tonsetr(source._Tonsetr), 
     _k0(source._k0), _Stiff_alphaDilatation(source._Stiff_alphaDilatation), _alphaDilatation(source._alphaDilatation)
{


    _TcTmWcWm = source._TcTmWcWm;
    _alphaParam = source._alphaParam;

    _temFunc_kqG = NULL;
    if (source._temFunc_kqG!= NULL)
    {_temFunc_kqG = source._temFunc_kqG->clone();}

    _temFunc_cG = NULL;
    if (source._temFunc_cG != NULL)
    {_temFunc_cG = source._temFunc_cG->clone();}

    _temFunc_alphaG = NULL;
    if (source._temFunc_alphaG != NULL)
    {_temFunc_alphaG = source._temFunc_alphaG->clone();}

    _temFunc_kqR = NULL;
    if (source._temFunc_kqR!= NULL)
    {_temFunc_kqR = source._temFunc_kqR->clone();}

    _temFunc_cR = NULL;
    if (source._temFunc_cR != NULL)
    {_temFunc_cR = source._temFunc_cR->clone();}

    _temFunc_alphaR = NULL;
    if (source._temFunc_alphaR != NULL)
    {_temFunc_alphaR = source._temFunc_alphaR->clone();}

    _temFunc_kqAM = NULL;
    if (source._temFunc_kqAM!=NULL)
    {_temFunc_kqAM = source._temFunc_kqAM->clone();}

    _temFunc_cAM = NULL;
    if (source._temFunc_cAM!=NULL)
    {_temFunc_cAM = source._temFunc_cAM->clone();}

    _temFunc_alphaAM = NULL;
    if (source._temFunc_alphaAM!=NULL)
    {_temFunc_alphaAM = source._temFunc_alphaAM->clone();}

    _yieldCrystallinityG = NULL;
    if (source._yieldCrystallinityG!=NULL)
    {_yieldCrystallinityG=source._yieldCrystallinityG->clone();}
    
    _yieldCrystallinityR = NULL;
    if (source._yieldCrystallinityR!=NULL)
    {_yieldCrystallinityR=source._yieldCrystallinityR->clone();}

    _yieldCrystallinityAM = NULL;
    if (source._yieldCrystallinityAM!=NULL)
    {_yieldCrystallinityAM=source._yieldCrystallinityAM->clone();}

    _crystallizationVolumeRate = NULL;
    if (source._crystallizationVolumeRate!= NULL)
    {_crystallizationVolumeRate=source._crystallizationVolumeRate->clone();}


    lawG = NULL;
    if (source.lawG != NULL)
        lawG = dynamic_cast<mlawPowerYieldHyper*>(source.lawG->clone());
    lawR = NULL;
    if (source.lawR != NULL)
        lawR = dynamic_cast<mlawPowerYieldHyper*>(source.lawR->clone());
    lawAM = NULL;
    if (source.lawAM != NULL)
        lawAM = dynamic_cast<mlawPowerYieldHyper*>(source.lawAM->clone());

    _EPFunc_AM = NULL;
    if (source._EPFunc_AM!=NULL)
    {_EPFunc_AM = source._EPFunc_AM->clone();}

    _EPFunc_R = NULL;
    if (source._EPFunc_R!=NULL)
    {_EPFunc_R = source._EPFunc_R->clone();}



}

mlawPhenomenologicalSMP& mlawPhenomenologicalSMP::operator=(const  materialLaw &source)
{
    mlawCoupledThermoMechanics::operator=(source);
    const mlawPhenomenologicalSMP* src =dynamic_cast<const mlawPhenomenologicalSMP*>(&source);
		if (src != NULL)
		{
        _t0         = src->_t0;
        _I2         = src->_I2;
        _I4         = src->_I4;
        _I4dev      = src->_I4dev;
        _rho        = src->_rho;
        _Stiff_alphaDilatation  = src->_Stiff_alphaDilatation;
        _alphaDilatation        = src->_alphaDilatation;
        _wm0         = src->_wm0;
        _wc0         = src->_wc0;
        _TaylorQuineyG = src->_TaylorQuineyG;
        _TaylorQuineyR = src->_TaylorQuineyR;
        _TaylorQuineyAM = src->_TaylorQuineyAM;
        _cfG        = src->_cfG;
        _cfR        = src->_cfR;
        _cfAM       = src->_cfAM;        
        _cg         = src->_cg;
        _cr         = src->_cr;
        _cAM         = src->_cAM;
        
        _Tm0        = src->_Tm0;
        _Tc0        = src->_Tc0;
        _xi         = src->_xi;
        _Tonsetg    = src->_Tonsetg;
        _Tonsetr    = src->_Tonsetr;
        
        _alphag0    = src->_alphag0;
        _alphar0    = src->_alphar0;
        _alphaAM0   = src->_alphaAM0;
        _alphaAD0   = src->_alphaAD0;
        _alpha      = src->_alpha;
        _beta       = src->_beta;
        _gamma      = src->_gamma;
        _Kxg        = src->_Kxg;
        _Kyg        = src->_Kyg;
        _Kzg        = src->_Kzg;
        _Kxr        = src->_Kxr;
        _Kyr        = src->_Kyr;
        _Kzr        = src->_Kzr;
        _KxAM       = src->_KxAM;
        _KyAM       = src->_KyAM;
        _KzAM       = src->_KzAM;
        _kqg        = src->_kqg;
        _kqr        = src->_kqr;
        _kqAM       = src->_kqAM;
        _k0         = src->_k0;
       


        _zAM        = src->_zAM;

        _TcTmWcWm   = src->_TcTmWcWm;
        _alphaParam = src->_alphaParam;


        if(lawG != NULL) delete lawG; lawG=NULL;
        if(src->lawG != NULL){lawG=dynamic_cast<mlawPowerYieldHyper*>(src->lawG->clone());}

        if(lawR!=NULL) delete lawR; lawR=NULL;
        if(src->lawR!=NULL)  {lawR=dynamic_cast<mlawPowerYieldHyper*>(src->lawR->clone());}

        if(lawAM!=NULL) delete lawAM; lawAM=NULL;
        if(src->lawAM!=NULL){lawAM=dynamic_cast<mlawPowerYieldHyper*>(src->lawAM->clone());}


        if(_temFunc_alphaG!=NULL) delete _temFunc_alphaG; _temFunc_alphaG=NULL;
        if(src->_temFunc_alphaG!=NULL){_temFunc_alphaG=src->_temFunc_alphaG->clone();}

        if(_temFunc_alphaR!=NULL) delete _temFunc_alphaR; _temFunc_alphaR=NULL;
        if(src->_temFunc_alphaR!=NULL){_temFunc_alphaR=src->_temFunc_alphaR->clone();}

        if(_temFunc_alphaAM!=NULL) delete _temFunc_alphaAM; _temFunc_alphaAM=NULL;
        if(src->_temFunc_alphaAM!=NULL){_temFunc_alphaAM=src->_temFunc_alphaAM->clone();}
 
        if(_temFunc_kqG!=NULL) delete _temFunc_kqG; _temFunc_kqG=NULL;
        if(src->_temFunc_kqG!=NULL){_temFunc_kqG=src->_temFunc_kqG->clone();}

        if(_temFunc_kqR!=NULL) delete _temFunc_kqR; _temFunc_kqR=NULL;
        if(src->_temFunc_kqR!=NULL){_temFunc_kqR=src->_temFunc_kqR->clone();}

        if(_temFunc_kqAM!=NULL) delete _temFunc_kqAM; _temFunc_kqAM=NULL;
        if(src->_temFunc_kqAM!=NULL){_temFunc_kqAM=src->_temFunc_kqAM->clone();}


        if(_temFunc_cG!=NULL) delete _temFunc_cG; _temFunc_cG=NULL;
        if(src->_temFunc_cG!=NULL){_temFunc_cG=src->_temFunc_cG->clone();}

        if(_temFunc_cR!=NULL) delete _temFunc_cR; _temFunc_cR=NULL;
        if(src->_temFunc_cR!=NULL){_temFunc_cR=src->_temFunc_cR->clone();}

        if(_temFunc_cAM!=NULL) delete _temFunc_cAM; _temFunc_cAM=NULL;
        if(src->_temFunc_cAM!=NULL){_temFunc_cAM=src->_temFunc_cAM->clone();}

        if(_yieldCrystallinityG != NULL) delete _yieldCrystallinityG; _yieldCrystallinityG=NULL;
        if (src->_yieldCrystallinityG!=NULL) {_yieldCrystallinityG=src->_yieldCrystallinityG->clone();}
    
        if(_yieldCrystallinityR != NULL) delete _yieldCrystallinityR; _yieldCrystallinityR=NULL;
        if (src->_yieldCrystallinityR!=NULL) {_yieldCrystallinityR=src->_yieldCrystallinityR->clone();}

        if(_yieldCrystallinityAM != NULL) delete _yieldCrystallinityAM; _yieldCrystallinityAM=NULL;
        if (src->_yieldCrystallinityAM!=NULL) {_yieldCrystallinityAM=src->_yieldCrystallinityAM->clone();}

        if(_crystallizationVolumeRate != NULL) delete _crystallizationVolumeRate; _crystallizationVolumeRate=NULL;
        if (src->_crystallizationVolumeRate!=NULL) {_crystallizationVolumeRate=src->_crystallizationVolumeRate->clone();}



        if(_EPFunc_AM!=NULL) delete _EPFunc_AM; _EPFunc_AM=NULL;
        if(src->_EPFunc_AM!=NULL){_EPFunc_AM=src->_EPFunc_AM->clone();}

        if(_EPFunc_R!=NULL) delete _EPFunc_R; _EPFunc_R=NULL;
        if(src->_EPFunc_R!=NULL){_EPFunc_R=src->_EPFunc_R->clone();}

        }

	return *this;
}





double mlawPhenomenologicalSMP::soundSpeed() const
{
    if(lawG==NULL) Msg::Error("mlawPhenomenologicalSMP: mlawPowerYieldHyper G not defined");
    if(lawR==NULL) Msg::Error("mlawPhenomenologicalSMP: mlawPowerYieldHyper R not defined");
    if(lawAM==NULL) Msg::Error("mlawPhenomenologicalSMP: mlawPowerYieldHyper AM not defined");

    return lawG->soundSpeed();
}




double mlawPhenomenologicalSMP::getExtraDofStoredEnergyPerUnitField(double zg) const
{
    //double specificheat =zg*_cg + (1.-zg)*_cr;s
    double specificheat = (1.-_zAM)*zg*_cg + (1.-_zAM)*(1.-zg)*_cr + _zAM*_cAM ;
    return specificheat;
}




void mlawPhenomenologicalSMP::createIPState(IPStateBase* &ips, bool hasBodyForce, const bool* state_, const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
    //bool inter=true;
    //const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
    //if(iele==NULL) inter=false;
    IPVariable* ipvi = new IPPhenomenologicalSMP(_t0, _Tc0, _Tm0, _wc0, _wm0, 
      lawG->getConstRefToCompressionHardening(), lawG->getConstRefToTractionHardening(), lawG->getConstRefToKinematicHardening(), lawG->getViscoElasticNumberOfElement(),
      lawR->getConstRefToCompressionHardening(), lawR->getConstRefToTractionHardening(), lawR->getConstRefToKinematicHardening(), lawR->getViscoElasticNumberOfElement(),
      lawAM->getConstRefToCompressionHardening(), lawAM->getConstRefToTractionHardening(), lawAM->getConstRefToKinematicHardening(), lawAM->getViscoElasticNumberOfElement());
    IPVariable* ipv1 = new IPPhenomenologicalSMP(_t0, _Tc0, _Tm0, _wc0, _wm0, 
      lawG->getConstRefToCompressionHardening(), lawG->getConstRefToTractionHardening(), lawG->getConstRefToKinematicHardening(), lawG->getViscoElasticNumberOfElement(),
      lawR->getConstRefToCompressionHardening(), lawR->getConstRefToTractionHardening(), lawR->getConstRefToKinematicHardening(), lawR->getViscoElasticNumberOfElement(),
      lawAM->getConstRefToCompressionHardening(), lawAM->getConstRefToTractionHardening(), lawAM->getConstRefToKinematicHardening(), lawAM->getViscoElasticNumberOfElement());
    IPVariable* ipv2 = new IPPhenomenologicalSMP(_t0, _Tc0, _Tm0, _wc0, _wm0, 
      lawG->getConstRefToCompressionHardening(), lawG->getConstRefToTractionHardening(), lawG->getConstRefToKinematicHardening(), lawG->getViscoElasticNumberOfElement(),
      lawR->getConstRefToCompressionHardening(), lawR->getConstRefToTractionHardening(), lawR->getConstRefToKinematicHardening(), lawR->getViscoElasticNumberOfElement(),
      lawAM->getConstRefToCompressionHardening(), lawAM->getConstRefToTractionHardening(), lawAM->getConstRefToKinematicHardening(), lawAM->getViscoElasticNumberOfElement());
     
    if(ips != NULL) delete ips;
    ips = new IP3State(state_,ipvi,ipv1,ipv2);
}



void mlawPhenomenologicalSMP::HookeTensor(const double KT, const double GT, STensor43& C) const
{
    for (int i=0; i<3; i++){
        for (int j=0; j<3; j++){
            for (int k=0; k<3; k++){
                for (int l=0; l<3; l++){
                    C(i,j,k,l) += KT*_I2(i,j)*_I2(k,l) + 2.*GT*_I4dev(i,j,k,l);
                }
            }
        }
    }
}

void mlawPhenomenologicalSMP::invHookeTensor(const double KT, const double GT, STensor43& C) const
{
    double a = -KT/(2.*GT*(3.*KT+2.*GT));
    double b = 1./(2.*GT);

    for (int i=0; i<3; i++){
        for (int j=0; j<3; j++){
            for (int k=0; k<3; k++){
                for (int l=0; l<3; l++){
                    C(i,j,k,l) += a*_I2(i,j)*_I2(k,l) + b*_I4dev(i,j,k,l);
                }
            }
        }
    }
}



void mlawPhenomenologicalSMP::thermalCalculations
    (const IPPhenomenologicalSMP *q0, IPPhenomenologicalSMP *q1, const STensor3& Fn, const double T0, const double T, const double dh,
     double &zg, double &dzgdT, STensor3 &dzgdF,
     STensor3 &FthG,  STensor3 &dFthGdT,  STensor43& dFthGdF,
     STensor3 &FthR,  STensor3 &dFthRdT,  STensor43& dFthRdF,
     STensor3 &FthAM, STensor3 &dFthAMdT, STensor43& dFthAMdF) const
{
    dzgdT=0.;
    STensorOperation::zero(dzgdF);
    STensorOperation::zero(FthG);
    STensorOperation::zero(FthR);
    STensorOperation::zero(FthAM);
    STensorOperation::zero(dFthGdT);
    STensorOperation::zero(dFthGdF);
    STensorOperation::zero(dFthRdT);
    STensorOperation::zero(dFthRdF);
    STensorOperation::zero(dFthAMdT);
    STensorOperation::zero(dFthAMdF);

    double &Tcrys =q1->getRefToTcrys();
    double &Tmelt =q1->getRefToTmelt();
    double &Wcrys =q1->getRefToWcrys();
    double &Wmelt =q1->getRefToWmelt();
#if 0
    STensor3 N1N1G0, N2N2G0, N3N3G0, N1N1R0, N2N2R0, N3N3R0, N1N1AM0, N2N2AM0, N3N3AM0;
    double alphaG1, alphaG2, alphaG3, alphaR1, alphaR2, alphaR3, alphaAM1, alphaAM2, alphaAM3;

    PhaseTransitionThermalParameters(q0, q1, Tcrys, Tmelt, Wcrys, Wmelt, N1N1G0, N2N2G0, N3N3G0, N1N1R0, N2N2R0, N3N3R0, N1N1AM0, N2N2AM0, N3N3AM0,
                              alphaG1, alphaG2, alphaG3, alphaR1, alphaR2, alphaR3, alphaAM1, alphaAM2, alphaAM3);

    STensor3 &AGl =q1->getRefToAGl();
    STensor3 &ARl =q1->getRefToARl();
    STensor3 &AAMl=q1->getRefToAAMl();
    AGl = alphaG1*N1N1G0 + alphaG2*N2N2G0 + alphaG3*N3N3G0;
    ARl = alphaR1*N1N1R0 + alphaR2*N2N2R0 + alphaR3*N3N3R0;
    AAMl= alphaAM1*N1N1AM0 + alphaAM2*N2N2AM0 + alphaAM3*N3N3AM0;
#else

    PhaseTransitionThermalParameters(q0, q1, Tcrys, Tmelt, Wcrys, Wmelt);
#endif

    const double &Tt0   =q0->getConstRefToTt();
    const double &wt0   =q0->getConstRefToWt();
    double &T_t         =q1->getRefToTt();
    double &wt          =q1->getRefToWt();
    T_t =Tt0;
    wt  =wt0;

    double dTcrysdT=0., dTmeltdT=0., dWcrysdT=0., dWmeltdT=0.;
    STensor3 dTcrysdEveR0, dTmeltdEveR0, dWcrysdEveR0, dWmeltdEveR0, dTcrysdF, dTmeltdF, dWcrysdF, dWmeltdF;
    double dTtdT=0., dwtdT=0.;
    STensor3 dTtdEveR0, dTtdF, dwtdEveR0, dwtdF, dzgdEveR0;

    get_Tt(q0, T, T0, Tcrys, Tmelt, Wcrys, Wmelt, dTcrysdT, dTmeltdT, dWcrysdT, dWmeltdT, dTcrysdEveR0, dTmeltdEveR0,  dWcrysdEveR0, dWmeltdEveR0,
                    dTcrysdF, dTmeltdF,  dWcrysdF, dWmeltdF, T_t, dTtdT, dTtdEveR0, dTtdF, wt, dwtdT, dwtdEveR0, dwtdF);

    get_zg(T, T0, q0, q1, T_t, dTtdT, dTtdEveR0, dTtdF, wt, dwtdT, dwtdEveR0, dwtdF, zg, dzgdT, dzgdEveR0, dzgdF);

    const STensor3& FthI0   =q0->getConstRefToFthI();
    STensor3& FthI          =q1->getRefToFthI();
    FthI  =FthI0;
    static STensor3 dFthIdT;
    static STensor43 dFthIdF;
    STensorOperation::zero(dFthIdT);
    STensorOperation::zero(dFthIdF);

    funFthI(q0, q1, T0, T, Tcrys, Tmelt, zg, dzgdT, FthI, dFthIdT, dFthIdF);

    FthG=FthI;
    dFthGdF=dFthIdF;
    dFthGdT=dFthIdT;

    FthR    =FthG;
    dFthRdT =dFthGdT;
    dFthRdF =dFthGdF;
    FthAM   =FthG;
    dFthAMdT=dFthGdT;
    dFthAMdF=dFthGdF;

}

void mlawPhenomenologicalSMP::PhaseTransitionThermalParameters
    (const IPPhenomenologicalSMP *q0, IPPhenomenologicalSMP *q1,
     double &Tcrys, double &Tmelt, double &Wcrys, double &Wmelt) const
{
     Tcrys=0.; Tmelt=0.; Wcrys=0.; Wmelt=0.;

    //rubbery
    const STensor3& FveR0  =q0->getConstRefToFveR();
    STensor3 CveR0, EveR0;
    STensorOperation::zero(EveR0);
    STensorOperation::zero(CveR0);
    STensorOperation::multSTensor3FirstTranspose(FveR0,FveR0,CveR0);
    bool ok=STensorOperation::logSTensor3(CveR0,lawR->getOrder(),EveR0);
    if(!ok)
    {
       //P(0,0) = P(1,1) = P(2,2) = sqrt(-1.);
       return; 
    }

    EveR0*=0.5;

    double EigValsR0_0, EigValsR0_1, EigValsR0_2;
    static STensor3 N1N1R0, N2N2R0, N3N3R0;
    STensorOperation::getEigenDecomposition(CveR0, EigValsR0_0, EigValsR0_1, EigValsR0_2, N1N1R0, N2N2R0, N3N3R0);


    double ELam1R0 =0.5*log(EigValsR0_0);
    double ELam2R0 =0.5*log(EigValsR0_1);
    double ELam3R0 =0.5*log(EigValsR0_2);
    double MagELamR0 =sqrt(ELam1R0*ELam1R0 + ELam2R0*ELam2R0 + ELam3R0*ELam3R0);


    //glassy
    const STensor3& FveG0  =q0->getConstRefToFveG();
    STensor3 CveG0, EveG0;
    STensorOperation::zero(EveG0);
    STensorOperation::zero(CveG0);
    STensorOperation::multSTensor3FirstTranspose(FveG0,FveG0,CveG0);
    ok=STensorOperation::logSTensor3(CveG0,lawG->getOrder(),EveG0);
    if(!ok)
    {
       //P(0,0) = P(1,1) = P(2,2) = sqrt(-1.);
       return; 
    }

    EveG0*=0.5;

    double EigValsG0_0, EigValsG0_1, EigValsG0_2;
    static STensor3 N1N1G0, N2N2G0, N3N3G0;
    STensorOperation::getEigenDecomposition(CveG0, EigValsG0_0, EigValsG0_1, EigValsG0_2, N1N1G0, N2N2G0, N3N3G0);

    double ELam1G0 =0.5*log(EigValsG0_0);
    double ELam2G0 =0.5*log(EigValsG0_1);
    double ELam3G0 =0.5*log(EigValsG0_2);
    double MagELamG0 =sqrt(ELam1G0*ELam1G0 + ELam2G0*ELam2G0 + ELam3G0*ELam3G0);

    //amorphous
    const STensor3& FveAM0 =q0->getConstRefToFveAM();
    STensor3 CveAM0, EveAM0;
    STensorOperation::zero(EveAM0);
    STensorOperation::zero(CveAM0);
    STensorOperation::multSTensor3FirstTranspose(FveAM0,FveAM0,CveAM0);
    ok=STensorOperation::logSTensor3(CveAM0,lawAM->getOrder(),EveAM0);
    if(!ok)
    {
       //P(0,0) = P(1,1) = P(2,2) = sqrt(-1.);
       return; 
    }

    EveAM0*=0.5;

    double EigValsAM0_0, EigValsAM0_1, EigValsAM0_2;
    static STensor3 N1N1AM0, N2N2AM0, N3N3AM0;
    STensorOperation::getEigenDecomposition(CveAM0, EigValsAM0_0, EigValsAM0_1, EigValsAM0_2, N1N1AM0, N2N2AM0, N3N3AM0);


    double ELam1AM0 =0.5*log(EigValsAM0_0);
    double ELam2AM0 =0.5*log(EigValsAM0_1);
    double ELam3AM0 =0.5*log(EigValsAM0_2);
    double MagELamAM0 =sqrt(ELam1AM0*ELam1AM0 + ELam2AM0*ELam2AM0 + ELam3AM0*ELam3AM0);


    ////
    double _Atc =(_TcTmWcWm)[0];
    double _Btm =(_TcTmWcWm)[1];
    double _Cwc =(_TcTmWcWm)[2];
    double _Dwm =(_TcTmWcWm)[3];
    
    double alphatc=(_TcTmWcWm)[4];
    double alphatm=(_TcTmWcWm)[5];
    double alphawc=(_TcTmWcWm)[6];
    double alphawm=(_TcTmWcWm)[7];

    
    
    Tcrys =_Atc*tanh(alphatc*MagELamR0) + _Tc0;
    Wcrys =_Cwc*tanh(alphawc*MagELamR0) + _wc0;
    Tmelt =_Btm*tanh(alphatm*MagELamR0) + _Tm0;
    Wmelt =_Dwm*tanh(alphawm*MagELamR0) + _wm0;

    ////////
}

void mlawPhenomenologicalSMP::getZgFunction_Sy0(const scalarFunction& Tfunc, const double zg, double &wp, double &dwpdz) const
{
  wp = 1.-Tfunc.getVal(zg);
  dwpdz = -Tfunc.getDiff(zg);

}


void mlawPhenomenologicalSMP::PhaseTransitionCrystallisationParameters
        (const IPPhenomenologicalSMP *q0, IPPhenomenologicalSMP *q1,
                 STensor3 &N1N1R, STensor3 &N2N2R, STensor3 &N3N3R,
         double &alphaAD1,  double &alphaAD2,  double &alphaAD3) const
{
 

     STensorOperation::zero(N1N1R);
     STensorOperation::zero(N2N2R);
     STensorOperation::zero(N3N3R);
     alphaAD1=0.; alphaAD2=0.; alphaAD3=0.;


    //rubbery
    const STensor3& FveR  =q1->getConstRefToFveR();
    STensor3 CveR, EveR;
    STensorOperation::zero(EveR);
    STensorOperation::zero(CveR);
    STensorOperation::multSTensor3FirstTranspose(FveR,FveR,CveR);
    bool ok=STensorOperation::logSTensor3(CveR,lawR->getOrder(),EveR);
    if(!ok)
    {
       //P(0,0) = P(1,1) = P(2,2) = sqrt(-1.);
       return; 
    }
    EveR*=0.5;


    double EigValsR_0, EigValsR_1, EigValsR_2;
    STensorOperation::getEigenDecomposition(CveR, EigValsR_0, EigValsR_1, EigValsR_2, N1N1R, N2N2R, N3N3R);

    double ELam1R =0.5*log(EigValsR_0);
    double ELam2R =0.5*log(EigValsR_1);
    double ELam3R =0.5*log(EigValsR_2);
    double MagELamR =sqrt(ELam1R*ELam1R + ELam2R*ELam2R + ELam3R*ELam3R);


    ////////
    double _ADP=(_alphaParam)[2], _ADN=(_alphaParam)[6];
    double _alphaADP=(_alphaParam)[10], _alphaADN=(_alphaParam)[14];
    

    if(ELam1R>=0.){
          alphaAD1 =_ADP*tanh(_alphaADP*ELam1R);
    }else{
          alphaAD1 =_ADN*tanh(_alphaADN*ELam1R);}

    if(ELam2R>=0.){
          alphaAD2 =_ADP*tanh(_alphaADP*ELam2R);
    }else{
          alphaAD2 =_ADN*tanh(_alphaADN*ELam2R);}

    if(ELam3R>=0.){
          alphaAD3 =_ADP*tanh(_alphaADP*ELam3R);
    }else{
          alphaAD3 =_ADN*tanh(_alphaADN*ELam3R);}



}
         

void mlawPhenomenologicalSMP::funFthAD
    (const IPPhenomenologicalSMP *q0, IPPhenomenologicalSMP *q1, const STensor3& ADl,
     const STensor3& FthAD0, STensor3& FthAD) const
{
    double zg=q1->getzg();
    static STensor3 lami;
    STensorOperation::zero(lami);

    STensorOperation::expSTensor3(ADl, lawR->getOrder(), lami, NULL);

    if(getTime()-_timeStep<0.){
        FthAD= FthAD0;
    }else{
        if(zg<=std::min(0.01,100.*toleranceOnZg))
            FthAD=lami;
        else
            FthAD= FthAD0;
    }        
}

void mlawPhenomenologicalSMP::funEnThAI(const double T, const double TRef, const IPHyperViscoElastic& q0, IPHyperViscoElastic &q1,
     double anisotropicThermalCoefAmplitudeBulk, double anisotropicThermalCoefAmplitudeShear, 
     double anisotropicThermalAlphaCoefTanhTempBulk, double anisotropicThermalAlphaCoefTanhTempShear) const
{
    double alphaTK=1.+anisotropicThermalCoefAmplitudeBulk*tanh(anisotropicThermalAlphaCoefTanhTempBulk*(T-TRef));
    q1.getRefToElasticBulkPropertyScaleFactor()=alphaTK;
    double alphaTG=1.+anisotropicThermalCoefAmplitudeShear*tanh(anisotropicThermalAlphaCoefTanhTempShear*(T-TRef));
    q1.getRefToElasticShearPropertyScaleFactor()=alphaTG;
    
}


#if 0
void mlawPhenomenologicalSMP::funEnThAI
    (const double T, const double TRef, 
     const STensor3& Ene, STensor3& strainEffect, STensor3& dStrainEffectdT, STensor43& dStrainEffectdEne, 
     double anisotropicThermalCoefAmplitude, double anisotropicThermalCoefUnused, 
     double anisotropicThermalAlphaCoefTanhEn, double anisotropicThermalAlphaCoefTanhTemp) const
{
    //STensor3 aux, dauxdT, lami;
    //STensor43 dlami;
    //STensorOperation::zero(aux);
    //STensorOperation::zero(dauxdT);
    //STensorOperation::zero(lami);
    //STensorOperation::zero(dlami);

    static STensor3 EneDev;
    EneDev=Ene.dev();
    double ELam1, ELam2, ELam3;
    static STensor3 N1N1, N2N2, N3N3, dELam1, dELam2, dELam3;
    static STensor43 dN1N1, dN2N2, dN3N3;
    //STensorOperation::getEigenDecomposition(Ene, ELam1, ELam2, ELam3, N1N1, N2N2, N3N3, dELam1, dELam2, dELam3, dN1N1, dN2N2, dN3N3);

    //double MagELam =sqrt(ELam1*ELam1 + ELam2*ELam2 + ELam3*ELam3);
    
    double MagELam = Ene.norm2();


/*    double alpha1=0.,alpha2=0.,alpha3=0., anisotropicThermalAlphaCoef1=0.,anisotropicThermalAlphaCoef2=0.,anisotropicThermalAlphaCoef3=0.,
           anisotropicThermalCoef1=0,anisotropicThermalCoef2=0,anisotropicThermalCoef3=0;
    
    if(ELam1>=0.){
          anisotropicThermalAlphaCoef1=anisotropicThermalAlphaCoefTanhEn;
          anisotropicThermalCoef1=anisotropicThermalCoefAmplitude;
    }else{
          anisotropicThermalAlphaCoef1=anisotropicThermalAlphaCoefTanhTemp;
          anisotropicThermalCoef1=anisotropicThermalCoefUnused;
    }
    alpha1  =anisotropicThermalCoef1*tanh(anisotropicThermalAlphaCoef1*ELam1);
    if(ELam2>=0.){
          anisotropicThermalAlphaCoef2=anisotropicThermalAlphaCoefTanhEn;
          anisotropicThermalCoef2=anisotropicThermalCoefAmplitude;
    }else{
          anisotropicThermalAlphaCoef2=anisotropicThermalAlphaCoefTanhTemp;
          anisotropicThermalCoef2=anisotropicThermalCoefUnused;
    }
    alpha2  =anisotropicThermalCoef2*tanh(anisotropicThermalAlphaCoef2*ELam2);

    if(ELam3>=0.){
          anisotropicThermalAlphaCoef3=anisotropicThermalAlphaCoefTanhEn;
          anisotropicThermalCoef3=anisotropicThermalCoefAmplitude;
    }else{
          anisotropicThermalAlphaCoef3=anisotropicThermalAlphaCoefTanhTemp;
          anisotropicThermalCoef3=anisotropicThermalCoefUnused;
    }
    alpha3  =anisotropicThermalCoef3*tanh(anisotropicThermalAlphaCoef3*ELam3);

    strainEffect = alpha1*N1N1*(T-TRef) + alpha2*N2N2*(T-TRef) + alpha3*N3N3*(T-TRef);
    strainEffect=strainEffect.dev();
    dStrainEffectdT =alpha1*N1N1 + alpha2*N2N2 + alpha3*N3N3;
    dStrainEffectdT=dStrainEffectdT.dev();
    
    
    STensorOperation::zero(dStrainEffectdEne);
    for(int i=0; i<3; i++)
    {
       for(int j=0; j<3; j++)
       {
          for(int k=0; k<3; k++)
          {
             for(int l=0; l<3; l++)
             {
                dStrainEffectdEne(i,j,k,l)=(anisotropicThermalCoef1*anisotropicThermalAlphaCoef1*(1.-tanh(anisotropicThermalAlphaCoef1*ELam1)*tanh(anisotropicThermalAlphaCoef1*ELam1)))
                                                  * N1N1(i,j)*dELam1(k,l)+anisotropicThermalCoef1*tanh(anisotropicThermalAlphaCoef1*ELam1)*dN1N1(i,j,k,l)+
                                               (anisotropicThermalCoef2*anisotropicThermalAlphaCoef2*(1.-tanh(anisotropicThermalAlphaCoef2*ELam2)*tanh(anisotropicThermalAlphaCoef2*ELam2)))
                                                  * N2N2(i,j)*dELam2(k,l)+anisotropicThermalCoef2*tanh(anisotropicThermalAlphaCoef2*ELam2)*dN2N2(i,j,k,l)+
                                               (anisotropicThermalCoef3*anisotropicThermalAlphaCoef3*(1.-tanh(anisotropicThermalAlphaCoef3*ELam3)*tanh(anisotropicThermalAlphaCoef3*ELam3)))
                                                  * N3N3(i,j)*dELam3(k,l)+anisotropicThermalCoef3*tanh(anisotropicThermalAlphaCoef3*ELam3)*dN3N3(i,j,k,l);
                dStrainEffectdEne(i,j,k,l)*=(T-TRef);
                
             }
          }
       }
     }
     static STensor3 dStrainEffectdEneTr;
     for(int i=0; i<3; i++)
     {
       for(int j=0; j<3; j++)
       {
          dStrainEffectdEneTr(i,j)=0.;
          for(int k=0; k<3; k++)
          {
            dStrainEffectdEneTr(i,j)+=dStrainEffectdEne(k,k,i,j);
          }
        }
     }   
     for(int i=0; i<3; i++)
     {
       for(int j=0; j<3; j++)
       {
          for(int k=0; k<3; k++)
          {
             for(int l=0; l<3; l++)
             {
                if(i==j) dStrainEffectdEne(i,j,k,l)-=dStrainEffectdEneTr(k,l);
             }
           }
         }
       }*/
    
    double alphaEn  = 0.;
    if (MagELam>0)
       alphaEn=anisotropicThermalCoefAmplitude* tanh(anisotropicThermalAlphaCoefTanhEn*MagELam)/MagELam;

    double alphaT=tanh(anisotropicThermalAlphaCoefTanhTemp*(T-TRef));
    strainEffect = EneDev;
    strainEffect*=alphaEn*alphaT;
    
    static STensor3 dMagELam1,dMagELam2,dMagELam3,dMagELam;
    static STensor43 I4;
    STensorOperation::unity(I4);
    STensorOperation::zero(dMagELam1); STensorOperation::zero(dMagELam2); STensorOperation::zero(dMagELam3); 
    static STensor3 dalphaEndEn;
    STensorOperation::zero(dalphaEndEn);
    if(MagELam>0.)
    {
     
      //dMagELam1=dELam1;
      //dMagELam1*=ELam1/MagELam;
      //dMagELam2=dELam2;
      //dMagELam2*=ELam2/MagELam;
      //dMagELam3=dELam3;
      //dMagELam3*=ELam3/MagELam;
      //dMagELam=dMagELam1;
      //dMagELam+=dMagELam2;
      //dMagELam+=dMagELam3;
      
      dMagELam=Ene;
      dMagELam*=1./MagELam;
      
      dalphaEndEn=dMagELam;
      dalphaEndEn*=(-alphaEn/MagELam + anisotropicThermalCoefAmplitude*anisotropicThermalAlphaCoefTanhEn/
               MagELam*(1.-tanh(anisotropicThermalAlphaCoefTanhEn*MagELam)*tanh(anisotropicThermalAlphaCoefTanhEn*MagELam)));
    }    
    double dalphaTdT= anisotropicThermalAlphaCoefTanhTemp*(1.-tanh(anisotropicThermalAlphaCoefTanhTemp*(T-TRef))*tanh(anisotropicThermalAlphaCoefTanhTemp*(T-TRef)));
    
     
    dStrainEffectdT = EneDev;
    dStrainEffectdT *= alphaEn*dalphaTdT;
    
    //Msg::Info("T %f, TRef %f, alpha %f, strain effet:",T,TRef,alpha);
    //strainEffect.print("strain effect");
    
    STensorOperation::zero(dStrainEffectdEne);
      
      for(int i=0; i<3; i++)
      {
        for(int j=0; j<3; j++)
        {  
          for(int k=0; k<3; k++)
          {
            for(int l=0; l<3; l++)
            {
              dStrainEffectdEne(i,j,k,l)=alphaT*EneDev(i,j)*dalphaEndEn(k,l)+
                                    alphaEn*alphaT*I4(i,j,k,l);
            }
          }
        }
      } 
      static STensor3 dStrainEffectdEneTr;
      for(int i=0; i<3; i++)
      {
        for(int j=0; j<3; j++)
        {
          dStrainEffectdEneTr(i,j)=0.;
          for(int k=0; k<3; k++)
          {
            dStrainEffectdEneTr(i,j)+=dStrainEffectdEne(k,k,i,j);
          }
        }
      }     
      for(int i=0; i<3; i++)
      {
        for(int j=0; j<3; j++)
        {
          for(int k=0; k<3; k++)
          {
             for(int l=0; l<3; l++)
             {
                if(i==j) dStrainEffectdEne(i,j,k,l)-=dStrainEffectdEneTr(k,l);
             }
           }
         }
       }  
}
#endif

void mlawPhenomenologicalSMP::funFthI
    (const IPPhenomenologicalSMP *q0, IPPhenomenologicalSMP *q1,
     const double T0, const double T, const double Tcrys, const double Tmelt, const double zg, const double dzgdT,
     STensor3& FthI, STensor3& dFthIdT, STensor43& dFthIdF) const
{
    const STensor3 &FthI0 =q0->getConstRefToFthI();
    STensorOperation::zero(FthI);
    STensorOperation::zero(dFthIdT);

    double zg0  =q0->getzg();
    if(getTime()-_timeStep<0.){zg0=zg;}

    const double zg_DEL = zg - zg0;
    const double zg_Av  =(zg + zg0)/2.;
    double zg_Cryst = _crystallizationVolumeRate->getVal(zg_Av)*zg_DEL;
    double dzg_CrystdT = _crystallizationVolumeRate->getVal(zg_Av)*dzgdT;

    /*double zAD0=0.7;
    double zAD1=0.9;
    double alphaAD0=0.;
    double dalphaAD0dT=0.;
    if(zg_Av<zAD1 and zAD0<zg_Av)
    {
      alphaAD0=_alphaAD0*2.*(zAD1-zg_Av)/(zAD1-zAD0)/(zAD1-zAD0);
      dalphaAD0dT=-dzgdT*_alphaAD0*2./(zAD1-zAD0)/(zAD1-zAD0);
    }
    */
    
    STensor3 aux, dauxdT, lamC;
    STensor43 dlamC;
    STensorOperation::zero(aux);
    STensorOperation::zero(dauxdT);
    STensorOperation::zero(lamC);
    STensorOperation::zero(dlamC);

    if(T-T0<-toleranceOnT){
        for(int i=0; i<3; i++){
            for(int j=0; j<3; j++){
                aux(i,j)    += (1-_zAM)*zg_Av*_alphag0*_I2(i,j)*(T-T0) + (1-_zAM)*(1.-zg_Av)*_alphar0*_I2(i,j)*(T-T0) + _zAM*_alphaAM0*_I2(i,j)*(T-T0)
                              +(1-_zAM)*zg_Cryst*_alphaAD0*_I2(i,j);
                              //+(1-_zAM)*zg_DEL*alphaAD0*_I2(i,j);
                dauxdT(i,j) += (1-_zAM)*0.5*dzgdT*_alphag0*_I2(i,j)*(T-T0) + (1-_zAM)*zg_Av*_alphag0*_I2(i,j)
                              -(1-_zAM)*0.5*dzgdT*_alphar0*_I2(i,j)*(T-T0) + (1-_zAM)*(1.-zg_Av)*_alphar0*_I2(i,j)
                              +_zAM*_alphaAM0*_I2(i,j)
                              +(1-_zAM)*dzg_CrystdT*_alphaAD0*_I2(i,j);
                              //+(1-_zAM)*dzgdT*alphaAD0*_I2(i,j)+(1-_zAM)*zg_DEL*dalphaAD0dT*_I2(i,j);
            }
        }
    }else if(T-T0>toleranceOnT){
        for(int i=0; i<3; i++){
            for(int j=0; j<3; j++){
                aux(i,j)   += (1-_zAM)*zg_Av*_alphag0*_I2(i,j)*(T-T0) + (1-_zAM)*(1.-zg_Av)*_alphar0*_I2(i,j)*(T-T0) + _zAM*_alphaAM0*_I2(i,j)*(T-T0)
                             +(1-_zAM)*zg_Cryst*_alphaAD0*_I2(i,j) 
                             //+(1-_zAM)*zg_DEL*alphaAD0*_I2(i,j) 
                             + (1-_zAM)*zg_DEL*(_alphar0*_I2(i,j) - _alphag0*_I2(i,j))*(Tcrys-Tmelt);
                dauxdT(i,j)+= (1-_zAM)*0.5*dzgdT*_alphag0*_I2(i,j)*(T-T0) + (1-_zAM)*zg_Av*_alphag0*_I2(i,j)
                             -(1-_zAM)*0.5*dzgdT*_alphar0*_I2(i,j)*(T-T0) + (1-_zAM)*(1.-zg_Av)*_alphar0*_I2(i,j)
                             +_zAM*_alphaAM0*_I2(i,j)
                             +(1-_zAM)*dzg_CrystdT*_alphaAD0*_I2(i,j)
                             //+(1-_zAM)*dzgdT*alphaAD0*_I2(i,j)+(1-_zAM)*zg_DEL*dalphaAD0dT*_I2(i,j)
                             +(1-_zAM)*dzgdT*(_alphar0*_I2(i,j) - _alphag0*_I2(i,j))*(Tcrys-Tmelt);
            }
        }
    }


    STensorOperation::expSTensor3(aux, lawR->getOrder(), lamC, &dlamC);

    if(getTime()-_timeStep<0.){
        FthI= FthI0;
        dFthIdT*=0.;
    }else{
        for(int i=0; i<3; i++){
            for(int j=0; j<3; j++){
                for(int k=0; k<3; k++){
                    FthI(i,k) += lamC(i,j)*FthI0(j,k);
                    for(int l=0; l<3; l++){
                        for(int m=0; m<3; m++){
                            dFthIdT(i,k) += dlamC(i,j,l,m)*dauxdT(l,m)*FthI0(j,k);
                        }
                    }
                }
            }
        }
    }
}

bool mlawPhenomenologicalSMP::checkDeltaEp(const STensor3& FfR0, const STensor3& FfR) const
{
    STensor3 CfR0,CfR1,EfR0,EfR1;
    STensor43 dlnCfR0,dlnCfR1;
    STensorOperation::zero(CfR0);
    STensorOperation::zero(EfR0);
    STensorOperation::zero(dlnCfR0);
    STensorOperation::zero(CfR1);
    STensorOperation::zero(EfR1);
    STensorOperation::zero(dlnCfR1);
    double EfRnorm0;
    double EfRnorm1;
    for(int i=0; i<2; i++)
    {
        if(i==0)
        {
            STensorOperation::multSTensor3FirstTranspose(FfR0,FfR0,CfR0);
            bool ok=STensorOperation::logSTensor3(CfR0,lawR->getOrder(),EfR0,&dlnCfR0);
            if(!ok)
            {
              //P(0,0) = P(1,1) = P(2,2) = sqrt(-1.);
              //return; 
            }
            EfR0*=0.5;
            EfRnorm0=EfR0.norm2();
        }

        if(i==1)
        {
            STensorOperation::multSTensor3FirstTranspose(FfR,FfR,CfR1);
            bool ok=STensorOperation::logSTensor3(CfR1,lawR->getOrder(),EfR1,&dlnCfR1);
            if(!ok)
            {
               //P(0,0) = P(1,1) = P(2,2) = sqrt(-1.);
               //return; 
            }
            EfR1*=0.5;
            EfRnorm1=EfR1.norm2();
        }
    }

    if(EfRnorm1>EfRnorm0)
    {
        return true;
    }

    else
    {
        return false;
    }
}




void mlawPhenomenologicalSMP::constitutive(
    const STensor3      &F0,
    const STensor3      &Fn,
    STensor3            &P,
    const IPVariable    *q0,
    IPVariable          *q1,
    STensor43           &Tangent,
    const bool          stiff,
    STensor43           *elasticTangent,
    const bool          dTangent,
    STensor63*          dCalgdeps) const
{
}

void mlawPhenomenologicalSMP::constitutive(
        const STensor3      &F0,            // initial deformation gradient (input @ time n)
        const STensor3      &Fn,            // updated deformation gradient (input @ time n+1)
        STensor3            &P,             // updated 1st Piola-Kirchhoff stress tensor (output)
        const STensor3      &P0,
        const IPVariable    *q0i,           // array of initial internal variable
        IPVariable          *q1i,           // updated array of internal variable (in ipvcur on output),
        STensor43           &Tangent,       // constitutive tangents (output)
        const double        T0,
        double              T,
        const SVector3      &gradT,
        SVector3            &fluxT,
        STensor3            &dPdT,
        STensor3            &dqdgradT,
        SVector3            &dqdT,
        STensor33           &dqdF,
        const bool          stiff,          // if true compute the tangents
        double              &wth,
        double              &dwthdT,
        STensor3            &dwthdF,
        double              &mechSource,
        double              &dmechSourcedT,
        STensor3            &dmechSourcedF,
        STensor43           *elasticTangent) const
{
    const IPPhenomenologicalSMP *q0 = dynamic_cast<const IPPhenomenologicalSMP *>(q0i);
    IPPhenomenologicalSMP *q1       = dynamic_cast<      IPPhenomenologicalSMP *>(q1i);
    if(elasticTangent!=NULL) Msg::Error("mlawPhenomenologicalSMP  elasticTangent not defined");
    q1->operator=(static_cast<const IPVariable&>(*q0));

    //initilisation of variables to zero
    wth=0.;
    dwthdT=0.;
    STensorOperation::zero(dwthdF);
    mechSource=0.;
    dmechSourcedT=0.;
    STensorOperation::zero(dmechSourcedF);
    STensorOperation::zero(fluxT);
    STensorOperation::zero(dqdT);
    STensorOperation::zero(dqdF);
    STensorOperation::zero(dqdgradT);
    STensorOperation::zero(P);
    STensorOperation::zero(dPdT);
    STensorOperation::zero(Tangent);

    const double dh= getTimeStep();

    STensor3& PG    =q1->getRefToPG();
    STensor3& PR    =q1->getRefToPR();
    STensor3& PAM   =q1->getRefToPAM();
    STensor3& PRL2  =q1->getRefToPRL2();
    STensor3& PAML2 =q1->getRefToPAML2();
    STensorOperation::zero(PG);
    STensorOperation::zero(PR);
    STensorOperation::zero(PAM);
    STensorOperation::zero(PRL2);
    STensorOperation::zero(PAML2);

    static STensor3 dPGdT, dPRdT, dPAMdT, dPAML2dT, dPRL2dT;
    STensorOperation::zero(dPGdT);
    STensorOperation::zero(dPRdT);
    STensorOperation::zero(dPAMdT);
    STensorOperation::zero(dPRL2dT);
    STensorOperation::zero(dPAML2dT);

    static STensor43 TangentG, TangentR, TangentAM, TangentAML2, TangentRL2;
    STensorOperation::zero(TangentG);
    STensorOperation::zero(TangentR);
    STensorOperation::zero(TangentAM);
    STensorOperation::zero(TangentAML2);
    STensorOperation::zero(TangentRL2);


    double &zg  =q1->getRefTozg();
    double dzgdT=0.;
    static STensor3 dzgdF;
    STensorOperation::zero(dzgdF);

    const STensor3& FthG0  = q0->getConstRefToFthG();
    const STensor3& FthR0  = q0->getConstRefToFthR();
    const STensor3& FthAM0 = q0->getConstRefToFthAM();
    STensor3 &FthG   = q1->getRefToFthG();
    STensor3 &FthR   = q1->getRefToFthR();
    STensor3 &FthAM  = q1->getRefToFthAM();
    STensorOperation::zero(FthG);
    STensorOperation::zero(FthR);
    STensorOperation::zero(FthAM);

    static STensor3 dFthGdT, dFthRdT, dFthAMdT;
    static STensor43 dFthGdF, dFthRdF, dFthAMdF;
    STensorOperation::zero(dFthGdT);
    STensorOperation::zero(dFthGdF);
    STensorOperation::zero(dFthRdT);
    STensorOperation::zero(dFthRdF);
    STensorOperation::zero(dFthAMdT);
    STensorOperation::zero(dFthAMdF);

    thermalCalculations(q0, q1, Fn, T0, T, dh, zg, dzgdT, dzgdF, FthG, dFthGdT, dFthGdF, FthR, dFthRdT, dFthRdF, FthAM, dFthAMdT, dFthAMdF);


    double dPlas=0.;
    double dPlasG=0.;
    double dPlasR=0.;
    double dPlasAM=0.;
    double dPlasdT=0.;
    double dPlasGdT=0.;
    double dPlasRdT=0.;
    double dPlasAMdT=0.;
    static STensor3 dPlasdF, dPlasGdF, dPlasRdF, dPlasAMdF;
    STensorOperation::zero(dPlasdF);
    STensorOperation::zero(dPlasGdF);
    STensorOperation::zero(dPlasRdF);
    STensorOperation::zero(dPlasAMdF);

    double dVisc=0.;
    double dViscG=0.;
    double dViscR=0.;
    double dViscAM=0.;
    double dViscdT=0.;
    double dViscGdT=0.;
    double dViscRdT=0.;
    double dViscAMdT=0.;
    static STensor3 dViscdF, dViscGdF, dViscRdF, dViscAMdF;
    STensorOperation::zero(dViscdF);
    STensorOperation::zero(dViscGdF);
    STensorOperation::zero(dViscRdF);
    STensorOperation::zero(dViscAMdF);

    double Psi=0.;
    double PsiG=0.;
    double PsiR=0.;
    double PsiAM=0.;
    double PsiRL2=0.;
    double PsiAML2=0.;

    double VFG     = (1-_zAM)*(zg);
    double VFR     = (1-_zAM)*(1.-zg);
    double VFAM    = _zAM;
    double dVFGdT  = (1-_zAM)*(dzgdT);
    double dVFRdT  = (1-_zAM)*(-dzgdT);
    double dVFAMdT = 0.;
    static STensor3 dVFGdF, dVFRdF, dVFAMdF;
    STensorOperation::zero(dVFGdF);
    STensorOperation::zero(dVFRdF);
    STensorOperation::zero(dVFAMdF);
    dVFGdF= (1-_zAM)*(dzgdF);
    dVFRdF= (1-_zAM)*(-1.*dzgdF);

    static STensor3 FmG,FmG0, FthGinv,FthG0inv;
    STensorOperation::zero(FmG);
    STensorOperation::zero(FmG0);
    STensorOperation::zero(FthGinv);
    STensorOperation::zero(FthG0inv);
    STensorOperation::inverseSTensor3(FthG,FthGinv);
    STensorOperation::inverseSTensor3(FthG0,FthG0inv);
    STensorOperation::multSTensor3(Fn,FthGinv,FmG);
    STensorOperation::multSTensor3(F0,FthG0inv,FmG0);

    static STensor3 FmR, FmR0,FthRinv,FthR0inv;
    STensorOperation::zero(FmR);
    STensorOperation::zero(FmR0);
    STensorOperation::zero(FthRinv);
    STensorOperation::zero(FthR0inv);
    STensorOperation::inverseSTensor3(FthR,FthRinv);
    STensorOperation::inverseSTensor3(FthR0,FthR0inv);
    STensorOperation::multSTensor3(Fn,FthRinv,FmR);
    STensorOperation::multSTensor3(F0,FthR0inv,FmR0);

    static STensor3 FmAM, FmAM0,FthAMinv,FthAM0inv;
    STensorOperation::zero(FmAM);
    STensorOperation::zero(FmAM0);
    STensorOperation::zero(FthAMinv);
    STensorOperation::zero(FthAM0inv);
    STensorOperation::inverseSTensor3(FthAM,FthAMinv);
    STensorOperation::inverseSTensor3(FthAM0,FthAM0inv);
    STensorOperation::multSTensor3(Fn,FthAMinv,FmAM);
    STensorOperation::multSTensor3(F0,FthAM0inv,FmAM0);
    
    if (zg<=toleranceOnZg)
    {
     bool success=
     constitutive1(q0, q1,
     T0, T, stiff, F0, Fn, 
     FthG0, FthG, FmG0, FmG, dFthGdT, dFthGdF,  
     FthR0, FthR, FmR0, FmR, dFthRdT, dFthRdF, 
     FthAM0, FthAM, FmAM0, FmAM, dFthAMdT,  dFthAMdF,
     PsiG, dPlasG, dPlasGdT, dPlasGdF, dViscG,dViscGdT, dViscGdF, 
     PG, dPGdT, TangentG,
     PsiR, dPlasR, dPlasRdT, dPlasRdF, dViscR,dViscRdT, dViscRdF, 
     PR,   dPRdT, TangentR,
     PsiAM, dPlasAM, dPlasAMdT, dPlasAMdF, dViscAM, dViscAMdT, dViscAMdF, 
     PAM, dPAMdT,  TangentAM,
     PsiRL2, PRL2, dPRL2dT, TangentRL2,
     PsiAML2, PAML2, dPAML2dT, TangentAML2);
     if(!success)
     {
       Msg::Info("mlawPhenomenologicalSMP::constitutive did not converge");
       P(0,0) = P(1,1) = P(2,2) = sqrt(-1.); // to exist NR and performed next step with a smaller incremene
       return;
     }
     
    }

    if ((zg>toleranceOnZg)and(zg<1.-toleranceOnZg))
    {bool success=constitutive2(q0, q1,
     T0, T, stiff, F0, Fn, 
     FthG0, FthG, FmG0, FmG, dFthGdT, dFthGdF,  
     FthR0, FthR, FmR0, FmR, dFthRdT, dFthRdF, 
     FthAM0, FthAM, FmAM0, FmAM, dFthAMdT,  dFthAMdF,
     PsiG, dPlasG, dPlasGdT, dPlasGdF, dViscG,dViscGdT, dViscGdF, 
     PG, dPGdT, TangentG,
     PsiR, dPlasR, dPlasRdT, dPlasRdF, dViscR,dViscRdT, dViscRdF, 
     PR,   dPRdT, TangentR,
     PsiAM, dPlasAM, dPlasAMdT, dPlasAMdF, dViscAM, dViscAMdT, dViscAMdF, 
     PAM, dPAMdT,  TangentAM,
     PsiRL2, PRL2, dPRL2dT, TangentRL2,
     PsiAML2, PAML2, dPAML2dT, TangentAML2);
     if(!success)
     {
       Msg::Info("mlawPhenomenologicalSMP::constitutive did not converge");
       P(0,0) = P(1,1) = P(2,2) = sqrt(-1.); // to exist NR and performed next step with a smaller incremene
       return;
     }

     
     }

    if (zg>=1.-toleranceOnZg)
    {bool success=constitutive3(q0, q1,
     T0, T, stiff, F0, Fn, 
     FthG0, FthG, FmG0, FmG, dFthGdT, dFthGdF,  
     FthR0, FthR, FmR0, FmR, dFthRdT, dFthRdF, 
     FthAM0, FthAM, FmAM0, FmAM, dFthAMdT,  dFthAMdF,
     PsiG, dPlasG, dPlasGdT, dPlasGdF, dViscG,dViscGdT, dViscGdF, 
     PG, dPGdT, TangentG,
     PsiR, dPlasR, dPlasRdT, dPlasRdF, dViscR,dViscRdT, dViscRdF, 
     PR,   dPRdT, TangentR,
     PsiAM, dPlasAM, dPlasAMdT, dPlasAMdF, dViscAM, dViscAMdT, dViscAMdF, 
     PAM, dPAMdT,  TangentAM,
     PsiRL2, PRL2, dPRL2dT, TangentRL2,
     PsiAML2, PAML2, dPAML2dT, TangentAML2);
     if(!success)
     {
       Msg::Info("mlawPhenomenologicalSMP::constitutive did not converge");
       P(0,0) = P(1,1) = P(2,2) = sqrt(-1.); // to exist NR and performed next step with a smaller incremene
       return;
     }

     }


    double VFAM2=VFAM;
    double dVFAM2dT=dVFAMdT;
    STensor3 dVFAM2dF(dVFAMdF);

    double VFR2=VFR;
    double dVFR2dT=dVFRdT;
    STensor3 dVFR2dF(dVFRdF);


    P =VFG*PG+ VFR*PR + VFAM*PAM + VFR2*PRL2 + VFAM2*PAML2;

    //Wth & mecSource
    dPlas   = VFG*dPlasG   + VFR*dPlasR   + VFAM*dPlasAM;
    dPlasdT = dVFGdT*dPlasG + VFG*dPlasGdT + dVFRdT*dPlasR + VFR*dPlasRdT + dVFAMdT*dPlasAM + VFAM*dPlasAMdT;
    dPlasdF = dVFGdF*dPlasG + VFG*dPlasGdF + dVFRdF*dPlasR + VFR*dPlasRdF + dVFAMdF*dPlasAM + VFAM*dPlasAMdF;

    dVisc   = VFG*dViscG + VFR*dViscR + VFAM*dViscAM;
    dViscdT = dVFGdT*dViscG + VFG*dViscGdT + dVFRdT*dViscR + VFR*dViscRdT + dVFAMdT*dViscAM + VFAM*dViscAMdT;
    dViscdF = dVFGdF*dViscG + VFG*dViscGdF + dVFRdF*dViscR + VFR*dViscRdF + dVFAMdF*dViscAM + VFAM*dViscAMdF;

    Psi     = VFG*PsiG + VFR*PsiR + VFAM*PsiAM + VFAM2*PsiAML2 + VFR2*PsiRL2;


    STensor3 Fninv, F0inv;
    STensorOperation::zero(Fninv);
    STensorOperation::zero(F0inv);
    STensorOperation::inverseSTensor3(Fn, Fninv);
    STensorOperation::inverseSTensor3(F0, F0inv);

    const STensor3& PG0    =q0->getConstRefToPG();
    const STensor3& PR0    =q0->getConstRefToPR();
    const STensor3& PAM0   =q0->getConstRefToPAM();
    const STensor3& PAML20 =q0->getConstRefToPAML2();
    const STensor3& PRL20 =q0->getConstRefToPRL2();

    STensor3 PmG, PmR, PmAM, PmRL2, PmAML2;
    STensorOperation::multSTensor3SecondTranspose(PG,FthG,PmG);
    STensorOperation::multSTensor3SecondTranspose(PR,FthR,PmR);
    STensorOperation::multSTensor3SecondTranspose(PAM,FthAM,PmAM);
    STensorOperation::multSTensor3SecondTranspose(PAML2,FthAM,PmAML2);
    STensorOperation::multSTensor3SecondTranspose(PRL2,FthR,PmRL2);
    STensor3 PmG0, PmR0, PmAM0, PmAML20, PmRL20;
    STensorOperation::multSTensor3SecondTranspose(PG0,FthG0,PmG0);
    STensorOperation::multSTensor3SecondTranspose(PR0,FthG0,PmR0);
    STensorOperation::multSTensor3SecondTranspose(PAM0,FthAM0,PmAM0);
    STensorOperation::multSTensor3SecondTranspose(PAML20,FthAM0,PmAML20);
    STensorOperation::multSTensor3SecondTranspose(PRL20,FthR0,PmRL20);
    

    double dAppl=0.;
    for(int i = 0; i< 3; i++){
        for(int j = 0; j< 3; j++){
            dAppl+=VFG*(PmG(i,j) + PmG0(i,j))/2.*(FmG(i,j) - FmG0(i,j))
                  +VFR*(PmR(i,j) + PmR0(i,j))/2.*(FmR(i,j) - FmR0(i,j))
                  +VFAM*(PmAM(i,j) + PmAM0(i,j))/2.*(FmAM(i,j) - FmAM0(i,j))
                  +VFAM2*(PmAML2(i,j) + PmAML20(i,j))/2.*(FmAM(i,j) - FmAM0(i,j));
                  +VFR2*(PmRL2(i,j) + PmRL20(i,j))/2.*(FmR(i,j) - FmR0(i,j));
        }
    }

    const double& appliedEnergy0    = q0->getConstRefToAppliedEnergy();
    const double& _SMPEnergy0       = q0->defoEnergy();
    const double& plasticDiss0      = q0->getConstRefToPlasticDiss();
    const double& freezedDiss0      = q0->getConstRefToFreezedDiss();
    const double& viscousDiss0      = q0->getConstRefToViscousDiss();

    double& appliedEnergy       = q1->getRefToAppliedEnergy();
    double& _SMPEnergy          = q1->getRefTodefoEnergy();
    double& plasticDiss         = q1->getRefToPlasticDiss();
    double& freezedDiss         = q1->getRefToFreezedDiss();
    double& viscousDiss         = q1->getRefToViscousDiss();

    appliedEnergy   = appliedEnergy0 +dAppl;
    _SMPEnergy      = Psi;
    //freezedDiss     = freezedDiss0 + dFrzd;
    plasticDiss     = plasticDiss0 + dPlas;
    viscousDiss     = viscousDiss0 + dVisc;

    double dcgdT, dcrdT, dcAMdT;
    static STensor3 dcpdF;
    STensorOperation::zero(dcpdF);
    double cgT  =get_cgT(T,&dcgdT);
    double crT  =get_crT(T,&dcrdT);
    double cAMT =get_cAMT(T,&dcAMdT);
    double cp   =VFG*cgT + VFR*crT + VFAM*cAMT;
    double dcpdT  = dVFGdT*cgT + VFG*dcgdT + dVFRdT*crT + VFR*dcrdT + dVFAMdT*cAMT + VFAM*dcAMdT;

    //damage(q0, q1, T0, T, dh, zg, cp, Fn, F0, P, dPdT, Tangent, dgammataueq, dgammataueqdT, dgammataueqdF);

    if(dh>0.)
    {
        wth         =-cp*(T-T0)/dh;
        mechSource  =(dPlas + dVisc)/dh ;
    }

    if(dh>0.)
    {
        dwthdT        =-dcpdT*(T-T0)/dh-cp/dh;
        dmechSourcedT =(dPlasdT + dViscdT)/dh;
        for(int K = 0; K< 3; K++){
            for(int i = 0; i< 3; i++){
                dwthdF(K,i)        += (-dcpdF(K,i))*(T-T0)/dh;
                dmechSourcedF(K,i) += (dPlasdF(K,i) + dViscdF(K,i))/dh;
            }
        }
    }


    double J= Fn.determinant();
    static STensor3 _Kqref, _kq;
    STensorOperation::zero(_Kqref);
    STensorOperation::zero(_kq);

    static STensor3 kqgT, kqrT, kqAMT, dkqgdT, dkqrdT, dkqAMdT;
    STensorOperation::zero(kqgT);
    STensorOperation::zero(kqrT);
    STensorOperation::zero(kqAMT);
    STensorOperation::zero(dkqgdT);
    STensorOperation::zero(dkqrdT);
    STensorOperation::zero(dkqAMdT);
    get_kqg(T,kqgT,&dkqgdT);
    get_kqr(T,kqrT,&dkqrdT);
    get_kqAM(T,kqAMT,&dkqAMdT);

    _kq = VFG*kqgT + VFR*kqrT + VFAM*kqAMT;
    for(int K = 0; K< 3; K++){
        for(int L = 0; L< 3; L++){
            for(int i = 0; i< 3; i++){
                for(int j = 0; j< 3; j++){
                    _Kqref(K,L) += Fninv(K,i)*_kq(i,j)*Fninv(L,j);
                }
            }
        }
    }
    STensorOperation::multSTensor3SVector3(_Kqref, gradT, fluxT);
    fluxT*=J;

    static STensor3 dkqdT;
    STensorOperation::zero(dkqdT);
    if(stiff)
    {
        dPdT +=dVFGdT*PG + VFG*dPGdT + dVFRdT*PR + VFR*dPRdT + dVFAMdT*PAM + dVFAM2dT*PAML2 + dVFR2dT*PRL2  + VFAM*dPAMdT + VFAM2*dPAML2dT + VFR2*dPRL2dT;
        for(int i = 0; i< 3; i++){
            for(int j = 0; j< 3; j++){
                for(int k = 0; k< 3; k++){
                    for(int l = 0; l< 3; l++){
                        Tangent(i,j,k,l)+=dVFGdF(k,l)*PG(i,j) + VFG*TangentG(i,j,k,l) + dVFRdF(k,l)*PR(i,j) + VFR*TangentR(i,j,k,l)
                                         +dVFAMdF(k,l)*PAM(i,j) + dVFAM2dF(k,l)*PAML2(i,j)+ dVFR2dF(k,l)*PRL2(i,j) + VFAM*TangentAM(i,j,k,l) + VFAM2*TangentAML2(i,j,k,l)
                                          + VFR2*TangentRL2(i,j,k,l);
                    }
                }
            }
        }


        dqdgradT =_Kqref*J;

        dkqdT= dVFGdT*kqgT + VFG*dkqgdT + dVFRdT*kqrT + VFR*dkqrdT + dVFAMdT*kqAMT + VFAM*dkqAMdT;
        STensor3 dKqrefdT;
        for(int K = 0; K< 3; K++){
            for(int m = 0; m< 3; m++){
                for(int N = 0; N< 3; N++){
                    for(int L = 0; L< 3; L++){
                        dKqrefdT(K,m)+= Fninv(K,N)*dkqdT(N,L)*Fninv(m,L);
                    }
                }
            }
        }
        STensorOperation::multSTensor3SVector3(dKqrefdT, gradT, dqdT);
        dqdT*=J;

        for(int K = 0; K< 3; K++){
            for(int m = 0; m< 3; m++){
                for(int N = 0; N< 3; N++){
                    for(int L = 0; L< 3; L++){
                        dqdF(K,m,N) += J*Fninv(N,m)*_Kqref(K,L)*gradT(L) - J*Fninv(K,m)*_Kqref(N,L)*gradT(L)
                                      -J*_Kqref(K,N)*Fninv(L,m)*gradT(L);
                    }
                }
            }
        }
    }

//    std::ofstream PPPout;
//    PPPout.open("PPPout.dat", std::ios::app);
//    PPPout <<getTime()<<'\t'<<P(2,2)<<'\t'<<Pg(2,2)<<'\t'<<Pr(2,2)<<'\t'<<PAM(2,2)<<'\t'<<PAML2(2,2)<<'\t'
//    <<'\t'<<Tangent(2,2,2,2)<<'\t'<<TangentG(2,2,2,2)<<'\t'<<TangentR(2,2,2,2)<<'\t'<<TangentAM(2,2,2,2)<<'\t'<<'\t'<<TangentAML2(2,2,2,2)<<std::endl;
//    PPPout.close();
}
void mlawPhenomenologicalSMP::evaluateFParti(const STensor3 &F, const STensor3 &Ff, const STensor3 &Fth, STensor3 &FVEVP) const
{
   static STensor3 Fthinv, FthinvT, Ffinv, FfinvT; 
   
   STensorOperation::zero(Fthinv);
   STensorOperation::zero(FthinvT);
   STensorOperation::inverseSTensor3(Fth,Fthinv);
   STensorOperation::transposeSTensor3(Fthinv, FthinvT);
   
   STensorOperation::zero(Ffinv);
   STensorOperation::zero(FfinvT);
   STensorOperation::inverseSTensor3(Ff,Ffinv);
   STensorOperation::transposeSTensor3(Ffinv, FfinvT);

   static STensor3 aux_1;
   STensorOperation::zero(aux_1);

   STensorOperation::multSTensor3(F,Fthinv,aux_1);
   STensorOperation::multSTensor3(aux_1,Ffinv,FVEVP);
}

void mlawPhenomenologicalSMP::assemblePParti(const STensor3 &PVEVP, const STensor3 &Ff, const STensor3 &Fth, const STensor3 &F, STensor3 &P) const

{
   static STensor3 Fthinv, FthinvT, Ffinv, FfinvT; 
   
   STensorOperation::zero(Fthinv);
   STensorOperation::zero(FthinvT);
   STensorOperation::inverseSTensor3(Fth,Fthinv);
   STensorOperation::transposeSTensor3(Fthinv, FthinvT);
   
   STensorOperation::zero(Ffinv);
   STensorOperation::zero(FfinvT);
   STensorOperation::inverseSTensor3(Ff,Ffinv);
   STensorOperation::transposeSTensor3(Ffinv, FfinvT);

   static STensor3 aux_1;
   STensorOperation::zero(aux_1);
   
   STensorOperation::multSTensor3(PVEVP,  FfinvT,     aux_1);
   STensorOperation::multSTensor3(aux_1, FthinvT, P);
}


void mlawPhenomenologicalSMP::assembleTangentParti(const STensor3 &PVEVP, const STensor43 &dPVEVPdFVEVP,const STensor3 &dPVEVPdT, const STensor3 &Ff, const STensor3 &Fth, const STensor43 &dFthdF, const STensor3 &dFthdT, const STensor3 &F, STensor43 &dPdF, STensor3 &dPdT) const

{
    static STensor3 Fthinv, FthinvT, Ffinv, FfinvT; 
    STensorOperation::zero(Fthinv);
    STensorOperation::zero(FthinvT); 
    STensorOperation::inverseSTensor3(Fth,Fthinv); 
    STensorOperation::transposeSTensor3(Fthinv, FthinvT);
   
    STensorOperation::zero(Ffinv);
    STensorOperation::zero(FfinvT);
    STensorOperation::inverseSTensor3(Ff,Ffinv);
    STensorOperation::transposeSTensor3(Ffinv, FfinvT);
 
    static STensor3 FthinvFfinv;
    STensorOperation::zero(FthinvFfinv);
    STensorOperation::multSTensor3(Fthinv,Ffinv,FthinvFfinv);

    static STensor3 FfinvTFthinvT;
    STensorOperation::zero(FfinvTFthinvT);
    STensorOperation::multSTensor3(FfinvT,FthinvT,FfinvTFthinvT);

   
    static STensor43 dFthinvdF;
    static STensor3 dFthinvdT;
    STensorOperation::zero(dFthinvdF);
    STensorOperation::zero(dFthinvdT);
    for (int i=0; i<3; i++){
        for (int j=0; j<3; j++){
            for (int p=0; p<3; p++){
                for (int q=0; q<3; q++){
                    dFthinvdT(i,j) -= Fthinv(i,p)*dFthdT(p,q)*Fthinv(q,j);
                     for (int k=0; k<3; k++){
                        for (int l=0; l<3; l++){
                            dFthinvdF(i,j,k,l) -= Fthinv(i,p)*dFthdF(p,q,k,l)*Fthinv(q,j);
                        }
                    }
                }
            }
        }
    }
    
    
    static STensor43 dFVEVPdF;
    STensorOperation::zero(dFVEVPdF);
    static STensor3 dFVEVPdT;
    STensorOperation::zero(dFVEVPdT);

    for(int i=0; i<3; i++)
    {
      for(int J=0; J<3; J++)
      {
        for(int k=0; k<3; k++)
        {
          for(int L=0; L<3; L++)
          {
             dFVEVPdF(i,J,k,L)=_I2(i,k)*FthinvFfinv(L,J);
             for(int m=0; m<3; m++)
             { 
               for(int o=0; o<3; o++)
               { 
                 dFVEVPdF(i,J,k,L)+= F(i,m)*dFthinvdF(m,o,k,L)*Ffinv(o,J);
               }
             }
             dFVEVPdT(i,J)+=F(i,L)*dFthinvdT(L,k)*Ffinv(k,J);
          }
        }
      }
    }
    
    for(int i=0; i<3; i++)
    {
      for(int J=0; J<3; J++)
      {
        for(int k=0; k<3; k++)
        {
          for(int L=0; L<3; L++)
          {
            dPdF(i,J,k,L)=0.;
            for(int M=0; M<3; M++)
            {
              for(int o=0; o<3; o++)
              {
                for(int P=0; P<3; P++)
                {
                     dPdF(i,J,k,L)+=dPVEVPdFVEVP(i,M,o,P)*dFVEVPdF(o,P,k,L)*FfinvTFthinvT(M,J);
                }
                dPdF(i,J,k,L)+= PVEVP(i,M)*FfinvT(M,o)*dFthinvdF(J,o,k,L);
              }
            }
          }
        }
      }
    }
    
    for(int i=0; i<3; i++)
    {
      for(int J=0; J<3; J++)
      {
        dPdT(i,J)=0.;
        for(int M=0; M<3; M++)
        {
          dPdT(i,J)+=dPVEVPdT(i,M)*FfinvTFthinvT(M,J);
          for(int o=0; o<3; o++)
          {
            dPdT(i,J)+= PVEVP(i,M)*FfinvT(M,o)*dFthinvdT(J,o);
            dPdT(i,J)+= dPVEVPdFVEVP(i,J,o,M)*dFVEVPdT(o,M);
          }
        }
      }
    }
} 
void mlawPhenomenologicalSMP::assembleDDissParti(const STensor3 &dDissVEVPdFVEVP, double dDissVEVPdT, const STensor3 &Ff, const STensor3 &Fth, const STensor43 &dFthdF, const STensor3 &dFthdT, const STensor3 &F, STensor3 &dDissdF, double &dDissdT) const
{
    static STensor3 Fthinv, Ffinv; 
    STensorOperation::zero(Fthinv);
    STensorOperation::inverseSTensor3(Fth,Fthinv);
    STensorOperation::zero(Ffinv);
    STensorOperation::inverseSTensor3(Ff,Ffinv);
   
    static STensor43 dFthinvdF;
    static STensor3 dFthinvdT;
    STensorOperation::zero(dFthinvdF);
    STensorOperation::zero(dFthinvdT);
    for (int i=0; i<3; i++){
        for (int j=0; j<3; j++){
            for (int p=0; p<3; p++){
                for (int q=0; q<3; q++){
                    dFthinvdT(i,j) -= Fthinv(i,p)*dFthdT(p,q)*Fthinv(q,j);
                     for (int k=0; k<3; k++){
                        for (int l=0; l<3; l++){
                            dFthinvdF(i,j,k,l) -= Fthinv(i,p)*dFthdF(p,q,k,l)*Fthinv(q,j);
                        }
                    }
                }
            }
        }
    }

    static STensor3 FthinvFfinv;
    STensorOperation::zero(FthinvFfinv);
    STensorOperation::multSTensor3(Fthinv,Ffinv,FthinvFfinv);
        
    static STensor43 dFVEVPdF;
    STensorOperation::zero(dFVEVPdF);
    static STensor3 dFVEVPdT;
    STensorOperation::zero(dFVEVPdT);

    for(int i=0; i<3; i++)
    {
      for(int J=0; J<3; J++)
      {
        for(int k=0; k<3; k++)
        {
          for(int L=0; L<3; L++)
          {
             dFVEVPdF(i,J,k,L)=_I2(i,k)*FthinvFfinv(L,J);
             for(int m=0; m<3; m++)
             { 
               for(int o=0; o<3; o++)
               { 
                 dFVEVPdF(i,J,k,L)+= F(i,m)*dFthinvdF(m,o,k,L)*Ffinv(o,J);
               }
             }
             dFVEVPdT(i,J)+=F(i,L)*dFthinvdT(L,k)*Ffinv(k,J);
          }
        }
      }
    }

    for(int i=0; i<3; i++)
    {
      for(int J=0; J<3; J++)
      {
        dDissdF(i,J)=0.;
        for(int k=0; k<3; k++)
        {
          for(int L=0; L<3; L++)
          {
            dDissdF(i,J)+=dDissVEVPdFVEVP(k,L)*dFVEVPdF(k,L,i,J);
          }
        }
      }
    }
    
    dDissdT=dDissVEVPdT;
    for(int i=0; i<3; i++)
    {
      for(int J=0; J<3; J++)
      {
        dDissdT+=dDissVEVPdFVEVP(i,J)*dFVEVPdT(i,J);
      }
    }

}
    

bool mlawPhenomenologicalSMP::constitutive1
    (const IPPhenomenologicalSMP *q0, IPPhenomenologicalSMP *q1,
     const double T0, const double T, const bool stiff, const STensor3& F0, const STensor3& Fn, 
     const STensor3& FthG0, const STensor3& FthG, const STensor3& FmG0, const STensor3& FmG, const STensor3 &dFthGdT, const STensor43 &dFthGdF,  
     const STensor3& FthR0, const STensor3& FthR, const STensor3& FmR0, const STensor3& FmR, const STensor3 &dFthRdT, const STensor43 &dFthRdF, 
     const STensor3& FthAM0, const STensor3& FthAM, const STensor3& FmAM0, const STensor3& FmAM, const STensor3 &dFthAMdT,  const STensor43 &dFthAMdF,
     double& PsiG, double &dPlasG, double& dPlasGdT, STensor3& dPlasGdF, double &dViscG, double& dViscGdT, STensor3& dViscGdF, 
     STensor3& PG, STensor3& dPGdT,   STensor43& TangentG,
     double& PsiR, double &dPlasR, double& dPlasRdT, STensor3& dPlasRdF, double &dViscR, double& dViscRdT, STensor3& dViscRdF, 
     STensor3& PR,    STensor3& dPRdT, STensor43& TangentR,
     double& PsiAM, double &dPlasAM, double& dPlasAMdT, STensor3& dPlasAMdF, double &dViscAM, double& dViscAMdT, STensor3& dViscAMdF, 
     STensor3 &PAM, STensor3 &dPAMdT,  STensor43 &TangentAM,
     double &PsiRL2, STensor3 &PRL2, STensor3 &dPRL2dT, STensor43 &TangentRL2,
     double &PsiAML2, STensor3 &PAML2, STensor3 &dPAML2dT, STensor43 &TangentAML2) const

{
     double anisotropicThermalCoefAmplitudeBulkG=(_alphaParam)[0];
     double anisotropicThermalCoefAmplitudeShearG=(_alphaParam)[4];
     double anisotropicThermalAlphaCoefTanhTempBulkG=(_alphaParam)[8];
     double anisotropicThermalAlphaCoefTanhTempShearG=(_alphaParam)[12];

     double anisotropicThermalCoefAmplitudeBulkR=(_alphaParam)[1];
     double anisotropicThermalCoefAmplitudeShearR=(_alphaParam)[5];
     double anisotropicThermalAlphaCoefTanhTempBulkR=(_alphaParam)[9];
     double anisotropicThermalAlphaCoefTanhTempShearR=(_alphaParam)[13];

     double anisotropicThermalCoefAmplitudeBulkAM=(_alphaParam)[3];
     double anisotropicThermalCoefAmplitudeShearAM=(_alphaParam)[7];
     double anisotropicThermalAlphaCoefTanhTempBulkAM=(_alphaParam)[11];
     double anisotropicThermalAlphaCoefTanhTempShearAM=(_alphaParam)[15];
        
    // Initialisation of internal variables and material properties
    double &Tdot            =q1->getRefToTdot();

    //Glassy: reset internal variables
    const STensor3 &FfG0 =q0->getConstRefToFfG();
    STensor3 &FfG        =q1->getRefToFfG();
    STensor3 &FpG        =q1->getRefToFpG();
    q1->resetAG();
    q1->resetBG();
    q1->resetPlasticityG();
    q1->getRefToTRefG()=T;
    q1->getRefToTRefR()=q0->getConstRefToTRefR();

    static STensor3 subs1;
    STensorOperation::zero(subs1);
    subs1 =  FmR;
    subs1-= _I2;
    FfG   = subs1;
    FfG  *=_cfG;
    FfG  +=_I2;
    //FfG  =_I2;
    subs1 = FfG;
//    if(T>_t0){
//        FfG=FfG0;
//    }

    //Rubbery
    const STensor3 &FfR0    =q0->getConstRefToFfR();
    STensor3 &FfR           =q1->getRefToFfR();
    FfR  =FfR0;
    
    static STensor3 FR0, FRn, PVEVPR, dPVEVPRdT;
    static STensor43 dPVEVPRdFR;
    STensorOperation::zero(PVEVPR);
    STensorOperation::zero(dPVEVPRdT);
    STensorOperation::zero(dPVEVPRdFR);
    STensorOperation::zero(FR0);
    STensorOperation::zero(FRn);
    
    evaluateFParti(Fn, FfR, FthR, FRn);
    evaluateFParti(F0, FfR0, FthR0, FR0);

    double wp=0., dwpdz=0.;
    getZgFunction_Sy0(*_yieldCrystallinityR,q0->getzg(), wp, dwpdz);
    q1->getRefToIPHyperViscoElastoPlasticR()._ipCompression->setScaleFactor(wp,0.);
    q1->getRefToIPHyperViscoElastoPlasticR()._ipTraction->setScaleFactor(wp,0.);
    funEnThAI(T0, q1->getConstRefToTRefR(), q0->getConstRefToIPHyperViscoElastoPlasticR(),q1->getRefToIPHyperViscoElastoPlasticR(),
       anisotropicThermalCoefAmplitudeBulkR, anisotropicThermalCoefAmplitudeShearR, 
       anisotropicThermalAlphaCoefTanhTempBulkR, anisotropicThermalAlphaCoefTanhTempShearR);
      
    
    lawR->constitutive(FR0,FRn, PVEVPR, &(q0->getConstRefToIPHyperViscoElastoPlasticR()), &(q1->getRefToIPHyperViscoElastoPlasticR()),dPVEVPRdFR,stiff);
    if (STensorOperation::isnan(PVEVPR))
    {
      Msg::Info("mlawPhenomenologicalSMP::constitutive1 did not converge");
      return false;
    
    }
    
    assemblePParti(PVEVPR, FfR, FthR, Fn, PR);
    assembleTangentParti(PR, dPVEVPRdFR, dPVEVPRdT, FfR, FthR, dFthRdF, dFthRdT, Fn, TangentR, dPRdT);

    PsiR=q1->getConstRefToElasticEnergyPartR();

    dPlasR=q1->getConstRefToPlasticEnergyPartR()-q0->getConstRefToPlasticEnergyPartR();
    static STensor3 dPlasVEVPRdFVEVP;
    dPlasVEVPRdFVEVP=q1->getConstRefToDPlasticEnergyPartRdF();
    double dPlasVEVPRdT=0.;
    assembleDDissParti(dPlasVEVPRdFVEVP, dPlasVEVPRdT, FfR, FthR, dFthRdF, dFthRdT, Fn, dPlasRdF, dPlasRdT);

    dViscR=q1->getConstRefToViscousEnergyPartR()-q0->getConstRefToViscousEnergyPartR();
    static STensor3 dViscVEVPRdFVEVP;
    dViscVEVPRdFVEVP=q1->getConstRefToDViscousEnergyPartRdF();
    double dViscVEVPRdT=0.;
    assembleDDissParti(dViscVEVPRdFVEVP, dViscVEVPRdT, FfR, FthR, dFthRdF, dFthRdT, Fn, dViscRdF, dViscRdT);



    //Amorphous
    const STensor3 &FfAM0    =q0->getConstRefToFfAM();
    STensor3 &FfAM           =q1->getRefToFfAM();
    FfAM  =FfAM0;
    
    static STensor3 FAM0, FAMn, PVEVPAM, dPVEVPAMdT;
    static STensor43 dPVEVPAMdFAM;
    STensorOperation::zero(PVEVPAM);
    STensorOperation::zero(dPVEVPAMdT);
    STensorOperation::zero(dPVEVPAMdFAM);
    STensorOperation::zero(FAM0);
    STensorOperation::zero(FAMn);
    
    evaluateFParti(Fn, FfAM, FthAM, FAMn);
    evaluateFParti(F0, FfAM0, FthAM0, FAM0);

    getZgFunction_Sy0(*_yieldCrystallinityAM,q0->getzg(), wp, dwpdz);
    q1->getRefToIPHyperViscoElastoPlasticAM()._ipCompression->setScaleFactor(wp,0.);
    q1->getRefToIPHyperViscoElastoPlasticAM()._ipTraction->setScaleFactor(wp,0.);
    funEnThAI(T0, q1->getConstRefToTRefAM(), q0->getConstRefToIPHyperViscoElastoPlasticAM(),q1->getRefToIPHyperViscoElastoPlasticAM(),
       anisotropicThermalCoefAmplitudeBulkAM, anisotropicThermalCoefAmplitudeShearAM, 
       anisotropicThermalAlphaCoefTanhTempBulkAM, anisotropicThermalAlphaCoefTanhTempShearAM);
    
    lawAM->constitutive(FAM0,FAMn, PVEVPAM, &(q0->getConstRefToIPHyperViscoElastoPlasticAM()), &(q1->getRefToIPHyperViscoElastoPlasticAM()),dPVEVPAMdFAM,stiff);
    if (STensorOperation::isnan(PVEVPAM))
    {
      Msg::Info("mlawPhenomenologicalSMP::constitutive1 did not converge");
      return false;
    
    }
    assemblePParti(PVEVPAM, FfAM, FthAM, Fn, PAM);
    assembleTangentParti(PAM, dPVEVPAMdFAM, dPVEVPAMdT, FfAM, FthAM, dFthAMdF, dFthAMdT, Fn, TangentAM, dPAMdT);


    PsiAM=q1->getConstRefToElasticEnergyPartAM();

    dPlasAM=q1->getConstRefToPlasticEnergyPartAM()-q0->getConstRefToPlasticEnergyPartAM();
    static STensor3 dPlasVEVPAMdFVEVP;
    dPlasVEVPAMdFVEVP=q1->getConstRefToDPlasticEnergyPartAMdF();
    double dPlasVEVPAMdT=0.;
    assembleDDissParti(dPlasVEVPAMdFVEVP, dPlasVEVPAMdT, FfAM, FthAM, dFthAMdF, dFthAMdT, Fn, dPlasAMdF, dPlasAMdT);

    dViscAM=q1->getConstRefToViscousEnergyPartAM()-q0->getConstRefToViscousEnergyPartAM();
    static STensor3 dViscVEVPAMdFVEVP;
    dViscVEVPAMdFVEVP=q1->getConstRefToDViscousEnergyPartAMdF();
    double dViscVEVPAMdT=0.;
    assembleDDissParti(dViscVEVPAMdFVEVP, dViscVEVPAMdT, FfAM, FthAM, dFthAMdF, dFthAMdT, Fn, dViscAMdF, dViscAMdT);


    //p-R: polynomial 2nd degree
    static STensor3 FRL20, FRL2n, PVEVPRL2, dPVEVPRL2dT;
    static STensor43 dPVEVPRL2dFRL2;
    STensorOperation::zero(PVEVPRL2);
    STensorOperation::zero(dPVEVPRL2dT);
    STensorOperation::zero(dPVEVPRL2dFRL2);
    STensorOperation::zero(FRL20);
    STensorOperation::zero(FRL2n);
    
    evaluateFParti(Fn, FfR, FthR, FRL2n);
    evaluateFParti(F0, FfR0, FthR0, FRL20);
    if(_EPFunc_R!=NULL)
    {
      _EPFunc_R->constitutive(FRL2n, PVEVPRL2, stiff, &dPVEVPRL2dFRL2);
      PsiRL2= _EPFunc_R->get(FRL2n);
      assemblePParti(PVEVPRL2, FfR, FthR, Fn, PRL2);
      assembleTangentParti(PRL2, dPVEVPRL2dFRL2, dPVEVPRL2dT, FfR, FthR, dFthRdF, dFthRdT, Fn, TangentRL2, dPRL2dT);
    }

    //p-AM: polynomial 2nd degree
    static STensor3 FAML20, FAML2n, PVEVPAML2, dPVEVPAML2dT;
    static STensor43 dPVEVPAML2dFAML2;
    STensorOperation::zero(PVEVPAML2);
    STensorOperation::zero(dPVEVPAML2dT);
    STensorOperation::zero(dPVEVPAML2dFAML2);
    STensorOperation::zero(FAML20);
    STensorOperation::zero(FAML2n);
    
    evaluateFParti(Fn, FfAM, FthAM, FAML2n);
    evaluateFParti(F0, FfAM0, FthAM0, FAML20);

    if(_EPFunc_AM!=NULL)
    {
      _EPFunc_AM->constitutive(FAML2n, PVEVPAML2, stiff, &dPVEVPAML2dFAML2);
      if (STensorOperation::isnan(PVEVPAML2))
      {
        Msg::Info("mlawPhenomenologicalSMP::constitutive1 did not converge");
        return false;
      } 
      PsiAML2= _EPFunc_AM->get(FAML2n);
      assemblePParti(PVEVPAML2, FfAM, FthAM, Fn, PAML2);
      assembleTangentParti(PAML2, dPVEVPAML2dFAML2, dPVEVPAML2dT, FfAM, FthAM, dFthAMdF, dFthAMdT, Fn, TangentAML2, dPAML2dT);
    }   
    //if(stiff)
    //{
    //    tangentamorphousEP(Fn, F0, FmAM, stiff, FthAM, dFthAMdT, dFthAMdF, PsiAML2, PAML2, dPAML2dT, TangentAML2);
    //}


    //internal variables

    STensor3 N1N1R, N2N2R, N3N3R;
    double alphaAD1, alphaAD2, alphaAD3;
    PhaseTransitionCrystallisationParameters(q0, q1, N1N1R, N2N2R, N3N3R, alphaAD1, alphaAD2, alphaAD3);

    STensor3 &ADl =q1->getRefToADl();
    ADl = alphaAD1*N1N1R + alphaAD2*N2N2R + alphaAD3*N3N3R;

    const STensor3& FthAD0  =q0->getConstRefToFthAD();
    STensor3& FthAD         =q1->getRefToFthAD();
    FthAD =FthAD0;

    funFthAD(q0, q1, ADl,FthAD0,FthAD);
    //FfG=subs1;
    STensorOperation::multSTensor3(subs1, FthAD, FfG);
    



    STensor3 &FveG  =q1->getRefToFveG();
    static STensor3 FthGinv, FfGinv, FpGinv;
    STensorOperation::zero(FthGinv);
    STensorOperation::zero(FfGinv);
    STensorOperation::zero(FpGinv);
    STensorOperation::inverseSTensor3(FthG, FthGinv);
    STensorOperation::inverseSTensor3(FfG,  FfGinv);
    STensorOperation::inverseSTensor3(FpG,  FpGinv);

    static STensor3 Fvei_int, Fvei_int2;
    STensorOperation::zero(Fvei_int);
    STensorOperation::zero(Fvei_int2);
    STensorOperation::multSTensor3(Fn, FthGinv, Fvei_int);
    STensorOperation::multSTensor3(Fvei_int,  FfGinv,  Fvei_int2);
    STensorOperation::multSTensor3(Fvei_int2, FpGinv,  FveG);

    return true;
}

bool mlawPhenomenologicalSMP::constitutive2
    (const IPPhenomenologicalSMP *q0, IPPhenomenologicalSMP *q1,
     const double T0, const double T, const bool stiff, const STensor3& F0, const STensor3& Fn, 
     const STensor3& FthG0, const STensor3& FthG, const STensor3& FmG0, const STensor3& FmG, const STensor3 &dFthGdT, const STensor43 &dFthGdF,  
     const STensor3& FthR0, const STensor3& FthR, const STensor3& FmR0, const STensor3& FmR, const STensor3 &dFthRdT, const STensor43 &dFthRdF, 
     const STensor3& FthAM0, const STensor3& FthAM, const STensor3& FmAM0, const STensor3& FmAM, const STensor3 &dFthAMdT,  const STensor43 &dFthAMdF,
     double& PsiG, double &dPlasG, double& dPlasGdT, STensor3& dPlasGdF, double &dViscG, double& dViscGdT, STensor3& dViscGdF, 
     STensor3& PG, STensor3& dPGdT,   STensor43& TangentG,
     double& PsiR, double &dPlasR, double& dPlasRdT, STensor3& dPlasRdF, double &dViscR, double& dViscRdT, STensor3& dViscRdF, 
     STensor3& PR,    STensor3& dPRdT, STensor43& TangentR,
     double& PsiAM, double &dPlasAM, double& dPlasAMdT, STensor3& dPlasAMdF, double &dViscAM, double& dViscAMdT, STensor3& dViscAMdF, 
     STensor3 &PAM, STensor3 &dPAMdT,  STensor43 &TangentAM,
     double &PsiRL2, STensor3 &PRL2, STensor3 &dPRL2dT, STensor43 &TangentRL2,
     double &PsiAML2, STensor3 &PAML2, STensor3 &dPAML2dT, STensor43 &TangentAML2) const
{
// Initialisation of internal variables and material properties
     double anisotropicThermalCoefAmplitudeBulkG=(_alphaParam)[0];
     double anisotropicThermalCoefAmplitudeShearG=(_alphaParam)[4];
     double anisotropicThermalAlphaCoefTanhTempBulkG=(_alphaParam)[8];
     double anisotropicThermalAlphaCoefTanhTempShearG=(_alphaParam)[12];

     double anisotropicThermalCoefAmplitudeBulkR=(_alphaParam)[1];
     double anisotropicThermalCoefAmplitudeShearR=(_alphaParam)[5];
     double anisotropicThermalAlphaCoefTanhTempBulkR=(_alphaParam)[9];
     double anisotropicThermalAlphaCoefTanhTempShearR=(_alphaParam)[13];

     double anisotropicThermalCoefAmplitudeBulkAM=(_alphaParam)[3];
     double anisotropicThermalCoefAmplitudeShearAM=(_alphaParam)[7];
     double anisotropicThermalAlphaCoefTanhTempBulkAM=(_alphaParam)[11];
     double anisotropicThermalAlphaCoefTanhTempShearAM=(_alphaParam)[15];

    double &Tdot         =q1->getRefToTdot();
    const STensor3& FthAD0  =q0->getConstRefToFthAD();
    STensor3& FthAD         =q1->getRefToFthAD();
    FthAD =FthAD0;
    q1->getRefToTRefG()=q0->getConstRefToTRefG();

    //Glassy    
    const STensor3 &FfG0 =q0->getConstRefToFfG();
    STensor3 &FfG        =q1->getRefToFfG();
    FfG    = FfG0;
       

    static STensor3 FG0, FGn, PVEVPG, dPVEVPGdT;
    static STensor43 dPVEVPGdFG;
    STensorOperation::zero(PVEVPG);
    STensorOperation::zero(dPVEVPGdT);
    STensorOperation::zero(dPVEVPGdFG);
    STensorOperation::zero(FG0);
    STensorOperation::zero(FGn);
    
    evaluateFParti(Fn, FfG, FthG, FGn);
    evaluateFParti(F0, FfG0, FthG0, FG0);
    
    double wp=0., dwpdz=0.;
    getZgFunction_Sy0(*_yieldCrystallinityG,q0->getzg(), wp, dwpdz);
    q1->getRefToIPHyperViscoElastoPlasticG()._ipCompression->setScaleFactor(wp,0.);
    q1->getRefToIPHyperViscoElastoPlasticG()._ipTraction->setScaleFactor(wp,0.);
    
    funEnThAI(T0, q1->getConstRefToTRefG(), q0->getConstRefToIPHyperViscoElastoPlasticG(),q1->getRefToIPHyperViscoElastoPlasticG(),
       anisotropicThermalCoefAmplitudeBulkG, anisotropicThermalCoefAmplitudeShearG, 
       anisotropicThermalAlphaCoefTanhTempBulkG, anisotropicThermalAlphaCoefTanhTempShearG);

    
    lawG->constitutive(FG0,FGn, PVEVPG, &(q0->getConstRefToIPHyperViscoElastoPlasticG()), &(q1->getRefToIPHyperViscoElastoPlasticG()),dPVEVPGdFG,stiff);
      if (STensorOperation::isnan(PVEVPG))
      {
        Msg::Info("mlawPhenomenologicalSMP::constitutive2 did not converge");
        return false;
      } 
    
    assemblePParti(PVEVPG, FfG, FthG, Fn, PG);
    assembleTangentParti(PG, dPVEVPGdFG, dPVEVPGdT, FfG, FthG, dFthGdF, dFthGdT, Fn, TangentG, dPGdT);

    PsiG=q1->getConstRefToElasticEnergyPartG();

    dPlasG=q1->getConstRefToPlasticEnergyPartG()-q0->getConstRefToPlasticEnergyPartG();
    static STensor3 dPlasVEVPGdFVEVP;
    dPlasVEVPGdFVEVP=q1->getConstRefToDPlasticEnergyPartGdF();
    double dPlasVEVPGdT=0.;
    assembleDDissParti(dPlasVEVPGdFVEVP, dPlasVEVPGdT, FfG, FthG, dFthGdF, dFthGdT, Fn, dPlasGdF, dPlasGdT);

    dViscG=q1->getConstRefToViscousEnergyPartG()-q0->getConstRefToViscousEnergyPartG();
    static STensor3 dViscVEVPGdFVEVP;
    dViscVEVPGdFVEVP=q1->getConstRefToDViscousEnergyPartGdF();
    double dViscVEVPGdT=0.;
    assembleDDissParti(dViscVEVPGdFVEVP, dViscVEVPGdT, FfG, FthG, dFthGdF, dFthGdT, Fn, dViscGdF, dViscGdT);



    //Rubbery
    const STensor3& FpR0 =q0->getConstRefToFpR();
    STensor3 &FpR        =q1->getRefToFpR();
    q1->getRefToTRefR()=q0->getConstRefToTRefR();
    const STensor3 &FfR0 =q0->getConstRefToFfR();
    STensor3 &FfR        =q1->getRefToFfR();
    FfR    =FfR0;

    static STensor3 FR0, FRn, PVEVPR, dPVEVPRdT;
    static STensor43 dPVEVPRdFR;
    STensorOperation::zero(PVEVPR);
    STensorOperation::zero(dPVEVPRdT);
    STensorOperation::zero(dPVEVPRdFR);
    STensorOperation::zero(FR0);
    STensorOperation::zero(FRn);
    
    evaluateFParti(Fn, FfR, FthR, FRn);
    evaluateFParti(F0, FfR0, FthR0, FR0);

    getZgFunction_Sy0(*_yieldCrystallinityR,q0->getzg(), wp, dwpdz);
    q1->getRefToIPHyperViscoElastoPlasticR()._ipCompression->setScaleFactor(wp,0.);
    q1->getRefToIPHyperViscoElastoPlasticR()._ipTraction->setScaleFactor(wp,0.);

    funEnThAI(T0, q1->getConstRefToTRefR(), q0->getConstRefToIPHyperViscoElastoPlasticR(),q1->getRefToIPHyperViscoElastoPlasticR(),
       anisotropicThermalCoefAmplitudeBulkR, anisotropicThermalCoefAmplitudeShearR, 
       anisotropicThermalAlphaCoefTanhTempBulkR, anisotropicThermalAlphaCoefTanhTempShearR);

    
    lawR->constitutive(FR0,FRn, PVEVPR, &(q0->getConstRefToIPHyperViscoElastoPlasticR()), &(q1->getRefToIPHyperViscoElastoPlasticR()),dPVEVPRdFR,stiff);
      if (STensorOperation::isnan(PVEVPR))
      {
        Msg::Info("mlawPhenomenologicalSMP::constitutive2 did not converge");
        return false;
      } 
    
    assemblePParti(PVEVPR, FfR, FthR, Fn, PR);
    assembleTangentParti(PR, dPVEVPRdFR, dPVEVPRdT, FfR, FthR, dFthRdF, dFthRdT, Fn, TangentR, dPRdT);    

    PsiR=q1->getConstRefToElasticEnergyPartR();

    dPlasR=q1->getConstRefToPlasticEnergyPartR()-q0->getConstRefToPlasticEnergyPartR();
    static STensor3 dPlasVEVPRdFVEVP;
    dPlasVEVPRdFVEVP=q1->getConstRefToDPlasticEnergyPartRdF();
    double dPlasVEVPRdT=0.;
    assembleDDissParti(dPlasVEVPRdFVEVP, dPlasVEVPRdT, FfR, FthR, dFthRdF, dFthRdT, Fn, dPlasRdF, dPlasRdT);

    dViscR=q1->getConstRefToViscousEnergyPartR()-q0->getConstRefToViscousEnergyPartR();
    static STensor3 dViscVEVPRdFVEVP;
    dViscVEVPRdFVEVP=q1->getConstRefToDViscousEnergyPartRdF();
    double dViscVEVPRdT=0.;
    assembleDDissParti(dViscVEVPRdFVEVP, dViscVEVPRdT, FfR, FthR, dFthRdF, dFthRdT, Fn, dViscRdF, dViscRdT);

     //Amorphous
    const STensor3 &FfAM0    =q0->getConstRefToFfAM();
    STensor3 &FfAM           =q1->getRefToFfAM();
    FfAM  =FfAM0;
    
    static STensor3 FAM0, FAMn, PVEVPAM, dPVEVPAMdT;
    static STensor43 dPVEVPAMdFAM;
    STensorOperation::zero(PVEVPAM);
    STensorOperation::zero(dPVEVPAMdT);
    STensorOperation::zero(dPVEVPAMdFAM);
    STensorOperation::zero(FAM0);
    STensorOperation::zero(FAMn);
    
    evaluateFParti(Fn, FfAM, FthAM, FAMn);
    evaluateFParti(F0, FfAM0, FthAM0, FAM0);

    getZgFunction_Sy0(*_yieldCrystallinityAM,q0->getzg(), wp, dwpdz);
    q1->getRefToIPHyperViscoElastoPlasticAM()._ipCompression->setScaleFactor(wp,0.);
    q1->getRefToIPHyperViscoElastoPlasticAM()._ipTraction->setScaleFactor(wp,0.);

    funEnThAI(T0, q1->getConstRefToTRefAM(), q0->getConstRefToIPHyperViscoElastoPlasticAM(),q1->getRefToIPHyperViscoElastoPlasticAM(),
       anisotropicThermalCoefAmplitudeBulkAM, anisotropicThermalCoefAmplitudeShearAM, 
       anisotropicThermalAlphaCoefTanhTempBulkAM, anisotropicThermalAlphaCoefTanhTempShearAM);

    lawAM->constitutive(FAM0,FAMn, PVEVPAM, &(q0->getConstRefToIPHyperViscoElastoPlasticAM()), &(q1->getRefToIPHyperViscoElastoPlasticAM()),dPVEVPAMdFAM,stiff);
      if (STensorOperation::isnan(PVEVPAM))
      {
        Msg::Info("mlawPhenomenologicalSMP::constitutive1 did not converge");
        return false;
      } 

    assemblePParti(PVEVPAM, FfAM, FthAM, Fn, PAM);
    assembleTangentParti(PAM, dPVEVPAMdFAM, dPVEVPAMdT, FfAM, FthAM, dFthAMdF, dFthAMdT, Fn, TangentAM, dPAMdT);

    PsiAM=q1->getConstRefToElasticEnergyPartAM();

    dPlasAM=q1->getConstRefToPlasticEnergyPartAM()-q0->getConstRefToPlasticEnergyPartAM();
    static STensor3 dPlasVEVPAMdFVEVP;
    dPlasVEVPAMdFVEVP=q1->getConstRefToDPlasticEnergyPartAMdF();
    double dPlasVEVPAMdT=0.;
    assembleDDissParti(dPlasVEVPAMdFVEVP, dPlasVEVPAMdT, FfAM, FthAM, dFthAMdF, dFthAMdT, Fn, dPlasAMdF, dPlasAMdT);

    dViscAM=q1->getConstRefToViscousEnergyPartAM()-q0->getConstRefToViscousEnergyPartAM();
    static STensor3 dViscVEVPAMdFVEVP;
    dViscVEVPAMdFVEVP=q1->getConstRefToDViscousEnergyPartAMdF();
    double dViscVEVPAMdT=0.;
    assembleDDissParti(dViscVEVPAMdFVEVP, dViscVEVPAMdT, FfAM, FthAM, dFthAMdF, dFthAMdT, Fn, dViscAMdF, dViscAMdT);

    //p-R: polynomial 2nd degree
    static STensor3 FRL20, FRL2n, PVEVPRL2, dPVEVPRL2dT;
    static STensor43 dPVEVPRL2dFRL2;
    STensorOperation::zero(PVEVPRL2);
    STensorOperation::zero(dPVEVPRL2dT);
    STensorOperation::zero(dPVEVPRL2dFRL2);
    STensorOperation::zero(FRL20);
    STensorOperation::zero(FRL2n);
    
    evaluateFParti(Fn, FfR, FthR, FRL2n);
    evaluateFParti(F0, FfR0, FthR0, FRL20);
   
    if(_EPFunc_R!=NULL)
    {
      _EPFunc_R->constitutive(FRL2n, PVEVPRL2, stiff, &dPVEVPRL2dFRL2);
      if (STensorOperation::isnan(PVEVPRL2))
      {
        Msg::Info("mlawPhenomenologicalSMP::constitutive2 did not converge");
        return false;
      } 
      PsiRL2= _EPFunc_R->get(FRL2n);
      assemblePParti(PVEVPRL2, FfR, FthR, Fn, PRL2);
      assembleTangentParti(PRL2, dPVEVPRL2dFRL2, dPVEVPRL2dT, FfR, FthR, dFthRdF, dFthRdT, Fn, TangentRL2, dPRL2dT);
    }

    //p-AM: polynomial 2nd degree
    static STensor3 FAML20, FAML2n, PVEVPAML2, dPVEVPAML2dT;
    static STensor43 dPVEVPAML2dFAML2;
    STensorOperation::zero(PVEVPAML2);
    STensorOperation::zero(dPVEVPAML2dT);
    STensorOperation::zero(dPVEVPAML2dFAML2);
    STensorOperation::zero(FAML20);
    STensorOperation::zero(FAML2n);
    
    evaluateFParti(Fn, FfAM, FthAM, FAML2n);
    evaluateFParti(F0, FfAM0, FthAM0, FAML20);

    if(_EPFunc_AM!=NULL)
    {
      _EPFunc_AM->constitutive(FAML2n, PVEVPAML2, stiff, &dPVEVPAML2dFAML2);
      if (STensorOperation::isnan(PVEVPAML2))
      {
        Msg::Info("mlawPhenomenologicalSMP::constitutive2 did not converge");
        return false;
      } 
      PsiAML2= _EPFunc_AM->get(FAML2n);
      assemblePParti(PVEVPAML2, FfAM, FthAM, Fn, PAML2);
      assembleTangentParti(PAML2, dPVEVPAML2dFAML2, dPVEVPAML2dT, FfAM, FthAM, dFthAMdF, dFthAMdT, Fn, TangentAML2, dPAML2dT);
    }
    
    return true;
}




bool mlawPhenomenologicalSMP::constitutive3
    (const IPPhenomenologicalSMP *q0, IPPhenomenologicalSMP *q1,
     const double T0, const double T, const bool stiff, const STensor3& F0, const STensor3& Fn, 
     const STensor3& FthG0, const STensor3& FthG, const STensor3& FmG0, const STensor3& FmG, const STensor3 &dFthGdT, const STensor43 &dFthGdF,  
     const STensor3& FthR0, const STensor3& FthR, const STensor3& FmR0, const STensor3& FmR, const STensor3 &dFthRdT, const STensor43 &dFthRdF, 
     const STensor3& FthAM0, const STensor3& FthAM, const STensor3& FmAM0, const STensor3& FmAM, const STensor3 &dFthAMdT,  const STensor43 &dFthAMdF,
     double& PsiG, double &dPlasG, double& dPlasGdT, STensor3& dPlasGdF, double &dViscG, double& dViscGdT, STensor3& dViscGdF, 
     STensor3& PG, STensor3& dPGdT,   STensor43& TangentG,
     double& PsiR, double &dPlasR, double& dPlasRdT, STensor3& dPlasRdF, double &dViscR, double& dViscRdT, STensor3& dViscRdF, 
     STensor3& PR,    STensor3& dPRdT, STensor43& TangentR,
     double& PsiAM, double &dPlasAM, double& dPlasAMdT, STensor3& dPlasAMdF, double &dViscAM, double& dViscAMdT, STensor3& dViscAMdF, 
     STensor3 &PAM, STensor3 &dPAMdT,  STensor43 &TangentAM,
     double &PsiRL2, STensor3 &PRL2, STensor3 &dPRL2dT, STensor43 &TangentRL2,
     double &PsiAML2, STensor3 &PAML2, STensor3 &dPAML2dT, STensor43 &TangentAML2) const
{
// Initialisation of internal variables and material properties
     double anisotropicThermalCoefAmplitudeBulkG=(_alphaParam)[0];
     double anisotropicThermalCoefAmplitudeShearG=(_alphaParam)[4];
     double anisotropicThermalAlphaCoefTanhTempBulkG=(_alphaParam)[8];
     double anisotropicThermalAlphaCoefTanhTempShearG=(_alphaParam)[12];

     double anisotropicThermalCoefAmplitudeBulkR=(_alphaParam)[1];
     double anisotropicThermalCoefAmplitudeShearR=(_alphaParam)[5];
     double anisotropicThermalAlphaCoefTanhTempBulkR=(_alphaParam)[9];
     double anisotropicThermalAlphaCoefTanhTempShearR=(_alphaParam)[13];

     double anisotropicThermalCoefAmplitudeBulkAM=(_alphaParam)[3];
     double anisotropicThermalCoefAmplitudeShearAM=(_alphaParam)[7];
     double anisotropicThermalAlphaCoefTanhTempBulkAM=(_alphaParam)[11];
     double anisotropicThermalAlphaCoefTanhTempShearAM=(_alphaParam)[15];

    double &Tdot    =q1->getRefToTdot();
    const STensor3& FthAD0  =q0->getConstRefToFthAD();
    STensor3& FthAD         =q1->getRefToFthAD();
    FthAD =FthAD0;

    //Glassy
    const STensor3& FpG0 =q0->getConstRefToFpG();
    STensor3 &FpG        =q1->getRefToFpG();

    const STensor3 &FfG0    =q0->getConstRefToFfG();
    STensor3 &FfG           =q1->getRefToFfG();
    FfG=FfG0;

    q1->getRefToTRefG()=q0->getConstRefToTRefG();
    q1->getRefToTRefR()=T;

    STensor3& FveG = q1->getRefToFveG();
    
    static STensor3 FG0, FGn, PVEVPG, dPVEVPGdT;
    static STensor43 dPVEVPGdFG;
    STensorOperation::zero(PVEVPG);
    STensorOperation::zero(dPVEVPGdT);
    STensorOperation::zero(dPVEVPGdFG);
    STensorOperation::zero(FG0);
    STensorOperation::zero(FGn);
    
    evaluateFParti(Fn, FfG, FthG, FGn);
    evaluateFParti(F0, FfG0, FthG0, FG0);
    
    double wp=0., dwpdz=0.;
    getZgFunction_Sy0(*_yieldCrystallinityG,q0->getzg(), wp, dwpdz);
    q1->getRefToIPHyperViscoElastoPlasticG()._ipCompression->setScaleFactor(wp,0.);
    q1->getRefToIPHyperViscoElastoPlasticG()._ipTraction->setScaleFactor(wp,0.);

    funEnThAI(T0, q1->getConstRefToTRefG(), q0->getConstRefToIPHyperViscoElastoPlasticG(),q1->getRefToIPHyperViscoElastoPlasticG(),
       anisotropicThermalCoefAmplitudeBulkG, anisotropicThermalCoefAmplitudeShearG, 
       anisotropicThermalAlphaCoefTanhTempBulkG, anisotropicThermalAlphaCoefTanhTempShearG);

    lawG->constitutive(FG0,FGn, PVEVPG, &(q0->getConstRefToIPHyperViscoElastoPlasticG()), &(q1->getRefToIPHyperViscoElastoPlasticG()),dPVEVPGdFG,stiff);
      if (STensorOperation::isnan(PVEVPG))
      {
        Msg::Info("mlawPhenomenologicalSMP::constitutive3 did not converge");
        return false;
      } 
    
    assemblePParti(PVEVPG, FfG, FthG, Fn, PG);
    assembleTangentParti(PG, dPVEVPGdFG, dPVEVPGdT, FfG, FthG, dFthGdF, dFthGdT, Fn, TangentG, dPGdT);
   
    PsiG=q1->getConstRefToElasticEnergyPartG();

    dPlasG=q1->getConstRefToPlasticEnergyPartG()-q0->getConstRefToPlasticEnergyPartG();
    static STensor3 dPlasVEVPGdFVEVP;
    dPlasVEVPGdFVEVP=q1->getConstRefToDPlasticEnergyPartGdF();
    double dPlasVEVPGdT=0.;
    assembleDDissParti(dPlasVEVPGdFVEVP, dPlasVEVPGdT, FfG, FthG, dFthGdF, dFthGdT, Fn, dPlasGdF, dPlasGdT);

    dViscG=q1->getConstRefToViscousEnergyPartG()-q0->getConstRefToViscousEnergyPartG();
    static STensor3 dViscVEVPGdFVEVP;
    dViscVEVPGdFVEVP=q1->getConstRefToDViscousEnergyPartGdF();
    double dViscVEVPGdT=0.;
    assembleDDissParti(dViscVEVPGdFVEVP, dViscVEVPGdT, FfG, FthG, dFthGdF, dFthGdT, Fn, dViscGdF, dViscGdT);

    //Rubbery
    const STensor3 &FfR0    =q0->getConstRefToFfR();
    const STensor3 &FpR0    =q0->getConstRefToFpR();
    STensor3 &FfR           =q1->getRefToFfR();
    STensor3 &FpR           =q1->getRefToFpR();
    q1->resetPlasticityR();
    q1->resetAR();
    q1->resetBR();


    static STensor3 subs3;
    STensorOperation::zero(subs3);
    STensorOperation::multSTensor3(FpG,FfG,subs3);
    subs3-=_I2;
    FfR  = subs3;
    FfR *= _cfR;
    FfR +=_I2;
    if(!checkDeltaEp(FfR0,FfR)) {FfR=FfR0;}

    STensor3 &FveR  =q1->getRefToFveR();
    static STensor3 FthRinv, FfRinv, FpRinv;
    STensorOperation::zero(FthRinv);
    STensorOperation::zero(FfRinv);
    STensorOperation::zero(FpRinv);
    STensorOperation::inverseSTensor3(FthR, FthRinv);
    STensorOperation::inverseSTensor3(FfR,  FfRinv);
    STensorOperation::inverseSTensor3(FpR,  FpRinv);

    static STensor3 Fvei_int, Fvei_int2;
    STensorOperation::zero(Fvei_int);
    STensorOperation::zero(Fvei_int2);
    STensorOperation::multSTensor3(Fn, FthRinv, Fvei_int);
    STensorOperation::multSTensor3(Fvei_int,  FfRinv,  Fvei_int2);
    STensorOperation::multSTensor3(Fvei_int2, FpRinv,  FveR);


     //Amorphous
    const STensor3 &FfAM0    =q0->getConstRefToFfAM();
    STensor3 &FfAM           =q1->getRefToFfAM();
    FfAM  =FfAM0;
    
    static STensor3 FAM0, FAMn, PVEVPAM, dPVEVPAMdT;
    static STensor43 dPVEVPAMdFAM;
    STensorOperation::zero(PVEVPAM);
    STensorOperation::zero(dPVEVPAMdT);
    STensorOperation::zero(dPVEVPAMdFAM);
    STensorOperation::zero(FAM0);
    STensorOperation::zero(FAMn);
    
    evaluateFParti(Fn, FfAM, FthAM, FAMn);
    evaluateFParti(F0, FfAM0, FthAM0, FAM0);

    getZgFunction_Sy0(*_yieldCrystallinityAM,q0->getzg(), wp, dwpdz);
    q1->getRefToIPHyperViscoElastoPlasticAM()._ipCompression->setScaleFactor(wp,0.);
    q1->getRefToIPHyperViscoElastoPlasticAM()._ipTraction->setScaleFactor(wp,0.);

    funEnThAI(T0, q1->getConstRefToTRefAM(), q0->getConstRefToIPHyperViscoElastoPlasticAM(),q1->getRefToIPHyperViscoElastoPlasticAM(),
       anisotropicThermalCoefAmplitudeBulkAM, anisotropicThermalCoefAmplitudeShearAM, 
       anisotropicThermalAlphaCoefTanhTempBulkAM, anisotropicThermalAlphaCoefTanhTempShearAM);

    lawAM->constitutive(FAM0,FAMn, PVEVPAM, &(q0->getConstRefToIPHyperViscoElastoPlasticAM()), &(q1->getRefToIPHyperViscoElastoPlasticAM()),dPVEVPAMdFAM,stiff);
      if (STensorOperation::isnan(PVEVPAM))
      {
        Msg::Info("mlawPhenomenologicalSMP::constitutive3 did not converge");
        return false;
      } 


    assemblePParti(PVEVPAM, FfAM, FthAM, Fn, PAM);
    assembleTangentParti(PAM, dPVEVPAMdFAM, dPVEVPAMdT, FfAM, FthAM, dFthAMdF, dFthAMdT, Fn, TangentAM, dPAMdT);

    PsiAM=q1->getConstRefToElasticEnergyPartAM();

    dPlasAM=q1->getConstRefToPlasticEnergyPartAM()-q0->getConstRefToPlasticEnergyPartAM();
    static STensor3 dPlasVEVPAMdFVEVP;
    dPlasVEVPAMdFVEVP=q1->getConstRefToDPlasticEnergyPartAMdF();
    double dPlasVEVPAMdT=0.;
    assembleDDissParti(dPlasVEVPAMdFVEVP, dPlasVEVPAMdT, FfAM, FthAM, dFthAMdF, dFthAMdT, Fn, dPlasAMdF, dPlasAMdT);

    dViscAM=q1->getConstRefToViscousEnergyPartAM()-q0->getConstRefToViscousEnergyPartAM();
    static STensor3 dViscVEVPAMdFVEVP;
    dViscVEVPAMdFVEVP=q1->getConstRefToDViscousEnergyPartAMdF();
    double dViscVEVPAMdT=0.;
    assembleDDissParti(dViscVEVPAMdFVEVP, dViscVEVPAMdT, FfAM, FthAM, dFthAMdF, dFthAMdT, Fn, dViscAMdF, dViscAMdT);

    //p-R: polynomial 2nd degree
    static STensor3 FRL20, FRL2n, PVEVPRL2, dPVEVPRL2dT;
    static STensor43 dPVEVPRL2dFRL2;
    STensorOperation::zero(PVEVPRL2);
    STensorOperation::zero(dPVEVPRL2dT);
    STensorOperation::zero(dPVEVPRL2dFRL2);
    STensorOperation::zero(FRL20);
    STensorOperation::zero(FRL2n);
    
    evaluateFParti(Fn, FfR, FthR, FRL2n);
    evaluateFParti(F0, FfR0, FthR0, FRL20);

    if(_EPFunc_R!=NULL)
    {
      _EPFunc_R->constitutive(FRL2n, PVEVPRL2, stiff, &dPVEVPRL2dFRL2);
      if (STensorOperation::isnan(PVEVPRL2))
      {
        Msg::Info("mlawPhenomenologicalSMP::constitutive3 did not converge");
        return false;
      } 

      PsiRL2= _EPFunc_R->get(FRL2n);
      assemblePParti(PVEVPRL2, FfR, FthR, Fn, PRL2);
      assembleTangentParti(PRL2, dPVEVPRL2dFRL2, dPVEVPRL2dT, FfR, FthR, dFthRdF, dFthRdT, Fn, TangentRL2, dPRL2dT);
    }

    //p-AM: polynomial 2nd degree
    static STensor3 FAML20, FAML2n, PVEVPAML2, dPVEVPAML2dT;
    static STensor43 dPVEVPAML2dFAML2;
    STensorOperation::zero(PVEVPAML2);
    STensorOperation::zero(dPVEVPAML2dT);
    STensorOperation::zero(dPVEVPAML2dFAML2);
    STensorOperation::zero(FAML20);
    STensorOperation::zero(FAML2n);
    
    evaluateFParti(Fn, FfAM, FthAM, FAML2n);
    evaluateFParti(F0, FfAM0, FthAM0, FAML20);

    if(_EPFunc_AM!=NULL)
    {
      _EPFunc_AM->constitutive(FAML2n, PVEVPAML2, stiff, &dPVEVPAML2dFAML2);
      if (STensorOperation::isnan(PVEVPAML2))
      {
        Msg::Info("mlawPhenomenologicalSMP::constitutive3 did not converge");
        return false;
      } 
      PsiAML2= _EPFunc_AM->get(FAML2n);
      assemblePParti(PVEVPAML2, FfAM, FthAM, Fn, PAML2);
      assembleTangentParti(PAML2, dPVEVPAML2dFAML2, dPVEVPAML2dT, FfAM, FthAM, dFthAMdF, dFthAMdT, Fn, TangentAML2, dPAML2dT);
    }
    
//    TangentG.print("con1 TanG");
//    TangentAM.print("con1 TanAM");
    return true;
}






void mlawPhenomenologicalSMP::predictorCorrectori
(
     const STensor3& Ffi0, STensor3& Ffi, const STensor3& Fpi0, STensor3& Fpi, const double &gammai0, double &gammai1, const STensor3& A1i0, const STensor3& A2i0,
     const double& B1i0, const double& B2i0, const STensor3& q1i0, const STensor3& q2i0, STensor3& Fvei,
     const char &phase, const std::vector<double> &_ViscParami,
     const STensor3& F0, const STensor3& Fn, const IPPhenomenologicalSMP *q0, IPPhenomenologicalSMP *q1,
     const double dh, const double T0, double T, const double Tdot, const bool stiff,
     const STensor3 &Fthi0, const STensor3 &Fthi, const STensor3 &dFthidT, const STensor43 &dFthidF,
     STensor3& A1i, STensor3& A2i, double& B1i, double& B2i, STensor3& q1i, STensor3& q2i,
     double& dPlasi, double& dPlasidT, STensor3& dPlasidF,
     double& dFrzdi, double& dFrzdidT, STensor3& dFrzdidF,
     double& dVisci, double& dViscidT, STensor3& dViscidF,
     double& Psii, STensor3& Pi, STensor3& dPidT, STensor43& Tangenti) const
{
/*    static STensor3 Fthi0inv, Ffi0inv, Fpi0inv;
    STensorOperation::zero(Fthi0inv);
    STensorOperation::zero(Ffi0inv);
    STensorOperation::zero(Fpi0inv);
    STensorOperation::inverseSTensor3(Fthi0, Fthi0inv);
    STensorOperation::inverseSTensor3(Ffi0, Ffi0inv);
    STensorOperation::inverseSTensor3(Fpi0, Fpi0inv);

    static STensor3 Fvei0aux1, Fvei0aux2, Fvei0;
    STensorOperation::zero(Fvei0aux1);
    STensorOperation::multSTensor3(F0, Fthi0inv, Fvei0aux1);
    STensorOperation::multSTensor3(Fvei0aux1, Ffi0inv, Fvei0aux2);
    STensorOperation::multSTensor3(Fvei0aux2, Fpi0inv, Fvei0);

    static STensor3 Cvei0, Evei0;
    STensorOperation::multSTensor3FirstTranspose(Fvei0, Fvei0, Cvei0);
    STensorOperation::logSTensor3(Cvei0, _order, Evei0);
    Evei0*=0.5;

    double GinfiT, KinfiT, dGinfiT, dKinfiT;
    double anisotropicThermalCoefAmplitude=0.;
    double anisotropicThermalCoefUnused=0.;
    double anisotropicThermalAlphaCoefTanhEn=0.;
    double anisotropicThermalAlphaCoefTanhTemp=0.;
    
    STensor3 *strainEffect;
    static STensor3 dStrainEffectDt;
    static STensor43  dStrainEffectdEne;
    double TRef=0.;
    double wp, dwpdz;
    wp=1.; dwpdz=0.;
    double dzgdT=0.;//to be changed
    switch(phase) {
      case 'G':
        GinfiT =get_GG(T, &dGinfiT);
        //getZgFunction_Sy0G(q0->getzg(), wp, dwpdz);
        //GinfiT*=wp;
        //dGinfiT*=wp;
        //dGinfiT+=GinfiT*dwpdz*dzgdT;
        KinfiT =get_KG(T, GinfiT, &dKinfiT, &dGinfiT);
        anisotropicThermalCoefAmplitude=(_alphaParam)[0];
        anisotropicThermalCoefUnused=(_alphaParam)[4];
        anisotropicThermalAlphaCoefTanhEn=(_alphaParam)[8];
        anisotropicThermalAlphaCoefTanhTemp=(_alphaParam)[12];
        TRef=q1->getConstRefToTRefG();
        strainEffect=&q1->getRefToEnThG();
        //Msg::Info("G phase");
        break;
      case 'R':
        GinfiT =get_GR(T, &dGinfiT);
        KinfiT =get_KR(T, GinfiT, &dKinfiT, &dGinfiT);
        anisotropicThermalCoefAmplitude=(_alphaParam)[1];
        anisotropicThermalCoefUnused=(_alphaParam)[5];
        anisotropicThermalAlphaCoefTanhEn=(_alphaParam)[9];
        anisotropicThermalAlphaCoefTanhTemp=(_alphaParam)[13];
        TRef=q1->getConstRefToTRefR();
        strainEffect=&q1->getRefToEnThR();
        //Msg::Info("R phase");
        break;
      case 'A':
        GinfiT =get_GAM(T, &dGinfiT);
        KinfiT =get_KAM(T, GinfiT, &dKinfiT, &dGinfiT);
        anisotropicThermalCoefAmplitude=(_alphaParam)[3];
        anisotropicThermalCoefUnused=(_alphaParam)[7];
        anisotropicThermalAlphaCoefTanhEn=(_alphaParam)[11];
        anisotropicThermalAlphaCoefTanhTemp=(_alphaParam)[15];
        TRef=q1->getConstRefToTRefAM();
        strainEffect=&q1->getRefToEnThAM();
        //Msg::Info("AM phase");
        break;
      default:
        std::cout << "wrong phase name" << std::endl;
    }

    double MU1i      =(_ViscParami)[0];
    double KAPPA1i   =(_ViscParami)[1];
    double TAU1i     =(_ViscParami)[2];
    double MU2i      =(_ViscParami)[3];
    double KAPPA2i   =(_ViscParami)[4];
    double TAU2i     =(_ViscParami)[5];

    double Gei= GinfiT + MU1i*exp(-dh/2./TAU1i) + MU2i*exp(-dh/2./TAU2i);
    double Kei= KinfiT + KAPPA1i*exp(-dh/2./TAU1i) + KAPPA2i*exp(-dh/2./TAU2i);

    // Initialisation of variables
    gammai1 =gammai0;

    // Compute predictor
    static STensor3 Fveipr, Eveipr, tauveipr;
    static STensor43 dlnCveipr;
    static STensor63 ddlnCveipr;
    STensorOperation::zero(Fveipr);
    STensorOperation::zero(Eveipr);
    STensorOperation::zero(tauveipr);
    STensorOperation::zero(dlnCveipr);
    STensorOperation::zero(ddlnCveipr);

    static STensor3 devq1i, devq2i;
    double pq1i, pq2i;
    STensorOperation::zero(devq1i);
    STensorOperation::zero(devq2i);





    Fpi=Fpi0;
    computePi(Fn, T, dh, KinfiT, GinfiT, Kei, Gei, _ViscParami,
              Evei0, Fthi, Ffi, Fpi0, Fveipr, Eveipr, tauveipr, dlnCveipr, ddlnCveipr, Pi,
              A1i0, A2i0, B1i0, B2i0, A1i, A2i, B1i, B2i, devq1i, pq1i, devq2i, pq2i,
              anisotropicThermalCoefAmplitude, anisotropicThermalCoefUnused, anisotropicThermalAlphaCoefTanhEn, anisotropicThermalAlphaCoefTanhTemp,TRef,
              *strainEffect,dStrainEffectDt,dStrainEffectdEne);


    double PsiiElasDEV, PsiiREST;
    double tauveipreq =sqrt(1.5*tauveipr.dev().dotprod());

    // Assume final values = predictor
    static STensor3 Evei, tauvei;
    static STensor43 dlnCvei;
    static STensor63 ddlnCvei;
    Fvei     =Fveipr;
    Evei     =Eveipr;
    tauvei   =tauveipr;
    dlnCvei  =dlnCveipr;
    ddlnCvei =ddlnCveipr;

    static STensor3 Ni;
    STensorOperation::zero(Ni);
    Ni  =3./2./tauveipreq*tauveipr.dev();
    if(tauveipreq==0.){STensorOperation::zero(Ni);}
    static STensor43 dexpDgammaNi;
    STensorOperation::zero(dexpDgammaNi);

    // Test plasticity //
    double tau_yi0T, tau_yi, Hi_gamma;
    switch(phase) {
      case 'G':
        _j2IHG->hardening(gammai0, q0->getConstRefToIPJ2IsotropicHardeningG(), gammai1, q1->getRefToIPJ2IsotropicHardeningG());
        tau_yi0T = _j2IHG->getYield0()*_temFunc_Sy0G->getVal(T)*wp;
        //q1->getRefToIPJ2IsotropicHardening().setScaleFactor(wp, 0.);
        tau_yi   = tau_yi0T + (q1->getConstRefToIPJ2IsotropicHardeningG().getR() - _j2IHG->getYield0())*_temFunc_HG->getVal(T)*wp;
        Hi_gamma = q1->getConstRefToIPJ2IsotropicHardeningG().getDR()*_temFunc_HG->getVal(T);
        //Msg::Info("zg %f, yield: %f",q1->getzg(), tau_yi0T);
        break;
      case 'R':
        _j2IHR->hardening(gammai0, q0->getConstRefToIPJ2IsotropicHardeningR(), gammai1, q1->getRefToIPJ2IsotropicHardeningR());
        tau_yi0T = _j2IHR->getYield0()*_temFunc_Sy0R->getVal(T);
        tau_yi   = tau_yi0T + (q1->getConstRefToIPJ2IsotropicHardeningR().getR() - _j2IHR->getYield0())*_temFunc_HR->getVal(T);
        Hi_gamma = q1->getConstRefToIPJ2IsotropicHardeningR().getDR()*_temFunc_HR->getVal(T);
        break;
      case 'A':
        _j2IHAM->hardening(gammai0, q0->getConstRefToIPJ2IsotropicHardeningAM(), gammai1, q1->getRefToIPJ2IsotropicHardeningAM());
        tau_yi0T = _j2IHAM->getYield0()*_temFunc_Sy0AM->getVal(T);
        tau_yi   = tau_yi0T + (q1->getConstRefToIPJ2IsotropicHardeningAM().getR() - _j2IHAM->getYield0())*_temFunc_HAM->getVal(T);
        Hi_gamma = q1->getConstRefToIPJ2IsotropicHardeningAM().getDR()*_temFunc_HAM->getVal(T);
        break;
      default:
        std::cout << "wrong phase name" << std::endl;
    }

    double fRes =tauveipreq-tau_yi;
    double Delta_gammai=0.;
    double Hi_T        =0.;
    double tauveieq    =0.;

    //double etaPloverdh=0.;
    //if(dh!=0.){etaPloverdh=1.e12/dh;}
    if(fRes>0.)
    {
        //Newton-Raphson
        double tolNR=1.e-6;

        //Initialization of variables
        int ite = 0, maxite = 1000;
        double eps_f=fabs(fRes/tauveipreq);

        //Iterative process
        while(eps_f>tolNR or ite<1)
        {
            //Compute Jacobian
            double j1    =1./(3.*Gei+Hi_gamma );  //+etaPloverdh);
            double depsi =j1*fRes;
            gammai1 +=depsi;
            Evei    -=Ni*depsi;
            tauveieq = tauveipreq - 3.*Gei*(gammai1-gammai0);

            switch(phase) {
              case 'G':           
                _j2IHG->hardening(gammai0, q0->getConstRefToIPJ2IsotropicHardeningG(), gammai1, q1->getRefToIPJ2IsotropicHardeningG());
                tau_yi     = tau_yi0T + (q1->getConstRefToIPJ2IsotropicHardeningG().getR() - _j2IHG->getYield0())*_temFunc_HG->getVal(T)*wp;
                Hi_gamma   = q1->getConstRefToIPJ2IsotropicHardeningG().getDR()*_temFunc_HG->getVal(T)*wp;
                break;
              case 'R':
                _j2IHR->hardening(gammai0, q0->getConstRefToIPJ2IsotropicHardeningR(), gammai1, q1->getRefToIPJ2IsotropicHardeningR());
                tau_yi     = tau_yi0T + (q1->getConstRefToIPJ2IsotropicHardeningR().getR() - _j2IHR->getYield0())*_temFunc_HR->getVal(T);
                Hi_gamma   = q1->getConstRefToIPJ2IsotropicHardeningR().getDR()*_temFunc_HR->getVal(T);
                break;
              case 'A':
                _j2IHAM->hardening(gammai0, q0->getConstRefToIPJ2IsotropicHardeningAM(), gammai1, q1->getRefToIPJ2IsotropicHardeningAM());
                tau_yi     = tau_yi0T + (q1->getConstRefToIPJ2IsotropicHardeningAM().getR() - _j2IHAM->getYield0())*_temFunc_HAM->getVal(T);
                Hi_gamma   = q1->getConstRefToIPJ2IsotropicHardeningAM().getDR()*_temFunc_HAM->getVal(T);
                break;
              default:
              std::cout << "wrong phase name" << std::endl;
            }
            fRes    =tauveieq-tau_yi;

           eps_f   =fabs(fRes/tauveipreq);
           if(ite > maxite)
            {
                Msg::Error("on %s SMP itereration %d, residual %f,time step %f ", phase, ite, eps_f,dh);
                break;
            }
            ite++;
        }
        Delta_gammai =gammai1-gammai0;
        static STensor3 DepsNi;
        DepsNi = Delta_gammai*Ni;

        static STensor3 expDgammaNi;
        STensorOperation::zero(expDgammaNi);
        STensorOperation::expSTensor3(DepsNi, _order, expDgammaNi, &dexpDgammaNi);

        //Update of variables
        STensorOperation::multSTensor3(expDgammaNi, Fpi0, Fpi);

        computePi(Fn, T, dh, KinfiT, GinfiT, Kei, Gei, _ViscParami,
                   Evei0, Fthi, Ffi, Fpi, Fvei, Evei, tauvei, dlnCvei, ddlnCvei, Pi,
                   A1i0, A2i0, B1i0, B2i0, A1i, A2i, B1i, B2i, devq1i, pq1i, devq2i, pq2i, 
                   anisotropicThermalCoefAmplitude, anisotropicThermalCoefUnused, anisotropicThermalAlphaCoefTanhEn, anisotropicThermalAlphaCoefTanhTemp, TRef,
                   *strainEffect,dStrainEffectDt,dStrainEffectdEne);
    }

    //dissipation
    computeFrzdSourcei(Ffi0, Ffi, Fpi, Fvei, tauvei, dlnCvei, dFrzdi);
    dPlasi = tauveieq*Delta_gammai*_TaylorQuiney;

    q1i =devq1i + pq1i*_I2;
    q2i =devq2i + pq2i*_I2;
    computeViscSourcei(_ViscParami, Evei, devq1i, pq1i, devq2i, pq2i, q1i0, q2i0, dVisci);

//    double PsiiVOL, PsiiDEV;
    computeESourcei(KinfiT, GinfiT, _ViscParami, Evei, devq1i, pq1i, devq2i, pq2i, PsiiElasDEV, PsiiREST);
    Psii =PsiiElasDEV + PsiiREST;

    //Tangent operators
    switch(phase) {
      case 'G':
        Hi_T = _j2IHG->getYield0()*_temFunc_Sy0G->getDiff(T)*wp+_j2IHG->getYield0()*_temFunc_Sy0G->getVal(T)*dwpdz*dzgdT
                + (q1->getConstRefToIPJ2IsotropicHardeningG().getR() - _j2IHG->getYield0())*_temFunc_HG->getDiff(T)*wp + 
                (q1->getConstRefToIPJ2IsotropicHardeningG().getR() - _j2IHG->getYield0())*_temFunc_HG->getVal(T)*dwpdz*dzgdT;
        break;
      case 'R':
        Hi_T = _j2IHR->getYield0()*_temFunc_Sy0R->getDiff(T)
                + (q1->getConstRefToIPJ2IsotropicHardeningR().getR() - _j2IHR->getYield0())*_temFunc_HR->getDiff(T);
        break;
      case 'A':
        Hi_T = _j2IHAM->getYield0()*_temFunc_Sy0AM->getDiff(T)
                + (q1->getConstRefToIPJ2IsotropicHardeningAM().getR() - _j2IHAM->getYield0())*_temFunc_HAM->getDiff(T);
        break;
      default:
      std::cout << "wrong phase name" << std::endl;
    }

    if(stiff)
    {
        tangentfunci(T0, T, Tdot, dh, F0, Fthi, dFthidT, dFthidF, Fn, Fpi0, Fpi, Ffi, Evei0, Fveipr, Eveipr, dlnCveipr,
                 tauveipr, tauveipreq, Ni, Delta_gammai, dexpDgammaNi, Hi_gamma, Fvei, Evei, dlnCvei, ddlnCvei, tauvei, tauveieq,
                 GinfiT, KinfiT, dKinfiT, dGinfiT, _ViscParami,
                 A1i0, A2i0, B1i0, B2i0, devq1i, pq1i, devq2i, pq2i, q1i0, q2i0, Hi_T,
                 dPlasidT, dPlasidF, dFrzdidT, dFrzdidF, dViscidT, dViscidF, dPidT, Tangenti,
                 anisotropicThermalCoefAmplitude, anisotropicThermalCoefUnused, anisotropicThermalAlphaCoefTanhEn, anisotropicThermalAlphaCoefTanhTemp,TRef,
                 *strainEffect,dStrainEffectDt,dStrainEffectdEne);
    }*/
}


void mlawPhenomenologicalSMP::tangentfunci
    (const double T0, const double T, const double Tdot, const double dh, const STensor3 &F0,
     const STensor3& Fthi, const STensor3 &dFthidT, const STensor43 &dFthidF,
     const STensor3& Fn, const STensor3& Fpi0, const STensor3 &Fpi, const STensor3 &Ffi,
     const STensor3& Evei0, const STensor3& Fveipr, const STensor3& Eveipr, const STensor43& dlnCveipr, const STensor3& tauveipr, const double tauveipreq,
     const STensor3& Nipr, const double Deltagammai, const STensor43& dexpDelta_gammaNipr, const double Hi_gamma,
     const STensor3& Fvei, const STensor3& Evei, const STensor43& dlnCvei, const STensor63& ddlnCvei, const STensor3& tauvei, const double tauveieq,
     const double GinfiT, const double KinfiT, const double dKinfiT, const double dGinfiT,
     const std::vector<double> _ViscParami,
     const STensor3 &A1i0, const STensor3 &A2i0, const double B1i0, const double B2i0,
     const STensor3 &devq1i, const double pq1i, const STensor3 &devq2i, const double pq2i, const STensor3& q1i0, const STensor3& q2i0, const double Hi_T,
     double& dPlasidT, STensor3& dPlasidF, double& dFrzdidT, STensor3& dFrzdidF, double& dViscidT, STensor3& dViscidF, STensor3& dPidT, STensor43& Tangenti, 
     double anisotropicThermalCoefAmplitude, double anisotropicThermalCoefUnused, 
     double anisotropicThermalAlphaCoefTanhEn, double anisotropicThermalAlphaCoefTanhTemp, double TRef,
     const STensor3 &strainEffect, const STensor3 &dStrainEffectDt, const STensor43  &dStrainEffectdEne) const
{
/*    dPlasidT=0.;
    dFrzdidT=0.;
    dViscidT=0.;
    STensorOperation::zero(dPlasidF);
    STensorOperation::zero(dFrzdidF);
    STensorOperation::zero(dViscidF);
    STensorOperation::zero(dPidT);
    STensorOperation::zero(Tangenti);

    double MU1i      =(_ViscParami)[0];
    double KAPPA1i   =(_ViscParami)[1];
    double TAU1i     =(_ViscParami)[2];
    double MU2i      =(_ViscParami)[3];
    double KAPPA2i   =(_ViscParami)[4];
    double TAU2i     =(_ViscParami)[5];

    double Gei    = GinfiT + MU1i*exp(-dh/2./TAU1i) + MU2i*exp(-dh/2./TAU2i);
    double Kei    = KinfiT + KAPPA1i*exp(-dh/2./TAU1i) + KAPPA2i*exp(-dh/2./TAU2i);
    double dGeidT = 0.;
    double dKeidT = 0.;


//    std::ofstream KEIout;
//    KEIout.open("KEIout.dat", std::ios::app);
//    KEIout <<getTime()<<'\t'<<T<<'\t'<<Gei<<'\t'<<GinfiT<<'\t'<<MU1i<<'\t'
//                                      <<MU2i<<'\t'<<'\t'<<Kei<<'\t'<<KinfiT<<'\t'
//                                      <<KAPPA1i<<'\t'<<KAPPA2i<<std::endl;
//    KEIout.close();

    STensor3 Fpi0inv, Fthiinv, Ffiinv, Fpiinv;
    STensorOperation::zero(Fpi0inv);
    STensorOperation::zero(Fthiinv);
    STensorOperation::zero(Ffiinv);
    STensorOperation::zero(Fpiinv);
    STensorOperation::inverseSTensor3(Fpi0, Fpi0inv);
    STensorOperation::inverseSTensor3(Fthi, Fthiinv);
    STensorOperation::inverseSTensor3(Ffi, Ffiinv);
    STensorOperation::inverseSTensor3(Fpi, Fpiinv);

    STensor43 dFthiinvdF;
    STensor3 dFthiinvdT;
    STensorOperation::zero(dFthiinvdF);
    STensorOperation::zero(dFthiinvdT);
    for (int i=0; i<3; i++){
        for (int j=0; j<3; j++){
            for (int p=0; p<3; p++){
                for (int q=0; q<3; q++){
                    dFthiinvdT(i,j) -= Fthiinv(i,p)*dFthidT(p,q)*Fthiinv(q,j);
                    for (int k=0; k<3; k++){
                        for (int l=0; l<3; l++){
                            dFthiinvdF(i,j,k,l) -= Fthiinv(i,p)*dFthidF(p,q,k,l)*Fthiinv(q,j);
                        }
                    }
                }
            }
        }
    }

    STensor43 dFveiprdF;
    STensor3 dFveiprdT;
    STensorOperation::zero(dFveiprdF);
    STensorOperation::zero(dFveiprdT);
    for (int i=0; i<3; i++){
        for (int N=0; N<3; N++){
            for (int G=0; G<3; G++){
                for (int X=0; X<3; X++){
                    for (int Y=0; Y<3; Y++){
                        dFveiprdT(i,N) += Fn(i,G)*dFthiinvdT(G,X)*Ffiinv(X,Y)*Fpi0inv(Y,N);
                        for (int j=0; j<3; j++){
                            for (int C=0; C<3; C++){
                                dFveiprdF(i,N,j,C) += _I2(i,j)*_I2(G,C)*Fthiinv(G,X)*Ffiinv(X,Y)*Fpi0inv(Y,N)
                                                      + Fn(i,G)*dFthiinvdF(G,X,j,C)*Ffiinv(X,Y)*Fpi0inv(Y,N);
                            }
                        }
                    }
                }
            }
        }
    }

    STensor43 dEveiprdF;
    STensor3 dEveiprdT;
    STensorOperation::zero(dEveiprdF);
    STensorOperation::zero(dEveiprdT);
    for (int i=0; i<3; i++){
        for (int j=0; j<3; j++){
            for (int k=0; k<3; k++){
                for (int l=0; l<3; l++){
                    for (int p=0; p<3; p++){
                        dEveiprdT(i,j) += dlnCveipr(i,j,k,l)*Fveipr(p,k)*dFveiprdT(p,l);
                        for (int q=0; q<3; q++){
                            for (int r=0; r<3; r++){
                                dEveiprdF(i,j,q,r) += dlnCveipr(i,j,k,l)*Fveipr(p,k)*dFveiprdF(p,l,q,r);
                            }
                        }
                    }
                }
            }
        }
    }

    STensor43 ddevEveiprdF;
    STensor3 dtrEveiprdF;
    STensorOperation::zero(ddevEveiprdF);
    STensorOperation::zero(dtrEveiprdF);
    for (int i=0; i<3; i++){
        for (int j=0; j<3; j++){
            for (int k=0; k<3; k++){
                for (int l=0; l<3; l++){
                    dtrEveiprdF(k,l) += _I2(i,j)*dEveiprdF(i,j,k,l);
                }
            }
        }
    }
    for (int i=0; i<3; i++){
        for (int j=0; j<3; j++){
            for (int k=0; k<3; k++){
                for (int l=0; l<3; l++){
                    ddevEveiprdF(i,j,k,l) += dEveiprdF(i,j,k,l) - 1./3*dtrEveiprdF(k,l)*_I2(i,j);
                }
            }
        }
    }

    STensor43 ddevtauveiprdF, dtauveiprdF;
    STensor3 dpveiprdF;
    STensorOperation::zero(ddevtauveiprdF);
    STensorOperation::zero(dpveiprdF);
    STensorOperation::zero(dtauveiprdF);
    for(int i=0; i<3; i++){
        for(int j=0; j<3; j++){
            dpveiprdF(i,j) += Kei*dtrEveiprdF(i,j);
            for (int k=0; k<3; k++){
                for (int l=0; l<3; l++){
                    ddevtauveiprdF(i,j,k,l) += 2.*Gei*ddevEveiprdF(i,j,k,l);
                }
            }
        }
    }

    for(int i=0; i<3; i++){
        for(int j=0; j<3; j++){
            for (int k=0; k<3; k++){
                for (int l=0; l<3; l++){
                    dtauveiprdF(i,j,k,l) +=ddevtauveiprdF(i,j,k,l) + dpveiprdF(k,l)*_I2(i,j);
                }
            }
        }
    }


    STensor3 ddevEveiprdT;
    STensorOperation::zero(ddevEveiprdT);
    double dtrEveiprdT=0.;
    for (int i=0; i<3; i++){
        for (int j=0; j<3; j++){
            dtrEveiprdT += _I2(i,j)*dEveiprdT(i,j);
        }
    }
    for (int i=0; i<3; i++){
        for (int j=0; j<3; j++){
            ddevEveiprdT(i,j) += dEveiprdT(i,j) - 1./3*dtrEveiprdT*_I2(i,j);
        }
    }

    STensor3 ddevtauveiprdT, dtauveiprdT;
    STensorOperation::zero(ddevtauveiprdT);
    STensorOperation::zero(dtauveiprdT);
    double dpveiprdT=0.;
    for(int i=0; i<3; i++){
        for(int j=0; j<3; j++){
            ddevtauveiprdT(i,j) += 2.*Gei*ddevEveiprdT(i,j);
        }
    }
    dpveiprdT =Kei*dtrEveiprdT;
    for(int i=0; i<3; i++){
        for(int j=0; j<3; j++){
            dtauveiprdT(i,j) += ddevtauveiprdT(i,j) + dpveiprdT*_I2(i,j);
        }
    }

    STensor3 devtauveipr;
    STensorOperation::zero(devtauveipr);
    double pveipr=0;
    STensorOperation::decomposeDevTr(tauveipr, devtauveipr, pveipr);
    pveipr=pveipr/3.;

    STensor3 devtauvei;
    STensorOperation::zero(devtauvei);
    double pvei=0.;
    STensorOperation::decomposeDevTr(tauvei, devtauvei, pvei);
    pvei=pvei/3.;

    STensor43 dtauveidF, dFpidF, dFfidF;
    STensor3 dtauveidT, dFpidT, dFfidT;
    STensorOperation::zero(dtauveidF);
    STensorOperation::zero(dtauveidT);
    STensorOperation::zero(dFpidF);
    STensorOperation::zero(dFpidT);
    STensorOperation::zero(dFfidF);
    STensorOperation::zero(dFfidT);

    dtauveidF=dtauveiprdF;
    dtauveidT=dtauveiprdT;
    if (Deltagammai > 0.)
    {
         //wrt T
        double dtauveipreqdT=0.;
        for (int C=0; C<3; C++){
            for (int D=0; D<3; D++){
                dtauveipreqdT +=(3./2./tauveipreq)*devtauveipr(C,D)*ddevtauveiprdT(C,D);
            }
        }

        STensor3 dNiprdT;
        STensorOperation::zero(dNiprdT);
        for (int C=0; C<3; C++){
            for (int D=0; D<3; D++){
                dNiprdT(C,D) +=(3./2/pow(tauveipreq,2))*(ddevtauveiprdT(C,D)*tauveipreq - devtauveipr(C,D)*dtauveipreqdT);
            }
        }

        double dDeltagammaidT;
        dDeltagammaidT = 1./(3.*Gei+Hi_gamma)*(dtauveipreqdT -Hi_T);

        STensor3  BT;
        STensorOperation::zero(BT);
        for (int i=0; i<3; i++){
            for (int j=0; j<3; j++){
                BT(i,j) += dDeltagammaidT*Nipr(i,j) + Deltagammai*dNiprdT(i,j);
            }
        }
        //dtauveidT.daxpy(BT,-2.*Gei);
        dtauveidT+= (-2.*Gei*BT);

        for (int i=0; i<3; i++){
            for (int j=0; j<3; j++){
                for (int k=0; k<3; k++){
                    for (int l=0; l<3; l++){
                        for (int m=0; m<3; m++){
                            dFpidT(i,m) += dexpDelta_gammaNipr(i,j,k,l)*BT(k,l)*Fpi0(j,m);
                        }
                    }
                }
            }
        }

        //wrt F
        STensor3 dtauveipreqdF;
        STensorOperation::zero(dtauveipreqdF);
        for (int M=0; M<3; M++){
            for (int X=0; X<3; X++){
                for (int j=0; j<3; j++){
                    for (int C=0; C<3; C++){
                        dtauveipreqdF(j,C) +=(3./2./tauveipreq)*devtauveipr(M,X)*ddevtauveiprdF(M,X,j,C);
                    }
                }
            }
        }

        STensor43 dNiprdF;
        STensorOperation::zero(dNiprdF);
        for (int M=0; M<3; M++){
            for (int X=0; X<3; X++){
                for (int j=0; j<3; j++){
                    for (int C=0; C<3; C++){
                        dNiprdF(M,X,j,C) +=(3./2/pow(tauveipreq,2))*(ddevtauveiprdF(M,X,j,C)*tauveipreq - devtauveipr(M,X)*dtauveipreqdF(j,C));
                    }
                }
            }
        }

        STensor3 dDeltagammaidF;
        STensorOperation::zero(dDeltagammaidF);
        dDeltagammaidF = 1./(3.*Gei+Hi_gamma)*dtauveipreqdF;

        STensor43  BE;
        STensorOperation::zero(BE);
        for (int i=0; i<3; i++){
            for (int j=0; j<3; j++){
                for (int k=0; k<3; k++){
                    for (int l=0; l<3; l++){
                        BE(i,j,k,l) += dDeltagammaidF(k,l)*Nipr(i,j) + Deltagammai*dNiprdF(i,j,k,l);
                    }
                }
            }
        }
        dtauveidF -=2.*Gei*BE;


        for (int i=0; i<3; i++){
            for (int j=0; j<3; j++){
                for (int k=0; k<3; k++){
                    for (int l=0; l<3; l++){
                        for (int m=0; m<3; m++){
                            for (int n=0; n<3; n++){
                                for (int p=0; p<3; p++){
                                    dFpidF(i,p,m,n) +=dexpDelta_gammaNipr(i,j,k,l)*BE(k,l,m,n)*Fpi0(j,p);
                                }
                            }
                        }
                    }
                }
            }
        }

        //dissipation
        double dtauveieqdT=0.;
        for (int C=0; C<3; C++){
            for (int D=0; D<3; D++){
                for (int A=0; A<3; A++){
                    for (int B=0; B<3; B++){
                        dtauveieqdT += (3./2./tauveieq)*devtauvei(C,D)*(dtauveidT(C,D) - 1./3.*_I2(A,B)*dtauveidT(A,B)*_I2(C,D));
                    }
                }
            }
        }
        dPlasidT = tauveieq*dDeltagammaidT + dtauveieqdT*Deltagammai;

        STensor3 dtauveieqdF;
        STensorOperation::zero(dtauveieqdF);
        for (int j=0; j<3; j++){
            for (int C=0; C<3; C++){
                for (int E=0; E<3; E++){
                    for (int F=0; F<3; F++){
                        for (int A=0; A<3; A++){
                            for (int B=0; B<3; B++){
                                dtauveieqdF(j,C) += (3./2./tauveieq)*devtauvei(E,F)*(dtauveidF(E,F,j,C) - 1./3.*_I2(A,B)*dtauveidF(A,B,j,C)*_I2(E,F));
                            }
                        }
                    }
                }
            }
        }

        for (int i=0; i<3; i++){
            for (int j=0; j<3; j++){
                dPlasidF+= tauveieq*dDeltagammaidF(i,j) + dtauveieqdF(i,j)*Deltagammai;
            }
        }
    }

    STensor43 dFpiinvdF;
    STensor3 dFpiinvdT;
    STensorOperation::zero(dFpiinvdF);
    STensorOperation::zero(dFpiinvdT);
    if (Deltagammai >0.){
        for (int i=0; i<3; i++){
            for (int j=0; j<3; j++){
                for (int p=0; p<3; p++){
                    for (int q=0; q<3; q++){
                        dFpiinvdT(i,j) -= Fpiinv(i,p)*dFpidT(p,q)*Fpiinv(q,j);
                        for (int k=0; k<3; k++){
                            for (int l=0; l<3; l++){
                                dFpiinvdF(i,j,k,l) -= Fpiinv(i,p)*dFpidF(p,q,k,l)*Fpiinv(q,j);
                            }
                        }
                    }
                }
            }
        }
    }

    STensor43 dFveidF;
    STensor3 dFveidT;
    STensorOperation::zero(dFveidF);
    STensorOperation::zero(dFveidT);
    for (int i=0; i<3; i++){
        for (int j=0; j<3; j++){
            for (int k=0; k<3; k++){
                for (int l=0; l<3; l++){
                    for (int r=0; r<3; r++){
                        dFveidT(i,j) += Fn(i,k)*dFthiinvdT(k,l)*Ffiinv(l,r)*Fpiinv(r,j) + Fn(i,k)*Fthiinv(k,l)*Ffiinv(l,r)*dFpiinvdT(r,j);
                        for (int t=0; t<3; t++){
                            for (int h=0; h<3; h++){
                                dFveidF(i,j,t,h) +=_I2(i,t)*_I2(k,h)*Fthiinv(k,l)*Ffiinv(l,r)*Fpiinv(r,j)
                                                   +Fn(i,k)*dFthiinvdF(k,l,t,h)*Ffiinv(l,r)*Fpiinv(r,j)
                                                   +Fn(i,k)*Fthiinv(k,l)*Ffiinv(l,r)*dFpiinvdF(r,j,t,h);
                            }
                        }
                    }
                }
            }
        }
    }

    STensor63 dLveidF;
    STensor43 dLveidT;
    STensorOperation::zero(dLveidF);
    STensorOperation::zero(dLveidT);
    for (int i=0; i<3; i++){
        for (int j=0; j<3; j++){
            for (int k=0; k<3; k++){
                for (int l=0; l<3; l++){
                    for (int r=0; r<3; r++){
                        for (int s=0; s<3; s++){
                            for (int a=0; a<3; a++){
                                dLveidT(i,j,k,l) += ddlnCvei(i,j,k,l,r,s)*2.*Fvei(a,r)*dFveidT(a,s);
                                for (int p=0; p<3; p++){
                                    for (int q=0; q<3; q++){
                                        dLveidF(i,j,k,l,p,q) += ddlnCvei(i,j,k,l,r,s)*2.*Fvei(a,r)*dFveidF(a,s,p,q);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    STensor3 Svei;
    STensorOperation::zero(Svei);
    STensorOperation::multSTensor3STensor43(tauvei,dlnCvei,Svei);

    STensor43 dSveidF;
    STensor3 dSveidT;
    STensorOperation::zero(dSveidF);
    STensorOperation::zero(dSveidT);
    for (int i=0; i<3; i++){
        for (int j=0; j<3; j++){
            for (int r=0; r<3; r++){
                for (int s=0; s<3; s++){
                    dSveidT(i,j) += dtauveidT(r,s)*dlnCvei(r,s,i,j) + tauvei(r,s)*dLveidT(r,s,i,j);
                    for (int k=0; k<3; k++){
                        for (int l=0; l<3; l++){
                            dSveidF(i,j,k,l) += dtauveidF(r,s,k,l)*dlnCvei(r,s,i,j) + tauvei(r,s)*dLveidF(r,s,i,j,k,l);
                        }
                    }
                }
            }
        }
    }

    // compute mechanical tangent
        STensor43 Tangent1i, Tangent2i;
    STensorOperation::zero(Tangent1i);
    STensorOperation::zero(Tangent2i);
    for (int i=0; i<3; i++){
        for (int j=0; j<3; j++){
            for (int k=0; k<3; k++){
                for (int l=0; l<3; l++){
                    for (int p=0; p<3; p++){
                        for (int q=0; q<3; q++){
                            dPidT(i,j) += dFveidT(i,k)*Svei(k,l)*Fpiinv(q,l)*Ffiinv(p,q)*Fthiinv(j,p)
                                         +Fvei(i,k)*dSveidT(k,l)*Fpiinv(q,l)*Ffiinv(p,q)*Fthiinv(j,p)
                                         +Fvei(i,k)*Svei(k,l)*dFpiinvdT(q,l)*Ffiinv(p,q)*Fthiinv(j,p)
                                         +Fvei(i,k)*Svei(k,l)*Fpiinv(q,l)*Ffiinv(p,q)*dFthiinvdT(j,p);
                            for (int r=0; r<3; r++){
                                for (int h=0; h<3; h++){
                                    Tangenti(i,j,r,h) +=dFveidF(i,k,r,h)*Svei(k,l)*Fpiinv(q,l)*Ffiinv(p,q)*Fthiinv(j,p)
                                                        +Fvei(i,k)*dSveidF(k,l,r,h)*Fpiinv(q,l)*Ffiinv(p,q)*Fthiinv(j,p)
                                                        +Fvei(i,k)*Svei(k,l)*dFpiinvdF(q,l,r,h)*Ffiinv(p,q)*Fthiinv(j,p)
                                                        +Fvei(i,k)*Svei(k,l)*Fpiinv(q,l)*Ffiinv(p,q)*dFthiinvdF(j,p,r,h);

                                    Tangent1i(i,j,r,h) +=dFveidF(i,k,r,h)*Svei(k,l)*Fpiinv(q,l)*Ffiinv(p,q)*Fthiinv(j,p);
                                    Tangent2i(i,j,r,h) +=Fvei(i,k)*dSveidF(k,l,r,h)*Fpiinv(q,l)*Ffiinv(p,q)*Fthiinv(j,p);
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    //dDviscdT
    STensor3 devEvei;
    STensorOperation::zero(devEvei);
    double trEvei=0.;
    STensorOperation::decomposeDevTr(Evei,devEvei,trEvei);

    double trq1i=3.*pq1i;
    double trq2i=3.*pq2i;

    STensor3 AAA1, BBB1, AAA2, BBB2;
    STensorOperation::zero(AAA1);
    STensorOperation::zero(BBB1);
    STensorOperation::zero(AAA2);
    STensorOperation::zero(BBB2);
    for (int y=0; y<3; y++){
        for (int z=0; z<3; z++){
            AAA1(y,z) += Evei(y,z) - 1./9/KAPPA1i*trq1i*_I2(y,z) - 1./2/MU1i*devq1i(y,z);
            AAA2(y,z) += Evei(y,z) - 1./9/KAPPA2i*trq2i*_I2(y,z) - 1./2/MU2i*devq2i(y,z);
            BBB1(y,z) += (devq1i(y,z)  +  1./3*trq1i*_I2(y,z)) - q1i0(y,z);
            BBB2(y,z) += (devq2i(y,z)  +  1./3*trq2i*_I2(y,z)) - q2i0(y,z);
            //BBB1(y,z) += 1./3/TAU1i*(3.*KAPPA1i*trEvei - trq1i)*_I2(y,z) + 1./TAU1i*(2.*MU1i*devEvei(y,z) - devq1i(y,z));
            //BBB2(y,z) += 1./3/TAU2i*(3.*KAPPA2i*trEvei - trq2i)*_I2(y,z) + 1./TAU2i*(2.*MU2i*devEvei(y,z) - devq2i(y,z));
        }
    }

    STensor3 dEveidT;
    STensorOperation::zero(dEveidT);
    for (int y=0; y<3; y++){
        for (int z=0; z<3; z++){
            for (int A=0; A<3; A++){
                for (int B=0; B<3; B++){
                    for (int E=0; E<3; E++){
                        dEveidT(y,z)+=dlnCvei(y,z,A,B)*Fvei(E,A)*dFveidT(E,B);
                    }
                }
            }
        }
    }

    double dtrEveidT=0.;
    STensor3 ddevEveidT;
    STensorOperation::zero(ddevEveidT);
    for (int y=0; y<3; y++){
        for (int z=0; z<3; z++){
            dtrEveidT += _I2(y,z)*dEveidT(y,z);
        }
    }
    for (int y=0; y<3; y++){
        for (int z=0; z<3; z++){
            ddevEveidT(y,z) += dEveidT(y,z) - 1./3*dtrEveidT*_I2(y,z);
        }
    }

    double dtrq1idT  =3.*KAPPA1i*dtrEveidT - 3.*KAPPA1i*exp(-dh/2/TAU1i)*dtrEveidT;
    double dtrq2idT  =3.*KAPPA2i*dtrEveidT - 3.*KAPPA2i*exp(-dh/2/TAU2i)*dtrEveidT;
    STensor3 ddevq1idT, ddevq2idT;
    STensorOperation::zero(ddevq1idT);
    STensorOperation::zero(ddevq2idT);
    for (int y=0; y<3; y++){
        for (int z=0; z<3; z++){
            ddevq1idT(y,z) +=2.*MU1i*ddevEveidT(y,z) - 2.*MU1i*exp(-dh/2/TAU1i)*ddevEveidT(y,z);
            ddevq2idT(y,z) +=2.*MU2i*ddevEveidT(y,z) - 2.*MU2i*exp(-dh/2/TAU2i)*ddevEveidT(y,z);
        }
    }


    STensor3 dAAA1dT, dBBB1dT, dAAA2dT, dBBB2dT;
    STensorOperation::zero(dAAA1dT);
    STensorOperation::zero(dBBB1dT);
    STensorOperation::zero(dAAA2dT);
    STensorOperation::zero(dBBB2dT);
    for (int y=0; y<3; y++){
        for (int z=0; z<3; z++){
            dAAA1dT(y,z) +=dEveidT(y,z) - 1./9/KAPPA1i*dtrq1idT*_I2(y,z) - 1./2/MU1i*ddevq1idT(y,z);
            dAAA2dT(y,z) +=dEveidT(y,z) - 1./9/KAPPA2i*dtrq2idT*_I2(y,z) - 1./2/MU2i*ddevq2idT(y,z);
            dBBB1dT(y,z) += ddevq1idT(y,z)  +  1./3*dtrq1idT*_I2(y,z);
            dBBB2dT(y,z) += ddevq2idT(y,z)  +  1./3*dtrq2idT*_I2(y,z);
//            dBBB1dT(y,z) +=1./3/TAU1i*(3*KAPPA1i*dtrEveidT - dtrq1idT)*_I2(y,z) + 1./TAU1i*(2*MU1i*ddevEveidT(y,z) - ddevq1idT(y,z));
//            dBBB2dT(y,z) +=1./3/TAU2i*(3*KAPPA2i*dtrEveidT - dtrq2idT)*_I2(y,z) + 1./TAU2i*(2*MU2i*ddevEveidT(y,z) - ddevq2idT(y,z));
        }
    }

    for (int y=0; y<3; y++){
        for (int z=0; z<3; z++){
            dViscidT += dAAA1dT(y,z)*BBB1(y,z) + AAA1(y,z)*dBBB1dT(y,z) + dAAA2dT(y,z)*BBB2(y,z) + AAA2(y,z)*dBBB2dT(y,z);
        }
    }

    //dDvisdF
    STensor43 dEveidF;
    STensorOperation::zero(dEveidF);
    for (int y=0; y<3; y++){
        for (int z=0; z<3; z++){
            for (int j=0; j<3; j++){
                for (int x=0; x<3; x++){
                    for (int A=0; A<3; A++){
                        for (int B=0; B<3; B++){
                            for (int E=0; E<3; E++){
                                dEveidF(y,z,j,x) +=dlnCvei(y,z,A,B)*Fvei(E,A)*dFveidF(E,B,j,x);
                            }
                        }
                    }
                }
            }
        }
    }

    STensor3 dtrEveidF;
    STensor43 ddevEveidF;
    STensorOperation::zero(dtrEveidF);
    STensorOperation::zero(ddevEveidF);
    for (int y=0; y<3; y++){
        for (int z=0; z<3; z++){
            for (int j=0; j<3; j++){
                for (int x=0; x<3; x++){
                    dtrEveidF(j,x) += _I2(y,z)*dEveidF(y,z,j,x);
                }
            }
        }
    }
    for (int y=0; y<3; y++){
        for (int z=0; z<3; z++){
            for (int j=0; j<3; j++){
                for (int x=0; x<3; x++){
                    ddevEveidF(y,z,j,x) += dEveidF(y,z,j,x) - 1./3*dtrEveidF(j,x)*_I2(y,z);
                }
            }
        }
    }


    STensor3 dtrq1idF, dtrq2idF;
    STensor43 ddevq1idF, ddevq2idF;
    STensorOperation::zero(dtrq1idF);
    STensorOperation::zero(dtrq2idF);
    STensorOperation::zero(ddevq1idF);
    STensorOperation::zero(ddevq2idF);
    for (int y=0; y<3; y++){
        for (int z=0; z<3; z++){
            dtrq1idF(y,z) +=3.*KAPPA1i*dtrEveidF(y,z) - 3.*KAPPA1i*exp(-dh/2/TAU1i)*dtrEveidF(y,z);
            dtrq2idF(y,z) +=3.*KAPPA2i*dtrEveidF(y,z) - 3.*KAPPA2i*exp(-dh/2/TAU2i)*dtrEveidF(y,z);
            for (int j=0; j<3; j++){
                for (int x=0; x<3; x++){
                    ddevq1idF(y,z,j,x) +=2.*MU1i*ddevEveidF(y,z,j,x) - 2.*MU1i*exp(-dh/2/TAU1i)*ddevEveidF(y,z,j,x);
                    ddevq2idF(y,z,j,x) +=2.*MU2i*ddevEveidF(y,z,j,x) - 2.*MU2i*exp(-dh/2/TAU2i)*ddevEveidF(y,z,j,x);
                }
            }
        }
    }


    STensor43 dAAA1dF, dBBB1dF, dAAA2dF, dBBB2dF;
    STensorOperation::zero(dAAA1dF);
    STensorOperation::zero(dBBB1dF);
    STensorOperation::zero(dAAA2dF);
    STensorOperation::zero(dBBB2dF);
    for (int y=0; y<3; y++){
        for (int z=0; z<3; z++){
            for (int j=0; j<3; j++){
                for (int x=0; x<3; x++){
                    dAAA1dF(y,z,j,x) +=dEveidF(y,z,j,x) -1./9/KAPPA1i*dtrq1idF(j,x)*_I2(y,z) - 1./2/MU1i*ddevq1idF(y,z,j,x);
                    dAAA2dF(y,z,j,x) +=dEveidF(y,z,j,x) -1./9/KAPPA2i*dtrq2idF(j,x)*_I2(y,z) - 1./2/MU2i*ddevq2idF(y,z,j,x);
                    dBBB1dF(y,z,j,x) += ddevq1idF(y,z,j,x)  +  1./3*dtrq1idF(j,x)*_I2(y,z);
                    dBBB2dF(y,z,j,x) += ddevq2idF(y,z,j,x)  +  1./3*dtrq2idF(j,x)*_I2(y,z);
//                    dBBB1dF(y,z,j,x) +=1./3/TAU1i*(3.*KAPPA1i*dtrEveidF(j,x) - dtrq1idF(j,x))*_I2(y,z) + 1./TAU1i*(2.*MU1i*ddevEveidF(y,z,j,x) - ddevq1idF(y,z,j,x));
//                    dBBB2dF(y,z,j,x) +=1./3/TAU2i*(3.*KAPPA2i*dtrEveidF(j,x) - dtrq2idF(j,x))*_I2(y,z) + 1./TAU2i*(2.*MU2i*ddevEveidF(y,z,j,x) - ddevq2idF(y,z,j,x));
                }
            }
        }
    }

    for (int y=0; y<3; y++){
        for (int z=0; z<3; z++){
            for (int j=0; j<3; j++){
                for (int x=0; x<3; x++){
                    dViscidF(j,x)+= dAAA1dF(y,z,j,x)*BBB1(y,z) + AAA1(y,z)*dBBB1dF(y,z,j,x) + dAAA2dF(y,z,j,x)*BBB2(y,z) + AAA2(y,z)*dBBB2dF(y,z,j,x);
                }
            }
        }
    }

    //dDEdisdF
    //dDEdisdT*/
}


void mlawPhenomenologicalSMP::tangentamorphousEP
    (const STensor3& Fn, const STensor3& F0, const STensor3& FmAM, const bool stiff,
     const STensor3 &FthAM, const STensor3 &dFthAMdT, const STensor43 &dFthAMdF,
      double &PsiAML2, STensor3& PAML2, STensor3& dPAML2dT, STensor43& TangentAML2) const
{
    PsiAML2=0.;
    STensorOperation::zero(PAML2);
    STensorOperation::zero(dPAML2dT);
    STensorOperation::zero(TangentAML2);

    STensor3 FthAMinv;
    STensorOperation::zero(FthAMinv);
    STensorOperation::inverseSTensor3(FthAM, FthAMinv);

    STensor3 dFthAMinvdT;
    STensorOperation::zero(dFthAMinvdT);
    for (int R=0; R<3; R++){
        for (int A=0; A<3; A++){
            for (int O=0; O<3; O++){
                for (int L=0; L<3; L++){
                    dFthAMinvdT(R,A) -= FthAMinv(R,O)*dFthAMdT(O,L)*FthAMinv(L,A);
                }
            }
        }
    }
    static STensor43 dFthAMinvdF;
    STensorOperation::zero(dFthAMinvdF);
    for (int i=0; i<3; i++){
        for (int l=0; l<3; l++){
            for (int m=0; m<3; m++){
                for (int n=0; n<3; n++){
                    for (int j=0; j<3; j++){
                        for (int k=0; k<3; k++){
                            dFthAMinvdF(i,l,m,n) -= FthAMinv(i,j)*dFthAMdF(j,k,m,n)*FthAMinv(k,l);
                        }
                    }
                }
            }
        }
    }

    STensor3 dFmAMdT;
    STensorOperation::zero(dFmAMdT);
    for (int i=0; i<3; i++){
        for (int j=0; j<3; j++){
            for (int k=0; k<3; k++){
                dFmAMdT(i,k) +=Fn(i,j)*dFthAMinvdT(j,k);
            }
        }
    }

    STensor43 dFmAMdF;
    STensorOperation::zero(dFmAMdF);
    for (int i=0; i<3; i++){
        for (int j=0; j<3; j++){
            for (int k=0; k<3; k++){
                for (int m=0; m<3; m++){
                    for (int n=0; n<3; n++){
                        dFmAMdF(i,k,m,n) += _I2(i,m)*_I2(j,n)*FthAMinv(j,k) + Fn(i,j)*dFthAMinvdF(j,k,m,n);
                    }
                }
            }
        }
    }


    STensor3 PmAM;
    STensor43 TangentmAM;
    STensorOperation::zero(PmAM);
    STensorOperation::zero(TangentmAM);
    _EPFunc_AM->constitutive(FmAM, PmAM, stiff, &TangentmAM);
    PsiAML2= _EPFunc_AM->get(FmAM);
    STensorOperation::multSTensor3SecondTranspose(PmAM, FthAMinv, PAML2);


    STensor3 dPmAMdT;
    STensorOperation::zero(dPmAMdT);
    STensorOperation::multSTensor43STensor3(TangentmAM, dFmAMdT, dPmAMdT);
    for (int i=0; i<3; i++){
        for (int j=0; j<3; j++){
            for (int k=0; k<3; k++){
                dPAML2dT(i,k) += dPmAMdT(i,j)*FthAMinv(k,j) + PmAM(i,j)*dFthAMinvdT(k,j);
            }
        }
    }


    STensor43 dPmAMdF;
    STensorOperation::zero(dPmAMdF);
    STensorOperation::multSTensor43(TangentmAM, dFmAMdF, dPmAMdF);
    for (int i=0; i<3; i++){
        for (int j=0; j<3; j++){
            for (int k=0; k<3; k++){
                for (int m=0; m<3; m++){
                    for (int n=0; n<3; n++){
                        TangentAML2(i,k,m,n) +=dPmAMdF(i,j,m,n)*FthAMinv(k,j) + PmAM(i,j)*dFthAMinvdF(k,j,m,n);
                    }
                }
            }
        }
    }


    //EDiss
    //EDissdT, EDissdF
}



void mlawPhenomenologicalSMP::computePi
(const STensor3& Fn, const double T,  const double dh,
 const double KinfiT, const double GinfiT, const double Kei, const double Gei, const std::vector<double> _ViscParami,
 const STensor3& Evei0, const STensor3& Fthi, const STensor3& Ffi, const STensor3& Fpi,
 STensor3& Fvei, STensor3& Evei, STensor3& tauvei, STensor43& dlnCvei, STensor63& ddlnCvei, STensor3& Pi,
 const STensor3 &A1i0, const STensor3 &A2i0, const double B1i0, const double B2i0,
 STensor3 &A1i, STensor3 &A2i, double &B1i,  double &B2i,
 STensor3& devq1i, double& pq1i, STensor3& devq2i, double& pq2i, 
       double anisotropicThermalCoefAmplitudeBulk, double anisotropicThermalCoefAmplitudeShear, 
       double anisotropicThermalAlphaCoefTanhTempBulk, double anisotropicThermalAlphaCoefTanhTempShear , double TRef,  
  STensor3 &strainEffect, STensor3 &dStrainEffectDt, STensor43  &dStrainEffectdEne) const
{
/*    double MU1i      =(_ViscParami)[0];
    double KAPPA1i   =(_ViscParami)[1];
    double TAU1i     =(_ViscParami)[2];
    double MU2i      =(_ViscParami)[3];double 
    double KAPPA2i   =(_ViscParami)[4];
    double TAU2i     =(_ViscParami)[5];

    STensorOperation::zero(Fvei);
    STensorOperation::zero(Evei);
    STensorOperation::zero(tauvei);
    STensorOperation::zero(dlnCvei);
    STensorOperation::zero(ddlnCvei);
    STensorOperation::zero(Pi);
    STensorOperation::zero(A1i);
    STensorOperation::zero(A2i);
    B1i=0.;
    B2i=0.;
    STensorOperation::zero(devq1i);
    STensorOperation::zero(pq1i);
    STensorOperation::zero(devq2i);
    STensorOperation::zero(pq2i);

    STensor3 Fthiinv, Ffiinv, Fpiinv;
    STensorOperation::zero(Fthiinv);
    STensorOperation::zero(Ffiinv);
    STensorOperation::zero(Fpiinv);
    STensorOperation::inverseSTensor3(Fthi, Fthiinv);
    STensorOperation::inverseSTensor3(Ffi,  Ffiinv);
    STensorOperation::inverseSTensor3(Fpi,  Fpiinv);

    static STensor3 Fvei_int, Fvei_int2;
    STensorOperation::zero(Fvei_int);
    STensorOperation::zero(Fvei_int2);
    STensorOperation::multSTensor3(Fn, Fthiinv, Fvei_int);
    STensorOperation::multSTensor3(Fvei_int,  Ffiinv,  Fvei_int2);
    STensorOperation::multSTensor3(Fvei_int2, Fpiinv,  Fvei);

    STensor3 Cvei;
    STensorOperation::zero(Cvei);
    STensorOperation::multSTensor3FirstTranspose(Fvei, Fvei, Cvei);
    STensorOperation::logSTensor3(Cvei, _order, Evei, &dlnCvei, &ddlnCvei);
    Evei*=0.5;
    STensor3 devEvei  =Evei.dev();
    double trEvei     =Evei.trace();
    
    
    //thermal anisotropy correction
    STensorOperation::zero(strainEffect);
    STensorOperation::zero(dStrainEffectDt);
    STensorOperation::zero(dStrainEffectdEne);
    funEnThAI(T,TRef, Evei0, strainEffect, dStrainEffectDt, dStrainEffectdEne, anisotropicThermalCoefAmplitude, anisotropicThermalCoefUnused,
              anisotropicThermalAlphaCoefTanhEn, anisotropicThermalAlphaCoefTanhTemp);


    STensor3 devEvei0 =Evei0.dev();
    double trEvei0    =Evei0.trace();

    for (int i=0; i<3; i++){
        for (int j=0; j<3; j++){
            A1i(i,j)+= A1i0(i,j)*exp(-dh/TAU1i) + 2.*MU1i*exp(-dh/2./TAU1i)*(devEvei(i,j) - devEvei0(i,j));
            A2i(i,j)+= A2i0(i,j)*exp(-dh/TAU2i) + 2.*MU2i*exp(-dh/2./TAU2i)*(devEvei(i,j) - devEvei0(i,j));
        }
    }
    B1i= B1i0*exp(-dh/TAU1i) + KAPPA1i*exp(-dh/2./TAU1i)*(trEvei - trEvei0);
    B2i= B2i0*exp(-dh/TAU2i) + KAPPA2i*exp(-dh/2./TAU2i)*(trEvei - trEvei0);

    STensor3 devtauvei;
    STensorOperation::zero(devtauvei);
    double pvei=0.;
    for (int i=0; i<3; i++){
        for (int j=0; j<3; j++){
            devtauvei(i,j) += 2.*Gei*devEvei(i,j) - 2.*(Gei-GinfiT)*devEvei0(i,j) + exp(-dh/TAU1i)*A1i0(i,j) + exp(-dh/TAU2i)*A2i0(i,j);
        }
    }
    devtauvei-=2.*GinfiT*strainEffect;
    pvei  = Kei*trEvei - (Kei-KinfiT)*trEvei0 + exp(-dh/TAU1i)*B1i0 + exp(-dh/TAU2i)*B2i0;
    tauvei= devtauvei + pvei*_I2;

    STensor3 Svei;
    STensorOperation::zero(Svei);
    STensorOperation::multSTensor3STensor43(tauvei, dlnCvei, Svei);

     STensor3 aux_1, aux_2, aux_3, FpiinvT, FfiinvT, FthiinvT;
    STensorOperation::zero(aux_1);
    STensorOperation::zero(aux_2);
    STensorOperation::zero(aux_3);
    STensorOperation::zero(FpiinvT);
    STensorOperation::zero(FfiinvT);
    STensorOperation::zero(FthiinvT);
    STensorOperation::transposeSTensor3(Fpiinv,  FpiinvT);
    STensorOperation::transposeSTensor3(Ffiinv,  FfiinvT);
    STensorOperation::transposeSTensor3(Fthiinv, FthiinvT);
    STensorOperation::multSTensor3(Fvei,  Svei,     aux_1);
    STensorOperation::multSTensor3(aux_1, FpiinvT,  aux_2);
    STensorOperation::multSTensor3(aux_2, FfiinvT,  aux_3);
    STensorOperation::multSTensor3(aux_3, FthiinvT, Pi);

    for (int i=0; i<3; i++){
        for (int j=0; j<3; j++){
            devq1i(i,j) += 2.*MU1i*devEvei(i,j) - A1i(i,j);
            devq2i(i,j) += 2.*MU2i*devEvei(i,j) - A2i(i,j);
        }
    }
    pq1i = KAPPA1i*trEvei - B1i;
    pq2i = KAPPA2i*trEvei - B2i;*/
}



void mlawPhenomenologicalSMP::computeFrzdSourcei(const STensor3& Ffi0, const STensor3& Ffi, const STensor3& Fpi,
                                        const STensor3& Fvei, const STensor3& tauvei, const STensor43& dlnCvei, double& dFrzdi) const
{
/*    dFrzdi=0.;
    STensor3 Ffiinv, Fpiinv, DFfi_int, DFfi, Mvei, Svei, Cvei, aux1, aux2;

    STensorOperation::inverseSTensor3(Ffi, Ffiinv);
    STensorOperation::inverseSTensor3(Fpi, Fpiinv);
    DFfi_int  = Ffi;
    DFfi_int -= Ffi0;
    STensorOperation::multSTensor3(DFfi_int, Ffiinv, DFfi);

    STensorOperation::multSTensor3(Fpi,  DFfi,   aux1);
    STensorOperation::multSTensor3(aux1, Fpiinv, aux2);
    STensorOperation::multSTensor3FirstTranspose(Fvei, Fvei, Cvei);
    STensorOperation::multSTensor3STensor43(tauvei, dlnCvei, Svei);
    STensorOperation::multSTensor3(Cvei, Svei, Mvei);

    dFrzdi = STensorOperation::doubledot(Mvei, aux2);*/
}


void mlawPhenomenologicalSMP::computeViscSourcei
    (const std::vector<double> _ViscParami,
     const STensor3& Evei, const STensor3& devq1i, const double pq1i, const STensor3& devq2i, const double pq2i,
     const STensor3& q1i0, const STensor3& q2i0, double& dVisci) const
{
/*    double MU1i      =(_ViscParami)[0];
    double KAPPA1i   =(_ViscParami)[1];
    double TAU1i     =(_ViscParami)[2];
    double MU2i      =(_ViscParami)[3];
    double KAPPA2i   =(_ViscParami)[4];
    double TAU2i     =(_ViscParami)[5];

    dVisci=0.;
    STensor3 devEvei;
    STensorOperation::zero(devEvei);
    double trEvei=0.;
    STensorOperation::decomposeDevTr(Evei,devEvei,trEvei);

    double trq1i=3.*pq1i;
    double trq2i=3.*pq2i;

    STensor3 AAA1, BBB1, AAA2, BBB2;
    STensorOperation::zero(AAA1);
    STensorOperation::zero(BBB1);
    STensorOperation::zero(AAA2);
    STensorOperation::zero(BBB2);
    for (int i=0; i<3; i++){
        for (int j=0; j<3; j++){
            AAA1(i,j) += Evei(i,j) - 1./9./KAPPA1i*trq1i*_I2(i,j) - 1./2./MU1i*devq1i(i,j);
            AAA2(i,j) += Evei(i,j) - 1./9./KAPPA2i*trq2i*_I2(i,j) - 1./2./MU2i*devq2i(i,j);
            BBB1(i,j) += (devq1i(i,j)  +  1./3*trq1i*_I2(i,j)) - q1i0(i,j);
            BBB2(i,j) += (devq2i(i,j)  +  1./3*trq2i*_I2(i,j)) - q2i0(i,j);
        }
    }

    for (int i=0; i<3; i++){
        for (int j=0; j<3; j++){
            dVisci += AAA1(i,j)*BBB1(i,j) + AAA2(i,j)*BBB2(i,j);
        }
    }*/
}




void mlawPhenomenologicalSMP::computeESourcei
    (const double KinfiT, const double  GinfiT, const std::vector<double> _ViscParami,
     const STensor3& Evei, const STensor3& devq1i, const double pq1i, const STensor3& devq2i, const double pq2i,
     double& PsiiElasDEV, double& PsiiREST) const
{
/*    PsiiElasDEV=0.;
    PsiiREST=0.;

    double MU1i      =(_ViscParami)[0];
    double KAPPA1i   =(_ViscParami)[1];
    double TAU1i     =(_ViscParami)[2];
    double MU2i      =(_ViscParami)[3];
    double KAPPA2i   =(_ViscParami)[4];
    double TAU2i     =(_ViscParami)[5];

    double trq1i=3.*pq1i;
    double trq2i=3.*pq2i;

    static STensor3 devEvei;
    STensorOperation::zero(devEvei);
    devEvei=Evei.dev();
    double trEvei=Evei.trace();

    PsiiREST= KinfiT/2.*trEvei*trEvei + KAPPA1i/2.*trEvei*trEvei + KAPPA2i/2.*trEvei*trEvei + 1./18./KAPPA1i*trq1i*trq1i + 1./18./KAPPA2i*trq2i*trq2i;
    for (int i=0; i<3; i++){
        for (int j=0; j<3; j++){
            PsiiElasDEV +=GinfiT*devEvei(i,j)*devEvei(i,j);
            PsiiREST +=MU1i*devEvei(i,j)*devEvei(i,j)  + MU2i*devEvei(i,j)*devEvei(i,j)
                      -(devq1i(i,j) + 1./3.*trq1i*_I2(i,j))*Evei(i,j)  + 1./4./MU1i*devq1i(i,j)*devq1i(i,j)
                      -(devq2i(i,j) + 1./3.*trq2i*_I2(i,j))*Evei(i,j)  + 1./4./MU2i*devq2i(i,j)*devq2i(i,j);
        }
    }
    //dEdiss*/
}

