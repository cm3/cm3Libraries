//
//
//
// Description: Define j2 hardening law
//
//
// Author:  <L. Noels>, (C) 2013
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "j2IsotropicHardening.h"

J2IsotropicHardening::J2IsotropicHardening(const int num, double yield0, const bool init): _num(num), _initialized(init),
											_yield0(yield0)
{
  _temFunc_Sy0= new constantScalarFunction(1.);
  if(_yield0 < 0) Msg::Error("J2IsotropicHardening: negative yield stress");
}

J2IsotropicHardening::J2IsotropicHardening(const J2IsotropicHardening &source)
{
  _temFunc_Sy0 = NULL;
  if (source._temFunc_Sy0 != NULL){
    _temFunc_Sy0 = source._temFunc_Sy0->clone();
  }
  _num = source._num;
  _initialized = source._initialized;
  _yield0 = source._yield0;
}

J2IsotropicHardening& J2IsotropicHardening::operator=(const J2IsotropicHardening &source)
{
  if (source._temFunc_Sy0 != NULL){
    if (_temFunc_Sy0!=NULL){
      _temFunc_Sy0->operator=(*source._temFunc_Sy0);
    }
    else{
      _temFunc_Sy0 = source._temFunc_Sy0->clone();
    }
  }
  _num = source._num;
  _initialized = source._initialized;
  _yield0 = source._yield0;
  return *this;
}

J2IsotropicHardening::~J2IsotropicHardening(){
  if (_temFunc_Sy0!= NULL){
    delete _temFunc_Sy0;
    _temFunc_Sy0 = NULL;
  }
};
//-----------------------added
void J2IsotropicHardening::setTemperatureFunction_Sy0(const scalarFunction& Tfunc){
  if (_temFunc_Sy0 != NULL) delete _temFunc_Sy0;
  _temFunc_Sy0 = Tfunc.clone();
}

//------------------------------------------end
PerfectlyPlasticJ2IsotropicHardening::PerfectlyPlasticJ2IsotropicHardening(const int num, double yield0, const bool init) :
					J2IsotropicHardening(num,yield0,init)
{

}
PerfectlyPlasticJ2IsotropicHardening::PerfectlyPlasticJ2IsotropicHardening(const PerfectlyPlasticJ2IsotropicHardening &source) :
					J2IsotropicHardening(source)
{

}

PerfectlyPlasticJ2IsotropicHardening& PerfectlyPlasticJ2IsotropicHardening::operator=(const J2IsotropicHardening &source)
{
  J2IsotropicHardening::operator=(source);
  const PerfectlyPlasticJ2IsotropicHardening* src = dynamic_cast<const PerfectlyPlasticJ2IsotropicHardening*>(&source);
  if(src != NULL)
  {
  }
  return *this;
}
void PerfectlyPlasticJ2IsotropicHardening::createIPVariable(IPJ2IsotropicHardening* &ipv) const
{
  if(ipv != NULL) delete ipv;
  ipv = new IPPerfectlyPlasticJ2IsotropicHardening(getYield0());
}

void PerfectlyPlasticJ2IsotropicHardening::hardening(double p0, const IPJ2IsotropicHardening &ipvprev, double p, IPJ2IsotropicHardening &ipv) const
{
  double dR=0, ddR=0, intR=0, dRdT =0, ddRdTT=0., ddRdT=0.;
  double R0 = getYield0();
  double R = R0;
  if (ipvprev.isSaturated()){
    R = ipvprev.getR();
    dR = 0.;
    ddR = 0.;
    intR = ipvprev.getIntegR() + R*(p-p0);
  }
  else{
    double tol=1.e-15;
    if(p>0)
    {
      R=getYield0()+tol*p;
      dR=tol;
      ddR=0;
      intR=0.5*tol*p*p+getYield0()*p;
    }
  }
  ipv.set(R0,R,dR,ddR,intR,dRdT,ddRdTT,ddRdT);
}
//-------------------------------------added
void PerfectlyPlasticJ2IsotropicHardening::hardening(double p0, const IPJ2IsotropicHardening &ipvprev, double p,double T, IPJ2IsotropicHardening &ipv) const
{
  double dR=0, ddR=0, intR=0, dRdT=0, ddRdTT=0, ddRdT=0.;
  double R0 = getYield0(T);
  double R = R0;
  if (ipvprev.isSaturated()){
    R = ipvprev.getR();
    dR = 0.;
    ddR = 0.;
    intR = ipvprev.getIntegR()+R*(p-p0);
  }
  else{
    double tol=1.e-15;
    if(p>0)
    {
      R=getYield0(T)+tol*p;
      dR=tol;
      ddR=0;
      intR=0.5*tol*p*p+getYield0(T)*p;
      dRdT=getDYield0DT(T);
      ddRdTT=getDDYield0DTT(T);
      ddRdT=0.;
    }
  }
  ipv.set(R0,R,dR,ddR,intR,dRdT,ddRdTT,ddRdT);
}
//-----------------------------------------end
J2IsotropicHardening * PerfectlyPlasticJ2IsotropicHardening::clone() const
{
  return new PerfectlyPlasticJ2IsotropicHardening(*this);
}


UserFunctionIsotropicHardening::UserFunctionIsotropicHardening(const int num, double yield0, const generalMapping& isoFunc, const bool init):
  J2IsotropicHardening(num,yield0,init)
{
  _isoFuncUser = isoFunc.clone();
};

UserFunctionIsotropicHardening::~UserFunctionIsotropicHardening()
{
  if (_isoFuncUser != NULL)
  {
    delete _isoFuncUser;
    _isoFuncUser = NULL;
  }
};
UserFunctionIsotropicHardening::UserFunctionIsotropicHardening(const UserFunctionIsotropicHardening &source):
  J2IsotropicHardening(source)
{
  _isoFuncUser = NULL;
  if (source._isoFuncUser != NULL)
  {
    _isoFuncUser = source._isoFuncUser->clone();
  }
}
UserFunctionIsotropicHardening& UserFunctionIsotropicHardening::operator=(const J2IsotropicHardening &source)
{
  J2IsotropicHardening::operator =(source);
  const UserFunctionIsotropicHardening* psrc = dynamic_cast<const UserFunctionIsotropicHardening*>(&source);
  if (psrc != NULL)
  {
    if (_isoFuncUser != NULL)
    {
      delete _isoFuncUser; 
      _isoFuncUser = NULL;
    }
    if (psrc->_isoFuncUser != NULL)
    {
      _isoFuncUser = psrc->_isoFuncUser->clone();
    }    
  };
  return *this;
};
    
void UserFunctionIsotropicHardening::createIPVariable(IPJ2IsotropicHardening* &ipv) const
{
  if(ipv != NULL) delete ipv;
  ipv = new IPUserJ2IsotropicHardening(getYield0());
}

void UserFunctionIsotropicHardening::hardening(double p0, const IPJ2IsotropicHardening &ipvprev, double p, IPJ2IsotropicHardening &ipv) const
{
  double dR=0, ddR=0, intR=0, dRdT =0, ddRdTT=0., ddRdT=0.;
  double R0 = getYield0();
  double R = R0;
  if (ipvprev.isSaturated()){
    R = ipvprev.getR();
    dR = 0.;
    ddR = 0.;
    intR = ipvprev.getIntegR() + R*(p-p0);
  }
  else{
    std::vector<double> xvars(1, p);
    std::vector<double> out = _isoFuncUser->evaluate(xvars);
    if (out.size() < 4)
    {
      Msg::Error("general mapping in UserFunctionIsotropicHardening must return a vector of at least 4 elements: R, dR, ddR, intR");
      Msg::Exit(0);
    }
    R = out[0];
    dR = out[1];
    ddR = out[2];
    intR = out[3]; 
  }
  ipv.set(R0,R,dR,ddR,intR,dRdT,ddRdTT,ddRdT);
}
//-------------------------------------added
void UserFunctionIsotropicHardening::hardening(double p0, const IPJ2IsotropicHardening &ipvprev, double p,double T, IPJ2IsotropicHardening &ipv) const
{
  double dR=0, ddR=0, intR=0, dRdT=0, ddRdTT=0, ddRdT=0.;
  double R0 = getYield0(T);
  double R = R0;
  if (ipvprev.isSaturated()){
    R = ipvprev.getR();
    dR = 0.;
    ddR = 0.;
    intR = ipvprev.getIntegR()+R*(p-p0);
  }
  else{
    std::vector<double> xvars(2);
    xvars[0] = p;
    xvars[1] = T;
    std::vector<double> out = _isoFuncUser->evaluate(xvars);
    if (out.size() < 7)
    {
      Msg::Error("general mapping in UserFunctionIsotropicHardening must return a vector of at least 7 elements: R,dR,ddR,intR,dRdT,ddRdTT,ddRdT");
      Msg::Exit(0);
    }
    R = out[0];
    dR = out[1];
    ddR = out[2];
    intR = out[3]; 
    dRdT = out[4]; 
    ddRdTT = out[5];
    ddRdT = out[6]; 
  }
  ipv.set(R0,R,dR,ddR,intR,dRdT,ddRdTT,ddRdT);
};
//-----------------------------------------end
J2IsotropicHardening * UserFunctionIsotropicHardening::clone() const
{
  return new UserFunctionIsotropicHardening(*this);
}

PowerLawJ2IsotropicHardening::PowerLawJ2IsotropicHardening(const int num, double yield0, double h, double hexp, double pth, const bool init) :
					J2IsotropicHardening(num,yield0,init), _h(h), _hexp(hexp), _pth(pth)
{
  if(_h<0. or _hexp<0. or _pth<0.) Msg::Error("PowerLawJ2IsotropicHardening: negative hardening parameters");
  if(_pth<1.e-16) _pth=1.e-16;
  _temFunc_h= new constantScalarFunction(1.);
  _temFunc_hexp= new constantScalarFunction(1.);
}

PowerLawJ2IsotropicHardening::PowerLawJ2IsotropicHardening(const PowerLawJ2IsotropicHardening &source) :
					J2IsotropicHardening(source)
{
  _h = source._h;
  _hexp = source._hexp;
  _pth = source._pth;

  _temFunc_h = NULL;
  if (source._temFunc_h != NULL){
    _temFunc_h = source._temFunc_h->clone();
  }

  _temFunc_hexp = NULL;
  if (source._temFunc_hexp != NULL){
    _temFunc_hexp = source._temFunc_hexp->clone();
  }
}

PowerLawJ2IsotropicHardening::~PowerLawJ2IsotropicHardening(){
  if (_temFunc_h != NULL) {delete _temFunc_h; _temFunc_h = NULL;};
  if (_temFunc_hexp != NULL){delete _temFunc_hexp; _temFunc_hexp = NULL;};
};

void PowerLawJ2IsotropicHardening::setTemperatureFunction_h(const scalarFunction& Tfunc){
      if (_temFunc_h != NULL) delete _temFunc_h;
      _temFunc_h = Tfunc.clone();}
void PowerLawJ2IsotropicHardening::setTemperatureFunction_hexp(const scalarFunction& Tfunc){
      if (_temFunc_hexp != NULL) delete _temFunc_hexp;
      _temFunc_hexp = Tfunc.clone();}

PowerLawJ2IsotropicHardening& PowerLawJ2IsotropicHardening::operator=(const J2IsotropicHardening &source)
{
  J2IsotropicHardening::operator=(source);
  const PowerLawJ2IsotropicHardening* src = dynamic_cast<const PowerLawJ2IsotropicHardening*>(&source);
  if(src != NULL)
  {
    _h = src->_h;
    _hexp = src->_hexp;
    _pth = src->_pth;

    if (src->_temFunc_h != NULL){
      if (_temFunc_h != NULL){
        _temFunc_h->operator=(*src->_temFunc_h);
      }
      else{
        _temFunc_h = src->_temFunc_h->clone();
      }
    }

    if (src->_temFunc_hexp != NULL){
      if (_temFunc_hexp != NULL){
        _temFunc_hexp->operator=(*src->_temFunc_hexp);
      }
      else
        _temFunc_hexp = src->_temFunc_hexp->clone();
    }

  }
  return *this;
}
void PowerLawJ2IsotropicHardening::createIPVariable(IPJ2IsotropicHardening* &ipv) const
{
  if(ipv != NULL) delete ipv;
  ipv = new IPPowerLawJ2IsotropicHardening(getYield0());
}

void PowerLawJ2IsotropicHardening::hardening(double p0, const IPJ2IsotropicHardening &ipvprev, double p, IPJ2IsotropicHardening &ipv) const
{
 // double tol=1.e-16;
  double dR=0, ddR=0, intR=0, dRdT=0, ddRdTT=0,ddRdT=0.;
  double R0 = getYield0();
  double R = R0;
  if (ipvprev.isSaturated()){
    R = ipvprev.getR();
    dR = 0.;
    ddR = 0.;
    intR = ipvprev.getIntegR()+R*(p-p0);
  }
  else{
    if(p< _pth)
    {
     R=getYield0()+_h*pow(_pth,_hexp)/_pth*p;
     dR=_h*pow(_pth,_hexp)/_pth;
     ddR=0.;
     intR=getYield0()*p+_h*pow(_pth,_hexp)/_pth*p*p/2.;
    }
    else
    {
      R=getYield0()+_h*pow(p,_hexp);
      dR = _h*_hexp*pow(p,_hexp)/p;
      ddR = dR*(_hexp-1.)/p;
      intR=getYield0()*p;
      if(fabs(_hexp+1.)!=0)
          intR+=_h*pow(_pth,_hexp)/_pth*_pth*_pth/2.+_h*pow(p,_hexp+1.)/(_hexp+1.)-_h*pow(_pth,_hexp+1.)/(_hexp+1.);
    }

  }
  ipv.set(R0,R,dR,ddR,intR,dRdT,ddRdTT,ddRdT);
}
//--------------------------------------------------added
void PowerLawJ2IsotropicHardening::hardening(double p0, const IPJ2IsotropicHardening &ipvprev, double p,double T, IPJ2IsotropicHardening &ipv) const
{
  double dR=0, ddR=0, intR=0, dRdT=0, ddRdTT=0, ddRdT=0.;
  double R0 = getYield0(T);
  double R = R0;
  if (ipvprev.isSaturated()){
    R = ipvprev.getR();
    dR = 0.;
    ddR = 0.;
    intR = ipvprev.getIntegR()+R*(p-p0);
  }
  else{
    double tol=1.e-16;
    double hT=_h*_temFunc_h->getVal(T);
    double hexpT=_hexp*_temFunc_hexp->getVal(T);
    if(p< tol)
    {
      R=getYield0(T);
      if(hT<1.)
      {
        dR = 1e20;
        ddR = -1e20;
      }
      else
      {
        dR = hT;
        ddR = 0.;
      }
    }
    else
    {
      R=getYield0(T)+hT*pow(p,hexpT);
      dR = hT*hexpT*pow(p,hexpT)/p;
      ddR = dR*(hexpT-1.)/p;
      intR=getYield0(T)*p;
      if(fabs(hexpT+1.)!=0)
          intR+=hT*pow(p,hexpT+1.)/(hexpT+1.);
    }

    double dhdT=_h*_temFunc_h->getDiff(T);
    double ddhdTT=_h*_temFunc_h->getDoubleDiff(T);
    double dhexpdT=_hexp*_temFunc_hexp->getDiff(T);
    double ddhexpdTT=_hexp*_temFunc_hexp->getDoubleDiff(T);
    if(p<tol && dhexpdT != 0)
    {
      dRdT=-1e20;
      ddRdTT=-1e20;
    }
    else if (p<tol && dhexpdT == 0)
    {
      dRdT=getDYield0DT(T)+dhdT*pow(p,hexpT);
      ddRdTT=getDDYield0DTT(T)+ddhdTT*pow(p,hexpT);
      ddRdT = 0.;
    }
    else
    {
      dRdT=getDYield0DT(T)+dhdT*pow(p,hexpT)+hT*dhexpdT*log(p)*pow(p,hexpT);
      ddRdTT=getDDYield0DTT(T)+ddhdTT*pow(p,hexpT)+2.*dhdT*dhexpdT*log(p)*pow(p,hexpT)+hT*( ddhexpdTT*log(p)*pow(p,hexpT) + pow(p,hexpT)*pow(dhexpdT*log(p),2) );
      ddRdT=dhdT*hexpT*pow(p,hexpT-1) + hT*dhexpdT*( pow(p,hexpT-1) + hexpT*(hexpT-1)*pow(p,hexpT-1)*log(p) );
    }

  }
  ipv.set(R0,R,dR,ddR,intR,dRdT,ddRdTT,ddRdT);
}
//--------------------------------------------------------------------------end
J2IsotropicHardening * PowerLawJ2IsotropicHardening::clone() const
{
  return new PowerLawJ2IsotropicHardening(*this);
}

//-----------------------------------------end
ExponentialJ2IsotropicHardening::ExponentialJ2IsotropicHardening(const int num, double yield0, double h, double hexp, const bool init) :
					J2IsotropicHardening(num,yield0,init), _h(h), _hexp(hexp)
{
  if(h<0 or hexp<0) Msg::Error("PowerLawJ2IsotropicHardening: negative hardening parameters");
  _temFunc_h= new constantScalarFunction(1.);
  _temFunc_hexp= new constantScalarFunction(1.);
}
ExponentialJ2IsotropicHardening::ExponentialJ2IsotropicHardening(const ExponentialJ2IsotropicHardening &source) :
					J2IsotropicHardening(source)
{
  _h = source._h;
  _hexp = source._hexp;

  _temFunc_h = NULL;
  if (source._temFunc_h != NULL){
    _temFunc_h = source._temFunc_h->clone();
  }

  _temFunc_hexp = NULL;
  if (source._temFunc_hexp != NULL){
    _temFunc_hexp = source._temFunc_hexp->clone();
  }
}

ExponentialJ2IsotropicHardening& ExponentialJ2IsotropicHardening::operator=(const J2IsotropicHardening &source)
{
  J2IsotropicHardening::operator=(source);
  const ExponentialJ2IsotropicHardening* src = dynamic_cast<const ExponentialJ2IsotropicHardening*>(&source);
  if(src != NULL)
  {
    _h = src->_h;
    _hexp = src->_hexp;
    if (src->_temFunc_h != NULL){
      if (_temFunc_h != NULL){
        _temFunc_h->operator=(*src->_temFunc_h);
      }
      else{
        _temFunc_h = src->_temFunc_h->clone();
      }
    }

    if (src->_temFunc_hexp != NULL){
      if (_temFunc_hexp != NULL){
        _temFunc_hexp->operator=(*src->_temFunc_hexp);
      }
      else
        _temFunc_hexp = src->_temFunc_hexp->clone();
    }

  }
  return *this;
}

ExponentialJ2IsotropicHardening::~ExponentialJ2IsotropicHardening(){
  if (_temFunc_h != NULL){delete _temFunc_h; _temFunc_h = NULL;};
  if (_temFunc_hexp != NULL) {delete _temFunc_hexp; _temFunc_hexp= NULL;};
};

void ExponentialJ2IsotropicHardening::setTemperatureFunction_h(const scalarFunction& Tfunc){
  if (_temFunc_h != NULL) delete _temFunc_h;
  _temFunc_h = Tfunc.clone();
}
void ExponentialJ2IsotropicHardening::setTemperatureFunction_hexp(const scalarFunction& Tfunc){
  if (_temFunc_hexp != NULL) delete _temFunc_hexp;
  _temFunc_hexp = Tfunc.clone();
}


void ExponentialJ2IsotropicHardening::createIPVariable(IPJ2IsotropicHardening* &ipv) const
{
  if(ipv != NULL) delete ipv;
  ipv = new IPExponentialJ2IsotropicHardening(getYield0());
}

void ExponentialJ2IsotropicHardening::hardening(double p0, const IPJ2IsotropicHardening &ipvprev, double p, IPJ2IsotropicHardening &ipv) const
{
  double dR=0, ddR=0, intR=0, dRdT=0, ddRdTT=0, ddRdT=0.;
  double R0 = getYield0();
  double R = R0;
  if (ipvprev.isSaturated()){
    R = ipvprev.getR();
    dR = 0.;
    ddR = 0.;
    intR = ipvprev.getIntegR()+ R*(p-p0);
  }
  else{
    double tol=1.e-16;
    if(p< tol)
    {
      R=getYield0();
      dR = _h*_hexp;
      ddR = -_hexp*(dR);
      intR=getYield0()*p;
    }
    else
    {
      double tmp = exp(-_hexp*p);
      R = getYield0()+_h*(1.-tmp);
      dR = _h*_hexp*tmp;
      ddR = -_hexp*(dR);
      intR=getYield0()*p+_h*p;
      if(fabs(_hexp)>1.e-16) intR+=_h*(exp(-_hexp*p)-1.)/(_hexp);
    }

  }
  ipv.set(R0,R,dR,ddR,intR,dRdT,ddRdTT,ddRdT);
}
//-----------------------------------------------added
void ExponentialJ2IsotropicHardening::hardening(double p0, const IPJ2IsotropicHardening &ipvprev, double p,double T, IPJ2IsotropicHardening &ipv) const
{
  double dR=0, ddR=0, intR=0, dRdT=0, ddRdTT=0, ddRdT=0.;
  double R0 = getYield0(T);
  double R = R0;
  if (ipvprev.isSaturated()){
    R = ipvprev.getR();
    dR = 0.;
    ddR = 0.;
    intR = ipvprev.getIntegR()+ R*(p-p0);
  }
  else{
    double tol=1.e-16;

    double hT=_h*_temFunc_h->getVal(T);
    double hexpT=_hexp*_temFunc_hexp->getVal(T);
    if(p< tol)
    {
      R=getYield0(T);
      dR = hT*hexpT;
      ddR = -hexpT*(dR);
    }
    else
    {
      double tmp = exp(-hexpT*p);
      R = getYield0(T)+hT*(1.-tmp);
      dR = hT*hexpT*tmp;
      ddR = -hexpT*(dR);
      intR=getYield0(T)*p+hT*p;
      if(fabs(hexpT)!=0) intR+=hT*(exp(-hexpT*p)-exp(0))/(hexpT);
    }

    double dhdT=_h*_temFunc_h->getDiff(T);
    double ddhdTT=_h*_temFunc_h->getDoubleDiff(T);
    double dhexpdT=_hexp*_temFunc_hexp->getDiff(T);
    double ddhexpdTT=_hexp*_temFunc_hexp->getDoubleDiff(T);
    if(p< tol)
    {
      dRdT=getDYield0DT(T);
      ddRdTT=getDDYield0DTT(T);
      ddRdT=0.;
    }
    else
    {
      dRdT=getDYield0DT(T)+dhdT*(1-exp(-hexpT*p))+hT*(dhexpdT*p*exp(-hexpT*p));
      ddRdTT=getDDYield0DTT(T)+ddhdTT*(1-exp(-hexpT*p))+dhdT*(dhexpdT*p*exp(-hexpT*p))+dhdT*(dhexpdT*p*exp(-hexpT*p))+hT*(ddhexpdTT*p*exp(-hexpT*p)-dhexpdT*dhexpdT*p*p*exp(-hexpT*p));
      ddRdT=dhdT*hexpT*exp(-hexpT*p) + hT*dhexpdT*(exp(-hexpT*p)-pow(hexpT,2)*exp(-hexpT*p));
    }
  }
  ipv.set(R0,R,dR,ddR,intR,dRdT,ddRdTT,ddRdT);
}


//----------------------------------------end
J2IsotropicHardening * ExponentialJ2IsotropicHardening::clone() const
{
  return new ExponentialJ2IsotropicHardening(*this);
}

//
//-----------------------------------------end
EnhancedExponentialJ2IsotropicHardening::EnhancedExponentialJ2IsotropicHardening(const int num, double yield0, double h, double hexp, double c, double n) :
					J2IsotropicHardening(num,yield0,true), _h(h), _hexp(hexp),_c(c),_n(n)
{
  if(h<0 or hexp<0) Msg::Error("EnhancedExponentialJ2IsotropicHardening: negative hardening parameters");
  _temFunc_h= new constantScalarFunction(1.);
  _temFunc_hexp= new constantScalarFunction(1.);
}
EnhancedExponentialJ2IsotropicHardening::EnhancedExponentialJ2IsotropicHardening(const EnhancedExponentialJ2IsotropicHardening &source) :
					J2IsotropicHardening(source)
{
  _h = source._h;
  _hexp = source._hexp;
  _c = source._c;
  _n = source._n;

  _temFunc_h = NULL;
  if (source._temFunc_h != NULL){
    _temFunc_h = source._temFunc_h->clone();
  }

  _temFunc_hexp = NULL;
  if (source._temFunc_hexp != NULL){
    _temFunc_hexp = source._temFunc_hexp->clone();
  }
}

EnhancedExponentialJ2IsotropicHardening& EnhancedExponentialJ2IsotropicHardening::operator=(const J2IsotropicHardening &source)
{
  J2IsotropicHardening::operator=(source);
  const EnhancedExponentialJ2IsotropicHardening* src = dynamic_cast<const EnhancedExponentialJ2IsotropicHardening*>(&source);
  if(src != NULL)
  {
    _h = src->_h;
    _hexp = src->_hexp;
    _c = src->_c;
    _n = src->_n;
    if (src->_temFunc_h != NULL){
      if (_temFunc_h != NULL){
        _temFunc_h->operator=(*src->_temFunc_h);
      }
      else{
        _temFunc_h = src->_temFunc_h->clone();
      }
    }

    if (src->_temFunc_hexp != NULL){
      if (_temFunc_hexp != NULL){
        _temFunc_hexp->operator=(*src->_temFunc_hexp);
      }
      else
        _temFunc_hexp = src->_temFunc_hexp->clone();
    }

  }
  return *this;
}

EnhancedExponentialJ2IsotropicHardening::~EnhancedExponentialJ2IsotropicHardening(){
  if (_temFunc_h != NULL){delete _temFunc_h; _temFunc_h = NULL;};
  if (_temFunc_hexp != NULL) {delete _temFunc_hexp; _temFunc_hexp= NULL;};
};

void EnhancedExponentialJ2IsotropicHardening::setTemperatureFunction_h(const scalarFunction& Tfunc){
  if (_temFunc_h != NULL) delete _temFunc_h;
  _temFunc_h = Tfunc.clone();
}
void EnhancedExponentialJ2IsotropicHardening::setTemperatureFunction_hexp(const scalarFunction& Tfunc){
  if (_temFunc_hexp != NULL) delete _temFunc_hexp;
  _temFunc_hexp = Tfunc.clone();
}


void EnhancedExponentialJ2IsotropicHardening::createIPVariable(IPJ2IsotropicHardening* &ipv) const
{
  if(ipv != NULL) delete ipv;
  ipv = new IPExponentialJ2IsotropicHardening(getYield0());
}

void EnhancedExponentialJ2IsotropicHardening::hardening(double p0, const IPJ2IsotropicHardening &ipvprev, double p, IPJ2IsotropicHardening &ipv) const
{
  double dR=0, ddR=0, intR=0, dRdT=0, ddRdTT=0, ddRdT=0;
  double R0 = getYield0();
  double R = R0;
  if (ipvprev.isSaturated()){
    R = ipvprev.getR();
    dR = 0.;
    ddR = 0.;
    intR = ipvprev.getIntegR()+ R*(p-p0);
  }
  else{
    double tmp = exp(-_hexp*p);
    double tmp2 = pow(1.+_c*p,_n);
    R = getYield0()+_h*(1.-tmp)*tmp2;
    double dtmp = -_hexp*tmp;
    double dtmp2 = (_c*_n)*pow(1.+_c*p,_n-1.);

    dR = -_h*dtmp*tmp2 + _h*(1.-tmp)*dtmp2;
    double ddtmp = -_hexp*dtmp;
    double ddtmp2 = (_c*_n)*(_c*(_n-1))*pow(1.+_c*p,_n-2.);

    ddR = -_h*ddtmp*tmp2 -_h*dtmp*dtmp2  - _h*dtmp*dtmp2+_h*(1.-tmp)*ddtmp2;

    intR= ipvprev.getIntegR()+ R*(p-p0);
  }
  ipv.set(R0,R,dR,ddR,intR,dRdT,ddRdTT,ddRdT);
}
//-----------------------------------------------added
void EnhancedExponentialJ2IsotropicHardening::hardening(double p0, const IPJ2IsotropicHardening &ipvprev, double p,double T, IPJ2IsotropicHardening &ipv) const
{
  Msg::Error("EnhancedExponentialJ2IsotropicHardening::hardening with temperature has not been implemented");
}


//----------------------------------------end
J2IsotropicHardening * EnhancedExponentialJ2IsotropicHardening::clone() const
{
  return new EnhancedExponentialJ2IsotropicHardening(*this);
}

//-
//-----------------------------------------end
ExponentialSeriesJ2IsotropicHardening::ExponentialSeriesJ2IsotropicHardening(const int num, const int n, double yield0) :
					J2IsotropicHardening(num,yield0,true), _h(n,0.),_hexp(n,0.),_temFunc_h(n,NULL),_temFunc_hexp(n,NULL)
{

}
ExponentialSeriesJ2IsotropicHardening::ExponentialSeriesJ2IsotropicHardening(const ExponentialSeriesJ2IsotropicHardening &source) :
					J2IsotropicHardening(source),_h(source._h),_hexp(source._hexp)
{
  int n = _h.size();
  _temFunc_h.resize(n,NULL);
  _temFunc_hexp.resize(n,NULL);
  for (int i=0; i< _h.size(); i++){
    _temFunc_h[i] = source._temFunc_h[i]->clone();
    _temFunc_hexp[i] = source._temFunc_hexp[i]->clone();
  }
}

ExponentialSeriesJ2IsotropicHardening& ExponentialSeriesJ2IsotropicHardening::operator=(const J2IsotropicHardening &source)
{
  J2IsotropicHardening::operator=(source);
  const ExponentialSeriesJ2IsotropicHardening* src = dynamic_cast<const ExponentialSeriesJ2IsotropicHardening*>(&source);
  if(src != NULL)
  {
    // clear old data
    for (int i=0; i< _h.size(); i++){
      if (_temFunc_h[i] != NULL) {delete _temFunc_h[i]; _temFunc_h[i] = NULL;}
      if (_temFunc_hexp[i] != NULL) {delete _temFunc_hexp[i]; _temFunc_hexp[i] = NULL;}
    }

    _h = src->_h;
    _hexp = src->_hexp;
    int n = _h.size();
    _temFunc_h.resize(n,NULL);
    _temFunc_hexp.resize(n,NULL);
    for (int i=0; i< _h.size(); i++){
      _temFunc_h[i] = src->_temFunc_h[i]->clone();
      _temFunc_hexp[i] = src->_temFunc_hexp[i]->clone();
    }
  }
  return *this;
}

ExponentialSeriesJ2IsotropicHardening::~ExponentialSeriesJ2IsotropicHardening(){
  // clear
  for (int i=0; i< _h.size(); i++){
    if (_temFunc_h[i] != NULL) {delete _temFunc_h[i]; _temFunc_h[i] = NULL;}
    if (_temFunc_hexp[i] != NULL) {delete _temFunc_hexp[i]; _temFunc_hexp[i] = NULL;}
  }
};


void ExponentialSeriesJ2IsotropicHardening::set(int i, double h, double hexp, const scalarFunction* func_h, const scalarFunction* func_hexp){
  if (i < _h.size()){
    _h[i] = h;
    _hexp[i] = hexp;
    if (func_h == NULL){
      _temFunc_h[i] = new constantScalarFunction(1.);
    }
    else{
      _temFunc_h[i] = func_h->clone();
    }
    if (func_hexp == NULL){
      _temFunc_hexp[i] = new constantScalarFunction(1.);
    }
    else{
      _temFunc_hexp[i] = func_hexp->clone();
    }
  }
  else{
    Msg::Error("error in setting ExponentialSeriesJ2IsotropicHardening::set");
  }
};


void ExponentialSeriesJ2IsotropicHardening::createIPVariable(IPJ2IsotropicHardening* &ipv) const
{
  if(ipv != NULL) delete ipv;
  ipv = new IPExponentialJ2IsotropicHardening(getYield0());
}

void ExponentialSeriesJ2IsotropicHardening::hardening(double p0, const IPJ2IsotropicHardening &ipvprev, double p, IPJ2IsotropicHardening &ipv) const
{
  double dR=0, ddR=0, intR=0, dRdT=0, ddRdTT=0,ddRdT=0;
  double R0 = getYield0();
  double R = R0;
  if (ipvprev.isSaturated()){
    R = ipvprev.getR();
    dR = 0.;
    ddR = 0.;
    intR = ipvprev.getIntegR()+ R*(p-p0);
  }
  else{
    intR =getYield0()*p;
    for (int i=0; i< _h.size(); i++){
      double tmp = exp(-_hexp[i]*p);
      R += _h[i]*(1.-tmp);
      dR += _h[i]*_hexp[i]*tmp;
      ddR -= _hexp[i]*_h[i]*_hexp[i]*tmp;
      intR += _h[i]*p;
      if(fabs(_hexp[i])!=0) intR+=_h[i]*(exp(-_hexp[i]*p)-exp(0))/(_hexp[i]);
    }
  }
  ipv.set(R0,R,dR,ddR,intR,dRdT,ddRdTT,ddRdT);
}
//-----------------------------------------------added
void ExponentialSeriesJ2IsotropicHardening::hardening(double p0, const IPJ2IsotropicHardening &ipvprev, double p,double T, IPJ2IsotropicHardening &ipv) const
{
  double dR=0, ddR=0, intR=0, dRdT=0, ddRdTT=0, ddRdT=0;
  double R0 = getYield0(T);
  double R = R0;
  if (ipvprev.isSaturated()){
    R = ipvprev.getR();
    intR = ipvprev.getIntegR()+ R*(p-p0);
  }
  else{
    intR =getYield0(T)*p;
    dRdT = getDYield0DT(T);
    int n = _h.size();
    for (int i=0; i< n; i++){
      double hT=_h[i]*_temFunc_h[i]->getVal(T);
      double hexpT=_hexp[i]*_temFunc_hexp[i]->getVal(T);

      double tmp = exp(-hexpT*p);
      R += hT*(1.-tmp);
      dR += hT*hexpT*tmp;
      ddR -= hT*hexpT*hexpT*tmp;
      intR += hT*p;
      if(fabs(hexpT)>0.) intR+=hT*(tmp-1)/(hexpT);

      double dhdT=hT*_temFunc_h[i]->getDiff(T);
      double dhexpdT=hexpT*_temFunc_hexp[i]->getDiff(T);
      dRdT += dhdT*(1-tmp)+hT*(dhexpdT*p*tmp);
    }
  }
  ipv.set(R0,R,dR,ddR,intR,dRdT,ddRdTT,ddRdT);
}


//----------------------------------------end
J2IsotropicHardening * ExponentialSeriesJ2IsotropicHardening::clone() const
{
  return new ExponentialSeriesJ2IsotropicHardening(*this);
}

//-

SwiftJ2IsotropicHardening::SwiftJ2IsotropicHardening(const int num, double yield0, double h, double hexp, double p0, const bool init) :
					J2IsotropicHardening(num,yield0,init), _h(h), _hexp(hexp),_p0(p0)
{
  if(_h<0 or _hexp<0) Msg::Error("SwiftJ2IsotropicHardening: negative hardening parameters");
  _temFunc_h= new constantScalarFunction(1.);
  _temFunc_hexp= new constantScalarFunction(1.);
}
SwiftJ2IsotropicHardening::SwiftJ2IsotropicHardening(const SwiftJ2IsotropicHardening &source) :
					J2IsotropicHardening(source)
{
  _h = source._h;
  _hexp = source._hexp;
  _p0 = source._p0;
  _temFunc_h = NULL;
  if (source._temFunc_h != NULL){
    _temFunc_h = source._temFunc_h->clone();
  }

  _temFunc_hexp = NULL;
  if (source._temFunc_hexp != NULL){
    _temFunc_hexp = source._temFunc_hexp->clone();
  }
}

SwiftJ2IsotropicHardening& SwiftJ2IsotropicHardening::operator=(const J2IsotropicHardening &source)
{
  J2IsotropicHardening::operator=(source);
  const SwiftJ2IsotropicHardening* src = dynamic_cast<const SwiftJ2IsotropicHardening*>(&source);
  if(src != NULL)
  {
    _h = src->_h;
    _hexp = src->_hexp;
    _p0 = src->_p0;
    if (src->_temFunc_h != NULL){
      if (_temFunc_h != NULL){
        _temFunc_h->operator=(*src->_temFunc_h);
      }
      else{
        _temFunc_h = src->_temFunc_h->clone();
      }
    }

    if (src->_temFunc_hexp != NULL){
      if (_temFunc_hexp != NULL){
        _temFunc_hexp->operator=(*src->_temFunc_hexp);
      }
      else
        _temFunc_hexp = src->_temFunc_hexp->clone();
    }
  }
  return *this;
}

SwiftJ2IsotropicHardening::~SwiftJ2IsotropicHardening(){
  if (_temFunc_h != NULL) {delete _temFunc_h; _temFunc_h = NULL;};
  if (_temFunc_hexp != NULL){delete _temFunc_hexp; _temFunc_hexp = NULL;};
};

void SwiftJ2IsotropicHardening::setTemperatureFunction_h(const scalarFunction& Tfunc){
  if (_temFunc_h != NULL) delete _temFunc_h;
  _temFunc_h = Tfunc.clone();
}
void SwiftJ2IsotropicHardening::setTemperatureFunction_hexp(const scalarFunction& Tfunc){
  if (_temFunc_hexp != NULL) delete _temFunc_hexp;
  _temFunc_hexp = Tfunc.clone();
}


void SwiftJ2IsotropicHardening::createIPVariable(IPJ2IsotropicHardening* &ipv) const
{
  if(ipv != NULL) delete ipv;
  ipv = new IPSwiftJ2IsotropicHardening(getYield0());
}

void SwiftJ2IsotropicHardening::hardening(double p0, const IPJ2IsotropicHardening &ipvprev, double p, IPJ2IsotropicHardening &ipv) const
{
  double dR=0, ddR=0, intR=0, dRdT=0, ddRdTT=0, ddRdT=0;
  double R0 = getYield0();
  double R = R0;
  if (ipvprev.isSaturated()){
    R = ipvprev.getR();
    dR = 0.;
    ddR = 0.;
    intR = ipvprev.getIntegR()+ R*(p-p0);
  }
  else{
    if(p< _p0)
    {
      R=getYield0();
      dR = 0.;
      ddR = 0.;
      intR = getYield0()*p;
    }
    else
    {
      double tmp = 1.+_h*(p-_p0);
      R = getYield0()*pow(tmp,_hexp);
      dR = R*_h*_hexp/tmp;
      ddR = (dR)*(_hexp-1.)*_h/tmp;
      if(fabs(_hexp+1.)!=0 && fabs(_h)!=0.) intR= getYield0()*_p0 + (R*tmp-getYield0())/(_hexp+1.)/_h;
    }
  }
  ipv.set(R0,R,dR,ddR,intR,dRdT,ddRdTT,ddRdT);
}
//----------------------added
void SwiftJ2IsotropicHardening::hardening(double p0, const IPJ2IsotropicHardening &ipvprev, double p,double T, IPJ2IsotropicHardening &ipv) const
{
  double dR=0, ddR=0, intR=0, dRdT=0, ddRdTT=0, ddRdT=0;
  double R0 = getYield0(T);
  double R = R0;
  if (ipvprev.isSaturated()){
    R = ipvprev.getR();
    dR = 0.;
    ddR = 0.;
    intR = ipvprev.getIntegR()+ R*(p-p0);
  }
  else{
    double hT = _h*_temFunc_h->getVal(T);
    double hexpT = _hexp*_temFunc_hexp->getVal(T);
    if(p< _p0)
    {
      R=getYield0(T);
      dR = 0.;
      ddR = 0.;
      intR = getYield0()*p;
    }
    else
    {
      double tmp = 1.+hT*(p-_p0);
      R = getYield0(T)*pow(tmp,hexpT);
      dR = R*hT*hexpT/tmp;
      ddR = (dR)*(hexpT-1.)*hT/tmp;
      if(fabs(hexpT+1.)!=0 && fabs(hT)!=0.) intR= getYield0(T)*_p0 + (R*tmp-getYield0(T))/(hexpT+1.)/hT;
    }

    double dhdT=_h*_temFunc_h->getDiff(T);
    double dhexpdT=_hexp*_temFunc_hexp->getDiff(T);
    if (p< _p0){
      dRdT=getDYield0DT(T);
    }
    else{
      double tmp = 1.+hT*(p-_p0);
      dRdT= R*getDYield0DT(T)/getYield0(T) + R*dhexpdT*log(tmp) + R*hexpT*dhdT*(p-_p0)/tmp;
    }
  }
  ipv.set(R0,R,dR,ddR,intR,dRdT,ddRdTT,ddRdT);
}

//------------------------------end
J2IsotropicHardening * SwiftJ2IsotropicHardening::clone() const
{
  return new SwiftJ2IsotropicHardening(*this);
}

// --------------------------
MultipleSwiftJ2IsotropicHardening::MultipleSwiftJ2IsotropicHardening(const int num,  double yield0, double h0, double n0, double p1, double h1, double n1) :
					J2IsotropicHardening(num,yield0,true), _h0(h0), _n0(n0),_p1(p1),_h1(h1),_n1(n1)
{

}
MultipleSwiftJ2IsotropicHardening::MultipleSwiftJ2IsotropicHardening(const MultipleSwiftJ2IsotropicHardening &source) :
					J2IsotropicHardening(source)
{
  _h0 = source._h0;
  _n0 = source._n0;
	_p1 = source._p1;
	_h1 = source._h1;
	_n1 = source._n1;
}

MultipleSwiftJ2IsotropicHardening& MultipleSwiftJ2IsotropicHardening::operator=(const J2IsotropicHardening &source)
{
  J2IsotropicHardening::operator=(source);
  const MultipleSwiftJ2IsotropicHardening* src = dynamic_cast<const MultipleSwiftJ2IsotropicHardening*>(&source);
  if(src != NULL)
  {
    _h0 = src->_h0;
		_n0 = src->_n0;
		_p1 = src->_p1;
		_h1 = src->_h1;
    _n1 = src->_n1;
  }
  return *this;
}

MultipleSwiftJ2IsotropicHardening::~MultipleSwiftJ2IsotropicHardening(){

};


void MultipleSwiftJ2IsotropicHardening::createIPVariable(IPJ2IsotropicHardening* &ipv) const
{
  if(ipv != NULL) delete ipv;
  ipv = new IPSwiftJ2IsotropicHardening(getYield0());
}

void MultipleSwiftJ2IsotropicHardening::hardening(double p0, const IPJ2IsotropicHardening &ipvprev, double p, IPJ2IsotropicHardening &ipv) const
{
  double dR=0., ddR=0., intR=0., dRdT=0., ddRdTT=0., ddRdT=0.;
  double R0 = getYield0();
  double R = R0;
  if (ipvprev.isSaturated()){
    R = ipvprev.getR();
    dR = 0.;
    ddR = 0.;
    intR = ipvprev.getIntegR()+ R*(p-p0);
  }
  else{
		double sigY0 = getYield0();
		if (p < _p1){
			double pp = 1.+ _h0*p;
			R = sigY0*pow(pp,_n0);
			dR = R*_h0*_n0/pp;
			intR = ipvprev.getIntegR()+ 0.5*(R+ipvprev.getR())*(p-p0); // approximation
		}
		else{
			double sigY1 = sigY0*pow(1.+ _h0*_p1,_n0);
			double pp = 1.+_h1*(p-_p1);
			R = sigY1*pow(pp,_n1);
			dR = R*_n1*_h1/pp;
			intR = ipvprev.getIntegR()+ 0.5*(R+ipvprev.getR())*(p-p0); // approximation
		}
  }
  ipv.set(R0,R,dR,ddR,intR,dRdT,ddRdTT,ddRdT);
}
//----------------------added
void MultipleSwiftJ2IsotropicHardening::hardening(double p0, const IPJ2IsotropicHardening &ipvprev, double p,double T, IPJ2IsotropicHardening &ipv) const
{
	// temperature dependence has not been implemented
  double dR=0., ddR=0., intR=0., dRdT=0., ddRdTT=0., ddRdT=0.;
  double R0 = getYield0(T);
  double R = R0;
  if (ipvprev.isSaturated()){
    R = ipvprev.getR();
    dR = 0.;
    ddR = 0.;
    intR = ipvprev.getIntegR()+ R*(p-p0);
  }
  else{
    double sigY0 = getYield0(T);
		if (p < _p1){
			double pp = 1.+ _h0*p;
			R = sigY0*pow(pp,_n0);
			dR = R*_h0*_n0/pp;
			ddR = dR* _h0*_n0/pp - R*_h0*_n0/(pp*pp)*_h0;
			intR = ipvprev.getIntegR()+ 0.5*(R+ipvprev.getR())*(p-p0); // approximation
		}
		else{
			double sigY1 = sigY0*pow(1.+ _h0*_p1,_n0);
			double pp = 1.+_h1*(p-_p1);
			R = sigY1*pow(pp,_n1);
			dR = R*_n1*_h1/pp;
			ddR = dR*_n1*_h1/pp - R*_n1*_h1/(pp*pp)*_h1;
			intR = ipvprev.getIntegR()+ 0.5*(R+ipvprev.getR())*(p-p0); // approximation
		}

    if (p < _p1){
      double pp = 1.+ _h0*p;
      dRdT = getDYield0DT(T)*pow(pp,_n0);
    }
    else{
      double DsigY1DT = getDYield0DT(T)*pow(1.+ _h0*_p1,_n0);
      double pp = 1.+_h1*(p-_p1);
      dRdT = DsigY1DT*pow(pp,_n1);
    }
  }
  ipv.set(R0,R,dR,ddR,intR,dRdT,ddRdTT,ddRdT);
}

//------------------------------end
J2IsotropicHardening * MultipleSwiftJ2IsotropicHardening::clone() const
{
  return new MultipleSwiftJ2IsotropicHardening(*this);
}


//--------------------------------------------------------------------------------------------------------end
LinearExponentialJ2IsotropicHardening::LinearExponentialJ2IsotropicHardening(const int num, double yield0, double h1,
					double h2, double hexp, const bool init) :
					J2IsotropicHardening(num,yield0,init), _h1(h1), _h2(h2), _hexp(hexp)
{
  if(h1<0 or h2<0 or hexp<0) Msg::Error("SwiftJ2IsotropicHardening: negative hardening parameters");
  _temFunc_h1= new constantScalarFunction(1.);
  _temFunc_h2= new constantScalarFunction(1.);
  _temFunc_hexp= new constantScalarFunction(1.);
}
LinearExponentialJ2IsotropicHardening::LinearExponentialJ2IsotropicHardening(const LinearExponentialJ2IsotropicHardening &source) :
					J2IsotropicHardening(source)
{
  _h1 = source._h1;
  _h2 = source._h2;
  _hexp = source._hexp;

  _temFunc_hexp = NULL;
  if (source._temFunc_hexp != NULL){
    _temFunc_hexp = source._temFunc_hexp->clone();
  }
  _temFunc_h1 = NULL;
  if (source._temFunc_h1 != NULL){
    _temFunc_h1 = source._temFunc_h1->clone();
  }
  _temFunc_h2 = NULL;
  if (source._temFunc_h2 != NULL){
    _temFunc_h2 = source._temFunc_h2->clone();
  }
}

LinearExponentialJ2IsotropicHardening& LinearExponentialJ2IsotropicHardening::operator=(const J2IsotropicHardening &source)
{
  J2IsotropicHardening::operator=(source);
  const LinearExponentialJ2IsotropicHardening* src = dynamic_cast<const LinearExponentialJ2IsotropicHardening*>(&source);
  if(src != NULL)
  {
    _h1 = src->_h1;
    _h2 = src->_h2;
    _hexp = src->_hexp;

    if (src->_temFunc_h1 != NULL){
      if (_temFunc_h1 != NULL){
        _temFunc_h1->operator=(*src->_temFunc_h1);
      }
      else{
        _temFunc_h1 = src->_temFunc_h1->clone();
      }
    }

    if (src->_temFunc_h2 != NULL){
      if (_temFunc_h2 != NULL){
        _temFunc_h2->operator=(*src->_temFunc_h2);
      }
      else{
        _temFunc_h2 = src->_temFunc_h2->clone();
      }
    }

    if (src->_temFunc_hexp != NULL){
      if (_temFunc_hexp != NULL){
        _temFunc_hexp->operator=(*src->_temFunc_hexp);
      }
      else
        _temFunc_hexp = src->_temFunc_hexp->clone();
    }
  }
  return *this;
}

LinearExponentialJ2IsotropicHardening::~LinearExponentialJ2IsotropicHardening(){
  if (_temFunc_h1 != NULL) {delete _temFunc_h1; _temFunc_h1 = NULL;};
  if (_temFunc_h2 != NULL) {delete _temFunc_h2; _temFunc_h2 = NULL;};
if (_temFunc_hexp != NULL) {delete _temFunc_hexp; _temFunc_hexp = NULL;};
};

void LinearExponentialJ2IsotropicHardening::setTemperatureFunction_h1(const scalarFunction& Tfunc){
  if (_temFunc_h1 != NULL) delete _temFunc_h1;
  _temFunc_h1 = Tfunc.clone();
}
void LinearExponentialJ2IsotropicHardening::setTemperatureFunction_h2(const scalarFunction& Tfunc){
  if (_temFunc_h2 != NULL) delete _temFunc_h2;
  _temFunc_h2 = Tfunc.clone();
}

void LinearExponentialJ2IsotropicHardening::setTemperatureFunction_hexp(const scalarFunction& Tfunc){
  if (_temFunc_hexp != NULL) delete _temFunc_hexp;
  _temFunc_hexp = Tfunc.clone();
}

void LinearExponentialJ2IsotropicHardening::createIPVariable(IPJ2IsotropicHardening* &ipv) const
{
  if(ipv != NULL) delete ipv;
  ipv = new IPLinearExponentialJ2IsotropicHardening(getYield0());
}

void LinearExponentialJ2IsotropicHardening::hardening(double p0, const IPJ2IsotropicHardening &ipvprev, double p, IPJ2IsotropicHardening &ipv) const
{

  double dR=0, ddR=0, intR=0, dRdT=0., ddRdTT=0., ddRdT=0.;
  double R0 = getYield0();
  double R = R0;
  if (ipvprev.isSaturated()){
    R = ipvprev.getR();
    dR = 0.;
    ddR = 0.;
    intR = ipvprev.getIntegR()+ R*(p-p0);
  }
  else{
    double tol=1.e-16;

    if(p< tol)
    {
      R=getYield0();
      dR = _h1 + _h2*_hexp;
      ddR = -_h2*_hexp*_hexp;
    }
    else
    {
      double tmp = exp(-_hexp*p);
      R = getYield0()+_h1*p + _h2*(1.-tmp);
      dR = _h1 + _h2*_hexp*tmp;
      ddR = - _h2*_hexp*_hexp*tmp;
      intR=getYield0()*p+ _h1*p*p/2.+_h2*p;
      if(fabs(_hexp)!=0) intR+=_h2*(exp(-_hexp*p)-exp(0))/(_hexp);
    }
  }
  ipv.set(R0,R,dR,ddR,intR,dRdT,ddRdTT,ddRdT);
}
//-----------------------------added
void LinearExponentialJ2IsotropicHardening::hardening(double p0, const IPJ2IsotropicHardening &ipvprev, double p,double T, IPJ2IsotropicHardening &ipv) const
{

  double dR=0, ddR=0, intR=0, dRdT=0., ddRdTT=0., ddRdT=0.;
  double R0 = getYield0(T);
  double R = R0;
  if (ipvprev.isSaturated()){
    R = ipvprev.getR();
    dR = 0.;
    ddR = 0.;
    intR = ipvprev.getIntegR()+ R*(p-p0);
    dRdT = 0.;
    ddRdTT = 0.;
  }
  else {
    double tol=1.e-16;

    double h1T=_h1*_temFunc_h1->getVal(T);
    double h2T=_h2*_temFunc_h2->getVal(T);
    double hexpT=_hexp*_temFunc_hexp->getVal(T);
    if(p< tol)
    {
      R=getYield0(T);
      dR = h1T + h2T*hexpT;
      ddR = -h2T*hexpT*hexpT;
    }
    else
    {
      double tmp = exp(-hexpT*p);
      R = getYield0(T)+h1T*p + h2T*(1.-tmp);
      dR = h1T + h2T*hexpT*tmp;
      ddR = - h2T*hexpT*hexpT*tmp;
      intR=getYield0(T)*p+ h1T*p*p/2.+h2T*p;
      if(fabs(hexpT)!=0) intR+=h2T*(exp(-hexpT*p)-exp(0))/(hexpT);
    }


    if (p< tol){
      dRdT = getDYield0DT(T);
      ddRdTT = getDDYield0DTT(T);
    }
    else{
      double tmp = exp(-hexpT*p);
      double dh1dT=_h1*_temFunc_h1->getDiff(T);
      double ddh1dTT=_h1*_temFunc_h1->getDoubleDiff(T);
      double dhexpdT=_hexp*_temFunc_hexp->getDiff(T);
      double ddhexpdTT=_hexp*_temFunc_hexp->getDoubleDiff(T);
      double dh2dT=_h2*_temFunc_h2->getDiff(T);
      double ddh2dTT=_h2*_temFunc_h2->getDoubleDiff(T);
      dRdT=getDYield0DT(T)+dh1dT*p+dh2dT*(1-tmp)+h2T*(dhexpdT*p*tmp);
      ddRdTT=getDDYield0DTT(T)+ddh1dTT*p+ddh2dTT*(1-tmp)+dh2dT*(dhexpdT*p*tmp)
            +dh2dT*(dhexpdT*p*tmp)+h2T*(ddhexpdTT*p*tmp-dhexpdT*p*dhexpdT*p*tmp);
    }

  }
  ipv.set(R0,R,dR,ddR,intR,dRdT,ddRdTT,ddRdT);
}

//----------------------------end
J2IsotropicHardening * LinearExponentialJ2IsotropicHardening::clone() const
{
  return new LinearExponentialJ2IsotropicHardening(*this);
}

//
//--------------------------------------------------------------------------------------------------------end
LinearExponentialFollowedBySwiftLawJ2IsotropicHardening::LinearExponentialFollowedBySwiftLawJ2IsotropicHardening(const int num, double yield0, double h1,
					double h2, double hexp, double h3, double p3, double n3) :
					LinearExponentialJ2IsotropicHardening(num,yield0,h1,h2,hexp,true), _h3(h3), _p3(p3), _n3(n3)
{
}
LinearExponentialFollowedBySwiftLawJ2IsotropicHardening::LinearExponentialFollowedBySwiftLawJ2IsotropicHardening(const LinearExponentialFollowedBySwiftLawJ2IsotropicHardening &source) :
					LinearExponentialJ2IsotropicHardening(source)
{
	_h3 = source._h3;
  _p3 = source._p3;
  _n3 = source._n3;
}

LinearExponentialFollowedBySwiftLawJ2IsotropicHardening& LinearExponentialFollowedBySwiftLawJ2IsotropicHardening::operator=(const J2IsotropicHardening &source)
{
  LinearExponentialJ2IsotropicHardening::operator=(source);
  const LinearExponentialFollowedBySwiftLawJ2IsotropicHardening* src = dynamic_cast<const LinearExponentialFollowedBySwiftLawJ2IsotropicHardening*>(&source);
  if(src != NULL)
  {
		_h3 = src->_h3;
    _p3 = src->_p3;
    _n3 = src->_n3;
  }
  return *this;
}

LinearExponentialFollowedBySwiftLawJ2IsotropicHardening::~LinearExponentialFollowedBySwiftLawJ2IsotropicHardening(){
};


void LinearExponentialFollowedBySwiftLawJ2IsotropicHardening::createIPVariable(IPJ2IsotropicHardening* &ipv) const
{
  if(ipv != NULL) delete ipv;
  ipv = new IPLinearExponentialJ2IsotropicHardening(getYield0());
}

void LinearExponentialFollowedBySwiftLawJ2IsotropicHardening::hardening(double p0, const IPJ2IsotropicHardening &ipvprev, double p, IPJ2IsotropicHardening &ipv) const
{
  if (p< _p3){
    LinearExponentialJ2IsotropicHardening::hardening(p0,ipvprev,p,ipv);
  }
  else{
    double dR=0, ddR=0, intR=0, dRdT=0., ddRdTT=0., ddRdT=0.;
    double R0 = getYield0();
    double R = R0;
    if (ipvprev.isSaturated()){
      R = ipvprev.getR();
      dR = 0.;
      ddR = 0.;
      intR = ipvprev.getIntegR()+ R*(p-p0);
    }
    else{
      double tmp = exp(-_hexp*_p3);
      double sy1 = getYield0()+_h1*_p3 + _h2*(1.-tmp);

      R = sy1*pow(1+_h3*(p-_p3),_n3);
      dR = R*_n3*_h3/(1+_h3*(p-_p3));
      ddR = dR*_n3*_h3/(1+_h3*(p-_p3)) - R*_n3*_h3*_h3/((1+_h3*(p-_p3))*(1+_h3*(p-_p3)));
      intR = ipvprev.getIntegR()+ 0.5*(R+ipvprev.getR())*(p-p0); // approximation
    }
    ipv.set(R0,R,dR,ddR,intR,dRdT,ddRdTT,ddRdT);
  }
}
//-----------------------------added
void LinearExponentialFollowedBySwiftLawJ2IsotropicHardening::hardening(double p0, const IPJ2IsotropicHardening &ipvprev, double p,double T, IPJ2IsotropicHardening &ipv) const
{
  if (p < _p3){
    LinearExponentialJ2IsotropicHardening::hardening(p0,ipvprev,p,T,ipv);
  }
  else{
    double dR=0, ddR=0, intR=0, dRdT=0., ddRdTT=0.,ddRdT=0.;
    double R0 = getYield0(T);
    double R = R0;
    if (ipvprev.isSaturated()){
      R = ipvprev.getR();
      dR = 0.;
      ddR = 0.;
      intR = ipvprev.getIntegR()+ R*(p-p0);
    }
    else {
      double h1T=_h1*_temFunc_h1->getVal(T);
      double h2T=_h2*_temFunc_h2->getVal(T);
      double hexpT=_hexp*_temFunc_hexp->getVal(T);

      double tmp = exp(-hexpT*_p3);
      double sy1 = getYield0(T)+h1T*_p3 + h2T*(1.-tmp);

      R = sy1*pow(1+_h3*(p-_p3),_n3);
      dR = R*_n3*_h3/(1+_h3*(p-_p3));
      ddR = dR*_n3*_h3/(1+_h3*(p-_p3)) - R*_n3*_h3*_h3/((1+_h3*(p-_p3))*(1+_h3*(p-_p3)));
      intR = ipvprev.getIntegR()+ 0.5*(R+ipvprev.getR())*(p-p0); // approximation

      dRdT = getYield0(T)*pow(1+_h3*(p-_p3),_n3);

    }
    ipv.set(R0,R,dR,ddR,intR,dRdT,ddRdTT,ddRdT);
  }
}

//----------------------------end
J2IsotropicHardening * LinearExponentialFollowedBySwiftLawJ2IsotropicHardening::clone() const
{
  return new LinearExponentialFollowedBySwiftLawJ2IsotropicHardening(*this);
}




/* LinearFollowedByExponentialJ2IsotropicHardening */
LinearFollowedByExponentialJ2IsotropicHardening::LinearFollowedByExponentialJ2IsotropicHardening(const int num,  double yield0,
    double h1, double pexp, double h2, double hexp2) :
    J2IsotropicHardening(num,yield0,true), _h1(h1), _pexp(pexp), _h2(h2), _hexp2(hexp2),
    _p3(1e10),_n3(1)
{
  if(h1 < 0. or h2 < 0. or hexp2 <= 0. or pexp < 0.) Msg::Error("LinearFollowedByExponentialJ2IsotropicHardening :: negative hardening parameters");
  _temFunc_h1= new constantScalarFunction(1.);
  _temFunc_h2= new constantScalarFunction(1.);
  _temFunc_pexp = new constantScalarFunction(1.);
  _temFunc_hexp2 = new constantScalarFunction(1.);
}

LinearFollowedByExponentialJ2IsotropicHardening::LinearFollowedByExponentialJ2IsotropicHardening(const int num,  double yield0, double h1, double pexp, double h2, double hexp2, double p3, double n3):
J2IsotropicHardening(num,yield0,true), _h1(h1), _pexp(pexp), _h2(h2), _hexp2(hexp2),_p3(p3),_n3(n3){
   if(h1 < 0. or h2 < 0. or hexp2 <= 0. or pexp < 0.) Msg::Error("LinearFollowedByExponentialJ2IsotropicHardening :: negative hardening parameters");
  _temFunc_h1= new constantScalarFunction(1.);
  _temFunc_h2= new constantScalarFunction(1.);
  _temFunc_pexp = new constantScalarFunction(1.);
  _temFunc_hexp2 = new constantScalarFunction(1.);
};

LinearFollowedByExponentialJ2IsotropicHardening::LinearFollowedByExponentialJ2IsotropicHardening(const LinearFollowedByExponentialJ2IsotropicHardening &source) :
    J2IsotropicHardening(source)
{
  _h1 = source._h1;
  _pexp = source._pexp;
  _h2 = source._h2;
  _hexp2 = source._hexp2;
  _p3 = source._p3;
  _n3 = source._n3;

  _temFunc_h1 = NULL;
  if (source._temFunc_h1 != NULL){
    _temFunc_h1 = source._temFunc_h1->clone();
  }
  _temFunc_h2 = NULL;
  if (source._temFunc_h2 != NULL){
    _temFunc_h2 = source._temFunc_h2->clone();
  }
  _temFunc_pexp = NULL;
  if (source._temFunc_pexp != NULL){
    _temFunc_pexp = source._temFunc_pexp->clone();
  }
  _temFunc_hexp2 = NULL;
  if (source._temFunc_hexp2 != NULL){
    _temFunc_hexp2 = source._temFunc_hexp2->clone();
  }
}

LinearFollowedByExponentialJ2IsotropicHardening& LinearFollowedByExponentialJ2IsotropicHardening::operator=(const J2IsotropicHardening &source)
{
  J2IsotropicHardening::operator=(source);
  const LinearFollowedByExponentialJ2IsotropicHardening* src = dynamic_cast<const LinearFollowedByExponentialJ2IsotropicHardening*>(&source);
  if(src != NULL)
  {
    _h1 = src->_h1;
    _pexp = src->_pexp;
    _h2 = src->_h2;
    _hexp2 = src->_hexp2;
    _p3 = src->_p3;
    _n3 = src->_n3;

     if (src->_temFunc_h1 != NULL){
      if (_temFunc_h1 != NULL){
        _temFunc_h1->operator=(*src->_temFunc_h1);
      }
      else{
        _temFunc_h1 = src->_temFunc_h1->clone();
      }
    }

    if (src->_temFunc_h2 != NULL){
      if (_temFunc_h2 != NULL){
        _temFunc_h2->operator=(*src->_temFunc_h2);
      }
      else{
        _temFunc_h2 = src->_temFunc_h2->clone();
      }
    }

    if (src->_temFunc_pexp != NULL){
      if (_temFunc_pexp != NULL){
        _temFunc_pexp->operator=(*src->_temFunc_pexp);
      }
      else{
        _temFunc_pexp = src->_temFunc_pexp->clone();
      }
    }

    if (src->_temFunc_hexp2 != NULL){
      if (_temFunc_hexp2 != NULL){
        _temFunc_hexp2->operator=(*src->_temFunc_hexp2);
      }
      else{
        _temFunc_hexp2 = src->_temFunc_hexp2->clone();
      }
    }
  }
  return *this;
}

LinearFollowedByExponentialJ2IsotropicHardening::~LinearFollowedByExponentialJ2IsotropicHardening(){
  if (_temFunc_h1 != NULL) {delete _temFunc_h1; _temFunc_h1 = NULL;};
  if (_temFunc_h2 != NULL) {delete _temFunc_h2; _temFunc_h2 = NULL;};
  if (_temFunc_pexp != NULL){delete _temFunc_pexp; _temFunc_pexp = NULL;};
  if (_temFunc_hexp2 != NULL) {delete _temFunc_hexp2; _temFunc_hexp2 = NULL;}
};


void LinearFollowedByExponentialJ2IsotropicHardening::setTemperatureFunction_h1(const scalarFunction& Tfunc){
  if (_temFunc_h1 != NULL) delete _temFunc_h1;
  _temFunc_h1 = Tfunc.clone();
}
void LinearFollowedByExponentialJ2IsotropicHardening::setTemperatureFunction_h2(const scalarFunction& Tfunc){
  if (_temFunc_h2 != NULL) delete _temFunc_h2;
  _temFunc_h2 = Tfunc.clone();
}

void LinearFollowedByExponentialJ2IsotropicHardening::setTemperatureFunction_pexp(const scalarFunction& Tfunc){
  if (_temFunc_pexp != NULL) delete _temFunc_pexp;
  _temFunc_pexp = Tfunc.clone();
}

void LinearFollowedByExponentialJ2IsotropicHardening::setTemperatureFunction_hexp2(const scalarFunction& Tfunc){
  if (_temFunc_hexp2 != NULL) delete _temFunc_hexp2;
  _temFunc_hexp2 = Tfunc.clone();
}

void LinearFollowedByExponentialJ2IsotropicHardening::createIPVariable(IPJ2IsotropicHardening* &ipv) const
{
  if(ipv != NULL) delete ipv;
  ipv = new IPLinearFollowedByExponentialJ2IsotropicHardening(getYield0());
}

void LinearFollowedByExponentialJ2IsotropicHardening::hardening(double p0, const IPJ2IsotropicHardening &ipvprev, double p, IPJ2IsotropicHardening &ipv) const
{
  double dR(0.), ddR(0.), intR(0.), dRdT(0.), ddRdTT(0.), ddRdT(0.);
  double R0 = getYield0();
  double R = R0;
  if (ipvprev.isSaturated()){
    R = ipvprev.getR();
    dR = 0.;
    ddR = 0.;
    intR = ipvprev.getIntegR()+ R*(p-p0);
  }
  else {

    if(p < 1.e-16) // Elastic case
    {
      R = getYield0();
      dR = _h1;
      ddR = 0.;
    }
    else if (p <= _pexp) // Plastic case: linear part
    {
      R += _h1*p;
      dR = _h1;
      ddR = 0.;
      intR = getYield0()*p + 0.5*_h1*p*p;
    }
    else if (p > _pexp and p<=_p3)
    {
      double tmp = exp(-(p-_pexp)/_hexp2);
      R += _h1*p + _h2*(1.-tmp);
      dR = _h1 + _h2 * tmp /_hexp2;
      ddR = -_h2 * tmp /_hexp2 /_hexp2;

      intR = getYield0()*p;
      intR += 0.5*_h1*p*p;
      intR += _h2*(p-_pexp) + _h2*(tmp-exp(0.)) *_hexp2;
    }
    else{
      double sy3 = getYield0() + _h1*_p3 + _h2*(1.-exp(-(_p3-_pexp)/_hexp2));
      R = sy3*pow(p/_p3,_n3);
      dR = sy3*pow(p/_p3,_n3-1.)*_n3/_p3;
      ddR = sy3*pow(p/_p3,_n3-2.)*_n3*(_n3-1.)/_p3/_p3;
      intR = ipvprev.getIntegR()+ R*(p-p0);
    }
  }
  ipv.set(R0,R,dR,ddR,intR,dRdT,ddRdTT,ddRdT);
}
//-----------------------------added
void LinearFollowedByExponentialJ2IsotropicHardening::hardening(double p0, const IPJ2IsotropicHardening &ipvprev, double p,double T, IPJ2IsotropicHardening &ipv) const
{
  double dR(0.), ddR(0.), intR(0.),dRdT(0.),ddRdTT(0.),ddRdT(0.);
  double R0 = getYield0(T);
  double R = R0;
  if (ipvprev.isSaturated()){
    R = ipvprev.getR();
    dR = 0.;
    ddR = 0.;
    intR = ipvprev.getIntegR()+ R*(p-p0);
  }
  else {

    double h1T = _h1*_temFunc_h1->getVal(T);
    double pexpT = _pexp*_temFunc_pexp->getVal(T);
    double h2T = _h2*_temFunc_h2->getVal(T);
    double hexp2T = _hexp2*_temFunc_hexp2->getVal(T);
    if(p < 1.e-16) // Elastic case
    {
      R = getYield0(T);
      dR = h1T;
      ddR = 0.;
    }
    else if (p <= pexpT) // Plastic case: linear part
    {
      R += h1T*p;
      dR = h1T;
      ddR = 0.;
      intR = getYield0(T)*p + 0.5*h1T*p*p;
    }
    else if (p > _pexp and p<=_p3)
    {
      double tmp = exp(-(p-pexpT)/hexp2T);
      R += h1T*p + h2T*(1.-tmp);
      dR = h1T + h2T * tmp /hexp2T;
      ddR = -h2T * tmp /hexp2T /hexp2T;

      intR = getYield0(T)*p;
      intR += 0.5*h1T*p*p;
      intR += h2T*(p-pexpT) + h2T*(tmp-exp(0.)) *hexp2T;
    }
    else{
      double sy3 = getYield0(T) + h1T*_p3 + h2T*(1.-exp(-(_p3-pexpT)/hexp2T));
      R = sy3*pow(p/_p3,_n3);
      dR = sy3*pow(p/_p3,_n3-1.)*_n3/_p3;
      ddR = sy3*pow(p/_p3,_n3-2.)*_n3*(_n3-1.)/_p3/_p3;
      intR = ipvprev.getIntegR()+ R*(p-p0);
    }

    double dh1dT=_h1*_temFunc_h1->getDiff(T);
    double ddh1dTT=_h1*_temFunc_h1->getDoubleDiff(T);
    double dhexp2dT=_hexp2*_temFunc_hexp2->getDiff(T);
    double ddhexp2dTT=_hexp2*_temFunc_hexp2->getDoubleDiff(T);
    double dh2dT=_h2*_temFunc_h2->getDiff(T);
    double ddh2dTT=_h2*_temFunc_h2->getDoubleDiff(T);
    double dpexpdT=_pexp*_temFunc_pexp->getDiff(T);
    double ddpexpdTT=_pexp*_temFunc_pexp->getDoubleDiff(T);
    if (p < 1.e-16){
      dRdT = getDYield0DT(T);
      ddRdTT = getDDYield0DTT(T);
    }
    else if (p<=pexpT)
    {
       dRdT=getDYield0DT(T)+dh1dT*p;
       ddRdTT=getDDYield0DTT(T)+ddh1dTT*p;
    }
    else if (p > _pexp and p<=_p3)
    {
       dRdT=getDYield0DT(T)+dh1dT*p+dh2dT*(1-exp(-hexp2T*(p-pexpT)))+h2T*(dhexp2dT*(p-pexpT)-hexp2T*dpexpdT)*exp(-hexp2T*p);
       dRdT=getDDYield0DTT(T)+ddh1dTT*p+ddh2dTT*(1-exp(-hexp2T*(p-pexpT)))+dh2dT*(dhexp2dT*(p-pexpT)-hexp2T*dpexpdT)*exp(-hexp2T*p)
            +dh2dT*(dhexp2dT*(p-pexpT)-hexp2T*dpexpdT)*exp(-hexp2T*p)
            +h2T*((ddhexp2dTT*(p-pexpT)-dhexp2dT*dpexpdT-dhexp2dT*dpexpdT-hexp2T*ddpexpdTT)*exp(-hexp2T*p)-(dhexp2dT*(p-pexpT)-hexp2T*dpexpdT)*dhexp2dT*p*exp(-hexp2T*p));
    }
    else{
      Msg::Error("DRDT for p > %e has not been implemented",_p3);
    }

  }
  ipv.set(R0,R,dR,ddR,intR,dRdT,ddRdTT,ddRdT);
}

//----------------------------end
J2IsotropicHardening* LinearFollowedByExponentialJ2IsotropicHardening::clone() const
{
  return new LinearFollowedByExponentialJ2IsotropicHardening(*this);
}


/* LinearFollowedByPowerLawJ2IsotropicHardening */
LinearFollowedByPowerLawJ2IsotropicHardening::LinearFollowedByPowerLawJ2IsotropicHardening(const int num,  double yield0, double h, double pexp, double hexp, bool init):
        J2IsotropicHardening(num,yield0,init), _h1(h), _pexp(pexp), _hexp(hexp)
{
  if(_h1 < 0. or _hexp < 0.) Msg::Error("LinearFollowedByPowerLawJ2IsotropicHardening:: negative hardening parameters");
  if(_pexp <= 1.e-9 ) Msg::Error("LinearFollowedByPowerLawJ2IsotropicHardening:: negative or too small hardening parameters p_exp, use PowerLawJ2IsotropicHardening instead");
  _temFunc_h1= new constantScalarFunction(1.);
  _temFunc_pexp = new constantScalarFunction(1.);
  _temFunc_hexp = new constantScalarFunction(1.);

}

LinearFollowedByPowerLawJ2IsotropicHardening::LinearFollowedByPowerLawJ2IsotropicHardening(const LinearFollowedByPowerLawJ2IsotropicHardening &source) :
					J2IsotropicHardening(source)
{
  _h1 = source._h1;
  _pexp = source._pexp;
  _hexp = source._hexp;

  if (source._temFunc_h1 != NULL){
    _temFunc_h1 = source._temFunc_h1->clone();
  }

  _temFunc_pexp = NULL;
  if (source._temFunc_pexp != NULL){
    _temFunc_pexp = source._temFunc_pexp->clone();
  }
  _temFunc_hexp = NULL;
  if (source._temFunc_hexp != NULL){
    _temFunc_hexp = source._temFunc_hexp->clone();
  }
}

LinearFollowedByPowerLawJ2IsotropicHardening& LinearFollowedByPowerLawJ2IsotropicHardening::operator=(const J2IsotropicHardening &source)
{
  J2IsotropicHardening::operator=(source);
  const LinearFollowedByPowerLawJ2IsotropicHardening* src = dynamic_cast<const LinearFollowedByPowerLawJ2IsotropicHardening*>(&source);
  if(src != NULL)
  {
    _h1 = src->_h1;
    _pexp = src->_pexp;
    _hexp = src->_hexp;
    if (src->_temFunc_h1 != NULL){
      if (_temFunc_h1 != NULL){
        _temFunc_h1->operator=(*src->_temFunc_h1);
      }
      else{
        _temFunc_h1 = src->_temFunc_h1->clone();
      }
    }

    if (src->_temFunc_pexp != NULL){
      if (_temFunc_pexp != NULL){
        _temFunc_pexp->operator=(*src->_temFunc_pexp);
      }
      else{
        _temFunc_pexp = src->_temFunc_pexp->clone();
      }
    }

    if (src->_temFunc_hexp != NULL){
      if (_temFunc_hexp != NULL){
        _temFunc_hexp->operator=(*src->_temFunc_hexp);
      }
      else{
        _temFunc_hexp = src->_temFunc_hexp->clone();
      }
    }

  }
  return *this;
}

LinearFollowedByPowerLawJ2IsotropicHardening::~LinearFollowedByPowerLawJ2IsotropicHardening(){
  if (_temFunc_h1 != NULL) {delete _temFunc_h1; _temFunc_h1 = NULL;}
  if (_temFunc_pexp != NULL){delete _temFunc_pexp; _temFunc_pexp = NULL;};
  if (_temFunc_hexp != NULL) {delete _temFunc_hexp; _temFunc_hexp = NULL;}
};
void LinearFollowedByPowerLawJ2IsotropicHardening::setTemperatureFunction_h1(const scalarFunction& Tfunc){
  if (_temFunc_h1 != NULL) delete _temFunc_h1;
  _temFunc_h1 = Tfunc.clone();
}

void LinearFollowedByPowerLawJ2IsotropicHardening::setTemperatureFunction_pexp(const scalarFunction& Tfunc){
  if (_temFunc_pexp != NULL) delete _temFunc_pexp;
  _temFunc_pexp = Tfunc.clone();
}

void LinearFollowedByPowerLawJ2IsotropicHardening::setTemperatureFunction_hexp(const scalarFunction& Tfunc){
  if (_temFunc_hexp != NULL) delete _temFunc_hexp;
  _temFunc_hexp = Tfunc.clone();
}


void LinearFollowedByPowerLawJ2IsotropicHardening::createIPVariable(IPJ2IsotropicHardening* &ipv) const
{
  if(ipv != NULL) delete ipv;
  ipv = new IPLinearFollowedByPowerLawJ2IsotropicHardening(getYield0());
}

void LinearFollowedByPowerLawJ2IsotropicHardening::hardening(double p0, const IPJ2IsotropicHardening &ipvprev, double p, IPJ2IsotropicHardening &ipv) const
{
  double dR=0., ddR=0., intR=0., dRdT=0., ddRdTT=0., ddRdT=0.;
  double R0 = getYield0();
  double R = R0;
  if (ipvprev.isSaturated()){
    R = ipvprev.getR();
    dR = 0.;
    ddR = 0.;
    intR = ipvprev.getIntegR()+ R*(p-p0);
  }
  else {
    double tol=1.e-16;
    // linear part
    if (p <= _pexp)
    {
      R = getYield0() + _h1*p;
      dR = _h1;
      ddR = 0.;
      intR = getYield0()*p + 0.5*_h1*p*p;
    }
    // power part
    else
    {
      R = (getYield0()+_h1*_pexp) * pow(p/_pexp,_hexp);
      dR = R*_hexp/p;
      ddR = dR*(_hexp-1.)/p;

      intR = getYield0()*_pexp + 0.5*_h1*_pexp*_pexp;
      intR += R*p /(_hexp+1.) -((getYield0()+_h1*_pexp)*_pexp/(_hexp+1.));
    }
  }
  ipv.set(R0,R,dR,ddR,intR,dRdT,ddRdTT,ddRdT);
}
//-----------------------------------------------------added
void LinearFollowedByPowerLawJ2IsotropicHardening::hardening(double p0, const IPJ2IsotropicHardening &ipvprev, double p,double T, IPJ2IsotropicHardening &ipv) const
{
  double dR=0., ddR=0., intR=0., dRdT=0., ddRdTT=0., ddRdT=0.;
  double R0 = getYield0(T);
  double R = R0;
  if (ipvprev.isSaturated()){
    R = ipvprev.getR();
    dR = 0.;
    ddR = 0.;
    intR = ipvprev.getIntegR()+ R*(p-p0);
  }
  else{
    double tol=1.e-16;
    double h1T=_h1*_temFunc_h1->getVal(T);
    double pexpT=_pexp*_temFunc_pexp->getVal(T);
    double hexpT=_hexp*_temFunc_hexp->getVal(T);

    if (p <= pexpT)
    {
      R = getYield0(T) + h1T*p;
      dR = h1T;
      ddR = 0.;
      intR = getYield0(T)*p + 0.5*h1T*p*p;
    }
    // power part
    else
    {
      R = (getYield0(T)+h1T*pexpT) * pow(p/pexpT,hexpT);
      dR = R*hexpT/p;
      ddR = dR*(hexpT-1.)/p;

      intR = getYield0(T)*pexpT + 0.5*h1T*pexpT*pexpT;
      intR += R*p /(hexpT+1.) -((getYield0(T)+h1T*pexpT)*pexpT/(hexpT+1.));
    }

    double dh1dT=_h1*_temFunc_h1->getDiff(T);
    double ddh1dTT=_h1*_temFunc_h1->getDoubleDiff(T);
    double dhexpdT=_hexp*_temFunc_hexp->getDiff(T);
    double ddhexpdTT=_hexp*_temFunc_hexp->getDoubleDiff(T);
    double dpexpdT=_pexp*_temFunc_pexp->getDiff(T);
    double ddpexpdTT=_pexp*_temFunc_pexp->getDoubleDiff(T);
    if(p<pexpT)
    {
      dRdT=getDYield0DT(T)+dh1dT*p;
      ddRdTT=getDDYield0DTT(T)+ddh1dTT*p;
    }
    else
    {
      dRdT=getDYield0DT(T)*(1+h1T*pexpT)*pow(p/pexpT,hexpT)+getYield0(T)*(1+dh1dT*pexpT+h1T*dpexpdT)*pow(p/pexpT,hexpT)
            +getYield0(T)*(1+h1T*pexpT)*((-dpexpdT/pexpT)*hexpT+log(p/pexpT)*dhexpdT)*pow(p/pexpT,hexpT);
      // ddRdTT=getDDYield0DTT(T)*(1+h1T*pexpT)*pow(p/pexpT,hexpT)+getDYield0DT(T)*((dh1dT*pexpT+h1T*dpexpdT)*pow(p/pexpT,hexpT)
       //             +(1+h1T*pexpT)*pow(p/pexpT,hexpT)*(dhexpdT*ln(p/pexpT)-hexpT/(p/pexpT)*(p/(pexpT*pexpT)*dpexpdT))); // 1st term lmao
       // TBD
    }

  }
  ipv.set(R0,R,dR,ddR,intR,dRdT,ddRdTT,ddRdT);
}

//--------------------------------end
J2IsotropicHardening * LinearFollowedByPowerLawJ2IsotropicHardening::clone() const
{
  return new LinearFollowedByPowerLawJ2IsotropicHardening(*this);
}

LinearFollowedByMultiplePowerLawJ2IsotropicHardening::LinearFollowedByMultiplePowerLawJ2IsotropicHardening(const int num,  double yield0, double h, double p1, double n1, double p2, double n2):
  J2IsotropicHardening(num,yield0,true),_H(h),_p1(p1),_n1(n1),_p2(p2),_n2(n2){};

LinearFollowedByMultiplePowerLawJ2IsotropicHardening::LinearFollowedByMultiplePowerLawJ2IsotropicHardening(const LinearFollowedByMultiplePowerLawJ2IsotropicHardening& src):
      J2IsotropicHardening(src),_H(src._H),_p1(src._p1),_n1(src._n1),_p2(src._p2),_n2(src._n2){}

LinearFollowedByMultiplePowerLawJ2IsotropicHardening& LinearFollowedByMultiplePowerLawJ2IsotropicHardening::operator =(const J2IsotropicHardening& src){
  J2IsotropicHardening::operator =(src);
  const LinearFollowedByMultiplePowerLawJ2IsotropicHardening* psrc = dynamic_cast<const LinearFollowedByMultiplePowerLawJ2IsotropicHardening*>(&src);
  if (psrc != NULL){
    _H = psrc->_H;
    _p1 = psrc->_p1;
    _n1 = psrc->_n1;
    _p2 = psrc->_p2;
    _n2 = psrc->_n2;
  }
  return *this;
}

LinearFollowedByMultiplePowerLawJ2IsotropicHardening::~LinearFollowedByMultiplePowerLawJ2IsotropicHardening(){};


void LinearFollowedByMultiplePowerLawJ2IsotropicHardening::createIPVariable(IPJ2IsotropicHardening* &ipv) const
{
  if(ipv != NULL) delete ipv;
  ipv = new IPLinearFollowedByMultiplePowerLawJ2IsotropicHardening(getYield0());
}

void LinearFollowedByMultiplePowerLawJ2IsotropicHardening::hardening(double p0, const IPJ2IsotropicHardening &ipvprev, double p, IPJ2IsotropicHardening &ipv) const
{
  double dR=0., ddR=0., intR=0., dRdT=0., ddRdTT=0., ddRdT=0.;
  double R0 = getYield0();
  double R = R0;
  if (ipvprev.isSaturated()){
    R = ipvprev.getR();
    dR = 0.;
    ddR = 0.;
    intR = ipvprev.getIntegR()+ R*(p-p0);
  }
  else{

    double sy1 = getYield0() + _H*_p1;
    double sy2 = sy1*pow(_p2/_p1,_n1);

    if (p <= _p1){
      R =  getYield0()+_H*p;
      dR = _H;
      ddR = 0.;
      intR = getYield0()*p + 0.5*_H*p*p;
    }
    else if ( p > _p1 and p <= _p2){
      R = sy1*pow(p/_p1,_n1);
      dR = R*_n1/p;
      ddR = dR*_n1/p - R*_n1/(p*p);
      intR = getYield0()*_p1 + 0.5*_H*_p1*_p1 + R*p/(_n1+1) - sy1*_p1/(_n1+1);
    }
    else{
      R = sy2*pow(p/_p2,_n2);
      dR = R*_n2/p;
      ddR = dR*_n2/p - R*_n2/(p*p);
      intR = getYield0()*_p1 + 0.5*_H*_p1*_p1 + sy2*_p2/(_n1+1) - sy1*_p1/(_n1+1) + R*p/(_n2+1) - sy2*_p2/(_n2+1);;
    }
  }
  ipv.set(R0,R,dR,ddR,intR,dRdT,ddRdTT,ddRdT);
}
//-----------------------------------------------------added
void LinearFollowedByMultiplePowerLawJ2IsotropicHardening::hardening(double p0, const IPJ2IsotropicHardening &ipvprev, double p,double T, IPJ2IsotropicHardening &ipv) const
{
  double dR=0., ddR=0., intR=0.,dRdT=0.,ddRdTT=0.,ddRdT=0.;
  double R0 = getYield0(T);
  double R = R0;
  if (ipvprev.isSaturated()){
    R = ipvprev.getR();
    dR = 0.;
    ddR = 0.;
    intR = ipvprev.getIntegR()+ R*(p-p0);
  }
  else{
    double sy1 = getYield0(T) + _H*_p1;
    double sy2 = sy1*pow(_p2/_p1,_n1);

    if (p <= _p1){
      R =  getYield0(T)+_H*p;
      dR = _H;
      ddR = 0.;
      intR = getYield0(T)*p + 0.5*_H*p*p;
      dRdT = getDYield0DT(T);
      ddRdTT = getDDYield0DTT(T);
    }
    else if ( p > _p1 and p <= _p2){
      R = sy1*pow(p/_p1,_n1);
      dR = R*_n1/p;
      ddR = dR*_n1/p - R*_n1/(p*p);
      intR = getYield0(T)*_p1 + 0.5*_H*_p1*_p1 + R*p/(_n1+1) - sy1*_p1/(_n1+1);
      dRdT = getDYield0DT(T)*pow(p/_p1,_n1);
      ddRdTT = getDDYield0DTT(T)*pow(p/_p1,_n1);
    }
    else{
      R = sy2*pow(p/_p2,_n2);
      dR = R*_n2/p;
      ddR = dR*_n2/p - R*_n2/(p*p);
      intR = getYield0(T)*_p1 + 0.5*_H*_p1*_p1 + sy2*_p2/(_n1+1) - sy1*_p1/(_n1+1) + R*p/(_n2+1) - sy2*_p2/(_n2+1);
      dRdT = getDYield0DT(T)*pow(p/_p2,_n2);
      ddRdTT = getDDYield0DTT(T)*pow(p/_p2,_n2);
    }
  }

  ipv.set(R0,R,dR,ddR,intR,dRdT,ddRdTT,ddRdT);

}

//--------------------------------end
J2IsotropicHardening * LinearFollowedByMultiplePowerLawJ2IsotropicHardening::clone() const
{
  return new LinearFollowedByMultiplePowerLawJ2IsotropicHardening(*this);
}

/* PolynomialJ2IsotropicHardening*/
PolynomialJ2IsotropicHardening::PolynomialJ2IsotropicHardening(const int num, double yield0, int order,  bool init):
J2IsotropicHardening(num,yield0,init),_order(order){
  _coefficients.resize(_order+1);
  _coefficients.setAll(0.);
  _coefficients(0) = _yield0;
};

PolynomialJ2IsotropicHardening::PolynomialJ2IsotropicHardening(const PolynomialJ2IsotropicHardening& src):
  J2IsotropicHardening(src), _order(src._order),_coefficients(src._coefficients){};

PolynomialJ2IsotropicHardening& PolynomialJ2IsotropicHardening::operator =(const J2IsotropicHardening& src){
  J2IsotropicHardening::operator=(src);
  const PolynomialJ2IsotropicHardening* psrc = dynamic_cast<const PolynomialJ2IsotropicHardening*>(&src);
  if(psrc != NULL)
  {
    _order = psrc->_order;
    _coefficients.resize(psrc->_coefficients.size());
    _coefficients.setAll(psrc->_coefficients);
  }
  return *this;
};

void PolynomialJ2IsotropicHardening::setCoefficients(const int i, const double val){
  _coefficients(i) = val;
  if (i==0) _yield0 = val;
};

void PolynomialJ2IsotropicHardening::createIPVariable(IPJ2IsotropicHardening* &ipv) const
{
  if(ipv != NULL) delete ipv;
  ipv = new IPPolynomialJ2IsotropicHardening(getYield0());
}

void PolynomialJ2IsotropicHardening::hardening(double p0, const IPJ2IsotropicHardening &ipvprev, double p, IPJ2IsotropicHardening &ipv) const
{
  //_coefficients.print("_coefficients");
  double R0 = getYield0();
  double R = R0;
  double dR=0., ddR=0., intR=0., dRdT=0., ddRdTT=0., ddRdT=0.;
  if (ipvprev.isSaturated()){
    R = ipvprev.getR();
    dR = 0.;
    ddR = 0.;
    intR = ipvprev.getIntegR()+ R*(p-p0);
  }
  else{
    if (_order == 1){
      R += _coefficients(0)+ _coefficients(1)*p;
      dR = _coefficients(1);
      ddR = 0.;
      intR = _coefficients(0)*p+ _coefficients(1)*p*p/2.;
    }
    else if (_order == 2){
      R += _coefficients(0)+ _coefficients(1)*p + _coefficients(2)*p*p;
      dR = _coefficients(1)+2.*_coefficients(2)*p;
      ddR = 2.*_coefficients(2);
      intR = _coefficients(0)*p+ _coefficients(1)*p*p/2.+ _coefficients(2)*p*p*p/3.;
    }
    else if (_order == 3){
      R += _coefficients(0)+ _coefficients(1)*p + _coefficients(2)*p*p+_coefficients(3)*p*p*p;
      dR = _coefficients(1)+2.*_coefficients(2)*p+ 3*_coefficients(3)*p*p;
      ddR = 2.*_coefficients(2)+6.*_coefficients(3)*p;
      intR = _coefficients(0)*p+ _coefficients(1)*p*p/2.+ _coefficients(2)*p*p*p/3.+_coefficients(3)*p*p*p*p/4.;
    }
    else
      Msg::Error("order %d is not implemented PolynomialJ2IsotropicHaderning::hardening",_order);
  }
  ipv.set(R0,R,dR,ddR,intR,dRdT,ddRdTT,ddRdT);
}

void PolynomialJ2IsotropicHardening::hardening(double p0, const IPJ2IsotropicHardening &ipvprev, double p, double T, IPJ2IsotropicHardening &ipv) const
{
  double dR=0., ddR=0., intR=0.,dRdT=0.,ddRdTT=0.,ddRdT=0.;
  double R0 = getYield0(T);
  double R = R0;
  if (ipvprev.isSaturated()){
    R = ipvprev.getR();
    dR = 0.;
    ddR = 0.;
    intR = ipvprev.getIntegR()+ R*(p-p0);
  }
  else{

    if (_order == 1){
      R += _coefficients(0)+ _coefficients(1)*p;
      dR = _coefficients(1);
      ddR = 0.;
      intR = _coefficients(0)*p+ _coefficients(1)*p*p/2.;
    }
    else if (_order == 2){
      R += _coefficients(0)+ _coefficients(1)*p + _coefficients(2)*p*p;
      dR = _coefficients(1)+2.*_coefficients(2)*p;
      ddR = 2.*_coefficients(2);
      intR = _coefficients(0)*p+ _coefficients(1)*p*p/2.+ _coefficients(2)*p*p*p/3.;
    }
    else if (_order == 3){
      R += _coefficients(0)+ _coefficients(1)*p + _coefficients(2)*p*p+_coefficients(3)*p*p*p;
      dR = _coefficients(1)+2.*_coefficients(2)*p+ 3*_coefficients(3)*p*p;
      ddR = 2.*_coefficients(2)+6.*_coefficients(3)*p;
      intR = _coefficients(0)*p+ _coefficients(1)*p*p/2.+ _coefficients(2)*p*p*p/3.+_coefficients(3)*p*p*p*p/4.;
    }
    else
      Msg::Error("order %d is not implemented PolynomialJ2IsotropicHaderning::hardening",_order);

    dRdT = getDYield0DT(T);
    dRdT = getDDYield0DTT(T);
  }
  ipv.set(R0,R,dR,ddR,intR,dRdT,ddRdTT,ddRdT);
};


J2IsotropicHardening * PolynomialJ2IsotropicHardening::clone() const
{
  return new PolynomialJ2IsotropicHardening(*this);
}


TwoExpJ2IsotropicHaderning::TwoExpJ2IsotropicHaderning(const int num,  double yield0, double K, bool init):
J2IsotropicHardening(num,yield0,init),_K(K){
  _temFunc_K  = new constantScalarFunction(1.);
};

TwoExpJ2IsotropicHaderning::TwoExpJ2IsotropicHaderning(const TwoExpJ2IsotropicHaderning& src):
  J2IsotropicHardening(src), _K(src._K){
  _temFunc_K = NULL;
  if (src._temFunc_K != NULL){
    _temFunc_K = src._temFunc_K->clone();
  }
};

TwoExpJ2IsotropicHaderning& TwoExpJ2IsotropicHaderning::operator =(const J2IsotropicHardening& src){
  J2IsotropicHardening::operator=(src);
  const TwoExpJ2IsotropicHaderning* psrc = dynamic_cast<const TwoExpJ2IsotropicHaderning*>(&src);
  if(psrc != NULL)
  {
    _K = psrc->_K;
    if (psrc->_temFunc_K != NULL){
      if (_temFunc_K != NULL){
        _temFunc_K->operator=(*psrc->_temFunc_K);
      }
      else{
        _temFunc_K = psrc->_temFunc_K->clone();
      }
    }
  }
  return *this;
};

TwoExpJ2IsotropicHaderning::~TwoExpJ2IsotropicHaderning(){
  if (_temFunc_K != NULL) {delete _temFunc_K; _temFunc_K = NULL;};
}

void TwoExpJ2IsotropicHaderning::setTemperatureFunction_K(const scalarFunction& Tfunc){
  if (_temFunc_K != NULL) delete _temFunc_K;
  _temFunc_K = Tfunc.clone();
}

void TwoExpJ2IsotropicHaderning::createIPVariable(IPJ2IsotropicHardening* &ipv) const
{
  if(ipv != NULL) delete ipv;
  ipv = new IPTwoExpJ2IsotropicHaderning(getYield0());
}

void TwoExpJ2IsotropicHaderning::hardening(double p0, const IPJ2IsotropicHardening &ipvprev, double p, IPJ2IsotropicHardening &ipv) const
{
  double R0 = getYield0();
  double R = R0;
  double dR=0., ddR=0., intR=0., dRdT=0., ddRdTT=0., ddRdT=0.;
  if (ipvprev.isSaturated()){
    R = ipvprev.getR();
    dR = 0.;
    ddR = 0.;
    intR = ipvprev.getIntegR()+ R*(p-p0);
  }
  else{
    R =_yield0+ _K*(exp(p)-exp(-2.*p));
    dR = _K*(exp(p)+2.*exp(-2.*p));
    ddR = _K*(exp(p)-4.*exp(-2.*p));
    intR = _yield0*p +_K*(exp(p)+0.5*exp(-2.*p));
  }
  ipv.set(R0,R,dR,ddR,intR,dRdT,ddRdTT,ddRdT);
}
//-------------------------------added
void TwoExpJ2IsotropicHaderning::hardening(double p0, const IPJ2IsotropicHardening &ipvprev, double p,double T, IPJ2IsotropicHardening &ipv) const
{
  double R0 = getYield0(T);
  double R = R0;
  double dR=0., ddR=0., intR=0., dRdT=0., ddRdTT=0., ddRdT=0.;
  if (ipvprev.isSaturated()){
    R = ipvprev.getR();
    dR = 0.;
    ddR = 0.;
    intR = ipvprev.getIntegR()+ R*(p-p0);
  }
  else{
    double yield0T=getYield0(T);
    double KT=_K*_temFunc_K->getVal(T);

    R =yield0T+ KT*(exp(p)-exp(-2.*p));
    dR = KT*(exp(p)+2.*exp(-2.*p));
    ddR = KT*(exp(p)-4.*exp(-2.*p));
    intR = yield0T*p +KT*(exp(p)+0.5*exp(-2.*p));

    double dKdT=_K*_temFunc_K->getDiff(T);
    double ddKdTT=_K*_temFunc_K->getDoubleDiff(T);
    dRdT=getDYield0DT(T)+dKdT*(exp(p)-exp(-2.*p));
    ddRdTT=getDDYield0DTT(T)+ddKdTT*(exp(p)-exp(-2.*p));
  }
  ipv.set(R0,R,dR,ddR,intR,dRdT,ddRdTT,ddRdT);
}

//-------------------------------------end
J2IsotropicHardening * TwoExpJ2IsotropicHaderning::clone() const
{
  return new TwoExpJ2IsotropicHaderning(*this);
}

TanhJ2IsotropicHardening::TanhJ2IsotropicHardening(const int num,  double yield0, double K, double p0, bool init):
J2IsotropicHardening(num,yield0,init),_K(K),_p0(p0){
  _temFunc_K= new constantScalarFunction(1.);
  _temFunc_p0= new constantScalarFunction(1.);
};

TanhJ2IsotropicHardening::TanhJ2IsotropicHardening(const TanhJ2IsotropicHardening& src):
  J2IsotropicHardening(src), _K(src._K),_p0(src._p0){
  _temFunc_K = NULL;
  if (src._temFunc_K != NULL){
    _temFunc_K = src._temFunc_K->clone();
  }

  _temFunc_p0 = NULL;
  if (src._temFunc_p0 != NULL){
    _temFunc_p0 = src._temFunc_p0->clone();
  }
};

TanhJ2IsotropicHardening& TanhJ2IsotropicHardening::operator =(const J2IsotropicHardening& src){
  J2IsotropicHardening::operator=(src);
  const TanhJ2IsotropicHardening* psrc = dynamic_cast<const TanhJ2IsotropicHardening*>(&src);
  if(psrc != NULL)
  {
    _K = psrc->_K;
    _p0 = psrc->_p0;
    if (psrc->_temFunc_K != NULL){
      if (_temFunc_K != NULL){
        _temFunc_K->operator=(*psrc->_temFunc_K);
      }
      else{
        _temFunc_K = psrc->_temFunc_K->clone();
      }
    }

    if (psrc->_temFunc_p0 != NULL){
      if (_temFunc_p0 != NULL){
        _temFunc_p0->operator=(*psrc->_temFunc_p0);
      }
      else{
        _temFunc_p0 = psrc->_temFunc_p0->clone();
      }
    }
  }
  return *this;
};

TanhJ2IsotropicHardening::~TanhJ2IsotropicHardening(){
  if (_temFunc_K != NULL) {delete _temFunc_K; _temFunc_K = NULL;}
  if (_temFunc_p0 != NULL) {delete _temFunc_p0; _temFunc_p0 = NULL;};
};

void TanhJ2IsotropicHardening::setTemperatureFunction_K(const scalarFunction& Tfunc){
  if (_temFunc_K != NULL) delete _temFunc_K;
  _temFunc_K = Tfunc.clone();
}
void TanhJ2IsotropicHardening::setTemperatureFunction_p0(const scalarFunction& Tfunc){
  if (_temFunc_p0 != NULL) delete _temFunc_p0;
  _temFunc_p0 = Tfunc.clone();
}

void TanhJ2IsotropicHardening::createIPVariable(IPJ2IsotropicHardening* &ipv) const
{
  if(ipv != NULL) delete ipv;
  ipv = new IPPolynomialJ2IsotropicHardening(getYield0());
}

void TanhJ2IsotropicHardening::hardening(double p0, const IPJ2IsotropicHardening &ipvprev, double p, IPJ2IsotropicHardening &ipv) const
{
  double R0 = getYield0();
  double R = R0;
  double dR=0., ddR=0., intR=0.,dRdT=0.,ddRdTT=0.,ddRdT=0.;
  if (ipvprev.isSaturated()){
    R = ipvprev.getR();
    dR = 0.;
    ddR = 0.;
    intR = ipvprev.getIntegR()+ R*(p-p0);
  }
  else{
    double th = tanh(p/_p0);
    R =_yield0+ _K*th;
    dR = _K*(1.-th*th)/_p0;
    ddR = _K*2.*th*(th*th-1.)/(_p0*_p0);
    intR = _yield0*p +_K*_p0*log(cosh(p/_p0));
  }
  ipv.set(R0,R,dR,ddR,intR,dRdT,ddRdTT,ddRdT);
}
//------------------------------added
void TanhJ2IsotropicHardening::hardening(double p0, const IPJ2IsotropicHardening &ipvprev, double p,double T, IPJ2IsotropicHardening &ipv) const
{
  double R0 = getYield0(T);
  double R = R0;
  double dR=0., ddR=0., intR=0., dRdT=0., ddRdTT=0., ddRdT=0.;
  if (ipvprev.isSaturated()){
    R = ipvprev.getR();
    dR = 0.;
    ddR = 0.;
    intR = ipvprev.getIntegR()+ R*(p-p0);
  }
  else{
    double p0T=_p0*_temFunc_p0->getVal(T);
    double yield0T=getYield0(T);
    double KT=_K*_temFunc_K->getVal(T);
    double th = tanh(p/p0T);

    R =yield0T+ KT*th;
    dR = KT*(1.-th*th)/p0T;
    ddR = KT*2.*th*(th*th-1.)/(p0T*p0T);
    intR = yield0T*p +KT*p0T*log(cosh(p/p0T));

    double dKdT=_K*_temFunc_K->getDiff(T);
    double ddKdTT=_K*_temFunc_K->getDoubleDiff(T);
    double dp0dT=_p0*_temFunc_p0->getDiff(T);
    double ddp0dTT=_p0*_temFunc_p0->getDoubleDiff(T);
    dRdT=getDYield0DT(T)+dKdT*tanh(p/p0T)+KT*(1-th*th)*(-p*dp0dT/p0T/p0T);
    ddRdTT=getDDYield0DTT(T)+ddKdTT*tanh(p/p0T)+2*dKdT*(1-th*th)*(-p*dp0dT/p0T/p0T)
            +KT*(-2*th*(1-th*th)*(-p*dp0dT/p0T/p0T)*(-p*dp0dT/p0T/p0T)+(1-th*th)*(-p*ddp0dTT/p0T/p0T+2*p*dp0dT*dp0dT/pow(p0T,3)));
  }
  ipv.set(R0,R,dR,ddR,intR,dRdT,ddRdTT,ddRdT);
}

//------------------------------------------end
J2IsotropicHardening * TanhJ2IsotropicHardening::clone() const
{
  return new TanhJ2IsotropicHardening(*this);
}


TanhSeriesJ2IsotropicHardening::TanhSeriesJ2IsotropicHardening(const int num,  double yield0, int Ksize):
J2IsotropicHardening(num,yield0,true),_Ksize(Ksize),_K(Ksize,0.),_p0(Ksize,0.){

};

TanhSeriesJ2IsotropicHardening::TanhSeriesJ2IsotropicHardening(const TanhSeriesJ2IsotropicHardening& src):
  J2IsotropicHardening(src), _Ksize(src._Ksize),_K(src._K),_p0(src._p0){

};

TanhSeriesJ2IsotropicHardening& TanhSeriesJ2IsotropicHardening::operator =(const J2IsotropicHardening& src){
  J2IsotropicHardening::operator=(src);
  const TanhSeriesJ2IsotropicHardening* psrc = dynamic_cast<const TanhSeriesJ2IsotropicHardening*>(&src);
  if(psrc != NULL)
  {
    _Ksize = psrc->_Ksize;
    _K = psrc->_K;
    _p0 = psrc->_p0;
  }
  return *this;
};

TanhSeriesJ2IsotropicHardening::~TanhSeriesJ2IsotropicHardening(){

};

void TanhSeriesJ2IsotropicHardening::set(int i, double K, double p0){
  if (i > _Ksize) Msg::Error("error in setting data TanhSeriesJ2IsotropicHardening::set");
  _K[i] = K;
  _p0[i] = p0;
};

void TanhSeriesJ2IsotropicHardening::createIPVariable(IPJ2IsotropicHardening* &ipv) const
{
  if(ipv != NULL) delete ipv;
  ipv = new IPPolynomialJ2IsotropicHardening(getYield0());
}

void TanhSeriesJ2IsotropicHardening::hardening(double p0, const IPJ2IsotropicHardening &ipvprev, double p, IPJ2IsotropicHardening &ipv) const
{
  double R0 = getYield0();
  double R = R0;
  double dR=0., ddR=0., intR=0.,dRdT=0., ddRdTT=0., ddRdT=0.;
  if (ipvprev.isSaturated()){
    R = ipvprev.getR();
    dR = 0.;
    ddR = 0.;
    intR = ipvprev.getIntegR()+ R*(p-p0);
  }
  else{
    for (int i=0; i< _Ksize; i++){
      double th = tanh(p/_p0[i]);
      R += _K[i]*th;
      dR += _K[i]*(1.-th*th)/_p0[i];
      ddR += _K[i]*2.*th*(th*th-1.)/(_p0[i]*_p0[i]);
      intR += getYield0()*p +_K[i]*_p0[i]*log(cosh(p/_p0[i]));
    }
  }
  ipv.set(R0,R,dR,ddR,intR,dRdT,ddRdTT,ddRdT);
}
//------------------------------added
void TanhSeriesJ2IsotropicHardening::hardening(double p0, const IPJ2IsotropicHardening &ipvprev, double p,double T, IPJ2IsotropicHardening &ipv) const
{
  double R0 = getYield0(T);
  double R = R0;
  double dR=0., ddR=0., intR=0.,dRdT=0.,ddRdTT=0.,ddRdT=0.;
  if (ipvprev.isSaturated()){
    R = ipvprev.getR();
    dR = 0.;
    ddR = 0.;
    intR = ipvprev.getIntegR()+ R*(p-p0);
  }
  else{
    for (int i=0; i< _Ksize; i++){
      double th = tanh(p/_p0[i]);
      R += _K[i]*th;
      dR += _K[i]*(1.-th*th)/_p0[i];
      ddR += _K[i]*2.*th*(th*th-1.)/(_p0[i]*_p0[i]);
      intR += getYield0(T)*p +_K[i]*_p0[i]*log(cosh(p/_p0[i]));
      dRdT = getDYield0DT(T);
      dRdT = getDDYield0DTT(T);
    }
  }
  ipv.set(R0,R,dR,ddR,intR,dRdT,ddRdTT,ddRdT);
}

//------------------------------------------end
J2IsotropicHardening * TanhSeriesJ2IsotropicHardening::clone() const
{
  return new TanhSeriesJ2IsotropicHardening(*this);
}

LinearFollowedByExponentialFollowedByMultiplePowerLawJ2IsotropicHardening::LinearFollowedByExponentialFollowedByMultiplePowerLawJ2IsotropicHardening(const int num,  double yield0, double h1, double pexp, double h2, double hexp2, double ppo, double npo, bool init):
    J2IsotropicHardening(num,yield0,init), _h1(h1), _pexp(pexp), _h2(h2), _hexp2(hexp2), _ppo(ppo), _npo(npo)
{
  if(h1 < 0. or h2 < 0. or hexp2 <= 0. or pexp < 0.) Msg::Error("LinearFollowedByExponentialJ2IsotropicHardening :: negative hardening parameters");
  _temFunc_h1= new constantScalarFunction(1.);
  _temFunc_h2= new constantScalarFunction(1.);
  _temFunc_pexp = new constantScalarFunction(1.);
  _temFunc_hexp2 = new constantScalarFunction(1.);
  _temFunc_ppo = new constantScalarFunction(1.);
  _temFunc_npo = new constantScalarFunction(1.);
}

LinearFollowedByExponentialFollowedByMultiplePowerLawJ2IsotropicHardening::LinearFollowedByExponentialFollowedByMultiplePowerLawJ2IsotropicHardening(const LinearFollowedByExponentialFollowedByMultiplePowerLawJ2IsotropicHardening &source) :
    J2IsotropicHardening(source)
{
  _h1 = source._h1;
  _pexp = source._pexp;
  _h2 = source._h2;
  _hexp2 = source._hexp2;
  _ppo = source._ppo;
  _npo = source._npo;

  _temFunc_h1 = NULL;
  if (source._temFunc_h1 != NULL){
    _temFunc_h1 = source._temFunc_h1->clone();
  }
  _temFunc_h2 = NULL;
  if (source._temFunc_h2 != NULL){
    _temFunc_h2 = source._temFunc_h2->clone();
  }
  _temFunc_pexp = NULL;
  if (source._temFunc_pexp != NULL){
    _temFunc_pexp = source._temFunc_pexp->clone();
  }
  _temFunc_hexp2 = NULL;
  if (source._temFunc_hexp2 != NULL){
    _temFunc_hexp2 = source._temFunc_hexp2->clone();
  }
  if (source._temFunc_ppo != NULL){
    _temFunc_ppo = source._temFunc_ppo->clone();
  }
  if (source._temFunc_npo != NULL){
    _temFunc_npo = source._temFunc_npo->clone();
  }
}

LinearFollowedByExponentialFollowedByMultiplePowerLawJ2IsotropicHardening& LinearFollowedByExponentialFollowedByMultiplePowerLawJ2IsotropicHardening::operator=(const J2IsotropicHardening &source)
{
  J2IsotropicHardening::operator=(source);
  const LinearFollowedByExponentialFollowedByMultiplePowerLawJ2IsotropicHardening* src = dynamic_cast<const LinearFollowedByExponentialFollowedByMultiplePowerLawJ2IsotropicHardening*>(&source);
  if(src != NULL)
  {
    _h1 = src->_h1;
    _pexp = src->_pexp;
    _h2 = src->_h2;
    _hexp2 = src->_hexp2;
    _ppo = src->_ppo;
    _npo = src->_npo;

     if (src->_temFunc_h1 != NULL){
      if (_temFunc_h1 != NULL){
        _temFunc_h1->operator=(*src->_temFunc_h1);
      }
      else{
        _temFunc_h1 = src->_temFunc_h1->clone();
      }
    }

    if (src->_temFunc_h2 != NULL){
      if (_temFunc_h2 != NULL){
        _temFunc_h2->operator=(*src->_temFunc_h2);
      }
      else{
        _temFunc_h2 = src->_temFunc_h2->clone();
      }
    }

    if (src->_temFunc_pexp != NULL){
      if (_temFunc_pexp != NULL){
        _temFunc_pexp->operator=(*src->_temFunc_pexp);
      }
      else{
        _temFunc_pexp = src->_temFunc_pexp->clone();
      }
    }

    if (src->_temFunc_hexp2 != NULL){
      if (_temFunc_hexp2 != NULL){
        _temFunc_hexp2->operator=(*src->_temFunc_hexp2);
      }
      else{
        _temFunc_hexp2 = src->_temFunc_hexp2->clone();
      }
    }
    if (src->_temFunc_ppo != NULL){
      if (_temFunc_ppo != NULL){
        _temFunc_ppo->operator=(*src->_temFunc_ppo);
      }
      else{
        _temFunc_ppo = src->_temFunc_ppo->clone();
      }
    }
    if (src->_temFunc_npo != NULL){
      if (_temFunc_npo != NULL){
        _temFunc_npo->operator=(*src->_temFunc_npo);
      }
      else{
        _temFunc_npo = src->_temFunc_npo->clone();
      }
    }
  }
  return *this;
}

LinearFollowedByExponentialFollowedByMultiplePowerLawJ2IsotropicHardening::~LinearFollowedByExponentialFollowedByMultiplePowerLawJ2IsotropicHardening(){
  if (_temFunc_h1 != NULL) {delete _temFunc_h1; _temFunc_h1 = NULL;};
  if (_temFunc_h2 != NULL) {delete _temFunc_h2; _temFunc_h2 = NULL;};
  if (_temFunc_pexp != NULL){delete _temFunc_pexp; _temFunc_pexp = NULL;};
  if (_temFunc_hexp2 != NULL) {delete _temFunc_hexp2; _temFunc_hexp2 = NULL;};
  if (_temFunc_ppo != NULL) {delete _temFunc_ppo; _temFunc_ppo = NULL;};
  if (_temFunc_npo != NULL) {delete _temFunc_npo; _temFunc_npo = NULL;}
};


void LinearFollowedByExponentialFollowedByMultiplePowerLawJ2IsotropicHardening::setTemperatureFunction_h1(const scalarFunction& Tfunc){
  if (_temFunc_h1 != NULL) delete _temFunc_h1;
  _temFunc_h1 = Tfunc.clone();
}
void LinearFollowedByExponentialFollowedByMultiplePowerLawJ2IsotropicHardening::setTemperatureFunction_h2(const scalarFunction& Tfunc){
  if (_temFunc_h2 != NULL) delete _temFunc_h2;
  _temFunc_h2 = Tfunc.clone();
}

void LinearFollowedByExponentialFollowedByMultiplePowerLawJ2IsotropicHardening::setTemperatureFunction_pexp(const scalarFunction& Tfunc){
  if (_temFunc_pexp != NULL) delete _temFunc_pexp;
  _temFunc_pexp = Tfunc.clone();
}

void LinearFollowedByExponentialFollowedByMultiplePowerLawJ2IsotropicHardening::setTemperatureFunction_hexp2(const scalarFunction& Tfunc){
  if (_temFunc_hexp2 != NULL) delete _temFunc_hexp2;
  _temFunc_hexp2 = Tfunc.clone();
}

void LinearFollowedByExponentialFollowedByMultiplePowerLawJ2IsotropicHardening::setTemperatureFunction_ppo(const scalarFunction& Tfunc){
  if (_temFunc_ppo != NULL) delete _temFunc_ppo;
  _temFunc_ppo = Tfunc.clone();
}

void LinearFollowedByExponentialFollowedByMultiplePowerLawJ2IsotropicHardening::setTemperatureFunction_npo(const scalarFunction& Tfunc){
  if (_temFunc_npo != NULL) delete _temFunc_npo;
  _temFunc_npo = Tfunc.clone();
}

void LinearFollowedByExponentialFollowedByMultiplePowerLawJ2IsotropicHardening::createIPVariable(IPJ2IsotropicHardening* &ipv) const
{
  if(ipv != NULL) delete ipv;
  ipv = new IPLinearFollowedByExponentialJ2IsotropicHardening(getYield0());
}

void LinearFollowedByExponentialFollowedByMultiplePowerLawJ2IsotropicHardening::hardening(double p0, const IPJ2IsotropicHardening &ipvprev, double p, IPJ2IsotropicHardening &ipv) const
{
  double R0 = getYield0();
  double R = R0;
  double dR(0.), ddR(0.), intR(0.), dRdT(0.), ddRdTT(0.),ddRdT(0.);
  if (ipvprev.isSaturated()){
    R = ipvprev.getR();
    dR = 0.;
    ddR = 0.;
    intR = ipvprev.getIntegR()+ R*(p-p0);
  }
  else {

    if(p < 1.e-16) // Elastic case
    {
      R = getYield0();
      dR = _h1;
      ddR = 0.;
    }
    else if (p <= _pexp) // Plastic case: linear part
    {
      R += _h1*p;
      dR = _h1;
      ddR = 0.;
      intR = getYield0()*p + 0.5*_h1*p*p;
    }
    else if (_pexp < p and p <= _ppo)// Plastic case: exponentional (saturation) part
    {
      double tmp = exp(-(p-_pexp)/_hexp2);
      R += _h1*p + _h2*(1.-tmp);
      dR = _h1 + _h2 * tmp /_hexp2;
      ddR = -_h2 * tmp /_hexp2 /_hexp2;

      intR = getYield0()*p;
      intR += 0.5*_h1*p*p;
      intR += _h2*(p-_pexp) + _h2*(tmp-exp(0.)) *_hexp2;
    }
    else{
      double tmp = exp(-(_ppo-_pexp)/_hexp2);
      double sy1 = getYield0();
      sy1 += _h1*_ppo + _h2*(1.-tmp);
      R = sy1*pow(p/_ppo,_npo);
      dR = R*_npo/p;
      ddR = dR*_npo/p - R*_npo/(p*p);

    }
  }
  ipv.set(R0,R,dR,ddR,intR,dRdT,ddRdTT,ddRdT);
}
//-----------------------------added
void LinearFollowedByExponentialFollowedByMultiplePowerLawJ2IsotropicHardening::hardening(double p0, const IPJ2IsotropicHardening &ipvprev, double p,double T, IPJ2IsotropicHardening &ipv) const
{
  double R0 = getYield0(T);
  double R = R0;
  double dR(0.), ddR(0.), intR(0.),dRdT(0.),ddRdTT(0.),ddRdT(0.);
  if (ipvprev.isSaturated()){
    R = ipvprev.getR();
    dR = 0.;
    ddR = 0.;
    intR = ipvprev.getIntegR()+ R*(p-p0);
  }
  else {

    double h1T = _h1*_temFunc_h1->getVal(T);
    double pexpT = _pexp*_temFunc_pexp->getVal(T);
    double h2T = _h2*_temFunc_h2->getVal(T);
    double hexp2T = _hexp2*_temFunc_hexp2->getVal(T);
    double ppoT = _ppo*_temFunc_ppo->getVal(T);
    double npoT = _npo*_temFunc_npo->getVal(T);
    if(p < 1.e-16) // Elastic case
    {
      R = getYield0(T);
      dR = h1T;
      ddR = 0.;
    }
    else if (p <= pexpT) // Plastic case: linear part
    {
      R += h1T*p;
      dR = h1T;
      ddR = 0.;
      intR = getYield0(T)*p + 0.5*h1T*p*p;
    }
    else if (pexpT < p and p <= ppoT) // Plastic case: exponentional (saturation) part
    {
      double tmp = exp(-(p-pexpT)/hexp2T);
      R += h1T*p + h2T*(1.-tmp);
      dR = h1T + h2T * tmp /hexp2T;
      ddR = -h2T * tmp /hexp2T /hexp2T;

      intR = getYield0(T)*p;
      intR += 0.5*h1T*p*p;
      intR += h2T*(p-pexpT) + h2T*(tmp-exp(0.)) *hexp2T;
    }
    else{
      double tmp = exp(-(ppoT-pexpT)/hexp2T);
      double sy1 = getYield0(T);
      sy1 += h1T*ppoT + h2T*(1.-tmp);
      R = sy1*pow(p/ppoT,npoT);
      dR = R*npoT/p;
      ddR = dR*npoT/p - R*npoT/(p*p);

      intR = getYield0(T)*p;
      intR += 0.5*h1T*p*p;
      intR += h2T*(p-pexpT) + h2T*(tmp-exp(0.)) *hexp2T;
    }
    double dh1dT=_h1*_temFunc_h1->getDiff(T);
    double ddh1dTT=_h1*_temFunc_h1->getDoubleDiff(T);
    double dhexp2dT=_hexp2*_temFunc_hexp2->getDiff(T);
    double ddhexp2dTT=_hexp2*_temFunc_hexp2->getDoubleDiff(T);
    double dh2dT=_h2*_temFunc_h2->getDiff(T);
    double ddh2dTT=_h2*_temFunc_h2->getDoubleDiff(T);
    double dpexpdT=_pexp*_temFunc_pexp->getDiff(T);
    double ddpexpdTT=_pexp*_temFunc_pexp->getDoubleDiff(T);
    if (p < 1.e-16){
      dRdT = getDYield0DT(T);
      ddRdTT = getDDYield0DTT(T);
    }
    else if (p<=pexpT)
    {
       dRdT=getDYield0DT(T)+dh1dT*p;
       ddRdTT=getDDYield0DTT(T)+ddh1dTT*p;
    }
    else
    {
       dRdT=getDYield0DT(T)+dh1dT*p+dh2dT*(1-exp(-hexp2T*(p-pexpT)))+h2T*(dhexp2dT*(p-pexpT)-hexp2T*dpexpdT)*exp(-hexp2T*p);
       ddRdTT=getDDYield0DTT(T)+ddh1dTT*p+ddh2dTT*(1-exp(-hexp2T*(p-pexpT)))+dh2dT*(dhexp2dT*(p-pexpT)-hexp2T*dpexpdT)*exp(-hexp2T*p)
              +dh2dT*((dhexp2dT*(p-pexpT)+hexp2T*(-dpexpdT))*exp(-hexp2T*(p-pexpT)))
              +h2T*((ddhexp2dTT*(p-pexpT)-dhexp2dT*dpexpdT-dhexp2dT*dpexpdT-hexp2T*ddpexpdTT)*exp(-hexp2T*p)-(dhexp2dT*(p-pexpT)-hexp2T*dpexpdT)*dhexp2dT*p*exp(-hexp2T*p));
    }

  }
  ipv.set(R0,R,dR,ddR,intR,dRdT,ddRdTT,ddRdT);
}

//----------------------------end
J2IsotropicHardening* LinearFollowedByExponentialFollowedByMultiplePowerLawJ2IsotropicHardening::clone() const
{
  return new LinearFollowedByExponentialFollowedByMultiplePowerLawJ2IsotropicHardening(*this);
}

LinearFollowedByMultipleSwiftJ2IsotropicHardening::LinearFollowedByMultipleSwiftJ2IsotropicHardening(const int num,  double yield0, double h0, double p1, double h1, double n1, double p2, double h2, double n2, bool init):
    J2IsotropicHardening(num,yield0,init), _h0(h0), _h1(h1), _h2(h2), _n1(n1), _n2(n2), _p1(p1), _p2(p2)
{
  if(h0 < 0. or h1 < 0. or h2 < 0. or n1 < 0. or n2 < 0. or p1 <= 0. or p2 < p1) Msg::Error("LinearFollowedByMultipleSwiftJ2IsotropicHardening :: negative or unsuitable hardening parameters");
  _temFunc_h0 = new constantScalarFunction(1.);
  _temFunc_h1 = new constantScalarFunction(1.);
  _temFunc_h2 = new constantScalarFunction(1.);
  _temFunc_n1 = new constantScalarFunction(1.);
  _temFunc_n2 = new constantScalarFunction(1.);
}

LinearFollowedByMultipleSwiftJ2IsotropicHardening::LinearFollowedByMultipleSwiftJ2IsotropicHardening(const LinearFollowedByMultipleSwiftJ2IsotropicHardening &source) :
    J2IsotropicHardening(source)
{
  _h0 = source._h0;
  _h1 = source._h1;
  _h2 = source._h2;
  _n1 = source._n1;
  _n2 = source._n2;
  _p1 = source._p1;
  _p2 = source._p2;

  _temFunc_h0 = NULL;
  if (source._temFunc_h0 != NULL){
    _temFunc_h0 = source._temFunc_h0->clone();
  }
  _temFunc_h1 = NULL;
  if (source._temFunc_h1 != NULL){
    _temFunc_h1 = source._temFunc_h1->clone();
  }
  _temFunc_h2 = NULL;
  if (source._temFunc_h2 != NULL){
    _temFunc_h2 = source._temFunc_h2->clone();
  }
  _temFunc_n1 = NULL;
  if (source._temFunc_n1 != NULL){
	 _temFunc_n1 = source._temFunc_n1->clone();
  }
  _temFunc_n2 = NULL;
  if (source._temFunc_n2 != NULL){
	 _temFunc_n2 = source._temFunc_n2->clone();
  }
}

LinearFollowedByMultipleSwiftJ2IsotropicHardening& LinearFollowedByMultipleSwiftJ2IsotropicHardening::operator=(const J2IsotropicHardening &source)
{
  J2IsotropicHardening::operator=(source);
  const LinearFollowedByMultipleSwiftJ2IsotropicHardening* src = dynamic_cast<const LinearFollowedByMultipleSwiftJ2IsotropicHardening*>(&source);
  if(src != NULL)
  {
	_h0 = src->_h0;
    _h1 = src->_h1;
    _h2 = src->_h2;
    _n1 = src->_n1;
    _n2 = src->_n2;
    _p1 = src->_p1;
    _p2 = src->_p2;

    if (src->_temFunc_h0 != NULL){
      if (_temFunc_h0 != NULL){
        _temFunc_h0->operator=(*src->_temFunc_h0);
      }
      else{
        _temFunc_h0 = src->_temFunc_h0->clone();
      }
    }

    if (src->_temFunc_h1 != NULL){
      if (_temFunc_h1 != NULL){
        _temFunc_h1->operator=(*src->_temFunc_h1);
      }
      else{
        _temFunc_h1 = src->_temFunc_h1->clone();
      }
    }

    if (src->_temFunc_h2 != NULL){
      if (_temFunc_h2 != NULL){
        _temFunc_h2->operator=(*src->_temFunc_h2);
      }
      else{
        _temFunc_h2 = src->_temFunc_h2->clone();
      }
    }

    if (src->_temFunc_n1 != NULL){
      if (_temFunc_n1 != NULL){
    	  _temFunc_n1->operator=(*src->_temFunc_n1);
      }
      else{
    	  _temFunc_n1 = src->_temFunc_n1->clone();
      }
    }

    if (src->_temFunc_n2 != NULL){
      if (_temFunc_n2 != NULL){
    	  _temFunc_n2->operator=(*src->_temFunc_n2);
      }
      else{
    	  _temFunc_n2 = src->_temFunc_n2->clone();
      }
    }
  }
  return *this;
}

LinearFollowedByMultipleSwiftJ2IsotropicHardening::~LinearFollowedByMultipleSwiftJ2IsotropicHardening(){
  if (_temFunc_h0 != NULL) {delete _temFunc_h0; _temFunc_h0 = NULL;};
  if (_temFunc_h1 != NULL) {delete _temFunc_h1; _temFunc_h1 = NULL;};
  if (_temFunc_h2 != NULL) {delete _temFunc_h2; _temFunc_h2 = NULL;};
  if (_temFunc_n1 != NULL) {delete _temFunc_n1; _temFunc_n1 = NULL;};
  if (_temFunc_n2 != NULL) {delete _temFunc_n2; _temFunc_n2 = NULL;};
};

void LinearFollowedByMultipleSwiftJ2IsotropicHardening::setTemperatureFunction_h0(const scalarFunction& Tfunc){
  if (_temFunc_h0 != NULL) delete _temFunc_h0;
  _temFunc_h0 = Tfunc.clone();
}
void LinearFollowedByMultipleSwiftJ2IsotropicHardening::setTemperatureFunction_h1(const scalarFunction& Tfunc){
  if (_temFunc_h1 != NULL) delete _temFunc_h1;
  _temFunc_h1 = Tfunc.clone();
}
void LinearFollowedByMultipleSwiftJ2IsotropicHardening::setTemperatureFunction_h2(const scalarFunction& Tfunc){
  if (_temFunc_h2 != NULL) delete _temFunc_h2;
  _temFunc_h2 = Tfunc.clone();
}
void LinearFollowedByMultipleSwiftJ2IsotropicHardening::setTemperatureFunction_n1(const scalarFunction& Tfunc){
  if (_temFunc_n1 != NULL) delete _temFunc_n1;
  _temFunc_n1 = Tfunc.clone();
}
void LinearFollowedByMultipleSwiftJ2IsotropicHardening::setTemperatureFunction_n2(const scalarFunction& Tfunc){
  if (_temFunc_n2 != NULL) delete _temFunc_n2;
  _temFunc_n2 = Tfunc.clone();
}


void LinearFollowedByMultipleSwiftJ2IsotropicHardening::createIPVariable(IPJ2IsotropicHardening* &ipv) const
{
  if(ipv != NULL) delete ipv;
  ipv = new IPLinearFollowedByExponentialJ2IsotropicHardening(getYield0());
}

void LinearFollowedByMultipleSwiftJ2IsotropicHardening::hardening(double p0, const IPJ2IsotropicHardening &ipvprev, double p, IPJ2IsotropicHardening &ipv) const
{
  double R0 = getYield0();
  double R = R0;
  double dR(0.), ddR(0.), intR(0.), dRdT(0.), ddRdTT(0.),ddRdT(0.);
  if (ipvprev.isSaturated()){
    R = ipvprev.getR();
    dR = 0.;
    ddR = 0.;
    intR = ipvprev.getIntegR()+ R*(p-p0);
  }
  else {

    if(p < 1.e-16) // Elastic case
    {
      R = getYield0();
      dR = _h0;
      ddR = 0.;
    }
    else if (p <= _p1) // Plastic case: linear part
    {
      R += _h0*p;
      dR = _h0;
      ddR = 0.;
      intR = R0*p + 0.5*_h0*p*p;
    }
    else if (_p1 < p and p <= _p2)// Plastic case: Swift1
    {
      double temp0 = R0 + _h0*p;
      double pp = 1.+ _h1*(p-_p1);
      R = temp0*pow(pp,_n1);
      dR = _h0*pow(pp,_n1) + temp0*_h1*_n1*pow(pp,_n1-1);
      ddR = 2*_h0*_h1*_n1*pow(pp,_n1-1) + _h1*_h1*(_n1-1)*_n1*temp0*pow(pp,_n1-2);
      intR = ( pow(pp,_n1+1) * (_h1*(_h0*((_n1+1)*p+_p1)+R0*(_n1+2))-_h0) ) / (_h1*_h1*(_n1+1)*(_n1+2));
    }
    else{
      double temp0 = R0 + _h0*p;
      double temp1 = temp0*pow(1.+_h1*(p-_p1), _n1);
      double pp = 1.+_h2*(p-_p2);
      R = temp1*pow(pp,_n2);

      double Dtemp0 = _h0;
      double Dtemp1 = Dtemp0*pow(1.+_h1*(p-_p1), _n1) + temp0*_h1*_n1*pow(1.+_h1*(p-_p1), _n1-1);
      double DDtemp1 = 2*Dtemp0*_h1*_n1*pow(1.+_h1*(p-_p1), _n1-1) + temp0*_h1*_h1*_n1*(_n1-1)*pow(1.+_h1*(p-_p1), _n1-2);
      dR = Dtemp1*pow(pp,_n2) + temp1*_h2*_n2*pow(pp,_n2-1);
      ddR = DDtemp1*pow(pp,_n2) + 2*Dtemp1*_h2*_n2*pow(pp,_n2-1) + temp1*_h2*_h2*_n2*(_n2-1)*pow(pp,_n2-2);
      intR = ipvprev.getIntegR()+ 0.5*(R+ipvprev.getR())*(p-p0); // approximation
    }
  }
  ipv.set(R0,R,dR,ddR,intR,dRdT,ddRdTT,ddRdT);
}
//-----------------------------added
void LinearFollowedByMultipleSwiftJ2IsotropicHardening::hardening(double p0, const IPJ2IsotropicHardening &ipvprev, double p,double T, IPJ2IsotropicHardening &ipv) const
{
  double R0 = getYield0(T);
  double R = R0;
  double dR(0.), ddR(0.), intR(0.),dRdT(0.),ddRdTT(0.),ddRdT(0.);
  if (ipvprev.isSaturated()){
    R = ipvprev.getR();
    dR = 0.;
    ddR = 0.;
    intR = ipvprev.getIntegR()+ R*(p-p0);
  }
  else {
	double h0 = _h0*_temFunc_h0->getVal(T);
    double h1 = _h1*_temFunc_h1->getVal(T);
    double h2 = _h2*_temFunc_h2->getVal(T);
    double n1 = _n1*_temFunc_n1->getVal(T);
    double n2 = _n2*_temFunc_n2->getVal(T);
    double dh0dT=_h0*_temFunc_h0->getDiff(T);
    double ddh0dTT=_h0*_temFunc_h0->getDoubleDiff(T);
    double dh1dT=_h1*_temFunc_h1->getDiff(T);
    double ddh1dTT=_h1*_temFunc_h1->getDoubleDiff(T);
    double dh2dT=_h2*_temFunc_h2->getDiff(T);
    double ddh2dTT=_h2*_temFunc_h2->getDoubleDiff(T);
    double dn1dT=_n1*_temFunc_n1->getDiff(T);
    double ddn1dTT=_n1*_temFunc_n1->getDoubleDiff(T);
    double dn2dT=_n2*_temFunc_n2->getDiff(T);
    double ddn2dTT=_n2*_temFunc_n2->getDoubleDiff(T);
    if(p < 1.e-16) // Elastic case
    {
      R = getYield0(T);
      dR = h0;
      ddR = 0.;
      dRdT = getDYield0DT(T);
      ddRdTT = getDDYield0DTT(T);
    }
    else if (p <= _p1) // Plastic case: linear part
    {
      R = R0 + h0*p;
      dR = h0;
      ddR = 0.;
      intR = R0*p + 0.5*h0*p*p;

      dRdT=getDYield0DT(T)+dh0dT*p;
      ddRdTT=getDDYield0DTT(T)+ddh0dTT*p;
    }
    else if (_p1 < p and p <= _p2)// Plastic case: Swift1
    {
      // _n1 and _n2 are assumed to be independent of T
      double temp0 = R0 + h0*p;
      double pp = 1.+ h1*(p-_p1);
      R = temp0*pow(pp,n1);
      dR = h0*pow(pp,n1) + temp0*h1*n1*pow(pp,n1-1);
      ddR = 2*h0*h1*n1*pow(pp,n1-1) + h1*h1*(n1-1)*n1*temp0*pow(pp,n1-2);
      intR = ( pow(pp,n1+1) * (h1*(h0*((n1+1)*p+_p1)+R0*(n1+2))-h0) ) / (h1*h1*(n1+1)*(n1+2));

      double Dtemp0DT = getDYield0DT(T) + dh0dT*p;
      double DDtemp0DTT = getDDYield0DTT(T) + ddh0dTT*p;
      double DppDT = dh1dT*(p-_p1);
      double DDppDTT = ddh1dTT*(p-_p1);
      dRdT = Dtemp0DT*pow(pp,n1) + temp0*DppDT*n1*pow(pp,n1-1);
      ddRdTT = DDtemp0DTT*pow(pp,n1) + 2*Dtemp0DT*DppDT*n1*pow(pp,n1-1) + temp0*(DDppDTT*n1*pow(pp,n1-1)+DppDT*DppDT*n1*(n1-1)*pow(pp,n1-2));
    }
    else{
        double temp0 = R0 + h0*p;
        double temp1 = temp0*pow(1.+h1*(p-_p1), n1);
        double pp0 = 1.+h1*(p-_p1);
        double pp = 1.+h2*(p-_p2);
        R = temp1*pow(pp,n2);

        double Dtemp0 = h0;
        double Dtemp1 = Dtemp0*pow(1.+h1*(p-_p1), n1) + temp0*h1*n1*pow(1.+h1*(p-_p1), n1-1);
        double DDtemp1 = 2*Dtemp0*h1*n1*pow(1.+h1*(p-_p1), n1-1) + temp0*h1*h1*n1*(n1-1)*pow(1.+h1*(p-_p1), n1-2);
        dR = Dtemp1*pow(pp,n2) + temp1*h2*n2*pow(pp, n2-1);
        ddR = DDtemp1*pow(pp,n2) + 2*Dtemp1*h2*n2*pow(pp, n2-1) + temp1*h2*h2*n2*(n2-1)*pow(pp,n2-2);
        intR = ipvprev.getIntegR()+ 0.5*(R+ipvprev.getR())*(p-p0); // approximation

        double Dtemp0DT = getDYield0DT(T) + dh0dT*p;
        double DDtemp0DTT = getDDYield0DTT(T) + ddh0dTT*p;
        double Dpp0DT = dh1dT*(p-_p1);
        double DDpp0DTT = ddh1dTT*(p-_p1);
        double DppDT = dh2dT*(p-_p2);
        double DDppDTT = ddh2dTT*(p-_p2);
        double Dtemp1DT = Dtemp0DT*pow(pp0, n1) + temp0*Dpp0DT*n1*pow(pp0,n1-1);
        double DDtemp1DTT = DDtemp0DTT*pow(pp0,n1) + 2*Dtemp0DT*Dpp0DT*n1*pow(pp0,n1-1) + temp0*(DDpp0DTT*n1*pow(pp0,n1-1)+Dpp0DT*Dpp0DT*n1*(n1-1)*pow(pp0,n1-2));
        dRdT = Dtemp1DT*pow(pp,n2) + temp1*DppDT*n2*pow(pp,n2-1);
        ddRdTT = DDtemp1DTT*pow(pp,n2) + 2*Dtemp1DT*DppDT*n2*pow(pp,n2-1) + temp1*(DDppDTT*n2*pow(pp,n2-1)+DppDT*DppDT*n2*(n2-1)*pow(pp,n2-2));
    }
  }
  ipv.set(R0,R,dR,ddR,intR,dRdT,ddRdTT,ddRdT);
}

//----------------------------end
J2IsotropicHardening* LinearFollowedByMultipleSwiftJ2IsotropicHardening::clone() const
{
  return new LinearFollowedByMultipleSwiftJ2IsotropicHardening(*this);
}















/*LinearFollowedByMultiplePowerLawJ2IsotropicHardening1::LinearFollowedByMultiplePowerLawJ2IsotropicHardening1(const int num,  double yield0, double h, double p1, double n1, double p2, double n2, double p3, double n3,double p4, double n4):
  J2IsotropicHardening(num,yield0,true),_H(h),_p1(p1),_n1(n1),_p2(p2),_n2(n2),_p3(p3),_n3(n3),_p4(p4),_n4(n4){};

LinearFollowedByMultiplePowerLawJ2IsotropicHardening1::LinearFollowedByMultiplePowerLawJ2IsotropicHardening1(const LinearFollowedByMultiplePowerLawJ2IsotropicHardening1& src):
      J2IsotropicHardening(src),_H(src._H),_p1(src._p1),_n1(src._n1),_p2(src._p2),_n2(src._n2),_p3(src._p3),_n3(src._n3),_p4(src._p4),_n4(src._n4){}

LinearFollowedByMultiplePowerLawJ2IsotropicHardening1& LinearFollowedByMultiplePowerLawJ2IsotropicHardening1::operator =(const J2IsotropicHardening& src){
  J2IsotropicHardening::operator =(src);
  const LinearFollowedByMultiplePowerLawJ2IsotropicHardening1* psrc = dynamic_cast<const LinearFollowedByMultiplePowerLawJ2IsotropicHardening1*>(&src);
  if (psrc != NULL){
    _H = psrc->_H;
    _p1 = psrc->_p1;
    _n1 = psrc->_n1;
    _p2 = psrc->_p2;
    _n2 = psrc->_n2;
    _p3 = psrc->_p3;
    _n3 = psrc->_n3;
    _p4 = psrc->_p4;
    _n4 = psrc->_n4;
  }
  return *this;
}

LinearFollowedByMultiplePowerLawJ2IsotropicHardening1::~LinearFollowedByMultiplePowerLawJ2IsotropicHardening1(){};


void LinearFollowedByMultiplePowerLawJ2IsotropicHardening1::createIPVariable(IPJ2IsotropicHardening* &ipv) const
{
  if(ipv != NULL) delete ipv;
  ipv = new IPLinearFollowedByMultiplePowerLawJ2IsotropicHardening();
}

void LinearFollowedByMultiplePowerLawJ2IsotropicHardening1::hardening(double p0, const IPJ2IsotropicHardening &ipvprev, double p, IPJ2IsotropicHardening &ipv) const
{
  double dR=0., ddR=0., intR=0., dRdT = 0.;
  double R = getYield0();
  if (ipvprev.isSaturated()){
    R = ipvprev.getR();
    dR = 0.;
    ddR = 0.;
    intR = ipvprev.getIntegR()+ R*(p-p0);
  }
  else{

    double sy1 = getYield0() + _H*_p1;
    double sy2 = sy1*pow(_p2/_p1,_n1);
    double sy3 = sy2*pow(_p3/_p2,_n2);
    double sy4 = sy3*pow(_p4/_p3,_n3);
    if (p <= _p1){
      R =  getYield0()+_H*p;
      dR = _H;
      ddR = 0.;
      intR = getYield0()*p + 0.5*_H*p*p;
    }
    else if ( p > _p1 and p <= _p2){
      R = sy1*pow(p/_p1,_n1);
      dR = R*_n1/p;
      ddR = dR*_n1/p - R*_n1/(p*p);
      intR = getYield0()*_p1 + 0.5*_H*_p1*_p1 + R*p/(_n1+1) - sy1*_p1/(_n1+1);
    }
    else if ( p > _p2 and p <= _p3){
      R = sy2*pow(p/_p2,_n2);
      dR = R*_n2/p;
      ddR = dR*_n2/p - R*_n2/(p*p);
      intR = getYield0()*_p1 + 0.5*_H*_p1*_p1 + sy2*_p2/(_n1+1) - sy1*_p1/(_n1+1) + R*p/(_n2+1) - sy2*_p2/(_n2+1);
    }
    else if ( p > _p3 and p <= _p4){
      R = sy3*pow(p/_p3,_n3);
      dR = R*_n3/p;
      ddR = dR*_n3/p - R*_n3/(p*p);
      intR = getYield0()*_p1 + 0.5*_H*_p1*_p1 + sy2*_p2/(_n1+1) - sy1*_p1/(_n1+1) + R*p/(_n2+1) - sy2*_p2/(_n2+1) + R*p/(_n3+1) - sy3*_p3/(_n3+1);
    }
    else {
      R = sy4*pow(p/_p4,_n4);
      dR = R*_n4/p;
      ddR = dR*_n4/p - R*_n4/(p*p);
      intR = getYield0()*_p1 + 0.5*_H*_p1*_p1 + sy2*_p2/(_n1+1) - sy1*_p1/(_n1+1) + R*p/(_n2+1) - sy2*_p2/(_n2+1) + R*p/(_n3+1) - sy3*_p3/(_n3+1)+ R*p/(_n4+1) - sy4*_p4/(_n4+1);

    }
  }
  ipv.set(R,dR,ddR,intR,dRdT);
}
//-----------------------------------------------------added
void LinearFollowedByMultiplePowerLawJ2IsotropicHardening1::hardening(double p0, const IPJ2IsotropicHardening &ipvprev, double p,double T, IPJ2IsotropicHardening &ipv) const
{
  double dR=0., ddR=0., intR=0.,dRdT = 0.;
  double R = getYield0(T);
  if (ipvprev.isSaturated()){
    R = ipvprev.getR();
    dR = 0.;
    ddR = 0.;
    intR = ipvprev.getIntegR()+ R*(p-p0);
  }
  else{
    double sy1 = getYield0(T) + _H*_p1;
    double sy2 = sy1*pow(_p2/_p1,_n1);
    double sy3 = sy2*pow(_p3/_p2,_n2);
    double sy4 = sy3*pow(_p4/_p3,_n3);
    if (p <= _p1){
      R =  getYield0(T)+_H*p;
      dR = _H;
      ddR = 0.;
      intR = getYield0(T)*p + 0.5*_H*p*p;
      dRdT = getDYield0DT(T);
    }
    else if ( p > _p1 and p <= _p2){
      R = sy1*pow(p/_p1,_n1);
      dR = R*_n1/p;
      ddR = dR*_n1/p - R*_n1/(p*p);
      intR = getYield0(T)*_p1 + 0.5*_H*_p1*_p1 + R*p/(_n1+1) - sy1*_p1/(_n1+1);
      dRdT = getDYield0DT(T)*pow(p/_p1,_n1);
    }
    else if ( p > _p2 and p <= _p3){
      R = sy2*pow(p/_p2,_n2);
      dR = R*_n2/p;
      ddR = dR*_n2/p - R*_n2/(p*p);
      intR = getYield0(T)*_p1 + 0.5*_H*_p1*_p1 + sy2*_p2/(_n1+1) - sy1*_p1/(_n1+1) + R*p/(_n2+1) - sy2*_p2/(_n2+1);
      dRdT = getDYield0DT(T)*pow(p/_p2,_n2);
    }
    else if ( p > _p3 and p <= _p4){
      R = sy3*pow(p/_p3,_n3);
      dR = R*_n3/p;
      ddR = dR*_n3/p - R*_n3/(p*p);
      intR = getYield0(T)*_p1 + 0.5*_H*_p1*_p1 + sy2*_p2/(_n1+1) - sy1*_p1/(_n1+1) + R*p/(_n2+1) - sy2*_p2/(_n2+1) + R*p/(_n3+1) - sy3*_p3/(_n3+1);
      dRdT = getDYield0DT(T)*pow(p/_p3,_n3);
    }
    else {
      R = sy4*pow(p/_p4,_n4);
      dR = R*_n4/p;
      ddR = dR*_n4/p - R*_n4/(p*p);
      intR = getYield0(T)*_p1 + 0.5*_H*_p1*_p1 + sy2*_p2/(_n1+1) - sy1*_p1/(_n1+1) + R*p/(_n2+1) - sy2*_p2/(_n2+1) + R*p/(_n3+1) - sy3*_p3/(_n3+1) + R*p/(_n4+1) - sy4*_p4/(_n4+1);
      dRdT = getDYield0DT(T)*pow(p/_p4,_n4);
    }
  }

  ipv.set(R,dR,ddR,intR,dRdT);

}

//--------------------------------end
J2IsotropicHardening * LinearFollowedByMultiplePowerLawJ2IsotropicHardening1::clone() const
{
  return new LinearFollowedByMultiplePowerLawJ2IsotropicHardening1(*this);
}
*/


