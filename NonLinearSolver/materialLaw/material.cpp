#ifndef MATERIAL_CC
#define MATERIAL_CC 1

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
//#define pi 3.14159265

#include "material.h"
#include "FiniteStrain.h"
#include "Weight.h"

#ifdef NONLOCALGMSH
#include "matrix_operations.h"
#include "lccfunctions.h"
#include "elasticlcc.h"
#include "j2plast.h"
#include "mlawNonLocalDamage.h"
#include "homogenization.h"
#include "homogenization_2rd.h"
using std::max;
using namespace MFH;
#include "rotations.h"
#endif


//****in all of the constbox**New tenor added by wu ling: double* dpdE, double* strs_dDdp_bar  ****************
//*******c_g the characteristic length tensor, the value is returned by Clength_creat function***********


int Material::constboxSecantMixte(double* dstrn, double* strs_n,  double* strs, double* statev_n, double* statev, double **Calgo, double** Csd, double*** dCsd, double** dCsdp_bar, double* dnu, double& dnudp_bar, double alpha, double* dpdE, double* dstrsdp_bar, double** c_g, int kinc, int kstep, double dt, bool forceZero, bool forceRes)
{
  printf("Function constBoxSecant is not defined for this material\n");
  return (1);

//  if(forceRes)
//    return constboxSecant(dstrn, strs_n,  strs, statev_n, statev, Calgo, Csd, dCsd, dCsdp_bar, dnu, dnudp_bar, alpha, dpdE, dstrsdp_bar,c_g, kinc, kstep, dt);  
//  if(forceZero)
//    return constboxSecantZero(dstrn, strs_n,  strs, statev_n, statev, Calgo, Csd, dCsd, dCsdp_bar, dnu, dnudp_bar, alpha, dpdE, dstrsdp_bar,c_g, kinc, kstep, dt);  
//  else
//    return constboxSecant(dstrn, strs_n,  strs, statev_n, statev, Calgo, Csd, dCsd, dCsdp_bar, dnu, dnudp_bar, alpha, dpdE, dstrsdp_bar,c_g, kinc, kstep, dt);  
}

int Material::constboxLargD(double* dstrn, double* strs_n,  double* strs, double* statev_n, double* statev, double **Calgo, double** Csd, double*** dCsd, double** dCsdp_bar, double* dnu, double& dnudp_bar, double alpha, double* dpdE, double* dstrsdp_bar, double** c_g, int kinc, int kstep, double dt)
{
  printf("Function constBoxLargD is not defined for this material\n");
  return (1);
}

int Material::constbox_2order(int mtx, double *DE, double* dstrn, double* strs, double* statev_n, double* statev, double **Calgo, Lcc* LCC, YieldF* YF, double** c_g, int kinc, int kstep, double dt)
{
  printf("Function constBox_2order is not defined for this material\n");
  return (1);
}

int Material::constbox_2ndNtr(double *DE, double* dstrn, double* strs_n,  double* strs, double* statev_n, double* statev,  ELcc* LCC, EPR* epresult, double alpha, double** c_g, int kinc, int kstep, double dt)
{
  printf("Function constbox_2ndNtr is not defined for this material\n");
  return (1);
}

int Material::get_pos_eqstrs() const
{
  printf("Function get_pos_eqstrs() is not defined for this material\n");
  return -1;
}

int Material::get_pos_pstrn() const 
{
  printf("Function get_pos_pstrn() is not defined for this material\n");
  return -1;
}
int Material::get_pos_dp() const 
{
  printf("Function get_pos_dp() is not defined for this material\n");
  return -1;
}
int Material::get_pos_twomu() const 
{
  printf("Function get_pos_twomu() is not defined for this material\n");
  return -1;
}


bool Material::eqstrs_exist() const
{
    return false;
}
int Material::constboxSecantMixteGeneric(double* dstrn, double* strs_n,  double* strs, double* statev_n, double* statev, double **Calgo, double** Csd, double*** dCsd, double** dCsdp_bar, double* dnu, double& dnudp_bar, double alpha, double* dpdE, double* dstrsdp_bar, double** c_g, int kinc, int kstep, double dt, bool forceZero, bool forceRes,
double E, double nu, int htype, double sy0, double hexp, double hmod1, double hmod2, double hp0, double alpha_DP, double m_DP, double nup, int viscmodel, double vmod, double vstressy, double vexp, bool evp)
{
  return (1);
}      

#ifdef NONLOCALGMSH
int Material::get_pos_mtx_stress () const
{ 
   return -1;
}
int Material::get_pos_inc_stress () const
{
   return -1;
}
int Material::get_pos_mtx_strain () const
{ 
   return -1;
}
int Material::get_pos_inc_strain () const
{
   return -1;
}
int Material::get_pos_mtx_Ja() const
{
  return -1;
}
int Material::get_pos_inc_Ja() const
{
  return -1;
}
int Material::get_pos_mtx_Dam() const
{
  return -1;
}
int Material::get_pos_inc_Dam() const
{
  return -1;
}
void Material::get_elOp_mtx(double* statev, double** Cel)
{
  printf("Function get_elOp_mtx is not defined for this material\n");
}

void Material::get_elOp_icl(double* statev, double** Cel)
{
  printf("Function get_elOp_icl is not defined for this material\n");

}
void Material::get_elOp_mtx(double** Cel)
{
  printf("Function get_elOp_mtx is not defined for this material\n");
}

void Material::get_elOp_icl(double** Cel)
{
  printf("Function get_elOp_icl is not defined for this material\n");

}

double Material::get_vfI()
{
  printf("Function get_vfI is not defined for this material\n");
  return 0.;
}

void Material::setEuler(double e1, double e2, double e3)
{
  printf("Function setEuler is not defined for this material\n");
  //int a = 1;
}    

#endif




//remarks: For tangent M-T method, the state varibles keep the effective stress value, then this value is corrected by minus (D_stres_eff) in finit element
//         For first order secant M-T method, the state varibles keep the apparent stress value, then D_stres_eff is set zero as feedback for finite element. 

//**************************
//LINEAR ELASTICITY 
//PROPS: MODEL, iddam, E, nu
//STATEV: strn, strs, dstrn
//**************************
//constructor
EL_Material::EL_Material(double* props, int idmat):Material(props, idmat){
	
	int iddam;
        int idclength;

	iddam = (int) props[idmat+1];
        idclength=(int) props[idmat+2];
	E = props[idmat+3];
	nu = props[idmat+4];

	nsdv = 18;
	pos_strn=0;
	pos_strs=6;
	pos_dstrn=12;
        pos_Ja=nsdv;
	nsdv +=1;
        pos_F = nsdv;
        nsdv += 9;
        pos_Base = nsdv;
        nsdv += 9;
        pos_Ri=nsdv;
        nsdv +=9; 

	dam = init_damage(props,iddam);
	if(dam){
		pos_dam = nsdv;
                pos_p = pos_dam+4;
		nsdv += dam->get_nsdv();
	}

        clength = init_clength(props,idclength);
}

#ifdef NONLOCALGMSH
int EL_Material::get_pos_currentplasticstrainfornonlocal() const 
{
	if(dam)
	{
		return pos_p;
	}
	return -1;
}
int EL_Material::get_pos_effectiveplasticstrainfornonlocal() const
{
	if(dam)
	{
		return pos_dam;
	}
	return -1;
}

int EL_Material::get_pos_damagefornonlocal() const
{
	if(dam) return pos_dam+2;
        return -1;
}
double EL_Material::getElasticEnergy (double* statev) 
{
    double eE = 0.;
    int i;
    static double* strn = NULL;
    static double* strs = NULL;
    //static double**CelD = NULL;
    //double  D;
    if(strn == NULL)
    {
       mallocvector(&strn,6);
       mallocvector(&strs,6);
       //mallocmatrix(&CelD,6,6);
    }
    cleartens2(strn);
    cleartens2(strs);

    copyvect(&(statev[this->get_pos_strn()]), strn,6);
    //get_elOD(statev, CelD);
    //contraction42(CelD,strn,strs);
    copyvect(&statev[get_pos_strs()], strs, 6);   
    //neegry due to damage has been removed strn3 =eps12*sqrt2, strs3=sqrt2*strs12
    for (i =0; i< 6; i++) eE+=0.5*(strs[i]*strn[i]);

    return eE;

}

double EL_Material::getPlasticEnergy (double* statev) 
{
    double eP = 0.;
    return eP;
}

#endif



//destructor
EL_Material::~EL_Material(){
	
	if(dam) delete dam;	
        if(clength) delete clength;

}

//print
void EL_Material::print(){

	printf("Linear elasticity\n");
	printf("Young modulus: %lf, Poisson ratio: %lf\n", E, nu);
	if(dam){ dam->print();}
        if(clength){clength->print();}
	
}


//constbox
int EL_Material::constbox(double* dstrn, double* strs_n,  double* strs, double* statev_n, double* statev, double** Cref, double*** dCref, double* tau, double** dtau, double** Calgo, double alpha, double* dpdE, double* strs_dDdp_bar,double *Sp_bar, double** c_g, double* dFd_d_bar, double* dstrs_dFd_bar, double *dFd_dE, double** c_gF, double* dpdFd, double* dFddp, int kinc, int kstep, double dt){
  return (1);
}

//costboxSecant
int EL_Material::constboxSecantMixte(double* dstrn, double* strs_n,  double* strs, double* statev_n, double* statev, double **Calgo, double** Csd, double*** dCsd, double** dCsdp_bar, double* dnu, double& dnudp_bar, double alpha, double* dpdE, double* dstrsdp_bar, double** c_g, int kinc, int kstep, double dt, bool forceZero, bool forceRes){

  return (1);
}


int EL_Material::constboxLargD(double* dstrn, double* strs_n,  double* strs, double* statev_n, double* statev, double **Calgo, double** Csd, double*** dCsd, double** dCsdp_bar, double* dnu, double& dnudp_bar, double alpha, double* dpdE, double* dstrsdp_bar, double** c_g, int kinc, int kstep, double dt){
 return (1);
}

//constitutive box called by 2rd order method*******************
//for second-order method
//**************************************************************
int EL_Material::constbox_2order( int mtx, double *DE, double* dstrn, double* strs, double* statev_n, double* statev, 
double **Calgo, Lcc* LCC, YieldF* YF, double** c_g, int kinc, int kstep, double dt){

  return (1);
}





//reference operator from statev or material properties 
// = Hooke's operator in any case
void EL_Material::get_refOp(double* statev, double** C, int kinc, int kstep){

	get_elOp(C);
}

void EL_Material::get_elOp(double** Cel){

	compute_Hooke(E,nu,Cel);
}
void EL_Material::get_elOD(double* statev, double** Cel)
{
     get_elOp(Cel);
     if(dam)
     {
       double damage = statev[get_pos_dam()];
       for(int i=0; i < 6; i++)
         for(int j=0; j < 6; j++)
           Cel[i][j]*=1.-damage;
    }
}


void EL_Material::unload_step(double* dstrn, double* strs_n, double* statev_n, int kinc, int kstep){

       printf("unload_step not implemented for EL material\n");
}

//*****************************************************
//RATE-INDEPENDENT ELASTO-PLASTICITY
//PROPS: MODEL, iddam, E, nu, sy0, htype, hmod1, (hmod2, hp0), hexp 
//STATEV: strn, strs, dstrn, pstrn, p, dp, 2*mu_iso
//*****************************************************
//constructor
EP_Material::EP_Material(double* props, int idmat):Material(props, idmat){
	int k;
	int iddam, idclength;
	

	iddam = (int)props[idmat+1];
        idclength = (int)props[idmat+2];

	k=idmat+3;          
	E = props[k++];
	nu = props[k++];
	sy0 = props[k++];
	htype = (int) props[k++];
	hmod1 = props[k++];

	//hardening models with two hardening moduli
	if(htype==H_LINEXP){
		hmod2 = props[k++];
                hp0 = 0.;
        }
        else if(htype==H_POW_EXP){
                hmod2 = props[k++];
                hp0 = 0.;
        }      
        else if(htype==H_POW_EXTRAPOL){
		hmod2 = props[k++];
                hp0   = props[k++];
	}
	else{
		hmod2 = 0.;
                hp0   = 0.;
	}
	hexp = props[k];

	nsdv = 28;
	pos_strn=0;
	pos_strs=6;
	pos_dstrn=12;
	pos_pstrn=18;
	pos_p = 24;
	pos_dp = 25;
	pos_twomu = 26;
	pos_eqstrs = 27;
        pos_F = nsdv;
        nsdv += 9;
        pos_Base = nsdv;
        nsdv += 9;
        pos_Ja=nsdv;
        nsdv += 1;
        pos_Ri = nsdv;
        nsdv += 9; 

	dam = init_damage(props,iddam);
	if(dam){
		pos_dam = nsdv;
		nsdv += dam->get_nsdv();
	}

        clength = init_clength(props,idclength);

}


#ifdef NONLOCALGMSH
int EP_Material::get_pos_currentplasticstrainfornonlocal() const 
{
	return pos_p;
}
int EP_Material::get_pos_effectiveplasticstrainfornonlocal() const
{
	if(dam)
	{
		return pos_dam;
	}
	return -1;
}

int EP_Material::get_pos_damagefornonlocal() const
{
	if(dam) return pos_dam+2;
        return -1;
}
double EP_Material::getElasticEnergy (double* statev)
{
  double eE = 0.;
  int i;
  static double* strn = NULL;
  static double* pstrn = NULL;
  static double* strs = NULL;
  //static double**CelD = NULL;
  if(strn == NULL)
  {
    mallocvector(&strn,6);
    mallocvector(&pstrn,6);
    mallocvector(&strs,6);
   // mallocmatrix(&CelD,6,6);
  }
  cleartens2(strn);
  cleartens2(pstrn);
  cleartens2(strs);

  copyvect(&(statev[pos_strn]), strn,6);
  copyvect(&(statev[pos_pstrn]),pstrn,6);
  copyvect(&statev[pos_strs], strs, 6);
  //enegry due to damage has been removed
  for (i =0; i< 6; i++) eE+=0.5*(strs[i]*(strn[i]-pstrn[i]));

  return eE;
}

double EP_Material::getPlasticEnergy (double* statev) 
{
  double eP = 0.;
  double p  = statev[get_pos_currentplasticstrainfornonlocal()];
  eP = sy0*p;
  switch(htype) {
    //POWER LAW HARDENING
    case H_POW:
      if(fabs(hexp+1.)!=0) 
         eP+=hmod1*pow(p,hexp+1.)/(hexp+1.);
      break;
    //EXPONENTIAL HARDENING
    case H_EXP:
      eP+=hmod1*p;
      if(fabs(hexp)!=0) eP+=hmod1*(exp(-hexp*p)-exp(0))/(hexp);
      break;
    //SWIFT LAW
    case H_SWIFT:
      eP-=sy0*p;
      if(fabs(hexp+1.)!=0 && fabs(hmod1)!=0.) eP+=sy0*pow(1.+hmod1*p,hexp+1.)/(hexp+1.)/hmod1;
      break;
    //LINEAR-EXPONENTIAL HARDENING
    case H_LINEXP:
      eP+=hmod1*p*p/2.+hmod2*p;
      if(fabs(hexp)!=0) eP+=hmod2*(exp(-hexp*p)-exp(0))/(hexp);
      break;
    //POWER LAW HARDENING EXTRAPOLATED AFTER 16% DEFO TO MIMIC DIGIMAT TO ABAQUS
    case H_POW_EXTRAPOL:
      if(p<hp0)
      {
        if(fabs(hexp+1.)!=0)
          eP+=hmod1*pow(p,hexp+1.)/(hexp+1.);
      }
      else if(p<10.*hp0)
      {
        if(fabs(hexp+1.)!=0)
          eP+=hmod1*pow(hp0,hexp+1.)/(hexp+1.);
        eP+=hmod1*pow(hp0,hexp+1.)*(p-hp0)+hmod2*(p-hp0)*(p-hp0)/2.;
      }
      else
      {
        if(fabs(hexp+1.)!=0)
          eP+=hmod1*pow(hp0,hexp+1.)/(hexp+1.);
        eP+=hmod1*pow(hp0,hexp+1.)*(10.*hp0-hp0)+hmod2*(10.*hp0-hp0)*(10.*hp0-hp0)/2.;
        eP+=(hmod1*pow(hp0,hexp+1.)+hmod2*(10.*hp0-hp0))*(p-10.*hp0)+hmod2*(p-10.*hp0)*(p-10.*hp0)/2000.;
      }
      break;
    default:
      printf("Bad choice of hardening law in j2hard: %d\n",htype);
      break;
  }
  return eP;
}
#endif

//destructor
EP_Material::~EP_Material(){
	if(dam) delete dam;
        if(clength) delete clength;
}

//print
void EP_Material::print(){

	printf("J2 elasto-plasticity\n");
	printf("Young modulus: %lf, Poisson ratio: %lf\n", E, nu);
	printf("Intial yield stress: %lf, hardening law: %d, hardening coefficients: %lf\t%lf\t%lf\t%lf\n", sy0, htype,hmod1,hmod2,hp0,hexp);
	if(dam){ dam->print();}
        if(clength){clength->print();}
}

//constbox      //*****************New tenor added by wu ling: double* dpdE, double* strs_dDdp_bar ****************
int EP_Material::constbox(double* dstrn, double* strs_n,  double* strs, double* statev_n, double* statev, double** Cref, double*** dCref, double* tau, double** dtau, double** Calgo, double alpha, double* dpdE, double* strs_dDdp_bar,double *Sp_bar, double** c_g, double* dFd_d_bar, double* dstrs_dFd_bar, double *dFd_dE, double** c_gF, double* dpdFd, double* dFddp, int kinc, int kstep, double dt){

 return (1);
}
//******************************************************
//costboxSecant
//****************************************************
int EP_Material::constboxSecantMixte(double* dstrn, double* strs_n,  double* strs, double* statev_n, double* statev, double **Calgo, double** Csd, double*** dCsd, double** dCsdp_bar, double* dnu, double& dnudp_bar, double alpha, double* dpdE, double* dstrsdp_bar, double** c_g, int kinc, int kstep, double dt, bool forceZero, bool forceRes){
  return (1);
}

//end of costboxSecant

//**************************************************************
// EP_Material::constbox for large deformation 
//*************************************************************************************************************
int EP_Material::constboxLargD(double* dstrn, double* strs_n,  double* strs, double* statev_n, double* statev, double **Calgo, double** Csd, double*** dCsd, double** dCsdp_bar, 
double* dnu, double &dnudp_bar, double alpha, double* dpdE, double* dstrsdp_bar, double** c_g, int kinc, int kstep, double dt){

  return (1);
}
//end of constbox large deformation

//****************************************************
// consbox called by MTSecNtr 2rd order method
//****************************************************

int EP_Material::constbox_2ndNtr(double *DE, double* dstrn, double* strs_n,  double* strs, double* statev_n, double* statev,  ELcc* LCC, EPR* epresult, double alpha, double** c_g, int kinc, int kstep, double dt){


 return (1);           
}
// end of constbox_2ndNtr()


//****************************************************
// consbox called by 2rd order method
//****************************************************

int EP_Material::constbox_2order(int mtx, double *DE, double* dstrn, double* strs, double* statev_n, double* statev,  double **Calgo, Lcc* LCC, YieldF* YF, double** c_g, int kinc, int kstep, double dt){


  return (1);
}

//end of consbox_2order

//****************************************************
//****************************************************

void EP_Material::get_refOp(double* statev, double** C, int kinc, int kstep){

	
	double twomu, threekappa, D;

	if(kinc<2 && kstep==1){
	   get_elOp(C);
	}
	//if(kinc<2 ){
                //get_elOD(statev,C);
	//}
	else{
                twomu = statev[pos_twomu];
		threekappa = E/(1.-2.*nu); 


                if(dam){                                           //*********add by wu ling 
                        D = statev[pos_dam+2];                     //***********13.04.2011********
		        twomu = twomu*(1-D);
		        threekappa = threekappa*(1-D);
                 }
		makeiso(threekappa/3.,twomu/2.,C);
	}

}

void EP_Material::get_elOD(double* statev, double** Cel)
{
     get_elOp(Cel);
     if(dam)
     {
       double damage = statev[pos_dam+2];
       for(int i=0; i < 6; i++)
         for(int j=0; j < 6; j++)
           Cel[i][j]*=1.-damage;
    }
}


void EP_Material::get_elOp(double** Cel){

	compute_Hooke(E,nu,Cel);
}

void EP_Material::unload_step(double* dstrn,double* strs_n, double* statev_n, int kinc, int kstep){

       if(kinc == 1 && kstep==1){
		return;
	}

	int i,error;
	static double **C0=NULL;
        static double **invC0=NULL;
	static double *strs=NULL;
        static double *udstrn=NULL;
        if(C0==NULL)
        {
  	  mallocmatrix(&C0,6,6);
	  mallocmatrix(&invC0,6,6);	
          mallocvector(&strs,6);
          mallocvector(&udstrn,6);
        }
        cleartens4(C0);
        cleartens4(invC0);
        cleartens2(strs);
        cleartens2(udstrn);



	get_elOD(statev_n, C0);
	inverse(C0,invC0,&error,6);
	if(error!=0) printf("problem while inversing C0 in unload_step of MTSecF\n");

	copyvect(&statev_n[pos_strs], strs, 6);        //stress in at tn
        contraction42(invC0, strs, udstrn);          //unloading strain of composite at tn
       
 // update statev_n to keep the imformation for residual condition 

        for(i=0;i<6;i++){
	strs_n[i]=0.0;	
        statev_n[pos_strs +i]=0.0;                                   //residual stress is 0
        statev_n[pos_strn +i]= statev_n[pos_strn +i]- udstrn[i];      //residual strain 
        statev_n[pos_dstrn +i]= dstrn[i]+ udstrn[i];    // strain increment in composte

        dstrn[i]= statev_n[pos_dstrn +i];
      	}

}



//*******************  Stochastic  EP  *************************
//Stochastic RATE-INDEPENDENT ELASTO-PLASTICITY
//PROPS: MODEL, iddam, E, nu, sy0, htype, hmod1, (hmod2, hp0), hexp 
//STATEV: strn, strs, dstrn, pstrn, p, dp, 2*mu_iso
//**************************************************************
//constructor
EP_Stoch_Material::EP_Stoch_Material(double* props, int idmat):Material(props, idmat){
	int k;
	int iddam, idclength, Flag_stoch ;	

	iddam = (int)props[idmat+1];
        idclength = (int)props[idmat+2];

	k=idmat+3;          
	_E = props[k++];
	_nu = props[k++];
	_sy0 = props[k++];
	htype = (int) props[k++];
	_hmod1 = props[k++];

	//hardening models with two hardening moduli
	if(htype==H_LINEXP)
        {
		_hmod2 = props[k++];
                _hp0 = 0.;
        }
        else if(htype==H_POW_EXTRAPOL){
		_hmod2 = props[k++];
                _hp0   = props[k++];
	}
	else{
		_hmod2 = 0.;
                _hp0   = 0.;
	}
	_hexp = props[k];

	nsdv = 28;
	pos_strn=0;
	pos_strs=6;
	pos_dstrn=12;
	pos_pstrn=18;
	pos_p = 24;
	pos_dp = 25;
	pos_twomu = 26;
	pos_eqstrs = 27;
        
        if(_E < 1.0e-6){
            pos_E = nsdv;
            nsdv += 1;}
        else{ pos_E = 0; }

        if(_nu < 1.0e-6){
            pos_nu = nsdv;
            nsdv += 1;}
        else{ pos_nu = 0; }

        if(_sy0 < 1.0e-6){
            pos_sy0 = nsdv;
            nsdv += 1;}
        else{ pos_sy0 = 0; }
   
        if(_hmod1 < 1.0e-6){
            pos_hmod1 = nsdv;
            nsdv += 1; }
        else{ pos_hmod1 = 0;}

        if((htype==H_LINEXP || htype==H_POW_EXTRAPOL) && (_hmod2 < 1.0e-6)){
            pos_hmod2 = nsdv;
            nsdv += 1; 
        }
        else{ pos_hmod2 = 0;}

        if((htype==H_POW_EXTRAPOL) && (_hp0 < 1.0e-6)){
            pos_hp0 = nsdv;
            nsdv += 1; 
        }
        else{ pos_hp0 = 0;}


        if(_hexp < 1.0e-6){
            pos_hexp = nsdv;
            nsdv += 1; }
        else{ pos_hexp = 0;}

	dam = init_damage(props,iddam);
	if(dam){
  	        pos_dam = nsdv;
		nsdv += dam->get_nsdv();
                Flag_stoch = dam->get_pos_DamParm1();

                if(Flag_stoch > 0){
                    pos_DamParm1 = pos_dam + dam->get_pos_DamParm1();
                    pos_DamParm2 = pos_dam + dam->get_pos_DamParm2();
                }
                else{
                    pos_DamParm1 = 0;
                    pos_DamParm2 = 0;
                }
	}
        else{
            pos_DamParm1 = 0;
            pos_DamParm2 = 0;
        }

        clength = init_clength(props,idclength);

}


bool EP_Stoch_Material::eqstrs_exist() const
{
        return true;
}


#ifdef NONLOCALGMSH
int EP_Stoch_Material::get_pos_currentplasticstrainfornonlocal() const 
{
	return pos_p;
}
int EP_Stoch_Material::get_pos_effectiveplasticstrainfornonlocal() const
{
	if(dam)
	{
		return pos_dam;
	}
	return -1;
}

int EP_Stoch_Material::get_pos_damagefornonlocal() const
{
	if(dam) return pos_dam+2;
        return -1;
}
double EP_Stoch_Material::getElasticEnergy (double* statev)
{
  double eE = 0.;
  int i;
  static double* strn = NULL;
  static double* pstrn = NULL;
  static double* strs = NULL;
  //static double**CelD = NULL;
  if(strn == NULL)
  {
    mallocvector(&strn,6);
    mallocvector(&pstrn,6);
    mallocvector(&strs,6);
  }
  cleartens2(strn);
  cleartens2(pstrn);
  cleartens2(strs);

  copyvect(&(statev[pos_strn]), strn,6);
  copyvect(&(statev[pos_pstrn]),pstrn,6);
  copyvect(&statev[pos_strs], strs, 6);
  //enegry due to damage has been removed
  for (i =0; i< 6; i++) eE+=0.5*(strs[i]*(strn[i]-pstrn[i]));

  return eE;
}

double EP_Stoch_Material::getPlasticEnergy (double* statev) 
{
  double eP = 0.;
  double p = statev[get_pos_currentplasticstrainfornonlocal()];
  double sy0, hexp, hmod1, hmod2, hp0;
  if(pos_sy0 != 0) sy0 = statev[pos_sy0];
  else sy0 = _sy0;

  if(pos_hexp != 0) hexp = statev[pos_hexp];
  else hexp = _hexp;

  if(pos_hmod1 != 0) hmod1 = statev[pos_hmod1];
  else hmod1 = _hmod1;
 
  if(pos_hmod2 != 0) hmod2 = statev[pos_hmod2];
  else hmod2 = _hmod2;

  if(pos_hp0 != 0) hp0 = statev[pos_hp0];
  else hp0 = _hp0;

  eP = sy0*p;
  switch(htype) {
    //POWER LAW HARDENING
    case H_POW:
      if(fabs(hexp+1.)!=0) 
         eP+= hmod1*pow(p,hexp+1.)/(hexp+1.);
      break;
    //EXPONENTIAL HARDENING
    case H_EXP:
      eP+=hmod1*p;
      if(fabs(hexp)!=0) eP+=hmod1*(exp(-hexp*p)-exp(0))/(hexp);
      break;
    //SWIFT LAW
    case H_SWIFT:
      eP-=sy0*p;
      if(fabs(hexp+1.)!=0 && fabs(hmod1)!=0.) eP+=sy0*pow(1.+hmod1*p,hexp+1.)/(hexp+1.)/hmod1;
      break;
    //LINEAR-EXPONENTIAL HARDENING
    case H_LINEXP:
      eP+=hmod1*p*p/2.+hmod2*p;
      if(fabs(hexp)!=0) eP+=hmod2*(exp(-hexp*p)-exp(0))/(hexp);
      break;
    //POWER LAW HARDENING EXTRAPOLATED AFTER 16% DEFO TO MIMIC DIGIMAT TO ABAQUS
    case H_POW_EXTRAPOL:
      if(p<hp0)
      {
        if(fabs(hexp+1.)!=0)
          eP+=hmod1*pow(p,hexp+1.)/(hexp+1.);
      }
      else if(p<10.*hp0)
      {
        if(fabs(hexp+1.)!=0)
          eP+=hmod1*pow(hp0,hexp+1.)/(hexp+1.);
        eP+=hmod1*pow(hp0,hexp+1.)*(p-hp0)+hmod2*(p-hp0)*(p-hp0)/2.;
      }
      else
      {
        if(fabs(hexp+1.)!=0)
          eP+=hmod1*pow(hp0,hexp+1.)/(hexp+1.);
        eP+=hmod1*pow(hp0,hexp+1.)*(10.*hp0-hp0)+hmod2*(10.*hp0-hp0)*(10.*hp0-hp0)/2.;
        eP+=(hmod1*pow(hp0,hexp+1.)+hmod2*(10.*hp0-hp0))*(p-10.*hp0)+hmod2*(p-10.*hp0)*(p-10.*hp0)/2000.;
      }
      break;
    default:
      printf("Bad choice of hardening law in j2hard: %d\n",htype);
      break;
  }
  return eP;
}
#endif

//destructor
EP_Stoch_Material::~EP_Stoch_Material(){
	if(dam) delete dam;
        if(clength) delete clength;
}

//print
void EP_Stoch_Material::print(){

	printf("Stochastic J2 elasto-plasticity\n");
	printf("Young modulus: %lf, Poisson ratio: %lf\n", _E, _nu);
	printf("Intial yield stress: %lf, hardening law: %d, hardening coefficients: %lf\t%lf\t%lf\t%lf\n", _sy0,
                                                                               htype,_hmod1,_hmod2,_hp0,_hexp);
	if(dam){ dam->print();}
        if(clength){clength->print();}
}

//constbox      //*****************New tenor added by wu ling: double* dpdE, double* strs_dDdp_bar ****************
int EP_Stoch_Material::constbox(double* dstrn, double* strs_n,  double* strs, double* statev_n, double* statev, double** Cref, double*** dCref, double* tau, double** dtau, double** Calgo, double alpha, double* dpdE, double* strs_dDdp_bar,double *Sp_bar, double** c_g, double* dFd_d_bar, double* dstrs_dFd_bar, double *dFd_dE, double** c_gF, double* dpdFd, double* dFddp, int kinc, int kstep, double dt){

  return (1);
}
//******************************************************
//costboxSecant
//****************************************************
int EP_Stoch_Material::constboxSecantMixte(double* dstrn, double* strs_n,  double* strs, double* statev_n, double*
statev, double **Calgo, double** Csd, double*** dCsd, double** dCsdp_bar, double* dnu, double& dnudp_bar, double alpha, double* dpdE, double* dstrsdp_bar, double** c_g, int kinc, int kstep, double dt, bool forceZero, bool forceRes)
{
  return (1);

}
//end of costboxSecant


//****************************************************
// consbox called by MTSecNtr 2rd order method
//****************************************************

int EP_Stoch_Material::constbox_2ndNtr(double *DE, double* dstrn, double* strs_n,  double* strs, double* statev_n, double* statev,  ELcc* LCC, EPR* epresult, double alpha, double** c_g, int kinc, int kstep, double dt){


 return (1);          
}
// end of constbox_2ndNtr()



//****************************************************
//****************************************************

void EP_Stoch_Material::get_refOp(double* statev, double** C, int kinc, int kstep){
	
	double twomu, threekappa, D;
        double E, nu;

        if(pos_E != 0) E = statev[pos_E];
        else E = _E;

        if(pos_nu != 0) nu = statev[pos_nu];
        else nu = _nu;

	if(kinc<2 && kstep==1){
	   get_elOp(statev, C);
	}
	//if(kinc<2 ){
                //get_elOD(statev,C);
	//}
	else{
                twomu = statev[pos_twomu];
		threekappa = E/(1.-2.*nu); 


                if(dam){                                           //*********add by wu ling 
                        D = statev[pos_dam+2];                     //***********13.04.2011********
		        twomu = twomu*(1-D);
		        threekappa = threekappa*(1-D);
                 }
		makeiso(threekappa/3.,twomu/2.,C);
	}

}

void EP_Stoch_Material::get_elOD(double* statev, double** Cel)
{
     get_elOp(statev, Cel);
     if(dam)
     {
       double damage = statev[pos_dam+2];
       for(int i=0; i < 6; i++)
         for(int j=0; j < 6; j++)
           Cel[i][j]*=1.-damage;
    }
}


void EP_Stoch_Material::get_elOp(double* statev, double** Cel){

        double E, nu;
        if(pos_E != 0) E = statev[pos_E];
        else E = _E;

        if(pos_nu != 0) nu = statev[pos_nu];
        else nu = _nu;

	compute_Hooke(E,nu,Cel);
}

void  EP_Stoch_Material::get_elOp(double** Cel) {
	compute_Hooke(_E,_nu,Cel);
}

void EP_Stoch_Material::unload_step(double* dstrn,double* strs_n, double* statev_n, int kinc, int kstep){

       if(kinc == 1 && kstep==1){
		return;
	}

	int i,error;
	static double **C0=NULL;
        static double **invC0=NULL;
	static double *strs=NULL;
        static double *udstrn=NULL;
        if(C0==NULL)
        {
  	  mallocmatrix(&C0,6,6);
	  mallocmatrix(&invC0,6,6);	
          mallocvector(&strs,6);
          mallocvector(&udstrn,6);
        }
        cleartens4(C0);
        cleartens4(invC0);
        cleartens2(strs);
        cleartens2(udstrn);



	get_elOD(statev_n, C0);
	inverse(C0,invC0,&error,6);
	if(error!=0) printf("problem while inversing C0 in unload_step of MTSecF\n");

	copyvect(&statev_n[pos_strs], strs, 6);        //stress in at tn
        contraction42(invC0, strs, udstrn);          //unloading strain of composite at tn
       
 // update statev_n to keep the imformation for residual condition 

        for(i=0;i<6;i++){
	strs_n[i]=0.0;	
        statev_n[pos_strs +i]=0.0;                                   //residual stress is 0
        statev_n[pos_strn +i]= statev_n[pos_strn +i]- udstrn[i];      //residual strain 
        statev_n[pos_dstrn +i]= dstrn[i]+ udstrn[i];    // strain increment in composte

        dstrn[i]= statev_n[pos_dstrn +i];
      	}


}


//****************************************************************************************
//**** RATE-DEPENDENT elasto-viscoplastic material ************************************
//PROPS: MODEL, iddam, E, nu, sy0, htype, hmod1, (hmod2, hp0), hexp, viscmodel, vmod, vexp 
//STATEV: strn, strs, dstrn, pstrn, p, dp, 2*mu_iso
//****************************************************************************************
//constructor
EVP_Material::EVP_Material(double* props, int idmat):Material(props, idmat){
	int k;
	int iddam, idclength;
	

	iddam = (int)props[idmat+1];
        idclength = (int)props[idmat+2];

	k=idmat+3;          
	E = props[k++];
	nu = props[k++];
	sy0 = props[k++];
	htype = (int) props[k++];
	hmod1 = props[k++];

	//hardening models with two hardening moduli
	if(htype==H_LINEXP) {
		hmod2 = props[k++];
                hp0 = 0.;
        }
        else if(htype==H_POW_EXTRAPOL){
		hmod2 = props[k++];
                hp0   = props[k++];
	}
	else{
		hmod2 = 0.;
                hp0   = 0.;
	}
	hexp = props[k++];

        viscmodel = (int) props[k++];
        vmod = props[k++];
        vexp = props[k++];
        if(viscmodel == V_POWER_NO_HARDENING){
                vstressy = props[k];
        }
        else{
                vstressy = 0.0;
        }
      
	nsdv = 28;
	pos_strn=0;
	pos_strs=6;
	pos_dstrn=12;
	pos_pstrn=18;
	pos_p = 24;
	pos_dp = 25;
	pos_twomu = 26;
	pos_eqstrs = 27;
        pos_F = nsdv;
        nsdv += 9;
        pos_Base = nsdv;
        nsdv += 9;
        pos_Ja=nsdv;
        nsdv += 1;
        pos_Ri = nsdv;
        nsdv += 9; 

	dam = init_damage(props,iddam);
	if(dam){
		pos_dam = nsdv;
		nsdv += dam->get_nsdv();
	}

        clength = init_clength(props,idclength);

}


bool EVP_Material::eqstrs_exist() const
{
        return true;
}


#ifdef NONLOCALGMSH
int EVP_Material::get_pos_currentplasticstrainfornonlocal() const 
{
	return pos_p;
}
int EVP_Material::get_pos_effectiveplasticstrainfornonlocal() const
{
	if(dam)
	{
		return pos_dam;
	}
	return -1;
}

int EVP_Material::get_pos_damagefornonlocal() const
{
	if(dam) return pos_dam+2;
        return -1;
}
double EVP_Material::getElasticEnergy (double* statev)
{
  double eE = 0.;
  int i;
  static double* strn = NULL;
  static double* pstrn = NULL;
  static double* strs = NULL;
  //static double**CelD = NULL;
  if(strn == NULL)
  {
    mallocvector(&strn,6);
    mallocvector(&pstrn,6);
    mallocvector(&strs,6);
   // mallocmatrix(&CelD,6,6);
  }
  cleartens2(strn);
  cleartens2(pstrn);
  cleartens2(strs);

  copyvect(&(statev[pos_strn]), strn,6);
  copyvect(&(statev[pos_pstrn]),pstrn,6);
  copyvect(&statev[pos_strs], strs, 6);
  //enegry due to damage has been removed
  for (i =0; i< 6; i++) eE+=0.5*(strs[i]*(strn[i]-pstrn[i]));

  return eE;
}

double EVP_Material::getPlasticEnergy (double* statev) 
{
  double eP = 0.;
  double p  = statev[get_pos_currentplasticstrainfornonlocal()];
  eP = sy0*p;
  switch(htype) {
    //POWER LAW HARDENING
    case H_POW:
      if(fabs(hexp+1.)!=0) 
         eP+=hmod1*pow(p,hexp+1.)/(hexp+1.);
      break;
    //EXPONENTIAL HARDENING
    case H_EXP:
      eP+=hmod1*p;
      if(fabs(hexp)!=0) eP+=hmod1*(exp(-hexp*p)-exp(0))/(hexp);
      break;
    //SWIFT LAW
    case H_SWIFT:
      eP-=sy0*p;
      if(fabs(hexp+1.)!=0 && fabs(hmod1)!=0.) eP+=sy0*pow(1.+hmod1*p,hexp+1.)/(hexp+1.)/hmod1;
      break;
    //LINEAR-EXPONENTIAL HARDENING
    case H_LINEXP:
      eP+=hmod1*p*p/2.+hmod2*p;
      if(fabs(hexp)!=0) eP+=hmod2*(exp(-hexp*p)-exp(0))/(hexp);
      break;
    //POWER LAW HARDENING EXTRAPOLATED AFTER 16% DEFO TO MIMIC DIGIMAT TO ABAQUS
    case H_POW_EXTRAPOL:
      if(p<hp0)
      {
        if(fabs(hexp+1.)!=0)
          eP+=hmod1*pow(p,hexp+1.)/(hexp+1.);
      }
      else if(p<10.*hp0)
      {
        if(fabs(hexp+1.)!=0)
          eP+=hmod1*pow(hp0,hexp+1.)/(hexp+1.);
        eP+=hmod1*pow(hp0,hexp+1.)*(p-hp0)+hmod2*(p-hp0)*(p-hp0)/2.;
      }
      else
      {
        if(fabs(hexp+1.)!=0)
          eP+=hmod1*pow(hp0,hexp+1.)/(hexp+1.);
        eP+=hmod1*pow(hp0,hexp+1.)*(10.*hp0-hp0)+hmod2*(10.*hp0-hp0)*(10.*hp0-hp0)/2.;
        eP+=(hmod1*pow(hp0,hexp+1.)+hmod2*(10.*hp0-hp0))*(p-10.*hp0)+hmod2*(p-10.*hp0)*(p-10.*hp0)/2000.;
      }
      break;
    default:
      printf("Bad choice of hardening law in j2hard: %d\n",htype);
      break;
  }
  return eP;
}
#endif

//destructor
EVP_Material::~EVP_Material(){
	if(dam) delete dam;
        if(clength) delete clength;
}

//print
void EVP_Material::print(){

	printf("J2 elasto-ViscoPlasticity\n");
	printf("Young modulus: %lf, Poisson ratio: %lf\n", E, nu);
	printf("Intial yield stress: %lf, hardening law: %d, hardening coefficients: %lf\t%lf\t%lf\t%lf\t%lf\n", sy0, htype,hmod1,hmod2,hp0, hp0,hexp);
	printf("ViscoPlastic function: %d, ViscoPlastic coefficients: %lf\t%lf\n", viscmodel, vmod, vexp);
        if(dam){ dam->print();}
        if(clength){clength->print();}
}

//constbox      //*****************New tenor added by wu ling: double* dpdE, double* strs_dDdp_bar ****************
int EVP_Material::constbox(double* dstrn, double* strs_n,  double* strs, double* statev_n, double* statev, double** Cref, double*** dCref, double* tau, double** dtau, double** Calgo, double alpha, double* dpdE, double* strs_dDdp_bar,double *Sp_bar, double** c_g, double* dFd_d_bar, double* dstrs_dFd_bar, double *dFd_dE, double** c_gF, double* dpdFd, double* dFddp, int kinc, int kstep, double dt){

  return (1);
}
//******************************************************
//costboxSecant
//****************************************************
int EVP_Material::constboxSecantMixte(double* dstrn, double* strs_n,  double* strs, double* statev_n, double*
statev, double **Calgo, double** Csd, double*** dCsd, double** dCsdp_bar, double* dnu, double& dnudp_bar, double alpha, double* dpdE, double* dstrsdp_bar, double** c_g, int kinc, int kstep, double dt, bool forceZero, bool forceRes)
{
  return (1);
}
//end of costboxSecant

//****************************************************
// consbox called by MTSecNtr 2rd order method
//****************************************************

int EVP_Material::constbox_2ndNtr(double *DE, double* dstrn, double* strs_n,  double* strs, double* statev_n, double* statev,  ELcc* LCC, EPR* epresult, double alpha, double** c_g, int kinc, int kstep, double dt){


  return (1);            
}
// end of constbox_2ndNtr()


//****************************************************
//    For large deformation
// consbox called by MTSecF 1rd order method
//****************************************************

int EVP_Material::constboxLargD(double* dstrn, double* strs_n,  double* strs, double* statev_n, double* statev, double **Calgo, double** Csd, double*** dCsd, double** dCsdp_bar, 
double* dnu, double &dnudp_bar, double alpha, double* dpdE, double* dstrsdp_bar, double** c_g, int kinc, int kstep, double dt){

  return (1);              
}
// end of constbox_LargD()

//****************************************************
// consbox called by 2rd order method
//****************************************************

int EVP_Material::constbox_2order(int mtx, double *DE, double* dstrn, double* strs, double* statev_n, double* statev,  double **Calgo, Lcc* LCC, YieldF* YF, double** c_g, int kinc, int kstep, double dt){

 return (1);
}

//end of consbox_2order


//****************************************************
//****************************************************

void EVP_Material::get_refOp(double* statev, double** C, int kinc, int kstep){

	
	double twomu, threekappa, D;

	if(kinc<2 && kstep==1){
	   get_elOp(C);
	}
	//if(kinc<2 ){
                //get_elOD(statev,C);
	//}
	else{
                twomu = statev[pos_twomu];
		threekappa = E/(1.-2.*nu); 


                if(dam){                                           
                        D = statev[pos_dam+2];           
		        twomu = twomu*(1-D);
		        threekappa = threekappa*(1-D);
                 }
		makeiso(threekappa/3.,twomu/2.,C);
	}

}

void EVP_Material::get_elOD(double* statev, double** Cel)
{
     get_elOp(Cel);
     if(dam)
     {
       double damage = statev[pos_dam+2];
       for(int i=0; i < 6; i++)
         for(int j=0; j < 6; j++)
           Cel[i][j]*=1.-damage;
    }
}


void EVP_Material::get_elOp(double** Cel){

	compute_Hooke(E,nu,Cel);
}

void EVP_Material::unload_step(double* dstrn,double* strs_n, double* statev_n, int kinc, int kstep){

       if(kinc == 1 && kstep==1){
		return;
	}

	int i,error;
	static double **C0 =NULL;
        static double **invC0=NULL;

	static double *strs=NULL;
        static double *udstrn=NULL;
        if(C0==NULL)
        {
          mallocmatrix(&C0,6,6);
          mallocmatrix(&invC0,6,6);
          mallocvector(&strs,6);
          mallocvector(&udstrn,6);
        }
        cleartens4(C0);
        cleartens4(invC0);
        cleartens2(strs);
        cleartens2(udstrn);


	get_elOD(statev_n, C0);
	inverse(C0,invC0,&error,6);
	if(error!=0) printf("problem while inversing C0 in unload_step of MTSecF\n");

	copyvect(&statev_n[pos_strs], strs, 6);        //stress in at tn
        contraction42(invC0, strs, udstrn);          //unloading strain of composite at tn
       
 // update statev_n to keep the imformation for residual condition 

        for(i=0;i<6;i++){
	strs_n[i]=0.0;	
        statev_n[pos_strs +i]=0.0;                                   //residual stress is 0
        statev_n[pos_strn +i]= statev_n[pos_strn +i]- udstrn[i];      //residual strain 
        statev_n[pos_dstrn +i]= dstrn[i]+ udstrn[i];    // strain increment in composte

        dstrn[i]= statev_n[pos_dstrn +i];
      	}


        //freematrix(C0,6);
	//freematrix(invC0,6);
	
        //free(strs);
        //free(udstrn);

}

//******End of elasto viscoplastic constitutive law***************************************

//********************************************************************************************
// COMPOSITE - VOIGT  
//PROPS: MODEL, IDDAM, NGrain, IDMAT phase 1, IDTOP phase 1, ... , 
//	 	...	IDMAT phase N, IDTOP phase N
//  (material properties of each phase), (volume fraction of each phase)
//STATEV: strn, strs, dstrn, (statev of each phase)   
//********************************************************************************************
//constructor
VT_Material::VT_Material(double* props, int idmat):Material(props, idmat){

	int i,idmat_i,idtop_i,k;
	int iddam, idclength;

	nsdv=18;
	iddam = (int) props[idmat+1];
        idclength = (int)props[idmat+2];
        NGrain = (int) props[idmat+3];
	mat = (Material**) malloc(NGrain*sizeof(Material*));	
	mallocvector(&vf,NGrain);
        
	k=idmat+4;

	for(i=0;i<NGrain;i++){
		idmat_i = (int)props[k++];
		idtop_i = (int)props[k++];
		
		mat[i] = init_material(props,idmat_i);
		vf[i] = props[idtop_i];
		nsdv += mat[i]->get_nsdv();
	}

	pos_strn=0;
	pos_strs=6;
	pos_dstrn=12;

        pos_p = 0; // defined by Ling Wu for the input of clength->creat_cg, which is not implemented for this material


	dam = init_damage(props,iddam);
	if(dam){
		pos_dam=nsdv;
		nsdv += dam->get_nsdv();
	}

        
        clength = init_clength(props,idclength);

 
}
#ifdef NONLOCALGMSH
int VT_Material::get_pos_currentplasticstrainfornonlocal() const
{
	return -1;
}
int VT_Material::get_pos_effectiveplasticstrainfornonlocal() const
{
	return -1;
}

int VT_Material::get_pos_damagefornonlocal() const
{
	return -1;
}
double VT_Material::getElasticEnergy (double* statev) 
{
    double eE = 0.;
    int i;
    static double* strn = NULL;
    static double* strs = NULL;
    //static double**CelD = NULL;
    //double  D;
    if(strn == NULL)
    {
       mallocvector(&strn,6);
       mallocvector(&strs,6);
       //mallocmatrix(&CelD,6,6);
    }
    cleartens2(strn);
    cleartens2(strs);
    copyvect(&(statev[get_pos_strn()]), strn,6);
    //get_elOD(statev, CelD);
    //contraction42(CelD,strn,strs);
    copyvect(&statev[get_pos_strs()], strs, 6);   
    //neegry due to damage has been removed strn3 =eps12*sqrt2, strs3=sqrt2*strs12
    for (i =0; i< 6; i++) eE+=0.5*(strs[i]*strn[i]);

    return eE;

}

double VT_Material::getPlasticEnergy (double* statev)
{
    double eP = 0.;
    return eP;
}

#endif

//destructor
VT_Material::~VT_Material(){

	int i;
	for (i=0;i<NGrain;i++){
		if(mat[i]){
			delete mat[i];
		}
	}
	free(mat);
	free(vf);
	if(dam) delete(dam);
        if(clength) delete clength;
	
}

//print
void VT_Material::print(){

	int i;
	printf("Voigt\n");
	if(dam){ dam->print();}

	printf("Number of phases: %d\n", NGrain);
	
	for(i=0;i<NGrain;i++){
		printf("phase %d, volume fraction: %lf\n",i+1,vf[i]);
		mat[i]->print();
	}
}

//constbox
int VT_Material::constbox(double* dstrn, double* strs_n,  double* strs, double* statev_n, double* statev, double** Cref, double*** dCref, double* tau, double** dtau, double** Calgo, double alpha, double* dpdE, double* strs_dDdp_bar, double *Sp_bar, double** c_g, double* dFd_d_bar, double* dstrs_dFd_bar, double *dFd_dE, double** c_gF, double* dpdFd, double* dFddp, int kinc, int kstep, double dt){

  return (1);
}

//reference operator from statev
void VT_Material::get_refOp(double* statev, double** C, int kinc, int kstep){

	printf("get_refOp not implemented for Voigt material\n");

}

void VT_Material::get_elOp(double** Cel){

	printf("get_elOp not implemented for Voigt material\n");

}

void VT_Material::unload_step(double* dstrn, double* strs_n, double* statev_n, int kinc, int kstep){

       printf("unload_step not implemented for Voigt material\n");
}

void VT_Material::get_elOD(double* statev, double** Cel)
{
     get_elOp(Cel);
     if(dam)
     {
       double damage = statev[get_pos_dam()];
       for(int i=0; i < 6; i++)
         for(int j=0; j < 6; j++)
           Cel[i][j]*=1.-damage;
    }
}

//********************************************************************************************************************
//  Laminate of two plies  
//PROPS: MODEL, IDDAM,  ... , 
//	 	...	IDMAT phase A, 
//  (material properties of each phase), (volume fraction of each phase)
//STATEV: strn, strs, dstrn, (statev of each phase)   
//********************************************************************************************
//constructor
LAM2Ply_Material::LAM2Ply_Material(double* props, int idmat):Material(props, idmat){

 
}
#ifdef NONLOCALGMSH
int LAM2Ply_Material::get_pos_currentplasticstrainfornonlocal() const
{
	return -1;
}
int LAM2Ply_Material::get_pos_effectiveplasticstrainfornonlocal() const
{
	return -1;
}

int LAM2Ply_Material::get_pos_damagefornonlocal() const
{
	return -1;
}
double LAM2Ply_Material::getElasticEnergy (double* statev) 
{
    double eE = 0.;
    int i;
    static double* strn = NULL;
    static double* strs = NULL;
    //static double**CelD = NULL;
    //double  D;
    if(strn == NULL)
    {
       mallocvector(&strn,6);
       mallocvector(&strs,6);
       //mallocmatrix(&CelD,6,6);
    }
    cleartens2(strn);
    cleartens2(strs);
    copyvect(&(statev[get_pos_strn()]), strn,6);
    //get_elOD(statev, CelD);
    //contraction42(CelD,strn,strs);
    copyvect(&statev[get_pos_strs()], strs, 6);   
    //neegry due to damage has been removed strn3 =eps12*sqrt2, strs3=sqrt2*strs12
    for (i =0; i< 6; i++) eE+=0.5*(strs[i]*strn[i]);

    return eE;

}

double LAM2Ply_Material::getPlasticEnergy (double* statev)
{
    double eP = 0.;
    return eP;
}

#endif

//destructor
LAM2Ply_Material::~LAM2Ply_Material(){

	int i;
	for (i=0;i<NPly;i++){
		if(mat[i]){
			delete mat[i];
		}
	}
	free(mat);
	free(vf);
	if(dam) delete(dam);
        if(clength) delete clength;
	
}

//print
void LAM2Ply_Material::print(){

	int i;
	printf("LAM2Ply\n");
	if(dam){ dam->print();}

	printf("Number of phases: %d\n", NPly);
	
	for(i=0;i<NPly;i++){
		printf("phase %d, volume fraction: %lf\n",i+1,vf[i]);
		mat[i]->print();
	}
}

//constbox
int LAM2Ply_Material::constbox(double* dstrn, double* strs_n,  double* strs, double* statev_n, double* statev, double** Cref, double*** dCref, double* tau, double** dtau, double** Calgo, double alpha, double* dpdE, double* strs_dDdp_bar, double *Sp_bar, double** c_g, double* dFd_d_bar, double* dstrs_dFd_bar, double *dFd_dE, double** c_gF, double* dpdFd, double* dFddp, int kinc, int kstep, double dt){


	
        return 1;
}

//reference operator from statev
void LAM2Ply_Material::get_refOp(double* statev, double** C, int kinc, int kstep){



}

void LAM2Ply_Material::get_elOp(double** Cel){	

}


void LAM2Ply_Material::unload_step(double* dstrn, double* strs_n, double* statev_n, int kinc, int kstep){
   printf("Function unload_step is not defined for this Lam2Ply material\n");
}


void LAM2Ply_Material::get_elOD(double* statev, double** Cel)
{
 
}

//**************************************************************************************************************************************************
// COMPOSITE - MORI-TANAKA  
//PROPS: MODEL, IDDAM, Nph=2, IDMAT matrix, IDMAT inclusion, IDTOP inclusion,
//  (material properties of each phase), topology of inclusion: vf, ar1, ar2, 3 euler angles
//STATEV: strn, strs, dstrn, MT macro operator (21 components - symmetric), MT Elastic operator (21 components - symmetric), (statev of each phase)   
//***************************************************************************************************************************************************
//constructor
MT_Material::MT_Material(double* props, int idmat):Material(props,idmat){

	int idmat_m, idmat_i, idtop_i,k;
	int iddam, idclength;
        int i;

	nsdv=39;  //18 (strn,strs,dstrn) + 21 (MT macro tangent operator) 
	Nph=2;

	iddam = (int) props[idmat+1];
        idclength = (int)props[idmat+2];
	k=idmat+3;	

	idmat_m=(int) props[k++];
	idmat_i=(int) props[k++];
	idtop_i=(int) props[k++];

	//create materials for each phase
	mtx_mat = init_material(props,idmat_m);
	icl_mat = init_material(props,idmat_i);

	//compute the total number of state variables
	idsdv_m = nsdv;
	idsdv_i = idsdv_m + mtx_mat->get_nsdv();

	nsdv += mtx_mat->get_nsdv(); 
	nsdv += icl_mat->get_nsdv();

	//allocate memory
	mallocvector(&ar,2);
	mallocvector(&euler,3);



	//get topology of inclusion
	k=idtop_i;
	vf_i = props[k++];
	ar[0] = props[k++];
	ar[1] = props[k++];
	euler[0] = props[k++];
	euler[1] = props[k++];
	euler[2] = props[k++];

	//        printf("phase prop: vi %f, AR %f %f, euler %f %f %f\n",vf_i, ar[0], ar[1], euler[0], euler[1], euler[2]  );

	
	vf_m = 1.-vf_i;
	//set positions of strn, strs, dstrn and tangent operator in statev vector
	pos_strn=0;
	pos_strs=6;
	pos_dstrn=12;
	pos_C = 18;

	dam = init_damage(props,iddam);
	if(dam){
		pos_dam = nsdv;
		nsdv += dam->get_nsdv();
	}


        clength = init_clength(props,idclength);
	
}
#ifdef NONLOCALGMSH
int MT_Material::get_pos_currentplasticstrainfornonlocal() const
{
        if(mtx_mat->get_pos_currentplasticstrainfornonlocal()>=0)	
	    return mtx_mat->get_pos_currentplasticstrainfornonlocal()+idsdv_m;
	return -1;

}
int MT_Material::get_pos_effectiveplasticstrainfornonlocal() const
{
       if(mtx_mat->get_pos_effectiveplasticstrainfornonlocal()>=0)
	   return mtx_mat->get_pos_effectiveplasticstrainfornonlocal()+idsdv_m;
       return -1;
}

int MT_Material::get_pos_damagefornonlocal() const
{
       if(mtx_mat->get_pos_damagefornonlocal() >=0)
	   return mtx_mat->get_pos_damagefornonlocal()+idsdv_m;
       return -1;
}
int MT_Material::get_pos_mtx_stress () const
{ 
   return idsdv_m + mtx_mat->get_pos_strs();
}
int MT_Material::get_pos_inc_stress () const
{
   return idsdv_i + icl_mat->get_pos_strs();
}
int MT_Material::get_pos_mtx_strain () const
{ 
   return idsdv_m + mtx_mat->get_pos_strn();
}
int MT_Material::get_pos_inc_strain () const
{
   return idsdv_i + icl_mat->get_pos_strn();
}
int MT_Material::get_pos_mtx_Dam () const
{ 
   return idsdv_m + mtx_mat->get_pos_dam();
}
int MT_Material::get_pos_inc_Dam () const
{
   return idsdv_i + icl_mat->get_pos_dam();
}
double MT_Material::getElasticEnergy (double* statev) 
{
    double eEi = icl_mat->getElasticEnergy(&statev[idsdv_i]);
    double eE0 = mtx_mat->getElasticEnergy(&statev[idsdv_m]);
    double eE = vf_i*eEi+vf_m*eE0;

    return eE;

}

double MT_Material::getPlasticEnergy (double* statev) 
{
    double ePi = icl_mat->getPlasticEnergy(&statev[idsdv_i]);
    double eP0 = mtx_mat->getPlasticEnergy(&statev[idsdv_m]);
    double eP = vf_i*ePi+vf_m*eP0;

    return eP;
}

void MT_Material::setEuler(double e1, double e2, double e3)
{
  euler[0] = e1;
  euler[1] = e2;
  euler[2] = e3;
  
  icl_mat->setEuler(e1,e2,e3);
}
#endif

int MT_Material::get_pos_Fd_bar()const
{
   if(icl_mat->get_pos_Fd_bar()!= 0) return idsdv_i + icl_mat->get_pos_Fd_bar();
   else return 0;
}
int MT_Material::get_pos_dFd_bar()const
{
   if(icl_mat->get_pos_dFd_bar()!= 0) return idsdv_i + icl_mat->get_pos_dFd_bar();
   else return 0;
}
int MT_Material::get_pos_locFd()const
{
   if(icl_mat->get_pos_locFd()!= 0) return idsdv_i + icl_mat->get_pos_locFd();
   else return 0;
}


//destructor
MT_Material::~MT_Material(){

	
	if(mtx_mat) delete mtx_mat;
	if(icl_mat) delete icl_mat;

	free(ar);
	free(euler);

	if(dam) delete(dam);
        if(clength) delete clength;

}

//print
void MT_Material::print(){

	
	printf("Mori-Tanaka\n");
	if(dam) dam->print();
	printf("Number of phases: %d\n", Nph);
	
	printf("matrix, volume fraction: %lf\n",vf_m);
	mtx_mat->print();

	printf("inclusion, volume fraction: %lf\n",vf_i);
	printf("aspect ratio: %lf, %lf\n",ar[0],ar[1]);
	printf("euler angles: %lf, %lf, %lf\n",euler[0],euler[1],euler[2]);	
	icl_mat->print();
	
}

//constbox   ************* new tensor added by wu ling: double* dpdE, double* strs_dDdp_bar 13.04.2011
int MT_Material::constbox(double* dstrn, double* strs_n,  double* strs, double* statev_n, double* statev, double** Cref, double*** dCref, double* tau, double** dtau, double** Calgo, double alpha, double* dpdE, double* strs_dDdp_bar, double *Sp_bar, double** c_g, double* dFd_d_bar, double* dstrs_dFd_bar, double *dFd_dE, double** c_gF, double* dpdFd, double* dFddp, int kinc, int kstep, double dt){
	//alpha: for mid-point integration law to compute reference MT operator: C_MT = (1-alpha)*C_MT(tn) + alpha*C_MT(tn+1)
	
  return (1);
}

void MT_Material::get_refOp(double* statev, double** C, int kinc, int kstep){


	int i,j,k;

	if(kinc < 2 && kstep==1){
		get_elOp(C);
	}
	else{
		k=pos_C;
		for(i=0;i<6;i++){
			for(j=i;j<6;j++){
				C[i][j] = statev[k++];
				C[j][i] = C[i][j];
			}
		}
	}		

}


void MT_Material::get_elOp(double** Cel){

	int i,error;
	static double **C0=NULL;
        static double **C1=NULL;
        static double **invC0=NULL;
        static double **C0_loc=NULL;
	static double **A=NULL;
        static double **invA=NULL;
	static double **S=NULL;
        static double **R66=NULL;
	static double **I=NULL;
	static double **mat1=NULL;

        if(C0==NULL)
        {
          mallocmatrix(&C0,6,6);
          mallocmatrix(&C1,6,6);
          mallocmatrix(&invC0,6,6);
          mallocmatrix(&C0_loc,6,6);
          mallocmatrix(&A,6,6);
          mallocmatrix(&invA,6,6);
          mallocmatrix(&S,6,6);
          mallocmatrix(&R66,6,6);
          mallocmatrix(&I,6,6);
          mallocmatrix(&mat1,6,6);
        }
        cleartens4(C0);
        cleartens4(C1);
        cleartens4(invC0);
        cleartens4(C0_loc);
        cleartens4(A);
        cleartens4(invA);
        cleartens4(S);
        cleartens4(R66);
        cleartens4(I);
        cleartens4(mat1);


	for(i=0;i<6;i++){
		I[i][i] = 1.;
	}

	mtx_mat->get_elOp(C0);
	icl_mat->get_elOp(C1);
	
	//if fibers not oriented with e3:
	if ( (ar[0] != 1 || ar[1] != 1) && (fabs(euler[0]) > 1e-3 || fabs(euler[1]) > 1e-3)){

			eul_mat(euler, R66);
			rot4(R66,C0,C0_loc);
			eshelby(C0_loc,ar[0],ar[1],mat1);  	//compute Eshelby's tensor in local frame
			transpose(R66,R66,6,6);
			rot4(R66,mat1,S);

		 
	}
	else{
		eshelby(C0,ar[0],ar[1],S);  
	}

	inverse(C0,invC0,&error,6);
	if(error!=0) printf("problem while inversing C0 in compute_elOp of MT\n");

	contraction44(invC0,C1,mat1);
	addtens4(1.,mat1,-1.,I,mat1);	
	contraction44(S,mat1,invA);
	addtens4(1.,I,vf_m,invA,invA);
	inverse(invA,A,&error,6);
	if(error!=0) printf("problem while inversing invA in compute_elOp of MT\n");


	addtens4(1.,C1,-1.,C0,mat1);
	contraction44(mat1,A,Cel);
	addtens4(1.,C0,vf_i,Cel,Cel);

}
// Cel with damaged matrix and inclusion, add by Ling WU June 2012 

void MT_Material::get_elOD(double* statev, double** Cel)
{
        int i,error;
	static double **C0=NULL;
        static double **C1=NULL;
        static double **invC0=NULL;
        static double **C0_loc=NULL;
	static double **A=NULL;
        static double **invA=NULL;
	static double **S=NULL;
        static double **R66=NULL;
	static double **I=NULL;
	static double **mat1=NULL;

        if(C0==NULL)
        {
          mallocmatrix(&C0,6,6); 
          mallocmatrix(&C1,6,6);
          mallocmatrix(&invC0,6,6);
          mallocmatrix(&C0_loc,6,6);
          mallocmatrix(&A,6,6);
          mallocmatrix(&invA,6,6);
          mallocmatrix(&S,6,6);
          mallocmatrix(&R66,6,6);
          mallocmatrix(&I,6,6);
          mallocmatrix(&mat1,6,6);
        }
        cleartens4(C0); 
        cleartens4(C1);
        cleartens4(invC0);
        cleartens4(C0_loc);
        cleartens4(A);
        cleartens4(invA);
        cleartens4(S);
        cleartens4(R66);
        cleartens4(I);
        cleartens4(mat1);

	for(i=0;i<6;i++){
		I[i][i] = 1.;
	}

	mtx_mat->get_elOD(&(statev[idsdv_m]), C0);
	icl_mat->get_elOD(&(statev[idsdv_i]), C1);
	
	//if fibers not oriented with e3:
	if ( (ar[0] != 1 || ar[1] != 1) && (fabs(euler[0]) > 1e-3 || fabs(euler[1]) > 1e-3)){

			eul_mat(euler, R66);
			rot4(R66,C0,C0_loc);
			eshelby(C0_loc,ar[0],ar[1],mat1);  	//compute Eshelby's tensor in local frame
			transpose(R66,R66,6,6);
			rot4(R66,mat1,S);

		 
	}
	else{
		eshelby(C0,ar[0],ar[1],S);  
	}

	inverse(C0,invC0,&error,6);
	if(error!=0) printf("problem while inversing C0 in compute_elOp of MT\n");

	contraction44(invC0,C1,mat1);
	addtens4(1.,mat1,-1.,I,mat1);	
	contraction44(S,mat1,invA);
	addtens4(1.,I,vf_m,invA,invA);
	inverse(invA,A,&error,6);
	if(error!=0) printf("problem while inversing invA in compute_elOp of MT\n");


	addtens4(1.,C1,-1.,C0,mat1);
	contraction44(mat1,A,Cel);
	addtens4(1.,C0,vf_i,Cel,Cel);

	//freematrix(C0,6);
	//freematrix(C1,6);
	//freematrix(invC0,6);
	//freematrix(C0_loc,6);
	//freematrix(A,6);
	//freematrix(invA,6);
	//freematrix(S,6);
	//freematrix(R66,6);
	//freematrix(I,6);
	//freematrix(mat1,6);
}


void MT_Material::unload_step(double* dstrn, double* strs_n, double* statev_n, int kinc, int kstep){
   printf("Function unload_step is not defined for this MT material\n");
}
//**************************************************************************************************************************************************
// COMPOSITE - MORI-TANAKA  
//*******modified by Ling wu for secant mehod of first moment---  JUNE 2012
//PROPS: MODEL, IDDAM, Nph=2, IDMAT matrix, IDMAT inclusion, IDTOP inclusion,
//  (material properties of each phase), topology of inclusion: vf, ar1, ar2, 3 euler angles
//STATEV: strn, strs, dstrn, MT macro operator (21 components - symmetric), MT Elastic operator (21 components - symmetric), (statev of each phase)   
//***************************************************************************************************************************************************
//
//constructor
MTSecF_Material::MTSecF_Material(double* props, int idmat):MT_Material(props,idmat){

       malloc_lcc(&LCC, Nph, vf_i);
}

//destructor
MTSecF_Material::~MTSecF_Material(){
    
      free_lcc(&LCC);
}


//constbox   ************* new tensor added by wu ling: double* dpdE, double* strs_dDdp_bar 13.04.2011
//*********************************************
//unloading is carried out at each iteration 

int MTSecF_Material::constbox(double* dstrn, double* strs_n,  double* strs, double* statev_n, double* statev, double** Cref, double*** dCref, double* tau, double** dtau, double** Calgo, double alpha, double* dpdE, double* dstrsdp_bar, double *Sp_bar, double** c_g, double* dFd_d_bar, double* dstrs_dFd_bar, double *dFd_dE, double** c_gF, double* dpdFd, double* dFddp, int kinc, int kstep, double dt){
	//alpha: for mid-point integration law to compute reference MT operator: C_MT = (1-alpha)*C_MT(tn) + alpha*C_MT(tn+1)
	
  return (1);
}



// unloading step for MTSecF_Material material to reset statev_n in residual condition, by Ling Wu June 2012

void MTSecF_Material::unload_step(double* dstrn, double* strs_n, double* statev_n, int kinc, int kstep){

        if(kinc == 1 && kstep==1){
		return;
	}

	int i,error;
	static double **C0=NULL;
        static double **C1=NULL;
	static double **Cel=NULL;
	static double **C0_loc=NULL;
        static double **invC0=NULL;
        static double **invCel=NULL;
	static double **A=NULL;
        static double **invA=NULL;
	static double **S=NULL;
        static double **R66=NULL;
	static double **I=NULL;
	static double **mat1=NULL;

        static double *strs_c=NULL;
        static double *dstrs_m=NULL;
        static double *dstrs_i=NULL;
        static double *dstrn_c=NULL;
        static double *dstrn_m=NULL;
        static double *dstrn_i=NULL;
        if(C0==NULL)
        {
          mallocmatrix(&C0,6,6);
          mallocmatrix(&C1,6,6);
          mallocmatrix(&Cel,6,6);
          mallocmatrix(&C0_loc,6,6);

	  mallocmatrix(&invC0,6,6);
          mallocmatrix(&invCel,6,6);
 
          mallocmatrix(&A,6,6);
	  mallocmatrix(&invA,6,6);
  	  mallocmatrix(&S,6,6);
          mallocmatrix(&R66,6,6);
	  mallocmatrix(&I,6,6);
	  mallocmatrix(&mat1,6,6);

          mallocvector(&strs_c,6);
          mallocvector(&dstrs_m,6);
          mallocvector(&dstrs_i,6);
          mallocvector(&dstrn_c,6);
          mallocvector(&dstrn_m,6);
          mallocvector(&dstrn_i,6);
        }
        cleartens4(C0);
        cleartens4(C1);
        cleartens4(Cel);
        cleartens4(C0_loc);

	cleartens4(invC0);
        cleartens4(invCel);
 
        cleartens4(A);
	cleartens4(invA);
  	cleartens4(S);
        cleartens4(R66);
	cleartens4(I);
        cleartens4(mat1);

        cleartens2(strs_c);
        cleartens2(dstrs_m);
        cleartens2(dstrs_i);
        cleartens2(dstrn_c);
        cleartens2(dstrn_m);
        cleartens2(dstrn_i);


	for(i=0;i<6;i++){
		I[i][i] = 1.;
	}

	mtx_mat->get_elOD(&(statev_n[idsdv_m]), C0);
	icl_mat->get_elOD(&(statev_n[idsdv_i]), C1);

        get_elOD(statev_n, Cel);

       	inverse(C0,invC0,&error,6);
	if(error!=0) printf("problem while inversing C0 in unload_step of MTSecF\n");

	inverse(Cel,invCel,&error,6);
	if(error!=0) printf("problem while inversing Cel in unload_step of MTSecF\n");

	//if fibers not oriented with e3:
	if ( (ar[0] != 1.0 || ar[1] != 1.0) && (fabs(euler[0]) > 1e-3 || fabs(euler[1]) > 1e-3)){

			eul_mat(euler, R66);
			rot4(R66,C0,C0_loc);
			eshelby(C0_loc,ar[0],ar[1],mat1);  	//compute Eshelby's tensor in local frame
			transpose(R66,R66,6,6);
			rot4(R66,mat1,S);
	}
	else{
		eshelby(C0,ar[0],ar[1],S);  
	}


        contraction44(invC0,C1,mat1);
	addtens4(1.,mat1,-1.,I,mat1);	
	contraction44(S,mat1,invA);
	addtens4(1.,I,vf_m,invA,invA);
	inverse(invA,A,&error,6);
	if(error!=0) printf("problem while inversing invA in unload_step of MT\n");

 
        copyvect(&(statev_n[pos_strs]), strs_c, 6);        //stress in composite at tn
        contraction42(invCel, strs_c, dstrn_c);          //unloading strain of composite at tn
        contraction42(A, dstrn_c, dstrn_i);              //unloading strain of inclusion at tn
        addtens2 (1./vf_m, dstrn_c, -1.*vf_i/vf_m, dstrn_i, dstrn_m); //unloading strain of matrix at tn

        contraction42(C1, dstrn_i, dstrs_i);              //unloading stress of inclusion at tn
        contraction42(C0, dstrn_m, dstrs_m);              //unloading stress of matrix at tn

 // update statev_n to keep the imformation for residual condition 

        for(i=0;i<6;i++){
	strs_n[i]=0.0;	
        statev_n[pos_strs +i]=0.0;                                   //residual stress in composte is 0
        statev_n[pos_strn +i]= statev_n[pos_strn +i]- dstrn_c[i];      //residual strain in composte
        statev_n[pos_dstrn +i]= dstrn[i]+ dstrn_c[i];    // strain increment in composte

        dstrn[i]= statev_n[pos_dstrn +i];

#if Crmatrix_1st==1
       statev_n[idsdv_m + mtx_mat->get_pos_strs()+i]= statev_n[idsdv_m + mtx_mat->get_pos_strs()+i]-dstrs_m[i];  //residual stress in marix
#else
       statev_n[idsdv_m + mtx_mat->get_pos_strs()+i]= 0.0;
#endif
        statev_n[idsdv_m + mtx_mat->get_pos_strn()+i]= statev_n[idsdv_m + mtx_mat->get_pos_strn()+i]-dstrn_m[i];  //residual strain in marix
     
#if Crinclusion==1
        statev_n[idsdv_i + icl_mat->get_pos_strs()+i]= statev_n[idsdv_i + icl_mat->get_pos_strs()+i]-dstrs_i[i];  //residual stress in inclusion
#else
        statev_n[idsdv_i + icl_mat->get_pos_strs()+i]= 0.0;
#endif

        statev_n[idsdv_i + icl_mat->get_pos_strn()+i]= statev_n[idsdv_i + icl_mat->get_pos_strn()+i]-dstrn_i[i];  //residual strain in inclusion
        } 

       

}

#ifdef NONLOCALGMSH

void MTSecF_Material:: get_elOp_mtx(double* statev, double** Cel){
       mtx_mat->get_elOp(&statev[idsdv_m],Cel);
}

void MTSecF_Material::get_elOp_icl(double* statev, double** Cel){
       icl_mat->get_elOp(&statev[idsdv_i],Cel);
}

void MTSecF_Material:: get_elOp_mtx(double** Cel){
       mtx_mat->get_elOp(Cel);
}

void MTSecF_Material::get_elOp_icl(double** Cel){
       icl_mat->get_elOp(Cel);
}

#endif



//**************************************************************************************************************************************************
// COMPOSITE - MORI-TANAKA   
//*******modified by Ling wu for second moment for Ntr--  Feb 2014
//PROPS: MODEL, IDDAM, Nph=2, IDMAT matrix, IDMAT inclusion, IDTOP inclusion,
//  (material properties of each phase), topology of inclusion: vf, ar1, ar2, 3 euler angles
//STATEV: strn, strs, dstrn, MT macro operator (21 components - symmetric), MT Elastic operator (21 components - symmetric), (statev of each phase)   
//***************************************************************************************************************************************************
//
//constructor
MTSecNtr_Material::MTSecNtr_Material(double* props, int idmat):MT_Material(props,idmat){
     malloc_lcc(&LCC, Nph, vf_i);

}

//destructor
MTSecNtr_Material::~MTSecNtr_Material(){
    free_lcc(&LCC);
}


//constbox   ************* new tensor added by wu ling: double* dpdE, double* strs_dDdp_bar 13.04.2011
//*********************************************
//unloading is carried out at each iteration 

int MTSecNtr_Material::constbox(double* dstrn, double* strs_n,  double* strs, double* statev_n, double* statev, double** Cref, double*** dCref, double* tau, double** dtau, double** Calgo, double alpha, double* dpdE, double* dstrsdp_bar, double *Sp_bar, double** c_g, double* dFd_d_bar, double* dstrs_dFd_bar, double *dFd_dE, double** c_gF, double* dpdFd, double* dFddp, int kinc, int kstep, double dt){
	//alpha: for mid-point integration law to compute reference MT operator: C_MT = (1-alpha)*C_MT(tn) + alpha*C_MT(tn+1)
	

  return (1);
}




// unloading step for MTSecF_Material material to reset statev_n in residual condition, by Ling Wu June 2012

void MTSecNtr_Material::unload_step(double* dstrn, double* strs_n, double* statev_n, int kinc, int kstep){

      //get value of mu kappa for LCC and C0 C1
	mtx_mat->get_elOD(&(statev_n[idsdv_m]), LCC.C0);
	icl_mat->get_elOD(&(statev_n[idsdv_i]), LCC.C1);

        LCC.mu[0] = LCC.C0[3][3]/2.0;
        LCC.mu[1] = LCC.C1[3][3]/2.0;

        LCC.kappa[0] = LCC.C0[0][1] + LCC.C0[3][3]/3.0;
        LCC.kappa[1] = LCC.C1[0][1] + LCC.C1[3][3]/3.0;

	int i,j,error;
        static double **invC0=NULL;
	static double **invCel=NULL;
	static double **invA=NULL;;
	static double **S=NULL;
	static double **dS=NULL;
	static double **ddS=NULL;
	static double **I=NULL;
	static double **Idev=NULL;
	static double **mat1=NULL;

        static double *strs_c=NULL;
	static double *dstrs_m=NULL;
	static double *dstrs_i=NULL;
        static double *dstrn_c=NULL;
	static double *dstrn_m=NULL;
	static double *dstrn_i=NULL;

        static double *dnu=NULL;
        
        double deqstrs2_m, number;    //square of incremental vm equivalent stress in matrix and inclusion   
        double eqstrs2_m;    // vm equivalent stress in matrix and inclusion      
        static double *dstrs_dev=NULL;
	static double *vec1=NULL; 

        static double *strs_rmdev=NULL; 
        if(invC0==NULL)
        {
  	  mallocmatrix(&invC0,6,6);
          mallocmatrix(&invCel,6,6);

	  mallocmatrix(&invA,6,6);
	  mallocmatrix(&S,6,6);
	  mallocmatrix(&dS,6,6);
	  mallocmatrix(&ddS,6,6);

	  mallocmatrix(&I,6,6);
	  mallocmatrix(&Idev,6,6);
	  mallocmatrix(&mat1,6,6);

          mallocvector(&strs_c,6);
          mallocvector(&dstrs_m,6);
          mallocvector(&dstrs_i,6);
          mallocvector(&dstrn_c,6);
          mallocvector(&dstrn_m,6);
          mallocvector(&dstrn_i,6);

          mallocvector(&dnu,5);

          mallocvector(&dstrs_dev,6);
          mallocvector(&vec1,6);

          mallocvector(&strs_rmdev,6);
        }
  	cleartens4(invC0);
        cleartens4(invCel);

	cleartens4(invA);
	cleartens4(S);
	cleartens4(dS);
	cleartens4(ddS);

	cleartens4(I);
	cleartens4(Idev);
        cleartens4(mat1);

        cleartens2(strs_c);
        cleartens2(dstrs_m);
        cleartens2(dstrs_i);
        cleartens2(dstrn_c);
        cleartens2(dstrn_m);
        cleartens2(dstrn_i);

        for(i=0;i<5;i++) dnu[i]=0.;

        cleartens2(dstrs_dev);
        cleartens2(vec1);

        cleartens2(strs_rmdev);

        for(i=0;i<3;i++){
		Idev[i][i]=1.;
		Idev[i+3][i+3]=1.;
		I[i][i]=1.;
		I[i+3][i+3]=1.;
		for(j=0;j<3;j++){
			Idev[i][j]=Idev[i][j]-1./3.;
		}
	}	
	


        Eshelby(LCC.C0, ar, euler, S, dS, ddS, dnu);
        CA_dCdA_MT(&LCC, vf_i, S, dS, dnu);


        for(i=0;i<6;i++){
              LCC.strs_n[i] = statev_n[idsdv_m + mtx_mat->get_pos_strs()+i];
              LCC.dstrnf[i] = dstrn[i];
              LCC.dstrn_m[i] = statev_n[idsdv_m + mtx_mat->get_pos_strn()+i];
        }

        if(kinc == 1 && kstep==1){
            if (mtx_mat->eqstrs_exist()){
                  statev_n[idsdv_m + mtx_mat->get_pos_eqstrs()] = 0.0;
            }
	    return;
	}


        inverse(LCC.C,invCel,&error,6);
	if(error!=0) printf("problem while inversing Cel in unload_step of MTSecSd\n");

        copyvect(&(statev_n[pos_strs]), strs_c, 6);        //stress in composite at tn
        contraction42(invCel, strs_c, dstrn_c);          //unloading strain of composite at tn
        contraction42(LCC.A, dstrn_c, dstrn_i);              //unloading strain of inclusion at tn
        addtens2 (1./vf_m, dstrn_c, -1.*vf_i/vf_m, dstrn_i, dstrn_m); //unloading strain of matrix at tn

        contraction42(LCC.C1, dstrn_i, dstrs_i);              //unloading stress of inclusion at tn
        contraction42(LCC.C0, dstrn_m, dstrs_m);              //unloading stress of matrix at tn    


    // update statev_n to keep the imformation for residual condition 
        contraction42(Idev, &(statev_n[idsdv_m + mtx_mat->get_pos_strs()]), strs_rmdev);

        for(i=0;i<6;i++){
	strs_n[i]=0.0;	
        statev_n[pos_strs +i]=0.0;                                   //residual stress in composte is 0

        statev_n[pos_strn +i]= statev_n[pos_strn +i]- dstrn_c[i];      //residual strain in composte
        statev_n[pos_dstrn +i]= dstrn[i]+ dstrn_c[i];    // strain increment in composte


        dstrn[i]= statev_n[pos_dstrn +i];
  
    //   printf("stressM=%lf\n",statev_n[idsdv_m + mtx_mat->get_pos_strs()+i]);

   //     printf("stressI=%lf\n",statev_n[idsdv_i + icl_mat->get_pos_strs()+i]);
#if Crmatrix_2nd==1
        statev_n[idsdv_m + mtx_mat->get_pos_strs()+i]= statev_n[idsdv_m + mtx_mat->get_pos_strs()+i]-dstrs_m[i];  //residual stress in marix
#else
        statev_n[idsdv_m + mtx_mat->get_pos_strs()+i]= 0.;  //residual 
#endif
    //   printf("stress_Mres= %lf\n",statev_n[idsdv_m + mtx_mat->get_pos_strs()+i]);
        statev_n[idsdv_m + mtx_mat->get_pos_strn()+i]= statev_n[idsdv_m + mtx_mat->get_pos_strn()+i]-dstrn_m[i];  //residual strain in marix
#if Crinclusion==1
        statev_n[idsdv_i + icl_mat->get_pos_strs()+i]= statev_n[idsdv_i + icl_mat->get_pos_strs()+i]-dstrs_i[i];  //residual stress in inclusion
#else
        statev_n[idsdv_i + icl_mat->get_pos_strs()+i]= 0.;  //residual 
#endif
     //   printf("stressIres=%lf\n",statev_n[idsdv_i + icl_mat->get_pos_strs()+i]);
        statev_n[idsdv_i + icl_mat->get_pos_strn()+i]= statev_n[idsdv_i + icl_mat->get_pos_strn()+i]-dstrn_i[i];  //residual strain in inclusion
        } 



//compute the second-moment equivalent stress (eqstrs) in eacher phases       
//1. in matrix 

        if (mtx_mat->eqstrs_exist()){

                  contraction42(Idev, dstrs_m, dstrs_dev);   
                  contraction42(LCC.dCdmu0,dstrn_c,vec1);
		  deqstrs2_m =(contraction22(dstrn_c,vec1))*3.0*LCC.mu[0]*LCC.mu[0]/vf_m;

                  eqstrs2_m = statev_n[idsdv_m + mtx_mat->get_pos_eqstrs()];
                // if(contraction22(dstrs_dev,strs_rmdev)>0)
                //    number= eqstrs2_m + deqstrs2_m - 3.0*contraction22(dstrs_dev,strs_rmdev);
                // else
                    number= eqstrs2_m-deqstrs2_m;
                  //for 0-incr.residual method either put number at 0 or scheme 1 in j2plast
                  //here we use the assumption
                  number=0.;
	          statev_n[idsdv_m + mtx_mat->get_pos_eqstrs()] = number;
                    //printf("number= %f\n", number);
       
	      /*    if(contraction22(dstrs_dev,strs_rmdev)<0){
	          statev_n[idsdv_m + mtx_mat->get_pos_eqstrs()] = 0.0;}
	          if(contraction22(dstrs_dev,strs_rmdev)==0){
	          statev_n[idsdv_m + mtx_mat->get_pos_eqstrs()] = 0.0;}*/
        }


}
//**************************************************************************************************************************************************
// COMPOSITE - MORI-TANAKA  
//*******modified by Ling wu for secant mehod of Second moment---  AUG 2013
//PROPS: MODEL, IDDAM, Nph=2, IDMAT matrix, IDMAT inclusion, IDTOP inclusion,
//  (material properties of each phase), topology of inclusion: vf, ar1, ar2, 3 euler angles
//STATEV: strn, strs, dstrn, MT macro operator (21 components - symmetric), MT Elastic operator (21 components - symmetric), (statev of each phase)   
//***************************************************************************************************************************************************
//
//constructor
MTSecSd_Material::MTSecSd_Material(double* props, int idmat):MT_Material(props,idmat){

     malloc_lcc(&LCC, Nph, vf_i);

}

//destructor
MTSecSd_Material::~MTSecSd_Material(){

     free_lcc(&LCC);  

}


//constbox   ************* new tensor added by wu ling: double* dpdE, double* strs_dDdp_bar 13.04.2011
//*********************************************
//unloading is carried out at each iteration 

int MTSecSd_Material::constbox(double* dstrn, double* strs_n,  double* strs, double* statev_n, double* statev, double** Cref, double*** dCref, double* tau, double** dtau, double** Calgo, double alpha, double* dpdE, double* dstrsdp_bar, double *Sp_bar, double** c_g, double* dFd_d_bar, double* dstrs_dFd_bar, double *dFd_dE, double** c_gF, double* dpdFd, double* dFddp, int kinc, int kstep, double dt){
	//alpha: for mid-point integration law to compute reference MT operator: C_MT = (1-alpha)*C_MT(tn) + alpha*C_MT(tn+1)

  return (1);       
}



// unloading step for MTSecSd_Material material to reset statev_n in residual condition, and the second monment von mise stress eqstrs in elastic-plastic phases, by Ling Wu AUG 2013
// the value of strain concentration tensor is also initialized with mu0 mu1 kappa0 kappa1

void MTSecSd_Material::unload_step(double* dstrn, double* strs_n, double* statev_n, int kinc, int kstep){

        if(kinc == 1 && kstep==1){
            //get value of mu kappa for LCC and C0 C1
  	    mtx_mat->get_elOD(&(statev_n[idsdv_m]), LCC.C0);
	    icl_mat->get_elOD(&(statev_n[idsdv_i]), LCC.C1);
 
            LCC.mu[0] = LCC.C0[3][3]/2.0;
            LCC.mu[1] = LCC.C1[3][3]/2.0;

            if (mtx_mat->eqstrs_exist()){
                  statev_n[idsdv_m + mtx_mat->get_pos_eqstrs()] = max(statev_n[idsdv_m + mtx_mat->get_pos_eqstrs()],0.0);
            }
            if (icl_mat->eqstrs_exist()){
                  statev_n[idsdv_i + icl_mat->get_pos_eqstrs()] = 0.0;
            }
	    return;
	}

	int i,j,error;
        static double **invC0=NULL;
        static double **invCel=NULL;
	static double **invA=NULL;
	static double **S=NULL;
        static double **dS=NULL;
        static double **ddS=NULL;
	static double **I=NULL;
        static double **Idev=NULL;
	static double **mat1=NULL;

        static double *strs_c=NULL;
        static double *dstrs_m=NULL;
        static double *dstrs_i=NULL;
        static double *dstrn_c=NULL;
        static double *dstrn_m=NULL;
        static double *dstrn_i=NULL;

        static double *dnu=NULL;
         
        double deqstrs2_m, deqstrs2_i, number;    //square of incremental vm equivalent stress in matrix and inclusion   
        double eqstrs_m, eqstrs_i;    // vm equivalent stress in matrix and inclusion      

        static double *strs_mdev=NULL;
        static double *strs_idev=NULL;
        static double *dstrs_dev=NULL;
        static double *vec1=NULL; 

        static double *strs_rmdev=NULL;
        static double *strs_ridev=NULL; 
       

	if(invC0==NULL)
        {
          mallocmatrix(&invC0,6,6);
          mallocmatrix(&invCel,6,6);

  	  mallocmatrix(&invA,6,6);
 	  mallocmatrix(&S,6,6);
	  mallocmatrix(&dS,6,6);
	  mallocmatrix(&ddS,6,6);


	  mallocmatrix(&I,6,6);
	  mallocmatrix(&Idev,6,6);
	  mallocmatrix(&mat1,6,6);


          mallocvector(&strs_c,6);
          mallocvector(&dstrs_m,6);
          mallocvector(&dstrs_i,6);
          mallocvector(&dstrn_c,6);
          mallocvector(&dstrn_m,6);
          mallocvector(&dstrn_i,6);

          mallocvector(&dnu,5);

          mallocvector(&strs_mdev,6);
          mallocvector(&strs_idev,6);
          mallocvector(&dstrs_dev,6);
          mallocvector(&vec1,6);

          mallocvector(&strs_rmdev,6);
          mallocvector(&strs_ridev,6);
        }
        cleartens4(invC0);
        cleartens4(invCel);

	cleartens4(invA);
	cleartens4(S);
	cleartens4(dS);
	cleartens4(ddS);


	cleartens4(I);
	cleartens4(Idev);
	cleartens4(mat1);


        cleartens2(strs_c);
        cleartens2(dstrs_m);
        cleartens2(dstrs_i);
        cleartens2(dstrn_c);
        cleartens2(dstrn_m);
        cleartens2(dstrn_i);

        for(i=0;i<5;i++) dnu[i]=0.;

        cleartens2(strs_mdev);
        cleartens2(strs_idev);
        cleartens2(dstrs_dev);
        cleartens2(vec1);

        cleartens2(strs_rmdev);
        cleartens2(strs_ridev);

        for(i=0;i<3;i++){
		Idev[i][i]=1.;
		Idev[i+3][i+3]=1.;
		I[i][i]=1.;
		I[i+3][i+3]=1.;
		for(j=0;j<3;j++){
			Idev[i][j]=Idev[i][j]-1./3.;
		}
	}	
	

      //get value of mu kappa for LCC and C0 C1
	mtx_mat->get_elOD(&(statev_n[idsdv_m]), LCC.C0);
	icl_mat->get_elOD(&(statev_n[idsdv_i]), LCC.C1);

        LCC.mu[0] = LCC.C0[3][3]/2.0;
        LCC.mu[1] = LCC.C1[3][3]/2.0;

        LCC.kappa[0] = LCC.C0[0][1] + LCC.C0[3][3]/3.0;
        LCC.kappa[1] = LCC.C1[0][1] + LCC.C1[3][3]/3.0;

        Eshelby(LCC.C0, ar, euler, S, dS, ddS, dnu);

        CA_dCdA_MT(&LCC, vf_i, S, dS, dnu);

       	inverse(LCC.C,invCel,&error,6);
	if(error!=0) printf("problem while inversing Cel in unload_step of MTSecSd\n");

 
        copyvect(&(statev_n[pos_strs]), strs_c, 6);        //stress in composite at tn
        contraction42(invCel, strs_c, dstrn_c);          //unloading strain of composite at tn
        contraction42(LCC.A, dstrn_c, dstrn_i);              //unloading strain of inclusion at tn
        addtens2 (1./vf_m, dstrn_c, -1.*vf_i/vf_m, dstrn_i, dstrn_m); //unloading strain of matrix at tn

        contraction42(LCC.C1, dstrn_i, dstrs_i);              //unloading stress of inclusion at tn
        contraction42(LCC.C0, dstrn_m, dstrs_m);              //unloading stress of matrix at tn

        contraction42(Idev, &(statev_n[idsdv_m + mtx_mat->get_pos_strs()]), strs_mdev);
        contraction42(Idev, &(statev_n[idsdv_i + icl_mat->get_pos_strs()]), strs_idev); 


    // update statev_n to keep the imformation for residual condition 

        for(i=0;i<6;i++){
	strs_n[i]=0.0;	
        statev_n[pos_strs +i]=0.0;                                   //residual stress in composte is 0
        statev_n[pos_strn +i]= statev_n[pos_strn +i]- dstrn_c[i];      //residual strain in composte
        statev_n[pos_dstrn +i]= dstrn[i]+ dstrn_c[i];    // strain increment in composte

        dstrn[i]= statev_n[pos_dstrn +i];
#if Crmatrix_2nd==1
        statev_n[idsdv_m + mtx_mat->get_pos_strs()+i]= statev_n[idsdv_m + mtx_mat->get_pos_strs()+i]-dstrs_m[i];  //residual stress in marix
#else
        statev_n[idsdv_m + mtx_mat->get_pos_strs()+i]= 0.;  //residual 
#endif
        statev_n[idsdv_m + mtx_mat->get_pos_strn()+i]= statev_n[idsdv_m + mtx_mat->get_pos_strn()+i]-dstrn_m[i];  //residual strain in marix

#if Crinclusion==1
        statev_n[idsdv_i + icl_mat->get_pos_strs()+i]= statev_n[idsdv_i + icl_mat->get_pos_strs()+i]-dstrs_i[i];  //residual stress in inclusion
#else
        statev_n[idsdv_i + icl_mat->get_pos_strs()+i]= 0.;
#endif
        statev_n[idsdv_i + icl_mat->get_pos_strn()+i]= statev_n[idsdv_i + icl_mat->get_pos_strn()+i]-dstrn_i[i];  //residual strain in inclusion
        } 

        contraction42(Idev, &(statev_n[idsdv_m + mtx_mat->get_pos_strs()]), strs_rmdev);
        contraction42(Idev, &(statev_n[idsdv_i + icl_mat->get_pos_strs()]), strs_ridev); 

//compute the second-moment equivalent stress (eqstrs) in eacher phases       
//1. in matrix 

        if (mtx_mat->eqstrs_exist()){

                  contraction42(Idev, dstrs_m, dstrs_dev);   
                  contraction42(LCC.dCdmu0,dstrn_c,vec1);
		  deqstrs2_m =(contraction22(dstrn_c,vec1))*3.0*LCC.mu[0]*LCC.mu[0]/vf_m;

                  eqstrs_m = statev_n[idsdv_m + mtx_mat->get_pos_eqstrs()];
                  number= 0.0;//eqstrs_m-deqstrs2_m-3.0*contraction22(dstrs_dev,strs_rmdev);




                //sqrt(deqstrs2_m-1.5*contraction22(dstrs_dev,dstrs_dev));
                 
               /*   number = eqstrs_m*eqstrs_m+deqstrs2_m-3.0*contraction22(dstrs_dev,strs_mdev)-2.0*
                           sqrt(max(0.0,(deqstrs2_m-1.5*contraction22(dstrs_dev,dstrs_dev))*
                                (eqstrs_m*eqstrs_m-1.5*contraction22(strs_mdev,strs_mdev))));
                       

                  //  number =  eqstrs_m*eqstrs_m-deqstrs2_m-3.0*contraction22(dstrs_dev,strs_rmdev);
                 //   number = max(number, 0.0);
                //  number= vonmises(strs_rmdev)*vonmises(strs_rmdev);
          
              //    if(number == 0.0){
                      //    for(i=0; i<6; i++){
                       //             statev_n[idsdv_m + mtx_mat->get_pos_strs()+i] = 0.0;
                       //    }
               //   }*/

	
	 statev_n[idsdv_m + mtx_mat->get_pos_eqstrs()] = number;
        }


//2. in inclusion 
        if (icl_mat->eqstrs_exist()){

                  contraction42(Idev, dstrs_i, dstrs_dev);   

                  contraction42(LCC.dCdmu1,dstrn_c,vec1);
		  deqstrs2_i =(contraction22(dstrn_c,vec1))*3.0*LCC.mu[1]*LCC.mu[1]/vf_i;

                  eqstrs_i = statev_n[idsdv_i + icl_mat->get_pos_eqstrs()];
                //  number = eqstrs_i*eqstrs_i + deqstrs2_i - 3.0*contraction22(dstrs_dev,strs_idev);
                 number = fabs( eqstrs_i*eqstrs_i -deqstrs2_i) ;
               //  number= max(number, 0.0);
                  statev_n[idsdv_i + icl_mat->get_pos_eqstrs()] = number;//sqrt(number);

        }


}


//**************************************************************************
//By Ling Wu May,2016
//Composite material homogenized with Mori-Tanaka scheme with First-order Incremental-secant method
//when vfi > 1, volume fraction is a random variable depends on the location of Gaussian point.
//when euler > 180, euler angles are random variables depend on the location of Gaussian point.
//**************************************************************************
//constructor
MTSecF_Stoch_Material::MTSecF_Stoch_Material(double* props, int idmat):MT_Material(props,idmat){

        if (vf_i > 1.0){
            pos_vfi = nsdv;
            nsdv += 1;
        }
        else{
            pos_vfi = 0;
        }

        if (euler[0] > 180.0+1.0e-6){
            pos_euler = nsdv;
            nsdv += 3;
        }
        else{
            pos_euler = 0;
        }

        if (ar[0] < 1.0e-6){
            pos_aspR = nsdv;
            nsdv += 1;
        }
        else{
            pos_aspR = 0;
        }	
}

//destructor
//destructor
MTSecF_Stoch_Material::~MTSecF_Stoch_Material(){

}

int MTSecF_Stoch_Material::get_pos_E()const 
{
   if(mtx_mat->get_pos_E() !=0) return idsdv_m + mtx_mat->get_pos_E();
   else return 0;
}
int MTSecF_Stoch_Material::get_pos_nu()const 
{
   if(mtx_mat->get_pos_nu() != 0) return idsdv_m + mtx_mat->get_pos_nu();
   else return 0;
}  
int MTSecF_Stoch_Material::get_pos_sy0()const 
{
   if(mtx_mat->get_pos_sy0()!= 0) return idsdv_m + mtx_mat->get_pos_sy0();
   else return 0;
}  
int MTSecF_Stoch_Material::get_pos_hmod1()const 
{
   if(mtx_mat->get_pos_hmod1()!= 0) return idsdv_m + mtx_mat->get_pos_hmod1();
   else return 0;
}  
int MTSecF_Stoch_Material::get_pos_hmod2()const 
{
   if(mtx_mat->get_pos_hmod2()!= 0) return idsdv_m + mtx_mat->get_pos_hmod2();
   else return 0;
}
int MTSecF_Stoch_Material::get_pos_alpha_DP()const
{
   if(mtx_mat->get_pos_alpha_DP()!= 0) return idsdv_m + mtx_mat->get_pos_alpha_DP();
   else return 0;
}
int MTSecF_Stoch_Material::get_pos_m_DP()const
{
   if(mtx_mat->get_pos_m_DP()!= 0) return idsdv_m + mtx_mat->get_pos_m_DP();
   else return 0;
}
int MTSecF_Stoch_Material::get_pos_nup()const
{
   if(mtx_mat->get_pos_nup()!= 0) return idsdv_m + mtx_mat->get_pos_nup();
   else return 0;
}
int MTSecF_Stoch_Material::get_pos_hp0()const 
{
   if(mtx_mat->get_pos_hp0()!= 0) return idsdv_m + mtx_mat->get_pos_hp0();
   else return 0;
}
int MTSecF_Stoch_Material::get_pos_hexp()const   
{
   if(mtx_mat->get_pos_hexp()!= 0) return idsdv_m + mtx_mat->get_pos_hexp();
   else return 0;
}

int MTSecF_Stoch_Material::get_pos_DamParm1()const   
{
   if(mtx_mat->get_pos_DamParm1()!= 0) return idsdv_m + mtx_mat->get_pos_DamParm1();
   else return 0;
}
int MTSecF_Stoch_Material::get_pos_DamParm2()const   
{
   if(mtx_mat->get_pos_DamParm2()!= 0) return idsdv_m + mtx_mat->get_pos_DamParm2();
   else return 0;
}
int MTSecF_Stoch_Material::get_pos_DamParm3()const
{
  if(mtx_mat->get_pos_DamParm3()!= 0) return idsdv_m + mtx_mat->get_pos_DamParm3();
  else return 0;
}


int MTSecF_Stoch_Material::get_pos_INCDamParm1()const   
{
   if(icl_mat->get_pos_DamParm1()!= 0) return idsdv_i + icl_mat->get_pos_DamParm1();
   else return 0;
}
int MTSecF_Stoch_Material::get_pos_INCDamParm2()const   
{
   if(icl_mat->get_pos_DamParm2()!= 0) return idsdv_i + icl_mat->get_pos_DamParm2();
   else return 0;
}

int MTSecF_Stoch_Material::get_pos_INCDamParm3()const
{
   if(icl_mat->get_pos_DamParm3()!= 0) return idsdv_i + icl_mat->get_pos_DamParm3();
   else return 0;
}

int MTSecF_Stoch_Material::get_pos_Fd_bar()const   
{
   if(icl_mat->get_pos_Fd_bar()!= 0) return idsdv_i + icl_mat->get_pos_Fd_bar();
   else return 0;
}
int MTSecF_Stoch_Material::get_pos_dFd_bar()const   
{
   if(icl_mat->get_pos_dFd_bar()!= 0) return idsdv_i + icl_mat->get_pos_dFd_bar();
   else return 0;
}
int MTSecF_Stoch_Material::get_pos_locFd()const   
{  
   if(icl_mat->get_pos_locFd()!= 0) return idsdv_i + icl_mat->get_pos_locFd();
   else return 0;
}
//constbox
int MTSecF_Stoch_Material::constbox(double* dstrn, double* strs_n,  double* strs, double* statev_n, double* statev, double** Cref, double*** dCref, double* tau, double** dtau, double** Calgo, double alpha, double* dpdE, double* dstrsdp_bar, double *Sp_bar, double** c_g, double* dFd_d_bar, double* dstrs_dFd_bar, double *dFd_dE, double** c_gF, double* dpdFd, double* dFddp, int kinc, int kstep, double dt){
	//alpha: for mid-point integration law to compute reference MT operator: C_MT = (1-alpha)*C_MT(tn) + alpha*C_MT(tn+1)
	
  return (1);
}

// unloading step for MTSecF_Material material to reset statev_n in residual condition, by Ling Wu June 2012

void MTSecF_Stoch_Material::unload_step(double* dstrn, double* strs_n, double* statev_n, double _Vfm, double _Vfi, 
                                        double* _euler, double* _ar, int kinc, int kstep){

        if(kinc == 1 && kstep==1){
		return;
	}

	int i,error;
	static double **C0=NULL;
        static double **C1=NULL;
        static double **Cel=NULL;
        static double **C0_loc=NULL;
        static double **invC0=NULL;
        static double **invCel=NULL;
	static double **A=NULL;
        static double **invA=NULL;
        static double **S=NULL;
        static double **R66=NULL;
        static double **I=NULL;
        static double **mat1;

        static double *strs_c=NULL;
        static double *dstrs_m=NULL;
        static double *dstrs_i=NULL;
        static double *dstrn_c=NULL;
        static double *dstrn_m=NULL;
        static double *dstrn_i=NULL;

        if(C0==NULL)
        {
 	  mallocmatrix(&C0,6,6);
	  mallocmatrix(&C1,6,6);
          mallocmatrix(&Cel,6,6);
          mallocmatrix(&C0_loc,6,6);

	  mallocmatrix(&invC0,6,6);
          mallocmatrix(&invCel,6,6);

	  mallocmatrix(&A,6,6);
	  mallocmatrix(&invA,6,6);
	  mallocmatrix(&S,6,6);
	  mallocmatrix(&R66,6,6);
	  mallocmatrix(&I,6,6);
	  mallocmatrix(&mat1,6,6);

          mallocvector(&strs_c,6);
          mallocvector(&dstrs_m,6);
          mallocvector(&dstrs_i,6);
          mallocvector(&dstrn_c,6);
          mallocvector(&dstrn_m,6);
          mallocvector(&dstrn_i,6);
        }
	cleartens4(C0);
	cleartens4(C1);
        cleartens4(Cel);
        cleartens4(C0_loc);

	cleartens4(invC0);
        cleartens4(invCel);

	cleartens4(A);
	cleartens4(invA);
	cleartens4(S);
	cleartens4(R66);
	cleartens4(I);
	cleartens4(mat1);

        cleartens2(strs_c);
        cleartens2(dstrs_m);
        cleartens2(dstrs_i);
        cleartens2(dstrn_c);
        cleartens2(dstrn_m);
        cleartens2(dstrn_i);


	for(i=0;i<6;i++){
		I[i][i] = 1.;
	}

	mtx_mat->get_elOD(&(statev_n[idsdv_m]), C0);
	icl_mat->get_elOD(&(statev_n[idsdv_i]), C1);

        get_elOD(statev_n, Cel);

       	inverse(C0,invC0,&error,6);
	if(error!=0) printf("problem while inversing C0 in unload_step of MTSecF\n");

	inverse(Cel,invCel,&error,6);
	if(error!=0) printf("problem while inversing Cel in unload_step of MTSecF\n");

	//if fibers not oriented with e3:
        if ( _ar[0] == 1.0 && _ar[1] == 1.0){
	    eshelby(C0,_ar[0],_ar[1],S);  	
        } 
        else if (fabs(_euler[0]) > 1.e-3 || fabs(_euler[1]) > 1.e-3){
	    eul_mat(_euler, R66);
	    rot4(R66,C0,C0_loc);
	    eshelby(C0_loc,_ar[0],_ar[1],mat1);  	//compute Eshelby's tensor in local frame
	    transpose(R66,R66,6,6);
	    rot4(R66,mat1,S);
	}
	else{
	    eshelby(C0,_ar[0],_ar[1],S);  	
	}

        contraction44(invC0,C1,mat1);
	addtens4(1.,mat1,-1.,I,mat1);	
	contraction44(S,mat1,invA);
	addtens4(1.,I,_Vfm,invA,invA);
	inverse(invA,A,&error,6);
	if(error!=0) printf("problem while inversing invA in unload_step of MT\n");

 
        copyvect(&(statev_n[pos_strs]), strs_c, 6);        //stress in composite at tn
        contraction42(invCel, strs_c, dstrn_c);          //unloading strain of composite at tn
        contraction42(A, dstrn_c, dstrn_i);              //unloading strain of inclusion at tn
        addtens2 (1./_Vfm, dstrn_c, -1.*_Vfi/_Vfm, dstrn_i, dstrn_m); //unloading strain of matrix at tn

        contraction42(C1, dstrn_i, dstrs_i);              //unloading stress of inclusion at tn
        contraction42(C0, dstrn_m, dstrs_m);              //unloading stress of matrix at tn

 // update statev_n to keep the imformation for residual condition 



        for(i=0;i<6;i++){
	strs_n[i]=0.0;	
        statev_n[pos_strs +i]=0.0;                                   //residual stress in composte is 0
        statev_n[pos_strn +i]= statev_n[pos_strn +i]- dstrn_c[i];      //residual strain in composte
        statev_n[pos_dstrn +i]= dstrn[i]+ dstrn_c[i];    // strain increment in composte

        dstrn[i]= statev_n[pos_dstrn +i];

#if Crmatrix_1st==1
         statev_n[idsdv_m + mtx_mat->get_pos_strs()+i]= statev_n[idsdv_m + mtx_mat->get_pos_strs()+i]-dstrs_m[i];  //residual stress in marix
#else
         statev_n[idsdv_m + mtx_mat->get_pos_strs()+i]= 0.0;
#endif

         statev_n[idsdv_m + mtx_mat->get_pos_strn()+i]= statev_n[idsdv_m + mtx_mat->get_pos_strn()+i]-dstrn_m[i];  //residual strain in marix
       //   statev_n[idsdv_m + mtx_mat->get_pos_strn()+i]= statev_n[pos_strn +i];

#if Crinclusion==1
        statev_n[idsdv_i + icl_mat->get_pos_strs()+i]= statev_n[idsdv_i + icl_mat->get_pos_strs()+i]-dstrs_i[i];  //residual stress in inclusion
#else
        statev_n[idsdv_i + icl_mat->get_pos_strs()+i]= 0.0;

#endif     // statev_n[idsdv_i + icl_mat->get_pos_strn()+i]= 0.0;
       statev_n[idsdv_i + icl_mat->get_pos_strn()+i]= statev_n[idsdv_i + icl_mat->get_pos_strn()+i]-dstrn_i[i];  //residual strain in inclusion
        } 
	
}

#ifdef NONLOCALGMSH
double MTSecF_Stoch_Material::getElasticEnergy (double* statev) 
{
    double _Vfi, _Vfm;
    if(pos_vfi == 0){
        _Vfi = vf_i;     
    }
    else{
        _Vfi = statev[pos_vfi];
    }
    _Vfm = 1.0 - _Vfi;

    double eEi = icl_mat->getElasticEnergy(&statev[idsdv_i]);
    double eE0 = mtx_mat->getElasticEnergy(&statev[idsdv_m]);
    double eE = _Vfi*eEi+_Vfm*eE0;

    return eE;

}

double MTSecF_Stoch_Material::getPlasticEnergy(double* statev) 
{
    double _Vfi, _Vfm;
    if(pos_vfi == 0){
        _Vfi = vf_i;     
    }
    else{
        _Vfi = statev[pos_vfi];
    }
    _Vfm = 1.0 - _Vfi;


    double ePi = icl_mat->getPlasticEnergy(&statev[idsdv_i]);
    double eP0 = mtx_mat->getPlasticEnergy(&statev[idsdv_m]);
    double eP = _Vfi*ePi+_Vfm*eP0;

    return eP;
}
#endif

void MTSecF_Stoch_Material::get_refOp(double* statev, double** C, int kinc, int kstep){

    int i,j,k;

    if(kinc < 2 && kstep==1){
	get_elOp(statev, C);
    }
    else{
        k=pos_C;
	for(i=0;i<6;i++){
	    for(j=i;j<6;j++){
		C[i][j] = statev[k++];
		C[j][i] = C[i][j];
	    }
	}
    }		

}

void MTSecF_Stoch_Material::get_elOp(double* statev, double** Cel){

	int i,error;
	static double **C0=NULL;
        static double **C1=NULL;
        static double **invC0=NULL;
        static double **C0_loc=NULL;
        static double **A=NULL;
        static double **invA=NULL;
        static double **S=NULL;
        static double **R66=NULL;
        static double **I=NULL;
        static double **mat1;

        double _Vfi, _Vfm;
        static double *_euler=NULL;
        static double *_ar=NULL;

	if(C0==NULL)
        {
          mallocmatrix(&C0,6,6);
  	  mallocmatrix(&C1,6,6);
	  mallocmatrix(&invC0,6,6);
	  mallocmatrix(&C0_loc,6,6);
	  mallocmatrix(&A,6,6);
	  mallocmatrix(&invA,6,6);
	  mallocmatrix(&S,6,6);
	  mallocmatrix(&R66,6,6);
	  mallocmatrix(&I,6,6);
	  mallocmatrix(&mat1,6,6);

          mallocvector(&_euler,3);
          mallocvector(&_ar,2);
        }
        cleartens4(C0);
	cleartens4(C1);
	cleartens4(invC0);
	cleartens4(C0_loc);
	cleartens4(A);
	cleartens4(invA);
	cleartens4(S);
	cleartens4(R66);
	cleartens4(I);
	cleartens4(mat1);

        for(i=0;i<3;i++) _euler[i]=0.;

        if(pos_vfi == 0){
            _Vfi = vf_i;     
        }
        else{
            _Vfi = statev[pos_vfi];
        }
        _Vfm = 1.0 - _Vfi;

        if( pos_euler == 0){
            copyvect(euler,_euler,3);  
        }
        else{
            _euler[0] = statev[pos_euler];
            _euler[1] = statev[pos_euler+1];
            _euler[2] = statev[pos_euler+2];
        }

        if( pos_aspR == 0){
            copyvect(ar,_ar,2);  
        }
        else if(fabs(statev[pos_aspR]-1.0) <= 1.0e-6){
            _ar[0] = 1000000.0;
            _ar[1] = 1000000.0;
        }
        else{
            _ar[0] = 1.0;
            _ar[1] = statev[pos_aspR];
        }


	for(i=0;i<6;i++){
		I[i][i] = 1.;
	}

	mtx_mat->get_elOp(C0);
	icl_mat->get_elOp(C1);
	
	//if fibers not oriented with e3:
	if ( _ar[0] == 1.0 && _ar[1] == 1.0){
	    eshelby(C0,_ar[0],_ar[1],S);  	
        } 
        else if (fabs(_euler[0]) > 1.e-3 || fabs(_euler[1]) > 1.e-3){
	    eul_mat(_euler, R66);
	    rot4(R66,C0,C0_loc);
	    eshelby(C0_loc,_ar[0],_ar[1],mat1);  	//compute Eshelby's tensor in local frame
	    transpose(R66,R66,6,6);
	    rot4(R66,mat1,S);
	}
	else{
	    eshelby(C0,_ar[0],_ar[1],S);  	
	}

	inverse(C0,invC0,&error,6);
	if(error!=0) printf("problem while inversing C0 in compute_elOp of MT\n");

	contraction44(invC0,C1,mat1);
	addtens4(1.,mat1,-1.,I,mat1);	
	contraction44(S,mat1,invA);
	addtens4(1.,I,_Vfm,invA,invA);
	inverse(invA,A,&error,6);
	if(error!=0) printf("problem while inversing invA in compute_elOp of MT\n");


	addtens4(1.,C1,-1.,C0,mat1);
	contraction44(mat1,A,Cel);
	addtens4(1.,C0,_Vfi,Cel,Cel);

}

void MTSecF_Stoch_Material::get_elOD(double* statev, double** Cel)
{
        int i,error;
	static double **C0=NULL;
        static double **C1=NULL;
        static double **invC0=NULL;
        static double **C0_loc=NULL;
        static double **A=NULL;
        static double **invA=NULL;
        static double **S=NULL;
        static double **R66=NULL;
        static double **I=NULL;
        static double **mat1=NULL;

        double _Vfi, _Vfm;
        static double *_euler=NULL;
        static double *_ar=NULL;

	if(C0==NULL)
        {
          mallocmatrix(&C0,6,6);
	  mallocmatrix(&C1,6,6);
	  mallocmatrix(&invC0,6,6);
	  mallocmatrix(&C0_loc,6,6);
	  mallocmatrix(&A,6,6);
	  mallocmatrix(&invA,6,6);
	  mallocmatrix(&S,6,6);
	  mallocmatrix(&R66,6,6);
	  mallocmatrix(&I,6,6);
	  mallocmatrix(&mat1,6,6);
          mallocvector(&_euler,3);
          mallocvector(&_ar,2);
        }
        cleartens4(C0);
	cleartens4(C1);
	cleartens4(invC0);
	cleartens4(C0_loc);
	cleartens4(A);
	cleartens4(invA);
	cleartens4(S);
	cleartens4(R66);
	cleartens4(I);
	cleartens4(mat1);
        for(i=0;i<3;i++) _euler[i]=0.;
        if(pos_vfi == 0){
            _Vfi = vf_i;     
        }
        else{
            _Vfi = statev[pos_vfi];
        }
        _Vfm = 1.0 - _Vfi;

        if( pos_euler == 0){
            copyvect(euler,_euler,3);  
        }
        else{
            _euler[0] = statev[pos_euler];
            _euler[1] = statev[pos_euler+1];
            _euler[2] = statev[pos_euler+2];
        }

        if( pos_aspR == 0){
            copyvect(ar,_ar,2);  
        }
        else if(fabs(statev[pos_aspR]-1.0) <= 1.0e-6){
            _ar[0] = 1000000.0;
            _ar[1] = 1000000.0;
        }
        else{
            _ar[0] = 1.0;
            _ar[1] = statev[pos_aspR];
        }

	for(i=0;i<6;i++){
		I[i][i] = 1.;
	}

	mtx_mat->get_elOD(&(statev[idsdv_m]), C0);
	icl_mat->get_elOD(&(statev[idsdv_i]), C1);
	
	//if fibers not oriented with e3:
	if ( _ar[0] == 1.0 && _ar[1] == 1.0){
	    eshelby(C0,_ar[0],_ar[1],S);  	
        } 
        else if (fabs(_euler[0]) > 1.e-3 || fabs(_euler[1]) > 1.e-3){
	    eul_mat(_euler, R66);
	    rot4(R66,C0,C0_loc);
	    eshelby(C0_loc,_ar[0],_ar[1],mat1);  	//compute Eshelby's tensor in local frame
	    transpose(R66,R66,6,6);
	    rot4(R66,mat1,S);
	}
	else{
	    eshelby(C0,_ar[0],_ar[1],S);  	
	}

	inverse(C0,invC0,&error,6);
	if(error!=0) printf("problem while inversing C0 in compute_elOp of MT\n");

	contraction44(invC0,C1,mat1);
	addtens4(1.,mat1,-1.,I,mat1);	
	contraction44(S,mat1,invA);
	addtens4(1.,I,_Vfm,invA,invA);
	inverse(invA,A,&error,6);
	if(error!=0) printf("problem while inversing invA in compute_elOp of MT\n");


	addtens4(1.,C1,-1.,C0,mat1);
	contraction44(mat1,A,Cel);
	addtens4(1.,C0,_Vfi,Cel,Cel);


}


#ifdef NONLOCALGMSH

void MTSecF_Stoch_Material:: get_elOp_mtx(double* statev, double** Cel){
       mtx_mat->get_elOp(&statev[idsdv_m],Cel);
}

void MTSecF_Stoch_Material::get_elOp_icl(double* statev, double** Cel){
       icl_mat->get_elOp(&statev[idsdv_i],Cel);
}

void MTSecF_Stoch_Material:: get_elOp_mtx(double** Cel){
       mtx_mat->get_elOp(Cel);
}

void MTSecF_Stoch_Material::get_elOp_icl(double** Cel){
       icl_mat->get_elOp(Cel);
}
#endif

//****end of stochastic material


//**************************************************************************
//By Ling Wu Dec 2016
//Composite material homogenized with Mori-Tanaka scheme with Second-order Incremental-secant method
// "vfi"- volume fraction and "euler" are variables in the case of large deformation.
//**************************************************************************
//constructor
MTSecF_LargD::MTSecF_LargD(double* props, int idmat):MT_Material(props,idmat){

        pos_vfi = nsdv;
        nsdv += 1;
        pos_aspR = nsdv;
        nsdv += 2;
        pos_R66 = nsdv;
        nsdv += 36;
        pos_dR = nsdv;
        nsdv += 9;
}

//destructor
//destructor
MTSecF_LargD::~MTSecF_LargD(){

}
//constbox
int MTSecF_LargD::constbox(double* dstrn, double* strs_n,  double* strs, double* statev_n, double* statev, double** Cref, double*** dCref, double* tau, double** dtau, double** Calgo, double alpha, double* dpdE, double* dstrsdp_bar, double *Sp_bar, double** c_g, double* dFd_d_bar, double* dstrs_dFd_bar, double *dFd_dE, double** c_gF, double* dpdFd, double* dFddp, int kinc, int kstep, double dt){
	//alpha: for mid-point integration law to compute reference MT operator: C_MT = (1-alpha)*C_MT(tn) + alpha*C_MT(tn+1)
  return (1);
}

// unloading step for MTSecF_Material material to reset statev_n in residual condition, by Ling Wu June 2012

void MTSecF_LargD::unload_step(double* dstrn, double* strs_n, double* statev_n, double _Vfm, double _Vfi, 
                                       double** R66, int kinc, int kstep){

        if(kinc == 1 && kstep==1){
          return;
	}

	int i,j,k,error;
        double JaI, JaM, JaC;
        static double **C0= NULL;
        static double **C1= NULL;
        static double **Cel= NULL;
        static double **C0_loc= NULL;
        static double **invC0= NULL;
        static double **invCel= NULL;
        static double **A= NULL;
        static double **invA= NULL;
        static double **S= NULL;
        static double **_R66= NULL;
        static double **I= NULL;
        static double **mat1= NULL;
        static double *strs_c= NULL;
        static double *dstrs_m= NULL;
        static double *dstrs_i= NULL;
        static double *dstrn_c= NULL;
        static double *dstrn_m= NULL;
        static double *dstrn_i=NULL;
        static double *resstrs_m=NULL;
        static double *resstrs_i=NULL;
     

        static double *_ar= NULL;
        static double** Fi0 = NULL;
        static double** Fm0 = NULL;
        static double** Fi1 = NULL;
        static double** Fm1 = NULL;

        if(Fi1 == NULL)
        {
	  mallocmatrix(&Fi1,3,3);
	  mallocmatrix(&Fm1,3,3);
	  mallocmatrix(&Fi0,3,3);
	  mallocmatrix(&Fm0,3,3);
	
          mallocmatrix(&C0,6,6);
          mallocmatrix(&C1,6,6);
          mallocmatrix(&Cel,6,6);
          mallocmatrix(&C0_loc,6,6);

	  mallocmatrix(&invC0,6,6);
          mallocmatrix(&invCel,6,6);

	  mallocmatrix(&A,6,6);
	  mallocmatrix(&invA,6,6);
	  mallocmatrix(&S,6,6);
	  mallocmatrix(&_R66,6,6);
	  mallocmatrix(&I,6,6);
	  mallocmatrix(&mat1,6,6);

          mallocvector(&strs_c,6);
          mallocvector(&dstrs_m,6);
          mallocvector(&dstrs_i,6);
          mallocvector(&dstrn_c,6);
          mallocvector(&dstrn_m,6);
          mallocvector(&dstrn_i,6);
          mallocvector(&resstrs_m,6);
          mallocvector(&resstrs_i,6);
          mallocvector(&_ar,2);
        }
	
        cleartens4(C0);
        cleartens4(C1);
        cleartens4(Cel);
        cleartens4(C0_loc);

	cleartens4(invC0);
        cleartens4(invCel);

	cleartens4(A);
	cleartens4(invA);
	cleartens4(S);
	cleartens4(_R66);
	cleartens4(I);
	cleartens4(mat1);

        cleartens2(strs_c);
        cleartens2(dstrs_m);
        cleartens2(dstrs_i);
        cleartens2(dstrn_c);
        cleartens2(dstrn_m);
        cleartens2(dstrn_i);
        for(i=0;i<2;i++) _ar[i]=0.;      

	for(i=0;i<6;i++){
		I[i][i] = 1.;
	}

        JaI = statev_n[idsdv_i + icl_mat->get_pos_Ja()];
        JaM = statev_n[idsdv_m + mtx_mat->get_pos_Ja()];
        JaC = (vf_i*JaI+(1.0-vf_i)*JaM);

        _ar[0] = statev_n[pos_aspR];
        _ar[1] = statev_n[pos_aspR+1];

	mtx_mat->get_elOD(&(statev_n[idsdv_m]), C0);
	icl_mat->get_elOD(&(statev_n[idsdv_i]), C1);

        get_elOD(statev_n, R66, Cel);

       	inverse(C0,invC0,&error,6);
	if(error!=0) printf("problem while inversing C0 in unload_step of MTSecF\n");

	inverse(Cel,invCel,&error,6);
	if(error!=0) printf("problem while inversing Cel in unload_step of MTSecF\n");

	//if fibers not oriented with e3:
	rot4(R66,C0,C0_loc);
	eshelby(C0_loc,_ar[0],_ar[1],mat1);  	//compute Eshelby's tensor in local frame
	transpose(R66,_R66,6,6);
	rot4(_R66,mat1,S);
	
	
        contraction44(invC0,C1,mat1);
	addtens4(1.,mat1,-1.,I,mat1);	
	contraction44(S,mat1,invA);
	addtens4(1.,I,_Vfm,invA,invA);
	inverse(invA,A,&error,6);
	if(error!=0) printf("problem while inversing invA in unload_step of MT\n");

        for(i=0;i<6;i++){
        //   strs_n[i] = JaC*(_Vfi*(statev_n[idsdv_i+icl_mat->get_pos_strs()+i]) 
         //               + _Vfm*(statev_n[idsdv_m + mtx_mat->get_pos_strs()+i]));
           strs_n[i] = JaC*(1./JaI*_Vfi*(statev_n[idsdv_i+icl_mat->get_pos_strs()+i])
                        + 1./JaM*_Vfm*(statev_n[idsdv_m + mtx_mat->get_pos_strs()+i]));

        }

        copyvect(strs_n, strs_c, 6);        //stress in composite at tn
        contraction42(invCel, strs_c, dstrn_c);          //unloading strain of composite at tn with shear factor sqrt(2)
        contraction42(A, dstrn_c, dstrn_i);              //unloading strain of inclusion at tn with shear factor sqrt(2)
        addtens2 (1./_Vfm, dstrn_c, -1.*_Vfi/_Vfm, dstrn_i, dstrn_m); //unloading strain of matrix at tn with shear factor sqrt(2)

        contraction42(C1, dstrn_i, dstrs_i);              //unloading stress of inclusion at tn 
        contraction42(C0, dstrn_m, dstrs_m);              //unloading stress of matrix at tn

        // For inclusion update R66 according to the results of previous step: to do before unloading. 
        //At this stage the phases strain are not the elastic ones 

        vect_to_matrix(3, &(statev_n[idsdv_i+icl_mat->get_pos_Ri()]), Fi0);         
        //printf("Rincl: %f %f %f  %f %f %f  %f %f %f\n", Fi0[0][0], Fi0[0][1], Fi0[0][2], Fi0[1][0], Fi0[1][1], Fi0[1][2], Fi0[2][0], Fi0[2][1], Fi0[2][2]); 

        vect_to_matrix(3, &(statev_n[idsdv_i+icl_mat->get_pos_F()]), Fi0);         
        vect_to_matrix(3, &(statev_n[idsdv_m+mtx_mat->get_pos_F()]), Fm0);         
        //printf("Fincl: %f %f %f  %f %f %f  %f %f %f\n", Fi0[0][0], Fi0[0][1], Fi0[0][2], Fi0[1][0], Fi0[1][1], Fi0[1][2], Fi0[2][0], Fi0[2][1], Fi0[2][2]); 
        UpdateIncl(ar, euler, Fi0, _ar, R66);
   
        statev_n[pos_aspR] = _ar[0];
        statev_n[pos_aspR+1] = _ar[1]; 

        //*************
        matrix_to_vect(6, R66, &(statev_n[pos_R66]));


        // update statev_n to keep the imformation for residual condition 
        // We use the strain in pos_strn  and in pos_strn with the shear factor of *sqrt(2)!!!!

        for(i=0;i<6;i++){
          strs_n[i]=0.0;	
          statev_n[pos_strs +i]=0.0;                                   //residual stress in composte is 0
          statev_n[pos_strn +i]= statev_n[pos_strn +i]- dstrn_c[i];      //residual strain in composte with shear factor sqrt(2)

//#if Crmatrix_LD==1
          dstrs_m[i]    = -dstrs_m[i]; //*JaM;
//#else
//          dstrs_m[i]    = -statev_n[idsdv_m + mtx_mat->get_pos_strs()+i];
//#endif 
          statev_n[idsdv_m + mtx_mat->get_pos_strs()+i]= statev_n[idsdv_m + mtx_mat->get_pos_strs()+i]+dstrs_m[i];  //residual stress in matrix strain
//#if Crinclusion_LD==1
          dstrs_i[i]    = -dstrs_i[i]; //*JaI;
//#else
//          dstrs_i[i]    = -statev_n[idsdv_i + icl_mat->get_pos_strs()+i];
//#endif  
          statev_n[idsdv_i + icl_mat->get_pos_strs()+i]= statev_n[idsdv_i + icl_mat->get_pos_strs()+i]+dstrs_i[i];  //residual stress in inclusion
        }
        //now we compute the elastic residual strain (!!! is residual was canceled they are 0, and we loose the basis)
        for(i=0;i<6;i++){
          resstrs_m[i] = statev_n[idsdv_m + mtx_mat->get_pos_strs()+i]; //*JaM;
          dstrs_m[i]   = dstrs_m[i]; //*JaM;
          resstrs_i[i] = statev_n[idsdv_i + icl_mat->get_pos_strs()+i];//*JaI;
          dstrs_i[i]   = dstrs_i[i];//*JaI;
        }
        contraction42(invC0, resstrs_m, &(statev_n[idsdv_m + mtx_mat->get_pos_strn()])); //with shear factor sqrt(2)
        contraction42(invC0, dstrs_m, dstrn_m);  //with shear factor sqrt(2)
        inverse(C1,invC0,&error,6);
        contraction42(invC0, resstrs_i, &(statev_n[idsdv_i + icl_mat->get_pos_strn()])); //with shear factor sqrt(2) 
        contraction42(invC0, dstrs_i, dstrn_i);  //with shear factor sqrt(2)

#if Crmatrix_LD==0
        for(i=0;i<6;i++)
          statev_n[idsdv_m + mtx_mat->get_pos_strs()+i]=0.;
#endif
#if Crinclusion_LD==0
        for(i=0;i<6;i++)
          statev_n[idsdv_i + icl_mat->get_pos_strs()+i]=0.;
#endif

        //we rotate the residual strain in composite (not yet in the phases, it will be done after)
        Rot_vect(&(statev_n[pos_dR]), &(statev_n[pos_strn])); //with shear factor sqrt(2)  


        for(i=0;i<6;i++){
           statev_n[pos_dstrn +i]= dstrn[i]- statev_n[pos_strn+ i];    // strain increment in composte
           dstrn[i]= statev_n[pos_dstrn +i];
        } 
        //update F in phases AFTER UNLOADING 
        UpdateF(Fi0, dstrn_i, Fi1, sqrt(2.)); 
        UpdateF(Fm0, dstrn_m, Fm1, sqrt(2.));
        matrix_to_vect(3, Fi1, &(statev_n[idsdv_i+icl_mat->get_pos_F()]));
        matrix_to_vect(3, Fm1, &(statev_n[idsdv_m+mtx_mat->get_pos_F()]));

}

#ifdef NONLOCALGMSH
double MTSecF_LargD::getElasticEnergy (double* statev) 
{
    double _Vfi, _Vfm;
    _Vfi = statev[pos_vfi];
    _Vfm = 1.0 - _Vfi;

    double eEi = icl_mat->getElasticEnergy(&statev[idsdv_i]);
    double eE0 = mtx_mat->getElasticEnergy(&statev[idsdv_m]);
    double eE = _Vfi*eEi+_Vfm*eE0;

    return eE;
}

double MTSecF_LargD::getPlasticEnergy(double* statev) 
{
    double _Vfi, _Vfm;
    _Vfi = statev[pos_vfi];    
    _Vfm = 1.0 - _Vfi;

    double ePi = icl_mat->getPlasticEnergy(&statev[idsdv_i]);
    double eP0 = mtx_mat->getPlasticEnergy(&statev[idsdv_m]);
    double eP = _Vfi*ePi+_Vfm*eP0;

    return eP;
}
#endif

void MTSecF_LargD::get_refOp(double* statev, double** _R66, double** C, int kinc, int kstep){


	int i,j,k;

	if(kinc < 2 && kstep==1){
		get_elOp(statev, _R66, C);
	}
	else{
		k=pos_C;
		for(i=0;i<6;i++){
			for(j=i;j<6;j++){
				C[i][j] = statev[k++];
				C[j][i] = C[i][j];
			}
		}
	}		

}

void MTSecF_LargD::get_elOp(double* statev, double** _R66, double** Cel){

	int i,error;
	static double **C0=NULL;
        static double **C1=NULL;
        static double **invC0=NULL;
        static double **C0_loc=NULL;
        static double **A=NULL;
        static double **invA=NULL;
        static double **S=NULL;
        static double **R66=NULL;
        static double **I=NULL;
        static double **mat1=NULL;
        static double *_ar=NULL;

        double _Vfi, _Vfm;

	if(C0==NULL)
        {
          mallocmatrix(&C0,6,6);
  	  mallocmatrix(&C1,6,6);
	  mallocmatrix(&invC0,6,6);
	  mallocmatrix(&C0_loc,6,6);
	  mallocmatrix(&A,6,6);
	  mallocmatrix(&invA,6,6);
	  mallocmatrix(&S,6,6);
	  mallocmatrix(&R66,6,6);
	  mallocmatrix(&I,6,6);
	  mallocmatrix(&mat1,6,6);

          mallocvector(&_ar,2);
        }
        cleartens4(C0);
	cleartens4(C1);
	cleartens4(invC0);
	cleartens4(C0_loc);
	cleartens4(A);
	cleartens4(invA);
	cleartens4(S);
	cleartens4(R66);
	cleartens4(I);
	cleartens4(mat1);

        for(i=0;i<2;i++) _ar[i]=0.;

        _Vfi = statev[pos_vfi];
        _Vfm = 1.0 - _Vfi;


	for(i=0;i<6;i++){
		I[i][i] = 1.;
	}

	mtx_mat->get_elOp(C0);
	icl_mat->get_elOp(C1);
	
	//if fibers not oriented with e3:
        mallocvector(&_ar,2);


        _Vfi = statev[pos_vfi];
        _Vfm = 1.0 - _Vfi;

        _ar[0] = statev[pos_aspR];
        _ar[1] = statev[pos_aspR+1];


	for(i=0;i<6;i++){
		I[i][i] = 1.;
	}

	mtx_mat->get_elOD(&(statev[idsdv_m]), C0);
	icl_mat->get_elOD(&(statev[idsdv_i]), C1);
	
	rot4(_R66,C0,C0_loc);
	eshelby(C0_loc,_ar[0],_ar[1],mat1);  	//compute Eshelby's tensor in local frame
	transpose(_R66,R66,6,6);
	rot4(R66,mat1,S);

	inverse(C0,invC0,&error,6);
	if(error!=0) printf("problem while inversing C0 in compute_elOp of MT\n");

	contraction44(invC0,C1,mat1);
	addtens4(1.,mat1,-1.,I,mat1);	
	contraction44(S,mat1,invA);
	addtens4(1.,I,_Vfm,invA,invA);
	inverse(invA,A,&error,6);
	if(error!=0) printf("problem while inversing invA in compute_elOp of MT\n");


	addtens4(1.,C1,-1.,C0,mat1);
	contraction44(mat1,A,Cel);
	addtens4(1.,C0,_Vfi,Cel,Cel);

	//freematrix(C0,6);
	//freematrix(C1,6);
	//freematrix(invC0,6);
	//freematrix(C0_loc,6);
	//freematrix(A,6);
	//freematrix(invA,6);
	//freematrix(S,6);
	//freematrix(R66,6);
	//freematrix(I,6);
	//freematrix(mat1,6);
        //free(_ar);
}

void MTSecF_LargD::get_elOD(double* statev, double** _R66, double** Cel)
{
        int i,error;
	static double **C0=NULL;
        static double **C1=NULL;
        static double **invC0=NULL;
        static double **C0_loc=NULL;
        static double **A=NULL;
        static double **invA=NULL;
        static double **S=NULL;
        static double **R66=NULL;
        static double **I=NULL;
        static double **mat1=NULL;
        static double *_ar=NULL;

        double _Vfi, _Vfm;

	if(C0==NULL)
        {
          mallocmatrix(&C0,6,6);
  	  mallocmatrix(&C1,6,6);
	  mallocmatrix(&invC0,6,6);
	  mallocmatrix(&C0_loc,6,6);
	  mallocmatrix(&A,6,6);
	  mallocmatrix(&invA,6,6);
	  mallocmatrix(&S,6,6);
	  mallocmatrix(&R66,6,6);
	  mallocmatrix(&I,6,6);
	  mallocmatrix(&mat1,6,6);

          mallocvector(&_ar,2);
        }
        cleartens4(C0);
	cleartens4(C1);
	cleartens4(invC0);
	cleartens4(C0_loc);
	cleartens4(A);
	cleartens4(invA);
	cleartens4(S);
	cleartens4(R66);
	cleartens4(I);
	cleartens4(mat1);

        _Vfi = statev[pos_vfi];
        _Vfm = 1.0 - _Vfi;

        _ar[0] = statev[pos_aspR];
        _ar[1] = statev[pos_aspR+1];


	for(i=0;i<6;i++){
		I[i][i] = 1.;
	}

	mtx_mat->get_elOD(&(statev[idsdv_m]), C0);
	icl_mat->get_elOD(&(statev[idsdv_i]), C1);
	
	rot4(_R66,C0,C0_loc);
	eshelby(C0_loc,_ar[0],_ar[1],mat1);  	//compute Eshelby's tensor in local frame
	transpose(_R66,R66,6,6);
	rot4(R66,mat1,S);
		 


	inverse(C0,invC0,&error,6);
	if(error!=0) printf("problem while inversing C0 in compute_elOp of MT\n");

	contraction44(invC0,C1,mat1);
	addtens4(1.,mat1,-1.,I,mat1);	
	contraction44(S,mat1,invA);
	addtens4(1.,I,_Vfm,invA,invA);
	inverse(invA,A,&error,6);
	if(error!=0) printf("problem while inversing invA in compute_elOp of MT\n");


	addtens4(vf_i,C1,-1.0*vf_m*_Vfi/_Vfm,C0,mat1);
	contraction44(mat1,A,Cel);
	addtens4(vf_m/_Vfm,C0,1.0,Cel,Cel);

	//freematrix(C0,6);
	//freematrix(C1,6);
	//freematrix(invC0,6);
	//freematrix(C0_loc,6);
	//freematrix(A,6);
	//freematrix(invA,6);
	//freematrix(S,6);
	//freematrix(R66,6);
	//freematrix(I,6);
	//freematrix(mat1,6);

        //free(_ar);
}


//********************************************************************************************
// COMPOSITE - SELF-CONSISTENT
//PROPS: MODEL, IDDAM, mtx (1 or 0), Nicl, IDMAT matrix (if any), IDMAT inclusion, IDTOP inclusion,
//  (material properties of each phase), topology of inclusions: vf, ar1, ar2, 3 euler angles
//STATEV: strn, strs, dstrn, SC macro operator (36 components) , (statev of each phase)   
//********************************************************************************************
//constructor
SC_Material::SC_Material(double* props, int idmat):Material(props, idmat){

	int i,idmat_m, idmat_i, idtop_i,k;
	int iddam, idclength;
	double sum_vf=0.;
	nsdv=54;  		//18 (strn,strs,dstrn) + 36 (SC macro tangent operator)
	iddam = (int) props[idmat+1];
        idclength = (int)props[idmat+2];
	mtx = (int) props[idmat+3];
	Nicl = (int) props[idmat+4];
	Nph = mtx + Nicl;
	

	if(Nicl==0){
		printf("Cannot use SC scheme without any inclusion\n");
	}

	//allocate memory
	icl_mat = (Material**) malloc(Nicl*sizeof(Material*));	
	mallocmatrix(&ar,Nicl,2);
	mallocmatrix(&euler,Nicl,3);
	mallocvector(&vf_i,Nicl);
	idsdv_i = (int*) malloc(Nicl*sizeof(int));


	//create materials for each phase, initialize topology and count state variables
	if(mtx){
		idmat_m=(int) props[idmat+5];
		mtx_mat = init_material(props,idmat_m);  //lecture de matrice 
		idsdv_m = nsdv;
		nsdv += mtx_mat->get_nsdv();
		k=idmat+6;
		
	}
	else{	
		k=idmat+5; // il commence par la lecture de l'inclusion 
		mtx_mat=NULL;	
	}

	for(i=0;i<Nicl;i++){
		idmat_i=(int) props[k++];
		idtop_i=(int) props[k++];
		icl_mat[i] = init_material(props,idmat_i);
		vf_i[i] = props[idtop_i++];
		ar[i][0] = props[idtop_i++];
		ar[i][1] = props[idtop_i++];
		euler[i][0] = props[idtop_i++];
		euler[i][1] = props[idtop_i++];
		euler[i][2] = props[idtop_i++];
		idsdv_i[i] = nsdv;		
		nsdv += icl_mat[i]->get_nsdv();
		sum_vf += vf_i[i];
	}

	if(mtx) vf_m = 1.-sum_vf;

	if( fabs(sum_vf-1.) > 1e-4) {
		printf("the sum of phase volume fractions is not equal to 1!: %.10e\n",sum_vf);
	} 

	//set positions of strn, strs, dstrn and tangent operator in statev vector
	pos_strn=0;
	pos_strs=6;
	pos_dstrn=12;
	pos_C = 18;

        pos_p =0; // For the input of clength->creat_cg, not implemented for this material

	dam = init_damage(props,iddam);
	if(dam){
		pos_dam = nsdv;
		nsdv += dam->get_nsdv();
	}


        clength = init_clength(props,idclength);
}
#ifdef NONLOCALGMSH
int SC_Material::get_pos_currentplasticstrainfornonlocal() const
{
	if(mtx_mat->get_pos_currentplasticstrainfornonlocal()>=0)
	     return mtx_mat->get_pos_currentplasticstrainfornonlocal()+idsdv_m;
        return -1;

}
int SC_Material::get_pos_effectiveplasticstrainfornonlocal() const
{
	if(mtx_mat->get_pos_effectiveplasticstrainfornonlocal()>=0)
             return mtx_mat->get_pos_effectiveplasticstrainfornonlocal()+idsdv_m;
	return -1;
}

int SC_Material::get_pos_damagefornonlocal() const
{
	if(mtx_mat->get_pos_damagefornonlocal() >=0)
	     return mtx_mat->get_pos_damagefornonlocal();
	return -1;
}
int SC_Material::get_pos_mtx_stress () const
{ 
   return idsdv_m + mtx_mat->get_pos_strs();
}
int SC_Material::get_pos_inc_stress () const
{
   return idsdv_i[0] + icl_mat[0]->get_pos_strs();
}
int SC_Material::get_pos_mtx_strain () const
{ 
   return idsdv_m + mtx_mat->get_pos_strn();
}
int SC_Material::get_pos_inc_strain () const
{
   return idsdv_i[0] + icl_mat[0]->get_pos_strn();
}
double SC_Material::getElasticEnergy (double* statev) 
{
    int i=0;
    double eEi = 0.;
    double eE0 = mtx_mat->getElasticEnergy(&statev[idsdv_m]);
    for(i=0;i<Nicl;i++)
	eEi+=vf_i[i]*icl_mat[i]->getElasticEnergy(&statev[idsdv_i[i]]);
    double eE = eEi+vf_m*eE0;
    return eE;

}

double SC_Material::getPlasticEnergy (double* statev) 
{
    int i=0;
    double ePi = 0.;
    double eP0 = mtx_mat->getPlasticEnergy(&statev[idsdv_m]);
    for(i=0;i<Nicl;i++) ePi += vf_i[i]*icl_mat[i]->getPlasticEnergy(&statev[idsdv_i[i]]);
    double eP = ePi+vf_m*eP0;

    return eP;
}

#endif

//destructor
SC_Material::~SC_Material(){

	int i;
	if(mtx_mat) delete mtx_mat;
	for(i=0;i<Nicl;i++){
		if(icl_mat[i]) delete icl_mat[i];
	}
	free(icl_mat);

	freematrix(ar,Nicl);
	freematrix(euler,Nicl);
	free(idsdv_i);
	free(vf_i);

	if(dam) delete(dam);
        if(clength) delete clength;

}

//print
void SC_Material::print(){

	int i;
	printf("Self-Consistent\n");
	if(dam) dam->print();
	printf("Number of phases: %d\n", Nicl);
	
	if(mtx){
		printf("matrix, volume fraction: %lf\n",vf_m);
		mtx_mat->print();
	}
	for(i=0;i<Nicl;i++){
		printf("inclusion %d, volume fraction: %lf\n",i+1,vf_i[i]);
		printf("aspect ratio: %lf, %lf\n",ar[i][0],ar[i][1]);
		printf("euler angles: %lf, %lf, %lf\n",euler[i][0],euler[i][1],euler[i][2]);	
		(icl_mat[i])->print();
	}
	
}

//constbox
int SC_Material::constbox(double* dstrn, double* strs_n,  double* strs, double* statev_n, double* statev, double** Cref, double*** dCref, double* tau, double** dtau, double** Calgo, double alpha, double* dpdE, double* strs_dDdp_bar, double *Sp_bar, double** c_g, double* dFd_d_bar, double* dstrs_dFd_bar, double *dFd_dE, double** c_gF, double* dpdFd, double* dFddp, int kinc, int kstep, double dt){
	

  return (1);
}

void SC_Material::get_refOp(double* statev_n, double** C, int kinc, int kstep){

	int i,j;
	
	//first increment: use reference operator of matrix material or of first inclusion 
	//TO DO (better): compute elastic, SC operator
	if((kinc < 2) && (kstep==1)){
		if(mtx){
			mtx_mat->get_elOp(C);
		}
		else{
			icl_mat[0]->get_elOp(C);
		}
	}
	//else: get C from statev
	else{
		for(i=0;i<6;i++){
			for(j=0;j<6;j++){
				C[i][j] = statev_n[18 + (i*6) + j];
			}
		}
	}

}


void SC_Material::get_elOp(double** Cel){

	printf("get_elOp not implemented for Self-Consistent material\n");
}

void SC_Material::get_elOD(double* statev, double** Cel)
{
     printf("get_elOD not implemented for Self-Consistent material\n");
}

void SC_Material::unload_step(double* dstrn, double* strs_n, double* statev_n, int kinc, int kstep){

       printf("unload_step not implemented for SC material\n");
}

//********************************************************************************************
// COMPOSITE - Two-step Mori-Tanaka/Voight 
//PROPS: MODEL, IDDAM, mtx (1 or 0), Nph, IDMAT matrix (if any), IDMAT inclusion, IDTOP inclusion,
//  (material properties of each phase), topology of inclusions: vf, ar1, ar2,  and Matrix orientation a
//STATEV: strn, strs, dstrn, MT macro operator (21 components) , (statev of each phase)   
//********************************************************************************************
//constructor

MT_VT_Material::MT_VT_Material(double* props, int idmat):Material(props, idmat){

        int i, j, k, l, lay, idclength, idtop, iddam, idmat_i, sizetable, ID_Mat_First_Step,nAR, Nlayers,nbpropperlayer,materialIndex;
        int *Total_number_phase_tmp;
        double a11,a22,a33,a12,a13,a23,corEuler1,corEuler2,corEuler3;
        double **a;
        double **probDensityList, **facetList,  **thetaList, **phiList;
        double *props_ph;
	double ***RBasis;
	double *eulerBasis;
        double Wtot, euler1, euler2, dir[3], dirtmp[3], s2;
	double **weightAR, **ARList;

        mallocmatrix(&a,3,3);
        mallocvector(&eulerBasis,3);
        
        nsdv=39;  //18 (strn,strs,dstrn) + 21 (MT macro tangent operator) 

	Total_number_of_AR_per_phase=1; //700; //50; //default value
	
 	iddam = (int) props[idmat+1];
        idclength = (int)props[idmat+2];
		
        k=idmat+5;
  

        idtop=(int) props[k];
        idmat_i=0; //identie material position
        Nlayers=(int) props[idtop];
        weightAR=new double*[Nlayers];
        ARList=new double*[Nlayers];;
        probDensityList=new double*[Nlayers];
        facetList=new double*[Nlayers];
        thetaList=new double*[Nlayers];
        phiList=new double*[Nlayers];
        RBasis=new double**[Nlayers];
        Total_number_phase_tmp=new int[Nlayers];
        Total_number_phase=0;
        nbpropperlayer=14;
        for(lay=0; lay<Nlayers; lay++)
        {

          N_theta_increment =  (int) props[idtop+4+lay*nbpropperlayer]; // phase number's in theta direction
            
          if((int)(N_theta_increment/2)*2+1 !=N_theta_increment){
                N_theta_increment+=1;
		printf("N_theta_increment set to %d\n",N_theta_increment);
	  } 
        
          //read the orientation matrix 
       
          k=idtop+5+lay*nbpropperlayer;
          mallocvector(&weightAR[lay],Total_number_of_AR_per_phase);
          mallocvector(&ARList[lay],Total_number_of_AR_per_phase);
          mallocmatrix(&RBasis[lay],3,3);
	
          a11=props[k++];
          a22=props[k++];
          a33=props[k++];
          a12=props[k++];
          a23=props[k++];
          a13=props[k++];
          corEuler1=corEuler2=corEuler3=0.;
          // the Gaussian identification is only reliable is a1>a2>a3 because of the order of theta and phi
          if(a11> a22 and a22> a33) //OK
          {
            a[0][0]=a11; a[1][1]=a22; a[2][2]=a33; 
            a[0][1]=a[1][0]=a12; a[1][2]=a[2][1]=a23; a[0][2]=a[2][0]=a13;
	    corEuler1=corEuler2=corEuler3=0.;
          }
          else if(a11> a33 and a33> a22) //OK
          {
            a[0][0]=a11; a[1][1]=a33; a[2][2]=a22; 
            a[0][1]=a[1][0]=a13; a[1][2]=a[2][1]=-a23; a[0][2]=a[2][0]=-a12;
            corEuler1=0;
            corEuler2=-90.;
            corEuler3=0.;
          }
          else if(a22> a11 and a11> a33) //OK
          {
            a[0][0]=a22; a[1][1]=a11; a[2][2]=a33; 
            a[0][1]=a[1][0]=-a12; a[1][2]=a[2][1]=a13; a[0][2]=a[2][0]=-a23;
            corEuler1=-90.;
            corEuler2=0.;
            corEuler3=0.;
          }
          else if(a22> a33 and a33> a11)//OK
          {
            a[0][0]=a22; a[1][1]=a33; a[2][2]=a11; 
            a[0][1]=a[1][0]=-a23; a[1][2]=a[2][1]=-a13; a[0][2]=a[2][0]=-a12;
            corEuler1=0.;
            corEuler2=90.;
            corEuler3=-90.;
          }
          else if(a33> a11 and a11> a22) //OK
          {
            a[0][0]=a33; a[1][1]=a11; a[2][2]=a22; 
            a[0][1]=a[1][0]=-a13; a[1][2]=a[2][1]=a12; a[0][2]=a[2][0]=-a23;
            corEuler1=-90.;
            corEuler2=90.;
            corEuler3=0.;
          }
          else if(a33> a22 and a22> a11)//OK
          {
            a[0][0]=a33; a[1][1]=a22; a[2][2]=a11; 
            a[0][1]=a[1][0]=a23; a[1][2]=a[2][1]=-a12; a[0][2]=a[2][0]=-a13;
            corEuler1=90.;
            corEuler2=90.;
            corEuler3=90.;
          }
          else
          {
            printf("MT_VT: enter the matrix a in descending order and use Euler angle for rotations\n");
            exit(0);
          }
          

          ID_Mat_First_Step = props[k++];  // read the identie of schemas for the first step : 4:MT ,8:MTSecF , 6:MTSecFirStoch .....
        
		   
          eulerBasis[0]= props[k++]+corEuler1;   //read rotation angle -->(OZ)
          eulerBasis[1]= props[k++]+corEuler2; //read rotation angle -->(OX)
          eulerBasis[2]= props[k++]+corEuler3;    //read rotation angle -->(OZ)
	  eul_mat33(eulerBasis,RBasis[lay]);
	  transpose(RBasis[lay],RBasis[lay],3,3);
	  //rot33(RBasis,atmp, a); //  we will apply the rotation later after Gaussian identification 
                                 //  which is only reliable is a1>a2>a3 because of the order of theta and phi
          Total_number_phase_tmp[lay] =generateWeight(N_theta_increment, a, &probDensityList[lay], &facetList[lay],  &thetaList[lay], &phiList[lay]);
	  Total_number_phase+=Total_number_phase_tmp[lay]*Total_number_of_AR_per_phase;
        }
	//allocate memory
	ph_mat = (Material**) malloc(Total_number_phase*sizeof(Material*));

        idsdv_i = (int*) malloc(Total_number_phase*sizeof(int));
        Wgt=(double*) malloc(Total_number_phase*sizeof(double)); // vector of Weight

	//create materials for each phase, initialize topology and count state variables

        mallocvector(&props_ph,idtop+6);
        for(i=0;i<idtop;i++){  //  we stop just before inclusin geometry 
          props_ph[i]=props[i];
        } 
        materialIndex=0;
        for(lay=0; lay<Nlayers; lay++)
        {     
          k=idtop+1+lay*nbpropperlayer;
          for(i=0;i<Total_number_phase_tmp[lay];i++){
            props_ph[idtop]=props[k]; //volume fraction
            props_ph[idtop+1]=props[k+1]; //AR1
            props_ph[idtop+2]=props[k+2]; //AR2
            ID_Mat_First_Step = props[k+10];  // read the identie of schemas for the first step : 4:MT ,8:MTSecF , 6:MTSecFirStoch .....
     
            props_ph[idmat_i] = ID_Mat_First_Step; //identie of schemas for the first step

            dirtmp[0]= cos(phiList[lay][i])*sin(thetaList[lay][i]);
            dirtmp[1]= sin(phiList[lay][i])*sin(thetaList[lay][i]);
            dirtmp[2]= cos(thetaList[lay][i]);
            for(j=0;j<3;j++)
            {
              dir[j]=0.;
              for(l=0;l<3;l++)
                dir[j]+=RBasis[lay][j][l]*dirtmp[l];
            }

            euler2 = acos(dir[2]);
            s2 = sqrt(1.-cos(euler2)*cos(euler2));
            euler1 = 0.;
            if(s2!=0.) 
              if(fabs(dir[1]/s2)<1.)
                euler1=acos(-1.*dir[1]/s2);
	      else
	        euler1=pi;
            if(dir[0]*s2*sin(euler1)<0.) euler1*=-1.;

            props_ph[idtop+3]=euler1*180./pi; 
            props_ph[idtop+4]=euler2*180./pi;              
            props_ph[idtop+5]=0.; 
          //verification
          //double *tmp,*eulertmp;
	  //double **Rtmp;
          //mallocvector(&tmp,3);
	  //mallocvector(&eulertmp,3);
	  //mallocmatrix(&Rtmp,3,3);
	  //eulertmp[0]=euler1*180./pi;
	  //eulertmp[1]=euler2*180./pi;
	  //eulertmp[2]=0.;
	  //eul_mat33(eulertmp,Rtmp);
	  //transpose(Rtmp,Rtmp,3,3);
	  //for(int j=0;j<3;j++)
          //  tmp[j]=Rtmp[j][2]; 
            if(Total_number_of_AR_per_phase>1)
	    {
	      getARWeight(thetaList[lay][i], phiList[lay][i], Total_number_of_AR_per_phase, weightAR[lay],ARList[lay]);
	    }
	    for(nAR=0;nAR<Total_number_of_AR_per_phase; nAR++)
	    {
	      Wgt[materialIndex+i*Total_number_of_AR_per_phase+nAR]    = probDensityList[lay][i]*facetList[lay][i]/float(Nlayers);
	      //here we need to change AR
	      if(Total_number_of_AR_per_phase>1)
	      {
	        props_ph[idtop+1]=ARList[lay][nAR];
	        props_ph[idtop+2]=ARList[lay][nAR];
                Wgt[materialIndex+i*Total_number_of_AR_per_phase+nAR]=weightAR[lay][nAR]/float(Nlayers);
	      }
              ph_mat[materialIndex+i*Total_number_of_AR_per_phase+nAR] = init_material(props_ph,idmat_i); // initialisation of phase
              //printf("phase prop: p %f %f %f, vi %f, AR %f %f, Weight %f\n",dir[0],dir[1],dir[2],props_ph[idtop],props_ph[idtop+1],props_ph[idtop+2],Wgt[materialIndex+i*Total_number_of_AR_per_phase+nAR]      );
	      //for(j=0; j<idtop+6; j++)
	      //  printf("phase %d prop: %e\n",materialIndex+i*Total_number_of_AR_per_phase+nAR,props_ph[j]);
              //check if damage
              if(i==0)
              {
                if(ph_mat[0]->getDamage()!=NULL)
                {
                  pos_dam=nsdv;
                  nsdv+=4;   // 1 pbar + Delta p bar + 1 D + p local
                  pos_p=nsdv-1;
                  withDamage=true;
	          //dam = ph_mat[0]->getDamage();
                }
	        else
	        {
                  withDamage=false;
	          dam=NULL;
	        }
              }
              idsdv_i[materialIndex+i*Total_number_of_AR_per_phase+nAR] = nsdv;		
              nsdv += ph_mat[materialIndex+i*Total_number_of_AR_per_phase+nAR]->get_nsdv();
            }
          }
          materialIndex=materialIndex+Total_number_phase_tmp[lay]*Total_number_of_AR_per_phase;
        }
        Wtot=0.;
	for(i=0;i<Total_number_phase;i++)
	    Wtot+=Wgt[i];
        printf(" %d phases of total weight %.10e, with %d internal variables\n", Total_number_phase, Wtot, nsdv);      

       
	//set positions of strn, strs, dstrn and tangent operator in statev vector
	pos_strn=0;
	pos_strs=6;
	pos_dstrn=12;
	pos_C = 18;

        clength = init_clength(props,idclength);

        free(props_ph);
	freematrix(a,3);
        for(lay=0;lay<Nlayers;lay++)
        {
          free(weightAR[lay]);
	  free(ARList[lay]);
          free(probDensityList[lay]);
          free(thetaList[lay]);
          free(phiList[lay]);
          free(facetList[lay]);
          freematrix(RBasis[lay],3);
        }
        free(Total_number_phase_tmp);
        free(RBasis);
	free(weightAR);
	free(ARList);
        free(probDensityList);
        free(thetaList);
        free(phiList);
        free(facetList);
	free(eulerBasis);
}      

#ifdef NONLOCALGMSH
int MT_VT_Material::get_pos_currentplasticstrainfornonlocal() const
{
   if(get_pos_p()>=0)
      return pos_p;
   return -1;

}
int MT_VT_Material::get_pos_effectiveplasticstrainfornonlocal() const
{
	if(get_pos_dam()>=0)
             return pos_dam;
	return -1;
}

int MT_VT_Material::get_pos_damagefornonlocal() const
{
	if(get_pos_dam() >=0)
	     return pos_dam+2;
	return -1;
}
int MT_VT_Material::get_pos_mtx_stress () const
{ 
   // at this point we take the value of the first phase
   return idsdv_i[0] + (ph_mat[0])->get_pos_mtx_stress ();
}
int MT_VT_Material::get_pos_inc_stress () const
{
   // at this point we take the value of the first phase
   return idsdv_i[0] + (ph_mat[0])->get_pos_inc_stress ();
}
int MT_VT_Material::get_pos_mtx_strain () const
{ 
   // at this point we take the value of the first phase
   return idsdv_i[0] + (ph_mat[0])->get_pos_mtx_strain ();
}
int MT_VT_Material::get_pos_inc_strain () const
{
   // at this point we take the value of the first phase
   return idsdv_i[0] + (ph_mat[0])->get_pos_inc_strain ();
}
double MT_VT_Material::getElasticEnergy (double* statev) 
{
    int i=0;
    double eE = 0.;
    for(i=0;i<Total_number_phase;i++)
	eE+=Wgt[i]*ph_mat[i]->getElasticEnergy(&statev[idsdv_i[i]]);;
    return eE;

}

double MT_VT_Material::getPlasticEnergy (double* statev) 
{
    int i=0;
    double eP = 0.;
    for(i=0;i<Total_number_phase;i++) eP += Wgt[i]*ph_mat[i]->getPlasticEnergy(&statev[idsdv_i[i]]);

    return eP;
}

#endif


//destructor
MT_VT_Material::~MT_VT_Material(){

  int i;
  for(i=0;i<Total_number_phase;i++){
    if(ph_mat[i]) delete ph_mat[i];
  }
  free(ph_mat);
  free(idsdv_i);
  if(clength) delete clength;
  free(Wgt);
  //if(dam) delete(dam);
}

void MT_VT_Material::print(){

	int i;
	printf("Two-step Mori-Tanaka/Voigt\n");
	if(dam) dam->print();
	printf("Number of phases: %d\n", Total_number_phase);

	for(i=0;i<Total_number_phase;i++){
			
		(ph_mat[i])->print();
	}
	
}

//constbox
int MT_VT_Material::constbox(double* dstrn, double* strs_n,  double* strs, double* statev_n, double* statev, double** Cref, double*** dCref, double* tau, double** dtau, double** Calgo, double alpha, double* dpdE, double* strs_dDdp_bar, double *Sp_bar, double** c_g, double* dFd_d_bar, double* dstrs_dFd_bar, double *dFd_dE, double** c_gF, double* dpdFd, double* dFddp, int kinc, int kstep, double dt){
	


  return (1);
} 
void MT_VT_Material::get_refOp(double* statev_n, double** C, int kinc, int kstep){

  int i,j;
  static double **Ctmp=NULL ;
  if(Ctmp==NULL){	
    mallocmatrix(&Ctmp,6,6);
  }
  cleartens4(Ctmp);
  //first increment: use reference operator of different phases  
  if((kinc < 2) && (kstep==1)){
    for(int i=0;i<Total_number_phase ;i++){
      ph_mat[i]->get_elOp(Ctmp);
      addtens4(1.,C,Wgt[i],Ctmp,C);
    }
  }
  else{
    for(i=0;i<6;i++){
      for(j=0;j<6;j++){
	 C[i][j] = statev_n[pos_C + (i*6) + j];
      }
    }
  }
}

void MT_VT_Material::get_elOp(double** Cel){
  static double **Ctmp=NULL ;
  if(Ctmp==NULL){	
    mallocmatrix(&Ctmp,6,6);
  }

  cleartens4(Ctmp);
	
  for(int i=0;i<Total_number_phase;i++){
    ph_mat[i]->get_elOp(Ctmp);
    addtens4(1.,Cel,Wgt[i],Ctmp,Cel);
  }
}
	
  


void MT_VT_Material::get_elOD(double* statev, double** Cel){
  
  static double **Ctmp=NULL ; 
  if(Ctmp==NULL){	
    mallocmatrix(&Ctmp,6,6);
  } 
  cleartens4(Ctmp);
	
  for(int i=0;i<Total_number_phase ;i++){
    ph_mat[i]->get_elOD(&(statev[idsdv_i[i]]),Ctmp);
    addtens4(1.,Cel,Wgt[i],Ctmp,Cel);
  }
}

void MT_VT_Material::unload_step(double* dstrn, double* strs_n, double* statev_n, int kinc, int kstep){

       printf("unload_step not implemented for Mori-Tanaka_Voight material\n");
}

//********************************************************************
//************  add by Ling wu 14/09/2011   *********************
//ANISOTROPIC LINEAR ELASTICITY 
//PROPS: MODEL, iddam, anpr(E1, E2,E3,nu12,nu13,nu23,G12,G13,G23) 9 material constants
//PROPS: three euler anagles from golbe coordinates to local coordinates 
//( in order to agree with the euler angle of inclusion)
//STATEV: strn, strs, dstrn in global coordinates
//*******************************************************************
//constructor
ANEL_Material::ANEL_Material(double* props, int idmat):Material(props, idmat){
	
	int k;
        int iddam, idclength;
        int i;

//allocate memory for the 9 material constants and 3 euler angles

	mallocvector(&anpr,9);
	mallocvector(&euler,3);


	iddam = (int) props[idmat+1];    // damage
        idclength = (int)props[idmat+2];

        k=idmat+3;

	anpr[0] = props[k++];
        anpr[1] = props[k++];
        anpr[2] = props[k++];
        anpr[3] = props[k++];
        anpr[4] = props[k++];
        anpr[5] = props[k++];
        anpr[6] = props[k++];
        anpr[7] = props[k++];
        anpr[8] = props[k++];

        euler[0] = props[k++];
        euler[1] = props[k++];
        euler[2] = props[k];
        
	nsdv = 18;
	pos_strn=0;
	pos_strs=6;
	pos_dstrn=12;

        pos_p =0; // for the input of clength->creat_cg, as damage is not implemented in this material
	
	dam = init_damage(props,iddam);
	if(dam){
		pos_dam = nsdv;
                if(dam->get_pos_DamParm1() != 0){
                    pos_NFiber = pos_dam + dam->get_pos_DamParm1();
                    pos_Pdam = pos_dam + dam->get_pos_DamParm2();
                 }
                else {
                   pos_NFiber = 0;
                   pos_Pdam = 0;
                 }
                pos_Fd_bar = pos_dam + dam->get_pos_Fd_bar();
		pos_dFd_bar = pos_dam + dam->get_pos_dFd_bar();
		pos_locFd = pos_dam + dam->get_pos_locFd();
		nsdv += dam->get_nsdv();
	}
        else{
                pos_NFiber = 0;
                pos_Pdam = 0;
                pos_Fd_bar = 0;
		pos_dFd_bar = 0;
                pos_locFd = 0;

        }
	

        clength = init_clength(props,idclength);
        cl = sqrt(props[idclength+3]);

}

//destructor
ANEL_Material::~ANEL_Material(){
	
	if(dam) delete dam;
        if(clength) delete clength;

        free(anpr);
        free(euler);	

}

//print
void ANEL_Material::print(){

	printf("Anisotropic Linear elasticity\n");
        printf("Young modulus E1,E2,E3: %lf,%lf,%lf\n", anpr[0],anpr[1],anpr[2]);
	printf("Poisson ratio nu12,nu13,nu23: %lf, %lf, %lf\n", anpr[3],anpr[4],anpr[5]);
        printf("Shear modulus G12,G13,G23: %lf, %lf, %lf\n", anpr[6],anpr[7],anpr[8]);
        printf("Euler angles: %lf, %lf, %lf\n", euler[0],euler[1],euler[2]);
	if(dam){ dam->print();}
	
}
#ifdef NONLOCALGMSH
int ANEL_Material::get_pos_currentplasticstrainfornonlocal() const {return -1;}
int ANEL_Material::get_pos_effectiveplasticstrainfornonlocal() const {return -1;}
int ANEL_Material::get_pos_damagefornonlocal() const
{
	if(dam) return pos_dam;
	return -1;
}
double ANEL_Material::getElasticEnergy (double* statev) 
{
    double eE = 0.;
    int i;
    static double* strn = NULL;
    static double* strs = NULL;
    if(strn == NULL)
    {
       mallocvector(&strn,6);
       mallocvector(&strs,6);
    }
    cleartens2(strn);
    cleartens2(strs);

    copyvect(&(statev[get_pos_strn()]), strn,6);
    copyvect(&statev[get_pos_strs()], strs, 6);   
    //enegry due to damage has been removed
    for (i =0; i< 6; i++) eE+=0.5*(strs[i]*strn[i]);

    return eE;

}

double ANEL_Material::getPlasticEnergy (double* statev) 
{
    double eP = 0.;
    return eP;
}

void ANEL_Material::setEuler(double e1, double e2, double e3)
{
  euler[0] = e1;
  euler[1] = e2;
  euler[2] = e3;
}
#endif

//constbox
int ANEL_Material::constbox(double* dstrn, double* strs_n,  double* strs, double* statev_n, double* statev, double** Cref, double*** dCref, double* tau, double** dtau, double** Calgo, double alpha, double* dpdE, double* strs_dDdp_bar,double *Sp_bar, double** c_g, double* dFd_d_bar, double* dstrs_dFd_bar, double *dFd_dE, double** c_gF, double* dpdFd, double* dFddp, int kinc, int kstep, double dt){


  return (1);
}

int ANEL_Material::constboxSecantMixte(double* dstrn, double* strs_n,  double* strs, double* statev_n, double* statev, double **Calgo, double** Csd, double*** dCsd, double** dCsdp_bar, double* dnu, double& dnudp_bar, double alpha, double* dpdE, double* dstrsdp_bar, double** c_g, int kinc, int kstep, double dt, bool forceZero, bool forceRes)
{
  return (1);
}


//****************************************************
// consbox called by 2rd order method
//****************************************************

int ANEL_Material::constbox_2order(int mtx, double *DE, double* dstrn, double* strs, double* statev_n, double* statev,  double **Calgo, Lcc* LCC, YieldF* YF, double** c_g, int kinc, int kstep, double dt){

   return (1);
}

//end of consbox_2order


//reference operator from statev or material properties 
// = anisotropic elastic operator in any case
void ANEL_Material::get_refOp(double* statev, double** C, int kinc, int kstep){

	get_elOp(C);
}

void ANEL_Material::get_elOp(double** Cel){

	compute_AnisoOperator(anpr, euler,Cel);
}
void ANEL_Material::get_elOp(double* statev, double** Cel){
        if(pos_euler != 0){         
            compute_AnisoOperator(anpr, &(statev[pos_euler]),Cel);
        }
        else{
            compute_AnisoOperator(anpr, euler,Cel);
        }
}
void ANEL_Material::get_elOD(double* statev, double** Cel)
{
     get_elOp(Cel);
     if(dam)
     {
       double damage = statev[get_pos_dam()];
       for(int i=0; i < 6; i++)
         for(int j=0; j < 6; j++)
           Cel[i][j]*=1.-damage;
    }
}

void ANEL_Material::unload_step(double* dstrn, double* strs_n, double* statev_n, int kinc, int kstep){

       printf("unload_step not implemented for ANEL material\n");
}
#endif
