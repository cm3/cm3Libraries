//
//  FiniteStrain.h
//  m
//
//  Created by Marieme Imene EL GHEZAL on 22/11/16.
//  Copyright (c) 2016 Marieme Imene EL GHEZAL. All rights reserved.
//

#ifndef __m__FiniteStrain__
#define __m__FiniteStrain__

#include <math.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <float.h>

int Getstrnln(double** Fn1, double* strnln);
int GetdRot(double** F0, double** Fn, double** dR);
int GetStretchTensor(double* strn, double** Vr, double shearRatio);
int GetTauRes(double** dr, double* strn, double *Vr, double E, double nu, double* strs_tr, double** Basenp1, double shearRatio1, double shearRatio2);
int GetTau(double** dr, double* strn, double E, double nu, double* strs_tr, double** Basenp1, double shearRatio1, double shearRatio2);
void GetEpsilonln(double** v, double* e, double** epln);
void UpdateIncl(double *ar0, double *euler, double**Fn1, double *ar1, double**R);
int GetdR(double** Basen, double* Vr, double** dR, double **Basenp1, double shearRatio);
int UpdateFandR(double** Fn, double** Rn, double* Vr, double** dR, double **Fnp1, double **Rnp1, double shearRatio);
int UpdateF(double** Fn, double* Vr, double **Fnp1, double shearRatio);
#endif
