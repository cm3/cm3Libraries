//
// C++ Interface: material law
//
// Description: j2 linear elasto-plastic law
//
//
// Author:  <Gauthier BECKER>, (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "mlawJ2linear.h"
#include <math.h>
#include "MInterfaceElement.h"
#include "nonLinearMechSolver.h"

// Keep this constructor for retro compatibility !
mlawJ2linear::mlawJ2linear(const int num,const double E,const double nu,
                           const double rho,const double sy0,const double h,
                           const double tol, const bool pert, const double eps) : materialLaw(num,true), _E(E), _nu(nu), _rho(rho),
                                                _lambda((E*nu)/(1.+nu)/(1.-2.*nu)),
                                               _mu(0.5*E/(1.+nu)),_K(E/3./(1.-2.*nu)), _K3(3.*_K), _mu3(3.*_mu),
                                               _mu2(2.*_mu), _tol(tol),_perturbationfactor(eps),_tangentByPerturbation(pert),_order(1),
                                               _viscosityLaw(NULL),_p(1.), _stressFormulation(CORO_KIRCHHOFF)
{

  _j2IH = new LinearExponentialJ2IsotropicHardening(num, sy0, h, 0., 10.);
  // Put a warning
  Msg::Warning("Deprecated constructor of mlawJ2linear! This constructor builds a LinearExponentialJ2IsotropicHardening to compute the hardening. You should provide the hardening law to the constructor");

  STensorOperation::zero(_Cel);
  _Cel(0,0,0,0) = _lambda + _mu2;
  _Cel(1,1,0,0) = _lambda;
  _Cel(2,2,0,0) = _lambda;
  _Cel(0,0,1,1) = _lambda;
  _Cel(1,1,1,1) = _lambda + _mu2;
  _Cel(2,2,1,1) = _lambda;
  _Cel(0,0,2,2) = _lambda;
  _Cel(1,1,2,2) = _lambda;
  _Cel(2,2,2,2) = _lambda + _mu2;

  _Cel(1,0,1,0) = _mu;
  _Cel(2,0,2,0) = _mu;
  _Cel(0,1,0,1) = _mu;
  _Cel(2,1,2,1) = _mu;
  _Cel(0,2,0,2) = _mu;
  _Cel(1,2,1,2) = _mu;

  _Cel(0,1,1,0) = _mu;
  _Cel(0,2,2,0) = _mu;
  _Cel(1,0,0,1) = _mu;
  _Cel(1,2,2,1) = _mu;
  _Cel(2,0,0,2) = _mu;
  _Cel(2,1,1,2) = _mu;


  STensor3 I(1.);
  for (int i=0; i<3; i++){
    for (int j=0; j<3; j++){
      for (int k=0; k<3; k++){
        for (int l=0; l<3; l++){
          _I4dev(i,j,k,l) = 0.5*(I(i,k)*I(j,l)+I(i,l)*I(j,k)) - I(i,j)*I(k,l)/3.;
        }
      }
    }
  }

}

mlawJ2linear::mlawJ2linear(const int num,const double E,const double nu, const double rho,const J2IsotropicHardening &j2IH,
                           const double tol, const bool pert, const double eps) : materialLaw(num,true), _E(E), _nu(nu), _rho(rho),
                                               _lambda((E*nu)/(1.+nu)/(1.-2.*nu)),
                                               _mu(0.5*E/(1.+nu)),_K(E/3./(1.-2.*nu)), _K3(3.*_K), _mu3(3.*_mu),
                                               _mu2(2.*_mu), _tol(tol),_perturbationfactor(eps),_tangentByPerturbation(pert),_order(1),
                                                _viscosityLaw(NULL),_p(1.), _stressFormulation(CORO_KIRCHHOFF)
{
  _j2IH=j2IH.clone();

  STensorOperation::zero(_Cel);
  _Cel(0,0,0,0) = _lambda + _mu2;
  _Cel(1,1,0,0) = _lambda;
  _Cel(2,2,0,0) = _lambda;
  _Cel(0,0,1,1) = _lambda;
  _Cel(1,1,1,1) = _lambda + _mu2;
  _Cel(2,2,1,1) = _lambda;
  _Cel(0,0,2,2) = _lambda;
  _Cel(1,1,2,2) = _lambda;
  _Cel(2,2,2,2) = _lambda + _mu2;

  _Cel(1,0,1,0) = _mu;
  _Cel(2,0,2,0) = _mu;
  _Cel(0,1,0,1) = _mu;
  _Cel(2,1,2,1) = _mu;
  _Cel(0,2,0,2) = _mu;
  _Cel(1,2,1,2) = _mu;

  _Cel(0,1,1,0) = _mu;
  _Cel(0,2,2,0) = _mu;
  _Cel(1,0,0,1) = _mu;
  _Cel(1,2,2,1) = _mu;
  _Cel(2,0,0,2) = _mu;
  _Cel(2,1,1,2) = _mu;


  STensor3 I(1.);
  for (int i=0; i<3; i++){
    for (int j=0; j<3; j++){
      for (int k=0; k<3; k++){
        for (int l=0; l<3; l++){
          _I4dev(i,j,k,l) = 0.5*(I(i,k)*I(j,l)+I(i,l)*I(j,k)) - I(i,j)*I(k,l)/3.;
        }
      }
    }
  }

}
mlawJ2linear::mlawJ2linear(const mlawJ2linear &source) : materialLaw(source),_E(source._E), _nu(source._nu), _rho(source._rho),
                                                         _lambda(source._lambda),
                                                         _mu(source._mu), _K(source._K),_K3(source._K3), _mu3(source._mu3),
                                                         _mu2(source._mu2), _tol(source._tol),
                                                         _perturbationfactor(source._perturbationfactor),
                                                         _tangentByPerturbation(source._tangentByPerturbation),_order(source._order),
                                                         _Cel(source._Cel),_I4dev(source._I4dev), _p(source._p),
                                                         _stressFormulation(source._stressFormulation)
{
	_j2IH = NULL;
  if(source._j2IH != NULL)
  {
    _j2IH=source._j2IH->clone();
  }

  _viscosityLaw = NULL;
  if (source._viscosityLaw != NULL){
    _viscosityLaw = source._viscosityLaw->clone();
  }
}

mlawJ2linear& mlawJ2linear::operator=(const materialLaw &source)
{
  materialLaw::operator=(source);
  const mlawJ2linear* src =static_cast<const mlawJ2linear*>(&source);
  if(src != NULL)
  {
    _E = src->_E;
    _nu = src->_nu;
    _rho = src->_rho;
    _lambda = src->_lambda;
    _mu = src->_mu;
    _K = src->_K;
    _K3 = src->_K3;
    _mu3 = src->_mu3;
    _mu2 = src->_mu2;
    _tol = src->_tol;
    _perturbationfactor = src->_perturbationfactor;
    _tangentByPerturbation = src->_tangentByPerturbation;
    _Cel = src->_Cel;
    _I4dev = src->_I4dev;
    _stressFormulation  = src->_stressFormulation;

    if(_j2IH != NULL) delete _j2IH;
    if(src->_j2IH != NULL)
    {
      _j2IH=src->_j2IH->clone();
    }

    if (_viscosityLaw) delete _viscosityLaw;
    if (src->_viscosityLaw != NULL){
      _viscosityLaw = src->_viscosityLaw->clone();
    }
  }
  return *this;
}

mlawJ2linear::~mlawJ2linear()
{
  if(_j2IH!=NULL) {delete _j2IH; _j2IH = NULL;};
  if (_viscosityLaw != NULL) {delete _viscosityLaw; _viscosityLaw = NULL;}
}

void mlawJ2linear::setViscosityEffect(const viscosityLaw& v, const double p){
  _p = p;
  if (_viscosityLaw) delete _viscosityLaw;
  _viscosityLaw = v.clone();
};

void mlawJ2linear::setStressFormulation(int s)
{
  _stressFormulation = (stressFormumation)s;
  if (_stressFormulation == CORO_CAUCHY)
  {
    Msg::Info("The corotational cauchy stress is used");
  }
  else if (_stressFormulation == CORO_KIRCHHOFF)
  {
    Msg::Info("The corotational Kirchoff stress is used");
  }
  else
  {
    Msg::Error("The formulation %d is not implemented, using corotational Kirchoff instead");
    _stressFormulation = CORO_KIRCHHOFF;
  }
}

void mlawJ2linear::createIPState(IPStateBase* &ips,bool hasBodyForce,const bool* state_,const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
  //bool inter=true;
  //const MInterfaceElement *iele = static_cast<const MInterfaceElement*>(ele);
  //if(iele==NULL) inter=false;
  IPVariable* ipvi = new IPJ2linear(_j2IH);
  IPVariable* ipv1 = new IPJ2linear(_j2IH);
  IPVariable* ipv2 = new IPJ2linear(_j2IH);
  if(ips != NULL) delete ips;
  ips = new IP3State(state_,ipvi,ipv1,ipv2);
}

double mlawJ2linear::soundSpeed() const
{
  double factornu = (1.-_nu)/((1.+_nu)*(1.-2.*_nu));
  return sqrt(_E*factornu/_rho);
}


void mlawJ2linear::constitutive(const STensor3& F0,
                                const STensor3& Fn,
                                STensor3 &P,
                                const IPVariable *q0i,
                                IPVariable *q1i,
                                STensor43 &Tangent,
                                const bool stiff, 
                                STensor43* elasticTangent, 
                                const bool dTangent,
                                STensor63* dCalgdeps) const
{
  const IPJ2linear* q0= dynamic_cast<const IPJ2linear*>(q0i);
        IPJ2linear* q1= dynamic_cast<IPJ2linear*>(q1i);
  static STensor43 dFpdF,dFedF;
  static STensor3 dpdF;
  predictorCorector(F0,Fn,P,q0,q1,Tangent,dFpdF,dFedF,dpdF,stiff,elasticTangent,dTangent,dCalgdeps);
}


void mlawJ2linear::tangent_full(STensor43 &T_,const STensor3 &P_,const STensor3 &F_,const STensor3 &Fp0_,const STensor3 &Fp1_,
                            const IPJ2linear* q0_,const IPJ2linear* q1_) const
{

  static STensor3 Fpinv,Fe,Ce, Fp0inv,Fe0,Epre,S,Finv;
  static STensor43 tmp,Ll,Lr,dFpdE,KK,dFpdF;

  /* Compute elastic predictor */
  STensorOperation::inverseSTensor3(Fp0_,Fp0inv);
  STensorOperation::multSTensor3(F_,Fp0inv,Fe0);
  STensorOperation::multSTensor3FirstTranspose(Fe0,Fe0,Ce);

  bool ok=STensorOperation::logSTensor3(Ce,_order,Epre,&tmp);
  Epre*=0.5;

  /* Compute current elastic-strain */
  STensorOperation::inverseSTensor3(Fp1_,Fpinv);
  STensorOperation::multSTensor3(F_,Fpinv,Fe);

  /* Compute small-strain tangent */
  this->tangent(KK,Epre,q0_,q1_,dFpdE);

  /* Small strain -> large deformations in log C */
  STensorOperation::zero(Ll);
  Lr = Ll;
  dFpdF = Ll;
  for(int l=0;l<3;l++)
    for(int k=0;k<3;k++)
      for(int j=0;j<3;j++)
        for(int i=0;i<3;i++)
          for(int m=0;m<3;m++)
            for(int n=0;n<3;n++)
            {
               Ll(i,j,k,l) += tmp(i,j,m,n)*Fe(k,m)*Fpinv(l,n);
               Lr(i,j,k,l) += tmp(i,j,m,n)*Fe0(k,m)*Fp0inv(l,n);
            }
  for(int l=0;l<3;l++)
    for(int k=0;k<3;k++)
      for(int j=0;j<3;j++)
        for(int i=0;i<3;i++){
          for(int m=0;m<3;m++)
            for(int n=0;n<3;n++)
              for(int o=0;o<3;o++)
              {
                dFpdF(i,j,k,l) += dFpdE(i,o,m,n)*Fp0_(o,j)*Lr(m,n,k,l);
              }
        }

  STensorOperation::multSTensor43FirstTranspose(Ll,KK,tmp);
  STensorOperation::multSTensor43(tmp,Lr,T_);

  /* Add geometric/kinematic terms */
  STensorOperation::inverseSTensor3(F_,Finv);
  STensorOperation::multSTensor3(Finv,P_,S);

  for(int l=0; l < 3; l++)
    for(int k=0; k < 3; k++)
      for(int j=0; j < 3; j++)
	  {
        T_(k,j,k,l) += S(l,j);
	    for (int i=0; i < 3; i++)
	    {
          for (int n=0;n<3;n++)
		    for (int o=0;o<3;o++)
		    {
			    T_(i,j,k,l)-=(Fe(i,o)*dFpdF(o,n,k,l)*S(n,j)+P_(i,n)*Fpinv(j,o)*dFpdF(o,n,k,l));
		    }
	    }
	  }
}

void mlawJ2linear::tangent(STensor43 &K_,STensor3 &Epre_,const IPJ2linear *q0_,const IPJ2linear *q1_,
                           STensor43 &dFpdE_) const
{
  static STensor3 coefEpre,MM,Aux;
  static STensor43 ddeM,iL;

  // elastic part
  K_ = _Cel;
  double deps = q1_->_j2lepspbarre - q0_->_j2lepspbarre;
  // if plastic occurs
  if(deps > 0.)
  {
    double tracediv3 = Epre_.trace()/3.;
    Epre_(0,0) -= tracediv3;
    Epre_(1,1) -= tracediv3;
    Epre_(2,2) -= tracediv3;

    double Eeq = Epre_.dotprod();
    Eeq = sqrt(2./3.)*sqrt(Eeq);
    double H    = q1_->getConstRefToIPJ2IsotropicHardening().getDR();
    if(Eeq > 1.e-12)
    {
      double EeqEeq = Eeq*Eeq;
      double factor = (_mu2/(_mu3+H) - 2.*deps/(3.*Eeq))/EeqEeq;
      coefEpre = Epre_;
      coefEpre *= factor;
      tensprod(coefEpre,Epre_,ddeM);
      double fact2 = deps/Eeq;
      ddeM.axpy(fact2,_I4dev);

      this->J2flow(MM,Epre_);
      MM*=deps;
      STensorOperation::expSTensor3(MM,_order,Aux,&iL);

      STensorOperation::multSTensor43(iL,ddeM,dFpdE_);

      // update elastoplastic tangent
      K_.axpy(-_mu2,ddeM);
    }
    else
    {
      STensorOperation::zero(dFpdE_);
    }
  }
  else
  {
    STensorOperation::zero(dFpdE_);
  }
}

void mlawJ2linear::J2flow(STensor3 &M_,const STensor3 &Ee_) const
{
  double tracediv3 = (Ee_.trace())/3.;
  M_ = Ee_;
  M_(0,0) -= tracediv3;
  M_(1,1) -= tracediv3;
  M_(2,2) -= tracediv3;

  double val = M_.dotprod();
  val = sqrt(1.5/val);

  M_*=val;
}

double mlawJ2linear::deformationEnergy(const STensor3 &C) const
{
  STensor3 logCdev;
  bool ok=STensorOperation::logSTensor3(C,_order,logCdev,NULL);
  double trace = logCdev.trace();

  double lnJ = 0.5*trace;
  logCdev(0,0)-=trace/3.;
  logCdev(1,1)-=trace/3.;
  logCdev(2,2)-=trace/3.;

  double Psy = _K*0.5*lnJ*lnJ+_mu*0.25*dot(logCdev,logCdev);
  return Psy;;
}


void mlawJ2linear::predictorCorector(const STensor3& F0,         // initial deformation gradient (input @ time n)
                            const STensor3& F,         // updated deformation gradient (input @ time n+1)
                            STensor3 &P,                // updated 1st Piola-Kirchhoff stress tensor (output)
                            const IPJ2linear *q0,       // array of initial internal variable
                            IPJ2linear *q,             // updated array of internal variable (in ipvcur on output),
                            STensor43& Tangent,         // mechanical tangents (output)
                            STensor43& dFpdF, // plastic tangent
                            STensor43& dFedF,
                            STensor3& dpdF,
                            const bool stiff,
                            STensor43* elasticTangent, 
                            const bool dTangent,
                            STensor63* dCalgdeps
                            ) const {
  double Jformule = 1;
  if (_stressFormulation == CORO_CAUCHY)
  {
    Jformule = STensorOperation::determinantSTensor3(F);
  }
  
  const STensor3& Fp0 = q0->getConstRefToFp();
  STensor3& Fp = q->getRefToFp();

  double& eps = q->getRefToEquivalentPlasticStrain();
  const double& eps0  = q0->getConstRefToEquivalentPlasticStrain();
  // elastic predictor
  Fp = Fp0;
  eps = eps0;

  static STensor3 invFp0, Fepr;
  STensorOperation::inverseSTensor3(Fp0,invFp0);
  STensorOperation::multSTensor3(F,invFp0,Fepr);

  static STensor3 Fpinv, Fe, Ce;
  Fpinv = invFp0;
  Fe = Fepr;
  STensorOperation::multSTensor3FirstTranspose(Fe,Fe,Ce);

  // compute strain by logarithmic operator
  static STensor43 Lpr; // dlogCepr/dCepr
  static STensor63 dLDCe; // ddlogCepr/ddCepr
  static STensor3 Eepr; // 0.5*log(Cepr)
  if (_order == 1){
    bool ok=STensorOperation::logSTensor3(Ce,_order,Eepr,&Lpr);
    if(!ok)
    {
       P(0,0) = P(1,1) = P(2,2) = sqrt(-1.);
       return; 
    }
  }
  else{
    bool ok=STensorOperation::logSTensor3(Ce,_order,Eepr,&Lpr,&dLDCe);
    if(!ok)
    {
       P(0,0) = P(1,1) = P(2,2) = sqrt(-1.);
       return; 
    }
  }
  Eepr *= 0.5;

  static STensor43 L; // dlogCe/dCe
  L = Lpr; // equal to dlogCepr/dCepr

  STensor3& Ee  = q->getRefToElasticDeformationTensor();
  Ee = Eepr; // current elatic part of total strain

  static STensor43 DexpA; // use for compute tangent later

  // compute corotational Kirchhoff stress
  double p = _K*Ee.trace();
  static STensor3 corKirDev; // deviatoric part
  corKirDev = Ee.dev();
  corKirDev *= (2.*_mu);
  double Deps = 0.;

  /* Test plasticity */
  _j2IH->hardening(eps0, q0->getConstRefToIPJ2IsotropicHardening(),eps, q->getRefToIPJ2IsotropicHardening());
  double Sy0 = _j2IH->getYield0();
  double Sy   = q->getConstRefToIPJ2IsotropicHardening().getR();
  double H    = q->getConstRefToIPJ2IsotropicHardening().getDR();

  static STensor3 N;
  this->J2flow(N,Eepr);

  if (q->dissipationIsBlocked()){
    q->getRefToDissipationActive() = false;
  }
  else{

    // equivalent stress
    double Seqpr = sqrt(1.5*corKirDev.dotprod())/Jformule;
    
    // plastic normal-> based on trial value
    double VMcriterion = Seqpr - Sy;

    if(VMcriterion/Sy0 >_tol)
    {
      q->getRefToDissipationActive() = true;
      int ite = 0, maxite = 1000;
      while(fabs(VMcriterion)/Sy0 > _tol)
      {
        double coef = _mu*3./Jformule+H;
        double deps = VMcriterion/coef;
        if (Deps + deps <=0.)
          Deps *= 0.5;
        else
          Deps += deps;
        //
        _j2IH->hardening(eps0, q0->getConstRefToIPJ2IsotropicHardening(), eps0+Deps, q->getRefToIPJ2IsotropicHardening());
        Sy = q->getConstRefToIPJ2IsotropicHardening().getR();
        H = q->getConstRefToIPJ2IsotropicHardening().getDR();
        if (_viscosityLaw != NULL){
          // viscosity
          double K, dK;
          _viscosityLaw->get(eps0+Deps,K,dK);
          // rate of plastic deformation
          double pdot = Deps/this->getTimeStep();
          Sy += K*pow(pdot,_p);
          H += (dK*pow(pdot,_p) + K*_p*pow(pdot,_p-1.)/this->getTimeStep());
          //Msg::Info("pdot=%e, p = %e, overstress = %e",pdot,_p,K*pow(pdot,_p));
        }
        VMcriterion = Seqpr - 3.*_mu*Deps/Jformule -Sy;

        ite++;
        if(ite > maxite)
        {
          Msg::Error("No convergence for plastic correction in J2 !! VMcriterion/sy0 = %e",VMcriterion/Sy0);
          //q->operator =(*dynamic_cast<const IPVariable*>(q0));
					P(0,0) = P(1,1) = P(2,2) = sqrt(-1.);
          return;
        }
      }
      // update plastic strain
      eps  = Deps+eps0;
      static STensor3 DepsN;
      DepsN = N;
      DepsN *= Deps;
      
      // update Kcor
      corKirDev.daxpy(DepsN,-_mu2);

      // update Fp
      static STensor3 expDepsN;
      STensorOperation::expSTensor3(DepsN,_order,expDepsN,&DexpA);
      STensorOperation::multSTensor3(expDepsN,Fp0,Fp);

      // update elastic strain
      STensorOperation::inverseSTensor3(Fp,Fpinv);
      STensorOperation::multSTensor3(F,Fpinv,Fe);
      STensorOperation::multSTensor3FirstTranspose(Fe,Fe,Ce);
      // recompute current strain
      if (_order == 1){
        bool ok=STensorOperation::logSTensor3(Ce,_order,Ee,&L);
        if(!ok)
        {
          P(0,0) = P(1,1) = P(2,2) = sqrt(-1.);
          return; 
        }
      }
      else{
        bool ok=STensorOperation::logSTensor3(Ce,_order,Ee,&L,&dLDCe);
        if(!ok)
        {
          P(0,0) = P(1,1) = P(2,2) = sqrt(-1.);
          return; 
        }
      }
      Ee *= 0.5;
    }
    else{
      q->getRefToDissipationActive() = false;
    };
  }

  // estimation of PK stress
  static STensor3 corKir;
  corKir = corKirDev;
  corKir(0,0) += p;
  corKir(1,1) += p;
  corKir(2,2) += p;

  static STensor3 S, FeS;
  STensorOperation::multSTensor3STensor43(corKir,L,S);
  STensorOperation::multSTensor3(Fe,S,FeS);
  STensorOperation::multSTensor3SecondTranspose(FeS,Fpinv,P);

  // elastic energy
  q->getRefToElasticEnergy()= this->deformationEnergy(Ce);
  // plastic energy
  q->getRefToPlasticEnergy() = q0->plasticEnergy();
    // plastic power (Wp1- Wp0)/dt
  if (Deps > 0.){
    q->getRefToPlasticEnergy() += Jformule*Deps*Sy;
    q->getRefToPlasticPower() = Jformule*Deps*Sy/this->getTimeStep();
  }
  else
  {
    q->getRefToPlasticPower() = 0.;
  }

  // irreversible energy
  if (this->getMacroSolver()->getPathFollowingLocalIncrementType() == pathFollowingManager::DEFO_ENERGY){
    //
    q->getRefToIrreversibleEnergy() = q->defoEnergy();
  }
  else if ((this->getMacroSolver()->getPathFollowingLocalIncrementType() == pathFollowingManager::PLASTIC_ENERGY) or
           (this->getMacroSolver()->getPathFollowingLocalIncrementType() == pathFollowingManager::DISSIPATION_ENERGY)) {
    q->getRefToIrreversibleEnergy() = q->plasticEnergy();
  }
  else{
    q->getRefToIrreversibleEnergy() = 0.;
  }

  if (stiff){
    if (_tangentByPerturbation){
      static STensor3 plusF, plusP, plusFe, plusFpinv;
      static STensor43 tmp43;
      static IPJ2linear qTmp(*q0);

      for (int k=0; k<3; k++){
        for (int l=0; l<3; l++){
          plusF = F;
          plusF(k,l) += _tol;
          this->constitutive(F0,plusF,plusP,q0,&qTmp,tmp43,false);
          STensorOperation::inverseSTensor3(qTmp._j2lepsp,plusFpinv);
          STensorOperation::multSTensor3(plusF,plusFpinv,plusFe);

          for (int i=0; i<3; i++){
            for (int j=0; j<3; j++){
              Tangent(i,j,k,l) = (plusP(i,j) - P(i,j))/_tol;
              dFpdF(i,j,k,l) = (qTmp._j2lepsp(i,j) - q->_j2lepsp(i,j))/_tol;
              dFedF(i,j,k,l) = (plusFe(i,j) - Fe(i,j))/_tol;
            }
          }

          dpdF(k,l) = (qTmp._j2lepspbarre - eps)/_tol;
          q->getRefToDIrreversibleEnergyDF()(k,l) = (qTmp.irreversibleEnergy() - q->irreversibleEnergy())/_tol;
        }
      }

      STensor3& dplasticpowerDF = q->getRefToDPlasticPowerDF();
      dplasticpowerDF = q->getConstRefToDIrreversibleEnergyDF();
      dplasticpowerDF *= (1./this->getTimeStep());
    }
    else{
      static STensor43 DcorKirDEepr;
      DcorKirDEepr =  (_Cel);
      static STensor43 dFpDEepr;
      static STensor3 DgammaDEepr;
      if (Deps > 0)
      {
        if (_stressFormulation == CORO_KIRCHHOFF)
        {
          DgammaDEepr = N;
          DgammaDEepr *= (2.*_mu/(3.*_mu+H));
        }
        else if (_stressFormulation == CORO_CAUCHY)
        {
          // J = Jepr*Jppr --> ln(J) = tr(Eepr) + ln(ppr) --> DJDEepr = J*I
          static STensor3 DdetFDEepr;
          STensorOperation::diag(DdetFDEepr,Jformule);
          static STensor43 DcorSigDEepr;
          DcorSigDEepr = DcorKirDEepr;
          DcorSigDEepr *= (1./Jformule);
          double ffff = -1./(Jformule*Jformule);
          STensorOperation::prodAdd(corKir,DdetFDEepr,ffff,DcorSigDEepr);
          STensorOperation::multSTensor3STensor43(N,DcorSigDEepr,DgammaDEepr);
          ffff = 3*_mu*Deps/(Jformule*Jformule);
          DgammaDEepr.daxpy(DdetFDEepr,ffff);
          DgammaDEepr *= (1./(3.*_mu/Jformule+H));
        }
        else
        {
          Msg::Error("stress formulation has not been implemented");
          Msg::Exit(0);
        }

        static STensor3 devEepr;
        devEepr = Eepr.dev();

        double EeprEq = sqrt(devEepr.dotprod()/1.5);

        static STensor43 BE;
        tensprod(N,N,BE);
        BE *= (2.*_mu/(3.*_mu+H)- 2.*Deps/3./EeprEq);
        double val=Deps/EeprEq;
        BE.axpy(val,_I4dev);

        // update with plasticity corKir
        DcorKirDEepr.axpy(-2.*_mu,BE);

        static STensor43 DexpABE;
        STensorOperation::multSTensor43(DexpA,BE,DexpABE);
        // update with plasticity Fp
        for (int i=0; i<3; i++){
          for (int j=0; j<3; j++){
            for (int k=0; k<3; k++){
              for (int l=0; l<3; l++){
                dFpDEepr(i,j,k,l) = 0.;
                for (int p=0; p<3; p++){
                  dFpDEepr(i,j,k,l) += DexpABE(i,p,k,l)*Fp0(p,j);
                }
              }
            }
          }
        }
      }
      else{
        STensorOperation::zero(dFpDEepr);
        STensorOperation::zero(DgammaDEepr);
      }

      static STensor43 EprToF;
      for (int i=0; i<3; i++){
        for (int j=0; j<3; j++){
          for (int k=0; k<3; k++){
            for (int l=0; l<3; l++){
              EprToF(i,j,k,l) = 0.;
              for (int p=0; p<3; p++){
                for (int q=0; q<3; q++){
                  EprToF(i,j,k,l) += Lpr(i,j,p,q)*Fepr(k,p)*invFp0(l,q);
                }
              }
            }
          }
        }
      }



      static STensor43 DcorKirDF;
      STensorOperation::multSTensor43(DcorKirDEepr,EprToF,DcorKirDF);
      if (Deps>0.){
        STensorOperation::multSTensor3STensor43(DgammaDEepr,EprToF,dpdF);
        STensorOperation::multSTensor43(dFpDEepr,EprToF,dFpdF);
      }
      else{
        STensorOperation::zero(dpdF);
        STensorOperation::zero(dFpdF);
      }

      // done DcorKirDF, DcorKirDT, DFpDF, DFpDT

      static STensor43 DinvFpdF;
      if (Deps >0){
        for (int i=0; i<3; i++){
          for (int j=0; j<3; j++){
            for (int k=0; k<3; k++){
              for (int l=0; l<3; l++){
                DinvFpdF(i,j,k,l) = 0.;
                for (int p=0; p<3; p++){
                  for (int q=0; q<3; q++){
                    DinvFpdF(i,j,k,l) -= Fpinv(i,p)*dFpdF(p,q,k,l)*Fpinv(q,j);
                  }
                }
              }
            }
          }
        }
      }
      else{
        STensorOperation::zero(DinvFpdF);
      }

      STensorOperation::zero(dFedF);
      for (int i=0; i<3; i++){
        for (int j=0; j<3; j++){
          for (int k=0; k<3; k++){
            dFedF(i,j,i,k) += Fpinv(k,j);
            if (Deps> 0){
              for (int l=0; l<3; l++){
                for (int p=0; p<3; p++){
                  dFedF(i,j,k,l) += F(i,p)*DinvFpdF(p,j,k,l);
                }
              }
            }
          }
        }
      }

      static STensor63 DLDF;
      if (_order != 1){
        for (int i=0; i<3; i++){
          for (int j=0; j<3; j++){
            for (int k=0; k<3; k++){
              for (int l=0; l<3; l++){
                for (int p=0; p<3; p++){
                  for (int q=0; q<3; q++){
                    DLDF(i,j,k,l,p,q) = 0.;
                    for (int r=0; r<3; r++){
                      for (int s=0; s<3; s++){
                        for (int a=0; a<3; a++){
                          DLDF(i,j,k,l,p,q) += dLDCe(i,j,k,l,r,s)*2.*Fe(a,r)*dFedF(a,s,p,q);
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }

      }
      else{
        STensorOperation::zero(DLDF);
      }
      //

      static STensor43 DSDF; // S = corKir:L
      for (int i=0; i<3; i++){
        for (int j=0; j<3; j++){
          for (int k=0; k<3; k++){
            for (int l=0; l<3; l++){
              DSDF(i,j,k,l) = 0.;
              for (int r=0; r<3; r++){
                for (int s=0; s<3; s++){
                  DSDF(i,j,k,l) += DcorKirDF(r,s,k,l)*L(r,s,i,j) ;
                  if (_order != 1){
                      DSDF(i,j,k,l) += corKir(r,s)*DLDF(r,s,i,j,k,l);
                  }
                }
              }
            }
          }
        }
      }



      STensorOperation::zero(Tangent);
      for (int i=0; i<3; i++){
        for (int j=0; j<3; j++){
          for (int k=0; k<3; k++){
            for (int l=0; l<3; l++){
              for (int p=0; p<3; p++){
                for (int q=0; q<3; q++){
                  Tangent(i,j,p,q) += (dFedF(i,k,p,q)*S(k,l)*Fpinv(j,l) + Fe(i,k)*DSDF(k,l,p,q)*Fpinv(j,l)+Fe(i,k)*S(k,l)*DinvFpdF(j,l,p,q));
                }
              }
            }
          }
        }
      }

        // compute mechanical tengent
      if (elasticTangent!= NULL){
        static STensor43 dKcorDFelastic;
        STensorOperation::multSTensor43(_Cel,EprToF,dKcorDFelastic);
        static STensor43 DSDFelastic;
        for (int i=0; i<3; i++){
          for (int j=0; j<3; j++){
            for (int k=0; k<3; k++){
              for (int l=0; l<3; l++){
                DSDFelastic(i,j,k,l) = 0.;
                for (int r=0; r<3; r++){
                  for (int s=0; s<3; s++){
                    DSDFelastic(i,j,k,l) += dKcorDFelastic(r,s,k,l)*L(r,s,i,j) ;
                  }
                }
              }
            }
          }
        }

        static STensor43 dFedFelastic;
        STensorOperation::zero(dFedFelastic);
        for (int i=0; i<3; i++){
          for (int j=0; j<3; j++){
            for (int k=0; k<3; k++){
              dFedFelastic(i,j,i,k) += Fpinv(k,j);
            }
          }
        }

        STensorOperation::zero(*elasticTangent);
        for (int i=0; i<3; i++){
          for (int j=0; j<3; j++){
            for (int k=0; k<3; k++){
              for (int l=0; l<3; l++){
                for (int p=0; p<3; p++){
                  for (int q=0; q<3; q++){
                    (*elasticTangent)(i,j,p,q) += (dFedFelastic(i,k,p,q)*S(k,l)*Fpinv(j,l) +Fe(i,k)*DSDFelastic(k,l,p,q)*Fpinv(j,l));
                  }
                }
              }
            }
          }
        }
      }


      
      STensor3& DplasticEnergyDF = q->getRefToDPlasticEnergyDF();
      if (Deps > 0.){
        DplasticEnergyDF = dpdF;
        DplasticEnergyDF *= Jformule*(Sy+H*Deps);
        if (_stressFormulation == CORO_CAUCHY)
        {
          static STensor3 Finv;
          STensorOperation::inverseSTensor3(F, Finv);
          for (int i=0; i<3; i++)
          {
            for (int j=0; j<3; j++)
            {
              DplasticEnergyDF(i,j) += Jformule*Finv(j,i)*Deps*Sy;
            }
          }
        }
      }
      else{
        STensorOperation::zero(DplasticEnergyDF);
      }
      // plastic power
      STensor3& dplasticpowerDF = q->getRefToDPlasticPowerDF();
      dplasticpowerDF = DplasticEnergyDF;
      dplasticpowerDF *= (1./this->getTimeStep());

      // irreversible energy
      STensor3& DirrEnegDF = q->getRefToDIrreversibleEnergyDF();
      if (this->getMacroSolver()->getPathFollowingLocalIncrementType() == pathFollowingManager::DEFO_ENERGY){
        for(int i=0;i<3;i++){
          for(int j=0;j<3;j++){
            DirrEnegDF(i,j) = 0.;
            for(int k=0;k<3;k++){
              for(int l=0;l<3;l++){
                for (int m=0; m<3; m++){
                  DirrEnegDF(i,j) += P(k,m)*Fp(l,m)*dFedF(k,l,i,j);
                }
              }
            }
          }
        }
      }
      else if ((this->getMacroSolver()->getPathFollowingLocalIncrementType() == pathFollowingManager::PLASTIC_ENERGY) or
               (this->getMacroSolver()->getPathFollowingLocalIncrementType() == pathFollowingManager::DISSIPATION_ENERGY)) {
        DirrEnegDF = q->getConstRefToDPlasticEnergyDF();
      }
      else{
        STensorOperation::zero(DirrEnegDF);
      }
    }
  }
};
void mlawJ2linear::ElasticStiffness(STensor43* elasticTensor) const
{
    elasticTensor->operator=(_Cel);
};
