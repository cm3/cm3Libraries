C
c     CAPSUL: CrystAl Plasticity UtiLities tool set 
C     Multiscale Materials Modelling Group, IMDEA Materials
C
C     UMAT Viscoplastic Von Mises Model with flow rule by Peric (1993) MODEL 
c     evpsvl.f
C

c     ********************************************************************** UMAT Interface

c     UMAT subroutine header is : 

      SUBROUTINE umat_gurson(STRESS,STATEV,DDSDDE,SSE,SPD,SCD,
     &        RPL,DDSDDT,DRPLDE,DRPLDT,
     &        STRAN,DSTRAN,TIM,DTIME,TEMP,DTEMP,PREDEF,DPRED, CMNAME,
     &        NDI,NSHR,NTENS,NSTATV,PROPS,NPROPS,COORDS,DROT,PNEWDT,
     &        CELENT,DFGRD0,DFGRD1,NOEL,NPT,LAYER,KSPT,KSTEP,KINC)


c    The estimated surface temperature at this time at this point (TEMP)
c    The step (KSETP) and increment (KINC) in which the routine is being called
c    The current value of the step (TIME(1)) and the total time (TIME(2))
c    The element number (NOEL) and integration point number (NPT)
c    The layer (LAYER) and section point numbers (KSPT), where appropriate
c    The coordinates of the integration point (COORDS) -> These are the current coordinates if the *STEP, NLGEOM option is used in this or a previous step
c    The array containing the solution-dependent state variables, STATEV(NSTATV), can be defined in USDFLD
c        -> These are passed in as the values at the beginning of the increment
c        -> In all cases, STATEV can be updated in this subroutine, and the updated values are passed into other user subroutines (CREEP, UMAT, ...) that are called atthis material point
c        -> The number of state variables associated with this material point is defined with the *DEPVAR option
c    The ratio, PNEWDT, of suggested new time increment to the time increment being used (DTIME) can be given
c    The name (CNAME) of the associated *MATERIAL option
c    The number of direct stress components (NDI) and the number of shear stress components (NSHR)
c    The current time increment (DTIME)
c    The layer (LAYER) abd tge section point numbers (KSPT), where appropriate


c       INCLUDE 'ABA_PARAM.INC' 

c kw has to be put in parameter

c    The implicit none statement is used to inhibit a very old feature of Fortran that by default treats all variables that start with the letters i, j, k, l, m and n as integers and all the other         c    variables as real arguments. Implicit none should always be used! It prevents potential confusion in variable types, and makes detection of typographic errors easier.


c       INCLUDE 'ABA_PARAM.INC' 

      implicit none
	
c    integer = entier (n=10)
c    real = rÃƒÂ©el (y = 1.0)
c    complex = complexe (z = cmplx(2.5))
c    character = caractÃƒÂ¨re (c = 'd')
c    logical = boolÃƒÂ©en (b =.true.)

c    All the variables must be claimed and put the statement " implicit none " at the beginning of all the declaration bloc 

      integer ntens, nstatv,nprops
	logical*8 Bool
      CHARACTER*8 CMNAME
      REAL*8 STRESS(NTENS),STATEV(NSTATV),
     1     DDSDDE(NTENS,NTENS),DDSDDT(NTENS),DRPLDE(NTENS),
     2     STRAN(NTENS),DSTRAN(NTENS),TIM(2),PREDEF(1),DPRED(1),
     3     PROPS(NPROPS),COORDS(3),DROT(3),
     4     DFGRD0(3,3),DFGRD1(3,3)

c The include statement sets the proper precision for floating point variables (REAL*8 on most machines)
c The material name, CMNAME is an 8-byte character variable

c     ***********************************************************************

      real*8 SSE, SPD, SCD, RPL, DTIME, TEMP, CELENT, DRPLDT
      real*8 PNEWDT, DTEMP, R, dR, ddR
      integer NDI, NSHR, NOEL, NPT, LAYER, KSPT, KSTEP, KINC, compt


      REAL*8 Fppr(3,3), Cepr(3,3), Ce(3,3), CeprINV(3,3),
     & CeINV(3,3), FpprINV(3,3), FpprINVT(3,3), Ee(3,3),
     & Eepr(3,3), DevEe(3,3), kcorDevPr(3,3), kcorDevFinal(3,3)
      REAL*8 WR(1,3), WI(1,3), Fepr(3,3), FeprT(3,3), Fe(3,3),
     &  FeT(3,3), FpT(3,3), FpINV(3,3), FpINVT(3,3)
      REAL*8 TrEe, KT, ppr, pFinal, FpprT(3,3), ppk(3,3),
     & ppk1(3,3), ppk2(3,3), ppk21(3,3), Tro3, fv_min, fv_max
	REAL*8 fv_hat, fv_lb, fv_ub, incrD, incrQ, incrP, v(3),
     & kcorEqPr, kcorEq, Visc, k_y, p_corr
	REAL*8 term1, term2, term3, fG, rp(3), norm_rp, NdG, dfGdfv_hat,
     & zeta, kw, Bn, dfvdD, J11, TOL, NqG, dfvdQ, J12, fac
	REAL*8 dfGdky, dfvdP, J13, An, h, J31, J32, J33, fV0,
     & dNqdfv_hat, dNddkEq, J21, dNqdp, J22, dNqdky, t1, t2, dNddky
	REAL*8 J23, Jv(3,3), JvINV(3,3), JvINVT(3,3), incrV(3),
     & kcorDev(3,3), Mat(3,3), gammaNp(3,3), p_visc, q_visc, eta_0
	REAL*8 Vi, dVi, eta_1,dT,incrfv, ExpM(3,3), val, val2, lambda0,
     & kappa,alpha,beta,Ct,fT,ptau,ntau,NdT,dftdkhi_hat 
	REAL*8 dkhidfv,NqT,dkhidP,dfTdky,dNqdkEq,dNqdpcorr,dNddpcorr,
     & Boolval,dNddkEqT,A0,dAn,fT_save,dfvhatdfv,dkhihatdkhi

     


c     MATERIAL PARAMETERS
      real*8 lm,young,nu, q1, q2, q3, sy0, h_hard, h_exp, smuT
      real*8 dens
      integer  hardlawInd, kindOfLaw2, Iter_max 
c     INTERNAL VARIABLES
      real*8 fvn, fv, hatpn, hatp, hatqn, hatq, hatdn, hatd
	real*8 khin,khi,khi_hat, khi_lb, khi_ub, khi_min, khi_max
	real*8 c, n, h1, h2, hatp1, hatp2, n1, n2, Pa, g0
      real*8 Fpn(3,3), Fp(3,3)
      real*8 Floc(3,3),Floc_t(3,3), FlocINV(3,3), FlocINVT(3,3)
      real*8 sigma(3,3),Fdet
      integer i,j,k,ii,jj,kk,ll,pp,nn,mm,istep,iii,jjj

c    All the properties are defined directly thanks to "props" 

      dens=props(1)
      young=props(2)
      nu=props(3)
      smuT=young/(2.*(1.+nu))
      lm=young*nu/((1.+nu)*(1.-2.*nu))
      KT = young / (3.*(1.- 2.*nu))
      q1=props(4)
      q2=props(5)
      q3=props(6)
	fv0 = props(7)
      hardlawInd=props(8)
      sy0=props(9)
      h_hard=props(10)
      h_exp=props(11)
	TOL = props(12)
	kw = props(13)
	Iter_max = props(14)
	lambda0 = props(15)
	kappa = props(16)
	n = props(17)
      c = props(18)
	h1 = props(19)
	h2 = props(20)
	hatp1 = props(21)
	hatp2 = props(22)
	n1 = props(23)
	n2 = props(24)
	kindOfLaw2 = props(25)
	eta_0 = props(26)
	q_visc = props(27)
	g0 = props(28)  
	A0 = props(29)
	fv_lb = props(30)
	fv_ub = props(31)
	fv_min = props(32)
	fv_max = props(33)
	khi_lb = props(34)
	khi_ub = props(35)
	khi_min = props(36)
	khi_max = props(37)
	compt = 0
	Pa = hatp/g0
	



c**** We first initialize the different properties related to plasticity **********

c     porosity      
      fvn=STATEV(1)

c     equivalent matrix plastic strain      
      hatpn=STATEV(2)

c     macro volumetric plastic deformation = tr(Dp)
      hatqn=STATEV(3)

c     macro deviatoric plastic equivalent deformation sqrt(2/3*devDp:DevDp)
      hatdn=STATEV(4) 

c	Value of the Boolean flag
	Boolval = STATEV(5)

	If (Boolval == 0.0) Then
	
		Bool = .false.
	Else 
		Bool = .true.

	End if

	Khin = STATEV(6)

c     plastic strain
      do,ii=1,3
        do,jj=1,3
          Fpn(ii,jj)=STATEV(6+(ii-1)*3+jj) 
        enddo
      enddo

c	khin must be different from zero

	If (Khin .lt. 1.e-8) Then
	  call ligam(fvn,lambda0,kappa,hatpn,Khin)
c        If(Khin .gt. 0.99) Then
c          Khin=0.99
c        End If
	End if 



cc****************************ELASTIC PREDICTOR***********************************

c     Predictor of internal variables : the plastic deformation gradient, as 
c     well as the other variables linked to plasticity is kept unchanged 
c     => (Z_pr = Z^n, including F_pr^p = F^pn)

      fv=fvn
      hatp=hatpn
      hatq=hatqn
      hatd=hatdn
	khi = Khin

cc*************************Exponential smoothing*********************************


	IF (fv .LE. fv_min) THEN 
	  CALL expo_smooth(fv_lb,fv_min,fv, fv_hat)
	ELSEIF (fv .GT. fv_min .AND. fv .LE. fv_max) THEN
	  fv_hat = fv
	ELSEIF (fv .GT. fv_max) THEN 
	  CALL expo_smooth(fv_ub,fv_max,fv, fv_hat)
	ENDIF


	IF (khi .LE. khi_min) THEN 
	  CALL expo_smooth(khi_lb,khi_min,khi, khi_hat)
	ELSEIF (khi .GT. khi_min .AND. khi .LE. khi_max) THEN
	  khi_hat = khi
	ELSEIF (khi .GT. khi_max) THEN 
	  CALL expo_smooth(khi_ub,khi_max,khi, khi_hat)
	ENDIF


cc********************************************************************************

c    Definition of the deforation gradient 
      do,i=1,3
         do,j=1,3
            Floc(i,j)=DFGRD1(i,j)
            Floc_t(j,i)=DFGRD1(i,j)
         enddo
      enddo

c     Calculate inverse of Fppr
      do,ii=1,3
        do,jj=1,3
          Fppr(ii,jj)=Fpn(ii,jj)
        enddo
      enddo

c     Compute the inverse of Fppr (Right factor of Eq.4)
      CALL M33INVT (Fppr, FpprINV, FpprINVT)

      CALL M33T(Fppr,FpprT)

c     Calculate Fepr (Eq.4)
      CALL MATRIXMULT (Floc, FpprINV, Fepr)

c     Compute the transpose and the inverse of F 
      CALL M33INVT(Floc, FlocINV, FlocINVT)

c     Calculate Cepr (Left Eq.5)
      CALL M33T(Fepr,FeprT)
      CALL CauchyElasPred(FeprT,Fepr,Cepr)  

c     Find the Strain tensor predictor (Eepr) (Right Eq.5)    
      CALL StrainTensP (Cepr, Eepr)
      CALL M33INV(Cepr, CeprINV)


c     Find the elastic predictor. Here, we are first compute the deviator of E_pr^e. For that purpose, 
c     the following formula is used : dev M = M - (Trace(M))/n * I_n
      CALL DevTrace(Eepr, TrEe, devEe, Tro3)

c     Then, the stress predictor in the corrotational frame is (Left in Eq.6) and p_pr (Right in Eq.6) 
      CALL kcorcomp(devEe, TrEe, smuT, KT, kcorDevPr, pPr)



cc***************************PLASTIC FLOW CRITERION******************************************

c	Initialization of the NR scheme

	incrD = 0.0
	incrQ = 0.0
	incrP = 0.0
	incrfV = 0.0	 

c     Compute the equivalent predictor corrotational stress Tau_pr^dev (Mid Eq.6)
      CALL kcor_eqpr(kcorDevPr, kcorEqPr)

c     Computation of the equivalent Kirchhoff stress in the corrotational frame (Left Eq.41)
      CALL kcor_eq(kcorEqPr,smuT,incrD, kcorEq)


c     Compute R from the equation page 2 for tau_Y
      call harderningLawMatrix(hardlawInd, sy0, h_hard, h_exp,
     &      hatp, hatp1, hatp2, n1, n2, c, n, h1, h2, R, dR, ddR)

c     Compute V from the equation page 2 for tau_Y  (TO BE CONTINUED..............)
c      call viscosity(kindOfLaw2, p_visc, q_visc, Pa, eta_0,
c     &                     eta_1, g0, Vi, dVi)

c     Compute yield stress (Full equation on bottom page 2)
      call yield_str(R,k_y)

c 	Compute h from Eq 25
	call h_Eq25(dR,h)

c     Compute the corrected pressure p' (Right in Eq.41) (intially dQ=0 -> p_corr=ppr)
      call corr_pressure(KT,incrQ,pPr,p_corr)


cccccccccccccccccccccccccc--------If we use Gurson---------cccccccccccccccccccccccccccccc 

c     We determine if this is Gurson or Thomason that must be used (True = Gurson)
	If (Bool .eqv. .false.) Then 

c	  Compute term 1 of Eq 10 for the Gurson yield surface fG
	  call term_1(kcorEq,k_y,term1)

c	  Compute term 2 of Eq 10 for the Gurson yield surface fG
	  call term_2(fv_hat,q1,q2,p_corr,k_y,term2)

c	  Compute term 3 of Eq 10 for the Gurson yield surface fG
	  call term_3(q3,fv_hat,term3)

c	  Compute Gurson yield surface (Eq. 10)
	  call f_G(term1,term2,term3,fG)

c	  Compute initial residual rp
	  rp(1) = fG
	  rp(2) = 0.0
	  rp(3) = 0.0


cccccccccccccccccccccccccc--------If we use Thomason---------cccccccccccccccccccccccccccccc

	Elseif (Bool .eqv. .true.) Then 

c	  Computation of alpha and beta
	  call pro(h_exp,alpha,beta)

c	  Compute the concentration factor
	  call C_t(khi_hat,alpha,beta,Ct)

c	  Compute the first term of Eq.11
	  call f_T(kcorEq,p_corr,n,Ct,k_y,fT)

c	  Compute initial residual rp
	  rp(1) = fT/sy0
	  rp(2) = 0.0
	  rp(3) = 0.0

	End if


c	Compute initial yield surface 
 	CALL norm3(rp,norm_rp)

cc-------------------------------------------PLASTIC CORRECTION--------------------------------------------------------

c     We determine if this is Gurson or Thomason that must be used (False = Gurson)
	If (Bool .eqv. .false.) Then 

cc 	We have to check the yield criterion meaning that if fG > 0 => Solve the residual form. Otherwise, tau = tau_pr

	  IF (fG .GT. 0.0)  THEN

	    DO WHILE(norm_rp .GT. TOL)


	compt = compt + 1

c	Stop the simulation if it exceeds a certain amount of step
		If (compt .gt. Iter_max)  THEN
		  write(*,*) 'Number of maximum iteration is exceeded!' 
		End If


c		Computation of the Jacobian matrix

c 		J11
		call normalD(kcorEq,k_y,NdG)
		call derfGderfv(q1,q2,q3,p_corr,k_y,fv,dfGdfv_hat)

	      IF (fv .LE. fv_min) THEN 
	        CALL derfvhatderfv(fv_lb,fv_min,fv, dfvhatdfv)
	      ELSEIF (fv .GT. fv_min .AND. fv .LE. fv_max) THEN
	        dfvhatdfv = 1.0  
	      ELSEIF (fv .GT. fv_max) THEN 
	        CALL derfvhatderfv(fv_ub,fv_max,fv, dfvhatdfv)
	      ENDIF

		call lode(kcorDevPr,kcorEqPr,zeta)
		call GrowthContribution(kw,zeta,Bn)
		call derfvhatderd(Bn,incrQ,incrD,fv_hat,dfvdD)
	      call Jacobian11(smuT,NdG,dfGdfv_hat,dfvhatdfv,dfvdD,J11)

c		J12 
	      call normalQ(q1,q2,fv_hat,k_y,p_corr,NqG)
	      call derfvhatderq(Bn,incrQ,incrD,fv_hat,dfvdQ)
	      call Jacobian12(KT,NqG,dfGdfv_hat,dfvhatdfv,dfvdQ,J12)

c		J13
	      call Nucleation(A0,An,dAn)
	      call derfGderky(kcorEq,k_y,q1,q2,p_corr,fv_hat,dfGdky,fac)
	      call derfvhatderp(An,dAn,Bn,incrP,incrQ,incrD,dfvdP)
	      call Jacobian13(dfGdky,h,dfGdfv_hat,dfvhatdfv,dfvdP,J13)

c		J21

	      CALL derNqderfvhat(k_y,q1,q2,p_corr,dNqdfv_hat)
	      call derNdderkEq(k_y,dNddkEq)
	      call Jacobian21(smuT,NqG,incrD,incrQ,dNqdfv_hat,dfvdD,
     &                     dNddkEq,dfvhatdfv,J21)

c 		J22 
		call derNqderp(q1,q2,fv_hat,k_y,p_corr,dNqdp)
	      call Jacobian22(KT,NdG,incrD,incrQ,dfvdQ,dNqdfv_hat,dNqdp,
     &                      dfvhatdfv,J22)

c		J23 
	      call derNqderky(q1,q2,fv_hat,p_corr,k_y,t1,t2,dNqdky)
	      call derNdderky(kcorEq,k_y,dNddky)
	      call Jacobian23(incrD,incrQ,h,dNqdfv_hat,dfvdP,dNqdky,
     &                     dNddky,dfvhatdfv,J23)

c 		J31
		call Jacobian31(kcorEq,smuT,incrD,J31)

c		J32
		call Jacobian32(p_corr,KT,incrQ,J32)

c 		J33
		call Jacobian33(fV0,k_y,h,incrP,J33)

c		Assembly of all the components into a Jv matrix
		Jv(1,1) = J11
		Jv(1,2) = J12
		Jv(1,3) = J13
		Jv(2,1) = J21
		Jv(2,2) = J22
		Jv(2,3) = J23
		Jv(3,1) = J31/sy0
		Jv(3,2) = J32/sy0
		Jv(3,3) = J33/sy0

c		Compute of the inverse of the Jacobian
		call M33INVT(Jv,JvINV,JvINVT)

c		Find the increment dv (From Ax = b) (x)
		call MULMATVEC(JvINV,-rp,incrV)

c		Update of v : v = v + incrV
	
		incrD = incrD+incrV(1)

		If (incrD .lt. 0.0) Then
		  incrD = -incrD/float(compt)
		End if

		If (incrD .gt. kcorEqPr/(3.*smuT)) Then
		  incrD = kcorEqPr/(3.*smuT*float(compt))
		End if 

		incrQ = incrQ+incrV(2)	
		incrP = incrP+incrV(3)
	
		If  (incrP .lt. 0.0) Then
		  incrP = -incrP/float(compt)
		End if
	
		hatp = hatpn + incrP
		hatq = hatqn + incrQ
		hatd = hatdn + incrD

CC    	Computation of the yield surface after plastic correction to put in Eq.16
c		Compute the increment of porosity inside the material                
		CALL incr_fv(fvn,incrQ,An,incrP,Bn,incrD,incrfv)

		If (incrfv .lt. 0.0) then
		  incrfv = -incrfv/float(compt) 
		end if

c 		Compute the update value of the porosity
		fv = fvn + incrfv  

c		We employ the exponential smoothing for robustess
		IF (fv .LE. fv_min) THEN 
	  	  CALL expo_smooth(fv_lb,fv_min,fv, fv_hat)
		ELSEIF (fv .GT. fv_min .AND. fv .LE. fv_max) THEN
	  	  fv_hat = fv
		ELSEIF (fv .GT. fv_max) THEN 
	  	  CALL expo_smooth(fv_ub,fv_max,fv, fv_hat)
		ENDIF

c     	Computation of the equivalent Kirchhoff stress in the corrotational frame (Left Eq.41)
      	CALL kcor_eq(kcorEqPr,smuT,incrD, kcorEq)

c     	Compute R from the equation page 2 for tau_Y
      	call harderningLawMatrix(hardlawInd, sy0, h_hard, h_exp,
     &          hatp, hatp1, hatp2, n1, n2, c, n, h1, h2, R, dR, ddR)

c     	Compute V from the equation page 2 for tau_Y  
c      	call viscosity(kindOfLaw2, p_visc, q_visc, Pa, eta_0,
c     	&                     eta_1, g0, Vi, dVi)

c 		Compute h from Eq 25
		call h_Eq25(dR,h)

c     	Compute yield stress (Full equation on bottom page 2)
      	call yield_str(R,k_y)

c     	Compute the corrected pressure p' (Right in Eq.41) (intially dQ=0 -> p_corr=ppr)
      	call corr_pressure(KT,incrQ,ppr,p_corr)

c		Compute term 1 of Eq 10 for the Gurson yield surface fG
		call term_1(kcorEq,k_y,term1)

c		Compute term 2 of Eq 10 for the Gurson yield surface fG
		call term_2(fv_hat,q1,q2,p_corr,k_y,term2)

c		Compute term 3 of Eq 10 for the Gurson yield surface fG
		call term_3(q3,fv_hat,term3)

c		Compute Gurson yield surface (Eq. 10)
		call f_G(term1,term2,term3,fG)

c		Update of rp 
		rp(1) = fG  
		rp(2) = incrD*NqG - incrQ*NdG
		rp(3) = ((1.-fV0)*k_y*incrP-kcorEq*incrD-p_corr*incrQ)/sy0


c		Update of the norm of rp
		CALL norm3(rp,norm_rp)


	    ENDDO


c         Computation of the deviatoric corrected stress (Middle Eq.41)
	    CALL kcor_dev(smuT,incrD,kcorEqPr,kcorDevPr, kcorDev)

c 	    Definition of a unit matrix (needed hereafter)
	    CALL MATUNI(Mat)
 
c         Computation of the plastic increment (Eq.43)
	    CALL DeltagammaNp(kcorDev,incrD,incrQ,kcorEq,Mat, gammaNp)

c         Computation of the current plastic deformation gradient, re-expressend in terms 
c         of the plasic update (Delta gamma N^p) and the predictor state.

	    CALL Expon(gammaNp,ExpM)

c 	    Computation of equation 42
	    Fp = MATMUL(ExpM,Fppr)

c	    Computation of Thomason Yield surface to see if fT is greater than zero.

c	    Computation of alpha and beta
	    call pro(h_exp,alpha,beta)

c	    Compute the ligament ratio
	    call ligam2(fvn,incrfv,kappa,incrP,khin,khi)
          
c         Employ the exponential smoothing
	    IF (khi .LE. khi_min) THEN 
	  	CALL expo_smooth(khi_lb,khi_min,khi, khi_hat)
	    ELSEIF (khi .GT. khi_min .AND. khi .LE. khi_max) THEN
	  	khi_hat = khi
	    ELSEIF (khi .GT. khi_max) THEN 
	  	CALL expo_smooth(khi_ub,khi_max,khi, khi_hat)
	    ENDIF

c	    if(khi.eq.0.99) then
c            khi=0.99
c          end if

c	    Compute the concentration factor
	    call C_t(khi_hat,alpha,beta,Ct)

c	    Compute the first term of Eq.11
	    call f_T(kcorEq,p_corr,n,Ct,k_y,fT)

c	    Verify if fT is greater than zero 
	    If (fT .gt. 0.0) Then
		Bool = .true.
	    End if


c	    UPDATE if we are in elastic case
	  ELSE 

	  do,ii=1,3
	    do,jj=1,3
		Fp(ii,jj)=Fppr(ii,jj)
	    enddo
	  enddo

	  END IF


cccccccccccccccccccccc----Now if we use Thomason-------ccccccccccccccccccccccccccc
 
	Elseif (Bool .eqv. .true.) Then
	
cc 	We have to check the yield criterion meaning that if fT > 0 => Solve the residual form. 		Otherwise, tau = tau_pr

        call f_T(kcorEq,p_corr,n,Ct,k_y,fT)
        
        If(ft .gt. 0.) then

c	    Computation of the positive and negative tau 
	    Call tau2(kcorEq,p_corr,ptau,ntau)

c	    Computation of the normals
 	    Call NormaldT(n,ptau,ntau,NdT)
	    Call NormalqT(n,ptau,ntau,NqT)

	    DO WHILE(norm_rp .GT. TOL)

	    compt = compt + 1

c	    Stop the simulation if it exceeds a certain amount of step
		If (compt .gt. Iter_max)  THEN
c		  write(*,*) 'Number of maximum iteration is exceeded!' 
c  	        write (*,*) compt, rp(1)
c	        write (*,*) compt, rp(2)
c	        write (*,*) compt, rp(3)
	        write (*,*) compt, khi
		  write (*,*) compt, khi_hat
		End If

c		Computation of the Jacobian matrix

c		Derivative of the regularized porosity 

	      IF (fv .LE. fv_min) THEN 
	        CALL derfvhatderfv(fv_lb,fv_min,fv, dfvhatdfv)
	      ELSEIF (fv .GT. fv_min .AND. fv .LE. fv_max) THEN
	        dfvhatdfv = 1.0  
	      ELSEIF (fv .GT. fv_max) THEN 
	        CALL derfvhatderfv(fv_ub,fv_max,fv, dfvhatdfv)
	      ENDIF


c		Derivative of the regularized ligament ratio
		
	      IF (khi .LE. khi_min) THEN 
	        CALL derkhihatderkhi(khi_lb,khi_min,khi, dkhihatdkhi)
	      ELSEIF (khi .GT. khi_min .AND. khi .LE. khi_max) THEN
	        dkhihatdkhi = 1.0  
	      ELSEIF (khi .GT. khi_max) THEN 
	        CALL derkhihatderkhi(khi_ub,khi_max,khi, dkhihatdkhi)
	      ENDIF

c		J11 
		Call derftdki_hat(k_y,khi,alpha,beta,dftdkhi_hat)
		Call derkhiderfv(Khin,fvn,incrfv,kappa,incrP,dkhidfv)
        	
		if(khi .eq. 0.99) then
              dkhidfv=0.
            end if

		Call lode(kcorDevPr,kcorEqPr,zeta)
		Call GrowthContribution(kw,zeta,Bn)
		Call derfvhatderd(Bn,incrQ,incrD,fv_hat,dfvdD)
	      call J11T(smuT,NdT,dftdkhi_hat,dkhihatdkhi,dkhidfv,
     &               dfvhatdfv,dfvdD,J11)

c		J12
		Call derfvhatderq(Bn,incrQ,incrD,fv_hat,dfvdQ)
		Call J12T(KT,NqT,dftdkhi_hat,dkhidfv,dkhihatdkhi,dfvhatdfv,
     &               dfvdQ,J12)

c		J13
		dfTdky = -Ct
		call Nucleation(A0,An,dAn)
		Call derfvhatderp(An,dAn,Bn,incrP,incrQ,incrD,dfvdP)
		Call derkhidp(fvn,incrfv,kappa,incrP,Khin,dkhidP)
        
		if(khi .eq. 0.99) then
          	  dkhidP=0.
        	end if

		Call J13T(dfTdky,h,dftdkhi_hat,dkhidfv,dfvdP,dkhidP,
     &              dkhihatdkhi,dfvhatdfv,J13)

c		J21 
		Call derNqderkeq(n,ptau,ntau,dNqdkEq)
		Call derNddTerkeq(n,ptau,ntau,dNddkEqT)
		Call J21T(NqT,incrD,dNqdkEq,smuT,incrQ,dNddkEqT,J21)

c		J22
		Call derNqderpcorr(n,ptau,ntau,dNqdpcorr)
		Call derNdderpcorr(n,ptau,ntau,dNddpcorr)
		Call J22T(incrD,KT,dNqdpcorr,NdT,incrQ,dNddpcorr,J22)

c		J23 
		J23 = 0.0

c 		J31
		Call Jacobian31(kcorEq,smuT,incrD,J31)

c		J32
		Call Jacobian32(p_corr,KT,incrQ,J32)

c 		J33
		Call Jacobian33(fV0,k_y,h,incrP,J33)

c		Assembly of all the components into a Jv matrix
		Jv(1,1) = J11/sy0
		Jv(1,2) = J12/sy0
		Jv(1,3) = J13/sy0
		Jv(2,1) = J21
		Jv(2,2) = J22
		Jv(2,3) = J23
		Jv(3,1) = J31/sy0
		Jv(3,2) = J32/sy0
		Jv(3,3) = J33/sy0

c		Compute of the inverse of the Jacobian
		call M33INVT(Jv,JvINV,JvINVT)

c		Find the increment dv (From Ax = b) (x)
		call MULMATVEC(JvINV,-rp,incrV)

c		Update of v : v = v + incrV
	
		incrD = incrD+incrV(1)

		If (incrD .lt. 0.0) Then
		  incrD = -incrD/float(compt)
		End if

		If (incrD .gt. kcorEqPr/(3.*smuT)) Then
		  incrD = kcorEqPr/(3.*smuT*float(compt))
		End if 

		incrQ = incrQ+incrV(2)	
		incrP = incrP+incrV(3)
	
		If  (incrP .lt. 0.0) Then
		  incrP = -incrP/float(compt)
		End if
	
		hatp = hatpn + incrP
		hatq = hatqn + incrQ
		hatd = hatdn + incrD

CC    	Computation of the yield surface after plastic correction to put in Eq.16
c		Compute the increment of porosity inside the material                
		CALL incr_fv(fvn,incrQ,An,incrP,Bn,incrD,incrfv)
	
		If (incrfv .lt. 0.0) then
		  incrfv = -incrfv/float(compt) 
		End if

c 		Compute the update value of the porosity
		fv = fvn + incrfv
        
c		We employ the exponential smoothing for robustess
		IF (fv .LE. fv_min) THEN 
	  	  CALL expo_smooth(fv_lb,fv_min,fv, fv_hat)
		ELSEIF (fv .GT. fv_min .AND. fv .LE. fv_max) THEN
	  	  fv_hat = fv
		ELSEIF (fv .GT. fv_max) THEN 
	  	  CALL expo_smooth(fv_ub,fv_max,fv, fv_hat)
		ENDIF	

c		if(fv.gt.0.99) then
c          	  fv=0.99
c          	  incrfv=fv-fvn
c        	endif
 
c		Compute the current ligament ratio
		call ligam2(fvn,incrfv,kappa,incrP,Khin,khi)
        
		IF (khi .LE. khi_min) THEN 
	  	  CALL expo_smooth(khi_lb,khi_min,khi, khi_hat)
		ELSEIF (khi .GT. khi_min .AND. khi .LE. khi_max) THEN
	  	  khi_hat = khi
		ELSEIF (khi .GT. khi_max) THEN 
	  	  CALL expo_smooth(khi_ub,khi_max,khi, khi_hat)
		ENDIF

		if (khi.gt.0.99) then
              khi=0.99
        	endif

c     	Computation of the equivalent Kirchhoff stress in the corrotational frame (Left Eq.41)
      	CALL kcor_eq(kcorEqPr,smuT,incrD, kcorEq)

c     	Compute R from the equation page 2 for tau_Y
      	call harderningLawMatrix(hardlawInd, sy0, h_hard, h_exp,
     &      	hatp, hatp1, hatp2, n1, n2, c, n, h1, h2, R, dR, ddR)

c     	Compute V from the equation page 2 for tau_Y  
c      	call viscosity(kindOfLaw2, p_visc, q_visc, Pa, eta_0,
c     &                     eta_1, g0, Vi, dVi)

c 		Compute h from Eq 25
		call h_Eq25(dR,h)

c     	Compute yield stress (Full equation on bottom page 2)
      	call yield_str(R,k_y)

c     	Compute the corrected pressure p' (Right in Eq.41) (intially dQ=0 -> p_corr=ppr)
      	call corr_pressure(KT,incrQ,ppr,p_corr)

c		Compute the update of ptau and ntau
		Call tau2(kcorEq,p_corr,ptau,ntau)

c		Compute the update normals
		Call NormaldT(n,ptau,ntau,NdT)
		Call NormalqT(n,ptau,ntau,NqT)

c		Compute the concentration factor
		call C_t(khi_hat,alpha,beta,Ct)

c		Compute the first term of Eq.11
		call f_T(kcorEq,p_corr,n,Ct,k_y,fT)


c		Update of rp 
		rp(1) = (fT)/sy0  
		rp(2) = (incrD*NqT - incrQ*NdT)
		rp(3) = ((1.-fV0)*k_y*incrP-kcorEq*incrD-p_corr*incrQ)/sy0


c		Update of the norm of rp
		CALL norm3(rp,norm_rp)
	
	    End do

c         Computation of the deviatoric corrected stress (Middle Eq.41)
	    CALL kcor_dev(smuT,incrD,kcorEqPr,kcorDevPr, kcorDev)

c 	    Definition of a unit matrix (needed hereafter)
	    CALL MATUNI(Mat)

c         Computation of the plastic increment (Eq.43)
	    CALL DeltagammaNp(kcorDev,incrD,incrQ,kcorEq,Mat, gammaNp)

c         Computation of the current plastic deformation gradient, re-expressend in terms 
c         of the plasic update (Delta gamma N^p) and the predictor state.

	    CALL Expon(gammaNp,ExpM)

c 	    Computation of equation 42
	    Fp = MATMUL(ExpM,Fppr) 

	

c	  UPDATE if we are in elastic case
	  ELSE 

	  do,ii=1,3
	    do,jj=1,3
		Fp(ii,jj)=Fppr(ii,jj)
	    enddo
	  enddo

	  END IF
      END IF
ccccccccccccccccccccccc-END-PLASTIC-CORRECTOR-cccccccccccccccccccccc


c     Find Fe
c     Compute the inverse of Fppr (Right factor of Eq.4)
      CALL M33INVT (Fp, FpINV, FpINVT)

c     Compute the transpose of Fppr
      CALL M33T(Fp,FpT)

c     Calculate Fepr (Eq.4)
      CALL MATRIXMULT (Floc, FpINV, Fe)

c     Calculate Ce
c     Calculate Ce (Left of Eq.5) :
      CALL M33T(Fe,FeT)
      CALL CauchyElasPred(FeT,Fe,Ce)  

c     Find the Strain tensor predictor (Right Eq.5)
      CALL StrainTensP (Ce, Ee)
      CALL M33INV(Ce, CeINV)

c     Recompute stress (first compute (E_pr^e)^dev)
      CALL DevTrace (Ee, TrEe, devEe, Tro3)
      CALL kcorcomp (devEe, TrEe, smuT, KT, kcorDevFinal, pFinal)


      ppk1= pFinal * FlocINVT
      ppk21 = MATMUL(CeINV, kcorDevFinal)
      ppk2 = MATMUL(MATMUL(Fe,ppk21), FpINVT)
      ppk = ppk1 + ppk2
      
      
      sigma=matmul(ppk,Floc_t)

      Fdet=Floc(1,1)*Floc(2,2)*Floc(3,3)+
     1 Floc(1,2)*Floc(2,3)*Floc(3,1)+
     1 Floc(1,3)*Floc(2,1)*Floc(3,2)-
     1 Floc(1,3)*Floc(2,2)*Floc(3,1)-
     1 Floc(2,3)*Floc(3,2)*Floc(1,1)-
     1 Floc(3,3)*Floc(1,2)*Floc(2,1)


      do,ii=1,3
        STRESS(ii)=sigma(ii,ii)/Fdet
      enddo
      STRESS(4)=sigma(1,2)/Fdet
      STRESS(5)=sigma(1,3)/Fdet
      STRESS(6)=sigma(2,3)/Fdet

c	Convert a logical value to a real one for simplicity 
	Boolval = merge(1.d0, 0.d0, Bool)

	
c    put back internal variables       
      STATEV(1)=fv
      STATEV(2)=hatp
      STATEV(3)=hatq  
      STATEV(4)=hatd 
	STATEV(5)=Boolval
	STATEV(6)=khi

      do,ii=1,3
        do,jj=1,3
          STATEV(6+(ii-1)*3+jj)=Fp(ii,jj) 
        enddo
      enddo
    


      return
      end





cc==============================================SUBROUTINES==========================================================



c----------------------------------------------HARDENING LAWS--------------------------------------------------------


      SUBROUTINE harderningLawMatrix(kindOfLaw, sy0, h_hard, h_exp,
     &           hatp,hatp1,hatp2,n1,n2,c,n,h1,h2, R, dR, ddR) 
      IMPLICIT NONE
c     =====  'ARGUMENTS:'
      REAL*8 sy0, h_hard, h_exp, hatp, R, dR, ddR
	REAL*8 c, n, h1, h2, hatp1, hatp2, n1, n2
      INTEGER kindOfLaw

      R=sy0
      dR=0.
      ddR=0.

cc    There are different hardening laws that can be implemented. Here are some of them

c	======
c     1) power law  (R = yield0 + h p^hexp)

      IF(kindOfLaw.eq.1 .and. hatp.gt.0.) THEN
         R=sy0+h_hard*(hatp**h_exp);
         dR=h_hard*h_exp*(hatp**(h_exp-1.));
         ddR=dR*(h_exp-1.)/hatp;
      END IF
    

c     ======
c     2) Exponential law  (R = yield0 +h * (1-exp (-hexp *p))

	IF(kindOfLaw.eq.2 .and. hatp.gt.0.) THEN
         R   = sy0 + h_hard*(1. - EXP(-h_exp*hatp));
         dR  = h_hard*h_exp*EXP(-h_exp*hatp);
         ddR = -dR*h_exp;
      END IF


c     ======
c     3) Enhanced exponential law
c	   R = yield0 +h * (1-exp (-hexp *p))*(1+c*p)^n


      IF(kindOfLaw.eq.3 .and. hatp.gt.0.) THEN
         R   = sy0 + h_hard*(1. - EXP(-h_exp*hatp))*(1. + c*hatp)**n;
         dR  = h_hard*h_exp*EXP(-h_exp*hatp)*(1. + c*hatp)**n 
     &	 + h_hard*(1.-EXP(-h_exp*hatp))*c*n*(1. + c*hatp)**(n-1.);
         ddR = -h_hard*(h_exp**2.)*EXP(-h_exp*hatp)*(1. + c*hatp)**n
     &	 + 2.*(h_hard*h_exp*EXP(-h_exp*hatp)*c*n*(1. 
     &	 + c*hatp)**(n-1.)) + h_hard*(1.-EXP(-h_exp*hatp))*(c**2.)
     &	 * n*(n-1.)*(1.+c*hatp)**(n-2.);
      END IF


c     =====
c	4) Linear exponential (R = yield0 +h1 * p + h2* (1- exp(-hexp*p)))

	IF(kindOfLaw.eq.4 .and. hatp.gt.0.) THEN
		R = sy0 + h1*hatp + h2*(1.-EXP(-h_exp*hatp))
		dR = h1 + h2*h_exp*EXP(-h_exp*hatp)
		ddR = -h2*h_exp**2.*EXP(-h_exp*hatp) 	
	END IF


c     =====
c     5) Perfectly plastic

      IF(kindOfLaw.eq.5 .and. hatp.gt.0.) THEN
         R   = sy0;
         dR  = 0.;
         ddR = 0.;
      END IF


c     =====
c	6) Mecar hardening law
	IF(kindOfLaw.eq.6 .and. hatp.gt.0) THEN
		IF(hatp.lt.hatp1) THEN
			R = sy0+h_hard*hatp		
			dR = h_hard
			ddR = 0.0
		ELSEIF(hatp.gt.hatp1 .and. hatp.lt.hatp2) THEN
			R = sy0+h_hard*hatp + (hatp/hatp1)**n1
			dR = h_hard+(n1/hatp1)*(hatp/hatp1)**(n1-1.)
			ddR = (n1*(n1-1.))/(hatp1**2.)*(hatp/hatp1)**(n1-2.) 	    
		ELSEIF(hatp.gt.hatp2) THEN
			R = sy0+h_hard*hatp + (hatp/hatp1)**n1 
     &              + (hatp/hatp2)**n2
			dR = h_hard+(n1/hatp1)*(hatp/hatp1)**(n1-1.) 
     &               + (n2/hatp2)*(hatp/hatp2)**(n2-1.)
			ddR = (n1*(n1-1.)/hatp1**2.)*(hatp/hatp1)**(n1-2.) 
     &                + (n2*(n2-1.)/hatp2**2.)*(hatp/hatp2)**(n2-2.)
		END IF
	END IF




      END SUBROUTINE harderningLawMatrix

c---------------------------------------------END HARDENING LAWS------------------------------------------------


c-----------------------------------------------VISCOSITY LAWS--------------------------------------------------

	SUBROUTINE viscosity(kindOfLaw2, p_visc, q_visc, Pa, eta_0,
     &                     eta_1, g0, Vi, dVi)
	IMPLICIT NONE 

	real*8 eta_0, p_visc, q_visc, Pa, eta_1, Vi, dVi, g0
	integer kindOfLaw2

c	======
c     1) Saturate Power Viscosity Law (R = _eta0 * pow(p,_q))

      IF(kindOfLaw2 .eq. 1.0) THEN
		IF(p_visc .lt. 1e-6 .and. q_visc .lt. 1.0) THEN
			Vi =  eta_0 * (1e-6)**q_visc*(p_visc/1e-6); 
    			dVi = eta_0 * (1e-6)**q_visc/1e-6;
		ELSE 
      		Vi = eta_0*p_visc**q_visc
			dVi = eta_0*q_visc*(p_visc**(q_visc-1.))
		END IF 
      END IF
	
c     =====
c     2) Saturate Sinh Viscosity Law	

	IF(kindOfLaw2 .eq. 2.0) THEN
		IF(Pa .lt. 1e-6) THEN
			Vi =  eta_0
    			dVi = 0.0
		ELSE 
			Vi = eta_1+(eta_0-eta_1)*Pa/SINH(Pa)
			dVi = (eta_0-eta_1)*(1./SINH(Pa) 
     &                  -(Pa*COSH(Pa))/(SINH(Pa)*SINH(PA)))/g0
		END IF 
      END IF
	

	END SUBROUTINE viscosity 

c---------------------------------------------END VISCOSITY LAWS

      SUBROUTINE M33INV (A, AINV)
      IMPLICIT NONE
      INTEGER I,J
      real*8 A(3,3), AINV(3,3)
      real*8 EPS 
      real*8 DET
      real*8 COFACTOR(3,3)
      EPS= 1.0D-10
      DET =   A(1,1)*A(2,2)*A(3,3)  
     &       - A(1,1)*A(2,3)*A(3,2)  
     &       - A(1,2)*A(2,1)*A(3,3)  
     &       + A(1,2)*A(2,3)*A(3,1)  
     &       + A(1,3)*A(2,1)*A(3,2)  
     &       - A(1,3)*A(2,2)*A(3,1)

      IF (ABS(DET) .LE. EPS) THEN
         AINV = 0.0D0
C         OK_FLAG1 = .FALSE.
         RETURN
      END IF

      COFACTOR(1,1) = +(A(2,2)*A(3,3)-A(2,3)*A(3,2))
      COFACTOR(1,2) = -(A(2,1)*A(3,3)-A(2,3)*A(3,1))
      COFACTOR(1,3) = +(A(2,1)*A(3,2)-A(2,2)*A(3,1))
      COFACTOR(2,1) = -(A(1,2)*A(3,3)-A(1,3)*A(3,2))
      COFACTOR(2,2) = +(A(1,1)*A(3,3)-A(1,3)*A(3,1))
      COFACTOR(2,3) = -(A(1,1)*A(3,2)-A(1,2)*A(3,1))
      COFACTOR(3,1) = +(A(1,2)*A(2,3)-A(1,3)*A(2,2))
      COFACTOR(3,2) = -(A(1,1)*A(2,3)-A(1,3)*A(2,1))
      COFACTOR(3,3) = +(A(1,1)*A(2,2)-A(1,2)*A(2,1))
      

C      AINV = TRANSPOSE(COFACTOR) / DET


      DO I = 1, 3
        DO J = 1, 3
          AINV(J,I) = COFACTOR(I,J) / DET
       END DO
      END DO       

C      OK_FLAG1 = .TRUE.

      RETURN

      END SUBROUTINE M33INV

      SUBROUTINE M33INVT (A, AINV, AINVT)
      IMPLICIT NONE
      INTEGER I,J
      real*8 A(3,3) , AINV(3,3)
      real*8 AINVT(3,3)

      real*8 EPS 
      real*8 DET
      real*8 COFACTOR(3,3)
      EPS = 1.0D-10


      DET =   A(1,1)*A(2,2)*A(3,3)  
     &       - A(1,1)*A(2,3)*A(3,2)  
     &       - A(1,2)*A(2,1)*A(3,3)  
     &       + A(1,2)*A(2,3)*A(3,1)  
     &       + A(1,3)*A(2,1)*A(3,2)  
     &       - A(1,3)*A(2,2)*A(3,1)

      IF (ABS(DET) .LE. EPS) THEN
         AINVT = 0.0D0

         RETURN
      END IF

      COFACTOR(1,1) = +(A(2,2)*A(3,3)-A(2,3)*A(3,2))
      COFACTOR(1,2) = -(A(2,1)*A(3,3)-A(2,3)*A(3,1))
      COFACTOR(1,3) = +(A(2,1)*A(3,2)-A(2,2)*A(3,1))
      COFACTOR(2,1) = -(A(1,2)*A(3,3)-A(1,3)*A(3,2))
      COFACTOR(2,2) = +(A(1,1)*A(3,3)-A(1,3)*A(3,1))
      COFACTOR(2,3) = -(A(1,1)*A(3,2)-A(1,2)*A(3,1))
      COFACTOR(3,1) = +(A(1,2)*A(2,3)-A(1,3)*A(2,2))
      COFACTOR(3,2) = -(A(1,1)*A(2,3)-A(1,3)*A(2,1))
      COFACTOR(3,3) = +(A(1,1)*A(2,2)-A(1,2)*A(2,1))

      DO I = 1, 3
        DO J = 1, 3
          AINV(J,I) = COFACTOR(I,J) / DET

       END DO
      END DO       

      DO I = 1, 3
        DO J = 1, 3

          AINVT(J,I) = AINV(I,J)
       END DO
      END DO       

      RETURN

      END SUBROUTINE M33INVT

      SUBROUTINE MATRIXMULT(A,B,C)
      IMPLICIT NONE
      
      REAL*8 A(3,3),B(3,3),C(3,3)

      C = MATMUL (A,B)
      
      END SUBROUTINE MATRIXMULT

      SUBROUTINE M33T (A, AT)
      IMPLICIT NONE
      INTEGER I,J
      real*8 A(3,3) 
      real*8 AT(3,3)

      DO I = 1,3
		DO J = 1,3
			
			AT(I,J) = A(J,I)

		END DO
	END DO

      RETURN

      END SUBROUTINE M33T

      SUBROUTINE CauchyElasPred(FeprT,Fepr,Cepr)
      IMPLICIT NONE
      INTEGER I,J
      real*8 FeprT(3,3), Fepr(3,3), Cepr(3,3)

      Cepr= MATMUL(FeprT,Fepr)

      RETURN
      END SUBROUTINE CauchyElasPred


      SUBROUTINE DevTrace (Eepr, TrEe, devEe, Tro3)
      IMPLICIT NONE

      real*8 Eepr(3,3)
      real*8 TrEe
      real*8 DevEe(3,3)
      real*8 Tro3
      TrEe= Eepr(1,1) + Eepr(2,2) + Eepr(3,3)
      
      Tro3=TrEe/3.
      devEe=Eepr
      devEe(1,1)=devEe(1,1)-Tro3
      devEe(2,2)=devEe(2,2)-Tro3
      devEe(3,3)=devEe(3,3)-Tro3      
      END SUBROUTINE DevTrace
 

      SUBROUTINE kcorcomp (devEe, TrEe, smuT, KT, kcorDevPr, ppr)
      IMPLICIT NONE
      real*8 devEe(3,3)
      real*8 kcorDevPr(3,3)
      real*8 smuT, KT, TrEe, ppr


      kcorDevPr = devEe *2. * smuT 
      ppr = KT * TrEe

      END SUBROUTINE kcorcomp


      SUBROUTINE corKirchhoffStress(kcorDev, ppr, kCor)
      IMPLICIT NONE
      real*8 kcorDev(3,3) 
      real*8 kCor(3,3)
      real*8 ppr 


      kCor= kcorDev
      kCor(1,1)= kCor(1,1) + ppr
      kCor(2,2)= kCor(2,2) + ppr
      kCor(3,3)= kCor(3,3) + ppr
      
      END SUBROUTINE corKirchhoffStress

      SUBROUTINE KirchhoffStress(ppr, Fe, Cepr, kcordev, kirch)
      IMPLICIT NONE
      real*8 kcorDev(3,3), Fe(3,3), Cepr(3,3) ,CeprINV(3,3)
      real*8 kCor(3,3), mid(3,3), FeT(3,3), midl(3,3), kirch(3,3)
      real*8 ppr 

      CALL M33INV(Cepr,CeprINV)
      CALL M33T(Fe,FeT)
      mid = MATMUL(CeprINV,kcorDev)
      midl = MATMUL(Fe,mid)
      kirch = MATMUL(midl,FeT)
      
      kirch(1,1) = kirch(1,1) + ppr
      kirch(2,2) = kirch(2,2) + ppr
      kirch(3,3) = kirch(3,3) + ppr

      END SUBROUTINE KirchhoffStress


      SUBROUTINE StrainTensP (Cepr, Eepr)
      IMPLICIT NONE
      INTEGER LDVL, LDVR, LWORK, INFO
      CHARACTER*1 JOBV 
      real*8 WORK(12,3)

      real*8 Cepr(3,3) 
      real*8 WR(1,3), WI(1,3), logWR(1,3), logWI(1,3)
      real*8 VL(3,3)
      real*8 VR(3,3)
      real*8 Eepr(3,3)
      real*8 loga(3,3)  
      INTEGER :: i,j

 
      call calclogM(Cepr,loga)
    
      Eepr= 0.5* loga

      RETURN
	END SUBROUTINE StrainTensP



      SUBROUTINE Expon(gammaNP,ExpM)
      IMPLICIT NONE
      INTEGER LDVL, LDVR, LWORK, INFO
      CHARACTER*1 JOBV 
      real*8 WORK(12,3)

      real*8 gammaNP(3,3) 
      real*8 WR(1,3), WI(1,3), ExpWR(1,3), expWI(1,3)
      real*8 VL(3,3)
      real*8 VR(3,3)
      real*8 ExpM(3,3) 
      INTEGER :: i,j

      call calcExpM(gammaNP,ExpM)

      RETURN
	END SUBROUTINE Expon
      


      subroutine calclogM(M,logM)

        implicit none

        double precision,dimension(3,3)::M,logM,VL,VR,logMapo,VRinv
        integer::n,INFO,LWORK,I,J

       double precision,dimension(3)::WR,WI,logWR,ipiv
       double precision,dimension(24)::WORK

       n=3
       LWORK=24


        call DGEEV( 'N', 'V', n, M, n, WR, WI, VL, n, VR,
     1                  n, WORK, LWORK, INFO )

C Check if all eigenvalues are greater than zero

      if (WR(1) .le. 0.D0) then
       write(*,*) 'Unable to compute matrix logarithm!'
      GOTO 111
      end if

       if (WR(2) .le. 0.D0) then
         write(*,*) 'Unable to compute matrix logarithm!'
         GOTO 111
       end if

        if (WR(3) .le. 0.D0) then
          write(*,*) 'Unable to compute matrix logarithm!'
          GOTO 111
        end if

       DO I = 1, 3
         DO J = 1, 3
             logMapo(I,J) = 0.D0
         END DO
        END DO

C Then Mapo will be a diagonal matrix whose diagonal elements 
C are eigenvalues of M. Replace each diagonal element of Mapo by its 
C (natural) logarithm in order to obtain logMapo.

       DO I = 1, 3
         LogMapo(I,I)=log(WR(I))
       END DO


C Calculate inverse of V with LU Factorisation
C Copy VR to VRinv
        DO I = 1, 3
         DO J = 1, 3
             VRinv(I,J) = VR(I,J)
         END DO
       END DO

        call dgetrf( n, n, VRinv, n, ipiv, info )
c       write(*,*) 'INFO',INFO

        call dgetri( n, VRinv, n, ipiv, WORK, LWORK, INFO )
c        write(*,*) 'INFO',INFO

C Build the logM Matrix
        logM = matmul(matmul(VR,logMapo),VRinv)      

      
111    END SUBROUTINE calclogM


      SUBROUTINE calcExpM(M,ExpM)
        implicit none

        double precision,dimension(3,3)::M,ExpM,VL,VR,ExpMapo,VRinv
        integer::n,INFO,LWORK,I,J

       double precision,dimension(3)::WR,WI,ExpWR,ipiv
       double precision,dimension(24)::WORK

       n=3
       LWORK=24


        call DGEEV( 'N', 'V', n, M, n, WR, WI, VL, n, VR,
     1                  n, WORK, LWORK, INFO )

C Check if all eigenvalues are greater than zero

c     if (WR(1) .le. 0.D0) then
c       write(*,*) 'Unable to compute matrix exponential!'
c      GOTO 111
c     end if

c       if (WR(2) .le. 0.D0) then
c         write(*,*) 'Unable to compute matrix exponential!'
c         GOTO 111
c       end if

c        if (WR(3) .le. 0.D0) then
c          write(*,*) 'Unable to compute matrix exponential!'
c          GOTO 111
c        end if

        DO I = 1, 3
          DO J = 1, 3
              ExpMapo(I,J) = 0.D0
          END DO
        END DO

C Then Mapo will be a diagonal matrix whose diagonal elements 
C are eigenvalues of M. Replace each diagonal element of Mapo by its 
C (natural) exponential in order to obtain ExpMapo.

       DO I = 1, 3
         ExpMapo(I,I)=exp(WR(I))
       END DO

C Calculate inverse of V with LU Factorisation
C Copy VR to VRinv
        DO I = 1, 3
         DO J = 1, 3
             VRinv(I,J) = VR(I,J)
         END DO
       END DO

        call dgetrf( n, n, VRinv, n, ipiv, info )
c       write(*,*) 'INFO',INFO

        call dgetri( n, VRinv, n, ipiv, WORK, LWORK, INFO )
c        write(*,*) 'INFO',INFO

C Build the Exponentiel Matrix
        ExpM = matmul(matmul(VR,ExpMapo),VRinv)      

111    END SUBROUTINE calcExpM


      SUBROUTINE M33DET (A, Jacob)
      IMPLICIT NONE
      INTEGER I,J
      real*8 A(3,3)

      real*8 Jacob


      Jacob =   A(1,1)*A(2,2)*A(3,3) 
     &       - A(1,1)*A(2,3)*A(3,2)  
     &       - A(1,2)*A(2,1)*A(3,3)  
     &       + A(1,2)*A(2,3)*A(3,1)  
     &       + A(1,3)*A(2,1)*A(3,2)  
     &       - A(1,3)*A(2,2)*A(3,1)

C      RETURN

      END SUBROUTINE M33DET

c Where we start 


	SUBROUTINE expo_smooth(x_b, x_ext, x, x_hat)
	real*8 x_b, x_ext, x, x_hat

	x_hat = x_b+(x_ext-x_b)*(1.-exp((x-x_ext)/(x_ext-x_b)))

	END SUBROUTINE expo_smooth



      SUBROUTINE kcor_eqpr(A,B)
      IMPLICIT NONE
      real*8 A(3,3)
      real*8 B
      real*8 ddotproduct

c The squares come from the fact that T_predictor_equivalent is symmetric

      ddotproduct = A(1,1)**2 + A(1,2)**2 + A(1,3)**2
     &             + A(2,1)**2 + A(2,2)**2 + A(2,3)**2
     &             + A(3,1)**2 + A(3,2)**2 + A(3,3)**2

      B = sqrt((3./2.)*ddotproduct)
	
	END SUBROUTINE kcor_eqpr


      SUBROUTINE kcor_eq(kcorEqPr,smuT,incrD, kcorEq)
      IMPLICIT NONE 
      real*8 kcorEqPr
      real*8 smuT
      real*8 incrD 
      real*8 kcorEq

      kcorEq = kcorEqPr-3.*smuT*incrD

      END SUBROUTINE kcor_eq


      SUBROUTINE yield_str(R,k_y)
      IMPLICIT NONE 
      real*8 R
      real*8 k_y
		      
      k_y = R
	
      END SUBROUTINE yield_str 	


      SUBROUTINE corr_pressure(KT,incrQ,ppr,p_corr)
      IMPLICIT NONE 
      real*8 KT
	real*8 incrQ
	real*8 ppr
	real*8 p_corr

	p_corr = ppr-KT*incrQ

      END SUBROUTINE corr_pressure


      SUBROUTINE term_1(kcorEq,k_y,term1)
      IMPLICIT NONE 
      real*8 kcorEq
	real*8 k_y
	real*8 term1

	term1 = (kcorEq/k_y)**2.

      END SUBROUTINE term_1


	SUBROUTINE term_2(fv_hat,q1,q2,p_corr,k_y,term2)
      IMPLICIT NONE 
	real*8 fv_hat 
	real*8 q1,q2
	real*8 p_corr
	real*8 k_y
	real*8 term2

	term2 = 2.*fv_hat*q1*cosh((3.*q2*p_corr)/(2.*k_y))

      END SUBROUTINE term_2
	

	SUBROUTINE term_3(q3,fv_hat,term3)
      IMPLICIT NONE 
	real*8 q3
	real*8 fv_hat
	real*8 term3

	term3 = (q3**2.)*(fv_hat**2.)	

      END SUBROUTINE term_3


      SUBROUTINE f_G(term1,term2,term3,fG)
      IMPLICIT NONE
      real*8 term1
      real*8 term2
      real*8 term3
      real*8 fG
      

      fG =  term1 + term2 - term3 - 1.0 

      END SUBROUTINE f_G


	SUBROUTINE norm3(x,normX)
	IMPLICIT NONE
	real*8 x(3)
	real*8 normX

	normX = sqrt(x(1)**2. + x(2)**2. + x(3)**2.)

	END SUBROUTINE norm3


	SUBROUTINE normalD(kcorEq,k_y,NdG)
	IMPLICIT NONE
	real*8 kcorEq, k_y, NdG
	
	NdG = 2.*kcorEq/(k_y**2.)

	END SUBROUTINE normalD


	SUBROUTINE derfGderfv(q1,q2,q3,p_corr,k_y,fv,dfGdfv_hat)
	IMPLICIT NONE
	real*8 q1,q2,q3,p_corr,k_y,fv,dfGdfv_hat
	
	dfGdfv_hat = 2.*q1*COSH(3.*q2*p_corr/(2.*k_y)) - 2.*fv*(q3**2.)

	END SUBROUTINE derfGderfv


	SUBROUTINE lode(A,kcorEqPr,zeta)
	IMPLICIT NONE
	real*8 A(3,3),kcorEqPr,zeta,DET
	
	      DET =   A(1,1)*A(2,2)*A(3,3)  
     &       - A(1,1)*A(2,3)*A(3,2)  
     &       - A(1,2)*A(2,1)*A(3,3)  
     &       + A(1,2)*A(2,3)*A(3,1)  
     &       + A(1,3)*A(2,1)*A(3,2)  
     &       - A(1,3)*A(2,2)*A(3,1)

	zeta = 27.*DET/(2.*(kcorEqPr**3.))
	
	END SUBROUTINE lode


	SUBROUTINE GrowthContribution(kw,zeta,Bn)
	IMPLICIT NONE
	real*8 kw,zeta,Bn

	Bn = kw*(1.-zeta**2.)

	END SUBROUTINE GrowthContribution


	SUBROUTINE derfvhatderd(Bn,incrQ,incrD,fv_hat,dfvdD)
   	IMPLICIT NONE
	real*8 Bn,incrQ,incrD,fv_hat,dfvdD
	
	dfvdD = (Bn*fv_hat)/(1.+incrQ-(Bn*incrD))

	END SUBROUTINE derfvhatderd

	SUBROUTINE Jacobian11(smuT,NdG,dfGdfv_hat,dfvhatdfv,dfvdD,J11)
	IMPLICIT NONE
	real*8 smuT,NdG,dfGdfv_hat,dfvdD,dfvhatdfv,J11

	J11 = -3.*smuT*NdG + dfGdfv_hat*dfvhatdfv*dfvdD

	END SUBROUTINE Jacobian11


	SUBROUTINE normalQ(q1,q2,fv_hat,k_y,p_corr,NqG)
	real*8 q1,q2,fv_hat,k_y,p_corr,NqG

	NqG = (3.*q1*q2*fv_hat/k_y)*SINH((3.*q2*p_corr)/(2.*k_y))	

	END SUBROUTINE normalQ 


	SUBROUTINE derfvhatderq(Bn,incrQ,incrD,fv_hat,dfvdQ)
	real*8 Bn,incrQ,incrD,fv_hat,dfvdQ

	dfvdQ = (1.-fv_hat)/(1.+incrQ-Bn*incrD)

	END SUBROUTINE derfvhatderq


	SUBROUTINE Jacobian12(KT,NqG,dfGdfv_hat,dfvhatdfv,dfvdQ,J12)
	IMPLICIT NONE
	real*8 KT,NqG,dfGdfv_hat,dfvhatdfv,dfvdQ,J12

	J12 = -KT*NqG + dfGdfv_hat*dfvhatdfv*dfvdQ

	END SUBROUTINE Jacobian12


	SUBROUTINE derfGderky(kcorEq,k_y,q1,q2,p_corr,fv_hat,dfGdky,fac)
	IMPLICIT NONE
	real*8 kcorEq,k_y,q1,q2,p_corr,fv_hat,dfGdky,fac

	fac = (SINH(3.*q2*p_corr/(2.*k_y)))/(k_y**2.)
	dfGdky = -2.*((kcorEq**2.)/(k_y**3.))-3.*q1*q2*p_corr*fv_hat*fac

	END SUBROUTINE derfGderky


	SUBROUTINE derfvhatderp(An,dAn,Bn,incrP,incrQ,incrD,dfvdP)
	IMPLICIT NONE
	real*8 An,dAn,Bn,incrP,incrQ,incrD,dfvdP

	dfvdP = (An+dAn*incrP)/(1.+incrQ-Bn*incrD)

	END SUBROUTINE derfvhatderp      


	SUBROUTINE Jacobian13(dfGdky,h,dfGdfv_hat,dfvhatdfv,dfvdP,J13)
	IMPLICIT NONE
	real*8 dfGdky,h,dfGdfv_hat,dfvhatdfv,dfvdP,J13

	J13 = dfGdky*h + dfGdfv_hat*dfvhatdfv*dfvdP

	END SUBROUTINE Jacobian13


	SUBROUTINE derNqderfvhat(k_y,q1,q2,p_corr,dNqdfv_hat)
	IMPLICIT NONE
	real*8 k_y,q1,q2,p_corr,fv,dNqdfv_hat
 
	dNqdfv_hat = (3.*q1*q2/k_y)*SINH(3.*q2*p_corr/(2.*k_y))

	END SUBROUTINE derNqderfvhat


	SUBROUTINE derNdderkEq(k_y,dNddkEq)
	IMPLICIT NONE	
	real*8 k_y,dNddkEq

	dNddkEq = 2./(k_y**2.)

	END SUBROUTINE derNdderkEq


	SUBROUTINE Jacobian21(smuT,NqG,incrD,incrQ,dNqdfv_hat,dfvdD,
     &                     dNddkEq,dfvhatdfv,J21)
	IMPLICIT NONE
	real*8 smuT,NqG,incrD,incrQ,dNqdfv_hat,dfvdD,dfvhatdfv,dNddkEq
	real*8 J21

	J21=NqG+incrD*dNqdfv_hat*dfvhatdfv*dfvdD+incrQ*(3.*smuT*dNddkEq)

	END SUBROUTINE Jacobian21


	SUBROUTINE derNqderp(q1,q2,fv_hat,k_y,p_corr,dNqdp)
	IMPLICIT NONE
	real*8 q1,q2,fv_hat,k_y,p_corr,dNqdp

	dNqdp=9.*q1*q2**2.*fv_hat*COSH(3.*q2*p_corr/(2.*k_y))
     &      /(2.*(k_y**2.))

	END SUBROUTINE derNqderp



	SUBROUTINE Jacobian22(KT,NdG,incrD,incrQ,dfvdQ,dNqdfv_hat,dNqdp,
     &                      dfvhatdfv,J22)
	IMPLICIT NONE
	real*8 KT,NdG,incrD,incrQ,dfvdQ,dNqdfv_hat,dfvhatdfv,dNqdp,J22

	J22=incrD*(dNqdfv_hat*dfvhatdfv*dfvdQ - KT*dNqdp) - NdG

	END SUBROUTINE Jacobian22


	SUBROUTINE derNqderky(q1,q2,fv_hat,p_corr,k_y,t1,t2,dNqdky)
	IMPLICIT NONE
	real*8 q1,q2,fv_hat,p_corr,k_y,t1,t2,dNqdky
	t1 = q1*q2**2.*fv_hat*p_corr*COSH(3.*q2*p_corr/(2.*k_y))
	t2 = ((3.*q1*q2*fv_hat)/(k_y**2.))*SINH(3.*q2*p_corr/(2.*k_y))
	dNqdky = (-9./(2.*k_y**3.))*t1-t2

	END SUBROUTINE derNqderky


	SUBROUTINE derNdderky(kcorEq,k_y,dNddky)
	IMPLICIT NONE
	real*8 kcorEq,k_y, dNddky

	dNddky = -4.*kcorEq/k_y**3.

	END SUBROUTINE derNdderky


	SUBROUTINE Jacobian23(incrD,incrQ,h,dNqdfv_hat,dfvdP,dNqdky,
     &          dNddky,dfvhatdfv,J23)
	IMPLICIT NONE
	real*8 incrD,incrQ,h,dNqdfv_hat,dNddfv,dfvdP,dNqdky,dNddky,J23
	real*8 dfvhatdfv

	J23=incrD*(dNqdfv_hat*dfvhatdfv*dfvdP+dNqdky*h)-incrQ*dNddky*h

	END SUBROUTINE Jacobian23


	SUBROUTINE Jacobian31(kcorEq,smuT,incrD,J31)
	IMPLICIT NONE
	real*8 kcorEq,smuT,incrD,J31

	J31 = -kcorEq + 3.*smuT*incrD

	END SUBROUTINE Jacobian31


	SUBROUTINE Jacobian32(p_corr,KT,incrQ,J32)
	IMPLICIT NONE
	real*8 p_corr,KT,incrQ,J32

	J32 = -p_corr + KT*incrQ

	END SUBROUTINE Jacobian32
	

	SUBROUTINE Jacobian33(fV0,k_y,h,incrP,J33)
	IMPLICIT NONE
	real*8 fV0,k_y,h,incrP,J33

	J33 = (1. - fV0)*(k_y + h*incrP)

	END SUBROUTINE Jacobian33


	SUBROUTINE MULMATVEC(A,x,b)
	IMPLICIT NONE

	real*8 A(3,3)
	real*8 x(3),b(3)

	b(1) = A(1,1)*x(1)+A(1,2)*x(2)+A(1,3)*x(3)
	b(2) = A(2,1)*x(1)+A(2,2)*x(2)+A(2,3)*x(3)
	b(3) = A(3,1)*x(1)+A(3,2)*x(2)+A(3,3)*x(3)
	
	END SUBROUTINE MULMATVEC


	SUBROUTINE kcor_dev(smuT,incrD,kcorEqPr,kcorDevPr, kcorDev)
	IMPLICIT NONE

	INTEGER I,J	
	real*8 smuT,incrD,kcorEqPr
	real*8 kcorDevPr(3,3),kcorDev(3,3)

	Do I=1,3
		Do J=1,3
			kcorDev(I,J) = ((kcorEqPr-3.*smuT*incrD)/kcorEqPr)*
     &                            kcorDevPr(I,J)
		End do
	End do

	END SUBROUTINE kcor_dev


	SUBROUTINE MATUNI(Mat)
	IMPLICIT NONE

	real*8 Mat(3,3)
	
	Mat(1,1) = 1.0	
	Mat(1,2) = 0.0
	Mat(1,3) = 0.0
	Mat(2,1) = 0.0
	Mat(2,2) = 1.0
	Mat(2,3) = 0.0
	Mat(3,1) = 0.0
	Mat(3,2) = 0.0
	Mat(3,3) = 1.0

	END SUBROUTINE MATUNI


	SUBROUTINE DeltagammaNp(kcorDev,incrD,incrQ,kcorEq,Mat, gammaNp)
	IMPLICIT NONE

	INTEGER I,J	
	real*8 kcorDev(3,3),Mat(3,3),gammaNp(3,3)
	real*8 incrD,incrQ,kcorEq 
	
	Do I=1,3
		Do J=1,3
	gammaNp(I,J)=((3.*incrD/(2.*kcorEq))*kcorDev(I,J))
     &             +Mat(I,J)*incrQ/3.
		End do	
	End do

	END SUBROUTINE DeltagammaNp

	
	SUBROUTINE h_Eq25(dR,h)
	IMPLICIT NONE
	real*8 dR,h

	h = dR

	END SUBROUTINE h_Eq25
	

	SUBROUTINE incr_fv(fvn,incrQ,An,incrP,Bn,incrD,incrfv)
	IMPLICIT NONE
	real*8 fvn,incrQ,An,incrP,Bn,incrD,incrfv

	incrfv=((1.-fvn)*incrQ+An*incrP+Bn*fvn*incrD)/(1.+incrQ-Bn*incrD)

	END SUBROUTINE incr_fv


	SUBROUTINE gamma_np_el(gammaNp)
	IMPLICIT NONE
	integer I,J	
	real*8 gammaNp(3,3)

	DO I = 1,3
		DO J = 1,3	
			If (I .eq. J) then
      			gammaNp(I,J) = 1.0
			else
				gammaNp(I,J) = 0.0
			End if 
		END DO
      END DO 

	END SUBROUTINE gamma_np_el


	SUBROUTINE ligam(fvn,lambda0,kappa,hatpn,Khin)
	IMPLICIT NONE
	real*8 fvn,lambda0,kappa,hatpn,Khin

	Khin = ((3./2.)*fvn*lambda0)**(1./3.) * EXP((1./3.)*kappa*hatpn)

	END SUBROUTINE ligam


	SUBROUTINE pro(h_exp,alpha,beta)
	IMPLICIT NONE
	real*8 h_exp,alpha,beta

	alpha = 0.1 + 0.217*h_exp + 4.83*(h_exp**2.)
	
	If (h_exp .gt. 0.0 .and. h_exp .lt. 0.3) Then
		beta = 1.24
	Else 
		write(*,*) 'Change the value of h_exp!'		
	End if

	END SUBROUTINE pro

	
	SUBROUTINE C_t(khi_hat,alpha,beta,Ct)
	IMPLICIT NONE 
	real*8 khi_hat,alpha,beta,Ct

	Ct = (1.-khi_hat**2.)*(alpha*((1./khi_hat)- 1.)**2.
     &     +beta*SQRT(1./khi_hat))

	END SUBROUTINE C_t


	SUBROUTINE f_T(kcorEq,p_corr,n,Ct,k_y,fT)
	IMPLICIT NONE
	real*8 kcorEq,p_corr,n,Ct,k_y,fT

	fT = (((2.*kcorEq/3.)+p_corr)**n 
     &        + ((2.*kcorEq/3.)-p_corr)**n)**(1./n) - Ct*k_y

	END SUBROUTINE f_T
	

	SUBROUTINE tau2(kcorEq,p_corr,ptau,ntau)
	IMPLICIT NONE
	real*8 kcorEq,p_corr,ptau,ntau

	ptau = (2.*kcorEq/3.)+p_corr
	ntau = (2.*kcorEq/3.)-p_corr

	END SUBROUTINE tau2


	SUBROUTINE NormaldT(n,ptau,ntau,NdT)
	IMPLICIT NONE 
	real*8 n,ptau,ntau,NdT

	NdT = (2./3.)*(ptau**n + ntau**n)**((1.-n)/n)
     &     *(ptau**(n-1.) + ntau**(n-1.))

	END SUBROUTINE NormaldT


	SUBROUTINE derftdki_hat(k_y,khi,alpha,beta,dftdkhi_hat)
	IMPLICIT NONE 

	real*8 k_y,khi,alpha,beta,dftdkhi_hat

	dftdkhi_hat=k_y*2.*khi*(alpha*((1./khi)- 1.)**2.
     &       +beta*SQRT(1./khi))+k_y*(1.-khi**2.)
     &       *(2.*alpha*((1./khi)- 1.)*(1./khi**2.)
     &       +(1./2.)*beta*(khi**(-3./2.)))


	END SUBROUTINE derftdki_hat


	SUBROUTINE derkhiderfv(Khin,fvn,incrfv,kappa,incrP,dkhidfv)
	IMPLICIT NONE 
	real*8 Khin,fvn,incrfv,kappa,incrP,dkhidfv

	dkhidfv=Khin*(1./(3.*fvn))*((fvn+incrfv)/fvn)**(-2./3.)
     &        *EXP(kappa*incrP/3.)

	END SUBROUTINE derkhiderfv


	SUBROUTINE J11T(smuT,NdT,dftdkhi_hat,dkhihatdkhi,dkhidfv,
     &               dfvhatdfv,dfvdD,J11)
	IMPLICIT NONE
	real*8 smuT,NdT,dftdkhi_hat,dkhidfv,dfvdD,J11
	real*8 dkhihatdkhi,dfvhatdfv

	J11 = -3.*smuT*NdT
     &      +(dftdkhi_hat*dkhihatdkhi*dkhidfv*dfvhatdfv)*dfvdD

	END SUBROUTINE J11T


	SUBROUTINE NormalqT(n,ptau,ntau,NqT)
	IMPLICIT NONE 
	real*8 n,ptau,ntau,NqT

	NqT = (ptau**n + ntau**n)**((1.-n)/n)
     &     *(ptau**(n-1.) - ntau**(n-1.))

	END SUBROUTINE

	
	SUBROUTINE J12T(KT,NqT,dftdkhi_hat,dkhidfv,dkhihatdkhi,dfvhatdfv,
     &               dfvdQ,J12)
	IMPLICIT NONE
	real*8 KT,NqT,dftdkhi_hat,dkhidfv,dfvdQ,J12
	real*8 dkhihatdkhi,dfvhatdfv

	J12 = -KT*NqT + (dftdkhi_hat*dkhihatdkhi*dkhidfv*dfvhatdfv)*dfvdQ
	
	END SUBROUTINE J12T


	SUBROUTINE derkhidp(fvn,incrfv,kappa,incrP,Khin,dkhidP)
	real*8 fvn,incrfv,kappa,incrP,Khin,dkhidP

        dkhidP = Khin*(((fvn+incrfv)/fvn)**(1./3.))*(1./3.)
     &         *kappa*EXP(kappa*incrP/3.)
	END SUBROUTINE derkhidp 



	SUBROUTINE J13T(dfTdky,h,dftdkhi_hat,dkhidfv,dfvdP,dkhidP,
     &              dkhihatdkhi,dfvhatdfv,J13)
	IMPLICIT NONE
	real*8 dfTdky,h,dftdkhi_hat,dkhidfv,dfvdP,dkhidP,J13
	real*8 dkhihatdkhi,dfvhatdfv

	J13 = dfTdky*h + (dftdkhi_hat*dkhihatdkhi*dkhidfv*dfvhatdfv)
     &     *dfvdP + dftdkhi_hat*dkhihatdkhi*dkhidP 

	END SUBROUTINE J13T


	SUBROUTINE derNqderkeq(n,ptau,ntau,dNqdkEq)
	IMPLICIT NONE 
	real*8 n,ptau,ntau,dNqdkEq,t1,t2
	
	t1=((ptau**n+ptau**n)**((1./n)- 1.)
     &    *(ntau**(n-2.)-ntau**(n-2.)))
	t2=((ptau**n+ntau**n)**((1./n)- 2.)
     &    *(ptau**((2.*n)- 2.)-ntau**((2.*n)- 2.)))
	dNqdkEq = (2.*(n-1.)/3.)*(t1-t2)

	END SUBROUTINE derNqderkeq


	SUBROUTINE derNddTerkeq(n,ptau,ntau,dNddkEqT)
	IMPLICIT NONE 
	real*8 n,ptau,ntau,dNddkEqT,t1,t2
	
	t1=((ptau**n+ntau**n)**((1./n)- 1.)
     &   *(ptau**(n-2.)+ntau**(n-2.)))
	t2=((ptau**n+ntau**n)**((1./n)- 2.)
     &   *(ptau**(n-1.)+ntau**(n-1.))**2.)
	dNddkEqT = (4.*(n-1.)/9.)*(t1-t2)

	END SUBROUTINE derNddTerkeq

	
	SUBROUTINE J21T(NqT,incrD,dNqdkEq,smuT,incrQ,dNddkEqT,J21)
	real*8 NqT,incrD,dNqdkEq,smuT,incrQ,dNddkEqT,J21

	J21 = NqT-3.*smuT*incrD*dNqdkEq+3.*incrQ*smuT*dNddkEqT

	END SUBROUTINE 


	SUBROUTINE derNqderpcorr(n,ptau,ntau,dNqdpcorr)
	IMPLICIT NONE
	real*8 n,ptau,ntau,dNqdpcorr,t1,t2

	t1=((ptau**n+ntau**n)**((1./n)- 1.)
     &   *(ptau**(n-2.)+ntau**(n-2.)))
	t2=((ptau**n+ntau**n)**((1./n)- 2.)
     &   *(ptau**(n-1.)-ntau**(n-1.))**2.)
	dNqdpcorr = (n-1.)*(t1-t2)

	END SUBROUTINE derNqderpcorr


	SUBROUTINE derNdderpcorr(n,ptau,ntau,dNddpcorr)
	IMPLICIT NONE
	real*8 n,ptau,ntau,dNddpcorr,t1,t2

	t1=((ptau**n+ntau**n)**((1./n)- 1.)
     &   *(ptau**(n-2.)-ntau**(n-2.)))
	t2=((ptau**n+ntau**n)**((1./n)- 2.)
     &   *(ptau**((2.*n)- 2.)-ntau**((2.*n)- 2.)))
	dNddpcorr = (2.*(n-1.)/3.)*(t1-t2)

	END SUBROUTINE


	SUBROUTINE J22T(incrD,KT,dNqdpcorr,NdT,incrQ,dNddpcorr,J22)
	IMPLICIT NONE
	real*8 incrD,KT,dNqdpcorr,NdT,incrQ,dNddpcorr,J22

	J22 = -incrD*KT*dNqdpcorr - NdT + incrQ*KT*dNddpcorr

	END SUBROUTINE

	
	SUBROUTINE ligam2(fvn,incrfv,kappa,incrP,khin,khi)
	IMPLICIT NONE
	real*8 fvn,incrfv,kappa,incrP,khin,khi

	khi = khin*(((fvn+incrfv)/fvn)**(1./3.))*EXP(kappa*incrP/3.)

	END SUBROUTINE ligam2


	SUBROUTINE Nucleation(A0,An,dAn)
	IMPLICIT NONE
	real*8 A0,An,dAn

	An = A0
	dAn = 0.0

	END SUBROUTINE Nucleation

	SUBROUTINE derfvhatderfv(x_b, x_ext, x, derx_hat)
	IMPLICIT NONE
	real*8 x_b, x_ext, x, derx_hat

	derx_hat = - EXP((x-x_ext)/(x_ext-x_b))

	END SUBROUTINE derfvhatderfv


	SUBROUTINE derkhihatderkhi(x_b, x_ext, x, derx_hat)
	IMPLICIT NONE
	real*8 x_b, x_ext, x, derx_hat

	derx_hat = - EXP((x-x_ext)/(x_ext-x_b))

	END SUBROUTINE derkhihatderkhi

	return	
      end


