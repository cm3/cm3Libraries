//
//
//
// Description: Define gurson damake nucleation
//
//
// Author:  <L. Noels>, (C) 2013
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "NucleationLaw.h"
#include "ipNonLocalPorosity.h"





// NucleationLaw
// -------------------------------------------
NucleationLaw::NucleationLaw( const int num ) :
    num_(num),
    isInitialized_(false),
    nNucleationLaw_(0)
{
  // Nothing more than initialising the member variables to do.
  // Vectors are initialised as empty and 
  // pointers are set later when they are added in the material law
  nuclFunctionVector_.clear();
  nuclCriterionVector_.clear();
};


NucleationLaw::NucleationLaw( const NucleationLaw& source ) :
  num_(source.num_),
  isInitialized_(source.isInitialized_),
  nNucleationLaw_(source.nNucleationLaw_)
{
  // Once classical variables are created, the containers, first empty,
  // are resized and then progressively filled.
  nuclFunctionVector_.clear();
  nuclFunctionVector_.resize( source.nuclFunctionVector_.size() );
  for ( int i = 0; i < source.nuclFunctionVector_.size(); i++ ) {
    if ( source.nuclFunctionVector_[i] != NULL ){
      nuclFunctionVector_[i] = source.nuclFunctionVector_[i]->clone();
    };
  };
  
  nuclCriterionVector_.clear();
  nuclCriterionVector_.resize( source.nuclCriterionVector_.size() );
  for ( int i = 0; i < source.nuclCriterionVector_.size(); i++ ) {
    if ( source.nuclCriterionVector_[i] != NULL ){
      nuclCriterionVector_[i] = source.nuclCriterionVector_[i]->clone();
    };
  };
  
  /// Simple check before finishing the function
  checkNumberOfNucleationLaw();
  
};


NucleationLaw& NucleationLaw::operator=( const NucleationLaw &source )
{
  // Copy first the variables, 
  // then, cleaning the old vectors elements and clone the new ones
  num_ = source.num_;
  isInitialized_ = source.isInitialized_;
  nNucleationLaw_ = source.nNucleationLaw_;
  
  /// For nuclFunctionVector
  for (int i = 0; i < nuclFunctionVector_.size(); i++ ) {
    if ( nuclFunctionVector_[i] != NULL ) {
      delete nuclFunctionVector_[i];
      nuclFunctionVector_[i] = NULL;
    };
  };
  
  nuclFunctionVector_.resize(source.nuclFunctionVector_.size());
  for (int i = 0; i < source.nuclFunctionVector_.size(); i++ ) {
    if (source.nuclFunctionVector_[i] != NULL){
      nuclFunctionVector_[i] = source.nuclFunctionVector_[i]->clone();
    };
  };
  
  
  /// For nuclCriterionVector_
  for (int i = 0; i < nuclCriterionVector_.size(); i++ ) {
    if ( nuclCriterionVector_[i] != NULL ) {
      delete nuclCriterionVector_[i];
      nuclCriterionVector_[i] = NULL;
    };
  };
  
  nuclCriterionVector_.resize(source.nuclCriterionVector_.size());
  for (int i = 0; i < source.nuclCriterionVector_.size(); i++ ) {
    if (source.nuclCriterionVector_[i] != NULL){
      nuclCriterionVector_[i] = source.nuclCriterionVector_[i]->clone();
    };
  };
  
  /// Simple check before finishing the function
  checkNumberOfNucleationLaw();
  
  return *this;
};


NucleationLaw::~NucleationLaw()
{
  // Cleaning the storage vectors of pointed objects before completing deletion.
  for (int i = 0; i < nuclFunctionVector_.size(); i++ ) {
    if ( nuclFunctionVector_[i] != NULL ) {
      delete nuclFunctionVector_[i];
      nuclFunctionVector_[i] = NULL;
    };
  };
  
  
  for (int i = 0; i < nuclCriterionVector_.size(); i++ ) {
    if ( nuclCriterionVector_[i] != NULL ) {
      delete nuclCriterionVector_[i];
      nuclCriterionVector_[i] = NULL;
    };
  };
};



void NucleationLaw::createIPVariable( IPNucleation* &ipNucl ) const
{
  if (ipNucl != NULL)   delete ipNucl;
  ipNucl = new IPNucleation( this );
};


void NucleationLaw::clearAllNucleationComponents()
{
  // Set the counter of nucleation components to zero
  // and cleaning all the laws.
  
  nNucleationLaw_ = 0;
  
  for (int i = 0; i < nuclFunctionVector_.size(); i++ ) {
    if ( nuclFunctionVector_[i] != NULL ) {
      delete nuclFunctionVector_[i];
      nuclFunctionVector_[i] = NULL;
    };
  };
  nuclFunctionVector_.clear();
  
  
  for (int i = 0; i < nuclCriterionVector_.size(); i++ ) {
    if ( nuclCriterionVector_[i] != NULL ) {
      delete nuclCriterionVector_[i];
      nuclCriterionVector_[i] = NULL;
    };
  };
  nuclCriterionVector_.clear();
  
  /// Simple check before finishing the function
  checkNumberOfNucleationLaw();
  
};


void NucleationLaw::addOneNucleationComponent( const NucleationFunctionLaw& nuclFunction, const NucleationCriterionLaw * const nuclCriterion )
{
  // The function law is first simply added, then the criterion. 
  // If the criterion is not provided, a trivial one is added instead.
  // The counter of nucleaiton laws is also modified.
  
  /// -Increment the counter
  nNucleationLaw_++;
  
  /// - Get the index of the newly added law (i.e of the last elements)
  int indexOfNewlyAddedLaw = nNucleationLaw_ - 1;
  
  /// -Add the function
  nuclFunctionVector_.push_back( nuclFunction.clone() );
  
  /// -Add the criterion
  if ( nuclCriterion != NULL ) {
    nuclCriterionVector_.push_back( nuclCriterion->clone() );
  }
  else {
    nuclCriterionVector_.push_back( new TrivialNucleationCriterionLaw(indexOfNewlyAddedLaw) );
  }
  
  /// -Simple check before finishing the function
  checkNumberOfNucleationLaw();
};



void NucleationLaw::initNucleationLaw( const mlawNonLocalPorosity * const mlawPorous )
{ 
  // Check for inconstencies before setting the initialisaiton flag to true.
  // A special case is observed when the manager is empty. 
  // In this case, a default empty case is created.
  
  /// Data integrity is verified by the following flag.
  /// It is modified only when an error is detected.
  bool isAnError = false;

  /// If the manager is empty, a stupid case (i.e. trivial case) is added to avoid null pointer
  if (nNucleationLaw_ == 0){
    nuclFunctionVector_.clear();
    nuclCriterionVector_.clear();
    TrivialNucleationFunctionLaw nuclFunction = TrivialNucleationFunctionLaw(0);
    TrivialNucleationCriterionLaw nuclCriterion = TrivialNucleationCriterionLaw(0);
    this->addOneNucleationComponent( nuclFunction, &nuclCriterion );
  }
  

  /// Check for compatibilities between vectors.
  checkNumberOfNucleationLaw();
  
  /// Check if the mlawPorous really exist (avoid null pointer)
  if ( mlawPorous == NULL ) {
    Msg::Error( "NucleationLaw::initNucleationLaw: mlawPorous pointer is null." );
    isAnError = true;
  }
  
  /// Check if the internal laws are correctly set:
  /// Normally, each law should be initialised, have the same index as their number
  /// and the index of exclusion rules should be existing.
  for( int it_law = 0; it_law < nNucleationLaw_; it_law++ ){
    
    int numFuncLaw = nuclFunctionVector_[it_law]->getNum();
    if( it_law != numFuncLaw ) {
      Msg::Warning( "NucleationLaw::initNucleationLaw: "
                    "indexes differ between the function law %d and the manager %d -> "
                    "be sure of what you're doing.", it_law, numFuncLaw );
      nuclFunctionVector_[it_law]->setNum(it_law);
    }
    
    
    int numCritLaw = nuclCriterionVector_[it_law]->getNum();
    if( it_law != numCritLaw ) {
      Msg::Warning( "NucleationLaw::initNucleationLaw: "
                    "indexes differ between the criterion law %d and the manager %d -> "
                    "be sure of what you're doing.", it_law, numCritLaw );
      nuclCriterionVector_[it_law]->setNum(it_law);
    }
    
    
    /// Initialise the internal laws.
    nuclFunctionVector_[it_law]->initFunctionLaw( mlawPorous );
    nuclCriterionVector_[it_law]->initCriterionLaw( mlawPorous );
    
    
    /// Check if each law is well initialised
    if ( !nuclFunctionVector_[it_law]->isInitialized() ){
      isAnError = true;
      Msg::Error( "NucleationLaw::initNucleationLaw: nucleation function %d not initialised.", it_law );
      break;
    }
    
    if ( !nuclCriterionVector_[it_law]->isInitialized() ){
      isAnError = true;
      Msg::Error( "NucleationLaw::initNucleationLaw: nucleation criterion %d not initialised.", it_law );
      break;
    }
    
    /// Check if the exclusion rules of a given criterion are correctly set.
    const std::vector<int>& indexExclu = nuclCriterionVector_[it_law]->getIndexOfExclusionRules();
    for( int i = 0; i < indexExclu.size(); i++ ){
      if (indexExclu[i] < 0 ){
        isAnError = true;
        Msg::Error( "NucleationLaw::initNucleationLaw: "
                    "exclusion rule in criterion %d points towards a non-existing criterion %d ", it_law, i );
      }
      else if ( indexExclu[i] >= nNucleationLaw_ ){
        isAnError = true;
        Msg::Error( "NucleationLaw::initNucleationLaw: "
                    "exclusion rule in criterion %d points towards a non-existing criterion %d ", it_law, i );
      }
      else if ( nuclCriterionVector_[ indexExclu[i] ]->getType() == NucleationCriterionLaw::trivialCriterion ){
        isAnError = true;
        Msg::Error( "NucleationLaw::initNucleationLaw: "
                    "exclusion rule in criterion %d points towards a trivial criterion %d ", it_law, i );
      }
    }
  }
  
  /// If no error has been detected, everything is fine. My job is done here.
  if (!isAnError){
    isInitialized_ = true;
  }
  else{
    /// This line should be never reached as Msg::Error stop everything before.
    isInitialized_ = false;
    Msg::Error( "NucleationLaw::initNucleationLaw: "
                "error have been detected during the initialisation" );
  }
};




void NucleationLaw::checkNumberOfNucleationLaw() const
{
  // Check if the vectors containers are correctly initialised.
  // The lenght of the vectors and nNucleationLaw_ should be all equal.
  
  /// Flag attesting the presence of an error
  bool isAnError = false;
  
  int nCriterion = nuclFunctionVector_.size();
  int nFunction = nuclCriterionVector_.size();
  if ( nCriterion  != nFunction ){
    isAnError = true;
  };
  
  if ( nCriterion  != nNucleationLaw_ ){
    isAnError = true;
  };
  
  /// Stop the program if an error is detected to avoid memory problems
  if ( isAnError ) {
    Msg::Error( "NucleationLaw:: the number of nucleation function and criteiron are not the same" );
  };
};


int NucleationLaw::getNumberOfNucleationComponents() const
{
  return nNucleationLaw_;
};



const NucleationFunctionLaw& NucleationLaw::getConstRefToNucleationFunctionLaw( const int i) const
{
  #ifdef _DEBUG
  if ( i >=  nNucleationLaw_ )           Msg::Error( "NucleationLaw:: index %d asked is larger than the vector size.", i );
  if ( nuclFunctionVector_[i] == NULL )  Msg::Error( "NucleationLaw:: nuclFunctionVector_[%d] is not defined.", i );
  #endif // _DEBUG
  return *nuclFunctionVector_[i];
};


const NucleationCriterionLaw& NucleationLaw::getConstRefToNucleationCriterionLaw( const int i) const
{
  #ifdef _DEBUG
  if ( i >=  nNucleationLaw_ )            Msg::Error( "NucleationLaw:: index %d asked is larger than the vector size.", i );
  if ( nuclCriterionVector_[i] == NULL )  Msg::Error( "NucleationLaw:: nuclCriterionVector_[%d] is not defined.", i );
  #endif // _DEBUG
  return *nuclCriterionVector_[i];
};




void NucleationLaw::nucleate( const double p, const double p0, IPNonLocalPorosity& q1Porous, const IPNonLocalPorosity& q0Porous ) const
{
  // Loop on each law and sum up the contributions of each components
  //! \fixme I need to store information differently, not inside ipvs
  
  double& fvNucl = q1Porous.getRefToIPNucleation().getRefToNucleatedPorosity();
  fvNucl = 0.0;
  double& dfV = q1Porous.getRefToIPNucleation().getRefToNucleatedPorosityIncrement();
  dfV = 0.0;
  double& ratefV = q1Porous.getRefToIPNucleation().getRefToDNucleatedPorosityDMatrixPlasticDeformation();
  ratefV = 0.0;
  
  for (int it_law = 0; it_law < nNucleationLaw_; it_law++ ){
    nuclFunctionVector_[it_law]->nucleate( p, p0, q1Porous, q0Porous );
    fvNucl += q1Porous.getRefToIPNucleation().getConstRefToIPNucleationFunction(it_law).getNucleatedPorosity();
    dfV += q1Porous.getRefToIPNucleation().getConstRefToIPNucleationFunction(it_law).getNucleatedPorosityIncrement();
    ratefV += q1Porous.getRefToIPNucleation().getConstRefToIPNucleationFunction(it_law).getNucleationRate();
  };
  
};






void NucleationLaw::checkNucleationActivation( IPNonLocalPorosity& q1Porous, const IPNonLocalPorosity& q0Porous ) const
{
  // Manage the nucleation activation by avoiding crossed nucleation inihibition 
  // (i.e. two simultaneous exclusive nucleation law activated at the same time)
  // To do so, each criterion data is first evaluated if needed by the underlying law.
  // If one or more criteria can be activated at this time step, this operation is made 
  // by applying the effects of the onset by decreasing order of the criterion value.
  //
  // NB: in future code versions, maybe this part should change...
  

  /// Asses each criterion and get the values of each criterion to compare and order them.
  /// A static vector can be used as the size needs to be initialised once 
  /// (and the size is already known and fixed once created).
  static std::vector<double> criterionValues( nNucleationLaw_ );
  #ifdef _DEBUG
  if ( criterionValues.size() !=  nNucleationLaw_ ) Msg::Error( "NucleationLaw::criterionValue size has changed !");
  #endif // _DEBUG
  
  for (int it_law = 0; it_law < nNucleationLaw_; it_law++ ){
    nuclCriterionVector_[it_law]->assessNucleationActivation( criterionValues[it_law], q1Porous, q0Porous );
  }
  
  /// Apply the onset effect on the IPs for each activated laws while there is remaining nucl. law to activate.
  while (true) {
    
    /// Search for the maximum value, greater than 0.0 (if there is any)
    double maxValue( 0.0 ); 
    int indexMax( 0 );
    for ( int it_law = 0 ; it_law < nNucleationLaw_ ; it_law++ ) {
      if ( criterionValues[it_law] > maxValue ) {
        maxValue = criterionValues[ it_law ];
        indexMax = it_law;
      }
    }
    
    /// Apply the onset effects of the criterion with the maximal value of its criterion.
    /// If there is no more maximum detected, go out of the loop.
    if ( maxValue > 0.0 ){
      nuclCriterionVector_[ indexMax ]->applyCriterionOnsetInIP( q1Porous );
      nuclFunctionVector_[ indexMax ]->applyFunctionOnsetInIP( q1Porous );
      criterionValues[ indexMax ] = -1.0;
    }
    else{
      /// No maximum is detected ( maxValue is still equal to zero)
      break; 
    }
  }
  
  
  /// Check if a nucleation law needs to be stopped.
  for (int it_law = 0; it_law < nNucleationLaw_; it_law++ ){
    const IPNucleationCriterion& q0i_criterion = q0Porous.getConstRefToIPNucleation().getConstRefToIPNucleationCriterion( it_law );
    IPNucleationCriterion& q1i_criterion = q1Porous.getRefToIPNucleation().getRefToIPNucleationCriterion( it_law );
    
    if ( q0i_criterion.hasNucleationActivated() ) {
      nuclCriterionVector_[ it_law ]->updateIfNucleationEnds( q1i_criterion, q1Porous );
    }
  }
  
  
};




LinearNucleationLaw::LinearNucleationLaw( const int num, const double A0, const double fmin, const double fmax ) : 
  NucleationLaw(num)
{
  // Class-function to preserve the old interface.
  // Practically the nucleation manager is first cleared, then fill accordingly with the new interface.
  Msg::Warning("LinearNucleationLaw:: This interface is deprecated and will be removed in the future.");
  
  this->clearAllNucleationComponents();
  LinearNucleationFunctionLaw function(num, A0);
  PorosityBasedNucleationCriterionLaw criterion1(num, fmin, fmax);
  this->addOneNucleationComponent(function,&criterion1);
};



ExponentialNucleationLaw::ExponentialNucleationLaw(const int num, double fn, double sn, double epsilonn) : 
  NucleationLaw(num)
{
  // Class-function to preserve the old interface.
  // Practically the nucleation manager is first cleared, then fill accordingly with the new interface.
  Msg::Warning("ExponentialNucleationLaw:: This interface is deprecated and will be removed in the future.");
  
  this->clearAllNucleationComponents();
  GaussianNucleationFunctionLaw function(num, fn, sn, epsilonn);
  this->addOneNucleationComponent(function);
};

