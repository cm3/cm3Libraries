//
// C++ Interface: material law
//
// Description: non-local damage elasto-plastic law
//
//
// Author:  <L. NOELS>, (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include <math.h>
#include "MInterfaceElement.h"
#include <fstream>
#include "mlawNonLocalDamage.h"
#include "STensorOperations.h"
#include "FiniteStrain.h"
#include "matrix_operations.h"
//#include "nonLinearMechSolver.h"

mlawNonLocalDamage::mlawNonLocalDamage(const int num, const double rho,
                   const char *propName) : materialLaw(num,true), _rho(rho), 
                             _Dmax_Inc(0.99), _Dmax_Mtx(0.99)
  {
	sq2 = sqrt(2.);

	//allocate memory
	mallocvector(&strn_n,6);
	mallocvector(&strs_n,6);
	mallocvector(&dstrn,6);
	mallocvector(&strs,6);
        mallocvector(&Vstrain,6);
	mallocmatrix(&Ftmp,3,3);
	mallocmatrix(&Ftmp0,3,3);
	mallocmatrix(&dR,3,3);

	mallocmatrix(&Calgo,6,6);
	mallocmatrix(&Cref,6,6);
	mallocmatrix(&C0PathFollowing,6,6);
	mallocmatrix(&invC0PathFollowing,6,6);
        mallocvector(&dEnePathFollowingdStrain,6);
        mallocvector(&elStrainPathFollowing,6);
	malloctens3(&dCref,6,6,6);
	mallocvector(&tau,6);
	mallocmatrix(&dtau,6,6);
        

        std::ifstream in(propName);
        if(!in) Msg::Error("Cannot open the file %s! Maybe is missing   ",propName);
        int dummy;
        in >> dummy;
        in.ignore(256,'\n');
        in >> nprops1;
        in.ignore(256,'\n');
        mallocvector(&props,nprops1);
        for (int i=0;i < nprops1; i++)
        {
          in >> props[i];
          in.ignore(256,'\n');
        }
        mat = init_material(props,0);
        nsdv = mat->get_nsdv();
        nlVar = mat->getNbNonLocalVariables();
        
        
	mallocvector(&dpdE,6);
        mallocvector(&strs_dDdp_bar,6);

  	mallocvector(&dFd_dE,6);
        mallocvector(&dstrs_dFd_bar,6);

        mallocvector(&statev_n,nsdv);


        mallocmatrix(&chara_Length,3,3);   
        mallocmatrix(&chara_Length_INC,3,3);            
  
        SpBar = 0.0; 
        dFd_d_bar = 0.0;
        dpdFd = 0.0;
        dFddp = 0.0;


	model = (int)props[0];
	
	switch(model){
	      case EP_Stoch:
		   break;

              case MTSecFirStoch:
		   break;

	      default :
                   double **C0;
                   mallocmatrix(&C0,6,6);
                   mat->get_elOp(C0);

                  double C1111 = C0[0][0];
                  double C2222 = C0[1][1];
                  double C3333 = C0[2][2];
                  double C1122 = C0[0][1];
                  double C1212 = C0[3][3];
                  double C1313 = C0[4][4];
                  double C2323 = C0[5][5];
                 // taka max value if anistotropy
                  _mu0 = C1212/2.;
                  _nu0 = C1111/2./(_mu0+C1111);
                  if(C1313>_mu0)
                  {
                   _mu0 = C1313;
                   _nu0 = C2222/2./(_mu0+C2222);
                  }
                  if(C2323>_mu0)
                  {
                   _mu0 = C2323;
                   _nu0 = C3333/2./(_mu0+C3333);
                  }
                  if (_nu0 < 0 or _nu0 >0.5) Msg::Error("mlawNonLocalDamage: wrong Poisson coefficient");
	          freematrix(C0,6);
        }

  }
  mlawNonLocalDamage::mlawNonLocalDamage(const mlawNonLocalDamage &source) :
                                        materialLaw(source), _rho(source._rho), 
                                        _mu0(source._mu0), _nu0(source._nu0),
                                        _Dmax_Inc(source._Dmax_Inc), _Dmax_Mtx(source._Dmax_Mtx)

  {
	sq2 = sqrt(2.);

	//allocate memory
	mallocvector(&strn_n,6);
	mallocvector(&strs_n,6);
	mallocvector(&dstrn,6);
	mallocvector(&strs,6);
        mallocvector(&Vstrain,6);
	mallocmatrix(&Ftmp,3,3);
	mallocmatrix(&Ftmp0,3,3);
	mallocmatrix(&dR,3,3);

	mallocmatrix(&Calgo,6,6);
	mallocmatrix(&Cref,6,6);
	mallocmatrix(&C0PathFollowing,6,6);
	mallocmatrix(&invC0PathFollowing,6,6);
        mallocvector(&dEnePathFollowingdStrain,6);
        mallocvector(&elStrainPathFollowing,6);
	malloctens3(&dCref,6,6,6);
	mallocvector(&tau,6);
	mallocmatrix(&dtau,6,6);

        for(int i=0; i< 3; i++)
        {
           for(int j=0; j<3; j++)
           {
              Ftmp[i][j] = source.Ftmp[i][j];
              Ftmp0[i][j] = source.Ftmp0[i][j];
              dR[i][j] = source.dR[i][j];
           }
        }
        for(int i=0; i< 6; i++)
        {
           strn_n[i] = source.strn_n[i];
           strs_n[i] = source.strs_n[i];
           dstrn[i]  = source.dstrn[i];
           strs[i]   = source.strs[i];
           tau[i]    = source.tau[i];
           Vstrain[i]    = source.Vstrain[i];
           dEnePathFollowingdStrain[i] = source.dEnePathFollowingdStrain[i];
           elStrainPathFollowing[i]    = source.elStrainPathFollowing[i];
           for(int j=0; j<6; j++)
           {
              Calgo[i][j] = source.Calgo[i][j];
              Cref[i][j]  = source.Cref[i][j];
              C0PathFollowing[i][j]=source.C0PathFollowing[i][j];
              invC0PathFollowing[i][j]=source.invC0PathFollowing[i][j];
              dtau[i][j]  = source.dtau[i][j];
	      for(int k=0; k<6; k++)
              {
                dCref[i][j][k] = source.dCref[i][j][k];
              }
           }
         }
         nprops1 = source.nprops1;
         mallocvector(&props,nprops1);
         for (int i=0;i < nprops1; i++)
         {
           props[i] = source.props[i];
         }
        mat = init_material(props,0);
        nsdv = mat->get_nsdv();
	nlVar = source.nlVar;

	mallocvector(&dpdE,6);
        mallocvector(&strs_dDdp_bar,6);
  	mallocvector(&dFd_dE,6);
        mallocvector(&dstrs_dFd_bar,6);

        for(int i=0; i< 6; i++)
        {
           dpdE[i] = source.dpdE[i];
           strs_dDdp_bar[i] = source.strs_dDdp_bar[i];
           dFd_dE[i] = source.dFd_dE[i];
           dstrs_dFd_bar[i] = source.dstrs_dFd_bar[i];

        }

        mallocmatrix(&chara_Length,3,3);        
        mallocmatrix(&chara_Length_INC,3,3);         

        mallocvector(&statev_n,nsdv);
        copyvect(source.statev_n, statev_n,nsdv);


        for(int i=0; i< 3; i++)
        {
           for (int j=0; j<3; j++)
               {
                chara_Length[i][j] = source.chara_Length[i][j];
                chara_Length_INC[i][j] = source.chara_Length_INC[i][j];
               }
        }                                             // by Wu Ling 2011/10/04
        SpBar = source.SpBar;
        dFd_d_bar = source.dFd_d_bar;
        dpdFd = source.dpdFd;
        dFddp = source.dFddp;

  }

  mlawNonLocalDamage& mlawNonLocalDamage::operator=(const materialLaw &source)
  {
     materialLaw::operator=(source);
     const mlawNonLocalDamage* src =static_cast<const mlawNonLocalDamage*>(&source);
     _rho = src->_rho;
     _Dmax_Inc = src->_Dmax_Inc;
     _Dmax_Mtx = src->_Dmax_Mtx;
	sq2 = sqrt(2.);
        for(int i=0; i< 3; i++)
        {
           for(int j=0; j<3; j++)
           {
              Ftmp[i][j] = src->Ftmp[i][j];
              Ftmp0[i][j] = src->Ftmp0[i][j];
              dR[i][j] = src->dR[i][j];
           }
        }

        for(int i=0; i< 6; i++)
        {
           strn_n[i] = src->strn_n[i];
           strs_n[i] = src->strs_n[i];
           dstrn[i]  = src->dstrn[i];
           strs[i]   = src->strs[i];
           tau[i]    = src->tau[i];
           Vstrain[i]= src->Vstrain[i];
           dEnePathFollowingdStrain[i] = src->dEnePathFollowingdStrain[i];
           elStrainPathFollowing[i] = src->elStrainPathFollowing[i];
           for(int j=0; j<6; j++)
           {
              Calgo[i][j] = src->Calgo[i][j];
              Cref[i][j]  = src->Cref[i][j];
              C0PathFollowing[i][j] = src->C0PathFollowing[i][j];
              invC0PathFollowing[i][j] = src->invC0PathFollowing[i][j];
              dtau[i][j]  = src->dtau[i][j];
	      for(int k=0; k<6; k++)
              {
                dCref[i][j][k] = src->dCref[i][j][k];
              }
           }
         }
         nprops1 = src->nprops1;
         for (int i=0;i < nprops1; i++)
         {
           props[i] = src->props[i];
         }
         mat = init_material(props,0);
         nsdv = mat->get_nsdv();
	 nlVar = src->nlVar;

         for(int i=0; i< 6; i++)
         {
           dpdE[i] = src->dpdE[i];
           strs_dDdp_bar[i] = src->strs_dDdp_bar[i];
           dFd_dE[i] = src->dFd_dE[i];
           dstrs_dFd_bar[i] = src->dstrs_dFd_bar[i];
         }

         for(int i=0; i< 3; i++)
         {
           for (int j=0; j<3; j++)
               {
                chara_Length[i][j] = src->chara_Length[i][j];
                chara_Length_INC[i][j] = src->chara_Length_INC[i][j];
               }
        }                                             // by Wu Ling 2011/10/04
        SpBar = src->SpBar;
        dFd_d_bar = src->dFd_d_bar;
        dpdFd = src->dpdFd;
        dFddp = src->dFddp;

        copyvect(src->statev_n, statev_n,nsdv);

  return *this;
}


  mlawNonLocalDamage::~mlawNonLocalDamage()
  {
    free(strn_n);
    free(strs_n);
    free(dstrn);
    free(strs);
    free(Vstrain);
    free(dEnePathFollowingdStrain);
    free(elStrainPathFollowing);
    freematrix(Ftmp,3);
    freematrix(Ftmp0,3);
    freematrix(dR,3);
    freematrix(Calgo,6);
    freematrix(Cref,6);
    freematrix(C0PathFollowing,6);
    freematrix(invC0PathFollowing,6);
    freetens3(dCref,6,6);
    free(tau);
    freematrix(dtau,6);

    free(props);
    free(statev_n);

    free(dpdE);
    free(strs_dDdp_bar);
    free(dFd_dE);
    free(dstrs_dFd_bar);

    freematrix(chara_Length,3); 
    freematrix(chara_Length_INC,3); 
  }
  
  
void mlawNonLocalDamage::createIPState(IPStateBase* &ips,bool hasBodyForce,const bool* state_,const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
  int nsdv = this->getNsdv();
  IPVariable* ivi = new IPNonLocalDamage(nsdv);
  IPVariable* iv1 = new IPNonLocalDamage(nsdv);
  IPVariable* iv2 = new IPNonLocalDamage(nsdv);
     
  if(ips != NULL) delete ips;
  ips = new IP3State(state_,ivi,iv1,iv2);
  /*IPNonLocalDamage* qi = static_cast<IPNonLocalDamage*>(ivi);
  IPNonLocalDamage* q1 = static_cast<IPNonLocalDamage*>(iv1);
  IPNonLocalDamage* q2 = static_cast<IPNonLocalDamage*>(iv2);
               
  createIPState(qi,q1,q2);*/
}

  
void mlawNonLocalDamage::createIPState(IPNonLocalDamage *ivi, IPNonLocalDamage *iv1, IPNonLocalDamage *iv2) const
{

  //initialize Eshelby tensor
  int kstep = 1;
  int kinc  = 1;
  double *stvi = ivi->_nldStatev;
  double *stv1 = iv1->_nldStatev;
  double *stv2 = iv2->_nldStatev;
  double SpBartmp = 0.0;
  double dFd_d_bartmp = 0.0;
  double dpdFdtmp = 0.0;
  double dFddptmp = 0.0;
  std::vector<double> euler_lam;

  ivi->setPosMaxD(mat->get_pos_maxD());
  ivi->setPosIncMaxD(mat->get_pos_inc_maxD());
  ivi->setPosMtxMaxD(mat->get_pos_mtx_maxD());
  iv1->setPosMaxD(mat->get_pos_maxD());
  iv1->setPosIncMaxD(mat->get_pos_inc_maxD());
  iv1->setPosMtxMaxD(mat->get_pos_mtx_maxD());
  iv2->setPosMaxD(mat->get_pos_maxD());
  iv2->setPosIncMaxD(mat->get_pos_inc_maxD());
  iv2->setPosMtxMaxD(mat->get_pos_mtx_maxD());

  ivi->setMaxD(_Dmax_Mtx);
  ivi->setIncMaxD(_Dmax_Inc);
  ivi->setMtxMaxD(_Dmax_Mtx);
  iv1->setMaxD(_Dmax_Mtx);
  iv1->setIncMaxD(_Dmax_Inc);
  iv1->setMtxMaxD(_Dmax_Mtx);
  iv2->setMaxD(_Dmax_Mtx);
  iv2->setIncMaxD(_Dmax_Inc);
  iv2->setMtxMaxD(_Dmax_Mtx);

  if (mat->get_constmodel() == VT or mat->get_constmodel() == LAM_2PLY)  // setIncMtxMaxD for VT or LAM_2PLY case
  {
      mat->populateMaxD(stvi, _Dmax_Inc, _Dmax_Mtx);
      mat->populateMaxD(stv1, _Dmax_Inc, _Dmax_Mtx);
      mat->populateMaxD(stv2, _Dmax_Inc, _Dmax_Mtx);
  }

  mat->constbox(dstrn, strs_n, strs, stvi, stv1, Cref, dCref, tau, dtau, Calgo, 1., dpdE, strs_dDdp_bar, &SpBartmp, chara_Length, &dFd_d_bartmp, dstrs_dFd_bar, dFd_dE, chara_Length_INC, &dpdFdtmp, &dFddptmp, kinc, kstep, _timeStep);
 
  updateCharacteristicLengthTensor(chara_Length,stv1);
  updateCharacteristicLengthTensor(chara_Length_INC,stv1);

  for(int i=0;i<3;i++)
  {
    for(int j=0;j<3;j++){
      ivi->_nldCharacteristicLengthMatrix(i,j) = chara_Length[i][j];                 //by Wu Ling
      iv1->_nldCharacteristicLengthMatrix(i,j) = chara_Length[i][j];                 //by Wu Ling
      iv2->_nldCharacteristicLengthMatrix(i,j) = chara_Length[i][j];                 //by Wu Ling
      ivi->_nldCharacteristicLengthFiber(i,j) = chara_Length_INC[i][j];
      iv1->_nldCharacteristicLengthFiber(i,j) = chara_Length_INC[i][j];
      iv2->_nldCharacteristicLengthFiber(i,j) = chara_Length_INC[i][j];
    }
  }
  ivi->setPosStrMtx(mat->get_pos_mtx_stress());
  ivi->setPosStrInc(mat->get_pos_inc_stress());
  ivi->setPosStnMtx(mat->get_pos_mtx_strain());
  ivi->setPosStnInc(mat->get_pos_inc_strain());
  iv1->setPosStrMtx(mat->get_pos_mtx_stress());
  iv1->setPosStrInc(mat->get_pos_inc_stress());
  iv1->setPosStnMtx(mat->get_pos_mtx_strain());
  iv1->setPosStnInc(mat->get_pos_inc_strain());
  iv1->setPosIncDam(mat->get_pos_inc_Dam());
  iv1->setPosMtxDam(mat->get_pos_mtx_Dam());
  iv2->setPosStrMtx(mat->get_pos_mtx_stress());
  iv2->setPosStrInc(mat->get_pos_inc_stress());
  iv2->setPosStnMtx(mat->get_pos_mtx_strain());
  iv2->setPosStnInc(mat->get_pos_inc_strain());
  ivi->setPosIncVfi(mat->get_pos_vfi());
  ivi->setPosIncAR(mat->get_pos_aspR());
  ivi->setPosIncR(mat->get_pos_Ri());
  ivi->setPosIncJa(mat->get_pos_inc_Ja());
  ivi->setPosMtxJa(mat->get_pos_mtx_Ja());
  ivi->setPosIncDam(mat->get_pos_inc_Dam());
  ivi->setPosMtxDam(mat->get_pos_mtx_Dam());
  iv2->setPosIncVfi(mat->get_pos_vfi());
  iv2->setPosIncAR(mat->get_pos_aspR());
  iv2->setPosIncR(mat->get_pos_Ri());
  iv2->setPosIncJa(mat->get_pos_inc_Ja());
  iv2->setPosMtxJa(mat->get_pos_mtx_Ja());
  iv2->setPosIncDam(mat->get_pos_inc_Dam());
  iv2->setPosMtxDam(mat->get_pos_mtx_Dam());


  // for VT case (VM or VLM)
  if(model==VT){
  	int iVTM0 = 0;
  	int iVTMT = 0;
  	int iVTLam = 0;
  	int iVTLamM0 = 0;
  	int iVTLamMT = 0;
  	// for each VT phase
  	for(int iVT=0;iVT<mat->getNPatch();iVT++){
  		switch(mat->get_mat_constmodel(iVT)){
  			case EP:  // for pure matrix grains
  				// strain
  				ivi->setPosStrnVTM0(mat->getPosStrnVTM0(iVTM0));
  				iv2->setPosStrnVTM0(mat->getPosStrnVTM0(iVTM0));
  				// stress
  				ivi->setPosStrsVTM0(mat->getPosStrsVTM0(iVTM0));
  				iv2->setPosStrsVTM0(mat->getPosStrsVTM0(iVTM0));
  				// damage
  				ivi->setPosDamVTM0(mat->getPosDamVTM0(iVTM0));
  				iv2->setPosDamVTM0(mat->getPosDamVTM0(iVTM0));
  				
  				++iVTM0;
  				break;
  				
  			case MTSecF:  // for MT(Mtx+Inc) grains
  				// strain
  				ivi->setPosStrnVTMT(mat->getPosStrnVTMT(iVTMT));
  				iv2->setPosStrnVTMT(mat->getPosStrnVTMT(iVTMT));
  				// stress
  				ivi->setPosStrsVTMT(mat->getPosStrsVTMT(iVTMT));
  				iv2->setPosStrsVTMT(mat->getPosStrsVTMT(iVTMT));
  				// for each material phase
  				// strain
  				ivi->setPosStrnVTMTMtx(mat->getPosStrnVTMTMtx(iVTMT));
  				ivi->setPosStrnVTMTInc(mat->getPosStrnVTMTInc(iVTMT));
  				iv2->setPosStrnVTMTMtx(mat->getPosStrnVTMTMtx(iVTMT));
  				iv2->setPosStrnVTMTInc(mat->getPosStrnVTMTInc(iVTMT));
  				// stress
  				ivi->setPosStrsVTMTMtx(mat->getPosStrsVTMTMtx(iVTMT));
  				ivi->setPosStrsVTMTInc(mat->getPosStrsVTMTInc(iVTMT));
  				iv2->setPosStrsVTMTMtx(mat->getPosStrsVTMTMtx(iVTMT));
  				iv2->setPosStrsVTMTInc(mat->getPosStrsVTMTInc(iVTMT));
  				// damage
  				ivi->setPosDamVTMTMtx(mat->getPosDamVTMTMtx(iVTMT));
  				ivi->setPosDamVTMTInc(mat->getPosDamVTMTInc(iVTMT));
  				iv2->setPosDamVTMTMtx(mat->getPosDamVTMTMtx(iVTMT));
  				iv2->setPosDamVTMTInc(mat->getPosDamVTMTInc(iVTMT));
  				
  				++iVTMT;
  				break;

  			case LAM_2PLY:  // for laminate grains
  				ivi->setEulerVTLam(mat->getEulerVTLam(euler_lam, iVTLam));
  				iv2->setEulerVTLam(mat->getEulerVTLam(euler_lam, iVTLam));
  				// strain
  				ivi->setPosStrnVTLam(mat->getPosStrnVTLam(iVTLam));
  				iv2->setPosStrnVTLam(mat->getPosStrnVTLam(iVTLam));
  				// stress
  				ivi->setPosStrsVTLam(mat->getPosStrsVTLam(iVTLam));
  				iv2->setPosStrsVTLam(mat->getPosStrsVTLam(iVTLam));
  				// for each ply in laminate grains
  				for(int jVTLam=0;jVTLam<mat->get_mat_NPatch(iVT);jVTLam++){
  					switch(mat->get_submat_constmodel(iVT, jVTLam)){
  						case EP:  // for pure matrix ply
  							// strain
  							ivi->setPosStrnVTLamM0(mat->getPosStrnVTLamM0(iVTLamM0));
  							iv2->setPosStrnVTLamM0(mat->getPosStrnVTLamM0(iVTLamM0));
  							// stress
  							ivi->setPosStrsVTLamM0(mat->getPosStrsVTLamM0(iVTLamM0));
  							iv2->setPosStrsVTLamM0(mat->getPosStrsVTLamM0(iVTLamM0));
  							// damage
  							ivi->setPosDamVTLamM0(mat->getPosDamVTLamM0(iVTLamM0));
  							iv2->setPosDamVTLamM0(mat->getPosDamVTLamM0(iVTLamM0));
  							
  							++iVTLamM0;
  							break;
  							
  						case MTSecF:  // for MT(Mtx+Inc) ply
  							// strain
  							ivi->setPosStrnVTLamMT(mat->getPosStrnVTLamMT(iVTLamMT));
  							iv2->setPosStrnVTLamMT(mat->getPosStrnVTLamMT(iVTLamMT));
  							// stress
  							ivi->setPosStrsVTLamMT(mat->getPosStrsVTLamMT(iVTLamMT));
  							iv2->setPosStrsVTLamMT(mat->getPosStrsVTLamMT(iVTLamMT));
  							// for each material phase
  							// strain
  							ivi->setPosStrnVTLamMTMtx(mat->getPosStrnVTLamMTMtx(iVTLamMT));
  							ivi->setPosStrnVTLamMTInc(mat->getPosStrnVTLamMTInc(iVTLamMT));
  							iv2->setPosStrnVTLamMTMtx(mat->getPosStrnVTLamMTMtx(iVTLamMT));
  							iv2->setPosStrnVTLamMTInc(mat->getPosStrnVTLamMTInc(iVTLamMT));
  							// stress
  							ivi->setPosStrsVTLamMTMtx(mat->getPosStrsVTLamMTMtx(iVTLamMT));
  							ivi->setPosStrsVTLamMTInc(mat->getPosStrsVTLamMTInc(iVTLamMT));
  							iv2->setPosStrsVTLamMTMtx(mat->getPosStrsVTLamMTMtx(iVTLamMT));
  							iv2->setPosStrsVTLamMTInc(mat->getPosStrsVTLamMTInc(iVTLamMT));
  							// damage
  							ivi->setPosDamVTLamMTMtx(mat->getPosDamVTLamMTMtx(iVTLamMT));
  							ivi->setPosDamVTLamMTInc(mat->getPosDamVTLamMTInc(iVTLamMT));
  							iv2->setPosDamVTLamMTMtx(mat->getPosDamVTLamMTMtx(iVTLamMT));
  							iv2->setPosDamVTLamMTInc(mat->getPosDamVTLamMTInc(iVTLamMT));
  							
  							++iVTLamMT;
  							break;
  					}
  				}
  				
  				++iVTLam;
  				break;
  		}
  	}
  }
  // for LAM_2PLY case (LVM)
  else if(model==LAM_2PLY){
	int iLamM0 = 0;
  	int iLamVT = 0;
  	int iLamVTMT = 0;
  	// for each laminate ply
  	for(int iLam=0;iLam<mat->getNPatch();iLam++){
  		switch(mat->get_mat_constmodel(iLam)){
  			case EP:  // for pure matrix ply
  				// strain
  				ivi->setPosStrnLamM0(mat->getPosStrnLamM0(iLamM0));
  				iv2->setPosStrnLamM0(mat->getPosStrnLamM0(iLamM0));
  				// stress
  				ivi->setPosStrsLamM0(mat->getPosStrsLamM0(iLamM0));
  				iv2->setPosStrsLamM0(mat->getPosStrsLamM0(iLamM0));
  				// damage
  				ivi->setPosDamLamM0(mat->getPosDamLamM0(iLamM0));
  				iv2->setPosDamLamM0(mat->getPosDamLamM0(iLamM0));
  				
  				++iLamM0;
  				break;
  				
  			case VT:  // for VT ply
  				// strain
  				ivi->setPosStrnLamVT(mat->getPosStrnLamVT(iLamVT));
  				iv2->setPosStrnLamVT(mat->getPosStrnLamVT(iLamVT));
  				// stress
  				ivi->setPosStrsLamVT(mat->getPosStrsLamVT(iLamVT));
  				iv2->setPosStrsLamVT(mat->getPosStrsLamVT(iLamVT));
  				// for each MT grain in VT ply
  				for(int jLamVT=0;jLamVT<mat->get_mat_NPatch(iLam);jLamVT++){
  					switch(mat->get_submat_constmodel(iLam, jLamVT)){
  						case MTSecF:  // for MT(Mtx+Inc) grains
  							// strain
  							ivi->setPosStrnLamVTMT(mat->getPosStrnLamVTMT(iLamVTMT));
  							iv2->setPosStrnLamVTMT(mat->getPosStrnLamVTMT(iLamVTMT));
  							// stress
  							ivi->setPosStrsLamVTMT(mat->getPosStrsLamVTMT(iLamVTMT));
  							iv2->setPosStrsLamVTMT(mat->getPosStrsLamVTMT(iLamVTMT));
  							// for each material phase
  							// strain
  							ivi->setPosStrnLamVTMTMtx(mat->getPosStrnLamVTMTMtx(iLamVTMT));
  							ivi->setPosStrnLamVTMTInc(mat->getPosStrnLamVTMTInc(iLamVTMT));
  							iv2->setPosStrnLamVTMTMtx(mat->getPosStrnLamVTMTMtx(iLamVTMT));
  							iv2->setPosStrnLamVTMTInc(mat->getPosStrnLamVTMTInc(iLamVTMT));
  							// stress
  							ivi->setPosStrsLamVTMTMtx(mat->getPosStrsLamVTMTMtx(iLamVTMT));
  							ivi->setPosStrsLamVTMTInc(mat->getPosStrsLamVTMTInc(iLamVTMT));
  							iv2->setPosStrsLamVTMTMtx(mat->getPosStrsLamVTMTMtx(iLamVTMT));
  							iv2->setPosStrsLamVTMTInc(mat->getPosStrsLamVTMTInc(iLamVTMT));
  							// damage
  							ivi->setPosDamLamVTMTMtx(mat->getPosDamLamVTMTMtx(iLamVTMT));
  							ivi->setPosDamLamVTMTInc(mat->getPosDamLamVTMTInc(iLamVTMT));
  							iv2->setPosDamLamVTMTMtx(mat->getPosDamLamVTMTMtx(iLamVTMT));
  							iv2->setPosDamLamVTMTInc(mat->getPosDamLamVTMTInc(iLamVTMT));
  							
  							++iLamVTMT;
  							break;
  					}
  				}
  				
  				++iLamVT;
  				break;
  		}
  	}  
  }


  copyvect(stv1,stvi,nsdv);
  copyvect(stv1,stv2,nsdv);

}

void mlawNonLocalDamage::createIPVariable(IPNonLocalDamage *&ipv,bool hasBodyForce, const MElement *ele,const int nbFF,const IntPt *GP, const int gpt) const
{
  //initialize Eshelby tensor
  int kstep = 1;
  int kinc  = 1;
  double *stvi = ipv->_nldStatev;
  double SpBartmp = 0.0;
  double dFd_d_bartmp = 0.0;
  double dpdFdtmp = 0.0;
  double dFddptmp = 0.0;

  ipv->setPosMaxD(mat->get_pos_maxD());
  ipv->setPosIncMaxD(mat->get_pos_inc_maxD());
  ipv->setPosMtxMaxD(mat->get_pos_mtx_maxD());

  ipv->setMaxD(_Dmax_Mtx);
  ipv->setIncMaxD(_Dmax_Inc);
  ipv->setMtxMaxD(_Dmax_Mtx);

  mat->constbox(dstrn, strs_n, strs, stvi, stvi, Cref, dCref, tau, dtau, Calgo, 1., dpdE, strs_dDdp_bar, &SpBartmp, chara_Length, &dFd_d_bartmp, dstrs_dFd_bar, dFd_dE, chara_Length_INC, &dpdFdtmp, &dFddptmp, kinc, kstep, _timeStep);

  updateCharacteristicLengthTensor(chara_Length,stvi);
  updateCharacteristicLengthTensor(chara_Length_INC,stvi);

  for(int i=0;i<3;i++)
  {
    for(int j=0;j<3;j++){
      ipv->_nldCharacteristicLengthMatrix(i,j) = chara_Length[i][j];                 //by Wu Ling
      ipv->_nldCharacteristicLengthFiber(i,j) = chara_Length_INC[i][j];                 //by Wu Ling
    }
  }
  ipv->setPosStrMtx(mat->get_pos_mtx_stress());
  ipv->setPosStrInc(mat->get_pos_inc_stress());
  ipv->setPosStnMtx(mat->get_pos_mtx_strain());
  ipv->setPosStnInc(mat->get_pos_inc_strain());
  ipv->setPosIncVfi(mat->get_pos_vfi());
  ipv->setPosIncAR(mat->get_pos_aspR());
  ipv->setPosIncR(mat->get_pos_Ri());
  ipv->setPosIncJa(mat->get_pos_inc_Ja());
  ipv->setPosMtxJa(mat->get_pos_mtx_Ja());
  ipv->setPosIncDam(mat->get_pos_inc_Dam());
  ipv->setPosMtxDam(mat->get_pos_mtx_Dam());
}

double mlawNonLocalDamage::soundSpeed() const
{
  double nu = poissonRatio();
  double mu = shearModulus();
  double E = 2.*mu*(1.+nu);
  double factornu = (1.-nu)/((1.+nu)*(1.-2.*nu));
  return sqrt(E*factornu/_rho);
}

void mlawNonLocalDamage::constitutive(const STensor3& F0,const STensor3& Fn,STensor3 &P,const IPVariable *ipvprevi,
                                      IPVariable *ipvcuri,STensor43 &Tangent,const bool stiff,STensor43* elasticTangent, 
                                      const bool dTangent,
                                      STensor63* dCalgdeps) const
{
  if(elasticTangent!=NULL) Msg::Error(" mlawNonLocalDamage STensor43* elasticTangent not defined");
  const IPNonLocalDamage *ipvprev = dynamic_cast<const IPNonLocalDamage *> (ipvprevi);
  IPNonLocalDamage *ipvcur = dynamic_cast<IPNonLocalDamage *> (ipvcuri);
  
  STensor3& eigenstrain = ipvcur->_eigenstrain;
  STensor3& eigenstress = ipvcur->_eigenstress;

  //convert F to eps  :order xx yy zz xy xz yz
  if(!mat->isInLargeDeformation())
  {
    for(int i=0;i<3;i++)
    {
      ipvcur->_nldStrain(i) = Fn(i,i)-1.;
    }
    //gamma
    ipvcur->_nldStrain(3) = Fn(0,1)+Fn(1,0);
    ipvcur->_nldStrain(4) = Fn(0,2)+Fn(2,0);
    ipvcur->_nldStrain(5) = Fn(2,1)+Fn(1,2);
  }
  else
  {
    for(int i=0;i<3;i++)
    {
       for(int j=0;j<3;j++)
       {
         Ftmp[i][j] = Fn(i,j);
         Ftmp0[i][j] = F0(i,j);
       }
    }
    Getstrnln(Ftmp, Vstrain);
    GetdRot(Ftmp0, Ftmp, dR);
    matrix_to_vect(3, dR, &(ipvprev->_nldStatev[mat->get_pos_dR()]));
    matrix_to_vect(3, dR, &(ipvcur->_nldStatev[mat->get_pos_dR()]));

    for(int i=0;i<3;i++){
      ipvcur->_nldStrain(i) = Vstrain[i];
      //ipvcur->_nldStatev[i] = Vstrain[i];
    }
    for(int i=3;i<6;i++){
      ipvcur->_nldStrain(i) = Vstrain[i];
      //ipvcur->_nldStatev[i] = Vstrain[i]/sq2;    //**using the current state varible to pass the total strain*****
    }
  }

  //get previous strain and stress
  for(int i=0;i<3;i++){
    strn_n[i]=(ipvprev->_nldStrain)(i);
    strs_n[i]=(ipvprev->_nldStress)(i);
    dstrn[i]=(ipvcur->_nldStrain)(i)-(ipvprev->_nldStrain)(i);
  }
  for(int i=3;i<6;i++){
    strn_n[i]=(ipvprev->_nldStrain)(i)/sq2;
    dstrn[i]=((ipvcur->_nldStrain)(i)-(ipvprev->_nldStrain)(i))/sq2;
    strs_n[i]=(ipvprev->_nldStress)(i)*sq2;
  }

  //HERE WE SHOULD COPY THE PREVIOUS ONE AND NOT CHANGING IT DIRECTLY
  //statev_n = ipvprev->_nldStatev;
  copyvect(ipvprev->_nldStatev, statev_n,nsdv);


  statev_1 = ipvcur->_nldStatev;

  //SHOULD BE MORE GENERAL
  if( mat->get_pos_effectiveplasticstrainfornonlocal() >=0 )
  {
    //should be 66 (-39 for EP)
    statev_n[mat->get_pos_effectiveplasticstrainfornonlocal()] = ipvprev->_nldEffectivePlasticStrain;
    statev_n[mat->get_pos_effectiveplasticstrainfornonlocal()+1] = (ipvcur->_nldEffectivePlasticStrain-
                                                                    ipvprev->_nldEffectivePlasticStrain);
  }
  if( mat->get_pos_Fd_bar() > 0 )
  {
    statev_n[mat->get_pos_Fd_bar()] =  ipvprev->_nlFiber_d_bar;  
    statev_n[mat->get_pos_dFd_bar()] = (ipvcur->_nlFiber_d_bar - ipvprev->_nlFiber_d_bar);
  } 


  // to required to use Eshelby tensor of previous step?
  copyvect(statev_n,statev_1,nsdv);
  if (ipvcur->dissipationIsBlocked() or ipvprev->dissipationIsBlocked())
  {
    for(int nlv=0; nlv< getNbNonLocalVariables(); nlv++)
    {
      ipvcur->setCriticalDamage(statev_n,ipvprev->getDamageIndicator(nlv),nlv);
      ipvcur->setCriticalDamage(statev_1,ipvprev->getDamageIndicator(nlv),nlv);
    }
  }
  if(mat->isInLargeDeformation())
  {
     for(int i=0;i<3;i++){
         statev_1[i] = Vstrain[i];
     }
     for(int i=3;i<6;i++){
         statev_1[i] = Vstrain[i]/sq2;    //**using the current state varible to pass the total strain*****
    }

  }
  int kinc = 1;
  int kstep = 2;
  SpBar = 0.0;
  dFd_d_bar = 0.0;
  dpdFd = 0.0;
  dFddp = 0.0;

  int error= mat->constbox(dstrn, strs_n, strs, statev_n, statev_1, Cref, dCref, tau, dtau, Calgo, 1., dpdE, strs_dDdp_bar, &SpBar, chara_Length, &dFd_d_bar, dstrs_dFd_bar, dFd_dE, chara_Length_INC, &dpdFd, &dFddp, kinc, kstep, _timeStep);

  //recompute becasue could have changed if no convergence and is used for Irreversible energy
  for(int i=0;i<3;i++){
    strn_n[i]=(ipvprev->_nldStrain)(i);
    strs_n[i]=(ipvprev->_nldStress)(i);
    dstrn[i]=(ipvcur->_nldStrain)(i)-(ipvprev->_nldStrain)(i);
  }
  for(int i=3;i<6;i++){
    strn_n[i]=(ipvprev->_nldStrain)(i)/sq2;
    dstrn[i]=((ipvcur->_nldStrain)(i)-(ipvprev->_nldStrain)(i))/sq2;
    strs_n[i]=(ipvprev->_nldStress)(i)*sq2;
  }
  if(error==0)
  {
    //update stress and tangent operator
    updateCharacteristicLengthTensor(chara_Length, statev_1);
    updateCharacteristicLengthTensor(chara_Length_INC, statev_1);
    for(int i=0;i<3;i++)
    {
      (ipvcur->_nldStress)(i)=strs[i];
      for(int j=0;j<3;j++){
        (ipvcur->_nldMaterialTensor)(i,j)=Calgo[i][j];
  
        (ipvcur->_nldCharacteristicLengthMatrix)(i,j) = chara_Length[i][j];                 //by Wu Ling
        (ipvcur->_nldCharacteristicLengthFiber)(i,j) = chara_Length_INC[i][j];
      }
      if( mat->get_pos_damagefornonlocal()>0)
      {
        ipvcur->_nldCouplingStressEffectiveStrain(i) = strs_dDdp_bar[i];
        ipvcur->_nldCouplingEffectiveStrainStress(i) = dpdE[i];
      }
      if( mat->get_pos_Fd_bar()>0)
      {
        ipvcur->_nldFiberDamdStrain(i) = dFd_dE[i];
        ipvcur->_nldStressdFiberDam(i) = dstrs_dFd_bar[i];
      }
    }

    ipvcur->_nldSpBar = SpBar;
    ipvcur->_nldFd_d_bar = dFd_d_bar;
    ipvcur->_nldpdFd = dpdFd;
    ipvcur->_nldFddp = dFddp;

    for(int i=3;i<6;i++)
    {
      (ipvcur->_nldStress)(i)=strs[i]/sq2;
      for(int j=0;j<3;j++)
      {
        (ipvcur->_nldMaterialTensor)(i,j)=Calgo[i][j]/sq2;
        (ipvcur->_nldMaterialTensor)(j,i)=Calgo[j][i]/sq2;
      }
      for(int j=3;j<6;j++)
      {
        (ipvcur->_nldMaterialTensor)(i,j)=Calgo[i][j]/2.;
      }
      if( mat->get_pos_damagefornonlocal()>=0)
      {
        ipvcur->_nldCouplingStressEffectiveStrain(i) = strs_dDdp_bar[i]/sq2;
        ipvcur->_nldCouplingEffectiveStrainStress(i) = dpdE[i]/sq2;
      }
      if( mat->get_pos_Fd_bar()>0) 
      {
        ipvcur->_nldFiberDamdStrain(i) = dFd_dE[i]/sq2;
        ipvcur->_nldStressdFiberDam(i) = dstrs_dFd_bar[i]/sq2;
      }
    }
    if( mat->get_pos_damagefornonlocal() >= 0)
    {
      ipvcur->_nldDamage = statev_1[mat->get_pos_damagefornonlocal()];
    }
    //if( mat->get_pos_Fd_bar() > 0)
    //{
    //  ipvcur->_nlFiber_d_bar = statev_1[mat->get_pos_Fd_bar()];
    //}

    if( mat->get_pos_currentplasticstrainfornonlocal() >=0)
    {
      ipvcur->_nldCurrentPlasticStrain = statev_1[mat->get_pos_currentplasticstrainfornonlocal()];
    }

    if( mat->get_pos_locFd() >0)
    {
      ipvcur->_nlFiber_loc = statev_1[mat->get_pos_locFd()];
    }

    //convert str to P  :order xx yy zz xy xz yz
    for(int i=0;i<3;i++)
    {
      P(i,i) = ipvcur->_nldStress(i);
    }
    P(0,1) = ipvcur->_nldStress(3);
    P(1,0) = ipvcur->_nldStress(3);
    P(0,2) = ipvcur->_nldStress(4);
    P(2,0) = ipvcur->_nldStress(4);
    P(1,2) = ipvcur->_nldStress(5);
    P(2,1) = ipvcur->_nldStress(5);
    if(mat->isInLargeDeformation())
    {
      //What we have is actually sigma -> send to P
      static STensor3 Finv, FinvT;
      STensorOperation::inverseSTensor3(Fn, Finv);
      STensorOperation::transposeSTensor3(Finv, FinvT);
      P*=FinvT;
      P*=Fn.determinant();
    }
    //convert MaterialTensor to Tangent  :order xx yy zz xy xz yz
   if(stiff){
    //if(mat->isInLargeDeformation())
    //{
    //  Msg::Info("Material tensor does not exist in large deformation");
    //}
    Tangent(0,0,0,0) = ipvcur->_nldMaterialTensor(0, 0);
    Tangent(0,0,0,1) = ipvcur->_nldMaterialTensor(0, 3);
    Tangent(0,0,0,2) = ipvcur->_nldMaterialTensor(0, 4);
    Tangent(0,0,1,0) = ipvcur->_nldMaterialTensor(0, 3);
    Tangent(0,0,1,1) = ipvcur->_nldMaterialTensor(0, 1);
    Tangent(0,0,1,2) = ipvcur->_nldMaterialTensor(0, 5);
    Tangent(0,0,2,0) = ipvcur->_nldMaterialTensor(0, 4);
    Tangent(0,0,2,1) = ipvcur->_nldMaterialTensor(0, 5);
    Tangent(0,0,2,2) = ipvcur->_nldMaterialTensor(0, 2);

    Tangent(1,1,0,0) = ipvcur->_nldMaterialTensor(1, 0);  
    Tangent(1,1,0,1) = ipvcur->_nldMaterialTensor(1, 3);
    Tangent(1,1,0,2) = ipvcur->_nldMaterialTensor(1, 4);
    Tangent(1,1,1,0) = ipvcur->_nldMaterialTensor(1, 3);
    Tangent(1,1,1,1) = ipvcur->_nldMaterialTensor(1, 1);
    Tangent(1,1,1,2) = ipvcur->_nldMaterialTensor(1, 5);
    Tangent(1,1,2,0) = ipvcur->_nldMaterialTensor(1, 4);
    Tangent(1,1,2,1) = ipvcur->_nldMaterialTensor(1, 5);
    Tangent(1,1,2,2) = ipvcur->_nldMaterialTensor(1, 2);

    Tangent(2,2,0,0) = ipvcur->_nldMaterialTensor(2, 0);  
    Tangent(2,2,0,1) = ipvcur->_nldMaterialTensor(2, 3);
    Tangent(2,2,0,2) = ipvcur->_nldMaterialTensor(2, 4);
    Tangent(2,2,1,0) = ipvcur->_nldMaterialTensor(2, 3);
    Tangent(2,2,1,1) = ipvcur->_nldMaterialTensor(2, 1);
    Tangent(2,2,1,2) = ipvcur->_nldMaterialTensor(2, 5);
    Tangent(2,2,2,0) = ipvcur->_nldMaterialTensor(2, 4);
    Tangent(2,2,2,1) = ipvcur->_nldMaterialTensor(2, 5);
    Tangent(2,2,2,2) = ipvcur->_nldMaterialTensor(2, 2);

    Tangent(0,1,0,0) = ipvcur->_nldMaterialTensor(3, 0);
    Tangent(0,1,0,1) = ipvcur->_nldMaterialTensor(3, 3);
    Tangent(0,1,0,2) = ipvcur->_nldMaterialTensor(3, 4);
    Tangent(0,1,1,0) = ipvcur->_nldMaterialTensor(3, 3);
    Tangent(0,1,1,1) = ipvcur->_nldMaterialTensor(3, 1);
    Tangent(0,1,1,2) = ipvcur->_nldMaterialTensor(3, 5);
    Tangent(0,1,2,0) = ipvcur->_nldMaterialTensor(3, 4);
    Tangent(0,1,2,1) = ipvcur->_nldMaterialTensor(3, 5);
    Tangent(0,1,2,2) = ipvcur->_nldMaterialTensor(3, 2);

    Tangent(1,0,0,0) = ipvcur->_nldMaterialTensor(3, 0);
    Tangent(1,0,0,1) = ipvcur->_nldMaterialTensor(3, 3);
    Tangent(1,0,0,2) = ipvcur->_nldMaterialTensor(3, 4);
    Tangent(1,0,1,0) = ipvcur->_nldMaterialTensor(3, 3);
    Tangent(1,0,1,1) = ipvcur->_nldMaterialTensor(3, 1);
    Tangent(1,0,1,2) = ipvcur->_nldMaterialTensor(3, 5);
    Tangent(1,0,2,0) = ipvcur->_nldMaterialTensor(3, 4);
    Tangent(1,0,2,1) = ipvcur->_nldMaterialTensor(3, 5);
    Tangent(1,0,2,2) = ipvcur->_nldMaterialTensor(3, 2);

    Tangent(0,2,0,0) = ipvcur->_nldMaterialTensor(4, 0);
    Tangent(0,2,0,1) = ipvcur->_nldMaterialTensor(4, 3);
    Tangent(0,2,0,2) = ipvcur->_nldMaterialTensor(4, 4);
    Tangent(0,2,1,0) = ipvcur->_nldMaterialTensor(4, 3);
    Tangent(0,2,1,1) = ipvcur->_nldMaterialTensor(4, 1);
    Tangent(0,2,1,2) = ipvcur->_nldMaterialTensor(4, 5);
    Tangent(0,2,2,0) = ipvcur->_nldMaterialTensor(4, 4);
    Tangent(0,2,2,1) = ipvcur->_nldMaterialTensor(4, 5);
    Tangent(0,2,2,2) = ipvcur->_nldMaterialTensor(4, 2);

    Tangent(2,0,0,0) = ipvcur->_nldMaterialTensor(4, 0);
    Tangent(2,0,0,1) = ipvcur->_nldMaterialTensor(4, 3);
    Tangent(2,0,0,2) = ipvcur->_nldMaterialTensor(4, 4);
    Tangent(2,0,1,0) = ipvcur->_nldMaterialTensor(4, 3);
    Tangent(2,0,1,1) = ipvcur->_nldMaterialTensor(4, 1);
    Tangent(2,0,1,2) = ipvcur->_nldMaterialTensor(4, 5);
    Tangent(2,0,2,0) = ipvcur->_nldMaterialTensor(4, 4);
    Tangent(2,0,2,1) = ipvcur->_nldMaterialTensor(4, 5);
    Tangent(2,0,2,2) = ipvcur->_nldMaterialTensor(4, 2);

    Tangent(2,1,0,0) = ipvcur->_nldMaterialTensor(5, 0);
    Tangent(2,1,0,1) = ipvcur->_nldMaterialTensor(5, 3);
    Tangent(2,1,0,2) = ipvcur->_nldMaterialTensor(5, 4);
    Tangent(2,1,1,0) = ipvcur->_nldMaterialTensor(5, 3);
    Tangent(2,1,1,1) = ipvcur->_nldMaterialTensor(5, 1);
    Tangent(2,1,1,2) = ipvcur->_nldMaterialTensor(5, 5);
    Tangent(2,1,2,0) = ipvcur->_nldMaterialTensor(5, 4);
    Tangent(2,1,2,1) = ipvcur->_nldMaterialTensor(5, 5);
    Tangent(2,1,2,2) = ipvcur->_nldMaterialTensor(5, 2);

    Tangent(1,2,0,0) = ipvcur->_nldMaterialTensor(5, 0);
    Tangent(1,2,0,1) = ipvcur->_nldMaterialTensor(5, 3);
    Tangent(1,2,0,2) = ipvcur->_nldMaterialTensor(5, 4);
    Tangent(1,2,1,0) = ipvcur->_nldMaterialTensor(5, 3);
    Tangent(1,2,1,1) = ipvcur->_nldMaterialTensor(5, 1);
    Tangent(1,2,1,2) = ipvcur->_nldMaterialTensor(5, 5);
    Tangent(1,2,2,0) = ipvcur->_nldMaterialTensor(5, 4);
    Tangent(1,2,2,1) = ipvcur->_nldMaterialTensor(5, 5);
    Tangent(1,2,2,2) = ipvcur->_nldMaterialTensor(5, 2);
   }

#if 0
  // TO BE IMPLEMENTED: elasticity to try
  double _E = props[7];
  double _nu = props[8];
  double FACT = _E / (1 + _nu);
  double C11 = FACT * (1 - _nu) / (1 - 2 * _nu);
  double C12 = FACT * _nu / (1 - 2 * _nu);
  double C44 = (C11 - C12);
  fullMatrix<double > *H = &ipvcur->MaterialTensor;
  H->scale(0.);
  // This tensor is for gamma in the strain field (gamma is stored)
  for(int i = 0; i < 3; i++) { H->operator()(i, i) = C11; H->operator()(i + 3, i + 3) = C44; }
  H->operator()(1, 0) = H->operator()(0, 1) = H->operator()(2, 0) = H->operator()(0, 2) = H->operator()(1, 2) = H->operator()(2, 1) = C12;

  ipvcur->stress.scale(0.);
  for (int i=0; i < 6; i++)
  {
    for(int j=0; j < 6; j++)
    {
      ipvcur->stress(i) +=H->operator()(i, j) *ipvcur->strain(j);

    }
  }
  ipvcur->effectivePlasticStrain = 0.;
  ipvcur->currentPlasticStrain = 0.;
  ipvcur->damage = 0.;
#endif


    ipvcur->_elasticEne = mat->getElasticEnergy(statev_1);
    ipvcur->_plasticEne = mat->getPlasticEnergy(statev_1);

    // we use plastic energy = plastenergy_n + sum Delta eps:(sigma_n+1+sigma_n)2.  
    // dissipated energy = plastic energy - 1/2 [(sigma_n+1 : Cele^-1 : sigma_n+1   ]
    ipvcur->_stressWork=ipvprev->_stressWork;
    for (int i=0; i < 6; i++)
    {
      ipvcur->_stressWork+= 0.5*(strs[i]+strs_n[i])*dstrn[i];
    /*if(STensorOperation::isnan(strs[i])) 
      Msg::Error("strs[i] is nan");
    if(STensorOperation::isnan(strs_n[i])) 
      Msg::Error("strs_n[i] is nan");
    if(STensorOperation::isnan(dstrn[i])) 
      Msg::Error("dstrn[i] is nan");*/
    }
    mat->get_elOp(statev_1,C0PathFollowing);
    int errorInv=0;
    inverse(C0PathFollowing,invC0PathFollowing,&errorInv,6);

    double &disEne=ipvcur->getRefToIrreversibleEnergy();
    STensor3& DirrEnergDF = ipvcur->getRefToDIrreversibleEnergyDF();
    double &dIrrDpTilde = ipvcur->getRefToDIrreversibleEnergyDNonLocalVariable(0);

    //elastic strain
    for (int i=0; i < 6; i++)
    {
      elStrainPathFollowing[i]=0.;
      for(int j=0; j < 6; j++)
      {
        elStrainPathFollowing[i] +=invC0PathFollowing[i][j] *strs[j];
      }
    }
    disEne=ipvcur->_stressWork;
    dIrrDpTilde=0.;
    if( mat->get_pos_Fd_bar()>0)
    {
      double &dIrrDdamTilde = ipvcur->getRefToDIrreversibleEnergyDNonLocalVariable(1);
      dIrrDdamTilde=0.;
    }
    for (int i=0; i < 6; i++)
    {
      disEne-=0.5*elStrainPathFollowing[i]*strs[i];
      dEnePathFollowingdStrain[i]=(strs[i]+strs_n[i])*0.5;
      for(int j=0; j < 6; j++)
      {
        dEnePathFollowingdStrain[i] += Calgo[j][i] *(0.5*dstrn[j]-elStrainPathFollowing[j]);
      }
      dIrrDpTilde+= strs_dDdp_bar[i]*(0.5*dstrn[i]-elStrainPathFollowing[i]);// dsigma d p tilde
      if( mat->get_pos_Fd_bar()>0)
      {
        double &dIrrDdamTilde = ipvcur->getRefToDIrreversibleEnergyDNonLocalVariable(1);
        dIrrDdamTilde+=dstrs_dFd_bar[i]*(0.5*dstrn[i]-elStrainPathFollowing[i]); //d sigma d d tilde d
      }
    }
    for(int i=0;i<3;i++)
    {
      DirrEnergDF(i,i) = dEnePathFollowingdStrain[i];
    }
    DirrEnergDF(0,1) = dEnePathFollowingdStrain[3]/sq2;
    DirrEnergDF(1,0) = dEnePathFollowingdStrain[3]/sq2;
    DirrEnergDF(0,2) = dEnePathFollowingdStrain[4]/sq2;
    DirrEnergDF(2,0) = dEnePathFollowingdStrain[4]/sq2;
    DirrEnergDF(1,2) = dEnePathFollowingdStrain[5]/sq2;
    DirrEnergDF(2,1) = dEnePathFollowingdStrain[5]/sq2;
    
    STensor43 Cel, Sel;
    ElasticStiffness(statev_1,&Cel);
    STensorOperation::inverseSTensor43(Cel,Sel);     
    static STensor3 I(1.);
    for (int i=0; i<3; i++){
      for (int j=0; j<3; j++){
        eigenstrain(i,j) =   0.5*(Fn(i,j)+Fn(j,i)) - I(i,j);
      }
    }
    STensorOperation::multSTensor43STensor3Add(Sel,P,-1.,eigenstrain);
    STensorOperation::multSTensor43STensor3(Cel,eigenstrain,eigenstress);
    eigenstress *= -1.;

/*  if(STensorOperation::isnan(disEne)) 
    Msg::Error("disEne is nan");
  if(STensorOperation::isnan(DirrEnergDF)) 
    Msg::Error("DirrEnergDF is nan");

  if(STensorOperation::isnan(dIrrDpTilde)) 
    Msg::Error("dIrrDpTilde is nan");
  if( mat->get_pos_Fd_bar()>0)
  {
      double &dIrrDdamTilde = ipvcur->getRefToDIrreversibleEnergyDNonLocalVariable(1);
      if(STensorOperation::isnan(dIrrDdamTilde)) 
      Msg::Error("dIrrDdamTilde is nan");
  }*/
  //if(disEne>1.e-13)
  //{
  //  Msg::Info("dissipated energy %f",disEne);
  //}

/*  if (this->getMacroSolver()->getPathFollowingLocalIncrementType() == pathFollowingManager::DEFO_ENERGY){
    ipvcur->getRefToIrreversibleEnergy() = ipvcur->_elasticEne;
  }
  else if (this->getMacroSolver()->getPathFollowingLocalIncrementType() == pathFollowingManager::PLASTIC_ENERGY) {
    ipvcur->getRefToIrreversibleEnergy() = ipvcur->_plasticEne;
  }
  else if (this->getMacroSolver()->getPathFollowingLocalIncrementType() == pathFollowingManager::DAMAGE_ENERGY){
    ipvcur->getRefToIrreversibleEnergy() = ipvcur->_damageEne;
  }
  else if (this->getMacroSolver()->getPathFollowingLocalIncrementType() == pathFollowingManager::DISSIPATION_ENERGY){
    ipvcur->getRefToIrreversibleEnergy() = ipvcur->_plasticEne+ipvcur->_damageEne;
  }
  else{
    ipvcur->getRefToIrreversibleEnergy() = 0.;
  }*/
  }
  else
  {
    //constbox did not converge
    P(0,0) = P(1,1) = P(2,2) = sqrt(-1.); // to exist NR and performed next step with a smaller incremenet
  }

}


void mlawNonLocalDamage::constitutiveTFA(const STensor3& F0,
                                         const STensor3& Fn,
                                         STensor3 &P,
                                         const IPVariable *ipvprevi,
                                         IPVariable *ipvcuri,
                                         STensor43 &Tangent,
                                         STensor3 &epsEig,
                                         STensor43 &depsEigdeps) const
{ 
  //const IPNonLocalDamage *ipvprev = dynamic_cast<const IPNonLocalDamage *> (ipvprevi);
  //IPNonLocalDamage *ipvcur = dynamic_cast<IPNonLocalDamage *> (ipvcuri);
  
  IPNonLocalDamage* ipvcur = static_cast<IPNonLocalDamage*>(ipvcuri);
  const IPNonLocalDamage* ipvprev = static_cast<const IPNonLocalDamage*>(ipvprevi);  
  
  STensorOperation::zero(P);
  
  STensor3& eigenstrain = ipvcur->_eigenstrain;
  STensor3& eigenstress = ipvcur->_eigenstress;

  //convert F to eps  :order xx yy zz xy xz yz
  if(!mat->isInLargeDeformation())
  {
    for(int i=0;i<3;i++)
    {
      ipvcur->_nldStrain(i) = Fn(i,i)-1.;
    }
    //gamma
    ipvcur->_nldStrain(3) = Fn(0,1)+Fn(1,0);
    ipvcur->_nldStrain(4) = Fn(0,2)+Fn(2,0);
    ipvcur->_nldStrain(5) = Fn(2,1)+Fn(1,2);
  }
  else
  {
    for(int i=0;i<3;i++)
    {
       for(int j=0;j<3;j++)
       {
         Ftmp[i][j] = Fn(i,j);
         Ftmp0[i][j] = F0(i,j);
       }
    }
    Getstrnln(Ftmp, Vstrain);
    GetdRot(Ftmp0, Ftmp, dR);
    matrix_to_vect(3, dR, &(ipvprev->_nldStatev[mat->get_pos_dR()]));
    matrix_to_vect(3, dR, &(ipvcur->_nldStatev[mat->get_pos_dR()]));

    for(int i=0;i<3;i++){
      ipvcur->_nldStrain(i) = Vstrain[i];
      //ipvcur->_nldStatev[i] = Vstrain[i];
    }
    for(int i=3;i<6;i++){
      ipvcur->_nldStrain(i) = Vstrain[i];
      //ipvcur->_nldStatev[i] = Vstrain[i]/sq2;    //**using the current state varible to pass the total strain*****
    }
  }

  //get previous strain and stress
  for(int i=0;i<3;i++){
    strn_n[i]=(ipvprev->_nldStrain)(i);
    strs_n[i]=(ipvprev->_nldStress)(i);
    dstrn[i]=(ipvcur->_nldStrain)(i)-(ipvprev->_nldStrain)(i);
  }
  for(int i=3;i<6;i++){
    strn_n[i]=(ipvprev->_nldStrain)(i)/sq2;
    dstrn[i]=((ipvcur->_nldStrain)(i)-(ipvprev->_nldStrain)(i))/sq2;
    strs_n[i]=(ipvprev->_nldStress)(i)*sq2;
  }

  //HERE WE SHOULD COPY THE PREVIOUS ONE AND NOT CHANGING IT DIRECTLY
  //statev_n = ipvprev->_nldStatev;
  copyvect(ipvprev->_nldStatev, statev_n,nsdv);


  statev_1 = ipvcur->_nldStatev;

  //SHOULD BE MORE GENERAL
  if( mat->get_pos_effectiveplasticstrainfornonlocal() >=0 )
  {
    //should be 66 (-39 for EP)
    statev_n[mat->get_pos_effectiveplasticstrainfornonlocal()] = ipvprev->_nldEffectivePlasticStrain;
    statev_n[mat->get_pos_effectiveplasticstrainfornonlocal()+1] = (ipvcur->_nldEffectivePlasticStrain-
                                                                    ipvprev->_nldEffectivePlasticStrain);
  }
  if( mat->get_pos_Fd_bar() > 0 )
  {
    statev_n[mat->get_pos_Fd_bar()] =  ipvprev->_nlFiber_d_bar;  
    statev_n[mat->get_pos_dFd_bar()] = (ipvcur->_nlFiber_d_bar - ipvprev->_nlFiber_d_bar);
  } 


  // to required to use Eshelby tensor of previous step?
  copyvect(statev_n,statev_1,nsdv);
  if (ipvcur->dissipationIsBlocked() or ipvprev->dissipationIsBlocked())
  {
    for(int nlv=0; nlv< getNbNonLocalVariables(); nlv++)
    {
      ipvcur->setCriticalDamage(statev_n,ipvprev->getDamageIndicator(nlv),nlv);
      ipvcur->setCriticalDamage(statev_1,ipvprev->getDamageIndicator(nlv),nlv);
    }
  }
  if(mat->isInLargeDeformation())
  {
     for(int i=0;i<3;i++){
         statev_1[i] = Vstrain[i];
     }
     for(int i=3;i<6;i++){
         statev_1[i] = Vstrain[i]/sq2;    //**using the current state varible to pass the total strain*****
    }

  }
  int kinc = 1;
  int kstep = 2;
  SpBar = 0.0;
  dFd_d_bar = 0.0;
  dpdFd = 0.0;
  dFddp = 0.0;
  
  int error= mat->constbox(dstrn, strs_n, strs, statev_n, statev_1, Cref, dCref, tau, dtau, Calgo, 1., dpdE, strs_dDdp_bar, &SpBar, chara_Length, &dFd_d_bar, dstrs_dFd_bar, dFd_dE, chara_Length_INC, &dpdFd, &dFddp, kinc, kstep, _timeStep);

  //recompute becasue could have changed if no convergence and is used for Irreversible energy
  for(int i=0;i<3;i++){
    strn_n[i]=(ipvprev->_nldStrain)(i);
    strs_n[i]=(ipvprev->_nldStress)(i);
    dstrn[i]=(ipvcur->_nldStrain)(i)-(ipvprev->_nldStrain)(i);
  }
  for(int i=3;i<6;i++){
    strn_n[i]=(ipvprev->_nldStrain)(i)/sq2;
    dstrn[i]=((ipvcur->_nldStrain)(i)-(ipvprev->_nldStrain)(i))/sq2;
    strs_n[i]=(ipvprev->_nldStress)(i)*sq2;
  }
  if(error==0)
  {
    //update stress and tangent operator
    updateCharacteristicLengthTensor(chara_Length, statev_1);
    updateCharacteristicLengthTensor(chara_Length_INC, statev_1);
    for(int i=0;i<3;i++)
    {
      (ipvcur->_nldStress)(i)=strs[i];
      for(int j=0;j<3;j++){
        (ipvcur->_nldMaterialTensor)(i,j)=Calgo[i][j];
  
        (ipvcur->_nldCharacteristicLengthMatrix)(i,j) = chara_Length[i][j];                 //by Wu Ling
        (ipvcur->_nldCharacteristicLengthFiber)(i,j) = chara_Length_INC[i][j];
      }
      if( mat->get_pos_damagefornonlocal()>0)
      {
        ipvcur->_nldCouplingStressEffectiveStrain(i) = strs_dDdp_bar[i];
        ipvcur->_nldCouplingEffectiveStrainStress(i) = dpdE[i];
      }
      if( mat->get_pos_Fd_bar()>0)
      {
        ipvcur->_nldFiberDamdStrain(i) = dFd_dE[i];
        ipvcur->_nldStressdFiberDam(i) = dstrs_dFd_bar[i];
      }
    }

    ipvcur->_nldSpBar = SpBar;
    ipvcur->_nldFd_d_bar = dFd_d_bar;
    ipvcur->_nldpdFd = dpdFd;
    ipvcur->_nldFddp = dFddp;

    for(int i=3;i<6;i++)
    {
      (ipvcur->_nldStress)(i)=strs[i]/sq2;
      for(int j=0;j<3;j++)
      {
        (ipvcur->_nldMaterialTensor)(i,j)=Calgo[i][j]/sq2;
        (ipvcur->_nldMaterialTensor)(j,i)=Calgo[j][i]/sq2;
      }
      for(int j=3;j<6;j++)
      {
        (ipvcur->_nldMaterialTensor)(i,j)=Calgo[i][j]/2.;
      }
      if( mat->get_pos_damagefornonlocal()>=0)
      {
        ipvcur->_nldCouplingStressEffectiveStrain(i) = strs_dDdp_bar[i]/sq2;
        ipvcur->_nldCouplingEffectiveStrainStress(i) = dpdE[i]/sq2;
      }
      if( mat->get_pos_Fd_bar()>0) 
      {
        ipvcur->_nldFiberDamdStrain(i) = dFd_dE[i]/sq2;
        ipvcur->_nldStressdFiberDam(i) = dstrs_dFd_bar[i]/sq2;
      }
    }
    if( mat->get_pos_damagefornonlocal() >= 0)
    {
      ipvcur->_nldDamage = statev_1[mat->get_pos_damagefornonlocal()];
    }

    if( mat->get_pos_currentplasticstrainfornonlocal() >=0)
    {
      ipvcur->_nldCurrentPlasticStrain = statev_1[mat->get_pos_currentplasticstrainfornonlocal()];
    }

    if( mat->get_pos_locFd() >0)
    {
      ipvcur->_nlFiber_loc = statev_1[mat->get_pos_locFd()];
    }

    //convert str to P  :order xx yy zz xy xz yz
    for(int i=0;i<3;i++)
    {
      P(i,i) = ipvcur->_nldStress(i);
    }
    P(0,1) = ipvcur->_nldStress(3);
    P(1,0) = ipvcur->_nldStress(3);
    P(0,2) = ipvcur->_nldStress(4);
    P(2,0) = ipvcur->_nldStress(4);
    P(1,2) = ipvcur->_nldStress(5);
    P(2,1) = ipvcur->_nldStress(5);
    if(mat->isInLargeDeformation())
    {
      //What we have is actually sigma -> send to P
      static STensor3 Finv, FinvT;
      STensorOperation::inverseSTensor3(Fn, Finv);
      STensorOperation::transposeSTensor3(Finv, FinvT);
      P*=FinvT;
      P*=Fn.determinant();
    }

    //convert MaterialTensor to Tangent  :order xx yy zz xy xz yz
    Tangent(0,0,0,0) = ipvcur->_nldMaterialTensor(0, 0);
    Tangent(0,0,0,1) = ipvcur->_nldMaterialTensor(0, 3);
    Tangent(0,0,0,2) = ipvcur->_nldMaterialTensor(0, 4);
    Tangent(0,0,1,0) = ipvcur->_nldMaterialTensor(0, 3);
    Tangent(0,0,1,1) = ipvcur->_nldMaterialTensor(0, 1);
    Tangent(0,0,1,2) = ipvcur->_nldMaterialTensor(0, 5);
    Tangent(0,0,2,0) = ipvcur->_nldMaterialTensor(0, 4);
    Tangent(0,0,2,1) = ipvcur->_nldMaterialTensor(0, 5);
    Tangent(0,0,2,2) = ipvcur->_nldMaterialTensor(0, 2);

    Tangent(1,1,0,0) = ipvcur->_nldMaterialTensor(1, 0);  
    Tangent(1,1,0,1) = ipvcur->_nldMaterialTensor(1, 3);
    Tangent(1,1,0,2) = ipvcur->_nldMaterialTensor(1, 4);
    Tangent(1,1,1,0) = ipvcur->_nldMaterialTensor(1, 3);
    Tangent(1,1,1,1) = ipvcur->_nldMaterialTensor(1, 1);
    Tangent(1,1,1,2) = ipvcur->_nldMaterialTensor(1, 5);
    Tangent(1,1,2,0) = ipvcur->_nldMaterialTensor(1, 4);
    Tangent(1,1,2,1) = ipvcur->_nldMaterialTensor(1, 5);
    Tangent(1,1,2,2) = ipvcur->_nldMaterialTensor(1, 2);

    Tangent(2,2,0,0) = ipvcur->_nldMaterialTensor(2, 0);  
    Tangent(2,2,0,1) = ipvcur->_nldMaterialTensor(2, 3);
    Tangent(2,2,0,2) = ipvcur->_nldMaterialTensor(2, 4);
    Tangent(2,2,1,0) = ipvcur->_nldMaterialTensor(2, 3);
    Tangent(2,2,1,1) = ipvcur->_nldMaterialTensor(2, 1);
    Tangent(2,2,1,2) = ipvcur->_nldMaterialTensor(2, 5);
    Tangent(2,2,2,0) = ipvcur->_nldMaterialTensor(2, 4);
    Tangent(2,2,2,1) = ipvcur->_nldMaterialTensor(2, 5);
    Tangent(2,2,2,2) = ipvcur->_nldMaterialTensor(2, 2);

    Tangent(0,1,0,0) = ipvcur->_nldMaterialTensor(3, 0);
    Tangent(0,1,0,1) = ipvcur->_nldMaterialTensor(3, 3);
    Tangent(0,1,0,2) = ipvcur->_nldMaterialTensor(3, 4);
    Tangent(0,1,1,0) = ipvcur->_nldMaterialTensor(3, 3);
    Tangent(0,1,1,1) = ipvcur->_nldMaterialTensor(3, 1);
    Tangent(0,1,1,2) = ipvcur->_nldMaterialTensor(3, 5);
    Tangent(0,1,2,0) = ipvcur->_nldMaterialTensor(3, 4);
    Tangent(0,1,2,1) = ipvcur->_nldMaterialTensor(3, 5);
    Tangent(0,1,2,2) = ipvcur->_nldMaterialTensor(3, 2);

    Tangent(1,0,0,0) = ipvcur->_nldMaterialTensor(3, 0);
    Tangent(1,0,0,1) = ipvcur->_nldMaterialTensor(3, 3);
    Tangent(1,0,0,2) = ipvcur->_nldMaterialTensor(3, 4);
    Tangent(1,0,1,0) = ipvcur->_nldMaterialTensor(3, 3);
    Tangent(1,0,1,1) = ipvcur->_nldMaterialTensor(3, 1);
    Tangent(1,0,1,2) = ipvcur->_nldMaterialTensor(3, 5);
    Tangent(1,0,2,0) = ipvcur->_nldMaterialTensor(3, 4);
    Tangent(1,0,2,1) = ipvcur->_nldMaterialTensor(3, 5);
    Tangent(1,0,2,2) = ipvcur->_nldMaterialTensor(3, 2);

    Tangent(0,2,0,0) = ipvcur->_nldMaterialTensor(4, 0);
    Tangent(0,2,0,1) = ipvcur->_nldMaterialTensor(4, 3);
    Tangent(0,2,0,2) = ipvcur->_nldMaterialTensor(4, 4);
    Tangent(0,2,1,0) = ipvcur->_nldMaterialTensor(4, 3);
    Tangent(0,2,1,1) = ipvcur->_nldMaterialTensor(4, 1);
    Tangent(0,2,1,2) = ipvcur->_nldMaterialTensor(4, 5);
    Tangent(0,2,2,0) = ipvcur->_nldMaterialTensor(4, 4);
    Tangent(0,2,2,1) = ipvcur->_nldMaterialTensor(4, 5);
    Tangent(0,2,2,2) = ipvcur->_nldMaterialTensor(4, 2);

    Tangent(2,0,0,0) = ipvcur->_nldMaterialTensor(4, 0);
    Tangent(2,0,0,1) = ipvcur->_nldMaterialTensor(4, 3);
    Tangent(2,0,0,2) = ipvcur->_nldMaterialTensor(4, 4);
    Tangent(2,0,1,0) = ipvcur->_nldMaterialTensor(4, 3);
    Tangent(2,0,1,1) = ipvcur->_nldMaterialTensor(4, 1);
    Tangent(2,0,1,2) = ipvcur->_nldMaterialTensor(4, 5);
    Tangent(2,0,2,0) = ipvcur->_nldMaterialTensor(4, 4);
    Tangent(2,0,2,1) = ipvcur->_nldMaterialTensor(4, 5);
    Tangent(2,0,2,2) = ipvcur->_nldMaterialTensor(4, 2);

    Tangent(2,1,0,0) = ipvcur->_nldMaterialTensor(5, 0);
    Tangent(2,1,0,1) = ipvcur->_nldMaterialTensor(5, 3);
    Tangent(2,1,0,2) = ipvcur->_nldMaterialTensor(5, 4);
    Tangent(2,1,1,0) = ipvcur->_nldMaterialTensor(5, 3);
    Tangent(2,1,1,1) = ipvcur->_nldMaterialTensor(5, 1);
    Tangent(2,1,1,2) = ipvcur->_nldMaterialTensor(5, 5);
    Tangent(2,1,2,0) = ipvcur->_nldMaterialTensor(5, 4);
    Tangent(2,1,2,1) = ipvcur->_nldMaterialTensor(5, 5);
    Tangent(2,1,2,2) = ipvcur->_nldMaterialTensor(5, 2);

    Tangent(1,2,0,0) = ipvcur->_nldMaterialTensor(5, 0);
    Tangent(1,2,0,1) = ipvcur->_nldMaterialTensor(5, 3);
    Tangent(1,2,0,2) = ipvcur->_nldMaterialTensor(5, 4);
    Tangent(1,2,1,0) = ipvcur->_nldMaterialTensor(5, 3);
    Tangent(1,2,1,1) = ipvcur->_nldMaterialTensor(5, 1);
    Tangent(1,2,1,2) = ipvcur->_nldMaterialTensor(5, 5);
    Tangent(1,2,2,0) = ipvcur->_nldMaterialTensor(5, 4);
    Tangent(1,2,2,1) = ipvcur->_nldMaterialTensor(5, 5);
    Tangent(1,2,2,2) = ipvcur->_nldMaterialTensor(5, 2);
    
    /*fullMatrix<double> Calgo_fullMatrix(6,6);
    for(int i=0;i<6;i++)
    {
      for(int j=0;j<6;j++){
        Calgo_fullMatrix(i,j)=Calgo[i][j];
      }
    }
    for(int i=3;i<6;i++)
    {
      for(int j=0;j<3;j++)
      {
        Calgo_fullMatrix(i,j)*=(1./sq2);
        Calgo_fullMatrix(j,i)*=2.;
        Calgo_fullMatrix(j,i)*=(1./sq2);
      }
    }   
    STensorOperation::fromFullMatrixToSTensor43(Calgo_fullMatrix,Tangent);*/


    ipvcur->_elasticEne = mat->getElasticEnergy(statev_1);
    ipvcur->_plasticEne = mat->getPlasticEnergy(statev_1);

    // we use plastic energy = plastenergy_n + sum Delta eps:(sigma_n+1+sigma_n)2.  
    // dissipated energy = plastic energy - 1/2 [(sigma_n+1 : Cele^-1 : sigma_n+1   ]
    ipvcur->_stressWork=ipvprev->_stressWork;
    for (int i=0; i < 6; i++)
    {
      ipvcur->_stressWork+= 0.5*(strs[i]+strs_n[i])*dstrn[i];
    }
    mat->get_elOp(statev_1,C0PathFollowing);
    int errorInv=0;
    inverse(C0PathFollowing,invC0PathFollowing,&errorInv,6);

    double &disEne=ipvcur->getRefToIrreversibleEnergy();
    STensor3& DirrEnergDF = ipvcur->getRefToDIrreversibleEnergyDF();
    double &dIrrDpTilde = ipvcur->getRefToDIrreversibleEnergyDNonLocalVariable(0);

    //elastic strain
    for (int i=0; i < 6; i++)
    {
      elStrainPathFollowing[i]=0.;
      for(int j=0; j < 6; j++)
      {
        elStrainPathFollowing[i] +=invC0PathFollowing[i][j] *strs[j];
      }
    }
    disEne=ipvcur->_stressWork;
    dIrrDpTilde=0.;
    if( mat->get_pos_Fd_bar()>0)
    {
      double &dIrrDdamTilde = ipvcur->getRefToDIrreversibleEnergyDNonLocalVariable(1);
      dIrrDdamTilde=0.;
    }
    for (int i=0; i < 6; i++)
    {
      disEne-=0.5*elStrainPathFollowing[i]*strs[i];
      dEnePathFollowingdStrain[i]=(strs[i]+strs_n[i])*0.5;
      for(int j=0; j < 6; j++)
      {
        dEnePathFollowingdStrain[i] += Calgo[j][i] *(0.5*dstrn[j]-elStrainPathFollowing[j]);
      }
      dIrrDpTilde+= strs_dDdp_bar[i]*(0.5*dstrn[i]-elStrainPathFollowing[i]);// dsigma d p tilde
      if( mat->get_pos_Fd_bar()>0)
      {
        double &dIrrDdamTilde = ipvcur->getRefToDIrreversibleEnergyDNonLocalVariable(1);
        dIrrDdamTilde+=dstrs_dFd_bar[i]*(0.5*dstrn[i]-elStrainPathFollowing[i]); //d sigma d d tilde d
      }
    }
    for(int i=0;i<3;i++)
    {
      DirrEnergDF(i,i) = dEnePathFollowingdStrain[i];
    }
    DirrEnergDF(0,1) = dEnePathFollowingdStrain[3]/sq2;
    DirrEnergDF(1,0) = dEnePathFollowingdStrain[3]/sq2;
    DirrEnergDF(0,2) = dEnePathFollowingdStrain[4]/sq2;
    DirrEnergDF(2,0) = dEnePathFollowingdStrain[4]/sq2;
    DirrEnergDF(1,2) = dEnePathFollowingdStrain[5]/sq2;
    DirrEnergDF(2,1) = dEnePathFollowingdStrain[5]/sq2;
    
    STensor43 Cel, Sel;
    ElasticStiffness(&Cel);
    STensorOperation::inverseSTensor43(Cel,Sel);
      
    STensor3 eps;
    static STensor3 I(1.);
    for (int i=0; i<3; i++){
      for (int j=0; j<3; j++){
        eps(i,j) =   0.5*(Fn(i,j)+Fn(j,i)) - I(i,j);
      }
    }
    epsEig = eps;
    STensorOperation::multSTensor43STensor3Add(Sel,P,-1.,epsEig);
    eigenstrain = epsEig;
    STensorOperation::multSTensor43STensor3(Cel,eigenstrain,eigenstress);
    eigenstress *= -1.;
    STensorOperation::unity(depsEigdeps);
    STensorOperation::multSTensor43Add(Sel,Tangent,-1.,depsEigdeps);  
  }
  else
  {
    //constbox did not converge
    P(0,0) = P(1,1) = P(2,2) = sqrt(-1.); // to exist NR and performed next step with a smaller incremenet
  }
}



void mlawNonLocalDamage::constitutiveTFA_incOri(const STensor3& F0,
                                         const STensor3& Fn,
                                         const fullVector<double>& euler,
                                         STensor3 &P,
                                         const IPVariable *ipvprevi,
                                         IPVariable *ipvcuri,
                                         STensor43 &Tangent,
                                         STensor3 &epsEig,
                                         STensor43 &depsEigdeps) const
{ 
  //const IPNonLocalDamage *ipvprev = dynamic_cast<const IPNonLocalDamage *> (ipvprevi);
  //IPNonLocalDamage *ipvcur = dynamic_cast<IPNonLocalDamage *> (ipvcuri);
  
  IPNonLocalDamage* ipvcur = static_cast<IPNonLocalDamage*>(ipvcuri);
  const IPNonLocalDamage* ipvprev = static_cast<const IPNonLocalDamage*>(ipvprevi);  
  
  double e1 = euler(0);
  double e2 = euler(1);
  double e3 = euler(2);
  mat->setEuler(e1,e2,e3);
  
  STensorOperation::zero(P);
  
  STensor3& eigenstrain = ipvcur->_eigenstrain;
  STensor3& eigenstress = ipvcur->_eigenstress;

  //convert F to eps  :order xx yy zz xy xz yz
  if(!mat->isInLargeDeformation())
  {
    for(int i=0;i<3;i++)
    {
      ipvcur->_nldStrain(i) = Fn(i,i)-1.;
    }
    //gamma
    ipvcur->_nldStrain(3) = Fn(0,1)+Fn(1,0);
    ipvcur->_nldStrain(4) = Fn(0,2)+Fn(2,0);
    ipvcur->_nldStrain(5) = Fn(2,1)+Fn(1,2);
  }
  else
  {
    for(int i=0;i<3;i++)
    {
       for(int j=0;j<3;j++)
       {
         Ftmp[i][j] = Fn(i,j);
         Ftmp0[i][j] = F0(i,j);
       }
    }
    Getstrnln(Ftmp, Vstrain);
    GetdRot(Ftmp0, Ftmp, dR);
    matrix_to_vect(3, dR, &(ipvprev->_nldStatev[mat->get_pos_dR()]));
    matrix_to_vect(3, dR, &(ipvcur->_nldStatev[mat->get_pos_dR()]));

    for(int i=0;i<3;i++){
      ipvcur->_nldStrain(i) = Vstrain[i];
      //ipvcur->_nldStatev[i] = Vstrain[i];
    }
    for(int i=3;i<6;i++){
      ipvcur->_nldStrain(i) = Vstrain[i];
      //ipvcur->_nldStatev[i] = Vstrain[i]/sq2;    //**using the current state varible to pass the total strain*****
    }
  }

  //get previous strain and stress
  for(int i=0;i<3;i++){
    strn_n[i]=(ipvprev->_nldStrain)(i);
    strs_n[i]=(ipvprev->_nldStress)(i);
    dstrn[i]=(ipvcur->_nldStrain)(i)-(ipvprev->_nldStrain)(i);
  }
  for(int i=3;i<6;i++){
    strn_n[i]=(ipvprev->_nldStrain)(i)/sq2;
    dstrn[i]=((ipvcur->_nldStrain)(i)-(ipvprev->_nldStrain)(i))/sq2;
    strs_n[i]=(ipvprev->_nldStress)(i)*sq2;
  }

  //HERE WE SHOULD COPY THE PREVIOUS ONE AND NOT CHANGING IT DIRECTLY
  //statev_n = ipvprev->_nldStatev;
  copyvect(ipvprev->_nldStatev, statev_n,nsdv);


  statev_1 = ipvcur->_nldStatev;

  //SHOULD BE MORE GENERAL
  if( mat->get_pos_effectiveplasticstrainfornonlocal() >=0 )
  {
    //should be 66 (-39 for EP)
    statev_n[mat->get_pos_effectiveplasticstrainfornonlocal()] = ipvprev->_nldEffectivePlasticStrain;
    statev_n[mat->get_pos_effectiveplasticstrainfornonlocal()+1] = (ipvcur->_nldEffectivePlasticStrain-
                                                                    ipvprev->_nldEffectivePlasticStrain);
  }
  if( mat->get_pos_Fd_bar() > 0 )
  {
    statev_n[mat->get_pos_Fd_bar()] =  ipvprev->_nlFiber_d_bar;  
    statev_n[mat->get_pos_dFd_bar()] = (ipvcur->_nlFiber_d_bar - ipvprev->_nlFiber_d_bar);
  } 


  // to required to use Eshelby tensor of previous step?
  copyvect(statev_n,statev_1,nsdv);
  if (ipvcur->dissipationIsBlocked() or ipvprev->dissipationIsBlocked())
  {
    for(int nlv=0; nlv< getNbNonLocalVariables(); nlv++)
    {
      ipvcur->setCriticalDamage(statev_n,ipvprev->getDamageIndicator(nlv),nlv);
      ipvcur->setCriticalDamage(statev_1,ipvprev->getDamageIndicator(nlv),nlv);
    }
  }
  if(mat->isInLargeDeformation())
  {
     for(int i=0;i<3;i++){
         statev_1[i] = Vstrain[i];
     }
     for(int i=3;i<6;i++){
         statev_1[i] = Vstrain[i]/sq2;    //**using the current state varible to pass the total strain*****
    }

  }
  int kinc = 1;
  int kstep = 2;
  SpBar = 0.0;
  dFd_d_bar = 0.0;
  dpdFd = 0.0;
  dFddp = 0.0;
  
  int error= mat->constbox(dstrn, strs_n, strs, statev_n, statev_1, Cref, dCref, tau, dtau, Calgo, 1., dpdE, strs_dDdp_bar, &SpBar, chara_Length, &dFd_d_bar, dstrs_dFd_bar, dFd_dE, chara_Length_INC, &dpdFd, &dFddp, kinc, kstep, _timeStep);

  //recompute becasue could have changed if no convergence and is used for Irreversible energy
  for(int i=0;i<3;i++){
    strn_n[i]=(ipvprev->_nldStrain)(i);
    strs_n[i]=(ipvprev->_nldStress)(i);
    dstrn[i]=(ipvcur->_nldStrain)(i)-(ipvprev->_nldStrain)(i);
  }
  for(int i=3;i<6;i++){
    strn_n[i]=(ipvprev->_nldStrain)(i)/sq2;
    dstrn[i]=((ipvcur->_nldStrain)(i)-(ipvprev->_nldStrain)(i))/sq2;
    strs_n[i]=(ipvprev->_nldStress)(i)*sq2;
  }
  if(error==0)
  {
    //update stress and tangent operator
    updateCharacteristicLengthTensor(chara_Length, statev_1);
    updateCharacteristicLengthTensor(chara_Length_INC, statev_1);
    for(int i=0;i<3;i++)
    {
      (ipvcur->_nldStress)(i)=strs[i];
      for(int j=0;j<3;j++){
        (ipvcur->_nldMaterialTensor)(i,j)=Calgo[i][j];
  
        (ipvcur->_nldCharacteristicLengthMatrix)(i,j) = chara_Length[i][j];                 //by Wu Ling
        (ipvcur->_nldCharacteristicLengthFiber)(i,j) = chara_Length_INC[i][j];
      }
      if( mat->get_pos_damagefornonlocal()>0)
      {
        ipvcur->_nldCouplingStressEffectiveStrain(i) = strs_dDdp_bar[i];
        ipvcur->_nldCouplingEffectiveStrainStress(i) = dpdE[i];
      }
      if( mat->get_pos_Fd_bar()>0)
      {
        ipvcur->_nldFiberDamdStrain(i) = dFd_dE[i];
        ipvcur->_nldStressdFiberDam(i) = dstrs_dFd_bar[i];
      }
    }

    ipvcur->_nldSpBar = SpBar;
    ipvcur->_nldFd_d_bar = dFd_d_bar;
    ipvcur->_nldpdFd = dpdFd;
    ipvcur->_nldFddp = dFddp;

    for(int i=3;i<6;i++)
    {
      (ipvcur->_nldStress)(i)=strs[i]/sq2;
      for(int j=0;j<3;j++)
      {
        (ipvcur->_nldMaterialTensor)(i,j)=Calgo[i][j]/sq2;
        (ipvcur->_nldMaterialTensor)(j,i)=Calgo[j][i]/sq2;
      }
      for(int j=3;j<6;j++)
      {
        (ipvcur->_nldMaterialTensor)(i,j)=Calgo[i][j]/2.;
      }
      if( mat->get_pos_damagefornonlocal()>=0)
      {
        ipvcur->_nldCouplingStressEffectiveStrain(i) = strs_dDdp_bar[i]/sq2;
        ipvcur->_nldCouplingEffectiveStrainStress(i) = dpdE[i]/sq2;
      }
      if( mat->get_pos_Fd_bar()>0) 
      {
        ipvcur->_nldFiberDamdStrain(i) = dFd_dE[i]/sq2;
        ipvcur->_nldStressdFiberDam(i) = dstrs_dFd_bar[i]/sq2;
      }
    }
    if( mat->get_pos_damagefornonlocal() >= 0)
    {
      ipvcur->_nldDamage = statev_1[mat->get_pos_damagefornonlocal()];
    }

    if( mat->get_pos_currentplasticstrainfornonlocal() >=0)
    {
      ipvcur->_nldCurrentPlasticStrain = statev_1[mat->get_pos_currentplasticstrainfornonlocal()];
    }

    if( mat->get_pos_locFd() >0)
    {
      ipvcur->_nlFiber_loc = statev_1[mat->get_pos_locFd()];
    }

    //convert str to P  :order xx yy zz xy xz yz
    for(int i=0;i<3;i++)
    {
      P(i,i) = ipvcur->_nldStress(i);
    }
    P(0,1) = ipvcur->_nldStress(3);
    P(1,0) = ipvcur->_nldStress(3);
    P(0,2) = ipvcur->_nldStress(4);
    P(2,0) = ipvcur->_nldStress(4);
    P(1,2) = ipvcur->_nldStress(5);
    P(2,1) = ipvcur->_nldStress(5);
    if(mat->isInLargeDeformation())
    {
      //What we have is actually sigma -> send to P
      static STensor3 Finv, FinvT;
      STensorOperation::inverseSTensor3(Fn, Finv);
      STensorOperation::transposeSTensor3(Finv, FinvT);
      P*=FinvT;
      P*=Fn.determinant();
    }

    //convert MaterialTensor to Tangent  :order xx yy zz xy xz yz
    Tangent(0,0,0,0) = ipvcur->_nldMaterialTensor(0, 0);
    Tangent(0,0,0,1) = ipvcur->_nldMaterialTensor(0, 3);
    Tangent(0,0,0,2) = ipvcur->_nldMaterialTensor(0, 4);
    Tangent(0,0,1,0) = ipvcur->_nldMaterialTensor(0, 3);
    Tangent(0,0,1,1) = ipvcur->_nldMaterialTensor(0, 1);
    Tangent(0,0,1,2) = ipvcur->_nldMaterialTensor(0, 5);
    Tangent(0,0,2,0) = ipvcur->_nldMaterialTensor(0, 4);
    Tangent(0,0,2,1) = ipvcur->_nldMaterialTensor(0, 5);
    Tangent(0,0,2,2) = ipvcur->_nldMaterialTensor(0, 2);

    Tangent(1,1,0,0) = ipvcur->_nldMaterialTensor(1, 0);  
    Tangent(1,1,0,1) = ipvcur->_nldMaterialTensor(1, 3);
    Tangent(1,1,0,2) = ipvcur->_nldMaterialTensor(1, 4);
    Tangent(1,1,1,0) = ipvcur->_nldMaterialTensor(1, 3);
    Tangent(1,1,1,1) = ipvcur->_nldMaterialTensor(1, 1);
    Tangent(1,1,1,2) = ipvcur->_nldMaterialTensor(1, 5);
    Tangent(1,1,2,0) = ipvcur->_nldMaterialTensor(1, 4);
    Tangent(1,1,2,1) = ipvcur->_nldMaterialTensor(1, 5);
    Tangent(1,1,2,2) = ipvcur->_nldMaterialTensor(1, 2);

    Tangent(2,2,0,0) = ipvcur->_nldMaterialTensor(2, 0);  
    Tangent(2,2,0,1) = ipvcur->_nldMaterialTensor(2, 3);
    Tangent(2,2,0,2) = ipvcur->_nldMaterialTensor(2, 4);
    Tangent(2,2,1,0) = ipvcur->_nldMaterialTensor(2, 3);
    Tangent(2,2,1,1) = ipvcur->_nldMaterialTensor(2, 1);
    Tangent(2,2,1,2) = ipvcur->_nldMaterialTensor(2, 5);
    Tangent(2,2,2,0) = ipvcur->_nldMaterialTensor(2, 4);
    Tangent(2,2,2,1) = ipvcur->_nldMaterialTensor(2, 5);
    Tangent(2,2,2,2) = ipvcur->_nldMaterialTensor(2, 2);

    Tangent(0,1,0,0) = ipvcur->_nldMaterialTensor(3, 0);
    Tangent(0,1,0,1) = ipvcur->_nldMaterialTensor(3, 3);
    Tangent(0,1,0,2) = ipvcur->_nldMaterialTensor(3, 4);
    Tangent(0,1,1,0) = ipvcur->_nldMaterialTensor(3, 3);
    Tangent(0,1,1,1) = ipvcur->_nldMaterialTensor(3, 1);
    Tangent(0,1,1,2) = ipvcur->_nldMaterialTensor(3, 5);
    Tangent(0,1,2,0) = ipvcur->_nldMaterialTensor(3, 4);
    Tangent(0,1,2,1) = ipvcur->_nldMaterialTensor(3, 5);
    Tangent(0,1,2,2) = ipvcur->_nldMaterialTensor(3, 2);

    Tangent(1,0,0,0) = ipvcur->_nldMaterialTensor(3, 0);
    Tangent(1,0,0,1) = ipvcur->_nldMaterialTensor(3, 3);
    Tangent(1,0,0,2) = ipvcur->_nldMaterialTensor(3, 4);
    Tangent(1,0,1,0) = ipvcur->_nldMaterialTensor(3, 3);
    Tangent(1,0,1,1) = ipvcur->_nldMaterialTensor(3, 1);
    Tangent(1,0,1,2) = ipvcur->_nldMaterialTensor(3, 5);
    Tangent(1,0,2,0) = ipvcur->_nldMaterialTensor(3, 4);
    Tangent(1,0,2,1) = ipvcur->_nldMaterialTensor(3, 5);
    Tangent(1,0,2,2) = ipvcur->_nldMaterialTensor(3, 2);

    Tangent(0,2,0,0) = ipvcur->_nldMaterialTensor(4, 0);
    Tangent(0,2,0,1) = ipvcur->_nldMaterialTensor(4, 3);
    Tangent(0,2,0,2) = ipvcur->_nldMaterialTensor(4, 4);
    Tangent(0,2,1,0) = ipvcur->_nldMaterialTensor(4, 3);
    Tangent(0,2,1,1) = ipvcur->_nldMaterialTensor(4, 1);
    Tangent(0,2,1,2) = ipvcur->_nldMaterialTensor(4, 5);
    Tangent(0,2,2,0) = ipvcur->_nldMaterialTensor(4, 4);
    Tangent(0,2,2,1) = ipvcur->_nldMaterialTensor(4, 5);
    Tangent(0,2,2,2) = ipvcur->_nldMaterialTensor(4, 2);

    Tangent(2,0,0,0) = ipvcur->_nldMaterialTensor(4, 0);
    Tangent(2,0,0,1) = ipvcur->_nldMaterialTensor(4, 3);
    Tangent(2,0,0,2) = ipvcur->_nldMaterialTensor(4, 4);
    Tangent(2,0,1,0) = ipvcur->_nldMaterialTensor(4, 3);
    Tangent(2,0,1,1) = ipvcur->_nldMaterialTensor(4, 1);
    Tangent(2,0,1,2) = ipvcur->_nldMaterialTensor(4, 5);
    Tangent(2,0,2,0) = ipvcur->_nldMaterialTensor(4, 4);
    Tangent(2,0,2,1) = ipvcur->_nldMaterialTensor(4, 5);
    Tangent(2,0,2,2) = ipvcur->_nldMaterialTensor(4, 2);

    Tangent(2,1,0,0) = ipvcur->_nldMaterialTensor(5, 0);
    Tangent(2,1,0,1) = ipvcur->_nldMaterialTensor(5, 3);
    Tangent(2,1,0,2) = ipvcur->_nldMaterialTensor(5, 4);
    Tangent(2,1,1,0) = ipvcur->_nldMaterialTensor(5, 3);
    Tangent(2,1,1,1) = ipvcur->_nldMaterialTensor(5, 1);
    Tangent(2,1,1,2) = ipvcur->_nldMaterialTensor(5, 5);
    Tangent(2,1,2,0) = ipvcur->_nldMaterialTensor(5, 4);
    Tangent(2,1,2,1) = ipvcur->_nldMaterialTensor(5, 5);
    Tangent(2,1,2,2) = ipvcur->_nldMaterialTensor(5, 2);

    Tangent(1,2,0,0) = ipvcur->_nldMaterialTensor(5, 0);
    Tangent(1,2,0,1) = ipvcur->_nldMaterialTensor(5, 3);
    Tangent(1,2,0,2) = ipvcur->_nldMaterialTensor(5, 4);
    Tangent(1,2,1,0) = ipvcur->_nldMaterialTensor(5, 3);
    Tangent(1,2,1,1) = ipvcur->_nldMaterialTensor(5, 1);
    Tangent(1,2,1,2) = ipvcur->_nldMaterialTensor(5, 5);
    Tangent(1,2,2,0) = ipvcur->_nldMaterialTensor(5, 4);
    Tangent(1,2,2,1) = ipvcur->_nldMaterialTensor(5, 5);
    Tangent(1,2,2,2) = ipvcur->_nldMaterialTensor(5, 2);
    
    /*fullMatrix<double> Calgo_fullMatrix(6,6);
    for(int i=0;i<6;i++)
    {
      for(int j=0;j<6;j++){
        Calgo_fullMatrix(i,j)=Calgo[i][j];
      }
    }
    for(int i=3;i<6;i++)
    {
      for(int j=0;j<3;j++)
      {
        Calgo_fullMatrix(i,j)*=(1./sq2);
        Calgo_fullMatrix(j,i)*=2.;
        Calgo_fullMatrix(j,i)*=(1./sq2);
      }
    }   
    STensorOperation::fromFullMatrixToSTensor43(Calgo_fullMatrix,Tangent);*/


    ipvcur->_elasticEne = mat->getElasticEnergy(statev_1);
    ipvcur->_plasticEne = mat->getPlasticEnergy(statev_1);

    // we use plastic energy = plastenergy_n + sum Delta eps:(sigma_n+1+sigma_n)2.  
    // dissipated energy = plastic energy - 1/2 [(sigma_n+1 : Cele^-1 : sigma_n+1   ]
    ipvcur->_stressWork=ipvprev->_stressWork;
    for (int i=0; i < 6; i++)
    {
      ipvcur->_stressWork+= 0.5*(strs[i]+strs_n[i])*dstrn[i];
    }
    mat->get_elOp(statev_1,C0PathFollowing);
    int errorInv=0;
    inverse(C0PathFollowing,invC0PathFollowing,&errorInv,6);

    double &disEne=ipvcur->getRefToIrreversibleEnergy();
    STensor3& DirrEnergDF = ipvcur->getRefToDIrreversibleEnergyDF();
    double &dIrrDpTilde = ipvcur->getRefToDIrreversibleEnergyDNonLocalVariable(0);

    //elastic strain
    for (int i=0; i < 6; i++)
    {
      elStrainPathFollowing[i]=0.;
      for(int j=0; j < 6; j++)
      {
        elStrainPathFollowing[i] +=invC0PathFollowing[i][j] *strs[j];
      }
    }
    disEne=ipvcur->_stressWork;
    dIrrDpTilde=0.;
    if( mat->get_pos_Fd_bar()>0)
    {
      double &dIrrDdamTilde = ipvcur->getRefToDIrreversibleEnergyDNonLocalVariable(1);
      dIrrDdamTilde=0.;
    }
    for (int i=0; i < 6; i++)
    {
      disEne-=0.5*elStrainPathFollowing[i]*strs[i];
      dEnePathFollowingdStrain[i]=(strs[i]+strs_n[i])*0.5;
      for(int j=0; j < 6; j++)
      {
        dEnePathFollowingdStrain[i] += Calgo[j][i] *(0.5*dstrn[j]-elStrainPathFollowing[j]);
      }
      dIrrDpTilde+= strs_dDdp_bar[i]*(0.5*dstrn[i]-elStrainPathFollowing[i]);// dsigma d p tilde
      if( mat->get_pos_Fd_bar()>0)
      {
        double &dIrrDdamTilde = ipvcur->getRefToDIrreversibleEnergyDNonLocalVariable(1);
        dIrrDdamTilde+=dstrs_dFd_bar[i]*(0.5*dstrn[i]-elStrainPathFollowing[i]); //d sigma d d tilde d
      }
    }
    for(int i=0;i<3;i++)
    {
      DirrEnergDF(i,i) = dEnePathFollowingdStrain[i];
    }
    DirrEnergDF(0,1) = dEnePathFollowingdStrain[3]/sq2;
    DirrEnergDF(1,0) = dEnePathFollowingdStrain[3]/sq2;
    DirrEnergDF(0,2) = dEnePathFollowingdStrain[4]/sq2;
    DirrEnergDF(2,0) = dEnePathFollowingdStrain[4]/sq2;
    DirrEnergDF(1,2) = dEnePathFollowingdStrain[5]/sq2;
    DirrEnergDF(2,1) = dEnePathFollowingdStrain[5]/sq2;
    
    STensor43 Cel, Sel;
    ElasticStiffness_incOri(e1,e2,e3,&Cel);
    STensorOperation::inverseSTensor43(Cel,Sel);
      
    STensor3 eps;
    static STensor3 I(1.);
    for (int i=0; i<3; i++){
      for (int j=0; j<3; j++){
        eps(i,j) =   0.5*(Fn(i,j)+Fn(j,i)) - I(i,j);
      }
    }
    epsEig = eps;
    STensorOperation::multSTensor43STensor3Add(Sel,P,-1.,epsEig);
    eigenstrain = epsEig;
    STensorOperation::multSTensor43STensor3(Cel,eigenstrain,eigenstress);
    eigenstress *= -1.;
    STensorOperation::unity(depsEigdeps);
    STensorOperation::multSTensor43Add(Sel,Tangent,-1.,depsEigdeps);  
  }
  else
  {
    //constbox did not converge
    P(0,0) = P(1,1) = P(2,2) = sqrt(-1.); // to exist NR and performed next step with a smaller incremenet
  }
}



void mlawNonLocalDamage::constitutive(
                            const STensor3& F0,         // initial deformation gradient (input @ time n)
                            const STensor3& Fn,         // updated deformation gradient (input @ time n+1)
                            STensor3 &P,                // updated 1st Piola-Kirchhoff stress tensor (output)
                                                        // contains the initial values on input
                            const IPVariable *ipvprevi,       // array of initial internal variable
                            IPVariable *ipvcuri,             // updated array of internal variable (in ipvcur on output),
                            STensor43 &Tangent,         // constitutive tangents (output)
                            std::vector<STensor3>  &dLocalPlasticStrainDStrain,
  			    std::vector<STensor3>  &dStressDNonLocalPlasticStrain,
                            fullMatrix<double>     &dLocalPlasticStrainDNonLocalPlasticStrain,
                            const bool stiff,
                            STensor43* elasticTangent) const
{
  if(elasticTangent!=NULL) Msg::Error(" mlawNonLocalDamage STensor43* elasticTangent not defined");
  const IPNonLocalDamage *ipvprev = dynamic_cast<const IPNonLocalDamage *> (ipvprevi);
  IPNonLocalDamage *ipvcur = dynamic_cast<IPNonLocalDamage *> (ipvcuri); 

  mlawNonLocalDamage::constitutive(F0, Fn, P, ipvprev, ipvcur, Tangent, stiff,elasticTangent);
  if(stiff && !STensorOperation::isnan(P))
  {
    dLocalPlasticStrainDStrain[0](0,0) = ipvcur->_nldCouplingEffectiveStrainStress(0); //minus?
    dLocalPlasticStrainDStrain[0](1,1) = ipvcur->_nldCouplingEffectiveStrainStress(1);
    dLocalPlasticStrainDStrain[0](2,2) = ipvcur->_nldCouplingEffectiveStrainStress(2);
    dLocalPlasticStrainDStrain[0](0,1) = ipvcur->_nldCouplingEffectiveStrainStress(3);
    dLocalPlasticStrainDStrain[0](1,0) = ipvcur->_nldCouplingEffectiveStrainStress(3);
    dLocalPlasticStrainDStrain[0](0,2) = ipvcur->_nldCouplingEffectiveStrainStress(4);
    dLocalPlasticStrainDStrain[0](2,0) = ipvcur->_nldCouplingEffectiveStrainStress(4);
    dLocalPlasticStrainDStrain[0](2,1) = ipvcur->_nldCouplingEffectiveStrainStress(5);
    dLocalPlasticStrainDStrain[0](1,2) = ipvcur->_nldCouplingEffectiveStrainStress(5);

    dStressDNonLocalPlasticStrain[0](0,0) = ipvcur->_nldCouplingStressEffectiveStrain(0);
    dStressDNonLocalPlasticStrain[0](1,1) = ipvcur->_nldCouplingStressEffectiveStrain(1);
    dStressDNonLocalPlasticStrain[0](2,2) = ipvcur->_nldCouplingStressEffectiveStrain(2);
    dStressDNonLocalPlasticStrain[0](0,1) = ipvcur->_nldCouplingStressEffectiveStrain(3);
    dStressDNonLocalPlasticStrain[0](1,0) = ipvcur->_nldCouplingStressEffectiveStrain(3);
    dStressDNonLocalPlasticStrain[0](0,2) = ipvcur->_nldCouplingStressEffectiveStrain(4);
    dStressDNonLocalPlasticStrain[0](2,0) = ipvcur->_nldCouplingStressEffectiveStrain(4);
    dStressDNonLocalPlasticStrain[0](2,1) = ipvcur->_nldCouplingStressEffectiveStrain(5);
    dStressDNonLocalPlasticStrain[0](1,2) = ipvcur->_nldCouplingStressEffectiveStrain(5);

    dLocalPlasticStrainDNonLocalPlasticStrain(0,0) = ipvcur->_nldSpBar;
    if(getNbNonLocalVariables()>1){
      dLocalPlasticStrainDNonLocalPlasticStrain(1,0) = ipvcur->_nldFddp; 
      dLocalPlasticStrainDNonLocalPlasticStrain(0,1) = ipvcur->_nldpdFd;
      dLocalPlasticStrainDNonLocalPlasticStrain(1,1) = ipvcur->_nldFd_d_bar;
    }

    if(getNbNonLocalVariables()>1){
      dLocalPlasticStrainDStrain[1](0,0) = ipvcur->_nldFiberDamdStrain(0); 
      dLocalPlasticStrainDStrain[1](1,1) = ipvcur->_nldFiberDamdStrain(1);
      dLocalPlasticStrainDStrain[1](2,2) = ipvcur->_nldFiberDamdStrain(2);
      dLocalPlasticStrainDStrain[1](0,1) = ipvcur->_nldFiberDamdStrain(3);
      dLocalPlasticStrainDStrain[1](1,0) = ipvcur->_nldFiberDamdStrain(3);
      dLocalPlasticStrainDStrain[1](0,2) = ipvcur->_nldFiberDamdStrain(4);
      dLocalPlasticStrainDStrain[1](2,0) = ipvcur->_nldFiberDamdStrain(4);
      dLocalPlasticStrainDStrain[1](2,1) = ipvcur->_nldFiberDamdStrain(5);
      dLocalPlasticStrainDStrain[1](1,2) = ipvcur->_nldFiberDamdStrain(5);


  
      dStressDNonLocalPlasticStrain[1](0,0) = ipvcur->_nldStressdFiberDam(0);
      dStressDNonLocalPlasticStrain[1](1,1) = ipvcur->_nldStressdFiberDam(1);
      dStressDNonLocalPlasticStrain[1](2,2) = ipvcur->_nldStressdFiberDam(2);
      dStressDNonLocalPlasticStrain[1](0,1) = ipvcur->_nldStressdFiberDam(3);
      dStressDNonLocalPlasticStrain[1](1,0) = ipvcur->_nldStressdFiberDam(3);
      dStressDNonLocalPlasticStrain[1](0,2) = ipvcur->_nldStressdFiberDam(4);
      dStressDNonLocalPlasticStrain[1](2,0) = ipvcur->_nldStressdFiberDam(4);
      dStressDNonLocalPlasticStrain[1](2,1) = ipvcur->_nldStressdFiberDam(5);   
      dStressDNonLocalPlasticStrain[1](1,2) = ipvcur->_nldStressdFiberDam(5);
    }
  }

  /*if(STensorOperation::isnan(P)) 
    Msg::Error("P is nan");
  if(STensorOperation::isnan(Tangent)) 
    Msg::Error("Tangent is nan");
  if(STensorOperation::isnan(dLocalPlasticStrainDStrain[0]))
    Msg::Error("dLocalPlasticStrainDStrain[0] is nan");
  if(STensorOperation::isnan(dGradLocalPlasticStrainDStrain[0]))
    Msg::Error("dGradLocalPlasticStrainDStrain[0] is nan");
  if(STensorOperation::isnan(dStressDNonLocalPlasticStrain[0]))
    Msg::Error("dStressDNonLocalPlasticStrain[0] is nan");
  if(STensorOperation::isnan(dLocalPlasticStrainDNonLocalPlasticStrain(0,0)))
    Msg::Error("dLocalPlasticStrainDNonLocalPlasticStrain[0,0] is nan");
  if(STensorOperation::isnan(dLocalPlasticStrainDGradNonLocalPlasticStrain(0,0)))
    Msg::Error("dLocalPlasticStrainDGradNonLocalPlasticStrain is nan[0,0]");
  if(getNbNonLocalVariables()>1){
    if(STensorOperation::isnan(dLocalPlasticStrainDStrain[1]))
      Msg::Error("dLocalPlasticStrainDStrain[1] is nan");
    if(STensorOperation::isnan(dStressDNonLocalPlasticStrain[1]))
      Msg::Error("dStressDNonLocalPlasticStrain[1] is nan");
    if(STensorOperation::isnan(dGradLocalPlasticStrainDStrain[1]))
      Msg::Error("dGradLocalPlasticStrainDStrain[1] is nan");
    if(STensorOperation::isnan(dLocalPlasticStrainDNonLocalPlasticStrain(1,0)))
      Msg::Error("dLocalPlasticStrainDNonLocalPlasticStrain[1,0] is nan");
    if(STensorOperation::isnan(dLocalPlasticStrainDNonLocalPlasticStrain(0,1)))
      Msg::Error("dLocalPlasticStrainDNonLocalPlasticStrain[0,1] is nan");
    if(STensorOperation::isnan(dLocalPlasticStrainDNonLocalPlasticStrain(1,1)))
      Msg::Error("dLocalPlasticStrainDNonLocalPlasticStrain[1,1] is nan");
    if(STensorOperation::isnan(dLocalPlasticStrainDGradNonLocalPlasticStrain(1,0)))
      Msg::Error("dLocalPlasticStrainDGradNonLocalPlasticStrain[1,0] is nan");
    if(STensorOperation::isnan(dLocalPlasticStrainDGradNonLocalPlasticStrain(0,1)))
      Msg::Error("dLocalPlasticStrainDGradNonLocalPlasticStrain[0,1] is nan");
    if(STensorOperation::isnan(dLocalPlasticStrainDGradNonLocalPlasticStrain(1,1)))
      Msg::Error("dLocalPlasticStrainDGradNonLocalPlasticStrain[1,1] is nan");
  }*/

}

const double mlawNonLocalDamage::bulkModulus() const
{
  return 2.*_mu0*(1+_nu0)/3./(1.-2.*_nu0);
}
const double mlawNonLocalDamage::shearModulus() const
{
  return _mu0;
}

const double mlawNonLocalDamage::poissonRatio() const
{
  return _nu0;
}
void mlawNonLocalDamage::ElasticStiffness(double *statev, STensor43* elasticTensor) const
{
  fullMatrix<double> Cel_fullMatrix(6,6);
  static double **Cel_matrix;
  static bool init=false;
  if(init==false)
  {
    mallocmatrix(&Cel_matrix,6,6);
    init=true;
  }
  mat->get_elOp(statev,Cel_matrix);
  for(int i=0;i<6;i++)
  {
    for(int j=0;j<6;j++){
      Cel_fullMatrix(i,j)=Cel_matrix[i][j];
    }
  }
  for(int i=3;i<6;i++)
  {
    for(int j=0;j<3;j++)
    {
      Cel_fullMatrix(i,j)*=(1./sq2);
      Cel_fullMatrix(j,i)*=2.;
      Cel_fullMatrix(j,i)*=(1./sq2);
    }
  }
  STensorOperation::fromFullMatrixToSTensor43(Cel_fullMatrix,(*elasticTensor));
};

void mlawNonLocalDamage::ElasticStiffness(STensor43* elasticTensor) const
{
  fullMatrix<double> Cel_fullMatrix(6,6);
  static double **Cel_matrix;
  static bool init=false;
  if(init==false)
  {
    mallocmatrix(&Cel_matrix,6,6);
    init=true;
  }
  mat->get_elOp(Cel_matrix);
  for(int i=0;i<6;i++)
  {
    for(int j=0;j<6;j++){
      Cel_fullMatrix(i,j)=Cel_matrix[i][j];
    }
  }
  for(int i=3;i<6;i++)
  {
    for(int j=0;j<3;j++)
    {
      Cel_fullMatrix(i,j)*=(1./sq2);
      Cel_fullMatrix(j,i)*=2.;
      Cel_fullMatrix(j,i)*=(1./sq2);
    }
  }
  STensorOperation::fromFullMatrixToSTensor43(Cel_fullMatrix,(*elasticTensor));
};

void mlawNonLocalDamage::ElasticStiffnessMtx(double *statev, STensor43* elasticTensor) const
{
  fullMatrix<double> Cel_fullMatrix(6,6);
  static double **Cel_matrix;
  static bool init=false;
  if(init==false)
  {
    mallocmatrix(&Cel_matrix,6,6);
    init=true;
  }
  mat->get_elOp_mtx(statev,Cel_matrix);
  for(int i=0;i<6;i++)
  {
    for(int j=0;j<6;j++){
      Cel_fullMatrix(i,j)=Cel_matrix[i][j];
    }
  }
  for(int i=3;i<6;i++)
  {
    for(int j=0;j<3;j++)
    {
      Cel_fullMatrix(i,j)*=(1./sq2);
      Cel_fullMatrix(j,i)*=2.;
      Cel_fullMatrix(j,i)*=(1./sq2);
    }
  }
  STensorOperation::fromFullMatrixToSTensor43(Cel_fullMatrix,(*elasticTensor));
};

void mlawNonLocalDamage::ElasticStiffnessInc(double *statev, STensor43* elasticTensor) const
{
  fullMatrix<double> Cel_fullMatrix(6,6);
  static double **Cel_matrix;
  static bool init=false;
  if(init==false)
  {
    mallocmatrix(&Cel_matrix,6,6);
    init=true;
  }
  mat->get_elOp_icl(statev,Cel_matrix);
  for(int i=0;i<6;i++)
  {
    for(int j=0;j<6;j++){
      Cel_fullMatrix(i,j)=Cel_matrix[i][j];
    }
  }
  for(int i=3;i<6;i++)
  {
    for(int j=0;j<3;j++)
    {
      Cel_fullMatrix(i,j)*=(1./sq2);
      Cel_fullMatrix(j,i)*=2.;
      Cel_fullMatrix(j,i)*=(1./sq2);
    }
  }
  STensorOperation::fromFullMatrixToSTensor43(Cel_fullMatrix,(*elasticTensor));
};

void mlawNonLocalDamage::ElasticStiffness_incOri(double e1, double e2, double e3, STensor43* elasticTensor) const
{
  fullMatrix<double> Cel_fullMatrix(6,6);
  static double **Cel_matrix;
  static bool init=false;
  if(init==false)
  {
    mallocmatrix(&Cel_matrix,6,6);
    init=true;
  }
  mat->setEuler(e1,e2,e3);
  mat->get_elOp(Cel_matrix);
  for(int i=0;i<6;i++)
  {
    for(int j=0;j<6;j++){
      Cel_fullMatrix(i,j)=Cel_matrix[i][j];
    }
  }
  for(int i=3;i<6;i++)
  {
    for(int j=0;j<3;j++)
    {
      Cel_fullMatrix(i,j)*=(1./sq2);
      Cel_fullMatrix(j,i)*=2.;
      Cel_fullMatrix(j,i)*=(1./sq2);
    }
  }
  STensorOperation::fromFullMatrixToSTensor43(Cel_fullMatrix,(*elasticTensor));
};

void mlawNonLocalDamage::SecantStiffnessIsotropic(const IPVariable *q1i, STensor43& Secant) const
{
  const IPNonLocalDamage* ipvcur = static_cast<const IPNonLocalDamage*>(q1i);

  STensor3 eps, P;
  for(int i=0;i<3;i++)
  {
    P(i,i) = ipvcur->_nldStress(i);
    eps(i,i) = ipvcur->_nldStrain(i);
  }
  P(0,1) = P(1,0) = ipvcur->_nldStress(3);
  P(0,2) = P(2,0) = ipvcur->_nldStress(4);
  P(1,2) = P(2,1) = ipvcur->_nldStress(5);
  eps(0,1) = eps(1,0) = ipvcur->_nldStrain(3);
  eps(0,2) = eps(2,0) = ipvcur->_nldStrain(4);
  eps(1,2) = eps(2,1) = ipvcur->_nldStrain(5);

  STensor3 eps_dev = eps.dev();
  double epsEq = sqrt(2./3.*eps_dev.dotprod());
  if(epsEq < 1.e-12)
  {
    ElasticStiffness(&Secant);
  }
  else
  {
    STensor3 P_dev = P.dev();
    double sigEq = sqrt(1.5*P_dev.dotprod());
    double shearmodulus_secant = sigEq/(3.*epsEq);
  
    double p = P(0,0)+P(1,1)+P(2,2);
    p *= (1./3.);
    double eps_vol = eps(0,0)+eps(1,1)+eps(2,2);
    double bulkmodulus_secant = p/eps_vol;

    STensor43 dev_part;
    STensorOperation::sphericalfunction(Secant);
    Secant *= 3.;
    Secant *= bulkmodulus_secant;
    STensorOperation::deviatorfunction(dev_part);    
    dev_part *= 2.;
    dev_part *= shearmodulus_secant;
    Secant += dev_part;
  }
};

MFH::Material* init_material(double *_props,int idmat)
{
	MFH::Material* _mat;
 	int model = (int)_props[idmat];

	//TO DO: use pointer of functions to avoid switch?
	switch(model){

		case EL:
			_mat = new MFH::EL_Material(_props,idmat);
			break;

		case EP:
			_mat = new MFH::EP_Material(_props,idmat);
			break;

		case EP_Stoch:
			_mat = new MFH::EP_Stoch_Material(_props,idmat);
			break;

		case EVP:
			_mat = new MFH::EVP_Material(_props,idmat);
			break;

		case VT:
			_mat = new MFH::VT_Material(_props,idmat);
			break;

		case MT:
			_mat = new MFH::MT_Material(_props,idmat);
			break;

		case SC:
			_mat = new MFH::SC_Material(_props,idmat);
			break;
		case ANEL:
			_mat = new MFH::ANEL_Material(_props,idmat);
			break;
                case MTSecF:
			_mat = new MFH::MTSecF_Material(_props,idmat);
			break;
		case MTSecNtr:
			_mat = new MFH::MTSecNtr_Material(_props,idmat);
			break;
		case MTSecSd:
			_mat = new MFH::MTSecSd_Material(_props,idmat);
			break;
                case MTSecFirStoch:
			_mat = new MFH::MTSecF_Stoch_Material(_props,idmat);
			break;
                case MTSecF_LargeDeform:
			_mat = new MFH::MTSecF_LargD(_props,idmat);
			break;
                case MT_VT:
			_mat = new MFH::MT_VT_Material(_props,idmat);
			break;
                case LAM_2PLY:
			_mat = new MFH::LAM2Ply_Material(_props,idmat);
			break;
		default :
			Msg::Error("Unknown material model: %d\n", model);
			_mat=NULL;

	}
    return _mat;
}

//*************add by wu ling *********************************

MFH::Damage* init_damage(double* props, int iddam){

	MFH::Damage* dam;
	int model;
	if(iddam != 0) model = (int)props[iddam];
	else model=0;

	switch(model){

		case 0:
			dam=NULL;
			break;

		case D_LC:
			dam = new MFH::LC_Damage(props,iddam);
			break;

	        case D_LCN:
			dam = new MFH::LCN_Damage(props,iddam);
			break;

	        case D_LCN_STOCH:
			dam = new MFH::LCN_Damage_Stoch(props,iddam);
			break;

	        case D_LINN:
			dam = new MFH::LINN_Damage(props,iddam);
			break;

	        case D_EXPN:
			dam = new MFH::EXPN_Damage(props,iddam);
			break;

	        case D_PLN:
			dam = new MFH::PLN_Damage(props,iddam);
			break;

                case D_WEIBULL:
                        dam = new MFH::Weibull_Damage(props,iddam);
                        break;

                case D_BILIN:
                        dam = new MFH::BILIN_Damage(props,iddam);
                        break;
                case D_BILIN_STOCH:
                        dam = new MFH::BILIN_Damage_Stoch(props,iddam);
                        break;
                case D_SIGMOID:
                        dam = new MFH::Sigmoid_Damage(props,iddam);
                        break;

		default:
			printf("Unknown damage model: %d\n", model);
			dam=NULL;

	}
	return dam;

}

//*************add by wu ling *********************************

MFH::Clength* init_clength(double* props, int idclength){

	MFH::Clength* clength;
	int model;
	if(idclength != 0) model = (int)props[idclength];
	else model=0;

	switch(model){
	
		case 0:
			clength = new MFH::Zero_Cl(props,idclength);
			break;

		case CL_Iso:
			clength = new MFH::Iso_Cl(props,idclength);
			break;

	        case CL_Aniso:
			clength = new MFH::Aniso_Cl(props,idclength);
			break;

	        case CL_Iso_V1:
			clength = new MFH::IsoV1_Cl(props,idclength);
			break;

	        case CL_Aniso_V1:
			clength = new MFH::AnisoV1_Cl(props,idclength);
			break;

                case CL_Stoch:
                        clength = new MFH::Stoch_Cl(props,idclength);
                        break;

		default:
			printf("Unknown Characteristic length model: %d\n", model);
			clength=NULL;			
	}
	return clength;
}
