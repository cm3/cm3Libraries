//
// C++ Interface: material law
//
// Description: LinearElecThermoMechanics law
//
//
// Author:  <L. Homsy>, (C) 2015
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "mlawLinearElecTherMech.h"
#include <math.h>
#include "MInterfaceElement.h"
#include "mlawLinearThermoMechanics.h"


mlawLinearElecTherMech::mlawLinearElecTherMech(const int num,const double rho,
			const double Ex, const double Ey, const double Ez, const double Vxy, const double Vxz, const double Vyz,
			 const double MUxy, const double MUxz, const double MUyz, const double alpha, const double beta, const double gamma,
			 const double t0,const double Kx,const double Ky,const double Kz,const double alphax,const double alphay,
		          const double alphaz,const  double lx,const  double ly,const  double lz,const double seebeck,const double cp,const double v0):
				 mlawLinearThermoMechanics( num, rho, Ex,  Ey,  Ez,  Vxy, Vxz,  Vyz, MUxy,MUxz,  MUyz, alpha, beta,  gamma,
				         t0, Kx, Ky, Kz, alphax,alphay, alphaz,cp) ,_lx(lx),_ly(ly),_lz(lz),_seebeck(seebeck),_v0(v0)
{
	STensor3 R;		//3x3 rotation matrix

	double c1,c2,c3,s1,s2,s3;
	double s1c2, c1c2;
	double pi(3.14159265359);
    	double fpi = pi/180.;

	c1 = cos(_alpha*fpi);
	s1 = sin(_alpha*fpi);

	c2 = cos(_beta*fpi);
	s2 = sin(_beta*fpi);

	c3 = cos(_gamma*fpi);
	s3 = sin(_gamma*fpi);

	s1c2 = s1*c2;
	c1c2 = c1*c2;

	R(0,0) = c3*c1 - s1c2*s3;
	R(0,1) = c3*s1 + c1c2*s3;
	R(0,2) = s2*s3;

	R(1,0) = -s3*c1 - s1c2*c3;
	R(1,1) = -s3*s1 + c1c2*c3;
	R(1,2) = s2*c3;

	R(2,0) = s1*s2;
	R(2,1) = -c1*s2;
	R(2,2) = c2;


	STensor3 l;
	// to be unifom in DG3D e= l' gradV with l'=-l instead of e=-l gradV
	l(0,0)  = -_lx;
	l(1,1)  = -_ly;
	l(2,2)  = -_lz;
  	for(int i=0;i<3;i++)
  	 {
    	  for(int j=0;j<3;j++)
    	   {
             _l0(i,j)=0.;
             for(int m=0;m<3;m++)
             {
               for(int n=0;n<3;n++)
            	{
              	  _l0(i,j)+=R(m,i)*R(n,j)*l(m,n);
       	        }
     	      }
   	    }
	  }	  	  
     /*  _l10 =_l0;
	_l20 =_l0;
	_l20 *=_seebeck;
	_k10=_l0;
	_k10*=_seebeck*_seebeck*_t0;
	_k10+=_k;
	_k20=_l0;
	_k20*=_seebeck*_t0;*/


}
mlawLinearElecTherMech::mlawLinearElecTherMech(const mlawLinearElecTherMech &source) : mlawLinearThermoMechanics(source),_lx(source._lx),
						      _ly(source._ly),_lz(source._lz),_seebeck(source._seebeck),_v0(source._v0)

{
  _l0 =source._l0;
  /*_k10 =source._k10;
  _k20 =source._k20;
  _l10 =source._l10;
  _l20 =source._l20;*/

}

mlawLinearElecTherMech& mlawLinearElecTherMech::operator=(const materialLaw &source)
{
  mlawLinearThermoMechanics::operator=(source);
  const mlawLinearElecTherMech* src =static_cast<const mlawLinearElecTherMech*>(&source);
  if(src !=NULL)
  {
    _lx = src->_lx;
    _ly = src->_ly;
    _lz = src->_lz;
    _seebeck=src->_seebeck;
    _l0 =src->_l0;
  /*  _k10 =src->_k10;
    _k20 =src->_k20;
    _l10 =src->_l10;
    _l20 =src->_l20;*/

  }
  return *this;
}

void mlawLinearElecTherMech::createIPState(IPStateBase* &ips,bool hasBodyForce,const bool* state_,const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
  bool inter=true;
  const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
  if(iele==NULL) inter=false;
  IPVariable* ipvi = new IPLinearElecTherMech();
  IPVariable* ipv1 = new IPLinearElecTherMech();
  IPVariable* ipv2 = new IPLinearElecTherMech();
  if(ips != NULL) delete ips;
  ips = new IP3State(state_,ipvi,ipv1,ipv2);
}


void mlawLinearElecTherMech::constitutive(
                            const STensor3& F0,         // initial deformation gradient (input @ time n)
                            const STensor3& Fn,         // updated deformation gradient (input @ time n+1)
                            STensor3 &P,                // updated 1st Piola-Kirchhoff stress tensor (output)
                                                        // contains the initial values on input
                            const IPLinearElecTherMech *q0,       // array of initial internal variable
                            IPLinearElecTherMech *q1,             // updated array of internal variable (in ipvcur on output),
                            STensor43 &Tangent,         // constitutive tangents (output)
                            double T0,
			    double T,
			    double V0,
			    double V,
  			    const SVector3 &gradT,
			    const SVector3 &gradV,
                            SVector3 &fluxT,
			    SVector3 &fluxV,
                            STensor3 &dPdT,
                            STensor3 &dPdV,
                            STensor3 &dqdgradT,
                            SVector3 &dqdT,
			    STensor3 &dqdgradV,
                            SVector3 &dqdV,
			    SVector3 &djedV,
			    STensor3 &djedgradV,
			    SVector3 &djedT,
			    STensor3 &djedgradT,
                            STensor33 &dqdF,
			    STensor33 &djedF,
			    double &w,
			    double &dwdt,
			    STensor3 &dwdf,
			    STensor3 &l10,
			    STensor3 &l20,
			    STensor3 &k10,
			    STensor3 &k20,
			    STensor3 &dl10dT,
			    STensor3 &dl20dT,
			    STensor3 &dk10dT,
			    STensor3 &dk20dT,
			    STensor3 &dl10dv,
			    STensor3 &dl20dv,
			    STensor3 &dk10dv,
			    STensor3 &dk20dv,
			    STensor43 &dl10dF,
			    STensor43 &dl20dF,
			    STensor43 &dk10dF,
			    STensor43 &dk20dF,
			    SVector3 &fluxjy,
			    SVector3 &djydV,
			    STensor3 &djydgradV,
			    SVector3 &djydT,
			    STensor3 &djydgradT,
                            STensor33 &djydF,
			    STensor3 &djydgradVdT,
			    STensor3 &djydgradVdV,
			    STensor43 &djydgradVdF,
			    STensor3 &djydgradTdT,
			    STensor3 &djydgradTdV,
			    STensor43 &djydgradTdF,
                            const bool stiff,
                            const bool evaluateCurlField
                           )const
{
    mlawLinearThermoMechanics::constitutive(F0,Fn, P, (const IPLinearThermoMechanics *) q0,  (IPLinearThermoMechanics *)q1,Tangent, T0, T, gradT, fluxT,
                            dPdT, dqdgradT, dqdT, dqdF,w,dwdt,dwdf,stiff); // the flux and dqd.. are not correct,  need to be recomputed

// if true then we solve EM problem and thus reevaluate Voltage field as well
// else do not consider Voltage contributions -> TM problem
if(evaluateCurlField)
{
    double dseebeckdT;
    static STensor3 dldT,dkdT;//,k1,k2,l10,l20;

    STensorOperation::zero(dldT);
    STensorOperation::zero(dkdT);
    STensorOperation::zero(dseebeckdT);
    STensorOperation::zero(djedV);
    STensorOperation::zero(dqdV);
    STensorOperation::zero(dqdF); // dependency of flux with deformation gradient
    STensorOperation::zero(djedF);
    STensorOperation::zero(dPdV);

    /*STensorOperation::zero(fluxjy);
    STensorOperation::zero(djydgradT);STensorOperation::zero(djydgradV); STensorOperation::zero(djydV); STensorOperation::zero(djydT);
   // STensorOperation::zero(jy1);STensorOperation::zero(djy1dT);STensorOperation::zero(djy1dV);
    STensorOperation::zero(djydgradVdT);STensorOperation::zero(djydgradVdV);STensorOperation::zero(djydgradVdF);
    STensorOperation::zero(djydgradTdT);STensorOperation::zero(djydgradTdV);STensorOperation::zero(djydgradTdF);*/

   // STensorOperation::zero(dPdT);

     //_k=1.758-5.290/1000.*T-9.582*pow(10,-10)*T*T;
     //_l0=1.028*pow(10,5)-5.369*100*T+1.824*T*T;
     //seebeck=1.802*pow(10,-4)+3.861*pow(10.-7)*T-9.582*pow(10.-10)*T*T;

      //modified
    STensorOperation::zero(l10);
    STensorOperation::zero(l20);
    STensorOperation::zero(k10);
    STensorOperation::zero(k20);
    STensorOperation::zero(dl10dT);
    STensorOperation::zero(dl20dT);
    STensorOperation::zero(dk10dT);
    STensorOperation::zero(dk20dT);
    STensorOperation::zero(dl10dv);
    STensorOperation::zero(dl20dv);
    STensorOperation::zero(dk10dv);
    STensorOperation::zero(dk20dv);
    STensorOperation::zero(dl10dF);
    STensorOperation::zero(dl20dF);
    STensorOperation::zero(dk10dF);
    STensorOperation::zero(dk20dF);
     for(int i = 0; i< 3; i++)
      {
        for(int j = 0; j< 3; j++)
        {
        l10(i,j) = -_l0(i,j)*T;
 	l20(i,j) = -_l0(i,j)*(V*T+_seebeck*T*T);
	//k10(i,j) = -_k(i,j)*T*T- _seebeck*_l0(i,j)*T*T*V-pow(_seebeck,2)*_l0(i,j)*pow(T,3);
	//k20(i,j) = -_seebeck*T*T*_l0(i,j) ;
        k20(i,j) = l20(i,j);
        k10(i,j) = -_k(i,j)*T*T-2.*_seebeck*_l0(i,j)*T*T*V-pow(_seebeck,2)*_l0(i,j)*pow(T,3)-_l0(i,j)*T*V*V;
	//jy1(i,j) = -_k(i,j)*T*T-2.*_seebeck*_l0(i,j)*T*T*V-pow(_seebeck,2)*_l0(i,j)*pow(T,3)-_l0(i,j)*T*V*V;
	}
      }




    for(int i=0;i<3;i++)
     {
       fluxT(i)=0.;
       for(int j=0;j<3;j++)
       {
	 fluxT(i)+=(_k(i,j)+_l0(i,j)*_seebeck*_seebeck*T)*gradT(j)+_l0(i,j)*_seebeck*T*gradV(j);
         fluxjy(i)+=(_k(i,j)+_l0(i,j)*_seebeck*_seebeck*T+_l0(i,j)*_seebeck*V)*gradT(j)+(_l0(i,j)*V+_l0(i,j)*_seebeck*T)*gradV(j);
       }
     }

     for(int i=0;i<3;i++)
      {
//	fluxjy(i)=fluxT(i)+V*fluxV(i);
      }
      // _l10=_l0;
      // _l20=_seebeck*_l0;

    for(int i=0;i<3;i++)
    {
      fluxV(i)=0.;
      for(int j=0;j<3;j++)
      {
         fluxV(i)+=_l0(i,j)*gradV(j)+_l0(i,j)*_seebeck*gradT(j);
      }
    }


             for(int i = 0; i< 3; i++)
	   {
	     for(int j = 0; j< 3; j++)
	       {
		 dl10dT(i,j) = -_l0(i,j);
		 dl20dT(i,j) = -_l0(i,j)*(V+2*_seebeck*T);
		 //dk10dT(i,j) = -2*_k(i,j)*T- 2*_seebeck*_l0(i,j)*T*V-3*pow(_seebeck,2)*_l0(i,j)*pow(T,2);
		 //dK20dT(i,j) = -2*_seebeck*T*_l0(i,j);
                 dk20dT(i,j) = dl20dT(i,j);
		 dl20dv(i,j)= -_l0(i,j)*(T);
                 dk20dv(i,j) = dl20dT(i,j);
		 //dk10dv(i,j)= -_seebeck*_l0(i,j)*T*T;
		// djy1dT(i,j) = -2.*_k(i,j)*T- 4.*_seebeck*_l0(i,j)*T*V-3*pow(_seebeck,2)*_l0(i,j)*pow(T,2)-_l0(i,j)*V*V;
		// djy1dV(i,j) = -2.*_seebeck*_l0(i,j)*T*T-2.*_l0(i,j)*T*V;
                 dk10dT(i,j) = -2.*_k(i,j)*T- 4.*_seebeck*_l0(i,j)*T*V-3*pow(_seebeck,2)*_l0(i,j)*pow(T,2)-_l0(i,j)*V*V;
                 dk10dv(i,j) = -2.*_seebeck*_l0(i,j)*T*T-2.*_l0(i,j)*T*V;
	       }
	   }


     // dedT=0.;
    for(int i=0;i<3;i++)
    {
      djedT(i)=0.;
      for(int j=0;j<3;j++)
      {
         djedT(i)+=dseebeckdT*_l0(i,j)*gradT(j)+_seebeck*dldT(i,j)*gradT(j)+dldT(i,j)*gradV(j);
       }
    }
    djedgradT=_seebeck*_l0;



    djedgradV =_l0;

    for(int i=0;i<3;i++)
    {
      dqdT(i)=0.;
      for(int j=0;j<3;j++)
      {
         dqdT(i)+=dkdT(i,j)*gradT(j)+2*_seebeck*dseebeckdT*_l0(i,j)*T*gradT(j)+_seebeck*_seebeck*dldT(i,j)*T*gradT(j)+_seebeck*_seebeck*_l0(i,j)*gradT(j)
	 +dseebeckdT*_l0(i,j)*T*gradV(j)+_seebeck*dldT(i,j)*T*gradV(j)+_seebeck*_l0(i,j)*gradV(j);

	 djydT(i)+=_seebeck*_seebeck*_l0(i,j)*gradT(j)+_seebeck*_l0(i,j)*gradV(j);
	 djydV(i)+=_seebeck*_l0(i,j)*gradT(j)+_l0(i,j)*gradV(j);
       }
    }
    STensorOperation::zero(dqdgradT);
    STensorOperation::zero(dqdgradV);
    for(int i=0;i<3;i++)
    {
      for(int k=0;k<3;k++)
      {
	dqdgradT(i,k)=(_k(i,k)+_l0(i,k)*_seebeck*_seebeck*T);
	dqdgradV(i,k)=_l0(i,k)*_seebeck*T;

	djydgradT(i,k)+=_k(i,k)+_l0(i,k)*_seebeck*_seebeck*T+_l0(i,k)*_seebeck*V;
	djydgradV(i,k) =_l0(i,k)*_seebeck*T+_l0(i,k)*V;
	djydgradVdT(i,k) =_l0(i,k)*_seebeck;
	djydgradVdV(i,k) =_l0(i,k);
	djydgradTdT(i,k)+=_l0(i,k)*_seebeck*_seebeck;
	djydgradTdV(i,k)+=_l0(i,k)*_seebeck;
      }
    }
   double dh;
   double dcpdt=0.;
   dh = getTimeStep();
   w=0.;dwdt=0.;STensorOperation::zero(dwdf);
   if(dh>0)
   {
     w=-_cp->getVal(T0)*(T-T0)/dh;
    // dwdt=-dcpdt-_cp->getVal(T0)/dh;
     dwdt=-dcpdt*(T-T0)/dh-_cp->getVal(T0)/dh;
   }

 //}
      STensor3 FnT=Fn.transpose();
      STensor3 defo(FnT);
      defo+=Fn;
      defo*=0.5;
      defo(0,0)-=1.;
      defo(1,1)-=1.;
      defo(2,2)-=1.;

   q1->_elasticEnergy=deformationEnergy(Fn,gradV,gradT,T,V);
   }

// to evaluate q or jy depending on je
// in TM problem with EM coupling
    /*{
        double dseebeckdT;
        static STensor3 dldT,dkdT;//,k1,k2,l10,l20;

        STensorOperation::zero(dldT);
        STensorOperation::zero(dkdT);
        STensorOperation::zero(dseebeckdT);

        STensorOperation::zero(fluxjy);
        STensorOperation::zero(djydgradT);STensorOperation::zero(djydgradV); STensorOperation::zero(djydV); STensorOperation::zero(djydT);
        // STensorOperation::zero(jy1);STensorOperation::zero(djy1dT);STensorOperation::zero(djy1dV);
        STensorOperation::zero(djydgradVdT);STensorOperation::zero(djydgradVdV);STensorOperation::zero(djydgradVdF);
        STensorOperation::zero(djydgradTdT);STensorOperation::zero(djydgradTdV);STensorOperation::zero(djydgradTdF);
        STensorOperation::zero(dqdV);
        STensorOperation::zero(dqdF);

        for(int i=0;i<3;i++)
        {
            fluxT(i)=0.;
            for(int j=0;j<3;j++)
            {
                fluxT(i)+=(_k(i,j)+_l0(i,j)*_seebeck*_seebeck*T)*gradT(j)+_l0(i,j)*_seebeck*T*gradV(j);
                fluxjy(i)+=(_k(i,j)+_l0(i,j)*_seebeck*_seebeck*T+_l0(i,j)*_seebeck*V)*gradT(j)+(_l0(i,j)*V+_l0(i,j)*_seebeck*T)*gradV(j);
            }
        }

        for(int i=0;i<3;i++)
        {
            dqdT(i)=0.;
            for(int j=0;j<3;j++)
            {
                dqdT(i)+=dkdT(i,j)*gradT(j)+2*_seebeck*dseebeckdT*_l0(i,j)*T*gradT(j)+_seebeck*_seebeck*dldT(i,j)*T*gradT(j)+_seebeck*_seebeck*_l0(i,j)*gradT(j)
                         +dseebeckdT*_l0(i,j)*T*gradV(j)+_seebeck*dldT(i,j)*T*gradV(j)+_seebeck*_l0(i,j)*gradV(j);

                djydT(i)+=_seebeck*_seebeck*_l0(i,j)*gradT(j)+_seebeck*_l0(i,j)*gradV(j);
                djydV(i)+=_seebeck*_l0(i,j)*gradT(j)+_l0(i,j)*gradV(j);
            }
        }
        STensorOperation::zero(dqdgradT);
        STensorOperation::zero(dqdgradV);
        for(int i=0;i<3;i++)
        {
            for(int k=0;k<3;k++)
            {
                dqdgradT(i,k)=(_k(i,k)+_l0(i,k)*_seebeck*_seebeck*T);
                dqdgradV(i,k)=_l0(i,k)*_seebeck*T;

                djydgradT(i,k)+=_k(i,k)+_l0(i,k)*_seebeck*_seebeck*T+_l0(i,k)*_seebeck*V;
                djydgradV(i,k) =_l0(i,k)*_seebeck*T+_l0(i,k)*V;
                djydgradVdT(i,k) =_l0(i,k)*_seebeck;
                djydgradVdV(i,k) =_l0(i,k);
                djydgradTdT(i,k)+=_l0(i,k)*_seebeck*_seebeck;
                djydgradTdV(i,k)+=_l0(i,k)*_seebeck;
            }
        }
    }*/
}
  
  double mlawLinearElecTherMech::deformationEnergy(STensor3 defo,const SVector3 &gradV,const SVector3 &gradT,const double T, const double V) const 
{
  double fz=0.;
	 for(int i=0;i<3;i++)
	 {
 	   for(int j=0;j<3;j++)
           {
	    fz+=defo(i,j)*defo(i,j);
           }
	 //fz+=(-gradV(i)/T+V*gradT(i)/(T*T))*(-gradV(i)/T+V*gradT(i)/(T*T));//-gradT(i)/(T*T);
	// fz+=-gradT(i)/(T*T)*(-gradT(i)/(T*T));
	 }  		
  return fabs(fz);  		
}









