//
// C++ Interface: material law
//
// Description: UMAT interface for crystal plasticity model
//
// Author:  <L. Noels>, (C) 2019
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef MLAWCRYSTALPLASTICITY_H_
#define MLAWCRYSTALPLASTICITY_H_
#include "mlawUMATInterface.h"
#include "ipCrystalPlasticity.h"

class mlawCrystalPlasticity : public mlawUMATInterface 
{

 protected:
  char *_matName;

 public:
  mlawCrystalPlasticity(const int num, const double rho, const double temperature, const char *propName, const char *matName);

 #ifndef SWIG
  mlawCrystalPlasticity(const mlawCrystalPlasticity &source);
  mlawCrystalPlasticity& operator=(const materialLaw &source);
  virtual ~mlawCrystalPlasticity();
  virtual materialLaw* clone() const {return new mlawCrystalPlasticity(*this);};
  virtual void checkInternalState(IPVariable* ipv, const IPVariable* ipvprev) const{}; // do nothing
  // function of materialLaw
  virtual matname getType() const{return materialLaw::crystalPlasticity;}

  virtual void createIPState(IPStateBase* &ips, bool hasBodyForce, const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const IntPt *GP=NULL, const int gpt = 0) const
  {
     Msg::Error("Cannot be called");
  }

  virtual void createIPState(IPCrystalPlasticity *ivi, IPCrystalPlasticity *iv1, IPCrystalPlasticity *iv2) const;
  virtual void createIPVariable(IPCrystalPlasticity *ipv,bool hasBodyForce,const MElement *ele,const int nbFF, const IntPt *GP, const int gpt) const;
  virtual void initLaws(const std::map<int,materialLaw*> &maplaw){}; // this law is initialized so nothing to do

  virtual void callUMAT(double *stress, double *statev, double **ddsdde, double &sse, double &spd, double &scd, double &rpl, 
                                 double *ddsddt, double *drplde, double &drpldt, double *stran, double *dtsran,
                                 double *tim, double timestep, double temperature, double deltaTemperature, double *predef, double *dpred,
                                 const char *CMNAME, int &ndi, int &nshr, int tensorSize, 
                                 int statevSize, double *prs, int matPropSize, double *coords, double **dRot,
                                 double &pnewdt, double &celent, double **F0, double **F1, 
                                 int &noel, int &npt, int &layer, int &kspt, int &kstep, int &kinc) const;    

  virtual const char* getCMNAME() const {return _matName;}
 public:

 #endif // SWIG
};

#endif // MLAWCRYSTALPLASTICITY_H_
