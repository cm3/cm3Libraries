#ifndef WEIGHT_PHASE_C  
#define WEIGHT_PHASE_C 1

#include "matrix_operations.h"
#include "Weight.h"
#include "rotations.h"
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

//#define pi 3.14159265
//******************************************************
//**** orientation distrubution function for each direction fiber  */
//**************************************************************************************

void getClosure(double a1, double a2, double a3, double A[3][3][3][3])
{
  double a1d[3][3], a2d[3][3], a3d[3][3];
  double Aclosure1D[3][3][3][3],Aclosure2D[3][3][3][3],Aclosure3D[3][3][3][3];
  double alpha1, alpha2, alpha3;
  int i,j,k,l;

  for(i=0;i<3;i++)
  {
    for(j=0;j<3;j++)
    {
      a1d[i][j]=a2d[i][j]=a3d[i][j]=0.;
    }
  }
  a1d[0][0]=1.;
  getQuadraticClosure(a1d,Aclosure1D);

  a2d[0][0]=a1/(a2+a1);
  a2d[1][1]=a2/(a2+a1);
  getHybridClosure(a2d,Aclosure2D,true);

  a3d[0][0]=a1;
  a3d[1][1]=a2;
  a3d[2][2]=a3;
  getHybridClosure(a3d,Aclosure3D,false);

  alpha1=(a1-a2)/a1;
  alpha2=(a2-a3)/a1;
  alpha3=(a3)/a1;

  for(i=0;i<3;i++)
  {
    for(j=0;j<3;j++)
    {
      for(k=0;k<3;k++)
      {
        for(l=0;l<3;l++)
        {
          A[i][j][k][l]=alpha1*Aclosure1D[i][j][k][l]+alpha2*Aclosure2D[i][j][k][l]+alpha3*Aclosure3D[i][j][k][l];
        }
      }    
    }
  }
}
void getLinearClosure(double a[3][3], double Al[3][3][3][3], bool is2D)
{
  int i,j,k,l;
  double alphal,betal;
  double I[3][3];
  double Ia[3][3][3][3],Iap1[3][3][3][3],Is[3][3][3][3];


  for(i=0;i<3;i++)
  {
    for(j=0;j<3;j++)
    {
      I[i][j]=0.;
    }
  }
  for(i=0;i<3;i++)
  {
    I[i][i]=1.;
  }
  for(i=0;i<3;i++)
  {
    for(j=0;j<3;j++)
    {
      for(k=0;k<3;k++)
      {
        for(l=0;l<3;l++)
        {
          Is[i][j][k][l]=0.5*I[i][k]*I[j][l]+0.5*I[i][l]*I[j][k];
          Ia[i][j][k][l]=0.5*a[i][k]*a[j][l]+0.5*a[i][l]*a[j][k];
          Iap1[i][j][k][l]=0.5*(a[i][k]+I[i][k])*(a[j][l]+I[j][l])+0.5*(a[i][l]+I[i][l])*(a[j][k]+I[j][k]);
        }
      }    
    }
  }
  alphal=-1./35.; betal=1./7.;
  if(is2D)
  {
    alphal=-1./24.; betal=1./6.;
  }

  for(i=0;i<3;i++)
  {
    for(j=0;j<3;j++)
    {
      for(k=0;k<3;k++)
      {
        for(l=0;l<3;l++)
        {
          Al[i][j][k][l]=alphal*I[i][j]*I[k][l]+2.*(alphal-betal)*Is[i][j][k][l]+betal*(I[i][j]*a[k][l]+a[i][j]*I[k][l])+2.*betal*(Iap1[i][j][k][l]-Ia[i][j][k][l]);
        }
      }    
    }
  }
}

void getQuadraticClosure(double a[3][3], double Aq[3][3][3][3])
{
  int i,j,k,l;
  for(i=0;i<3;i++)
  {
    for(j=0;j<3;j++)
    {
      for(k=0;k<3;k++)
      {
        for(l=0;l<3;l++)
        {
          Aq[i][j][k][l]=a[i][j]*a[k][l];
        }
      }    
    }
  }
}
void getHybridClosure(double a[3][3], double Ah[3][3][3][3], bool is2D)
{
  int i,j,k,l;
  double deta, Nh, f;
  double Al[3][3][3][3], Aq[3][3][3][3]; 
  double **atmp;
  mallocmatrix(&atmp,3,3);
  for(i=0;i<3;i++)
  {
    for(j=0;j<3;j++)
    {
      atmp[i][j]=a[i][j];
    }
  } 
  if(is2D)
  {
    for(i=0;i<2;i++)
    {
      atmp[i][2]=0.;
      atmp[2][i]=0.;
    }
    atmp[2][2]=1.;
  }

  deta=Determinant(atmp,3);
  Nh=27.;
  if(is2D)
  {
    deta=Determinant(atmp,2);
    Nh=4.;
  }
  f=1.-Nh*deta;

  getQuadraticClosure(a,Aq);
  getLinearClosure(a,Al,is2D);
  for(i=0;i<3;i++)
  {
    for(j=0;j<3;j++)
    {
      for(k=0;k<3;k++)
      {
        for(l=0;l<3;l++)
        {
          Ah[i][j][k][l]=(1.-f)*Al[i][j][k][l]+f*Aq[i][j][k][l];
        }
      }    
    }
  }
  freematrix(atmp,3);
}
double getODF(double p[3], double bofa[3][3], double bigBofa[3][3][3][3], bool is2D)
{
  int i,j,k,l;
  double trfp;
  double pp[3][3], fp[3][3];
  double Aq[3][3][3][3], Al[3][3][3][3];
  double psi,psi1, psi2, psi3;

  psi1=1./4./pi;
  psi2=15./8./pi;
  psi3=315./32./pi;
  if(is2D)
  {
    psi1=1./2./pi;
    psi2=2./pi;
    psi3=8./pi;
  }
  for(i=0;i<3;i++)
  {
    for(j=0;j<3;j++)
    {
      pp[i][j]=p[i]*p[j];
      fp[i][j]=p[i]*p[j];
    }
  }
  trfp=fp[0][0]+fp[1][1]+fp[2][2];
  for(i=0;i<3;i++)
  {
    fp[i][i]-=trfp/3.;
  }
  getLinearClosure(pp, Al, is2D);
  getQuadraticClosure(pp, Aq);
  psi=psi1;
  for(i=0;i<3;i++)
  {
    for(j=0;j<3;j++)
    {
      psi+=psi2*bofa[i][j]*fp[i][j];
      for(k=0;k<3;k++)
      {
        for(l=0;l<3;l++)
        {
          psi+=psi3*bigBofa[i][j][k][l]*(Aq[i][j][k][l]-Al[i][j][k][l]);
        }
      }
    }
  }
  return psi;
}
double generateWeight(int N, double **a, double **probDensityList, double **facetList, double **thetaList, double **phiList)
{
  int i,j,k,l,m,n,o,p;
  double *lambdaA;
  double **atmp, **eA, **Rot;
  double *thetaPrimGaussian,*deltaThetaPrimGaussian,*phiPrimGaussian,*deltaPhiPrimGaussian, **distr_odf;
  int eigOrderLarger, eigOrderMid, eigOrderSmaller;
  double A[3][3][3][3],Atmp[3][3][3][3], bigBofa[3][3][3][3], Al[3][3][3][3];
  double bofa[3][3], a33[3][3];
  double dir[3];
  double deltaTheta,theta,surfFacet,surfTotal,phi,deltaPhi,trbofa,probDensity;
  int *facesPerTheta;
  int totalFaces, Nphi;
  double f1=0.;
  double f2=0.;
  double f3=0.;
  double area=0.;
  double esp_theta=0.; //we should use the value of the principal direction as center!
  double va_theta=0.;
  double esp_phi=0.;
  double va_phi=0.;

  int Ind_Max_theta=0;
  double theta_max=0.;
  double dist_theta_max=0.;
  int Ind_Max_phi=0;
  double phi_max=0.;
  double dist_phi_max=0.;
  
  bool WithoutGaussian=false;
  double negValues=0.;
  int nbFacetGIden=45; //should be odd
  nbFacetGIden=24*N+1;

  mallocmatrix(&atmp,3,3);
  mallocvector(&lambdaA,3);
  mallocmatrix(&eA,3,3);
  mallocmatrix(&Rot,3,3);

  for(i=0;i<3;i++)
  {
    for(j=0;j<3;j++)
    {
      atmp[i][j]=a[i][j];
      bofa[i][j]=a[i][j];
      a33[i][j]=a[i][j];
    }
  } 
  trbofa=bofa[0][0]+bofa[1][1]+bofa[2][2];
  for(i=0;i<3;i++)
  {
    bofa[i][i]-=trbofa/3.;
  }
  getLinearClosure(a33, Al, false);
  Jacobian_Eigen( 3, atmp, lambdaA, eA);
  //here we need to check the order
  if(lambdaA[0]>=lambdaA[1] && lambdaA[1]>=lambdaA[2])
  {
    eigOrderLarger=0;
    eigOrderMid=1;
    eigOrderSmaller=2;
  }
  else if(lambdaA[0]>=lambdaA[2] && lambdaA[2]>=lambdaA[1])
  {
    eigOrderLarger=0;
    eigOrderMid=2;
    eigOrderSmaller=1;
  }
  else if(lambdaA[1]>=lambdaA[2] && lambdaA[2]>=lambdaA[0])
  {
    eigOrderLarger=1;
    eigOrderMid=2;
    eigOrderSmaller=0;
  }
  else if(lambdaA[1]>=lambdaA[0] && lambdaA[0]>=lambdaA[2])
  {
    eigOrderLarger=1;
    eigOrderMid=0;
    eigOrderSmaller=2;
  }
  else if(lambdaA[2]>=lambdaA[1] && lambdaA[1]>=lambdaA[0])
  {
    eigOrderLarger=2;
    eigOrderMid=1;
    eigOrderSmaller=0;
  }
  else if(lambdaA[2]>=lambdaA[0] && lambdaA[0]>=lambdaA[1])
  {
    eigOrderLarger=2;
    eigOrderMid=0;
    eigOrderSmaller=1;
  }
  getClosure(lambdaA[eigOrderLarger], lambdaA[eigOrderMid], lambdaA[eigOrderSmaller], Atmp);
  for(i=0;i<3;i++)
  {
    Rot[i][0]=eA[i][eigOrderLarger];
    Rot[i][1]=eA[i][eigOrderMid];
    Rot[i][2]=eA[i][eigOrderSmaller];
  } 
  for(i=0;i<3;i++)
  {
    for(j=0;j<3;j++)
    {
      for(k=0;k<3;k++)
      {
        for(l=0;l<3;l++)
        {
          A[i][j][k][l]=0.;
          for(m=0;m<3;m++)
          {
            for(n=0;n<3;n++)
            {
              for(o=0;o<3;o++)
              {
                for(p=0;p<3;p++)
                {
                  A[i][j][k][l]+=Rot[i][m]*Rot[j][n]*Rot[k][o]*Rot[l][p]*Atmp[m][n][o][p];
                }
              }
            }
          }
          bigBofa[i][j][k][l]=A[i][j][k][l]-Al[i][j][k][l];
        }
      }
    }
  }
  
 
  //for gaussian identification
  
  mallocvector(&thetaPrimGaussian,nbFacetGIden);
  mallocvector(&deltaThetaPrimGaussian,nbFacetGIden);
  mallocvector(&phiPrimGaussian,nbFacetGIden);
  mallocvector(&deltaPhiPrimGaussian,nbFacetGIden);
  mallocmatrix(&distr_odf,nbFacetGIden,nbFacetGIden);

  for(i=0;i<nbFacetGIden;i++)
  {
    if(i==0)
    {
      theta=0.;
      deltaTheta=pi/(nbFacetGIden-1)/2.;
    }
    else if(i==nbFacetGIden-1)
    {
      theta=pi;
      deltaTheta=pi/(nbFacetGIden-1)/2.;
    }
    else
    {
      theta=i*pi/(nbFacetGIden-1);
      deltaTheta=pi/(nbFacetGIden-1);
    }
    thetaPrimGaussian[i]=theta;
    deltaThetaPrimGaussian[i]=deltaTheta; //*sin(theta);
    for(j=0;j<nbFacetGIden;j++)
    {
      if(j==0)
      {
        phi=0.;
        deltaPhi=pi/(nbFacetGIden-1)/2.;
      }
      else if(j==nbFacetGIden-1)
      {
        phi=pi;
        deltaPhi=pi/(nbFacetGIden-1)/2.;
      }
      else
      {
        phi=j*pi/(nbFacetGIden-1);
        deltaPhi=pi/(nbFacetGIden-1);
      }
      phiPrimGaussian[j]=phi;
      deltaPhiPrimGaussian[j]=deltaPhi;
      dir[0]= cos(phi)*sin(theta);
      dir[1]= sin(phi)*sin(theta);
      dir[2]= cos(theta);
      distr_odf[i][j]=2.*getODF(dir,bofa, bigBofa, false); //*sin(theta);
      if(distr_odf[i][j]<0.)  negValues+=distr_odf[i][j]*deltaPhi;
     }   
  }
  if(negValues<-0.01) WithoutGaussian=false;
  else WithoutGaussian=true;
  //WithoutGaussian=true;
  if(!WithoutGaussian)
  {
    Ind_Max_theta=0;
    theta_max=0.;
    dist_theta_max=0.;
    Ind_Max_phi=0;
    phi_max=0.;
    dist_phi_max=0.;
 
    for(i=0;i<nbFacetGIden;i++){
      for(j=0;j<nbFacetGIden;j++){
        if(distr_odf[i][j]>distr_odf[Ind_Max_theta][Ind_Max_phi])
        {
          Ind_Max_theta=i;
          Ind_Max_phi=j;
        }
      }
    }
    theta_max=thetaPrimGaussian[Ind_Max_theta];
    if(theta_max>pi/2.) theta_max-=pi;//so between -pi/2 and pi/2
    phi_max=phiPrimGaussian[Ind_Max_phi];
    if(phi_max>pi/2.) phi_max-=pi;//so between -pi/2 and pi/2
    for(i=0;i<nbFacetGIden; i++){
      //We need to account for cyclic symmetry
      double thetaprim=thetaPrimGaussian[i]; 
      if(thetaprim>theta_max+pi/2.) thetaprim-=pi;
      thetaPrimGaussian[i]=thetaprim;
      double phiprim=phiPrimGaussian[i]; 
      if(phiprim>phi_max+pi/2.) phiprim-=pi;
      phiPrimGaussian[i]=phiprim;
    }      
    esp_theta=theta_max; //we should use the value of the principal direction as center!
    va_theta=0.;
    esp_phi=phi_max;
    va_phi=0.;

    //initial guess of variance
    int Ind_Max_theta_p1=Ind_Max_theta;
    int Ind_Max_phi_p1=Ind_Max_phi;
    for(i=1;i<nbFacetGIden/2.;i++)
    {
      int tmpInMaxp1=Ind_Max_theta+i;
      if(tmpInMaxp1>=nbFacetGIden) tmpInMaxp1-=nbFacetGIden;
      if(distr_odf[tmpInMaxp1][Ind_Max_phi] < distr_odf[Ind_Max_theta_p1][Ind_Max_phi] &&
         distr_odf[tmpInMaxp1][Ind_Max_phi]>0.) 
        Ind_Max_theta_p1=tmpInMaxp1;
      //if(distr_odf[tmpInMaxp1][Ind_Max_phi]<=0.) break;
      //if(distr_odf[tmpInMaxp1][Ind_Max_phi]< distr_odf[Ind_Max_theta_p1][Ind_Max_phi]) Ind_Max_theta_p1=tmpInMaxp1;
    }
    for(i=1;i<nbFacetGIden/2.;i++)
    {
      int tmpInMaxp1=Ind_Max_phi+i;
      if(tmpInMaxp1>=nbFacetGIden) tmpInMaxp1-=nbFacetGIden; 
      if(distr_odf[Ind_Max_theta][tmpInMaxp1]<distr_odf[Ind_Max_theta][Ind_Max_phi_p1] &&
         distr_odf[Ind_Max_theta][tmpInMaxp1]>0.)
         Ind_Max_phi_p1=tmpInMaxp1;
      //if(distr_odf[Ind_Max_theta][tmpInMaxp1]<=0.) break;
      //if(distr_odf[Ind_Max_theta][tmpInMaxp1]< distr_odf[Ind_Max_theta][Ind_Max_phi_p1]) Ind_Max_phi_p1=tmpInMaxp1;
    } 

   // if(Ind_Max_theta_p1>=nbFacetGIden) Ind_Max_theta_p1-=nbFacetGIden; 
   // if(Ind_Max_phi_p1>=nbFacetGIden) Ind_Max_phi_p1-=nbFacetGIden; 
    double ratioDistr_theta=distr_odf[Ind_Max_theta_p1][Ind_Max_phi]/distr_odf[Ind_Max_theta][Ind_Max_phi];
    double ratioDistr_phi=distr_odf[Ind_Max_theta][Ind_Max_phi_p1]/distr_odf[Ind_Max_theta][Ind_Max_phi];
    va_theta=3*pi;
    if(fabs(log(ratioDistr_theta))>1.e-12) 
      va_theta = -1./2./log(ratioDistr_theta)*((thetaPrimGaussian[Ind_Max_theta_p1] - esp_theta)*(thetaPrimGaussian[Ind_Max_theta_p1] - esp_theta)-
                                               (thetaPrimGaussian[Ind_Max_theta] - esp_theta)*(thetaPrimGaussian[Ind_Max_theta] - esp_theta));
    va_phi=3*pi;
    if(fabs(log(ratioDistr_phi))>1.e-12) 
      va_phi = -1./2./log(ratioDistr_phi)*((phiPrimGaussian[Ind_Max_phi_p1] - esp_phi)*(phiPrimGaussian[Ind_Max_phi_p1] - esp_phi)-
                                               (phiPrimGaussian[Ind_Max_phi] - esp_phi)*(phiPrimGaussian[Ind_Max_phi] - esp_phi));

    //Least square identification of Gaussian
    double old_va_theta=va_theta;
    double old_va_phi=va_phi;

    for(j=1;j<1000;j++)
    {
      va_theta= 1.e-3;
      double error=evaluateErrorGaussian(Ind_Max_phi, va_phi, true, va_theta ,esp_theta, distr_odf, thetaPrimGaussian, deltaThetaPrimGaussian,nbFacetGIden);
      for(i=0; i<10000; i++)
      {
        double va_theta_tmp=1.e-3+i*4*pi/10000.;
        double error1=evaluateErrorGaussian(Ind_Max_phi, va_phi, true, va_theta_tmp,esp_theta, distr_odf, thetaPrimGaussian, deltaThetaPrimGaussian,nbFacetGIden);
        if(error1< error)
        {
          va_theta=va_theta_tmp;
          error=error1;
          //printf("   iteration %d, va-of-Gaussian-theta, %e, error %e \n",i,va_theta,error);     
        }
      }
      va_phi= 1.e-3;
      error=evaluateErrorGaussian(Ind_Max_theta, va_theta, false, va_phi,esp_phi, distr_odf, phiPrimGaussian, deltaPhiPrimGaussian,nbFacetGIden);
      for(i=0; i<10000; i++)
      {
        double va_phi_tmp=1.e-3+i*4*pi/10000.;
        double error1=evaluateErrorGaussian(Ind_Max_theta, va_theta, false, va_phi_tmp, esp_phi, distr_odf, phiPrimGaussian, deltaPhiPrimGaussian,nbFacetGIden);
        if(error1< error)
        {
          va_phi=va_phi_tmp;
          error=error1;
          //printf("   iteration %d, va-of-Gaussian-phi, %e, error %e \n",i,va_phi,error);     
        }
      }
      printf("iteration %d,va-of-Gaussian-theta, %e,  va-of-Gaussian-phi, %e\n",j,va_theta,va_phi);     
      if(fabs(va_phi-old_va_phi)/old_va_phi<1.e-4 and fabs(va_theta-old_va_theta)/old_va_theta <1.e-4) break;
      old_va_theta = va_theta;
      old_va_phi=va_phi;
    }
    //verification
    f1=0.;
    f2=0.;
    f3=0.;
    area=0.;
 
    for(i=0;i<nbFacetGIden;i++)
    {
      theta=thetaPrimGaussian[i];
      phi=phiPrimGaussian[i];     
      f1+=( deltaThetaPrimGaussian[i]*(exp(-0.5*(theta - esp_theta)*(theta - esp_theta)/va_theta))/(sqrt(2.*pi*va_theta)) );
      f2+=( deltaPhiPrimGaussian[i]*(exp(-0.5*(phi - esp_phi)*(phi - esp_phi)/va_phi))/(sqrt(2.*pi*va_phi)) );
    }
    for(i=0;i<nbFacetGIden;i++)
    {
      theta=thetaPrimGaussian[i];
      for(j=0;j<nbFacetGIden;j++)
      {
        phi=phiPrimGaussian[j];
        f3+=( deltaPhiPrimGaussian[j]*(exp(-0.5*(phi - esp_phi)*(phi - esp_phi)/va_phi))/(sqrt(2.*pi*va_phi)) )*
            ( deltaThetaPrimGaussian[i]*(exp(-0.5*(theta - esp_theta)*(theta - esp_theta)/va_theta))/(sqrt(2.*pi*va_theta)) );
        area+=deltaThetaPrimGaussian[i]*deltaPhiPrimGaussian[j]*sin(theta);
      }
    }
    printf(" esp-of-Gaussian-theta: %e\n",esp_theta);
    printf(" variance-Gaussian-theta: %e\n",va_theta);
    printf(" check-Gaussian-associated-with-theta: %e\n",f1);
    printf(" esp-of-Gaussian-phi: %e\n",esp_phi);
    printf(" variance-Gaussian-phi: %e\n",va_phi);
    printf(" check-Gaussian-associated-with-phi: %e\n",f2);
    printf(" check-global-Gaussian-: %e\n",f3);
    printf(" Gaussian-identification-area: %e\n",area);
  }



  deltaTheta=pi/(N-1); //poles are of angle deltaTheta/2 and other of angle deltaTheta
  facesPerTheta = new int[N];
  totalFaces=0;

  for(i=0;i<N;i++)
  {
    if(i==0) 
    {
      theta=0.;
      Nphi=1;
    }
    else if(i==N-1)
    {
      theta=pi;
      Nphi=1;
    }
    else 
    {
      theta=i*deltaTheta;
      Nphi=(int)(((double)(N-1))*sin(theta));
      if((int)(Nphi/2)*2+1!=Nphi) Nphi+=1;
    } 
    facesPerTheta[i]=Nphi;
    totalFaces+=Nphi;
  }
   
  mallocvector(probDensityList,totalFaces);
  mallocvector(facetList,totalFaces);
  mallocvector(thetaList,totalFaces);
  mallocvector(phiList,totalFaces);

  k=0;
  surfTotal=0.;
  probDensity=0.;
  double *probDensityListOther;
  mallocvector(&probDensityListOther,totalFaces);

  for(i=0;i<N;i++)
  {
    if(i==0) 
    {
      theta=0.;
      surfFacet=pi*(1.-cos(deltaTheta/2.))/facesPerTheta[i];
    }
    else if(i==N-1)
    {
      theta=pi;
      surfFacet=pi*(1.-cos(deltaTheta/2.))/facesPerTheta[i];
    }
    else 
    {
      theta=i*deltaTheta;
      surfFacet=2.*pi*sin(theta)*sin(deltaTheta/2.)/(facesPerTheta[i]-1);
    }     
   
    for(j=0;j<facesPerTheta[i];j++)
    {
      if(j==0 && i!=0 && i!=N-1)
      {
        phi=0.;
        (*facetList)[k]= surfFacet/2.;
         deltaPhi=pi/(facesPerTheta[i]-1)/2.;
      }
      else if(j==facesPerTheta[i]-1 && i!=0 && i!=N-1)
      {
        phi=pi;
        (*facetList)[k]= surfFacet/2.;
         deltaPhi=pi/(facesPerTheta[i]-1)/2.;
      }
      else
      {
        if(i!=0 && i!=N-1)
        { 
          deltaPhi=pi/(facesPerTheta[i]-1);
          phi=j*deltaPhi;
        }
        else
        {
          phi=0.;
          deltaPhi=pi/(facesPerTheta[i]);          
        }
        (*facetList)[k]= surfFacet;
      }
      dir[0]= cos(phi)*sin(theta);
      dir[1]= sin(phi)*sin(theta);
      dir[2]= cos(theta);
      (*thetaList)[k]=theta;
      (*phiList)[k]=phi;  
      if(WithoutGaussian)
      {
        (*probDensityList)[k]=2.*getODF(dir,bofa, bigBofa, false);
      }
      else
      {
        double thetaprim=theta; 
        if(thetaprim>theta_max+pi/2.) thetaprim-=pi;
        double phiprim=phi; 
        if(phiprim>phi_max+pi/2.) phiprim-=pi;
          (*probDensityList)[k]= exp(-0.5*(phiprim-esp_phi)*(phiprim-esp_phi)/va_phi)/sqrt(2*pi*va_phi)*exp(-0.5*(thetaprim - esp_theta)*(thetaprim - esp_theta)/va_theta)/sqrt(2*pi*va_theta)/f1/f2;
        probDensityListOther[k]=2.*getODF(dir,bofa, bigBofa, false);
        //printf("theta %e, phi %e, Probability closure: %e, Probability Gauss: %e\n", theta, phi, probDensityListOther[k], (*probDensityList)[k]);
      }
      probDensity+=(*probDensityList)[k]*((*facetList)[k]);
      surfTotal+=(*facetList)[k];
      k+=1;
    }
  }
  for(k=0;k<totalFaces;k++)
  {
   (*probDensityList)[k]/= probDensity;
  }
  printf("Nfacet: %d, Surface: %e, Probability: %e\n",totalFaces,surfTotal,probDensity);

   
        


  free(lambdaA);
  freematrix(atmp,3);
  freematrix(eA,3);
  freematrix(Rot,3);
  free(facesPerTheta);
  free(thetaPrimGaussian);
  free(deltaThetaPrimGaussian);
  free(phiPrimGaussian);
  free(deltaPhiPrimGaussian);
  freematrix(distr_odf,nbFacetGIden);        
     

  //  printf(" nombre de case negatif: %d\n",nbn);

  return totalFaces;
}

double evaluateErrorGaussian(int fixAngleInd, double fixAngleVar, bool theta, double va, double esp, double **odf, double *angleList, double *dangleList, int nbFacetGIden)
{
  int i=fixAngleInd;
  int j=fixAngleInd;
  int nbpt=0;
  double ang=0.;
  double f=0.;
  double error=0.;
  for(int k=0;k<nbFacetGIden; k++)
  {
    if(theta) i=k; else j=k;
    double odfGauss=1./sqrt(2.*pi*va)*exp(-0.5*(angleList[k]-esp)*(angleList[k]-esp)/va);
    f+=odfGauss*dangleList[k];
  }
  for(int k=0;k<nbFacetGIden; k++)
  {
    if(theta) i=k; else j=k;
    double odfClos=odf[i][j]*sqrt(2.*pi*fixAngleVar); //we need to normalize)
    double odfGauss=1./sqrt(2.*pi*va)*exp(-0.5*(angleList[k]-esp)*(angleList[k]-esp)/va)/f;
    if(odfClos>0.)
    {
      nbpt+=1;
      error+=(odfClos-odfGauss)*(odfClos-odfGauss)*dangleList[k]*dangleList[k];
      ang+=dangleList[k];
    }
  }
  error=sqrt(error)/ang;
  return error;
}
//this file reads the input from ARtable.i01 generated by ~/cm3Data/Stochastic/BayesianIdentification/FiberDistribution/stat_plaque.py
void getARWeight(double theta, double phi, int Total_number_of_AR_per_phase, double *weightAR, double *ARList)
{

  FILE *ARtable=NULL;
  char line[1000];
  int Nfacets,NAR,i,j,index;
  double DAR,dangle;
  static double *listTheta=NULL;
  static double *listPhi=NULL;
  static double *listSurf=NULL;
  static double *listVol=NULL;
  static double **pdfAR=NULL;
  
  ARtable = fopen("ARtable.i01", "r");
  if(ARtable==NULL)
    printf("Error, no ARtable.i01 file\n");
  int okf = fscanf(ARtable,"%s%*[^\n]",line);
  okf =fscanf(ARtable, "%s", line);
  Nfacets = atoi(line);
  okf = fscanf(ARtable, "%s", line);
  NAR = atoi(line);
  if(NAR!=Total_number_of_AR_per_phase)
    printf("NAR!=Total_number_of_AR_per_phase\n");
  okf =fscanf(ARtable, "%s%*[^\n]", line);
  DAR = atof(line);

  if(listTheta==NULL)
  {
    mallocvector(&listTheta,Nfacets);
    mallocvector(&listPhi,Nfacets);
    mallocvector(&listSurf,Nfacets);
    mallocvector(&listVol,Nfacets);
    mallocmatrix(&pdfAR,Nfacets,NAR);
  }
  
  okf =fscanf(ARtable,"%s%*[^\n]",line);
  for(i=0;i<Nfacets;i++)
  {
    okf =fscanf(ARtable, "%s", line);
    listTheta[i]=atof(line);
    okf =fscanf(ARtable, "%s", line);
    listPhi[i]=atof(line);
    okf =fscanf(ARtable, "%s", line);
    listSurf[i]=atof(line);
    okf =fscanf(ARtable, "%s", line);
    listVol[i]=atof(line);
    for(j=0;j<NAR;j++)
    {
      if(j<NAR-1)
	okf =fscanf(ARtable, "%s", line);
      else
	okf =fscanf(ARtable, "%s%*[^\n]", line);
      pdfAR[i][j]=atof(line);
    }	
  }
  //identify closest direction
  dangle=100.;
  index=-1;
  for(i=0;i<Nfacets;i++)
  {
    if(fabs(theta-listTheta[i])+fabs(phi-listPhi[i])<dangle)
    {
      
      dangle=fabs(theta-listTheta[i])+fabs(phi-listPhi[i]);
      index=i;
    }
  }
  if(dangle>0.01)
    printf("The angles were not identified in ARtable.i01 \n");
  
  for(i=0; i<Total_number_of_AR_per_phase; i++)
  {
    weightAR[i]=pdfAR[index][i]*listVol[index];
    ARList[i]=((double)(i)+0.5)*DAR;
  }

  fclose(ARtable);
}
  
#endif
