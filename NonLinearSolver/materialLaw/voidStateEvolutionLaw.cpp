//
// C++ Interface: Void State
//
// Description: Storing class for computation of void state
//
//
// Author:  <V.-D. Nguyen>, (C) 2019
//
// Copyright: See COPYING file that comes with this distribution
//

#include "voidStateEvolutionLaw.h"
#include "ipNonLocalPorosity.h"

voidStateEvolutionLaw::voidStateEvolutionLaw(const int num): _num(num),_voidLigamentRatioAtFailure(0.99), _ChiRegularizedFunction(NULL){
};
voidStateEvolutionLaw::voidStateEvolutionLaw(const voidStateEvolutionLaw& src):_voidLigamentRatioAtFailure(src._voidLigamentRatioAtFailure)
{
  _ChiRegularizedFunction = NULL;
  if (src._ChiRegularizedFunction != NULL){
    _ChiRegularizedFunction = src._ChiRegularizedFunction->clone();
  }
};

void voidStateEvolutionLaw::setRegularizedFunction(const scalarFunction& f){
  if (_ChiRegularizedFunction != NULL){
    delete _ChiRegularizedFunction;
  }
  _ChiRegularizedFunction = f.clone();
};

voidStateEvolutionLaw::~voidStateEvolutionLaw()
{
  if (_ChiRegularizedFunction != NULL){
    delete _ChiRegularizedFunction;
     _ChiRegularizedFunction = NULL;
  }
}


TrivialEvolutionLaw::TrivialEvolutionLaw(const int num): voidStateEvolutionLaw(num){

};

void TrivialEvolutionLaw::createIPVariable(const double f0, IPVoidState* &ipv) const {
  if(ipv != NULL) delete ipv;
  ipv = new IPVoidState(f0,1.,1,0.5);
};

void TrivialEvolutionLaw::evolve(const double yieldfV0, const double yieldfV1,
                        const double DeltahatD, const double DeltahatQ, const double DeltahatP,
                        const IPNonLocalPorosity *q0Porous, const IPNonLocalPorosity *q1Porous,
                        const IPVoidState& sPrev, IPVoidState& sCur) const
{
  sCur.copyAndZeroDerivativesFrom(sPrev);
};

SphericalVoidStateEvolutionLawWithMatrixPlasticDeformation::SphericalVoidStateEvolutionLawWithMatrixPlasticDeformation(const int num, const double l0, const double k, const double W0):
voidStateEvolutionLaw(num),_lambda0(l0),_kappa(k), _W0(W0)
{

};
SphericalVoidStateEvolutionLawWithMatrixPlasticDeformation::SphericalVoidStateEvolutionLawWithMatrixPlasticDeformation(const SphericalVoidStateEvolutionLawWithMatrixPlasticDeformation& src):
  voidStateEvolutionLaw(src),_lambda0(src._lambda0),_kappa(src._kappa), _W0(src._W0)
{

}
SphericalVoidStateEvolutionLawWithMatrixPlasticDeformation::~SphericalVoidStateEvolutionLawWithMatrixPlasticDeformation() {
  
};


void SphericalVoidStateEvolutionLawWithMatrixPlasticDeformation::createIPVariable(const double f0, IPVoidState* &ipv) const{
  if(ipv != NULL) delete ipv;
  ipv = new IPVoidState(f0,_lambda0,_W0,0.5);
};
// in general, physics changes when coalesence occurs
void SphericalVoidStateEvolutionLawWithMatrixPlasticDeformation::evolve(const double yieldfV0, const double yieldfV1,
                        const double DeltahatD, const double DeltahatQ, const double DeltahatP,
                        const IPNonLocalPorosity *q0Porous, const IPNonLocalPorosity *q1Porous,
                        const IPVoidState& sPrev, IPVoidState& sCur) const
{
  sCur.copyAndZeroDerivativesFrom(sPrev);
  if (((_ChiRegularizedFunction == NULL) and sCur.isSaturated()) || q0Porous->dissipationIsBlocked()) 
  {
    // do nothing if ip already saturated without using regularization function for chi
    return;
  }
  
  if (DeltahatP >= 0.)
  {
    // void shape factor and void aspect ratio do not change
    // update void spacing from previous state
    double lambdaPrev = sPrev.getVoidSpacingRatio();
    double& lambda = sCur.getRefToVoidSpacingRatio();
    double& DlambdaDHatP = sCur.getRefToDVoidSpacingRatioDMatrixPlasticDeformation();
    
    lambda = lambdaPrev*exp(_kappa*DeltahatP);
    DlambdaDHatP =  _kappa*lambda;
    
    // compute void ligament ratio
    // we have X = [(3*gamma*f*lambda)/W]^(1/3)
    // we also Xprev = [(3*gamma*fPrev*lambdaPrev)/W]^(1/3)
    // leading (X/Xprev)^3 = f*lambda/(fPrev*lambdaPrev)  = fact
    //
    double fact = yieldfV1*lambda/(yieldfV0*lambdaPrev);
    // estimate rough value of void ligamant
    double roughChiPrev = sPrev.getRoughVoidLigamentRatio();
    double& roughChi = sCur.getRefToRoughVoidLigamentRatio();
    roughChi = roughChiPrev*pow(fact,1./3.);
    double droughChidf =  roughChi/(3.*yieldfV1);
    double droughChidhatP = DlambdaDHatP*roughChi/(3.*lambda);
    
    // void ligament ratio
    double& Chi = sCur.getRefToVoidLigamentRatio();
    double& dChidf = sCur.getRefToDVoidLigamentRatioDYieldPorosity();
    double& dChidhatP = sCur.getRefToDVoidLigamentRatioDMatrixPlasticDeformation();
    if (_ChiRegularizedFunction != NULL)
    {
      Chi = _ChiRegularizedFunction->getVal(roughChi);
      double DiffChiComputed = _ChiRegularizedFunction->getDiff(roughChi);
      dChidf = DiffChiComputed*droughChidf;
      dChidhatP = DiffChiComputed*droughChidhatP;
      if (!sPrev.isSaturated())
      {
        // check saturate
        if (Chi >= _voidLigamentRatioAtFailure)
        {
          sCur.saturate(true);
        }
        else
        {
          sCur.saturate(false);
        }
      }
    }
    else
    {
      Chi = roughChi;
      dChidf = droughChidf;
      dChidhatP = droughChidhatP;
      // estimate void ligament
      if (Chi >= _voidLigamentRatioAtFailure)
      {
        sCur.saturate(true);
        double chiPrev = sPrev.getVoidLigamentRatio();
        double ff = (_voidLigamentRatioAtFailure - chiPrev)/(Chi - chiPrev);
        Chi = _voidLigamentRatioAtFailure;
        dChidf *= ff;
        dChidhatP *= ff;
      }
      else
      {
        sCur.saturate(false);
      }
    }
  }
};


SphericalVoidStateEvolutionLawWithDeviatoricPlasticDeformation::SphericalVoidStateEvolutionLawWithDeviatoricPlasticDeformation(const int num, const double l0, const double k, const double W0):
SphericalVoidStateEvolutionLawWithMatrixPlasticDeformation(num,l0,k,W0){};
SphericalVoidStateEvolutionLawWithDeviatoricPlasticDeformation::SphericalVoidStateEvolutionLawWithDeviatoricPlasticDeformation(const SphericalVoidStateEvolutionLawWithDeviatoricPlasticDeformation& src):
SphericalVoidStateEvolutionLawWithMatrixPlasticDeformation(src){};
SphericalVoidStateEvolutionLawWithDeviatoricPlasticDeformation::~SphericalVoidStateEvolutionLawWithDeviatoricPlasticDeformation(){};

void SphericalVoidStateEvolutionLawWithDeviatoricPlasticDeformation::evolve(const double yieldfV0, const double yieldfV1,
                        const double DeltahatD, const double DeltahatQ, const double DeltahatP,
                        const IPNonLocalPorosity *q0Porous, const IPNonLocalPorosity *q1Porous,
                        const IPVoidState& sPrev, IPVoidState& sCur) const
{
  sCur.copyAndZeroDerivativesFrom(sPrev);
  if (((_ChiRegularizedFunction == NULL) and sCur.isSaturated()) || q0Porous->dissipationIsBlocked()) 
  {
    // do nothing if ip already saturated without using regularization function for chi
    return;
  }
 
  if (DeltahatD >= 0.)
  {
    // void shape factor and void aspect ratio do not change
    // update void spacing from previous state
    double lambdaPrev = sPrev.getVoidSpacingRatio();
    double& lambda = sCur.getRefToVoidSpacingRatio();
    double& DlambdaDHatD = sCur.getRefToDVoidSpacingRatioDDeviatoricPlasticDeformation();

    lambda = lambdaPrev*exp(_kappa*DeltahatD);
    DlambdaDHatD =  _kappa*lambda;
    
    
    // compute void ligament ratio
    // we have X = [(3*gamma*f*lambda)/W]^(1/3)
    // we also Xprev = [(3*gamma*fPrev*lambdaPrev)/W]^(1/3)
    // leading (X/Xprev)^3 = f*lambda/(fPrev*lambdaPrev)  = fact
    //
    double fact = yieldfV1*lambda/(yieldfV0*lambdaPrev);
    // estimate rough value of void ligamant
    double roughChiPrev = sPrev.getRoughVoidLigamentRatio();
    double& roughChi = sCur.getRefToRoughVoidLigamentRatio();
    roughChi = roughChiPrev*pow(fact,1./3.);
    double droughChidf =  roughChi/(3.*yieldfV1);
    double droughChidhatD = DlambdaDHatD*roughChi/(3.*lambda);
    
    // void ligament ratio
    double& Chi = sCur.getRefToVoidLigamentRatio();
    double& dChidf = sCur.getRefToDVoidLigamentRatioDYieldPorosity();
    double& dChidhatD = sCur.getRefToDVoidLigamentRatioDDeviatoricPlasticDeformation();
    if (_ChiRegularizedFunction != NULL)
    {
      Chi = _ChiRegularizedFunction->getVal(roughChi);
      double DiffChiComputed = _ChiRegularizedFunction->getDiff(roughChi);
      dChidf = DiffChiComputed*droughChidf;
      dChidhatD = DiffChiComputed*droughChidhatD;
      // check saturate
      if (!sPrev.isSaturated())
      {
        if (Chi >= _voidLigamentRatioAtFailure)
        {
          sCur.saturate(true);
        }
        else
        {
          sCur.saturate(false);
        }
      }
    }
    else
    {
      Chi = roughChi;
      dChidf = droughChidf;
      dChidhatD = droughChidhatD;
      // estimate void ligament
      if (Chi >= _voidLigamentRatioAtFailure)
      {
        sCur.saturate(true);
        double chiPrev = sPrev.getVoidLigamentRatio();
        double ff = (_voidLigamentRatioAtFailure - chiPrev)/(Chi - chiPrev);
        Chi = _voidLigamentRatioAtFailure;
        dChidf *= ff;
        dChidhatD *= ff;
      }
      else
      {
        sCur.saturate(false);
      }
    }
  }
};

SpheroidalVoidStateEvolutionLawWithAspectRatioEvolution::SpheroidalVoidStateEvolutionLawWithAspectRatioEvolution(const int num, const double l0, const double W0, const double k):
voidStateEvolutionLaw(num),_lambda0(l0),_W0(W0),_kappa(k){
};

SpheroidalVoidStateEvolutionLawWithAspectRatioEvolution::SpheroidalVoidStateEvolutionLawWithAspectRatioEvolution(const SpheroidalVoidStateEvolutionLawWithAspectRatioEvolution& src):
  voidStateEvolutionLaw(src),_lambda0(src._lambda0),_W0(src._W0),_kappa(src._kappa){
}
SpheroidalVoidStateEvolutionLawWithAspectRatioEvolution::~SpheroidalVoidStateEvolutionLawWithAspectRatioEvolution() {

};

void SpheroidalVoidStateEvolutionLawWithAspectRatioEvolution::createIPVariable(const double f0, IPVoidState* &ipv) const{
  if(ipv != NULL) delete ipv;
  ipv = new IPVoidState(f0,_lambda0,_W0,0.5);
};
// in general, physics changes when coalesence occurs
void SpheroidalVoidStateEvolutionLawWithAspectRatioEvolution::evolve(const double yieldfV0, const double yieldfV1,
                        const double DeltahatD, const double DeltahatQ, const double DeltahatP,
                        const IPNonLocalPorosity *q0Porous, const IPNonLocalPorosity *q1Porous,
                        const IPVoidState& sPrev, IPVoidState& sCur) const
{
  sCur.copyAndZeroDerivativesFrom(sPrev);
  if (((_ChiRegularizedFunction == NULL) and sCur.isSaturated()) || q0Porous->dissipationIsBlocked()) 
  {
    // do nothing if ip already saturated without using regularization function for chi
    return;
  }

  // evolutation with plastic deformation
  if (DeltahatD >= 0.)
  {
    // previous values
    double lambdaPrev = sPrev.getVoidSpacingRatio();
    double roughXPrev = sPrev.getRoughVoidLigamentRatio();
    double WPrev = sPrev.getVoidAspectRatio();
    // Get ipvs    
    double& roughX = sCur.getRefToRoughVoidLigamentRatio();

    double& lambda = sCur.getRefToVoidSpacingRatio();
    double& DlambdaDhatD = sCur.getRefToDVoidSpacingRatioDDeviatoricPlasticDeformation();

    // void spacing
    lambda = lambdaPrev*exp(_kappa*DeltahatD);
    DlambdaDhatD = _kappa*lambda;
    //
    double Zprev = roughXPrev*WPrev;
    double DeltaZ = 0.;
    double DZdHatD = 0.;
    if (_kappa > 0.)
    {      
      DeltaZ =  1.5*(lambda-lambdaPrev)/_kappa;
      DZdHatD = 1.5*DlambdaDhatD/_kappa;
    }
    else
    {
      DeltaZ = 1.5*lambda*DeltahatD;
      DZdHatD = 1.5*lambda;
    }
    double Z = Zprev+DeltaZ;

    roughX = sqrt(1.5-(1.5-roughXPrev*roughXPrev)*Zprev/Z);
    double droughXdHatD = (3./4.)*(lambda*roughX/Z)*(1.5/roughX/roughX-1.);
    
    // void ligament ratio
    double& X = sCur.getRefToVoidLigamentRatio();
    double& dXdhatD = sCur.getRefToDVoidLigamentRatioDDeviatoricPlasticDeformation();
    if (_ChiRegularizedFunction != NULL)
    {
      X = _ChiRegularizedFunction->getVal(roughX);
      double DiffChiComputed = _ChiRegularizedFunction->getDiff(roughX);
      dXdhatD = DiffChiComputed*droughXdHatD;
      // check saturate
      if (!sPrev.isSaturated())
      {
        if (X >= _voidLigamentRatioAtFailure)
        {
          sCur.saturate(true);
        }
        else
        {
          sCur.saturate(false);
        }
      }
    }
    else
    {
      X = roughX;
      dXdhatD = droughXdHatD;
      // estimate void ligament
      if (X >= _voidLigamentRatioAtFailure)
      {
        sCur.saturate(true);
        double XPrev = sPrev.getVoidLigamentRatio();
        double ff = (_voidLigamentRatioAtFailure - XPrev)/(X - XPrev);
        X = _voidLigamentRatioAtFailure;
        dXdhatD *= ff;
      }
      else
      {
        sCur.saturate(false);
      }
    }
    double& W = sCur.getRefToVoidAspectRatio();
    double& dWdHatD = sCur.getRefToDVoidAspectRatioDDeviatoricPlasticDeformation();
    W = Z/roughX;
    dWdHatD = (9./4.)*(lambda/roughX)*(1.-0.5/roughX/roughX);
  };
};

VoidStateLinearEvolutionLaw::VoidStateLinearEvolutionLaw(const int num, const double l0, const double kappa, const double kL):
  voidStateEvolutionLaw(num), _lambda0(l0), _kappa(kappa),_kLigament(kL){};

VoidStateLinearEvolutionLaw::VoidStateLinearEvolutionLaw(const VoidStateLinearEvolutionLaw& src):
    voidStateEvolutionLaw(src), _lambda0(src._lambda0), _kappa(src._kappa),_kLigament(src._kLigament){}

void VoidStateLinearEvolutionLaw::createIPVariable(const double f0, IPVoidState* &ipv) const
{
  if(ipv != NULL) delete ipv;
  ipv = new IPVoidState(f0,_lambda0,1.,0.5);
}

void VoidStateLinearEvolutionLaw::evolve(const double yieldfV0, const double yieldfV1,
                        const double DeltahatD, const double DeltahatQ, const double DeltahatP,
                        const IPNonLocalPorosity *q0Porous, const IPNonLocalPorosity *q1Porous,
                        const IPVoidState& sPrev, IPVoidState& sCur) const
{
  sCur.copyAndZeroDerivativesFrom(sPrev);
  if (((_ChiRegularizedFunction == NULL) and sCur.isSaturated()) || q0Porous->dissipationIsBlocked()) 
  {
    // do nothing if ip already saturated without using regularization function for chi
    return;
  }
  
  if (DeltahatD >= 0.)
  {
    double roughChiPrev = sPrev.getRoughVoidLigamentRatio();
    double& roughChi = sCur.getRefToRoughVoidLigamentRatio();
    // previous values
    double XPrev = sPrev.getVoidLigamentRatio();
    double& X = sCur.getRefToVoidLigamentRatio();
    double& dXdHatD = sCur.getRefToDVoidLigamentRatioDDeviatoricPlasticDeformation();

    roughChi = roughChiPrev + _kLigament*DeltahatD;
    double droughChidHatD = _kLigament;
    
    // void ligament ratio
    if (_ChiRegularizedFunction != NULL)
    {
      X = _ChiRegularizedFunction->getVal(roughChi);
      double DiffChiComputed = _ChiRegularizedFunction->getDiff(roughChi);
      dXdHatD = DiffChiComputed*droughChidHatD;
      // check saturate
      if (!sPrev.isSaturated())
      {
        if (X >= _voidLigamentRatioAtFailure)
        {
          sCur.saturate(true);
        }
        else
        {
          sCur.saturate(false);
        }
      }
    }
    else
    {
      X = roughChi;
      dXdHatD = droughChidHatD;
      // estimate void ligament
      if (X >= _voidLigamentRatioAtFailure)
      {
        sCur.saturate(true);
        double ff = (_voidLigamentRatioAtFailure - XPrev)/(X - XPrev);
        X = _voidLigamentRatioAtFailure;
        dXdHatD *= ff;
      }
      else
      {
        sCur.saturate(false);
      }
    }
    double& lambda = sCur.getRefToVoidSpacingRatio();
    double& DlambdaDhatD = sCur.getRefToDVoidSpacingRatioDDeviatoricPlasticDeformation();
    double fVPrev = q0Porous->getYieldPorosity();
    lambda = 2.*X*X*X/(3.*fVPrev);
    DlambdaDhatD = (3.*lambda/X)*dXdHatD;
  }
};


VoidStateExponentialEvolutionLaw::VoidStateExponentialEvolutionLaw(const int num, const double l0, const double kappa, const double kL):
  VoidStateLinearEvolutionLaw(num,l0,kappa,kL){
  _voidLigamentRatioAtFailure = 0.999;  
};

VoidStateExponentialEvolutionLaw::VoidStateExponentialEvolutionLaw(const VoidStateExponentialEvolutionLaw& src):
    VoidStateLinearEvolutionLaw(src){}

void VoidStateExponentialEvolutionLaw::evolve(const double yieldfV0, const double yieldfV1,
                        const double DeltahatD, const double DeltahatQ, const double DeltahatP,
                        const IPNonLocalPorosity *q0Porous, const IPNonLocalPorosity *q1Porous,
                        const IPVoidState& sPrev, IPVoidState& sCur) const
{
  sCur.copyAndZeroDerivativesFrom(sPrev);
  if (((_ChiRegularizedFunction == NULL) and sCur.isSaturated()) || q0Porous->dissipationIsBlocked()) 
  {
    // do nothing if ip already saturated without using regularization function for chi
    return;
  }
  
  
  if (DeltahatD >= 0.)
  {
    // previous values
    double XPrev = sPrev.getVoidLigamentRatio();
    double& X = sCur.getRefToVoidLigamentRatio();
    double& dXdHatD = sCur.getRefToDVoidLigamentRatioDDeviatoricPlasticDeformation();

     // void spacing
    X = _voidLigamentRatioAtFailure- (_voidLigamentRatioAtFailure-XPrev)*exp(-_kLigament*DeltahatD);
    dXdHatD = (_voidLigamentRatioAtFailure-X)*_kLigament;
    // check saturate
    if (!sPrev.isSaturated())
    {
      if (X >= _voidLigamentRatioAtFailure)
      {
        sCur.saturate(true);
      }
      else
      {
        sCur.saturate(false);
      }
    }
    sCur.getRefToRoughVoidLigamentRatio()  = sCur.getVoidLigamentRatio();
  
    double& lambda = sCur.getRefToVoidSpacingRatio();
    double& DlambdaDhatD = sCur.getRefToDVoidSpacingRatioDDeviatoricPlasticDeformation();
    double fVPrev = q0Porous->getYieldPorosity();
    lambda = 2.*X*X*X/(3.*fVPrev);
    DlambdaDhatD = (3.*lambda/X)*dXdHatD;
  }
};