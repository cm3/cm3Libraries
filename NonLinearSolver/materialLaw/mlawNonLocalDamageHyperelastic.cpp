//
// C++ Interface: material law
//
// Description: mlawNonLocalDamageQuadYieldHyper
//
// Author:  V.D. Nguyen, (C) 2014
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "mlawNonLocalDamageHyperelastic.h"
#include "nonLinearMechSolver.h"


mlawNonLocalDamagePowerYieldHyper::mlawNonLocalDamagePowerYieldHyper(const int num,const double E,const double nu, const double rho,
				const double tol, const bool matrixbyPerturbation , const double pert):
				mlawPowerYieldHyper(num,E,nu,rho,tol,matrixbyPerturbation,pert),cLLaw(NULL),damLaw(NULL){};



mlawNonLocalDamagePowerYieldHyper::mlawNonLocalDamagePowerYieldHyper(const mlawNonLocalDamagePowerYieldHyper &source):mlawPowerYieldHyper(source){
  cLLaw = NULL;
  damLaw = NULL;
  if(source.cLLaw != NULL)
  {
    cLLaw=source.cLLaw->clone();
  }
  if(source.damLaw != NULL)
  {
    damLaw=source.damLaw->clone();
  };
}

void mlawNonLocalDamagePowerYieldHyper::setCLengthLaw(const CLengthLaw &_cLLaw){
  if (cLLaw) delete cLLaw;
  cLLaw = _cLLaw.clone();
};
void mlawNonLocalDamagePowerYieldHyper::setDamageLaw(const DamageLaw &_damLaw){
  if (damLaw) delete damLaw;
  damLaw = _damLaw.clone();
};


mlawNonLocalDamagePowerYieldHyper::~mlawNonLocalDamagePowerYieldHyper(){
  if (cLLaw!= NULL) delete cLLaw;
  if (damLaw!= NULL) delete damLaw;
};

void mlawNonLocalDamagePowerYieldHyper::createIPState(IPStateBase* &ips, bool hasBodyForce,const bool* state_,const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
  IPVariable* ipvi = new IPHyperViscoElastoPlasticNonLocalDamage(_compression,_traction,_kinematic,_N,cLLaw,damLaw);
  IPVariable* ipv1 = new IPHyperViscoElastoPlasticNonLocalDamage(_compression,_traction,_kinematic,_N,cLLaw,damLaw);
  IPVariable* ipv2 = new IPHyperViscoElastoPlasticNonLocalDamage(_compression,_traction,_kinematic,_N,cLLaw,damLaw);
  if(ips != NULL) delete ips;
  ips = new IP3State(state_,ipvi,ipv1,ipv2);
}
void mlawNonLocalDamagePowerYieldHyper::createIPState(IPHyperViscoElastoPlasticNonLocalDamage *ivi, IPHyperViscoElastoPlasticNonLocalDamage *iv1, IPHyperViscoElastoPlasticNonLocalDamage *iv2) const
{

}
void mlawNonLocalDamagePowerYieldHyper::createIPVariable(IPHyperViscoElastoPlasticNonLocalDamage *&ipv,bool hasBodyForce, const MElement *ele,const int nbFF,const IntPt *GP, const int gpt) const
{

}

double mlawNonLocalDamagePowerYieldHyper::soundSpeed() const
{
  return mlawPowerYieldHyper::soundSpeed();
}

void mlawNonLocalDamagePowerYieldHyper::constitutive(const STensor3& F0,
                                                     const STensor3& Fn,
                                                     STensor3 &P,
                                                     const IPHyperViscoElastoPlasticNonLocalDamage *q0,
	                                             IPHyperViscoElastoPlasticNonLocalDamage *q1,
                                                     STensor43 &Tangent,
                                                     const bool stiff, 
                                                     const bool dTangentdeps,
                                                     STensor63* dCalgdeps) const
{
  mlawPowerYieldHyper::constitutive(F0,Fn,P,q0,q1,Tangent,stiff);
}

void mlawNonLocalDamagePowerYieldHyper::constitutive(
                            const STensor3& F0,         // initial deformation gradient (input @ time n)
                            const STensor3& Fn,         // updated deformation gradient (input @ time n+1)
                            STensor3 &P,                // updated 1st Piola-Kirchhoff stress tensor (output)
                                                        // contains the initial values on input
                            const IPHyperViscoElastoPlasticNonLocalDamage *ipvprev,       // array of initial internal variable
                            IPHyperViscoElastoPlasticNonLocalDamage *ipvcur,             // updated array of internal variable (in ipvcur on output),
                            STensor43 &Tangent,         // constitutive tangents (output)
                            STensor3  &dLocalPlasticStrainDStrain,
                            STensor3  &dStressDNonLocalPlasticStrain,
                            double   &dLocalPlasticStrainDNonLocalPlasticStrain,
                            const bool stiff,            // if true compute the tangents
                            STensor43 *elasticTangent,
                            const bool dTangentdeps,
                            STensor63* dCalgdeps) const
{
  double p0 = ipvprev->getCurrentPlasticStrain();
  cLLaw->computeCL(p0, ipvcur->getRefToIPCLength());

  static STensor43 dFedF, dFpdF;
  static STensor3 Peff;
  mlawPowerYieldHyper::predictorCorrector(Fn,ipvprev,ipvcur,Peff,stiff,Tangent,dFedF,dFpdF,elasticTangent);

  const STensor3& Fe = ipvcur->_Fe;
  const STensor3& dgammadF = ipvcur->_DgammaDF;
  const STensor3 &Fp  = ipvcur->getConstRefToFp();

  double ene = ipvcur->defoEnergy();
  //Msg::Info("enery = %e",ene);

  damLaw->computeDamage(ipvcur->getEffectivePlasticStrain(),
                        ipvprev->getEffectivePlasticStrain(),
                        ene, Fe, Fp, Peff, Cel,
                        ipvprev->getConstRefToIPDamage(),ipvcur->getRefToIPDamage());


  double D = ipvcur->getDamage();
  double Dprev = ipvprev->getDamage();
  P = Peff;
  P*=(1.-D);

  ipvcur->getRefToElasticEnergy() *= (1-D);
  double DenerPlas = ipvcur->plasticEnergy() - ipvprev->plasticEnergy();
  ipvcur->getRefToPlasticEnergy() = ipvprev->plasticEnergy()+ DenerPlas*(1-D);
  ipvcur->getRefToDamageEnergy() = ipvprev->damageEnergy()+ ene*(D-Dprev);

  if (this->getMacroSolver()->getPathFollowingLocalIncrementType() == pathFollowingManager::DEFO_ENERGY){
    ipvcur->getRefToIrreversibleEnergy() = ipvcur->defoEnergy();
  }
  else if (this->getMacroSolver()->getPathFollowingLocalIncrementType() == pathFollowingManager::PLASTIC_ENERGY) {
    ipvcur->getRefToIrreversibleEnergy() = ipvcur->plasticEnergy();
  }
  else if (this->getMacroSolver()->getPathFollowingLocalIncrementType() == pathFollowingManager::DAMAGE_ENERGY){
    ipvcur->getRefToIrreversibleEnergy() = ipvcur->damageEnergy();
  }
  else if (this->getMacroSolver()->getPathFollowingLocalIncrementType() == pathFollowingManager::DISSIPATION_ENERGY){
    ipvcur->getRefToIrreversibleEnergy() = ipvcur->plasticEnergy()+ipvcur->damageEnergy();
  }
  else{
    ipvcur->getRefToIrreversibleEnergy() = 0.;
  }

  if(stiff)
  {
    // we need to correct partial P/partial F: (1-D) partial P/partial F - Peff otimes partial D partial F
    static STensor3 dDdF;
    STensorOperation::multSTensor3STensor43(ipvcur->getConstRefToDDamageDFe(),dFedF,dDdF);

    Tangent*=(1.-D);

    for(int i=0;i<3;i++){
      for(int j=0;j<3;j++){
        for(int k=0;k<3;k++){
          for(int l=0;l<3;l++){
            Tangent(i,j,k,l)-=Peff(i,j)*dDdF(k,l);
          }
        }
      }
    }


    // partial p/partial F
    dLocalPlasticStrainDStrain = dgammadF;

    // -hat{P} partial D/partial tilde p
    dStressDNonLocalPlasticStrain = Peff*(-1*ipvcur->getDDamageDp());

    // partial p partial tilde p (0 if no MFH)
    dLocalPlasticStrainDNonLocalPlasticStrain = 0.;

    STensor3& DirrEnergDF = ipvcur->getRefToDIrreversibleEnergyDF();
    if (this->getMacroSolver()->getPathFollowingLocalIncrementType() == pathFollowingManager::DEFO_ENERGY){
      DirrEnergDF*= (1-D);
      DirrEnergDF.daxpy(dDdF,-ene);
      ipvcur->getRefToDIrreversibleEnergyDNonLocalVariable() = -ene*ipvcur->getDDamageDp();
    }
    else if (this->getMacroSolver()->getPathFollowingLocalIncrementType() == pathFollowingManager::PLASTIC_ENERGY) {
      DirrEnergDF*= (1-D);
      DirrEnergDF.daxpy(dDdF,-DenerPlas);
      ipvcur->getRefToDIrreversibleEnergyDNonLocalVariable() = -DenerPlas*ipvcur->getDDamageDp();
    }
    else if (this->getMacroSolver()->getPathFollowingLocalIncrementType() == pathFollowingManager::DAMAGE_ENERGY){
      for(int i=0;i<3;i++){
        for(int j=0;j<3;j++){
          DirrEnergDF(i,j) = 0.;
          for(int k=0;k<3;k++){
            for(int l=0;l<3;l++){
              for(int m=0; m<3; m++){
                DirrEnergDF(i,j) += (D - Dprev)*Peff(k,m)*Fp(l,m)*dFedF(k,l,i,j);
              };
            }
          }
        }
      }
      DirrEnergDF.daxpy(dDdF,ene);
      ipvcur->getRefToDIrreversibleEnergyDNonLocalVariable() = ene*ipvcur->getDDamageDp();
    }
    else if (this->getMacroSolver()->getPathFollowingLocalIncrementType() == pathFollowingManager::DISSIPATION_ENERGY){
      DirrEnergDF*= (1-D);
      for(int i=0;i<3;i++){
        for(int j=0;j<3;j++){
          for(int k=0;k<3;k++){
            for(int l=0;l<3;l++){
              for(int m=0; m<3; m++){
                DirrEnergDF(i,j) += (D - Dprev)*Peff(k,m)*Fp(l,m)*dFedF(k,l,i,j);
              };
            }
          }
        }
      }
      DirrEnergDF.daxpy(dDdF,ene - DenerPlas);
      ipvcur->getRefToDIrreversibleEnergyDNonLocalVariable() = (ene - DenerPlas)*ipvcur->getDDamageDp();
    }
    else{
      STensorOperation::zero(DirrEnergDF);
      ipvcur->getRefToDIrreversibleEnergyDNonLocalVariable() = 0.;
    }

  }

}

// ================================================================================================= END CLASS===============================================================================

mlawLocalDamagePowerYieldHyperWithFailure::mlawLocalDamagePowerYieldHyperWithFailure(const int num,const double E,const double nu, const double rho,
				const double tol, const bool matrixbyPerturbation , const double pert):
				mlawPowerYieldHyperWithFailure(num,E,nu,rho,tol,matrixbyPerturbation,pert){};

void mlawLocalDamagePowerYieldHyperWithFailure::clearAllDamageLaw(){
  for (int i=0; i< damLaw.size(); i++){
    if (damLaw[i]!= NULL) delete damLaw[i];
  }
  damLaw.clear();
};

void mlawLocalDamagePowerYieldHyperWithFailure::setDamageLaw(const DamageLaw &_damLaw){
  damLaw.push_back(_damLaw.clone());
};

mlawLocalDamagePowerYieldHyperWithFailure::mlawLocalDamagePowerYieldHyperWithFailure(const mlawLocalDamagePowerYieldHyperWithFailure &source):
            mlawPowerYieldHyperWithFailure(source){
  damLaw.clear();
  for (int i=0; i< source.damLaw.size(); i++){
    if(source.damLaw[i] != NULL)
    {
      damLaw.push_back(source.damLaw[i]->clone());
    };
  }
}

mlawLocalDamagePowerYieldHyperWithFailure::~mlawLocalDamagePowerYieldHyperWithFailure(){
  for (int i=0; i< damLaw.size(); i++){
    if (damLaw[i]!= NULL) delete damLaw[i];
  }
  damLaw.clear();
};

void mlawLocalDamagePowerYieldHyperWithFailure::createIPState(IPStateBase* &ips, bool hasBodyForce, const bool* state_,const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
  IPVariable* ipvi = new IPHyperViscoElastoPlasticMultipleLocalDamage(_compression,_traction,_kinematic,_N,damLaw);
  IPVariable* ipv1 = new IPHyperViscoElastoPlasticMultipleLocalDamage(_compression,_traction,_kinematic,_N,damLaw);
  IPVariable* ipv2 = new IPHyperViscoElastoPlasticMultipleLocalDamage(_compression,_traction,_kinematic,_N,damLaw);
  if(ips != NULL) delete ips;
  ips = new IP3State(state_,ipvi,ipv1,ipv2);
}
void mlawLocalDamagePowerYieldHyperWithFailure::createIPState(IPHyperViscoElastoPlasticMultipleLocalDamage *ivi, IPHyperViscoElastoPlasticMultipleLocalDamage *iv1, IPHyperViscoElastoPlasticMultipleLocalDamage *iv2) const
{

}
void mlawLocalDamagePowerYieldHyperWithFailure::createIPVariable(IPHyperViscoElastoPlasticMultipleLocalDamage *&ipv,bool hasBodyForce, const MElement *ele,const int nbFF,const IntPt *GP, const int gpt) const
{

}

double mlawLocalDamagePowerYieldHyperWithFailure::soundSpeed() const
{
  return mlawPowerYieldHyperWithFailure::soundSpeed();
}

void mlawLocalDamagePowerYieldHyperWithFailure::constitutive(
                            const STensor3& F0,         // initial deformation gradient (input @ time n)
                            const STensor3& Fn,         // updated deformation gradient (input @ time n+1)
                            STensor3 &P,                // updated 1st Piola-Kirchhoff stress tensor (output)
                            const IPHyperViscoElastoPlasticMultipleLocalDamage *ipvprev,       // array of initial internal variable
                            IPHyperViscoElastoPlasticMultipleLocalDamage *ipvcur,             // updated array of internal variable (in ipvcur on output),
                            STensor43 &Tangent,         // constitutive tangents (output)
                            const bool stiff,            // if true compute the tangents
                            const bool dTangentdeps,
                            STensor63* dCalgdepsconst 
                           ) const
{
  static STensor43 dFedF, dFpdF;
  static STensor3 Peff;
  mlawPowerYieldHyperWithFailure::predictorCorrector(Fn,ipvprev,ipvcur,Peff,stiff,Tangent,dFedF,dFpdF,NULL);

  // get result from effective law
  const STensor3& Fe = ipvcur->_Fe;
  const STensor3& dgammadF = ipvcur->_DgammaDF;
  const STensor3 &Fp  = ipvcur->getConstRefToFp();
  const STensor3& dgFdF = ipvcur->getConstRefToDFailurePlasticityDF();

  double ene = ipvcur->defoEnergy();
  //Msg::Info("enery = %e",ene);

  // saturation damage always develops
  damLaw[0]->computeDamage(ipvcur->getConstRefToEqPlasticStrain(),
                        ipvprev->getConstRefToEqPlasticStrain(),
                        ene, Fe, Fp, Peff, Cel,
                        ipvprev->getConstRefToIPDamage(0),ipvcur->getRefToIPDamage(0));



  damLaw[1]->computeDamage(ipvcur->getFailurePlasticity(),
                        ipvprev->getFailurePlasticity(),
                        ene, Fe, Fp, Peff, Cel,
                        ipvprev->getConstRefToIPDamage(1),ipvcur->getRefToIPDamage(1));


  // computue total softening
  double D1 = ipvcur->getDamage(0);
  double D2 = ipvcur->getDamage(1);
  double D =  1. - (1.- D1)*(1.-D2);
  double Dprev = 1- (1-ipvprev->getDamage(0))*(1-ipvprev->getDamage(1));
  P = Peff;
  P*= (1.- D);

  ipvcur->getRefToElasticEnergy() = (1-D)*ene;
  double DenerPlas = ipvcur->plasticEnergy()-ipvprev->plasticEnergy();
  ipvcur->getRefToPlasticEnergy() = ipvprev->plasticEnergy() + (1-D)*DenerPlas;
  ipvcur->getRefToDamageEnergy() = ipvprev->damageEnergy() + ene*(D-Dprev);

  if (this->getMacroSolver()->getPathFollowingLocalIncrementType() == pathFollowingManager::DEFO_ENERGY){
    ipvcur->getRefToIrreversibleEnergy() = ipvcur->defoEnergy();
  }
  else if (this->getMacroSolver()->getPathFollowingLocalIncrementType() == pathFollowingManager::PLASTIC_ENERGY) {
    ipvcur->getRefToIrreversibleEnergy() = ipvcur->plasticEnergy();
  }
  else if (this->getMacroSolver()->getPathFollowingLocalIncrementType() == pathFollowingManager::DAMAGE_ENERGY){
    ipvcur->getRefToIrreversibleEnergy() = ipvcur->damageEnergy();
  }
  else if (this->getMacroSolver()->getPathFollowingLocalIncrementType() == pathFollowingManager::DISSIPATION_ENERGY){
    ipvcur->getRefToIrreversibleEnergy() = ipvcur->plasticEnergy()+ipvcur->damageEnergy();
  }
  else{
    ipvcur->getRefToIrreversibleEnergy() = 0.;
  }


  if(stiff)
  {

    static STensor3 dDdF;
    for(int i=0;i<3;i++){
      for(int j=0;j<3;j++){
        dDdF(i,j) = (ipvcur->getDDamageDp(0)*dgammadF(i,j)*(1.- D2)+(1.-D1)*ipvcur->getDDamageDp(1)*dgFdF(i,j));
        for(int k=0;k<3;k++){
          for(int l=0;l<3;l++){
            dDdF(i,j) += ((1.-D2)*ipvcur->getConstRefToDDamageDFe(0)(k,l) +(1.-D1)*ipvcur->getConstRefToDDamageDFe(1)(k,l)) *dFedF(k,l,i,j);
          }
        }
      }
    }

    Tangent*=(1.-D);
    for(int i=0;i<3;i++){
      for(int j=0;j<3;j++){
        for(int k=0;k<3;k++){
          for(int l=0;l<3;l++){
            Tangent(i,j,k,l)-=Peff(i,j)*dDdF(k,l);

          }
        }
      }
    }

    STensor3& DirrEnergDF = ipvcur->getRefToDIrreversibleEnergyDF();
    if (this->getMacroSolver()->getPathFollowingLocalIncrementType() == pathFollowingManager::DEFO_ENERGY){
      DirrEnergDF*= (1-D);
      DirrEnergDF.daxpy(dDdF,-ene);
    }
    else if (this->getMacroSolver()->getPathFollowingLocalIncrementType() == pathFollowingManager::PLASTIC_ENERGY) {
      DirrEnergDF*= (1-D);
      DirrEnergDF.daxpy(dDdF,-DenerPlas);
    }
    else if (this->getMacroSolver()->getPathFollowingLocalIncrementType() == pathFollowingManager::DAMAGE_ENERGY){
      for(int i=0;i<3;i++){
        for(int j=0;j<3;j++){
          DirrEnergDF(i,j) = 0.;
          for(int k=0;k<3;k++){
            for(int l=0;l<3;l++){
              for(int m=0; m<3; m++){
                DirrEnergDF(i,j) += (D - Dprev)*Peff(k,m)*Fp(l,m)*dFedF(k,l,i,j);
              };
            }
          }
        }
      }
      DirrEnergDF.daxpy(dDdF,ene);
    }
    else if (this->getMacroSolver()->getPathFollowingLocalIncrementType() == pathFollowingManager::DISSIPATION_ENERGY){
      DirrEnergDF*= (1-D);
      for(int i=0;i<3;i++){
        for(int j=0;j<3;j++){
          for(int k=0;k<3;k++){
            for(int l=0;l<3;l++){
              for(int m=0; m<3; m++){
                DirrEnergDF(i,j) += (D - Dprev)*Peff(k,m)*Fp(l,m)*dFedF(k,l,i,j);
              };
            }
          }
        }
      }
      DirrEnergDF.daxpy(dDdF,ene - DenerPlas);
    }
    else{
      STensorOperation::zero(DirrEnergDF);
    }

  }
}



// ================================================================================================= END CLASS===============================================================================

mlawNonLocalDamagePowerYieldHyperWithFailure::mlawNonLocalDamagePowerYieldHyperWithFailure(const int num,const double E,const double nu, const double rho,
				const double tol, const bool matrixbyPerturbation , const double pert):
				mlawPowerYieldHyperWithFailure(num,E,nu,rho,tol,matrixbyPerturbation,pert),_criticalDamage(0.999),_blockHardeningAfterFailure(false),
        _blockDamageAfterFailure(false){};

void mlawNonLocalDamagePowerYieldHyperWithFailure::clearAllCLengthLaw(){
  for (int i=0; i< cLLaw.size(); i++){
    if (cLLaw[i]!= NULL) delete cLLaw[i];
  }
  cLLaw.clear();
};
void mlawNonLocalDamagePowerYieldHyperWithFailure::clearAllDamageLaw(){
  for (int i=0; i< damLaw.size(); i++){
    if (damLaw[i]!= NULL) delete damLaw[i];
  }
  damLaw.clear();
};

void mlawNonLocalDamagePowerYieldHyperWithFailure::setCLengthLaw(const CLengthLaw &_cLLaw){
  cLLaw.push_back(_cLLaw.clone());
};
void mlawNonLocalDamagePowerYieldHyperWithFailure::setDamageLaw(const DamageLaw &_damLaw){
  damLaw.push_back(_damLaw.clone());
};

void mlawNonLocalDamagePowerYieldHyperWithFailure::setCritialDamage(const double cr) {
  _criticalDamage = cr;
};

mlawNonLocalDamagePowerYieldHyperWithFailure::mlawNonLocalDamagePowerYieldHyperWithFailure(const mlawNonLocalDamagePowerYieldHyperWithFailure &source):
            mlawPowerYieldHyperWithFailure(source), _criticalDamage(source._criticalDamage), _blockHardeningAfterFailure(source._blockHardeningAfterFailure),
            _blockDamageAfterFailure(source._blockDamageAfterFailure){
  cLLaw.clear();
  damLaw.clear();

  for (int i=0; i< source.cLLaw.size(); i++){
    if(source.cLLaw[i] != NULL)
    {
      cLLaw.push_back(source.cLLaw[i]->clone());
    }
    if(source.damLaw[i] != NULL)
    {
      damLaw.push_back(source.damLaw[i]->clone());
    };
  }
}

mlawNonLocalDamagePowerYieldHyperWithFailure::~mlawNonLocalDamagePowerYieldHyperWithFailure(){
  for (int i=0; i< cLLaw.size(); i++){
    if (cLLaw[i]!= NULL) delete cLLaw[i];
    if (damLaw[i]!= NULL) delete damLaw[i];
  }
  cLLaw.clear();
  damLaw.clear();
};

void mlawNonLocalDamagePowerYieldHyperWithFailure::createIPState(IPStateBase* &ips, bool hasBodyForce, const bool* state_,const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
  IPVariable* ipvi = new IPHyperViscoElastoPlasticMultipleNonLocalDamage(_compression,_traction,_kinematic,_N,cLLaw,damLaw);
  IPVariable* ipv1 = new IPHyperViscoElastoPlasticMultipleNonLocalDamage(_compression,_traction,_kinematic,_N,cLLaw,damLaw);
  IPVariable* ipv2 = new IPHyperViscoElastoPlasticMultipleNonLocalDamage(_compression,_traction,_kinematic,_N,cLLaw,damLaw);
  if(ips != NULL) delete ips;
  ips = new IP3State(state_,ipvi,ipv1,ipv2);
}
void mlawNonLocalDamagePowerYieldHyperWithFailure::createIPState(IPHyperViscoElastoPlasticMultipleNonLocalDamage *ivi, IPHyperViscoElastoPlasticMultipleNonLocalDamage *iv1, IPHyperViscoElastoPlasticMultipleNonLocalDamage *iv2) const
{

}
void mlawNonLocalDamagePowerYieldHyperWithFailure::createIPVariable(IPHyperViscoElastoPlasticMultipleNonLocalDamage *&ipv,bool hasBodyForce, const MElement *ele,const int nbFF,const IntPt *GP, const int gpt) const
{

}

double mlawNonLocalDamagePowerYieldHyperWithFailure::soundSpeed() const
{
  return mlawPowerYieldHyperWithFailure::soundSpeed();
}

void mlawNonLocalDamagePowerYieldHyperWithFailure::constitutive(const STensor3& F0,const STensor3& Fn,STensor3 &P,const IPHyperViscoElastoPlasticMultipleNonLocalDamage *q0,
	IPHyperViscoElastoPlasticMultipleNonLocalDamage *q1,STensor43 &Tangent, const bool stiff,const bool dTangentdeps, STensor63* dCalgdeps) const
{
  mlawPowerYieldHyperWithFailure::constitutive(F0,Fn,P,q0,q1,Tangent,stiff);
}

void mlawNonLocalDamagePowerYieldHyperWithFailure::checkInternalState(IPVariable* q1, const IPVariable* q0) const{
  mlawPowerYieldHyperWithFailure::checkInternalState(q1,q0);

  IPHyperViscoElastoPlasticMultipleNonLocalDamage* ipv = dynamic_cast<IPHyperViscoElastoPlasticMultipleNonLocalDamage*>(q1);
  const IPHyperViscoElastoPlasticMultipleNonLocalDamage* ipvprev = dynamic_cast<const IPHyperViscoElastoPlasticMultipleNonLocalDamage*>(q0);

  if (ipv != NULL){
    // computue total softening
    double D0 = ipv->getDamage(0);
    double D1 = ipv->getDamage(1);
    double D =  1. - (1.- D0)*(1.-D1);

    if (_blockHardeningAfterFailure){
      // saturation of the hardening after damage reach critical value
      if (!ipvprev->isSaturated()){
        // if damage reaches its critical values
        if (D1 > _criticalDamage or D > _criticalDamage){
          ipv->saturate(true);
        }
        else{
          // other wise, normal state is used
          ipv->saturate(false);
        }
      }
      else{
        ipv->saturate(false);
      }
    }

    // check block damage
    if (_blockDamageAfterFailure){
      if (!ipvprev->dissipationIsBlocked()){
        // if damage reaches its critical values
        if (D1 > _criticalDamage or D > _criticalDamage){
          ipv->blockDissipation(true);
        }
        else{
          // other wise, normal state is used
          ipv->blockDissipation(false);
        }
      }
      else{
        ipv->blockDissipation(true);
      }
    }

  }
};

void mlawNonLocalDamagePowerYieldHyperWithFailure::predictorCorrector(
                            const STensor3& F0,         // initial deformation gradient (input @ time n)
                            const STensor3& Fn,         // updated deformation gradient (input @ time n+1)
                            STensor3 &P,                // updated 1st Piola-Kirchhoff stress tensor (output)
                            const IPHyperViscoElastoPlasticMultipleNonLocalDamage *ipvprev,       // array of initial internal variable
                            IPHyperViscoElastoPlasticMultipleNonLocalDamage *ipvcur,             // updated array of internal variable (in ipvcur on output),
                            STensor43 &Tangent,         // constitutive tangents (output)
                            std::vector<STensor3>  &dLocalVariableDStrain,
                            std::vector<STensor3>  &dStressDNonLocalVariable,
                            fullMatrix<double>   &dLocalVariableDNonLocalVariable,
                            const bool stiff,            // if true compute the tangents
                            STensor43* elasticTangent) const
{
  // compute non-local length scales for first and second damage variable
  //double p0 = ipvprev->getCurrentPlasticStrain();
  //cLLaw[0]->computeCL(p0, ipvcur->getRefToIPCLength(0));

  double D0prev = ipvprev->getDamage(0);
  cLLaw[0]->computeCL(D0prev, ipvcur->getRefToIPCLength(0));

  //double p1 = ipvprev->getFailurePlasticity();
  //cLLaw[1]->computeCL(p1, ipvcur->getRefToIPCLength(1));
  double D1prev = ipvprev->getDamage(1);
  cLLaw[1]->computeCL(D1prev, ipvcur->getRefToIPCLength(1));


  static STensor43 dFedF, dFpdF;
  static STensor3 Peff;
  mlawPowerYieldHyperWithFailure::predictorCorrector(Fn,ipvprev,ipvcur,Peff,stiff,Tangent,dFedF,dFpdF, elasticTangent);

  // get result from effective law
  const STensor3& Fe = ipvcur->_Fe;
  const STensor3& dgammadF = ipvcur->_DgammaDF;
  const STensor3 &Fp  = ipvcur->getConstRefToFp();
  const STensor3& dgFdF = ipvcur->getConstRefToDFailurePlasticityDF();

  double ene = ipvcur->defoEnergy();
  //Msg::Info("enery = %e",ene);

  if (ipvcur->dissipationIsBlocked()){
    // damage stop increasing
    IPDamage& curDama0 = ipvcur->getRefToIPDamage(0);
    const IPDamage& prevDama0 = ipvprev->getConstRefToIPDamage(0);

    curDama0.getRefToDamage() = prevDama0.getDamage();
    curDama0.getRefToDDamageDp() = 0.;
    STensorOperation::zero(curDama0.getRefToDDamageDFe());
    curDama0.getRefToDeltaDamage() = 0;
    curDama0.getRefToMaximalP() = prevDama0.getMaximalP();

    IPDamage& curDama1 = ipvcur->getRefToIPDamage(1);
    const IPDamage& prevDama1 = ipvprev->getConstRefToIPDamage(1);

    curDama1.getRefToDamage() = prevDama1.getDamage();
    curDama1.getRefToDDamageDp() = 0.;
    STensorOperation::zero(curDama1.getRefToDDamageDFe());
    curDama1.getRefToDeltaDamage() = 0;
    curDama1.getRefToMaximalP() = prevDama1.getMaximalP();
  }
  else{
    // saturation damage always develops
    damLaw[0]->computeDamage(ipvcur->getEffectivePlasticStrain(),
                          ipvprev->getEffectivePlasticStrain(),
                          ene, Fe, Fp, Peff, Cel,
                          ipvprev->getConstRefToIPDamage(0),ipvcur->getRefToIPDamage(0));


    damLaw[1]->computeDamage(ipvcur->getNonLocalFailurePlasticity(),
                          ipvprev->getNonLocalFailurePlasticity(),
                          ene, Fe, Fp, Peff, Cel,
                          ipvprev->getConstRefToIPDamage(1),ipvcur->getRefToIPDamage(1));
  };
  // computue total softening
  double D0 = ipvcur->getDamage(0);
  double D1 = ipvcur->getDamage(1);
  double D =  1. - (1.- D0)*(1.-D1);

  double DeltaD = (D0-D0prev)*(1.-D1)+ (1.-D0)*(D1-D1prev);

  P = Peff;
  P*= (1.- D);

  ipvcur->getRefToElasticEnergy() = (1-D)*ene;
  double plasticDisStep = ipvcur->plasticEnergy()-ipvprev->plasticEnergy(); // plastic step cumputed from nondamage law
  ipvcur->getRefToPlasticEnergy() = ipvprev->plasticEnergy() + (1.-D)*plasticDisStep;
  ipvcur->getRefToDamageEnergy() = ipvprev->damageEnergy() + ene*DeltaD;

  if (getMacroSolver()->getPathFollowingLocalIncrementType() == pathFollowingManager::DEFO_ENERGY){
    ipvcur->getRefToIrreversibleEnergy() = ipvcur->defoEnergy();
  }
  else if (getMacroSolver()->getPathFollowingLocalIncrementType() == pathFollowingManager::PLASTIC_ENERGY){
    ipvcur->getRefToIrreversibleEnergy() = ipvcur->plasticEnergy();
  }
  else if (getMacroSolver()->getPathFollowingLocalIncrementType() == pathFollowingManager::DAMAGE_ENERGY){
    ipvcur->getRefToIrreversibleEnergy() = ipvcur->damageEnergy();
  }
  else if (getMacroSolver()->getPathFollowingLocalIncrementType() == pathFollowingManager::DISSIPATION_ENERGY){
    ipvcur->getRefToIrreversibleEnergy() = ipvcur->plasticEnergy()+ipvcur->damageEnergy();
  }
  else{
    ipvcur->getRefToIrreversibleEnergy() = 0.;
  }


  if(stiff)
  {
    // we need to correct partial P/partial F: (1-D) partial P/partial F - Peff otimes partial D partial F
    Tangent*=(1.-D);

    static STensor3 dD0dF, dD1dF;
    STensorOperation::multSTensor3STensor43(ipvcur->getConstRefToDDamageDFe(0),dFedF, dD0dF);
    STensorOperation::multSTensor3STensor43(ipvcur->getConstRefToDDamageDFe(1),dFedF, dD1dF);
    static STensor3 dDdF;
    dDdF = dD0dF;
    dDdF *= (1-D1);
    dDdF.daxpy(dD1dF,1.-D0);

    for(int i=0;i<3;i++){
      for(int j=0;j<3;j++){
        for(int k=0;k<3;k++){
          for(int l=0;l<3;l++){
            Tangent(i,j,k,l)-=Peff(i,j)*dDdF(k,l);
          }
        }
      }
    }

    if (elasticTangent != NULL){
    // update with damage
      (*elasticTangent) *= (1.-D);
     }
    dLocalVariableDStrain[0] = dgammadF;
    dLocalVariableDStrain[1] = dgFdF;

    // -hat{P} partial D/partial tilde p
    for (int i=0; i<3; i++){
      for (int j=0; j<3; j++){
        // partial p/partial F
        dStressDNonLocalVariable[0](i,j) = -1.*Peff(i,j)*ipvcur->getDDamageDp(0)*(1.- D1);
        dStressDNonLocalVariable[1](i,j) = -1.*Peff(i,j)*(1.-D0)*ipvcur->getDDamageDp(1);
      }
    }

    // partial p partial tilde p (0 if no MFH)
    dLocalVariableDNonLocalVariable.setAll(0.);


    // for irreversible energy
     STensor3& DirrEnergDF = ipvcur->getRefToDIrreversibleEnergyDF();
    if (getMacroSolver()->getPathFollowingLocalIncrementType() == pathFollowingManager::DEFO_ENERGY){
      DirrEnergDF*= (1-D);
      DirrEnergDF.daxpy(dDdF,-ene);

      ipvcur->getRefToDIrreversibleEnergyDNonLocalVariable(0) = (-ene)*(ipvcur->getDDamageDp(0)*(1.- D1));
      ipvcur->getRefToDIrreversibleEnergyDNonLocalVariable(1) = (-ene)*(ipvcur->getDDamageDp(1)*(1.- D0));
    }
    else if (getMacroSolver()->getPathFollowingLocalIncrementType() == pathFollowingManager::PLASTIC_ENERGY){
      DirrEnergDF *= (1-D);
      DirrEnergDF.daxpy(dDdF,-plasticDisStep);

      ipvcur->getRefToDIrreversibleEnergyDNonLocalVariable(0) = (-plasticDisStep)*(ipvcur->getDDamageDp(0)*(1.- D1));
      ipvcur->getRefToDIrreversibleEnergyDNonLocalVariable(1) = (-plasticDisStep)*(ipvcur->getDDamageDp(1)*(1.- D0));
    }
    else if (getMacroSolver()->getPathFollowingLocalIncrementType() == pathFollowingManager::DAMAGE_ENERGY){
      for(int i=0;i<3;i++){
        for(int j=0;j<3;j++){
          DirrEnergDF(i,j) = 0.;
          for(int k=0;k<3;k++){
            for(int l=0;l<3;l++){
              for(int m=0; m<3; m++){
                DirrEnergDF(i,j) += DeltaD*Peff(k,m)*Fp(l,m)*dFedF(k,l,i,j);
              };
            }
          }
        }
      }
      static STensor3 DDeltaDDF;
      DDeltaDDF = dD0dF;
      DDeltaDDF *= (1.-D1-(D1-D1prev));
      DDeltaDDF.daxpy(dD1dF,1.-D0-(D0-D0prev));

      DirrEnergDF.daxpy(DDeltaDDF,ene);

      ipvcur->getRefToDIrreversibleEnergyDNonLocalVariable(0) = (ene)*(ipvcur->getDDamageDp(0)*(1.- D1- (D1-D1prev)));
      ipvcur->getRefToDIrreversibleEnergyDNonLocalVariable(1) = (ene)*(ipvcur->getDDamageDp(1)*(1.- D0- (D0-D0prev)));

    }
    else if (getMacroSolver()->getPathFollowingLocalIncrementType() == pathFollowingManager::DISSIPATION_ENERGY){
      // plastic part
      DirrEnergDF *= (1-D);
      DirrEnergDF.daxpy(dDdF,-plasticDisStep);

      ipvcur->getRefToDIrreversibleEnergyDNonLocalVariable(0) = (-plasticDisStep)*(ipvcur->getDDamageDp(0)*(1.- D1));
      ipvcur->getRefToDIrreversibleEnergyDNonLocalVariable(1) = (-plasticDisStep)*(ipvcur->getDDamageDp(1)*(1.- D0));

      for(int i=0;i<3;i++){
        for(int j=0;j<3;j++){
          for(int k=0;k<3;k++){
            for(int l=0;l<3;l++){
              for(int m=0; m<3; m++){
                DirrEnergDF(i,j) += DeltaD*Peff(k,m)*Fp(l,m)*dFedF(k,l,i,j);
              };
            }
          }
        }
      }
      // damage part
      static STensor3 DDeltaDDF;
      DDeltaDDF = dD0dF;
      DDeltaDDF *= (1.-D1-(D1-D1prev));
      DDeltaDDF.daxpy(dD1dF,1.-D0-(D0-D0prev));

      DirrEnergDF.daxpy(DDeltaDDF,ene);

      ipvcur->getRefToDIrreversibleEnergyDNonLocalVariable(0) += (ene)*(ipvcur->getDDamageDp(0)*(1.- D1- (D1-D1prev)));
      ipvcur->getRefToDIrreversibleEnergyDNonLocalVariable(1) += (ene)*(ipvcur->getDDamageDp(1)*(1.- D0- (D0-D0prev)));
    }
    else{
      STensorOperation::zero(DirrEnergDF);
    }

  }
};


void mlawNonLocalDamagePowerYieldHyperWithFailure::constitutive(
                            const STensor3& F0,         // initial deformation gradient (input @ time n)
                            const STensor3& Fn,         // updated deformation gradient (input @ time n+1)
                            STensor3 &P,                // updated 1st Piola-Kirchhoff stress tensor (output)
                            const IPHyperViscoElastoPlasticMultipleNonLocalDamage *ipvprev,       // array of initial internal variable
                            IPHyperViscoElastoPlasticMultipleNonLocalDamage *ipvcur,             // updated array of internal variable (in ipvcur on output),
                            STensor43 &Tangent,         // constitutive tangents (output)
                            std::vector<STensor3>  &dLocalVariableDStrain,
                            std::vector<STensor3>  &dStressDNonLocalVariable,
                            fullMatrix<double>   &dLocalVariableDNonLocalVariable,
                            const bool stiff,            // if true compute the tangents
                            STensor43* elasticTangent,
                            const bool dTangentdeps,
                            STensor63* dCalgdeps) const
{
  if (_tangentByPerturbation){
    predictorCorrector(F0,Fn,P,ipvprev,ipvcur,Tangent,dLocalVariableDStrain,dStressDNonLocalVariable,dLocalVariableDNonLocalVariable,false,NULL);

    if (stiff){
      static STensor3 Fplus, Pplus;
      static IPHyperViscoElastoPlasticMultipleNonLocalDamage q11(*ipvprev);

      // some internal variable should be initialized (nonlocal var)
      q11.operator =(*dynamic_cast<const IPVariable*>(ipvcur));

      for (int i=0; i<3; i++){
        for (int j=0; j<3; j++){
          Fplus = Fn;
          Fplus(i,j)+=_perturbationfactor;
          this->predictorCorrector(F0,Fplus,Pplus,ipvprev,&q11,Tangent,dLocalVariableDStrain,dStressDNonLocalVariable,dLocalVariableDNonLocalVariable,false,NULL);

          // local var
          ipvcur->_DgammaDF(i,j) = (q11._epspbarre - ipvcur->_epspbarre)/_perturbationfactor;
          ipvcur->_dgFdF(i,j) = (q11._gF - ipvcur->_gF)/_perturbationfactor;

          dLocalVariableDStrain[0](i,j) = ipvcur->_DgammaDF(i,j);
          dLocalVariableDStrain[1](i,j) = ipvcur->_dgFdF(i,j);

          // irreversible energy
          ipvcur->_DirreversibleEnergyDF(i,j) = (q11._irreversibleEnergy - ipvcur->_irreversibleEnergy)/_perturbationfactor;
          // tangent
          for (int k=0; k<3; k++){
            for (int l=0; l<3; l++){
              Tangent(k,l,i,j) = (Pplus(k,l)-P(k,l))/_perturbationfactor;
            }
          }
        }
      }

      q11.operator =(*dynamic_cast<const IPVariable*>(ipvcur));
      q11.getRefToEffectivePlasticStrain() = ipvcur->getEffectivePlasticStrain() + _perturbationfactor;
      predictorCorrector(F0,Fn,Pplus,ipvprev,&q11,Tangent,dLocalVariableDStrain,dStressDNonLocalVariable,dLocalVariableDNonLocalVariable,false,NULL);
      for (int k=0; k<3; k++){
        for (int l=0; l<3; l++){
          dStressDNonLocalVariable[0](k,l) = (Pplus(k,l)-P(k,l))/_perturbationfactor;
        }
      }
      dLocalVariableDNonLocalVariable(0,0) =   (q11._epspbarre - ipvcur->_epspbarre)/_perturbationfactor;
      dLocalVariableDNonLocalVariable(1,0) =   (q11._gF - ipvcur->_gF)/_perturbationfactor;
      ipvcur->getRefToDIrreversibleEnergyDNonLocalVariable(0) = (q11._irreversibleEnergy - ipvcur->_irreversibleEnergy)/_perturbationfactor;



      q11.operator =(*dynamic_cast<const IPVariable*>(ipvcur));
      q11.getRefToNonLocalFailurePlasticity() = ipvcur->getNonLocalFailurePlasticity() + _perturbationfactor;
      predictorCorrector(F0,Fn,Pplus,ipvprev,&q11,Tangent,dLocalVariableDStrain,dStressDNonLocalVariable,dLocalVariableDNonLocalVariable,false,NULL);
      for (int k=0; k<3; k++){
        for (int l=0; l<3; l++){
          dStressDNonLocalVariable[1](k,l) = (Pplus(k,l)-P(k,l))/_perturbationfactor;
        }
      }
      dLocalVariableDNonLocalVariable(0,1) =   (q11._epspbarre - ipvcur->_epspbarre)/_perturbationfactor;
      dLocalVariableDNonLocalVariable(1,1) =   (q11._gF - ipvcur->_gF)/_perturbationfactor;
      ipvcur->getRefToDIrreversibleEnergyDNonLocalVariable(1) = (q11._irreversibleEnergy - ipvcur->_irreversibleEnergy)/_perturbationfactor;
    }

  }
  else{
    predictorCorrector(F0,Fn,P,ipvprev,ipvcur,Tangent,dLocalVariableDStrain,dStressDNonLocalVariable,dLocalVariableDNonLocalVariable,stiff, elasticTangent);
  }

};
