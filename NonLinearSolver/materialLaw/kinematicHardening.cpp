//
// Description: Define kinematic hardening laws for  plasticity
//
//
// Author:  <V.D. Nguyen>, (C) 2014
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "kinematicHardening.h"

kinematicHardening::kinematicHardening(const int num,const bool init): _num(num), _initialized(init){}

kinematicHardening::kinematicHardening(const kinematicHardening &source)
{
  _num = source._num;
  _initialized = source._initialized;
}

kinematicHardening& kinematicHardening::operator=(const kinematicHardening &source)
{
  _num = source._num;
  _initialized = source._initialized;
  return *this;
}


PolynomialKinematicHardening::PolynomialKinematicHardening(const int num, int order,  bool init):
  kinematicHardening(num,init),_order(order){
      
    _coefficients.resize(_order+1);
    _coefficients.setAll(0.);
    _coefficients(0) = 0.;
    
    _temFunc_K= new constantScalarFunction(1.);
};

PolynomialKinematicHardening::PolynomialKinematicHardening(const PolynomialKinematicHardening& src):
  kinematicHardening(src), _order(src._order),_coefficients(src._coefficients){
      
    _temFunc_K = NULL;
    if (src._temFunc_K != NULL){
        _temFunc_K = src._temFunc_K->clone();
    }
};

PolynomialKinematicHardening& PolynomialKinematicHardening::operator =(const kinematicHardening& src){
  kinematicHardening::operator=(src);
  const PolynomialKinematicHardening* psrc = dynamic_cast<const PolynomialKinematicHardening*>(&src);
  if(psrc != NULL)
  {
    _order = psrc->_order;
    _coefficients.resize(psrc->_coefficients.size());
    _coefficients.setAll(psrc->_coefficients);
    
    if (psrc->_temFunc_K != NULL){
      if (_temFunc_K != NULL){
        _temFunc_K->operator=(*psrc->_temFunc_K);
      }
      else{
        _temFunc_K = psrc->_temFunc_K->clone();
      }
    }
  }
  return *this;
};

PolynomialKinematicHardening::~PolynomialKinematicHardening(){
  if (_temFunc_K != NULL) {delete _temFunc_K; _temFunc_K = NULL;};
};

void PolynomialKinematicHardening::setCoefficients(const int i, const double val){
  _coefficients(i) = val;
};

void PolynomialKinematicHardening::setTemperatureFunction_K(const scalarFunction& Tfunc){
  if (_temFunc_K != NULL) delete _temFunc_K;
  _temFunc_K = Tfunc.clone();
}

void PolynomialKinematicHardening::createIPVariable(IPKinematicHardening* &ipv) const
{
  if(ipv != NULL) delete ipv;
  ipv = new IPKinematicHardening();
}

void PolynomialKinematicHardening::hardening(double p0, const IPKinematicHardening &ipvprev, double _p, IPKinematicHardening &ipv) const
{
  double tol = 1.e-20;
  double p = _p;
  if (_p<tol){
    p = tol; 
  }  
  //_coefficients.print("_coefficients");
  double R=0., dR=0., ddR=0., dRdT = 0., ddRdTT = 0., ddRdT = 0.;
  if (ipvprev.isSaturated()){
    R = ipvprev.getR();
    dR = 0.;
    ddR = 0.;
  }
  else{
    if (_order == 1){
      R = _coefficients(0)+ _coefficients(1)*p;
      dR = _coefficients(1);
      ddR = 0.;
    }
    else if (_order == 2){
      R = _coefficients(0)+ _coefficients(1)*p + _coefficients(2)*p*p;
      dR = _coefficients(1)+2.*_coefficients(2)*p;
      ddR = 2.*_coefficients(2);
    }
    else if (_order == 3){
      R = _coefficients(0)+ _coefficients(1)*p + _coefficients(2)*p*p+_coefficients(3)*p*p*p;
      dR = _coefficients(1)+2.*_coefficients(2)*p+ 3*_coefficients(3)*p*p;
      ddR = 2.*_coefficients(2)+6.*_coefficients(3)*p;
    }
    else if (_order == 4){
      R = _coefficients(0)+ _coefficients(1)*p + _coefficients(2)*p*p+_coefficients(3)*p*p*p+_coefficients(4)*p*p*p*p;
      dR = _coefficients(1)+2.*_coefficients(2)*p+ 3*_coefficients(3)*p*p+4*_coefficients(4)*p*p*p;
      ddR = 2.*_coefficients(2)+6.*_coefficients(3)*p+12.*_coefficients(4)*p*p;
    }
    else if (_order == 5){
      double p2 = p*p;
      double p3 = p2*p;
      double p4 = p3*p;
      double p5 = p4*p;
      
      R = _coefficients(0)+ _coefficients(1)*p + _coefficients(2)*p2+_coefficients(3)*p2+_coefficients(4)*p4+_coefficients(5)*p5;
      dR = _coefficients(1)+2.*_coefficients(2)*p+ 3*_coefficients(3)*p2+4*_coefficients(4)*p3+5*_coefficients(5)*p4;
      ddR = 2.*_coefficients(2)+6.*_coefficients(3)*p+12.*_coefficients(4)*p2+20*_coefficients(5)*p3;
    }
    else
      Msg::Error("order %d is not implemented PolynomialJ2IsotropicHaderning::hardening",_order);
  }
  ipv.set(R,dR,ddR,dRdT,ddRdTT,ddRdT);
}

void PolynomialKinematicHardening::hardening(double p0, const IPKinematicHardening &ipvprev, double _p, double T, IPKinematicHardening &ipv) const
{
  double tol = 1.e-20;
  double p = _p;
  if (_p<tol){
    p = tol; 
  }
  
  //_coefficients.print("_coefficients");
  double R=0., dR=0., ddR=0., dRdT=0., ddRdTT=0., ddRdT = 0.;
  
  if (ipvprev.isSaturated()){
    R = ipvprev.getR();
    dR = 0.;
    ddR = 0.;
    dRdT = 0.;
    ddRdTT = 0.;
    ddRdT = 0.;
  }
  else{
    if (_order == 1){
      R = _coefficients(0)+ _coefficients(1)*p;
      dR = _coefficients(1);
      ddR = 0.;
    }
    else if (_order == 2){
      R = _coefficients(0)+ _coefficients(1)*p + _coefficients(2)*p*p;
      dR = _coefficients(1)+2.*_coefficients(2)*p;
      ddR = 2.*_coefficients(2);
    }
    else if (_order == 3){
      R = _coefficients(0)+ _coefficients(1)*p + _coefficients(2)*p*p+_coefficients(3)*p*p*p;
      dR = _coefficients(1)+2.*_coefficients(2)*p+ 3*_coefficients(3)*p*p;
      ddR = 2.*_coefficients(2)+6.*_coefficients(3)*p;
    }
    else if (_order == 4){
      R = _coefficients(0)+ _coefficients(1)*p + _coefficients(2)*p*p+_coefficients(3)*p*p*p+_coefficients(4)*p*p*p*p;
      dR = _coefficients(1)+2.*_coefficients(2)*p+ 3*_coefficients(3)*p*p+4*_coefficients(4)*p*p*p;
      ddR = 2.*_coefficients(2)+6.*_coefficients(3)*p+12.*_coefficients(4)*p*p;
    }
    else if (_order == 5){
      double p2 = p*p;
      double p3 = p2*p;
      double p4 = p3*p;
      double p5 = p4*p;
      
      R = _coefficients(0)+ _coefficients(1)*p + _coefficients(2)*p2+_coefficients(3)*p2+_coefficients(4)*p4+_coefficients(5)*p5;
      dR = _coefficients(1)+2.*_coefficients(2)*p+ 3*_coefficients(3)*p2+4*_coefficients(4)*p3+5*_coefficients(5)*p4;
      ddR = 2.*_coefficients(2)+6.*_coefficients(3)*p+12.*_coefficients(4)*p2+20*_coefficients(5)*p3;
    }
    else
      Msg::Error("order %d is not implemented PolynomialJ2IsotropicHaderning::hardening",_order);
  }
  
  R *= _temFunc_K->getVal(T); // Hb
  dR *= _temFunc_K->getVal(T); // dHb
  ddR *= _temFunc_K->getVal(T); // ddHb
  dRdT = R*_temFunc_K->getDiff(T)/_temFunc_K->getVal(T); // dHbdT
  ddRdTT = R*_temFunc_K->getDoubleDiff(T)/_temFunc_K->getVal(T); // ddHbddT
  ddRdT = dR*_temFunc_K->getDiff(T)/_temFunc_K->getVal(T); // ddHbdT = ddHbdTdgamma

  ipv.set(R,dR,ddR,dRdT,ddRdTT,ddRdT);
}

kinematicHardening * PolynomialKinematicHardening::clone() const
{
  return new PolynomialKinematicHardening(*this);
}



PowerKinematicHardening::PowerKinematicHardening(const int num, const double h, const double exp,  bool init):
kinematicHardening(num,init),_h(h),_exp(exp){};

PowerKinematicHardening::PowerKinematicHardening(const PowerKinematicHardening& src):
  kinematicHardening(src), _h(src._h),_exp(src._exp){};

PowerKinematicHardening& PowerKinematicHardening::operator =(const kinematicHardening& src){
  kinematicHardening::operator=(src);
  const PowerKinematicHardening* psrc = dynamic_cast<const PowerKinematicHardening*>(&src);
  if(psrc != NULL)
  {
    _h = psrc->_h;
    _exp = psrc->_exp;
  }
  return *this;
};


void PowerKinematicHardening::createIPVariable(IPKinematicHardening* &ipv) const
{
  if(ipv != NULL) delete ipv;
  ipv = new IPKinematicHardening();
}

void PowerKinematicHardening::hardening(double p0, const IPKinematicHardening &ipvprev, double p, IPKinematicHardening &ipv) const
{
  
  double dR=0., ddR=0., dRdT=0., ddRdTT=0., ddRdT = 0.;
  double R = 0.;
  if (ipvprev.isSaturated()){
    R = ipvprev.getR();
    dR = 0.;
    ddR = 0.;
  }
  else{
    double tol=1.e-16;
    if(p< tol)
    {
      R=0.;
      if(_h<1.)
      {
        dR = 1e20;
        ddR = -1e20;
      }
      else
      {
        dR = _h;
        ddR = 0.;
      }
    }
    else
    {
      R=_h*pow(p,_exp);
      dR = _exp*R/p;
      ddR = (_exp-1.)*dR/p;
    }
  }
  ipv.set(R,dR,ddR,dRdT,ddRdTT,ddRdT);
}

void PowerKinematicHardening::hardening(double p0, const IPKinematicHardening &ipvprev, double p, double T, IPKinematicHardening &ipv) const
{
  //_coefficients.print("_coefficients");
  double R =0., dR=0., ddR=0., dRdT=0., ddRdTT=0., ddRdT = 0.;
  
  ipv.set(R,dR,ddR,dRdT,ddRdTT,ddRdT);
}

kinematicHardening * PowerKinematicHardening::clone() const
{
  return new PowerKinematicHardening(*this);
}


CoshKinematicHardening::CoshKinematicHardening(const int num, const double K, const double p0,  bool init):
kinematicHardening(num,init),_K(K),_p0(p0){};

CoshKinematicHardening::CoshKinematicHardening(const CoshKinematicHardening& src):
  kinematicHardening(src), _K(src._K),_p0(src._p0){};

CoshKinematicHardening& CoshKinematicHardening::operator =(const kinematicHardening& src){
  kinematicHardening::operator=(src);
  const CoshKinematicHardening* psrc = dynamic_cast<const CoshKinematicHardening*>(&src);
  if(psrc != NULL)
  {
    _K = psrc->_K;
    _p0 = psrc->_p0;
  }
  return *this;
};


void CoshKinematicHardening::createIPVariable(IPKinematicHardening* &ipv) const
{
  if(ipv != NULL) delete ipv;
  ipv = new IPKinematicHardening();
}

void CoshKinematicHardening::hardening(double p0, const IPKinematicHardening &ipvprev, double p, IPKinematicHardening &ipv) const
{
  double R(0.), dR(0.), ddR(0.), dRdT(0.), ddRdTT(0.), ddRdT(0.);
  if (ipvprev.isSaturated()){
    R = ipvprev.getR();
    dR = 0.;
    ddR = 0.;
  }
  else{  
    R = _K*(cosh(p/_p0)-1.);
    dR= _K*sinh(p/_p0)/_p0;
    ddR = _K*cosh(p/_p0)/(_p0*_p0);
  }

  ipv.set(R,dR,ddR,dRdT,ddRdTT,ddRdT);
}

void CoshKinematicHardening::hardening(double p0, const IPKinematicHardening &ipvprev, double p, double T, IPKinematicHardening &ipv) const
{
  //_coefficients.print("_coefficients");
  double R =0., dR=0., ddR=0., dRdT=0., ddRdTT=0., ddRdT = 0.;
  
  ipv.set(R,dR,ddR,dRdT,ddRdTT,ddRdT);
}

kinematicHardening * CoshKinematicHardening::clone() const
{
  return new CoshKinematicHardening(*this);
}


exponentialKinematicHardeningLaw::exponentialKinematicHardeningLaw(const int num, const double KK, const double h, const double alp): kinematicHardening(num,true),
  _K(KK),_h(h),_alpha(alp){
      
    if(_K<0) Msg::Error("exponentialKinematicHardening: negative _K parameter");
    _temFunc_K= new constantScalarFunction(1.);
};
  
exponentialKinematicHardeningLaw::exponentialKinematicHardeningLaw(const exponentialKinematicHardeningLaw& src): 
      kinematicHardening(src), _K(src._K), _h(src._h),_alpha(src._alpha){
          
    _temFunc_K = NULL;
    if (src._temFunc_K != NULL){
        _temFunc_K = src._temFunc_K->clone();
    }
};

exponentialKinematicHardeningLaw& exponentialKinematicHardeningLaw::operator =(const kinematicHardening& src){
  kinematicHardening::operator =(src);
  const exponentialKinematicHardeningLaw* psrc  = dynamic_cast<const exponentialKinematicHardeningLaw*>(&src);
  if (psrc != NULL){
    _K = psrc->_K;
    _h = psrc->_h;
    _alpha = psrc->_alpha;
    
    if (psrc->_temFunc_K != NULL){
      if (_temFunc_K != NULL){
        _temFunc_K->operator=(*psrc->_temFunc_K);
      }
      else{
        _temFunc_K = psrc->_temFunc_K->clone();
      }
    }
  }
  return *this;
};

exponentialKinematicHardeningLaw::~exponentialKinematicHardeningLaw(){
  if (_temFunc_K != NULL) {delete _temFunc_K; _temFunc_K = NULL;};
};

void exponentialKinematicHardeningLaw::setTemperatureFunction_K(const scalarFunction& Tfunc){
  if (_temFunc_K != NULL) delete _temFunc_K;
  _temFunc_K = Tfunc.clone();
}

void exponentialKinematicHardeningLaw::createIPVariable(IPKinematicHardening* &ipv) const
{
  if(ipv != NULL) delete ipv;
  ipv = new IPKinematicHardening();
}

void exponentialKinematicHardeningLaw::hardening(double p0, const IPKinematicHardening &ipvprev, double p, IPKinematicHardening &ipv) const
{
  double R(0.), dR(0.), ddR(0.), dRdT(0.), ddRdTT(0.), ddRdT(0.); 
  if (ipvprev.isSaturated()){
    R = ipvprev.getR();
    dR = 0.;
    ddR = 0.;
  }
  else{
    double term = -_h*pow(p,_alpha);
    double expTerm = exp(term);
    
    R = _K*(1.-expTerm);
    dR= _h*_alpha*pow(p,_alpha-1.)*(_K-R);
    ddR = _h*_alpha*((_alpha-1.)* pow(p,_alpha-2.) *(_K-R) - pow(p,_alpha-1.)*dR);
  }

  ipv.set(R,dR,ddR,dRdT,ddRdTT,ddRdT); 
}

void exponentialKinematicHardeningLaw::hardening(double p0, const IPKinematicHardening &ipvprev, double p, double T, IPKinematicHardening &ipv) const
{
  double R(0.), dR(0.), ddR(0.), dRdT(0.), ddRdTT(0.), ddRdT(0.); 
  if (ipvprev.isSaturated()){
    R = ipvprev.getR();
    dR = 0.;
    ddR = 0.;
  }
  else{
    double term = -_h*pow(p,_alpha);
    double expTerm = exp(term);
    double KT = _K*_temFunc_K->getVal(T);
    double dKdT = _K*_temFunc_K->getDiff(T);
    double ddKdTT = _K*_temFunc_K->getDoubleDiff(T);
    
    R = KT*(1.-expTerm);
    dR = _h*_alpha*pow(p,_alpha-1.)*(KT-R);
    ddR = _h*_alpha*((_alpha-1.)* pow(p,_alpha-2.) *(KT-R) - pow(p,_alpha-1.)*dR);
    dRdT = dKdT*(1.-expTerm);
    ddRdTT = ddKdTT*(1.-expTerm);
    ddRdT = dKdT*_h*_alpha*pow(p,_alpha-1.)*exp(-_h*pow(p,_alpha));
  }

  ipv.set(R,dR,ddR,dRdT,ddRdTT,ddRdT); //added dRdT
}

kinematicHardening * exponentialKinematicHardeningLaw::clone() const
{
  return new exponentialKinematicHardeningLaw(*this);
}
