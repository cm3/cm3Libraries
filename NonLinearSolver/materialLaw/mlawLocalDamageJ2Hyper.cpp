//
// C++ Interface: material law
//
// Description: j2 elasto-plastic law with non local damage interface
//
//
// Author:  <Van Dung NGUYEN>, (C) 2016
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "mlawLocalDamageJ2Hyper.h"
#include <math.h>

mlawLocalDamageJ2Hyper::mlawLocalDamageJ2Hyper(const int num,const double E,const double nu,
                           const double rho, const J2IsotropicHardening &_j2IH,
                           const DamageLaw &_damLaw,
			   const double tol, const bool pert, const double eps)
				: mlawJ2linear(num,E,nu,rho,_j2IH,tol,pert,eps)
{
  damLaw = _damLaw.clone();
  STensorOperation::zero(Cel);
    Cel(0,0,0,0) = _lambda + _mu2;
    Cel(1,1,0,0) = _lambda;
    Cel(2,2,0,0) = _lambda;
    Cel(0,0,1,1) = _lambda;
    Cel(1,1,1,1) = _lambda + _mu2;
    Cel(2,2,1,1) = _lambda;
    Cel(0,0,2,2) = _lambda;
    Cel(1,1,2,2) = _lambda;
    Cel(2,2,2,2) = _lambda + _mu2;

    Cel(1,0,1,0) = _mu;
    Cel(2,0,2,0) = _mu;
    Cel(0,1,0,1) = _mu;
    Cel(2,1,2,1) = _mu;
    Cel(0,2,0,2) = _mu;
    Cel(1,2,1,2) = _mu;

    Cel(0,1,1,0) = _mu;
    Cel(0,2,2,0) = _mu;
    Cel(1,0,0,1) = _mu;
    Cel(1,2,2,1) = _mu;
    Cel(2,0,0,2) = _mu;
    Cel(2,1,1,2) = _mu;


}
mlawLocalDamageJ2Hyper::mlawLocalDamageJ2Hyper(const mlawLocalDamageJ2Hyper &source) : mlawJ2linear(source),Cel(source.Cel)
{
  if(source.damLaw != NULL)
  {
    damLaw=source.damLaw->clone();
  }
}


void mlawLocalDamageJ2Hyper::createIPState(IPStateBase* &ips,bool hasBodyForce,const bool* state_,const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
  IPVariable* ipvi = new IPLocalDamageJ2Hyper(_j2IH,damLaw);
  IPVariable* ipv1 = new IPLocalDamageJ2Hyper(_j2IH,damLaw);
  IPVariable* ipv2 = new IPLocalDamageJ2Hyper(_j2IH,damLaw);
  if(ips != NULL) delete ips;
  ips = new IP3State(state_,ipvi,ipv1,ipv2);
}

double mlawLocalDamageJ2Hyper::soundSpeed() const
{
  return mlawJ2linear::soundSpeed();
}


void mlawLocalDamageJ2Hyper::constitutive(const STensor3& F0,
                                          const STensor3& Fn,
                                          STensor3 &P,
                                          const IPLocalDamageJ2Hyper *q0, 
                                          IPLocalDamageJ2Hyper *q1,
                                          STensor43 &Tangent, 
                                          const bool stiff, 
                                          STensor43* elasticTangent,
                                          const bool dTangent,
                                          STensor63* dCalgdeps) const
{

  static STensor43 dFpdF, dFedF;
  static STensor3 Fe, Peff;
  static STensor3 Fpinv;
  static STensor3 dpdF;
  mlawJ2linear::predictorCorector(F0,Fn,Peff,q0,q1,Tangent,dFpdF,dFedF,dpdF,stiff);
  const STensor3 &Fp  = q1->getConstRefToFp();
  STensorOperation::inverseSTensor3(Fp,Fpinv);
  STensorOperation::multSTensor3(Fn,Fpinv,Fe);

  double ene = q1->defoEnergy();

  if (q1->dissipationIsBlocked()){
  //if (1){
    // at bulk elelent when damage is blocked
    // damage stop increasing
    IPDamage& curDama = q1->getRefToIPDamage();
    curDama.getRefToDamage() = q0->getDamage();
    curDama.getRefToDDamageDp() = 0.;
    STensorOperation::zero(curDama.getRefToDDamageDFe());
    curDama.getRefToDeltaDamage() = 0;
    curDama.getRefToMaximalP() = q1->getMaximalP();
    //Msg::Info("bulk damage stops increasing = %e",ipvcur->getDamage());

    q1->getRefToDissipationActive() = (false);
  }
  else{
    // compute damage for ipcur
    damLaw->computeDamage(q1->getConstRefToEquivalentPlasticStrain(),q0->getConstRefToEquivalentPlasticStrain(),
                        ene, Fe, Fp, Peff, Cel,
                        q0->getConstRefToIPDamage(),q1->getRefToIPDamage());

    // check active damage
    if ((q1->getDamage() > q0->getDamage()) and (q1->getConstRefToEquivalentPlasticStrain() > q0->getConstRefToEquivalentPlasticStrain())){
      q1->getRefToDissipationActive()  = (true);
    }
    else{
      q1->getRefToDissipationActive() = (false);
    }
  }

  // get true PK1 stress from damage and effective PK1 stress
  double D = q1->getDamage();
	P = Peff;
  P*=(1.-D);
  double elEne = (1-D)*ene;
  q1->_elasticEnergy=elEne;

  if(stiff)
  {
    static STensor3 DDamageDF;

    for (int i=0; i<3; i++){
      for (int j=0; j<3; j++){
        DDamageDF(i,j) = 0.;
        for (int k=0; k<3; k++){
          for (int l=0; l<3; l++){
            DDamageDF(i,j) += q1->getConstRefToDDamageDFe()(k,l)*dFedF(k,l,i,j);
          }
        }
      }
    }

    for (int i=0; i<3; i++){
      for (int j=0; j<3; j++){
        DDamageDF(i,j) +=q1->getDDamageDp()*dpdF(i,j);
      }
    }

    // we need to correct partial P/partial F: (1-D) partial P/partial F - Peff otimes partial D partial F
    Tangent*=(1.-D);
    for(int i=0;i<3;i++)
    {
      for(int j=0;j<3;j++)
      {
        for(int k=0;k<3;k++)
        {
          for(int l=0;l<3;l++)
          {
            Tangent(i,j,k,l)-=Peff(i,j)*DDamageDF(k,l);

          }
        }
      }
    }

  }
}

