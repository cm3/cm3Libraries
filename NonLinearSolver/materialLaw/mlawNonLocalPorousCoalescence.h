//
// C++ Interface: material law
//
// Description: Coalescence elasto-plastic law with non-local damage
//
// Author:  <J. Leclerc, V.D - Nguyen>, (C) 2017
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef MLAWNONLOCALPOROUSCOALESCENCE_H_
#define MLAWNONLOCALPOROUSCOALESCENCE_H_
#include "mlawNonLocalPorous.h"

class mlawNonLocalPorousThomasonLaw : public mlawNonLocalPorosity
{
  #ifndef SWIG
  protected:
    double _n; // interpolation exponent
    
    /// Void state evolution law
    voidStateEvolutionLaw* _voidEvolutionLaw;
    /// Coalescense law
    CoalescenceLaw* _coalescenceLaw;


  #endif //SWIG
  public:

    // Default constructor
    mlawNonLocalPorousThomasonLaw(const int num,const double E,const double nu, const double rho,
                        const double fVinitial, const double lambda0, const J2IsotropicHardening &j2IH, const CLengthLaw &cLLaw,
                        const double tol=1.e-8, const bool matrixbyPerturbation = false, const double pert = 1e-8);

                        // Default constructor
    mlawNonLocalPorousThomasonLaw(const int num,const double E,const double nu, const double rho,
                        const double fVinitial, const double lambda0, const double kappa, const J2IsotropicHardening &j2IH, const CLengthLaw &cLLaw,
                        const double tol=1.e-8, const bool matrixbyPerturbation = false, const double pert = 1e-8);

    // Option settings
    virtual void setVoidEvolutionLaw(const voidStateEvolutionLaw& voidtateLaw);
    virtual void setCoalescenceLaw(const CoalescenceLaw& added_coalsLaw); 
    virtual void setYieldSurfaceExponent(const double newN);
  #ifndef SWIG
    // Constructors & destructor
    mlawNonLocalPorousThomasonLaw(const mlawNonLocalPorousThomasonLaw &source);
    virtual ~mlawNonLocalPorousThomasonLaw();
    virtual materialLaw* clone() const {return new mlawNonLocalPorousThomasonLaw(*this);};


    // Function of materialLaw
    virtual matname getType() const {return materialLaw::nonLocalDamageThomason;};
    virtual void createIPState(IPStateBase* &ips,bool hasBodyForce,const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const IntPt *GP=NULL, const int gpt = 0) const;
    virtual void createIPVariable(const double fInit, IPNonLocalPorosity* &ipv) const;
    virtual void initLaws(const std::map<int,materialLaw*> &maplaw); // this law is initialized so nothing to do
  
    const voidStateEvolutionLaw* getVoidEvolutionLaw() const {return _voidEvolutionLaw;};
    const CoalescenceLaw* getCoalescenceLaw() const {return _coalescenceLaw;};

  public:
    virtual void voidStateGrowth(const double yieldfV0, const double yieldfV1, // yield porosity
                        const double DeltahatD, const double DeltahatQ, const double DeltahatP, // internal variables
                        const IPNonLocalPorosity *q0, IPNonLocalPorosity *q1) const;
    // Case dependent function from mlawNonLocalPorous needed to be specified
    virtual double yieldFunction(const double kcorEq, const double pcor, const double R, const double yieldfV,
                                const IPNonLocalPorosity* q0, const IPNonLocalPorosity* q1, const double* T = NULL,
                                bool diff=false, fullVector<double>* grad = NULL,
                                bool withthermic = false, double* dfdT = NULL) const;

    virtual void plasticFlow(double & Ns, double& Nv,
                              const double kcorEq, const double pcor, const double R,
                              const IPNonLocalPorosity* q0, const IPNonLocalPorosity* q1, const double* T = NULL,
                              bool diff=false, fullVector<double>* gradNs=NULL, // grad of Ns with respect to [kcorEq pcor R fV Chi W]
                              fullVector<double>* gradNv = NULL, // grad of Nv with respect to  [kcorEq pcor R fV Chi W]
                              bool withTher = false, double* dNsDT = NULL, double* dNvDT=NULL) const;

    virtual double getOnsetCriterion(const IPNonLocalPorosity* q1) const;
    virtual void checkCoalescence(IPNonLocalPorosity* q1, const IPNonLocalPorosity *q0, const double* T) const;
    virtual void forceCoalescence(IPNonLocalPorosity* q1, const IPNonLocalPorosity *q0, const double* T) const;
    virtual bool checkCrackCriterion(IPNonLocalPorosity* q1, const IPNonLocalPorosity *q0, const STensor3& Fcur, const SVector3& normal, double delayFactor, const double* T) const;
    virtual void checkFailed(IPNonLocalPorosity* q1, const IPNonLocalPorosity *q0) const;

  protected:
    // Specific functions
    void yieldFunctionBase(double p, const double s, double& f,
                          bool diff=false, double* dfdp=NULL , double* dfds=NULL,
                          bool hess=false, double* ddfdpdp=NULL, double* ddfdpds=NULL,double* ddfdsdp=NULL,double* ddfdsds=NULL) const;

      // general interafce for I1-J2-J3 plasticity
  public:
    virtual double I1J2J3_yieldFunction(const STensor3& kcor, const double R, const IPNonLocalPorosity* q0, const IPNonLocalPorosity* q1, const double* T = NULL,
                                        bool diff=false, STensor3* DFDkcor=NULL, double* DFDR=NULL, std::vector<double>* DFDY=NULL,
                                        bool withThermic=false, double *DFDT=NULL) const;
    virtual void I1J2J3_getYieldNormal(STensor3& Np, const STensor3& kcor, const double R, const IPNonLocalPorosity* q0, const IPNonLocalPorosity* q1, const double* T = NULL,
                                        bool diff=false, STensor43* DNpDkcor=NULL, STensor3* DNpDR=NULL, std::vector<STensor3>* DNpDY=NULL,
                                        bool withThermic=false, STensor3* DNpDT=NULL
                                        ) const;
  #endif //SWIG
};



// MPS- maximum principal stress
class mlawNonLocalPorousThomasonLawWithMPS : public mlawNonLocalPorousThomasonLaw
{
  protected:
    double _cosSmmoothFactor; // smoothing factor

  public:
    // Default constructor
    mlawNonLocalPorousThomasonLawWithMPS(const int num,const double E,const double nu, const double rho,
                        const double fVinitial, const double lambda0, const J2IsotropicHardening &j2IH, const CLengthLaw &cLLaw,
                        const double tol=1.e-8, const bool matrixbyPerturbation = false, const double pert = 1e-8);

                        // Default constructor
    mlawNonLocalPorousThomasonLawWithMPS(const int num,const double E,const double nu, const double rho,
                        const double fVinitial, const double lambda0, const double kappa, const J2IsotropicHardening &j2IH, const CLengthLaw &cLLaw,
                        const double tol=1.e-8, const bool matrixbyPerturbation = false, const double pert = 1e-8);
    virtual void setSmoothFactor(double cs);    
    #ifndef SWIG
    // Constructors & destructor
    mlawNonLocalPorousThomasonLawWithMPS(const mlawNonLocalPorousThomasonLawWithMPS &source);
    virtual ~mlawNonLocalPorousThomasonLawWithMPS(){};
    virtual materialLaw* clone() const {return new mlawNonLocalPorousThomasonLawWithMPS(*this);};

    // Case-dependent functions
     virtual double yieldFunction(const double kcorEq, const double pcor, const double R, const double yieldfV,
                                const IPNonLocalPorosity* q0, const IPNonLocalPorosity* q1, const double* T = NULL,
                                bool diff=false, fullVector<double>* grad = NULL,
                                bool withthermic = false, double* dfdT = NULL) const
                                {
                                  Msg::Error("mlawNonLocalPorousThomasonLawWithMPS::yieldFunction cannot be called");
                                  return -1.;
                                }

    virtual void plasticFlow(double & Ns, double& Nv,
                              const double kcorEq, const double pcor, const double R,
                              const IPNonLocalPorosity* q0, const IPNonLocalPorosity* q1, const double* T = NULL,
                              bool diff=false, fullVector<double>* gradNs=NULL, // grad of Ns with respect to [kcorEq pcor R fV Chi W]
                              fullVector<double>* gradNv = NULL, // grad of Nv with respect to  [kcorEq pcor R fV Chi W]
                              bool withTher = false, double* dNsDT = NULL, double* dNvDT=NULL) const
                              {
                                Msg::Error("mlawNonLocalPorousThomasonLawWithMPS::plasticFlow cannot be called");
                              }


      // general interafce for I1-J2-J3 plasticity
  public:
    virtual double I1J2J3_yieldFunction(const STensor3& kcor, const double R, const IPNonLocalPorosity* q0, const IPNonLocalPorosity* q1, const double* T = NULL,
                                        bool diff=false, STensor3* DFDkcor=NULL, double* DFDR=NULL, std::vector<double>* DFDY=NULL,
                                        bool withThermic=false, double *DFDT=NULL) const;
    virtual void I1J2J3_getYieldNormal(STensor3& Np, const STensor3& kcor, const double R, const IPNonLocalPorosity* q0, const IPNonLocalPorosity* q1, const double* T = NULL,
                                        bool diff=false, STensor43* DNpDkcor=NULL, STensor3* DNpDR=NULL, std::vector<STensor3>* DNpDY=NULL,
                                        bool withThermic=false, STensor3* DNpDT=NULL
                                        ) const;
  private:
    void cosLodeAngle(double regFact, double xi, double& F, bool diff=false, double* dF=NULL, bool hess=false, double* ddF=NULL) const;

    // for F = cos(acos(xi)/3)
    void cosFunc(double xi, double& F, bool diff=false, double* dF=NULL, bool hess=false, double* ddF=NULL) const;
    #endif //SWIG

};

// MSS -maximum shear stress
class mlawNonLocalPorousThomasonLawWithMSS: public mlawNonLocalPorousThomasonLaw
{
  protected:
    double _sinSmmoothFactor; // smoothing factor for sinus of lode angle

  public:
    // Default constructor
    mlawNonLocalPorousThomasonLawWithMSS(const int num,const double E,const double nu, const double rho,
                        const double fVinitial, const double lambda0, const J2IsotropicHardening &j2IH, const CLengthLaw &cLLaw,
                        const double tol=1.e-8, const bool matrixbyPerturbation = false, const double pert = 1e-8);

                        // Default constructor
    mlawNonLocalPorousThomasonLawWithMSS(const int num,const double E,const double nu, const double rho,
                        const double fVinitial, const double lambda0, const double kappa, const J2IsotropicHardening &j2IH, const CLengthLaw &cLLaw,
                        const double tol=1.e-8, const bool matrixbyPerturbation = false, const double pert = 1e-8);
    virtual void setSmoothFactor(double cs);
    virtual void setShearFactor(double g);
    virtual void setVoidLigamentExponent(double e);
    #ifndef SWIG
    mlawNonLocalPorousThomasonLawWithMSS(const mlawNonLocalPorousThomasonLawWithMSS& src);
    virtual ~mlawNonLocalPorousThomasonLawWithMSS(){}
    virtual materialLaw* clone() const {return new mlawNonLocalPorousThomasonLawWithMSS(*this);};
  public:
    // Case-dependent functions
     virtual double yieldFunction(const double kcorEq, const double pcor, const double R, const double yieldfV,
                                const IPNonLocalPorosity* q0, const IPNonLocalPorosity* q1, const double* T = NULL,
                                bool diff=false, fullVector<double>* grad = NULL,
                                bool withthermic = false, double* dfdT = NULL) const
                                {
                                  Msg::Error("mlawNonLocalPorousThomasonLawWithMSS::yieldFunction cannot be called");
                                  return -1.;
                                }

    virtual void plasticFlow(double & Ns, double& Nv,
                              const double kcorEq, const double pcor, const double R,
                              const IPNonLocalPorosity* q0, const IPNonLocalPorosity* q1, const double* T = NULL,
                              bool diff=false, fullVector<double>* gradNs=NULL, // grad of Ns with respect to [kcorEq pcor R fV Chi W]
                              fullVector<double>* gradNv = NULL, // grad of Nv with respect to  [kcorEq pcor R fV Chi W]
                              bool withTher = false, double* dNsDT = NULL, double* dNvDT=NULL) const
                              {
                                Msg::Error("mlawNonLocalPorousThomasonLawWithMSS::plasticFlow cannot be called");
                              }

    virtual double I1J2J3_yieldFunction(const STensor3& kcor, const double R, const IPNonLocalPorosity* q0, const IPNonLocalPorosity* q1, const double* T = NULL,
                                        bool diff=false, STensor3* DFDkcor=NULL, double* DFDR=NULL, std::vector<double>* DFDY=NULL,
                                        bool withThermic=false, double *DFDT=NULL) const;
    virtual void I1J2J3_getYieldNormal(STensor3& Np, const STensor3& kcor, const double R, const IPNonLocalPorosity* q0, const IPNonLocalPorosity* q1, const double* T = NULL,
                                        bool diff=false, STensor43* DNpDkcor=NULL, STensor3* DNpDR=NULL, std::vector<STensor3>* DNpDY=NULL,
                                        bool withThermic=false, STensor3* DNpDT=NULL
                                        ) const;
  private:
    void sinLodeAngle_shear(double regFact, double xi, double& F, bool diff=false, double* dF=NULL, bool hess=false, double* ddF=NULL) const;
    void cosLodeAngle_shear(double regFact, double xi, double& F, bool diff=false, double* dF=NULL, bool hess=false, double* ddF=NULL) const;
     // for F = sin(acos(xi)/3)
    void sinFunc_acos(double xi, double& F, bool diff=false, double* dF=NULL, bool hess=false, double* ddF=NULL) const;
    // for F = cos(acos(xi)/3)
    void cosFunc_acos(double xi, double& F, bool diff=false, double* dF=NULL, bool hess=false, double* ddF=NULL) const;
    #endif //SWIG
};

#endif // MLAWNONLOCALPOROUSCOALESCENCE_H_
