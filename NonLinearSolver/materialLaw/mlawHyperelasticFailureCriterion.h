//
// C++ Interface: failure criterions
//
// Author:  <Van Dung Nguyen>, (C) 2018
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef MLAWHYPERELASTICFAILURECRITERION_H_
#define MLAWHYPERELASTICFAILURECRITERION_H_

#ifndef SWIG
#include "STensor3.h"
#include "ipvariable.h"
#endif // SWIG
#include "materialStrengthLaw.h"


class mlawHyperelasticFailureCriterionBase{
  protected:
    int _num;
    bool _init;
  #ifndef SWIG
  public:
    mlawHyperelasticFailureCriterionBase(const int num, const bool init = true): _num(num),_init(init){};
    mlawHyperelasticFailureCriterionBase(const mlawHyperelasticFailureCriterionBase& src): _num(src._num),_init(src._init){}
    virtual ~mlawHyperelasticFailureCriterionBase(){};
    // failure check based on stress state tensor and internal variable
    virtual bool isFailed(const IPVariable* ipvprev, const IPVariable* ipv) const = 0;
    virtual double getFailureCriterion(const IPVariable* ipvprev, const IPVariable* ipv) const = 0;
    virtual mlawHyperelasticFailureCriterionBase* clone() const = 0;
  #endif // SWIG
};

class mlawHyperelasticFailureTrivialCriterion : public mlawHyperelasticFailureCriterionBase{
  public:
    mlawHyperelasticFailureTrivialCriterion(const int num);
    #ifndef SWIG
    mlawHyperelasticFailureTrivialCriterion(const mlawHyperelasticFailureTrivialCriterion& src): mlawHyperelasticFailureCriterionBase(src){}
    virtual ~mlawHyperelasticFailureTrivialCriterion(){}
    virtual double getFailureCriterion(const IPVariable* ipvprev, const IPVariable* ipv) const {return -1e10;};
    virtual bool isFailed(const IPVariable* ipvprev, const IPVariable* ipv) const {return false;};
    virtual mlawHyperelasticFailureTrivialCriterion* clone() const {return new mlawHyperelasticFailureTrivialCriterion(*this);};
    #endif // SWIG
};


class PowerFailureCriterion : public mlawHyperelasticFailureCriterionBase{
  protected:
    #ifndef SWIG
    RateFailureLaw* _compFailure;
    RateFailureLaw* _tracFailure;
    double _failurePower;
    #endif //SWIG

  public:
    PowerFailureCriterion(const int num, const RateFailureLaw& trac, const RateFailureLaw& comp, const double power);
    #ifndef SWIG
    PowerFailureCriterion(const PowerFailureCriterion& src);
    virtual ~PowerFailureCriterion();
    virtual double getFailureCriterion(const IPVariable* ipvprev, const IPVariable* ipv) const;
    virtual bool isFailed(const IPVariable* ipvprev, const IPVariable* ipv) const;
    virtual mlawHyperelasticFailureCriterionBase* clone() const {return new PowerFailureCriterion(*this);};
    #endif //SWIG  
};

class CritialPlasticDeformationCriteration : public mlawHyperelasticFailureCriterionBase{
  protected:
    #ifndef SWIG
    double _a, _b, _c; // p_cr = a*(exp(-b*X))+c (X-stress-triaxiality = p/sigVM)
    #endif //SWIG
 
  public:
    CritialPlasticDeformationCriteration(const int num, const double a, const double b, const double c);
    #ifndef SWIG
    CritialPlasticDeformationCriteration(const CritialPlasticDeformationCriteration& src);
    virtual ~CritialPlasticDeformationCriteration(){};
    virtual double getFailureCriterion(const IPVariable* ipvprev, const IPVariable* ipv) const;
    virtual bool isFailed(const IPVariable* ipvprev, const IPVariable* ipv) const;
    virtual mlawHyperelasticFailureCriterionBase* clone() const {return new CritialPlasticDeformationCriteration(*this);};
    #endif //SWIG
};

class ThreeCriticalStressCriterion : public mlawHyperelasticFailureCriterionBase{
   protected:
    #ifndef SWIG
    RateFailureLaw* _compFailure;
    RateFailureLaw* _tracFailure;
    RateFailureLaw* _shearFailure;
    bool _isConstant;
    mutable double a, b, c; // cache for surface sigVM = c + a/(p+b) , a,b,c estimated from sigc, sigt, sigs
    
private:
    void getFactor(const double sigt, const double sigc, const double sigs, double& aa, double& bb, double& cc) const;
    #endif //SWIG
 
  public:
    ThreeCriticalStressCriterion(const int num, const RateFailureLaw& trac, const RateFailureLaw& comp, const RateFailureLaw& shear);
    #ifndef SWIG
    ThreeCriticalStressCriterion(const ThreeCriticalStressCriterion& src);
    virtual ~ThreeCriticalStressCriterion();
    virtual double getFailureCriterion(const IPVariable* ipvprev, const IPVariable* ipv) const;
    virtual bool isFailed(const IPVariable* ipvprev, const IPVariable* ipv) const;
    virtual mlawHyperelasticFailureCriterionBase* clone() const {return new ThreeCriticalStressCriterion(*this);};
    #endif //SWIG
};

// check nonlocal version
class CritialNonLocalPlasticDeformationCriteration : public mlawHyperelasticFailureCriterionBase{
  protected:
    #ifndef SWIG
    double _a, _b, _c; // p_cr = a*(exp(-b*X))+c (X-stress-triaxiality = p/sigVM)
    #endif //SWIG
 
  public:
    CritialNonLocalPlasticDeformationCriteration(const int num, const double a, const double b, const double c);
    #ifndef SWIG
    CritialNonLocalPlasticDeformationCriteration(const CritialNonLocalPlasticDeformationCriteration& src);
    virtual ~CritialNonLocalPlasticDeformationCriteration(){};
    virtual double getFailureCriterion(const IPVariable* ipvprev, const IPVariable* ipv) const;
    virtual bool isFailed(const IPVariable* ipvprev, const IPVariable* ipv) const;
    virtual mlawHyperelasticFailureCriterionBase* clone() const {return new CritialNonLocalPlasticDeformationCriteration(*this);};
    #endif //SWIG
};

#endif //MLAWHYPERELASTICFAILURECRITERION_H_