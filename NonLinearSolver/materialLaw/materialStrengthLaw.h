//
// Description: define material strength in function of internal variables
//              in order to model the evolution of the failure surface
//
// Author:  <V.D. Nguyen>, (C) 2015
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef MATERIALSTRENGTHLAW_H_
#define MATERIALSTRENGTHLAW_H_
#ifndef SWIG
#include <cstddef>
#endif
class RateFailureLaw {
  #ifndef SWIG
  protected:
    bool _init;
    int _num;
  public:
    RateFailureLaw(const int num, const bool init = true);
    RateFailureLaw(const RateFailureLaw& src);
    virtual RateFailureLaw& operator = (const RateFailureLaw& src);
    virtual ~RateFailureLaw();
    virtual double getRefStrength() const = 0;
    virtual void get(const double rate, double& R, double& dR) const = 0;
    virtual RateFailureLaw* clone() const  = 0;
    virtual bool isConstant() const = 0;
    #endif // SWIG
};

class ConstantFailureLaw : public RateFailureLaw{
  protected:
    double _X;
  public:
    ConstantFailureLaw(const int num, const double X, const bool init = true);
    #ifndef SWIG
    ConstantFailureLaw(const ConstantFailureLaw& src):RateFailureLaw(src),_X(src._X){};
    ConstantFailureLaw& operator= (const RateFailureLaw& src){
      RateFailureLaw::operator=(src);
      const ConstantFailureLaw* psrc = dynamic_cast<const ConstantFailureLaw*>(&src);
      if (psrc != NULL){
        _X = psrc->_X;
      }
      return *this;
    };
    virtual ~ConstantFailureLaw(){}
    virtual double getRefStrength() const{return _X;}
    virtual void get(const double rate, double& R, double& dR) const {
      R = _X;
      dR = 0.;
    };
    virtual RateFailureLaw* clone() const  {return new ConstantFailureLaw(*this);};
    virtual bool isConstant() const {return true;};
    #endif // SWIG
};

class PowerFailureLaw : public RateFailureLaw{
  // X = X_0 + A*rate^alp
  protected:
    double _X0, _A, _alp;

  public:
    PowerFailureLaw(const int num, const double X0, const double A, const double alp, const bool init = true);
    #ifndef SWIG
    PowerFailureLaw(const PowerFailureLaw& src);
    virtual PowerFailureLaw& operator =(const RateFailureLaw& src);
    virtual ~PowerFailureLaw();
    virtual double getRefStrength() const{return _X0;}
    virtual void get(const double rate, double& R, double& dR) const ;
    virtual RateFailureLaw* clone() const  {return new PowerFailureLaw(*this);};
    virtual bool isConstant() const {return false;};
    #endif // SWIG
};

class LograrithmicFailureLaw : public RateFailureLaw{
  // X = X_r *(1+ Ar*ln (rate/rr))
  protected:
    double _Xr;
    double _rr;
    double _Ar;

  public:
    LograrithmicFailureLaw(const int num, const double Xr, const double rr, const double A, const bool init = true);
    #ifndef SWIG
    LograrithmicFailureLaw(const LograrithmicFailureLaw& src);
    virtual LograrithmicFailureLaw& operator =(const RateFailureLaw& src);
    virtual ~LograrithmicFailureLaw();
    virtual double getRefStrength() const{return _Xr;}
    virtual void get(const double rate, double& R, double& dR) const ;
    virtual RateFailureLaw* clone() const  {return new LograrithmicFailureLaw(*this);};
    virtual bool isConstant() const {return false;};
    #endif // SWIG
};

#endif // MATERIALSTRENGTHLAW_H_
