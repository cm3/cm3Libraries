//
// C++ Interface: material law
//
// Author:  <Van Dung Nguyen>, (C) 2017
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "elasticPotential.h"
#include "highOrderTensor.h"

smallStrainLinearElasticPotential::smallStrainLinearElasticPotential(const int num, const double E, const double nu): elasticPotential(num), _ERef(E), _nuRef(nu){

  double K = E/(3.*(1.-2.*nu));
  double mu = E/(2.*(1.+nu));
  double lambda = K- 2.*mu/3.;
  static const STensor3 I(1.);
  for (int i=0; i<3; i++){
    for (int j=0; j<3; j++){
      for (int k=0; k<3; k++){
        for (int l=0; l<3; l++){
          _Cel(i,j,k,l) = lambda*I(i,j)*I(k,l)+mu*(I(i,k)*I(j,l)+I(i,l)*I(j,k));
        }
      }
    }
	}
	_Cel.print("_Cel");
};
smallStrainLinearElasticPotential::smallStrainLinearElasticPotential(const smallStrainLinearElasticPotential& src) : elasticPotential(src),
	_ERef(src._ERef),_nuRef(src._nuRef), _Cel(src._Cel){};
  

void smallStrainLinearElasticPotential::createElasticTensor(const double Ex, const double Ey, const double Ez,
                                    const double Vxy, const double Vxz, const double Vyz,
                                    const double MUxy, const double MUxz, const double MUyz,
                                    const double alpha, const double beta, const double gamma, STensor43& CC)
{
  const double Vyx= Vxy*Ey/Ex  ;
	const double Vzx= Vxz*Ez/Ex  ;
	const double Vzy= Vyz*Ez/Ey  ;
	const double D=( 1-Vxy*Vyx-Vzy*Vyz-Vxz*Vzx-2*Vxy*Vyz*Vzx ) / ( Ex*Ey*Ez );

 	STensor43 ElasticityTensor(0.);

 	ElasticityTensor(0,0,0,0)=( 1-Vyz*Vzy ) / (Ey*Ez*D );
 	ElasticityTensor(1,1,1,1)=( 1-Vxz*Vzx ) / (Ex*Ez*D );
	ElasticityTensor(2,2,2,2)=( 1-Vyx*Vxy ) / (Ey*Ex*D );

	ElasticityTensor(0,0,1,1)=( Vyx+Vzx*Vyz ) / (Ey*Ez*D );
	ElasticityTensor(0,0,2,2)=( Vzx+Vyx*Vzy ) / (Ey*Ez*D );
	ElasticityTensor(1,1,2,2)=( Vzy+Vxy*Vzx ) / (Ex*Ez*D );

	ElasticityTensor(1,1,0,0)=( Vxy+Vzy*Vxz ) / (Ex*Ez*D );
	ElasticityTensor(2,2,0,0)=( Vxz+Vxy*Vyz ) / (Ey*Ex*D );
	ElasticityTensor(2,2,1,1)=( Vyz+Vxz*Vyx ) / (Ey*Ex*D );

 	ElasticityTensor(1,2,1,2)=MUyz;ElasticityTensor(1,2,2,1)=MUyz;
	ElasticityTensor(2,1,2,1)=MUyz;ElasticityTensor(2,1,1,2)=MUyz;

	ElasticityTensor(0,1,0,1)=MUxy;ElasticityTensor(0,1,1,0)=MUxy;
	ElasticityTensor(1,0,1,0)=MUxy;ElasticityTensor(1,0,0,1)=MUxy;

	ElasticityTensor(0,2,0,2)=MUxz;ElasticityTensor(0,2,2,0)=MUxz;
	ElasticityTensor(2,0,2,0)=MUxz;ElasticityTensor(2,0,0,2)=MUxz;


	double c1,c2,c3,s1,s2,s3;
	double s1c2, c1c2;
	double pi(3.14159265359);
  double fpi = pi/180.;

	c1 = cos(alpha*fpi);
	s1 = sin(alpha*fpi);

	c2 = cos(beta*fpi);
	s2 = sin(beta*fpi);

	c3 = cos(gamma*fpi);
	s3 = sin(gamma*fpi);

	s1c2 = s1*c2;
	c1c2 = c1*c2;

  STensor3 R; //3x3 rotation matrix
	R(0,0) = c3*c1 - s1c2*s3;
	R(0,1) = c3*s1 + c1c2*s3;
	R(0,2) = s2*s3;

	R(1,0) = -s3*c1 - s1c2*c3;
	R(1,1) = -s3*s1 + c1c2*c3;
	R(1,2) = s2*c3;

	R(2,0) = s1*s2;
	R(2,1) = -c1*s2;
	R(2,2) = c2;

  for(int i=0;i<3;i++){
    for(int j=0;j<3;j++){
      for(int k=0;k<3;k++){
        for(int l=0;l<3;l++){
          CC(i,j,k,l)=0.;
          for(int m=0;m<3;m++){
            for(int n=0;n<3;n++){
              for(int o=0;o<3;o++){
                for(int p=0;p<3;p++){
                  CC(i,j,k,l)+=R(m,i)*R(n,j)*R(o,k)*R(p,l)*ElasticityTensor(m,n,o,p);
                }
              }
            }
          }
        }
      }
    }
  }
};

void smallStrainLinearElasticPotential::createElasticTensor(const double C00, const double C11, const double C22, const double C33, const double C44, const double C55,
                                                            const double C01, const double C02, const double C03, const double C04, const double C05,
                                                            const double C12, const double C13, const double C14, const double C15,
                                                            const double C23, const double C24, const double C25,
                                                            const double C34, const double C35,
                                                            const double C45,
                                                            STensor43& CC)
{
 	CC(0,0,0,0)=C00;
 	CC(1,1,1,1)=C11;
	CC(2,2,2,2)=C22;
	CC(0,1,0,1)=CC(0,1,1,0)=CC(1,0,0,1)=CC(1,0,1,0)=C33/2.;
	CC(0,2,0,2)=CC(0,2,2,0)=CC(2,0,0,2)=CC(2,0,2,0)=C44/2.;
	CC(1,2,1,2)=CC(1,2,2,1)=CC(2,1,1,2)=CC(2,1,2,1)=C55/2.;
	
	CC(0,0,1,1)=CC(1,1,0,0)=C01;
	CC(0,0,2,2)=CC(2,2,0,0)=C02;
	CC(0,0,1,2)=CC(0,0,2,1)=CC(1,2,0,0)=CC(2,1,0,0)=C05;
	CC(0,0,0,2)=CC(0,0,2,0)=CC(0,2,0,0)=CC(2,0,0,0)=C04;
	CC(0,0,0,1)=CC(0,0,1,0)=CC(0,1,0,0)=CC(1,0,0,0)=C03;

        CC(1,1,2,2)=CC(2,2,1,1)=C12;
        CC(1,1,1,2)=CC(1,1,2,1)=CC(1,2,1,1)=CC(2,1,1,1)=C15;
        CC(1,1,0,2)=CC(1,1,2,0)=CC(0,2,1,1)=CC(2,0,1,1)=C14;
        CC(1,1,0,1)=CC(1,1,1,0)=CC(0,1,1,1)=CC(1,0,1,1)=C13;
        
        CC(2,2,1,2)=CC(2,2,2,1)=CC(1,2,2,2)=CC(2,1,2,2)=C25;
        CC(2,2,0,2)=CC(2,2,2,0)=CC(0,2,2,2)=CC(2,0,2,2)=C24;
        CC(2,2,0,1)=CC(2,2,1,0)=CC(0,1,2,2)=CC(1,0,2,2)=C23;
        
        CC(0,2,0,1)=CC(0,2,1,0)=CC(2,0,0,1)=CC(2,0,1,0)=CC(0,1,0,2)=CC(0,1,2,0)=CC(1,0,0,2)=CC(1,0,2,0)=C34/2.;
        CC(1,2,0,1)=CC(1,2,1,0)=CC(2,1,0,1)=CC(2,1,1,0)=CC(0,1,1,2)=CC(0,1,2,1)=CC(1,0,1,2)=CC(1,0,2,1)=C35/2.;
        
        CC(1,2,0,2)=CC(1,2,2,0)=CC(2,1,0,2)=CC(2,1,2,0)=CC(0,2,1,2)=CC(0,2,2,1)=CC(2,0,1,2)=CC(2,0,2,1)=C45/2.;
        
        CC.print("_ElasticityTensor");
};
  
smallStrainLinearElasticPotential::smallStrainLinearElasticPotential(const int num,
                                    const double Ex, const double Ey, const double Ez,
                                    const double Vxy, const double Vxz, const double Vyz,
                                    const double MUxy, const double MUxz, const double MUyz,
                                    const double alpha, const double beta, const double gamma): elasticPotential(num){
  smallStrainLinearElasticPotential::createElasticTensor(Ex,Ey, Ez,Vxy,Vxz,Vyz,MUxy,MUxz,MUyz,alpha,beta,gamma,_Cel);
  _ERef = std::max(Ex,std::max(Ey,Ez));

	//Init du _poissonMax
	if (Vxy >=  Vxz && Vxy >= Vyz){
    _nuRef=Vxy;
  }
	else if (Vxz >=  Vyz && Vxz >= Vxy){
    _nuRef=Vxz;
  }
	else{
    _nuRef=Vyz;
  }
  _Cel.print("_Cel");
};

smallStrainLinearElasticPotential::smallStrainLinearElasticPotential(const int num, const STensor43& C):elasticPotential(num),_Cel(C){
  _ERef = _Cel(0,0,0,0);
  _nuRef = 0.;
  _Cel.print("_Cel");
};
smallStrainLinearElasticPotential::smallStrainLinearElasticPotential(const int num, const fullMatrix<double>& C) :elasticPotential(num)
{
  if (C.size1() != 9 || C.size2() !=9)
  {
    Msg::Error("size of input matrix must be 9x9");
    C.print("C");
    Msg::Exit(0);
  }

  for (int row = 0; row <9; row ++)
  {
    int i, j;
    Tensor23::getIntsFromIndex(row,i,j);
    for (int col = 0; col < 9; col ++)
    {
      int k,l;
      Tensor23::getIntsFromIndex(col,k,l);
      _Cel(i,j,k,l) = C(row,col);
    }
  }
  _ERef = _Cel(0,0,0,0);
  _nuRef = 0.;
  _Cel.print("Cel");
};

smallStrainLinearElasticPotential::smallStrainLinearElasticPotential(const int num,
                                    const double C00, const double C11, const double C22, const double C33, const double C44, const double C55,
                                    const double C01, const double C02, const double C03, const double C04, const double C05,
                                    const double C12, const double C13, const double C14, const double C15,
                                    const double C23, const double C24, const double C25,
                                    const double C34, const double C35,
                                    const double C45): elasticPotential(num){
  smallStrainLinearElasticPotential::createElasticTensor(C00,C11,C22,C33,C44,C55,C01,C02,C03,C04,C05,C12,C13,C14,C15,C23,C24,C25,C34,C35,C45,_Cel);
  _Cel.print("_Cel");
};

double smallStrainLinearElasticPotential::get(const STensor3& F) const{
  // small strain value
  static STensor3 E;
  STensorOperation::diag(E,-1.);
	for (int i=0; i<3; i++){
		for (int j=0; j<3; j++){
			E(i,j) += (F(i,j) + F(j,i))*0.5;
		}
	}
	double val = 0.;
	for (int i=0; i<3;i++){
		for (int j=0; j<3; j++){
			for (int k=0; k<3; k++){
				for (int l=0; l<3; l++){
					val += E(i,j)*_Cel(i,j,k,l)*E(k,l)*0.5;
				}
			}
		}
	}
	return val;
};
void smallStrainLinearElasticPotential::constitutive(const STensor3& F, STensor3& P, const bool stiff, STensor43* L, const bool dTangent, STensor63* dCalgdeps) const{
  static STensor3 E;
  STensorOperation::diag(E,-1.);
	for (int i=0; i<3; i++){
		for (int j=0; j<3; j++){
			E(i,j) += (F(i,j) + F(j,i))*0.5;
		}
	}

	for (int i=0; i<3;i++){
		for (int j=0; j<3; j++){
                    P(i,j) = 0.;
			for (int k=0; k<3; k++){
				for (int l=0; l<3; l++){
					P(i,j) += _Cel(i,j,k,l)*E(k,l);
				}
			}
		}
	}
	if (stiff){
		(*L) = _Cel;
	}
	if(dTangent){
	    STensorOperation::zero(*dCalgdeps);
	}
};

void smallStrainLinearElasticPotential::getLocalValForNonLocalEquation(const STensor3& F, double& val, const bool stiff, STensor3* DvalDF) const{
	double Y = get(F); // potential
	double E = getYoungModulus(); // young modulus

	val = sqrt(2*Y/E); // isotropic nonlocal strain
	if (stiff){
		this->constitutive(F,*DvalDF,false,NULL);
		if (val > 1e-10){
			(*DvalDF)*= (1./(E*val));
		}
		else{
			(*DvalDF)*= (1./(E*1e-10));
		}

	}
};

// for anisotropic
double smallStrainLinearElasticPotential::getPositiveValue(const STensor3& F) const {
  return get(F);
};
double smallStrainLinearElasticPotential::getNegativeValue(const STensor3& F) const {
  return 0.;
};
void smallStrainLinearElasticPotential::constitutivePositive(const STensor3& F, STensor3& P, const bool stiff, STensor43* L) const {
  this->constitutive(F,P,stiff,L);
};
void smallStrainLinearElasticPotential::constitutiveNegative(const STensor3& F, STensor3& P, const bool stiff, STensor43* L) const {
  STensorOperation::zero(P);
  if (stiff){
    STensorOperation::zero(*L);
  }
};
void smallStrainLinearElasticPotential::getLocalPositiveValForNonLocalEquation(const STensor3& F, double& val, const bool stiff, STensor3* DvalDF) const {
  this->getLocalValForNonLocalEquation(F,val,stiff,DvalDF);
};

largeStrainAnisotropicPotential::largeStrainAnisotropicPotential(const int num,
                                    const double Ex, const double Ey, const double Ez,
                                    const double Vxy, const double Vxz, const double Vyz,
                                    const double MUxy, const double MUxz, const double MUyz,
                                    const double alpha, const double beta, const double gamma): elasticPotential(num){
  smallStrainLinearElasticPotential::createElasticTensor(Ex,Ey, Ez,Vxy,Vxz,Vyz,MUxy,MUxz,MUyz,alpha,beta,gamma,_Cel);
  _Emax = std::max(Ex,std::max(Ey,Ez));

	//Init du _poissonMax
	if (Vxy >=  Vxz && Vxy >= Vyz){
    _poissonMax=Vxy;
  }
	else if (Vxz >=  Vyz && Vxz >= Vxy){
    _poissonMax=Vxz;
  }
	else{
    _poissonMax=Vyz;
  }
};

largeStrainAnisotropicPotential::largeStrainAnisotropicPotential(const int num, const STensor43& C):elasticPotential(num),_Cel(C){
  _Emax = _Cel(0,0,0,0);
  _poissonMax = 0.;
};

double largeStrainAnisotropicPotential::get(const STensor3& F) const{
  static STensor3 E;
  STensorOperation::diag(E,-0.5);
  for (int i=0; i<3; i++){
    for (int j=0; j<3; j++){
      E(i,j) += 0.5*(F(0,i)*F(0,j)+F(1,i)*F(1,j)+F(2,i)*F(2,j));
    }
  }

  double val = 0.;
  for (int i=0; i<3; i++){
    for (int j=0; j<3; j++){
      for (int k=0; k<3; k++){
        for (int l=0; l<3; l++){
          val += 0.5*E(i,j)*_Cel(i,j,k,l)*E(k,l);
        }
      }
    }
  }
  return val;
}; // elastic potential
void largeStrainAnisotropicPotential::constitutive(const STensor3& F, STensor3& P, const bool stiff, STensor43* L, const bool dTangent, STensor63* dCalgdeps) const{
  static STensor3 E;
  STensorOperation::diag(E,-0.5);
  for (int i=0; i<3; i++){
    for (int j=0; j<3; j++){
      E(i,j) += 0.5*(F(0,i)*F(0,j)+F(1,i)*F(1,j)+F(2,i)*F(2,j));
    }
  }

  static STensor3 secondPK;
  for (int i=0; i<3; i++){
    for (int j=0; j<3; j++){
      secondPK(i,j) = 0.;
      for (int k=0; k<3; k++){
        for (int l=0; l<3; l++){
          secondPK(i,j) += _Cel(i,j,k,l)*E(k,l);
        }
      }
    }
  }

  for (int i=0; i<3; i++){
    for (int j=0; j<3; j++){
      P(i,j) = F(i,0)*secondPK(0,j)+F(i,1)*secondPK(1,j)+F(i,2)*secondPK(2,j);
    }
  }

  if (stiff){
    STensorOperation::zero(*L);
    for (int i=0; i<3; i++){
      for (int j=0; j<3; j++){
        for (int p=0; p<3; p++){
          (*L)(i,j,i,p) += secondPK(p,j);
          for (int q=0; q<3; q++){
            for (int s=0; s<3; s++){
              for (int k=0; k<3; k++){
                (*L)(i,j,p,q) += F(i,k)*_Cel(k,j,q,s)*F(p,s);
              }
            }
          }
        }
      }
    }
  }
  if(dTangent){
      STensorOperation::zero(*dCalgdeps);
      static STensor3 I;
      STensorOperation::unity(I);
      
      for (int i=0; i<3; i++){
       for (int j=0; j<3; j++){
        for (int k=0; k<3; k++){
         for (int l=0; l<3; l++){
          for (int p=0; p<3; p++){
           for (int q=0; q<3; q++){
            for (int s=0; s<3; s++){ 
                (*dCalgdeps)(i,j,k,l,p,q) += I(i,k)*_Cel(l,j,q,s)*F(p,s)+ I(i,p)*_Cel(q,j,l,s)*F(k,s)+ I(k,p)*_Cel(s,j,l,q)*F(i,s);
             }
           }
          }
         }
        }
       }
      }
  }
};

void largeStrainAnisotropicPotential::getLocalValForNonLocalEquation(const STensor3& F, double& val, const bool stiff, STensor3* DvalDF) const{
  double Y = get(F);
	double E = getYoungModulus();

	val = sqrt(2*Y/E);
	if (stiff){
		STensorOperation::zero(*DvalDF);
    this->constitutive(F,*DvalDF,false,NULL);
    if (val > 1e-10){
      (*DvalDF) *= (1./(E*val));
    }
    else{
      (*DvalDF) *= (1./(E*1e-10));
    }
	}
};

// for anisotropic
double largeStrainAnisotropicPotential::getPositiveValue(const STensor3& F) const{
  return get(F);
};
double largeStrainAnisotropicPotential::getNegativeValue(const STensor3& F) const{
  return 0.;
};
void largeStrainAnisotropicPotential::constitutivePositive(const STensor3& F, STensor3& P, const bool stiff, STensor43* L) const{
  this->constitutive(F,P,stiff,L);
};
void largeStrainAnisotropicPotential::constitutiveNegative(const STensor3& F, STensor3& P, const bool stiff, STensor43* L) const{
  STensorOperation::zero(P);
  if (stiff){
    STensorOperation::zero(*L);
  }
};
void largeStrainAnisotropicPotential::getLocalPositiveValForNonLocalEquation(const STensor3& F, double& val, const bool stiff, STensor3* DvalDF) const{
  getLocalValForNonLocalEquation(F,val,stiff,DvalDF);
};


largeStrainAnisotropicPotential::largeStrainAnisotropicPotential(const largeStrainAnisotropicPotential& src): elasticPotential(src),
          _Cel(src._Cel),_poissonMax(src._poissonMax){};

largeStrainTransverseIsotropicPotential::largeStrainTransverseIsotropicPotential(const int num,
                                    const double ET, // inplane modulus
                                    const double EL,  // longitudinal modulus
                                    const double nuTT, // inplane Poisson ratio
                                    const double nuLT,  // inplane-longitudinal Possion ratio
                                    const double MUTT,  // inplane shear
                                    const double MUTL, // out-plane shear
                                    const double alpha, const double beta, const double gamma)
                                    : elasticPotential(num){
  const double nuTL = ET*nuLT/EL;
  const double D=(1.+nuTT)*(1.-nuTT-2*nuTL*nuLT)/(ET*ET*EL);

 	STensor43 ElasticityTensor(0.);

 	ElasticityTensor(0,0,0,0)=( 1-nuTL*nuLT ) / (ET*EL*D );
 	ElasticityTensor(1,1,1,1)=( 1-nuTL*nuLT ) / (ET*EL*D );
	ElasticityTensor(2,2,2,2)=( 1-nuTT*nuTT ) / (ET*ET*D );

	ElasticityTensor(0,0,1,1)=( nuTT+nuLT*nuTL) / (ET*EL*D );
	ElasticityTensor(0,0,2,2)=( nuLT+nuTT*nuLT) / (ET*EL*D );
	ElasticityTensor(1,1,2,2)=( nuLT+nuTT*nuLT) / (ET*EL*D );

	ElasticityTensor(1,1,0,0)=ElasticityTensor(0,0,1,1); //( nuTT+nuLT*nuTL) / (ET*EL*D );
	ElasticityTensor(2,2,0,0)=ElasticityTensor(0,0,2,2); //( nuTL+nuTT*nuTL) / (ET*ET*D );
	ElasticityTensor(2,2,1,1)=ElasticityTensor(1,1,2,2); //( nuTL+nuTT*nuTL) / (ET*ET*D );

 	ElasticityTensor(1,2,1,2)=MUTL;
  ElasticityTensor(1,2,2,1)=MUTL;
	ElasticityTensor(2,1,2,1)=MUTL;
  ElasticityTensor(2,1,1,2)=MUTL;

  ElasticityTensor(0,2,0,2)=MUTL;
  ElasticityTensor(0,2,2,0)=MUTL;
	ElasticityTensor(2,0,2,0)=MUTL;
  ElasticityTensor(2,0,0,2)=MUTL;

	ElasticityTensor(0,1,0,1)=MUTT;
  ElasticityTensor(0,1,1,0)=MUTT;
	ElasticityTensor(1,0,1,0)=MUTT;
  ElasticityTensor(1,0,0,1)=MUTT;

	double c1,c2,c3,s1,s2,s3;
	double s1c2, c1c2;
	double pi(3.14159265359);
  double fpi = pi/180.;

	c1 = cos(alpha*fpi);
	s1 = sin(alpha*fpi);

	c2 = cos(beta*fpi);
	s2 = sin(beta*fpi);

	c3 = cos(gamma*fpi);
	s3 = sin(gamma*fpi);

	s1c2 = s1*c2;
	c1c2 = c1*c2;

  STensor3 R; //3x3 rotation matrix
	R(0,0) = c3*c1 - s1c2*s3;
	R(0,1) = c3*s1 + c1c2*s3;
	R(0,2) = s2*s3;

	R(1,0) = -s3*c1 - s1c2*c3;
	R(1,1) = -s3*s1 + c1c2*c3;
	R(1,2) = s2*c3;

	R(2,0) = s1*s2;
	R(2,1) = -c1*s2;
	R(2,2) = c2;

  for(int i=0;i<3;i++){
    for(int j=0;j<3;j++){
      for(int k=0;k<3;k++){
        for(int l=0;l<3;l++){
          _Cel(i,j,k,l)=0.;
          for(int m=0;m<3;m++){
            for(int n=0;n<3;n++){
              for(int o=0;o<3;o++){
                for(int p=0;p<3;p++){
                  _Cel(i,j,k,l)+=R(m,i)*R(n,j)*R(o,k)*R(p,l)*ElasticityTensor(m,n,o,p);
                }
              }
            }
          }
        }
      }
    }
  }

  _Emax = std::max(ET,EL);
  _poissonMax = std::max(nuTT,std::max(nuTL,nuLT));
};

largeStrainTransverseIsotropicPotential::largeStrainTransverseIsotropicPotential(const largeStrainTransverseIsotropicPotential& src): elasticPotential(src),
          _Cel(src._Cel),_poissonMax(src._poissonMax){};


double largeStrainTransverseIsotropicPotential::get(const STensor3& F) const{
  static STensor3 E;
  STensorOperation::diag(E,-0.5);
  for (int i=0; i<3; i++){
    for (int j=0; j<3; j++){
      E(i,j) += 0.5*(F(0,i)*F(0,j)+F(1,i)*F(1,j)+F(2,i)*F(2,j));
    }
  }

  double val = 0.;
  for (int i=0; i<3; i++){
    for (int j=0; j<3; j++){
      for (int k=0; k<3; k++){
        for (int l=0; l<3; l++){
          val += 0.5*E(i,j)*_Cel(i,j,k,l)*E(k,l);
        }
      }
    }
  }
  return val;
}; // elastic potential
void largeStrainTransverseIsotropicPotential::constitutive(const STensor3& F, STensor3& P, const bool stiff, STensor43* L, const bool dTangent, STensor63* dCalgdeps) const{
  static STensor3 E;
  STensorOperation::diag(E,-0.5);
  for (int i=0; i<3; i++){
    for (int j=0; j<3; j++){
      E(i,j) += 0.5*(F(0,i)*F(0,j)+F(1,i)*F(1,j)+F(2,i)*F(2,j));
    }
  }

  static STensor3 secondPK;
  for (int i=0; i<3; i++){
    for (int j=0; j<3; j++){
      secondPK(i,j) = 0.;
      for (int k=0; k<3; k++){
        for (int l=0; l<3; l++){
          secondPK(i,j) += _Cel(i,j,k,l)*E(k,l);
        }
      }
    }
  }

  for (int i=0; i<3; i++){
    for (int j=0; j<3; j++){
      P(i,j) = F(i,0)*secondPK(0,j)+F(i,1)*secondPK(1,j)+F(i,2)*secondPK(2,j);
    }
  }

  if (stiff){
    STensorOperation::zero(*L);
    for (int i=0; i<3; i++){
      for (int j=0; j<3; j++){
        for (int p=0; p<3; p++){
          (*L)(i,j,i,p) += secondPK(p,j);
          for (int q=0; q<3; q++){
            for (int s=0; s<3; s++){
              for (int k=0; k<3; k++){
                (*L)(i,j,p,q) += F(i,k)*_Cel(k,j,q,s)*F(p,s);
              }
            }
          }
        }
      }
    }
  }
  
  if(dTangent){
      STensorOperation::zero(*dCalgdeps);
      static STensor3 I;
      STensorOperation::unity(I);
      
      for (int i=0; i<3; i++){
       for (int j=0; j<3; j++){
        for (int k=0; k<3; k++){
         for (int l=0; l<3; l++){
          for (int p=0; p<3; p++){
           for (int q=0; q<3; q++){
            for (int s=0; s<3; s++){ 
                (*dCalgdeps)(i,j,k,l,p,q) += I(i,k)*_Cel(l,j,q,s)*F(p,s)+ I(i,p)*_Cel(q,j,l,s)*F(k,s)+ I(k,p)*_Cel(s,j,l,q)*F(i,s);
             }
           }
          }
         }
        }
       }
      }
  }  
  
  
};

void largeStrainTransverseIsotropicPotential::getLocalValForNonLocalEquation(const STensor3& F, double& val, const bool stiff, STensor3* DvalDF) const{
  double Y = get(F);
	double E = getYoungModulus();

	val = sqrt(2*Y/E);
	if (stiff){
		STensorOperation::zero(*DvalDF);
    this->constitutive(F,*DvalDF,false,NULL);
    if (val > 1e-10){
      (*DvalDF) *= (1./(E*val));
    }
    else{
      (*DvalDF) *= (1./(E*1e-10));
    }
	}
};

// for anisotropic
double largeStrainTransverseIsotropicPotential::getPositiveValue(const STensor3& F) const{
  return get(F);
};
double largeStrainTransverseIsotropicPotential::getNegativeValue(const STensor3& F) const{
  return 0.;
};
void largeStrainTransverseIsotropicPotential::constitutivePositive(const STensor3& F, STensor3& P, const bool stiff, STensor43* L) const{
  this->constitutive(F,P,stiff,L);
};
void largeStrainTransverseIsotropicPotential::constitutiveNegative(const STensor3& F, STensor3& P, const bool stiff, STensor43* L) const{
  STensorOperation::zero(P);
  if (stiff){
    STensorOperation::zero(*L);
  }
};
void largeStrainTransverseIsotropicPotential::getLocalPositiveValForNonLocalEquation(const STensor3& F, double& val, const bool stiff, STensor3* DvalDF) const{
  getLocalValForNonLocalEquation(F,val,stiff,DvalDF);
};



biLogarithmicElasticPotential::biLogarithmicElasticPotential(const int num, const double E, const double nu, const int order, const bool pert, const double tol) :
	elasticPotential(num), _E(E), _nu(nu), _order(order), _ByPerturbation(pert),_tol(tol){
  _K = _E/(3.*(1.-2.*_nu));
  _mu = _E/(2.*(1.+_nu));
  double lambda = _K- 2.*_mu/3.;
  const STensor3 I(1.);
  for (int i=0; i<3; i++){
    for (int j=0; j<3; j++){
      for (int k=0; k<3; k++){
        for (int l=0; l<3; l++){
          _Cel(i,j,k,l) = lambda*I(i,j)*I(k,l)+_mu*(I(i,k)*I(j,l)+I(i,l)*I(j,k));
        }
      }
    }
  }
};

biLogarithmicElasticPotential::biLogarithmicElasticPotential(const biLogarithmicElasticPotential& src) : elasticPotential(src),
	_E(src._E),_nu(src._nu),_K(src._K),_mu(src._mu), _Cel(src._Cel),_order(src._order),
	_ByPerturbation(src._ByPerturbation),_tol(src._tol){};

double biLogarithmicElasticPotential::get(const STensor3& F) const{
  double J = F.determinant();
  static STensor3 C, logC;

  for (int i=0; i<3; i++)
    for (int j=0; j<3; j++)
      C(i,j) = F(0,i)*F(0,j)+F(1,i)*F(1,j)+F(2,i)*F(2,j);

  bool ok=STensorOperation::logSTensor3(C,_order,logC);
  if(!ok)
  {
     return sqrt(-1.); 
  }
  static STensor3 logCdev;
  double trlogC;
  STensorOperation::decomposeDevTr(logC, logCdev, trlogC);

  double val = 0.5*_K*log(J)*log(J)+ 0.25*_mu*logCdev.dotprod();
  return val;
};
void biLogarithmicElasticPotential::constitutive(const STensor3& F, STensor3& P, const bool stiff, STensor43* L, const bool dTangent, STensor63* dCalgdeps) const{
  if (_ByPerturbation){
     double potential = get(F);
     static STensor3 Fp;
     for (int i=0; i<3; i++){
       for (int j=0; j<3; j++){
         Fp=(F);
         Fp(i,j) += _tol;
         double potentialPlus = get(Fp);
         P(i,j) = (potentialPlus - potential)/_tol;
       }
     }

     if (stiff){
      static STensor3 Fp2,Pp;
      for (int i=0; i<3; i++){
        for (int j=0; j<3; j++){
          Fp2 = F;
          Fp2(i,j) += _tol;
          constitutive(Fp2,Pp,false,NULL);
          for (int p=0; p<3; p++){
            for (int q=0; q<3; q++){
              (*L)(p,q,i,j) = (Pp(p,q) - P(p,q))/_tol;
            }
          }
        }
      }
    }
    
    if(dTangent){
      STensorOperation::zero(*dCalgdeps);
      static STensor3 Fpp2,Ppp;
      static STensor43* Lp;
      for (int i=0; i<3; i++){
        for (int j=0; j<3; j++){
          Fpp2 = F;
          Fpp2(i,j) += _tol;
          constitutive(Fpp2,Ppp,true,Lp,false,NULL);
      
        for (int k=0; k<3; k++){
         for (int l=0; l<3; l++){
          for (int p=0; p<3; p++){
           for (int q=0; q<3; q++){
               (*dCalgdeps)(k,l,p,q,i,j) = ((*Lp)(k,l,p,q)-(*L)(k,l,p,q))/_tol;
            }
           }
          }
         }
        }
     }
    }     
   }
   else{
     double J = F.determinant();
     static STensor3 C, logC;
     for (int i=0; i<3; i++)
       for (int j=0; j<3; j++)
         C(i,j) = F(0,i)*F(0,j)+F(1,i)*F(1,j)+F(2,i)*F(2,j);
    static STensor3 invC;
    STensorOperation::inverseSTensor3(C, invC);

    static STensor43 dlogCdC;
    bool ok=STensorOperation::logSTensor3(C,_order,logC,&dlogCdC);
    if(!ok)
    {
       P(0,0) = P(1,1) = P(2,2) = sqrt(-1.);
       return; 
    }
    static STensor3 logCdev;
    double trlogC;
    STensorOperation::decomposeDevTr(logC, logCdev, trlogC);

    static STensor3 Svol, Sdev;
    for (int i=0; i<3;i++){
      for (int j=0; j<3; j++){
        Svol(i,j) = _K*log(J)*invC(i,j);
        Sdev(i,j) = 0.;
        for (int k=0; k<3; k++){
          Sdev(i,j) += _mu*invC(i,k)*logCdev(k,j);
        }
      };
    }

    for (int i=0; i<3; i++){
      for (int j=0; j<3; j++){
        P(i,j) = F(i,0)*(Svol(0,j)+Sdev(0,j))+F(i,1)*(Svol(1,j)+Sdev(1,j))+F(i,2)*(Svol(2,j)+Sdev(2,j));
      }
    }

    if (stiff){
      static STensor43 dSvoldC, dSdevdC;
      for (int i=0; i<3; i++){
        for (int j=0; j<3; j++){
          for (int k=0; k<3; k++){
            for (int l=0; l<3; l++){
              dSvoldC(i,j,k,l) = 0.5*_K*invC(i,j)*invC(k,l) - 0.5*_K*log(J)*(invC(i,k)*invC(j,l)+invC(i,l)*invC(j,k));
              dSdevdC(i,j,k,l) = 0.;
              for (int p=0; p<3; p++){
                dSdevdC(i,j,k,l) += -_mu*0.5*(invC(i,k)*invC(p,l)+invC(i,l)*invC(p,k))*logCdev(p,j)
                                    +_mu*invC(i,p)*dlogCdC(p,j,k,l)
                                    -_mu*invC(i,j)*dlogCdC(p,p,k,l)/3.;
              }
            };
          };
        };
      };


      STensorOperation::zero(*L);
      for (int i=0; i<3; i++){
        for (int j=0; j<3; j++){
          for (int p=0; p<3; p++){
            (*L)(i,j,i,p) += Svol(p,j)+Sdev(p,j);
            for (int q=0; q<3; q++){
              for (int s=0; s<3; s++){
                for (int k=0; k<3; k++){
                  (*L)(i,j,p,q) += 2.*F(i,k)*(dSvoldC(k,j,q,s)+dSdevdC(k,j,q,s))*F(p,s);
                }
              }
            }
          }
        }
      }
    
    if(dTangent){
      STensorOperation::zero(*dCalgdeps);
      
      static STensor63 dlogCddC, dlogCdevddC;
      bool ok=STensorOperation::logSTensor3(C,_order,logC,&dlogCdC, &dlogCddC);
      if(!ok)
      {
         P(0,0) = P(1,1) = P(2,2) = sqrt(-1.);
         return; 
      }
      static STensor63 dCddF,dSddC; 
      static STensor43 dSdF, dinvCdC; 
      STensorOperation::zero(dSdF);
      STensorOperation::zero(dinvCdC);
      STensorOperation::zero(dSddC);
      static STensor43 dlogCdevdC;
      static const STensor3 I(1.);
      
      for (int i=0; i<3; i++){
        for (int j=0; j<3; j++){
          for (int k=0; k<3; k++){            
            for (int l=0; l<3; l++){
              dinvCdC(i,j,k,l) = -0.5*(invC(i,k)*invC(j,l)+invC(i,l)*invC(j,k));
              dlogCdevdC(i,j,k,l) = dlogCdC(i,j,k,l);
              for (int r=0; r<3; r++){
                dSdF(i,j,k,l) += 2.0*(dSvoldC(i,j,l,r)+dSdevdC(i,j,l,r))*F(k,r);
                for (int s=0; s<3; s++){
                  dCddF(i,j,k,l,r,s) = I(k,r)*(I(i,l)*I(j,s)+I(i,s)*I(j,l));
                  dlogCdevddC(i,j,k,l,r,s) = dlogCddC(i,j,k,l,r,s);
                }
              }
            }
          }
        }
      }
    
      for (int i=0; i<3; i++){
        for (int j=0; j<3; j++){
          double trdlogC = 0.0;
          for (int k=0; k<3; k++){
             trdlogC += dlogCdC(k,k,i,j)/3.;
          }   
          for (int l=0; l<3; l++){
             dlogCdevdC(l,l,i,j) -= trdlogC;
          }
          for (int p=0; p<3; p++){
            for (int q=0; q<3; q++){
              trdlogC = 0.0;
              for (int m=0; m<3; m++){
                trdlogC += dlogCddC(m,m,i,j,p,q)/3.;
              } 
              for (int m=0; m<3; m++){
                dlogCdevddC(m,m,i,j,p,q) -= trdlogC;
              }
            }
           }     
         } 
       }      
                                  
      
      for (int i=0; i<3; i++){
        for (int j=0; j<3; j++){
          for (int k=0; k<3; k++){
            for (int l=0; l<3; l++){
              for (int p=0; p<3; p++){
                for (int q=0; q<3; q++){
                   dSddC(i,j,k,l,p,q) = 0.5*_K*(dinvCdC(i,j,p,q)*invC(k,l)+invC(i,j)*dinvCdC(k,l,p,q)) 
                                          - 0.25*_K*invC(p,q)*(invC(i,k)*invC(j,l)+invC(i,l)*invC(j,k))
                                          - 0.5*_K*log(J)*(dinvCdC(i,k,p,q)*invC(j,l)+invC(i,k)*dinvCdC(j,l,p,q)+dinvCdC(i,l,p,q)*invC(j,k)+invC(i,l)*dinvCdC(j,k,p,q));
                   for (int s=0; s<3; s++){
                      dSddC(i,j,k,l,p,q) += -_mu*0.5*(dinvCdC(i,k,p,q)*invC(s,l)+ invC(i,k)*dinvCdC(s,l,p,q)+ dinvCdC(i,l,p,q)*invC(s,k) 
                                               +invC(i,l)*dinvCdC(s,k,p,q))*logCdev(s,j)-_mu*0.5*(invC(i,k)*invC(s,l)+invC(i,l)*invC(s,k))*dlogCdevdC(s,j,p,q)
                                               +_mu*dinvCdC(i,s,p,q)*dlogCdevdC(s,j,k,l) + _mu*invC(i,s)*dlogCdevddC(s,j,k,l,p,q);
                   }
                 }                                         
               }
             }
           }
         }
       }            
      
      
      for (int i=0; i<3; i++){
        for (int j=0; j<3; j++){
          for (int k=0; k<3; k++){
            for (int l=0; l<3; l++){
              for (int p=0; p<3; p++){
                for (int q=0; q<3; q++){
                    (*dCalgdeps)(i,j,k,l,p,q) = I(i,k)*dSdF(l,j,p,q)+I(i,p)*dSdF(q,j,k,l);
                    for(int r=0; r<3; r++){                     
                      for(int s=0; s<3; s++){
                         for(int t=0; t<3; t++){
                            (*dCalgdeps)(i,j,k,l,p,q) +=  F(i,r)*(dSvoldC(r,j,s,t)+dSdevdC(r,j,s,t))*dCddF(s,t,k,l,p,q);
                            for(int m=0; m<3; m++){
                                for(int n=0; n<3; n++){
                                  (*dCalgdeps)(i,j,k,l,p,q) += F(i,r)*dSddC(r,j,s,t,m,n)*(I(s,l)*F(k,t)+F(k,s)*I(t,l))*(I(m,q)*F(p,n)+F(p,m)*I(n,q));
                                }
                            }
                         }
                      }
                   }
                }
              }
            }
          }
        }
       }  
      }   
    }
  }  
};



void biLogarithmicElasticPotential::getLocalValForNonLocalEquation(const STensor3& F, double& val, const bool stiff, STensor3* DvalDF) const{
	double Y = get(F);
	double E = getYoungModulus();

	val = sqrt(2*Y/E);
	if (stiff){
		STensorOperation::zero(*DvalDF);
		if (_ByPerturbation){
      static STensor3 Fp;
			for (int i=0; i<3; i++){
				for (int j=0; j<3; j++){
          Fp=(F);
					Fp(i,j) += _tol;
					double valp;
					getLocalValForNonLocalEquation(Fp,valp,false,NULL);
					(*DvalDF)(i,j) = (valp-val)/_tol;
				}
			}
		}
		else{
			this->constitutive(F,*DvalDF,false,NULL);
			if (val > 1e-10){
				(*DvalDF) *= (1./(E*val));
			}
			else{
				(*DvalDF) *= (1./(E*1e-10));
			}

		}
	}
};

// for anisotropic
double biLogarithmicElasticPotential::getPositiveValue(const STensor3& F) const {
  double J = F.determinant();
  static STensor3 C, logC;
  for (int i=0; i<3; i++)
    for (int j=0; j<3; j++)
      C(i,j) = F(0,i)*F(0,j)+F(1,i)*F(1,j)+F(2,i)*F(2,j);

  bool ok= STensorOperation::logSTensor3(C,_order,logC);
  if(!ok)
  {
     return sqrt(-1.);
  }
  static STensor3 logCdev;
  double trlogC;
  STensorOperation::decomposeDevTr(logC, logCdev, trlogC);

  if (J >=1.){
    return 0.5*_K*log(J)*log(J)+ 0.25*_mu*logCdev.dotprod();
  }
  else{
    return 0.25*_mu*logCdev.dotprod();
  }

};
double biLogarithmicElasticPotential::getNegativeValue(const STensor3& F) const {
  double J = F.determinant();
  if (J>=1.) return 0.;
  else return 0.5*_K*log(J)*log(J);
};
void biLogarithmicElasticPotential::constitutivePositive(const STensor3& F, STensor3& P, const bool stiff, STensor43* L) const {
  double J = F.determinant();
  double fact = 1.;
  if (J < 1.) fact = 0.;

  STensor3 C, logC;
  for (int i=0; i<3; i++)
    for (int j=0; j<3; j++)
      C(i,j) = F(0,i)*F(0,j)+F(1,i)*F(1,j)+F(2,i)*F(2,j);
  static STensor3 invC;
  STensorOperation::inverseSTensor3(C, invC);

  static STensor43 dlogCdC;
  bool ok=STensorOperation::logSTensor3(C,_order,logC,&dlogCdC);
  if(!ok)
  {
     P(0,0) = P(1,1) = P(2,2) = sqrt(-1.);
     return; 
  }
  static STensor3 logCdev;
  double trlogC;
  STensorOperation::decomposeDevTr(logC, logCdev, trlogC);

  static STensor3 Svol, Sdev;

  for (int i=0; i<3;i++){
    for (int j=0; j<3; j++){
      Svol(i,j) = _K*log(J)*invC(i,j);
      Sdev(i,j) = 0.;
      for (int k=0; k<3; k++){
        Sdev(i,j) += _mu*invC(i,k)*logCdev(k,j);
      }
    };
  }

  for (int i=0; i<3; i++){
    for (int j=0; j<3; j++){
      P(i,j) = F(i,0)*(fact*Svol(0,j)+Sdev(0,j))+F(i,1)*(fact*Svol(1,j)+Sdev(1,j))+F(i,2)*(fact*Svol(2,j)+Sdev(2,j));
    }
  }

  if (stiff){
    static STensor43 dSvoldC, dSdevdC;
    for (int i=0; i<3; i++){
      for (int j=0; j<3; j++){
        for (int k=0; k<3; k++){
          for (int l=0; l<3; l++){
            dSvoldC(i,j,k,l) = 0.5*_K*invC(i,j)*invC(k,l) - 0.5*_K*log(J)*(invC(i,k)*invC(j,l)+invC(i,l)*invC(j,k));
            dSdevdC(i,j,k,l) = 0.;
            for (int p=0; p<3; p++){
              dSdevdC(i,j,k,l) += -_mu*0.5*(invC(i,k)*invC(p,l)+invC(i,l)*invC(p,k))*logCdev(p,j)
                                  +_mu*invC(i,p)*dlogCdC(p,j,k,l)
                                  -_mu*invC(i,j)*dlogCdC(p,p,k,l)/3.;
            }
          };
        };
      };
    };


    STensorOperation::zero(*L);
    for (int i=0; i<3; i++){
      for (int j=0; j<3; j++){
        for (int p=0; p<3; p++){
          (*L)(i,j,i,p) += fact*Svol(p,j)+Sdev(p,j);
          for (int q=0; q<3; q++){
            for (int s=0; s<3; s++){
              for (int k=0; k<3; k++){
                (*L)(i,j,p,q) += 2.*F(i,k)*(fact*dSvoldC(k,j,q,s)+dSdevdC(k,j,q,s))*F(p,s);
              }
            }
          }
        }
      }
    }
  }
};
void biLogarithmicElasticPotential::constitutiveNegative(const STensor3& F, STensor3& P, const bool stiff, STensor43* L) const {
  double J = F.determinant();
  if (J< 1.){
    static STensor3 C;
    for (int i=0; i<3; i++)
      for (int j=0; j<3; j++)
        C(i,j) = F(0,i)*F(0,j)+F(1,i)*F(1,j)+F(2,i)*F(2,j);
    static STensor3 invC;
    STensorOperation::inverseSTensor3(C, invC);

    static STensor3 Svol;
    Svol=(invC);
    Svol *= (_K*log(J));

		for (int i=0; i<3; i++){
			for (int j=0; j<3; j++){
				P(i,j) = F(i,0)*(Svol(0,j))+F(i,1)*(Svol(1,j))+F(i,2)*(Svol(2,j));
      }
    }

    if (stiff){
      static STensor43 dSvoldC;
      for (int i=0; i<3; i++){
        for (int j=0; j<3; j++){
          for (int k=0; k<3; k++){
            for (int l=0; l<3; l++){
              dSvoldC(i,j,k,l) = 0.5*_K*invC(i,j)*invC(k,l) - 0.5*_K*log(J)*(invC(i,k)*invC(j,l)+invC(i,l)*invC(j,k));
            };
          };
        };
      };


      STensorOperation::zero(*L);
      for (int i=0; i<3; i++){
        for (int j=0; j<3; j++){
          for (int p=0; p<3; p++){
            (*L)(i,j,i,p) += Svol(p,j);
            for (int q=0; q<3; q++){
              for (int s=0; s<3; s++){
                for (int k=0; k<3; k++){
                  (*L)(i,j,p,q) += 2.*F(i,k)*dSvoldC(k,j,q,s)*F(p,s);
                }
              }
            }
          }
        }
      }
		}

  }
  else{
    STensorOperation::zero(P);
    if (stiff){
      STensorOperation::zero(*L);
    }
  }
};
void biLogarithmicElasticPotential::getLocalPositiveValForNonLocalEquation(const STensor3& F, double& val, const bool stiff, STensor3* DvalDF) const {
  double Y = getPositiveValue(F);
	double E = getYoungModulus();

	val = sqrt(2*Y/E);
	if (stiff){
		STensorOperation::zero(*DvalDF);
		if (_ByPerturbation){
      static STensor3 Fp;
			for (int i=0; i<3; i++){
				for (int j=0; j<3; j++){
          Fp=(F);
					Fp(i,j) += _tol;
					double valp;
					getLocalPositiveValForNonLocalEquation(Fp,valp,false,NULL);
					(*DvalDF)(i,j) = (valp-val)/_tol;
				}
			}
		}
		else{
			this->constitutivePositive(F,*DvalDF,false,NULL);
			if (val > 1e-10){
				(*DvalDF) *= (1./(E*val));
			}
			else{
				(*DvalDF) *= (1./(E*1e-10));
			}

		}
	}
};

NeoHookeanElasticPotential::NeoHookeanElasticPotential(const int num, const double E, const double nu, const bool pert, const double tol) :
	elasticPotential(num), _E(E), _nu(nu), _ByPerturbation(pert),_tol(tol){
  _K = _E/(3.*(1.-2.*_nu));
  _mu = _E/(2.*(1.+_nu));
  double lambda = _K- 2.*_mu/3.;
  static const STensor3 I(1.);
  for (int i=0; i<3; i++){
    for (int j=0; j<3; j++){
      for (int k=0; k<3; k++){
        for (int l=0; l<3; l++){
          _Cel(i,j,k,l) = lambda*I(i,j)*I(k,l)+_mu*(I(i,k)*I(j,l)+I(i,l)*I(j,k));
        }
      }
    }
  }
};

NeoHookeanElasticPotential::NeoHookeanElasticPotential(const NeoHookeanElasticPotential& src) : elasticPotential(src),
	_E(src._E),_nu(src._nu),_K(src._K),_mu(src._mu), _Cel(src._Cel),
	_ByPerturbation(src._ByPerturbation),_tol(src._tol){};

double NeoHookeanElasticPotential::get(const STensor3& F) const{
	double J = F.determinant();
  double J2over3= pow(J,2./3.);
  static STensor3 C;
  for (int i=0; i<3; i++)
    for (int j=0; j<3; j++) {
      C(i,j) = 0.;
      for (int k=0; k<3; k++){
        C(i,j) += F(k,i)*F(k,j);
      }
    }
  C *= (1./J2over3);

  double val = 0.5*_K*log(J)*log(J) + 0.5*_mu*(C.trace()-3.);
  return val;
};
void NeoHookeanElasticPotential::constitutive(const STensor3& F, STensor3& P, const bool stiff, STensor43* L, const bool dTangent, STensor63* dCalgdeps) const{
	if (_ByPerturbation){

		double potential = get(F);
    static STensor3 Fp;
		for (int i=0; i<3; i++){
			for (int j=0; j<3; j++){
				Fp=(F);
				Fp(i,j) += _tol;
				double potentialPlus = get(Fp);
				P(i,j) = (potentialPlus - potential)/_tol;
			}
		}

		if (stiff){

			static STensor3 Fp2, Pp;
			for (int i=0; i<3; i++){
				for (int j=0; j<3; j++){
					Fp2 = F;
					Fp2(i,j) += _tol;
					constitutive(Fp2,Pp,false,NULL);
					for (int p=0; p<3; p++){
						for (int q=0; q<3; q++){
							(*L)(p,q,i,j) = (Pp(p,q) - P(p,q))/_tol;
						}
					}
				}
			}
		}
		if(dTangent){
                  STensorOperation::zero(*dCalgdeps);
                  static STensor3 Fpp2,Ppp;
                  static STensor43* Lp;
                  for (int i=0; i<3; i++){
                   for (int j=0; j<3; j++){
                    Fpp2 = F;
                    Fpp2(i,j) += _tol;
                    constitutive(Fpp2,Ppp,true,Lp,false,NULL);
      
                   for (int k=0; k<3; k++){
                    for (int l=0; l<3; l++){
                     for (int p=0; p<3; p++){
                      for (int q=0; q<3; q++){
                       (*dCalgdeps)(k,l,p,q,i,j) = ((*Lp)(k,l,p,q)-(*L)(k,l,p,q))/_tol;
                      }
                    }
                   }
                 }
                }
               }
            }     
	}
  else{
    double J = F.determinant();
    double J2over3= pow(J,2./3.);
    double logJ = log(J);
    static STensor3 Finv;
    STensorOperation::inverseSTensor3(F, Finv);

    static STensor3 C;
    for (int i=0; i<3; i++)
      for (int j=0; j<3; j++)
        C(i,j) = (F(0,i)*F(0,j)+F(1,i)*F(1,j)+F(2,i)*F(2,j));

    double trC = C.trace();
    static STensor3 invC;
    STensorOperation::inverseSTensor3(C, invC);

    static const STensor3 I(1.);
    static STensor3 Svol, Sdev;
    for (int i=0; i<3; i++){
      for (int j=0; j<3; j++){
        Svol(i,j) = _K*logJ*invC(i,j);
        Sdev(i,j) = (_mu/J2over3)*(I(i,j) - trC*invC(i,j)/3.);
      }
    }


    for (int i=0; i<3; i++){
      for (int j=0; j<3; j++){
        P(i,j) = F(i,0)*(Svol(0,j)+Sdev(0,j))+ F(i,1)*(Svol(1,j)+Sdev(1,j))+F(i,2)*(Svol(2,j)+Sdev(2,j));
      }
    }

    if (stiff){

      static STensor43 dSvoldC, dSdevdC;
      for (int i=0; i<3; i++){
        for (int j=0; j<3; j++){
          for (int k=0; k<3; k++){
            for (int l=0; l<3; l++){
              dSvoldC(i,j,k,l) = 0.5*_K*invC(i,j)*invC(k,l) - 0.5*_K*log(J)*(invC(i,k)*invC(j,l)+invC(i,l)*invC(j,k));
              dSdevdC(i,j,k,l) = -Sdev(i,j)*invC(k,l)/3. - (_mu/J2over3)*invC(i,j)*I(k,l)/3. +
                               (_mu*trC/(6.*J2over3))*(invC(i,k)*invC(j,l)+invC(i,l)*invC(j,k));

            };
          };
        };
      };

      STensorOperation::zero(*L);
      for (int i=0; i<3; i++){
        for (int j=0; j<3; j++){
          for (int p=0; p<3; p++){
            (*L)(i,j,i,p) += (Svol(p,j)+Sdev(p,j));
            for (int q=0; q<3; q++){
              for (int s=0; s<3; s++){
                for (int k=0; k<3; k++){
                  (*L)(i,j,p,q) += 2.*F(i,k)*(dSvoldC(k,j,q,s)+dSdevdC(k,j,q,s))*F(p,s);
                }
              }
            }
          }
        }
      }
      
      if(dTangent){
      STensorOperation::zero(*dCalgdeps);
      
      static STensor63 dCddF,dSddC; 
      static STensor43 dSdF, dinvCdC; 
      STensorOperation::zero(dSdF);
      STensorOperation::zero(dinvCdC);
      STensorOperation::zero(dSddC);
      static const STensor3 I(1.);
      
      for (int i=0; i<3; i++){
        for (int j=0; j<3; j++){
          for (int k=0; k<3; k++){            
            for (int l=0; l<3; l++){
              dinvCdC(i,j,k,l) = -0.5*(invC(i,k)*invC(j,l)+invC(i,l)*invC(j,k));
              for (int r=0; r<3; r++){
                dSdF(i,j,k,l) += 2.0*(dSvoldC(i,j,l,r)+dSdevdC(i,j,l,r))*F(k,r);
                for (int s=0; s<3; s++){
                  dCddF(i,j,k,l,r,s) = I(k,r)*(I(i,l)*I(j,s)+I(i,s)*I(j,l));                  
                }
              }
            }
          }
        }
      }                           
      
      for (int i=0; i<3; i++){
        for (int j=0; j<3; j++){
          for (int k=0; k<3; k++){
            for (int l=0; l<3; l++){
              for (int p=0; p<3; p++){
                for (int q=0; q<3; q++){
                   dSddC(i,j,k,l,p,q) = 0.5*_K*(dinvCdC(i,j,p,q)*invC(k,l)+invC(i,j)*dinvCdC(k,l,p,q)) 
                                          - 0.25*_K*invC(p,q)*(invC(i,k)*invC(j,l)+invC(i,l)*invC(j,k))
                                          - 0.5*_K*log(J)*(dinvCdC(i,k,p,q)*invC(j,l)+invC(i,k)*dinvCdC(j,l,p,q)+dinvCdC(i,l,p,q)*invC(j,k)
                                          +invC(i,l)*dinvCdC(j,k,p,q));
                 
                   dSddC(i,j,k,l,p,q) += -1.0/3.0*(dSdevdC(i,j,p,q)*invC(k,l)+ Sdev(i,j)*dinvCdC(k,l,p,q))
                                         +1.0/9.0*_mu/J2over3*invC(p,q)*(I(k,l)*invC(i,j)+trC*dinvCdC(i,j,k,l))
                                         -1.0/3.0*_mu/J2over3*(I(k,l)*dinvCdC(i,j,p,q)+I(p,q)*dinvCdC(i,j,k,l)- trC*0.5*(dinvCdC(i,k,p,q)*invC(j,l)
                                         +invC(i,k)*dinvCdC(j,l,p,q)+dinvCdC(i,l,p,q)*invC(j,k)+invC(i,l)*dinvCdC(j,k,p,q)));                   
                 }                                         
               }
             }
           }
         }
       }            
      
      
      for (int i=0; i<3; i++){
        for (int j=0; j<3; j++){
          for (int k=0; k<3; k++){
            for (int l=0; l<3; l++){
              for (int p=0; p<3; p++){
                for (int q=0; q<3; q++){
                    (*dCalgdeps)(i,j,k,l,p,q) = I(i,k)*dSdF(l,j,p,q)+I(i,p)*dSdF(q,j,k,l);
                    for(int r=0; r<3; r++){ 
                      for(int s=0; s<3; s++){
                         for(int t=0; t<3; t++){
                            (*dCalgdeps)(i,j,k,l,p,q) +=  F(i,r)*(dSvoldC(r,j,s,t)+dSdevdC(r,j,s,t))*dCddF(s,t,k,l,p,q);
                            for(int m=0; m<3; m++){
                                for(int n=0; n<3; n++){
                                  (*dCalgdeps)(i,j,k,l,p,q) += F(i,r)*dSddC(r,j,s,t,m,n)*(I(s,l)*F(k,t)+F(k,s)*I(t,l))*(I(m,q)*F(p,n)+F(p,m)*I(n,q));
                                }
                            }
                         }
                      }
                   }
                }
              }
            }
          }
        }
       }  
      }    
    }
  }
};
void NeoHookeanElasticPotential::getLocalValForNonLocalEquation(const STensor3& F, double& val, const bool stiff, STensor3* DvalDF) const{
	double Y = get(F);
	double E = getYoungModulus();

	val = sqrt(2*Y/E);
	if (stiff){
		STensorOperation::zero(*DvalDF);
		if (_ByPerturbation){
      static STensor3 Fp;
			for (int i=0; i<3; i++){
				for (int j=0; j<3; j++){
					Fp=(F);
					Fp(i,j) += _tol;
					double valp;
					getLocalValForNonLocalEquation(Fp,valp,false,NULL);
					(*DvalDF)(i,j) = (valp-val)/_tol;
				}
			}
		}
		else{
			this->constitutive(F,*DvalDF,false,NULL);
			if (val > 1e-10){
				(*DvalDF) *= (1./(E*val));
			}
			else{
				(*DvalDF) *= (1./(E*1e-10));
			}

		}
	}
};


// for anisotropic
double NeoHookeanElasticPotential::getPositiveValue(const STensor3& F) const {
  double J = F.determinant();
  double J2over3= pow(J,2./3.);
  static STensor3 C;
  for (int i=0; i<3; i++)
    for (int j=0; j<3; j++)
      C(i,j) = (F(0,i)*F(0,j)+F(1,i)*F(1,j)+F(2,i)*F(2,j))/J2over3;

  double val = 0.;
  if (J >= 1.){
     val = 0.5*_K*log(J)*log(J) + 0.5*_mu*(C.trace()-3.);
  }
  else{
    val  = 0.5*_mu*(C.trace()-3.);
  }
  return val;
};
double NeoHookeanElasticPotential::getNegativeValue(const STensor3& F) const {
  double J = F.determinant();
  double val = 0.;
  if (J <1.){
    val =  0.5*_K*log(J)*log(J);
  }
  return val;
};
void NeoHookeanElasticPotential::constitutivePositive(const STensor3& F, STensor3& P, const bool stiff, STensor43* L) const {
  double J = F.determinant();
  double J2over3= pow(J,2./3.);
  double logJ = log(J);
  static STensor3 C;
  for (int i=0; i<3; i++)
    for (int j=0; j<3; j++)
      C(i,j) = (F(0,i)*F(0,j)+F(1,i)*F(1,j)+F(2,i)*F(2,j));

  double trC = C.trace();
  static STensor3 invC;
  STensorOperation::inverseSTensor3(C, invC);

  static const STensor3 I(1.);
  static STensor3 Spos, Sdev;

  for (int i=0; i<3; i++){
    for (int j=0; j<3; j++){
      Sdev(i,j) = (_mu/J2over3)*(I(i,j) - trC*invC(i,j)/3.);
      Spos(i,j) = Sdev(i,j);
      if (J>=1.){
        Spos(i,j) += _K*logJ*invC(i,j);
      }
    }
  }


  for (int i=0; i<3; i++){
    for (int j=0; j<3; j++){
      P(i,j) = F(i,0)*Spos(0,j)+ F(i,1)*Spos(1,j)+F(i,2)*Spos(2,j);
    }
  }

  if (stiff){
    static STensor43 dSposdC;
    for (int i=0; i<3; i++){
      for (int j=0; j<3; j++){
        for (int k=0; k<3; k++){
          for (int l=0; l<3; l++){
            dSposdC(i,j,k,l) = -Sdev(i,j)*invC(k,l)/3. - (_mu/J2over3)*invC(i,j)*I(k,l)/3. +
                               (_mu*trC/(6.*J2over3))*(invC(i,k)*invC(j,l)+invC(i,l)*invC(j,k));
            if (J >=1.){
              dSposdC(i,j,k,l) += 0.5*_K*invC(i,j)*invC(k,l) - 0.5*_K*log(J)*(invC(i,k)*invC(j,l)+invC(i,l)*invC(j,k));
            }


          };
        };
      };
    };

    STensorOperation::zero(*L);
    for (int i=0; i<3; i++){
      for (int j=0; j<3; j++){
        for (int p=0; p<3; p++){
          (*L)(i,j,i,p) += Spos(p,j);
          for (int q=0; q<3; q++){
            for (int s=0; s<3; s++){
              for (int k=0; k<3; k++){
                (*L)(i,j,p,q) += 2.*F(i,k)*dSposdC(k,j,q,s)*F(p,s);
              }
            }
          }
        }
      }
    }
  }
};
void NeoHookeanElasticPotential::constitutiveNegative(const STensor3& F, STensor3& P, const bool stiff, STensor43* L) const {
  double J = F.determinant();
  if (J < 1.){
    double logJ = log(J);
    static STensor3 C;

    for (int i=0; i<3; i++)
      for (int j=0; j<3; j++)
        C(i,j) = (F(0,i)*F(0,j)+F(1,i)*F(1,j)+F(2,i)*F(2,j));

    static STensor3 invC;
    STensorOperation::inverseSTensor3(C, invC);
    static STensor3 Svol;
    Svol=(invC);
    Svol*= (_K*logJ);


    for (int i=0; i<3; i++){
      for (int j=0; j<3; j++){
        P(i,j) = F(i,0)*Svol(0,j)+ F(i,1)*Svol(1,j)+F(i,2)*Svol(2,j);
      }
    }

    if (stiff){
      static STensor43 dSvoldC;

      for (int i=0; i<3; i++){
        for (int j=0; j<3; j++){
          for (int k=0; k<3; k++){
            for (int l=0; l<3; l++){
              dSvoldC(i,j,k,l) = 0.5*_K*invC(i,j)*invC(k,l)- 0.5*_K*log(J)*(invC(i,k)*invC(j,l)+invC(i,l)*invC(j,k));
            };
          };
        };
      };

      STensorOperation::zero(*L);
      for (int i=0; i<3; i++){
        for (int j=0; j<3; j++){
          for (int p=0; p<3; p++){
            (*L)(i,j,i,p) += (Svol(p,j));
            for (int q=0; q<3; q++){
              for (int s=0; s<3; s++){
                for (int k=0; k<3; k++){
                  (*L)(i,j,p,q) += 2.*F(i,k)*dSvoldC(k,j,q,s)*F(p,s);
                }
              }
            }
          }
        }
      }
		}
  }
  else{
    STensorOperation::zero(P);
    if (stiff){
      STensorOperation::zero(*L);
    }
  }
};
void NeoHookeanElasticPotential::getLocalPositiveValForNonLocalEquation(const STensor3& F, double& val, const bool stiff, STensor3* DvalDF) const {
  double Y = getPositiveValue(F);
	double E = getYoungModulus();

	val = sqrt(2*Y/E);
	if (stiff){
		STensorOperation::zero(*DvalDF);
		if (_ByPerturbation){
      static STensor3 Fp;
			for (int i=0; i<3; i++){
				for (int j=0; j<3; j++){
					Fp=(F);
					Fp(i,j) += _tol;
					double valp;
					getLocalPositiveValForNonLocalEquation(Fp,valp,false,NULL);
					(*DvalDF)(i,j) = (valp-val)/_tol;
				}
			}
		}
		else{
			this->constitutivePositive(F,*DvalDF,false,NULL);
			if (val > 1e-10){
				(*DvalDF) *= (1./(E*val));
			}
			else{
				(*DvalDF) *= (1./(E*1e-10));
			}

		}
	}
};


CompressiveNeoHookeanPotential::CompressiveNeoHookeanPotential(const int num, const double E, const double nu) :
	elasticPotential(num), _E(E), _nu(nu){
  double K = _E/(3.*(1.-2.*_nu));
  _mu = _E/(2.*(1.+_nu));
  _lambda = K- 2.*_mu/3.;
  static const STensor3 I(1.);
  STensorOperation::zero(_Cel);
  for (int i=0; i<3; i++){
    for (int j=0; j<3; j++){
      for (int k=0; k<3; k++){
        for (int l=0; l<3; l++){
          _Cel(i,j,k,l) = _lambda*I(i,j)*I(k,l)+_mu*(I(i,k)*I(j,l)+I(i,l)*I(j,k));
        }
      }
    }
  }
};

CompressiveNeoHookeanPotential::CompressiveNeoHookeanPotential(const CompressiveNeoHookeanPotential& src) : elasticPotential(src),
	_E(src._E),_nu(src._nu),_lambda(src._lambda),_mu(src._mu), _Cel(src._Cel){};

double CompressiveNeoHookeanPotential::get(const STensor3& F) const{
	double J = F.determinant();
  static STensor3 C, invC;
  STensorOperation::multSTensor3FirstTranspose(F,F,C);
  STensorOperation::inverseSTensor3(C,invC);
  double traceC = C.trace();
  return 0.5*_lambda*log(J)*log(J) - _mu*log(J) + 0.5*_mu*(traceC-3);
};
void CompressiveNeoHookeanPotential::constitutive(const STensor3& F, STensor3& P, const bool stiff, STensor43* DPDF, const bool dTangent, STensor63* dCalgdeps) const{
  double J = F.determinant();
  double logJ = log(J);
  static STensor3 C, invC, S;
  STensorOperation::multSTensor3FirstTranspose(F,F,C);
  static STensor43 DinvCDC;
  if (stiff)
  {
    STensorOperation::inverseSTensor3(C,invC,&DinvCDC,true);
  }
  else
  {
    STensorOperation::inverseSTensor3(C,invC);
  }
  
  //S=mu*(I-invC)+lambda*(logJ)*invC
  STensorOperation::diag(S,_mu);
  STensorOperation::axpy(S,invC,-_mu+_lambda*logJ);
  STensorOperation::multSTensor3(F, S, P);
  if (stiff)
  {
    static STensor43 DSDC;
    DSDC = DinvCDC;
    DSDC *= (_lambda*logJ-_mu);
    STensorOperation::prodAdd(invC,invC,0.5*_lambda,DSDC);
    STensorOperation::zero(*DPDF);
    for (int i = 0; i < 3; i++) {
    for (int J = 0; J < 3; J++) {
      for (int k = 0; k < 3; k++) {
          for (int L = 0; L < 3; L++) {
            for (int K = 0; K < 3; K++) {
              for (int I = 0; I < 3; I++) {
                (*DPDF)(i, J, k, L) += 2.*DSDC(I, J, K, L) * F(i,I) * F(k,K);
              }
            }
            if (i == k) {
              (*DPDF)(i, J, k, L) += S(J,L);
            }
          }
        }
      }
    }
  }  
  if (dTangent)
  {
    Msg::Error("compute dCalgdeps is not implemented in CompressiveNeoHookeanPotential");
  }
};
void CompressiveNeoHookeanPotential::getLocalValForNonLocalEquation(const STensor3& F, double& val, const bool stiff, STensor3* DvalDF) const{
	double Y = get(F);
	double E = getYoungModulus();
	val = sqrt(2*Y/E);
	if (stiff){
		STensorOperation::zero(*DvalDF);
    this->constitutive(F,*DvalDF,false,NULL);
    if (val > 1e-10){
      (*DvalDF) *= (1./(E*val));
    }
    else{
      (*DvalDF) *= (1./(E*1e-10));
    }
	}
};


// for anisotropic
double CompressiveNeoHookeanPotential::getPositiveValue(const STensor3& F) const {
  return get(F);
};
double CompressiveNeoHookeanPotential::getNegativeValue(const STensor3& F) const {
  return 0;
};
void CompressiveNeoHookeanPotential::constitutivePositive(const STensor3& F, STensor3& P, const bool stiff, STensor43* L) const {
  constitutive(F,P,stiff,L);
};
void CompressiveNeoHookeanPotential::constitutiveNegative(const STensor3& F, STensor3& P, const bool stiff, STensor43* L) const {
  STensorOperation::zero(P);
  if (stiff){
    STensorOperation::zero(*L);
  }
};
void CompressiveNeoHookeanPotential::getLocalPositiveValForNonLocalEquation(const STensor3& F, double& val, const bool stiff, STensor3* DvalDF) const {
  double Y = getPositiveValue(F);
	double E = getYoungModulus();
	val = sqrt(2*Y/E);
	if (stiff){
		STensorOperation::zero(*DvalDF);
    this->constitutivePositive(F,*DvalDF,false,NULL);
    if (val > 1e-10){
      (*DvalDF) *= (1./(E*val));
    }
    else{
      (*DvalDF) *= (1./(E*1e-10));
    }

	}
};


HyperElasticPolynomialPotential::HyperElasticPolynomialPotential(const int num, const double E, const double nu, const bool pert, const double tol):
  elasticPotential(num), _E(E), _nu(nu), _ByPerturbation(pert), _tol(tol)
{
  _K = _E/(3.0*(1.0-2.0*_nu));
  _mu = _E/(2.0*(1.0+_nu));

  // Dummy elastic tensor.
  // Should be implemented with the proper one.
  for(int i = 0; i < 3; i++)
    for(int j = 0; j < 3; j++)
      for(int k = 0; k < 3; k++)
        for(int l = 0; l < 0; l++)
          _Cel(i, j, k, l) = 0.0;
}

HyperElasticPolynomialPotential::HyperElasticPolynomialPotential(const HyperElasticPolynomialPotential &src):
  elasticPotential(src),
  _E(src._E), _nu(src._nu), _K(src._K), _mu(src._mu), _A(src._A), _D(src._D), _Cel(src._Cel),
  _ByPerturbation(src._ByPerturbation), _tol(src._tol)
{}



void HyperElasticPolynomialPotential::setSizeDistortionalParameters(int m, int n)
{
  _A.resize(m);
  for(int i = 0; i < m; i++)
    _A[i].resize(n);
}

void HyperElasticPolynomialPotential::setDistortionalParameter(int i, int j, double val)
{
  _A[i][j] = val;
}

void HyperElasticPolynomialPotential::setSizeVolumetricParameters(int n)
{
  _D.resize(n);
}

void HyperElasticPolynomialPotential::setVolumetricParameter(int i, double val)
{
  _D[i] = val;
}

double HyperElasticPolynomialPotential::get(const STensor3 &F) const
{
  const double J = F.determinant();
  const double J2over3 = pow(J, 2.0/3.0);
  const double J4over3 = pow(J, 4.0/3.0);
  STensor3 B; // transpose of C
  double I1, I2, Ibar1, Ibar2, ret = 0.0;
  double logJ = log(J);

  computeB(F, B);
  computeI1(B, I1);
  computeI2(B, I1, I2);

  Ibar1 = I1/J2over3;
  Ibar2 = I2/J4over3;

  // compute strain density energy
  for(typename std::vector<std::vector<double>>::size_type i = 0; i < _A.size(); ++i)
    for(typename std::vector<double>::size_type j = 0; j < _A[i].size(); ++j)
      ret += _A[i][j]*pow(Ibar1-3, i)*pow(Ibar2-3, j);

  for(typename std::vector<double>::size_type k = 1;  k <= _D.size(); ++k)
    ret += _D[k-1]*pow(J-1, 2*k);  

  return ret;
}

void HyperElasticPolynomialPotential::constitutive(const STensor3 &F, STensor3 &P, const bool stiff, STensor43 *L, const bool dTangent, STensor63* dCalgdeps) const
{
  if(_ByPerturbation)
  {
    double potential = get(F);
    static STensor3 Fp;

    for(int i = 0; i < 3; i++)
      for(int j = 0; j < 3; j++)
      {
        Fp = (F);
        Fp(i, j) += _tol;
        double potentialPlus = get(Fp);
        P(i, j) = (potentialPlus - potential) / _tol;
      }

    if(stiff)
    {
      static STensor3 Fp2, Pp;
      for(int i = 0; i < 3; i++)
        for(int j = 0; j < 3; j++)
        {
          Fp2 = F;
          Fp2(i, j) += _tol;
          constitutive(Fp2, Pp, false, NULL);

          for(int p = 0; p < 3; p++)
            for(int q = 0; q < 3; q++)
                (*L)(p, q, i, j) = (Pp(p, q) - P(p, q)) / _tol;
        }
    }
    
    if(dTangent){
      STensorOperation::zero(*dCalgdeps);
      static STensor3 Fpp2,Ppp;
      static STensor43* Lp;
      for (int i=0; i<3; i++){
        for (int j=0; j<3; j++){
          Fpp2 = F;
          Fpp2(i,j) += _tol;
          constitutive(Fpp2,Ppp,true,Lp,false,NULL);
      
        for (int k=0; k<3; k++){
         for (int l=0; l<3; l++){
          for (int p=0; p<3; p++){
           for (int q=0; q<3; q++){
               (*dCalgdeps)(k,l,p,q,i,j) = ((*Lp)(k,l,p,q)-(*L)(k,l,p,q))/_tol;
            }
           }
          }
         }
        }
     }
    }     
  }
  else
  {
          
  const double J = F.determinant();
  const double J2over3 = pow(J, 2.0/3.0);
  const double J4over3 = pow(J, 4.0/3.0);
  double I1, I2, Ibar1, Ibar2, dPsidIbar1, dPsidIbar2, dPsidJ;
  double d2PsidIbar1, d2PsidIbar2, d2PsidJ;
  double d3PsidIbar1, d3PsidIbar2, d3PsidJ;
  STensor3 B, Finv, dJdF, dIbar1dF, dIbar2dF; 

  STensorOperation::inverseSTensor3(F, Finv);
  computeB(F, B);
  computeI1(B, I1);
  computeI2(B, I1, I2);

  Ibar1 = I1/J2over3;
  Ibar2 = I2/J4over3;

  computedJdF(J, Finv, dJdF);
  computedIbar1dF(J2over3, I1, F, Finv, dIbar1dF);
  computedIbar2dF(J4over3, I1, I2, F, Finv, B, dIbar2dF);
  computedPsidIbar1(Ibar1, Ibar2, dPsidIbar1, d2PsidIbar1,d3PsidIbar1);
  computedPsidIbar2(Ibar1, Ibar2, dPsidIbar2, d2PsidIbar2, d3PsidIbar2);
  computedPsidJ(J, dPsidJ,d2PsidJ,d3PsidJ);
  
  

  // Compute P.
  for(int i = 0; i < 3; i++)
    for(int j = 0; j < 3; j++)
    {
      P(i, j) = dPsidIbar1*dIbar1dF(i, j) + dPsidIbar2*dIbar2dF(i, j) + dPsidJ*dJdF(i, j); 
    }

  // Compute tangent.
  if(stiff)
  {
    STensor43 d2JdFdF, d2Ibar1dFdF, d2Ibar2dFdF;
    STensor3 d2PsidFdIbar1, d2PsidFdIbar2, d2PsidFdJ;

    computed2JdFdF(J, Finv, d2JdFdF);
    computed2Ibar1dFdF(J2over3, Ibar1, F, Finv, dIbar1dF, d2Ibar1dFdF);
    computed2Ibar2dFdF(J4over3, I1, I2, F, Finv, B, dIbar2dF, d2Ibar2dFdF);
    computed2Psi(J, Ibar1, Ibar2, dJdF, dIbar1dF, dIbar2dF, d2PsidFdIbar1, d2PsidFdIbar2, d2PsidFdJ);

    STensorOperation::zero(*L);
    for(int i = 0; i < 3; i++)
      for(int j = 0; j < 3; j++)
        for(int k = 0; k < 3; k++)
          for(int l = 0; l < 3; l++)
            (*L)(i, j, k, l) = d2PsidFdIbar1(k, l)*dIbar1dF(i, j)
                             + dPsidIbar1*d2Ibar1dFdF(k, l, i, j)
                             + d2PsidFdIbar2(k, l)*dIbar2dF(i, j)
                             + dPsidIbar2*d2Ibar2dFdF(k, l, i, j)
                             + d2PsidFdJ(k, l)*dJdF(i, j)
                             + dPsidJ*d2JdFdF(k, l, i, j);                                     
                             
    if(dTangent){
      STensorOperation::zero(*dCalgdeps);  
      static STensor63 d3JdF3;
      static STensor63 d3Ibar1dF3; 
      static STensor63 d3Ibar2dF3;
      
      computed3JdF3(J, Finv, d3JdF3);
      computed3Ibar1dF3(J, Ibar1, F, Finv, dIbar1dF, d2Ibar1dFdF, d3Ibar1dF3);
      computed3Ibar2dF3(J, Ibar1, Ibar2, F, Finv, B, dIbar1dF, d2Ibar1dFdF, dIbar2dF, d2Ibar2dFdF, d3Ibar2dF3); 
      for(int i = 0; i < 3; i++)
       for(int j = 0; j < 3; j++)
        for(int k = 0; k < 3; k++)
         for(int l = 0; l < 3; l++)
          for(int p = 0; p < 3; p++)
           for(int q = 0; q < 3; q++)     
            (*dCalgdeps)(i,j,k,l,p,q) = d3PsidIbar1*dIbar1dF(i,j)*dIbar1dF(k,l)*dIbar1dF(p,q) + d2PsidIbar1*(d2Ibar1dFdF(i,j,p,q)*dIbar1dF(k,l)+d2Ibar1dFdF(k,l,p,q)*dIbar1dF(i,j)
                                              + d2Ibar1dFdF(i,j,k,l)*dIbar1dF(p,q)) + dPsidIbar1*d3Ibar1dF3(i,j,k,l,p,q)
                                       + d3PsidIbar2*dIbar2dF(i,j)*dIbar2dF(k,l)*dIbar2dF(p,q) + d2PsidIbar2*(d2Ibar2dFdF(i,j,p,q)*dIbar2dF(k,l)+d2Ibar2dFdF(k,l,p,q)*dIbar2dF(i,j)
                                              + d2Ibar2dFdF(i,j,k,l)*dIbar2dF(p,q)) + dPsidIbar2*d3Ibar2dF3(i,j,k,l,p,q)        
                                       + d3PsidJ*dJdF(i,j)*dJdF(k,l)*dJdF(p,q) + d2PsidJ*(d2JdFdF(i,j,p,q)*dJdF(k,l)+d2JdFdF(k,l,p,q)*dJdF(i,j)
                                              + d2JdFdF(i,j,k,l)*dJdF(p,q)) + dPsidJ*d3JdF3(i,j,k,l,p,q);                       
   }                          
  }
 }
}

void HyperElasticPolynomialPotential::computeB(const STensor3 &F, STensor3 &B) const
{
  for(int i = 0; i < 3; i++)
    for(int j = 0; j < 3; j++)
    {
      B(i, j) = 0.0;
      for(int k = 0; k < 3; k++)
        B(i, j) += F(i, k)*F(j, k);
    }
}

void HyperElasticPolynomialPotential::computeI1(const STensor3 &B, double &I1) const
{
  I1 = B.trace();
}

void HyperElasticPolynomialPotential::computeI2(const STensor3 &B, const double &I1, double &I2) const
{
  I2 = 0.5*I1*I1;
  for(int i = 0; i < 3; i++)
    for(int k = 0; k < 3; k++)
      I2 -= 0.5*B(i, k)*B(k, i);
}

void HyperElasticPolynomialPotential::computedJdF(const double J, const STensor3 &Finv, STensor3 &dJdF) const
{
  for(int i = 0; i < 3; i++)
    for(int j = 0; j < 3; j++)
      dJdF(i, j) = J*Finv(j, i);
}


void HyperElasticPolynomialPotential::computedIbar1dF(const double J2over3, const double I1, const STensor3 &F, const STensor3 &Finv, STensor3 &dIbar1dF) const
{
  for(int i = 0; i < 3; i++)
    for(int j = 0; j < 3; j++)
      dIbar1dF(i, j) = 2.0/J2over3 * (F(i, j) - I1/3.0*Finv(j, i));
}

void HyperElasticPolynomialPotential::computedIbar2dF(const double J4over3, const double I1, const double I2, const STensor3 &F, const STensor3 &Finv, const STensor3 &B, STensor3 &dIbar2dF) const
{
  for(int i = 0; i < 3; i++)
    for(int j = 0; j < 3; j++)
    {
      double BikFkj = 0.0;
      for(int k = 0; k < 3; k++)
        BikFkj += B(i, k)*F(k, j);

      dIbar2dF(i, j) = 2.0/J4over3 * (I1*F(i, j)
                     - BikFkj - 2.0/3.0*I2*Finv(j, i));
    }
}

void HyperElasticPolynomialPotential::computedPsidIbar1(const double Ibar1, const double Ibar2, double &dPsidIbar1, double &d2PsidIbar1, double &d3PsidIbar1) const
{
  dPsidIbar1 = 0.0;
  d2PsidIbar1 = 0.0;
  d2PsidIbar1 = 0.0;
  for(typename std::vector<std::vector<double> >::size_type i = 1; i < _A.size(); i++)
  {
    for(typename std::vector<double>::size_type j = 0; j < _A[i].size(); j++)
    {
      dPsidIbar1 += _A[i][j]*i*pow(Ibar1-3, i-1)*pow(Ibar2-3, j);
    }
  }
  
  for(typename std::vector<std::vector<double> >::size_type i = 2; i < _A.size(); i++)
  {
    for(typename std::vector<double>::size_type j = 0; j < _A[i].size(); j++)
    {
      d2PsidIbar1 += _A[i][j]*i*(i-1)*pow(Ibar1-3, i-2)*pow(Ibar2-3, j);
    }
  }
  
  for(typename std::vector<std::vector<double> >::size_type i = 3; i < _A.size(); i++)
  {
    for(typename std::vector<double>::size_type j = 0; j < _A[i].size(); j++)
    {
      d3PsidIbar1 += _A[i][j]*i*(i-1)*(i-2)*pow(Ibar1-3, i-3)*pow(Ibar2-3, j);
    }
  }
}

void HyperElasticPolynomialPotential::computedPsidIbar2(const double Ibar1, const double Ibar2, double &dPsidIbar2, double &d2PsidIbar2, double &d3PsidIbar2) const
{
  dPsidIbar2 = 0.0;
  d2PsidIbar2 = 0.0;
  d3PsidIbar2 = 0.0;
  for(typename std::vector<std::vector<double> >::size_type i = 0; i < _A.size(); i++)
  {
    for(typename std::vector<double>::size_type j = 1; j < _A[i].size(); j++)
    {
      dPsidIbar2 += _A[i][j]*j*pow(Ibar1-3, i)*pow(Ibar2-3, j-1);
    }
    
    for(typename std::vector<double>::size_type j = 2; j < _A[i].size(); j++)
    {
      d2PsidIbar2 += _A[i][j]*j*(j-1)*pow(Ibar1-3, i)*pow(Ibar2-3, j-2);
    }
    
    for(typename std::vector<double>::size_type j = 3; j < _A[i].size(); j++)
    {
      d3PsidIbar2 += _A[i][j]*j*(j-1)*(j-2)*pow(Ibar1-3, i)*pow(Ibar2-3, j-3);
    }
  }
}

void HyperElasticPolynomialPotential::computedPsidJ(const double J, double &dPsidJ, double &d2PsidJ, double &d3PsidJ) const
{
  dPsidJ = 0.0;
  d2PsidJ = 0.0;
  d3PsidJ = 0.0;
  for(typename std::vector<double>::size_type k = 1; k <= _D.size(); k++)
  {
    dPsidJ += _D[k-1]*2*k*pow(J-1, 2*k-1);
    d2PsidJ += _D[k-1]*2*k*(2*k-1)*pow(J-1, 2*k-2);
  }
   for(typename std::vector<double>::size_type k = 2; k <= _D.size(); k++)
  {
    d3PsidJ += _D[k-1]*2*k*(2*k-1)*2*(k-1)*pow(J-1, 2*k-3);
  }
}

void HyperElasticPolynomialPotential::computed2JdFdF(const double J, const STensor3 &Finv, STensor43 &d2J) const
{
  for(int i = 0; i < 3; i++)
    for(int j = 0; j < 3; j++)
      for(int k = 0; k < 3; k++)
        for(int l = 0; l < 3; l++)
          d2J(i, j, k, l) = J*(Finv(j, i)*Finv(l, k) - Finv(l, i)*Finv(j, k));
}

void HyperElasticPolynomialPotential::computed3JdF3(const double J, const STensor3 &Finv, STensor63 &d3J) const
{          
  for(int i = 0; i < 3; i++)
    for(int j = 0; j < 3; j++)
      for(int k = 0; k < 3; k++)
        for(int l = 0; l < 3; l++)
          for(int p = 0; p < 3; p++)
            for(int q = 0; q < 3; q++)
              d3J(i,j,k,l,p,q) =J*(Finv(q,p)*Finv(j, i)*Finv(l, k) - Finv(q,p)*Finv(l, i)*Finv(j, k)
                                  -Finv(q, i)*Finv(j, p)*Finv(l, k)- Finv(j, i)*Finv(q, k)*Finv(l, p)
                                  +Finv(q, i)*Finv(l, p)*Finv(j, k)+ Finv(l, i)*Finv(q, k)*Finv(j, p));      
}

void HyperElasticPolynomialPotential::computed2Ibar1dFdF(const double J2over3, const double Ibar1, const STensor3 &F, const STensor3 &Finv, const STensor3 &dIbar1dF, STensor43 &d2Ibar1) const
{
  STensor3 I(1.0);
  for(int i = 0; i < 3; i++)
    for(int j = 0; j < 3; j++)
      for(int k = 0; k < 3; k++)
        for(int l = 0; l < 3; l++)
          d2Ibar1(i, j, k, l) = -4.0/(3.0*J2over3)*Finv(j, i)*F(k, l)
                              + 2.0/J2over3*I(i, k)*I(j, l)
                              - 2.0/3.0*dIbar1dF(i, j)*Finv(l, k)
                              + 2.0/3.0*Ibar1*Finv(l, i)*Finv(j, k);
}


void HyperElasticPolynomialPotential::computed3Ibar1dF3(const double J2over3, const double Ibar1, const STensor3 &F, const STensor3 &Finv, const STensor3 &dIbar1dF, const STensor43 &d2Ibar1, STensor63 &d3Ibar1) const
{
  STensor3 I(1.0);
  for(int i = 0; i < 3; i++)
   for(int j = 0; j < 3; j++)
    for(int k = 0; k < 3; k++)
     for(int l = 0; l < 3; l++)
      for(int p = 0; p < 3; p++)
       for(int q = 0; q < 3; q++)
        d3Ibar1(i, j, k, l, p, q) = 8.0/(9.0*J2over3)*Finv(q,p)*F(i, j)*Finv(l, k)- 4.0/(3.0*J2over3)*I(i,p)*I(j,q)*Finv(l, k)+ 4.0/(3.0*J2over3)*F(i, j)*Finv(l, p)*Finv(q,k)
                                   -4.0/(3.0*J2over3)*Finv(q,p)*I(i,k)*I(j,l)- 2.0/3.0*d2Ibar1(k,l,p,q)*Finv(j,i) + 2.0/3.0*dIbar1dF(k, l)*Finv(q, i)*Finv(j,p) 
                                   + 2.0/3.0*dIbar1dF(p, q)*Finv(j, k)*Finv(l, i) - 2.0/3.0*Ibar1*(Finv(j, p)*Finv(q, k)*Finv(l, i) + Finv(j, k)*Finv(q, i)*Finv(l, p));         

}


void HyperElasticPolynomialPotential::computed2Ibar2dFdF(const double J4over3, const double I1, const double I2, const STensor3 &F, const STensor3 &Finv, const STensor3 &B, const STensor3 &dIbar2dF, STensor43 &d2Ibar2) const
{
  STensor3 I(1.0), FmjFml, BimFmj;

  for(int i = 0; i < 3; i++)
    for(int j = 0; j < 3; j++)
    {
      BimFmj(i, j) = 0.0;
      FmjFml(i, j) = 0.0;

      for(int m = 0; m < 3; m++)
      {
        BimFmj(i, j) += B(i, m)*F(m, j);
        FmjFml(i, j) += F(m, i)*F(m, j);
      }
    }

  for(int i = 0; i < 3; i++)
    for(int j = 0; j < 3; j++)
      for(int k = 0; k < 3; k++)
        for(int l = 0; l < 3; l++)
          d2Ibar2(i, j, k, l) =
            - 4.0/3.0*Finv(j, i)*dIbar2dF(k, l)
            + 4.0/(3.0*J4over3)*(
                3.0*F(i, j)*F(k, l) + 3.0/2.0*I1*I(i, k)*I(j, l)
              - 3.0/2.0*I(i, k)*FmjFml(j, l) - 3.0/2.0*F(i, l)*F(k, j)
              - 3.0/2.0*I(j, l)*B(k, i) - 2.0*I1*F(i, j)*Finv(l, k)
              + 2.0*BimFmj(i, j)*Finv(l, k) + I2*Finv(l, i)*Finv(j, k));
}

void HyperElasticPolynomialPotential::computed3Ibar2dF3(const double J, const double Ibar1, const double Ibar2, const STensor3 &F, const STensor3 &Finv, const STensor3 &B, const STensor3 &dIbar1dF, const STensor43 &d2Ibar1, const STensor3 &dIbar2dF, const STensor43 &d2Ibar2, STensor63 &d3Ibar2) const
{
  STensor3 I(1.0), FmjFml, BimFmj;
  const double J2over3 = pow(J, 2.0/3.0);
  const double J4over3 = pow(J, 4.0/3.0);

  for(int i = 0; i < 3; i++)
    for(int j = 0; j < 3; j++)
    {
      BimFmj(i, j) = 0.0;
      FmjFml(i, j) = 0.0;

      for(int m = 0; m < 3; m++)
      {
        BimFmj(i, j) += B(i, m)*F(m, j);
        FmjFml(i, j) += F(m, i)*F(m, j);
      }
    }

  for(int i = 0; i < 3; i++)
   for(int j = 0; j < 3; j++)
    for(int k = 0; k < 3; k++)
     for(int l = 0; l < 3; l++)
      for(int p = 0; p < 3; p++)
       for(int q = 0; q < 3; q++)
        d3Ibar2(i,j,k,l,p,q) = -4.0/3.0*(d2Ibar2(k,l,p,q)*Finv(j,i)- dIbar2dF(k,l)*Finv(q,i)*Finv(j,p)- dIbar2dF(p,q)*Finv(l,i)*Finv(j,k)
                                        + Ibar2*(Finv(q,i)*Finv(l,p)*Finv(j,k)+Finv(l,i)*Finv(j,p)*Finv(q,k)))
                               +4.0/(3.0*J2over3)*(2.0/3.0*Finv(q,p)*Finv(l,k)*Ibar1*F(i,j)+ Finv(l,p)*Finv(q,k)*Ibar1*F(i,j)- Finv(l,k)*dIbar1dF(p,q)*F(i,j) 
                                       - Finv(l,k)*Ibar1*I(i,p)*I(j,q)- Finv(q,p)*(dIbar1dF(k,l)*F(i,j)+Ibar1*I(i,k)*I(j,l)))
                               +2.0/J2over3*(d2Ibar1(k,l,p,q)*F(i,j)+ dIbar1dF(k,l)*I(i,p)*I(j,q)+ dIbar1dF(p,q)*I(i,k)*I(j,l)) 
                               +8.0/(3.0*J4over3)*(4.0/3.0*Finv(q,p)*Finv(l,k)*BimFmj(i,j)- Finv(l,p)*Finv(q,k)*BimFmj(i,j)- Finv(l,k)*(I(i,k)*FmjFml(l,j)+F(i,l)*F(k,j))
                                       +Finv(l,k)*B(i,p)*I(j,q) + Finv(q,p)*(I(i,k)*FmjFml(l,j)+F(i,l)*F(k,j)+B(i,k)*I(j,l)))       
                               -2.0/J4over3*(I(i,k)*I(l,q)*F(p,j)+ I(i,k)*F(p,l)*I(j,q)+ I(i,p)*I(l,q)*F(k,j) +F(i,l)*I(k,p)*I(j,q) + I(i,p)*F(k,q)*I(j,l)+ I(k,p)*F(i,q)*I(j,l));       
        
}




void HyperElasticPolynomialPotential::computed2Psi(const double J, const double Ibar1, const double Ibar2, const STensor3 &dJdF, const STensor3 &dIbar1dF, const STensor3 &dIbar2dF, STensor3 &d2PsidFdIbar1, STensor3 &d2PsidFdIbar2, STensor3 &d2PsidFdJ) const
{
  for(int k = 0; k < 3; k++)
    for(int l = 0; l < 3; l++)
    {
      d2PsidFdIbar1(k, l) = 0.0;
      d2PsidFdIbar2(k, l) = 0.0;

      for(typename std::vector<std::vector<double> >::size_type i = 2; i < _A.size(); i++)
        for(typename std::vector<double>::size_type j = 0; j < _A[i].size(); j++)
          d2PsidFdIbar1(k, l) += _A[i][j] * i*(i-1)*pow(Ibar1-3, i-2)*pow(Ibar2-3, j)*dIbar1dF(k, l);

      for(typename std::vector<std::vector<double> >::size_type i = 1; i < _A.size(); i++)
        for(typename std::vector<double>::size_type j = 1; j < _A[i].size(); j++)
        {
          d2PsidFdIbar1(k, l) += _A[i][j] * i*j*pow(Ibar1-3, i-1)*pow(Ibar2-3, j-1)*dIbar2dF(k, l);
          d2PsidFdIbar2(k, l) += _A[i][j] * i*j*pow(Ibar1-3, i-1)*pow(Ibar2-3, j-1)*dIbar1dF(k, l);
        }

      for(typename std::vector<std::vector<double> >::size_type i = 0; i < _A.size(); i++)
        for(typename std::vector<double>::size_type j = 2; j < _A[i].size(); j++)
          d2PsidFdIbar2(k, l) += _A[i][j] * j*(j-1)*pow(Ibar1-3, i)*pow(Ibar2-3, j-2)*dIbar2dF(k, l);
    }

   for(int m = 0; m < 3; m++)
     for(int l = 0; l < 3; l++)
     {
       d2PsidFdJ(m, l) = 0.0;

       for(typename std::vector<double>::size_type k = 1; k <= _D.size(); k++)
         d2PsidFdJ(m, l) += _D[k-1]*2*k*(2*k-1)*pow(J-1, 2*k-2)*dJdF(m, l);
     }
}

//////////////////////////////////////////////////////////////////////

SuperElasticPotential::SuperElasticPotential(const int num, const double E, const double nu, const bool pert, const double tol):
  elasticPotential(num), _E(E), _nu(nu), _ByPerturbation(pert), _tol(tol)
{
  _mu = _E/(2.0*(1.0+_nu));

  // Dummy elastic tensor.
  // Should be implemented with the proper one.
  for(int i = 0; i < 3; i++)
    for(int j = 0; j < 3; j++)
      for(int k = 0; k < 3; k++)
        for(int l = 0; l < 0; l++)
          _Cel(i, j, k, l) = 0.0;
}

SuperElasticPotential::SuperElasticPotential(const SuperElasticPotential &src):
  elasticPotential(src),
  _E(src._E), _nu(src._nu), _K(src._K), _mu(src._mu), _C1(src._C1), _C2(src._C2), _Cel(src._Cel),
  _ByPerturbation(src._ByPerturbation), _tol(src._tol)
{}


void SuperElasticPotential::setParameters_C1_C2_K(double val_C1,double val_C2,double val_K)
{
  _C1 = val_C1;
  _C2 = val_C2;
  _K = val_K;
}


double SuperElasticPotential::get(const STensor3 &F) const
{
  const double J = F.determinant();
  static STensor3 C;
    for (int i=0; i<3; i++)
      for (int j=0; j<3; j++)
        C(i,j) = (F(0,i)*F(0,j)+F(1,i)*F(1,j)+F(2,i)*F(2,j));

  double I1 = C.trace();
  double ret = 0.0;
  double logJ = log(J);

  // compute strain density energy
  ret = _C1*(I1-3.0) + _C2*(I1-3.0)*(I1-3.0) - 2.0*_C1*logJ + _K/2.0*(J-1.0)*(J-1.0);

  return ret;
}

void SuperElasticPotential::constitutive(const STensor3 &F, STensor3 &P, const bool stiff, STensor43 *L, const bool dTangent, STensor63* dCalgdeps) const
{
  if(_ByPerturbation)
  {
    double potential = get(F);
    static STensor3 Fp;

    for(int i = 0; i < 3; i++)
      for(int j = 0; j < 3; j++)
      {
        Fp = (F);
        Fp(i, j) += _tol;
        double potentialPlus = get(Fp);
        P(i, j) = (potentialPlus - potential) / _tol;
      }

    if(stiff)
    {
      static STensor3 Fp2, Pp;
      for(int i = 0; i < 3; i++)
        for(int j = 0; j < 3; j++)
        {
          Fp2 = F;
          Fp2(i, j) += _tol;
          constitutive(Fp2, Pp, false, NULL);

          for(int p = 0; p < 3; p++)
            for(int q = 0; q < 3; q++)
                (*L)(p, q, i, j) = (Pp(p, q) - P(p, q)) / _tol;
        }
    }
    
    if(dTangent){
      STensorOperation::zero(*dCalgdeps);
      static STensor3 Fpp2,Ppp;
      static STensor43* Lp;
      for (int i=0; i<3; i++){
        for (int j=0; j<3; j++){
          Fpp2 = F;
          Fpp2(i,j) += _tol;
          constitutive(Fpp2,Ppp,true,Lp,false,NULL);
      
        for (int k=0; k<3; k++){
         for (int l=0; l<3; l++){
          for (int p=0; p<3; p++){
           for (int q=0; q<3; q++){
               (*dCalgdeps)(k,l,p,q,i,j) = ((*Lp)(k,l,p,q)-(*L)(k,l,p,q))/_tol;
            }
           }
          }
         }
        }
     }
    }     
  }
  else
  {          
  const double J = F.determinant();
  static STensor3 Finv;
  STensorOperation::inverseSTensor3(F, Finv);
  static STensor3 C;
    for (int i=0; i<3; i++)
      for (int j=0; j<3; j++)
        C(i,j) = (F(0,i)*F(0,j)+F(1,i)*F(1,j)+F(2,i)*F(2,j));

  double I1 = C.trace();
  // Compute P.
  for(int i = 0; i < 3; i++)
    for(int j = 0; j < 3; j++)
    {
      P(i, j) = 2.0*(_C1 + 2.0*_C2*(I1-3.0))*F(i, j) + J*(_K*(J-1.0)-2.0*_C1/J)*Finv(j,i); 
    }  
  // Compute tangent.
  if(stiff)
  { 
    STensor3 I(1.0);
    STensor3 dI1dF = 2.0*F;
    STensor3 dJdF;
    computedJdF(J, Finv, dJdF);
    STensor43 dFinvdF;
    computedFinvdF(Finv,dFinvdF);
    
    STensorOperation::zero(*L);
    for(int i = 0; i < 3; i++)
      for(int j = 0; j < 3; j++)
        for(int k = 0; k < 3; k++)
          for(int l = 0; l < 3; l++)
            (*L)(i, j, k, l) = 2.0*(_C1+2.0*_C2*(I1-3.0))*I(i,k)*I(j,l) + 4.0*_C2* dI1dF(k,l)*F(i,j) 
                               + dJdF(k,l)*_K*(2.0*J-1.0)*Finv(j,i) + J*(_K*(J-1.0)-2.0*_C1/J)*dFinvdF(j,i,k,l);
    
    if(dTangent){
      STensorOperation::zero(*dCalgdeps);
      STensor43 d2JdFdF; 
      computed2JdFdF(J, Finv, d2JdFdF);
    
      for(int i = 0; i < 3; i++)
       for(int j = 0; j < 3; j++)
        for(int k = 0; k < 3; k++)
         for(int l = 0; l < 3; l++)
          for(int p = 0; p < 3; p++)
           for(int q = 0; q < 3; q++)     
            (*dCalgdeps)(i,j,k,l,p,q) = 4.0*_C2*dI1dF(p,q)*I(i,k)*I(j,l) + 8.0*_C2*I(k,p)*I(l,q)*F(i,j) + 4.0*_C2* dI1dF(k,l)*I(i,p)*I(j,q)
                                       + d2JdFdF(k,l,p,q)*_K*(2.0*J-1.0)*Finv(j,i) + dJdF(k,l)*_K*2.0*dJdF(p,q)*Finv(j,i) + dJdF(k,l)*_K*(2.0*J-1.0)*dFinvdF(j,i,p,q)
                                       + dJdF(p,q)*_K*(2.0*J-1.0)*dFinvdF(j,i,k,l) + J*(_K*(J-1.0)-2.0*_C1/J)*(Finv(q,i)*Finv(l,p)*F(j,k) + Finv(l,i)*Finv(q,k)*Finv(j,p));
                        
   }                          
  }
 }
}


void SuperElasticPotential::computedJdF(const double J, const STensor3 &Finv, STensor3 &dJdF) const
{
  for(int i = 0; i < 3; i++)
    for(int j = 0; j < 3; j++)
      dJdF(i, j) = J*Finv(j, i);
}


void SuperElasticPotential::computedFinvdF(const STensor3 &Finv, STensor43 &dFinvdF) const
{
  for(int i = 0; i < 3; i++)
    for(int j = 0; j < 3; j++)
      for(int k = 0; k < 3; k++)
        for(int l = 0; l < 3; l++)
          dFinvdF(i,j,k,l) = -Finv(l, j)*Finv(i,k);
}


void SuperElasticPotential::computed2JdFdF(const double J, const STensor3 &Finv, STensor43 &d2J) const
{
  for(int i = 0; i < 3; i++)
    for(int j = 0; j < 3; j++)
      for(int k = 0; k < 3; k++)
        for(int l = 0; l < 3; l++)
          d2J(i, j, k, l) = J*(Finv(j, i)*Finv(l, k) - Finv(l, i)*Finv(j, k));
}


