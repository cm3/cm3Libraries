//
// C++ Interface: material law
//
// Description: non local damage elasto-plastic law it is a "virtual implementation" you have to define a law
//              in your project which derive from this law. you have to do THE SAME for your IPVariable
//              which have to derive from IPJ2linear (all data in this ipvarible have name beggining by _nld...
//
// Author:  <L. Noels>, (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef MLAWNONLOCALDAMAGE_H_
#define MLAWNONLOCALDAMAGE_H_
#include "mlaw.h"
#include "STensor3.h"
#include "STensor43.h"
#include "ipNonLocalDamage.h"
#include "material.h"
#include "nonLocalDamageLaw.h"

class mlawNonLocalDamage : public materialLaw
{

 protected:
  double _rho,_nu0,_mu0; //maximal mu for anisotropic

  // to be compatible with umat: use same variable
  double sq2;
  mutable double* strn_n, *strs_n, *dstrn, *strs;
  mutable double* statev_n, *statev_1;
  mutable double** Calgo;
  mutable double** Cref;
  mutable double*** dCref;
  mutable double* tau;
  mutable double** dtau;
  mutable double *Vstrain;
  mutable double **Ftmp;
  mutable double **Ftmp0;
  mutable double **dR;
  mutable double** C0PathFollowing;
  mutable double** invC0PathFollowing;
  mutable double*  dEnePathFollowingdStrain;
  mutable double*  elStrainPathFollowing;  

  MFH::Material* mat;

  double *props;
  int nprops1;

  int nsdv;
  int nlVar;
  int model, model_1;

  double _Dmax_Inc, _Dmax_Mtx;

  mutable double* dpdE;
  mutable double* strs_dDdp_bar;
  mutable double** chara_Length;
  mutable double** chara_Length_INC;
  mutable double SpBar;
  mutable double dFd_d_bar;
  mutable double dpdFd;
  mutable double dFddp;

  mutable double* dFd_dE;
  mutable double* dstrs_dFd_bar;

 public:
  mlawNonLocalDamage(const int num, const double rho, const char *propName);
  void setNumberOfNonLocalVariables(int nb) {nlVar=nb;}; //in case we want to combine two materials of different kind 

  void setInitialDmax_Inc(double d) {_Dmax_Inc=d;};
  void setInitialDmax_Mtx(double d) {_Dmax_Mtx=d;};
  void setInitialDmax(double d) {_Dmax_Mtx=d;};


 #ifndef SWIG
  mlawNonLocalDamage(const mlawNonLocalDamage &source);
  mlawNonLocalDamage& operator=(const materialLaw &source);
  virtual ~mlawNonLocalDamage();
	virtual materialLaw* clone() const {return new mlawNonLocalDamage(*this);};
	virtual void checkInternalState(IPVariable* ipv, const IPVariable* ipvprev) const{}; // do nothing
  // function of materialLaw
  virtual matname getType() const{return materialLaw::nonLocalDamage;}

  virtual void createIPState(IPStateBase* &ips, bool hasBodyForce, const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const IntPt *GP=NULL, const int gpt = 0) const;

  virtual bool withEnergyDissipation() const {return true;};
  virtual void createIPState(IPNonLocalDamage *ivi, IPNonLocalDamage *iv1, IPNonLocalDamage *iv2) const;
  virtual void createIPVariable(IPNonLocalDamage *&ipv,bool hasBodyForce, const MElement *ele,const int nbFF, const IntPt *GP, const int gpt) const;
  virtual void initLaws(const std::map<int,materialLaw*> &maplaw){}; // this law is initialized so nothing to do
  virtual double soundSpeed() const; // default but you can redefine it for your case
  virtual const double bulkModulus() const;
  virtual const double shearModulus() const;
  virtual const double poissonRatio() const;
  virtual int getNbNonLocalVariables() const {return nlVar;};
  virtual void updateCharacteristicLengthTensor(double** chara_Length, double *statev) const {};
  virtual void ElasticStiffness(STensor43* elasticTensor) const;
  virtual void ElasticStiffness(double *statev, STensor43* elasticTensor) const;
  virtual void ElasticStiffnessMtx(double *statev, STensor43* elasticTensor) const;
  virtual void ElasticStiffnessInc(double *statev, STensor43* elasticTensor) const;
  virtual void ElasticStiffness_incOri(double e1, double e2, double e3,STensor43* elasticTensor) const;
  // specific function
 public:
  virtual void constitutive(
                            const STensor3& F0,         // initial deformation gradient (input @ time n)
                            const STensor3& Fn,         // updated deformation gradient (input @ time n+1)
                            STensor3 &P,                // updated 1st Piola-Kirchhoff stress tensor (output)
                                                        // contains the initial values on input
                            const IPVariable *q0,       // array of initial internal variable
                            IPVariable *q1,             // updated array of internal variable (in ipvcur on output),
                            STensor43 &Tangent,         // constitutive tangents (output)
                            const bool stiff,            // if true compute the tangents
                            STensor43* elasticTangent = NULL, 
                            const bool dTangent =false,
                            STensor63* dCalgdeps = NULL) const;

  virtual void constitutiveTFA(
                            const STensor3& F0,         // previous deformation gradient (input @ time n)
                            const STensor3& Fn,         // current deformation gradient (input @ time n+1)
                            STensor3 &P,                // current 1st Piola-Kirchhoff stress tensor (output)
                            const IPVariable *q0,       // array of previous internal variables
                            IPVariable *q1,             // current array of internal variable (in ipvcur on output),
                            STensor43 &Tangent,         // tangents (output)
                            STensor3 &epsEig,
                            STensor43 &depsEigdeps) const;
                            
  virtual void constitutiveTFA_incOri(
                            const STensor3& F0,         // previous deformation gradient (input @ time n)
                            const STensor3& Fn,         // current deformation gradient (input @ time n+1)
                            const fullVector<double>& euler,
                            STensor3 &P,                // current 1st Piola-Kirchhoff stress tensor (output)
                            const IPVariable *q0,       // array of previous internal variables
                            IPVariable *q1,             // current array of internal variable (in ipvcur on output),
                            STensor43 &Tangent,         // tangents (output)
                            STensor3 &epsEig,
                            STensor43 &depsEigdeps) const;

  virtual void constitutive(
                            const STensor3& F0,         // initial deformation gradient (input @ time n)
                            const STensor3& Fn,         // updated deformation gradient (input @ time n+1)
                            STensor3 &P,                // updated 1st Piola-Kirchhoff stress tensor (output)
                                                        // contains the initial values on input
                            const IPVariable *q0,       // array of initial internal variable
                            IPVariable *q1,             // updated array of internal variable (in ipvcur on output),
                            STensor43 &Tangent,         // constitutive tangents (output)
                            std::vector< STensor3 >  &dLocalPlasticStrainDStrain,
  			    std::vector< STensor3 >  &dStressDNonLocalPlasticStrain,
                            fullMatrix<double>   &dLocalPlasticStrainDNonLocalPlasticStrain,
                            const bool stiff,            // if true compute the tangents
                            STensor43* elasticTangent = NULL) const;

  int getNsdv() const { return nsdv;}
  
  virtual void SecantStiffnessIsotropic(const IPVariable *q1i, STensor43& Secant) const;

 #endif // SWIG
};

MFH::Material *init_material(double *_props, int idmat);
MFH::Damage* init_damage(double* props, int iddam);
MFH::Clength* init_clength(double* props, int idclength);

#endif // MLAWNONLOCALDAMAGE_H_
