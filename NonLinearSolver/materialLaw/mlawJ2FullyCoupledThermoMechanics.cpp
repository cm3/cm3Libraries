//
// C++ Interface: material law
//
// Description: j2 thermo elasto-plastic law

// Author:  <Van Dung NGUYEN>, (C) 2016
//
// Copyright: See COPYING file that comes with this distribution
//
//

#include "mlawJ2FullyCoupledThermoMechanics.h"
#include "STensorOperations.h"

mlawJ2FullyCoupledThermoMechanics::mlawJ2FullyCoupledThermoMechanics(const int num, const double E, const double nu,const double rho,
                               const double sy0,const double h,const double tol,
                               const bool matrixbyPerturbation, const double pert, const bool init):
                               materialLaw(num,init),_tol(tol),_perturbationfactor(pert), _rho(rho),
                               _tangentByPerturbation(matrixbyPerturbation), _order(1), _TaylorQuineyFactor(0.9),
                               _thermalEstimationPreviousConfig(true){
  _j2IH = new LinearExponentialJ2IsotropicHardening(num, sy0, h, 0., 10.);
  _K = E/(3.*(1.-2.*nu));
  _mu = E/(2.*(1.+nu));
  _alp = 0.;
  _Tref = 298.;
  _ThermalConductivity = 0.;
  _cp = 0.;

  // by default, no temperature dependent
  _temFunc_K = new constantScalarFunction(1.);
  _temFunc_mu = new constantScalarFunction(1.);
  _temFunc_Sy0 = new constantScalarFunction(1.);
  _temFunc_H = new constantScalarFunction(1.);
  _temFunc_ThermalConductivity = new constantScalarFunction(1.);
  _temFunc_cp = new constantScalarFunction(1.);
  _tempFunc_alp = new constantScalarFunction(1.);

  STensor3 I(1.);
  for (int i=0; i<3; i++){
    for (int j=0; j<3; j++){
      for (int k=0; k<3; k++){
        for (int l=0; l<3; l++){
          _I4dev(i,j,k,l) = 0.5*(I(i,k)*I(j,l)+I(i,l)*I(j,k)) - I(i,j)*I(k,l)/3.;
        }
      }
    }
  }
};

mlawJ2FullyCoupledThermoMechanics::mlawJ2FullyCoupledThermoMechanics(const mlawJ2FullyCoupledThermoMechanics& src):materialLaw(src){
  _j2IH = NULL;
  if (src._j2IH != NULL)
    _j2IH = src._j2IH->clone();

  _temFunc_Sy0 = NULL; // temperature dependence of initial yield stress
  if (src._temFunc_Sy0!= NULL)
    _temFunc_Sy0 = src._temFunc_Sy0->clone();

  _temFunc_H = NULL; // temperature dependence of hardening stress
  if (src._temFunc_H != NULL){
    _temFunc_H = src._temFunc_H->clone();
  }

  _temFunc_ThermalConductivity = NULL;
  if (src._temFunc_ThermalConductivity!= NULL){
    _temFunc_ThermalConductivity = src._temFunc_ThermalConductivity->clone();
  }

  _temFunc_cp = NULL;
  if (src._temFunc_cp != NULL){
    _temFunc_cp = src._temFunc_cp->clone();
  }

  _tempFunc_alp = NULL;
  if (src._tempFunc_alp != NULL){
    _tempFunc_alp = src._tempFunc_alp->clone();
  }

  _rho = src._rho;
  _alp = src._alp;
  _Tref = src._Tref;
  _TaylorQuineyFactor = src._TaylorQuineyFactor;
  _ThermalConductivity = src._ThermalConductivity;
  _cp = src._cp;

    // temperature dependent elastic behavior
  _K = src._K;
  _mu = src._mu;

  _temFunc_K = NULL; // bulk modulus;
  if (src._temFunc_K != NULL){
    _temFunc_K = src._temFunc_K->clone();
  }

  _temFunc_mu = NULL; // shear modulus;
  if (src._temFunc_mu!=NULL){
    _temFunc_mu = src._temFunc_mu->clone();
  }

  _tol = src._tol; // tolerace of plastic corrector
  _perturbationfactor = src._perturbationfactor; // perturbation factor
  _tangentByPerturbation = src._tangentByPerturbation; // flag for tangent by perturbation
  _order = src._order; // order for log and exp approximation

  _I4dev = src._I4dev;
  _thermalEstimationPreviousConfig = src._thermalEstimationPreviousConfig;
};
mlawJ2FullyCoupledThermoMechanics& mlawJ2FullyCoupledThermoMechanics::operator=(const materialLaw &source)
{
  materialLaw::operator=(source);
  const mlawJ2FullyCoupledThermoMechanics* src = dynamic_cast<const mlawJ2FullyCoupledThermoMechanics*>(&source);
  if(src != NULL)
  {
    if(_j2IH != NULL) delete _j2IH;
    if (src->_j2IH != NULL)
      _j2IH = src->_j2IH->clone();
    if(_temFunc_Sy0 != NULL) delete _temFunc_Sy0; // temperature dependence of initial yield stress
    if (src->_temFunc_Sy0!= NULL)
      _temFunc_Sy0 = src->_temFunc_Sy0->clone();
    if(_temFunc_H != NULL) delete _temFunc_H; // temperature dependence of hardening stress
    if (src->_temFunc_H != NULL){
      _temFunc_H = src->_temFunc_H->clone();
    } 
    if(_temFunc_ThermalConductivity != NULL) delete _temFunc_ThermalConductivity;
    if (src->_temFunc_ThermalConductivity!= NULL){
      _temFunc_ThermalConductivity = src->_temFunc_ThermalConductivity->clone();
    }
    if(_temFunc_cp != NULL) delete _temFunc_cp;
    if (src->_temFunc_cp != NULL){
      _temFunc_cp = src->_temFunc_cp->clone();
    }
    if(_tempFunc_alp != NULL) delete _tempFunc_alp;
    if (src->_tempFunc_alp != NULL){
      _tempFunc_alp = src->_tempFunc_alp->clone();
    }

    _rho = src->_rho;
    _alp = src->_alp;
    _Tref = src->_Tref;
    _TaylorQuineyFactor = src->_TaylorQuineyFactor;
    _ThermalConductivity = src->_ThermalConductivity;
    _cp = src->_cp;

    // temperature dependent elastic behavior
    _K = src->_K;
    _mu = src->_mu;

    if(_temFunc_K != NULL) delete _temFunc_K; // bulk modulus;
    if (src->_temFunc_K != NULL){
      _temFunc_K = src->_temFunc_K->clone();
    }

    if(_temFunc_mu != NULL) delete _temFunc_mu; // shear modulus;
    if (src->_temFunc_mu!=NULL){
      _temFunc_mu = src->_temFunc_mu->clone();
    }

    _tol = src->_tol; // tolerace of plastic corrector
    _perturbationfactor = src->_perturbationfactor; // perturbation factor
    _tangentByPerturbation = src->_tangentByPerturbation; // flag for tangent by perturbation
    _order = src->_order; // order for log and exp approximation

    _I4dev = src->_I4dev;
    _thermalEstimationPreviousConfig = src->_thermalEstimationPreviousConfig;
  }
  return *this;

};


mlawJ2FullyCoupledThermoMechanics::~mlawJ2FullyCoupledThermoMechanics(){
  if (_j2IH != NULL) delete _j2IH; _j2IH = NULL;
  if (_temFunc_Sy0 != NULL) delete _temFunc_Sy0;  _temFunc_Sy0 = NULL;
  if (_temFunc_H != NULL) delete _temFunc_H; _temFunc_H= NULL;
  if (_temFunc_K != NULL) delete _temFunc_K; _temFunc_K = NULL;
  if (_temFunc_mu != NULL) delete _temFunc_mu; _temFunc_mu = NULL;
};

void mlawJ2FullyCoupledThermoMechanics::createIPState(IPStateBase* &ips, bool hasBodyForce, const bool* state_,const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
  Msg::Error("this function is not used; IPVariable is not constructed by this law");
}

double mlawJ2FullyCoupledThermoMechanics::soundSpeed() const
{
  return sqrt((_K+4.*_mu/3.)/_rho);
}

void mlawJ2FullyCoupledThermoMechanics::constitutive(const STensor3& F0,         // previous deformation gradient (input @ time n)
                            const STensor3& F1,         // current deformation gradient (input @ time n+1)
                            STensor3 &P1,                // current 1st Piola-Kirchhoff stress tensor (output)
                            const IPVariable *q0i,       // array of previous internal variables
                            IPVariable *q1i,             // current array of internal variable (in ipvcur on output),
                            STensor43 &Tangent,         // tangents (output)
                            const bool stiff,            // if true compute the tangents
                            STensor43* elasticTangent, 
                            const bool dTangent,
                            STensor63* dCalgdeps) const{
  static SVector3 gradT, temp2;
  static STensor3 temp3;
  static STensor33 temp33;
  static double tempVal;
  static STensor43 dFpdF, dFedF;
  static STensor3 dFpdT, dFedT, dGammadF;
  double dGammadT;
  const IPJ2ThermoMechanics *q0 = dynamic_cast<const IPJ2ThermoMechanics *> (q0i);
        IPJ2ThermoMechanics *q1 = dynamic_cast<IPJ2ThermoMechanics *>(q1i);
  if(elasticTangent != NULL) Msg::Error("mlawJ2FullyCoupledThermoMechanics elasticTangent not defined"); 
  predictorCorector(F0,F1,P1,q0,q1,Tangent,dFpdF, dFpdT, dFedF, dFedT,dGammadF,dGammadT,_Tref,_Tref,gradT,gradT,temp2,temp3,temp3,temp2,temp33,tempVal,
                      tempVal,temp3,tempVal,tempVal,temp3,stiff);
  

};
void mlawJ2FullyCoupledThermoMechanics::constitutive(const STensor3& F0,         // initial deformation gradient (input @ time n)
                            const STensor3& F,         // updated deformation gradient (input @ time n+1)
                            STensor3 &P,                // updated 1st Piola-Kirchhoff stress tensor (output)
                            const IPVariable *q0i,       // array of initial internal variable
                            IPVariable *q1i,             // updated array of internal variable (in ipvcur on output),
                            STensor43& Tangent,         // mechanical tangents (output)
                            const double& T0, // previous temperature
			                      const double& T, // temperature
			                      const SVector3 &gradT0, // previous temeprature gradient
  			                    const SVector3 &gradT, // temeprature gradient
                            SVector3 &fluxT, // temperature flux
                            STensor3 &dPdT, // mechanical-thermal coupling
                            STensor3 &dfluxTdgradT, // thermal tengent
                            SVector3 &dfluxTdT,
                            STensor33 &dfluxTdF, // thermal-mechanical coupling
                            double &thermalSource,   // - cp*dTdt
                            double &dthermalSourcedT, // thermal source
                            STensor3 &dthermalSourcedF,
                            double& mechanicalSource, // mechanical source--> convert to heat
                            double & dmechanicalSourcedT,
                            STensor3 & dmechanicalSourceF,
                            const bool stiff,
                            STensor43* elasticTangent
                            ) const{
  const IPJ2ThermoMechanics *q0 = dynamic_cast<const IPJ2ThermoMechanics *> (q0i);
        IPJ2ThermoMechanics *q = dynamic_cast<IPJ2ThermoMechanics *>(q1i);
  if(elasticTangent != NULL) Msg::Error("mlawJ2FullyCoupledThermoMechanics elasticTangent not defined"); 
  if (!_tangentByPerturbation){
    static STensor43 dFpdF, dFedF;
    static STensor3 dFpdT, dFedT, dGammadF;
    double dGammadT;
    predictorCorector(F0,F,P,q0,q,Tangent,dFpdF,dFpdT, dFedF,dFedT,dGammadF,dGammadT,T0,T,gradT0,gradT,fluxT,dPdT,dfluxTdgradT,dfluxTdT,dfluxTdF,
                      thermalSource,dthermalSourcedT,dthermalSourcedF,
                      mechanicalSource,dmechanicalSourcedT,dmechanicalSourceF,stiff);
  }
  else{
    predictorCorector(F0,F,P,q0,q,T0,T,gradT0,gradT,fluxT,thermalSource, mechanicalSource);

    static STensor3 Fplus, Pplus;
    static SVector3 fluxTPlus, gradTplus;
    static double thermalSourcePlus;
    static double mechanicalSourcePlus;
    static IPJ2ThermoMechanics qPlus(*q0);

    // perturbe F
    for (int i=0; i<3; i++){
      for (int j=0; j<3; j++){
        Fplus = (F);
        Fplus(i,j) += _perturbationfactor;
        predictorCorector(F0,Fplus,Pplus,q0,&qPlus,T0,T,gradT0,gradT,fluxTPlus,thermalSourcePlus, mechanicalSourcePlus);
        for (int k=0; k<3; k++){
          for (int l=0; l<3; l++){
            Tangent(k,l,i,j) = (Pplus(k,l) - P(k,l))/(_perturbationfactor);
          }
          dfluxTdF(k,i,j) = (fluxTPlus(k) - fluxT(k))/_perturbationfactor;
        }
        dthermalSourcedF(i,j) = (thermalSourcePlus - thermalSource)/_perturbationfactor;
        dmechanicalSourceF(i,j) = (mechanicalSourcePlus - mechanicalSource)/_perturbationfactor;
      }
    }
    // perturbe gradT

    double gradTpert = _perturbationfactor*T0/1e-3;
    for (int i=0; i<3; i++){
      gradTplus = (gradT);
      gradTplus(i) += (gradTpert);
      predictorCorector(F0,F,Pplus,q0,&qPlus,T0,T,gradT0,gradTplus,fluxTPlus,thermalSourcePlus, mechanicalSourcePlus);
      for (int k=0; k<3; k++){
        dfluxTdgradT(k,i) = (fluxTPlus(k) - fluxT(k))/gradTpert;
      }
    }

    // perturb on T
    double Tplus = T+ _perturbationfactor*T0;
    predictorCorector(F0,F,Pplus,q0,&qPlus,T0,Tplus,gradT0,gradT,fluxTPlus,thermalSourcePlus, mechanicalSourcePlus);
    for (int k=0; k<3; k++){
      for (int l=0; l<3; l++){
        dPdT(k,l) = (Pplus(k,l) - P(k,l))/(_perturbationfactor*T0);
      }
      dfluxTdT(k) = (fluxTPlus(k) - fluxT(k))/(_perturbationfactor*T0);
    }
    dthermalSourcedT = (thermalSourcePlus - thermalSource)/(_perturbationfactor*T0);
    dmechanicalSourcedT = (mechanicalSourcePlus - mechanicalSource)/(_perturbationfactor*T0);

  }

  //Tangent.print("Tangent");
};

void mlawJ2FullyCoupledThermoMechanics::stress(const STensor3& Ee, const double& KT, const double& muT, const double& alpT, const double& Tr, const double& T, double &p, STensor3& corKirDev) const{
  p = KT*(Ee.trace() - 3.*alpT*(T-Tr));
  corKirDev = Ee.dev();
  corKirDev *= (2.*muT);
};


void mlawJ2FullyCoupledThermoMechanics::HookeTensor(const double KT, const double muT, STensor43& C) const{
  static const STensor3 Isecond(1.);
  for (int i=0; i<3; i++){
    for (int j=0; j<3; j++){
      for (int k=0; k<3; k++){
        for (int l=0; l<3; l++){
          C(i,j,k,l) = KT*Isecond(i,j)*Isecond(k,l) + 2.*muT*_I4dev(i,j,k,l);
        }
      }
    }
  }
};

double mlawJ2FullyCoupledThermoMechanics::deformationEnergy(const STensor3& C, const double& Tr, const double& T) const{
  double Jac= sqrt((C(0,0) * (C(1,1) * C(2,2) - C(1,2) * C(2,1)) -
          C(0,1) * (C(1,0) * C(2,2) - C(1,2) * C(2,0)) +
          C(0,2) * (C(1,0) * C(2,1) - C(1,1) * C(2,0))));
  double lnJ = log(Jac);
  static STensor3 logCdev;
  bool ok=STensorOperation::logSTensor3(C,_order,logCdev);
  double trace = logCdev.trace();
  logCdev(0,0)-=trace/3.;
  logCdev(1,1)-=trace/3.;
  logCdev(2,2)-=trace/3.;


  double KT = _K*_temFunc_K->getVal(T);
  double muT = _mu*_temFunc_mu->getVal(T);

  lnJ -= 3.*_alp*_tempFunc_alp->getVal(T)*(T-Tr);
  double Psy = KT*0.5*lnJ*lnJ+muT*0.25*STensorOperation::doubledot(logCdev,logCdev);
  return Psy;;
};

void mlawJ2FullyCoupledThermoMechanics::predictorCorector(const STensor3& F0,         // initial deformation gradient (input @ time n)
                            const STensor3& F,         // updated deformation gradient (input @ time n+1)
                            STensor3 &P,                // updated 1st Piola-Kirchhoff stress tensor (output)
                            const IPJ2ThermoMechanics *q0,       // array of initial internal variable
                            IPJ2ThermoMechanics *q,             // updated array of internal variable (in ipvcur on output),
                            const double& T0, // previous temperature
			                      const double& T, // temperature
			                      const SVector3 &gradT0, // previous temeprature gradient
  			                    const SVector3 &gradT, // temeprature gradient
                            SVector3 &fluxT, // temperature flux)
                            double &thermalSource,
                            double& mechanicalSource
                            ) const{
  // temp variables
  static STensor43 Tangent, dFpdF, dFedF;
  static STensor3 dPdT, dfluxTdgradT, dthermalSourcedF, dmechanicalSourceF, dFpdT, dFedT, dGammadF;
  double dGammadT=0.;
  static STensor33 dfluxTdF;
  static SVector3 dfluxTdT;
  static double dthermalSourcedT, dmechanicalSourcedT;
  predictorCorector(F0,F,P,q0,q,Tangent,dFpdF,dFpdT, dFedF,dFedT,dGammadF,dGammadT,T0,T,gradT0,gradT,fluxT,dPdT,dfluxTdgradT,dfluxTdT,dfluxTdF,
                      thermalSource,dthermalSourcedT,dthermalSourcedF,
                      mechanicalSource,dmechanicalSourcedT,dmechanicalSourceF,false);
}

void mlawJ2FullyCoupledThermoMechanics::predictorCorector(const STensor3& F0,         // initial deformation gradient (input @ time n)
                            const STensor3& F,         // updated deformation gradient (input @ time n+1)
                            STensor3 &P,                // updated 1st Piola-Kirchhoff stress tensor (output)
                            const IPJ2ThermoMechanics *q0,       // array of initial internal variable
                            IPJ2ThermoMechanics *q,             // updated array of internal variable (in ipvcur on output),
                            STensor43& Tangent,         // mechanical tangents (output)
                            STensor43& dFpdF, // plastic tangent
                            STensor3& dFpdT, // plastic tangent
                            STensor43& dFedF, // elastic tangent
                            STensor3& dFedT, // elastic tangent
                            STensor3 &dGammadF,  //d plastic strain dF
                            double &dGammadT,     //d plastic strain dT
                            const double& T0, // previous temperature
			                      const double& T, // temperature
			                      const SVector3 &gradT0, // previous temeprature gradient
  			                    const SVector3 &gradT, // temeprature gradient
                            SVector3 &fluxT, // temperature flux
                            STensor3 &dPdT, // mechanical-thermal coupling
                            STensor3 &dfluxTdgradT, // thermal tengent
                            SVector3 &dfluxTdT,
                            STensor33 &dfluxTdF, // thermal-mechanical coupling
                            double &thermalSource,   // - cp*dTdt
                            double &dthermalSourcedT, // thermal source
                            STensor3 &dthermalSourcedF,
                            double& mechanicalSource, // mechanical source--> convert to heat
                            double & dmechanicalSourcedT,
                            STensor3 & dmechanicalSourceF,
                            const bool stiff
                            ) const {
  STensor3& Fp = q->getRefToFp();
  double& eps = q->getRefToEquivalentPlasticStrain();
  
  double eps0 = q0->getConstRefToEquivalentPlasticStrain();

  // elastic predictor
  q->getRefToIPJ2IsotropicHardening() = q0->getConstRefToIPJ2IsotropicHardening();
  Fp = q0->getConstRefToFp();
  eps = eps0;

  static STensor3 Fpinv, Fe, Ce;
  STensorOperation::inverseSTensor3(Fp,Fpinv);
  STensorOperation::multSTensor3(F,Fpinv,Fe);
  STensorOperation::multSTensor3FirstTranspose(Fe,Fe,Ce);
  // strain
  static STensor43 Lpr;
  static STensor63 dLDCe;
  /* Compute equivalent stress */
  static STensor3 Eepr;
  bool ok=STensorOperation::logSTensor3(Ce,_order,Eepr,&Lpr,&dLDCe);
  if(!ok)
  {
     P(0,0) = P(1,1) = P(2,2) = sqrt(-1.);
     return; 
  }
  Eepr *= 0.5;

  static STensor43 L;
  L = Lpr;
  STensor3& Ee  = q->getRefToElasticDeformationTensor();
  Ee = Eepr;

  static STensor43 DexpA; // use for compute tangent later

  // temperature dependent material properties
  double KT = _K*_temFunc_K->getVal(T);
  double muT = _mu*_temFunc_mu->getVal(T);
  double alpT = _alp*_tempFunc_alp->getVal(T);
  double conductT = _ThermalConductivity*_temFunc_ThermalConductivity->getVal(T);
  double cpT = _cp*_temFunc_cp->getVal(T0); // predious --> derivatives 0

  // compute corotational Kirchhoff stress
  static double p;
  static STensor3 corKirDev;
  this->stress(Ee,KT,muT,alpT,_Tref,T,p,corKirDev);

  // equivalent stress
  double Seqpr = sqrt(1.5*corKirDev.dotprod());
  // plastic normal-> based on trial value
  static STensor3 devEepr;
  devEepr = Eepr.dev();
  double EeprEq = sqrt(devEepr.dotprod()/1.5);
  static STensor3 N;
  N = (devEepr);
  N *= (1./EeprEq);
  double Deps = 0.;

  /* Test plasticity */
  _j2IH->hardening(eps0,q0->getConstRefToIPJ2IsotropicHardening(), eps, q->getRefToIPJ2IsotropicHardening());
  double Sy0T = _j2IH->getYield0()*_temFunc_Sy0->getVal(T);
  double Sy   = Sy0T + (q->getConstRefToIPJ2IsotropicHardening().getR() - _j2IH->getYield0())*_temFunc_H->getVal(T);
  double H    = q->getConstRefToIPJ2IsotropicHardening().getDR()*_temFunc_H->getVal(T);

  double VMcriterion = Seqpr - Sy;
  if(VMcriterion >0.)
  {

    int ite = 0, maxite = 1000;
    while(fabs(VMcriterion)/Sy0T > _tol)
    {
      double coef = muT*3.+H;
      double deps = VMcriterion/coef;
      //
      eps += deps;
      // update elastic strain
      Ee.daxpy(N,-deps);
      this->stress(Ee,KT,muT,alpT,_Tref,T,p,corKirDev);
      double Seq = sqrt(1.5*corKirDev.dotprod());
      _j2IH->hardening(eps0,q0->getConstRefToIPJ2IsotropicHardening(), eps, q->getRefToIPJ2IsotropicHardening());
      Sy = Sy0T + (q->getConstRefToIPJ2IsotropicHardening().getR() - _j2IH->getYield0())*_temFunc_H->getVal(T);
      H = q->getConstRefToIPJ2IsotropicHardening().getDR()*_temFunc_H->getVal(T);

      VMcriterion = Seq-Sy;
      ite++;
      if(ite > maxite)
      {
        Msg::Error("No convergence for plastic correction in J2 !!");
        break;
      }
    }
    // update plastic strain
    Deps = eps-q0->getConstRefToEquivalentPlasticStrain();
    static STensor3 DepsN;
    DepsN = (N);
    DepsN *= Deps;
    static STensor3 expDepsN;
    STensorOperation::expSTensor3(DepsN,_order,expDepsN,&DexpA);
    STensorOperation::multSTensor3(expDepsN,q0->getConstRefToFp(),Fp);

    // update elastic strain
    STensorOperation::inverseSTensor3(Fp,Fpinv);
    STensorOperation::multSTensor3(F,Fpinv,Fe);
    STensorOperation::multSTensor3FirstTranspose(Fe,Fe,Ce);
    bool ok=STensorOperation::logSTensor3(Ce,_order,Ee,&L,&dLDCe);
    if(!ok)
    {
       P(0,0) = P(1,1) = P(2,2) = sqrt(-1.);
       return; 
    }
    Ee *= 0.5;
  };

  // estimation of PK stress
  static STensor3 corKir;
  STensorOperation::diag(corKir,p);
  corKir += corKirDev;
  static STensor3 S;
  STensorOperation::multSTensor3STensor43(corKir,L,S);
  static STensor3 tempTensor23;
  STensorOperation::multSTensor3(Fe,S,tempTensor23);
  STensorOperation::multSTensor3SecondTranspose(tempTensor23,Fpinv,P);

  // elastic energy
  q->getRefToElasticEnergy()= this->deformationEnergy(Ce,_Tref,T);
    // plastic power (Wp1- Wp0)/dt
  if (this->getTimeStep() > 0){
    q->getRefToPlasticPower() = Deps*Sy/this->getTimeStep();
  }
  else{
    q->getRefToPlasticPower() = 0.;
    #ifdef _DEBUG
    Msg::Warning("Time step is zero in mlawJ2FullyCoupledThermoMechanics::predictorCorector");
    #endif //_DEBUG
  }
  // thermal energy
  q->_thermalEnergy = cpT*T;

  double J  = 1.;
  STensor3 Finv(0.);
  if (_thermalEstimationPreviousConfig){
    STensorOperation::inverseSTensor3(F0,Finv);
    J = STensorOperation::determinantSTensor3(F0);
  }
  else{
    STensorOperation::inverseSTensor3(F,Finv);
    J = STensorOperation::determinantSTensor3(F);
  }
  static STensor3 Cinv;
  STensorOperation::multSTensor3SecondTranspose(Finv,Finv,Cinv);
  STensorOperation::multSTensor3SVector3(Cinv,gradT,fluxT);
  fluxT *= (-conductT*J);
  
  if (this->getTimeStep() > 0.){
    thermalSource = -cpT*(T-T0)/this->getTimeStep();
  }
  else 
    thermalSource = 0.;
    
  mechanicalSource = 0.;
  // thermoelastic heat
  double DKDT = _K*_temFunc_K->getDiff(T);
  double DmuDT = _mu*_temFunc_mu->getDiff(T);
  double DalpDT = _alp*_tempFunc_alp->getDiff(T);
  double DconductDT = _ThermalConductivity*_temFunc_ThermalConductivity->getDiff(T);
  double DcpdT = 0.;
  double HT = _j2IH->getYield0()*_temFunc_Sy0->getDiff(T) + (q->getConstRefToIPJ2IsotropicHardening().getR() - _j2IH->getYield0())*_temFunc_H->getDiff(T);
  //
  static STensor3 DcorKirDT;
  STensorOperation::zero(DcorKirDT); // derivative in respect to T
  for (int i=0; i<3; i++){
    DcorKirDT(i,i) += p*DKDT/KT - 3*KT*(DalpDT*(T-_Tref) + alpT);
    for (int j=0; j<3; j++){
      DcorKirDT(i,j) += corKirDev(i,j)*DmuDT/muT;
    }
  }
  // previous elastic deformation
  static STensor3 DEe;
  DEe = Ee;
  DEe -= q0->getConstRefToElasticDeformationTensor();
  if (this->getTimeStep() > 0){
    mechanicalSource += (STensorOperation::doubledot(DcorKirDT,DEe)*T/this->getTimeStep());
  }


  // thermoplastic heat
  mechanicalSource += _TaylorQuineyFactor*q->getRefToPlasticPower();

  if (stiff){
    static STensor43 DcorKirDEepr;
    HookeTensor(KT,muT,DcorKirDEepr);
    //material properties on temperature

    static STensor3 dPlasticPowerDEepr;
    static double dPlasticPowerDT;
    static STensor43 dFpDEepr;
    static STensor3 DgammaDEepr;
    
    STensorOperation::zero(dPlasticPowerDEepr);
    STensorOperation::zero(dPlasticPowerDT);
    STensorOperation::zero(dFpdT);
    STensorOperation::zero(dFpDEepr);
    STensorOperation::zero(dGammadF);
    STensorOperation::zero(DgammaDEepr);
    dGammadT=0.;
    
    if (Deps > 0){
      DgammaDEepr = (N);
      DgammaDEepr *= (2.*muT/(3.*muT+H));
      dGammadT = (Seqpr/muT*DmuDT - HT-3.*DmuDT*Deps)/(3.*muT+H);

      static STensor43 BE;
      tensprod(N,N,BE);
      BE *= (2.*muT/(3.*muT+H)- 2.*Deps/3./EeprEq);
      double val=Deps/EeprEq;
      BE.axpy(val,_I4dev);

      static STensor3  BT;
      BT = (N);
      BT*= dGammadT;

      // update with plasticity corKir
      DcorKirDEepr.axpy(-2.*muT,BE);
      DcorKirDT.daxpy(BT,-2.*muT);

      const STensor3& Fp0 = q0->getConstRefToFp();
      static STensor43 DexpABE;
      STensorOperation::multSTensor43(DexpA,BE,DexpABE);
      // update with plasticity Fp
      for (int i=0; i<3; i++){
        for (int j=0; j<3; j++){
          for (int k=0; k<3; k++){
            for (int l=0; l<3; l++){
              for (int p=0; p<3; p++){
                dFpDEepr(i,j,k,l) += DexpABE(i,p,k,l)*Fp0(p,j);
              }
            }
          }
        }
      }
      static STensor3 DexpABT;
      STensorOperation::multSTensor43STensor3(DexpA,BT,DexpABT);
      STensorOperation::multSTensor3Add(DexpABT,Fp0,1.,dFpdT);
    
      double DPlasticPowerDgamma =  0.;
      if (this->getTimeStep() > 0.){
        DPlasticPowerDgamma = (Sy + Deps*H)/this->getTimeStep();
      }
      dPlasticPowerDEepr.daxpy(DgammaDEepr,DPlasticPowerDgamma);
      if (this->getTimeStep() > 0.){
        dPlasticPowerDT = (dGammadT*Sy + Deps*HT)/this->getTimeStep();
      }
      else{
        dPlasticPowerDT = 0.;
      }
    }

    const STensor3& Fp0 = q0->getConstRefToFp();
    static STensor3 invFp0;
    STensorOperation::inverseSTensor3(Fp0,invFp0);
    static STensor3 Fepr;
    STensorOperation::multSTensor3(F,invFp0,Fepr);
    static STensor43 EprToF;
    for (int i=0; i<3; i++){
      for (int j=0; j<3; j++){
        for (int k=0; k<3; k++){
          for (int l=0; l<3; l++){
            EprToF(i,j,k,l) = 0.;
            for (int p=0; p<3; p++){
              for (int q=0; q<3; q++){
                EprToF(i,j,k,l) += Lpr(i,j,p,q)*Fepr(k,p)*invFp0(l,q);
              }
            }
          }
        }
      }
    }

    static STensor43 DcorKirDF;
    STensorOperation::multSTensor43(DcorKirDEepr,EprToF,DcorKirDF);
    static STensor3 dPlasticPowerDF;
    if (Deps>0.){
      STensorOperation::multSTensor43(dFpDEepr,EprToF,dFpdF);
      STensorOperation::multSTensor3STensor43(DgammaDEepr,EprToF,dGammadF);
      STensorOperation::multSTensor3STensor43(dPlasticPowerDEepr,EprToF,dPlasticPowerDF);
    }
    else{
      STensorOperation::zero(dFpdF);
      STensorOperation::zero(dGammadF);
      dGammadT=0.;
      STensorOperation::zero(dPlasticPowerDF);
    }

    // done DcorKirDF, DcorKirDT, DFpDF, DFpdT

    static STensor43 DinvFpdF;
    static STensor3 DinvFpdT;
    STensorOperation::zero(DinvFpdF);
    STensorOperation::zero(DinvFpdT);
    if (Deps >0){
      for (int i=0; i<3; i++){
        for (int j=0; j<3; j++){
          for (int p=0; p<3; p++){
            for (int q=0; q<3; q++){
              DinvFpdT(i,j) -= Fpinv(i,p)*dFpdT(p,q)*Fpinv(q,j);
              for (int k=0; k<3; k++){
                for (int l=0; l<3; l++){
                  DinvFpdF(i,j,k,l) -= Fpinv(i,p)*dFpdF(p,q,k,l)*Fpinv(q,j);
                }
              }
            }
          }
        }
      }
    }

    STensorOperation::zero(dFedF);
    STensorOperation::zero(dFedT);
    for (int i=0; i<3; i++){
      for (int j=0; j<3; j++){
        for (int k=0; k<3; k++){
          dFedF(i,j,i,k) += Fpinv(k,j);
          dFedT(i,j) += F(i,k)*DinvFpdT(k,j);
          if (Deps> 0){
            for (int l=0; l<3; l++){
              for (int p=0; p<3; p++){
                dFedF(i,j,k,l) += F(i,p)*DinvFpdF(p,j,k,l);
              }
            }
          }
        }
      }
    }
    static STensor63 DLDF;
    static STensor43 DLDT;
    STensorOperation::zero(DLDF);
    STensorOperation::zero(DLDT);
    for (int i=0; i<3; i++){
      for (int j=0; j<3; j++){
        for (int k=0; k<3; k++){
          for (int l=0; l<3; l++){
            for (int r=0; r<3; r++){
              for (int s=0; s<3; s++){
                for (int a=0; a<3; a++){
                  DLDT(i,j,k,l) += dLDCe(i,j,k,l,r,s)*2.*Fe(a,r)*dFedT(a,s);
                  for (int p=0; p<3; p++){
                    for (int q=0; q<3; q++){
                      DLDF(i,j,k,l,p,q) += dLDCe(i,j,k,l,r,s)*2.*Fe(a,r)*dFedF(a,s,p,q);
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
    //

    static STensor43 DSDF; // S = corKir:L
    static STensor3 DSDT;
    STensorOperation::zero(DSDF);
    STensorOperation::zero(DSDT);
    for (int i=0; i<3; i++){
      for (int j=0; j<3; j++){
        for (int r=0; r<3; r++){
          for (int s=0; s<3; s++){
            DSDT(i,j) += DcorKirDT(r,s)*L(r,s,i,j) + corKir(r,s)*DLDT(r,s,i,j);
            for (int k=0; k<3; k++){
              for (int l=0; l<3; l++){
                DSDF(i,j,k,l) += DcorKirDF(r,s,k,l)*L(r,s,i,j) + corKir(r,s)*DLDF(r,s,i,j,k,l);
              }
            }
          }
        }
      }
    }


    // compute mechanical tengent
    STensorOperation::zero(Tangent);
    STensorOperation::zero(dPdT);
    for (int i=0; i<3; i++){
      for (int j=0; j<3; j++){
        for (int k=0; k<3; k++){
          for (int l=0; l<3; l++){
            dPdT(i,j) += (dFedT(i,k)*S(k,l)*Fpinv(j,l) + Fe(i,k)*DSDT(k,l)*Fpinv(j,l)+Fe(i,k)*S(k,l)*DinvFpdT(j,l));
            for (int p=0; p<3; p++){
              for (int q=0; q<3; q++){
                Tangent(i,j,p,q) += (dFedF(i,k,p,q)*S(k,l)*Fpinv(j,l) + Fe(i,k)*DSDF(k,l,p,q)*Fpinv(j,l)+Fe(i,k)*S(k,l)*DinvFpdF(j,l,p,q));
              }
            }
          }
        }
      }
    }
    //

    // fluxT
    dfluxTdT = fluxT;
    dfluxTdT *= (DconductDT/conductT);
    dfluxTdgradT = Cinv;
    dfluxTdgradT *= (-conductT*J);
    STensorOperation::zero(dfluxTdF);

    if (!_thermalEstimationPreviousConfig){
      static const STensor43 I4(1.,1.);
      static STensor3 DJDF;
      static STensor43 DCinvDF;
      for (int i=0; i<3; i++){
        for (int j=0; j<3; j++){
          DJDF(i,j) = J*Finv(j,i);
          for (int k=0; k<3; k++){
            for (int l=0; l<3; l++){
              DCinvDF(i,j,k,l) = 0.;
              for (int p=0; p<3; p++){
                for (int a=0; a<3; a++){
                  for (int b=0; b<3; b++){
                    DCinvDF(i,j,k,l) -= 2.*F(k,p)*Cinv(i,a)*Cinv(j,b)*I4(a,b,p,l);
                  }
                }
              }
            }
          }
        }
      }

      for (int i=0; i<3; i++){
        for (int j=0; j<3; j++){
          for (int k=0; k<3; k++){
            dfluxTdF(i,j,k) += (DJDF(j,k)*fluxT(i)/J);
            for (int l=0; l<3; l++){
              dfluxTdF(i,j,k) -= (J*DCinvDF(i,l,j,k)*gradT(l)*conductT);
            }
          }
        };
      }
    }


    // thermal source
    if (this->getTimeStep() > 0){
      dthermalSourcedT =-cpT/this->getTimeStep()-DcpdT*(T-T0)/this->getTimeStep();
    }
    else{
      dthermalSourcedT = 0.;
    }
    STensorOperation::zero(dthermalSourcedF);

    // mechanical source
    dmechanicalSourcedT = _TaylorQuineyFactor*dPlasticPowerDT;
    dmechanicalSourceF = dPlasticPowerDF;
    dmechanicalSourceF*=_TaylorQuineyFactor;
  }
};
