//
// C++ Interface: material law
//
// Author:  <Van Dung Nguyen>, (C) 2014
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef MLAWHYPERELASTIC_H_
#define MLAWHYPERELASTIC_H_

#include "mlaw.h"
#include "STensor3.h"
#include "STensor43.h"
#include "STensor63.h"
#include "ipHyperelastic.h"
#include "viscosityLaw.h"
#include "mlawHyperelasticFailureCriterion.h"


class mlawHyperViscoElastic : public materialLaw{
  public:
    enum viscoelasticType{Maxwell=0,KelvinVoight=1};
    enum extraBranchType{Bilogarithmic=0};
    enum extraBranchNLType{expType=0,tanhType=1,sigmoidType=2,hyperType=3,TensionCompressionRegularisedType=4,hyper_exp_TCasymm_Type=5,hyper_TCasymm_Type2=6};
  #ifndef SWIG
  protected:
    STensor43 _I4; // symetric 4th order unit tensor
    STensor3  _I; // second order tensor
    STensor43 _Idev; // dev part of _I4
    double _rho; // density
    double _mu; // 2nd lame parameter (=G)
    double _K; // bulk modulus

    STensor43 Cel; // elastic hook tensor
    int _order ; // to approximated log and exp of a tensor

    double _perturbationfactor; // perturbation factor
    bool _tangentByPerturbation; // flag for tangent by perturbation

     // viscoelastic data
    viscoelasticType _viscoMethod; // viscoelastic method 0- generalized Maxwell model, 1- generalized Kelvin-Voight model
    int _N; // number of elements
    std::vector<double> _Ki;
    std::vector<double> _ki;
    std::vector<double> _Gi;
    std::vector<double> _gi;
    
    // extraBranch parameters
    double _volCorrection; //Vc correction on volume potential
    double _xivolCorrection; //Xi correction on volume potential
    double _zetavolCorrection; //Zeta correction on volume potential
    
    double _devCorrection; //Dc correction on volume potential
    double _thetadevCorrection; //Theta correction on volume potential
    double _pidevCorrection; //pi correction on volume potential
    double _compCorrection; // compression correction 
    double _compCorrection_2; // compression correction 
    double _compCorrection_3; // compression correction 
    double _tensionCompressionRegularisation; // regularisation parameter for tension-compression asymmetry in extrabranch
    extraBranchType _extraBranchType;
    extraBranchNLType _extraBranchNLType;

    double _volCorrection2; //Vc correction on volume potential
    double _xivolCorrection2; //Xi correction on volume potential
    double _zetavolCorrection2; //Zeta correction on volume potential
    double _devCorrection2; //Dc correction on volume potential
    double _thetadevCorrection2; //Theta correction on volume potential
    double _pidevCorrection2; //pi correction on volume potential


  protected:
    virtual double deformationEnergy(const IPHyperViscoElastic& q) const ;
    virtual void dDeformationEnergydF(const IPHyperViscoElastic& q,  const STensor3 &P, STensor3 &dElasticEnergydF) const ;
    virtual double viscousEnergy(const IPHyperViscoElastic& q0, const IPHyperViscoElastic& q) const ;
    virtual void dViscousEnergydF(const IPHyperViscoElastic& q0, const IPHyperViscoElastic& q,  const STensor43 &dlnCdC, const STensor3 &Fe, STensor3 &dViscousEnergydF) const ;
    void updateViscoElasticFlow(const IPHyperViscoElastic *q0, IPHyperViscoElastic *q1, double& Ke, double& Ge) const;

    void viscoElasticPredictor(const STensor3& Ee, const STensor3& Ee0,
              const IPHyperViscoElastic *q0, IPHyperViscoElastic *q1,
              double& Ke, double& Ge) const;
    void extraBranchLaw(const STensor3& Ee, const IPHyperViscoElastic *q0, const IPHyperViscoElastic *q1, 
                        STensor3& sig, bool stiff=false, STensor43* DsigDEe=NULL) const;
    
    void isotropicHookTensor(const double K, const double G, STensor43& L) const;

    void predictorCorrector_ViscoElastic(const STensor3& F0, const STensor3& F, STensor3&P, const IPHyperViscoElastic *q0_, IPHyperViscoElastic *q1,
                                  const bool stiff, STensor43& Tangent) const;

    void evaluatePhiPCorrection(double trEe, const STensor3 &devEpr,  double &A_v, double &dAprdEepr, double &intA, double &B_d, STensor3 &dB_vddevEe, double &intB, 
                                double* dB_dTrEe = NULL, double* psiInfCorrector = NULL, double* DintA = NULL, double* DintB = NULL) const;
    //void evaluateA_dA(double trEe, double &A_v, double &dAprdEepr) const;
    
  #endif // SWIG

  public:
    mlawHyperViscoElastic(const int num,const double E,const double nu, const double rho,
                        const bool matrixbyPerturbation = false, const double pert = 1e-8);
    virtual void setStrainOrder(const int order);
    virtual int getOrder() const {return _order;}
    virtual void setViscoelasticMethod(const int method);
    virtual void setViscoElasticNumberOfElement(const int N);
    virtual void setViscoElasticData(const int i, const double Ei, const double taui);
    virtual void setViscoElasticData_Bulk(const int i, const double Ki, const double ki);
    virtual void setViscoElasticData_Shear(const int i, const double Gi, const double gi);
    virtual void setViscoElasticData(const std::string filename);
    virtual void setVolumeCorrection(double _vc, double _xivc, double _zetavc, double _dc, double _thetadc, double _pidc) {_volCorrection=_vc,_xivolCorrection=_xivc,_zetavolCorrection=_zetavc, _devCorrection=_dc,_thetadevCorrection=_thetadc,_pidevCorrection=_pidc;};
    virtual double getVolumeCorrection() const {return _volCorrection;};
    virtual double getXiVolumeCorrection() const {return _xivolCorrection;};
    virtual double getZetaVolumeCorrection() const {return _zetavolCorrection;};
    
    virtual double getDevCorrection() const {return _devCorrection;};
    virtual double getThetaDevCorrection() const {return _thetadevCorrection;};
    virtual double getPiDevCorrection() const {return _pidevCorrection;};
    virtual void setExtraBranch_CompressionParameter(const double compCorrection, const double compCorrection_2, const double compCorrection_3) {
                                    _compCorrection = compCorrection; _compCorrection_2 = compCorrection_2; _compCorrection_3 = compCorrection_3;};

    virtual void setAdditionalVolumeCorrections(const double V3, const double V4, const double V5, const double D3, const double D4, const double D5) {
    								_volCorrection2 = V3; _xivolCorrection2 = V4; _zetavolCorrection2 = V5; _devCorrection2 = D3; _thetadevCorrection2 = D4; _pidevCorrection2 = D5;};
    virtual void setTensionCompressionRegularisation(double tensionCompressionRegularisation) {_tensionCompressionRegularisation = tensionCompressionRegularisation;};
    virtual void setExtraBranchType(const int type);
    virtual void setExtraBranchNLType(const int type);

    #ifndef SWIG
    mlawHyperViscoElastic(const mlawHyperViscoElastic& src);
    mlawHyperViscoElastic& operator=(const materialLaw& source);
    virtual ~mlawHyperViscoElastic(){};

    virtual matname getType() const{return materialLaw::hyperviscoelastic;}
    virtual void createIPState(IPStateBase* &ips, bool hasBodyForce, const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const IntPt *GP=NULL, const int gpt = 0) const{
        Msg::Error("mlawHyperViscoElastic::createIPState has not been defined");
    };
    virtual void initLaws(const std::map<int,materialLaw*> &maplaw){}; // this law is initialized so nothing to do

    virtual bool withEnergyDissipation() const {if (_N == 0) return false; else return true;};
    virtual int getViscoElasticNumberOfElement() const{return _N;};

    virtual double soundSpeed() const; // default but you can redefine it for your case
    virtual double density()const{return _rho;}
    virtual const double bulkModulus() const{return _K;}
    virtual const double shearModulus() const{return _mu;}
    virtual const double poissonRatio() const{return  (3.*_K-2.*_mu)/2./(3.*_K+_mu);}
    
    virtual double getUpdatedBulkModulus(const IPHyperViscoElastic *q1) const { return _K*q1->getConstRefToElasticBulkPropertyScaleFactor(); } 
    virtual double getUpdatedShearModulus(const IPHyperViscoElastic *q1) const { return _mu*q1->getConstRefToElasticShearPropertyScaleFactor(); } 

    virtual double scaleFactor() const { return _mu;};

    virtual void constitutive(
            const STensor3& F0,         // initial deformation gradient (input @ time n)
            const STensor3& Fn,         // updated deformation gradient (input @ time n+1)
            STensor3 &P,                // updated 1st Piola-Kirchhoff stress tensor (output)
            const IPVariable *q0,       // array of initial internal variable
            IPVariable *q1,             // updated array of internal variable (in ipvcur on output),
            STensor43 &Tangent,         // constitutive tangents (output)
            const bool stiff,          // if true compute the tangents
            STensor43* elasticTangent = NULL,
            const bool dTangent =false,
            STensor63* dCalgdeps = NULL) const;


    virtual materialLaw* clone() const {return new mlawHyperViscoElastic(*this);};
    virtual void checkInternalState(IPVariable* ipv, const IPVariable* ipvprev) const{}; // do nothing
    #endif // SWIG
};

// general interface for yield surface f = a2 sigVM^n - (a1*p+a0)
// non-associated flow wit f = sigVM^2 + b* p^2
// associated flow
class mlawPowerYieldHyper : public mlawHyperViscoElastic{
  protected:
    double _tol; // for plasticity convergence

    // rate effects
    double _p; // exponent in rate
    viscosityLaw* _viscosity;

    // hardening data
    J2IsotropicHardening* _compression;
    J2IsotropicHardening* _traction;
    kinematicHardening* _kinematic;

    // yield surface option
    double _n;
    double _b; // flow coefficient g = sigVM^2+ bp^2
    bool _nonAssociatedFlow;

  protected:

    virtual void hardening(const IPHyperViscoElastoPlastic* q0, IPHyperViscoElastoPlastic* q) const;

     virtual void updateEqPlasticDeformation(IPHyperViscoElastoPlastic *q1, const IPHyperViscoElastoPlastic *q0,
                                            const double& nup, const double& Dgamma) const;

    virtual void getYieldCoefficients(const IPHyperViscoElastoPlastic *q, fullVector<double>& coeffs) const;
    virtual void getYieldCoefficientDerivatives(const IPHyperViscoElastoPlastic *q, const double& nup, fullVector<double>& Dcoeffs) const;
    
    void extraBranchCorrector(double Gt, double Kt, const STensor3& Eepr, const STensor3& sigExtraPr,
                              const STensor3& devPhipr, double ptildepr, const IPHyperViscoElastic *q0, const IPHyperViscoElastic *q1, 
                              double Gamma, const STensor43& DdevPhiprDEepr, const STensor3& DptildeprDEepr, STensor3& sigExtra, 
                              bool stiff, STensor3& DsigExtraDGamma, STensor43& DsigExtraDEepr) const;

  #ifndef SWIG
  protected:
    virtual void dDeformationEnergydF(const IPHyperViscoElastoPlastic& q,  const STensor3 &P, STensor3 &dElasticEnergydF, const STensor3 &Fp, const STensor43 &dFedF) const ;
    virtual void dViscousEnergydF(const IPHyperViscoElastoPlastic& q0,  const IPHyperViscoElastoPlastic& q,  const STensor43 &dlnCdC, const STensor3 &Fe, STensor3 &dViscousEnergydF, const STensor43 &dFedF) const;
    virtual void dPlasticEnergydF(const STensor3 &N, double Gamma, double Dgamma, const STensor43 &dKcorDcepr, const STensor3 &DGDCepr,
       const STensor43 &DdevNDCepr, const STensor3 &DtrNDCepr, const STensor3 &KS, const STensor43 &CeprToF, STensor3 &dPlasticEnergydF) const;

    virtual void predictorCorrector_nonAssociatedFlow(const STensor3& F, const IPHyperViscoElastoPlastic *q0_, IPHyperViscoElastoPlastic *q1,
                            STensor3&P, const bool stiff, STensor43& Tangent,  STensor43& dFedF, STensor43& dFpdF) const;

    virtual void predictorCorrector_associatedFlow(const STensor3& F, const IPHyperViscoElastoPlastic *q0_, IPHyperViscoElastoPlastic *q1,
                            STensor3&P, const bool stiff, STensor43& Tangent, STensor43& dFedF, STensor43& dFpdF) const;
   
  protected:


    virtual void predictorCorrector(const STensor3& F, const IPHyperViscoElastoPlastic *q0_, IPHyperViscoElastoPlastic *q1,
                            STensor3&P, const bool stiff, STensor43& Tangent, STensor43& dFedF, STensor43& dFpdF,
                            STensor43* elasticTangent) const;
    virtual void tangent_full_perturbation( // compute tangent by perturbation
                             STensor43 &T, // current tangent
                             STensor43& dFedF,
                             STensor43& dFpdF,
                             const STensor3 &P, // current PK stress
                             const STensor3 &F, // current deformation gradient
                             const IPHyperViscoElastoPlastic* q0, // previous internal variables
                             IPHyperViscoElastoPlastic* q1 // current internal variables
                           ) const;
                           
                           

  #endif // SWIG
  public:
    mlawPowerYieldHyper(const int num,const double E,const double nu, const double rho,
                        const double tol = 1.e-6,
                        const bool matrixbyPerturbation = false, const double pert = 1e-8);

    void setPowerFactor(const double n);
    void nonAssociatedFlowRuleFactor(const double b);
    void setPlasticPoissonRatio(const double nup);
    void setNonAssociatedFlow(const bool flag);

    void setCompressionHardening(const J2IsotropicHardening& comp);
    void setTractionHardening(const J2IsotropicHardening& trac);
    void setKinematicHardening(const kinematicHardening& kin);
    void setViscosityEffect(const viscosityLaw& v, const double p);

    #ifndef SWIG
    mlawPowerYieldHyper(const mlawPowerYieldHyper& src);
    mlawPowerYieldHyper& operator=(const materialLaw& source);
    virtual ~mlawPowerYieldHyper();

    virtual materialLaw* clone() const{return new mlawPowerYieldHyper(*this);};
    virtual bool withEnergyDissipation() const {return true;};

    virtual J2IsotropicHardening* getConstRefToCompressionHardening() const {return _compression;};
    virtual J2IsotropicHardening* getConstRefToTractionHardening() const{return _traction;};
    virtual kinematicHardening* getConstRefToKinematicHardening() const{return _kinematic;};

    virtual matname getType() const{return materialLaw::powerYieldLaw;}
    virtual void createIPState(IPStateBase* &ips,bool hasBodyForce, const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const IntPt *GP=NULL, const int gpt = 0) const;
    virtual void initLaws(const std::map<int,materialLaw*> &maplaw){}; // this law is initialized so nothing to do

    virtual void constitutive(
            const STensor3& F0,         // initial deformation gradient (input @ time n)
            const STensor3& Fn,         // updated deformation gradient (input @ time n+1)
            STensor3 &P,                // updated 1st Piola-Kirchhoff stress tensor (output)
            const IPVariable *q0,       // array of initial internal variable
            IPVariable *q1,             // updated array of internal variable (in ipvcur on output),
            STensor43 &Tangent,         // constitutive tangents (output)
            const bool stiff,          // if true compute the tangents
            STensor43* elasticTangent = NULL,
            const bool dTangent =false,
            STensor63* dCalgdeps = NULL) const;

    virtual void checkInternalState(IPVariable* ipv, const IPVariable* ipvprev) const{}; // do nothing
    #endif // SWIG
};

class mlawPowerYieldHyperWithFailure : public mlawPowerYieldHyper{
  protected:
    #ifndef SWIG
    mlawHyperelasticFailureCriterionBase* _failureCriterion;
  protected:
    virtual void predictorCorrector(const STensor3& F, const IPHyperViscoElastoPlastic *q0_, IPHyperViscoElastoPlastic *q1,
                            STensor3&P, const bool stiff, STensor43& Tangent, STensor43& dFedF, STensor43& dFpdF,
                            STensor43* elasticTangent) const;
    #endif // SWIG
  public:
    mlawPowerYieldHyperWithFailure(const int num,const double E,const double nu, const double rho,
                        const double tol = 1.e-6,
                        const bool matrixbyPerturbation = false, const double pert = 1e-8);


    void setFailureCriterion(const mlawHyperelasticFailureCriterionBase& fCr);

  #ifndef SWIG
    mlawPowerYieldHyperWithFailure(const mlawPowerYieldHyperWithFailure& src);
    virtual ~mlawPowerYieldHyperWithFailure();

    virtual materialLaw* clone() const{return new mlawPowerYieldHyperWithFailure(*this);};
    virtual void checkInternalState(IPVariable* ipv, const IPVariable* ipvprev) const;
    virtual matname getType() const{return materialLaw::powerYieldLawWithFailure;}
    virtual void initLaws(const std::map<int,materialLaw*> &maplaw){}; // this law is initialized so nothing to do

  #endif // SWIG

};
#endif // MLAWHYPERELASTIC_H_
