//
// C++ Interface: J2 material in small strains
//
//
// Author:  <Van Dung NGUYEN>, (C) 2016
//
// Copyright: See COPYING file that comes with this distribution
//
//

#include "mlawJ2VMSmallStrain.h"

#include <iostream>
using namespace std;

mlawJ2VMSmallStrain::mlawJ2VMSmallStrain(const int num, const double E, const double nu,const double rho,
                               const double sy0,const double h,const double tol,
                               const bool matrixbyPerturbation, const double pert, const bool init):
                               materialLaw(num,init),_tol(tol),_perturbationfactor(pert), _rho(rho),
                               _tangentByPerturbation(matrixbyPerturbation), _E(E),_nu(nu){
  _j2IH = new LinearExponentialJ2IsotropicHardening(num, sy0, h, 0., 10.);
  _K = E/(3.*(1.-2.*nu));
  _mu = E/(2.*(1.+nu));
  _lambda = (E*nu)/(1.+nu)/(1.-2.*nu);

  STensor3 I(1.);
  for (int i=0; i<3; i++){
    for (int j=0; j<3; j++){
      for (int k=0; k<3; k++){
        for (int l=0; l<3; l++){
          _I4(i,j,k,l) = 0.5*(I(i,k)*I(j,l)+I(i,l)*I(j,k));
          _I4dev(i,j,k,l) = 0.5*(I(i,k)*I(j,l)+I(i,l)*I(j,k)) - I(i,j)*I(k,l)/3.;
        }
      }
    }
  }


  STensorOperation::zero(_Cel);
  _Cel(0,0,0,0) = _lambda + 2.*_mu;
  _Cel(1,1,0,0) = _lambda;
  _Cel(2,2,0,0) = _lambda;
  _Cel(0,0,1,1) = _lambda;
  _Cel(1,1,1,1) = _lambda + 2.*_mu;
  _Cel(2,2,1,1) = _lambda;
  _Cel(0,0,2,2) = _lambda;
  _Cel(1,1,2,2) = _lambda;
  _Cel(2,2,2,2) = _lambda + 2.*_mu;

  _Cel(1,0,1,0) = _mu;
  _Cel(2,0,2,0) = _mu;
  _Cel(0,1,0,1) = _mu;
  _Cel(2,1,2,1) = _mu;
  _Cel(0,2,0,2) = _mu;
  _Cel(1,2,1,2) = _mu;

  _Cel(0,1,1,0) = _mu;
  _Cel(0,2,2,0) = _mu;
  _Cel(1,0,0,1) = _mu;
  _Cel(1,2,2,1) = _mu;
  _Cel(2,0,0,2) = _mu;
  _Cel(2,1,1,2) = _mu;
};

mlawJ2VMSmallStrain::mlawJ2VMSmallStrain(const int num, const double E, const double nu,const double rho,
                               const J2IsotropicHardening& j2IH, const double tol,
                               const bool matrixbyPerturbation, const double pert, const bool init):
                               materialLaw(num,init),_tol(tol),_perturbationfactor(pert), _rho(rho),
                               _tangentByPerturbation(matrixbyPerturbation), _E(E),_nu(nu){
  _j2IH = j2IH.clone();
  _K = E/(3.*(1.-2.*nu));
  _mu = E/(2.*(1.+nu));
  _lambda = (E*nu)/(1.+nu)/(1.-2.*nu);

  STensor3 I(1.);
  for (int i=0; i<3; i++){
    for (int j=0; j<3; j++){
      for (int k=0; k<3; k++){
        for (int l=0; l<3; l++){
          _I4dev(i,j,k,l) = 0.5*(I(i,k)*I(j,l)+I(i,l)*I(j,k)) - I(i,j)*I(k,l)/3.;
        }
      }
    }
  }

  STensorOperation::zero(_Cel);
  _Cel(0,0,0,0) = _lambda + 2.*_mu;
  _Cel(1,1,0,0) = _lambda;
  _Cel(2,2,0,0) = _lambda;
  _Cel(0,0,1,1) = _lambda;
  _Cel(1,1,1,1) = _lambda + 2.*_mu;
  _Cel(2,2,1,1) = _lambda;
  _Cel(0,0,2,2) = _lambda;
  _Cel(1,1,2,2) = _lambda;
  _Cel(2,2,2,2) = _lambda + 2.*_mu;

  _Cel(1,0,1,0) = _mu;
  _Cel(2,0,2,0) = _mu;
  _Cel(0,1,0,1) = _mu;
  _Cel(2,1,2,1) = _mu;
  _Cel(0,2,0,2) = _mu;
  _Cel(1,2,1,2) = _mu;

  _Cel(0,1,1,0) = _mu;
  _Cel(0,2,2,0) = _mu;
  _Cel(1,0,0,1) = _mu;
  _Cel(1,2,2,1) = _mu;
  _Cel(2,0,0,2) = _mu;
  _Cel(2,1,1,2) = _mu;
};



mlawJ2VMSmallStrain::mlawJ2VMSmallStrain(const mlawJ2VMSmallStrain& src):materialLaw(src){
  _j2IH = NULL;
  if (src._j2IH != NULL)
    _j2IH = src._j2IH->clone();
  _rho = src._rho;
  _K = src._K;
  _mu = src._mu;
  _E = src._E;
  _nu = src._nu;
  _lambda = src._lambda;
  _tol = src._tol; // tolerace of plastic corrector
  _perturbationfactor = src._perturbationfactor; // perturbation factor
  _tangentByPerturbation = src._tangentByPerturbation; // flag for tangent by perturbation
  _I4dev = src._I4dev;
  _Cel = src._Cel;
};

mlawJ2VMSmallStrain::~mlawJ2VMSmallStrain(){
  if (_j2IH) delete _j2IH; _j2IH = NULL;
};

void mlawJ2VMSmallStrain::createIPState(IPStateBase* &ips,  bool hasBodyForce, const bool* state_,const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
  IPVariable* ipvi = new IPJ2linear(_j2IH);
  IPVariable* ipv1 = new IPJ2linear(_j2IH);
  IPVariable* ipv2 = new IPJ2linear(_j2IH);
  if(ips != NULL) delete ips;
  ips = new IP3State(state_,ipvi,ipv1,ipv2);
};

double mlawJ2VMSmallStrain::soundSpeed() const
{
  double factornu = (1.-_nu)/((1.+_nu)*(1.-2.*_nu));
  return sqrt(_E*factornu/_rho);
};

void mlawJ2VMSmallStrain::constitutive(
                            const STensor3& F0,         // previous deformation gradient (input @ time n)
                            const STensor3& Fn,         // current deformation gradient (input @ time n+1)
                            STensor3 &P,                // current 1st Piola-Kirchhoff stress tensor (output)
                            const IPVariable *q0i,       // array of previous internal variables
                            IPVariable *q1i,             // current array of internal variable (in ipvcur on output),
                            STensor43 &Tangent,         // tangents (output)
                            const bool stiff,            // if true compute the tangents
                            STensor43* elasticTangent,
                            const bool dTangent,
                            STensor63* dCalgdeps) const{

  static STensor43 depsEldF;
  static STensor3 dpdF;
  IPJ2linear* q1 = static_cast<IPJ2linear*>(q1i);
  const IPJ2linear* q0 = static_cast<const IPJ2linear*>(q0i);

  predictorCorector(F0,Fn,P,q0,q1,Tangent,depsEldF,dpdF,stiff,elasticTangent,dTangent,dCalgdeps);
};

void mlawJ2VMSmallStrain::constitutiveTFA(
                            const STensor3& F0,         // previous deformation gradient (input @ time n)
                            const STensor3& Fn,         // current deformation gradient (input @ time n+1)
                            STensor3 &P,                // current 1st Piola-Kirchhoff stress tensor (output)
                            const IPVariable *q0i,       // array of previous internal variables
                            IPVariable *q1i,             // current array of internal variable (in ipvcur on output),
                            STensor43 &Tangent,         // tangents (output)
                            STensor3 &epsEig,
                            STensor43 &depsEigdeps) const{

  IPJ2linear* q1 = static_cast<IPJ2linear*>(q1i);
  const IPJ2linear* q0 = static_cast<const IPJ2linear*>(q0i);
  predictorCorectorTFA(F0,Fn,P,q0,q1,Tangent,epsEig,depsEigdeps);
};

void mlawJ2VMSmallStrain::constitutiveTFA_corr(
                            const STensor3& F0,         // previous deformation gradient (input @ time n)
                            const STensor3& Fn,         // current deformation gradient (input @ time n+1)
                            const STensor3& Fn_min,         // current deformation gradient (input @ time n+1)
                            const STensor3& Fn_max,         // current deformation gradient (input @ time n+1)
                            STensor3 &P,                // current 1st Piola-Kirchhoff stress tensor (output)
                            const IPVariable *q0i,       // array of previous internal variables
                            IPVariable *q1i,             // current array of internal variable (in ipvcur on output),
                            STensor43 &Tangent,         // tangents (output)
                            STensor3 &epsEig,
                            STensor43 &depsEigdeps) const{

  IPJ2linear* q1 = static_cast<IPJ2linear*>(q1i);
  const IPJ2linear* q0 = static_cast<const IPJ2linear*>(q0i);
  predictorCorectorTFA_corr(F0,Fn,Fn_min,Fn_max,P,q0,q1,Tangent,epsEig,depsEigdeps);
};

void mlawJ2VMSmallStrain::constitutiveIsotropicTangentTFA(
                            const STensor3& F0,         // previous deformation gradient (input @ time n)
                            const STensor3& Fn,         // current deformation gradient (input @ time n+1)
                            STensor3 &P,                // current 1st Piola-Kirchhoff stress tensor (output)
                            const IPVariable *q0i,       // array of previous internal variables
                            IPVariable *q1i,             // current array of internal variable (in ipvcur on output),
                            STensor43 &Tangent,         // tangents (output)
                            STensor3 &epsEig,
                            STensor43 &depsEigdeps) const{

  IPJ2linear* q1 = static_cast<IPJ2linear*>(q1i);
  const IPJ2linear* q0 = static_cast<const IPJ2linear*>(q0i);
  predictorCorectorIsotropicTangentTFA(F0,Fn,P,q0,q1,Tangent,epsEig,depsEigdeps);
};


void mlawJ2VMSmallStrain::constitutiveResidualSecantTFA(
                            const STensor3& F0,         // previous deformation gradient (input @ time n)
                            const STensor3& Fn,         // current deformation gradient (input @ time n+1)
                            STensor3 &P,                // current 1st Piola-Kirchhoff stress tensor (output)
                            const IPVariable *q_unloadedi,       // array of previous internal variables
                            IPVariable *q1i,             // current array of internal variable (in ipvcur on output),
                            STensor43 &Secant,         // tangents (output)
                            STensor3 &epsEig,
                            STensor43 &depsEigdeps,
                            const bool tangent_compute,
                            STensor43* Tangent) const{

  IPJ2linear* q1 = static_cast<IPJ2linear*>(q1i);
  const IPJ2linear* q_unloaded = static_cast<const IPJ2linear*>(q_unloadedi);
  predictorCorectorResidualSecantTFA(F0,Fn,P,q_unloaded,q1,Secant,epsEig,depsEigdeps,tangent_compute,Tangent);
};

void mlawJ2VMSmallStrain::constitutiveResidualSecant(
                            const STensor3& F0,         // previous deformation gradient (input @ time n)
                            const STensor3& Fn,         // current deformation gradient (input @ time n+1)
                            STensor3 &P,                // current 1st Piola-Kirchhoff stress tensor (output)
                            const IPVariable *q_unloadedi,       // array of previous internal variables
                            IPVariable *q1i,             // current array of internal variable (in ipvcur on output),
                            STensor43 &Secant,         // tangents (output)
                            const bool stiff,            // if true compute the tangents
                            STensor43* elasticTangent,
                            const bool dSecant,
                            STensor63* dCsecdeps,
                            const bool Calg_compute,
                            STensor43* C_algo) const{

  static STensor43 depsEldF;
  static STensor3 dpdF;
  IPJ2linear* q1 = static_cast<IPJ2linear*>(q1i);
  const IPJ2linear* q_unloaded = static_cast<const IPJ2linear*>(q_unloadedi);
  predictorCorectorResidualSecant(F0,Fn,P,q_unloaded,q1,Secant,depsEldF,dpdF,stiff,elasticTangent,dSecant,dCsecdeps,Calg_compute,C_algo);
};


void mlawJ2VMSmallStrain::constitutiveZeroSecant(
                            const STensor3& F0,         // previous deformation gradient (input @ time n)
                            const STensor3& Fn,         // current deformation gradient (input @ time n+1)
                            STensor3 &P,                // current 1st Piola-Kirchhoff stress tensor (output)
                            const IPVariable *q_unloadedi,       // array of previous internal variables
                            IPVariable *q1i,             // current array of internal variable (in ipvcur on output),
                            STensor43 &Secant,         // tangents (output)
                            const bool stiff,            // if true compute the tangents
                            STensor43* elasticTangent,
                            const bool dSecant,
                            STensor63* dCsecdeps,
                            const bool Calg_compute,
                            STensor43* C_algo) const{

  static STensor43 depsEldF;
  static STensor3 dpdF;
  IPJ2linear* q1 = static_cast<IPJ2linear*>(q1i);
  const IPJ2linear* q_unloaded = static_cast<const IPJ2linear*>(q_unloadedi);

  predictorCorectorZeroSecant(F0,Fn,P,q_unloaded,q1,Secant,depsEldF,dpdF,stiff,elasticTangent,dSecant,dCsecdeps,Calg_compute,C_algo);
};


void mlawJ2VMSmallStrain::constitutiveVirtualUnloading(
                            const STensor3& F0,         // previous deformation gradient (input @ time n)
                            const STensor3& Fn,         // current deformation gradient (input @ time n+1)
                            STensor3 &P,                // current 1st Piola-Kirchhoff stress tensor (output)
                            const IPVariable *q0i,       // array of previous internal variables
                            IPVariable *q_unloadedi) const{

  IPJ2linear* q_unloaded = static_cast<IPJ2linear*>(q_unloadedi);
  const IPJ2linear* q0 = static_cast<const IPJ2linear*>(q0i);
  elasticUnloading(F0,Fn,P,q0,q_unloaded);
};


void mlawJ2VMSmallStrain::predictorCorector(const STensor3& F0,         // initial deformation gradient (input @ time n)
                            const STensor3& Fn,         // updated deformation gradient (input @ time n+1)
                            STensor3 &P,                // updated 1st Piola-Kirchhoff stress tensor (output)
                            const IPJ2linear *q0,       // array of initial internal variable
                            IPJ2linear *q1,             // updated array of internal variable (in ipvcur on output),
                            STensor43& Tangent,         // mechanical tangents (output)
                            STensor43& depsEldF,
                            STensor3& dpdF,
                            const bool stiff,
                            STensor43* elasticTangent,
                            const bool dTangent,
                            STensor63* dCalgdeps) const {
  // same state
  // plastic strain
  STensor3& Ep = q1->_j2lepsp;
  double& gamma = q1->_j2lepspbarre;
  double& svm = q1->_svm;
  
  STensor3& sig = q1->_sig;
  
  double& eigenstress_eq = q1->_eigenstress_eq;
  eigenstress_eq = q0->_eigenstress_eq;
  STensor3& eigenstress = q1->_eigenstress;
  eigenstress = q0->_eigenstress;
  STensor3 eigenstress_dev;

  STensor3& eigenstrain = q1->_eigenstrain;

  const STensor3 &sig_0 = q0->getConstRefToStress();
  Ep = q0->_j2lepsp; // plastic strain tensor (equal I if no plastic occurs)
  gamma = q0->_j2lepspbarre; // equivalent plastic strain
  
  static const STensor3 I(1.); // identity tensor

  // small strain
  static STensor3 eps, devSigtr;
  for (int i=0; i<3; i++){
    for (int j=0; j<3; j++){
      eps(i,j) =   0.5*(Fn(i,j)+Fn(j,i)) - I(i,j);
    }
  }
  q1->_strain = eps;

  // trial elastic strain Eetr = eps-(Ep-I)
  static STensor3 Eetr, devEetr;
  Eetr = (eps);
  Eetr -= Ep;
  Eetr += I;

  // trace and dev parts
  double traceEetr = Eetr.trace();
  devEetr = Eetr.dev();

  // trial pressure and deviatoric stress
  double p = _K*traceEetr; // pressure
  devSigtr = (devEetr); // trial deviatoric stress
  devSigtr *= (2.*_mu);

  // stress
  P = devSigtr;
  P.daxpy(I,p);
  sig = P;
  // elastic strain
  q1->_Ee = Eetr;
  q1->_plasticPower = 0.;
  q1->_plasticEnergy = q0->_plasticEnergy;
  q1->_j2ldsy = 0.;

  if (stiff and !_tangentByPerturbation){
    Tangent = _Cel;
    STensorOperation::zero(dpdF);
    STensorOperation::zero(depsEldF);
  }
  
  if (elasticTangent != NULL){
    (*elasticTangent) =  _Cel;
  }

  if(dTangent){
    STensorOperation::zero(*dCalgdeps);
  }

  // VM stress
  //devSigtr = sig.dev();
  double sigVMtr = sqrt(1.5*devSigtr.dotprod());
  svm = sigVMtr;

  // get current yield stress
  _j2IH->hardening(q0->_j2lepspbarre, q0->getConstRefToIPJ2IsotropicHardening(), gamma, q1->getRefToIPJ2IsotropicHardening());
  double  Sy0   = q0->getConstRefToIPJ2IsotropicHardening().getR();
  double H    = q0->getConstRefToIPJ2IsotropicHardening().getDR();
  double ddR  = q0->getConstRefToIPJ2IsotropicHardening().getDDR();
  double intR = q0->getConstRefToIPJ2IsotropicHardening().getIntegR();

  double VMCriterion = sigVMtr -Sy0;
  if (VMCriterion >0.){
    // plastic occurs
    int ite =0, maxite = 1000;
    double deps = 0.;
    double Sy = Sy0;
    while (fabs(VMCriterion/_j2IH->getYield0())> _tol){
      // plastic increment
      deps += VMCriterion/(3.*_mu+H);
      // recompute plastic state
      _j2IH->hardening(q0->_j2lepspbarre, q0->getConstRefToIPJ2IsotropicHardening(), gamma+deps, q1->getRefToIPJ2IsotropicHardening());
      Sy   = q1->getConstRefToIPJ2IsotropicHardening().getR();
      H    = q1->getConstRefToIPJ2IsotropicHardening().getDR();
      ddR  = q1->getConstRefToIPJ2IsotropicHardening().getDDR();
      intR = q1->getConstRefToIPJ2IsotropicHardening().getIntegR();
      
      VMCriterion = sigVMtr - 3.*_mu*deps - Sy;
      ite ++;

      if(ite > maxite){
        Msg::Error("No convergence for plastic correction in j2linearVM small strain !!");
        break;
      }

    }
    gamma += deps;
    // update plastic strain
    static STensor3 N;
    N = (devSigtr);
    N*= (1.5/sigVMtr);

    Ep.daxpy(N,deps);
    q1->_Ee.daxpy(N,-deps);
    
    double fact = -2.*_mu*deps;
    P.daxpy(N,fact);
    sig = P;  
    svm = sigVMtr - 3.*_mu*deps;
    eigenstress += fact*N;
    eigenstress_dev = eigenstress.dev();
    eigenstress_eq = sqrt(1.5*eigenstress_dev.dotprod());
    
    q1->_plasticPower = (deps*Sy)/this->getTimeStep();
    q1->_plasticEnergy = q0->_plasticEnergy+(deps*Sy); // q1->getConstRefToIPJ2IsotropicHardening().getIntegR();
    //Msg::Info("%f,%f",intR, q0->_plasticEnergy+(deps*Sy));

    q1->_j2ldsy = Sy - Sy0;
    

    if (stiff and !_tangentByPerturbation){
      STensorOperation::multSTensor3STensor43(N,_Cel,dpdF);
      dpdF *= (1./(3.*_mu+H));
      
      
      static STensor43 dNdSigpr;
      for (int i=0; i<3; i++){
        for (int j=0; j<3; j++){
          for (int k=0; k<3; k++){
            for (int l=0; l<3; l++){
              dNdSigpr(i,j,k,l) = (1.5*_I4dev(i,j,k,l) - N(i,j)*N(k,l))/sigVMtr;
            }
          }
        }
      }
      
      STensorOperation::multSTensor43(dNdSigpr,_Cel,depsEldF);
      depsEldF *= (-deps);
      STensorOperation::prodAdd(N,dpdF,-1.,depsEldF);
      depsEldF += _I4;
      
      
      for (int i=0; i<3; i++){
        for (int j=0; j<3; j++){
          for (int k=0; k<3; k++){
            for (int l=0; l<3; l++){
              for (int p=0; p<3; p++){
                for (int q=0; q<3; q++){
                  for (int r=0; r<3; r++){
                    for (int s=0; s<3; s++){
                      Tangent(i,j,k,l) -= _Cel(i,j,r,s)*N(r,s)*N(p,q)*_Cel(p,q,k,l)/(3.*_mu+H) + _Cel(i,j,r,s)*deps*dNdSigpr(r,s,p,q)*_Cel(p,q,k,l);
                    }
                  }
                }
              }
            }
          }
        }
      }

      if (dTangent){
        double h;
        h = 3.*_mu+H;
        double fac1, fac2, fac3, fac4;
        fac1 = -1.5/h/sigVMtr*(2.*_mu)*(2.*_mu)*(2.*_mu);
        fac2 = (1./sigVMtr+ddR/h/h)*(2.*_mu)*(2.*_mu)*(2.*_mu)/h;
        fac3 = (deps/sigVMtr-1./h)*(2.*_mu)*(2.*_mu)*(2.*_mu)/sigVMtr;
        fac4 = (2.*_mu)*(2.*_mu)*(2.*_mu)*deps/sigVMtr/sigVMtr;
        
        for (int i=0; i<3; i++){
          for (int j=0; j<3; j++){
            for (int k=0; k<3; k++){
              for (int l=0; l<3; l++){
                for (int m=0; m<3; m++){
                  for (int n=0; n<3; n++){
                    (*dCalgdeps)(i,j,k,l,m,n) = fac1*_I4dev(i,j,k,l)*N(m,n) + fac2 * N(i,j)*N(k,l)*N(m,n) + fac3 * (1.5*(_I4dev(i,j,k,l)*N(m,n)            						+ N(i,j)*_I4dev(k,l,m,n))-2.*N(i,j)*N(k,l)*N(m,n));
                    				+ fac4*(1.5*N(i,j)*_I4dev(k,l,m,n) - N(i,j)*N(k,l)*N(m,n));
                  }
                }
              }
            }
          }
        }
      }
    }
  }
  
  eigenstrain = Ep;
  eigenstrain -= I;

  q1->_elasticEnergy = deformationEnergy(q1->_Ee);

  if (stiff and _tangentByPerturbation){
    static STensor43 Temp;
    static STensor3 Fpp, Pp;
    static IPJ2linear q11(*q1);
    for (int i=0; i<3; i++){
      for (int j=0; j<3; j++){
        Fpp = (Fn);
        Fpp(i,j)+=_perturbationfactor;
        this->constitutive(F0,Fpp,Pp,q0,&q11,Temp,false);
        for (int k=0; k<3; k++){
          for (int l=0; l<3; l++){
            Tangent(k,l,i,j) = (Pp(k,l)-P(k,l))/_perturbationfactor;
            depsEldF(k,l,i,j) = (q11._Ee(k,l) - q1->_Ee(k,l))/_perturbationfactor;
          }
        }
        
        dpdF(i,j) = (q11._j2lepspbarre - q1->_j2lepspbarre)/_perturbationfactor;
      }
    }
  }

};


void mlawJ2VMSmallStrain::predictorCorectorTFA(const STensor3& F0,         // initial deformation gradient (input @ time n)
                            const STensor3& Fn,         // updated deformation gradient (input @ time n+1)
                            STensor3 &P,                // updated 1st Piola-Kirchhoff stress tensor (output)
                            const IPJ2linear *q0,       // array of initial internal variable
                            IPJ2linear *q1,             // updated array of internal variable (in ipvcur on output),
                            STensor43& Tangent,         // mechanical tangents (output)
                            STensor3& epsEig,
                            STensor43& depsEigdeps
                            ) const {
  // same state
  // plastic strain
  STensor3& Ep = q1->_j2lepsp;
  double& gamma = q1->_j2lepspbarre;
  double& svm = q1->_svm;
  
  STensor3& sig = q1->_sig;
  
  double& eigenstress_eq = q1->_eigenstress_eq;
  eigenstress_eq = q0->_eigenstress_eq;
  STensor3& eigenstress = q1->_eigenstress;
  eigenstress = q0->_eigenstress;
  STensor3 eigenstress_dev;

  Ep = q0->_j2lepsp; // plastic strain tensor (equal I if no plastic occurs)
  gamma = q0->_j2lepspbarre; // equivalent plastic strain
  
  static const STensor3 I(1.); // identity tensor

  // small strain
  static STensor3 eps, eps0, devSigtr;
  for (int i=0; i<3; i++){
    for (int j=0; j<3; j++){
      eps(i,j) =   0.5*(Fn(i,j)+Fn(j,i)) - I(i,j);
      eps0(i,j) =  0.5*(F0(i,j)+F0(j,i)) - I(i,j);
    }
  }
  q1->_strain = eps;

  // trial elastic strain Eetr = eps-(Ep-I)
  static STensor3 Eetr, devEetr;
  Eetr = (eps);
  Eetr -= Ep;
  Eetr += I;

  // trace and dev parts
  double traceEetr = Eetr.trace();
  devEetr = Eetr.dev();

  // trial pressure and deviatoric stress
  double p = _K*traceEetr; // pressure
  devSigtr = (devEetr); // trial deviatoric stress
  devSigtr *= (2.*_mu);

  // stress
  P = devSigtr;
  P.daxpy(I,p);
  // elastic strain
  q1->_Ee = Eetr;
  q1->_plasticPower = 0.;
  q1->_plasticEnergy = q0->_plasticEnergy;
  q1->_j2ldsy = 0.;

  if (!_tangentByPerturbation){
    Tangent = _Cel;
  }

  epsEig = Ep;
  epsEig -= I;
  STensorOperation::zero(depsEigdeps);

  // VM stress
  //devSigtr = sig.dev();
  double sigVMtr = sqrt(1.5*devSigtr.dotprod());
  svm = sigVMtr;

  // get current yield stress
  _j2IH->hardening(q0->_j2lepspbarre, q0->getConstRefToIPJ2IsotropicHardening(), gamma, q1->getRefToIPJ2IsotropicHardening());
  double  Sy0   = q0->getConstRefToIPJ2IsotropicHardening().getR();
  double H    = q0->getConstRefToIPJ2IsotropicHardening().getDR();
  double ddR  = q0->getConstRefToIPJ2IsotropicHardening().getDDR();

  double VMCriterion = sigVMtr -Sy0;
  if (VMCriterion >0.){
    // plastic occurs
    int ite =0, maxite = 1000;
    double deps = 0.;
    double Sy = Sy0;
    while (fabs(VMCriterion/_j2IH->getYield0())> _tol){
      // plastic increment
      deps += VMCriterion/(3.*_mu+H);
      // recompute plastic state
      _j2IH->hardening(q0->_j2lepspbarre, q0->getConstRefToIPJ2IsotropicHardening(), gamma+deps, q1->getRefToIPJ2IsotropicHardening());
      Sy   = q1->getConstRefToIPJ2IsotropicHardening().getR();
      H    = q1->getConstRefToIPJ2IsotropicHardening().getDR();
      ddR  = q1->getConstRefToIPJ2IsotropicHardening().getDDR();
      
      VMCriterion = sigVMtr - 3.*_mu*deps - Sy;
      ite ++;

      if(ite > maxite){
        Msg::Error("No convergence for plastic correction in j2linearVM small strain !!");
        break;
      }

    }
    gamma += deps;
    // update plastic strain
    static STensor3 N;
    N = (devSigtr);
    N*= (1.5/sigVMtr);

    Ep.daxpy(N,deps);
    q1->_Ee.daxpy(N,-deps);
    
    double fact = -2.*_mu*deps;
    P.daxpy(N,fact);
    svm = sigVMtr - 3.*_mu*deps;
    eigenstress += fact*N;
    eigenstress_dev = eigenstress.dev();
    eigenstress_eq = sqrt(1.5*eigenstress_dev.dotprod());
    
    q1->_plasticPower = (deps*Sy)/this->getTimeStep();
    q1->_plasticEnergy += deps*Sy;
    q1->_j2ldsy = Sy - Sy0;
    
    epsEig = Ep;
    epsEig -= I;

    if (!_tangentByPerturbation){ 
      double h;
      h = 3.*_mu+H;  
      for (int i=0; i<3; i++){
        for (int j=0; j<3; j++){
          for (int k=0; k<3; k++){
            for (int l=0; l<3; l++){
              depsEigdeps(i,j,k,l) = 2.*_mu/h*N(i,j)*N(k,l) + 2.*_mu*deps/sigVMtr*(1.5*_I4dev(i,j,k,l)-N(i,j)*N(k,l));
              Tangent(i,j,k,l) -= 2.*_mu*depsEigdeps(i,j,k,l);
            }
          }
        }
      }    
    }
  }

  q1->_elasticEnergy = deformationEnergy(q1->_Ee);
  q1->_P = P;
  /*double detJ = STensorOperation::determinantSTensor3(Fn);
  for (int i = 0; i< 3; i ++){
    for (int j = 0; j< 3; j ++){
      sig(i,j) = 0.;
      for(int k =0; k <3; k++){
        sig(i,j) += P(i,k)*Fn(j,k)/detJ;
      }
    }
  }*/
  sig = P;
};


void mlawJ2VMSmallStrain::predictorCorectorTFA_corr(const STensor3& F0,         // initial deformation gradient (input @ time n)
                            const STensor3& Fn,         // updated deformation gradient (input @ time n+1)
                            const STensor3& Fn_min,
                            const STensor3& Fn_max,
                            STensor3 &P,                // updated 1st Piola-Kirchhoff stress tensor (output)
                            const IPJ2linear *q0,       // array of initial internal variable
                            IPJ2linear *q1,             // updated array of internal variable (in ipvcur on output),
                            STensor43& Tangent,         // mechanical tangents (output)
                            STensor3& epsEig,
                            STensor43& depsEigdeps
                            ) const {
  // same state
  // plastic strain
  STensor3& Ep = q1->_j2lepsp;
  double& gamma = q1->_j2lepspbarre;
  double& svm = q1->_svm;
  
  STensor3& sig = q1->_sig;
  
  double& eigenstress_eq = q1->_eigenstress_eq;
  eigenstress_eq = q0->_eigenstress_eq;
  STensor3& eigenstress = q1->_eigenstress;
  eigenstress = q0->_eigenstress;
  STensor3 eigenstress_dev;

  Ep = q0->_j2lepsp; // plastic strain tensor (equal I if no plastic occurs)
  gamma = q0->_j2lepspbarre; // equivalent plastic strain
  
  static const STensor3 I(1.); // identity tensor

  // small strain
  static STensor3 eps, eps_min, eps_max, eps0, devSigtr;
  for (int i=0; i<3; i++){
    for (int j=0; j<3; j++){
      eps(i,j) =   0.5*(Fn(i,j)+Fn(j,i)) - I(i,j);
      eps_min(i,j) =   0.5*(Fn_min(i,j)+Fn_min(j,i)) - I(i,j);
      eps_max(i,j) =   0.5*(Fn_max(i,j)+Fn_max(j,i)) - I(i,j);
      eps0(i,j) =  0.5*(F0(i,j)+F0(j,i)) - I(i,j);
    }
  }
  q1->_strain = eps;

  // trial elastic strain Eetr = eps-(Ep-I)
  static STensor3 Eetr, devEetr;
  Eetr = (eps);
  Eetr -= Ep;
  Eetr += I;

  // trace and dev parts
  double traceEetr = Eetr.trace();
  devEetr = Eetr.dev();

  // trial pressure and deviatoric stress
  double p = _K*traceEetr; // pressure
  devSigtr = (devEetr); // trial deviatoric stress
  devSigtr *= (2.*_mu);

  // stress
  P = devSigtr;
  P.daxpy(I,p);
  // elastic strain
  q1->_Ee = Eetr;
  q1->_plasticPower = 0.;
  q1->_plasticEnergy = q0->_plasticEnergy;
  q1->_j2ldsy = 0.;

  if (!_tangentByPerturbation){
    Tangent = _Cel;
  }

  epsEig = Ep;
  epsEig -= I;
  STensorOperation::zero(depsEigdeps);

  // VM stress
  //devSigtr = sig.dev();
  double sigVMtr = sqrt(1.5*devSigtr.dotprod());
  svm = sigVMtr;
  

  static STensor3 Eetr_min, devEetr_min,devSigtr_min;
  Eetr_min = (eps_min);
  Eetr_min -= Ep;
  Eetr_min += I;
  double traceEetr_min = Eetr_min.trace();
  devEetr_min = Eetr_min.dev();
  double p_min = _K*traceEetr_min; // pressure
  devSigtr_min = (devEetr_min); // trial deviatoric stress
  devSigtr_min *= (2.*_mu);
  double sigVMtr_min = sqrt(1.5*devSigtr_min.dotprod());
  
  static STensor3 Eetr_max, devEetr_max,devSigtr_max;
  Eetr_max = (eps_max);
  Eetr_max -= Ep;
  Eetr_max += I;
  double traceEetr_max = Eetr_max.trace();
  devEetr_max = Eetr_max.dev();
  double p_max = _K*traceEetr_max; // pressure
  devSigtr_max = (devEetr_max); // trial deviatoric stress
  devSigtr_max *= (2.*_mu);
  double sigVMtr_max = sqrt(1.5*devSigtr_max.dotprod());
  
  printf("sigVMtr %.16g,sigVMtrMin %.16g,sigVMtrMax %.16g \n",sigVMtr,sigVMtr_min,sigVMtr_max);

  // get current yield stress
  _j2IH->hardening(q0->_j2lepspbarre, q0->getConstRefToIPJ2IsotropicHardening(), gamma, q1->getRefToIPJ2IsotropicHardening());
  double  Sy0   = q0->getConstRefToIPJ2IsotropicHardening().getR();
  double H    = q0->getConstRefToIPJ2IsotropicHardening().getDR();
  double ddR  = q0->getConstRefToIPJ2IsotropicHardening().getDDR();

  double VMCriterion = sigVMtr -Sy0;
  double VMCriterion_min = sigVMtr_min -Sy0;
  double VMCriterion_max = sigVMtr_max -Sy0;
  if (VMCriterion_max > 0. or VMCriterion_min > 0.){
    // plastic occurs
    int ite =0, maxite = 1000;
    double deps_max = 0.;
    double Sy = Sy0;
    while (fabs(VMCriterion_max/_j2IH->getYield0())> _tol){
      // plastic increment
      deps_max += VMCriterion_max/(3.*_mu+H);
      // recompute plastic state
      _j2IH->hardening(q0->_j2lepspbarre, q0->getConstRefToIPJ2IsotropicHardening(), gamma+deps_max, q1->getRefToIPJ2IsotropicHardening());
      Sy   = q1->getConstRefToIPJ2IsotropicHardening().getR();
      H    = q1->getConstRefToIPJ2IsotropicHardening().getDR();
      ddR  = q1->getConstRefToIPJ2IsotropicHardening().getDDR();
      
      VMCriterion_max = sigVMtr_max - 3.*_mu*deps_max - Sy;
      ite ++;

      if(ite > maxite){
        Msg::Error("No convergence for plastic correction in j2linearVM small strain !!");
        break;
      }
    }
    
    _j2IH->hardening(q0->_j2lepspbarre, q0->getConstRefToIPJ2IsotropicHardening(), gamma, q1->getRefToIPJ2IsotropicHardening());
    Sy0   = q0->getConstRefToIPJ2IsotropicHardening().getR();
    H    = q0->getConstRefToIPJ2IsotropicHardening().getDR();
    ddR  = q0->getConstRefToIPJ2IsotropicHardening().getDDR();
    ite =0;
    double deps_min = 0.;
    Sy = Sy0;
    while (fabs(VMCriterion_min/_j2IH->getYield0())> _tol){
      // plastic increment
      deps_min += VMCriterion_min/(3.*_mu+H);
      // recompute plastic state
      _j2IH->hardening(q0->_j2lepspbarre, q0->getConstRefToIPJ2IsotropicHardening(), gamma+deps_min, q1->getRefToIPJ2IsotropicHardening());
      Sy   = q1->getConstRefToIPJ2IsotropicHardening().getR();
      H    = q1->getConstRefToIPJ2IsotropicHardening().getDR();
      ddR  = q1->getConstRefToIPJ2IsotropicHardening().getDDR();
      
      VMCriterion_min = sigVMtr_min - 3.*_mu*deps_min - Sy;
      ite ++;

      if(ite > maxite){
        Msg::Error("No convergence for plastic correction in j2linearVM small strain !!");
        break;
      }
    }
    
    _j2IH->hardening(q0->_j2lepspbarre, q0->getConstRefToIPJ2IsotropicHardening(), gamma, q1->getRefToIPJ2IsotropicHardening());
    Sy0   = q0->getConstRefToIPJ2IsotropicHardening().getR();
    H    = q0->getConstRefToIPJ2IsotropicHardening().getDR();
    ddR  = q0->getConstRefToIPJ2IsotropicHardening().getDDR();
    ite =0;
    double deps = 0.;
    Sy = Sy0;
    while (fabs(VMCriterion/_j2IH->getYield0())> _tol){
      // plastic increment
      deps += VMCriterion/(3.*_mu+H);
      // recompute plastic state
      _j2IH->hardening(q0->_j2lepspbarre, q0->getConstRefToIPJ2IsotropicHardening(), gamma+deps_min, q1->getRefToIPJ2IsotropicHardening());
      Sy   = q1->getConstRefToIPJ2IsotropicHardening().getR();
      H    = q1->getConstRefToIPJ2IsotropicHardening().getDR();
      ddR  = q1->getConstRefToIPJ2IsotropicHardening().getDDR();
      
      VMCriterion = sigVMtr - 3.*_mu*deps - Sy;
      ite ++;

      if(ite > maxite){
        Msg::Error("No convergence for plastic correction in j2linearVM small strain !!");
        break;
      }
    }
    
    printf("deps %.16g,depsMin %.16g,depsMax %.16g \n",deps,deps_min,deps_max);
    
    deps += deps_max + deps_min;
    deps *= 1./3.;
    
    gamma += deps;
    // update plastic strain
    static STensor3 N;
    N = (devSigtr);
    N*= (1.5/sigVMtr);

    Ep.daxpy(N,deps);
    q1->_Ee.daxpy(N,-deps);
    
    double fact = -2.*_mu*deps;
    P.daxpy(N,fact);
    svm = sigVMtr - 3.*_mu*deps;
    eigenstress += fact*N;
    eigenstress_dev = eigenstress.dev();
    eigenstress_eq = sqrt(1.5*eigenstress_dev.dotprod());
    
    q1->_plasticPower = (deps*Sy)/this->getTimeStep();
    q1->_plasticEnergy += deps*Sy;
    q1->_j2ldsy = Sy - Sy0;
    
    epsEig = Ep;
    epsEig -= I;

    if (!_tangentByPerturbation){ 
      double h;
      h = 3.*_mu+H;  
      for (int i=0; i<3; i++){
        for (int j=0; j<3; j++){
          for (int k=0; k<3; k++){
            for (int l=0; l<3; l++){
              depsEigdeps(i,j,k,l) = 2.*_mu/h*N(i,j)*N(k,l) + 2.*_mu*deps/sigVMtr*(1.5*_I4dev(i,j,k,l)-N(i,j)*N(k,l));
              Tangent(i,j,k,l) -= 2.*_mu*depsEigdeps(i,j,k,l);
            }
          }
        }
      }    
    } 
  }

  q1->_elasticEnergy = deformationEnergy(q1->_Ee);
  q1->_P = P;
  /*double detJ = STensorOperation::determinantSTensor3(Fn);
  for (int i = 0; i< 3; i ++){
    for (int j = 0; j< 3; j ++){
      sig(i,j) = 0.;
      for(int k =0; k <3; k++){
        sig(i,j) += P(i,k)*Fn(j,k)/detJ;
      }
    }
  }*/
  sig = P;
};


void mlawJ2VMSmallStrain::predictorCorectorIsotropicTangentTFA(const STensor3& F0,         // initial deformation gradient (input @ time n)
                            const STensor3& Fn,         // updated deformation gradient (input @ time n+1)
                            STensor3 &P,                // updated 1st Piola-Kirchhoff stress tensor (output)
                            const IPJ2linear *q0,       // array of initial internal variable
                            IPJ2linear *q1,             // updated array of internal variable (in ipvcur on output),
                            STensor43& Tangent,         // mechanical tangents (output)
                            STensor3& epsEig,
                            STensor43& depsEigdeps) const {

  STensor3& Ep = q1->_j2lepsp;
  double& gamma = q1->_j2lepspbarre;
  double& svm = q1->_svm;
  
  STensor3& sig = q1->_sig;
  
  STensor3& eigenstress = q1->_eigenstress;
  eigenstress = q0->_eigenstress;
  STensor3 eigenstress_dev;

  Ep = q0->_j2lepsp; // plastic strain tensor (equal I if no plastic occurs)
  gamma = q0->_j2lepspbarre; // equivalent plastic strain

  static STensor43 I_dev;
  STensorOperation::deviatorfunction(I_dev);

  static const STensor3 I(1.); // identity tensor

  // small strain
  static STensor3 eps, eps_0, epsincr, epsincr_dev;
  for (int i=0; i<3; i++){
    for (int j=0; j<3; j++){
      eps(i,j) =  0.5*(Fn(i,j)+Fn(j,i)) - I(i,j);
      eps_0(i,j) =  0.5*(F0(i,j)+F0(j,i)) - I(i,j);
    }
  }
  q1->_strain = eps;
  epsincr = eps;
  epsincr -= eps_0;
  epsincr_dev = epsincr.dev();
  double epsincrEq = sqrt(2./3.*epsincr_dev.dotprod());

  // trial elastic strain Eetr = eps-(Ep-I)
  static STensor3 Eetr;
  Eetr = (eps);
  Eetr -= Ep;
  Eetr += I;

  // trace and dev parts
  double traceEetr = Eetr.trace();
  static STensor3 devEetr;
  devEetr = Eetr.dev();

  // trial pressure and deviatoric stress
  double p = _K*traceEetr; // pressure
  static STensor3 devSigtr;
  devSigtr = (devEetr); // trial deviatoric stress
  devSigtr *= (2.*_mu);

  // stress
  sig = devSigtr;
  sig.daxpy(I,p);
  P = sig;
  // elastic strain
  q1->_Ee = Eetr;
  q1->_plasticPower = 0.;
  q1->_plasticEnergy = q0->_plasticEnergy;
  q1->_j2ldsy = 0.;
  
  if (!_tangentByPerturbation){
    Tangent = _Cel;
  }

  epsEig = Ep;
  epsEig -= I;
  STensorOperation::zero(depsEigdeps);

  // VM stress
  double sigVMtr = sqrt(1.5*devSigtr.dotprod());
  svm = sigVMtr;

  // get current yield stress
  _j2IH->hardening(q0->_j2lepspbarre, q0->getConstRefToIPJ2IsotropicHardening(), gamma, q1->getRefToIPJ2IsotropicHardening());
  double  Sy0   = q0->getConstRefToIPJ2IsotropicHardening().getR();
  double H    = q0->getConstRefToIPJ2IsotropicHardening().getDR();
  double ddR  = q0->getConstRefToIPJ2IsotropicHardening().getDDR();

  double VMCriterion = sigVMtr -Sy0;
  if (VMCriterion >0.){
    // plastic occurs
    int ite =0, maxite = 1000;
    double deps = 0.;
    double Sy = Sy0;
    while (fabs(VMCriterion/_j2IH->getYield0())> _tol){
      // plastic increment
      deps += VMCriterion/(3.*_mu+H);
      // recompute plastic state
      _j2IH->hardening(q0->_j2lepspbarre, q0->getConstRefToIPJ2IsotropicHardening(), gamma+deps, q1->getRefToIPJ2IsotropicHardening());
      Sy   = q1->getConstRefToIPJ2IsotropicHardening().getR();
      H    = q1->getConstRefToIPJ2IsotropicHardening().getDR();
      ddR  = q1->getConstRefToIPJ2IsotropicHardening().getDDR();
      

      VMCriterion = sigVMtr - 3.*_mu*deps - Sy;
      ite ++;

      if(ite > maxite){
        Msg::Error("No convergence for plastic correction in j2linearVM small strain !!");
        break;
      }

    }

    gamma += deps;
    // update plastic strain
    static STensor3 N;
    N = (devSigtr);
    N*= (1.5/sigVMtr);

    Ep.daxpy(N,deps);
    double fact = -2.*_mu*deps;
    P.daxpy(N,fact);
    q1->_Ee.daxpy(N,-deps);
    sig = P;
    svm = sigVMtr - 3.*_mu*deps;
    q1->_plasticPower = (deps*Sy)/this->getTimeStep();
    q1->_plasticEnergy += deps*Sy;
    q1->_j2ldsy = Sy - Sy0;
    
    epsEig = Ep;
    epsEig -= I;

    double h = 3.*_mu+H;
    double mu_T;
    //mu_T = _mu*H/h;
    //mu_T = _mu - 1./5.*_mu*(3.*_mu/h + 4.*deps/epsincrEq);
    mu_T = _mu*(1.-3.*_mu/h);

    static STensor43 dev_part;
    STensorOperation::sphericalfunction(Tangent);
    STensorOperation::deviatorfunction(dev_part);
    Tangent *= 3.;
    Tangent *= _K;
    dev_part *= 2.;
    dev_part *= mu_T;
    Tangent += dev_part;
    
    depsEigdeps = _Cel;
    depsEigdeps -= Tangent;
    depsEigdeps *= 1./(2*_mu);
    
    STensor3 dsig_approx, depsPl_approx;
    STensor43 Sel;
    STensorOperation::inverseSTensor43(_Cel,Sel);
    STensorOperation::multSTensor43STensor3(Tangent,epsincr,dsig_approx);
    depsPl_approx = epsincr;
    STensorOperation::multSTensor43STensor3Add(Sel,dsig_approx,-1.,depsPl_approx);
  }

  q1->_elasticEnergy = deformationEnergy(q1->_Ee);
  q1->_P = P;
  /*double detJ = STensorOperation::determinantSTensor3(Fn);
  for (int i = 0; i< 3; i ++){
    for (int j = 0; j< 3; j ++){
      sig(i,j) = 0.;
      for(int k =0; k <3; k++){
        sig(i,j) += P(i,k)*Fn(j,k)/detJ;
      }
    }
  }*/
  sig = P;

  q1->_elasticEnergy = deformationEnergy(q1->_Ee);
};


void mlawJ2VMSmallStrain::predictorCorectorResidualSecantTFA(
                            const STensor3& F_res,         // initial deformation gradient (input @ time n)
                            const STensor3& Fn,         // updated deformation gradient (input @ time n+1)
                            STensor3 &P,                // updated 1st Piola-Kirchhoff stress tensor (output)
                            const IPJ2linear *q_unloaded,       // array of initial internal variable
                            IPJ2linear *q1,             // updated array of internal variable (in ipvcur on output),
                            STensor43& Secant,         // mechanical tangents (output)
                            STensor3& epsEig,
                            STensor43& depsEigdeps,
                            const bool tangent_compute,
                            STensor43 *C_algo) const {
   // same state
  // plastic strain
    STensor3& Ep = q1->_j2lepsp;
  double& gamma = q1->_j2lepspbarre;
  double& svm = q1->_svm;
  
  STensor3& sig = q1->_sig;

  STensor3& eigenstress = q1->_eigenstress;
  eigenstress = q_unloaded->_eigenstress;
  STensor3 eigenstress_dev;

  Ep = q_unloaded->_j2lepsp; // plastic strain tensor (equal I if no plastic occurs)
  gamma = q_unloaded->_j2lepspbarre; // equivalent plastic strain

  const STensor3 &sig_res = q_unloaded->getConstRefToStress();

  static const STensor3 I(1.); // identity tensor

  // small strain
  static STensor3 eps, eps_res, eps_reload, sig_reload_tr, sig_reload_tr_dev;
  for (int i=0; i<3; i++){
    for (int j=0; j<3; j++){
      eps(i,j) =  0.5*(Fn(i,j)+Fn(j,i)) - I(i,j);
      eps_res(i,j) =  0.5*(F_res(i,j)+F_res(j,i)) - I(i,j);
    }
  }
  q1->_strain = eps;

  eps_reload = eps;
  eps_reload -= eps_res;
  STensorOperation::multSTensor43STensor3(_Cel,eps_reload,sig_reload_tr);
  sig_reload_tr_dev = sig_reload_tr.dev();

  // trial elastic strain Eetr = eps-(Ep-I)
  static STensor3 Eetr;
  Eetr = (eps);
  Eetr -= Ep;
  Eetr += I;

  // trace and dev parts
  double traceEetr = Eetr.trace();
  static STensor3 devEetr;
  devEetr = Eetr.dev();

  // trial pressure and deviatoric stress
  double p = _K*traceEetr; // pressure
  static STensor3 devSigtr;
  devSigtr = (devEetr); // trial deviatoric stress
  devSigtr *= (2.*_mu);

  // stress
  P = devSigtr;
  P.daxpy(I,p);
  sig = P;
  // elastic strain
  q1->_Ee = Eetr;
  q1->_plasticPower = 0.;
  q1->_j2ldsy = 0.;

  if (!_tangentByPerturbation and tangent_compute){
  (*C_algo) = _Cel;
  }
  
  Secant = _Cel;
  
  epsEig = Ep;
  epsEig -= I;
  STensorOperation::zero(depsEigdeps);

  // VM stress
  double sigVMtr = sqrt(1.5*devSigtr.dotprod());
  svm = sigVMtr;
  double sig_reloadVMtr = sqrt(1.5*sig_reload_tr_dev.dotprod());

  static STensor3 N, N_reload, N_inv;
  N = (devSigtr);
  N*= (1.5/sigVMtr);
  N_reload = (sig_reload_tr_dev);
  N_reload*= (1.5/sig_reloadVMtr);


  // get current yield stress
  _j2IH->hardening(q_unloaded->_j2lepspbarre, q_unloaded->getConstRefToIPJ2IsotropicHardening(), gamma, q1->getRefToIPJ2IsotropicHardening());
  double  Sy0   = q_unloaded->getConstRefToIPJ2IsotropicHardening().getR();
  double H    = q_unloaded->getConstRefToIPJ2IsotropicHardening().getDR();
  double ddR  = q_unloaded->getConstRefToIPJ2IsotropicHardening().getDDR();

  double h_reload, h;
  h = H + 3.*_mu;
  STensorOperation::inverseSTensor3(N,N_inv);
  STensorOperation::doubleContractionSTensor3(N_reload,N_inv,h_reload);
  h_reload *= 1./3.;
  h_reload *= H;
  h_reload += 3.*_mu;

  double VMCriterion = sigVMtr -Sy0;
  if (VMCriterion >0.){
    // plastic occurs
    int ite =0, maxite = 1000;
    double deps = 0.;
    double Sy = Sy0;
    while (fabs(VMCriterion/_j2IH->getYield0())> _tol){
      // plastic increment
      //deps += VMCriterion/h_reload;
      deps += VMCriterion/h;
      // recompute plastic state
      _j2IH->hardening(q_unloaded->_j2lepspbarre, q_unloaded->getConstRefToIPJ2IsotropicHardening(), gamma+deps, q1->getRefToIPJ2IsotropicHardening());
      Sy   = q1->getConstRefToIPJ2IsotropicHardening().getR();
      H    = q1->getConstRefToIPJ2IsotropicHardening().getDR();
      h = H + 3.*_mu;
      STensorOperation::inverseSTensor3(N,N_inv);
      STensorOperation::doubleContractionSTensor3(N_reload,N_inv,h_reload);
      h_reload *= 1./3.;
      h_reload *= H;
      h_reload += 3.*_mu;
      

      VMCriterion = sigVMtr - 3.*_mu*deps - Sy;
      ite ++;

      if(ite > maxite){
        Msg::Error("No convergence for plastic correction in j2linearVM small strain !!");
        break;
      }

    }

    gamma += deps;

    // update plastic strain
    Ep.daxpy(N_reload,deps);
    double fact = -2.*_mu*deps;
    P.daxpy(N_reload,fact);
    q1->_Ee.daxpy(N_reload,-deps);

    /*Ep.daxpy(N,deps);
    double fact = -2.*_mu*deps;
    P.daxpy(N,fact);
    q1->_Ee.daxpy(N,-deps);*/

    sig = P;
    svm = sigVMtr - 3.*_mu*deps;

    q1->_plasticPower = (deps*Sy)/this->getTimeStep();
    q1->_j2ldsy = Sy - Sy0;
    
    epsEig = Ep;
    epsEig -= I;

    static STensor3 sig_reload, eps_reload_dev, sig_reload_dev, sig_dev;
    sig_reload = P;
    sig_reload -= sig_res;
    eps_reload_dev=eps_reload.dev();
    sig_reload_dev=sig_reload.dev();
    sig_dev=P.dev();
    double eps_reload_eq = sqrt(2./3.*eps_reload_dev.dotprod());
    double sig_reloadVM = sqrt(1.5*sig_reload_dev.dotprod());
    double sigVM = sqrt(1.5*sig_dev.dotprod());

    double mu_ep;
    mu_ep = sig_reloadVM/(3.*eps_reload_eq);

    static STensor43 dev_part;
    STensorOperation::sphericalfunction(Secant);
    STensorOperation::deviatorfunction(dev_part);
    Secant *= 3.;
    Secant *= _K;
    dev_part *= 2.;
    dev_part *= mu_ep;
    Secant += dev_part;

    static STensor43 dNdSigpr_reload, dNdSigpr;
    for (int i=0; i<3; i++){
      for (int j=0; j<3; j++){
        for (int k=0; k<3; k++){
          for (int l=0; l<3; l++){
            dNdSigpr_reload(i,j,k,l) = (1.5*_I4dev(i,j,k,l) - N_reload(i,j)*N_reload(k,l))/sig_reloadVMtr;
            dNdSigpr(i,j,k,l) = (1.5*_I4dev(i,j,k,l) - N(i,j)*N(k,l))/sigVMtr;
          }
        }
      }
    }

    STensor43 I_dev;
    STensorOperation::deviatorfunction(I_dev);
    STensor3 fac3;
    fac3 = sig_reload_dev;
    STensor3 fac2(eps_reload_dev), fac1;
    fac3 *= 1./(6.*mu_ep*eps_reload_eq*eps_reload_eq);
    fac2 *= -2./(3.*eps_reload_eq*eps_reload_eq)*mu_ep;
    STensor43 Tangent;
    Tangent = _Cel;
    
    for (int i=0; i<3; i++){
      for (int j=0; j<3; j++){
        for (int k=0; k<3; k++){
          for (int l=0; l<3; l++){
            depsEigdeps(i,j,k,l) = 2.*_mu/h_reload*N_reload(i,j)*N_reload(k,l) + 2.*_mu*deps/sig_reloadVMtr*(1.5*_I4dev(i,j,k,l)-N_reload(i,j)*N_reload(k,l));
            //depsEigdeps(i,j,k,l) = 2.*_mu/h*N(i,j)*N(k,l) + 2.*_mu*deps/sigVMtr*(1.5*_I4dev(i,j,k,l)-N(i,j)*N(k,l));
            Tangent(i,j,k,l) -= 2.*_mu*depsEigdeps(i,j,k,l);
          }
        }
      }
    }
      
    if (tangent_compute){
    STensorOperation::multSTensor3STensor43(fac3,Tangent,fac1);
    STensor3 dGdeps(fac2);
    dGdeps += fac1;
    STensor63 dCsecdeps;
    STensorOperation::TensorProdSTensor43STensor3(I_dev,dGdeps,dCsecdeps);
    dCsecdeps *= 2.;
    
    STensorOperation::multSTensor63_ind34_STensor3(dCsecdeps,eps_reload,(*C_algo));
    (*C_algo) += Secant;
    }
      
  }

  q1->_elasticEnergy = deformationEnergy(q1->_Ee);
};


void mlawJ2VMSmallStrain::predictorCorectorResidualSecant(
                            const STensor3& F_res,         // initial deformation gradient (input @ time n)
                            const STensor3& Fn,         // updated deformation gradient (input @ time n+1)
                            STensor3 &P,                // updated 1st Piola-Kirchhoff stress tensor (output)
                            const IPJ2linear *q_unloaded,       // array of initial internal variable
                            IPJ2linear *q1,             // updated array of internal variable (in ipvcur on output),
                            STensor43& Secant,         // mechanical tangents (output)
                            STensor43& depsEldF,
                            STensor3& dpdF,
                            const bool stiff,
                            STensor43* elasticTangent,
                            const bool dSecant,
                            STensor63* dCsecdeps,
                            const bool Calg_compute,
                            STensor43* C_algo) const {
   // same state
  // plastic strain
  STensor3& Ep = q1->_j2lepsp;
  double& gamma = q1->_j2lepspbarre;
  STensor3& sig = q1->_sig;

  const STensor3 &sig_res = q_unloaded->getConstRefToStress();

  Ep = q_unloaded->_j2lepsp; // plastic strain tensor (equal I if no plastic occurs)
  gamma = q_unloaded->_j2lepspbarre; // equivalent plastic strain

  static const STensor3 I(1.); // identity tensor

  // small strain
  static STensor3 eps, eps_res, eps_reload, sig_reload_tr, sig_reload_tr_dev;
  for (int i=0; i<3; i++){
    for (int j=0; j<3; j++){
      eps(i,j) =  0.5*(Fn(i,j)+Fn(j,i)) - I(i,j);
      eps_res(i,j) =  0.5*(F_res(i,j)+F_res(j,i)) - I(i,j);
    }
  }
  q1->_strain = eps;

  eps_reload = eps;
  eps_reload -= eps_res;
  STensorOperation::multSTensor43STensor3(_Cel,eps_reload,sig_reload_tr);
  sig_reload_tr_dev = sig_reload_tr.dev();

  // trial elastic strain Eetr = eps-(Ep-I)
  static STensor3 Eetr;
  Eetr = (eps);
  Eetr -= Ep;
  Eetr += I;

  // trace and dev parts
  double traceEetr = Eetr.trace();
  static STensor3 devEetr;
  devEetr = Eetr.dev();

  // trial pressure and deviatoric stress
  double p = _K*traceEetr; // pressure
  static STensor3 devSigtr;
  devSigtr = (devEetr); // trial deviatoric stress
  devSigtr *= (2.*_mu);

  // stress
  P = devSigtr;
  P.daxpy(I,p);
  sig = P;
  // elastic strain
  q1->_Ee = Eetr;
  q1->_plasticPower = 0.;
  q1->_j2ldsy = 0.;

  if (stiff and !_tangentByPerturbation){
    Secant = _Cel;
    STensorOperation::zero(dpdF);
    STensorOperation::zero(depsEldF);
  }
  
  if (elasticTangent != NULL){
    (*elasticTangent) =  _Cel;
  }

  if(dSecant){
    STensorOperation::zero(*dCsecdeps);
    if (Calg_compute){
      (*C_algo) =  _Cel;
    }
  }


  // VM stress
  double sigVMtr = sqrt(1.5*devSigtr.dotprod());
  double sig_reloadVMtr = sqrt(1.5*sig_reload_tr_dev.dotprod());

  static STensor3 N, N_reload, N_inv;
  N = (devSigtr);
  N*= (1.5/sigVMtr);
  N_reload = (sig_reload_tr_dev);
  N_reload*= (1.5/sig_reloadVMtr);


  // get current yield stress
  _j2IH->hardening(q_unloaded->_j2lepspbarre, q_unloaded->getConstRefToIPJ2IsotropicHardening(), gamma, q1->getRefToIPJ2IsotropicHardening());
  double  Sy0   = q_unloaded->getConstRefToIPJ2IsotropicHardening().getR();
  double H    = q_unloaded->getConstRefToIPJ2IsotropicHardening().getDR();
  double ddR  = q_unloaded->getConstRefToIPJ2IsotropicHardening().getDDR();

  double h_reload, h;
  h = H + 3.*_mu;
  STensorOperation::inverseSTensor3(N,N_inv);
  STensorOperation::doubleContractionSTensor3(N_reload,N_inv,h_reload);
  h_reload *= 1./3.;
  h_reload *= H;
  h_reload += 3.*_mu;

  double VMCriterion = sigVMtr -Sy0;
  if (VMCriterion >0.){
    // plastic occurs
    int ite =0, maxite = 1000;
    double deps = 0.;
    double Sy = Sy0;
    while (fabs(VMCriterion/_j2IH->getYield0())> _tol){
      // plastic increment
      //deps += VMCriterion/h_reload;
      deps += VMCriterion/h;
      // recompute plastic state
      _j2IH->hardening(q_unloaded->_j2lepspbarre, q_unloaded->getConstRefToIPJ2IsotropicHardening(), gamma+deps, q1->getRefToIPJ2IsotropicHardening());
      Sy   = q1->getConstRefToIPJ2IsotropicHardening().getR();
      H    = q1->getConstRefToIPJ2IsotropicHardening().getDR();
      h = H + 3.*_mu;
      STensorOperation::inverseSTensor3(N,N_inv);
      STensorOperation::doubleContractionSTensor3(N_reload,N_inv,h_reload);
      h_reload *= 1./3.;
      h_reload *= H;
      h_reload += 3.*_mu;
      

      VMCriterion = sigVMtr - 3.*_mu*deps - Sy;
      ite ++;

      if(ite > maxite){
        Msg::Error("No convergence for plastic correction in j2linearVM small strain !!");
        break;
      }

    }

    gamma += deps;

    // update plastic strain
    Ep.daxpy(N_reload,deps);
    double fact = -2.*_mu*deps;
    P.daxpy(N_reload,fact);
    q1->_Ee.daxpy(N_reload,-deps);

    /*Ep.daxpy(N,deps);
    double fact = -2.*_mu*deps;
    P.daxpy(N,fact);
    q1->_Ee.daxpy(N,-deps);*/

    sig = P;

    q1->_plasticPower = (deps*Sy)/this->getTimeStep();
    q1->_j2ldsy = Sy - Sy0;

    static STensor3 sig_reload, eps_reload_dev, sig_reload_dev, sig_dev;
    sig_reload = P;
    sig_reload -= sig_res;
    eps_reload_dev=eps_reload.dev();
    sig_reload_dev=sig_reload.dev();
    sig_dev=P.dev();
    double eps_reload_eq = sqrt(2./3.*eps_reload_dev.dotprod());
    double sig_reloadVM = sqrt(1.5*sig_reload_dev.dotprod());
    double sigVM = sqrt(1.5*sig_dev.dotprod());

    double mu_ep;
    mu_ep = sig_reloadVM/(3.*eps_reload_eq);

    static STensor43 dev_part;
    STensorOperation::sphericalfunction(Secant);
    STensorOperation::deviatorfunction(dev_part);
    Secant *= 3.;
    Secant *= _K;
    dev_part *= 2.;
    dev_part *= mu_ep;
    Secant += dev_part;

    static STensor43 dNdSigpr_reload, dNdSigpr;
    for (int i=0; i<3; i++){
      for (int j=0; j<3; j++){
        for (int k=0; k<3; k++){
          for (int l=0; l<3; l++){
            dNdSigpr_reload(i,j,k,l) = (1.5*_I4dev(i,j,k,l) - N_reload(i,j)*N_reload(k,l))/sig_reloadVMtr;
            dNdSigpr(i,j,k,l) = (1.5*_I4dev(i,j,k,l) - N(i,j)*N(k,l))/sigVMtr;
          }
        }
      }
    }

    if (dSecant){
      STensor43 I_dev;
      STensorOperation::deviatorfunction(I_dev);
      STensor3 fac3;
      fac3 = sig_reload_dev;
      STensor3 fac2(eps_reload_dev), fac1;
      fac3 *= 1./(6.*mu_ep*eps_reload_eq*eps_reload_eq);
      fac2 *= -2./(3.*eps_reload_eq*eps_reload_eq)*mu_ep;
      STensor43 Tangent;
      Tangent = _Cel;

      for (int i=0; i<3; i++){
        for (int j=0; j<3; j++){
          for (int k=0; k<3; k++){
            for (int l=0; l<3; l++){
              for (int p=0; p<3; p++){
                for (int q=0; q<3; q++){
                  for (int r=0; r<3; r++){
                    for (int s=0; s<3; s++){
                      Tangent(i,j,k,l) -= _Cel(i,j,r,s)*N_reload(r,s)*N_reload(p,q)*_Cel(p,q,k,l)/h_reload + _Cel(i,j,r,s)*deps*dNdSigpr_reload(r,s,p,q)*_Cel(p,q,k,l);
                      //Tangent(i,j,k,l) -= _Cel(i,j,r,s)*N(r,s)*N(p,q)*_Cel(p,q,k,l)/h + _Cel(i,j,r,s)*deps*dNdSigpr(r,s,p,q)*_Cel(p,q,k,l);
                    }
                  }
                }
              }
            }
          }
        }
      }
      STensorOperation::multSTensor3STensor43(fac3,Tangent,fac1);
      STensor3 dGdeps(fac2);
      dGdeps += fac1;
      STensorOperation::TensorProdSTensor43STensor3(I_dev,dGdeps,(*dCsecdeps));
      (*dCsecdeps) *= 2.;

      if (Calg_compute){
        STensorOperation::multSTensor63_ind34_STensor3((*dCsecdeps),eps_reload,(*C_algo));
        (*C_algo) += Secant;
      }
    }
  }

  q1->_elasticEnergy = deformationEnergy(q1->_Ee);
};


void mlawJ2VMSmallStrain::predictorCorectorZeroSecant(const STensor3& F_res,         // initial deformation gradient (input @ time n)
                            const STensor3& Fn,         // updated deformation gradient (input @ time n+1)
                            STensor3 &P,                // updated 1st Piola-Kirchhoff stress tensor (output)
                            const IPJ2linear *q_unloaded,       // array of initial internal variable
                            IPJ2linear *q1,             // updated array of internal variable (in ipvcur on output),
                            STensor43& Secant,         // mechanical tangents (output)
                            STensor43& depsEldF,
                            STensor3& dpdF,
                            const bool stiff,
                            STensor43* elasticTangent,
                            const bool dSecant,
                            STensor63* dCsecdeps,
                            const bool Calg_compute,
                            STensor43* C_algo) const {
   // same state
  // plastic strain
  STensor3& Ep = q1->_j2lepsp;
  double& gamma = q1->_j2lepspbarre;

  STensor3& sig = q1->_sig;

  const STensor3 &sig_res = q_unloaded->getConstRefToStress();

  Ep = q_unloaded->_j2lepsp; // plastic strain tensor (equal I if no plastic occurs)
  gamma = q_unloaded->_j2lepspbarre; // equivalent plastic strain

  static const STensor3 I(1.); // identity tensor

  // small strain
  static STensor3 eps, eps_res, eps_reload, sig_reload_tr, sig_reload_tr_dev;
  for (int i=0; i<3; i++){
    for (int j=0; j<3; j++){
      eps(i,j) =  0.5*(Fn(i,j)+Fn(j,i)) - I(i,j);
      eps_res(i,j) =  0.5*(F_res(i,j)+F_res(j,i)) - I(i,j);
    }
  }
  q1->_strain = eps;

  eps_reload = eps;
  eps_reload -= eps_res;
  STensorOperation::multSTensor43STensor3(_Cel,eps_reload,sig_reload_tr);
  sig_reload_tr_dev = sig_reload_tr.dev();

  // trial elastic strain Eetr = eps-(Ep-I)
  static STensor3 Eetr;
  Eetr = (eps);
  Eetr -= Ep;
  Eetr += I;

  // trace and dev parts
  double traceEetr = Eetr.trace();
  static STensor3 devEetr;
  devEetr = Eetr.dev();

  // trial pressure and deviatoric stress
  double p = _K*traceEetr; // pressure
  static STensor3 devSigtr;
  devSigtr = (devEetr); // trial deviatoric stress
  devSigtr *= (2.*_mu);

  // stress
  P = devSigtr;
  P.daxpy(I,p);
  sig = P;

  // elastic strain
  q1->_Ee = Eetr;
  q1->_plasticPower = 0.;
  q1->_j2ldsy = 0.;

  if (stiff and !_tangentByPerturbation){
    Secant = _Cel;
    STensorOperation::zero(dpdF);
    STensorOperation::zero(depsEldF);
  }
  
  if (elasticTangent != NULL){
    (*elasticTangent) =  _Cel;
  }

  if(dSecant){
    STensorOperation::zero(*dCsecdeps);
    if (Calg_compute){
      (*C_algo) =  _Cel;
    }
  }

  // VM stress
  double sigVMtr = sqrt(1.5*devSigtr.dotprod());
  double sig_reloadVMtr = sqrt(1.5*sig_reload_tr_dev.dotprod());

  static STensor3 N, N_reload, N_inv;
  N = (devSigtr);
  N*= (1.5/sigVMtr);
  N_reload = (sig_reload_tr_dev);
  N_reload*= (1.5/sig_reloadVMtr);


  // get current yield stress
  _j2IH->hardening(q_unloaded->_j2lepspbarre, q_unloaded->getConstRefToIPJ2IsotropicHardening(), gamma, q1->getRefToIPJ2IsotropicHardening());
  double  Sy0   = q_unloaded->getConstRefToIPJ2IsotropicHardening().getR();
  double H    = q_unloaded->getConstRefToIPJ2IsotropicHardening().getDR();
  double ddR  = q_unloaded->getConstRefToIPJ2IsotropicHardening().getDDR();

  double h_reload, h;
  h = H + 3.*_mu;
  STensorOperation::inverseSTensor3(N,N_inv);
  STensorOperation::doubleContractionSTensor3(N_reload,N_inv,h_reload);
  h_reload *= 1./3.;
  h_reload *= H;
  h_reload += 3.*_mu;

  double VMCriterion = sigVMtr -Sy0;
  if (VMCriterion >0.){
    // plastic occurs
    int ite =0, maxite = 1000;
    double deps = 0.;
    double Sy = Sy0;
    while (fabs(VMCriterion/_j2IH->getYield0())> _tol){
      // plastic increment
      if(sig_reloadVMtr > sigVMtr){deps += VMCriterion/h_reload;}
      else{deps += VMCriterion/h;}
      // recompute plastic state
      _j2IH->hardening(q_unloaded->_j2lepspbarre, q_unloaded->getConstRefToIPJ2IsotropicHardening(), gamma+deps, q1->getRefToIPJ2IsotropicHardening());
      Sy   = q1->getConstRefToIPJ2IsotropicHardening().getR();
      H    = q1->getConstRefToIPJ2IsotropicHardening().getDR();
      STensorOperation::doubleContractionSTensor3(N_reload,N_inv,h_reload);
      h_reload *= 1./3.;
      h_reload *= H;
      h_reload += 3.*_mu;
      
      VMCriterion = sigVMtr - 3.*_mu*deps - Sy;
      ite ++;

      if(ite > maxite){
        Msg::Error("No convergence for plastic correction in j2linearVM small strain !!");
        break;
      }

    }

    gamma += deps;

    // update plastic strain
    if(sig_reloadVMtr > sigVMtr){
      Ep.daxpy(N,deps);
      double fact = -2.*_mu*deps;
      P.daxpy(N,fact);
      q1->_Ee.daxpy(N,-deps);}
    else{
      Ep.daxpy(N_reload,deps);
      double fact = -2.*_mu*deps;
      P.daxpy(N_reload,fact);
      q1->_Ee.daxpy(N_reload,-deps);}

    sig = P;
    q1->_plasticPower = (deps*Sy)/this->getTimeStep();
    q1->_j2ldsy = Sy - Sy0;

    static STensor3 sig_reload, sig_reload_dev, sig_dev;
    sig_reload = P;
    sig_reload -= sig_res;
    sig_reload_dev=sig_reload.dev();
    double sig_reloadVM = sqrt(1.5*sig_reload_dev.dotprod());
    sig_dev=P.dev();
    double sigVM = sqrt(1.5*sig_dev.dotprod());

    static STensor3 eps_reload_dev;
    eps_reload_dev=eps_reload.dev();
    double eps_reload_eq = sqrt(2./3.*eps_reload_dev.dotprod());
    double eps_reload_vol = eps_reload.trace();

    double mu_ep;  
    if(sig_reloadVMtr > sigVMtr){mu_ep = sigVM;}
    else{mu_ep = sig_reloadVM;}
    mu_ep /= 3.;
    mu_ep /= eps_reload_eq;

    double kappa_ep = _K;
    /*if(sig_reloadVM > sigVM){kappa_ep = P.trace();}
    else{kappa_ep = sig_reload.trace();}
    kappa_ep /= 3.;
    kappa_ep /= eps_reload_vol;*/


    static STensor43 dev_part;
    STensorOperation::sphericalfunction(Secant);
    STensorOperation::deviatorfunction(dev_part);
    Secant *= 3.;
    Secant *= kappa_ep;
    dev_part *= 2.;
    dev_part *= mu_ep;
    Secant += dev_part;

    static STensor43 dNdSigpr, dNdSigpr_reload;
    for (int i=0; i<3; i++){
      for (int j=0; j<3; j++){
        for (int k=0; k<3; k++){
          for (int l=0; l<3; l++){
            dNdSigpr(i,j,k,l) = (1.5*_I4dev(i,j,k,l) - N(i,j)*N(k,l))/sigVMtr;
            dNdSigpr_reload(i,j,k,l) = (1.5*_I4dev(i,j,k,l) - N_reload(i,j)*N_reload(k,l))/sig_reloadVMtr;
          }
        }
      }
    }


    if (dSecant){
      STensor43 I_dev;
      STensorOperation::deviatorfunction(I_dev);

      STensor3 fac3;
      if(sig_reloadVMtr > sigVMtr){fac3 = sig_dev;}
      else{fac3 = sig_reload_dev;}
      STensor3 fac2(eps_reload_dev), fac1;
      fac3 *= 1./(6.*mu_ep*eps_reload_eq*eps_reload_eq);
      fac2 *= -2./(3.*eps_reload_eq*eps_reload_eq)*mu_ep;
      STensor43 Tangent;
      Tangent = _Cel;

      for (int i=0; i<3; i++){
        for (int j=0; j<3; j++){
          for (int k=0; k<3; k++){
            for (int l=0; l<3; l++){
              for (int p=0; p<3; p++){
                for (int q=0; q<3; q++){
                  for (int r=0; r<3; r++){
                    for (int s=0; s<3; s++){
                      if(sig_reloadVM > sigVM){Tangent(i,j,k,l) -= _Cel(i,j,r,s)*N(r,s)*N(p,q)*_Cel(p,q,k,l)/h + _Cel(i,j,r,s)*deps*dNdSigpr(r,s,p,q)*_Cel(p,q,k,l);}
                      else{Tangent(i,j,k,l) -= _Cel(i,j,r,s)*N_reload(r,s)*N_reload(p,q)*_Cel(p,q,k,l)/h_reload + _Cel(i,j,r,s)*deps*dNdSigpr_reload(r,s,p,q)*_Cel(p,q,k,l);}
                    }
                  }
                }
              }
            }
          }
        }
      }
      STensorOperation::multSTensor3STensor43(fac3,Tangent,fac1);
      STensor3 dGdeps(fac2);
      dGdeps += fac1;
      STensorOperation::TensorProdSTensor43STensor3(I_dev,dGdeps,(*dCsecdeps));
      (*dCsecdeps) *= 2.;

      /*STensor63 vol_part;
      static STensor43 I_vol;
      STensorOperation::sphericalfunction(I_vol);
      STensor3 dKdeps(1.);
      dKdeps *= 3.*(_K-kappa_ep);
      dKdeps *= (1./eps_reload_vol);
      STensorOperation::TensorProdSTensor43STensor3(I_vol,dKdeps,vol_part);
      (*dCsecdeps) += vol_part;*/

      if (Calg_compute){
        STensorOperation::multSTensor63_ind34_STensor3((*dCsecdeps),eps_reload,(*C_algo));
        (*C_algo) += Secant;
      }
    }
  }

  q1->_elasticEnergy = deformationEnergy(q1->_Ee);
};




void mlawJ2VMSmallStrain::elasticUnloading(
                            const STensor3& F0,         // initial deformation gradient (input @ time n)
                            const STensor3& F_res,         // updated deformation gradient (input @ time n+1)
                            STensor3 &P_res,                // updated 1st Piola-Kirchhoff stress tensor (output)
                            const IPJ2linear *q0,       // array of initial internal variable
                            IPJ2linear *q_unloaded) const {

   // same state
  // plastic strain
  STensor3& Ep = q_unloaded->_j2lepsp;
  double& gamma = q_unloaded->_j2lepspbarre;
  const STensor3 &sig_0 = q0->getConstRefToStress();

  Ep = q0->_j2lepsp; // plastic strain tensor (equal I if no plastic occurs)
  gamma = q0->_j2lepspbarre; // equivalent plastic strain

  static const STensor3 I(1.); // identity tensor

  // small strain
  static STensor3 eps_0, eps_res, eps_unload, sig_unload;
  for (int i=0; i<3; i++){
    for (int j=0; j<3; j++){
      eps_0(i,j) =  0.5*(F0(i,j)+F0(j,i)) - I(i,j);
    }
  }
  for (int i=0; i<3; i++){
    for (int j=0; j<3; j++){
      eps_res(i,j) =  0.5*(F_res(i,j)+F_res(j,i)) - I(i,j);
    }
  }
  q_unloaded->_strain = eps_res;

  eps_unload = eps_0;
  eps_unload -= eps_res;
  STensorOperation::multSTensor43STensor3(_Cel,eps_unload,sig_unload);

  // trial elastic strain Eetr = eps-(Ep-I)
  static STensor3 Eetr;
  Eetr = (eps_res);
  Eetr -= Ep;
  Eetr += I;

  // trace and dev parts
  double traceEetr = Eetr.trace();
  static STensor3 devEetr;
  devEetr = Eetr.dev();

  // stress
  P_res = sig_0;
  P_res -= sig_unload;

  STensor3 sig_res_dev = P_res.dev();
  double sig_resvM = sqrt(1.5*sig_res_dev.dotprod());
  double  Sy0   = q_unloaded->getConstRefToIPJ2IsotropicHardening().getR();

  q_unloaded->_sig = P_res;
  // elastic strain
  q_unloaded->_Ee = Eetr;
  q_unloaded->_plasticPower = 0.;
  q_unloaded->_j2ldsy = 0.;
  q_unloaded->_elasticEnergy = deformationEnergy(q_unloaded->_Ee);
};

double mlawJ2VMSmallStrain::deformationEnergy(const STensor3 &Ee) const
{
  double ener = 0.;
  for (int i=0; i< 3; i++){
    for (int j=0; j<3; j++){
      for (int k=0; k<3; k++){
        for (int l=0; l<3; l++){
          ener += 0.5*Ee(i,j)*_Cel(i,j,k,l)*Ee(k,l);
        }
      } 
    }
  }
  return ener;
};


void mlawJ2VMSmallStrain::ElasticStiffness(STensor43* elasticTensor) const
{
  (*elasticTensor) = _Cel;
};

double mlawJ2VMSmallStrain::TangentShearModulus(const IPVariable *q0i, const IPVariable *q1i) const
{
  const IPJ2linear* q0 = static_cast<const IPJ2linear*>(q0i);
  const IPJ2linear* q1 = static_cast<const IPJ2linear*>(q1i);

  const double& gamma0 = q0->_j2lepspbarre;
  const double& gamma = q1->_j2lepspbarre;
  double dp = gamma-gamma0;

  double dR    = q1->getConstRefToIPJ2IsotropicHardening().getDR();
  double h = 3.*_mu + dR;

  /*double shearmodulus_tangent = _mu;
  if(dp>1.e-15)
  {
  shearmodulus_tangent *= dR/h;
  }*/

  const STensor3& P0 = q0->_sig;
  const STensor3& eps0 = q0->_strain;
  const STensor3& P = q1->_sig;
  const STensor3& eps = q1->_strain;

  STensor3 dP, deps;
  dP = P;
  dP -= P0;
  deps = eps;
  deps -= eps0;

  STensor3 dP_dev = dP.dev();
  double dsigEq = sqrt(1.5*dP_dev.dotprod());
  STensor3 deps_dev = deps.dev();
  double depsEq = sqrt(2./3.*deps_dev.dotprod());

  //double shearmodulus_tangent = dsigEq/(3.*depsEq);

  double shearmodulus_tangent = _mu - 1./5.*_mu*(3.*_mu/h + 4.*dp/depsEq);

  return shearmodulus_tangent;
};

double mlawJ2VMSmallStrain::SecantShearModulus(const IPVariable *q1i) const
{
  const IPJ2linear* q1 = static_cast<const IPJ2linear*>(q1i);

  const STensor3& P = q1->_sig;
  const STensor3& eps = q1->_strain;

  STensor3 P_dev = P.dev();
  double sigEq = sqrt(1.5*P_dev.dotprod());
  STensor3 eps_dev = eps.dev();
  double epsEq = sqrt(2./3.*eps_dev.dotprod());

  double shearmodulus_secant = sigEq/(3.*epsEq);

  return shearmodulus_secant;
};

double mlawJ2VMSmallStrain::ResidualSecantShearModulus(const IPVariable *q_unloadedi, const IPVariable *q1i) const
{
  const IPJ2linear* q_unloaded = static_cast<const IPJ2linear*>(q_unloadedi);
  const IPJ2linear* q1 = static_cast<const IPJ2linear*>(q1i);

  const STensor3& P_res = q_unloaded->_sig;
  const STensor3& eps_res = q_unloaded->_strain;
  const STensor3& P = q1->_sig;
  const STensor3& eps = q1->_strain;

  STensor3 P_reload = P;
  P_reload -= P_res;
  STensor3 eps_reload = eps;
  eps_reload -= eps_res;

  STensor3 P_reload_dev = P_reload.dev();
  double sigreloadEq = sqrt(1.5*P_reload_dev.dotprod());
  STensor3 eps_reload_dev = eps_reload.dev();
  double epsreloadEq = sqrt(2./3.*eps_reload_dev.dotprod());

  double shearmodulus_secant = sigreloadEq/(3.*epsreloadEq);

  return shearmodulus_secant;
};

double mlawJ2VMSmallStrain::ZeroResidualSecantShearModulus(const IPVariable *q_unloadedi, const IPVariable *q1i) const
{
  const IPJ2linear* q_unloaded = static_cast<const IPJ2linear*>(q_unloadedi);
  const IPJ2linear* q1 = static_cast<const IPJ2linear*>(q1i);

  const STensor3& eps_res = q_unloaded->_strain;
  const STensor3& P = q1->_sig;
  const STensor3& eps = q1->_strain;

  STensor3 eps_reload = eps;
  eps_reload -= eps_res;

  STensor3 P_dev = P.dev();
  double sigEq = sqrt(1.5*P_dev.dotprod());
  STensor3 eps_reload_dev = eps_reload.dev();
  double epsreloadEq = sqrt(2./3.*eps_reload_dev.dotprod());

  double shearmodulus_secant = sigEq/(3.*epsreloadEq);

  return shearmodulus_secant;
};


void mlawJ2VMSmallStrain::ElasticStiffnessIsotropic(STensor43& ElasticTangent) const
{
  double shearmodulus_tangent = _mu;
  double bulkmodulus_tangent = _K;

  STensor43 dev_part;
  STensorOperation::sphericalfunction(ElasticTangent);
  STensorOperation::deviatorfunction(dev_part);
  ElasticTangent *= 3.;
  ElasticTangent *= bulkmodulus_tangent;
  dev_part *= 2.;
  dev_part *= shearmodulus_tangent;
  ElasticTangent += dev_part;
};


void mlawJ2VMSmallStrain::TangentStiffnessIsotropic(const IPVariable *q0i, const IPVariable *q1i, STensor43& Tangent) const
{
  const IPJ2linear* q0 = static_cast<const IPJ2linear*>(q0i);
  const IPJ2linear* q1 = static_cast<const IPJ2linear*>(q1i);

  const double& gamma0 = q0->_j2lepspbarre;
  const double& gamma = q1->_j2lepspbarre;
  double depspl = gamma-gamma0;

  double shearmodulus_tangent = _mu;
  if(depspl>1.e-15)
  {
  double dR    = q1->getConstRefToIPJ2IsotropicHardening().getDR();
  double h = 3.*_mu + dR;
  shearmodulus_tangent = _mu*dR/h;
  }
  double bulkmodulus_tangent = _K;

  STensor43 dev_part;
  STensorOperation::sphericalfunction(Tangent);
  STensorOperation::deviatorfunction(dev_part);
  Tangent *= 3.;
  Tangent *= bulkmodulus_tangent;
  dev_part *= 2.;
  dev_part *= shearmodulus_tangent;
  Tangent += dev_part;
};


void mlawJ2VMSmallStrain::SecantStiffnessIsotropic(const IPVariable *q1i, STensor43& Secant) const
{
  const IPJ2linear* q1 = static_cast<const IPJ2linear*>(q1i);

  const STensor3& P = q1->_sig;
  const STensor3& eps = q1->_strain;

  STensor3 P_dev = P.dev();
  double sigEq = sqrt(1.5*P_dev.dotprod());
  STensor3 eps_dev = eps.dev();
  double epsEq = sqrt(2./3.*eps_dev.dotprod());
  double shearmodulus_secant = sigEq/(3.*epsEq);
  double bulkmodulus_secant = _K;

  STensor43 dev_part;
  STensorOperation::sphericalfunction(Secant);
  STensorOperation::deviatorfunction(dev_part);
  Secant *= 3.;
  Secant *= bulkmodulus_secant;
  dev_part *= 2.;
  dev_part *= shearmodulus_secant;
  Secant += dev_part;
};


void mlawJ2VMSmallStrain::ResidualSecantStiffnessIsotropic(const IPVariable *q_unloadedi, const IPVariable *q1i, STensor43& Secant) const
{
  const IPJ2linear* q_unloaded = static_cast<const IPJ2linear*>(q_unloadedi);
  const IPJ2linear* q1 = static_cast<const IPJ2linear*>(q1i);

  const STensor3& P_res = q_unloaded->_sig;
  const STensor3& eps_res = q_unloaded->_strain;
  const STensor3& P = q1->_sig;
  const STensor3& eps = q1->_strain;

  STensor3 P_reload = P;
  P_reload -= P_res;
  STensor3 eps_reload = eps;
  eps_reload -= eps_res;

  STensor3 P_reload_dev = P_reload.dev();
  double sigreloadEq = sqrt(1.5*P_reload_dev.dotprod());
  STensor3 eps_reload_dev = eps_reload.dev();
  double epsreloadEq = sqrt(2./3.*eps_reload_dev.dotprod());

  double shearmodulus_secant = sigreloadEq/(3.*epsreloadEq);
  double bulkmodulus_secant = _K;

  STensor43 dev_part;
  STensorOperation::sphericalfunction(Secant);
  STensorOperation::deviatorfunction(dev_part);
  Secant *= 3.;
  Secant *= bulkmodulus_secant;
  dev_part *= 2.;
  dev_part *= shearmodulus_secant;
  Secant += dev_part;
};


void mlawJ2VMSmallStrain::ZeroResidualSecantStiffnessIsotropic(const IPVariable *q_unloadedi, const IPVariable *q1i, STensor43& Secant) const
{
  const IPJ2linear* q_unloaded = static_cast<const IPJ2linear*>(q_unloadedi);
  const IPJ2linear* q1 = static_cast<const IPJ2linear*>(q1i);

  const STensor3& eps_res = q_unloaded->_strain;
  const STensor3& P = q1->_sig;
  const STensor3& eps = q1->_strain;

  STensor3 eps_reload = eps;
  eps_reload -= eps_res;

  STensor3 P_dev = P.dev();
  double sigEq = sqrt(1.5*P_dev.dotprod());
  STensor3 eps_reload_dev = eps_reload.dev();
  double epsreloadEq = sqrt(2./3.*eps_reload_dev.dotprod());

  double shearmodulus_secant = sigEq/(3.*epsreloadEq);
  double bulkmodulus_secant = _K;

  STensor43 dev_part;
  STensorOperation::sphericalfunction(Secant);
  STensorOperation::deviatorfunction(dev_part);
  Secant *= 3.;
  Secant *= bulkmodulus_secant;
  dev_part *= 2.;
  dev_part *= shearmodulus_secant;
  Secant += dev_part;
};


double mlawJ2VMSmallStrain::getH(const IPVariable *q0i, const IPVariable *q1i) const
{
  const IPJ2linear* q1 = static_cast<const IPJ2linear*>(q1i);
  const IPJ2linear* q0 = static_cast<const IPJ2linear*>(q0i);
  double gamma = q1->_j2lepspbarre;
  double gamma0 = q0->_j2lepspbarre;
  double dR;
  if( (gamma - gamma0) < 10.e-10 )
  {
    dR = 0.;
  }
  else
  {
    dR = q1->getConstRefToIPJ2IsotropicHardening().getDR();
  }
  return dR;
};

