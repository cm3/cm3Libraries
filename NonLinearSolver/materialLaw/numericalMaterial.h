//
// C++ Interface: numericalMaterial
//
// Description: Class creates microsolver from setting
//
//
// Author:  <Van Dung NGUYEN>, (C) 2012
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef NUMERICALMATERIAL_H_
#define NUMERICALMATERIAL_H_



#include "nonLinearMicroBC.h"
#include "mlaw.h"
#include "numericalFunctions.h"

class nonLinearMechSolver;
class partDomain;
class IPStateBase;
class TwoNum;

class numericalMaterialBase{
  #ifndef SWIG
  // two function allows creating a unique number from element number and gauss
  // number --> maximal 100 gausspoint per element (increasing if necessary)
  public:
    inline static int createTypeWithTwoInts(int i1, int i2)
    {
      return i2 + 100 * i1;
    }
    inline static void getTwoIntsFromType(int t, int &i1, int &i2)
    {
      i2 = t % 100;
      i1 = t / 100;
    }
    // function to distribut IP to different process
    static void distributeMacroIP(const std::vector<int>& allIPNum, const std::vector<int>& allRanks,
                                    std::map<int,std::set<int> >& mapIP);

  public:
    numericalMaterialBase(){}
    numericalMaterialBase(const numericalMaterialBase& src){}
    virtual numericalMaterialBase& operator = (const numericalMaterialBase& src){
    return *this;
    }
    virtual ~numericalMaterialBase(){}

    virtual nonLinearMechSolver* createMicroSolver(const int ele, const int gpt) const = 0;

    virtual void createIPState(const bool issolve,  /*create IPstate but create solver if true else false*/
                                IPStateBase* &ips, bool hasBodyForce, const bool* state_=NULL,
                                const MElement *ele=NULL, const int nbFF_=0, const IntPt *GP=NULL, const int gpt=0) const  = 0;

    virtual void printMeshIdMap() const = 0;
    //this function provides an id for a GP
		virtual void assignMeshId(const int e, const int gpt) = 0;
		virtual void setMeshId(const int e, const int gpt, const int id)= 0;
		// this function get mesh id
		virtual int getMeshId(const int e, const int gpt) const= 0;
		#if defined(HAVE_MPI)
		// these function used for comunicate MPI
		virtual int getNumberValuesMPI() const= 0;
		virtual void fillValuesMPI(int* val) const= 0;
		virtual void getValuesMPI(const int* val, const int size)= 0;
		#endif //HAVE_MPI
  #endif // SWIG
};

class numericalMaterial : public numericalMaterialBase
{
  #ifndef SWIG
  protected:
    mutable std::vector<partDomain*> _allocatedDomains; // cache
    mutable std::vector<materialLaw*> _allocatedLaws;
    const nonLinearMechSolver* _macroSolver;
    nonLinearMechSolver* _microSolver;
    //
    std::map<int,int> _meshIdMap; // mesh id map,
    std::map<int,std::string>::iterator _idIterator;// iterator to acces meshes
    std::map<int,std::string> _allPertMesh; // all perturbed meshes
		//
		std::map<int,std::string> _meshOnIP; // constraint mesh on each ip
    //
    std::set<int> _pertMaterialLawIP; // all perturbed by material law
    std::vector<materialLaw*> _allPertubationLaw; // all perturbed law
    
    std::map<TwoNum, std::set<int> > _solverToView; // micro-solver to view
    bool _isViewAll; // true if view all
    std::set<int> _allGP; // view all GP
    #endif

	public:
    #ifndef SWIG
		numericalMaterial(int tag);
		numericalMaterial(const numericalMaterial& src);

		virtual ~numericalMaterial();
    // create micro solver
    virtual nonLinearMechSolver* createMicroSolver(const int ele, const int gpt) const;

		#endif
		nonLinearMechSolver* getMicroSolver();
		
		virtual void addPertutationalMeshIP(const int ele, const int ip, const std::string meshfile);
    virtual void setPerturbationMaterialLawIP(const int ele, const int ip);
    virtual void addPerturbationMaterialLaw(materialLaw* mlaw);

		// set view to this element
		virtual void addViewMicroSolver(const int e, const int gp);
    virtual void addViewMicroSolver(const int em, const int ep, const int gp);
		// set view all elements
		virtual void setViewAllMicroProblems(const bool flag, const int algp);
		// this function load all meshes in current folder
		virtual void loadAllRVEMeshes(const std::string prefix);
		virtual void printMeshIdMap() const;
		#ifndef SWIG
		//this function provides an id for a GP
		virtual void assignMeshId(const int e, const int gpt);
		virtual void setMeshId(const int e, const int gpt, const int id);
		// this function get mesh id
		virtual int getMeshId(const int e, const int gpt) const;
		// this function get mesh filename
		virtual std::string getMeshFileName(const int e, const int gpt) const;
		#if defined(HAVE_MPI)
		// these function used for comunicate MPI
		virtual int getNumberValuesMPI() const;
		virtual void fillValuesMPI(int* val) const;
		virtual void getValuesMPI(const int* val, const int size);
		#endif //HAVE_MPI
		#endif //SWIG
};

#ifndef SWIG
class fileNameFilter {
  protected:
    std::string _prefix;
    std::string _fileTag;
  public:
    fileNameFilter(const std::string prefix, const std::string tag);
    ~fileNameFilter(){}
    int getNumber(const std::string& filename);
};

#endif

#endif // NUMERICALMATERIAL_H_
