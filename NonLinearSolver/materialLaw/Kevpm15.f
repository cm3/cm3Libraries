c
c	******************************************************
c	******  Determines S & DSDE within a cluster   *******
c	******	    of adjacent grains	   *******
c	************************************************
	subroutine Kevpm( S, Sb, Dtot, NR, Wtot
     &			, tau_c, gam, rho, muBn, dtaub
     &			, M_tc, MF0, gam0, expo0, dMFmx
     &			, weight, gamrlx, Arlx, Brlx, icstt
     &			, nebrlx, nrlx, nneb, nneb0, check )
	implicit none
c	===================================================
	include 'LD_param_size.f'
c	===================================================
c	=====  ARGUMENTS:
	real*8 S(6,nneb), Sb(6,nneb), Dtot(6,nneb), NR(mxneq,mxneq)
	real*8 Wtot(3,nneb), tau_c(mxslip,nneb), gam(mxslip,nneb)
	real*8 rho(mxs6,mxmods,nneb0), muBn(mxslip,nneb0)
	real*8 dtaub(12,nneb0), M_tc(mxmods,mxmods,nneb)
	real*8 MF0(nneb), gam0(mxmods,mxcstt)
	real*8 expo0(mxcstt), dMFmx(mxcstt), weight(nneb)
	real*8 gamrlx(mmxrlx), Arlx(6,mmxrlx), Brlx(3,mmxrlx)
	integer*4 icstt(nneb), nebrlx(2,7,nneb), nrlx, nneb, nneb0
	logical check
c	===================================================
c	=====  'COMMON BLOCKS:'
c	::::  'Constants'
	real*8 sq2, C1, C2, C3, C4, C12
	integer*4 i6(3,3)
	common /KcstLD/ sq2, C1, C2, C3, C4, C12, i6
c	::::  'Slip system geometry'
	real*8 A1(6,mxslip,mxcstt), B1(3,mxslip,mxcstt)
	real*8 miller(7,mxslip,mxcstt)
	common /KLP0/ A1, B1, miller
c	::::  'Other material parameters'
	real*8 Emod(6,6,mxcstt), Ecpl(6,6,mxcstt)
	real*8 twinS(mxmods,mxcstt)
	integer*4 nslip(mxmods,mxcstt), nmods(mxcstt)
	integer*4 nslipT(mxcstt), idtwin(3,mxmods,mxcstt)
	logical incrH(mxcstt), latentH(mxcstt)
	common /KmatLD/ Emod, Ecpl, twinS, idtwin, incrH
     &			, latentH, nslip, nmods, nslipT
c	::::  'Strain hardening'
	real*8 hard_p(mxHP,mxmods,mxcstt), hard_M(mxs2,mxs2)
	integer*4 IDhard(mxmods,mxcstt)
	common /KhardLD/ hard_p, hard_M, IDhard
c	::::  'Kinematic hardening'
	real*8 AB(2,mxcstt)
	logical kinH(mxcstt)
	common /KkinHLD/ AB, kinH
c	===================================================
c	=====  'LOCAL VARIABLES:'
	integer ineb, icrys, icrys0
c	::::  'EVP'
	real*8 cvtol, MisfT, MisfT0, MisfT00, det
	real*8 Lp(10,mxneb), sumgam(mxmods,mxneb), dLpdS(10,6)
	real*8 tau(mxslip,mxneb), dtau(mxslip,mxneb), MF(mxneb)
	real*8 dX(mxneq), dX_mx(mxneq), Residu(mxneq), E0(6,mxneb)
	real*8 tc_mx(mxslip,mxneb), tau_c1(mxslip,mxneb), dMdksi, ksi
	integer iter, NOEL, NPT
	integer*4 idNR(mxneq), nbactiv(mxneb)
	logical last, chkpr, activ(mxslip,mxneb)
	logical first, explH, notyet, noconv
	logical elast_0, chk_elast, chk_avoid_elast, avoid_elast, chk
c	::::  'Lattice rotation'
	real*8 DtotR(6), DtotM(6,3), WtotM(3,3), alpha_R
	real*8 ArlxM(6,3,mmxrlx), BrlxM(3,3,mmxrlx)
	real*8 w_ltc(3,mxneb)
	logical RotD, ThermActiv, FX
c	::::  'other local variables'
	real*8 vec6(6), mat66(6,6), nn(3), gg(3)
	real*8 tmp, tmp2, tmp3, tmp4, testhard, toltau(mxneb)
	real*8 toltau0(mxcstt), toltau1(mxcstt), expo(mxcstt)
	real*8 BB1(mxneq), adim, x(mxneq), tau_b, Sb0(6,mxneb)
	real*8 old_fac_xp, fac_xp, fac_xp0
	integer neq1, nvar, iloop, c_iter, k, ncstt, isol, ID_bstress
	integer*4 i, j, i1, j1, k2, ii, n, m, kcstt, irlx2, iter2, nbrlx
	integer*4 nebj, irlx, jrlx, ineb2, islip, islip2, imods, imods2
c	::::  'FFT with Augmented Lagrangian'
	real*8 M_augm_L, S_augm_L(6), c_augm_L(6,6), DDtot(6), dEH, SH
	integer it_augm_L
	real*8 ERRE, ERRS
	logical Augm_L, FFT
	common /K_augm_L/ ERRE, ERRS, Augm_L
	integer idd
	common /K_idd/ idd
c

	return
	end
c
c	*******************************************************
c	*******		Generalized Schmid law 	*******
c	*******************************************************
	subroutine KSchmid( tau, S, icstt, nslipT )
	implicit none
	include 'LD_param_size.f'
c	::::  'Strain hardening'
	real*8 hard_p(mxHP,mxmods,mxcstt), hard_M(mxs2,mxs2)
	integer*4 IDhard(mxmods,mxcstt)
	common /KhardLD/ hard_p, hard_M, IDhard
c	::::  Arguments
	real*8 tau(mxslip), S(6)
	integer*4 icstt, nslipT
c	::::  Slip system geometry
	real*8 A1(6,mxslip,mxcstt), B1(3,mxslip,mxcstt)
	common /KLP0/ A1, B1
c	::::  Local variables
	integer*4 i, j, islip
c

	return
	end
c
c	*******************************************************
c	*******		Update unknowns in Kevp 	*******
c	*******************************************************
	subroutine Kupd_evp( dX, tmp, S, w_ltc, Sb, gamrlx, Dtot, Wtot
     &				, tau, dtau, Arlx, Brlx, nebrlx
     &				, nrlx, nneb, icstt, RotD )
	implicit none
	include 'LD_param_size.f'
c	::::  Arguments
	real*8 dX(mxneq), tmp, S(6,nneb), w_ltc(3,nneb), Sb(6,nneb)
	real*8 gamrlx(mmxrlx), Dtot(6,nneb), Wtot(3,nneb)
	real*8 tau(mxslip,nneb), dtau(mxslip,nneb)
	real*8 Arlx(6,mmxrlx), Brlx(3,mmxrlx)
	integer*4 nebrlx(2,7,nneb), nrlx, nneb, icstt(nneb)
	logical RotD
c	::::  Material parameters
	real*8 Emod(6,6,mxcstt), Ecpl(6,6,mxcstt)
	real*8 twinS(mxmods,mxcstt)
	integer*4 nslip(mxmods,mxcstt), nmods(mxcstt)
	integer*4 nslipT(mxcstt), idtwin(3,mxmods,mxcstt)
	logical incrH(mxcstt), latentH(mxcstt)
	common /KmatLD/ Emod, Ecpl, twinS, idtwin, incrH
     &			, latentH, nslip, nmods, nslipT
c	::::  'Kinematic hardening'
	real*8 AB(2,mxcstt)
	logical kinH(mxcstt)
	common /KkinHLD/ AB, kinH
c	::::  Local variables
	real*8 dGrlx(mxrlx), tmp2, SmSb(6)
	integer i, j
	integer*4 irlx, jrlx, ineb, islip, imods, kcstt
	logical chgDtot

	return
	end
c
c	*******************************************************************
c	********   Compute ratios of tau vs tau_c
c	*******************************************************************
	subroutine Kratiotau( tmp3, activ, nbactiv
     &				, tau, tau_c, nmods, nslip, kcstt
     &				, idtwin, latentH, toltau0, toltau1 )
	implicit none
	include 'LD_param_size.f'
c	::::  'Strain hardening'
	real*8 hard_p(mxHP,mxmods,mxcstt), hard_M(mxs2,mxs2)
	integer*4 IDhard(mxmods,mxcstt)
	common /KhardLD/ hard_p, hard_M, IDhard
	real*8 ratio(mxslip), tau(mxslip), tau_c(mxslip)
	real*8 tmp, tmp2, tmp3, toltau0, toltau1
	integer*4 imods, nmods, islip, nslip(mxmods), nslipT
	integer*4 k, idtwin(3,mxmods), nbactiv, islip2, kcstt
	logical latentH, activ(mxslip), chk

	return
	end
c
c	*******************************************************************
c	********   Taylor hardening
c	*******************************************************************
	subroutine KTaylorH( tau_c, dtauc, rho1, rho0, visco_p
     &			, temR, islip, imods, icstt, icode, mxrho )
	implicit none
	include 'LD_param_size.f'
c	::::  'arguments'
	integer mxrho
	real*8 tau_c, dtauc, rho1(mxrho), rho0(mxrho), visco_p(3), temR
	integer*4 islip, imods, icstt, icode
c	::::  Hardening parameters
	real*8 hard_p(mxHP,mxmods,mxcstt), hard_M(mxs2,mxs2)
	integer*4 IDhard(mxmods,mxcstt)
	common /KhardLD/ hard_p, hard_M, IDhard
c	::::  'Other material parameters'
	real*8 Emod(6,6,mxcstt), Ecpl(6,6,mxcstt)
	real*8 twinS(mxmods,mxcstt)
	integer*4 nslip(mxmods,mxcstt), nmods(mxcstt)
	integer*4 nslipT(mxcstt), idtwin(3,mxmods,mxcstt)
	logical incrH(mxcstt), latentH(mxcstt)
	common /KmatLD/ Emod, Ecpl, twinS, idtwin, incrH
     &			, latentH, nslip, nmods, nslipT
c	::::  'local variables'
	real*8 invDgr, mu2b, k_forest, h_recov, drho, sumgam
	real*8 tmp, tmp2, tmp3, tmp4, rho, abmu, rho_sat, k_2, k_1
	real*8 p_i, q_i, G0kT, lnGam0, rho_sat0, pq, sumgam1, tau0K
	logical ath_bcc
	integer*4 islip2, imods2, i
	logical first
	first= .true.

	return
	end
c
c	*******************************************************************
c	********   Implements the hardening of the slip systems
c	********   (MF is the sum of the slip increments which are
c	********    also given in the X array (for latent hardening))
c	*******************************************************************
	subroutine Kharden( tau_c1, dtau, sumgam, tau_c, MF0
     &					, grain_size
     &					, imods, icstt, icode )
	implicit none
c	===================================================
	include 'LD_param_size.f'
c	===================================================
c	=====  'ARGUMENTS:'
	real*8 tau_c1, dtau, sumgam, tau_c, MF0, grain_size
	integer*4 imods, icstt
	integer icode
c	===================================================
c	=====  'COMMON BLOCKS:'
c	::::  Hardening parameters
	real*8 hard_p(mxHP,mxmods,mxcstt), hard_M(mxs2,mxs2)
	integer*4 IDhard(mxmods,mxcstt)
	common /KhardLD/ hard_p, hard_M, IDhard
c	::::  'Other material parameters'
	real*8 Emod(6,6,mxcstt), Ecpl(6,6,mxcstt)
	real*8 twinS(mxmods,mxcstt)
	integer*4 nslip(mxmods,mxcstt), nmods(mxcstt)
	integer*4 nslipT(mxcstt), idtwin(3,mxmods,mxcstt)
	logical incrH(mxcstt), latentH(mxcstt)
	common /KmatLD/ Emod, Ecpl, twinS, idtwin, incrH
     &			, latentH, nslip, nmods, nslipT
c	===================================================
c	=====  'LOCAL VARIABLES:'
	real*8 tmp, tmp2, tmp3, tmp4, MF
	integer*4 i, j, k, islip2
	logical deriv, contract, last, guerric, first

	return
	end
c
c	/// Prepare inclusion of plastic spin
	subroutine KmkMw( DtotM, Dtot, sq2 )
	implicit none
	real*8 DtotM(3,6), Dtot(6), sq2
	DtotM(1,1)= 0.D0
	DtotM(2,1)= sq2 * Dtot(5) 
	DtotM(3,1)= -sq2 * Dtot(4) 
	DtotM(1,2)= -sq2 * Dtot(6) 
	DtotM(2,2)= 0.D0
	DtotM(3,2)= -DtotM(3,1) 
	DtotM(1,3)= -DtotM(1,2)
	DtotM(2,3)= -DtotM(2,1)
	DtotM(3,3)= 0.D0
	DtotM(1,4)= -Dtot(5)
	DtotM(2,4)=  Dtot(6) 
	DtotM(3,4)= sq2 * ( Dtot(1)-Dtot(2) )
	DtotM(1,5)= Dtot(4) 
	DtotM(2,5)= sq2 * ( Dtot(3)-Dtot(1) )
	DtotM(3,5)= -Dtot(6)
	DtotM(1,6)= sq2 * ( Dtot(2)-Dtot(3) )
	DtotM(2,6)= -Dtot(4)
	DtotM(3,6)= Dtot(5)
	return
	end
c
c	/// Derive rotated skew-sym tensor relative to the spin
	subroutine K_RWR_R( M, W0, fac )
	implicit none
	real*8 M(3,3), W0(3), fac, W(3)
	integer i

	return
	end
c
c	/// Derive rotated sym-tensor relative to the tensor itself
	subroutine K_RDR_D( dRdD, W0, fac, sq2 )
	implicit none
	real*8 dRdD(6,6), W0(3), W(3), sq2, fac
	integer i, j

	return
	end
c
c	***************************
	subroutine KLineSrch( ksi0, ksi, ksi1, y0, y, y1 )
	implicit none
	real*8 ksi0, ksi, ksi1, y0, y, y1, A, B, C, tmp
c
	
	return
	end
c
c	***********************************************
c	****	Viscoplastic computation       ****
c	****  of Dp based on the estimate of Sdev  ****
c	****      (only 1 crystal at a time)       ****
c	****   >>>> NO LATENT HARDENING <<<<	 ****
c	***********************************************
	subroutine Kvp1_n( Lp, dLpdS, tau_c, tau_c1, tau, gam
     &				, M_tc, rho, muBn, MF, sumgam1
     &				, gam0, expo, backstress, kcstt
     &				, activ, explH, chkconv, c_iter )
	implicit none
c	===================================================
	include 'LD_param_size.f'
c	===================================================
c	=====  ARGUMENTS:
	real*8 Lp(10), dLpdS(10,6), tau_c(mxmods)
	real*8 tau_c1(mxmods), tau(mxslip), gam(mxslip)
	real*8 M_tc(mxmods,mxmods), rho(mxs6,mxmods), expo
	real*8 muBn(mxslip), MF, sumgam1(mxmods), gam0(mxmods)
	integer*4 kcstt
	logical activ(mxslip), explH, chkconv
	integer c_iter, backstress
c	===================================================
c	=====  COMMON BLOCKS:
c	::::  Slip system geometry
	real*8 A1(6,mxslip,mxcstt), B1(3,mxslip,mxcstt)
	common /KLP0/ A1, B1
c	::::  Other material parameters
	real*8 Emod(6,6,mxcstt), Ecpl(6,6,mxcstt)
	real*8 twinS(mxmods,mxcstt), id9(mxslip)
	integer*4 nslip(mxmods,mxcstt), nmods(mxcstt)
	integer*4 nslipT(mxcstt), idtwin(3,mxmods,mxcstt)
	logical incrH(mxcstt), latentH(mxcstt)
	common /KmatLD/ Emod, Ecpl, twinS, idtwin, incrH
     &			, latentH, nslip, nmods, nslipT
	real*8 hard_p(mxHP,mxmods,mxcstt), hard_M(mxs2,mxs2)
	integer*4 IDhard(mxmods,mxcstt)
	common /KhardLD/ hard_p, hard_M, IDhard
c	===================================================
c	=====  LOCAL VARIABLES:
	real*8 dgam(mxslip), xlink(mxslip+mxmods), BB2(mxslip)
	real*8 X, dX, dtau(mxmods), dsumgam, dsgdtau
	real*8 sumgam_min, sumgam_mx
	real*8 BB(6), BB1(mxmods), mat6(mxmods,mxmods)
	real*8 MisfT, MisfT0, mxchg, MF0, det, vec6(6), mxBB
        real*8 sumgam, tmp, tmp2, tmp3, BB3(mxslip)
	integer ineb, icrys
	integer*4 islip, islip2, imods, i, j, k, k2, k3, iter, iter2
	integer*4 count2, refslp(mxslip,mxmods), refmod(mxmods)
	integer*4 nlink, nn(mxmods), nsum, imods2, i1, i2
	integer nvar, ncrys, npt, NOEL, nbactiv
	logical chg(mxslip), last, check, chkpr, notyet, ThermActiv
	logical unstable, peierls, bounds, ath_bcc(mxmods)
	real*8 AkT(mxmods), G0KT(mxmods), tau0K(mxmods)
	real*8 p(mxmods), q(mxmods), pq(mxmods)
c
	return
	end

