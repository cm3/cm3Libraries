//
// C++ Interface: Void State
//
// Description: Storing class for computation of void state
//
//
// Author:  <V.-D. Nguyen>, (C) 2019
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef VOIDSTATEEVOLUTIONLAW_H_
#define VOIDSTATEEVOLUTIONLAW_H_

#ifndef SWIG
#include "ipVoidState.h"
#include "scalarFunction.h"
class IPNonLocalPorosity;
#endif //SWIG

class voidStateEvolutionLaw{
  public:
    public:
    enum EvolutionType{NoneState=0,Spherical=1,Spheroidal=2};

  protected:
    int _num;
    double _voidLigamentRatioAtFailure;
    // Regularisation functions
    scalarFunction* _ChiRegularizedFunction;  // when Chi --> 1

  public:
    virtual void setVoidLigamentRatioAtFailure(const double chif) {_voidLigamentRatioAtFailure = chif;};
    virtual void setRegularizedFunction(const scalarFunction& f);
    #ifndef SWIG
    voidStateEvolutionLaw(const int num);
    voidStateEvolutionLaw(const voidStateEvolutionLaw& src);
    virtual ~voidStateEvolutionLaw();
    virtual int getNum() const {return _num;}
    //! \todo update these lines
    virtual const bool isInitialized() const final { return true; };
    virtual void initPorousInternalLaw(){};
    
    virtual voidStateEvolutionLaw* clone() const = 0;
    virtual EvolutionType getType() const=0;
    virtual void createIPVariable(const double f0, IPVoidState* &ipv) const=0;
    // in general, physics changes when coalesence occurs
    virtual void evolve(const double yieldfV0, const double yieldfV1, // yield porosity
                        const double DeltahatD, const double DeltahatQ, const double DeltahatP, // internal variables
                        const IPNonLocalPorosity *q0Porous, const IPNonLocalPorosity *q1Porous, // other internal variables
                        const IPVoidState& sPrev, IPVoidState& sCur) const = 0;
    virtual bool useYieldPorosityDuringCoalescence() const =0;
    virtual void printInfo() const = 0;
    // true if porosity is a driven mechanism of these void characteristics evolution
    // false otherwise
    #endif //SWIG
};

class TrivialEvolutionLaw : public voidStateEvolutionLaw{
  public:
    TrivialEvolutionLaw(const int num);
    #ifndef SWIG
    TrivialEvolutionLaw(const TrivialEvolutionLaw& src): voidStateEvolutionLaw(src){};
    virtual ~TrivialEvolutionLaw(){};
    virtual voidStateEvolutionLaw* clone() const {return new TrivialEvolutionLaw(*this);};
    virtual EvolutionType getType() const {return voidStateEvolutionLaw::NoneState;};
    virtual void createIPVariable(const double f0, IPVoidState* &ipv) const ;
    // in general, physics changes when coalesence occurs
    virtual void evolve(const double yieldfV0, const double yieldfV1, // yield porosity
                        const double DeltahatD, const double DeltahatQ, const double DeltahatP, // internal variables
                        const IPNonLocalPorosity *q0Porous, const IPNonLocalPorosity *q1Porous, // other internal variables
                        const IPVoidState& sPrev, IPVoidState& sCur) const;
    virtual bool useYieldPorosityDuringCoalescence() const {return true;};
    virtual void printInfo() const {Msg::Info("using TrivialEvolutionLaw");};
    #endif //SWIG
};


class SphericalVoidStateEvolutionLawWithMatrixPlasticDeformation : public voidStateEvolutionLaw
{
  // in this model // Besson 2009
  // void assumes to always be spherical
  // matrix plastic deformation is driven mechanism

  protected:
    #ifndef SWIG
    double _lambda0; // initial void spacing
    double _kappa; // acceleration parameter in void spacing , by default
    double _W0;
    #endif //SWIG

  public:
    SphericalVoidStateEvolutionLawWithMatrixPlasticDeformation(const int num, const double l0, const double k, const double W0 = 1.0);
    
    #ifndef SWIG
    SphericalVoidStateEvolutionLawWithMatrixPlasticDeformation(const SphericalVoidStateEvolutionLawWithMatrixPlasticDeformation& src);
    virtual ~SphericalVoidStateEvolutionLawWithMatrixPlasticDeformation();

    virtual voidStateEvolutionLaw* clone() const {return new SphericalVoidStateEvolutionLawWithMatrixPlasticDeformation(*this);};
    virtual EvolutionType getType() const {return voidStateEvolutionLaw::Spherical;};

    virtual void createIPVariable(const double f0, IPVoidState* &ipv) const;
    // in general, physics changes when coalesence occurs
    virtual void evolve(const double yieldfV0, const double yieldfV1,
                        const double DeltahatD, const double DeltahatQ, const double DeltahatP,
                        const IPNonLocalPorosity *q0Porous, const IPNonLocalPorosity *q1Porous,
                        const IPVoidState& sPrev, IPVoidState& sCur) const;
    virtual bool useYieldPorosityDuringCoalescence() const{return true;}
    virtual void printInfo() const {Msg::Info("using SphericalVoidStateEvolutionLawWithMatrixPlasticDeformation");};
    #endif // SWIG
};

class SphericalVoidStateEvolutionLawWithDeviatoricPlasticDeformation : public SphericalVoidStateEvolutionLawWithMatrixPlasticDeformation{
  // in this model // modified version of Besson 2009
  // void assumes to always be spherical
  // deviatoric plastic deformation is driven mechanism

  public:
    SphericalVoidStateEvolutionLawWithDeviatoricPlasticDeformation(const int num, const double l0, const double k, const double W0 = 1.0);
    #ifndef SWIG
    SphericalVoidStateEvolutionLawWithDeviatoricPlasticDeformation(const SphericalVoidStateEvolutionLawWithDeviatoricPlasticDeformation& src);
    virtual ~SphericalVoidStateEvolutionLawWithDeviatoricPlasticDeformation();

    virtual voidStateEvolutionLaw* clone() const {return new SphericalVoidStateEvolutionLawWithDeviatoricPlasticDeformation(*this);};

    // in general, physics changes when coalesence occurs
    virtual void evolve(const double yieldfV0, const double yieldfV1,
                        const double DeltahatD, const double DeltahatQ, const double DeltahatP,
                        const IPNonLocalPorosity *q0Porous, const IPNonLocalPorosity *q1Porous,
                        const IPVoidState& sPrev, IPVoidState& sCur) const;
    virtual bool useYieldPorosityDuringCoalescence() const{return true;}
    virtual void printInfo() const {Msg::Info("using SphericalVoidStateEvolutionLawWithDeviatoricPlasticDeformation");};
    #endif // SWIG
};

class SpheroidalVoidStateEvolutionLawWithAspectRatioEvolution : public voidStateEvolutionLaw{
  // the modified verison of Benzaga 2002
  // during void growth spheroidal shape does not change -> W=W0
  // during coalescence, void shape remains spheroidal, W, X, and lambda evolve

  protected:
    #ifndef SWIG
    double _W0; // initial void aspect ratio
    double _lambda0; // initial void spacing
    double _kappa; // acceleration parameter in void spacing , by default
    #endif //SWIG

  public:
		SpheroidalVoidStateEvolutionLawWithAspectRatioEvolution(const int num, const double l0, const double W0, const double k);
    #ifndef SWIG
    SpheroidalVoidStateEvolutionLawWithAspectRatioEvolution(const SpheroidalVoidStateEvolutionLawWithAspectRatioEvolution& src);
    virtual ~SpheroidalVoidStateEvolutionLawWithAspectRatioEvolution();

    virtual voidStateEvolutionLaw* clone() const {return new SpheroidalVoidStateEvolutionLawWithAspectRatioEvolution(*this);};
    virtual EvolutionType getType() const {return voidStateEvolutionLaw::Spheroidal;};

    virtual void createIPVariable(const double f0, IPVoidState* &ipv) const;
    // in general, physics changes when coalesence occurs
    virtual void evolve(const double yieldfV0, const double yieldfV1,
                        const double DeltahatD, const double DeltahatQ, const double DeltahatP,
                        const IPNonLocalPorosity *q0Porous, const IPNonLocalPorosity *q1Porous,
                        const IPVoidState& sPrev, IPVoidState& sCur) const;
    virtual bool useYieldPorosityDuringCoalescence() const{return false;}
    virtual void printInfo() const {Msg::Info("using SpheroidalVoidStateEvolutionLawWithAspectRatioEvolution");};
    #endif // SWIG
};

class VoidStateLinearEvolutionLaw : public voidStateEvolutionLaw
{
// implementation of a lnear law
// dot{Chi} = kLigament\dot{\deviatoricPlasticStrain}
  protected:
    double _lambda0;
    double _kappa; // void spacing growth rate
    double _kLigament; // ligament growth rate

  public:
    VoidStateLinearEvolutionLaw(const int num, const double l0, const double kappa, const double kL);
    #ifndef SWIG
    VoidStateLinearEvolutionLaw(const VoidStateLinearEvolutionLaw& src);
    virtual ~VoidStateLinearEvolutionLaw(){};

    virtual voidStateEvolutionLaw* clone() const {return new VoidStateLinearEvolutionLaw(*this);};
    virtual EvolutionType getType() const {return voidStateEvolutionLaw::Spheroidal;};

    virtual void createIPVariable(const double f0, IPVoidState* &ipv) const;
    // in general, physics changes when coalesence occurs
    virtual void evolve(const double yieldfV0, const double yieldfV1,
                        const double DeltahatD, const double DeltahatQ, const double DeltahatP,
                        const IPNonLocalPorosity *q0Porous, const IPNonLocalPorosity *q1Porous,
                        const IPVoidState& sPrev, IPVoidState& sCur) const;
    virtual bool useYieldPorosityDuringCoalescence() const{return false;}
    virtual void printInfo() const {Msg::Info("using VoidStateLinearEvolutionLaw");};
    #endif //SWIG
};



class VoidStateExponentialEvolutionLaw : public VoidStateLinearEvolutionLaw
{
   public:
    VoidStateExponentialEvolutionLaw(const int num, const double l0, const double kappa, const double kL);
    #ifndef SWIG
    VoidStateExponentialEvolutionLaw(const VoidStateExponentialEvolutionLaw& src);
    virtual ~VoidStateExponentialEvolutionLaw(){};
    virtual voidStateEvolutionLaw* clone() const {return new VoidStateExponentialEvolutionLaw(*this);};
    virtual void evolve(const double yieldfV0, const double yieldfV1,
                        const double DeltahatD, const double DeltahatQ, const double DeltahatP,
                        const IPNonLocalPorosity *q0Porous, const IPNonLocalPorosity *q1Porous,
                        const IPVoidState& sPrev, IPVoidState& sCur) const;
    virtual bool useYieldPorosityDuringCoalescence() const{return false;}
    virtual void printInfo() const {Msg::Info("using VoidStateExponentialEvolutionLaw");};
    #endif //SWIG
};
#endif //VOIDSTATEEVOLUTIONLAW_H_
