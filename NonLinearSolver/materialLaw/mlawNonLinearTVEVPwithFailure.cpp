// C++ Interface: Material Law
//
// Description: Non-Linear Thermo-Visco-Mechanics with Failure/damage-local/non-local (Thermo-ViscoElasto-ViscoPlasto Law) with Non-Local Damage Interface (finally here!)
//
// Author: <Ujwal Kishore J - FLE_Knight>, (C) 2024
//
// Copyright: See COPYING file that comes with this distribution
//
//

#include "mlawNonLinearTVEVPwithFailure.h"
#include "nonLinearMechSolver.h"

mlawNonLinearTVEVPwithFailure::mlawNonLinearTVEVPwithFailure(const int num, const double E, const double nu, const double rho,
                                const double Tinitial, const double Alpha, const double KThCon, const double Cp,
                                const double tol, const bool matrixbyPerturbation, const double pert,
                                const double stressIteratorTol, const bool thermalEstimationPreviousConfig):
                   	mlawNonLinearTVENonLinearTVP(num,E,nu,rho,tol,Tinitial,Alpha,KThCon,Cp,
                                   matrixbyPerturbation,pert,stressIteratorTol,thermalEstimationPreviousConfig){
  _failureCriterion = new mlawHyperelasticFailureTrivialCriterion(num);
};



mlawNonLinearTVEVPwithFailure::mlawNonLinearTVEVPwithFailure(const mlawNonLinearTVEVPwithFailure& src):
    mlawNonLinearTVENonLinearTVP(src){
  _failureCriterion = NULL;
  if (src._failureCriterion){
    _failureCriterion = src._failureCriterion->clone();
  }
};
mlawNonLinearTVEVPwithFailure::~mlawNonLinearTVEVPwithFailure(){
  if (_failureCriterion != NULL) {
    delete _failureCriterion;
    _failureCriterion = NULL;
  }
};

void mlawNonLinearTVEVPwithFailure::checkInternalState(IPVariable* ipv, const IPVariable* ipvprev) const{
  IPNonLinearTVP *q1 = dynamic_cast<IPNonLinearTVP*>(ipv);
  const IPNonLinearTVP *q0 = dynamic_cast<const IPNonLinearTVP*>(ipvprev);
  if (q1 != NULL){
    double& r = q1->getRefToFailureOnset();
    r = q0->getFailureOnset();
    double failCr = _failureCriterion->getFailureCriterion(q0,q1);
    if (failCr > r){
      r = failCr;
    }
    // check failure
    if (q0->inPostFailureStage()){
      q1->getRefToInPostFailureStage() = true;
    }
    else{
      if (r > 0.){
        q1->getRefToInPostFailureStage() = true;
      }
      else{
        q1->getRefToInPostFailureStage() = false;
      }
    }
  }
};

void mlawNonLinearTVEVPwithFailure::predictorCorrector(const STensor3& F0, const STensor3& F1, STensor3& P, const IPNonLinearTVP* q0, IPNonLinearTVP* q1,
    				STensor43& Tangent, STensor43& dFpdF, STensor3& dFpdT, STensor43& dFedF, STensor3& dFedT,
                                const double& T0, const double& T, const SVector3& gradT0, const SVector3& gradT, SVector3& fluxT,
                                STensor3& dPdT, STensor3& dfluxTdgradT, SVector3& dfluxTdT, STensor33& dfluxTdF,
                                double& thermalSource, double& dthermalSourcedT, STensor3& dthermalSourcedF,
                                double& mechanicalSource, double& dmechanicalSourcedT, STensor3& dmechanicalSourcedF,
                            	const bool stiff, STensor43* elasticTangent) const{

  mlawNonLinearTVENonLinearTVP::predictorCorrector_ThermoViscoPlastic(F0,F1,P,q0,q1,Tangent,dFpdF,dFpdT,dFedF,dFedT,T0,T,gradT0,gradT,fluxT,
                                                 dPdT,dfluxTdgradT,dfluxTdT,dfluxTdF,thermalSource,dthermalSourcedT,dthermalSourcedF,
                                                 mechanicalSource,dmechanicalSourcedT,dmechanicalSourcedF,stiff,elasticTangent);

  // compute failure r and DrDF, DrdT
  double& gF = q1->getRefToFailurePlasticity();
	gF = q0->getFailurePlasticity();

  if (q1->dissipationIsBlocked()){
    if (stiff){
      STensorOperation::zero(q1->getRefToDFailurePlasticityDF());
      q1->getRefToDFailurePlasticityDT() = 0.;
    }
  }
  else{
    if (q1->inPostFailureStage()){
      gF += q1->_epspbarre - q0->_epspbarre;
      if (stiff){
        q1->getRefToDFailurePlasticityDF() = q1->_DgammaDF;
        q1->getRefToDFailurePlasticityDT() = q1->_DgammaDT;
      }
    }
    else{
      if (stiff){
        STensorOperation::zero(q1->getRefToDFailurePlasticityDF());
        q1->getRefToDFailurePlasticityDT() = 0.;
      }
    }

  }
};

void mlawNonLinearTVEVPwithFailure::setFailureCriterion(const mlawHyperelasticFailureCriterionBase& fCr){
  if (_failureCriterion) delete _failureCriterion;
  _failureCriterion = fCr.clone();
};

// ################################################################## END CLASS 1 #################################################################


mlawNonLocalDamagaNonLinearTVEVP::mlawNonLocalDamagaNonLinearTVEVP(const int num, const double E, const double nu, const double rho,
                                const double Tinitial, const double Alpha, const double KThCon, const double Cp,
                                const double tol, const bool matrixbyPerturbation, const double pert,
                                const double stressIteratorTol, const bool thermalEstimationPreviousConfig):
				mlawNonLinearTVENonLinearTVP(num,E,nu,rho,tol,Tinitial,Alpha,KThCon,Cp,
                                   matrixbyPerturbation,pert,stressIteratorTol,thermalEstimationPreviousConfig),cLLaw(NULL),damLaw(NULL){};


mlawNonLocalDamagaNonLinearTVEVP::mlawNonLocalDamagaNonLinearTVEVP(const mlawNonLocalDamagaNonLinearTVEVP &source):mlawNonLinearTVENonLinearTVP(source){
  cLLaw = NULL;
  damLaw = NULL;
  if(source.cLLaw != NULL)
  {
    cLLaw=source.cLLaw->clone();
  }
  if(source.damLaw != NULL)
  {
    damLaw=source.damLaw->clone();
  };
}

void mlawNonLocalDamagaNonLinearTVEVP::setCLengthLaw(const CLengthLaw &_cLLaw){
  if (cLLaw) delete cLLaw;
  cLLaw = _cLLaw.clone();
};
void mlawNonLocalDamagaNonLinearTVEVP::setDamageLaw(const DamageLaw &_damLaw){
  if (damLaw) delete damLaw;
  damLaw = _damLaw.clone();
};


mlawNonLocalDamagaNonLinearTVEVP::~mlawNonLocalDamagaNonLinearTVEVP(){
  if (cLLaw!= NULL) delete cLLaw;
  if (damLaw!= NULL) delete damLaw;
};

void mlawNonLocalDamagaNonLinearTVEVP::createIPState(IPStateBase* &ips, bool hasBodyForce,const bool* state_,const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
  IPVariable* ipvi = new IPNonLinearTVEVPNonLocalDamage(_compression,_traction,_kinematic,_N,_mullinsEffect,cLLaw,damLaw);
  IPVariable* ipv1 = new IPNonLinearTVEVPNonLocalDamage(_compression,_traction,_kinematic,_N,_mullinsEffect,cLLaw,damLaw);
  IPVariable* ipv2 = new IPNonLinearTVEVPNonLocalDamage(_compression,_traction,_kinematic,_N,_mullinsEffect,cLLaw,damLaw);
  if(ips != NULL) delete ips;
  ips = new IP3State(state_,ipvi,ipv1,ipv2);
}
void mlawNonLocalDamagaNonLinearTVEVP::createIPState(IPNonLinearTVEVPNonLocalDamage *ivi, IPNonLinearTVEVPNonLocalDamage *iv1, IPNonLinearTVEVPNonLocalDamage *iv2) const
{

}
void mlawNonLocalDamagaNonLinearTVEVP::createIPVariable(IPNonLinearTVEVPNonLocalDamage *&ipv,bool hasBodyForce, const MElement *ele,const int nbFF,const IntPt *GP, const int gpt) const
{

}

double mlawNonLocalDamagaNonLinearTVEVP::soundSpeed() const
{
  return mlawNonLinearTVENonLinearTVP::soundSpeed();
}

void mlawNonLocalDamagaNonLinearTVEVP::constitutive(const STensor3& F0,
                                                    const STensor3& F1,
                                                    STensor3 &P,
                                                    const IPNonLinearTVEVPNonLocalDamage *q0,
	                                                  IPNonLinearTVEVPNonLocalDamage *q1,
                                                    STensor43 &Tangent,
                                                    const bool stiff,
                                                    const bool dTangentdeps,
                                                    STensor63* dCalgdeps) const
{
  mlawNonLinearTVENonLinearTVP::constitutive(F0,F1,P,q0,q1,Tangent,stiff);
}

void mlawNonLocalDamagaNonLinearTVEVP::constitutive(const STensor3& F0, const STensor3& F1, STensor3& P,
                              const IPNonLinearTVEVPNonLocalDamage* q0, IPNonLinearTVEVPNonLocalDamage* q1,

                              STensor43& Tangent, STensor3 &dLocalPlasticStrainDStrain,
                              STensor3 &dStressDNonLocalPlasticStrain,
                              double &dLocalPlasticStrainDNonLocalPlasticStrain,

                              STensor3& dPdT, STensor33& dPdgradT,
                              double& dLocalPlasticStrainDT,

                              const double& T0, const double& T,
                              const SVector3& gradT0, const SVector3& gradT,

                              SVector3& fluxT,
                              STensor33& dfluxTdF, SVector3& dfluxTdNonLocalPlasticStrain,
                              SVector3& dfluxTdT, STensor3& dfluxTdgradT,

                              double& thermalSource,
                              STensor3& dthermalSourcedF, double& dthermalSourcedNonLocalPlasticStrain,
                              double& dthermalSourcedT, SVector3& dthermalSourcedgradT,

                              double& mechanicalSource,
                              STensor3& dmechanicalSourcedF, double& dmechanicalSourcedNonLocalPlasticStrain,
                              double& dmechanicalSourcedT, SVector3& dmechanicalSourcedgradT,

                              const bool stiff,
                              STensor43* elasticTangent,
                              const bool dTangentdeps,
                              STensor63* dCalgdeps) const
{

  if(elasticTangent != NULL) Msg::Error("mlawNonLocalDamagaNonLinearTVEVP elasticTangent not defined");
  if(_tangentByPerturbation) Msg::Error("mlawNonLocalDamagaNonLinearTVEVP numerical tangent not implemented. Switching to algorithmic tangent.");

  double p0 = q0->getCurrentPlasticStrain(); // dont know if its loaded CHECK
  cLLaw->computeCL(p0, q1->getRefToIPCLength());

  static STensor43 dFedF, dFpdF;
  static STensor3 Fpinv, Peff, dPeffdT, dFpdT, dFedT;
  mlawNonLinearTVENonLinearTVP::predictorCorrector_ThermoViscoPlastic(F0,F1,Peff,q0,q1,Tangent,dFpdF,dFpdT,dFedF,dFedT,T0,T,gradT0,gradT,fluxT,
                                                 dPeffdT,dfluxTdgradT,dfluxTdT,dfluxTdF,thermalSource,dthermalSourcedT,dthermalSourcedF,
                                                 mechanicalSource,dmechanicalSourcedT,dmechanicalSourcedF,stiff,elasticTangent);

  const STensor3& Fe = q1->_Fe;
  const STensor3& DgammadF = q1->_DgammaDF;
  const STensor3& Fp  = q1->getConstRefToFp();  // dont know if its loaded CHECK
  const double& DgammadT = q1->_DgammaDT;
  STensorOperation::inverseSTensor3(Fp,Fpinv);

  double ene = q1->defoEnergy();
  //Msg::Info("enery = %e",ene);

  damLaw->computeDamage(q1->getEffectivePlasticStrain(),
                        q0->getEffectivePlasticStrain(),
                        ene, Fe, Fp, Peff, Cel,
                        q0->getConstRefToIPDamage(),q1->getRefToIPDamage());
/* // NEEDED ?? CHECK ??
  if (q1->dissipationIsBlocked()){
    // damage stop increasing
    IPDamage& curDama = q1->getRefToIPDamage();
    curDama.getRefToDamage() = q0->getDamage();
    curDama.getRefToDDamageDp() = 0.;
    STensorOperation::zero(curDama.getRefToDDamageDFe());
    curDama.getRefToDeltaDamage() = 0;
    curDama.getRefToMaximalP() = q0->getMaximalP();
    // damage is not active
    q1->getRefToDissipationActive() =  false;
  }
  else{
    // Probably need to calculate Cel?? CHECK ??
    // if damage is not blocked
    if (q1->getNonLocalToLocal()){
      // when considering crack transition
      // increment of non-local plastic strain by true plastic strain
      // in order to obtain damage continuously developed
      q1->getRefToEffectivePlasticStrain() = q0->getEffectivePlasticStrain();
      q1->getRefToEffectivePlasticStrain() += (q1->getConstRefToEquivalentPlasticStrain() - q0->getConstRefToEquivalentPlasticStrain());
      // computed damage with current effective plastic strain
      damLaw->computeDamage(q1->getEffectivePlasticStrain(),
                        q0->getEffectivePlasticStrain(),
                        ene, Fe, Fp, Peff, Cel,
                        q0->getConstRefToIPDamage(),q1->getRefToIPDamage());
     //Msg::Info("continue damage in interface = %e",q1->getDamage());

    }
    else{
      //
      // compute damage for q1
      damLaw->computeDamage(q1->getEffectivePlasticStrain(),
                        q0->getEffectivePlasticStrain(),
                        ene, Fe, Fp, Peff, Cel,
                        q0->getConstRefToIPDamage(),q1->getRefToIPDamage());
    }

    if (q1->getDamage() > q0->getDamage() and q1->getConstRefToEquivalentPlasticStrain() > q0->getConstRefToEquivalentPlasticStrain()){
      q1->getRefToDissipationActive() = true;
    }
    else{
      q1->getRefToDissipationActive() = false;
    }
  }*/

  double D = q1->getDamage();
  double Dprev = q0->getDamage();
  P = Peff;
  P *= (1.-D);

  mechanicalSource *= (1.-D);

  q1->getRefToElasticEnergy() *= (1-D);
  double DenerPlas = q1->plasticEnergy() - q0->plasticEnergy();
  q1->getRefToPlasticEnergy() = q0->plasticEnergy()+ DenerPlas*(1-D);
  q1->getRefToDamageEnergy() = q0->damageEnergy()+ ene*(D-Dprev);

  // irreversible energy
  if (this->getMacroSolver()->getPathFollowingLocalIncrementType() == pathFollowingManager::DEFO_ENERGY){
    q1->getRefToIrreversibleEnergy() = q1->defoEnergy();
  }
  else if (this->getMacroSolver()->getPathFollowingLocalIncrementType() == pathFollowingManager::PLASTIC_ENERGY) {
    q1->getRefToIrreversibleEnergy() = q1->plasticEnergy();
  }
  else if (this->getMacroSolver()->getPathFollowingLocalIncrementType() == pathFollowingManager::DAMAGE_ENERGY){
    q1->getRefToIrreversibleEnergy() = q1->damageEnergy();
  }
  else if (this->getMacroSolver()->getPathFollowingLocalIncrementType() == pathFollowingManager::DISSIPATION_ENERGY){
    q1->getRefToIrreversibleEnergy() = q1->plasticEnergy()+q1->damageEnergy();
  }
  else{
    q1->getRefToIrreversibleEnergy() = 0.;
  }

  if(stiff){

    // we need to correct partial P/partial F: (1-D) partial P/partial F - Peff otimes partial D partial F
    static STensor3 dDdF;
    STensorOperation::multSTensor3STensor43(q1->getConstRefToDDamageDFe(),dFedF,dDdF);

    Tangent*=(1.-D);

    for(int i=0;i<3;i++){
      for(int j=0;j<3;j++){
        for(int k=0;k<3;k++){
          for(int l=0;l<3;l++){
            Tangent(i,j,k,l)-=Peff(i,j)*dDdF(k,l);
          }
        }
      }
    }


    // partial p/partial F
    dLocalPlasticStrainDStrain = DgammadF;

    dLocalPlasticStrainDT = DgammadT;
    // -hat{P} partial D/partial tilde p
    // if (q1->getNonLocalToLocal()){   // CHECK what this does
      // STensorOperation::zero(dStressDNonLocalPlasticStrain);
    // }
    // else{
    dStressDNonLocalPlasticStrain = Peff*(-1*q1->getDDamageDp());
    // }

    // partial p partial tilde p (0 if no MFH)
    dLocalPlasticStrainDNonLocalPlasticStrain = 0.;

    dPdT=dPeffdT;
    dPdT*=(1.-D);

    dmechanicalSourcedF*=(1.-D);
    dmechanicalSourcedF-=(mechanicalSource/(1.-D))*dDdF;
    dmechanicalSourcedT*=(1.-D);
    // we assume dDammage dT=0 // CHECK AND REFORMULATE

    STensorOperation::zero(dPdgradT);
    STensorOperation::zero(dfluxTdNonLocalPlasticStrain);
    dthermalSourcedNonLocalPlasticStrain=0.;
    dmechanicalSourcedNonLocalPlasticStrain=0.;

    // Plastic power stuff
    // STensor3& DplasticPowerDF = q1->getRefToDPlasticPowerDF(); // CHECK if exists or ADD
    // DplasticPowerDF *= (1-D); //  ADD
    // DplasticPowerDF.daxpy(dDdF,-q1->getConstRefToPlasticPower()/(1.-D)); //  ADD

    // irreversible energy
    STensor3& DirrEnergDF = q1->getRefToDIrreversibleEnergyDF();
    if (this->getMacroSolver()->getPathFollowingLocalIncrementType() == pathFollowingManager::DEFO_ENERGY){
      DirrEnergDF*= (1-D);
      DirrEnergDF.daxpy(dDdF,-ene);
      q1->getRefToDIrreversibleEnergyDNonLocalVariable() = -ene*q1->getDDamageDp();
    }
    else if (this->getMacroSolver()->getPathFollowingLocalIncrementType() == pathFollowingManager::PLASTIC_ENERGY) {
      DirrEnergDF*= (1-D);
      DirrEnergDF.daxpy(dDdF,-DenerPlas);
      q1->getRefToDIrreversibleEnergyDNonLocalVariable() = -DenerPlas*q1->getDDamageDp();
    }
    else if (this->getMacroSolver()->getPathFollowingLocalIncrementType() == pathFollowingManager::DAMAGE_ENERGY){
      for(int i=0;i<3;i++){
        for(int j=0;j<3;j++){
          DirrEnergDF(i,j) = 0.;
          for(int k=0;k<3;k++){
            for(int l=0;l<3;l++){
              for(int m=0; m<3; m++){
                DirrEnergDF(i,j) += (D - Dprev)*Peff(k,m)*Fp(l,m)*dFedF(k,l,i,j);
              };
            }
          }
        }
      }
      DirrEnergDF.daxpy(dDdF,ene);
      q1->getRefToDIrreversibleEnergyDNonLocalVariable() = ene*q1->getDDamageDp();
    }
    else if (this->getMacroSolver()->getPathFollowingLocalIncrementType() == pathFollowingManager::DISSIPATION_ENERGY){
      DirrEnergDF*= (1-D);
      for(int i=0;i<3;i++){
        for(int j=0;j<3;j++){
          for(int k=0;k<3;k++){
            for(int l=0;l<3;l++){
              for(int m=0; m<3; m++){
                DirrEnergDF(i,j) += (D - Dprev)*Peff(k,m)*Fp(l,m)*dFedF(k,l,i,j);
              };
            }
          }
        }
      }
      DirrEnergDF.daxpy(dDdF,ene - DenerPlas);
      q1->getRefToDIrreversibleEnergyDNonLocalVariable() = (ene - DenerPlas)*q1->getDDamageDp();
    }
    else{
      STensorOperation::zero(DirrEnergDF);
      q1->getRefToDIrreversibleEnergyDNonLocalVariable() = 0.;
    }

  }

}

// ################################################################## END CLASS 2 #################################################################

mlawLocalDamageNonLinearTVEVPWithFailure::mlawLocalDamageNonLinearTVEVPWithFailure(const int num, const double E, const double nu, const double rho,
                                const double Tinitial, const double Alpha, const double KThCon, const double Cp,
                                const double tol, const bool matrixbyPerturbation, const double pert,
                                const double stressIteratorTol, const bool thermalEstimationPreviousConfig):
				mlawNonLinearTVEVPwithFailure(num,E,nu,rho,tol,Tinitial,Alpha,KThCon,Cp,
                                   matrixbyPerturbation,pert,stressIteratorTol,thermalEstimationPreviousConfig){};

void mlawLocalDamageNonLinearTVEVPWithFailure::clearAllDamageLaw(){
  for (int i=0; i< damLaw.size(); i++){
    if (damLaw[i]!= NULL) delete damLaw[i];
  }
  damLaw.clear();
};

void mlawLocalDamageNonLinearTVEVPWithFailure::setDamageLaw(const DamageLaw &_damLaw){
  damLaw.push_back(_damLaw.clone());
};

mlawLocalDamageNonLinearTVEVPWithFailure::mlawLocalDamageNonLinearTVEVPWithFailure(const mlawLocalDamageNonLinearTVEVPWithFailure &source):
            mlawNonLinearTVEVPwithFailure(source){
  damLaw.clear();
  for (int i=0; i< source.damLaw.size(); i++){
    if(source.damLaw[i] != NULL)
    {
      damLaw.push_back(source.damLaw[i]->clone());
    };
  }
}

mlawLocalDamageNonLinearTVEVPWithFailure::~mlawLocalDamageNonLinearTVEVPWithFailure(){
  for (int i=0; i< damLaw.size(); i++){
    if (damLaw[i]!= NULL) delete damLaw[i];
  }
  damLaw.clear();
};

void mlawLocalDamageNonLinearTVEVPWithFailure::createIPState(IPStateBase* &ips, bool hasBodyForce, const bool* state_,const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
  IPVariable* ipvi = new IPNonLinearTVEVPMultipleLocalDamage(_compression,_traction,_kinematic,_N,_mullinsEffect,damLaw);
  IPVariable* ipv1 = new IPNonLinearTVEVPMultipleLocalDamage(_compression,_traction,_kinematic,_N,_mullinsEffect,damLaw);
  IPVariable* ipv2 = new IPNonLinearTVEVPMultipleLocalDamage(_compression,_traction,_kinematic,_N,_mullinsEffect,damLaw);
  if(ips != NULL) delete ips;
  ips = new IP3State(state_,ipvi,ipv1,ipv2);
}
void mlawLocalDamageNonLinearTVEVPWithFailure::createIPState(IPNonLinearTVEVPMultipleLocalDamage *ivi, IPNonLinearTVEVPMultipleLocalDamage *iv1, IPNonLinearTVEVPMultipleLocalDamage *iv2) const
{

}
void mlawLocalDamageNonLinearTVEVPWithFailure::createIPVariable(IPNonLinearTVEVPMultipleLocalDamage *&ipv,bool hasBodyForce, const MElement *ele,const int nbFF,const IntPt *GP, const int gpt) const
{

}

double mlawLocalDamageNonLinearTVEVPWithFailure::soundSpeed() const
{
  return mlawNonLinearTVEVPwithFailure::soundSpeed();
}

void mlawLocalDamageNonLinearTVEVPWithFailure::constitutive(
                            const STensor3& F0,         // initial deformation gradient (input @ time n)
                            const STensor3& F1,         // updated deformation gradient (input @ time n+1)
                            STensor3& P,                // updated 1st Piola-Kirchhoff stress tensor (output)
                            const IPNonLinearTVEVPMultipleLocalDamage *q0,       // array of initial internal variable
                            IPNonLinearTVEVPMultipleLocalDamage *q1,             // updated array of internal variable (in q1 on output),
                            STensor43& Tangent,         // constitutive tangents (output)
                            const bool stiff,            // if true compute the tangents
                            const bool dTangentdeps,
                            STensor63* dCalgdeps) const
{
  mlawNonLinearTVENonLinearTVP::constitutive(F0,F1,P,q0,q1,Tangent,stiff);
}

void mlawLocalDamageNonLinearTVEVPWithFailure::constitutive(
			                        const STensor3& F0, const STensor3& F1, STensor3& P,
                              const IPNonLinearTVEVPMultipleLocalDamage* q0, IPNonLinearTVEVPMultipleLocalDamage* q1,

                              STensor43& Tangent,
                              STensor3& dPdT, STensor33& dPdgradT,

                              const double& T0, const double& T,
                              const SVector3& gradT0, const SVector3& gradT,

                              SVector3& fluxT,
                              STensor33& dfluxTdF,
                              SVector3& dfluxTdT, STensor3& dfluxTdgradT,

                              double& thermalSource,
                              STensor3& dthermalSourcedF,
                              double& dthermalSourcedT, SVector3& dthermalSourcedgradT,

                              double& mechanicalSource,
                              STensor3& dmechanicalSourcedF,
                              double& dmechanicalSourcedT, SVector3& dmechanicalSourcedgradT,

                              const bool stiff,
                              STensor43* elasticTangent,
                              const bool dTangentdeps,
                              STensor63* dCalgdeps) const
{
  static STensor43 dFedF, dFpdF;
  static STensor3 Fpinv, Peff, dPeffdT, dFpdT, dFedT;

  mlawNonLinearTVEVPwithFailure::predictorCorrector(F0,F1,Peff,q0,q1,Tangent,dFpdF,dFpdT,dFedF,dFedT, T0,T,gradT0,gradT,fluxT,
                                dPeffdT,dfluxTdgradT,dfluxTdT,dfluxTdF,thermalSource,dthermalSourcedT,dthermalSourcedF,mechanicalSource,dmechanicalSourcedT,dmechanicalSourcedF,
                            	stiff,elasticTangent);

  // get result from effective law
  const STensor3& Fe = q1->_Fe;
  const STensor3& DgammadF = q1->_DgammaDF;
  const STensor3& Fp  = q1->getConstRefToFp();
  const STensor3& dgFdF = q1->getConstRefToDFailurePlasticityDF();

  double ene = q1->defoEnergy();

  // saturation damage always develops
  damLaw[0]->computeDamage(q1->getConstRefToEqPlasticStrain(),
                        q0->getConstRefToEqPlasticStrain(),
                        ene, Fe, Fp, Peff, Cel,
                        q0->getConstRefToIPDamage(0),q1->getRefToIPDamage(0));


  damLaw[1]->computeDamage(q1->getFailurePlasticity(),
                        q0->getFailurePlasticity(),
                        ene, Fe, Fp, Peff, Cel,
                        q0->getConstRefToIPDamage(1),q1->getRefToIPDamage(1));


  // compute total softening
  double D1 = q1->getDamage(0);
  double D2 = q1->getDamage(1);
  double D =  1. - (1.- D1)*(1.-D2);
  double Dprev = 1- (1-q0->getDamage(0))*(1-q0->getDamage(1));
  P = Peff;
  P*= (1.- D);

  mechanicalSource*=(1.-D);

  q1->getRefToElasticEnergy() = (1.-D)*ene;
  // q1->getRefToPlasticPower() *= (1.-D); // CHECK AND ADD
  double DenerPlas = q1->plasticEnergy()-q0->plasticEnergy();
  q1->getRefToPlasticEnergy() = q0->plasticEnergy() + (1.-D)*DenerPlas;
  q1->getRefToDamageEnergy() = q0->damageEnergy() + ene*(D-Dprev);

  // irreversible energy
  if (this->getMacroSolver()->getPathFollowingLocalIncrementType() == pathFollowingManager::DEFO_ENERGY){
    q1->getRefToIrreversibleEnergy() = q1->defoEnergy();
  }
  else if (this->getMacroSolver()->getPathFollowingLocalIncrementType() == pathFollowingManager::PLASTIC_ENERGY) {
    q1->getRefToIrreversibleEnergy() = q1->plasticEnergy();
  }
  else if (this->getMacroSolver()->getPathFollowingLocalIncrementType() == pathFollowingManager::DAMAGE_ENERGY){
    q1->getRefToIrreversibleEnergy() = q1->damageEnergy();
  }
  else if (this->getMacroSolver()->getPathFollowingLocalIncrementType() == pathFollowingManager::DISSIPATION_ENERGY){
    q1->getRefToIrreversibleEnergy() = q1->plasticEnergy()+q1->damageEnergy();
  }
  else{
    q1->getRefToIrreversibleEnergy() = 0.;
  }


  if(stiff)
  {
    static STensor3 dDdF;
    for(int i=0;i<3;i++){
      for(int j=0;j<3;j++){
        dDdF(i,j) = (q1->getDDamageDp(0)*DgammadF(i,j)*(1.- D2)+(1.-D1)*q1->getDDamageDp(1)*dgFdF(i,j));
        for(int k=0;k<3;k++){
          for(int l=0;l<3;l++){
            dDdF(i,j) += ((1.-D2)*q1->getConstRefToDDamageDFe(0)(k,l) +(1.-D1)*q1->getConstRefToDDamageDFe(1)(k,l)) *dFedF(k,l,i,j);
          }
        }
      }
    }

    Tangent*=(1.-D);
    for(int i=0;i<3;i++){
      for(int j=0;j<3;j++){
        for(int k=0;k<3;k++){
          for(int l=0;l<3;l++){
            Tangent(i,j,k,l)-=Peff(i,j)*dDdF(k,l);

          }
        }
      }
    }

    // Plastic power stuff
    // STensor3& DplasticPowerDF = q1->getRefToDPlasticPowerDF();  // CHECK if it exists  // CHECK AND ADD
    // DplasticPowerDF *= (1.-D);  // CHECK AND ADD
    // DplasticPowerDF.daxpy(dDdF,-q1->getConstRefToPlasticPower()/(1.-D));  // CHECK AND ADD

    // Everything else
    dPdT=dPeffdT;
    dPdT*=(1.-D);
    STensorOperation::zero(dPdgradT);

    dmechanicalSourcedF*=(1.-D);
    dmechanicalSourcedF-=(mechanicalSource/(1.-D))*dDdF;
    dmechanicalSourcedT*=(1.-D);
    // we assume dDammage dT=0  // CHECK AND REFORMULATE


    // irreversible energy
    STensor3& DirrEnergDF = q1->getRefToDIrreversibleEnergyDF();
    if (this->getMacroSolver()->getPathFollowingLocalIncrementType() == pathFollowingManager::DEFO_ENERGY){
      DirrEnergDF*= (1-D);
      DirrEnergDF.daxpy(dDdF,-ene);
    }
    else if (this->getMacroSolver()->getPathFollowingLocalIncrementType() == pathFollowingManager::PLASTIC_ENERGY) {
      DirrEnergDF*= (1-D);
      DirrEnergDF.daxpy(dDdF,-DenerPlas);
    }
    else if (this->getMacroSolver()->getPathFollowingLocalIncrementType() == pathFollowingManager::DAMAGE_ENERGY){
      for(int i=0;i<3;i++){
        for(int j=0;j<3;j++){
          DirrEnergDF(i,j) = 0.;
          for(int k=0;k<3;k++){
            for(int l=0;l<3;l++){
              for(int m=0; m<3; m++){
                DirrEnergDF(i,j) += (D - Dprev)*Peff(k,m)*Fp(l,m)*dFedF(k,l,i,j);
              };
            }
          }
        }
      }
      DirrEnergDF.daxpy(dDdF,ene);
    }
    else if (this->getMacroSolver()->getPathFollowingLocalIncrementType() == pathFollowingManager::DISSIPATION_ENERGY){
      DirrEnergDF*= (1-D);
      for(int i=0;i<3;i++){
        for(int j=0;j<3;j++){
          for(int k=0;k<3;k++){
            for(int l=0;l<3;l++){
              for(int m=0; m<3; m++){
                DirrEnergDF(i,j) += (D - Dprev)*Peff(k,m)*Fp(l,m)*dFedF(k,l,i,j);
              };
            }
          }
        }
      }
      DirrEnergDF.daxpy(dDdF,ene - DenerPlas);
    }
    else{
      STensorOperation::zero(DirrEnergDF);
    }

  }
}


// ================================================================================================= END CLASS 3 ===============================================================================

mlawNonLocalDamageNonLinearTVEVPWithFailure::mlawNonLocalDamageNonLinearTVEVPWithFailure(const int num, const double E, const double nu, const double rho,
                                const double Tinitial, const double Alpha, const double KThCon, const double Cp,
                                const double tol, const bool matrixbyPerturbation, const double pert,
                                const double stressIteratorTol, const bool thermalEstimationPreviousConfig):
				mlawNonLinearTVEVPwithFailure(num,E,nu,rho,tol,Tinitial,Alpha,KThCon,Cp,
                                   matrixbyPerturbation,pert,stressIteratorTol,thermalEstimationPreviousConfig),
                                _criticalDamage(0.999),_blockHardeningAfterFailure(false),_blockDamageAfterFailure(false){};

void mlawNonLocalDamageNonLinearTVEVPWithFailure::clearAllCLengthLaw(){
  for (int i=0; i< cLLaw.size(); i++){
    if (cLLaw[i]!= NULL) delete cLLaw[i];
  }
  cLLaw.clear();
};
void mlawNonLocalDamageNonLinearTVEVPWithFailure::clearAllDamageLaw(){
  for (int i=0; i< damLaw.size(); i++){
    if (damLaw[i]!= NULL) delete damLaw[i];
  }
  damLaw.clear();
};

void mlawNonLocalDamageNonLinearTVEVPWithFailure::setCLengthLaw(const CLengthLaw &_cLLaw){
  cLLaw.push_back(_cLLaw.clone());
};
void mlawNonLocalDamageNonLinearTVEVPWithFailure::setDamageLaw(const DamageLaw &_damLaw){
  damLaw.push_back(_damLaw.clone());
};

void mlawNonLocalDamageNonLinearTVEVPWithFailure::setCritialDamage(const double cr) {
  _criticalDamage = cr;
};

mlawNonLocalDamageNonLinearTVEVPWithFailure::mlawNonLocalDamageNonLinearTVEVPWithFailure(const mlawNonLocalDamageNonLinearTVEVPWithFailure &source):
            mlawNonLinearTVEVPwithFailure(source), _criticalDamage(source._criticalDamage), _blockHardeningAfterFailure(source._blockHardeningAfterFailure),
            _blockDamageAfterFailure(source._blockDamageAfterFailure){
  cLLaw.clear();
  damLaw.clear();

  for (int i=0; i< source.cLLaw.size(); i++){
    if(source.cLLaw[i] != NULL)
    {
      cLLaw.push_back(source.cLLaw[i]->clone());
    }
    if(source.damLaw[i] != NULL)
    {
      damLaw.push_back(source.damLaw[i]->clone());
    };
  }
}

mlawNonLocalDamageNonLinearTVEVPWithFailure::~mlawNonLocalDamageNonLinearTVEVPWithFailure(){
  for (int i=0; i< cLLaw.size(); i++){
    if (cLLaw[i]!= NULL) delete cLLaw[i];
    if (damLaw[i]!= NULL) delete damLaw[i];
  }
  cLLaw.clear();
  damLaw.clear();
};

void mlawNonLocalDamageNonLinearTVEVPWithFailure::createIPState(IPStateBase* &ips, bool hasBodyForce, const bool* state_,const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
  IPVariable* ipvi = new IPNonLinearTVEVPMultipleNonLocalDamage(_compression,_traction,_kinematic,_N,_mullinsEffect,cLLaw,damLaw);
  IPVariable* ipv1 = new IPNonLinearTVEVPMultipleNonLocalDamage(_compression,_traction,_kinematic,_N,_mullinsEffect,cLLaw,damLaw);
  IPVariable* ipv2 = new IPNonLinearTVEVPMultipleNonLocalDamage(_compression,_traction,_kinematic,_N,_mullinsEffect,cLLaw,damLaw);
  if(ips != NULL) delete ips;
  ips = new IP3State(state_,ipvi,ipv1,ipv2);
}
void mlawNonLocalDamageNonLinearTVEVPWithFailure::createIPState(IPNonLinearTVEVPMultipleNonLocalDamage *ivi, IPNonLinearTVEVPMultipleNonLocalDamage *iv1, IPNonLinearTVEVPMultipleNonLocalDamage *iv2) const
{

}
void mlawNonLocalDamageNonLinearTVEVPWithFailure::createIPVariable(IPNonLinearTVEVPMultipleNonLocalDamage *&ipv,bool hasBodyForce, const MElement *ele,const int nbFF,const IntPt *GP, const int gpt) const
{

}

double mlawNonLocalDamageNonLinearTVEVPWithFailure::soundSpeed() const
{
  return mlawNonLinearTVEVPwithFailure::soundSpeed();
}

void mlawNonLocalDamageNonLinearTVEVPWithFailure::checkInternalState(IPVariable* q1, const IPVariable* q0) const{
  mlawNonLinearTVEVPwithFailure::checkInternalState(q1,q0);

  IPNonLinearTVEVPMultipleNonLocalDamage* ipv = dynamic_cast<IPNonLinearTVEVPMultipleNonLocalDamage*>(q1);
  const IPNonLinearTVEVPMultipleNonLocalDamage* ipvprev = dynamic_cast<const IPNonLinearTVEVPMultipleNonLocalDamage*>(q0);

  if (ipv != NULL){
    // computue total softening
    double D0 = ipv->getDamage(0);
    double D1 = ipv->getDamage(1);
    double D =  1. - (1.- D0)*(1.-D1);

    if (_blockHardeningAfterFailure){
      // saturation of the hardening after damage reach critical value
      if (!ipvprev->isSaturated()){
        // if damage reaches its critical values
        if (D1 > _criticalDamage or D > _criticalDamage){
          ipv->saturate(true);
        }
        else{
          // other wise, normal state is used
          ipv->saturate(false);
        }
      }
      else{
        ipv->saturate(false);
      }
    }

    // check block damage
    if (_blockDamageAfterFailure){
      if (!ipvprev->dissipationIsBlocked()){
        // if damage reaches its critical values
        if (D1 > _criticalDamage or D > _criticalDamage){
          ipv->blockDissipation(true);
        }
        else{
          // other wise, normal state is used
          ipv->blockDissipation(false);
        }
      }
      else{
        ipv->blockDissipation(true);
      }
    }

  }
};

void mlawNonLocalDamageNonLinearTVEVPWithFailure::predictorCorrector(const STensor3& F0, const STensor3& F1, STensor3& P,
                              const IPNonLinearTVEVPMultipleNonLocalDamage* q0, IPNonLinearTVEVPMultipleNonLocalDamage* q1,

                              STensor43& Tangent,
                              std::vector<STensor3>& dLocalVariableDStrain,
                              std::vector<STensor3>& dStressDNonLocalVariable,
                              fullMatrix<double>& dLocalVariableDNonLocalVariable,

                              STensor3& dPdT, STensor33& dPdgradT,
                              fullMatrix<double>& dLocalVariableDT,

                              const double& T0, const double& T,
                              const SVector3& gradT0, const SVector3& gradT,

                              SVector3& fluxT,
                              STensor33& dfluxTdF,
                              std::vector<SVector3>& dfluxTdNonLocalVariable,
                              SVector3& dfluxTdT, STensor3& dfluxTdgradT,

                              double& thermalSource,
                              STensor3& dthermalSourcedF,
                              fullMatrix<double>& dthermalSourcedNonLocalVariable,
                              double& dthermalSourcedT, SVector3& dthermalSourcedgradT,

                              double& mechanicalSource,
                              STensor3& dmechanicalSourcedF,
                              fullMatrix<double>& dmechanicalSourcedNonLocalVariable,
                              double& dmechanicalSourcedT, SVector3& dmechanicalSourcedgradT,

                              const bool stiff,
                              STensor43* elasticTangent) const
{
  // compute non-local length scales for first and second damage variable

  //double p0 = q0->getCurrentPlasticStrain();
  double D0prev = q0->getDamage(0);
  cLLaw[0]->computeCL(D0prev, q1->getRefToIPCLength(0));

  //double p1 = q0->getFailurePlasticity();
  double D1prev = q0->getDamage(1);
  cLLaw[1]->computeCL(D1prev, q1->getRefToIPCLength(1));


  static STensor43 dFedF, dFpdF;
  static STensor3 Fpinv, Peff, dPeffdT, dFpdT, dFedT;

  mlawNonLinearTVEVPwithFailure::predictorCorrector(F0,F1,Peff,q0,q1,Tangent,dFpdF,dFpdT,dFedF,dFedT, T0,T,gradT0,gradT,fluxT,
                                dPeffdT,dfluxTdgradT,dfluxTdT,dfluxTdF,thermalSource,dthermalSourcedT,dthermalSourcedF,mechanicalSource,dmechanicalSourcedT,dmechanicalSourcedF,
                            	stiff,elasticTangent);


  // get results from effective law
  const STensor3& Fe = q1->_Fe;
  const STensor3& DgammadF = q1->_DgammaDF;
  const double& DgammadT = q1->_DgammaDT;
  const STensor3& Fp  = q1->getConstRefToFp();
  const STensor3& dgFdF = q1->getConstRefToDFailurePlasticityDF();
  const double& dgFdT = q1->getConstRefToDFailurePlasticityDT();

  double ene = q1->defoEnergy();
  //Msg::Info("enery = %e",ene);

  if (q1->dissipationIsBlocked()){
    // damage stops increasing
    IPDamage& curDama0 = q1->getRefToIPDamage(0);
    const IPDamage& prevDama0 = q0->getConstRefToIPDamage(0);

    curDama0.getRefToDamage() = prevDama0.getDamage();
    curDama0.getRefToDDamageDp() = 0.;
    STensorOperation::zero(curDama0.getRefToDDamageDFe());
    curDama0.getRefToDeltaDamage() = 0;
    curDama0.getRefToMaximalP() = prevDama0.getMaximalP();

    IPDamage& curDama1 = q1->getRefToIPDamage(1);
    const IPDamage& prevDama1 = q0->getConstRefToIPDamage(1);

    curDama1.getRefToDamage() = prevDama1.getDamage();
    curDama1.getRefToDDamageDp() = 0.;
    STensorOperation::zero(curDama1.getRefToDDamageDFe());
    curDama1.getRefToDeltaDamage() = 0;
    curDama1.getRefToMaximalP() = prevDama1.getMaximalP();
  }
  else{
    // saturation damage always develops
    damLaw[0]->computeDamage(q1->getEffectivePlasticStrain(),
                          q0->getEffectivePlasticStrain(),
                          ene, Fe, Fp, Peff, Cel,
                          q0->getConstRefToIPDamage(0),q1->getRefToIPDamage(0));


    damLaw[1]->computeDamage(q1->getNonLocalFailurePlasticity(),
                          q0->getNonLocalFailurePlasticity(),
                          ene, Fe, Fp, Peff, Cel,
                          q0->getConstRefToIPDamage(1),q1->getRefToIPDamage(1));
  };

  // compute total softening
  double D0 = q1->getDamage(0);
  double D1 = q1->getDamage(1);
  double D =  1. - (1.- D0)*(1.-D1);

  double DeltaD = (D0-D0prev)*(1.-D1)+ (1.-D0)*(D1-D1prev);

  P = Peff;
  P*= (1.- D);

  mechanicalSource*=(1.-D);

  q1->getRefToElasticEnergy() = (1.-D)*ene;
  // q1->getRefToPlasticPower() *= (1.-D); // check if it exists // CHECK AND ADD

  double plasticDisStep = q1->plasticEnergy()-q0->plasticEnergy(); // plastic step computed from nondamage law
  q1->getRefToPlasticEnergy() = q0->plasticEnergy() + (1.-D)*plasticDisStep;
  q1->getRefToDamageEnergy() = q0->damageEnergy() + ene*DeltaD;

  // irreversible energy
  if (getMacroSolver()->getPathFollowingLocalIncrementType() == pathFollowingManager::DEFO_ENERGY){
    q1->getRefToIrreversibleEnergy() = q1->defoEnergy();
  }
  else if (getMacroSolver()->getPathFollowingLocalIncrementType() == pathFollowingManager::PLASTIC_ENERGY){
    q1->getRefToIrreversibleEnergy() = q1->plasticEnergy();
  }
  else if (getMacroSolver()->getPathFollowingLocalIncrementType() == pathFollowingManager::DAMAGE_ENERGY){
    q1->getRefToIrreversibleEnergy() = q1->damageEnergy();
  }
  else if (getMacroSolver()->getPathFollowingLocalIncrementType() == pathFollowingManager::DISSIPATION_ENERGY){
    q1->getRefToIrreversibleEnergy() = q1->plasticEnergy()+q1->damageEnergy();
  }
  else{
    q1->getRefToIrreversibleEnergy() = 0.;
  }


  if(stiff)
  {
    // we need to correct partial P/partial F: (1-D) partial P/partial F - Peff otimes partial D partial F
    Tangent*=(1.-D);

    static STensor3 dD0dF, dD1dF;
    STensorOperation::multSTensor3STensor43(q1->getConstRefToDDamageDFe(0),dFedF, dD0dF);
    STensorOperation::multSTensor3STensor43(q1->getConstRefToDDamageDFe(1),dFedF, dD1dF);
    static STensor3 dDdF;
    dDdF = dD0dF;
    dDdF *= (1-D1);
    dDdF.daxpy(dD1dF,1.-D0);

    for(int i=0;i<3;i++){
      for(int j=0;j<3;j++){
        for(int k=0;k<3;k++){
          for(int l=0;l<3;l++){
            Tangent(i,j,k,l)-=Peff(i,j)*dDdF(k,l);
          }
        }
      }
    }

    if (elasticTangent != NULL){
    // update with damage
      (*elasticTangent) *= (1.-D);
     }
    dLocalVariableDStrain[0] = DgammadF;
    dLocalVariableDStrain[1] = dgFdF;

    // -hat{P} partial D/partial tilde p
    for (int i=0; i<3; i++){
      for (int j=0; j<3; j++){
        // partial p/partial F
        dStressDNonLocalVariable[0](i,j) = -1.*Peff(i,j)*q1->getDDamageDp(0)*(1.- D1);
        dStressDNonLocalVariable[1](i,j) = -1.*Peff(i,j)*(1.-D0)*q1->getDDamageDp(1);
      }
    }

    // dLocalVariableDT = dgFdT;
    // IS THIS NEEDED ?? CHECK
    dLocalVariableDT.setAll(0.); // CHECK
    dLocalVariableDT(0,0) = DgammadT;
    dLocalVariableDT(1,0) = dgFdT;


    dPdT=dPeffdT;
    dPdT*=(1.-D);

    dmechanicalSourcedF*=(1.-D);
    dmechanicalSourcedF-=(mechanicalSource/(1.-D))*dDdF;
    dmechanicalSourcedT*=(1.-D);
    // we assume dDammage dT=0 // CHECK and REFORMULATE

    STensorOperation::zero(dPdgradT);
    STensorOperation::zero(dfluxTdNonLocalVariable[0]);
    STensorOperation::zero(dfluxTdNonLocalVariable[1]);
    dthermalSourcedNonLocalVariable.setAll(0.);
    dmechanicalSourcedNonLocalVariable.setAll(0.);

    // partial p partial tilde p (0 if no MFH)
    dLocalVariableDNonLocalVariable.setAll(0.);

    // plastic power stuff
    // STensor3& DplasticPowerDF = q1->getRefToDPlasticPowerDF(); // CHECK and ADD
    // DplasticPowerDF *= (1.-D); // CHECK and ADD
    // DplasticPowerDF.daxpy(dDdF,-q1->getConstRefToPlasticPower()/(1.-D)); // CHECK and ADD

    // for irreversible energy
    STensor3& DirrEnergDF = q1->getRefToDIrreversibleEnergyDF();
    if (getMacroSolver()->getPathFollowingLocalIncrementType() == pathFollowingManager::DEFO_ENERGY){
      DirrEnergDF*= (1.-D);
      DirrEnergDF.daxpy(dDdF,-ene);

      q1->getRefToDIrreversibleEnergyDNonLocalVariable(0) = (-ene)*(q1->getDDamageDp(0)*(1.- D1));
      q1->getRefToDIrreversibleEnergyDNonLocalVariable(1) = (-ene)*(q1->getDDamageDp(1)*(1.- D0));
    }
    else if (getMacroSolver()->getPathFollowingLocalIncrementType() == pathFollowingManager::PLASTIC_ENERGY){
      DirrEnergDF *= (1.-D);
      DirrEnergDF.daxpy(dDdF,-plasticDisStep);

      q1->getRefToDIrreversibleEnergyDNonLocalVariable(0) = (-plasticDisStep)*(q1->getDDamageDp(0)*(1.- D1));
      q1->getRefToDIrreversibleEnergyDNonLocalVariable(1) = (-plasticDisStep)*(q1->getDDamageDp(1)*(1.- D0));
    }
    else if (getMacroSolver()->getPathFollowingLocalIncrementType() == pathFollowingManager::DAMAGE_ENERGY){
      for(int i=0;i<3;i++){
        for(int j=0;j<3;j++){
          DirrEnergDF(i,j) = 0.;
          for(int k=0;k<3;k++){
            for(int l=0;l<3;l++){
              for(int m=0; m<3; m++){
                DirrEnergDF(i,j) += DeltaD*Peff(k,m)*Fp(l,m)*dFedF(k,l,i,j);
              };
            }
          }
        }
      }
      static STensor3 DDeltaDDF;
      DDeltaDDF = dD0dF;
      DDeltaDDF *= (1.-D1-(D1-D1prev));
      DDeltaDDF.daxpy(dD1dF,1.-D0-(D0-D0prev));

      DirrEnergDF.daxpy(DDeltaDDF,ene);

      q1->getRefToDIrreversibleEnergyDNonLocalVariable(0) = (ene)*(q1->getDDamageDp(0)*(1.- D1- (D1-D1prev)));
      q1->getRefToDIrreversibleEnergyDNonLocalVariable(1) = (ene)*(q1->getDDamageDp(1)*(1.- D0- (D0-D0prev)));

    }
    else if (getMacroSolver()->getPathFollowingLocalIncrementType() == pathFollowingManager::DISSIPATION_ENERGY){
      // plastic part
      DirrEnergDF *= (1-D);
      DirrEnergDF.daxpy(dDdF,-plasticDisStep);

      q1->getRefToDIrreversibleEnergyDNonLocalVariable(0) = (-plasticDisStep)*(q1->getDDamageDp(0)*(1.- D1));
      q1->getRefToDIrreversibleEnergyDNonLocalVariable(1) = (-plasticDisStep)*(q1->getDDamageDp(1)*(1.- D0));

      for(int i=0;i<3;i++){
        for(int j=0;j<3;j++){
          for(int k=0;k<3;k++){
            for(int l=0;l<3;l++){
              for(int m=0; m<3; m++){
                DirrEnergDF(i,j) += DeltaD*Peff(k,m)*Fp(l,m)*dFedF(k,l,i,j);
              };
            }
          }
        }
      }
      // damage part
      static STensor3 DDeltaDDF;
      DDeltaDDF = dD0dF;
      DDeltaDDF *= (1.-D1-(D1-D1prev));
      DDeltaDDF.daxpy(dD1dF,1.-D0-(D0-D0prev));

      DirrEnergDF.daxpy(DDeltaDDF,ene);

      q1->getRefToDIrreversibleEnergyDNonLocalVariable(0) += (ene)*(q1->getDDamageDp(0)*(1.- D1- (D1-D1prev)));
      q1->getRefToDIrreversibleEnergyDNonLocalVariable(1) += (ene)*(q1->getDDamageDp(1)*(1.- D0- (D0-D0prev)));
    }
    else{
      STensorOperation::zero(DirrEnergDF);
    }

  }
};


void mlawNonLocalDamageNonLinearTVEVPWithFailure::constitutive(const STensor3& F0,         // initial deformation gradient (input @ time n)
                              const STensor3& F1,         // updated deformation gradient (input @ time n+1)
                              STensor3 &P,                // updated 1st Piola-Kirchhoff stress tensor (output)
                              const IPNonLinearTVEVPMultipleNonLocalDamage *q0,       // array of initial internal variable
                              IPNonLinearTVEVPMultipleNonLocalDamage *q1,             // updated array of internal variable (in q1 on output),
                              STensor43 &Tangent,         // constitutive tangents (output)
                              const bool stiff,           // if true compute the tangents
                              const bool dTangentdeps,
                              STensor63* dCalgdeps) const
{
	mlawNonLinearTVENonLinearTVP::constitutive(F0,F1,P,q0,q1,Tangent,stiff);
}

void mlawNonLocalDamageNonLinearTVEVPWithFailure::constitutive(const STensor3& F0, const STensor3& F1, STensor3& P,
                              const IPNonLinearTVEVPMultipleNonLocalDamage* q0, IPNonLinearTVEVPMultipleNonLocalDamage* q1,

                              STensor43& Tangent,
                              std::vector<STensor3>& dLocalVariableDStrain,
                              std::vector<STensor3>& dStressDNonLocalVariable,
                              fullMatrix<double>& dLocalVariableDNonLocalVariable,

                              STensor3& dPdT, STensor33& dPdgradT,
                              fullMatrix<double>& dLocalVariableDT,

                              const double& T0, const double& T,
                              const SVector3& gradT0, const SVector3& gradT,

                              SVector3& fluxT,
                              STensor33& dfluxTdF,
                              std::vector<SVector3>& dfluxTdNonLocalVariable,
                              SVector3& dfluxTdT, STensor3& dfluxTdgradT,

                              double& thermalSource,
                              STensor3& dthermalSourcedF,
                              fullMatrix<double>& dthermalSourcedNonLocalVariable,
                              double& dthermalSourcedT, SVector3& dthermalSourcedgradT,

                              double& mechanicalSource,
                              STensor3& dmechanicalSourcedF,
                              fullMatrix<double>& dmechanicalSourcedNonLocalVariable,
                              double& dmechanicalSourcedT, SVector3& dmechanicalSourcedgradT,

                              const bool stiff,            // if true compute the tangents
                              STensor43* elasticTangent,
                              const bool dTangentdeps,
                              STensor63* dCalgdeps) const
{
  if (!_tangentByPerturbation){
  	predictorCorrector(F0,F1,P,q0,q1,Tangent,dLocalVariableDStrain,dStressDNonLocalVariable,dLocalVariableDNonLocalVariable,dPdT,dPdgradT,dLocalVariableDT,
                            T0,T,gradT0,gradT,fluxT,dfluxTdF,dfluxTdNonLocalVariable,dfluxTdT,dfluxTdgradT,
                 	    thermalSource,dthermalSourcedF,dthermalSourcedNonLocalVariable,dthermalSourcedT,dthermalSourcedgradT,
          		    mechanicalSource,dmechanicalSourcedF,dmechanicalSourcedNonLocalVariable,dmechanicalSourcedT,dmechanicalSourcedgradT,
			    stiff,elasticTangent);
  }
  else{
    Msg::Error("mlawNonLocalDamageNonLinearTVEVPWithFailure numerical tangent not implemented");
  }
};

// ================================================================================================= END CLASS 3 ===============================================================================
