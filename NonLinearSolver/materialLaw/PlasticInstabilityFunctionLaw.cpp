//
// C++ Interface: Plastic instability function law
//
//
// Author:  <L. Noels>, (C) 2020
//
// Copyright: See COPYING file that comes with this distribution
//
#include <math.h>       /* exp */
#include <stddef.h>
#include "PlasticInstabilityFunctionLaw.h"


ExponentialPlasticInstabilityFunctionLaw::ExponentialPlasticInstabilityFunctionLaw( const int num, const double eps0, const double deltaY ) :
  PlasticInstabilityFunctionLaw(num), eps0_(eps0), deltaY_(deltaY)
{
  // Nothing more to do.
};


ExponentialPlasticInstabilityFunctionLaw::ExponentialPlasticInstabilityFunctionLaw( const ExponentialPlasticInstabilityFunctionLaw& source ) :
  PlasticInstabilityFunctionLaw(source), eps0_(source.eps0_), deltaY_(source.deltaY_)
{
  // Nothing more to do.
};


ExponentialPlasticInstabilityFunctionLaw& ExponentialPlasticInstabilityFunctionLaw::operator= ( const PlasticInstabilityFunctionLaw& source )
{
  PlasticInstabilityFunctionLaw::operator =( source );
  const ExponentialPlasticInstabilityFunctionLaw* source_casted = dynamic_cast< const ExponentialPlasticInstabilityFunctionLaw* >( &source );
  if( source_casted != NULL )
  {
    eps0_   = source_casted->eps0_;
    deltaY_ = source_casted->deltaY_;

  }
  return *this;
};


