//
// Description: define material strength in function of internal variables
//              in order to model the evolution of the failure surface
//
// Author:  <V.D. Nguyen>, (C) 2015
//
// Copyright: See COPYING file that comes with this distribution
//
//

#include "materialStrengthLaw.h"
#include "math.h"


RateFailureLaw::RateFailureLaw(const int num, const bool init): _num(num),_init(init){};

RateFailureLaw::RateFailureLaw(const RateFailureLaw& src):_num(src._num),_init(src._init){};

RateFailureLaw& RateFailureLaw::operator = (const RateFailureLaw& src){
  _num = src._num;
  _init = src._init;
return *this;
};
RateFailureLaw::~RateFailureLaw(){}


ConstantFailureLaw::ConstantFailureLaw(const int num, const double X, const bool init):RateFailureLaw(num,init),_X(X){};


PowerFailureLaw::PowerFailureLaw(const int num, const double X0, const double A, const double alp, const bool init): RateFailureLaw(num,init),
      _X0(X0),_A(A), _alp(alp){}
PowerFailureLaw::PowerFailureLaw(const PowerFailureLaw& src): RateFailureLaw(src), _X0(src._X0),_A(src._A),_alp(src._alp){}
PowerFailureLaw& PowerFailureLaw::operator =(const RateFailureLaw& src){
  RateFailureLaw::operator=(src);
  const PowerFailureLaw* psrc = dynamic_cast<const PowerFailureLaw*>(&src);
  if (psrc != NULL){
    _X0 = psrc->_X0;
    _A = psrc->_A;
    _alp = psrc->_alp;
  }
  return *this;
}
PowerFailureLaw::~PowerFailureLaw(){};
void PowerFailureLaw::get(const double rate, double& R, double& dR) const {
  if (rate <  1e-20){
    R = _X0;
    dR = 0.;
  }
  else{
    R = _X0 + _A*pow(rate,_alp);
    dR = _A*_alp*pow(rate,_alp-1.);
  }

};



LograrithmicFailureLaw::LograrithmicFailureLaw(const int num, const double Xr, const double rr, const double A, const bool init): RateFailureLaw(num,init),
      _Xr(Xr),_rr(rr), _Ar(A){}
LograrithmicFailureLaw::LograrithmicFailureLaw(const LograrithmicFailureLaw& src): RateFailureLaw(src), _Xr(src._Xr),_rr(src._rr),_Ar(src._Ar){}
LograrithmicFailureLaw& LograrithmicFailureLaw::operator =(const RateFailureLaw& src){
  RateFailureLaw::operator=(src);
  const LograrithmicFailureLaw* psrc = dynamic_cast<const LograrithmicFailureLaw*>(&src);
  if (psrc != NULL){
    _Xr = psrc->_Xr;
    _rr = psrc->_rr;
    _Ar = psrc->_Ar;
  }
  return *this;
}
LograrithmicFailureLaw::~LograrithmicFailureLaw(){};
void LograrithmicFailureLaw::get(const double rate, double& R, double& dR) const {
  if (rate == 0){
    R = _Xr;
    if (_Ar == 0)
      dR = 0.;
    else
      dR = 1e10;
  }
  else{
    R = _Xr*(1.+_Ar*log(rate/_rr));
    dR = _Xr*_Ar/rate;
  }
};
