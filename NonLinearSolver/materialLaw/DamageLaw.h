//
// C++ Interface: terms
//
// Description: Define damage law for non local j2 plasticity
//
//
// Author:  <L. Noels>, (C) 2013
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef _DAMAGELAW_H_
#define _DAMAGELAW_H_
#ifndef SWIG
#include "ipstate.h"
#include "MElement.h"
#include "ipDamage.h"
#include "STensor43.h"
#endif
#include "scalarFunction.h"
class DamageLaw{
 public :
  enum damagelawname{lemaitrechabochedamagelaw,powerdamagelaw,saturatedamagelaw,exponentialLaw,sigmoidLaw,powerbrittlelaw,smallstrainlemaitrechabochedamagelaw};
  
 protected :
  int _num; // number of law (must be unique !)
  bool _initialized; // to initialize law
  double _fscmin,_fscmax; // factor max; min
  
public:
  void setMinMaxFactor(const double ma, const double m);
  double getFactor() const;
  // constructor
#ifndef SWIG
  DamageLaw(const int num, const bool init=true);
  virtual ~DamageLaw(){}
  DamageLaw(const DamageLaw &source);
  virtual DamageLaw& operator=(const DamageLaw &source);
  virtual int getNum() const{return _num;}
  virtual damagelawname getType() const=0;
  virtual void createIPVariable(IPDamage* &ipv) const=0;
  virtual void initLaws(const std::map<int,DamageLaw*> &maplaw)=0;
  virtual const bool isInitialized() {return _initialized;}
  virtual void computeDamage(double p1, double p0, double phiel, const STensor3 &Fe,
                             const STensor3 &Fp, const STensor3 &Peff,
                             const STensor43 &Cel, const IPDamage &ipvprev, IPDamage &ipvcur) const=0;
  virtual DamageLaw * clone() const=0;
#endif
};

class LemaitreChabocheDamageLaw : public DamageLaw
{
 public :
 protected :
  double pInit, pc, n, alpha;

 public:
  // constructor
  LemaitreChabocheDamageLaw(const int num, const double _p0, const double _pc, const double _n,
                            const double _alpha, const bool init=true);
	#ifndef SWIG
	virtual ~LemaitreChabocheDamageLaw(){}
  LemaitreChabocheDamageLaw(const LemaitreChabocheDamageLaw &source);
  LemaitreChabocheDamageLaw& operator=(const DamageLaw &source);
  virtual damagelawname getType() const {return DamageLaw::lemaitrechabochedamagelaw;};
  virtual void createIPVariable(IPDamage* &ipv) const;
  virtual void initLaws(const std::map<int,DamageLaw*> &maplaw){}; //nothing now, we will see if we use the mapping;
  virtual void computeDamage(double p1, double p0, double phiel, const STensor3 &Fe,
                           const STensor3 &Fp, const STensor3 &Peff,
                             const STensor43 &Cel, const IPDamage &ipvprev, IPDamage &ipvcur) const;
  virtual DamageLaw * clone() const;
#endif
};

class SmallStrainLemaitreChabocheDamageLaw : public DamageLaw
{
 public :
 protected :
  double pInit, pc, n, alpha;

 public:
  // constructor
  SmallStrainLemaitreChabocheDamageLaw(const int num, const double _p0, const double _pc, const double _n,
                            const double _alpha, const bool init=true);
	#ifndef SWIG
	virtual ~SmallStrainLemaitreChabocheDamageLaw(){}
  SmallStrainLemaitreChabocheDamageLaw(const SmallStrainLemaitreChabocheDamageLaw &source);
  SmallStrainLemaitreChabocheDamageLaw& operator=(const DamageLaw &source);
  virtual damagelawname getType() const {return DamageLaw::smallstrainlemaitrechabochedamagelaw;};
  virtual void createIPVariable(IPDamage* &ipv) const;
  virtual void initLaws(const std::map<int,DamageLaw*> &maplaw){}; //nothing now, we will see if we use the mapping;
  virtual void computeDamage(double p1, double p0, double phiel, const STensor3 &Fe,
                           const STensor3 &Fp, const STensor3 &Peff,
                             const STensor43 &Cel, const IPDamage &ipvprev, IPDamage &ipvcur) const;
  virtual DamageLaw * clone() const;
#endif
};




// ========== Power Damage Law ==========
class PowerDamageLaw : public DamageLaw
{
 // D = 1- (_pi/p)^beta * ((_p-_pi)/(_pc-_pi))^_alpha
 public:
 protected:
  double _pi, _pc;        // Threshold and critical/failure values of memory variable
  double _alpha, _beta;   // Exponents of damage model

 public:
  // Constructors & Destructor
  PowerDamageLaw(const int num, const double pi, const double pc, const double alpha,
                            const double beta, const bool init=true);
#ifndef SWIG
	virtual ~PowerDamageLaw(){}
  PowerDamageLaw(const PowerDamageLaw &source);
  PowerDamageLaw& operator=(const DamageLaw &source);

  // General functions
  virtual damagelawname getType() const {return DamageLaw::powerdamagelaw;};
  virtual void createIPVariable(IPDamage* &ipv) const;
  virtual void initLaws(const std::map<int,DamageLaw*> &maplaw){};
  virtual DamageLaw * clone() const;

  // Specific functions
  virtual void computeDamage(const double p1, const double p0, double phiel, const STensor3 &Fe,
                             const STensor3 &Fp, const STensor3 &Peff,
                             const STensor43 &Cel, const IPDamage &ipvprev, IPDamage &ipvcur) const;
#endif
};



class ExponentialDamageLaw : public DamageLaw{
  // D = 1-[(pi+b)/(p+b)]^alpha*exp(-K*(p-pi))
  //
  protected:
    double _K, _pi, _b,_alpha;


  public:
    ExponentialDamageLaw(const int num, const double pinit, const double b, const double K, const double alp =1, const bool init = true);
    #ifndef SWIG
    ExponentialDamageLaw(const ExponentialDamageLaw &source);
    ExponentialDamageLaw& operator=(const DamageLaw &source);
		virtual ~ExponentialDamageLaw(){}
    virtual damagelawname getType() const {return DamageLaw::exponentialLaw;};
    virtual void createIPVariable(IPDamage* &ipv) const;
    virtual void initLaws(const std::map<int,DamageLaw*> &maplaw){}; //nothing now, we will see if we use the mapping;
    virtual void computeDamage(double p1, double p0, double phiel, const STensor3 &Fe,
                             const STensor3 &Fp, const STensor3 &Peff,
                             const STensor43 &Cel, const IPDamage &ipvprev, IPDamage &ipvcur) const;
    virtual DamageLaw * clone() const{return new ExponentialDamageLaw(*this);};
    #endif // SWIG
};

class SigmoidDamageLaw : public DamageLaw{
  // D = Dmax  / ( 1+exp( beta*(pc-p0) ) -D0) / (1-D0)
  // D0 = 1. / (1+exp(beta pc) )
  protected:
    double _beta, _pc, _Dmax;


  public:
    SigmoidDamageLaw(const int num, const double pc, const double beta, const double Dmax=1., const bool init = true);
    #ifndef SWIG
    SigmoidDamageLaw(const SigmoidDamageLaw &source);
    SigmoidDamageLaw& operator=(const DamageLaw &source);
    virtual ~SigmoidDamageLaw(){}
    virtual damagelawname getType() const {return DamageLaw::sigmoidLaw;};
    virtual void createIPVariable(IPDamage* &ipv) const;
    virtual void initLaws(const std::map<int,DamageLaw*> &maplaw){}; //nothing now, we will see if we use the mapping;
    virtual void computeDamage(double p1, double p0, double phiel, const STensor3 &Fe,
                             const STensor3 &Fp, const STensor3 &Peff,
                             const STensor43 &Cel, const IPDamage &ipvprev, IPDamage &ipvcur) const;
    virtual DamageLaw * clone() const{return new SigmoidDamageLaw(*this);};
    #endif // SWIG
};

class ThreeParametersExponentialDamageLaw : public DamageLaw{
  // D =  1- (pi/p)^gamma*(1-alpha + alpha*exp(-beta*(p-pi)))
  //
  protected:
    double _alpha, _beta, _gamma, _pi;


  public:
    ThreeParametersExponentialDamageLaw(const int num, const double pinit, const double a, const double b, const double g);
    #ifndef SWIG
    ThreeParametersExponentialDamageLaw(const ThreeParametersExponentialDamageLaw &source);
    ThreeParametersExponentialDamageLaw& operator=(const DamageLaw &source);
		virtual ~ThreeParametersExponentialDamageLaw(){}
    virtual damagelawname getType() const {return DamageLaw::exponentialLaw;};
    virtual void createIPVariable(IPDamage* &ipv) const;
    virtual void initLaws(const std::map<int,DamageLaw*> &maplaw){}; //nothing now, we will see if we use the mapping;
    virtual void computeDamage(double p1, double p0, double phiel, const STensor3 &Fe,
                             const STensor3 &Fp, const STensor3 &Peff,
                             const STensor43 &Cel, const IPDamage &ipvprev, IPDamage &ipvcur) const;
    virtual DamageLaw * clone() const{return new ThreeParametersExponentialDamageLaw(*this);};
    #endif // SWIG
};



/*D = Dinf(1-exp(-H*(p-pInit)^alpha))*/
class SimpleSaturateDamageLaw: public DamageLaw{
protected :
  double H, Dinf, pInit, alpha;

 public:
  SimpleSaturateDamageLaw(const int num, const double Hh, const double Dinfty, const double pi, const double alpha, const bool init=true);
  #ifndef SWIG
  virtual ~SimpleSaturateDamageLaw(){}
  SimpleSaturateDamageLaw(const SimpleSaturateDamageLaw &source);
  SimpleSaturateDamageLaw& operator=(const DamageLaw &source);
  virtual damagelawname getType() const {return SimpleSaturateDamageLaw::saturatedamagelaw;};
  virtual void createIPVariable(IPDamage* &ipv) const;
  virtual void initLaws(const std::map<int,DamageLaw*> &maplaw){}; //nothing now, we will see if we use the mapping;
  virtual void computeDamage(double p1, double p0, double phiel, const STensor3 &Fe,
                             const STensor3 &Fp, const STensor3 &Peff,
                             const STensor43 &Cel, const IPDamage &ipvprev, IPDamage &ipvcur) const;
  virtual DamageLaw * clone() const;
  #endif
};

//
// dD = H (p)^m (1-D)^{-n} dp
// D = 1-(1- H*(n+1)/(m+1) * (p^{m+1}-pi^{m+1}))^{1/(m+1)}
// H = (m+1)/(n+1) * 1/(pf^{m+1} - pi^{m+1})
class PowerBrittleDamagelaw : public DamageLaw{
  private:
    #ifndef SWIG
    double getP(const double D) const;
    #endif //SWIG
    
  protected:
    double _pi; // damage onset --> damage = 0
    double _pf; // total failure--> damage = Dc
    double _m; // strain power
    double _n; // damage power
    double _H; // compute from others
    double _Dc;
    scalarFunction* _regFunction;

  public:
    PowerBrittleDamagelaw(const int n, const double pi, const double pf, const double m, const double nn, const bool init = true);
    void setRegulizedFunction(const scalarFunction& fct);
    virtual void setCriticalDamage(const double Dc);
    #ifndef SWIG
    PowerBrittleDamagelaw(const PowerBrittleDamagelaw& src);
    virtual PowerBrittleDamagelaw& operator=(const DamageLaw& src);

    virtual ~PowerBrittleDamagelaw(){
      if (_regFunction) {delete _regFunction; _regFunction = NULL;}
    }
    virtual damagelawname getType() const {return SimpleSaturateDamageLaw::powerbrittlelaw;};
    virtual void createIPVariable(IPDamage* &ipv) const;
    virtual void initLaws(const std::map<int,DamageLaw*> &maplaw){}; //nothing now, we will see if we use the mapping;
    virtual void computeDamage(double p1, double p0, double phiel, const STensor3 &Fe,
                               const STensor3 &Fp, const STensor3 &Peff,
                               const STensor43 &Cel, const IPDamage &ipvprev, IPDamage &ipvcur) const;
    virtual DamageLaw * clone() const;
    
  
    #endif // SWIG
};

#endif //DAMAGELAW_H_
