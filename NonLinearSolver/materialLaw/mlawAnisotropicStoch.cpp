//
// C++ Interface: material law
//
// Description: AnisotropicStoch law
//
//
// Author:  <L.WU>, (C) 2017
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "mlawAnisotropicStoch.h"
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include "MInterfaceElement.h"

mlawAnisotropicStoch::mlawAnisotropicStoch(const int num, const double rho, const double alpha, const double beta, 
                                          const double gamma, const double OrigX, const double OrigY, const char *propName, const int intpl) : materialLaw(num,true),
                                          _rho(rho), _alpha(alpha), _beta(beta), _gamma(gamma), _OrigX(OrigX), _OrigY(OrigY), _intpl(intpl)
{
   double Anisoprop[9];
   int nxy[2];

   FILE *Props = fopen(propName, "r");
   if ( Props != NULL ){
     int okf = fscanf(Props, "%d %d\n", &nxy[0], &nxy[1]);

     bool resizeFlag = _ExMat.resize(nxy[0], nxy[1], true);
     resizeFlag = _EyMat.resize(nxy[0], nxy[1], true);
     resizeFlag = _EzMat.resize(nxy[0], nxy[1], true);

     resizeFlag = _VxyMat.resize(nxy[0], nxy[1], true);
     resizeFlag = _VxzMat.resize(nxy[0], nxy[1], true);
     resizeFlag = _VyzMat.resize(nxy[0], nxy[1], true);
     resizeFlag = _MUxyMat.resize(nxy[0], nxy[1], true);
     resizeFlag = _MUxzMat.resize(nxy[0], nxy[1], true);
     resizeFlag = _MUyzMat.resize(nxy[0], nxy[1], true);
     okf = fscanf(Props, "%lf %lf\n", &_dx, &_dy);

     if (resizeFlag){
       for(int i=0; i<nxy[0]; i++){
          for(int j=0;j<nxy[1];j++){
             okf = fscanf(Props, "%lf %lf %lf %lf %lf %lf %lf %lf %lf\n", &Anisoprop[0], &Anisoprop[1],&Anisoprop[2],
                          &Anisoprop[3],&Anisoprop[4],&Anisoprop[5],&Anisoprop[6],&Anisoprop[7],&Anisoprop[8]);

             _ExMat.set(i, j, Anisoprop[0]);
             _EyMat.set(i, j, Anisoprop[1]);
             _EzMat.set(i, j, Anisoprop[2]);
             _VxyMat.set(i, j, Anisoprop[3]);
             _VxzMat.set(i, j, Anisoprop[4]);
             _VyzMat.set(i, j, Anisoprop[5]);
             _MUxyMat.set(i, j, Anisoprop[6]);
             _MUxzMat.set(i, j, Anisoprop[7]);
             _MUyzMat.set(i, j, Anisoprop[8]);
          }
       }
     }
     else{
         printf("Couldn't resize the property matrices.\n");
     }
     fclose(Props);
   }
   else{
        printf("Property file is not usable.\n");
   } 

   const double Ex = _ExMat(0,0);
   const double Ey = _EyMat(0,0);
   const double Ez = _EzMat(0,0);

   const double Vxy = _VxyMat(0,0);
   const double Vxz = _VxzMat(0,0);
   const double Vyz = _VyzMat(0,0);

   const double MUxy = _MUxyMat(0,0);
   const double MUxz = _MUxzMat(0,0);
   const double MUyz = _MUyzMat(0,0);           

   AnisoElasticTensor(Ex, Ey, Ez, Vxy, Vxz, Vyz, MUxy, MUxz, MUyz, _alpha, _beta, _gamma, _ElasticityTensor);

  //Init du _poissonMax
  if (Vxy >=  Vxz && Vxy >= Vyz) {_poissonMax=Vxy;}
  else if (Vxz >=  Vyz && Vxz >= Vxy) {_poissonMax=Vxz;}
  else {_poissonMax=Vyz;}

}



mlawAnisotropicStoch::mlawAnisotropicStoch(const mlawAnisotropicStoch &source) : materialLaw(source),_ElasticityTensor(source._ElasticityTensor),
                                                       _rho(source._rho),_poissonMax(source._poissonMax),_alpha(source._alpha),_beta(source._beta), 
                                                       _gamma(source._gamma), _ExMat(source._ExMat), _EyMat(source._EyMat), _EzMat(source._EzMat), 
                                                       _VxyMat(source._VxyMat), _VxzMat(source._VxzMat), _VyzMat(source._VyzMat), 
                                                       _MUxyMat(source._MUxyMat), _MUxzMat(source._MUxzMat), _MUyzMat(source._MUyzMat), _dx(source._dx),
                                                       _dy(source._dy), _OrigX(source._OrigX), _OrigY(source._OrigY),_intpl(source._intpl) {}

mlawAnisotropicStoch& mlawAnisotropicStoch::operator=(const materialLaw &source)
{
  materialLaw::operator=(source);
  const mlawAnisotropicStoch* src =dynamic_cast<const mlawAnisotropicStoch*>(&source);
  if (src != NULL){
    _ElasticityTensor = src->_ElasticityTensor;
    _rho = src->_rho;
    _poissonMax = src->_poissonMax;
    _alpha = src->_alpha;
    _beta = src->_beta;
    _gamma = src->_gamma;

    _ExMat = src->_ExMat;
    _EyMat = src->_EyMat;
    _EzMat = src->_EzMat;

    _VxyMat = src->_VxyMat;
    _VxzMat = src->_VxzMat;
    _VyzMat = src->_VyzMat;
   
    _MUxyMat = src->_MUxyMat;
    _MUxzMat = src->_MUxzMat;
    _MUyzMat = src->_MUyzMat;

    _dx = src->_dx;
    _dy = src->_dy;
    _OrigX = src->_OrigX;
    _OrigY = src->_OrigY;
    _intpl = src->_intpl;
  }
  return *this;
}

void  mlawAnisotropicStoch::AnisoElasticTensor(const double Ex, const double Ey, const double Ez, const double Vxy, const double Vxz, const double Vyz,
                                 const double MUxy, const double MUxz, const double MUyz, const double alpha, const double beta, 
                                 const double gamma, STensor43 &ElastTensor) const{
   double Vyx= Vxy*Ey/Ex; 
   double Vzx= Vxz*Ez/Ex; 
   double Vzy= Vyz*Ez/Ey;
   double D=( 1.0-Vxy*Vyx-Vzy*Vyz-Vxz*Vzx-2.0*Vxy*Vyz*Vzx ) / ( Ex*Ey*Ez );

   static STensor43 ElasticityTensor; 
   STensorOperation::zero(ElasticityTensor);
   ElasticityTensor(0,0,0,0)=( 1-Vyz*Vzy ) / (Ey*Ez*D );
   ElasticityTensor(1,1,1,1)=( 1-Vxz*Vzx ) / (Ex*Ez*D );
   ElasticityTensor(2,2,2,2)=( 1-Vyx*Vxy ) / (Ey*Ex*D );

   ElasticityTensor(0,0,1,1)=( Vyx+Vzx*Vyz ) / (Ey*Ez*D );
   ElasticityTensor(0,0,2,2)=( Vzx+Vyx*Vzy ) / (Ey*Ez*D );
   ElasticityTensor(1,1,2,2)=( Vzy+Vxy*Vzx ) / (Ex*Ez*D );

   ElasticityTensor(1,1,0,0)=( Vxy+Vzy*Vxz ) / (Ex*Ez*D );
   ElasticityTensor(2,2,0,0)=( Vxz+Vxy*Vyz ) / (Ey*Ex*D );
   ElasticityTensor(2,2,1,1)=( Vyz+Vxz*Vyx ) / (Ey*Ex*D );

   ElasticityTensor(1,2,1,2)=MUyz;  ElasticityTensor(1,2,2,1)=MUyz;
   ElasticityTensor(2,1,2,1)=MUyz;  ElasticityTensor(2,1,1,2)=MUyz;

   ElasticityTensor(0,1,0,1)=MUxy;  ElasticityTensor(0,1,1,0)=MUxy;
   ElasticityTensor(1,0,1,0)=MUxy;  ElasticityTensor(1,0,0,1)=MUxy;

   ElasticityTensor(0,2,0,2)=MUxz;  ElasticityTensor(0,2,2,0)=MUxz;
   ElasticityTensor(2,0,2,0)=MUxz;  ElasticityTensor(2,0,0,2)=MUxz;

   
   double c1,c2,c3,s1,s2,s3;
   double s1c2, c1c2;
   double pi(3.14159265359);
   double fpi = pi/180.;

   c1 = cos(alpha*fpi);
   s1 = sin(alpha*fpi);

   c2 = cos(beta*fpi);
   s2 = sin(beta*fpi);

   c3 = cos(gamma*fpi);
   s3 = sin(gamma*fpi);

   s1c2 = s1*c2;
   c1c2 = c1*c2;
  
   static STensor3 R;		//3x3 rotation matrix
   R(0,0) = c3*c1 - s1c2*s3;
   R(0,1) = c3*s1 + c1c2*s3;
   R(0,2) = s2*s3;

   R(1,0) = -s3*c1 - s1c2*c3;
   R(1,1) = -s3*s1 + c1c2*c3;
   R(1,2) = s2*c3;

   R(2,0) = s1*s2;
   R(2,1) = -c1*s2;
   R(2,2) = c2;

  for(int i=0;i<3;i++){
    for(int j=0;j<3;j++){
      for(int k=0;k<3;k++){
         for(int l=0;l<3;l++){
            ElastTensor(i,j,k,l)=0.;
            for(int m=0;m<3;m++){
               for(int n=0;n<3;n++){
              	  for(int o=0;o<3;o++){
                     for(int p=0;p<3;p++){
                  	 ElastTensor(i,j,k,l)+=R(m,i)*R(n,j)*R(o,k)*R(p,l)*ElasticityTensor(m,n,o,p);
                     }
              	  }
               }
            }
       	 }
      }
    }
  } 
} 

void mlawAnisotropicStoch::createIPState(IPStateBase* &ips, bool hasBodyForce, const bool* state_,const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
  bool inter=true;
  const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
  if(iele==NULL) inter=false;

  static SPoint3 pt_Global;
  ele->pnt(GP->pt[0],GP->pt[1],GP->pt[2],pt_Global);
  static SVector3 GaussP;
  GaussP[0] = pt_Global[0];
  GaussP[1] = pt_Global[1];
  GaussP[2] = pt_Global[2];
  

  IPVariable* ipvi = new IPAnisotropicStoch(GaussP, _ExMat, _EyMat, _EzMat, _VxyMat, _VxzMat, _VyzMat, _MUxyMat, _MUxzMat, _MUyzMat, _dx, _dy,
                                                  _OrigX, _OrigY, _intpl);
  IPVariable* ipv1 = new IPAnisotropicStoch(GaussP, _ExMat, _EyMat, _EzMat, _VxyMat, _VxzMat, _VyzMat, _MUxyMat, _MUxzMat, _MUyzMat, _dx, _dy,
                                                  _OrigX, _OrigY, _intpl);
  IPVariable* ipv2 = new IPAnisotropicStoch(GaussP, _ExMat, _EyMat, _EzMat, _VxyMat, _VxzMat, _VyzMat, _MUxyMat, _MUxzMat, _MUyzMat, _dx, _dy,
                                                  _OrigX, _OrigY, _intpl);
  if(ips != NULL) delete ips;
  ips = new IP3State(state_,ipvi,ipv1,ipv2);
}


double mlawAnisotropicStoch::poissonRatio() const
{
	return _poissonMax;
}

double mlawAnisotropicStoch::shearModulus() const
{
	if (_ElasticityTensor(1,2,1,2) >=  _ElasticityTensor(0,1,0,1) && _ElasticityTensor(1,2,1,2) >= _ElasticityTensor(2,0,2,0))
		{return _ElasticityTensor(1,2,1,2);}
	else if (_ElasticityTensor(0,1,0,1) >=  _ElasticityTensor(2,0,2,0) && _ElasticityTensor(0,1,0,1) >= _ElasticityTensor(1,2,1,2))
		{return _ElasticityTensor(0,1,0,1);}
	else
		{return _ElasticityTensor(2,0,2,0);}
}

double mlawAnisotropicStoch::soundSpeed() const
{
  double nu=poissonRatio();
  double factornu=(1.-nu) / ( (1.+nu) * (1.-2.*nu) ) ;
  double E(0.);

	if (_ElasticityTensor(0,0,0,0) >=  _ElasticityTensor(1,1,1,1) && _ElasticityTensor(0,0,0,0) >= _ElasticityTensor(2,2,2,2))
		{double E=_ElasticityTensor(0,0,0,0);}
	else if (_ElasticityTensor(1,1,1,1) >=  _ElasticityTensor(2,2,2,2) && _ElasticityTensor(1,1,1,1) >= _ElasticityTensor(0,0,0,0))
		{double E=_ElasticityTensor(1,1,1,1);}
	else
		{double E=_ElasticityTensor(2,2,2,2);}	

  return sqrt(E*factornu/_rho);
}


void mlawAnisotropicStoch::constitutive(const STensor3& F0,
                                        const STensor3& Fn,STensor3 &P,
                                        const IPVariable *q0i, 
                                        IPVariable *q1i,
                                        STensor43 &Tangent,
                                        const bool stiff, 
                                        STensor43* elasticTangent, 
                                        const bool dTangent,
                                        STensor63* dCalgdeps) const
{
    const IPAnisotropicStoch *q0= dynamic_cast<const IPAnisotropicStoch *> (q0i);
          IPAnisotropicStoch *q1= dynamic_cast<IPAnisotropicStoch *> (q1i);

    static STensor3 FnT;
    STensorOperation::transposeSTensor3(Fn,FnT);
    static STensor43 ElastTensor;
    STensorOperation::zero(ElastTensor);
    this->AnisoElasticTensor(q0->getEx(), q0->getEy(), q0->getEz(), q0->getVxy(), q0->getVxz(), q0->getVyz(), q0->getMUxy(), 
                             q0->getMUxz(), q0->getMUyz(), _alpha, _beta, _gamma, ElastTensor);

    static STensor3 defo;
    defo=(FnT); // static
    defo+=Fn;
    defo*=0.5;
    defo(0,0)-=1.; 
    defo(1,1)-=1.; 
    defo(2,2)-=1.; 

    STensorOperation::zero(P);
    for(int i=0;i<3;i++)
    {
      for(int j=0;j<3;j++)
      {
        for(int k=0;k<3;k++)
        {
          for(int l=0;l<3;l++)
          {
            P(i,j)+= ElastTensor(i,j,k,l)*defo(k,l);
          }
        }
      }
    }

    q1->setElasticEnergy(deformationEnergy(defo,P));
    if(stiff)
      Tangent = ElastTensor;
    if(elasticTangent!=NULL)
      elasticTangent->operator=(ElastTensor);
}

double mlawAnisotropicStoch::deformationEnergy(const STensor3 &defo, const STensor43& ElastTensor) const // If i need to compute sigma
{
    static STensor3 sigma;
    STensorOperation::zero(sigma);
    //These 'fors' can be avoided with the surcharged function 
    for(int i=0;i<3;i++)
    {
      for(int j=0;j<3;j++)
      {
        for(int k=0;k<3;k++)
        {
          for(int l=0;l<3;l++)
          {
            sigma(i,j)+= ElastTensor(i,j,k,l)*defo(k,l);
          }
        }
      }
    }
  static STensor3 defoT,defoTSigma;
  STensorOperation::transposeSTensor3(defo,defoT);
  STensorOperation::multSTensor3(defoT, sigma, defoTSigma);
  double En=defoTSigma.trace();

  return 0.5*En;  			// W=0.5*trace(epsilon^T*sigma)

}

double mlawAnisotropicStoch::deformationEnergy(const STensor3 &defo, const STensor3& sigma) const // If i have sigma ...
{

  static STensor3 defoT,defoTSigma;
  STensorOperation::transposeSTensor3(defo,defoT);
  STensorOperation::multSTensor3(defoT, sigma, defoTSigma);
  double En=defoTSigma.trace();

  return 0.5*En;  			// W=0.5*trace(epsilon^T*sigma)
}


