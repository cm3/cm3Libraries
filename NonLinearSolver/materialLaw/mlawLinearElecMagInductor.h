//
// C++ Interface: material law for inductor region in ElecMag problem
// computing normal direction to a gauss point to impose current
// density js0 in inductor
//              
// Author:  <Vinayak GHOLAP>, (C) 2020
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef MLAWLINEARELECMAGINDUCTOR_H_
#define MLAWLINEARELECMAGINDUCTOR_H_
#include "mlawLinearElecMagTherMech.h"
#include "ipLinearElecMagInductor.h"

class mlawLinearElecMagInductor : public mlawLinearElecMagTherMech 
{
 protected:
  SVector3 _Centroid;
  SVector3 _CentralAxis;
  bool _useFluxT;   // if true thermal problem formulated using heat flux q
                    // else using thermal energy flux jy

  // designed to be used for coupled simulations
  // between EM and TM solvers with internal state data transfer
  bool _evaluateCurlField;  // if true curl field and relevant variables
                            // are computed (eg ThermalEMFieldSource, fluxje etc)
                            // else imported values from other solver used

 public:
  mlawLinearElecMagInductor(int num,const double rho, const double Ex, const double Ey, const double Ez,
  const double Vxy, const double Vxz, const double Vyz, const double MUxy, const double MUxz,
  const double MUyz,  const double alpha, const double beta, const double gamma,
  const double t0,const double Kx,const double Ky, const double Kz,const double alphax,
  const double alphay,const double alphaz, const  double lx,const  double ly,
  const  double lz,const double seebeck,const double cp,const double v0,
  const double mu_x, const double mu_y, const double mu_z, const double A0_x, const double A0_y,
  const double A0_z, const double Irms, const double freq, const unsigned int nTurnsCoil, 
  const double coilLength_x, const double coilLength_y, const double coilLength_z, const double coilWidth, 
  const double Centroid_x, const double Centroid_y, const double Centroid_z, 
  const double CentralAxis_x, const double CentralAxis_y, const double CentralAxis_z, const bool useFluxT,
  const bool evaluateCurlField = true);
 #ifndef SWIG
  mlawLinearElecMagInductor(const mlawLinearElecMagInductor &source);
  mlawLinearElecMagInductor& operator=(const materialLaw &source);
	virtual ~mlawLinearElecMagInductor(){}
	
	virtual materialLaw* clone() const {return new mlawLinearElecMagInductor(*this);}
  virtual bool withEnergyDissipation() const {return false;}

  // function of materialLaw
  virtual matname getType() const{return materialLaw::LinearElecMagInductor;}
  virtual void createIPState(IPStateBase* &ips, bool hasBodyForce, const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const IntPt *GP=NULL, const int gpt =0) const;

  virtual void getNormalDirection(SVector3 &normal, const IPLinearElecMagTherMech *q1) const 
  {
     const IPLinearElecMagInductor *q = dynamic_cast<const IPLinearElecMagInductor*>(q1);
     if(q != NULL)
     {
        q->getNormalDirection(normal); 
     }
  }
  
  SVector3 getCentroid() const { return _Centroid;}
  SVector3 getCentralAxis() const { return _CentralAxis;}
  virtual bool isInductor() const {return true;}
 #endif // SWIG
};

#endif // MLAWLINEARELECMAGINDUCTOR_H_
