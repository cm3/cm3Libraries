//
// C++ Interface: Material Law
//
// Description: Non-Linear Thermo-Visco-Mechanics (Thermo-ViscoElasto-ViscoPlasto Law) with Non-Local Damage Interface (soon....?)
//
// Author: <Ujwal Kishore J - FLE_Knight>, (C) 2024
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef MLAWNONLINEARTVENONLINEARTVP_H_
#define MLAWNONLINEARTVENONLINEARTVP_H_

#include "ipNonLinearTVP.h"
#include "mlawNonLinearTVP.h"

class mlawNonLinearTVENonLinearTVP : public mlawNonLinearTVP{

    protected:
		double _stressIteratorTol;

    protected:
		virtual double freeEnergyPlasticity(const IPNonLinearTVP *q0, IPNonLinearTVP *q1, const double& T0, const double& T) const;

		virtual void freeEnergyPlasticityDerivatives(const IPNonLinearTVP *q0, IPNonLinearTVP *q1, const double& T0, const double& T, STensor3& dPsy_TVPdF, double& dPsy_TVPdT) const;

        virtual void getIterated_DPhi(const STensor3& F, const double& T0, const double& T, const IPNonLinearTVP *q0, IPNonLinearTVP *q1,
                                            double& Gamma, const double& Cxtr, const double& Cxdev,
                                            const STensor3& Cepr, const STensor3& Eepr,
                                            const double& trXn, const STensor3& devXn,
                                            double& Ke, double& Ge, STensor43& Ge_Tensor,
                                            double& ptilde, STensor3& devPhi,
                                            STensor3& Phi, STensor3& N, STensor3& expGN, STensor43& dexpAdA,
                                            STensor43& Dho, STensor43& Dho2inv, STensor43& Dho2_u_inv, double& Dho2_v_inv, STensor3& Fp1,
                                            STensor43* Ge_TrEeTensor = NULL, STensor43* rotationStiffness = NULL, STensor3* devXns = NULL, STensor43* backStressRotationStiffness = NULL) const;

        virtual void getDho(const double& Gamma, const STensor3& Cepr, const STensor3& Ceinvpr, const STensor3& KS,
                                  const double& Ke, const STensor43& Ge_Tensor,
                                  const double& kk, const double& Hb, const double& Cxtr, const double& Cxdev,
                                  const STensor3& expGN, const STensor43& dexpAdA,
                                  STensor43& Dho, STensor43& Dho2inv, STensor43& Dho2_u_inv, double& Dho2_v_inv,
								  STensor43* Ge_TrEeTensor = NULL, STensor43* rotationStiffness = NULL, STensor43* backStressRotationStiffness = NULL) const;

        virtual void get_G1_Tensor(const STensor3& Cepr, const STensor3& expGN, const STensor43& DCeinvprDCepr, const STensor3& KS, STensor43& G1) const;

        virtual void predictorCorrector_TVP_nonAssociatedFlow_nonLinearTVE(const STensor3& F0, const STensor3& F, const IPNonLinearTVP *q0_, IPNonLinearTVP *q1,
                            STensor3&P, const bool stiff, STensor43& Tangent,  STensor43& dFedF, STensor43& dFpdF, STensor3& dFedT, STensor3& dFpdT,
                            const double T0, const double T1,
                            const SVector3 &gradT0,                 // previous temperature gradient
                            const SVector3 &gradT,                  // temperature gradient
                                  SVector3 &fluxT,                  // temperature flux
                                  STensor3 &dPdT,                   // mechanical-thermal coupling
                                  STensor3 &dfluxTdgradT,           // thermal tengent
                                  SVector3 &dfluxTdT,
                                  STensor33 &dfluxTdF,              // thermal-mechanical coupling
                                  double &thermalSource,            // - Cp*dTdt
                                  double &dthermalSourcedT,         // thermal source
                                  STensor3 &dthermalSourcedF,
                                  double &mechanicalSource,         // mechanical source--> convert to heat
                                  double &dmechanicalSourcedT,
                                  STensor3 &dmechanicalSourceF) const;


        virtual void predictorCorrector_ThermoViscoPlastic(
                            const STensor3& F0,         // initial deformation gradient (input @ time n)
                            const STensor3& F1,         // updated deformation gradient (input @ time n+1)
                                  STensor3& P,                // updated 1st Piola-Kirchhoff stress tensor (output)
                            const IPNonLinearTVP *q0,       // array of initial internal variable
                                  IPNonLinearTVP *q1,             // updated array of internal variable (in ipvcur on output),
                            const double& T0, // previous temperature
                            const double& T, // temperature
                            const SVector3 &gradT0, // previous temeprature gradient
                            const SVector3 &gradT, // temeprature gradient
                                  SVector3 &fluxT, // temperature flux)
                                  double &thermalSource,
                                  double &mechanicalSource,
                                  STensor43* elasticTangent) const;

        virtual void predictorCorrector_ThermoViscoPlastic(
                            const STensor3& F0,         // initial deformation gradient (input @ time n)
                            const STensor3& F1,         // updated deformation gradient (input @ time n+1)
                                  STensor3& P,                // updated 1st Piola-Kirchhoff stress tensor (output)
                            const IPNonLinearTVP *q0,       // array of initial internal variable
                                  IPNonLinearTVP *q1,             // updated array of internal variable (in ipvcur on output),
                                  STensor43 &Tangent,         // mechanical tangents (output)
                                  STensor43 &dFpdF, // plastic tangent
                                  STensor3 &dFpdT, // plastic tangent
                                  STensor43 &dFedF, // elastic tangent
                                  STensor3 &dFedT, // elastic tangent
                            const double& T0, // previous temperature
                            const double& T, // temperature
                            const SVector3 &gradT0, // previoustemeprature gradient
                            const SVector3 &gradT, // temeprature gradient
                                  SVector3 &fluxT, // temperature flux
                                  STensor3 &dPdT, // mechanical-thermal coupling
                                  STensor3 &dfluxTdgradT, // thermal tengent
                                  SVector3 &dfluxTdT,
                                  STensor33 &dfluxTdF, // thermal-mechanical coupling
                                  double &thermalSource,   // - cp*dTdt
                                  double &dthermalSourcedT, // thermal source
                                  STensor3 &dthermalSourcedF,
                                  double &mechanicalSource, // mechanical source--> convert to heat
                                  double &dmechanicalSourcedT,
                                  STensor3 &dmechanicalSourceF,
                            const bool stiff,
                                  STensor43* elasticTangent) const; // tangent is used by pointer, if NULL->no compute

        virtual void tangent_full_perturbation( // compute tangent by perturbation
                            const STensor3& F0,         // initial deformation gradient (input @ time n)
                            const STensor3& F1,         // updated deformation gradient (input @ time n+1)
                                  STensor3& P,                // updated 1st Piola-Kirchhoff stress tensor (output)
                            const IPNonLinearTVP *q0,       // array of initial internal variable
                                  IPNonLinearTVP *q1,             // updated array of internal variable (in ipvcur on output),
                            const double& T0, // previous temperature
                            const double& T, // temperature
                            const SVector3 &gradT0, // previous temeprature gradient
                            const SVector3 &gradT, // temeprature gradient
                                  SVector3 &fluxT, // temperature flux)
                                  double &thermalSource,
                                  double &mechanicalSource,
                                  STensor43 &Tangent,         // mechanical tangents (output)
                                  STensor43 &dFpdF, // plastic tangent
                                  STensor3 &dFpdT, // plastic tangent
                                  STensor43 &dFedF, // elastic tangent
                                  STensor3 &dFedT, // elastic tangent
                                  STensor3 &dPdT, // mechanical-thermal coupling
                                  STensor3 &dfluxTdgradT, // thermal tengent
                                  SVector3 &dfluxTdT,
                                  STensor33 &dfluxTdF, // thermal-mechanical coupling
                                  double &dthermalSourcedT, // thermal source
                                  STensor3 &dthermalSourcedF,
                                  double &dmechanicalSourcedT,
                                  STensor3 &dmechanicalSourceF) const;


    public:
        mlawNonLinearTVENonLinearTVP(const int num, const double E, const double nu, const double rho, const double tol,
                         const double Tinitial, const double Alpha, const double KThCon, const double Cp,
                         const bool matrixbyPerturbation = false, const double pert = 1e-8, const double stressIteratorTol = 1.e-9, const bool thermalEstimationPreviousConfig = true);

        #ifndef SWIG
        mlawNonLinearTVENonLinearTVP(const mlawNonLinearTVENonLinearTVP& src);
        mlawNonLinearTVENonLinearTVP& operator=(const materialLaw& source);
        virtual ~mlawNonLinearTVENonLinearTVP();

        virtual bool withEnergyDissipation() const {return true;};

        virtual matname getType() const{return materialLaw::nonlinearTVEnonlinearTVP2;}
        virtual void createIPState(IPStateBase* &ips, bool hasBodyForce, const bool* state_=NULL,const MElement *ele=NULL,
            const int nbFF_=0, const IntPt *GP=NULL, const int gpt = 0) const{
            Msg::Error("mlawNonLinearTVENonLinearTVP::createIPState has not been defined");
        }
        virtual void initLaws(const std::map<int,materialLaw*> &maplaw){}; // this law is initialized so nothing to do

        virtual void constitutive(
                    const STensor3& F0,         // initial deformation gradient (input @ time n)
                    const STensor3& Fn,         // updated deformation gradient (input @ time n+1)
                          STensor3 &P,                // updated 1st Piola-Kirchhoff stress tensor (output)
                    const IPVariable *q0,       // array of initial internal variable
                          IPVariable *q1,             // updated array of internal variable (in ipvcur on output),
                          STensor43 &Tangent,         // constitutive tangents (output)
                    const bool stiff,           // if true compute the tangents
                          STensor43* elasticTangent = NULL,
                    const bool dTangent =false,
                          STensor63* dCalgdeps = NULL) const;

        virtual void constitutive(
                    const STensor3& F0,                     // initial deformation gradient (input @ time n)
                    const STensor3& F1,                     // updated deformation gradient (input @ time n+1)
                          STensor3& P,                      // updated 1st Piola-Kirchhoff stress tensor (output)
                    const IPVariable *q0,                   // array of initial internal variable
                          IPVariable *q1,                   // updated array of internal variable (in ipvcur on output),
                          STensor43 &Tangent,               // constitutive mechanical tangents (output)
                    const double& T0,                       // previous temperature
                    const double& T,                        // temperature
                    const SVector3 &gradT0,                 // previous temperature gradient
                    const SVector3 &gradT,                  // temperature gradient
                          SVector3 &fluxT,                  // temperature flux
                          STensor3 &dPdT,                   // mechanical-thermal coupling
                          STensor3 &dfluxTdgradT,           // thermal tengent
                          SVector3 &dfluxTdT,
                          STensor33 &dfluxTdF,              // thermal-mechanical coupling
                          double &thermalSource,            // - Cp*dTdt
                          double &dthermalSourcedT,         // thermal source
                          STensor3 &dthermalSourcedF,
                          double &mechanicalSource,         // mechanical source--> convert to heat
                          double &dmechanicalSourcedT,
                          STensor3 &dmechanicalSourceF,
                    const bool stiff,
                          STensor43* elasticTangent = NULL) const;

        virtual materialLaw* clone() const{return new mlawNonLinearTVENonLinearTVP(*this);};
        virtual void checkInternalState(IPVariable* ipv, const IPVariable* ipvprev) const{}; // do nothing
        #endif // SWIG
};

#endif //mlawNonLinearTVENonLinearTVP
