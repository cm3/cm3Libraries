//
// C++ Interface: material law
//
// Description: nonlinear viscoelasticity (Simo J.C., Computational Inelasticity, Springer)
//
//
// Author:  <Dongli Li, Antoine Jerusalem>, (C) 2014
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef MLAWVISCOELASTIC_H_
#define MLAWVISCOELASTIC_H_
#include "mlaw.h"
#include "STensor3.h"
#include "STensor43.h"
#include "ipViscoelastic.h"



class mlawViscoelastic : public materialLaw
{
 protected:
  double _rho;                 // density
  double _E;                   // Young's modulus
  double _nu;                  // Poisson's ratio
  double _K;                   // bulk modulus
  double _tol;                 // tolerance for iterative process
  double _perturbationfactor;  // perturbation factor
  bool _tangentByPerturbation; // flag for tangent by perturbation
  int _N;                      // number of internal variables
  std::vector<double> _eta;    // viscosity vector, it starts with eta1 for the first viscoelastic branch (spring+damper)
  std::vector<double> _mu;     // shear modulus vector, it starts with mu_inf at t = inf
  std::vector<double> _tau;    // relaxation time


#ifndef SWIG
#endif // SWIG

 public:

  mlawViscoelastic(const int num,const double K, const double rho, const std::string &array_eta, int array_size_eta, const std::string &array_mu,
                 const double tol=1.e-6, const bool pert=false,const double eps = 1.e-8);

#ifndef SWIG
  mlawViscoelastic(const mlawViscoelastic &source);
  mlawViscoelastic& operator=(const materialLaw &source);
  virtual ~mlawViscoelastic()
  {

  }

  // function of materialLaw
  virtual materialLaw * clone() const;
  virtual void checkInternalState(IPVariable* ipv, const IPVariable* ipvprev) const{}; // do nothing
  virtual bool withEnergyDissipation() const {return false;};
  virtual matname getType() const{return materialLaw::viscoelastic;}
  virtual void createIPState(IPStateBase* &ips, bool hasBodyForce, const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const IntPt *GP=NULL, const int gpt = 0) const;
  virtual void initLaws(const std::map<int,materialLaw*> &maplaw){};
  virtual double soundSpeed() const;
  virtual double density()const{return _rho;}
  virtual const double bulkModulus() const{return _K;}
  virtual const std::vector<double> shearModulus() const{return _mu;}
  virtual const std::vector<double> viscosity() const{return _eta;}
  virtual const double poissonRatio() const{return _nu;}
  virtual const int numInternalVariable() const{return _N;}
  virtual std::vector<double> read_file(const char* file_name, int file_size); //read from files

  // specific function
 public:
  virtual void constitutive(const STensor3& F0,         // initial deformation gradient (input @ time n)
          const STensor3& Fn,                           // updated deformation gradient (input @ time n+1)
          STensor3 &P,                                  // updated 1st Piola-Kirchhoff stress tensor (output)
          const IPVariable *IPvisprev,              // previous IP variable
          IPVariable *IPvis,                        // current IP variable
          STensor43 &Tangent,         // constitutive tangents (output)
          const bool stiff,                              // if true compute the tangents
          STensor43* elasticTangent=NULL, 
          const bool dTangent =false,
          STensor63* dCalgdeps = NULL)const;

 protected:

  virtual void update(
          const STensor3& F0,
          const STensor3& F,
          STensor3&P,
          IPViscoelastic *IPvis,
          const IPViscoelastic *IPvisprev
          )const;

  virtual void update_IP(
          const STensor3 &F1,
          STensor3 &kirchhoff1_,
          IPViscoelastic *IPvis,
          const IPViscoelastic *IPvisprev,
          double &g,
          STensor3 &hn,
          const std::vector<double> &r
          ) const;

  virtual void stress(
          STensor3 &Sig_,
          const STensor3 &Fn,
          IPViscoelastic *IPvis,
          const IPViscoelastic *IPvisprev
          )const;

  virtual void initialKirchhoffStressTensor(
          const STensor3 &Fn,
          STensor3 &Kirchhoff_
          ) const;

  protected:

  virtual double determinantSTensor3(const STensor3 &a) const;
  virtual void inverseSTensor3(const STensor3 &a, STensor3 &ainv) const;
  virtual void multSTensor3(const STensor3 &a, const STensor3 &b, STensor3 &c) const;
  virtual void multSTensor3FirstTranspose(const STensor3 &A, const STensor3 &B, STensor3 &C) const;
  virtual void multSTensor3SecondTranspose(const STensor3 &a, const STensor3 &b, STensor3 &c) const;
  virtual STensor3 devSTensor3(STensor3 &a) const;
  static void Write_Matrix_To_File(std::ofstream& my_file, const STensor3 &X, std::string matrix_name);
 #endif // SWIG
};

#endif // MLAWVISCOELASTIC_H_
