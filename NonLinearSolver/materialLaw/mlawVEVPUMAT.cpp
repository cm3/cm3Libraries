//
// C++ Interface: material law
//
// Description: UMAT interface for VEVP model
//
//
// Author:  <V.D. NGUYEN>, (C) 2023
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include <math.h>
#include "MInterfaceElement.h"
#include <fstream>
#include "mlawVEVPUMAT.h"
#include "STensorOperations.h"

#if !defined(F77NAME)
#define F77NAME(x) (x##_)
#endif

extern "C" {
  void F77NAME(umat_vevp)(double *STRESS, double *STATEV, double *DDSDDE, double *SSE, double *SPD, double *SCD,
              double *RPL, double *DDSDDT, double *DRPLDE, double *DRPLDT,
              double *STRAN, double *DSTRAN, double *TIM, double *DTIME, double *TEMP, double *DTEMP, double *PREDEF, double *DPRED, const char *CMNAME,
              int *NDI, int*NSHR, int* NTENS, int*NSTATV, double *PROPS, int *NPROPS, double *COORDS, double *DROT, double *PNEWDT,
              double *CELENT, double *DFGRD0, double *DFGRD1, int *NOEL, int *NPT, int *LAYER, int *KSPT, int *KSTEP, int *KINC);
}



mlawVEVPUMAT::mlawVEVPUMAT(const int num,  const char *propName) : mlawUMATInterface(num,0.,propName)
{
    //should depend on the umat: here we read the grains.inp file with 3 euler angle, 2 parameters equal to 1, and the number of internal variables (23)
    std::ifstream in(propName);
    if(!in) Msg::Error("Cannot open the file %s! Maybe is missing   ",propName);
    std::string line;
    std::getline(in, line,'\n');
    nsdv=atoi(line.c_str());
    std::getline(in, line,'\n');
    nprops1=atoi(line.c_str());
    std::cout << "number of internal variables: "<< nsdv << "; number of properties: "<< nprops1 << std::endl;
    props=new double[nprops1];
    int i=0;
    while (std::getline(in, line,'\n') and i<nprops1)
    {
      //std::cout << line << std::endl;
      props[i]=atof(line.c_str());
      std::cout << "property "<< i << ": "<< props[i] << std::endl;
      i++;
    }
    in.close();

    double Kinfty = props[1];
    double Ginfty = props[2];
    _rho = 0.;
    _nu0 = (3*Kinfty - 2*Ginfty)/2./(3*Kinfty+Ginfty);
    _mu0 = Ginfty;
}
mlawVEVPUMAT::mlawVEVPUMAT(const mlawVEVPUMAT &source) :
                                        mlawUMATInterface(source)
{

}

mlawVEVPUMAT& mlawVEVPUMAT::operator=(const materialLaw &source)
{
   mlawUMATInterface::operator=(source);
   const mlawVEVPUMAT* src =static_cast<const mlawVEVPUMAT*>(&source);
   return *this;
};

mlawVEVPUMAT::~mlawVEVPUMAT()
{

}
void mlawVEVPUMAT::createIPState(IPVEVPUMAT *ivi, IPVEVPUMAT *iv1, IPVEVPUMAT *iv2) const
{
  mlawUMATInterface::createIPState(ivi, iv1, iv2);
}

void mlawVEVPUMAT::createIPVariable(IPVEVPUMAT *ipv, bool hasBodyForce, const MElement *ele,const int nbFF,const IntPt *GP, const int gpt) const
{
  mlawUMATInterface::createIPVariable(ipv, hasBodyForce, ele, nbFF,GP, gpt);
}


void mlawVEVPUMAT::callUMAT(double *stress, double *statev, double **ddsdde, double &sse, double &spd, double &scd, double &rpl,
                                 double *ddsddt, double *drplde, double &drpldt, double *stran, double *dtsran,
                                 double *tim, double timestep, double temperature, double deltaTemperature, double *predef, double *dpred,
                                 const char *CMNAME, int &ndi, int &nshr, int tensorSize,
                                 int statevSize, double *prs, int matPropSize, double *coords, double **dRot,
                                 double &pnewdt, double &celent, double **F0, double **F1,
                                 int &noel, int &npt, int &layer, int &kspt, int &kstep, int &kinc) const
{


  double dRtmp[9];
  double F0tmp[9];
  double F1tmp[9];

  double ddsddetmp[36];

  convert3x3To9((const double**)dRot, dRtmp);
  convert3x3To9((const double**)F0, F0tmp);
  convert3x3To9((const double**)F1, F1tmp);

  convert6x6To36((const double**)ddsdde,ddsddetmp);

  F77NAME(umat_vevp)(stress, statev, ddsddetmp, &sse, &spd, &scd, &rpl,
                   ddsddt, drplde, &drpldt, stran, dtsran,
                   tim, &timestep, &temperature, &deltaTemperature, predef, dpred,
                   CMNAME, &ndi, &nshr, &tensorSize, &statevSize, prs, &matPropSize, coords, dRtmp,
                   &pnewdt, &celent, F0tmp, F1tmp,
                   &noel, &npt, &layer, &kspt, &kstep, &kinc);

  convert9To3x3((const double*)dRtmp,dRot);
  convert36To6x6((const double*)ddsddetmp,ddsdde);
}

