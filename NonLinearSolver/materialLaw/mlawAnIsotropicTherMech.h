//
// C++ Interface: material law
//              AnIsotropicTherMech law
// Author:  <L. Homsy>, (C) 2016
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef MLAWANISOTROPICTHERMECH_H_
#define MLAWANISOTROPICTHERMECH_H_
#include "STensor3.h"
#include "STensor43.h"
#include "mlaw.h"

#include "mlawTransverseIsotropic.h"
#include "ipAnIsotropicTherMech.h"

class mlawAnIsotropicTherMech : public mlawTransverseIsotropic //, public mlawLinearElecTherMech
{

 protected:
  
  double _kx;
  double _ky;
  double _kz;
  double _t0;
  double _cp;
  double _alphath;
  STensor3 _k0;

  // function of materialLaw
  STensor3 _rotationMatrix;
  STensor3 _Stiff_alphaDilatation;
  
 public:
 
   mlawAnIsotropicTherMech(const int num,const double E,const double nu, const double rho, const double EA, const double GA, const double _nu_minor,
			  const double Ax, const double Ay, const double Az,const double alpharot, const double betarot, const double gammarot,const double t0,
			   const double kx,const double ky, const double kz,const double cp,const double alphath);
   #ifndef SWIG
   mlawAnIsotropicTherMech(const mlawAnIsotropicTherMech &source);
   mlawAnIsotropicTherMech& operator=(const  materialLaw &source);
   virtual ~mlawAnIsotropicTherMech(){};
   virtual materialLaw* clone() const {return new mlawAnIsotropicTherMech(*this);};
   virtual bool withEnergyDissipation() const {return false;};
   // function of materialLaw
   virtual matname getType() const{return materialLaw::AnIsotropicTherMech;}  
   virtual void createIPState(IPStateBase* &ips, bool hasBodyForce, const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const IntPt *GP=NULL, const int gpt =0) const;

public:
    virtual void constitutive(
                            const STensor3& F0,         // initial deformation gradient (input @ time n)
                            const STensor3& Fn,         // updated deformation gradient (input @ time n+1)
                            STensor3 &P,                // updated 1st Piola-Kirchhoff stress tensor (output)
                            const IPAnIsotropicTherMech *q0,       // array of initial internal variable
                            IPAnIsotropicTherMech *q1,             // updated array of internal variable (in ipvcur on output),
                            STensor43 &Tangent,         // constitutive tangents (output)
                            double T0,			// initial Temperature
			    double T,			// updated Temperature			 
  			    const SVector3 &gradT,
                            SVector3 &fluxT,            // Thermal flux	// Electric current flux
                            STensor3 &dPdT,
                            STensor3 &dqdgradT,
                            SVector3 &dqdT,
                            STensor33 &dqdF,
			    double &w,			// Thermal source (ex. heat cpacity)
			    double &dwdt,
			    STensor3 &dwdf,
                            const bool stiff
                           )const;
			   
    double getInitialTemperature() const {return _t0;};
    virtual const STensor3&  getConductivityTensor() const { return _k0;};
    virtual const STensor3& getStiff_alphaDilatation()const { return _Stiff_alphaDilatation;};


 protected:
   virtual double deformationEnergy(const STensor3 &C, const SVector3 &A, double T) const ;


 #endif // SWIG
};

#endif // MLAWANISOTROPICELECTHERMECH_H_


