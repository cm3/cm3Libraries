#ifndef LAMHOMOGENIZATION_H
#define LAMHOMOGENIZATION_H 1

#ifdef NONLOCALGMSH
#include "material.h"
#include "elasticlcc.h"

int Laminate_2ply(double* DE, MFH::Material* A_mat, MFH::Material* B_mat, double VA, double* statev_n, double* statev, int nsdv, int idsdv_A, int idsdv_B, double* angles, double** C, double*** dC, double** Calgo, int kinc, int kstep, double dt);

int Lam2plyNR(double* DE, double* DeA, double* DeB, MFH::Material* A_mat, MFH::Material* B_mat, double VA, double* statev_n, double* statev, int idsdv_A, int idsdv_B, double** R66, int kinc, int kstep, double dt, int last, double* F, double** J, double** C, double*** dC, double** Calgo);

#else

int Laminate_2ply(double* DE, Material* A_mat, Material* B_mat, double VA, double* statev_n, double* statev, int nsdv, int idsdv_A, int idsdv_B, double* angles, double** C, double*** dC, double** Calgo, int kinc, int kstep, double dt);

int Lam2plyNR(double* DE, double* DeA, double* DeB, Material* A_mat, Material* B_mat, double VA, double* statev_n, double* statev, int idsdv_A, int idsdv_B, double** R66, int kinc, int kstep, double dt, int last, double* F, double** J, double** C, double*** dC, double** Calgo);

#endif
int TensPA(double VA, double** CA, double** CB, double** PA);

#endif
