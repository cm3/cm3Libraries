//
// Description: Define material law for cohesive layer
// the cohesive traction is computed form cohesive jump
// since the cohesive law is intitialized at insertion time,
// the parameters are locally given in IPCohesive
//
// Author:  <V.D. Nguyen>, (C) 2015
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef MLAWCOHESIVE_H_
#define MLAWCOHESIVE_H_

#include "ipCohesive.h"
#include "STensor3.h"
#include "STensor33.h"
#include "mlaw.h"
#include "cohesiveDamageLaw.h"
#include "cohesiveElasticPotential.h"

class CohesiveLawBase : public materialLaw{
  protected:

  public:
    CohesiveLawBase(const int num, const bool init = true): materialLaw(num,init){}
    CohesiveLawBase(const CohesiveLawBase& src): materialLaw(src){}
    virtual CohesiveLawBase& operator=(const materialLaw &source);
    virtual ~CohesiveLawBase(){}
    
    virtual void createIPVariable(IPCohesive* &ipv) const = 0;
    virtual matname getType() const {return materialLaw::cohesiveLaw;};
    virtual void createIPState(IPStateBase* &ips, bool hasBodyForce, const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const IntPt *GP=NULL, const int gpt = 0) const = 0;
    virtual void initLaws(const std::map<int,materialLaw*> &maplaw){};
    virtual double soundSpeed() const{
      Msg::Warning("Use a cohesive law to determine sound speed ?? value =1");
      return 1;
    };
    // general consitutive law for cohesive
    virtual void constitutive(const SVector3& jump1,  // current jump
                              const SVector3& normal1, // current normal
                              const IPCohesive* q0, // previous internal data
                              IPCohesive* q1, // current internal data
                              SVector3& Tcohesive, // interface traction from cohesive law
                              const bool stiff, // stiffness fracture
                              STensor3& DTcohesiveDjump // cohesive tangnet
                              ) const = 0;
    virtual void constitutive(const STensor3& F0,         // initial deformation gradient (input @ time n)
                            const STensor3& Fn,         // updated deformation gradient (input @ time n+1)
                            STensor3 &P,                // updated 1st Piola-Kirchhoff stress tensor (output)
                                                        // contains the initial values on input
                            const IPVariable *q0,       // array of initial internal variable
                            IPVariable *q1,              // updated array of internal variable (in ipvcur on output),
			    STensor43 &Tangent,         // constitutive tangents (output)
			    const bool stiff,
                            STensor43* elasticTangent = NULL, 
                            const bool dTangent =false,
                            STensor63* dCalgdeps = NULL
			    ) const
    {
      Msg::Error("CohesiveLawBase::constitutive does not exist");

    }
    virtual const cohesiveDamageLaw* getCohesiveDamageLaw() const = 0;
    virtual cohesiveDamageLaw* getCohesiveDamageLaw() = 0;
  
    virtual materialLaw* clone() const = 0;
};

class generalElasticDamageCohesiveLaw : public CohesiveLawBase{
  protected:
    cohesiveElasticPotential* _elasticPotential;
    cohesiveDamageLaw* _damageLaw;

  public:
    generalElasticDamageCohesiveLaw(const int num, const cohesiveElasticPotential& elP, const cohesiveDamageLaw& damageLaw);
    generalElasticDamageCohesiveLaw(const generalElasticDamageCohesiveLaw& src);
    virtual generalElasticDamageCohesiveLaw& operator=(const materialLaw &source);
    virtual ~generalElasticDamageCohesiveLaw();
    
    virtual void createIPVariable(IPCohesive* &ipv) const;
    
    virtual void createIPState(IPStateBase* &ips, bool hasBodyForce, const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const IntPt *GP=NULL, const int gpt = 0) const{
      Msg::Error("generalElasticDamageCohesiveLaw::createIPState has not been implemented");
    };

    virtual void constitutive(const SVector3& jump1, const SVector3& normal1,
                              const IPCohesive* q0, IPCohesive* q1, SVector3& T, const bool stiff, STensor3& DTDjump) const;

    virtual const cohesiveDamageLaw* getCohesiveDamageLaw() const {return _damageLaw;};
    virtual cohesiveDamageLaw* getCohesiveDamageLaw() {return _damageLaw;};
    virtual materialLaw* clone() const {return new generalElasticDamageCohesiveLaw(*this);};
    virtual void checkInternalState(IPVariable* ipv, const IPVariable* ipvprev) const{}; // do nothing
    virtual bool withEnergyDissipation() const {return true;};
};


// bilinear cohesive law
class BiLinearCohesiveLaw : public CohesiveLawBase{
  protected:
		cohesiveDamageLaw* _damageLaw;

  public:
    BiLinearCohesiveLaw(const int num, const bool init=true):CohesiveLawBase(num,init){
			_damageLaw = new BilinearCohesiveDamageLaw(num);
		}
		BiLinearCohesiveLaw(const int num, const cohesiveDamageLaw* damLaw, const bool init=true):CohesiveLawBase(num,init){
			_damageLaw = damLaw->clone();
		}

		BiLinearCohesiveLaw(const BiLinearCohesiveLaw& src): CohesiveLawBase(src){}
    virtual BiLinearCohesiveLaw& operator=(const materialLaw &source);
    virtual ~BiLinearCohesiveLaw(){
			if (_damageLaw != NULL){
				delete _damageLaw; _damageLaw = NULL;
			}
		}
    virtual void createIPVariable(IPCohesive* &ipv) const;
    virtual void createIPState(IPStateBase* &ips,  bool hasBodyForce, const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const IntPt *GP=NULL, const int gpt = 0) const;

    virtual void constitutive(const SVector3& jump1, const SVector3& normal1,
                              const IPCohesive* q0, IPCohesive* q1, SVector3& T, const bool stiff, STensor3& DTDjump) const;
    
    virtual const cohesiveDamageLaw* getCohesiveDamageLaw() const {return _damageLaw;};
    virtual cohesiveDamageLaw* getCohesiveDamageLaw() {return _damageLaw;};
    virtual materialLaw* clone() const {return new BiLinearCohesiveLaw(*this);};
    virtual void checkInternalState(IPVariable* ipv, const IPVariable* ipvprev) const{}; // do nothing
    virtual bool withEnergyDissipation() const {return true;};
};

class TransverseIsotropicCohesiveLaw : public BiLinearCohesiveLaw{
  protected:
    SVector3 _A; // direction of anisotropy
  public:
    TransverseIsotropicCohesiveLaw(const double Ax, const double Ay, const double Az, const int num, const bool init=true);
    TransverseIsotropicCohesiveLaw(const TransverseIsotropicCohesiveLaw& src);
    TransverseIsotropicCohesiveLaw& operator=(const materialLaw &source);
    virtual ~TransverseIsotropicCohesiveLaw(){}
    
    virtual void createIPVariable(IPCohesive* &ipv) const;
    virtual void createIPState(IPStateBase* &ips, bool hasBodyForce, const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const IntPt *GP=NULL, const int gpt = 0) const;

    virtual materialLaw* clone() const {return new TransverseIsotropicCohesiveLaw(*this);};
    virtual void getTransverseDirection(SVector3 &A, const IPCohesive *q1) const
    {
       A=_A;
    }
    virtual bool withEnergyDissipation() const {return true;};

  protected:
    TransverseIsotropicCohesiveLaw(const int num, const bool init=true):BiLinearCohesiveLaw(num,init)
    {
        Msg::Error("Cannot construct default TransverseIsotropicCohesiveLaw");
    }

};

class TransverseIsoCurvatureCohesiveLaw : public TransverseIsotropicCohesiveLaw{
  protected:
    SVector3 _Centroid;
    SVector3 _PointStart1;
    SVector3 _PointStart2;
    double _init_Angle;
  public:
    TransverseIsoCurvatureCohesiveLaw(const double Centroid_x, const double Centroid_y, const double Centroid_z, const double PointStart1_x, const double PointStart1_y, const double PointStart1_z,
                                      const double PointStart2_x, const double PointStart2_y, const double PointStart2_z,const double init_Angle, const int num, const bool init=true);
    TransverseIsoCurvatureCohesiveLaw(const TransverseIsoCurvatureCohesiveLaw& src);
    TransverseIsoCurvatureCohesiveLaw& operator=(const materialLaw &source);
    virtual ~TransverseIsoCurvatureCohesiveLaw(){}
    
    virtual void createIPVariable(IPCohesive* &ipv) const;
    virtual void createIPState(IPStateBase* &ips,  bool hasBodyForce,const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const IntPt *GP=NULL, const int gpt = 0) const;

    virtual materialLaw* clone() const {return new TransverseIsoCurvatureCohesiveLaw(*this);};
    virtual bool withEnergyDissipation() const {return true;};
    virtual void getTransverseDirection(SVector3 &A, const IPCohesive *q1) const
    {
      const IPTransverseIsoCurvatureCohesive *q = NULL;
      q=dynamic_cast<const IPTransverseIsoCurvatureCohesive*>(q1);
      if(q!=NULL)
      {
        q->getTransverseDirection(A);
      }
    }
    SVector3 getCentroid() const { return _Centroid;};
    SVector3 getPointStart1() const { return _PointStart1;};
    SVector3 getPointStart2() const { return _PointStart2;};
    double getInit_Angle() const { return _init_Angle;};
 protected:
    TransverseIsoCurvatureCohesiveLaw(const int num, const bool init=true):TransverseIsotropicCohesiveLaw(num,init)
    {
        Msg::Error("Cannot construct default TransverseIsotropicCohesiveLaw");
    }
};

#endif // MLAWCOHESIVE_H_
