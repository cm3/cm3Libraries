//
// C++ Interface: Material Law
//
// Description: Non-Linear Thermo-Visco-Mechanics (Thermo-ViscoElasto-ViscoPlasto Law) with Non-Local Damage Interface (soon.....)
//
// Author: <Ujwal Kishore J - FLE_Knight>, (C) 2023
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef MLAWNONLINEARTVM_H_
#define MLAWNONLINEARTVM_H_

#include "mlaw.h"
#include "STensor3.h"
#include "STensor43.h"
#include "STensor63.h"
#include "ipNonLinearTVM.h"
#include "viscosityLaw.h"
#include "mlawHyperelasticFailureCriterion.h"
#include "scalarFunction.h"
#include "mlawHyperelastic.h"
#include "elasticPotential.h"

class mlawNonLinearTVM : public mlawPowerYieldHyper{  
   protected:

  // Renaming _mu to _G
    double _G;          // 2nd Lame parameter (=mu) Shear Modulus

  // Material Properties at Room Temp. - Thermal
    double _scalarAlpha;    // (scalar) Thermal Dilatation Coefficient (=alpha)                                   - Tensor exists for Anisotropic Dilatation
    double _scalarK;        // (scalar) Thermal Conductivity (K)                                                  - Tensor exists for Anisotropic Conductivity - Generic Fourier's Law
    double _Cp;       // (scalar) Specific Heat at const pressure or volume or deformation????   - WHAT?

    STensor3 _tensorAlpha;  // (tensor3) Thermal Dilatation Coefficient (=alpha)
    STensor3 _tensorK;      // (tensor3) Thermal Conductivity (K)

  // Misc.
    bool _thermalEstimationPreviousConfig;  // flap for thermalEstimation using PreviousConfig

  // Testing
    int _testFlag;                          // for various solutions to convolution

  // Temperature Settings and Functions
    double _Tinitial;                               // Initial Temperature
    double _Tref;                                   // Reference Tempereture           - Set this regardless of thermo problem, can check the code at this constant temperature.
    scalarFunction* _temFunc_K;                     // bulk modulus temperature function
    scalarFunction* _temFunc_G;                     // shear modulus temperature function
    scalarFunction* _temFunc_Alpha;                 // alpha temperature function
    scalarFunction* _temFunc_KThCon;                // thermal conductivity temperature function
    scalarFunction* _temFunc_Cp;                    // specific heat temperature function

  // Shift factor - Everything related to it
    double _C1;                                     // WLF Universal Constant 1 - set in .py file
    double _C2;                                     // WLF Universal Constant 2 - set in .py file
    
  // Mullins Effect
    mullinsEffect* _mullinsEffect;
    
  // Hyperelastic Potential - Non-linear elasticity
    elasticPotential* _elasticPotential; 
    bool _useExtraBranch;
    bool _useExtraBranch_TVE;
    bool _useRotationCorrection;
    int _rotationCorrectionScheme;
    int _ExtraBranch_TVE_option;
    scalarFunction* _temFunc_elasticCorrection_Bulk;
    scalarFunction* _temFunc_elasticCorrection_Shear;
    
  // Extrabranch Corrector parameters
    std::vector<double> _V0;
    std::vector<double> _V1;
    std::vector<double> _V2;
    std::vector<double> _V3;
    std::vector<double> _V4;
    std::vector<double> _V5;
    std::vector<double> _D0;
    std::vector<double> _D1;
    std::vector<double> _D2;
    std::vector<double> _D3;
    std::vector<double> _D4;
    std::vector<double> _D5;
    std::vector<double> _Ci;


  #ifndef SWIG  
  protected:
  // Const Functions for constitutive function - To update any variable, pass it as an argument by reference.
    virtual double ShiftFactorWLF(const double T, double *DaDT = NULL, double *DDaDDT = NULL) const ;
    virtual void shiftedTime(const double t, const double T0, const double T1,
                             double *t_shift = NULL, double *Ddt_shiftDT = NULL, double *DDdt_shiftDT = NULL,
                             const int opt_time = 1, const int opt_order = 2) const;
    virtual double linearInterpTemp(const double t_interp, const double t0, const double t1, const double T0, const double T1) const;
    virtual double freeEnergyMechanical(const IPNonLinearTVM& q0, IPNonLinearTVM& q, const double T0, const double T, double* DpsiDT = NULL, STensor3* DpsiDE = NULL) const ;
    virtual double setTemp(const double T0, const double T1, const int para) const ;
    
    virtual void calculateMullinsEffectScaler(const IPNonLinearTVM* q0, IPNonLinearTVM *q1, const double T, const double* dPsiDT = NULL) const;

    virtual void evaluateElasticCorrection(const double trE, const STensor3 &devE, const double& T, 
                                                 double &A_v, double &dAdE, double &intA, double &dAdT,
                                                 double &B_d, STensor3 &dB_vddev, double &intB, double &dBdT,
                                                 double *ddAdTT = NULL, double *ddBdTT = NULL,
                                                 double *ddA_vdTdE = NULL, STensor3 *ddB_vdTdevE = NULL,
                                                 double *dB_dTrEe = NULL, double *dB_dTdTrEe = NULL, double *psiInfCorrector = NULL, double* DintA = NULL, double* DintB = NULL) const;
    
    virtual void extraBranchLaw(const STensor3& Ee,  const double& T, const IPNonLinearTVM *q0, IPNonLinearTVM *q1, 
                        STensor3& sig, const bool stiff = false, STensor43* DsigDEe = NULL, STensor3* DsigDT = NULL, 
                        double* DsigV_dTrEe = NULL, STensor43* DsigD_dDevEe = NULL,
                        double* DsigV_dT = NULL, STensor3* DsigD_dT = NULL,
                        double* DDsigV_dTT = NULL, STensor3* DDsigD_dTT = NULL, 
                        double* DDsigV_dTdTrEe = NULL, STensor43* DDsigD_dTdDevEe = NULL,
                        STensor3* DsigD_dTrEe = NULL, STensor3* DsigD_dTdTrEe = NULL) const;
    
    virtual void extraBranch_nonLinearTVE(const int i, const STensor3& Ee,  const double& T,
                        double& Av, double& dA_dTrEe, double& intA, double& intA1, double& intA2, double& dA_dT, double& ddA_dTT, double& ddA_dTdTrEe,
                        double& Bd, STensor3& dB_dDevEe, double &intB, STensor3 &intB1, STensor3 &intB2, double &dB_dT, double &ddB_dTT, STensor3& ddB_dTdDevEe,
						double& DintA, double& DintB, double* dB_dTrEe = NULL) const;

    void corKirInfinity(const IPNonLinearTVM *q1, const STensor3& devEe, const double& trEe, const double T, STensor3& CorKirDevInf, double& CorKirTrInf) const;
    
    void rotationTensor_N_to_Nplus1(const STensor3& Ee00, const STensor3& Fe0, const STensor3& Fe, const STensor3& Ce, const STensor3& Ee, const STensor3& Ee0,
    												STensor3& R, STensor43& dRdEe, STensor43& dRtdEe) const;

    void getdRedEe(const STensor3& Fe, const STensor3& Ce, const STensor43& dEedFe, const STensor43& dEedCe, STensor3& Ue, STensor3& Re, STensor43& dRedEe) const;
    void getdUedCe(const STensor3& Ce, const STensor3& Ue, STensor43& dUedCe) const;
    void getEe0s(const STensor3& Ce, const STensor3& Ee, const STensor3& Ee0, STensor3& Ee0s, STensor43& dEe0sdEe) const;
    void getAi0s(const STensor3& Ce, const STensor3& Ee, const std::vector<STensor3>& Ai0, std::vector<STensor3>& Ai0s, std::vector<STensor43>& dAi0sdEe) const;
    void getEe0s2(const STensor3& Ce, const STensor3& Ee, const STensor3& Ee0, STensor3& Ee0s, STensor43& dEe0sdEe) const;
    void getAi0s2(const STensor3& Ce, const STensor3& Ee, const std::vector<STensor3>& Ai0, std::vector<STensor3>& Ai0s, std::vector<STensor43>& dAi0sdEe) const;
    void getdEe0dEe(const STensor3& Ce, const STensor3& Ee, const STensor3& Ee0, STensor43& dEe0sdEe) const;

    void ThermoViscoElasticPredictor(const STensor3& Ee, const STensor3& Ee0,
          const IPNonLinearTVM *q0, IPNonLinearTVM *q1,
          double& Ke, double& Ge,
          const double T0, const double T1, const bool stiff, STensor43& Bd_stiffnessTerm, STensor43* Bd_stiffnessTerm2 = NULL) const;
          
    void ThermoViscoElasticPredictor_forTVP(const STensor3& Ce, const STensor3& Ee, const STensor3& Ee0,
          const IPNonLinearTVM *q0, IPNonLinearTVM *q1,
          double& Ke, double& Ge,
          const double T0, const double T1, const bool stiff, STensor43& Bd_stiffnessTerm, STensor43* Bd_stiffnessTerm2 = NULL, STensor43* rotationStiffness = NULL) const;
              
    void isotropicHookTensor(const double K, const double G, STensor43& L) const;
                                  
    void getMechSource_TVE_term(const IPNonLinearTVM *q0, IPNonLinearTVM *q1, const double& T0, const double& T, 
                                        double& Wm, const bool stiff, STensor3 &dWmdE, double& dWmdT) const;
                                        
    void getMechSource_nonLinearTVE_term(const IPNonLinearTVM *q0, IPNonLinearTVM *q1, const double& T0, const double& T, 
                                        double& Wm, const bool stiff, STensor3 &dWmdE, double& dWmdT) const;
                                  
    void getTVEdCorKirDT(const IPNonLinearTVM *q0, IPNonLinearTVM *q1, const double& T0, const double& T) const;

    void predictorCorrector_ThermoViscoElastic( 
                                        const STensor3& F0,                             // initial deformation gradient (input @ time n)
                                        const STensor3& F,                              // updated deformation gradient (input @ time n+1)
                                              STensor3 &P,                              // updated 1st Piola-Kirchhoff stress tensor (output)
                                        const IPNonLinearTVM *q0,                       // array of initial internal variables
                                              IPNonLinearTVM *q1,                       // updated array of internal variables (in ipvcur on output),
                                        const double& T0,                               // previous temperature
                                        const double& T,                                // temperature
                                        const SVector3& gradT0,                         // previous temperature gradient
                                        const SVector3& gradT,                          // temperature gradient
                                              SVector3& fluxT,                          // temperature flux
                                              double& thermalSource,
                                              double& mechanicalSource) const;
                            
    void predictorCorrector_ThermoViscoElastic(    
                                        const STensor3& F0,                             // initial deformation gradient (input @ time n)
                                        const STensor3& F,                              // updated deformation gradient (input @ time n+1)
                                              STensor3& P,                              // updated 1st Piola-Kirchhoff stress tensor (output)
                                        const IPNonLinearTVM *q0,                       // array of initial internal variables
                                              IPNonLinearTVM *q1,                       // updated array of internal variableS (in ipvcur on output),
                                              STensor43& Tangent,                       // mechanical tangents (output)
                                              STensor43& dFedF,                         // elastic tangent
                                              STensor3& dFedT,                          // elastic tangent
                                        const double& T0,                               // previous temperature
                                        const double& T,                                // temperature
                                        const SVector3 &gradT0,                         // previous temperature gradient
                                        const SVector3 &gradT,                          // temperature gradient
                                              SVector3 &fluxT,                          // temperature flux
                                              STensor3 &dPdT,                           // mechanical-thermal coupling
                                              STensor3 &dfluxTdgradT,                   // thermal tengent
                                              SVector3 &dfluxTdT,
                                              STensor33 &dfluxTdF,                      // thermal-mechanical coupling
                                              double& thermalSource,                    // - Cp*dTdt
                                              double& dthermalSourcedT,                 // thermal source
                                              STensor3 &dthermalSourcedF,
                                              double& mechanicalSource,                 // mechanical source --> convert to heat
                                              double& dmechanicalSourcedT,
                                              STensor3& dmechanicalSourceF,
                                        const bool stiff) const;
  #endif // SWIG

  public:
    // Constructor Declaration
    mlawNonLinearTVM(const int num,const double E,const double nu, const double rho, const double tol, const double Tinitial, const double Alpha, const double KThCon, const double Cp,
                const bool matrixbyPerturbation = false, const double pert = 1e-8, const bool thermalEstimationPreviousConfig = true);

    // Copy Contructor
    mlawNonLinearTVM(const mlawNonLinearTVM& src);

    // Operator Assignment
    mlawNonLinearTVM& operator=(const materialLaw& source);

    // Virtual Destructor
    virtual ~mlawNonLinearTVM();
    
    void setMullinsEffect(const mullinsEffect& mullins){
        if (_mullinsEffect) delete _mullinsEffect;
            _mullinsEffect = mullins.clone();
    };
  
  // Non-Const Functions for strain_order and VE data that are called from .py file.  ---- These WILL change the values of the variables defined in the class object.
    virtual void setViscoElasticNumberOfElement(const int N);
    virtual void setViscoElasticData(const int i, const double Ei, const double taui);
    virtual void setViscoElasticData_Bulk(const int i, const double Ki, const double ki);
    virtual void setViscoElasticData_Shear(const int i, const double Gi, const double gi);
    virtual void setCorrectionsAllBranchesTVE(const int i, const double V0, const double V1, const double V2, const double D0, const double D1, const double D2);
    virtual void setAdditionalCorrectionsAllBranchesTVE(const int i, const double V3, const double D3);
    virtual void setAdditionalCorrectionsAllBranchesTVE(const int i, const double V3, const double V4, const double V5, const double D3, const double D4, const double D5);
    virtual void setCompressionCorrectionsAllBranchesTVE(const int i, const double Ci);
    virtual void setViscoElasticData(const std::string filename);
    
  // Non-Const Functions for temp and temp_functions that are called from .py file.  ---- These WILL change the values of the variables defined in the class object.
    virtual void setReferenceTemperature(const double Tr){_Tref = Tr;};
    void setShiftFactorConstantsWLF(const double C1, const double C2){_C1 = C1; _C2 = C2;};
    virtual void useExtraBranchBool(const bool useExtraBranch){_useExtraBranch = useExtraBranch;};
    virtual void useExtraBranchBool_TVE(const bool useExtraBranch_TVE, const int ExtraBranch_TVE_option){
                                                _useExtraBranch_TVE = useExtraBranch_TVE; _ExtraBranch_TVE_option = ExtraBranch_TVE_option;};
    virtual void useRotationCorrectionBool(const bool useRotationCorrection, const int rotationCorrectionScheme){_useRotationCorrection = useRotationCorrection;
    																						_rotationCorrectionScheme = rotationCorrectionScheme;};
    void setTemperatureFunction_BulkModulus(const scalarFunction& Tfunc){
     if (_temFunc_K != NULL) delete _temFunc_K;
      _temFunc_K = Tfunc.clone();
    }
    void setTemperatureFunction_ShearModulus(const scalarFunction& Tfunc){
      if (_temFunc_G != NULL) delete _temFunc_G;
      _temFunc_G = Tfunc.clone();
    }
    void setTemperatureFunction_ThermalDilationCoefficient(const scalarFunction& Tfunc){
      if (_temFunc_Alpha != NULL) delete _temFunc_Alpha;
      _temFunc_Alpha = Tfunc.clone();
    }
    void setTemperatureFunction_ThermalConductivity(const scalarFunction& Tfunc){
      if (_temFunc_KThCon != NULL) delete _temFunc_KThCon;
      _temFunc_KThCon = Tfunc.clone();
    }
    void setTemperatureFunction_SpecificHeat(const scalarFunction& Tfunc){
      if (_temFunc_Cp != NULL) delete _temFunc_Cp;
      _temFunc_Cp = Tfunc.clone();
    }
    void setTemperatureFunction_ElasticCorrection_Bulk(const scalarFunction& Tfunc){
      if (_temFunc_elasticCorrection_Bulk != NULL) delete _temFunc_elasticCorrection_Bulk;
      _temFunc_elasticCorrection_Bulk = Tfunc.clone();
    }
    void setTemperatureFunction_ElasticCorrection_Shear(const scalarFunction& Tfunc){
      if (_temFunc_elasticCorrection_Shear != NULL) delete _temFunc_elasticCorrection_Shear;
      _temFunc_elasticCorrection_Shear = Tfunc.clone();
    }
    
#ifndef SWIG

// Const Functions to get temp-updated values of material properties - Args by reference, or return the variable.
    virtual const mullinsEffect* getConstRefToMullinsEffect() const {return _mullinsEffect;};
        
    virtual void getK(const IPNonLinearTVM *q1, double& KT, const double Tc, double *dK = NULL, double *ddK = NULL) const{
        double K = _K; // *q1->getConstRefToElasticBulkPropertyScaleFactor();
        KT = K*_temFunc_K->getVal(Tc);
        if(dK!=NULL){
            *dK = K*_temFunc_K->getDiff(Tc);  // Here, *dK can be imagined as *(dK+0) or dK[0] if dK were a vector. See dKi below.
        }
        if(ddK!=NULL){
            *ddK = K*_temFunc_K->getDoubleDiff(Tc);
        }
    }
    virtual void getG(const IPNonLinearTVM *q1, double& GT, const double Tc, double *dG = NULL, double *ddG = NULL) const{
        double G = _G; // *q1->getConstRefToElasticShearPropertyScaleFactor();
        GT = G*_temFunc_G->getVal(Tc);
        if(dG!=NULL){
            *dG = G*_temFunc_G->getDiff(Tc);
        }
        if(ddG!=NULL){
            *ddG = G*_temFunc_G->getDoubleDiff(Tc);
        }
    }
     virtual void getAlpha(double& alphaT, const double Tc, double *dAlpha = NULL, double *ddAlpha = NULL) const{
        alphaT = _scalarAlpha*_temFunc_Alpha->getVal(Tc);
        if(dAlpha!=NULL){
            *dAlpha = _scalarAlpha*_temFunc_Alpha->getDiff(Tc);
        }
        if(ddAlpha!=NULL){
            *ddAlpha = _scalarAlpha*_temFunc_Alpha->getDoubleDiff(Tc);
        }
    }
    virtual void getKTHCon(double& KThConT,const double Tc, double *dKThCon = NULL) const{
        KThConT = _scalarK*_temFunc_KThCon->getVal(Tc);
        if(dKThCon!=NULL){
            *dKThCon = _scalarK*_temFunc_KThCon->getDiff(Tc);
        }
    }
    virtual void getCp(double& CpT, const double Tc, double *dCp = NULL, double *ddCp = NULL) const{
        CpT = _Cp*_temFunc_Cp->getVal(Tc);
        if(dCp!=NULL){
            *dCp = _Cp*_temFunc_Cp->getDiff(Tc);
        }
        if(ddCp!=NULL){
            *ddCp = _Cp*_temFunc_Cp->getDoubleDiff(Tc);
        }
    }

//    #ifndef SWIG

// Some more Member Functions
    virtual matname getType() const{return materialLaw::nonlineartvm;}    // function of materialLaw
    virtual void createIPState(IPStateBase* &ips,const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const IntPt *GP=NULL, const int gpt = 0) const{
        Msg::Error("mlawNonLinearTVM::createIPState has not been defined");
    };
    virtual void initLaws(const std::map<int,materialLaw*> &maplaw){}; // this law is initialized so nothing to do
    virtual double soundSpeed() const; // default but you can redefine it for your case
  
// Added from LinearThermomech
    virtual double getInitialTemperature() const {return _Tinitial;};
    virtual double getInitialExtraDofStoredEnergyPerUnitField() const {return getExtraDofStoredEnergyPerUnitField(_Tinitial);}
    virtual double getExtraDofStoredEnergyPerUnitField(double T) const {
        double Cp; getCp(Cp,T);
        return Cp;
    }; 
    
// Const functions to directly export the thermal_cond and alpha when called for ipvcur in dG3DMaterialLaw.cpp
    virtual const STensor3& getConductivityTensor() const {return _tensorK;}; // isotropic only for now
    virtual const STensor3& getStiff_alphaDilatation() const {return _tensorAlpha;}; // isotropic only for now

    // Default Constitutive function - placeholder
    virtual void constitutive(
            const STensor3& F0,         // initial deformation gradient (input @ time n)
            const STensor3& Fn,         // updated deformation gradient (input @ time n+1)
            STensor3& P,                // updated 1st Piola-Kirchhoff stress tensor (output)
            const IPVariable *q0,       // array of initial internal variables
            IPVariable *q1,             // updated array of internal variables (in ipvcur on output),
            STensor43& Tangent,         // constitutive tangents (output)
            const bool stiff,           // if true compute the tangents
            STensor43* elasticTangent = NULL,
            const bool dTangent =false,
            STensor63* dCalgdeps = NULL) const;

    // ThermoMech Constitutive function - J2Full
    virtual void constitutive(
            const STensor3& F0,                     // initial deformation gradient (input @ time n)
            const STensor3& F1,                     // updated deformation gradient (input @ time n+1)
                  STensor3& P,                      // updated 1st Piola-Kirchhoff stress tensor (output)
            const IPVariable *q0,                   // array of initial internal variable
                  IPVariable *q1,                   // updated array of internal variable (in ipvcur on output),
                  STensor43 &Tangent,               // constitutive mechanical tangents (output)
            const double& T0,                       // previous temperature
            const double& T,                        // temperature
            const SVector3 &gradT0,                 // previous temperature gradient
            const SVector3 &gradT,                  // temperature gradient
                  SVector3 &fluxT,                  // temperature flux
                  STensor3 &dPdT,                   // mechanical-thermal coupling
                  STensor3 &dfluxTdgradT,           // thermal tengent
                  SVector3 &dfluxTdT,
                  STensor33 &dfluxTdF,              // thermal-mechanical coupling
                  double &thermalSource,            // - Cp*dTdt
                  double &dthermalSourcedT,         // thermal source
                  STensor3 &dthermalSourcedF,
                  double &mechanicalSource,         // mechanical source--> convert to heat
                  double &dmechanicalSourcedT,
                  STensor3 & dmechanicalSourceF,
            const bool stiff,
                  STensor43* elasticTangent = NULL) const;
 
                  
    virtual materialLaw* clone() const {return new mlawNonLinearTVM(*this);};
    virtual void checkInternalState(IPVariable* ipv, const IPVariable* ipvprev) const{}; // do nothing
    #endif // SWIG
};
#endif //mlawNonLinearTVM
