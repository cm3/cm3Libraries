//
// C++ Interface: material law
//
// Description: TransverseIsotropic law
//
//
// Author:  <L. Noels>, (C) 2011
// Modified by <B. Yang > 2016
// Copyright: See COPYING file that comes with this distribution
//
//
#include "mlawTransverseIsoYarnB.h"
#include <math.h>
#include "MInterfaceElement.h"

mlawTransverseIsoYarnB::mlawTransverseIsoYarnB(const mlawTransverseIsoYarnB &source) : materialLaw(source)
{
  // FILL  HERE
}


mlawTransverseIsoYarnB& mlawTransverseIsoYarnB::operator=(const materialLaw &source)
{
  materialLaw::operator=(source);
  const mlawTransverseIsoYarnB* src =dynamic_cast<const mlawTransverseIsoYarnB*>(&source);
  if(src != NULL)
  {
    // FILL  HERE
    _physicnum = src->_physicnum;
  }
  return *this;
}
 


mlawTransverseIsoYarnB::mlawTransverseIsoYarnB(int lawnum,          //tag of the material law
                                               double rho,          //material density 
                                               int physicnum,       //physic number of 
                                               string yarnInfo,     //vector container of YarnDirections
                                               int isunidir,        // 0:Woven yarn. 1:Straight yarn. 2:Homogenuous material
                                               double rDSvDT,       //To initialize '_CriticalrDSvDT'
                                               double MaxStrainRate, //The maximal value of the strain rate
			                       const double Cxx11,                 
		                               const double Cxx21,                  
			                       const double Cxx22,                 
			                       const double Cxx31,                 
			                       const double Cxx32,                   
			                       const double Cyy11,                 
			                       const double Cyy21,                   
			                       const double Cyy22,                  
			                       const double Cyy31,                 
			                       const double Cyy32,                   
			                       const double Cxy11,                 
			                       const double Cxy21,                   
			                       const double Cxy22,                  
			                       const double Cxy31,                 
			                       const double Cxy32,          
			                       const double Cyz11,                 
			                       const double Cyz21,                   
			                       const double Cyz22,                  
			                       const double Cyz31,                 
			                       const double Cyz32,                  
			                       const double nuxy, 
                                               const double nuxz,
                                               const double nuyz): materialLaw(lawnum,true)
{
 _physicnum = physicnum;
 _rho = rho;
 _CriticalrDSvDT = rDSvDT;
 _MaxStrainRate = MaxStrainRate;
 //****Initialize member variables********
 //Fitted data
 _Cxx11=Cxx11; _Cxx21=Cxx21; _Cxx22=Cxx22; _Cxx31=Cxx31; _Cxx32=Cxx32;            
 _Cyy11=Cyy11; _Cyy21=Cyy21; _Cyy22=Cyy22; _Cyy31=Cyy31; _Cyy32=Cyy32;                   
 _Cxy11=Cxy11; _Cxy21=Cxy21; _Cxy22=Cxy22; _Cxy31=Cxy31; _Cxy32=Cxy32;                  
 _Cyz11=Cyz11; _Cyz21=Cyz21; _Cyz22=Cyz22; _Cyz31=Cyz31; _Cyz32=Cyz32;                 
	                         
 //Poisson's ratio
 _nuxy = nuxy;
 _nuxz = nuxz;
 _nuyz = nuyz;
  	
 _CArray[0][0]=Cxx11; _CArray[0][1]=Cxx21; _CArray[0][2]=Cxx22; //xx
 _CArray[0][3]=Cxx31; _CArray[0][4]=Cxx32;

 _CArray[1][0]=Cxy11; _CArray[1][1]=Cxy21; _CArray[1][2]=Cxy22; //xy
 _CArray[1][3]=Cxy31; _CArray[1][4]=Cxy32;
	
 _CArray[2][0]=Cxy11; _CArray[2][1]=Cxy21; _CArray[2][2]=Cxy22; //xz
 _CArray[2][3]=Cxy31; _CArray[2][4]=Cxy32;
	
 _CArray[3][0]=Cxy11; _CArray[3][1]=Cxy21; _CArray[3][2]=Cxy22; //yx
 _CArray[3][3]=Cxy31; _CArray[3][4]=Cxy32;
 //Remark*: Here we used the relation Cxy=Cyx=Cxz=Czx
 
 _CArray[4][0]=Cyy11; _CArray[4][1]=Cyy21; _CArray[4][2]=Cyy22; //yy
 _CArray[4][3]=Cyy31; _CArray[4][4]=Cyy32;
	
 _CArray[5][0]=Cyz11; _CArray[5][1]=Cyz21; _CArray[5][2]=Cyz22; //yz
 _CArray[5][3]=Cyz31; _CArray[5][4]=Cyz32;
	
 _CArray[6][0]=Cxy11; _CArray[6][1]=Cxy21; _CArray[6][2]=Cxy22; //zx
 _CArray[6][3]=Cxy31; _CArray[6][4]=Cxy32;
	
 _CArray[7][0]=Cyz11; _CArray[7][1]=Cyz21; _CArray[7][2]=Cyz22; //zy
 _CArray[7][3]=Cyz31; _CArray[7][4]=Cyz32;
	
 _CArray[8][0]=Cyy11; _CArray[8][1]=Cyy21; _CArray[8][2]=Cyy22; //zz
 _CArray[8][3]=Cyy31; _CArray[8][4]=Cyy32;
 //Remark*: Here we used the relation Cyy=Czz

 _IsUniDir = isunidir;
 int stretchDirect;
 switch(isunidir)
 {
  case 0://Woven yarn
         _YD  = Yarn(yarnInfo, //Physic num
                     physicnum //Segments of the yarn
                     );
         break;
  case 1://Straight yarn
         _YD = Yarn(yarnInfo, //Physic num
                    physicnum //Segments of the yarn
                    );
         _UniDir = vector<double>(3); 
         stretchDirect = _YD.get_StretchAxis();
         _UniDir[stretchDirect] = 1;
         break;
 }
}

mlawTransverseIsoYarnB::mlawTransverseIsoYarnB(int lawnum,          //tag of the material law
                                               double rho,          //material density 
                                               int physicnum,       //physic number of 
                                               double rDSvDT,       //To initialize '_CriticalrDSvDT'
                                               double MaxStrainRate, //The maximal value of the strain rate
			                       const double Cxx11,                 
		                               const double Cxx21,                  
			                       const double Cxx22,                 
			                       const double Cxx31,                 
			                       const double Cxx32,                   
			                       const double Cyy11,                 
			                       const double Cyy21,                   
			                       const double Cyy22,                  
			                       const double Cyy31,                 
			                       const double Cyy32,                   
			                       const double Cxy11,                 
			                       const double Cxy21,                   
			                       const double Cxy22,                  
			                       const double Cxy31,                 
			                       const double Cxy32,          
			                       const double Cyz11,                 
			                       const double Cyz21,                   
			                       const double Cyz22,                  
			                       const double Cyz31,                 
			                       const double Cyz32,                  
			                       const double nuxy, 
                                               const double nuxz,
                                               const double nuyz): materialLaw(lawnum,true)
{
 _physicnum = physicnum;
 _rho = rho;
 _CriticalrDSvDT = rDSvDT;
 _MaxStrainRate = MaxStrainRate;
 //****Initialize member variables********
 //Fitted data
 _Cxx11=Cxx11; _Cxx21=Cxx21; _Cxx22=Cxx22; _Cxx31=Cxx31; _Cxx32=Cxx32;            
 _Cyy11=Cyy11; _Cyy21=Cyy21; _Cyy22=Cyy22; _Cyy31=Cyy31; _Cyy32=Cyy32;                   
 _Cxy11=Cxy11; _Cxy21=Cxy21; _Cxy22=Cxy22; _Cxy31=Cxy31; _Cxy32=Cxy32;                  
 _Cyz11=Cyz11; _Cyz21=Cyz21; _Cyz22=Cyz22; _Cyz31=Cyz31; _Cyz32=Cyz32;                 
	                         
 //Poisson's ratio
 _nuxy = nuxy;
 _nuxz = nuxz;
 _nuyz = nuyz;
  	
 _CArray[0][0]=Cxx11; _CArray[0][1]=Cxx21; _CArray[0][2]=Cxx22; //xx
 _CArray[0][3]=Cxx31; _CArray[0][4]=Cxx32;

 _CArray[1][0]=Cxy11; _CArray[1][1]=Cxy21; _CArray[1][2]=Cxy22; //xy
 _CArray[1][3]=Cxy31; _CArray[1][4]=Cxy32;
	
 _CArray[2][0]=Cxy11; _CArray[2][1]=Cxy21; _CArray[2][2]=Cxy22; //xz
 _CArray[2][3]=Cxy31; _CArray[2][4]=Cxy32;
	
 _CArray[3][0]=Cxy11; _CArray[3][1]=Cxy21; _CArray[3][2]=Cxy22; //yx
 _CArray[3][3]=Cxy31; _CArray[3][4]=Cxy32;
 //Remark*: Here we used the relation Cxy=Cyx=Cxz=Czx
 
 _CArray[4][0]=Cyy11; _CArray[4][1]=Cyy21; _CArray[4][2]=Cyy22; //yy
 _CArray[4][3]=Cyy31; _CArray[4][4]=Cyy32;
	
 _CArray[5][0]=Cyz11; _CArray[5][1]=Cyz21; _CArray[5][2]=Cyz22; //yz
 _CArray[5][3]=Cyz31; _CArray[5][4]=Cyz32;
	
 _CArray[6][0]=Cxy11; _CArray[6][1]=Cxy21; _CArray[6][2]=Cxy22; //zx
 _CArray[6][3]=Cxy31; _CArray[6][4]=Cxy32;
	
 _CArray[7][0]=Cyz11; _CArray[7][1]=Cyz21; _CArray[7][2]=Cyz22; //zy
 _CArray[7][3]=Cyz31; _CArray[7][4]=Cyz32;
	
 _CArray[8][0]=Cyy11; _CArray[8][1]=Cyy21; _CArray[8][2]=Cyy22; //zz
 _CArray[8][3]=Cyy31; _CArray[8][4]=Cyy32;
 //Remark*: Here we used the relation Cyy=Czz

 _IsUniDir = 2;
}

/*
mlawTransverseIsoYarnB& mlawTransverseIsoYarnB::operator=(const materialLaw &source)
{
  mlawTransverseIsotropic::operator=(source);
  const mlawTransverseIsoYarnB* src =static_cast<const mlawTransverseIsoYarnB*>(&source);
  _Centroid = src->_Centroid;
  _PointStart1 = src->_PointStart1;
  _PointStart2 = src->_PointStart2;
  _init_Angle = src->_init_Angle;
  return *this;
}
*/

//=========================Constitutive Law=========================
//+++++++++++++ Entrance of the constitutive law +++++++++++++++++++
//TODO: Check input data and call 'update' function to update the
//      stress tensor
//Input:
//      STensor3& Fn: An 3*3 tensor object at time t(n)
//      STensor3& Fn1: An 3*3 tensor object at time t(n+1)
//      STensor3 &P: The pointer of the updated tensor
//Output:
//      STensor3 &P: The updated stress tensor

void mlawTransverseIsoYarnB::constitutive(const STensor3 &F0,
                                          const STensor3 &Fn1,            // updated deformation gradient (input @ time n+1)
                                          STensor3 &P,                    // 1st Piola-Kirchoff stress
					  const IPVariable *q0i, // internal point variable @ time n 
					  IPVariable *q1i,       // internal point will be updated @ time n+1
                                          STensor43 &Tangent,
                                          const bool stiff,                // if true compute the tangents
                                          STensor43* elasticTangent, 
                                          const bool dTangent,
                                          STensor63* dCalgdeps) const
{     
        const IPTransverseIsoYarnB *q0 = dynamic_cast<const IPTransverseIsoYarnB *> (q0i);
        IPTransverseIsoYarnB *q1 = dynamic_cast<IPTransverseIsoYarnB *> (q1i);
        double time_c = getTime();  
        double time_step = getTimeStep();
	//Declare fullMatrix and fullVector objects, see fullMatrix.h
	
	//****************Initialize Deformation gradient************************     
	STensor3 F1square(0.0);      	
	F1square = Fn1;   //Initialize F1square
	//***********************************************************************
	
	//****************Computer Linear strain in global coor sys**************
	//Remark: Formula used in computation
	//        e = 0.5*((V-I)+(V-I)^T), V = (F*F^T)^(1/2) = AHA^T,
	//        where H is a diagnose matrix containing the square root of 
	//        eigenvalues of matrix F*F^T, and each column of matrix A contains
	//        the corresponding eigenvector.
	
	//========Step 1: Compute V = (F*F^T)^(1/2)= AHA^T=========
	//--------1.1 Allocate sapce for A and H-----------------------
	fullMatrix<double> An1_full(3,3);       //Eigenvector of F*F^T at time tn+1
	fullVector<double> Hn1_full_v(3);       //Eigenvalue of F*F^T at time tn+1
	fullMatrix<double> Hn1_full_m(3,3);     //The matrix form of Hn1_full_v	
	//--------1.2 Compute F*F^T---------------------------------
	F1square *= Fn1.transpose();	
	//--------1.3 Compute A and H-------------------------------
	F1square.eig(An1_full,Hn1_full_v);

	//--------Transform A and H from fullMatrix 
	//        type to STensor3 type-----------------------------
	STensor3 An1(0.0);
	An1.setMat(An1_full);
	
	for(int i=0; i<Hn1_full_v.size();i++)
	{
         Hn1_full_m.set(i, i, sqrt(Hn1_full_v(i)));
	} //Compute sqrt(H)	
 
	//See definition of 'inline scalar operator () (int i)  const { return _data[i]; }'
        //  in fullMatrix.h	
	STensor3 Hn1(0.0);
	Hn1.setMat(Hn1_full_m);
	
	//----------1.4 Compute V=AHA^(-1)----------------------------------
	STensor3 Vn1;
        Vn1 =  Hn1;
        Vn1 *= An1.invert();
        An1 *= Vn1;
        Vn1 = An1; //Vn1 = An1*Hn1*An1^(-1)
   
        
	
	//========Step 2: Compute e = 0.5*((V-I)+(V-I)^T)================
	//Remark: The computation here is under global coordinate system
	//--------2.1 Allocate sapce for identify matrix and strain tensors-
	STensor3 I(1.0);//Declare Identity matrix
	STensor3 Epsn1(Vn1); //Declare Strain matrix at tn1
        Epsn1-= I;
	STensor3 Epsn1_Global(0.0);
	//--------2.2 Compute e = 0.5*((V-I)+(V-I)^T)--------------------
        Epsn1 += Epsn1.transpose();
        Epsn1 = 0.5*Epsn1;
        Epsn1_Global = Epsn1;


        //****************Computer Linear strain in local coor sys********
        //========Step 1: read rotation matrix and fiber direction========
        STensor3 M = q0->getRotMatrix();          
        SVector3 A = q0->getDirection();
        SVector3 Coor = q0 ->getCoor();

        /*
        STensor3 Vn1Square;
        Vn1Square = Vn1;
        Vn1Square *= Vn1.transpose();	
  
        STensor3 R;
        R = Vn1.inverse();
        R *= Fn1;
        */
 
        q1->setDirection(q0->getDirection());
        q1->setRotMatrix(q0->getRotMatrix());
        /*Remark: Let [e1,e2,e3] be the basis of the local coor sys 
          corresponding to [l1,l2,l3] in global coor sys with basis [E1,E2,E3],
          then M*[l1,l2,l3] = [e1,e2,e3].
        */
        //=======Step 2: transform global strain into local coor sys======
        STensor3 invM;
	invM = M.invert(); //Inverse matrix of M

        Epsn1 *= M.transpose(); //Eps = Eps*M'
        M *= Epsn1;            //M = M*Eps*M'
        Epsn1 = M; //Strain in local sys. Eps = M*Eps*M'

	//=========Step 3: Compute stress ================================	
	//----------3.1 Computer Linear stress in local coor system-------
        STensor3 LStressTemp(0.0);//Uncoupled components
                                  //LStressTemp_ij = Int([0,t],E_ij(t-tau)*(deps_dtau), dtau)
	double dstress = 0;
        int id = 0;
        double Epsnij = 0;

	//Remark: getTime() is defined in mlaw.h. It is used to return the
        //        current time.	
        double eps_t;
	for(int i = 0; i < 3; i++)
        {
	    for(int j = 0; j < 3; j++)
	    { 
              id = LStressTemp.getIndex(i,j);
	      dstress = 0;   
              Epsnij = Epsn1(i,j);

              if(time_c==0)
              {eps_t = 0;}
              else
              {eps_t = Epsnij/time_c;}    

	      dstress += _CArray[id][0]*Epsnij;
	      dstress += _CArray[id][1]*eps_t*(1-exp(-1.0*time_c*_CArray[id][2]));
	      dstress += _CArray[id][3]*eps_t*(1-exp(time_c*_CArray[id][4]));
	      LStressTemp(i,j)=dstress;	
	    }
        }
        
        
        //U = MatrixInv(U);//U^(-1)
        vector<vector<double> > SigmaR(6,vector<double>(1));
        SigmaR[0][0] = LStressTemp(0,0);
        SigmaR[1][0] = LStressTemp(1,1);
        SigmaR[2][0] = LStressTemp(2,2);
        SigmaR[3][0] = LStressTemp(1,2);
        SigmaR[4][0] = LStressTemp(0,2);
        SigmaR[5][0] = LStressTemp(0,1);

        //Consider the coupled effect
        //vector<vector<double> > U(6,vector<double>(6)); 
        //U[0][0] = 1;           U[1][0] = -1.0*_nuxy;      U[2][0] = -1.0*_nuxz;
        //U[0][1] = -1.0*_nuxy;  U[1][1] = 1;               U[2][1] = -1.0*_nuyz;
        //U[0][2] = -1.0*_nuxz;  U[1][2] = -1.0*_nuyz;      U[2][2] = 1;
        //U[3][3] = 1;           U[4][4] = 1;               U[5][5] = 1;
        //SigmaR = MatrixProd(U,SigmaR);


        STensor3 LStress(0.0);
        LStress(0,0)=SigmaR[0][0];  LStress(1,0)=SigmaR[5][0]; LStress(2,0)=SigmaR[4][0];
        LStress(0,1)=SigmaR[5][0];  LStress(1,1)=SigmaR[1][0]; LStress(2,1)=SigmaR[3][0];
        LStress(0,2)=SigmaR[4][0];  LStress(1,2)=SigmaR[3][0]; LStress(2,2)=SigmaR[2][0];
	

        //----------3.2 Transform stress into global coor sys-------------
        LStress *= invM.transpose();
        invM *= LStress;
        LStress = invM;	
		
	//----------3.3 Computer First Piola-Kirchoff stress 
	//              in global coor system-----------------------------	
        //Remark: Let s be the linear stress, F be the deformation gradient,
        //        and J be the determinant of F, the first Piola-Kirchhoff
        //        stress is formulated by 
        //        TPK = s*F^(-T)*J   
	STensor3 mlawFn(0.0);
        mlawFn = Fn1.transpose();
	double mlawFnDet = Fn1.determinant();

        P = LStress;
	P *= mlawFn.invert();//First Piola-Kirchoff Stress.	
        P *= mlawFnDet;

        //***************Compute tangent modulis***************************
        if(stiff)
        {
          for(int i=0; i<3; i++)
             for(int j=0; j<3; j++)
             {
               double EM = 0;
               if(!(abs(Epsn1_Global(i,j)) <= 1.0e-8))
               {
                EM = LStress(i,j)/Epsn1_Global(i,j);
               }
               else
               {
                int EMid = LStress.getIndex(i,j);
                EM = _CArray[EMid][0];  
               }
               Tangent(i,j,i,j) = EM;
             }
          if(elasticTangent != NULL) elasticTangent->operator=(Tangent);
        }
}

void mlawTransverseIsoYarnB::createIPState(IPStateBase* &ips, bool hasBodyForce, const bool* state_,const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
  bool inter=true;
  const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
  if(iele==NULL) inter=false;

  SPoint3 pt_Global;
  ele->pnt(GP->pt[0],GP->pt[1],GP->pt[2],pt_Global);
  SVector3 GaussP(pt_Global);

  IPVariable* ipvi;
  IPVariable* ipv1;
  IPVariable* ipv2;

  switch(_IsUniDir)
  {
   case 0: //Woven yarn
         ipvi = new IPTransverseIsoYarnB(GaussP, _YD);
         ipv1 = new IPTransverseIsoYarnB(GaussP, _YD);
         ipv2 = new IPTransverseIsoYarnB(GaussP, _YD);
         if(ips != NULL) delete ips;
         ips = new IP3State(state_,ipvi,ipv1,ipv2);
         break;
   case 1: //Strate yarn
         ipvi = new IPTransverseIsoYarnB(GaussP,_UniDir);
         ipv1 = new IPTransverseIsoYarnB(GaussP,_UniDir);
         ipv2 = new IPTransverseIsoYarnB(GaussP,_UniDir);
         if(ips != NULL) delete ips;
         ips = new IP3State(state_,ipvi,ipv1,ipv2);
         break;
   case 2:
         ipvi = new IPTransverseIsoYarnB(GaussP);
         ipv1 = new IPTransverseIsoYarnB(GaussP);
         ipv2 = new IPTransverseIsoYarnB(GaussP);
         if(ips != NULL) delete ips;
         ips = new IP3State(state_,ipvi,ipv1,ipv2);
         break;  
  }
}

double mlawTransverseIsoYarnB::soundSpeed() const
{
  double k1 = _Cxx11+_MaxStrainRate*(_Cxx21+_Cxx31);
  double k2 = _Cyy11+_MaxStrainRate*(_Cyy21+_Cyy31);
  double E = max(k1,k2);
  double nu = 0.4;
  double s = sqrt((1-nu)*(E)/((1+nu)*(1-2*nu)*_rho));
  return s;
}


