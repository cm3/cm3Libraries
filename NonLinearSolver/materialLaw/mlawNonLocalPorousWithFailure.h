// C++ Interface: material law
//
// Description: general class for nonlocal porous material law
//
// Author:  <V.D. Nguyen>, (C) 2018
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef MLAWNONLOCALPOROUSWITHFAILURE_H_
#define MLAWNONLOCALPOROUSWITHFAILURE_H_

#include "mlawNonLocalPorous.h"
#include "failureCriterion.h"
#include "DamageLaw.h"

class IPNonLocalPorosityWithCleavage;

class mlawNonLocalPorousWithCleavageFailure : public materialLaw{
  protected:
    mlawNonLocalPorosity* _nonlocalPorousLaw;
    FailureCriterionBase* _failureCriterion;

    CLengthLaw *_cLLawCleavage;
    DamageLaw  *_damLawCleavage;

  public:
    mlawNonLocalPorousWithCleavageFailure(mlawNonLocalPorosity& law, const CLengthLaw &cLLaw,
                                const DamageLaw &damLaw, const double sigmac);

    mlawNonLocalPorousWithCleavageFailure(mlawNonLocalPorosity& law, const CLengthLaw &cLLaw,
                                const DamageLaw &damLaw, const FailureCriterionBase& failureLaw);
    #ifndef SWIG
    mlawNonLocalPorousWithCleavageFailure(const mlawNonLocalPorousWithCleavageFailure& src);
    virtual ~mlawNonLocalPorousWithCleavageFailure();
    virtual materialLaw* clone() const {return new mlawNonLocalPorousWithCleavageFailure(*this);}

    virtual void setTime(const double ctime,const double dtime){
      materialLaw::setTime(ctime,dtime);
      _nonlocalPorousLaw->setTime(ctime,dtime);
    }
    virtual matname getType() const {return _nonlocalPorousLaw->getType();};
    virtual void createIPState(IPStateBase* &ips,bool hasBodyForce, const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const IntPt *GP=NULL, const int gpt = 0) const;
    virtual void createIPVariable(const double fInit, IPNonLocalPorosity* &ipv) const;
    virtual void initLaws(const std::map<int,materialLaw*> &maplaw);
    // for explicit scheme it must return sqrt(E/rho) adapted to your case
    virtual double soundSpeed() const {return _nonlocalPorousLaw->soundSpeed();};
    virtual double density() const{return _nonlocalPorousLaw->density();}
    // for multiscale analysis
    virtual double scaleFactor() const { return _nonlocalPorousLaw->scaleFactor();};
    virtual bool isHighOrder() const { return _nonlocalPorousLaw->isHighOrder();};
    virtual bool isNumeric() const {return _nonlocalPorousLaw->isNumeric();};
    virtual int getNumberOfExtraDofsDiffusion() const {return _nonlocalPorousLaw->getNumberOfExtraDofsDiffusion();}
    virtual double getInitialExtraDofStoredEnergyPerUnitField() const {return _nonlocalPorousLaw->getInitialExtraDofStoredEnergyPerUnitField();}
    virtual double getExtraDofStoredEnergyPerUnitField(double T) const {return _nonlocalPorousLaw->getExtraDofStoredEnergyPerUnitField(T);}
    virtual double getCharacteristicLength() const {return _nonlocalPorousLaw->getCharacteristicLength();};
    virtual void setMacroSolver(const nonLinearMechSolver* sv){
      materialLaw::setMacroSolver(sv);
      _nonlocalPorousLaw->setMacroSolver(sv);
    };

    virtual const CLengthLaw *getCleavageCLengthLaw() const {return _cLLawCleavage; };
    virtual const DamageLaw *getCleavageDamageLaw() const {return _damLawCleavage; };

    // Access functions
    mlawNonLocalPorosity* getNonLocalPorosityLaw() {return _nonlocalPorousLaw;};
    const mlawNonLocalPorosity* getNonLocalPorosityLaw() const {return _nonlocalPorousLaw;};
    virtual int getNumNonLocalVariables() const {return 1+_nonlocalPorousLaw->getNumNonLocalVariables();}

    mlawNonLocalPorosity::nonLocalMethod getNonLocalMethod() const {return _nonlocalPorousLaw->getNonLocalMethod();}
    double generateARandomInitialPorosityValue() const {return _nonlocalPorousLaw->generateARandomInitialPorosityValue();};

    virtual bool withEnergyDissipation() const {return _nonlocalPorousLaw->withEnergyDissipation();};
    // function to define when one IP is broken for block dissipation

    virtual void checkInternalState(IPVariable* ipv, const IPVariable* ipvprev) const ;

    public:

      virtual void predictorCorrector(const STensor3& F0,         // initial deformation gradient (input @ time n)
                              const STensor3& Fn,         // updated deformation gradient (input @ time n+1)
                              STensor3 &P,                // updated 1st Piola-Kirchhoff stress tensor (output)
                                                          // contains the initial values on input
                              const IPNonLocalPorosityWithCleavage *q0,       // array of initial internal variable
                              IPNonLocalPorosityWithCleavage *q1,             // updated array of internal variable (in ipvcur on output),
                              STensor43 &Tangent,         // constitutive tangents (output)
                              std::vector<STensor3>  &dLocalVarDF,
                              std::vector<STensor3>  &dStressDNonLocalVar,
                              fullMatrix<double>    &dLocalVarDNonLocalVar,
                              const bool stiff) const;

       virtual void constitutive(const STensor3& F0,         // initial deformation gradient (input @ time n)
                            const STensor3& Fn,         // updated deformation gradient (input @ time n+1)
                            STensor3 &P,                // updated 1st Piola-Kirchhoff stress tensor (output)
                                                        // contains the initial values on input
                            const IPVariable *q0,       // array of initial internal variable
                            IPVariable *q1,              // updated array of internal variable (in ipvcur on output),
			    STensor43 &Tangent,         // constitutive tangents (output)
			    const bool stiff,
                            STensor43* elasticTangent = NULL, 
                            const bool dTangent =false,
                            STensor63* dCalgdeps = NULL
			    ) const
      {
        Msg::Error("mlawNonLocalPorousWithCleavageFailure constitutive not defined");
      }

    #endif // SWIG
};
#endif // MLAWNONLOCALPOROUSWITHFAILURE_H_
