//
// C++ Interface: Material Law
//
// Description: Non-Linear Thermo-Visco-Mechanics (Thermo-ViscoElasto-ViscoPlasto Law) with Non-Local Damage Interface (soon.....)
//
// Author: <Ujwal Kishore J - FLE_Knight>, (C) 2022
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef MLAWNONLINEARTVP_H_
#define MLAWNONLINEARTVP_H_

#include "ipNonLinearTVP.h"
#include "mlawNonLinearTVM.h"

// class mlawNonLinearTVP : public mlawNonLinearTVE{
class mlawNonLinearTVP : public mlawNonLinearTVM{

    protected:
        // double _tol; // for plasticity convergence
        double _TaylorQuineyFactor; // for heat conversion
        bool _TaylorQuineyFlag;
        double _PolyOrderChaboche; // polynomial order of Chaboche Coeffs
        fullVector<double> _PolyCoeffsChaboche; // coeff of polymonial _coefficients(0)+_coefficients(1)*eps+...

        scalarFunction* _temFunc_Sy0; // temperature dependence of initial yield stress
        scalarFunction* _temFunc_H; // temperature dependence of hardening stress
        scalarFunction* _temFunc_visc; // temperature dependence of viscosity law
        scalarFunction* _temFunc_Hb; // temperature dependence of kinematic hardening modulus

        std::vector<double> _HR; // isotropic hardening moduli for R

    protected:

        virtual void hardening(const IPNonLinearTVP* q0, IPNonLinearTVP* q1, const double& T) const;
        virtual void getModifiedMandel(const STensor3& C, const IPNonLinearTVP *q0_, IPNonLinearTVP *q1) const;
        virtual void checkCommutavity(STensor3& commuteCheck, const STensor3& Ce, const STensor3& S, const IPNonLinearTVP *q1) const;
        virtual void getChabocheCoeffs(fullVector<double>& coeffs, const double& opt, const IPNonLinearTVP *q1, std::vector<double>* dcoeffs=NULL) const;
        virtual void getPlasticPotential(const double& phiP, const double& phiE, double& Px) const;
        virtual void getYieldCoefficientTempDers(const IPNonLinearTVP *q1, const double& T, fullVector<double>& DcoeffsDT, fullVector<double>& DDcoeffsDTT) const;

        virtual void getIsotropicHardeningForce(const IPNonLinearTVP *q0, IPNonLinearTVP *q1, const double T0, const double& T,
                                      double* ddRdTT = NULL, STensor3* ddRdFdT = NULL) const;
        virtual void getIsotropicHardeningForce(const IPNonLinearTVP *q0, IPNonLinearTVP *q1, const double T0, const double& T,
                                      const STensor3& DphiPDF, std::vector<double>* ddRdTT = NULL, std::vector<STensor3>* ddRdFdT = NULL) const;

        virtual void getMechSourceTVP(const STensor3& F0, const STensor3& F, const IPNonLinearTVP *q0, IPNonLinearTVP *q1,
                                      const double T0, const double& T, const STensor3& Fepr, const STensor3& Cepr,
                                      const STensor3& DphiPDF, double& mechSourceTVP, STensor3& dmechSourceTVPdF, double& dmechSourceTVPdT) const;

        virtual void getFreeEnergyTVM(const IPNonLinearTVP *q0, IPNonLinearTVP *q1, const double& T0, const double& T,
                                      double *psiTVM = NULL, double *DpsiTVMdT = NULL, double *DDpsiTVMdTT = NULL) const;

        virtual void getIterated_DPhi(const double& T, const IPNonLinearTVP *q0, IPNonLinearTVP *q1,
                                      const double& u, const double& v, const double& Gamma, const double& Cxtr, const double& Cxdev, const STensor3& Phipr,
                                            const double& trXn, const STensor3& devXn, const STensor3& Cepr, const STensor3& Eepr, const STensor3& Kepr,
                                            double& ptilde, STensor3& devPhi,
                                            STensor3& Phi, STensor3& N, STensor3& expGN, STensor43& dexpAdA,
                                            STensor43& Dho3, STensor43& Dho4inv, STensor43& Dho4_u_inv, double& Dho4_v_inv,
                                            STensor3& sigExtra_pr, STensor3& sigExtra, double& DsigV_dTrEe, STensor43& DsigD_dDevEe) const;

        virtual void getDho3(const double& u, const double& v, const double& Gamma, const STensor3& Cepr, const STensor3& Ceinvpr, const STensor3& Kepr,
                                  const STensor3& expGN, const STensor43& dexpAdA, STensor43& Dho3, STensor43& Dho4inv,
                                  STensor43& Dho4_u_inv, double& Dho4_v_inv, const double* DsigV_dTrEe = NULL, const STensor43* DsigD_dDevEe = NULL) const;

        virtual void getG2Tensor(const STensor3& Cepr, const STensor3& Kepr, const STensor3& expGN,
                                   const STensor43& DCeinvprDCepr, const STensor43& DKeprDCepr, STensor43& G2) const;

        virtual void getG2TTensor(const STensor3& Cepr, const STensor3& expGN, const STensor3& dKeprDT, STensor3& G2T) const;


        virtual void plasticCorrector_TVP(const STensor3& F0, const STensor3& F, const IPNonLinearTVP *q0, IPNonLinearTVP *q1, const double T0, const double T,
                                          STensor3& Kepr, STensor3& Mepr,
                                          double& u, double& v,
                                          STensor3& corKir_e, STensor3& Me, STensor3& N, STensor3& P,
                                          double& Dgamma, double& Gamma,
                                          double& ptildepr, double& ptilde,
                                          STensor3& devPhipr, STensor3& devPhi,
                                          const bool stiff = false, STensor3 *DphiPDF = NULL) const{};

        virtual void predictorCorrector_TVP_nonAssociatedFlow(const STensor3& F0, const STensor3& F, const IPNonLinearTVP *q0_, IPNonLinearTVP *q1,
                            STensor3&P, const bool stiff, STensor43& Tangent,  STensor43& dFedF, STensor43& dFpdF, STensor3& dFedT, STensor3& dFpdT,
                            const double T0, const double T1,
                            const SVector3 &gradT0,                 // previous temperature gradient
                            const SVector3 &gradT,                  // temperature gradient
                                  SVector3 &fluxT,                  // temperature flux
                                  STensor3 &dPdT,                   // mechanical-thermal coupling
                                  STensor3 &dfluxTdgradT,           // thermal tengent
                                  SVector3 &dfluxTdT,
                                  STensor33 &dfluxTdF,              // thermal-mechanical coupling
                                  double &thermalSource,            // - Cp*dTdt
                                  double &dthermalSourcedT,         // thermal source
                                  STensor3 &dthermalSourcedF,
                                  double &mechanicalSource,         // mechanical source--> convert to heat
                                  double &dmechanicalSourcedT,
                                  STensor3 &dmechanicalSourceF) const;

        virtual void predictorCorrector_TVP_AssociatedFlow(const STensor3& F, const IPNonLinearTVP *q0_, IPNonLinearTVP *q1,
                            STensor3&P, const bool stiff, STensor43& Tangent, STensor43& dFedF, STensor43& dFpdF,
                            const double T0, const double T1) const;

        virtual void predictorCorrector_ThermoViscoPlastic(
                            const STensor3& F0,         // initial deformation gradient (input @ time n)
                            const STensor3& F1,         // updated deformation gradient (input @ time n+1)
                                  STensor3& P,                // updated 1st Piola-Kirchhoff stress tensor (output)
                            const IPNonLinearTVP *q0,       // array of initial internal variable
                                  IPNonLinearTVP *q1,             // updated array of internal variable (in ipvcur on output),
                            const double& T0, // previous temperature
                            const double& T, // temperature
                            const SVector3 &gradT0, // previous temeprature gradient
                            const SVector3 &gradT, // temeprature gradient
                                  SVector3 &fluxT, // temperature flux)
                                  double &thermalSource,
                                  double &mechanicalSource,
                                  STensor43* elasticTangent) const;

        virtual void predictorCorrector_ThermoViscoPlastic(
                            const STensor3& F0,         // initial deformation gradient (input @ time n)
                            const STensor3& F1,         // updated deformation gradient (input @ time n+1)
                                  STensor3& P,                // updated 1st Piola-Kirchhoff stress tensor (output)
                            const IPNonLinearTVP *q0,       // array of initial internal variable
                                  IPNonLinearTVP *q1,             // updated array of internal variable (in ipvcur on output),
                                  STensor43 &Tangent,         // mechanical tangents (output)
                                  STensor43 &dFpdF, // plastic tangent
                                  STensor3 &dFpdT, // plastic tangent
                                  STensor43 &dFedF, // elastic tangent
                                  STensor3 &dFedT, // elastic tangent
                            const double& T0, // previous temperature
                            const double& T, // temperature
                            const SVector3 &gradT0, // previoustemeprature gradient
                            const SVector3 &gradT, // temeprature gradient
                                  SVector3 &fluxT, // temperature flux
                                  STensor3 &dPdT, // mechanical-thermal coupling
                                  STensor3 &dfluxTdgradT, // thermal tengent
                                  SVector3 &dfluxTdT,
                                  STensor33 &dfluxTdF, // thermal-mechanical coupling
                                  double &thermalSource,   // - cp*dTdt
                                  double &dthermalSourcedT, // thermal source
                                  STensor3 &dthermalSourcedF,
                                  double &mechanicalSource, // mechanical source--> convert to heat
                                  double &dmechanicalSourcedT,
                                  STensor3 &dmechanicalSourceF,
                            const bool stiff,
                                  STensor43* elasticTangent) const; // tangent is used by pointer, if NULL->no compute

        virtual void tangent_full_perturbation( // compute tangent by perturbation
                            const STensor3& F0,         // initial deformation gradient (input @ time n)
                            const STensor3& F1,         // updated deformation gradient (input @ time n+1)
                                  STensor3& P,                // updated 1st Piola-Kirchhoff stress tensor (output)
                            const IPNonLinearTVP *q0,       // array of initial internal variable
                                  IPNonLinearTVP *q1,             // updated array of internal variable (in ipvcur on output),
                            const double& T0, // previous temperature
                            const double& T, // temperature
                            const SVector3 &gradT0, // previous temeprature gradient
                            const SVector3 &gradT, // temeprature gradient
                                  SVector3 &fluxT, // temperature flux)
                                  double &thermalSource,
                                  double &mechanicalSource,
                                  STensor43 &Tangent,         // mechanical tangents (output)
                                  STensor43 &dFpdF, // plastic tangent
                                  STensor3 &dFpdT, // plastic tangent
                                  STensor43 &dFedF, // elastic tangent
                                  STensor3 &dFedT, // elastic tangent
                                  STensor3 &dPdT, // mechanical-thermal coupling
                                  STensor3 &dfluxTdgradT, // thermal tengent
                                  SVector3 &dfluxTdT,
                                  STensor33 &dfluxTdF, // thermal-mechanical coupling
                                  double &dthermalSourcedT, // thermal source
                                  STensor3 &dthermalSourcedF,
                                  double &dmechanicalSourcedT,
                                  STensor3 &dmechanicalSourceF) const;


    public:
        mlawNonLinearTVP(const int num, const double E, const double nu, const double rho, const double tol,
                         const double Tinitial, const double Alpha, const double KThCon, const double Cp,
                         const bool matrixbyPerturbation = false, const double pert = 1e-8, const bool thermalEstimationPreviousConfig = true);

        #ifndef SWIG
        mlawNonLinearTVP(const mlawNonLinearTVP& src);
        mlawNonLinearTVP& operator=(const materialLaw& source);
        virtual ~mlawNonLinearTVP();

        virtual bool withEnergyDissipation() const {return true;};

        virtual void setIsotropicHardeningCoefficients(const double HR1, const double HR2, const double HR3);

        virtual void setPolynomialOrderChabocheCoeffs(const int order);
        virtual void setPolynomialCoeffsChabocheCoeffs(const int i, const double val);

        void setTemperatureFunction_InitialYieldStress(const scalarFunction& Tfunc){
            if (_temFunc_Sy0 != NULL) delete _temFunc_Sy0;
            _temFunc_Sy0 = Tfunc.clone();
        }
        void setTemperatureFunction_Hardening(const scalarFunction& Tfunc){
            if (_temFunc_H != NULL) delete _temFunc_H;
            _temFunc_H = Tfunc.clone();
            // _compression->setTemperatureFunction(Tfunc);
            // _traction->setTemperatureFunction(Tfunc); // - this is difficult, because of multiple parameters
        }
        void setTemperatureFunction_KinematicHardening(const scalarFunction& Tfunc){
            if (_temFunc_Hb != NULL) delete _temFunc_Hb;
            _temFunc_Hb = Tfunc.clone();
            // _kinematic->setTemperatureFunction_K(Tfunc);
                    // - this is not a pure virtual parent class, on the other hand the viscosity/kine harde type isnt set here but in the .py file.
        }
        void setTemperatureFunction_Viscosity(const scalarFunction& Tfunc){
            if (_temFunc_visc != NULL) delete _temFunc_visc;
            _temFunc_visc = Tfunc.clone();
            // _viscosity->setTemperatureFunction(Tfunc);
        }

        void setTaylorQuineyFactor(const double f, const bool flag){_TaylorQuineyFactor = f; _TaylorQuineyFlag = flag;};

        virtual matname getType() const{return materialLaw::nonlinearTVP;}
        virtual void createIPState(IPStateBase* &ips, bool hasBodyForce, const bool* state_=NULL,const MElement *ele=NULL,
            const int nbFF_=0, const IntPt *GP=NULL, const int gpt = 0) const{
            Msg::Error("mlawNonLinearTVP::createIPState has not been defined");
        }
        virtual void initLaws(const std::map<int,materialLaw*> &maplaw){}; // this law is initialized so nothing to do

        virtual void constitutive(
                    const STensor3& F0,         // initial deformation gradient (input @ time n)
                    const STensor3& Fn,         // updated deformation gradient (input @ time n+1)
                          STensor3 &P,                // updated 1st Piola-Kirchhoff stress tensor (output)
                    const IPVariable *q0,       // array of initial internal variable
                          IPVariable *q1,             // updated array of internal variable (in ipvcur on output),
                          STensor43 &Tangent,         // constitutive tangents (output)
                    const bool stiff,           // if true compute the tangents
                          STensor43* elasticTangent = NULL,
                    const bool dTangent =false,
                          STensor63* dCalgdeps = NULL) const;

        virtual void constitutive(
                    const STensor3& F0,                     // initial deformation gradient (input @ time n)
                    const STensor3& F1,                     // updated deformation gradient (input @ time n+1)
                          STensor3& P,                      // updated 1st Piola-Kirchhoff stress tensor (output)
                    const IPVariable *q0,                   // array of initial internal variable
                          IPVariable *q1,                   // updated array of internal variable (in ipvcur on output),
                          STensor43 &Tangent,               // constitutive mechanical tangents (output)
                    const double& T0,                       // previous temperature
                    const double& T,                        // temperature
                    const SVector3 &gradT0,                 // previous temperature gradient
                    const SVector3 &gradT,                  // temperature gradient
                          SVector3 &fluxT,                  // temperature flux
                          STensor3 &dPdT,                   // mechanical-thermal coupling
                          STensor3 &dfluxTdgradT,           // thermal tengent
                          SVector3 &dfluxTdT,
                          STensor33 &dfluxTdF,              // thermal-mechanical coupling
                          double &thermalSource,            // - Cp*dTdt
                          double &dthermalSourcedT,         // thermal source
                          STensor3 &dthermalSourcedF,
                          double &mechanicalSource,         // mechanical source--> convert to heat
                          double &dmechanicalSourcedT,
                          STensor3 &dmechanicalSourceF,
                    const bool stiff,
                          STensor43* elasticTangent = NULL) const;

        virtual materialLaw* clone() const{return new mlawNonLinearTVP(*this);};
        virtual void checkInternalState(IPVariable* ipv, const IPVariable* ipvprev) const{}; // do nothing
        #endif // SWIG
};

#endif //mlawNonLinearTVP

/*
void mlawNonLinearTVP::plasticCorrector_TVP(const STensor3& F0, const STensor3& F,
                                            const IPNonLinearTVP *q0, IPNonLinearTVP *q1, const double T0, const double T,
                                          STensor3& Kepr, STensor3& Mepr, double& u, double& v,
                                          STensor3& corKir_e, STensor3& Me, STensor3& N, STensor3& P,
                                          double& Dgamma, double& Gamma,
                                          double& ptildepr, double& ptilde,
                                          STensor3& devPhipr, STensor3& devPhi,
                                          const bool stiff, STensor3 *DphiPDF) const{

// compute elastic predictor
  STensor3& Fp1 = q1->_Fp;
  const STensor3& Fp0 = q0->_Fp;

  // Initialise predictor values
  Fp1 = Fp0; // plastic deformation tensor
  q1->_epspbarre = q0->_epspbarre; // plastic equivalent strain
  q1->_epspCompression = q0->_epspCompression;
  q1->_epspTraction = q0->_epspTraction;
  q1->_epspShear = q0->_epspShear;
  q1->_backsig = q0->_backsig; // backstress tensor
  q1->_DbackSigDT = q0->_DbackSigDT; // DbackSigDT
  q1->_DgammaDt = 0.;

  // Get hardening parameters
  this->hardening(q0,q1,T);
  static fullVector<double> a(3), Da(3); // yield coefficients and derivatives in respect to plastic deformation
  this->getYieldCoefficients(q1,a);
  //a.print("a init");

  double Hb(0.), dHb(0.), ddHb(0.), dHbdT(0.), ddHbdgammadT(0.), ddHbddT(0.);
  if (q1->_ipKinematic != NULL){
    Hb = q1->_ipKinematic->getDR(); // kinematic hardening parameter
    dHb = q1->_ipKinematic->getDDR(); // kinematic hardening parameter derivative (dHb/dgamma)
    dHbdT = Hb * q1->_ipKinematic->getDRDT();  // make sure to normalise DRDT while defining in python file //NEW
    ddHbddT = Hb * q1->_ipKinematic->getDDRDTT();
  }

  double eta(0.),Deta(0.),DetaDT(0.);
  if (_viscosity != NULL)
    _viscosity->get(q1->_epspbarre,T,eta,Deta,DetaDT);

  // Get Cepr, Ceinvpr
  static STensor3 Fpinv, Ce, Fepr;
  STensorOperation::inverseSTensor3(Fp1,Fpinv);
  STensorOperation::multSTensor3(F,Fpinv,Fepr);
  STensorOperation::multSTensor3FirstTranspose(Fepr,Fepr,Ce);

  static STensor3 Eepr, Cepr, Ceinvpr;
  Cepr = Ce;
  STensorOperation::inverseSTensor3(Cepr,Ceinvpr);

  static STensor3 invFp0; // plastic predictor
  invFp0= Fpinv;
  STensor3& Fe = q1->_Fe;
  Fe = Fepr;

  // Predictor - TVE
  static STensor43 DlnDCepr, DlnDCe;
  static STensor63 DDlnDDCe;
  static STensor3 expGN, GammaN;
  static STensor43 dexpAdA; // estimation of dexpA/dA
  expGN = _I; // if Gamma = 0, expGN = _I
  dexpAdA = _I4;

  STensor3& Ee = q1->_Ee;
  bool ok=STensorOperation::logSTensor3(Ce,_order,Ee,&DlnDCepr,&DDlnDDCe);
  if(!ok)
  {
       P(0,0) = P(1,1) = P(2,2) = sqrt(-1.);
       return;
  }
  Ee *= 0.5;
  Eepr = Ee;
  DlnDCe = DlnDCepr;

  // update A, B
  // update A, B
  double Ke, Ge = 0.; double Kde, Gde = 0.; double KTsum, GTsum = 0.;
  double DKe, DGe = 0.; double DKde, DGde = 0.; double DKDTsum, DGDTsum = 0.;
  // ThermoViscoElasticPredictor(Ee,q0->_Ee,q0,q1,Ke,Ge,Kde,Gde,DKe,DGe,DKde,DGde,KTsum,GTsum,DKDTsum,DGDTsum,T0,T);
  mlawNonLinearTVM::ThermoViscoElasticPredictor(Ee,q0->_Ee,q0,q1,Ke,Ge,DKDTsum,DGDTsum,T0,T,stiff,Bd_stiffnessTerm);
  mlawNonLinearTVM::getTVEdCorKirDT(q0,q1,T0,T);

  // additional branch - update predictor
  double DsigV_dTrEe_pr(0.), DsigV_dTrEe(0.), DsigV_dT(0.);
  static STensor3 sigExtra_pr, sigExtra, DsigD_dT;
  static STensor43 DsigD_dDevEe_pr, DsigD_dDevEe;
  STensorOperation::zero(sigExtra_pr);
  STensorOperation::zero(sigExtra);
  STensorOperation::zero(DsigD_dT);
  STensorOperation::zero(DsigD_dDevEe_pr);
  STensorOperation::zero(DsigD_dDevEe);
  if (_useExtraBranch){
    mlawNonLinearTVM::extraBranchLaw(Ee,T,q0,q1,sigExtra_pr,stiff,NULL,NULL,&DsigV_dTrEe_pr,&DsigD_dDevEe_pr,&DsigV_dT,&DsigD_dT);
  }
  q1->_corKirExtra = sigExtra_pr;
  q1->_kirchhoff += sigExtra_pr;
  const STensor3& dKeprDT = q1->_DcorKirDT; // Get the predicted value - dont add extra branch here

  // Initialise Dho
  static STensor43 Dho3, Dho4inv, Dho4_u_inv;  // This will be the final important tensor.
  double Dho4_v_inv(0.);

  // Get Mepr
  // static STensor3 Me, Mepr, Kepr;  // Ke is corKir
  getModifiedMandel(Ce,q0,q1);
  Me = q1->_ModMandel;
  Mepr = Me;
  Kepr = q1-> _kirchhoff;

  // Get Xn; Initialise X
  static STensor3 devXn, devX;
  static double trXn, pXn, eXn, trX;
  STensorOperation::decomposeDevTr(q0->_backsig,devXn,trXn); // needed for chaboche model
  pXn = 1./3.*trXn;  // 1st invariant of Xn
  eXn = sqrt(1.5*devXn.dotprod()); // J2 invariant of Xn

  // Initialise Phipr
  static STensor3 PhiPr, Phi;
  PhiPr = q1->_ModMandel;
  PhiPr -= q1->_backsig;
  Phi = PhiPr;

  // static STensor3 devPhipr,devPhi; // effective dev stress predictor
  // static double ptildepr,ptilde;
  STensorOperation::decomposeDevTr(PhiPr,devPhipr,ptildepr);
  ptildepr/= 3.;

  // Initialise Normal
  static STensor3 devN; // dev part of yield normal
  double trN = 0.; // trace part of yield normal
  // static STensor3 N; // yield normal
  STensorOperation::zero(devN);
  STensorOperation::zero(N);

  // Get plastic poisson ratio
  q1->_nup = (9.-2.*_b)/(18.+2.*_b);
  double kk = 1./sqrt(1.+2.*q1->_nup*q1->_nup);

  // Initialise variables
  // double& Gamma = q1->_Gamma;   // flow rule parameter
  Gamma = 0.;
  double dDgammaDGamma = 0.;
  // double Dgamma = 0.; // eqplastic strain
  Dgamma = 0.; // eqplastic strain
  static fullVector<double> m(2);
  getChabocheCoeffs(m,0,q1);

  double DfDGamma = 0.;
  double dfdDgamma(0.), dfdGamma(0.);
  double dAdDgamma(0.), dAdGamma(0.);
  // double u = 1.;
  u = 1.;
  // double v = 1.;
  v = 1.;

  // Initialise Cx, Gt, Kt
  double Cxdev = 1. + 3.*m(1)*pow(kk,1.)*Hb; // pow(kk,2) DEBUG
  double Cxtr = 1. + 2.*_b/3. *m(1)*pow(kk,1.)*Hb; // pow(kk,2) DEBUG
  double dCxdevdDgamma(0.), dCxtrdDgamma(0.), dudDgamma(0.), dvdDgamma(0.), dudGamma(0.), dvdGamma(0.);
  if (Hb>0.){
    Cxdev -= m(0)*dHb*Dgamma/Hb + 1./Hb * dHbdT * (T-T0);
    Cxtr -= m(0)*dHb*Dgamma/Hb + 1./Hb * dHbdT * (T-T0);
  }
  double Gt = Ge + pow(kk,1.) * Hb/(2.*Cxdev); // pow(kk,2) DEBUG
  double Kt = Ke + pow(kk,1.) * Hb/(3.*Cxtr); // pow(kk,2) DEBUG

  // Initialise ptilde and devPhi (phi)
  ptilde = ptildepr; // current effective pressure  -> first invariant
  devPhi = devPhipr;

  // Initialise the rest
  getDho3(u,v,Gamma,Cepr,Ceinvpr,Kepr,expGN,dexpAdA,Dho3,Dho4inv,Dho4_u_inv,Dho4_v_inv,&DsigV_dTrEe,&DsigD_dDevEe);

  double PhiEqpr2 = 1.5*devPhi.dotprod();
  double PhiEqpr = sqrt(PhiEqpr2);      // -> second invariant
  double PhiEq = PhiEqpr;   // current effective deviator

  // Get f and A
  double f = a(2)*pow(PhiEq,_n) - (a(1)*ptilde+a(0));
  double A = sqrt(6.*PhiEq*PhiEq+4.*_b*_b/3.*ptilde*ptilde);

  // Initialise g
  double dgdDgamma=0., dgdGamma = 0.;
  double g = Dgamma - kk*Gamma*A;

  // Test
  double Dgammma_test = 0.;

  // NR Loop - Gamma and Dgamma
    if (q1->dissipationIsBlocked()){
      q1->getRefToDissipationActive() = false;
    }
    else{
      if (f>_tol){
        q1->getRefToDissipationActive() = true;
         // plasticity
        int ite = 0;
        int maxite = 100; // maximal number of iters
        //Msg::Error("plasticity occurs f = %e",f);
        //double f0 = fabs(f);

        while (fabs(f) >_tol or ite <1){

        // Viscosity term
        double etaOverDt = eta/this->getTimeStep();

        // Get dCxdevdDgamma, dCxtrdDgamma, dudDgamma dvdDgamma, dudGamma, dvdGamma
        dCxdevdDgamma = 0.; dCxtrdDgamma = 0.;
        if (Hb>0.){
          dCxdevdDgamma = -m(0)*1./Hb*dHb - m(0)*Dgamma*(-1./pow(Hb,2.)*pow(dHb,2.) + 1./Hb*ddHb) - (T-T0)*(-1./pow(Hb,2.)*dHb*dHbdT + 1./Hb*ddHbdgammadT);
          dCxtrdDgamma = dCxdevdDgamma;
        }
        dCxdevdDgamma += 3.*m(1)*pow(kk,1.)*dHb; // pow(kk,2.) DEBUG
        dCxtrdDgamma += (2.*_b/3.)*m(1)*pow(kk,1.)*dHb; // pow(kk,2.) DEBUG

        dudDgamma = 6.*Gamma*( pow(kk,1.)/(2.*Cxdev)*dHb - (pow(kk,1.)*Hb)/(2.*pow(Cxdev,2.))*dCxdevdDgamma ); // pow(kk,2.) DEBUG
        dvdDgamma = 2.*_b*Gamma*( pow(kk,1.)/(3.*Cxtr)*dHb - (pow(kk,1.)*Hb)/(3.*pow(Cxtr,2.))*dCxtrdDgamma ); // pow(kk,2.) DEBUG

        dudGamma = 6.*(Ge + pow(kk,1.)*Hb/(2.*Cxdev)); // pow(kk,2.) DEBUG
        dvdGamma = 2.*_b*(Ke + pow(kk,1.)*Hb/(3.*Cxtr)); // pow(kk,2.) DEBUG

        // Get Hp = dPhiPdDgamma, dPhiPdGamma
        double Hp =  (1./3.*trXn/(pow(Cxtr,2.))*dCxtrdDgamma - ptilde*dvdDgamma)*Dho4_v_inv;  // /v;

        double dPhiPdGamma_RHS = - ptilde*dvdGamma;
        if (_useExtraBranch){
            dPhiPdGamma_RHS -= 2*_b*ptilde*DsigV_dTrEe;
        }
        double dPhiPdGamma = dPhiPdGamma_RHS*Dho4_v_inv;  // /v;

        // Get He = dPhiEdGamma
          // Get DdevPhidGamma
        STensor3 DdevPhidDgamma, DdevPhidDgamma_RHS;
        STensor3 DdevPhidGamma, DdevPhidGamma_RHS;
        for (int i=0; i<3; i++)
          for (int j=0; j<3; j++){
            DdevPhidDgamma_RHS(i,j) = devXn(i,j)/(pow(Cxdev,2.))*dCxdevdDgamma - dudDgamma*devPhi(i,j);
            DdevPhidGamma_RHS(i,j) = -dudGamma*devPhi(i,j);
            for (int k=0; k<3; k++)
              for (int l=0; l<3; l++){
                DdevPhidDgamma_RHS(i,j) += (2.*_b/3.)*Gamma*Dho3(i,j,k,l)*_I(k,l)*Hp; //ADD

                DdevPhidGamma_RHS(i,j) += (2.*_b/3.)*Gamma*Dho3(i,j,k,l)*_I(k,l)*dPhiPdGamma; //ADD

                DdevPhidGamma_RHS(i,j) += Dho3(i,j,k,l)*N(k,l);

                if (_useExtraBranch){
                   DdevPhidGamma_RHS(i,j) -= DsigD_dDevEe(i,j,k,l)*3*devPhi(k,l);
                }

              }
            }
        for (int i=0; i<3; i++)
          for (int j=0; j<3; j++){
            DdevPhidGamma(i,j) = 0.;
            DdevPhidGamma(i,j) = 0.;
            for (int k=0; k<3; k++)
              for (int l=0; l<3; l++){
                DdevPhidDgamma(i,j) += Dho4_u_inv(i,j,k,l)*DdevPhidDgamma_RHS(k,l);
                DdevPhidGamma(i,j) += Dho4_u_inv(i,j,k,l)*DdevPhidGamma_RHS(k,l);
              }
          }

        double Stemp1 = STensorOperation::doubledot(devPhi,DdevPhidDgamma);
        double Stemp2 = STensorOperation::doubledot(devPhi,DdevPhidGamma);
        double He = 1.5*Stemp1/PhiEq;
        double dPhiEdGamma = 1.5*Stemp2/PhiEq;

        // dAdGamma and dDgammaDGamma
        dAdDgamma = (12.*PhiEq*He + 8.*_b*_b*ptilde*Hp/3.)/(2.*A);
        dAdGamma = (12.*PhiEq*dPhiEdGamma + 8.*_b*_b*ptilde*dPhiPdGamma/3.)/(2.*A);

        dDgammaDGamma = kk*(A+Gamma*dAdGamma);  // mistake in the paper (VD 2016)

        this->getYieldCoefficientDerivatives(q1,q1->_nup,Da);

        // DEBUG FLE
        double a0 = a(0);
        double a1 = a(1);
        double a2 = a(2);
        double da0 = Da(0);
        double da1 = Da(1);
        double da2 = Da(2);

        dfdDgamma = Da(2)*pow(PhiEq,_n) - Da(1)*ptilde -Da(0);
        dfdDgamma += a(2)*_n*pow(PhiEq,(_n-1.))*He - a(1)*Hp;

        if (Gamma>0 and etaOverDt>0)
          dfdDgamma -= _p*pow(etaOverDt,_p-1.)*Deta/this->getTimeStep()*pow(Gamma,_p);  // THIS term is absent in the paper!! WHY??

        dfdGamma = _n*a(2)*pow(PhiEq,(_n-1.))*dPhiEdGamma - a(1)*dPhiPdGamma;
        if (Gamma>0 and etaOverDt>0)
          dfdGamma -= pow(etaOverDt,_p)*_p*pow(Gamma,(_p-1.));

        DfDGamma = dfdDgamma*dDgammaDGamma + dfdGamma;

        dgdDgamma = 1. - kk*Gamma*dAdDgamma;
        dgdGamma = - dDgammaDGamma;

        double dGamma = (-f + dfdDgamma*g/dgdDgamma)/(-dfdDgamma*dgdGamma/dgdDgamma + dfdGamma); // NR
        double dDgamma = -(g+dgdGamma*dGamma)/dgdDgamma; // NR

        if (Gamma + dGamma <=0.){
            Gamma /= 2.;                // NR
        }
        else
          Gamma += dGamma;

        // Dgammma_test += dDgamma;
        if (Dgamma + dDgamma <=0.){
            Dgamma /= 2.;
            }
        else
          Dgamma += dDgamma;

        //Msg::Error("Gamma = %e",Gamma);
        //Msg::Error("Dgamma = %e",Dgamma);

        //
        // UPDATE FOR NEXT ITERATION

        // Update gamma and all Hardening moduli
        updateEqPlasticDeformation(q1,q0,q1->_nup,Dgamma);
        hardening(q0,q1,T);
        getYieldCoefficients(q1,a);
        if (q1->_ipKinematic != NULL){
            Hb = q1->_ipKinematic->getDR(); // kinematic hardening parameter
            dHb = q1->_ipKinematic->getDDR(); // kinematic hardening parameter derivative (dHb/dgamma ??)
            dHbdT = Hb * q1->_ipKinematic->getDRDT();  // make sure to normalise DRDT while defining in python file //NEW
            ddHbddT = Hb * q1->_ipKinematic->getDDRDTT();
        }
        //a.print("a update");

        // Update Cx, u, v
        Cxdev = 1. + 3.*m(1)*pow(kk,1.)*Hb;  // pow(kk,2) DEBUG
        Cxtr = 1. + 2.*_b/3. *m(1)*pow(kk,1.)*Hb; // pow(kk,2) DEBUG
        if (Hb>0.){
          Cxdev -= m(0)*dHb*Dgamma/Hb - 1./Hb * dHbdT * (T-T0);
          Cxtr -= m(0)*dHb*Dgamma/Hb - 1./Hb * dHbdT * (T-T0);
        }
        Gt = Ge + pow(kk,1.) * Hb/(2.*Cxdev); // pow(kk,2) DEBUG
        Kt = Ke + pow(kk,1.) * Hb/(3.*Cxtr); // pow(kk,2) DEBUG
        u = 1.+6.*Gt*Gamma;
        v = 1.+2.*_b*Kt*Gamma;

        // Update Phi
        getIterated_DPhi(T,q0,q1,u,v,Gamma,Cxtr,Cxdev,PhiPr,trXn,devXn,Cepr,Eepr,Kepr,ptilde,devPhi,Phi,N,expGN,dexpAdA,
                            Dho3,Dho4inv,Dho4_u_inv,Dho4_v_inv,
                            sigExtra_pr,sigExtra,DsigV_dTrEe,DsigD_dDevEe);
        PhiEq = sqrt(1.5*devPhi.dotprod());

        // Update A
        A = sqrt(6.*PhiEq*PhiEq+4.*_b*_b/3.*ptilde*ptilde);

        Dgamma = kk*Gamma*A;
        updateEqPlasticDeformation(q1,q0,q1->_nup,Dgamma);
        hardening(q0,q1,T);
        getYieldCoefficients(q1,a);

        // Update f, g
        f = a(2)*pow(PhiEq,_n) - (a(1)*ptilde+a(0));
        double viscoTerm = etaOverDt*Gamma;
        if (Gamma>0. and etaOverDt>0.) f-= pow(viscoTerm,_p);

        g = Dgamma - kk*Gamma*A;

        ite++;
        //if (ite> maxite-5)
         //Msg::Error("it = %d, DfDGamma = %e error = %e dGamma = %e, Gamma = %e",ite,DfDGamma,f,dGamma,Gamma);

        if (fabs(f) <_tol) break;

        if(ite > maxite){
          Msg::Error("No convergence for plastic correction in mlawNonLinearTVP nonAssociatedFlow Maxwell iter = %d, f = %e!!",ite,f);
          P(0,0) = P(1,1) = P(2,2) = sqrt(-1.);
          return;
        }
    } // while

    q1->_DgammaDt = Dgamma/this->getTimeStep();

    // Correct Cx, u, v
    Cxdev = 1. + 3.*m(1)*pow(kk,1.)*Hb;  // pow(kk,2.) DEBUG
    Cxtr = 1. + 2.*_b/3. *m(1)*pow(kk,1.)*Hb;  // pow(kk,2.) DEBUG
    if (Hb>0.){
      Cxdev -= m(0)*dHb*Dgamma/Hb - 1./Hb * dHbdT * (T-T0);
      Cxtr -= m(0)*dHb*Dgamma/Hb - 1./Hb * dHbdT * (T-T0);
    }
    Gt = Ge + pow(kk,1.) * Hb/(2.*Cxdev); // pow(kk,2.) DEBUG
    Kt = Ke + pow(kk,1.) * Hb/(3.*Cxtr); // pow(kk,2.) DEBUG
    u = 1.+6.*Gt*Gamma;
    v = 1.+2.*_b*Kt*Gamma;

    // Correct Phi
    getIterated_DPhi(T,q0,q1,u,v,Gamma,Cxtr,Cxdev,PhiPr,trXn,devXn,Cepr,Eepr,Kepr,ptilde,devPhi,Phi,N,expGN,dexpAdA,
                        Dho3,Dho4inv,Dho4_u_inv,Dho4_v_inv,
                        sigExtra_pr,sigExtra,DsigV_dTrEe,DsigD_dDevEe);
    PhiEq = sqrt(1.5*devPhi.dotprod());

    // Correct Normal, H = expGN
    STensorOperation::decomposeDevTr(N,devN,trN);

    // Get GammaN for mechSrcTVP
    STensorOperation::zero(GammaN);
    GammaN = N;
    GammaN *= Gamma;
    q1->_GammaN = GammaN;

    // Correct plastic deformation tensor
    STensorOperation::multSTensor3(expGN,Fp0,Fp1);

    // Correct IP gamma
    updateEqPlasticDeformation(q1,q0,q1->_nup,Dgamma);
          // Msg::Info("setting: gamma=%e ",q1->_epspbarre);

    // Correct elastic deformation tensor, corotational stress
    STensorOperation::inverseSTensor3(Fp1,Fpinv);
    STensorOperation::multSTensor3(F,Fpinv,Fe);
    STensorOperation::multSTensor3FirstTranspose(Fe,Fe,Ce);
    bool ok=STensorOperation::logSTensor3(Ce,_order,Ee,&DlnDCe,&DDlnDDCe);
    if(!ok)
    {
       P(0,0) = P(1,1) = P(2,2) = sqrt(-1.);
       return;
    }
    Ee *= 0.5;

    // Correct A, B
    // ThermoViscoElasticPredictor(Ee,q0->_Ee,q0,q1,Ke,Ge,Kde,Gde,DKe,DGe,DKde,DGde,KTsum,GTsum,DKDTsum,DGDTsum,T0,T); //,false);
    mlawNonLinearTVM::ThermoViscoElasticPredictor(Ee,q0->_Ee,q0,q1,Ke,Ge,DKDTsum,DGDTsum,T0,T,stiff,Bd_stiffnessTerm); //,false);
    mlawNonLinearTVM::getTVEdCorKirDT(q0,q1,T0,T); // update dCorKirdT - don't do this probably
    // DKDTsum and DGDTsum = sum of bulk/shear moduli derivatives wrt T

    // Correct Extrabranch - update stress
    if (_useExtraBranch){
        mlawNonLinearTVM::extraBranchLaw(Ee,T,q0,q1,sigExtra,stiff,NULL,NULL,&DsigV_dTrEe,&DsigD_dDevEe,&DsigV_dT,&DsigD_dT);
    }
    q1->_corKirExtra = sigExtra;
    q1->_kirchhoff += sigExtra;

    // Correct backstress;
    devX = (pow(kk,1.)*Hb*Gamma*devN + devXn); // pow(kk,2) DEBUG
    devX *= 1./Cxdev;
    trX = (pow(kk,1.)*Hb*Gamma*trN + trXn)*1./Cxtr; // pow(kk,2) DEBUG
    q1->_backsig = devX;
    q1->_backsig(0,0) += trX/3.;
    q1->_backsig(1,1) += trX/3.;
    q1->_backsig(2,2) += trX/3.;

    // Correct Mandel
    q1->_ModMandel = devPhi;
    q1->_ModMandel += q1->_backsig;
    q1->_ModMandel(0,0) += (ptilde);
    q1->_ModMandel(1,1) += (ptilde);
    q1->_ModMandel(2,2) += (ptilde);

    } // 2nd if
    else{
        q1->getRefToDissipationActive() = false;
    }
  } // 1st if

  corKir_e = q1->_kirchhoff;
  const STensor3& KS = q1->_kirchhoff;
  getModifiedMandel(Ce, q0, q1); // update Mandel

  const STensor3& MS = q1->_ModMandel;
  Me = MS;
  // second Piola Kirchhoff stress
  static STensor3 S, Ceinv;
  STensorOperation::inverseSTensor3(Ce,Ceinv);
  STensorOperation::multSTensor3(Ceinv,q1->_ModMandel,S);

  // first PK
  for(int i=0; i<3; i++)
    for(int j=0; j<3; j++){
      P(i,j) = 0.;
      for(int k=0; k<3; k++)
        for(int l=0; l<3; l++)
          P(i,j) += Fe(i,k)*S(k,l)*Fpinv(j,l);
    }

if (stiff){

    // DECLARE UNDECLARED HERE
    static STensor3 dFpdT;
    static STensor43 dFpdF, dFedF;
    STensorOperation::zero(dFpdT);
    STensorOperation::zero(dFpdF);
    STensorOperation::zero(dFedF);

    // dP/dF

    //1. get DtrKeprDCepr and DdevKeprDCepr, also DKeprDCepr
    static STensor3 DpDCepr;
    static STensor43 DdevKDCepr;
    static STensor43 DdevKeprDCepr, DdevMeprDCepr, DKeprDCepr;
    static STensor3 DpKeprDCepr, DpMeprDCepr;      // K = corKir; pK -> pressure of corKir
    STensorOperation::multSTensor3STensor43(_I,DlnDCepr,DpKeprDCepr);
    DpKeprDCepr*= (0.5*Ke);
    STensorOperation::multSTensor43(_Idev,DlnDCepr,DdevKeprDCepr);
    DdevKeprDCepr*= Ge;

    // DKeprDCepr
    DKeprDCepr = DdevKeprDCepr;
    for (int i=0; i<3; i++)
      for (int j=0; j<3; j++)
        for (int k=0; k<3; k++)
          for (int l=0; l<3; l++){
            DKeprDCepr(i,j,k,l) += _I(i,j)*DpKeprDCepr(k,l);
          }

    // initiate here - corrected corKir derivatives wrt Cepr
    DpDCepr = DpKeprDCepr;
    DdevKDCepr = DdevKeprDCepr;

    //2. get DCeprDCepr = _I4 and DCeinvprDCepr
    static STensor43 DCeinvprDCepr;

    for (int i=0; i<3; i++)
      for (int s=0; s<3; s++)
        for (int k=0; k<3; k++)
          for (int l=0; l<3; l++){
            DCeinvprDCepr(i,s,k,l) = 0.;
            for (int m=0; m<3; m++)
              for (int j=0; j<3; j++)
                DCeinvprDCepr(i,s,k,l) -= Ceinvpr(i,m)*_I4(m,j,k,l)*Ceinvpr(j,s);
          }

    //3. get DtrMeprDCepr and DdevMeprDCepr
    static STensor43 DMeprDCepr;

    static STensor3 temp1;
    static STensor43 temp2, temp3;
    STensorOperation::multSTensor3(Kepr,Ceinvpr,temp1);

    for (int i=0; i<3; i++)
      for (int s=0; s<3; s++)
        for (int k=0; k<3; k++)
          for (int l=0; l<3; l++){
            temp2(i,s,k,l) = 0.;
            temp3(i,s,k,l) = 0.;
            for (int m=0; m<3; m++){
              temp2(i,s,k,l) += _I4(i,m,k,l)*temp1(m,s);
              temp3(i,s,k,l) += DKeprDCepr(i,m,k,l)*Ceinvpr(m,s) + Kepr(i,m)*DCeinvprDCepr(m,s,k,l);
            }
          }

    for (int i=0; i<3; i++)
      for (int s=0; s<3; s++)
        for (int k=0; k<3; k++)
          for (int l=0; l<3; l++){
            DMeprDCepr(i,s,k,l) = 0.;
            for (int m=0; m<3; m++)
              DMeprDCepr(i,s,k,l) += Cepr(i,m)*temp3(m,s,k,l);
          }

    DMeprDCepr += temp2;
    DMeprDCepr += DKeprDCepr;
    DMeprDCepr *= 0.5;

    // Because, TrMepr = TrKepr
    DpMeprDCepr = DpKeprDCepr;

    //
    STensorOperation::zero(DdevMeprDCepr);
    for (int i=0; i<3; i++)
      for (int s=0; s<3; s++)
        for (int k=0; k<3; k++)
          for (int l=0; l<3; l++){
            DdevMeprDCepr(i,s,k,l) += DMeprDCepr(i,s,k,l) - _I(i,s)*DpMeprDCepr(k,l);
          }

    //4. get DdevphiDCepr and DphiPprDCepr
    static STensor3 DphiPprDCepr, DphiPDCepr;
    static STensor43 DdevphiprDCepr, DdevphiDCepr;

    DphiPprDCepr = DpMeprDCepr;
    DdevphiprDCepr = DdevMeprDCepr;

    DphiPDCepr = DphiPprDCepr;
    DphiPDCepr *= 1./v;
    DdevphiDCepr = DdevphiprDCepr;
    DdevphiDCepr *= 1./u;

    //5. get DphiEprDCepr from DdevphiDCepr
    static STensor3 DphiEDCepr, DphiEDdevPhi;
    if (PhiEq >0.){
        DphiEDdevPhi = devPhi;
        DphiEDdevPhi *= 1.5/(PhiEq);
    }

    STensorOperation::multSTensor3STensor43(DphiEDdevPhi,DdevphiDCepr,DphiEDCepr);

    // 6. to 11. (inside if loop-> Gamma > 0.)
      static STensor3 dAdCepr, dfDCepr, dgDCepr;
      static STensor3 DGDCepr;
      static STensor3 DgammaDCepr;
      static STensor3 DtrNDCepr;
      static STensor43 DdevNDCepr;
      static STensor43 dFpDCepr;
      static STensor43 dCedCepr, dCeinvdCepr;
      static STensor43 dXdCepr;
      static STensor3 dTrXdCepr;
      static STensor43 dGammaNdCepr;

      if (Gamma >0.){

        //6. get dAdCepr from DdevphiDCepr
        double fact = a(2)*_n*pow(PhiEq,_n-1.);
        for (int i=0; i<3; i++){
          for (int j=0; j<3; j++){
            dAdCepr(i,j) = ( 4./3.*_b*_b*ptilde*DphiPDCepr(i,j) + 6.*PhiEq*DphiEDCepr(i,j) )/A;
            dfDCepr(i,j) = fact*DphiEDCepr(i,j)-a(1)*DphiPDCepr(i,j);
            // dgDCepr(i,j) = -kk*Gamma*dAdCepr(i,j);
          }
        }
        dgDCepr = dAdCepr;
        dgDCepr *= (-kk*Gamma);

        //7. get DGDCepr, DgammaDCepr
        static STensor3 DGDCepr_test, DgammaDCepr_test;
          for (int i=0; i<3; i++){
            for (int j=0; j<3; j++){
              DGDCepr(i,j) = (-dfDCepr(i,j) + dfdDgamma*dgDCepr(i,j)/dgdDgamma)/(dfdGamma - dfdDgamma*dgdGamma/dgdDgamma);
            }
          }

          for (int i=0; i<3; i++){
            for (int j=0; j<3; j++){
              DgammaDCepr(i,j) = (-dgdGamma*DGDCepr(i,j) - dgDCepr(i,j))/dgdDgamma;
            }
          }

        //8. update DdevphiDCepr and DphiEDCepr, DphiPDCepr
        static STensor3 dCxdevdCepr, dCxtrdCepr;
        dCxdevdCepr = DgammaDCepr;
        dCxdevdCepr *= dCxdevdDgamma;
        dCxtrdCepr = DgammaDCepr;
        dCxdevdCepr *= dCxtrdDgamma;

        static STensor3 dudCepr, dvdCepr;
        for (int i=0; i<3; i++)
          for (int j=0; j<3; j++){
            dudCepr(i,j) = dudDgamma*DgammaDCepr(i,j) + dudGamma*DGDCepr(i,j);
            dvdCepr(i,j) = dvdDgamma*DgammaDCepr(i,j) + dvdGamma*DGDCepr(i,j);
          }

       // update DphiPDCepr
       for (int i=0; i<3; i++)
         for (int j=0; j<3; j++){
           DphiPDCepr(i,j) += ( -dvdCepr(i,j)*ptilde + 1./3.*trXn/(pow(Cxtr,2.))*dCxtrdCepr(i,j) )/v;
         }

       // update DdevphiDCepr, DphiEDCepr
       // Use Dho for DdevphiDCepr
       static STensor43 G2, G3;
       getG2Tensor(Cepr,Kepr,expGN,DCeinvprDCepr,DKeprDCepr,G2);
       for (int i=0; i<3; i++)
         for (int j=0; j<3; j++)
           for (int k=0; k<3; k++)
             for (int l=0; l<3; l++){
               G3(i,j,k,l) = G2(i,j,k,l) + DdevphiprDCepr(i,j,k,l) + devXn(i,j)*dCxdevdCepr(k,l)/(pow(Cxdev,2.)) - devPhi(i,j) * dudCepr(k,l);
               for (int p=0; p<3; p++)
                 for (int q=0; q<3; q++)
                    G3(i,j,k,l) +=  Dho3(i,j,p,q)* ( N(p,q)*DGDCepr(k,l) + 2.*_b*Gamma/3.*_I(p,q)*DphiPDCepr(k,l) );
              }

        STensorOperation::multSTensor43(Dho4_u_inv,G3,DdevphiDCepr);
        STensorOperation::multSTensor3STensor43(DphiEDdevPhi,DdevphiDCepr,DphiEDCepr);


        //9. get DtrNDCepr DdevNdCepr
        DtrNDCepr = DphiPDCepr;
        DtrNDCepr *= (2.*_b);

        DdevNDCepr = DdevphiDCepr;
        DdevNDCepr *= 3.;

        // 10. get dFpDCepr

        // dFpDCepr
        STensorOperation::zero(dGammaNdCepr);
        for (int i=0; i<3; i++)
          for (int j=0; j<3; j++){
            for (int k=0; k<3; k++)
              for (int l=0; l<3; l++)
                dGammaNdCepr(i,j,k,l) = N(i,j)*DGDCepr(k,l) + Gamma*DdevNDCepr(i,j,k,l) + Gamma/3.*_I(i,j)*DtrNDCepr(k,l);
        }

        static STensor43 CeprFp0;
        for (int i=0; i<3; i++)
          for (int j=0; j<3; j++)
            for (int k=0; k<3; k++)
              for (int l=0; l<3; l++){
                CeprFp0(i,j,k,l) = 0.;
                for (int s=0; s<3; s++){
                  CeprFp0(i,j,k,l) += dexpAdA(i,s,k,l)*Fp0(s,j);
                }
              }

         STensorOperation::multSTensor43(CeprFp0,dGammaNdCepr,dFpDCepr);

         // 10.1 get dXDCepr
         static STensor3 DtrXdCepr;
         static STensor43 DdevXdCepr;
         for (int i=0; i<3; i++)
           for (int j=0; j<3; j++){
             DtrXdCepr(i,j) = pow(kk,1.)*dHb*Gamma*trN*DgammaDCepr(i,j) + pow(kk,1.)*Hb*( trN*DGDCepr(i,j) + Gamma*DtrNDCepr(i,j) ) - trX*dCxtrdCepr(i,j) ; // pow(kk,2.) DEBUG
             for (int k=0; k<3; k++)
               for (int l=0; l<3; l++){
                 DdevXdCepr(i,j,k,l) = pow(kk,1.)*dHb*Gamma*devN(i,j)*DgammaDCepr(k,l) + pow(kk,1.)*Hb*( devN(i,j)*DGDCepr(k,l) + Gamma*DdevNDCepr(i,j,k,l) ) -
                                        devX(i,j)*dCxdevdCepr(k,l) ; // pow(kk,2.) DEBUG
                }
           }
         DdevXdCepr *= 1./Cxdev;
         DtrXdCepr *= 1./Cxtr;

         STensorOperation::zero(dXdCepr);
         dXdCepr = DdevXdCepr;
         for (int i=0; i<3; i++)
           for (int j=0; j<3; j++)
             for (int k=0; k<3; k++)
               for (int l=0; l<3; l++)
                 dXdCepr(i,j,k,l) += 1./3. * _I(i,j)*DtrXdCepr(k,l);


         // 11. update DpDCepr, DdevKDCepr
         // (DpKeprDCepr DdevKeprDCepr)
         for (int i=0; i<3; i++){
           for (int j=0; j<3; j++){
             DpDCepr(i,j) -= Ke*(DGDCepr(i,j)*trN + Gamma*DtrNDCepr(i,j));
             for (int k=0; k<3; k++){
               for (int l=0; l<3; l++){
                 DdevKDCepr(i,j,k,l) -=  2.*Ge*(DGDCepr(k,l)*devN(i,j) + Gamma*DdevNDCepr(i,j,k,l));
               }
             }
           }
         }

      } // if Gamma
      else{
      // elastic
        STensorOperation::zero(DgammaDCepr);
        STensorOperation::zero(dFpDCepr);
        STensorOperation::zero(DtrNDCepr);
        STensorOperation::zero(DdevNDCepr);
        STensorOperation::zero(DGDCepr);
        STensorOperation::zero(dXdCepr);
        STensorOperation::zero(dTrXdCepr);
      }

      // 12. get dKcorDcepr
      static STensor43 dKcorDcepr;
      dKcorDcepr = DdevKDCepr;
      for (int i=0; i<3; i++){
        for (int j=0; j<3; j++){
          for (int k=0; k<3; k++){
            for (int l=0; l<3; l++){
              dKcorDcepr(i,j,k,l) += _I(i,j)*DpDCepr(k,l);
            }
          }
        }
      }

      // 13. get  CeprToF conversion tensor
      static STensor43 CeprToF;
      for (int i=0; i<3; i++){
        for (int j=0; j<3; j++){
          for (int k=0; k<3; k++){
            for (int l=0; l<3; l++){
              CeprToF(i,j,k,l) = 2.*Fepr(k,i)*invFp0(j,l);
            }
          }
        }
      }

      static STensor43 DKcorDF;
      STensorOperation::multSTensor43(dKcorDcepr,CeprToF,DKcorDF);

      // 14. get dSdCepr and dSdF; also get dMedCepr and dMedF
      // Done below -> need dCedF

      STensor43& dXdF = q1->getRefToDbackStressdF();
      STensorOperation::zero(dXdF);
      STensorOperation::multSTensor43(dXdCepr,CeprToF,dXdF);

      STensor43& dGammaNdF = q1->_dGammaNdF;
      STensorOperation::zero(dGammaNdF);
      STensorOperation::multSTensor43(dGammaNdCepr,CeprToF,dGammaNdF);

      // DphiPDCepr - use this for the pressure term in R
      if(DphiPDF!=NULL){
        STensor3 _DphiPDF(0.);
        for (int i=0; i<3; i++)
          for (int j=0; j<3; j++){
            _DphiPDF(i,j) = 0.;
            for (int k=0; k<3; k++)
              for (int l=0; l<3; l++){
                _DphiPDF(i,j) = DphiPDCepr(k,l)*CeprToF(k,l,i,j);
              }
          }
        *DphiPDF = _DphiPDF;
      }
        // 15. get DgammaDF
        STensor3& DgammaDF = q1->_DgammaDF;
        STensor3& DGammaDF = q1->_DGammaDF;
        STensorOperation::zero(DgammaDF);
        STensorOperation::zero(DGammaDF);
        if (Gamma > 0){
          STensorOperation::multSTensor3STensor43(DgammaDCepr,CeprToF,DgammaDF);
          STensorOperation::multSTensor3STensor43(DGDCepr,CeprToF,DGammaDF);
          STensorOperation::multSTensor43(dFpDCepr,CeprToF,dFpdF);
        }

        // 16. Everything else
        static STensor43 DinvFpDF, dCedF, dCeinvDF; //
        for (int i=0; i<3; i++)
          for (int s=0; s<3; s++)
            for (int k=0; k<3; k++)
              for (int l=0; l<3; l++){
                DinvFpDF(i,s,k,l) = 0.;
                for (int m=0; m<3; m++)
                  for (int j=0; j<3; j++)
                    DinvFpDF(i,s,k,l) -= Fpinv(i,m)*dFpdF(m,j,k,l)*Fpinv(j,s);
              }

        for (int m=0; m<3; m++)
          for (int j=0; j<3; j++)
            for (int k=0; k<3; k++)
              for (int l=0; l<3; l++){
                dFedF(m,j,k,l) = _I(m,k)*Fpinv(l,j);
                for (int s=0; s<3; s++)
                  dFedF(m,j,k,l) += F(m,s)*DinvFpDF(s,j,k,l);
              }

        for (int i=0; i<3; i++)
          for (int j=0; j<3; j++)
            for (int k=0; k<3; k++)
              for (int l=0; l<3; l++){
                dCedF(i,j,k,l) = 0.;
                for (int p=0; p<3; p++){
                    dCedF(i,j,k,l) += Fe(p,i)*dFedF(p,j,k,l) + dFedF(p,i,k,l)*Fe(p,j); // This Works!
                }
              }

        for (int i=0; i<3; i++)
          for (int s=0; s<3; s++)
            for (int k=0; k<3; k++)
              for (int l=0; l<3; l++){
                dCeinvDF(i,s,k,l) = 0.;
                for (int m=0; m<3; m++)
                  for (int j=0; j<3; j++)
                    dCeinvDF(i,s,k,l) -= Ceinv(i,m)*dCedF(m,j,k,l)*Ceinv(j,s);
              }

        // 17. Tangent - dPdF
        static STensor43 dSdCepr, dSdF, dMedCepr;
        STensor43& dMedF = q1->getRefToDModMandelDF();
        STensorOperation::zero(dMedF);
        for (int i=0; i<3; i++)
          for (int j=0; j<3; j++)
            for (int k=0; k<3; k++)
              for (int l=0; l<3; l++){
                dMedF(i,j,k,l) = DKcorDF(i,j,k,l);
                for (int m=0; m<3; m++)
                  for (int s=0; s<3; s++)
                    dMedF(i,j,k,l) += dCedF(i,m,k,l)*KS(m,s)*Ceinv(s,j) + Ce(i,m)*(DKcorDF(m,s,k,l)*Ceinv(s,j) + KS(m,s)*dCeinvDF(s,j,k,l));
                                                     // KS is corKir
              }
        dMedF *= 0.5;

    // dP/dT

    // 1. get dMeprDT from dKeprDT  -> DcorKirprDT from TVE
    static STensor3 dMeprDT, dDevMeprDT, DcorKirDT;
    static double dpMeprDT;
    // const STensor3& dKeprDT = q1->getConstRefToDcorKirDT(); // Predictor DcorKirDT
    STensorOperation::zero(dMeprDT);

    static STensor3 temp16, temp17;
    STensorOperation::multSTensor3(dKeprDT, Ceinvpr, temp16);
    STensorOperation::multSTensor3(Cepr, temp16, temp17);

    dMeprDT = 0.5*(dKeprDT + temp17);

    STensorOperation::decomposeDevTr(dMeprDT,dDevMeprDT,dpMeprDT);
    dpMeprDT = dMeprDT.trace()/3.;

    DcorKirDT = dKeprDT; // update later

    // 2. get dPhipprDT and dDevPhiprDT
    static double dPhipprDT;
    static STensor3 dDevPhiprDT;

    dPhipprDT = dpMeprDT;
    dDevPhiprDT = dDevMeprDT;

    // 3. get dXdT
    // need this for DmechsourceDT -> initialise here (X is backStress)
    STensor3& dXdT = q1->getRefToDbackStressdT();
    STensorOperation::zero(dXdT);
    STensor3& dGammaNdT =  q1->_dGammaNdT;
    STensorOperation::zero(dGammaNdT);

    // 4. get dPhiPdT and dDevPhiDT
    static double dPhiPdT;
    static STensor3 dDevPhiDT;

    dPhiPdT = dPhipprDT;
    dPhiPdT /= v;

    dDevPhiDT = dDevPhiprDT;
    dDevPhiDT *= 1./u;

    // 5. get dPhiEdT
    static double dPhiEdT;
    STensorOperation::doubleContractionSTensor3(DphiEDdevPhi,dDevPhiDT,dPhiEdT);

    // 6. to 11. (inside if loop-> Gamma > 0.)
    static double dAdT, dfdT, dgdT;
    static double dGammaDT;
    static double DgammaDT;
    static double DtrNdT;
    static STensor3 DdevNdT;
    static STensor3 dFpDT;

    double eta(0.),Deta(0.),DetaDT(0.);
    if (_viscosity != NULL)
        _viscosity->get(q1->_epspbarre,T,eta,Deta,DetaDT);
    double etaOverDt = eta/this->getTimeStep();

    if (Gamma >0){
        // plastic

        // 6. get dAdT, dfdT, dgdT
        fullVector<double> DaDT(3), DDaDTT(3);
        getYieldCoefficientTempDers(q1,T,DaDT,DDaDTT);

        dAdT = (4./3.*_b*_b*ptilde*dPhiPdT + 6.*PhiEq*dPhiEdT)/A;
        dfdT = DaDT(2)*pow(PhiEq,_n) + a(2)*_n*pow(PhiEq,_n-1.)*dPhiEdT - DaDT(1)*ptilde - a(1)*dPhiPdT - DaDT(0);
        if (this->getTimeStep()>0){
          dfdT -= (DetaDT*Gamma/this->getTimeStep())*_p*pow((eta*Gamma/this->getTimeStep()),(_p-1.));
        }

        dgdT = -kk*Gamma*dAdT;

        // 7. get dGammaDT, DgammaDT
        dGammaDT = (-dfdT + dfdDgamma*dgdT/dgdDgamma)/(dfdGamma - dfdDgamma*dgdGamma/dgdDgamma);
        DgammaDT = (-dgdGamma*dGammaDT - dgdT)/dgdDgamma;

        // 8.0 update dCxdT, dudT, dvdT
        static double dCxdevdT, dCxtrdT;
        dCxdevdT = 0.; dCxtrdT = 0.;
        // dHbdT+dHb*DgammaDT
        if (Hb>0.){
           dCxdevdT = -m(0)*( -1./pow(Hb,2.)*dHb*Dgamma*(dHbdT) + 1./Hb*(ddHbdgammadT*Dgamma + dHb*DgammaDT))
                      + (1./pow(Hb,2.)*(dHbdT)*(dHbdT) - 1./Hb*ddHbddT)*(T-T0) -1./Hb*dHbdT;
        }
        dCxtrdT = dCxdevdT;
        dCxdevdT += 3.*m(1)*pow(kk,1.)*(dHbdT); // pow(kk,2.) DEBUG
        dCxtrdT += 2.*_b/3.*m(1)*pow(kk,1.)*(dHbdT); // pow(kk,2.) DEBUG

        static double dudT, dvdT;
        dudT = 6.*Gamma*(DGDTsum + pow(kk,1.)/(2.*Cxdev)*(dHbdT) - (pow(kk,1.)*Hb)/(2.*pow(Cxdev,2))*dCxdevdT); // pow(kk,2.) DEBUG
        dvdT = 2.*_b*Gamma*(DKDTsum + pow(kk,1.)/(3.*Cxtr)*(dHbdT) - (pow(kk,1.)*Hb)/(3.*pow(Cxtr,2))*dCxtrdT); // pow(kk,2.) DEBUG

        dudT += 6.*dGammaDT*(Ge+pow(kk,1)*Hb/(2.*Cxdev));  // pow(kk,2) DEBUG
        dvdT += 2.*_b*dGammaDT*(Ke+pow(kk,1)*Hb/(3.*Cxtr));  // pow(kk,2) DEBUG

        // 8. update DdevphiDT and DphiPDT
        dPhiPdT = (dPhipprDT - dvdT*ptilde + 1./3.*trXn/(pow(Cxtr,2.))*dCxtrdT)/v;

        static STensor3 G2T, G3T;
        getG2TTensor(Cepr,expGN,dKeprDT,G2T);
        for (int i=0; i<3; i++)
          for (int j=0; j<3; j++){
            G3T(i,j) = G2T(i,j) + dDevPhiprDT(i,j) + devXn(i,j)/pow(Cxdev,2.)*dCxdevdT - dudT*devPhi(i,j);
            for (int p=0; p<3; p++)
              for (int q=0; q<3; q++)
                G3T(i,j) += Dho3(i,j,p,q)*(dGammaDT*N(p,q) + 2.*_b*Gamma/3.*dPhiPdT*_I(p,q));
          }

        STensorOperation::multSTensor3STensor43(G3T,Dho4_u_inv,dDevPhiDT);

        // 9. get DtrNdT DdevNdT
        DtrNdT = dPhiPdT;
        DtrNdT *= (2.*_b);

        DdevNdT = dDevPhiDT;
        DdevNdT *= 3.;

        // 10. get dFpdT
        for (int i=0; i<3; i++)
          for (int j=0; j<3; j++)
            dGammaNdT(i,j) += N(i,j)*dGammaDT + Gamma*DdevNdT(i,j) + Gamma/3.*_I(i,j)*DtrNdT;

        static STensor43 CeprFp0;
        for (int i=0; i<3; i++)
          for (int j=0; j<3; j++)
            for (int k=0; k<3; k++)
              for (int l=0; l<3; l++){
                CeprFp0(i,j,k,l) = 0.;
                for (int s=0; s<3; s++){
                  CeprFp0(i,j,k,l) += dexpAdA(i,s,k,l)*Fp0(s,j);
            }
          }

        for (int i=0; i<3; i++)
          for (int j=0; j<3; j++){
            dFpdT(i,j) = 0.;
            for (int k=0; k<3; k++)
              for (int l=0; l<3; l++){
                dFpdT(i,j) += CeprFp0(i,j,k,l)*dGammaNdT(k,l);
              }
            }

        // 10.1 dXdT - backstress temperature derivative -> correction - CHECK!!!
        static double DtrXdT;
        static STensor3 DdevXdT;
        DtrXdT = pow(kk,1.)*Gamma*(dHbdT)*trN + pow(kk,1.)*Hb*(dGammaDT*trN + Gamma*DtrNdT) - dCxtrdT*trX; // pow(kk,2.) DEBUG
        for (int i=0; i<3; i++)
          for (int j=0; j<3; j++){
                DdevXdT(i,j) = pow(kk,1.)*Gamma*(dHbdT)*devN(i,j) + pow(kk,1.)*Hb*( dGammaDT*devN(i,j) + Gamma*DdevNdT(i,j) ) - dCxdevdT*devX(i,j) ; // pow(kk,2.) DEBUG
        }
        DdevXdT *= 1./Cxdev;
        DtrXdT *= 1./Cxtr;

        STensorOperation::zero(dXdT);
        dXdT = DdevXdT;
        dXdT(0,0) = DtrXdT/3.;
        dXdT(1,1) = DtrXdT/3.;
        dXdT(2,2) = DtrXdT/3.;

        // 11. DcorKirDT
        for (int i=0; i<3; i++){
          for (int j=0; j<3; j++){
            DcorKirDT(i,j) -= ( DKDTsum*Gamma*trN + Ke*(dGammaDT*trN+Gamma*DtrNdT))*_I(i,j);
            DcorKirDT(i,j) -=  2.*( DGDTsum*Gamma*devN(i,j) + Ge*(dGammaDT*devN(i,j)+Gamma*DdevNdT(i,j)));
          }
        }
        q1->_DcorKirDT = DcorKirDT; // Update in IP for mechSrcTVE

      } // if Gamma
      else{
          // elastic
          STensorOperation::zero(DgammaDT);
          // STensorOperation::zero(dFpDT);
          STensorOperation::zero(DtrNdT);
          STensorOperation::zero(DdevNdT);
          STensorOperation::zero(dGammaDT);
          }

      // 12. get dKcorDT
      // done above

      // 13. get DinvFpdT, dFedT, dCedT, dCeinvDT
      static STensor3 DinvFpdT, dFedT, dCedT, dCeinvdT;
      STensorOperation::zero(DinvFpdT);
       for (int i=0; i<3; i++)
         for (int j=0; j<3; j++)
           for (int p=0; p<3; p++)
             for (int q=0; q<3; q++){
               DinvFpdT(i,j) -= Fpinv(i,p)*dFpdT(p,q)*Fpinv(q,j);
             }

       STensorOperation::zero(dFedT);
       for (int i=0; i<3; i++)
         for (int j=0; j<3; j++)
           for (int k=0; k<3; k++){
             dFedT(i,j) += F(i,k)*DinvFpdT(k,j);
           }

       STensorOperation::zero(dCedT);
       for (int i=0; i<3; i++)
         for (int j=0; j<3; j++){
             for (int p=0; p<3; p++){
               dCedT(i,j) += Fe(p,i)*dFedT(p,j) + dFedT(p,i)*Fe(p,j); // This Works!
             }
           }

       STensorOperation::zero(dCeinvdT);
       for (int i=0; i<3; i++)
         for (int s=0; s<3; s++)
           for (int k=0; k<3; k++)
             for (int l=0; l<3; l++){
               dCeinvdT(i,s) -= Ceinv(i,k)*dCedT(k,l)*Ceinv(l,s);
             }

        // 13.1 get dMeDT -> needed for DmechSourceDT
        STensor3& dMeDT = q1->getRefToDModMandelDT();
        STensorOperation::zero(dMeDT);

        for (int i=0; i<3; i++)
          for (int j=0; j<3; j++){
            dMeDT(i,j) = DcorKirDT(i,j);
            for (int k=0; k<3; k++)
              for (int l=0; l<3; l++){
                dMeDT(i,j) += dCedT(i,k)*KS(k,l)*Ceinv(l,j) + Ce(i,k)*(DcorKirDT(k,l)*Ceinv(l,j) + KS(k,l)*dCeinvdT(l,j));
              }
            }
        dMeDT *= 0.5;

  } // stiff

};
*/
