//
// C++ Interface: terms
//
// Description: Define electrophysiology law
//
//
// Author:  <A. Jerusalem>, (C) 2013
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef _ELECTROPHYSIOLAW_H_
#define _ELECTROPHYSIOLAW_H_
#ifndef SWIG
#include "ipstate.h"
#include "MElement.h"
#include "ipElectroPhysio.h"
#include "STensor43.h"
#endif
class ElectroPhysioLaw{
 public :
  enum electrophysiolawname{cabletheory,hodgkinhuxley};
 protected :
  int _num; // number of law (must be unique !)
  bool _initialized; // to initialize law
 public:
  // constructor
#ifndef SWIG
  ElectroPhysioLaw(const int num, const bool init=true);
  ~ElectroPhysioLaw(){}
  ElectroPhysioLaw(const ElectroPhysioLaw &source);
  ElectroPhysioLaw& operator=(const ElectroPhysioLaw &source);
  virtual int getNum() const{return _num;}
  virtual electrophysiolawname getType() const=0;
  virtual void createIPVariable(IPElectroPhysio* &ipv) const=0;
  virtual void initLaws(const std::map<int,ElectroPhysioLaw*> &maplaw)=0;
  virtual const bool isInitialized() {return _initialized;}
  virtual void computePotential(double p1, double p0, double phiel, const STensor3 &Fe, 
                             const STensor3 &Fp, const STensor3 &Peff,
                             const STensor43 &Cel, const IPElectroPhysio &ipvprev, IPElectroPhysio &ipvcur) const=0;
  virtual ElectroPhysioLaw * clone() const=0;
#endif
};

class CytoConductionLaw : public ElectroPhysioLaw
{
 public :
 protected :
  double rhoc, rhotilde;
  
 public:
  // constructor
  CytoConductionLaw(const int num, const double _rhoc, const double _rhotilde, const bool init=true);
  ~CytoConductionLaw(){}
#ifndef SWIG
  CytoConductionLaw(const CytoConductionLaw &source);
  CytoConductionLaw& operator=(const ElectroPhysioLaw &source);
  virtual electrophysiolawname getType() const {return ElectroPhysioLaw::cytoconduction;};
  virtual void createIPVariable(IPElectroPhysio* &ipv) const;
  virtual void initLaws(const std::map<int,ElectroPhysioLaw*> &maplaw){}; //nothing now, we will see if we use the mapping;
  virtual ElectroPhysioLaw * clone() const;
#endif
};

class CableTheoryLaw : public ElectroPhysioLaw // TO BE MODIFIED
{
/*  public : */
/*  protected : */
/*   double p0, pc, n, alpha; */
  
/*  public: */
/*   // constructor */
/*   CableTheoryLaw(const int num, const double _p0, const double _pc, const double _n, */
/*                             const double _alpha, const bool init=true); */
/*   ~CableTheoryLaw(){} */
/* #ifndef SWIG */
/*   CableTheoryLaw(const CableTheoryLaw &source); */
/*   CableTheoryLaw& operator=(const ElectroPhysioLaw &source); */
/*   virtual electrophysiolawname getType() const {return ElectroPhysioLaw::cabletheory;}; */
/*   virtual void createIPVariable(IPElectroPhysio* &ipv) const; */
/*   virtual void initLaws(const std::map<int,ElectroPhysioLaw*> &maplaw){}; //nothing now, we will see if we use the mapping; */
/*   virtual void computePotential(double p1, double p0, double phiel, const STensor3 &Fe,  */
/*                            const STensor3 &Fp, const STensor3 &Peff, */
/*                              const STensor43 &Cel, const IPElectroPhysio &ipvprev, IPElectroPhysio &ipvcur) const; */
/*   virtual ElectroPhysioLaw * clone() const; */
/* #endif */
};


class HodgkinHuxleyLaw : public ElectroPhysioLaw // TO BE MODIFIED
{
/*  public : */
/*  protected : */
/*   double p0, pc, alpha, beta;  */
  
/*  public: */
/*   // constructor */
/*   HodgkinHuxleyLaw(const int num, const double _p0, const double _pc, const double _alpha, */
/*                             const double _beta, const bool init=true); */
/*   ~HodgkinHuxleyLaw(){} */
/* #ifndef SWIG */
/*   HodgkinHuxleyLaw(const HodgkinHuxleyLaw &source); */
/*   HodgkinHuxleyLaw& operator=(const ElectroPhysioLaw &source); */
/*   virtual electrophysiolawname getType() const {return ElectroPhysioLaw::hodgkinhuxley;}; */
/*   virtual void createIPVariable(IPElectroPhysio* &ipv) const; */
/*   virtual void initLaws(const std::map<int,ElectroPhysioLaw*> &maplaw){}; //nothing now, we will see if we use the mapping; */
/*   virtual void computePotential(double p1, double p0, double phiel, const STensor3 &Fe,  */
/*                              const STensor3 &Fp, const STensor3 &Peff, */
/*                              const STensor43 &Cel, const IPElectroPhysio &ipvprev, IPElectroPhysio &ipvcur) const; */
/*   virtual ElectroPhysioLaw * clone() const; */
/* #endif */
};

#endif //ELECTROPHYSIOLAW_H_
