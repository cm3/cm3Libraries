//
// Description: Define viscosity laws for  plasticity
//
//
// Author:  <V.D. Nguyen>, (C) 2014
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef VISCOSITYLAW_H_
#define VISCOSITYLAW_H_

#include <cstddef>
#include "scalarFunction.h"

class viscosityLaw {
  protected:
    int _num;
    bool _initialized;
    

  #ifndef SWIG
  public:
    viscosityLaw(const int num, const bool init = true);
    viscosityLaw(const viscosityLaw& src);
    virtual viscosityLaw& operator = (const viscosityLaw& src);
    virtual  ~viscosityLaw(){};
    virtual int getNum() const {return _num;};
    virtual void get(const double p, double& R, double& dR) const = 0;
    virtual void get(const double p, const double T, double& R, double& dR, double& dRdT) const = 0;
    virtual void get(const double p, const double T, double& R, double& dR, double& dRdT, double& ddRdTT) const = 0;
    virtual void get(const double p, const double T, double& R, double& dR, double& dRdT, double& ddRdTT, double& ddRdT) const = 0;
    virtual viscosityLaw* clone() const = 0;
  #endif // SWIG
};

class constantViscosityLaw : public viscosityLaw{
  protected:
    double _eta0;
    scalarFunction* _temFunc;

  public:
    constantViscosityLaw(const int num, const double val, const bool init = true);
    void setTemperatureFunction(const scalarFunction& Tfunc);
    #ifndef SWIG
    constantViscosityLaw(const constantViscosityLaw& src);
    virtual constantViscosityLaw& operator = (const viscosityLaw& src);
    virtual ~constantViscosityLaw();
    virtual void get(const double p, double& R, double& dR) const{R = _eta0; dR = 0.;};
    virtual void get(const double p, const double T, double& R, double& dR, double& dRdT) const; 
    virtual void get(const double p, const double T, double& R, double& dR, double& dRdT, double& ddRdTT) const; 
    virtual void get(const double p, const double T, double& R, double& dR, double& dRdT, double& ddRdTT, double& ddRdT) const; 
    virtual viscosityLaw* clone() const {return new constantViscosityLaw(*this);};
    #endif // SWIG
};

class saturatePowerViscosityLaw : public viscosityLaw{
  protected:
    double _eta0;
    double _q;
    scalarFunction* _temFunc_eta0;
    scalarFunction* _temFunc_q;

  public:
    saturatePowerViscosityLaw(const int num, const double eta0, const double q, const bool init = true);
    void setTemperatureFunction_eta0(const scalarFunction& Tfunc);
    void setTemperatureFunction_q(const scalarFunction& Tfunc);
    #ifndef SWIG
    saturatePowerViscosityLaw(const saturatePowerViscosityLaw& src);
    virtual saturatePowerViscosityLaw& operator = (const viscosityLaw& src);
    virtual ~saturatePowerViscosityLaw();
    virtual void get(const double p, double& R, double& dR) const;
    virtual void get(const double p, const double T, double& R, double& dR, double& dRdT) const; 
    virtual void get(const double p, const double T, double& R, double& dR, double& dRdT, double& ddRdTT) const; 
    virtual void get(const double p, const double T, double& R, double& dR, double& dRdT, double& ddRdTT, double& ddRdT) const; 
    virtual viscosityLaw* clone() const {return new saturatePowerViscosityLaw(*this);};
    #endif // SWIG
};



/*eta = eta_1+ (_eta0-_eta1)*(p/g0)/sinh(p/g0) */
class saturateSinhViscosityLaw : public viscosityLaw{
  protected:
    double _eta0, _eta1;
    double _g0;
    scalarFunction* _temFunc_eta0;
    scalarFunction* _temFunc_g0;
    scalarFunction* _temFunc_eta1;

  public:
    saturateSinhViscosityLaw(const int num, const double eta0, const double eta1, const double g0, const bool init = true);
    void setTemperatureFunction_eta0(const scalarFunction& Tfunc);
    void setTemperatureFunction_eta1(const scalarFunction& Tfunc);
    void setTemperatureFunction_g0(const scalarFunction& Tfunc);
    #ifndef SWIG
    saturateSinhViscosityLaw(const saturateSinhViscosityLaw& src);
    virtual saturateSinhViscosityLaw& operator = (const viscosityLaw& src);
    virtual ~saturateSinhViscosityLaw();
    virtual void get(const double p, double& R, double& dR) const;
    virtual void get(const double p, const double T, double& R, double& dR, double& dRdT) const; 
    virtual void get(const double p, const double T, double& R, double& dR, double& dRdT, double& ddRdTT) const; 
    virtual void get(const double p, const double T, double& R, double& dR, double& dRdT, double& ddRdTT, double& ddRdT) const; 
    virtual viscosityLaw* clone() const {return new saturateSinhViscosityLaw(*this);};
    #endif // SWIG
};

#endif // VISCOSITYLAW_H_
