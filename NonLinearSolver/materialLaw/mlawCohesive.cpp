//
// Description: Define material law for cohesive layer
// the cohesive traction is computed form cohesive jump
// since the cohesive law is intitialized at insertion time,
// the parameters are locally given in IPCohesive
//
// Author:  <V.D. Nguyen>, (C) 2015
//
// Copyright: See COPYING file that comes with this distribution
//
//

#include "mlawCohesive.h"

CohesiveLawBase& CohesiveLawBase::operator=(const materialLaw &source)
{
  materialLaw::operator=(source);
  return *this;
}

generalElasticDamageCohesiveLaw::generalElasticDamageCohesiveLaw(const int num, const cohesiveElasticPotential& elP, const cohesiveDamageLaw& dam):CohesiveLawBase(num,true){
  _elasticPotential  =elP.clone();
  _damageLaw = dam.clone();
};
generalElasticDamageCohesiveLaw::generalElasticDamageCohesiveLaw(const generalElasticDamageCohesiveLaw& src): CohesiveLawBase(src){
  _elasticPotential = NULL;
  if (src._elasticPotential != NULL){
    _elasticPotential = src._elasticPotential->clone();
  }

  _damageLaw = NULL;
  if (src._damageLaw != NULL){
    _damageLaw = src._damageLaw->clone();
  }
}

generalElasticDamageCohesiveLaw& generalElasticDamageCohesiveLaw::operator=(const materialLaw &source){
  CohesiveLawBase::operator=(source);
	const generalElasticDamageCohesiveLaw* psrc = dynamic_cast<const generalElasticDamageCohesiveLaw*>(&source);
	if (psrc !=NULL){
    if (_elasticPotential) {delete _elasticPotential; _elasticPotential = NULL;}
    if (psrc->_elasticPotential != NULL){
      _elasticPotential = psrc->_elasticPotential->clone();
    }
		if (_damageLaw) {delete _damageLaw; _damageLaw = NULL;};
		if (psrc->_damageLaw != NULL){
			_damageLaw = psrc->_damageLaw->clone();
		}
	}
  return *this;
};

generalElasticDamageCohesiveLaw::~generalElasticDamageCohesiveLaw(){
  if (_elasticPotential) delete _elasticPotential;
  if (_damageLaw) delete _damageLaw;
};

void generalElasticDamageCohesiveLaw::createIPVariable(IPCohesive* &ipv) const
{
  if (ipv != NULL) delete ipv;
  if (_damageLaw->getType() == cohesiveDamageLaw::Zero)
  {
    ipv = new IPBiLinearCohesive();
  }
  else if (_damageLaw->getType() == cohesiveDamageLaw::Bilinear)
  {
    ipv = new IPBiLinearCohesive();
  }
  else if (_damageLaw->getType() == cohesiveDamageLaw::Exponential)
  {
    const ExponentialCohesiveDamageLaw* damLaw =dynamic_cast<const ExponentialCohesiveDamageLaw*>(_damageLaw);
    ipv = new IPExponentialCohesive(damLaw->getPowerParameter());
  }
  else
  {
    Msg::Error("missing case in generalElasticDamageCohesiveLaw::createIPVariable");
    Msg::Exit(0);
  }
};

void generalElasticDamageCohesiveLaw::constitutive(const SVector3& jump, const SVector3& normDir,
                              const IPCohesive* q0, IPCohesive* q, SVector3& T, const bool stiff, STensor3& DTDjump) const{

  // take the same internal state as previous
  q->operator=(*dynamic_cast<const IPVariable*>(q0));

  // estimation of current kinematic variables
	// normal part
	double deltan = dot(jump,normDir);
	if (deltan>=0){
		q->getRefToTensionFlag() = true;
	}
	else{
		q->getRefToTensionFlag() = false;
	}

  double beta = q->getMixedModeParameter();
  double Kp = q->getCompressivePenality();
  double Kn = q->getNormalPenality();
  

  double energyPos = _elasticPotential->getPositiveValue(jump,normDir,Kn,beta);
  double energyNeg = _elasticPotential->getNegativeValue(jump,normDir,Kp,beta);

  static SVector3 TPos, TNeg;
  static STensor3 dTPosDjump, dTNegDjump;

  _elasticPotential->constitutivePositive(jump,normDir,Kn,beta,TPos,stiff,&dTPosDjump);
  _elasticPotential->constitutiveNegative(jump,normDir,Kp,beta,TNeg,stiff,&dTNegDjump);

  double delta(0.);
  static SVector3 DdeltaDjump;
  _elasticPotential->getEffectiveJump(jump,normDir,beta,delta,stiff,&DdeltaDjump);

  // compute maximal effective opening
	double& deltamax = q->getRefToMaximalEffectiveJump();
	const double& deltamaxPrev = q0->getConstRefToMaximalEffectiveJump();
	deltamax = std::max(deltamaxPrev,delta);

  // compute damage
	double delta0 = q->getEffectiveJumpDamageOnset();
  double deltac = q->getCriticalEffectiveJump();
  double sigmac = q->getCriticalEffectiveStress();

	double& D = q->getRefToCohesiveDamage();
	const double& Dprev = q0->getConstRefToCohesiveDamage();
	double& dDDdelta = q->getRefToDCohesiveDamageDEffectiveJump();

	D = Dprev;
	dDDdelta = 0.;

	_damageLaw->computeDamage(delta0,deltac,deltamaxPrev,deltamax,q0->getConstRefToIPCohesiveDamage(),*(q->getRefToIPCohesiveDamage()), stiff);

	  // parameters need to estimate for current step
  double& sigmamax = q->getRefToMaximalEffectiveStress();
	sigmamax = (1-D)*Kp*deltamax;

	if (D > 0.9999999999){
		q->Delete(true);
	}
	else{
		q->Delete(false);
	}

  T = TPos;
  T *= (1.-D);
  T += TNeg;

  double& fracEnergy  = q->getRefToFractureEnergy();
  fracEnergy +=  0.5*deltac*(q0->getConstRefToMaximalEffectiveStress() - sigmamax);

  double revEnerg = q->getRefToReversibleEnergy();
  revEnerg = (1.-D)*energyPos+energyNeg;

  double & irreEnerg  = q->getRefToIrreversibleEnergy();
  irreEnerg = q0->irreversibleEnergy();
	irreEnerg +=  energyPos*(D-Dprev);

  if (stiff){
    DTDjump = dTPosDjump;
    DTDjump*= (1.-D);
    DTDjump += dTNegDjump;
    if (fabs(dDDdelta) > 0.)
    {
      for(int i =0; i<3; i++){
        for(int j=0; j<3; j++){
          DTDjump(i,j) -= TPos(i)*dDDdelta*DdeltaDjump(j);
        }
      }
    }

		SVector3& dirreEnergDjump = q->getRefToDIrreversibleEnergyDJump();
		STensorOperation::zero(dirreEnergDjump);
    if (fabs(dDDdelta) > 0.)
    {
      for (int i=0; i<3; i++){
        dirreEnergDjump(i) += (TPos(i)*(D-Dprev) + energyPos*dDDdelta*DdeltaDjump(i));
      };
    }
  }
};


BiLinearCohesiveLaw& BiLinearCohesiveLaw::operator=(const materialLaw &source)
{
  CohesiveLawBase::operator=(source);
	const BiLinearCohesiveLaw* psrc = dynamic_cast<const BiLinearCohesiveLaw*>(&source);
	if (psrc !=NULL){
		if (_damageLaw) {delete _damageLaw; _damageLaw = NULL;};
		if (psrc->_damageLaw != NULL){
			_damageLaw = psrc->_damageLaw->clone();
		}
	}
  return *this;
}

void BiLinearCohesiveLaw::createIPVariable(IPCohesive* &ipv) const
{
  if (ipv != NULL) delete ipv;
  ipv = new IPBiLinearCohesive();
}


void BiLinearCohesiveLaw::createIPState(IPStateBase* &ips, bool hasBodyForce, const bool* state_,const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
  IPVariable* ipvi = new IPBiLinearCohesive();
  IPVariable* ipv1 = new IPBiLinearCohesive();
  IPVariable* ipv2 = new IPBiLinearCohesive();
  if(ips != NULL) delete ips;
  ips = new IP3State(state_,ipvi,ipv1,ipv2);
}



void BiLinearCohesiveLaw::constitutive(const SVector3& jump, const SVector3& normDir,
                              const IPCohesive* q0, IPCohesive* q, SVector3& T, const bool stiff, STensor3& DTDjump) const{

        // take the same internal state as previous
        q->operator=(*dynamic_cast<const IPVariable*>(q0));

	// estimation of current kinematic variables
	// normal part
	double deltan = dot(jump,normDir);
	double alp = 0.; // tension, compression index
	if (deltan>=0){
		q->getRefToTensionFlag() = true;
		alp = 1.;
	}
	else{
		q->getRefToTensionFlag() = false;
		alp = 0.;
	}

	// tangential part
	static SVector3 deltat;
  for (int i=0; i<3; i++){
    deltat(i) = jump(i) - deltan*normDir(i);
	}
	double deltatSq = dot(deltat,deltat);

  double beta = q->getMixedModeParameter();
  double betaSq = beta*beta;

        // estimation of the effective cohesive opening
	double delta = sqrt(alp*deltan*deltan + betaSq*deltatSq);

	// compute maximal effective opening
	double& deltamax = q->getRefToMaximalEffectiveJump();
	const double& deltamaxPrev = q0->getConstRefToMaximalEffectiveJump();
	deltamax = std::max(deltamaxPrev,delta);

	// compute damage
	double delta0 = q->getEffectiveJumpDamageOnset();
  double Kpe = q->getCompressivePenality();
  double Kn = q->getNormalPenality();
  double deltac = q->getCriticalEffectiveJump();
  double sigmac = q->getCriticalEffectiveStress();

	double& D = q->getRefToCohesiveDamage();
	const double& Dprev = q0->getConstRefToCohesiveDamage();
	double& dDDdelta = q->getRefToDCohesiveDamageDEffectiveJump();

	D = Dprev;
	dDDdelta = 0.;
	_damageLaw->computeDamage(delta0,deltac,deltamaxPrev,deltamax,q0->getConstRefToIPCohesiveDamage(),*(q->getRefToIPCohesiveDamage()), stiff);

  // parameters need to estimate for current step
  double& sigmamax = q->getRefToMaximalEffectiveStress();
	sigmamax = (1-D)*Kn*deltamax;

	if (D > 0.9999999999){
		q->Delete(true);
	}
	else{
		q->Delete(false);
	}

	// cohesive traction
	// normal part
	STensorOperation::zero(T);
  for (int i=0; i<3; i++){
    // normal part
    if (q0->ifTension()){
      T(i) += (1.-D)*Kn*deltan*normDir(i); // traction case
    }
    else{
      T(i) += Kpe*deltan*normDir(i); // compression case
    }
    // tangent part
    T(i) += betaSq*(1.-D)*Kn*deltat(i);
  }

  double& fracEnergy  = q->getRefToFractureEnergy();
  fracEnergy +=  0.5*deltac*(q0->getConstRefToMaximalEffectiveStress() - sigmamax);
	double& reEnerg = q->getRefToReversibleEnergy();
	reEnerg = Kn*delta*delta/2.;
	double & irreEnerg  = q->getRefToIrreversibleEnergy();
  irreEnerg = q0->irreversibleEnergy();
	irreEnerg += reEnerg*(D-Dprev);

  if (stiff){
    STensorOperation::zero(DTDjump);
    // derivative in respect to jump
    static STensor3 DdeltatDjump;
    STensorOperation::diag(DdeltatDjump,1.);

    for(int i =0; i<3; i++){
      for(int j=0; j<3; j++){
        DdeltatDjump(i,j) -= normDir(i)*normDir(j);
      }
    }

    for (int i=0; i<3; i++){
      for (int j=0; j<3; j++){
        // derivative DnormalDjump
        if (q0->ifTension()){
          DTDjump(i,j) += (1.-D)*Kn*normDir(i)*normDir(j);
        }
        else{
          DTDjump(i,j) +=  Kpe*normDir(i)*normDir(j);
        }
        // derivative DtangentEffJumpDjump
        DTDjump(i,j) += betaSq*(1.-D)*Kn*DdeltatDjump(i,j);
      }
    }

		SVector3& dirreEnergDjump = q->getRefToDIrreversibleEnergyDJump();
		STensorOperation::zero(dirreEnergDjump);

    // derivative in resepect to damage
    if (dDDdelta > 0){
      static SVector3 dTdD;
      STensorOperation::zero(dTdD);

      for (int i=0; i<3; i++){
        dTdD(i)  -= betaSq*Kn*deltat(i);
        if (q0->ifTension()){
          dTdD(i) -= Kn*deltan*normDir(i);
        }
      }

      static SVector3 DdeltaDjump;
      STensorOperation::zero(DdeltaDjump);
      for (int i=0; i<3; i++){
        if (q0->ifTension()){
          DdeltaDjump(i) += (deltan/delta)*normDir(i);
        }
        for (int j=0; j<3; j++){
          DdeltaDjump(i) += (betaSq/delta)*deltat(j)*DdeltatDjump(j,i);
        }
      }

      // estimatted
      for (int i=0; i<3; i++){
        for (int j=0; j<3; j++){
          DTDjump(i,j) += dTdD(i)*dDDdelta*DdeltaDjump(j);
        }
      }

			double DirreEnergDdelta = Kn*delta*(D-Dprev) + reEnerg*dDDdelta;
			for (int i=0; i<3; i++){
				dirreEnergDjump(i) = DirreEnergDdelta*DdeltaDjump(i);
			}
    }
  }

};


TransverseIsotropicCohesiveLaw::TransverseIsotropicCohesiveLaw(const double Ax, const double Ay, const double Az, const int num, const bool init):BiLinearCohesiveLaw(num,init), _A(Ax,Ay,Az)
{
  if(_A.norm()!=0.) _A.normalize();
}
TransverseIsotropicCohesiveLaw::TransverseIsotropicCohesiveLaw(const TransverseIsotropicCohesiveLaw& src): BiLinearCohesiveLaw(src), _A(src._A)
{

}
TransverseIsotropicCohesiveLaw& TransverseIsotropicCohesiveLaw::operator=(const materialLaw &source)
{
  BiLinearCohesiveLaw::operator=(source);
  const TransverseIsotropicCohesiveLaw* src =static_cast<const TransverseIsotropicCohesiveLaw*>(&source);
  if(src!=NULL)
  {
    _A=src->_A;
  }
  return *this;
}

void TransverseIsotropicCohesiveLaw::createIPVariable(IPCohesive* &ipv) const
{
  if (ipv != NULL) delete ipv;
  ipv = new IPTransverseIsotropicCohesive();
}

void TransverseIsotropicCohesiveLaw::createIPState(IPStateBase* &ips, bool hasBodyForce, const bool* state_,const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
  IPVariable* ipvi = new IPTransverseIsotropicCohesive();
  IPVariable* ipv1 = new IPTransverseIsotropicCohesive();
  IPVariable* ipv2 = new IPTransverseIsotropicCohesive();
  if(ips != NULL) delete ips;
  ips = new IP3State(state_,ipvi,ipv1,ipv2);
}


TransverseIsoCurvatureCohesiveLaw::TransverseIsoCurvatureCohesiveLaw(const double Centroid_x, const double Centroid_y, const double Centroid_z, const double PointStart1_x, const double PointStart1_y, const double PointStart1_z,
                                      const double PointStart2_x, const double PointStart2_y, const double PointStart2_z,const double init_Angle,
                                      const int num, const bool init):TransverseIsotropicCohesiveLaw(0.,0.,0.,num,init),
                                             _Centroid(Centroid_x, Centroid_y, Centroid_z), _PointStart1(PointStart1_x, PointStart1_y, PointStart1_z),
                                             _PointStart2(PointStart2_x, PointStart2_y, PointStart2_z),_init_Angle(init_Angle)
{

}
TransverseIsoCurvatureCohesiveLaw::TransverseIsoCurvatureCohesiveLaw(const TransverseIsoCurvatureCohesiveLaw& src): TransverseIsotropicCohesiveLaw(src),
                                           _Centroid(src._Centroid), _PointStart1(src._PointStart1), _PointStart2(src._PointStart2),_init_Angle(src._init_Angle)
{


}
TransverseIsoCurvatureCohesiveLaw& TransverseIsoCurvatureCohesiveLaw::operator=(const materialLaw &source)
{
  TransverseIsotropicCohesiveLaw::operator=(source);
  const TransverseIsoCurvatureCohesiveLaw* src =static_cast<const TransverseIsoCurvatureCohesiveLaw*>(&source);
  if(src!=NULL)
  {
     _Centroid=src->_Centroid;
     _PointStart1=src->_PointStart1;
     _PointStart2=src->_PointStart2;
     _init_Angle=src->_init_Angle;
  }
  return *this;
}

void TransverseIsoCurvatureCohesiveLaw::createIPVariable(IPCohesive* &ipv) const
{

};

void TransverseIsoCurvatureCohesiveLaw::createIPState(IPStateBase* &ips, bool hasBodyForce, const bool* state_,const MElement *ele, const int nbFF, const IntPt *GP, const int gpt) const
{


  SPoint3 pt_Global;
  ele->pnt(GP->pt[0],GP->pt[1],GP->pt[2],pt_Global);
  SVector3 GaussP(pt_Global);

  IPVariable* ipvi = new IPTransverseIsoCurvatureCohesive(GaussP, _Centroid, _PointStart1, _PointStart2, _init_Angle);
  IPVariable* ipv1 = new IPTransverseIsoCurvatureCohesive(GaussP, _Centroid, _PointStart1, _PointStart2, _init_Angle);
  IPVariable* ipv2 = new IPTransverseIsoCurvatureCohesive(GaussP, _Centroid, _PointStart1, _PointStart2, _init_Angle);
  if(ips != NULL) delete ips;
  ips = new IP3State(state_,ipvi,ipv1,ipv2);

}


