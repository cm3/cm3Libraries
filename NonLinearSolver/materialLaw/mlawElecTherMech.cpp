//
// C++ Interface: material law
//
// Description: LinearThermoMechanics law
//
//
// Author:  <L. Homsy>, (C) 2013
//
// Copyright: See COPYING file that comes with this distribution
/
//
#include "mlawLinearElecTherMech.h"
#include <math.h>
#include "MInterfaceElement.h"
#include "mlawElecTherMech.h"
#include "mlawTransverseIsotropic.h"


mlawElecTherMech::mlawElecTherMech(const int num,const double rho,const double Ex, const double Ey, const double Ez, const double Vxy, const double Vxz, const double Vyz,
			 const double MUxy, const double MUxz, const double MUyz, const double alpha, const double beta, const double gamma,
			 const double cv,const double t0,const double Kx,const double Ky,const double Kz,const double alphax,const double alphay,
		          const double alphaz,const  double lx,const  double ly,const  double lz,const double seebeck,const double EA, const double GA, const double nu_minor,
                           const double Ax, const double Ay, const double Az):
				// mlawLinearElecTherMech( num, rho, Ex,  Ey,  Ez,  Vxy, Vxz,  Vyz, MUxy,MUxz,  MUyz, alpha, beta,  gamma,
				      //   cv, t0, Kx, Ky, Kz, alphax,alphay, alphaz,lx,ly,lz,seebeck) ,
				 mlawTransverseIsotropic(EA,GA,nu_minor,Ax,Ay,Az)
				 
{
	
	 //_lawLinearETM = new mlawLinearElecTherMech( num, rho, Ex,  Ey,  Ez,  Vxy, Vxz,  Vyz, MUxy,MUxz,  MUyz, alpha, beta,  gamma,
				  //       cv, t0, Kx, Ky, Kz, alphax,alphay, alphaz,lx,ly,lz,seebeck);
	//  double poissonRatio=_lawLinearETM->poissonRatio();
	
} 
mlawElecTherMech::mlawElecTherMech(const mlawElecTherMech &source) :mlawTransverseIsotropic (source) // mlawLinearThermoMechanics(source)


{
	_lawLinearETM = NULL;
	if (source._lawLinearETM){
		_lawLinearETM  = new mlawLinearElecTherMech(*(source._lawLinearETM));
	}
}

mlawElecTherMech& mlawElecTherMech::operator=(const materialLaw &source)
{
  mlawTransverseIsotropic::operator=(source);
  //mlawLinearElecTherMech::operator=(source);
  if (_lawLinearETM != NULL) delete _lawLinearETM;
  _lawLinearETM  = new mlawLinearElecTherMech(*(source._lawLinearETM));
  // ??
  const mlawElecTherMech* src =dynamic_cast<const mlawElecTherMech*>(&source);
  if(src !=NULL)
  {

 
 
  }  
  return *this;
}

void mlawElecTherMech::createIPState(IPStateBase* &ips, bool hasBodyForce, const bool* state_,const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
  bool inter=true;
  const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
  if(iele==NULL) inter=false;

  IPVariable* ipvi = new IPElecTherMech();
  IPVariable* ipv1 = new IPElecTherMech();
  IPVariable* ipv2 = new IPElecTherMech();
  if(ips != NULL) delete ips;
  ips = new IP3State(state_,ipvi,ipv1,ipv2);
}


void mlawLinearElecTherMech::constitutive(
                            const STensor3& F0,         // initial deformation gradient (input @ time n)
                            const STensor3& Fn,         // updated deformation gradient (input @ time n+1)
                            STensor3 &P,                // updated 1st Piola-Kirchhoff stress tensor (output)
                                                        // contains the initial values on input
                            const IPElecTherMech *q0,       // array of initial internal variable
                            IPElecTherMech *q1,             // updated array of internal variable (in ipvcur on output),
                            STensor43 &Tangent,         // constitutive tangents (output)
                            double T,
			    double V,
  			    const SVector3 &gradT,
			    const SVector3 &gradV,
                            SVector3 &fluxT,
			    SVector3 &fluxV,
                            STensor3 &dPdT,
                            STensor3 &dPdV,
                            STensor3 &dqdgradT,
                            SVector3 &dqdT,
			    STensor3 &dqdgradV,
                            SVector3 &dqdV,
			    SVector3 &dedV,
			    STensor3 &dedgradV,
			    SVector3 &dedT,
			    STensor3 &dedgradT,
                            STensor33 &dqdF,
			    STensor33 &dedF,
                            const bool stiff  
                           )const
{
    mlawLinearElecTherMech::constitutive(F0,Fn, P, (const IPElecTherMech *) q0,  (IPElecTherMech *)q1,Tangent,stiff);        

    double dseebeckdT=0.;
    static STensor3 dLdT,dKdT;//,k1,k2,l10,l20;
    
    STensorOperation::zero(dLdT);
    STensorOperation::zero(dKdT); 
    STensorOperation::zero(dseebeckdT);
    STensorOperation::zero(dedV);
    STensorOperation::zero(dqdV);      
    STensorOperation::zero(dqdF); // dependency of flux with deformation gradient
    STensorOperation::zero(dedF);
    STensorOperation::zero(dPdT);
    STensorOperation::zero(dPdV);
        

   
   //large defo
   static STensor3 Fninv;
   static STensor3 _Kref10,_Kref20,_Lref10,_Lref20,_Lref0;
   STensorOperation::inverseSTensor3(Fn, Fninv);
   double J= Fn.determinant(); 
   STensorOperation::zero(_Kref20);
   STensorOperation::zero(_Kref10);
   for(int K = 0; K< 3; K++)
   {
     for(int L = 0; L< 3; L++)
     {
       for(int i = 0; i< 3; i++)
       {
         for(int j = 0; j< 3; j++)
         {
           _Kref10(K,L) += Fninv(K,i)*_k10(i,j)*Fninv(L,j)*J; //put in reference configuration
           _Kref20(K,L) += Fninv(K,i)*_k20(i,j)*Fninv(L,j)*J;
           _Lref10(K,L) += Fninv(K,i)*_l10(i,j)*Fninv(L,j)*J; //put in reference configuration
           _Lref20(K,L) += Fninv(K,i)*_l20(i,j)*Fninv(L,j)*J;
	  
	   _Lref0(K,L) += Fninv(K,i)*_l0(i,j)*Fninv(L,j)*J;
         }
       }
     }
   } 
  
    _l10 = _Lref10;
    _l20 = _Lref20;
    _k10 = _Kref10;
    _k20 = _Kref20;
    _l0=_Lref0;
    
    
    STensorOperation::zero(dqdF);
    for(int K = 0; K< 3; K++)
    {
      for(int m = 0; m< 3; m++)
      {
        for(int N = 0; N< 3; N++)
        {
          for(int L = 0; L< 3; L++)
          {
            dqdF(K,m,N) -= Fninv(K,m)*_Kref10(N,L)*gradT(L);
            dqdF(K,m,N) -= _Kref10(K,N)*Fninv(L,m)*gradT(L);
            dqdF(K,m,N) += _Kref10(K,L)*gradT(L)*Fninv(N,m);
	    dqdF(K,m,N) -= Fninv(K,m)*_Kref20(N,L)*gradT(L);
            dqdF(K,m,N) -= _Kref20(K,N)*Fninv(L,m)*gradT(L);
            dqdF(K,m,N) += _Kref20(K,L)*gradT(L)*Fninv(N,m);
          }
        }
      }
    }
       for(int K = 0; K< 3; K++)
    {
      for(int m = 0; m< 3; m++)
      {
        for(int N = 0; N< 3; N++)
        {
          for(int L = 0; L< 3; L++)
          {
            dedF(K,m,N) -= Fninv(K,m)*_Lref10(N,L)*gradT(L);
            dedF(K,m,N) -= _Lref10(K,N)*Fninv(L,m)*gradT(L);
            dedF(K,m,N) += _Lref10(K,L)*gradT(L)*Fninv(N,m);
	    dedF(K,m,N) -= Fninv(K,m)*_Lref20(N,L)*gradT(L);
            dedF(K,m,N) -= _Lref20(K,N)*Fninv(L,m)*gradT(L);
            dedF(K,m,N) += _Lref20(K,L)*gradT(L)*Fninv(N,m);
          }
        }
      }
    }
        
  
    
    for(int i=0;i<3;i++)
     {
       fluxT(i)=0.;
      for(int j=0;j<3;j++)
      {
	 fluxT(i)+=_Kref10(i,j)*gradT(j)+_Kref20(i,j)*gradV(j);
       }
     }
       
       
    for(int i=0;i<3;i++)
    {
      fluxV(i)=0.;
      for(int j=0;j<3;j++)
      {
         fluxV(i)+=_Lref10(i,j)*gradV(j)+_Lref20(i,j)*gradT(j);
      }
    } 
   
      
     // dedT=0.;
    for(int i=0;i<3;i++)
    {
      dedT(i)=0.;
      for(int j=0;j<3;j++)
      {
         dedT(i)+=dseebeckdT*_Lref0(i,j)*gradT(j)+_seebeck*dLdT(i,j)*gradT(j)+dLdT(i,j)*gradV(j);
       }
    }
    dedgradT=_Lref0;
    dedgradT*=_seebeck;

    dedgradV =_Lref0;
    
    for(int i=0;i<3;i++)
    {
      dqdT(i)=0.;
      for(int j=0;j<3;j++)
      {
         dqdT(i)+=dKdT(i,j)*gradT(j)+2*_seebeck*dseebeckdT*_Lref0(i,j)*T*gradT(j)+_seebeck*_seebeck*dLdT(i,j)*T*gradT(j)+_seebeck*_seebeck*_Lref0(i,j)*gradT(j)
	 +dseebeckdT*_Lref0(i,j)*T*gradV(j)+_seebeck*dLdT(i,j)*T*gradV(j)+_seebeck*_Lref0(i,j)*gradV(j);
       }
    }
    
    for(int i=0;i<3;i++)
    {
      for(int k=0;k<3;k++)
      {
	dqdgradT(i,k)=_Kref10(i,k);
	//dqdgradT(i,k)+=_seebeck*_seebeck*_Lref0(i,k)*T;
      }
    }
        
   
    dqdgradV=_Lref0*(T*_seebeck);
    

 }
 
 

 
  

  





