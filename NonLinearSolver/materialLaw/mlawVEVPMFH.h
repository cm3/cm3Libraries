/*********************************************************************************************/
/** Copyright (C) 2022 - See COPYING file that comes with this distribution	                **/
/** Author: Mohamed HADDAD (MEMA-iMMC-UCLouvain)							                              **/
/** Contact: mohamed.haddad@uclouvain.be					                                      		**/
/**											                                                                  	**/
/** C++ Interface: material law                                                             **/
/** Description: ViscoElastic-ViscoPlastic material law (small strains)             				**/
/** See B.Miled et I.Doghri 2011                                                            **/
/**********************************************************************************************/


#ifndef MLAWVEVPMFH_H_
#define MLAWVEVPMFH_H_
#include "mlaw.h"
#include "STensor3.h"
#include "STensor43.h"

#include "ipVEVPMFH.h"
#include "UsefulFunctionsVEVPMFH.h"

class mlawVEVPMFH : public materialLaw{
  
  protected:
    double _rho;                                              // maximal mu for anisotropic
    VEVPMFHSPACE::MATPROP matrprop;                           // structures to store matrix & inclusions VE-VP properties
    VEVPMFHSPACE::MATPROP inclprop;
    const char* inputfilename;                                // input file
  
  public:
    mlawVEVPMFH(const int num, const double rho, const char *propName);

  #ifndef SWIG
  mlawVEVPMFH(const mlawVEVPMFH &source);
  mlawVEVPMFH& operator=(const materialLaw &source);
  virtual ~mlawVEVPMFH();
  virtual materialLaw* clone() const {return new mlawVEVPMFH(*this);};
  virtual void checkInternalState(IPVariable* ipv, const IPVariable* ipvprev) const{}; // do nothing

  virtual matname getType() const{return materialLaw::vevpmfh;}       // function of materialLaw

  virtual void createIPState(IPStateBase* &ips, bool hasBodyForce, const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const IntPt *GP=NULL, const int gpt = 0) const;

  virtual bool withEnergyDissipation() const {return true;};
  virtual void initLaws(const std::map<int,materialLaw*> &maplaw){}; 	// this law is initialized so nothing to do
  virtual double soundSpeed() const; 	
  virtual const double YoungModulus() const;					                      // default but you can redefine it for your case
  virtual const double bulkModulus() const;
  virtual const double shearModulus() const;
  virtual const double poissonRatio() const;

  // specific function
  public:
  virtual void constitutive(
                            const STensor3& F0,                     // initial deformation gradient (input @ time n)
                            const STensor3& Fn,                     // updated deformation gradient (input @ time n+1)
                            STensor3 &P,                            // updated 1st Piola-Kirchhoff stress tensor (output)
                                                                    // contains the initial values on input
                            const IPVariable *q0,                   // array of initial internal variable
                            IPVariable *q1,                         // updated array of internal variable (in ipvcur on output),
                            STensor43 &Tangent,                     // constitutive tangents (output)
                            const bool stiff,                       // if true compute the tangents
                            STensor43* elasticTangent = NULL, 
                            const bool dTangent =false,
                            STensor63* dCalgdeps = NULL) const;
  
  #endif // SWIG
};

#endif 
