//
// C++ Interface: material law
//              Linear Thermo Mechanical law
// Author:  <L. Homsy>, (C) 2013
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef MLAWLINEARELECTHERMECH_H_
#define MLAWLINEARELECTHERMECH_H_
#include "STensor3.h"
#include "STensor43.h"

#include "mlawLinearThermoMechanics.h"
#include "ipLinearElecTherMech.h"


  
class mlawLinearElecTherMech : public mlawLinearThermoMechanics {
 protected:
  // can't be const due to operator= constructor (HOW TO CHANGE THIS ??)
  double _lx;
  double _ly;
  double _lz; 
  double _v0;
  
  STensor3 _l0;
 /* STensor3 _k10;
  STensor3 _k20;
  STensor3 _l10;
  STensor3 _l20;*/

  // function of materialLaw
  double _seebeck;
  //STensor43 K_;//fill elastic stiffenss
  //STensor3 _Stiff_alphaDilatation;
  

#ifndef SWIG
 private: 
   
#endif // SWIG
 public:
  mlawLinearElecTherMech(int num,const double rho, const double Ex, const double Ey, const double Ez, const double Vxy, const double Vxz, const double Vyz,
	  const double MUxy, const double MUxz, const double MUyz,  const double alpha, const double beta, const double gamma  ,
         const double t0,const double Kx,const double Ky, const double Kz,const double alphax,const double alphay,const double alphaz,
	 const  double lx,const  double ly,const  double lz,const double seebeck,const double cp,const double v0);
   #ifndef SWIG
	mlawLinearElecTherMech(const mlawLinearElecTherMech &source);
	mlawLinearElecTherMech& operator=(const  materialLaw &source);
	virtual ~mlawLinearElecTherMech(){}
	virtual materialLaw* clone() const {return new mlawLinearElecTherMech(*this);};
  virtual bool withEnergyDissipation() const {return false;};
  // function of materialLaw
  virtual matname getType() const{return materialLaw::LinearElecTherMech;}  
  virtual void createIPState(IPStateBase* &ips, bool hasBodyForce, const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const IntPt *GP=NULL, const int gpt =0) const;
 
public:
   virtual void constitutive(
                            const STensor3& F0,         // initial deformation gradient (input @ time n)
                            const STensor3& Fn,         // updated deformation gradient (input @ time n+1)
                            STensor3 &P,                // updated 1st Piola-Kirchhoff stress tensor (output)
                                                        // contains the initial values on input
                            const IPLinearElecTherMech *q0,       // array of initial internal variable
                            IPLinearElecTherMech *q1,             // updated array of internal variable (in ipvcur on output),
                            STensor43 &Tangent,         // constitutive tangents (output)
                            double T0,
			    double T,
			    double V0,			
			    double V,
  			    const SVector3 &gradT,
			    const SVector3 &gradV,
                            SVector3 &fluxT,
			    SVector3 &fluxV,
                            STensor3 &dPdT,
                            STensor3 &dPdV,
                            STensor3 &dqdgradT,
                            SVector3 &dqdT,
			    STensor3 &dqdgradV,
                            SVector3 &dqdV,
			    SVector3 &dedV,
			    STensor3 &dedgradV,
			    SVector3 &dedT,
			    STensor3 &dedgradT,
                            STensor33 &dqdF,
			    STensor33 &dedF,
			    double &w,
			    double &dwdt,
			    STensor3 &dwdf,
			    STensor3 &l10,
			    STensor3 &l20,
			    STensor3 &k10,
			    STensor3 &k20,
			    STensor3 &dl10dT,
			    STensor3 &dl20dT,
			    STensor3 &dk10dT,
			    STensor3 &dK20dT,
			    STensor3 &dl10dv,
			    STensor3 &dl20dv,
			    STensor3 &dk10dv,
			    STensor3 &dk20dv,
			    STensor43 &dl10dF,
			    STensor43 &dl20dF,
			    STensor43 &dk10dF,
			    STensor43 &dk20dF,
			    SVector3 &fluxjy,
			    SVector3 &djydV,
			    STensor3 &djydgradV,
			    SVector3 &djydT,
			    STensor3 &djydgradT,
                            STensor33 &djydF,
			    STensor3 &djydgradVdT,
			    STensor3 &djydgradVdV,
			    STensor43 &djydgradVdF,
			    STensor3 &djydgradTdT,
			    STensor3 &djydgradTdV,
			    STensor43 &djydgradTdF,
                            const bool stiff,
                            const bool evaluateCurlField = true
                           ) const;
    double getInitialTemperature() const {return _t0;};
    double getInitialVoltage()     const {return _v0;};
  //  virtual const STensor3& getStiff_alphaDilatation()const { return _Stiff_alphaDilatation;};
  /*  const STensor3&  getInitialElecConductivityTensor() const { return _l10;};
    const STensor3&  getInitialElecCrossConductivityTensor() const { return _l20;};
    const STensor3&  getInitialConductivityTensor() const { return _k10;};
    const STensor3&  getInitialCrossConductivityTensor() const { return _k20; };*/
    
 protected:
   virtual double deformationEnergy(STensor3 defo,const SVector3 &gradV,const SVector3 &gradT,const double T, const double V) const ;
  //virtual double deformationEnergy(STensor3 defo) const ;
  //virtual double deformationEnergy(STensor3 defo, const STensor3& sigma) const ;

 #endif // SWIG
};

#endif // MLAWELECTHERMOMECH_H_
