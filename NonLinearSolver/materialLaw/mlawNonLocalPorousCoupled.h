//
// C++ Interface: material law
//
// Description: Porous material law with Gurson void growth and coalescence
//
// Author:  <J. Leclerc,  V-D Nguyen>, (C) 2017
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef MLAWNONLOCALPOROUSCOUPLED_H_
#define MLAWNONLOCALPOROUSCOUPLED_H_
#include "mlawNonLocalPorous.h"
#include "mlawNonLocalDamageGurson.h"
#include "mlawNonLocalPorousCoalescence.h"
#include "ipNonLocalPorosity.h"


class mlawNonLocalPorousCoupledLaw : public combinedPorousLaw
{
#ifndef SWIG
  protected:    
    bool _useTwoYieldRegularization; // true if power form made from yield surfaces are used
    double _yieldRegExponent; 
    
    // Included material law
    mlawNonLocalDamageGurson *_mlawGrowth;
    mlawNonLocalPorousThomasonLaw *_mlawCoales;
		double _coalesTol; // coalescence tolerane if using regulized multi-surface    
    // 
    bool _checkCoalescenceWithNormal; // if normal is used
    double _beta; // mode mixed 
    bool _withLodeOffset; // true if lode offset is used
    bool _withCftOffset; // true if CftOffset is used
    bool _withYieldOffset; // true if yield offset if used
    
    // 
    bool _withBothYieldSurfacesInBulk; // true if two yield surfaces are verified in bulk element
  
  public:
    // Default constructor
    mlawNonLocalPorousCoupledLaw(const int num,const double E,const double nu, const double rho,
                        const double q1, const double q2, const double q3,
                        const double fVinitial, const double lambda0, const J2IsotropicHardening &j2IH, const CLengthLaw &cLLaw,
                        const double tol=1.e-8, const bool matrixbyPerturbation = false, const double pert = 1e-8);
    mlawNonLocalPorousCoupledLaw(const int num,const double E,const double nu, const double rho,
                        const double q1, const double q2, const double q3,
                        const double fVinitial, const double lambda0, const double kappa, const J2IsotropicHardening &j2IH, const CLengthLaw &cLLaw,
                        const double tol=1.e-8, const bool matrixbyPerturbation = false, const double pert = 1e-8);

    // Option settings
    virtual void setVoidEvolutionLaw(const voidStateEvolutionLaw& voidtateLaw);
    virtual void setGrowthVoidEvolutionLaw(const voidStateEvolutionLaw& voidtateLaw);
    virtual void setCoalescenceVoidEvolutionLaw(const voidStateEvolutionLaw& voidtateLaw);
    virtual void setCoalescenceLaw(const CoalescenceLaw& added_coalsLaw); 
    
    virtual void useTwoYieldRegularization(const bool fl, const double n=50.);
    virtual void setYieldSurfaceExponent(const double newN);
    virtual void setCrackTransition(const bool fl, const double beta=0.);
    virtual void setLodeOffset(const bool fl);
    virtual void setYieldOffset(const bool fl);
    virtual void setCftOffset(const bool fl);
		virtual void setCoalescenceTolerance(const double t);
    
    virtual void setBulkCouplingBehaviour(const bool fl);
    //
  public:
    // Constructors & destructor
    mlawNonLocalPorousCoupledLaw(const mlawNonLocalPorousCoupledLaw &source);
    virtual ~mlawNonLocalPorousCoupledLaw();
    virtual materialLaw* clone() const {return new mlawNonLocalPorousCoupledLaw(*this);};
    
    // Function of materialLaw
    virtual matname getType() const {return materialLaw::nonLocalDamageGursonThomason;};
    virtual void createIPState(IPStateBase* &ips,bool hasBodyForce,const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const IntPt *GP=NULL, const int gpt = 0) const;
    virtual void createIPVariable(const double fInit, IPNonLocalPorosity* &ipv) const;
    
    virtual void voidCharacteristicsEvolution_NonLocalPorosity(const STensor3& kcor,
                                                const IPNonLocalPorosity *q0, IPNonLocalPorosity* q1,
                                                const double DeltaHatD, const double DeltaHatQ, const double DeltaHatP,
                                                double& DfVDDeltaHatD, double& DfVDDeltaHatQ, double& DfVDDeltaHatP,
                                                STensor3& DfVDkcorpr) const;
    virtual void voidCharacteristicsEvolutionLocal(const STensor3& kcor,
                                                const IPNonLocalPorosity *q0, IPNonLocalPorosity* q1,
                                                const double DeltaHatD, const double DeltaHatQ, const double DeltaHatP,
                                                const double DtildefVstarDDeltafV,
                                                double& DyieldfVDDeltafV,
                                                double& DDeltafVDDeltaHatD, double& DDeltafVDDeltaHatQ, double& DDeltafVDDeltaHatP,
                                                STensor3& DDeltafVDKcor) const;
    virtual void voidCharacteristicsEvolution_multipleNonLocalVariables(const STensor3& kcor,
                                                const IPNonLocalPorosity *q0, IPNonLocalPorosity* q1,
                                                std::vector<double>& DyieldfVDNonlocalVars,
                                                STensor3& DyieldfVDKcor, const bool blockVoidGrowth
                                                ) const;   
                                                
    virtual void voidStateGrowth(const double yieldfV0, const double yieldfV1, // yield porosity
                        const double DeltahatD, const double DeltahatQ, const double DeltahatP, // internal variables
                        const IPNonLocalPorosity *q0, IPNonLocalPorosity *q1) const;

    virtual double yieldFunction(const double kcorEq, const double pcor, const double R, const double yieldfV,
                                const IPNonLocalPorosity* q0, const IPNonLocalPorosity* q1, const double* T = NULL,
                                bool diff=false, fullVector<double>* grad = NULL,
                                bool withthermic = false, double* dfdT = NULL) const;
                                
    virtual void plasticFlow(double & Ns, double& Nv,
                              const double kcorEq, const double pcor, const double R,
                              const IPNonLocalPorosity* q0, const IPNonLocalPorosity* q1, const double* T = NULL,
                              bool diff=false, fullVector<double>* gradNs=NULL, // grad of Ns with respect to [kcorEq pcor R fV Chi W]
                              fullVector<double>* gradNv = NULL, // grad of Nv with respect to  [kcorEq pcor R fV Chi W]
                              bool withTher = false, double* dNsDT = NULL, double* dNvDT=NULL) const;
    
                                  
    virtual double getOnsetCriterion(const IPNonLocalPorosity* q1) const;
    virtual void checkCoalescence(IPNonLocalPorosity* q1, const IPNonLocalPorosity *q0, const double* T) const;
    virtual void forceCoalescence(IPNonLocalPorosity* q1, const IPNonLocalPorosity *q0, const double* T) const;
    virtual bool checkCrackCriterion(IPNonLocalPorosity* q1, const IPNonLocalPorosity *q0, const STensor3& Fcur, const SVector3& normal, double delayFactor, const double* T) const;

    virtual void checkFailed(IPNonLocalPorosity* q1, const IPNonLocalPorosity *q0) const;
  
    virtual const NucleationLaw* getNucleationLaw() const {return _mlawGrowth->getNucleationLaw();};
    virtual const voidStateEvolutionLaw* getVoidEvolutionLaw() const {Msg::Error("getVoidEvolutionLaw cannot be called!!!"); return NULL;};
    virtual const CoalescenceLaw* getCoalescenceLaw() const {return _mlawCoales->getCoalescenceLaw();};
    
                                        
    virtual void I1J2J3_voidCharacteristicsEvolution_NonLocal(const STensor3& sig,
                                                const IPNonLocalPorosity *q0, IPNonLocalPorosity* q1,
                                                bool stiff = false,
                                                std::vector<double>* Y = NULL,
                                                std::vector<STensor3>* DYDsig = NULL,
                                                std::vector<std::vector<double> >* DYDNonlocalVars = NULL
                                                ) const;
    virtual void I1J2J3_voidCharacteristicsEvolution_Local(const STensor3& sig, const STensor3& DeltaEp, const double DeltaHatP,
                                                const IPNonLocalPorosity *q0, IPNonLocalPorosity* q1,
                                                bool stiff = false,
                                                std::vector<double>* Y = NULL,
                                                std::vector<STensor3>* DYDsig = NULL,
                                                std::vector<STensor3>* DYDDeltaEp = NULL,
                                                std::vector<double>* DYDDeltaHatP = NULL
                                                ) const;                                                  
    virtual double I1J2J3_yieldFunction(const STensor3& kcor, const double R, const IPNonLocalPorosity* q0, const IPNonLocalPorosity* q1, const double* T = NULL,
                                        bool diff=false, STensor3* DFDkcor=NULL, double* DFDR=NULL, std::vector<double>* DFDY=NULL, 
                                        bool withThermic=false, double *DFDT=NULL) const;
    virtual void I1J2J3_getYieldNormal(STensor3& Np, const STensor3& kcor, const double R, const IPNonLocalPorosity* q0, const IPNonLocalPorosity* q1, const double* T = NULL,
                                        bool diff=false, STensor43* DNpDkcor=NULL, STensor3* DNpDR=NULL, std::vector<STensor3>* DNpDY=NULL,
                                        bool withThermic=false, STensor3* DNpDT=NULL  
                                        ) const;
#endif //SWIG
};

// MPS-maximum principal stress
class mlawNonLocalPorousCoupledLawWithMPS : public mlawNonLocalPorousCoupledLaw{
   public:
        // Default constructor
      mlawNonLocalPorousCoupledLawWithMPS(const int num,const double E,const double nu, const double rho,
                          const double q1, const double q2, const double q3,
                          const double fVinitial, const double lambda0, const J2IsotropicHardening &j2IH, const CLengthLaw &cLLaw,
                          const double tol=1.e-8, const bool matrixbyPerturbation = false, const double pert = 1e-8);
      mlawNonLocalPorousCoupledLawWithMPS(const int num,const double E,const double nu, const double rho,
                          const double q1, const double q2, const double q3,
                          const double fVinitial, const double lambda0, const double kappa, const J2IsotropicHardening &j2IH, const CLengthLaw &cLLaw,
                          const double tol=1.e-8, const bool matrixbyPerturbation = false, const double pert = 1e-8);
    #ifndef SWIG
    mlawNonLocalPorousCoupledLawWithMPS(const mlawNonLocalPorousCoupledLawWithMPS& src):mlawNonLocalPorousCoupledLaw(src){};
    
    virtual ~mlawNonLocalPorousCoupledLawWithMPS(){};
    virtual materialLaw* clone() const {return new mlawNonLocalPorousCoupledLawWithMPS(*this);};
    
    virtual double yieldFunction(const double kcorEq, const double pcor, const double R, const double yieldfV,
                                const IPNonLocalPorosity* q0, const IPNonLocalPorosity* q1, const double* T = NULL,
                                bool diff=false, fullVector<double>* grad = NULL,
                                bool withthermic = false, double* dfdT = NULL) const
                                {
                                  Msg::Error("mlawNonLocalPorousCoupledLawWithMPS::yieldFunction cannot be used");
                                  return -1.;
                                };
                                
    virtual void plasticFlow(double & Ns, double& Nv,
                              const double kcorEq, const double pcor, const double R,
                              const IPNonLocalPorosity* q0, const IPNonLocalPorosity* q1, const double* T = NULL,
                              bool diff=false, fullVector<double>* gradNs=NULL, // grad of Ns with respect to [kcorEq pcor R fV Chi W]
                              fullVector<double>* gradNv = NULL, // grad of Nv with respect to  [kcorEq pcor R fV Chi W]
                              bool withTher = false, double* dNsDT = NULL, double* dNvDT=NULL) const
                              {
                                Msg::Error("mlawNonLocalPorousCoupledLawWithMPS::plasticFlow cannot be used");
                              };
                              
    virtual void checkCoalescence(IPNonLocalPorosity* q1, const IPNonLocalPorosity *q0, const double* T) const;
    #endif //SWIH
};

class mlawNonLocalPorousCoupledHill48LawWithMPS : public mlawNonLocalPorousCoupledLawWithMPS
{
  protected:
    STensor43 _aniM; // anisotropic tensor // sigEff = _aniM:sig
    
  public:
     mlawNonLocalPorousCoupledHill48LawWithMPS(const int num,const double E,const double nu, const double rho,
                        const double q1, const double q2, const double q3,
                        const double fVinitial, const double lambda0, const J2IsotropicHardening &j2IH, const CLengthLaw &cLLaw,
                        const double tol=1.e-8, const bool matrixbyPerturbation = false, const double pert = 1e-8):
                        mlawNonLocalPorousCoupledLawWithMPS(num,E,nu,rho,q1,q2,q3,fVinitial,lambda0,j2IH,cLLaw,tol,matrixbyPerturbation,pert),
                        _aniM(1.,1.){}
    mlawNonLocalPorousCoupledHill48LawWithMPS(const int num,const double E,const double nu, const double rho,
                        const double q1, const double q2, const double q3,
                        const double fVinitial, const double lambda0, const double kappa, const J2IsotropicHardening &j2IH, const CLengthLaw &cLLaw,
                        const double tol=1.e-8, const bool matrixbyPerturbation = false, const double pert = 1e-8):
                        mlawNonLocalPorousCoupledLawWithMPS(num,E,nu,rho,q1,q2,q3,fVinitial,lambda0, kappa,j2IH,cLLaw,tol,matrixbyPerturbation,pert),
                        _aniM(1.,1.){}
                        
    mlawNonLocalPorousCoupledHill48LawWithMPS(const mlawNonLocalPorousCoupledHill48LawWithMPS& src): mlawNonLocalPorousCoupledLawWithMPS(src), _aniM(src._aniM){}
    virtual ~mlawNonLocalPorousCoupledHill48LawWithMPS(){}
    virtual void setYieldParameters(const std::vector<double>& params);
    virtual const STensor43& getAnisotropicTensorReferenceConfiguration() const {return _aniM;};
    virtual void I1J2J3_voidCharacteristicsEvolution_NonLocal_aniso(const STensor3& sig,
                                                const IPNonLocalPorosity *q0, IPNonLocalPorosity* q1,
                                                bool stiff = false,
                                                std::vector<double>* Y = NULL,
                                                std::vector<STensor3>* DYDsig = NULL,
                                                std::vector<std::vector<double> >* DYDNonlocalVars = NULL
                                                ) const;

    virtual double I1J2J3_yieldFunction_aniso(const STensor3& kcor, const double R, const IPNonLocalPorosity* q0, const IPNonLocalPorosity* q1, const double* T = NULL,
                                        bool diff=false, STensor3* DFDkcor=NULL, double* DFDR=NULL, std::vector<double>* DFDY=NULL, 
                                        STensor3* DFDDeltaEp = NULL, STensor3* DFDFdefo = NULL, 
                                        bool withThermic=false, double *DFDT=NULL) const;
    virtual void I1J2J3_getYieldNormal_aniso(STensor3& Np, const STensor3& kcor, const double R, const IPNonLocalPorosity* q0, const IPNonLocalPorosity* q1, const double* T = NULL,
                                        bool diff=false, STensor43* DNpDkcor=NULL, STensor3* DNpDR=NULL, std::vector<STensor3>* DNpDY=NULL,
                                        STensor43* DNpDDeltaEp=NULL, STensor43* DNpDFdefo=NULL,
                                        bool withThermic=false, STensor3* DNpDT=NULL  
                                        ) const;
    virtual void checkCoalescence(IPNonLocalPorosity* q1, const IPNonLocalPorosity *q0, const double* T) const;
    virtual void forceCoalescence(IPNonLocalPorosity* q1, const IPNonLocalPorosity *q0, const double* T) const;
    virtual bool checkCrackCriterion(IPNonLocalPorosity* q1, const IPNonLocalPorosity *q0, const STensor3& Fcur, const SVector3& normal, double delayFactor, const double* T) const;
    

                                  
    virtual void I1J2J3_predictorCorrector_NonLocal(const STensor3& F0,         // initial deformation gradient (input @ time n)
                            const STensor3& F1,         // updated deformation gradient (input @ time n+1)
                            STensor3 &P,                // updated 1st Piola-Kirchhoff stress tensor (output)
                            const IPNonLocalPorosity *ipvprev,       // array of initial internal variable
                            IPNonLocalPorosity *ipvcur,             // updated array of internal variable (in ipvcur on output),
                            STensor43 &Tangent,         // constitutive tangents (output)
                            std::vector<STensor3>  &dLocalVarDF,
                            std::vector<STensor3>  &dStressDNonLocalVar,
                            fullMatrix<double>    &dLocalVarDNonLocalVar,
                            const bool stiff,                   // if true compute the tangents
                            STensor43& dFedF, std::vector<STensor3>& dFeDNonLocalVar,
                            STensor3*  dStressDT=NULL,
                            fullMatrix<double>* dLocalVarDT=NULL,
                            const double T0 =0,const double T=0
                           ) const
    {
      mlawNonLocalPorosity::I1J2J3_predictorCorrector_NonLocal_aniso(F0, F1, P, ipvprev, ipvcur, 
                                  Tangent, dLocalVarDF, dStressDNonLocalVar, dLocalVarDNonLocalVar, stiff, 
                                  dFedF, dFeDNonLocalVar, dStressDT, dLocalVarDT, T0, T);
    }
  
};


class mlawNonLocalPorousCoupledLawWithMPSAndMSS: public mlawNonLocalPorousCoupledLawWithMPS{
  protected:
    mlawNonLocalPorousThomasonLawWithMSS *_mlawShear;
    bool _freezeCoalescenceState;
  
  public:
        // Default constructor
    mlawNonLocalPorousCoupledLawWithMPSAndMSS(const int num,const double E,const double nu, const double rho,
                        const double q1, const double q2, const double q3,
                        const double fVinitial, const double lambda0, const J2IsotropicHardening &j2IH, const CLengthLaw &cLLaw,
                        const double tol=1.e-8, const bool matrixbyPerturbation = false, const double pert = 1e-8);
    mlawNonLocalPorousCoupledLawWithMPSAndMSS(const int num,const double E,const double nu, const double rho,
                        const double q1, const double q2, const double q3,
                        const double fVinitial, const double lambda0, const double kappa, const J2IsotropicHardening &j2IH, const CLengthLaw &cLLaw,
                        const double tol=1.e-8, const bool matrixbyPerturbation = false, const double pert = 1e-8);
    virtual void setThomasonSmoothFactor(const double fact);
    virtual void setShearSmoothFactor(const double fact);
    virtual void setShearFactor(double g);
    virtual void setVoidLigamentExponent(double e);
    virtual void freezeCoalescenceState(const bool fl);
    virtual void setCoalescenceLaw(const CoalescenceLaw& added_coalsLaw); 
    virtual void setNeckingCoalescenceLaw(const CoalescenceLaw& added_coalsLaw); 
    virtual void setShearCoalescenceLaw(const CoalescenceLaw& added_coalsLaw); 
    virtual void setShearVoidEvolutionLaw(const voidStateEvolutionLaw& voidtateLaw);
    #ifndef SWIG
    mlawNonLocalPorousCoupledLawWithMPSAndMSS(const mlawNonLocalPorousCoupledLawWithMPSAndMSS& src);
    
    virtual ~mlawNonLocalPorousCoupledLawWithMPSAndMSS();
    virtual materialLaw* clone() const {return new mlawNonLocalPorousCoupledLawWithMPSAndMSS(*this);};
    
    virtual const CoalescenceLaw* getCoalescenceLaw() const {Msg::Error("getCoalescenceLaw cannot be called!!!"); return NULL;};
        
    virtual double yieldFunction(const double kcorEq, const double pcor, const double R, const double yieldfV,
                                const IPNonLocalPorosity* q0, const IPNonLocalPorosity* q1, const double* T = NULL,
                                bool diff=false, fullVector<double>* grad = NULL,
                                bool withthermic = false, double* dfdT = NULL) const
                                {
                                  Msg::Error("mlawNonLocalPorousCoupledLawWithMPSAndMSS::yieldFunction cannot be used");
                                  return -1.;
                                };
                                
    virtual void plasticFlow(double & Ns, double& Nv,
                              const double kcorEq, const double pcor, const double R,
                              const IPNonLocalPorosity* q0, const IPNonLocalPorosity* q1, const double* T = NULL,
                              bool diff=false, fullVector<double>* gradNs=NULL, // grad of Ns with respect to [kcorEq pcor R fV Chi W]
                              fullVector<double>* gradNv = NULL, // grad of Nv with respect to  [kcorEq pcor R fV Chi W]
                              bool withTher = false, double* dNsDT = NULL, double* dNvDT=NULL) const
                              {
                                Msg::Error("mlawNonLocalPorousCoupledLawWithMPSAndMSS::plasticFlow cannot be used");
                              };
                              
    virtual void voidStateGrowth(const double yieldfV0, const double yieldfV1, // yield porosity
                        const double DeltahatD, const double DeltahatQ, const double DeltahatP, // internal variables
                        const IPNonLocalPorosity *q0, IPNonLocalPorosity *q1) const;
                        
    virtual void I1J2J3_voidCharacteristicsEvolution_NonLocal(const STensor3& sig,
                                                const IPNonLocalPorosity *q0, IPNonLocalPorosity* q1,
                                                bool stiff = false,
                                                std::vector<double>* Y = NULL,
                                                std::vector<STensor3>* DYDsig = NULL,
                                                std::vector<std::vector<double> >* DYDNonlocalVars = NULL
                                                ) const;
    virtual void I1J2J3_voidCharacteristicsEvolution_Local(const STensor3& sig, const STensor3& DeltaEp, const double DeltaHatP,
                                                const IPNonLocalPorosity *q0, IPNonLocalPorosity* q1,
                                                bool stiff = false,
                                                std::vector<double>* Y = NULL,
                                                std::vector<STensor3>* DYDsig = NULL,
                                                std::vector<STensor3>* DYDDeltaEp = NULL,
                                                std::vector<double>* DYDDeltaHatP = NULL
                                                ) const;                          
    virtual double I1J2J3_yieldFunction(const STensor3& kcor, const double R, const IPNonLocalPorosity* q0, const IPNonLocalPorosity* q1, const double* T = NULL,
                                        bool diff=false, STensor3* DFDkcor=NULL, double* DFDR=NULL, std::vector<double>* DFDY=NULL, 
                                        bool withThermic=false, double *DFDT=NULL) const;
    virtual void I1J2J3_getYieldNormal(STensor3& Np, const STensor3& kcor, const double R, const IPNonLocalPorosity* q0, const IPNonLocalPorosity* q1, const double* T = NULL,
                                        bool diff=false, STensor43* DNpDkcor=NULL, STensor3* DNpDR=NULL, std::vector<STensor3>* DNpDY=NULL,
                                        bool withThermic=false, STensor3* DNpDT=NULL  
                                        ) const;
                                        
    virtual void checkCoalescence(IPNonLocalPorosity* q1, const IPNonLocalPorosity *q0, const double* T) const;
    virtual void forceCoalescence(IPNonLocalPorosity* q1, const IPNonLocalPorosity *q0, const double* T) const;
    virtual bool checkCrackCriterion(IPNonLocalPorosity* q1, const IPNonLocalPorosity *q0, const STensor3& Fcur, const SVector3& normal, double delayFactor, const double* T) const;
    #endif //SWIH
    
};


class mlawNonLocalPorousCoupledHill48LawWithMPSAndMSS : public mlawNonLocalPorousCoupledLawWithMPSAndMSS
{
  protected:
    STensor43 _aniM; // anisotropic tensor // sigEff = _aniM:sig
    
  public:
     mlawNonLocalPorousCoupledHill48LawWithMPSAndMSS(const int num,const double E,const double nu, const double rho,
                        const double q1, const double q2, const double q3,
                        const double fVinitial, const double lambda0, const J2IsotropicHardening &j2IH, const CLengthLaw &cLLaw,
                        const double tol=1.e-8, const bool matrixbyPerturbation = false, const double pert = 1e-8):
                        mlawNonLocalPorousCoupledLawWithMPSAndMSS(num,E,nu,rho,q1,q2,q3,fVinitial,lambda0,j2IH,cLLaw,tol,matrixbyPerturbation,pert),
                        _aniM(1.,1.){}
    mlawNonLocalPorousCoupledHill48LawWithMPSAndMSS(const int num,const double E,const double nu, const double rho,
                        const double q1, const double q2, const double q3,
                        const double fVinitial, const double lambda0, const double kappa, const J2IsotropicHardening &j2IH, const CLengthLaw &cLLaw,
                        const double tol=1.e-8, const bool matrixbyPerturbation = false, const double pert = 1e-8):
                        mlawNonLocalPorousCoupledLawWithMPSAndMSS(num,E,nu,rho,q1,q2,q3,fVinitial,lambda0, kappa,j2IH,cLLaw,tol,matrixbyPerturbation,pert),
                        _aniM(1.,1.){}
                        
    mlawNonLocalPorousCoupledHill48LawWithMPSAndMSS(const mlawNonLocalPorousCoupledHill48LawWithMPSAndMSS& src): mlawNonLocalPorousCoupledLawWithMPSAndMSS(src) , _aniM(src._aniM){}
    virtual ~mlawNonLocalPorousCoupledHill48LawWithMPSAndMSS(){}
    virtual void setYieldParameters(const std::vector<double>& params);
    virtual const STensor43& getAnisotropicTensorReferenceConfiguration() const {return _aniM;};
    virtual void I1J2J3_voidCharacteristicsEvolution_NonLocal_aniso(const STensor3& sig,
                                                const IPNonLocalPorosity *q0, IPNonLocalPorosity* q1,
                                                bool stiff = false,
                                                std::vector<double>* Y = NULL,
                                                std::vector<STensor3>* DYDsig = NULL,
                                                std::vector<std::vector<double> >* DYDNonlocalVars = NULL
                                                ) const;
    
    virtual void computeEffectivePressureSVM(const STensor3& F, const IPNonLocalPorosity* q0, const IPNonLocalPorosity* q1,
                                             double& effPressure, double& effSVM) const;
    virtual double I1J2J3_yieldFunction_aniso(const STensor3& kcor, const double R, const IPNonLocalPorosity* q0, const IPNonLocalPorosity* q1, const double* T = NULL,
                                        bool diff=false, STensor3* DFDkcor=NULL, double* DFDR=NULL, std::vector<double>* DFDY=NULL, 
                                        STensor3* DFDDeltaEp = NULL, STensor3* DFDFdefo = NULL, 
                                        bool withThermic=false, double *DFDT=NULL) const;
    virtual void I1J2J3_getYieldNormal_aniso(STensor3& Np, const STensor3& kcor, const double R, const IPNonLocalPorosity* q0, const IPNonLocalPorosity* q1, const double* T = NULL,
                                        bool diff=false, STensor43* DNpDkcor=NULL, STensor3* DNpDR=NULL, std::vector<STensor3>* DNpDY=NULL,
                                        STensor43* DNpDDeltaEp=NULL, STensor43* DNpDFdefo=NULL,
                                        bool withThermic=false, STensor3* DNpDT=NULL  
                                        ) const;
    virtual void checkCoalescence(IPNonLocalPorosity* q1, const IPNonLocalPorosity *q0, const double* T) const;
    virtual void forceCoalescence(IPNonLocalPorosity* q1, const IPNonLocalPorosity *q0, const double* T) const;
    virtual bool checkCrackCriterion(IPNonLocalPorosity* q1, const IPNonLocalPorosity *q0, const STensor3& Fcur, const SVector3& normal, double delayFactor, const double* T) const;
    

                                  
    virtual void I1J2J3_predictorCorrector_NonLocal(const STensor3& F0,         // initial deformation gradient (input @ time n)
                            const STensor3& F1,         // updated deformation gradient (input @ time n+1)
                            STensor3 &P,                // updated 1st Piola-Kirchhoff stress tensor (output)
                            const IPNonLocalPorosity *ipvprev,       // array of initial internal variable
                            IPNonLocalPorosity *ipvcur,             // updated array of internal variable (in ipvcur on output),
                            STensor43 &Tangent,         // constitutive tangents (output)
                            std::vector<STensor3>  &dLocalVarDF,
                            std::vector<STensor3>  &dStressDNonLocalVar,
                            fullMatrix<double>    &dLocalVarDNonLocalVar,
                            const bool stiff,                   // if true compute the tangents
                            STensor43& dFedF, std::vector<STensor3>& dFeDNonLocalVar,
                            STensor3*  dStressDT=NULL,
                            fullMatrix<double>* dLocalVarDT=NULL,
                            const double T0 =0,const double T=0
                           ) const
{
  mlawNonLocalPorosity::I1J2J3_predictorCorrector_NonLocal_aniso(F0, F1, P, ipvprev, ipvcur, 
                              Tangent, dLocalVarDF, dStressDNonLocalVar, dLocalVarDNonLocalVar, stiff, 
                              dFedF, dFeDNonLocalVar, dStressDT, dLocalVarDT, T0, T);
                              
                              
  computeEffectivePressureSVM(F1, ipvprev, ipvcur, ipvcur->getRefToEffectivePressure(), ipvcur->getRefToEffectiveSVM());
}
  
};

#endif // MLAWNONLOCALPOROUSCOALESCENCE_H_
