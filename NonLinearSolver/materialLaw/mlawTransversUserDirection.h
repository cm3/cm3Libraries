//
// C++ Interface: material law for user direction
//
// Author:  <Van Dung NGUYEN (C) 2021>
//
// Copyright: See COPYING file that comes with this distribution
//

#ifndef MLAWTRANSVERSEISOUSERDIRECTION_H_
#define MLAWTRANSVERSEISOUSERDIRECTION_H_
#ifndef SWIG
#include "mlawTransverseIsotropic.h"
#include "UserDirectionGenerator.h"
#include "ipTransverseIsoUserDirection.h"

#endif //SWIG
class mlawTransverseIsoUserDirection : public mlawTransverseIsotropic
{
  protected:
    UserDirectionGenerator* _dirGenerator;
  
  public:
    mlawTransverseIsoUserDirection(const int num,const double E,const double nu, const double rho, 
                  const double EA, const double GA, const double nu_minor, const UserDirectionGenerator& dirGen);
    #ifndef SWIG
    mlawTransverseIsoUserDirection(const mlawTransverseIsoUserDirection& src);
    virtual ~mlawTransverseIsoUserDirection();
    
    const UserDirectionGenerator& getUserDirectionGenerator() const {return *_dirGenerator;};
    virtual materialLaw* clone() const {return new mlawTransverseIsoUserDirection(*this);}
    virtual bool withEnergyDissipation() const {return false;};

    // function of materialLaw
    virtual matname getType() const{return materialLaw::transverseIsoUserDirection;}
    virtual void createIPState(IPStateBase* &ips,bool hasBodyForce,const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const IntPt *GP=NULL, const int gpt =0) const;
    //virtual void initLaws(const std::map<int,materialLaw*> &maplaw){}; // this law is initialized so nothing to do
    //virtual double soundSpeed() const; // default but you can redefine it for your case

    virtual void getTransverseDirection(SVector3 &A, const IPTransverseIsotropic *q1) const 
    {
       const IPTransverseIsoUserDirection *q = NULL;
       q=dynamic_cast<const IPTransverseIsoUserDirection*>(q1);
       if(q!=NULL)
       {
          q->getTransverseDirection(A); 
       }
    }
    #endif //SWIG
};

#endif // MLAWTRANSVERSEISOUSERDIRECTION_H_
