#ifndef LCC_FUNCTIONS_C
#define LCC_FUNCTIONS_C 1

#include "lccfunctions.h"
#include "matrix_operations.h"
#include <stdio.h>
#include <stdlib.h>

#ifdef NONLOCALGMSH
using namespace MFH;
#endif


//*************************************************************************
void malloc_lcc(Lcc* LCC, int N, double vf_i){

	LCC->Nph=N;
        LCC->v1=vf_i;
	//shear and bulk moduli of the phases in the LCC
	mallocvector(&(LCC->mu),N);
	mallocvector(&(LCC->kappa),N);

	//strain concentration tensor (for the inclusion) and derivatives w.r.t. mu0 and mu1 
	//should be generalized for more than 2 phases
	mallocmatrix(&(LCC->A),6,6);
	mallocmatrix(&(LCC->dAdmu0),6,6);
	mallocmatrix(&(LCC->dAdmu1),6,6);
	mallocmatrix(&(LCC->ddAddmu0),6,6);
	mallocmatrix(&(LCC->ddAddmu1),6,6);
	mallocmatrix(&(LCC->ddAdmu0dmu1),6,6);
	
	//effective stiffness of the LCC and derivatives w.r.t. mu0 and mu1
	//should be generalized for more than 2 phases
	mallocmatrix(&(LCC->C),6,6);
	mallocmatrix(&(LCC->C0),6,6);
	mallocmatrix(&(LCC->C1),6,6);
	mallocmatrix(&(LCC->dCdmu0),6,6);
	mallocmatrix(&(LCC->dCdmu1),6,6);
	mallocmatrix(&(LCC->ddCddmu0),6,6);
	mallocmatrix(&(LCC->ddCddmu1),6,6);
	mallocmatrix(&(LCC->ddCdmu0dmu1),6,6);

	//square of an equivalent strain measure and derivatives w.r.t to mu and epsilon
        mallocmatrix(&(LCC->dCdp_bar),6,6);
        mallocmatrix(&(LCC->ddCdmu0dp_bar),6,6);
        mallocmatrix(&(LCC->ddCdmu1dp_bar),6,6); 
        malloctens3(&(LCC->dCdstrn),6,6,6); 
        malloctens3(&(LCC->ddCdmu0dstrn),6,6,6); 
        malloctens3(&(LCC->ddCdmu1dstrn),6,6,6);
}

void set_elprops(Lcc* LCC, double* mu, double* kappa){
	
	if(LCC->Nph==1){
		LCC->mu[0]=mu[0];
                LCC->kappa[0]=kappa[0];
		makeiso(LCC->kappa[0],LCC->mu[0],LCC->C0);
	}

	else if(LCC->Nph==2){
		LCC->mu[0]=mu[0];
		LCC->mu[1]=mu[1];
		LCC->kappa[0]=kappa[0];
		LCC->kappa[1]=kappa[1];
		makeiso(LCC->kappa[0],LCC->mu[0],LCC->C0);
		makeiso(LCC->kappa[1],LCC->mu[1],LCC->C1);
	}
	else{
		printf("Case of LCC with more than two phase not allowed!\n");
	}
	
}


void free_lcc(Lcc* LCC){

	int Nph=LCC->Nph;

	free(LCC->mu);
	free(LCC->kappa);
	freematrix(LCC->A,6);
	freematrix(LCC->dAdmu0,6);
	freematrix(LCC->dAdmu1,6);
	freematrix(LCC->ddAddmu0,6);
	freematrix(LCC->ddAddmu1,6);
	freematrix(LCC->ddAdmu0dmu1,6);

	freematrix(LCC->C,6);
	freematrix(LCC->C0,6);
	freematrix(LCC->C1,6);
	freematrix(LCC->dCdmu0,6);
	freematrix(LCC->dCdmu1,6);
	freematrix(LCC->ddCddmu0,6);
	freematrix(LCC->ddCddmu1,6);
	freematrix(LCC->ddCdmu0dmu1,6);

        freematrix(LCC->dCdp_bar,6);
        freematrix(LCC->ddCdmu0dp_bar,6);
        freematrix(LCC->ddCdmu1dp_bar,6); 
        freetens3(LCC->dCdstrn,6,6); 
        freetens3(LCC->ddCdmu0dstrn,6,6); 
        freetens3(LCC->ddCdmu1dstrn,6,6);

}

void malloc_YF(YieldF* YFdf){

	//derivatives w.r. strain
	mallocvector(&(YFdf->dfdstrn),6);
	mallocvector(&(YFdf->dpdstrn),6);

	mallocvector(&(YFdf->dfdmu),2);
	mallocvector(&(YFdf->dpdmu),2);
}

void free_YF(YieldF* YFdf){

	free(YFdf->dfdstrn);
	free(YFdf->dpdstrn);

	free(YFdf->dfdmu);
	free(YFdf->dpdmu);
}



//************************************************************************
// COMPUTE MAROSTIFFNESS AND STRAIN CONCENTRATION TENSORS AND THEIR FIRST DERIVATIVES
//************************************************************************
void CA_dCdA_MT(Lcc* LCC, double v1, double **S, double **dS, double *dnu){

	int i,j;
	int error;
	double v0;
	double** diffC, **invA;
	double** P, **Pdev;
	double** mat1, **mat2, **mat3;
	double** Idev, **Ivol, **I;
	double** Sdev;
	double** invC;
	double** dinvAdmu0, **dinvAdmu1;
	double** devA;

	double mu0,mu1;
	
	mallocmatrix(&diffC,6,6);
	mallocmatrix(&invA,6,6);
	mallocmatrix(&mat1,6,6);
	mallocmatrix(&mat2,6,6);
	mallocmatrix(&mat3,6,6);
	mallocmatrix(&Idev,6,6);
	mallocmatrix(&Ivol,6,6);
	mallocmatrix(&I,6,6);
	mallocmatrix(&P,6,6);
	mallocmatrix(&Pdev,6,6);

	mallocmatrix(&invC,6,6);
	mallocmatrix(&Sdev,6,6);
	mallocmatrix(&dinvAdmu0,6,6);
	mallocmatrix(&dinvAdmu1,6,6);

	mallocmatrix(&devA,6,6);


	//to check: maybe not necessary
	cleartens4(LCC->A);
	cleartens4(LCC->dAdmu0);
	cleartens4(LCC->dAdmu1);

	v0 = 1.-v1;

	mu0=LCC->mu[0];
	mu1=LCC->mu[1];

	for(i=0;i<3;i++){
		Idev[i][i]=1.;
		Idev[i+3][i+3]=1.;
		I[i][i]=1.;
		I[i+3][i+3]=1.;
		for(j=0;j<3;j++){
			Ivol[i][j]=1./3.;
			Idev[i][j]=Idev[i][j]-1./3.;
		}
	}	

      //*** Compute A*******
	inverse(LCC->C0,invC,&error,6);
	contraction44(S,invC,P);
	contraction44(P,Idev,Pdev);
	contraction44(S,Idev,Sdev);

			
	contraction44(invC,LCC->C1,mat1); //mat1 = invC0:C1 - 1
	for(i=0;i<6;i++){
		mat1[i][i] -= 1.;
	}
	contraction44(S,mat1,invA);
	addtens4(1.,I, v0,invA,invA);

	inverse(invA,LCC->A,&error,6);
	

        //1st derivatives of A
	//dAdmu0:

        addtens4(0.0,I,dnu[0],dS,dS);      //dsdmu=dsdnu*dnudmu

	contraction44(dS,mat1,mat2);
	
	addtens4(-v0,mat2,v0*mu1/(mu0*mu0),Sdev,dinvAdmu0);  //warning: opposite sign
	
	contraction44(dinvAdmu0,LCC->A,mat2); 
	contraction44(LCC->A,mat2,LCC->dAdmu0);
	
	//dAdmu1:
	addtens4(-2.*v0,Pdev,0,Pdev,dinvAdmu1);	//warning: opposite sign
	contraction44(dinvAdmu1,LCC->A,mat3);
	contraction44(LCC->A,mat3,LCC->dAdmu1);


     //***** Compute C **********
        addtens4(1.,LCC->C1,-1.,LCC->C0,diffC);
	contraction44(diffC,LCC->A,mat1);
	addtens4(1.,LCC->C0,v1,mat1,LCC->C);


        contraction44(Idev,LCC->A,devA);
	
        //1st derivatives of C
	//dCdmu0:
	contraction44(diffC,LCC->dAdmu0,mat1);
	addtens4(2.,Idev,-2.*v1,devA,LCC->dCdmu0);
	addtens4(1.,LCC->dCdmu0,v1,mat1,LCC->dCdmu0);
	
	//dCdmu1:
	contraction44(diffC,LCC->dAdmu1,mat1);
	addtens4(2.*v1,devA,v1,mat1,LCC->dCdmu1);

		
	freematrix(invA,6);
	freematrix(mat1,6);
	freematrix(mat2,6);
	freematrix(mat3,6);
	freematrix(Idev,6);
	freematrix(Ivol,6);
	freematrix(I,6);
	freematrix(P,6);
	freematrix(Pdev,6);
	freematrix(invC,6);
	freematrix(Sdev,6);
	freematrix(diffC,6);

	freematrix(devA,6);
	freematrix(dinvAdmu0,6);
	freematrix(dinvAdmu1,6);
	
}


//************************************************************************
// COMPUTE MAROSTIFFNESS AND STRAIN CONCENTRATION TENSORS AND THEIR FIRST  and second DERIVATIVES
//************************************************************************
void CA_2ndCdA_MT(Lcc* LCC, double v1, double **S, double **dS, double **ddS, double *dnu){

	int i,j;
	int error;
	double v0;
	double** diffC, **invA;
	double** P, **Pdev;
	double** mat1, **mat2, **mat3;
	double** Idev, **Ivol, **I;
	double** Sdev, **dSdev;
	double** invC;
	double** dinvAdmu0, **dinvAdmu1;
	double** devA, **devdAdmu0, **devdAdmu1;

        double **ddinvAddmu0, **ddinvAdmu0dmu1;

	double mu0,mu1;
	
	mallocmatrix(&diffC,6,6);
	mallocmatrix(&invA,6,6);
	mallocmatrix(&mat1,6,6);
	mallocmatrix(&mat2,6,6);
	mallocmatrix(&mat3,6,6);
	mallocmatrix(&Idev,6,6);
	mallocmatrix(&Ivol,6,6);
	mallocmatrix(&I,6,6);
	mallocmatrix(&P,6,6);
	mallocmatrix(&Pdev,6,6);

	mallocmatrix(&invC,6,6);
	mallocmatrix(&Sdev,6,6);
	mallocmatrix(&dSdev,6,6);
	mallocmatrix(&dinvAdmu0,6,6);
	mallocmatrix(&dinvAdmu1,6,6);

	mallocmatrix(&devA,6,6);
	mallocmatrix(&devdAdmu0,6,6);
	mallocmatrix(&devdAdmu1,6,6);

	mallocmatrix(&ddinvAddmu0,6,6);
	mallocmatrix(&ddinvAdmu0dmu1,6,6);


	//to check: maybe not necessary
	cleartens4(LCC->A);
	cleartens4(LCC->dAdmu0);
	cleartens4(LCC->dAdmu1);

	cleartens4(LCC->C);
	cleartens4(LCC->dCdmu0);
	cleartens4(LCC->dCdmu1);

	v0 = 1.-v1;

	mu0=LCC->mu[0];
	mu1=LCC->mu[1];

	for(i=0;i<3;i++){
		Idev[i][i]=1.;
		Idev[i+3][i+3]=1.;
		I[i][i]=1.;
		I[i+3][i+3]=1.;
		for(j=0;j<3;j++){
			Ivol[i][j]=1./3.;
			Idev[i][j]=Idev[i][j]-1./3.;
		}
	}	

      //*** Compute A*******
	inverse(LCC->C0,invC,&error,6);
	contraction44(S,invC,P);
	contraction44(P,Idev,Pdev);
	contraction44(S,Idev,Sdev);
			
	contraction44(invC,LCC->C1,mat1); //mat1 = invC0:C1 - 1
	for(i=0;i<6;i++){
		mat1[i][i] -= 1.;
	}
	contraction44(S,mat1,invA);
	addtens4(1.,I, v0,invA,invA);

	inverse(invA,LCC->A,&error,6);

        addtens4(dnu[1],dS, dnu[0]*dnu[0],ddS,ddS);      //ddsddmu=ddsddnu*(dnudmu)^2+dS*ddnuddmu
        addtens4(0.0,I,dnu[0],dS,dS);      //dsdmu=dsdnu*dnudmu	
	contraction44(dS,Idev,dSdev);

        //1st derivatives of A
	//dAdmu0:

	contraction44(dS,mat1,mat2);
	
	addtens4(-v0,mat2,v0*mu1/(mu0*mu0),Sdev,dinvAdmu0);  //warning: opposite sign
	
	contraction44(dinvAdmu0,LCC->A,mat2); 
	contraction44(LCC->A,mat2,LCC->dAdmu0);
	
	//dAdmu1:
	addtens4(-2.*v0,Pdev,0,Pdev,dinvAdmu1);	//warning: opposite sign
	contraction44(dinvAdmu1,LCC->A,mat3);
	contraction44(LCC->A,mat3,LCC->dAdmu1);

        //2st derivatives of A

	//ddAdmu0dmu1:
	contraction44(ddS,mat1,mat2);
	addtens4(1.,dSdev,-1./mu0,Sdev,mat3);
	addtens4(v0,mat2,-2.*v0*mu1/(mu0*mu0),mat3,ddinvAddmu0);	//good sign
	//ddinvAdmu0dmu1
	addtens4(v0/mu0,dSdev,-v0/(mu0*mu0),Sdev,ddinvAdmu0dmu1); //good sign

	//ddAddmu0:
        contraction44(LCC->dAdmu0,dinvAdmu0,mat2);
	contraction44(LCC->A,ddinvAddmu0,mat3);
	addtens4(1.,mat2,-1.,mat3,mat2);
	contraction44(mat2,LCC->A,LCC->ddAddmu0);
	contraction44(dinvAdmu0,LCC->dAdmu0,mat2);
	contraction44(LCC->A,mat2,mat3);
	addtens4(1.,LCC->ddAddmu0,1.,mat3,LCC->ddAddmu0);

	//ddAdmu0dmu1:
	contraction44(LCC->dAdmu0,dinvAdmu1,mat2);
	contraction44(LCC->A,ddinvAdmu0dmu1,mat3);
	addtens4(1.,mat2,-1.,mat3,mat2);
	contraction44(mat2,LCC->A,LCC->ddAdmu0dmu1);
	contraction44(dinvAdmu1,LCC->dAdmu0,mat2);
	contraction44(LCC->A,mat2,mat3);
	addtens4(1.,LCC->ddAdmu0dmu1,1.,mat3,LCC->ddAdmu0dmu1);

	//ddAddmu1:
	contraction44(LCC->dAdmu1,dinvAdmu1,mat2);
	contraction44(mat2,LCC->A,LCC->ddAddmu1);
	contraction44(dinvAdmu1,LCC->dAdmu1,mat2);
	contraction44(LCC->A,mat2,mat3);
	addtens4(1.,LCC->ddAddmu1,1.,mat3,LCC->ddAddmu1);



     //***** Compute C **********
        addtens4(1.,LCC->C1,-1.,LCC->C0,diffC);
	contraction44(diffC,LCC->A,mat1);
	addtens4(1.,LCC->C0,v1,mat1,LCC->C);


        contraction44(Idev,LCC->A,devA);
	contraction44(Idev,LCC->dAdmu0,devdAdmu0);
	contraction44(Idev,LCC->dAdmu1,devdAdmu1);
	
        //1st derivatives of C
	//dCdmu0:
	contraction44(diffC,LCC->dAdmu0,mat1);
	addtens4(2.,Idev,-2.*v1,devA,LCC->dCdmu0);
	addtens4(1.,LCC->dCdmu0,v1,mat1,LCC->dCdmu0);
	
	//dCdmu1:
	contraction44(diffC,LCC->dAdmu1,mat1);
	addtens4(2.*v1,devA,v1,mat1,LCC->dCdmu1);


        //2nd derivatives of C
	//ddCddmu0
	contraction44(diffC,LCC->ddAddmu0,mat1);
	addtens4(-4.*v1,devdAdmu0,v1,mat1,LCC->ddCddmu0);

	//ddCdmu0dmu1
	contraction44(diffC,LCC->ddAdmu0dmu1,mat1);
	addtens4(1.,devdAdmu0,-1.,devdAdmu1,LCC->ddCdmu0dmu1);
	addtens4(2.*v1,LCC->ddCdmu0dmu1,v1,mat1,LCC->ddCdmu0dmu1);

	//ddCddmu1
	contraction44(diffC,LCC->ddAddmu1,mat1);
	addtens4(4.*v1,devdAdmu1,v1,mat1,LCC->ddCddmu1);

		
	freematrix(invA,6);
	freematrix(mat1,6);
	freematrix(mat2,6);
	freematrix(mat3,6);
	freematrix(Idev,6);
	freematrix(Ivol,6);
	freematrix(I,6);
	freematrix(P,6);
	freematrix(Pdev,6);
	freematrix(invC,6);
	freematrix(Sdev,6);
	freematrix(dSdev,6);
	freematrix(diffC,6);

	freematrix(devA,6);
	freematrix(devdAdmu0,6);
	freematrix(devdAdmu1,6);
	freematrix(dinvAdmu0,6);
	freematrix(dinvAdmu1,6);

	freematrix(ddinvAddmu0,6);
	freematrix(ddinvAdmu0dmu1,6);
	
}



//****************************************************************
//COMPUTE EQUIVALENT STRAIN AND DERIVATIVES - 2nd order 
//****************************************************************
void eqstrn2_order2(int mtx, double* e, Lcc* LCC, double* es2, double* des2de, double* des2dmu0, double* des2dmu1){

	double** dC, **ddCmix0, **ddCmix1;
	double v0,v;
	double* vec1;
	
	v0 = 1.- LCC->v1;
	
	mallocmatrix(&dC,6,6);
	mallocmatrix(&ddCmix0,6,6);
	mallocmatrix(&ddCmix1,6,6);
	mallocvector(&vec1,6);
	
	if(mtx){
			
		copymatr(LCC->dCdmu0,dC,6,6);
		copymatr(LCC->ddCddmu0,ddCmix0,6,6);
		copymatr(LCC->ddCdmu0dmu1,ddCmix1,6,6);
	
		v=v0;
	}
	else{
		copymatr(LCC->dCdmu1,dC,6,6);
		copymatr(LCC->ddCdmu0dmu1,ddCmix0,6,6);
		copymatr(LCC->ddCddmu1,ddCmix1,6,6);
		
		v=LCC->v1;
	}		
		
	contraction42(dC,e,vec1);
		
	*es2=(contraction22(e,vec1))/(3.*v);
			
	//first derivatives
	//des2de
	addtens2(2./(3.*v),vec1,0,vec1,des2de);
	//des2dmu0
	contraction42(ddCmix0,e,vec1);
	*des2dmu0=(contraction22(e,vec1))/(3.*v);
	//des2dmu1
	contraction42(ddCmix1,e,vec1);
	*des2dmu1=(contraction22(e,vec1))/(3.*v);
			
	free(vec1);		
	freematrix(dC,6);
	freematrix(ddCmix0,6);
	freematrix(ddCmix1,6);		
			
}


#endif
