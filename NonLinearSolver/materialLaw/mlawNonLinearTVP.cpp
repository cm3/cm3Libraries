//
// C++ Interface: Material Law
//
// Description: Non-Linear Thermo-Visco-Mechanics (Thermo-ViscoElasto-ViscoPlasto Law) with Non-Local Damage Interface (soon.....)
//
// Author: <Ujwal Kishore J - FLE_Knight>, (C) 2022
//
// Copyright: See COPYING file that comes with this distribution
//
//

#include "STensorOperations.h"
#include "nonLinearMechSolver.h"
#include "mlawNonLinearTVP.h"


mlawNonLinearTVP::mlawNonLinearTVP(const int num,const double E,const double nu, const double rho, const double tol,
                                   const double Tinitial, const double Alpha, const double KThCon, const double Cp,
                                   const bool matrixbyPerturbation, const double pert, const bool thermalEstimationPreviousConfig):
                                   // mlawNonLinearTVE(num, E, nu, rho, tol, Tinitial, Alpha, KThCon, Cp, matrixbyPerturbation, pert, thermalEstimationPreviousConfig),
                                   mlawNonLinearTVM(num, E, nu, rho, tol, Tinitial, Alpha, KThCon, Cp, matrixbyPerturbation, pert, thermalEstimationPreviousConfig),
                                   _TaylorQuineyFactor(0.9), _TaylorQuineyFlag(false),_PolyOrderChaboche(0), _HR(1.){

      _PolyCoeffsChaboche.resize(_PolyOrderChaboche+1);
      _PolyCoeffsChaboche.setAll(0.);
      _PolyCoeffsChaboche(0) = 1.;

      // by default, no temperature dependence
      _temFunc_Sy0 = new constantScalarFunction(1.);
      _temFunc_H = new constantScalarFunction(1.);
      _temFunc_Hb = new constantScalarFunction(1.);
      _temFunc_visc = new constantScalarFunction(1.);

};

// , mlawPowerYieldHyper(src)
mlawNonLinearTVP::mlawNonLinearTVP(const mlawNonLinearTVP& src): mlawNonLinearTVM(src) {

    _TaylorQuineyFactor = src._TaylorQuineyFactor;
    _TaylorQuineyFlag = src._TaylorQuineyFlag;
    _PolyOrderChaboche = src._PolyOrderChaboche;
    _PolyCoeffsChaboche = src._PolyCoeffsChaboche;
    _HR = src._HR;

  _temFunc_Sy0 = NULL; // temperature dependence of initial yield stress
  if (src._temFunc_Sy0!= NULL)
    _temFunc_Sy0 = src._temFunc_Sy0->clone();

  _temFunc_H = NULL; // temperature dependence of hardening stress
  if (src._temFunc_H != NULL){
    _temFunc_H = src._temFunc_H->clone();
  }

  _temFunc_Hb = NULL; // temperature dependence of kinematic hardening
  if (src._temFunc_Hb != NULL)
    _temFunc_Hb = src._temFunc_Hb->clone();

  _temFunc_visc = NULL; // temperature dependence of viscosity
  if (src._temFunc_visc != NULL)
    _temFunc_visc = src._temFunc_visc->clone();
};

mlawNonLinearTVP& mlawNonLinearTVP::operator=(const materialLaw& source){

    mlawNonLinearTVM::operator=(source);
    // mlawPowerYieldHyper::operator=(source);
    const mlawNonLinearTVP* src =dynamic_cast<const mlawNonLinearTVP*>(&source);
    if(src != NULL){
        _TaylorQuineyFactor = src->_TaylorQuineyFactor;
        _TaylorQuineyFlag = src->_TaylorQuineyFlag;
        _PolyOrderChaboche = src->_PolyOrderChaboche;

        _PolyCoeffsChaboche.resize(src->_PolyCoeffsChaboche.size());
        _PolyCoeffsChaboche.setAll(src->_PolyCoeffsChaboche);

        _HR = src->_HR;
        if(_temFunc_Sy0 != NULL) delete _temFunc_Sy0; // temperature dependence of initial yield stress
        if (src->_temFunc_Sy0!= NULL)
            _temFunc_Sy0 = src->_temFunc_Sy0->clone();
        if(_temFunc_H != NULL) delete _temFunc_H; // temperature dependence of hardening stress
        if (src->_temFunc_H != NULL)
            _temFunc_H = src->_temFunc_H->clone();
        if(_temFunc_Hb != NULL) delete _temFunc_Hb; // temperature dependence of kinematic hardening
        if (src->_temFunc_Hb != NULL)
            _temFunc_Hb = src->_temFunc_Hb->clone();
        if(_temFunc_visc != NULL) delete _temFunc_visc; // temperature dependence of viscosity
        if (src->_temFunc_visc != NULL)
            _temFunc_visc = src->_temFunc_visc->clone();
    }
    return *this;
};

mlawNonLinearTVP::~mlawNonLinearTVP(){
    if (_temFunc_Sy0 != NULL) delete _temFunc_Sy0;  _temFunc_Sy0 = NULL;
    if (_temFunc_H != NULL) delete _temFunc_H; _temFunc_H= NULL;
    if (_temFunc_Hb != NULL) delete _temFunc_Hb; _temFunc_Hb= NULL;
    if (_temFunc_visc != NULL) delete _temFunc_visc; _temFunc_visc= NULL;
};

// CHECK IF IPSTATE NEEDS the same definition as in mlawPowerYieldHyper --- WHAT??

void mlawNonLinearTVP::setIsotropicHardeningCoefficients(const double HR1, const double HR2, const double HR3){

    _HR.clear();
    _HR.resize(3,0.);
    _HR[0] = HR1;
    _HR[1] = HR2;
    _HR[2] = HR3;
};

void mlawNonLinearTVP::hardening(const IPNonLinearTVP* q0, IPNonLinearTVP* q1, const double& T) const{
  //Msg::Error("epspCompression = %e, epspTRaction = %e, epspShear = %e",q->_epspCompression,q->_epspTraction,q->_epspShear);
  if (_compression != NULL && q1->_ipCompression != NULL){
    _compression->hardening(q0->_epspCompression, *q0->_ipCompression, q1->_epspCompression,T,*q1->_ipCompression);
  }

  if (_traction!= NULL && q1->_ipTraction != NULL){
    _traction->hardening(q0->_epspTraction,*q0->_ipTraction, q1->_epspTraction,T,*q1->_ipTraction);
  }

  if (_kinematic!= NULL && q1->_ipKinematic != NULL)
    _kinematic->hardening(q0->_epspbarre,*q0->_ipKinematic,q1->_epspbarre,T,*q1->_ipKinematic);
};

void mlawNonLinearTVP::getModifiedMandel(const STensor3& C, const IPNonLinearTVP *q0, IPNonLinearTVP *q1) const{

    STensor3& M = q1->getRefToModifiedMandel();
    const STensor3& corKir = q1->getConstRefToCorotationalKirchhoffStress();

    STensor3 Cinv;
    STensorOperation::inverseSTensor3(C, Cinv);

    STensor3 temp1, temp2;
    STensorOperation::multSTensor3(C, corKir, temp1);
    STensorOperation::multSTensor3(temp1, Cinv, temp2);

    M = 0.5*(corKir + temp2);
};

void mlawNonLinearTVP::checkCommutavity(STensor3& commuteCheck,const STensor3& Ce, const STensor3& S, const IPNonLinearTVP *q1) const{

    const STensor3& M = q1->getConstRefToModifiedMandel();
    const STensor3& corKir = q1->getConstRefToCorotationalKirchhoffStress();

    STensor3 temp1, temp2, temp3, temp4, temp6;
    STensorOperation::multSTensor3(Ce, S, temp1);
    STensorOperation::multSTensor3(S, Ce, temp2);
    STensorOperation::multSTensor3(corKir, Ce, temp3);
    STensorOperation::multSTensor3(Ce, corKir, temp4);

    // Check S
    commuteCheck = temp1;
    commuteCheck -= temp2;

    // Check corKir (just for fun)
    temp6 = temp3;
    temp6 -= temp4;

    // RANK OF A ZERO TENSOR is ZERO - maybe use this?
};

void mlawNonLinearTVP::setPolynomialOrderChabocheCoeffs(const int order){_PolyOrderChaboche = order;};

void mlawNonLinearTVP::setPolynomialCoeffsChabocheCoeffs(const int i, const double val){
    _PolyCoeffsChaboche(i) = val;
    Msg::Info("setting: term=%d PolyCoeffsChaboche= %e",i,val);
}

void mlawNonLinearTVP::getChabocheCoeffs(fullVector<double>& coeffs, const double& opt, const IPNonLinearTVP *q1, std::vector<double>* dcoeffs) const{

    const double p = q1->_epspbarre; // eq.plastic strain
    coeffs.resize(2);

    coeffs(0) = 1; // 0; //DEBUGGING
    coeffs(1) = 0; // 0; //DEBUGGING


    if(dcoeffs!=NULL){
        dcoeffs->resize(2);
        dcoeffs->at(0) = 1;
        dcoeffs->at(1) = 0;
    }
    if (opt==1){
        if (_PolyOrderChaboche == 1){
            coeffs(0) += _PolyCoeffsChaboche(0)+ _PolyCoeffsChaboche(1)*p;
            if(dcoeffs!=NULL){dcoeffs->at(0) += _PolyCoeffsChaboche(1);}
        }
        else if (_PolyOrderChaboche == 2){
            coeffs(0) += _PolyCoeffsChaboche(0)+ _PolyCoeffsChaboche(1)*p + _PolyCoeffsChaboche(2)*p*p;
            if(dcoeffs!=NULL){dcoeffs->at(0) += _PolyCoeffsChaboche(1)+2.*_PolyCoeffsChaboche(2)*p;}
        }
        else if (_PolyOrderChaboche == 3){
            coeffs(0) += _PolyCoeffsChaboche(0)+ _PolyCoeffsChaboche(1)*p + _PolyCoeffsChaboche(2)*p*p+_PolyCoeffsChaboche(3)*p*p*p;
            if(dcoeffs!=NULL){dcoeffs->at(0) +=  _PolyCoeffsChaboche(1)+2.*_PolyCoeffsChaboche(2)*p+ 3*_PolyCoeffsChaboche(3)*p*p;}
        }
        else
            Msg::Error("order %d is not implemented in PolynomialChacbocheCoeffs of mlawNonLinearTVP::getChabocheCoeffs",_PolyOrderChaboche);
    }
};

void mlawNonLinearTVP::getPlasticPotential(const double& phiP, const double& phiE, double& Px) const{

    Px = pow(phiE,2) + _b*pow(phiP,2);
};

void mlawNonLinearTVP::getYieldCoefficientTempDers(const IPNonLinearTVP *q1, const double& T, fullVector<double>& DcoeffsDT, fullVector<double>& DDcoeffsDTT) const{
  double sigc(0.), Hc(0.), dsigcdT(0.), ddsigcdTT(0.);
  sigc = q1->_ipCompression->getR();
  Hc = q1->_ipCompression->getDR();
  dsigcdT = q1->_ipCompression->getDRDT();
  ddsigcdTT = q1->_ipCompression->getDDRDTT();


  double sigt(0.), Ht(0.), dsigtdT(0.), ddsigtdTT(0.);
  sigt = q1->_ipTraction->getR();
  Ht = q1->_ipTraction->getDR();
  dsigtdT = q1->_ipTraction->getDRDT();
  ddsigtdTT = q1->_ipTraction->getDDRDTT();


  DcoeffsDT.resize(3);
  DDcoeffsDTT.resize(3);
  double m = sigt/sigc;
  double DmDT = (dsigtdT*sigc- sigt*dsigcdT)/(sigc*sigc);
  double DDmDTT = (ddsigtdTT*sigc - sigt*ddsigcdTT)/(sigc*sigc) - 2.*(dsigtdT*sigc- sigt*dsigcdT)/(sigc*sigc*sigc)*dsigcdT;
  double Da1Dm = 3./sigc*(_n*pow(m,_n-1.)/(m+1.) - (pow(m,_n)-1.)/(m+1.)/(m+1.));
  double DDa1Dmm = 3./sigc*(_n*(_n-1.)*pow(m,_n-2.)/(m+1.) - _n*(pow(m,_n)-1.)/(m+1.)/(m+1.) - _n*pow(m,_n-1)/(m+1.)/(m+1.) + 2.*(pow(m,_n)-1.)/(m+1.)/(m+1.)/(m+1.));
  double term = _n*pow(m,_n) + _n*pow(m,_n-1.) - pow(m,_n);
  double dterm = pow(_n,2.)*pow(m,_n-1.) + _n*(_n-1)*pow(m,_n-2.) - _n*pow(m,_n-1.);

  DcoeffsDT(2) = -_n*pow(sigc,-_n-1.)*dsigcdT;
  DcoeffsDT(1) = Da1Dm*DmDT + 3.*(pow(m,_n)-1.)/(m+1.)/(sigc*sigc)*dsigcdT;
  DcoeffsDT(0) = ((_n*pow(m,_n-1.)+1.)/(m+1.) - (pow(m,_n)+m)/(m+1.)/(m+1.))*DmDT;

  DDcoeffsDTT(2) = -_n*pow(sigc,-_n-1.)*ddsigcdTT -_n*(-_n-1.)*pow(sigc,-_n-2.)*dsigcdT*dsigcdT;
  DDcoeffsDTT(1) = -1./sigc*dsigcdT*DcoeffsDT(1) + 3./sigc*( (dterm/(m+1.)/(m+1.) -2.*term/(m+1.)/(m+1.)/(m+1.))*DmDT*DmDT + term/(m+1.)/(m+1.)*DDmDTT
                   + 1./sigc*( (1./sigc*pow(dsigtdT,2.) - ddsigcdTT) * (pow(m,_n)-1.)/(m+1.)  + dsigtdT * term/(m+1.)/(m+1.) ) );
  DDcoeffsDTT(0) = ((_n*pow(m,_n-1.)+1.)/(m+1) - (pow(m,_n)+m)/(m+1.)/(m+1.))*DDmDTT
                    + ((_n*(_n-1.)*pow(m,_n-2.))/(m+1.) + 2.*(pow(m,_n)+m)/(m+1.)/(m+1.)/(m+1.))*DmDT*DmDT;

};

void mlawNonLinearTVP::getFreeEnergyTVM(const IPNonLinearTVP *q0, IPNonLinearTVP *q1, const double& T0, const double& T,
                                      double *psiTVM, double *DpsiTVMdT, double *DDpsiTVMdTT) const{

    // get some Things
    const double& Gamma = q1->_Gamma;
    const double& DgammaDT = q1->_DgammaDT;
    const double& dGammaDT = q1->_DGammaDT;
    const STensor3& Me = q1->_ModMandel;
    const STensor3& X = q1->_backsig;
    const STensor3& dXdT  = q1->_DbackSigDT;

    STensor3 devMe, devX, dDevXdT, dDevMeDT;
    double trMe, trX, dTrXdT, dTrMeDT;
    STensorOperation::decomposeDevTr(q1->_ModMandel,devMe,trMe);
    STensorOperation::decomposeDevTr(q1->_backsig,devX,trX);
    STensorOperation::decomposeDevTr(q1->_DbackSigDT,dDevXdT,dTrXdT);
    STensorOperation::decomposeDevTr(q1->_DModMandelDT,dDevMeDT,dTrMeDT);

    // Start
    double psiVE, psiVP, psiT = 0.;
    double DpsiVEdT, DpsiVPdT, DpsiTdT = 0.;
    double DDpsiVEdTT, DDpsiVPdTT, DpsiTdTT = 0.;

    double CpT, DCpDT, DDCpDTT;
    mlawNonLinearTVM::getCp(CpT,T,&DCpDT,&DDCpDTT);

    // psiT
    psiT = CpT*((T-_Tinitial)-T*log(T/_Tinitial));
    DpsiTdT = -CpT*log(T/_Tinitial) - psiT/CpT*DCpDT;
    DpsiTdTT = -CpT/T - psiT/CpT*DDCpDTT;

    //
    // psiVP

    // get R, dRdT
    fullVector<double> a(3), Da(3), DaDT(3), DDaDTT(3);
    getYieldCoefficients(q1,a);
    getYieldCoefficientDerivatives(q1,q1->_nup,Da);
    getYieldCoefficientTempDers(q1,T,DaDT,DDaDTT);
    double sigc(0.), Hc(0.), dsigcdT(0.), ddsigcdTT(0.);
    sigc = q1->_ipCompression->getR();
    Hc = q1->_ipCompression->getDR();
    dsigcdT = q1->_ipCompression->getDRDT();
    ddsigcdTT = q1->_ipCompression->getDDRDTT();

    double R1(0.), R2(0.), R3(0.), dR1dT(0.), dR2dT(0.), dR3dT(0.), ddR1dTT(0.), ddR2dTT(0.), ddR3dTT(0.);

    const std::vector<double>& IsoHardForce =  q1->_IsoHardForce;
    const std::vector<double>& dIsoHardForcedT =  q1->_dIsoHardForcedT;
    const std::vector<double>& ddIsoHardForcedTT =  q1->_ddIsoHardForcedTT;

    R1 = IsoHardForce[0]; R2 = IsoHardForce[1]; R3 = IsoHardForce[2];
    dR1dT = dIsoHardForcedT[0]; dR2dT = dIsoHardForcedT[1]; dR3dT = dIsoHardForcedT[2];
    ddR1dTT = ddIsoHardForcedTT[0]; ddR2dTT = ddIsoHardForcedTT[1]; ddR3dTT = ddIsoHardForcedTT[2];

     // get Kinematic Hardening stuff
    double Hb(0.), dHbdT(0.), ddHbddT(0.);
    if (q1->_ipKinematic != NULL){
        Hb = q1->_ipKinematic->getR(); // kinematic hardening parameter
        dHbdT = q1->_ipKinematic->getDRDT();
        ddHbddT = q1->_ipKinematic->getDDRDTT();
    }

    double kk = 1./sqrt(1.+2.*q1->_nup*q1->_nup);
    static STensor3 alpha;
    alpha = X;
    alpha *= 1/(pow(kk,2)*Hb);

    // ddXdTT
    static STensor3 ddXdTT; // ddGammaDTT and ddQDTT are very difficult to estimate
    STensorOperation::zero(ddXdTT);
    double delT = T-T0;
    if (delT>1e-10){
     for (int i=0; i<3; i++)
       for (int j=0; j<3; j++){
         ddXdTT(i,j) += 2*dXdT(i,j)/(T-T0) - 2*(X(i,j)-q0->_backsig(i,j))/pow((T-T0),2);
       }
    }

    static STensor3 dAlphadT, ddAlphadTT;
    for (int i=0; i<3; i++)
      for (int j=0; j<3; j++){
        dAlphadT(i,j) = dXdT(i,j)*1/(pow(kk,2)*Hb) - X(i,j)*1/(pow(kk*Hb,2))*dHbdT;
        ddAlphadTT(i,j) = ddXdTT(i,j)*1/(pow(kk,2)*Hb) - 2*dXdT(i,j)*1/(pow(kk*Hb,2))*dHbdT
                        - X(i,j)*( -2/(pow(kk*Hb,3))*dHbdT*dHbdT +  1/(pow(kk*Hb,2))*ddHbddT);
      }

    // finally
    STensorOperation::doubleContractionSTensor3(X,alpha,psiVP);
    psiVP *= 0.5;
    psiVP += 0.5*( 1/_HR[0]*R1*a(1)*sigc + 1/_HR[1]*pow(R2,2) + 1/_HR[2]*pow(R3,2) );

    for (int i=0; i<3; i++)
      for (int j=0; j<3; j++){
        DpsiVPdT += 0.5*(dXdT(i,j)*alpha(i,j)+X(i,j)*dAlphadT(i,j));
        DDpsiVPdTT += 0.5*(ddXdTT(i,j)*alpha(i,j) + 2*dXdT(i,j)*dAlphadT(i,j) + X(i,j)*ddAlphadTT(i,j));
      }
    DpsiVPdT += 0.5*( 1/_HR[0]*(dR1dT*a(1)*sigc + R1*(DaDT(1)*sigc + a(1)*dsigcdT)) + 2/_HR[1]*R2*dR2dT + 2/_HR[2]*R3*dR3dT );

    DDpsiVPdTT += 0.5*( 1/_HR[0]*(ddR1dTT*a(1)*sigc + 2*dR1dT*(DaDT(1)*sigc + a(1)*dsigcdT) + R1*(DDaDTT(1)*sigc + 2*DaDT(1)*dsigcdT + a(1)*ddsigcdTT))
                + 2/_HR[1]*R2*ddR2dTT + 2/_HR[2]*R3*ddR3dTT  + 2/_HR[1]*dR2dT*dR2dT + 2/_HR[2]*dR3dT*dR3dT); // CHANGE!!  2nd derivative of R and X are difficult

    //
    // psiVE

    double KT, GT, cteT, dKdT, ddKdTT, dGdT, ddGdTT, DcteDT, DDcteDTT; getK(q1,KT,T,&dKdT,&ddKdTT); getG(q1,GT,T,&dGdT,&ddGdTT); getAlpha(cteT,T,&DcteDT,&DDcteDTT);

    const STensor3& E = q1->getConstRefToElasticStrain();
    static STensor3 devKeinf, DdevKeinfDT, DDdevKeinfDTT, devEe;
    static double trKeinf, DtrKeinfDT, DDtrKeinfDTT, trEe;
    STensorOperation::decomposeDevTr(E,devEe,trEe);
    mlawNonLinearTVM::corKirInfinity(q1,devEe,trEe,T,devKeinf,trKeinf);

    DtrKeinfDT = trKeinf*dKdT/KT - 3*KT*(DcteDT*(T-_Tinitial) + cteT);
    DDtrKeinfDTT = trKeinf*ddKdTT/KT - 6*dKdT*(DcteDT*(T-_Tinitial) + cteT)- 3*KT*(DDcteDTT*(T-_Tinitial) + 2*DcteDT);
    DdevKeinfDT = devKeinf;
    DdevKeinfDT *= dGdT/GT;
    DDdevKeinfDTT = devKeinf;
    DDdevKeinfDTT *= ddGdTT/GT;

    /*
    const std::vector<STensor3>& devOi = q1->getConstRefToDevViscoElasticOverStress();
    const std::vector<double>& trOi = q1->getConstRefToTrViscoElasticOverStress();
    const std::vector<STensor3>& devDOiDT = q1->getConstRefToDevDOiDT();
    const std::vector<double>& trDOiDT = q1->getConstRefToTrDOiDT();
    const std::vector<STensor3>& devDDOiDTT = q1->getConstRefToDevDDOiDTT();
    const std::vector<double>& trDDOiDTT = q1->getConstRefToTrDDOiDTT();

    psiVE = mlawNonLinearTVE::freeEnergyMechanical(*q0,*q1,T0,T);

    DpsiVEdT = 1/9 * (1/KT * trKeinf * DtrKeinfDT - 1/(2*pow(KT,2)) * dKdT * pow(trKeinf,2));
    DDpsiVEdTT = 1/9 * (1/KT * (pow(DtrKeinfDT,2)+trKeinf*DDtrKeinfDTT) - 1/pow(KT,2)*dKdT*trKeinf*DtrKeinfDT
                        - 1/(2*pow(KT,2)) *(ddKdTT*pow(trKeinf,2) + 2*dKdT*trKeinf*DtrKeinfDT)
                        + 1/pow(KT,3)*pow(dKdT,2)*pow(trKeinf,2));
    for (int i=0; i<3; i++)
      for (int j=0; j<3; j++){
        DpsiVEdT += 2/GT * devKeinf(i,j)*DdevKeinfDT(i,j) - 1/pow(GT,2) * dGdT * devKeinf(i,j)*devKeinf(i,j);
        DDpsiVEdTT += 2/GT*(DdevKeinfDT(i,j)*DdevKeinfDT(i,j) + devKeinf(i,j)*DDdevKeinfDTT(i,j) - dGdT*devKeinf(i,j)*DdevKeinfDT(i,j))
                      - 1/pow(GT,2) * (ddGdTT*devKeinf(i,j)*devKeinf(i,j) + 2*dGdT*devKeinf(i,j)*DdevKeinfDT(i,j));
      }
    if ((_Ki.size() > 0) or (_Gi.size() > 0)){
      std::vector<double> KiT, GiT; KiT.resize(_N,0.); GiT.resize(_N,0.);
      getKi(q1,KiT,T); getGi(q1,GiT,T);
      for (int k=0; k<_Gi.size(); k++){
        DpsiVEdT += 1/9 * 1/KiT[k] * trOi[k] * trDOiDT[k];
        DDpsiVEdTT += 1/9 * 1/KiT[k] * (trOi[k]*trDDOiDTT[k] + pow(trDOiDT[k],2));
        for (int i=0; i<3; i++)
          for (int j=0; j<3; j++){
            DpsiVEdT += 2/GiT[k] * devOi[k](i,j) * devDOiDT[k](i,j);
            DDpsiVEdTT += 2/GiT[k] * (devOi[k](i,j) * devDDOiDTT[k](i,j) + pow(devDOiDT[k](i,j),2));
          }
        }
    }

    // FINALLY
    if(psiTVM!=NULL){
        *psiTVM = psiT + psiVE + psiVP;
    }
    if(DpsiTVMdT!=NULL){
        *DpsiTVMdT = DpsiTdT + DpsiVEdT + DpsiVPdT;
    }
    if(DDpsiTVMdTT!=NULL){
        *DDpsiTVMdTT = DpsiTdTT + DDpsiVEdTT + DDpsiVPdTT;
    }
*/
};

void mlawNonLinearTVP::getIsotropicHardeningForce(const IPNonLinearTVP *q0, IPNonLinearTVP *q1, const double T0, const double& T,
                                      double* ddRdTT, STensor3* ddRdFdT) const{

    const double& DgammaDT = q1->_DgammaDT;
    const STensor3& DgammaDF = q1->_DgammaDF;

    // Initialize
    double& R = q1->_IsoHardForce_simple;
    double& dRdT = q1->_dIsoHardForcedT_simple;
    STensor3& dRdF = q1->_dIsoHardForcedF_simple;

    // Get Hc, etc.
    double sigc0(0.), dsigc0dT(0.), ddsigc0dTT(0.);
    double sigc(0.), Hc(0.), dHcdT(0.), dsigcdT(0.), ddsigcdTT(0.);
    sigc0 = q1->_ipCompression->getR0();         // Initial yield stress
    dsigc0dT = sigc0*_temFunc_Sy0->getDiff(T);
    ddsigc0dTT = sigc0*_temFunc_Sy0->getDoubleDiff(T);
    sigc = q1->_ipCompression->getR();
    Hc = q1->_ipCompression->getDR();
    dHcdT = q1->_ipCompression->getDDRDT();
    dsigcdT = q1->_ipCompression->getDRDT();
    ddsigcdTT = q1->_ipCompression->getDDRDTT();

    // Get R
    R = sigc - sigc0;

    // dRdT
    dRdT = dsigcdT - dsigc0dT;
    dRdT += Hc*q1->_DgammaDT;

    // dRdF
    STensorOperation::zero(dRdF);
    for (int i=0; i<3; i++)
      for (int j=0; j<3; j++)
        dRdF(i,j) = Hc*DgammaDF(i,j);


    // ddRdTT
    if(ddRdTT!=NULL){
       *ddRdTT =  (ddsigcdTT - ddsigc0dTT) + dHcdT*DgammaDT; // DDgammaDTT = 0.
    }

    // ddRdTF
    if(ddRdFdT!=NULL){
        static STensor3 _ddRdFdT;
        STensorOperation::zero(_ddRdFdT);

        for (int i=0; i<3; i++)
          for (int j=0; j<3; j++)
            _ddRdFdT(i,j) = dHcdT*DgammaDF(i,j); // DDgammaDFDT = 0.

        *ddRdFdT = _ddRdFdT;
    }
};

void mlawNonLinearTVP::getMechSourceTVP(const STensor3& F0, const STensor3& F,
                                        const IPNonLinearTVP *q0, IPNonLinearTVP *q1, const double T0, const double& T, const STensor3& Fepr,  const STensor3& Cepr,
                                        const STensor3& DphiPDF, double& mechSourceTVP, STensor3& dmechSourceTVPdF, double& dmechSourceTVPdT) const{

    const double& DgammaDT = q1->_DgammaDT;
    const STensor3& DgammaDF = q1->_DgammaDF;
    const double& gamma0 = q0->_epspbarre;
    const double& gamma1 = q1->_epspbarre;
    const STensor3& Fp1 = q1->_Fp;
    const STensor3& Me = q1->_kirchhoff; // q1->_ModMandel; // debug
    const STensor3& X = q1->_backsig;
    const STensor3& dXdT = q1->_DbackSigDT;
    const STensor43& dXdF  =  q1->_DbackSigDF;
    const STensor3& dMeDT  = q1->_DModMandelDT;
    const STensor43& dMedF  = q1->_DModMandelDF;
    const STensor43& dGammaNdF = q1->_dGammaNdF;
    const STensor3& dGammaNdT = q1->_dGammaNdT;
    const STensor43& DphiDF = q1->_DphiDF;
    const STensor3& DphiDT = q1->_DphiDT;

    // get Dgamma
    double Dgamma = gamma1 - gamma0;

    // get Dp - dont use Dp = Fp_dot*Fpinv, USE Dp = Gamma*N/this->getTimeStep()
    const STensor3& Dp = q1->_GammaN;

    // get TempX - convenient backStress tensor
    static STensor3 tempX;
    tempX = X;
    // tempX -= T*dXdT;

    // get R, dRdT, dRdF
    double R(0.), dRdT(0.), ddRdTT(0.);
    static STensor3 dRdF, ddRdFdT;
    STensorOperation::zero(dRdF); STensorOperation::zero(ddRdFdT);

    // New Formulation
    getIsotropicHardeningForce(q0,q1,T0,T,&ddRdTT,&ddRdFdT);
    R = q1->_IsoHardForce_simple;
    dRdT = q1->_dIsoHardForcedT_simple;
    dRdF = q1->_dIsoHardForcedF_simple;

    // for convenience
    // R -= T*dRdT;

    // mechSourceTVP
    mechSourceTVP = 0.;
    if (this->getTimeStep() > 0){
      mechSourceTVP += STensorOperation::doubledot(Me,Dp);
      mechSourceTVP -= STensorOperation::doubledot(X,Dp); // dot_alpha = Dp
      mechSourceTVP *= 1/this->getTimeStep();

      if (!_TaylorQuineyFlag){
          mechSourceTVP -= (R*Dgamma)/this->getTimeStep();
      }
    }

    // dDpdF and dDpdT
    // dDpdF = dGammaNdF/this->getTimeStep();
    // dDpdT = dGammaNdT/this->getTimeStep();


    STensorOperation::zero(dmechSourceTVPdF);
    dmechSourceTVPdT = 0.;
    if (this->getTimeStep() > 0){
     for (int i=0; i<3; i++)
       for (int j=0; j<3; j++)
         for (int k=0; k<3; k++)
           for (int l=0; l<3; l++){
          	 dmechSourceTVPdF(k,l) += DphiDF(i,j,k,l)*Dp(i,j) + (Me(i,j)-X(i,j))*dGammaNdF(i,j,k,l)/this->getTimeStep();
          	 // dmechSourceTVPdF(k,l) += dMedF(i,j,k,l)*Dp(i,j) + Me(i,j)*dGammaNdF(i,j,k,l)/this->getTimeStep();
             // dmechSourceTVPdF(k,l) += - (dXdF(i,j,k,l) - T*ddXdTdF(i,j,k,l))*Dp(i,j) - tempX(i,j)*dGammaNdF(i,j,k,l)/this->getTimeStep();
             // dmechSourceTVPdF(k,l) += - (dXdF(i,j,k,l))*Dp(i,j) - tempX(i,j)*dGammaNdF(i,j,k,l)/this->getTimeStep();
           }

     for (int i=0; i<3; i++)
       for (int j=0; j<3; j++){
          	dmechSourceTVPdT += DphiDT(i,j)*Dp(i,j) + (Me(i,j)-X(i,j))*dGammaNdT(i,j)/this->getTimeStep();
         // dmechSourceTVPdT += dMeDT(i,j)*Dp(i,j) + Me(i,j)*dGammaNdT(i,j)/this->getTimeStep();
         // dmechSourceTVPdT += T*ddXdTT(i,j)*Dp(i,j) - tempX(i,j)*dGammaNdT(i,j)/this->getTimeStep();// old
         // dmechSourceTVPdT -= dXdT(i,j)*Dp(i,j) - tempX(i,j)*dGammaNdT(i,j)/this->getTimeStep();

       }

     if (!_TaylorQuineyFlag){
      for (int i=0; i<3; i++)
        for (int j=0; j<3; j++){
        	// dmechSourceTVPdF(i,j) += - (dRdF(i,j) - T*ddRdFdT(i,j))*Dgamma/this->getTimeStep() - R*DgammaDF(i,j)/this->getTimeStep(); // old
        	dmechSourceTVPdF(i,j) += - dRdF(i,j)*Dgamma/this->getTimeStep() - R*DgammaDF(i,j)/this->getTimeStep(); // new
        }

      // dmechSourceTVPdT += T*ddRdTT*Dgamma/this->getTimeStep() - R*DgammaDT/this->getTimeStep(); // old
      dmechSourceTVPdT += - dRdT*Dgamma/this->getTimeStep() - R*DgammaDT/this->getTimeStep(); // new
     }
     else{
      dmechSourceTVPdF *= _TaylorQuineyFactor;
      dmechSourceTVPdT *= _TaylorQuineyFactor;
     }
    }

    double debug_CHECKER = 0.;
};


void mlawNonLinearTVP::constitutive( // This function is just a placeholder, defined in the pure virtual class -> does nothing, its never called.
                            const STensor3& F0,         // previous deformation gradient (input @ time n)
                            const STensor3& F1,         // current deformation gradient (input @ time n+1)
                                  STensor3 &P1,                // current 1st Piola-Kirchhoff stress tensor (output)
                            const IPVariable *q0i,       // array of previous internal variables
                                  IPVariable *q1i,             // current array of internal variable (in ipvcur on output),
                                  STensor43 &Tangent,         // tangents (output)
                            const bool stiff,            // if true compute the tangents
                                  STensor43* elasticTangent,
                            const bool dTangent,
                                  STensor63* dCalgdeps) const{
    static SVector3 gradT, temp2;
    static STensor3 temp3;
    static STensor33 temp33;
    static double tempVal;
    static STensor43 dFpdF, dFedF;
    static STensor3 dFpdT, dFedT;

    const IPNonLinearTVP *q0 = dynamic_cast<const IPNonLinearTVP *> (q0i);
          IPNonLinearTVP *q1 = dynamic_cast<IPNonLinearTVP *>(q1i);

    if(elasticTangent != NULL) Msg::Error("mlawNonLinearTVP elasticTangent not defined");
    predictorCorrector_ThermoViscoPlastic(F0,F1,P1,q0,q1,Tangent,dFpdF, dFpdT, dFedF, dFedT,
                                          _Tref,_Tref,gradT,gradT,temp2,temp3,temp3,temp2,temp33,
                                          tempVal,tempVal,temp3,
                                          tempVal,tempVal,temp3,
                                          stiff, NULL);
};

void mlawNonLinearTVP::constitutive(
                            const STensor3& F0,                     // initial deformation gradient (input @ time n)
                            const STensor3& F1,                     // updated deformation gradient (input @ time n+1)
                                  STensor3& P,                      // updated 1st Piola-Kirchhoff stress tensor (output)
                            const IPVariable *q0i,                   // array of initial internal variable
                                  IPVariable *q1i,                   // updated array of internal variable (in ipvcur on output),
                                  STensor43 &Tangent,               // constitutive mechanical tangents (output)
                            const double& T0,                       // previous temperature
                            const double& T,                        // temperature
                            const SVector3 &gradT0,                 // previous temperature gradient
                            const SVector3 &gradT,                  // temperature gradient
                                  SVector3 &fluxT,                  // temperature flux
                                  STensor3 &dPdT,                   // mechanical-thermal coupling
                                  STensor3 &dfluxTdgradT,           // thermal tengent
                                  SVector3 &dfluxTdT,
                                  STensor33 &dfluxTdF,              // thermal-mechanical coupling
                                  double &thermalSource,            // - Cp*dTdt
                                  double &dthermalSourcedT,         // thermal source
                                  STensor3 &dthermalSourcedF,
                                  double &mechanicalSource,         // mechanical source--> convert to heat
                                  double &dmechanicalSourcedT,
                                  STensor3 &dmechanicalSourceF,
                            const bool stiff,
                                  STensor43* elasticTangent) const{

    const IPNonLinearTVP *q0 = dynamic_cast<const IPNonLinearTVP *>(q0i);
          IPNonLinearTVP *q1 = dynamic_cast<IPNonLinearTVP *>(q1i);

    // Declaring here, coz I dont know better.
    static STensor43 dFedF, dFpdF;
    static STensor3 dFpdT, dFedT;

    if (!_tangentByPerturbation){

        this->predictorCorrector_ThermoViscoPlastic(F0, F1, P, q0, q1, Tangent, dFpdF, dFpdT, dFedF, dFedT,
                                                    T0, T, gradT0, gradT, fluxT, dPdT, dfluxTdgradT, dfluxTdT, dfluxTdF,
                                                    thermalSource, dthermalSourcedT, dthermalSourcedF,
                                                    mechanicalSource, dmechanicalSourcedT, dmechanicalSourceF,
                                                    stiff, elasticTangent);
    }

    else{

        this->predictorCorrector_ThermoViscoPlastic(F0, F1, P, q0, q1, T0, T, gradT0, gradT, fluxT, thermalSource, mechanicalSource, elasticTangent);
        if (stiff)
            this->tangent_full_perturbation(F0,F1,P,q0,q1,T0, T, gradT0, gradT, fluxT, thermalSource, mechanicalSource,
                                            Tangent, dFpdF, dFpdT, dFedF, dFedT,
                                            dPdT, dfluxTdgradT, dfluxTdT, dfluxTdF,
                                            dthermalSourcedT, dthermalSourcedF,
                                            dmechanicalSourcedT, dmechanicalSourceF);
    }
};


void mlawNonLinearTVP::predictorCorrector_ThermoViscoPlastic(
                                                    const STensor3& F0,         // initial deformation gradient (input @ time n)
                                                    const STensor3& F1,         // updated deformation gradient (input @ time n+1)
                                                          STensor3 &P,                // updated 1st Piola-Kirchhoff stress tensor (output)
                                                    const IPNonLinearTVP *q0,       // array of initial internal variable
                                                          IPNonLinearTVP *q1,             // updated array of internal variable (in ipvcur on output),
                                                    const double& T0, // previous temperature
                                                    const double& T, // temperature
                                                    const SVector3 &gradT0, // previous temeprature gradient
                                                    const SVector3 &gradT, // temeprature gradient
                                                          SVector3 &fluxT, // temperature flux)
                                                          double &thermalSource,
                                                          double& mechanicalSource,
                                                          STensor43* elasticTangent) const{
  // temp variables
  static STensor43 Tangent, dFpdF, dFedF;
  static STensor3 dPdT, dfluxTdgradT, dthermalSourcedF, dmechanicalSourceF, dFpdT, dFedT;
  static STensor33 dfluxTdF;
  static SVector3 dfluxTdT;
  static double dthermalSourcedT, dmechanicalSourcedT;
  predictorCorrector_ThermoViscoPlastic(F0,F1,P,q0,q1,Tangent,dFpdF,dFpdT,dFedF,dFedT,
                                        T0,T,gradT0,gradT,fluxT,dPdT,dfluxTdgradT,dfluxTdT,dfluxTdF,
                                        thermalSource,dthermalSourcedT,dthermalSourcedF,
                                        mechanicalSource,dmechanicalSourcedT,dmechanicalSourceF,false,elasticTangent);
};


void mlawNonLinearTVP::tangent_full_perturbation(
                            const STensor3& F0,
                            const STensor3& F1,         // updated deformation gradient (input @ time n+1)
                                  STensor3& P,                // updated 1st Piola-Kirchhoff stress tensor (output)
                            const IPNonLinearTVP *q0,       // array of initial internal variable
                                  IPNonLinearTVP *q1,             // updated array of internal variable (in ipvcur on output),
                            const double& T0, // previous temperature
                            const double& T, // temperature
                            const SVector3 &gradT0, // previous temeprature gradient
                            const SVector3 &gradT, // temeprature gradient
                                  SVector3 &fluxT, // temperature flux)
                                  double &thermalSource,
                                  double &mechanicalSource,
                                  STensor43 &Tangent,         // mechanical tangents (output)
                                  STensor43 &dFpdF, // plastic tangent
                                  STensor3 &dFpdT, // plastic tangent
                                  STensor43 &dFedF, // elastic tangent
                                  STensor3 &dFedT, // elastic tangent
                                  STensor3 &dPdT, // mechanical-thermal coupling
                                  STensor3 &dfluxTdgradT, // thermal tengent
                                  SVector3 &dfluxTdT,
                                  STensor33 &dfluxTdF, // thermal-mechanical coupling
                                  double &dthermalSourcedT, // thermal source
                                  STensor3 &dthermalSourcedF,
                                  double &dmechanicalSourcedT,
                                  STensor3 &dmechanicalSourceF) const{

    static STensor3 Fplus, Pplus;
    static SVector3 fluxTPlus, gradTplus;
    static double thermalSourcePlus;
    static double mechanicalSourcePlus;
    static IPNonLinearTVP qPlus(*q0);

    // perturb F
    for (int i=0; i<3; i++){
      for (int j=0; j<3; j++){
        Fplus = F1;
        Fplus(i,j) += _perturbationfactor;

        predictorCorrector_ThermoViscoPlastic(F0,Fplus,Pplus,q0,&qPlus,T0,T,gradT0,gradT,fluxTPlus,thermalSourcePlus,mechanicalSourcePlus, NULL);

        q1->_DgammaDF(i,j) = (qPlus._epspbarre - q1->_epspbarre)/_perturbationfactor;
        q1->_DirreversibleEnergyDF(i,j) = (qPlus._irreversibleEnergy - q1->_irreversibleEnergy)/_perturbationfactor;

        for (int k=0; k<3; k++){
          for (int l=0; l<3; l++){
            Tangent(k,l,i,j) = (Pplus(k,l) - P(k,l))/(_perturbationfactor);
            dFpdF(k,l,i,j) = (qPlus._Fp(k,l)-q1->_Fp(k,l))/_perturbationfactor;
            dFedF(k,l,i,j) = (qPlus._Fe(k,l)-q1->_Fe(k,l))/_perturbationfactor;
          }
          dfluxTdF(k,i,j) = (fluxTPlus(k) - fluxT(k))/_perturbationfactor;
        }

        q1->getRefToDElasticEnergyPartdF()(i,j)=(qPlus.getConstRefToElasticEnergyPart()-q1->getConstRefToElasticEnergyPart())/_perturbationfactor;
        q1->getRefToDViscousEnergyPartdF()(i,j)=(qPlus.getConstRefToViscousEnergyPart()-q1->getConstRefToViscousEnergyPart())/_perturbationfactor;
        q1->getRefToDPlasticEnergyPartdF()(i,j)=(qPlus.getConstRefToPlasticEnergyPart()-q1->getConstRefToPlasticEnergyPart())/_perturbationfactor;

        dthermalSourcedF(i,j) = (thermalSourcePlus - thermalSource)/_perturbationfactor;
        dmechanicalSourceF(i,j) = (mechanicalSourcePlus - mechanicalSource)/_perturbationfactor;
      }
    }

    // perturb gradT
    double gradTpert = _perturbationfactor*T0/1e-3;
    for (int i=0; i<3; i++){
      gradTplus = gradT;
      gradTplus(i) += gradTpert;
      predictorCorrector_ThermoViscoPlastic(F0,F1,Pplus,q0,&qPlus,T0,T,gradT0,gradTplus,fluxTPlus,thermalSourcePlus,mechanicalSourcePlus, NULL);
      for (int k=0; k<3; k++){
        dfluxTdgradT(k,i) = (fluxTPlus(k) - fluxT(k))/gradTpert;
      }
    }

    // perturb T
    double Tplus = T+ _perturbationfactor*T0;
    predictorCorrector_ThermoViscoPlastic(F0,F1,Pplus,q0,&qPlus,T0,Tplus,gradT0,gradT,fluxTPlus,thermalSourcePlus,mechanicalSourcePlus, NULL);

    q1->_DgammaDT = (qPlus._epspbarre - q1->_epspbarre)/(_perturbationfactor*T0);
    q1->_DirreversibleEnergyDT = (qPlus._irreversibleEnergy - q1->_irreversibleEnergy)/(_perturbationfactor*T0);

    for (int k=0; k<3; k++){
      for (int l=0; l<3; l++){
        dFpdT(k,l) = (qPlus._Fp(k,l) - q1->_Fp(k,l))/(_perturbationfactor*T0);
        dFedT(k,l) = (qPlus._Fe(k,l) - q1->_Fe(k,l))/(_perturbationfactor*T0);
        dPdT(k,l) = (Pplus(k,l) - P(k,l))/(_perturbationfactor*T0);
      }
      dfluxTdT(k) = (fluxTPlus(k) - fluxT(k))/(_perturbationfactor*T0);
    }

    q1->getRefToDElasticEnergyPartdT() = (qPlus.getConstRefToElasticEnergyPart()-q1->getConstRefToElasticEnergyPart())/(_perturbationfactor*T0);
    q1->getRefToDViscousEnergyPartdT() = (qPlus.getConstRefToViscousEnergyPart()-q1->getConstRefToViscousEnergyPart())/(_perturbationfactor*T0);
    q1->getRefToDPlasticEnergyPartdT() = (qPlus.getConstRefToPlasticEnergyPart()-q1->getConstRefToPlasticEnergyPart())/(_perturbationfactor*T0);

    dthermalSourcedT = (thermalSourcePlus - thermalSource)/(_perturbationfactor*T0);
    dmechanicalSourcedT = (mechanicalSourcePlus - mechanicalSource)/(_perturbationfactor*T0);

};

void mlawNonLinearTVP::predictorCorrector_ThermoViscoPlastic(
                                                    const STensor3& F0,         // initial deformation gradient (input @ time n)
                                                    const STensor3& F1,         // updated deformation gradient (input @ time n+1)
                                                          STensor3& P,                // updated 1st Piola-Kirchhoff stress tensor (output)
                                                    const IPNonLinearTVP *q0,       // array of initial internal variable
                                                          IPNonLinearTVP *q1,             // updated array of internal variable (in ipvcur on output),
                                                          STensor43 &Tangent,         // mechanical tangents (output)
                                                          STensor43 &dFpdF, // plastic tangent
                                                          STensor3 &dFpdT, // plastic tangent
                                                          STensor43 &dFedF, // elastic tangent
                                                          STensor3 &dFedT, // elastic tangent
                                                    const double& T0, // previous temperature
                                                    const double& T, // temperature
                                                    const SVector3 &gradT0, // previoustemeprature gradient
                                                    const SVector3 &gradT, // temeprature gradient
                                                          SVector3 &fluxT, // temperature flux
                                                          STensor3 &dPdT, // mechanical-thermal coupling
                                                          STensor3 &dfluxTdgradT, // thermal tengent
                                                          SVector3 &dfluxTdT,
                                                          STensor33 &dfluxTdF, // thermal-mechanical coupling
                                                          double &thermalSource,   // - cp*dTdt
                                                          double &dthermalSourcedT, // thermal source
                                                          STensor3 &dthermalSourcedF,
                                                          double &mechanicalSource, // mechanical source--> convert to heat
                                                          double &dmechanicalSourcedT,
                                                          STensor3 &dmechanicalSourceF,
                                                    const bool stiff,
                                                          STensor43* elasticTangent) const{

  if (_tangentByPerturbation){
    if (_nonAssociatedFlow){
      this->predictorCorrector_TVP_nonAssociatedFlow(F0,F1,q0,q1,P,false,Tangent,dFedF,dFpdF,dFedT,dFpdT,
                                                        T0,T,gradT0,gradT,fluxT,dPdT,dfluxTdgradT,dfluxTdT,dfluxTdF,
                                                        thermalSource,dthermalSourcedT,dthermalSourcedF,
                                                        mechanicalSource,dmechanicalSourcedT,dmechanicalSourceF);
    }
    else{
      this->predictorCorrector_TVP_AssociatedFlow(F1,q0,q1,P,false,Tangent,dFedF,dFpdF,T0,T);
    }

//    if (stiff){                                                                      // WHAT?????  WHY TWICE? (The same line happens above as STIFF is always TRUE)
//      tangent_full_perturbation(Tangent,dFedF,dFpdF,P,F,q0,q1);
//    }

  }
  else{
    if (_nonAssociatedFlow){
      this->predictorCorrector_TVP_nonAssociatedFlow(F0,F1,q0,q1,P,stiff,Tangent,dFedF,dFpdF,dFedT,dFpdT,
                                                        T0,T,gradT0,gradT,fluxT,dPdT,dfluxTdgradT,dfluxTdT,dfluxTdF,
                                                        thermalSource,dthermalSourcedT,dthermalSourcedF,
                                                        mechanicalSource,dmechanicalSourcedT,dmechanicalSourceF);
    }
    else{
      this->predictorCorrector_TVP_AssociatedFlow(F1,q0,q1,P,stiff,Tangent,dFedF,dFpdF,T0,T);
    }
  }

};

void mlawNonLinearTVP::predictorCorrector_TVP_nonAssociatedFlow(const STensor3& F0, const STensor3& F, const IPNonLinearTVP *q0, IPNonLinearTVP *q1,
                            STensor3&P, const bool stiff, STensor43& Tangent, STensor43& dFedF, STensor43& dFpdF, STensor3& dFedT, STensor3& dFpdT,
                            const double T0,
                            const double T,
                            const SVector3 &gradT0,                 // previous temperature gradient
                            const SVector3 &gradT,                  // temperature gradient
                                  SVector3 &fluxT,                  // temperature flux
                                  STensor3 &dPdT,                   // mechanical-thermal coupling
                                  STensor3 &dfluxTdgradT,           // thermal tengent
                                  SVector3 &dfluxTdT,
                                  STensor33 &dfluxTdF,              // thermal-mechanical coupling
                                  double &thermalSource,            // - Cp*dTdt
                                  double &dthermalSourcedT,         // thermal source
                                  STensor3 &dthermalSourcedF,
                                  double &mechanicalSource,         // mechanical source--> convert to heat
                                  double &dmechanicalSourcedT,
                                  STensor3 &dmechanicalSourceF) const{

  // compute elastic predictor
  STensor3& Fp1 = q1->_Fp;
  const STensor3& Fp0 = q0->_Fp;

  // Update the Properties to the current temperature (see below which ones are being used)         
  double CpT, DCpDT; getCp(CpT,T,&DCpDT); 
    
  // Initialise predictor values
  Fp1 = Fp0; // plastic deformation tensor
  q1->_epspbarre = q0->_epspbarre; // plastic equivalent strain
  q1->_epspCompression = q0->_epspCompression;
  q1->_epspTraction = q0->_epspTraction;
  q1->_epspShear = q0->_epspShear;
  q1->_backsig = q0->_backsig; // backstress tensor
  q1->_DbackSigDT = q0->_DbackSigDT; // DbackSigDT
  q1->_DgammaDt = 0.;

  // Get hardening parameters
  this->hardening(q0,q1,T);
  static fullVector<double> a(3), Da(3); // yield coefficients and derivatives in respect to plastic deformation
  this->getYieldCoefficients(q1,a);
  //a.print("a init");

  double Hb(0.), dHb(0.), ddHb(0.), dHbdT(0.), ddHbdgammadT(0.), ddHbddT(0.);
  if (q1->_ipKinematic != NULL){
    Hb = q1->_ipKinematic->getR(); // kinematic hardening parameter
    dHb = q1->_ipKinematic->getDR(); // kinematic hardening parameter derivative (dHb/dgamma)
    dHbdT = q1->_ipKinematic->getDRDT();  // make sure to normalise DRDT while defining in python file //NEW
    ddHbddT = q1->_ipKinematic->getDDRDTT();
  }

  double eta(0.),Deta(0.),DetaDT(0.);
  if (_viscosity != NULL)
    _viscosity->get(q1->_epspbarre,T,eta,Deta,DetaDT);

  // Get Cepr, Ceinvpr
  static STensor3 Fpinv, Ce, Fepr;
  STensorOperation::inverseSTensor3(Fp1,Fpinv);
  STensorOperation::multSTensor3(F,Fpinv,Fepr);
  STensorOperation::multSTensor3FirstTranspose(Fepr,Fepr,Ce);

  static STensor3 Eepr, Cepr, Ceinvpr;
  Cepr = Ce;
  STensorOperation::inverseSTensor3(Cepr,Ceinvpr);

  static STensor3 invFp0; // plastic predictor
  invFp0= Fpinv;
  STensor3& Fe = q1->_Fe;
  Fe = Fepr;

  // Predictor - TVE
  static STensor43 DlnDCepr, DlnDCe;
  static STensor63 DDlnDDCe;
  static STensor3 expGN, GammaN;
  static STensor43 dexpAdA; // estimation of dexpA/dA
  expGN = _I; // if Gamma = 0, expGN = _I
  dexpAdA = _I4;

  STensor3& Ee = q1->_Ee;
  STensorOperation::logSTensor3(Ce,_order,Ee,&DlnDCepr,&DDlnDDCe);
  Ee *= 0.5;
  Eepr = Ee;
  DlnDCe = DlnDCepr;

  // update A, B
  double Ke, Ge = 0.; double Kde, Gde = 0.; double KTsum, GTsum = 0.;
  double DKe, DGe = 0.; double DKde, DGde = 0.; double DKDTsum, DGDTsum = 0.;
  static STensor43 Bd_stiffnessTerm;
  // ThermoViscoElasticPredictor(Ee,q0->_Ee,q0,q1,Ke,Ge,Kde,Gde,DKe,DGe,DKde,DGde,KTsum,GTsum,DKDTsum,DGDTsum,T0,T);
  mlawNonLinearTVM::ThermoViscoElasticPredictor(Ee,q0->_Ee,q0,q1,Ke,Ge,T0,T,stiff,Bd_stiffnessTerm);
  mlawNonLinearTVM::getTVEdCorKirDT(q0,q1,T0,T);

  // additional branch - update predictor
  double DsigV_dTrEe_pr(0.), DsigV_dTrEe(0.), DsigV_dT(0.);
  static STensor3 sigExtra_pr, sigExtra, DsigD_dT;
  static STensor43 DsigD_dDevEe_pr, DsigD_dDevEe;
  STensorOperation::zero(sigExtra_pr);
  STensorOperation::zero(sigExtra);
  STensorOperation::zero(DsigD_dT);
  STensorOperation::zero(DsigD_dDevEe_pr);
  STensorOperation::zero(DsigD_dDevEe);
  if (_useExtraBranch){
    mlawNonLinearTVM::extraBranchLaw(Ee,T,q0,q1,sigExtra_pr,stiff,NULL,NULL,&DsigV_dTrEe_pr,&DsigD_dDevEe_pr,&DsigV_dT,&DsigD_dT);
  }
  q1->_corKirExtra = sigExtra_pr;
  q1->_kirchhoff += sigExtra_pr;
  sigExtra = sigExtra_pr;
  DsigV_dTrEe = DsigV_dTrEe_pr;
  DsigD_dDevEe = DsigD_dDevEe_pr;
  const STensor3& dKeprDT = q1->_DcorKirDT; // Get the predicted value - Has extraBranch inside

  // Initialise Dho
  static STensor43 Dho3, Dho4inv, Dho4_u_inv;  // This will be the final important tensor.
  double Dho4_v_inv(0.);

  // Get Mepr
  static STensor3 Me, Mepr, Kepr;  // Ke is corKir
  getModifiedMandel(Ce,q0,q1);
  Me = q1->_ModMandel;
  Mepr = Me;
  Kepr = q1-> _kirchhoff;

  // Get Xn; Initialise X
  static STensor3 devXn, devX;
  static double trXn, pXn, eXn, trX;
  STensorOperation::decomposeDevTr(q0->_backsig,devXn,trXn); // needed for chaboche model
  pXn = 1./3.*trXn;  // 1st invariant of Xn
  eXn = sqrt(1.5*devXn.dotprod()); // J2 invariant of Xn

  // Initialise Phipr
  static STensor3 PhiPr, Phi;
  PhiPr = q1->_ModMandel;
  PhiPr -= q1->_backsig;
  Phi = PhiPr;

  static STensor3 devPhipr,devPhi; // effective dev stress predictor
  static double ptildepr,ptilde;
  STensorOperation::decomposeDevTr(PhiPr,devPhipr,ptildepr);
  ptildepr/= 3.;

  // Initialise Normal
  static STensor3 devN; // dev part of yield normal
  double trN = 0.; // trace part of yield normal
  static STensor3 N; // yield normal
  STensorOperation::zero(devN);
  STensorOperation::zero(N);

  // Get plastic poisson ratio
  q1->_nup = (9.-2.*_b)/(18.+2.*_b);
  double kk = 1./sqrt(1.+2.*q1->_nup*q1->_nup);

  // Initialise variables
  // double Gamma = 0.;
  double& Gamma = q1->_Gamma;   // flow rule parameter
  Gamma = 0.;
  double dDgammaDGamma = 0.;
  double Dgamma = 0.; // eqplastic strain
  static fullVector<double> m(2);
  getChabocheCoeffs(m,0,q1);

  double DfDGamma = 0.;
  double dfdDgamma(0.), dfdGamma(0.);
  double dAdDgamma(0.), dAdGamma(0.);
  double u = 1.;
  double v = 1.;

  // Initialise Cx, Gt, Kt
  double Cxdev = 1. + 3.*m(1)*pow(kk,1.)*Hb; // pow(kk,2) DEBUG
  double Cxtr = 1. + 2.*_b/3. *m(1)*pow(kk,1.)*Hb; // pow(kk,2) DEBUG
  double dCxdevdDgamma(0.), dCxtrdDgamma(0.), dudDgamma(0.), dvdDgamma(0.), dudGamma(0.), dvdGamma(0.);
  if (Hb>0.){
    Cxdev -= m(0)*dHb*Dgamma/Hb + 1./Hb * dHbdT * (T-T0);
    Cxtr -= m(0)*dHb*Dgamma/Hb + 1./Hb * dHbdT * (T-T0);
  }
  double Gt = Ge + pow(kk,1.) * Hb/(2.*Cxdev); // pow(kk,2) DEBUG
  double Kt = Ke + pow(kk,1.) * Hb/(3.*Cxtr); // pow(kk,2) DEBUG

  // Initialise ptilde and devPhi (phi)
  ptilde = ptildepr; // current effective pressure  -> first invariant
  devPhi = devPhipr;

  // Initialise the rest
  getDho3(u,v,Gamma,Cepr,Ceinvpr,Kepr,expGN,dexpAdA,Dho3,Dho4inv,Dho4_u_inv,Dho4_v_inv,&DsigV_dTrEe,&DsigD_dDevEe);

  double PhiEqpr2 = 1.5*devPhi.dotprod();
  double PhiEqpr = sqrt(PhiEqpr2);      // -> second invariant
  double PhiEq = PhiEqpr;   // current effective deviator

  // Get f and A
  double f = a(2)*pow(PhiEq,_n) - (a(1)*ptilde+a(0));
  double A = sqrt(6.*PhiEq*PhiEq+4.*_b*_b/3.*ptilde*ptilde);

  // Initialise g
  double dgdDgamma=0., dgdGamma = 0.;
  double g = Dgamma - kk*Gamma*A;

  // Test
  double Dgammma_test = 0.;

  // NR Loop - Gamma and Dgamma
    if (q1->dissipationIsBlocked()){
      q1->getRefToDissipationActive() = false;
    }
    else{
      if (f>_tol){
        q1->getRefToDissipationActive() = true;
         // plasticity
        int ite = 0;
        int maxite = 100; // maximal number of iters
        // Msg::Error("plasticity occurs f = %e",f);
        //double f0 = fabs(f);

        while (fabs(f) >_tol or ite <1){

        // Viscosity term
        double etaOverDt = eta/this->getTimeStep();

        // Get dCxdevdDgamma, dCxtrdDgamma, dudDgamma dvdDgamma, dudGamma, dvdGamma
        dCxdevdDgamma = 0.; dCxtrdDgamma = 0.;
        if (Hb>0.){
          dCxdevdDgamma = -m(0)*1./Hb*dHb - m(0)*Dgamma*(-1./pow(Hb,2.)*pow(dHb,2.) + 1./Hb*ddHb) - (T-T0)*(-1./pow(Hb,2.)*dHb*dHbdT + 1./Hb*ddHbdgammadT);
          dCxtrdDgamma = dCxdevdDgamma;
        }
        dCxdevdDgamma += 3.*m(1)*pow(kk,1.)*dHb; // pow(kk,2.) DEBUG
        dCxtrdDgamma += (2.*_b/3.)*m(1)*pow(kk,1.)*dHb; // pow(kk,2.) DEBUG

        dudDgamma = 6.*Gamma*( pow(kk,1.)/(2.*Cxdev)*dHb - (pow(kk,1.)*Hb)/(2.*pow(Cxdev,2.))*dCxdevdDgamma ); // pow(kk,2.) DEBUG
        dvdDgamma = 2.*_b*Gamma*( pow(kk,1.)/(3.*Cxtr)*dHb - (pow(kk,1.)*Hb)/(3.*pow(Cxtr,2.))*dCxtrdDgamma ); // pow(kk,2.) DEBUG

        dudGamma = 6.*(Ge + pow(kk,1.)*Hb/(2.*Cxdev)); // pow(kk,2.) DEBUG
        dvdGamma = 2.*_b*(Ke + pow(kk,1.)*Hb/(3.*Cxtr)); // pow(kk,2.) DEBUG

        // Get Hp = dPhiPdDgamma, dPhiPdGamma
        double Hp =  (1./3.*trXn/(pow(Cxtr,2.))*dCxtrdDgamma - ptilde*dvdDgamma)*Dho4_v_inv;  // /v;
        
        double dPhiPdGamma_RHS = - ptilde*dvdGamma;
        if (_useExtraBranch){
            dPhiPdGamma_RHS -= 2*_b*ptilde*DsigV_dTrEe;
        }
        double dPhiPdGamma = dPhiPdGamma_RHS*Dho4_v_inv;  // /v;

        // Get He = dPhiEdGamma
          // Get DdevPhidGamma
        STensor3 DdevPhidDgamma, DdevPhidDgamma_RHS;
        STensor3 DdevPhidGamma, DdevPhidGamma_RHS;
        for (int i=0; i<3; i++)
          for (int j=0; j<3; j++){
            DdevPhidDgamma_RHS(i,j) = devXn(i,j)/(pow(Cxdev,2.))*dCxdevdDgamma - dudDgamma*devPhi(i,j);
            DdevPhidGamma_RHS(i,j) = -dudGamma*devPhi(i,j);
            for (int k=0; k<3; k++)
              for (int l=0; l<3; l++){
                DdevPhidDgamma_RHS(i,j) += (2.*_b/3.)*Gamma*Dho3(i,j,k,l)*_I(k,l)*Hp; //ADD

                DdevPhidGamma_RHS(i,j) += (2.*_b/3.)*Gamma*Dho3(i,j,k,l)*_I(k,l)*dPhiPdGamma; //ADD

                DdevPhidGamma_RHS(i,j) += Dho3(i,j,k,l)*N(k,l);
                
                if (_useExtraBranch){
                   DdevPhidGamma_RHS(i,j) -= DsigD_dDevEe(i,j,k,l)*3*devPhi(k,l);
                }
                
              }
            }
        for (int i=0; i<3; i++)
          for (int j=0; j<3; j++){
            DdevPhidGamma(i,j) = 0.;
            DdevPhidGamma(i,j) = 0.;
            for (int k=0; k<3; k++)
              for (int l=0; l<3; l++){
                DdevPhidDgamma(i,j) += Dho4_u_inv(i,j,k,l)*DdevPhidDgamma_RHS(k,l);
                DdevPhidGamma(i,j) += Dho4_u_inv(i,j,k,l)*DdevPhidGamma_RHS(k,l);
              }
          }

        double Stemp1 = STensorOperation::doubledot(devPhi,DdevPhidDgamma);
        double Stemp2 = STensorOperation::doubledot(devPhi,DdevPhidGamma);
        double He = 1.5*Stemp1/PhiEq;
        double dPhiEdGamma = 1.5*Stemp2/PhiEq;

        // dAdGamma and dDgammaDGamma
        dAdDgamma = (12.*PhiEq*He + 8.*_b*_b*ptilde*Hp/3.)/(2.*A);
        dAdGamma = (12.*PhiEq*dPhiEdGamma + 8.*_b*_b*ptilde*dPhiPdGamma/3.)/(2.*A);

        dDgammaDGamma = kk*(A+Gamma*dAdGamma);  // mistake in the paper (VD 2016)

        this->getYieldCoefficientDerivatives(q1,q1->_nup,Da);

        dfdDgamma = Da(2)*pow(PhiEq,_n) - Da(1)*ptilde -Da(0);
        dfdDgamma += a(2)*_n*pow(PhiEq,(_n-1.))*He - a(1)*Hp;

        if (Gamma>0 and etaOverDt>0)
          dfdDgamma -= _p*pow(etaOverDt,_p-1.)*Deta/this->getTimeStep()*pow(Gamma,_p);  // THIS term is absent in the paper!! WHY??

        dfdGamma = _n*a(2)*pow(PhiEq,(_n-1.))*dPhiEdGamma - a(1)*dPhiPdGamma;
        if (Gamma>0 and etaOverDt>0)
          dfdGamma -= pow(etaOverDt,_p)*_p*pow(Gamma,(_p-1.));

        DfDGamma = dfdDgamma*dDgammaDGamma + dfdGamma;

        dgdDgamma = 1. - kk*Gamma*dAdDgamma;
        dgdGamma = - dDgammaDGamma;

        double dGamma = (-f + dfdDgamma*g/dgdDgamma)/(-dfdDgamma*dgdGamma/dgdDgamma + dfdGamma); // NR
        double dDgamma = -(g+dgdGamma*dGamma)/dgdDgamma; // NR

        if (Gamma + dGamma <=0.){
            Gamma /= 2.;                // NR
        }
        else
          Gamma += dGamma;

        // Dgammma_test += dDgamma;
        if (Dgamma + dDgamma <=0.){
            Dgamma /= 2.;
            }
        else
          Dgamma += dDgamma;

        //Msg::Error("Gamma = %e",Gamma);
        //Msg::Error("Dgamma = %e",Dgamma);

        
        // UPDATE FOR NEXT ITERATION

        // Update gamma and all Hardening moduli
        updateEqPlasticDeformation(q1,q0,q1->_nup,Dgamma);
        hardening(q0,q1,T);
        getYieldCoefficients(q1,a);
        if (q1->_ipKinematic != NULL){
            Hb = q1->_ipKinematic->getR(); // kinematic hardening parameter
            dHb = q1->_ipKinematic->getDR(); // kinematic hardening parameter derivative (dHb/dgamma ??)
            dHbdT = q1->_ipKinematic->getDRDT();  // make sure to normalise DRDT while defining in python file //NEW
            ddHbddT = q1->_ipKinematic->getDDRDTT();
        }
        //a.print("a update");

        // Update Cx, u, v
        Cxdev = 1. + 3.*m(1)*pow(kk,1.)*Hb;  // pow(kk,2) DEBUG
        Cxtr = 1. + 2.*_b/3. *m(1)*pow(kk,1.)*Hb; // pow(kk,2) DEBUG
        if (Hb>0.){
          Cxdev -= m(0)*dHb*Dgamma/Hb - 1./Hb * dHbdT * (T-T0);
          Cxtr -= m(0)*dHb*Dgamma/Hb - 1./Hb * dHbdT * (T-T0);
        }
        Gt = Ge + pow(kk,1.) * Hb/(2.*Cxdev); // pow(kk,2) DEBUG
        Kt = Ke + pow(kk,1.) * Hb/(3.*Cxtr); // pow(kk,2) DEBUG
        u = 1.+6.*Gt*Gamma;
        v = 1.+2.*_b*Kt*Gamma;

        // Update Phi
        getIterated_DPhi(T,q0,q1,u,v,Gamma,Cxtr,Cxdev,PhiPr,trXn,devXn,Cepr,Eepr,Kepr,ptilde,devPhi,Phi,N,expGN,dexpAdA,
                            Dho3,Dho4inv,Dho4_u_inv,Dho4_v_inv,
                            sigExtra_pr,sigExtra,DsigV_dTrEe,DsigD_dDevEe);
        PhiEq = sqrt(1.5*devPhi.dotprod());

        // Update A
        A = sqrt(6.*PhiEq*PhiEq+4.*_b*_b/3.*ptilde*ptilde);

        Dgamma = kk*Gamma*A;
        updateEqPlasticDeformation(q1,q0,q1->_nup,Dgamma);
        hardening(q0,q1,T);
        getYieldCoefficients(q1,a);

        // Update f, g
        f = a(2)*pow(PhiEq,_n) - (a(1)*ptilde+a(0));
        double viscoTerm = etaOverDt*Gamma;
        if (Gamma>0. and etaOverDt>0.) f-= pow(viscoTerm,_p);

        g = Dgamma - kk*Gamma*A;

        ite++;
        //if (ite> maxite-5)
         //Msg::Error("it = %d, DfDGamma = %e error = %e dGamma = %e, Gamma = %e",ite,DfDGamma,f,dGamma,Gamma);

        if (fabs(f) <_tol) break;

        if(ite > maxite){
          Msg::Error("No convergence for plastic correction in mlawNonLinearTVP nonAssociatedFlow Maxwell iter = %d, f = %e!!",ite,f);
          P(0,0) = P(1,1) = P(2,2) = sqrt(-1.);
          return;
        }
    } // while

    q1->_DgammaDt = Dgamma/this->getTimeStep();

    // Correct Cx, u, v
    Cxdev = 1. + 3.*m(1)*pow(kk,1.)*Hb;  // pow(kk,2.) DEBUG
    Cxtr = 1. + 2.*_b/3. *m(1)*pow(kk,1.)*Hb;  // pow(kk,2.) DEBUG
    if (Hb>0.){
      Cxdev -= m(0)*dHb*Dgamma/Hb - 1./Hb * dHbdT * (T-T0);
      Cxtr -= m(0)*dHb*Dgamma/Hb - 1./Hb * dHbdT * (T-T0);
    }
    Gt = Ge + pow(kk,1.) * Hb/(2.*Cxdev); // pow(kk,2.) DEBUG
    Kt = Ke + pow(kk,1.) * Hb/(3.*Cxtr); // pow(kk,2.) DEBUG
    u = 1.+6.*Gt*Gamma;
    v = 1.+2.*_b*Kt*Gamma;

    // Correct Phi
    getIterated_DPhi(T,q0,q1,u,v,Gamma,Cxtr,Cxdev,PhiPr,trXn,devXn,Cepr,Eepr,Kepr,ptilde,devPhi,Phi,N,expGN,dexpAdA,
                        Dho3,Dho4inv,Dho4_u_inv,Dho4_v_inv,
                        sigExtra_pr,sigExtra,DsigV_dTrEe,DsigD_dDevEe);
    PhiEq = sqrt(1.5*devPhi.dotprod());

    // Correct Normal, H = expGN
    STensorOperation::decomposeDevTr(N,devN,trN);

    // Get GammaN for mechSrcTVP
    STensorOperation::zero(GammaN);
    GammaN = N;
    GammaN *= Gamma;
    q1->_GammaN = GammaN;

    // Correct plastic deformation tensor
    STensorOperation::multSTensor3(expGN,Fp0,Fp1);

    // Correct IP gamma
    updateEqPlasticDeformation(q1,q0,q1->_nup,Dgamma);
          // Msg::Info("setting: gamma=%e ",q1->_epspbarre);

    // Correct elastic deformation tensor, corotational stress
    STensorOperation::inverseSTensor3(Fp1,Fpinv);
    STensorOperation::multSTensor3(F,Fpinv,Fe);
    STensorOperation::multSTensor3FirstTranspose(Fe,Fe,Ce);
    STensorOperation::logSTensor3(Ce,_order,Ee,&DlnDCe,&DDlnDDCe);
    Ee *= 0.5;

    // Correct A, B
    // ThermoViscoElasticPredictor(Ee,q0->_Ee,q0,q1,Ke,Ge,Kde,Gde,DKe,DGe,DKde,DGde,KTsum,GTsum,DKDTsum,DGDTsum,T0,T); //,false);  - DEPRECATED
    mlawNonLinearTVM::ThermoViscoElasticPredictor(Ee,q0->_Ee,q0,q1,Ke,Ge,T0,T,stiff,Bd_stiffnessTerm); //,false);
    mlawNonLinearTVM::getTVEdCorKirDT(q0,q1,T0,T); // update dCorKirdT - don't do this probably
    // DKDTsum and DGDTsum = sum of bulk/shear moduli derivatives wrt T - DEPRECATED

    // Correct Extrabranch - update stress
    if (_useExtraBranch){
        mlawNonLinearTVM::extraBranchLaw(Ee,T,q0,q1,sigExtra,stiff,NULL,NULL,&DsigV_dTrEe,&DsigD_dDevEe,&DsigV_dT,&DsigD_dT);
    }
    q1->_corKirExtra = sigExtra;
    q1->_kirchhoff += sigExtra;

    // Correct backstress;
    devX = (pow(kk,1.)*Hb*Gamma*devN + devXn); // pow(kk,2) DEBUG
    devX *= 1./Cxdev;
    trX = (pow(kk,1.)*Hb*Gamma*trN + trXn)*1./Cxtr; // pow(kk,2) DEBUG
    q1->_backsig = devX;
    q1->_backsig(0,0) += trX/3.;
    q1->_backsig(1,1) += trX/3.;
    q1->_backsig(2,2) += trX/3.;

    // Correct Mandel
    q1->_ModMandel = devPhi;
    q1->_ModMandel += q1->_backsig;
    q1->_ModMandel(0,0) += (ptilde);
    q1->_ModMandel(1,1) += (ptilde);
    q1->_ModMandel(2,2) += (ptilde);

    } // 2nd if
    else{
        q1->getRefToDissipationActive() = false;
    }
  } // 1st if


  const STensor3& KS = q1->_kirchhoff;
  getModifiedMandel(Ce, q0, q1); // update Mandel

  const STensor3& MS = q1->_ModMandel;
  Me = MS;
  // second Piola Kirchhoff stress
  static STensor3 S, Ceinv;
  STensorOperation::inverseSTensor3(Ce,Ceinv);
  STensorOperation::multSTensor3(Ceinv,q1->_ModMandel,S);

  // first PK
  for(int i=0; i<3; i++)
    for(int j=0; j<3; j++){
      P(i,j) = 0.;
      for(int k=0; k<3; k++)
        for(int l=0; l<3; l++)
          P(i,j) += Fe(i,k)*S(k,l)*Fpinv(j,l);
    }

  // defo energy

  // Elastic energy and Mullin's effect
  double& Dpsi_DT = q1->getRefTo_Dpsi_DT();
  STensor3& Dpsi_DE = q1->getRefTo_Dpsi_DE();
  q1->_elasticEnergy = mlawNonLinearTVM::freeEnergyMechanical(*q0,*q1,T0,T,&Dpsi_DT,&Dpsi_DE);

  // Mullins Effect
  if (_mullinsEffect != NULL && q1->_ipMullinsEffect != NULL){
     mlawNonLinearTVM::calculateMullinsEffectScaler(q0, q1, T, &Dpsi_DT);
  }
  double eta_mullins = 1.;
  if (q1->_ipMullinsEffect != NULL){
      
        // Put Stress in IP
        q1->_P_cap = P;
        
        // Modify Stress for mullins
        eta_mullins = q1->_ipMullinsEffect->getEta();
        P *= eta_mullins;
        q1->_elasticEnergy *= eta_mullins;
    }

  // Msg::Error(" Inside TVP, after correction, after updating q1->_elasticEnergy = %e, eta = %e !!",q1->_elasticEnergy, eta_mullins);

  // q1->getRefToElasticEnergy()=deformationEnergy(*q1);
  q1->getRefToViscousEnergyPart()=viscousEnergy(*q0,*q1)+q0->getConstRefToViscousEnergyPart();
  q1->getRefToPlasticEnergy() = q0->plasticEnergy();
  if (Gamma > 0){
    double dotMSN = dot(MS,N);
    q1->getRefToPlasticEnergy() += Gamma*dotMSN; // = Dp:Me
  }
  
  // fluxT
  double KThConT, DKThConDT;
  mlawNonLinearTVM::getKTHCon(KThConT,T,&DKThConDT);
  double J  = 1.;
  STensor3 Finv(0.);
  if (_thermalEstimationPreviousConfig){                                            // ADD  _thermalEstimationPreviousConfig
    STensorOperation::inverseSTensor3(F0,Finv);
    J = STensorOperation::determinantSTensor3(F0);
  }
  else{
    STensorOperation::inverseSTensor3(F,Finv);
    J = STensorOperation::determinantSTensor3(F);
  }

  static STensor3 Cinv;
  STensorOperation::multSTensor3SecondTranspose(Finv,Finv,Cinv);
  STensorOperation::multSTensor3SVector3(Cinv,gradT,fluxT);
  fluxT *= (-KThConT*J);

  // ThermSrc and MechSrc after dPdF and dPdT - But need the following tensors for the mechSrcTVP function call
  static STensor3 DphiPDF;
  STensorOperation::zero(dFpdF);
  // STensorOperation::zero(dFpdT);

  // I didnt make this
  if (this->getMacroSolver()->getPathFollowingLocalIncrementType() == pathFollowingManager::DEFO_ENERGY){
    q1->getRefToIrreversibleEnergy() = q1->defoEnergy();
  }
  else if ((this->getMacroSolver()->getPathFollowingLocalIncrementType() == pathFollowingManager::PLASTIC_ENERGY) or
             (this->getMacroSolver()->getPathFollowingLocalIncrementType() == pathFollowingManager::DISSIPATION_ENERGY)){
      q1->getRefToIrreversibleEnergy() = q1->plasticEnergy();
  }
  else{
    q1->getRefToIrreversibleEnergy() = 0.;
  }
  
 // thermalEnergy
 if (stiff){
     double& DDpsiTVMdTT = q1->getRefToDDFreeEnergyTVMdTT();
     // getFreeEnergyTVM(q0,q1,T0,T,NULL,NULL,&DDpsiTVMdTT);
     // CpT = -T*DDpsiTVMdTT;
     getCp(CpT,T);
  }
 else{
     getCp(CpT,T);
  }

 q1->_thermalEnergy = CpT*T;

 // thermalSource
 if (this->getTimeStep() > 0.){
  thermalSource = -CpT*(T-T0)/this->getTimeStep();
  }
 else
  thermalSource = 0.;

  // mechanical Source
  double& Wm_TVP = q1->getRefToMechSrcTVP(); // TVP
  double& Wm_TVE = q1->getRefToMechSrcTVE(); // TVE
  double& dWmdT_TVP = q1->getRefTodMechSrcTVPdT();
  STensor3& dWmdF_TVP = q1->getRefTodMechSrcTVPdF();

  mechanicalSource = 0.;
  
  // 1) ThermoElastic Heat
  static STensor3 DEe;
  DEe = q1->_Ee;
  DEe -= q0->_Ee;
  if (this->getTimeStep() > 0){
    mechanicalSource += (STensorOperation::doubledot(q1->_DcorKirDT,DEe)*T/this->getTimeStep());
  }
  
  // 2) Viscoelastic Contribution to mechSrc
  // mlawNonLinearTVE::getMechSourceTVE(q0,q1,T0,T,_I4,&Wm_TVE); // mechSourceTVE
  double& dWmdT_TVE = q1->getRefTodMechSrcTVEdT();
  STensor3& dWmdF_TVE = q1->getRefTodMechSrcTVEdF();
  static STensor3 dWmdE_TVE;
  getMechSource_TVE_term(q0,q1,T0,T,Wm_TVE,stiff,dWmdE_TVE,dWmdT_TVE);
  mechanicalSource += Wm_TVE;

  // 3) Viscoplastic Contribution to mechSrc
  getMechSourceTVP(F0,F,q0,q1,T0,T,Fepr,Cepr,DphiPDF,Wm_TVP,dWmdF_TVP,dWmdT_TVP);
  mechanicalSource += Wm_TVP;

  // freeEnergy and elastic energy
  double& psiTVM = q1->getRefToFreeEnergyTVM();
  // getFreeEnergyTVM(q0,q1,T0,T,&psiTVM,NULL);

  if (stiff){

    // dP/dF

    //1. get DtrKeprDCepr and DdevKeprDCepr, also DKeprDCepr
    static STensor3 DpDCepr;
    static STensor43 DdevKDCepr;
    static STensor43 DdevKeprDCepr, DdevMeprDCepr, DKeprDCepr;
    static STensor3 DpKeprDCepr, DpMeprDCepr;      // K = corKir; pK -> pressure of corKir
    STensorOperation::multSTensor3STensor43(_I,DlnDCepr,DpKeprDCepr);
    DpKeprDCepr*= (0.5*Ke);
    STensorOperation::multSTensor43(_Idev,DlnDCepr,DdevKeprDCepr);
    DdevKeprDCepr*= Ge;

    // DKeprDCepr
    DKeprDCepr = DdevKeprDCepr;
    for (int i=0; i<3; i++)
      for (int j=0; j<3; j++)
        for (int k=0; k<3; k++)
          for (int l=0; l<3; l++){
            DKeprDCepr(i,j,k,l) += _I(i,j)*DpKeprDCepr(k,l);
          }

    // initiate here - corrected corKir derivatives wrt Cepr
    DpDCepr = DpKeprDCepr;
    DdevKDCepr = DdevKeprDCepr;

    //2. get DCeprDCepr = _I4 and DCeinvprDCepr
    static STensor43 DCeinvprDCepr;

    for (int i=0; i<3; i++)
      for (int s=0; s<3; s++)
        for (int k=0; k<3; k++)
          for (int l=0; l<3; l++){
            DCeinvprDCepr(i,s,k,l) = 0.;
            for (int m=0; m<3; m++)
              for (int j=0; j<3; j++)
                DCeinvprDCepr(i,s,k,l) -= Ceinvpr(i,m)*_I4(m,j,k,l)*Ceinvpr(j,s);
          }

    //3. get DtrMeprDCepr and DdevMeprDCepr
    static STensor43 DMeprDCepr;

    static STensor3 temp1;
    static STensor43 temp2, temp3;
    STensorOperation::multSTensor3(Kepr,Ceinvpr,temp1);

    for (int i=0; i<3; i++)
      for (int s=0; s<3; s++)
        for (int k=0; k<3; k++)
          for (int l=0; l<3; l++){
            temp2(i,s,k,l) = 0.;
            temp3(i,s,k,l) = 0.;
            for (int m=0; m<3; m++){
              temp2(i,s,k,l) += _I4(i,m,k,l)*temp1(m,s);
              temp3(i,s,k,l) += DKeprDCepr(i,m,k,l)*Ceinvpr(m,s) + Kepr(i,m)*DCeinvprDCepr(m,s,k,l);
            }
          }

    for (int i=0; i<3; i++)
      for (int s=0; s<3; s++)
        for (int k=0; k<3; k++)
          for (int l=0; l<3; l++){
            DMeprDCepr(i,s,k,l) = 0.;
            for (int m=0; m<3; m++)
              DMeprDCepr(i,s,k,l) += Cepr(i,m)*temp3(m,s,k,l);
          }

    DMeprDCepr += temp2;
    DMeprDCepr += DKeprDCepr;
    DMeprDCepr *= 0.5;

    // Because, TrMepr = TrKepr
    DpMeprDCepr = DpKeprDCepr;

    //
    STensorOperation::zero(DdevMeprDCepr);
    for (int i=0; i<3; i++)
      for (int s=0; s<3; s++)
        for (int k=0; k<3; k++)
          for (int l=0; l<3; l++){
            DdevMeprDCepr(i,s,k,l) += DMeprDCepr(i,s,k,l) - _I(i,s)*DpMeprDCepr(k,l);
          }

    //3.1 Add predicted tangents for extrabranch (the corrected values are independent so we can put predicted values here)
    static STensor3 DsigV_DCepr;
    static STensor43 DsigD_DCepr;
    if (_useExtraBranch){

        STensorOperation::multSTensor3STensor43(_I,DlnDCepr,DsigV_DCepr);
        DsigV_DCepr *= 0.5*DsigV_dTrEe_pr;
        DpDCepr += DsigV_DCepr;
        DpMeprDCepr += DsigV_DCepr; // goes to DphiPDCepr
        
        static STensor43 temp3; 
        STensorOperation::multSTensor43(DsigD_dDevEe_pr,_Idev,temp3);
        STensorOperation::multSTensor43(temp3,DlnDCepr,DsigD_DCepr);
        DsigD_DCepr *= 0.5;
        DdevKDCepr += DsigD_DCepr;
        DdevMeprDCepr += DsigD_DCepr; // goes to DdevphiprDCepr
       
        // dMedF, dSdF come from dKcorDcepr directly
    }

    //4. get DdevphiDCepr and DphiPprDCepr
    static STensor3 DphiPprDCepr, DphiPDCepr;
    static STensor43 DdevphiprDCepr, DdevphiDCepr;

    DphiPprDCepr = DpMeprDCepr;
    DdevphiprDCepr = DdevMeprDCepr;

    DphiPDCepr = DphiPprDCepr;
    DphiPDCepr *= 1./v;
    DdevphiDCepr = DdevphiprDCepr;
    DdevphiDCepr *= 1./u;

    //5. get DphiEprDCepr from DdevphiDCepr
    static STensor3 DphiEDCepr, DphiEDdevPhi;
    if (PhiEq >0.){
        DphiEDdevPhi = devPhi;
        DphiEDdevPhi *= 1.5/(PhiEq);
    }

    STensorOperation::multSTensor3STensor43(DphiEDdevPhi,DdevphiDCepr,DphiEDCepr);


    // 6. to 11. (inside if loop-> Gamma > 0.)
      static STensor3 dAdCepr, dfDCepr, dgDCepr;
      static STensor3 DGDCepr;
      static STensor3 DgammaDCepr;
      static STensor3 DtrNDCepr;
      static STensor43 DdevNDCepr;
      static STensor43 dFpDCepr;
      static STensor43 dCedCepr, dCeinvdCepr;
      static STensor43 dXdCepr;
      static STensor3 dTrXdCepr;
      static STensor43 dGammaNdCepr;
      static STensor43 DEeDCepr_CHECK; // DEBUG
          
      if (Gamma >0.){

        //6. get dAdCepr from DdevphiDCepr
        double fact = a(2)*_n*pow(PhiEq,_n-1.);
        for (int i=0; i<3; i++){
          for (int j=0; j<3; j++){
            dAdCepr(i,j) = ( 4./3.*_b*_b*ptilde*DphiPDCepr(i,j) + 6.*PhiEq*DphiEDCepr(i,j) )/A;
            dfDCepr(i,j) = fact*DphiEDCepr(i,j)-a(1)*DphiPDCepr(i,j);
            // dgDCepr(i,j) = -kk*Gamma*dAdCepr(i,j);
          }
        }
        dgDCepr = dAdCepr;
        dgDCepr *= (-kk*Gamma);

        //7. get DGDCepr, DgammaDCepr
        static STensor3 DGDCepr_test, DgammaDCepr_test;
          for (int i=0; i<3; i++){
            for (int j=0; j<3; j++){
              // DGDCepr(i,j) = (-dfDCepr(i,j)-dfdDgamma*kk*Gamma*dAdCepr(i,j))/DfDGamma;
              DGDCepr(i,j) = (-dfDCepr(i,j) + dfdDgamma*dgDCepr(i,j)/dgdDgamma)/(dfdGamma - dfdDgamma*dgdGamma/dgdDgamma);
            }
          }

          for (int i=0; i<3; i++){
            for (int j=0; j<3; j++){
              // DgammaDCepr(i,j) = kk*Gamma*dAdCepr(i,j)+ dDgammaDGamma*DGDCepr(i,j);
              DgammaDCepr(i,j) = (-dgdGamma*DGDCepr(i,j) - dgDCepr(i,j))/dgdDgamma;
            }
          }

        //8. update DdevphiDCepr and DphiEDCepr, DphiPDCepr
        static STensor3 dCxdevdCepr, dCxtrdCepr;
        dCxdevdCepr = DgammaDCepr;
        dCxdevdCepr *= dCxdevdDgamma;
        dCxtrdCepr = DgammaDCepr;
        dCxdevdCepr *= dCxtrdDgamma;

        static STensor3 dudCepr, dvdCepr;
        for (int i=0; i<3; i++)
          for (int j=0; j<3; j++){
            dudCepr(i,j) = dudDgamma*DgammaDCepr(i,j) + dudGamma*DGDCepr(i,j);
            dvdCepr(i,j) = dvdDgamma*DgammaDCepr(i,j) + dvdGamma*DGDCepr(i,j);
          }

        //8.0 Do extraBranch stuff
        if (_useExtraBranch){

            // subtract predictor
            DphiPprDCepr -= DsigV_DCepr;
            DpDCepr -= DsigV_DCepr;

            DdevphiprDCepr -= DsigD_DCepr;
            DdevKDCepr -= DsigD_DCepr;
       }

       // update DphiPDCepr
       STensorOperation::zero(DphiPDCepr);
       DphiPDCepr += DphiPprDCepr;
       for (int i=0; i<3; i++)
         for (int j=0; j<3; j++){
           DphiPDCepr(i,j) += ( -dvdCepr(i,j)*ptilde + 1./3.*trXn/(pow(Cxtr,2.))*dCxtrdCepr(i,j) );

           if (_useExtraBranch){
               DphiPDCepr(i,j) -= DsigV_dTrEe * 2*_b*DGDCepr(i,j)*ptilde;
               for (int k=0; k<3; k++)
                 for (int l=0; l<3; l++){
                    DphiPDCepr(i,j) += 0.5 * DsigV_dTrEe * _I(k,l)*DlnDCepr(k,l,i,j);
                 }
           }
         }
       if (_useExtraBranch){
           DphiPDCepr *= 1/(v + 2*_b*Gamma*DsigV_dTrEe);
       }
       else{
           DphiPDCepr *= Dho4_v_inv;  // where, Dho4_v_inv = 1/v  
       }
       

       // update DdevphiDCepr, DphiEDCepr
       // Use Dho3 for DdevphiDCepr
       static STensor43 G2, G3;
       getG2Tensor(Cepr,Kepr,expGN,DCeinvprDCepr,DKeprDCepr,G2);
       for (int i=0; i<3; i++)
         for (int j=0; j<3; j++)
           for (int k=0; k<3; k++)
             for (int l=0; l<3; l++){
               G3(i,j,k,l) = G2(i,j,k,l) + DdevphiprDCepr(i,j,k,l) + devXn(i,j)*dCxdevdCepr(k,l)/(pow(Cxdev,2.)) - devPhi(i,j) * dudCepr(k,l);
               for (int p=0; p<3; p++)
                 for (int q=0; q<3; q++){
                    G3(i,j,k,l) +=  Dho3(i,j,p,q)* ( N(p,q)*DGDCepr(k,l) + 2.*_b*Gamma/3.*_I(p,q)*DphiPDCepr(k,l) );

                    if (_useExtraBranch){
                        G3(i,j,k,l) -= DsigD_dDevEe(i,j,p,q)*3*devPhi(p,q)*DGDCepr(k,l);
                        for (int r=0; r<3; r++)
                          for (int s=0; s<3; s++){
                            G3(i,j,k,l) += 0.5*DsigD_dDevEe(i,j,p,q)*_Idev(p,q,r,s)*DlnDCepr(r,s,k,l);
                          }
                    }
                 }
              }
        
       if (_useExtraBranch){
           static STensor43 temp, temp_inv;
           for (int i=0; i<3; i++)
             for (int j=0; j<3; j++)
               for (int k=0; k<3; k++)
                 for (int l=0; l<3; l++){
                    temp(i,j,k,l) = u*_I4(i,j,k,l) - 3.*Gamma*Dho3(i,j,k,l) + 3.*Gamma*DsigD_dDevEe(i,j,k,l); 
           }
           STensorOperation::inverseSTensor43(temp,temp_inv);
           STensorOperation::multSTensor43(temp_inv,G3,DdevphiDCepr);
       }
       else{
           STensorOperation::multSTensor43(Dho4_u_inv,G3,DdevphiDCepr);  
       }
        
        STensorOperation::multSTensor3STensor43(DphiEDdevPhi,DdevphiDCepr,DphiEDCepr);

        //9. get DtrNDCepr DdevNdCepr
        DtrNDCepr = DphiPDCepr;
        DtrNDCepr *= (2.*_b);

        DdevNDCepr = DdevphiDCepr;
        DdevNDCepr *= 3.;

    for (int i=0; i<3; i++)
      for (int j=0; j<3; j++)
        for (int k=0; k<3; k++)
           for (int l=0; l<3; l++)
               DEeDCepr_CHECK = 0.5*DlnDCepr(i,j,k,l) - N(i,j)*DGDCepr(k,l) - Gamma*DdevNDCepr(i,j,k,l) - Gamma/3.*_I(i,j)*DtrNDCepr(k,l);

        // 10. get dFpDCepr

        // dFpDCepr
        STensorOperation::zero(dGammaNdCepr);
        for (int i=0; i<3; i++)
          for (int j=0; j<3; j++){
            for (int k=0; k<3; k++)
              for (int l=0; l<3; l++)
                dGammaNdCepr(i,j,k,l) = N(i,j)*DGDCepr(k,l) + Gamma*DdevNDCepr(i,j,k,l) + Gamma/3.*_I(i,j)*DtrNDCepr(k,l);
        }

        static STensor43 CeprFp0;
        for (int i=0; i<3; i++)
          for (int j=0; j<3; j++)
            for (int k=0; k<3; k++)
              for (int l=0; l<3; l++){
                CeprFp0(i,j,k,l) = 0.;
                for (int s=0; s<3; s++){
                  CeprFp0(i,j,k,l) += dexpAdA(i,s,k,l)*Fp0(s,j);
                }
              }

         STensorOperation::multSTensor43(CeprFp0,dGammaNdCepr,dFpDCepr);

         // 10.1 get dXDCepr
         static STensor3 DtrXdCepr;
         static STensor43 DdevXdCepr;
         for (int i=0; i<3; i++)
           for (int j=0; j<3; j++){
             DtrXdCepr(i,j) = pow(kk,1.)*dHb*Gamma*trN*DgammaDCepr(i,j) + pow(kk,1.)*Hb*( trN*DGDCepr(i,j) + Gamma*DtrNDCepr(i,j) ) - trX*dCxtrdCepr(i,j) ; // pow(kk,2.) DEBUG
             for (int k=0; k<3; k++)
               for (int l=0; l<3; l++){
                 DdevXdCepr(i,j,k,l) = pow(kk,1.)*dHb*Gamma*devN(i,j)*DgammaDCepr(k,l) + pow(kk,1.)*Hb*( devN(i,j)*DGDCepr(k,l) + Gamma*DdevNDCepr(i,j,k,l) ) -
                                        devX(i,j)*dCxdevdCepr(k,l) ; // pow(kk,2.) DEBUG
                }
           }
         DdevXdCepr *= 1./Cxdev;
         DtrXdCepr *= 1./Cxtr;

         STensorOperation::zero(dXdCepr);
         dXdCepr = DdevXdCepr;
         for (int i=0; i<3; i++)
           for (int j=0; j<3; j++)
             for (int k=0; k<3; k++)
               for (int l=0; l<3; l++)
                 dXdCepr(i,j,k,l) += 1./3. * _I(i,j)*DtrXdCepr(k,l);


         // 11. update DpDCepr, DdevKDCepr

         //11.0 Correct extraBranch
         if (_useExtraBranch){

            // Dont do it like this
            // correct DsigV_DCepr and DsigD_DCepr; Also add to DpDCepr and DdevKDCepr
            // STensorOperation::multSTensor3STensor43(_I,DlnDCepr,DsigV_DCepr);
            // DsigV_DCepr *= DsigV_dTrEe;
            // STensorOperation::multSTensor43(DsigD_dDevEe,DlnDCepr,DsigD_DCepr);

            static STensor43 dDevEe_dCepr;
            STensorOperation::zero(DsigV_DCepr);
            STensorOperation::zero(DsigD_DCepr);
            STensorOperation::zero(dDevEe_dCepr);

            STensorOperation::multSTensor3STensor43(_I,DlnDCepr,DsigV_DCepr);
            DsigV_DCepr *= 0.5;
            STensorOperation::multSTensor43(_Idev,DlnDCepr,dDevEe_dCepr);
            dDevEe_dCepr *= 0.5;
            for (int i=0; i<3; i++)
              for (int j=0; j<3; j++){
                 DsigV_DCepr(i,j) -= 2*_b*(DGDCepr(i,j)*ptilde + Gamma*DphiPDCepr(i,j));
                 for (int k=0; k<3; k++)
                    for (int l=0; l<3; l++){
                       dDevEe_dCepr(i,j,k,l) -= 3*(devPhi(i,j)*DGDCepr(k,l) + Gamma*DdevphiDCepr(i,j,k,l));
                    }
              }

            DsigV_DCepr *= DsigV_dTrEe;
            STensorOperation::multSTensor43(DsigD_dDevEe,dDevEe_dCepr,DsigD_DCepr);

            DpDCepr += DsigV_DCepr;
            DdevKDCepr += DsigD_DCepr;

          // dMedF, dSdF come from dKcorDcepr directly
         }

         // (DpKeprDCepr DdevKeprDCepr)
         for (int i=0; i<3; i++){
           for (int j=0; j<3; j++){
             DpDCepr(i,j) -= Ke*(DGDCepr(i,j)*trN + Gamma*DtrNDCepr(i,j));
             for (int k=0; k<3; k++){
               for (int l=0; l<3; l++){
                 DdevKDCepr(i,j,k,l) -=  2.*Ge*(DGDCepr(k,l)*devN(i,j) + Gamma*DdevNDCepr(i,j,k,l));
               }
             }
           }
         }

      } // if Gamma
      else{
      // elastic
        STensorOperation::zero(DgammaDCepr);
        STensorOperation::zero(dFpDCepr);
        STensorOperation::zero(DtrNDCepr);
        STensorOperation::zero(DdevNDCepr);
        STensorOperation::zero(DGDCepr);
        STensorOperation::zero(dXdCepr);
        STensorOperation::zero(dTrXdCepr);
      }

      // 12. get dKcorDcepr
      static STensor43 dKcorDcepr;
      dKcorDcepr = DdevKDCepr;
      for (int i=0; i<3; i++){
        for (int j=0; j<3; j++){
          for (int k=0; k<3; k++){
            for (int l=0; l<3; l++){
              dKcorDcepr(i,j,k,l) += _I(i,j)*DpDCepr(k,l);
            }
          }
        }
      }

      // 13. get  CeprToF conversion tensor
      static STensor43 CeprToF;
      for (int i=0; i<3; i++){
        for (int j=0; j<3; j++){
          for (int k=0; k<3; k++){
            for (int l=0; l<3; l++){
              CeprToF(i,j,k,l) = 2.*Fepr(k,i)*invFp0(j,l);
            }
          }
        }
      }

      static STensor43 DKcorDF;
      STensorOperation::multSTensor43(dKcorDcepr,CeprToF,DKcorDF);

      // 14. dXdF, dGammaNdF, DphiPDF for mechSrc
      STensor43& dXdF = q1->getRefToDbackStressdF();
      STensorOperation::zero(dXdF);
      STensorOperation::multSTensor43(dXdCepr,CeprToF,dXdF);

      STensor43& dGammaNdF = q1->_dGammaNdF;
      STensorOperation::zero(dGammaNdF);
      STensorOperation::multSTensor43(dGammaNdCepr,CeprToF,dGammaNdF);

      // DphiPDCepr - use this for the pressure term in R
      for (int i=0; i<3; i++)
        for (int j=0; j<3; j++){
          DphiPDF(i,j) = 0.;
          for (int k=0; k<3; k++)
            for (int l=0; l<3; l++){
              DphiPDF(i,j) = DphiPDCepr(k,l)*CeprToF(k,l,i,j);
            }
          }

        // 15. get DgammaDF
        STensor3& DgammaDF = q1->_DgammaDF;
        STensor3& DGammaDF = q1->_DGammaDF;
        STensorOperation::zero(DgammaDF);
        STensorOperation::zero(DGammaDF);
        if (Gamma > 0){
          STensorOperation::multSTensor3STensor43(DgammaDCepr,CeprToF,DgammaDF);
          STensorOperation::multSTensor3STensor43(DGDCepr,CeprToF,DGammaDF);
          STensorOperation::multSTensor43(dFpDCepr,CeprToF,dFpdF);
        }

        // 16. Everything else - Conversion Tensors
        static STensor43 DinvFpDF, dCedF, dCeinvDF; //
        for (int i=0; i<3; i++)
          for (int s=0; s<3; s++)
            for (int k=0; k<3; k++)
              for (int l=0; l<3; l++){
                DinvFpDF(i,s,k,l) = 0.;
                for (int m=0; m<3; m++)
                  for (int j=0; j<3; j++)
                    DinvFpDF(i,s,k,l) -= Fpinv(i,m)*dFpdF(m,j,k,l)*Fpinv(j,s);
              }

        for (int m=0; m<3; m++)
          for (int j=0; j<3; j++)
            for (int k=0; k<3; k++)
              for (int l=0; l<3; l++){
                dFedF(m,j,k,l) = _I(m,k)*Fpinv(l,j);
                for (int s=0; s<3; s++)
                  dFedF(m,j,k,l) += F(m,s)*DinvFpDF(s,j,k,l);
              }

        for (int i=0; i<3; i++)
          for (int j=0; j<3; j++)
            for (int k=0; k<3; k++)
              for (int l=0; l<3; l++){
                dCedF(i,j,k,l) = 0.;
                for (int p=0; p<3; p++){
                    dCedF(i,j,k,l) += Fe(p,i)*dFedF(p,j,k,l) + dFedF(p,i,k,l)*Fe(p,j); // This Works!
                }
              }

        for (int i=0; i<3; i++)
          for (int s=0; s<3; s++)
            for (int k=0; k<3; k++)
              for (int l=0; l<3; l++){
                dCeinvDF(i,s,k,l) = 0.;
                for (int m=0; m<3; m++)
                  for (int j=0; j<3; j++)
                    dCeinvDF(i,s,k,l) -= Ceinv(i,m)*dCedF(m,j,k,l)*Ceinv(j,s);
              }

        // 17. Tangent - dPdF
        static STensor43 dSdCepr, dSdF, dMedCepr;
        STensor43& dMedF = q1->getRefToDModMandelDF();
        STensorOperation::zero(dMedF);

        for (int i=0; i<3; i++)
          for (int j=0; j<3; j++)
            for (int k=0; k<3; k++)
              for (int l=0; l<3; l++){
                dMedF(i,j,k,l) = DKcorDF(i,j,k,l);
                for (int m=0; m<3; m++)
                  for (int s=0; s<3; s++)
                    dMedF(i,j,k,l) += dCedF(i,m,k,l)*KS(m,s)*Ceinv(s,j) + Ce(i,m)*(DKcorDF(m,s,k,l)*Ceinv(s,j) + KS(m,s)*dCeinvDF(s,j,k,l));
                                                     // KS is corKir
              }
        dMedF *= 0.5;

        for (int i=0; i<3; i++)
          for (int j=0; j<3; j++)
            for (int k=0; k<3; k++)
              for (int l=0; l<3; l++){
                dSdF(i,j,k,l) = 0.;
                for (int m=0; m<3; m++){
                  dSdF(i,j,k,l) += dCeinvDF(i,m,k,l)*MS(m,j) + Ceinv(i,m)*dMedF(m,j,k,l);
                }
              }


          for (int i=0; i<3; i++)
            for (int j=0; j<3; j++)
              for (int k=0; k<3; k++)
                for (int l=0; l<3; l++){
                  Tangent(i,j,k,l) = 0.;
                  for (int m=0; m<3; m++){
                    for (int n=0; n<3; n++){
                      Tangent(i,j,k,l) += dFedF(i,m,k,l)*S(m,n)*Fpinv(j,n);
                      Tangent(i,j,k,l) += Fe(i,m)*dSdF(m,n,k,l)*Fpinv(j,n);
                      Tangent(i,j,k,l) += Fe(i,m)*S(m,n)*DinvFpDF(j,n,k,l);
                    }
                  }
                }

    // dP/dT

    // 1. get dMeprDT from dKeprDT  -> DcorKirprDT from TVE
    static STensor3 dMeprDT, dDevMeprDT, DcorKirDT;
    static double dpMeprDT;
    // const STensor3& dKeprDT = q1->_DcorKirDT; // Get the predicted value
    STensorOperation::zero(dMeprDT);

    static STensor3 temp16, temp17;
    STensorOperation::multSTensor3(dKeprDT, Ceinvpr, temp16);
    STensorOperation::multSTensor3(Cepr, temp16, temp17);

    dMeprDT = 0.5*(dKeprDT + temp17);

    STensorOperation::decomposeDevTr(dMeprDT,dDevMeprDT,dpMeprDT);
    // dpMeprDT *= 1./3.; WHAT IS THE PROBLEM HERE? WHY DOES IT GIVE ZERO?
    dpMeprDT = dMeprDT.trace()/3.;

    DcorKirDT = dKeprDT; // update later

    // Not required anymore -> dKeprDT has extrabranch inside
    // 1.1 add extraBranch DsigV_DT, DsigD_DT (these do not need to be corrected) 
    /*
    if (_useExtraBranch){

        DcorKirDT += DsigD_dT;
        for (int i=0; i<3; i++)
          for (int j=0; j<3; j++){
              DcorKirDT(i,j) += DsigV_dT*_I(i,j);
          }

        dpMeprDT += DsigV_dT; // goes to dPhipprDT
        dDevMeprDT += DsigD_dT; // goes to dDevPhiprDT

        // dMedT, dSdT come from DcorKirDT directly
    }*/ 

    // 2. get dPhipprDT and dDevPhiprDT
    static double dPhipprDT;
    static STensor3 dDevPhiprDT;

    dPhipprDT = dpMeprDT;
    dDevPhiprDT = dDevMeprDT;

    // 3. get dCxdT, dXdT

    // need this for DmechsourceDT -> initialise here (X is backStress)
    STensor3& dXdT = q1->getRefToDbackStressdT();
    STensorOperation::zero(dXdT);
    STensor3& dGammaNdT =  q1->_dGammaNdT;
    STensorOperation::zero(dGammaNdT);

    // 4. get dPhiPdT and dDevPhiDT
    static double dPhiPdT;
    static STensor3 dDevPhiDT;

    dPhiPdT = dPhipprDT;
    dPhiPdT /= v;

    dDevPhiDT = dDevPhiprDT;
    dDevPhiDT *= 1./u;

    // 5. get dPhiEdT
    static double dPhiEdT;
    STensorOperation::doubleContractionSTensor3(DphiEDdevPhi,dDevPhiDT,dPhiEdT);

    // 6. to 11. (inside if loop-> Gamma > 0.)
    static double dAdT, dfdT, dgdT;
    static double dGammaDT;
    static double DgammaDT;
    static double DtrNdT;
    static STensor3 DdevNdT;
    static STensor3 dFpDT;

    double eta(0.),Deta(0.),DetaDT(0.);
    if (_viscosity != NULL)
        _viscosity->get(q1->_epspbarre,T,eta,Deta,DetaDT);
    double etaOverDt = eta/this->getTimeStep();

    if (Gamma >0){
        // plastic

        // 6. get dAdT, dfdT, dgdT
        fullVector<double> DaDT(3), DDaDTT(3);
        getYieldCoefficientTempDers(q1,T,DaDT,DDaDTT);

        dAdT = (4./3.*_b*_b*ptilde*dPhiPdT + 6.*PhiEq*dPhiEdT)/A;
        dfdT = DaDT(2)*pow(PhiEq,_n) + a(2)*_n*pow(PhiEq,_n-1.)*dPhiEdT - DaDT(1)*ptilde - a(1)*dPhiPdT - DaDT(0);
        if (this->getTimeStep()>0){
          dfdT -= (DetaDT*Gamma/this->getTimeStep())*_p*pow((eta*Gamma/this->getTimeStep()),(_p-1.));
        }

        dgdT = -kk*Gamma*dAdT;

        // 7. get dGammaDT, DgammaDT
        // double dGammaDT_old = (-dfdT-dfdDgamma*kk*Gamma*dAdT)/DfDGamma;
        dGammaDT = (-dfdT + dfdDgamma*dgdT/dgdDgamma)/(dfdGamma - dfdDgamma*dgdGamma/dgdDgamma);
        // double DgammaDT_old = kk*Gamma*dAdT + dDgammaDGamma*dGammaDT;
        DgammaDT = (-dgdGamma*dGammaDT - dgdT)/dgdDgamma;


        // 8.0 update dCxdT, dudT, dvdT
        static double dCxdevdT, dCxtrdT;
        dCxdevdT = 0.; dCxtrdT = 0.;
        // dHbdT+dHb*DgammaDT
        if (Hb>0.){
           dCxdevdT = -m(0)*( -1./pow(Hb,2.)*dHb*Dgamma*(dHbdT) + 1./Hb*(ddHbdgammadT*Dgamma + dHb*DgammaDT))
                      + (1./pow(Hb,2.)*(dHbdT)*(dHbdT) - 1./Hb*ddHbddT)*(T-T0) -1./Hb*dHbdT;
        }
        dCxtrdT = dCxdevdT;
        dCxdevdT += 3.*m(1)*pow(kk,1.)*(dHbdT); // pow(kk,2.) DEBUG
        dCxtrdT += 2.*_b/3.*m(1)*pow(kk,1.)*(dHbdT); // pow(kk,2.) DEBUG

        static double dudT, dvdT;
        dudT = 6.*Gamma*(DGDTsum + pow(kk,1.)/(2.*Cxdev)*(dHbdT) - (pow(kk,1.)*Hb)/(2.*pow(Cxdev,2))*dCxdevdT); // pow(kk,2.) DEBUG
        dvdT = 2.*_b*Gamma*(DKDTsum + pow(kk,1.)/(3.*Cxtr)*(dHbdT) - (pow(kk,1.)*Hb)/(3.*pow(Cxtr,2))*dCxtrdT); // pow(kk,2.) DEBUG

        dudT += 6.*dGammaDT*(Ge+pow(kk,1)*Hb/(2.*Cxdev));  // pow(kk,2) DEBUG
        dvdT += 2.*_b*dGammaDT*(Ke+pow(kk,1)*Hb/(3.*Cxtr));  // pow(kk,2) DEBUG

        // 8. update DdevphiDT and DphiPDT
        dPhiPdT = (dPhipprDT - dvdT*ptilde + 1./3.*trXn/(pow(Cxtr,2.))*dCxtrdT);
        if (_useExtraBranch){
            dPhiPdT -= 2*_b*DsigV_dTrEe*dGammaDT*ptilde;
        }
        dPhiPdT *= Dho4_v_inv;

        static STensor3 G2T, G3T;
        getG2TTensor(Cepr,expGN,dKeprDT,G2T);
        for (int i=0; i<3; i++)
          for (int j=0; j<3; j++){
            G3T(i,j) = G2T(i,j) + dDevPhiprDT(i,j) + devXn(i,j)/pow(Cxdev,2.)*dCxdevdT - dudT*devPhi(i,j);
            for (int p=0; p<3; p++)
              for (int q=0; q<3; q++){
                G3T(i,j) += Dho3(i,j,p,q)*(dGammaDT*N(p,q) + 2.*_b*Gamma/3.*dPhiPdT*_I(p,q));

                if (_useExtraBranch){
                    G3T(i,j) -= 3*DsigD_dDevEe(i,j,p,q)*dGammaDT*devPhi(p,q);
                }
              }
          }

        STensorOperation::multSTensor3STensor43(G3T,Dho4_u_inv,dDevPhiDT);

        // 9. get DtrNdT DdevNdT
        DtrNdT = dPhiPdT;
        DtrNdT *= (2.*_b);

        DdevNdT = dDevPhiDT;
        DdevNdT *= 3.;

        // 10. get dFpdT
        STensorOperation::zero(dGammaNdT);
        for (int i=0; i<3; i++)
          for (int j=0; j<3; j++)
            dGammaNdT(i,j) += N(i,j)*dGammaDT + Gamma*DdevNdT(i,j) + Gamma/3.*_I(i,j)*DtrNdT;

        static STensor43 CeprFp0;
        for (int i=0; i<3; i++)
          for (int j=0; j<3; j++)
            for (int k=0; k<3; k++)
              for (int l=0; l<3; l++){
                CeprFp0(i,j,k,l) = 0.;
                for (int s=0; s<3; s++){
                  CeprFp0(i,j,k,l) += dexpAdA(i,s,k,l)*Fp0(s,j);
            }
          }

        for (int i=0; i<3; i++)
          for (int j=0; j<3; j++){
            dFpdT(i,j) = 0.;
            for (int k=0; k<3; k++)
              for (int l=0; l<3; l++){
                dFpdT(i,j) += CeprFp0(i,j,k,l)*dGammaNdT(k,l);
              }
            }

        // 10.1 dXdT - backstress temperature derivative -> correction - CHECK!!!
        static double DtrXdT;
        static STensor3 DdevXdT;
        DtrXdT = pow(kk,1.)*Gamma*(dHbdT)*trN + pow(kk,1.)*Hb*(dGammaDT*trN + Gamma*DtrNdT) - dCxtrdT*trX; // pow(kk,2.) DEBUG
        for (int i=0; i<3; i++)
          for (int j=0; j<3; j++){
                DdevXdT(i,j) = pow(kk,1.)*Gamma*(dHbdT)*devN(i,j) + pow(kk,1.)*Hb*( dGammaDT*devN(i,j) + Gamma*DdevNdT(i,j) ) - dCxdevdT*devX(i,j) ; // pow(kk,2.) DEBUG
        }
        DdevXdT *= 1./Cxdev;
        DtrXdT *= 1./Cxtr;

        STensorOperation::zero(dXdT);
        dXdT = DdevXdT;
        dXdT(0,0) = DtrXdT/3.;
        dXdT(1,1) = DtrXdT/3.;
        dXdT(2,2) = DtrXdT/3.;

        // 11. DcorKirDT

         //11.0 Correct extraBranch
         if (_useExtraBranch){

            for (int i=0; i<3; i++)
              for (int j=0; j<3; j++){
                 DcorKirDT(i,j) -= DsigV_dTrEe*2*_b*(dGammaDT*ptilde + Gamma*dPhiPdT)*_I(i,j);
                 for (int k=0; k<3; k++)
                    for (int l=0; l<3; l++){
                       DcorKirDT(i,j) -= DsigD_dDevEe(i,j,k,l)*3*(devPhi(k,l)*dGammaDT + Gamma*dDevPhiDT(k,l));
                    }
              }

          // dMedT, dSdT come from dKcorDcepr directly
         }

        for (int i=0; i<3; i++){
          for (int j=0; j<3; j++){
            DcorKirDT(i,j) -= ( DKDTsum*Gamma*trN + Ke*(dGammaDT*trN+Gamma*DtrNdT))*_I(i,j);
            DcorKirDT(i,j) -=  2.*( DGDTsum*Gamma*devN(i,j) + Ge*(dGammaDT*devN(i,j)+Gamma*DdevNdT(i,j)));
          }
        }
        q1->_DcorKirDT = DcorKirDT; // update in IP

      } // if Gamma
      else{
          // elastic
          STensorOperation::zero(DgammaDT);
          STensorOperation::zero(dFpDT);
          STensorOperation::zero(DtrNdT);
          STensorOperation::zero(DdevNdT);
          STensorOperation::zero(dGammaDT);
          }

      // 12. get dKcorDT
      // done above

      // 13. get DinvFpdT, dFedT, dCedT, dCeinvDT
      static STensor3 DinvFpdT, dFedT, dCedT, dCeinvdT;
      STensorOperation::zero(DinvFpdT);
       for (int i=0; i<3; i++)
         for (int j=0; j<3; j++)
           for (int p=0; p<3; p++)
             for (int q=0; q<3; q++){
               DinvFpdT(i,j) -= Fpinv(i,p)*dFpdT(p,q)*Fpinv(q,j);
             }

       STensorOperation::zero(dFedT);
       for (int i=0; i<3; i++)
         for (int j=0; j<3; j++)
           for (int k=0; k<3; k++){
             dFedT(i,j) += F(i,k)*DinvFpdT(k,j);
           }

       STensorOperation::zero(dCedT);
       for (int i=0; i<3; i++)
         for (int j=0; j<3; j++){
             for (int p=0; p<3; p++){
               dCedT(i,j) += Fe(p,i)*dFedT(p,j) + dFedT(p,i)*Fe(p,j); // This Works!
             }
           }

       STensorOperation::zero(dCeinvdT);
       for (int i=0; i<3; i++)
         for (int s=0; s<3; s++)
           for (int k=0; k<3; k++)
             for (int l=0; l<3; l++){
               dCeinvdT(i,s) -= Ceinv(i,k)*dCedT(k,l)*Ceinv(l,s);
             }

        // 13.1 get dMeDT -> needed for DmechSourceDT
        STensor3& dMeDT = q1->getRefToDModMandelDT();
        STensorOperation::zero(dMeDT);

        for (int i=0; i<3; i++)
          for (int j=0; j<3; j++){
            dMeDT(i,j) = DcorKirDT(i,j);
            for (int k=0; k<3; k++)
              for (int l=0; l<3; l++){
                dMeDT(i,j) += dCedT(i,k)*KS(k,l)*Ceinv(l,j) + Ce(i,k)*(DcorKirDT(k,l)*Ceinv(l,j) + KS(k,l)*dCeinvdT(l,j));
              }
            }
        dMeDT *= 0.5;

        // 14. get dSdT
        static STensor3 dSdT;
        for (int i=0; i<3; i++)
          for (int j=0; j<3; j++){
            dSdT(i,j) = 0.;
            for (int m=0; m<3; m++){
              dSdT(i,j) += Ceinv(i,m)*dMeDT(m,j) + dCeinvdT(i,m)*MS(m,j);
            }
         }

         // 15. Finally, get dPdT
        for (int i=0; i<3; i++)
          for (int j=0; j<3; j++){
            dPdT(i,j) = 0.;
            for (int k=0; k<3; k++)
              for (int l=0; l<3; l++){
                dPdT(i,j) += (dFedT(i,k)*S(k,l)*Fpinv(j,l) + Fe(i,k)*dSdT(k,l)*Fpinv(j,l) + Fe(i,k)*S(k,l)*DinvFpdT(j,l));
              }
          }
        
        // Important conversion tensors
        static STensor43 DEeDCepr, DEeDF;
        STensorOperation::zero(DEeDCepr);
        STensorOperation::zero(DEeDF);
            // DEeDCepr to DEeDF
        // STensorOperation::multSTensor43(_I4, DlnDCepr, DEeDCepr);
        DEeDCepr = DlnDCepr;
        DEeDCepr *= 0.5;
        DEeDCepr -= dGammaNdCepr;
        STensorOperation::multSTensor43(DEeDCepr, CeprToF, DEeDF);
        
        // Note: DEeDT = - dGammaNdT
        
        // Check DEeDF
        
        // Compute Tangents for Mullin's Effect
        if (_mullinsEffect != NULL && q1->_ipMullinsEffect != NULL){
            

            // 1st Term
            Tangent *= q1->_ipMullinsEffect->getEta();
            dPdT *= q1->_ipMullinsEffect->getEta();
            
            // 2nd Term
            static double Deta_mullins_DT(0.);
            static STensor3 Deta_mullins_DF, Deta_mullins_DEe;
            
                // Deta_mullins_DF
            STensorOperation::multSTensor3STensor43(q1->_DpsiDE,DEeDF,Deta_mullins_DF);
            Deta_mullins_DF *= q1->_DmullinsDamage_Dpsi_cap;
            
                // Deta_mullins_DT
            STensorOperation::doubleContractionSTensor3(q1->_DpsiDE,dGammaNdT,Deta_mullins_DT);
            Deta_mullins_DT *= -q1->_DmullinsDamage_Dpsi_cap;
            Deta_mullins_DT += q1->_DmullinsDamage_DT;
            
            for (int i=0; i<3; i++)
                for (int j=0; j<3; j++){
                    dPdT(i,j) += q1->_P_cap(i,j) * Deta_mullins_DT;
                        for (int k=0; k<3; k++)
                            for (int l=0; l<3; l++)
                                Tangent(i,j,k,l) += q1->_P_cap(i,j) * Deta_mullins_DF(k,l);
                }
            
        }
        

    // TVP - flux derivatives
    // fluxT
    dfluxTdT = fluxT;
    dfluxTdT *= (DKThConDT/KThConT);
    dfluxTdgradT = Cinv;
    dfluxTdgradT *= (-KThConT*J);
    STensorOperation::zero(dfluxTdF);

    if (!_thermalEstimationPreviousConfig){

     static STensor3 DJDF;
     static STensor43 DCinvDF;
     for (int i=0; i<3; i++){
      for (int j=0; j<3; j++){
        DJDF(i,j) = J*Finv(j,i);
        for (int k=0; k<3; k++){
          for (int l=0; l<3; l++){
            DCinvDF(i,j,k,l) = 0.;
            for (int p=0; p<3; p++){
              for (int a=0; a<3; a++){
                for (int b=0; b<3; b++){
                  DCinvDF(i,j,k,l) -= 2.*F(k,p)*Cinv(i,a)*Cinv(j,b)*_I4(a,b,p,l);
                }
              }
            }
          }
        }
       }
      }

     for (int i=0; i<3; i++){
       for (int j=0; j<3; j++){
         for (int k=0; k<3; k++){
           dfluxTdF(i,j,k) += (DJDF(j,k)*fluxT(i)/J);
           for (int l=0; l<3; l++){
             dfluxTdF(i,j,k) -= (J*DCinvDF(i,l,j,k)*gradT(l)*KThConT);
           }
         }
       }
      }
    }

 // thermSrc and MechSrc Derivatives

    // const double& DDpsiTVMdTT_0 = q0->getConstRefToDDFreeEnergyTVMdTT();
    // double& DDpsiTVMdTT = q1->getRefToDDFreeEnergyTVMdTT();
    // getFreeEnergyTVM(q0,q1,T0,T,NULL,NULL,&DDpsiTVMdTT);
    // double CpT = -T*DDpsiTVMdTT;
    // double CpT_0 = -T0*DDpsiTVMdTT_0;

    // thermal source derivatives
    double DCpDT(0.);
    mlawNonLinearTVM::getCp(CpT,T,&DCpDT);
    static STensor3 DCpDF;
    STensorOperation::zero(DCpDF);                                                       // CHANGE for DCpDF

    if (this->getTimeStep() > 0){
      dthermalSourcedT = -DCpDT*(T-T0)/this->getTimeStep() - CpT/this->getTimeStep();
   // dthermalSourcedT = -CpT/this->getTimeStep() - (CpT-CpT_0)/this->getTimeStep();
      for(int i = 0; i< 3; i++){
        for(int j = 0; j< 3; j++){
          dthermalSourcedF(i,j) = -DCpDF(i,j)*(T-T0)/this->getTimeStep();
        }
      }
    }
    else{
      dthermalSourcedT = 0.;
      STensorOperation::zero(dthermalSourcedF);
    }


  // mechSourceTVP derivatives
  dmechanicalSourcedT = 0.;
  STensorOperation::zero(dmechanicalSourceF);
  static STensor3 dmechanicalSourceE;
  STensorOperation::zero(dmechanicalSourceE);
  
/*
    // Numerical Derivatives
    double T_plus(0.), Wm_TVE_plus(0.), Wm_TVP_plus(0.), dWm_TVEdT_plus(0.), dWm_TVPdT_plus(0.), DKDTsum_plus(0.), DGDTsum_plus(0.);
    static STensor3 F_plus, Kepr_plus, Mepr_plus, Ke_plus, Me_plus, N_plus, P_plus, DphiPDF_plus, dWm_TVEdF_plus, dWm_TVPdF_plus;
    static STensor43 ddXdTdF_plus;

    static IPNonLinearTVP q_Plus(*q0);

    // Thermal Derivatives
    T_plus = T + _perturbationfactor*T0;

    plasticCorrector_TVP(F0,F,q0,&q_Plus,T0,T_plus,Kepr_plus,Mepr_plus,DKDTsum_plus,DGDTsum_plus,Ke_plus,Me_plus,N_plus,P_plus,true,&DphiPDF_plus);
    mlawNonLinearTVE::getMechSourceTVE(q0,&q_Plus,T0,T_plus,DKDTsum_plus,DGDTsum_plus,_I4,&Wm_TVE_plus);
    mlawNonLinearTVP::getMechSourceTVP(F0,F,q0,q1,T0,T_plus,Fepr,Cepr,DphiPDF_plus,&Wm_TVP_plus);

    dWm_TVEdT_plus = (Wm_TVE_plus-Wm_TVE)/(_perturbationfactor*T0);
    dWm_TVPdT_plus = (Wm_TVP_plus-Wm_TVP)/(_perturbationfactor*T0);

    // Deformation Derivatives
    for (int i=0; i<3; i++){
      for (int j=0; j<3; j++){
        F_plus = (F);
        F_plus(i,j) += _perturbationfactor;

        plasticCorrector_TVP(F0,F_plus,q0,&q_Plus,T0,T,Kepr_plus,Mepr_plus,DKDTsum_plus,DGDTsum_plus,Ke_plus,Me_plus,N_plus,P_plus,true,&DphiPDF_plus);
        mlawNonLinearTVE::getMechSourceTVE(q0,&q_Plus,T0,T,DKDTsum_plus,DGDTsum_plus,_I4,&Wm_TVE_plus);
        mlawNonLinearTVP::getMechSourceTVP(F0,F_plus,q0,&q_Plus,T0,T,Fepr,Cepr,DphiPDF_plus,&Wm_TVP_plus); // F_plus here doesnt change anything

        dWm_TVEdF_plus(i,j) = (Wm_TVE_plus - Wm_TVE)/_perturbationfactor;
        dWm_TVPdF_plus(i,j) = (Wm_TVP_plus - Wm_TVP)/_perturbationfactor;
      }
    }*/

  // DcorKirDT Term
  const STensor3& Correct_DcorKirDT = q1->getRefToDcorKirDT();
    // Recall the TVM member function and recast
  mlawNonLinearTVM::getTVEdCorKirDT(q0,q1,T0,T);
  STensor3& temp_DcorKirDT = q1->getRefToDcorKirDT();
  STensor3& DDcorKirDTT = q1->getRefToDDcorKirDTT();
  STensor43& DDcorKirDTDEe = q1->getRefToDDcorKirDTDE();
    
    static STensor3 DcorKirDT_I;
    STensorOperation::multSTensor3STensor43(Correct_DcorKirDT,_I4,DcorKirDT_I);
    if (this->getTimeStep() > 0){
            for (int i=0; i<3; i++){
                for (int j=0; j<3; j++){
                    dmechanicalSourceE(i,j) += T*DcorKirDT_I(i,j)/this->getTimeStep();
                    for (int k=0; k<3; k++)
                        for (int l=0; l<3; l++){
                            dmechanicalSourceE(i,j) += T*DDcorKirDTDEe(k,l,i,j)*DEe(k,l)/this->getTimeStep();
                            DDcorKirDTT(i,j) += - DDcorKirDTDEe(i,j,k,l)*dGammaNdT(k,l); // additional term due to TVP
                        }
                }
            }
            STensorOperation::multSTensor3STensor43(dmechanicalSourceE,DEeDF,dmechanicalSourceF);
        
            dmechanicalSourcedT += (STensorOperation::doubledot(DcorKirDT,DEe)/this->getTimeStep());
            dmechanicalSourcedT += T*(STensorOperation::doubledot(DDcorKirDTT,DEe)/this->getTimeStep());      
        }
  
  // TVE Term
  STensorOperation::multSTensor3STensor43(dWmdE_TVE, DEeDF, dWmdF_TVE);
  for (int i=0; i<3; i++)
    for (int j=0; j<3; j++)
      dWmdT_TVE += - dWmdE_TVE(i,j) * dGammaNdT(i,j); // the first term in dWmdT_TVE from pure TVE temperature dependency is obtained above.

  // TVP Term
  dmechanicalSourceF += dWmdF_TVE;
  dmechanicalSourcedT += dWmdT_TVE;
  dmechanicalSourceF += dWmdF_TVP; // dWmdF_TVP;
  dmechanicalSourcedT += dWmdT_TVP; // dWmdF_TVP;

  /*
  dmechanicalSourceF += dWm_TVEdF_plus; // dWmdF_TVP;
  dmechanicalSourceF += dWm_TVPdF_plus; // dWmdF_TVP;
  dmechanicalSourcedT += dWm_TVEdT_plus;
  dmechanicalSourcedT += dWm_TVPdT_plus;  */

  // Restore the original value in the IP
  q1->getRefToDcorKirDT() =  Correct_DcorKirDT; 
 }
}

void mlawNonLinearTVP::getG2Tensor(const STensor3& Cepr, const STensor3& Kepr, const STensor3& expGN,
                                    const STensor43& DCeinvprDCepr, const STensor43& DKeprDCepr, STensor43& G2) const{

  static STensor3 Hinv;
  STensorOperation::inverseSTensor3(expGN,Hinv);

  static STensor3 Ceinvpr;
  STensorOperation::inverseSTensor3(Cepr,Ceinvpr);

  static STensor43 term1, term2, term3;

  for (int i=0; i<3; i++)
    for (int j=0; j<3; j++)
      for (int k=0; k<3; k++)
        for (int l=0; l<3; l++){
          term1(i,j,k,l) = 0.;
          term2(i,j,k,l) = 0.;
          term3(i,j,k,l) = 0.;
          for (int p=0; p<3; p++)
            for (int q=0; q<3; q++){
                term2(i,j,k,l) -= _I4(i,p,k,l)*Kepr(p,q)*Ceinvpr(q,j);
                term3(i,j,k,l) -= (Cepr(i,p)*DKeprDCepr(p,q,k,l)*Ceinvpr(q,j) + Cepr(i,p)*Kepr(p,q)*DCeinvprDCepr(q,j,k,l));
              for (int r=0; r<3; r++)
                for (int s=0; s<3; s++)
                  for (int m=0; m<3; m++)
                    for (int o=0; o<3; o++){
                      term1(i,j,k,l) += ( Hinv(i,p)*Hinv(p,q)*_I4(q,r,k,l)*Kepr(r,s)*Ceinvpr(s,m)*expGN(m,o)*expGN(o,j) +
                                        Hinv(i,p)*Hinv(p,q)*Cepr(q,r)*DKeprDCepr(r,s,k,l)*Ceinvpr(s,m)*expGN(m,o)*expGN(o,j) );
                      term2(i,j,k,l) += Hinv(i,p)*Hinv(p,q)*Cepr(q,r)*Kepr(r,s)*DCeinvprDCepr(s,m,k,l)*expGN(m,o)*expGN(o,j);
                    }
            }
         }
  STensorOperation::zero(G2);
  G2 = term1+term2+term3;
  G2 *= 0.5;

}

void mlawNonLinearTVP::getG2TTensor(const STensor3& Cepr, const STensor3& expGN, const STensor3& dKeprDT, STensor3& G2T) const{

  static STensor3 Hinv;
  STensorOperation::inverseSTensor3(expGN,Hinv);

  static STensor3 Ceinvpr;
  STensorOperation::inverseSTensor3(Cepr,Ceinvpr);

  for (int i=0; i<3; i++)
    for (int j=0; j<3; j++){
      G2T(i,j) = 0.;
      for (int p=0; p<3; p++)
        for (int q=0; q<3; q++){
          G2T(i,j) -= Cepr(i,p)*dKeprDT(p,q)*Ceinvpr(q,j);
          for (int r=0; r<3; r++)
            for (int s=0; s<3; s++)
              for (int m=0; m<3; m++)
                for (int o=0; o<3; o++){
                  G2T(i,j) += Hinv(i,p)*Hinv(p,q)*Cepr(q,r)*dKeprDT(r,s)*Ceinvpr(s,m)*expGN(m,o)*expGN(o,j);
                }
        }
    }
    G2T *= 0.5;
}

void mlawNonLinearTVP::getDho3(const double& u, const double& v, const double& Gamma, const STensor3& Cepr, const STensor3& Ceinvpr, const STensor3& Kepr,
                                  const STensor3& expGN, const STensor43& dexpAdA, STensor43& Dho3, STensor43& Dho4inv, STensor43& Dho4_u_inv , double& Dho4_v_inv,
                                  const double* DsigV_dTrEe, const STensor43* DsigD_dDevEe) const{

  static STensor3 HT, Hinv, HTinv; // H = expGN is a symmetric tensor
  STensorOperation::inverseSTensor3(expGN,Hinv);
  STensorOperation::transposeSTensor3(expGN,HT);
  STensorOperation::transposeSTensor3(Hinv,HTinv);

  static STensor43 dHinvdH;
  for (int i=0; i<3; i++)
    for (int s=0; s<3; s++)
      for (int k=0; k<3; k++)
        for (int l=0; l<3; l++){
          dHinvdH(i,s,k,l) = 0.;
          for (int m=0; m<3; m++)
            for (int j=0; j<3; j++){
              dHinvdH(i,s,k,l) -= Hinv(i,m)*_I4(m,j,k,l)*Hinv(j,s);
            }
        }

  static STensor43 dHHdH, dHinvHinvdH;
  for (int i=0; i<3; i++)
    for (int j=0; j<3; j++)
      for (int m=0; m<3; m++)
        for (int n=0; n<3; n++){
          dHHdH(i,j,m,n) = 0.5*( _I(i,m)*expGN(j,n) + _I(i,n)*expGN(j,m) + _I(j,n)*expGN(i,m) + _I(j,m)*expGN(i,n));
          dHinvHinvdH(i,j,m,n) = 0.;
          for (int p=0; p<3; p++){
              dHinvHinvdH(i,j,m,n) += dHinvdH(i,p,m,n)*Hinv(p,j) + Hinv(i,p)*dHinvdH(p,j,m,n);
          }
        }

  static STensor3 CeprKeprCeinvpr;
  for (int i=0; i<3; i++)
    for (int j=0; j<3; j++){
      CeprKeprCeinvpr(i,j) = 0.;
      for (int k=0; k<3; k++)
        for (int l=0; l<3; l++){
          CeprKeprCeinvpr(i,j) += Cepr(i,k)*Kepr(k,l)*Ceinvpr(l,j);
     }
   }

  static STensor43 dDhoHdH;
  for (int i=0; i<3; i++)
    for (int j=0; j<3; j++)
      for (int k=0; k<3; k++)
        for (int l=0; l<3; l++){
          dDhoHdH(i,j,k,l) = 0.;
          for (int p=0; p<3; p++)
            for (int q=0; q<3; q++)
              for (int r=0; r<3; r++)
                for (int s=0; s<3; s++)
                    dDhoHdH(i,j,k,l) += 0.5*(dHinvHinvdH(i,p,k,l)*CeprKeprCeinvpr(p,q)*expGN(r,s)*expGN(s,j) +
                                               Hinv(i,p)*Hinv(p,q)*CeprKeprCeinvpr(q,r)*dHHdH(s,j,k,l));
         }


  for (int i=0; i<3; i++)
    for (int j=0; j<3; j++)
      for (int k=0; k<3; k++)
        for (int l=0; l<3; l++){
          Dho3(i,j,k,l) = 0.;
          for (int p=0; p<3; p++)
            for (int q=0; q<3; q++)
              Dho3(i,j,k,l) += dDhoHdH(i,j,p,q)*dexpAdA(p,q,k,l);
        }

  static STensor43 Dho4, Dho4_u;
  static STensor3 Dho4_v; // The implementation below is incorrect
  double Dho4_v_scalar_correct = 0.;
  STensorOperation::zero(Dho4_u);
  STensorOperation::zero(Dho4_v);
  for (int i=0; i<3; i++)
    for (int j=0; j<3; j++){
      Dho4_v(i,j) += v*_I(i,j);
      for (int k=0; k<3; k++)
        for (int l=0; l<3; l++){

          Dho4(i,j,k,l) = 0.;
          Dho4_u(i,j,k,l) += u*_I4(i,j,k,l) - 3.*Gamma*Dho3(i,j,k,l);
          Dho4_v(i,j) -= 2.*_b/3.*Gamma*Dho3(i,j,k,l)*_I(k,l);

          if (_useExtraBranch){
             Dho4_u(i,j,k,l) += 3.*Gamma* (*DsigD_dDevEe)(i,j,k,l);
          }

          for (int p=0; p<3; p++)
            for (int q=0; q<3; q++){
              Dho4(i,j,k,l) += (u*_I4(i,j,p,q) - 3.*Gamma*Dho3(i,j,p,q))*_Idev(p,q,k,l) + (v*_I4(i,j,p,q) - 2.*_b/3.*Gamma*Dho3(i,j,p,q))*(_I4(p,q,k,l) -_Idev(p,q,k,l));

              if (_useExtraBranch){
                  Dho4(i,j,k,l) += ( 3.*Gamma* (*DsigD_dDevEe)(i,j,p,q) )*_Idev(p,q,k,l) + (2.*_b*Gamma*(*DsigV_dTrEe)*_I4(i,j,p,q))*(_I4(p,q,k,l) -_Idev(p,q,k,l));
              }

        }
      }
    }


  STensorOperation::inverseSTensor43(Dho4,Dho4inv);
  STensorOperation::inverseSTensor43(Dho4_u,Dho4_u_inv);
  // STensorOperation::inverseSTensor3(Dho4_v,Dho4_v_inv);

  Dho4_v_scalar_correct = v;
  if (_useExtraBranch){
    Dho4_v_scalar_correct += 2.*_b*Gamma* (*DsigV_dTrEe);
  }
  if(abs(Dho4_v_scalar_correct)>0.){
    Dho4_v_inv = 1./Dho4_v_scalar_correct;
  }

}

void mlawNonLinearTVP::getIterated_DPhi(const double& T, const IPNonLinearTVP *q0, IPNonLinearTVP *q1,
                                            const double& u, const double& v, const double& Gamma, const double& Cxtr, const double& Cxdev, const STensor3& Phipr,
                                            const double& trXn, const STensor3& devXn, const STensor3& Cepr, const STensor3& Eepr, const STensor3& Kepr,
                                            double& ptilde, STensor3& devPhi,
                                            STensor3& Phi, STensor3& N, STensor3& expGN, STensor43& dexpAdA,
                                            STensor43& Dho3, STensor43& Dho4inv, STensor43& Dho4_u_inv , double& Dho4_v_inv,
                                            STensor3& sigExtra_pr, STensor3& sigExtra, double& DsigV_dTrEe, STensor43& DsigD_dDevEe) const{

  static STensor3 Hinv, GammaN;
  static STensor3 Ceinvpr;
  STensorOperation::inverseSTensor3(expGN,Hinv);
  STensorOperation::inverseSTensor3(Cepr,Ceinvpr);

  // Initialise Ce, Ceinv
  static STensor3 Ce, Ceinv; // Temporary Local Variable
  for (int i=0; i<3; i++)
    for (int j=0; j<3; j++){
      Ce(i,j) = 0.;
      Ceinv(i,j) = 0.;
      for (int k=0; k<3; k++)
        for (int l=0; l<3; l++){
          Ce(i,j) += Hinv(i,k)*Hinv(k,l)*Cepr(l,j);
          Ceinv(i,j) += Ceinvpr(i,k)*expGN(k,l)*expGN(l,j);
     }
   }

  // Initialise D
  static STensor3 D;
  for (int i=0; i<3; i++)
    for (int j=0; j<3; j++){
      D(i,j) = 0.;
      for (int k=0; k<3; k++)
        for (int l=0; l<3; l++){
          D(i,j) += 0.5*( Ce(i,k)*Kepr(k,l)*Ceinv(l,j) - Cepr(i,k)*Kepr(k,l)*Ceinvpr(l,j) );
     }
   }

  // Initialise J
  static STensor3 J, J_constant;
  STensorOperation::zero(J);
  STensorOperation::zero(J_constant);
  for (int i=0; i<3; i++){
    for (int j=0; j<3; j++){
      J_constant(i,j) += Phipr(i,j) + 1./3.*trXn*(1.-1./Cxtr)*_I(i,j) + devXn(i,j)*(1.-1./Cxdev);
      J(i,j) +=  devPhi(i,j)*u + ptilde*_I(i,j)*v - D(i,j);
    }
  }
  if (_useExtraBranch){
      J_constant -= sigExtra_pr; // Remove from Phipr
  }
  J -= J_constant;

  // Initialise Extra Branch, save sigExtra_pr
    static STensor3 Ee;
    DsigV_dTrEe = 0.;
    STensorOperation::zero(Ee);
    STensorOperation::zero(DsigD_dDevEe);
    if (_useExtraBranch){
        for (int i=0; i<3; i++)
          for (int j=0; j<3; j++){
            Ee(i,j) = Eepr(i,j) - Gamma*N(i,j);
        }
        mlawNonLinearTVM::extraBranchLaw(Ee,T,q0,q1,sigExtra,true,NULL,NULL,&DsigV_dTrEe,&DsigD_dDevEe);
        J -= sigExtra;
    }

  // Initialise J_tol
  double J_tol = 0.;
  for (int i=0; i<3; i++){
    for (int j=0; j<3; j++){
      J_tol += abs(J(i,j)) ;
    }
  }

  // Initialise Dho3, Dho4inv
  getDho3(u,v,Gamma,Cepr,Ceinvpr,Kepr,expGN,dexpAdA,Dho3,Dho4inv,Dho4_u_inv,Dho4_v_inv,&DsigV_dTrEe,&DsigD_dDevEe);

  // Initialise DPhi
  static STensor3 DPhi;

  // Initialise numerical quantities
  static STensor3 Phi_plus, J_plus, devPhi_plus, N_plus, Ee_plus,sigExtra_plus;
  double pPhi_plus, perturbation_factor;
  static STensor43 dJdPhi_plus, dJdPhi_plus_inv;

  int ite = 0;
  int maxite = 1000;

  double tol = 1e-9;
  while (fabs(J_tol) > tol or ite <1){
    
     /* 
     // numerical Dho4inv (Jacobian dJ/dPhi)
     perturbation_factor = _perturbationfactor; // = DPhi_numerical
     for (int i=0; i<3; i++){
       for (int j=0; j<3; j++){
         Phi_plus = Phi; 
         Phi_plus(i,j) += 0.5*perturbation_factor;
         Phi_plus(j,i) += 0.5*perturbation_factor;
         STensorOperation::decomposeDevTr(Phi_plus,devPhi_plus,pPhi_plus);
         pPhi_plus = Phi_plus.trace()/3;
         N_plus = _I;
         N_plus *= 2.*_b/3;
         N_plus += 3*devPhi_plus;
         Ee_plus = - Gamma*N_plus;
         Ee_plus += Eepr(i,j);
         sigExtra_plus = sigExtra;
         // mlawNonLinearTVM::extraBranchLaw(Ee_plus,T,q0,q1,sigExtra_plus,true,NULL,NULL,NULL,NULL);
         for (int k=0; k<3; k++){
           for (int l=0; l<3; l++){
             J_plus(k,l) = devPhi_plus(k,l)*u + pPhi_plus*_I(k,l)*v - D(k,l) - J_constant(k,l) - sigExtra_plus(k,l);
             dJdPhi_plus(k,l,i,j) = (J_plus(k,l) - J(k,l))/(perturbation_factor);
           }
         }
       }
     }
     STensorOperation::inverseSTensor43(dJdPhi_plus,dJdPhi_plus_inv);*/

     // update DPhi
     for (int i=0; i<3; i++)
       for (int j=0; j<3; j++){
         DPhi(i,j) = 0.;
         for (int k=0; k<3; k++)
           for (int l=0; l<3; l++){
             DPhi(i,j) -= Dho4inv(i,j,k,l)*J(k,l);
           }
         }

      // update devPhi
      for (int i=0; i<3; i++){
        for (int j=0; j<3; j++){
       		Phi(i,j) += DPhi(i,j);
       	}
      }

      // update devPhi, ptilde
      STensorOperation::decomposeDevTr(Phi,devPhi,ptilde);
      ptilde = Phi.trace()/3;

      // update N
      STensorOperation::zero(N);
      for (int i=0; i<3; i++)
        for (int j=0; j<3; j++)
          N(i,j) = 3.*devPhi(i,j) + 2.*_b/3.*ptilde*_I(i,j);

      // update H = exp(GN) = A in dexpAdA; Hinv
      GammaN = N;
      GammaN *= Gamma;
      STensorOperation::expSTensor3(GammaN,_order,expGN,&dexpAdA);
      STensorOperation::inverseSTensor3(expGN,Hinv);

      // Update Ce, Ceinv
      for (int i=0; i<3; i++)
        for (int j=0; j<3; j++){
          Ce(i,j) = 0.;
          Ceinv(i,j) = 0.;
          for (int k=0; k<3; k++)
            for (int l=0; l<3; l++){
              Ce(i,j) += Hinv(i,k)*Hinv(k,l)*Cepr(l,j);
              Ceinv(i,j) += Ceinvpr(i,k)*expGN(k,l)*expGN(l,j);
          }
        }

      // update D
      for (int i=0; i<3; i++)
        for (int j=0; j<3; j++){
          D(i,j) = 0.;
          for (int k=0; k<3; k++)
            for (int l=0; l<3; l++){
              D(i,j) += 0.5*( Ce(i,k)*Kepr(k,l)*Ceinv(l,j) - Cepr(i,k)*Kepr(k,l)*Ceinvpr(l,j) );
            }
          }


      // update J
      STensorOperation::zero(J);
      for (int i=0; i<3; i++){
        for (int j=0; j<3; j++){
          J(i,j) +=  devPhi(i,j)*u + ptilde*_I(i,j)*v - D(i,j);
         }
      }
      J -= J_constant;

      // update Extra Branch
      DsigV_dTrEe = 0.;
      STensorOperation::zero(Ee);
      STensorOperation::zero(sigExtra);
      STensorOperation::zero(DsigD_dDevEe);
      if (_useExtraBranch){
        for (int i=0; i<3; i++)
          for (int j=0; j<3; j++){
            Ee(i,j) = Eepr(i,j) - Gamma*N(i,j);
        }
        mlawNonLinearTVM::extraBranchLaw(Ee,T,q0,q1,sigExtra,true,NULL,NULL,&DsigV_dTrEe,&DsigD_dDevEe);
        J -= sigExtra;
      }

      // update Dho3, Dho4inv
      getDho3(u,v,Gamma,Cepr,Ceinvpr,Kepr,expGN,dexpAdA,Dho3,Dho4inv,Dho4_u_inv,Dho4_v_inv,&DsigV_dTrEe,&DsigD_dDevEe);

      // update J_tol
      /*
      J_tol = 0.;
      for (int i=0; i<3; i++){
        for (int j=0; j<3; j++){
          J_tol += abs(J(i,j));
        }
      }*/

      J_tol = J.norm0();
      J_tol/=(_K+_G);

      ite++;

      //if (ite> maxite-5)
      // Msg::Info("it = %d, tol %e, J_tol = %e, Gamma = %e",ite,tol,J_tol,Gamma);

      if (fabs(J_tol) <_tol) break;

      if(ite > maxite){
        Msg::Error("No convergence for iterated Phi mlawNonLinearTVP nonAssociatedFlow Maxwell iter = %d, J_tol = %e!!",ite,J_tol);
        Phi(0,0) = Phi(1,1) = Phi(2,2) = sqrt(-1.);
       return;
       }
     } // while

}

void mlawNonLinearTVP::predictorCorrector_TVP_AssociatedFlow(const STensor3& F, const IPNonLinearTVP *q0, IPNonLinearTVP *q1,
                            STensor3&P, const bool stiff, STensor43& Tangent, STensor43& dFedF, STensor43& dFpdF, const double T0, const double T) const{
  // compute elastic predictor
  STensor3& Fp1 = q1->_Fp;
  const STensor3& Fp0 = q0->_Fp;

  Fp1 = Fp0; // plastic deformation tensor
  q1->_epspbarre = q0->_epspbarre; // plastic equivalent strain
  q1->_epspCompression = q0->_epspCompression;
  q1->_epspTraction = q0->_epspTraction;
  q1->_epspShear = q0->_epspShear;
  q1->_backsig = q0->_backsig; // backstress tensor
  q1->_DgammaDt = 0.; // plastic rate --> failure

  static STensor3 Fpinv, Ce, Fepr;
  STensorOperation::inverseSTensor3(Fp1,Fpinv);
  STensorOperation::multSTensor3(F,Fpinv,Fepr);
  STensorOperation::multSTensor3FirstTranspose(Fepr,Fepr,Ce);

  static STensor3 invFp0; // plastic predictor
  invFp0= Fpinv;
  STensor3& Fe = q1->_Fe;
  Fe = Fepr;

  static STensor43 DlnDCepr, DlnDCe;
  static STensor63 DDlnDDCe;
  static STensor43 dexpAdA; // estimation of dexpA/dA

  STensor3& Ee = q1->_Ee;
  bool ok=STensorOperation::logSTensor3(Ce,_order,Ee,&DlnDCepr,&DDlnDDCe);
  if(!ok)
  {
     P(0,0) = P(1,1) = P(2,2) = sqrt(-1.);
     return;
  }
  Ee *= 0.5;
  DlnDCe = DlnDCepr;

  // update A, B
  // double Ge, Ke;
  double Ke(0.), Ge(0.);
  double DKDTsum, DGDTsum = 0.; // Deprecated
  static STensor43 Bd_stiffnessTerm;
  // ThermoViscoElasticPredictor(Ee,q0->_Ee,q0,q1,Ke,Ge,Kde,Gde,DKe,DGe,DKde,DGde,KTsum,GTsum,DKDTsum,DGDTsum,T0,T);
  ThermoViscoElasticPredictor(Ee,q0->_Ee,q0,q1,Ke,Ge,T0,T,stiff,Bd_stiffnessTerm);

  static STensor3 devKpr; // dev corotational kirchoff stress predictor
  static double ppr; // pressure predictor

  STensorOperation::decomposeDevTr(q1->_kirchhoff,devKpr,ppr);
  ppr /= 3.;
  double keqpr = sqrt(1.5*devKpr.dotprod());

  static STensor3 devK;
  devK= devKpr;  // dev corotational kirchoff stress
  double p = ppr; // pressure

   // hardening
  this->hardening(q0,q1,T);
  fullVector<double> a(3), Da(3); // yield coefficients and derivatives in respect to plastic deformation
  this->getYieldCoefficients(q1,a);

  static STensor3 devN; // dev part of yield normal
  double trN = 0.; // trace part of yield normal
  static STensor3 N; // yield normal

  STensorOperation::zero(devN);
  STensorOperation::zero(N);

  double sigVM = keqpr;
  double Dgamma = 0.;
  double Gamma = 0.;

  double g0 = 0.;
  double z = a(1)/a(2)*pow(sigVM,1.-_n);
  double A = sqrt(1.5+1.*z*z/(3.*_n*_n));

  static fullMatrix<double> invJ(2,2); // inversed jacobian
  invJ.setAll(0.);

  double dAdz =0.;
  double dzdDgamma = 0.;
  double dzdsigVM = 0.;
  double dg0dsigVM = -1./(3.*Ge);

  if (q1->dissipationIsBlocked()){
    q1->getRefToDissipationActive() = false;
  }
  else{
    double f = a(2)*pow(sigVM,_n) - (a(1)*ppr+a(0));
    if (f>_tol){
      q1->getRefToDissipationActive() = true;
      // plasticity
      int ite = 0;
      int maxite = 1000; // maximal number of iters
      double val  = 1.5*a(2)*_n*pow(3.*fabs(p),_n-2.);
      q1->_nup = (val*p+ a(1)/3.)/(val*2.*p - a(1)/3.);
      double kk = 1./sqrt(1.+2.*q1->_nup*q1->_nup);
      A*=kk;

      fullVector<double> R(2); // residual of eqs to estimate Dgamma, Dpression and Gamma

      R(0) = Dgamma - g0*A;
      R(1) = a(2)*pow(sigVM,_n) - a(1)*(ppr+Ke*z*g0/_n) - a(0);

      double res = R.norm();

      while(res>_tol or ite<1){

        this->getYieldCoefficientDerivatives(q1,q1->_nup,Da);

        static fullMatrix<double> J(2,2);
        J.setAll(0.);

        dAdz = kk*z/(3.*_n*_n*A);
        dzdDgamma = (Da(1)*a(2) - a(1)*Da(2))/(a(2)*a(2))*pow(sigVM,1.-_n);
        dzdsigVM = a(1)/a(2)*(1.-_n)*pow(sigVM,-_n);



        J(0,0) = 1. - g0*dAdz*dzdDgamma;
        J(0,1) = -dg0dsigVM*A-g0*dAdz*dzdsigVM;

        J(1,0) =  Da(2)*pow(sigVM,_n) - Da(1)*(ppr+Ke*z*g0/_n) - Da(0) - a(1)*Ke*dzdDgamma*g0/_n;
        J(1,1) = a(2)*_n*pow(sigVM,_n-1) - a(1)*Ke/_n*(dzdsigVM*g0+z*dg0dsigVM);



        double detJ = J(0,0)*J(1,1) - J(1,0)*J(0,1);
        if (detJ == 0.) Msg::Error("the corrected system can not be solved: mlawNonLinearTVP::predictorCorrector_associatedFlow");

        invJ(0,0) = J(1,1)/detJ;
        invJ(0,1) = -J(0,1)/detJ;
        invJ(1,0) = -J(1,0)/detJ;
        invJ(1,1) = J(0,0)/detJ;

        double solDgamma = -invJ(0,0)*R(0)-invJ(0,1)*R(1);
        double solsigVM = -invJ(1,0)*R(0)-invJ(1,1)*R(1);

       // bool isSolved = J.luSolve(residual,sol);
       // if (!isSolved) Msg::Error("the corrected system can not be solved: mlawPowerYieldHyper::predictorCorrector");
        //sol.print("sol");

        if (solDgamma >0.1)
          Dgamma+= 0.1;
        else if (Dgamma+ solDgamma< 0.)
          Dgamma /= 2.;
        else
          Dgamma += solDgamma;

        if (sigVM + solsigVM< 0.)
          sigVM /= 2.;
        else
          sigVM += solsigVM;


        updateEqPlasticDeformation(q1,q0,q1->_nup,Dgamma);
        this->hardening(q0,q1,T);
        this->getYieldCoefficients(q1,a);

        g0 = (keqpr- sigVM)/(3.*Ge);
        z = a(1)/a(2)*pow(sigVM,1.-_n);
        A = kk*sqrt(1.5+1.*z*z/(3.*_n*_n));

        R(0) = Dgamma - g0*A;
        R(1) = a(2)*pow(sigVM,_n) - a(1)*(ppr+Ke*z*g0/_n) - a(0);

        res = R.norm();

        ite++;

        //Msg::Info("iter = %d, res = %e,  Dgamma = %e, sigVM/keqpr = %e",ite,res, Dgamma, sigVM/keqpr);

        if(ite > maxite){
          Msg::Error("No convergence for plastic correction in mlawPowerYieldHyper iter = %d, res = %e!!",ite,res);
          P(0,0) = P(1,1) = P(2,2) = sqrt(-1.);
          break;
        }
      }

      Gamma = z*g0/(_n*a(1));

      // update
      p += Ke*Gamma*a(1);

      double ff =a(2)*_n*pow(sigVM,_n-2.);
      devK*= 1./(1.+3.*Ge*Gamma*ff);

      // estimate yield normal
      devN = devK;
      devN *=  1.5*ff;
      trN = -a(1);

      N = devN;
      N(0,0) += trN/3.;
      N(1,1) += trN/3.;
      N(2,2) += trN/3.;

      // estimate exp(GammaN)
      static STensor3 expGN;
      static STensor3 GammaN;
      GammaN = N;
      GammaN *= Gamma;
      STensorOperation::expSTensor3(GammaN,_order,expGN,&dexpAdA);

      // update plastic deformation tensor
      STensorOperation::multSTensor3(expGN,Fp0,Fp1);
      // update IP
      updateEqPlasticDeformation(q1,q0,q1->_nup,Dgamma);

      // update elastic deformation tensor, corotational stress
      STensorOperation::inverseSTensor3(Fp1,Fpinv);
      STensorOperation::multSTensor3(F,Fpinv,Fe);
      STensorOperation::multSTensor3FirstTranspose(Fe,Fe,Ce);
      bool ok=STensorOperation::logSTensor3(Ce,_order,Ee,&DlnDCe,&DDlnDDCe);
      if(!ok)
      {
         P(0,0) = P(1,1) = P(2,2) = sqrt(-1.);
         return;
      }
      Ee *= 0.5;

      //
      // ThermoViscoElasticPredictor(Ee,q0->_Ee,q0,q1,Ke,Ge,Kde,Gde,DKe,DGe,DKde,DGde,KTsum,GTsum,DKDTsum,DGDTsum,T0,T,false);
      ThermoViscoElasticPredictor(Ee,q0->_Ee,q0,q1,Ke,Ge,T0,T,false,Bd_stiffnessTerm);
    }
    else{
      q1->getRefToDissipationActive() = false;
    }
  }

  // corotational Kirchhoff stress tenor

  STensor3& KS = q1->_kirchhoff;
  KS = devK;
  KS(0,0) += p;
  KS(1,1) += p;
  KS(2,2) += p;

  // first Piola Kirchhoff stress
  static STensor3 S;
  S*=(0.);
  for(int i=0; i<3; i++)
    for(int j=0; j<3; j++)
      for(int k=0; k<3; k++)
        for(int l=0; l<3; l++)
          S(i,j)+= KS(k,l)*DlnDCe(k,l,i,j);

  STensorOperation::zero(P);
  for(int i=0; i<3; i++)
    for(int j=0; j<3; j++)
      for(int k=0; k<3; k++)
        for(int l=0; l<3; l++)
          P(i,j) += Fe(i,k)*S(k,l)*Fpinv(j,l);

  // defo energy
  q1->_elasticEnergy=deformationEnergy(*q1);
  q1->getRefToPlasticEnergy() = q0->plasticEnergy();
  if (Gamma > 0){
    double dotKSN = dot(KS,N);
    q1->getRefToPlasticEnergy() += Gamma*dotKSN;
  }

  if (this->getMacroSolver()->getPathFollowingLocalIncrementType() == pathFollowingManager::DEFO_ENERGY){
    q1->getRefToIrreversibleEnergy() = q1->defoEnergy();
  }
  else if ((this->getMacroSolver()->getPathFollowingLocalIncrementType() == pathFollowingManager::PLASTIC_ENERGY) or
           (this->getMacroSolver()->getPathFollowingLocalIncrementType() == pathFollowingManager::DISSIPATION_ENERGY)){
    q1->getRefToIrreversibleEnergy() = q1->plasticEnergy();
  }
  else{
    q1->getRefToIrreversibleEnergy() = 0.;
  }

  if (stiff){
     // FILL THIS
      }
};

// Old IsotropicHardeningForce formulation
void mlawNonLinearTVP::getIsotropicHardeningForce(const IPNonLinearTVP *q0, IPNonLinearTVP *q1, const double T0, const double& T,
                                      const STensor3& DphiPDF, std::vector<double>* ddRdTT, std::vector<STensor3>* ddRdFdT) const{

    const double& Gamma_0 = q0->_Gamma;
    const double& Gamma = q1->_Gamma;
    const double& DgammaDT = q1->_DgammaDT;
    const double& dGammaDT_0 = q0->_DGammaDT;
    const double& dGammaDT = q1->_DGammaDT; // = 0. for debugging //DEBUGGING
    const STensor3& DgammaDF = q1->_DgammaDF;
    const STensor3& DGammaDF = q1->_DGammaDF;
    const double& gamma0 = q0->_epspbarre;
    const double& gamma1 = q1->_epspbarre;

    static STensor3 devMe, devX1, dDevXdT, dDevMeDT;
    static double pMe, pX1, dpXdT, dpMeDT;
    static double pMe_0, pX1_0, dpXdT_0, dpMeDT_0;

    STensorOperation::decomposeDevTr(q1->_ModMandel,devMe,pMe);
    STensorOperation::decomposeDevTr(q0->_ModMandel,devMe,pMe_0);
    STensorOperation::decomposeDevTr(q1->_backsig,devX1,pX1);
    STensorOperation::decomposeDevTr(q0->_backsig,devX1,pX1_0);
    STensorOperation::decomposeDevTr(q1->_DbackSigDT,dDevXdT,dpXdT);
    STensorOperation::decomposeDevTr(q0->_DbackSigDT,dDevXdT,dpXdT_0);
    STensorOperation::decomposeDevTr(q1->_DModMandelDT,dDevMeDT,dpMeDT);
    STensorOperation::decomposeDevTr(q0->_DModMandelDT,dDevMeDT,dpMeDT_0);

    pMe /= 3.; pX1 /= 3.; dpMeDT /= 3.; dpXdT /= 3.;
    pMe_0 /= 3.; pX1_0 /= 3.; dpMeDT_0 /= 3.; dpXdT_0 /= 3.;

    // dpMeDT = 0.; dpXdT = 0.; // for debugging only //DEBUGGING

    // get R
    double eta(0.),Deta(0.),DetaDT(0.),DDetaDTT(0.),DDetaDT(0.);
    fullVector<double> a(3), Da(3), DaDT(3), DDaDTT(3), DDaDT(3);
    getYieldCoefficients(q1,a);
    getYieldCoefficientDerivatives(q1,q1->_nup,Da);
    getYieldCoefficientTempDers(q1,T,DaDT,DDaDTT);

    if (_viscosity != NULL)
        _viscosity->get(q1->_epspbarre,T,eta,Deta,DetaDT,DDetaDTT,DDetaDT);
    double etaOverDt = eta/this->getTimeStep();
    double viscoTerm = etaOverDt*Gamma;

    const std::vector<double>& R0 = q0->_IsoHardForce;
    std::vector<double>& R = q1->_IsoHardForce;
    const std::vector<double>& dRdT_0 = q0->_dIsoHardForcedT;
    std::vector<double>& dRdT = q1->_dIsoHardForcedT;
    R.resize(3,0.);
    dRdT.resize(3,0.);

    double sigc(0.), Hc(0.), dsigcdT(0.), ddsigcdTT(0.);
    sigc = q1->_ipCompression->getR();
    Hc = q1->_ipCompression->getDR();
    dsigcdT = q1->_ipCompression->getDRDT();
    ddsigcdTT = q1->_ipCompression->getDDRDTT();

    R[0] = sigc * a(1) * (pMe-pX1);
    dRdT[0] = dsigcdT*a(1)*(pMe-pX1) + sigc*( DaDT(1)*(pMe-pX1) + a(1)*(dpMeDT-dpXdT) );
    R[1] =  sigc * a(0);
    dRdT[1] = dsigcdT * a(0) + sigc * DaDT(0) ;
    if (Gamma>0. and etaOverDt>0.){
       R[2] = sigc * pow(eta*Gamma/this->getTimeStep(),_p) ;
       dRdT[2] =  dsigcdT * pow(eta*Gamma/this->getTimeStep(),_p) +
                  sigc * _p*pow(eta*Gamma/this->getTimeStep(),_p-1)*(DetaDT*Gamma + eta*dGammaDT)/this->getTimeStep() ;
    }

    // dRdF
    const std::vector<STensor3>& dRdF_0 = q0->_dIsoHardForcedF;
    std::vector<STensor3>& dRdF = q1->_dIsoHardForcedF;
    dRdF.resize(3,0.);

    for (int i=0; i<3; i++)
      for (int j=0; j<3; j++){
        dRdF[0](i,j) = Hc*DgammaDF(i,j)*a(1)*(pMe-pX1) + sigc*( Da(1)*DgammaDF(i,j)*(pMe-pX1) + a(1)*DphiPDF(i,j) );
        dRdF[1](i,j) = Hc*DgammaDF(i,j)*a(0) + sigc*Da(0)*DgammaDF(i,j);
        }

    if (Gamma>0 and etaOverDt>0){
      for (int i=0; i<3; i++)
        for (int j=0; j<3; j++){
          dRdF[2](i,j) = Hc*DgammaDF(i,j) *pow(eta*Gamma/this->getTimeStep(),_p) +
                            sigc* _p*pow(eta*Gamma/this->getTimeStep(),_p-1)*(Deta*DgammaDF(i,j) *Gamma + eta* DGammaDF(i,j))/this->getTimeStep() ;
        }
    }

    // ddRdTT
    double delT = T-T0;
    if(ddRdTT!=NULL){

       ddRdTT->at(0) =  ddsigcdTT *a(1)*(pMe-pX1) + 2.*dsigcdT *( DaDT(1)*(pMe-pX1) + a(1)*(dpMeDT-dpXdT) ) + sigc*( DDaDTT(1)*(pMe-pX1) + 2.*DaDT(1)*(dpMeDT-dpXdT) );
       ddRdTT->at(1) =  ddsigcdTT *a(0) +  2.*dsigcdT *DaDT(0) + sigc* DDaDTT(0) ;

       if (Gamma>0 and etaOverDt>0){
        ddRdTT->at(2) = ddsigcdTT * pow(eta*Gamma/this->getTimeStep(),_p)
                        + 2.*dsigcdT *_p*pow(eta*Gamma/this->getTimeStep(),_p-1)*(DetaDT*Gamma + eta*dGammaDT)/this->getTimeStep()
                        + sigc* _p*(_p-1)*pow(eta*Gamma/this->getTimeStep(),_p-2)*pow( ((DetaDT*Gamma + eta*dGammaDT)/this->getTimeStep()) ,2)
                        + sigc* _p*pow(eta*Gamma/this->getTimeStep(),_p-1)* (DDetaDTT*Gamma + 2.*DetaDT*dGammaDT )/this->getTimeStep() ;
       }

       if (delT>1e-10){
        double DphiPDT = dpMeDT- dpXdT;
        double DDphiPDTT = 0.; // (DphiPDT - (trMe-trMe_0+trX1_0-trX1)/delT)/delT; // 2*DphiPDT/delT - 2*(trMe-trMe_0+trX1_0-trX1)/pow(delT,2); // DEBUGGING
        ddRdTT->at(0) += sigc * a(1) * DDphiPDTT ;

        if (Gamma>0 and etaOverDt>0){
         double DDGammaDTT =  0.; // 2*dGammaDT/delT - 2*(Gamma-Gamma_0)/pow(delT,2); // DEBUGGING
         ddRdTT->at(2) += sigc* _p*pow(eta*Gamma/this->getTimeStep(),_p-1)*(eta*DDGammaDTT)/this->getTimeStep() ;
        }
       }
       q1->_ddIsoHardForcedTT = *ddRdTT;
    }
    // ddRdTT = 2*dRdT/(T-T0) - 2*(R-R0)/pow((T-T0),2); // ddGammaDTT is very difficult to estimate

    // ddRdTF
    if(ddRdFdT!=NULL){
        /*
      for (int i=0; i<3; i++)
        for (int j=0; j<3; j++){
          ddRdFdT->at(0)(i,j) = 0.;
        }*/

      for (int k=0; k<3; k++){
        STensorOperation::zero(ddRdFdT->at(k));
        if(delT>1e-10){
          ddRdFdT->at(k) = dRdF[k];
          ddRdFdT->at(k) -= dRdF_0[k];
          ddRdFdT->at(k) *= 1/delT;
        }
      }
    }
};
