
c
c	>>>>>>>>>>>>>>>>>>>>>>>>>>>>>><<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
	subroutine firstABQ( DFGRD0, STRAN1, STRESS, STATEV, TEMP
     &			, PROPS, NOEL, NSTATV, NPROPS, CMNAME, i6 )
	implicit none
c       ::::  Arguments of UMAT
	integer NPROPS, NSTATV, NTENS
	parameter(ntens=6)
	CHARACTER*8 CMNAME
	real*8 STRESS(NTENS),STATEV(NSTATV),
     1  DDSDDE(NTENS,NTENS),STRAN1(NTENS),DSTRAN(NTENS),
     2  TIM(2),PROPS(NPROPS),DROT(3,3),DFGRD0(3,3),DFGRD1(3,3)
	real*8 PNEWDT, DTIME, STRESS1(NTENS), tmp, q(4), angl
	real*8 TEMP, DTEMP
	integer NOEL, NPT, LAYER, KSTEP, KINC
	integer i, j, idir
	integer*4 i6(3,3)
	character*2 code
c
	DTEMP= 0.
	KSTEP= 0
	KINC= 0
	LAYER= 1
	PNEWDT= 1.D0
	TIM(1)= 0.D0
	TIM(2)= 0.D0
	DTIME= 0.D0
	do i= 1,3
	  do j= 1,3
	    DFGRD0(i,j)= 0.D0
	    DFGRD1(i,j)= 0.D0
	    DROT(i,j)= 0.D0
	  end do
	  DFGRD0(i,i)= 1.D0
	  DFGRD1(i,i)= 1.D0
	  DROT(i,i)= 1.D0
	end do
	do i= 1,6
	  STRAN1(i)= 0.D0
	  DSTRAN(i)= 0.D0
	  STRESS1(i)= 0.D0
	end do
	tmp=0.D0
	if ( .false. ) then
	  write(*,*) 'Rotation of sample:'
	  read(*,*) code
	  if ( code.eq.'RD' ) then
	    q(1)= 1.D0
	    q(2)= 0.D0
	    q(3)= 0.D0
	    q(4)= 0.D0
	  else if ( code.eq.'TD' ) then
	    angl= 90.
	    q(1)= cos(angl*3.141592/360.)
	    q(2)= 0.D0
	    q(3)= 0.D0
	    q(4)= sqrt(1.d0 - q(1)**2)
	  else if ( code.eq.'TT' ) then
	    q(1)= cos(3.141592/3.)
	    q(2)= sin(3.141592/3.) / sqrt(3.d0)
	    q(3)= q(2)
	    q(4)= q(2)
	  else
	    write(*,*) 'code must be RD, TD or TT'
	    stop
	  end if
	  call KQ4_mat( q, DROT )
	  do i= 1,3
	    do j= 1,3
	      DFGRD1(i,j)= DROT(i,j)
	    end do
	  end do
	  tmp= 1.D0
	end if
	call call_UM( DSTRAN, DROT, DFGRD1, STRESS, DDSDDE
     &			, DFGRD0, STRAN1, STRESS1, STATEV
     &			, TEMP, DTEMP
     &			, TIM, DTIME, PROPS, NSTATV, NPROPS
     &			, KSTEP, KINC, NOEL, NPT, LAYER
     &			, tmp, CMNAME, i6 )
c
	do i= 1,3
	  do j= 1,3
	    DFGRD0(i,j)= 0.D0
	    DFGRD1(i,j)= 0.D0
	  end do
	  DFGRD0(i,i)= 1.D0
	  DFGRD1(i,i)= 1.D0
	end do
	return
	end
c
c	>>>>>>>>>>>>>>>>>>>>>>>>>>>>>><<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
	subroutine prepBC_F( DF, i9, nincr, nstress, istress
     &				, DTIME, F0, F_end, i6 )
	implicit none
	real*8 F0(3,3), F_end(3,3), DF(3,3), DTIME
	integer i, j, nincr
	integer*4 i9(3,3), i6(3,3), prescdef(3,3), nstress
	logical istress(6)
	nincr= 1
	DTIME= 1.D0
	do i= 1,3
	  do j= 1,3
	    F0(i,j)= F_end(i,j)
	  end do
	  read(1,*) ( F_end(i,j), j=1,3 )
	  do j= 1,3
	    DF(i,j)= ( F_end(i,j)-F0(i,j) ) / nincr
	  end do
	end do
	do i= 1,3
	  read(1,*) ( prescdef(i,j), j= 1,3 )
	end do
	do i= 1,6
	  istress(i) = .true.
	end do
	nstress= 6
	do i= 1,3
	  do 15 j= i,3
	    if ( prescdef(i,j).eq.1 .and. prescdef(j,i).eq.1 ) then
	      nstress= nstress - 1
	      istress(i6(i,j)) = .false.
	      i9(i,j)= 0
	      i9(j,i)= 0
	      cycle
	    else if ( prescdef(i,j).eq.0 ) then
	      i9(j,i)= 0
	      i9(i,j)= i6(i,j)
	    else if ( prescdef(j,i).eq.0 ) then
	      i9(i,j)= 0
	      i9(j,i)= i6(i,j)
	    end if
	    if ( i.eq.j .or. prescdef(i,j).eq.1 
     &			.or. prescdef(j,i).eq.1 ) cycle
	    write(*,*) ' Prob in defining deformation mode:', i,j
	    write(*,*) prescdef
	    write(*,*) i9
	    stop
  15	  continue
	end do
	return
	end
c	>>>>>>>>>>>>>>>>>>>>>>>>>>>>>><<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
	subroutine prepBC_L( L0, prescdef, nincr, nstress, istress
     &				, DTIME, defo, def0, idg, i6 )
	implicit none
	real*8 L0(3,3), L_sa(3,3), defo, def0, DTIME
	integer i, j, idg, nincr, kstep
	integer*4 prescdef(3,3), i6(3,3), nstress
	logical istress(6)
	DTIME= 1.D0
	if ( idg.eq.0 ) then
	  do i= 1,3
	    read(1,*) ( L0(i,j), j=1,3 )
	  end do
	  do i= 1,3
	    read(1,*) ( prescdef(i,j), j=1,3 )
	  end do
	else
	  do i= 1,3
	    do j= 1,3
	      L0(i,j)= 0.D0
	      prescdef(i,j)= 1
	    end do
	  end do
	  if ( idg.eq.1 ) then
	    L0(idg,idg)= defo - def0
	    prescdef(2,2)= 0
	    prescdef(3,3)= 0
	    nincr= L0(1,1) / 0.01
	  else if ( idg.eq.2 ) then
	    L0(idg,idg)= defo - def0
	    prescdef(1,1)= 0
	    prescdef(3,3)= 0
	    nincr= L0(2,2) / 0.01
	  else
	    L0(1,2)= defo - def0
	    nincr= L0(1,2) / 0.017
	  end if
	  nincr= max( 3, nincr )
	  def0= defo
	end if
	do i= 1,3
	  do j= 1,3
	    L0(i,j)= L0(i,j) / nincr
	  end do
	end do
	do i= 1,6
	  istress(i) = .true.
	end do
	nstress= 6
	do i= 1,3
	  do j= i,3
	    if ( prescdef(i,j).eq.1 .and. prescdef(j,i).eq.1 ) then
	      nstress= nstress - 1
	      istress(i6(i,j)) = .false.
	    end if
	  end do
	end do
	return
	end
c
c	>>>>>>>>>>>>>>>>>>>>>>>>>>>>>><<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
	subroutine prepUM_L( DROT, F1, L, F0 )
	implicit none
	real*8 DF(3,3), F0(3,3), F1(3,3), L(3,3), De(6), DROT(3,3)
	real*8 omega(3), tmp, angle, q(4), si
	integer*4 i, j, k
	
c	:::  Compute deformation gradient tensor
	call mkfij( L, DF )
	do i= 1,3
	  do j= 1,3
	    tmp= 0.D0
	    do k= 1,3
	      tmp= tmp + DF(i,k) * F0(k,j)
	    end do
	    F1(i,j)= tmp
	  end do
	end do
c	:::  Compute the rigid body spin (Jaumann) & the associated rotation increment
	omega(1)= ( L(2,3) - L(3,2) ) / 2.D0
	omega(2)= ( L(3,1) - L(1,3) ) / 2.D0
	omega(3)= ( L(1,2) - L(2,1) ) / 2.D0
	angle= 0.d0
	do i= 1,3
	  q(i+1)= -omega(i)
c	  (change sign to conform to exponential map)
	  angle= angle + q(i+1)**2
	end do
	if ( angle.gt.1.d-10 ) then
	  angle= dsqrt( angle ) / 2.d0
	  q(1)= dcos( angle )
	  si= dsin( angle )
	  si= si / angle / 2.d0
	  do i= 1,3
	    q(i+1)= q(i+1) * si
	  end do
	  call KQ4_mat( q, DROT )
	else
	  do i= 1,3
	    do j= 1,3
	      DROT(i,j)= 0.D0
	    end do
	    DROT(i,i)= 1.D0
	  end do
	end if
	return
	end
c
c	************************************************
c	***  Converts quaternion into rotation matrix
c	************************************************
	Subroutine Kq4_mat( q, mat )
	implicit none
	real*8 q(4), mat(3,3), tmp1, tmp2
	integer i,j
	tmp1= q(1)**2 - 0.5d0
	do i= 1,3
	  mat(i,i)= q(i+1)**2 + tmp1
	end do
	tmp1= q(2)*q(3)
	tmp2= q(1)*q(4)
	mat(1,2)= tmp1 - tmp2
	mat(2,1)= tmp1 + tmp2
	tmp1= q(2)*q(4)
	tmp2= q(1)*q(3)
	mat(1,3)= tmp1 + tmp2
	mat(3,1)= tmp1 - tmp2
	tmp1= q(3)*q(4)
	tmp2= q(1)*q(2)
	mat(2,3)= tmp1 - tmp2
	mat(3,2)= tmp1 + tmp2
	do i=1,3
	  do j= 1,3
	    mat(i,j)= 2.d0 * mat(i,j)
	  end do
	end do
	return
	end
c
c	>>>>>>>>>>>>>>>>>>>>>>>>>>>>>><<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
	subroutine prepUM_F( DROT, L, Finv, F1, DF, F0 )
	implicit none
	real*8 DF(3,3), F0(3,3), F1(3,3), L(3,3), DROT(3,3)
	real*8 tmp, Finv(3,3), mat(3,3), mat2(3,3)
	logical last
	integer*4 i, j, k, ll
	last= .false.
  1	do i= 1,3
	  do j= 1,3
	    Finv(i,j)= F0(i,j) + DF(i,j) / 2.D0
	    F1(i,j)= F0(i,j) + DF(i,j)
	  end do
	end do
	call KInv33( Finv, tmp )
c	call prod( L, Finv, DF )
	call prod( L, DF, Finv )
	do i= 1,3
	  mat(i,i)= 1.D0
	  mat2(i,i)= 1.D0
	  do j= i+1,3
	    mat(i,j)= ( L(i,j) - L(j,i) ) / 4.D0
	    mat(j,i)= -mat(i,j)
	    mat2(i,j)= mat(j,i)
	    mat2(j,i)= mat(i,j)
	  end do
	end do
	call KInv33( mat2, tmp )
	call prod( DROT, mat2, mat )
	return
	end
c
c	>>>>>>>>>>>>>>>>>>>>>>>>>>>>>><<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
	subroutine call_UM( DSTRAN1, DROT, DFGRD1, STRESS, DDSDDE
     &			, DFGRD0, STRAN1, STRESS1, STATEV
     &			, TEMP, DTEMP
     &			, TIM, DTIME, PROPS, NSTATV, NPROPS
     &			, KSTEP, KINC, NOEL, NPT, LAYER
     &			, PNEWDT, CMNAME, i6 )
	implicit none
c       ::::  Arguments of UMAT
	integer ntens, mxstatv
	parameter(ntens=6,mxstatv=200000)
	integer NPROPS, NSTATV
	CHARACTER*8 CMNAME
	real*8 STRESS(NTENS),STATEV(NSTATV),
     1  DDSDDE(NTENS,NTENS),DDSDDT(NTENS),DRPLDE(NTENS),
     2  STRAN(NTENS),DSTRAN(NTENS),TIM(2),PREDEF(1),DPRED(1),
     3  PROPS(NPROPS),COORDS(3),DROT(3,3),
     4  DFGRD0(3,3),DFGRD1(3,3)
	real*8 SSE, SPD, SCD, RPL, DTIME, TEMP, CELENT, DRPLDT
	real*8 PNEWDT, DTEMP
	integer NDI, NSHR, NOEL, NPT, LAYER, KSPT, KSTEP, KINC
	logical last
c	:::: Other variables
	real*8 STRAN1(6), STRESS1(6), DSTRAN1(6), STATEV1(MXSTATV)
	integer i, j
	integer*4 i6(3,3)
	last= .false.
	if ( PNEWDT.eq.1.D0 ) last= .true.
	PNEWDT= 1.D0
	if ( NSTATV.gt.MXSTATV ) then
	  write(*,*) 'Prob in call_UM', nstatv, mxstatv
	  stop
	end if
	NPT= 1
	do i= 1,6
	  STRAN(i)= STRAN1(i)
	  DSTRAN(i)= DSTRAN1(i)
	end do
	do i= 4,6
	  DSTRAN(i)= 2.D0*DSTRAN(i)
	end do
	do j= 1,nstatv
	  statev1(j)= statev(j)
	end do
	call Krots3( DROT, STRESS1, STRESS, i6 )
c	do i= 1,6
c	  STRESS(i)= STRESS1(i)
c	end do
c
	call umat_cp( STRESS,STATEV,DDSDDE,SSE,SPD,SCD,
     &		RPL,DDSDDT,DRPLDE,DRPLDT,STRAN,DSTRAN,TIM,
     &		DTIME,TEMP,DTEMP,PREDEF,DPRED,CMNAME,NDI,NSHR,
     &		NTENS,NSTATV,PROPS,NPROPS,COORDS,DROT,PNEWDT,
     &		CELENT,DFGRD0,DFGRD1,NOEL,NPT,
     &		LAYER,KSPT,KSTEP,KINC )
c
	if ( PNEWDT.lt.1.D0 .and. KINC.gt.0 ) then
c	  write(*,*) ' Deformation increment is too large'
c     &				, PNEWDT, NOEL, KSTEP, KINC
	  last= .false.
	end if
c
	if ( last ) then
	  do i= 1,6
	    STRAN1(i)= STRAN(i) + DSTRAN(i)
	    STRESS1(i)= STRESS(i)
	  end do
	  do i= 1,3
	    do j= 1,3
	      DFGRD0(i,j)= DFGRD1(i,j)
	    end do
	  end do
	else
	  do j= 1,nstatv
	    statev(j)= statev1(j)
	  end do
	end if
	return
	end
c
c	>>>>>>>>>>>>>>>>>>>>>>>>>>>>>><<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
	subroutine prepNR_L( NR, DDSDDE, stress, istress )
	implicit none
	real*8 NR(6,6), DDSDDE(6,6), stress(6), dsdw(6,3)
	integer i, j
	logical istress(6)
	if ( istress(4).or.istress(5).or.istress(6) ) then
	  call K_RDR_R( dsdw, stress, 1.D0, 1.D0 )
	  do j=1,3
	    do i= 4,6
	      dsdw(i,j)= dsdw(i,j) * 0.5D0
	    end do
	  end do
	  do i= 1,6
	    DDSDDE(i,4)= DDSDDE(i,4) - dsdw(i,3)
	    DDSDDE(i,5)= DDSDDE(i,5) + dsdw(i,2)
	    DDSDDE(i,6)= DDSDDE(i,6) - dsdw(i,1)
	  end do
	end if
	do i= 1,6
	  if ( istress(i) ) then
c	    >>  Constraints on the stress
	    do j= 1,6
	      NR(i,j)= DDSDDE(i,j)
	    end do
	  else
c	    >>  Pseudo-constraints on the strain
	    do j= 1,6
	      NR(i,j)= 0.D0
	    end do
	    NR(i,i)= 1.D0
	  end if
	end do
c
	return
	end
c
c	>>>>>>>>>>>>>>>>>>>>>>>>>>>>>><<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
	subroutine prepNR_F( dSdF, L_sa, Finv, stress, ddsdde
     &				, istress, i9, i6 )
	implicit none
	real*8 dSdF(6,6), L_sa(3,3), Finv(3,3), stress(6), ddsdde(6,6)
	real*8 tmp, tmp2, dSdW(6,3), dSdL(3,3)
	integer i, j, k, l, m, n
	integer*4 i9(3,3), i6(3,3)
	logical istress(6)
c	=== Derivative of S relative to the spin
	call K_RDR_R( dSdW, STRESS, 1.D0, 1.D0 )
c	=== Loop on the stress components
	do i= 1,6
	  if ( .not.istress(i) ) then
c	    >>  Pseudo-Constraints on the strain
	    do j= 1,6
	      dSdF(i,j)= 0.D0
	    end do
	    dSdF(i,i)= 1.D0
	    cycle
	  end if
c	  >>  Constraints on the stress
c	  -- Derivative of S relative to the velocity gradient tensor
	  do j= 4,6
	    DDSDDE(i,j)= DDSDDE(i,j) * 2.D0
	  end do
	  do j= 1,3
	    do k= 1,3
	      dSdL(j,k)= DDSDDE(i,i6(j,k))
	    end do
	  end do
	  dSdL(2,3)= dSdL(2,3) + dSdW(i,1)
	  dSdL(3,2)= dSdL(3,2) - dSdW(i,1)
	  dSdL(3,1)= dSdL(3,1) + dSdW(i,2)
	  dSdL(1,3)= dSdL(1,3) - dSdW(i,2)
	  dSdL(1,2)= dSdL(1,2) + dSdW(i,3)
	  dSdL(2,1)= dSdL(2,1) - dSdW(i,3)
c
c	  -- Derivative of S relative to the DF components
	  do k= 1,3
	    do l= 1,3
	      j= i9(k,l)
	      if ( j.eq.0 ) cycle
	      tmp= 0.D0
	      do m= 1,3
		tmp2= 0.D0
		do n= 1,3
		  tmp2= tmp2 + dSdL(m,n) * L_sa(l,n)
		end do
		tmp= tmp + ( dSdL(m,l) - tmp2/2.D0 ) * Finv(m,k)
	      end do
	      dSdF(i,j)= tmp
	    end do
	  end do
	end do
c
	return
	end
c
c	>>>>>>>>>>>>>>>>>>>>>>>>>>>>>><<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
	subroutine solve_NR( Misf, dX, ksi, NR, BB, dX_mx, Misf1
     &				, dMdksi, idNR, isol, n, nmax )
	implicit none
	integer isol, n, nmax, npi
	real*8 Misf, dX(nmax), NR(nmax,nmax), BB(nmax), dX_mx(nmax)
	real*8 Misf1, Misf11, Misf12, dMdksi, det, tmp, tmp2, ksi
	integer*4 idNR(nmax)
	integer i, j
	logical forceNR, nochk
c	====  Check that the misfit is decreasing
	if ( isol.ne.-2 ) then
	  Misf= 0.d0
	  do i= 1,n
	    Misf= Misf + bb(i)**2
	  end do
	  Misf= sqrt( Misf )
	end if
	if ( isol.eq.-3 ) return
	forceNR= .false.
	nochk= .false.
	if ( isol.lt.0 ) then
	  isol= -isol
	  forceNR= .true.
	  if ( isol.eq.2 ) nochk= .true.
	end if
	if ( Misf.lt.max(Misf1,1.d-20) .or. forceNR ) then
	  ksi= 1.d0
	  isol= 0
c	  ====  Solve linear equation set
	  call LUDCMP(NR,idNR,det,N,NMAX)
	  if ( det.eq.0.d0 ) then 
	    write(*,*) ' Singular NR matrix', n, nmax
	    isol= 10
	    return
	  end if
	  do i= 1,n
	    dX(i)= bb(i)
	  end do
	  call LUBKSB(dX,NR,idNR,N,NMAX,NMAX)
	else if ( Misf.lt.-max(Misf1,1.d-20) ) then
	  ksi= 1.d0
	  isol= 2
c	  ====  Follow gradient
	  do i= 1,n
	    dX(i)= 0.D0
	    do j= 1,n
	      dX(i)= dX(i) + NR(j,i) * BB(j)
	    end do
	  end do
	else
	  ksi= 0.5d0
	  isol= 1
	  return
	end if
	if ( nochk ) return
c	====  Ensure that the correction is not excessive
	tmp= 0.d0
	do i= 1,n
	  tmp2= abs(dX(i)/dX_mx(i))
	  if ( tmp.gt.tmp2 ) cycle
	  tmp= tmp2
	  j= i
	end do
	if ( tmp.gt.1.D0 ) then
	  ksi= 1.D0 / tmp
	  do i= 1,n
	    dX(i)= dX(i) / tmp
	  end do
	end if
c	write(*,89) 'nr', ( dX(i), i=1,n )
c	write(*,89) 'gr', ( dX_gr(i), i=1,n )
  89	format(a2,20E12.4)
	return
	end
c
c	>>>>>>>>>>>>>>>>>>>>>>>>>>>>>><<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
	subroutine postNR_L( L, dX, ksi, L0, prescdef, i6 )
	implicit none
	real*8 L(3,3), L0(3,3), De(6), dX(6), tmp, ksi
	integer*4 prescdef(3,3), i6(3,3), i, j
c	>> New estimate of the strain tensor
	if ( ksi.gt.0.D0 ) then
	  do i= 1,3
	    De(i)= L(i,i) + dX(i) 
	    do j=i+1,3
	      De(i6(i,j))= ( L(i,j) + L(j,i)
     &				+ dX(i6(i,j)) ) / 2.D0
	    end do
	  end do
	else
	  ksi= -ksi
	  do i= 1,3
	    tmp= dX(i)
	    dX(i)= ksi * dX(i)
	    De(i)= L(i,i) - tmp + dX(i) 
	    do j=i+1,3
	      tmp= dX(i6(i,j))
	      dX(i6(i,j))= ksi * dX(i6(i,j))
	      De(i6(i,j))= ( L(i,j) + L(j,i)
     &				- tmp + dX(i6(i,j)) ) / 2.D0
	    end do
	  end do
	end if
c
	do i= 1,3
	  L(i,i)= De(i6(i,i))
	  do j= i+1,3
	    L(i,j)= De(i6(i,j))
	    L(j,i)= L(i,j)
	    if ( prescdef(i,j).eq.0 ) then
	      tmp= L0(i,j) - L(i,j)
	      L(i,j)= L(i,j) + tmp
	      L(j,i)= L(j,i) - tmp
	    else if ( prescdef(j,i).eq.0 ) then
	      tmp= L0(j,i) - L(j,i)
	      L(i,j)= L(i,j) - tmp
	      L(j,i)= L(j,i) + tmp
	    else
	      tmp= (L0(j,i) - L(j,i)) / 2.d0
	      L(i,j)= L(i,j) + tmp
	      L(j,i)= L(j,i) + tmp
	    end if
	  end do
	end do
	return
	end
c
c	>>>>>>>>>>>>>>>>>>>>>>>>>>>>>><<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
	subroutine postNR_F( DF, dX, ksi, i9 )
	implicit none
	real*8 DF(3,3), dX(6), ksi, tmp
	integer*4 i9(3,3), i, j
c	>>> New value of the deformation gradient tensor:
	if ( ksi.gt.0.D0 ) then
	  do i= 1,3
	    do j= 1,3
	      if ( i9(i,j).eq.0 ) cycle
	      DF(i,j)= DF(i,j) + dX(i9(i,j))
	    end do
	  end do
	else
	  ksi= -ksi
	  do i= 1,3
	    do j= 1,3
	      if ( i9(i,j).eq.0 ) cycle
	      tmp= dX(i9(i,j))
	      dX(i9(i,j))= ksi * dX(i9(i,j))
	      DF(i,j)= DF(i,j) - tmp + dX(i9(i,j))
	    end do
	  end do
	end if
	return
	end
c
c	>>>>>>>>>>>>>>>>>>>>>>>>>>>>>><<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
	subroutine mkfij( L, F )
	implicit none
	real*8 L(3,3), A(3,3), F(3,3), X(3,3), DG(3,3)
	real*8 x1(3,3)
	integer i, j, k, ndeg
	ndeg= 15
	do i= 1,3
	  do j= 1,3
	    DG(i,j)= L(i,j)
	  end do
	end do

	do i= 1,3
	  do j= 1,3
	    X(i,j)= DG(i,j) 
	    X1(i,j)= X(i,j)
	    F(i,j)= X(i,j)
	  end do
	  F(i,i)= 1.D0 + F(i,i)
	end do
	do k= 2,ndeg
	  call prod( A, X, X1 )
	  do i= 1,3
	    do j= 1,3
	      X1(i,j)= A(i,j) / k
	      F(i,j)= F(i,j) + X1(i,j)
	    end do
	  end do
	end do
	end
c
c	>>>>>>>>>>>>>>>>>>>>>>>>>>>>>><<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
	subroutine prod( C, A, B )
	implicit none
	real*8 C(3,3), B(3,3), A(3,3), x
	integer i, j, k
	do i= 1,3
	  do j= 1,3
	    x= 0.D0
	    do k= 1,3
	      x= x + A(i,k) * B(k,j)
	    end do
	    C(i,j)= x
	  end do
	end do
	return
	end
c
c	*******************************************************
c	*** LU decomposition of square matrix
c	*******************************************************
	SUBROUTINE LUDCMP(A,INDX,D,N,NP)
	implicit none
	integer n, np, nmax
	parameter(nmax=100)
	real*8 A(NP,NP),VV(nmax), D, tiny, dum
	real*8 AAMAX, SUM
	integer*4 INDX(NP), i, j, k, imax
	TINY=1.D-30
	if ( np.gt.nmax ) then
	  write(*,*) 'increase nmax in ludcmp', nmax, np
	  call xit
	end if
	D=1.d0
	DO 12 I=1,N
	  AAMAX=0.d0
	  DO 11 J=1,N
	    IF (ABS(A(I,J)).GT.AAMAX) AAMAX=ABS(A(I,J))
11	  CONTINUE
	  IF (AAMAX.lt.tiny) then
	    write(*,*) 'Singular matrix'
	    stop
	  end if
	  VV(I)=1.d0/AAMAX
12	CONTINUE
	DO 19 J=1,N
	  DO 14 I=1,J-1
	    SUM=A(I,J)
	    DO 13 K=1,I-1
	      SUM=SUM-A(I,K)*A(K,J)
13	    CONTINUE
	    A(I,J)=SUM
14	  CONTINUE
c
	  AAMAX=0.d0
	  DO 16 I=J,N
	    SUM=A(I,J)
	    DO 15 K=1,J-1
	      SUM=SUM-A(I,K)*A(K,J)
15	    CONTINUE
	    A(I,J)=SUM
	    DUM=VV(I)*ABS(SUM)
	    IF (DUM.GE.AAMAX) THEN
	      IMAX=I
	      AAMAX=DUM
	    ENDIF
16	  CONTINUE
	  IF (J.NE.IMAX)THEN
	    DO 17 K=1,N
	      DUM=A(IMAX,K)
	      A(IMAX,K)=A(J,K)
	      A(J,K)=DUM
17	    CONTINUE
	    D=-D
	    VV(IMAX)=VV(J)
	  ENDIF
	  INDX(J)=IMAX
	  D= D * A(J,J)
	  if ( J.eq.N ) cycle
c	  IF(A(J,J).EQ.0.d0) A(J,J)=TINY
	  IF (abs(A(J,J)).lt.tiny) then
	    write(*,*) 'Singular matrix.'
	    stop
	  end if
	  DO 18 I=J+1,N
	    A(I,J)=A(I,J) / A(J,J)
18	  CONTINUE
19	CONTINUE
	RETURN
	END

c
c	*******************************************************
c	*** Equation set solution after LU decomposition
c	*******************************************************
	SUBROUTINE LUBKSB(B,A,INDX,N,NP,NPI)
	implicit none
	integer n, np, npi
	real*8 A(NP,NP),B(NPI), sum
	integer*4 INDX(NP), ii, i, j, k, ll
	II=0
	DO 12 I=1,N
	  LL=INDX(I)
	  SUM=B(LL)
	  B(LL)=B(I)
	  IF (II.NE.0)THEN
	    DO 11 J=II,I-1
	      SUM=SUM-A(I,J)*B(J)
11	    CONTINUE
	  ELSE IF (SUM.NE.0.d0) THEN
	    II=I
	  ENDIF
	  B(I)=SUM
12	CONTINUE
	DO 14 I=N,1,-1
	  SUM=B(I)
	  IF(I.LT.N)THEN
	    DO 13 J=I+1,N
	      SUM=SUM-A(I,J)*B(J)
13	    CONTINUE
	  ENDIF
	  B(I)=SUM/A(I,I)
14	CONTINUE
	RETURN
	END
c
c	*******************************************************
c	*** Matrix inversion after LU decomposition
c	*******************************************************
	subroutine KinvLU( NR, idNR, N, NP )
	implicit none
	integer n, np, nmax
	parameter(nmax=100)
	real*8 NR(NP,NP), M(nmax,nmax)
	integer*4 idNR(NP), i, j
	if ( np.gt.nmax ) then
	  write(*,*) 'increase nmax in kinvLU', nmax, np
	  call xit
	end if
	do i= 1,n
	  do j= 1,n
	    M(j,i)= 0.D0
	  end do
	  M(i,i)= 1.D0
	  call LUBKSB(M(1,i),NR,idNR,N,NP,NMAX)
	end do
	do i= 1,n
	  do j= 1,n
	    NR(i,j)= M(i,j)
	  end do
	end do
	return
	end
c
c	*******************************************************
c	*** Computes vonMises equivalent strain or stress
c	*******************************************************
	function vonmises( x, k )
	implicit none
	real*8 x(6), vonmises, tmp
	integer k, j
c
	tmp= (X(1)-X(2))**2 + (X(3)-X(2))**2 + (X(1)-X(3))**2
	tmp= tmp / 3.D0
	do j= 4,6
	  tmp= tmp + X(j)**2
	end do
	if ( k.eq.1 ) then
	  vonmises= sqrt( tmp * 1.5d0 )
	else
	  vonmises= sqrt( tmp / 1.5d0 )
	end if 
c
	return
	end
c
c	****************************************************
	subroutine KInv33( M, det )
	implicit none
	real*8 Mi(3,3), M(3,3), det
	integer*4 i, j, ii(3), jj(3)
	ii(1)= 2
	ii(2)= 3
	ii(3)= 1
	jj(1)= 3
	jj(2)= 1
	jj(3)= 2
	do i= 1,3
	  do j= 1,3
	    Mi(j,i)= M(ii(i),ii(j)) * M(jj(i),jj(j))
     &		- M(ii(i),jj(j)) * M(jj(i),ii(j))
	  end do
	end do
	det= M(1,1)*Mi(1,1) + M(1,2)*Mi(2,1) + M(1,3)*Mi(3,1)
	if ( abs(det).lt.1.d-30 ) then
	  write(*,*) 'cannot invert in Kinv33', det
	  call xit
	end if
	do i= 1,3
	  do j= 1,3
	    M(i,j)= Mi(i,j) / det
	  end do
	end do
	return
	end
c
c	****************************************************
c	/// Derive rotated sym tensor relative to the spin
	subroutine K_RDR_R( M, D0, fac, sq2 )
	implicit none
	real*8 M(6,3), D0(6), sq2, fac, D(6)
	integer i
	do i= 1,6
	  D(i)= D0(i) * fac
	end do
	M(1,1)=  0.D0
	M(1,2)=  sq2 * D(5) 
	M(1,3)= -sq2 * D(4) 
	M(2,1)= -sq2 * D(6) 
	M(2,2)=  0.D0
	M(2,3)= -M(1,3) 
	M(3,1)= -M(2,1)
	M(3,2)= -M(1,2)
	M(3,3)=  0.D0
	M(4,1)= -D(5)
	M(4,2)=  D(6) 
	M(4,3)=  sq2 * ( D(1)-D(2) )
	M(5,1)=  D(4) 
	M(5,2)=  sq2 * ( D(3)-D(1) )
	M(5,3)= -D(6)
	M(6,1)= -( M(5,2) + M(4,3) )
	M(6,2)= -D(4)
	M(6,3)=  D(5)
	return
	end
c
c       ************************************************
	subroutine Krots3( rot, Dmat, D, i6 )
c       !! Rotation of a symmetric matrix
	implicit none
	real*8 tmp, rot(3,3), Dmat(6), D(6)
	integer*4 i, j, k, l, i6(3,3)
	do 1 i= 1,6
	  D(i)= 0.D0
  1     continue
	do 2 i= 1,3
	  do 2 k= 1,3
c	   (Only the upper-right part of the tensor is computed)
	    tmp= rot(i,k) * Dmat(i6(k,k))
	    do 2 j= i,3
	      D(i6(i,j))= D(i6(i,j)) + rot(j,k) * tmp
	      do 2 l= k+1,3
	        D(i6(i,j))= D(i6(i,j))
     &				+ ( rot(i,k) * rot(j,l) + rot(i,l) * rot(j,k) )
     &				* Dmat(i6(k,l))
c	       (this is because Dmat(kl)= Dmat(lk))
  2     continue
	return
	end
