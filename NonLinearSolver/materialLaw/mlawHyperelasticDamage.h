//
// C++ Interface: material law
//
// Author:  <Van Dung Nguyen>, (C) 2017
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef MLAWHYPERELASTICDAMAGE_H_
#define MLAWHYPERELASTICDAMAGE_H_

#include "mlaw.h"
#include "STensor3.h"
#include "STensor43.h"
#include "ipNonLocalDamageIsotropicElasticity.h"
#include "CLengthLaw.h"
#include "DamageLaw.h"
#include "elasticPotential.h"

class mlawLocalDamageHyperelastic : public materialLaw {
	protected:
		double _rho;
		elasticPotential* _elasticLaw;;
		DamageLaw *_damLaw;

	public:
	  mlawLocalDamageHyperelastic(const int num, const double rho, const elasticPotential& elasticLaw, const DamageLaw &damLaw);
		#ifndef SWIG
		mlawLocalDamageHyperelastic(const mlawLocalDamageHyperelastic& src);
		virtual ~mlawLocalDamageHyperelastic();
		virtual materialLaw* clone() const {return new mlawLocalDamageHyperelastic(*this);};
    virtual void checkInternalState(IPVariable* ipv, const IPVariable* ipvprev) const{}; // do nothing
    // General functions
    virtual matname getType() const {return materialLaw::nonLocalDamageIsotropicElasticity;}
    virtual bool withEnergyDissipation() const {return true;};
    virtual void createIPState(IPStateBase* &ips, bool hasBodyForce, const bool* state_ = NULL,const MElement *ele = NULL, const int nbFF_ = 0, const IntPt *GP=NULL, const int gpt=0) const{
		};
    virtual void createIPState(IPLocalDamageIsotropicElasticity *ivi, IPLocalDamageIsotropicElasticity *iv1, IPLocalDamageIsotropicElasticity *iv2) const{
		};
    virtual void createIPVariable(IPLocalDamageIsotropicElasticity *&ipv) const{
		};
    virtual void initLaws(const std::map<int,materialLaw*> &maplaw) {}; // this law is initialized so nothing to do
    virtual double density() const {return _rho;};
    virtual double soundSpeed() const;
		virtual double scaleFactor() const;
    virtual const DamageLaw *getDamageLaw() const{return _damLaw;};

		virtual void constitutive(
        const STensor3& F0,                             // initial deformation gradient (input @ time n)
        const STensor3& F1,                             // updated deformation gradient (input @ time n+1)
        STensor3 &P,                                    // updated 1st Piola-Kirchhoff stress tensor (output)
        const IPVariable *q0,  // array of initial internal variable (in ipvprev on input)
        IPVariable *q1,        // updated array of internal variable (in ipvcur on output),
        STensor43 &L,                             // constitutive tangents (output) // partial stress / partial strains
        const bool stiff,                                // if true: compute the tangents analytically
        STensor43* elasticTangent = NULL, 
        const bool dTangent =false,
        STensor63* dCalgdeps = NULL
    ) const;

		#endif //SWIG
};

class mlawLocalAnisotropicDamageHyperelastic : public mlawLocalDamageHyperelastic{
  public:
	  mlawLocalAnisotropicDamageHyperelastic(const int num, const double rho, const elasticPotential& elasticLaw, const DamageLaw &damLaw):
              mlawLocalDamageHyperelastic(num,rho,elasticLaw,damLaw){};

    #ifndef SWIG
    mlawLocalAnisotropicDamageHyperelastic(const mlawLocalAnisotropicDamageHyperelastic& src): mlawLocalDamageHyperelastic(src){}
    virtual ~mlawLocalAnisotropicDamageHyperelastic(){};

    virtual materialLaw* clone() const {return new mlawLocalAnisotropicDamageHyperelastic(*this);};

    virtual void constitutive(
        const STensor3& F0,                             // initial deformation gradient (input @ time n)
        const STensor3& F1,                             // updated deformation gradient (input @ time n+1)
        STensor3 &P,                                    // updated 1st Piola-Kirchhoff stress tensor (output)
        const IPVariable *q0,  // array of initial internal variable (in ipvprev on input)
        IPVariable *q1,        // updated array of internal variable (in ipvcur on output),
        STensor43 &L,                             // constitutive tangents (output) // partial stress / partial strains
        const bool stiff,                                // if true: compute the tangents analytically
        STensor43* elasticTangent = NULL, 
        const bool dTangent =false,
        STensor63* dCalgdeps = NULL
         ) const;

    #endif //SWIG
};


class mlawNonlocalDamageHyperelastic : public materialLaw {
	protected:
		double _rho;
		elasticPotential* _elasticLaw;;
		CLengthLaw *_cLLaw;
		DamageLaw *_damLaw;

	public:
	  mlawNonlocalDamageHyperelastic(const int num, const double rho, const elasticPotential& elasticLaw, const CLengthLaw &cLLaw,const DamageLaw &damLaw);
		#ifndef SWIG
		mlawNonlocalDamageHyperelastic(const mlawNonlocalDamageHyperelastic& src);
		virtual ~mlawNonlocalDamageHyperelastic();
		virtual materialLaw* clone() const {return new mlawNonlocalDamageHyperelastic(*this);};
		virtual void checkInternalState(IPVariable* ipv, const IPVariable* ipvprev) const{}; // do nothing

    // General functions
    virtual matname getType() const {return materialLaw::nonLocalDamageIsotropicElasticity;}
    virtual bool withEnergyDissipation() const {return true;};
    virtual void createIPState(IPStateBase* &ips,  bool hasBodyForce, const bool* state_ = NULL,const MElement *ele = NULL, const int nbFF_ = 0, const IntPt *GP=NULL, const int gpt=0) const{
		};
    virtual void createIPState(IPNonLocalDamageIsotropicElasticity *ivi, IPNonLocalDamageIsotropicElasticity *iv1, IPNonLocalDamageIsotropicElasticity *iv2) const{
		};
    virtual void createIPVariable(IPNonLocalDamageIsotropicElasticity *&ipv) const{
		};
    virtual void initLaws(const std::map<int,materialLaw*> &maplaw) {}; // this law is initialized so nothing to do
    virtual double density() const {return _rho;};
    virtual double soundSpeed() const;
		virtual double scaleFactor() const;
		virtual const CLengthLaw *getCLengthLaw() const {return _cLLaw;};
    virtual const DamageLaw *getDamageLaw() const{return _damLaw;};

    virtual void constitutive(
        const STensor3& F0,                             // initial deformation gradient (input @ time n)
        const STensor3& F1,                             // updated deformation gradient (input @ time n+1)
        STensor3 &P,                                    // updated 1st Piola-Kirchhoff stress tensor (output)
        const IPVariable *q0,  // array of initial internal variable (in ipvprev on input)
        IPVariable *q1,        // updated array of internal variable (in ipvcur on output),
        STensor43 &L,                             // constitutive tangents (output) // partial stress / partial strains
        const bool stiff,                                // if true: compute the tangents analytically
        STensor43* elasticTangent = NULL, 
        const bool dTangent =false,
        STensor63* dCalgdeps = NULL
         ) const
        {
          Msg::Error("mlawNonlocalDamageHyperelastic constitutive not defined");
        }


		virtual void constitutive(
        const STensor3& F0,                             // initial deformation gradient (input @ time n)
        const STensor3& F1,                             // updated deformation gradient (input @ time n+1)
        STensor3 &P,                                    // updated 1st Piola-Kirchhoff stress tensor (output)
        const IPVariable *q0,  // array of initial internal variable (in ipvprev on input)
        IPVariable *q1,        // updated array of internal variable (in ipvcur on output),
        STensor43 &L,                             // constitutive tangents (output) // partial stress / partial strains
        STensor3 &dLocalEffectiveStrainsDStrains,       //                                // partial Local Effective Strains / partial Strains
        STensor3 &dStressDNonLocalEffectiveStrains,     //                                // partial stress / partial Non-Local Effective strains
        double &dLocalEffectiveStrainsDNonLocalStrains, //                                // partial Local Effective Strains / partial Non Local effective strains
        const bool stiff,                                // if true: compute the tangents analytically
        STensor43* elasticTangent = NULL
      ) const;

		#endif //SWIG
};

class mlawNonlocalAnisotropicDamageHyperelastic : public mlawNonlocalDamageHyperelastic{
  public:
	  mlawNonlocalAnisotropicDamageHyperelastic(const int num, const double rho, const elasticPotential& elasticLaw, const CLengthLaw &cLLaw,const DamageLaw &damLaw):
              mlawNonlocalDamageHyperelastic(num,rho,elasticLaw,cLLaw,damLaw){};

    #ifndef SWIG
    mlawNonlocalAnisotropicDamageHyperelastic(const mlawNonlocalAnisotropicDamageHyperelastic& src): mlawNonlocalDamageHyperelastic(src){}
    virtual ~mlawNonlocalAnisotropicDamageHyperelastic(){};

    virtual materialLaw* clone() const {return new mlawNonlocalAnisotropicDamageHyperelastic(*this);};

    virtual void constitutive(
        const STensor3& F0,                             // initial deformation gradient (input @ time n)
        const STensor3& F1,                             // updated deformation gradient (input @ time n+1)
        STensor3 &P,                                    // updated 1st Piola-Kirchhoff stress tensor (output)
        const IPVariable *q0,  // array of initial internal variable (in ipvprev on input)
        IPVariable *q1,        // updated array of internal variable (in ipvcur on output),
        STensor43 &L,                             // constitutive tangents (output) // partial stress / partial strains
        STensor3 &dLocalEffectiveStrainsDStrains,       //                                // partial Local Effective Strains / partial Strains
        STensor3 &dStressDNonLocalEffectiveStrains,     //                                // partial stress / partial Non-Local Effective strains
        double &dLocalEffectiveStrainsDNonLocalStrains, //                                // partial Local Effective Strains / partial Non Local effective strains
        const bool stiff,                                // if true: compute the tangents analytically
        STensor43* elasticTangent = NULL
        ) const;

    #endif //SWIG
};

#endif // MLAWHYPERELASTICDAMAGE_H_
