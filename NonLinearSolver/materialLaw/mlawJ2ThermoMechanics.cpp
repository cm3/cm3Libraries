//
// C++ Interface: material law
//
// Description: j2ThermoMechanics law
//
//
// Author:  <L. Noels>, (C) 2013
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "mlawJ2ThermoMechanics.h"

#include <math.h>
#include "MInterfaceElement.h"

mlawJ2ThermoMechanics::mlawJ2ThermoMechanics(const int num,const double E,const double nu, const double rho,const double sy0,const double h,
              const double t0,const double Kther, const double alphather, const double cp,const double tol,
              const bool matrixbyPerturbation, const double pert):mlawJ2linear(num,E,nu,rho,sy0,h,tol, matrixbyPerturbation, pert),
                             _t0(t0),_Kther(Kther),_alphather(alphather),_cp(cp),_TaylorQuineyFactor(0.)

              {
   STensorOperation::zero(_k);
	// to be unifom in DG3D q= k' gradT with k'=-k instead of q=-kgradT
	_k(0,0)  = -_Kther;
	_k(1,1)  = -_Kther;
	_k(2,2)  = -_Kther;

        STensorOperation::zero(_alphaDilatation);
	_alphaDilatation(0,0)= _alphather;
	_alphaDilatation(1,1)= _alphather;
	_alphaDilatation(2,2)= _alphather;
	
	double mu = 0.5*E/(1.+nu);
	double lambda = (E*nu)/(1.+nu)/(1.-2.*nu);
	double twicemu = mu+mu;
	STensorOperation::zero(K_);
	K_(0,0,0,0) = lambda + twicemu;
	K_(1,1,0,0) = lambda;
	K_(2,2,0,0) = lambda;
	K_(0,0,1,1) = lambda;
	K_(1,1,1,1) = lambda + twicemu;
	K_(2,2,1,1) = lambda;
	K_(0,0,2,2) = lambda;
	K_(1,1,2,2) = lambda;
	K_(2,2,2,2) = lambda + twicemu;

	if(lambda>=1000.*mu)
	{
	   mu = lambda + mu;
	}

	K_(1,0,1,0) = mu;
	K_(2,0,2,0) = mu;
	K_(0,1,0,1) = mu;
	K_(2,1,2,1) = mu;
	K_(0,2,0,2) = mu;
	K_(1,2,1,2) = mu;

	K_(0,1,1,0) = mu;
	K_(0,2,2,0) = mu;
	K_(1,0,0,1) = mu;
	K_(1,2,2,1) = mu;
	K_(2,0,0,2) = mu;
	K_(2,1,1,2) = mu;

	
	for(int i=0;i<3;i++)
	  {
	    for(int j=0;j<3;j++)
	    {
	      _Stiff_alphaDilatation(i,j)=0.;
	      for(int k=0;k<3;k++)
	      {
		for(int l=0;l<3;l++)
		{
		_Stiff_alphaDilatation(i,j)+=K_(i,j,k,l)*_alphaDilatation(k,l);
		}
	      }
	    }
	  }
	
	
};


mlawJ2ThermoMechanics::mlawJ2ThermoMechanics(const int num,const double E,const double nu, const double rho,const J2IsotropicHardening &_j2IH,
                        const double t0,const double Kther, const double alphather, const double cp,
                        const double tol, const bool matrixbyPerturbation, const double pert) :
                             mlawJ2linear(num,E,nu,rho,_j2IH,tol, matrixbyPerturbation, pert),
                             _t0(t0),_Kther(Kther),_alphather(alphather),_cp(cp),_TaylorQuineyFactor(0.)
{
        STensorOperation::zero(_k);
	// to be unifom in DG3D q= k' gradT with k'=-k instead of q=-kgradT
	_k(0,0)  = -_Kther;
	_k(1,1)  = -_Kther;
	_k(2,2)  = -_Kther;

        STensorOperation::zero(_alphaDilatation);
	_alphaDilatation(0,0)= _alphather;
	_alphaDilatation(1,1)= _alphather;
	_alphaDilatation(2,2)= _alphather;
	
	double mu = 0.5*E/(1.+nu);
	double lambda = (E*nu)/(1.+nu)/(1.-2.*nu);
	double twicemu = mu+mu;
	STensorOperation::zero(K_);
	K_(0,0,0,0) = lambda + twicemu;
	K_(1,1,0,0) = lambda;
	K_(2,2,0,0) = lambda;
	K_(0,0,1,1) = lambda;
	K_(1,1,1,1) = lambda + twicemu;
	K_(2,2,1,1) = lambda;
	K_(0,0,2,2) = lambda;
	K_(1,1,2,2) = lambda;
	K_(2,2,2,2) = lambda + twicemu;

	if(lambda>=1000.*mu)
	{
	   mu = lambda + mu;
	}

	K_(1,0,1,0) = mu;
	K_(2,0,2,0) = mu;
	K_(0,1,0,1) = mu;
	K_(2,1,2,1) = mu;
	K_(0,2,0,2) = mu;
	K_(1,2,1,2) = mu;

	K_(0,1,1,0) = mu;
	K_(0,2,2,0) = mu;
	K_(1,0,0,1) = mu;
	K_(1,2,2,1) = mu;
	K_(2,0,0,2) = mu;
	K_(2,1,1,2) = mu;

	
	for(int i=0;i<3;i++)
	  {
	    for(int j=0;j<3;j++)
	    {
	      _Stiff_alphaDilatation(i,j)=0.;
	      for(int k=0;k<3;k++)
	      {
		for(int l=0;l<3;l++)
		{
		_Stiff_alphaDilatation(i,j)+=K_(i,j,k,l)*_alphaDilatation(k,l);
		}
	      }
	    }
	  }
	
}
mlawJ2ThermoMechanics::mlawJ2ThermoMechanics(const mlawJ2ThermoMechanics &source) : mlawJ2linear(source),
                                         _t0(source. _t0),_Kther(source._Kther),_alphather(source._alphather),
                                         _k(source._k), _alphaDilatation(source._alphaDilatation),_cp(source._cp),
                                         _TaylorQuineyFactor(source._TaylorQuineyFactor),K_(source.K_),_Stiff_alphaDilatation(source._Stiff_alphaDilatation)
{

}


mlawJ2ThermoMechanics& mlawJ2ThermoMechanics::operator=(const materialLaw &source)
{
  mlawJ2linear::operator=(source);
  const mlawJ2ThermoMechanics* src =static_cast<const mlawJ2ThermoMechanics*>(&source);
  if(src !=NULL)
  {
    _t0 = src->_t0,
    _Kther = src->_Kther;
    _alphather = src->_alphather;
    _k = src->_k;
    _alphaDilatation = src->_alphaDilatation;
    _cp = src->_cp;
    _TaylorQuineyFactor = src->_TaylorQuineyFactor;
    _Stiff_alphaDilatation=src->_Stiff_alphaDilatation;
    K_=src->K_;
  }
  return *this;
}

void mlawJ2ThermoMechanics::createIPState(IPStateBase* &ips, bool hasBodyForce,const bool* state_,const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
  bool inter=true;
  const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
  if(iele==NULL) inter=false;
  IPVariable* ipvi = new IPJ2ThermoMechanics(_j2IH);
  IPVariable* ipv1 = new IPJ2ThermoMechanics(_j2IH);
  IPVariable* ipv2 = new IPJ2ThermoMechanics(_j2IH);
  if(ips != NULL) delete ips;
  ips = new IP3State(state_,ipvi,ipv1,ipv2);
}


void mlawJ2ThermoMechanics::constitutive(const STensor3& F0,
                                         const STensor3& Fn,
                                         STensor3 &P,
                                         const IPJ2ThermoMechanics *q0, 
                                         IPJ2ThermoMechanics *q1,
                                         STensor43 &Tangent,
                                         const bool stiff, 
                                         STensor43* elasticTangent, 
                                         const bool dTangent,
                                         STensor63* dCalgdeps) const
{

  mlawJ2linear::constitutive(F0,Fn,P, q0, q1, Tangent, stiff);

}

void mlawJ2ThermoMechanics::constitutive(
                            const STensor3& F0,         // initial deformation gradient (input @ time n)
                            const STensor3& Fn,         // updated deformation gradient (input @ time n+1)
                            STensor3 &P,                // updated 1st Piola-Kirchhoff stress tensor (output)
                                                        // contains the initial values on input
                            const IPJ2ThermoMechanics *q0,       // array of initial internal variable
                            IPJ2ThermoMechanics *q1,             // updated array of internal variable (in ipvcur on output),
                            STensor43 &Tangent,         // constitutive tangents (output)
                            double T0,
			    double T,
  			    const SVector3 &gradT,
                            SVector3 &fluxT,
                            STensor3 &dPdT,
                            STensor3 &dqdgradT,
                            SVector3 &dqdT,
                            STensor33 &dqdF,
			    double &w,
			    double &dwdt,
			    STensor3 &dwdf,
                            const bool stiff            // if true compute the tangents
                           ) const
{
  //one solution is to modify F (requries correct tgte here we assume small def)
  //STensor3 Fther(_alphaDilatation);
  //Fther*=(T-_t0)*(-1.);

  //STensor3 Fmod(Fn);
  //Fmod*=Fther;

  //mlawJ2linear::constitutive(F0,Fmod,P, q0, q1, Tangent, stiff);



  mlawJ2linear::constitutive(F0,Fn,P, q0, q1, Tangent, stiff);
  static STensor3 cauchyDil;
  static STensor3 Fninv;
  Fninv  = Fn.invert();
  double Jac= Fn.determinant();

  for(int i=0;i<3;i++)
  {
    for(int j=0;j<3;j++)
    {
      cauchyDil(i,j) =-3.*_K*_alphaDilatation(i,j)*(T-_t0);
    }
  }
  for(int i=0;i<3;i++)
  {
    for(int J=0;J<3;J++)
    {
      for(int m=0;m<3;m++)
      {
        P(i,J) += cauchyDil(i,m)*Fninv(J,m)*Jac;
      }
    }
  }
  //put flux in reference configuration
  STensorOperation::zero(fluxT);
  static STensor3 _Kref;
  STensorOperation::zero(_Kref);
  for(int K = 0; K< 3; K++)
  {
    for(int L = 0; L< 3; L++)
    {
      for(int i = 0; i< 3; i++)
      {
        for(int j = 0; j< 3; j++)
        {
          _Kref(K,L) += Fninv(K,i)*_k(i,j)*Fninv(L,j); //put in reference configuration
        }
      }
      fluxT(K) += _Kref(K,L)*gradT(L)*Jac;
    }
  }
  double dh = getTimeStep();
  double dcpdt = 0.;
  dwdt=0.;
  STensorOperation::zero(dwdf);
  if(dh>0)
    w=-1.*_cp*(T-T0)/dh;
  q1->_thermalEnergy = _cp*T; //(T-_t0);
  if(stiff)
  {

    for(int i=0;i<3;i++)
    {
      for(int J=0;J<3;J++)
      {
        for(int k=0;k<3;k++)
        {
          for(int L=0;L<3;L++)
          {
            for(int m=0;m<3;m++)
            {
              Tangent(i,J,k,L) += cauchyDil(i,m)*Fninv(J,m)*Fninv(L,k)*Jac-cauchyDil(i,m)*Fninv(J,k)*Fninv(L,m)*Jac;
            }
          }
        }
      }
    }

    STensorOperation::zero(dPdT);
    for(int i=0;i<3;i++)
    {
      for(int J=0;J<3;J++)
      {
        for(int m=0;m<3;m++)
        {
          dPdT(i,J)-=3.*_K*_alphaDilatation(i,m)*Fninv(J,m)*Jac;
        }
      }
    }

    STensorOperation::zero(dqdT); //if _k depends on T this should be here

    dqdgradT = _Kref*Jac;

    STensorOperation::zero(dqdF);
    for(int K = 0; K< 3; K++)
    {
      for(int m = 0; m< 3; m++)
      {
        for(int N = 0; N< 3; N++)
        {
          for(int L = 0; L< 3; L++)
          {
            dqdF(K,m,N) -= Fninv(K,m)*_Kref(N,L)*gradT(L)*Jac;
            dqdF(K,m,N) -= _Kref(K,N)*Fninv(L,m)*gradT(L)*Jac;
            dqdF(K,m,N) += _Kref(K,L)*gradT(L)*Fninv(N,m)*Jac;
           }
         }
       }
     }
     if(dh>0)
     {
       dwdt=-1.*dcpdt-_cp/dh;
       STensorOperation::zero(dwdf);
     }
  }
}

void mlawJ2ThermoMechanics::constitutive(
              const STensor3& F0,         // initial deformation gradient (input @ time n)
              const STensor3& Fn,         // updated deformation gradient (input @ time n+1)
              STensor3 &P,                // updated 1st Piola-Kirchhoff stress tensor (output)
                                          // contains the initial values on input
              const IPJ2ThermoMechanics *q0,       // array of initial internal variable
              IPJ2ThermoMechanics *q1,             // updated array of internal variable (in ipvcur on output),
              STensor43 &Tangent,         // constitutive tangents (output)
              double T0,
              double T,
              const SVector3 &gradT,
              SVector3 &fluxT,
              STensor3 &dPdT,
              STensor3 &dqdgradT,
              SVector3 &dqdT,
              STensor33 &dqdF,
              double &w,   //cp*dTdh
              double &dwdt,
              STensor3 &dwdf,
              double& mecaSour,
              double& dmecaSourdT,
              STensor3& dmecaSourdF,
              const bool stiff            // if true compute the tangents
             ) const{
   mlawJ2ThermoMechanics::constitutive(F0,Fn,P,q0,q1,Tangent,T0,T,gradT,fluxT,dPdT,dqdgradT,dqdT,dqdF,w,dwdt,dwdf,stiff);
   mecaSour = _TaylorQuineyFactor*q1->getConstRefToPlasticPower();
   dmecaSourdT = 0.;
   if (stiff){
    dmecaSourdF = q1->getConstRefToDPlasticPowerDF();
    dmecaSourdF*=_TaylorQuineyFactor;
   }
};


