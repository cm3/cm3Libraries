//
// C++ Interface: material law
//
// Description: LinearElecMagThermoMechanics law
//
//
// Author:  <Vinayak GHOLAP>, (C) 2019
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "mlawLinearElecMagTherMech.h"
#include <math.h>
#include "MInterfaceElement.h"
#include "mlawLinearElecTherMech.h"

mlawLinearElecMagTherMech::mlawLinearElecMagTherMech(const int num,const double rho,
const double Ex, const double Ey, const double Ez, const double Vxy, const double Vxz, const double Vyz,
const double MUxy, const double MUxz, const double MUyz, const double alpha, const double beta, const double gamma,
const double t0,const double Kx,const double Ky,const double Kz,const double alphax,const double alphay,
const double alphaz,const  double lx,const  double ly,const  double lz,const double seebeck,const double cp,const double v0,
const double mu_x, const double mu_y, const double mu_z, const double A0_x, const double A0_y, const double A0_z, 
const double Irms, const double freq, const unsigned int nTurnsCoil, 
const double coilLength_x, const double coilLength_y, const double coilLength_z, const double coilWidth, const bool useFluxT,
const bool evaluateCurlField)
: mlawLinearElecTherMech(num, rho, Ex, Ey, Ez, Vxy, Vxz, Vyz, MUxy, MUxz, MUyz, alpha, beta, gamma, t0, Kx, Ky, Kz,
                         alphax, alphay, alphaz, lx, ly, lz, seebeck, cp, v0),
                        _mu_x(mu_x), _mu_y(mu_y), _mu_z(mu_z), _A0_x(A0_x), _A0_y(A0_y), _A0_z(A0_z), _freqEM(freq), _useFluxT(useFluxT),
                        _evaluateCurlField(evaluateCurlField)
{
    STensor3 R;		//3x3 rotation matrix
   	double fpi = M_PI/180.;

	const double c1 = std::cos(_alpha*fpi);
	const double s1 = std::sin(_alpha*fpi);

	const double c2 = std::cos(_beta*fpi);
	const double s2 = std::sin(_beta*fpi);

	const double c3 = std::cos(_gamma*fpi);
	const double s3 = std::sin(_gamma*fpi);

	const double s1c2 = s1*c2;
	const double c1c2 = c1*c2;

	R(0,0) = c3*c1 - s1c2*s3;
	R(0,1) = c3*s1 + c1c2*s3;
	R(0,2) = s2*s3;

	R(1,0) = -s3*c1 - s1c2*c3;
	R(1,1) = -s3*s1 + c1c2*c3;
	R(1,2) = s2*c3;

	R(2,0) = s1*s2;
	R(2,1) = -c1*s2;
	R(2,2) = c2;

	STensor3 mu;
	// to be uniform in DG3D j= l' dAdt + l' gradV with l'=-l
	// h = mu^-1 b
	mu(0,0)  = _mu_x;
	mu(1,1)  = _mu_y;
	mu(2,2)  = _mu_z;

  	for(unsigned int i=0; i<3; ++i)
  	{
        for(unsigned int j=0; j<3; ++j)
    	   {
             _mu0(i,j) = 0.;

             for(unsigned int m=0; m<3; ++m)
               for(unsigned int n=0; n<3; ++n)
              	  _mu0(i,j) += R(m,i)*R(n,j)*mu(m,n);
            }
  	}

    _nu0 = _mu0.invert();
    
    this->_js0 = 0.0; // Non-inductor domain: no imposed current density
}

mlawLinearElecMagTherMech::mlawLinearElecMagTherMech(const mlawLinearElecMagTherMech &source)
: mlawLinearElecTherMech(source),
  _mu_x(source._mu_x), _mu_y(source._mu_y), _mu_z(source._mu_z), _A0_x(source._A0_x),
  _A0_y(source._A0_y), _A0_z(source._A0_z), _freqEM(source._freqEM), _useFluxT(source._useFluxT),
  _evaluateCurlField(source._evaluateCurlField)
  {
    _mu0 = source._mu0;
    _nu0 = source._nu0;
    _js0 = source._js0;
  }

mlawLinearElecMagTherMech& mlawLinearElecMagTherMech::operator=(const materialLaw &source)
{
  mlawLinearElecTherMech::operator=(source);
  const mlawLinearElecMagTherMech* src =static_cast<const mlawLinearElecMagTherMech*>(&source);
  if(src !=NULL)
  {
    _mu_x = src->_mu_x;
    _mu_y = src->_mu_y;
    _mu_z = src->_mu_z;
    _A0_x = src->_A0_x;
    _A0_y = src->_A0_y;
    _A0_z = src->_A0_z;
    _mu0 = src->_mu0;
    _nu0 = src->_nu0;
    _js0 = src->_js0;
    _freqEM = src->_freqEM;
    _useFluxT = src->_useFluxT;
    _evaluateCurlField = src->_evaluateCurlField;
  }
  return *this;
}

void mlawLinearElecMagTherMech::createIPState(IPStateBase* &ips, bool hasBodyForce, const bool* state_,const MElement *ele,
const int nbFF_, const IntPt *GP, const int gpt) const
{
  bool inter=true;
  const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
  if(iele==NULL) inter=false;
  IPVariable* ipvi = new IPLinearElecMagTherMech();
  IPVariable* ipv1 = new IPLinearElecMagTherMech();
  IPVariable* ipv2 = new IPLinearElecMagTherMech();
  if(ips != NULL) delete ips;
  ips = new IP3State(state_,ipvi,ipv1,ipv2);
}


void mlawLinearElecMagTherMech::constitutive(
                                            const STensor3& F0,         // initial deformation gradient (input @ time n)
                                            const STensor3& Fn,         // updated deformation gradient (input @ time n+1)
                                            STensor3 &P,                // updated 1st Piola-Kirchhoff stress tensor (output)
                                                                        // contains the initial values on input
                                            const IPLinearElecMagTherMech *q0,       // array of initial internal variable
                                            IPLinearElecMagTherMech *q1,             // updated array of internal variable (in ipvcur on output),
                                            STensor43 &Tangent,         // constitutive tangents (output)
                                            double T0,
                                            double T,
                                            double V0,
                                            double V,
                                            const SVector3 &gradT,
                                            const SVector3 &gradV,
                                            SVector3 &fluxT,
                                            SVector3 &fluxje,
                                            STensor3 &dPdT,
                                            STensor3 &dPdV,
                                            STensor3 &dqdgradT,
                                            SVector3 &dqdT,
                                            STensor3 &dqdgradV,
                                            SVector3 &dqdV,
                                            SVector3 &dedV,
                                            STensor3 &dedgradV,
                                            SVector3 &dedT,
                                            STensor3 &dedgradT,
                                            STensor33 &dqdF,
                                            STensor33 &dedF,
                                            double &w_T,
                                            double &dw_Tdt,
                                            double &dw_TdV,
                                            STensor3 &dw_Tdf,
                                            double &wV,  //related to dV/dt
                                            double &dwVdt,
                                            double &dwVdV,
                                            STensor3 &dwVdF,
                                            STensor3 &l10,
                                            STensor3 &l20,
                                            STensor3 &k10,
                                            STensor3 &k20,
                                            STensor3 &dl10dT,
                                            STensor3 &dl20dT,
                                            STensor3 &dk10dT,
                                            STensor3 &dk20dT,
                                            STensor3 &dl10dv,
                                            STensor3 &dl20dv,
                                            STensor3 &dk10dv,
                                            STensor3 &dk20dv,
                                            STensor43 &dl10dF,
                                            STensor43 &dl20dF,
                                            STensor43 &dk10dF,
                                            STensor43 &dk20dF,
                                            SVector3 &fluxjy,
                                            SVector3 &djydV,
                                            STensor3 &djydgradV,
                                            SVector3 &djydT,
                                            STensor3 &djydgradT,
                                            STensor33 &djydF,
                                            STensor3 & djydA,
                                            STensor3 & djydB,
                                            STensor3 &djydgradVdT,
                                            STensor3 &djydgradVdV,
                                            STensor43 &djydgradVdF,
                                            STensor3 &djydgradTdT,
                                            STensor3 &djydgradTdV,
                                            STensor43 &djydgradTdF,
                                            const bool stiff,
                                            const SVector3 & A0, // magnetic potential at time n
                                            const SVector3 & An, // magnetic potential at time n+1
                                            const SVector3 & B, // magneticinduction B = Curl A
                                            SVector3 & H, // magnetic field H
                                            SVector3 & js0, // current source vector field in inductor
                                            SVector3 & dBdT,
                                            STensor3 & dBdGradT,
                                            SVector3 & dBdV,
                                            STensor3 & dBdGradV,
                                            STensor33 & dBdF,
                                            STensor3 & dBdA, // A magnetic vector potential
                                            STensor3 & dfluxTdA,
                                            STensor3 & dfluxjedA,
                                            STensor3 & dfluxTdB,
                                            STensor3 & dfluxjedB,
                                            SVector3 &dw_TdA,
                                            SVector3 & dw_TdB,
                                            SVector3 & dmechSourcedB,
                                            SVector3 & sourceVectorField, // Magnetic source vector field (T, grad V, F)
                                            SVector3 & dsourceVectorFielddt,
                                            double & ThermalEMFieldSource,
                                            double & VoltageEMFieldSource,
                                            STensor3 & dHdA,
                                            STensor3 & dHdB,
                                            STensor33 & dPdB,
                                            STensor33 & dHdF,
                                            STensor33 & dsourceVectorFielddF,
                                            SVector3 & mechFieldSource, // Electromagnetic force
                                            STensor3 & dmechFieldSourcedB,
                                            STensor33 & dmechFieldSourcedF,
                                            SVector3 & dHdT,
                                            STensor3 & dHdgradT,
                                            SVector3 & dHdV,
                                            STensor3 & dHdgradV,
                                            SVector3 & dsourceVectorFielddT,
                                            STensor3 & dsourceVectorFielddgradT,
                                            SVector3 & dsourceVectorFielddV,
                                            STensor3 & dsourceVectorFielddgradV,
                                            STensor3 & dsourceVectorFielddA,
                                            STensor3 & dsourceVectorFielddB,
                                            SVector3 & dmechFieldSourcedT,
                                            STensor3 & dmechFieldSourcedgradT,
                                            SVector3 & dmechFieldSourcedV,
                                            STensor3 & dmechFieldSourcedgradV,
                                            SVector3 & dThermalEMFieldSourcedA,
                                            SVector3 & dThermalEMFieldSourcedB,
                                            STensor3 & dThermalEMFieldSourcedF,
                                            double & dThermalEMFieldSourcedT,
                                            SVector3 & dThermalEMFieldSourcedGradT,
                                            double & dThermalEMFieldSourcedV,
                                            SVector3 & dThermalEMFieldSourcedGradV,
                                            SVector3 & dVoltageEMFieldSourcedA,
                                            SVector3 & dVoltageEMFieldSourcedB,
                                            STensor3 & dVoltageEMFieldSourcedF,
                                            double & dVoltageEMFieldSourcedT,
                                            SVector3 & dVoltageEMFieldSourcedGradT,
                                            double & dVoltageEMFieldSourcedV,
                                            SVector3 & dVoltageEMFieldSourcedGradV

                           )const
{
    mlawLinearElecTherMech::constitutive(F0,Fn, P, (const IPLinearElecTherMech *) q0, (IPLinearElecTherMech *) q1,
                                         Tangent, T0, T, V0, V, gradT, gradV, fluxT, fluxje, dPdT, dPdV, dqdgradT, dqdT,
                                         dqdgradV, dqdV, dedV, dedgradV, dedT,
                                         dedgradT, dqdF, dedF, w_T, dw_Tdt, dw_Tdf, l10, l20, k10, k20, dl10dT, dl20dT,
                                         dk10dT, dk20dT, dl10dv, dl20dv, dk10dv, dk20dv,
                                         dl10dF, dl20dF, dk10dF, dk20dF, fluxjy, djydV, djydgradV, djydT, djydgradT,
                                         djydF, djydgradVdT, djydgradVdV, djydgradVdF,
                                         djydgradTdT, djydgradTdV, djydgradTdF, stiff,this->_evaluateCurlField);

    // here magnetic, electric field and temp dependent

    // if true then we solve EM problem and thus reevaluate Magnetic potential as well
    // else do not consider Magnetic contributions -> TM problem
    if (this->_evaluateCurlField)
    {
    STensorOperation::zero(H);
    STensorOperation::zero(dBdT);
    STensorOperation::zero(dBdGradT);
    STensorOperation::zero(dBdV);
    STensorOperation::zero(dBdGradV);
    STensorOperation::zero(dBdF);
    STensorOperation::zero(dBdA);
    STensorOperation::zero(dfluxTdA);
    STensorOperation::zero(dfluxjedA);
    STensorOperation::zero(dfluxTdB);
    STensorOperation::zero(dfluxjedB);
    STensorOperation::zero(dw_TdA);
    STensorOperation::zero(dw_TdB);
    STensorOperation::zero(dHdA);
    STensorOperation::zero(dHdB);
    STensorOperation::zero(dPdB);
    STensorOperation::zero(dHdF);
    STensorOperation::zero(dHdT);
    STensorOperation::zero(dHdgradT);
    STensorOperation::zero(dHdV);
    STensorOperation::zero(dHdgradV);
    STensorOperation::zero(sourceVectorField);
    STensorOperation::zero(dsourceVectorFielddT);
    STensorOperation::zero(dsourceVectorFielddgradT);
    STensorOperation::zero(dsourceVectorFielddV);
    STensorOperation::zero(dsourceVectorFielddgradV);
    STensorOperation::zero(dsourceVectorFielddA);
    STensorOperation::zero(dsourceVectorFielddB);
    STensorOperation::zero(dsourceVectorFielddt);
    STensorOperation::zero(dsourceVectorFielddF);
    STensorOperation::zero(djydA);
    STensorOperation::zero(djydB);
    ThermalEMFieldSource = 0.0;
    VoltageEMFieldSource = 0.0;
    STensorOperation::zero(dThermalEMFieldSourcedA);
    STensorOperation::zero(dThermalEMFieldSourcedB);
    STensorOperation::zero(dThermalEMFieldSourcedF);
    STensorOperation::zero(dThermalEMFieldSourcedT);
    STensorOperation::zero(dThermalEMFieldSourcedGradT);
    STensorOperation::zero(dThermalEMFieldSourcedV);
    STensorOperation::zero(dThermalEMFieldSourcedGradV);
    STensorOperation::zero(dVoltageEMFieldSourcedA);
    STensorOperation::zero(dVoltageEMFieldSourcedB);
    STensorOperation::zero(dVoltageEMFieldSourcedF);
    STensorOperation::zero(dVoltageEMFieldSourcedT);
    STensorOperation::zero(dVoltageEMFieldSourcedGradT);
    STensorOperation::zero(dVoltageEMFieldSourcedV);
    STensorOperation::zero(dVoltageEMFieldSourcedGradV);

    wV=0.;  //related to dV/dt
    dwVdt=0.;
    dwVdV=0.;
    STensorOperation::zero(dwVdF);



    static STensor3 dldT,dkdT,dnudT;
    double dseebeckdT = 0.0;
    STensor33 d_nu0dB(0.0);

    STensorOperation::zero(dldT);
    STensorOperation::zero(dkdT);
    STensorOperation::zero(dnudT);

    // Linear magnetic constitutive law H = (1/mu) * B
    for (unsigned int i = 0; i < 3; ++i)
    {
        H(i) = 0.0;
        for (unsigned int j = 0; j < 3; ++j)
        {
            H(i) += _nu0(i,j) * B(j);
        }
    }

    // Add contribution from j_p and j_m too???
    const double ctime = getTime();
    const double dh = getTimeStep();
    double invdh;
    if (dh < 0.0)
        Msg::Error("Negative time step size in mlawEMTM::constitutive()");
    else if (dh == 0.0)
        invdh = 0.0;
    else
        invdh = 1.0/dh;
    // phase angle for sin wave function
    // phi = M_PI / 2.0 if static analysis in EM
    // phi = 0.0 if time domain dynamic analysis
    const double phi = 0.0;
    const double js0_sin_wt_phi = this->_js0 * std::sin(2.0 * M_PI * _freqEM * ctime + phi);

    static SVector3 normalDir(0.0);
    this->getNormalDirection(normalDir, q1);

    for(unsigned int i = 0; i < 3; ++i)
    {
        js0(i) = js0_sin_wt_phi * normalDir(i);
    }

    for(unsigned int i = 0; i < 3; ++i)
    {
        if (!isInductor())
        {
            for (unsigned int j = 0; j < 3; ++j)
            {
                // j_e, q, j_y
                fluxje(i) += (_l0(i, j) * (An(j) - A0(j)) * invdh);
                fluxT(i) += ((_l0(i, j) * _seebeck * T) * (An(j) - A0(j)) * invdh);
                fluxjy(i) += ((_l0(i, j) * V + _l0(i, j) * _seebeck * T) * (An(j) - A0(j)) * invdh);

                if (stiff)
                {
                    // derivatives of j_e
                    dedT(i) += dldT(i, j) * (An(j) - A0(j)) * invdh;
                    dedV(i) += 0.0;
                    dfluxjedA(i, j) += _l0(i, j) * invdh;

                    // derivatives of q or fluxT
                    dqdT(i) += dldT(i, j) * _seebeck * T * (An(j) - A0(j)) * invdh +
                               dseebeckdT * _l0(i, j) * T * (An(j) - A0(j)) * invdh +
                               _l0(i, j) * _seebeck * (An(j) - A0(j)) * invdh;
                    dqdV(i) += 0.0;
                    dfluxTdA(i, j) += _l0(i, j) * _seebeck * T * invdh;

                    // derivatives of fluxjy
                    djydT(i) += (_seebeck * _l0(i, j) + dseebeckdT * T * _l0(i, j) + _seebeck * T * dldT(i,j) + V * dldT(i,j)) * (An(j) - A0(j)) * invdh;
                    djydV(i) += _l0(i, j) * (An(j) - A0(j)) * invdh;
                    djydA(i, j) += (_l0(i, j) * V + _l0(i, j) * _seebeck * T) * invdh;


                    // derivatives of sourceVectorField
                    dsourceVectorFielddB(i, j) += 0.0;
                    dsourceVectorFielddgradV(i, j) += (-_l0(i, j));
                    dsourceVectorFielddgradT(i, j) += (-_seebeck * _l0(i ,j));

                    dsourceVectorFielddV(i) += 0.0;
                    dsourceVectorFielddT(i) += (-dseebeckdT * _l0(i, j) * gradT(j) - _seebeck * dldT(i,j) * gradT(j)
                                                - dldT(i,j) * (An(j) - A0(j)) * invdh - dldT(i,j) * gradV(j));
                    dsourceVectorFielddA(i, j) += (-_l0(i, j) * invdh);
                }
            }
            // -j_e
            sourceVectorField(i) = -fluxje(i);
            // w_AV = j_e \cdot -dadt + h \cdot dbdt
            ThermalEMFieldSource += (sourceVectorField(i) * ((An(i) - A0(i)) * invdh) /*+ H(i) * (Bn(i) - B0(i)) * invdh*/);

            // to debug weak vs strong coupling results by
            // using analytical EM thermal source in SMP only with
            // A²/2 being mean value that is calculated
            // in weak coupling for considered parameter values

            /*
              if(this->_num == 1) // if SMP
                // A² cos²(2 pi f t)
                // A²/2 = Mean value over one period = 3559017.23076498
                ThermalEMFieldSource = 2.0 * 2227580. * std::cos(2.0 * M_PI * _freqEM * ctime) *
                                        std::cos(2.0 * M_PI * _freqEM * ctime);
            else
                ThermalEMFieldSource = 0.0;
            */

            // w_AV += je \cdot -gradV
            if (_useFluxT)
                ThermalEMFieldSource += (sourceVectorField(i) * gradV(i));
        }

        // Need to check contribution to jy from source js0
        if (isInductor())
        {
            fluxje(i) += 0.0;
            fluxT(i) += 0.0;
            fluxjy(i) += (_seebeck * T + V) * js0(i);

            if (stiff)
            {
                dedT(i) += 0.0;
                dedV(i) += 0.0;

                dqdT(i) += 0.0;
                dqdV(i) += 0.0;

                djydT(i) += (_seebeck + dseebeckdT * T) * js0(i);

                // derivatives of sourceVectorField
                dsourceVectorFielddV(i) += 0.0;
                dsourceVectorFielddT(i) += 0.0;

                for (unsigned int j = 0; j < 3; ++j)
                {
                    dfluxjedA(i,j) += 0.0;
                    dfluxTdA(i,j) += 0.0;
                    djydA(i,j) += 0.0;

                    dsourceVectorFielddA(i, j) += 0.0;
                    dsourceVectorFielddB(i, j) += 0.0;
                    dsourceVectorFielddgradV(i, j) += 0.0;
                    dsourceVectorFielddgradT(i, j) += 0.0;
                }
                djydV(i) += js0(i);
            }
            // -j_e
            sourceVectorField(i) = - js0(i);
            // w_AV = j_e \cdot -dadt + h \cdot dbdt
            ThermalEMFieldSource += 0.0;//(sourceVectorField(i) * ((An(i) - A0(i)) * invdh) /*+ H(i) * (Bn(i) - B0(i)) * invdh*/);

            // w_AV += je \cdot -gradV
            if (_useFluxT)
                ThermalEMFieldSource += 0.0;//(sourceVectorField(i) * gradV(i));
        }

        // For test with unit cube to check
        // interpolation of curlVals
        //js0(0) = 1.0;
        //js0(1) = js0(2) = 0.0;
        //sourceVectorField = -js0;
    }

    if (stiff)
    {
    for (unsigned int m = 0; m < 3; ++m)
    {
        dHdV(m) += 0.0;
        for (unsigned int n = 0; n < 3; ++n)
        {
            dHdA(m,n) += 0.0;
            dHdT(m) += dnudT(m,n) * B(n);

            for (unsigned int l = 0; l < 3; ++l)
                dHdB(m,n) += (d_nu0dB(m,n,l) * B(l));

            dHdB(m,n) += _nu0(m,n);

            dHdgradT(m,n) += 0.0;
            dHdgradV(m,n) += 0.0;

            dedgradT(m, n) += 0.0;
            dedgradV(m, n) += 0.0;
            dfluxjedB(m, n) += 0.0;

            djydgradT(m, n) += 0.0;
            djydgradV(m, n) += 0.0;
            djydB(m, n) += 0.0;
            djydgradTdT(m, n) += 0.0;
            djydgradTdV(m, n) += 0.0;
            djydgradVdT(m, n) += 0.0;
            djydgradVdV(m, n) += 0.0;

            dqdgradT(m, n) += 0.0;
            dqdgradV(m, n) += 0.0;
            dfluxTdB(m, n) += 0.0;
        }
    }

    dw_TdV += 0.0;
    if (!isInductor())
    {
    for (unsigned  int m = 0; m < 3; ++m)
    {
        dw_TdA(m) += 0.0;
        for (unsigned int n = 0; n < 3; ++n)
        {
            dThermalEMFieldSourcedA(m) += (-dfluxjedA(m,n) * ((An(n) - A0(n)) * invdh));
            dThermalEMFieldSourcedB(m) += /*(dHdB(m,n) * (Bn(n) - B0(n)) * invdh)*/ 0.0;
            dThermalEMFieldSourcedGradT(m) +=  (-dedgradT(m,n) * (An(n) - A0(n)) * invdh);
            dThermalEMFieldSourcedGradV(m) += (-dedgradV(m,n) * (An(n) - A0(n)) * invdh);

            if (_useFluxT)
            {
                dThermalEMFieldSourcedGradV(m) += (- dedgradV(m,n) * gradV(n));
                dThermalEMFieldSourcedA(m) += (-dfluxjedA(m,n) * gradV(n));
            }
        }
        dThermalEMFieldSourcedA(m) += (-fluxje(m) * invdh);
        dThermalEMFieldSourcedB(m) += /*(H(m) * invdh)*/ 0.0;

        if (_useFluxT)
            dThermalEMFieldSourcedGradV(m) += ( - fluxje(m));

        dThermalEMFieldSourcedT += (-dedT(m) * (An(m) - A0(m)) * invdh /*+ dnudT(m,n) * B(n) * (Bn(m) - B0(m)) * invdh*/);
        dThermalEMFieldSourcedV += 0.0;
    }
    }

    }

    // Later think of flux terms and higher order terms d*d*
    /*STensorOperation::zero(dBdT);
    STensorOperation::zero(dBdGradT);
    STensorOperation::zero(dBdV);
    STensorOperation::zero(dBdGradV);
    STensorOperation::zero(dBdF);
    STensorOperation::zero(dBdA);
    STensorOperation::zero(dw_TdB);
    STensorOperation::zero(dmechSourcedB);
    STensorOperation::zero(dsourceVectorFielddt);
    STensorOperation::zero(dPdB);
    STensorOperation::zero(dHdF);
    STensorOperation::zero(dsourceVectorFielddF);*/
    STensorOperation::zero(mechFieldSource);
    /*STensorOperation::zero(dmechFieldSourcedB);
    STensorOperation::zero(dmechFieldSourcedF);
    STensorOperation::zero(dHdT);
    STensorOperation::zero(dHdgradT);
    STensorOperation::zero(dHdV);
    STensorOperation::zero(dHdgradV);
    STensorOperation::zero(dmechFieldSourcedT);
    STensorOperation::zero(dmechFieldSourcedgradT);
    STensorOperation::zero(dmechFieldSourcedV);
    STensorOperation::zero(dmechFieldSourcedgradV);*/

    }
}

  double mlawLinearElecMagTherMech::deformationEnergy(STensor3 defo,const SVector3 &gradV,const SVector3 &gradT,
  const double T, const double V) const
{
  double fz=0.;

	 for(unsigned int i=0; i<3; ++i)
        for(unsigned int j=0; j<3; ++j)
            fz+=defo(i,j)*defo(i,j);

  return std::fabs(fz);
}
