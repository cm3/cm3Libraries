//
// C++ Interface: failure criterions
//
// Author:  <Van Dung Nguyen>, (C) 2015
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef FAILURECRITERION_H_
#define FAILURECRITERION_H_

#ifndef SWIG
#include "STensor3.h"
#include "ipvariable.h"
#endif // SWIG
#include "ipField.h"

class FailureCriterionBase{
  protected:
    int _num;
    bool _init;
  #ifndef SWIG
  public:
    FailureCriterionBase(const int num, const bool init = true): _num(num),_init(init){};
    FailureCriterionBase(const FailureCriterionBase& src): _num(src._num),_init(src._init){}
    virtual ~FailureCriterionBase(){};
    // failure check based on stress state tensor and internal variable
    virtual bool isFailed(const IPVariable* ipvprev, const IPVariable* ipv) const = 0;
    virtual FailureCriterionBase* clone() const = 0;
  #endif // SWIG
};

class TrivialFailureCriterion : public FailureCriterionBase{
  public:
    TrivialFailureCriterion(const int num);
    #ifndef SWIG
    TrivialFailureCriterion(const TrivialFailureCriterion& src): FailureCriterionBase(src){}
    virtual ~TrivialFailureCriterion(){}
    virtual bool isFailed(const IPVariable* ipvprev, const IPVariable* ipv) const {return false;};
    virtual FailureCriterionBase* clone() const {return new TrivialFailureCriterion(*this);};
    #endif // SWIG
};

class CriticalDamageCriterion : public FailureCriterionBase{
	protected:
		double _Dc; // critical damage

	public:
		CriticalDamageCriterion(const int num, const double Dc);
		#ifndef SWIG
		CriticalDamageCriterion(const CriticalDamageCriterion& src):FailureCriterionBase(src),_Dc(src._Dc){}
		virtual ~CriticalDamageCriterion(){}
		virtual bool isFailed(const IPVariable* ipvprev, const IPVariable* ipv) const;
    virtual FailureCriterionBase* clone() const {return new CriticalDamageCriterion(*this);};
		#endif // SWIG
};

class GeneralCritialCriterion : public FailureCriterionBase{
  public:
      enum EXTREME_TYPE{UPPER_BOUND=1, LOWER_BOUND=2};

  protected:
    double _cricalValue;
    IPField::Output _ipVal;
    EXTREME_TYPE _type;

  public:
    GeneralCritialCriterion(const int num, const double cval, const int ipval, const int type);
    #ifndef SWIG
    GeneralCritialCriterion(const GeneralCritialCriterion& src):FailureCriterionBase(src),_cricalValue(src._cricalValue),
            _ipVal(src._ipVal),_type(src._type){}
		virtual ~GeneralCritialCriterion(){}
		virtual bool isFailed(const IPVariable* ipvprev, const IPVariable* ipv) const;
    virtual FailureCriterionBase* clone() const {return new GeneralCritialCriterion(*this);};
    #endif //SWIG
};

class maximalPrincipleStressWithIPNonLocalPorosity : public FailureCriterionBase{
  protected:
    double _sigmac; // critical value

  public:
    maximalPrincipleStressWithIPNonLocalPorosity(const int num, const double sig);
    #ifndef SWIG
    maximalPrincipleStressWithIPNonLocalPorosity(const maximalPrincipleStressWithIPNonLocalPorosity& src):FailureCriterionBase(src),_sigmac(src._sigmac){}
    virtual ~maximalPrincipleStressWithIPNonLocalPorosity(){};
    // failure check based on stress state tensor and internal variable
    virtual bool isFailed(const IPVariable* ipvprev, const IPVariable* ipv) const;
    virtual FailureCriterionBase* clone() const {return new maximalPrincipleStressWithIPNonLocalPorosity(*this);};
    #endif // SWIG
};

class ThomasonCoalescenceConditionWithIPNonLocalPorosity : public FailureCriterionBase{
  protected:
    double _alpha, _beta;
    double _kappa;
    double _lambda0;

  public:
    ThomasonCoalescenceConditionWithIPNonLocalPorosity(const int num, const double kk, const double lam0);
    #ifndef SWIG
    ThomasonCoalescenceConditionWithIPNonLocalPorosity(const ThomasonCoalescenceConditionWithIPNonLocalPorosity& src):FailureCriterionBase(src),
      _alpha(src._alpha),_beta(src._beta),_kappa(src._kappa),_lambda0(src._lambda0){}
    virtual ~ThomasonCoalescenceConditionWithIPNonLocalPorosity(){};
    // failure check based on stress state tensor and internal variable
    virtual bool isFailed(const IPVariable* ipvprev, const IPVariable* ipv) const;
    virtual FailureCriterionBase* clone() const {return new ThomasonCoalescenceConditionWithIPNonLocalPorosity(*this);};
    #endif // SWIG
};

class MultipleFailureCriterion : public FailureCriterionBase{
  protected:
    std::vector<FailureCriterionBase*> _allCriterion;

  public:
    MultipleFailureCriterion(const int num);
    void setFailureCriterion(const FailureCriterionBase& cr);
    #ifndef SWIG
    MultipleFailureCriterion(const MultipleFailureCriterion& src);
    virtual ~MultipleFailureCriterion();
    // failure check based on stress state tensor and internal variable
    virtual bool isFailed(const IPVariable* ipvprev, const IPVariable* ipv) const;
    virtual FailureCriterionBase* clone() const {return new MultipleFailureCriterion(*this);};
    #endif // SWIG
};
#endif // FAILURECRITERION_H_
