//
// C++ Interface: material law
//
// Description: Generic Crack Phase Field
//
// Mohib Mustafa
// 03/12/24

#ifndef MLAWGENERICCRACKPHASEFIELD_H
#define MLAWGENERICCRACKPHASEFIELD_H

#include "mlaw.h"
#include "STensor3.h"
#include "STensor43.h"
#include "STensor63.h"
#include "ipGenericCrackPhaseField.h"
#include "CLengthLaw.h"
#include "MInterfaceElement.h"
#include "nonLinearMechSolver.h"

class mlawGenericCrackPhaseField : public materialLaw{
protected:

    materialLaw* _matlaw;
    CLengthLaw* _cLLaw;
    double _rho;
    // double _lc;
    double _gc;


public:
    mlawGenericCrackPhaseField(int num, double rho, const CLengthLaw &cLLaw, double gc);
    const materialLaw& getConstRefMechanicalMaterialLaw() const;
    materialLaw &getRefMechanicalMaterialLaw();
    virtual void setMechanicalMaterialLaw(const materialLaw *matlaw);
    virtual void setTime(const double ctime, const double dtime);
    virtual void setMacroSolver(const nonLinearMechSolver *sv);

#ifndef SWIG
    mlawGenericCrackPhaseField(const mlawGenericCrackPhaseField& source);
    
    virtual ~mlawGenericCrackPhaseField();
    mlawGenericCrackPhaseField& operator=(const materialLaw &source);
    virtual void initLaws(const std::map<int, materialLaw *> &maplaw){};
    virtual double soundSpeed() const;
    virtual double density() const;
    virtual double ElasticShearModulus() const;
    virtual void checkInternalState(IPVariable *ipv, const IPVariable *ipvprev) const{};
    virtual matname getType() const{return materialLaw::genericCrackPhaseField;}
    virtual materialLaw* clone() const {return new mlawGenericCrackPhaseField(*this);}

    virtual void constitutive(
                          const STensor3& F0,         // initial deformation gradient (input @ time n)
                          const STensor3& Fn,         // updated deformation gradient (input @ time n+1)
                          STensor3 &P,                // updated 1st Piola-Kirchhoff stress tensor (output)
                                                      // contains the initial values on input
                          const IPVariable *q0,       // array of initial internal variable
                          IPVariable *q1,             // updated array of internal variable (in ipvcur on output),
                          STensor43 &Tangent,         // constitutive tangents (output)
                          const bool stiff,            // if true compute the tangents
                          STensor43* elasticTangent = NULL,
                          const bool dTangent =false,
                          STensor63* dCalgdeps = NULL) const
    {Msg::Error(("mlawGenericCrackPhaseField::constitutive only non local version should be called"));}

    virtual void constitutive(
                          const STensor3& F0,         // initial deformation gradient (input @ time n)
                          const STensor3& Fn,         // updated deformation gradient (input @ time n+1)
                          STensor3 &P,                // updated 1st Piola-Kirchhoff stress tensor (output)
                                                      // contains the initial values on input
                          const IPVariable *q0,       // array of initial internal variable
                          IPVariable *q1,             // updated array of internal variable (in ipvcur on output),
                          STensor43 &Tangent,         // constitutive tangents (output)
                          std::vector< STensor3 >  &dLocalPlasticStrainDStrain,
                          std::vector< STensor3 >  &dStressDNonLocalPlasticStrain,
                          fullMatrix<double>   &dLocalPlasticStrainDNonLocalPlasticStrain,
                          const bool stiff,            // if true compute the tangents
                          STensor43* elasticTangent = NULL) const;


    virtual bool withEnergyDissipation() const;
    virtual void ElasticStiffness(STensor43 *elasticStiffness) const;
    virtual void createIPState(IPStateBase* &ips,bool hasBodyForce,const bool* state_,const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const;
    virtual void createIPVariable(IPGenericCrackPhaseField *&ipv, bool hasBodyForce, const MElement *ele,const int nbFF, const IntPt *GP, const int gpt) const;
    virtual int getNbNonLocalVariables() const {return 1;}
#endif //SWIG
};

#endif //MLAWGENERICCRACKPHASEFIELD_H
