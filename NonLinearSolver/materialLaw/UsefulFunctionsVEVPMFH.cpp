/*************************************************************************************************/
/** Copyright (C) 2022 - See COPYING file that comes with this distribution	                    **/
/** Author: Mohamed HADDAD (MEMA-iMMC-UCLouvain)							                    **/
/** Contact: mohamed.haddad@uclouvain.be					                                    **/
/**											                                                    **/
/** Description: storing functions needed for VE-VP material law (small strains)    		    **/
/** VE-VP material law : See B.Miled et I.Doghri 2011                                           **/
/*************************************************************************************************/

#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "UsefulFunctionsVEVPMFH.h"


//Description: Convert a matrix(3*3) to a vector(6*1) (Issam Doghri's convention)
//Author: Mohamed Haddad
//Created: 01/04/2020
//Copyright: See COPYING file that comes with this distribution

void VEVPMFHSPACE::convertmatrvect (double **matr, double *vect){
  
  /** Declaration **/
  int i;

  /**** Start ****/
  for (i=0; i<3; i++) {vect[i] = matr[i][i];}
  
  vect[3]=sqrt(2)*matr[0][1];
  vect[4]=sqrt(2)*matr[1][2];
  vect[5]=sqrt(2)*matr[0][2];
  
}


//Description: Convert a vector(6*1) to a matrix (3*3) (Issam Doghri's convention)
//Author: Mohamed Haddad
//Created: 01/04/2020
//Copyright: See COPYING file that comes with this distribution

void VEVPMFHSPACE::convertvectmatr (double *vect, double **matr){
  
  /** Declaration **/
  int i;
  
  /**** Start ****/
  for (i=0; i<3; i++) {matr[i][i] = vect[i];}
  
  matr[0][1] = vect[3]/sqrt(2);
  matr[1][0] = matr[0][1];
  matr[1][2] = vect[4]/sqrt(2);
  matr[2][1] = matr[1][2];
  matr[0][2] = vect[5]/sqrt(2);
  matr[2][0] = matr[0][2];
  
}


//Description: Sum of two 4th order tensors (stored as matrices)
//Author: Mohamed Haddad
//Created: 01/04/2020
//Copyright: See COPYING file that comes with this distribution

void VEVPMFHSPACE::addtens4 (double a, double **A, double b, double **B, double **res){
  
  /** Declaration **/
  int i,j;

  /**** Start ****/
  if (b!=0) {      /* b != 0 */
    for (i=0; i<6;i++) {
      for (j=0; j<6; j++) {
	res[i][j] = a*A[i][j] + b*B[i][j];
      }
    }
  }

  else {           /* b == 0 */
    for (i=0; i<6;i++) {
      for (j=0; j<6; j++) {
	res[i][j] = a*A[i][j];
      }
    }
  }
  
}




//Description: Sum of two 2nd order tensors (stored as vectors)
//Author: Mohamed Haddad
//Created: 01/04/2020
//Copyright: See COPYING file that comes with this distribution

void VEVPMFHSPACE::addtens2 (double a, double *A, double b, double *B, double *res){
  
  /** Declaration **/
  int i;

  /**** Start ****/
  if (b!=0) {                /* b!=0 */
    for (i=0; i<6; i++) {
      res[i] = a*A[i] + b*B[i];
    }
  }

  else {                      /* b==0 */
    for (i=0; i<6; i++) {
      res[i] = a*A[i];
    }
  }

}



//Description: Double contraction of two 2nd order tensors (stored as vectors)
//Author: Mohamed Haddad
//Created: 01/04/2020
//Copyright: See COPYING file that comes with this distribution

double VEVPMFHSPACE::contraction22 (const double *TA, const double *TB){

  /** Declaration **/
  int i;
  double res = 0;
  
  /**** Start ****/
  for(i=0; i<6; i++) {res = res + TA[i]*TB[i];}

  return res;

}
    
    
    
//Description: Double contraction of 4th order tensor with a 2nd order tensors
//Author: Mohamed Haddad
//Created: 01/04/2020
//Copyright: See COPYING file that comes with this distribution

void VEVPMFHSPACE::contraction42 (double **T4, double *T2, double *Sol){

  /** Declaration **/
  int i,j;
  
  /**** Start ****/
  for (i=0; i<6; i++) {
    Sol[i] = 0;

    for (j=0; j<6; j++) {
      Sol[i] = Sol[i] + T4[i][j] * T2[j];
    }
  }
  
}



//Description: Double contraction of two 4th order tensors
//Author: Mohamed Haddad
//Created: 01/04/2020
//Copyright: See COPYING file that comes with this distribution

void VEVPMFHSPACE::contraction44 (double **TA, double **TB, double **Tres){
  
  /** Declaration **/
  int i,j,k;

  /**** Start ****/
  for (i=0; i<6; i++) {

    for (j=0; j<6; j++) {
      Tres[i][j] = 0;

      for (k=0;k<6;k++) {
        Tres[i][j] = Tres[i][j] + TA[i][k]*TB[k][j];
      }
    }
  }

}



//Description: Double contraction of two 4th order tensors
//Author: Mohamed Haddad
//Created: 01/04/2020
//Copyright: See COPYING file that comes with this distribution

double VEVPMFHSPACE::dcontr44 (double **TA, double **TB){
  
  /** Declaration **/
  int i,j;
  double sol;

  /**** Start ****/
  sol = 0;

  for (i=0; i<6; i++) {
    for (j=0; j<6; j++) {
      sol = sol + TA[i][j]*TB[i][j];
    }
  }
  
  return(sol);

}



//Description: Double contraction of two 4th order tensors followed by a double contraction with 2nd order tensor 
//Author: Mohamed Haddad
//Created: 01/04/2020
//Copyright: See COPYING file that comes with this distribution

void VEVPMFHSPACE::contraction442 (double **A, double **B, double *C, double *Tres){
  
  /** Declaration **/
  double temp[6];

  /**** Start ****/
  VEVPMFHSPACE::contraction42(B,C,temp);
  VEVPMFHSPACE::contraction42(A, temp, Tres);
  
}



//Description: Double contraction of three 4th order tensors
//Author: Mohamed Haddad
//Created: 01/04/2020
//Copyright: See COPYING file that comes with this distribution

double VEVPMFHSPACE::contr444_reduced (double **A, double **B, double **C){

  /** Declaration **/
  int i;
  double I2[6];
  double *temp1, *temp2, *temp3;
  double sol;

  /** Allocation & Init. **/
  for (i=0; i<3; i++) {
    I2[i] = 1;
    I2[i+3] = 0;
  }

  temp1 = (double*)malloc(6*sizeof(double));
  temp2 = (double*)malloc(6*sizeof(double));
  temp3 = (double*)malloc(6*sizeof(double));
  
  /**** Start ****/
  VEVPMFHSPACE::contraction42(A, I2, temp1);
  VEVPMFHSPACE::contraction42(C, I2, temp2);
  VEVPMFHSPACE::contraction42(B, temp2, temp3);
  sol=VEVPMFHSPACE::contraction22(temp1, temp3);
  
  /** Freeing **/
  free(temp1);
  free(temp2);
  free(temp3);

  return(sol);

}



//Description: Double contraction of three 4th order tensors
//Author: Mohamed Haddad
//Created: 01/04/2020
//Copyright: See COPYING file that comes with this distribution

double VEVPMFHSPACE::contraction444 (double **A, double **B, double **C){
  
  /** Declaration **/
  double **temp;
  double sol;

  /** Allocation & Init. **/
  VEVPMFHSPACE::mallocmatrix(&temp, 6, 6);

  /**** Start ****/
  VEVPMFHSPACE::contraction44(A, B, temp);
  sol=VEVPMFHSPACE::dcontr44(temp, C);

  /** Freeing **/
  VEVPMFHSPACE::freematrix(temp, 6);

  return(sol);
  
}



//Description: Tensor product of two 2nd order tensors  
//Author: Mohamed Haddad
//Created: 01/04/2020
//Copyright: See COPYING file that comes with this distribution

void VEVPMFHSPACE::produit_tensoriel (double *TA, double *TB, double **Tres){
  
  /** Declaration **/
  int i,j;

  /**** Start ****/
  for (i=0; i<6; i++) {
    for (j=0; j<6; j++) {
      Tres[i][j] = TA[i]*TB[j];
    }
  }
  
}





//Description: Max of two numbers 
//Author: Mohamed Haddad
//Created: april 2020
//Copyright: See COPYING file that comes with this distribution

double VEVPMFHSPACE::max(double a, double b){

  if (a>b) return(a);
  else return (b);

}


//Description: DECOMPOSITION LU D'UNE MATRICE CAREE D'ORDRE QUELCONQUE
//Author: Olivier Pierard
//Created: april 2003
//Copyright: See COPYING file that comes with this distribution

/* "ludcmp": computes the LU decomposition of a square matrix.
 * Original: Olivier Pierard (april 2003)
 * Source : Numerical recipies in C pp44-50(available from internet)
 * Some modifications about indices were rapported */

#define TINY 1.0e-14

void VEVPMFHSPACE::ludcmp(double **a, int n, int *indx, double *d, int *error){
/* Given a matrix a(n*n), this routine replaces it by the LU decomposition of a rows permutation 
itself.  a and n are input.  a is output containing L and U; indx[1..n] si an output vector that records 
the row permutation effected y the partial pivoting, d is outpus as +/-1 depending on whether the number
of row interchanges was even or odd, respectively.  This routine is used in combination with lubksb to 
solve linear equations or invert a matrix.  error=1 if matrix is singular(but LU decomposition possible).
If one row is null, the function is automatically stopped*/


  int i,imax,j,k;
  double big, dum, sum, temp;
  double *vv;
  
  *error = 0;
  *d = 1.0;
  vv=(double*)malloc(n*sizeof(double));
  for (i=0;i<n;i++) {
    big = 0.0;
    for(j=0;j<n;j++) {
      temp = fabs(a[i][j]);
      if (temp>big) {
	big = temp;
      }
    }
    if (big < TINY) {
      printf("Singular matrix in routine ludcmp\n"); 
      *error = 1;
      return;
    }
    vv[i]=1.0/big;  /* Save th scaling */
  }
  for (j=0;j<n;j++) {
    for (i=0;i<j;i++) {
      sum = a[i][j];
      for(k=0;k<i;k++) {
	sum -= a[i][k]*a[k][j];
      }
      a[i][j]=sum;
    }
    big = 0.0;
    for(i=j;i<n;i++) {
      sum = a[i][j];
      for(k=0;k<j;k++) {
	sum -= a[i][k]*a[k][j];      
      }
      a[i][j]=sum;
      if ( (dum=vv[i]*fabs(sum)) >=big) {
	/* Is the figure of merit for the pivot better than the best so far? */
	big=dum;
	imax = i;
      }
    }
    if(j!= imax) { /* Do we need to interchange rows? */
      for (k=0;k<n;k++) {  /* Yes, do so... */
	dum = a[imax][k];
	a[imax][k]=a[j][k];
	a[j][k]=dum;
      }
      *d = -(*d);  /* And change the parity of d */
      vv[imax] = vv[j];  /* Also interchange the scale factor */
    }
    indx[j]=imax;
    if (a[j][j] == 0.0) {
      a[j][j] = TINY;
      *error = 1;
    }
    /* If the pivot element is zero in the matrix is singular (at least to the precision of the algorithm).
       For some applications on singular matrices, it is desirale to substitute TINY for zero. */

    if (j != n) {
      dum = 1.0/(a[j][j]);
      for (i=j+1;i<n;i++) {
	a[i][j] *= dum;
      }
    }
  }
  free(vv);
}




//Description: FORWARD AND BACKWARD SUBSTITUTION
//Author: Olivier Pierard
//Created: april 2003
//Copyright: See COPYING file that comes with this distribution

void VEVPMFHSPACE::lubksb (double **a, int n, int *indx, double b[]){

     /* Solves the set of n linear equations A.X=b.  Here a(n*n) is input, not as the matrix A but rather 
as its LU decomposition, determined by the routine ludcmp.  indx[1..n] is input as the permutation vector 
returned by ludcmp.  b[1..n] is input as the right-hand side vector , and returns with the solution 
vector X.  a,n, and indx are not modified by this routine and can be left in place for successive calls 
with different right-hand sides b.  This routine takes into account the possibility that b will begin 
with many zero elements, do it is efficient for use in matrix inversion. */


  int i, ii=-1, ip, j;
  double sum;

  for(i=0;i<n;i++) { /* When ii is set to a positive or null value, it will become the */
    ip = indx[i];    /* index of the first non vanishing element of b. We now  */
    sum = b[ip];     /* do the forward substitution.  The only new wrinkle is  */
    b[ip]=b[i];      /* to unscramble the permutation as we go.                */
    if (ii>=0)
      for (j=ii;j<i;j++) {
	sum -= a[i][j]*b[j];
      }
    else if (sum) {  /* A nonzero element was encountered, so from now on we */
      ii=i;          /* will have to do the sums in the loop above.          */
    }
    b[i]=sum;
  }

  for (i = n-1;i>=0;i--) {  /* Now we do the backsubstitution */
    sum=b[i];
    for (j=i+1;j<n;j++) {
      sum -= a[i][j]*b[j];
    }   
    b[i]=sum/a[i][i];    /* Store a component of the solution vector X. */
  }                      /* All done! */

}



//Description: Compute the inverse of matrix
//Author: Olivier Pierard
//Created: april 2003
//Copyright: See COPYING file that comes with this distribution

void VEVPMFHSPACE::inverse (double **a, double **y, int *error, int n){

  double d;
  double *col;
  int i,j,*indx;
  double **aoriginal; 
  
  VEVPMFHSPACE::mallocmatrix(&aoriginal, n, n);
  col = (double*)malloc(n*sizeof(double));
  indx = (int*)malloc (n*sizeof(int));

  for(i=0;i<n;i++) {     /* On mémorise la matrice de départ à VEVPMFHSPACE::inverser */
    for(j=0;j<n;j++) {
      aoriginal[i][j]=a[i][j];
    }
  }

  VEVPMFHSPACE::ludcmp(a,n,indx,&d,error);
  if (*error != 0) {
    return;
  }

  for(j=0;j<n;j++) {
    for(i=0;i<n;i++) {
      col[i]=0.0;
    }
    col[j]=1.0;
    VEVPMFHSPACE::lubksb(a,n,indx,col);
    for (i=0;i<n;i++) {
      y[i][j]=col[i];
    }
  }
  for(i=0;i<n;i++) {     /* A has the LU decomposition form; we have to put the  */
    for(j=0;j<n;j++) {   /* original expression back */
      a[i][j] = aoriginal[i][j];
    }
  }

  free(col);
  free(indx);
  VEVPMFHSPACE::freematrix(aoriginal, n);
}




//Description: Extract the isotropic part of a 4th order tensor 
//Author: Olivier Pierard
//Created: april 2003
//Modified:  02/04/2020 by M.Haddad
//Copyright: See COPYING file that comes with this distribution
//** INPUT : A fourth order tensor (6*6)
//** OUTPUT : An isotropic fourth order tensor

void VEVPMFHSPACE::extractiso (double **tensani, double **tensiso)
{
  double deltaij[6], deltakl[6];
  double I[6][6]={0};
  double **I_vol;
  double I_dev[6][6]={0};
  double partvol=0;
  double partdev=0;
  int i,j;

  for (i=0;i<3;i++) {
    deltaij[i] = 1;
    deltakl[i] = 1;
    deltaij[i+3] = 0;
    deltakl[i+3] = 0;
  }

  VEVPMFHSPACE::mallocmatrix(&I_vol,6,6);
 VEVPMFHSPACE::produit_tensoriel(deltaij, deltakl, I_vol);

  for (i=0;i<6;i++) {
    for (j=0;j<6;j++) {
      I_vol[i][j] = I_vol[i][j]/3;
    }
    I[i][i]=1;
  }
  for (i=0;i<6;i++) {
    for (j=0;j<6;j++) {
      I_dev[i][j]=I[i][j]-I_vol[i][j];
    }
  }

  for (i=0;i<3;i++) {
    for (j=0;j<3;j++) {
      partvol = partvol + tensani[i][j];
    }
  }
  partvol =  partvol/3;

  for (i=0;i<6;i++) {
    partdev = partdev + tensani[i][i];
  }
  partdev = partdev - partvol;

  for (i=0;i<6;i++) {
    for (j=0;j<6;j++) {
      tensiso[i][j] = partvol*I_vol[i][j] + 0.2*partdev*I_dev[i][j];
    }
  }

  VEVPMFHSPACE::freematrix(I_vol,6);
}




//Description: Memory allocation for a table of pointers 
//Author: Mohamed Haddad
//Created: 03/04/2020
//Copyright: See COPYING file that comes with this distribution
 
void VEVPMFHSPACE::mallocmatrix (double ***A, int m, int n){
  
  /** Dec **/
  int i,j;

  /** Alloc. & Init. **/
  *A = (double**)malloc(m*sizeof(double*));
  
  /**** Start ****/
  for(i=0;i<m;i++){
    (*A)[i] = (double*)malloc(n*sizeof(double));
  }

  for(i=0;i<m;i++) {
    for(j=0;j<n;j++) {
      (*A)[i][j]=0;
    }
  }
  
}




//Description: Freeing memory of a table of pointers
//Author: Mohamed Haddad
//Created: 01/04/2020
//Copyright: See COPYING file that comes with this distribution

void VEVPMFHSPACE::freematrix (double **A, int m){
  
  /** Dec **/
  int i;

  /**** Start ****/
  for (i=0;i<m;i++) {
    free (A[i]);
  }
  
  /** Freeing **/
  free (A);

}




//Description: Memory allocation for a 3D matrix
//Author: Mohamed Haddad
//Created: 03/04/2020
//Copyright: See COPYING file that comes with this distribution

void VEVPMFHSPACE::mallocmatrix3D (double ****A, int p, int m, int n){
  
  /** Dec **/
  int i,j,k;

  /** Alloc. & Init. **/
  *A = (double***)malloc(p*sizeof(double**));
  
  /**** Start ****/
  for(k=0;k<p;k++){
    (*A)[k] = (double**)malloc(m*sizeof(double*));
  }

  for(k=0;k<p;k++) {
    for(i=0;i<m;i++) {
      (*A)[k][i] = (double*)malloc(n*sizeof(double));
      for(j=0;j<n;j++) {
	(*A)[k][i][j]=0;
      }
    }
  }

}





//Description: Freeing memory of a 3D matrix
//Author: Mohamed Haddad
//Created: 03/04/2020
//Copyright: See COPYING file that comes with this distribution

void VEVPMFHSPACE::freematrix3D (double ***A, int p, int m){

  /** Dec **/
  int k, i;

  /**** Start ****/
  for (k=0; k<p; k++) {
    for (i=0; i<m; i++) {
      free (A[k][i]);
    }
    free (A[k]);
  }
  
  /** Freeing **/
  free(A);
  
}





//Description: COMPUTE THE ISOTROPIC HOOKE'S ELASTIC OPERATOR FROM E AND NU 
//Author: Mohamed Haddad
//Created: 03/04/2020
//Copyright: See COPYING file that comes with this distribution

void VEVPMFHSPACE::compute_Hooke(double E, double nu, double **C){
  
  /** Dec **/
  double lambda;
  double mu;
  int i,j;

  /**** Start ****/
  if(nu==0.5) {
    printf("nu = 0.5, Impossible to compute lambda\n");
    exit(0);
  }

  mu = E/(2*(1+nu));
  lambda = (E*nu)/((1-2*nu)*(1+nu));

  for(i=0;i<6;i++) {
    for(j=0;j<6;j++) {
      C[i][j]=0;
    }
  }

  for (i=0;i<3;i++) {
    for(j=0;j<3;j++) {
      C[i][j] = lambda;
    }
  }
     
  for (i=0;i<6;i++) {
    C[i][i] = C[i][i] + 2*mu;
  }
     
}



//Description:  Print on the screen values of a vector
//Author: Mohamed Haddad
//Created: 03/04/2020
//Copyright: See COPYING file that comes with this distribution

void VEVPMFHSPACE::printvect(double *vect, int n){
  
  /** Dec **/
  int i;

  /**** Start */
  for (i=0;i<n;i++) {
    printf("\t %g\t",vect[i]);
  }
  printf("\n");
  
}



//Description: Print values of a vector to a specified file 
//Author: Mohamed Haddad
//Created: 03/04/2020
//Copyright: See COPYING file that comes with this distribution

void VEVPMFHSPACE::fprintvect(FILE *pfile, double *vect, int n){

  /** Dec **/
  int i;
 
  /** Start **/ 
  for (i=0; i<n; i++) {
    fprintf(pfile, "\t %g\t",vect[i]);
  }
  fprintf(pfile, "\n");
  
}



//Description: Print on the screen values of a m*n matrix  
//Author: Mohamed Haddad
//Created: 03/04/2020
//Copyright: See COPYING file that comes with this distribution

void VEVPMFHSPACE::printmatr(double **matr4, int m, int n){
  
  /** Dec **/
  int i, j;

  /**** Start **/
  for (i=0; i<m; i++) {
    for (j=0; j<n; j++) {
      printf ("\t %g\t ", matr4[i][j]);
    }
    printf("\n");
  }
  printf("\n");
  
}   




//Description: Print values of a m*n matrix to a specified file   
//Author: Mohamed Haddad
//Created: 03/04/2020
//Copyright: See COPYING file that comes with this distribution

void VEVPMFHSPACE::fprintmatr(FILE *pfile, double **matr4, int m, int n){

  /** Dec **/
  int i, j;

  /**** Start **/
  for (i=0; i<m; i++) {
    for (j=0; j<n; j++) {
      fprintf (pfile, "\t %g\t ", matr4[i][j]);
    }
    fprintf(pfile, "\n");
  }
  fprintf(pfile, "\n");
  
}   




//Description: Compute the trace of a second order tensor   
//Author: Mohamed Haddad
//Created: 05/04/2020
//Copyright: See COPYING file that comes with this distribution

double VEVPMFHSPACE::trace(double *tens2){

  /** Dec **/
  int i;
  double tr=0;

  for (i=0;i<3;i++) {
    tr = tr + tens2[i];
  }
  
  return (tr);

}





//Description: Extract the deviatoric part of a second order tensor  
//Author: Mohamed Haddad
//Created: 05/04/2020
//Copyright: See COPYING file that comes with this distribution

void VEVPMFHSPACE::dev(double *tens2, double *dev_tens2){

  /** Dec **/
  int i;
  double tr_tens2=0;

  /**** Start ****/
  for (i=0;i<3;i++) {
    tr_tens2 = tr_tens2 + tens2[i];
  }

  for (i=0;i<3;i++) {
    dev_tens2[i] = tens2[i] - tr_tens2/3;
  }

  for (i=3;i<6;i++) {
    dev_tens2[i] = tens2[i];
  }

}






//Description: Compute the von Mises equivalent stress  
//Author: Mohamed Haddad
//Created: 05/04/2020
//Copyright: See COPYING file that comes with this distribution

double VEVPMFHSPACE::vonmises(double *tens2){

  return(pow(0.5*(pow(tens2[0]-tens2[1],2) + pow(tens2[1]-tens2[2],2) + pow(tens2[2]-tens2[0],2)) + 3*pow(tens2[3]/sqrt(2),2) + 3*pow(tens2[4]/sqrt(2),2) + 3*pow(tens2[5]/sqrt(2),2),0.5));

}





//Description: Compute the equivalent strain   
//Author: Mohamed Haddad
//Created: 05/04/2020
//Copyright: See COPYING file that comes with this distribution

double VEVPMFHSPACE::eqstrn(double *tens2){
  return(pow(2./9.*(pow(tens2[0]-tens2[1],2) + pow(tens2[1]-tens2[2],2) + pow(tens2[2]-tens2[0],2)) + 4./3.*pow(tens2[3]/sqrt(2),2) + 4./3.*pow(tens2[4]/sqrt(2),2) + 4./3.*pow(tens2[5]/sqrt(2),2),0.5));

}





//Description: Check if a vector is null 
//Author: Mohamed Haddad
//Created: 05/04/2020
//Copyright: See COPYING file that comes with this distribution

int VEVPMFHSPACE::isnull(double *A, int size){

  /** Dec & Init. **/
  int i, nil=1;

  /**** Start ****/
  for (i=0; i<size; i++) {
    if (A[i]!=0) {
      nil=0;
      break;
    }
  }

  return nil;

}






//Description: Check if a fourth order tensor is isotropic
//Author: O. Pierard
//Created: April 2004
//Copyright: See COPYING file that comes with this distribution
//
//**Input: A: a fourth order tensor
//**Output: res: 1 if A is isotropic; 0 otherwise 

int VEVPMFHSPACE::isisotropic(double**A){

  /** Dec & Init. **/ 
  double lambda, mu;
  int i, j;
  int res = 1;
  double tol=1e-2;

  /**** Start ****/
  lambda = A[0][1];
  mu = A[3][3]/2;

  for (i=0;i<3;i++) {
    for (j=0;j<3;j++) {
      if (i!=j) {
	if (fabs(A[i][j]-lambda) > tol) 
	  res = 0;
	if (fabs(A[i+3][j+3]) >tol)
	  res = 0;
      }
      else {   /* i==j i,j=1:3*/
	if (fabs(A[i][i] - (lambda+2*mu)) > tol) 
	  res = 0;
	if (fabs(A[i+3][j+3] - 2*mu)>tol)
	  res=0;
      }

      if (fabs(A[i+3][j])>tol)
	res=0;
      if (fabs(A[i][j+3]>tol))
	res=0;
    }
  }

  return res;
  
}




//Description: Copy a vector into another 
//Author: Mohamed Haddad 
//Created: 06/04/2020
//Copyright: See COPYING file that comes with this distribution

void VEVPMFHSPACE::copyvect(double *A, double *B, int size){

  /** Dec **/
  int i;

  for (i=0; i<size; i++) {
    B[i] = A[i];
  }
  
}





//Description: Compute Identity tensor I
//Author: Mohamed Haddad 
//Created: November 2020 
//Copyright: See COPYING file that comes with this distribution

void VEVPMFHSPACE::compute_I(double **I){
  
  /** Dec **/
  int i,j;

  /**** Start ****/
  for(j=0;j<6;j++){
    for(i=0;i<6;i++)
      I[i][j] = 0;  
   }

  for(i=0;i<6;i++)
    I[i][i] = 1;

}




//Description: FUNCTION TO COMPUTE Ivol (6*6) TENSOR
//Author: Mohamed Haddad 
//Created: November 2020 
//Copyright: See COPYING file that comes with this distribution

void VEVPMFHSPACE::compute_Ivol(double **I_vol){
  
  /** Dec **/
  int i,j;
  double *deltaij;
  double *deltakl;
  
  /** Alloc. **/
  deltaij=(double*)malloc(6*sizeof(double));
  deltakl=(double*)malloc(6*sizeof(double));

  /**** Start ****/
  for (i=0;i<3;i++) {
    deltaij[i] = 1;
    deltakl[i] = 1;
    deltaij[i+3] = 0;
    deltakl[i+3] = 0;
  }
  

  VEVPMFHSPACE::produit_tensoriel(deltaij, deltakl, I_vol);

    for (i=0;i<6;i++) {
      for (j=0;j<6;j++) {
        I_vol[i][j] = I_vol[i][j]/3;
      }
    }

  /** Freeing **/
  free(deltaij);
  free(deltakl);

}






//Description: FUNCTION TO COMPUTE Idev (6*6) TENSOR
//Author: Mohamed Haddad 
//Created: 06/04/2020
//Copyright: See COPYING file that comes with this distribution

void VEVPMFHSPACE::compute_Idev(double **I_dev){
  
  /** Declaration **/
  double **I, **Ivol;

  /** Allocation **/
  VEVPMFHSPACE::mallocmatrix(&I,6,6);
  VEVPMFHSPACE::mallocmatrix(&Ivol,6,6);

  /**** Start ****/
  VEVPMFHSPACE::compute_I(I);
  VEVPMFHSPACE::compute_Ivol(Ivol);

  VEVPMFHSPACE::addtens4(1,I,-1,Ivol,I_dev);


  /** Freeing **/
  VEVPMFHSPACE::freematrix(I,6);
  VEVPMFHSPACE::freematrix(Ivol,6);

}





//Description: Read material properties & simulation parameters from .dat file
//Author: Mohamed Haddad 
//Created: 06/04/2020
//Copyright: See COPYING file that comes with this distribution
void VEVPMFHSPACE::mallocMATPROP(VEVPMFHSPACE::MATPROP* matr, VEVPMFHSPACE::MATPROP* incl, const char* inputfilename){
  FILE *inputfile;
  char c;
  int i;
  
  int matr_nbrGi, matr_nbrKj, incl_nbrGi, incl_nbrKj;
  
  /**** Start ****/
  // Open input .dat file
  inputfile = fopen(inputfilename,"r");

  if (inputfile==NULL) {
    printf("\n The file %s cannot be opened, check that the file currently exists. \n", inputfilename);
    exit(1);
  }
  
  // Read kine by line 
  // Matrix properties
  
  for(i=0;i<18;i++) {
    while((c=fgetc(inputfile))!='\n');
  }
  fscanf(inputfile,"%i",&matr_nbrGi);
  
  for(i=0;i<(2*matr_nbrGi+1);i++) {
    while((c=fgetc(inputfile))!='\n');
  }
  fscanf(inputfile,"%i",&matr_nbrKj);
  
  for(i=0;i<26;i++) {
    while((c=fgetc(inputfile))!='\n');
  }
  fscanf(inputfile,"%i",&incl_nbrGi);
  
  for(i=0;i<(2*incl_nbrGi+1);i++) {
    while((c=fgetc(inputfile))!='\n');
  }
  fscanf(inputfile,"%i",&incl_nbrKj);
  
  matr->gi = (double*)calloc(matr_nbrGi,sizeof(double));
  matr->gi = (double*)calloc(matr_nbrGi,sizeof(double));
  
  matr->Kj = (double*)calloc(matr_nbrKj,sizeof(double));
  matr->kaj = (double*)calloc(matr_nbrKj,sizeof(double));
  
  
  incl->gi = (double*)calloc(incl_nbrGi,sizeof(double));
  incl->gi = (double*)calloc(incl_nbrGi,sizeof(double));
  
  incl->Kj = (double*)calloc(incl_nbrKj,sizeof(double));
  incl->kaj = (double*)calloc(incl_nbrKj,sizeof(double));
  
  
  fclose(inputfile);
}



//Description: Read material properties & simulation parameters from .dat file
//Author: Mohamed Haddad 
//Created: 06/04/2020
//Copyright: See COPYING file that comes with this distribution

void VEVPMFHSPACE::readinput (VEVPMFHSPACE::MATPROP *matr, VEVPMFHSPACE::MATPROP *incl, const char* inputfilename){

  /** Dec **/
  FILE *inputfile;
  char c;
  int i;
  int temp=0;
  double temp1=0;
  
  /**** Start ****/
  // Open input .dat file
  inputfile = fopen(inputfilename,"r");

  if (inputfile==NULL) {
    printf("\n The file %s cannot be opened, check that the file currently exists. \n", inputfilename);
    exit(1);
  }

  // Read input file line by line 
  // Matrix properties
  for(i=0;i<6;i++) {
    while((c=fgetc(inputfile))!='\n');
  }
  fscanf(inputfile,"%lf",&(matr->E0));
  while((c=fgetc(inputfile))!='\n');
  fscanf(inputfile,"%lf",&(matr->nu));
  while((c=fgetc(inputfile))!='\n');
  fscanf(inputfile,"%lf",&(matr->yldstrs));
  while((c=fgetc(inputfile))!='\n');
  fscanf(inputfile,"%d",&(matr->hardmodel));
  while((c=fgetc(inputfile))!='\n');
  fscanf(inputfile,"%lf",&(matr->hardmodulus1));
  while((c=fgetc(inputfile))!='\n');
  fscanf(inputfile,"%lf",&(matr->hardmodulus2));
  while((c=fgetc(inputfile))!='\n');
  fscanf(inputfile,"%lf",&(matr->hardexp));
  while((c=fgetc(inputfile))!='\n');
  fscanf(inputfile,"%d",&(matr->viscmodel));
  while((c=fgetc(inputfile))!='\n');
  fscanf(inputfile,"%lf",&(matr->visccoef));
  while((c=fgetc(inputfile))!='\n');
  fscanf(inputfile,"%lf",&(matr->viscexp1));
  while((c=fgetc(inputfile))!='\n');
  fscanf(inputfile,"%lf",&(matr->viscexp2));
  while((c=fgetc(inputfile))!='\n');

  while((c=fgetc(inputfile))!='\n');
  fscanf(inputfile,"%i",&(matr->nbrGi));
  matr->Gi = (double*) malloc((matr->nbrGi)*sizeof(double));

  for(i=0;i<matr->nbrGi;i++) {
    while((c=fgetc(inputfile))!='\n');
    fscanf(inputfile,"%le",&temp1);
    (matr->Gi)[i]=temp1;
  }

  matr->gi = (double*) malloc((matr->nbrGi)*sizeof(double));
  for(i=0;i<matr->nbrGi;i++) {
    while((c=fgetc(inputfile))!='\n');
    fscanf(inputfile,"%le",&temp1);
    (matr->gi)[i]=temp1;
  }

  matr->G0 = matr->E0/(2*(1+matr->nu));
  
  temp1=0;
  for(i=0;i<matr->nbrGi;i++)
    temp1+=matr->Gi[i];

  matr->G_inf = matr->G0 - temp1;

  while((c=fgetc(inputfile))!='\n');
  fscanf(inputfile,"%i",&(matr->nbrKj));

  matr->Kj = (double*) malloc((matr->nbrKj)*sizeof(double));

  for(i=0;i<matr->nbrKj;i++) {
    while((c=fgetc(inputfile))!='\n');
    fscanf(inputfile,"%le",&temp1);
    (matr->Kj)[i]=temp1;
  }

  matr->kaj = (double*) malloc((matr->nbrKj)*sizeof(double));
  for(i=0;i<matr->nbrKj;i++) {
    while((c=fgetc(inputfile))!='\n');
    fscanf(inputfile,"%le",&temp1);
    (matr->kaj)[i]=temp1;
  }


  matr->K0 = matr->E0/(3*(1-(2*matr->nu)));

  temp1=0;
  for(i=0;i<matr->nbrKj;i++)
    temp1+=matr->Kj[i];

  matr->K_inf = matr->K0 - temp1;

  for(i=0;i<matr->nbrGi;i++)
  if (matr->gi[i] == 0){
    printf("FATAL ERROR: gi must be not nil for the matrix\n");
    exit(0);
  }

  for(i=0;i<matr->nbrKj;i++)
  if (matr->kaj[i] == 0){
    printf("FATAL ERROR: kj must be not nil for the matrix\n");
    exit(0);
  }

 
  // Inclusion properties 
  for(i=0;i<4;i++) {
    while((c=fgetc(inputfile))!='\n');
  }

  fscanf(inputfile,"%lf",&(incl->E0));
  while((c=fgetc(inputfile))!='\n');
  fscanf(inputfile,"%lf",&(incl->nu));
  while((c=fgetc(inputfile))!='\n');
  fscanf(inputfile,"%lf",&(incl->yldstrs));
  while((c=fgetc(inputfile))!='\n');
  fscanf(inputfile,"%lf",&(incl->v));
  while((c=fgetc(inputfile))!='\n');
  fscanf(inputfile,"%lf",&(incl->ar));
  while((c=fgetc(inputfile))!='\n');
  fscanf(inputfile,"%d",&(incl->hardmodel));
  while((c=fgetc(inputfile))!='\n');
  fscanf(inputfile,"%lf",&(incl->hardmodulus1));
  while((c=fgetc(inputfile))!='\n');
  fscanf(inputfile,"%lf",&(incl->hardmodulus2));
  while((c=fgetc(inputfile))!='\n');
  fscanf(inputfile,"%lf",&(incl->hardexp));
  while((c=fgetc(inputfile))!='\n');
  fscanf(inputfile,"%d",&(incl->viscmodel));
  while((c=fgetc(inputfile))!='\n');
  fscanf(inputfile,"%lf",&(incl->visccoef));
  while((c=fgetc(inputfile))!='\n');
  fscanf(inputfile,"%lf",&(incl->viscexp1));
  while((c=fgetc(inputfile))!='\n');
  fscanf(inputfile,"%lf",&(incl->viscexp2));
  while((c=fgetc(inputfile))!='\n');

  while((c=fgetc(inputfile))!='\n');
  fscanf(inputfile,"%i",&(incl->nbrGi));


  incl->Gi = (double*) malloc((incl->nbrGi)*sizeof(double));

  for(i=0;i<incl->nbrGi;i++) {
    while((c=fgetc(inputfile))!='\n');
    fscanf(inputfile,"%le",&temp1);
    (incl->Gi)[i]=temp1;
  }

  incl->gi = (double*) malloc((incl->nbrGi)*sizeof(double));
  for(i=0;i<incl->nbrGi;i++) {
    while((c=fgetc(inputfile))!='\n');
    fscanf(inputfile,"%le",&temp1);
    (incl->gi)[i]=temp1;
  }

  incl->G0 = incl->E0/(2*(1+incl->nu));
  
  temp1=0;
  for(i=0;i<incl->nbrGi;i++)
    temp1+=incl->Gi[i];

  incl->G_inf = incl->G0 - temp1;

  while((c=fgetc(inputfile))!='\n');
  fscanf(inputfile,"%i",&(incl->nbrKj));

  incl->Kj = (double*) malloc((incl->nbrKj)*sizeof(double));

  for(i=0;i<incl->nbrKj;i++) {
    while((c=fgetc(inputfile))!='\n');
    fscanf(inputfile,"%le",&temp1);
    (incl->Kj)[i]=temp1;
  }

  incl->kaj = (double*) malloc((incl->nbrKj)*sizeof(double));
  for(i=0;i<incl->nbrKj;i++) {
    while((c=fgetc(inputfile))!='\n');
    fscanf(inputfile,"%le",&temp1);
    (incl->kaj)[i]=temp1;
  }


  incl->K0 = incl->E0/(3*(1-(2*incl->nu)));

  temp1=0;
  for(i=0;i<incl->nbrKj;i++)
    temp1+=incl->Kj[i];

  incl->K_inf = incl->K0 - temp1;
  
  // Matrix volume fraction
  matr->v = 1 - incl->v;
  matr->ar=0;         /* Aspect Ratio has no sense for the matrix */
  matr->orient[0]=0;  /* Could represent the orientation of the RVE in a global referential */
  matr->orient[1]=0;
  matr->orient[2]=0;


  // Avoid case of zero relaxation times (if yes exit(0)) 
  for(i=0;i<incl->nbrGi;i++)
  if (incl->gi[i] == 0){
    printf("FATAL ERROR: gi must be not nil for the inclusions\n");
    exit(0);
  }

  for(i=0;i<incl->nbrKj;i++)
  if (incl->kaj[i] == 0){
    printf("FATAL ERROR: kj must be not nil for the inclusions\n");
    exit(0);
  }

 
  
  // Inclusion's orientation 
  for(i=0;i<4;i++) {
    while((c=fgetc(inputfile))!='\n');
  }

  fscanf(inputfile,"%lf",&(incl->orient[0]));
  while((c=fgetc(inputfile))!='\n');
  fscanf(inputfile,"%lf",&(incl->orient[1]));
  while((c=fgetc(inputfile))!='\n');
  fscanf(inputfile,"%lf",&(incl->orient[2]));
  
  // End reading & close input file 
  fclose(inputfile);
  
}

 
 
 

//Description: COMPUTE HARDENING
//Author: Mohamed Haddad 
//Created: 06/04/2020
//Modified: December 2020 by M.Haddad (Addition of Mixed power and exponential law) 
//Copyright: See COPYING file that comes with this distribution 
// 										
/**INPUT:	-ACUMULTED PLASTICITY
//	  	-MATERIAL PARAMETERS
//**OUTPUT: 	-R(P)	
//	   	-dR/dp*/							

void VEVPMFHSPACE::compute_R (double p, const VEVPMFHSPACE::MATPROP *mat, double *Rp, double *dRdp){

  // Invalid hardening properties 
  if ((p<0.)||((mat->hardexp)<=0.)) {
    printf("Invalid parameter(s) for the hardening law in function VEVPMFHSPACE::compute_R:\np: %g, hardmod1: %g, hardmod2: %g, hardexp: %g\n", p, mat->hardmodulus1 , mat->hardmodulus2, mat->hardexp);
    exit(0);
  }


  switch (mat->hardmodel) {
  
  // Law 1: Power law
  case 1:
         
    /*if (hardexp<=1) 
      printf("WARNING: Non continuous derivative of the power law hardening function due to a hardening exponent <= 1\n");*/
    
    if ((mat->hardexp)>1) 
      printf("WARNING: Exponent of the power law hardening function should be inferior to 1 to be physical (Lemaitre/Chaboche p168/177)!\n");

    // p=0
    if (p==0) {
      *Rp=0;
      if ((mat->hardexp)>1.0+1e-10) 					/* n>1 */
	*dRdp = 0;
      else if (fabs((mat->hardexp)-1.0)<1e-10) 			/* n=1 */
	*dRdp = (mat->hardmodulus1)*(mat->hardexp);
      else if (((mat->hardexp)>0)&&((mat->hardexp)<1.0-1e-10)) 	/* n < 1*/
	*dRdp = (mat->hardmodulus1)*(mat->hardexp)/(pow(1e-10, 1-(mat->hardexp)));
      else
	printf("Bad choice of hardening exponent with the Power law hardening model\n");
    }
    
    // p!= 0
    else {      
      *Rp = (mat->hardmodulus1)*pow(p,(mat->hardexp));
      *dRdp = (mat->hardexp)*(*Rp)/p ; 
    }
    break;


  // Exponential law 
  case 2:      
    // p=0 
    if (p==0) {
      *Rp=0;
      *dRdp = (mat->hardexp)*(mat->hardmodulus1);
    }
    // p!= 0
    else {
      *Rp = (mat->hardmodulus1)*(1-exp(-(mat->hardexp)*p));
      *dRdp = (mat->hardexp)*(mat->hardmodulus1)*exp(-(mat->hardexp)*p);
    }
    break;
    
    
    
  // Mixed power and exponential law 
  case 3:
    // p=0
    if (p==0.) {
      *Rp = 0.;
      *dRdp = (mat->hardmodulus1) + (mat->hardmodulus2)*(mat->hardexp);
    }
    // p!= 0
    else {
      *Rp = (mat->hardmodulus1)*p + (mat->hardmodulus2)*(1-exp(-(mat->hardexp)*p));
      *dRdp = (mat->hardmodulus1) +  (mat->hardexp)*(mat->hardmodulus2)*exp(-(mat->hardexp)*p) ;
    }
    break;
    
       
    
  default :
    printf("parameter chhard invalid in function VEVPMFHSPACE::compute_R\n");
    exit(0);
  
  // End switch
  }             


}






//Description: EVALUATION OF THE VISCOPLASTIC FUNCTION (GENERAL FORM)
//Author: Mohamed Haddad 
//Created: 06/04/2020
//Copyright: See COPYING file that comes with this distribution 
//
//    INPUT : strseq : Von Mises equivalent stress
//            p : Accumulated plasticity
//            mat : material properties and choice of the viscoplastic function
//    OUTPUT : gv : Viscoplastic function evaluated at strseq and p
//             dgvdstrseq : Derivative of gv w.r.t. strseq
//             dgvdp : Derivative of gv w.r.t. p */

void VEVPMFHSPACE::computevisc (double strseq, double p, const VEVPMFHSPACE::MATPROP *mat, double *gv, double *dgvdstrseq, double *dgvdp){
  
  /** Dec **/
  double Rp, dRdp, f;

  /**** Start ****/
  VEVPMFHSPACE::compute_R(p, mat, &Rp, &dRdp);
  f = strseq - (mat->yldstrs) - Rp;


  
  switch (mat->viscmodel) {
  // Norton's power law 
  case 1:     
    if ((mat->visccoef<0)||(mat->viscexp1<1)) {
      printf("Invalid value of the creep coefficient (must be > 0) or creep exponent1 (must be > 1 for continuous derivatives - and also to be physical (Lemaitre/Chaboche p257)) in the Norton's viscous function\n");
      /*exit(0);*/
    }

    if (f <= 0) {
      *gv = 0;
      *dgvdstrseq = 0;
      *dgvdp = 0;
    }

    else {
	*gv = (mat->visccoef)*pow((f/mat->yldstrs),mat->viscexp1);
	*dgvdstrseq = (mat->viscexp1/f)* (*gv);
        *dgvdp = (mat->viscexp1/f)*(-dRdp) * (*gv);
    }
    break;


  // 3 parameters creep function (power_law3 in Digimat and defined in ABAQUS)
  case 2:      
    if ((mat->visccoef<=0)||(mat->viscexp1<=0)) {
      printf("Invalid value of the creep coefficient (must be > 0) or creep exponent1 (must be > 0) in the 3 parameters creep function\n");
      exit(0);
    }
    if (f <= 0) {
      *gv = 0;
      *dgvdstrseq = 0;  
      *dgvdp = 1e12;  /* Infty */
    }
    else {
      if (p==0)
       p=1e-12;   /* Why p==0 && f>0 ???? */
      *gv = pow((mat->visccoef)*pow(strseq, mat->viscexp1), 1/(mat->viscexp2+1)) * pow((mat->viscexp2+1)*p, (mat->viscexp2)/(mat->viscexp2+1));
      *dgvdstrseq = (mat->viscexp1 * mat->visccoef * pow(strseq, mat->viscexp1 - 1) / (mat->viscexp2+1)) * pow(mat->visccoef*pow(strseq, mat->viscexp1), -mat->viscexp2/(mat->viscexp2 + 1)) * pow((mat->viscexp2+1)*p, (mat->viscexp2)/(mat->viscexp2+1));
      *dgvdp = mat->viscexp2 * pow((mat->visccoef * pow(strseq, mat->viscexp1))/((mat->viscexp2 + 1)*p), 1/(mat->viscexp2 + 1));
    }
    break;
    
    

  // 2 parameters creep function (power_law2 in Digimat and defined in J. Li and G.J. Weng(1998), CST, 58/11, 1803-1810 ****/
  case 3:     
    if ((mat->visccoef<=0)||(mat->viscexp1<=0)) {   
      printf("Invalid value of the creep coefficient (must be > 0) or creep exponent1 (must be > 0) in the 2 parameters creep function\n");
      exit(0);
    }
    if ((f <= 0)) {  /* Viscoplastic creep even if f<0 (f<0 once p becomes large */
      *gv = 0;     /* Should be visccoef !!! Bad model ???? */
      *dgvdstrseq = 1e-8;   
      *dgvdp = 1e-8;
    }
    else {
      *gv = mat->visccoef * pow(strseq/(mat->yldstrs + Rp), mat->viscexp1);
      *dgvdstrseq = (mat->viscexp1 * mat->visccoef * pow(strseq, mat->viscexp1-1))/pow(mat->yldstrs + Rp, mat->viscexp1);
      *dgvdp = -(mat->viscexp1 * mat->visccoef * pow(strseq, mat->viscexp1)) / pow(mat->yldstrs + Rp, mat->viscexp1+1) * dRdp;
    }
    break;



  // From Abaqus *RATE DEPENDENT, TYPE=POWERLAW  ******/
  case 4:
    if (f<=0) {
      *gv = 0;
      *dgvdstrseq = 0;
      *dgvdp = 0;
    }
    else {
      *gv = mat->visccoef*pow(f/(mat->yldstrs + Rp),mat->viscexp1);
      *dgvdstrseq = (mat->viscexp1/f)*(*gv);
      *dgvdp = (mat->viscexp1)*((-dRdp)/(mat->yldstrs+Rp))*(strseq/f)* (*gv);
    }
    break;



  default :
    printf("parameter choice_viscoplastic invalid in function VEVPMFHSPACE::computevisc in fonctions_affine.c\n");
    exit(0);
    
  //End of switch  
  }          

}





//Description: Constructor of a state structure
//Author: Mohamed Haddad 
//Created: 07/04/2020
//Copyright: See COPYING file that comes with this distribution 

void VEVPMFHSPACE::mallocstate(VEVPMFHSPACE::STATE *st) {

  /** Dec **/
  int i, j;
  
  /** Alloc. & Init. **/ 
  st->strn = (double*)calloc(6,sizeof(double));
  st->strs = (double*)calloc(6,sizeof(double));
  st->pstrn = (double*)calloc(6,sizeof(double));
  st->p = 0;
  
  /**** Start ****/
  VEVPMFHSPACE::mallocmatrix(&st->Si, VEVPMFHSPACE::max_nbrGi, 6);
  VEVPMFHSPACE::mallocmatrix(&st->Sigma_Hj, VEVPMFHSPACE::max_nbrKj, 6);
  
}
  






//Description: Destructor of a state structure 
//Author: Mohamed Haddad 
//Created: 07/04/2020
//Copyright: See COPYING file that comes with this distribution 

void VEVPMFHSPACE::freestate(VEVPMFHSPACE::STATE st) {

  /** Dec **/
  int i;
  
  /**** Start ****/
  free(st.strn);
  free(st.strs);
  free(st.pstrn);
  VEVPMFHSPACE::freematrix(st.Si,VEVPMFHSPACE::max_nbrGi);
  VEVPMFHSPACE::freematrix(st.Sigma_Hj,VEVPMFHSPACE::max_nbrKj);

}






//Description: Copy a state structure A into a B one
//Author: Mohamed Haddad 
//Created: 07/04/2020
//Copyright: See COPYING file that comes with this distribution 

void VEVPMFHSPACE::copystate(const VEVPMFHSPACE::STATE *A, VEVPMFHSPACE::STATE *B) {

  /** Dec **/
  int i,j;
  B->p = A->p;

  /**** Start ****/
  for (i=0; i<6; i++) {
    B->strn[i] = A->strn[i];
    B->strs[i] = A->strs[i];
    B->pstrn[i] = A->pstrn[i];
  }


   for (i=0; i<VEVPMFHSPACE::max_nbrGi; i++) {
    for (j=0; j<6; j++) {
      B->Si[i][j] = A->Si[i][j];
     }
    }
  

  for (i=0; i<VEVPMFHSPACE::max_nbrKj; i++) {
    for (j=0; j<6; j++) {
      B->Sigma_Hj[i][j] = A->Sigma_Hj[i][j];
     }
   }

}






//Description: Print contents of a state structure to the file pfile
//Author: Mohamed Haddad 
//Created: 07/04/2020
//Copyright: See COPYING file that comes with this distribution 

void VEVPMFHSPACE::fprintstate(FILE *pfile, VEVPMFHSPACE::STATE *A) {

  /** Dec **/
  int i;
  double temp;
  
  /** Init. **/
  temp = A->p;

  /**** Start ****/
  fprintf(pfile, "Strain:");
  VEVPMFHSPACE::fprintvect(pfile,A->strn,6);
  fprintf(pfile, "Stress:");
  VEVPMFHSPACE::fprintvect(pfile,A->strs,6);
  fprintf(pfile, "Plastic strain:");
  VEVPMFHSPACE::fprintvect(pfile, A->pstrn, 6);
  fprintf(pfile, "p :%f \n",temp);
  fprintf(pfile, "\n");
  
}



//Description: Print contents of a state structure to the screen
//Author: Mohamed Haddad 
//Created: 07/04/2020
//Copyright: See COPYING file that comes with this distribution 

void VEVPMFHSPACE::printstate(VEVPMFHSPACE::STATE *A) {

  /** Dec **/
  int i;
  double temp;

  /** Init. **/
  temp = A->p;
  
  /**** Start ****/
  printf("\nStrain:");
  VEVPMFHSPACE::printvect(A->strn,6);
  printf("Stress:");
  VEVPMFHSPACE::printvect(A->strs,6);
  printf("Plastic strain:");
  VEVPMFHSPACE::printvect(A->pstrn, 6);

  printf("p : %g \n ",temp);

  printf("Si:"); 
  printmatr(A->Si,VEVPMFHSPACE::max_nbrGi,6);

  printf("sigma_Hj:"); 
  VEVPMFHSPACE::printmatr(A->Sigma_Hj,VEVPMFHSPACE::max_nbrKj,6);

  printf("\n");

}



//Description: Print results of a the matrix phase in output file
//Author: Mohamed Haddad 
//Created: September 2022
//Copyright: See COPYING file that comes with this distribution 

void fileprintresults(FILE *pfile, double time, VEVPMFHSPACE::STATE *st){

  fprintf(pfile, "%e\t%e\t%e\t%e\t%e\t%e\t%e\t%e\t%e\t%e\t%e\t%e\t%e\t%e\t%e\t%e\t%e\t%e\t%e\t%e\t%e\t%e \n", time, st->strn[0], st->strn[1], st->strn[2],\
  st->strs[0], st->strs[1], st->strs[2],sqrt(2)*st->strn[3],sqrt(2)*st->strn[4], sqrt(2)*st->strn[5],\
  st->strs[3]/sqrt(2), st->strs[4]/sqrt(2),st->strs[5]/sqrt(2), st->p, VEVPMFHSPACE::vonmises(st->strs), VEVPMFHSPACE::eqstrn(st->strn),\
  st->pstrn[0], st->pstrn[1], st->pstrn[2], sqrt(2)*st->pstrn[3], sqrt(2)*st->pstrn[4], sqrt(2)*st->pstrn[5]); 
  
}



//Description: Print contents of a MATPROP structure in a file
//Author: Mohamed Haddad 
//Created: 08/04/2020
//Copyright: See COPYING file that comes with this distribution 

void VEVPMFHSPACE::fprintmatprop(FILE *pfile, VEVPMFHSPACE::MATPROP *A) {

  /** Dec **/
  int i;
  
  /**** Start ****/
  fprintf(pfile, "\n E(t=0): %e\n",A->E0);
  fprintf(pfile, "nu: %g\n",A->nu);
  fprintf(pfile, "yldstrs: %e\n",A->yldstrs);
  fprintf(pfile, "hardmodel: %d\n",A->hardmodel);
  fprintf(pfile, "hardmodulus1: %e\n",A->hardmodulus1);
  fprintf(pfile, "hardmodulus1: %e\n",A->hardmodulus2);
  fprintf(pfile, "hardexp: %g\n",A->hardexp);
  fprintf(pfile, "viscmodel: %d\n",A->viscmodel);
  fprintf(pfile, "visccoef: %e\n",A->visccoef);
  fprintf(pfile, "viscexp1: %g\n",A->viscexp1);
  fprintf(pfile, "viscexp2: %g\n",A->viscexp2);
  fprintf(pfile, "v: %g\n",A->v);
  fprintf(pfile, "ar: %g\n",A->ar);
  fprintf(pfile, "orientation: %g %g %g\n", A->orient[0], A->orient[1], A->orient[2]);

  fprintf(pfile, "G0: %e\n",A->G0);
  fprintf(pfile, "G_inf: %e\n",A->G_inf);
  fprintf(pfile, "number of shear relaxation times: %d\n",A->nbrGi);
  fprintf(pfile, "Gi:");
  for(i=0;i<A->nbrGi;i++){
    fprintf(pfile, "%e\t",A->Gi[i]);
    fprintf(pfile, "\n");
  }

  fprintf(pfile, "gi:");
  for(i=0;i<A->nbrGi;i++){
    fprintf(pfile, "%e\t",A->gi[i]);
    fprintf(pfile, "\n");
  }

  fprintf(pfile, "K0: %e\n",A->K0);
  fprintf(pfile, "K_inf: %e\n",A->K_inf);
  fprintf(pfile, "number of bulk relaxation times: %d\n",A->nbrKj);
  fprintf(pfile, "Kj:");
  for(i=0;i<A->nbrKj;i++){
    fprintf(pfile, "%e\t",A->Kj[i]);
    fprintf(pfile, "\n");
  }

  fprintf(pfile, "kj:");
  for(i=0;i<A->nbrKj;i++){
    fprintf(pfile, "%e\t",A->kaj[i]);
    fprintf(pfile, "\n");
  }
  
}



//Description: Print contents of a MATPROP structure on the screen
//Author: Mohamed Haddad 
//Created: 08/04/2020
//Copyright: See COPYING file that comes with this distribution 

void VEVPMFHSPACE::printmatprop(VEVPMFHSPACE::MATPROP *A) {

  /**** Start ****/
  printf("\n E(t=0): %g\n",A->E0);
  printf("nu: %g\n",A->nu);
  printf("yldstrs: %g\n",A->yldstrs);
  printf("hardmodel: %d\n",A->hardmodel);
  printf("hardmodulus1: %g\n",A->hardmodulus1);
  printf("hardmodulus2: %g\n",A->hardmodulus2);
  printf("hardexp: %g\n",A->hardexp);
  printf("viscmodel: %d\n",A->viscmodel);
  printf("visccoef: %g\n",A->visccoef);
  printf("viscexp1: %g\n",A->viscexp1);
  printf("viscexp2: %g\n",A->viscexp2);
  printf("v: %g\n",A->v);
  printf("ar: %g\n",A->ar);
  printf("orientation: %g %g %g\n", A->orient[0], A->orient[1], A->orient[2]);

  printf("G0: %e\n",A->G0);
  printf("G_inf: %e\n",A->G_inf);
  printf("number of Gi: %d\n",A->nbrGi);
  printf("Gi:");
  VEVPMFHSPACE::printvect(A->Gi, A->nbrGi);
  printf("gi:");
  VEVPMFHSPACE::printvect(A->gi, A->nbrGi);

  printf("K0: %e\n",A->K0);
  printf("K_inf: %e\n",A->K_inf);
  printf("number of Kj: %d\n",A->nbrKj);
  printf("Kj:");
  VEVPMFHSPACE::printvect(A->Kj, A->nbrKj);
  printf("kj:");
  VEVPMFHSPACE::printvect(A->kaj, A->nbrKj);

}






//Description: COMPUTE G_tilde(dt) (see B.Miled & I.Doghri, 2011, IJSS.)
//Author: Mohamed Haddad 
//Created: November 2020
//Copyright: See COPYING file that comes with this distribution 

double VEVPMFHSPACE::compute_Gtilde(const VEVPMFHSPACE::MATPROP *material_prop, double dt){

  /** Dec. & Init. **/ 
  int i;
  double result=material_prop->G_inf;

  /**** Start ****/
  for(i=0;i<material_prop->nbrGi;i++){
    result += material_prop->Gi[i] * (1 - exp(-dt / (material_prop->gi[i])) ) * (material_prop->gi[i] / dt);
  }

  return(result);
  
}



//Description: COMPUTE K_tilde(dt) (see B.Miled & I.Doghri, 2011, IJSS.)
//Author: Mohamed Haddad 
//Created: November 2020
//Copyright: See COPYING file that comes with this distribution 

double VEVPMFHSPACE::compute_Ktilde(const VEVPMFHSPACE::MATPROP *material_prop, double dt){

  int i;
  double result=material_prop->K_inf;

  for(i=0;i<material_prop->nbrKj;i++){
    /*printf("part %d = %g \n", i, material_prop->Kj[i] * (1 - exp(-dt / (material_prop->kaj[i])) ) * (dt / material_prop->kaj[i]));*/
    result += material_prop->Kj[i] * (1 - exp(-dt / (material_prop->kaj[i])) ) * (material_prop->kaj[i] / dt);
  }

  return(result);
}



//Description: COMPUTE E_tilde(dt) (see B.Miled & I.Doghri, 2011, IJSS.)
//Author: Mohamed Haddad 
//Created: November 2020
//Copyright: See COPYING file that comes with this distribution 

void VEVPMFHSPACE::compute_Etilde(const VEVPMFHSPACE::MATPROP *material_prop, double dt, double **E_tilde){
  
  /** Dec. **/
  double Gtilde, Ktilde;
  double **Idev, **Ivol;

  /** Alloc. & Init. **/
  VEVPMFHSPACE::mallocmatrix(&Idev,6,6);
  VEVPMFHSPACE::mallocmatrix(&Ivol,6,6);
  
  /**** Start ****/
  Gtilde = compute_Gtilde(material_prop,dt);
  Ktilde = compute_Ktilde(material_prop,dt);

  VEVPMFHSPACE::compute_Idev(Idev);
  VEVPMFHSPACE::compute_Ivol(Ivol);

  VEVPMFHSPACE::addtens4(2*Gtilde,Idev,3*Ktilde,Ivol,E_tilde);
  
  /** Freeing **/
  VEVPMFHSPACE::freematrix(Idev,6);
  VEVPMFHSPACE::freematrix(Ivol,6); 

}



//Description: COMPUTE E_inf(dt) (see B.Miled & I.Doghri, 2011, IJSS.)
//Author: Mohamed Haddad 
//Created: November 2020
//Copyright: See COPYING file that comes with this distribution 

void VEVPMFHSPACE::compute_Einf(const VEVPMFHSPACE::MATPROP *material_prop, double **E_inf){

  double **Idev, **Ivol;

  VEVPMFHSPACE::mallocmatrix(&Idev,6,6);
  VEVPMFHSPACE::mallocmatrix(&Ivol,6,6);

  VEVPMFHSPACE::compute_Idev(Idev);
  VEVPMFHSPACE::compute_Ivol(Ivol);


  VEVPMFHSPACE::addtens4(2*(material_prop->G_inf),Idev,3*(material_prop->K_inf),Ivol,E_inf);

  VEVPMFHSPACE::freematrix(Idev,6);
  VEVPMFHSPACE::freematrix(Ivol,6); 

}




//Description: DECOMPOSITION OF VE STRAIN TO VOL AND DEV PARTS (see B.Miled & I.Doghri, 2011, IJSS.)
//Author: Mohamed Haddad 
//Created: November 2020
//Copyright: See COPYING file that comes with this distribution 

void VEVPMFHSPACE::decomposition_dstrn_ve(double *dstrn_ve, double *dstrn_ve_volpart, double *dstrn_ve_devpart){

  /** Dec. **/
  double **Idev, **Ivol;

  /** Alloc. and Init. **/ 
  VEVPMFHSPACE::mallocmatrix(&Idev,6,6);
  VEVPMFHSPACE::mallocmatrix(&Ivol,6,6);

  /**** Start ****/
  VEVPMFHSPACE::compute_Ivol(Ivol);
  VEVPMFHSPACE::compute_Idev(Idev);
 
  VEVPMFHSPACE::contraction42(Ivol, dstrn_ve, dstrn_ve_volpart);
  VEVPMFHSPACE::contraction42(Idev, dstrn_ve, dstrn_ve_devpart);

  /** Freeing **/
  VEVPMFHSPACE::freematrix(Idev,6);
  VEVPMFHSPACE::freematrix(Ivol,6); 

}






//Description: COMPUTE Si(tn+1) (see B.Miled & I.Doghri, 2011, IJSS.)
//Author: Mohamed Haddad 
//Created: November 2020
//Copyright: See COPYING file that comes with this distribution 

void VEVPMFHSPACE::compute_Si(const VEVPMFHSPACE::MATPROP *material_prop, const VEVPMFHSPACE::STATE *tn, double dt, double *dstrn_ve, double **Si){

   /** Dec. **/
   int i,j;
   double *dstrn_ve_vol, *dstrn_ve_dev;

   /** Alloc. **/
   dstrn_ve_vol= (double*) calloc(6,sizeof(double));
   dstrn_ve_dev= (double*) calloc(6,sizeof(double));

   /**** Start ****/
   // comute dev and vol parts of the strain 
   VEVPMFHSPACE::decomposition_dstrn_ve(dstrn_ve,dstrn_ve_vol,dstrn_ve_dev);

   // Si_n+1 = exp(-dt/gi)*Si_n + (1 - exp(-dt/gi)) * (gi/dt) * (dstrn_ve)_dev 
   for(i=0;i<material_prop->nbrGi;i++){
      for(j=0;j<6;j++){
        Si[i][j] = ( exp(-dt / material_prop->gi[i]) * tn->Si[i][j] ) + ( 2 * material_prop->Gi[i] * (1 - exp(-dt / (material_prop->gi[i])) ) * (material_prop->gi[i] / dt) * dstrn_ve_dev[j]);
      }

   }

  /** Freeing **/
  free(dstrn_ve_vol);
  free(dstrn_ve_dev); 
   
}






//Description: COMPUTE Sigma_Hj(tn+1) (see B.Miled & I.Doghri, 2011, IJSS.)
//Author: Mohamed Haddad 
//Created: November 2020
//Copyright: See COPYING file that comes with this distribution 

void VEVPMFHSPACE::compute_SigmaHj(const VEVPMFHSPACE::MATPROP *material_prop, const VEVPMFHSPACE::STATE *tn, double dt, double *dstrn_ve, double **Sigma_Hj){

   /** Dec. **/
   int i,j;
   double *dstrn_ve_vol, *dstrn_ve_dev;

   /** Alloc. **/
   dstrn_ve_vol= (double*) calloc(6,sizeof(double));
   dstrn_ve_dev= (double*) calloc(6,sizeof(double));

   /**** Start ****/
   VEVPMFHSPACE::decomposition_dstrn_ve(dstrn_ve,dstrn_ve_vol,dstrn_ve_dev);

   for(i=0;i<material_prop->nbrKj;i++){
      for(j=0;j<6;j++){
         Sigma_Hj[i][j] = ( exp(-dt / (material_prop->kaj[i])) * tn->Sigma_Hj[i][j] ) + ( 3 * material_prop->Kj[i] * (1 - exp(-dt / (material_prop->kaj[i]))) * (material_prop->kaj[i] / dt) * dstrn_ve_vol[j]);
      }
   }

  /** Freeing **/
  free(dstrn_ve_vol);
  free(dstrn_ve_dev);
   
}







//Description: COMPUTE sum of Si(tn)*exp(-dt/gi) (see B.Miled & I.Doghri, 2011, IJSS.)
//Author: Mohamed Haddad 
//Created: November 2020
//Copyright: See COPYING file that comes with this distribution 

void VEVPMFHSPACE::compute_sum_Siexp(const VEVPMFHSPACE::MATPROP *material_prop, double dt, double **Si, double *sum_Siexp){
  
  /** Dec. **/
  int i,j;

  /** Init. **/
  for(j=0;j<6;j++){sum_Siexp[j] = 0;}

  
  /**** Start ****/
  for(j=0;j<6;j++){
    for(i=0;i<material_prop->nbrGi;i++){
       sum_Siexp[j] = sum_Siexp[j] + (Si[i][j] * exp( -dt / (material_prop->gi[i]))) ;
    }
  }


}






//Description: COMPUTE sum of SigmaHj(tn)*exp(-dt/kj) (see B.Miled & I.Doghri, 2011, IJSS.)
//Author: Mohamed Haddad 
//Created: November 2020
//Copyright: See COPYING file that comes with this distribution 

void VEVPMFHSPACE::compute_sum_SigmaHjexp(const VEVPMFHSPACE::MATPROP *material_prop, double dt, double **Sigma_Hj, double *sum_SigmaHjexp){

  /** Dec. **/
  int i,j;

  /** Init. **/
  for(j=0;j<6;j++){sum_SigmaHjexp[j] = 0;}

  /**** Starting ****/
  for(j=0;j<6;j++){
     for(i=0;i<material_prop->nbrKj;i++){
       sum_SigmaHjexp[j] = sum_SigmaHjexp[j]  + (Sigma_Hj[i][j] * exp( -dt / (material_prop->kaj[i]))) ;
     }
  }

}



//Description: Constitutive box for VE-VP homogeneous isotropic phase (see B.Miled & I.Doghri, 2011, IJSS.)
//Author: Mohamed Haddad 
//Created: November 2020
//Copyright: See COPYING file that comes with this distribution 
//	** INPUT: 	material : material properties with a MATPROP struct
//         	 	  dt: time step
//			        STATE tn : State at time tn with a STATE struct
//		          dstrn: strain increment on the time step
//	** OUTPUT: 	STATE tn+1 : State at time tn with a STATE struct
//           		Update strs, p and pstrn 
           
int VEVPMFHSPACE::ConstitutiveBoxViscoElasticViscoPlasticSmallStrains(const VEVPMFHSPACE::MATPROP *material, const VEVPMFHSPACE::STATE *tn, double dt, double *dstrn, VEVPMFHSPACE::STATE *tau, double **Calgo){

  /**	Declaration	**/
  /***********************/
  double **E_tilde;
  double **E_inf;
  double *sum_Siexp_tn;
  double *sum_SigmaHjexp_tn;
  double *strntn_ve;
  double *part1; 
  double *part2; 
  double *strs_pred;
  double strseq_pred, f_pred = 0;
  double R, dRdp;
  double *S_pred;
  double *N_pred;
  double strseq_tau, ptau=0, G_tld;
  double *strstau;
  double *pstrntau; 
  double **tens4;
  double gv, dgvdstrseq, dgvdp;
  double kp, kstrs, res, tol = 1.e-8;
  int m, i;
  double cp;
  double *Stau;
  double trace_strs_pred;
  double dp, hv;
  double **NcrossN; 
  double **dNdstrs; 
  double **Idev;
  
  
  /**	Memory allocation	**/
  /******************************/
  VEVPMFHSPACE::mallocmatrix(&E_tilde, 6, 6); 
  VEVPMFHSPACE::mallocmatrix(&E_inf, 6, 6);
  strntn_ve=(double*)calloc(6,sizeof(double));
  sum_Siexp_tn=(double*)calloc(6,sizeof(double));
  sum_SigmaHjexp_tn=(double*)calloc(6,sizeof(double));
  part1 = (double*)calloc(6,sizeof(double));
  part2 = (double*)calloc(6,sizeof(double));
  strs_pred = (double*)calloc(6,sizeof(double));
  strstau = (double*)calloc(6,sizeof(double));
  Stau = (double*)calloc(6,sizeof(double));
  pstrntau = (double*)calloc(6,sizeof(double));
  S_pred = (double*)calloc(6,sizeof(double));
  N_pred = (double*)calloc(6,sizeof(double));
  VEVPMFHSPACE::mallocmatrix(&tens4, 6, 6); 
  VEVPMFHSPACE::mallocmatrix(&NcrossN, 6, 6); 
  VEVPMFHSPACE::mallocmatrix(&dNdstrs, 6, 6); 
  VEVPMFHSPACE::mallocmatrix(&Idev, 6, 6); 
   
   
  /**	Begin code	**/
  /***********************/
  VEVPMFHSPACE::compute_Etilde(material,dt,E_tilde);
  VEVPMFHSPACE::compute_Einf(material, E_inf);
  VEVPMFHSPACE::compute_sum_Siexp(material, dt, tn->Si, sum_Siexp_tn);
  VEVPMFHSPACE::compute_sum_SigmaHjexp(material, dt, tn->Sigma_Hj, sum_SigmaHjexp_tn);
  
  VEVPMFHSPACE::addtens2(1, tn->strn, -1, tn->pstrn, strntn_ve);
  VEVPMFHSPACE::contraction42(E_inf, strntn_ve,part1);
  VEVPMFHSPACE::contraction42(E_tilde, dstrn, part2);
  
  VEVPMFHSPACE::addtens2(1, part1, 1, part2, part1);
  VEVPMFHSPACE::addtens2(1, part1, 1, sum_Siexp_tn, part1);
  VEVPMFHSPACE::addtens2(1, part1, 1, sum_SigmaHjexp_tn, strs_pred);
  
  strseq_pred = VEVPMFHSPACE::vonmises(strs_pred);
  VEVPMFHSPACE::compute_R (tn->p, material, &R, &dRdp);
  /** yield function  **/
  f_pred = strseq_pred - R - (material->yldstrs);
  
  
  /** VE Predictor **/
  /******************/
  if (f_pred <= 0){
    VEVPMFHSPACE::copyvect(strs_pred, tau->strs, 6);
    tau->p = tn->p;
    VEVPMFHSPACE::copyvect(tn->pstrn, tau->pstrn, 6);
    VEVPMFHSPACE::addtens4(1, E_tilde, 0, E_tilde, Calgo);  
  }
  
  /** VP Corrector  **/
  /*******************/
  else{
  
    /* compute N_pred */
     VEVPMFHSPACE::dev(strs_pred, S_pred);
     VEVPMFHSPACE::addtens2((3/(2*strseq_pred)), S_pred, 0, S_pred, N_pred);


    /*Initial values of p and equivelent stress*/
    ptau=tn->p;
    strseq_tau = strseq_pred;

    /*compute G_tilde (dt)*/
    G_tld = VEVPMFHSPACE::compute_Gtilde(material,dt);

    VEVPMFHSPACE::computevisc (strseq_tau, ptau, material, &gv, &dgvdstrseq, &dgvdp);
  
  
    /*Initial values of kp and k_sigma*/
    kp = ptau - (tn->p) - (gv * dt);
    kstrs = strseq_tau + (3 * G_tld * (ptau-(tn->p))) - strseq_pred;

    res = VEVPMFHSPACE::max(fabs(kp),fabs(kstrs/(material->E0)));



    for (m=1; res > tol; m++) { 
    
      /* Compute correction on p = cp */
      /*cp =  - (kp + dgvdstrseq*dt*kstrs)/(1+(3*G_tld+dRdp)*dgvdstrseq*dt);*/
      cp =  - (kp + dgvdstrseq*dt*kstrs)/(1-(dgvdp*dt)+ (3*G_tld*dgvdstrseq*dt));


      /*update accumulated plastic strain p and eq_stress */
      ptau += cp;
      strseq_tau += -kstrs-(3 * G_tld * cp);

      VEVPMFHSPACE::compute_R (ptau, material, &R, &dRdp);
      VEVPMFHSPACE::computevisc (strseq_tau, ptau, material, &gv, &dgvdstrseq, &dgvdp);
    

      /* update kp and k_strs for the next iteration if res still > tol*/
      kp = ptau - (tn->p) - (gv * dt);
      kstrs = strseq_tau + (3 * G_tld * (ptau-(tn->p))) - strseq_pred;
      res = VEVPMFHSPACE::max(fabs(kp),fabs(kstrs/(material->E0)));


      if (m>100) {
        /*printf("\nWARNING: Problems of convergence in constbox_VEVP !\n");
        printf("dstrn[0]=%g; ptn:%g; strstn[0]: %g; dt:%g; G_tld:%g; strseqtr:%g\n", dstrn[0], tn->p, (tn->strs)[0], dt, G_tld, strseq_tr); 
        printf("p:%g; dRdp:%g; gv:%g; dgvdstrseq:%g; dgvdp:%g\n", *p, dRdp, gv, dgvdstrseq, dgvdp);
        printf("ftr: %g; strseq:%g; material->yldstrs: %g; R: %g; f: %g\n", ftr, strseq, material->yldstrs, R, strseq - material->yldstrs-R); */
        printf("fabs(kp):%g;  fabs(kstrs/(material->yldstrs))=%g; res:%g; tol:%g\n", fabs(kp),fabs(kstrs/(material->yldstrs)), res, tol); 
        return (0);
      }    
   }
  
 

    VEVPMFHSPACE::addtens2((2.0*strseq_tau)/3.0, N_pred, 0, N_pred, Stau);
    trace_strs_pred = VEVPMFHSPACE::trace(strs_pred);     
   
   
    /* Update stress*/
    for (i=0;i<3;i++) {
      (tau->strs)[i] = Stau[i] + (1.0/3.0)*trace_strs_pred;
      (tau->strs)[i+3] = Stau[i+3];
    }

    /* Update p and plastic strain*/
    tau->p = ptau;
    VEVPMFHSPACE::addtens2((ptau-(tn->p)), N_pred, 1, tn->pstrn, tau->pstrn);
    
    
    /* Update Calgo **/
    VEVPMFHSPACE::computevisc (strseq_tau, ptau, material, &gv, &dgvdstrseq, &dgvdp);
    dp = ptau - (tn->p);
    
    hv = (1 / (dt * dgvdstrseq)) + (3*G_tld) - (dgvdp/dgvdstrseq);
    VEVPMFHSPACE::produit_tensoriel(N_pred, N_pred, NcrossN);
    VEVPMFHSPACE::compute_Idev(Idev);
    VEVPMFHSPACE::addtens4((3/(2*strseq_tau)), Idev, (-1/strseq_tau), NcrossN, dNdstrs);  
    VEVPMFHSPACE::addtens4(1, E_tilde, -(pow((2*G_tld),2)/hv), NcrossN, tens4);
    VEVPMFHSPACE::addtens4(1, tens4, -(pow((2*G_tld),2)*((strseq_tau*dp)/(strseq_tau + (3*G_tld*dp)))), dNdstrs, Calgo);
  }
  

  /** Freeing **/ 
  VEVPMFHSPACE::freematrix(E_tilde,6); 
  VEVPMFHSPACE::freematrix(E_inf,6);
  free(strntn_ve);
  free(sum_Siexp_tn); 
  free(sum_SigmaHjexp_tn);
  free(part1); 
  free(part2);
  free(strs_pred); 
  free(strstau); 
  free(pstrntau);
  free(S_pred); 
  free(N_pred);
  free(Stau);
  VEVPMFHSPACE::freematrix(tens4,6); 
  VEVPMFHSPACE::freematrix(NcrossN,6);  
  VEVPMFHSPACE::freematrix(dNdstrs,6); 
  VEVPMFHSPACE::freematrix(Idev,6);
   
  return(1) ;

}
/** END **/
/**********/


  
