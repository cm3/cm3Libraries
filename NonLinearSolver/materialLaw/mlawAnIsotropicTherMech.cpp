//

//
// C++ Interface: material law
//
// Description: AnIsotropicTherMech law
//
//
// Author:  <L. Homsy>, (C) 2016//
// Copyright: See COPYING file that comes with this distribution
//
//
//#include "mlawLinearElecTherMech.h"
#include <math.h>
#include "MInterfaceElement.h"
#include "mlawAnIsotropicTherMech.h"
#include "mlawTransverseIsotropic.h"


mlawAnIsotropicTherMech::mlawAnIsotropicTherMech(const int num,const double E, const double nu,const double rho,const double EA, const double GA, const double nu_minor,
                           const double Ax, const double Ay, const double Az, const double alpha_rot, const double beta_rot, const double gamma_rot,const double t0
                           ,const double kx,const double ky, const double kz,const double cp,const double alphath): mlawTransverseIsotropic(num,E,nu,rho,EA,GA,nu_minor,Ax,Ay,Az), 
                           _t0(t0),_kx(kx),_ky(ky),_kz(kz),_cp(cp),_alphath(alphath)

{

	double co1,co2,co3,si1,si2,si3;
	double s1c2, c1c2;
	double _pi(3.14159265359);
    	double fpi = _pi/180.;

	co1 = cos(alpha_rot*fpi);
	si1 = sin(alpha_rot*fpi);

	co2 = cos(beta_rot*fpi);
	si2 = sin(beta_rot*fpi);

	co3 = cos(gamma_rot*fpi);
	si3 = sin(gamma_rot*fpi);

	s1c2 = si1*co2;
	c1c2 = co1*co2;

	_rotationMatrix(0,0) = co3*co1 - s1c2*si3;
	_rotationMatrix(0,1) = co3*si1 + c1c2*si3;
	_rotationMatrix(0,2) = si2*si3;

	_rotationMatrix(1,0) = -si3*co1 - s1c2*co3;
	_rotationMatrix(1,1) = -si3*si1 + c1c2*co3;
	_rotationMatrix(1,2) = si2*co3;

	_rotationMatrix(2,0) = si1*si2;
	_rotationMatrix(2,1) = -co1*si2;
	_rotationMatrix(2,2) = co2;


        STensor3 k(0.);
	// to be unifom in DG3D q= k' gradT with k'=-kwant instead of q=-kgradT
	k(0,0)  = -_kx;
	k(1,1)  = -_ky;
	k(2,2)  = -_kz;
  	for(int i=0;i<3;i++)
  	 {
    	  for(int j=0;j<3;j++)
    	   {
             _k0(i,j)=0.;
             for(int m=0;m<3;m++)
             {
               for(int n=0;n<3;n++)
            	{
              	  _k0(i,j)+=_rotationMatrix(m,i)*_rotationMatrix(n,j)*k(m,n);
       	        }
     	      }
   	    }
	  }
        for(int i=0;i<3;i++)
        {
          for(int j=0;j<3;j++)
          {
	   _Stiff_alphaDilatation(i,j)=0.;
           if(i==j) _Stiff_alphaDilatation(i,j)=3.*_K *_alphath;
          }
        }
}

mlawAnIsotropicTherMech::mlawAnIsotropicTherMech(const mlawAnIsotropicTherMech &source) : mlawTransverseIsotropic(source),
						      _kx(source._kx),_ky(source._ky),_kz(source._kz),_t0(source. _t0),
						      _cp(source._cp),_alphath(source._alphath), _k0(source._k0), 
                                                      _rotationMatrix(source._rotationMatrix), _Stiff_alphaDilatation(source._Stiff_alphaDilatation)


{

}


mlawAnIsotropicTherMech& mlawAnIsotropicTherMech::operator=(const materialLaw &source)  
{
  mlawTransverseIsotropic::operator=(source);
  const mlawAnIsotropicTherMech* src =dynamic_cast<const mlawAnIsotropicTherMech*>(&source);
  if(src !=NULL)
  {
   _kx = src->_kx;
   _ky = src->_ky;
   _kz  = src->_kz;
   _t0 = src->_t0;
   _k0 =src->_k0;
   _cp =src->_cp;
   _rotationMatrix = src->_rotationMatrix;
   _Stiff_alphaDilatation=src->_Stiff_alphaDilatation;
   _alphath= src->_alphath;
  }
  return *this;
}



void mlawAnIsotropicTherMech::createIPState(IPStateBase* &ips, bool hasBodyForce, const bool* state_,const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
  bool inter=true;
  const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
  if(iele==NULL) inter=false;

  IPVariable* ipvi = new IPAnIsotropicTherMech();
  IPVariable* ipv1 = new IPAnIsotropicTherMech();
  IPVariable* ipv2 = new IPAnIsotropicTherMech();
  if(ips != NULL) delete ips;
  ips = new IP3State(state_,ipvi,ipv1,ipv2);
}





void mlawAnIsotropicTherMech::constitutive(
                            const STensor3& F0,         // initial deformation gradient (input @ time n)
                            const STensor3& Fn,         // updated deformation gradient (input @ time n+1)
                            STensor3 &P,                // updated 1st Piola-Kirchhoff stress tensor (output)
                            const IPAnIsotropicTherMech *q0,       // array of initial internal variable
                            IPAnIsotropicTherMech *q1,             // updated array of internal variable (in ipvcur on output),
                            STensor43 &Tangent,         // constitutive tangents (output)
                            double T0,			// initial Temperature
			    double T,			// updated Temperature			 
  			    const SVector3 &gradT,
                            SVector3 &fluxT,            // Thermal flux	// Electric current flux
                            STensor3 &dPdT,
                            STensor3 &dqdgradT,
                            SVector3 &dqdT,
                            STensor33 &dqdF,
			    double &w,			// Thermal source (ex. heat cpacity)
			    double &dwdt,
			    STensor3 &dwdf,
                            const bool stiff
                           )const
{
  mlawTransverseIsotropic::constitutive( F0, Fn,P,(const IPTransverseIsotropic *)q0, (IPTransverseIsotropic *)q1,Tangent,stiff) ;
  static SVector3 A;
  getTransverseDirection( A, q0);
  A.normalize();

  static STensor3 C;
  STensorOperation::zero(C);
  for(int I=0; I<3; I++)
    for(int J=0; J<3; J++)
      for(int k=0; k<3; k++)
        C(I,J)+=Fn(k,I)*Fn(k,J);

  static STensor3 Cinv;
  STensorOperation::inverseSTensor3(C,Cinv);
  static STensor3 Siso;
  Siso=Cinv;
  Siso*=-3.*_K*_alphath*(T-_t0);

  static SVector3 CA;
  STensorOperation::zero(CA);//double _alphaDilatationI=0.;
  for(int I=0; I<3; I++)
    for(int J=0; J<3; J++)
      CA(I) += C(I,J)*A(J);
  static STensor3 AA;
  tensprod(A,A,AA);

  static STensor3 Str;
  Str = AA;
  Str *= (-12.*_beta*_K/_lambda*_alphath*(T-_t0));

  for(int i=0; i<3; i++)
    for(int J=0; J<3; J++)
      for(int K=0; K<3; K++)
        P(i,J)+=Fn(i,K)*(Str(K,J)+Siso(K,J));

  q1->_elasticEnergy=deformationEnergy(C,A,T);
  static STensor43 Kalpha;
  STensorOperation::zero(Kalpha);
  
  /* tangents */
  if(stiff)
  {
    for(int i=0; i<3; i++){
      for(int J=0; J<3; J++){
        for(int k=0; k<3; k++){
          for(int L=0; L<3; L++){
             for(int M=0; M<3; M++){
              for(int N=0; N<3; N++){
                double val=3./2.*_alphath*(T-_t0)*(Cinv(M,L)*Cinv(J,N)+Cinv(M,N)*Cinv(J,L))*_K;
		Tangent(i,J,k,L) += val*Fn(i,M)*Fn(k,N);
	      }
              for(int d=0; d<3; d++){
                double val=3./2.*_alphath*(T-_t0)*(Cinv(M,d)*Cinv(J,L)+Cinv(M,L)*Cinv(J,d))*_K;
                Tangent(i,J,k,L) += val*Fn(i,M)*Fn(k,d);// just one was before
	      }
	    }
            if(i==k) Tangent(i,J,k,L) += (Siso(J,L)+Str(J,L));

          }
        }
      }
    }
    STensorOperation::zero(dPdT);
    
    for(int i=0; i<3; i++)
      for(int J=0; J<3; J++)
	for(int k=0; k<3; k++)
	{
	  dPdT(i,J)-=3.*Fn(i,k)*_alphath*Cinv(k,J)*_K;
	  dPdT(i,J)-=(12.*_beta*_K*_alphath/_lambda)*Fn(i,k)*A(k)*A(J);
	}
  }
  static STensor3 Fninv;
  STensorOperation::inverseSTensor3(Fn,Fninv);
  double Jac= Fn.determinant();

  static STensor3 dKdT;
  STensorOperation::zero(dKdT);

  STensorOperation::zero(dqdF); // dependency of flux with deformation gradient
  dwdt= 0.;
  STensorOperation::zero(dqdgradT);
  double dcpdt=0.;
  STensorOperation::zero(dwdf);
   //large defo
  static STensor3 _Kref0;
  double fz;

  STensorOperation::zero(_Kref0);
  for(int K = 0; K< 3; K++)
  {
    for(int L = 0; L< 3; L++)
    {
      for(int i = 0; i< 3; i++)
      {
        for(int j = 0; j< 3; j++)
        {
	  _Kref0(K,L)  += Fninv(K,i)*_k0(i,j)*Fninv(L,j);
        }
      }
    }
  }
  for(int i=0;i<3;i++)
  {
     fluxT(i)=0.;
     for(int j=0;j<3;j++)
     {
       fluxT(i)+=(_Kref0(i,j))*gradT(j)*Jac;
     }
  }
  double dh;
  dh = getTimeStep();
  if(dh>0.)
  {
    w =-_cp*(T-T0)/dh;
  }
  else
  {
    w = 0.;
  }
  q1->_thermalEnergy = _cp*(T-_t0);
  
  if(stiff)
  {
    for(int i=0;i<3;i++)
    {
      dqdT(i)=0.;
      for(int j=0;j<3;j++)
      {
        dqdT(i)+=dKdT(i,j)*gradT(j)*Jac;
      }
    }
    for(int i=0;i<3;i++)
    {
      for(int k=0;k<3;k++)
      {
        dqdgradT(i,k) =_Kref0(i,k)*Jac;
      }
    }
    STensorOperation::zero(dqdF);
    for(int K = 0; K< 3; K++)
    {
      for(int m = 0; m< 3; m++)
      {
        for(int N = 0; N< 3; N++)
        {
          for(int L = 0; L< 3; L++)
          {
            dqdF(K,m,N) -= Fninv(K,m)*_Kref0(N,L)*gradT(L)*Jac;
            dqdF(K,m,N) -= _Kref0(K,N)*Fninv(L,m)*gradT(L)*Jac;
            dqdF(K,m,N) += _Kref0(K,L)*gradT(L)*Fninv(N,m)*Jac;
           }
         }
       }
     }
    if(dh>0.)
    {
      dwdt =-dcpdt*(T-T0)/dh-_cp/dh;
    }
    else
    {
      dwdt= 0.;
    }
  }
}

double mlawAnIsotropicTherMech::deformationEnergy(const STensor3 &C, const SVector3 &A, double T) const
{
  double ene=mlawTransverseIsotropic::deformationEnergy(C, A);
  double Jac= STensorOperation::determinantSTensor3(C); 
  double lnJ = log(Jac)/2.;

  static SVector3 CA;
  STensorOperation::zero(CA);
  for(int I=0; I<3; I++)
    for(int J=0; J<3; J++)
      CA(I) += C(I,J)*A(J);

  double I4= dot(A,CA);
  static STensor3 CC;
  STensorOperation::multSTensor3(C,C,CC);
 
  static SVector3 CCA;
  STensorOperation::zero(CCA);
  
  for(int I=0; I<3; I++)
    for(int J=0; J<3; J++)
      CCA(I) += CC(I,J)*A(J);
  double I5= dot(A,CCA);
  double th=3*_alphath*_K/_lambda*(T-_t0);
  double PsyIso = -2.*lnJ*th+th*th;
  double PsyTr = -6. *_beta*_K/_lambda*_alphath*(T-_t0)*(I4-1.);
  return (PsyIso+PsyTr);

}


