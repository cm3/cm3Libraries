//
// C++ Interface: material law with hyperelastic potential
//
// Author:  <Van Dung Nguyen>, (C) 2023
//
// Copyright: See COPYING file that comes with this distribution
//
//

#include "mlawHyperelasticWithPotential.h"
#include "ipHyperelasticPotential.h"

mlawHyperelasticWithPotential::mlawHyperelasticWithPotential(const int num, const double rho, const elasticPotential& potential):
  materialLaw(num,true), _rho(rho)
{
    _elasticPotential = potential.clone();
};
  
mlawHyperelasticWithPotential::mlawHyperelasticWithPotential(const mlawHyperelasticWithPotential& src):
  materialLaw(src), _rho(src._rho)
{
  _elasticPotential = NULL;
  if (src._elasticPotential != NULL)
  {
    _elasticPotential = src._elasticPotential->clone();
  }
}

mlawHyperelasticWithPotential& mlawHyperelasticWithPotential::operator=(const materialLaw &source)
{
  materialLaw::operator =(source);
  _elasticPotential = NULL;
  const mlawHyperelasticWithPotential* psrc = dynamic_cast<const mlawHyperelasticWithPotential*>(&source);
  if (psrc != NULL)
  {
    if (psrc->_elasticPotential != NULL)
    {
      _elasticPotential = psrc->_elasticPotential->clone();
    }
  }
  return *this;
};

mlawHyperelasticWithPotential::~mlawHyperelasticWithPotential()
{
  if (_elasticPotential != NULL)
  {
    delete _elasticPotential;
    _elasticPotential = NULL;
  }
}

void mlawHyperelasticWithPotential::createIPState(IPStateBase* &ips,bool hasBodyForce, const bool* state_,const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
    IPVariable* ipvi = new IPHyperelasticPotential();
    IPVariable* ipv1 = new IPHyperelasticPotential();
    IPVariable* ipv2 = new IPHyperelasticPotential();
    if(ips != NULL) delete ips;
    ips = new IP3State(state_,ipvi,ipv1,ipv2);
}

double mlawHyperelasticWithPotential::soundSpeed() const
{
  double E = _elasticPotential->getYoungModulus();
	double nu = _elasticPotential->getPoissonRatio();

  double factornu = (1.-nu)/((1.+nu)*(1.-2.*nu));
  return sqrt(E*factornu/_rho);
};

double mlawHyperelasticWithPotential::scaleFactor() const{
	return _elasticPotential->getYoungModulus();
};

void mlawHyperelasticWithPotential::constitutive(const STensor3& F0,         // initial deformation gradient (input @ time n)
                              const STensor3& Fn,         // updated deformation gradient (input @ time n+1)
                              STensor3 &P,                // updated 1st Piola-Kirchhoff stress tensor (output)
                                                          // contains the initial values on input
                              const IPVariable *q0,       // array of initial internal variable
                              IPVariable *q1,              // updated array of internal variable (in ipvcur on output),
                              STensor43 &Tangent,         // constitutive tangents (output)
                              const bool stiff,
                              STensor43* elasticTangent,
                              const bool dTangent,
                              STensor63* dCalgdeps) const
{
	_elasticPotential->constitutive(Fn,P,stiff,&Tangent);
  IPHyperelasticPotential* q1Hyper = static_cast<IPHyperelasticPotential*>(q1);
  q1Hyper->getRefToDeforEnergy() = _elasticPotential->get(Fn);
};