//
// C++ Interface: failure criterions
//
// Author:  <Van Dung Nguyen>, (C) 2018
//
// Copyright: See COPYING file that comes with this distribution
//
//

#include "mlawHyperelasticFailureCriterion.h"
#include "ipHyperelastic.h"
#include "ipNonLocalDamageHyperelastic.h"

mlawHyperelasticFailureTrivialCriterion::mlawHyperelasticFailureTrivialCriterion(const int num): mlawHyperelasticFailureCriterionBase(num,true){};;

PowerFailureCriterion::PowerFailureCriterion(const int num, const RateFailureLaw& trac, const RateFailureLaw& comp, const double power): mlawHyperelasticFailureCriterionBase(num),
    _failurePower(power){
  _compFailure = comp.clone();
  _tracFailure = trac.clone();
};

PowerFailureCriterion::PowerFailureCriterion(const PowerFailureCriterion& src): mlawHyperelasticFailureCriterionBase(src),_failurePower(src._failurePower){
  _compFailure = NULL;
  if (src._compFailure){
    _compFailure= src._compFailure->clone();
  }
  _tracFailure = NULL;
  if (src._tracFailure){
    _tracFailure = src._tracFailure->clone();
  }
  
};
PowerFailureCriterion::~PowerFailureCriterion(){
  if (_compFailure){
    delete _compFailure;
    _compFailure = NULL;
  }
  if (_tracFailure){
    delete _tracFailure;
    _tracFailure = NULL;
  }
}

double PowerFailureCriterion::getFailureCriterion(const IPVariable* ipvprev, const IPVariable* ipv) const{
  const IPHyperViscoElastoPlastic* viscoipv = dynamic_cast<const IPHyperViscoElastoPlastic*>(ipv);
  if (viscoipv == NULL){
    Msg::Error("PowerFailureCriterion::isFailed is implemented for IPHyperViscoElastoPlastic only");
    return 1e10;
  }
  else{
    // compute failure r and DrDF
    const STensor3& K = viscoipv->_kirchhoff;
    double p = K.trace()/3.;
    STensor3 devK = K.dev();
    double Keq = sqrt(1.5*devK.dotprod());
    
    double Xc, dXc, Xt, dXt;
    const IPHyperViscoElastoPlastic* viscoipvprev = static_cast<const IPHyperViscoElastoPlastic*>(ipvprev);
    _compFailure->get(viscoipvprev->_DgammaDt,Xc,dXc);
    _tracFailure->get(viscoipvprev->_DgammaDt,Xt,dXt);

    double m = Xt/Xc;
    double mExal = pow(m,_failurePower);
    double KeqXc = Keq/Xc;
    double pXc = p/Xc;
    double KeqExal = 0.;

    if (KeqXc > 1e-20){
      KeqExal = pow(KeqXc,_failurePower);
    }

    double b2 = (m+1.)/(mExal+m);
    double b1 = (1.- mExal)/(mExal+m);
    double b0 = 1.;

    double Phif = b2*KeqExal+  b1* 3.*pXc - b0;
    return Phif;
  }
};

bool PowerFailureCriterion::isFailed(const IPVariable* ipvprev, const IPVariable* ipv) const{
  const IPHyperViscoElastoPlastic* viscoipv = dynamic_cast<const IPHyperViscoElastoPlastic*>(ipv);
  if (viscoipv == NULL){
    Msg::Error("PowerFailureCriterion::isFailed is implemented for IPHyperViscoElastoPlastic only");
    return false;
  }
  else{
    double Phif = this->getFailureCriterion(ipvprev,ipv);
    double r = viscoipv->getFailureOnset();  
    double Ff =  Phif  - r;
    if (Ff >=0) return true;
    else return false;
  }
};

CritialPlasticDeformationCriteration::CritialPlasticDeformationCriteration(const int num, const double a, const double b, const double c): mlawHyperelasticFailureCriterionBase(num),
  _a(a),_b(b),_c(c){};
  
CritialPlasticDeformationCriteration::CritialPlasticDeformationCriteration(const CritialPlasticDeformationCriteration& src): mlawHyperelasticFailureCriterionBase(src),
  _a(src._a),_b(src._b),_c(src._c){};

double CritialPlasticDeformationCriteration::getFailureCriterion(const IPVariable* ipvprev, const IPVariable* ipv) const{
  const IPHyperViscoElastoPlastic* viscoipv = dynamic_cast<const IPHyperViscoElastoPlastic*>(ipv);
  if (viscoipv == NULL){
    Msg::Error("CritialPlasticDeformationCriteration::getFailureCriterion is implemented for IPHyperViscoElastoPlastic only");
    return 1e10;
  }
  else{
    // compute failure r and DrDF
    const STensor3& K = viscoipv->_kirchhoff;
    double pres = K.trace()/3.;
    STensor3 devK = K.dev();
    double Keq = sqrt(1.5*devK.dotprod());
    double p = viscoipv->getConstRefToEqPlasticStrain();
    
    double T = pres/Keq;
    if (T != T) T = 0.;
    return (p - _a*(exp(-_b*T)) - _c);
  }
};
bool CritialPlasticDeformationCriteration::isFailed(const IPVariable* ipvprev, const IPVariable* ipv) const{
  const IPHyperViscoElastoPlastic* viscoipv = dynamic_cast<const IPHyperViscoElastoPlastic*>(ipv);
  if (viscoipv == NULL){
    Msg::Error("CritialPlasticDeformationCriteration::isFailed is implemented for IPHyperViscoElastoPlastic only");
    return false;
  }
  else{
    double Phif = this->getFailureCriterion(ipvprev,ipv);
    double r = viscoipv->getFailureOnset();  
    if (Phif >= r) return true;
    else return false;
  }
};

void ThreeCriticalStressCriterion::getFactor(const double sigt, const double sigc, const double sigs, double& aa, double& bb, double& cc) const{
  double pc = -sigc/3.;
  double pt = sigt/3.;
  
  double mat[2][2];
  mat[0][0] = pc;
  mat[0][1] = sigc-sigs;
  mat[1][0] = pt;
  mat[1][1] = sigt-sigs;
  
  double det = mat[0][0] * mat[1][1] - mat[1][0] * mat[0][1];
  double inv[2][2];
  if(fabs(det) > 0){
    double ud = 1. / det;
    inv[0][0] =  mat[1][1] * ud;
    inv[1][0] = -mat[1][0] * ud;
    inv[0][1] = -mat[0][1] * ud;
    inv[1][1] =  mat[0][0] * ud;
  }
  else{
    Msg::Error("Singular matrix 3x3 ThreeCriticalStressCriterion::ThreeCriticalStressCriterion");
  }
  
  double vec[2];
  vec[0] = (sigs-sigc)*pc;
  vec[1] = (sigs-sigt)*pt;
  
  double sol[2];
  sol[0] = inv[0][0]*vec[0]+inv[0][1]*vec[1];
  sol[1] = inv[1][0]*vec[0]+inv[1][1]*vec[1];
  
  // compute factor a, b, c
  aa = sol[0]*sol[1];
  bb = sol[1];
  cc = sigs - aa/bb;
  
  //printf(" aa = %e bb = %e cc = %e\n",aa,bb,cc);
};

ThreeCriticalStressCriterion::ThreeCriticalStressCriterion(const int num, const RateFailureLaw& trac, const RateFailureLaw& comp, const RateFailureLaw& shear): mlawHyperelasticFailureCriterionBase(num){
  _compFailure = comp.clone();
  _tracFailure = trac.clone();
  _shearFailure = shear.clone();
  _isConstant = (comp.isConstant() && trac.isConstant() && shear.isConstant());
  // esitmated a,b, c
  if (_isConstant){
    double sigc,sigt,sigs,dR;
    _compFailure->get(0,sigc,dR);
    _tracFailure->get(0,sigt,dR);
    _shearFailure->get(0,sigs,dR);
  
    getFactor(sigt,sigc,sigs,a,b,c);
  }
};

  
ThreeCriticalStressCriterion::ThreeCriticalStressCriterion(const ThreeCriticalStressCriterion& src): mlawHyperelasticFailureCriterionBase(src), a(src.a),b(src.b),c(src.c), _isConstant(src._isConstant){
  _compFailure = NULL;
  if (src._compFailure){
    _compFailure= src._compFailure->clone();
  }
  _tracFailure = NULL;
  if (src._tracFailure){
    _tracFailure = src._tracFailure->clone();
  }
  _shearFailure = NULL;
  if (src._shearFailure){
    _shearFailure = src._shearFailure->clone();
  }
};

ThreeCriticalStressCriterion::~ThreeCriticalStressCriterion(){
  if (_compFailure){
    delete _compFailure;
    _compFailure = NULL;
  }
  if (_tracFailure){
    delete _tracFailure;
    _tracFailure = NULL;
  }
  if (_shearFailure){
    delete _shearFailure;
    _shearFailure = NULL;
  }
}

double ThreeCriticalStressCriterion::getFailureCriterion(const IPVariable* ipvprev, const IPVariable* ipv) const{
  const IPHyperViscoElastoPlastic* viscoipv = dynamic_cast<const IPHyperViscoElastoPlastic*>(ipv);
  if (viscoipv == NULL){
    Msg::Error("IPHyperViscoElastoPlastic::getFailureCriterion is implemented for IPHyperViscoElastoPlastic only");
    return 1e10;
  }
  else{
    if (!_isConstant){
      double sigc,sigt,sigs,dR;
      const IPHyperViscoElastoPlastic* viscoipvprev = static_cast<const IPHyperViscoElastoPlastic*>(ipvprev);
      _compFailure->get(viscoipvprev->_DgammaDt,sigc,dR);
      _tracFailure->get(viscoipvprev->_DgammaDt,sigt,dR);
      _shearFailure->get(viscoipvprev->_DgammaDt,sigs,dR);
      getFactor(sigt,sigc,sigs,a,b,c);
    }
    
    // compute failure r and DrDF
    const STensor3& K = viscoipv->_kirchhoff;
    double pres = K.trace()/3.;
    STensor3 devK = K.dev();
    double Keq = sqrt(1.5*devK.dotprod());
    // get coeficinet
    //K.print("Kir stess");
    //Msg::Info(" p = %e  KEq = %e, cr = %e",pres,Keq,Keq - a/(pres+b) -c);
    return (Keq - a/(pres+b) -c);
  }
};
bool ThreeCriticalStressCriterion::isFailed(const IPVariable* ipvprev, const IPVariable* ipv) const{
  const IPHyperViscoElastoPlastic* viscoipv = dynamic_cast<const IPHyperViscoElastoPlastic*>(ipv);
  if (viscoipv == NULL){
    Msg::Error("IPHyperViscoElastoPlastic::isFailed is implemented for IPHyperViscoElastoPlastic only");
    return false;
  }
  else{
    double Phif = this->getFailureCriterion(ipvprev,ipv);
    double r = viscoipv->getFailureOnset();  
    if (Phif >= r) return true;
    else return false;
  }
};


CritialNonLocalPlasticDeformationCriteration::CritialNonLocalPlasticDeformationCriteration(const int num, const double a, const double b, const double c): mlawHyperelasticFailureCriterionBase(num),
  _a(a),_b(b),_c(c){};
  
CritialNonLocalPlasticDeformationCriteration::CritialNonLocalPlasticDeformationCriteration(const CritialNonLocalPlasticDeformationCriteration& src): mlawHyperelasticFailureCriterionBase(src),
  _a(src._a),_b(src._b),_c(src._c){};

double CritialNonLocalPlasticDeformationCriteration::getFailureCriterion(const IPVariable* ipvprev, const IPVariable* ipv) const{
  const IPHyperViscoElastoPlasticMultipleNonLocalDamage* viscoipv = dynamic_cast<const IPHyperViscoElastoPlasticMultipleNonLocalDamage*>(ipv);
  if (viscoipv == NULL){
    Msg::Error("CritialNonLocalPlasticDeformationCriteration::getFailureCriterion is implemented for IPHyperViscoElastoPlasticMultipleNonLocalDamage only");
    return 1e10;
  }
  else{
    // compute failure r and DrDF
    const STensor3& K = viscoipv->_kirchhoff;
    double pres = K.trace()/3.;
    STensor3 devK = K.dev();
    double Keq = sqrt(1.5*devK.dotprod());
    double p = viscoipv->getNonLocalFailurePlasticity();
    
    double T = pres/Keq;
    if (T != T) T = 0.;
    return (p - _a*(exp(-_b*T)) - _c);
  }
};
bool CritialNonLocalPlasticDeformationCriteration::isFailed(const IPVariable* ipvprev, const IPVariable* ipv) const{
  const IPHyperViscoElastoPlasticMultipleNonLocalDamage* viscoipv = dynamic_cast<const IPHyperViscoElastoPlasticMultipleNonLocalDamage*>(ipv);
  if (viscoipv == NULL){
    Msg::Error("CritialNonLocalPlasticDeformationCriteration::isFailed is implemented for IPHyperViscoElastoPlasticMultipleNonLocalDamage only");
    return false;
  }
  else{
    double Phif = this->getFailureCriterion(ipvprev,ipv);
    double r = viscoipv->getFailureOnset();  
    if (Phif >= r) return true;
    else return false;
  }
};