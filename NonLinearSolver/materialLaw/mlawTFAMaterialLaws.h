//
// C++ Interface: material law
//
// Author:  <Kevin Spilker>, (C) 2019
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef MLAWTFA_H_
#define MLAWTFA_H_

#include "mlaw.h"
#include "STensor3.h"
#include "STensor43.h"
#include "STensor63.h"
#include "ipTFA.h"
#include "ipField.h"
#include "modelReduction.h"

class mlawGenericTFAMaterialLaws : public materialLaw{

  #ifndef SWIG
  protected:
    double  _mu, _nu, _rho;
    bool _ownData;
    ReductionTFA *_reductionModel;
   #endif // SWIG

  public:
    mlawGenericTFAMaterialLaws(const int num, double rho);
    void setTFAMethod(int TFAMethodNum, int dim, int correction=0, int solverType=0, int tag=1000);
    void loadClusterSummaryAndInteractionTensorsFromFiles(const std::string clusterSummaryFileName, 
                                        const std::string interactionTensorsFileName);
    void loadClusterSummaryAndInteractionTensorsHomogenizedFrameFromFiles(const std::string clusterSummaryFileName, 
                                        const std::string interactionTensorsFileName);
    void loadClusterStandardDeviationFromFile(const std::string ClusterStrainConcentrationSTD);

    void loadPlasticEqStrainConcentrationFromFile(const std::string PlasticEqStrainConcentrationFileName,
                                                  const std::string PlasticEqStrainConcentrationGeometricFileName,
                                                  const std::string PlasticEqStrainConcentrationHarmonicFileName);

    void loadElasticStrainConcentrationDerivativeFromFile(const std::string ElasticStrainConcentrationDerivative);

    void loadEshelbyInteractionTensorsFromFile(const std::string EshelbyInteractionTensorsFileName);
    void loadInteractionTensorsMatrixFrameELFromFile(const std::string InteractionTensorsMatrixFrameELFileName);
    void loadInteractionTensorsHomogenizedFrameELFromFile(const std::string InteractionTensorsHomogenizedFrameELFileName);
    
    void loadReferenceStiffnessFromFile(const std::string ReferenceStiffnessFileName);
    void loadReferenceStiffnessIsoFromFile(const std::string ReferenceStiffnessIsoFileName);
    void loadInteractionTensorIsoFromFile(const std::string InteractionTensorIsoFileName);
    
    void setClusterIncOrientation();
    void loadClusterFiberOrientationFromFile(const std::string OrientationDataFileName);
    
    void addClusterMaterialLaw(int clusterNb, materialLaw* clusterLaw);
 
    #ifndef SWIG
    mlawGenericTFAMaterialLaws(const mlawGenericTFAMaterialLaws& src);
    mlawGenericTFAMaterialLaws& operator=(const materialLaw &source);    
    virtual ~mlawGenericTFAMaterialLaws();

    virtual matname getType() const{return materialLaw::tfa;}
    virtual void initLaws(const std::map<int,materialLaw*> &maplaw); //need to fill  std::set<int, const materialLaw *> matlawptr

    virtual void constitutive(
            const STensor3& F0,         // initial deformation gradient (input @ time n)
            const STensor3& Fn,         // updated deformation gradient (input @ time n+1)
            STensor3 &P,                // updated 1st Piola-Kirchhoff stress tensor (output)
            const IPVariable *q0,       // array of initial internal variable
            IPVariable *q1,             // updated array of internal variable (in ipvcur on output),
            STensor43 &Tangent,         // constitutive tangents (output)
            const bool stiff,          // if true compute the tangents
            STensor43* elasticTangent = NULL, 
            const bool dTangent =false,
            STensor63* dCalgdeps = NULL) const;    
    
    virtual materialLaw* clone() const {return new mlawGenericTFAMaterialLaws(*this);};
    virtual void checkInternalState(IPVariable* ipv, const IPVariable* ipvprev) const{}; // do nothing

    virtual void createIPState(IPTFA *ivi, IPTFA *iv1, IPTFA *iv2) const;
    virtual void createIPState(IPStateBase* &ips,bool hasBodyForce, const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const IntPt *GP=NULL, const int gpt = 0) const;
    virtual void createIPVariable(IPTFA *&ipv,  bool hasBodyForce, const MElement *ele,const int nbFF, const IntPt *GP, const int gpt) const;

    double soundSpeed() const;
            
    bool withEnergyDissipation() const { Msg::Error("mlawGenericTFAMaterialLaws does not have withEnergyDissipation"); return false;}

    virtual double shearModulus() const {return _mu;}
    virtual double poissonRatio() const {return _nu;}
    virtual double density() const{return _rho;};

    #endif // SWIG
};

class mlawTFAMaterialLaws : public mlawGenericTFAMaterialLaws{   
  
  public:
    mlawTFAMaterialLaws(const int num, double rho): mlawGenericTFAMaterialLaws(num,rho) {};
    
    #ifndef SWIG
    mlawTFAMaterialLaws(const mlawTFAMaterialLaws& src): mlawGenericTFAMaterialLaws(src) {};
    mlawTFAMaterialLaws& operator=(const materialLaw &source);
    virtual ~mlawTFAMaterialLaws();
    
    virtual materialLaw* clone() const {return new mlawTFAMaterialLaws(*this);};
    
    virtual void createIPState(IPTFA *ivi, IPTFA *iv1, IPTFA *iv2) const;
    virtual void createIPState(IPStateBase* &ips,bool hasBodyForce, const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const IntPt *GP=NULL, const int gpt = 0) const;
    virtual void createIPVariable(IPTFA *&ipv,  bool hasBodyForce, const MElement *ele,const int nbFF, const IntPt *GP, const int gpt) const;
    
    bool withEnergyDissipation() const { Msg::Error("mlawTFAMaterialLaws does not have withEnergyDissipation"); return false;}
    
    #endif // SWIG
};

class mlawTFAHierarchicalMaterialLaws : public mlawGenericTFAMaterialLaws{

  protected:
    int _tfaLawID;
    int _nbLevels;
    int _level;
    
    std::map<int,ReductionTFA*> _reductionModels2Levels;
   
  public:
  
    mlawTFAHierarchicalMaterialLaws(const int num, double rho) : mlawGenericTFAMaterialLaws(num,rho) {};
    
    void setTFAMethod(int TFAMethodNum, int dim, int correction=0, int solverType=0, int tag=1000);
    
    #ifndef SWIG
    mlawTFAHierarchicalMaterialLaws(const mlawTFAHierarchicalMaterialLaws& src) : mlawGenericTFAMaterialLaws(src) {};
    mlawTFAHierarchicalMaterialLaws& operator=(const materialLaw &source);
    virtual ~mlawTFAHierarchicalMaterialLaws();
    
    virtual materialLaw* clone() const {return new mlawTFAHierarchicalMaterialLaws(*this);};
    
    virtual void createIPState(IPTFA2Levels *ivi, IPTFA2Levels *iv1, IPTFA2Levels *iv2) const;
    virtual void createIPState(IPStateBase* &ips,bool hasBodyForce, const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const IntPt *GP=NULL, const int gpt = 0) const;
    virtual void createIPVariable(IPTFA2Levels *&ipv,  bool hasBodyForce, const MElement *ele,const int nbFF, const IntPt *GP, const int gpt) const;
    
    bool withEnergyDissipation() const { Msg::Error("mlawTFAHierarchicalMaterialLaws does not have withEnergyDissipation"); return false;}
    
    void setTFALawID(int tfaLaw) {_tfaLawID = tfaLaw;};
    
    void loadClusterSummaryAndInteractionTensorsLevel1FromFiles(const std::string clusterSummaryFileName, 
                                                                const std::string interactionTensorsFileName);
                                                                
    virtual void initLaws(const std::map<int,materialLaw*> &maplaw);
    
    virtual void constitutive(
            const STensor3& F0,         // initial deformation gradient (input @ time n)
            const STensor3& Fn,         // updated deformation gradient (input @ time n+1)
            STensor3 &P,                // updated 1st Piola-Kirchhoff stress tensor (output)
            const IPVariable *q0,       // array of initial internal variable
            IPVariable *q1,             // updated array of internal variable (in ipvcur on output),
            STensor43 &Tangent,         // constitutive tangents (output)
            const bool stiff,          // if true compute the tangents
            STensor43* elasticTangent = NULL, 
            const bool dTangent =false,
            STensor63* dCalgdeps = NULL) const;
    
    #endif // SWIG
};

#endif //MLAWTFAMATERIALLAWS



/*class mlawTFAMaterialLaws : public materialLaw{

  #ifndef SWIG
  protected:
    double  _mu, _nu, _rho;
    bool _ownData;
    ReductionTFA *_reductionModel;
   #endif // SWIG

  public:
    mlawTFAMaterialLaws(const int num, double rho);
    void setTFAMethod(int TFAMethodNum, int dim, int correction=0, int solverType=0, int tag=1000);
    void loadClusterSummaryAndInteractionTensorsFromFiles(const std::string clusterSummaryFileName, 
                                        const std::string interactionTensorsFileName);
    void loadClusterSummaryAndInteractionTensorsHomogenizedFrameFromFiles(const std::string clusterSummaryFileName, 
                                        const std::string interactionTensorsFileName);
    void loadClusterStandardDeviationFromFile(const std::string ClusterStrainConcentrationSTD);

    void loadPlasticEqStrainConcentrationFromFile(const std::string PlasticEqStrainConcentrationFileName,
                                                  const std::string PlasticEqStrainConcentrationGeometricFileName,
                                                  const std::string PlasticEqStrainConcentrationHarmonicFileName,
                                                  const std::string PlasticEqStrainConcentrationPowerFileName);

    void loadElasticStrainConcentrationDerivativeFromFile(const std::string ElasticStrainConcentrationDerivative);

    void loadEshelbyInteractionTensorsFromFile(const std::string EshelbyInteractionTensorsFileName);
    void loadInteractionTensorsMatrixFrameELFromFile(const std::string InteractionTensorsMatrixFrameELFileName);
    void loadInteractionTensorsHomogenizedFrameELFromFile(const std::string InteractionTensorsHomogenizedFrameELFileName);
    
    void loadReferenceStiffnessFromFile(const std::string ReferenceStiffnessFileName);
    void loadReferenceStiffnessIsoFromFile(const std::string ReferenceStiffnessIsoFileName);
    void loadInteractionTensorIsoFromFile(const std::string InteractionTensorIsoFileName);
    
    void setClusterIncOrientation();
    void loadClusterFiberOrientationFromFile(const std::string OrientationDataFileName);
    
    void addClusterMaterialLaw(int clusterNb, materialLaw* clusterLaw);
 
    #ifndef SWIG
    mlawTFAMaterialLaws(const mlawTFAMaterialLaws& src);
    mlawTFAMaterialLaws& operator=(const materialLaw &source);    
    virtual ~mlawTFAMaterialLaws();

    virtual matname getType() const{return materialLaw::tfa;}
    virtual void initLaws(const std::map<int,materialLaw*> &maplaw); //need to fill  std::set<int, const materialLaw *> matlawptr

    virtual void constitutive(
            const STensor3& F0,         // initial deformation gradient (input @ time n)
            const STensor3& Fn,         // updated deformation gradient (input @ time n+1)
            STensor3 &P,                // updated 1st Piola-Kirchhoff stress tensor (output)
            const IPVariable *q0,       // array of initial internal variable
            IPVariable *q1,             // updated array of internal variable (in ipvcur on output),
            STensor43 &Tangent,         // constitutive tangents (output)
            const bool stiff,          // if true compute the tangents
            STensor43* elasticTangent = NULL, 
            const bool dTangent =false,
            STensor63* dCalgdeps = NULL) const;
    
    
    virtual materialLaw* clone() const {return new mlawTFAMaterialLaws(*this);};
    virtual void checkInternalState(IPVariable* ipv, const IPVariable* ipvprev) const{}; // do nothing

    virtual void createIPState(IPTFA *ivi, IPTFA *iv1, IPTFA *iv2) const;
    virtual void createIPState(IPStateBase* &ips,bool hasBodyForce, const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const IntPt *GP=NULL, const int gpt = 0) const;
    virtual void createIPVariable(IPTFA *&ipv,  bool hasBodyForce, const MElement *ele,const int nbFF, const IntPt *GP, const int gpt) const;

    double soundSpeed() const;
            
    bool withEnergyDissipation() const { Msg::Error("mlawTFAMaterialLaws does not have withEnergyDissipation"); return false;}

    virtual double shearModulus() const {return _mu;}
    virtual double poissonRatio() const {return _nu;}
    virtual double density() const{return _rho;};

    #endif // SWIG
};

#endif //MLAWTFAMATERIALLAWS*/




