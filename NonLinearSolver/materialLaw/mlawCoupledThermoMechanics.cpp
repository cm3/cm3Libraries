//
// Created by vinayak on 27.06.23.
//
// C++ Interface: material law
//              Coupled Thermo-Mechanical law
//              with link to use any TM law
//              such as LinearTM, PhenomenologicalSMP etc.
// Author:  <Vinayak GHOLAP>, (C) 2023
//
// Copyright: See COPYING file that comes with this distribution
//
//

#include "mlawCoupledThermoMechanics.h"
#include <math.h>
#include "MInterfaceElement.h"

mlawCoupledThermoMechanics::mlawCoupledThermoMechanics(const int num, const bool init):
materialLaw(num,init),applyReferenceF(false)
{
}


mlawCoupledThermoMechanics::mlawCoupledThermoMechanics(const mlawCoupledThermoMechanics& source)
: materialLaw(source), applyReferenceF(source.applyReferenceF)
{
}

mlawCoupledThermoMechanics& mlawCoupledThermoMechanics::operator=(const materialLaw& source)
{
    materialLaw::operator=(source);
    const mlawCoupledThermoMechanics* src =static_cast<const mlawCoupledThermoMechanics*>(&source);
    if(src !=NULL)
    {
        applyReferenceF=src->applyReferenceF;
    }
    return *this;
}