#ifndef STENSOROPERATIONS_H_
#define STENSOROPERATIONS_H_

#include "STensor3.h"
#include "STensor33.h"
#include "STensor43.h"
#include "STensor53.h"
#include "STensor63.h"
#include "fullMatrix.h"
#include <cmath>
#include <algorithm>

namespace STensorOperation{
  inline void zero(double& a){
    a= 0.;
  };

  inline void zero(SVector3& a){
    a(0) = 0.; a(1) = 0.; a(2) = 0.;
  };

  inline void zero(STensor3& a){
    a(0,0) = 0.; a(0,1) = 0.; a(0,2) = 0.;
    a(1,0) = 0.; a(1,1) = 0.; a(1,2) = 0.;
    a(2,0) = 0.; a(2,1) = 0.; a(2,2) = 0.;
  };
  inline void unity(STensor3& a){
    a(0,0) = 1.; a(0,1) = 0.; a(0,2) = 0.;
    a(1,0) = 0.; a(1,1) = 1.; a(1,2) = 0.;
    a(2,0) = 0.; a(2,1) = 0.; a(2,2) = 1.;
  };


  inline void diag(STensor3& a, const double s){
    a(0,0) = s;  a(0,1) = 0.; a(0,2) = 0.;
    a(1,0) = 0.; a(1,1) = s;  a(1,2) = 0.;
    a(2,0) = 0.; a(2,1) = 0.; a(2,2) = s;
  };

  inline void zero(STensor33& a){
    for (int i=0; i<3; i++){
      for (int j=0; j<3; j++){
        for (int k=0; k<3; k++){
          a(i,j,k) = 0.;
        }
      }
    }
  };

  inline void zero(STensor43& a){
    for (int i=0; i<3; i++){
      for (int j=0; j<3; j++){
        for (int k=0; k<3; k++){
          for (int l=0; l<3; l++){
            a(i,j,k,l) = 0.;
          }
        }
      }
    }
  };

  inline void unity(STensor43& a){

    for (int i=0; i<3; i++){
      for (int j=0; j<3; j++){
        for (int k=0; k<3; k++){
          for (int l=0; l<3; l++){
            a(i,j,k,l) = 0.;
            if( (i==l) && (j==k) )
              {a(i,j,k,l) += 0.5;}
            if( (i==k) && (j==l) )
              {a(i,j,k,l) += 0.5;}
          }
        }
      }
    }
  };


  inline void zero(STensor53& a){
    for (int i=0; i<3; i++){
      for (int j=0; j<3; j++){
        for (int k=0; k<3; k++){
          for (int l=0; l<3; l++){
            for (int p=0; p<3; p++){
              a(i,j,k,l,p) = 0.;
            }
          }
        }
      }
    }
  };
  inline void zero(STensor63& a){
    for (int i=0; i<3; i++){
      for (int j=0; j<3; j++){
        for (int k=0; k<3; k++){
          for (int l=0; l<3; l++){
            for (int p=0; p<3; p++){
              for (int q=0; q<3; q++){
                a(i,j,k,l,p,q) = 0.;
              }
            }
          }
        }
      }
    }
  };


  inline void scale(const STensor3& a, double x, STensor3& ax)
  {
    for (int i=0; i<3; i++)
    {
      for (int j=0; j<3; j++)
      {
        ax(i,j) = a(i,j)*x;
      }
    }
  }
  inline void scale(const STensor43& a, double x, STensor43& ax)
  {
    for (int i=0; i<3; i++)
    {
      for (int j=0; j<3; j++)
      {
        for (int k=0; k<3; k++)
        {
          for (int l=0; l<3; l++)
          {
            ax(i,j,k,l) = a(i,j,k,l)*x;
          }
        }
      }
    }
  }

  inline bool isnan(const double a){
    if (a != a) return true;
    return false;
  };

  inline bool isnan(const SVector3& a){
    for (int i=0; i<3; i++){
      if (a(i) != a(i)) return true;
    }
    return false;
  };

  inline bool isnan(const STensor3& a){
    for (int i=0; i<3; i++){
      for (int j=0; j<3; j++){
        if (a(i,j) != a(i,j)) return true;
      }
    }
    return false;
  };
  inline bool isnan(const STensor43& a){
    for (int i=0; i<3; i++){
      for (int j=0; j<3; j++){
         for(int k=0; k<3; k++) {
           for(int l=0; l<3; l++) {
            if (a(i,j,k,l) != a(i,j,k,l)) return true;
           }
         }
      }
    }
    return false;
  };


  inline bool isinf(const double a){
    return isinf(a);
  };

  inline bool isinf(const STensor3& a){
    for (int i=0; i<3; i++){
      for (int j=0; j<3; j++){
        if (isinf(a(i,j))) return true;
      }
    }
    return false;
  };

  inline double trace(const STensor3& a){
    return a(0,0) + a(1,1)+ a(2,2);
  };

  inline double trace(const STensor43& a){
    return a(0,0,0,0) + a(1,1,1,1) + a(2,2,2,2) + 2.*a(0,1,0,1) + 2.*a(0,2,0,2) + 2.*a(1,2,1,2);
  };

  inline void decomposeDevTr(const STensor3& E, STensor3& devE, double& trE){
    trE = E.trace();
    devE = E;
    double trEover3 = trE/3.;
    devE(0,0) -=trEover3;
    devE(1,1) -=trEover3;
    devE(2,2) -=trEover3;
  };

  inline double determinantSTensor3(const STensor3 &a)
  {
    return (a(0,0) * (a(1,1) * a(2,2) - a(1,2) * a(2,1)) -
            a(0,1) * (a(1,0) * a(2,2) - a(1,2) * a(2,0)) +
            a(0,2) * (a(1,0) * a(2,1) - a(1,1) * a(2,0)));
  }

  inline double dot(const SVector3& a, const SVector3& b)
  {
    return a(0)*b(0) + a(1)*b(1) + a(2)*b(2);
  }
  inline double doubledot(const STensor3& a, const STensor3& b){
    double val= 0.;
    for (int i=0; i<3; i++){
      for (int j=0; j<3; j++){
        val += a(i,j)*b(i,j);
      }
    }
    return val;
  };


  inline void fromSTensor3ToFullVector_ConsistentForm(const STensor3& a, fullVector<double> &vec)
  {
    vec.resize(6,true);
    vec.set(0,a(0,0));
    vec.set(1,a(1,1));
    vec.set(2,a(2,2));
    vec.set(3,a(0,1)*sqrt(2.));
    vec.set(4,a(0,2)*sqrt(2.));
    vec.set(5,a(1,2)*sqrt(2.));
  };
  inline void fromFullVectorToSTensor3_ConsistentForm(const fullVector<double> &vec, STensor3& a)
  {
    a(0,0)=vec(0);
    a(0,1)=a(1,0)=vec(3)/sqrt(2);
    a(0,2)=a(2,0)=vec(4)/sqrt(2);
    a(1,1)=vec(1);
    a(1,2)=a(2,1)=vec(5)/sqrt(2);
    a(2,2)=vec(2);
  };
  inline void fromSTensor43ToFullMatrix_ConsistentForm(const STensor43& a, fullMatrix<double> &mat)
  {
    mat.resize(6,6,true);

    mat.set(0,0,a(0,0,0,0));
    mat.set(0,1,a(0,0,1,1));
    mat.set(0,2,a(0,0,2,2));
    mat.set(0,3,sqrt(2)*a(0,0,0,1));
    mat.set(0,4,sqrt(2)*a(0,0,0,2));
    mat.set(0,5,sqrt(2)*a(0,0,1,2));

    mat.set(1,0,a(1,1,0,0));
    mat.set(1,1,a(1,1,1,1));
    mat.set(1,2,a(1,1,2,2));
    mat.set(1,3,sqrt(2)*a(1,1,0,1));
    mat.set(1,4,sqrt(2)*a(1,1,0,2));
    mat.set(1,5,sqrt(2)*a(1,1,1,2));

    mat.set(2,0,a(2,2,0,0));
    mat.set(2,1,a(2,2,1,1));
    mat.set(2,2,a(2,2,2,2));
    mat.set(2,3,sqrt(2)*a(2,2,0,1));
    mat.set(2,4,sqrt(2)*a(2,2,0,2));
    mat.set(2,5,sqrt(2)*a(2,2,1,2));

    mat.set(3,0,sqrt(2)*a(0,1,0,0));
    mat.set(3,1,sqrt(2)*a(0,1,1,1));
    mat.set(3,2,sqrt(2)*a(0,1,2,2));
    mat.set(3,3,2*a(0,1,0,1));
    mat.set(3,4,2*a(0,1,0,2));
    mat.set(3,5,2*a(0,1,1,2));

    mat.set(4,0,sqrt(2)*a(0,2,0,0));
    mat.set(4,1,sqrt(2)*a(0,2,1,1));
    mat.set(4,2,sqrt(2)*a(0,2,2,2));
    mat.set(4,3,2*a(0,2,0,1));
    mat.set(4,4,2*a(0,2,0,2));
    mat.set(4,5,2*a(0,2,1,2));

    mat.set(5,0,sqrt(2)*a(1,2,0,0));
    mat.set(5,1,sqrt(2)*a(1,2,1,1));
    mat.set(5,2,sqrt(2)*a(1,2,2,2));
    mat.set(5,3,2*a(1,2,0,1));
    mat.set(5,4,2*a(1,2,0,2));
    mat.set(5,5,2*a(1,2,1,2));
  };
  inline void fromFullMatrixToSTensor43_ConsistentForm(const fullMatrix<double> &mat,STensor43& a)
  {
    a(0,0,0,0)=mat(0,0);
    a(0,0,1,1)=mat(0,1);
    a(0,0,2,2)=mat(0,2);
    a(0,0,0,1)=a(0,0,1,0)=mat(0,3)/sqrt(2);
    a(0,0,0,2)=a(0,0,2,0)=mat(0,4)/sqrt(2);
    a(0,0,1,2)=a(0,0,2,1)=mat(0,5)/sqrt(2);


    a(1,1,0,0)=mat(1,0);
    a(1,1,1,1)=mat(1,1);
    a(1,1,2,2)=mat(1,2);
    a(1,1,0,1)=a(1,1,1,0)=mat(1,3)/sqrt(2);
    a(1,1,0,2)=a(1,1,2,0)=mat(1,4)/sqrt(2);
    a(1,1,1,2)=a(1,1,2,1)=mat(1,5)/sqrt(2);


    a(2,2,0,0)=mat(2,0);
    a(2,2,1,1)=mat(2,1);
    a(2,2,2,2)=mat(2,2);
    a(2,2,0,1)=a(2,2,1,0)=mat(2,3)/sqrt(2);
    a(2,2,0,2)=a(2,2,2,0)=mat(2,4)/sqrt(2);
    a(2,2,1,2)=a(2,2,2,1)=mat(2,5)/sqrt(2);


    a(0,1,0,0)=a(1,0,0,0)=mat(3,0)/sqrt(2);
    a(0,1,1,1)=a(1,0,1,1)=mat(3,1)/sqrt(2);
    a(0,1,2,2)=a(1,0,2,2)=mat(3,2)/sqrt(2);
    a(0,1,0,1)=a(0,1,1,0)=a(1,0,0,1)=a(1,0,1,0)=mat(3,3)/2;
    a(0,1,0,2)=a(0,1,2,0)=a(1,0,0,2)=a(1,0,2,0)=mat(3,4)/2;
    a(0,1,1,2)=a(0,1,2,1)=a(1,0,1,2)=a(1,0,2,1)=mat(3,5)/2;


    a(0,2,0,0)=a(2,0,0,0)=mat(4,0)/sqrt(2);
    a(0,2,1,1)=a(2,0,1,1)=mat(4,1)/sqrt(2);
    a(0,2,2,2)=a(2,0,2,2)=mat(4,2)/sqrt(2);
    a(0,2,0,1)=a(0,2,1,0)=a(2,0,0,1)=a(2,0,1,0)=mat(4,3)/2;
    a(0,2,0,2)=a(0,2,2,0)=a(2,0,0,2)=a(2,0,2,0)=mat(4,4)/2;
    a(0,2,1,2)=a(0,2,2,1)=a(2,0,1,2)=a(2,0,2,1)=mat(4,5)/2;


    a(1,2,0,0)=a(2,1,0,0)=mat(5,0)/sqrt(2);
    a(1,2,1,1)=a(2,1,1,1)=mat(5,1)/sqrt(2);
    a(1,2,2,2)=a(2,1,2,2)=mat(5,2)/sqrt(2);
    a(1,2,0,1)=a(1,2,1,0)=a(2,1,0,1)=a(2,1,1,0)=mat(5,3)/2;
    a(1,2,0,2)=a(1,2,2,0)=a(2,1,0,2)=a(2,1,2,0)=mat(5,4)/2;
    a(1,2,1,2)=a(1,2,2,1)=a(2,1,1,2)=a(2,1,2,1)=mat(5,5)/2;
  };


  inline void fromSTensor3ToFullVector(const STensor3& a, fullVector<double> &vec)
  {
    vec.resize(6,true);

    vec.set(0,a(0,0));
    vec.set(1,a(1,1));
    vec.set(2,a(2,2));
    vec.set(3,a(0,1));
    vec.set(4,a(0,2));
    vec.set(5,a(1,2));

  };
  inline void fromFullVectorToSTensor3(const  fullVector<double> &vec, STensor3& a)
  {
    a(0,0)=vec(0);
    a(0,1)=a(1,0)=vec(3);
    a(0,2)=a(2,0)=vec(4);
    a(1,1)=vec(1);
    a(1,2)=a(2,1)=vec(5);
    a(2,2)=vec(2);

  };

  inline void fromSTensor43ToFullMatrix(const STensor43& a, fullMatrix<double> &mat)
  {
    mat.resize(6,6,true);

    mat.set(0,0,a(0,0,0,0));
    mat.set(0,1,a(0,0,1,1));
    mat.set(0,2,a(0,0,2,2));
    mat.set(0,3,2*a(0,0,0,1));
    mat.set(0,4,2*a(0,0,0,2));
    mat.set(0,5,2*a(0,0,1,2));

    mat.set(1,0,a(1,1,0,0));
    mat.set(1,1,a(1,1,1,1));
    mat.set(1,2,a(1,1,2,2));
    mat.set(1,3,2*a(1,1,0,1));
    mat.set(1,4,2*a(1,1,0,2));
    mat.set(1,5,2*a(1,1,1,2));

    mat.set(2,0,a(2,2,0,0));
    mat.set(2,1,a(2,2,1,1));
    mat.set(2,2,a(2,2,2,2));
    mat.set(2,3,2*a(2,2,0,1));
    mat.set(2,4,2*a(2,2,0,2));
    mat.set(2,5,2*a(2,2,1,2));

    mat.set(3,0,a(0,1,0,0));
    mat.set(3,1,a(0,1,1,1));
    mat.set(3,2,a(0,1,2,2));
    mat.set(3,3,2*a(0,1,0,1));
    mat.set(3,4,2*a(0,1,0,2));
    mat.set(3,5,2*a(0,1,1,2));

    mat.set(4,0,a(0,2,0,0));
    mat.set(4,1,a(0,2,1,1));
    mat.set(4,2,a(0,2,2,2));
    mat.set(4,3,2*a(0,2,0,1));
    mat.set(4,4,2*a(0,2,0,2));
    mat.set(4,5,2*a(0,2,1,2));

    mat.set(5,0,a(1,2,0,0));
    mat.set(5,1,a(1,2,1,1));
    mat.set(5,2,a(1,2,2,2));
    mat.set(5,3,2*a(1,2,0,1));
    mat.set(5,4,2*a(1,2,0,2));
    mat.set(5,5,2*a(1,2,1,2));

    //here some with 2
  };
  inline void fromFullMatrixToSTensor43(const fullMatrix<double> &mat,STensor43& a)
  {
    a(0,0,0,0)=mat(0,0);
    a(0,0,1,1)=mat(0,1);
    a(0,0,2,2)=mat(0,2);
    a(0,0,0,1)=a(0,0,1,0)=mat(0,3)/2;
    a(0,0,0,2)=a(0,0,2,0)=mat(0,4)/2;
    a(0,0,1,2)=a(0,0,2,1)=mat(0,5)/2;


    a(1,1,0,0)=mat(1,0);
    a(1,1,1,1)=mat(1,1);
    a(1,1,2,2)=mat(1,2);
    a(1,1,0,1)=a(1,1,1,0)=mat(1,3)/2;
    a(1,1,0,2)=a(1,1,2,0)=mat(1,4)/2;
    a(1,1,1,2)=a(1,1,2,1)=mat(1,5)/2;


    a(2,2,0,0)=mat(2,0);
    a(2,2,1,1)=mat(2,1);
    a(2,2,2,2)=mat(2,2);
    a(2,2,0,1)=a(2,2,1,0)=mat(2,3)/2;
    a(2,2,0,2)=a(2,2,2,0)=mat(2,4)/2;
    a(2,2,1,2)=a(2,2,2,1)=mat(2,5)/2;


    a(0,1,0,0)=a(1,0,0,0)=mat(3,0);
    a(0,1,1,1)=a(1,0,1,1)=mat(3,1);
    a(0,1,2,2)=a(1,0,2,2)=mat(3,2);
    a(0,1,0,1)=a(0,1,1,0)=a(1,0,0,1)=a(1,0,1,0)=mat(3,3)/2;
    a(0,1,0,2)=a(0,1,2,0)=a(1,0,0,2)=a(1,0,2,0)=mat(3,4)/2;
    a(0,1,1,2)=a(0,1,2,1)=a(1,0,1,2)=a(1,0,2,1)=mat(3,5)/2;


    a(0,2,0,0)=a(2,0,0,0)=mat(4,0);
    a(0,2,1,1)=a(2,0,1,1)=mat(4,1);
    a(0,2,2,2)=a(2,0,2,2)=mat(4,2);
    a(0,2,0,1)=a(0,2,1,0)=a(2,0,0,1)=a(2,0,1,0)=mat(4,3)/2;
    a(0,2,0,2)=a(0,2,2,0)=a(2,0,0,2)=a(2,0,2,0)=mat(4,4)/2;
    a(0,2,1,2)=a(0,2,2,1)=a(2,0,1,2)=a(2,0,2,1)=mat(4,5)/2;


    a(1,2,0,0)=a(2,1,0,0)=mat(5,0);
    a(1,2,1,1)=a(2,1,1,1)=mat(5,1);
    a(1,2,2,2)=a(2,1,2,2)=mat(5,2);
    a(1,2,0,1)=a(1,2,1,0)=a(2,1,0,1)=a(2,1,1,0)=mat(5,3)/2;
    a(1,2,0,2)=a(1,2,2,0)=a(2,1,0,2)=a(2,1,2,0)=mat(5,4)/2;
    a(1,2,1,2)=a(1,2,2,1)=a(2,1,1,2)=a(2,1,2,1)=mat(5,5)/2;

    //here some with 1/2
  };

  inline void inverseSTensor3(const STensor3 &a, STensor3 &ainv, STensor43* DainvDa=NULL, bool sym=true)
  {
    double udet = 1./determinantSTensor3(a);
    ainv(0,0) =  (a(1,1) * a(2,2) - a(1,2) * a(2,1))* udet;
    ainv(1,0) = -(a(1,0) * a(2,2) - a(1,2) * a(2,0))* udet;
    ainv(2,0) =  (a(1,0) * a(2,1) - a(1,1) * a(2,0))* udet;
    ainv(0,1) = -(a(0,1) * a(2,2) - a(0,2) * a(2,1))* udet;
    ainv(1,1) =  (a(0,0) * a(2,2) - a(0,2) * a(2,0))* udet;
    ainv(2,1) = -(a(0,0) * a(2,1) - a(0,1) * a(2,0))* udet;
    ainv(0,2) =  (a(0,1) * a(1,2) - a(0,2) * a(1,1))* udet;
    ainv(1,2) = -(a(0,0) * a(1,2) - a(0,2) * a(1,0))* udet;
    ainv(2,2) =  (a(0,0) * a(1,1) - a(0,1) * a(1,0))* udet;

    if (DainvDa != NULL)
    {
      for (int i=0; i< 3; i++)
      {
        for (int j=0; j< 3; j++)
        {
          for (int k=0; k< 3; k++)
          {
            for (int l=0; l< 3; l++)
            {
              if (sym)
              {
                (*DainvDa)(i,j,k,l) = -0.5*(ainv(i,k)*ainv(j,l)+ ainv(i,l)*ainv(j,k));
              }
              else
              {
                (*DainvDa)(i,j,k,l)  = -ainv(i,k)*ainv(l,j);
              }
          }
          }
        }
      }
    }

  };

  inline void inverseAndTransposeSTensor3(const STensor3 &a, STensor3 &ainvT)
  {
    double udet = 1./determinantSTensor3(a);
    ainvT(0,0) =  (a(1,1) * a(2,2) - a(1,2) * a(2,1))* udet;
    ainvT(0,1) = -(a(1,0) * a(2,2) - a(1,2) * a(2,0))* udet;
    ainvT(0,2) =  (a(1,0) * a(2,1) - a(1,1) * a(2,0))* udet;
    ainvT(1,0) = -(a(0,1) * a(2,2) - a(0,2) * a(2,1))* udet;
    ainvT(1,1) =  (a(0,0) * a(2,2) - a(0,2) * a(2,0))* udet;
    ainvT(1,2) = -(a(0,0) * a(2,1) - a(0,1) * a(2,0))* udet;
    ainvT(2,0) =  (a(0,1) * a(1,2) - a(0,2) * a(1,1))* udet;
    ainvT(2,1) = -(a(0,0) * a(1,2) - a(0,2) * a(1,0))* udet;
    ainvT(2,2) =  (a(0,0) * a(1,1) - a(0,1) * a(1,0))* udet;
  };

  inline void inverseSTensor43(const STensor43 &a, STensor43 &ainv)
  {
    //go to 6 by 6
    fullMatrix<double> mat;
    //invert
    fromSTensor43ToFullMatrix(a,mat);
    mat.invertInPlace();
    //go back
    fromFullMatrixToSTensor43(mat,ainv);
  };

  inline void inverseSTensor43InPlace(STensor43 &a)
  {
    fullMatrix<double> mat;
    fromSTensor43ToFullMatrix(a,mat);
    mat.invertInPlace();
    fromFullMatrixToSTensor43(mat,a);
  };

  inline bool invertFullMatrix4x4(const double m[16], double inv[16])
  {
    inv[0] = m[5]  * m[10] * m[15] -
             m[5]  * m[11] * m[14] -
             m[9]  * m[6]  * m[15] +
             m[9]  * m[7]  * m[14] +
             m[13] * m[6]  * m[11] -
             m[13] * m[7]  * m[10];

    inv[4] = -m[4]  * m[10] * m[15] +
              m[4]  * m[11] * m[14] +
              m[8]  * m[6]  * m[15] -
              m[8]  * m[7]  * m[14] -
              m[12] * m[6]  * m[11] +
              m[12] * m[7]  * m[10];

    inv[8] = m[4]  * m[9] * m[15] -
             m[4]  * m[11] * m[13] -
             m[8]  * m[5] * m[15] +
             m[8]  * m[7] * m[13] +
             m[12] * m[5] * m[11] -
             m[12] * m[7] * m[9];

    inv[12] = -m[4]  * m[9] * m[14] +
               m[4]  * m[10] * m[13] +
               m[8]  * m[5] * m[14] -
               m[8]  * m[6] * m[13] -
               m[12] * m[5] * m[10] +
               m[12] * m[6] * m[9];

    inv[1] = -m[1]  * m[10] * m[15] +
              m[1]  * m[11] * m[14] +
              m[9]  * m[2] * m[15] -
              m[9]  * m[3] * m[14] -
              m[13] * m[2] * m[11] +
              m[13] * m[3] * m[10];

    inv[5] = m[0]  * m[10] * m[15] -
             m[0]  * m[11] * m[14] -
             m[8]  * m[2] * m[15] +
             m[8]  * m[3] * m[14] +
             m[12] * m[2] * m[11] -
             m[12] * m[3] * m[10];

    inv[9] = -m[0]  * m[9] * m[15] +
              m[0]  * m[11] * m[13] +
              m[8]  * m[1] * m[15] -
              m[8]  * m[3] * m[13] -
              m[12] * m[1] * m[11] +
              m[12] * m[3] * m[9];

    inv[13] = m[0]  * m[9] * m[14] -
              m[0]  * m[10] * m[13] -
              m[8]  * m[1] * m[14] +
              m[8]  * m[2] * m[13] +
              m[12] * m[1] * m[10] -
              m[12] * m[2] * m[9];

    inv[2] = m[1]  * m[6] * m[15] -
             m[1]  * m[7] * m[14] -
             m[5]  * m[2] * m[15] +
             m[5]  * m[3] * m[14] +
             m[13] * m[2] * m[7] -
             m[13] * m[3] * m[6];

    inv[6] = -m[0]  * m[6] * m[15] +
              m[0]  * m[7] * m[14] +
              m[4]  * m[2] * m[15] -
              m[4]  * m[3] * m[14] -
              m[12] * m[2] * m[7] +
              m[12] * m[3] * m[6];

    inv[10] = m[0]  * m[5] * m[15] -
              m[0]  * m[7] * m[13] -
              m[4]  * m[1] * m[15] +
              m[4]  * m[3] * m[13] +
              m[12] * m[1] * m[7] -
              m[12] * m[3] * m[5];

    inv[14] = -m[0]  * m[5] * m[14] +
               m[0]  * m[6] * m[13] +
               m[4]  * m[1] * m[14] -
               m[4]  * m[2] * m[13] -
               m[12] * m[1] * m[6] +
               m[12] * m[2] * m[5];

    inv[3] = -m[1] * m[6] * m[11] +
              m[1] * m[7] * m[10] +
              m[5] * m[2] * m[11] -
              m[5] * m[3] * m[10] -
              m[9] * m[2] * m[7] +
              m[9] * m[3] * m[6];

    inv[7] = m[0] * m[6] * m[11] -
             m[0] * m[7] * m[10] -
             m[4] * m[2] * m[11] +
             m[4] * m[3] * m[10] +
             m[8] * m[2] * m[7] -
             m[8] * m[3] * m[6];

    inv[11] = -m[0] * m[5] * m[11] +
               m[0] * m[7] * m[9] +
               m[4] * m[1] * m[11] -
               m[4] * m[3] * m[9] -
               m[8] * m[1] * m[7] +
               m[8] * m[3] * m[5];

    inv[15] = m[0] * m[5] * m[10] -
              m[0] * m[6] * m[9] -
              m[4] * m[1] * m[10] +
              m[4] * m[2] * m[9] +
              m[8] * m[1] * m[6] -
              m[8] * m[2] * m[5];

    double det = m[0] * inv[0] + m[1] * inv[4] + m[2] * inv[8] + m[3] * inv[12];

    if (det == 0)
        return false;

    det = 1.0 / det;
    for (int i = 0; i < 16; i++)
        inv[i] *= det;
    return true;
  }


  inline double determinantSTensor43(const STensor43 &a)
  {
    fullMatrix<double> mat;
    fromSTensor43ToFullMatrix(a,mat);
    double det = mat.determinant();
    return det;
  };


  inline void deviatorfunction(STensor43& a){
    fullMatrix<double> mat(6,6);
    mat.set(0,0,2./3.);
    mat.set(1,1,2./3.);
    mat.set(2,2,2./3.);
    mat.set(0,1,-1./3.);
    mat.set(0,2,-1./3.);
    mat.set(1,0,-1./3.);
    mat.set(1,2,-1./3.);
    mat.set(2,0,-1./3.);
    mat.set(2,1,-1./3.);
    mat.set(3,3,1.);
    mat.set(4,4,1.);
    mat.set(5,5,1.);

    fromFullMatrixToSTensor43(mat,a);
  };


  inline void sphericalfunction(STensor43& a){
    fullMatrix<double> mat(6,6);
    mat.set(0,0,1./3.);
    mat.set(1,1,1./3.);
    mat.set(2,2,1./3.);
    mat.set(0,1,1./3.);
    mat.set(0,2,1./3.);
    mat.set(1,0,1./3.);
    mat.set(1,2,1./3.);
    mat.set(2,0,1./3.);
    mat.set(2,1,1./3.);

    fromFullMatrixToSTensor43(mat,a);
  };


  inline void sphericalfunction_Voigt(fullMatrix<double>& mat){
    mat.set(0,0,1./3.);
    mat.set(1,1,1./3.);
    mat.set(2,2,1./3.);
    mat.set(0,1,1./3.);
    mat.set(0,2,1./3.);
    mat.set(1,0,1./3.);
    mat.set(1,2,1./3.);
    mat.set(2,0,1./3.);
    mat.set(2,1,1./3.);
  };

  inline void deviatorfunction_Voigt(fullMatrix<double>& mat){
    mat.set(0,0,2./3.);
    mat.set(1,1,2./3.);
    mat.set(2,2,2./3.);
    mat.set(0,1,-1./3.);
    mat.set(0,2,-1./3.);
    mat.set(1,0,-1./3.);
    mat.set(1,2,-1./3.);
    mat.set(2,0,-1./3.);
    mat.set(2,1,-1./3.);
    mat.set(3,3,1.);
    mat.set(4,4,1.);
    mat.set(5,5,1.);
  };

  inline void unity_Voigt(fullMatrix<double>& mat){
    mat.set(0,0,1.);
    mat.set(1,1,1.);
    mat.set(2,2,1.);
    mat.set(3,3,1.);
    mat.set(4,4,1.);
    mat.set(5,5,1.);
  };


  inline void prod(const STensor3& a, const STensor3& b, const double alp, STensor43& c){
    for (int i=0; i<3; i++){
      for (int j=0; j<3; j++){
        for (int k=0; k<3; k++){
          for (int l=0; l<3; l++){
            c(i,j,k,l) = alp*a(i,j)*b(k,l);
          }
        }
      }
    }
  }
  // c += alp*axb
  inline void prodAdd(const STensor3& a, const STensor3& b, const double alp, STensor43& c){
    for (int i=0; i<3; i++){
      for (int j=0; j<3; j++){
        for (int k=0; k<3; k++){
          for (int l=0; l<3; l++){
            c(i,j,k,l) += alp*a(i,j)*b(k,l);
          }
        }
      }
    }
  }

  inline void axpy(STensor43& y, const STensor43& x, const double alp){
    for (int i=0; i<3; i++){
      for (int j=0; j<3; j++){
        for (int k=0; k<3; k++){
          for (int l=0; l<3; l++){
            y(i,j,k,l) += alp*x(i,j,k,l);
          }
        }
      }
    }
  }
  inline void axpy(STensor3& y, const STensor3& x, const double alp){
    for (int i=0; i<3; i++){
      for (int j=0; j<3; j++){
        y(i,j) += alp*x(i,j);
      }
    }
  }


  inline void transposeSTensor3(const STensor3 &a, STensor3 &aT)
  {
    aT(0,0) = a(0,0); aT(0,1) = a(1,0); aT(0,2) = a(2,0);
    aT(1,0) = a(0,1); aT(1,1) = a(1,1); aT(1,2) = a(2,1);
    aT(2,0) = a(0,2); aT(2,1) = a(1,2); aT(2,2) = a(2,2);
  };



  inline void multSTensor3(const STensor3& a, const STensor3&b, STensor3& c){
    for (int i=0; i<3; i++){
      for (int j=0; j<3; j++){
        c(i,j) = 0.;
        for (int k=0; k<3; k++){
          c(i,j) +=a(i,k)*b(k,j);
        }
      }
    }
  };

  inline void STensor63ContractionSTensor33(const STensor63& a, const STensor33&b, STensor33& c){

    for (int i=0; i<3; i++)
      for (int j=0; j<3; j++)
        for (int k=0; k<3; k++){
          c(i,j,k) = 0.0;
          for (int l=0; l<3; l++)
            for (int p=0; p<3; p++)
              for (int q=0; q<3; q++){
                 c(i,j,k) += a(i,j,k,l,p,q)*b(l,p,q);
              }
    }
  };

  inline void doubleContractionSTensor3(const STensor3& a, const STensor3&b, double& c){
    c = 0.;
    for (int i=0; i<3; i++){
      for (int j=0; j<3; j++){
        c += a(i,j)*b(i,j);
      }
    }
  };

  inline void multSTensor3Add(const STensor3& a, const STensor3&b, const double alph, STensor3& c){
    for (int i=0; i<3; i++){
      for (int j=0; j<3; j++){
        for (int k=0; k<3; k++){
          c(i,j) +=alph*a(i,k)*b(k,j);
        }
      }
    }
  };

  inline void multSTensor3FirstTranspose(const STensor3& a, const STensor3&b, STensor3& c){
    for (int i=0; i<3; i++){
      for (int j=0; j<3; j++){
        c(i,j) = 0.;
        for (int k=0; k<3; k++){
          c(i,j) +=a(k,i)*b(k,j);
        }
      }
    }
  };
  inline void multSTensor3FirstTransposeAdd(const STensor3& a, const STensor3&b, STensor3& c){
    for (int i=0; i<3; i++){
      for (int j=0; j<3; j++){
        for (int k=0; k<3; k++){
          c(i,j) +=a(k,i)*b(k,j);
        }
      }
    }
  };

  inline void multSTensor3STensor43(const STensor3& a, const STensor43& b, STensor3& c){
    for (int i=0; i<3; i++){
      for (int j=0; j<3; j++){
        c(i,j) = 0.;
        for (int k=0; k<3; k++){
          for (int l=0; l<3; l++){
            c(i,j) += a(k,l)*b(k,l,i,j);
          }
        }
      }
    }
  };
  inline void multSTensor3STensor63(const STensor3& a, const STensor63& b, STensor43& c){
    for (int i=0; i<3; i++){
      for (int j=0; j<3; j++){
        for (int p=0; p<3; p++){
          for (int q=0; q<3; q++){
            c(i,j, p, q)  = 0.;
            for (int k=0; k<3; k++){
              for (int l=0; l<3; l++){
                c(i,j, p, q) += a(k,l)*b(k,l,i,j, p, q);
              }
            }
          }
        }
      }
    }
  };
  inline void multSTensor3STensor63Add(const STensor3& a, const STensor63& b, STensor43& c, double fact=1.){
    for (int i=0; i<3; i++){
      for (int j=0; j<3; j++){
        for (int p=0; p<3; p++){
          for (int q=0; q<3; q++){
            for (int k=0; k<3; k++){
              for (int l=0; l<3; l++){
                c(i,j, p, q) += fact*a(k,l)*b(k,l,i,j, p, q);
              }
            }
          }
        }
      }
    }
  };

  inline void multSTensor3STensor33(const STensor3& a, const STensor33& b, SVector3& c){
    for (int i=0; i<3; i++){
      c(i) = 0.;
      for (int k=0; k<3; k++){
        for (int l=0; l<3; l++){
          c(i) += a(k,l)*b(k,l,i);
        }
      }
    }
  };

  inline void multSTensor3STensor43Add(const STensor3& a, const STensor43& b, const double fact, STensor3& c){
    for (int i=0; i<3; i++){
      for (int j=0; j<3; j++){
        for (int k=0; k<3; k++){
          for (int l=0; l<3; l++){
            c(i,j) += fact*a(k,l)*b(k,l,i,j);
          }
        }
      }
    }
  };

  inline void multSTensor3SecondTranspose(const STensor3& a, const STensor3& b, STensor3& c){
    for (int i=0; i<3; i++){
      for (int j=0; j<3; j++){
        c(i,j) = 0.;
        for (int k=0; k<3; k++){
          c(i,j) +=a(i,k)*b(j,k);
        }
      }
    }
  };

  inline void multSTensor43(const STensor43& a, const STensor43& b, STensor43& c){
    for (int i=0; i<3; i++){
      for (int j=0; j<3; j++){
        for (int k=0; k<3; k++){
          for (int l=0; l<3; l++){
            c(i,j,k,l) = 0.;
            for (int p=0; p<3; p++){
              for (int q=0; q<3; q++){
                c(i,j,k,l) += a(i,j,p,q)*b(p,q,k,l);
              }
            }
          }
        }
      }
    }
  };

  inline void multSTensor43FirstIndexes(const STensor43& a, const STensor43& b, STensor43& c){
    // cijkl = a_pqij*b_pqkl
    for (int i=0; i<3; i++){
      for (int j=0; j<3; j++){
        for (int k=0; k<3; k++){
          for (int l=0; l<3; l++){
            c(i,j,k,l) = 0.;
            for (int p=0; p<3; p++){
              for (int q=0; q<3; q++){
                c(i,j,k,l) += a(p,q,i,j)*b(p,q,k,l);
              }
            }
          }
        }
      }
    }
  };
  inline void multSTensor43LastIndexes(const STensor43& a, const STensor43& b, STensor43& c){
    // cijkl = a_ijpq*b_klpq
    for (int i=0; i<3; i++){
      for (int j=0; j<3; j++){
        for (int k=0; k<3; k++){
          for (int l=0; l<3; l++){
            c(i,j,k,l) = 0.;
            for (int p=0; p<3; p++){
              for (int q=0; q<3; q++){
                c(i,j,k,l) += a(i,j,p,q)*b(k,l,p,q);
              }
            }
          }
        }
      }
    }
  };

  inline void multSTensor43InPlace(STensor43& a, const STensor43& b){
    STensor43 a_new;
    for (int i=0; i<3; i++){
      for (int j=0; j<3; j++){
        for (int k=0; k<3; k++){
          for (int l=0; l<3; l++){
            a_new(i,j,k,l) = 0.;
            for (int p=0; p<3; p++){
              for (int q=0; q<3; q++){
                a_new(i,j,k,l) += a(i,j,p,q)*b(p,q,k,l);
              }
            }
          }
        }
      }
    }
    a = a_new;
  };

  inline void multSTensor43InPlaceSecond(const STensor43& a, STensor43& b){
    STensor43 b_new;
    for (int i=0; i<3; i++){
      for (int j=0; j<3; j++){
        for (int k=0; k<3; k++){
          for (int l=0; l<3; l++){
            b_new(i,j,k,l) = 0.;
            for (int p=0; p<3; p++){
              for (int q=0; q<3; q++){
                b_new(i,j,k,l) += a(i,j,p,q)*b(p,q,k,l);
              }
            }
          }
        }
      }
    }
    b = b_new;
  };

  inline void mult3xSTensor43(const STensor43& a, const STensor43& b, const STensor43& c, STensor43& abc){
    multSTensor43(a,b,abc);
    multSTensor43InPlace(abc,c);
  };

  inline void contract4STensor43(const STensor43& a, const STensor43& b, double& c){
    c=0.;
    for (int i=0; i<3; i++){
      for (int j=0; j<3; j++){
        for (int k=0; k<3; k++){
          for (int l=0; l<3; l++){
            c += a(i,j,k,l)*b(i,j,k,l);
          }
        }
      }
    }
  };


  inline void contract4STensor43STensor63(const STensor43& a, const STensor63& b, STensor3& c){
    for (int m=0; m<3; m++){
      for (int n=0; n<3; n++){
        c(m,n)=0.;
        for (int i=0; i<3; i++){
          for (int j=0; j<3; j++){
            for (int k=0; k<3; k++){
              for (int l=0; l<3; l++){
                c(m,n) += a(i,j,k,l)*b(i,j,k,l,m,n);
              }
            }
          }
        }
      }
    }
  };


  inline void multSTensor63_ind56_STensor43_ind12(const STensor63& a, const STensor43& b, STensor63& c){
  //multiplication of 6th order and 4th order tensors, with summation over the indices 5 and 6 of the 6th order tensor and the first two indices of the 4th order tensor
    for (int i=0; i<3; i++){
      for (int j=0; j<3; j++){
        for (int k=0; k<3; k++){
          for (int l=0; l<3; l++){
            for (int m=0; m<3; m++){
              for (int n=0; n<3; n++){
                c(i,j,k,l,m,n) = 0.;
                for (int p=0; p<3; p++){
                  for (int q=0; q<3; q++){
                    c(i,j,k,l,m,n) += a(i,j,k,l,p,q)*b(p,q,m,n);
                  }
                }
              }
            }
          }
        }
      }
    }
  };


  inline void multSTensor63_ind56_STensor43_ind34(const STensor63& a, const STensor43& b, STensor63& c){
  //multiplication of 6th order and 4th order tensors, with summation over the indices 5 and 6 of the 6th order tensor and the last two indices of the 4th order tensor
    for (int i=0; i<3; i++){
      for (int j=0; j<3; j++){
        for (int k=0; k<3; k++){
          for (int l=0; l<3; l++){
            for (int m=0; m<3; m++){
              for (int n=0; n<3; n++){
                c(i,j,k,l,m,n) = 0.;
                for (int p=0; p<3; p++){
                  for (int q=0; q<3; q++){
                    c(i,j,k,l,m,n) += a(i,j,k,l,p,q)*b(m,n,p,q);
                  }
                }
              }
            }
          }
        }
      }
    }
  };


  inline void multSTensor63_ind12_STensor43_ind12(const STensor63& a, const STensor43& b, STensor63& c){
    //multiplication of 6th order and 4th order tensors, with summation over the indices 1 and 2 of the 6th order tensor and the first two indices of the 4th order tensor
    for (int i=0; i<3; i++){
      for (int j=0; j<3; j++){
        for (int k=0; k<3; k++){
          for (int l=0; l<3; l++){
            for (int m=0; m<3; m++){
              for (int n=0; n<3; n++){
                c(m,n,i,j,k,l) = 0.;
                for (int p=0; p<3; p++){
                  for (int q=0; q<3; q++){
                    c(m,n,i,j,k,l) += a(p,q,i,j,k,l)*b(p,q,m,n);
                  }
                }
              }
            }
          }
        }
      }
    }
  };

  inline void multSTensor63_ind12_STensor43_ind34(const STensor63& a, const STensor43& b, STensor63& c){
    //multiplication of 6th order and 4th order tensors, with summation over the indices 1 and 2 of the 6th order tensor and the last two indices of the 4th order tensor
    for (int i=0; i<3; i++){
      for (int j=0; j<3; j++){
        for (int k=0; k<3; k++){
          for (int l=0; l<3; l++){
            for (int m=0; m<3; m++){
              for (int n=0; n<3; n++){
                c(m,n,i,j,k,l) = 0.;
                for (int p=0; p<3; p++){
                  for (int q=0; q<3; q++){
                    c(m,n,i,j,k,l) += a(p,q,i,j,k,l)*b(m,n,p,q);
                  }
                }
              }
            }
          }
        }
      }
    }
  };



  inline void multSTensor63_ind34_STensor43_ind12(const STensor63& a, const STensor43& b, STensor63& c){
    //multiplication of 6th order and 4th order tensors, with summation over the indices 3 and 4 of the 6th order tensor and the first two indices of the 4th order tensor
    for (int i=0; i<3; i++){
      for (int j=0; j<3; j++){
        for (int k=0; k<3; k++){
          for (int l=0; l<3; l++){
            for (int m=0; m<3; m++){
              for (int n=0; n<3; n++){
                c(i,j,m,n,k,l) = 0.;
                for (int p=0; p<3; p++){
                  for (int q=0; q<3; q++){
                    c(i,j,m,n,k,l) += a(i,j,p,q,k,l)*b(p,q,m,n);
                  }
                }
              }
            }
          }
        }
      }
    }
  };

  inline void multSTensor63_ind34_STensor43_ind34(const STensor63& a, const STensor43& b, STensor63& c){
    //multiplication of 6th order and 4th order tensors, with summation over the indices 3 and 4 of the 6th order tensor and the last two indices of the 4th order tensor
    for (int i=0; i<3; i++){
      for (int j=0; j<3; j++){
        for (int k=0; k<3; k++){
          for (int l=0; l<3; l++){
            for (int m=0; m<3; m++){
              for (int n=0; n<3; n++){
                c(i,j,m,n,k,l) = 0.;
                for (int p=0; p<3; p++){
                  for (int q=0; q<3; q++){
                    c(i,j,m,n,k,l) += a(i,j,p,q,k,l)*b(m,n,p,q);
                  }
                }
              }
            }
          }
        }
      }
    }
  };
  inline void multSTensor43STensor63( const STensor43& a, const STensor63& b, STensor63& c){
    //multiplication of 6th order and 4th order tensors, with summation over the indices 3 and 4 of the 6th order tensor and the last two indices of the 4th order tensor
    for (int i=0; i<3; i++){
      for (int j=0; j<3; j++){
        for (int k=0; k<3; k++){
          for (int l=0; l<3; l++){
            for (int m=0; m<3; m++){
              for (int n=0; n<3; n++){
                c(i,j,m,n,k,l) = 0.;
                for (int p=0; p<3; p++){
                  for (int q=0; q<3; q++){
                    c(i,j,m,n,k,l) += a(i,j,p,q)*b(p,q,m,n,k,l);
                  }
                }
              }
            }
          }
        }
      }
    }
  };

  inline void multSTensor63_ind56_STensor3(const STensor63& a, const STensor3& b, STensor43& c){
    for (int i=0; i<3; i++){
      for (int j=0; j<3; j++){
        for (int k=0; k<3; k++){
          for (int l=0; l<3; l++){
            c(i,j,k,l) = 0.;
            for (int p=0; p<3; p++){
              for (int q=0; q<3; q++){
                c(i,j,k,l) += a(i,j,k,l,p,q)*b(p,q);
              }
            }
          }
        }
      }
    }
  };


  inline void multSTensor63_ind12_STensor3(const STensor63& a, const STensor3& b, STensor43& c){
    for (int i=0; i<3; i++){
      for (int j=0; j<3; j++){
        for (int k=0; k<3; k++){
          for (int l=0; l<3; l++){
            c(i,j,k,l) = 0.;
            for (int p=0; p<3; p++){
              for (int q=0; q<3; q++){
                c(i,j,k,l) += a(p,q,i,j,k,l)*b(p,q);
              }
            }
          }
        }
      }
    }
  };

  inline void multSTensor63_ind34_STensor3(const STensor63& a, const STensor3& b, STensor43& c){
    for (int i=0; i<3; i++){
      for (int j=0; j<3; j++){
        for (int k=0; k<3; k++){
          for (int l=0; l<3; l++){
            c(i,j,k,l) = 0.;
            for (int p=0; p<3; p++){
              for (int q=0; q<3; q++){
                c(i,j,k,l) += a(i,j,p,q,k,l)*b(p,q);
              }
            }
          }
        }
      }
    }
  };
  inline void multSTensor63_ind34_STensor3_Add(const STensor63& a, const STensor3& b, STensor43& c){
    for (int i=0; i<3; i++){
      for (int j=0; j<3; j++){
        for (int k=0; k<3; k++){
          for (int l=0; l<3; l++){
            for (int p=0; p<3; p++){
              for (int q=0; q<3; q++){
                c(i,j,k,l) += a(i,j,p,q,k,l)*b(p,q);
              }
            }
          }
        }
      }
    }
  };
  inline void multSTensor43Add(const STensor43& a, const STensor43& b, const double alpha, STensor43& c){
    for (int i=0; i<3; i++){
      for (int j=0; j<3; j++){
        for (int k=0; k<3; k++){
          for (int l=0; l<3; l++){
            for (int p=0; p<3; p++){
              for (int q=0; q<3; q++){
                c(i,j,k,l) += alpha*a(i,j,p,q)*b(p,q,k,l);
              }
            }
          }
        }
      }
    }
  };

  inline void multSTensor43FirstTranspose(const STensor43& a, const STensor43& b, STensor43& c){
    for (int i=0; i<3; i++){
      for (int j=0; j<3; j++){
        for (int k=0; k<3; k++){
          for (int l=0; l<3; l++){
            c(i,j,k,l) = 0.;
            for (int p=0; p<3; p++){
              for (int q=0; q<3; q++){
                c(i,j,k,l) += a(p,q,i,j)*b(p,q,k,l);
              }
            }
          }
        }
      }
    }
  };


  inline void multSTensor43STensor3(const STensor43& a, const STensor3& b, STensor3& c, double fact = 1.){
    for (int i=0; i<3; i++){
      for (int j=0; j<3; j++){
        c(i,j) = 0.;
        for (int k=0; k<3; k++){
          for (int l=0; l<3; l++){
            c(i,j) += fact*a(i,j,k,l)*b(k,l);
          }
        }
      }
    }
  };

  inline void multSTensor43STensor33(const STensor43& a, const STensor33& b, STensor33& c, double fact = 1.){
    for (int i=0; i<3; i++){
      for (int j=0; j<3; j++){
        for (int p=0; p<3; p++)
        {
          c(i,j,p) = 0.;
          for (int k=0; k<3; k++){
            for (int l=0; l<3; l++){
              c(i,j,p) += fact*a(i,j,k,l)*b(k,l,p);
            }
          }
        }
      }
    }
  };

  inline void multSTensor43STensor33RightTranspose(const STensor43& a, const STensor33& b, STensor33& c, double fact = 1.){
    for (int i=0; i<3; i++){
      for (int j=0; j<3; j++){
        for (int p=0; p<3; p++)
        {
          c(i,j,p) = 0.;
          for (int k=0; k<3; k++){
            for (int l=0; l<3; l++){
              c(i,j,p) += fact*a(i,j,k,l)*b(k,p,l);
            }
          }
        }
      }
    }
  };

  inline void multSTensor43STensor3Add(const STensor43& a, const STensor3& b, double fact, STensor3& c){
    for (int i=0; i<3; i++){
      for (int j=0; j<3; j++){
        for (int k=0; k<3; k++){
          for (int l=0; l<3; l++){
            c(i,j) += fact*a(i,j,k,l)*b(k,l);
          }
        }
      }
    }
  };

  inline void multSTensor43STensor3SecondTranspose(const STensor43& a, const STensor3& b, STensor3& c){
    for (int i=0; i<3; i++){
      for (int j=0; j<3; j++){
        c(i,j) = 0.;
        for (int k=0; k<3; k++){
          for (int l=0; l<3; l++){
            c(i,j) += a(i,j,k,l)*b(l,k);
          }
        }
      }
    }
  };

  inline void multSTensor3SVector3(const STensor3& a, const SVector3& b, SVector3& c){
    for (int i=0; i<3; i++){
      c(i) = 0.;
      for (int j=0; j<3; j++){
        c(i) += a(i,j)*b(j);
      }
    }
  };
   inline void multSTensor33SVector3(const STensor33& a, const SVector3& b, STensor3& c, double fact =1.){
    for (int i=0; i<3; i++)
    {
      for (int p=0; p<3; p++)
      {
        c(i,p) = 0.;
        for (int j=0; j<3; j++){
          c(i,p) += fact*a(i,p,j)*b(j);
        }
      }
    }
  };

  inline void multSVector3STensor3(const SVector3& a, const STensor3& b,  SVector3& c){
    for (int i=0; i<3; i++){
      c(i) = 0.;
      for (int j=0; j<3; j++){
        c(i) += a(j)*b(j,i);
      }
    }
  };




  inline void TensorProdSTensor43STensor3(const STensor43& a, const STensor3& b, STensor63& c){

    for (int i=0; i<3; i++){
      for (int j=0; j<3; j++){
        for (int k=0; k<3; k++){
          for (int l=0; l<3; l++){
            for (int m=0; m<3; m++){
              for (int n=0; n<3; n++){
                c(i,j,k,l,m,n) = a(i,j,k,l)*b(m,n);
              }
            }
          }
        }
      }
    }



  };

  inline void TensorProdSTensor3STensor43(const STensor43& a, const STensor3& b, STensor63& c){

    for (int i=0; i<3; i++){
      for (int j=0; j<3; j++){
        for (int k=0; k<3; k++){
          for (int l=0; l<3; l++){
            for (int m=0; m<3; m++){
              for (int n=0; n<3; n++){
                c(i,j,k,l,m,n) = b(i,j)*a(k,l,m,n);
              }
            }
          }
        }
      }
    }



  };



  inline void derivTensorProdSTensor43STensor3(const STensor43& dNdeps, const STensor3& N, STensor63& M){

    for (int i=0; i<3; i++){
      for (int j=0; j<3; j++){
        for (int k=0; k<3; k++){
          for (int l=0; l<3; l++){
            for (int m=0; m<3; m++){
              for (int n=0; n<3; n++){
                M(i,j,k,l,m,n) = dNdeps(i,j,m,n)*N(k,l) + dNdeps(k,l,m,n)*N(i,j);
              }
            }
          }
        }
      }
    }



  };



  inline void STensor43Isotropization(const STensor43& a, STensor43& a_iso){
    STensor43 I_vol, I_dev;
    sphericalfunction(I_vol);
    deviatorfunction(I_dev);

    double a_iso_vol_fac,a_iso_dev_fac;
    STensorOperation::contract4STensor43(I_vol,a,a_iso_vol_fac);
    STensorOperation::contract4STensor43(I_dev,a,a_iso_dev_fac);

    STensor43 a_iso_vol(I_vol);
    a_iso_vol *= a_iso_vol_fac;

    a_iso=I_dev;
    a_iso *= 1./5.*a_iso_dev_fac;
    a_iso += a_iso_vol;
  };


  inline void STensor43IsotropizationDerivBySTensor3(const STensor63& a_deriv, STensor63& a_iso_deriv){
    STensor3 sphere_scalar_derivative, deviator_scalar_derivative;
    STensor43 I_vol, I_dev;
    sphericalfunction(I_vol);
    deviatorfunction(I_dev);
    STensor63 sphere_derivative;

    contract4STensor43STensor63(I_vol,a_deriv,sphere_scalar_derivative);
    contract4STensor43STensor63(I_dev,a_deriv,deviator_scalar_derivative);

    TensorProdSTensor43STensor3(I_vol,sphere_scalar_derivative,sphere_derivative);
    TensorProdSTensor43STensor3(I_dev,deviator_scalar_derivative,a_iso_deriv);
    a_iso_deriv *= 1./5.;
    a_iso_deriv += sphere_derivative;
  };

  inline void STensor43Isotropization_Norris(const STensor43& a, STensor43& a_iso){
    STensor43 I_vol, I_dev;
    sphericalfunction(I_vol);
    deviatorfunction(I_dev);

    double a_iso_vol_fac,a_iso_dev_fac;
    STensor43 t1, t2;
    multSTensor43(a,I_vol,t1);
    a_iso_vol_fac = trace(t1);
    multSTensor43(a,I_dev,t2);
    a_iso_dev_fac = trace(t2);

    STensor43 a_iso_vol(I_vol);
    a_iso_vol *= a_iso_vol_fac;

    a_iso=I_dev;
    a_iso *= 1./5.*a_iso_dev_fac;
    a_iso += a_iso_vol;
  };

  inline void STensor43TransverseIsotropization_Norris(const int& p, const STensor43& A, STensor43& A_transverseIso){
    // Norris, 2018; Appendix A.1.4
    fullMatrix<double> Amat(6,6), A_transverseIso_mat(6,6);
    fromSTensor43ToFullMatrix(A,Amat);
    if(p==2)
    {
    A_transverseIso_mat(0,0) = A_transverseIso_mat(1,1) = Amat(0,0);
    A_transverseIso_mat(2,2) = Amat(2,2);
    //A_transverseIso_mat(3,3) = A_transverseIso_mat(4,4) = (Amat(3,3) + Amat(4,4))/2.;
    //A_transverseIso_mat(5,5) = Amat(5,5);
    A_transverseIso_mat(3,3) = Amat(3,3);
    A_transverseIso_mat(4,4) = A_transverseIso_mat(5,5) = (Amat(4,4) + Amat(5,5))/2.;
    //A_transverseIso_mat(0,1) = A_transverseIso_mat(1,0) = Amat(0,0) - Amat(5,5);
    A_transverseIso_mat(0,1) = A_transverseIso_mat(1,0) = Amat(0,0) - Amat(3,3);
    A_transverseIso_mat(0,2) = A_transverseIso_mat(2,0) = A_transverseIso_mat(1,2) = A_transverseIso_mat(2,1) = (Amat(0,2) + Amat(1,2))/2.;
    }
    else if(p==1)
    {
    A_transverseIso_mat(1,1) = Amat(1,1);
    A_transverseIso_mat(0,0) = A_transverseIso_mat(2,2) = Amat(0,0);
    A_transverseIso_mat(4,4) = Amat(4,4);
    A_transverseIso_mat(3,3) = A_transverseIso_mat(5,5) = (Amat(3,3) + Amat(5,5))/2.;
    A_transverseIso_mat(0,2) = A_transverseIso_mat(2,0) = Amat(0,0) - Amat(4,4);
    A_transverseIso_mat(0,1) = A_transverseIso_mat(1,0) = A_transverseIso_mat(1,2) = A_transverseIso_mat(2,1) = (Amat(0,1) + Amat(1,2))/2.;
    }
    else if(p==0)
    {
    A_transverseIso_mat(0,0) = Amat(0,0);
    A_transverseIso_mat(2,2) = A_transverseIso_mat(1,1) = Amat(1,1);
    A_transverseIso_mat(5,5) = Amat(5,5);
    A_transverseIso_mat(4,4) = A_transverseIso_mat(3,3) = (Amat(4,4) + Amat(3,3))/2.;
    A_transverseIso_mat(2,1) = A_transverseIso_mat(1,2) = Amat(1,1) - Amat(5,5);
    A_transverseIso_mat(0,2) = A_transverseIso_mat(2,0) = A_transverseIso_mat(1,0) = A_transverseIso_mat(0,1) = (Amat(0,2) + Amat(1,0))/2.;
    }
    fromFullMatrixToSTensor43(A_transverseIso_mat,A_transverseIso);
  };

  inline void getThreeInvariantsSTensor3(const STensor3& a, double& I1, double& I2, double& I3){
    I1 = trace(a);
    static STensor3 aa;
    multSTensor3(a,a,aa);
    I2 = 0.5*(I1*I1-trace(aa));
    I3 = determinantSTensor3(a);
  };

  //! Compute the Von Mises equivalent stress from the (stress) tensor.
  inline void getVonMisesStressFromSTensor3( const STensor3& a, double& vonMisesStress ){
    double a0m1 = a(0,0) - a(1,1);
    double a1m2 = a(1,1) - a(2,2);
    double a2m0 = a(2,2) - a(0,0);
    vonMisesStress = sqrt( a0m1*a0m1 + a1m2*a1m2 + a2m0*a2m0 + 6.0*( a(0,1)*a(0,1) + a(1,2)*a(1,2) + a(0,2)*a(0,2) ));

  };

  //! Compute the Von Mises equivalent stress from the DEVIATORIC (stress) tensor.
  inline void getVonMisesStressFromDevSTensor3( const STensor3& a_dev, double& vonMisesStress ){
    vonMisesStress = sqrt( 1.5*(   a_dev(0,0)*a_dev(0,0) + a_dev(0,1)*a_dev(0,1) + a_dev(0,2)*a_dev(0,2)
                                 + a_dev(1,0)*a_dev(1,0) + a_dev(1,1)*a_dev(1,1) + a_dev(1,2)*a_dev(1,2)
                                 + a_dev(2,0)*a_dev(2,0) + a_dev(2,1)*a_dev(2,1) + a_dev(2,2)*a_dev(2,2)
                                )
                          );
  };

  //! Compute the J3 and the Lode variable (xi) from the DEVIATORIC (stress) tensor and the Von Mises stress.
  inline void getJ3AndLodeVariableFromDevSTensor3( const STensor3& a_dev, const double& vm_stress, double& J3, double& lodeVar ){
    J3 = STensorOperation::determinantSTensor3(a_dev);
    lodeVar = 27.0 * J3 / (2.0 * vm_stress * vm_stress * vm_stress);
  };

  //! Compute the von Mises stress, the J3 and then the Lode variable (xi) from the DEVIATORIC (stress) tensor.
  inline void getDevInvariantsFromDevSTensor3( const STensor3& a_dev, double& vm_stress, double& J3, double& lodeVar ){
    STensorOperation::getVonMisesStressFromDevSTensor3( a_dev, vm_stress );
    STensorOperation::getJ3AndLodeVariableFromDevSTensor3( a_dev, vm_stress, J3, lodeVar );
  };


  inline void getEigenDecomposition(const STensor3& a, double& x1, double& x2, double& x3, STensor3& E1, STensor3& E2, STensor3& E3)
  {

    static fullMatrix<double> m(3, 3);
    static fullVector<double> eigenValReal(3);
    static fullVector<double> eigenValImag(3);
    static fullMatrix<double> leftEigenVect(3,3);
    static fullMatrix<double> rightEigenVect(3,3);
    m.setAll(0.);
    eigenValReal.setAll(0.);
    eigenValImag.setAll(0.);
    leftEigenVect.setAll(0.);
    rightEigenVect.setAll(0.);

    // Get eigen values and vectors
    a.getMat(m);
    m.eig(eigenValReal,eigenValImag,leftEigenVect,rightEigenVect,false);
    x1=eigenValReal(0);
    x2=eigenValReal(1);
    x3=eigenValReal(2);
    for(int i=0; i<3; i++)
    {
      for(int j=0; j<3; j++)
      {
        E1(i,j)= leftEigenVect(i,0)*rightEigenVect(j,0);
        E2(i,j)= leftEigenVect(i,1)*rightEigenVect(j,1);
        E3(i,j)= leftEigenVect(i,2)*rightEigenVect(j,2);
      }
    }
  }

  inline void getBasisRotationTensor(const STensor3& A, const STensor3& B, STensor3& R){

	// Only for single-legged symmetric tensors A and B -> leftEigenVect = rightEigenVect and eigenvals are always real
	// R rotates the bases of A to that of B, so R = R_(ba), i.e., R = sum (eb * ea)
	// B = R*A*Rt

    static fullMatrix<double> m1(3, 3), m2(3, 3);
    static fullVector<double> eigenValReal1(3), eigenValReal2(3);
    static fullVector<double> eigenValImag1(3), eigenValImag2(3);
    static fullMatrix<double> leftEigenVect1(3,3), leftEigenVect2(3,3);
    static fullMatrix<double> rightEigenVect1(3,3), rightEigenVect2(3,3);
    m1.setAll(0.); m2.setAll(0.);
    eigenValReal1.setAll(0.); eigenValReal2.setAll(0.);
    eigenValImag1.setAll(0.); eigenValImag2.setAll(0.);
    leftEigenVect1.setAll(0.); leftEigenVect2.setAll(0.);
    rightEigenVect1.setAll(0.); rightEigenVect2.setAll(0.);

    // Get eigen values and vectors
    A.getMat(m1); B.getMat(m2);
    m1.eig(eigenValReal1,eigenValImag1,leftEigenVect1,rightEigenVect1,false);
    m2.eig(eigenValReal2,eigenValImag2,leftEigenVect2,rightEigenVect2,false);

    zero(R);
    for(int i=0; i<3; i++)
      for(int j=0; j<3; j++)
        R(i,j) += rightEigenVect2(i,0)*rightEigenVect1(j,0) + rightEigenVect2(i,1)*rightEigenVect1(j,1) + rightEigenVect2(i,2)*rightEigenVect1(j,2);
  }

  void getBasisRotationTensor(const STensor3& A, const STensor3& B, STensor3& R, STensor43& dRdB, STensor43& dRtdB);

  void getEigenDecomposition(const STensor3& a, double& x1, double& x2, double& x3, STensor3& E1, STensor3& E2, STensor3& E3,
                                    STensor3& dx1da, STensor3& dx2da, STensor3& dx3da, STensor43& dE1da, STensor43& dE2da, STensor43& dE3da);

  // NEW
  void getEigenDecomposition(const STensor3& a, double& x1, double& x2, double& x3, STensor3& E1, STensor3& E2, STensor3& E3,
		  fullVector<double> &eigenValReal, fullMatrix<double>& leftEigenVect, fullMatrix<double>& rightEigenVect);

  // NEW
  void alignEigenDecomposition_NormBased_withDerivative(const STensor3& A, const STensor3& B, STensor3& Bs, STensor43& dBsdA);

  // NEW
  void alignEigenDecomposition_NormBased_withDerivative_Vector(const STensor3& A, const std::vector<STensor3>& Bi, std::vector<STensor3>& Bi_s, std::vector<STensor43>& dBi_sdA);

  // NEW
  void alignEigenDecomposition_NormBased(const STensor3& A, const STensor3& B, const STensor3& A1, const STensor3& A2, const STensor3& A3,
  								const fullVector<double> &eigenValReal1, const fullVector<double> &eigenValReal2,
  		  	  	  	  	  	  	const fullMatrix<double>& rightEigenVect1, const fullMatrix<double>& rightEigenVect2,
  								double& b1_align, double& b2_align, double& b3_align, fullMatrix<double>& rightEigenVect2_align, int& align);

  void alignEigenDecomposition_NormBased(const STensor3& A, const STensor3& B, const fullVector<double> &eigenValReal1, const fullVector<double> &eigenValReal2,
  		  	  	  	  	  	  	  	  const fullMatrix<double>& rightEigenVect1, const fullMatrix<double>& rightEigenVect2,
  									  double& a1_align, double& a2_align, double& a3_align, fullMatrix<double>& rightEigenVect, int& align);

  void alignEigenDecomposition_NormBased_indexOnly(const STensor3& A, const STensor3& B, fullVector<int>& alignedIndex);

  inline void subMember_getEigenDecomposition_getDi(const double& xi, const double& tr, const double& det, const STensor3& dxidC, const STensor3& dI3dC, double& Di, STensor3& dDidC){
	  // C is 42 tensor
	  Di = 2*pow(xi,2) - tr*xi + det/xi;

	  STensor3 I(1.);

	  zero(dDidC);
	  for(int i=0; i<3; i++)
		  for(int j=0; j<3; j++)
			  dDidC(i,j) += 4*xi*dxidC(i,j) - xi*I(i,j) - tr*dxidC(i,j) + dI3dC(i,j)*1/xi - (det/pow(xi,2))*dxidC(i,j);
  }

  inline void subMember_getEigenDecomposition_getdEidC(const STensor3& Ei, const double& xi, const double& det, const double& Di, const STensor3& Cinv, const STensor3& dxidC, const STensor43& dCinvdC,
		  	  	  	  	  	  	  	  	  	  	  const STensor3& dI3dC, const STensor3& dDidC, STensor43& dEidC){
	  // C is 42 tensor
	  STensor3 I(1.);
	  STensor43 I4(1.,1.);

	  zero(dEidC);
	  if(abs(xi)>0. && abs(Di)>0.){
		  for(int i=0; i<3; i++)
			  for(int j=0; j<3; j++)
				  for (int k=0; k<3; k++)
					  for (int l=0; l<3; l++){
						  dEidC(i,j,k,l) += Ei(i,j)*dxidC(k,l)/xi
								  + xi/Di*(I4(i,j,k,l) - I(i,j)*I(k,l) + I(i,j)*dxidC(k,l) + det*(1/xi*dCinvdC(i,j,k,l) - (1/pow(xi,2))*Cinv(i,j)*dxidC(k,l)) + 1/xi * Cinv(i,j)*dI3dC(k,l))
					  	  	  	  	  - Ei(i,j)/Di*dDidC(k,l);
					  }
	  }
  }

  inline void getEigenDecomposition_getdeidC(const fullMatrix<double>& rightEigenVect, const double& x0, const double& x1, const double& x2, STensor33& de0dC, STensor33& de1dC, STensor33& de2dC){
	  // C is 42 tensor

	  zero(de0dC); zero(de1dC); zero(de2dC);
	  for(int i=0; i<3; i++)
	    for (int k=0; k<3; k++)
		  for (int l=0; l<3; l++){

			 if (x0!=x1){
				 de0dC(i,k,l) += 1/(x0-x1)/2.*(rightEigenVect(l,0)*rightEigenVect(k,1)+rightEigenVect(k,0)*rightEigenVect(l,1))*rightEigenVect(i,1);
				 de1dC(i,k,l) += 1/(x1-x0)/2.*(rightEigenVect(l,1)*rightEigenVect(k,0)+rightEigenVect(k,1)*rightEigenVect(l,0))*rightEigenVect(i,0);
			 }

			 if (x0!=x2){
				 de0dC(i,k,l) += 1/(x0-x2)/2.*(rightEigenVect(l,0)*rightEigenVect(k,2)+rightEigenVect(k,0)*rightEigenVect(l,2))*rightEigenVect(i,2);
				 de2dC(i,k,l) += 1/(x2-x0)/2.*(rightEigenVect(l,2)*rightEigenVect(k,0)+rightEigenVect(k,2)*rightEigenVect(l,0))*rightEigenVect(i,0);
			 }

			 if (x1!=x2){
				 de1dC(i,k,l) += 1/(x1-x2)/2.*(rightEigenVect(l,1)*rightEigenVect(k,2)+rightEigenVect(k,1)*rightEigenVect(l,2))*rightEigenVect(i,2);
				 de2dC(i,k,l) += 1/(x2-x1)/2.*(rightEigenVect(l,2)*rightEigenVect(k,1)+rightEigenVect(k,2)*rightEigenVect(l,1))*rightEigenVect(i,1);
			 }
	      }
  }

  inline void getEigenDecompositionExplicitBase(const STensor3& a, double& x1, double& x2, double& x3, STensor3& E1, STensor3& E2, STensor3& E3)
  {
    // a = x1*E1+x2*E2+x3*E3
    // if eigven values xi are duplicated, its related Ei is not vi\timesvi

    double I1,I2,I3;
    getThreeInvariantsSTensor3(a,I1,I2,I3);

    double Q = (I1*I1 - 3.*I2)/9.;
    if (Q >0.)
    {
      double R = (-2.*I1*I1*I1 +9.*I1*I2-27.*I3)/54.;
      double ff = R/(Q*sqrt(Q));
      if (ff < -1.)
      {
        ff = -1.;
      }
      else if (ff >1)
      {
        ff = 1.;
      }
      double theta = acos(ff);
      double sqrtQ = sqrt(Q);
      double pi = 3.14159265358979323846;
      x1 = -2.*sqrtQ*cos(theta/3.)+I1/3.;
      x2 = -2.*sqrtQ*cos((theta+2.*pi)/3.)+I1/3.;
      x3 = -2.*sqrtQ*cos((theta-2.*pi)/3.)+I1/3.;
    }
    else
    {
      x1 = I1/3.;
      x2 = I1/3.;
      x3 = I1/3.;
    }
    static STensor3 I(1.);
    if ((x1==x2) and (x1 == x3))
    {
      E1 = I;
      E1 *= (1./3);
      E2 = E1;
      E3 = E1;
    }
    else if ((x1==x2) and (x3!=x2))
    {
      for (int i=0; i< 3; i++)
      {
        for (int j=0; j<3; j++)
        {
          E3(i,j) = 0.;
          for (int k=0; k<3; k++)
          {
            E3(i,j) += (a(i,k)-x2*I(i,k))*(a(k,j)-x1*I(k,j))/(x3-x1)/(x3-x2);
          }
        }
      }
      zero(E2);
      E2.daxpy(I,0.5);
      E2.daxpy(E3,-0.5);
      E1 = E2;
    }
    else if ((x1 == x3) and (x2!=x1))
    {
      for (int i=0; i< 3; i++)
      {
        for (int j=0; j<3; j++)
        {
          E2(i,j) = 0.;
          for (int k=0; k<3; k++)
          {
            E2(i,j) += (a(i,k)-x1*I(i,k))*(a(k,j)-x3*I(k,j))/(x2-x1)/(x2-x3);
          }
        }
      }
      zero(E3);
      E3.daxpy(I,0.5);
      E3.daxpy(E2,-0.5);
      E1 = E3;
    }
    else if ((x2 == x3) and (x1!=x2))
    {
      for (int i=0; i< 3; i++)
      {
        for (int j=0; j<3; j++)
        {
          E1(i,j) = 0.;
          for (int k=0; k<3; k++)
          {
            E1(i,j) += (a(i,k)-x2*I(i,k))*(a(k,j)-x3*I(k,j))/(x1-x2)/(x1-x3);
          }
        }
      }
      zero(E2);
      E2.daxpy(I,0.5);
      E2.daxpy(E1,-0.5);
      E3 = E2;
    }
    else
    {
      for (int i=0; i< 3; i++)
      {
        for (int j=0; j<3; j++)
        {
          E1(i,j) = 0.;
          E2(i,j) = 0.;
          E3(i,j) = 0.;
          for (int k=0; k<3; k++)
          {
            E1(i,j) += (a(i,k)-x2*I(i,k))*(a(k,j)-x3*I(k,j))/(x1-x2)/(x1-x3);
            E2(i,j) += (a(i,k)-x1*I(i,k))*(a(k,j)-x3*I(k,j))/(x2-x1)/(x2-x3);
            E3(i,j) += (a(i,k)-x2*I(i,k))*(a(k,j)-x1*I(k,j))/(x3-x1)/(x3-x2);
          }
        }
      }
    }
  }

  inline bool sqrtSTensor3(const STensor3& a, STensor3& sqrta)
  {
    static fullMatrix<double> m(3, 3);
    static fullVector<double> eigenValReal(3);
    static fullVector<double> eigenValImag(3);
    static fullMatrix<double> leftEigenVect(3,3);
    static fullMatrix<double> rightEigenVect(3,3);
    a.getMat(m);
    eigenValReal.setAll(0.);
    eigenValImag.setAll(0.);
    leftEigenVect.setAll(0.);
    rightEigenVect.setAll(0.);
    // Get eigen values and vectors
    bool ok = m.eig(eigenValReal,eigenValImag,leftEigenVect,rightEigenVect,false);
    if (not(ok))
    {
      #ifdef _DEBUG
      Msg::Error("fullMatrix::eig fails");
      #endif
      return false;
    }
    static fullMatrix<double> rightEigenVectInv(3,3);
    rightEigenVect.invert(rightEigenVectInv);
    double normEig = eigenValReal.norm();
    static fullMatrix<double> D(3,3);
    D.setAll(0.);
    for (int i=0; i< 3; i++)
    {
      if (fabs(eigenValReal(i)) < 1e-8*normEig)
      {
        eigenValReal(i) = 0;
      }
      else if (eigenValReal(i) < 0)
      {
        #ifdef _DEBUG
        Msg::Error("negative eigven value: x[%d]=%e",i,eigenValReal(i));
        #endif
        return false;
      }
      D(i,i) = sqrt(eigenValReal(i));
    }
    static fullMatrix<double> temp(3,3), sqrtm(3, 3);
    rightEigenVect.mult(D, temp);
    temp.mult(rightEigenVectInv, sqrtm);
    sqrta.setMat(sqrtm);
    return true;
  };

  inline bool sqrtSTensor43(const STensor43& a, STensor43& sqrta)
  {
    static fullMatrix<double> m(6, 6);
    static fullVector<double> eigenValReal(6);
    static fullVector<double> eigenValImag(6);
    static fullMatrix<double> leftEigenVect(6,6);
    static fullMatrix<double> rightEigenVect(6,6);
    m.setAll(0.);
    eigenValReal.setAll(0.);
    eigenValImag.setAll(0.);
    leftEigenVect.setAll(0.);
    rightEigenVect.setAll(0.);
    fromSTensor43ToFullMatrix_ConsistentForm(a,m);
    // Get eigen values and vectors
    bool ok = m.eig(eigenValReal,eigenValImag,leftEigenVect,rightEigenVect,false);
    if (not(ok))
    {
      #ifdef _DEBUG
      Msg::Error("fullMatrix::eig fails");
      #endif
      return false;
    }
    static fullMatrix<double> rightEigenVectInv(6,6);
    rightEigenVect.invert(rightEigenVectInv);
    double normEig = eigenValReal.norm();
    static fullMatrix<double> D(6,6);
    D.setAll(0.);
    for (int i=0; i< 6; i++)
    {
      if (fabs(eigenValReal(i)) < 1e-8*normEig)
      {
        eigenValReal(i) = 0;
      }
      else if (eigenValReal(i) < 0)
      {
        #ifdef _DEBUG
        Msg::Error("negative eigven value: x[%d]=%e",i,eigenValReal(i));
        #endif
        return false;
      }
      D(i,i) = sqrt(eigenValReal(i));
    }
    static fullMatrix<double> temp(6,6), sqrtm(6, 6);
    rightEigenVect.mult(D, temp);
    temp.mult(rightEigenVectInv, sqrtm);
    fromFullMatrixToSTensor43_ConsistentForm(sqrtm, sqrta);
    return true;
  };

  // F = sum_{i=0, order) coeffs[i]*a^i
  bool polynomial(const STensor3& a, const int order, const fullVector<double>& coeffs, STensor3& F, STensor43* dF= NULL, STensor63* ddF=NULL);
  bool logSTensor3(const STensor3& a, const int order, STensor3& loga, STensor43* dloga = NULL, STensor63* ddloga = NULL);
  bool expSTensor3(const STensor3& a, const int order, STensor3& expa, STensor43* dexpa = NULL, STensor63* ddexpa = NULL);

  void VRDecomposition(const STensor3& a, STensor3& V, STensor3&R);
  void RUDecomposition(const STensor3& a, STensor3& U, STensor3&R);


  inline void Ddeta_Da_sym(const STensor3& a, const double deta, STensor3& Ddeta_Da){
    // with a symetric
    // D(det(a))Da = det(a)*inv(a) = a*a - a*tr(a) + 0.5*(tr(a)*tr(a) - tr(a*a))I

    multSTensor3(a,a,Ddeta_Da);
    double tr_a = trace(a);
    double tr_aa = trace(Ddeta_Da);

    Ddeta_Da.daxpy(a,-tr_a);
    double factI = 0.5*(tr_a*tr_a -tr_aa);
    Ddeta_Da(0,0) += factI;
    Ddeta_Da(1,1) += factI;
    Ddeta_Da(2,2) += factI;
  };

};

#endif // STENSOROPERATIONS_H_
