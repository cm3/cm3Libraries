//
// Description: Define mullins effect for softening in polymers in (pseudo-)hyperelastic regime 
//
//
// Author:  <Ujwal Kishore J.>, (C) 2023
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef MULLINSEFFECT_H_
#define MULLINSEFFECT_H_

#include "ipMullinsEffect.h"
#include "fullMatrix.h"
#include "scalarFunction.h"

class mullinsEffect {
  public:
    enum scalingFunction{negativeExponentialScaler,hyperbolicScaler,linearScaler,positiveExponentialScaler};

  protected:
    int _num; // number of law (must be unique !)
    bool _initialized; // to initialize law

  public:
  #ifndef SWIG
    mullinsEffect(const int num, const bool init=true);
    virtual ~mullinsEffect(){}
    mullinsEffect(const mullinsEffect &source);
    virtual mullinsEffect& operator=(const mullinsEffect &source);
    virtual int getNum() const{return _num;}
    virtual scalingFunction getType() const=0;
    virtual void createIPVariable(IPMullinsEffect* &ipv) const=0;
    virtual void initLaws(const std::map<int,mullinsEffect*> &maplaw)=0;
    virtual const bool isInitialized() {return _initialized;}
    virtual void mullinsEffectScaling(double _psi0, double _psi, const IPMullinsEffect &ipvprev, IPMullinsEffect &ipv) const=0;
    virtual void mullinsEffectScaling(double _psi0, double _psi, const IPMullinsEffect &ipvprev, IPMullinsEffect &ipv, const double T, const double& dPsi_capDT) const=0;
    virtual mullinsEffect * clone() const=0;
  #endif

};

// eta = 1 - r* ( 1 - exp(-sqrt(m*(psi_max-psi))) ) -> Ricker et al, 2021 (modified Zuniga, 2005) - OLD and deleted
// eta = (1-r*sqrt(tanh(m*(_psi_max-psi_cap))) (modified Wrublescki Marczak, 2015) - NEW and correct
class negativeExponentialScaler : public mullinsEffect{
  #ifndef SWIG
  protected:
    double _r; 
    double _m; 
    scalarFunction* _temFunc_r;
    scalarFunction* _temFunc_m;
  #endif // SWIG

  public:
    negativeExponentialScaler(const int num, double r, double m, bool init = true);
    void setTemperatureFunction_r(const scalarFunction& Tfunc);
    void setTemperatureFunction_m(const scalarFunction& Tfunc);
    #ifndef SWIG
    virtual ~negativeExponentialScaler();
    negativeExponentialScaler(const negativeExponentialScaler& src);
    virtual negativeExponentialScaler& operator =(const mullinsEffect& src);
    virtual scalingFunction getType() const{return mullinsEffect::negativeExponentialScaler; };
    virtual void createIPVariable(IPMullinsEffect* &ipv) const;
    virtual void initLaws(const std::map<int,mullinsEffect*> &maplaw) {}; //nothing now, we will see if we use the mapping
    virtual void mullinsEffectScaling(double _psi0, double _psi, const IPMullinsEffect &ipvprev, IPMullinsEffect &ipv) const;
    virtual void mullinsEffectScaling(double _psi0, double _psi, const IPMullinsEffect &ipvprev, IPMullinsEffect &ipv, const double T, const double& dPsi_capDT) const;
    virtual mullinsEffect * clone() const;
    #endif // SWIG
};

// eta = (1 - r)/( 1 - r*psi/psi_max ) -> hyperbolic
class hyperbolicScaler : public mullinsEffect{
  #ifndef SWIG
  protected:
    double _r; 
    scalarFunction* _temFunc_r;
  #endif // SWIG

  public:
    hyperbolicScaler(const int num, double r, bool init = true);
    void setTemperatureFunction_r(const scalarFunction& Tfunc);
    #ifndef SWIG
    virtual ~hyperbolicScaler();
    hyperbolicScaler(const hyperbolicScaler& src);
    virtual hyperbolicScaler& operator =(const mullinsEffect& src);
    virtual scalingFunction getType() const{return mullinsEffect::hyperbolicScaler; };
    virtual void createIPVariable(IPMullinsEffect* &ipv) const;
    virtual void initLaws(const std::map<int,mullinsEffect*> &maplaw) {}; //nothing now, we will see if we use the mapping
    virtual void mullinsEffectScaling(double _psi0, double _psi, const IPMullinsEffect &ipvprev, IPMullinsEffect &ipv) const;
    virtual void mullinsEffectScaling(double _psi0, double _psi, const IPMullinsEffect &ipvprev, IPMullinsEffect &ipv, const double T, const double& dPsi_capDT) const;
    virtual mullinsEffect * clone() const;
    #endif // SWIG
};

// eta = 1 - r* ( 1 - psi/psi_max ) -> linear
class linearScaler : public mullinsEffect{
  #ifndef SWIG
  protected:
    double _r; 
    scalarFunction* _temFunc_r;
  #endif // SWIG

  public:
    linearScaler(const int num, double r, bool init = true);
    void setTemperatureFunction_r(const scalarFunction& Tfunc);
    #ifndef SWIG
    virtual ~linearScaler();
    linearScaler(const linearScaler& src);
    virtual linearScaler& operator =(const mullinsEffect& src);
    virtual scalingFunction getType() const{return mullinsEffect::linearScaler; };
    virtual void createIPVariable(IPMullinsEffect* &ipv) const;
    virtual void initLaws(const std::map<int,mullinsEffect*> &maplaw) {}; //nothing now, we will see if we use the mapping
    virtual void mullinsEffectScaling(double _psi0, double _psi, const IPMullinsEffect &ipvprev, IPMullinsEffect &ipv) const;
    virtual void mullinsEffectScaling(double _psi0, double _psi, const IPMullinsEffect &ipvprev, IPMullinsEffect &ipv, const double T, const double& dPsi_capDT) const;
    virtual mullinsEffect * clone() const;
    #endif // SWIG
};

// eta = 1 + r* ( 1 - exp((m*(1-psi/psi_max))) ) 
class positiveExponentialScaler : public mullinsEffect{
  #ifndef SWIG
  protected:
    double _r; 
    double _m; 
    scalarFunction* _temFunc_r;
    scalarFunction* _temFunc_m;
  #endif // SWIG

  public:
    positiveExponentialScaler(const int num, double r, double m, bool init = true);
    void setTemperatureFunction_r(const scalarFunction& Tfunc);
    void setTemperatureFunction_m(const scalarFunction& Tfunc);
    #ifndef SWIG
    virtual ~positiveExponentialScaler();
    positiveExponentialScaler(const positiveExponentialScaler& src);
    virtual positiveExponentialScaler& operator =(const mullinsEffect& src);
    virtual scalingFunction getType() const{return mullinsEffect::positiveExponentialScaler; };
    virtual void createIPVariable(IPMullinsEffect* &ipv) const;
    virtual void initLaws(const std::map<int,mullinsEffect*> &maplaw) {}; //nothing now, we will see if we use the mapping
    virtual void mullinsEffectScaling(double _psi0, double _psi, const IPMullinsEffect &ipvprev, IPMullinsEffect &ipv) const;
    virtual void mullinsEffectScaling(double _psi0, double _psi, const IPMullinsEffect &ipvprev, IPMullinsEffect &ipv, const double T, const double& dPsi_capDT) const;
    virtual mullinsEffect * clone() const;
    #endif // SWIG
};
/*
// eta = 1 + r* ( 1 - exp((m*(1-psi))) ) 
class arbitraryScaler : public mullinsEffect{
  #ifndef SWIG
  protected:
    double _r; 
    double _m; 
    scalarFunction* _temFunc_r;
    scalarFunction* _temFunc_m;
  #endif // SWIG

  public:
    positiveExponentialScaler(const int num, double r, double m, bool init = true);
    void setTemperatureFunction_r(const scalarFunction& Tfunc);
    void setTemperatureFunction_m(const scalarFunction& Tfunc);
    #ifndef SWIG
    virtual ~positiveExponentialScaler();
    positiveExponentialScaler(const positiveExponentialScaler& src);
    virtual positiveExponentialScaler& operator =(const mullinsEffect& src);
    virtual scalingFunction getType() const{return mullinsEffect::positiveExponentialScaler; };
    virtual void createIPVariable(IPMullinsEffect* &ipv) const;
    virtual void initLaws(const std::map<int,mullinsEffect*> &maplaw) {}; //nothing now, we will see if we use the mapping
    virtual void mullinsEffectScaling(double _psi0, double _psi, const IPMullinsEffect &ipvprev, IPMullinsEffect &ipv) const;
    virtual void mullinsEffectScaling(double _psi0, double _psi, const IPMullinsEffect &ipvprev, IPMullinsEffect &ipv, const double T, const double& dPsi_capDT) const;
    virtual mullinsEffect * clone() const;
    #endif // SWIG
};*/

#endif // MULLINSEFFECT_H_
