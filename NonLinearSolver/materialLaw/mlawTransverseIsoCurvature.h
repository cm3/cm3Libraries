//
// C++ Interface: material law
//              Transverse Isotropic law for large strains it is a "virtual implementation" you have to define a law
//              in your project which derive from this law. you have to do THE SAME for your IPVariable
//              which have to derive from IPIPTransverseIsotropic
//              Implementation follows J. Bonet, AJ Burton, CMAME 1998, 162, 151-164
//
//              Rotation Angles and defined by the location of Gauss Point
// Author:  <Ling Wu , Xing Liu>, (C) 2016
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef MLAWTRANSVERSEISOTROPICCURV_H_
#define MLAWTRANSVERSEISOTROPICCURV_H_
#include "mlaw.h"
#include "mlawTransverseIsotropic.h"
#include "STensor3.h"
#include "STensor43.h"
#include "ipTransverseIsoCurvature.h"


class mlawTransverseIsoCurvature : public mlawTransverseIsotropic 
{
 protected:
  SVector3 _Centroid;
  SVector3 _PointStart1;
  SVector3 _PointStart2;
  double _init_Angle;

 public:
  mlawTransverseIsoCurvature(const int num,const double E,const double nu, const double rho, const double EA, const double GA, const double nu_minor, const double Centroid_x, const double Centroid_y, const double Centroid_z, const double PointStart1_x, const double PointStart1_y, const double PointStart1_z, const double PointStart2_x, const double PointStart2_y, const double PointStart2_z,const double init_Angle);
 #ifndef SWIG
  mlawTransverseIsoCurvature(const mlawTransverseIsoCurvature &source);
  mlawTransverseIsoCurvature& operator=(const materialLaw &source);
	virtual ~mlawTransverseIsoCurvature(){}
	
	virtual materialLaw* clone() const {return new mlawTransverseIsoCurvature(*this);}
  virtual bool withEnergyDissipation() const {return false;};

  // function of materialLaw
  virtual matname getType() const{return materialLaw::transverseIsoCurvature;}
  virtual void createIPState(IPStateBase* &ips, bool hasBodyForce,const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const IntPt *GP=NULL, const int gpt =0) const;
  //virtual void initLaws(const std::map<int,materialLaw*> &maplaw){}; // this law is initialized so nothing to do
  //virtual double soundSpeed() const; // default but you can redefine it for your case

  virtual void getTransverseDirection(SVector3 &A, const IPTransverseIsotropic *q1) const 
  {
     const IPTransverseIsoCurvature *q = NULL;
     q=dynamic_cast<const IPTransverseIsoCurvature*>(q1);
     if(q!=NULL)
     {
        q->getTransverseDirection(A); 
     }
  }
  SVector3 getCentroid() const { return _Centroid;};
  SVector3 getPointStart1() const { return _PointStart1;};
  SVector3 getPointStart2() const { return _PointStart2;};
  double getInit_Angle() const { return _init_Angle;};
  


 #endif // SWIG
};

#endif // MLAWTRANSVERSEISOTROPICCURV_H_
