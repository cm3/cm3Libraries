//
// C++ Interface: material law
//
// Description: gurson elasto-plastic law with non local damage interface
//
//
// Author:  <L. Noels>, (C) 2013
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "mlawNonLocalDamageGurson.h"
#include <math.h>
#include "MInterfaceElement.h"
#include "nonLinearMechSolver.h"
#include "NucleationLaw.h"
#include "ipNonLocalPorosity.h"

mlawNonLocalDamageGurson::mlawNonLocalDamageGurson(const int num,const double E,const double nu, const double rho,
                           const double q1, const double q2, const double q3, const double fVinitial,
                           const J2IsotropicHardening &j2IH, const CLengthLaw &cLLaw,
                           const double tol, const bool matrixbyPerturbation, const double pert) :
                                               mlawNonLocalPorosity(num,E,nu,rho,fVinitial,j2IH,cLLaw,tol,matrixbyPerturbation,pert),
                                               _q1(q1), _q2(q2) , _q3(q3){
  // Compute ffstar from Gurson parameters
  _fV_failure = (_q1 - sqrt(_q1*_q1 -_q3*_q3))/_q3/_q3;
  
  _coalescenceLaw = new NoCoalescenceLaw(num);          // no coalesnce law by default / trivial one
  _voidEvolutionLaw = new SphericalVoidStateEvolutionLawWithMatrixPlasticDeformation(num,1.,1.5); // void description by default

  if (_regularisationFunc_porosityEff != NULL)  delete _regularisationFunc_porosityEff;
  _regularisationFunc_porosityEff = new fourVariableExponentialSaturationScalarFunction( _localfVMin * 2.0, _localfVMin, 0.98 * _fV_failure, 0.999 * _fV_failure) ;

  /// _failedTol have to be smaller than the largest factor reachable after regularisation (i.e. "0.995" here).
  _failedTol = 0.995;

  _temFunc_q1 = new constantScalarFunction(1.);
  _temFunc_q2 = new constantScalarFunction(1.);
  _temFunc_q3 = new constantScalarFunction(1.);
};

mlawNonLocalDamageGurson::mlawNonLocalDamageGurson(const mlawNonLocalDamageGurson &source) :
   mlawNonLocalPorosity(source),
   _q1(source._q1), _q2(source._q2), _q3(source._q3)
{


    // Coalescence law
  _coalescenceLaw = NULL;
  if(source._coalescenceLaw != NULL){
    _coalescenceLaw=source._coalescenceLaw->clone();
  }
    // Void state law
  _voidEvolutionLaw = NULL;
  if (source._voidEvolutionLaw != NULL){
    _voidEvolutionLaw = source._voidEvolutionLaw->clone();
  }

  _temFunc_q1 = NULL;
  if (source._temFunc_q1 != NULL){
    _temFunc_q1 = source._temFunc_q1->clone();
  }

  _temFunc_q2 = NULL;
  if (source._temFunc_q2!= NULL){
    _temFunc_q2 = source._temFunc_q2->clone();
  }

  _temFunc_q3 = NULL;
  if (source._temFunc_q3 != NULL){
    _temFunc_q3 = source._temFunc_q3->clone();
  }

}

mlawNonLocalDamageGurson::~mlawNonLocalDamageGurson()
{
  if (_coalescenceLaw!=NULL) delete _coalescenceLaw; _coalescenceLaw = NULL;
  if (_voidEvolutionLaw!=NULL) delete _voidEvolutionLaw;
  
  if(_temFunc_q1 != NULL) delete _temFunc_q1;
  _temFunc_q1 = NULL;
  if(_temFunc_q2 != NULL) delete _temFunc_q2;
  _temFunc_q2 = NULL;
  if(_temFunc_q3 != NULL) delete _temFunc_q3;
  _temFunc_q3 = NULL;
}

void mlawNonLocalDamageGurson::setVoidEvolutionLaw(const voidStateEvolutionLaw& voidtateLaw)
{
  if (_voidEvolutionLaw != NULL) delete _voidEvolutionLaw;
  _voidEvolutionLaw = voidtateLaw.clone();
}
void mlawNonLocalDamageGurson::setCoalescenceLaw(const CoalescenceLaw& added_coalsLaw)
{
  if (_coalescenceLaw != NULL) delete _coalescenceLaw;
  _coalescenceLaw = added_coalsLaw.clone();
}

void mlawNonLocalDamageGurson::createIPState(IPStateBase* &ips,bool hasBodyForce, const bool* state_,const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
  Msg::Error("mlawNonLocalDamageGurson::createIPState was not defined!!!");
};

void mlawNonLocalDamageGurson::createIPVariable(const double fvInitial, IPNonLocalPorosity* &ipv) const
{
  if (ipv!=NULL) delete ipv;
  ipv = new IPNonLocalPorosity(fvInitial, _j2IH, &_cLLaw,_nucleationLaw,_coalescenceLaw,_voidEvolutionLaw);
};


void mlawNonLocalDamageGurson::initLaws( const std::map< int, materialLaw* > &maplaw)
{
  // Initialise internal laws.
  _nucleationLaw->initNucleationLaw( this );
};
  



void mlawNonLocalDamageGurson::voidStateGrowth(const double yieldfV0, const double yieldfV1, // yield porosity
                        const double DeltahatD, const double DeltahatQ, const double DeltahatP, // internal variables
                        const IPNonLocalPorosity *q0, IPNonLocalPorosity *q1) const
{                        
  _voidEvolutionLaw->evolve(yieldfV0,yieldfV1,DeltahatD,DeltahatQ,DeltahatP,
                            q0,q1, q0->getConstRefToIPVoidState(),q1->getRefToIPVoidState());
}

double mlawNonLocalDamageGurson::yieldFunction(const double kcorEq, const double pcor, const double R, const double yieldfV,
                                const IPNonLocalPorosity* q0, const IPNonLocalPorosity* q1, const double* T,
                                bool diff, fullVector<double>* gradf,
                                bool withthermic , double* dfdT ) const{
  double q1T = _q1;
  double q2T = _q2;
  double q3T = _q3;
  if (isThermomechanicallyCoupled()){
    q1T=getq1T(*T);
    q2T=getq2T(*T);
    q3T=getq3T(*T);
  };
  double fact = 1.5*q2T*pcor/R;
  double coshFact = cosh(fact);
  double RR = R*R;
  double fg = kcorEq*kcorEq/RR+2.*yieldfV*q1T*coshFact-q3T*q3T*yieldfV*yieldfV-1.;
  
  if (diff)
  {
    double sinhFact = sinh(fact);
    double DfactDpcor = 1.5*q2T/R;
    double DfactDR = -1.5*q2T*pcor/RR;

    (*gradf)(0) = 2.*kcorEq/RR;
    (*gradf)(1) = 2.*yieldfV*q1T*sinhFact*DfactDpcor;
    (*gradf)(2) = kcorEq*kcorEq*(-2./(RR*R))+ 2.*yieldfV*q1T*sinhFact*DfactDR;
    (*gradf)(3) = 2.*q1T*coshFact-2.*q3T*q3T*yieldfV;
    (*gradf)(4) = 0.;
    (*gradf)(5) = 0.; 
    
    if (withthermic){
      double dq1dT=getq1Der(*T);
      double dq2dT=getq2Der(*T);
      double dq3dT=getq3Der(*T);
      double DfactDT = 1.5*dq2dT*pcor/R;
      *dfdT = 2.*yieldfV*dq1dT*coshFact+ 2.*yieldfV*q1T*sinhFact*DfactDT -2*q3T*dq3dT*yieldfV*yieldfV;
    }
  }
  return fg;
};


void mlawNonLocalDamageGurson::plasticFlow(double & Ns, double& Nv,
                              const double kcorEq, const double pcor, const double R,
                              const IPNonLocalPorosity* q0, const IPNonLocalPorosity* q1, const double* T,
                              bool diff, fullVector<double>* gradNs, // grad of Ns with respect to [kcorEq pcor R fV Chi W]
                              fullVector<double>* gradNv, // grad of Nv with respect to  [kcorEq pcor R fV Chi W]
                              bool withTher, double* dNsDT, double* dNvDT
                              ) const{
  double yieldfV = q1->getYieldPorosity();
  double q1T = _q1;
  double q2T = _q2;
  double q3T = _q3;
  if (isThermomechanicallyCoupled()){
    q1T=getq1T(*T);
    q2T=getq2T(*T);
    q3T=getq3T(*T);
  };

  // associated flow rulle
  // Ns = df_dkcorEq, Nv = df_dpcor
  double fact = 1.5*q2T*pcor/R;
  double sinhFact = sinh(fact);
  double RR = R*R;
  double DfactDpcor = 1.5*q2T/R;

  Ns = 2.*kcorEq/RR;
  Nv = 2.*yieldfV*q1T*sinhFact*DfactDpcor;
  
  if (diff)
  {
    (*gradNs)(0) = 2./RR;
    (*gradNs)(1) = 0.;
    (*gradNs)(2) = 2.*kcorEq*(-2./(RR*R));
    (*gradNs)(3) = 0.;
    (*gradNs)(4) = 0.;
    (*gradNs)(5) = 0.;

    double coshFact = cosh(fact);
    double DfactDR= -1.5*q2T*pcor/RR;
    double DDfactDpcorDR = -1.5*q2T/RR;

    (*gradNv)(0) = 0.;
    (*gradNv)(1) = 2.*yieldfV*q1T*DfactDpcor*coshFact*DfactDpcor;
    (*gradNv)(2) = 2.*yieldfV*q1T*(DfactDpcor*coshFact*DfactDR+ sinhFact*DDfactDpcorDR);
    (*gradNv)(3) = 2.*q1T*sinhFact*DfactDpcor;
    (*gradNv)(4) = 0.;
    (*gradNv)(5) = 0.;
    
    if (withTher){
      double dq1dT=getq1Der(*T);
      double dq2dT=getq2Der(*T);
      double dq3dT=getq3Der(*T);
    
      *dNsDT = 0.;
      double DfactDT = 1.5*dq2dT*pcor/R;
      double DDfactDpcorDT = 1.5*dq2dT/R;
      *dNvDT = 2.*yieldfV*dq1dT*sinhFact*DfactDpcor+ 2.*yieldfV*q1T*coshFact*DfactDT*DfactDpcor+2.*yieldfV*q1T*sinhFact*DDfactDpcorDT;
    }
  }
};



double mlawNonLocalDamageGurson::getOnsetCriterion(const IPNonLocalPorosity* q1) const{
  return q1->getYieldPorosity();
};


void mlawNonLocalDamageGurson::checkCoalescence(IPNonLocalPorosity* q1, const IPNonLocalPorosity *q0, const double *T) const{
  // check coalescence
  _coalescenceLaw->checkCoalescenceGTN(q0,q1);
};

void mlawNonLocalDamageGurson::forceCoalescence(IPNonLocalPorosity* q1, const IPNonLocalPorosity *q0, const double* T) const
{
  _coalescenceLaw->forceCoalescenceGTN(q0,q1);
}

bool mlawNonLocalDamageGurson::checkCrackCriterion(IPNonLocalPorosity* q1, const IPNonLocalPorosity *q0, const STensor3& Fcur, const SVector3& normal, double delayFactor, const double* T) const
{
  _coalescenceLaw->checkCoalescenceGTNWithNormal(normal,q0,q1);
  if (q1->getConstRefToIPCoalescence().getCoalescenceOnsetFlag())
  {
    if (delayFactor >=1.) return true;
    double fV = q1->getYieldPorosity();
    const IPCoalescence& ipvCoales = q1->getConstRefToIPCoalescence();
    double fVOnset = ipvCoales.getIPvAtCoalescenceOnset()->getYieldPorosity();
    if (fV >= fVOnset*delayFactor + _fV_failure*(1-delayFactor))
    {
      return true;
    }
    else
    {
      return false;
    }
  }
  return false;
};


void mlawNonLocalDamageGurson::checkFailed(IPNonLocalPorosity* q1, const IPNonLocalPorosity *q0) const{
  // manage failure flag
  if (q0->isFailed()){
    q1->setFailed(true);
  }
  else{
    if (q1->getYieldPorosity() > _failedTol*_fV_failure){
      q1->setFailed(true);
      #ifdef _DEBUG
      Msg::Info("mlawNonLocalDamageGurson::checkFailed: an ip has failed at fV = %e",q1->getYieldPorosity());
      #endif //_DEBUG
    }
    else{
      q1->setFailed(false);
    }
  }
};

double mlawNonLocalDamageGurson::I1J2J3_yieldFunction(const STensor3& kcor, const double R, const IPNonLocalPorosity* q0, const IPNonLocalPorosity* q1, const double* T,
                                                    bool diff, STensor3* DFDkcor, double* DFDR, std::vector<double>* DFDY, 
                                                    bool withThermic, double *DFDT) const{
  double yieldfV = q1->getYieldPorosity();
  double q1T = _q1;
  double q2T = _q2;
  double q3T = _q3;
  if (isThermomechanicallyCoupled()){
    q1T=getq1T(*T);
    q2T=getq2T(*T);
    q3T=getq3T(*T);
  };
  
  static STensor3 devKcor;
  static double pcor, kcorEqSq;
  STensorOperation::decomposeDevTr(kcor,devKcor,pcor);
  pcor /= 3.;
  kcorEqSq = 1.5*STensorOperation::doubledot(devKcor,devKcor);
  
  double f1 = 1.5*q2T*pcor/R;
  double coshf1 = cosh(f1);
  double G =  kcorEqSq/R/R+2.*yieldfV*q1T*coshf1-q3T*q3T*yieldfV*yieldfV-1.;
  double phi = -2.*yieldfV*q1T+q3T*q3T*yieldfV*yieldfV+1.;
  double u = G/phi + 1.;
  double F = sqrt(u) - 1.;
  
  if (diff)
  {
    double f0 = yieldfV*q1T*q2T/R;
    double sinhf1 = sinh(f1);
    
    static STensor3 DGDkcor;
    DGDkcor = devKcor;
    DGDkcor *= (3./R/R);
    double factP = f0*sinhf1;
    DGDkcor(0,0) += factP;
    DGDkcor(1,1) += factP;
    DGDkcor(2,2) += factP;
     
    double Df1DR = -f1/R;
    double DGDR = -2.*kcorEqSq/(R*R*R) + 2.*yieldfV*q1T*sinhf1*Df1DR;
    double DGDyieldfV = 2.*q1T*coshf1-2.*q3T*q3T*yieldfV;
    double DphiDyieldfV =-2.*q1T+q3T*q3T*2.*yieldfV; 
    
    double DFDu = 1./(2.*(1+F));
    double DuDG = 1./phi;
    double DuDphi = -G/(phi*phi);
    double DFDG = DFDu*DuDG;
    double DFDphi =DFDu*DuDphi;
    
    // kcor
    *DFDkcor = DGDkcor;
    *DFDkcor *= DFDG;
    // R
    *DFDR = DFDG*DGDR;
    // 
    (*DFDY)[0] = DFDG*DGDyieldfV + DFDphi*DphiDyieldfV;;
    for (int i=1; i< DFDY->size(); i++)
    {
      (*DFDY)[i] = 0.;
    }
    // 
    if (withThermic){
      double dq1dT=getq1Der(*T);
      double dq2dT=getq2Der(*T);
      double dq3dT=getq3Der(*T);
      
      double Df1DT = 1.5*dq2dT*pcor/R;
      double DGDT = 2.*yieldfV*dq1dT*coshf1 + 2.*yieldfV*q1T*sinhf1*Df1DT-2.*q3T*dq3dT*yieldfV*yieldfV;
      double DphiDT = -2.*yieldfV*dq1dT+2.*q3T*dq3dT*yieldfV*yieldfV;
      
      *DFDT = DFDG*DGDT + DFDphi*DphiDT;
    }
  }
  
  return F;
};


void mlawNonLocalDamageGurson::I1J2J3_getYieldNormal(STensor3& Np, const STensor3& kcor, const double R, const IPNonLocalPorosity* q0, const IPNonLocalPorosity* q1, const double* T,
                                      bool diff, STensor43* DNpDkcor, STensor3* DNpDR, std::vector<STensor3>* DNpDY,
                                      bool withThermic, STensor3* DNpDT) const{
  double yieldfV = q1->getYieldPorosity();
  double q1T = _q1;
  double q2T = _q2;
  double q3T = _q3;
  if (isThermomechanicallyCoupled()){
    q1T=getq1T(*T);
    q2T=getq2T(*T);
    q3T=getq3T(*T);
  };
  static STensor3 devKcor;  
  double pcor, kcorEqSq;
  STensorOperation::decomposeDevTr(kcor,devKcor,pcor);
  pcor /= 3.;
  kcorEqSq = 1.5*STensorOperation::doubledot(devKcor,devKcor);

  double f0 = yieldfV*q1T*q2T/R;
  double f1 = 1.5*q2T*pcor/(R);
  double sinhf1 = sinh(f1);
  double coshf1 = cosh(f1);
  double RR = R*R;
  
  static STensor3 DGDkcor;
  DGDkcor = devKcor;
  double factDev = (3./RR);
  DGDkcor *= factDev;
  double factP = f0*sinhf1;
  DGDkcor(0,0) += factP;
  DGDkcor(1,1) += factP;
  DGDkcor(2,2) += factP;
  
  double G =  kcorEqSq/R/R+2.*yieldfV*q1T*coshf1-q3T*q3T*yieldfV*yieldfV-1.;
  double phi = -2.*yieldfV*q1T+q3T*q3T*yieldfV*yieldfV+1.;
  double u = G/phi + 1.;
  double F = sqrt(u) - 1.;
  double DFDu = 1./(2.*(1+F));
  double DuDG = 1./phi;
  double DuDphi = -G/(phi*phi);
  double DFDG = DFDu*DuDG;
  
  double R0 = _j2IH->getYield0();
  Np = DGDkcor;
  Np *= (R0*DFDG);
  
  if (diff)
  {
    
    double Df1Dpcor = 1.5*q2T/R;
    double DfactPDpcor = f0*coshf1*Df1Dpcor;
    static STensor43 DDGDkcorDkcor;
    DDGDkcorDkcor = _I4dev;
    DDGDkcorDkcor *= (factDev);
    STensorOperation::prodAdd(_I,_I,DfactPDpcor/3.,DDGDkcorDkcor);
    
     //
    double DfactDevDR = -2.*factDev/R;
    double Df0DR = -f0/R;
    double Df1DR = -f1/R;
    double DfactPDR = Df0DR*sinhf1+f0*coshf1*Df1DR;
    
    static STensor3 DDGDkcorDR;
    DDGDkcorDR = devKcor;
    DDGDkcorDR *= (DfactDevDR);
    DDGDkcorDR(0,0) += DfactPDR;
    DDGDkcorDR(1,1) += DfactPDR;
    DDGDkcorDR(2,2) += DfactPDR;
    
    //
    double Df0Dyieldf = q1T*q2T/R;
    double DfactPDyieldf = Df0Dyieldf*sinhf1;
    static STensor3 DDGDkcorDyieldfV;
    STensorOperation::diag(DDGDkcorDyieldfV,DfactPDyieldf);
    
   
    //
    double DGDR = kcorEqSq*(-2./(RR*R))+ 2.*yieldfV*q1T*sinhf1*Df1DR;
    double DGDyieldfV = 2.*q1T*coshf1-2.*q3T*q3T*yieldfV;
    double DphiDyieldfV =-2.*q1T+q3T*q3T*2.*yieldfV; 
    
    double DDFDuDu = -DFDu*DFDu/(1+F); 
    double DDFDGDG = DDFDuDu*DuDG*DuDG;
    double DDuDGDphi = -1./(phi*phi);
    double DDFDGDphi = DDFDuDu*DuDphi*DuDG + DFDu*DDuDGDphi;
    
    //
    *DNpDkcor = DDGDkcorDkcor;
    *DNpDkcor *= (R0*DFDG);
    STensorOperation::prodAdd(DGDkcor,DGDkcor,R0*DDFDGDG,*DNpDkcor);
    //
    *DNpDR = DDGDkcorDR;
    *DNpDR *= (R0*DFDG);
    DNpDR->daxpy(DGDkcor,R0*DDFDGDG*DGDR);
    
    //
    (*DNpDY)[0] = DDGDkcorDyieldfV;
    (*DNpDY)[0] *= (R0*DFDG);
    (*DNpDY)[0].daxpy(DGDkcor,R0*(DDFDGDG*DGDyieldfV+DDFDGDphi*DphiDyieldfV));
    for (int i=1; i< DNpDY->size(); i++)
    {
      STensorOperation::zero((*DNpDY)[i]);
    }

    // 
    if (withThermic){
      double dq1dT=getq1Der(*T);
      double dq2dT=getq2Der(*T);
      double dq3dT=getq3Der(*T);
      
      double Df0DT = R0*yieldfV*(dq1dT*q2T + q1T*dq2dT)/R;
      double Df1DT = 1.5*dq2dT*pcor/R;
      double DfactPDT = Df0DT*sinhf1+f0*coshf1*Df1DT;
      static STensor3 DDGDkcorDT;
      STensorOperation::diag(DDGDkcorDT,DfactPDT);
      
      double DGDT = 2.*yieldfV*dq1dT*coshf1 + 2.*yieldfV*q1T*sinhf1*Df1DT-2.*q3T*dq3dT*yieldfV*yieldfV;
      double DphiDT = -2.*yieldfV*dq1dT+2.*q3T*dq3dT*yieldfV*yieldfV;
      
      *DNpDT = DDGDkcorDT;
      *DNpDT *= (R0*DFDG);
      DNpDT->daxpy(DGDkcor,R0*DDFDGDG*DGDT + R0*DDFDGDphi*DphiDT);
    }
  }
};

void mlawNonLocalDamageGursonHill48::setYieldParameters(const std::vector<double>& Mvec)
{
  if (Mvec.size() != 9)
  {
    Msg::Error("mlawNonLocalDamageGursonHill48 requires 9 parameters to define yield surface");
    Msg::Exit(0);
  }
  STensorOperation::zero(_aniM);
  
  Msg::Info("-----\nHill parameters");
  for (int i=0; i< Mvec.size(); i++)
  {
    printf("%.16g ", Mvec[i]);
  }
  Msg::Info("\n------");
    
  // [F, G, H, L, M, N] = Mvec
  double F = Mvec[0];
  double G = Mvec[1];
  double H = Mvec[2];
  double L = Mvec[3];
  double M = Mvec[4];
  double N = Mvec[5];
  double P = Mvec[6];
  double Q = Mvec[7];
  double R = Mvec[8];
  // define the equivalent stress under the form
  // sigEq**2 = F*0.5*(s11-s22)**2 
  //           +G*0.5*(s22-s00)**2
  //           +H*0.5*(s00-s11)**2 
  //           +L*3*sig12**2 + M*3*sig02**2 + N*3*s01**2) = 1.5*s:aniMSqr:s
  // p = (P*sig00 + Q*sig11 + R*sig22)/3
  
  STensor43 aniMSqr;
  STensorOperation::zero(aniMSqr);
  aniMSqr(0,0,0,0) = (G+H)/3.;
  aniMSqr(0,0,1,1) = -H/3;
  aniMSqr(0,0,2,2) = -G/3.;
  
  aniMSqr(1,1,0,0) = -H/3.;
  aniMSqr(1,1,1,1) = (H+F)/3.;
  aniMSqr(1,1,2,2) = -F/3.;
  
  aniMSqr(2,2,0,0) = -G/3.;
  aniMSqr(2,2,1,1) = -F/3.;
  aniMSqr(2,2,2,2) = (F+G)/3.;
  
  aniMSqr(1,0,1,0) = N*0.5;
  aniMSqr(0,1,0,1) = N*0.5;
  aniMSqr(0,1,1,0) = N*0.5;
  aniMSqr(1,0,0,1) = N*0.5;
  
  aniMSqr(2,0,2,0) = M*0.5;
  aniMSqr(0,2,2,0) = M*0.5;
  aniMSqr(0,2,0,2) = M*0.5;
  aniMSqr(2,0,0,2) = M*0.5;
  
  aniMSqr(2,1,2,1) = L*0.5;
  aniMSqr(1,2,1,2) = L*0.5;
  aniMSqr(1,2,2,1) = L*0.5;
  aniMSqr(2,1,1,2) = L*0.5; 
  
  STensorOperation::sqrtSTensor43(aniMSqr, _aniM);
  _aniM.print("anisotropic tensor");
  fullMatrix<double> tmp(6,6);
  STensorOperation::fromSTensor43ToFullMatrix_ConsistentForm(aniMSqr,tmp);
  tmp.print("aniMSqr");
  STensorOperation::fromSTensor43ToFullMatrix_ConsistentForm(_aniM,tmp);
  tmp.print("_aniM");
  
  fullMatrix<double> test(6,6);
  tmp.mult(tmp, test);
  test.print("test");
  STensor3 I(1.);
  STensor3 D(0);
  D(0,0) = P;
  D(1,1) = Q;
  D(2,2) = R;
  STensorOperation::prodAdd(I,D,1./3.,_aniM);
  _aniM.print("anisotropic tensor");
};

                                              
void mlawNonLocalDamageGursonHill48::I1J2J3_voidCharacteristicsEvolution_NonLocal_aniso(const STensor3& sig,
                                                const IPNonLocalPorosity *q0, IPNonLocalPorosity* q1,
                                                bool stiff,
                                                std::vector<double>* Y ,
                                                std::vector<STensor3>* DYDsig ,
                                                std::vector<std::vector<double> >* DYDNonlocalVars
                                                  ) const
{
  const STensor43& M = q1->getConstRefToAnisotropicTensor();
  static STensor3 sigEff;
  static std::vector<STensor3> DYDsigEff(4, STensor3());
  STensorOperation::multSTensor43STensor3(M, sig, sigEff);
  mlawNonLocalDamageGurson::I1J2J3_voidCharacteristicsEvolution_NonLocal(sigEff, q0, q1, stiff, Y, &DYDsigEff, DYDNonlocalVars);
  if (stiff)
  {
    for (int i=0; i< 4; i++)
    {
      STensorOperation::multSTensor3STensor43(DYDsigEff[i],M,(*DYDsig)[i]);
    }
  }
};                                             

double mlawNonLocalDamageGursonHill48::I1J2J3_yieldFunction_aniso(const STensor3& kcor, const double R, const IPNonLocalPorosity* q0, const IPNonLocalPorosity* q1, const double* T,
                                        bool diff, STensor3* DFDkcor, double* DFDR, std::vector<double>* DFDY, 
                                        STensor3* DFDDeltaEp, STensor3* DFDFdefo, 
                                        bool withThermic, double *DFDT) const
{
  const STensor43& M = q1->getConstRefToAnisotropicTensor();
  const STensor63& DMDFdefo = q1->getConstRefToDAnisotropicTensorDF();
  const STensor63& DMDDeltaEp = q1->getConstRefToDAnisotropicTensorDEp();
  
  static STensor3 sigEff;
  static STensor3 DFDsigEff;
  STensorOperation::multSTensor43STensor3(M, kcor, sigEff);
  
  double fval = mlawNonLocalDamageGurson::I1J2J3_yieldFunction(sigEff, R, q0, q1, T, diff,&DFDsigEff,DFDR, DFDY,withThermic,DFDT);
  if (diff)
  {
    STensorOperation::multSTensor3STensor43(DFDsigEff, M,*DFDkcor);
    
    static STensor43 temp;
    STensorOperation::multSTensor3STensor63(kcor, DMDDeltaEp, temp);
    STensorOperation::multSTensor3STensor43(DFDsigEff,temp,*DFDDeltaEp);
    
    STensorOperation::multSTensor3STensor63(kcor, DMDFdefo, temp);
    STensorOperation::multSTensor3STensor43(DFDsigEff,temp,*DFDFdefo);
  }
  return fval;
};
void mlawNonLocalDamageGursonHill48::I1J2J3_getYieldNormal_aniso(STensor3& Np, const STensor3& kcor, const double R, const IPNonLocalPorosity* q0, const IPNonLocalPorosity* q1, const double* T,
                                  bool diff, STensor43* DNpDkcor, STensor3* DNpDR, std::vector<STensor3>* DNpDY,
                                  STensor43* DNpDDeltaEp, STensor43* DNpDFdefo,
                                  bool withThermic, STensor3* DNpDT  
                                  ) const
{
  const STensor43& M = q1->getConstRefToAnisotropicTensor();
  const STensor63& DMDFdefo = q1->getConstRefToDAnisotropicTensorDF();
  const STensor63& DMDDeltaEp = q1->getConstRefToDAnisotropicTensorDEp();
  
  static STensor3 sigEff, NpEff, DNpEffDR, DNpEffDT; 
  static std::vector<STensor3> DNpEffDY;
  static STensor43 DNpEffDsigEff;
  if (diff)
  {
    DNpEffDY.resize(DNpDY->size(), STensor3(0.));
  }
  STensorOperation::multSTensor43STensor3(M, kcor, sigEff);
  mlawNonLocalDamageGurson::I1J2J3_getYieldNormal(NpEff, sigEff, R, q0, q1, T, diff, &DNpEffDsigEff, &DNpEffDR, &DNpEffDY, withThermic, &DNpEffDT);
  STensorOperation::multSTensor3STensor43(NpEff, M, Np);
  if (diff)
  {
    static STensor43 DNpDsigEff;
    STensorOperation::multSTensor43FirstIndexes(M, DNpEffDsigEff, DNpDsigEff);
    STensorOperation::multSTensor43(DNpDsigEff, M, *DNpDkcor);
    
    STensorOperation::multSTensor3STensor43(DNpEffDR, M, *DNpDR);
    for (int i=0; i< DNpEffDY.size(); i++)
    {
      STensorOperation::multSTensor3STensor43(DNpEffDY[i],M, (*DNpDY)[i]);
    }
    
    static STensor43 tmp;
    STensorOperation::multSTensor3STensor63(kcor, DMDDeltaEp, tmp);    
    STensorOperation::multSTensor43(DNpDsigEff, tmp, *DNpDDeltaEp);
    STensorOperation::multSTensor3STensor63Add(NpEff, DMDDeltaEp, *DNpDDeltaEp);
    
    STensorOperation::multSTensor3STensor63(kcor, DMDFdefo, tmp);
    STensorOperation::multSTensor43(DNpDsigEff,tmp,*DNpDFdefo);
    STensorOperation::multSTensor3STensor63Add(NpEff, DMDFdefo, *DNpDFdefo);
    
    if (withThermic)
    {
      STensorOperation::multSTensor3STensor43(DNpEffDT, M, *DNpDT);
    }
  }
};
