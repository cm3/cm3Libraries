//
// C++ Interface: material law
//
// Description: local damage law
//
// Author:  <V-D Nguyen>, (C) 2021
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef MLAWLOCALDAMAGEJ2SMALLSTRAIN_H_
#define MLAWLOCALDAMAGEJ2SMALLSTRAIN_H_
#include "mlawJ2VMSmallStrain.h"
#include "ipNonLocalDamageJ2Hyper.h"
#include "j2IsotropicHardening.h"
#include "CLengthLaw.h"
#include "DamageLaw.h"

class mlawLocalDamageJ2SmallStrain : public mlawJ2VMSmallStrain
{
 protected:
  DamageLaw  *damLaw;
 public:
  mlawLocalDamageJ2SmallStrain(const int num,const double E,const double nu, const double rho,
				const J2IsotropicHardening &_j2IH, const DamageLaw &_damLaw,
				const double tol=1.e-6, const bool matrixbyPerturbation = false, const double pert = 1e-8);
 #ifndef SWIG
  mlawLocalDamageJ2SmallStrain(const mlawLocalDamageJ2SmallStrain &source);
  virtual ~mlawLocalDamageJ2SmallStrain()
  {
    if (damLaw!= NULL) delete damLaw;
  }
	virtual materialLaw* clone() const {return new mlawLocalDamageJ2SmallStrain(*this);}
  // function of materialLaw
  virtual matname getType() const{return materialLaw::localDamageJ2SmallStrain;}
  virtual bool withEnergyDissipation() const {return true;};
  virtual void createIPState(IPStateBase* &ips,bool hasBodyForce,const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const IntPt *GP=NULL, const int gpt = 0) const;

  virtual void initLaws(const std::map<int,materialLaw*> &maplaw){}; // this law is initialized so nothing to do
  virtual double soundSpeed() const; // default but you can redefine it for your case
  virtual const DamageLaw *getDamageLaw() const {return damLaw; };
	virtual double scaleFactor() const {return _Cel(0,0,0,0);};
  // specific function
 public:
  virtual void constitutive(
                            const STensor3& F0,         // initial deformation gradient (input @ time n)
                            const STensor3& Fn,         // updated deformation gradient (input @ time n+1)
                            STensor3 &P,                // updated 1st Piola-Kirchhoff stress tensor (output)
                            const IPVariable *q0,       // array of initial internal variable
                            IPVariable *q1,             // updated array of internal variable (in ipvcur on output),
                            STensor43 &Tangent,         // constitutive tangents (output)
                            const bool stiff, 
                            STensor43* elasticTangent =NULL,
                            const bool dTangent =false,
                            STensor63* dCalgdeps = NULL          // if true compute the tangents
                           ) const;


 #endif // SWIG
};

#endif // MLAWLOCALDAMAGEJ2SMALLSTRAIN_H_
