#ifndef ID_H
#define ID_H 1

//value to keep residual or not
#define Crmatrix_1st 0
#define Crmatrix_2nd 1
#define Crinclusion 1
#define Crmatrix_LD 0
#define Crinclusion_LD 1
#define RESIDUAL_FROM_STRAIN_LD 1

//Identifiers of constitutive models
#define EL 1
#define EP 2
#define EP_Stoch 21
#define VT 3
#define MT 4
#define SC 5
#define MTSecFirStoch 6
#define ANEL 7
#define MTSecF 8
#define MTSecNtr 9
#define MTSecSd 10
#define EVP 11
#define MTSecF_LargeDeform 12
#define MT_VT 13
#define LAM_2PLY 14

//Identifiers of hardening models
#define H_POW 1
#define H_EXP 2
#define H_LINEXP 3
#define H_SWIFT 4
#define H_POW_EXTRAPOL 5
#define H_POW_EXP 6
#define H_POW_DP 11
#define H_EXP_DP 12
#define H_LINEXP_DP 13
#define H_SWIFT_DP 14
#define H_POW_EXTRAPOL_DP 15
#define H_POW_EXP_DP 16


//Identifiers of damage models
#define D_LC 1
#define D_LCN 2
#define D_LINN 3
#define D_EXPN 4
#define D_PLN 5
#define D_LCN_STOCH 6
#define D_WEIBULL 7
#define D_BILIN 8
#define D_BILIN_STOCH 9
#define D_SIGMOID 10

//Identifiers of characteristic length models
#define CL_Iso 1
#define CL_Aniso 2
#define CL_Iso_V1 3
#define CL_Aniso_V1 4
#define CL_Stoch 5

//Identifiers of solver
#define NR 1		//Newton-Raphson



//Identifiers of isotropization methods
#define ALL_ISO 1
#define ESH_ISO 2
#define ALL_ANI 3


//Identifiers of viscoplastic function
#define V_NORTON 1
#define V_PERZYNA 2
#define V_POWER 3
#define V_PRANDTL 4
#define V_POWER_NO_HARDENING 5
#endif
