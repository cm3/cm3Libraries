// C++ Interface: material law
// Description: PhenomenologicalSMP law
// Author:  <H. Gulasik, M. Pareja>, (C) 2019
// Copyright: See COPYING file that comes with this distribution

#ifndef MLAWPHENOMENOLOGICALSMP_H_
#define MLAWPHENOMENOLOGICALSMP_H_
#include "ipPhenomenologicalSMP.h"
#include "mlaw.h"
#include "STensor3.h"
#include "STensor43.h"
#include "STensor63.h"
#include "scalarFunction.h"
#include "elasticPotential.h"
#include "mlawHyperelastic.h"
#include "mlawCoupledThermoMechanics.h"


class mlawPhenomenologicalSMP : public  mlawCoupledThermoMechanics
{
protected:
    double toleranceOnZg=0.001;
    double toleranceOnT=0.001;

//  new architecture
    mlawPowerYieldHyper *lawG;
    mlawPowerYieldHyper *lawR;
    mlawPowerYieldHyper *lawAM;


    double      _t0;
    STensor3    _I2;
    STensor43   _I4, _I4dev;
    STensor3    _alphaDilatation, _Stiff_alphaDilatation;
    double      _alpha, _beta, _gamma;
    double      _rho;
    
    double      _Kxg, _Kyg, _Kzg, _Kxr, _Kyr, _Kzr, _KxAM, _KyAM, _KzAM;
    STensor3    _k0, _kqg, _kqr, _kqAM;
    double  _alphag0, _alphar0, _alphaAM0;
    double      _wm0, _wc0, _Tm0, _alphaAD0, _Tc0, _xi, _Tonsetr, _Tonsetg;
    double  _cfG, _cfR, _cfAM, _cg, _cr, _cAM, _TaylorQuineyG,_TaylorQuineyR,_TaylorQuineyAM;
    double      _zAM;
    
    std::vector<double> _TcTmWcWm;
    std::vector<double> _alphaParam;
 
    scalarFunction *_temFunc_alphaG;
    scalarFunction *_temFunc_alphaR;
    scalarFunction *_temFunc_alphaAM;
    scalarFunction *_temFunc_kqG;
    scalarFunction *_temFunc_kqR;
    scalarFunction *_temFunc_kqAM;
    scalarFunction *_temFunc_cG;
    scalarFunction *_temFunc_cR;
    scalarFunction *_temFunc_cAM;
    
    scalarFunction *_yieldCrystallinityG;
    scalarFunction *_yieldCrystallinityR;
    scalarFunction *_yieldCrystallinityAM;
 
    scalarFunction *_crystallizationVolumeRate;

    elasticPotential *_EPFunc_R;
    elasticPotential *_EPFunc_AM;

public:
    mlawPhenomenologicalSMP(const int num, const double rho, const double alpha, const double beta, const double gamma, const double t0, 
                            const double Kxg, const double Kyg, const double Kzg, const double Kxr, const double Kyr, const double Kzr,
                            const double KxAM, const double KyAM, const double KzAM,
                            const double cfG, const double cfR, const double cfAM,
                            const double cg, const double cr, const double cAM, 
                            const double Tm0, const double Tc0, const double xi, const double wm0, const double wc0, 
                            const double alphag0, const double alphar0, const double alphaAM0, const double alphaAD0,
                            const double Eg, const double Er,const double EAM, 
                            const double nug, const double nur, const double nuAM,
                            const double TaylorQuineyG,const double TaylorQuineyR, const double TaylorQuineyAM, 
                            const double zAM);
    virtual void setTime(const double ctime,const double dtime){
      mlawCoupledThermoMechanics::setTime(ctime,dtime);
      lawG->setTime(ctime,dtime);
      lawR->setTime(ctime,dtime);
      lawAM->setTime(ctime,dtime);
    }
    virtual void setMacroSolver(const nonLinearMechSolver* sv)
    {
      mlawCoupledThermoMechanics::setMacroSolver( sv);
      if(lawG==NULL) Msg::Error("mlawPhenomenologicalSMP: setMacroSolver G not defined");
        lawG->setMacroSolver( sv);
 
      if(lawR==NULL) Msg::Error("mlawPhenomenologicalSMP: setMacroSolver R not defined");
        lawR->setMacroSolver( sv);
    
      if(lawAM==NULL) Msg::Error("mlawPhenomenologicalSMP: setMacroSolver AM not defined");
        lawAM->setMacroSolver( sv);
  
    }
    virtual void setStrainOrder(const int order)
    {
      if(lawG==NULL) Msg::Error("mlawPhenomenologicalSMP: mlawPowerYieldHyper G not defined");
      if(lawR==NULL) Msg::Error("mlawPhenomenologicalSMP: mlawPowerYieldHyper R not defined");
      if(lawAM==NULL) Msg::Error("mlawPhenomenologicalSMP: mlawPowerYieldHyper AM not defined");
      lawG->setStrainOrder(order);
      lawR->setStrainOrder(order);
      lawAM->setStrainOrder(order);
    }
    
    virtual void setViscoelasticMethod(const int method)
    {
      if(lawG==NULL) Msg::Error("mlawPhenomenologicalSMP: mlawPowerYieldHyper G not defined");
      if(lawR==NULL) Msg::Error("mlawPhenomenologicalSMP: mlawPowerYieldHyper R not defined");
      if(lawAM==NULL) Msg::Error("mlawPhenomenologicalSMP: mlawPowerYieldHyper AM not defined");
      lawG->setViscoelasticMethod(method);
      lawR->setViscoelasticMethod(method);
      lawAM->setViscoelasticMethod(method);
     
    }
    virtual void setViscoElasticNumberOfElementG(const int N)
    {
      if(lawG==NULL) Msg::Error("mlawPhenomenologicalSMP: mlawPowerYieldHyper G not defined");
      lawG->setViscoElasticNumberOfElement(N);
    }
    virtual void setViscoElasticNumberOfElementR(const int N)
    {
      if(lawR==NULL) Msg::Error("mlawPhenomenologicalSMP: mlawPowerYieldHyper R not defined");
      lawR->setViscoElasticNumberOfElement(N);
    }
    virtual void setViscoElasticNumberOfElementAM(const int N)
    {
      if(lawAM==NULL) Msg::Error("mlawPhenomenologicalSMP: mlawPowerYieldHyper AM not defined");
      lawAM->setViscoElasticNumberOfElement(N);
    }
    virtual void setViscoElasticDataG(const int i, const double Ei, const double taui)
    {
      if(lawG==NULL) Msg::Error("mlawPhenomenologicalSMP: mlawPowerYieldHyper G not defined");
      lawG->setViscoElasticData( i,  Ei, taui);
    }
    virtual void setViscoElasticDataR(const int i, const double Ei, const double taui)
    {
      if(lawR==NULL) Msg::Error("mlawPhenomenologicalSMP: mlawPowerYieldHyper R not defined");
      lawR->setViscoElasticData( i,  Ei, taui);
    }
    virtual void setViscoElasticDataAM(const int i, const double Ei, const double taui)
    {
      if(lawAM==NULL) Msg::Error("mlawPhenomenologicalSMP: mlawPowerYieldHyper AM not defined");
      lawAM->setViscoElasticData( i,  Ei, taui);
    }
    virtual void setViscoElasticData_BulkG(const int i, const double Ei, const double taui)
    {
      if(lawG==NULL) Msg::Error("mlawPhenomenologicalSMP: mlawPowerYieldHyper G not defined");
      lawG->setViscoElasticData_Bulk( i,  Ei, taui);
    }
    virtual void setViscoElasticData_BulkR(const int i, const double Ei, const double taui)
    {
      if(lawR==NULL) Msg::Error("mlawPhenomenologicalSMP: mlawPowerYieldHyper R not defined");
      lawR->setViscoElasticData_Bulk( i,  Ei, taui);
    }
    virtual void setViscoElasticData_BulkAM(const int i, const double Ei, const double taui)
    {
      if(lawAM==NULL) Msg::Error("mlawPhenomenologicalSMP: mlawPowerYieldHyper AM not defined");
      lawAM->setViscoElasticData_Bulk( i,  Ei, taui);
    }
    virtual void setViscoElasticData_ShearG(const int i, const double Ei, const double taui)
    {
      if(lawG==NULL) Msg::Error("mlawPhenomenologicalSMP: mlawPowerYieldHyper G not defined");
      lawG->setViscoElasticData_Shear( i,  Ei, taui);
    }
    virtual void setViscoElasticData_ShearR(const int i, const double Ei, const double taui)
    {
      if(lawR==NULL) Msg::Error("mlawPhenomenologicalSMP: mlawPowerYieldHyper R not defined");
      lawR->setViscoElasticData_Shear( i,  Ei, taui);
    }
    virtual void setViscoElasticData_ShearAM(const int i, const double Ei, const double taui)
    {
      if(lawAM==NULL) Msg::Error("mlawPhenomenologicalSMP: mlawPowerYieldHyper AM not defined");
      lawAM->setViscoElasticData_Shear( i,  Ei, taui);
    }
    virtual void setViscoElasticDataG(const std::string filename)
    {
      if(lawG==NULL) Msg::Error("mlawPhenomenologicalSMP: mlawPowerYieldHyper G not defined");
      lawG->setViscoElasticData(filename);
    }
    virtual void setViscoElasticDataR(const std::string filename)
    {
      if(lawR==NULL) Msg::Error("mlawPhenomenologicalSMP: mlawPowerYieldHyper R not defined");
      lawR->setViscoElasticData( filename);
    }
    virtual void setViscoElasticDataAM(const std::string filename)
    {
      if(lawAM==NULL) Msg::Error("mlawPhenomenologicalSMP: mlawPowerYieldHyper AM not defined");
      lawAM->setViscoElasticData( filename);
    }       
    virtual int getViscoElasticNumberOfElementG() const
    {
      if(lawG==NULL) Msg::Error("mlawPhenomenologicalSMP: mlawPowerYieldHyper G not defined");
      return lawG->getViscoElasticNumberOfElement();
    };
    virtual int getViscoElasticNumberOfElementR() const
    {
      if(lawR==NULL) Msg::Error("mlawPhenomenologicalSMP: mlawPowerYieldHyper R not defined");
      return lawR->getViscoElasticNumberOfElement();
    };
    virtual int getViscoElasticNumberOfElementAM() const
    {
      if(lawAM==NULL) Msg::Error("mlawPhenomenologicalSMP: mlawPowerYieldHyper AM not defined");
      return lawAM->getViscoElasticNumberOfElement();
    };
    virtual void setPowerFactorG(const double n) 
    {
      if(lawG==NULL) Msg::Error("mlawPhenomenologicalSMP: mlawPowerYieldHyper G not defined");
      lawG->setPowerFactor(n);
    };
    virtual void setPowerFactorR(const double n) 
    {
      if(lawR==NULL) Msg::Error("mlawPhenomenologicalSMP: mlawPowerYieldHyper R not defined");
      lawR->setPowerFactor(n);
    };
    virtual void setPowerFactorAM(const double n) 
    {
      if(lawAM==NULL) Msg::Error("mlawPhenomenologicalSMP: mlawPowerYieldHyper AM not defined");
      lawAM->setPowerFactor(n);
    };
    virtual void nonAssociatedFlowRuleFactorG(const double n) 
    {
      if(lawG==NULL) Msg::Error("mlawPhenomenologicalSMP: mlawPowerYieldHyper G not defined");
      lawG->nonAssociatedFlowRuleFactor(n);
    };
    virtual void nonAssociatedFlowRuleFactorR(const double n) 
    {
      if(lawR==NULL) Msg::Error("mlawPhenomenologicalSMP: mlawPowerYieldHyper R not defined");
      lawR->nonAssociatedFlowRuleFactor(n);
    };
    virtual void nonAssociatedFlowRuleFactorAM(const double n)
    {
      if(lawAM==NULL) Msg::Error("mlawPhenomenologicalSMP: mlawPowerYieldHyper AM not defined");
      lawAM->nonAssociatedFlowRuleFactor(n);
    };
    virtual void setPlasticPoissonRatioG(const double n) 
    {
      if(lawG==NULL) Msg::Error("mlawPhenomenologicalSMP: mlawPowerYieldHyper G not defined");
      lawG->setPlasticPoissonRatio(n);
    };
    virtual void setPlasticPoissonRatioR(const double n) 
    {
      if(lawR==NULL) Msg::Error("mlawPhenomenologicalSMP: mlawPowerYieldHyper R not defined");
      lawR->setPlasticPoissonRatio(n);
    };
    virtual void setPlasticPoissonRatioAM(const double n) 
    {
      if(lawAM==NULL) Msg::Error("mlawPhenomenologicalSMP: mlawPowerYieldHyper AM not defined");
      lawAM->setPlasticPoissonRatio(n);
    };
    virtual void setNonAssociatedFlowG(const bool n) 
    {
      if(lawG==NULL) Msg::Error("mlawPhenomenologicalSMP: mlawPowerYieldHyper G not defined");
      lawG->setNonAssociatedFlow(n);
    };
    virtual void setNonAssociatedFlowR(const bool n) 
    {
      if(lawR==NULL) Msg::Error("mlawPhenomenologicalSMP: mlawPowerYieldHyper R not defined");
      lawR->setNonAssociatedFlow(n);
    };
    virtual void setNonAssociatedFlowAM(const bool n) 
    {
      if(lawAM==NULL) Msg::Error("mlawPhenomenologicalSMP: mlawPowerYieldHyper AM not defined");
      lawAM->setNonAssociatedFlow(n);
    };

    virtual const J2IsotropicHardening * getJ2IsotropicHardeningCompressionG() const {
      if(lawG==NULL) Msg::Error("mlawPhenomenologicalSMP: mlawPowerYieldHyper G not defined");
      return lawG->getConstRefToCompressionHardening();
    };
    virtual void setJ2IsotropicHardeningCompressionG(const J2IsotropicHardening& isoHard)  {
      if(lawG==NULL) Msg::Error("mlawPhenomenologicalSMP: mlawPowerYieldHyper G not defined");
      lawG->setCompressionHardening(isoHard);
    };
    virtual const J2IsotropicHardening * getJ2IsotropicHardeningTractionG() const {
      if(lawG==NULL) Msg::Error("mlawPhenomenologicalSMP: mlawPowerYieldHyper G not defined");
      return lawG->getConstRefToTractionHardening();
    };
    virtual void setJ2IsotropicHardeningTractionG(const J2IsotropicHardening& isoHard)  {
      if(lawG==NULL) Msg::Error("mlawPhenomenologicalSMP: mlawPowerYieldHyper G not defined");
      lawG->setTractionHardening(isoHard);
    };
    virtual const kinematicHardening * getKinematicHardeningG() const {
      if(lawG==NULL) Msg::Error("mlawPhenomenologicalSMP: mlawPowerYieldHyper G not defined");
      return lawG->getConstRefToKinematicHardening();
    };
    virtual void setKinematicHardeningG(const kinematicHardening& isoHard)  {
      if(lawG==NULL) Msg::Error("mlawPhenomenologicalSMP: mlawPowerYieldHyper G not defined");
      lawG->setKinematicHardening(isoHard);
    };
    
    virtual const J2IsotropicHardening * getJ2IsotropicHardeningCompressionR() const {
      if(lawR==NULL) Msg::Error("mlawPhenomenologicalSMP: mlawPowerYieldHyper R not defined");
      return lawR->getConstRefToCompressionHardening();
    };
    virtual void setJ2IsotropicHardeningCompressionR(const J2IsotropicHardening& isoHard)  {
      if(lawR==NULL) Msg::Error("mlawPhenomenologicalSMP: mlawPowerYieldHyper R not defined");
      lawR->setCompressionHardening(isoHard);
    };
    virtual const J2IsotropicHardening * getJ2IsotropicHardeningTractionR() const {
      if(lawR==NULL) Msg::Error("mlawPhenomenologicalSMP: mlawPowerYieldHyper R not defined");
      return lawR->getConstRefToTractionHardening();
    };
    virtual void setJ2IsotropicHardeningTractionR(const J2IsotropicHardening& isoHard)  {
      if(lawR==NULL) Msg::Error("mlawPhenomenologicalSMP: mlawPowerYieldHyper R not defined");
      lawR->setTractionHardening(isoHard);
    };
    virtual const kinematicHardening * getKinematicHardeningR() const {
      if(lawR==NULL) Msg::Error("mlawPhenomenologicalSMP: mlawPowerYieldHyper R not defined");
      return lawR->getConstRefToKinematicHardening();
    };
    virtual void setKinematicHardeningR(const kinematicHardening& isoHard)  {
      if(lawR==NULL) Msg::Error("mlawPhenomenologicalSMP: mlawPowerYieldHyper R not defined");
      lawR->setKinematicHardening(isoHard);
    };

    virtual const J2IsotropicHardening * getJ2IsotropicHardeningCompressionAM() const {
      if(lawAM==NULL) Msg::Error("mlawPhenomenologicalSMP: mlawPowerYieldHyper AM not defined");
      return lawAM->getConstRefToCompressionHardening();
    };
    virtual void setJ2IsotropicHardeningCompressionAM(const J2IsotropicHardening& isoHard)  {
      if(lawAM==NULL) Msg::Error("mlawPhenomenologicalSMP: mlawPowerYieldHyper AM not defined");
      lawAM->setCompressionHardening(isoHard);
    };
    virtual const J2IsotropicHardening * getJ2IsotropicHardeningTractionAM() const {
      if(lawAM==NULL) Msg::Error("mlawPhenomenologicalSMP: mlawPowerYieldHyper AM not defined");
      return lawAM->getConstRefToTractionHardening();
    };
    virtual void setJ2IsotropicHardeningTractionAM(const J2IsotropicHardening& isoHard) {
      if(lawAM==NULL) Msg::Error("mlawPhenomenologicalSMP: mlawPowerYieldHyper AM not defined");
      lawAM->setTractionHardening(isoHard);
    };
    virtual const kinematicHardening * getKinematicHardeningAM() const {
      if(lawAM==NULL) Msg::Error("mlawPhenomenologicalSMP: mlawPowerYieldHyper AM not defined");
      return lawAM->getConstRefToKinematicHardening();
    };
    virtual void setKinematicHardeningAM(const kinematicHardening& isoHard)  {
      if(lawAM==NULL) Msg::Error("mlawPhenomenologicalSMP: mlawPowerYieldHyper AM not defined");
      lawAM->setKinematicHardening(isoHard);
    };
        
    virtual void setViscosityEffectG(const viscosityLaw& isoHard, const double p) {
      if(lawG==NULL) Msg::Error("mlawPhenomenologicalSMP: mlawPowerYieldHyper G not defined");
      lawG->setViscosityEffect(isoHard,p);
    };
   virtual void setViscosityEffectR(const viscosityLaw& isoHard, const double p)  {
      if(lawR==NULL) Msg::Error("mlawPhenomenologicalSMP: mlawPowerYieldHyper R not defined");
      lawR->setViscosityEffect(isoHard,p);
    };
   virtual void setViscosityEffectAM(const viscosityLaw& isoHard, const double p)  {
      if(lawAM==NULL) Msg::Error("mlawPhenomenologicalSMP: mlawPowerYieldHyper AM not defined");
      lawAM->setViscosityEffect(isoHard,p);
    };
    virtual void setVolumeCorrectionG(double _vc, double _xivc, double _zetavc, double _dc, double _thetadc, double _pidc)  {
      if(lawG==NULL) Msg::Error("mlawPhenomenologicalSMP: mlawPowerYieldHyper G not defined");
      lawG->setVolumeCorrection(_vc, _xivc, _zetavc, _dc, _thetadc, _pidc);
    };
   virtual void setVolumeCorrectionR(double _vc, double _xivc, double _zetavc, double _dc, double _thetadc, double _pidc)  {
      if(lawR==NULL) Msg::Error("mlawPhenomenologicalSMP: mlawPowerYieldHyper R not defined");
      lawR->setVolumeCorrection(_vc, _xivc, _zetavc, _dc, _thetadc, _pidc);
    };
   virtual void setVolumeCorrectionAM(double _vc, double _xivc, double _zetavc, double _dc, double _thetadc, double _pidc)  {
      if(lawAM==NULL) Msg::Error("mlawPhenomenologicalSMP: mlawPowerYieldHyper AM not defined");
      lawAM->setVolumeCorrection(_vc, _xivc, _zetavc, _dc, _thetadc, _pidc);
    };
    virtual bool withEnergyDissipation() const {
      if(lawG==NULL) Msg::Error("mlawPhenomenologicalSMP: mlawPowerYieldHyper G not defined");
      if(lawR==NULL) Msg::Error("mlawPhenomenologicalSMP: mlawPowerYieldHyper R not defined");
      if(lawAM==NULL) Msg::Error("mlawPhenomenologicalSMP: mlawPowerYieldHyper AM not defined");
      if(lawG->withEnergyDissipation() or lawR->withEnergyDissipation() or lawAM->withEnergyDissipation())
        return true;
      return false;
    };




#ifndef SWIG
    mlawPhenomenologicalSMP(const mlawPhenomenologicalSMP &source);
    mlawPhenomenologicalSMP& operator=(const materialLaw &source);
    virtual ~mlawPhenomenologicalSMP() 
    {
      if(lawG!=NULL) delete lawG;
      if(lawR!=NULL) delete lawR;
      if(lawAM!=NULL) delete lawAM;


      if(_temFunc_alphaG!=NULL) delete _temFunc_alphaG;
      if(_temFunc_alphaR!=NULL) delete _temFunc_alphaR;
      if(_temFunc_alphaAM!=NULL) delete _temFunc_alphaAM;
 
      if(_temFunc_kqG!=NULL) delete _temFunc_kqG;
      if(_temFunc_kqR!=NULL) delete _temFunc_kqR;
      if(_temFunc_kqAM!=NULL) delete _temFunc_kqAM;

      if(_temFunc_cG!=NULL) delete _temFunc_cG;
      if(_temFunc_cR!=NULL) delete _temFunc_cR;
      if(_temFunc_cAM!=NULL) delete _temFunc_cAM;
 
      if(_yieldCrystallinityG!=NULL) delete _yieldCrystallinityG;
      if(_yieldCrystallinityR!=NULL) delete _yieldCrystallinityR;
      if(_yieldCrystallinityAM!=NULL) delete _yieldCrystallinityAM;

      if(_crystallizationVolumeRate!=NULL) delete _crystallizationVolumeRate;

      if(_EPFunc_AM!=NULL) delete _EPFunc_AM; 
      if(_EPFunc_R!=NULL) delete _EPFunc_R; 
    };
    virtual materialLaw* clone() const {return new mlawPhenomenologicalSMP(*this);};
    virtual double density() const{
        return this->_rho;
    };

    virtual void checkInternalState(IPVariable* ipv, const IPVariable* ipvprev) const {};
    virtual matname getType() const                                 {return materialLaw::PhenomenologicalSMP;}// function of materialLaw
    virtual void createIPState(IPStateBase* &ips, bool hasBodyForce,  const bool* state_=NULL, const MElement *ele=NULL, const int nbFF_=0,
                                                                                                const IntPt *GP=NULL, const int gpt = 0) const;


    virtual void get_kqg(double T, STensor3& condg, STensor3* dcondg = NULL) const
    {
        condg(0,0)  = _kqg(0,0)*_temFunc_kqG->getVal(T);
        condg(1,1)  = _kqg(1,1)*_temFunc_kqG->getVal(T);
        condg(2,2)  = _kqg(2,2)*_temFunc_kqG->getVal(T);
        if(dcondg != NULL)
        {
            STensor3 A;
            A(0,0)  = _kqg(0,0)*_temFunc_kqG->getDiff(T);
            A(1,1)  = _kqg(1,1)*_temFunc_kqG->getDiff(T);
            A(2,2)  = _kqg(2,2)*_temFunc_kqG->getDiff(T);
            *dcondg = A;
        }
    }

    virtual void get_kqr(const double T, STensor3& condr, STensor3* dcondr = NULL) const
    {
        condr(0,0)  = _kqr(0,0)*_temFunc_kqR->getVal(T);
        condr(1,1)  = _kqr(1,1)*_temFunc_kqR->getVal(T);
        condr(2,2)  = _kqr(2,2)*_temFunc_kqR->getVal(T);
        if(dcondr != NULL)
        {
            STensor3 A;
            A(0,0)  = _kqr(0,0)*_temFunc_kqR->getDiff(T);
            A(1,1)  = _kqr(1,1)*_temFunc_kqR->getDiff(T);
            A(2,2)  = _kqr(2,2)*_temFunc_kqR->getDiff(T);
            *dcondr = A;
        }
    }
    virtual void get_kqAM (double T, STensor3& condAM, STensor3* dcondAM = NULL) const
    {
        condAM(0,0)  = _kqAM(0,0)*_temFunc_kqAM->getVal(T);
        condAM(1,1)  = _kqAM(1,1)*_temFunc_kqAM->getVal(T);
        condAM(2,2)  = _kqAM(2,2)*_temFunc_kqAM->getVal(T);
        if(dcondAM != NULL)
        {
            STensor3 A;
            A(0,0)  = _kqAM(0,0)*_temFunc_kqAM->getDiff(T);
            A(1,1)  = _kqAM(1,1)*_temFunc_kqAM->getDiff(T);
            A(2,2)  = _kqAM(2,2)*_temFunc_kqAM->getDiff(T);
            *dcondAM = A;
        }
    }


    virtual double get_alphaG (double T, double *dalphag=NULL) const
    {
        double alphagT = _alphag0*_temFunc_alphaG->getVal(T);
        if(dalphag!=NULL)
        {
            *dalphag=_alphag0*_temFunc_alphaG->getDiff(T);
        }
        return alphagT;
    }

    virtual double get_alphaR (double T, double *dalphar=NULL) const
    {
        double alpharT = _alphar0*_temFunc_alphaR->getVal(T);
        if(dalphar!=NULL)
        {
            *dalphar=_alphar0*_temFunc_alphaR->getDiff(T);
        }
        return alpharT;
    }
    virtual double get_alphaAM (double T, double *dalphaAM=NULL) const
    {
        double alphaAMT = _alphaAM0*_temFunc_alphaAM->getVal(T);
        if(dalphaAM!=NULL)
        {
            *dalphaAM=_alphaAM0*_temFunc_alphaAM->getDiff(T);
        }
        return alphaAMT;
    }

    virtual double get_alphaAM (const double alphaIJ, const double T, double *dalphaAM=NULL) const
    {
        double alphaAMT = alphaIJ*_temFunc_alphaAM->getVal(T);
        if(dalphaAM!=NULL)
        {
            *dalphaAM=alphaIJ*_temFunc_alphaAM->getDiff(T);
        }
        return alphaAMT;
    }

    virtual double get_cgT (double T, double *dcg=NULL) const
    {
        double cgT = _cg*_temFunc_cG->getVal(T);
        if(dcg!=NULL)
        {
            *dcg=_cg*_temFunc_cG->getDiff(T);
        }
        return cgT;
    }

    virtual double get_crT (double T, double *dcr=NULL) const
    {
        double crT = _cr*_temFunc_cR->getVal(T);
        if(dcr!=NULL)
        {
            *dcr=_cr*_temFunc_cR->getDiff(T);
        }
        return crT;
    }
    virtual double get_cAMT (double T, double *dcAM=NULL) const
    {
        double cAMT = _cAM*_temFunc_cAM->getVal(T);
        if(dcAM!=NULL)
        {
            *dcAM=_cAM*_temFunc_cAM->getDiff(T);
        }
        return cAMT;
    }


    virtual void setTemperatureFunction_ThermalExpansionCoefficient_glassy(const scalarFunction& Tfunc)
    {
        if (_temFunc_alphaG != NULL) delete _temFunc_alphaG;
        _temFunc_alphaG = Tfunc.clone();
    }

    virtual void setTemperatureFunction_ThermalExpansionCoefficient_rubbery(const scalarFunction& Tfunc)
    {
        if (_temFunc_alphaR != NULL) delete _temFunc_alphaR;
        _temFunc_alphaR = Tfunc.clone();
    }

    virtual void setTemperatureFunction_ThermalExpansionCoefficient_AM(const scalarFunction& Tfunc)
    {
        if (_temFunc_alphaAM != NULL) delete _temFunc_alphaAM;
        _temFunc_alphaAM = Tfunc.clone();
    }

    virtual void setTemperatureFunction_ThermalConductivity_glassy(const scalarFunction& Tfunc)
    {
        if (_temFunc_kqG != NULL) delete _temFunc_kqG;
        _temFunc_kqG = Tfunc.clone();
    }

    virtual void setTemperatureFunction_ThermalConductivity_rubbery(const scalarFunction& Tfunc)
    {
        if (_temFunc_kqR != NULL) delete _temFunc_kqR;
        _temFunc_kqR = Tfunc.clone();
    }

    virtual void setTemperatureFunction_ThermalConductivity_AM(const scalarFunction& Tfunc)
    {
        if (_temFunc_kqAM != NULL) delete _temFunc_kqAM;
        _temFunc_kqAM = Tfunc.clone();
    }

    virtual void setTemperatureFunction_Cp_glassy(const scalarFunction& Tfunc)
    {
        if (_temFunc_cG != NULL) delete _temFunc_cG;
        _temFunc_cG = Tfunc.clone();
    }

    virtual void setTemperatureFunction_Cp_rubbery(const scalarFunction& Tfunc)
    {
        if (_temFunc_cR != NULL) delete _temFunc_cR;
        _temFunc_cR = Tfunc.clone();
    }
    
    virtual void setTemperatureFunction_Cp_AM(const scalarFunction& Tfunc)
    {
        if (_temFunc_cAM != NULL) delete _temFunc_cAM;
        _temFunc_cAM = Tfunc.clone();
    }
    
    virtual void setFunctionYieldCrystallinityG(const scalarFunction& Tfunc)
    {
      if(_yieldCrystallinityG!=NULL) delete _yieldCrystallinityG;
      _yieldCrystallinityG=Tfunc.clone();
    }

    virtual void setFunctionYieldCrystallinityR(const scalarFunction& Tfunc)
    {
      if(_yieldCrystallinityR!=NULL) delete _yieldCrystallinityR;
      _yieldCrystallinityR=Tfunc.clone();
    }

    virtual void setFunctionYieldCrystallinityAM(const scalarFunction& Tfunc)
    {
      if(_yieldCrystallinityAM!=NULL) delete _yieldCrystallinityAM;
      _yieldCrystallinityAM=Tfunc.clone();
    }
    
    virtual void setFunctionCrystallizationVolumeRate( const scalarFunction & Tfunc)
    {
      if(_crystallizationVolumeRate!=NULL) delete _crystallizationVolumeRate;
      _crystallizationVolumeRate=Tfunc.clone();
       
    }
    
    virtual void getZgFunction_Sy0(const scalarFunction& Tfunc, const double zg, double &wp, double &dwpdz) const;


    virtual void get_zg (const double T, const double T0, const IPPhenomenologicalSMP *q0, IPPhenomenologicalSMP *q1,
                         const double T_t, const double dTtdT, const STensor3& dTtdEveR, const STensor3& dTtdF,
                         const  double wt, const double dwtdT, const STensor3& dwtdEveR, const STensor3& dwtdF,
                         double& zg, double& dzgdT, STensor3& dzgdEveR, STensor3& dzgdF) const
    {
        dzgdT=0.;
        STensorOperation::zero(dzgdEveR);
        STensorOperation::zero(dzgdF);

        const double &Tt0   =q0->getConstRefToTt();
        const double &wt0   =q0->getConstRefToWt();
        const double &PDF0  =q0->getConstRefToPDF();
        const double &zg0   =q0->getzg();
        double &PDF         =q1->getRefToPDF();
        zg  =zg0;


        PDF           = 1./wt/std::sqrt(2.*M_PI)*std::exp(-1./2*pow((T-T_t)/wt,2));
        double PDFaux = 1./wt/std::sqrt(2.*M_PI)*std::exp(-1./2*pow((T0-T_t)/wt,2));
        double zg_aux = 1.-1./2*(1. + std::erf((T0-T_t)/wt/std::sqrt(2.)));

        double weight=1., dweightdT=0.;
        if(T_t==Tt0 && wt==wt0){
            weight=1.;
        }else{
            if(T-T0<-toleranceOnT){
                if(zg_aux==1.){
                    weight=1.;
                }else{
                    weight= (1.-zg0)/(1.-zg_aux);
                }
            }
            if(T-T0>toleranceOnT){
                if(zg_aux==0.){
                    weight=0.;
                }else{
                    weight= (zg0)/(zg_aux);
                }
            }
        }

        double dPDFdT = -PDF/wt - PDF*((T-T_t)/wt)*((wt - T*dwtdT - dTtdT*wt + T_t*dwtdT)/wt/wt);
        double PDFnorm, PDFauxnorm, dPDFnormdT, dPDFauxnormdT=0.;
        PDFnorm     = PDF*weight;
        PDFauxnorm  = PDFaux*weight;
        dPDFnormdT  = dPDFdT*weight + PDF*dweightdT;

        double delzg = 0.;
        if(T-T0<-toleranceOnT || T-T0>toleranceOnT){
            delzg = (PDFnorm + PDFauxnorm)*(T0-T)/2.;
            zg    = zg0 + delzg;
            dzgdT = (dPDFnormdT + dPDFauxnormdT)*(T0-T)/2. -(PDFnorm + PDFauxnorm)/2.;
        }
//        if(zg<toleranceOnZg){zg=0.;}
//        if(zg>1.-toleranceOnZg){zg=1.;}
    }


    virtual void get_Tt(const IPPhenomenologicalSMP* q0, const double T, const double T0,
                          const double Tcrys, const double Tmelt, const double Wcrys, const double Wmelt,
                          const double dTcrysdT, const double dTmeltdT, const double dWcrysdT, const double dWmeltdT,
                          const STensor3 &dTcrysdE, const STensor3 &dTmeltdE, const STensor3 &dWcrysdE, const STensor3 &dWmeltdE,
                          const STensor3 &dTcrysdF, const STensor3 &dTmeltdF, const STensor3 &dWcrysdF, const STensor3 &dWmeltdF,
                          double &T_t, double &dTtdT, STensor3 &dTtdE, STensor3 &dTtdF,
                          double &wt, double &dwtdT, STensor3 &dwtdE, STensor3 &dwtdF) const
    {
        const double &Tt0   =q0->getConstRefToTt();
        const double &wt0   =q0->getConstRefToWt();
        if(T-T0<-toleranceOnT){
            T_t  =Tcrys;
            wt   =Wcrys;
            dTtdT=dTcrysdT;
            dwtdT=dWcrysdT;
            dTtdE=dTcrysdE;
            dwtdE=dWcrysdE;
            dTtdF=dTcrysdF;
            dwtdF=dWcrysdF;
        }else if(T - T0 > toleranceOnT){
            T_t  =Tmelt;
            wt   =Wmelt;
            dTtdT=dTmeltdT;
            dwtdT=dWmeltdT;
            dTtdE=dTmeltdE;
            dwtdE=dWmeltdE;
            dTtdF=dTmeltdF;
            dwtdF=dWmeltdF;
        }else{
            T_t  =Tt0;
            wt   =wt0;
            dTtdT=0.;
            dwtdT=0.;
            if(T_t==Tcrys){
                dTtdE=dTcrysdE;
                dwtdE=dWcrysdE;
                dTtdF=dTcrysdF;
                dwtdF=dWcrysdF;
            }else{
                dTtdE=dTmeltdE;
                dwtdE=dWmeltdE;
                dTtdF=dTmeltdF;
                dwtdF=dWmeltdF;
            }
        }
    }

    virtual void setTemperatureFunction_alphaAM(const scalarFunction& Tfunc)
    {
        if (_temFunc_alphaAM != NULL)
            delete _temFunc_alphaAM;
        _temFunc_alphaAM = Tfunc.clone();
    }

    virtual void setTemperatureFunction_kqAM(const scalarFunction& Tfunc)
    {
        if (_temFunc_kqAM != NULL)
            delete _temFunc_kqAM;
        _temFunc_kqAM = Tfunc.clone();
    }

    virtual void setTemperatureFunction_cAM(const scalarFunction& Tfunc)
    {
        if (_temFunc_cAM != NULL) delete _temFunc_cAM;
        _temFunc_cAM = Tfunc.clone();
    }

    virtual void setElasticPotentialFunctionAM(const elasticPotential& EPfunc)
    {
        if (_EPFunc_AM != NULL) delete _EPFunc_AM;
        _EPFunc_AM = EPfunc.clone();
    }
    virtual void setElasticPotentialFunctionR(const elasticPotential& EPfunc)
    {
        if (_EPFunc_R != NULL) delete _EPFunc_R;
        _EPFunc_R = EPfunc.clone();
    }


    void setTcTmWcWm(const double Atc, const double Btm, const double Cwc, const double Dwm,const double alphatc, const double alphatm, const double alphawc, const double alphawm)
    {
        (_TcTmWcWm)[0]=Atc;
        (_TcTmWcWm)[1]=Btm;
        (_TcTmWcWm)[2]=Cwc;
        (_TcTmWcWm)[3]=Dwm;
        (_TcTmWcWm)[4]=alphatc;
        (_TcTmWcWm)[5]=alphatm;
        (_TcTmWcWm)[6]=alphawc;
        (_TcTmWcWm)[7]=alphawm;
    };

    void setAlphaParam(const double GP, const double RP, const double ADP, const double AMP, const double GN, const double RN, const double ADN, const double AMN,
                       const double alphaGP, const double alphaRP, const double alphaADP, const double alphaAMP, 
                       const double alphaGN, const double alphaRN, const double alphaADN, const double alphaAMN)
    {
        (_alphaParam)[0]=GP;    (_alphaParam)[1]=RP;    (_alphaParam)[2]=ADP;   (_alphaParam)[3]=AMP;
        (_alphaParam)[4]=GN;    (_alphaParam)[5]=RN;    (_alphaParam)[6]=ADN;   (_alphaParam)[7]=AMN;
        (_alphaParam)[8]=alphaGP;    (_alphaParam)[9]=alphaRP;    (_alphaParam)[10]=alphaADP;  (_alphaParam)[11]=alphaAMP;
        (_alphaParam)[12]=alphaGN;   (_alphaParam)[13]=alphaRN;   (_alphaParam)[14]=alphaADN;  (_alphaParam)[15]=alphaAMN;
    };


public:

    double getInitialTemperature()const         {return _t0;};
    double getMeltingTemperature()const         {return _Tm0;};
    double getCrystallizationTemperature()const {return _Tc0;};
    double getMeltingParameter()const           {return _wm0;};
    double getCrystallizationParameter()const   {return _wc0;};
//    double getAlphaTrans()const {return _alphaAD0;};
//    double getTonsetg()const {return _Tonsetg;};
//    double getTonsetr()const {return _Tonsetr;};
    double getExtraDofStoredEnergyPerUnitField(double zg)const;
    //virtual double getInitialExtraDofStoredEnergyPerUnitField() const {getExtraDofStoredEnergyPerUnitField(zg);}

    virtual void constitutive(
        const STensor3      &F0,                   // initial deformation gradient (input @ time n)
        const STensor3      &Fn,                   // updated deformation gradient (input @ time n+1)
        STensor3            &P,                    // updated 1st Piola-Kirchhoff stress tensor (output)
        const IPVariable    *q0,                   // array of initial internal variable
        IPVariable          *q1,                   // updated array of internal variable (in ipvcur on output),
        STensor43           &Tangent,              // constitutive tangents (output)
        const bool          stiff,
        STensor43           *elasticTangent=NULL,
        const bool dTangent =false,
        STensor63* dCalgdeps = NULL
    ) const;

    virtual void constitutive(
        const STensor3   &F0,
        const STensor3   &Fn,
        STensor3         &P,
        const STensor3   &P0,
        const IPVariable *q0,
        IPVariable       *q1,
        STensor43        &Tangent,
        const double     T0,
        double           T,
        const SVector3   &gradT,
        SVector3         &fluxT,
        STensor3         &dPdT,
        STensor3         &dqdgradT,
        SVector3         &dqdT,
        STensor33        &dqdF,
        const bool       stiff,
        double           &wth,
        double           &dwthdt,
        STensor3         &dwthdF,
        double           &mechSource,
        double           &dmechSourcedT,
        STensor3         &dmechSourcedF,
        STensor43        *elasticTangent=NULL
    ) const;
  bool constitutive1(
     const IPPhenomenologicalSMP *q0, IPPhenomenologicalSMP *q1,
     const double T0, const double T, const bool stiff, const STensor3& F0, const STensor3& Fn, 
     const STensor3& FthG0, const STensor3& FthG, const STensor3& FmG0, const STensor3& FmG, const STensor3 &dFthGdT, const STensor43 &dFthGdF,  
     const STensor3& FthR0, const STensor3& FthR, const STensor3& FmR0, const STensor3& FmR, const STensor3 &dFthRdT, const STensor43 &dFthRdF, 
     const STensor3& FthAM0, const STensor3& FthAM, const STensor3& FmAM0, const STensor3& FmAM, const STensor3 &dFthAMdT,  const STensor43 &dFthAMdF,
     double& PsiG, double &dPlasG, double& dPlasGdT, STensor3& dPlasGdF, double &dViscG, double& dViscGdT, STensor3& dViscGdF, 
     STensor3& PG, STensor3& dPGdT,   STensor43& TangentG,
     double& PsiR, double &dPlasR, double& dPlasRdT, STensor3& dPlasRdF, double &dViscR, double& dViscRdT, STensor3& dViscRdF, 
     STensor3& PR,    STensor3& dPRdT, STensor43& TangentR,
     double& PsiAM, double &dPlasAM, double& dPlasAMdT, STensor3& dPlasAMdF, double &dViscAM, double& dViscAMdT, STensor3& dViscAMdF, 
     STensor3 &PAM, STensor3 &dPAMdT,  STensor43 &TangentAM,
     double &PsiRL2, STensor3 &PRL2, STensor3 &dPRL2dT, STensor43 &TangentRL2,
     double &PsiAML2, STensor3 &PAML2, STensor3 &dPAML2dT, STensor43 &TangentAML2) const;

    bool constitutive2(
     const IPPhenomenologicalSMP *q0, IPPhenomenologicalSMP *q1,
     const double T0, const double T, const bool stiff, const STensor3& F0, const STensor3& Fn, 
     const STensor3& FthG0, const STensor3& FthG, const STensor3& FmG0, const STensor3& FmG, const STensor3 &dFthGdT, const STensor43 &dFthGdF,  
     const STensor3& FthR0, const STensor3& FthR, const STensor3& FmR0, const STensor3& FmR, const STensor3 &dFthRdT, const STensor43 &dFthRdF, 
     const STensor3& FthAM0, const STensor3& FthAM, const STensor3& FmAM0, const STensor3& FmAM, const STensor3 &dFthAMdT,  const STensor43 &dFthAMdF,
     double& PsiG, double &dPlasG, double& dPlasGdT, STensor3& dPlasGdF, double &dViscG, double& dViscGdT, STensor3& dViscGdF, 
     STensor3& PG, STensor3& dPGdT,   STensor43& TangentG,
     double& PsiR, double &dPlasR, double& dPlasRdT, STensor3& dPlasRdF, double &dViscR, double& dViscRdT, STensor3& dViscRdF, 
     STensor3& PR,    STensor3& dPRdT, STensor43& TangentR,
     double& PsiAM, double &dPlasAM, double& dPlasAMdT, STensor3& dPlasAMdF, double &dViscAM, double& dViscAMdT, STensor3& dViscAMdF, 
     STensor3 &PAM, STensor3 &dPAMdT,  STensor43 &TangentAM,
     double &PsiRL2, STensor3 &PRL2, STensor3 &dPRL2dT, STensor43 &TangentRL2,
     double &PsiAML2, STensor3 &PAML2, STensor3 &dPAML2dT, STensor43 &TangentAML2) const;

    bool constitutive3(
     const IPPhenomenologicalSMP *q0, IPPhenomenologicalSMP *q1,
     const double T0, const double T, const bool stiff, const STensor3& F0, const STensor3& Fn, 
     const STensor3& FthG0, const STensor3& FthG, const STensor3& FmG0, const STensor3& FmG, const STensor3 &dFthGdT, const STensor43 &dFthGdF,  
     const STensor3& FthR0, const STensor3& FthR, const STensor3& FmR0, const STensor3& FmR, const STensor3 &dFthRdT, const STensor43 &dFthRdF, 
     const STensor3& FthAM0, const STensor3& FthAM, const STensor3& FmAM0, const STensor3& FmAM, const STensor3 &dFthAMdT,  const STensor43 &dFthAMdF,
     double& PsiG, double &dPlasG, double& dPlasGdT, STensor3& dPlasGdF, double &dViscG, double& dViscGdT, STensor3& dViscGdF, 
     STensor3& PG, STensor3& dPGdT,   STensor43& TangentG,
     double& PsiR, double &dPlasR, double& dPlasRdT, STensor3& dPlasRdF, double &dViscR, double& dViscRdT, STensor3& dViscRdF, 
     STensor3& PR,    STensor3& dPRdT, STensor43& TangentR,
     double& PsiAM, double &dPlasAM, double& dPlasAMdT, STensor3& dPlasAMdF, double &dViscAM, double& dViscAMdT, STensor3& dViscAMdF, 
     STensor3 &PAM, STensor3 &dPAMdT,  STensor43 &TangentAM,
     double &PsiRL2, STensor3 &PRL2, STensor3 &dPRL2dT, STensor43 &TangentRL2,
     double &PsiAML2, STensor3 &PAML2, STensor3 &dPAML2dT, STensor43 &TangentAML2) const;

    const STensor3&  getConductivityTensor()const            {return _k0;};
    virtual const STensor3& getStiff_alphaDilatation()const         {return _Stiff_alphaDilatation;};
    virtual const STensor3& getAlphaDilatation() const { return _alphaDilatation;};
    virtual double soundSpeed()const;
    virtual void initLaws(const std::map<int,materialLaw*> &maplaw) {}; // this law is initialized so nothing to do

protected:
    bool checkDeltaEp  (const STensor3& FfR0,   const STensor3& FfR) const;   //?????

    virtual void HookeTensor(const double KT, const double GT, STensor43& C) const;
    virtual void invHookeTensor(const double KT, const double GT, STensor43& C) const;

    void thermalCalculations
        (const IPPhenomenologicalSMP *q0, IPPhenomenologicalSMP *q1, const STensor3& Fn, const double T0, const double T, const double dh,
         double &zg, double &dzgdT, STensor3 &dzgdF,
         STensor3 &FthG,  STensor3 &dFthGdT,  STensor43& dFthGdF,
         STensor3 &FthR,  STensor3 &dFthRdT,  STensor43& dFthRdF,
         STensor3 &FthAM, STensor3 &dFthAMdT, STensor43& dFthAMdF) const;

    void PhaseTransitionThermalParameters
    (const IPPhenomenologicalSMP *q0, IPPhenomenologicalSMP *q1,
     double &Tcrys, double &Tmelt, double &Wcrys, double &Wmelt) const;
    void PhaseTransitionCrystallisationParameters
        (const IPPhenomenologicalSMP *q0, IPPhenomenologicalSMP *q1,
                 STensor3 &N1N1R, STensor3 &N2N2R, STensor3 &N3N3R,
         double &alphaAD1,  double &alphaAD2,  double &alphaAD3) const;
    void evaluateFParti(const STensor3 &F, const STensor3 &Ff, const STensor3 &Fth, STensor3 &FVEVP) const;
    void assemblePParti(const STensor3 &PVEVP, const STensor3 &Ff, const STensor3 &Fth, const STensor3 &F, STensor3 &P) const;
    void assembleTangentParti(const STensor3 &PVEVP, const STensor43 &dPVEVPdFVEVP, const STensor3 &dPVEVPdT, const STensor3 &Ff, const STensor3 &Fth, const STensor43 &dFthdF, const STensor3 &dFthdT, const STensor3 &F, STensor43 &dPdF, STensor3 &dPdT) const; 
    void assembleDDissParti(const STensor3 &dDissVEVPdFVEVP, double dDissVEVPdT, const STensor3 &Ff, const STensor3 &Fth, const STensor43 &dFthdF, const STensor3 &dFthdT, const STensor3 &F, STensor3 &dDissdF, double &dDissdT) const; 
    
    
    void NumericalEigVectorDerivation(const STensor3 target, const double factor, STensor43 &dN1N1dE, STensor43 &dN2N2dE, STensor43 &dN3N3dE) const;

    void funFthAD
    (const IPPhenomenologicalSMP *q0, IPPhenomenologicalSMP *q1, const STensor3& ADl,
     const STensor3& FthAD0, STensor3& FthAD) const;

    void funFthI
    (const IPPhenomenologicalSMP *q0, IPPhenomenologicalSMP *q1,
     const double T0, const double T, const double Tcrys, const double Tmelt, const double zg, const double dzgdT,
     STensor3& FthI, STensor3& dFthIdT, STensor43& dFthIdF) const;

    //void funEnThAI
    //(const double T, const double TRefR, 
    // const STensor3& Ene, STensor3& strainEffect, STensor3& dStrainEffectdT, STensor43& dStrainEffectdEne, 
    // double anisotropicThermalCoefAmplitude, double anisotropicThermalCoefUnused,
    // double anisotropicThermalAlphaCoefTanhEn, double anisotropicThermalAlphaCoefTanhTemp) const;


    void funEnThAI(const double T, const double TRef, const IPHyperViscoElastic& q0, IPHyperViscoElastic &q1,  
         double anisotropicThermalCoefAmplitudeBulkAM, double anisotropicThermalCoefAmplitudeShearAM, 
         double anisotropicThermalAlphaCoefTanhTempBulkAM, double anisotropicThermalAlphaCoefTanhTempShearAM) const;


    void predictorCorrectori(
    const STensor3& Ffi0, STensor3& Ffi, const STensor3& Fpi0, STensor3& Fpi, const double &gammai0, double &gammai1, const STensor3& A1i0, const STensor3& A2i0,
     const double& B1i0, const double& B2i0, const STensor3& q1i0, const STensor3& q2i0, STensor3& Fvei,
     const char &phase, const std::vector<double> &_ViscParami,
     const STensor3& F0, const STensor3& Fn, const IPPhenomenologicalSMP *q0, IPPhenomenologicalSMP *q1,
     const double dh, const double T0, double T, const double Tdot, const bool stiff,
     const STensor3 &Fthi0, const STensor3 &Fthi, const STensor3 &dFthidT, const STensor43 &dFthidF,
     STensor3& A1i, STensor3& A2i, double& B1i, double& B2i, STensor3& q1i, STensor3& q2i,
     double& dPlasi, double& dPlasidT, STensor3& dPlasidF,
     double& dFrzdi, double& dFrzdidT, STensor3& dFrzdidF,
     double& dVisci, double& dViscidT, STensor3& dViscidF,
     double& Psii, STensor3& Pi, STensor3& dPidT, STensor43& Tangenti) const;


     void tangentfunci(const double T0, const double T, const double Tdot, const double dh, const STensor3 &F0,
                const STensor3& Fthi, const STensor3 &dFthidT, const STensor43 &dFthidF,
                const STensor3& Fn, const STensor3& Fpi0, const STensor3 &Fpi, const STensor3 &Ffi, const STensor3& Evei0,
                const STensor3& Fveipr, const STensor3& Eveipr, const STensor43& dlnCveipr, const STensor3& tauveipr, const double tauveipreq,
                const STensor3& Nipr, const double Deltagammai, const STensor43& dexpDelta_gammaNipr, const double Hi_gamma,
                const STensor3& Fvei, const STensor3& Evei, const STensor43& dlnCvei, const STensor63& ddlnCvei, const STensor3& tauvei, const double tauveieq,
                const double GinfiT, const double KinfiT, const double dKinfiT, const double dGinfiT,const std::vector<double> _ViscParami,
                const STensor3 &A1i0, const STensor3 &A2i0, const double B1i0, const double B2i0,
                const STensor3 &devq1i, const double pq1i, const STensor3 &devq2i, const double pq2i, const STensor3& q1i0, const STensor3& q2i0,
                const double Hi_T,
                double& dPlasidT, STensor3& dPlasidF, double& dFrzdidT, STensor3& dFrzdidF, double& dViscidT, STensor3& dViscidF,
                STensor3& dPidT, STensor43& Tangenti, double anisotropicThermalCoefAmplitude, double anisotropicThermalCoefUnused, 
                double anisotropicThermalAlphaCoefTanhEn, double anisotropicThermalAlphaCoefTanhTemp, 
                double Tref, const STensor3 &strainEffect, const STensor3 &dStrainEffectDt, const STensor43  &dStrainEffectdEne) const;


    void tangentamorphousEP(const STensor3& Fn, const STensor3& F0, const STensor3& FmAM, const bool stiff,
                             const STensor3 &FthAM, const STensor3 &dFthAMdT, const STensor43 &dFthAMdF,
                             double &PsiAML2, STensor3& PAML2, STensor3& dPAML2dT, STensor43& TangentAML2) const;

    void computePi(const STensor3& Fn, const double T,  const double dh,
                   const double KinfiT, const double GinfiT, const double Kei, const double Gei, const std::vector<double> _ViscParami,
                   const STensor3& Evei0, const STensor3& Fthi, const STensor3& Ffi,  const STensor3& Fpi,
                   STensor3& Fvei, STensor3& Evei, STensor3& tauvei, STensor43& dlnCvei, STensor63& ddlnCvei, STensor3& Pi,
                   const STensor3 &A1i0, const STensor3 &A2i0, const double B1i0, const double B2i0,
                   STensor3 &A1i, STensor3 &A2i, double &B1i, double &B2i, STensor3& devq1i, double& pq1i, STensor3& devq2i, double& pq2i, 
                   double anisotropicThermalCoefAmplitude, double anisotropicThermalCoefUnused, double anisotropicThermalAlphaCoefTanhEn, 
                   double anisotropicThermalAlphaCoefTanhTemp, double Tref,  
                   STensor3 &strainEffect, STensor3 &dStrainEffectDt, STensor43  &dStrainEffectdEne) const;


    void computeFrzdSourcei(const STensor3& Ffi0, const STensor3& Ffi, const STensor3& Fpi,
                            const STensor3 &Fvei, const STensor3 &tauvei, const STensor43 &dlnCvei, double &dFrzdi) const;

    void computeViscSourcei(const std::vector<double> _ViscParami,
                            const STensor3& Evei, const STensor3& devq1i, const double pq1i, const STensor3& devq2i, const double pq2i,
                            const STensor3& q1i0, const STensor3& q2i0, double& dVisci)const;

    void computeESourcei
                (const double Kinfi, const double  Ginfi, const std::vector<double> _ViscParami,
                 const STensor3& Evei, const STensor3& devq1i, const double pq1i, const STensor3& devq2i, const double pq2i,
                 double& PsiiElasDEV, double& PsiiREST) const;

#endif // SWIG
};
#endif // MLAWPHENOMENOLOGICALSMP_H_
