//
// C++ random field class
//
// Description: random field for composite material parameters
//
// Author:  <L. Wu>, (C) 2016
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef RANDOMFIELD_H_
#define RANDOMFIELD_H_
#include "SVector3.h"
#include <math.h>
#include <fstream>

// general class for random field
class Yarn_class{

 private:
  double  _OrigX, _OrigY, _OrigZ;
  double _a, _b, _c;
  double _L, _l;
  const char* _WirGeo;
  const char* _Warp_Weft;


 public:
  Yarn_class(const double OrigX, const double OrigY, const double OrigZ, const double a, const double b, const double L, const double l, const char* WirGeo, const char* Warp_Weft);
  Yarn_class(const Yarn_class &source);
  Yarn_class& operator=(const Yarn_class &source);
  virtual Yarn_class* clone() const {return new Yarn_class(*this);};
  ~Yarn_class();

  virtual double WireFunc(const double x, const double b, const double L) const;
  virtual double WireFunc_Tang(const double x, const double b, const double L) const;
//  virtual void Inside_Yarn(const SVector3 &GaussP, double& k, double* V_Rand) const;
  virtual void Inside_Yarn(const double xGlo, const double yGlo, const double zGlo, const double Eul0_mean, double& k, double* V_Rand) const;

  virtual double dfx(const double x, const double xG, const double yG, const double b, const double L) const;

  virtual double gradient_descent(double x0, const double error, const double gamma, const unsigned int max_iters, const double xG, const double yG, const double b, const double L) const;

};




// general class for random field
class Random_Fclass{

 protected:
  SVector3 _Ldomain;     //size of the random filed, or the contour of the Domain
  double _dx, _dy, _dz;
  double _OrigX, _OrigY, _OrigZ;
  double _nx, _ny, _nz;

 public:
  Random_Fclass(const SVector3& Ldomain, const double OrigX, const double OrigY, const double OrigZ, const double dx, const double dy, const double dz);
  Random_Fclass(const Random_Fclass &source);
  Random_Fclass& operator=(const Random_Fclass &source);
  virtual Random_Fclass* clone() const =0;
  ~Random_Fclass();

  virtual void RandomGen(const SVector3 &GaussP, double* V_Rand) = 0;

};



class Random_Feul : public Random_Fclass{

 protected:
  double* _Eul_mean;
  double Eul0_mean;
  double _l;
  double**** _RF_eul;
  char _Geo[256];
  Yarn_class *Weft1, *Weft2, *Weft3, *Warp1, *Warp2, *Warp3; 

 public:
  Random_Feul(const double* mean, const SVector3& Ldomain, const double OrigX, const double OrigY, const double OrigZ, 
              const double dx, const double dy, const double dz, const char* Geo);
  Random_Feul(const Random_Feul &source);
  Random_Feul& operator=(const Random_Fclass &source);
  ~Random_Feul();
  virtual void RandomGen(const SVector3 &GaussP, double* V_Rand);
  virtual Random_Fclass* clone() const {return new Random_Feul(*this);};
};


class Random_FProp : public Random_Fclass{

 protected:
  int _Randnum;
  double**** _RF_Prop;

 public:
  Random_FProp(const int Randnum, const SVector3& Ldomain, const double OrigX, const double OrigY, const double OrigZ, 
               const double dx, const double dy, const double dz, const char*RandProp);

  Random_FProp(const Random_FProp &source);
  Random_FProp& operator=(const Random_Fclass &source);
  ~Random_FProp();
  virtual void RandomGen(const SVector3 &GaussP, double* V_Rand);
  virtual Random_Fclass* clone() const {return new Random_FProp(*this);};
};


Random_Fclass* init_RandF(const double* mean, const SVector3& Ldomain, const double OrigX, const double OrigY, const double OrigZ,
                          const double dx, const double dy, const double dz, const char* Geo);
Random_Fclass* init_RandF(const int Randnum, const SVector3& Ldomain, const double OrigX, const double OrigY, const double OrigZ,
                          const double dx, const double dy, const double dz, const char*RandProp);


Yarn_class* init_Yarn(const double OrigX, const double OrigY, const double OrigZ, const double a, const double b, const double L, const double l, const char* WirGeo, const char* Warp_Weft);




#endif  // RANDOMFIELD_H_

