//
// C++ Interface: material law plus Stochastic, random field
//
// Description: non local damage elasto-plastic law it is a "virtual implementation" you have to define a law
//              in your project which derive from this law. you have to do THE SAME for your IPVariable
//              which have to derive from IPJ2linear (all data in this ipvarible have name beggining by _nld...
//
// Author:  <L. Noels>, (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef MLAWNONLOCALDAMAGE_STOCH_H_
#define MLAWNONLOCALDAMAGE_STOCH_H_
#include <random>
#include "mlaw.h"
#include "STensor3.h"
#include "STensor43.h"
#include "ipNonLocalDamage.h"
#include "material.h"
#include "nonLocalDamageLaw.h"
#include "mlawNonLocalDamage.h"
#include "RandomField.h"
#include "homogenization.h"
class mlawNonLocalDamage_Stoch : public mlawNonLocalDamage
{
 protected:
  int pos_vfi;
  int pos_euler;
  int pos_aspR;
  int pos_ME;
  int pos_Mnu;
  int pos_Msy0;
  int pos_Mhmod1;
  int pos_Mhmod2;
  int pos_Malpha_DP;
  int pos_Mm_DP;
  int pos_Mnup;
  int pos_Mhexp;
  int pos_DamParm1;
  int pos_DamParm2;
  int pos_DamParm3;
  int pos_INCDamParm1;
  int pos_INCDamParm2;
  int pos_INCDamParm3;
  int Randnum;

  int _intpl;

  double _dx, _dy, _dz;    // Increament of coordinates  
  double _OrigX, _OrigY, _OrigZ;  //the origin point coordinates of structure     
  SVector3 _Ldomain;     //size of the random filed, or the contour of the Domain

  mutable double* _Euler_mean;
  mutable double* _Euler_tmp;
  mutable double** _Euler_tmp2;
  mutable double** _Euler_tmp3;

  Random_Fclass*  Reuler;    // class of random field
  Random_Fclass*  RMatProp;  //  random vectors (Vfi, aspR, Mtx(E,nu,sy0,hmod1,hmod2,hexp)) or plus (Euler)

  // set random variables by prepared data file
  fullMatrix<double> _VfMat;
  fullMatrix<double> _aspRMat;

  fullMatrix<double> _E_Mat;
  fullMatrix<double> _nu_Mat;
  fullMatrix<double> _sy0_Mat;
  fullMatrix<double> _alpha_DP_Mat;
  fullMatrix<double> _m_DP_Mat;
  fullMatrix<double> _nup_Mat;

  fullMatrix<double> _hmod1_Mat;
  fullMatrix<double> _hmod2_Mat;
  fullMatrix<double> _hexp_Mat;
  fullMatrix<double> _euler_Mat;

  fullMatrix<double> _dam_Parm1_Mat;
  fullMatrix<double> _dam_Parm2_Mat;
  fullMatrix<double> _dam_Parm3_Mat;


 public:
  mlawNonLocalDamage_Stoch(const int num, const double rho, const char *propName, const double Ori_x, const double Ori_y, const double Ori_z,const double Lx,const double Ly,const double Lz, const double dx,const double dy, const double dz, const double euler0= 0.0, const double euler1= 0.0, const double euler2= 0.0, const char *Geo=NULL, const char *RandProp=NULL);

  mlawNonLocalDamage_Stoch(const int num, const double rho, const char *propName, const double Ori_x, const double Ori_y, const double Ori_z,const double Lx,const double Ly,const double Lz, const char *RandProp, const int intpl);

 #ifndef SWIG
  mlawNonLocalDamage_Stoch(const mlawNonLocalDamage_Stoch &source);
  mlawNonLocalDamage_Stoch& operator=(const materialLaw &source);
  virtual ~mlawNonLocalDamage_Stoch();
  virtual materialLaw* clone() const {return new mlawNonLocalDamage_Stoch(*this);};
  virtual bool withEnergyDissipation() const {return true;};
  // function of materialLaw
  virtual void updateCharacteristicLengthTensor(double** chara_Length, double *statev) const;
  virtual matname getType() const{return materialLaw::nonLocalDamage;}
  virtual void createIPState(const SVector3 &GaussP, IPNonLocalDamage *ivi, IPNonLocalDamage *iv1, IPNonLocalDamage *iv2) const;
  virtual void createIPVariable(const SVector3 &GaussP, IPNonLocalDamage *&ipv, bool hasBodyForce, const MElement *ele,const int nbFF, const IntPt *GP, const int gpt) const;
  virtual bool materialLawHasLocalOrientation() const {return true;}
  virtual void getLocalOrientation(fullVector<double> &GaussP, fullVector<double> &vecEulerAngles) const;
 #endif // SWIG
};

#endif // MLAWNONLOCALDAMAGE_STOCH_H_
