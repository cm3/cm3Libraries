
//
// C++ Interface: material law for user direction
//
// Author:  <Van Dung NGUYEN (C) 2021>
//
// Copyright: See COPYING file that comes with this distribution
//
#include "GmshMessage.h"
#include "UserDirectionGenerator.h"
#include "GmshMessage.h"

UniformDirectionGenerator::UniformDirectionGenerator(double x, double y, double z): UserDirectionGenerator(), direction(x,y,z)
{
  double norm = direction.normalize();
  if (norm == 0)
  {
    Msg::Error("zero vector is added in UniformDirectionGenerator");
  }
}
UniformDirectionGenerator::UniformDirectionGenerator(const UniformDirectionGenerator& src): UserDirectionGenerator(src), direction(src.direction)
{}
void UniformDirectionGenerator::getTransverseDirection(SVector3 &A, const SVector3 &pg) const
{
  A = direction;
};
