#ifndef LAMHOMOGENIZATION_C
#define LAMHOMOGENIZATION_C 1

#ifdef NONLOCALGMSH
#include "Lamhomogenization.h"
#include "matrix_operations.h"
#include "rotations.h"
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "ID.h"
#include "material.h"
#include "elasticlcc.h"
using namespace MFH;
#endif
/*    
                                                         Debut presque 

                                                                                                           */
/*******************************************************************************************************
//Resolution of the localization problem on the 2 plies laminate system
// INPUT:	DE: macro strain increment (given)
// 		DeA, DeB: first guess for the strain increment in each ply
// 		C_n: reference operator (MA->plyA, MB->PlyB) at tn for the computation of cencertration tensor PA
// 		DeA = PA:De	
// OUTPUT: solution DeM and DeI
//		updated state variables in each ply
//		Reference macro operator Cref and derivatives dCref, macro algo tangent operator Calgo 
//      NOTE:   static double can't be used for all tensors, because the function may call itself indirectly.
//********************************************************************************************************/
//***************new tensor added by wu ling: double* dpdE, double* strs_dDdp_bar
int Laminate_2ply(double* DE, Material* A_mat, Material* B_mat, double VA, double* statev_n, double* statev, int nsdv, int idsdv_A, int idsdv_B, double* angles, double** C, double*** dC, double** Calgo, int kinc, int kstep, double dt){

	int i,it,pos,j;
	int last;
	int error,errorlu,errortmp;
        double VB=1.0-VA;
	double res, res_r, res_rr; 
	int corr;
	double odd;		//also used for LU decomposition
	double TOL_NR = 1e-6;

	double* DeA;		//New location is required for each call A strain increment
	double* DeA_r;		//A strain increment of the previous iteration
	double* DeB;		//B strain increment
        double* DE_loc;	        //strain increment in the local frame of the laminate	
	double** R66;		//rotation matrix
        mallocvector(&DE_loc,6);
        mallocvector(&DeA,6);
        mallocvector(&DeA_r,6);
        mallocvector(&DeB,6);
	mallocmatrix(&R66,6,6);

	double* F; 		//residual vector;
	double** J;		//jacobian matrix
	double* F_r;		//residual vector of at the previous iteration
        int* indx;
	mallocvector(&F,6);		
	mallocvector(&F_r,6);
	mallocmatrix(&J,6,6);
	indx = (int*)malloc(6*sizeof(int));

        error=0;
        static bool initialized=false;
	static double** CA=NULL;
	static double** CB=NULL;
	static double** PA=NULL;
	
        if(!initialized)
        {
          initialized=true;
 	  //memory allocation
	  mallocmatrix(&CA,6,6);
	  mallocmatrix(&CB,6,6);
	  mallocmatrix(&PA,6,6);
        }
	cleartens4(R66);
	cleartens4(CA);
	cleartens4(CB);
	cleartens4(PA);
        cleartens2(DE_loc);
        cleartens2(DeA);
	cleartens2(DeA_r);
        cleartens2(DeB);
	cleartens4(J);
        cleartens2(F);
        cleartens2(F_r);

        eul_mat(angles,R66);
        rot2(R66, DE, DE_loc);

	//first guess for DeA, DeB and C
	if(kinc < 2){  //first increment of a step: use macro strain increment and elastic macro operator
	    A_mat->get_elOp(CA);	
	    B_mat->get_elOp(CB);		
	}
	else{	
	    A_mat->get_refOp(&(statev_n[idsdv_A]), CA, kinc, kstep);
            B_mat->get_refOp(&(statev_n[idsdv_B]), CB, kinc, kstep);
	}
        TensPA(VA, CA, CB, PA);
        contraction42(PA, DE_loc, DeA);
        addtens2(1./VB, DE_loc, -VA/VB, DeA, DeB);

	last=0;
	it=0;	
        errortmp = Lam2plyNR(DE_loc, DeA, DeB, A_mat, B_mat, VA, statev_n, statev, idsdv_A, idsdv_B, R66, kinc, kstep, dt, last, F, J, C, dC, Calgo);
        if(errortmp!=0) error=errortmp;
        res = 0.0;
        for(i=0;i<6;i++) res += F[i]*F[i];
        res = sqrt(res);
       
#ifdef NONLOCALGMSH
#else
	printf("1st Misf: %.10e\n",res);
#endif

	if(res<TOL_NR){
		last=1;
	}
	res_r=res;

	//NEWTON-RAPHSON LOOP
	while(res>TOL_NR || last == 1){
            it++;
	    corr=0;
	    ludcmp(J,6,indx,&odd,&errorlu);			//compute LU decomposition of jacobian matrix
	    if(errorlu!=0){
		printf("problem in LU decomposition of Jacobian matrix, iteration: %d\n",it);
		error=1;
                break;
	     }
	     //solve NR system -> F contains now the NR correction
	     lubksb(J,6,indx,F);  		
	     //store the solution at the previous iteration
	     copyvect(DeA,DeA_r,6);
	     copyvect(F,F_r,6);
	     res_r=res;		

	     //update strain increments
	     addtens2(1., DeA, -1., F, DeA);
             addtens2(1./VB, DE_loc, -VA/VB, DeA, DeB);	
		
             //compute new residual
             errortmp = Lam2plyNR(DE_loc, DeA, DeB, A_mat, B_mat, VA, statev_n, statev, idsdv_A, idsdv_B, R66, kinc, kstep, dt, last, F, J, C, dC, Calgo);
             if(errortmp!=0) error=errortmp;
             res = 0.0;
             for(i=0;i<6;i++) res += F[i]*F[i];
             res = sqrt(res);

             if((res<TOL_NR) && (last==0)){
		last=1;
	     }
             else{
		last=0;
	     }
	     //check if residual decreases
             while((res>res_r) && (error==0) && (res>TOL_NR) && (it>1) ){	
		res_rr = res;
		corr++;
		//reduce proposed correction
		for(i=0;i<6;i++) F[i] = 0.5*F_r[i];
		copyvect(F,F_r,6);
		//get back to the result of previous iteration
		copyvect(DeA_r,DeA,6);
		addtens2(1., DeA, -1., F, DeA);
                addtens2(1./VB, DE, -VA/VB, DeA, DeB);
          
		//compute residual with the reduced correction
		errortmp = Lam2plyNR(DE_loc, DeA, DeB, A_mat, B_mat, VA, statev_n, statev, idsdv_A, idsdv_B, R66, kinc, kstep, dt, last, F, J, C, dC, Calgo);
                if(errortmp!=0) error=errortmp;
                 res = 0.0;
                 for(i=0;i<6;i++) res += F[i]*F[i];
                 res = sqrt(res);	 
		 //if corrections are inefficient
		 if((corr > 5) && (fabs(res-res_rr) < 1e-6)){
		    //printf("corrections are inefficient\n");
                    //error=1;
		    break;
		  }
	     }//end while residual > previous residual
#ifdef NONLOCALGMSH	
#else
		printf("iteration %d, Misf: %.10e\n",it,res);
#endif
		if (it>1500){
			//printf("WARNING: too many iterations in Newton-Raphson loop\n");
			
		    if(res<1e-4){	//not too bad - exit anyway
				res=0.0;
		    }
		    else{
                         error=1;
			 printf("No conv in NR loop, residual: %.10e\n",res);
			 res=0.0;
		    }
		    //compute algo tangent anyway...
		    last=1;
	            errortmp = Lam2plyNR(DE_loc, DeA, DeB, A_mat, B_mat, VA, statev_n, statev, idsdv_A, idsdv_B, R66, kinc, kstep, dt, last, F, J, C, dC, Calgo);
	            last=0; //to exit the loop
                    if(errortmp!=0) 
                    error=1;
		}

	}  //end NR loop
        free(DeA);
        free(DeA_r);
        free(DeB);
        free(DE_loc);
        freematrix(R66, 6);

        free(F);		
	free(F_r);
	freematrix(J,6);
	free(indx);
	return error;
}


/**************************************************************************************************************************************************/
 //*****************New tenor added by wu ling: double* dpdE, double* strs_dDdp_bar ****************
int Lam2plyNR(double* DE, double* DeA, double* DeB, Material* A_mat, Material* B_mat, double VA, double* statev_n, double* statev, int idsdv_A, int idsdv_B, double** R66, int kinc, int kstep, double dt, int last, double* F, double** J, double** C, double*** dC, double** Calgo)
{

	int i,j,k,m,n,l;      
	double VB = 1.0-VA;
        int MI[3], MO[3];
	int error=0;
	int errortmp;
        double res;
        double alpha1 = 1;

	double* sig_A;
        double* sig_B;
	double** dFdE;

        double** Calgo_loc, **C_loc;		//algorithmic tangent operators 				
	double** PA;				//Hill's polarization tensor	
	double** CalgoB, **CalgoA;		//algorithmic tangent operators 
	
	double** CB,**CA; 			//effective operators 
	double*** dCAde, ***dCBde;		// ... and derivatives
        double **deAdE, **deBdE;  //temporary matrices
	double** invJ;				//inverse of jacobian matrix


	mallocvector(&sig_A,6);
	mallocvector(&sig_B,6);
	mallocmatrix(&dFdE,6,6);
	mallocmatrix(&C_loc,6,6);
	mallocmatrix(&Calgo_loc,6,6);
	malloctens3(&dCAde,6,6,6);
	malloctens3(&dCBde,6,6,6);

	mallocmatrix(&PA,6,6);


	mallocmatrix(&CA,6,6);
	mallocmatrix(&CB,6,6);
	mallocmatrix(&CalgoA,6,6);
	mallocmatrix(&CalgoB,6,6);
	mallocmatrix(&deAdE,6,6);
	mallocmatrix(&deBdE,6,6);
	mallocmatrix(&invJ,6,6);


	static double** I=NULL;				//identity tensor
        static double **mat1=NULL, **mat2=NULL;
        static double *vec1=NULL, *vec2=NULL, *vec3=NULL, *vec4=NULL;
        static double *vec5=NULL, *vec6=NULL, *vec7=NULL, *vec8=NULL, *vec9=NULL;

        static double** cg_temp=NULL;
          
	//Memory allocation
        static bool initialized=false;
        if(!initialized)
        {
          initialized=true;
 	  mallocmatrix(&I,6,6);
	  mallocvector(&vec1,6);
	  mallocvector(&vec2,6);
	  mallocvector(&vec3,6);
	  mallocvector(&vec4,6);
	  mallocvector(&vec5,6);
	  mallocvector(&vec6,6);
	  mallocvector(&vec7,6);
	  mallocvector(&vec8,6);
	  mallocvector(&vec9,6);
	  mallocmatrix(&mat1,6,6);
	  mallocmatrix(&mat2,6,6);
          mallocmatrix(&cg_temp,3,3);	
        }

        cleartens4(I);
	cleartens6(dCAde);
	cleartens6(dCBde);
        cleartens4(PA);

	cleartens4(CalgoA);
	cleartens4(CalgoB);
	cleartens4(CA);
	cleartens4(CB);
	cleartens6(dCAde);
	cleartens6(dCBde);

	cleartens2(sig_A);
	cleartens2(sig_B);
	cleartens2(vec1);
	cleartens2(vec2);
	cleartens2(vec3);
	cleartens2(vec4);
	cleartens2(vec5);
	cleartens2(vec6);
	cleartens2(vec7);
	cleartens2(vec8);
	cleartens2(vec9);
	cleartens4(mat1);
	cleartens4(mat2);
	cleartens4(deAdE);
	cleartens4(deBdE);
	cleartens4(invJ);

        for(i=0;i<3;i++) cg_temp[i][i] = 1.0;    
	for(i=0;i<6;i++) I[i][i]=1.;
	MI[0] = 0; 
	MI[1] = 1; 
	MI[2] = 3; 
	MO[0] = 2; 
	MO[1] = 4; 
	MO[2] = 5; 

	//RESET TO ZERO...
        cleartens4(J);
        cleartens2(F);
       	
    //************* Begining of computation*****************************
    //***********************************************************************************************
    // UPDATE Material A VARIABLES 
       errortmp=A_mat->constbox(DeA, &(statev_n[idsdv_A + A_mat->get_pos_strs()]),  &(statev[idsdv_A + A_mat->get_pos_strs()]), &(statev_n[idsdv_A]), &(statev[idsdv_A]), CA, dCAde, vec1, mat1, CalgoA, alpha1, vec2, vec3, vec4, cg_temp, vec5, vec6, vec7, cg_temp, vec8, vec9, kinc, kstep,dt);
       if(errortmp!=0) error=1;
    // UPDATE Material B VARIABLES 
       errortmp=B_mat->constbox(DeB, &(statev_n[idsdv_B + B_mat->get_pos_strs()]),  &(statev[idsdv_B + B_mat->get_pos_strs()]), &(statev_n[idsdv_B]), &(statev[idsdv_B]), CB, dCBde, vec1, mat1, CalgoB, alpha1, vec2, vec3, vec4, cg_temp, vec5, vec6, vec7, cg_temp, vec8, vec9, kinc, kstep,dt);
       if(errortmp!=0) error=1;
            
       copyvect(&(statev[idsdv_A + A_mat->get_pos_strs()]), sig_A, 6);
       copyvect(&(statev[idsdv_B + B_mat->get_pos_strs()]), sig_B, 6);
       for(i=0;i<3;i++){
           F[MO[i]] = sig_A[MO[i]] - sig_B[MO[i]] ; 
           F[MI[i]] = DeA[MI[i]] - DE[MI[i]] ;   			
       } 

       //************** compute Jacobian  ***************************** 

       for(i=0;i<3;i++){
           for(j=0;j<3;j++){
               J[MO[i]][MO[j]] = CalgoA[MO[i]][MO[j]] + VA/VB*CalgoB[MO[i]][MO[j]];
	   }
           J[MI[i]][MI[i]] = 1.0;
       }
               
	//COMPUTE MACRO TANGENT OPERATORS, CONTINUUM AND ALGORITHMIC
       if(last){
	   inverse(J,invJ,&errortmp, 6);
	   if(errortmp!=0){
	      printf("Problem in inversing Jacobian matrix in Lam2plyNR\n");
              error=errortmp;
	      return error;
	   }

            cleartens4(dFdE);
	    for(i=0;i<3;i++){
		for(j=0;j<3;j++){
                    dFdE[MO[i]][MI[j]] = CalgoA[MO[i]][MI[j]] + VA/VB*CalgoB[MO[i]][MI[j]];
                    dFdE[MI[i]][MI[j]] -= 1.0;
                }
            }

	    for(i=0;i<3;i++){
		for(j=0;j<6;j++){
                    dFdE[MO[i]][j] -= 1.0/VB*CalgoB[MO[i]][j]; 
                }
            }

	    for(i=0;i<6;i++){
	        for(m=0;m<6;m++){
		    for(j=0;j<6;j++){
                        deAdE[i][j]+= invJ[i][m]*dFdE[m][j];		//warning: absolute value
		    }
                    deBdE[i][j] = VA/VB*deAdE[i][j];
		    deAdE[i][j] = -deAdE[i][j];
		}
                deBdE[i][i] = 1.0/VB + deBdE[i][i];
	    }            

            TensPA(VA, CA, CB, PA);
            cleartens4(C_loc);
            addtens4(1.0, CA, -1.0, CB, mat1);             
            contraction44(mat1, PA, mat2); 
            addtens4(VA, mat2, 1.0, CB, C_loc);  

            //1. COMPUTE ALGORITHMIC TANGENT OPERATOR
            for(i=0;i<6;i++){
		for(j=0;j<6;j++){
                    Calgo_loc[i][j] = 0.0;
    		    for(l=0;l<6;l++){
                        Calgo_loc[i][j] += VA*CalgoA[i][l]*deAdE[l][j] + VB*CalgoB[i][l]*deBdE[l][j];
                    }
                }
             }

             // **********  compute stress of laminate ***************************
            transpose(R66,R66,6,6);  			//transpose rotation matrix for inverse transformation
            for(i=0;i<6;i++){
               vec1[i]= VA*(statev[idsdv_A + A_mat->get_pos_strs()+i]) + VB*(statev[idsdv_B + B_mat->get_pos_strs()+i]);
            }
          //  printf("stressA:, %f7,%f7,%f7,%f7,%f7,%f7\n",statev[idsdv_A + A_mat->get_pos_strs()+0],statev[idsdv_A + A_mat->get_pos_strs()+1],statev[idsdv_A + A_mat->get_pos_strs()+2],statev[idsdv_A + A_mat->get_pos_strs()+3],statev[idsdv_A + A_mat->get_pos_strs()+4],statev[idsdv_A + A_mat->get_pos_strs()+5]);
          //  printf("stressB:, %f7,%f7,%f7,%f7,%f7,%f7\n",statev[idsdv_B + B_mat->get_pos_strs()+0],statev[idsdv_B + B_mat->get_pos_strs()+1],statev[idsdv_B + B_mat->get_pos_strs()+2],statev[idsdv_B + B_mat->get_pos_strs()+3],statev[idsdv_B + B_mat->get_pos_strs()+4],statev[idsdv_B + B_mat->get_pos_strs()+5]);

          //  printf("strainA:, %f7,%f7,%f7,%f7,%f7,%f7\n",statev[idsdv_A + A_mat->get_pos_strn()+0],statev[idsdv_A + A_mat->get_pos_strn()+1],statev[idsdv_A + A_mat->get_pos_strn()+2],statev[idsdv_A + A_mat->get_pos_strn()+3],statev[idsdv_A + A_mat->get_pos_strn()+4],statev[idsdv_A + A_mat->get_pos_strn()+5]);
         //   printf("strainB:, %f7,%f7,%f7,%f7,%f7,%f7\n",statev[idsdv_B + B_mat->get_pos_strn()+0],statev[idsdv_B + B_mat->get_pos_strn()+1],statev[idsdv_B + B_mat->get_pos_strn()+2],statev[idsdv_B + B_mat->get_pos_strn()+3],statev[idsdv_B + B_mat->get_pos_strn()+4],statev[idsdv_B + B_mat->get_pos_strn()+5]);
            rot2(R66, vec1, &(statev[6])); 
	    rot4(R66,Calgo_loc,Calgo); 
	    rot4(R66,C_loc,C); 
            
       }    

       free(sig_A);
       free(sig_B);

       freematrix(C_loc,6);
       freematrix(Calgo_loc,6);

       freetens3(dCAde,6,6);
       freetens3(dCBde,6,6);
       freematrix(dFdE,6);
       freematrix(PA,6);

       freematrix(CA,6);
       freematrix(CB,6);
       freematrix(CalgoA,6);
       freematrix(CalgoB,6);
       freematrix(deAdE,6);
       freematrix(deBdE,6);
       freematrix(invJ,6);
       return error;
}



/************************************************
 * Compute PA **
 * *********************************************/

int TensPA(double VA, double** CA, double** CB, double** PA)
{
      int i,j,k;
      int error=0;
      double VB = 1.0-VA;
      int MI[3], MO[3];
      static double** I=NULL;				//identity tensor
      static double** A1=NULL;	
      static double** invA1=NULL;	
      static double** A2=NULL;
      static double** mat1=NULL;

      static bool initialized=false;
      if(!initialized)
      {
          initialized=true;
          mallocmatrix(&I,6,6);
	  mallocmatrix(&A1,6,6);
	  mallocmatrix(&invA1,6,6);
	  mallocmatrix(&A2,6,6);
	  mallocmatrix(&mat1,6,6);
      }

        cleartens4(I);
        cleartens4(A1);
        cleartens4(invA1);
        cleartens4(A2);
        cleartens4(mat1);

	for(i=0;i<6;i++) I[i][i]=1.;
	MI[0] = 0; 
	MI[1] = 1; 
	MI[2] = 3; 
	MO[0] = 2; 
	MO[1] = 4; 
	MO[2] = 5; 


       addtens4(VA,CB, VB, CA, mat1);                     //mat1 = vaCb+vbCa
       for(i=0;i<3;i++){
           for(j=0;j<3;j++){ 
               A1[MO[i]][MO[j]] = mat1[MO[i]][MO[j]];
           }
           A1[MI[i]][MI[i]] = 1.0;
       }
 
       for(i=0;i<3;i++){
           for(j=0;j<3;j++){ 
               A2[MO[i]][MI[j]] = mat1[MO[i]][MI[j]];
           }
           for(k=0;k<6;k++){       
               A2[MO[i]][k] = CB[MO[i]][k] - A2[MO[i]][k];
           }
           A2[MI[i]][MI[i]] = 1.0;
       }

       inverse(A1,invA1,&error,6);
       if(error!=0){
	   printf("Problem while inversing A1 operator in Lam2plyNR, A1=\n");
	   printmatr(A1,6,6);
           return error;
	}

        contraction44(invA1, A2, PA);  
        return error;   
}    



#endif

