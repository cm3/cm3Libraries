//
// C++ Interface: material law
//
// Description: Gurson elasto-plastic law with non local damage interface
//
// Author:  <L. Noels>, (C) 2013
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef MLAWNONLOCALDAMAGEGURSON_H_
#define MLAWNONLOCALDAMAGEGURSON_H_
#include "mlawNonLocalPorous.h"
#include "scalarFunction.h"
class mlawNonLocalDamageGurson : public mlawNonLocalPorosity
{
  #ifndef SWIG
  protected:
    // Material parameters - gurson yield surface
    double _q1;
    double _q2;
    double _q3;
    
    /// Void state evolution law
    voidStateEvolutionLaw* _voidEvolutionLaw;
    /// Coalescense law
    CoalescenceLaw* _coalescenceLaw;

    //adding temFunc for Gurson parameters----------------added
    scalarFunction* _temFunc_q1;
    scalarFunction* _temFunc_q2;
    scalarFunction* _temFunc_q3;
    //end of temFuncs---------------------------------------end

    #endif //SWIG

  public:
    /* Default constructor */
    mlawNonLocalDamageGurson(const int num,const double E,const double nu, const double rho,
                           const double q1, const double q2, const double q3, const double fVinitial,
                           const J2IsotropicHardening &j2IH, const CLengthLaw &cLLaw,
                           const double tol, const bool matrixbyPerturbation, const double pert);

    //set for temperature functions--------------------added
    void setTemperatureFunction_q1(const scalarFunction& Tfunc){
      if (_temFunc_q1 != NULL) delete _temFunc_q1;
      _temFunc_q1 = Tfunc.clone();
    };
    void setTemperatureFunction_q2(const scalarFunction& Tfunc){
      if (_temFunc_q2 != NULL) delete _temFunc_q2;
      _temFunc_q2 = Tfunc.clone();
    };
    void setTemperatureFunction_q3(const scalarFunction& Tfunc){
      if (_temFunc_q3 != NULL) delete _temFunc_q3;
      _temFunc_q3 = Tfunc.clone();
    };

    virtual double getq1T (double T) const{return _q1*_temFunc_q1->getVal(T);};
    virtual double getq2T (double T) const{return _q2*_temFunc_q2->getVal(T);};
    virtual double getq3T (double T) const{return _q3*_temFunc_q3->getVal(T);};
    virtual double getq1Der (double T)const{return _q1*_temFunc_q1->getDiff(T);};
    virtual double getq2Der (double T)const{return _q2*_temFunc_q2->getDiff(T);};
    virtual double getq3Der (double T)const{return _q3*_temFunc_q3->getDiff(T);};
    //end of get functions for parameters-----------------------end
    
    virtual void setVoidEvolutionLaw(const voidStateEvolutionLaw& voidtateLaw);
    virtual void setCoalescenceLaw(const CoalescenceLaw& added_coalsLaw); 

    #ifndef SWIG
    // Constructors & destructor
    mlawNonLocalDamageGurson(const mlawNonLocalDamageGurson &source);
    virtual ~mlawNonLocalDamageGurson();
    virtual materialLaw* clone() const {return new mlawNonLocalDamageGurson(*this);};

    // Function of materialLaw
    virtual matname getType() const{return materialLaw::nonLocalDamageGurson;}
    virtual void createIPState(IPStateBase* &ips, bool hasBodyForce, const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const IntPt *GP=NULL, const int gpt = 0) const;
    virtual void createIPVariable(const double fInit, IPNonLocalPorosity* &ipv) const;
    virtual void initLaws(const std::map<int,materialLaw*> &maplaw);
  
    virtual const voidStateEvolutionLaw* getVoidEvolutionLaw() const {return _voidEvolutionLaw;};
    virtual const CoalescenceLaw* getCoalescenceLaw() const {return _coalescenceLaw;};
    
    // Specific functions
  public:
    virtual void voidStateGrowth(const double yieldfV0, const double yieldfV1, // yield porosity
                        const double DeltahatD, const double DeltahatQ, const double DeltahatP, // internal variables
                        const IPNonLocalPorosity *q0, IPNonLocalPorosity *q1) const;
                        
    virtual double yieldFunction(const double kcorEq, const double pcor, const double R, const double yieldfV,
                                const IPNonLocalPorosity* q0, const IPNonLocalPorosity* q1, const double* T = NULL,
                                bool diff=false, fullVector<double>* grad = NULL,
                                bool withthermic = false, double* dfdT = NULL) const;
                                
    virtual void plasticFlow(double & Ns, double& Nv,
                              const double kcorEq, const double pcor, const double R,
                              const IPNonLocalPorosity* q0, const IPNonLocalPorosity* q1, const double* T = NULL,
                              bool diff=false, fullVector<double>* gradNs=NULL, // grad of Ns with respect to [kcorEq pcor R fV Chi W]
                              fullVector<double>* gradNv = NULL, // grad of Nv with respect to  [kcorEq pcor R fV Chi W]
                              bool withTher = false, double* dNsDT = NULL, double* dNvDT=NULL) const;
                                  
                                  
    virtual double getOnsetCriterion(const IPNonLocalPorosity* q1) const;
    virtual void checkCoalescence(IPNonLocalPorosity* q1, const IPNonLocalPorosity *q0, const double* T) const;
    virtual void forceCoalescence(IPNonLocalPorosity* q1, const IPNonLocalPorosity *q0, const double* T) const;
    virtual bool checkCrackCriterion(IPNonLocalPorosity* q1, const IPNonLocalPorosity *q0, const STensor3& Fcur, const SVector3& normal, double delayFactor, const double* T) const;
    virtual void checkFailed(IPNonLocalPorosity* q1, const IPNonLocalPorosity *q0) const;

      
      // general interafce for I1-J2-J3 plasticity
  public:
    virtual double I1J2J3_yieldFunction(const STensor3& kcor, const double R, const IPNonLocalPorosity* q0, const IPNonLocalPorosity* q1, const double* T = NULL,
                                        bool diff=false, STensor3* DFDkcor=NULL, double* DFDR=NULL, std::vector<double>* DFDY=NULL, 
                                        bool withThermic=false, double *DFDT=NULL) const;
    virtual void I1J2J3_getYieldNormal(STensor3& Np, const STensor3& kcor, const double R, const IPNonLocalPorosity* q0, const IPNonLocalPorosity* q1, const double* T = NULL,
                                        bool diff=false, STensor43* DNpDkcor=NULL, STensor3* DNpDR=NULL, std::vector<STensor3>* DNpDY=NULL,
                                        bool withThermic=false, STensor3* DNpDT=NULL  
                                        ) const;
 #endif // SWIG
};

class mlawNonLocalDamageGursonHill48 : public mlawNonLocalDamageGurson
{
  protected:
    STensor43 _aniM; // anisotropic tensor // sigEff = _aniM:sig
  
  public:
    mlawNonLocalDamageGursonHill48(const int num,const double E,const double nu, const double rho,
                           const double q1, const double q2, const double q3, const double fVinitial,
                           const J2IsotropicHardening &j2IH, const CLengthLaw &cLLaw,
                           const double tol, const bool matrixbyPerturbation, const double pert): 
                            mlawNonLocalDamageGurson(num,E,nu,rho,q1,q2,q3,fVinitial, j2IH, cLLaw, tol, matrixbyPerturbation, pert),
                            _aniM(1.,1.){};
    mlawNonLocalDamageGursonHill48(const mlawNonLocalDamageGursonHill48& src): mlawNonLocalDamageGurson(src),
    _aniM(src._aniM){}
    virtual ~mlawNonLocalDamageGursonHill48(){}
    virtual void setYieldParameters(const std::vector<double>& params);
    virtual const STensor43& getAnisotropicTensorReferenceConfiguration() const {return _aniM;};
    virtual void I1J2J3_voidCharacteristicsEvolution_NonLocal_aniso(const STensor3& sig,
                                                const IPNonLocalPorosity *q0, IPNonLocalPorosity* q1,
                                                bool stiff = false,
                                                std::vector<double>* Y = NULL,
                                                std::vector<STensor3>* DYDsig = NULL,
                                                std::vector<std::vector<double> >* DYDNonlocalVars = NULL
                                                ) const;
    virtual double I1J2J3_yieldFunction_aniso(const STensor3& kcor, const double R, const IPNonLocalPorosity* q0, const IPNonLocalPorosity* q1, const double* T = NULL,
                                        bool diff=false, STensor3* DFDkcor=NULL, double* DFDR=NULL, std::vector<double>* DFDY=NULL, 
                                        STensor3* DFDDeltaEp = NULL, STensor3* DFDFdefo = NULL, 
                                        bool withThermic=false, double *DFDT=NULL) const;
    virtual void I1J2J3_getYieldNormal_aniso(STensor3& Np, const STensor3& kcor, const double R, const IPNonLocalPorosity* q0, const IPNonLocalPorosity* q1, const double* T = NULL,
                                        bool diff=false, STensor43* DNpDkcor=NULL, STensor3* DNpDR=NULL, std::vector<STensor3>* DNpDY=NULL,
                                        STensor43* DNpDDeltaEp=NULL, STensor43* DNpDFdefo=NULL,
                                        bool withThermic=false, STensor3* DNpDT=NULL  
                                        ) const;
                                  
    virtual void I1J2J3_predictorCorrector_NonLocal(const STensor3& F0,         // initial deformation gradient (input @ time n)
                            const STensor3& F1,         // updated deformation gradient (input @ time n+1)
                            STensor3 &P,                // updated 1st Piola-Kirchhoff stress tensor (output)
                            const IPNonLocalPorosity *ipvprev,       // array of initial internal variable
                            IPNonLocalPorosity *ipvcur,             // updated array of internal variable (in ipvcur on output),
                            STensor43 &Tangent,         // constitutive tangents (output)
                            std::vector<STensor3>  &dLocalVarDF,
                            std::vector<STensor3>  &dStressDNonLocalVar,
                            fullMatrix<double>    &dLocalVarDNonLocalVar,
                            const bool stiff,                   // if true compute the tangents
                            STensor43& dFedF, std::vector<STensor3>& dFeDNonLocalVar,
                            STensor3*  dStressDT=NULL,
                            fullMatrix<double>* dLocalVarDT=NULL,
                            const double T0 =0,const double T=0
                           ) const
    {
      mlawNonLocalPorosity::I1J2J3_predictorCorrector_NonLocal_aniso(F0, F1, P, ipvprev, ipvcur, 
                                  Tangent, dLocalVarDF, dStressDNonLocalVar, dLocalVarDNonLocalVar, stiff, 
                                  dFedF, dFeDNonLocalVar, dStressDT, dLocalVarDT, T0, T);
    }
};
#endif // MLAWNONLOCALDAMAGEGURSON_H_
