//====================================================mlawEOS(replace the original volumetric response by EOS)===================================//
//
// C++ Interface: material law
//
// Description: Equation of state with the options of acoustic EOS and Hugoniot EOS
//
//
// Author:  <Dongli Li, Antoine Jerusalem>, (C) 2014
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef MLAWEOS_H_
#define MLAWEOS_H_
#include "mlaw.h"
#include "STensor3.h"
#include "STensor43.h"
#include "ipEOS.h"

class mlawEOS : public materialLaw
{
protected:
    double _rho;                 // density
    double _Pressure0;           // ambient pressure
    double _tol;                 // tolerance for iterative process
    double _perturbationfactor;  // perturbation factor
    bool _tangentByPerturbation; // flag for tangent by perturbation
    double _KEOS;                // bulk modulus for accoustic EOS
    double _C0;                  // 1st Hugoniot parameter
    double _s;                   // 2nd Hugoniot parameter
    double _Gamma;               // Grueneisen coefficient
    int _method;                 // options for EOS methods (accoustic EOS or Hugoniot)
    int _flagAV;                 // artificial viscosity flag;
    double _cl;                  // quadratic artificial viscosity coefficient;
    double _c1;                  // linear artificial viscosity coefficient;
#ifndef SWIG

#endif // SWIG
public:

    mlawEOS(const int num,
            const double rho,const double K, const double Pressure0 = 0.);
    mlawEOS(int flag, const int num,
            const double rho,const double P1, double P2, double P3, const double Pressure0 = 0.);
    mlawEOS(int flag, const int num,
            const double rho,const double s, const double C0, double c1, double cl, const double Gamma);

#ifndef SWIG
    mlawEOS(const mlawEOS &source);
    mlawEOS& operator=(const materialLaw &source);
    virtual ~mlawEOS()
    {

    }
		virtual materialLaw* clone() const {return new mlawEOS(*this);};
		virtual void checkInternalState(IPVariable* ipv, const IPVariable* ipvprev) const{}; // do nothing
    virtual bool withEnergyDissipation() const {return false;};

    // function of materialLaw
    virtual matname getType() const{return materialLaw::EOS;}
    virtual void createIPState(IPStateBase* &ips,bool hasBodyForce,const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const IntPt *GP=NULL, const int gpt = 0) const;
    virtual void initLaws(const std::map<int,materialLaw*> &maplaw){};
    virtual double soundSpeed() const;
    virtual double density()const{return _rho;}
    virtual const double bulkModulus() const{return _KEOS;}
    virtual const double ambientPressure() const{return _Pressure0;}
    // specific function
public:


    virtual void constitutive(const STensor3& F0,         // initial deformation gradient (input @ time n)
                            const STensor3& Fn,         // updated deformation gradient (input @ time n+1)
                            STensor3 &P,                // updated 1st Piola-Kirchhoff stress tensor (output)
                                                        // contains the initial values on input
                            const IPVariable *q0,       // array of initial internal variable
                            IPVariable *q1,              // updated array of internal variable (in ipvcur on output),
			    STensor43 &Tangent,         // constitutive tangents (output)
			    const bool stiff,
                            STensor43* elasticTangent = NULL, 
                            const bool dTangent =false,
                            STensor63* dCalgdeps =NULL
			    ) const
   {
     Msg::Error("mlawEOS: constitutive not defined");
   }

    virtual void constitutive(
            const STensor3& F0,         // initial deformation gradient (input @ time n)
            const STensor3& Fn,         // updated deformation gradient (input @ time n+1)
            STensor3 &P,                // updated 1st Piola-Kirchhoff stress tensor (output)
            STensor3 &Piola_vis,        // updated 1st Piola-Kirchhoff stress tensor for viscous part introduced by AV
            STensor3 &P_b4AV,
            IPEOS *ipEOS,
            const IPEOS *ipEOSprev,
            STensor3 &Fdot
            );

protected:


    virtual void update(
            const STensor3& F0,
            const STensor3& Fn,
            STensor3&P,
            STensor3 &Piola_vis,
            STensor3 &P_b4AV,
            IPEOS *ipEOS,
            const IPEOS *ipEOSprev,
            STensor3 &Fdot
            );
    virtual double eos(
            const STensor3 &Fn	//updated deformation gradient (input @ time n+1)
            );
    virtual void volumetricPart(
            STensor3 &Sig_v,   //volumetric part of first PK stress
            const STensor3 &Fn //updated deformation gradient
            );
    virtual void artificialViscosity(
            double elementSize,
            const STensor3 &F1,
            const STensor3 &F0,
            STensor3 &Pialo_vis,
            STensor3 &Fdot
            );

    virtual void bulkViscosity(
            double elementSize1,
            STensor3 F1,
            const STensor3 F0,
            STensor3 &Piola_vis,
            STensor3 &Fdot
            );
protected:

    virtual void multSTensor3(const STensor3 &a, const STensor3 &b, STensor3 &c) const;
    virtual void multSTensor3SecondTranspose(const STensor3 &a, const STensor3 &b, STensor3 &c) const;
    virtual void Write_Matrix_To_File(std::ofstream& my_file, const STensor3 &X, std::string matrix_name)const;
    virtual STensor3 devSTensor3(STensor3 &a) const;
    virtual STensor3 symSTensor3(STensor3 &a) const;

#endif // SWIG
};

#endif // MLAWEOS_H_

