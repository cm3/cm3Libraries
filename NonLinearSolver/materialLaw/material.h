#ifndef MATERIAL_H
#define MATERIAL_H 1

#include "ID.h"
#include "damage.h" 
#include "clength.h"
#include "lccfunctions.h"
#include "elasticlcc.h"
#include <vector>


#ifdef NONLOCALGMSH
namespace MFH {
#endif
//****in all of the constbox**New tenor added by wu ling: double* dpdE, double* strs_dDdp_bar ****************
//Generic material
class Material{

	protected:
		int constmodel;
		int nsdv;
		int pos_strn, pos_strs, pos_dstrn; 		//position of strain, stress and dstrn in the statev vector
		Damage* dam;
		int pos_dam;	//position of damage state variables in statev vector

		bool localDamage;

                int pos_p;

                Clength* clength;

                bool evaluateStiffness;

	public:
		
		
		Material(double* props, int idmat){
			constmodel = (int) props[idmat];
                        evaluateStiffness = true;
                        localDamage = false;
		}
		virtual ~Material(){ ; }
		inline int get_constmodel(){  return constmodel; }
		
		//give the number of state variables needed for the constitutive model
		inline int get_nsdv() const{  return nsdv; }
		
		//give the position of strn, strs and dstrn in the statev vector
		inline int get_pos_strn() const {	return pos_strn; }
		inline int get_pos_strs() const {	return pos_strs; }
		inline int get_pos_dstrn() const	{	return pos_dstrn; }
                virtual int get_pos_dR() const { return -1;}


                //give the position of damage in the statev vector === p_bar, dp_bar ,D, dD
                virtual int get_pos_dam() const { if(dam)   return pos_dam; else return -1; }    
                virtual int get_pos_p() const { return pos_p; }  
                virtual bool isInLargeDeformation() const {return false;}
		virtual const Damage* getDamage() const {return dam;}

                virtual int get_pos_maxD() const {if (dam) return pos_dam+dam->get_pos_maxD(); else return -1;}
                virtual double getMaxD(double *statev) const {if (dam) return statev[get_pos_maxD()]; else return -1.;}
                virtual void setMaxD(double *statev, double Dmax) {if (dam) statev[get_pos_maxD()]=Dmax;}
		virtual int getNbNonLocalVariables() const {if(dam) return dam->getNbNonLocalVariables(); else return 0;}

                virtual int get_pos_pstrn() const;
                virtual int get_pos_dp() const;
                virtual int get_pos_twomu() const;
                virtual int get_pos_eqstrs() const;
                virtual bool eqstrs_exist() const;


		//print the material properties
		virtual void print()=0;

                inline bool getEvaluateStiffness() const {return evaluateStiffness;}
                inline void setEvaluateStiffness(bool stiff) {evaluateStiffness = stiff;}

		//compute the constitutive response of the material
		//INPUT: dstrn, strs_n, statev_n, kinc, kstep
		//OUTPUT: strs, statev, Cref, dCref, Calgo
		virtual int constbox(double* dstrn, double* strs_n,  double* strs, double* statev_n, double* statev,double** Cref, double*** dCref, double* tau, double** dtau, double** Calgo, double alpha, double* dpdE, double* strs_dDdp_bar, double *Sp_bar, double** c_g, double* dFd_d_bar, double* dstrs_dFd_bar, double *dFd_dE, double** c_gF, double* dpdFd, double* dFddp, int kinc, int kstep, double dt)=0;

/*		virtual int constboxSecant(double* dstrn, double* strs_n,  double* strs, double* statev_n, double* statev, double **Calgo, double** Csd, double*** dCsd, double** dCsdp_bar, double* dnu, double& dnudp_bar, double alpha, double* dpdE, double* dstrsdp_bar, double** c_g, int kinc, int kstep, double dt );
		virtual int constboxSecantZero(double* dstrn, double* strs_n,  double* strs, double* statev_n, double* statev, double **Calgo, double** Csd, double*** dCsd, double** dCsdp_bar, double* dnu, double& dnudp_bar, double alpha, double* dpdE, double* dstrsdp_bar, double** c_g, int kinc, int kstep, double dt);*/
                virtual int constboxSecantMixte(double* dstrn, double* strs_n,  double* strs, double* statev_n, double* statev, double **Calgo, double** Csd, double*** dCsd, double** dCsdp_bar, double* dnu, double& dnudp_bar, double alpha, double* dpdE, double* dstrsdp_bar, double** c_g, int kinc, int kstep, double dt, bool forceZero, bool forceRes);

                virtual int constboxSecantMixteGeneric(double* dstrn, double* strs_n,  double* strs, double* statev_n, double* statev, double **Calgo, double** Csd, double*** dCsd, double** dCsdp_bar, double* dnu, double& dnudp_bar, double alpha, double* dpdE, double* dstrsdp_bar, double** c_g, int kinc, int kstep, double dt, bool forceZero, bool forceRes,
double E, double nu, int htype, double sy0, double hexp, double hmod1, double hmod2, double hp0, double alpha_DP, double m_DP, double nup, int viscmodel, double vmod, double vstressy, double vexp, bool evp);

                virtual int constboxLargD(double* dstrn, double* strs_n,  double* strs, double* statev_n, double* statev, double **Calgo, double** Csd, double*** dCsd, double** dCsdp_bar, double* dnu, double& dnudp_bar, double alpha, double* dpdE, double* dstrsdp_bar, double** c_g, int kinc, int kstep, double dt);

                virtual int constbox_2order(int mtx, double *DE, double* dstrn, double* strs, double* statev_n, double* statev, double **Calgo, Lcc* LCC, YieldF* YF, double** c_g, int kinc, int kstep, double dt);

                virtual int constbox_2ndNtr(double *DE, double* dstrn, double* strs_n,  double* strs, double* statev_n, double* statev,  ELcc* LCC, EPR* epresult, double alpha, double** c_g, int kinc, int kstep, double dt);

              
		//compute a reference tangent operator over the time increment using a generalized mid-point rule
		virtual void get_refOp(double* statev, double** C, int kinc, int kstep)=0;

		//compute an elastic operator 
		virtual void get_elOp(double** Cel)=0;
		virtual void get_elOp(double* statev, double** Cel) {get_elOp(Cel);} ;
		virtual void get_elOD(double* statev, double** Cel)=0;
                virtual void unload_step(double* dstrn, double* strs_n, double* statev, int kinc, int kstep)=0;

                //add function for random volume fraction and euler angle.
		virtual int get_pos_vfi() const {  return 0; }
		virtual int get_pos_euler() const { return 0; }
		virtual int get_pos_aspR() const { return 0; }
		virtual int get_pos_Base() const { return 0; }
		virtual int get_pos_F() const { return 0; }
		virtual int get_pos_Basem() const { return 0; }
		virtual int get_pos_Basei() const { return 0; }
		virtual int get_pos_Fm() const { return 0; }
		virtual int get_pos_Fi() const { return 0; }
		virtual int get_pos_Ja() const { return 0; }
		virtual int get_pos_Ri() const { return 0; }

		virtual int get_pos_E() const {  return 0; } 
		virtual int get_pos_nu() const {  return 0; } 
		virtual int get_pos_sy0() const {  return 0; }  
		virtual int get_pos_hmod1() const {  return 0; } 
		virtual int get_pos_hmod2() const {  return 0; } 
		virtual int get_pos_alpha_DP() const {  return 0; } 
		virtual int get_pos_m_DP() const {  return 0; } 
		virtual int get_pos_nup() const {  return 0; } 
		virtual int get_pos_hp0() const {  return 0; } 
		virtual int get_pos_hexp() const {  return 0; } 
		virtual int get_pos_DamParm1() const {  return 0; } 
		virtual int get_pos_DamParm2() const {  return 0; } 
		virtual int get_pos_DamParm3() const {  return 0; } 
		virtual int get_pos_INCDamParm1() const {  return 0; } 
		virtual int get_pos_INCDamParm2() const {  return 0; } 
		virtual int get_pos_INCDamParm3() const {  return 0; } 
		virtual int get_pos_Fd_bar() const {  return 0; } 
		virtual int get_pos_dFd_bar() const {  return 0; } 
		virtual int get_pos_locFd() const {  return 0; }


                virtual int get_pos_inc_maxD() const {return -1;  }
                virtual double get_inc_MaxD(double *statev) const {  return -1.; }
                virtual void set_inc_MaxD(double *statev, double Dmax) {}
                virtual int get_pos_mtx_maxD() const {return -1; }
                virtual double get_mtx_MaxD(double *statev) const {return -1.; }
                virtual void set_mtx_MaxD(double *statev, double Dmax) {}
                
                virtual std::vector<double> &getEuler(std::vector<double> &_euler) {return _euler;}
                virtual int getNPatch() const {return -1;}
                virtual int get_mat_NPatch(int matNb) const {return -1;}
		virtual int get_mat_constmodel(int matNb) const {return -1;}
		virtual int get_submat_constmodel(int matNb, int submatNb) const {return -1;}


#ifdef NONLOCALGMSH
		virtual int get_pos_currentplasticstrainfornonlocal() const = 0;
		virtual int get_pos_effectiveplasticstrainfornonlocal() const = 0;
		virtual int get_pos_damagefornonlocal() const = 0;
                virtual int get_pos_mtx_stress () const;
                virtual int get_pos_inc_stress () const;
                virtual int get_pos_mtx_strain () const;
                virtual int get_pos_inc_strain () const;
		virtual int get_pos_mtx_Ja() const;
		virtual int get_pos_inc_Ja() const;
		virtual int get_pos_mtx_Dam() const;
		virtual int get_pos_inc_Dam() const;
		virtual double getElasticEnergy (double* statev) =0;
		virtual double getPlasticEnergy (double* statev) =0;

		virtual void get_elOp_mtx(double* statev, double** Cel);
		virtual void get_elOp_icl(double* statev, double** Cel);

		virtual void get_elOp_mtx(double** Cel);
		virtual void get_elOp_icl(double** Cel);
                virtual double get_vfI();
                
                virtual void setEuler(double e1, double e2, double e3);
                
		virtual void setLocalDamage() {
			localDamage=true;
			if(dam) dam->setLocalDamage();
		}

		virtual void setNonLocalDamage() {
			localDamage=false;
			if(dam) dam->setNonLocalDamage();
		}
		
		virtual int get_pos_mat_inc_maxD(int matNb) const {return -1;}
		virtual int get_pos_mat_mtx_maxD(int matNb) const {return -1;}
                virtual void populateMaxD(double *statev, double Dmax_Inc, double Dmax_Mtx) const {}


		// for VT or LAM_2PLY case (VM, LVM or VLM)
                virtual int get_pos_mat_strain(int matNb) const {return -1;}
                virtual int get_pos_mat_stress(int matNb) const {return -1;}
                virtual int get_pos_mat_Dam(int matNb) const {return -1;}
                virtual int get_pos_mat_mtx_strain(int matNb) const {return -1;}
                virtual int get_pos_mat_inc_strain(int matNb) const {return -1;}
                virtual int get_pos_mat_mtx_stress(int matNb) const {return -1;}
                virtual int get_pos_mat_inc_stress(int matNb) const {return -1;}
                virtual int get_pos_mat_mtx_Dam(int matNb) const {return -1;}
                virtual int get_pos_mat_inc_Dam(int matNb) const {return -1;}
                
                // for VT case (VM or VLM)
                // for VLM case
		// laminate euler angles
		virtual std::vector<double> &getEulerVTLam(std::vector<double> &euler_lam, int grainNb) {return euler_lam;}
		// for each VT grain
		// strain
		virtual int getPosStrnVTM0(int grainNb) const {return -1;}
		virtual int getPosStrnVTMT(int grainNb) const {return -1;}
		virtual int getPosStrnVTLam(int grainNb) const {return -1;}
		// stress
		virtual int getPosStrsVTM0(int grainNb) const {return -1.;}
		virtual int getPosStrsVTMT(int grainNb) const {return -1;}
		virtual int getPosStrsVTLam(int grainNb) const {return -1;}
		// damage
                virtual int getPosDamVTM0(int grainNb) const {return -1;}
                // for each laminate ply in VT grains
		// strain
		virtual int getPosStrnVTLamM0(int grainNb) const {return -1;}
		virtual int getPosStrnVTLamMT(int grainNb) const {return -1;}
		// stress
		virtual int getPosStrsVTLamM0(int grainNb) const {return -1;}
		virtual int getPosStrsVTLamMT(int grainNb) const {return -1;}
		// damage
		virtual int getPosDamVTLamM0(int grainNb) const {return -1;}
		// for each material phase
		// strain
		virtual int getPosStrnVTMTMtx(int grainNb) const {return -1;}
		virtual int getPosStrnVTMTInc(int grainNb) const {return -1;}
		virtual int getPosStrnVTLamMTMtx(int grainNb) const {return -1;}
		virtual int getPosStrnVTLamMTInc(int grainNb) const {return -1;}
		// stress
		virtual int getPosStrsVTMTMtx(int grainNb) const {return -1;}
		virtual int getPosStrsVTMTInc(int grainNb) const {return -1;}
		virtual int getPosStrsVTLamMTMtx(int grainNb) const {return -1;}
		virtual int getPosStrsVTLamMTInc(int grainNb) const {return -1;}
		// damage
		virtual int getPosDamVTMTMtx(int grainNb) const {return -1;}
		virtual int getPosDamVTMTInc(int grainNb) const {return -1;}
		virtual int getPosDamVTLamMTMtx(int grainNb) const {return -1;}
		virtual int getPosDamVTLamMTInc(int grainNb) const {return -1;}
                
                
                // for LAM_2PLY case (LVM)
                // for each laminate ply
                // strain
		virtual int getPosStrnLamM0(int laminateNb) const {return -1;}
		virtual int getPosStrnLamVT(int laminateNb) const {return -1;}
		// stress
		virtual int getPosStrsLamM0(int laminateNb) const {return -1;}
		virtual int getPosStrsLamVT(int laminateNb) const {return -1;}
		// damage
		virtual int getPosDamLamM0(int laminateNb) const {return -1;}
		// for each MT grain in VT ply
		// strain
		virtual int getPosStrnLamVTMT(int grainNb) const {return -1;}
		// stress
		virtual int getPosStrsLamVTMT(int grainNb) const {return -1;}
		// for each material phase
		// strain
		virtual int getPosStrnLamVTMTMtx(int grainNb) const {return -1;}
		virtual int getPosStrnLamVTMTInc(int grainNb) const {return -1;}
		// stress
		virtual int getPosStrsLamVTMTMtx(int grainNb) const {return -1;}
		virtual int getPosStrsLamVTMTInc(int grainNb) const {return -1;}
		// damage
		virtual int getPosDamLamVTMTMtx(int grainNb) const {return -1;}
		virtual int getPosDamLamVTMTInc(int grainNb) const {return -1;}
#endif


};

//Linear elastic material
class EL_Material : public Material{

	private:
		double E, nu;
                int pos_Ja;
                int pos_Ri;
                int pos_Base;
                int pos_F;
               
	public:
		
		EL_Material(double* props, int idmat);
		~EL_Material();
		
		virtual void print();
		virtual int constbox(double* dstrn, double* strs_n,  double* strs, double* statev_n, double* statev, double** Cref, double*** dCref, double* tau, double** dtau, double** Calgo, double alpha, double* dpdE, double* strs_dDdp_bar, double *Sp_bar, double** c_g, double* dFd_d_bar, double* dstrs_dFd_bar, double *dFd_dE, double** c_gF, double* dpdFd, double* dFddp, int kinc, int kstep, double dt);
		virtual int constboxSecantMixte(double* dstrn, double* strs_n,  double* strs, double* statev_n, double* statev, double **Calgo, double** Csd, double*** dCsd, double** dCsdp_bar, double* dnu, double& dnudp_bar, double alpha, double* dpdE, double* dstrsdp_bar, double** c_g, int kinc, int kstep, double dt, bool forceZero, bool forceRes);

		virtual int constboxLargD(double* dstrn, double* strs_n,  double* strs, double* statev_n, double* statev, double **Calgo, double** Csd, double*** dCsd, double** dCsdp_bar, double* dnu, double& dnudp_bar, double alpha, double* dpdE, double* dstrsdp_bar, double** c_g, int kinc, int kstep, double dt);

                virtual int constbox_2order(int mtx, double *DE, double* dstrn, double* strs, double* statev_n, double* statev, double **Calgo, Lcc* LCC, YieldF* YF, double** c_g, int kinc, int kstep, double dt);

               	virtual void get_refOp(double* statev, double** C, int kinc, int kstep);
		virtual void get_elOp(double** Cel);
		virtual void get_elOD(double* statev, double** Cel);
                virtual void unload_step(double* dstrn, double* strs_n, double* statev, int kinc, int kstep);

		virtual int get_pos_Ja() const {  return pos_Ja; } 
		virtual int get_pos_Ri() const {  return pos_Ri; }  
		virtual int get_pos_Base() const {  return pos_Base; } 
		virtual int get_pos_F() const {  return pos_F; } 
#ifdef NONLOCALGMSH
		virtual int get_pos_currentplasticstrainfornonlocal() const;
		virtual int get_pos_effectiveplasticstrainfornonlocal() const;
		virtual int get_pos_damagefornonlocal() const;
                virtual double getElasticEnergy (double* statev) ;
		virtual double getPlasticEnergy (double* statev) ;
#endif
		
}; 

//Elasto-plastic material
class EP_Material : public Material{

	private:
		double E,nu,sy0;
		int htype;
		double hmod1, hmod2, hp0, hexp, alpha_DP, m_DP, nup;
		int pos_pstrn, pos_dp, pos_twomu,pos_twok, pos_eqstrs;
                int pos_Base, pos_F, pos_Ja;
                int pos_Ri;

	public:
	

		EP_Material(double* props, int idmat);
		~EP_Material();

		virtual void print();
		virtual int constbox(double* dstrn, double* strs_n,  double* strs, double* statev_n, double* statev, double** Cref, double*** dCref, double* tau, double** dtau, double** Calgo, double alpha, double* dpdE, double* strs_dDdp_bar, double *Sp_bar, double** c_g, double* dFd_d_bar, double* dstrs_dFd_bar, double *dFd_dE, double** c_gF, double* dpdFd, double* dFddp, int kinc, int kstep, double dt);
//*****************New tenor added by wu ling: double* dpdE, double* strs_dDdp_bar, double* dDdE,****************
/*		virtual int constboxSecant(double* dstrn, double* strs_n,  double* strs, double* statev_n, double* statev, double **Calgo, double** Csd, double*** dCsd, double** dCsdp_bar, double* dnu, double& dnudp_bar, double alpha, double* dpdE, double* dstrsdp_bar, double** c_g, int kinc, int kstep, double dt);
                virtual int constboxSecantZero(double* dstrn, double* strs_n,  double* strs, double* statev_n, double* statev, double **Calgo, double** Csd, double*** dCsd, double** dCsdp_bar, double* dnu, double& dnudp_bar, double alpha, double* dpdE, double* dstrsdp_bar, double** c_g, int kinc, int kstep, double dt);*/
                virtual int constboxSecantMixte(double* dstrn, double* strs_n,  double* strs, double* statev_n, double* statev, double **Calgo, double** Csd, double*** dCsd, double** dCsdp_bar, double* dnu, double& dnudp_bar, double alpha, double* dpdE, double* dstrsdp_bar, double** c_g, int kinc, int kstep, double dt, bool forceZero, bool forceRes);
//******************new constbox for large deformation
                virtual int constboxLargD(double* dstrn, double* strs_n,  double* strs, double* statev_n, double* statev, double **Calgo, double** Csd, double*** dCsd, double** dCsdp_bar, 
double* dnu, double &dnudp_bar, double alpha, double* dpdE, double* dstrsdp_bar, double** c_g, int kinc, int kstep, double dt);
//******************new constbox for 2rd order method
                virtual int constbox_2order( int mtx, double *DE, double* dstrn, double* strs, double* statev_n, double* statev, double **Calgo, Lcc* LCC, YieldF* YF, double** c_g, int kinc, int kstep, double dt);

//******************new constbox for 2nd moment estimation of stress_trial and Ntrial
                virtual int constbox_2ndNtr(double *DE, double* dstrn, double* strs_n,  double* strs, double* statev_n, double* statev,  ELcc* LCC, EPR* epresult, double alpha, double** c_g, int kinc, int kstep, double dt);


		virtual void get_refOp(double* statev, double** C, int kinc, int kstep);
		virtual void get_elOp(double** Cel);
		virtual void get_elOD(double* statev, double** Cel);
                virtual void unload_step(double* dstrn, double* strs_n, double* statev, int kinc, int kstep);

		virtual int get_pos_Base() const {  return pos_Base; } 
		virtual int get_pos_F() const {  return pos_F; } 
		virtual int get_pos_Ja() const {  return pos_Ja; } 
		virtual int get_pos_Ri() const {  return pos_Ri; }  

                virtual int get_pos_pstrn() const { return pos_pstrn; } 
                virtual int get_pos_dp() const {return pos_dp;}
                virtual int get_pos_twomu() const { return pos_twomu;}
                virtual int get_pos_eqstrs() const {return pos_eqstrs;}
                virtual bool eqstrs_exist() const {return true;}


#ifdef NONLOCALGMSH
		virtual int get_pos_currentplasticstrainfornonlocal() const;
		virtual int get_pos_effectiveplasticstrainfornonlocal() const;
		virtual int get_pos_damagefornonlocal() const;
                virtual double getElasticEnergy (double* statev) ;
		virtual double getPlasticEnergy (double* statev) ;
#endif


};


// Elasto-plastic Stochastic material
class EP_Stoch_Material : public Material{

	private:
		double _E,_nu,_sy0, _alpha_DP, _m_DP, _nup;
		int htype;
		double _hmod1, _hmod2, _hp0, _hexp;
                int pos_E, pos_nu, pos_sy0, pos_alpha_DP, pos_m_DP, pos_nup;
                int pos_hmod1, pos_hmod2, pos_hp0, pos_hexp;
                int pos_DamParm1, pos_DamParm2, pos_DamParm3;
		int pos_pstrn, pos_dp, pos_twomu, pos_eqstrs;
	public:
		EP_Stoch_Material(double* props, int idmat);
		~EP_Stoch_Material();

		virtual void print();
		virtual int constbox(double* dstrn, double* strs_n,  double* strs, double* statev_n, double* statev, double** Cref, double*** dCref, double* tau, double** dtau, double** Calgo, double alpha, double* dpdE, double* strs_dDdp_bar, double *Sp_bar, double** c_g, double* dFd_d_bar, double* dstrs_dFd_bar, double *dFd_dE, double** c_gF, double* dpdFd, double* dFddp, int kinc, int kstep, double dt);
//*****************New tenor added by wu ling: double* dpdE, double* strs_dDdp_bar, double* dDdE,****************
/*		virtual int constboxSecant(double* dstrn, double* strs_n,  double* strs, double* statev_n, double* statev, double **Calgo, double** Csd, double*** dCsd, double** dCsdp_bar, double* dnu, double& dnudp_bar, double alpha, double* dpdE, double* dstrsdp_bar, double** c_g, int kinc, int kstep, double dt);
                virtual int constboxSecantZero(double* dstrn, double* strs_n,  double* strs, double* statev_n, double* statev, double **Calgo, double** Csd, double*** dCsd, double** dCsdp_bar, double* dnu, double& dnudp_bar, double alpha, double* dpdE, double* dstrsdp_bar, double** c_g, int kinc, int kstep, double dt);*/
                virtual int constboxSecantMixte(double* dstrn, double* strs_n,  double* strs, double* statev_n, double* statev, double **Calgo, double** Csd, double*** dCsd, double** dCsdp_bar, double* dnu, double& dnudp_bar, double alpha, double* dpdE, double* dstrsdp_bar, double** c_g, int kinc, int kstep, double dt, bool forceZero, bool forceRes);
//******************new constbox for 2nd moment estimation of stress_trial and Ntrial
                virtual int constbox_2ndNtr(double *DE, double* dstrn, double* strs_n,  double* strs, double* statev_n, double* statev,  ELcc* LCC, EPR* epresult, double alpha, double** c_g, int kinc, int kstep, double dt);

		virtual void get_refOp(double* statev, double** C, int kinc, int kstep);
		virtual void get_elOp(double* statev, double** Cel);
                virtual void get_elOp(double** Cel);
		virtual void get_elOD(double* statev, double** Cel);
                virtual void unload_step(double* dstrn, double* strs_n, double* statev, int kinc, int kstep);

                virtual bool eqstrs_exist() const;
                virtual int get_pos_pstrn() const { return pos_pstrn; } 
                virtual int get_pos_dp() const {return pos_dp;}
                virtual int get_pos_twomu() const { return pos_twomu;}
                virtual int get_pos_eqstrs() const {return pos_eqstrs;}


		virtual int get_pos_E() const {  return pos_E; } 
		virtual int get_pos_nu() const {  return pos_nu; } 
		virtual int get_pos_sy0() const {  return pos_sy0; } 

		virtual int get_pos_alpha_DP() const {  return pos_alpha_DP; } 
		virtual int get_pos_m_DP () const {  return pos_m_DP ; } 
		virtual int get_pos_nup() const {  return pos_nup; } 
 
		virtual int get_pos_hmod1() const {  return pos_hmod1; } 
		virtual int get_pos_hmod2() const {  return pos_hmod2; } 
		virtual int get_pos_hp0() const {  return pos_hp0; } 
		virtual int get_pos_hexp() const {  return pos_hexp; }  
		virtual int get_pos_DamParm1() const {  return pos_DamParm1; } 
		virtual int get_pos_DamParm2() const {  return pos_DamParm2; } 
		virtual int get_pos_DamParm3() const {  return pos_DamParm3; } 

#ifdef NONLOCALGMSH
		virtual int get_pos_currentplasticstrainfornonlocal() const;
		virtual int get_pos_effectiveplasticstrainfornonlocal() const;
		virtual int get_pos_damagefornonlocal() const;
                virtual double getElasticEnergy (double* statev) ;
		virtual double getPlasticEnergy (double* statev) ;
#endif
};



//Elasto-Viscoplastic material
class EVP_Material : public Material{

	private:
		double E,nu,sy0;
		int htype;
		double hmod1, hmod2, hp0, hexp, alpha_DP, m_DP, nup;
		int viscmodel;
                double vmod, vexp, vstressy;
		int pos_pstrn, pos_dp, pos_twomu, pos_eqstrs;
                int pos_Base, pos_F, pos_Ja; 
                int pos_Ri;           

	public:
	
		EVP_Material(double* props, int idmat);
		~EVP_Material();

		virtual void print();
		virtual int constbox(double* dstrn, double* strs_n,  double* strs, double* statev_n, double* statev, double** Cref, double*** dCref, double* tau, double** dtau, double** Calgo, double alpha, double* dpdE, double* strs_dDdp_bar, double *Sp_bar, double** c_g, double* dFd_d_bar, double* dstrs_dFd_bar, double *dFd_dE, double** c_gF, double* dpdFd, double* dFddp, int kinc, int kstep, double dt);
//*****************New tenor added by wu ling: double* dpdE, double* strs_dDdp_bar, double* dDdE,****************
/*		virtual int constboxSecant(double* dstrn, double* strs_n,  double* strs, double* statev_n, double* statev, double **Calgo, double** Csd, double*** dCsd, double** dCsdp_bar, double* dnu, double& dnudp_bar, double alpha, double* dpdE, double* dstrsdp_bar, double** c_g, int kinc, int kstep, double dt);
                virtual int constboxSecantZero(double* dstrn, double* strs_n,  double* strs, double* statev_n, double* statev, double **Calgo, double** Csd, double*** dCsd, double** dCsdp_bar, double* dnu, double& dnudp_bar, double alpha, double* dpdE, double* dstrsdp_bar, double** c_g, int kinc, int kstep, double dt);*/
                virtual int constboxSecantMixte(double* dstrn, double* strs_n,  double* strs, double* statev_n, double* statev, double **Calgo, double** Csd, double*** dCsd, double** dCsdp_bar, double* dnu, double& dnudp_bar, double alpha, double* dpdE, double* dstrsdp_bar, double** c_g, int kinc, int kstep, double dt, bool forceZero, bool forceRes);
                virtual int constbox_2ndNtr(double *DE, double* dstrn, double* strs_n,  double* strs, double* statev_n, double* statev,  ELcc* LCC, EPR* epresult, double alpha, double** c_g, int kinc, int kstep, double dt);
                virtual int constboxLargD(double* dstrn, double* strs_n,  double* strs, double* statev_n, double* statev, double **Calgo, double** Csd, double*** dCsd, double** dCsdp_bar, 
double* dnu, double &dnudp_bar, double alpha, double* dpdE, double* dstrsdp_bar, double** c_g, int kinc, int kstep, double dt);
                virtual int constbox_2order(int mtx, double *DE, double* dstrn, double* strs, double* statev_n, double* statev,  double **Calgo, Lcc* LCC, YieldF* YF, double** c_g, int kinc, int kstep, double dt);      

		virtual void get_refOp(double* statev, double** C, int kinc, int kstep);
		virtual void get_elOp(double** Cel);
		virtual void get_elOD(double* statev, double** Cel);
                virtual void unload_step(double* dstrn, double* strs_n, double* statev, int kinc, int kstep);

                virtual int get_pos_pstrn() const { return pos_pstrn; } 
                virtual int get_pos_dp() const {return pos_dp;}
                virtual int get_pos_twomu() const { return pos_twomu;}
                virtual int get_pos_eqstrs() const {return pos_eqstrs;}
                virtual bool eqstrs_exist() const;
		virtual int get_pos_Base() const {  return pos_Base; } 
		virtual int get_pos_F() const {  return pos_F; } 
		virtual int get_pos_Ja() const {  return pos_Ja; } 
		virtual int get_pos_Ri() const {  return pos_Ri; }  

#ifdef NONLOCALGMSH
		virtual int get_pos_currentplasticstrainfornonlocal() const;
		virtual int get_pos_effectiveplasticstrainfornonlocal() const;
		virtual int get_pos_damagefornonlocal() const;
                virtual double getElasticEnergy (double* statev) ;
		virtual double getPlasticEnergy (double* statev) ;
#endif

};



//Composite material homogenized with Voigt scheme
class VT_Material : public Material{

	private:
		int NGrain;					//number of grains
		Material** mat;		//array of pointers on Materials
		double* vf;				//array of volume fraction of each grain
				
#ifdef NONLOCALGMSH
		// for VT case (VM or VLM)
		// for VLM case
		// laminate euler angles
		std::vector<double> euler_vt_lam;
		// for each VT grain
		// strain
		std::vector<int> pos_strn_vt_m0, pos_strn_vt_mt, pos_strn_vt_lam;
		// stress
		std::vector<int> pos_strs_vt_m0, pos_strs_vt_mt, pos_strs_vt_lam;
		// damage
		std::vector<int> pos_dam_vt_m0;
		// for each laminate ply in VT grains
		// strain
		std::vector<int> pos_strn_vt_lam_m0, pos_strn_vt_lam_mt;
		// stress
		std::vector<int> pos_strs_vt_lam_m0, pos_strs_vt_lam_mt;
		// damage
		std::vector<int> pos_dam_vt_lam_m0;
		// for each material phase
		// strain
		std::vector<int> pos_strn_vt_mt_mtx, pos_strn_vt_mt_inc;
		std::vector<int> pos_strn_vt_lam_mt_mtx, pos_strn_vt_lam_mt_inc;
		// stress
		std::vector<int> pos_strs_vt_mt_mtx, pos_strs_vt_mt_inc;
		std::vector<int> pos_strs_vt_lam_mt_mtx, pos_strs_vt_lam_mt_inc;
		// damage
		std::vector<int> pos_dam_vt_mt_mtx, pos_dam_vt_mt_inc;
		std::vector<int> pos_dam_vt_lam_mt_mtx, pos_dam_vt_lam_mt_inc;
#endif

	public:
		
		VT_Material(double* props, int idmat);
		~VT_Material();

		virtual void print();
		virtual int constbox(double* dstrn, double* strs_n,  double* strs, double* statev_n, double* statev, double** Cref, double*** dCref, double* tau, double** dtau, double** Calgo, double alpha, double* dpdE, double* strs_dDdp_bar, double *Sp_bar, double** c_g, double* dFd_d_bar, double* dstrs_dFd_bar, double *dFd_dE, double** c_gF, double* dpdFd, double* dFddp, int kinc, int kstep, double dt);
		virtual void get_refOp(double* statev, double** C, int kinc, int kstep);
		virtual void get_elOp(double** Cel);
		virtual void get_elOD(double* statev, double** Cel);
                virtual void unload_step(double* dstrn, double* strs_n, double* statev, int kinc, int kstep);

                virtual int getNPatch() const {return NGrain;}
                virtual int get_mat_NPatch(int grainNb) const {return mat[grainNb]->getNPatch();}                
                virtual int get_mat_constmodel(int grainNb) const {return mat[grainNb]->get_constmodel();}
                virtual int get_submat_constmodel(int grainNb, int laminateNb) const {return mat[grainNb]->get_mat_constmodel(laminateNb);}
                             
#ifdef NONLOCALGMSH
		virtual int get_pos_currentplasticstrainfornonlocal() const;
		virtual int get_pos_effectiveplasticstrainfornonlocal() const;
		virtual int get_pos_damagefornonlocal() const;
                virtual double getElasticEnergy (double* statev) ;
		virtual double getPlasticEnergy (double* statev) ;
		
		/*virtual void setLocalDamage()
		{
                    Material::setLocalDamage();
                    for(i=0;i<NGrain;i++){
		        mat[i]->setLocalDamage();
                    }
                }*/
                
                virtual int get_pos_mat_inc_maxD(int grainNb) const {             
                	int idsdv = 18;  // position in statev where the statev of the first phase start
                	for(int i=0;i<grainNb;i++) {
                		idsdv += mat[i]->get_nsdv();
                	}
                	
			switch(mat[grainNb]->get_constmodel()){		                
				case MTSecF:  // MFH Fisrst order secant
					if(mat[grainNb]->get_pos_inc_maxD()>-1)  // MT-Inc phase
						return (idsdv + mat[grainNb]->get_pos_inc_maxD());
					return -1;
					break;
					
				default:
					return -1;
					break;
			}        
                }
                
                virtual int get_pos_mat_mtx_maxD(int grainNb) const {             
                	int idsdv = 18;  // position in statev where the statev of the first phase start
                	for(int i=0;i<grainNb;i++) {
                		idsdv += mat[i]->get_nsdv();
                	}
                	
			switch(mat[grainNb]->get_constmodel()){		                
				case EP:  // pure matrix
		                	if(mat[grainNb]->get_pos_maxD()>-1)  // EP case
		                		return (idsdv + mat[grainNb]->get_pos_maxD());
		                	return -1;
		                	break;
				
				case MTSecF:  // MFH Fisrst order secant
					if(mat[grainNb]->get_pos_mtx_maxD()>-1)  // MT-Inc phase
						return (idsdv + mat[grainNb]->get_pos_mtx_maxD());
					return -1;
					break;
					
				default:
					return -1;
					break;
			}        
                }
            
		// here we only work for the initial value, will not work to block damage evolution in the grain on the fly
                virtual void populateMaxD(double *statev, double Dmax_Inc, double Dmax_Mtx) const {                    
                	int idsdv = 18;  // position in statev where the statev of the first phase start
                	for(int i=0;i<NGrain;i++) {
                        	switch(mat[i]->get_constmodel()){
                        		case EP:  // pure matrix
                        			if (mat[i]->get_pos_maxD()>-1)
		                		{
		                    			statev[idsdv + mat[i]->get_pos_maxD()] = Dmax_Mtx;
		                		}
		                		break;
		                		
		                	case MTSecF:  // MFH First order secant
		                		if(mat[i]->get_pos_inc_maxD()>-1)  // MT-Inc phase
		                		{
		                    			statev[idsdv + mat[i]->get_pos_inc_maxD()] = Dmax_Inc;
		                		}
		                		if(mat[i]->get_pos_mtx_maxD()>-1)  // MT-Mtx phase
		                		{
		                    			statev[idsdv + mat[i]->get_pos_mtx_maxD()] = Dmax_Mtx;
		                		}
		                		break;
		                		
		                	case LAM_2PLY:
		                		for(int j=0;j<mat[i]->getNPatch();j++)
		                		{
		                			if(mat[i]->get_pos_mat_inc_maxD(j)>-1)  // LAM_2PLY-Inc phase
		                			{
		                    				statev[idsdv + mat[i]->get_pos_mat_inc_maxD(j)] = Dmax_Inc;
		                			}
		                			if(mat[i]->get_pos_mat_mtx_maxD(j)>-1)  // LAM_2PLY-Mtx phase
		                			{
		                    				statev[idsdv + mat[i]->get_pos_mat_mtx_maxD(j)] = Dmax_Mtx;
		                			}
		                		}
		                		break;
		                }
		                
		                idsdv += mat[i]->get_nsdv();
		        }
                }
                
                // for VT case (VM or VLM)
                // for each VT grain
                // strain
                virtual int get_pos_mat_strain (int grainNb) const
                {
                    int idsdv = 18;  // position in statev where the statev of the first phase start
                    for(int i=0;i<grainNb;i++){
                        idsdv += mat[i]->get_nsdv();
                    }
		    return idsdv + mat[grainNb]->get_pos_strn();
		}
		// stress
		virtual int get_pos_mat_stress (int grainNb) const
                {
                    int idsdv = 18;  // position in statev where the statev of the first phase start
                    for(int i=0;i<grainNb;i++){
                        idsdv += mat[i]->get_nsdv();
                    }
		    return idsdv + mat[grainNb]->get_pos_strs();
		}
		// damage
		virtual int get_pos_mat_Dam (int grainNb) const
                {
                    if(mat[grainNb]->get_pos_damagefornonlocal()>-1){
                        int idsdv = 18;  // position in statev where the statev of the first phase start
                        for(int i=0;i<grainNb;i++){
                            idsdv += mat[i]->get_nsdv();
                        }
		        return idsdv + mat[grainNb]->get_pos_damagefornonlocal();
		    }
		    else{
		        return -1;
		    }
		}
		// for each material phase
		// strain
		virtual int get_pos_mat_mtx_strain (int grainNb) const
		{
		    int idsdv = 18;  // position in statev where the statev of the first phase start
                    for(int i=0;i<grainNb;i++){
                        idsdv += mat[i]->get_nsdv();
                    }
		    return idsdv + mat[grainNb]->get_pos_mtx_strain();
		}
		virtual int get_pos_mat_inc_strain (int grainNb) const
		{
		    int idsdv = 18;  // position in statev where the statev of the first phase start
                    for(int i=0;i<grainNb;i++){
                        idsdv += mat[i]->get_nsdv();
                    } 
		    return idsdv + mat[grainNb]->get_pos_inc_strain();
		}
		// stress
		virtual int get_pos_mat_mtx_stress (int grainNb) const
		{
		    int idsdv = 18;  // position in statev where the statev of the first phase start
                    for(int i=0;i<grainNb;i++){
                        idsdv += mat[i]->get_nsdv();
                    }
		    return idsdv + mat[grainNb]->get_pos_mtx_stress();
		}
		virtual int get_pos_mat_inc_stress (int grainNb) const
		{
		    int idsdv = 18;  // position in statev where the statev of the first phase start
                    for(int i=0;i<grainNb;i++){
                        idsdv += mat[i]->get_nsdv();
                    }
		    return idsdv + mat[grainNb]->get_pos_inc_stress();
		}
		// damage
		virtual int get_pos_mat_mtx_Dam (int grainNb) const
		{
		    if(mat[grainNb]->get_pos_mtx_Dam()>-1){
                        int idsdv = 18;  // position in statev where the statev of the first phase start
                        for(int i=0;i<grainNb;i++){
                            idsdv += mat[i]->get_nsdv();
                        }
		        return idsdv + mat[grainNb]->get_pos_mtx_Dam();
		    }
		    else{
		        return -1;
		    }
		}
		virtual int get_pos_mat_inc_Dam (int grainNb) const
		{
		    if(mat[grainNb]->get_pos_inc_Dam()>-1){
                        int idsdv = 18;  // position in statev where the statev of the first phase start
                        for(int i=0;i<grainNb;i++){
                            idsdv += mat[i]->get_nsdv();
                        }
		        return idsdv + mat[grainNb]->get_pos_inc_Dam();
		    }
		    else{
		        return -1;
		    }
		}

		// for VT case (VM or VLM)
		// for VLM case
		// laminate euler angles
		virtual std::vector<double> &getEulerVTLam(std::vector<double> &euler_lam, int grainNb)
		{
			euler_lam.clear();
			euler_lam.emplace_back(euler_vt_lam[0+grainNb*3]);
			euler_lam.emplace_back(euler_vt_lam[1+grainNb*3]);
			euler_lam.emplace_back(euler_vt_lam[2+grainNb*3]);
			return euler_lam;
		}
		// for each VT grain
		// strain
		virtual int getPosStrnVTM0(int grainNb) const {return (int)pos_strn_vt_m0[grainNb];}
		virtual int getPosStrnVTMT(int grainNb) const {return (int)pos_strn_vt_mt[grainNb];}
		virtual int getPosStrnVTLam(int grainNb) const {return (int)pos_strn_vt_lam[grainNb];}
		// stress
		virtual int getPosStrsVTM0(int grainNb) const {return (int)pos_strs_vt_m0[grainNb];}
		virtual int getPosStrsVTMT(int grainNb) const {return (int)pos_strs_vt_mt[grainNb];}
		virtual int getPosStrsVTLam(int grainNb) const {return (int)pos_strs_vt_lam[grainNb];}
		// damage
		virtual int getPosDamVTM0(int grainNb) const {return (int)pos_dam_vt_m0[grainNb];}
		// for each laminate ply in VT grains
		// strain
		virtual int getPosStrnVTLamM0(int grainNb) const {return (int)pos_strn_vt_lam_m0[grainNb];}
		virtual int getPosStrnVTLamMT(int grainNb) const {return (int)pos_strn_vt_lam_mt[grainNb];}
		// stress
		virtual int getPosStrsVTLamM0(int grainNb) const {return (int)pos_strs_vt_lam_m0[grainNb];}
		virtual int getPosStrsVTLamMT(int grainNb) const {return (int)pos_strs_vt_lam_mt[grainNb];}
		// damage
		virtual int getPosDamVTLamM0(int grainNb) const {return (int)pos_dam_vt_lam_m0[grainNb];}
                // for each material phase
                // strain
                virtual int getPosStrnVTMTMtx(int grainNb) const {return (int)pos_strn_vt_mt_mtx[grainNb];}
                virtual int getPosStrnVTMTInc(int grainNb) const {return (int)pos_strn_vt_mt_inc[grainNb];}
                virtual int getPosStrnVTLamMTMtx(int grainNb) const {return (int)pos_strn_vt_lam_mt_mtx[grainNb];}
                virtual int getPosStrnVTLamMTInc(int grainNb) const {return (int)pos_strn_vt_lam_mt_inc[grainNb];}
                // stress
                virtual int getPosStrsVTMTMtx(int grainNb) const {return (int)pos_strs_vt_mt_mtx[grainNb];}
                virtual int getPosStrsVTMTInc(int grainNb) const {return (int)pos_strs_vt_mt_inc[grainNb];}
                virtual int getPosStrsVTLamMTMtx(int grainNb) const {return (int)pos_strs_vt_lam_mt_mtx[grainNb];}
                virtual int getPosStrsVTLamMTInc(int grainNb) const {return (int)pos_strs_vt_lam_mt_inc[grainNb];}
                // damage
                virtual int getPosDamVTMTMtx(int grainNb) const {return (int)pos_dam_vt_mt_mtx[grainNb];}
                virtual int getPosDamVTMTInc(int grainNb) const {return (int)pos_dam_vt_mt_inc[grainNb];}
                virtual int getPosDamVTLamMTMtx(int grainNb) const {return (int)pos_dam_vt_lam_mt_mtx[grainNb];}
                virtual int getPosDamVTLamMTInc(int grainNb) const {return (int)pos_dam_vt_lam_mt_inc[grainNb];}
#endif

};  

//  Laminate of 2-ply  ****************************************************************
class LAM2Ply_Material : public Material{

	private:
		int NPly=2;					//number of plies
		Material** mat;		//array of pointers on Materials
		double* vf;				//array of volume fraction of each ply
                int idsdv_A, idsdv_B;
                double* euler;
                int pos_C;
                
#ifdef NONLOCALGMSH
		// for LAM_2PLY case (LVM)
		// for each laminate ply
		// strain
		std::vector<int> pos_strn_lam_m0, pos_strn_lam_vt;
		// stress
		std::vector<int> pos_strs_lam_m0, pos_strs_lam_vt;
		// damage
		std::vector<int> pos_dam_lam_m0;
		// for each MT grain in VT ply
		// strain
		std::vector<int> pos_strn_lam_vt_mt;
		// stress
		std::vector<int> pos_strs_lam_vt_mt;
		// for each material phase
		// strain
		std::vector<int> pos_strn_lam_vt_mt_mtx, pos_strn_lam_vt_mt_inc;
		// stress
		std::vector<int> pos_strs_lam_vt_mt_mtx, pos_strs_lam_vt_mt_inc;
		// damage
		std::vector<int> pos_dam_lam_vt_mt_mtx, pos_dam_lam_vt_mt_inc;
#endif                
	
	public:
		
		LAM2Ply_Material(double* props, int idmat);
		~LAM2Ply_Material();

		virtual void print();
		virtual int constbox(double* dstrn, double* strs_n,  double* strs, double* statev_n, double* statev, double** Cref, double*** dCref, double* tau, double** dtau, double** Calgo, double alpha, double* dpdE, double* strs_dDdp_bar, double *Sp_bar, double** c_g, double* dFd_d_bar, double* dstrs_dFd_bar, double *dFd_dE, double** c_gF, double* dpdFd, double* dFddp, int kinc, int kstep, double dt);
		virtual void get_refOp(double* statev, double** C, int kinc, int kstep);
		virtual void get_elOp(double** Cel);
		virtual void get_elOD(double* statev, double** Cel);
                virtual void unload_step(double* dstrn, double* strs_n, double* statev, int kinc, int kstep);
                virtual int get_pos_C() const {	return pos_C; }

		virtual std::vector<double> &getEuler(std::vector<double> &_euler)
		{
			_euler.clear();
			_euler.emplace_back(euler[0]);
			_euler.emplace_back(euler[1]);
			_euler.emplace_back(euler[2]);
			return _euler;
		}

		virtual int getNPatch() const {return NPly;}
		virtual int get_mat_NPatch(int laminateNb) const {return mat[laminateNb]->getNPatch();}
		virtual int get_mat_constmodel(int laminateNb) const {return mat[laminateNb]->get_constmodel();}
		virtual int get_submat_constmodel(int laminateNb, int grainNb) const {return mat[laminateNb]->get_mat_constmodel(grainNb);}

#ifdef NONLOCALGMSH
		virtual int get_pos_currentplasticstrainfornonlocal() const;
		virtual int get_pos_effectiveplasticstrainfornonlocal() const;
		virtual int get_pos_damagefornonlocal() const;
                virtual double getElasticEnergy (double* statev) ;
		virtual double getPlasticEnergy (double* statev) ;

		/*virtual void setLocalDamage()
		{
                    Material::setLocalDamage();
                    for(i=0;i<NPly;i++){
		        mat[i]->setLocalDamage();
                    }
                }*/
                
                virtual int get_pos_mat_inc_maxD(int laminateNb) const {             
                	int idsdv = 39;  // position in statev where the statev of the first phase start
                	for(int i=0;i<laminateNb;i++) {
                		idsdv += mat[i]->get_nsdv();
                	}
                	
			switch(mat[laminateNb]->get_constmodel()){		                
				case MTSecF:  // MFH Fisrst order secant
					if(mat[laminateNb]->get_pos_inc_maxD()>-1)  // MT-Inc phase
						return (idsdv + mat[laminateNb]->get_pos_inc_maxD());
					return -1;
					break;
				
				default:
					return -1;
					break;
				
			}        
                }
                
                virtual int get_pos_mat_mtx_maxD(int laminateNb) const {             
                	int idsdv = 39;  // position in statev where the statev of the first phase start
                	for(int i=0;i<laminateNb;i++) {
                		idsdv += mat[i]->get_nsdv();
                	}
                	
			switch(mat[laminateNb]->get_constmodel()){		                
				case EP:  // pure matrix
		                	if(mat[laminateNb]->get_pos_maxD()>-1)  // EP case
						return (idsdv + mat[laminateNb]->get_pos_maxD());
		                	return -1;
		                	break;
				
				case MTSecF:  // MFH Fisrst order secant
					if(mat[laminateNb]->get_pos_mtx_maxD()>-1)  // MT-Inc phase
						return (idsdv + mat[laminateNb]->get_pos_mtx_maxD());
					return -1;
					break;
					
				default:
					return -1;
					break;
			}        
                }
                
                // here we only work for the initial value, will not work to block damage evolution in the grain on the fly
                virtual void populateMaxD(double *statev, double Dmax_Inc, double Dmax_Mtx) const {                    
                	int idsdv = 39;  // position in statev where the statev of the first phase start
                	for(int i=0;i<NPly;i++) {
                        	switch(mat[i]->get_constmodel()){
                        		case EP:  // pure matrix
                        			if (mat[i]->get_pos_maxD()>-1)
		                		{
		                    			statev[idsdv + mat[i]->get_pos_maxD()] = Dmax_Mtx;
		                		}
		                		break;
		                		
		                	case MTSecF:  // MFH First order secant
		                		if(mat[i]->get_pos_inc_maxD()>-1)  // MT-Inc phase
		                		{
		                    			statev[idsdv + mat[i]->get_pos_inc_maxD()] = Dmax_Inc;
		                		}
		                		if(mat[i]->get_pos_mtx_maxD()>-1)  // MT-Mtx phase
		                		{
		                    			statev[idsdv + mat[i]->get_pos_mtx_maxD()] = Dmax_Mtx;
		                		}
		                		break;
		                		
		                	case VT:
		                		for(int j=0;j<mat[i]->getNPatch();j++)
		                		{
		                			if(mat[i]->get_pos_mat_inc_maxD(j)>-1)  // LAM_2PLY-Inc phase
		                			{
		                    				statev[idsdv + mat[i]->get_pos_mat_inc_maxD(j)] = Dmax_Inc;
		                			}
		                			if(mat[i]->get_pos_mat_mtx_maxD(j)>-1)  // LAM_2PLY-Mtx phase
		                			{
		                    				statev[idsdv + mat[i]->get_pos_mat_mtx_maxD(j)] = Dmax_Mtx;
		                			}
		                		}
		                		break;
		                }
		                
		                idsdv += mat[i]->get_nsdv();
		        }
                }

		/*virtual std::vector<int> &get_allPos_mtx_maxD() const {
                    std::vector<int> allPos_mtx_maxD;
                    
                    int idsdv = 39;  // position in statev where the statev of the first phase start                    
		    for(int i=0;i<NPly;i++) {
		        switch(mat[i]->get_constmodel()){
		            case EP:  // pure matrix
		                if (mat[i]->get_pos_maxD()>-1)  // EP case
		                {
		                    allPos_mtx_maxD.emplace_back(idsdv + mat[i]->get_pos_maxD());
		                }
		                break;
		                
		            case MTSecF:  // MFH Fisrst order secant
		                if(mat[i]->get_pos_mtx_maxD()>-1)  // MT-Mtx phase
		                {
		                    allPos_mtx_maxD.emplace_back(idsdv + mat[i]->get_pos_mtx_maxD());
		                }
		                break;
		                
		            case VT:
		                std::vector<int> allPos_mat_mtx_maxD = mat[i]->get_allPos_mtx_maxD();
		                for(int j=0;j<allPos_mat_mtx_maxD.size();j++)  // LAM_2PLY-Mtx phase
		                {
		                    allPos_mtx_maxD.emplace_back(idsdv + allPos_mat_mtx_maxD[j]);
		                }
		                break;		        
		        }
		        		        
		        idsdv += mat[i]->get_nsdv();
		    }
		    
		    return allPos_mtx_maxD;
                }

                virtual void populateMaxD(double *statev, double Dmax_Inc, double Dmax_Mtx) const {
                    std::vector<int> allPos_inc_maxD = this->get_allPos_inc_maxD();
		    for(int i=0;i<allPos_inc_maxD.size();i++) {
		        statev[allPos_inc_maxD[i]] = Dmax_Inc;
		    }
		    std::vector<int> allPos_mtx_maxD = this->get_allPos_mtx_maxD();
		    for(int i=0;i<allPos_mtx_maxD.size();i++) {
		        statev[allPos_mtx_maxD[i]] = Dmax_Mtx;
		    }
                }*/

		// for LAM_2PLY case (LVM)
                // for each laminate ply
                // strain
                virtual int get_pos_mat_strain (int laminateNb) const
                {
                    int idsdv = 39;  // position in statev where the statev of the first phase start
                    for(int i=0;i<laminateNb;i++){
                        idsdv += mat[i]->get_nsdv();
                    }
		    return idsdv + mat[laminateNb]->get_pos_strn();
		}
		// stress
		virtual int get_pos_mat_stress (int laminateNb) const
                {
                    int idsdv = 39;  // position in statev where the statev of the first phase start
                    for(int i=0;i<laminateNb;i++){
                        idsdv += mat[i]->get_nsdv();
                    }
		    return idsdv + mat[laminateNb]->get_pos_strs();
		}
		// damage
		virtual int get_pos_mat_Dam (int laminateNb) const
                {
                    if(mat[laminateNb]->get_pos_damagefornonlocal()>-1){
                        int idsdv = 39;  // position in statev where the statev of the first phase start
                        for(int i=0;i<laminateNb;i++){
                            idsdv += mat[i]->get_nsdv();
                        }
		        return idsdv + mat[laminateNb]->get_pos_damagefornonlocal();
		    }
		    else{
		        return -1;
		    }
		}
		// for each material phase
		// strain
		virtual int get_pos_mat_mtx_strain (int laminateNb) const
                {
                    int idsdv = 39;  // position in statev where the statev of the first phase start
                    for(int i=0;i<laminateNb;i++){
                        idsdv += mat[i]->get_nsdv();
                    }
		    return idsdv + mat[laminateNb]->get_pos_mtx_strain();
		}
		virtual int get_pos_mat_inc_strain (int laminateNb) const
                {
                    int idsdv = 39;  // position in statev where the statev of the first phase start
                    for(int i=0;i<laminateNb;i++){
                        idsdv += mat[i]->get_nsdv();
                    }
		    return idsdv + mat[laminateNb]->get_pos_inc_strain();
		}
		// stress
		virtual int get_pos_mat_mtx_stress (int laminateNb) const
                {
                    int idsdv = 39;  // position in statev where the statev of the first phase start
                    for(int i=0;i<laminateNb;i++){
                        idsdv += mat[i]->get_nsdv();
                    }
		    return idsdv + mat[laminateNb]->get_pos_mtx_stress();
		}
		virtual int get_pos_mat_inc_stress (int laminateNb) const
                {
                    int idsdv = 39;  // position in statev where the statev of the first phase start
                    for(int i=0;i<laminateNb;i++){
                        idsdv += mat[i]->get_nsdv();
                    }
		    return idsdv + mat[laminateNb]->get_pos_inc_stress();
		}
		// damage
		virtual int get_pos_mat_mtx_Dam (int laminateNb) const
                {
                    if(mat[laminateNb]->get_pos_mtx_Dam()>-1){
                        int idsdv = 39;  // position in statev where the statev of the first phase start
                        for(int i=0;i<laminateNb;i++){
                            idsdv += mat[i]->get_nsdv();
                        }
		        return idsdv + mat[laminateNb]->get_pos_mtx_Dam();
		    }
		    else{
		        return -1;
		    }
		}
		virtual int get_pos_mat_inc_Dam (int laminateNb) const
                {
                    if(mat[laminateNb]->get_pos_inc_Dam()>-1){
                        int idsdv = 39;  // position in statev where the statev of the first phase start
                        for(int i=0;i<laminateNb;i++){
                            idsdv += mat[i]->get_nsdv();
                        }
		        return idsdv + mat[laminateNb]->get_pos_inc_Dam();
		    }
		    else{
		        return -1;
		    }
		}
		
		// for LAM_2PLY case (LVM)
		// for each laminate ply
		// strain
		virtual int getPosStrnLamM0(int laminateNb) const {return (int)pos_strn_lam_m0[laminateNb];}
		virtual int getPosStrnLamVT(int laminateNb) const {return (int)pos_strn_lam_vt[laminateNb];}
		// stress
		virtual int getPosStrsLamM0(int laminateNb) const {return (int)pos_strs_lam_m0[laminateNb];}
		virtual int getPosStrsLamVT(int laminateNb) const {return (int)pos_strs_lam_vt[laminateNb];}
		// damage
		virtual int getPosDamLamM0(int laminateNb) const {return (int)pos_dam_lam_m0[laminateNb];}
		// for each MT grain in VT ply
		// strain
		virtual int getPosStrnLamVTMT(int grainNb) const {return (int)pos_strn_lam_vt_mt[grainNb];}
		// stress
		virtual int getPosStrsLamVTMT(int grainNb) const {return (int)pos_strs_lam_vt_mt[grainNb];}
		// for each material phase
		// strain
		virtual int getPosStrnLamVTMTMtx(int grainNb) const {return (int)pos_strn_lam_vt_mt_mtx[grainNb];}
		virtual int getPosStrnLamVTMTInc(int grainNb) const {return (int)pos_strn_lam_vt_mt_inc[grainNb];}
		// stress
		virtual int getPosStrsLamVTMTMtx(int grainNb) const {return (int)pos_strs_lam_vt_mt_mtx[grainNb];}
		virtual int getPosStrsLamVTMTInc(int grainNb) const {return (int)pos_strs_lam_vt_mt_inc[grainNb];}
		// damage
		virtual int getPosDamLamVTMTMtx(int grainNb) const {return (int)pos_dam_lam_vt_mt_mtx[grainNb];}
		virtual int getPosDamLamVTMTInc(int grainNb) const {return (int)pos_dam_lam_vt_mt_inc[grainNb];}
#endif

};  
// Composite material homogenized with Mori-Tanaka scheme
class MT_Material : public Material{

	protected:
		int Nph;
		Material* mtx_mat;
		Material* icl_mat;
		int idsdv_m, idsdv_i;
		double vf_i, vf_m;
		double* ar;
		double* euler;
		int pos_C;
       
	public:
		
		MT_Material(double* props, int idmat);
		~MT_Material();

		virtual void print();
		virtual int constbox(double* dstrn, double* strs_n,  double* strs, double* statev_n, double* statev, double** Cref, double*** dCref, double* tau, double** dtau, double** Calgo, double alpha, double* dpdE, double* strs_dDdp_bar, double *Sp_bar, double** c_g, double* dFd_d_bar, double* dstrs_dFd_bar, double *dFd_dE, double** c_gF, double* dpdFd, double* dFddp, int kinc, int kstep, double dt);
		virtual int get_pos_C() const {return pos_C; } 
	
		virtual void get_refOp(double* statev, double** C, int kinc, int kstep);
		virtual void get_elOp(double** Cel);
		virtual void get_elOD(double* statev, double** Cel);
                virtual void unload_step(double* dstrn, double* strs_n, double* statev, int kinc, int kstep);

                virtual int get_pos_inc_maxD() const {
                   if(icl_mat->get_pos_maxD()>-1)
	              return icl_mat->get_pos_maxD()+idsdv_i;
                   return -1;
                }
                virtual double get_inc_MaxD(double *statev) const {
                   if(icl_mat->get_pos_maxD()>-1)
	              return statev[get_pos_inc_maxD()];
                   return -1.;
                }
                virtual void set_inc_MaxD(double *statev, double Dmax)
                {
                   if(icl_mat->get_pos_maxD()>-1)
	              statev[get_pos_inc_maxD()]=Dmax;
                }

                virtual int get_pos_mtx_maxD() const {
                   if(mtx_mat->get_pos_maxD()>-1)
	              return mtx_mat->get_pos_maxD()+idsdv_m;
                   return -1;
                }
                virtual double get_mtx_MaxD(double *statev) const {
                   if(mtx_mat->get_pos_maxD()>-1)
	              return statev[get_pos_mtx_maxD()];
                   return -1.;
                }
                virtual void set_mtx_MaxD(double *statev, double Dmax)
                {
                   if(mtx_mat->get_pos_maxD()>-1)
	              statev[get_pos_mtx_maxD()]=Dmax;
                }

		virtual int getNbNonLocalVariables() const 
                {
                   int nb=0;
                   nb=nb+mtx_mat->getNbNonLocalVariables();
                   nb=nb+icl_mat->getNbNonLocalVariables(); 
                   return nb;
                }
                virtual int get_pos_Fd_bar()const;
                virtual int get_pos_dFd_bar()const;
                virtual int get_pos_locFd() const;


#ifdef NONLOCALGMSH
		virtual int get_pos_currentplasticstrainfornonlocal() const;
		virtual int get_pos_effectiveplasticstrainfornonlocal() const;
		virtual int get_pos_damagefornonlocal() const;
                virtual int get_pos_mtx_stress () const;
                virtual int get_pos_inc_stress () const;
                virtual int get_pos_mtx_strain () const;
                virtual int get_pos_inc_strain () const;
                virtual int get_pos_mtx_Dam () const;
                virtual int get_pos_inc_Dam () const;
                virtual double getElasticEnergy (double* statev) ;
		virtual double getPlasticEnergy (double* statev) ;
		
		virtual void setEuler(double e1, double e2, double e3);

		/*virtual void setLocalDamage()
		{
                    Material::setLocalDamage();
                    mtx_mat->setLocalDamage();
                    inc_mat->setLocalDamage();
                }*/
#endif
};


//Composite material homogenized with Mori-Tanaka scheme with fist-order Incremental-secant method
class MTSecF_Material : public MT_Material{

	private:
                 ELcc LCC;
	public:
		
		MTSecF_Material(double* props, int idmat);
		~MTSecF_Material();
		

		virtual int constbox(double* dstrn, double* strs_n,  double* strs, double* statev_n, double* statev, double** Cref, double*** dCref, double* tau, double** dtau, double** Calgo, double alpha, double* dpdE, double* strs_dDdp_bar, double *Sp_bar, double** c_g, double* dFd_d_bar, double* dstrs_dFd_bar, double *dFd_dE, double** c_gF,  double* dpdFd, double* dFddp, int kinc, int kstep, double dt);
                virtual void unload_step(double* dstrn, double* strs_n, double* statev, int kinc, int kstep);
                
#ifdef NONLOCALGMSH
		virtual void get_elOp_mtx(double* statev, double** Cel);
		virtual void get_elOp_icl(double* statev, double** Cel);
		virtual void get_elOp_mtx(double** Cel);
		virtual void get_elOp_icl(double** Cel);
                inline double get_vfI() {return vf_i;}
#endif
};

//Composite material homogenized with Mori-Tanaka scheme with incremental secant method, second moment for the elastic predictor
class MTSecNtr_Material : public MT_Material{

	private:
                  ELcc LCC;
	public:
		
		MTSecNtr_Material(double* props, int idmat);
		~MTSecNtr_Material();

		virtual int constbox(double* dstrn, double* strs_n,  double* strs, double* statev_n, double* statev, double** Cref, double*** dCref, double* tau, double** dtau, double** Calgo, double alpha, double* dpdE, double* strs_dDdp_bar, double *Sp_bar, double** c_g, double* dFd_d_bar, double* dstrs_dFd_bar, double *dFd_dE, double** c_gF,double* dpdFd, double* dFddp, int kinc, int kstep, double dt);
                virtual void unload_step(double* dstrn, double* strs_n, double* statev, int kinc, int kstep);
};

//By Ling Wu AUG 2013
//Composite material homogenized with Mori-Tanaka scheme with Second-order Incremental-secant method
class MTSecSd_Material : public MT_Material{

	private:
                  Lcc LCC;
	public:
		
		MTSecSd_Material(double* props, int idmat);
		~MTSecSd_Material();

		virtual int constbox(double* dstrn, double* strs_n,  double* strs, double* statev_n, double* statev, double** Cref, double*** dCref, double* tau, double** dtau, double** Calgo, double alpha, double* dpdE, double* strs_dDdp_bar, double *Sp_bar, double** c_g, double* dFd_d_bar, double* dstrs_dFd_bar, double *dFd_dE, double** c_gF,  double* dpdFd, double* dFddp, int kinc, int kstep, double dt);
                virtual void unload_step(double* dstrn, double* strs_n, double* statev, int kinc, int kstep);
};

//By Ling Wu May 2016
//Composite material homogenized with Mori-Tanaka scheme with First-order Incremental-secant method
//when vfi > 1, volume fraction is a random variable depends on the location of Gaussian point.
//when euler > 180, euler angles are random variables depend on the location of Gaussian point.
class MTSecF_Stoch_Material : public MT_Material{

	private:
                 int pos_vfi;
                 int pos_euler;
                 int pos_aspR;      //aspect ratio  ar2/ar1, ar1=1.0, ar2=aspR
                 int pos_icl_euler;
	public:
		
		MTSecF_Stoch_Material(double* props, int idmat);
		~MTSecF_Stoch_Material();
		virtual int constbox(double* dstrn, double* strs_n,  double* strs, double* statev_n, double* statev, double** Cref, double*** dCref, double* tau, double** dtau, double** Calgo, double alpha, double* dpdE, double* strs_dDdp_bar, double *Sp_bar, double** c_g, double* dFd_d_bar, double* dstrs_dFd_bar, double *dFd_dE, double** c_gF, double* dpdFd, double* dFddp, int kinc, int kstep, double dt);
                virtual void unload_step(double* dstrn, double* strs_n, double* statev_n, double _Vfm, double _Vfi, 
                                                                          double* _euler, double* _ar, int kinc, int kstep);
		virtual int get_pos_vfi() const {  return pos_vfi; } 
		virtual int get_pos_euler() const {  return pos_euler; } 
		virtual int get_pos_aspR() const {  return pos_aspR; } 

		virtual int get_pos_E()const; 
		virtual int get_pos_nu()const;  
		virtual int get_pos_sy0()const;   
		virtual int get_pos_hmod1()const;  
		virtual int get_pos_hmod2()const;  
		virtual int get_pos_alpha_DP ()const; 
		virtual int get_pos_m_DP ()const;
		virtual int get_pos_nup ()const;
		virtual int get_pos_hp0()const;  
		virtual int get_pos_hexp()const;  
		virtual int get_pos_DamParm1()const;  
		virtual int get_pos_DamParm2()const;   
		virtual int get_pos_DamParm3()const;
		virtual int get_pos_INCDamParm1()const; 
		virtual int get_pos_INCDamParm2()const; 
		virtual int get_pos_INCDamParm3()const; 
		virtual int get_pos_Fd_bar()const;
		virtual int get_pos_dFd_bar()const;
		virtual int get_pos_locFd() const;
                

#ifdef NONLOCALGMSH
                virtual double getElasticEnergy (double* statev);
		virtual double getPlasticEnergy (double* statev);
		virtual void get_elOp_mtx(double* statev, double** Cel);
		virtual void get_elOp_icl(double* statev, double** Cel);
		virtual void get_elOp_mtx(double** Cel);
		virtual void get_elOp_icl(double** Cel);
                inline double get_vfI() {return vf_i;}
#endif
                virtual void get_refOp(double* statev, double** C, int kinc, int kstep);
                virtual void get_elOp(double* statev, double** Cel);
		virtual void get_elOD(double* statev, double** Cel);
};


//By Ling Wu Dec 2016
//Composite material homogenized with Mori-Tanaka scheme with Second-order Incremental-secant method
// "vfi"- volume fraction and "euler" are variables in the case of large deformation.
class MTSecF_LargD : public MT_Material{

	private:
                 int pos_vfi;
                 int pos_aspR;
                 int pos_R66;
                 int pos_dR;
                 
	public:
		
		MTSecF_LargD(double* props, int idmat);
		~MTSecF_LargD();
		virtual int constbox(double* dstrn, double* strs_n,  double* strs, double* statev_n, double* statev, double** Cref, double*** dCref, double* tau, double** dtau, double** Calgo, double alpha, double* dpdE, double* strs_dDdp_bar, double *Sp_bar, double** c_g, double* dFd_d_bar, double* dstrs_dFd_bar, double *dFd_dE, double** c_gF, double* dpdFd, double* dFddp,  int kinc, int kstep, double dt);
                virtual void unload_step(double* dstrn, double* strs_n, double* statev_n, double _Vfm, double _Vfi, 
                                                                                       double** R66, int kinc, int kstep);
		virtual int get_pos_vfi() const {  return pos_vfi; } 
		virtual int get_pos_aspR() const {  return pos_aspR; }
		virtual int get_pos_R66() const {  return pos_R66; }
		virtual int get_pos_dR() const {  return pos_dR; }
                virtual int get_pos_Ri() const { return idsdv_i+icl_mat->get_pos_Ri(); }
		virtual int get_pos_Basem() const { return idsdv_m+mtx_mat->get_pos_Base(); }
		virtual int get_pos_Basei() const { return idsdv_i+icl_mat->get_pos_Base(); }
		virtual int get_pos_Fm() const { return idsdv_m+mtx_mat->get_pos_F(); }
		virtual int get_pos_Fi() const { return idsdv_i+icl_mat->get_pos_F(); }
		virtual int get_pos_mtx_Ja() const {return idsdv_m + mtx_mat->get_pos_Ja(); }
                virtual int get_pos_inc_Ja() const { return idsdv_i + icl_mat->get_pos_Ja(); }


                virtual bool isInLargeDeformation() const {return true;}
       

#ifdef NONLOCALGMSH
                virtual double getElasticEnergy (double* statev);
		virtual double getPlasticEnergy (double* statev);
#endif
                virtual void get_refOp(double* statev, double** _R66, double** C, int kinc, int kstep);
                virtual void get_elOp(double* statev, double** _R66, double** Cel);
		virtual void get_elOD(double* statev,  double** _R66, double** Cel);
};



// Composite material homogenized with Self-Consistent scheme  //la
class SC_Material : public Material{

	private:
		int Nph;
		int mtx;
		int Nicl;
		Material* mtx_mat;
		Material** icl_mat;
		int idsdv_m, *idsdv_i;
		double *vf_i, vf_m;
		double** ar;
		double** euler;
		int pos_C;

	public:
		
		SC_Material(double* props, int idmat);
		~SC_Material();

		virtual void print();
		virtual int constbox(double* dstrn, double* strs_n,  double* strs, double* statev_n, double* statev, double** Cref, double*** dCref, double* tau, double** dtau, double** Calgo, double alpha, double* dpdE, double* strs_dDdp_bar, double *Sp_bar, double** c_g, double* dFd_d_bar, double* dstrs_dFd_bar, double *dFd_dE, double** c_gF, double* dpdFd, double* dFddp, int kinc, int kstep, double dt);
		virtual int get_pos_C() const {	return pos_C; } 
		
		virtual void get_refOp(double* statev, double** C, int kinc, int kstep);
		virtual void get_elOp(double** Cel);
		virtual void get_elOD(double* statev, double** Cel);
                virtual void unload_step(double* dstrn, double* strs_n, double* statev, int kinc, int kstep);

#ifdef NONLOCALGMSH
		virtual int get_pos_currentplasticstrainfornonlocal() const;
		virtual int get_pos_effectiveplasticstrainfornonlocal() const;
		virtual int get_pos_damagefornonlocal() const;
                virtual int get_pos_mtx_stress () const;
                virtual int get_pos_inc_stress () const;
                virtual int get_pos_mtx_strain () const;
                virtual int get_pos_inc_strain () const;
                virtual double getElasticEnergy (double* statev) ;
		virtual double getPlasticEnergy (double* statev) ;
#endif
};



// Composite material with multi-phase homogenized with Two-step shemas(Mori-Tnaka/Voight )  add by Mohamed Janv 2018
class MT_VT_Material : public Material{

	private:
		int N_theta_increment;
                int Total_number_phase;
 		Material** ph_mat; 
                int *idsdv_i;
		double* Wgt;//   vector of weight for all phase 
		int pos_C;
                bool withDamage;
		int Total_number_of_AR_per_phase;

	public:
		MT_VT_Material(double* props, int idmat);
		~MT_VT_Material();

		virtual void print();
		virtual int constbox(double* dstrn, double* strs_n,  double* strs, double* statev_n, double* statev,double** Cref, double*** dCref, double* tau, double** dtau, double** Calgo, double alpha, double* dpdE, double* strs_dDdp_bar, double *Sp_bar, double** c_g, double* dFd_d_bar, double* dstrs_dFd_bar, double *dFd_dE, double** c_gF, double* dpdFd, double* dFddp, int kinc, int kstep, double dt);

		virtual int get_pos_C() const {	return pos_C; } 
		
		virtual void get_refOp(double* statev, double** C, int kinc, int kstep);
		virtual void get_elOp(double** Cel);
		virtual void get_elOD(double* statev, double** Cel);
                virtual void unload_step(double* dstrn, double* strs_n, double* statev, int kinc, int kstep);

#ifdef NONLOCALGMSH
		virtual int get_pos_currentplasticstrainfornonlocal() const;
		virtual int get_pos_effectiveplasticstrainfornonlocal() const;
		virtual int get_pos_damagefornonlocal() const;
                virtual int get_pos_mtx_stress () const;
                virtual int get_pos_inc_stress () const;
                virtual int get_pos_mtx_strain () const;
                virtual int get_pos_inc_strain () const;
                virtual double getElasticEnergy (double* statev) ;
		virtual double getPlasticEnergy (double* statev) ;
#endif

};  //           add by Mohamed MOHAMEDOU Janv 2018

// Anisotropic Linear elastic material
class ANEL_Material : public Material{

	private:
		double* anpr;
                double* euler;
                double cl;
                int pos_NFiber;
                int pos_Pdam;
		int pos_Fd_bar;
		int pos_dFd_bar;
		int pos_locFd;
                double dDeldD;
                double v12, v13, v31, E1, E3;
                int pos_euler;


	public:
		
		ANEL_Material(double* props, int idmat);
		~ANEL_Material();
		
		virtual void print();
		virtual int constbox(double* dstrn, double* strs_n,  double* strs, double* statev_n, double* statev, double** Cref, double*** dCref, double* tau, double** dtau, double** Calgo, double alpha, double* dpdE, double* strs_dDdp_bar, double *Sp_bar, double** c_g, double* dFd_d_bar, double* dstrs_dFd_bar, double *dFd_dE, double** c_gF, double* dpdFd, double* dFddp, int kinc, int kstep, double dt);

		virtual int constboxSecantMixte(double* dstrn, double* strs_n,  double* strs, double* statev_n, double* statev, double **Calgo, double** Csd, double*** dCsd, double** dCsdp_bar, double* dnu, double& dnudp_bar, double alpha, double* dpdE, double* dstrsdp_bar, double** c_g, int kinc, int kstep, double dt, bool forceZero, bool forceRes);

//******************new constbox for 2rd order method
                virtual int constbox_2order(int mtx, double *DE, double* dstrn, double* strs, double* statev_n, double* statev, double **Calgo, Lcc* LCC, YieldF* YF, double** c_g, int kinc, int kstep, double dt);

		virtual void get_refOp(double* statev, double** C, int kinc, int kstep);
		virtual void get_elOp(double** Cel);
                virtual void get_elOp(double* statev, double** Cel);

		virtual void get_elOD(double* statev, double** Cel);
                virtual void unload_step(double* dstrn, double* strs_n, double* statev, int kinc, int kstep);

		virtual int get_pos_DamParm1() const { return pos_NFiber; } 
		virtual int get_pos_DamParm2() const { return pos_Pdam; } 
		virtual int get_pos_Fd_bar() const {  return pos_Fd_bar; } 
		virtual int get_pos_dFd_bar() const {  return pos_dFd_bar; } 
		virtual int get_pos_locFd() const {  return pos_locFd; }
                virtual int get_pos_euler() const {  return pos_euler; }
                
                
#ifdef NONLOCALGMSH
		virtual int get_pos_currentplasticstrainfornonlocal() const;
		virtual int get_pos_effectiveplasticstrainfornonlocal() const;
		virtual int get_pos_damagefornonlocal() const;
                virtual double getElasticEnergy (double* statev) ;
		virtual double getPlasticEnergy (double* statev) ;
		
		virtual void setEuler(double e1, double e2, double e3);
#endif
		
};

#ifdef NONLOCALGMSH
#else
Damage* init_damage(double* props, int iddam);
Clength* init_clength(double* props, int idclength);
#endif
#ifdef NONLOCALGMSH
}
#endif
#endif

