#include "STensorOperations.h"


bool STensorOperation::polynomial(const STensor3& a, const int order, const fullVector<double>& coeffs, STensor3& F, STensor43* dF, STensor63* ddF){
  if (order+1 != coeffs.size()) {
    coeffs.print("coeffs");
    Msg::Error("approximation is wrongly defined STensorOperation::polynomialApproximation order = %d",order);
  };


  static const STensor3 I(1.);
  static const STensor43 I4(1.,1.);

  if (STensorOperation::isnan(a))
  {
    Msg::Info("STensorOperation::polynomial of a NAN tensor");
    STensorOperation::zero(F);
    if (dF!=NULL){
      (*dF) = I4;
      if (ddF!=NULL){
        STensorOperation::zero(*ddF);
      }
    }
    return false;
  }

  static STensor3 Bn;
  static STensor43 dBnDa;
  static STensor63 ddBnDDa;

  for (int j=0; j<3; j++){
    for (int k=0; k<3; k++){
      Bn(j,k) = I(j,k)*coeffs(order);
    }
  }
  if (dF != NULL){
    STensorOperation::zero(dBnDa);
    if (ddF!= NULL){
      STensorOperation::zero(ddBnDDa);
    }
  }

  for (int i=0; i< order; i++){
    STensorOperation::zero(F);
    if (dF != NULL){
      STensorOperation::zero(*dF);
      if (ddF != NULL){
        STensorOperation::zero(*ddF);
      }
    }

    int n =order-i;
    for (int j=0; j<3; j++){
      for (int k=0; k<3; k++){
        F(j,k) = coeffs(n-1)*I(j,k);
        for (int l=0; l<3; l++){
          F(j,k) +=  Bn(j,l)*a(l,k);
          if (dF != NULL){
            for (int r=0; r<3; r++){
              for (int s=0; s<3; s++){
                (*dF)(j,k,r,s) +=  dBnDa(j,l,r,s)*a(l,k) + Bn(j,l)*I4(l,k,r,s);
                if (ddF != NULL){
                  for (int p=0; p<3; p++){
                    for (int q=0; q<3; q++){
                      (*ddF)(j,k,r,s,p,q) +=  ddBnDDa(j,l,r,s,p,q)*a(l,k)+dBnDa(j,l,r,s)*I4(l,k,p,q) + dBnDa(j,l,p,q)*I4(l,k,r,s);
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
    Bn = F;
    if (dF != NULL){
      dBnDa = (*dF);
      if (ddF != NULL){
        ddBnDDa = (*ddF);
      }
    }
  }
  return true;
};

bool STensorOperation::logSTensor3(const STensor3& a, const int order, STensor3& loga, STensor43* dloga, STensor63* ddloga){
  // Tools
  if(determinantSTensor3(a)<1.e-24)
  {
    Msg::Info("STensorOperation::logSTensor3 of a tensor with a negative determinant");
    return false;
  }

  static const STensor3 mI(-1.);
  static const STensor43 I4(1.,1.);
  static STensor3 ami;
  STensorOperation::zero(ami);

  // Choose approximation order
  if (order == 1){
    // Order 1 : log(1+A) = A
    ami = mI;
    ami+= a;
    loga = ami;
    // First derivative
    if(dloga!=NULL)
    {
      (*dloga) = I4;
    }
    // Second derivative
    if(ddloga!=NULL)
    {
      STensorOperation::zero(*ddloga);
    }
  }
  else if (order > 1){
    static STensor3 I(1.);
    ami = mI;
    ami+= a;

    static fullVector<double> coeffs(order+1);

    for (int i=0; i< order+1; i++){
      if (i==0){
        coeffs(i) = 0.;
      }
      else if (i%2 == 0){
        coeffs(i) = -1./double(i);
      }
      else{
        coeffs(i) = 1./double(i);
      }
    }
    //coeffs.print("coeffs");

    bool ok =  STensorOperation::polynomial(ami,order,coeffs,loga,dloga,ddloga);
    if (!ok){
      //#ifdef _DEBUG
      Msg::Info("error in computing logSTensor3");
      STensorOperation::zero(loga);
      if( dloga!=NULL) STensorOperation::zero(*dloga);
      if( ddloga!=NULL) STensorOperation::zero(*ddloga);

      return false;
      //#endif // _DEBUG
    }
  }
  else if (order == -1)
  {
    /*
    static fullMatrix<double> m(3, 3);
    static fullVector<double> eigenValReal(3);
    static fullVector<double> eigenValImag(3);
    static fullMatrix<double> leftEigenVect(3,3);
    static fullMatrix<double> rightEigenVect(3,3);
    m.setAll(0.);
    eigenValReal.setAll(0.);
    eigenValImag.setAll(0.);
    leftEigenVect.setAll(0.);
    rightEigenVect.setAll(0.);

    // Get eigen values and vectors
    a.getMat(m);
    m.eig(eigenValReal,eigenValImag,leftEigenVect,rightEigenVect,0);

    // Spectral sum for log function
    for (int i=0; i<3; i++){
      for (int j=0; j<3; j++){
        loga(i,j) =  log(eigenValReal(0)) *leftEigenVect(i,0)*rightEigenVect(j,0)
                    +log(eigenValReal(1)) *leftEigenVect(i,1)*rightEigenVect(j,1)
                    +log(eigenValReal(2)) *leftEigenVect(i,2)*rightEigenVect(j,2);
      }
    }*/


    double x1, x2, x3;
    static STensor3 E1, E2, E3;
    STensorOperation::getEigenDecomposition(a,x1,x2,x3,E1,E2,E3);
    if(x1<=0. or x2<=0. or x3<=0.)
    {
       Msg::Error("STensorOperations: log of a tensor with negative eigenvalues");
       return false;
    }
    double logx1(0), logx2(0),logx3(0.);
    if (x1 >0.) logx1 = log(x1);
    if (x2 >0.) logx2 = log(x2);
    if (x3 >0.) logx3 = log(x3);
    for (int i=0; i<3; i++){
      for (int j=0; j<3; j++){

        loga(i,j) =  logx1*E1(i,j) + logx2*E2(i,j) + logx3*E3(i,j);
      }
    }


    // First derivative
    static STensor3 ainv;
    inverseSTensor3(a,ainv);
    if(dloga!=NULL)
    {
      STensorOperation::zero(*dloga);
      for (int i=0; i<3; i++){
        for (int j=0; j<3; j++){
          for (int k=0; k<3; k++){
            for (int l=0; l<3; l++){
              for (int r=0; r<3; r++){
                dloga->operator()(i,j,k,l) += I4(i,r,k,l)*ainv(r,j);
              }
            }
          }
        }
      }
    }
    //Second derivative
    if(ddloga!=NULL)
    {
      STensorOperation::inverseSTensor3(a,ainv);
      for (int i=0; i<3; i++){
        for (int j=0; j<3; j++){
          for (int k=0; k<3; k++){
            for (int l=0; l<3; l++){
              for (int m=0; m<3; m++){
                for (int n=0; n<3; n++){
                  ddloga->operator()(i,j,k,l,m,n) = 0.;
                  for (int r=0; r<3; r++){
                    for (int s=0; s<3; s++){
                      for (int q=0; q<3; q++){
                        ddloga->operator()(i,j,k,l,m,n) -= I4(i,r,k,l)*I4(r,s,m,n)*ainv(s,q)*ainv(q,j);
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }
  else
  {
    Msg::Error("order = %d is not implemented",order);
    return false;
  }
  return true;
}


bool STensorOperation::expSTensor3(const STensor3& a, const int order, STensor3& expa, STensor43* dexpa, STensor63* ddexpa)
{
  // Tools
  static const STensor3 I(1.);
  static const STensor43 I4(1.);

  // Choose approximation order
  if (order == 1)
  {
    // Order 1 : exp(A) = I + A
    expa = I;
    expa += a;
    // First derivative
    if(dexpa!=NULL)
    {
     (*dexpa) = I4;
    }
    // Second derivative
    if(ddexpa!=NULL)
    {
      STensorOperation::zero(*ddexpa);
    }
  }
  else if (order > 1){
    fullVector<double> coeffs(order+1);
    coeffs(0) =1.;
    coeffs(1) =1;
    for (int i=2; i< order+1; i++){
      coeffs(i) = coeffs(i-1)/double(i);
    }
    //coeffs.print("coeffs");

    bool ok = STensorOperation::polynomial(a,order,coeffs,expa,dexpa,ddexpa);
    if (!ok){
      #ifdef _DEBUG
      Msg::Error("error in computing expSTensor3");
      #endif // _DEBUG
    }
    return ok;
  }
  else if (order == -1)
  {

    /*static fullMatrix<double> m(3, 3);
    static fullVector<double> eigenValReal(3);
    static fullVector<double> eigenValImag(3);
    static fullMatrix<double> leftEigenVect(3,3);
    static fullMatrix<double> rightEigenVect(3,3);

    m.setAll(0.);
    eigenValReal.setAll(0.);
    eigenValImag.setAll(0.);
    leftEigenVect.setAll(0.);
    rightEigenVect.setAll(0.);

    // Get eigen values and vectors
    a.getMat(m);
    m.eig(eigenValReal,eigenValImag,leftEigenVect,rightEigenVect,0);

    // Spectral sum for exp function
    for (int i=0; i<3; i++){
      for (int j=0; j<3; j++){
        expa(i,j) =  exp(eigenValReal(0)) *leftEigenVect(i,0)*rightEigenVect(j,0)
                    +exp(eigenValReal(1)) *leftEigenVect(i,1)*rightEigenVect(j,1)
                    +exp(eigenValReal(2)) *leftEigenVect(i,2)*rightEigenVect(j,2);
      }
    }*/

    double x1, x2, x3;
    static STensor3 E1, E2, E3;
    STensorOperation::getEigenDecomposition(a,x1,x2,x3,E1,E2,E3);
    for (int i=0; i<3; i++){
      for (int j=0; j<3; j++){
        expa(i,j) =  exp(x1)*E1(i,j) + exp(x2)*E2(i,j) + exp(x3)*E3(i,j);
      }
    }
    // First derivative
    if(dexpa!=NULL)
    {
      for (int i=0; i<3; i++){
        for (int j=0; j<3; j++){
          for (int k=0; k<3; k++){
            for (int l=0; l<3; l++){
              dexpa->operator()(i,j,k,l) = 0.;
              for (int r=0; r<3; r++){
                dexpa->operator()(i,j,k,l) += I4(i,r,k,l)*expa(r,j);
              }
            }
          }
        }
      }
    }
    // Second derivative
    if(ddexpa!=NULL)
    {
      for (int i=0; i<3; i++){
        for (int j=0; j<3; j++){
          for (int k=0; k<3; k++){
            for (int l=0; l<3; l++){
              for (int m=0; m<3; m++){
                for (int n=0; n<3; n++){
                  ddexpa->operator()(i,j,k,l,m,n) = 0.;
                  for (int r=0; r<3; r++){
                    for (int s=0; s<3; s++){
                      ddexpa->operator()(i,j,k,l,m,n) += I4(i,r,k,l)*I4(r,s,m,n)*expa(s,j);
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }
  else
  {
    Msg::Error("order = %d is not implemented",order);
    return false;
  }
  return true;
}

// NEW
void STensorOperation::getEigenDecomposition(const STensor3& a, double& x1, double& x2, double& x3, STensor3& E1, STensor3& E2, STensor3& E3,
							fullVector<double> &eigenValReal, fullMatrix<double>& leftEigenVect, fullMatrix<double>& rightEigenVect){

	static fullMatrix<double> m(3, 3);
    static fullVector<double> eigenValImag(3);
    m.setAll(0.);
    eigenValReal.setAll(0.);
    eigenValImag.setAll(0.);
    leftEigenVect.setAll(0.);
    rightEigenVect.setAll(0.);

    STensorOperation::zero(E1); STensorOperation::zero(E2); STensorOperation::zero(E3);

    // Get eigen values and vectors
    a.getMat(m);
    m.eig(eigenValReal,eigenValImag,leftEigenVect,rightEigenVect,false);
    x1=eigenValReal(0);
    x2=eigenValReal(1);
    x3=eigenValReal(2);
    for(int i=0; i<3; i++)
    {
      for(int j=0; j<3; j++)
      {
        E1(i,j)= leftEigenVect(i,0)*rightEigenVect(j,0);
        E2(i,j)= leftEigenVect(i,1)*rightEigenVect(j,1);
        E3(i,j)= leftEigenVect(i,2)*rightEigenVect(j,2);
      }
    }
}

void STensorOperation::getEigenDecomposition(const STensor3& a, double& x1, double& x2, double& x3, STensor3& E1, STensor3& E2, STensor3& E3,
                                    STensor3& dx1da, STensor3& dx2da, STensor3& dx3da, STensor43& dE1da, STensor43& dE2da, STensor43& dE3da){

	getEigenDecomposition(a, x1, x2, x3, E1, E2, E3);

   /*
   double x1p,x2p,x3p,x1m,x2m,x3m;
   static STensor3 E1p,E2p,E3p, E1m,E2m, E3m, ap, am;
   double pert=1.e-9;

   for(int i=0; i<3; i++){
     for(int j=0; j<3; j++){
       ap=a;
       am=a;
       ap(i,j)=ap(i,j)+pert;
       getEigenDecomposition(ap, x1p, x2p, x3p, E1p, E2p, E3p);
       am(i,j)=am(i,j)-pert;
       getEigenDecomposition(am, x1m, x2m, x3m, E1m, E2m, E3m);
       dx1da(i,j)=(x1p-x1m)/2./pert;
       dx2da(i,j)=(x2p-x2m)/2./pert;
       dx3da(i,j)=(x3p-x3m)/2./pert;
       for(int k=0; k<3; k++)
       {
         for(int l=0; l<3; l++)
         {
           dE1da(k,l,i,j) = (E1p(k,l)-E1m(k,l))/2./pert;
           dE2da(k,l,i,j) = (E2p(k,l)-E2m(k,l))/2./pert;
           dE3da(k,l,i,j) = (E3p(k,l)-E3m(k,l))/2./pert;
         }
       }
     }
   }*/

   double tr = a.trace();
   double det = STensorOperation::determinantSTensor3(a);
   static STensor3 ainv, dI3da;
   STensorOperation::inverseSTensor3(a,ainv);
   dI3da = ainv;
   dI3da *= det;

   const STensor43 I4(1.,1.);
   static STensor43 dainvda;
   for (int i=0; i<3; i++)
       for (int s=0; s<3; s++)
           for (int k=0; k<3; k++)
               for (int l=0; l<3; l++){
            	   dainvda(i,s,k,l) = 0.;
                   for (int m=0; m<3; m++)
                       for (int j=0; j<3; j++)
                    	   dainvda(i,s,k,l) -= ainv(i,m)*I4(m,j,k,l)*ainv(j,s);
     }

   dx1da = E1;
   dx2da = E2;
   dx3da = E3;

   double D1(0.),D2(0.),D3(0.);
   static STensor3 dD1da, dD2da, dD3da;
   STensorOperation::subMember_getEigenDecomposition_getDi(x1,tr,det,dx1da,dI3da,D1,dD1da);
   STensorOperation::subMember_getEigenDecomposition_getDi(x2,tr,det,dx2da,dI3da,D2,dD2da);
   STensorOperation::subMember_getEigenDecomposition_getDi(x3,tr,det,dx3da,dI3da,D3,dD3da);
   STensorOperation::subMember_getEigenDecomposition_getdEidC(E1,x1,det,D1,ainv,dx1da,dainvda,dI3da,dD1da,dE1da);
   STensorOperation::subMember_getEigenDecomposition_getdEidC(E2,x2,det,D2,ainv,dx2da,dainvda,dI3da,dD2da,dE2da);
   STensorOperation::subMember_getEigenDecomposition_getdEidC(E3,x3,det,D3,ainv,dx3da,dainvda,dI3da,dD3da,dE3da);

}

// NEW
void STensorOperation::alignEigenDecomposition_NormBased_withDerivative(const STensor3& A, const STensor3& B, STensor3& Bs, STensor43& dBsdA){
	// B is transformed into the config of A to become Bs.

    double trA = A.trace();
    double detA = STensorOperation::determinantSTensor3(A);
    double detB = STensorOperation::determinantSTensor3(B);
    if(fabs(detA)<=1.e-15 || fabs(detB)<=1.e-15 ){
      Bs = B;
      STensorOperation::zero(dBsdA);
      return;
    }

    static fullVector<double> eigenValReal_a(3), eigenValReal_b(3);
    eigenValReal_a.setAll(0); eigenValReal_b.setAll(0);
    static fullMatrix<double> leftEigenVect_a(3,3), leftEigenVect_b(3,3);
    leftEigenVect_a.setAll(0); leftEigenVect_b.setAll(0);
    static fullMatrix<double> rightEigenVect_a(3,3), rightEigenVect_b(3,3), rightEigenVect_b_align(3,3);
    rightEigenVect_a.setAll(0); rightEigenVect_b.setAll(0); rightEigenVect_b_align.setAll(0);

    int align;
    double a1, a2, a3, b1, b2, b3, b1_align, b2_align, b3_align;
    static STensor3 A1, A2, A3, B1, B2, B3, da1dA, da2dA, da3dA;
    STensorOperation::zero(A1); STensorOperation::zero(A2); STensorOperation::zero(A3); STensorOperation::zero(B1);
    STensorOperation::zero(B2); STensorOperation::zero(B3); STensorOperation::zero(da1dA); STensorOperation::zero(da2dA); STensorOperation::zero(da3dA);
    static STensor43 dA1dA, dA2dA, dA3dA;
    STensorOperation::zero(dA1dA); STensorOperation::zero(dA2dA); STensorOperation::zero(dA3dA);
    STensorOperation::getEigenDecomposition(A, a1, a2, a3, A1, A2, A3, eigenValReal_a, leftEigenVect_a, rightEigenVect_a);
    STensorOperation::getEigenDecomposition(B, b1, b2, b3, B1, B2, B3, eigenValReal_b, leftEigenVect_b, rightEigenVect_b);

    double tol = 1.e-5;
    /*
    if (fabs(fabs(a1)-fabs(a2)) <= tol || fabs(fabs(a1)-fabs(a3)) <= tol || fabs(fabs(a2)-fabs(a3)) <= tol ){
      Bs = B;
      STensorOperation::zero(dBsdA);
      return;
    }*/

    STensorOperation::alignEigenDecomposition_NormBased(A,B,A1,A2,A3, eigenValReal_a, eigenValReal_b, rightEigenVect_a, rightEigenVect_b,
									b1_align,b2_align,b3_align,rightEigenVect_b_align,align);

    if(fabs(detA)>1.e-15){
      static STensor3 Ainv, dI3dA;
      STensorOperation::inverseSTensor3(A,Ainv);
      dI3dA = Ainv;
      dI3dA *= detA;

      const STensor43 I4(1.,1.);
      static STensor43 dAinvdA;
      for (int i=0; i<3; i++)
        for (int s=0; s<3; s++)
          for (int k=0; k<3; k++)
            for (int l=0; l<3; l++){
              dAinvdA(i,s,k,l) = 0.;
              for (int m=0; m<3; m++)
                for (int j=0; j<3; j++)
                  dAinvdA(i,s,k,l) -= Ainv(i,m)*I4(m,j,k,l)*Ainv(j,s);
            }

            da1dA = A1;
            da2dA = A2;
            da3dA = A3;

      double D1(0.),D2(0.),D3(0.);
	    static STensor3 dD1dA, dD2dA, dD3dA;
	    STensorOperation::subMember_getEigenDecomposition_getDi(a1,trA,detA,da1dA,dI3dA,D1,dD1dA);
	    STensorOperation::subMember_getEigenDecomposition_getDi(a2,trA,detA,da2dA,dI3dA,D2,dD2dA);
	    STensorOperation::subMember_getEigenDecomposition_getDi(a3,trA,detA,da3dA,dI3dA,D3,dD3dA);
	    STensorOperation::subMember_getEigenDecomposition_getdEidC(A1,a1,detA,D1,Ainv,da1dA,dAinvdA,dI3dA,dD1dA,dA1dA);
	    STensorOperation::subMember_getEigenDecomposition_getdEidC(A2,a2,detA,D2,Ainv,da2dA,dAinvdA,dI3dA,dD2dA,dA2dA);
	    STensorOperation::subMember_getEigenDecomposition_getdEidC(A3,a3,detA,D3,Ainv,da3dA,dAinvdA,dI3dA,dD3dA,dA3dA);
    }
    else{
	    STensorOperation::zero(da1dA); STensorOperation::zero(da2dA); STensorOperation::zero(da3dA); STensorOperation::zero(dA1dA); STensorOperation::zero(dA2dA); STensorOperation::zero(dA3dA);
    }

    static STensor43 dAdA;
    STensorOperation::zero(dBsdA); STensorOperation::zero(dAdA);
    STensorOperation::zero(Bs);

    if (fabs(fabs(b1)-fabs(b2)) <= tol || fabs(fabs(b1)-fabs(b3)) <= tol || fabs(fabs(b2)-fabs(b3)) <= tol ){
      Bs = B;
    	STensorOperation::zero(dBsdA);
    }
    else{
    if(align == 1){
      for(int i=0; i<3; i++)
        for(int j=0; j<3; j++){
          Bs(i,j) += b1_align*A1(i,j) + b2_align*A2(i,j) + b3_align*A3(i,j);
          for(int k=0; k<3; k++)
            for(int l=0; l<3; l++){
              dBsdA(i,j,k,l) += b1_align*dA1dA(i,j,k,l) + b2_align*dA2dA(i,j,k,l) + b3_align*dA3dA(i,j,k,l);
              dAdA(i,j,k,l) += da1dA(i,j)*A1(k,l) + da2dA(i,j)*A2(k,l) + da3dA(i,j)*A3(k,l) + a1*dA1dA(i,j,k,l) + a2*dA2dA(i,j,k,l) + a3*dA3dA(i,j,k,l);  // Should be equal to _I4
           }
        }
    }
    else{
    	Bs = B;
    	STensorOperation::zero(dBsdA);
    }
  }
    const STensor43 _I4(1.,1.);
    static STensor43 check;
    check = dAdA;
    check -= _I4;
}

void STensorOperation::alignEigenDecomposition_NormBased_withDerivative_Vector(const STensor3& A, const std::vector<STensor3>& Bi, std::vector<STensor3>& Bi_s, std::vector<STensor43>& dBi_sdA){
	// Bi is transformed into the config of A to become Bi_s.

    static fullVector<double> eigenValReal_a(3), eigenValReal_b(3);
    eigenValReal_a.setAll(0); eigenValReal_b.setAll(0);
    static fullMatrix<double> leftEigenVect_a(3,3), leftEigenVect_b(3,3);
    leftEigenVect_a.setAll(0); leftEigenVect_b.setAll(0);
    static fullMatrix<double> rightEigenVect_a(3,3), rightEigenVect_b(3,3), rightEigenVect_b_align(3,3);
    rightEigenVect_a.setAll(0); rightEigenVect_b.setAll(0); rightEigenVect_b_align.setAll(0);

    int align;
    double a1, a2, a3, b1, b2, b3, b1_align, b2_align, b3_align;
    static STensor3 A1, A2, A3, B1, B2, B3, da1dA, da2dA, da3dA;
    STensorOperation::zero(A1); STensorOperation::zero(A2); STensorOperation::zero(A3); STensorOperation::zero(B1);
    STensorOperation::zero(B2); STensorOperation::zero(B3); STensorOperation::zero(da1dA); STensorOperation::zero(da2dA); STensorOperation::zero(da3dA);
    static STensor43 dA1dA, dA2dA, dA3dA;
    STensorOperation::zero(dA1dA); STensorOperation::zero(dA2dA); STensorOperation::zero(dA3dA);

    STensorOperation::getEigenDecomposition(A, a1, a2, a3, A1, A2, A3, eigenValReal_a, leftEigenVect_a, rightEigenVect_a);

    double tol = 1.e-5;
    /*if (fabs(fabs(a1)-fabs(a2)) <= tol || fabs(fabs(a1)-fabs(a3)) <= tol || fabs(fabs(a2)-fabs(a3)) <= tol ){
      for (int ite=0; ite<Bi.size(); ite++){
        Bi_s[ite] = Bi[ite];
        STensorOperation::zero(dBi_sdA[ite]);
      }
      return;
    }*/

	double trA = A.trace();
	double detA = STensorOperation::determinantSTensor3(A);

	if(fabs(detA)>1.e-15){
		static STensor3 Ainv, dI3dA;
		STensorOperation::inverseSTensor3(A,Ainv);
		dI3dA = Ainv;
		dI3dA *= detA;

		const STensor43 I4(1.,1.);
		static STensor43 dAinvdA;
		for (int i=0; i<3; i++)
			for (int s=0; s<3; s++)
				for (int k=0; k<3; k++)
					for (int l=0; l<3; l++){
						dAinvdA(i,s,k,l) = 0.;
						for (int m=0; m<3; m++)
							for (int j=0; j<3; j++)
								dAinvdA(i,s,k,l) -= Ainv(i,m)*I4(m,j,k,l)*Ainv(j,s);
		     }

		da1dA = A1;
		da2dA = A2;
		da3dA = A3;

		double D1(0.),D2(0.),D3(0.);
	    static STensor3 dD1dA, dD2dA, dD3dA;
	    STensorOperation::subMember_getEigenDecomposition_getDi(a1,trA,detA,da1dA,dI3dA,D1,dD1dA);
	    STensorOperation::subMember_getEigenDecomposition_getDi(a2,trA,detA,da2dA,dI3dA,D2,dD2dA);
	    STensorOperation::subMember_getEigenDecomposition_getDi(a3,trA,detA,da3dA,dI3dA,D3,dD3dA);
	    STensorOperation::subMember_getEigenDecomposition_getdEidC(A1,a1,detA,D1,Ainv,da1dA,dAinvdA,dI3dA,dD1dA,dA1dA);
	    STensorOperation::subMember_getEigenDecomposition_getdEidC(A2,a2,detA,D2,Ainv,da2dA,dAinvdA,dI3dA,dD2dA,dA2dA);
	    STensorOperation::subMember_getEigenDecomposition_getdEidC(A3,a3,detA,D3,Ainv,da3dA,dAinvdA,dI3dA,dD3dA,dA3dA);
	}
	else{
		STensorOperation::zero(da1dA); STensorOperation::zero(da2dA); STensorOperation::zero(da3dA); STensorOperation::zero(dA1dA); STensorOperation::zero(dA2dA); STensorOperation::zero(dA3dA);
	}

	for (int ite=0; ite<Bi.size(); ite++){

		double detBi = STensorOperation::determinantSTensor3(Bi[ite]);

	    if(fabs(detBi)<=1.e-15){
	      Bi_s[ite] = Bi[ite];
	      STensorOperation::zero(dBi_sdA[ite]);
	    }
	    else{
	    	STensorOperation::getEigenDecomposition(Bi[ite], b1, b2, b3, B1, B2, B3, eigenValReal_b, leftEigenVect_b, rightEigenVect_b);

        if (fabs(fabs(b1)-fabs(b2)) <= tol || fabs(fabs(b1)-fabs(b3)) <= tol || fabs(fabs(b2)-fabs(b3)) <= tol ){
            Bi_s[ite] = Bi[ite];
            STensorOperation::zero(dBi_sdA[ite]);
        }
        else{
        rightEigenVect_b_align.setAll(0.);
	    	STensorOperation::alignEigenDecomposition_NormBased(A, Bi[ite], A1,A2,A3, eigenValReal_a, eigenValReal_b, rightEigenVect_a, rightEigenVect_b,
	    			b1_align,b2_align,b3_align,rightEigenVect_b_align,align);

	    	STensorOperation::zero(dBi_sdA[ite]);
	    	STensorOperation::zero(Bi_s[ite]);
	    	if(align == 1){
	    		for(int i=0; i<3; i++)
	    			for(int j=0; j<3; j++){
	    				Bi_s[ite](i,j) += b1_align*A1(i,j) + b2_align*A2(i,j) + b3_align*A3(i,j);
	    				for(int k=0; k<3; k++)
	    					for(int l=0; l<3; l++)
	    						dBi_sdA[ite](i,j,k,l) += b1_align*dA1dA(i,j,k,l) + b2_align*dA2dA(i,j,k,l) + b3_align*dA3dA(i,j,k,l);
	    			}
	    	} // align
	    	else{
	    		Bi_s[ite] = Bi[ite];
	    	}
	    }
    }
	} // ite
}

void STensorOperation::alignEigenDecomposition_NormBased(const STensor3& A, const STensor3& B, const STensor3& A1, const STensor3& A2, const STensor3& A3,
								const fullVector<double> &eigenValReal1, const fullVector<double> &eigenValReal2,
		  	  	  	  	  	  	const fullMatrix<double>& rightEigenVect1, const fullMatrix<double>& rightEigenVect2,
								double& b1_align, double& b2_align, double& b3_align, fullMatrix<double>& rightEigenVect2_align, int& align){
  	  	// AAlign B to A where A is fixed
		align = 1;

	  	  // Get dotproducts of Eigen Vectors -> 3C1*3C1 possibilities
	  	  static STensor3 dot, theta;
	  	  STensorOperation::zero(dot);
		  for (int k=0; k<3; k++)
			for (int l=0; l<3; l++)
			  for(int i=0; i<3; i++){
				 dot(k,l) += rightEigenVect2(k,i)*rightEigenVect1(l,i);
			  }

		  // Get angles
		  double pi = 3.14159265358979323846;
		  STensorOperation::zero(theta);
		  for (int k=0; k<3; k++)
			for (int l=0; l<3; l++){
			  theta(k,l) = acos(dot(k,l)); // /(abs1(k)*abs2(l)));
			  if (theta(k,l) > pi/2) {
				  theta(k,l) = pi - theta(k,l);
			  }
			}

	  	  // Form the tensor
	  	  double norm(0.), norm_min(1.e+10), norm_theta(0.), norm_min_theta(1.e+10), norm_dot(0.), norm_max_dot(1.e-8);
	  	  double tol(1.e-5);
	  	  static STensor3 B_reformed;

	  	  // Generate permutated eigVal vector
	  	  static fullVector<double> eigenValReal2_permuted(3);
	  	  static fullVector<int> eigenValReal2_permuted_Index(3);
	  	  eigenValReal2_permuted.setAll(0.); eigenValReal2_permuted_Index.setAll(0);
	  	  double l1(0.), l2(0.), l3(0.);
	  	  for (int p=0; p<3; p++){
	  		l1 = eigenValReal2(p);
	  		for (int q=0; q<3; q++){
	  		  if(q!=p){
	  			l2 = eigenValReal2(q);
	  			for (int r=0; r<3; r++){
	  			  if(r!=p && r!=q){
	  				l3 = eigenValReal2(r);

	  				STensorOperation::zero(B_reformed);
	  				for (int k=0; k<3; k++)
	  				  for (int l=0; l<3; l++)
	  		  		    B_reformed(k,l) += l1*A1(k,l) + l2*A2(k,l) + l3*A3(k,l);
	  		  	        B_reformed -= B;
	  		  	        STensorOperation::doubleContractionSTensor3(B_reformed,B_reformed,norm);
	  		  	        norm = pow(norm,0.5);

	  		  	        norm_theta = theta(p,0) + theta(q,1) + theta(r,2);
	  		  	        norm_dot = fabs(dot(p,0)) + fabs(dot(q,1)) + fabs(dot(r,2));

	  		  	    // if (fabs(fabs(l1)-fabs(l2)) > tol || fabs(fabs(l1)-fabs(l3)) > tol || fabs(fabs(l2)-fabs(l3)) > tol ){
	  		  	       if(norm < norm_min){      // if( ( - norm + norm_min)>1.e-6 )
	  		  	    		norm_min = norm;
	  		  	    		norm_min_theta = norm_theta;
	  		  	    		eigenValReal2_permuted(0) = l1;
	  		  	    		eigenValReal2_permuted_Index(0) = p;
	  	    		  	    eigenValReal2_permuted(1) = l2;
	  	    		  	    eigenValReal2_permuted_Index(1) = q;
	  	    		  	    eigenValReal2_permuted(2) = l3;
	  	    		  	    eigenValReal2_permuted_Index(2) = r;
	  		  	      }
	  		  	    //}
	  		  	    /*else{
	  		  	    	if(norm_dot>norm_max_dot){
	  		  	    		norm_max_dot = norm_dot;
	  		  	    		eigenValReal2_permuted(0) = l1;
	  		  	    		eigenValReal2_permuted_Index(0) = p;
	  	    		  	    eigenValReal2_permuted(1) = l2;
	  	    		  	    eigenValReal2_permuted_Index(1) = q;
	  	    		  	    eigenValReal2_permuted(2) = l3;
	  	    		  	    eigenValReal2_permuted_Index(2) = r;
	  		  	    	}
	  		  	    }*/


	  		  	    }

	  			  }
	  			}

	  		  }
	  		}

	  	  if(align==0){
	  		    eigenValReal2_permuted(0) = eigenValReal2(0);
	  		  	eigenValReal2_permuted_Index(0) = 0;
	  		  	eigenValReal2_permuted(1) = eigenValReal2(1);
	  		  	eigenValReal2_permuted_Index(1) = 1;
	  		  	eigenValReal2_permuted(2) = eigenValReal2(2);
	  		  	eigenValReal2_permuted_Index(2) = 2;
	  	  }

	  	  // AAlign the eigVals and eigVecs
	  	  b1_align = eigenValReal2_permuted(0);
	  	  b2_align = eigenValReal2_permuted(1);
	  	  b3_align = eigenValReal2_permuted(2);
		  for(int i=0; i<3; i++){
			  rightEigenVect2_align(i,0) = rightEigenVect2(i,eigenValReal2_permuted_Index(0));
			  rightEigenVect2_align(i,1) = rightEigenVect2(i,eigenValReal2_permuted_Index(1));
			  rightEigenVect2_align(i,2) = rightEigenVect2(i,eigenValReal2_permuted_Index(2));
		  }


	      // Convert to tensors
	//	  static STensor3 eig1, eig2, eig_align;
	//	  for (int k=0; k<3; k++)
	//		for (int l=0; l<3; l++){
	//			eig1(k,l) = rightEigenVect1(k,l);
	//			eig2(k,l) = rightEigenVect2(k,l);
	//			eig_align(k,l) = rightEigenVect2_align(k,l);
	//		}

  }

void STensorOperation::alignEigenDecomposition_NormBased(const STensor3& A, const STensor3& B, const fullVector<double> &eigenValReal1, const fullVector<double> &eigenValReal2,
		  	  	  	  	  	  	  	  const fullMatrix<double>& rightEigenVect1, const fullMatrix<double>& rightEigenVect2,
									  double& a1_align, double& a2_align, double& a3_align, fullMatrix<double>& rightEigenVect, int& align){
  	  // Align 1 to 2 where 2 is fixed
		align = 1;

	  	  // Get eigen bases
	  	  double b1, b2, b3;
	  	  static STensor3 B1, B2, B3;
	  	  STensorOperation::getEigenDecomposition(B,b1,b2,b3,B1,B2,B3);

	  	  // Get dotproducts of Eigen Vectors -> 3C1*3C1 possibilities
	  	  static STensor3 dot1, theta1;
	  	  STensorOperation::zero(dot1);
		  for (int k=0; k<3; k++)
			for (int l=0; l<3; l++)
			  for(int i=0; i<3; i++){
				 dot1(k,l) += rightEigenVect1(k,i)*rightEigenVect2(l,i);
			  }

		  // Get angles
		  double pi = 3.14159265358979323846;
		  STensorOperation::zero(theta1);
		  for (int k=0; k<3; k++)
			for (int l=0; l<3; l++){
			  theta1(k,l) = acos(dot1(k,l)); // /(abs1(k)*abs2(l)));
			  if (theta1(k,l) > pi/2) {
				  theta1(k,l) = pi - theta1(k,l);
			  }
			}

	  	  // Form the tensor
	  	  double norm(0.), norm_min(1.e+10), norm_theta(0.), norm_min_theta(1.e+10);
	  	  double tol(1.e-6);
	  	  static STensor3 A_reformed;

	  	  // Generate permutated eigVal vector
	  	  static fullVector<double> eigenValReal1_permuted(3);
	  	  static fullVector<int> eigenValReal1_permuted_Index(3);
	  	  double l1(0.), l2(0.), l3(0.);
	  	  for (int p=0; p<3; p++){
	  		l1 = eigenValReal1(p);
	  		for (int q=0; q<3; q++){
	  		  if(q!=p){
	  			l2 = eigenValReal1(q);
	  			for (int r=0; r<3; r++){
	  			  if(r!=p && r!=q){
	  				l3 = eigenValReal1(r);

	  				STensorOperation::zero(A_reformed);
	  				for (int k=0; k<3; k++)
	  				  for (int l=0; l<3; l++)
	  		  		    A_reformed(k,l) += l1*B1(k,l) + l2*B2(k,l) + l3*B3(k,l);
	  		  	    A_reformed -= A;
	  		  	    STensorOperation::doubleContractionSTensor3(A_reformed,A_reformed,norm);
	  		  	    norm = pow(norm,0.5);

	  		  	    norm_theta = theta1(p,0) + theta1(q,1) + theta1(r,2);

	  		  	 // if( fabs(fabs(l1)-fabs(l2)) < tol || fabs(fabs(l1)-fabs(l3)) < tol || fabs(fabs(l2)-fabs(l3)) < tol ){
	  		  	      if(norm < norm_min){      // if( ( - norm + norm_min)>1.e-6 )
	  		  	    	 // if(norm_theta < norm_min_theta){
	  		  	    		norm_min = norm;
	  		  	    		norm_min_theta = norm_theta;
	  			  		  	eigenValReal1_permuted(0) = l1;
	  			  		  	eigenValReal1_permuted_Index(0) = p;
	  			  		  	eigenValReal1_permuted(1) = l2;
	  			  		  	eigenValReal1_permuted_Index(1) = q;
	  			  		  	eigenValReal1_permuted(2) = l3;
	  			  		  	eigenValReal1_permuted_Index(2) = r;
	  			  		  	// align = 0;
	  		  	    	 // }
	  		  	      }
	  		  	   //}
	  		  	    /* else if (fabs(fabs(l1)-fabs(l2)) > tol || fabs(fabs(l1)-fabs(l3)) > tol || fabs(fabs(l2)-fabs(l3)) > tol ){
		  		  	      if(norm < norm_min){
		  		  	    		norm_min = norm;
		  			  		  	eigenValReal1_permuted(0) = l1;
		  			  		  	eigenValReal1_permuted_Index(0) = p;
		  			  		  	eigenValReal1_permuted(1) = l2;
		  			  		  	eigenValReal1_permuted_Index(1) = q;
		  			  		  	eigenValReal1_permuted(2) = l3;
		  			  		  	eigenValReal1_permuted_Index(2) = r;
		  		  	    	 }
		  		  	   }  // if (fabs(.....
	  		  	   */
	  		  	    /*
		  		  	  if( fabs(norm - norm_min) > 1.e-5){
		  		  	      if(norm < norm_min){      // if( ( - norm + norm_min)>1.e-6 )
		  		  	    		norm_min = norm;
		  		  	    		norm_min_theta = norm_theta;
		  			  		  	eigenValReal1_permuted(0) = l1;
		  			  		  	eigenValReal1_permuted_Index(0) = p;
		  			  		  	eigenValReal1_permuted(1) = l2;
		  			  		  	eigenValReal1_permuted_Index(1) = q;
		  			  		  	eigenValReal1_permuted(2) = l3;
		  			  		  	eigenValReal1_permuted_Index(2) = r;
		  			  		  	// align = 0;
		  		  	      }
		  		  	  }
		  		  	 else{
			  		  	   if(norm < norm_min){
			  		  		   if(norm_theta < norm_min_theta){
			  		  	    		norm_min = norm;
			  			  		  	eigenValReal1_permuted(0) = l1;
			  			  		  	eigenValReal1_permuted_Index(0) = p;
			  			  		  	eigenValReal1_permuted(1) = l2;
			  			  		  	eigenValReal1_permuted_Index(1) = q;
			  			  		  	eigenValReal1_permuted(2) = l3;
			  			  		  	eigenValReal1_permuted_Index(2) = r;
			  		  	    	 }
	  		  	    	 }
			  		 }*/ // if (fabs(.....

	  		  	    }

	  			  }
	  			}

	  		  }
	  		}

	  	  if(align==0){
	  		    eigenValReal1_permuted(0) = eigenValReal1(0);
	  		  	eigenValReal1_permuted_Index(0) = 0;
	  		  	eigenValReal1_permuted(1) = eigenValReal1(1);
	  		  	eigenValReal1_permuted_Index(1) = 1;
	  		  	eigenValReal1_permuted(2) = eigenValReal1(2);
	  		  	eigenValReal1_permuted_Index(2) = 2;
	  	  }

	  	  // Align the eigVals and eigVecs
	  	  a1_align = eigenValReal1_permuted(0);
	  	  a2_align = eigenValReal1_permuted(1);
	  	  a3_align = eigenValReal1_permuted(2);
		  for(int i=0; i<3; i++){
			  rightEigenVect(i,0) = rightEigenVect1(i,eigenValReal1_permuted_Index(0));
			  rightEigenVect(i,1) = rightEigenVect1(i,eigenValReal1_permuted_Index(1));
			  rightEigenVect(i,2) = rightEigenVect1(i,eigenValReal1_permuted_Index(2));
		  }


	      // Convert to tensors
		  static STensor3 eig1, eig2, eig_align;
		  for (int k=0; k<3; k++)
			for (int l=0; l<3; l++){
				eig1(k,l) = rightEigenVect1(k,l);
				eig2(k,l) = rightEigenVect2(k,l);
				eig_align(k,l) = rightEigenVect(k,l);
			}

  }

void STensorOperation::getBasisRotationTensor(const STensor3& A, const STensor3& B, STensor3& R, STensor43& dRdB, STensor43& dRtdB){

	// Only for single-legged symmetric tensors A and B -> leftEigenVect = rightEigenVect and eigenvals are always real
	// R rotates the bases of A to that of B, so R = R_(ba), i.e., R = sum (eb * ea)
	// B = R*A*Rt

  static fullMatrix<double> m1(3, 3), m2(3, 3);
  static fullVector<double> eigenValReal1(3), eigenValReal2(3), AlignedEigenValReal1(3);
  static fullVector<double> eigenValImag1(3), eigenValImag2(3);
  static fullMatrix<double> leftEigenVect1(3,3), leftEigenVect2(3,3);
  static fullMatrix<double> rightEigenVect1(3,3), rightEigenVect2(3,3), AlignedRightEigenVect1(3,3);
  m1.setAll(0.); m2.setAll(0.);
  eigenValReal1.setAll(0.); eigenValReal2.setAll(0.); AlignedEigenValReal1.setAll(0.);
  eigenValImag1.setAll(0.); eigenValImag2.setAll(0.);
  leftEigenVect1.setAll(0.); leftEigenVect2.setAll(0.);
  rightEigenVect1.setAll(0.); rightEigenVect2.setAll(0.); AlignedRightEigenVect1.setAll(0.);

  // Get eigen values and vectors
  A.getMat(m1); B.getMat(m2);
  m1.eig(eigenValReal1,eigenValImag1,leftEigenVect1,rightEigenVect1,false);
  m2.eig(eigenValReal2,eigenValImag2,leftEigenVect2,rightEigenVect2,false);

  // Make sure rightEigenVect1 is aligned with rightEigenVect2
  // STensorOperation::alignEigenDecomposition_EigenVectorDotProductBased(eigenValReal1,eigenValReal2,rightEigenVect1,rightEigenVect2,AlignedEigenValReal1,AlignedRightEigenVect1);
  double a1_align, a2_align, a3_align;

  int align=0;
  STensorOperation::alignEigenDecomposition_NormBased(A,B,eigenValReal1,eigenValReal2,rightEigenVect1,rightEigenVect2,  a1_align, a2_align, a3_align, AlignedRightEigenVect1, align);
  // eigenValReal1 = AlignedEigenValReal1;
  rightEigenVect1 = AlignedRightEigenVect1;

  STensorOperation::zero(R);
  for(int i=0; i<3; i++)
    for(int j=0; j<3; j++)
      R(i,j) += rightEigenVect2(i,0)*rightEigenVect1(j,0) + rightEigenVect2(i,1)*rightEigenVect1(j,1) + rightEigenVect2(i,2)*rightEigenVect1(j,2);

  static STensor33 de0dB, de1dB, de2dB;
  STensorOperation::getEigenDecomposition_getdeidC(rightEigenVect2,eigenValReal2(0),eigenValReal2(1),eigenValReal2(2),de0dB,de1dB,de2dB);

  STensorOperation::zero(dRdB); STensorOperation::zero(dRtdB);
	for(int i=0; i<3; i++)
	  for(int j=0; j<3; j++)
		for (int k=0; k<3; k++)
		  for (int l=0; l<3; l++){
			dRdB(i,j,k,l) +=  de0dB(i,k,l)*rightEigenVect1(j,0) + de1dB(i,k,l)*rightEigenVect1(j,1) + de2dB(i,k,l)*rightEigenVect1(j,2);
			dRtdB(i,j,k,l) += de0dB(j,k,l)*rightEigenVect1(i,0) + de1dB(j,k,l)*rightEigenVect1(i,1) + de2dB(j,k,l)*rightEigenVect1(i,2);
	      }

	/*
	// checkers
	static STensor3 B1,B2,B3;
	double b1,b2,b3;
	static STensor43 dBdB; // must be I4(1.,1.)
	STensorOperation::zero(dBdB);
	STensorOperation::getEigenDecomposition(B,b1,b2,b3,B1,B2,B3);
	for(int i=0; i<3; i++)
	  for(int j=0; j<3; j++)
		for (int k=0; k<3; k++)
		  for (int l=0; l<3; l++){
			dBdB(i,j,k,l) += B1(i,j)*B1(k,l) + B2(i,j)*B2(k,l) + B3(i,j)*B3(k,l) +
      						b1*(de0dB(i,k,l)*rightEigenVect2(j,0) + de0dB(j,k,l)*rightEigenVect2(i,0)) +
								b2*(de1dB(i,k,l)*rightEigenVect2(j,1) + de1dB(j,k,l)*rightEigenVect2(i,1)) +
								b3*(de2dB(i,k,l)*rightEigenVect2(j,2) + de2dB(j,k,l)*rightEigenVect2(i,2));
		  }*/
}

void STensorOperation::alignEigenDecomposition_NormBased_indexOnly(const STensor3& A, const STensor3& B, fullVector<int>& alignedIndex){
  	  // Align A to B, where B is fixed

	  	  // Initialise 1
		  alignedIndex(0) = 0; alignedIndex(1) = 1; alignedIndex(2) = 2;
		  static fullVector<double> eigenValReal1(3); eigenValReal1.setAll(0.);
		  double a1,a2,a3,b1,b2,b3; // eigenvalues
		  static STensor3 A1,A2,A3,B1,B2,B3; // bases

		  // Get eigen and assign
		  STensorOperation::getEigenDecomposition(A,a1,a2,a3,A1,A2,A3);
		  STensorOperation::getEigenDecomposition(B,b1,b2,b3,B1,B2,B3);
		  eigenValReal1(0) = a1; eigenValReal1(1) = a2; eigenValReal1(2) = a3;

		  // Initialise 2
	  	  double norm(0.), norm_min(1.e+20);
	  	  static STensor3 A_reformed;

	  	  // Generate permutated eigVal vector
	  	  static fullVector<double> eigenValReal1_permuted(3);
	  	  static fullVector<int> eigenValReal1_permuted_Index(3);
	  	  for (int p=0; p<3; p++)
	  		for (int q=0; q<3; q++){
	  		  if(q!=p){
	  			for (int r=0; r<3; r++){
	  			  if(r!=p && r!=q){

	  				zero(A_reformed);
	  				for (int k=0; k<3; k++)
	  				  for (int l=0; l<3; l++)
	  		  		    A_reformed(k,l) += eigenValReal1(p)*B1(k,l) + eigenValReal1(q)*B2(k,l) + eigenValReal1(r)*B3(k,l);
	  		  	    A_reformed -= A;
	  		  	    doubleContractionSTensor3(A_reformed,A_reformed,norm);
	  		  	    norm = pow(norm,0.5);

	  		  	    if(norm < norm_min){      // if( ( - norm + norm_min)>1.e-6 )
	  		  	    		norm_min = norm;
	  			  		  	eigenValReal1_permuted(0) = eigenValReal1(p);
	  			  		  	eigenValReal1_permuted_Index(0) = p;
	  			  		  	eigenValReal1_permuted(1) = eigenValReal1(q);
	  			  		  	eigenValReal1_permuted_Index(1) = q;
	  			  		  	eigenValReal1_permuted(2) = eigenValReal1(r);
	  			  		  	eigenValReal1_permuted_Index(2) = r;
	  		  	    }
	  			  }
	  			}

	  		  }
	  		}

	  	  // Assign the aligned indices
	  	  alignedIndex = eigenValReal1_permuted_Index;
  }

void STensorOperation::VRDecomposition(const STensor3& a, STensor3& V, STensor3&R)
{
    static STensor3 biot,invV;
    static fullMatrix<double> m(3, 3);
    static fullVector<double> eigenValReal(3);
    static fullVector<double> eigenValImag(3);
    static fullMatrix<double> leftEigenVect(3,3);
    static fullMatrix<double> rightEigenVect(3,3);

    STensorOperation::multSTensor3SecondTranspose(a,a,biot);
    // Get eigen values and vectors
    biot.getMat(m);
    m.eig(eigenValReal,eigenValImag,leftEigenVect,rightEigenVect,0);

    // Spectral sum for exp function
    for (int i=0; i<3; i++){
      for (int j=0; j<3; j++){
        V(i,j) =  sqrt(eigenValReal(0)) *leftEigenVect(i,0)*rightEigenVect(j,0)
                    +sqrt(eigenValReal(1)) *leftEigenVect(i,1)*rightEigenVect(j,1)
                    +sqrt(eigenValReal(2)) *leftEigenVect(i,2)*rightEigenVect(j,2);
        invV(i,j) =  1./sqrt(eigenValReal(0)) *leftEigenVect(i,0)*rightEigenVect(j,0)
                    +1./sqrt(eigenValReal(1)) *leftEigenVect(i,1)*rightEigenVect(j,1)
                    +1./sqrt(eigenValReal(2)) *leftEigenVect(i,2)*rightEigenVect(j,2);
      }
    }
    STensorOperation::multSTensor3(invV, a, R);
    //check
    //static STensor3 abis;
    //abis=V;
    //abis*=R;
    //abis-=a;
    //double norm=abis.norm2();
    //Msg::Info("norm of VRDecomposition: %f",norm);
}


void STensorOperation::RUDecomposition(const STensor3& a, STensor3& U, STensor3&R)
{
    static STensor3 C,invU;
    static fullMatrix<double> m(3, 3);
    static fullVector<double> eigenValReal(3);
    static fullVector<double> eigenValImag(3);
    static fullMatrix<double> leftEigenVect(3,3);
    static fullMatrix<double> rightEigenVect(3,3);

    STensorOperation::multSTensor3FirstTranspose(a,a,C);
    // Get eigen values and vectors
    C.getMat(m);
    m.eig(eigenValReal,eigenValImag,leftEigenVect,rightEigenVect,0);

    // Spectral sum for exp function
    for (int i=0; i<3; i++){
      for (int j=0; j<3; j++){
        U(i,j) =  sqrt(eigenValReal(0)) *leftEigenVect(i,0)*rightEigenVect(j,0)
                    +sqrt(eigenValReal(1)) *leftEigenVect(i,1)*rightEigenVect(j,1)
                    +sqrt(eigenValReal(2)) *leftEigenVect(i,2)*rightEigenVect(j,2);
        invU(i,j) =  1./sqrt(eigenValReal(0)) *leftEigenVect(i,0)*rightEigenVect(j,0)
                    +1./sqrt(eigenValReal(1)) *leftEigenVect(i,1)*rightEigenVect(j,1)
                    +1./sqrt(eigenValReal(2)) *leftEigenVect(i,2)*rightEigenVect(j,2);
      }
    }
    STensorOperation::multSTensor3(a,invU, R);
    //check
    //static STensor3 abis;
    //abis=R;
    //abis*=U;
    //abis-=a;
    //double norm=abis.norm2();
    //Msg::Info("norm of RUDecomposition: %f ",norm);

}

/* TBD
void STensorOperation::rotateTensorBasesMinimumEnergy(const STensor3& A, const STensor3& B, const STensor3& stress, STensor3& C){

	double a1,a2,a3,b1,b2,b3;
	static STensor3 A1,A2,A3,B1,B2,B3;
	STensorOperation::getEigenDecompositionExplicitBase(A,a1,a2,a3,A1,A2,A3);
	STensorOperation::getEigenDecompositionExplicitBase(B,b1,b2,b3,B1,B2,B3);

	double initialEnergy = 0.;
	STensorOperation::doubleContractionSTensor3(stress,A,initialEnergy);

	static fullVector<double> A_eigVal(3);
	A(0)=a1; A(1)=a2; A(2)=a3;

}
*/
