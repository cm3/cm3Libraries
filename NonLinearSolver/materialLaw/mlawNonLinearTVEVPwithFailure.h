//
// C++ Interface: Material Law
//
// Description: Non-Linear Thermo-Visco-Mechanics with Failure/damage-local/non-local (Thermo-ViscoElasto-ViscoPlasto Law) with Non-Local Damage Interface (finally here!)
//
// Author: <Ujwal Kishore J - FLE_Knight>, (C) 2024
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef MLAWNONLINEARTVETVPWITHFAILURE_H_
#define MLAWNONLINEARTVETVPWITHFAILURE_H_

#include "ipNonLinearTVP.h"
#include "ipNonLocalDamageHyperelastic.h"
#include "ipNonLocalDamageNonLinearTVEVP.h"
#include "mlawNonLinearTVENonLinearTVP.h"
#include "CLengthLaw.h"
#include "DamageLaw.h"

class mlawNonLinearTVEVPwithFailure : public mlawNonLinearTVENonLinearTVP{
  protected:
    #ifndef SWIG
    mlawHyperelasticFailureCriterionBase* _failureCriterion;
  protected:

    virtual void predictorCorrector(const STensor3& F0, const STensor3& F1, STensor3& P, const IPNonLinearTVP* q0, IPNonLinearTVP* q1,
    				STensor43& Tangent, STensor43& dFpdF, STensor3& dFpdT, STensor43& dFedF, STensor3& dFedT,
                                const double& T0, const double& T, const SVector3& gradT0, const SVector3& gradT, SVector3& fluxT,
                                STensor3& dPdT, STensor3& dfluxTdgradT, SVector3& dfluxTdT, STensor33& dfluxTdF,
                                double& thermalSource, double& dthermalSourcedT, STensor3& dthermalSourcedF,
                                double& mechanicalSource, double& dmechanicalSourcedT, STensor3& dmechanicalSourcedF,
                            	const bool stiff, STensor43* elasticTangent) const;

    #endif // SWIG
  public:
    mlawNonLinearTVEVPwithFailure(const int num, const double E, const double nu, const double rho,
                                const double Tinitial, const double Alpha, const double KThCon, const double Cp,
                                const double tol = 1.e-6, const bool matrixbyPerturbation = false, const double pert = 1e-8,
                                const double stressIteratorTol = 1.e-9, const bool thermalEstimationPreviousConfig = true);

    void setFailureCriterion(const mlawHyperelasticFailureCriterionBase& fCr);

  #ifndef SWIG
    mlawNonLinearTVEVPwithFailure(const mlawNonLinearTVEVPwithFailure& src);
    virtual ~mlawNonLinearTVEVPwithFailure();

    virtual materialLaw* clone() const{return new mlawNonLinearTVEVPwithFailure(*this);};
    virtual void checkInternalState(IPVariable* ipv, const IPVariable* ipvprev) const;
    virtual matname getType() const{return materialLaw::TVEVPwithFailure;}
    virtual void initLaws(const std::map<int,materialLaw*> &maplaw){}; // this law is initialized so nothing to do

  #endif // SWIG

};

class mlawNonLocalDamagaNonLinearTVEVP : public mlawNonLinearTVENonLinearTVP{
  protected:
    CLengthLaw *cLLaw;
    DamageLaw  *damLaw;

  public:
    mlawNonLocalDamagaNonLinearTVEVP(const int num, const double E, const double nu, const double rho,
                                const double Tinitial, const double Alpha, const double KThCon, const double Cp,
                                const double tol = 1.e-6, const bool matrixbyPerturbation = false, const double pert = 1e-8,
                                const double stressIteratorTol = 1.e-9, const bool thermalEstimationPreviousConfig = true);
    void setCLengthLaw(const CLengthLaw &_cLLaw);
    void setDamageLaw(const DamageLaw &_damLaw);

   #ifndef SWIG
    mlawNonLocalDamagaNonLinearTVEVP(const mlawNonLocalDamagaNonLinearTVEVP &source);
    virtual ~mlawNonLocalDamagaNonLinearTVEVP();

    virtual materialLaw* clone() const { return new mlawNonLocalDamagaNonLinearTVEVP(*this);}

    // function of materialLaw
    virtual matname getType() const{return materialLaw::nonLocalDamageTVEVP;}
    virtual void createIPState(IPNonLinearTVEVPNonLocalDamage *ivi, IPNonLinearTVEVPNonLocalDamage *iv1, IPNonLinearTVEVPNonLocalDamage *iv2) const;
    virtual void createIPState(IPStateBase* &ips,bool hasBodyForce,const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const IntPt *GP=NULL, const int gpt = 0) const;
    virtual void createIPVariable(IPNonLinearTVEVPNonLocalDamage *&ipv,bool hasBodyForce, const MElement *ele,const int nbFF,const IntPt *GP, const int gpt) const;

    virtual void initLaws(const std::map<int,materialLaw*> &maplaw){}; // this law is initialized so nothing to do
    virtual double soundSpeed() const; // default but you can redefine it for your case
    virtual const CLengthLaw *getCLengthLaw() const {return cLLaw; };
    virtual const DamageLaw *getDamageLaw() const {return damLaw; };
    // specific function

   public:
    virtual void constitutive(const STensor3& F0, 				// initial deformation gradient (input @ time n)
                              const STensor3& F1, 				// updated deformation gradient (input @ time n+1)
                              STensor3& P,        				// updated 1st Piola-Kirchhoff stress tensor (output)
                              const IPNonLinearTVEVPNonLocalDamage *q0,    	// array of initial internal variable
                              IPNonLinearTVEVPNonLocalDamage *q1,		// updated array of internal variable (in ipvcur on output),
                              STensor43 &Tangent,				// constitutive tangents (output)
                              const bool stiff, 				 // if true compute the tangents
                              const bool dTangentdeps=false,
                              STensor63* dCalgdeps = NULL) const;

    virtual void constitutive(const STensor3& F0, const STensor3& F1, STensor3& P,
                              const IPNonLinearTVEVPNonLocalDamage* q0, IPNonLinearTVEVPNonLocalDamage* q1,

                              STensor43& Tangent, STensor3 &dLocalPlasticStrainDStrain,
                              STensor3 &dStressDNonLocalPlasticStrain,
                              double &dLocalPlasticStrainDNonLocalPlasticStrain,

                              STensor3& dPdT, STensor33& dPdgradT,
                              double& dLocalPlasticStrainDT,

                              const double& T0, const double& T,
                              const SVector3& gradT0, const SVector3& gradT,

                              SVector3& fluxT,
                              STensor33& dfluxTdF, SVector3& dfluxTdNonLocalPlasticStrain,
                              SVector3& dfluxTdT, STensor3& dfluxTdgradT,

                              double& thermalSource,
                              STensor3& dthermalSourcedF, double& dthermalSourcedNonLocalPlasticStrain,
                              double& dthermalSourcedT, SVector3& dthermalSourcedgradT,

                              double& mechanicalSource,
                              STensor3& dmechanicalSourcedF, double& dmechanicalSourcedNonLocalPlasticStrain,
                              double& dmechanicalSourcedT, SVector3& dmechanicalSourcedgradT,

                              const bool stiff,
                              STensor43* elasticTangent = NULL,
                              const bool dTangentdeps=false,
                              STensor63* dCalgdeps = NULL) const;



#endif // SWIG
};


class mlawLocalDamageNonLinearTVEVPWithFailure : public mlawNonLinearTVEVPwithFailure{
  protected:
    std::vector<DamageLaw*> damLaw;

  public:
    mlawLocalDamageNonLinearTVEVPWithFailure(const int num, const double E, const double nu, const double rho,
                                const double Tinitial, const double Alpha, const double KThCon, const double Cp,
                                const double tol = 1.e-6, const bool matrixbyPerturbation = false, const double pert = 1e-8,
                                const double stressIteratorTol = 1.e-9, const bool thermalEstimationPreviousConfig = true);

    void clearAllDamageLaw();
    void setDamageLaw(const DamageLaw &_damLaw);

   #ifndef SWIG
    mlawLocalDamageNonLinearTVEVPWithFailure(const mlawLocalDamageNonLinearTVEVPWithFailure &source);
    virtual ~mlawLocalDamageNonLinearTVEVPWithFailure();

    virtual materialLaw* clone() const { return new mlawLocalDamageNonLinearTVEVPWithFailure(*this);}

    // function of materialLaw
    virtual matname getType() const{return materialLaw::localDamageTVEVPWithFailure;}
    virtual void createIPState(IPNonLinearTVEVPMultipleLocalDamage *ivi, IPNonLinearTVEVPMultipleLocalDamage *iv1, IPNonLinearTVEVPMultipleLocalDamage *iv2) const;
    virtual void createIPState(IPStateBase* &ips,bool hasBodyForce,const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const IntPt *GP=NULL, const int gpt = 0) const;
    virtual void createIPVariable(IPNonLinearTVEVPMultipleLocalDamage *&ipv,bool hasBodyForce, const MElement *ele,const int nbFF,const IntPt *GP, const int gpt) const;

    virtual void initLaws(const std::map<int,materialLaw*> &maplaw){}; // this law is initialized so nothing to do
    virtual double soundSpeed() const; // default but you can redefine it for your case
    virtual const std::vector<DamageLaw*>& getDamageLaw() const {return damLaw; };
    // specific function
   public:

    virtual void constitutive(const STensor3& F0,         // initial deformation gradient (input @ time n)
                              const STensor3& F1,         // updated deformation gradient (input @ time n+1)
                              STensor3& P,                // updated 1st Piola-Kirchhoff stress tensor (output)
                              const IPNonLinearTVEVPMultipleLocalDamage *q0,       // array of initial internal variable
                              IPNonLinearTVEVPMultipleLocalDamage *q1,             // updated array of internal variable (in ipvcur on output),
                              STensor43 &Tangent,         // constitutive tangents (output)
                              const bool stiff,            // if true compute the tangents
                              const bool dTangentdeps=false,
                              STensor63* dCalgdeps = NULL) const;

    virtual void constitutive(const STensor3& F0, const STensor3& F1, STensor3& P,
                              const IPNonLinearTVEVPMultipleLocalDamage* q0, IPNonLinearTVEVPMultipleLocalDamage* q1,

                              STensor43& Tangent,
                              STensor3& dPdT, STensor33& dPdgradT,

                              const double& T0, const double& T,
                              const SVector3& gradT0, const SVector3& gradT,

                              SVector3& fluxT,
                              STensor33& dfluxTdF,
                              SVector3& dfluxTdT, STensor3& dfluxTdgradT,

                              double& thermalSource,
                              STensor3& dthermalSourcedF,
                              double& dthermalSourcedT, SVector3& dthermalSourcedgradT,

                              double& mechanicalSource,
                              STensor3& dmechanicalSourcedF,
                              double& dmechanicalSourcedT, SVector3& dmechanicalSourcedgradT,

                              const bool stiff,
                              STensor43* elasticTangent = NULL,
                              const bool dTangentdeps=false,
                              STensor63* dCalgdeps = NULL) const;
  #endif // SWIG
};


class mlawNonLocalDamageNonLinearTVEVPWithFailure : public mlawNonLinearTVEVPwithFailure{
  protected:
    std::vector<CLengthLaw*> cLLaw;
    std::vector<DamageLaw*> damLaw;
    double _criticalDamage; // hardening is saturated after damage attained this value
    bool _blockHardeningAfterFailure;
    bool _blockDamageAfterFailure;

  public:
    mlawNonLocalDamageNonLinearTVEVPWithFailure(const int num, const double E, const double nu, const double rho,
                                const double Tinitial, const double Alpha, const double KThCon, const double Cp,
                                const double tol = 1.e-6, const bool matrixbyPerturbation = false, const double pert = 1e-8,
                                const double stressIteratorTol = 1.e-9, const bool thermalEstimationPreviousConfig = true);

    void clearAllCLengthLaw();
    void clearAllDamageLaw();
    void setCLengthLaw(const CLengthLaw &_cLLaw);
    void setDamageLaw(const DamageLaw &_damLaw);
    void setCritialDamage(const double cr);


   #ifndef SWIG
    mlawNonLocalDamageNonLinearTVEVPWithFailure(const mlawNonLocalDamageNonLinearTVEVPWithFailure &source);
    virtual ~mlawNonLocalDamageNonLinearTVEVPWithFailure();

    virtual materialLaw* clone() const { return new mlawNonLocalDamageNonLinearTVEVPWithFailure(*this);}

    // function of materialLaw
    virtual matname getType() const{return materialLaw::nonLocalDamageTVEVPWithFailure;}
    virtual void createIPState(IPNonLinearTVEVPMultipleNonLocalDamage *ivi, IPNonLinearTVEVPMultipleNonLocalDamage *iv1, IPNonLinearTVEVPMultipleNonLocalDamage *iv2) const;
    virtual void createIPState(IPStateBase* &ips, bool hasBodyForce, const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const IntPt *GP=NULL, const int gpt = 0) const;
    virtual void createIPVariable(IPNonLinearTVEVPMultipleNonLocalDamage *&ipv,bool hasBodyForce, const MElement *ele,const int nbFF,const IntPt *GP, const int gpt) const;

    virtual void initLaws(const std::map<int,materialLaw*> &maplaw){}; // this law is initialized so nothing to do
    virtual double soundSpeed() const; // default but you can redefine it for your case
    virtual const std::vector<CLengthLaw*>& getCLengthLaw() const {return cLLaw; };
    virtual const std::vector<DamageLaw*>& getDamageLaw() const {return damLaw; };
    // specific function
  public:
    virtual void checkInternalState(IPVariable* ipv, const IPVariable* ipvprev) const;

    virtual void predictorCorrector(const STensor3& F0, const STensor3& Fn, STensor3& P,
                              const IPNonLinearTVEVPMultipleNonLocalDamage* q0, IPNonLinearTVEVPMultipleNonLocalDamage* q1,

                              STensor43& Tangent,
                              std::vector<STensor3>& dLocalVariableDStrain,
                              std::vector<STensor3>& dStressDNonLocalVariable,
                              fullMatrix<double>& dLocalVariableDNonLocalVariable,

                              STensor3& dPdT, STensor33& dPdgradT,
                              fullMatrix<double>& dLocalVariableDT,

                              const double& T0, const double& T,
                              const SVector3& gradT0, const SVector3& gradT,

                              SVector3& fluxT,
                              STensor33& dfluxTdF,
                              std::vector<SVector3>& dfluxTdNonLocalVariable,
                              SVector3& dfluxTdT, STensor3& dfluxTdgradT,

                              double& thermalSource,
                              STensor3& dthermalSourcedF,
                              fullMatrix<double>& dthermalSourcedNonLocalVariable,
                              double& dthermalSourcedT, SVector3& dthermalSourcedgradT,

                              double& mechanicalSource,
                              STensor3& dmechanicalSourcedF,
                              fullMatrix<double>& dmechanicalSourcedNonLocalVariable,
                              double& dmechanicalSourcedT, SVector3& dmechanicalSourcedgradT,

                              const bool stiff,
                              STensor43* elasticTangent = NULL) const;

    virtual void constitutive(const STensor3& F0,         // initial deformation gradient (input @ time n)
                              const STensor3& Fn,         // updated deformation gradient (input @ time n+1)
                              STensor3 &P,                // updated 1st Piola-Kirchhoff stress tensor (output)
                              const IPNonLinearTVEVPMultipleNonLocalDamage *q0,       // array of initial internal variable
                              IPNonLinearTVEVPMultipleNonLocalDamage *q1,             // updated array of internal variable (in ipvcur on output),
                              STensor43 &Tangent,         // constitutive tangents (output)
                              const bool stiff,           // if true compute the tangents
                              const bool dTangentdeps=false,
                              STensor63* dCalgdeps = NULL
                             ) const;

    virtual void constitutive(const STensor3& F0, const STensor3& Fn, STensor3& P,
                              const IPNonLinearTVEVPMultipleNonLocalDamage* q0, IPNonLinearTVEVPMultipleNonLocalDamage* q1,

                              STensor43& Tangent,
                              std::vector<STensor3>& dLocalVariableDStrain,
                              std::vector<STensor3>& dStressDNonLocalVariable,
                              fullMatrix<double>& dLocalVariableDNonLocalVariable,

                              STensor3& dPdT, STensor33& dPdgradT,
                              fullMatrix<double>& dLocalVariableDT,

                              const double& T0, const double& T,
                              const SVector3& gradT0, const SVector3& gradT,

                              SVector3& fluxT,
                              STensor33& dfluxTdF,
                              std::vector<SVector3>& dfluxTdNonLocalVariable,
                              SVector3& dfluxTdT, STensor3& dfluxTdgradT,

                              double& thermalSource,
                              STensor3& dthermalSourcedF,
                              fullMatrix<double>& dthermalSourcedNonLocalVariable,
                              double& dthermalSourcedT, SVector3& dthermalSourcedgradT,

                              double& mechanicalSource,
                              STensor3& dmechanicalSourcedF,
                              fullMatrix<double>& dmechanicalSourcedNonLocalVariable,
                              double& dmechanicalSourcedT, SVector3& dmechanicalSourcedgradT,

                              const bool stiff,            // if true compute the tangents
                              STensor43* elasticTangent,
                              const bool dTangentdeps=false,
                              STensor63* dCalgdeps = NULL ) const;

  #endif // SWIG
};

#endif // MLAWNONLINEARTVETVPWITHFAILURE_H_
