//
// C++ Interface: Material Law
//
// Description: IPNonLinearTVE (IP Thermo-ViscoElasto-ViscoPlasto Law) with Non-Local Damage Interface (soon.....)
//
// Author: <Ujwal Kishore J - FLE_Knight>, (C) 2022
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef IPNONLINEARTVP_H_
#define IPNONLINEARTVP_H_
#include "ipNonLinearTVE.h"
#include "ipNonLinearTVM.h"
#include "j2IsotropicHardening.h"
#include "kinematicHardening.h"
#include "STensor3.h"
#include "STensor43.h"
#include "mullinsEffect.h"

class IPNonLinearTVP : public IPNonLinearTVM{

    public:
        double _DgammaDT; // DequivalentPlasticStrainDT
        double _DGammaDT;
        double _DirreversibleEnergyDT;
        double _mechSrcTVP;
        double _DmechSrcTVPdT;

        double _IsoHardForce_simple;
        double _dIsoHardForcedT_simple;
        std::vector<double> _IsoHardForce;
        std::vector<double> _dIsoHardForcedT;
        std::vector<double> _ddIsoHardForcedTT;

        double _psiTVM; // free energy - full TVM
        double _DpsiTVMdT;
        double _DDpsiTVMdTT;

        STensor3 _DGammaDF;
        STensor3 _DModMandelDT;
        STensor3 _DbackSigDT;
        STensor3 _DphiDT;
        STensor3 _DmechSrcTVPdF;
        STensor3 _dIsoHardForcedF_simple;
        std::vector<STensor3> _dIsoHardForcedF;

        STensor43 _DbackSigDF;
        STensor43 _DModMandelDF;
        STensor43 _DphiDF;
        STensor3 _DphiPDF; // need this for mechSrc

        STensor3 _GammaN;
        STensor3 _dGammaNdT;
        STensor43 _dGammaNdF;

        // failure
	double _dgFdT;

        // remove later
        double _detEe;
    protected:
        double _dPlasticEnergyPartdT;
        double _dElasticEnergyPartdT, _dViscousEnergyPartdT;

    public:
        IPNonLinearTVP(const J2IsotropicHardening* comp,
                    const J2IsotropicHardening* trac,
                    const kinematicHardening* kin, const int N, const mullinsEffect* mullins);
        IPNonLinearTVP(const IPNonLinearTVP& src);
        virtual IPNonLinearTVP& operator =(const IPVariable &source);
        virtual ~IPNonLinearTVP();

        virtual STensor3& getRefToModifiedMandel() {return _ModMandel;};
        virtual const STensor3& getConstRefToModifiedMandel() const {return _ModMandel;};
        virtual STensor3& getRefToDModMandelDT() {return _DModMandelDT;};
        virtual const STensor3& getConstRefToDModMandelDT() const {return _DModMandelDT;};
        virtual STensor43& getRefToDModMandelDF() {return _DModMandelDF;};
        virtual const STensor43& getConstRefToDModMandelDF() const {return _DModMandelDF;};

        /*virtual double &getRefToIsotropicHardeningForce() {return _IsoHardForce;}
        virtual const double &getConstRefToIsotropicHardeningForce() const {return _IsoHardForce;}*/

        virtual double &getRefToFreeEnergyTVM() {return _psiTVM;}
        virtual const double &getConstRefToFreeEnergyTVM() const {return _psiTVM;}

        virtual double &getRefToDDFreeEnergyTVMdTT() {return _DDpsiTVMdTT;}
        virtual const double &getConstRefToDDFreeEnergyTVMdTT() const {return _DDpsiTVMdTT;}

        virtual double &getRefToMechSrcTVP() {return _mechSrcTVP;}
        virtual const double &getConstRefToMechSrcTVP() const {return _mechSrcTVP;}
        virtual double &getRefTodMechSrcTVPdT() {return _DmechSrcTVPdT;}
        virtual const double &getConstRefTodMechSrcTVPdT() const {return _DmechSrcTVPdT;}
        virtual STensor3 &getRefTodMechSrcTVPdF() {return _DmechSrcTVPdF;}
        virtual const STensor3 &getConstRefTodMechSrcTVPdF() const {return _DmechSrcTVPdF;}

        virtual STensor3& getRefToDbackStressdT() {return _DbackSigDT;}
        virtual const STensor3& getConstRefToDbackStressdT() const {return _DbackSigDT;}
        virtual STensor43& getRefToDbackStressdF() {return _DbackSigDF;};
        virtual const STensor43& getConstRefToDbackStressdF() const {return _DbackSigDF;};

        virtual STensor3& getRefToDphiPDF() {return _DphiPDF;}
        virtual const STensor3& getConstRefToDphiPDF() const {return _DphiPDF;}

        virtual STensor3& getRefToGammaN() {return _GammaN;}
        virtual const STensor3& getConstRefToGammaN() const {return _GammaN;}

        virtual double &getRefToDElasticEnergyPartdT() {return _dElasticEnergyPartdT;}
        virtual const double &getConstRefToDElasticEnergyPartdT() const {return _dElasticEnergyPartdT;}
        virtual double &getRefToDViscousEnergyPartdT() {return _dViscousEnergyPartdT;}
        virtual const double &getConstRefToDViscousEnergyPartdT() const {return _dViscousEnergyPartdT;}

        virtual double &getRefToDPlasticEnergyPartdT() {return _dPlasticEnergyPartdT;}
        virtual const double &getConstRefToDPlasticEnergyPartdT() const {return _dPlasticEnergyPartdT;}

        virtual double& getRefToDIrreversibleEnergyDT() {return _DirreversibleEnergyDT;};
        virtual const double& getConstRefToDIrreversibleEnergyDT() const{return _DirreversibleEnergyDT;};

    	virtual const double& getConstRefToDFailurePlasticityDT() const {return _dgFdT;};
    	virtual double& getRefToDFailurePlasticityDT() {return _dgFdT;};

        virtual IPVariable* clone() const{return new IPNonLinearTVP(*this);};
        virtual void restart();

};

#endif // IPNonLinearTVP.h
