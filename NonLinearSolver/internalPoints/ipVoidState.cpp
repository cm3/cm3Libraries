//
// C++ Interface: Void State
//
// Description: Storing class for void state behind porosity
//
//
// Author:  <V.-D. Nguyen>, (C) 2019
//
// Copyright: See COPYING file that comes with this distribution
//
//

#include "ipVoidState.h"
#include "restartManager.h"
#include <math.h>

IPVoidState::IPVoidState(const double fV0, const double lambda0, const double W0, const double gamma0):
  _Chi(pow(3.*gamma0*fV0*lambda0/W0,1./3.)),_DChiDyieldfV(0.),_DChiDhatD(0.),_DChiDhatP(0.),_DChiDhatQ(0.),
  _W(W0),_DWDyieldfV(0.),_DWDhatD(0.),_DWDhatP(0.),_DWDhatQ(0.),
  _gamma(gamma0),_DgammaDyieldfV(0.),_DgammaDhatD(0.),_DgammaDhatP(0.),_DgammaDhatQ(0.),
  _lambda(lambda0),_DlambdaDyieldfV(0.),_DlambdaDhatD(0.),_DlambdaDhatP(0.),_DlambdaDhatQ(0.),
  _isSaturated(false)
{
  _roughChi = _Chi;
}

IPVoidState::IPVoidState(const IPVoidState& src):
  _roughChi(src._roughChi),
  _Chi(src._Chi),
  _DChiDyieldfV(src._DChiDyieldfV),
  _DChiDhatD(src._DChiDhatD),
  _DChiDhatP(src._DChiDhatP),
  _DChiDhatQ(src._DChiDhatQ),
  _W(src._W),
  _DWDyieldfV(src._DWDyieldfV),
  _DWDhatD(src._DWDhatD),
  _DWDhatP(src._DWDhatP),
  _DWDhatQ(src._DWDhatQ),
  _gamma(src._gamma),
  _DgammaDyieldfV(src._DgammaDyieldfV),
  _DgammaDhatD(src._DgammaDhatD),
  _DgammaDhatP(src._DgammaDhatP),
  _DgammaDhatQ(src._DgammaDhatQ),
  _lambda(src._lambda),
  _DlambdaDyieldfV(src._DlambdaDyieldfV),
  _DlambdaDhatD(src._DlambdaDhatD),
  _DlambdaDhatP(src._DlambdaDhatP),
  _DlambdaDhatQ(src._DlambdaDhatQ),
  _isSaturated(src._isSaturated)
{

}

IPVoidState& IPVoidState::operator=(const IPVoidState& src)
{
  _roughChi = src._roughChi;
  _Chi = src._Chi;
  _DChiDyieldfV = src._DChiDyieldfV;
  _DChiDhatD = src._DChiDhatD;
  _DChiDhatP = src._DChiDhatP;
  _DChiDhatQ = src._DChiDhatQ;

  _W = src._W;
  _DWDyieldfV = src._DWDyieldfV;
  _DWDhatD = src._DWDhatD;
  _DWDhatP = src._DWDhatP;
  _DWDhatQ = src._DWDhatQ;

  _gamma = src._gamma;
  _DgammaDyieldfV = src._DgammaDyieldfV;
  _DgammaDhatD = src._DgammaDhatD;
  _DgammaDhatP = src._DgammaDhatP;
  _DgammaDhatQ = src._DgammaDhatQ;

  _lambda = src._lambda;
  _DlambdaDyieldfV = src._DlambdaDyieldfV;
  _DlambdaDhatD = src._DlambdaDhatD;
  _DlambdaDhatP = src._DlambdaDhatP;
  _DlambdaDhatQ = src._DlambdaDhatQ;

  _isSaturated = src._isSaturated;
  return *this;
}

void IPVoidState::copyAndZeroDerivativesFrom(const IPVoidState& src)
{
  _roughChi = src._roughChi;
  _Chi = src._Chi;
  _DChiDyieldfV = 0.;
  _DChiDhatD = 0.;
  _DChiDhatP = 0.;
  _DChiDhatQ = 0.;

  _W = src._W;
  _DWDyieldfV = 0.;
  _DWDhatD = 0.;
  _DWDhatP = 0.;
  _DWDhatQ =0.;

  _gamma = src._gamma;
  _DgammaDyieldfV = 0.;
  _DgammaDhatD = 0.;
  _DgammaDhatP = 0.;
  _DgammaDhatQ = 0.;

  _lambda = src._lambda;
  _DlambdaDyieldfV = 0.;
  _DlambdaDhatD = 0.;
  _DlambdaDhatP = 0.;
  _DlambdaDhatQ = 0.;
  
  _isSaturated = src._isSaturated;
}

void IPVoidState::restart()
{
  restartManager::restart(_roughChi);
  restartManager::restart(_Chi);
  restartManager::restart(_DChiDyieldfV);
  restartManager::restart(_DChiDhatD);
  restartManager::restart(_DChiDhatQ);
  restartManager::restart(_DChiDhatP);

  restartManager::restart(_W);
  restartManager::restart(_DWDyieldfV);
  restartManager::restart(_DWDhatD);
  restartManager::restart(_DWDhatQ);
  restartManager::restart(_DWDhatP);

  restartManager::restart(_gamma);
  restartManager::restart(_DgammaDyieldfV);
  restartManager::restart(_DgammaDhatD);
  restartManager::restart(_DgammaDhatQ);
  restartManager::restart(_DgammaDhatP);

  restartManager::restart(_lambda);
  restartManager::restart(_DlambdaDyieldfV);
  restartManager::restart(_DlambdaDhatD);
  restartManager::restart(_DlambdaDhatQ);
  restartManager::restart(_DlambdaDhatP);

  restartManager::restart(_isSaturated);
}

