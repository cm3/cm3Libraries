//
// Description: 
// Author:  <V.-D. Nguyen>, (C) 2021
//
// Copyright: See COPYING file that comes with this distribution
//
//

#include "ipTransverseIsoUserDirection.h"

IPTransverseIsoUserDirection::IPTransverseIsoUserDirection() : IPTransverseIsotropic() {Msg::Error("Cannot construct default IPTransverseIsoUserDirection");}

IPTransverseIsoUserDirection::IPTransverseIsoUserDirection(const IPTransverseIsoUserDirection &source) : IPTransverseIsotropic(source), _A(source._A){}
IPTransverseIsoUserDirection& IPTransverseIsoUserDirection::operator=(const IPVariable &source)
{
  IPTransverseIsotropic::operator=(source);
  const IPTransverseIsoUserDirection* src = dynamic_cast<const IPTransverseIsoUserDirection*>(&source);
	if (src != NULL)
  {
		_A = src->_A;
	}
	return *this;
}

void IPTransverseIsoUserDirection::restart()
{
  IPTransverseIsotropic::restart();
  restartManager::restart(_A);
  return;
}

IPTransverseIsoUserDirection::IPTransverseIsoUserDirection(const SVector3 &dir) : 
                          IPTransverseIsotropic(), _A(dir) {

}