//
// C++ Interface: ipvariable
//
// Description: IPNonLocalDamageHyperelasic
//
// Author:  V.D. Nguyen, (C) 2014
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "ipNonLocalDamageHyperelastic.h"
#include "restartManager.h"

IPHyperViscoElastoPlasticLocalDamage::IPHyperViscoElastoPlasticLocalDamage(const J2IsotropicHardening* comp,
                    const J2IsotropicHardening* trac, const kinematicHardening* kin, const int N,
                    const DamageLaw *daml): IPHyperViscoElastoPlastic(comp,trac,kin,N),
                     _damageEnergy(0.)
{
  ipvDam=NULL;
  if(daml ==NULL) Msg::Error("IPHyperViscoElastoPlasticLocalDamage::IPHyperViscoElastoPlasticLocalDamage has no daml");
  daml->createIPVariable(ipvDam);

};

IPHyperViscoElastoPlasticLocalDamage::~IPHyperViscoElastoPlasticLocalDamage(){
  if(ipvDam !=NULL)
  {
    delete ipvDam;
  }
};


IPHyperViscoElastoPlasticLocalDamage::IPHyperViscoElastoPlasticLocalDamage(const IPHyperViscoElastoPlasticLocalDamage &source):
      IPHyperViscoElastoPlastic(source),_damageEnergy(source._damageEnergy){

  ipvDam = NULL;
  if(source.ipvDam != NULL)
  {
    ipvDam = dynamic_cast<IPDamage*>(source.ipvDam->clone());
  }
};
IPHyperViscoElastoPlasticLocalDamage& IPHyperViscoElastoPlasticLocalDamage::operator=(const IPVariable &source){
  IPHyperViscoElastoPlastic::operator=(source);
  const IPHyperViscoElastoPlasticLocalDamage* src = dynamic_cast<const IPHyperViscoElastoPlasticLocalDamage*>(&source);
  if(src != NULL)
  {
    _damageEnergy = src->_damageEnergy;

    if(src->ipvDam != NULL)
    {
			if (ipvDam!= NULL)
				ipvDam->operator=(*dynamic_cast<const IPVariable*>(src->ipvDam));
			else
				ipvDam=dynamic_cast<IPDamage*>(src->ipvDam->clone());
    }
		else{
			if(ipvDam != NULL) delete ipvDam; ipvDam = NULL;
		}
  }
  return *this;
  };

void IPHyperViscoElastoPlasticLocalDamage::restart()
{
  IPHyperViscoElastoPlastic::restart();
  restartManager::restart(ipvDam);
  restartManager::restart(_damageEnergy);
  return;
}


IPHyperViscoElastoPlasticNonLocalDamage::IPHyperViscoElastoPlasticNonLocalDamage(const J2IsotropicHardening* comp,
                    const J2IsotropicHardening* trac, const kinematicHardening* kin, const int N,
                    const CLengthLaw *cll, const DamageLaw *daml): IPHyperViscoElastoPlasticLocalDamage(comp,trac,kin,N,daml),
                    _nonlocalPlasticStrain(0.), _DirreversibleEnergyDNonLocalVariable(0.)
{
  ipvCL=NULL;
  if(cll ==NULL) Msg::Error("IPHyperViscoElastoPlasticNonLocalDamage::IPHyperViscoElastoPlasticNonLocalDamage has no cll");
  cll->createIPVariable(ipvCL);
};

IPHyperViscoElastoPlasticNonLocalDamage::~IPHyperViscoElastoPlasticNonLocalDamage(){
  if(ipvCL != NULL)
  {
    delete ipvCL;
  }
};

IPHyperViscoElastoPlasticNonLocalDamage::IPHyperViscoElastoPlasticNonLocalDamage(const IPHyperViscoElastoPlasticNonLocalDamage &source):
      IPHyperViscoElastoPlasticLocalDamage(source), _nonlocalPlasticStrain(source._nonlocalPlasticStrain),
      _DirreversibleEnergyDNonLocalVariable(source._DirreversibleEnergyDNonLocalVariable){
  ipvCL = NULL;
  if(source.ipvCL != NULL)
  {
    ipvCL = dynamic_cast<IPCLength*>(source.ipvCL->clone());
  }
};
IPHyperViscoElastoPlasticNonLocalDamage& IPHyperViscoElastoPlasticNonLocalDamage::operator=(const IPVariable &source){
  IPHyperViscoElastoPlasticLocalDamage::operator=(source);
  const IPHyperViscoElastoPlasticNonLocalDamage* src = dynamic_cast<const IPHyperViscoElastoPlasticNonLocalDamage*>(&source);
  if(src != NULL)
  {
    _nonlocalPlasticStrain=src->_nonlocalPlasticStrain;
    _DirreversibleEnergyDNonLocalVariable = src->_DirreversibleEnergyDNonLocalVariable;
    if(src->ipvCL != NULL)
    {
			if (ipvCL != NULL){
				ipvCL->operator=(*dynamic_cast<const IPCLength*>(src->ipvCL));
			}
			else
				ipvCL= dynamic_cast<IPCLength*>(src->ipvCL->clone());
    }
		else{
			if(ipvCL != NULL) delete ipvCL; ipvCL = NULL;
		}

  }
  return *this;
};

void IPHyperViscoElastoPlasticNonLocalDamage::restart()
{
  IPHyperViscoElastoPlasticLocalDamage::restart();
  restartManager::restart(ipvCL);
  restartManager::restart(_nonlocalPlasticStrain);
  restartManager::restart(_DirreversibleEnergyDNonLocalVariable);
};

IPHyperViscoElastoPlasticMultipleLocalDamage::IPHyperViscoElastoPlasticMultipleLocalDamage(const J2IsotropicHardening* comp,
                const J2IsotropicHardening* trac,const kinematicHardening* kin,
                const int N, const std::vector<DamageLaw*>& dl): IPHyperViscoElastoPlastic(comp,trac,kin,N),
                _damageEnergy(0.){
  numNonLocalVariable = dl.size();
  ipvDam.clear();
  for (int i=0; i< dl.size(); i++){
    IPDamage* ipvD = NULL;
    dl[i]->createIPVariable(ipvD);
    ipvDam.push_back(ipvD);
  }
};
IPHyperViscoElastoPlasticMultipleLocalDamage::~IPHyperViscoElastoPlasticMultipleLocalDamage(){
  for (int i=0; i< numNonLocalVariable; i++){
    delete ipvDam[i];
  }
  ipvDam.clear();
};
IPHyperViscoElastoPlasticMultipleLocalDamage::IPHyperViscoElastoPlasticMultipleLocalDamage(const IPHyperViscoElastoPlasticMultipleLocalDamage& source):
  IPHyperViscoElastoPlastic(source), _damageEnergy(source._damageEnergy){
  numNonLocalVariable = source.numNonLocalVariable;

  ipvDam.clear();
  for (int i=0; i< numNonLocalVariable; i++){
    if(source.ipvDam[i] != NULL){
      ipvDam.push_back(dynamic_cast<IPDamage*>(source.ipvDam[i]->clone()));
    }
  }
};
IPHyperViscoElastoPlasticMultipleLocalDamage& IPHyperViscoElastoPlasticMultipleLocalDamage::operator=(const IPVariable& source){
  IPHyperViscoElastoPlastic::operator=(source);
  const IPHyperViscoElastoPlasticMultipleLocalDamage* ipsource = dynamic_cast<const IPHyperViscoElastoPlasticMultipleLocalDamage*>(&source);
  if (ipsource != NULL){
    numNonLocalVariable = ipsource->numNonLocalVariable;
    _damageEnergy = ipsource->_damageEnergy;
    for (int i=0; i< ipsource->numNonLocalVariable; i++){
      if(ipsource->ipvDam[i] != NULL){
        if (ipvDam[i] != NULL){
          ipvDam[i]->operator=(*dynamic_cast<const IPVariable*>(ipsource->ipvDam[i]));
        }
      }
    }
  }
  return *this;
};


void IPHyperViscoElastoPlasticMultipleLocalDamage::restart(){
  IPHyperViscoElastoPlastic::restart();
  restartManager::restart(numNonLocalVariable);
  restartManager::restart(_damageEnergy);
  for (int i=0; i< numNonLocalVariable; i++){
    ipvDam[i]->restart();
  }
};



IPHyperViscoElastoPlasticMultipleNonLocalDamage::IPHyperViscoElastoPlasticMultipleNonLocalDamage(const J2IsotropicHardening* comp,
                const J2IsotropicHardening* trac, const kinematicHardening* kin,
                const int N, const std::vector<CLengthLaw*>& cll, const std::vector<DamageLaw*>& dl):
                IPHyperViscoElastoPlasticMultipleLocalDamage(comp,trac,kin,N,dl),
                _nonlocalEqPlasticStrain(0.),_nonlocalFailurePlasticity(0.){
  ipvCL.clear();
  _DirreversibleEnergyDNonLocalVariable.clear();
  for (int i=0; i< cll.size(); i++){
    IPCLength* ipvLength=NULL;
    cll[i]->createIPVariable(ipvLength);
    ipvCL.push_back(ipvLength);
    _DirreversibleEnergyDNonLocalVariable.push_back(0.);
  }
};
IPHyperViscoElastoPlasticMultipleNonLocalDamage::~IPHyperViscoElastoPlasticMultipleNonLocalDamage(){
  for (int i=0; i< numNonLocalVariable; i++){
    delete ipvCL[i];
  }
  ipvCL.clear();
};
IPHyperViscoElastoPlasticMultipleNonLocalDamage::IPHyperViscoElastoPlasticMultipleNonLocalDamage(const IPHyperViscoElastoPlasticMultipleNonLocalDamage& source):
  IPHyperViscoElastoPlasticMultipleLocalDamage(source){
  _nonlocalEqPlasticStrain= source._nonlocalEqPlasticStrain;
  _nonlocalFailurePlasticity = source._nonlocalFailurePlasticity;
  _DirreversibleEnergyDNonLocalVariable = source._DirreversibleEnergyDNonLocalVariable;
  ipvCL.clear();
  for (int i=0; i< numNonLocalVariable; i++){
    if(source.ipvCL[i] != NULL){
      ipvCL.push_back(dynamic_cast<IPCLength*>(source.ipvCL[i]->clone()));
    }
  }
};
IPHyperViscoElastoPlasticMultipleNonLocalDamage& IPHyperViscoElastoPlasticMultipleNonLocalDamage::operator=(const IPVariable& source){
  IPHyperViscoElastoPlasticMultipleLocalDamage::operator=(source);
  const IPHyperViscoElastoPlasticMultipleNonLocalDamage* ipsource = dynamic_cast<const IPHyperViscoElastoPlasticMultipleNonLocalDamage*>(&source);
  if (ipsource != NULL){
    _nonlocalEqPlasticStrain = ipsource->_nonlocalEqPlasticStrain;
    _nonlocalFailurePlasticity = ipsource->_nonlocalFailurePlasticity;
    _DirreversibleEnergyDNonLocalVariable = ipsource->_DirreversibleEnergyDNonLocalVariable;

    for (int i=0; i< ipsource->numNonLocalVariable; i++){
      if(ipsource->ipvCL[i] != NULL){
        if (ipvCL[i] != NULL){
          ipvCL[i]->operator=(*dynamic_cast<const IPCLength*>(ipsource->ipvCL[i]));
        }
      }
    }
  }
  return *this;
};


void IPHyperViscoElastoPlasticMultipleNonLocalDamage::restart(){
  IPHyperViscoElastoPlasticMultipleLocalDamage::restart();
  restartManager::restart(_nonlocalEqPlasticStrain);
  restartManager::restart(_nonlocalFailurePlasticity);
  for (int i=0; i< numNonLocalVariable; i++){
    restartManager::restart(_DirreversibleEnergyDNonLocalVariable[i]);
  }
  for (int i=0; i< numNonLocalVariable; i++){
    ipvCL[i]->restart();
  }
};
