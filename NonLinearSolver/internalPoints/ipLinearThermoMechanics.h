//
// Description: storing class for linear thermomechanical law it is a "virtual implementation" you have to define an ipvariable
//              in your project which derive from this one.
// Author:  <L. Homsy>, (C) 2013
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef IPLINEARTHERMOMECHANICS_H_
#define IPLINEARTHERMOMECHANICS_H_
#include "ipvariable.h"
#include "STensor3.h"
class IPLinearThermoMechanics : public IPVariableMechanics
{
 public:
  double _elasticEnergy;
  double _thermalEnergy;
  double _fracEnergy;
 public:
  IPLinearThermoMechanics();
  IPLinearThermoMechanics(const IPLinearThermoMechanics &source);
  IPLinearThermoMechanics& operator=(const IPVariable &source);
  virtual double defoEnergy() const;
  virtual double getThermalEnergy() const;
  virtual void restart();
  virtual IPVariable* clone() const {return new IPLinearThermoMechanics(*this);};
  virtual double getConstRefToFractureEnergy() const {return _fracEnergy;};
  virtual double get(const int comp) const;
  virtual double & getRef(const int comp);
};

#endif // IPLINEARTHERMOMECHANICS_H_
