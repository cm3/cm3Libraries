//
// C++ Interface: ipNucleationCriterion
//
// Description: Provide ip class for nucleation criterion 
//
//
// Author:  <J. Leclerc>, (C) 2019
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef IPNUCLEATIONCRITERION_H_
#define IPNUCLEATIONCRITERION_H_
#ifndef SWIG
#include "ipvariable.h"
#endif



/*! \class IPNucleationCriterion
 * Base or abstract class for NucleationCriterionlaw-related IPV.
 */
class IPNucleationCriterion
{
  // Nothing is needed yet.
 
public :
  //! @name Constructors / destructors / restart
  IPNucleationCriterion();
  IPNucleationCriterion( const IPNucleationCriterion& source );
  virtual IPNucleationCriterion &operator=( const IPNucleationCriterion& source );
  virtual ~IPNucleationCriterion(){};
  virtual IPNucleationCriterion* clone() const = 0;
  virtual void restart();
  //! @}


  //! @name Access functions
  //! return if the nucleation criterion is verified now (i.e. if nucleation is still active).
  virtual bool hasNucleationActivated() const = 0;
  //! update the nucleation criterion status.
  virtual void setNucleationActivation( const bool willBeActivated ) = 0;
  
  //! return if the nucleation criterion was verified at least once.
  virtual bool hasNucleationOnset() const = 0;
  //! update the nucleation criterion status.
  virtual void setNucleationOnset( const bool willBeOnset ) = 0;
  
  //! return if the nucleation is inhibited (i.e. the criterion will never be verified again).
  virtual bool isInhibited() const = 0;
  //! update the nucleation inhibition.
  virtual void setNucleationInhibition( const bool willBeInhibited ) = 0;
  
  
  
  //! return the total value of nucleation porosity
  inline virtual double  getAmpli() const { return 1.0; };
  //! update the total value of nucleation porosity 
  inline virtual void    setAmpli( const double& ampliFactor ) {
    Msg::Error( "IPNonTrivialNucleationFunction::setMeanNucleationStrain should never be used." );
  };
  
  
  //! @}
  
};




/*! \class IPTrivialNucleationCriterion
 * IPV-class for TrivialNucleationCriterionLaw.
 * By default, the nucleation is considered as activated 
 * and never inhibited
 * (nothing need to be added)
 */
class IPTrivialNucleationCriterion : public IPNucleationCriterion
{
  // Nothing is needed.
  
public :
  //! @name Constructors / destructors / restart

  IPTrivialNucleationCriterion();
  IPTrivialNucleationCriterion( const IPTrivialNucleationCriterion& source );
  virtual IPTrivialNucleationCriterion &operator=( const IPNucleationCriterion& source );
  virtual ~IPTrivialNucleationCriterion(){};
  virtual IPNucleationCriterion* clone() const { return new IPTrivialNucleationCriterion( *this ); };
  virtual void restart();
  //! @}
 
 
  //! @name Access functions

  //! return always true as it is trivial (nucleation = always activated).
  inline virtual bool hasNucleationActivated() const { return true; };
  //! send back an error as this variables is not stored.
  inline virtual void setNucleationActivation( const bool willBeActivated ) {
    Msg::Error("IPTrivialNucleationCriterion::setNucleationActivation this function should not be used");
  };
  
  //! return always true as it is trivial (nucleation = always activated).
  inline virtual bool hasNucleationOnset() const { return true; };
  //! send back an error as this variables is not stored.
  inline virtual void setNucleationOnset( const bool willBeOnset ) {
    Msg::Error("IPTrivialNucleationCriterion::setNucleationOnset this function should not be used");;
  };
  
  
  //! return always false as it is trivial (nucleation = can not be inhibited).
  inline virtual bool isInhibited() const { return false; };
  //! send back an error as this variables is not stored.
  inline virtual void setNucleationInhibition( const bool willBeInhibited ) {
    Msg::Error("IPTrivialNucleationCriterion::setNucleationInhibition this function should not be used");
  };
  //! @}

};





/*! \class IPNonTrivialNucleationCriterion
 * IPV-class basis for all criterion which are non-trivial. Variables
 * common to all non-trivial IPV are defined here as well the interface.
 */
class IPNonTrivialNucleationCriterion : public IPNucleationCriterion
{
protected :
  //! activation flag (==true if nucleation is activated),
  //! Inititated to false.
  bool hasNucleationActivated_;
  
  //! onset flag (==true if nucleation has been activated at least once),
  //! Inititated to false.
  bool hasNucleationOnset_;
  
  //! inhibition flag (==true if nucleation is inhibited),
  //! Inititated to false.
  bool isInhibited_;
  
  
public :
  //! @name Constructors / destructors / restart
  IPNonTrivialNucleationCriterion();
  IPNonTrivialNucleationCriterion( const IPNonTrivialNucleationCriterion& source );
  virtual IPNonTrivialNucleationCriterion &operator=( const IPNucleationCriterion& source );
  virtual ~IPNonTrivialNucleationCriterion(){};
  virtual IPNucleationCriterion* clone() const = 0;
  virtual void restart();
  //! @}
 
 
  //! @name Access functions
  inline virtual bool hasNucleationActivated() const { return hasNucleationActivated_; };
  inline virtual void setNucleationActivation( const bool willBeActivated ) {
    hasNucleationActivated_ = willBeActivated;
  };
  
  
  inline virtual bool hasNucleationOnset() const { return hasNucleationOnset_; };
  inline virtual void setNucleationOnset( const bool willBeOnset ) {
    hasNucleationOnset_ = willBeOnset;
  };


  inline virtual bool isInhibited() const { return isInhibited_; };
  inline virtual void setNucleationInhibition( const bool willBeInhibited ) {
    isInhibited_ = willBeInhibited;
  };
  //! @}
  
  
};





/*! \class IPPlasticBasedNucleationCriterion
 * IPV-class for PlasticBasedNucleationCriterionLaw.
 */
class IPPlasticBasedNucleationCriterion : public IPNonTrivialNucleationCriterion
{
protected :
  // Nothing more is needed.
  
public :
  //! @name Constructors / destructors / restart
  IPPlasticBasedNucleationCriterion();
  IPPlasticBasedNucleationCriterion( const IPPlasticBasedNucleationCriterion& source );
  virtual IPPlasticBasedNucleationCriterion &operator=( const IPNucleationCriterion& source );
  virtual ~IPPlasticBasedNucleationCriterion(){};
  virtual IPNucleationCriterion* clone() const { return new IPPlasticBasedNucleationCriterion( *this ); };
  virtual void restart();
  //! @}
};




/*! \class IPPorosityBasedNucleationCriterion
 * IPV-class for PorosityBasedNucleationCriterionLaw.
 */
class IPPorosityBasedNucleationCriterion : public IPNonTrivialNucleationCriterion
{
protected :
  // Nothing more is needed.
  
public :
  //! @name Constructors / destructors / restart
  IPPorosityBasedNucleationCriterion();
  IPPorosityBasedNucleationCriterion( const IPPorosityBasedNucleationCriterion& source );
  virtual IPPorosityBasedNucleationCriterion &operator=( const IPNucleationCriterion& source );
  virtual ~IPPorosityBasedNucleationCriterion(){};
  virtual IPNucleationCriterion* clone() const { return new IPPorosityBasedNucleationCriterion( *this ); };
  virtual void restart();
  //! @}

};




/*! \class IPBereminNucleationCriterion
 * IPV-class for BereminNucleationCriterionLaw.
 */
class IPBereminNucleationCriterion : public IPNonTrivialNucleationCriterion
{
protected :
  // Nothing more is needed.
  double ampliFactor_;
public :
  //! @name Constructors / destructors / restart
  IPBereminNucleationCriterion();
  IPBereminNucleationCriterion( const IPBereminNucleationCriterion& source );
  virtual IPBereminNucleationCriterion &operator=( const IPNucleationCriterion& source );
  virtual ~IPBereminNucleationCriterion(){};
  virtual IPNucleationCriterion* clone() const { return new IPBereminNucleationCriterion( *this ); };
  
  //! return the total value of nucleation porosity
  inline virtual double  getAmpli() const { return ampliFactor_; };
  //! update the total value of nucleation porosity 
  inline virtual void    setAmpli( const double& ampliFactor ) { ampliFactor_ = ampliFactor; };
  
  virtual void restart();
  //! @}
};





#endif //IPNUCLEATIONCRITERION_H_