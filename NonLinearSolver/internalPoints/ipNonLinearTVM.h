//
// C++ Interface: Material Law
//
// Description: IPNonLinearTVM (IP Thermo-ViscoElasto-ViscoPlasto Law) with Non-Local Damage Interface (soon.....)
//
// Author: <Ujwal Kishore J - FLE_Knight>, (C) 2023
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef IPNONLINEARTVM_H_
#define IPNONLINEARTVM_H_
#include "ipHyperelastic.h"
#include "j2IsotropicHardening.h"       
#include "kinematicHardening.h"
#include "STensor3.h"
#include "STensor43.h"
#include "mullinsEffect.h"


class IPNonLinearTVM : public IPHyperViscoElastoPlastic{
    public:
        double _thermalEnergy;                                                                                                               // Added _thermalEnergy
        double _T; 
        STensor3 _devDE;
        double _trDE;
        STensor3 _DE;
        double _mechSrcTVE;
        double _DmechSrcTVEdT;
        STensor3 _DmechSrcTVEdE;
        
        // mandel and commuteChecker
        STensor3 _ModMandel;
        double _mandelCommuteChecker;

        // CorKir - Inf and extraBranch TVE
        double _trCorKirinf_TVE;
        STensor3 _devCorKirinf_TVE;
        STensor3 _corKirExtra;
        double _psiInfCorrector;
        double _pressure;
        double _dAv_dTrEe, _dBd_dTrEe;
        double _Av_TVE, _Bd_TVE, _dAv_dTrEe_TVE;
        double _intAv_TVE, _intBd_TVE;
        double _intAv_1_TVE, _intAv_2_TVE;
        STensor3 _dBd_dDevEe;
        STensor3 _dBd_dDevEe_TVE, _intBd_1_TVE, _intBd_2_TVE;
        std::vector<double> _Av_TVE_vector, _Bd_TVE_vector, _dAv_dTrEe_TVE_vector, _dBd_dTrEe_TVE_vector, _intAv_TVE_vector, _intBd_TVE_vector,  _DintAv_TVE_vector, _DintBd_TVE_vector;
        std::vector<STensor3> _dBd_dDevEe_TVE_vector; 
        
        // DcorKirDT
        STensor3 _DcorKirDT;
        STensor3 _DDcorKirDTT;
        STensor43 _DDcorKirDTDE;
        
        // Derivatives of A and B
        std::vector<STensor3> _DA_DT, _DDA_DTT, _DA_DtrE, _DDA_DTDtrE;     
        std::vector<STensor43> _DA_DdevE, _DDA_DTDdevE;     
        std::vector<double> _DB_DT, _DDB_DTT, _DB_DtrE, _DDB_DTDtrE;                        
        
        // dtShift and derivatives
        double _dtShift;
        double _DdtShiftDT;
        double _DDdtShiftDDT;
        
        // Mullins Effect IP
        STensor3 _P_cap;
        IPMullinsEffect* _ipMullinsEffect;
        double _mullinsDamage;
        double _DmullinsDamage_Dpsi_cap;
        double _DmullinsDamage_DT;
        double _psiMax; // maximum strain energy
        double _DpsiDT;
        STensor3 _DpsiDE;
        
        // Dissipation IP
        double _viscousDissipatedEnergy;
        double _mullinsDissipatedEnergy;
        
        // Rotation tensors & derivatives
        STensor3 _R;
        STensor43 _dRdEe, _dRtdEe;


    public:
        IPNonLinearTVM(const J2IsotropicHardening* comp,
                    const J2IsotropicHardening* trac,
                    const kinematicHardening* kin, const int N, const mullinsEffect* mullins);
        IPNonLinearTVM(const IPNonLinearTVM& src);
        virtual IPNonLinearTVM& operator=(const IPVariable& src);
        virtual ~IPNonLinearTVM();
        
        virtual double& getRefToTrDE() {return _trDE;};
        virtual const double& getConstRefToTrDE() const {return _trDE;};
        virtual STensor3& getRefToDevDE() {return _devDE;};
        virtual const STensor3& getConstRefToDevDE() const {return _devDE;};
        virtual STensor3& getRefToDE() {return _DE;};
        virtual const STensor3& getConstRefToDE() const {return _DE;};
        
        virtual STensor3& getRefToDcorKirDT() {return _DcorKirDT;};
        virtual const STensor3& getConstRefToDcorKirDT() const {return _DcorKirDT;};
        
        virtual STensor3& getRefToDDcorKirDTT() {return _DDcorKirDTT;};
        virtual const STensor3& getConstRefToDDcorKirDTT() const {return _DDcorKirDTT;};
        
        virtual STensor43& getRefToDDcorKirDTDE() {return _DDcorKirDTDE;};
        virtual const STensor43& getConstRefToDDcorKirDTDE() const {return _DDcorKirDTDE;};
        
        virtual double &getRefToMechSrcTVE() {return _mechSrcTVE;}
        virtual const double &getConstRefToMechSrcTVE() const {return _mechSrcTVE;}
        virtual double &getRefTodMechSrcTVEdT() {return _DmechSrcTVEdT;}
        virtual const double &getConstRefTodMechSrcTVEdT() const {return _DmechSrcTVEdT;}
        virtual STensor3 &getRefTodMechSrcTVEdF() {return _DmechSrcTVEdE;}
        virtual const STensor3 &getConstRefTodMechSrcTVEdF() const {return _DmechSrcTVEdE;}

        virtual const IPMullinsEffect& getConstRefToIPMullinsEffect() const;
        virtual IPMullinsEffect& getRefToIPMullinsEffect();
        
        virtual double &getRefTo_Dpsi_DT() {return _DpsiDT;}
        virtual const double &getConstRefTo_Dpsi_DT() const {return _DpsiDT;}
        virtual STensor3 &getRefTo_Dpsi_DE() {return _DpsiDE;}
        virtual const STensor3 &getConstRefTo_DpsiVE_DE() const {return _DpsiDE;}
        
        virtual double defoEnergy() const;
        virtual double getThermalEnergy() const {return _thermalEnergy;};                     

        virtual IPVariable* clone() const{return new IPNonLinearTVM(*this);};
        virtual void restart();
};
#endif // IPNonLinearTVM.h
