//
// Description: storing class for vevp umat interface
// Author:  <V.D. NGUYEN>, (C) 2023
//
// Copyright: See COPYING file that comes with this distribution
//
//

#include "ipVEVPUMAT.h"
#include "ipField.h"
#include "restartManager.h"


double IPVEVPUMAT::get(int comp) const
{
  if(comp==IPField::PLASTICSTRAIN)
    return _statev[18];
  else
    return IPUMATInterface::get(comp);
}

void IPVEVPUMAT::restart()
{
  IPUMATInterface::restart();
 return;
}
