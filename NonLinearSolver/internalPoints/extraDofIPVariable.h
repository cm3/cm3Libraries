//
// C++ Interface: ipvariable
//
// Description: Define damage variable for extraDofs
//
//
// Author:  <V.D. Nguyen>, (C) 2016
//
// Copyright: See COPYING file that comes with this distribution
//
//

// all ipvariables with extraDof must be derived from this class to obtain homogenised properties


#ifndef EXTRADOFIPVARIABLE_H
#define EXTRADOFIPVARIABLE_H
#include "ipvariable.h"
class extraDofIPVariableBase
{
	public:
		extraDofIPVariableBase(){};
		virtual ~extraDofIPVariableBase(){}
		// constitutive field flux
		virtual void getConstitutiveExtraDofFlux(const int index, SVector3& flux) const = 0;
		// constitutive field internal energy
		virtual double getConstitutiveExtraDofInternalEnergy(const int index) const = 0;
		// constitutive field capacity per unit field
		virtual double getConstitutiveExtraDofFieldCapacityPerUnitField(const int index) const = 0;
		// mechanical source converted to this constitutive field
		virtual double getConstitutiveExtraDofMechanicalSource(const int index) const = 0;
		// non-constitutive field value
		virtual void getNonConstitutiveExtraDofFlux(const int index,SVector3& flux) const = 0;
};

#endif // EXTRADOFIPVARIABLE_H