//
// C++ Interface: Material Law
//
// Description: IPNonLinearTVE (IP Thermo-ViscoElasto-ViscoPlasto Law) with Non-Local Damage Interface (soon.....)
//
// Author: <Ujwal Kishore J - FLE_Knight>, (C) 2022; <Van Dung Nguyen>, (C) 2014
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef IPNONLINEARTVE_H_
#define IPNONLINEARTVE_H_
#include "ipHyperelastic.h"
#include "j2IsotropicHardening.h"       
#include "kinematicHardening.h"
#include "STensor3.h"
#include "STensor43.h"
#include "mullinsEffect.h"

class IPNonLinearTVE : public IPHyperViscoElastoPlastic{
    public:
        // Temperature history (I think) - from IPLinearThermoMechanics.h (fracEnergy is unused w/o fracture - add and initialise it later)
        double _thermalEnergy;                                                                                                               // Added _thermalEnergy
        double _T; 
        //  double _fracEnergy;
        STensor3 _devDE;
        double _trDE;
        STensor3 _DE;
        double _mechSrcTVE;
        double _DmechSrcTVEdT;
        STensor3 _DmechSrcTVEdF;
        
        double _trCorKirinf_TVE;
        STensor3 _devCorKirinf_TVE;
        STensor3 _corKirExtra;
        
        // Viscoelastic Strain
        std::vector<STensor3> _devOGammai;   // dev viscoelastic strain for each branch
        std::vector<double> _trOGammai;      // tr viscoelastic strain for each branch
        std::vector<STensor3> _devDOGammaiDT;  
        std::vector<double> _trDOGammaiDT;     
        
        // Viscoelastic Stress
        std::vector<STensor3> _devOi;   // dev viscoelastic stress for each branch
        std::vector<double> _trOi;      // tr viscoelastic stress for each branch
        std::vector<STensor3> _devDOiDT;  
        std::vector<double> _trDOiDT; 
        std::vector<STensor3> _devDDOiDTT;  
        std::vector<double> _trDDOiDTT; 
        
        // DcorKirDT
        STensor3 _DcorKirDT;
        STensor3 _DDcorKirDTT;
        
        // Additional Internal Variables 
        std::vector<STensor3> _C;           // dGi/dT summation
        std::vector<double> _D;             // dKi/dT summation
        std::vector<double> _E, _F, _G;
        
        // Additional variables
        double _dtShift;
        double _DdtShiftDT;
        double _DDdtShiftDDT;
        
        // Mullins Effect IP
        IPMullinsEffect* _ipMullinsEffect;
        double _mullinsDamage;
        double _psiMax; // maximum strain energy
        double _DpsiDT;
        
    public:
        IPNonLinearTVE(const J2IsotropicHardening* comp,
                    const J2IsotropicHardening* trac,
                    const kinematicHardening* kin, const int N, const mullinsEffect* mullins);
        IPNonLinearTVE(const IPNonLinearTVE& src);
        virtual IPNonLinearTVE& operator=(const IPVariable& src);
        virtual ~IPNonLinearTVE();
        
        virtual std::vector<STensor3>& getRefToDevViscoElasticOverStrain() {return _devOGammai;};
        virtual const std::vector<STensor3>& getConstRefToDevViscoElasticOverStrain() const {return _devOGammai;};
        
        virtual std::vector<double>& getRefToTrViscoElasticOverStrain() {return _trOGammai;};
        virtual const std::vector<double>& getConstRefToTrViscoElasticOverStrain() const {return _trOGammai;};
        
        virtual std::vector<STensor3>& getRefToDevDOGammaiDT() {return _devDOGammaiDT;};
        virtual const std::vector<STensor3>& getConstRefToDevDOGammaiDT() const {return _devDOGammaiDT;};
        
        virtual std::vector<double>& getRefToTrDOGammaiDT() {return _trDOGammaiDT;};
        virtual const std::vector<double>& getConstRefToTrDOGammaiDT() const {return _trDOGammaiDT;};
        
        virtual std::vector<STensor3>& getRefToDevViscoElasticOverStress() {return _devOi;};
        virtual const std::vector<STensor3>& getConstRefToDevViscoElasticOverStress() const {return _devOi;};
        
        virtual std::vector<double>& getRefToTrViscoElasticOverStress() {return _trOi;};
        virtual const std::vector<double>& getConstRefToTrViscoElasticOverStress() const {return _trOi;};
        
        virtual std::vector<STensor3>& getRefToDevDOiDT() {return _devDOiDT;};
        virtual const std::vector<STensor3>& getConstRefToDevDOiDT() const {return _devDOiDT;};
        
        virtual std::vector<double>& getRefToTrDOiDT() {return _trDOiDT;};
        virtual const std::vector<double>& getConstRefToTrDOiDT() const {return _trDOiDT;};
        
        virtual std::vector<STensor3>& getRefToDevDDOiDTT() {return _devDDOiDTT;};
        virtual const std::vector<STensor3>& getConstRefToDevDDOiDTT() const {return _devDDOiDTT;};
        
        virtual std::vector<double>& getRefToTrDDOiDTT() {return _trDDOiDTT;};
        virtual const std::vector<double>& getConstRefToTrDDOiDTT() const {return _trDDOiDTT;};
        
        virtual STensor3& getRefToDcorKirDT() {return _DcorKirDT;};
        virtual const STensor3& getConstRefToDcorKirDT() const {return _DcorKirDT;};
        
        virtual STensor3& getRefToDDcorKirDTT() {return _DDcorKirDTT;};
        virtual const STensor3& getConstRefToDDcorKirDTT() const {return _DDcorKirDTT;};
        
        virtual double& getRefToTrDE() {return _trDE;};
        virtual const double& getConstRefToTrDE() const {return _trDE;};
        virtual STensor3& getRefToDevDE() {return _devDE;};
        virtual const STensor3& getConstRefToDevDE() const {return _devDE;};
        
        virtual STensor3& getRefToDE() {return _DE;};
        virtual const STensor3& getConstRefToDE() const {return _DE;};
        
        virtual double &getRefToMechSrcTVE() {return _mechSrcTVE;}
        virtual const double &getConstRefToMechSrcTVE() const {return _mechSrcTVE;}
        virtual double &getRefTodMechSrcTVEdT() {return _DmechSrcTVEdT;}
        virtual const double &getConstRefTodMechSrcTVEdT() const {return _DmechSrcTVEdT;}
        virtual STensor3 &getRefTodMechSrcTVEdF() {return _DmechSrcTVEdF;}
        virtual const STensor3 &getConstRefTodMechSrcTVEdF() const {return _DmechSrcTVEdF;}
        
        virtual double& getShiftedTimeStep() {return _dtShift;};
        virtual const double& getConstShiftedTimeStep() const {return _dtShift;};
        virtual double& getShiftedTimeStepTempDerivative() {return _DdtShiftDT;};
        virtual const double& getConstShiftedTimeStepTempDerivative() const {return _DdtShiftDT;};
        virtual double& getShiftedTimeStepTempDoubleDerivative() {return _DDdtShiftDDT;};
        virtual const double& getConstShiftedTimeStepTempDoubleDerivative() const {return _DDdtShiftDDT;};
        
        virtual const IPMullinsEffect& getConstRefToIPMullinsEffect() const;
        virtual IPMullinsEffect& getRefToIPMullinsEffect();
        
        virtual double defoEnergy() const;
        virtual double getThermalEnergy() const {return _thermalEnergy;};                       // Added _thermalEnergy
        // virtual double getConstRefToFractureEnergy() const {return _fracEnergy;};            // Added   - Unused (edit this because its neither "const" nor "ref" here)

        virtual IPVariable* clone() const{return new IPNonLinearTVE(*this);};
        virtual void restart();
};
#endif // IPNonLinearTVE.h