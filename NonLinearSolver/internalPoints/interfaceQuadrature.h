//
// C++ Interface: Quadrature Rule for interface element
//
// Description: Gives the value on minus and plus element from the interface value
//
//
// Author:  <Gauthier BECKER>, (C) 2012
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef INTERFACEQUADRATURE_H_
#define INTERFACEQUADRATURE_H_
#include "MInterfaceElement.h"
#include "GaussIntegration.h"
class interfaceQuadratureBase
{
 public:
  interfaceQuadratureBase(){}
  interfaceQuadratureBase(const interfaceQuadratureBase &source){}
  virtual ~interfaceQuadratureBase(){}
  virtual int getIntPoints(MInterfaceElement *iele, IntPt *GPs, IntPt **GPm, IntPt **GPp)=0;
};

class interfaceQuadratureVoid : public interfaceQuadratureBase{
  public:
    interfaceQuadratureVoid(){}
    interfaceQuadratureVoid(const interfaceQuadratureVoid &source){}
    virtual ~interfaceQuadratureVoid(){}
    virtual int getIntPoints(MInterfaceElement *iele, IntPt *GPs, IntPt **GPm, IntPt **GPp){
      GPm = NULL; GPp=NULL;
      return 0;
    };
};

/*
// General case (cannot be used as different Gauss points can be in the same array)
class interfaceQuadrature : public interfaceQuadratureBase
{
 protected:
  int _npts;
  IntPt *_GPm; // cache to avoid multiple allocation --> no copy
  IntPt *_GPp; // cache to avoid multiple allocation --> no copy
 public:
  interfaceQuadrature(const int order) : interfaceQuadratureBase(), _npts((order+1)/2), _GPm(NULL), _GPp(NULL)
  {
    _GPm = new IntPt[_npts];
    _GPp = new IntPt[_npts];
  }
  interfaceQuadrature(const interfaceQuadrature &source) : interfaceQuadratureBase(source), _npts(source._npts){}
  virtual ~interfaceQuadrature()
  {
    if(_GPm!=NULL) delete[] _GPm; _GPm=NULL;
    if(_GPp!=NULL) delete[] _GPp; _GPp=NULL;
  }
  virtual int getIntPoints(MInterfaceElement *iele, IntPt *GPs, IntPt **GPm, IntPt **GPp);
};
*/
// For 1D (Quadrangle and triangle)
class interface1DQuadrature : public interfaceQuadratureBase
{
 protected:
  int _npts;
  IntPt *_GPq;  // quadrangle
  IntPt *_GPt;  // triangle
  IntPt *_GPqI; // Two orientations are possible
  IntPt *_GPtI;
 public:
  interface1DQuadrature(const int order, const bool Lobatto=false);
  virtual ~interface1DQuadrature();
  virtual int getIntPoints(MInterfaceElement *iele, IntPt *GPs, IntPt **GPm, IntPt **GPp);
};

// For 2D interface (tetra and hexa)
class interface2DQuadrature : public interfaceQuadratureBase
{
 protected:
  int _nptst;
  int _nptsh;
  IntPt *_GPt;
  IntPt *_GPtI;
  IntPt *_GPh;
  IntPt *_GPhI;

  IntPt* _GPpris[5]; // for prismatic element 5 faces
  IntPt* _GPprisI[5];
/*
  double uvwTetQuad[4][3][6][3];
  double uvwTetQuadI[4][3][6][3];

  double uvwHexLin[6][4][4][3];
  double uvwHexLinI[6][4][4][3];

  double uvwHexQuad[6][4][9][3];
  double uvwHexQuadI[6][4][9][3];

  double uvwTetOrd6[4][3][12][3];
  double uvwTetOrd6I[4][3][12][3];
*/

 public:
  interface2DQuadrature(const int order, const bool Lobatto);
  virtual ~interface2DQuadrature();
  virtual int getIntPoints(MInterfaceElement *IElement,IntPt *GPs,IntPt **GPm,IntPt **GPp);
};

#endif // INTERFACEQUADRATURE_H_
