//
// C++ Interface: Material Law
//
// Description: IPNonLinearTVM (IP Thermo-ViscoElasto-ViscoPlasto Law) with Non-Local Damage Interface (soon.....)
//
// Author: <Ujwal Kishore J - FLE_Knight>, (C) 2023
//
// Copyright: See COPYING file that comes with this distribution
//
//

#include "ipNonLinearTVM.h"

IPNonLinearTVM::IPNonLinearTVM(const J2IsotropicHardening* comp,
                    const J2IsotropicHardening* trac,
                    const kinematicHardening* kin, const int N, const mullinsEffect* mullins): IPHyperViscoElastoPlastic(comp,trac,kin,N),
                     _T(298.15),_devDE(0.),_trDE(0.),_DE(0.),_DcorKirDT(0.),_DDcorKirDTT(0.), _DDcorKirDTDE(0.), _thermalEnergy(0.), 
                     _mechSrcTVE(0.),_DmechSrcTVEdT(0.),_DmechSrcTVEdE(0.),
                     _dtShift(0.),_DdtShiftDT(0.),_DDdtShiftDDT(0.), 
                     _trCorKirinf_TVE(0.), _devCorKirinf_TVE(0.), _corKirExtra(0.), 
                     _psiMax(0.), _mullinsDamage(0.), _DpsiDT(0.), _DpsiDE(0.), _DmullinsDamage_Dpsi_cap(0.), _DmullinsDamage_DT(0.), _P_cap(0.),
                     _pressure(0.), _Av_TVE(1.), _Bd_TVE(1.), _dAv_dTrEe(0.), _dBd_dTrEe(0.), _dAv_dTrEe_TVE(0.), _intAv_TVE(0.), _intBd_TVE(0.), 
                     _intAv_1_TVE(0.), _intBd_1_TVE(0.), _intAv_2_TVE(0.), _intBd_2_TVE(0.), _dBd_dDevEe(0.), _dBd_dDevEe_TVE(0.),
                     _viscousDissipatedEnergy(0.), _mullinsDissipatedEnergy(0.), _psiInfCorrector(0.), _ModMandel(0.), _mandelCommuteChecker(0.),
					 _R(0.),_dRdEe(0.),_dRtdEe(0.){
        
    _DA_DT.clear(); _DDA_DTT.clear(); 
    _DA_DtrE.clear(); _DDA_DTDtrE.clear();
    _DA_DdevE.clear(); _DDA_DTDdevE.clear();
    _DB_DT.clear(); _DDB_DTT.clear();
    _DB_DtrE.clear(); _DDB_DTDtrE.clear();
    _Av_TVE_vector.clear(); _dAv_dTrEe_TVE_vector.clear();
    _Bd_TVE_vector.clear(); _dBd_dTrEe_TVE_vector.clear(); _dBd_dDevEe_TVE_vector.clear();
    _intAv_TVE_vector.clear(); _intBd_TVE_vector.clear();
    _DintAv_TVE_vector.clear(); _DintBd_TVE_vector.clear();
        
    for (int i=0; i < N; i++){
        STensor3 el(0.);
        STensor43 el43(0.);
        _DA_DT.push_back(el);
        _DDA_DTT.push_back(el);
        _DA_DtrE.push_back(el);
        _DDA_DTDtrE.push_back(el);
        _dBd_dDevEe_TVE_vector.push_back(el);
        _DB_DT.push_back(0.);
        _DDB_DTT.push_back(0.);
        _DB_DtrE.push_back(0.);
        _DDB_DTDtrE.push_back(0.);
        _Av_TVE_vector.push_back(0.);
        _Bd_TVE_vector.push_back(0.);
        _dAv_dTrEe_TVE_vector.push_back(0.);
        _dBd_dTrEe_TVE_vector.push_back(0.);
        _intAv_TVE_vector.push_back(0.);
        _intBd_TVE_vector.push_back(0.);
        _DintAv_TVE_vector.push_back(0.);
        _DintBd_TVE_vector.push_back(0.);
        _DA_DdevE.push_back(el43);
        _DDA_DTDdevE.push_back(el43);
    }
    
    _ipMullinsEffect = NULL;
    if (mullins!= NULL)  mullins->createIPVariable(_ipMullinsEffect);
    
};

IPNonLinearTVM::IPNonLinearTVM(const IPNonLinearTVM& src): IPHyperViscoElastoPlastic(src),
            _DA_DT(src._DA_DT), _DDA_DTT(src._DDA_DTT), _DA_DdevE(src._DA_DdevE), _DDA_DTDdevE(src._DDA_DTDdevE), _DA_DtrE(src._DA_DT), _DDA_DTDtrE(src._DDA_DTT),
            _DB_DT(src._DB_DT), _DDB_DTT(src._DDB_DTT), _DB_DtrE(src._DB_DtrE), _DDB_DTDtrE(src._DDB_DTDtrE),
            _T(src._T),_devDE(src._devDE), _trDE(src._trDE), _DE(src._DE), _DcorKirDT(src._DcorKirDT), _DDcorKirDTT(src._DDcorKirDTT), _DDcorKirDTDE(src._DDcorKirDTDE),
            _mechSrcTVE(src._mechSrcTVE),_DmechSrcTVEdT(src._DmechSrcTVEdT),_DmechSrcTVEdE(src._DmechSrcTVEdE),
            _thermalEnergy(src._thermalEnergy),
            _dtShift(src._dtShift),_DdtShiftDT(src._DdtShiftDT),_DDdtShiftDDT(src._DDdtShiftDDT), _psiMax(src._psiMax),
            _trCorKirinf_TVE(src._trCorKirinf_TVE), _devCorKirinf_TVE(src._devCorKirinf_TVE), _corKirExtra(src._corKirExtra), 
            _mullinsDamage(src._mullinsDamage), _DpsiDT(src._DpsiDT), _DpsiDE(src._DpsiDE),
            _DmullinsDamage_Dpsi_cap(src._DmullinsDamage_Dpsi_cap), _DmullinsDamage_DT(src._DmullinsDamage_DT), _P_cap(src._P_cap),
            _pressure(src._pressure), _Av_TVE(src._Av_TVE), _Bd_TVE(src._Bd_TVE), 
            _dAv_dTrEe(src._dAv_dTrEe), _dBd_dTrEe(src._dBd_dTrEe), _dAv_dTrEe_TVE(src._dAv_dTrEe_TVE), 
            _intAv_TVE(src._intAv_TVE), _intBd_TVE(src._intBd_TVE),
            _intAv_1_TVE(src._intAv_1_TVE), _intBd_1_TVE(src._intBd_1_TVE), _intAv_2_TVE(src._intAv_2_TVE), _intBd_2_TVE(src._intBd_2_TVE),
            _dBd_dDevEe(src._dBd_dDevEe), _dBd_dDevEe_TVE(src._dBd_dDevEe_TVE),
            _Av_TVE_vector(src._Av_TVE_vector), _Bd_TVE_vector(src._Bd_TVE_vector), _dAv_dTrEe_TVE_vector(src._dAv_dTrEe_TVE_vector), _dBd_dTrEe_TVE_vector(src._dBd_dTrEe_TVE_vector),
            _intAv_TVE_vector(src._intAv_TVE_vector), _intBd_TVE_vector(src._intBd_TVE_vector),_DintAv_TVE_vector(src._DintAv_TVE_vector), _DintBd_TVE_vector(src._DintBd_TVE_vector),
			_dBd_dDevEe_TVE_vector(src._dBd_dDevEe_TVE_vector),
            _viscousDissipatedEnergy(src._viscousDissipatedEnergy), _mullinsDissipatedEnergy(src._mullinsDissipatedEnergy), _psiInfCorrector(src._psiInfCorrector),
			_ModMandel(src._ModMandel), _mandelCommuteChecker(src._mandelCommuteChecker),_R(src._R),_dRdEe(src._dRdEe),_dRtdEe(src._dRtdEe){
                
    if (src._ipMullinsEffect != NULL)
        _ipMullinsEffect = dynamic_cast<IPMullinsEffect*>(src._ipMullinsEffect->clone());
    else
        _ipMullinsEffect = NULL;                
};
            
IPNonLinearTVM& IPNonLinearTVM::operator=(const IPVariable& src)
{
  IPHyperViscoElastoPlastic::operator=(src);
  const IPNonLinearTVM* psrc = dynamic_cast<const IPNonLinearTVM*>(&src);
  if(psrc != NULL)
  {
    _DA_DT = psrc->_DA_DT; 
    _DDA_DTT = psrc->_DDA_DTT; 
    _DA_DtrE = psrc->_DA_DtrE; 
    _DDA_DTDtrE = psrc->_DDA_DTDtrE; 
    _DA_DdevE = psrc->_DA_DdevE; 
    _DDA_DTDdevE = psrc->_DDA_DTDdevE; 
    _DB_DT = psrc->_DB_DT; 
    _DDB_DTT = psrc->_DDB_DTT; 
    _DB_DtrE = psrc->_DB_DtrE; 
    _DDB_DTDtrE = psrc->_DDB_DTDtrE; 
    _T = psrc->_T;
    _devDE = psrc->_devDE; 
    _trDE = psrc->_trDE;
    _DE = psrc->_DE;
    _mechSrcTVE = psrc->_mechSrcTVE;
    _DmechSrcTVEdT = psrc->_DmechSrcTVEdT;
    _DmechSrcTVEdE = psrc->_DmechSrcTVEdE;
    _DcorKirDT = psrc->_DcorKirDT;
    _DDcorKirDTT = psrc->_DDcorKirDTT;
    _DDcorKirDTDE = psrc->_DDcorKirDTDE;
    _thermalEnergy = psrc->_thermalEnergy;
    _dtShift = psrc->_dtShift;
    _DdtShiftDT = psrc->_DdtShiftDT;
    _DDdtShiftDDT = psrc->_DDdtShiftDDT;
    _psiMax = psrc->_psiMax;
    _trCorKirinf_TVE = psrc->_trCorKirinf_TVE;
    _devCorKirinf_TVE = psrc->_devCorKirinf_TVE;
    _corKirExtra = psrc->_corKirExtra;
    _mullinsDamage = psrc->_mullinsDamage;
    _DpsiDT = psrc->_DpsiDT;
    _DmullinsDamage_Dpsi_cap = psrc->_DmullinsDamage_Dpsi_cap;
    _DmullinsDamage_DT = psrc->_DmullinsDamage_DT;
    _P_cap = psrc->_P_cap;
    _pressure = psrc->_pressure;
    _Av_TVE = psrc->_Av_TVE;
    _Bd_TVE = psrc->_Bd_TVE;
    _dAv_dTrEe = psrc->_dAv_dTrEe;
    _dBd_dTrEe = psrc->_dBd_dTrEe;
    _dAv_dTrEe_TVE = psrc->_dAv_dTrEe_TVE;
    _intAv_TVE = psrc->_intAv_TVE;
    _intBd_TVE = psrc->_intBd_TVE;
    _intAv_1_TVE = psrc->_intAv_1_TVE;
    _intBd_1_TVE = psrc->_intBd_1_TVE;
    _intAv_2_TVE = psrc->_intAv_2_TVE;
    _intBd_2_TVE = psrc->_intBd_2_TVE;
    _dBd_dDevEe = psrc->_dBd_dDevEe;
    _dBd_dDevEe_TVE = psrc->_dBd_dDevEe_TVE;
    _Av_TVE_vector = psrc->_Av_TVE_vector;
    _Bd_TVE_vector = psrc->_Bd_TVE_vector;
    _dAv_dTrEe_TVE_vector = psrc->_dAv_dTrEe_TVE_vector;
    _dBd_dTrEe_TVE_vector = psrc->_dBd_dTrEe_TVE_vector;
    _intAv_TVE_vector = psrc->_intAv_TVE_vector;
    _intBd_TVE_vector = psrc->_intBd_TVE_vector;
    _DintAv_TVE_vector = psrc->_DintAv_TVE_vector;
    _DintBd_TVE_vector = psrc->_DintBd_TVE_vector;
    _dBd_dDevEe_TVE_vector = psrc->_dBd_dDevEe_TVE_vector;
    _viscousDissipatedEnergy = psrc->_viscousDissipatedEnergy;
    _mullinsDissipatedEnergy = psrc->_mullinsDissipatedEnergy;
    _psiInfCorrector = psrc->_psiInfCorrector;
    _ModMandel = psrc->_ModMandel;
    _mandelCommuteChecker = psrc->_mandelCommuteChecker;
    _R = psrc->_R;
    _dRdEe = psrc->_dRdEe;
    _dRtdEe = psrc->_dRtdEe;
    
    if ( psrc->_ipMullinsEffect != NULL) {
      if (_ipMullinsEffect == NULL){
        _ipMullinsEffect = dynamic_cast<IPMullinsEffect*>(psrc->_ipMullinsEffect->clone());
      }
      else{
        _ipMullinsEffect->operator=(*dynamic_cast<const IPMullinsEffect*>(psrc->_ipMullinsEffect));
      }
    }
    
  }
  return *this;
}

IPNonLinearTVM::~IPNonLinearTVM(){
  if (_ipMullinsEffect != NULL) delete _ipMullinsEffect;
  _ipMullinsEffect = NULL;
};

double IPNonLinearTVM::defoEnergy() const
{
  return IPHyperViscoElastic::defoEnergy();
}

void IPNonLinearTVM::restart(){
  IPHyperViscoElastoPlastic::restart();
  restartManager::restart(_DA_DT);
  restartManager::restart(_DDA_DTT);
  restartManager::restart(_DA_DtrE);
  restartManager::restart(_DDA_DTDtrE);
  restartManager::restart(_DA_DdevE);
  restartManager::restart(_DDA_DTDdevE);
  restartManager::restart(_DB_DT);
  restartManager::restart(_DDB_DTT);
  restartManager::restart(_DB_DtrE);
  restartManager::restart(_DDB_DTDtrE);
  restartManager::restart(_devDE);
  restartManager::restart(_trDE);
  restartManager::restart(_DE);
  restartManager::restart(_DcorKirDT);
  restartManager::restart(_DDcorKirDTT);
  restartManager::restart(_DDcorKirDTDE);
  restartManager::restart(_mechSrcTVE);
  restartManager::restart(_DmechSrcTVEdT);
  restartManager::restart(_DmechSrcTVEdE);
  restartManager::restart(_T);
  restartManager::restart(_thermalEnergy);
  restartManager::restart(_dtShift);
  restartManager::restart(_DdtShiftDT);
  restartManager::restart(_DDdtShiftDDT);
  restartManager::restart(_psiMax);
  restartManager::restart(_trCorKirinf_TVE);
  restartManager::restart(_devCorKirinf_TVE);
  restartManager::restart(_corKirExtra);
  restartManager::restart(_mullinsDamage);
  restartManager::restart(_DpsiDT);
  restartManager::restart(_DpsiDE);
  restartManager::restart(_DmullinsDamage_Dpsi_cap);
  restartManager::restart(_DmullinsDamage_DT);
  restartManager::restart(_P_cap);
  restartManager::restart(_pressure);
  restartManager::restart(_Av_TVE);
  restartManager::restart(_Bd_TVE);
  restartManager::restart(_dAv_dTrEe);
  restartManager::restart(_dBd_dTrEe);
  restartManager::restart(_dAv_dTrEe_TVE);
  restartManager::restart(_intAv_TVE);
  restartManager::restart(_intBd_TVE);
  restartManager::restart(_intAv_1_TVE);
  restartManager::restart(_intBd_1_TVE);
  restartManager::restart(_intAv_2_TVE);
  restartManager::restart(_intBd_2_TVE);
  restartManager::restart(_dBd_dDevEe);
  restartManager::restart(_dBd_dDevEe_TVE);
  restartManager::restart(_Av_TVE_vector);
  restartManager::restart(_Bd_TVE_vector);
  restartManager::restart(_dAv_dTrEe_TVE_vector);
  restartManager::restart(_dBd_dTrEe_TVE_vector);
  restartManager::restart(_intAv_TVE_vector);
  restartManager::restart(_intBd_TVE_vector);
  restartManager::restart(_DintAv_TVE_vector);
  restartManager::restart(_DintBd_TVE_vector);
  restartManager::restart(_dBd_dDevEe_TVE_vector);
  restartManager::restart(_viscousDissipatedEnergy);
  restartManager::restart(_mullinsDissipatedEnergy);
  restartManager::restart(_psiInfCorrector);
  restartManager::restart(_ModMandel);
  restartManager::restart(_mandelCommuteChecker);
  restartManager::restart(_R);
  restartManager::restart(_dRdEe);
  restartManager::restart(_dRtdEe);
  
  if (_ipMullinsEffect != NULL)
   restartManager::restart(_ipMullinsEffect);
}

const IPMullinsEffect& IPNonLinearTVM::getConstRefToIPMullinsEffect() const{
  if(_ipMullinsEffect==NULL)
      Msg::Error("IPNonLinearTVM: _ipMullinsEffect not initialized");
  return *_ipMullinsEffect;
};
IPMullinsEffect& IPNonLinearTVM::getRefToIPMullinsEffect(){
  if(_ipMullinsEffect==NULL)
      Msg::Error("IPNonLinearTVM: _ipMullinsEffect not initialized");
  return *_ipMullinsEffect;
};
