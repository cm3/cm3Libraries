//
// Created by vinayak on 24.08.22.
//
//
// Description: storing class for Electro-Magnetic Inductor region
// computing normal direction to a gauss point to impose current
// density js0 in inductor
//
// Author:  <Vinayak GHOLAP>, (C) 2022
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef IPELECMAGINDUCTOR_H_
#define IPELECMAGINDUCTOR_H_

#include "ipElecMagGenericThermoMech.h"

class IPElecMagInductor : public IPElecMagGenericThermoMech
{
protected:
    SVector3 _normal;
public:
    IPElecMagInductor();
    IPElecMagInductor(const SVector3 &GaussP, const SVector3 &Centroid, const SVector3 &CentralAxis);
    IPElecMagInductor(const IPElecMagInductor &source);
    IPElecMagInductor& operator=(const IPVariable &source);
    virtual void getNormalDirection(SVector3 &normal) const { normal =_normal;}
    virtual void restart();
    virtual IPVariable* clone() const {return new IPElecMagInductor(*this);}
    virtual double defoEnergy() const;
    virtual double plasticEnergy() const;
    virtual double damageEnergy() const;
    virtual double irreversibleEnergy() const;
    virtual double get(const int comp) const;
    virtual double & getRef(const int comp);
};

#endif //IPELECMAGINDUCTOR_H_
