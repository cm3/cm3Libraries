//
// Description: storing class for J2 Thermo Mechanics
//
//
// Author:  <L. Noels>, (C) 2014
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "ipJ2ThermoMechanics.h"
IPJ2ThermoMechanics::IPJ2ThermoMechanics() : IPJ2linear(), _thermalEnergy(0.){};
IPJ2ThermoMechanics::IPJ2ThermoMechanics(const IPJ2ThermoMechanics &source) : IPJ2linear(source), _thermalEnergy(source._thermalEnergy){}
IPJ2ThermoMechanics::IPJ2ThermoMechanics(const J2IsotropicHardening *j2IH) : IPJ2linear(j2IH){}

IPJ2ThermoMechanics& IPJ2ThermoMechanics::operator=(const IPVariable &source)
{
  IPJ2linear::operator=(source);
  const IPJ2ThermoMechanics* src = dynamic_cast<const IPJ2ThermoMechanics*>(&source);
  if(src)
  {
    _thermalEnergy=src->_thermalEnergy;
  }
  return *this;
}

double IPJ2ThermoMechanics::defoEnergy() const
{
  return IPJ2linear::defoEnergy();

}
double IPJ2ThermoMechanics::plasticEnergy() const
{
  return IPJ2linear::plasticEnergy();
}

void IPJ2ThermoMechanics::restart(){
  IPJ2linear::restart();
  restartManager::restart(_thermalEnergy);
}
