//
// C++ Interface: ipNucleation
//
// Description: Base class for ipNucleation
//
//
// Author:  <L. Noels>, (C) 2013
//
// Copyright: See COPYING file that comes with this distribution
//
//
// class with the variables of IP
#ifndef IPNUCLEATION_H_
#define IPNUCLEATION_H_
#include <stdlib.h>
#include <stdio.h>
#include "GmshConfig.h"
#include "ipvariable.h"
#include "STensorOperations.h"
#include "ipNucleationCriterion.h"
#include "ipNucleationFunction.h"
class NucleationLaw;



/*! \class IPNucleation
 * This class stores data corresponding to the "NucleationLaw".
 * Practically, it gathers the vectors containing the ipvs
 * associated with each nucleation criterion and each function
 */
class IPNucleation
{
protected :
  //! vector gathering the ipv pointers for nucleation function.
  std::vector< IPNucleationFunction* >  ipFunctionVector_;
  
  //! vector gathering the ipv pointers for the nucleation criterion.
  std::vector< IPNucleationCriterion* > ipCriterionVector_;
  
  
  //! \fixme I should not be stored here.
  double nucleatedfV; // nucleated porosity
  double DeltafV; // increment compared to previous
  double DDeltafVDHatP;// derivatives

  //! @name Constructors / destructors / restart
private :
  //! default constructor forbiden
  IPNucleation();
public :
  IPNucleation( const NucleationLaw* nuclLaw );
  IPNucleation( const IPNucleation& source );
  virtual IPNucleation& operator=( const IPNucleation& source );
  virtual ~IPNucleation();
  virtual void restart();
  virtual IPNucleation * clone() const { return new IPNucleation(*this); };
  //! @}
  
  
  
  //! @name Access functions
  inline const IPNucleationFunction&   getConstRefToIPNucleationFunction( const int i ) const {
    #ifdef _DEBUG
    if ( ipFunctionVector_[i] == NULL ) Msg::Error( "IPNucleation:: ipFunctionVector_[%d] is not defined.", i );
    #endif // _DEBUG
    return *ipFunctionVector_[i];
  };
  
  inline       IPNucleationFunction&   getRefToIPNucleationFunction( const int i ) const{
    #ifdef _DEBUG
    if ( ipFunctionVector_[i] == NULL ) Msg::Error( "IPNucleation:: ipFunctionVector_[%d] is not defined.", i );
    #endif // _DEBUG
    return *ipFunctionVector_[i];
  };
  
  inline const IPNucleationCriterion&  getConstRefToIPNucleationCriterion( const int i ) const{
    #ifdef _DEBUG
    if ( ipCriterionVector_[i] == NULL ) Msg::Error( "IPNucleation:: ipCriterionVector_[%d] is not defined.", i );
    #endif // _DEBUG
    return *ipCriterionVector_[i];
  };
  
  inline       IPNucleationCriterion&  getRefToIPNucleationCriterion( const int i ) const{
    #ifdef _DEBUG
    if ( ipCriterionVector_[i] == NULL ) Msg::Error( "IPNucleation:: ipCriterionVector_[%d] is not defined.", i );
    #endif // _DEBUG
    return *ipCriterionVector_[i];
  };


  inline double   getNucleatedPorosity() const { return nucleatedfV; };
  inline double&  getRefToNucleatedPorosity()  { return nucleatedfV; };

  inline double   getNucleatedPorosityIncrement() const { return DeltafV; };
  inline double&  getRefToNucleatedPorosityIncrement()  { return DeltafV; };
  
  inline double   getDNucleatedPorosityDMatrixPlasticDeformation() const { return DDeltafVDHatP; };
  inline double&  getRefToDNucleatedPorosityDMatrixPlasticDeformation()  { return DDeltafVDHatP; };
  //! @}
  
  
};

#endif //IPNUCLEATION_H_
