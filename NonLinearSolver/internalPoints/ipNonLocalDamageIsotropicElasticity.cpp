//
// Description: storing class for j2 elasto-plastic law with non-local damage
//
//
// Author:  <L. Noels>, (C) 2014
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "ipNonLocalDamageIsotropicElasticity.h"
#include "restartManager.h"

IPLocalDamageIsotropicElasticity::IPLocalDamageIsotropicElasticity(const DamageLaw* damLaw)
    : IPVariableMechanics(), _elasticEnergy(0.), _EffectiveStrains(0.),  _CriticalEffectiveStrains(0.),
    _dissipationBlocked(false),_activeDamaging(false), _irreversibleEnergy(0.), _DirreversibleEnergyDF(0.), 
    _damageEnergy(0.){
  
  _ipvDam = NULL;
  if(damLaw == NULL)
  {
    Msg::Error("IPLocalDamageIsotropicElasticity::IPLocalDamageIsotropicElasticity has no damage law");
  }
  else
  {
    damLaw->createIPVariable(_ipvDam);
  }

}


IPLocalDamageIsotropicElasticity::IPLocalDamageIsotropicElasticity(const IPLocalDamageIsotropicElasticity& source) : IPVariableMechanics(source),
  _elasticEnergy(source._elasticEnergy), _EffectiveStrains(source._EffectiveStrains),
  _CriticalEffectiveStrains(source._CriticalEffectiveStrains),
  _dissipationBlocked(source._dissipationBlocked),
  _activeDamaging(source._activeDamaging),
  _irreversibleEnergy(source._irreversibleEnergy),
  _DirreversibleEnergyDF(source._DirreversibleEnergyDF),
  _damageEnergy(source._damageEnergy){
      
  _ipvDam = NULL;
  if(source._ipvDam != NULL){
    _ipvDam = dynamic_cast<IPDamage*>(source._ipvDam->clone());
  }
}


IPLocalDamageIsotropicElasticity& IPLocalDamageIsotropicElasticity::operator=(const IPVariable& source)
{
  IPVariableMechanics::operator=(source);
  const IPLocalDamageIsotropicElasticity* src = dynamic_cast<const IPLocalDamageIsotropicElasticity*>(&source);
  if(src != NULL)
  {
    _elasticEnergy = src->_elasticEnergy;
    _EffectiveStrains = src->_EffectiveStrains;
    _CriticalEffectiveStrains = src->_CriticalEffectiveStrains;
    _dissipationBlocked = src->_dissipationBlocked;
    _activeDamaging = src->_activeDamaging;
    _irreversibleEnergy = src->_irreversibleEnergy;
    _DirreversibleEnergyDF = src->_DirreversibleEnergyDF;
    _damageEnergy = src->_damageEnergy;

    if(src->_ipvDam != NULL)
    {
			if (_ipvDam != NULL){
				_ipvDam->operator=(*dynamic_cast<const IPVariable*>(src->_ipvDam));
			}
			else{
				_ipvDam= dynamic_cast<IPDamage*>(src->_ipvDam->clone());
			}
    }
		else{
			if(_ipvDam != NULL) delete _ipvDam; _ipvDam = NULL;
		}
  }
  return *this;
}



void IPLocalDamageIsotropicElasticity::restart()
{
  IPVariableMechanics::restart();
  restartManager::restart(_ipvDam);
  restartManager::restart(_elasticEnergy);
  restartManager::restart(_EffectiveStrains);
  restartManager::restart(_CriticalEffectiveStrains);
  restartManager::restart(_dissipationBlocked);
  restartManager::restart(_activeDamaging);
  restartManager::restart(_irreversibleEnergy);
  restartManager::restart(_DirreversibleEnergyDF);
  restartManager::restart(_damageEnergy);
  return;
}


IPNonLocalDamageIsotropicElasticity::IPNonLocalDamageIsotropicElasticity(const CLengthLaw* cLLaw, const DamageLaw* damLaw)
    : IPLocalDamageIsotropicElasticity(damLaw), _NonLocalEffectiveStrains(0.), _NonLocalToLocal(false),
    _DirreversibleEnergyDNonLocalVariable(0.)
{
  _ipvCL = NULL;
  if(cLLaw == NULL)
  {
      Msg::Error("IPNonLocalDamageIsotropicElasticity::IPNonLocalDamageIsotropicElasticity has no c_l law");
  }
  else
  {
    cLLaw->createIPVariable(_ipvCL);
  }
}


IPNonLocalDamageIsotropicElasticity::IPNonLocalDamageIsotropicElasticity(const IPNonLocalDamageIsotropicElasticity& source) : 
  IPLocalDamageIsotropicElasticity(source),
    _NonLocalEffectiveStrains(source._NonLocalEffectiveStrains),
    _NonLocalToLocal(source._NonLocalToLocal),
    _DirreversibleEnergyDNonLocalVariable(source._DirreversibleEnergyDNonLocalVariable){
  _ipvCL = NULL;
  if(source._ipvCL != NULL)
  {
    _ipvCL = dynamic_cast<IPCLength*>(source._ipvCL->clone());
  }
}


IPNonLocalDamageIsotropicElasticity& IPNonLocalDamageIsotropicElasticity::operator=(const IPVariable& source)
{
  IPLocalDamageIsotropicElasticity::operator=(source);
  const IPNonLocalDamageIsotropicElasticity* src = dynamic_cast<const IPNonLocalDamageIsotropicElasticity*>(&source);
  if(src != NULL)
  {
    _NonLocalEffectiveStrains = src->_NonLocalEffectiveStrains;
    _NonLocalToLocal = src->_NonLocalToLocal;
    _DirreversibleEnergyDNonLocalVariable = src->_DirreversibleEnergyDNonLocalVariable;
    if(src->_ipvCL != NULL)
    {
			if (_ipvCL != NULL){
				_ipvCL->operator=(*dynamic_cast<const IPCLength*>(src->_ipvCL));
			}
			else{
				_ipvCL= dynamic_cast<IPCLength*>(src->_ipvCL->clone());
			}
    }
		else{
			if(_ipvCL != NULL) delete _ipvCL; _ipvCL = NULL;
		}

  }
  return *this;
}



void IPNonLocalDamageIsotropicElasticity::restart()
{
  IPLocalDamageIsotropicElasticity::restart();
  restartManager::restart(_ipvCL);
  restartManager::restart(_NonLocalEffectiveStrains);
  restartManager::restart(_NonLocalToLocal);
  restartManager::restart(_DirreversibleEnergyDNonLocalVariable);
  return;
}









