//
// Description: storing class for characteristic length
//
//
// Author:  <L. Noels>, (C) 2013
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "ipCLength.h"
#include "restartManager.h"

IPCLength::IPCLength(): cL(0)
{
 for(int i=1; i<3; i++)
   cL(i,i)= 0.;
}

IPCLength::IPCLength(const IPCLength &source): cL(source.cL)
{

}

IPCLength &IPCLength::operator=(const IPCLength &source)
{
  const IPCLength* src = dynamic_cast<const IPCLength*>(&source);
  if(src!=NULL)
  {
    cL=src->cL;
  }
  return *this;
}

void IPCLength::restart()
{
  restartManager::restart(cL);
  return;
}

IPZeroCLength::IPZeroCLength() : IPCLength()
{

}
IPZeroCLength::IPZeroCLength(const IPZeroCLength &source) :IPCLength(source)
{

}

IPZeroCLength &IPZeroCLength::operator=(const IPCLength &source)
{
  IPCLength::operator=(source);
  const IPZeroCLength* src = dynamic_cast<const IPZeroCLength*>(&source);
  if(src!=NULL)
  {
  }
  return *this;
}
void IPZeroCLength::restart()
{
  IPCLength::restart();
}

IPCLength *IPZeroCLength::clone() const
{
  return new IPZeroCLength(*this);
}

IPIsotropicCLength::IPIsotropicCLength() : IPCLength()
{

}
IPIsotropicCLength::IPIsotropicCLength(const IPIsotropicCLength &source) :IPCLength(source)
{

}

IPIsotropicCLength &IPIsotropicCLength::operator=(const IPCLength &source)
{
  IPCLength::operator=(source);
  const IPIsotropicCLength* src = dynamic_cast<const IPIsotropicCLength*>(&source);
  if(src!=NULL)
  {
  }
  return *this;
}
void IPIsotropicCLength::restart()
{
  IPCLength::restart();
}


IPCLength* IPIsotropicCLength::clone() const
{
  return new IPIsotropicCLength(*this);
}

IPAnisotropicCLength::IPAnisotropicCLength() : IPCLength()
{

}
IPAnisotropicCLength::IPAnisotropicCLength(const IPAnisotropicCLength &source) :IPCLength(source)
{

}

IPAnisotropicCLength &IPAnisotropicCLength::operator=(const IPCLength &source)
{
  IPCLength::operator=(source);
  const IPAnisotropicCLength* src = dynamic_cast<const IPAnisotropicCLength*>(&source);
  if(src!=NULL)
  {
  }
  return *this;
}
void IPAnisotropicCLength::restart()
{
  IPCLength::restart();
}

IPCLength* IPAnisotropicCLength::clone() const
{
  return new IPAnisotropicCLength(*this);
}


IPVariableIsotropicCLength::IPVariableIsotropicCLength() : IPCLength()
{

}
IPVariableIsotropicCLength::IPVariableIsotropicCLength(const IPVariableIsotropicCLength &source) :IPCLength(source)
{

}

IPVariableIsotropicCLength &IPVariableIsotropicCLength::operator=(const IPCLength &source)
{
  IPCLength::operator=(source);
  const IPVariableIsotropicCLength* src = dynamic_cast<const IPVariableIsotropicCLength*>(&source);
  if(src!=NULL)
  {
  }
  return *this;
}
void IPVariableIsotropicCLength::restart()
{
  IPCLength::restart();
}


IPCLength* IPVariableIsotropicCLength::clone() const
{
  return new IPVariableIsotropicCLength(*this);
}

IPVariableAnisotropicCLength::IPVariableAnisotropicCLength() : IPCLength()
{

}
IPVariableAnisotropicCLength::IPVariableAnisotropicCLength(const IPVariableAnisotropicCLength &source) :IPCLength(source)
{

}

IPVariableAnisotropicCLength &IPVariableAnisotropicCLength::operator=(const IPCLength &source)
{
  IPCLength::operator=(source);
  const IPVariableAnisotropicCLength* src = dynamic_cast<const IPVariableAnisotropicCLength*>(&source);
  if(src!=NULL)
  {
  }
  return *this;
}
void IPVariableAnisotropicCLength::restart()
{
  IPCLength::restart();
}


IPCLength* IPVariableAnisotropicCLength::clone() const
{
  return new IPVariableAnisotropicCLength(*this);
}

