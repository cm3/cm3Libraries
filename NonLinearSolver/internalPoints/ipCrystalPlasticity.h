//
// Description: storing class for crystal plasticity umat interface 
// Author:  <L. NOELS>, (C) 2019
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef IPCRYSTALPLASTICITY_H_
#define IPCRYSTALPLASTICITY_H_
#include "ipUMATInterface.h"
#include "STensor3.h"
class IPCrystalPlasticity : public IPUMATInterface
{
 protected:

 public: // All data public to avoid the creation of function to access

 public:
  IPCrystalPlasticity(int _nsdv, double eleSize) : IPUMATInterface(_nsdv, eleSize) 
    {
    }
  IPCrystalPlasticity(const IPCrystalPlasticity &source) : IPUMATInterface(source)
    {
    }
  IPCrystalPlasticity &operator = (const IPVariable &_source)
  {
      IPUMATInterface::operator=(_source);
      return *this;
  }
  virtual ~IPCrystalPlasticity()
  {
  }
  virtual IPVariable* clone() const {return new IPUMATInterface(*this);};
  virtual void restart();
  virtual double get(const int i) const;

 protected:
  IPCrystalPlasticity(){}
};

#endif // IPNONLOCALDAMAGE_H_
