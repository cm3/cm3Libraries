//
// Description: storing class for Linear Thermo Mechanics
//
//
// Author:  <Lina Homsy>, (C) 2013
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "ipLinearElecTherMech.h"

IPLinearElecTherMech::IPLinearElecTherMech() : IPLinearThermoMechanics() {};
IPLinearElecTherMech::IPLinearElecTherMech(const IPLinearElecTherMech &source) :  IPLinearThermoMechanics(source){}
IPLinearElecTherMech& IPLinearElecTherMech::operator=(const IPVariable &source)
{
  IPLinearThermoMechanics::operator=(source);
  const IPLinearElecTherMech* src = static_cast<const IPLinearElecTherMech*>(&source);
  return *this;
}

double IPLinearElecTherMech::defoEnergy() const
{
  return  IPLinearThermoMechanics::defoEnergy();
}

void IPLinearElecTherMech::restart()
{
  IPLinearThermoMechanics::restart();//n
  IPVariableMechanics::restart();
  restartManager::restart( _elasticEnergy);
}
double IPLinearElecTherMech::get(const int comp) const
{
    return IPLinearThermoMechanics::get(comp);
}

double & IPLinearElecTherMech::getRef(const int comp)
{
    return IPLinearThermoMechanics::getRef(comp);
}
