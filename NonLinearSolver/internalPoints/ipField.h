//
// C++ Interface: terms
//
// Description: Class to compute Internal point
//
//
// Author:  <Gauthier BECKER>, (C) 2010
// <Van Dung NGUYEN>, (C) 2019
// Copyright: See COPYING file that comes with this distribution
//
//
# ifndef _IPFIELD_H_
# define _IPFIELD_H_

#ifndef SWIG
#include <vector>
#include "quadratureRules.h"
#include "unknownField.h"
#include "elementsField.h"
#include "GModel.h"
#include "ipstate.h"
#include "partDomain.h"
#include "InterfaceKeys.h"


class TwoNum{
  public:
    int small, large;
    TwoNum(){ //define for restartManager
      small=0;
      large=0;
    }
    TwoNum(const int e1, const int e2){
      if (e1 < e2) {
        small = e1;
        large = e2;
      }
      else{
        small = e2;
        large = e1;
      }
    }
    TwoNum(const int e1): small(e1),large(e1){}
    TwoNum(const TwoNum& src): small(src.small),large(src.large){}
    ~TwoNum(){};
    TwoNum& operator = (const TwoNum& other) {
      small = other.small;
      large = other.large;
      return *this;
    };
    bool operator < (const TwoNum &other) const{
      if (small < other.small) return true;
      if (small > other.small) return false;
      if (large < other.large) return true;
      return false;
    }
    bool operator == (const TwoNum &other) const{
      return (small == other.small && large == other.large);
    }
    bool operator != (const TwoNum &other) const{
      return (small != other.small  || large != other.large);
    };
    /*void restart()
    {
      restartManager::restart(small);
      restartManager::restart(large);
      return;
    }*/

};

#endif //SWIG

class IPDataTransferMat
{
  public:
    #ifndef SWIG
    virtual ~IPDataTransferMat(){};
    virtual void transferData(const IPVariable* src, IPVariable* dest) const = 0;
    virtual IPDataTransferMat* clone() const = 0;
    #endif //SWIG
};

class EqualIPDataTransferMat : public IPDataTransferMat
{
  public:
    #ifndef SWIG
    EqualIPDataTransferMat(){}
    virtual ~EqualIPDataTransferMat(){};
    virtual void transferData(const IPVariable* src, IPVariable* dest) const
    {
      dest->operator=(*src);
    }
    virtual IPDataTransferMat* clone() const {return new EqualIPDataTransferMat();};
    #endif //SWIG
};

class IPDataTransfer
{
  #ifndef SWIG
  protected:
    std::map<int, IPDataTransferMat*> _allTransfers; // physical number of domain as key

  protected:
    const IPDataTransferMat* getIPDataTransferMat(int phys) const;
  #endif //SWIG
  public:
    IPDataTransfer(){}
    virtual ~IPDataTransfer();
    void add(int phys, const IPDataTransferMat* cl);
    #ifndef SWIG
    void transferDataIPv(int phys, const IPVariable* src, IPVariable* dest) const;
    bool withTranfer(int phys) const;
    #endif //SWIG
};

class IPField : public elementsField {
  public:
    enum  Output { SVM=0,SIG_XX, SIG_YY, SIG_ZZ, SIG_XY, SIG_YZ, SIG_XZ, DAMAGE, PLASTICSTRAIN, GL_EQUIVALENT_STRAIN,
                    STRAIN_XX, STRAIN_YY, STRAIN_ZZ, STRAIN_XY, STRAIN_YZ, STRAIN_XZ, HIGHSTRAIN_NORM, VOLUMETRIC_STRAIN,
                    HIGHSTRESS_NORM, GL_NORM, U_NORM, STRESS_NORM, P_STRESS_NORM,
                    U_XX, U_XY, U_XZ, U_YY, U_YZ, U_ZZ,
                    GL_XX, GL_XY, GL_XZ, GL_YY, GL_YZ, GL_ZZ,
                    F_XX, F_XY, F_XZ, F_YX, F_YY,F_YZ, F_ZX, F_ZY, F_ZZ,
                    P_XX, P_XY, P_XZ,P_YX, P_YY, P_YZ, P_ZX, P_ZY, P_ZZ,
                    P_XX_EFF, P_XY_EFF, P_XZ_EFF,P_YX_EFF, P_YY_EFF, P_YZ_EFF, P_ZX_EFF, P_ZY_EFF, P_ZZ_EFF,
                    PRESSION_EFF, SVM_EFF, STRESS_TRIAXIALITY_EFF,
                    // for VT case (VM or VLM)
                    // for each VT phase
                    // strain
                    STRAIN_XX_VTM01, STRAIN_XX_VTMT1, STRAIN_XX_VTMT2, STRAIN_XX_VTMT3, STRAIN_XX_VTMT4, STRAIN_XX_VTMT5, STRAIN_XX_VTMT6,
                    //
                    STRAIN_XX_VTLAM1, STRAIN_XX_VTLAM2, STRAIN_XX_VTLAM3, STRAIN_XX_VTLAM4, STRAIN_XX_VTLAM5, STRAIN_XX_VTLAM6,
                    STRAIN_XX_VTLAM7, STRAIN_XX_VTLAM8, STRAIN_XX_VTLAM9, STRAIN_XX_VTLAM10, STRAIN_XX_VTLAM11, STRAIN_XX_VTLAM12,
                    // stress
                    STRESS_XX_VTM01, STRESS_XX_VTMT1, STRESS_XX_VTMT2, STRESS_XX_VTMT3, STRESS_XX_VTMT4, STRESS_XX_VTMT5, STRESS_XX_VTMT6,
                    //
                    STRESS_XX_VTLAM1, STRESS_XX_VTLAM2, STRESS_XX_VTLAM3, STRESS_XX_VTLAM4, STRESS_XX_VTLAM5, STRESS_XX_VTLAM6,
                    STRESS_XX_VTLAM7, STRESS_XX_VTLAM8, STRESS_XX_VTLAM9, STRESS_XX_VTLAM10, STRESS_XX_VTLAM11, STRESS_XX_VTLAM12,
                    // damage
                    DAMAGE_VTM01,
                    // for each laminate ply
                    // strain
                    STRAIN_XX_VTLAM1_M0, STRAIN_XX_VTLAM1_MT, STRAIN_XX_VTLAM2_M0, STRAIN_XX_VTLAM2_MT, STRAIN_XX_VTLAM3_M0, STRAIN_XX_VTLAM3_MT,
                    STRAIN_XX_VTLAM4_M0, STRAIN_XX_VTLAM4_MT, STRAIN_XX_VTLAM5_M0, STRAIN_XX_VTLAM5_MT, STRAIN_XX_VTLAM6_M0, STRAIN_XX_VTLAM6_MT,
                    STRAIN_XX_VTLAM7_M0, STRAIN_XX_VTLAM7_MT, STRAIN_XX_VTLAM8_M0, STRAIN_XX_VTLAM8_MT, STRAIN_XX_VTLAM9_M0, STRAIN_XX_VTLAM9_MT,
                    STRAIN_XX_VTLAM10_M0, STRAIN_XX_VTLAM10_MT, STRAIN_XX_VTLAM11_M0, STRAIN_XX_VTLAM11_MT, STRAIN_XX_VTLAM12_M0, STRAIN_XX_VTLAM12_MT,
                    // stress
                    STRESS_XX_VTLAM1_M0, STRESS_XX_VTLAM1_MT, STRESS_XX_VTLAM2_M0, STRESS_XX_VTLAM2_MT, STRESS_XX_VTLAM3_M0, STRESS_XX_VTLAM3_MT,
                    STRESS_XX_VTLAM4_M0, STRESS_XX_VTLAM4_MT, STRESS_XX_VTLAM5_M0, STRESS_XX_VTLAM5_MT, STRESS_XX_VTLAM6_M0, STRESS_XX_VTLAM6_MT,
                    STRESS_XX_VTLAM7_M0, STRESS_XX_VTLAM7_MT, STRESS_XX_VTLAM8_M0, STRESS_XX_VTLAM8_MT, STRESS_XX_VTLAM9_M0, STRESS_XX_VTLAM9_MT,
                    STRESS_XX_VTLAM10_M0, STRESS_XX_VTLAM10_MT, STRESS_XX_VTLAM11_M0, STRESS_XX_VTLAM11_MT, STRESS_XX_VTLAM12_M0, STRESS_XX_VTLAM12_MT,
                    // damage
                    DAMAGE_VTLAM1_M0, DAMAGE_VTLAM2_M0, DAMAGE_VTLAM3_M0, DAMAGE_VTLAM4_M0, DAMAGE_VTLAM5_M0, DAMAGE_VTLAM6_M0,
                    DAMAGE_VTLAM7_M0, DAMAGE_VTLAM8_M0, DAMAGE_VTLAM9_M0, DAMAGE_VTLAM10_M0, DAMAGE_VTLAM11_M0, DAMAGE_VTLAM12_M0,
		    // for each material phase
		    // strain
                    STRAIN_XX_VTMT1_MTX, STRAIN_XX_VTMT1_INC, STRAIN_XX_VTMT2_MTX, STRAIN_XX_VTMT2_INC, STRAIN_XX_VTMT3_MTX, STRAIN_XX_VTMT3_INC,
                    STRAIN_XX_VTMT4_MTX, STRAIN_XX_VTMT4_INC, STRAIN_XX_VTMT5_MTX, STRAIN_XX_VTMT5_INC, STRAIN_XX_VTMT6_MTX, STRAIN_XX_VTMT6_INC,
                    //
                    STRAIN_XX_VTLAM1_MT_MTX, STRAIN_XX_VTLAM1_MT_INC, STRAIN_XX_VTLAM2_MT_MTX, STRAIN_XX_VTLAM2_MT_INC,
                    STRAIN_XX_VTLAM3_MT_MTX, STRAIN_XX_VTLAM3_MT_INC, STRAIN_XX_VTLAM4_MT_MTX, STRAIN_XX_VTLAM4_MT_INC,
                    STRAIN_XX_VTLAM5_MT_MTX, STRAIN_XX_VTLAM5_MT_INC, STRAIN_XX_VTLAM6_MT_MTX, STRAIN_XX_VTLAM6_MT_INC,
                    STRAIN_XX_VTLAM7_MT_MTX, STRAIN_XX_VTLAM7_MT_INC, STRAIN_XX_VTLAM8_MT_MTX, STRAIN_XX_VTLAM8_MT_INC,
                    STRAIN_XX_VTLAM9_MT_MTX, STRAIN_XX_VTLAM9_MT_INC, STRAIN_XX_VTLAM10_MT_MTX, STRAIN_XX_VTLAM10_MT_INC,
                    STRAIN_XX_VTLAM11_MT_MTX, STRAIN_XX_VTLAM11_MT_INC, STRAIN_XX_VTLAM12_MT_MTX, STRAIN_XX_VTLAM12_MT_INC,
                    // stress
                    STRESS_XX_VTMT1_MTX, STRESS_XX_VTMT1_INC, STRESS_XX_VTMT2_MTX, STRESS_XX_VTMT2_INC, STRESS_XX_VTMT3_MTX, STRESS_XX_VTMT3_INC,
                    STRESS_XX_VTMT4_MTX, STRESS_XX_VTMT4_INC, STRESS_XX_VTMT5_MTX, STRESS_XX_VTMT5_INC, STRESS_XX_VTMT6_MTX, STRESS_XX_VTMT6_INC,
                    //
                    STRESS_XX_VTLAM1_MT_MTX, STRESS_XX_VTLAM1_MT_INC, STRESS_XX_VTLAM2_MT_MTX, STRESS_XX_VTLAM2_MT_INC,
                    STRESS_XX_VTLAM3_MT_MTX, STRESS_XX_VTLAM3_MT_INC, STRESS_XX_VTLAM4_MT_MTX, STRESS_XX_VTLAM4_MT_INC,
                    STRESS_XX_VTLAM5_MT_MTX, STRESS_XX_VTLAM5_MT_INC, STRESS_XX_VTLAM6_MT_MTX, STRESS_XX_VTLAM6_MT_INC,
                    STRESS_XX_VTLAM7_MT_MTX, STRESS_XX_VTLAM7_MT_INC, STRESS_XX_VTLAM8_MT_MTX, STRESS_XX_VTLAM8_MT_INC,
                    STRESS_XX_VTLAM9_MT_MTX, STRESS_XX_VTLAM9_MT_INC, STRESS_XX_VTLAM10_MT_MTX, STRESS_XX_VTLAM10_MT_INC,
                    STRESS_XX_VTLAM11_MT_MTX, STRESS_XX_VTLAM11_MT_INC, STRESS_XX_VTLAM12_MT_MTX, STRESS_XX_VTLAM12_MT_INC,
                    // damage
                    DAMAGE_VTMT1_MTX, DAMAGE_VTMT1_INC, DAMAGE_VTMT2_MTX, DAMAGE_VTMT2_INC, DAMAGE_VTMT3_MTX, DAMAGE_VTMT3_INC,
                    DAMAGE_VTMT4_MTX, DAMAGE_VTMT4_INC, DAMAGE_VTMT5_MTX, DAMAGE_VTMT5_INC, DAMAGE_VTMT6_MTX, DAMAGE_VTMT6_INC,
                    //
                    DAMAGE_VTLAM1_MT_MTX, DAMAGE_VTLAM1_MT_INC, DAMAGE_VTLAM2_MT_MTX, DAMAGE_VTLAM2_MT_INC,
                    DAMAGE_VTLAM3_MT_MTX, DAMAGE_VTLAM3_MT_INC, DAMAGE_VTLAM4_MT_MTX, DAMAGE_VTLAM4_MT_INC,
                    DAMAGE_VTLAM5_MT_MTX, DAMAGE_VTLAM5_MT_INC, DAMAGE_VTLAM6_MT_MTX, DAMAGE_VTLAM6_MT_INC,
                    DAMAGE_VTLAM7_MT_MTX, DAMAGE_VTLAM7_MT_INC, DAMAGE_VTLAM8_MT_MTX, DAMAGE_VTLAM8_MT_INC,
                    DAMAGE_VTLAM9_MT_MTX, DAMAGE_VTLAM9_MT_INC, DAMAGE_VTLAM10_MT_MTX, DAMAGE_VTLAM10_MT_INC,
                    DAMAGE_VTLAM11_MT_MTX, DAMAGE_VTLAM11_MT_INC, DAMAGE_VTLAM12_MT_MTX, DAMAGE_VTLAM12_MT_INC,

                    // for LAM_2PLY case (LVM)
                    // for each laminate ply
                    // strain
                    STRAIN_XX_LAMM01, STRAIN_XX_LAMVT1,
                    // stress
                    STRESS_XX_LAMM01, STRESS_XX_LAMVT1,
                    // damage
                    DAMAGE_LAMM01,
                    // for each VT grain in laminate ply
                    // strain
                    STRAIN_XX_LAMVTMT1, STRAIN_XX_LAMVTMT2, STRAIN_XX_LAMVTMT3, STRAIN_XX_LAMVTMT4, STRAIN_XX_LAMVTMT5,
                    STRAIN_XX_LAMVTMT6, STRAIN_XX_LAMVTMT7, STRAIN_XX_LAMVTMT8, STRAIN_XX_LAMVTMT9, STRAIN_XX_LAMVTMT10,
                    STRAIN_XX_LAMVTMT11, STRAIN_XX_LAMVTMT12, STRAIN_XX_LAMVTMT13, STRAIN_XX_LAMVTMT14, STRAIN_XX_LAMVTMT15,
                    STRAIN_XX_LAMVTMT16, STRAIN_XX_LAMVTMT17, STRAIN_XX_LAMVTMT18, STRAIN_XX_LAMVTMT19, STRAIN_XX_LAMVTMT20,
                    // stress
                    STRESS_XX_LAMVTMT1, STRESS_XX_LAMVTMT2, STRESS_XX_LAMVTMT3, STRESS_XX_LAMVTMT4, STRESS_XX_LAMVTMT5,
                    STRESS_XX_LAMVTMT6, STRESS_XX_LAMVTMT7, STRESS_XX_LAMVTMT8, STRESS_XX_LAMVTMT9, STRESS_XX_LAMVTMT10,
                    STRESS_XX_LAMVTMT11, STRESS_XX_LAMVTMT12, STRESS_XX_LAMVTMT13, STRESS_XX_LAMVTMT14, STRESS_XX_LAMVTMT15,
                    STRESS_XX_LAMVTMT16, STRESS_XX_LAMVTMT17, STRESS_XX_LAMVTMT18, STRESS_XX_LAMVTMT19, STRESS_XX_LAMVTMT20,
                    // for each material phase
                    // strain
                    STRAIN_XX_LAMVTMT1_MTX, STRAIN_XX_LAMVTMT1_INC, STRAIN_XX_LAMVTMT2_MTX, STRAIN_XX_LAMVTMT2_INC,
                    STRAIN_XX_LAMVTMT3_MTX, STRAIN_XX_LAMVTMT3_INC, STRAIN_XX_LAMVTMT4_MTX, STRAIN_XX_LAMVTMT4_INC,
                    STRAIN_XX_LAMVTMT5_MTX, STRAIN_XX_LAMVTMT5_INC, STRAIN_XX_LAMVTMT6_MTX, STRAIN_XX_LAMVTMT6_INC,
                    STRAIN_XX_LAMVTMT7_MTX, STRAIN_XX_LAMVTMT7_INC, STRAIN_XX_LAMVTMT8_MTX, STRAIN_XX_LAMVTMT8_INC,
                    STRAIN_XX_LAMVTMT9_MTX, STRAIN_XX_LAMVTMT9_INC, STRAIN_XX_LAMVTMT10_MTX, STRAIN_XX_LAMVTMT10_INC,
                    STRAIN_XX_LAMVTMT11_MTX, STRAIN_XX_LAMVTMT11_INC, STRAIN_XX_LAMVTMT12_MTX, STRAIN_XX_LAMVTMT12_INC,
                    STRAIN_XX_LAMVTMT13_MTX, STRAIN_XX_LAMVTMT13_INC, STRAIN_XX_LAMVTMT14_MTX, STRAIN_XX_LAMVTMT14_INC,
                    STRAIN_XX_LAMVTMT15_MTX, STRAIN_XX_LAMVTMT15_INC, STRAIN_XX_LAMVTMT16_MTX, STRAIN_XX_LAMVTMT16_INC,
                    STRAIN_XX_LAMVTMT17_MTX, STRAIN_XX_LAMVTMT17_INC, STRAIN_XX_LAMVTMT18_MTX, STRAIN_XX_LAMVTMT18_INC,
                    STRAIN_XX_LAMVTMT19_MTX, STRAIN_XX_LAMVTMT19_INC, STRAIN_XX_LAMVTMT20_MTX, STRAIN_XX_LAMVTMT20_INC,
                    // stress
                    STRESS_XX_LAMVTMT1_MTX, STRESS_XX_LAMVTMT1_INC, STRESS_XX_LAMVTMT2_MTX, STRESS_XX_LAMVTMT2_INC,
                    STRESS_XX_LAMVTMT3_MTX, STRESS_XX_LAMVTMT3_INC, STRESS_XX_LAMVTMT4_MTX, STRESS_XX_LAMVTMT4_INC,
                    STRESS_XX_LAMVTMT5_MTX, STRESS_XX_LAMVTMT5_INC, STRESS_XX_LAMVTMT6_MTX, STRESS_XX_LAMVTMT6_INC,
                    STRESS_XX_LAMVTMT7_MTX, STRESS_XX_LAMVTMT7_INC, STRESS_XX_LAMVTMT8_MTX, STRESS_XX_LAMVTMT8_INC,
                    STRESS_XX_LAMVTMT9_MTX, STRESS_XX_LAMVTMT9_INC, STRESS_XX_LAMVTMT10_MTX, STRESS_XX_LAMVTMT10_INC,
                    STRESS_XX_LAMVTMT11_MTX, STRESS_XX_LAMVTMT11_INC, STRESS_XX_LAMVTMT12_MTX, STRESS_XX_LAMVTMT12_INC,
                    STRESS_XX_LAMVTMT13_MTX, STRESS_XX_LAMVTMT13_INC, STRESS_XX_LAMVTMT14_MTX, STRESS_XX_LAMVTMT14_INC,
                    STRESS_XX_LAMVTMT15_MTX, STRESS_XX_LAMVTMT15_INC, STRESS_XX_LAMVTMT16_MTX, STRESS_XX_LAMVTMT16_INC,
                    STRESS_XX_LAMVTMT17_MTX, STRESS_XX_LAMVTMT17_INC, STRESS_XX_LAMVTMT18_MTX, STRESS_XX_LAMVTMT18_INC,
                    STRESS_XX_LAMVTMT19_MTX, STRESS_XX_LAMVTMT19_INC, STRESS_XX_LAMVTMT20_MTX, STRESS_XX_LAMVTMT20_INC,
                    // damage
                    DAMAGE_LAMVTMT1_MTX, DAMAGE_LAMVTMT1_INC, DAMAGE_LAMVTMT2_MTX, DAMAGE_LAMVTMT2_INC,
                    DAMAGE_LAMVTMT3_MTX, DAMAGE_LAMVTMT3_INC, DAMAGE_LAMVTMT4_MTX, DAMAGE_LAMVTMT4_INC,
                    DAMAGE_LAMVTMT5_MTX, DAMAGE_LAMVTMT5_INC, DAMAGE_LAMVTMT6_MTX, DAMAGE_LAMVTMT6_INC,
                    DAMAGE_LAMVTMT7_MTX, DAMAGE_LAMVTMT7_INC, DAMAGE_LAMVTMT8_MTX, DAMAGE_LAMVTMT8_INC,
                    DAMAGE_LAMVTMT9_MTX, DAMAGE_LAMVTMT9_INC, DAMAGE_LAMVTMT10_MTX, DAMAGE_LAMVTMT10_INC,
                    DAMAGE_LAMVTMT11_MTX, DAMAGE_LAMVTMT11_INC, DAMAGE_LAMVTMT12_MTX, DAMAGE_LAMVTMT12_INC,
                    DAMAGE_LAMVTMT13_MTX, DAMAGE_LAMVTMT13_INC, DAMAGE_LAMVTMT14_MTX, DAMAGE_LAMVTMT14_INC,
                    DAMAGE_LAMVTMT15_MTX, DAMAGE_LAMVTMT15_INC, DAMAGE_LAMVTMT16_MTX, DAMAGE_LAMVTMT16_INC,
                    DAMAGE_LAMVTMT17_MTX, DAMAGE_LAMVTMT17_INC, DAMAGE_LAMVTMT18_MTX, DAMAGE_LAMVTMT18_INC,
                    DAMAGE_LAMVTMT19_MTX, DAMAGE_LAMVTMT19_INC, DAMAGE_LAMVTMT20_MTX, DAMAGE_LAMVTMT20_INC,
                    // body force
                    BODYFORCE_X, BODYFORCE_Y, BODYFORCE_Z,
                    dFmdFM_XXXX, dFmdFM_XXXY, dFmdFM_XXXZ, dFmdFM_XXYX, dFmdFM_XXYY, dFmdFM_XXYZ, dFmdFM_XXZX, dFmdFM_XXZY, dFmdFM_XXZZ,
                    dFmdFM_XYXX, dFmdFM_XYXY, dFmdFM_XYXZ, dFmdFM_XYYX, dFmdFM_XYYY, dFmdFM_XYYZ, dFmdFM_XYZX, dFmdFM_XYZY, dFmdFM_XYZZ,
                    dFmdFM_XZXX, dFmdFM_XZXY, dFmdFM_XZXZ, dFmdFM_XZYX, dFmdFM_XZYY, dFmdFM_XZYZ, dFmdFM_XZZX, dFmdFM_XZZY, dFmdFM_XZZZ,
                    dFmdFM_YXXX, dFmdFM_YXXY, dFmdFM_YXXZ, dFmdFM_YXYX, dFmdFM_YXYY, dFmdFM_YXYZ, dFmdFM_YXZX, dFmdFM_YXZY, dFmdFM_YXZZ,
                    dFmdFM_YYXX, dFmdFM_YYXY, dFmdFM_YYXZ, dFmdFM_YYYX, dFmdFM_YYYY, dFmdFM_YYYZ, dFmdFM_YYZX, dFmdFM_YYZY, dFmdFM_YYZZ,
                    dFmdFM_YZXX, dFmdFM_YZXY, dFmdFM_YZXZ, dFmdFM_YZYX, dFmdFM_YZYY, dFmdFM_YZYZ, dFmdFM_YZZX, dFmdFM_YZZY, dFmdFM_YZZZ,
                    dFmdFM_ZXXX, dFmdFM_ZXXY, dFmdFM_ZXXZ, dFmdFM_ZXYX, dFmdFM_ZXYY, dFmdFM_ZXYZ, dFmdFM_ZXZX, dFmdFM_ZXZY, dFmdFM_ZXZZ,
                    dFmdFM_ZYXX, dFmdFM_ZYXY, dFmdFM_ZYXZ, dFmdFM_ZYYX, dFmdFM_ZYYY, dFmdFM_ZYYZ, dFmdFM_ZYZX, dFmdFM_ZYZY, dFmdFM_ZYZZ,
                    dFmdFM_ZZXX, dFmdFM_ZZXY, dFmdFM_ZZXZ, dFmdFM_ZZYX, dFmdFM_ZZYY, dFmdFM_ZZYZ, dFmdFM_ZZZX, dFmdFM_ZZZY, dFmdFM_ZZZZ,
                    // stress strain
                    S_XX, S_XY, S_XZ, S_YY, S_YZ, S_ZZ,
                    G_XXX, G_XXY, G_XXZ, G_XYX, G_XYY, G_XYZ,G_XZX, G_XZY, G_XZZ,
                    G_YXX, G_YXY, G_YXZ, G_YYX, G_YYY, G_YYZ,G_YZX, G_YZY, G_YZZ,
                    G_ZXX, G_ZXY, G_ZXZ, G_ZYX, G_ZYY, G_ZYZ,G_ZZX, G_ZZY, G_ZZZ,
                    Q_XXX, Q_XXY, Q_XXZ, Q_XYX, Q_XYY, Q_XYZ,Q_XZX, Q_XZY, Q_XZZ,
                    Q_YXX, Q_YXY, Q_YXZ, Q_YYX, Q_YYY, Q_YYZ,Q_YZX, Q_YZY, Q_YZZ,
                    Q_ZXX, Q_ZXY, Q_ZXZ, Q_ZYX, Q_ZYY, Q_ZYZ,Q_ZZX, Q_ZZY, Q_ZZZ,
                    ISO_YIELD,ISO_YIELD_TENSILE, ISO_YIELD_COMPRESSION, ISO_YIELD_SHEAR,KIN_YIELD,
                    MTX_SVM,MTX_SIG_XX,MTX_SIG_YY,MTX_SIG_ZZ,MTX_SIG_XY,MTX_SIG_YZ,MTX_SIG_XZ,
                    INC_SVM,INC_SIG_XX,INC_SIG_YY,INC_SIG_ZZ,INC_SIG_XY,INC_SIG_YZ,INC_SIG_XZ,
                    TEMPERATURE,THERMALFLUX_X,THERMALFLUX_Y,THERMALFLUX_Z,THERMALSOURCE,
                    VOLTAGE,ELECTRICALFLUX_X,ELECTRICALFLUX_Y,ELECTRICALFLUX_Z,ELECTRICALSOURCE,
                    LOCAL_POROSITY, NONLOCAL_POROSITY, CORRECTED_POROSITY, YIELD_POROSITY,
                    NUCLEATED_POROSITY_TOT, NUCLEATED_POROSITY_0, NUCLEATED_POROSITY_1, NUCLEATED_POROSITY_2,
                    NUCLEATION_ONSET_0, NUCLEATION_ONSET_1, NUCLEATION_ONSET_2,
                    LIGAMENT_RATIO, PLASTIC_INSTABILITY_VAL,
                    PRESSION, PLASTIC_POISSON_RATIO,PLASTICSTRAIN_COMPRESSION, PLASTICSTRAIN_TRACTION, PLASTICSTRAIN_SHEAR,
                    NONLOCAL_PLASTICSTRAIN,BACKSTRESS, JACOBIAN, ELASTIC_JACOBIAN, PLASTIC_JACOBIAN,
                    PLASTIC_RATE,FLOW_FACTOR_RATE, OCTAHEDRAL_SHEAR_STRAIN_RATE, OCTAHEDRAL_NORM_STRAIN_RATE,
                    K_XX, K_YY, K_ZZ, K_XY, K_YZ, K_XZ, KVM,FAILURE_ONSET, GL_EQUIVALENT_STRAIN_RATE,
                    NONLOCAL_FAILURE_PLASTICSTRAIN, FAILURE_PLASTICSTRAIN, DAMAGE_0, DAMAGE_1, LOCAL_EQUIVALENT_STRAINS, NONLOCAL_EQUIVALENT_STRAINS,
                    FIRST_PRINCIPAL_STRESS, SECOND_PRINCIPAL_STRESS, THIRD_PRINCIPAL_STRESS, GL_EFFECTIVE_STRAIN_RATE,
                    DISP_JUMP_X,DISP_JUMP_Y,DISP_JUMP_Z,STRESS_TRIAXIALITY,ACTIVE_DISSIPATION,
                    FIELD_CAPACITY, MECHANICAL_SOURCE, FIELD_SOURCE,INCOMPATIBLE_STRAIN_X,INCOMPATIBLE_STRAIN_Y,INCOMPATIBLE_STRAIN_Z,
                    COHESIVE_JUMP_X,COHESIVE_JUMP_Y,COHESIVE_JUMP_Z,DAMAGE_IS_BLOCKED,
                    DEFO_ENERGY,PLASTIC_ENERGY,IRREVERSIBLE_ENERGY,DAMAGE_ENERGY,
                    MTX_STRAIN_XX,MTX_STRAIN_YY,MTX_STRAIN_ZZ,MTX_STRAIN_XY,MTX_STRAIN_YZ,MTX_STRAIN_XZ, MTX_DAMAGE,
                    INC_STRAIN_XX,INC_STRAIN_YY,INC_STRAIN_ZZ,INC_STRAIN_XY,INC_STRAIN_YZ,INC_STRAIN_XZ, INC_DAMAGE,
                    EXTRAFIELD_1,EXTRAFIELD_2,EXTRAFIELD_3,EXTRAFIELD_4,EXTRAFIELD_5,EXTRAFIELD_6,EXTRAFIELD_7,EXTRAFIELD_8,EXTRAFIELD_9,
                    EXTRAFIELD_NORM, DELETED,LOST_ELLIPTICITY,
                    LOCAL_0,LOCAL_1, LOCAL_2,LOCAL_3,LOCAL_4,LOCAL_5,LOCAL_6,LOCAL_7,LOCAL_8,LOCAL_9,
                    NONLOCAL_0, NONLOCAL_1, NONLOCAL_2, NONLOCAL_3, NONLOCAL_4, NONLOCAL_5, NONLOCAL_6, NONLOCAL_7, NONLOCAL_8, NONLOCAL_9, CONSTITUTIVEEXTRADOFDIFFUSION_0, CONSTITUTIVEEXTRADOFDIFFUSION_1, CONSTITUTIVEEXTRADOFDIFFUSION_2, FAILED,
                    COALESCENCE_CRITERION, COALESCENCE_ACTIVE, COALESCENCE, COALESCENCE_NECKING, COALESCENCE_SHEAR,POROSITY_COALESCENCE_ONSET,
                    ACTIVE_YIELD_0, ACTIVE_YIELD_1, ACTIVE_YIELD_2, YIELD_VALUE_0,  YIELD_VALUE_1,  YIELD_VALUE_2,
                    CL_XX, CL_YY, CL_ZZ, VOLUMETRIC_PLASTIC_STRAIN, LODE_PARAMETER, OMEGA_PARAMETER, LODE_ANGLE,DEVIATORIC_PLASTIC_STRAIN,
                    ASPECT_RATIO, SHAPE_FACTOR, LIGAMENT_RATIO_COALESCENCE_ONSET,ASPECT_RATIO_COALESCENCE_ONSET, SHAPE_FACTOR_COALESCENCE_ONSET,
                    LIMIT_LOAD_FACTOR,
                    INC_VOLUME_RATIO, INC_ASPECT_RATIO_1, INC_ASPECT_RATIO_2, INC_R_XX, INC_R_XY, INC_R_XZ, INC_R_YX, INC_R_YY, INC_R_YZ,
                    INC_R_ZX, INC_R_ZY, INC_R_ZZ,
                    //
                    FthG_XX,    FthG_YY,    FthG_ZZ,    FthG_XY,    FthG_YZ,    FthG_ZX,
                    FthR_XX,    FthR_YY,    FthR_ZZ,    FthR_XY,    FthR_YZ,    FthR_ZX,
                    FthI_XX,    FthI_YY,    FthI_ZZ,    FthI_XY,    FthI_YZ,    FthI_ZX,
#if 0
                    FthAI_XX,   FthAI_YY,   FthAI_ZZ,   FthAI_XY,   FthAI_YZ,   FthAI_ZX,
#endif
                    FthAD_XX,   FthAD_YY,   FthAD_ZZ,   FthAD_XY,   FthAD_YZ,   FthAD_ZX,
                    FfG_XX,     FfG_YY,     FfG_ZZ,     FfG_XY,     FfG_YZ,     FfG_ZX,
                    FpG_XX,     FpG_YY,     FpG_ZZ,     FpG_XY,     FpG_YZ,     FpG_ZX,
                    FveG_XX,    FveG_YY,     FveG_ZZ,     FveG_XY,     FveG_YZ,     FveG_ZX,
                    FP_XX,      FP_YY,     FP_ZZ,     FP_XY,     FP_YX,     FP_YZ,     FP_ZY,  FP_ZX,  FP_XZ,
                    FpR_XX,     FpR_YY,     FpR_ZZ,     FpR_XY,     FpR_YZ,     FpR_ZX,
                    FveR_XX,    FveR_YY,     FveR_ZZ,     FveR_XY,     FveR_YZ,     FveR_ZX,
                    epsG, ZG,         PDF,        TT, WT,
                    FthAM_XX,    FthAM_YY,    FthAM_ZZ,    FthAM_XY,    FthAM_YZ,    FthAM_ZX,
                    FfAM_XX,    FfAM_YY,    FfAM_ZZ,    FfAM_XY,    FfAM_YZ,    FfAM_ZX,
                    FpAM_XX,    FpAM_YY,    FpAM_ZZ,    FpAM_XY,    FpAM_YZ,    FpAM_ZX,
                    FveAM_XX,    FveAM_YY,    FveAM_ZZ,    FveAM_XY,    FveAM_YZ,    FveAM_ZX,
                    epsAM, epsR, Tcrys,  Tmelt, Wcrys, Wmelt, TRefR, TRefG, TRefAM,
#if 0
                    AGl_XX,     AGl_YY,     AGl_ZZ,     AGl_XY,     AGl_YZ,     AGl_ZX,
                    ARl_XX,     ARl_YY,     ARl_ZZ,     ARl_XY,     ARl_YZ,     ARl_ZX,
                    AAMl_XX,    AAMl_YY,    AAMl_ZZ,    AAMl_XY,    AAMl_YZ,    AAMl_ZX,
#endif
                    ADl_XX,     ADl_YY,     ADl_ZZ,     ADl_XY,     ADl_YZ,     ADl_ZX,
                    EnThG_XX,   EnThG_YY,   EnThG_ZZ,   EnThG_XY,   EnThG_YZ,   EnThG_ZX,
                    EnThR_XX,   EnThR_YY,   EnThR_ZZ,   EnThR_XY,   EnThR_YZ,   EnThR_ZX,
                    EnThAM_XX,  EnThAM_YY,  EnThAM_ZZ,  EnThAM_XY,  EnThAM_YZ,  EnThAM_ZX,
                    appliedEnergy, _SMPEnergy, _thermalEnergy, plasticDiss, freezedDiss, viscousDiss, EDiss,
                    //
                    Ee_XX, Ee_YY,  Ee_ZZ, Ee_XY, Ee_XZ, Ee_YZ, detEe,
                    Eve1_XX, Eve1_YY,  Eve1_ZZ, Eve1_XY, Eve1_XZ, Eve1_YZ,
                    Eve2_XX, Eve2_YY,  Eve2_ZZ, Eve2_XY, Eve2_XZ, Eve2_YZ,
                    corKir_XX, corKir_YY, corKir_ZZ, corKir_XY, corKir_XZ, corKir_YZ, mandel_XX, mandel_YY, mandel_ZZ, mandel_XY, mandel_XZ, mandel_YZ, mandel_YX, mandel_ZX, mandel_ZY, mandelCommuteChecker,
                    corKirExtra_XX, corKirExtra_YY, corKirExtra_ZZ, corKirExtra_XY, corKirExtra_XZ, corKirExtra_YZ,
                    Dev_corKir_Inf_XX, Dev_corKir_Inf_YY, Dev_corKir_Inf_ZZ, Dev_corKir_Inf_XY, Dev_corKir_Inf_XZ, Dev_corKir_Inf_YZ, Tr_corKir_Inf,
                    hyperElastic_BulkScalar,hyperElastic_ShearScalar,
                    USER0,USER1,USER2,USER3,USER4,USER5,USER6,USER7,USER8,USER9,USER10,
                    MAGNETICVECTORPOTENTIAL_X, MAGNETICVECTORPOTENTIAL_Y, MAGNETICVECTORPOTENTIAL_Z,
                    MAGNETICINDUCTION_X, MAGNETICINDUCTION_Y, MAGNETICINDUCTION_Z,
                    MAGNETICFIELD_X, MAGNETICFIELD_Y, MAGNETICFIELD_Z,
                    EMSOURCEVECTORFIELD_X,EMSOURCEVECTORFIELD_Y,EMSOURCEVECTORFIELD_Z,
                    INDUCTORSOURCEVECTORFIELD_X,INDUCTORSOURCEVECTORFIELD_Y,INDUCTORSOURCEVECTORFIELD_Z, EMFIELDSOURCE,
                    ELECDISPLACEMENT_X,ELECDISPLACEMENT_Y,ELECDISPLACEMENT_Z,
                    BROKEN,
                    EQUIVALENT_EIGENSTRESS,EQUIVALENT_EIGENSTRAIN,
                    EIGENSTRESS_XX, EIGENSTRESS_YY, EIGENSTRESS_ZZ, EIGENSTRESS_XY, EIGENSTRESS_XZ, EIGENSTRESS_YZ,
                    EIGENSTRAIN_XX, EIGENSTRAIN_YY, EIGENSTRAIN_ZZ, EIGENSTRAIN_XY, EIGENSTRAIN_XZ, EIGENSTRAIN_YZ,
                    // moduli scalers e.g. mullins effect
                    MAX_DEFO_ENERGY, MULLINS_DAMAGE,
                    PRESSURE,
                    // dissipated Energies
                    viscousDissipatedEnergy,mullinsDissipatedEnergy,
                    ONE, _EMEnergy,
                    //local damage DTensor43 (only 6 out of 81 components because we start in 1D and use the major symmetries)
                    DAMAGE_TENSOR43_0011,DAMAGE_TENSOR43_0022,DAMAGE_TENSOR43_1111,DAMAGE_TENSOR43_1122,DAMAGE_TENSOR43_2222,DAMAGE_TENSOR43_0101,
                    //plastic strain
                    EPLAST_XX, EPLAST_YY, EPLAST_ZZ, EPLAST_XY, EPLAST_XZ, EPLAST_YZ,
                    //elastic stiffness without damage (only 7 out 81 components because we start in 1D and use the major symmetries)
                    C_EL_0000,C_EL_0011,C_EL_0022,C_EL_1111,C_EL_1122,C_EL_2222,C_EL_0101,
                    //DStressDNonLocalVariable
                    DSIGMA_XX_DNONLOCALVAR,
                    //DLocalVariableDStrain (just epsilon_XX as we start in 1D)
                    DLOCALVAR_DEPSILON_XX,
                    //DStressreconstructedDStrain (we start in 1D so there are just 4 components for the reconstructed stress: XX, YY, ZZ, XY and one component for the strain: XX)
                    DSIGMAREC_XX_DEPSILON_XX, DSIGMAREC_YY_DEPSILON_XX,DSIGMAREC_ZZ_DEPSILON_XX,DSIGMAREC_XY_DEPSILON_XX,
                    //DEplastDE (we start in 1D so only 4 components for the plastic strain: Eplast_XX, Eplast_YY, Eplast_ZZ, Eplast_XY and 1 component for epsilon: epsilon_XX)
                    DEPLAST_XX_DEPSILON_XX, DEPLAST_YY_DEPSILON_XX, DEPLAST_ZZ_DEPSILON_XX, DEPLAST_XY_DEPSILON_XX,
                    UNDEFINED};
    enum  Operator { MEAN_VALUE=1, MIN_VALUE, MAX_VALUE, CRUDE_VALUE};
    static std::string ToString(const int i);
  #ifndef SWIG
    // Struct for archiving
    struct ip2archive{
      int tag;
      double value; // current value
      std::vector< MElement * > ele; // element where the point is situated (if node is common to several element the first one met is chosen. Normally OK continuity)
      MVertex* _v;  // = vertex if archiving on node
      int numgauss; // local gauss num
      int dim; // dim, equal -1 if get value from "numgauss" of element "numphys"
      int comp;  // component to archive == ipval
      int numphys; //

      int minusNum; // ele to archive
      int plusNum; // element to archive  minusNum= plusNum for bulk elements otherwise for interface element

      int nstep;
      std::vector<const partDomain *> domain; // address of domain needed to access IP
      nlsField::ElemValue evalue;
      FILE *fp;

      void openFile(const std::string prefix)
      {
        std::string fname;
        std::ostringstream oss;
        if (numphys > 0)
          oss << numphys;
        else{
          oss<< ele[0]->getNum();
        }
        std::string s = oss.str();
        oss.str("");
        //oss << comp;
        //std::string s2 = oss.str();
        std::string s2 = IPField::ToString(comp);
        switch(evalue){
         case nlsField::mean:
          oss.str("Mean");
          break;
         case nlsField::min:
          oss.str("Min");
          break;
         case nlsField::max:
          oss.str("Max");
          break;
         default:
          oss.str("");
        }

        std::string s3 = oss.str();
        #if defined(HAVE_MPI)
        if(Msg::GetCommSize() != 1){
          oss.str("");
          oss << Msg::GetCommRank();
          s3 += "_part"+oss.str();
        }
        #endif // HAVE_MPI

        if ( tag >=0)
        {
          std::string tagStr  = std::to_string(tag)+"_";
          if (dim == -1){
            std::ostringstream numgss;
            if (evalue == nlsField::crude)
              numgss << numgauss;
            fname = prefix+"IPShell"+tagStr+numgss.str()+"OnElement"+s+"val_"+s2+s3+".csv";
          }
          else if(dim == 0)
            fname = prefix+"IPShell"+tagStr+s+"val_"+s2+s3+".csv";
          else if(dim == 1)
            fname = prefix+"IPShellLine"+tagStr+s+"val_"+s2+s3+".csv";
          else if(dim == 2)
            fname = prefix+"IPShellFace"+tagStr+s+"val_"+s2+s3+".csv";
          else
            fname = prefix+"IPShellVolume"+tagStr+s+"val_"+s2+s3+".csv";
        }
        else
        {

          if (dim == -1){
            std::ostringstream numgss;
            if (evalue == nlsField::crude)
              numgss << numgauss;
            fname = prefix+"IP"+numgss.str()+"OnElement"+s+"val_"+s2+s3+".csv";
          }
          else if(dim == 0)
            fname = prefix+"IP"+s+"val_"+s2+s3+".csv";
          else if(dim == 1)
            fname = prefix+"IPLine"+s+"val_"+s2+s3+".csv";
          else if(dim == 2)
            fname = prefix+"IPFace"+s+"val_"+s2+s3+".csv";
          else
            fname = prefix+"IPVolume"+s+"val_"+s2+s3+".csv";
        }

        if (restartManager::available())
        {
          fp = fopen(fname.c_str(),"a");
        }
        else
        {
          fp = fopen(fname.c_str(),"w");
        }
      }

      void setTag(const int t) {tag = t;};

      // constructor for solver
      ip2archive(const int dim_,const int nphys_,const int ipval_,const int eval_,
                 const int nstep_, const int numgau=0) : dim(dim_), numphys(nphys_), comp(ipval_), nstep(nstep_),fp(NULL),// the other has to be defined later
                                     numgauss(numgau),minusNum(0),plusNum(0.), value(0.), _v(NULL), tag(-1)
      {
        // empty vectors (they will be filled later) clear to be sure
        ele.clear();
        domain.clear();
        switch(eval_){
         case 1:
          evalue=nlsField::mean;
          break;
         case 2:
          evalue=nlsField::min;
          break;
         case 3:
          evalue=nlsField::max;
          break;
         default:
          evalue=nlsField::crude;
        }

        if (dim == 0){
          elementGroup g(dim,numphys);
          elementGroup::vertexContainer::const_iterator itv = g.vbegin();
          _v = itv->second;
        }
      }

      ip2archive(const int ipval_,const int nstep_, MVertex* v): dim(0), numphys(v->getNum()), comp(ipval_), nstep(nstep_),fp(NULL),// the other has to be defined later
                                     numgauss(0),minusNum(0),plusNum(0.), value(0.), _v(v), evalue(nlsField::crude), tag(-1){
        ele.clear();
        domain.clear();
      };

      ip2archive(const int dim_,const int minusEle, const int plusEle,const int ipval_,const int eval_,
                 const int nstep_, const int numgau) : dim(dim_), numphys(0), comp(ipval_), nstep(nstep_),fp(NULL),// the other has to be defined later
                                     numgauss(numgau),minusNum(minusEle),plusNum(plusEle),value(0.), _v(NULL), tag(-1)
      {
        // empty vectors (they will be filled later) clear to be sure
        ele.clear();
        domain.clear();
        switch(eval_){
         case 1:
          evalue=nlsField::mean;
          break;
         case 2:
          evalue=nlsField::min;
          break;
         case 3:
          evalue=nlsField::max;
          break;
         default:
          evalue=nlsField::crude;
        }
      }

      ip2archive(const ip2archive &source) : ele(source.ele), domain(source.domain), fp(source.fp), numgauss(source.numgauss),
                                             numphys(source.numphys), comp(source.comp), evalue(source.evalue), dim(source.dim),
                                             nstep(source.nstep),plusNum(source.plusNum),minusNum(source.minusNum),value(source.value),
                                             _v(source._v), tag(source.tag) {}
      ~ip2archive(){
        if(fp!=NULL) fclose(fp);
        fp=NULL;
      }
    };
  protected :
    AllIPState *_AIPS;
    std::vector<ip2archive> viparch;
    nonLinearMechSolver* _solver; // solver to which ipField belongs
    //
    std::string _fileNamePrefixInterface;
    std::vector<dataBuildView>& _vBuildViewInterface;

    bool _withMultiscale; // for synchronisation MPI
    bool _withDissipation;  // for active dissipation
    bool _withCohesiveFailure; // for failure synchronisation MPI

    int _numOfActiveDissipationIPs;
    int _numOfActiveDissipationIPsReached;
    int _numOfNewlyBrokenElements;
    int _totalNumOfNewlyBrokenElements; // sum in all procs when using MPI

     // current step data
    bool _failureAndInternalAreChecked;
    bool _failureStateIsFlushed;

    // Cracked elements management
    std::set<int> _newlyBrokenElements;    // (buffer) all interface elements where crack insertion occurs at the current step
    std::set<int> _brokenElements;         // all interface broken element is stored in order to avoid rechecking
    // Blocked elements management
    std::set<int> _newlyBlockedDissipationElements;  // (buffer) all bulk elements where damage blocking occurs at the current step
    std::set<int> _blockedDissipationElements;       // all blocked element is stored in order to avoid rechecking/reblocking

    // for damage blockage
    // for multiscale analysis
    std::set<int> _blockedDissipationIPs;
    std::set<int> _brokenIPs;
    std::set<int> _solverBrokenIPs;

    std::set<int> _nonBrokenIPs; // part of _brokenIPs must be set to nonBroken
    std::set<int> _nonBlockedDissipationIPs; // part of _blockedDissipationIPs must be set to non dissipation

    // for saving
    int _lastSaveStep;
    int _numStepBetweenTwoSaves;

 protected:
  void checkInternalState();
  void checkFailure();
  virtual void buildDataInterface(FILE* myview, const nonLinearMechSolver* solver, const int cc, const nlsField::ElemValue ev, const int gp, const nlsField::ValType vt) const;
  virtual void buildAllView(const nonLinearMechSolver* solver ,const double time, const int numstep, const bool forceView);

 public :
  IPField(nonLinearMechSolver* sl, const std::vector<ip2archive> &vaip,
          std::vector<dataBuildView> &dbview_, std::vector<dataBuildView> &dbviewInterface_,
          std::string filename = "stress",  const std::string filenameInterface = "interfaceStress");
  virtual ~IPField();
  virtual void closeArchivingFiles();
  virtual void resetArchiving();
  virtual void archive(const double time,const int step, const bool forceView);
  double getIPValueOnPhysical(const int dim, const int physical, const int ipVal) const;

  AllIPState* getAips() {return _AIPS;}
  const AllIPState* getAips() const {return _AIPS;}

  const std::vector<partDomain*>& getDomainVector() const;


  bool compute1state(IPStateBase::whichState ws, bool stiff);
  void setDeformationGradientGradient(const STensor33 &GM,const IPStateBase::whichState ws);
  void setdFmdFM(std::map<Dof, STensor3> &dUdF, const IPStateBase::whichState ws);
  void setdFmdGM(std::map<Dof, STensor33> &dUdG, const IPStateBase::whichState ws);
  void setValuesForBodyForce(const IPStateBase::whichState ws);

  // num allows to select a particular gauss' point. Ins value is used only if ev = crude
  double getIPcompAtGP(const partDomain *ef,MElement *ele, const IPStateBase::whichState ws,
                   const int cmp, const int gp, const nlsField::ValType vt) const;

  // num allows to select a particular gauss' point. Ins value is used only if ev = crude
  double getIPcomp(const partDomain *ef,MElement *ele, const IPStateBase::whichState ws,
                   const int cmp,const nlsField::ElemValue ev, const int num, const nlsField::ValType vt) const;
  // vol return the associated volume
  double getIPcompAndVolume(const partDomain *ef,MElement *ele, const IPStateBase::whichState ws,
                   const int cmp,const nlsField::ElemValue ev, const nlsField::ValType vt, double &vol) const;
  // function to archive
  virtual void get(partDomain *dom, MElement *ele, std::vector<double> &val, const int cc,
                   const nlsField::ElemValue ev, const int gp, const nlsField::ValType vt)const{
    val[0]=  this->getIPcomp(dom,ele,IPStateBase::current,cc,ev,gp,vt);
  }

  // Interaction with Aips
  void copy(IPStateBase::whichState source, IPStateBase::whichState dest);

  void checkAll();
  bool isAllChecked() const {return _failureAndInternalAreChecked;}
  bool isFlushedFailureState() const {return _failureStateIsFlushed;};
  void flushFailureState();

  void nextStep(const double time=0.);
  int getNumOfActiveDissipationIPs() const {return _numOfActiveDissipationIPsReached;};
  int getNumOfActiveDissipationIPsCurrent() const {return _numOfActiveDissipationIPs;};
  int getLocalNumOfNewlyBrokenInterfaceElementInAllDomains() const {return _numOfNewlyBrokenElements;};
  int getNumOfNewlyBrokenInterfaceElementInAllDomains() const {return _totalNumOfNewlyBrokenElements;};

  void checkActiveDissipation();

  // initial broken
  void initialBroken(GModel* pModel, std::vector<std::pair<int,int> > &vnumphys);
  void initialBrokenDomain(GModel* pModel, std::vector<std::pair<int,int> > &twoPhys);
  void initialBroken(MElement *iele, materialLaw *mlaw);
  void setFactorOnArchivingFiles(const int fact);


  template<class T> void getIPv(const MElement *ele,const T** vipv, const IPStateBase::whichState ws= IPStateBase::current) const
  {
    AllIPState::ipstateElementContainer* vips = _AIPS->getIPstate(ele->getNum());
    for(int i=0; i<vips->size(); i++){
      IPStateBase* ips = (*vips)[i];
      vipv[i] = static_cast<const T*>(ips->getState(ws));
    }
  }
  template<class T> void getIPv(const MElement *ele,const T** vipv, const T** vipvprev) const
  {
    AllIPState::ipstateElementContainer *vips = _AIPS->getIPstate(ele->getNum());
    for(int i=0; i<vips->size(); i++){
      IPStateBase* ips = (*vips)[i];
      vipv[i] = static_cast<const T*>(ips->getState(IPStateBase::current));
      vipvprev[i] = static_cast<const T*>(ips->getState(IPStateBase::previous));
    }
  }
  template<class T1,class T2> void getIPv(const MInterfaceElement *iele, const T1** vipv_m, const T2** vipv_p,
                                          const IPStateBase::whichState ws=IPStateBase::current) const
  {
    const MElement* e = dynamic_cast<const MElement*>(iele);
    AllIPState::ipstateElementContainer *vips = _AIPS->getIPstate(e->getNum());
    // SAME NUMBER OF GAUSS POINTS ON BOTH SIDES
    int npts = vips->size()/2;
    for(int i=0;i<npts;i++){
      IPStateBase *ipsm = (*vips)[i];
      IPStateBase *ipsp = (*vips)[i+npts];
      vipv_m[i] = dynamic_cast<const T1*>(ipsm->getState(ws));
      vipv_p[i] = dynamic_cast<const T2*>(ipsp->getState(ws));
    }
  }
  template<class T1,class T2> void getIPv(const MInterfaceElement *iele, const T1** vipv_m, const T1** vipvprev_m,
                                             const T2** vipv_p, const T2** vipvprev_p) const
  {
    const MElement* e = dynamic_cast<const MElement*>(iele);
    AllIPState::ipstateElementContainer *vips = _AIPS->getIPstate(e->getNum());
    // SAME NUMBER OF GAUSS POINTS ON BOTH SIDES
    int npts = vips->size()/2;
    for(int i=0;i<npts;i++){
      IPStateBase *ipsm = (*vips)[i];
      IPStateBase *ipsp = (*vips)[i+npts];
      vipv_m[i] = static_cast<const T1*>(ipsm->getState(IPStateBase::current));
      vipv_p[i] = static_cast<const T2*>(ipsp->getState(IPStateBase::current));
      vipvprev_m[i] = static_cast<const T1*>(ipsm->getState(IPStateBase::previous));
      vipvprev_p[i] = static_cast<const T2*>(ipsp->getState(IPStateBase::previous));
    }
  }
  template<class T1,class T2> void getIPv(const MInterfaceElement *iele, T1** vipv_m, T2** vipv_p,
                                             const IPStateBase::whichState ws=IPStateBase::current) const
  {
    const MElement* e = dynamic_cast<const MElement*>(iele);
    AllIPState::ipstateElementContainer *vips = _AIPS->getIPstate(e->getNum());
    // SAME NUMBER OF GAUSS POINTS ON BOTH SIDES
    int npts = vips->size()/2;
    for(int i=0;i<npts;i++){
      IPStateBase *ipsm = (*vips)[i];
      IPStateBase *ipsp = (*vips)[i+npts];
      vipv_m[i] = static_cast<T1*>(ipsm->getState(ws));
      vipv_p[i] = static_cast<T2*>(ipsp->getState(ws));
    }
  }
  template<class T1,class T2> void getIPv(const MInterfaceElement *iele, T1** vipv_m, T2** vipvprev_m,
                                             T2** vipv_p, T2** vipvprev_p) const
  {
    const MElement* e = dynamic_cast<const MElement*>(iele);
    AllIPState::ipstateElementContainer *vips = _AIPS->getIPstate(e->getNum());
    // SAME NUMBER OF GAUSS POINTS ON BOTH SIDES
    int npts = vips->size()/2;
    for(int i=0;i<npts;i++){
      IPStateBase *ipsm = (*vips)[i];
      IPStateBase *ipsp = (*vips)[i+npts];
      vipv_m[i] = static_cast<T1*>(ipsm->getState(IPStateBase::current));
      vipv_p[i] = static_cast<T2*>(ipsp->getState(IPStateBase::current));
      vipvprev_m[i] = static_cast<T1*>(ipsm->getState(IPStateBase::previous));
      vipvprev_p[i] = static_cast<T2*>(ipsp->getState(IPStateBase::previous));
    }
  }


  template<class T1> void getIPv(const MInterfaceElement *iele, const T1** vipv_m,
                                   const IPStateBase::whichState ws=IPStateBase::current) const
  {
    const MElement* e = dynamic_cast<const MElement*>(iele);
    AllIPState::ipstateElementContainer *vips = _AIPS->getIPstate(e->getNum());
    // SAME NUMBER OF GAUSS POINTS ON BOTH SIDES
    int npts = vips->size();
    for(int i=0;i<npts;i++){
      IPStateBase *ipsm = (*vips)[i];
      vipv_m[i] = static_cast<const T1*>(ipsm->getState(ws));
    }
  }
  template<class T1> void getIPv(const MInterfaceElement *iele, const T1** vipv_m, const T1** vipvprev_m) const
  {
    const MElement* e = dynamic_cast<const MElement*>(iele);
    AllIPState::ipstateElementContainer *vips = _AIPS->getIPstate(e->getNum());
    // SAME NUMBER OF GAUSS POINTS ON BOTH SIDES
    int npts = vips->size();
    for(int i=0;i<npts;i++){
      IPStateBase *ipsm = (*vips)[i];
      vipv_m[i] = static_cast<const T1*>(ipsm->getState(IPStateBase::current));
      vipvprev_m[i] = static_cast<const T1*>(ipsm->getState(IPStateBase::previous));
    }
  }

  template<class T1> void getIPv(const MInterfaceElement *iele, T1** vipv_m,
                                   const IPStateBase::whichState ws=IPStateBase::current) const
  {
    const MElement* e = dynamic_cast<const MElement*>(iele);
    AllIPState::ipstateElementContainer *vips = _AIPS->getIPstate(e->getNum());
    int npts = vips->size();
    for(int i=0;i<npts;i++){
      IPStateBase *ipsm = (*vips)[i];
      vipv_m[i] = static_cast<T1*>(ipsm->getState(ws));
    }
  }
  template<class T1> void getIPv(const MInterfaceElement *iele, T1** vipv_m, T1** vipvprev_m) const
  {
    const MElement* e = dynamic_cast<const MElement*>(iele);
    AllIPState::ipstateElementContainer *vips = _AIPS->getIPstate(e->getNum());
    int npts = vips->size();
    for(int i=0;i<npts;i++){
      IPStateBase *ipsm = (*vips)[i];
      vipv_m[i] = static_cast<T1*>(ipsm->getState(IPStateBase::current));
      vipvprev_m[i] = static_cast<T1*>(ipsm->getState(IPStateBase::previous));
    }
  }
  double computeBulkDeformationEnergy(MElement *ele, const partDomain *dom, const IPStateBase::whichState ws) const;
  double computeInterfaceDeformationEnergy(MElement *ele, const dgPartDomain *dom, const IPStateBase::whichState ws) const;
  double computeDeformationEnergy(const IPStateBase::whichState ws) const;

  double computeBulkPlasticEnergy(MElement *ele, const partDomain *dom, const IPStateBase::whichState ws) const;
  double computeInterfacePlasticEnergy(MElement *ele, const dgPartDomain *dom, const IPStateBase::whichState ws) const;
  double computePlasticEnergy(const IPStateBase::whichState ws) const;

  double computeBulkDamageEnergy(MElement *ele, const partDomain *dom, const IPStateBase::whichState ws) const;
  double computeInterfaceDamageEnergy(MElement *ele, const dgPartDomain *dom, const IPStateBase::whichState ws) const;
  double computeDamageEnergy(const IPStateBase::whichState ws) const;

  double computeBulkPathFollowingLocalValue(MElement *ele, const partDomain *dom, const IPStateBase::whichState ws) const;
  double computeInterfacePathFollowingLocalValue(MElement *ele, const dgPartDomain *dom, const IPStateBase::whichState ws) const;
  double computePathFollowingLocalValue(const IPStateBase::whichState ws) const;

  int computeFractureEnergy(MElement *ele,const dgPartDomain *dom,double* arrayEnergy, const IPStateBase::whichState ws) const;
  int computeFractureEnergy(double * arrayEnergy,const IPStateBase::whichState ws) const;

  virtual void restart();

  void setTime(const double time, const double dtime, const int numstep);

  // block damage of problem
  void blockDissipation(const IPStateBase::whichState ws, const bool fl);

  // block dissipation at element level
  void addOneBlockedDissipationElement(MElement* ele);
  bool dissipationIsBlockedOnElement(MElement* ele) const;

  void addOneBrokenElement(MElement* ele);
  bool elementIsBroken(MElement* ele) const;

  void blockDissipationIP(const int ip);
  void brokenIP(const int ip);
  void solverBroken(const int ip);

  const std::set<int>& getConstRefToNonBrokenIPs() const{return _nonBrokenIPs;};
  std::set<int>& getRefToNonBrokenIPs() {return _nonBrokenIPs;};

  const std::set<int>& getConstRefToNonBlockedDissipationIPs() const{return _nonBlockedDissipationIPs;};
  std::set<int>& getRefToNonBlockedDissipationIPs() {return _nonBlockedDissipationIPs;};

  // _brokenIPs, _blockedDissipationIPs cannot be modifed outside class
  const std::set<int>& getConstRefToBrokenIPs() const{return _brokenIPs;};
  const std::set<int>& getConstRefToBlockedDissipationIPs() const{return _blockedDissipationIPs;};

  // new element
  const std::set<int>& getConstRefToNewlyBrokenElements() const {return _newlyBrokenElements;};
  std::set<int>& getRefToNewlyBrokenElements(){return _newlyBrokenElements;};
  //
  const std::set<int>& getConstRefToNewlyBlockedDissipationElements() const {return _newlyBlockedDissipationElements;};
  std::set<int>& getRefToNewlyBlockedDissipationElements() {return _newlyBlockedDissipationElements;};
  #endif // SWIG
};
#endif // IPField
