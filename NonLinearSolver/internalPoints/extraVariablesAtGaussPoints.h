//
// Description: storing class for nonlinear viscoelasticity
//
//
// Author:  <Dongli Li, Antoine Jerusalem>, (C) 2014
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef ExtraVariablesAtGaussPoints_H_
#define ExtraVariablesAtGaussPoints_H_
#include "ipvariable.h"
#include "STensor3.h"
#include <vector>


class ExtraVariablesAtGaussPoints
{
    bool filled;
    std::vector<SVector3> locations;
    std::vector< std::vector<double> > field;
    std::map <int,int> _storeExtraInputAtGp; //map between location in torch input and stored location in _ExtraVariablesAtGaussPoints
  public:
    ExtraVariablesAtGaussPoints():filled(false), field(),  _storeExtraInputAtGp() {}
    ExtraVariablesAtGaussPoints(const ExtraVariablesAtGaussPoints& src):filled(src.filled), locations(src.locations), field(src.field),  
                           _storeExtraInputAtGp(src._storeExtraInputAtGp) {}
    ExtraVariablesAtGaussPoints& operator = (const ExtraVariablesAtGaussPoints& src)
    {
      filled=src.filled; locations=src.locations; field=src.field;  _storeExtraInputAtGp=src._storeExtraInputAtGp;
      return *this;
    }
    ~ExtraVariablesAtGaussPoints(){}
    void fill(const std::string fname);
    const std::vector<double> &getFieldAtClosestLocation(const SVector3 &gp) const;  
    void print() const;
    void fillExtraInputAtGp(int nb1, int nb2)
    {
      if(_storeExtraInputAtGp.find(nb1) == _storeExtraInputAtGp.end())
        _storeExtraInputAtGp[nb1]=nb2;
      else
        Msg::Error("ExtraVariablesAtGaussPoints::fillExtraInputAtGp already defined");
    }
    bool isStored(int nb1) const
    {
      if(_storeExtraInputAtGp.find(nb1) == _storeExtraInputAtGp.end())
        return false;
      else
        return true;
    }
    double returnFieldAtClosestLocation(int nb1, const SVector3 &gp) const
    {
      if(!isStored(nb1))
        Msg::Error("ExtraVariablesAtGaussPoints::returnFieldAtClosestLocation field %d is not to be stored",nb1);
      std::map <int,int>::const_iterator it=_storeExtraInputAtGp.find(nb1);
      const std::vector<double> & field=getFieldAtClosestLocation(gp);
      if(it->second>field.size())
        Msg::Error("ExtraVariablesAtGaussPoints::returnFieldAtClosestLocation field %d is not stored at location %d",nb1,it->second);
      return field[it->second];
    }

};

#endif // ExtraVariablesAtGaussPoints_H_

