//
// Description: storing class for  nucleation
//
//
// Author:  <L. Noels>, (C) 2013
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "ipNucleation.h"
#include "restartManager.h"

#include "NucleationLaw.h"


// IPNucleation
// -------------------------------------------
IPNucleation::IPNucleation() : 
  nucleatedfV(0.0), DeltafV(0.0), DDeltafVDHatP(0.0)
{
  // default constructor is forbiden and send back an error
  ipFunctionVector_.clear();
  ipCriterionVector_.clear();
  
  Msg::Error( "IPNucleation:: default constructor should never be called." );
};



IPNucleation::IPNucleation( const NucleationLaw* nuclLaw ) :
  nucleatedfV(0.), DeltafV(0.), DDeltafVDHatP(0.)
{
  // After initialising its own variables, vectors are sized and filled.
  ipFunctionVector_.clear();
  ipCriterionVector_.clear();
  
  ipFunctionVector_.resize(nuclLaw->getNumberOfNucleationComponents());
  ipCriterionVector_.resize(nuclLaw->getNumberOfNucleationComponents());
  
  ipFunctionVector_.shrink_to_fit();
  ipCriterionVector_.shrink_to_fit();
  
  
  for ( int it_law = 0 ; it_law < nuclLaw->getNumberOfNucleationComponents() ; it_law++ ) {
    const NucleationFunctionLaw& functionLaw = nuclLaw->getConstRefToNucleationFunctionLaw(it_law);
    functionLaw.createIPVariable( ipFunctionVector_[it_law] );
    
    const NucleationCriterionLaw& criterionLaw = nuclLaw->getConstRefToNucleationCriterionLaw(it_law);
    criterionLaw.createIPVariable( ipCriterionVector_[it_law] );

  };

};



IPNucleation::IPNucleation( const IPNucleation& source ) :
  nucleatedfV(source.nucleatedfV), DeltafV(source.DeltafV), DDeltafVDHatP(source.DDeltafVDHatP)
{
  ipFunctionVector_.clear();
  ipCriterionVector_.clear();
  
  ipFunctionVector_.resize(source.ipFunctionVector_.size());
  ipCriterionVector_.resize(source.ipCriterionVector_.size());
  
  ipFunctionVector_.shrink_to_fit();
  ipCriterionVector_.shrink_to_fit();
  
  for ( int i = 0 ; i < source.ipFunctionVector_.size() ; i++ ){
    if( source.ipFunctionVector_[i] != NULL ){
      ipFunctionVector_[i] = source.ipFunctionVector_[i]->clone();
    }
    else{
      ipFunctionVector_[i] = NULL;
    }
  }
  
  for (int i = 0 ; i < source.ipCriterionVector_.size(); i++){
    if( source.ipCriterionVector_[i] != NULL ){
      ipCriterionVector_[i] = source.ipCriterionVector_[i]->clone();
    }
    else{
      ipCriterionVector_[i] = NULL;
    }
  }
  
};



IPNucleation& IPNucleation::operator=( const IPNucleation& source )
{
  nucleatedfV = source.nucleatedfV;
  DeltafV = source.DeltafV;
  DDeltafVDHatP = source.DDeltafVDHatP;
  
  //! \fixme is this complexity needed as objects members are always initialised and therefore, not null????
  if( source.ipFunctionVector_.size() == ipFunctionVector_.size() )
  {
    
    for ( int i = 0 ; i < source.ipFunctionVector_.size() ; i++ ) {
      if ( source.ipFunctionVector_[i] != NULL ) {
        if ( ipFunctionVector_[i] != NULL ) {
          ipFunctionVector_[i]->operator=( *source.ipFunctionVector_[i] );
        }
        else
          ipFunctionVector_[i] = source.ipFunctionVector_[i]->clone();
      }
      else{
        if ( ipFunctionVector_[i] != NULL ) {
          delete ipFunctionVector_[i]; 
          ipFunctionVector_[i] = NULL;
        }
      }
    }
  }
  else
  {
    for ( int i=0 ; i < ipFunctionVector_.size() ; i++ ) {
      if ( ipFunctionVector_[i] != NULL ) {
        delete ipFunctionVector_[i];
      }
    }
    
    ipFunctionVector_.resize( source.ipFunctionVector_.size() );
    for ( int i = 0 ; i < source.ipFunctionVector_.size() ; i++ ) {
      if ( source.ipFunctionVector_[i] != NULL ) {
        ipFunctionVector_[i]= source.ipFunctionVector_[i]->clone();
      }
      else{
        ipFunctionVector_[i] = NULL;
      }
    }
  };
  
  
  
  if( source.ipCriterionVector_.size() == ipCriterionVector_.size() ) {
    for ( int i = 0 ; i < source.ipCriterionVector_.size() ; i++ ) {
      if ( source.ipCriterionVector_[i] != NULL ) {
        if ( ipCriterionVector_[i] != NULL ) {
          ipCriterionVector_[i]->operator=( *source.ipCriterionVector_[i] );
        }
        else
          ipCriterionVector_[i] = source.ipCriterionVector_[i]->clone();
      }
      else{
        if ( ipCriterionVector_[i] != NULL ) {
          delete ipCriterionVector_[i]; 
          ipCriterionVector_[i] = NULL;
        }
      }
    }
  }
  else
  {
    for ( int i = 0 ; i<ipCriterionVector_.size() ; i++ ) {
      if ( ipCriterionVector_[i] != NULL ) {
        delete ipCriterionVector_[i];
      }
    }
    
    ipCriterionVector_.resize( source.ipCriterionVector_.size() );
    for ( int i = 0; i<source.ipCriterionVector_.size() ; i++ )
    {
      if ( source.ipCriterionVector_[i] != NULL ) {
        ipCriterionVector_[i] = source.ipCriterionVector_[i]->clone();
      }
      else{
        ipCriterionVector_[i] = NULL;
      }
    }
  };
  
  return *this;
};



IPNucleation::~IPNucleation()
{
  // Before deletion, the objects vectors needs to be cleaned.
  for ( int i = 0 ; i < ipFunctionVector_.size() ; i ++ ) {
    if ( ipFunctionVector_[i] != NULL ) {
      delete ipFunctionVector_[i]; 
      ipFunctionVector_[i] = NULL;
    }
  };
  
  for ( int i = 0 ; i < ipCriterionVector_.size() ; i ++ ) {
    if ( ipCriterionVector_[i] != NULL ){
      delete ipCriterionVector_[i]; 
      ipCriterionVector_[i] = NULL;
    }
  };

};
 


void IPNucleation::restart()
{
  restartManager::restart(nucleatedfV);
  restartManager::restart(DeltafV);
  restartManager::restart(DDeltafVDHatP);
  
  restartManager::restart(ipFunctionVector_);
  restartManager::restart(ipCriterionVector_);
  return;
};




