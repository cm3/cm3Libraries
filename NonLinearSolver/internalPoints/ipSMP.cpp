//
// Description: storing class for Linear Thermo Mechanics
//
//
// Author:  <Lina Homsy>, (C) 2013
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "ipSMP.h"
#include "mlawSMP.h"
#include "ipElecSMP.h"
#include "restartManager.h"

IPSMP::IPSMP(double _Sa1, double _phia, double _Sastar1):IPVariableMechanics(),_thermalEnergy(0.) , _epsilon1(0.),_SMPEnergy(0.)//: IPLinearThermoMechanics() //n
{
  STensorOperation::zero(Fp1);
  Fp1(0,0) = 1.;
  Fp1(1,1) = 1.;
  Fp1(2,2) = 1.;

  STensorOperation::zero(Fp2);
  Fp2(0,0) = 1.;
  Fp2(1,1) = 1.;
  Fp2(2,2) = 1.;

  Sa1=_Sa1;
  phia1=_phia;
  phiastar1=0.;
  Sastar1=_Sastar1;
};


IPSMP::IPSMP():IPVariableMechanics(), _thermalEnergy(0.) , _epsilon1(0.)// : IPLinearThermoMechanics()  //n
{
  STensorOperation::zero(Fp1);
  Fp1(0,0) = 1.;
  Fp1(1,1) = 1.;
  Fp1(2,2) = 1.;

  STensorOperation::zero(Fp2);
  Fp2(0,0) = 1.;
  Fp2(1,1) = 1.;
  Fp2(2,2) = 1.;

  Sa1=0.;
  phia1=0.;
  phiastar1=0.;
  Sastar1=0.;
  

  Msg::Info("creation of IPSMP without initialization");

};



IPSMP::IPSMP(const IPSMP &source) :IPVariableMechanics()//: IPLinearThermoMechanics(source)  //n
{
 _SMPEnergy=source._SMPEnergy;//n
  Fp1 = source.Fp1;
  Fp2 = source.Fp2;
  Sa1= source.Sa1;
  phia1= source.phia1;
  Sastar1= source.Sastar1;
  phiastar1= source.phiastar1;
  _thermalEnergy= source._thermalEnergy;
  _epsilon1= source._epsilon1;
  _SMPEnergy=source._SMPEnergy;
}


IPSMP& IPSMP::operator=(const IPVariable &source)
{
  IPVariableMechanics::operator=(source);
 //IPLinearThermoMechanics::operator=(source); //n

  const IPSMP* src = dynamic_cast<const IPSMP*>(&source);
  if(src)
  {
    Fp1 = src->Fp1;
    Fp2 = src->Fp2;
    Sa1 = src->Sa1;
    phia1 = src->phia1;
    Sastar1 = src->Sastar1;
    phiastar1 = src->phiastar1;
    _thermalEnergy = src->_thermalEnergy;
    _epsilon1=src->_epsilon1;
    _SMPEnergy=src->_SMPEnergy;
  }
  return *this;
}

/*double IPSMP::defoEnergy() const
{
  return _SMPEnergy;
//   return 0;
}*/


void IPSMP::restart()
{
  IPVariableMechanics::restart();
 // IPLinearThermoMechanics::restart();  //n
  restartManager::restart(Fp2);
  restartManager::restart(Fp1);
  restartManager::restart(Sa1);
  restartManager::restart(phia1);
  restartManager::restart(Sastar1);
  restartManager::restart(phiastar1);
  restartManager::restart(_SMPEnergy);
  restartManager::restart(_thermalEnergy);
  restartManager::restart(_epsilon1);
  return;
}
