//
// Created by vinayak on 05.07.22.
//

#include "ipGenericTM.h"

IPGenericTM::IPGenericTM() : IPCoupledThermoMechanics()
{
    _ipMeca=NULL;
    STensorOperation::unity(FthI0);
}

IPGenericTM::IPGenericTM(const IPGenericTM &source) :
IPCoupledThermoMechanics(source),
_ipMeca(NULL)
{
    if(source._ipMeca!=NULL)
    {
        if(_ipMeca!=NULL)
        {
            delete _ipMeca;
            _ipMeca=NULL;
        }
        _ipMeca = dynamic_cast<IPVariableMechanics *>(source.getConstRefToIpMeca().clone());
    }
    FthI0=source.FthI0;
}

IPGenericTM& IPGenericTM::operator=(const IPVariable &source)
{
    IPCoupledThermoMechanics::operator=(source);
    const IPGenericTM* src = dynamic_cast<const IPGenericTM*>(&source);
    if(src)
    {
        if(src->_ipMeca!=NULL)
        {
            if(_ipMeca!=NULL)
            {
                _ipMeca->operator=(dynamic_cast<const IPVariable &> (*(src->_ipMeca)));
            }
            else
                _ipMeca = dynamic_cast<IPVariableMechanics *>((src->getConstRefToIpMeca()).clone());
        }
        FthI0=src->FthI0;
    }
    return *this;
}

void IPGenericTM::restart()
{
    _ipMeca->restart();
    restartManager::restart(FthI0);
    IPCoupledThermoMechanics::restart();
}

double IPGenericTM::get(const int comp) const
{
   double val=IPCoupledThermoMechanics::get(comp);
   if(val==0.)
       val = _ipMeca->get(comp);
    return  val;
}

double & IPGenericTM::getRef(const int comp)
{
    double & val=IPCoupledThermoMechanics::getRef(comp);
    if(val==0.)
        val = _ipMeca->getRef(comp);
    return  val;
}