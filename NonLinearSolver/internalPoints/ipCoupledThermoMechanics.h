//
// Created by vinayak on 28.06.23.
//
// Author:  <Vinayak GHOLAP>, (C) 2023
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef IPCOUPLEDTHERMOMECHANICS_H_
#define IPCOUPLEDTHERMOMECHANICS_H_

#include "ipvariable.h"
#include "STensor3.h"
#include "STensorOperations.h"

class IPCoupledThermoMechanics : public IPVariableMechanics
{
protected:
    STensor3 referenceF;

public:
    double _thermalEnergy;
    IPCoupledThermoMechanics();
    virtual ~IPCoupledThermoMechanics(){}
    IPCoupledThermoMechanics(const IPCoupledThermoMechanics &source);
    virtual IPCoupledThermoMechanics& operator=(const IPVariable &source);

    virtual double getThermalEnergy() const{return _thermalEnergy;};
    virtual const STensor3 & getConstRefToReferenceF() const { return referenceF;}
    virtual STensor3 & getRefToReferenceF(){ return referenceF;}
    virtual void restart();
};


#endif //IPCOUPLEDTHERMOMECHANICS_H_
