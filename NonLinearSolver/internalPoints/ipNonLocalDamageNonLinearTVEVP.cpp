//
// C++ Interface: Material Law
//
// Description: IPNonLinearTVEVPNonLocalDamage (IP Thermo-ViscoElasto-ViscoPlasto Law) with Non-Local Damage Interface (Its here!)
//
// Author: <Ujwal Kishore J - FLE_Knight>, (C) 2024
//
// Copyright: See COPYING file that comes with this distribution
//
//

#include "ipNonLocalDamageNonLinearTVEVP.h"
#include "restartManager.h"

IPNonLinearTVEVPLocalDamage::IPNonLinearTVEVPLocalDamage(const J2IsotropicHardening* comp,
                    const J2IsotropicHardening* trac, const kinematicHardening* kin, const int N, const mullinsEffect* mullins,
                    const DamageLaw *daml): IPNonLinearTVP(comp,trac,kin,N,mullins),
                     _damageEnergy(0.)
{
  ipvDam=NULL;
  if(daml ==NULL) Msg::Error("IPNonLinearTVEVPLocalDamage::IPNonLinearTVEVPLocalDamage has no daml");
  daml->createIPVariable(ipvDam);

};

IPNonLinearTVEVPLocalDamage::~IPNonLinearTVEVPLocalDamage(){
  if(ipvDam !=NULL)
  {
    delete ipvDam;
  }
};


IPNonLinearTVEVPLocalDamage::IPNonLinearTVEVPLocalDamage(const IPNonLinearTVEVPLocalDamage &source):
      IPNonLinearTVP(source),_damageEnergy(source._damageEnergy){

  ipvDam = NULL;
  if(source.ipvDam != NULL)
  {
    ipvDam = dynamic_cast<IPDamage*>(source.ipvDam->clone());
  }
};
IPNonLinearTVEVPLocalDamage& IPNonLinearTVEVPLocalDamage::operator=(const IPVariable &source){
  IPNonLinearTVP::operator=(source);
  const IPNonLinearTVEVPLocalDamage* src = dynamic_cast<const IPNonLinearTVEVPLocalDamage*>(&source);
  if(src != NULL)
  {
    _damageEnergy = src->_damageEnergy;

    if(src->ipvDam != NULL)
    {
			if (ipvDam!= NULL)
				ipvDam->operator=(*dynamic_cast<const IPVariable*>(src->ipvDam));
			else
				ipvDam=dynamic_cast<IPDamage*>(src->ipvDam->clone());
    }
		else{
			if(ipvDam != NULL) delete ipvDam; ipvDam = NULL;
		}
  }
  return *this;
  };

void IPNonLinearTVEVPLocalDamage::restart()
{
  IPNonLinearTVP::restart();
  restartManager::restart(ipvDam);
  restartManager::restart(_damageEnergy);
  return;
}

// ################################################################## END CLASS 1 #################################################################


IPNonLinearTVEVPNonLocalDamage::IPNonLinearTVEVPNonLocalDamage(const J2IsotropicHardening* comp,
                    const J2IsotropicHardening* trac, const kinematicHardening* kin, const int N, const mullinsEffect* mullins,
                    const CLengthLaw *cll, const DamageLaw *daml): IPNonLinearTVEVPLocalDamage(comp,trac,kin,N,mullins,daml),
                    _nonlocalPlasticStrain(0.), _DirreversibleEnergyDNonLocalVariable(0.)
{
  ipvCL=NULL;
  if(cll ==NULL) Msg::Error("IPNonLinearTVEVPNonLocalDamage::IPNonLinearTVEVPNonLocalDamage has no cll");
  cll->createIPVariable(ipvCL);
};

IPNonLinearTVEVPNonLocalDamage::~IPNonLinearTVEVPNonLocalDamage(){
  if(ipvCL != NULL)
  {
    delete ipvCL;
  }
};

IPNonLinearTVEVPNonLocalDamage::IPNonLinearTVEVPNonLocalDamage(const IPNonLinearTVEVPNonLocalDamage &source):
      IPNonLinearTVEVPLocalDamage(source), _nonlocalPlasticStrain(source._nonlocalPlasticStrain),
      _DirreversibleEnergyDNonLocalVariable(source._DirreversibleEnergyDNonLocalVariable){
  ipvCL = NULL;
  if(source.ipvCL != NULL)
  {
    ipvCL = dynamic_cast<IPCLength*>(source.ipvCL->clone());
  }
};
IPNonLinearTVEVPNonLocalDamage& IPNonLinearTVEVPNonLocalDamage::operator=(const IPVariable &source){
  IPNonLinearTVEVPLocalDamage::operator=(source);
  const IPNonLinearTVEVPNonLocalDamage* src = dynamic_cast<const IPNonLinearTVEVPNonLocalDamage*>(&source);
  if(src != NULL)
  {
    _nonlocalPlasticStrain=src->_nonlocalPlasticStrain;
    _DirreversibleEnergyDNonLocalVariable = src->_DirreversibleEnergyDNonLocalVariable;
    if(src->ipvCL != NULL)
    {
			if (ipvCL != NULL){
				ipvCL->operator=(*dynamic_cast<const IPCLength*>(src->ipvCL));
			}
			else
				ipvCL= dynamic_cast<IPCLength*>(src->ipvCL->clone());
    }
		else{
			if(ipvCL != NULL) delete ipvCL; ipvCL = NULL;
		}

  }
  return *this;
};

void IPNonLinearTVEVPNonLocalDamage::restart()
{
  IPNonLinearTVEVPLocalDamage::restart();
  restartManager::restart(ipvCL);
  restartManager::restart(_nonlocalPlasticStrain);
  restartManager::restart(_DirreversibleEnergyDNonLocalVariable);
};

// ################################################################## END CLASS 2 #################################################################

IPNonLinearTVEVPMultipleLocalDamage::IPNonLinearTVEVPMultipleLocalDamage(const J2IsotropicHardening* comp,
                const J2IsotropicHardening* trac,const kinematicHardening* kin,
                const int N,  const mullinsEffect* mullins, const std::vector<DamageLaw*>& dl): IPNonLinearTVP(comp,trac,kin,N,mullins),
                _damageEnergy(0.){
  numNonLocalVariable = dl.size();
  ipvDam.clear();
  for (int i=0; i< dl.size(); i++){
    IPDamage* ipvD = NULL;
    dl[i]->createIPVariable(ipvD);
    ipvDam.push_back(ipvD);
  }
};
IPNonLinearTVEVPMultipleLocalDamage::~IPNonLinearTVEVPMultipleLocalDamage(){
  for (int i=0; i< numNonLocalVariable; i++){
    delete ipvDam[i];
  }
  ipvDam.clear();
};
IPNonLinearTVEVPMultipleLocalDamage::IPNonLinearTVEVPMultipleLocalDamage(const IPNonLinearTVEVPMultipleLocalDamage& source):
  IPNonLinearTVP(source), _damageEnergy(source._damageEnergy){
  numNonLocalVariable = source.numNonLocalVariable;

  ipvDam.clear();
  for (int i=0; i< numNonLocalVariable; i++){
    if(source.ipvDam[i] != NULL){
      ipvDam.push_back(dynamic_cast<IPDamage*>(source.ipvDam[i]->clone()));
    }
  }
};
IPNonLinearTVEVPMultipleLocalDamage& IPNonLinearTVEVPMultipleLocalDamage::operator=(const IPVariable& source){
  IPNonLinearTVP::operator=(source);
  const IPNonLinearTVEVPMultipleLocalDamage* ipsource = dynamic_cast<const IPNonLinearTVEVPMultipleLocalDamage*>(&source);
  if (ipsource != NULL){
    numNonLocalVariable = ipsource->numNonLocalVariable;
    _damageEnergy = ipsource->_damageEnergy;
    for (int i=0; i< ipsource->numNonLocalVariable; i++){
      if(ipsource->ipvDam[i] != NULL){
        if (ipvDam[i] != NULL){
          ipvDam[i]->operator=(*dynamic_cast<const IPVariable*>(ipsource->ipvDam[i]));
        }
      }
    }
  }
  return *this;
};


void IPNonLinearTVEVPMultipleLocalDamage::restart(){
  IPNonLinearTVP::restart();
  restartManager::restart(numNonLocalVariable);
  restartManager::restart(_damageEnergy);
  for (int i=0; i< numNonLocalVariable; i++){
    ipvDam[i]->restart();
  }
};

// ################################################################## END CLASS 3 #################################################################


IPNonLinearTVEVPMultipleNonLocalDamage::IPNonLinearTVEVPMultipleNonLocalDamage(const J2IsotropicHardening* comp,
                const J2IsotropicHardening* trac, const kinematicHardening* kin,
                const int N,  const mullinsEffect* mullins, const std::vector<CLengthLaw*>& cll, const std::vector<DamageLaw*>& dl):
                IPNonLinearTVEVPMultipleLocalDamage(comp,trac,kin,N,mullins,dl),
                _nonlocalEqPlasticStrain(0.),_nonlocalFailurePlasticity(0.){
  ipvCL.clear();
  _DirreversibleEnergyDNonLocalVariable.clear();
  for (int i=0; i< cll.size(); i++){
    IPCLength* ipvLength=NULL;
    cll[i]->createIPVariable(ipvLength);
    ipvCL.push_back(ipvLength);
    _DirreversibleEnergyDNonLocalVariable.push_back(0.);
  }
};
IPNonLinearTVEVPMultipleNonLocalDamage::~IPNonLinearTVEVPMultipleNonLocalDamage(){
  for (int i=0; i< numNonLocalVariable; i++){
    delete ipvCL[i];
  }
  ipvCL.clear();
};
IPNonLinearTVEVPMultipleNonLocalDamage::IPNonLinearTVEVPMultipleNonLocalDamage(const IPNonLinearTVEVPMultipleNonLocalDamage& source):
  IPNonLinearTVEVPMultipleLocalDamage(source){
  _nonlocalEqPlasticStrain= source._nonlocalEqPlasticStrain;
  _nonlocalFailurePlasticity = source._nonlocalFailurePlasticity;
  _DirreversibleEnergyDNonLocalVariable = source._DirreversibleEnergyDNonLocalVariable;
  ipvCL.clear();
  for (int i=0; i< numNonLocalVariable; i++){
    if(source.ipvCL[i] != NULL){
      ipvCL.push_back(dynamic_cast<IPCLength*>(source.ipvCL[i]->clone()));
    }
  }
};
IPNonLinearTVEVPMultipleNonLocalDamage& IPNonLinearTVEVPMultipleNonLocalDamage::operator=(const IPVariable& source){
  IPNonLinearTVEVPMultipleLocalDamage::operator=(source);
  const IPNonLinearTVEVPMultipleNonLocalDamage* ipsource = dynamic_cast<const IPNonLinearTVEVPMultipleNonLocalDamage*>(&source);
  if (ipsource != NULL){
    _nonlocalEqPlasticStrain = ipsource->_nonlocalEqPlasticStrain;
    _nonlocalFailurePlasticity = ipsource->_nonlocalFailurePlasticity;
    _DirreversibleEnergyDNonLocalVariable = ipsource->_DirreversibleEnergyDNonLocalVariable;

    for (int i=0; i< ipsource->numNonLocalVariable; i++){
      if(ipsource->ipvCL[i] != NULL){
        if (ipvCL[i] != NULL){
          ipvCL[i]->operator=(*dynamic_cast<const IPCLength*>(ipsource->ipvCL[i]));
        }
      }
    }
  }
  return *this;
};


void IPNonLinearTVEVPMultipleNonLocalDamage::restart(){
  IPNonLinearTVEVPMultipleLocalDamage::restart();
  restartManager::restart(_nonlocalEqPlasticStrain);
  restartManager::restart(_nonlocalFailurePlasticity);
  for (int i=0; i< numNonLocalVariable; i++){
    restartManager::restart(_DirreversibleEnergyDNonLocalVariable[i]);
  }
  for (int i=0; i< numNonLocalVariable; i++){
    ipvCL[i]->restart();
  }
};


// ################################################################## END CLASS 4 #################################################################
