//
// Description: storing class for transverse Isotropic law it is a "virtual implementation" you have to define an ipvariable
//              in your project which derive from this one. (all data in this ipvarible have name beggining by _j2l...)
//              so don't do the same in your project...
// Author:  <L. WU>, (C) 2017
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef IPANISOTROPICSTOCH_H_
#define IPANISOTROPICSTOCH_H_
#include "ipvariable.h"
#include "STensor3.h"

class IPAnisotropicStoch : public IPVariableMechanics
{
 protected:
  double Ex, Ey, Ez;
  double Vxy, Vxz, Vyz;
  double MUxy, MUxz, MUyz;
  double _elasticEnergy;
 public:
  IPAnisotropicStoch(const SVector3 &GaussP, const fullMatrix<double> &ExMat, const fullMatrix<double> &EyMat, const fullMatrix<double> &EzMat, 
                     const fullMatrix<double> &VxyMat, const fullMatrix<double> &VxzMat, const fullMatrix<double> &VyzMat, 
                     const fullMatrix<double> &MUxyMat, const fullMatrix<double> &MUxzMat, const fullMatrix<double> &MUyzMat, 
                     const double dx, const double dy, const double OrigX, const double OrigY, const int intpl);
  IPAnisotropicStoch(const IPAnisotropicStoch &source);
  IPAnisotropicStoch& operator=(const IPVariable &source);
  virtual double defoEnergy() const;
  virtual void restart();
  virtual IPVariable* clone() const {return new IPAnisotropicStoch(*this);};

  double getEx() const { return Ex;}
  double getEy() const { return Ey;}
  double getEz() const { return Ez;}
  double getVxy() const { return Vxy;}
  double getVxz() const { return Vxz;}
  double getVyz() const { return Vyz;}
  double getMUxy() const { return MUxy;}
  double getMUxz() const { return MUxz;}
  double getMUyz() const { return MUyz;}

  virtual void setElasticEnergy(const double Ev) {_elasticEnergy=Ev;}

 private:
  IPAnisotropicStoch();
};

#endif // IPANISOTROPICSTOCH_H_


