//
// C++ Interface: terms
//
// Description: Class to store internal variables at gauss point
//
//
// Author:  <Gauthier BECKER>, (C) 2010
//
// Copyright: See COPYING file that comes with this distribution
//
//

#include "ipstate.h"
#include "partDomain.h"
#include "MInterfaceElement.h"
#include "MElement.h"
#include "nonLinearMechSolver.h"
#include "numericalMaterial.h"
#include "DeepMaterialNetworks.h"

#if defined(HAVE_MPI)
#include <mpi.h>
#endif // HAVE_MPI

IPStateBase::~IPStateBase(){
	if (_solver) {delete _solver; _solver = NULL;}
}

void IPStateBase::copy(const IPStateBase::whichState ws1, const IPStateBase::whichState ws2)
{
  IPVariable *ipv1= this->getState(ws1);
  IPVariable *ipv2= this->getState(ws2);
  if (ipv1 != ipv2)
  {
    *ipv2 =*ipv1;
  }
  // if DMN is present in IP
  if (ipv1->withDeepMN())
  {
    std::vector<DeepMaterialNetwork*> allDMNs;
    ipv1->getAllDeepMNs(allDMNs);
    for (int j=0; j< allDMNs.size(); j++)
    {
      allDMNs[j]->copy(ws1,ws2);
    }
  }
};

IP1State::~IP1State(){
  if(_current) {delete _current; _current = NULL;};
}

IP1State::IP1State(const IP1State &source) : IPStateBase(source)
{
	_current = NULL;
	if (source._current){
		_current = source._current->clone();
	}
}

IP1State & IP1State::operator = (const IPStateBase &source){
  IPStateBase::operator=(source);
  const IP1State *src = dynamic_cast<const IP1State*>(&source);
	if (src != NULL){
		if (src->_current){
			if (_current){
				_current->operator= (*src->_current);
			}
			else{
				_current = src->_current->clone();
			}

		}
	}
  return *this;
}

IPVariable* IP1State::getState(const whichState wst) const
{
  return _current;
}

void IP1State::restart()
{
  _current->restart();
}


IP3State::~IP3State(){
  if(_initial) {delete _initial; _initial=NULL;}
  if(_step2) {delete _step2; _step2=NULL;}
  if(_step1) {delete _step1; _step1 = NULL;}
	if (_temp) {delete _temp; _temp = NULL;};
	if (_activeDissipation) {delete _activeDissipation; _activeDissipation = NULL;};
}

IP3State::IP3State(const IP3State &source) : IPStateBase(source)
{
	_initial = NULL;
	if (source._initial){
		_initial = source._initial->clone();
	}
	_step1 = NULL;
	if (source._step1){
		_step1 = source._step1->clone();
	}
	_step2 = NULL;
	if (source._step2){
		_step2 = source._step2->clone();
	}
  _st = source._st;
	_temp = NULL;
	_activeDissipation = NULL;
}

IP3State & IP3State::operator = (const IPStateBase &source){
  IPStateBase::operator=(source);
  const IP3State *src = dynamic_cast<const IP3State*>(&source);
	if (src != NULL){
		if (src->_initial){
			if (_initial){
				_initial->operator=(*src->_initial);
			}
			else{
				_initial = src->_initial->clone();
			}
		}

		if (src->_step1){
			if (_step1){
				_step1->operator= (*src->_step1);
			}
			else{
				_step1 = src->_step1->clone();
			}
		}
		if (src->_step2){
			if (_step2){
				_step2->operator=(*src->_step2);
			}
			else{
				_step2 = src->_step2->clone();
			}
		}
		_st = src->_st;
	}
  return *this;
}

IPVariable* IP3State::getState(const whichState wst) const
{
  switch(wst){
    case temp :
      if (_temp == NULL){
        if (_initial)
          _temp = _initial->clone();
        else if (_step1)
          _temp = _step1->clone();
        else if (_step2)
          _temp = _step2->clone();
        else
          Msg::Error("this IP3State is not initialized");
      }
      return _temp;
      break;
		case activeDissipation :
			if (_activeDissipation == NULL){
				if (_initial)
					_activeDissipation = _initial->clone();
				else if (_step1)
					_activeDissipation = _step1->clone();
				else if (_step2)
					_activeDissipation = _step2->clone();
				else
					Msg::Error("this IP3State is not initialized");
			}
			return _activeDissipation;
			break;
    case initial :
      return _initial;
      break;
    case previous :
      if(*_st) return _step1; else return _step2;
      break;
    case current :
      if(*_st) return _step2; else return _step1;
      break;
    default : Msg::Error("Impossible to select the desired state for internal variable \n");
			return NULL;
  }
}

void IP3State::restart()
{
  IPVariable *ipv = this->getState(IPStateBase::initial);
  ipv->restart();
  ipv = this->getState(IPStateBase::previous);
  ipv->restart();
  ipv = this->getState(IPStateBase::current);
  ipv->restart();
}


AllIPState::AllIPState(std::vector<partDomain*> &vdom) : _domainVector(vdom)
{
  state = true; // at creation of object state is true
  isMultiscale = false;
  std::map<int,materialLaw*> allNumericLaw;
  #if defined(HAVE_MPI)
  int rootRank = 0;
  std::set<int> otherRanks;
  #endif //HAVE_MPI
  for(std::vector<partDomain*>::iterator itdom=vdom.begin(); itdom!=vdom.end(); ++itdom){
    partDomain *dom = *itdom;
    materialLaw* mlaw = dom->getMaterialLaw();
    if(mlaw!=NULL)
    {
      if (mlaw->isNumeric()){
        allNumericLaw[mlaw->getNum()] = mlaw;
      }
    }
    dgPartDomain* dgdom = dynamic_cast<dgPartDomain*>(dom);
    if (dgdom!= NULL){
      materialLaw* mlawPlus = dgdom->getMaterialLawPlus();
      materialLaw* mlawMinus = dgdom->getMaterialLawMinus();
      if(mlawMinus!=NULL)
      {
        if (allNumericLaw.find(mlawMinus->getNum())== allNumericLaw.end() and  mlawMinus->isNumeric()){
          allNumericLaw[mlawMinus->getNum()] = mlawMinus;
        }
      }

      if(mlawPlus!=NULL)
      {
        if (allNumericLaw.find(mlawPlus->getNum())== allNumericLaw.end() and  mlawPlus->isNumeric()){
          allNumericLaw[mlawPlus->getNum()] = mlawPlus;
        }
      }


    }
    #if defined(HAVE_MPI)
    if (otherRanks.size()==0){
      rootRank = dom->getRootRank();
      dom->getOtherRanks(otherRanks);
    }
    if (rootRank == Msg::GetCommRank())
    #endif
    {
      dom->initMicroMeshId();
    }
  };

  if (allNumericLaw.size()>0){
    isMultiscale = true;
  }

  #if defined(HAVE_MPI)
  for (std::map<int,materialLaw*>::iterator it = allNumericLaw.begin(); it!= allNumericLaw.end(); it++){
    materialLaw* mlaw = it->second;
    numericalMaterialBase* nummat = dynamic_cast<numericalMaterialBase*>(mlaw);
    if (rootRank == Msg::GetCommRank()){
      int bufferSize= nummat->getNumberValuesMPI();
      int* buffer=NULL;
      if (bufferSize>0){
        buffer = new int[bufferSize];
        nummat->fillValuesMPI(buffer);
      }
      for (std::set<int>::iterator itR = otherRanks.begin(); itR != otherRanks.end(); itR++){
        int other=*itR;
        int tag1 = numericalMaterialBase::createTypeWithTwoInts(mlaw->getNum(),other);
        MPI_Send(&bufferSize,1,MPI_INT,other,tag1,MPI_COMM_WORLD);
        if (bufferSize>0){
          int tag2 = numericalMaterialBase::createTypeWithTwoInts(mlaw->getNum(),other+1);
          MPI_Send(buffer,bufferSize,MPI_INT,other,tag2,MPI_COMM_WORLD);
        }
      }
      if (bufferSize>0){
        delete buffer;
      }
    }
    else if (otherRanks.find(Msg::GetCommRank()) != otherRanks.end()){
      int bufferSize = 0;
      int tag1 = numericalMaterialBase::createTypeWithTwoInts(mlaw->getNum(),Msg::GetCommRank());
      MPI_Status status;
      MPI_Recv(&bufferSize,1,MPI_INT,rootRank,tag1,MPI_COMM_WORLD,&status);
      if (bufferSize>0){
        int* buffer = new int[bufferSize];
        int tag2 = numericalMaterialBase::createTypeWithTwoInts(mlaw->getNum(),Msg::GetCommRank()+1);
        MPI_Recv(buffer,bufferSize,MPI_INT,rootRank,tag2,MPI_COMM_WORLD,&status);
        nummat->getValuesMPI(buffer,bufferSize);
        delete buffer;
      }
    }
		#ifdef _DEBUG
    nummat->printMeshIdMap();
		#endif //_DEBUG
  }

  if (isMultiscale){
    printf("init all mesh id for micro problems  in proc %d \n",Msg::GetCommRank());
    Msg::Barrier();
  }
  #endif//HAVE_MPI
  
  if (Msg::GetCommRank() == 0)
  {
    for (std::map<int,materialLaw*>::iterator it = allNumericLaw.begin(); it!= allNumericLaw.end(); it++){
      materialLaw* mlaw = it->second;
      numericalMaterialBase* nummat = dynamic_cast<numericalMaterialBase*>(mlaw);
      nummat->printMeshIdMap();
    }
  }

  for(std::vector<partDomain*>::iterator itdom=vdom.begin(); itdom!=vdom.end(); ++itdom){
    partDomain *dom = *itdom;
    #if defined(HAVE_MPI)
    dom->createIPMap();
    #endif // HAVE_MPI
    dom->createIPState(_mapall,&state);
  }


 #if defined(HAVE_MPI)
  if(Msg::GetCommSize()>1) // maybe that some values have to be exchanged between interDomain!
  {
    // Communication with the domain in 2 times. First to known the number of value to exchanged
    // to size the array and a second to fill the array
    std::map<dgPartDomain*,int*> interSize;
    for(std::vector<partDomain*>::iterator itdom=vdom.begin(); itdom!=vdom.end(); ++itdom)
    {
      partDomain *dom = *itdom;
      if(dom->IsInterfaceTerms())
      {
        dgPartDomain *dgdom = static_cast<dgPartDomain*>(dom);
        if(dgdom->elementGroupSize() ==0) // otherwise the domain is not an interface domain
        {
           int *sizeeachrank = new int[Msg::GetCommSize()];
           for(int i=0;i<Msg::GetCommSize();i++)
              sizeeachrank[i]=0;
           dgdom->numberValuesToTransfertMPI(this,sizeeachrank);
           bool vex=false;
           for(int i=0;i<Msg::GetCommSize();i++)
           {
             if(sizeeachrank[i]!=0)
             {
               vex = true;
               break;
             }
           }
           if(vex)
           {
             interSize.insert(std::pair<dgPartDomain*,int*>(dgdom,sizeeachrank));
           }
           else
           {
             delete[] sizeeachrank;
           }
        }
      }
    }

    // compute the total size to communicate to each rank
    int *allsize = new int[Msg::GetCommSize()];
    for(int i=0;i<Msg::GetCommSize();i++)
      allsize[i] = 0;
    for(std::map<dgPartDomain*,int*>::iterator it=interSize.begin(); it!=interSize.end();++it)
    {
      for(int i=0;i<Msg::GetCommSize();i++)
        if(it->second[i]>0)
          allsize[i] += (it->second[i]+1); // as we give in first argument the domain num
    }
    double *arrayMPI;
    int arraympisize=0;
    MPI_Status mpistatus;
    for(int i=1;i<Msg::GetCommSize();i++) // no comm with node 0 (we take the data from the lowest rank)
    {
      if(Msg::GetCommRank() < i) // communicate data from this rank to rank i
      {
        if(allsize[i]!=0) // otherwise no data to transfert
        {
          if(arraympisize<allsize[i])
          {
            if(arraympisize!=0) delete[] arrayMPI;
            arraympisize = allsize[i];
            arrayMPI = new double[arraympisize];
          }
          for(int k=0; k<arraympisize; k++) arrayMPI[k]=0.;
          // fill the vector to communicate to rank i
          int curpos=0;
          for(std::map<dgPartDomain*,int*>::iterator it=interSize.begin(); it!=interSize.end();++it)
          {
            if(it->second[i]!=0){
              int val=it->first->getPhysical();
              //Msg::Error("Domain %d", val);
              arrayMPI[curpos] = ((double)(val)); //it->first->getPhysical();
              //Msg::Error("Send from proc %d to proc %d Domain %d %f", Msg::GetCommRank(), i, val, arrayMPI[curpos]);
              it->first->fillArrayIPvariableMPI(this,i,&arrayMPI[curpos+1]);
              curpos+=(it->second[i]+1); // as we communicate the number of domain in argument 0
            }
          }
          MPI_Send(arrayMPI,allsize[i],MPI_DOUBLE,i,Msg::GetCommRank(),MPI_COMM_WORLD);
        } 
      }
      else if(Msg::GetCommRank() == i) // recieved the data from rank i
      {
        for(int j=0;j<i;j++){
          if(allsize[j]!=0){
            // array dimension
            if(arraympisize<allsize[j])
            {
              if(arraympisize!=0) delete[] arrayMPI;
              arraympisize = allsize[j];
              arrayMPI = new double[arraympisize];
            }
            for(int k=0; k<arraympisize; k++) arrayMPI[k]=0.;
            // receieve the data
            MPI_Recv(arrayMPI,allsize[j],MPI_DOUBLE,j,j,MPI_COMM_WORLD,&mpistatus);
            // set data in each interDomain
            int curpos=0;
            while(curpos<allsize[j])
            {
              double val=arrayMPI[curpos];
              int domphys = ((int)(val));
              //Msg::Error("Domain %f %d", arrayMPI[curpos], domphys);
              // get the domain
              dgPartDomain *idom=NULL;
              int domsize=0;
              //Msg::Error("Receive from proc %d to proc %d Domain %d %f", j, Msg::GetCommRank(), domphys,val);
              for(std::map<dgPartDomain*,int*>::iterator it=interSize.begin(); it!=interSize.end();++it)
              {

                if(domphys == it->first->getPhysical())
                {
                  idom = it->first;
                  domsize = it->second[j];
                  break;
                }
              }
              if(idom!=NULL){
                idom->setIPVariableFromArrayMPI(this,j,domsize,&arrayMPI[curpos+1]);
                curpos+= (1+domsize);
              }
              else
              {
                Msg::Error("Domain %d to communicate ipvariable from rank %d seems not existing on rank %d", domphys,j,Msg::GetCommRank());
                Msg::Exit(0);
              }
            }
          }
        }
      }
    }

    // clear previously allocated array
    if(arraympisize!=0) delete[] arrayMPI;
    delete[] allsize;
    for(std::map<dgPartDomain*,int*>::iterator it=interSize.begin(); it!=interSize.end();++it)
    {
      delete[] it->second;
    }
  }

 #endif // HAVE_MPI
}
AllIPState::~AllIPState()
{
  for(ipstateContainer::iterator it=_mapall.begin();it!=_mapall.end();++it){
    ipstateElementContainer vips = (*it).second;
    for(int i=0;i<vips.size();i++)
	  delete vips[i];
  }
};

AllIPState::ipstateElementContainer* AllIPState::getIPstate(const long int num)
{

  ipstateContainer::iterator it = _mapall.find(num);
 #ifdef _DEBUG
  if(it == _mapall.end()){
    Msg::Error("Try to get an inexisting IPState on rank %d",Msg::GetCommRank());
    return NULL;
  }
  else
  #endif // _DEBUG
  {
    return &(it->second);
  }
}

AllIPState::ipstateElementContainer* AllIPState::operator[](const long int num)
{
  return &(_mapall.find(num)->second);
}

const AllIPState::ipstateElementContainer* AllIPState::getIPstate(const long int num) const
{

  ipstateContainer::const_iterator it = _mapall.find(num);
 #ifdef _DEBUG
  if(it == _mapall.end()){
    Msg::Error("Try to get an inexisting IPState on rank %d",Msg::GetCommRank());
    return NULL;
  }
  else
  #endif // _DEBUG
  {
    return &(it->second);
  }
}

const AllIPState::ipstateElementContainer* AllIPState::operator[](const long int num) const
{
  return &(_mapall.find(num)->second);
}

void AllIPState::restart()
{
  for(ipstateContainer::iterator its=_mapall.begin(); its!=_mapall.end();++its)
  {
    ipstateElementContainer& vips = its->second;
    for(ipstateElementContainer::iterator it=vips.begin(); it!=vips.end();++it)
    {
       IPStateBase* ips = *it;
       ips->restart();
    }
  }
}

void AllIPState::setTime(const double time, const double dtime, const int numstep){
  if (isMultiscale){
    for(ipstateContainer::iterator its=_mapall.begin(); its!=_mapall.end();++its)
    {
      ipstateElementContainer& vips = its->second;
      for(ipstateElementContainer::iterator it=vips.begin(); it!=vips.end();++it)
      {
         IPStateBase* ips = *it;
         if (ips->getMicroSolver() != NULL){
           ips->getMicroSolver()->setTime(time,dtime,numstep);
         }
      }
    }

  }
};

void AllIPState::copy(const IPStateBase::whichState ws1, const IPStateBase::whichState ws2){
  for(ipstateContainer::iterator it=_mapall.begin(); it!=_mapall.end();++it){
    std::vector<IPStateBase*> *vips = &((*it).second);
    for(int i=0;i<vips->size();i++)
    {
      (*vips)[i]->copy(ws1,ws2);
    }
  }
}

void AllIPState::nextStep() {
  this->copy(IPStateBase::current,IPStateBase::previous);

	if (isMultiscale){
		for (ipstateContainer::iterator it = _mapall.begin(); it!= _mapall.end(); it++){
			std::vector<IPStateBase*> *vips = &((*it).second);
			for (int i = 0; i< vips->size(); i++){
				nonLinearMechSolver* solver = (*vips)[i]->getMicroSolver();
				if (solver!= NULL){
					solver->prepareForNextMicroSolve();
				}
			}
		}
	}
}
