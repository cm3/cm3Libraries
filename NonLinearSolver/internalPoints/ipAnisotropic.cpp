//
// Description: storing class for j2 linear elasto-plastic law
//
//
// Author:  <Gauthier BECKER>, (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "ipAnisotropic.h"
#include "restartManager.h"
#include "ipField.h"
IPAnisotropic::IPAnisotropic() : IPVariableMechanics(), _elasticEnergy(0.), _P(0.), _sig(0.), _strain(0.) {};
IPAnisotropic::IPAnisotropic(const IPAnisotropic &source) : IPVariableMechanics(source), _elasticEnergy(source._elasticEnergy), _P(source._P), _sig(source._sig), _strain(source._strain) {}
IPAnisotropic& IPAnisotropic::operator=(const IPVariable &source)
{
  IPVariableMechanics::operator=(source);
  const IPAnisotropic* src = dynamic_cast<const IPAnisotropic*>(&source);
	if (src!=NULL){
		_elasticEnergy = src->_elasticEnergy;
		_P = src->_P;
		_sig = src->_sig;
		_strain = src->_strain;
	}
	return *this;
}

double IPAnisotropic::defoEnergy() const
{
  return _elasticEnergy;
}


double IPAnisotropic::get(int comp) const
{
  if(comp == IPField::SIG_XX)
  {
    const STensor3 tensor = _sig;
    double val = tensor(0,0);
    return val;
  }
  else if(comp == IPField::SIG_YY)
  {
    const STensor3 tensor = _sig;
    double val = tensor(1,1);
    return val;
  }
  else if(comp == IPField::SIG_ZZ)
  {
    const STensor3 tensor = _sig;
    double val = tensor(2,2);
    return val;
  }
  else if(comp == IPField::SIG_XY)
  {
    const STensor3 tensor = _sig;
    double val = tensor(0,1);
    return val;
  }
  else if(comp == IPField::SIG_XZ)
  {
    const STensor3 tensor = _sig;
    double val = tensor(0,2);
    return val;
  }
  else if(comp == IPField::SIG_YZ)
  {
    const STensor3 tensor = _sig;
    double val = tensor(1,2);
    return val;
  } 
  else if(comp == IPField::P_XX)
  {
    const STensor3 tensor = _P;
    double val = tensor(0,0);
    return val;
  }
  else if(comp == IPField::P_YY)
  {
    const STensor3 tensor = _P;
    double val = tensor(1,1);
    return val;
  }
  else if(comp == IPField::P_ZZ)
  {
    const STensor3 tensor = _P;
    double val = tensor(2,2);
    return val;
  }
  else if(comp == IPField::P_XY)
  {
    const STensor3 tensor = _P;
    double val = tensor(0,1);
    return val;
  }
  else if(comp == IPField::P_XZ)
  {
    const STensor3 tensor = _P;
    double val = tensor(0,2);
    return val;
  }
  else if(comp == IPField::P_YZ)
  {
    const STensor3 tensor = _P;
    double val = tensor(1,2);
    return val;
  }
  else if(comp == IPField::STRAIN_XX)
  {
    const STensor3 tensor = _strain;
    double val = tensor(0,0);
    return val;
  }
  else if(comp == IPField::STRAIN_YY)
  {
    const STensor3 tensor = _strain;
    double val = tensor(1,1);
    return val;
  }
  else if(comp == IPField::STRAIN_ZZ)
  {
    const STensor3 tensor = _strain;
    double val = tensor(2,2);
    return val;
  }
  else if(comp == IPField::STRAIN_XY)
  {
    const STensor3 tensor = _strain;
    double val = tensor(0,1);
    return val;
  }
  else if(comp == IPField::STRAIN_XZ)
  {
    const STensor3 tensor = _strain;
    double val = tensor(0,2);
    return val;
  }
  else if(comp == IPField::STRAIN_YZ)
  {
    const STensor3 tensor = _strain;
    double val = tensor(1,2);
    return val;
  }
  else
  {
    return 0.;
  }
}


void IPAnisotropic::restart()
{
  IPVariableMechanics::restart();
  restartManager::restart(_elasticEnergy);
  restartManager::restart(_P);
  restartManager::restart(_sig);
  restartManager::restart(_strain);
  return;
}

