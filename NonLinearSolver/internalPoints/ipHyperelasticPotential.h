//
// C++ Interface: ipvariable
//
// Description: IP for hyperelastic potential
//
// Author:  V.D. Nguyen, (C) 2023
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef IPHYPERELASTICPOTENTIAL_H_
#define IPHYPERELASTICPOTENTIAL_H_

#include "ipvariable.h"

class IPHyperelasticPotential : public IPVariableMechanics
{
  protected:
    double _defoEnergy;
  public:
    IPHyperelasticPotential(): IPVariableMechanics(), _defoEnergy(0){};
    IPHyperelasticPotential(const IPHyperelasticPotential& src): IPVariableMechanics(src), _defoEnergy(src._defoEnergy){};
    IPHyperelasticPotential& operator =(const IPVariable& src)
    {
      IPVariableMechanics::operator =(src);
      const IPHyperelasticPotential* psrc = dynamic_cast<const IPHyperelasticPotential*>(&src);
      if (psrc != NULL)
      {
        _defoEnergy = psrc->_defoEnergy;
      }
      return *this;
    };
    virtual ~IPHyperelasticPotential(){}
    
    virtual double defoEnergy() const {return _defoEnergy;};
    virtual double& getRefToDeforEnergy() {return _defoEnergy;};
    
    virtual  IPVariable * clone() const{return new IPHyperelasticPotential(*this);}
    virtual void restart()
    {
      restartManager::restart(_defoEnergy);
    }
};

#endif //IPHYPERELASTICPOTENTIAL_H_