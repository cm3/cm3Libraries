//
// Created by vinayak on 17.08.22.
//
//
// Description: storing class for Electro-Magnetic Generic-Thermo-Mechanics
//
//
// Author:  <Vinayak GHOLAP>, (C) 2022
//
// Copyright: See COPYING file that comes with this distribution
//
//

#include "ipElecMagGenericThermoMech.h"
#include "ipField.h"

IPElecMagGenericThermoMech::IPElecMagGenericThermoMech():
IPVariable(),_EMEnergy(0.0)
{
    _ipThermoMech = NULL;
}

IPElecMagGenericThermoMech::IPElecMagGenericThermoMech(const IPElecMagGenericThermoMech& source):
IPVariable(source),
_ipThermoMech(NULL)
{
    if(source._ipThermoMech != NULL)
    {
        if(_ipThermoMech != NULL)
        {
            delete _ipThermoMech;
            _ipThermoMech = NULL;
        }
        _ipThermoMech = dynamic_cast<IPCoupledThermoMechanics*>(source.getConstRefToIpThermoMech().clone());
    }
    _EMEnergy = source._EMEnergy;
}

IPElecMagGenericThermoMech& IPElecMagGenericThermoMech::operator=(const IPVariable& source)
{
    IPVariable::operator=(source);
    const IPElecMagGenericThermoMech* src= dynamic_cast<const IPElecMagGenericThermoMech*>(&source);
    if(src)
    {
        if(src->_ipThermoMech!=NULL)
        {
            if(_ipThermoMech!=NULL)
            {
                _ipThermoMech->operator=(dynamic_cast<const IPVariable&>(*(src->_ipThermoMech)));
            }
            else
                _ipThermoMech = dynamic_cast<IPCoupledThermoMechanics*>((src->getConstRefToIpThermoMech()).clone());
        }
        _EMEnergy = src->_EMEnergy;
    }
    return  *this;
}

double IPElecMagGenericThermoMech::get(const int comp) const
{
    double val=IPVariable::get(comp);
    if(comp == IPField::_EMEnergy)     {return defoEnergy();}
    if(val==0.)
        val = _ipThermoMech->get(comp);
    return  val;
}

double & IPElecMagGenericThermoMech::getRef(const int comp)
{
    double & val=IPVariable::getRef(comp);
    if(comp == IPField::_EMEnergy)     {return getRefToDefoEnergy();}
    if(val==0.)
        val = _ipThermoMech->getRef(comp);
    return  val;
}

void IPElecMagGenericThermoMech::restart()
{
    IPVariable::restart();
    _ipThermoMech->restart();
    restartManager::restart(_EMEnergy);
}