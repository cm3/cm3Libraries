//
// Description: storing class for EOS
//
//
// Author:  <Dongli Li, Antoine Jerusalem>, (C) 2014
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "ipEOS.h"
#include "restartManager.h"

IPEOS::IPEOS(double elementSize) : _elementSize(elementSize)
{

}

IPEOS::IPEOS(double elementSize, IPVariable *IPV) : _elementSize(elementSize)
{
    _IPV = IPV; //ipvariable instance for deviatoric material law
}

IPEOS::IPEOS(const IPEOS &source) : _elementSize(source._elementSize)
{
    _IPV = source._IPV;
}

IPEOS& IPEOS::operator=(const IPVariable &source)
{
    IPVariableMechanics::operator=(source);
    const IPEOS* src = dynamic_cast<const IPEOS*>(&source);
    if(src!=NULL)
    {

        _IPV = src->_IPV;

    }
    return *this;
}

void IPEOS::restart()
{
    IPVariableMechanics::restart();
    restartManager::restart(_IPV);
    restartManager::restart(_elementSize);
    return;
}

