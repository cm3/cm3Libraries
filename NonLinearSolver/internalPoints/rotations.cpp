#ifndef ROTATIONS_C 
#define ROTATIONS_C 1


/*
 *
 * VERSION 30 septembre 2008
 * opérations sur les quaternions testées en comparant avec le programme de Maxime (~/Maxime/..) 	-> ok 
 * changement de repère pour un tenseur symétrique 3x3 testé: convention LD vs convention ID vs Matlab ->ok
	======================================================================
	Définition des angles d''Euler (phi1, PHI, phi2)
	1° Au départ, les deux repères d''axes coincident. On effectue trois rotations successives
	   pour amener un des deux repères à son orientation souhaitée.
	2° On effectue une première rotation d''un angle phi1 autour de x3
	3° On effectue une deuxième rotation d''un angle PHI autour du x1 tourné
	4° On effectue une troisième rotation d''un angle phi2 autour du x3 tourné

	======================================================================
	Définition d''un quaternion 'q'
	Le changement de repère est défini sur base d''un axe de rotation 'n'
	et d''un angle de rotation 'alpha'
	Le quaternion vaut: q(1)= cos(alpha/2)  ; q(i+1)= n(i)*sin(alpha/2)

	==========================================================================
	CONVENTION:  T'ij = Aik*Ajl*Tkl				(MMC convention - different from LD!)
	***************************************************************************/
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include "rotations.h"

// Calcul de la matrice de rotation 6x6 correspondant aux angles d'euler
// euler: euler angles (phi1,phi,phi2)
// R: rotation matrix (6X6), see Doghri appendix C2
void eul_mat(double* euler, double** R){

	int i,j;
	double P[3][3];		//3x3 rotation matrix

	double c1,c2,c3,s1,s2,s3;
	double s1c2, c1c2;
	double sq2;
  double fpi = pi/180.;

	sq2 = sqrt(2.);

	c1 = cos(euler[0]*fpi);
	s1 = sin(euler[0]*fpi);

	c2 = cos(euler[1]*fpi);
	s2 = sin(euler[1]*fpi);
	
	c3 = cos(euler[2]*fpi);
	s3 = sin(euler[2]*fpi);
	
	s1c2 = s1*c2;
	c1c2 = c1*c2;

	P[0][0] = c3*c1 - s1c2*s3; 
	P[0][1] = c3*s1 + c1c2*s3;
	P[0][2] = s2*s3;

	P[1][0] = -s3*c1 - s1c2*c3;
	P[1][1] = -s3*s1 + c1c2*c3;
	P[1][2] = s2*c3;

	P[2][0] = s1*s2;
	P[2][1] = -c1*s2;
	P[2][2] = c2;

	/*printf("rotation matrix 3x3, new method\n");
	for(i=0;i<3;i++){
		for(j=0;j<3;j++){
			printf("%lf\t",P[i][j]);
		}
		printf("\n");
	}*/

   for(i=0;i<6;i++)
     for(j=0;j<6;j++)
         R[i][j]=0.;

   for(i=0;i<3;i++){

	    R[i][3]=P[i][0]*P[i][1]*sq2;
			R[i][4]=P[i][0]*P[i][2]*sq2;
      R[i][5]=P[i][1]*P[i][2]*sq2;

			R[3][i] = P[0][i]*P[1][i]*sq2;
			R[4][i] = P[0][i]*P[2][i]*sq2;
			R[5][i] = P[1][i]*P[2][i]*sq2;

	    for(j=0;j<3;j++){
			   R[i][j] = P[i][j]*P[i][j];
			}
	 
	 }

   R[3][3] = P[0][0]*P[1][1] + P[0][1]*P[1][0];
   R[3][4] = P[0][0]*P[1][2] + P[0][2]*P[1][0];
   R[3][5] = P[0][1]*P[1][2] + P[0][2]*P[1][1];
   R[4][3] = P[0][0]*P[2][1] + P[0][1]*P[2][0];
   R[4][4] = P[0][0]*P[2][2] + P[0][2]*P[2][0];
   R[4][5] = P[0][1]*P[2][2] + P[0][2]*P[2][1];
   R[5][3] = P[1][0]*P[2][1] + P[1][1]*P[2][0];
   R[5][4] = P[1][0]*P[2][2] + P[1][2]*P[2][0];
   R[5][5] = P[1][1]*P[2][2] + P[1][2]*P[2][1];

}


// Calcul du quaternion correspondant à un ensemble d'angles d'euler */
void eul_q4(double* euler, double* q){

	double sum_f, diff_f, Fi, fpi2, si_Fi, co_Fi;
	int j;
  double fpi = pi/180.;
	
	fpi2= fpi / 2.;
	sum_f=  ( euler[0] + euler[2] ) * fpi2; 
	diff_f= ( euler[0] - euler[2] ) * fpi2; 
	Fi= euler[1] * fpi2;
	co_Fi= cos(Fi);
	si_Fi= sin(Fi);

	q[0]= co_Fi * cos( sum_f );
	q[1]= si_Fi * cos( diff_f );
	q[2]= si_Fi * sin( diff_f );
	q[3]= co_Fi * sin( sum_f );
  if(q[0]<0.){
	   for(j=0;j<4;j++){
		    q[j]=-q[j];
		 }
	}
  return;
}

// Calcul de la matrice de rotation correspondant au quaternion q
void q4_mat(double* q, double **mat){

	double tmp1, tmp2;
	int i,j;
	tmp1= (q[0]*q[0]) - 0.5;
	for(i=0;i<3;i++){
	  mat[i][i]= (q[i+1]*q[i+1]) + tmp1;
	}

	tmp1= q[1]*q[2];
	tmp2= q[0]*q[3];
	mat[1][0]= tmp1 - tmp2;
	mat[0][1]= tmp1 + tmp2;
	tmp1= q[1]*q[3];
	tmp2= q[0]*q[2];
	mat[2][0]= tmp1 + tmp2;
	mat[0][2]= tmp1 - tmp2;
	tmp1= q[2]*q[3];
	tmp2= q[0]*q[1];
	mat[2][1]= tmp1 - tmp2;
	mat[1][2]= tmp1 + tmp2;
	for(i=0;i<3;i++){
		for(j=0;j<3;j++){
			mat[i][j] = 2.*mat[i][j];
		}
	}
	return;
}

// Calcul du quaternion correspondant à la matrice de rotation 'mat'
void mat_q4(double** mat, double* q){
	double tmp, tmp2;

	q[0]= mat[0][0] + mat[1][1] + mat[2][2] + 1.;

  if(q[0]<=1e-10){

	  tmp= ( mat[0][0] + 1.)/2.;
	  tmp2= ( mat[1][1] + 1.)/2.;
	  if ( (tmp<0) || (tmp2<0.) || (tmp+tmp2 > 1.)){
			printf("Prob in mat_Q4, %lf, %lf\n",tmp,tmp2);
		  return;
	  }
	  q[0]= 0.;
	  q[1]= sqrt( tmp );
	  q[2]= sqrt( tmp2 );
	  q[3]= sqrt( 1. - tmp - tmp2 );
	  if (mat[1][0]<0.) {q[2]= -q[2];}
	  if (mat[2][0]<0.) {q[3]= -q[3];}
	  return;
	}
	q[0]= sqrt(q[0])/2.;
	tmp= q[0] * 4.;
	q[1]= (mat[1][2]-mat[2][1])/tmp;
	q[2]= (mat[2][0]-mat[0][2])/tmp;
	q[3]= (mat[0][1]-mat[1][0])/tmp;
	return;
}


//Produit de deux quaternions (=composition de rotations)
//Attention: le produit q1*q2 correspond au produit de matrices mat2.mat1
void q4mult(int Res, double* q_1, double* q_2){
	int i;
	double* q_tmp;

  q_tmp=(double*) malloc(4*sizeof(double));

	q_tmp[0]= q_1[0] * q_2[0];
  for(i=1;i<4;i++){

	  q_tmp[0]= q_tmp[0] - q_1[i] * q_2[i];
	  q_tmp[i]= q_1[0] * q_2[i] + q_2[0] * q_1[i]; 
	}
	q_tmp[1]= q_tmp[1] - ( q_1[2]*q_2[3] - q_1[3]*q_2[2] );
	q_tmp[2]= q_tmp[2] - ( q_1[3]*q_2[1] - q_1[1]*q_2[3] );
	q_tmp[3]= q_tmp[3] - ( q_1[1]*q_2[2] - q_1[2]*q_2[1] );

	if (Res==1){
	  for(i=0;i<4;i++){
	    q_1[i]= q_tmp[i];
	  }
	}
	else{
	  for(i=0;i<4;i++){
	    q_2[i]= q_tmp[i];
	  }
	}
	return;
}

//Calcul des angles d''euler correspondant à un quaternions 'r'
void q4_eul(double* r, double* ang){

	int i;
	double tmp, msin, mcos;
  double fpi = pi/180.;

	if (r[0]<0.){
	  for(i=0;i<4;i++){
	    r[i]= -r[i];
	  }
	}
	tmp= r[0]*r[0] + r[3]*r[3];
	if (fabs(1.-tmp)<1.e-7){
	  mcos= r[0];
	  msin= r[3];
	  ang[0]= 2.*Karc(msin,mcos) - pi/4.;
	  ang[1]= 0.;
	  ang[2]= pi/4.;
	}
	else if (fabs(tmp)<1.e-7){
	  mcos= r[1];
	  msin= r[2];
	  ang[0]= 2.*Karc(msin,mcos) + pi/4.;
	  ang[1]= pi;
	  ang[2]= pi/4.;
	}
	else{
	  ang[1]= acos(2.*tmp - 1.);
	  msin= r[1]*r[3] + r[0]*r[2];
	  mcos= -r[2]*r[3] + r[0]*r[1];
	  ang[0]= Karc(msin,mcos);
	  msin= r[1]*r[3] - r[0]*r[2];
	  mcos= r[2]*r[3] + r[0]*r[1];
	  ang[2]= Karc(msin,mcos);
	}
	for(i=0;i<3;i++){
	  ang[i]= ang[i]/fpi;
	}
	return;
}

// Calcul de arctan
double Karc(double si,double co){
	double tmp;
	
	if (fabs(co)<=1.e-10){
	  if (si>=0) tmp= pi/2.;
	  if (si<0)  tmp= -pi/2.;
	}
	else{
	  if (co>=0) tmp= atan(si/co);
	  if (co<0.) tmp= atan(si/co) + pi;
	}
	if (tmp>pi) tmp= tmp - 2.*pi;

	return tmp;
	
}


//Rotation d''un tenseur d''ordre 2 symétrique
//(i6 décrit la convention pour convertir en vecteur à 6 composantes)
void Krots3(double** rot, double* D0, double* D, int** i6){
	int i,j,k,l;
	
  for(i=0;i<6;i++){D[i]=0.;}

	for(i=0;i<3;i++){
	   for(j=i;j<3;j++){
	  
	      //(Only the upper-right part of the tensor is computed)
		    for(k=0;k<3;k++){
		       D[i6[i][j]] = D[i6[i][j]] + rot[i][k]*rot[j][k]*D0[i6[k][k]];
           for(l=k+1;l<3;l++){
	            D[i6[i][j]]= D[i6[i][j]] + (rot[i][k] * rot[j][l] + rot[i][l] * rot[j][k] )* D0[i6[k][l]];
     	       //(this is because D0(kl)= D0(lk))
					 }
		    }
		 }
	}
  return;
}

//** Rotation d''un tenseur d''ordre 2 anti-symétrique
//** convention: w(1)= W(1,2) ; w(2)= W(1,3) ; w(3)= W(2,3)
void Krotw3(double** rot, double* w1, double* w2, int** i3){
	
	double tmp;
	int i,j,k,l;

  for(i=0;i<3;i++){
	   w2[i]=0.;
	}

  for(i=0;i<3;i++){
		 for(k=0;k<3;k++){
	      for(j=i+1;j<3;j++){
				   for(l=k+1;l<3;l++){    
	            tmp= rot[i][k] * rot[j][l] - rot[i][l] * rot[j][k];
	            w2[i3[i][j]]= w2[i3[i][j]] + w1[i3[k][l]] * tmp;
					 }
				}
		 }
	}
	return;
}

//** Calcule la matrice de rotation 6x6 (convention d'Issam)
//   correspondant à la matrice P (3x3)
void rmat66(double** P, double** R){
  
	 int i,j;
	 double sq2;
	 double tmp;
   sq2=sqrt(2.);

   for(i=0;i<3;i++){

	    R[i][3]=P[i][0]*P[i][1]*sq2;
			R[i][4]=P[i][1]*P[i][2]*sq2;
      R[i][5]=P[i][0]*P[i][2]*sq2;

			R[3][i] = P[0][i]*P[1][i]*sq2;
			R[4][i] = P[1][i]*P[2][i]*sq2;
			R[5][i] = P[0][i]*P[2][i]*sq2;

	    for(j=0;j<3;j++){
			   R[i][j] = P[i][j]*P[i][j];
			}
	 
	 }

   R[3][3] = P[0][0]*P[1][1] + P[0][1]*P[1][0];
   R[3][4] = P[0][1]*P[1][2] + P[0][2]*P[1][1];
   R[3][5] = P[0][0]*P[1][2] + P[0][2]*P[1][0];
   R[4][3] = P[1][0]*P[2][1] + P[1][1]*P[2][0];
   R[4][4] = P[1][1]*P[2][2] + P[1][2]*P[2][1];
   R[4][5] = P[1][0]*P[2][2] + P[1][2]*P[2][0];
   R[5][3] = P[0][0]*P[2][1] + P[0][1]*P[2][0];
   R[5][4] = P[0][1]*P[2][2] + P[0][2]*P[2][1];
   R[5][5] = P[0][0]*P[2][2] + P[0][2]*P[2][0];
 
   //swap rows and columns 13 and 23
	 for(i=0;i<6;i++){
	    tmp=R[4][i];
			R[4][i]=R[5][i];
			R[5][i]=tmp;
	 }
	 for(i=0;i<6;i++){
	    tmp=R[i][4];
			R[i][4]=R[i][5];
			R[i][5]=tmp;
	 }

}

//** Rotation d'un tenseur d'ordre 2 symétrique stocké selon la convention d'Issam
// préserve la convention d'Issam
void rot2(double** q, double* a, double* res){

  int i,j;
	for(i=0;i<6;i++){
	   res[i]=0.;
	}

   for(i=0;i<6;i++){
	    for(j=0;j<6;j++){
			   res[i] = res[i] + q[i][j]*a[j];
			}
	 }

}
//** Rotation d'un tenseur d'ordre 4 stocké selon la convention d'Issam
void rot4(double** q, double** T, double** res){
// INPUT: q: matrice de rotation 6x6
// 				T: tenseur d'ordre 4, convention d'Issam
// OUTPUT: res=T'=q*T*q')


int i,j, k,l;

for(i=0;i<6;i++){
  for(j=0;j<6;j++){
	   res[i][j]=0.;
	}
}

for(i=0;i<6;i++){
   for(j=0;j<6;j++){
	    for(k=0;k<6;k++){
			  for(l=0;l<6;l++){
				   res[i][j] = res[i][j] + q[i][k]*q[j][l]*T[k][l];
				}
			}
	 }
}


}



void eul_mat33(double* euler, double** P){
//input euler angles globle to local, calculate rotation matrix P,3x3 rotation matrix 
// By Ling Wu 3-10-2011

	int i,j;
	double c1,c2,c3,s1,s2,s3;
	double s1c2, c1c2;
	double sq2;
  double fpi = pi/180.;

	sq2 = sqrt(2.);

	c1 = cos(euler[0]*fpi);
	s1 = sin(euler[0]*fpi);

	c2 = cos(euler[1]*fpi);
	s2 = sin(euler[1]*fpi);
	
	c3 = cos(euler[2]*fpi);
	s3 = sin(euler[2]*fpi);
	
	s1c2 = s1*c2;
	c1c2 = c1*c2;

	P[0][0] = c3*c1 - s1c2*s3; 
	P[0][1] = c3*s1 + c1c2*s3;
	P[0][2] = s2*s3;

	P[1][0] = -s3*c1 - s1c2*c3;
	P[1][1] = -s3*s1 + c1c2*c3;
	P[1][2] = s2*c3;

	P[2][0] = s1*s2;
	P[2][1] = -c1*s2;
	P[2][2] = c2;

}




void rot33(double** P, double** T, double** res){
// INPUT: P: matrice de rotation 3x3
//  T: 3x3
// OUTPUT: res=T'=P*T*P'       By Ling Wu 3-10-2011

int i,j, k,l;

for(i=0;i<3;i++){
  for(j=0;j<3;j++){
	   res[i][j]=0.;
	}
}

for(i=0;i<3;i++){
   for(j=0;j<3;j++){
	    for(k=0;k<3;k++){
			  for(l=0;l<3;l++){
				   res[i][j] = res[i][j] + P[i][k]*P[j][l]*T[k][l];
				}
			}
	 }
}

}






#endif
