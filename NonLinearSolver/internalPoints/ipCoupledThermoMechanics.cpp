//
// Created by vinayak on 28.06.23.
//
// Author:  <Vinayak GHOLAP>, (C) 2023
//
// Copyright: See COPYING file that comes with this distribution
//
//

#include "ipCoupledThermoMechanics.h"

IPCoupledThermoMechanics::IPCoupledThermoMechanics(): IPVariableMechanics(), _thermalEnergy(0.0)
{
    STensorOperation::unity(referenceF);
}

IPCoupledThermoMechanics::IPCoupledThermoMechanics(const IPCoupledThermoMechanics &source) :IPVariableMechanics(source)
{
    referenceF = source.referenceF;
    _thermalEnergy = source._thermalEnergy;
}

IPCoupledThermoMechanics &IPCoupledThermoMechanics::operator=(const IPVariable &source)
{
    IPVariableMechanics::operator=(source);
    const IPCoupledThermoMechanics* src = dynamic_cast<const IPCoupledThermoMechanics*>(&source);
    if(src)
    {
        referenceF = src->referenceF;
        _thermalEnergy =src->_thermalEnergy;
    }
    return  *this;
}

void IPCoupledThermoMechanics::restart()
{
    restartManager::restart(referenceF);
    restartManager::restart(_thermalEnergy);
    IPVariableMechanics::restart();
}