//
// Description: Storing class for gurson Coalescence
//
//
// Author:  <J. Leclerc>, (C) 2017
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "ipCoalescence.h"
#include "restartManager.h"
#include "ipNonLocalPorosity.h"

// IPCoalescenceBase
// -------------------------------------------
IPCoalescence::IPCoalescence():  _acceleratedRate(1.), _coalescenceOnsetFlag(false),_ipvAtCoalescenceOnset(NULL),
																 _coalescenceActive(false), _coalescenceMode(0),
                                 _crackOffsetOnCft(1.), _yieldOffset(0.), _LodeOffset(1.)
{

};


IPCoalescence::IPCoalescence(const IPCoalescence &source) :
    _acceleratedRate(source._acceleratedRate),
    _coalescenceOnsetFlag(source._coalescenceOnsetFlag), 
   _coalescenceActive(source._coalescenceActive), 
   _coalescenceMode(source._coalescenceMode),
  _crackOffsetOnCft(source._crackOffsetOnCft), _yieldOffset(source._yieldOffset),
  _LodeOffset(source._LodeOffset)
{
  _ipvAtCoalescenceOnset = NULL;
  if (source._ipvAtCoalescenceOnset != NULL){
    _ipvAtCoalescenceOnset = dynamic_cast<IPNonLocalPorosity*>(source._ipvAtCoalescenceOnset->clone());
  }

};


IPCoalescence &IPCoalescence::operator=(const IPCoalescence &source)
{
  _acceleratedRate = source._acceleratedRate;
  _coalescenceOnsetFlag = source._coalescenceOnsetFlag;
  _coalescenceActive = source._coalescenceActive;
  _coalescenceMode = source._coalescenceMode;
  _crackOffsetOnCft = source._crackOffsetOnCft;
  _yieldOffset = source._yieldOffset;
  _LodeOffset = source._LodeOffset;
  
  if (source._ipvAtCoalescenceOnset != NULL)
  {
    if (_ipvAtCoalescenceOnset != NULL)
    {
      _ipvAtCoalescenceOnset->operator=(*dynamic_cast<const IPVariable*>(source._ipvAtCoalescenceOnset));
    }
    else
    {
      _ipvAtCoalescenceOnset =dynamic_cast<IPNonLocalPorosity*>(source._ipvAtCoalescenceOnset->clone());
    }
  }
  else
  {
    if (_ipvAtCoalescenceOnset) {delete _ipvAtCoalescenceOnset; _ipvAtCoalescenceOnset = NULL;}
  }
  return *this;
};

IPCoalescence::~IPCoalescence(){
  if (_ipvAtCoalescenceOnset) {
    delete _ipvAtCoalescenceOnset;
    _ipvAtCoalescenceOnset = NULL;
  }
  
};

void IPCoalescence::restart()
{
  restartManager::restart(_acceleratedRate);
  restartManager::restart(_coalescenceOnsetFlag);
  restartManager::restart(_ipvAtCoalescenceOnset);
  restartManager::restart(_coalescenceActive);
  restartManager::restart(_coalescenceMode);
  restartManager::restart(_crackOffsetOnCft);
  restartManager::restart(_yieldOffset);
  restartManager::restart(_LodeOffset);
};

const IPNonLocalPorosity* IPCoalescence::getIPvAtCoalescenceOnset() const{
  if (_ipvAtCoalescenceOnset == NULL)
  {
    Msg::Error("IPCoalescence::getIPVoidStateAtCoalescenceOnset in an ipv does not exist");
    return NULL;
  }
  else
  {
    return _ipvAtCoalescenceOnset;
  }
};

void IPCoalescence::deleteIPvAtCoalescenceOnset()
{
  if (_ipvAtCoalescenceOnset != NULL)
  {
    delete _ipvAtCoalescenceOnset;
    _ipvAtCoalescenceOnset = NULL;
  }
}

void IPCoalescence::setIPvAtCoalescenceOnset(const IPNonLocalPorosity& ipvVoid)
{
  if (_ipvAtCoalescenceOnset == NULL){
    _ipvAtCoalescenceOnset = dynamic_cast<IPNonLocalPorosity*>(ipvVoid.clone());
  }
  else{
    _ipvAtCoalescenceOnset->operator =(*dynamic_cast<const IPVariable*>(&ipvVoid));
  }
};


// IPNoCoalescence
// -------------------------------------------
IPNoCoalescence::IPNoCoalescence() : IPCoalescence()
{
};


IPNoCoalescence::IPNoCoalescence(const IPNoCoalescence &source) : IPCoalescence(source)
{
};


IPNoCoalescence &IPNoCoalescence::operator=(const IPCoalescence &source)
{
  IPCoalescence::operator=(source);
  return *this;
};

IPCoalescence * IPNoCoalescence::clone() const
{
  return new IPNoCoalescence(*this);
};



// IPFstarCoalescence
// -------------------------------------------
IPFstarCoalescence::IPFstarCoalescence() : IPCoalescence()
{

};


IPFstarCoalescence::IPFstarCoalescence(const IPFstarCoalescence &source) : IPCoalescence(source)
{

};


IPFstarCoalescence &IPFstarCoalescence::operator=(const IPCoalescence &source)
{
  IPCoalescence::operator=(source);
  return *this;
};


IPCoalescence * IPFstarCoalescence::clone() const
{
  return new IPFstarCoalescence(*this);
};


/* IPThomasonCoalescence */
// -------------------------------------------
IPLoadConcentrationFactorBasedCoalescence::IPLoadConcentrationFactorBasedCoalescence(): IPCoalescence()
{

};


IPLoadConcentrationFactorBasedCoalescence::IPLoadConcentrationFactorBasedCoalescence(const IPLoadConcentrationFactorBasedCoalescence& src): IPCoalescence(src)
{

};


IPLoadConcentrationFactorBasedCoalescence& IPLoadConcentrationFactorBasedCoalescence::operator = (const IPCoalescence& src){
  IPCoalescence::operator =(src);
  return *this;
};



