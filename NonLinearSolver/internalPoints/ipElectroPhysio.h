//
// C++ Interface: terms
//
// Description: Define electro-physiological internal variables
//
//
// Author:  <A. Jerusalem>, (C) 2013
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef IPELECTROPHYSIO_H_
#define IPELECTROPHYSIO_H_
#include <stdlib.h>
#include <stdio.h>
#include "GmshConfig.h"
#include "STensor3.h"
#include "ipvariable.h"

class IPElectroPhysio : public IPVariable{
 protected:
  double CaC, NaC, KC;
 public:
  IPElectroPhysio();
  IPElectroPhysio(const IPElectroPhysio &source);
  virtual IPElectroPhysio &operator=(const IPVariable &source);
  virtual ~IPElectroPhysio(){}
  virtual void restart();
  virtual double getCaConcentration() const {return CaC;}
  virtual double getNaConcentration() const {return NaC;}
  virtual double getKConcentration()  const {return KC;}

  virtual void setValues(const double &_calcium, const double &_sodium, const double &_potassium)
  {
    CaC        = _calcium;
    NaC        = _sodium;
    KC         = _potassium;
  }

  virtual IPVariable* clone() const=0;
};

class IPCableTheoryEP : public IPElectroPhysio
{

 protected:

 public:
  IPCableTheoryEP();
  IPCableTheoryEP(const IPCableTheoryEP &source);
  virtual IPCableTheoryEP &operator=(const IPVariable &source);
  virtual ~IPCableTheoryEP(){}
  virtual void restart();
  virtual IPVariable* clone() const;

};

class IPHodgkinHuxleyEP : public IPElectroPhysio
{

 protected:

 public:
  IPHodgkinHuxleyEP();
  IPHodgkinHuxleyEP(const IPHodgkinHuxleyEP &source);
  virtual IPHodgkinHuxleyEP &operator=(const IPVariable &source);
  virtual ~IPHodgkinHuxleyEP(){}
  virtual void restart();
  virtual IPVariable* clone() const;

};

#endif //IPELECTROPHYSIO_H_

