//
// Description: Provide ip class for nucleation criterion 
//
//
// Author:  <J. Leclerc>, (C) 2019
//
// Copyright: See COPYING file that comes with this distribution
//
//

#include "ipNucleationCriterion.h"
#include "restartManager.h"


// IPNucleationCriterion
// -------------------------------------------
IPNucleationCriterion::IPNucleationCriterion()
{
  // Nothing more needs to be done here.
};


IPNucleationCriterion::IPNucleationCriterion( const IPNucleationCriterion& source )
{
  // Nothing more needs to be done here.
};


IPNucleationCriterion& IPNucleationCriterion::operator=( const IPNucleationCriterion& source )
{
  // Nothing more needs to be done here.
  return *this;
};


void IPNucleationCriterion::restart()
{
  // Nothing more needs to be done here.
  return;
};




// IPTrivialNucleationCriterion
// -------------------------------------------
IPTrivialNucleationCriterion::IPTrivialNucleationCriterion() :
  IPNucleationCriterion()
{
  // Nothing more needs to be done here.
};


IPTrivialNucleationCriterion::IPTrivialNucleationCriterion( const IPTrivialNucleationCriterion& source ) :
  IPNucleationCriterion(source)
{
  // Nothing more needs to be done here.
};


IPTrivialNucleationCriterion& IPTrivialNucleationCriterion::operator=( const IPNucleationCriterion& source )
{
  /// Copy mother class
  IPNucleationCriterion::operator=( source );
  
  /// Copy this class
  //const IPTrivialNucleationCriterion* source_casted = dynamic_cast<const IPTrivialNucleationCriterion*>( &source );
  //if( source_casted != NULL )
  //{
    /// Nothing more needs to be done here.
  //}
  return *this;
};


void IPTrivialNucleationCriterion::restart()
{
  IPNucleationCriterion::restart();
  return;
};







// IPNonTrivialNucleationCriterion
// -------------------------------------------
IPNonTrivialNucleationCriterion::IPNonTrivialNucleationCriterion() :
  IPNucleationCriterion(),
  hasNucleationActivated_(false), hasNucleationOnset_(false), isInhibited_(false)
{
  // Nothing more needs to be done here.
};


IPNonTrivialNucleationCriterion::IPNonTrivialNucleationCriterion( const IPNonTrivialNucleationCriterion& source ) :
  IPNucleationCriterion(source),
  hasNucleationActivated_(source.hasNucleationActivated_), hasNucleationOnset_(source.hasNucleationOnset_),
  isInhibited_(source.isInhibited_)
{
  // Nothing more needs to be done here.
};


IPNonTrivialNucleationCriterion& IPNonTrivialNucleationCriterion::operator=( const IPNucleationCriterion& source )
{
  /// Copy mother class
  IPNucleationCriterion::operator=( source );
  
  /// Copy this class
  const IPNonTrivialNucleationCriterion* source_casted = dynamic_cast< const IPNonTrivialNucleationCriterion* >( &source );
  if( source_casted != NULL )
  {
    hasNucleationActivated_ = source_casted->hasNucleationActivated_;
    hasNucleationOnset_ = source_casted->hasNucleationOnset_;
    isInhibited_ = source_casted->isInhibited_;
  }
  return *this;
};


void IPNonTrivialNucleationCriterion::restart()
{
  IPNucleationCriterion::restart();
  restartManager::restart( hasNucleationActivated_ );
  restartManager::restart( hasNucleationOnset_ );
  restartManager::restart( isInhibited_ );
  return;
};













// IPPlasticBasedNucleationCriterion
// -------------------------------------------
IPPlasticBasedNucleationCriterion::IPPlasticBasedNucleationCriterion() :
  IPNonTrivialNucleationCriterion()
{
  // Nothing more needs to be done here.
};


IPPlasticBasedNucleationCriterion::IPPlasticBasedNucleationCriterion( const IPPlasticBasedNucleationCriterion& source ) :
  IPNonTrivialNucleationCriterion(source)
{
  // Nothing more needs to be done here.
};


IPPlasticBasedNucleationCriterion& IPPlasticBasedNucleationCriterion::operator=( const IPNucleationCriterion& source )
{
  /// Copy mother class
  IPNonTrivialNucleationCriterion::operator=( source );
  
  /// Copy this class
  //const IPPlasticBasedNucleationCriterion* source_casted = dynamic_cast< const IPPlasticBasedNucleationCriterion* >( &source );
  //if( source_casted != NULL )
  //{
  //
  //}
  return *this;
};


void IPPlasticBasedNucleationCriterion::restart()
{
  IPNonTrivialNucleationCriterion::restart();
  return;
};




// IPPorosityBasedNucleationCriterion
// -------------------------------------------
IPPorosityBasedNucleationCriterion::IPPorosityBasedNucleationCriterion() :
  IPNonTrivialNucleationCriterion()
{
  // Nothing more needs to be done here.
};


IPPorosityBasedNucleationCriterion::IPPorosityBasedNucleationCriterion( const IPPorosityBasedNucleationCriterion& source ) :
  IPNonTrivialNucleationCriterion(source)
{
  // Nothing more needs to be done here.
};


IPPorosityBasedNucleationCriterion& IPPorosityBasedNucleationCriterion::operator=( const IPNucleationCriterion& source )
{
  /// Copy mother class
  IPNonTrivialNucleationCriterion::operator=( source );
  
  /// Copy this class
  //const IPPorosityBasedNucleationCriterion* source_casted = dynamic_cast<const IPPorosityBasedNucleationCriterion*>( &source );
  //if( source_casted != NULL )
  //{
  //
  //}
  return *this;
};


void IPPorosityBasedNucleationCriterion::restart()
{
  IPNonTrivialNucleationCriterion::restart();
  return;
};




// IPBereminNucleationCriterion
// -------------------------------------------
IPBereminNucleationCriterion::IPBereminNucleationCriterion() :
    IPNonTrivialNucleationCriterion(),
    ampliFactor_(1.0)
{
  
};


IPBereminNucleationCriterion::IPBereminNucleationCriterion( const IPBereminNucleationCriterion& source ) :
    IPNonTrivialNucleationCriterion(source),
    ampliFactor_(source.ampliFactor_)
{
  
};


IPBereminNucleationCriterion &IPBereminNucleationCriterion::operator=( const IPNucleationCriterion& source )
{
  // Copy mother class
  IPNonTrivialNucleationCriterion::operator=(source);
  
  // Copy this class
  const IPBereminNucleationCriterion* source_casted = dynamic_cast< const IPBereminNucleationCriterion* >( &source );
  if( source_casted != NULL )
  {
    ampliFactor_ = source_casted->ampliFactor_;
  }
  return *this;
};


void IPBereminNucleationCriterion::restart()
{
  IPNonTrivialNucleationCriterion::restart();
  restartManager::restart( ampliFactor_ );
  return;
};


