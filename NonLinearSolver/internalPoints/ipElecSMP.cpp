//
// Description: storing class for Electro Thermo Mechanics
//
//
// Author:  <Lina Homsy>, (C) 2013
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "ipElecSMP.h"

IPElecSMP::IPElecSMP(double _Sa1, double _phia, double _Sastar1) : IPSMP(_Sa1, _phia, _Sastar1) {

};

IPElecSMP::IPElecSMP() : IPSMP() {

};
IPElecSMP::IPElecSMP(const IPElecSMP &source) :  IPSMP(source){

}
IPElecSMP& IPElecSMP::operator=(const IPVariable &source)
{
  IPSMP::operator=(source);
  const IPElecSMP* src = dynamic_cast<const IPElecSMP*>(&source);
  if(src)
  {

  }
  return *this;
}

double IPElecSMP::defoEnergy() const
{
  return  IPSMP::defoEnergy();
}

