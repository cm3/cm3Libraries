//
// Description: storing class for j2 linear elasto-plastic law
//
//
// Author:  <Gauthier BECKER>, (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "ipJ2linear.h"
#include "restartManager.h"
#include "ipField.h"
IPJ2linear::IPJ2linear() : IPVariableMechanics(), _j2lepspbarre(0.), _j2lepsp(1.), _j2ldsy(0.),  _elasticEnergy(0.),
_plasticPower(0.),_DplasticPowerDF(0.),_Ee(0.),_irreversibleEnergy(0.),_DirreversibleEnergyDF(0.),
_dissipationBlocked(false),_dissipationActive(false),_plasticEnergy(0.), 
_P(0.), _sig(0.), _svm(0.), _strain(0.), _eigenstress(0.), _eigenstress_eq(0.), _eigenstrain(0.),
_DplasticEnergyDF(0.)
{
  ipvJ2IsotropicHardening=NULL;
  Msg::Error("IPJ2Linear::IPJ2Linear is not initialized with a hardening IP Variable");

};
IPJ2linear::IPJ2linear(const J2IsotropicHardening *j2IH) : IPVariableMechanics(), _j2lepspbarre(0.), _j2lepsp(1.), _j2ldsy(0.),
_elasticEnergy(0.),_plasticPower(0.),_DplasticPowerDF(0.),_Ee(0.),_irreversibleEnergy(0.),_DirreversibleEnergyDF(0.),
_dissipationBlocked(false),_dissipationActive(false),_plasticEnergy(0.),
_P(0.), _sig(0.),_svm(0.), _strain(0.), _eigenstress(0.), _eigenstress_eq(0.), _eigenstrain(0.),
_DplasticEnergyDF(0.)
{
  ipvJ2IsotropicHardening=NULL;
  if(j2IH ==NULL) Msg::Error("IPJ2Linear::IPJ2Linear has no j2IH");
  j2IH->createIPVariable(ipvJ2IsotropicHardening);

};
IPJ2linear::IPJ2linear(const IPJ2linear &source) : IPVariableMechanics(source), _j2lepspbarre(source._j2lepspbarre),
                                                   _j2lepsp(source._j2lepsp), _j2ldsy(source._j2ldsy),
                                                   _elasticEnergy(source._elasticEnergy),
                                                   _plasticPower(source._plasticPower),_DplasticPowerDF(source._DplasticPowerDF), _Ee(source._Ee),_irreversibleEnergy(source._irreversibleEnergy), _DirreversibleEnergyDF(source._DirreversibleEnergyDF),																									 _dissipationBlocked(source._dissipationBlocked),_dissipationActive(source._dissipationActive),_plasticEnergy(source._plasticEnergy),
_P(source._P), _sig(source._sig), _svm(source._svm), _strain(source._strain), _eigenstress(source._eigenstress), _eigenstress_eq(source._eigenstress_eq), _eigenstrain(source._eigenstrain),
_DplasticEnergyDF(source._DplasticEnergyDF)
{
  ipvJ2IsotropicHardening = NULL;
  if(source.ipvJ2IsotropicHardening != NULL)
  {
    ipvJ2IsotropicHardening = dynamic_cast<IPJ2IsotropicHardening*>(source.ipvJ2IsotropicHardening->clone());
  }
}
IPJ2linear& IPJ2linear::operator=(const IPVariable &source)
{
  IPVariableMechanics::operator=(source);
  const IPJ2linear* src = dynamic_cast<const IPJ2linear*>(&source);
  if(src!=NULL)
  {
    _j2lepspbarre = src->_j2lepspbarre;
    _j2lepsp = src->_j2lepsp;
    _j2ldsy = src->_j2ldsy;
    _elasticEnergy = src->_elasticEnergy;
    _plasticPower = src->_plasticPower;
    _DplasticPowerDF = src->_DplasticPowerDF;
    _Ee = src->_Ee;
		_irreversibleEnergy = src->_irreversibleEnergy;
		_DirreversibleEnergyDF = src->_DirreversibleEnergyDF;
		_dissipationBlocked = src->_dissipationBlocked;
    _dissipationActive = src->_dissipationActive;
    _plasticEnergy = src->_plasticEnergy;
    _DplasticEnergyDF= src->_DplasticEnergyDF;
    _P = src->_P;
    _sig = src->_sig;
    _svm = src->_svm;
    _strain = src->_strain;
    _eigenstress = src->_eigenstress;
    _eigenstress_eq = src->_eigenstress_eq;
    _eigenstrain = src->_eigenstrain;
    if(src->ipvJ2IsotropicHardening != NULL)
    {
      if (ipvJ2IsotropicHardening!=NULL){
        ipvJ2IsotropicHardening->operator=(*(dynamic_cast<IPJ2IsotropicHardening*>(src->ipvJ2IsotropicHardening)));
      }
      else
        ipvJ2IsotropicHardening= dynamic_cast<IPJ2IsotropicHardening*>(src->ipvJ2IsotropicHardening->clone());
    }
    else{
      if (ipvJ2IsotropicHardening) {
        delete ipvJ2IsotropicHardening;
        ipvJ2IsotropicHardening = NULL; 
      }
    }
  }
  return *this;
}

double IPJ2linear::defoEnergy() const
{
  return _elasticEnergy;

}
double IPJ2linear::plasticEnergy() const
{
  return _plasticEnergy;
}
double IPJ2linear::plasticEqStrain() const
{
  return _j2lepspbarre;
}
double IPJ2linear::eigenstressEq() const
{
  return _eigenstress_eq;
}
double IPJ2linear::svm() const
{
  return _svm;
}

double IPJ2linear::get(int comp) const
{
  if(comp == IPField::P_XX)
  {
    return _P(0,0);
  }
  else if(comp == IPField::P_YY)
  {
    return _P(1,1);
  }
  else if(comp == IPField::P_ZZ)
  {
    return _P(2,2);
  }
  else if(comp == IPField::P_XY)
  {
    return _P(0,1);
  }
  else if(comp == IPField::P_XZ)
  {
    return _P(0,2);
  }
  else if(comp == IPField::P_YZ)
  {
    return _P(1,2);
  } 
  else if(comp == IPField::SVM)
  {
    return _svm;
  }
  else if(comp == IPField::SIG_XX)
  {
    return _sig(0,0);
  }
  else if(comp == IPField::SIG_YY)
  {
    return _sig(1,1);
  }
  else if(comp == IPField::SIG_ZZ)
  {
    return _sig(2,2);
  }
  else if(comp == IPField::SIG_XY)
  {
    return _sig(0,1);
  }
  else if(comp == IPField::SIG_XZ)
  {
    return _sig(0,2);
  }
  else if(comp == IPField::SIG_YZ)
  {
    return _sig(1,2);
  } 

  else if(comp == IPField::STRAIN_XX)
  {
    return _strain(0,0);
  }
  else if(comp == IPField::STRAIN_YY)
  {
    return _strain(1,1);
  }
  else if(comp == IPField::STRAIN_ZZ)
  {
    return _strain(2,2);
  }
  else if(comp == IPField::STRAIN_XY)
  {
    return _strain(0,1);
  }
  else if(comp == IPField::STRAIN_XZ)
  {
    return _strain(0,2);
  }
  else if(comp == IPField::STRAIN_YZ)
  {
    return _strain(1,2);
  }
  else if(comp == IPField::PLASTICSTRAIN)
  {
    return _j2lepspbarre;
  }
  else if(comp == IPField::EQUIVALENT_EIGENSTRESS)
  {
    return _eigenstress_eq;
  }
  else if(comp==IPField::EIGENSTRESS_XX)
  {
    return _eigenstress(0,0);
  }
  else if(comp==IPField::EIGENSTRESS_YY)
  {
    return _eigenstress(1,1);
  }
  else if(comp==IPField::EIGENSTRESS_ZZ)
  {
    return _eigenstress(2,2);
  }
  else if(comp==IPField::EIGENSTRESS_XY)
  {
    return _eigenstress(0,1);
  }
  else if(comp==IPField::EIGENSTRESS_XZ)
  {
    return _eigenstress(0,2);
  }
  else if(comp==IPField::EIGENSTRESS_YZ)
  {
    return _eigenstress(1,2);
  }
  else if(comp==IPField::EIGENSTRAIN_XX)
  {
    return _eigenstrain(0,0);
  }
  else if(comp==IPField::EIGENSTRAIN_YY)
  {
    return _eigenstrain(1,1);
  }
  else if(comp==IPField::EIGENSTRAIN_ZZ)
  {
    return _eigenstrain(2,2);
  }
  else if(comp==IPField::EIGENSTRAIN_XY)
  {
    return _eigenstrain(0,1);
  }
  else if(comp==IPField::EIGENSTRAIN_XZ)
  {
    return _eigenstrain(0,2);
  }
  else if(comp==IPField::EIGENSTRAIN_YZ)
  {
    return _eigenstrain(1,2);
  }
  else
  {
    return 0.;
  }
}

void IPJ2linear::restart()
{
  IPVariableMechanics::restart();
  restartManager::restart(_j2lepspbarre);
  restartManager::restart(_j2lepsp);
  restartManager::restart(_j2ldsy);
  restartManager::restart(_elasticEnergy);
  restartManager::restart(ipvJ2IsotropicHardening);
  restartManager::restart(_plasticPower);
  restartManager::restart(_DplasticPowerDF);
  restartManager::restart(_Ee);
	restartManager::restart(_irreversibleEnergy);
	restartManager::restart(_DirreversibleEnergyDF);
	restartManager::restart(_dissipationBlocked);
  restartManager::restart(_dissipationActive);
  restartManager::restart(_plasticEnergy);
  restartManager::restart(_DplasticEnergyDF);
  restartManager::restart(_P);
  restartManager::restart(_sig);
  restartManager::restart(_svm);
  restartManager::restart(_strain);
  restartManager::restart(_eigenstress);
  restartManager::restart(_eigenstress_eq);
  restartManager::restart(_eigenstress);
  restartManager::restart(_eigenstrain);
  return;
}

