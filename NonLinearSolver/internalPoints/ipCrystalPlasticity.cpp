//
// Description: storing class for crystal plasticity
//
//
// Author:  <L. Noels>, (C) 2019
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "ipCrystalPlasticity.h"
#include "ipField.h"
#include "restartManager.h"


double IPCrystalPlasticity::get(int comp) const
{
  //if(comp==IPField::???) 
  //  return ????
  //else
    return IPUMATInterface::get(comp);
}

void IPCrystalPlasticity::restart()
{
  IPUMATInterface::restart();
 return;
}
