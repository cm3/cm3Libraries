//
// Created by vinayak on 04.07.22.
//

#ifndef IPGENERICTM_H_
#define IPGENERICTM_H_

#include "ipvariable.h"
#include "STensor3.h"
#include "STensorOperations.h"
#include "ipCoupledThermoMechanics.h"

class IPGenericTM : public IPCoupledThermoMechanics
{
protected:
    IPVariableMechanics * _ipMeca;
    STensor3 FthI0;
public:

public:
    IPGenericTM();
    ~IPGenericTM()
    {
        if(_ipMeca!=NULL)
        {
            delete _ipMeca;
            _ipMeca=NULL;
        }
    }

    IPGenericTM(const IPGenericTM &source);
    virtual IPGenericTM& operator=(const IPVariable &source);
    virtual double defoEnergy() const{return _ipMeca->defoEnergy();};
    virtual double plasticEnergy() const{return _ipMeca->plasticEnergy();}
    virtual double damageEnergy() const{return  _ipMeca->damageEnergy();}
    virtual double irreversibleEnergy() const{return  _ipMeca->irreversibleEnergy();}

    virtual void restart();
    virtual void setValueForBodyForce(const STensor43& K, const int ModuliType){ _ipMeca->setValueForBodyForce(K,ModuliType); }
    virtual const STensor43 &getConstRefToTangentModuli() const{return _ipMeca->getConstRefToTangentModuli();};
    virtual void computeBodyForce(const STensor33& G, SVector3& B) const { _ipMeca->computeBodyForce(G,B); }

    virtual IPVariable* clone() const {return new IPGenericTM(*this);};

    const IPVariableMechanics& getConstRefToIpMeca() const {return *_ipMeca;}
    IPVariableMechanics& getRefToIpMeca() {return *_ipMeca;}
    void setIpMeca(IPVariable& ipMeca)
    {
        if(_ipMeca!=NULL)
        {
            delete _ipMeca;
            _ipMeca=NULL;
        }
        _ipMeca= (dynamic_cast<IPVariableMechanics *>(ipMeca.clone()));
    }
    const STensor3 & getConstRefToFthI() const { return FthI0;}
    STensor3 & getRefToFthI(){ return FthI0;}

    virtual double get(const int comp) const;
    virtual double & getRef(const int comp);
};

#endif //IPGENERICTM_H_
