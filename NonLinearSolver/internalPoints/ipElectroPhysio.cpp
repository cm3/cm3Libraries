//
// Description: storing class for Electro-Physiology
//
//
// Author:  <A. Jerusalem>, (C) 2013
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "ipElectroPhysio.h"
#include "restartManager.h"
IPElectroPhysio::IPElectroPhysio(): IPVariable(), CaC(0), NaC(0), KC(0)
{
}

IPElectroPhysio::IPElectroPhysio(const IPElectroPhysio &source) : IPVariable(source)
{
   CaC      = source.CaC;
   NaC      = source.NaC;
   KC       = source.KC;
}

IPElectroPhysio &IPElectroPhysio::operator=(const IPVariable &source)
{
  IPVariable::operator=(source);
  const IPElectroPhysio* src = dynamic_cast<const IPElectroPhysio*>(&source);
  if(src!=NULL)
  {
   CaC      = src->CaC;
   NaC      = src->NaC;
   KC       = src->KC;
  }
  return *this;
}

void IPElectroPhysio::createRestart()
{
  IPVariable::restart();
  restartManager::restart(Cac);
  restartManager::restart(NaC);
  restartManager::restart(KC);
  return;
}

IPCableTheoryEP::IPCableTheoryEP() : IPElectroPhysio()
{

}
IPCableTheoryEP::IPCableTheoryEP(const IPCableTheoryEP &source) :IPElectroPhysio(source)
{

}

IPCableTheoryEP &IPCableTheoryEP::operator=(const IPVariable &source)
{
  IPElectroPhysio::operator=(source);
  const IPCableTheoryEP* src = dynamic_cast<const IPCableTheoryEP*>(&source);
  if(src!=NULL)
  {
  }
  return *this;
}
void IPCableTheoryEP::restart()
{
  IPElectroPhysio::restart();
}

IPVariable *IPCableTheoryEP::clone() const
{
  return new IPCableTheoryEP(*this);
}


IPHodgkinHuxleyEP::IPHodgkinHuxleyEP() : IPElectroPhysio()
{

}
IPHodgkinHuxleyEP::IPHodgkinHuxleyEP(const IPHodgkinHuxleyEP &source) :IPElectroPhysio(source)
{

}

IPHodgkinHuxleyEP &IPHodgkinHuxleyEP::operator=(const IPVariable &source)
{
  IPElectroPhysio::operator=(source);
  const IPHodgkinHuxleyEP* src = dynamic_cast<const IPHodgkinHuxleyEP*>(&source);
  if(src!=NULL)
  {
  }
  return *this;
}
void IPHodgkinHuxleyEP::restart()
{
  IPElectroPhysio::restart();
}

IPVariable *IPHodgkinHuxleyEP::clone() const
{
  return new IPHodgkinHuxleyEP(*this);
}

