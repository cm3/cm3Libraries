//
// Description: storing class for j2 elasto-plastic law with electro-physiology interface
//
//
// Author:  <A. Jerusalem>, (C) 2013
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "ipElectroPhysioJ2Hyper.h"
#include "restartManager.h"
IPElectroPhysioJ2Hyper::IPElectroPhysioJ2Hyper() : IPJ2linear(),  _epSurfaceStrain(0), _epDamageParameter(0)
{

  Msg::Error("IPElectroPhysioJ2Hyper::IPElectroPhysioJ2Hyper is not initialized with a ElectroPhysio IP Variables");
ipvEP=NULL;

};

IPElectroPhysioJ2Hyper::IPElectroPhysioJ2Hyper(const J2IsotropicHardening *j2IH, const ElectroPhysioLaw *epl) :
                                                    IPJ2linear(j2IH), _epSurfaceStrain(0), _epDamageParameter(0)
{

  ipvEP=NULL;
  if(epl ==NULL) Msg::Error("IPElectroPhysioJ2Hyper::IPElectroPhysioJ2Hyper has no epl");
  epl->createIPVariable(ipvEP);

};




IPElectroPhysioJ2Hyper::IPElectroPhysioJ2Hyper(const IPElectroPhysioJ2Hyper &source) : IPJ2linear(source)
{
  _epSurfaceStrain=source._epSurfaceStrain;
  _epDamageParameter=source._epDamageParameter;

  if(source.ipvEP != NULL)
  {
    ipvEP = source.ipvEP->clone();
  }
}
IPElectroPhysioJ2Hyper& IPElectroPhysioJ2Hyper::operator=(const IPVariable &source)
{
  IPJ2linear::operator=(source);
  const IPElectroPhysioJ2Hyper* src = dynamic_cast<const IPElectroPhysioJ2Hyper*>(&source);
  if(src != NULL)
  {
    _epSurfaceStrain=src->_epSurfaceStrain;
    _epDamageParameter=src->_epDamageParameter;

    if(ipvEP != NULL) delete ipvEP;
    if(src->ipvEP != NULL)
    {
      ipvEP=src->ipvEP->clone();
    }
  }
  return *this;
}

void IPElectroPhysioJ2Hyper::restart()
{
  IPJ2linear::restart();
  restartManager::restart(_epSurfaceStrain);
  restartManager::restart(_epDamageParameter);
  restartManager::restart(ipvEP);
  return;
}
