//
// C++ Interface: ipvariable
//
// Description: IPNonLocalDamageQuadYieldHyper
//
// Author:  V.D. Nguyen, (C) 2014
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef IPNONLOCALDAMAGEHYPERELASTIC_H_
#define IPNONLOCALDAMAGEHYPERELASTIC_H_

#include "ipHyperelastic.h"
#include "ipCLength.h"
#include "ipDamage.h"
#include "CLengthLaw.h"
#include "DamageLaw.h"

class IPHyperViscoElastoPlasticLocalDamage : public IPHyperViscoElastoPlastic
{
 protected:
  IPDamage*  ipvDam;
  double _damageEnergy;

 public:
  IPHyperViscoElastoPlasticLocalDamage(const J2IsotropicHardening* comp,
                    const J2IsotropicHardening* trac, const kinematicHardening* kin,
                    const int N, const DamageLaw *dl);
  virtual ~IPHyperViscoElastoPlasticLocalDamage();

  IPHyperViscoElastoPlasticLocalDamage(const IPHyperViscoElastoPlasticLocalDamage &source);
  virtual IPHyperViscoElastoPlasticLocalDamage& operator=(const IPVariable &source);

  virtual IPVariable* clone() const{return new IPHyperViscoElastoPlasticLocalDamage(*this);}

  virtual double damageEnergy() const {return _damageEnergy;};
  virtual double& getRefToDamageEnergy() {return _damageEnergy;};

  virtual void restart();

  virtual const IPDamage &getConstRefToIPDamage() const
  {
    if(ipvDam==NULL)
      Msg::Error("IPHyperViscoElastoPlasticLocalDamage: ipvDam not initialized");
    return *ipvDam;
  }
  virtual IPDamage &getRefToIPDamage()
  {
    if(ipvDam==NULL)
      Msg::Error("IPHyperViscoElastoPlasticLocalDamage: ipvDam not initialized");
    return *ipvDam;
  }

  virtual double getDamage() const
  {
    if(ipvDam==NULL)
      Msg::Error("IPHyperViscoElastoPlasticLocalDamage: ipvDam not initialized");
    return ipvDam->getDamage();
  }
  virtual double getMaximalP() const
  {
    if(ipvDam==NULL)
      Msg::Error("IPHyperViscoElastoPlasticLocalDamage: ipvDam not initialized");
    return ipvDam->getMaximalP();
  }
  virtual double getDeltaDamage() const
  {
    if(ipvDam==NULL)
      Msg::Error("IPHyperViscoElastoPlasticLocalDamage: ipvDam not initialized");
    return ipvDam->getDeltaDamage();
  }
  virtual double getDDamageDp() const
  {
    if(ipvDam==NULL)
      Msg::Error("IPHyperViscoElastoPlasticLocalDamage: ipvDam not initialized");
    return ipvDam->getDDamageDp();
  }
  virtual const STensor3 &getConstRefToDDamageDFe() const
  {
    if(ipvDam==NULL)
      Msg::Error("IPHyperViscoElastoPlasticLocalDamage: ipvDam not initialized");
    return ipvDam->getConstRefToDDamageDFe();
  }

};


class IPHyperViscoElastoPlasticNonLocalDamage : public IPHyperViscoElastoPlasticLocalDamage
{
 protected:
  IPCLength* ipvCL;
  double _nonlocalPlasticStrain;
  double _DirreversibleEnergyDNonLocalVariable;

 public:
  IPHyperViscoElastoPlasticNonLocalDamage(const J2IsotropicHardening* comp,
                    const J2IsotropicHardening* trac, const kinematicHardening* kin,
                    const int N,
                    const CLengthLaw *cll, const DamageLaw *dl);
  virtual ~IPHyperViscoElastoPlasticNonLocalDamage();

  IPHyperViscoElastoPlasticNonLocalDamage(const IPHyperViscoElastoPlasticNonLocalDamage &source);
  virtual IPHyperViscoElastoPlasticNonLocalDamage& operator=(const IPVariable &source);

  virtual IPVariable* clone() const{return new IPHyperViscoElastoPlasticNonLocalDamage(*this);}

  virtual const double& getDIrreversibleEnergyDNonLocalVariable() const {return _DirreversibleEnergyDNonLocalVariable;};
	virtual double& getRefToDIrreversibleEnergyDNonLocalVariable() {return _DirreversibleEnergyDNonLocalVariable;};

  virtual void restart();
  virtual double getCurrentPlasticStrain() const { return _epspbarre;}
  virtual double &getRefToCurrentPlasticStrain() { return _epspbarre;}
  virtual double getEffectivePlasticStrain() const { return _nonlocalPlasticStrain;}
  virtual double &getRefToEffectivePlasticStrain() { return _nonlocalPlasticStrain;}

  virtual const IPCLength &getConstRefToIPCLength() const
  {
    if(ipvCL==NULL)
      Msg::Error("IPHyperViscoElastoPlasticNonLocalDamage: ipvCL not initialized");
    return *ipvCL;
  }
  virtual IPCLength &getRefToIPCLength()
  {
    if(ipvCL==NULL)
      Msg::Error("IPHyperViscoElastoPlasticNonLocalDamage: ipvCL not initialized");
    return *ipvCL;
  }
  virtual const STensor3 &getConstRefToCharacteristicLength() const
  {
    if(ipvCL==NULL)
      Msg::Error("IPHyperViscoElastoPlasticNonLocalDamage: ipvCL not initialized");
    return ipvCL->getConstRefToCL();
  }
  virtual STensor3 &getRefToCharacteristicLength() const
  {
    if(ipvCL==NULL)
      Msg::Error("IPHyperViscoElastoPlasticNonLocalDamage: ipvCL not initialized");
    return ipvCL->getRefToCL();
  }
};

class IPHyperViscoElastoPlasticMultipleLocalDamage : public IPHyperViscoElastoPlastic{
  protected:
    int numNonLocalVariable;
    std::vector<IPDamage*> ipvDam;

    double _damageEnergy;

  public:
    IPHyperViscoElastoPlasticMultipleLocalDamage(const J2IsotropicHardening* comp,
                    const J2IsotropicHardening* trac, const kinematicHardening* kin,
                    const int N, const std::vector<DamageLaw*>& dl);
    virtual ~IPHyperViscoElastoPlasticMultipleLocalDamage();
    IPHyperViscoElastoPlasticMultipleLocalDamage(const IPHyperViscoElastoPlasticMultipleLocalDamage& source);
    virtual IPHyperViscoElastoPlasticMultipleLocalDamage& operator=(const IPVariable& source);

    virtual double damageEnergy() const {return _damageEnergy;};
    virtual double& getRefToDamageEnergy() {return _damageEnergy;};

    virtual int getNumNonLocalVariable() const{return numNonLocalVariable;};
    virtual IPVariable* clone() const{return new IPHyperViscoElastoPlasticMultipleLocalDamage(*this);}

    virtual void restart();

    virtual const IPDamage &getConstRefToIPDamage(const int i) const
    {
      if(ipvDam[i]==NULL)
        Msg::Error("IPHyperViscoElastoPlasticMultipleLocalDamage: ipvCL[%d] not initialized",i);
      return *ipvDam[i];
    }
    virtual IPDamage &getRefToIPDamage(const int i)
    {
      if(ipvDam[i]==NULL)
        Msg::Error("IPHyperViscoElastoPlasticMultipleLocalDamage: ipvCL[%d] not initialized",i);
      return *ipvDam[i];
    }

    virtual double getDamage(const int i) const
    {
      if(ipvDam[i]==NULL)
        Msg::Error("IPHyperViscoElastoPlasticMultipleLocalDamage: ipvCL[%d] not initialized",i);
      return ipvDam[i]->getDamage();
    }
    virtual double getMaximalP(const int i) const
    {
      if(ipvDam[i]==NULL)
        Msg::Error("IPHyperViscoElastoPlasticMultipleLocalDamage: ipvCL[%d] not initialized",i);
      return ipvDam[i]->getMaximalP();
    }
    virtual double getDeltaDamage(const int i) const
    {
      if(ipvDam[i]==NULL)
        Msg::Error("IPHyperViscoElastoPlasticMultipleLocalDamage: ipvCL[%d] not initialized",i);
      return ipvDam[i]->getDeltaDamage();
    }
    virtual double getDDamageDp( const int i) const
    {
      if(ipvDam[i]==NULL)
        Msg::Error("IPHyperViscoElastoPlasticMultipleLocalDamage: ipvCL[%d] not initialized",i);
      return ipvDam[i]->getDDamageDp();
    }
    virtual const STensor3 &getConstRefToDDamageDFe(const int i) const
    {
      if(ipvDam[i]==NULL)
        Msg::Error("IPHyperViscoElastoPlasticMultipleLocalDamage: ipvCL[%d] not initialized",i);
      return ipvDam[i]->getConstRefToDDamageDFe();
    }
};



class IPHyperViscoElastoPlasticMultipleNonLocalDamage : public IPHyperViscoElastoPlasticMultipleLocalDamage{
  protected:
    std::vector<IPCLength*> ipvCL;
    // bareps
    double _nonlocalEqPlasticStrain;
    double _nonlocalFailurePlasticity;
    std::vector<double> _DirreversibleEnergyDNonLocalVariable;

  public:
    IPHyperViscoElastoPlasticMultipleNonLocalDamage(const J2IsotropicHardening* comp,
                    const J2IsotropicHardening* trac, const kinematicHardening* kin,
                    const int N, const std::vector<CLengthLaw*>& cll, const std::vector<DamageLaw*>& dl);
    virtual ~IPHyperViscoElastoPlasticMultipleNonLocalDamage();
    IPHyperViscoElastoPlasticMultipleNonLocalDamage(const IPHyperViscoElastoPlasticMultipleNonLocalDamage& source);
    virtual IPHyperViscoElastoPlasticMultipleNonLocalDamage& operator=(const IPVariable& source);

    virtual IPVariable* clone() const{return new IPHyperViscoElastoPlasticMultipleNonLocalDamage(*this);}


    virtual const double& getDIrreversibleEnergyDNonLocalVariable(const int i) const {return _DirreversibleEnergyDNonLocalVariable[i];};
    virtual double& getRefToDIrreversibleEnergyDNonLocalVariable(const int i) {return _DirreversibleEnergyDNonLocalVariable[i];};

    virtual void restart();
    virtual double getCurrentPlasticStrain() const { return _epspbarre;}
    virtual double &getRefToCurrentPlasticStrain() { return _epspbarre;}

    virtual double getEffectivePlasticStrain() const { return _nonlocalEqPlasticStrain;}
    virtual double &getRefToEffectivePlasticStrain() { return _nonlocalEqPlasticStrain;}

    virtual double getNonLocalFailurePlasticity() const {return _nonlocalFailurePlasticity;};
    virtual double & getRefToNonLocalFailurePlasticity() {return _nonlocalFailurePlasticity;};

    virtual const IPCLength &getConstRefToIPCLength(const int i) const
    {
      if(ipvCL[i]==NULL)
        Msg::Error("IPHyperViscoElastoPlasticMultipleNonLocalDamage: ipvCL[%d] not initialized",i);
      return *ipvCL[i];
    }
    virtual IPCLength &getRefToIPCLength(const int i)
    {
      if(ipvCL[i]==NULL)
        Msg::Error("IPHyperViscoElastoPlasticMultipleNonLocalDamage: ipvCL[%d] not initialized",i);
      return *ipvCL[i];
    }
    virtual const STensor3 &getConstRefToCharacteristicLength(const int i) const
    {
      if(ipvCL[i]==NULL)
        Msg::Error("IPHyperViscoElastoPlasticMultipleNonLocalDamage: ipvCL[%d] not initialized",i);
      return ipvCL[i]->getConstRefToCL();
    }
    virtual STensor3 &getRefToCharacteristicLength(const int i) const
    {
      if(ipvCL[i]==NULL)
        Msg::Error("IPHyperViscoElastoPlasticMultipleNonLocalDamage: ipvCL[%d] not initialized",i);
      return ipvCL[i]->getRefToCL();
    }
};

#endif // IPNONLOCALDAMAGEHYPERELASTIC_H_
