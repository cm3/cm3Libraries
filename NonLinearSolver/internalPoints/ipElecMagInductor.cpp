//
// Created by vinayak on 24.08.22.
//
//
// Description: storing class for Electro-Magnetic Inductor region
// computing normal direction to a gauss point to impose current
// density js0 in inductor
//
// Author:  <Vinayak GHOLAP>, (C) 2022
//
// Copyright: See COPYING file that comes with this distribution
//
//

#include "ipElecMagInductor.h"

IPElecMagInductor::IPElecMagInductor() : IPElecMagGenericThermoMech()
{
    Msg::Error("Cannot create default IPNormalDirectionGP");
}

IPElecMagInductor::IPElecMagInductor(const SVector3 &GaussP, const SVector3 &Centroid, const SVector3 &CentralAxis):
IPElecMagGenericThermoMech()
{
    SVector3 vec1(GaussP-Centroid);
    SVector3 normal(crossprod(CentralAxis,vec1));
    if (normal.norm() != 1.0)
    {
        normal.normalize();
    }
    _normal = normal;
}

IPElecMagInductor::IPElecMagInductor(const IPElecMagInductor &source): IPElecMagGenericThermoMech(source),
_normal(source._normal)
{}

IPElecMagInductor& IPElecMagInductor::operator=(const IPVariable &source)
{
    IPElecMagGenericThermoMech::operator=(source);
    const IPElecMagInductor* src = dynamic_cast<const IPElecMagInductor*>(&source);
    if (src != NULL)
    {
        _normal = src->_normal;
    }
    return *this;
}

double IPElecMagInductor::defoEnergy() const
{
    return  IPElecMagGenericThermoMech::defoEnergy();
}
double IPElecMagInductor::plasticEnergy() const
{
    return  IPElecMagGenericThermoMech::plasticEnergy();
}
double IPElecMagInductor::damageEnergy() const
{
    return  IPElecMagGenericThermoMech::damageEnergy();
}
double IPElecMagInductor::irreversibleEnergy() const
{
    return  IPElecMagGenericThermoMech::irreversibleEnergy();
}

double IPElecMagInductor::get(const int comp) const
{
    return IPElecMagGenericThermoMech::get(comp);
}

double & IPElecMagInductor::getRef(const int comp)
{
    return IPElecMagGenericThermoMech::getRef(comp);
}

void IPElecMagInductor::restart()
{
    IPElecMagGenericThermoMech::restart();
    restartManager::restart(_normal);
    return;
}