//
// C++ Interface: ipvariable
//
// Description: IPHyperelastic
//
// Author:  V.D. Nguyen, (C) 2014
//
// Copyright: See COPYING file that comes with this distribution
//
//

#include "ipHyperelastic.h"
#include "restartManager.h"
#include "STensorOperations.h"

IPHyperViscoElastic::IPHyperViscoElastic(const int N):IPVariableMechanics(),_N(N),_elasticEnergy(0.),_Ee(0.),_kirchhoff(0.),
      _irreversibleEnergy(0.),_DirreversibleEnergyDF(0.), _viscousEnergyPart(0.), _dElasticEnergyPartdF(0.), _dViscousEnergyPartdF(0.), _elasticBulkPropertyScaleFactor(1.), _elasticShearPropertyScaleFactor(1.),
	  _intA(0.), _intB(0.), _DintA(0.), _DintB(0.), _Re(0.), _Re0(0.), _Ee0(0.)
{
  _A.clear(); _A_rot.clear();
  _B.clear();
  _psi_branch.clear();
  for (int i=0; i< N; i++){
    STensor3 el(0.);
    _A.push_back(el);
    _A_rot.push_back(el);
    _B.push_back(0.);
    _psi_branch.push_back(0.);
  }
};
IPHyperViscoElastic::IPHyperViscoElastic(const IPHyperViscoElastic& src): IPVariableMechanics(src), _Ee(src._Ee),
    _N(src._N),_A(src._A),_B(src._B),_elasticEnergy(src._elasticEnergy), _kirchhoff(src._kirchhoff),
    _irreversibleEnergy(src._irreversibleEnergy),_DirreversibleEnergyDF(src._DirreversibleEnergyDF),
    _viscousEnergyPart(src._viscousEnergyPart), _dElasticEnergyPartdF(src._dElasticEnergyPartdF), 
    _dViscousEnergyPartdF(src._dViscousEnergyPartdF), _elasticBulkPropertyScaleFactor(src._elasticBulkPropertyScaleFactor), _elasticShearPropertyScaleFactor(src._elasticShearPropertyScaleFactor),
	_intA(src._intA),_intB(src._intB), _psi_branch(src._psi_branch), _DintA(src._DintA),_DintB(src._DintB), _A_rot(src._A_rot), _Re(src._Re), _Re0(src._Re0), _Ee0(src._Ee0)

{

};

IPHyperViscoElastic& IPHyperViscoElastic::operator =(const IPVariable& src){
  IPVariableMechanics::operator =(src);
  const IPHyperViscoElastic* psrc = dynamic_cast<const IPHyperViscoElastic*>(&src);
  if (psrc != NULL){
    _Ee = psrc->_Ee;
    _kirchhoff = psrc->_kirchhoff;
    _N = psrc->_N;
    _A = psrc->_A;
    _B = psrc->_B;
    _elasticEnergy = psrc->_elasticEnergy;
    _irreversibleEnergy = psrc->_irreversibleEnergy;
    _DirreversibleEnergyDF = psrc->_DirreversibleEnergyDF;
    _viscousEnergyPart = psrc->_viscousEnergyPart;
    _dElasticEnergyPartdF = psrc->_dElasticEnergyPartdF;
    _dViscousEnergyPartdF = psrc->_dViscousEnergyPartdF;
    _elasticBulkPropertyScaleFactor= psrc->_elasticBulkPropertyScaleFactor;
    _elasticShearPropertyScaleFactor= psrc->_elasticShearPropertyScaleFactor;
    _intA= psrc->_intA;
    _intB= psrc->_intB;
    _DintA= psrc->_DintA;
    _DintB= psrc->_DintB;
    _psi_branch= psrc->_psi_branch;
    _A_rot=psrc->_A_rot;
    _Re=psrc->_Re;
    _Re0=psrc->_Re0;
    _Ee0=psrc->_Ee0;
  }
  return *this;
};

void IPHyperViscoElastic::restart() {
  IPVariableMechanics::restart();
  restartManager::restart(_elasticEnergy);
  restartManager::restart(_N);
  restartManager::restart(_A);
  restartManager::restart(_B);
  restartManager::restart(_Ee);
  restartManager::restart(_kirchhoff);
  restartManager::restart(_irreversibleEnergy);
  restartManager::restart(_DirreversibleEnergyDF);
  restartManager::restart(_viscousEnergyPart);
  restartManager::restart(_dElasticEnergyPartdF);
  restartManager::restart(_dViscousEnergyPartdF);
  restartManager::restart(_elasticBulkPropertyScaleFactor);
  restartManager::restart(_elasticShearPropertyScaleFactor);
  restartManager::restart(_intA);
  restartManager::restart(_intB);
  restartManager::restart(_DintA);
  restartManager::restart(_DintB);
  restartManager::restart(_psi_branch);
  restartManager::restart(_A_rot);
  restartManager::restart(_Re);
  restartManager::restart(_Re0);
  restartManager::restart(_Ee0);
};

void IPHyperViscoElastic::getViscoElasticStrain(int i, STensor3& Ev) const
{
  if (i > _A.size() or i < 1)
  {
    Msg::Error("viscoelastic branch %d does not exist",i);
  }
  else
  {
    Ev = _A[i-1];
    Ev(0,0) += _B[i-1]/3.;
    Ev(1,1) += _B[i-1]/3.;
    Ev(2,2) += _B[i-1]/3.;
  }
};

void IPHyperViscoElastic::resetA()
{
  for(int i=0; i<_A.size(); i++)
    STensorOperation::zero(_A[i]);
  getRefToElasticEnergyPart()=0.;
  getRefToViscousEnergyPart()=0.;
}
void IPHyperViscoElastic::resetB()
{
  for(int i=0;i<_B.size(); i++)
    _B[i]=0.;
  getRefToElasticEnergyPart()=0.;
  getRefToViscousEnergyPart()=0.;
}

IPHyperViscoElastoPlastic::IPHyperViscoElastoPlastic(const J2IsotropicHardening* comp,
                    const J2IsotropicHardening* trac,
                    const kinematicHardening* kin, const int N):IPHyperViscoElastic(N),
                    _backsig(0),_Fp(1.),_epspbarre(0.),_nup(0.), _Gamma(0.),
                    _epspCompression(0.),_epspTraction(0.),_epspShear(0.),
                    _DgammaDt(0.),_Fe(1.),_r(-1e100), _inPostFailureStage(false),
                    _DgammaDF(0.),_gF(0.),_dgFdF(0.), _plasticEnergy(0.),
                    _dissipationActive(false),_dissipationBlocked(false),
                    _dPlasticEnergyPartdF(0)

{

  _ipCompression = NULL;
  _ipTraction = NULL;
  _ipKinematic = NULL;

  if (comp!= NULL)  comp->createIPVariable(_ipCompression);
  if (trac != NULL) trac->createIPVariable(_ipTraction);
  if (kin != NULL) kin->createIPVariable(_ipKinematic);
};

IPHyperViscoElastoPlastic::IPHyperViscoElastoPlastic(const IPHyperViscoElastoPlastic& src):IPHyperViscoElastic(src),
      _Fp(src._Fp),_epspbarre(src._epspbarre),_nup(src._nup), _Gamma(src._Gamma),
      _epspCompression(src._epspCompression),_epspTraction(src._epspTraction),_epspShear(src._epspShear),
      _backsig(src._backsig),_DgammaDt(src._DgammaDt),_Fe(src._Fe),
       _inPostFailureStage(src._inPostFailureStage),_r(src._r),_DgammaDF(src._DgammaDF),_gF(src._gF),_dgFdF(src._dgFdF),
       _plasticEnergy(src._plasticEnergy),_dissipationActive(src._dissipationActive),_dissipationBlocked(src._dissipationBlocked),
       _dPlasticEnergyPartdF(src._dPlasticEnergyPartdF)
{
  if (src._ipCompression != NULL)
    _ipCompression = dynamic_cast<IPJ2IsotropicHardening*>(src._ipCompression->clone());
  else
    _ipCompression = NULL;

  if (src._ipTraction != NULL)
    _ipTraction = dynamic_cast<IPJ2IsotropicHardening*>(src._ipTraction->clone());
  else
   _ipTraction = NULL;

  if (src._ipKinematic != NULL)
    _ipKinematic = dynamic_cast<IPKinematicHardening*>(src._ipKinematic->clone());
  else
    _ipKinematic = NULL;
};

IPHyperViscoElastoPlastic& IPHyperViscoElastoPlastic::operator =(const IPVariable &source){
  IPHyperViscoElastic::operator=(source);
  const IPHyperViscoElastoPlastic* ps = dynamic_cast<const IPHyperViscoElastoPlastic*>(&source);
  if (ps != NULL){
    _backsig = ps->_backsig;
    _Fp = ps->_Fp;
    _epspbarre = ps->_epspbarre;
    _nup = ps->_nup;
    _Gamma = ps->_Gamma;
    _epspCompression = ps->_epspCompression;
    _epspTraction = ps->_epspTraction;
    _epspShear = ps->_epspShear;
    _DgammaDt = ps->_DgammaDt;
    _Fe = ps->_Fe;
    _inPostFailureStage = ps->_inPostFailureStage;
    _r = ps->_r;
    _DgammaDF = ps->_DgammaDF;
    _gF = ps->_gF;
    _dgFdF = ps->_dgFdF;

    _plasticEnergy = ps->_plasticEnergy;
    _dissipationActive = ps->_dissipationActive;
    _dissipationBlocked = ps->_dissipationBlocked;

    _dPlasticEnergyPartdF=ps->_dPlasticEnergyPartdF;

    if ( ps->_ipCompression != NULL) {
      if (_ipCompression == NULL){
        _ipCompression = dynamic_cast<IPJ2IsotropicHardening*>(ps->_ipCompression->clone());
      }
      else{
        _ipCompression->operator=(*dynamic_cast<const IPJ2IsotropicHardening*>(ps->_ipCompression));
      }
    }

     if ( ps->_ipTraction != NULL) {
      if (_ipTraction == NULL){
        _ipTraction = dynamic_cast<IPJ2IsotropicHardening*>(ps->_ipTraction->clone());
      }
      else{
        _ipTraction->operator=(*dynamic_cast<const IPJ2IsotropicHardening*>(ps->_ipTraction));
      }
    }

    if ( ps->_ipKinematic != NULL) {
      if (_ipKinematic == NULL){
        _ipKinematic = dynamic_cast<IPKinematicHardening*>(ps->_ipKinematic->clone());
      }
      else{
        _ipKinematic->operator=(*dynamic_cast<const IPKinematicHardening*>(ps->_ipKinematic));
      }
    }

  }
  return *this;
};

IPHyperViscoElastoPlastic::~IPHyperViscoElastoPlastic(){
  if (_ipCompression != NULL) delete _ipCompression;
  _ipCompression = NULL;
  if (_ipTraction != NULL) delete _ipTraction;
  _ipTraction = NULL;
  if (_ipKinematic!= NULL) delete _ipKinematic;
  _ipKinematic = NULL;
};


void IPHyperViscoElastoPlastic::restart() {
  IPHyperViscoElastic::restart();
  if (_ipCompression != NULL)
    restartManager::restart(_ipCompression);
  if (_ipTraction != NULL)
    restartManager::restart(_ipTraction);
  if (_ipKinematic != NULL)
    restartManager::restart(_ipKinematic);

  restartManager::restart(_backsig);
  restartManager::restart(_Fe);
  restartManager::restart(_epspbarre);
  restartManager::restart(_Fp);
  restartManager::restart(_nup);
  restartManager::restart(_Gamma);
  restartManager::restart(_epspCompression);
  restartManager::restart(_epspTraction);
  restartManager::restart(_epspShear);
  restartManager::restart(_DgammaDt);
  restartManager::restart(_inPostFailureStage);
  restartManager::restart(_r);
  restartManager::restart(_DgammaDF);
  restartManager::restart(_gF);
  restartManager::restart(_dgFdF);
  restartManager::restart(_plasticEnergy);
  restartManager::restart(_dissipationActive);
  restartManager::restart(_dissipationBlocked);
  restartManager::restart(_dPlasticEnergyPartdF);
  return;
}

const STensor3& IPHyperViscoElastoPlastic::getConstRefToBackStress() const{
  return _backsig;
};

STensor3& IPHyperViscoElastoPlastic::getRefToBackStress(){
  return _backsig;
};


const STensor3& IPHyperViscoElastoPlastic::getConstRefToFe() const{
  return _Fe;
};
STensor3& IPHyperViscoElastoPlastic::getRefToFe(){
  return _Fe;
};

const STensor3 & IPHyperViscoElastoPlastic::getConstRefToFp() const{
  return _Fp;
};
STensor3& IPHyperViscoElastoPlastic::getRefToFp(){
  return _Fp;
};

const double& IPHyperViscoElastoPlastic::getConstRefToCompressionPlasticStrain() const{
  return _epspCompression;
};
double & IPHyperViscoElastoPlastic::getRefToCompressionPlasticStrain(){
  return _epspCompression;
};

const double& IPHyperViscoElastoPlastic::getConstRefToTractionPlasticStrain() const{
  return _epspTraction;
};
double & IPHyperViscoElastoPlastic::getRefToTractionPlasticStrain(){
  return _epspTraction;
};

const double& IPHyperViscoElastoPlastic::getConstRefToShearPlasticStrain() const{
  return _epspShear;
};
double & IPHyperViscoElastoPlastic::getRefToShearPlasticStrain(){
  return _epspShear;
};

const double& IPHyperViscoElastoPlastic::getConstRefToEqPlasticStrain() const{
  return _epspbarre;
};
double& IPHyperViscoElastoPlastic::getRefToEqPlasticStrain(){
  return _epspbarre;
};

const double IPHyperViscoElastoPlastic::getConstRefToPlasticPoissonRatio() const{
  return _nup;
};
double& IPHyperViscoElastoPlastic::getRefToPlasticPoissonRatio(){
  return _nup;
};

const double& IPHyperViscoElastoPlastic::getConstRefToPlasticDeformationRate() const{
  return _DgammaDt;
};
double& IPHyperViscoElastoPlastic::getRefToPlasticDeformationRate(){
  return _DgammaDt;
};

const IPJ2IsotropicHardening& IPHyperViscoElastoPlastic::getConstRefToIPCompressionHardening() const{
  if(_ipCompression==NULL)
      Msg::Error("IPHyperViscoElastoPlastic: _ipCompression not initialized");
  return *_ipCompression;
};
IPJ2IsotropicHardening& IPHyperViscoElastoPlastic::getRefToIPCompressionHardening(){
  if(_ipCompression==NULL)
      Msg::Error("IPHyperViscoElastoPlastic: _ipCompression not initialized");
  return *_ipCompression;
};

const IPJ2IsotropicHardening& IPHyperViscoElastoPlastic::getConstRefToIPTractionHardening() const{
  if(_ipTraction==NULL)
      Msg::Error("IPHyperViscoElastoPlastic: _ipTraction not initialized");
  return *_ipTraction;
};
IPJ2IsotropicHardening& IPHyperViscoElastoPlastic::getRefToIPTractionHardening(){
  if(_ipTraction==NULL)
      Msg::Error("IPHyperViscoElastoPlastic: _ipTraction not initialized");
  return *_ipTraction;
};

const IPKinematicHardening& IPHyperViscoElastoPlastic::getConstRefToKinematicHardening() const{
  if (_ipKinematic == NULL)
    Msg::Error("IPHyperViscoElastoPlastic: _ipKinematic not initialized");
  return *_ipKinematic;
};

IPKinematicHardening& IPHyperViscoElastoPlastic::getRefToKinematicHardening(){
  if (_ipKinematic == NULL)
    Msg::Error("IPHyperViscoElastoPlastic: _ipKinematic not initialized");
  return *_ipKinematic;
};


void IPHyperViscoElastoPlastic::resetPlasticity()
{
   STensorOperation::zero(_backsig); // backstress
  _Gamma=0.;
  _epspbarre=0.; // equivalent plastic strain
   STensorOperation::unity(_Fp);
  _epspCompression=0.;
  _epspTraction=0.;
  _epspShear=0.;

  getRefToPlasticEnergyPart()=0.;
}
