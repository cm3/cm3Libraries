//
// C++ Interface: Quadrature Rule
//
// Description: Define 2D Gauss-Lobatto quadrature rule (number of point 3 to 6)
//
//
// Author:  <L. Noels>, (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "GaussLobatto2DQuadratureRules.h"
#include "GaussLobattoQuadrature.h"
int GaussLobatto2DQuadrature::getIntPoints(MElement *e, IntPt **GP)
{
  switch(e->getType()){
   case TYPE_PNT :
    *GP = getGaussLobattoQuadrature0DPts(0);
    return getNGaussLobattoQuadrature0DPts(0);
   case TYPE_LIN :
    *GP = getGaussLobattoQuadrature1DPts(_npts);
    return getNGaussLobattoQuadrature1DPts(_npts);
   case TYPE_TRI :
    *GP = getGaussLobattoQuadrature2DPtsTri(_npts);
    return getNGaussLobattoQuadrature2DPtsTri(_npts);
   case TYPE_QUA :
    *GP = getGaussLobattoQuadrature2DPtsQuad(_npts);
    return getNGaussLobattoQuadrature2DPtsQuad(_npts);
   default:
    Msg::Error("The Gauss-Lobatto quadrature rule is not defined for this type of element");
    return 0;
  }
}
