//
// Created by vinayak on 17.08.22.
//
//
// Description: storing class for Electro-Magnetic Generic-Thermo-Mechanics
//
//
// Author:  <Vinayak GHOLAP>, (C) 2022
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef IPELECMAGGENERICTHERMOMECH_H_
#define IPELECMAGGENERICTHERMOMECH_H_

#include "ipCoupledThermoMechanics.h"

class IPElecMagGenericThermoMech : public IPVariable
{
protected:
    IPCoupledThermoMechanics * _ipThermoMech;
    double _EMEnergy;

public:
    IPElecMagGenericThermoMech();
    ~IPElecMagGenericThermoMech()
    {
        if(_ipThermoMech != NULL)
        {
            delete _ipThermoMech;
            _ipThermoMech = NULL;
        }
    }

    IPElecMagGenericThermoMech(const IPElecMagGenericThermoMech& source);
    virtual IPElecMagGenericThermoMech& operator=(const IPVariable& source);
    virtual double defoEnergy() const{return _EMEnergy;};
    virtual double & getRefToDefoEnergy() {return _EMEnergy;};
    virtual double TMDefoEnergy() const{return getConstRefToIpThermoMech().defoEnergy();};
    virtual double plasticEnergy() const{return getConstRefToIpThermoMech().plasticEnergy();}
    virtual double damageEnergy() const{return  getConstRefToIpThermoMech().damageEnergy();}
    virtual double irreversibleEnergy() const{return  getConstRefToIpThermoMech().irreversibleEnergy();}
    virtual double getThermalEnergy() const{return getConstRefToIpThermoMech().getThermalEnergy();};
    virtual void restart();
    virtual void setValueForBodyForce(const STensor43& K, const int ModuliType){
        _ipThermoMech->setValueForBodyForce(K,ModuliType);
    }
    virtual const STensor43 &getConstRefToTangentModuli() const{return _ipThermoMech->getConstRefToTangentModuli();};
    virtual void computeBodyForce(const STensor33& G, SVector3& B) const {
        _ipThermoMech->computeBodyForce(G,B);
    }
    virtual IPVariable* clone() const {return new IPElecMagGenericThermoMech(*this);};

    const IPCoupledThermoMechanics& getConstRefToIpThermoMech() const {return *_ipThermoMech;}
    IPCoupledThermoMechanics& getRefToIpThermoMech() {return *_ipThermoMech;}
    void setIpThermoMech(IPVariable& ipThermoMech)
    {
        if(_ipThermoMech!=NULL)
        {
            delete _ipThermoMech;
            _ipThermoMech=NULL;
        }
        _ipThermoMech= (dynamic_cast<IPCoupledThermoMechanics*>((ipThermoMech.clone())));
    }
    virtual double get(const int comp) const;
    virtual double & getRef(const int comp);
};
#endif //IPELECMAGGENERICTHERMOMECH_H_
