//
// Description: storing class for j2 linear elasto-plastic law
//
//
// Author:  <Gauthier BECKER>, (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "ipTransverseIsoCurvature.h"
#include "restartManager.h"
IPTransverseIsoCurvature::IPTransverseIsoCurvature() : IPTransverseIsotropic() {Msg::Error("Cannot construct default IPTransverseIsoCurvature");}

IPTransverseIsoCurvature::IPTransverseIsoCurvature(const IPTransverseIsoCurvature &source) : IPTransverseIsotropic(source), _A(source._A){}
IPTransverseIsoCurvature& IPTransverseIsoCurvature::operator=(const IPVariable &source)
{
  IPTransverseIsotropic::operator=(source);
  const IPTransverseIsoCurvature* src = dynamic_cast<const IPTransverseIsoCurvature*>(&source);
	if (src != NULL){
		_A = src->_A;
	}
	return *this;
}

void IPTransverseIsoCurvature::restart()
{
  IPTransverseIsotropic::restart();
  restartManager::restart(_A);
  return;
}

IPTransverseIsoCurvature::IPTransverseIsoCurvature(const SVector3 &GaussP, const SVector3 &Centroid, const SVector3 &PointStrart1, const SVector3 &PointStrart2, const double init_Angle) : 
                          IPTransverseIsotropic() {

  SVector3 init_A(PointStrart1 - Centroid);
  SVector3 Vec1(PointStrart2 - Centroid);
  SVector3 N_local(crossprod(init_A,Vec1));
  SVector3 Vec2( GaussP - Centroid);
  SVector3 Proj_A(crossprod(N_local,Vec2));
  if(Proj_A.norm()<1.e-18)
  {
    N_local.print("P1-Centroid wedge P2-Centroid");
    Vec2.print("GP - Centroid");
    Proj_A.print("(P1-Centroid wedge P2-Centroid) wedge (GP-Centroid)");
    Msg::Error("cannot projet point in IPTransverseIsoCurvature::IPTransverseIsoCurvature");
  }
  Proj_A.normalize(); 
  if(fabs(fabs(init_Angle)-90.)<1.e-12){
    _A = N_local;}
  else{
    N_local.normalize();
    _A = Proj_A + tan(init_Angle*3.14159/180.)*N_local;
  }
  _A.normalize();
}

