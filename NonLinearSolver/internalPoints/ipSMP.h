//
// Description: storing class for linear thermomechanical law it is a "virtual implementation" you have to define an ipvariable
//              in your project which derive from this one.
// Author:  <L. Homsy>, (C) 2013
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef IPSMP_H_
#define IPSMP_H_
#include "ipLinearThermoMechanics.h"
#include "STensor3.h"
#include "ipvariable.h"
//#include "mlawSMP.h"

class IPSMP : public IPVariableMechanics//: public IPLinearThermoMechanics  //, public mlawSMP
{
public:
   double _SMPEnergy;
protected:
  STensor3 Fp2,Fp1;
  double Sa1,phia1,Sastar1,phiastar1;
  //double _SMPEnergy;
  double _thermalEnergy;
  double _epsilon1; // equivalent plastic strain
 public:
  IPSMP(double _Sa1, double _phia, double _Sastar1);

  //IPSMP(const materialLaw* l = NULL);
  IPSMP(const IPSMP &source);
  IPSMP& operator=(const IPVariable &source);
  //virtual double defoEnergy() const ;
  virtual double defoEnergy() const{return _SMPEnergy;}
  virtual double getThermalEnergy() const {return _thermalEnergy;};

  virtual STensor3 &getRefToFp1() {return Fp1;}
  virtual const STensor3 &getConstRefToFp1() const {return Fp1;}
  virtual STensor3 &getRefToFp2() {return Fp2;}
  virtual const STensor3 &getConstRefToFp2() const {return Fp2;}
  virtual double &getRefToSa1() {return Sa1;}
  virtual double getSa1() const{return Sa1;}
  virtual double &getRefToSastar1() {return Sastar1;}
  virtual double getSastar1()  const {return Sastar1;}
  virtual double &getRefToPhia1() {return phia1;}
  virtual double getPhia1()  const {return phia1;}
  virtual double &getRefToPhiastar1() {return phiastar1;}
  virtual double getPhiastar1()   const {return phiastar1;}
  virtual void restart();
  virtual IPVariable* clone() const {return new IPSMP(*this);};

  virtual double& getRefToEquivalentPlasticDefo1() {return _epsilon1;};
  virtual const double& getConstRefToEquivalentPlasticDefo1() const {return _epsilon1;};
  
  //virtual double& getRefTodefoEnergy() {return _SMPEnergy;};
//  virtual const double& getConstRefTodefoEnergy() const {return _SMPEnergy;};
  
protected:
  IPSMP();

};

#endif // IPSMP_H_
