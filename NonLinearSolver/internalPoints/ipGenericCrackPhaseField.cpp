//
// Created by mohib on 12/6/24.
//

#include "ipGenericCrackPhaseField.h"

IPGenericCrackPhaseField::IPGenericCrackPhaseField(const CLengthLaw* cLLaw) : IPVariableMechanics() {
    
    _ipMeca = NULL;

    localDamageVariable = 0.0;
    nonLocalDamageVariable = 0.0;

    _ipvCL = NULL;
    STensorOperation::zero(_PMax);
    _effEner = 0.0;
    _defoEnergy = 0.0;
    _plasticEnergy = 0.0;
    _irreversibleEnergy = 0.0;
    _damageEnergy = 0.0;
    STensorOperation::zero(_DirreversibleEnergyDF);
    _DirreversibleEnergyDNonLocalVariable = 0.0;

    if ( cLLaw == NULL )
    {
        Msg::Error("IPGenericCrackPhaseField::IPGenericCrackPhaseField has no c_l law");
    }
    else
    {
        cLLaw->createIPVariable(_ipvCL);
    }
}



IPGenericCrackPhaseField::IPGenericCrackPhaseField(const IPGenericCrackPhaseField &source) : 
IPVariableMechanics(source), _ipMeca(NULL), _ipvCL(NULL) {
    if(source._ipMeca != NULL)
    {
        if(_ipMeca != NULL){
            delete _ipMeca;
            _ipMeca=NULL;
        }
        _ipMeca = dynamic_cast<IPVariableMechanics* >(source.getConstRefToIpMeca().clone());
    }

    localDamageVariable=source.localDamageVariable;
    nonLocalDamageVariable=source.nonLocalDamageVariable;

    // _lc=source._lc;
    if (source._ipvCL != NULL)
    {
        if(_ipvCL != NULL){
            delete _ipvCL;
            _ipvCL=NULL;    
        }
        _ipvCL = dynamic_cast<IPCLength*>(source._ipvCL->clone());
    }
    _PMax = source._PMax;
    _effEner = source._effEner;
    _defoEnergy = source._defoEnergy;
    _plasticEnergy = source._plasticEnergy;
    _irreversibleEnergy = source._irreversibleEnergy;
    _damageEnergy = source._damageEnergy;
    _DirreversibleEnergyDF = source._DirreversibleEnergyDF;
    _DirreversibleEnergyDNonLocalVariable = source._DirreversibleEnergyDNonLocalVariable;

}

IPGenericCrackPhaseField& IPGenericCrackPhaseField::operator=(const IPVariable &source)
{
    IPVariableMechanics::operator=(source);
    const IPGenericCrackPhaseField* src = dynamic_cast<const IPGenericCrackPhaseField*>(&source);
    if(src)
    {
        if(src->_ipMeca!=NULL)
        {
            if(_ipMeca!=NULL)
            {
                _ipMeca->operator=(dynamic_cast<const IPVariable &> (*(src->_ipMeca)));
            }
            else
                _ipMeca = dynamic_cast<IPVariableMechanics *>((src->getConstRefToIpMeca()).clone());
        }
        localDamageVariable=src->localDamageVariable;
        nonLocalDamageVariable=src->nonLocalDamageVariable;
        // _lc=src->_lc;

        if(src->_ipvCL != NULL)
        {
            if (_ipvCL != NULL){
                _ipvCL->operator=(*dynamic_cast<const IPCLength*>(src->_ipvCL));
            }
            else{
                _ipvCL= dynamic_cast<IPCLength*>(src->_ipvCL->clone());
            }
        }
        else{
            if(_ipvCL != NULL) delete _ipvCL; _ipvCL = NULL;
        }

        _PMax=src->_PMax;
        _effEner=src->_effEner;
        _defoEnergy=src->_defoEnergy;
        _plasticEnergy=src->_plasticEnergy;
        _irreversibleEnergy=src->_irreversibleEnergy;
        _damageEnergy=src->_damageEnergy;
        _DirreversibleEnergyDF=src->_DirreversibleEnergyDF;
        _DirreversibleEnergyDNonLocalVariable=src->_DirreversibleEnergyDNonLocalVariable;
    }
    return *this;
}

double& IPGenericCrackPhaseField::getRefToIrreversibleEnergy(){
    // return _ipMeca -> irreversibleEnergy(); // TODO use getter function !! mod the local 
    // return ire;
    return _irreversibleEnergy;
}
void IPGenericCrackPhaseField::restart() {
    _ipMeca->restart();
    restartManager::restart(localDamageVariable);
    restartManager::restart(nonLocalDamageVariable);
    //restartManager::restart(_ipvCL);
    // restartManager::restart(_lc);
    _ipvCL->restart();
    restartManager::restart(_PMax);
    restartManager::restart(_effEner);
    restartManager::restart(_defoEnergy);
    restartManager::restart(_plasticEnergy);
    restartManager::restart(_irreversibleEnergy);
    restartManager::restart(_damageEnergy);
    restartManager::restart(_DirreversibleEnergyDF);
    restartManager::restart(_DirreversibleEnergyDNonLocalVariable);
    IPVariableMechanics::restart();
}

double IPGenericCrackPhaseField::get(const int comp) const
{
    if (comp==IPField::DAMAGE)
    {
        return nonLocalDamageVariable;
    }
    else
    {
        return _ipMeca -> get(comp);
    }
}

double & IPGenericCrackPhaseField::getRef(const int comp)
{
    if (comp==IPField::DAMAGE)
    {
        return nonLocalDamageVariable;
    }
    else
    {
        return _ipMeca -> getRef(comp);
    }
}
