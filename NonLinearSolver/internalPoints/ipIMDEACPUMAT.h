//
// Description: storing class for vevp umat interface
// Author:  <V.D. NGUYEN>, (C) 2023
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef IPIMDEACPUMAT_H_
#define IPIMDEACPUMAT_H_
#include "ipUMATInterface.h"
#include "STensor3.h"
class IPIMDEACPUMAT : public IPUMATInterface
{
 protected:

 public: // All data public to avoid the creation of function to access

 public:
  IPIMDEACPUMAT(int _nsdv, double eleSize) : IPUMATInterface(_nsdv, eleSize)
  {

    // here initialised internal variables which have to be 
    //_statev[0]=1;
    //_statev[1]=1;
    //_statev[2]=1;
  }
  IPIMDEACPUMAT(const IPIMDEACPUMAT &source) : IPUMATInterface(source)
  {
  }
  IPIMDEACPUMAT &operator = (const IPVariable &_source)
  {
      IPUMATInterface::operator=(_source);
      return *this;
  }
  virtual ~IPIMDEACPUMAT()
  {
  }
  virtual IPVariable* clone() const {return new IPIMDEACPUMAT(*this);};
  virtual void restart();
  virtual double get(const int i) const;

 protected:
  IPIMDEACPUMAT(){}
};

#endif // IPIMDEACPUMAT_H_
