//
// C++ Interface: Quadrature Rule
//
// Description: Define 2D Gauss-Lobatto quadrature rule (number of point 3 to 6)
//
//
// Author:  <Gauthier BECKER>, (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef GAUSSLOBATTO2DQUADRATURERULES_H_
#define GAUSSLOBATTO2DQUADRATURERULES_H_
#include "quadratureRules.h"
class GaussLobatto2DQuadrature : public QuadratureBase
{
 private:
  int _npts; // number of point for the integration I don't understand the principe and the interest of order
 public:
  GaussLobatto2DQuadrature(int npts=3) : _npts(npts){}
  virtual ~GaussLobatto2DQuadrature(){}
  virtual int getIntPoints(MElement *e, IntPt **GP);
};
#endif // GAUSSLOBATTO2DQUADRATURERULES_H_
