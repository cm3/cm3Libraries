//
// Description: storing class for Mullins Effect
//
//
// Author:  <Ujwal Kishore J.>, (C) 2023
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef IPMULLINSEFFECT_H_
#define IPMULLINSEFFECT_H_

#include "ipHardening.h"

class IPMullinsEffect{
    
  protected:
    double eta; // scaling variable
    double DetaDpsi;   
    double DDetaDpsipsi;  
    double DetaDT; 
    double DDetaDTT; 
    double psiMax;
    double DpsiNew_DpsiMax;
    double DDpsiNew_DpsiMaxDpsi;
    double DDpsiNew_DpsiMaxDT;
    double DpsiMax_Dpsi;

  public:
    IPMullinsEffect();
    IPMullinsEffect(const IPMullinsEffect &source);
    virtual IPMullinsEffect &operator=(const IPMullinsEffect &source);
    virtual ~IPMullinsEffect(){}
    virtual void restart();

    virtual IPMullinsEffect * clone() const {return new IPMullinsEffect(*this);};

    virtual double getEta() const {return eta;}
    virtual double getDetaDpsi() const {return DetaDpsi;}
    virtual double getDDetaDpsipsi() const {return DDetaDpsipsi;};
    virtual double getDetaDT() const{ return DetaDT;};
    virtual double getDDetaDTT() const{ return DDetaDTT;};
    virtual double getpsiMax() const{ return psiMax;};
    virtual double getDpsiNew_DpsiMax() const{ return DpsiNew_DpsiMax;};
    virtual double getDDpsiNew_DpsiMaxDpsi() const{ return DDpsiNew_DpsiMaxDpsi;};
    virtual double getDDpsiNew_DpsiMaxDT() const{ return DDpsiNew_DpsiMaxDT;};
    virtual double getDpsiMax_Dpsi() const{ return DpsiMax_Dpsi;};
    virtual void set(const double &_eta, const double &_DetaDpsi, const double& _DDetaDpsipsi, 
                     const double& _DetaDT, const double& _DDetaDTT,
                     const double& _DpsiNew_DpsiMax, const double& _DDpsiNew_DpsiMaxDpsi, const double& _DDpsiNew_DpsiMaxDT)
    {
      eta=_eta;
      DetaDpsi=_DetaDpsi;
      DDetaDpsipsi = _DDetaDpsipsi;
      DetaDT = _DetaDT;
      DDetaDTT = _DDetaDTT;
      DpsiNew_DpsiMax = _DpsiNew_DpsiMax;
      DDpsiNew_DpsiMaxDpsi = _DDpsiNew_DpsiMaxDpsi;
      DDpsiNew_DpsiMaxDT = _DDpsiNew_DpsiMaxDT;
    }
    virtual void setPsiMax(const double &_psiMax){
      psiMax = _psiMax;  
    }
    virtual void setDpsiMax_Dpsi(const double &_DpsiMax_Dpsi){
      DpsiMax_Dpsi = _DpsiMax_Dpsi;
    }
};


#endif // IPMULLINSEFFECT_H_
