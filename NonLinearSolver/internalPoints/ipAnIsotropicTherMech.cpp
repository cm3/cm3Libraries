//
// Description: storing class for  Thermo Mechanics
//
//
// Author:  <Lina Homsy>, (C) 2016
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "ipAnIsotropicTherMech.h"
#include "restartManager.h"
IPAnIsotropicTherMech::IPAnIsotropicTherMech() : IPTransverseIsotropic(),_thermalEnergy(0.){

}

IPAnIsotropicTherMech::IPAnIsotropicTherMech(const IPAnIsotropicTherMech &source) :  IPTransverseIsotropic(source), _thermalEnergy(source._thermalEnergy)
{

}

IPAnIsotropicTherMech& IPAnIsotropicTherMech::operator=(const IPVariable &source)
{
  IPTransverseIsotropic::operator=(source);
  const IPAnIsotropicTherMech* src = dynamic_cast<const IPAnIsotropicTherMech*>(&source);
  if(src)
  {
     _thermalEnergy=src->_thermalEnergy;
  }
  return *this;
}

double IPAnIsotropicTherMech::getThermalEnergy() const
{
  return _thermalEnergy;
}


void IPAnIsotropicTherMech::restart()
{
  IPTransverseIsotropic::restart();
  restartManager::restart(_thermalEnergy);
}

