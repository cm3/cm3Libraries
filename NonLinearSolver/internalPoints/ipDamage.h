//
// C++ Interface: terms
//
// Description: Define damage variable for non local j2 plasticity
//
//
// Author:  <L. Noels>, (C) 2013
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef IPDAMAGE_H_
#define IPDAMAGE_H_
#include <stdlib.h>
#include <stdio.h>
#include "GmshConfig.h"
#include "STensor3.h"
#include "ipvariable.h"

class IPDamage : public IPVariable{
  protected:
   double D, maximalp, DeltaD, dDdp;
   STensor3 dDdFe; // derivative of damage respect to elastic deformation
   double initp; //initial value of damage
   
 public:
  IPDamage(const double pinit);
  IPDamage(const IPDamage &source);
  virtual IPDamage &operator=(const IPVariable &source);
  virtual ~IPDamage(){}
  virtual void restart();
  virtual double getDamage() const {return D;}
  virtual double getInitialP() const {return initp;}
  virtual double getMaximalP() const {return maximalp;}
  virtual double getDeltaDamage() const {return DeltaD;}
  virtual double getDDamageDp() const {return dDdp;}
  virtual const STensor3 &getConstRefToDDamageDFe() const {return dDdFe;}

  virtual double& getRefToDamage() {return D;}
  virtual double& getRefToInitialP() {return initp;}
  virtual double& getRefToMaximalP() {return maximalp;}
  virtual double& getRefToDeltaDamage() {return DeltaD;}
  virtual double& getRefToDDamageDp() {return dDdp;}
  virtual STensor3 &getRefToDDamageDFe() {return dDdFe;}
  virtual void setValueForBodyForce(const STensor43& K, const int ModuliType){ Msg::Error("setValueForBodyForce is not defined in ipDamage"); }
  virtual const STensor43 &getConstRefToTangentModuli() const{Msg::Error("getConstRefToTangentModuli ipDamage"); static STensor43 dummy; return dummy;};
  virtual void computeBodyForce(const STensor33& G, SVector3& B) const { Msg::Error("ComputeBodyForce is not defined in ipDamage"); }

  virtual void setValues(const double &_dam, const double &_mp, const double &_DD,
                         const double &_dDdp, const STensor3 &_dDdFe)
  {
    D        = _dam;
    maximalp = _mp;
    DeltaD   = _DD;
    dDdp     = _dDdp;
    dDdFe    = _dDdFe;
  }

  virtual IPVariable* clone() const {return new IPDamage(*this);};
};

class IPLemaitreChabocheDamage : public IPDamage
{

 protected:

 public:
  IPLemaitreChabocheDamage(const double pinit);
  IPLemaitreChabocheDamage(const IPLemaitreChabocheDamage &source);
  virtual IPLemaitreChabocheDamage &operator=(const IPVariable &source);
  virtual ~IPLemaitreChabocheDamage(){}
  virtual void restart();
  virtual IPVariable* clone() const;

};

class IPPowerLaw : public IPDamage
{

 protected:

 public:
  IPPowerLaw(const double pinit);
  IPPowerLaw(const IPPowerLaw &source);
  virtual IPPowerLaw &operator=(const IPVariable &source);
  virtual ~IPPowerLaw(){}
  virtual void restart();
  virtual IPVariable* clone() const;
};


class IPExponentialDamageLaw : public IPDamage
{

 protected:

 public:
  IPExponentialDamageLaw(const double pinit);
  IPExponentialDamageLaw(const IPExponentialDamageLaw &source);
  virtual IPExponentialDamageLaw &operator=(const IPVariable &source);
  virtual ~IPExponentialDamageLaw(){}
  virtual void restart();
  virtual IPVariable* clone() const;
};

class IPSigmoidDamageLaw : public IPDamage
{

 protected:

 public:
  IPSigmoidDamageLaw(const double pinit);
  IPSigmoidDamageLaw(const IPSigmoidDamageLaw &source);
  virtual IPSigmoidDamageLaw &operator=(const IPVariable &source);
  virtual ~IPSigmoidDamageLaw(){}
  virtual void restart();
  virtual IPVariable* clone() const;
};

class IPThreeParametersExponentialDamageLaw : public IPDamage
{

 protected:

 public:
  IPThreeParametersExponentialDamageLaw(const double pinit);
  IPThreeParametersExponentialDamageLaw(const IPThreeParametersExponentialDamageLaw &source);
  virtual IPThreeParametersExponentialDamageLaw &operator=(const IPVariable &source);
  virtual ~IPThreeParametersExponentialDamageLaw(){}
  virtual void restart();
  virtual IPVariable* clone() const;
};



class IPSimpleSaturateDamageLaw : public IPDamage
{

 protected:

 public:
  IPSimpleSaturateDamageLaw(const double pinit);
  IPSimpleSaturateDamageLaw(const IPSimpleSaturateDamageLaw &source);
  virtual IPSimpleSaturateDamageLaw &operator=(const IPVariable &source);
  virtual ~IPSimpleSaturateDamageLaw(){}
  virtual void restart();
  virtual IPVariable* clone() const;
};


class IPPowerBrittleDamagelaw : public IPDamage
{

 protected:

 public:
  IPPowerBrittleDamagelaw(const double pinit);
  IPPowerBrittleDamagelaw(const IPPowerBrittleDamagelaw &source);
  virtual IPPowerBrittleDamagelaw &operator=(const IPVariable &source);
  virtual ~IPPowerBrittleDamagelaw(){}
  virtual void restart();
  virtual IPVariable* clone() const;
};



#endif //IPDamage_H_

