//
// Description: storing class for gurson plasticity
//
//
// Author:  <L. Noels>, (C) 2019
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "ipGursonUMAT.h"
#include "ipField.h"
#include "restartManager.h"


double IPGursonUMAT::get(int comp) const
{
  if(comp==IPField::LOCAL_POROSITY) 
    return getFv();
  else if(comp==IPField::DAMAGE) 
    return getFv();
  else if(comp==IPField::PLASTICSTRAIN)
    return getMatrixEps();
  else if(comp==IPField::VOLUMETRIC_PLASTIC_STRAIN)
    return getMacroVolEps();
  else if(comp==IPField::DEVIATORIC_PLASTIC_STRAIN)
    return getMacroDevEps();
  else if(comp==IPField::LIGAMENT_RATIO)
    return getMacroDevEps();
  else if(comp==IPField::COALESCENCE_ACTIVE)
    return (double)getThomasonFlag();
  else
    return IPUMATInterface::get(comp);
}

void IPGursonUMAT::restart()
{
  IPUMATInterface::restart();
 return;
}
