//

// Author:  <L. Homsy>, (C) 2016
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef IPANISOTROPICTHERMECH_H_
#define IPANISOTROPICTHERMECH_H_
#include "ipvariable.h"

#include "ipTransverseIsotropic.h"
#include "STensor3.h"

class IPAnIsotropicTherMech : public IPTransverseIsotropic
{
public:
    double _thermalEnergy;
public:

  IPAnIsotropicTherMech();
  IPAnIsotropicTherMech(const IPAnIsotropicTherMech &source);
  IPAnIsotropicTherMech& operator=(const IPVariable &source);
  virtual IPVariable* clone() const {return new IPAnIsotropicTherMech(*this);};
  virtual void restart(); 
  virtual double getThermalEnergy() const;

};

#endif // IPANISOTROPICTHERMECH_H_
