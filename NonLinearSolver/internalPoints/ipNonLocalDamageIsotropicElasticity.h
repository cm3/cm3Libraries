//
// Description: storing class for linear isotropic elastic law with non-local damage interface
// Author:  <J. Leclerc>, (C) 2015
//
// Copyright: See COPYING file that comes with this distribution
//
//


#ifndef IPNONLOCALDAMAGEISOTROPICELASTICITY_H_
#define IPNONLOCALDAMAGEISOTROPICELASTICITY_H_
#include "ipvariable.h"
#include "ipCLength.h"
#include "ipDamage.h"
#include "CLengthLaw.h"
#include "DamageLaw.h"

class IPLocalDamageIsotropicElasticity : public IPVariableMechanics
{
  protected:
    // Specific variables
    IPDamage* _ipvDam;      // IPv for damage evolution
    double _elasticEnergy;              // Elastic energy stored
    double _EffectiveStrains;           // Local equivalent strains
    
    // Transition management
    double _CriticalEffectiveStrains;   // Critical effective strains values
    bool _activeDamaging;               // true if damage growing false otherwise
    bool _dissipationBlocked;                // Flag true if damage evolution is blocked
    double _damageEnergy;
    
    // Path following
    double _irreversibleEnergy;         // Irreversible energy (=dissipated) accumulated from the beginning
    STensor3 _DirreversibleEnergyDF;    // Derivatives in terms of deformation gradient
  
  public:
    // Constructors & destructors
    IPLocalDamageIsotropicElasticity(const DamageLaw* damLaw = NULL);
    IPLocalDamageIsotropicElasticity(const IPLocalDamageIsotropicElasticity &source);
    virtual IPLocalDamageIsotropicElasticity& operator=(const IPVariable &source);
    virtual ~IPLocalDamageIsotropicElasticity(){
      if(_ipvDam != NULL){
        delete _ipvDam;
      }
    }

    virtual void blockDissipation(const bool fl){_dissipationBlocked = fl;};
    virtual bool dissipationIsBlocked() const {return _dissipationBlocked;};

    virtual void activeDamage(const bool flg) {_activeDamaging = flg;};
    virtual bool dissipationIsActive() const {return _activeDamaging;};

    // General functions
    virtual double defoEnergy() const {return _elasticEnergy;};
    virtual double plasticEnergy() const{return 0.;}
    virtual double damageEnergy() const {return _damageEnergy;};
    virtual double& getRefToDamageEnergy() {return _damageEnergy;};
    virtual void restart();
    virtual IPVariable* clone() const {return new IPLocalDamageIsotropicElasticity(*this);};

    // Access functions - members values
    virtual double &getRefToElasticEnergy() {return _elasticEnergy;};

    virtual const double getLocalEffectiveStrains() const {return _EffectiveStrains;};
    virtual double &getRefToLocalEffectiveStrains() {return _EffectiveStrains;};
    
    virtual const double getCriticalEffectiveStrains() const {return _CriticalEffectiveStrains;}
    virtual double &getRefToCriticalEffectiveStrains() {return _CriticalEffectiveStrains;}

    // Path following
    virtual double irreversibleEnergy() const {return _irreversibleEnergy;};
    virtual double& getRefToIrreversibleEnergy() {return _irreversibleEnergy;};
    
    virtual const STensor3& getConstRefToDIrreversibleEnergyDF() const{return _DirreversibleEnergyDF;};
    virtual STensor3& getRefToDIrreversibleEnergyDF() {return _DirreversibleEnergyDF;};
    
    
    // Access functions - IPDamage
    virtual const IPDamage &getConstRefToIPDamage() const
    {
        if(_ipvDam==NULL)
            Msg::Error("IPLocalDamageIsotropicElasticity: _ipvDam not initialized");
        return *_ipvDam;
    }
    virtual IPDamage &getRefToIPDamage()
    {
        if(_ipvDam==NULL)
            Msg::Error("IPLocalDamageIsotropicElasticity: _ipvDam not initialized");
        return *_ipvDam;
    }
    virtual double getDamage() const
    {
        if(_ipvDam==NULL)
            Msg::Error("IPLocalDamageIsotropicElasticity: _ipvDam not initialized");
        return _ipvDam->getDamage();
    }
    virtual double getMaximalP() const
    {
        if(_ipvDam==NULL)
            Msg::Error("IPLocalDamageIsotropicElasticity: _ipvDam not initialized");
        return _ipvDam->getMaximalP();
    }
    virtual double getDDamageDp() const
    {
        if(_ipvDam==NULL)
            Msg::Error("IPLocalDamageIsotropicElasticity: _ipvDam not initialized");
        return _ipvDam->getDDamageDp();
    }

};


class IPNonLocalDamageIsotropicElasticity : public IPLocalDamageIsotropicElasticity{
  protected:
    // Specific variables
    IPCLength* _ipvCL;      // IPv for non-local lenght
    double _NonLocalEffectiveStrains;   // Non local equivalent strains
    bool _NonLocalToLocal;              // Flag to switch (if true) between non-local and local scheme after Dc
    double _DirreversibleEnergyDNonLocalVariable; // Derivatives in terms of non-local variables
  
  public:
    // Constructors & destructors
    IPNonLocalDamageIsotropicElasticity(const CLengthLaw* cLLaw = NULL, const DamageLaw* damLaw = NULL);
    IPNonLocalDamageIsotropicElasticity(const IPNonLocalDamageIsotropicElasticity &source);
    virtual IPNonLocalDamageIsotropicElasticity& operator=(const IPVariable &source);
    virtual ~IPNonLocalDamageIsotropicElasticity()
    {
      if(_ipvCL != NULL)
      {
          delete _ipvCL;
      }
    }

    virtual void restart();
    virtual IPVariable* clone() const {return new IPNonLocalDamageIsotropicElasticity(*this);};

    virtual const double getNonLocalEffectiveStrains() const {return _NonLocalEffectiveStrains;}
    virtual double &getRefToNonLocalEffectiveStrains() {return _NonLocalEffectiveStrains;}

    virtual const bool getNonLocalToLocal() const {return _NonLocalToLocal;};
    virtual void setNonLocalToLocal(const bool flag){_NonLocalToLocal = flag;};
    
    virtual const double& getDIrreversibleEnergyDNonLocalVariable() const {return _DirreversibleEnergyDNonLocalVariable;};
    virtual double& getRefToDIrreversibleEnergyDNonLocalVariable() {return _DirreversibleEnergyDNonLocalVariable;};
    
        
    // Access functions - IPCLength
    virtual const IPCLength &getConstRefToIPCLength() const
    {
      if(_ipvCL==NULL)
        Msg::Error("IPNonLocalDamageIsotropicElasticity: _ipvCL not initialized");
      return *_ipvCL;
    }
    virtual IPCLength &getRefToIPCLength()
    {
      if(_ipvCL==NULL)
        Msg::Error("IPNonLocalDamageIsotropicElasticity: _ipvCL not initialized");
      return *_ipvCL;
    }
    virtual const STensor3 &getConstRefToCharacteristicLength() const
    {
      if(_ipvCL==NULL)
        Msg::Error("IPNonLocalDamageIsotropicElasticity: _ipvCL not initialized");
      return _ipvCL->getConstRefToCL();
    }
    virtual STensor3 &getRefToCharacteristicLength() const
    {
      if(_ipvCL==NULL)
        Msg::Error("IPNonLocalDamageIsotropicElasticity: _ipvCL not initialized");
      return _ipvCL->getRefToCL();
    }

};


#endif // IPNONLOCALDAMAGEISOTROPICELASTICITY_H_




