//
// Description: storing class for Electro Thermo Mechanics
//
//
// Author:  <Lina Homsy>, (C) 2013
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "ipElecTherMech.h"

IPElecTherMech::IPElecTherMech() : IPLinearElecTherMech() {};
IPElecTherMech::IPElecTherMech(const IPElecTherMech &source) :  IPLinearElecTherMech(source){}
IPElecTherMech& IPElecTherMech::operator=(const IPVariable &source)
{
  IPLinearElecTherMech::operator=(source);
  const IPElecTherMech* src = static_cast<const IPElecTherMech*>(&source);
}

double IPElecTherMech::defoEnergy() const
{
  return  IPLinearElecTherMech::defoEnergy();
}

