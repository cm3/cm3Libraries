//
// Description: storing class for non local damage
//
//
// Author:  <L. Noels>, (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "ipNonLocalDamage.h"
#include "ipField.h"
#include "restartManager.h"


double IPNonLocalDamage::defoEnergy() const
{
  return _elasticEne;
}
double IPNonLocalDamage::plasticEnergy() const
{
  return _plasticEne;
}
double IPNonLocalDamage::stressWork() const
{
  return _stressWork;
}

double IPNonLocalDamage::get(int comp) const
{


  //here to vizualize direction
 // const double *euler=&(_nldStatev[_pos_euler]);
 // if(comp == IPField::SIG_XX) return euler[0];
 // if(comp == IPField::SIG_YY) return euler[1];
 // if(comp == IPField::SIG_ZZ) return euler[2];

  if(comp == IPField::SIG_XX || comp == IPField::P_XX)
	  return _nldStress(0);
  else if(comp == IPField::SIG_YY || comp == IPField::P_YY)
	  return _nldStress(1);
  else if(comp == IPField::SIG_ZZ || comp == IPField::P_ZZ)
	  return _nldStress(2);	  
  else if(comp==IPField::SIG_XY || comp == IPField::P_XY || comp == IPField::P_YX) // order xx yy zz xy xz yz
	  return _nldStress(3);
  else if(comp==IPField::SIG_XZ || comp == IPField::P_XZ || comp == IPField::P_ZX) // order xx yy zz xy xz yz
	  return _nldStress(4);
  else if(comp==IPField::SIG_YZ || comp == IPField::P_YZ || comp == IPField::P_YZ) // order xx yy zz xy xz yz
	  return _nldStress(5);
  else if(comp == IPField::SVM) // von Mises
  {

     double svm= (_nldStress(0)-_nldStress(1))*(_nldStress(0)-_nldStress(1))+(_nldStress(2)-_nldStress(1))*(_nldStress(2)-_nldStress(1))+(_nldStress(2)-_nldStress(0))*(_nldStress(2)-_nldStress(0));
     svm += 6.*(_nldStress(3)*_nldStress(3)+_nldStress(4)*_nldStress(4)+_nldStress(5)*_nldStress(5));
     svm /= 2.;
     svm = sqrt(svm);
     return svm;
  }


  // for VT case (VM or VLM)
  // for each VT phase
  // strain
  else if(comp == IPField::STRAIN_XX_VTM01)
     return _nldStatev[_pos_strn_vt_m0[0]];
  else if(comp == IPField::STRAIN_XX_VTMT1)
     return _nldStatev[_pos_strn_vt_mt[0]];
  else if(comp == IPField::STRAIN_XX_VTMT2)
     return _nldStatev[_pos_strn_vt_mt[1]];
  else if(comp == IPField::STRAIN_XX_VTMT3)
     return _nldStatev[_pos_strn_vt_mt[2]];
  else if(comp == IPField::STRAIN_XX_VTMT4)
     return _nldStatev[_pos_strn_vt_mt[3]];
  else if(comp == IPField::STRAIN_XX_VTMT5)
     return _nldStatev[_pos_strn_vt_mt[4]];
  else if(comp == IPField::STRAIN_XX_VTMT6)
     return _nldStatev[_pos_strn_vt_mt[5]];
  else if(comp == IPField::STRAIN_XX_VTLAM1)
     return _nldStatev[_pos_strn_vt_lam[0]];
  else if(comp == IPField::STRAIN_XX_VTLAM2)
     return _nldStatev[_pos_strn_vt_lam[1]];
  else if(comp == IPField::STRAIN_XX_VTLAM3)
     return _nldStatev[_pos_strn_vt_lam[2]];
  else if(comp == IPField::STRAIN_XX_VTLAM4)
     return _nldStatev[_pos_strn_vt_lam[3]];
  else if(comp == IPField::STRAIN_XX_VTLAM5)
     return _nldStatev[_pos_strn_vt_lam[4]];
  else if(comp == IPField::STRAIN_XX_VTLAM6)
     return _nldStatev[_pos_strn_vt_lam[5]];
  else if(comp == IPField::STRAIN_XX_VTLAM7)
     return _nldStatev[_pos_strn_vt_lam[6]];
  else if(comp == IPField::STRAIN_XX_VTLAM8)
     return _nldStatev[_pos_strn_vt_lam[7]];
  else if(comp == IPField::STRAIN_XX_VTLAM9)
     return _nldStatev[_pos_strn_vt_lam[8]];
  else if(comp == IPField::STRAIN_XX_VTLAM10)
     return _nldStatev[_pos_strn_vt_lam[9]];
  else if(comp == IPField::STRAIN_XX_VTLAM11)
     return _nldStatev[_pos_strn_vt_lam[10]];
  else if(comp == IPField::STRAIN_XX_VTLAM12)
     return _nldStatev[_pos_strn_vt_lam[11]];
  // stress
  else if(comp == IPField::STRESS_XX_VTM01)
     return _nldStatev[_pos_strs_vt_m0[0]];
  else if(comp == IPField::STRESS_XX_VTMT1)
     return _nldStatev[_pos_strs_vt_mt[0]];
  else if(comp == IPField::STRESS_XX_VTMT2)
     return _nldStatev[_pos_strs_vt_mt[1]];
  else if(comp == IPField::STRESS_XX_VTMT3)
     return _nldStatev[_pos_strs_vt_mt[2]];
  else if(comp == IPField::STRESS_XX_VTMT4)
     return _nldStatev[_pos_strs_vt_mt[3]];
  else if(comp == IPField::STRESS_XX_VTMT5)
     return _nldStatev[_pos_strs_vt_mt[4]];
  else if(comp == IPField::STRESS_XX_VTMT6)
     return _nldStatev[_pos_strs_vt_mt[5]];
  else if(comp == IPField::STRESS_XX_VTLAM1)
     return _nldStatev[_pos_strs_vt_lam[0]];
  else if(comp == IPField::STRESS_XX_VTLAM2)
     return _nldStatev[_pos_strs_vt_lam[1]];
  else if(comp == IPField::STRESS_XX_VTLAM3)
     return _nldStatev[_pos_strs_vt_lam[2]];
  else if(comp == IPField::STRESS_XX_VTLAM4)
     return _nldStatev[_pos_strs_vt_lam[3]];
  else if(comp == IPField::STRESS_XX_VTLAM5)
     return _nldStatev[_pos_strs_vt_lam[4]];
  else if(comp == IPField::STRESS_XX_VTLAM6)
     return _nldStatev[_pos_strs_vt_lam[5]];
  else if(comp == IPField::STRESS_XX_VTLAM7)
     return _nldStatev[_pos_strs_vt_lam[6]];
  else if(comp == IPField::STRESS_XX_VTLAM8)
     return _nldStatev[_pos_strs_vt_lam[7]];
  else if(comp == IPField::STRESS_XX_VTLAM9)
     return _nldStatev[_pos_strs_vt_lam[8]];
  else if(comp == IPField::STRESS_XX_VTLAM10)
     return _nldStatev[_pos_strs_vt_lam[9]];
  else if(comp == IPField::STRESS_XX_VTLAM11)
     return _nldStatev[_pos_strs_vt_lam[10]];
  else if(comp == IPField::STRESS_XX_VTLAM12)
     return _nldStatev[_pos_strs_vt_lam[11]];
  // damage
  else if(comp == IPField::DAMAGE_VTM01){
      if(_pos_dam_vt_m0[0] > -1) return _nldStatev[_pos_dam_vt_m0[0]];
      return 0.;
  }
  // for each laminate ply
  // strain
  else if(comp == IPField::STRAIN_XX_VTLAM1_M0){
     int grainNb = 0;
     rotLocToGlo(_str_loc, _str_glo, _nldStatev, _pos_strn_vt_lam_m0[grainNb], grainNb);
     return _str_glo[0];
  }
  else if(comp == IPField::STRAIN_XX_VTLAM1_MT){
     int grainNb = 0;
     rotLocToGlo(_str_loc, _str_glo, _nldStatev, _pos_strn_vt_lam_mt[grainNb], grainNb);
     return _str_glo[0];
  }
  else if(comp == IPField::STRAIN_XX_VTLAM2_M0){
     int grainNb = 1;
     rotLocToGlo(_str_loc, _str_glo, _nldStatev, _pos_strn_vt_lam_m0[grainNb], grainNb);
     return _str_glo[0];
  }
  else if(comp == IPField::STRAIN_XX_VTLAM2_MT){
     int grainNb = 1;
     rotLocToGlo(_str_loc, _str_glo, _nldStatev, _pos_strn_vt_lam_mt[grainNb], grainNb);
     return _str_glo[0];
  }
  else if(comp == IPField::STRAIN_XX_VTLAM3_M0){
     int grainNb = 2;
     rotLocToGlo(_str_loc, _str_glo, _nldStatev, _pos_strn_vt_lam_m0[grainNb], grainNb);
     return _str_glo[0];
  }
  else if(comp == IPField::STRAIN_XX_VTLAM3_MT){
     int grainNb = 2;
     rotLocToGlo(_str_loc, _str_glo, _nldStatev, _pos_strn_vt_lam_mt[grainNb], grainNb);
     return _str_glo[0];
  }
  else if(comp == IPField::STRAIN_XX_VTLAM4_M0){
     int grainNb = 3;
     rotLocToGlo(_str_loc, _str_glo, _nldStatev, _pos_strn_vt_lam_m0[grainNb], grainNb);
     return _str_glo[0];
  }
  else if(comp == IPField::STRAIN_XX_VTLAM4_MT){
     int grainNb = 3;
     rotLocToGlo(_str_loc, _str_glo, _nldStatev, _pos_strn_vt_lam_mt[grainNb], grainNb);
     return _str_glo[0];
  }
  else if(comp == IPField::STRAIN_XX_VTLAM5_M0){
     int grainNb = 4;
     rotLocToGlo(_str_loc, _str_glo, _nldStatev, _pos_strn_vt_lam_m0[grainNb], grainNb);
     return _str_glo[0];
  }
  else if(comp == IPField::STRAIN_XX_VTLAM5_MT){
     int grainNb = 4;
     rotLocToGlo(_str_loc, _str_glo, _nldStatev, _pos_strn_vt_lam_mt[grainNb], grainNb);
     return _str_glo[0];
  }
  else if(comp == IPField::STRAIN_XX_VTLAM6_M0){
     int grainNb = 5;
     rotLocToGlo(_str_loc, _str_glo, _nldStatev, _pos_strn_vt_lam_m0[grainNb], grainNb);
     return _str_glo[0];
  }
  else if(comp == IPField::STRAIN_XX_VTLAM6_MT){
     int grainNb = 5;
     rotLocToGlo(_str_loc, _str_glo, _nldStatev, _pos_strn_vt_lam_mt[grainNb], grainNb);
     return _str_glo[0];
  }
  else if(comp == IPField::STRAIN_XX_VTLAM7_M0){
     int grainNb = 6;
     rotLocToGlo(_str_loc, _str_glo, _nldStatev, _pos_strn_vt_lam_m0[grainNb], grainNb);
     return _str_glo[0];
  }
  else if(comp == IPField::STRAIN_XX_VTLAM7_MT){
     int grainNb = 6;
     rotLocToGlo(_str_loc, _str_glo, _nldStatev, _pos_strn_vt_lam_mt[grainNb], grainNb);
     return _str_glo[0];
  }
  else if(comp == IPField::STRAIN_XX_VTLAM8_M0){
     int grainNb = 7;
     rotLocToGlo(_str_loc, _str_glo, _nldStatev, _pos_strn_vt_lam_m0[grainNb], grainNb);
     return _str_glo[0];
  }
  else if(comp == IPField::STRAIN_XX_VTLAM8_MT){
     int grainNb = 7;
     rotLocToGlo(_str_loc, _str_glo, _nldStatev, _pos_strn_vt_lam_mt[grainNb], grainNb);
     return _str_glo[0];
  }
  else if(comp == IPField::STRAIN_XX_VTLAM9_M0){
     int grainNb = 8;
     rotLocToGlo(_str_loc, _str_glo, _nldStatev, _pos_strn_vt_lam_m0[grainNb], grainNb);
     return _str_glo[0];
  }
  else if(comp == IPField::STRAIN_XX_VTLAM9_MT){
     int grainNb = 8;
     rotLocToGlo(_str_loc, _str_glo, _nldStatev, _pos_strn_vt_lam_mt[grainNb], grainNb);
     return _str_glo[0];
  }
  else if(comp == IPField::STRAIN_XX_VTLAM10_M0){
     int grainNb = 9;
     rotLocToGlo(_str_loc, _str_glo, _nldStatev, _pos_strn_vt_lam_m0[grainNb], grainNb);
     return _str_glo[0];
  }
  else if(comp == IPField::STRAIN_XX_VTLAM10_MT){
     int grainNb = 9;
     rotLocToGlo(_str_loc, _str_glo, _nldStatev, _pos_strn_vt_lam_mt[grainNb], grainNb);
     return _str_glo[0];
  }
  else if(comp == IPField::STRAIN_XX_VTLAM11_M0){
     int grainNb = 10;
     rotLocToGlo(_str_loc, _str_glo, _nldStatev, _pos_strn_vt_lam_m0[grainNb], grainNb);
     return _str_glo[0];
  }
  else if(comp == IPField::STRAIN_XX_VTLAM11_MT){
     int grainNb = 10;
     rotLocToGlo(_str_loc, _str_glo, _nldStatev, _pos_strn_vt_lam_mt[grainNb], grainNb);
     return _str_glo[0];
  }
  else if(comp == IPField::STRAIN_XX_VTLAM12_M0){
     int grainNb = 11;
     rotLocToGlo(_str_loc, _str_glo, _nldStatev, _pos_strn_vt_lam_m0[grainNb], grainNb);
     return _str_glo[0];
  }
  else if(comp == IPField::STRAIN_XX_VTLAM12_MT){
     int grainNb = 11;
     rotLocToGlo(_str_loc, _str_glo, _nldStatev, _pos_strn_vt_lam_mt[grainNb], grainNb);
     return _str_glo[0];
  }
  // stress
  else if(comp == IPField::STRESS_XX_VTLAM1_M0){
     int grainNb = 0;
     rotLocToGlo(_str_loc, _str_glo, _nldStatev, _pos_strs_vt_lam_m0[grainNb], grainNb);
     return _str_glo[0];
  }
  else if(comp == IPField::STRESS_XX_VTLAM1_MT){
     int grainNb = 0;
     rotLocToGlo(_str_loc, _str_glo, _nldStatev, _pos_strs_vt_lam_mt[grainNb], grainNb);
     return _str_glo[0];
  }
  else if(comp == IPField::STRESS_XX_VTLAM2_M0){
     int grainNb = 1;
     rotLocToGlo(_str_loc, _str_glo, _nldStatev, _pos_strs_vt_lam_m0[grainNb], grainNb);
     return _str_glo[0];
  }
  else if(comp == IPField::STRESS_XX_VTLAM2_MT){
     int grainNb = 1;
     rotLocToGlo(_str_loc, _str_glo, _nldStatev, _pos_strs_vt_lam_mt[grainNb], grainNb);
     return _str_glo[0];
  }
  else if(comp == IPField::STRESS_XX_VTLAM3_M0){
     int grainNb = 2;
     rotLocToGlo(_str_loc, _str_glo, _nldStatev, _pos_strs_vt_lam_m0[grainNb], grainNb);
     return _str_glo[0];
  }
  else if(comp == IPField::STRESS_XX_VTLAM3_MT){
     int grainNb = 2;
     rotLocToGlo(_str_loc, _str_glo, _nldStatev, _pos_strs_vt_lam_mt[grainNb], grainNb);
     return _str_glo[0];
  }
  else if(comp == IPField::STRESS_XX_VTLAM4_M0){
     int grainNb = 3;
     rotLocToGlo(_str_loc, _str_glo, _nldStatev, _pos_strs_vt_lam_m0[grainNb], grainNb);
     return _str_glo[0];
  }
  else if(comp == IPField::STRESS_XX_VTLAM4_MT){
     int grainNb = 3;
     rotLocToGlo(_str_loc, _str_glo, _nldStatev, _pos_strs_vt_lam_mt[grainNb], grainNb);
     return _str_glo[0];
  }
  else if(comp == IPField::STRESS_XX_VTLAM5_M0){
     int grainNb = 4;
     rotLocToGlo(_str_loc, _str_glo, _nldStatev, _pos_strs_vt_lam_m0[grainNb], grainNb);
     return _str_glo[0];
  }
  else if(comp == IPField::STRESS_XX_VTLAM5_MT){
     int grainNb = 4;
     rotLocToGlo(_str_loc, _str_glo, _nldStatev, _pos_strs_vt_lam_mt[grainNb], grainNb);
     return _str_glo[0];
  }
  else if(comp == IPField::STRESS_XX_VTLAM6_M0){
     int grainNb = 5;
     rotLocToGlo(_str_loc, _str_glo, _nldStatev, _pos_strs_vt_lam_m0[grainNb], grainNb);
     return _str_glo[0];
  }
  else if(comp == IPField::STRESS_XX_VTLAM6_MT){
     int grainNb = 5;
     rotLocToGlo(_str_loc, _str_glo, _nldStatev, _pos_strs_vt_lam_mt[grainNb], grainNb);
     return _str_glo[0];
  }
  else if(comp == IPField::STRESS_XX_VTLAM7_M0){
     int grainNb = 6;
     rotLocToGlo(_str_loc, _str_glo, _nldStatev, _pos_strs_vt_lam_m0[grainNb], grainNb);
     return _str_glo[0];
  }
  else if(comp == IPField::STRESS_XX_VTLAM7_MT){
     int grainNb = 6;
     rotLocToGlo(_str_loc, _str_glo, _nldStatev, _pos_strs_vt_lam_mt[grainNb], grainNb);
     return _str_glo[0];
  }
  else if(comp == IPField::STRESS_XX_VTLAM8_M0){
     int grainNb = 7;
     rotLocToGlo(_str_loc, _str_glo, _nldStatev, _pos_strs_vt_lam_m0[grainNb], grainNb);
     return _str_glo[0];
  }
  else if(comp == IPField::STRESS_XX_VTLAM8_MT){
     int grainNb = 7;
     rotLocToGlo(_str_loc, _str_glo, _nldStatev, _pos_strs_vt_lam_mt[grainNb], grainNb);
     return _str_glo[0];
  }
  else if(comp == IPField::STRESS_XX_VTLAM9_M0){
     int grainNb = 8;
     rotLocToGlo(_str_loc, _str_glo, _nldStatev, _pos_strs_vt_lam_m0[grainNb], grainNb);
     return _str_glo[0];
  }
  else if(comp == IPField::STRESS_XX_VTLAM9_MT){
     int grainNb = 8;
     rotLocToGlo(_str_loc, _str_glo, _nldStatev, _pos_strs_vt_lam_mt[grainNb], grainNb);
     return _str_glo[0];
  }
  else if(comp == IPField::STRESS_XX_VTLAM10_M0){
     int grainNb = 9;
     rotLocToGlo(_str_loc, _str_glo, _nldStatev, _pos_strs_vt_lam_m0[grainNb], grainNb);
     return _str_glo[0];
  }
  else if(comp == IPField::STRESS_XX_VTLAM10_MT){
     int grainNb = 9;
     rotLocToGlo(_str_loc, _str_glo, _nldStatev, _pos_strs_vt_lam_mt[grainNb], grainNb);
     return _str_glo[0];
  }
  else if(comp == IPField::STRESS_XX_VTLAM11_M0){
     int grainNb = 10;
     rotLocToGlo(_str_loc, _str_glo, _nldStatev, _pos_strs_vt_lam_m0[grainNb], grainNb);
     return _str_glo[0];
  }
  else if(comp == IPField::STRESS_XX_VTLAM11_MT){
     int grainNb = 10;
     rotLocToGlo(_str_loc, _str_glo, _nldStatev, _pos_strs_vt_lam_mt[grainNb], grainNb);
     return _str_glo[0];
  }
  else if(comp == IPField::STRESS_XX_VTLAM12_M0){
     int grainNb = 11;
     rotLocToGlo(_str_loc, _str_glo, _nldStatev, _pos_strs_vt_lam_m0[grainNb], grainNb);
     return _str_glo[0];
  }
  else if(comp == IPField::STRESS_XX_VTLAM12_MT){
     int grainNb = 11;
     rotLocToGlo(_str_loc, _str_glo, _nldStatev, _pos_strs_vt_lam_mt[grainNb], grainNb);
     return _str_glo[0];
  }
  // damage
  else if(comp == IPField::DAMAGE_VTLAM1_M0){
      if(_pos_dam_vt_lam_m0[0] > -1) return _nldStatev[_pos_dam_vt_lam_m0[0]];
      return 0.;
  }
  else if(comp == IPField::DAMAGE_VTLAM2_M0){
      if(_pos_dam_vt_lam_m0[1] > -1) return _nldStatev[_pos_dam_vt_lam_m0[1]];
      return 0.;
  }
  else if(comp == IPField::DAMAGE_VTLAM3_M0){
      if(_pos_dam_vt_lam_m0[2] > -1) return _nldStatev[_pos_dam_vt_lam_m0[2]];
      return 0.;
  }
  else if(comp == IPField::DAMAGE_VTLAM4_M0){
      if(_pos_dam_vt_lam_m0[3] > -1) return _nldStatev[_pos_dam_vt_lam_m0[3]];
      return 0.;
  }
  else if(comp == IPField::DAMAGE_VTLAM5_M0){
      if(_pos_dam_vt_lam_m0[4] > -1) return _nldStatev[_pos_dam_vt_lam_m0[4]];
      return 0.;
  }
  else if(comp == IPField::DAMAGE_VTLAM6_M0){
      if(_pos_dam_vt_lam_m0[5] > -1) return _nldStatev[_pos_dam_vt_lam_m0[5]];
      return 0.;
  }
  else if(comp == IPField::DAMAGE_VTLAM7_M0){
      if(_pos_dam_vt_lam_m0[6] > -1) return _nldStatev[_pos_dam_vt_lam_m0[6]];
      return 0.;
  }
  else if(comp == IPField::DAMAGE_VTLAM8_M0){
      if(_pos_dam_vt_lam_m0[7] > -1) return _nldStatev[_pos_dam_vt_lam_m0[7]];
      return 0.;
  }
  else if(comp == IPField::DAMAGE_VTLAM9_M0){
      if(_pos_dam_vt_lam_m0[8] > -1) return _nldStatev[_pos_dam_vt_lam_m0[8]];
      return 0.;
  }
  else if(comp == IPField::DAMAGE_VTLAM10_M0){
      if(_pos_dam_vt_lam_m0[9] > -1) return _nldStatev[_pos_dam_vt_lam_m0[9]];
      return 0.;
  }
  else if(comp == IPField::DAMAGE_VTLAM11_M0){
      if(_pos_dam_vt_lam_m0[10] > -1) return _nldStatev[_pos_dam_vt_lam_m0[10]];
      return 0.;
  }
  else if(comp == IPField::DAMAGE_VTLAM12_M0){
      if(_pos_dam_vt_lam_m0[11] > -1) return _nldStatev[_pos_dam_vt_lam_m0[11]];
      return 0.;
  }
  // for each material phase
  // strain
  else if(comp == IPField::STRAIN_XX_VTMT1_MTX)
     return _nldStatev[_pos_strn_vt_mt_mtx[0]];
  else if(comp == IPField::STRAIN_XX_VTMT1_INC)
     return _nldStatev[_pos_strn_vt_mt_inc[0]];
  else if(comp == IPField::STRAIN_XX_VTMT2_MTX)
     return _nldStatev[_pos_strn_vt_mt_mtx[1]];
  else if(comp == IPField::STRAIN_XX_VTMT2_INC)
     return _nldStatev[_pos_strn_vt_mt_inc[1]];
  else if(comp == IPField::STRAIN_XX_VTMT3_MTX)
     return _nldStatev[_pos_strn_vt_mt_mtx[2]];
  else if(comp == IPField::STRAIN_XX_VTMT3_INC)
     return _nldStatev[_pos_strn_vt_mt_inc[2]];
  else if(comp == IPField::STRAIN_XX_VTMT4_MTX)
     return _nldStatev[_pos_strn_vt_mt_mtx[3]];
  else if(comp == IPField::STRAIN_XX_VTMT4_INC)
     return _nldStatev[_pos_strn_vt_mt_inc[3]];
  else if(comp == IPField::STRAIN_XX_VTMT5_MTX)
     return _nldStatev[_pos_strn_vt_mt_mtx[4]];
  else if(comp == IPField::STRAIN_XX_VTMT5_INC)
     return _nldStatev[_pos_strn_vt_mt_inc[4]];
  else if(comp == IPField::STRAIN_XX_VTMT6_MTX)
     return _nldStatev[_pos_strn_vt_mt_mtx[5]];
  else if(comp == IPField::STRAIN_XX_VTMT6_INC)
     return _nldStatev[_pos_strn_vt_mt_inc[5]];
  else if(comp == IPField::STRAIN_XX_VTLAM1_MT_MTX){
     int grainNb = 0;
     rotLocToGlo(_str_loc, _str_glo, _nldStatev, _pos_strn_vt_lam_mt_mtx[grainNb], grainNb);
     return _str_glo[0];
  }
  else if(comp == IPField::STRAIN_XX_VTLAM1_MT_INC){
     int grainNb = 0;
     rotLocToGlo(_str_loc, _str_glo, _nldStatev, _pos_strn_vt_lam_mt_inc[grainNb], grainNb);
     return _str_glo[0];
  }
  else if(comp == IPField::STRAIN_XX_VTLAM2_MT_MTX){
     int grainNb = 1;
     rotLocToGlo(_str_loc, _str_glo, _nldStatev, _pos_strn_vt_lam_mt_mtx[grainNb], grainNb);
     return _str_glo[0];
  }
  else if(comp == IPField::STRAIN_XX_VTLAM2_MT_INC){
     int grainNb = 1;
     rotLocToGlo(_str_loc, _str_glo, _nldStatev, _pos_strn_vt_lam_mt_inc[grainNb], grainNb);
     return _str_glo[0];
  }
  else if(comp == IPField::STRAIN_XX_VTLAM3_MT_MTX){
     int grainNb = 2;
     rotLocToGlo(_str_loc, _str_glo, _nldStatev, _pos_strn_vt_lam_mt_mtx[grainNb], grainNb);
     return _str_glo[0];
  }
  else if(comp == IPField::STRAIN_XX_VTLAM3_MT_INC){
     int grainNb = 2;
     rotLocToGlo(_str_loc, _str_glo, _nldStatev, _pos_strn_vt_lam_mt_inc[grainNb], grainNb);
     return _str_glo[0];
  }
  else if(comp == IPField::STRAIN_XX_VTLAM4_MT_MTX){
     int grainNb = 3;
     rotLocToGlo(_str_loc, _str_glo, _nldStatev, _pos_strn_vt_lam_mt_mtx[grainNb], grainNb);
     return _str_glo[0];
  }
  else if(comp == IPField::STRAIN_XX_VTLAM4_MT_INC){
     int grainNb = 3;
     rotLocToGlo(_str_loc, _str_glo, _nldStatev, _pos_strn_vt_lam_mt_inc[grainNb], grainNb);
     return _str_glo[0];
  }
  else if(comp == IPField::STRAIN_XX_VTLAM5_MT_MTX){
     int grainNb = 4;
     rotLocToGlo(_str_loc, _str_glo, _nldStatev, _pos_strn_vt_lam_mt_mtx[grainNb], grainNb);
     return _str_glo[0];
  }
  else if(comp == IPField::STRAIN_XX_VTLAM5_MT_INC){
     int grainNb = 4;
     rotLocToGlo(_str_loc, _str_glo, _nldStatev, _pos_strn_vt_lam_mt_inc[grainNb], grainNb);
     return _str_glo[0];
  }
  else if(comp == IPField::STRAIN_XX_VTLAM6_MT_MTX){
     int grainNb = 5;
     rotLocToGlo(_str_loc, _str_glo, _nldStatev, _pos_strn_vt_lam_mt_mtx[grainNb], grainNb);
     return _str_glo[0];
  }
  else if(comp == IPField::STRAIN_XX_VTLAM6_MT_INC){
     int grainNb = 5;
     rotLocToGlo(_str_loc, _str_glo, _nldStatev, _pos_strn_vt_lam_mt_inc[grainNb], grainNb);
     return _str_glo[0];
  }
  else if(comp == IPField::STRAIN_XX_VTLAM7_MT_MTX){
     int grainNb = 6;
     rotLocToGlo(_str_loc, _str_glo, _nldStatev, _pos_strn_vt_lam_mt_mtx[grainNb], grainNb);
     return _str_glo[0];
  }
  else if(comp == IPField::STRAIN_XX_VTLAM7_MT_INC){
     int grainNb = 6;
     rotLocToGlo(_str_loc, _str_glo, _nldStatev, _pos_strn_vt_lam_mt_inc[grainNb], grainNb);
     return _str_glo[0];
  }
  else if(comp == IPField::STRAIN_XX_VTLAM8_MT_MTX){
     int grainNb = 7;
     rotLocToGlo(_str_loc, _str_glo, _nldStatev, _pos_strn_vt_lam_mt_mtx[grainNb], grainNb);
     return _str_glo[0];
  }
  else if(comp == IPField::STRAIN_XX_VTLAM8_MT_INC){
     int grainNb = 7;
     rotLocToGlo(_str_loc, _str_glo, _nldStatev, _pos_strn_vt_lam_mt_inc[grainNb], grainNb);
     return _str_glo[0];
  }
  else if(comp == IPField::STRAIN_XX_VTLAM9_MT_MTX){
     int grainNb = 8;
     rotLocToGlo(_str_loc, _str_glo, _nldStatev, _pos_strn_vt_lam_mt_mtx[grainNb], grainNb);
     return _str_glo[0];
  }
  else if(comp == IPField::STRAIN_XX_VTLAM9_MT_INC){
     int grainNb = 8;
     rotLocToGlo(_str_loc, _str_glo, _nldStatev, _pos_strn_vt_lam_mt_inc[grainNb], grainNb);
     return _str_glo[0];
  }
  else if(comp == IPField::STRAIN_XX_VTLAM10_MT_MTX){
     int grainNb = 9;
     rotLocToGlo(_str_loc, _str_glo, _nldStatev, _pos_strn_vt_lam_mt_mtx[grainNb], grainNb);
     return _str_glo[0];
  }
  else if(comp == IPField::STRAIN_XX_VTLAM10_MT_INC){
     int grainNb = 9;
     rotLocToGlo(_str_loc, _str_glo, _nldStatev, _pos_strn_vt_lam_mt_inc[grainNb], grainNb);
     return _str_glo[0];
  }
  else if(comp == IPField::STRAIN_XX_VTLAM11_MT_MTX){
     int grainNb = 10;
     rotLocToGlo(_str_loc, _str_glo, _nldStatev, _pos_strn_vt_lam_mt_mtx[grainNb], grainNb);
     return _str_glo[0];
  }
  else if(comp == IPField::STRAIN_XX_VTLAM11_MT_INC){
     int grainNb = 10;
     rotLocToGlo(_str_loc, _str_glo, _nldStatev, _pos_strn_vt_lam_mt_inc[grainNb], grainNb);
     return _str_glo[0];
  }
  else if(comp == IPField::STRAIN_XX_VTLAM12_MT_MTX){
     int grainNb = 11;
     rotLocToGlo(_str_loc, _str_glo, _nldStatev, _pos_strn_vt_lam_mt_mtx[grainNb], grainNb);
     return _str_glo[0];
  }
  else if(comp == IPField::STRAIN_XX_VTLAM12_MT_INC){
     int grainNb = 11;
     rotLocToGlo(_str_loc, _str_glo, _nldStatev, _pos_strn_vt_lam_mt_inc[grainNb], grainNb);
     return _str_glo[0];
  }
  // stress
  else if(comp == IPField::STRESS_XX_VTMT1_MTX)
     return _nldStatev[_pos_strs_vt_mt_mtx[0]];
  else if(comp == IPField::STRESS_XX_VTMT1_INC)
     return _nldStatev[_pos_strs_vt_mt_inc[0]];
  else if(comp == IPField::STRESS_XX_VTMT2_MTX)
     return _nldStatev[_pos_strs_vt_mt_mtx[1]];
  else if(comp == IPField::STRESS_XX_VTMT2_INC)
     return _nldStatev[_pos_strs_vt_mt_inc[1]];
  else if(comp == IPField::STRESS_XX_VTMT3_MTX)
     return _nldStatev[_pos_strs_vt_mt_mtx[2]];
  else if(comp == IPField::STRESS_XX_VTMT3_INC)
     return _nldStatev[_pos_strs_vt_mt_inc[2]];
  else if(comp == IPField::STRESS_XX_VTMT4_MTX)
     return _nldStatev[_pos_strs_vt_mt_mtx[3]];
  else if(comp == IPField::STRESS_XX_VTMT4_INC)
     return _nldStatev[_pos_strs_vt_mt_inc[3]];
  else if(comp == IPField::STRESS_XX_VTMT5_MTX)
     return _nldStatev[_pos_strs_vt_mt_mtx[4]];
  else if(comp == IPField::STRESS_XX_VTMT5_INC)
     return _nldStatev[_pos_strs_vt_mt_inc[4]];
  else if(comp == IPField::STRESS_XX_VTMT6_MTX)
     return _nldStatev[_pos_strs_vt_mt_mtx[5]];
  else if(comp == IPField::STRESS_XX_VTMT6_INC)
     return _nldStatev[_pos_strs_vt_mt_inc[5]];
  else if(comp == IPField::STRESS_XX_VTLAM1_MT_MTX){
     int grainNb = 0;
     rotLocToGlo(_str_loc, _str_glo, _nldStatev, _pos_strs_vt_lam_mt_mtx[grainNb], grainNb);
     return _str_glo[0];
  }
  else if(comp == IPField::STRESS_XX_VTLAM1_MT_INC){
     int grainNb = 0;
     rotLocToGlo(_str_loc, _str_glo, _nldStatev, _pos_strs_vt_lam_mt_inc[grainNb], grainNb);
     return _str_glo[0];
  }
  else if(comp == IPField::STRESS_XX_VTLAM2_MT_MTX){
     int grainNb = 1;
     rotLocToGlo(_str_loc, _str_glo, _nldStatev, _pos_strs_vt_lam_mt_mtx[grainNb], grainNb);
     return _str_glo[0];
  }
  else if(comp == IPField::STRESS_XX_VTLAM2_MT_INC){
     int grainNb = 1;
     rotLocToGlo(_str_loc, _str_glo, _nldStatev, _pos_strs_vt_lam_mt_inc[grainNb], grainNb);
     return _str_glo[0];
  }
  else if(comp == IPField::STRESS_XX_VTLAM3_MT_MTX){
     int grainNb = 2;
     rotLocToGlo(_str_loc, _str_glo, _nldStatev, _pos_strs_vt_lam_mt_mtx[grainNb], grainNb);
     return _str_glo[0];
  }
  else if(comp == IPField::STRESS_XX_VTLAM3_MT_INC){
     int grainNb = 2;
     rotLocToGlo(_str_loc, _str_glo, _nldStatev, _pos_strs_vt_lam_mt_inc[grainNb], grainNb);
     return _str_glo[0];
  }
  else if(comp == IPField::STRESS_XX_VTLAM4_MT_MTX){
     int grainNb = 3;
     rotLocToGlo(_str_loc, _str_glo, _nldStatev, _pos_strs_vt_lam_mt_mtx[grainNb], grainNb);
     return _str_glo[0];
  }
  else if(comp == IPField::STRESS_XX_VTLAM4_MT_INC){
     int grainNb = 3;
     rotLocToGlo(_str_loc, _str_glo, _nldStatev, _pos_strs_vt_lam_mt_inc[grainNb], grainNb);
     return _str_glo[0];
  }
  else if(comp == IPField::STRESS_XX_VTLAM5_MT_MTX){
     int grainNb = 4;
     rotLocToGlo(_str_loc, _str_glo, _nldStatev, _pos_strs_vt_lam_mt_mtx[grainNb], grainNb);
     return _str_glo[0];
  }
  else if(comp == IPField::STRESS_XX_VTLAM5_MT_INC){
     int grainNb = 4;
     rotLocToGlo(_str_loc, _str_glo, _nldStatev, _pos_strs_vt_lam_mt_inc[grainNb], grainNb);
     return _str_glo[0];
  }
  else if(comp == IPField::STRESS_XX_VTLAM6_MT_MTX){
     int grainNb = 5;
     rotLocToGlo(_str_loc, _str_glo, _nldStatev, _pos_strs_vt_lam_mt_mtx[grainNb], grainNb);
     return _str_glo[0];
  }
  else if(comp == IPField::STRESS_XX_VTLAM6_MT_INC){
     int grainNb = 5;
     rotLocToGlo(_str_loc, _str_glo, _nldStatev, _pos_strs_vt_lam_mt_inc[grainNb], grainNb);
     return _str_glo[0];
  }
  else if(comp == IPField::STRESS_XX_VTLAM7_MT_MTX){
     int grainNb = 6;
     rotLocToGlo(_str_loc, _str_glo, _nldStatev, _pos_strs_vt_lam_mt_mtx[grainNb], grainNb);
     return _str_glo[0];
  }
  else if(comp == IPField::STRESS_XX_VTLAM7_MT_INC){
     int grainNb = 6;
     rotLocToGlo(_str_loc, _str_glo, _nldStatev, _pos_strs_vt_lam_mt_inc[grainNb], grainNb);
     return _str_glo[0];
  }
  else if(comp == IPField::STRESS_XX_VTLAM8_MT_MTX){
     int grainNb = 7;
     rotLocToGlo(_str_loc, _str_glo, _nldStatev, _pos_strs_vt_lam_mt_mtx[grainNb], grainNb);
     return _str_glo[0];
  }
  else if(comp == IPField::STRESS_XX_VTLAM8_MT_INC){
     int grainNb = 7;
     rotLocToGlo(_str_loc, _str_glo, _nldStatev, _pos_strs_vt_lam_mt_inc[grainNb], grainNb);
     return _str_glo[0];
  }
  else if(comp == IPField::STRESS_XX_VTLAM9_MT_MTX){
     int grainNb = 8;
     rotLocToGlo(_str_loc, _str_glo, _nldStatev, _pos_strs_vt_lam_mt_mtx[grainNb], grainNb);
     return _str_glo[0];
  }
  else if(comp == IPField::STRESS_XX_VTLAM9_MT_INC){
     int grainNb = 8;
     rotLocToGlo(_str_loc, _str_glo, _nldStatev, _pos_strs_vt_lam_mt_inc[grainNb], grainNb);
     return _str_glo[0];
  }
  else if(comp == IPField::STRESS_XX_VTLAM10_MT_MTX){
     int grainNb = 9;
     rotLocToGlo(_str_loc, _str_glo, _nldStatev, _pos_strs_vt_lam_mt_mtx[grainNb], grainNb);
     return _str_glo[0];
  }
  else if(comp == IPField::STRESS_XX_VTLAM10_MT_INC){
     int grainNb = 9;
     rotLocToGlo(_str_loc, _str_glo, _nldStatev, _pos_strs_vt_lam_mt_inc[grainNb], grainNb);
     return _str_glo[0];
  }
  else if(comp == IPField::STRESS_XX_VTLAM11_MT_MTX){
     int grainNb = 10;
     rotLocToGlo(_str_loc, _str_glo, _nldStatev, _pos_strs_vt_lam_mt_mtx[grainNb], grainNb);
     return _str_glo[0];
  }
  else if(comp == IPField::STRESS_XX_VTLAM11_MT_INC){
     int grainNb = 10;
     rotLocToGlo(_str_loc, _str_glo, _nldStatev, _pos_strs_vt_lam_mt_inc[grainNb], grainNb);
     return _str_glo[0];
  }
  else if(comp == IPField::STRESS_XX_VTLAM12_MT_MTX){
     int grainNb = 11;
     rotLocToGlo(_str_loc, _str_glo, _nldStatev, _pos_strs_vt_lam_mt_mtx[grainNb], grainNb);
     return _str_glo[0];
  }
  else if(comp == IPField::STRESS_XX_VTLAM12_MT_INC){
     int grainNb = 11;
     rotLocToGlo(_str_loc, _str_glo, _nldStatev, _pos_strs_vt_lam_mt_inc[grainNb], grainNb);
     return _str_glo[0];
  }
  // damage
  else if(comp == IPField::DAMAGE_VTMT1_MTX){
      if(_pos_dam_vt_mt_mtx[0] > -1) return _nldStatev[_pos_dam_vt_mt_mtx[0]];
      return 0.;
  }
  else if(comp == IPField::DAMAGE_VTMT1_INC){
      if(_pos_dam_vt_mt_inc[0] > -1) return _nldStatev[_pos_dam_vt_mt_inc[0]];
      return 0.;
  }
  else if(comp == IPField::DAMAGE_VTMT2_MTX){
      if(_pos_dam_vt_mt_mtx[1] > -1) return _nldStatev[_pos_dam_vt_mt_mtx[1]];
      return 0.;
  }
  else if(comp == IPField::DAMAGE_VTMT2_INC){
      if(_pos_dam_vt_mt_inc[1] > -1) return _nldStatev[_pos_dam_vt_mt_inc[1]];
      return 0.;
  }
  else if(comp == IPField::DAMAGE_VTMT3_MTX){
      if(_pos_dam_vt_mt_mtx[2] > -1) return _nldStatev[_pos_dam_vt_mt_mtx[2]];
      return 0.;
  }
  else if(comp == IPField::DAMAGE_VTMT3_INC){
      if(_pos_dam_vt_mt_inc[2] > -1) return _nldStatev[_pos_dam_vt_mt_inc[2]];
      return 0.;
  }
  else if(comp == IPField::DAMAGE_VTMT4_MTX){
      if(_pos_dam_vt_mt_mtx[3] > -1) return _nldStatev[_pos_dam_vt_mt_mtx[3]];
      return 0.;
  }
  else if(comp == IPField::DAMAGE_VTMT4_INC){
      if(_pos_dam_vt_mt_inc[3] > -1) return _nldStatev[_pos_dam_vt_mt_inc[3]];
      return 0.;
  }
  else if(comp == IPField::DAMAGE_VTMT5_MTX){
      if(_pos_dam_vt_mt_mtx[4] > -1) return _nldStatev[_pos_dam_vt_mt_mtx[4]];
      return 0.;
  }
  else if(comp == IPField::DAMAGE_VTMT5_INC){
      if(_pos_dam_vt_mt_inc[4] > -1) return _nldStatev[_pos_dam_vt_mt_inc[4]];
      return 0.;
  }
  else if(comp == IPField::DAMAGE_VTMT6_MTX){
      if(_pos_dam_vt_mt_mtx[5] > -1) return _nldStatev[_pos_dam_vt_mt_mtx[5]];
      return 0.;
  }
  else if(comp == IPField::DAMAGE_VTMT6_INC){
      if(_pos_dam_vt_mt_inc[5] > -1) return _nldStatev[_pos_dam_vt_mt_inc[5]];
      return 0.;
  }
  else if(comp == IPField::DAMAGE_VTLAM1_MT_MTX){
      if(_pos_dam_vt_lam_mt_mtx[0] > -1) return _nldStatev[_pos_dam_vt_lam_mt_mtx[0]];
      return 0.;
  }
  else if(comp == IPField::DAMAGE_VTLAM1_MT_INC){
      if(_pos_dam_vt_lam_mt_inc[0] > -1) return _nldStatev[_pos_dam_vt_lam_mt_inc[0]];
      return 0.;
  }
  else if(comp == IPField::DAMAGE_VTLAM2_MT_MTX){
      if(_pos_dam_vt_lam_mt_mtx[1] > -1) return _nldStatev[_pos_dam_vt_lam_mt_mtx[1]];
      return 0.;
  }
  else if(comp == IPField::DAMAGE_VTLAM2_MT_INC){
      if(_pos_dam_vt_lam_mt_inc[1] > -1) return _nldStatev[_pos_dam_vt_lam_mt_inc[1]];
      return 0.;
  }
  else if(comp == IPField::DAMAGE_VTLAM3_MT_MTX){
      if(_pos_dam_vt_lam_mt_mtx[2] > -1) return _nldStatev[_pos_dam_vt_lam_mt_mtx[2]];
      return 0.;
  }
  else if(comp == IPField::DAMAGE_VTLAM3_MT_INC){
      if(_pos_dam_vt_lam_mt_inc[2] > -1) return _nldStatev[_pos_dam_vt_lam_mt_inc[2]];
      return 0.;
  }
  else if(comp == IPField::DAMAGE_VTLAM4_MT_MTX){
      if(_pos_dam_vt_lam_mt_mtx[3] > -1) return _nldStatev[_pos_dam_vt_lam_mt_mtx[3]];
      return 0.;
  }
  else if(comp == IPField::DAMAGE_VTLAM4_MT_INC){
      if(_pos_dam_vt_lam_mt_inc[3] > -1) return _nldStatev[_pos_dam_vt_lam_mt_inc[3]];
      return 0.;
  }
  else if(comp == IPField::DAMAGE_VTLAM5_MT_MTX){
      if(_pos_dam_vt_lam_mt_mtx[4] > -1) return _nldStatev[_pos_dam_vt_lam_mt_mtx[4]];
      return 0.;
  }
  else if(comp == IPField::DAMAGE_VTLAM5_MT_INC){
      if(_pos_dam_vt_lam_mt_inc[4] > -1) return _nldStatev[_pos_dam_vt_lam_mt_inc[4]];
      return 0.;
  }
  else if(comp == IPField::DAMAGE_VTLAM6_MT_MTX){
      if(_pos_dam_vt_lam_mt_mtx[5] > -1) return _nldStatev[_pos_dam_vt_lam_mt_mtx[5]];
      return 0.;
  }
  else if(comp == IPField::DAMAGE_VTLAM6_MT_INC){
      if(_pos_dam_vt_lam_mt_inc[5] > -1) return _nldStatev[_pos_dam_vt_lam_mt_inc[5]];
      return 0.;
  }
  else if(comp == IPField::DAMAGE_VTLAM7_MT_MTX){
      if(_pos_dam_vt_lam_mt_mtx[6] > -1) return _nldStatev[_pos_dam_vt_lam_mt_mtx[6]];
      return 0.;
  }
  else if(comp == IPField::DAMAGE_VTLAM7_MT_INC){
      if(_pos_dam_vt_lam_mt_inc[6] > -1) return _nldStatev[_pos_dam_vt_lam_mt_inc[6]];
      return 0.;
  }
  else if(comp == IPField::DAMAGE_VTLAM8_MT_MTX){
      if(_pos_dam_vt_lam_mt_mtx[7] > -1) return _nldStatev[_pos_dam_vt_lam_mt_mtx[7]];
      return 0.;
  }
  else if(comp == IPField::DAMAGE_VTLAM8_MT_INC){
      if(_pos_dam_vt_lam_mt_inc[7] > -1) return _nldStatev[_pos_dam_vt_lam_mt_inc[7]];
      return 0.;
  }
  else if(comp == IPField::DAMAGE_VTLAM9_MT_MTX){
      if(_pos_dam_vt_lam_mt_mtx[8] > -1) return _nldStatev[_pos_dam_vt_lam_mt_mtx[8]];
      return 0.;
  }
  else if(comp == IPField::DAMAGE_VTLAM9_MT_INC){
      if(_pos_dam_vt_lam_mt_inc[8] > -1) return _nldStatev[_pos_dam_vt_lam_mt_inc[8]];
      return 0.;
  }
  else if(comp == IPField::DAMAGE_VTLAM10_MT_MTX){
      if(_pos_dam_vt_lam_mt_mtx[9] > -1) return _nldStatev[_pos_dam_vt_lam_mt_mtx[9]];
      return 0.;
  }
  else if(comp == IPField::DAMAGE_VTLAM10_MT_INC){
      if(_pos_dam_vt_lam_mt_inc[9] > -1) return _nldStatev[_pos_dam_vt_lam_mt_inc[9]];
      return 0.;
  }
  else if(comp == IPField::DAMAGE_VTLAM11_MT_MTX){
      if(_pos_dam_vt_lam_mt_mtx[10] > -1) return _nldStatev[_pos_dam_vt_lam_mt_mtx[10]];
      return 0.;
  }
  else if(comp == IPField::DAMAGE_VTLAM11_MT_INC){
      if(_pos_dam_vt_lam_mt_inc[10] > -1) return _nldStatev[_pos_dam_vt_lam_mt_inc[10]];
      return 0.;
  }
  else if(comp == IPField::DAMAGE_VTLAM12_MT_MTX){
      if(_pos_dam_vt_lam_mt_mtx[11] > -1) return _nldStatev[_pos_dam_vt_lam_mt_mtx[11]];
      return 0.;
  }
  else if(comp == IPField::DAMAGE_VTLAM12_MT_INC){
      if(_pos_dam_vt_lam_mt_inc[11] > -1) return _nldStatev[_pos_dam_vt_lam_mt_inc[11]];
      return 0.;
  }
  
  // for LAM_2PLY case (LVM)
  // for each laminate ply
  // strain
  else if(comp == IPField::STRAIN_XX_LAMM01)
     return _nldStatev[_pos_strn_lam_m0[0]];
  else if(comp == IPField::STRAIN_XX_LAMVT1)
     return _nldStatev[_pos_strn_lam_vt[0]];
  // stress
  else if(comp == IPField::STRESS_XX_LAMM01)
     return _nldStatev[_pos_strs_lam_m0[0]];
  else if(comp == IPField::STRESS_XX_LAMVT1)
     return _nldStatev[_pos_strs_lam_vt[0]];
  // damage
  else if(comp == IPField::DAMAGE_LAMM01){
      if(_pos_dam_lam_m0[0] > -1) return _nldStatev[_pos_dam_lam_m0[0]];
      return 0.;
  }
  // for each VT grain in laminate ply
  // strain
  else if(comp == IPField::STRAIN_XX_LAMVTMT1)
     return _nldStatev[_pos_strn_lam_vt_mt[0]];
  else if(comp == IPField::STRAIN_XX_LAMVTMT2)
     return _nldStatev[_pos_strn_lam_vt_mt[1]];
  else if(comp == IPField::STRAIN_XX_LAMVTMT3)
     return _nldStatev[_pos_strn_lam_vt_mt[2]];
  else if(comp == IPField::STRAIN_XX_LAMVTMT4)
     return _nldStatev[_pos_strn_lam_vt_mt[3]];
  else if(comp == IPField::STRAIN_XX_LAMVTMT5)
     return _nldStatev[_pos_strn_lam_vt_mt[4]];
  else if(comp == IPField::STRAIN_XX_LAMVTMT6)
     return _nldStatev[_pos_strn_lam_vt_mt[5]];
  else if(comp == IPField::STRAIN_XX_LAMVTMT7)
     return _nldStatev[_pos_strn_lam_vt_mt[6]];
  else if(comp == IPField::STRAIN_XX_LAMVTMT8)
     return _nldStatev[_pos_strn_lam_vt_mt[7]];
  else if(comp == IPField::STRAIN_XX_LAMVTMT9)
     return _nldStatev[_pos_strn_lam_vt_mt[8]];
  else if(comp == IPField::STRAIN_XX_LAMVTMT10)
     return _nldStatev[_pos_strn_lam_vt_mt[9]];
  else if(comp == IPField::STRAIN_XX_LAMVTMT11)
     return _nldStatev[_pos_strn_lam_vt_mt[10]];
  else if(comp == IPField::STRAIN_XX_LAMVTMT12)
     return _nldStatev[_pos_strn_lam_vt_mt[11]];
  else if(comp == IPField::STRAIN_XX_LAMVTMT13)
     return _nldStatev[_pos_strn_lam_vt_mt[12]];
  else if(comp == IPField::STRAIN_XX_LAMVTMT14)
     return _nldStatev[_pos_strn_lam_vt_mt[13]];
  else if(comp == IPField::STRAIN_XX_LAMVTMT15)
     return _nldStatev[_pos_strn_lam_vt_mt[14]];
  else if(comp == IPField::STRAIN_XX_LAMVTMT16)
     return _nldStatev[_pos_strn_lam_vt_mt[15]];
  else if(comp == IPField::STRAIN_XX_LAMVTMT17)
     return _nldStatev[_pos_strn_lam_vt_mt[16]];
  else if(comp == IPField::STRAIN_XX_LAMVTMT18)
     return _nldStatev[_pos_strn_lam_vt_mt[17]];
  else if(comp == IPField::STRAIN_XX_LAMVTMT19)
     return _nldStatev[_pos_strn_lam_vt_mt[18]];
  else if(comp == IPField::STRAIN_XX_LAMVTMT20)
     return _nldStatev[_pos_strn_lam_vt_mt[19]];
  // stress
  else if(comp == IPField::STRESS_XX_LAMVTMT1)
     return _nldStatev[_pos_strs_lam_vt_mt[0]];
  else if(comp == IPField::STRESS_XX_LAMVTMT2)
     return _nldStatev[_pos_strs_lam_vt_mt[1]];
  else if(comp == IPField::STRESS_XX_LAMVTMT3)
     return _nldStatev[_pos_strs_lam_vt_mt[2]];
  else if(comp == IPField::STRESS_XX_LAMVTMT4)
     return _nldStatev[_pos_strs_lam_vt_mt[3]];
  else if(comp == IPField::STRESS_XX_LAMVTMT5)
     return _nldStatev[_pos_strs_lam_vt_mt[4]];
  else if(comp == IPField::STRESS_XX_LAMVTMT6)
     return _nldStatev[_pos_strs_lam_vt_mt[5]];
  else if(comp == IPField::STRESS_XX_LAMVTMT7)
     return _nldStatev[_pos_strs_lam_vt_mt[6]];
  else if(comp == IPField::STRESS_XX_LAMVTMT8)
     return _nldStatev[_pos_strs_lam_vt_mt[7]];
  else if(comp == IPField::STRESS_XX_LAMVTMT9)
     return _nldStatev[_pos_strs_lam_vt_mt[8]];
  else if(comp == IPField::STRESS_XX_LAMVTMT10)
     return _nldStatev[_pos_strs_lam_vt_mt[9]];
  else if(comp == IPField::STRESS_XX_LAMVTMT11)
     return _nldStatev[_pos_strs_lam_vt_mt[10]];
  else if(comp == IPField::STRESS_XX_LAMVTMT12)
     return _nldStatev[_pos_strs_lam_vt_mt[11]];
  else if(comp == IPField::STRESS_XX_LAMVTMT13)
     return _nldStatev[_pos_strs_lam_vt_mt[12]];
  else if(comp == IPField::STRESS_XX_LAMVTMT14)
     return _nldStatev[_pos_strs_lam_vt_mt[13]];
  else if(comp == IPField::STRESS_XX_LAMVTMT15)
     return _nldStatev[_pos_strs_lam_vt_mt[14]];
  else if(comp == IPField::STRESS_XX_LAMVTMT16)
     return _nldStatev[_pos_strs_lam_vt_mt[15]];
  else if(comp == IPField::STRESS_XX_LAMVTMT17)
     return _nldStatev[_pos_strs_lam_vt_mt[16]];
  else if(comp == IPField::STRESS_XX_LAMVTMT18)
     return _nldStatev[_pos_strs_lam_vt_mt[17]];
  else if(comp == IPField::STRESS_XX_LAMVTMT19)
     return _nldStatev[_pos_strs_lam_vt_mt[18]];
  else if(comp == IPField::STRESS_XX_LAMVTMT20)
     return _nldStatev[_pos_strs_lam_vt_mt[19]];
  // for each material phase
  // strain
  else if(comp == IPField::STRAIN_XX_LAMVTMT1_MTX)
     return _nldStatev[_pos_strn_lam_vt_mt_mtx[0]];
  else if(comp == IPField::STRAIN_XX_LAMVTMT1_INC)
     return _nldStatev[_pos_strn_lam_vt_mt_inc[0]];
  else if(comp == IPField::STRAIN_XX_LAMVTMT2_MTX)
     return _nldStatev[_pos_strn_lam_vt_mt_mtx[1]];
  else if(comp == IPField::STRAIN_XX_LAMVTMT2_INC)
     return _nldStatev[_pos_strn_lam_vt_mt_inc[1]];
  else if(comp == IPField::STRAIN_XX_LAMVTMT3_MTX)
     return _nldStatev[_pos_strn_lam_vt_mt_mtx[2]];
  else if(comp == IPField::STRAIN_XX_LAMVTMT3_INC)
     return _nldStatev[_pos_strn_lam_vt_mt_inc[2]];
  else if(comp == IPField::STRAIN_XX_LAMVTMT4_MTX)
     return _nldStatev[_pos_strn_lam_vt_mt_mtx[3]];
  else if(comp == IPField::STRAIN_XX_LAMVTMT4_INC)
     return _nldStatev[_pos_strn_lam_vt_mt_inc[3]];
  else if(comp == IPField::STRAIN_XX_LAMVTMT5_MTX)
     return _nldStatev[_pos_strn_lam_vt_mt_mtx[4]];
  else if(comp == IPField::STRAIN_XX_LAMVTMT5_INC)
     return _nldStatev[_pos_strn_lam_vt_mt_inc[4]];
  else if(comp == IPField::STRAIN_XX_LAMVTMT6_MTX)
     return _nldStatev[_pos_strn_lam_vt_mt_mtx[5]];
  else if(comp == IPField::STRAIN_XX_LAMVTMT6_INC)
     return _nldStatev[_pos_strn_lam_vt_mt_inc[5]];
  else if(comp == IPField::STRAIN_XX_LAMVTMT7_MTX)
     return _nldStatev[_pos_strn_lam_vt_mt_mtx[6]];
  else if(comp == IPField::STRAIN_XX_LAMVTMT7_INC)
     return _nldStatev[_pos_strn_lam_vt_mt_inc[6]];
  else if(comp == IPField::STRAIN_XX_LAMVTMT8_MTX)
     return _nldStatev[_pos_strn_lam_vt_mt_mtx[7]];
  else if(comp == IPField::STRAIN_XX_LAMVTMT8_INC)
     return _nldStatev[_pos_strn_lam_vt_mt_inc[7]];
  else if(comp == IPField::STRAIN_XX_LAMVTMT9_MTX)
     return _nldStatev[_pos_strn_lam_vt_mt_mtx[8]];
  else if(comp == IPField::STRAIN_XX_LAMVTMT9_INC)
     return _nldStatev[_pos_strn_lam_vt_mt_inc[8]];
  else if(comp == IPField::STRAIN_XX_LAMVTMT10_MTX)
     return _nldStatev[_pos_strn_lam_vt_mt_mtx[9]];
  else if(comp == IPField::STRAIN_XX_LAMVTMT10_INC)
     return _nldStatev[_pos_strn_lam_vt_mt_inc[9]];
  else if(comp == IPField::STRAIN_XX_LAMVTMT11_MTX)
     return _nldStatev[_pos_strn_lam_vt_mt_mtx[10]];
  else if(comp == IPField::STRAIN_XX_LAMVTMT11_INC)
     return _nldStatev[_pos_strn_lam_vt_mt_inc[10]];
  else if(comp == IPField::STRAIN_XX_LAMVTMT12_MTX)
     return _nldStatev[_pos_strn_lam_vt_mt_mtx[11]];
  else if(comp == IPField::STRAIN_XX_LAMVTMT12_INC)
     return _nldStatev[_pos_strn_lam_vt_mt_inc[11]];
  else if(comp == IPField::STRAIN_XX_LAMVTMT13_MTX)
     return _nldStatev[_pos_strn_lam_vt_mt_mtx[12]];
  else if(comp == IPField::STRAIN_XX_LAMVTMT13_INC)
     return _nldStatev[_pos_strn_lam_vt_mt_inc[12]];
  else if(comp == IPField::STRAIN_XX_LAMVTMT14_MTX)
     return _nldStatev[_pos_strn_lam_vt_mt_mtx[13]];
  else if(comp == IPField::STRAIN_XX_LAMVTMT14_INC)
     return _nldStatev[_pos_strn_lam_vt_mt_inc[13]];
  else if(comp == IPField::STRAIN_XX_LAMVTMT15_MTX)
     return _nldStatev[_pos_strn_lam_vt_mt_mtx[14]];
  else if(comp == IPField::STRAIN_XX_LAMVTMT15_INC)
     return _nldStatev[_pos_strn_lam_vt_mt_inc[14]];
  else if(comp == IPField::STRAIN_XX_LAMVTMT16_MTX)
     return _nldStatev[_pos_strn_lam_vt_mt_mtx[15]];
  else if(comp == IPField::STRAIN_XX_LAMVTMT16_INC)
     return _nldStatev[_pos_strn_lam_vt_mt_inc[15]];
  else if(comp == IPField::STRAIN_XX_LAMVTMT17_MTX)
     return _nldStatev[_pos_strn_lam_vt_mt_mtx[16]];
  else if(comp == IPField::STRAIN_XX_LAMVTMT17_INC)
     return _nldStatev[_pos_strn_lam_vt_mt_inc[16]];
  else if(comp == IPField::STRAIN_XX_LAMVTMT18_MTX)
     return _nldStatev[_pos_strn_lam_vt_mt_mtx[17]];
  else if(comp == IPField::STRAIN_XX_LAMVTMT18_INC)
     return _nldStatev[_pos_strn_lam_vt_mt_inc[17]];
  else if(comp == IPField::STRAIN_XX_LAMVTMT19_MTX)
     return _nldStatev[_pos_strn_lam_vt_mt_mtx[18]];
  else if(comp == IPField::STRAIN_XX_LAMVTMT19_INC)
     return _nldStatev[_pos_strn_lam_vt_mt_inc[18]];
  else if(comp == IPField::STRAIN_XX_LAMVTMT20_MTX)
     return _nldStatev[_pos_strn_lam_vt_mt_mtx[19]];
  else if(comp == IPField::STRAIN_XX_LAMVTMT20_INC)
     return _nldStatev[_pos_strn_lam_vt_mt_inc[19]];
  // stress
  else if(comp == IPField::STRESS_XX_LAMVTMT1_MTX)
     return _nldStatev[_pos_strs_lam_vt_mt_mtx[0]];
  else if(comp == IPField::STRESS_XX_LAMVTMT1_INC)
     return _nldStatev[_pos_strs_lam_vt_mt_inc[0]];
  else if(comp == IPField::STRESS_XX_LAMVTMT2_MTX)
     return _nldStatev[_pos_strs_lam_vt_mt_mtx[1]];
  else if(comp == IPField::STRESS_XX_LAMVTMT2_INC)
     return _nldStatev[_pos_strs_lam_vt_mt_inc[1]];
  else if(comp == IPField::STRESS_XX_LAMVTMT3_MTX)
     return _nldStatev[_pos_strs_lam_vt_mt_mtx[2]];
  else if(comp == IPField::STRESS_XX_LAMVTMT3_INC)
     return _nldStatev[_pos_strs_lam_vt_mt_inc[2]];
  else if(comp == IPField::STRESS_XX_LAMVTMT4_MTX)
     return _nldStatev[_pos_strs_lam_vt_mt_mtx[3]];
  else if(comp == IPField::STRESS_XX_LAMVTMT4_INC)
     return _nldStatev[_pos_strs_lam_vt_mt_inc[3]];
  else if(comp == IPField::STRESS_XX_LAMVTMT5_MTX)
     return _nldStatev[_pos_strs_lam_vt_mt_mtx[4]];
  else if(comp == IPField::STRESS_XX_LAMVTMT5_INC)
     return _nldStatev[_pos_strs_lam_vt_mt_inc[4]];
  else if(comp == IPField::STRESS_XX_LAMVTMT6_MTX)
     return _nldStatev[_pos_strs_lam_vt_mt_mtx[5]];
  else if(comp == IPField::STRESS_XX_LAMVTMT6_INC)
     return _nldStatev[_pos_strs_lam_vt_mt_inc[5]];
  else if(comp == IPField::STRESS_XX_LAMVTMT7_MTX)
     return _nldStatev[_pos_strs_lam_vt_mt_mtx[6]];
  else if(comp == IPField::STRESS_XX_LAMVTMT7_INC)
     return _nldStatev[_pos_strs_lam_vt_mt_inc[6]];
  else if(comp == IPField::STRESS_XX_LAMVTMT8_MTX)
     return _nldStatev[_pos_strs_lam_vt_mt_mtx[7]];
  else if(comp == IPField::STRESS_XX_LAMVTMT8_INC)
     return _nldStatev[_pos_strs_lam_vt_mt_inc[7]];
  else if(comp == IPField::STRESS_XX_LAMVTMT9_MTX)
     return _nldStatev[_pos_strs_lam_vt_mt_mtx[8]];
  else if(comp == IPField::STRESS_XX_LAMVTMT9_INC)
     return _nldStatev[_pos_strs_lam_vt_mt_inc[8]];
  else if(comp == IPField::STRESS_XX_LAMVTMT10_MTX)
     return _nldStatev[_pos_strs_lam_vt_mt_mtx[9]];
  else if(comp == IPField::STRESS_XX_LAMVTMT10_INC)
     return _nldStatev[_pos_strs_lam_vt_mt_inc[9]];
  else if(comp == IPField::STRESS_XX_LAMVTMT11_MTX)
     return _nldStatev[_pos_strs_lam_vt_mt_mtx[10]];
  else if(comp == IPField::STRESS_XX_LAMVTMT11_INC)
     return _nldStatev[_pos_strs_lam_vt_mt_inc[10]];
  else if(comp == IPField::STRESS_XX_LAMVTMT12_MTX)
     return _nldStatev[_pos_strs_lam_vt_mt_mtx[11]];
  else if(comp == IPField::STRESS_XX_LAMVTMT12_INC)
     return _nldStatev[_pos_strs_lam_vt_mt_inc[11]];
  else if(comp == IPField::STRESS_XX_LAMVTMT13_MTX)
     return _nldStatev[_pos_strs_lam_vt_mt_mtx[12]];
  else if(comp == IPField::STRESS_XX_LAMVTMT13_INC)
     return _nldStatev[_pos_strs_lam_vt_mt_inc[12]];
  else if(comp == IPField::STRESS_XX_LAMVTMT14_MTX)
     return _nldStatev[_pos_strs_lam_vt_mt_mtx[13]];
  else if(comp == IPField::STRESS_XX_LAMVTMT14_INC)
     return _nldStatev[_pos_strs_lam_vt_mt_inc[13]];
  else if(comp == IPField::STRESS_XX_LAMVTMT15_MTX)
     return _nldStatev[_pos_strs_lam_vt_mt_mtx[14]];
  else if(comp == IPField::STRESS_XX_LAMVTMT15_INC)
     return _nldStatev[_pos_strs_lam_vt_mt_inc[14]];
  else if(comp == IPField::STRESS_XX_LAMVTMT16_MTX)
     return _nldStatev[_pos_strs_lam_vt_mt_mtx[15]];
  else if(comp == IPField::STRESS_XX_LAMVTMT16_INC)
     return _nldStatev[_pos_strs_lam_vt_mt_inc[15]];
  else if(comp == IPField::STRESS_XX_LAMVTMT17_MTX)
     return _nldStatev[_pos_strs_lam_vt_mt_mtx[16]];
  else if(comp == IPField::STRESS_XX_LAMVTMT17_INC)
     return _nldStatev[_pos_strs_lam_vt_mt_inc[16]];
  else if(comp == IPField::STRESS_XX_LAMVTMT18_MTX)
     return _nldStatev[_pos_strs_lam_vt_mt_mtx[17]];
  else if(comp == IPField::STRESS_XX_LAMVTMT18_INC)
     return _nldStatev[_pos_strs_lam_vt_mt_inc[17]];
  else if(comp == IPField::STRESS_XX_LAMVTMT19_MTX)
     return _nldStatev[_pos_strs_lam_vt_mt_mtx[18]];
  else if(comp == IPField::STRESS_XX_LAMVTMT19_INC)
     return _nldStatev[_pos_strs_lam_vt_mt_inc[18]];
  else if(comp == IPField::STRESS_XX_LAMVTMT20_MTX)
     return _nldStatev[_pos_strs_lam_vt_mt_mtx[19]];
  else if(comp == IPField::STRESS_XX_LAMVTMT20_INC)
     return _nldStatev[_pos_strs_lam_vt_mt_inc[19]];
  // damage
  else if(comp == IPField::DAMAGE_LAMVTMT1_MTX){
      if(_pos_dam_lam_vt_mt_mtx[0] > -1) return _nldStatev[_pos_dam_lam_vt_mt_mtx[0]];
      return 0.;
  }
  else if(comp == IPField::DAMAGE_LAMVTMT1_INC){
      if(_pos_dam_lam_vt_mt_inc[0] > -1) return _nldStatev[_pos_dam_lam_vt_mt_inc[0]];
      return 0.;
  }
  else if(comp == IPField::DAMAGE_LAMVTMT2_MTX){
      if(_pos_dam_lam_vt_mt_mtx[1] > -1) return _nldStatev[_pos_dam_lam_vt_mt_mtx[1]];
      return 0.;
  }
  else if(comp == IPField::DAMAGE_LAMVTMT2_INC){
      if(_pos_dam_lam_vt_mt_inc[1] > -1) return _nldStatev[_pos_dam_lam_vt_mt_inc[1]];
      return 0.;
  }
  else if(comp == IPField::DAMAGE_LAMVTMT3_MTX){
      if(_pos_dam_lam_vt_mt_mtx[2] > -1) return _nldStatev[_pos_dam_lam_vt_mt_mtx[2]];
      return 0.;
  }
  else if(comp == IPField::DAMAGE_LAMVTMT3_INC){
      if(_pos_dam_lam_vt_mt_inc[2] > -1) return _nldStatev[_pos_dam_lam_vt_mt_inc[2]];
      return 0.;
  }
  else if(comp == IPField::DAMAGE_LAMVTMT4_MTX){
      if(_pos_dam_lam_vt_mt_mtx[3] > -1) return _nldStatev[_pos_dam_lam_vt_mt_mtx[3]];
      return 0.;
  }
  else if(comp == IPField::DAMAGE_LAMVTMT4_INC){
      if(_pos_dam_lam_vt_mt_inc[3] > -1) return _nldStatev[_pos_dam_lam_vt_mt_inc[3]];
      return 0.;
  }
  else if(comp == IPField::DAMAGE_LAMVTMT5_MTX){
      if(_pos_dam_lam_vt_mt_mtx[4] > -1) return _nldStatev[_pos_dam_lam_vt_mt_mtx[4]];
      return 0.;
  }
  else if(comp == IPField::DAMAGE_LAMVTMT5_INC){
      if(_pos_dam_lam_vt_mt_inc[4] > -1) return _nldStatev[_pos_dam_lam_vt_mt_inc[4]];
      return 0.;
  }
  else if(comp == IPField::DAMAGE_LAMVTMT6_MTX){
      if(_pos_dam_lam_vt_mt_mtx[5] > -1) return _nldStatev[_pos_dam_lam_vt_mt_mtx[5]];
      return 0.;
  }
  else if(comp == IPField::DAMAGE_LAMVTMT6_INC){
      if(_pos_dam_lam_vt_mt_inc[5] > -1) return _nldStatev[_pos_dam_lam_vt_mt_inc[5]];
      return 0.;
  }
  else if(comp == IPField::DAMAGE_LAMVTMT7_MTX){
      if(_pos_dam_lam_vt_mt_mtx[6] > -1) return _nldStatev[_pos_dam_lam_vt_mt_mtx[6]];
      return 0.;
  }
  else if(comp == IPField::DAMAGE_LAMVTMT7_INC){
      if(_pos_dam_lam_vt_mt_inc[6] > -1) return _nldStatev[_pos_dam_lam_vt_mt_inc[6]];
      return 0.;
  }
  else if(comp == IPField::DAMAGE_LAMVTMT8_MTX){
      if(_pos_dam_lam_vt_mt_mtx[7] > -1) return _nldStatev[_pos_dam_lam_vt_mt_mtx[7]];
      return 0.;
  }
  else if(comp == IPField::DAMAGE_LAMVTMT8_INC){
      if(_pos_dam_lam_vt_mt_inc[7] > -1) return _nldStatev[_pos_dam_lam_vt_mt_inc[7]];
      return 0.;
  }
  else if(comp == IPField::DAMAGE_LAMVTMT9_MTX){
      if(_pos_dam_lam_vt_mt_mtx[8] > -1) return _nldStatev[_pos_dam_lam_vt_mt_mtx[8]];
      return 0.;
  }
  else if(comp == IPField::DAMAGE_LAMVTMT9_INC){
      if(_pos_dam_lam_vt_mt_inc[8] > -1) return _nldStatev[_pos_dam_lam_vt_mt_inc[8]];
      return 0.;
  }
  else if(comp == IPField::DAMAGE_LAMVTMT10_MTX){
      if(_pos_dam_lam_vt_mt_mtx[9] > -1) return _nldStatev[_pos_dam_lam_vt_mt_mtx[9]];
      return 0.;
  }
  else if(comp == IPField::DAMAGE_LAMVTMT10_INC){
      if(_pos_dam_lam_vt_mt_inc[9] > -1) return _nldStatev[_pos_dam_lam_vt_mt_inc[9]];
      return 0.;
  }
  else if(comp == IPField::DAMAGE_LAMVTMT11_MTX){
      if(_pos_dam_lam_vt_mt_mtx[10] > -1) return _nldStatev[_pos_dam_lam_vt_mt_mtx[10]];
      return 0.;
  }
  else if(comp == IPField::DAMAGE_LAMVTMT11_INC){
      if(_pos_dam_lam_vt_mt_inc[10] > -1) return _nldStatev[_pos_dam_lam_vt_mt_inc[10]];
      return 0.;
  }
  else if(comp == IPField::DAMAGE_LAMVTMT12_MTX){
      if(_pos_dam_lam_vt_mt_mtx[11] > -1) return _nldStatev[_pos_dam_lam_vt_mt_mtx[11]];
      return 0.;
  }
  else if(comp == IPField::DAMAGE_LAMVTMT12_INC){
      if(_pos_dam_lam_vt_mt_inc[11] > -1) return _nldStatev[_pos_dam_lam_vt_mt_inc[11]];
      return 0.;
  }
  else if(comp == IPField::DAMAGE_LAMVTMT13_MTX){
      if(_pos_dam_lam_vt_mt_mtx[12] > -1) return _nldStatev[_pos_dam_lam_vt_mt_mtx[12]];
      return 0.;
  }
  else if(comp == IPField::DAMAGE_LAMVTMT13_INC){
      if(_pos_dam_lam_vt_mt_inc[12] > -1) return _nldStatev[_pos_dam_lam_vt_mt_inc[12]];
      return 0.;
  }
  else if(comp == IPField::DAMAGE_LAMVTMT14_MTX){
      if(_pos_dam_lam_vt_mt_mtx[13] > -1) return _nldStatev[_pos_dam_lam_vt_mt_mtx[13]];
      return 0.;
  }
  else if(comp == IPField::DAMAGE_LAMVTMT14_INC){
      if(_pos_dam_lam_vt_mt_inc[13] > -1) return _nldStatev[_pos_dam_lam_vt_mt_inc[13]];
      return 0.;
  }
  else if(comp == IPField::DAMAGE_LAMVTMT15_MTX){
      if(_pos_dam_lam_vt_mt_mtx[14] > -1) return _nldStatev[_pos_dam_lam_vt_mt_mtx[14]];
      return 0.;
  }
  else if(comp == IPField::DAMAGE_LAMVTMT15_INC){
      if(_pos_dam_lam_vt_mt_inc[14] > -1) return _nldStatev[_pos_dam_lam_vt_mt_inc[14]];
      return 0.;
  }
  else if(comp == IPField::DAMAGE_LAMVTMT16_MTX){
      if(_pos_dam_lam_vt_mt_mtx[15] > -1) return _nldStatev[_pos_dam_lam_vt_mt_mtx[15]];
      return 0.;
  }
  else if(comp == IPField::DAMAGE_LAMVTMT16_INC){
      if(_pos_dam_lam_vt_mt_inc[15] > -1) return _nldStatev[_pos_dam_lam_vt_mt_inc[15]];
      return 0.;
  }
  else if(comp == IPField::DAMAGE_LAMVTMT17_MTX){
      if(_pos_dam_lam_vt_mt_mtx[16] > -1) return _nldStatev[_pos_dam_lam_vt_mt_mtx[16]];
      return 0.;
  }
  else if(comp == IPField::DAMAGE_LAMVTMT17_INC){
      if(_pos_dam_lam_vt_mt_inc[16] > -1) return _nldStatev[_pos_dam_lam_vt_mt_inc[16]];
      return 0.;
  }
  else if(comp == IPField::DAMAGE_LAMVTMT18_MTX){
      if(_pos_dam_lam_vt_mt_mtx[17] > -1) return _nldStatev[_pos_dam_lam_vt_mt_mtx[17]];
      return 0.;
  }
  else if(comp == IPField::DAMAGE_LAMVTMT18_INC){
      if(_pos_dam_lam_vt_mt_inc[17] > -1) return _nldStatev[_pos_dam_lam_vt_mt_inc[17]];
      return 0.;
  }
  else if(comp == IPField::DAMAGE_LAMVTMT19_MTX){
      if(_pos_dam_lam_vt_mt_mtx[18] > -1) return _nldStatev[_pos_dam_lam_vt_mt_mtx[18]];
      return 0.;
  }
  else if(comp == IPField::DAMAGE_LAMVTMT19_INC){
      if(_pos_dam_lam_vt_mt_inc[18] > -1) return _nldStatev[_pos_dam_lam_vt_mt_inc[18]];
      return 0.;
  }
  else if(comp == IPField::DAMAGE_LAMVTMT20_MTX){
      if(_pos_dam_lam_vt_mt_mtx[19] > -1) return _nldStatev[_pos_dam_lam_vt_mt_mtx[19]];
      return 0.;
  }
  else if(comp == IPField::DAMAGE_LAMVTMT20_INC){
      if(_pos_dam_lam_vt_mt_inc[19] > -1) return _nldStatev[_pos_dam_lam_vt_mt_inc[19]];
      return 0.;
  }


  else if(comp == IPField::DAMAGE) // damage
     return _nldDamage;
  else if(comp == IPField::PLASTICSTRAIN)
     return _nldCurrentPlasticStrain;
  else if(comp >= IPField::STRAIN_XX and comp<IPField::STRAIN_XY )
	  return _nldStrain(comp-IPField::STRAIN_XX);
  else if(comp == IPField::STRAIN_XY)
	  return _nldStrain(comp-IPField::STRAIN_XX)/2.;
  else if(comp==IPField::STRAIN_XZ) // order xx yy zz xy xz yz
	  return _nldStrain(comp-IPField::STRAIN_XX-1)/2.;
  else if(comp==IPField::STRAIN_YZ)
	  return _nldStrain(comp-IPField::STRAIN_XX+1)/2.;
  else if(comp>=  IPField::MTX_SVM and comp<=IPField::INC_SIG_XZ and _pos_str_mtx >=0 and _pos_str_inc >=0)
  {
     const double *str = NULL;
     double sq2 = sqrt(2.);
     double Ja=1.;
     if(comp>=  IPField::MTX_SVM and comp<=IPField::MTX_SIG_XZ)
     { 
        str = &(_nldStatev[_pos_str_mtx]);
        if(_pos_mtx_Ja>=0) Ja=_nldStatev[_pos_mtx_Ja];
     }
     else
     { 
        str = &(_nldStatev[_pos_str_inc]);
        if(_pos_inc_Ja>=0) Ja=_nldStatev[_pos_inc_Ja];
     }
     static fullVector<double> _cauchy(6);
     //in composite cauchy is stored, but in phases this is Kirchhoff
     for(int i=0;i<3;i++)
     {
       _cauchy(i)=str[i]/Ja;
       _cauchy(i+3) = str[i+3]/sq2/Ja;
     }
     if(comp == IPField::MTX_SVM or comp == IPField::INC_SVM)
     {
        double svm= (_cauchy(0)-_cauchy(1))*(_cauchy(0)-_cauchy(1))+(_cauchy(2)-_cauchy(1))*(_cauchy(2)-_cauchy(1))+(_cauchy(2)-_cauchy(0))*(_cauchy(2)-_cauchy(0));
       svm += 6.*(_cauchy(3)*_cauchy(3)+_cauchy(4)*_cauchy(4)+_cauchy(5)*_cauchy(5));
       svm /= 2.;
       svm = sqrt(svm);
       return svm;
     }
     else if(comp == IPField::MTX_SIG_XX or comp == IPField::INC_SIG_XX)
       return _cauchy(0);
     else if(comp == IPField::MTX_SIG_YY or comp == IPField::INC_SIG_YY)
       return _cauchy(1);
     else if(comp == IPField::MTX_SIG_ZZ or comp == IPField::INC_SIG_ZZ)
       return _cauchy(2);
     else if(comp == IPField::MTX_SIG_XY or comp == IPField::INC_SIG_XY)
       return _cauchy(3);
     else if(comp == IPField::MTX_SIG_YZ or comp == IPField::INC_SIG_YZ)
       return _cauchy(5);
     else if(comp == IPField::MTX_SIG_XZ or comp == IPField::INC_SIG_XZ)
       return _cauchy(4);
     else
       return 0.;
  }
  else if((comp>=  IPField::MTX_STRAIN_XX and comp<=IPField::MTX_STRAIN_XZ) or
          (comp>=  IPField::INC_STRAIN_XX and comp<=IPField::INC_STRAIN_XZ) 
           and _pos_str_mtx >=0 and _pos_str_inc >=0)
  {
     const double *stn = NULL;
     double sq2 = sqrt(2.);
     if(comp>=  IPField::MTX_STRAIN_XX and comp<=IPField::MTX_STRAIN_XZ) stn = &(_nldStatev[_pos_stn_mtx]);
     else stn = &(_nldStatev[_pos_stn_inc]);

     static fullVector<double> _strain(6);
     for(int i=0;i<3;i++)
     {
       _strain(i)=stn[i];
       _strain(i+3) = stn[i+3]*sq2;
     }
     if(comp == IPField::MTX_STRAIN_XX or comp == IPField::INC_STRAIN_XX)
       return _strain(0);
     else if(comp == IPField::MTX_STRAIN_YY or comp == IPField::INC_STRAIN_YY)
       return _strain(1);
     else if(comp == IPField::MTX_STRAIN_ZZ or comp == IPField::INC_STRAIN_ZZ)
       return _strain(2);
     else if(comp == IPField::MTX_STRAIN_XY or comp == IPField::INC_STRAIN_XY)
       return _strain(3);
     else if(comp == IPField::MTX_STRAIN_YZ or comp == IPField::INC_STRAIN_YZ)
       return _strain(5);
     else if(comp == IPField::MTX_STRAIN_XZ or comp == IPField::INC_STRAIN_XZ)
       return _strain(4);
     else
       return 0.;
  }
  else if(comp==IPField::INC_VOLUME_RATIO)
  {
    if(_pos_inc_vfi>0)
      return _nldStatev[_pos_inc_vfi];
    else
      return 0.;
  }
  else if(comp==IPField::INC_DAMAGE)
  {
    if(_pos_inc_Dam>0)
      return _nldStatev[_pos_inc_Dam];
    else
      return 0.;
  }
  else if(comp==IPField::MTX_DAMAGE)
  {
    if(_pos_mtx_Dam>0)
      return _nldStatev[_pos_mtx_Dam];
    else
      return 0.;
  }
  else if(comp==IPField::INC_ASPECT_RATIO_1 || comp==IPField::INC_ASPECT_RATIO_2)
  {
    if(_pos_inc_ar>0)
      return _nldStatev[_pos_inc_ar+comp-IPField::INC_ASPECT_RATIO_1];
    else
      return 0.;
  }
  else if(comp>=IPField::INC_R_XX and comp<=IPField::INC_R_ZZ)
  {
    if(_pos_inc_R>0)
      return _nldStatev[_pos_inc_R+comp-IPField::INC_R_XX];
    else
      return 0.;
  }
  else if(comp==IPField::EIGENSTRESS_XX)
  {
    return _eigenstress(0,0);
  }
  else if(comp==IPField::EIGENSTRESS_YY)
  {
    return _eigenstress(1,1);
  }
  else if(comp==IPField::EIGENSTRESS_ZZ)
  {
    return _eigenstress(2,2);
  }
  else if(comp==IPField::EIGENSTRESS_XY)
  {
    return _eigenstress(0,1);
  }
  else if(comp==IPField::EIGENSTRESS_XZ)
  {
    return _eigenstress(0,2);
  }
  else if(comp==IPField::EIGENSTRESS_YZ)
  {
    return _eigenstress(1,2);
  }
  else if(comp==IPField::EIGENSTRAIN_XX)
  {
    return _eigenstrain(0,0);
  }
  else if(comp==IPField::EIGENSTRAIN_YY)
  {
    return _eigenstrain(1,1);
  }
  else if(comp==IPField::EIGENSTRAIN_ZZ)
  {
    return _eigenstrain(2,2);
  }
  else if(comp==IPField::EIGENSTRAIN_XY)
  {
    return _eigenstrain(0,1);
  }
  else if(comp==IPField::EIGENSTRAIN_XZ)
  {
    return _eigenstrain(0,2);
  }
  else if(comp==IPField::EIGENSTRAIN_YZ)
  {
    return _eigenstrain(1,2);
  }
  else
     return 0.;
  return 0.;
}

void IPNonLocalDamage::restart()
{
  IPVariableMechanics::restart();
  restartManager::restart(_nldNsdv);
  restartManager::restart(_pos_str_mtx);
  restartManager::restart(_pos_str_inc);
  restartManager::restart(_pos_stn_mtx);
  restartManager::restart(_pos_stn_inc);
  restartManager::restart(_pos_inc_vfi);
  restartManager::restart(_pos_inc_ar);
  restartManager::restart(_pos_inc_R);        
  restartManager::restart(_pos_inc_Ja);
  restartManager::restart(_pos_mtx_Ja);
  restartManager::restart(_pos_inc_Dam);
  restartManager::restart(_pos_mtx_Dam);
  restartManager::restart(_nldMaterialTensor.getDataPtr(),36);
  restartManager::restart(_nldCouplingStressEffectiveStrain.getDataPtr(),6);
  restartManager::restart(_nldCouplingEffectiveStrainStress.getDataPtr(),6);
  restartManager::restart(_nldFiberDamdStrain.getDataPtr(),6);
  restartManager::restart(_nldStressdFiberDam.getDataPtr(),6);

  restartManager::restart(_nldStrain.getDataPtr(),6);
  restartManager::restart(_nldStress.getDataPtr(),6);
  restartManager::restart(_nldCharacteristicLengthMatrix);
  restartManager::restart(_nldCharacteristicLengthFiber);
  restartManager::restart(_nldEffectivePlasticStrain);
  restartManager::restart(_nldCurrentPlasticStrain);
  restartManager::restart(_nlFiber_loc);
  restartManager::restart(_nldDamage);
  restartManager::restart(_nlFiber_d_bar);
  restartManager::restart(_nldSpBar);
  restartManager::restart(_nldFd_d_bar);
  restartManager::restart(_nldpdFd);
  restartManager::restart(_nldFddp);

  restartManager::restart(_nldStatev,_nldNsdv);
  restartManager::restart(_elasticEne);
  restartManager::restart(_plasticEne);
  restartManager::restart(_stressWork);
  restartManager::restart(_dissipationBlocked);

  restartManager::restart(pos_vfi);
  restartManager::restart(pos_euler);
  restartManager::restart(pos_aspR); 
  restartManager::restart(pos_ME);
  restartManager::restart(pos_Mnu);
  restartManager::restart(pos_Msy0);
  restartManager::restart(pos_Mhmod1);
  restartManager::restart(pos_Mhmod2);
  restartManager::restart(pos_Malpha_DP);
  restartManager::restart(pos_Mm_DP);
  restartManager::restart(pos_Mnup);
  restartManager::restart(pos_Mhexp);
  restartManager::restart(pos_DamParm1);
  restartManager::restart(pos_DamParm2);
  restartManager::restart(pos_DamParm3);

  restartManager::restart(pos_INCDamParm1);
  restartManager::restart(pos_INCDamParm2);
  restartManager::restart(pos_INCDamParm3);

  restartManager::restart(Randnum);

  restartManager::restart(_irreversibleEnergy);
  restartManager::restart(_DirreversibleEnergyDF);
  restartManager::restart(_DirreversibleEnergyDNonLocalVariableMatrix);
  restartManager::restart(_DirreversibleEnergyDNonLocalVariableFiber);
  
  restartManager::restart(_eigenstress);
  restartManager::restart(_eigenstrain);

  // for VT case (VM or VLM)
  // for VLM case
  // laminate euler angles
  restartManager::restart(_euler_vt_lam);
  // rotate the strain or stress from local to global
  restartManager::restart(_str_loc);
  restartManager::restart(_str_glo);
  // for each VT phase
  // strain
  restartManager::restart(_pos_strn_vt_m0);
  restartManager::restart(_pos_strn_vt_mt);
  restartManager::restart(_pos_strn_vt_lam);
  // stress
  restartManager::restart(_pos_strs_vt_m0);
  restartManager::restart(_pos_strs_vt_mt);
  restartManager::restart(_pos_strs_vt_lam);
  // damage
  restartManager::restart(_pos_dam_vt_m0);
  // for each laminate ply
  // strain
  restartManager::restart(_pos_strn_vt_lam_m0);
  restartManager::restart(_pos_strn_vt_lam_mt);
  // stress
  restartManager::restart(_pos_strs_vt_lam_m0);
  restartManager::restart(_pos_strs_vt_lam_mt);
  // damage
  restartManager::restart(_pos_dam_vt_lam_m0);
  // for each material phase
  // strain
  restartManager::restart(_pos_strn_vt_mt_mtx);
  restartManager::restart(_pos_strn_vt_mt_inc);
  // stress
  restartManager::restart(_pos_strs_vt_mt_mtx);
  restartManager::restart(_pos_strs_vt_mt_inc);
  restartManager::restart(_pos_strs_vt_lam_mt_mtx);
  restartManager::restart(_pos_strs_vt_lam_mt_inc);
  // damage
  restartManager::restart(_pos_dam_vt_mt_mtx);
  restartManager::restart(_pos_dam_vt_mt_inc);
  restartManager::restart(_pos_dam_vt_lam_mt_mtx);
  restartManager::restart(_pos_dam_vt_lam_mt_inc);
  
  // for LAM_2PLY case (LVM)
  // for each laminate ply
  // strain
  restartManager::restart(_pos_strn_lam_m0);
  restartManager::restart(_pos_strn_lam_vt);
  // stress
  restartManager::restart(_pos_strs_lam_m0);
  restartManager::restart(_pos_strs_lam_vt);
  // damage
  restartManager::restart(_pos_dam_lam_m0);
  // for each VT grain in laminate ply
  // strain
  restartManager::restart(_pos_strn_lam_vt_mt);
  // stress
  restartManager::restart(_pos_strs_lam_vt_mt);
  // for each material phase
  // strain
  restartManager::restart(_pos_strn_lam_vt_mt_mtx);
  restartManager::restart(_pos_strn_lam_vt_mt_inc);
  // stress
  restartManager::restart(_pos_strs_lam_vt_mt_mtx);
  restartManager::restart(_pos_strs_lam_vt_mt_inc);
  // damage
  restartManager::restart(_pos_dam_lam_vt_mt_mtx);
  restartManager::restart(_pos_dam_lam_vt_mt_inc);

  return;
}

// MPI function to define value to be commmunicated between processes
#if defined(HAVE_MPI)
int IPNonLocalDamage::numberValuesToCommunicateMPI()const
{   
   int nb=Randnum;
   if(pos_euler !=0)  
     nb+=2;
   //Msg::Error("Use %d values",nb);
   return nb;
}
void IPNonLocalDamage::fillValuesToCommunicateMPI(double *arrayMPI)const
{
   //Msg::Error("Fill %d values",Randnum);
   if(Randnum>0)
   {
     int i=0;
     if(pos_vfi !=0)
     {
       arrayMPI[i]=_nldStatev[pos_vfi];
       i++;
     }
     if(pos_aspR !=0)
     {
       arrayMPI[i]=_nldStatev[pos_aspR];
       i++;
     }
     if(pos_ME !=0) 
     {
       arrayMPI[i]=_nldStatev[pos_ME];
       i++;
     }
     if(pos_Mnu !=0) 
     {
       arrayMPI[i]=_nldStatev[pos_Mnu];
       i++;
     }
     if(pos_Msy0 !=0) 
     {
       arrayMPI[i]=_nldStatev[pos_Msy0];
       i++;
     }
     if(pos_Mhmod1 !=0) 
     {
       arrayMPI[i]=_nldStatev[pos_Mhmod1];
       i++;
     }
     if(pos_Mhmod2 !=0) 
     {
       arrayMPI[i]=_nldStatev[pos_Mhmod2];
       i++;
     }
     if(pos_Malpha_DP !=0) 
     {
       arrayMPI[i]=_nldStatev[pos_Malpha_DP];
       i++;
     }
     if(pos_Mm_DP !=0) 
     {
       arrayMPI[i]=_nldStatev[pos_Mm_DP];
       i++;
     }
     if(pos_Mnup !=0) 
     {
       arrayMPI[i]=_nldStatev[pos_Mnup];
       i++;
     }
     if(pos_Mhexp !=0)  
     {
       arrayMPI[i]=_nldStatev[pos_Mhexp];
       i++;
     }
     if(pos_euler !=0)  
     {
       arrayMPI[i]=_nldStatev[pos_euler];
       i++;
       arrayMPI[i]=_nldStatev[pos_euler+1];
       i++;
       arrayMPI[i]=_nldStatev[pos_euler+2];
       i++;
     }
     if(pos_DamParm1 !=0)  
     {
       arrayMPI[i]=_nldStatev[pos_DamParm1];
       i++;
     }
     if(pos_DamParm2 !=0)  
     {
       arrayMPI[i]=_nldStatev[pos_DamParm2];
       i++;
     }
     if(pos_DamParm3 !=0)  
     {
       arrayMPI[i]=_nldStatev[pos_DamParm3];
       i++;
     }
     if(pos_INCDamParm1 !=0)  
     {
       arrayMPI[i]=_nldStatev[pos_INCDamParm1];
       i++;
     }
     if(pos_INCDamParm2 !=0)  
     {
       arrayMPI[i]=_nldStatev[pos_INCDamParm2];
       i++;
     }
     if(pos_INCDamParm3 !=0)  
     {
       arrayMPI[i]=_nldStatev[pos_INCDamParm3];
       i++;
     }
   }
};
void IPNonLocalDamage::getValuesFromMPI(const double *arrayMPI)
{
   //Msg::Error("Get %d values",Randnum);
   if(Randnum>0)
   {
     int i=0;
     if(pos_vfi !=0)
     {
       _nldStatev[pos_vfi]=arrayMPI[i];
       i++;
     }
     if(pos_aspR !=0)
     {
       _nldStatev[pos_aspR]=arrayMPI[i];
       i++;
     }
     if(pos_ME !=0) 
     {
       _nldStatev[pos_ME]=arrayMPI[i];
       i++;
     }
     if(pos_Mnu !=0) 
     {
       _nldStatev[pos_Mnu]=arrayMPI[i];
       i++;
     }
     if(pos_Msy0 !=0) 
     {
       _nldStatev[pos_Msy0]=arrayMPI[i];
       i++;
     }
     if(pos_Mhmod1 !=0) 
     {
       _nldStatev[pos_Mhmod1]=arrayMPI[i];
       i++;
     }
     if(pos_Mhmod2 !=0) 
     {
       _nldStatev[pos_Mhmod2]=arrayMPI[i];
       i++;
     }
     if(pos_Malpha_DP !=0) 
     {
       _nldStatev[pos_Malpha_DP]=arrayMPI[i];
       i++;
     }
     if(pos_Mm_DP !=0) 
     {
       _nldStatev[pos_Mm_DP]=arrayMPI[i];
       i++;
     }
     if(pos_Mnup !=0) 
     {
       _nldStatev[pos_Mnup]=arrayMPI[i];
       i++;
     }
     if(pos_Mhexp !=0)  
     {
       _nldStatev[pos_Mhexp]=arrayMPI[i];
       i++;
     }
     if(pos_euler !=0)  
     {
       _nldStatev[pos_euler]=arrayMPI[i];
       i++;
       _nldStatev[pos_euler+1]=arrayMPI[i];
       i++;
       _nldStatev[pos_euler+2]=arrayMPI[i];
       i++;
     }
     if(pos_DamParm1 !=0)  
     {
       _nldStatev[pos_DamParm1]=arrayMPI[i];
       i++;
     }
     if(pos_DamParm2 !=0)  
     {
       _nldStatev[pos_DamParm2]=arrayMPI[i];
       i++;
     }
     if(pos_DamParm3 !=0)  
     {
       _nldStatev[pos_DamParm3]=arrayMPI[i];
       i++;
     }
     if(pos_INCDamParm1 !=0)  
     {
       _nldStatev[pos_INCDamParm1]=arrayMPI[i];
       i++;
     }
     if(pos_INCDamParm2 !=0)  
     {
       _nldStatev[pos_INCDamParm2]=arrayMPI[i];
       i++;
     }
     if(pos_INCDamParm3 !=0)  
     {
       _nldStatev[pos_INCDamParm3]=arrayMPI[i];
       i++;
     }
   }

};
#endif // HAVE_MPI

void IPNonLocalDamage::setCriticalDamage(const double DT, int idex){
  if(getPosIncMaxD()>0) //use MFH
  {
    if(idex == 0)
       setMtxMaxD(DT);
    else if (idex == 1)
       setIncMaxD(DT);
    else
      Msg::Error("the non-local variable %d is not defined",idex);
  }
  else //no MFH
  {
    if(idex == 0)
       setMaxD(DT);
    else
      Msg::Error("the non-local variable %d is not defined",idex);
  }
} // set critical damage value
double IPNonLocalDamage::getCriticalDamage(int idex) const {
  if(getPosIncMaxD()>0) //use MFH
  {
    if(idex == 0)
       return getMtxMaxD();
    else if (idex == 1)
       return getIncMaxD();
    else
      Msg::Error("the non-local variable %d is not defined",idex);
    return 1.;
   }
   else //no MFH
   {
     if(idex == 0)
       return getMaxD();
     else
       Msg::Error("the non-local variable %d is not defined",idex);
     return 1.;

   }
};
double IPNonLocalDamage::getDamageIndicator(int idex) const 
{
  if(getPosIncMaxD()>0) //use MFH
  {
    if(idex == 0)
       return get(IPField::MTX_DAMAGE);
    else if (idex == 1)
       return get(IPField::INC_DAMAGE);
    else return 1.; 
  }
  else //no MFH
  {
    if(idex == 0)
      return get(IPField::DAMAGE);
    else return 1.; 
  }
};

void IPNonLocalDamage::setCriticalDamage(double *state, const double DT, int idex){
  if(getPosIncMaxD()>0) //use MFH
  {
    if(idex == 0)
       setMtxMaxD(state, DT);
    else if (idex == 1)
       setIncMaxD(state, DT);
    else
      Msg::Error("the non-local variable %d is not defined",idex);
  }
  else //no MFH
  {
    if(idex == 0)
       setMaxD(state, DT);
    else
      Msg::Error("the non-local variable %d is not defined",idex);
  }
} // set critical damage value
double IPNonLocalDamage::getCriticalDamage(double *state, int idex) const {
  if(getPosIncMaxD()>0) //use MFH
  {
    if(idex == 0)
       return getMtxMaxD(state);
    else if (idex == 1)
       return getIncMaxD(state);
    else
      Msg::Error("the non-local variable %d is not defined",idex);
    return 1.;
   }
   else //no MFH
   {
     if(idex == 0)
       return getMaxD(state);
     else
       Msg::Error("the non-local variable %d is not defined",idex);
     return 1.;
   }
};

