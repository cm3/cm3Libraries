//
// C++ Interface: ipNucleationFunction
//
// Description: Class for ipNucleationFunction
//
//
// Author:  <J. Leclerc>, (C) 2019
//
// Copyright: See COPYING file that comes with this distribution

#ifndef IPNUCLEATIONFUNCTION_H_
#define IPNUCLEATIONFUNCTION_H_
#ifndef SWIG
#include "ipvariable.h"
#endif




/*! \class IPNucleationFunction
 * This mother class creates the interface to store information
 * about the nucleation function law "NucleationFunctionLaw"
 */
class IPNucleationFunction
{
protected :
  // Nothing needs to be stored in the basis class and the trivial ones.
  
  //! @name Constructors and destructors
public :
  IPNucleationFunction();
  IPNucleationFunction( const IPNucleationFunction& source );
  virtual IPNucleationFunction& operator=( const IPNucleationFunction& source );
  virtual ~IPNucleationFunction(){};
  virtual void restart();
  virtual IPNucleationFunction* clone() const = 0;
  //! @}
  
  
  //! @name Access functions
  //! return the porosity value nucleated so far.
  virtual double  getNucleatedPorosity() const = 0;
  //! set the porosity value nucleated so far.
  virtual void    setNucleatedPorosity( const double& nucleatedfV ) = 0;
  
  //! return the increment of the porosity nucleated at this time step.
  virtual double  getNucleatedPorosityIncrement() const = 0;
  //! set the increment of the porosity nucleated at this time step.
  virtual void    setNucleatedPorosityIncrement( const double& delta_fvNucl ) = 0;
  
  //! return the porosity rate nucleated at this time step.
  virtual double  getNucleationRate() const = 0;
  //! set the porosity rate nucleated  at this time step.
  virtual void    setNucleationRate( const double& fV_rate ) = 0;
  
  //! return the strain value at wich half of the nucleation is nucleated (for Gaussian law).
  virtual double  getMeanNucleationStrain() const = 0;
  //! update the strain value at wich half of the nucleation is nucleated (for Gaussian law).
  virtual void    setMeanNucleationStrain( const double& meanNucleationStrain ) = 0;
  
  //! return the total value of nucleation porosity
  virtual double  getTotalNucleatedQuantity() const = 0;
  //! send back an error as nothing is stored.
  virtual void    setTotalNucleatedQuantity( const double& fvtot ) = 0;
  //! @}
  
  
};




/*! \class IPTrivialNucleationFunction
 * This class stores data corresponding to the "TrivialNucleationFunctionLaw".
 * Practically, nothing is stored.
 */
class IPTrivialNucleationFunction : public IPNucleationFunction
{
protected :
  // Nothing needs to be stored in the trivial class.
  
  //! @name Constructors and destructors
public :
  IPTrivialNucleationFunction();
  IPTrivialNucleationFunction( const IPTrivialNucleationFunction& source );
  virtual IPTrivialNucleationFunction& operator=( const IPNucleationFunction& source );
  virtual ~IPTrivialNucleationFunction(){};
  virtual void restart();
  virtual IPNucleationFunction* clone() const { return new IPTrivialNucleationFunction( *this ); };
  //! @}
  
  
  //! @name Access functions
  //! return always 0. as nothing is nucleated.
  inline virtual double  getNucleatedPorosity() const { return 0.0; };
  //! send back an error as nothing is stored.
  inline virtual void    setNucleatedPorosity( const double& nucleatedfV ) {
    Msg::Error( "IPTrivialNucleationFunction::setNucleatedPorosity should never be used." );
  };
  
  //! return always 0. as nothing is nucleated.
  inline virtual double  getNucleatedPorosityIncrement() const { return 0.0; };
  //! send back an error as nothing is stored.
  inline virtual void    setNucleatedPorosityIncrement( const double& delta_fvNucl ) {
    Msg::Error( "IPTrivialNucleationFunction::setNucleatedPorosityIncrement should never be used." );
  };
  
  //! return the porosity rate nucleated at this time step, which is always equal to 0.
  inline virtual double  getNucleationRate() const { return 0.0; };
  //! send back an error as nothing is stored.
  inline virtual void    setNucleationRate( const double& fV_rate ) {
    Msg::Error( "IPTrivialNucleationFunction::setNucleationRate should never be used." );
  };
  
  //! return the strain value at wich half of the nucleation is nucleated.
  inline virtual double  getMeanNucleationStrain() const { return 0.0; };
  //! send back an error as nothing is stored.
  inline virtual void    setMeanNucleationStrain( const double& meanNucleationStrain ) {
    Msg::Error( "IPTrivialNucleationFunction::setMeanNucleationStrain should never be used." );
  };
  
  //! return the total value of nucleation porosity
  inline virtual double  getTotalNucleatedQuantity() const { return 0.0; };
  //! send back an error as nothing is stored.
  inline virtual void    setTotalNucleatedQuantity( const double& fvtot ) {
    Msg::Error( "IPNonTrivialNucleationFunction::setMeanNucleationStrain should never be used." );
  };
  
  //! @}
  
};




/*! \class IPLinearNucleationFunction
 * IPV-class abstract for all nucleation functions which are non-trivial. Variables
 * common to all non-trivial IPV are defined here as well the interface.
 */
class IPNonTrivialNucleationFunction : public IPNucleationFunction
{
protected :
  //! porosity nucleated so far.
  double nucleatedfV_;
  
  //! increment of porosity nucleated at this time step.
  double deltafV_;
  
  //! \fixme temporary : should be not store here in the IPV.
  double fV_rate_;
  
  
  //! @name Constructors / destructors / restart
public :
  IPNonTrivialNucleationFunction();
  IPNonTrivialNucleationFunction( const IPNonTrivialNucleationFunction& source );
  virtual IPNonTrivialNucleationFunction& operator=( const IPNucleationFunction& source );
  virtual ~IPNonTrivialNucleationFunction(){};
  virtual void restart();
  virtual IPNucleationFunction* clone() const = 0;
  //! @}
  
  
  //! @name Access functions
  //! return the value of nucleated porosity so far.
  inline virtual double  getNucleatedPorosity() const { return nucleatedfV_; };
  //! update the value of the nucleated porosity.
  inline virtual void    setNucleatedPorosity( const double& nucleatedfV ) { nucleatedfV_ = nucleatedfV; };
  
  //! return the value of the increment.
  inline virtual double  getNucleatedPorosityIncrement() const { return deltafV_; };
  //! update the value of the porosity increment.
  inline virtual void    setNucleatedPorosityIncrement( const double& deltafV ) { deltafV_ = deltafV; };
  
  //! return the porosity rate nucleated at this time step.
  inline virtual double  getNucleationRate() const { return fV_rate_; };
  //! set the porosity rate nucleated  at this time step.
  inline virtual void    setNucleationRate( const double& fV_rate ) { fV_rate_ = fV_rate; };
  
  
  //! return the strain value at wich half of the nucleation is nucleated.
  inline virtual double  getMeanNucleationStrain() const { return 0.0; };
  //! send back an error as nothing is stored.
  inline virtual void    setMeanNucleationStrain( const double& meanNucleationStrain ) {
    Msg::Error( "IPNonTrivialNucleationFunction::setMeanNucleationStrain should never be used." );
  };
  
 
  //! return the total value of nucleation porosity
  inline virtual double  getTotalNucleatedQuantity() const { return 0.0; };
  //! send back an error as nothing is stored.
  inline virtual void    setTotalNucleatedQuantity( const double& fvtot ) {
    Msg::Error( "IPNonTrivialNucleationFunction::setMeanNucleationStrain should never be used." );
  };
  //! @}
};




/*! \class IPLinearNucleationFunction
 * This class stores data corresponding to the "LinearNucleationFunctionLaw".
 * Practically, nothing more than for IPNonTrivialNucleationFunction is needed.
 */
 class IPLinearNucleationFunction : public IPNonTrivialNucleationFunction
{
protected :
  // Nothing more is needed.
  
  //! @name Constructors / destructors / restart
public :
  IPLinearNucleationFunction();
  IPLinearNucleationFunction( const IPLinearNucleationFunction& source );
  virtual IPLinearNucleationFunction& operator=( const IPNucleationFunction& source );
  virtual ~IPLinearNucleationFunction(){};
  virtual void restart();
  virtual IPNucleationFunction* clone() const { return new IPLinearNucleationFunction(*this); };
  //! @}
};





/*! \class IPGaussianNucleationFunction
 * This class stores data corresponding to the "GaussianNucleationFunctionLaw".
 * The position of the mean nucleation strain is added to the storage.
 */
class IPGaussianNucleationFunction : public IPNonTrivialNucleationFunction
{
protected :

  //! Strain value at wich half of the nucleation is nucleated 
  //! (i.e. the maximum position of the Gaussian law).
  double meanNucleationStrain_;
  
  double fvtot_;
  
  //! @name Constructors / destructors / restart
private :
  IPGaussianNucleationFunction();
public :
  IPGaussianNucleationFunction( const double meanNucleationStrain, const double fvtot );
  IPGaussianNucleationFunction( const IPGaussianNucleationFunction& source );
  virtual IPGaussianNucleationFunction& operator=( const IPNucleationFunction& source );
  virtual ~IPGaussianNucleationFunction(){};
  virtual void restart();
  virtual IPNucleationFunction* clone() const { return new IPGaussianNucleationFunction(*this); };
  //! @}
  
  
  //! @name Access functions
  //! return the strain value at wich half of the nucleation is nucleated.
  inline virtual double  getMeanNucleationStrain() const { return meanNucleationStrain_; };
  //! update the strain value at wich half of the nucleation is nucleated.
  inline virtual void    setMeanNucleationStrain( const double& meanNucleationStrain ) { meanNucleationStrain_ = meanNucleationStrain; };
  
  //! return the total value of nucleation porosity
  inline virtual double  getTotalNucleatedQuantity() const { return fvtot_; };
  //! update the total value of nucleation porosity 
  inline virtual void    setTotalNucleatedQuantity( const double& fvtot ) { fvtot_ = fvtot; };
  //! @}

};


#endif // IPNUCLEATIONFUNCTION_H_