//
// Description: storing class for J2 thermomechanical law
// Author:  <L. Noels>, (C) 2014
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef IPJ2THERMOMECHANICS_H_
#define IPJ2THERMOMECHANICS_H_
#include "ipJ2linear.h"
#include "STensor3.h"

class IPJ2ThermoMechanics : public IPJ2linear
{
 public:
  double _thermalEnergy; //

 public:
  IPJ2ThermoMechanics();
  IPJ2ThermoMechanics(const J2IsotropicHardening *j2IH);
  IPJ2ThermoMechanics(const IPJ2ThermoMechanics &source);
  virtual IPJ2ThermoMechanics& operator=(const IPVariable &source);
  virtual ~IPJ2ThermoMechanics(){}
  virtual double defoEnergy() const;
  virtual double plasticEnergy() const;
  virtual double getThermalEnergy() const {return _thermalEnergy;};

  virtual IPVariable* clone() const {return new IPJ2ThermoMechanics(*this);};
  virtual void restart();
};

#endif // IPJ2THERMOMECHANICS_H_
