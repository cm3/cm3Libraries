//
// C++ Interface: terms
//
// Description: Class to store internal variables at gauss point
//
//
// Author:  <Gauthier BECKER>, (C) 2010
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef IPSTATE_H_
#define IPSTATE_H_
#include "GModel.h"
#include "ipvariable.h"

// enum to access to component of stress and deformation GLOBAL ??
struct component{
 enum enumcomp{xx,yy,zz,xy,yz,xz};
};
class partDomain;
class dgPartDomain;
class materialLaw;
class DeepMaterialNetwork;

class IPStateBase{
	protected:
		nonLinearMechSolver* _solver;
	public:
		IPStateBase():_solver(NULL){}
		IPStateBase(const IPStateBase &source):_solver(source._solver){}
		virtual IPStateBase& operator=(const IPStateBase &source){
			_solver = source._solver;
			return *this;
		}
		virtual ~IPStateBase();

		enum whichState{initial, previous, current, temp, activeDissipation};
		virtual IPVariable* getState(const whichState wst=IPStateBase::current) const=0;
    virtual void copy(const IPStateBase::whichState ws1, const IPStateBase::whichState ws2);
		virtual void restart()=0;
		virtual void getAllIPVariable(std::vector<IPVariable*>& allIP) = 0;

		virtual void setLocation(const IPVariable::LOCATION loc){
			std::vector<IPVariable*> allIP;
			getAllIPVariable(allIP);
			for (int i=0; i< allIP.size(); i++){
				allIP[i]->setLocation(loc);
			}
		}

		virtual void setMicroSolver(nonLinearMechSolver* s){
			_solver = s;
			std::vector<IPVariable*> allIP;
			getAllIPVariable(allIP);
			for (int i=0; i< allIP.size(); i++){
				allIP[i]->setMicroSolver(s);
			}
		};
		virtual nonLinearMechSolver* getMicroSolver() {return _solver;};
		virtual const nonLinearMechSolver* getMicroSolver() const {return _solver;};
};

class IP1State : public IPStateBase{
 protected :
  IPVariable *_current;
 public :
  IP1State() : IPStateBase(), _current(NULL){}
  IP1State(IPVariable *cur):IPStateBase(), _current(cur){}
  virtual ~IP1State();
  IP1State(const IP1State &source);
  virtual IP1State & operator = (const IPStateBase &source);
  virtual IPVariable* getState(const whichState wst=IPStateBase::current) const;
  virtual void restart();
  virtual void getAllIPVariable(std::vector<IPVariable*>& allIP) {
    allIP.clear();
    allIP.push_back(_current);
  };
};

class IP3State : public IPStateBase{
 protected :
  mutable IPVariable *_temp; // temporary state
	mutable IPVariable *_activeDissipation; // dissipative active
  IPVariable *_initial;     // initial state t=0
  IPVariable *_step1;    // previous step if _st = true and current step otherwise
  IPVariable *_step2;     // current step if _st = true and previous step otherwise
  const bool *_st; // pointer on a bool value to choice what vector is current and what vector is previous
 public :
  IP3State(bool *st) : IPStateBase(),_st(st), _initial(NULL), _step1(NULL), _step2(NULL),_temp(NULL),_activeDissipation(NULL){}
  IP3State(const bool *st,IPVariable *init,
           IPVariable *st1, IPVariable *st2): _st(st), _initial(init), _step1(st1), _step2(st2),_temp(NULL),_activeDissipation(NULL){}
  virtual ~IP3State();
  IP3State(const IP3State &source);
  virtual IP3State & operator = (const IPStateBase &source);
  virtual IPVariable* getState(const whichState wst=IPStateBase::current) const;
  virtual void restart();
  virtual void getAllIPVariable(std::vector<IPVariable*>& allIP) {
    allIP.clear();
    allIP.push_back(_initial);
    allIP.push_back(_step1);
    allIP.push_back(_step2);
  };
};

// Class to access to the IPState of all gauss point
class AllIPState{
 public:
  typedef std::vector<IPStateBase*> ipstateElementContainer;
  typedef std::map<long int, ipstateElementContainer > ipstateContainer;
  typedef std::pair<long int, ipstateElementContainer > ipstatePairType;
 protected:
	std::vector<partDomain*>& _domainVector;
  ipstateContainer _mapall;
  bool isMultiscale;
  bool state; // flag to switch previous and current (change the pointer in place of copy all variables)
 public :
  AllIPState(std::vector<partDomain*> &vdom);
  ~AllIPState();
  ipstateElementContainer* getIPstate(const long int num);
  ipstateElementContainer* operator[](const long int num);

  const ipstateElementContainer* getIPstate(const long int num) const;
  const ipstateElementContainer* operator[] (const long int num) const;

	const std::vector<partDomain*>& getDomainVector() const {return _domainVector;};

  void nextStep();
  void copy(const IPStateBase::whichState ws1, const IPStateBase::whichState ws2);
  virtual void restart();

  void setTime(const double time, const double dtime, const int numstep);

  ipstateContainer* getAIPSContainer() {return &_mapall;};
	const ipstateContainer* getAIPSContainer() const {return &_mapall;};

	ipstateContainer::const_iterator begin() const {return _mapall.begin();};
	ipstateContainer::const_iterator end() const {return _mapall.end();};
};
#endif // IPSTATE_H_
