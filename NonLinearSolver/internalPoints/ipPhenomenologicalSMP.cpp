// Description: storing class for Phenomenological SMP
// Author:  <H. Gulasik, M. Pareja>, (C) 2019
// Copyright: See COPYING file that comes with this distribution

#include "ipPhenomenologicalSMP.h"
#include "restartManager.h"
#include "STensorOperations.h"
#include "ipField.h"

IPPhenomenologicalSMP::IPPhenomenologicalSMP(const double t0, const double Tc0, const double Tm0, const double wc0, const double wm0,
                          const J2IsotropicHardening* compG, const J2IsotropicHardening* tracG, const kinematicHardening* kinG, const int NG,
                          const J2IsotropicHardening* compR, const J2IsotropicHardening* tracR, const kinematicHardening* kinR, const int NR,
                          const J2IsotropicHardening* compAM, const J2IsotropicHardening* tracAM, const kinematicHardening* kinAM, const int NAM)
                      :IPCoupledThermoMechanics(), Tdot(0.), Tmin(t0), _SMPEnergy(0.)
{
    if (t0 > Tm0){
        T_t =Tc0;
        wt  =wc0;
        zs=1.;
        TRefR=t0;
        TRefG=Tm0;
    } else{
        T_t =Tm0;
        wt  =wm0;
        zs=0.;
        TRefR=Tm0;
        TRefG=t0;
    }
    TRefAM=t0;
    //zg  =1./(1.+exp(2.*100.*wt*(t0-T_t)));
    zg= 1.-1./2*(1. + std::erf((t0-T_t)/wt/std::sqrt(2.)));
    PDF=0.;

    STensorOperation::unity(FthI);
//    STensorOperation::unity(FthAI);
    STensorOperation::unity(FthAD);

    STensorOperation::unity(FthG);
    STensorOperation::unity(FfG);

    STensorOperation::unity(FthR);
    STensorOperation::unity(FfR);
   
    qG=new IPHyperViscoElastoPlastic(compG, tracG, kinG, NG);
    qR=new IPHyperViscoElastoPlastic(compR, tracR, kinR, NR);

    Tcrys   = Tc0;
    Tmelt   = Tm0;
    Wcrys   = wc0;
    Wmelt   = wm0;
    alphaig =0.;
#if 0
    STensorOperation::zero(AGl);
    STensorOperation::zero(ARl);
    STensorOperation::zero(AAMl);
#endif

    STensorOperation::zero(ADl);

    STensorOperation::zero(EnThG);
    STensorOperation::zero(EnThR);
    STensorOperation::zero(EnThAM);


    STensorOperation::unity(FthAM);
    STensorOperation::unity(FfAM);
    qAM=new IPHyperViscoElastoPlastic(compAM, tracAM, kinAM, NAM);

    //energy
    appliedEnergy =0.;
    freezedDiss   =0.;
    plasticDiss   =0.;
    viscousDiss   =0.;
    EDiss         =0.;
    STensorOperation::zero(PG);
    STensorOperation::zero(PR);
    STensorOperation::zero(PAM);
    STensorOperation::zero(PAML2);
    STensorOperation::zero(PRL2);


};



IPPhenomenologicalSMP::IPPhenomenologicalSMP(const IPPhenomenologicalSMP &source)
                      :IPCoupledThermoMechanics(source) //: IPLinearThermoMechanics(source)
{
    FthI    = source.FthI;
//    FthAI   = source.FthAI;
    FthAD   = source.FthAD;

    FthG    = source.FthG;
    FfG     = source.FfG;

    FthR    = source.FthR;
    FfR     = source.FfR;

    zg      = source.zg;
    PDF     = source.PDF;
    Tdot    = source.Tdot;
    T_t     = source.T_t;
    wt      = source.wt;

    qG = NULL;
    if(source.qG != NULL)
      qG = dynamic_cast<IPHyperViscoElastoPlastic*>(source.qG->clone());

    qR = NULL;
    if(source.qR != NULL)
      qR = dynamic_cast<IPHyperViscoElastoPlastic*>(source.qR->clone());

    zs      = source.zs;
    TRefR   = source.TRefR;    
    TRefG   = source.TRefG;    
    TRefAM   = source.TRefAM;    
    Tcrys   = source.Tcrys;
    Tmelt   = source.Tmelt;
    Wcrys   = source.Wcrys;
    Wmelt   = source.Wmelt;
    alphaig = source.alphaig;
    Tmin    = source.Tmin;
#if 0
    AGl     = source.AGl;
    ARl     = source.ARl;
    AAMl    = source.AAMl;
#endif
    ADl     = source.ADl;
    
    EnThG   = source.EnThG;
    EnThR   = source.EnThR;
    EnThAM  = source.EnThAM;


    FthAM   = source.FthAM;
    FfAM    = source.FfAM;

    qAM = NULL;
    if(source.qAM != NULL)
      qAM = dynamic_cast<IPHyperViscoElastoPlastic*>(source.qAM->clone());

    //energy
    appliedEnergy   =source.appliedEnergy;
    _SMPEnergy   =source._SMPEnergy;
    freezedDiss     =source.freezedDiss;
    plasticDiss     =source.plasticDiss;
    viscousDiss     =source.viscousDiss;
    EDiss           =source.EDiss;
    PG      =source.PG;
    PR      =source.PR;
    PAM     =source.PAM;
    PAML2   =source.PAML2;
    PRL2    =source.PRL2;

}



IPPhenomenologicalSMP& IPPhenomenologicalSMP::operator=(const IPVariable &source)
{
    IPCoupledThermoMechanics::operator=(source);

    const IPPhenomenologicalSMP* src = dynamic_cast<const IPPhenomenologicalSMP*>(&source);
    if(src)
    {
        FthI    = src->FthI;
//        FthAI   = src->FthAI;
        FthAD   = src->FthAD;

        FthG    = src->FthG;
        FfG     = src->FfG;

        FthR    = src->FthR;
        FfR     = src->FfR;

        zg      = src->zg;
        PDF     = src->PDF;
        Tdot    = src->Tdot;
        T_t     = src->T_t;
        wt      = src->wt;

        zs      = src->zs;
        TRefR   = src->TRefR;
        TRefG   = src->TRefG;
        TRefAM   = src->TRefAM;
        Tcrys   = src->Tcrys;
        Tmelt   = src->Tmelt;
        Wcrys   = src->Wcrys;
        Wmelt   = src->Wmelt;
        alphaig = src->alphaig;
        Tmin    = src->Tmin;
#if 0
        AGl     = src->AGl;
        ARl     = src->ARl;
        AAMl    = src->AAMl;
#endif
        ADl     = src->ADl;

        EnThG   = src->EnThG;
        EnThR   = src->EnThR;
        EnThAM  = src->EnThAM;

        FthAM   = src->FthAM;
        FfAM    = src->FfAM;

        PG      =src->PG;
        PR      =src->PR;
        PAM     =src->PAM;
        PAML2   =src->PAML2;
        PRL2    =src->PRL2;

        //energy
        appliedEnergy   =src->appliedEnergy;
        _SMPEnergy   =src->_SMPEnergy;
        freezedDiss     =src->freezedDiss;
        plasticDiss     =src->plasticDiss;
        viscousDiss     =src->viscousDiss;
        EDiss           =src->EDiss;

        if(src->qG != NULL)
        {
          if(qG!=NULL)
          { 
            qG->operator=((dynamic_cast<const IPVariable&>(*(src->qG))));
          }
          else
            qG=dynamic_cast<IPHyperViscoElastoPlastic*>(src->qG->clone());
        }
        else
        {
           if(qG!=NULL) delete qG;
           qG=NULL;
        }
        if(src->qR != NULL)
        {
          if(qR!=NULL)
            qR->operator=((dynamic_cast<const IPVariable&>(*(src->qR))));
          else
            qR=dynamic_cast<IPHyperViscoElastoPlastic*>(src->qR->clone());
        }
        else
        {
           if(qR!=NULL) delete qR;
           qR=NULL;
        }
        if(src->qAM != NULL)
        {
          if(qAM!=NULL)
            qAM->operator=((dynamic_cast<const IPVariable&>(*(src->qAM))));
          else
            qAM=dynamic_cast<IPHyperViscoElastoPlastic*>(src->qAM->clone());
        }
        else
        {
           if(qAM!=NULL) delete qAM;
           qAM=NULL;
        }
    }
    return *this;
}




void IPPhenomenologicalSMP::restart()
{
    IPCoupledThermoMechanics::restart();
    if(qG!=NULL)
      restartManager::restart(qG);
    if(qR!=NULL)
      restartManager::restart(qR);
    if(qAM!=NULL)
      restartManager::restart(qAM);
    
    restartManager::restart(FthI);
//    restartManager::restart(FthAI);
    restartManager::restart(FthAD);

    restartManager::restart(FthG);
    restartManager::restart(FfG);

    restartManager::restart(FthR);
    restartManager::restart(FfR);

    restartManager::restart(zg);
    restartManager::restart(PDF);
    restartManager::restart(Tdot);
    restartManager::restart(T_t);
    restartManager::restart(wt);

    restartManager::restart(zs);
    restartManager::restart(TRefR);
    restartManager::restart(TRefG);
    restartManager::restart(TRefAM);
    restartManager::restart(Tcrys);
    restartManager::restart(Tmelt);
    restartManager::restart(Wcrys);
    restartManager::restart(Wmelt);
    restartManager::restart(alphaig);
    restartManager::restart(Tmin);
#if 0
    restartManager::restart(AGl);
    restartManager::restart(ARl);
    restartManager::restart(AAMl);
#endif
    restartManager::restart(ADl);

    restartManager::restart(EnThG);
    restartManager::restart(EnThR); 
    restartManager::restart(EnThAM);

    restartManager::restart(FthAM);
    restartManager::restart(FfAM);

    restartManager::restart(PG);
    restartManager::restart(PR);
    restartManager::restart(PAM);
    restartManager::restart(PAML2);
    restartManager::restart(PRL2);

    //energy
    restartManager::restart(appliedEnergy);
    restartManager::restart(_SMPEnergy);
    restartManager::restart(freezedDiss);
    restartManager::restart(plasticDiss);
    restartManager::restart(viscousDiss);
    restartManager::restart(EDiss);


    return;
}

double IPPhenomenologicalSMP::get(const int i) const
{
    if(i==IPField::PLASTICSTRAIN)   {return getConstRefToEquivalentPlasticDefoG();}

    else if(i==IPField::FthG_XX)     {return getConstRefToFthG()(0,0);}
    else if(i==IPField::FthG_YY)     {return getConstRefToFthG()(1,1);}
    else if(i==IPField::FthG_ZZ)     {return getConstRefToFthG()(2,2);}
    else if(i==IPField::FthG_XY)     {return getConstRefToFthG()(0,1);}
    else if(i==IPField::FthG_YZ)     {return getConstRefToFthG()(1,2);}
    else if(i==IPField::FthG_ZX)     {return getConstRefToFthG()(0,2);}

    else if(i==IPField::FthR_XX)     {return getConstRefToFthR()(0,0);}
    else if(i==IPField::FthR_YY)     {return getConstRefToFthR()(1,1);}
    else if(i==IPField::FthR_ZZ)     {return getConstRefToFthR()(2,2);}
    else if(i==IPField::FthR_XY)     {return getConstRefToFthR()(0,1);}
    else if(i==IPField::FthR_YZ)     {return getConstRefToFthR()(1,2);}
    else if(i==IPField::FthR_ZX)     {return getConstRefToFthR()(0,2);}

    else if(i==IPField::FthI_XX)     {return getConstRefToFthI()(0,0);}
    else if(i==IPField::FthI_YY)     {return getConstRefToFthI()(1,1);}
    else if(i==IPField::FthI_ZZ)     {return getConstRefToFthI()(2,2);}
    else if(i==IPField::FthI_XY)     {return getConstRefToFthI()(0,1);}
    else if(i==IPField::FthI_YZ)     {return getConstRefToFthI()(1,2);}
    else if(i==IPField::FthI_ZX)     {return getConstRefToFthI()(0,2);}
#if 0
        else if(i==IPField::FthAI_XX)     {return getConstRefToFthAI()(0,0);}
    else if(i==IPField::FthAI_YY)     {return getConstRefToFthAI()(1,1);}
    else if(i==IPField::FthAI_ZZ)     {return getConstRefToFthAI()(2,2);}
    else if(i==IPField::FthAI_XY)     {return getConstRefToFthAI()(0,1);}
    else if(i==IPField::FthAI_YZ)     {return getConstRefToFthAI()(1,2);}
    else if(i==IPField::FthAI_ZX)     {return getConstRefToFthAI()(0,2);}
#endif
    else if(i==IPField::FthAD_XX)     {return getConstRefToFthAD()(0,0);}
    else if(i==IPField::FthAD_YY)     {return getConstRefToFthAD()(1,1);}
    else if(i==IPField::FthAD_ZZ)     {return getConstRefToFthAD()(2,2);}
    else if(i==IPField::FthAD_XY)     {return getConstRefToFthAD()(0,1);}
    else if(i==IPField::FthAD_YZ)     {return getConstRefToFthAD()(1,2);}
    else if(i==IPField::FthAD_ZX)     {return getConstRefToFthAD()(0,2);}

    else if(i==IPField::FfG_XX)      {return getConstRefToFfG()(0,0);}
    else if(i==IPField::FfG_YY)      {return getConstRefToFfG()(1,1);}
    else if(i==IPField::FfG_ZZ)      {return getConstRefToFfG()(2,2);}
    else if(i==IPField::FfG_XY)      {return getConstRefToFfG()(0,1);}
    else if(i==IPField::FfG_YZ)      {return getConstRefToFfG()(1,2);}
    else if(i==IPField::FfG_ZX)      {return getConstRefToFfG()(0,2);}

    else if(i==IPField::FpG_XX)     {return getConstRefToFpG()(0,0);}
    else if(i==IPField::FpG_YY)     {return getConstRefToFpG()(1,1);}
    else if(i==IPField::FpG_ZZ)     {return getConstRefToFpG()(2,2);}
    else if(i==IPField::FpG_XY)     {return getConstRefToFpG()(0,1);}
    else if(i==IPField::FpG_YZ)     {return getConstRefToFpG()(1,2);}
    else if(i==IPField::FpG_ZX)     {return getConstRefToFpG()(0,2);}

    else if(i==IPField::FveG_XX)      {return getConstRefToFveG()(0,0);}
    else if(i==IPField::FveG_YY)      {return getConstRefToFveG()(1,1);}
    else if(i==IPField::FveG_ZZ)      {return getConstRefToFveG()(2,2);}
    else if(i==IPField::FveG_XY)      {return getConstRefToFveG()(0,1);}
    else if(i==IPField::FveG_YZ)      {return getConstRefToFveG()(1,2);}
    else if(i==IPField::FveG_ZX)      {return getConstRefToFveG()(0,2);}

    else if(i==IPField::FP_XX)      {return getConstRefToFfR()(0,0);}
    else if(i==IPField::FP_YY)      {return getConstRefToFfR()(1,1);}
    else if(i==IPField::FP_ZZ)      {return getConstRefToFfR()(2,2);}
    else if(i==IPField::FP_XZ)      {return getConstRefToFfR()(0,1);}
    else if(i==IPField::FP_YZ)      {return getConstRefToFfR()(1,2);}
    else if(i==IPField::FP_ZX)      {return getConstRefToFfR()(0,2);}

    else if(i==IPField::FpR_XX)     {return getConstRefToFpR()(0,0);}
    else if(i==IPField::FpR_YY)     {return getConstRefToFpR()(1,1);}
    else if(i==IPField::FpR_ZZ)     {return getConstRefToFpR()(2,2);}
    else if(i==IPField::FpR_XY)     {return getConstRefToFpR()(0,1);}
    else if(i==IPField::FpR_YZ)     {return getConstRefToFpR()(1,2);}
    else if(i==IPField::FpR_ZX)     {return getConstRefToFpR()(0,2);}

    else if(i==IPField::FveR_XX)      {return getConstRefToFveR()(0,0);}
    else if(i==IPField::FveR_YY)      {return getConstRefToFveR()(1,1);}
    else if(i==IPField::FveR_ZZ)      {return getConstRefToFveR()(2,2);}
    else if(i==IPField::FveR_XY)      {return getConstRefToFveR()(0,1);}
    else if(i==IPField::FveR_YZ)      {return getConstRefToFveR()(1,2);}
    else if(i==IPField::FveR_ZX)      {return getConstRefToFveR()(0,2);}

    else if(i==IPField::ZG)         {return getzg();}
    else if(i==IPField::PDF)        {return getConstRefToPDF();}
    else if(i==IPField::TT)         {return getConstRefToTt();}
    else if(i==IPField::WT)         {return getConstRefToWt();}

    else if(i==IPField::FthAM_XX)     {return getConstRefToFthAM()(0,0);}
    else if(i==IPField::FthAM_YY)     {return getConstRefToFthAM()(1,1);}
    else if(i==IPField::FthAM_ZZ)     {return getConstRefToFthAM()(2,2);}
    else if(i==IPField::FthAM_XY)     {return getConstRefToFthAM()(0,1);}
    else if(i==IPField::FthAM_YZ)     {return getConstRefToFthAM()(1,2);}
    else if(i==IPField::FthAM_ZX)     {return getConstRefToFthAM()(0,2);}

    else if(i==IPField::FfAM_XX)      {return getConstRefToFfAM()(0,0);}
    else if(i==IPField::FfAM_YY)      {return getConstRefToFfAM()(1,1);}
    else if(i==IPField::FfAM_ZZ)      {return getConstRefToFfAM()(2,2);}
    else if(i==IPField::FfAM_XY)      {return getConstRefToFfAM()(0,1);}
    else if(i==IPField::FfAM_YZ)      {return getConstRefToFfAM()(1,2);}
    else if(i==IPField::FfAM_ZX)      {return getConstRefToFfAM()(0,2);}

    else if(i==IPField::FpAM_XX)      {return getConstRefToFpAM()(0,0);}
    else if(i==IPField::FpAM_YY)      {return getConstRefToFpAM()(1,1);}
    else if(i==IPField::FpAM_ZZ)      {return getConstRefToFpAM()(2,2);}
    else if(i==IPField::FpAM_XY)      {return getConstRefToFpAM()(0,1);}
    else if(i==IPField::FpAM_YZ)      {return getConstRefToFpAM()(1,2);}
    else if(i==IPField::FpAM_ZX)      {return getConstRefToFpAM()(0,2);}

    else if(i==IPField::FveAM_XX)      {return getConstRefToFveAM()(0,0);}
    else if(i==IPField::FveAM_YY)      {return getConstRefToFveAM()(1,1);}
    else if(i==IPField::FveAM_ZZ)      {return getConstRefToFveAM()(2,2);}
    else if(i==IPField::FveAM_XY)      {return getConstRefToFveAM()(0,1);}
    else if(i==IPField::FveAM_YZ)      {return getConstRefToFveAM()(1,2);}
    else if(i==IPField::FveAM_ZX)      {return getConstRefToFveAM()(0,2);}

    else if(i==IPField::epsAM)        {return getConstRefToEquivalentPlasticDefoAM();}
    else if(i==IPField::epsR)         {return getConstRefToEquivalentPlasticDefoR();}
    else if(i==IPField::epsG)         {return getConstRefToEquivalentPlasticDefoG();}

    else if(i==IPField::Tcrys)        {return getConstRefToTcrys();}
    else if(i==IPField::Tmelt)        {return getConstRefToTmelt();}
    else if(i==IPField::Wcrys)        {return getConstRefToWcrys();}
    else if(i==IPField::Wmelt)        {return getConstRefToWmelt();}

    else if(i==IPField::TRefG)        {return getConstRefToTRefG();}
    else if(i==IPField::TRefR)        {return getConstRefToTRefR();}
    else if(i==IPField::TRefAM)        {return getConstRefToTRefAM();}
#if 0
        else if(i==IPField::AGl_XX)      {return getConstRefToAGl()(0,0);}
    else if(i==IPField::AGl_YY)      {return getConstRefToAGl()(1,1);}
    else if(i==IPField::AGl_ZZ)      {return getConstRefToAGl()(2,2);}
    else if(i==IPField::AGl_XY)      {return getConstRefToAGl()(0,1);}
    else if(i==IPField::AGl_YZ)      {return getConstRefToAGl()(1,2);}
    else if(i==IPField::AGl_ZX)      {return getConstRefToAGl()(0,2);}

    else if(i==IPField::ARl_XX)      {return getConstRefToARl()(0,0);}
    else if(i==IPField::ARl_YY)      {return getConstRefToARl()(1,1);}
    else if(i==IPField::ARl_ZZ)      {return getConstRefToARl()(2,2);}
    else if(i==IPField::ARl_XY)      {return getConstRefToARl()(0,1);}
    else if(i==IPField::ARl_YZ)      {return getConstRefToARl()(1,2);}
    else if(i==IPField::ARl_ZX)      {return getConstRefToARl()(0,2);}

    else if(i==IPField::AAMl_XX)      {return getConstRefToAAMl()(0,0);}
    else if(i==IPField::AAMl_YY)      {return getConstRefToAAMl()(1,1);}
    else if(i==IPField::AAMl_ZZ)      {return getConstRefToAAMl()(2,2);}
    else if(i==IPField::AAMl_XY)      {return getConstRefToAAMl()(0,1);}
    else if(i==IPField::AAMl_YZ)      {return getConstRefToAAMl()(1,2);}
    else if(i==IPField::AAMl_ZX)      {return getConstRefToAAMl()(0,2);}
#endif
    else if(i==IPField::ADl_XX)      {return getConstRefToADl()(0,0);}
    else if(i==IPField::ADl_YY)      {return getConstRefToADl()(1,1);}
    else if(i==IPField::ADl_ZZ)      {return getConstRefToADl()(2,2);}
    else if(i==IPField::ADl_XY)      {return getConstRefToADl()(0,1);}
    else if(i==IPField::ADl_YZ)      {return getConstRefToADl()(1,2);}
    else if(i==IPField::ADl_ZX)      {return getConstRefToADl()(0,2);}

    else if(i==IPField::EnThG_XX)      {return getConstRefToEnThG()(0,0);}
    else if(i==IPField::EnThG_YY)      {return getConstRefToEnThG()(1,1);}
    else if(i==IPField::EnThG_ZZ)      {return getConstRefToEnThG()(2,2);}
    else if(i==IPField::EnThG_XY)      {return getConstRefToEnThG()(0,1);}
    else if(i==IPField::EnThG_YZ)      {return getConstRefToEnThG()(1,2);}
    else if(i==IPField::EnThG_ZX)      {return getConstRefToEnThG()(0,2);}

    else if(i==IPField::EnThR_XX)      {return getConstRefToEnThR()(0,0);}
    else if(i==IPField::EnThR_YY)      {return getConstRefToEnThR()(1,1);}
    else if(i==IPField::EnThR_ZZ)      {return getConstRefToEnThR()(2,2);}
    else if(i==IPField::EnThR_XY)      {return getConstRefToEnThR()(0,1);}
    else if(i==IPField::EnThR_YZ)      {return getConstRefToEnThR()(1,2);}
    else if(i==IPField::EnThR_ZX)      {return getConstRefToEnThR()(0,2);}

    else if(i==IPField::EnThAM_XX)      {return getConstRefToEnThAM()(0,0);}
    else if(i==IPField::EnThAM_YY)      {return getConstRefToEnThAM()(1,1);}
    else if(i==IPField::EnThAM_ZZ)      {return getConstRefToEnThAM()(2,2);}
    else if(i==IPField::EnThAM_XY)      {return getConstRefToEnThAM()(0,1);}
    else if(i==IPField::EnThAM_YZ)      {return getConstRefToEnThAM()(1,2);}
    else if(i==IPField::EnThAM_ZX)      {return getConstRefToEnThAM()(0,2);}

    else if(i==IPField::appliedEnergy)  {return getConstRefToAppliedEnergy();}
    else if(i==IPField::_SMPEnergy)     {return defoEnergy();}
    else if(i==IPField::_thermalEnergy) {return getThermalEnergy();}
    else if(i==IPField::plasticDiss)    {return getConstRefToPlasticDiss();}
    else if(i==IPField::freezedDiss)    {return getConstRefToFreezedDiss();}
    else if(i==IPField::viscousDiss)    {return getConstRefToViscousDiss();}
    else if(i==IPField::EDiss)          {return getConstRefToEDiss();}

    else
    {
        return 0.;
    }
}

double & IPPhenomenologicalSMP::getRef(const int i)
{
    if(i==IPField::PLASTICSTRAIN)   {return getRefToEquivalentPlasticDefoG();}

    else if(i==IPField::FthG_XX)     {return getRefToFthG()(0,0);}
    else if(i==IPField::FthG_YY)     {return getRefToFthG()(1,1);}
    else if(i==IPField::FthG_ZZ)     {return getRefToFthG()(2,2);}
    else if(i==IPField::FthG_XY)     {return getRefToFthG()(0,1);}
    else if(i==IPField::FthG_YZ)     {return getRefToFthG()(1,2);}
    else if(i==IPField::FthG_ZX)     {return getRefToFthG()(0,2);}

    else if(i==IPField::FthR_XX)     {return getRefToFthR()(0,0);}
    else if(i==IPField::FthR_YY)     {return getRefToFthR()(1,1);}
    else if(i==IPField::FthR_ZZ)     {return getRefToFthR()(2,2);}
    else if(i==IPField::FthR_XY)     {return getRefToFthR()(0,1);}
    else if(i==IPField::FthR_YZ)     {return getRefToFthR()(1,2);}
    else if(i==IPField::FthR_ZX)     {return getRefToFthR()(0,2);}

    else if(i==IPField::FthI_XX)     {return getRefToFthI()(0,0);}
    else if(i==IPField::FthI_YY)     {return getRefToFthI()(1,1);}
    else if(i==IPField::FthI_ZZ)     {return getRefToFthI()(2,2);}
    else if(i==IPField::FthI_XY)     {return getRefToFthI()(0,1);}
    else if(i==IPField::FthI_YZ)     {return getRefToFthI()(1,2);}
    else if(i==IPField::FthI_ZX)     {return getRefToFthI()(0,2);}
#if 0
        else if(i==IPField::FthAI_XX)     {return getRefToFthAI()(0,0);}
    else if(i==IPField::FthAI_YY)     {return getRefToFthAI()(1,1);}
    else if(i==IPField::FthAI_ZZ)     {return getRefToFthAI()(2,2);}
    else if(i==IPField::FthAI_XY)     {return getRefToFthAI()(0,1);}
    else if(i==IPField::FthAI_YZ)     {return getRefToFthAI()(1,2);}
    else if(i==IPField::FthAI_ZX)     {return getRefToFthAI()(0,2);}
#endif
    else if(i==IPField::FthAD_XX)     {return getRefToFthAD()(0,0);}
    else if(i==IPField::FthAD_YY)     {return getRefToFthAD()(1,1);}
    else if(i==IPField::FthAD_ZZ)     {return getRefToFthAD()(2,2);}
    else if(i==IPField::FthAD_XY)     {return getRefToFthAD()(0,1);}
    else if(i==IPField::FthAD_YZ)     {return getRefToFthAD()(1,2);}
    else if(i==IPField::FthAD_ZX)     {return getRefToFthAD()(0,2);}

    else if(i==IPField::FfG_XX)      {return getRefToFfG()(0,0);}
    else if(i==IPField::FfG_YY)      {return getRefToFfG()(1,1);}
    else if(i==IPField::FfG_ZZ)      {return getRefToFfG()(2,2);}
    else if(i==IPField::FfG_XY)      {return getRefToFfG()(0,1);}
    else if(i==IPField::FfG_YZ)      {return getRefToFfG()(1,2);}
    else if(i==IPField::FfG_ZX)      {return getRefToFfG()(0,2);}

    else if(i==IPField::FpG_XX)     {return getRefToFpG()(0,0);}
    else if(i==IPField::FpG_YY)     {return getRefToFpG()(1,1);}
    else if(i==IPField::FpG_ZZ)     {return getRefToFpG()(2,2);}
    else if(i==IPField::FpG_XY)     {return getRefToFpG()(0,1);}
    else if(i==IPField::FpG_YZ)     {return getRefToFpG()(1,2);}
    else if(i==IPField::FpG_ZX)     {return getRefToFpG()(0,2);}

    else if(i==IPField::FveG_XX)      {return getRefToFveG()(0,0);}
    else if(i==IPField::FveG_YY)      {return getRefToFveG()(1,1);}
    else if(i==IPField::FveG_ZZ)      {return getRefToFveG()(2,2);}
    else if(i==IPField::FveG_XY)      {return getRefToFveG()(0,1);}
    else if(i==IPField::FveG_YZ)      {return getRefToFveG()(1,2);}
    else if(i==IPField::FveG_ZX)      {return getRefToFveG()(0,2);}

    else if(i==IPField::FP_XX)      {return getRefToFfR()(0,0);}
    else if(i==IPField::FP_YY)      {return getRefToFfR()(1,1);}
    else if(i==IPField::FP_ZZ)      {return getRefToFfR()(2,2);}
    else if(i==IPField::FP_XZ)      {return getRefToFfR()(0,1);}
    else if(i==IPField::FP_YZ)      {return getRefToFfR()(1,2);}
    else if(i==IPField::FP_ZX)      {return getRefToFfR()(0,2);}

    else if(i==IPField::FpR_XX)     {return getRefToFpR()(0,0);}
    else if(i==IPField::FpR_YY)     {return getRefToFpR()(1,1);}
    else if(i==IPField::FpR_ZZ)     {return getRefToFpR()(2,2);}
    else if(i==IPField::FpR_XY)     {return getRefToFpR()(0,1);}
    else if(i==IPField::FpR_YZ)     {return getRefToFpR()(1,2);}
    else if(i==IPField::FpR_ZX)     {return getRefToFpR()(0,2);}

    else if(i==IPField::FveR_XX)      {return getRefToFveR()(0,0);}
    else if(i==IPField::FveR_YY)      {return getRefToFveR()(1,1);}
    else if(i==IPField::FveR_ZZ)      {return getRefToFveR()(2,2);}
    else if(i==IPField::FveR_XY)      {return getRefToFveR()(0,1);}
    else if(i==IPField::FveR_YZ)      {return getRefToFveR()(1,2);}
    else if(i==IPField::FveR_ZX)      {return getRefToFveR()(0,2);}

    //else if(i==IPField::ZG)         {return getzg();}
    else if(i==IPField::PDF)        {return getRefToPDF();}
    else if(i==IPField::TT)         {return getRefToTt();}
    else if(i==IPField::WT)         {return getRefToWt();}

    else if(i==IPField::FthAM_XX)     {return getRefToFthAM()(0,0);}
    else if(i==IPField::FthAM_YY)     {return getRefToFthAM()(1,1);}
    else if(i==IPField::FthAM_ZZ)     {return getRefToFthAM()(2,2);}
    else if(i==IPField::FthAM_XY)     {return getRefToFthAM()(0,1);}
    else if(i==IPField::FthAM_YZ)     {return getRefToFthAM()(1,2);}
    else if(i==IPField::FthAM_ZX)     {return getRefToFthAM()(0,2);}

    else if(i==IPField::FfAM_XX)      {return getRefToFfAM()(0,0);}
    else if(i==IPField::FfAM_YY)      {return getRefToFfAM()(1,1);}
    else if(i==IPField::FfAM_ZZ)      {return getRefToFfAM()(2,2);}
    else if(i==IPField::FfAM_XY)      {return getRefToFfAM()(0,1);}
    else if(i==IPField::FfAM_YZ)      {return getRefToFfAM()(1,2);}
    else if(i==IPField::FfAM_ZX)      {return getRefToFfAM()(0,2);}

    else if(i==IPField::FpAM_XX)      {return getRefToFpAM()(0,0);}
    else if(i==IPField::FpAM_YY)      {return getRefToFpAM()(1,1);}
    else if(i==IPField::FpAM_ZZ)      {return getRefToFpAM()(2,2);}
    else if(i==IPField::FpAM_XY)      {return getRefToFpAM()(0,1);}
    else if(i==IPField::FpAM_YZ)      {return getRefToFpAM()(1,2);}
    else if(i==IPField::FpAM_ZX)      {return getRefToFpAM()(0,2);}

    else if(i==IPField::FveAM_XX)      {return getRefToFveAM()(0,0);}
    else if(i==IPField::FveAM_YY)      {return getRefToFveAM()(1,1);}
    else if(i==IPField::FveAM_ZZ)      {return getRefToFveAM()(2,2);}
    else if(i==IPField::FveAM_XY)      {return getRefToFveAM()(0,1);}
    else if(i==IPField::FveAM_YZ)      {return getRefToFveAM()(1,2);}
    else if(i==IPField::FveAM_ZX)      {return getRefToFveAM()(0,2);}

    else if(i==IPField::epsAM)        {return getRefToEquivalentPlasticDefoAM();}
    else if(i==IPField::epsR)         {return getRefToEquivalentPlasticDefoR();}
    else if(i==IPField::epsG)         {return getRefToEquivalentPlasticDefoG();}

    else if(i==IPField::Tcrys)        {return getRefToTcrys();}
    else if(i==IPField::Tmelt)        {return getRefToTmelt();}
    else if(i==IPField::Wcrys)        {return getRefToWcrys();}
    else if(i==IPField::Wmelt)        {return getRefToWmelt();}

    else if(i==IPField::TRefG)        {return getRefToTRefG();}
    else if(i==IPField::TRefR)        {return getRefToTRefR();}
    else if(i==IPField::TRefAM)        {return getRefToTRefAM();}
#if 0
        else if(i==IPField::AGl_XX)      {return getRefToAGl()(0,0);}
    else if(i==IPField::AGl_YY)      {return getRefToAGl()(1,1);}
    else if(i==IPField::AGl_ZZ)      {return getRefToAGl()(2,2);}
    else if(i==IPField::AGl_XY)      {return getRefToAGl()(0,1);}
    else if(i==IPField::AGl_YZ)      {return getRefToAGl()(1,2);}
    else if(i==IPField::AGl_ZX)      {return getRefToAGl()(0,2);}

    else if(i==IPField::ARl_XX)      {return getRefToARl()(0,0);}
    else if(i==IPField::ARl_YY)      {return getRefToARl()(1,1);}
    else if(i==IPField::ARl_ZZ)      {return getRefToARl()(2,2);}
    else if(i==IPField::ARl_XY)      {return getRefToARl()(0,1);}
    else if(i==IPField::ARl_YZ)      {return getRefToARl()(1,2);}
    else if(i==IPField::ARl_ZX)      {return getRefToARl()(0,2);}

    else if(i==IPField::AAMl_XX)      {return getRefToAAMl()(0,0);}
    else if(i==IPField::AAMl_YY)      {return getRefToAAMl()(1,1);}
    else if(i==IPField::AAMl_ZZ)      {return getRefToAAMl()(2,2);}
    else if(i==IPField::AAMl_XY)      {return getRefToAAMl()(0,1);}
    else if(i==IPField::AAMl_YZ)      {return getRefToAAMl()(1,2);}
    else if(i==IPField::AAMl_ZX)      {return getRefToAAMl()(0,2);}
#endif
    else if(i==IPField::ADl_XX)      {return getRefToADl()(0,0);}
    else if(i==IPField::ADl_YY)      {return getRefToADl()(1,1);}
    else if(i==IPField::ADl_ZZ)      {return getRefToADl()(2,2);}
    else if(i==IPField::ADl_XY)      {return getRefToADl()(0,1);}
    else if(i==IPField::ADl_YZ)      {return getRefToADl()(1,2);}
    else if(i==IPField::ADl_ZX)      {return getRefToADl()(0,2);}

    else if(i==IPField::EnThG_XX)      {return getRefToEnThG()(0,0);}
    else if(i==IPField::EnThG_YY)      {return getRefToEnThG()(1,1);}
    else if(i==IPField::EnThG_ZZ)      {return getRefToEnThG()(2,2);}
    else if(i==IPField::EnThG_XY)      {return getRefToEnThG()(0,1);}
    else if(i==IPField::EnThG_YZ)      {return getRefToEnThG()(1,2);}
    else if(i==IPField::EnThG_ZX)      {return getRefToEnThG()(0,2);}

    else if(i==IPField::EnThR_XX)      {return getRefToEnThR()(0,0);}
    else if(i==IPField::EnThR_YY)      {return getRefToEnThR()(1,1);}
    else if(i==IPField::EnThR_ZZ)      {return getRefToEnThR()(2,2);}
    else if(i==IPField::EnThR_XY)      {return getRefToEnThR()(0,1);}
    else if(i==IPField::EnThR_YZ)      {return getRefToEnThR()(1,2);}
    else if(i==IPField::EnThR_ZX)      {return getRefToEnThR()(0,2);}

    else if(i==IPField::EnThAM_XX)      {return getRefToEnThAM()(0,0);}
    else if(i==IPField::EnThAM_YY)      {return getRefToEnThAM()(1,1);}
    else if(i==IPField::EnThAM_ZZ)      {return getRefToEnThAM()(2,2);}
    else if(i==IPField::EnThAM_XY)      {return getRefToEnThAM()(0,1);}
    else if(i==IPField::EnThAM_YZ)      {return getRefToEnThAM()(1,2);}
    else if(i==IPField::EnThAM_ZX)      {return getRefToEnThAM()(0,2);}

    else if(i==IPField::appliedEnergy)  {return getRefToAppliedEnergy();}
    //else if(i==IPField::_SMPEnergy)     {return defoEnergy();}
    //else if(i==IPField::_thermalEnergy) {return getThermalEnergy();}
    else if(i==IPField::plasticDiss)    {return getRefToPlasticDiss();}
    else if(i==IPField::freezedDiss)    {return getRefToFreezedDiss();}
    else if(i==IPField::viscousDiss)    {return getRefToViscousDiss();}
    else if(i==IPField::EDiss)          {return getRefToEDiss();}

    else
    {
        static double temp = 0.0; return temp;
    }
}