//
// Description: storing class for inductor region in ElecMag problem
// computing normal direction to a gauss point to impose current
// density js0 in inductor
//
// Author:  <Vinayak GHOLAP>, (C) 2020
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "ipLinearElecMagInductor.h"
#include "restartManager.h"

IPLinearElecMagInductor::IPLinearElecMagInductor() : IPLinearElecMagTherMech() {Msg::Error("Cannot create default IPNormalDirectionGP");}

IPLinearElecMagInductor::IPLinearElecMagInductor(const SVector3 &GaussP, const SVector3 &Centroid, const SVector3 &CentralAxis):
IPLinearElecMagTherMech()
{
    SVector3 vec1(GaussP-Centroid);
    SVector3 normal(crossprod(CentralAxis,vec1));
    if (normal.norm() != 1.0)
    {
      normal.normalize();
    }
    _normal = normal;
}

IPLinearElecMagInductor::IPLinearElecMagInductor(const IPLinearElecMagInductor &source): IPLinearElecMagTherMech(source), _normal(source._normal) {}

IPLinearElecMagInductor& IPLinearElecMagInductor::operator=(const IPVariable &source)
{
  IPLinearElecMagTherMech::operator=(source);
  const IPLinearElecMagInductor* src = dynamic_cast<const IPLinearElecMagInductor*>(&source);
	if (src != NULL)
  {
		_normal = src->_normal;
	}
	return *this;
}

double IPLinearElecMagInductor::defoEnergy() const
{
  return  IPLinearElecMagTherMech::defoEnergy();
}

double IPLinearElecMagInductor::get(const int comp) const
{
    return IPLinearElecMagTherMech::get(comp);
}

double & IPLinearElecMagInductor::getRef(const int comp)
{
    return IPLinearElecMagTherMech::getRef(comp);
}

void IPLinearElecMagInductor::restart()
{
  IPLinearElecMagTherMech::restart();
  restartManager::restart(_normal);
  return;  
}