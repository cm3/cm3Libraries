//
// Description: storing class for hardening law
//
//
// Author:  <L. Noels>, (C) 2013
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "ipHardening.h"
#include "restartManager.h"

IPJ2IsotropicHardening::IPJ2IsotropicHardening(const double R0_):
      R0(R0_), R(R0_), dR(0.), ddR(0.), integR(0.),_isSaturated(false),dRdT(0.), ddRdTT(0.), ddRdT(0.), wp(0.), dwp(0.)
{

}

IPJ2IsotropicHardening::IPJ2IsotropicHardening(const IPJ2IsotropicHardening &source):
      R0(source.R0), R(source.R), dR (source.dR), ddR (source.ddR),integR(source.integR),_isSaturated(source._isSaturated), 
      dRdT(source.dRdT), ddRdTT(source.ddRdTT), ddRdT(source.ddRdT),
      wp(source.wp), dwp(source.dwp)
{

}

IPJ2IsotropicHardening &IPJ2IsotropicHardening::operator=(const IPJ2IsotropicHardening &src)
{
  R0     = src.R0;
  R      = src.R;
  dR     = src.dR;
  ddR    = src.ddR;
  integR = src.integR;
  _isSaturated = src._isSaturated;
  dRdT = src.dRdT;
  ddRdTT = src.ddRdTT;
  ddRdT = src.ddRdT;
  wp   = src.wp;
  dwp   = src.dwp;
  return *this;
}

void IPJ2IsotropicHardening::restart()
{
  restartManager::restart(R0);
  restartManager::restart(R);
  restartManager::restart(dR);
  restartManager::restart(ddR);
  restartManager::restart(integR);
  restartManager::restart(_isSaturated);
  restartManager::restart(dRdT);
  restartManager::restart(ddRdTT);
  restartManager::restart(ddRdT);
  restartManager::restart(wp);
  restartManager::restart(dwp);
  return;
}

IPUserJ2IsotropicHardening::IPUserJ2IsotropicHardening(const double R0_): IPJ2IsotropicHardening(R0_)
{

}

IPUserJ2IsotropicHardening::IPUserJ2IsotropicHardening(const IPUserJ2IsotropicHardening &source) : IPJ2IsotropicHardening(source)
{
}

IPUserJ2IsotropicHardening &IPUserJ2IsotropicHardening::operator=(const IPJ2IsotropicHardening &source)
{
  IPJ2IsotropicHardening::operator=(source);
  const IPUserJ2IsotropicHardening* src = dynamic_cast<const IPUserJ2IsotropicHardening*>(&source);
  if(src != NULL)
  {
  }
  return *this;
}

IPJ2IsotropicHardening * IPUserJ2IsotropicHardening::clone() const
{

  return new IPUserJ2IsotropicHardening(*this);
}

void IPUserJ2IsotropicHardening::restart()
{
  IPJ2IsotropicHardening::restart();
}

IPPerfectlyPlasticJ2IsotropicHardening::IPPerfectlyPlasticJ2IsotropicHardening(const double R0_): IPJ2IsotropicHardening(R0_)
{

}

IPPerfectlyPlasticJ2IsotropicHardening::IPPerfectlyPlasticJ2IsotropicHardening(const IPPerfectlyPlasticJ2IsotropicHardening &source) : IPJ2IsotropicHardening(source)
{
}

IPPerfectlyPlasticJ2IsotropicHardening &IPPerfectlyPlasticJ2IsotropicHardening::operator=(const IPJ2IsotropicHardening &source)
{
  IPJ2IsotropicHardening::operator=(source);
  const IPPerfectlyPlasticJ2IsotropicHardening* src = dynamic_cast<const IPPerfectlyPlasticJ2IsotropicHardening*>(&source);
  if(src != NULL)
  {
  }
  return *this;
}

IPJ2IsotropicHardening * IPPerfectlyPlasticJ2IsotropicHardening::clone() const
{

  return new IPPerfectlyPlasticJ2IsotropicHardening(*this);
}

void IPPerfectlyPlasticJ2IsotropicHardening::restart()
{
  IPJ2IsotropicHardening::restart();
}

IPPowerLawJ2IsotropicHardening::IPPowerLawJ2IsotropicHardening(const double R0_): IPJ2IsotropicHardening(R0_)
{

}

IPPowerLawJ2IsotropicHardening::IPPowerLawJ2IsotropicHardening(const IPPowerLawJ2IsotropicHardening &source) : IPJ2IsotropicHardening(source)
{
}

IPPowerLawJ2IsotropicHardening &IPPowerLawJ2IsotropicHardening::operator=(const IPJ2IsotropicHardening &source)
{
  IPJ2IsotropicHardening::operator=(source);
  const IPPowerLawJ2IsotropicHardening* src = dynamic_cast<const IPPowerLawJ2IsotropicHardening*>(&source);
  if(src != NULL)
  {
  }
  return *this;
}

void IPPowerLawJ2IsotropicHardening::restart()
{
  IPJ2IsotropicHardening::restart();
}

IPJ2IsotropicHardening * IPPowerLawJ2IsotropicHardening::clone() const
{
  return new IPPowerLawJ2IsotropicHardening(*this);
}


IPExponentialJ2IsotropicHardening::IPExponentialJ2IsotropicHardening(const double R0_): IPJ2IsotropicHardening(R0_)
{

}

IPExponentialJ2IsotropicHardening::IPExponentialJ2IsotropicHardening(const IPExponentialJ2IsotropicHardening &source) : IPJ2IsotropicHardening(source)
{
}

IPExponentialJ2IsotropicHardening &IPExponentialJ2IsotropicHardening::operator=(const IPJ2IsotropicHardening &source)
{
  IPJ2IsotropicHardening::operator=(source);
  const IPExponentialJ2IsotropicHardening* src = dynamic_cast<const IPExponentialJ2IsotropicHardening*>(&source);
  if(src != NULL)
  {
  }
  return *this;
}

void IPExponentialJ2IsotropicHardening::restart()
{
  IPJ2IsotropicHardening::restart();
}

IPJ2IsotropicHardening * IPExponentialJ2IsotropicHardening::clone() const
{
 return new IPExponentialJ2IsotropicHardening(*this);
}


IPSwiftJ2IsotropicHardening::IPSwiftJ2IsotropicHardening(const double R0_): IPJ2IsotropicHardening(R0_)
{

}

IPSwiftJ2IsotropicHardening::IPSwiftJ2IsotropicHardening(const IPSwiftJ2IsotropicHardening &source) : IPJ2IsotropicHardening(source)
{
}

IPSwiftJ2IsotropicHardening &IPSwiftJ2IsotropicHardening::operator=(const IPJ2IsotropicHardening &source)
{
  IPJ2IsotropicHardening::operator=(source);
  const IPSwiftJ2IsotropicHardening* src = dynamic_cast<const IPSwiftJ2IsotropicHardening*>(&source);
  if(src != NULL)
  {
  }
  return *this;
}

void IPSwiftJ2IsotropicHardening::restart()
{
  IPJ2IsotropicHardening::restart();
}

IPJ2IsotropicHardening * IPSwiftJ2IsotropicHardening::clone() const
{
  return new IPSwiftJ2IsotropicHardening(*this);
}

IPLinearExponentialJ2IsotropicHardening::IPLinearExponentialJ2IsotropicHardening(const double R0_): IPJ2IsotropicHardening(R0_)
{

}

IPLinearExponentialJ2IsotropicHardening::IPLinearExponentialJ2IsotropicHardening(const IPLinearExponentialJ2IsotropicHardening &source) : IPJ2IsotropicHardening(source)
{
}

IPLinearExponentialJ2IsotropicHardening &IPLinearExponentialJ2IsotropicHardening::operator=(const IPJ2IsotropicHardening &source)
{
  IPJ2IsotropicHardening::operator=(source);
  const IPLinearExponentialJ2IsotropicHardening* src = dynamic_cast<const IPLinearExponentialJ2IsotropicHardening*>(&source);
  if(src != NULL)
  {
  }
  return *this;
}

void IPLinearExponentialJ2IsotropicHardening::restart()
{
  IPJ2IsotropicHardening::restart();
}

IPJ2IsotropicHardening * IPLinearExponentialJ2IsotropicHardening::clone() const
{
  return new IPLinearExponentialJ2IsotropicHardening(*this);
}



/* IPLinearFollowedByExponentialJ2IsotropicHardening */
IPLinearFollowedByExponentialJ2IsotropicHardening::IPLinearFollowedByExponentialJ2IsotropicHardening(const double R0_): IPJ2IsotropicHardening(R0_)
{
}

IPLinearFollowedByExponentialJ2IsotropicHardening::IPLinearFollowedByExponentialJ2IsotropicHardening(const IPLinearFollowedByExponentialJ2IsotropicHardening &source) : IPJ2IsotropicHardening(source)
{
}

IPLinearFollowedByExponentialJ2IsotropicHardening &IPLinearFollowedByExponentialJ2IsotropicHardening::operator=(const IPJ2IsotropicHardening &source)
{
  IPJ2IsotropicHardening::operator=(source);
  const IPLinearFollowedByExponentialJ2IsotropicHardening* src = dynamic_cast<const IPLinearFollowedByExponentialJ2IsotropicHardening*>(&source);
  if(src != NULL)
  {
  }
  return *this;
}

void IPLinearFollowedByExponentialJ2IsotropicHardening::restart()
{
  IPJ2IsotropicHardening::restart();
}

IPJ2IsotropicHardening * IPLinearFollowedByExponentialJ2IsotropicHardening::clone() const
{
  return new IPLinearFollowedByExponentialJ2IsotropicHardening(*this);
}




/* IPLinearFollowedByPowerLawJ2IsotropicHardening */
IPLinearFollowedByPowerLawJ2IsotropicHardening::IPLinearFollowedByPowerLawJ2IsotropicHardening(const double R0_): IPJ2IsotropicHardening(R0_)
{
}

IPLinearFollowedByPowerLawJ2IsotropicHardening::IPLinearFollowedByPowerLawJ2IsotropicHardening(const IPLinearFollowedByPowerLawJ2IsotropicHardening &source) : IPJ2IsotropicHardening(source)
{
}

IPLinearFollowedByPowerLawJ2IsotropicHardening &IPLinearFollowedByPowerLawJ2IsotropicHardening::operator=(const IPJ2IsotropicHardening &source)
{
  IPJ2IsotropicHardening::operator=(source);
  const IPLinearFollowedByPowerLawJ2IsotropicHardening* src = dynamic_cast<const IPLinearFollowedByPowerLawJ2IsotropicHardening*>(&source);
  if(src != NULL)
  {
  }
  return *this;
}

void IPLinearFollowedByPowerLawJ2IsotropicHardening::restart()
{
  IPJ2IsotropicHardening::restart();
}

IPJ2IsotropicHardening * IPLinearFollowedByPowerLawJ2IsotropicHardening::clone() const
{
  return new IPLinearFollowedByPowerLawJ2IsotropicHardening(*this);
}


IPLinearFollowedByMultiplePowerLawJ2IsotropicHardening::IPLinearFollowedByMultiplePowerLawJ2IsotropicHardening(const double R0_): IPJ2IsotropicHardening(R0_)
{
}

IPLinearFollowedByMultiplePowerLawJ2IsotropicHardening::IPLinearFollowedByMultiplePowerLawJ2IsotropicHardening(const IPLinearFollowedByMultiplePowerLawJ2IsotropicHardening &source) : IPJ2IsotropicHardening(source)
{
}

IPLinearFollowedByMultiplePowerLawJ2IsotropicHardening &IPLinearFollowedByMultiplePowerLawJ2IsotropicHardening::operator=(const IPJ2IsotropicHardening &source)
{
  IPJ2IsotropicHardening::operator=(source);
  const IPLinearFollowedByMultiplePowerLawJ2IsotropicHardening* src = dynamic_cast<const IPLinearFollowedByMultiplePowerLawJ2IsotropicHardening*>(&source);
  if(src != NULL)
  {
  }
  return *this;
}

void IPLinearFollowedByMultiplePowerLawJ2IsotropicHardening::restart()
{
  IPJ2IsotropicHardening::restart();
}

IPJ2IsotropicHardening * IPLinearFollowedByMultiplePowerLawJ2IsotropicHardening::clone() const
{
  return new IPLinearFollowedByMultiplePowerLawJ2IsotropicHardening(*this);
}


IPPolynomialJ2IsotropicHardening::IPPolynomialJ2IsotropicHardening(const double R0_): IPJ2IsotropicHardening(R0_)
{

}

IPPolynomialJ2IsotropicHardening::IPPolynomialJ2IsotropicHardening(const IPPolynomialJ2IsotropicHardening &source) : IPJ2IsotropicHardening(source)
{
}

IPPolynomialJ2IsotropicHardening &IPPolynomialJ2IsotropicHardening::operator=(const IPJ2IsotropicHardening &source)
{
  IPJ2IsotropicHardening::operator=(source);
  const IPPolynomialJ2IsotropicHardening* src = dynamic_cast<const IPPolynomialJ2IsotropicHardening*>(&source);
  if(src != NULL)
  {
  }
  return *this;
}

void IPPolynomialJ2IsotropicHardening::restart()
{
  IPJ2IsotropicHardening::restart();
}

IPJ2IsotropicHardening * IPPolynomialJ2IsotropicHardening::clone() const
{
  return new IPPolynomialJ2IsotropicHardening(*this);
}

IPTwoExpJ2IsotropicHaderning::IPTwoExpJ2IsotropicHaderning(const double R0_): IPJ2IsotropicHardening(R0_)
{

}

IPTwoExpJ2IsotropicHaderning::IPTwoExpJ2IsotropicHaderning(const IPTwoExpJ2IsotropicHaderning &source) : IPJ2IsotropicHardening(source)
{
}

IPTwoExpJ2IsotropicHaderning &IPTwoExpJ2IsotropicHaderning::operator=(const IPJ2IsotropicHardening &source)
{
  IPJ2IsotropicHardening::operator=(source);
  const IPTwoExpJ2IsotropicHaderning* src = dynamic_cast<const IPTwoExpJ2IsotropicHaderning*>(&source);
  if(src != NULL)
  {
  }
  return *this;
}

void IPTwoExpJ2IsotropicHaderning::restart()
{
  IPJ2IsotropicHardening::restart();
}

IPJ2IsotropicHardening * IPTwoExpJ2IsotropicHaderning::clone() const
{
  return new IPTwoExpJ2IsotropicHaderning(*this);
}
