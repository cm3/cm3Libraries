//
// Description: storing class for transverse Isotropic law it is a "virtual implementation" you have to define an ipvariable
//              in your project which derive from this one. (all data in this ipvarible have name beggining by _j2l...)
//              so don't do the same in your project...
// Author:  <B. YANG, X. Liu, L. Wu>, (C) 2016
// 
// Copyright: See COPYING file that comes with this distribution
#ifndef IPTRANSVERSEISOTROPICYarnB_H_
#define IPTRANSVERSEISOTROPICYarnB_H_
#include "ipvariable.h"
#include "STensor3.h"
#include "ipTransverseIsotropic.h"
#include<iomanip>
#include<iostream>
#include<fstream>
#include<sstream>
#include<vector>
#include<string.h> 
#include<stdio.h>
#include<set>
#include<stdlib.h>
#include<algorithm>
using namespace std;

//TODO: Class Direction is used to describe the
//---------------------------------------------------------------------------
class Node {
protected:
	int _tag;//The unique index of the node
	double _Coor[3];//Coordinate variable
	bool _is_visited; //

public:
	//Constructor
	Node(int tag = 0, double ix = 0, double iy = 0, double iz = 0)
	{
		_tag = tag; _Coor[0] = ix; _Coor[1] = iy; _Coor[2] = iz;
		_is_visited = false;
	};
	
	void setNode(Node &source)
	{
		this->_tag = source._tag;
		this->_Coor[0] = source._Coor[0];
		this->_Coor[1] = source._Coor[1];
		this->_Coor[2] = source._Coor[2];
	};

	int get_tag() const{ return _tag;};
	double get_x() const{ return _Coor[0]; };
	double get_y() const{ return _Coor[1]; };
	double get_z() const{ return _Coor[2]; };
	double get_xyz(int Index) { return _Coor[Index]; };
	bool get_is_visited() { return _is_visited; }
	void set_is_visited(bool is_visited) { _is_visited = is_visited; }

	void NodePrint()
	{
		cout << _tag << _Coor[0] << _Coor[1] << _Coor[2] << endl;
	}
	
	virtual ~Node() {}

};


//----------Declare some useful functions-----------------------------------
// Order two Nodes
bool NodeAscending_x(const Node &N1, const Node &N2);
bool NodeAscending_y(const Node &N1,const  Node &N2);
bool NodeAscending_z(const Node &N1,const Node &N2);
int FirstApproxNode(std::vector<Node> NodeSet, int stretch_com, double v);
int maxArray_pos(double Array[]);
//---------------------------------------------------------------------------


class NodesSet {
protected:
	int _physic;//Physic number of the nods set
	std::vector<Node> _vec_nodes;//vector container of nodes
        vector<vector<double> > _max_min_xyz;
public:
	//Constructor
	NodesSet(int physic = 0){ _physic = physic;};
	NodesSet() { _physic = 0; };

	void NodePushBack(Node n) { _vec_nodes.push_back(n); };

	int get_physic() { return _physic; };
	Node get_node(int num){return _vec_nodes[num];};
	vector<Node> get_nodesset() {return _vec_nodes;};
    
	int get_NodeSetLength() { return _vec_nodes.size(); };
        vector<vector<double> > get_max_min_xyz(){return _max_min_xyz;}
        void set_max_min_xyz(vector<vector<double> > max_min_xyz){ _max_min_xyz = max_min_xyz;}
	virtual ~NodesSet() {}

};

//---------------------------------------------------------------------------
class Element {
protected:
	int _type; //An integer indicating Element type
	int _physical;  //An integer indicating the physical tag of this element
	int _eNum; //Index of this element
	vector<int> _nodes_id;//Nodes indices of this element

public:
	//Constructor
	Element(int type, int physical, int eNum, vector<int> nodes_id)
	{
		_type = type; _physical = physical; _eNum = eNum; _nodes_id = nodes_id;
	};

	int get_type() { return _type; };
	double get_physical() { return _physical; };
	double get_eNum() { return _eNum; };
	vector<int> get_nodes_id() { return _nodes_id; };

	void ElementPrint()
	{
		for (int i = 0; i<_nodes_id.size(); i++) { cout << _nodes_id[i] << " "; }
		cout << endl;
	}
	virtual ~Element() {}
};
//--------------------------------------------------------------------

class Mesher 
{
protected:
        vector<Node> _nodes_vec;	//vector of Nodes
		vector<Element> _elements_vec; //vector of elements
		vector<int> _physics_ref;      //vector of physics number
		int _physics_num;              //total physics number
        
		vector<int> _nodes_id_pos; //The position of this vector corresponds
		                           //to the id of the nodes and the value
		                           //is the position of this node in _nodes_vec.
                vector<int> _ele_id_physical_tab; //The position of this vector corresponds to
                                              //the id of the element and its value is the
                                              //physical value of the element.


		//TODO: Build the mesh by reading *.msh file
public:
		Mesher(string filePath);//Constructor
		int get_ele_size() { return _elements_vec.size(); }
		int get_nodes_size() { return _nodes_vec.size(); }

		Element get_element(int ele_position); 

		Node &get_node(int node_reference);

		void MesherPrint(ofstream *Outputfile);
		
		std::vector<Node> NodesGathering(int physics);
		std::vector<NodesSet> NodesGathering();

                std::vector<int> get_ele_id_physical_tab(){return _ele_id_physical_tab;}
		virtual ~Mesher() {}
};
//---------------------------------------------------------------------------
//TODO:Cubic spline for single variable
//********************Back ground*****************************
//Let interval is partitioned into sub intervals [xi-1,xi], for
//any x in [x(i-1),x(i)], the cubic spline s(x) is expressed by
//s(x)=M(i-1)*(x(i)-x)/h(i)+M(i)*(x-x(i-1))/h(i)
//with h(i) = x(i)-x(i-1).
//
//Let lam(i)=h(i)/(h(i)+h(i+1)), nu(i)=h(i+1)/(h(i)+h(i+1)),
//f[x(i),x(j)]=(f(x(i))-f(x(j)))/(x(i)-x(j))
//f[x(i),x(j),x(k)]=(f(x(i),x(j))-f((x(j),x(k)))/(x(i)-x(k)),
//then coefficient M is determined by
//2     1                                (f[x0,x1]-m0)/h(1)
//lam1  2     nu1                        6*f[x0,x1,x2]
//     lam2   2     nu2                  6*f[x1,x2,x3]
//            lam3   2    nu3             .
//                .    .     .    =       .
//                  .    .     .
//                  lamn-1 2 nun-1       6*f[x(n-2),x(n-1),x(n)]
//                         1   2         (mn-f[x(n-1),x(n)])/h(n)
//where m0 and mn are first order derivatives at the ends.
class Spline
{
 protected:
   vector<double> _M; //
   vector<double> _coor;
   vector<double> _f;
 public:
	 Spline() {};
     Spline(vector<double> v_ind, vector<double> v_f);
     ~Spline(){};
 
	 vector<double> get_M() const { return _M; };
	 vector<double> get_coor() const { return _coor; };
	 vector<double> get_f() const { return _f; };

	 void operator =(const Spline s);
	 void fulfill(vector<double> v_ind, vector<double> v_f);
     double get_dev(double x);
	 double get_val(double x);
};

//TODO: Cubic spline for 3D point series
//*******************Background*******************************
//The stretch component variable is considered as independent 
//variable and the other two are considered as function values.
//Suppose z is the free variable, then the space spline is expressed
//by (x(t),y(t),z(t)), x(t), y(t) and z(t) are cubic splines about z.
class SpaceSpline
{
 protected:
   Spline _s1;
   Spline _s2;
   Spline _s3;
   vector<double> _t;
 public:
     SpaceSpline(const vector<vector<double> > &xyz, const vector<double> &t);
     SpaceSpline(const Spline &s1, const Spline &s2, const Spline &s3);
     SpaceSpline(){};
     ~SpaceSpline(){};

     vector<double> get_dir(double t);
     vector<double> get_val(double t);

     void dataprint(string filename, vector<double> &t);
     void fulfill(const vector<vector<double> > &xyz, const vector<double> &t);
     vector<double> get_var(){return _t;};
};

//TODO: To describe the linear equation, the full expression
//      is expressed by M*x = V, where M is a matrix, x is 
//      the unknown vector and V is the right hand side.
class LinearEqs
{
 protected:
   vector<vector<double> > _M; //Matrix on LHS
   vector<double> _V;         //Vector on RHS
 public:
   LinearEqs(vector<vector<double> >& M, vector<double>& V);
   ~LinearEqs(){};
 
   void solve(vector<double>& s); 
};

//TODO: Class Direction is used to describe the stretch direction of Yarn
class Yarn
{
 protected:
  int _physics;
  int _SegNum;
  int _StretchAxis;
  vector<double> _PlanOuterNormal;

  SpaceSpline _s;

  public:
  // Constructor
  Yarn(NodesSet &NodeSet, 
       const int SegNum	            
       ); 

  Yarn(string yarninfoPath,
	   int PhysicalId,
	   int SegNum = 25
  );

  Yarn(){};

  // Destructor
  ~Yarn(){};

  int get_physics() {return _physics;}
  int get_segnum() {return _SegNum;}
  vector<double> get_dir(vector<double> x);
  //Remarks: The output is the vector of the spatial derivative of the yarn.
  //         The input vector is the coordinate where the derivative is computed.
  void InfoPrint(string filename);
  int get_StretchAxis(){return _StretchAxis;}
  vector<double> get_PlanOuterNormal(){return _PlanOuterNormal;}
};
//---------------------------------------------------------------------------

class IPTransverseIsoYarnB : public IPTransverseIsotropic
{
 protected:
  SVector3 _local_x;//Yarn stretch direction
  STensor3 _M;//Rotation matrix between local coor and global coor
  STensor3 _LS;//The linear stress tensor in local coordinate system
  SVector3 _Coor;//Coordinate of IP 
 public:

  IPTransverseIsoYarnB(const SVector3 &GaussP, Yarn YD);
  IPTransverseIsoYarnB(const SVector3 &GaussP, const vector<double> &Dir);
  IPTransverseIsoYarnB(const SVector3 &GaussP);
  IPTransverseIsoYarnB(const IPTransverseIsoYarnB &source);  
  IPTransverseIsoYarnB& operator=(const IPVariable &source);

 
  virtual void setLStress(const STensor3 &LS){_LS = LS;}
  virtual void setDirection(const SVector3 &A){_local_x = A;}
  virtual void setRotMatrix(const STensor3 &M){_M = M;}

  virtual const SVector3& getDirection() const {return _local_x;}  
  virtual SVector3& getDirection() {return _local_x;}  
  
  virtual const SVector3& getCoor() const{return _Coor;}
  virtual SVector3& getCoor(){return _Coor;}

  virtual const STensor3& getRotMatrix() const {return _M;}                                                                                                                 
  virtual STensor3& getRotMatrix(){return _M;}
                                              
  virtual const STensor3 getLStress() const{return _LS;}                                         
  virtual void restart();
  
  virtual IPVariable* clone() const{return new IPTransverseIsoYarnB(*this);}
 private:
  IPTransverseIsoYarnB();
};

#endif // IPTRANSVERSEISOTROPICYarnB_H_
