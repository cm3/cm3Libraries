//
// Description: storing class for linear thermomechanical law it is a "virtual implementation" you have to define an ipvariable
//              in your project which derive from this one.
// Author:  <L. Homsy>, (C) 2013
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef IPELECSMP_H_
#define IPELECSMP_H_
#include "ipvariable.h"

#include "ipSMP.h"
#include "STensor3.h"

class IPElecSMP : public IPSMP
{

public:
  IPElecSMP(double _Sa1, double _phia, double _Sastar1);
  IPElecSMP(const IPElecSMP &source);
  IPElecSMP& operator=(const IPVariable &source);
  virtual double defoEnergy() const;
  virtual IPVariable* clone() const {return new IPElecSMP(*this);};
protected:
  IPElecSMP();


};

#endif // IPANISOTROPICELECTHERMECH_H_
