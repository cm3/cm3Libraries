//
// C++ Interface: ipvariable
//
// Description: Base class for ipvariable
//
//
// Author:  <Gauthier BECKER>, (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
//
// class with the variables of IP (stress, deformation and localBasis)

#include "ipvariable.h"
#include "nonLinearMechSolver.h"

void IPVariable::setLocation(const IPVariable::LOCATION loc) {
	_onWhat = loc;
};


void IPVariable::get(const std::vector<int>& comp, std::vector<double>& val) const
{
  val.resize(comp.size());
  for (int i=0; i< comp.size(); i++)
  {
    val[i] = get(comp[i]);
  }
}

IPVariableMechanics &IPVariableMechanics::operator=(const IPVariable &source){
    IPVariable::operator=(source);
    const IPVariableMechanics* psrc = dynamic_cast<const IPVariableMechanics*>(&source);
    if (psrc != NULL){
    
    }
    return *this;
  }

