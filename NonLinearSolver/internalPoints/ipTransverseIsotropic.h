//
// Description: storing class for transverse Isotropic law it is a "virtual implementation" you have to define an ipvariable
//              in your project which derive from this one. (all data in this ipvarible have name beggining by _j2l...)
//              so don't do the same in your project...
// Author:  <L. Noels>, (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef IPTRANSVERSEISOTROPIC_H_
#define IPTRANSVERSEISOTROPIC_H_
#include "ipvariable.h"
#include "STensor3.h"
class IPTransverseIsotropic : public IPVariableMechanics
{
 public:
  double _elasticEnergy;
 public:
  IPTransverseIsotropic();
  IPTransverseIsotropic(const IPTransverseIsotropic &source);
  virtual IPTransverseIsotropic& operator=(const IPVariable &source);
  virtual double defoEnergy() const;
  virtual void restart();
  virtual IPVariable* clone() const {return new IPTransverseIsotropic(*this);};
};

#endif // IPTRANSVERSEISOTROPIC_H_
