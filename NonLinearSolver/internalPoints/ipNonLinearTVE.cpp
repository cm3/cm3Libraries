//
// C++ Interface: Material Law
//
// Description: IPNonLinearTVM (IP Thermo-ViscoElasto-ViscoPlasto Law) with Non-Local Damage Interface (soon.....)
//
// Author: <Ujwal Kishore J - FLE_Knight>, (C) 2022; <Van Dung Nguyen>, (C) 2014
//
// Copyright: See COPYING file that comes with this distribution
//
//

// #include "ipHyperelastic.h"
#include "ipNonLinearTVE.h"
// #include "restartManager.h"

IPNonLinearTVE::IPNonLinearTVE(const J2IsotropicHardening* comp,
                    const J2IsotropicHardening* trac,
                    const kinematicHardening* kin, const int N, const mullinsEffect* mullins): IPHyperViscoElastoPlastic(comp,trac,kin,N), // IPHyperViscoElastic(N), 
                                        _T(298.15),_devDE(0.),_trDE(0.),_DE(0.),_DcorKirDT(0.),_DDcorKirDTT(0.),
                                        _mechSrcTVE(0.),_DmechSrcTVEdT(0.),_DmechSrcTVEdF(0.),
                                        _thermalEnergy(0.),_dtShift(0.),_DdtShiftDT(0.),_DDdtShiftDDT(0.), _psiMax(0.), 
                                        _trCorKirinf_TVE(0.), _devCorKirinf_TVE(0.), _corKirExtra(0.), _mullinsDamage(0.), _DpsiDT(0.){
    _devOGammai.clear();
    _trOGammai.clear();
    _devDOGammaiDT.clear();
    _trDOGammaiDT.clear();
    _devOi.clear();
    _trOi.clear();
    _devDOiDT.clear();
    _trDOiDT.clear();
    _devDDOiDTT.clear();
    _trDDOiDTT.clear();
    _C.clear();
    _D.clear();
    _E.clear();
    _F.clear();
    _G.clear();
    for (int i=0; i < N; i++){
        STensor3 el(0.);
        _devOGammai.push_back(el);
        _trOGammai.push_back(0.);
        _devDOGammaiDT.push_back(el);
        _trDOGammaiDT.push_back(0.);
        _devOi.push_back(el);
        _trOi.push_back(0.);
        _devDOiDT.push_back(el);
        _trDOiDT.push_back(0.);
        _devDDOiDTT.push_back(el);
        _trDDOiDTT.push_back(0.);
        _C.push_back(el);
        _D.push_back(0.);
        _E.push_back(0.);
        _F.push_back(0.);
        _G.push_back(0.);
    }
    
    _ipMullinsEffect = NULL;
    if (mullins!= NULL)  mullins->createIPVariable(_ipMullinsEffect);
    
};

IPNonLinearTVE::IPNonLinearTVE(const IPNonLinearTVE& src): IPHyperViscoElastoPlastic(src), //IPHyperViscoElastic(src), 
            _devOGammai(src._devOGammai), _trOGammai(src._trOGammai), 
            _devDOGammaiDT(src._devDOGammaiDT), _trDOGammaiDT(src._trDOGammaiDT),
            _devOi(src._devOi), _trOi(src._trOi), _devDOiDT(src._devDOiDT), _trDOiDT(src._trDOiDT), _devDDOiDTT(src._devDDOiDTT), _trDDOiDTT(src._trDDOiDTT),
            _T(src._T),_devDE(src._devDE), _trDE(src._trDE), _DE(src._DE), _DcorKirDT(src._DcorKirDT), _DDcorKirDTT(src._DDcorKirDTT),
            _mechSrcTVE(src._mechSrcTVE),_DmechSrcTVEdT(src._DmechSrcTVEdT),_DmechSrcTVEdF(src._DmechSrcTVEdF),
            _C(src._C),_D(src._D), _E(src._E), _F(src._F), _G(src._G),
            _thermalEnergy(src._thermalEnergy),
            _dtShift(src._dtShift),_DdtShiftDT(src._DdtShiftDT),_DDdtShiftDDT(src._DDdtShiftDDT), _psiMax(src._psiMax),
            _trCorKirinf_TVE(src._trCorKirinf_TVE), _devCorKirinf_TVE(src._devCorKirinf_TVE), _corKirExtra(src._corKirExtra), 
            _mullinsDamage(src._mullinsDamage), _DpsiDT(src._DpsiDT){
                
    if (src._ipMullinsEffect != NULL)
        _ipMullinsEffect = dynamic_cast<IPMullinsEffect*>(src._ipMullinsEffect->clone());
    else
        _ipMullinsEffect = NULL;                
                
};
            
IPNonLinearTVE& IPNonLinearTVE::operator=(const IPVariable& src)
{
  // IPHyperViscoElastic::operator=(src);
  IPHyperViscoElastoPlastic::operator=(src);
  const IPNonLinearTVE* psrc = dynamic_cast<const IPNonLinearTVE*>(&src);
  if(psrc != NULL)
  {
    _devOGammai = psrc->_devOGammai; 
    _trOGammai = psrc->_trOGammai; 
    _devDOGammaiDT = psrc->_devDOGammaiDT; 
    _trDOGammaiDT = psrc->_trDOGammaiDT; 
    _T = psrc->_T;
    _mechSrcTVE = psrc->_mechSrcTVE;
    _DmechSrcTVEdT = psrc->_DmechSrcTVEdT;
    _DmechSrcTVEdF = psrc->_DmechSrcTVEdF;
    _devOi = psrc->_devOi; 
    _trOi = psrc->_trOi;
    _devDOiDT = psrc->_devDOiDT; 
    _trDOiDT = psrc->_trDOiDT;
    _devDDOiDTT = psrc->_devDDOiDTT; 
    _trDDOiDTT = psrc->_trDDOiDTT; 
    _devDE = psrc->_devDE; 
    _trDE = psrc->_trDE;
    _DE = psrc->_DE;
    _DcorKirDT = psrc->_DcorKirDT;
    _DDcorKirDTT = psrc->_DDcorKirDTT;
    _C = psrc->_C;
    _D = psrc->_D;
    _E = psrc->_E;
    _F = psrc->_F;
    _G = psrc->_G;
    _thermalEnergy = psrc->_thermalEnergy;
    _dtShift = psrc->_dtShift;
    _DdtShiftDT = psrc->_DdtShiftDT;
    _DDdtShiftDDT = psrc->_DDdtShiftDDT;
    _psiMax = psrc->_psiMax;
    _trCorKirinf_TVE = psrc->_trCorKirinf_TVE;
    _devCorKirinf_TVE = psrc->_devCorKirinf_TVE;
    _corKirExtra = psrc->_corKirExtra;
    _mullinsDamage = psrc->_mullinsDamage;
    _DpsiDT = psrc->_DpsiDT;
    
    if ( psrc->_ipMullinsEffect != NULL) {
      if (_ipMullinsEffect == NULL){
        _ipMullinsEffect = dynamic_cast<IPMullinsEffect*>(psrc->_ipMullinsEffect->clone());
      }
      else{
        _ipMullinsEffect->operator=(*dynamic_cast<const IPMullinsEffect*>(psrc->_ipMullinsEffect));
      }
    }
    
  }
  return *this;
}

IPNonLinearTVE::~IPNonLinearTVE(){
  if (_ipMullinsEffect != NULL) delete _ipMullinsEffect;
  _ipMullinsEffect = NULL;
};

double IPNonLinearTVE::defoEnergy() const
{
  return IPHyperViscoElastic::defoEnergy();
}

// Add plasticEnergy later -----------                                      CHANGE!!!
/*
double IPNonLinearTVE::plasticEnergy() const
{
  return IPHyperViscoElastic::plasticEnergy();
}
*/

void IPNonLinearTVE::restart(){
  // IPHyperViscoElastic::restart();
  IPHyperViscoElastoPlastic::restart();
  restartManager::restart(_devOGammai);
  restartManager::restart(_trOGammai);
  restartManager::restart(_devDOGammaiDT);
  restartManager::restart(_trDOGammaiDT);
  restartManager::restart(_devOi);
  restartManager::restart(_trOi);
  restartManager::restart(_devDOiDT);
  restartManager::restart(_trDOiDT);
  restartManager::restart(_devDDOiDTT);
  restartManager::restart(_trDDOiDTT);
  restartManager::restart(_devDE);
  restartManager::restart(_trDE);
  restartManager::restart(_DE);
  restartManager::restart(_DcorKirDT);
  restartManager::restart(_DDcorKirDTT);
  restartManager::restart(_mechSrcTVE);
  restartManager::restart(_DmechSrcTVEdT);
  restartManager::restart(_DmechSrcTVEdF);
  restartManager::restart(_T);
  restartManager::restart(_C);
  restartManager::restart(_D);
  restartManager::restart(_E);
  restartManager::restart(_F);
  restartManager::restart(_G);
  restartManager::restart(_thermalEnergy);
  restartManager::restart(_dtShift);
  restartManager::restart(_DdtShiftDT);
  restartManager::restart(_DDdtShiftDDT);
  restartManager::restart(_psiMax);
  restartManager::restart(_trCorKirinf_TVE);
  restartManager::restart(_devCorKirinf_TVE);
  restartManager::restart(_corKirExtra);
  restartManager::restart(_mullinsDamage);
  restartManager::restart(_DpsiDT);
  
  if (_ipMullinsEffect != NULL)
   restartManager::restart(_ipMullinsEffect);
}

const IPMullinsEffect& IPNonLinearTVE::getConstRefToIPMullinsEffect() const{
  if(_ipMullinsEffect==NULL)
      Msg::Error("IPNonLinearTVE: _ipMullinsEffect not initialized");
  return *_ipMullinsEffect;
};
IPMullinsEffect& IPNonLinearTVE::getRefToIPMullinsEffect(){
  if(_ipMullinsEffect==NULL)
      Msg::Error("IPNonLinearTVE: _ipMullinsEffect not initialized");
  return *_ipMullinsEffect;
};