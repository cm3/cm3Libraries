//
// Description: storing class for crystal plasticity
//
//
// Author:  <L. Noels>, (C) 2019
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "ipUMATInterface.h"
#include "ipField.h"
#include "restartManager.h"

double IPUMATInterface::defoEnergy() const
{
  return _elasticEne;
}
double IPUMATInterface::plasticEnergy() const
{
  return _plasticEne;
}

double IPUMATInterface::damageEnergy() const
{
  return _damageEne;
}

double IPUMATInterface::elementSize() const
{
  return _elementSize;
}

double IPUMATInterface::get(int comp) const
{

  if(comp >= IPField::SIG_XX and comp<=IPField::SIG_XY )
	  return _stress(comp-1);
  else if(comp==IPField::SIG_XZ) // order xx yy zz xy xz yz
	  return _stress(comp-2);
  else if(comp==IPField::SIG_YZ)
	  return _stress(comp);
  else if(comp == IPField::SVM) // von Mises
  {

     double svm= (_stress(0)-_stress(1))*(_stress(0)-_stress(1))+(_stress(2)-_stress(1))*(_stress(2)-_stress(1))+(_stress(2)-_stress(0))*(_stress(2)-_stress(0));
     svm += 6.*(_stress(3)*_stress(3)+_stress(4)*_stress(4)+_stress(5)*_stress(5));
     svm /= 2.;
     svm = sqrt(svm);
     return svm;
  }
  else if(comp >= IPField::STRAIN_XX and comp<IPField::STRAIN_XY )
	  return _strain(comp-IPField::STRAIN_XX);
  else if(comp == IPField::STRAIN_XY)
	  return _strain(comp-IPField::STRAIN_XX)/2.;
  else if(comp==IPField::STRAIN_XZ) // order xx yy zz xy xz yz
	  return _strain(comp-IPField::STRAIN_XX-1)/2.;
  else if(comp==IPField::STRAIN_YZ)
	  return _strain(comp-IPField::STRAIN_XX+1)/2.;
  else
    return 0.;
}

void IPUMATInterface::restart()
{
  IPVariableMechanics::restart();
  restartManager::restart(_nsdv);
  restartManager::restart(_materialTensor.getDataPtr(),36);
  restartManager::restart(_strain.getDataPtr(),6);
  restartManager::restart(_stress.getDataPtr(),6);
  restartManager::restart(_statev,_nsdv);
  restartManager::restart(_elasticEne);
  restartManager::restart(_plasticEne);
  restartManager::restart(_damageEne);
  restartManager::restart(_elementSize);
  restartManager::restart(_dissipationBlocked);
  return;
}
