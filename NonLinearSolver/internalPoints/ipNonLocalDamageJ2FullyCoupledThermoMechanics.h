//
// Description: storing class for j2 thermo-elasto-plastic law with non local damage
// Author:  <L. Noels>, (C) 2021
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef IPNONLOCALDAMAGEJ2FULLYCOUPLEDTHERMOMECHANICS_H_
#define IPNONLOCALDAMAGEJ2FULLYCOUPLEDTHERMOMECHANICS_H_
#include "ipJ2ThermoMechanics.h"
#include "ipHardening.h"
#include "ipCLength.h"
#include "ipDamage.h"
#include "CLengthLaw.h"
#include "DamageLaw.h"


class IPLocalDamageJ2FullyCoupledThermoMechanics : public IPJ2ThermoMechanics
{
 protected:
  IPDamage*  ipvDam;
  double _damageEnergy;

 public:
  IPLocalDamageJ2FullyCoupledThermoMechanics();
  IPLocalDamageJ2FullyCoupledThermoMechanics(const J2IsotropicHardening *j2IH,  const DamageLaw *dl);
  virtual ~IPLocalDamageJ2FullyCoupledThermoMechanics()
  {
    if(ipvDam !=NULL)
    {
      delete ipvDam; ipvDam = NULL;
    }
  }

  IPLocalDamageJ2FullyCoupledThermoMechanics(const IPLocalDamageJ2FullyCoupledThermoMechanics &source);
  virtual IPLocalDamageJ2FullyCoupledThermoMechanics& operator=(const IPVariable &source);
  virtual void restart();
  
  virtual double damageEnergy()  const {return _damageEnergy;};
  virtual double& getRefToDamageEnergy() {return _damageEnergy;};

  virtual IPVariable* clone() const {return new IPLocalDamageJ2FullyCoupledThermoMechanics(*this);};

  virtual const IPDamage &getConstRefToIPDamage() const
  {
    if(ipvDam==NULL)
      Msg::Error("IPNonLocalDamageJ2FullyCoupledThermoMechanics: ipvDam not initialized");
    return *ipvDam;
  }
  virtual IPDamage &getRefToIPDamage()
  {
    if(ipvDam==NULL)
      Msg::Error("IPNonLocalDamageJ2FullyCoupledThermoMechanics: ipvDam not initialized");
    return *ipvDam;
  }

  virtual double getDamage() const
  {
    if(ipvDam==NULL)
      Msg::Error("IPNonLocalDamageJ2FullyCoupledThermoMechanics: ipvDam not initialized");
    return ipvDam->getDamage();
  }
  virtual double getMaximalP() const
  {
    if(ipvDam==NULL)
      Msg::Error("IPNonLocalDamageJ2FullyCoupledThermoMechanics: ipvDam not initialized");
    return ipvDam->getMaximalP();
  }
  virtual double getDeltaDamage() const
  {
    if(ipvDam==NULL)
      Msg::Error("IPNonLocalDamageJ2FullyCoupledThermoMechanics: ipvDam not initialized");
    return ipvDam->getDeltaDamage();
  }
  virtual double getDDamageDp() const
  {
    if(ipvDam==NULL)
      Msg::Error("IPNonLocalDamageJ2FullyCoupledThermoMechanics: ipvDam not initialized");
    return ipvDam->getDDamageDp();
  }
  virtual const STensor3 &getConstRefToDDamageDFe() const
  {
    if(ipvDam==NULL)
      Msg::Error("IPNonLocalDamageJ2FullyCoupledThermoMechanics: ipvDam not initialized");
    return ipvDam->getConstRefToDDamageDFe();
  }
 
 
 };
 
 
class IPNonLocalDamageJ2FullyCoupledThermoMechanics : public IPLocalDamageJ2FullyCoupledThermoMechanics
{
 protected:
  IPCLength* ipvCL;

  bool nonLocalToLocal; // allows switching from non-local to local
	double _DirreversibleEnergyDNonLocalVariable;
// bareps
  double _nonlocalPlasticStrain;

 public:
  IPNonLocalDamageJ2FullyCoupledThermoMechanics();
  IPNonLocalDamageJ2FullyCoupledThermoMechanics(const J2IsotropicHardening *j2IH, const CLengthLaw *cll, const DamageLaw *dl);
  virtual ~IPNonLocalDamageJ2FullyCoupledThermoMechanics()
  {
    if(ipvCL != NULL)
    {
      delete ipvCL; ipvCL = NULL;
    }
  }


  IPNonLocalDamageJ2FullyCoupledThermoMechanics(const IPNonLocalDamageJ2FullyCoupledThermoMechanics &source);
  virtual IPNonLocalDamageJ2FullyCoupledThermoMechanics& operator=(const IPVariable &source);
  virtual void restart();

  virtual void setNonLocalToLocal(const bool fl){nonLocalToLocal = fl;};
  virtual bool getNonLocalToLocal() const {return nonLocalToLocal;};
	
	virtual const double& getDIrreversibleEnergyDNonLocalVariable() const {return _DirreversibleEnergyDNonLocalVariable;};
	virtual double& getRefToDIrreversibleEnergyDNonLocalVariable() {return _DirreversibleEnergyDNonLocalVariable;};


  virtual IPVariable* clone() const {return new IPNonLocalDamageJ2FullyCoupledThermoMechanics(*this);};
  virtual double getEffectivePlasticStrain() const { return _nonlocalPlasticStrain;}
  virtual double &getRefToEffectivePlasticStrain() { return _nonlocalPlasticStrain;}

 
  virtual const IPCLength &getConstRefToIPCLength() const
  {
    if(ipvCL==NULL)
      Msg::Error("IPNonLocalDamageJ2FullyCoupledThermoMechanics: ipvCL not initialized");
    return *ipvCL;
  }
  virtual IPCLength &getRefToIPCLength()
  {
    if(ipvCL==NULL)
      Msg::Error("IPNonLocalDamageJ2FullyCoupledThermoMechanics: ipvCL not initialized");
    return *ipvCL;
  }
  virtual const STensor3 &getConstRefToCharacteristicLength() const
  {
    if(ipvCL==NULL)
      Msg::Error("IPNonLocalDamageJ2FullyCoupledThermoMechanics: ipvCL not initialized");
    return ipvCL->getConstRefToCL();
  }
  virtual STensor3 &getRefToCharacteristicLength() const
  {
    if(ipvCL==NULL)
      Msg::Error("IPNonLocalDamageJ2FullyCoupledThermoMechanics: ipvCL not initialized");
    return ipvCL->getRefToCL();
  }

};

 #endif //NONLOCALDAMAGEJ2FULLYCOUPLEDTHERMOMECHANICS_H_
