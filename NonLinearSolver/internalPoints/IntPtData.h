//
//
// Description: store the data of shape functions: values and derivatives
// to avoid recompute in the iterative procedure
//
// Author:  <Van Dung NGUYEN>, (C) 2012
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef INTPTDATA_H_
#define INTPTDATA_H_

#include "functionSpace.h"
#include "nlsFunctionSpace.h"
#include "CurlFunctionSpace.h"

template<class T>
class IntPtData{
  public:
    typedef typename TensorialTraits<T>::ValType ValType;
    typedef typename TensorialTraits<T>::GradType GradType;
    typedef typename TensorialTraits<T>::HessType HessType;
    typedef typename TensorialTraits<T>::ThirdDevType ThirdDevType;
    typedef typename CurlTraits<T>::CurlType CurlType;
    typedef typename CurlTraits<T>::CurlCurlType CurlCurlType;

    std::vector<ValType>  val; // all values of shape functions
    std::vector<GradType> grad; // all gradients of shape functions
    std::vector<HessType> hess; // all hessiens of shape functions
    std::vector<ThirdDevType> third; // all third derivatives of shape functions
    std::vector<CurlType> curlVal; // all values of shape functions in Hcurl
    std::vector<CurlType> CurlcurlVal; // all Curl values of shape functions in Hcurl
    double jacobianDet; // jacobien determinent
		
		bool _valEstimated;
		bool _gradEstimated;
		bool _hessEstimated;
		bool _thirdEstimated;
    bool _curlEstimated;
    bool _CurlcurlEstimated;
		bool _jacobianEstimated;
		
  public:
    IntPtData():_valEstimated(false),_gradEstimated(false),_hessEstimated(false),_thirdEstimated(false),
                _curlEstimated(false),_CurlcurlEstimated(false),_jacobianEstimated(false){};
    IntPtData(const IntPtData& src) :val(src.val), grad(src.grad),hess(src.hess),third(src.third),
                                     curlVal(src.curlVal),CurlcurlVal(src.CurlcurlVal),jacobianDet(src.jacobianDet),
                                    _valEstimated(src._valEstimated),_gradEstimated(src._gradEstimated),
																		_hessEstimated(src._hessEstimated),_thirdEstimated(src._thirdEstimated),
                                    _curlEstimated(src._curlEstimated), _CurlcurlEstimated(src._CurlcurlEstimated),
																		_jacobianEstimated(src._jacobianEstimated){}
    IntPtData& operator = (const IntPtData& src){
      val = src.val;
      grad =src.grad;
      hess = src.hess;
      third = src.third;
      curlVal = src.curlVal;
      CurlcurlVal = src.CurlcurlVal;
      jacobianDet = src.jacobianDet;

			_valEstimated = src._valEstimated;
			_gradEstimated = src._gradEstimated;
			_hessEstimated = src._hessEstimated;
			_thirdEstimated = src._thirdEstimated;
      _curlEstimated = src._curlEstimated;
      _CurlcurlEstimated = src._CurlcurlEstimated;
			_jacobianEstimated =src._jacobianEstimated;
      return *this;
    };
		
		std::vector<ValType>& f(const FunctionSpaceBase* space, MElement* ele, IntPt &GP){
			if (!_valEstimated){
				_valEstimated = true;
				const FunctionSpace<T>* spT = dynamic_cast<const FunctionSpace<T>*>(space);
				if (spT!=NULL){
					spT->f(ele,GP.pt[0],GP.pt[1],GP.pt[2],val);
				};
			};
			return val;
		};
		
		std::vector<GradType>& gradf(const FunctionSpaceBase* space, MElement* ele, IntPt &GP){
			if (!_gradEstimated){
				_gradEstimated=true;
				const FunctionSpace<T>* spT = dynamic_cast<const FunctionSpace<T>*>(space);
				if (spT!=NULL){
					spT->gradf(ele,GP.pt[0],GP.pt[1],GP.pt[2],grad);
				};
			};
			return grad;
		};
		
    std::vector<CurlType> & fcurl(const FunctionSpaceBase* space, MElement* ele, IntPt &GP, const std::string &typeFunction)
    {
      if (!_curlEstimated)
      {
        _curlEstimated = true;
        const CurlFunctionSpaceBase<T>* spT = dynamic_cast<const CurlFunctionSpaceBase<T>* >(space);
        if (spT != NULL)
        {
          if (typeFunction == "HcurlLegendre")
            spT->fcurl(ele,GP.pt[0],GP.pt[1],GP.pt[2],typeFunction,curlVal);
          else if (typeFunction == "CurlHcurlLegendre")
              Msg::Error("Wrong function invoked: use Curlfcurl() instead");
          else
            Msg::Error("Invalid typeFunction argument parsed in fcurl()");
        }
      }
      return curlVal;
    }
    
    std::vector<CurlCurlType> & Curlfcurl(const FunctionSpaceBase* space, MElement* ele, IntPt &GP, const std::string &typeFunction)
    {
      if (!_CurlcurlEstimated)
      {
        _CurlcurlEstimated = true;
        const CurlFunctionSpaceBase<T>* spT = dynamic_cast<const CurlFunctionSpaceBase<T>* >(space);
        if (spT != NULL)
        {
          if (typeFunction == "CurlHcurlLegendre")
            spT->Curlfcurl(ele,GP.pt[0],GP.pt[1],GP.pt[2],typeFunction,CurlcurlVal);
          else if (typeFunction == "HcurlLegendre")
              Msg::Error("Wrong function invoked: use fcurl() instead");
          else
            Msg::Error("Invalid typeFunction argument parsed in Curlfcurl()");
        }
      }
      return CurlcurlVal;
    }    
    
		std::vector<HessType>& hessf(const FunctionSpaceBase* space, MElement* ele, IntPt &GP){
			if (!_hessEstimated){
				_hessEstimated = true;
				const FunctionSpace<T>* spT = dynamic_cast<const FunctionSpace<T>*>(space);
				if (spT!=NULL){
					spT->hessf(ele,GP.pt[0],GP.pt[1],GP.pt[2],hess);
				};
			};
			return hess;
		};
		
		std::vector<ThirdDevType>&  thirdDevf(const FunctionSpaceBase* space, MElement* ele, IntPt &GP){
			if (!_thirdEstimated){
				_thirdEstimated = true;
				const FunctionSpace<T>* spT = dynamic_cast<const FunctionSpace<T>*>(space);
				if (spT!=NULL){
					spT->thirdDevf(ele,GP.pt[0],GP.pt[1],GP.pt[2],third);
				};
			};
			return third;
		};
		
		double& getJacobianDeterminant(MElement* ele, IntPt &GP){
			if (!_jacobianEstimated){
				_jacobianEstimated = true;
				jacobianDet = ele->getJacobianDeterminant(GP.pt[0],GP.pt[1],GP.pt[2]);
			}
			return jacobianDet;
		};
		
		void clearAll(){
			val.clear();
      grad.clear();
      hess.clear();
      third.clear();
      curlVal.clear();
      CurlcurlVal.clear();
			_valEstimated = false;
			_gradEstimated = false;
			_hessEstimated = false;
			_thirdEstimated = false;
      _curlEstimated = false;
      _CurlcurlEstimated = false;
			_jacobianEstimated = false;
		};
    ~IntPtData(){
      val.clear();
      grad.clear();
      hess.clear();
      third.clear();
      curlVal.clear();
      CurlcurlVal.clear();
		};
};


#endif // INTPTDATA_H_
