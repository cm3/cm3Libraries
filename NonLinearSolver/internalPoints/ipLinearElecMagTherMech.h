//
// Description: storing class for linear electro-magnetic-thermo-mechanical law it is a "virtual implementation" you have to define an ipvariable
//              in your project which derive from this one.
// Author:  <Vinayak GHOLAP>, (C) 2019
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef IPLINEARELECMAGTHERMECH_H_
#define IPLINEARELECMAGTHERMECH_H_
#include "ipvariable.h"

#include "ipLinearElecTherMech.h"
#include "STensor3.h"

class IPLinearElecMagTherMech : public  IPLinearElecTherMech
{
public:
  IPLinearElecMagTherMech();
  IPLinearElecMagTherMech(const IPLinearElecMagTherMech &source);
  IPLinearElecMagTherMech& operator=(const IPVariable &source);
  virtual double defoEnergy() const;
  virtual IPVariable* clone() const
  {
    return new IPLinearElecMagTherMech(*this);
  }
  virtual void restart();
  virtual double get(const int comp) const;
  virtual double & getRef(const int comp);
};

#endif // IPLinearElecMagTherMech_H_

