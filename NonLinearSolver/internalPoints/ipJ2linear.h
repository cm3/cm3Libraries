//
// Description: storing class for j2 linear elasto-plastic law it is a "virtual implementation" you have to define an ipvariable
//              in your project which derive from this one. (all data in this ipvarible have name beggining by _j2l...)
//              so don't do the same in your project...
// Author:  <Gauthier BECKER>, (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef IPJ2LINEAR_H_
#define IPJ2LINEAR_H_
#include "ipvariable.h"
#include "STensor3.h"
#include "ipHardening.h"
#include "j2IsotropicHardening.h"
#include "ipField.h"

class IPJ2linear : public IPVariableMechanics
{
 protected:
  IPJ2IsotropicHardening* ipvJ2IsotropicHardening;

 public: // All data public to avoid the creation of function to access
  double _j2lepspbarre; // equivalent plastic strain
  STensor3 _j2lepsp;    // plastic part of the deformation gradient
  double _j2ldsy;       // yield stress increment
  double _elasticEnergy; // elastic energy stored

  STensor3 _P;
  STensor3 _sig;
  double _svm;
  double _eigenstress_eq;
  STensor3 _eigenstress;
  STensor3 _eigenstrain;

  STensor3 _strain;
  
  double _plasticEnergy;
  STensor3 _DplasticEnergyDF;
  double _plasticPower; // plastic power
  STensor3 _DplasticPowerDF; // dplastic power DF
  STensor3 _Ee; // elastic deformation
	
	double _irreversibleEnergy;
	STensor3 _DirreversibleEnergyDF;
	
	bool _dissipationBlocked;
  bool _dissipationActive;
	
 public:
  IPJ2linear();
  IPJ2linear(const J2IsotropicHardening *j2IH);
  IPJ2linear(const IPJ2linear &source);
  virtual IPJ2linear& operator=(const IPVariable &source);
  virtual ~IPJ2linear()
  {
    if(ipvJ2IsotropicHardening != NULL)
    {
      delete ipvJ2IsotropicHardening; ipvJ2IsotropicHardening = NULL;
    }
  }
  
  virtual bool& getRefToDissipationActive(){return _dissipationActive;};
  virtual bool dissipationIsActive() const {return _dissipationActive;};

	virtual void blockDissipation(const bool fl){_dissipationBlocked = fl;};
  virtual bool dissipationIsBlocked() const{return _dissipationBlocked;};
	
  virtual double defoEnergy() const;
  virtual double plasticEnergy() const;
  virtual double plasticEqStrain() const;
  virtual double eigenstressEq() const;
  virtual double svm() const;

	virtual double irreversibleEnergy() const {return _irreversibleEnergy;};
	virtual double & getRefToIrreversibleEnergy() {return _irreversibleEnergy;};
	
	virtual STensor3& getRefToDIrreversibleEnergyDF() {return _DirreversibleEnergyDF;};
	virtual const STensor3& getConstRefToDIrreversibleEnergyDF() const{return _DirreversibleEnergyDF;};
		
  virtual void restart();
  virtual const IPJ2IsotropicHardening &getConstRefToIPJ2IsotropicHardening() const
  {
    if(ipvJ2IsotropicHardening==NULL)
      Msg::Error("IPJ2linear: ipvJ2IsotropicHardening not initialized");
    return *ipvJ2IsotropicHardening;
  }
  virtual IPJ2IsotropicHardening &getRefToIPJ2IsotropicHardening()
  {
    if(ipvJ2IsotropicHardening==NULL)
      Msg::Error("IPJ2linear: ipvJ2IsotropicHardening not initialized");
    return *ipvJ2IsotropicHardening;
  }
  virtual const STensor3 & getConstRefToFp() const{return _j2lepsp; }
  virtual STensor3 & getRefToFp() { return _j2lepsp;}

  virtual double& getRefToEquivalentPlasticStrain() {return _j2lepspbarre;};
  virtual const double& getConstRefToEquivalentPlasticStrain() const {return _j2lepspbarre;};
  
  virtual double& getRefToSVM() {return _svm;};
  virtual const double& getConstRefToSVM() const {return _svm;};

  virtual double& getRefToYieldIncrement() {return _j2ldsy;};
  virtual const double& getConstRefToYieldIncrement() const {return _j2ldsy;};

  virtual double& getRefToElasticEnergy() {return _elasticEnergy;};

  virtual double& getRefToPlasticEnergy() {return _plasticEnergy;};
  virtual const double& getConstRefToPlasticEnergy() const {return _plasticEnergy;};
  
  virtual STensor3& getRefToDPlasticEnergyDF() {return _DplasticEnergyDF;};
  virtual const STensor3& getConstRefToDPlasticEnergyDF() const {return _DplasticEnergyDF;};

  virtual double& getRefToPlasticPower() {return _plasticPower;};
  virtual const double& getConstRefToPlasticPower() const {return _plasticPower;};

  virtual STensor3& getRefToDPlasticPowerDF() {return _DplasticPowerDF;};
  virtual const STensor3& getConstRefToDPlasticPowerDF() const {return _DplasticPowerDF;};

  virtual STensor3& getRefToElasticDeformationTensor() {return _Ee;}
  virtual const STensor3& getConstRefToElasticDeformationTensor() const {return _Ee;};

  virtual STensor3& getRefToStress() {return _sig;};
  virtual const STensor3& getConstRefToStress() const {return _sig;};

  virtual STensor3& getRefToStrain() {return _strain;};
  virtual const STensor3& getConstRefToStrain() const {return _strain;};

  virtual double get(const int comp) const;

  virtual IPVariable* clone() const {return new IPJ2linear(*this);};
};

#endif // IPJ2LINEAR_H_

