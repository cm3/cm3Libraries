/*********************************************************************************************/
/** Copyright (C) 2022 - See COPYING file that comes with this distribution	                **/
/** Author: Mohamed HADDAD (MEMA-iMMC-UCLouvain)							                              **/
/** Contact: mohamed.haddad@uclouvain.be					                                      		**/
/**											                                                                  	**/
/** Description: storing class of IP variabales for VE-VP material law (small strains)		  **/
/** VE-VP material law : See B.Miled et I.Doghri 2011                                       **/
/**********************************************************************************************/

#ifndef IPVEVPMFH_H_
#define IPVEVPMFH_H_
#include "ipvariable.h"
#include "STensor3.h"
#include "fullMatrix.h"

#include "UsefulFunctionsVEVPMFH.h"

class IPVEVPMFH : public IPVariableMechanics{

  public: 
    VEVPMFHSPACE::STATE _state;               // Structure containing all state variables (defined in UsefulFunctionsVEVPMFH.h)
    double _elasticEne;
    double _plasticEne;
  
  protected:
    double _irreversibleEnergy;
    STensor3 _DirreversibleEnergyDF;    


  public:
    IPVEVPMFH() : IPVariableMechanics(), _elasticEne(0.),_plasticEne(0.), _irreversibleEnergy(0),  _DirreversibleEnergyDF(0.){ 
       VEVPMFHSPACE::mallocstate(&_state);    //create and initialize state structure
    };

    IPVEVPMFH(const IPVEVPMFH &source) : IPVariableMechanics(source){
        VEVPMFHSPACE::mallocstate(&_state);   //create and state structure
        VEVPMFHSPACE::copystate(&(source._state), &_state); //copy state structure
	      _elasticEne=source._elasticEne;
 	      _plasticEne=source._plasticEne;
        _irreversibleEnergy = source._irreversibleEnergy;
        _DirreversibleEnergyDF = source._DirreversibleEnergyDF;
    };

    IPVEVPMFH &operator = (const IPVariable &_source){
      IPVariableMechanics::operator=(_source);
      const IPVEVPMFH* source=static_cast<const IPVEVPMFH *> (&_source);
      
      VEVPMFHSPACE::copystate(&(source->_state), &_state);
      _elasticEne=source->_elasticEne;
      _plasticEne=source->_plasticEne;
      _irreversibleEnergy = source->_irreversibleEnergy;
      _DirreversibleEnergyDF = source->_DirreversibleEnergyDF; 
      
      return *this; 
    };
  
    virtual ~IPVEVPMFH(){
      VEVPMFHSPACE::freestate(_state);     //Destructor of state structure
    };
  
    virtual double get(const int i) const;
    virtual double defoEnergy() const;
    virtual double plasticEnergy() const;

    virtual IPVariable* clone() const {return new IPVEVPMFH(*this);};
    virtual void restart();

    virtual double irreversibleEnergy() const {return _irreversibleEnergy;};
    virtual double& getRefToIrreversibleEnergy() {return _irreversibleEnergy;};  
    virtual const STensor3& getConstRefToDIrreversibleEnergyDF() const{return _DirreversibleEnergyDF;};
    virtual STensor3& getRefToDIrreversibleEnergyDF() {return _DirreversibleEnergyDF;};

    virtual VEVPMFHSPACE::STATE* getSTATE() {return &_state;}
    virtual const VEVPMFHSPACE::STATE* getSTATE() const {return &_state;}
   
};

#endif // IPVEVPMFH_H_