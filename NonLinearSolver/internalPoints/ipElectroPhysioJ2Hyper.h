//
// Description: storing class for j2 elasto-plastic law with electro-physiology interface
// Author:  <A. Jerusalem>, (C) 2013
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef IPELECTROPHYSIOJ2HYPER_H_
#define IPELECTROPHYSIOJ2HYPER_H_
#include "ipJ2linear.h"
#include "ipHardening.h"
#include "ipDamage.h"
#include "ElectroPhysioLaw.h"

class IPElectroPhysioJ2Hyper : public IPJ2linear
{
 protected:
  IPElectroPhysio*  ipvEP;

 protected:
  double _epSurfaceStrain;
  double _epDamageParameter;


 public:
  IPElectroPhysioJ2Hyper();
  IPElectroPhysioJ2Hyper(const J2IsotropicHardening *j2IH, const CLengthLaw *cll, const ElectroPhysioLaw *epl);
  ~IPElectroPhysioJ2Hyper()
  {

    if(ipvEP !=NULL)
    {
      delete ipvEP;
    }
  }


  IPElectroPhysioJ2Hyper(const IPElectroPhysioJ2Hyper &source);
  IPElectroPhysioJ2Hyper& operator=(const IPVariable &source);
  virtual void restart();
  virtual double getCurrentPlasticStrain() const { return _j2lepspbarre;}
  virtual double &getRefToCurrentPlasticStrain() { return _j2lepspbarre;}
  virtual double getCurrentSurfaceStrain() const { return _epSurfaceStrain;}
  virtual double &getRefToCurrentSurfaceStrain() { return _epSurfaceStrain;}
  virtual double getCurrentDamageParameter() const { return _epDamageParameter;}
  virtual double &getRefToCurrentDamageParameter() { return _epDamageParameter;}

  virtual const IPElectroPhysio &getConstRefToIPElectroPhysio() const
  {
    if(ipvEP==NULL)
      Msg::Error("IPElectroPhysioJ2Hyper: ipvEP not initialized");
    return *ipvEP;
  }
  virtual IPElectroPhysio &getRefToIPElectroPhysio()
  {
    if(ipvEP==NULL)
      Msg::Error("IPElectroPhysioJ2Hyper: ipvEP not initialized");
    return *ipvEP;
  }
 virtual double getCaConcentration() const
  {
    if(ipvEP==NULL)
      Msg::Error("IPElectroPhysioJ2Hyper: ipvEP not initialized");
    return ipvEP->getCaConcentration();
  }
 virtual double getNaConcentration() const
  {
    if(ipvEP==NULL)
      Msg::Error("IPElectroPhysioJ2Hyper: ipvEP not initialized");
    return ipvEP->getNaConcentration();
  }
 virtual double getKConcentration() const
  {
    if(ipvEP==NULL)
      Msg::Error("IPElectroPhysioJ2Hyper: ipvEP not initialized");
    return ipvEP->getKConcentration();
  }

  virtual IPVariable* clone() const{return new IPElectroPhysioJ2Hyper(*this);};
};

#endif // IPELECTROPHYSIOJ2HYPER_H_w
