//
// C++ Interface: ipd data
//
// Description: ip data for plastic anisotropy
//
// Author:  <V.D. Nguyen>, (C) 2023
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef IPANISOTROPICPLASTICITY_H_
#define IPANISOTROPICPLASTICITY_H_

#include "ipJ2linear.h"

class IPAnisotropicPlasticity : public IPJ2linear 
{
  protected:
    const STensor3* _F; // pointer to deformation gradient 
    STensor3 _Ep;
    STensor43 _anisotropicMatrix;
    STensor63 _DanisotropicMatrixDF;
    STensor63 _DanisotropicMatrixDEp;
    double _effectiveSVM;
    double _effectivePressure;

  public:
    IPAnisotropicPlasticity(): IPJ2linear(), _F(NULL), _Ep(0), 
                        _anisotropicMatrix(0), _DanisotropicMatrixDF(0.), _DanisotropicMatrixDEp(0.){};
    IPAnisotropicPlasticity(const J2IsotropicHardening *j2IH): IPJ2linear(j2IH), _F(NULL), _Ep(0), 
                        _anisotropicMatrix(0), _DanisotropicMatrixDF(0.), _DanisotropicMatrixDEp(0.),
                        _effectiveSVM(0), _effectivePressure(0){};
    IPAnisotropicPlasticity(const IPAnisotropicPlasticity &source): IPJ2linear(source), _F(NULL), _Ep(source._Ep),
                        _anisotropicMatrix(source._anisotropicMatrix), _DanisotropicMatrixDF(source._DanisotropicMatrixDF), 
                        _DanisotropicMatrixDEp(source._DanisotropicMatrixDEp),_effectiveSVM(source._effectiveSVM),
                        _effectivePressure(source._effectivePressure){};
    virtual IPAnisotropicPlasticity& operator=(const IPVariable &source)
    {
      IPJ2linear::operator =(source);
      _F = NULL;
      const IPAnisotropicPlasticity* psrc = dynamic_cast<const IPAnisotropicPlasticity*>(&source);
      if (psrc != NULL)
      {
        _Ep = psrc->_Ep;
        _anisotropicMatrix = psrc->_anisotropicMatrix;
        _DanisotropicMatrixDF = psrc->_DanisotropicMatrixDF;
        _DanisotropicMatrixDEp = psrc->_DanisotropicMatrixDEp;
        _effectiveSVM = psrc->_effectiveSVM;
        _effectivePressure = psrc->_effectivePressure;
      }
      return *this;
    }
    virtual ~IPAnisotropicPlasticity()
    {
      
    }
     virtual void setRefToDeformationGradient(const STensor3& F)
    {
      _F = &F;
    }
    virtual const STensor3& getConstRefToDeformationGradient() const
    {
      if (_F == NULL) Msg::Error("IPAnisotropicPlasticity::setRefToDeformationGradient must be called before getting value");
      return *_F;
    };
    
    virtual const STensor3& getConstRefToPlasticDeformationTensor() const {return _Ep;}
    virtual STensor3& getRefToPlasticDeformationTensor() {return _Ep;}
    
    virtual const STensor43& getConstRefToAnisotropicTensor() const {return _anisotropicMatrix;};
    virtual STensor43& getRefToAnisotropicTensor() {return _anisotropicMatrix;};
    
    virtual const STensor63& getConstRefToDAnisotropicTensorDF() const {return _DanisotropicMatrixDF;};
    virtual STensor63& getRefToDAnisotropicTensorDF() {return _DanisotropicMatrixDF;};
    
    virtual const STensor63& getConstRefToDAnisotropicTensorDEp() const {return _DanisotropicMatrixDEp;};
    virtual STensor63& getRefToDAnisotropicTensorDEp() {return _DanisotropicMatrixDEp;};
    
    virtual double getConstRefToEffectiveSVM() const {return _effectiveSVM;}
    virtual double& getRefToEffectiveSVM() {return _effectiveSVM;}
    
    virtual double getConstRefToEffectivePressure() const {return _effectivePressure;}
    virtual double& getRefToEffectivePressure() {return _effectivePressure;}
    
    virtual void restart()
    {
      IPJ2linear::restart();
      _F = NULL;
      restartManager::restart(_Ep);
      restartManager::restart(_anisotropicMatrix);
      restartManager::restart(_DanisotropicMatrixDF);
      restartManager::restart(_DanisotropicMatrixDEp);
      restartManager::restart(_effectiveSVM);
      restartManager::restart(_effectivePressure);
    }
    
    virtual IPVariable* clone() const {return new IPAnisotropicPlasticity(*this);};
};


#endif //IPANISOTROPICPLASTICITY_H_