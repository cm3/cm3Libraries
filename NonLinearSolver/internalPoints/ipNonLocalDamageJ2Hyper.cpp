//
// Description: storing class for j2 elasto-plastic law with non-local damage
//
//
// Author:  <L. Noels>, (C) 2014
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "ipNonLocalDamageJ2Hyper.h"
#include "restartManager.h"

IPLocalDamageJ2Hyper::IPLocalDamageJ2Hyper() : IPJ2linear(), _damageEnergy(0.)
{
  Msg::Error("IPNonLocalDamageJ2Hyper::IPNonLocalDamageJ2Hyper is not initialized with a damage IP Variable nor a IP CLength");
ipvDam=NULL;

};

IPLocalDamageJ2Hyper::IPLocalDamageJ2Hyper(const J2IsotropicHardening *j2IH,
                                                 const DamageLaw *daml) :
                                                    IPJ2linear(j2IH), _damageEnergy(0.)
{

  ipvDam=NULL;
  if(daml ==NULL) Msg::Error("IPNonLocalDamageJ2Hyper::IPNonLocalDamageJ2Hyper has no daml");
  daml->createIPVariable(ipvDam);

};




IPLocalDamageJ2Hyper::IPLocalDamageJ2Hyper(const IPLocalDamageJ2Hyper &source) : IPJ2linear(source),_damageEnergy(source._damageEnergy)
{
  ipvDam = NULL;
  if(source.ipvDam != NULL)
  {
    ipvDam = dynamic_cast<IPDamage*>(source.ipvDam->clone());
  }
}
IPLocalDamageJ2Hyper& IPLocalDamageJ2Hyper::operator=(const IPVariable &source)
{
  IPJ2linear::operator=(source);
  const IPLocalDamageJ2Hyper* src = dynamic_cast<const IPLocalDamageJ2Hyper*>(&source);
  if(src != NULL)
  {
    _damageEnergy = src->_damageEnergy;
    if(src->ipvDam != NULL)
    {
			if (ipvDam != NULL)
				ipvDam->operator=(*dynamic_cast<const IPVariable*>(src->ipvDam));
			else
				ipvDam= dynamic_cast<IPDamage*>(src->ipvDam->clone());
    }
		else{
			if(ipvDam != NULL) delete ipvDam; ipvDam = NULL;
		}
  }
  return *this;
}

void IPLocalDamageJ2Hyper::restart()
{
  IPJ2linear::restart();
  restartManager::restart(ipvDam);
  restartManager::restart(_damageEnergy);
  return;
}



IPNonLocalDamageJ2Hyper::IPNonLocalDamageJ2Hyper() : IPLocalDamageJ2Hyper(),
							_nonlocalPlasticStrain (0), nonLocalToLocal(false),_DirreversibleEnergyDNonLocalVariable(0.)
{
  Msg::Error("IPNonLocalDamageJ2Hyper::IPNonLocalDamageJ2Hyper is not initialized with a Characteritsic IP Variable nor a IP CLength");
  ipvCL=NULL;

};

IPNonLocalDamageJ2Hyper::IPNonLocalDamageJ2Hyper(const J2IsotropicHardening *j2IH,
                                                 const CLengthLaw *cll, const DamageLaw *daml) :
                                                    IPLocalDamageJ2Hyper(j2IH,daml),
						_nonlocalPlasticStrain (0),nonLocalToLocal(false),
						_DirreversibleEnergyDNonLocalVariable(0.)
{
  ipvCL=NULL;
  if(cll ==NULL) Msg::Error("IPNonLocalDamageJ2Hyper::IPNonLocalDamageJ2Hyper has no cll");
  cll->createIPVariable(ipvCL);
};




IPNonLocalDamageJ2Hyper::IPNonLocalDamageJ2Hyper(const IPNonLocalDamageJ2Hyper &source) : IPLocalDamageJ2Hyper(source)
{
  _nonlocalPlasticStrain=source._nonlocalPlasticStrain;
  nonLocalToLocal = source.nonLocalToLocal;
	_DirreversibleEnergyDNonLocalVariable = source._DirreversibleEnergyDNonLocalVariable;
	ipvCL = NULL;
  if(source.ipvCL != NULL)
  {
    ipvCL = dynamic_cast<IPCLength*>(source.ipvCL->clone());
  }
}
IPNonLocalDamageJ2Hyper& IPNonLocalDamageJ2Hyper::operator=(const IPVariable &source)
{
  IPLocalDamageJ2Hyper::operator=(source);
  const IPNonLocalDamageJ2Hyper* src = dynamic_cast<const IPNonLocalDamageJ2Hyper*>(&source);
  if(src != NULL)
  {
    _nonlocalPlasticStrain=src->_nonlocalPlasticStrain;
    nonLocalToLocal = src->nonLocalToLocal;
		_DirreversibleEnergyDNonLocalVariable = src->_DirreversibleEnergyDNonLocalVariable;
    
    if(src->ipvCL != NULL)
    {
			if (ipvCL != NULL)
				ipvCL->operator=(*dynamic_cast<const IPCLength*>(src->ipvCL));
			else
				ipvCL=dynamic_cast<IPCLength*>(src->ipvCL->clone());
    }
		else{
			if(ipvCL != NULL) delete ipvCL; ipvCL = NULL;
		}
  }
  return *this;
}

void IPNonLocalDamageJ2Hyper::restart()
{
  IPLocalDamageJ2Hyper::restart();
  restartManager::restart(_nonlocalPlasticStrain);
  restartManager::restart(ipvCL);
  restartManager::restart(nonLocalToLocal);
	restartManager::restart(_DirreversibleEnergyDNonLocalVariable);
  return;
}


