//
// Description: storing class for Electro Thermo Mechanics
//
//
// Author:  <Lina Homsy>, (C) 2015
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "ipAnIsotropicElecTherMech.h"

IPAnIsotropicElecTherMech::IPAnIsotropicElecTherMech() :IPAnIsotropicTherMech(), _ElecThermEnergy(0.)
{

};
IPAnIsotropicElecTherMech::IPAnIsotropicElecTherMech(const IPAnIsotropicElecTherMech &source) :  IPAnIsotropicTherMech(source), _ElecThermEnergy(source._ElecThermEnergy)
{
}
IPAnIsotropicElecTherMech& IPAnIsotropicElecTherMech::operator=(const IPVariable &source)
{
  IPAnIsotropicTherMech::operator=(source);
  const IPAnIsotropicElecTherMech* src = static_cast<const IPAnIsotropicElecTherMech*>(&source);
  if(src)
  {
    _ElecThermEnergy=src->_ElecThermEnergy;
  }
  return *this;
}

void IPAnIsotropicElecTherMech::restart()
{
  IPAnIsotropicTherMech::restart();
  restartManager::restart(_ElecThermEnergy);
}

