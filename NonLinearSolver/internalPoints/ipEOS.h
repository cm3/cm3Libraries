//
// Description: storing class for EOS
//
//
// Author:  <Dongli Li, Antoine Jerusalem>, (C) 2014
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef IPEOS_H_
#define IPEOS_H_
#include "ipvariable.h"
#include "STensor3.h"


class IPEOS : public IPVariableMechanics
{

public:

    IPVariable* _IPV;
    double _elementSize;

public:

    IPEOS(double elementSize);
    IPEOS(double elementSize, IPVariable *IPV);

    IPEOS(const IPEOS &source);
    IPEOS& operator=(const IPVariable &source);
    ~IPEOS()
    {
        if(_IPV!=NULL)
        {
            delete _IPV;
        }
    }
    virtual IPVariable* clone() const {return new IPEOS(*this);};

    virtual void restart();
};

#endif // IPEOS_H_

