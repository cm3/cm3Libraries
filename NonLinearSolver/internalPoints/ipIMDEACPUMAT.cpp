//
// Description: storing class for cp umat interface
// Author:  <V.D. NGUYEN>, (C) 2023
//
// Copyright: See COPYING file that comes with this distribution
//
//

#include "ipIMDEACPUMAT.h"
#include "ipField.h"
#include "restartManager.h"


double IPIMDEACPUMAT::get(int comp) const
{
  // for vizualization, with tag in ipField.h (only variables)
  //if(comp==IPField::PLASTICSTRAIN)
  //  return _statev[18];
  //else
    return IPUMATInterface::get(comp);
}

void IPIMDEACPUMAT::restart()
{
  IPUMATInterface::restart();
 return;
}
