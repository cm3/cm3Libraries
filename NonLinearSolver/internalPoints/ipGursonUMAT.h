//
// Description: storing class for gurson umat interface 
// Author:  <L. NOELS>, (C) 2019
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef IPGURSONUMAT_H_
#define IPGURSONUMAT_H_
#include "ipUMATInterface.h"
#include "STensor3.h"
class IPGursonUMAT : public IPUMATInterface
{
 protected:

 public: // All data public to avoid the creation of function to access

 public:
  IPGursonUMAT(int _nsdv, double eleSize) : IPUMATInterface(_nsdv, eleSize) 
    {
    }
  IPGursonUMAT(const IPGursonUMAT &source) : IPUMATInterface(source)
    {
    }
  IPGursonUMAT &operator = (const IPVariable &_source)
  {
      IPUMATInterface::operator=(_source);
      return *this;
  }
  virtual ~IPGursonUMAT()
  {
  }
  virtual IPVariable* clone() const {return new IPUMATInterface(*this);};
  virtual void restart();
  virtual double get(const int i) const;
  virtual void   setFv(double f) { _statev[0]=f;}  
  virtual double getFv() const { return _statev[0];}  
  virtual void   setMatrixEps(double epsm) { _statev[1]=epsm;}  
  virtual double getMatrixEps() const { return _statev[1];}  
  virtual void   setMacroVolEps(double epsm) { _statev[2]=epsm;}  
  virtual double getMacroVolEps() const { return _statev[2];}  
  virtual void   setMacroDevEps(double epsm) { _statev[3]=epsm;}  
  virtual double getMacroDevEps() const { return _statev[3];}
  virtual void   setThomasonFlag(bool fl) { _statev[4]=(double)fl;}  
  virtual bool getThomasonFlag() const { return (bool)_statev[4];}
  virtual void   setLigament(double chi) { _statev[5]=chi;}  
  virtual double getLigament() const { return _statev[5];}

  virtual void   setFp(double fp, int i, int j) { _statev[6+(i)*3+j]=fp;}  
  virtual double getFp(int i, int j) const { return _statev[6+(i)*3+j];}

 protected:
  IPGursonUMAT(){}
};

#endif // IPGURSONUMAT_H_
