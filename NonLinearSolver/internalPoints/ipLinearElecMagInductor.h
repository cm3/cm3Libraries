//
// Description: storing class for inductor region in ElecMag problem
// computing normal direction to a gauss point to impose current
// density js0 in inductor
//
// Author:  <Vinayak GHOLAP>, (C) 2020
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef IPLINEARELECMAGINDUCTOR_H_
#define IPLINEARELECMAGINDUCTOR_H_
#include "ipLinearElecMagTherMech.h"
#include "SVector3.h"
class IPLinearElecMagInductor : public IPLinearElecMagTherMech
{
protected:
  SVector3 _normal;
public:
  IPLinearElecMagInductor(const SVector3 &GaussP, const SVector3 &Centroid, const SVector3 &CentralAxis);
  IPLinearElecMagInductor(const IPLinearElecMagInductor &source);
  IPLinearElecMagInductor& operator=(const IPVariable &source);
  virtual void getNormalDirection(SVector3 &normal) const { normal =_normal;}
  virtual void restart();
  virtual IPVariable* clone() const {return new IPLinearElecMagInductor(*this);}
  virtual double defoEnergy() const;
  virtual double get(const int comp) const;
  virtual double & getRef(const int comp);
private:
  IPLinearElecMagInductor();
};

#endif // IPLINEARELECMAGINDUCTOR_H_