#ifndef MATRIX_OPERATIONS_C
#define MATRIX_OPERATIONS_C 1

#include "matrix_operations.h"
#include "rotations.h"
#include <stdio.h>
#include <stdlib.h>
#include <math.h>


//******************************************************
//**** caculate eigenvalue e, eigenvector u and the derivatives of eigenvaule to the eoriginal matrix dedS
//**** with jacobian iteration, S is symetric real matrix
//**************************************************************************************

int Jacobian_Eigen( int n, double** input, double* e, double** U, double*** dedS)
{

     int i,j,k,l;
     int col, row;
     double t, c, s, theta;
     double max, norm_S;
     double Tol = 1.0e-14;
     int iterm;
     int itmax = 50;

     static double **R = NULL; 
     static double **temp = NULL;
     static double **S=NULL;
     static int oldn=0;
    
     if(n!=oldn)
     {
        if(R!=NULL)
        {
          freematrix(R,n);
          freematrix(temp,n);
          freematrix(S,n);
        }
        mallocmatrix(&R,n,n);
        mallocmatrix(&S,n,n);
        mallocmatrix(&temp,n,n);
        oldn=n;
     }
     for (i=0; i<n; i++ ){
       for (j=0; j<n; j++ ){
         R[i][j]=0.;
         S[i][j]=input[i][j];
         temp[i][j]=0.;
       }
     }

     for (i=0; i<n; i++ ){
           U[i][i] = 1.0;
           for (j=i+1; j<n; j++ ){
                U[i][j] = U[j][i]=0.;
           }
     }

     norm_S = 0.0;
     for (i=0; i<n; i++ ){
          for (j=0; j<n; j++ ){
                norm_S = norm_S + S[i][j]*S[i][j];
          }
     }
     norm_S = sqrt(norm_S);

     iterm = 0;

     max = 1.0;
     while( max > Tol * norm_S && iterm < itmax )
     {
        iterm = iterm + 1;
//*** find the maximum abs(component) which is not on diagonal
        row = col = 0;
        max = 0.0;
        for (i=0; i<n; i++ ){
             for (j=i+1; j<n; j++ ){
                 if(fabs(S[i][j]) > max){
                    row = i;
                    col = j;
                    max = fabs(S[i][j]);
                 }
             }
        }

        if (row != col){

             // computer rotation matrix

              theta=(S[col][col]-S[row][row])/(2.0*S[row][col]);

              t = 1.0/(fabs(theta)+sqrt(theta*theta+1.0));

              if (theta<0.) t = -t;

              c = 1.0/sqrt(t*t + 1.0);
              s = c*t;

              for (i=0; i<n; i++ ){
                   R[i][i]=1.0;
                   for (j=i+1; j<n; j++ ){
                        R[i][j] = R[j][i] = 0.0;
                   }
              }

               R[row][row] = R[col][col] = c;
               R[row][col] = s;
               R[col][row] = -s;

               for (i=0; i<n; i++){
                    for (j=0; j<n; j++){
                         temp[i][j] = 0.0;
                          for (k=0; k<n; k++){
                               for (l=0; l<n; l++){
                                        temp[i][j] = temp[i][j] + R[k][i]*S[k][l]*R[l][j];
                               }
                          }
                    }
               }

               for (i=0; i<n; i++){
                    for (j=0; j<n; j++){
                         S[i][j] = temp[i][j];
                         temp[i][j] = 0.0;
                         for (k=0; k<n; k++){
                              temp[i][j] = temp[i][j] + U[i][k]*R[k][j];
                         }
                    }
               }

               for (i=0; i<n; i++){
                    for (j=0; j<n; j++){
                        U[i][j] = temp[i][j];
                    }
               }

          }
     }

       if (max <= Tol * norm_S){
              for (int i=0; i<n; i++) e[i] = S[i][i];
       }
       else{
              printf("iteration dosen't converge after maximum iterations\n");
              return 0;
       }

       //compute derivatives dedS

        for (i=0; i<n; i++ ){
            for (j=0; j<n; j++ ){
                for (k=0; k<n; k++ ){
                     dedS[i][j][k]=U[j][i]*U[k][i];
                }
            }
       }

      // freematrix(R,n);
      // freematrix(temp,n);

      return 0;
  
}


//*************************************************************************************
//**** caculate eigenvalue e, eigenvector u %% I removed the computation of derivatives
//**** with jacobian iteration, S is symetric real matrix
//**************************************************************************************
int Jacobian_Eigen( int n, double** input, double* e, double** U)
{
    
    int i,j,k,l;
    int col, row;
    double t, c, s, theta;
    double max, norm_S;
    double Tol = 1.0e-14;
    int iterm;
    int itmax = 50;
    
    static double **R = NULL;
    static double **S=NULL;
    static double **temp = NULL;
    static int oldn=0;
    
    if(n!=oldn)
    {
        if(R!=NULL)
        {
          freematrix(R,n);
          freematrix(S,n);
          freematrix(temp,n);
        }
        mallocmatrix(&R,n,n);
        mallocmatrix(&S,n,n);
        mallocmatrix(&temp,n,n);
        oldn=n;
    }
    for (i=0; i<n; i++ ){
      for (j=0; j<n; j++ ){
        R[i][j]=0.;
        temp[i][j]=0.;
        S[i][j]=input[i][j];
      }
    }

    for (i=0; i<n; i++ ){
        U[i][i] = 1.0;
        for (j=i+1; j<n; j++ ){
            U[i][j] = U[j][i]=0.;
        }
    }
    
    norm_S = 0.0;
    for (i=0; i<n; i++ ){
        for (j=0; j<n; j++ ){
            norm_S = norm_S + S[i][j]*S[i][j];
        }
    }
    norm_S = sqrt(norm_S);
    
    iterm = 0;
    
    max = 1.0;
    while( max > Tol * norm_S && iterm < itmax )
    {
        iterm = iterm + 1;
        //*** find the maximum abs(component) which is not on diagonal
        row = col = 0;
        max = 0.0;
        for (i=0; i<n; i++ ){
            for (j=i+1; j<n; j++ ){
                if(fabs(S[i][j]) > max){
                    row = i;
                    col = j;
                    max = fabs(S[i][j]);
                }
            }
        }
        
        if (row != col){
            
            // computer rotation matrix
            
            theta=(S[col][col]-S[row][row])/(2.0*S[row][col]);
            
            t = 1.0/(fabs(theta)+sqrt(theta*theta+1.0));
            
            if (theta<0.) t = -t;
            
            c = 1.0/sqrt(t*t + 1.0);
            s = c*t;
            
            for (i=0; i<n; i++ ){
                R[i][i]=1.0;
                for (j=i+1; j<n; j++ ){
                    R[i][j] = R[j][i] = 0.0;
                }
            }
            
            R[row][row] = R[col][col] = c;
            R[row][col] = s;
            R[col][row] = -s;
            
            for (i=0; i<n; i++){
                for (j=0; j<n; j++){
                    temp[i][j] = 0.0;
                    for (k=0; k<n; k++){
                        for (l=0; l<n; l++){
                            temp[i][j] = temp[i][j] + R[k][i]*S[k][l]*R[l][j];
                        }
                    }
                }
            }
            
            for (i=0; i<n; i++){
                for (j=0; j<n; j++){
                    S[i][j] = temp[i][j];
                    temp[i][j] = 0.0;
                    for (k=0; k<n; k++){
                        temp[i][j] = temp[i][j] + U[i][k]*R[k][j];
                    }
                }
            }
            
            for (i=0; i<n; i++){
                for (j=0; j<n; j++){
                    U[i][j] = temp[i][j];
                }
            }
            
        }
    }
    
    if (max <= Tol * norm_S){
        for (int i=0; i<n; i++) e[i] = S[i][i];
    }
    else{
        printf("iteration dosen't converge after maximum iterations\n");
        return 0;
    }
    
      // freematrix(R,n);
    // freematrix(temp,n);
    
    return 0;
    
}

/*****************************************
//    convert vector to matrix      ******
// n- dimension of matrix
/*****************************************/

void vect_to_matrix (int n, double* A, double** M)
{    
    int i, j, k;
    k=0;
    for (i=0; i<n; i++){
        for (j=0; j<n; j++){
             M[i][j] = A[k];
             k +=1;
         }
    }

}


/*****************************************
/*rotation of strain/stress vector*/
// strain: (e11,e22,e33,sq2*e12,sq2*e13,sq2*e23)
// stress: (s11,s22,s33,sq2*s12,sq2*s13,sq2*s23)
/*****************************************/
void Rot_vect(double *R, double*V)
{
    int i;
    double sq2 = sqrt(2.);
    static double** M= NULL; //matrix form
    static double** dR= NULL; //matrix form
    static double** C= NULL; //matrix form
    
    if(M==NULL) 
    {
        mallocmatrix(&M,3,3);
        mallocmatrix(&dR,3,3);
        mallocmatrix(&C,3,3);
    }
    vect_to_matrix(3, R, dR);
    for(i=0;i<3;i++) M[i][i] = V[i];
    M[0][1] = V[3]/sq2;
    M[0][2] = V[4]/sq2;
    M[1][2] = V[5]/sq2;
    M[1][0] = M[0][1];
    M[2][0] = M[0][2];
    M[2][1] = M[1][2];

    contraction1(dR, M, C);
    transpose(dR,M,3,3);
    contraction1(C, M, dR);

    for(i=0;i<3;i++) V[i] = dR[i][i];
    V[3] = dR[0][1]*sq2;
    V[4] = dR[0][2]*sq2;
    V[5] = dR[1][2]*sq2;
}
/*****************************************
/*      convert matrix to vector ******
/*****************************************/
void matrix_to_vect (int n, double** M, double* A)
{
    int i, j, k;
    k=0;
    for (i=0; i<n; i++){
        for (j=0; j<n; j++){
             A[k] = M[i][j];
             k +=1;
         }
    }

}

/* ********************************************
// ** SUM OF TWO TENSORS OF THE SECOND ORDER **
// ** EACH ONE CAN BE PREVIOUSLY BY A SCALAR **
// ********************************************/

void addtens2 (double a, double *A, double b, double *B, double *res)
{
  int i;

  if (fabs(b) > 1e-12) {                /* b!=0 */
    for (i=0; i<6; i++) {
      res[i] = a*A[i] + b*B[i];
    }
  }

  else {                      /* b==0 */
    for (i=0; i<6; i++) {
      res[i] = a*A[i];
    }
  }

}

/* ********************************************
// ** SUM OF TWO TENSORS OF THE FOURTH ORDER **
// ** EACH ONE CAN BE PREVIOUSLY BY A SCALAR **
// ********************************************/

void addtens4 (double a, double **A, double b, double **B, double **res)
{
  int i,j;

  if (fabs(b)>1e-12) {      /* b != 0 */
 
   for (i=0; i<6;i++) {
      for (j=0; j<6; j++) {
				res[i][j] = a*A[i][j] + b*B[i][j];
      }
    }
  }

  else {           /* b == 0 */

    for (i=0; i<6;i++) {
      for (j=0; j<6; j++) {
				res[i][j] = a*A[i][j];
      }
    }
  }
  
}

/* ********************************************
// ** SUM OF TWO TENSORS OF THE SIXTH ORDER **
// ** EACH ONE CAN BE PREVIOUSLY BY A SCALAR **
// ********************************************/

void addtens6 (double a, double ***A, double b, double ***B, double ***res)
{
  int i,j,k;

  if (fabs(b)>1e-12) {      /* b != 0 */
 
   for (i=0; i<6;i++) {
      for (j=0; j<6; j++) {
				for(k=0;k<6;k++) {
					res[i][j][k] = a*A[i][j][k] + b*B[i][j][k];
				}      
			}
    }
  }

  else {           /* b == 0 */

    for (i=0;i<6;i++) {
      for (j=0;j<6; j++) {
				for(k=0;k<6;k++){
					res[i][j][k] = a*A[i][j][k];
				}      
			}
    }
  }
  
}

/* ************************************************************
 // ** Contraction  DE DEUX TENSEURS D'ORDRE 2 sous forme matricielle **
 // **  RENVOIE UN TENSEUR D'ORDRE 2 SOUS FORME MATRICIELLE   **
 // ***********************************************************/
void contraction1 (double**A, double** B, double** C)

{
    int i,j;
    
    for (i=0; i<3; i++) {
        for (j=0; j<3; j++) {
            C[i][j] = A[i][0]*B[0][j]+ A[i][1]*B[1][j]+ A[i][2]*B[2][j];
        };
    };
    
}

/* *************************************************************      
// ** CONTRACTION D'UN TENSEUR D'ORDRE 2 (mis sous forme 6*1) **
// **      ET D'UN TENSEUR D'ORDRE 2 (sous forme 6*1)         **
// **  POUR DONNER UN SCALAIRE   **
// ************************************************************/

double contraction22(double* a, double* b){

	int i;
	double tmp=0;
	for(i=0;i<6;i++){
		tmp += a[i]*b[i];
	}
	return tmp;

}


/* *************************************************************      
// ** CONTRACTION D'UN TENSEUR D'ORDRE 4 (mis sous forme 6*6) **
// **      ET D'UN TENSEUR D'ORDRE 2 (sous forme 6*1)         **
// **  POUR DONNER UN TENSEUR D'ORDRE 2 (mis sous forme 6*1)  **
// ************************************************************/

void contraction42 (double **T4, double *T2, double *Sol)

{

  int i,j;
  
  for (i=0; i<6; i++) {
    Sol[i] = 0.;
  }
  for(i=0;i<6;i++){
    for (j=0; j<6; j++) {
      Sol[i] = Sol[i] + T4[i][j] * T2[j];
    }
  }
  
}

/* ************************************************************** 
// ** CALCUL DE LA CONTRACTION DE DEUX TENSEURS D'ORDRE 4(6*6) **
// **  RENVOIE UN TENSEUR D'ORDRE 4 SOUS FORME 6*6             **
// **************************************************************/

void contraction44 (double **TA, double **TB, double **Tres)
{
  int i,j,k;

  for (i=0; i<6; i++) {

    for (j=0; j<6; j++) {
      Tres[i][j] = 0;

      for (k=0;k<6;k++) {
        Tres[i][j] = Tres[i][j] + TA[i][k]*TB[k][j];
      }
    }
  }

}


/* ***************************************************************
 * **  CALCULE LA CONTRACTION D'UN TENSEUR D'ORDRE 6 (6*6*6)    **
   **  AVEC UN TENSEUR D'ORDRE 2 (6*1)                          **
   ***************************************************************/
/** INPUT: T6: a sixth-order tensor    
 *         T2: a second-order tensor
 *         idx: index on which contraction must be preformed
 *  OUTPUT: Tres: a second-order tensor                        */
 
void contraction62 (double*** T6, double* T2, double**Tres, int idx)
{
   int i,j,k;
   

   for(i=0;i<6;i++){
      for(j=0;j<6;j++){
	 Tres[i][j]=0.;
      }
   }

   switch(idx){

     case 1:
       for(i=0;i<6;i++){
          for(j=0;j<6;j++){
             for(k=0;k<6;k++){
                Tres[i][j] += T6[k][i][j]*T2[k];
	     }
          }
       }
     break;
     case 2:
       for(i=0;i<6;i++){
          for(j=0;j<6;j++){
             for(k=0;k<6;k++){
                Tres[i][j] += T6[i][k][j]*T2[k];
	     }
          }
       }

     break;
     case 3:
       for(i=0;i<6;i++){
          for(j=0;j<6;j++){
             for(k=0;k<6;k++){
                Tres[i][j] += T6[i][j][k]*T2[k];
	     }
          }
       }

     break;
    }

}


/* ************************************************************ 
// ** CALCUL DU PRODUIT TENSORIEL DE DEUX TENSEURS D'ORDRE 2 **
// **  RENVOIE UN TENSEUR D'ORDRE 4 SOUS FORME MATRICIELLE   **
// ***********************************************************/

void produit_tensoriel (double *TA, double *TB, double **Tres)

{
  int i,j;

  for (i=0; i<6; i++) {
    for (j=0; j<6; j++) {
      Tres[i][j] = TA[i]*TB[j];
    };
  };
  
}


/*********************************
 * TRANSPOSE UNE MATRICE MxN       **
 * ******************************/
void transpose(double** Tinit, double** Ttrans, int m, int n){

	int i,j;
	double tmp;
	for(i=0;i<m;i++){
		Ttrans[i][i]=Tinit[i][i];
		for(j=i+1;j<n;j++){
	    tmp = Tinit[i][j];
			Ttrans[i][j]=Tinit[j][i];
			Ttrans[j][i]=tmp;
		}
	}
	

}

/*********************************************/
/** Compute the von Mises equivalent stress **/
/*********************************************/

/* October 2003 */
/* New prototype in May 2004 */

double vonmises(double *tens2)
{
  return(pow(0.5*(pow(tens2[0]-tens2[1],2.) + pow(tens2[1]-tens2[2],2.) + pow(tens2[2]-tens2[0],2.)) + 3.*pow(tens2[3]/sqrt(2.),2.) + 3.*pow(tens2[4]/sqrt(2.),2.) + 3.*pow(tens2[5]/sqrt(2.),2.),0.5));

}

/******************************************/
/** Compute the equivalent strain scalar **/
/******************************************/

/* November 2005 */

double eqstrn(double *tens2)
{
  return(pow(2./9.*(pow(tens2[0]-tens2[1],2.) + pow(tens2[1]-tens2[2],2.) + pow(tens2[2]-tens2[0],2.)) + 4./3.*pow(tens2[3]/sqrt(2.),2.) + 4./3.*pow(tens2[4]/sqrt(2.),2.) + 4./3.*pow(tens2[5]/sqrt(2.),2.),0.5));

}


/*******************************************************************************
// ** COMPUTE THE ISOTROPIC HOOKE'S ELASTIC OPERATOR FROM E AND NU **
********************************************************************************/

/* June 2003
// INPUT : E : Elastic young's modulus in the longitudinal direction (same in other directions if isotropic)
//         nu : Poisson's ratio in the longitudinal direction (same in other directions if isotropic)
// OUTPUT : Isotropic stiffness tensor stored as in book of ID p.555
*/

void compute_Hooke(double E, double nu, double **C)

{
  double lambda;
  double mu;
  int i,j;

  if(nu==0.5) {
    printf("nu = 0.5, Impossible to compute lambda\n");
    exit(0);
  }

  mu = E/(2.*(1.+nu));
  lambda = (E*nu)/((1.-2.*nu)*(1.+nu));

  for(i=0;i<6;i++) {
    for(j=0;j<6;j++) {
      C[i][j]=0.;
    }
  }

  for (i=0;i<3;i++) {
    for(j=0;j<3;j++) {
      C[i][j] = lambda;
    }
  }
     
  for (i=0;i<6;i++) {
    C[i][i] = C[i][i] + 2.*mu; /*Doghri's convention!!*/
  }
}


/*******************************************************************************
// ** COMPUTE THE ANISOTROPIC ELASTIC OPERATOR FROM EX,NUXX and GXX   **
********************************************************************************/

/* Sept 2011,  by Ling Wu
// INPUT : anpr : E1, E2,E3,nu12,nu13,nu23,G12,G13,G23
//         Euler angles for global to local coordinates
// OUTPUT : Anisotropic stiffness tensor in global coordinates (see book of ID p.555)
*/

void compute_AnisoOperator(double *anpr, double *euler, double **C)

{
  double Gama;
  double E1, E2, E3;
  double nu12, nu13, nu23;
  double nu21, nu31, nu32;
  int i,j;

  static double** R66;
  static double** C_L;   // elastic operator in local coordinate
  static bool initialized = false;
  if(!initialized){
    mallocmatrix(&C_L,6,6);
    mallocmatrix(&R66,6,6);
    initialized = true;
  }
  cleartens4(C_L);
  cleartens4(R66);

  E1 = anpr[0];
  E2 = anpr[1];
  E3 = anpr[2];

  nu12 = anpr[3];
  nu13 = anpr[4];
  nu23 = anpr[5];

  nu21 = nu12*E2/E1;
  nu31 = nu13*E3/E1;
  nu32 = nu23*E3/E2;

  Gama = 1.0/(1.0-nu12*nu21-nu23*nu32-nu31*nu13-2.0*nu21*nu32*nu13);


  C_L[0][0] = E1*(1.0-nu23*nu32)*Gama;
  C_L[1][1] = E2*(1.0-nu13*nu31)*Gama;
  C_L[2][2] = E3*(1.0-nu12*nu21)*Gama;

  C_L[0][1] = E1*(nu21+nu31*nu23)*Gama;
  C_L[0][2] = E1*(nu31+nu21*nu32)*Gama;
  C_L[1][2] = E2*(nu32+nu12*nu31)*Gama;

  C_L[1][0] = C_L[0][1];
  C_L[2][0] = C_L[0][2];
  C_L[2][1] = C_L[1][2];

  C_L[3][3] = 2.0*anpr[6];   // the order for shear is '12 13 23'
  C_L[4][4] = 2.0*anpr[7];
  C_L[5][5] = 2.0*anpr[8];

  eul_mat(euler,R66);
  transpose(R66,R66,6,6); 			//transpose rotation matrix for inverse transformation
  rot4(R66,C_L,C);				//express elastic operator in global axes

  //freematrix(C_L,6);
  //freematrix(R66,6);
  
}

/*******************************************************************
// ** MAKE AN ISOTROPIC TENSOR FROM A BULK and SHEAR MODULI   ******
********************************************************************/
//  INTPUT: kappa: a bulk modulus
//	    mu: a shear modulus 
//    OUtPUT: Ciso: an isotropic fourth-order tensor (6x6) 
 
void makeiso(double kappa, double mu, double** Ciso)
{
  int i,j;

  for(i=0;i<6;i++){
		for(j=0;j<6;j++){
			Ciso[i][j]=0.;
		}
	}

	for(i=0;i<3;i++){
		for(j=0;j<3;j++){
			Ciso[i][j] = kappa - (2.*mu/3.);
		}
		Ciso[i][i]+= 2.*mu;
		Ciso[i+3][i+3] = 2.*mu;
	}

  
}

/* ******************************************************************************       
// ** EXTRACTION OF THE ISOTROPIC PART OF A FOURTH ORDER TENSOR: GENERAL METHOD **
// *******************************************************************************/
/*
 * "extractiso": Extract the isotropic part of a fourth order tensor.
 * Original: Olivier Pierard (April 2003)
 * Modified: Laurence Brassart (December 2008)
 * INPUT : A fourth order tensor (6*6)
 * OUTPUT : An isotropic fourth order tensor
 */

void extractiso(double** Cani, double** Ciso ){
		
	int i,j;
	double troisk, deuxmu;
	static double **Idev=NULL;
        static double **Ivol=NULL;

        if(Idev==NULL)
        {
  	  mallocmatrix(&Idev,6,6);
	  mallocmatrix(&Ivol,6,6);
        }
        cleartens4(Idev);
        cleartens4(Ivol);
	troisk=0.;
	deuxmu=0.;
	for(i=0;i<3;i++){
		deuxmu += Cani[i][i];
		deuxmu += Cani[i+3][i+3];
		Idev[i][i]=1.;
		Idev[i+3][i+3]=1.;
		for(j=0;j<3;j++){
			troisk += Cani[i][j];
			Ivol[i][j]=1./3.;
			Idev[i][j]=Idev[i][j]-1./3.;
		}
	}
	troisk = troisk/3.;
	deuxmu = (deuxmu - troisk)/5.;
	
	addtens4(troisk,Ivol,deuxmu,Idev,Ciso);

	//freematrix(Idev,6);
	//freematrix(Ivol,6);

}
/******************************/
/* Copy a vector into another */
/******************************/

/* November 2003 */

void copyvect(double *A, double *B, int size)
{
  int i;

  for (i=0; i<size; i++) {
    B[i] = A[i];
  }
}

/******************************/
/* Copy a matrix into another */
/******************************/

/* November 2003 */

void copymatr(double **A, double **B, int size1, int size2)
{
  int i,j;

  for (i=0; i<size1; i++) {
    for (j=0; j<size2; j++) {
      B[i][j] = A[i][j];
      }
  }
}

/************************************************/
/** Compute the trace of a second order tensor **/
/************************************************/

/* October 2003 */
/* New prototype in May 2004 */

double trace(double *tens2)
{
  int i;
  double tr=0.0;

  for (i=0;i<3;i++) {
    tr = tr + tens2[i];
  }
  return (tr);
}

/**********************************************************/
/** Extract the deviatoric part of a second order tensor **/
/**********************************************************/


/* October 2003 */

void dev(double *tens2, double *dev_tens2)
{
  int i;
  double tr_tens2=0.0;

  for (i=0;i<3;i++) {
    tr_tens2 = tr_tens2 + tens2[i];
  }

  for (i=0;i<3;i++) {
    dev_tens2[i] = tens2[i] - tr_tens2/3.0;
  }

  for (i=3;i<6;i++) {
    dev_tens2[i] = tens2[i];
  }

}


/* *************************************************************       
// ** DECOMPOSITION LU D'UNE MATRICE CAREE D'ORDRE QUELCONQUE **
// *************************************************************/
/*
 * "ludcmp": computes the LU decomposition of a square matrix.
 * Original: Olivier Pierard (april 2003)
 * Source : Numerical recipies in C pp44-50(available from internet)
 * Some modifications about indices were rapported
 */

#define TINY 1.0e-14

void ludcmp(double **a, int n, int *indx, double *d, int *error)
     /* Given a matrix a(n*n), this routine replaces it by the LU decomposition of a rows permutation 
itself.  a and n are input.  a is output containing L and U; indx[1..n] si an output vector that records 
the row permutation effected y the partial pivoting, d is outpus as +/-1 depending on whether the number
of row interchanges was even or odd, respectively.  This routine is used in combination with lubksb to 
solve linear equations or invert a matrix.  error=1 if matrix is singular(but LU decomposition possible).
If one row is null, the function is automatically stopped*/

{
  int i,imax,j,k;
  double big, dum, sum, temp;
  static double *vv=NULL;
  static int nold=n-1; // be sure that oldn != n at initialization
  
  *error = 0;
  *d = 1.0;

  if(nold!=n)
  {
    if(vv!=NULL)  free(vv);
    vv=(double*)malloc(n*sizeof(double));
    nold = n;
  }
  for (i=0;i<n;i++) {
    vv[i]=0.;
  }


  for (i=0;i<n;i++) {
    big = 0.0;
    for(j=0;j<n;j++) {
      temp = fabs(a[i][j]);
      if (temp>big) {
	big = temp;
      }
    }
    if (big < TINY) {
      printf("Singular matrix in routine ludcmp\n"); 
      *error = 1;

      printf("err: %d\n", *error);
      printf("big: %.10e\n", big);
      return;
    }
    vv[i]=1.0/big;  /* Save th scaling */
  }
  for (j=0;j<n;j++) {
    for (i=0;i<j;i++) {
      sum = a[i][j];
      for(k=0;k<i;k++) {
	sum -= a[i][k]*a[k][j];
      }
      a[i][j]=sum;
    }
    big = 0.0;
    for(i=j;i<n;i++) {
      sum = a[i][j];
      for(k=0;k<j;k++) {
	sum -= a[i][k]*a[k][j];      
      }
      a[i][j]=sum;
      if ( (dum=vv[i]*fabs(sum)) >=big) {
	/* Is the figure of merit for the pivot better than the best so far? */
	big=dum;
	imax = i;
      }
    }
    if(j!= imax) { /* Do we need to interchange rows? */
      for (k=0;k<n;k++) {  /* Yes, do so... */
	dum = a[imax][k];
	a[imax][k]=a[j][k];
	a[j][k]=dum;
      }
      *d = -(*d);  /* And change the parity of d */
      vv[imax] = vv[j];  /* Also interchange the scale factor */
    }
    indx[j]=imax;
    if (a[j][j] == 0.0) {
      a[j][j] = TINY;
      *error = 1;
    }
    /* If the pivot element is zero in the matrix is singular (at least to the precision of the algorithm).
       For some applications on singular matrices, it is desirale to substitute TINY for zero. */

    if (j != n) {
      dum = 1.0/(a[j][j]);
      for (i=j+1;i<n;i++) {
	a[i][j] *= dum;
      }
    }
  }
  //free(vv);
}

/**************************************
** FORWARD AND BACKWARD SUBSTITUTION **
***************************************/

void lubksb (double **a, int n, int *indx, double b[])

     /* Solves the set of n linear equations A.X=b.  Here a(n*n) is input, not as the matrix A but rather 
as its LU decomposition, determined by the routine ludcmp.  indx[1..n] is input as the permutation vector 
returned by ludcmp.  b[1..n] is input as the right-hand side vector , and returns with the solution 
vector X.  a,n, and indx are not modified by this routine and can be left in place for successive calls 
with different right-hand sides b.  This routine takes into account the possibility that b will begin 
with many zero elements, do it is efficient for use in matrix inversion. */

{
  int i, ii=-1, ip, j;
  double sum;

  for(i=0;i<n;i++) { /* When ii is set to a positive or null value, it will become the */
    ip = indx[i];    /* index of the first non vanishing element of b. We now  */
    sum = b[ip];     /* do the forward substitution.  The only new wrinkle is  */
    b[ip]=b[i];      /* to unscramble the permutation as we go.                */
    if (ii>=0)
      for (j=ii;j<i;j++) {
	sum -= a[i][j]*b[j];
      }
    else if (sum) {  /* A nonzero element was encountered, so from now on we */
      ii=i;          /* will have to do the sums in the loop above.          */
    }
    b[i]=sum;
  }

  for (i = n-1;i>=0;i--) {  /* Now we do the backsubstitution */
    sum=b[i];
    for (j=i+1;j<n;j++) {
      sum -= a[i][j]*b[j];
    }   
    b[i]=sum/a[i][i];    /* Store a component of the solution vector X. */
  }                      /* All done! */

}



/************************************************
** CALCUL DE L'INVERSE D'UNE MATRICE D'ORDRE n **
*************************************************/
/* 
 * INPUT: **a : the original matrix to invert
 * 	  **y : the inverse matrix
 *	  *error: error flag
 *	  n: size of the square matrix
*/
void inverse (double **a, double **y, int *error, int n)

{
  double d;
  int i,j;
  static int *indx = NULL;
  static int nold=n-1; // be sure that oldn != n at initialization
  static double *col = NULL;
  static double **aoriginal = NULL; 
  if(nold!=n)
  {
    if(col!=NULL)  free(col);
    if(indx!=NULL) free(indx);
    if(aoriginal !=NULL) freematrix(aoriginal, nold);   

    mallocmatrix(&aoriginal, n, n);
    col = (double*)malloc(n*sizeof(double));
    indx = (int*)malloc (n*sizeof(int));
    nold = n;
  }
  for(i=0;i<n;i++) {
    indx[i]=0;
    col[i]=0.;
    for(j=0;j<n;j++) {
      aoriginal[i][j]=0.;
    }
  }

  for(i=0;i<n;i++) {     /* On mémorise la matrice de départ à inverser */
    for(j=0;j<n;j++) {
      aoriginal[i][j]=a[i][j];
    }
  }

  ludcmp(a,n,indx,&d,error);
  if (*error != 0) {
    return;
  }

  for(j=0;j<n;j++) {
    for(i=0;i<n;i++) {
      col[i]=0.0;
    }
    col[j]=1.0;
    lubksb(a,n,indx,col);
    for (i=0;i<n;i++) {
      y[i][j]=col[i];
    }
  }
  for(i=0;i<n;i++) {     /* A has the LU decomposition form; we have to put the  */
    for(j=0;j<n;j++) {   /* original expression back */
      a[i][j] = aoriginal[i][j];
    }
  }

//  free(col);
//  free(indx);
//  freematrix(aoriginal, n);
}

/***********************************************/
/* Check if a fourth order tensor is isotropic */
/***********************************************/

/* April 2004 - O. Pierard */
/* Input: A: a fourth order tensor
   Output: res: 1 if A is isotropic; 0 otherwise */

int isisotropic(double**A)
{
  double lambda, mu;
  int i, j;
  double tol=1e-2;

  lambda = A[0][1];
  mu = A[3][3]/2.0;

  for (i=0;i<3;i++) {
    for (j=0;j<3;j++) {
      if (i!=j) {
				if (fabs(A[i][j]-lambda)/lambda > tol) 
	  			return 0;
				if (fabs(A[i+3][j+3])/lambda >tol)
	  			return 0;
      }
      else {   /* i==j i,j=1:3*/
				if (fabs(A[i][i] - (lambda+2.0*mu))/lambda > tol) 
	  			return 0;
				if (fabs(A[i+3][j+3] - 2.0*mu)/lambda>tol)
	  			return 0;
      }

      if (fabs(A[i+3][j])/lambda>tol)
				return 0;
      if (fabs(A[i][j+3]/lambda>tol))
				return 0;
    }
  }

  return 1;
}

/**************************************************
* Memory allocation for a table of pointers (1D) **
* **************************************************/
void mallocvector (double** V, int m)
{
  int i;

  *V = (double*)malloc(m*sizeof(double));
  
  for(i=0;i<m;i++){
     (*V)[i]=0.;
  }
}  

/* ***************************************************       
// ** Memory allocation for a table of pointers (2D) **
// **************************************************/
/*
 * "mallocmatrix": Memory allocation for a table of pointers.
 * INPUT : m : the number of lines(number of boxes for pointers)
 *         n : the number of rows
 *         **A : A declared pointer to pointers not yet initialized
 * OUTPUT : 
 */

void mallocmatrix (double ***A, int m, int n)

{
  int i,j;

  *A = (double**)malloc(m*sizeof(double*));
  for(i=0;i<m;i++){
    (*A)[i] = (double*)malloc(n*sizeof(double));
  }

  for(i=0;i<m;i++) {
    for(j=0;j<n;j++) {
      (*A)[i][j]=0.0;
    }
  }
}

/* *********************************************
// ** Memory allocation for a "cube" of data  **
// ********************************************/
void malloctens3( double**** A, int m, int n, int o){

  int i,j,k;

  *A = (double***) malloc(m*sizeof(double**));
  for(i=0;i<m;i++){
     (*A)[i] = (double**) malloc(n*sizeof(double*));
     for(j=0;j<n;j++){
        (*A)[i][j] = (double*) malloc (o*sizeof(double));
     }
  }
 
  for(i=0;i<m;i++){
     for(j=0;j<n;j++){
        for(k=0;k<o;k++){
           (*A)[i][j][k]=0.;
        }
     }
  }
}

/***********************************************
 * Memory allocation for a 4D-matrix					**
 * *********************************************/
void malloctens4(double ***** A, int m, int n, int p, int q){

   int i,j,k,l;
	 *A = (double****) malloc(m*sizeof(double***));
	 for(i=0;i<m;i++){
	   (*A)[i] = (double***) malloc(n*sizeof(double**));
		 for(j=0;j<n;j++){
		   (*A)[i][j] = (double**) malloc(p*sizeof(double*));
			 for(k=0;k<p;k++){
			    (*A)[i][j][k] = (double*) malloc(q*sizeof(double));
			 }
		 }
	 }
  for(i=0;i<m;i++){
     for(j=0;j<n;j++){
        for(k=0;k<p;k++){
				   for(l=0;l<q;l++){
              (*A)[i][j][k][l]=0.;
					 }
        }
     }
  }

}


/***********************************************
 * Freeing memory of a table of pointers **
 **********************************************/
/*
 * "freematrix": free memory of a table of pointers
 * INPUT : Tensor which has to be freed, number of lines
 * OUTPUT : nothing; memory cleaned
*/
void freematrix (double **A, int m)

{
  int i;

  for (i=0;i<m;i++) {
    free (A[i]);
  }
  free (A);

}

/************************************************
 *  Freing memory of a table of pointers (3D)  **
 *  *********************************************/
void freetens3 (double ***A, int m, int n)
{
  int i,j;
 
  for(i=0;i<m;i++){
     for(j=0;j<n;j++){
        free (A[i][j]);
        }
     free (A[i]);
  }
  free(A);
}

/************************************************
 *  Freing memory of a table of pointers (4D)  **
 *  *********************************************/
void freetens4 (double ****A, int m, int n, int p)
{
  int i,j, k;
 
  for(i=0;i<m;i++){
     for(j=0;j<n;j++){
		    for(k=0;k<p;k++){
           free (A[i][j][k]);
        }
     free (A[i][j]);
		 }
	free(A[i]);
  }
free(A);
}

/*************************************************/
/** Print on the screen values of a m*1 vector  **/
/************************************************/
void printvector(double *vect, int m)
{
 int i;
 for(i=0;i<m;i++){
    printf("%.9le \t",vect[i]);
 }
 printf("\n");
}

/************************************************/
/** Print on the screen values of a m*n matrix **/
/************************************************/

void printmatr(double **matr4, int m, int n)
{
  int i, j;

  for (i=0; i<m; i++) {
    for (j=0; j<n; j++) {
      printf("%.10le \t ", matr4[i][j]);
    }
    printf("\n");
  }
  printf("\n");
}   

/*************************************************/
/** check if all components of a vector are zero **
 * ***********************************************/
int iszero(double* vec, int size, double tol){

  int i;
  for(i=0;i<size;i++){
     if(fabs(vec[i])>tol){
        return 0;
     }

  }
  return 1;

}

void cleartens2(double* T){

	int i;
	for(i=0;i<6;i++){
		T[i]=0.;	
	}
}

void cleartens4(double** T){

	int i,j;
	for(i=0;i<6;i++){
		for(j=0;j<6;j++){
			T[i][j]=0.;
		}
	}
}
void cleartens6(double*** T){

	int i,j,k;
	for(i=0;i<6;i++){
		for(j=0;j<6;j++){
			for(k=0;k<6;k++){
				T[i][j][k] = 0.;				
			}
		}
	}
}


//creat cg -- charateristic length tensor in globle axis,
void creat_cg(double *pro, double *statev_n, int pos_p, double** cg){

      int i,j;
      static double* euler;
      static double** P;
      static double** T;
      
      static bool initialized = false;
      if(!initialized)
      {
        mallocmatrix(&P,3,3);
        mallocmatrix(&T,3,3);
        mallocvector(&euler,3);
        initialized = true;
      }
      for(i=0;i<3;i++)
      {
        euler[i]=0.;
        for(j=0;j<3;j++)
        {
          P[i][j]=0.;
          T[i][j]=0.;
         }
      }

      for(i=0;i<3;i++){
               T[i][i]= pro[i];
               euler[i]= pro[i+3];

      } 
     eul_mat33(euler, P);
     transpose(P,P,3,3);  
     rot33(P,T, cg);
     
     //freematrix(P,3);
     //freematrix(T,3);
     //free(euler);
		
}


void creat_cg(double pro, double** cg){

     int i, j;
     for(i=0;i<3;i++){
             for(j=0;j<3;j++){
                    cg[i][j]=0.0;
             }
             cg[i][i]=pro;
     }
}


/************************************************
 ** CALCUL DU DETERMINANT D'UNE MATRICE D'ORDRE n **
 *************************************************/
double Determinant(double **a,int n)
{
    int i,j,j1,j2 ;                    // general loop and matrix subscripts
    double det = 0.0 ;                   // init determinant
    static double **m = NULL ;                // pointer to pointers to implement 2d
    static int old_n=0;
    // square array
    
    if (n < 1)    {   }                // error condition, should never get here
    
    else if (n == 1) {                 // should not get here
        det = a[0][0] ;
    }
    
    else if (n == 2)  {                // basic 2X2 sub-matrix determinate
        // definition. When n==2, this ends the
        det = a[0][0] * a[1][1] - a[1][0] * a[0][1] ;// the recursion series
    }
    
    
    // recursion continues, solve next sub-matrix
    else {                             // solve the next minor by building a
        // sub matrix
        det = 0.0 ;                      // initialize determinant of sub-matrix
        
        // for each column in sub-matrix
        for (j1 = 0 ; j1 < n ; j1++) {
            // get space for the pointer list
            if(n!=old_n)
            {
              if(m!=NULL)
              {
                for (i = 0 ; i < old_n-1 ; i++) free(m[i]) ;// free the storage allocated to
                // to this minor's set of pointers
                free(m) ;                       // free the storage for the original
              }
              m = (double **) malloc((n-1)* sizeof(double *)) ;
            
              for (i = 0 ; i < n-1 ; i++)
                 m[i] = (double *) malloc((n-1)* sizeof(double)) ;
              old_n=n;
            }
            for (i = 0 ; i < n-1 ; i++)
            {
               for (j = 0 ; j < n-1 ; j++)
               {
                 m[i][j]=0.;
               }
            }
          
            //     i[0][1][2][3]  first malloc
            //  m -> +  +  +  +   space for 4 pointers
            //       |  |  |  |          j  second malloc
            //       |  |  |  +-> _ _ _ [0] pointers to
            //       |  |  +----> _ _ _ [1] and memory for
            //       |  +-------> _ a _ [2] 4 doubles
            //       +----------> _ _ _ [3]
            //
            //                   a[1][2]
            // build sub-matrix with minor elements excluded
            for (i = 1 ; i < n ; i++) {
                j2 = 0 ;               // start at first sum-matrix column position
                // loop to copy source matrix less one column
                for (j = 0 ; j < n ; j++) {
                    if (j == j1) continue ; // don't copy the minor column element
                    
                    m[i-1][j2] = a[i][j] ;  // copy source element into new sub-matrix
                    // i-1 because new sub-matrix is one row
                    // (and column) smaller with excluded minors
                    j2++ ;                  // move to next sub-matrix column position
                }
            }
            
            det += pow(-1.0,1.0 + j1 + 1.0) * a[0][j1] * Determinant(m,n-1) ;
            // sum x raised to y power
            // recursively get determinant of next
            // sub-matrix which is now one
            // row & column smaller
            
            //for (i = 0 ; i < n-1 ; i++) free(m[i]) ;// free the storage allocated to
            // to this minor's set of pointers
            //free(m) ;                       // free the storage for the original
            // pointer to pointer
        }
    }
    return(det) ;
}






#endif
