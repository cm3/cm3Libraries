//
// Description: storing class for TFA law
//
//
// Author:  <Kevin Spilker>, (C) 2019
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "ipTFA.h"
#include "restartManager.h"
#include "ipField.h"
#include "ClusteringData.h"

/*IPTFA::IPTFA() : IPVariableMechanics(), _clData(NULL)//, _mtxStress(6)
 {Msg::Error("Cannot construct default IPTFA");}



double IPTFA::get(int comp) const
{
  double val = _clData->get(comp);
  return val;
}





void IPTFA::restart()
{
  IPVariableMechanics::restart();
  restartManager::restart(_clData);
}*/


GenericIPTFA::GenericIPTFA() : IPVariableMechanics(), _genericClData(NULL)
 {Msg::Error("Cannot construct default GenericIPTFA");}

double GenericIPTFA::get(int comp) const
{
  double val = _genericClData->get(comp);
  return val;
}

void GenericIPTFA::restart()
{
  IPVariableMechanics::restart();
  restartManager::restart(_genericClData);
}

GenericIPTFA::~GenericIPTFA()
{
  if (_genericClData!=NULL) delete _genericClData;
  _genericClData = NULL;
}


double IPTFA::get(int comp) const
{
  double val = _clData->get(comp);
  return val;
}

void IPTFA::restart()
{
  GenericIPTFA::restart();
}

IPTFA::~IPTFA()
{
  if (_clData!=NULL) delete _clData;
  _clData = NULL;
}


double IPTFA2Levels::get(int comp) const
{
  double val = _clDataLevel0->get(comp);
  return val;
}

void IPTFA2Levels::restart()
{
  GenericIPTFA::restart();
  restartManager::restart(_clDataLevel0);
  for(std::map<int,ClusteringData*>::const_iterator itLv0 = _clDataLevel1.begin(); itLv0 != _clDataLevel1.end(); itLv0++)
  {
    restartManager::restart(itLv0->second);
  }
}

IPTFA2Levels::~IPTFA2Levels()
{
  if (_clDataLevel0!=NULL) delete _clDataLevel0;
  _clDataLevel0 = NULL;
  if (!(_clDataLevel1.empty()))
  {
    _clDataLevel1.clear();
  }
}

