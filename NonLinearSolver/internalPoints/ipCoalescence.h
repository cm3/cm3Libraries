//
// C++ Interface: ipGursonCoalescence
//
// Description: Storing class for coalescence law
//
//
// Author:  <J. Leclerc>, (C) 2017
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef IPCOALESCENCE_H_
#define IPCOALESCENCE_H_
#include <stdlib.h>
#include <stdio.h>
#include "GmshConfig.h"
#include "ipVoidState.h"

class IPNonLocalPorosity;

class IPCoalescence
{
  // Base class for coalescence IPV
  protected:
    double _acceleratedRate;    // Rate acceleration - equal 1 by default
    bool _coalescenceOnsetFlag;  // =true if coalescence occurs once
    bool _coalescenceActive;     // coalescence active
    int _coalescenceMode; // 0 - undef, 1 - necking, 2 - shearning
    IPNonLocalPorosity *_ipvAtCoalescenceOnset; // state at coalescence
    
    // Offset and coupling management (updated at the end of a converged step)
    double _crackOffsetOnCft;  // Offset applied on Cft to obtain fT = 0
    double _yieldOffset;       // Yield surface value for the unused plastic mode
    double _LodeOffset;


  public:
    // Constructors & destructor
    IPCoalescence();
    IPCoalescence(const IPCoalescence &source);
    virtual IPCoalescence &operator=(const IPCoalescence &source);
    virtual ~IPCoalescence();

    // Standard functions
    virtual void restart();
    virtual IPCoalescence * clone() const=0;

    // Access functions
    virtual bool getCoalescenceOnsetFlag() const {return _coalescenceOnsetFlag;};
    virtual bool& getRefToCoalescenceOnsetFlag() {return _coalescenceOnsetFlag;};
    
    virtual bool getCoalescenceActiveFlag() const {return _coalescenceActive;};
    virtual bool& getRefToCoalescenceActiveFlag() {return _coalescenceActive;};
    
    virtual int getCoalescenceMode() const {return _coalescenceMode;};
    virtual int& getRefToCoalescenceMode() {return _coalescenceMode;};

    virtual double getAccelerateRate() const {return _acceleratedRate;};
    virtual double& getRefToAccelerateRate() {return _acceleratedRate;};
    
    virtual void deleteIPvAtCoalescenceOnset();
    virtual const IPNonLocalPorosity* getIPvAtCoalescenceOnset() const;
    virtual void setIPvAtCoalescenceOnset(const IPNonLocalPorosity& ipvVoid);
    
    virtual double getCrackOffsetOnCft() const {return _crackOffsetOnCft;};
    virtual double& getRefToCrackOffsetOnCft() {return _crackOffsetOnCft;};

    virtual double getYieldOffset() const {return _yieldOffset;};
    virtual double& getRefToYieldOffset() {return _yieldOffset;};
    
    virtual double getLodeParameterOffset() const {return _LodeOffset;};
    virtual double& getRefToLodeParameterOffset() {return _LodeOffset;};

};



class IPNoCoalescence : public IPCoalescence{
  // IPV for NoCoalescenceLaw
  public:
    // Constructors & destructor
    IPNoCoalescence();
    IPNoCoalescence(const IPNoCoalescence &source);
    virtual IPNoCoalescence &operator=(const IPCoalescence &source);
    virtual ~IPNoCoalescence(){};
    // Standard functions
    virtual IPCoalescence * clone() const;
};



class IPFstarCoalescence : public IPCoalescence{
  // IPV for FstarCoalescenceLaw
  public:
    // Constructors & destructor
    IPFstarCoalescence();
    IPFstarCoalescence(const IPFstarCoalescence &source);
    virtual IPFstarCoalescence &operator=(const IPCoalescence &source);
    virtual ~IPFstarCoalescence(){};
    // Standard functions
    virtual IPCoalescence * clone() const;
};


class IPLoadConcentrationFactorBasedCoalescence : public IPCoalescence{
    // IPV for ThomasonCoalescenceLaw
  protected:

  public:
    IPLoadConcentrationFactorBasedCoalescence();
    IPLoadConcentrationFactorBasedCoalescence(const IPLoadConcentrationFactorBasedCoalescence& src);
    virtual IPLoadConcentrationFactorBasedCoalescence& operator = (const IPCoalescence& src);
    virtual ~IPLoadConcentrationFactorBasedCoalescence(){};
    virtual IPCoalescence * clone() const {return new IPLoadConcentrationFactorBasedCoalescence(*this);};
};


#endif // IPCOALESCENCE_H_
