//
// C++ Interface: terms
//
// Description: Class to compute Internal point
//
//
// Author:  <Gauthier BECKER>, (C) 2010
// <Van Dung NGUYEN>, (C) 2019
// Copyright: See COPYING file that comes with this distribution
//
//
#include "ipField.h"
#include "mlaw.h"
#include "ipstate.h"
#include "nonLinearMechSolver.h"
#include "nlsFunctionSpace.h"
#include "energyField.h"
#include "numericalMaterial.h"
#include "ipFiniteStrain.h"
#include "DeepMaterialNetworks.h"

IPDataTransfer::~IPDataTransfer()
{
  for (std::map<int, IPDataTransferMat*>::iterator it = _allTransfers.begin(); it != _allTransfers.end(); it++)
  {
    delete it->second;
  }
  _allTransfers.clear();
};

const IPDataTransferMat* IPDataTransfer::getIPDataTransferMat(int phys) const
{
  std::map<int, IPDataTransferMat*>::const_iterator itF = _allTransfers.find(phys);
  if (itF == _allTransfers.end())
  {
    return NULL;
  }
  return itF->second;
};

bool IPDataTransfer::withTranfer(int phys) const
{
  std::map<int, IPDataTransferMat*>::const_iterator itF = _allTransfers.find(phys);
  if (itF == _allTransfers.end())
  {
    return false;
  }
  return true;
}

void IPDataTransfer::add(int phys, const IPDataTransferMat* cl)
{
  _allTransfers[phys] = cl->clone();
}
void IPDataTransfer::transferDataIPv(int phys, const IPVariable* src, IPVariable* dest) const
{
  const IPDataTransferMat* obj = getIPDataTransferMat(phys);
  obj->transferData(src,dest);
};

std::string IPField::ToString(const int i){
  if (i == SVM) return "SVM";
  else if (i == SIG_XX) return "SIG_XX";
  else if (i == SIG_YY) return "SIG_YY";
  else if (i == SIG_ZZ) return "SIG_ZZ";
  else if (i == SIG_XY) return "SIG_XY";
  else if (i == SIG_YZ) return "SIG_YZ";
  else if (i == SIG_XZ) return "SIG_XZ";
  else if (i == DAMAGE) return "DAMAGE";
  else if (i == PLASTICSTRAIN) return "PLASTICSTRAIN";
  else if (i == GL_EQUIVALENT_STRAIN) return "GL_EQUIVALENT_STRAIN";
  else if (i == STRAIN_XX) return "STRAIN_XX";
  else if (i == STRAIN_YY) return "STRAIN_YY";
  else if (i == STRAIN_ZZ) return "STRAIN_ZZ";
  else if (i == STRAIN_XY) return "STRAIN_XY";
  else if (i == STRAIN_YZ) return "STRAIN_YZ";
  else if (i == STRAIN_XZ) return "STRAIN_XZ";
  else if (i == VOLUMETRIC_STRAIN) return "VOLUMETRIC_STRAIN";
  else if (i == HIGHSTRAIN_NORM) return "HIGHSTRAIN_NORM";
  else if (i == HIGHSTRESS_NORM) return "HIGHSTRESS_NORM";
  else if (i == GL_NORM) return "GL_NORM";
  else if (i == U_NORM) return "U_NORM";
  else if (i == STRESS_NORM) return "STRESS_NORM";
  else if (i == P_STRESS_NORM) return "P_STRESS_NORM";
  else if (i == U_XX) return "U_XX";
  else if (i == U_XY) return "U_XY";
  else if (i == U_XZ) return "U_XZ";
  else if (i == U_YY) return "U_YY";
  else if (i == U_YZ) return "U_YZ";
  else if (i == U_ZZ) return "U_ZZ";
  else if (i == GL_XX) return "GL_XX";
  else if (i == GL_XY) return "GL_XY";
  else if (i == GL_XZ) return "GL_XZ";
  else if (i == GL_YY) return "GL_YY";
  else if (i == GL_YZ) return "GL_YZ";
  else if (i == GL_ZZ) return "GL_ZZ";
  else if (i == F_XX) return "F_XX";
  else if (i == F_XY) return "F_XY";
  else if (i == F_XZ) return "F_XZ";
  else if (i == F_YX) return "F_YX";
  else if (i == F_YY) return "F_YY";
  else if (i == F_YZ) return "F_YZ";
  else if (i == F_ZX) return "F_ZX";
  else if (i == F_ZY) return "F_ZY";
  else if (i == F_ZZ) return "F_ZZ";
  else if (i == P_XX) return "P_XX";
  else if (i == P_XY) return "P_XY";
  else if (i == P_XZ) return "P_XZ";
  else if (i == P_YX) return "P_YX";
  else if (i == P_YY) return "P_YY";
  else if (i == P_YZ) return "P_YZ";
  else if (i == P_ZX) return "P_ZX";
  else if (i == P_ZY) return "P_ZY";
  else if (i == P_ZZ) return "P_ZZ";

  else if (i == P_XX_EFF) return "P_XX_EFF";
  else if (i == P_XY_EFF) return "P_XY_EFF";
  else if (i == P_XZ_EFF) return "P_XZ_EFF";
  else if (i == P_YX_EFF) return "P_YX_EFF";
  else if (i == P_YY_EFF) return "P_YY_EFF";
  else if (i == P_YZ_EFF) return "P_YZ_EFF";
  else if (i == P_ZX_EFF) return "P_ZX_EFF";
  else if (i == P_ZY_EFF) return "P_ZY_EFF";
  else if (i == P_ZZ_EFF) return "P_ZZ_EFF";
  else if (i == PRESSION_EFF) return "PRESSION_EFF";
  else if (i == SVM_EFF) return "SVM_EFF";
  else if (i == STRESS_TRIAXIALITY_EFF) return "STRESS_TRIAXIALITY_EFF";
  // for VT case (VM or VLM)
  // for each VT phase
  // strain
  else if (i == STRAIN_XX_VTM01) return "STRAIN_XX_VTM01";
  else if (i == STRAIN_XX_VTMT1) return "STRAIN_XX_VTMT1";
  else if (i == STRAIN_XX_VTMT2) return "STRAIN_XX_VTMT2";
  else if (i == STRAIN_XX_VTMT3) return "STRAIN_XX_VTMT3";
  else if (i == STRAIN_XX_VTMT4) return "STRAIN_XX_VTMT4";
  else if (i == STRAIN_XX_VTMT5) return "STRAIN_XX_VTMT5";
  else if (i == STRAIN_XX_VTMT6) return "STRAIN_XX_VTMT6";
  else if (i == STRAIN_XX_VTLAM1) return "STRAIN_XX_VTLAM1";
  else if (i == STRAIN_XX_VTLAM2) return "STRAIN_XX_VTLAM2";
  else if (i == STRAIN_XX_VTLAM3) return "STRAIN_XX_VTLAM3";
  else if (i == STRAIN_XX_VTLAM4) return "STRAIN_XX_VTLAM4";
  else if (i == STRAIN_XX_VTLAM5) return "STRAIN_XX_VTLAM5";
  else if (i == STRAIN_XX_VTLAM6) return "STRAIN_XX_VTLAM6";
  else if (i == STRAIN_XX_VTLAM7) return "STRAIN_XX_VTLAM7";
  else if (i == STRAIN_XX_VTLAM8) return "STRAIN_XX_VTLAM8";
  else if (i == STRAIN_XX_VTLAM9) return "STRAIN_XX_VTLAM9";
  else if (i == STRAIN_XX_VTLAM10) return "STRAIN_XX_VTLAM10";
  else if (i == STRAIN_XX_VTLAM11) return "STRAIN_XX_VTLAM11";
  else if (i == STRAIN_XX_VTLAM12) return "STRAIN_XX_VTLAM12";
  // stress
  else if (i == STRESS_XX_VTM01) return "STRESS_XX_VTM01";
  else if (i == STRESS_XX_VTMT1) return "STRESS_XX_VTMT1";
  else if (i == STRESS_XX_VTMT2) return "STRESS_XX_VTMT2";
  else if (i == STRESS_XX_VTMT3) return "STRESS_XX_VTMT3";
  else if (i == STRESS_XX_VTMT4) return "STRESS_XX_VTMT4";
  else if (i == STRESS_XX_VTMT5) return "STRESS_XX_VTMT5";
  else if (i == STRESS_XX_VTMT6) return "STRESS_XX_VTMT6";
  else if (i == STRESS_XX_VTLAM1) return "STRESS_XX_VTLAM1";
  else if (i == STRESS_XX_VTLAM2) return "STRESS_XX_VTLAM2";
  else if (i == STRESS_XX_VTLAM3) return "STRESS_XX_VTLAM3";
  else if (i == STRESS_XX_VTLAM4) return "STRESS_XX_VTLAM4";
  else if (i == STRESS_XX_VTLAM5) return "STRESS_XX_VTLAM5";
  else if (i == STRESS_XX_VTLAM6) return "STRESS_XX_VTLAM6";
  else if (i == STRESS_XX_VTLAM7) return "STRESS_XX_VTLAM7";
  else if (i == STRESS_XX_VTLAM8) return "STRESS_XX_VTLAM8";
  else if (i == STRESS_XX_VTLAM9) return "STRESS_XX_VTLAM9";
  else if (i == STRESS_XX_VTLAM10) return "STRESS_XX_VTLAM10";
  else if (i == STRESS_XX_VTLAM11) return "STRESS_XX_VTLAM11";
  else if (i == STRESS_XX_VTLAM12) return "STRESS_XX_VTLAM12";
  // damage
  else if (i == DAMAGE_VTM01) return "DAMAGE_VTM01";
  // for each laminate ply in VT grains
  // strain
  else if (i == STRAIN_XX_VTLAM1_M0) return "STRAIN_XX_VTLAM1_M0";
  else if (i == STRAIN_XX_VTLAM1_MT) return "STRAIN_XX_VTLAM1_MT";
  else if (i == STRAIN_XX_VTLAM2_M0) return "STRAIN_XX_VTLAM2_M0";
  else if (i == STRAIN_XX_VTLAM2_MT) return "STRAIN_XX_VTLAM2_MT";
  else if (i == STRAIN_XX_VTLAM3_M0) return "STRAIN_XX_VTLAM3_M0";
  else if (i == STRAIN_XX_VTLAM3_MT) return "STRAIN_XX_VTLAM3_MT";
  else if (i == STRAIN_XX_VTLAM4_M0) return "STRAIN_XX_VTLAM4_M0";
  else if (i == STRAIN_XX_VTLAM4_MT) return "STRAIN_XX_VTLAM4_MT";
  else if (i == STRAIN_XX_VTLAM5_M0) return "STRAIN_XX_VTLAM5_M0";
  else if (i == STRAIN_XX_VTLAM5_MT) return "STRAIN_XX_VTLAM5_MT";
  else if (i == STRAIN_XX_VTLAM6_M0) return "STRAIN_XX_VTLAM6_M0";
  else if (i == STRAIN_XX_VTLAM6_MT) return "STRAIN_XX_VTLAM6_MT";
  else if (i == STRAIN_XX_VTLAM7_M0) return "STRAIN_XX_VTLAM7_M0";
  else if (i == STRAIN_XX_VTLAM7_MT) return "STRAIN_XX_VTLAM7_MT";
  else if (i == STRAIN_XX_VTLAM8_M0) return "STRAIN_XX_VTLAM8_M0";
  else if (i == STRAIN_XX_VTLAM8_MT) return "STRAIN_XX_VTLAM8_MT";
  else if (i == STRAIN_XX_VTLAM9_M0) return "STRAIN_XX_VTLAM9_M0";
  else if (i == STRAIN_XX_VTLAM9_MT) return "STRAIN_XX_VTLAM9_MT";
  else if (i == STRAIN_XX_VTLAM10_M0) return "STRAIN_XX_VTLAM10_M0";
  else if (i == STRAIN_XX_VTLAM10_MT) return "STRAIN_XX_VTLAM10_MT";
  else if (i == STRAIN_XX_VTLAM11_M0) return "STRAIN_XX_VTLAM11_M0";
  else if (i == STRAIN_XX_VTLAM11_MT) return "STRAIN_XX_VTLAM11_MT";
  else if (i == STRAIN_XX_VTLAM12_M0) return "STRAIN_XX_VTLAM12_M0";
  else if (i == STRAIN_XX_VTLAM12_MT) return "STRAIN_XX_VTLAM12_MT";
  // stress
  else if (i == STRESS_XX_VTLAM1_M0) return "STRESS_XX_VTLAM1_M0";
  else if (i == STRESS_XX_VTLAM1_MT) return "STRESS_XX_VTLAM1_MT";
  else if (i == STRESS_XX_VTLAM2_M0) return "STRESS_XX_VTLAM2_M0";
  else if (i == STRESS_XX_VTLAM2_MT) return "STRESS_XX_VTLAM2_MT";
  else if (i == STRESS_XX_VTLAM3_M0) return "STRESS_XX_VTLAM3_M0";
  else if (i == STRESS_XX_VTLAM3_MT) return "STRESS_XX_VTLAM3_MT";
  else if (i == STRESS_XX_VTLAM4_M0) return "STRESS_XX_VTLAM4_M0";
  else if (i == STRESS_XX_VTLAM4_MT) return "STRESS_XX_VTLAM4_MT";
  else if (i == STRESS_XX_VTLAM5_M0) return "STRESS_XX_VTLAM5_M0";
  else if (i == STRESS_XX_VTLAM5_MT) return "STRESS_XX_VTLAM5_MT";
  else if (i == STRESS_XX_VTLAM6_M0) return "STRESS_XX_VTLAM6_M0";
  else if (i == STRESS_XX_VTLAM6_MT) return "STRESS_XX_VTLAM6_MT";
  else if (i == STRESS_XX_VTLAM7_M0) return "STRESS_XX_VTLAM7_M0";
  else if (i == STRESS_XX_VTLAM7_MT) return "STRESS_XX_VTLAM7_MT";
  else if (i == STRESS_XX_VTLAM8_M0) return "STRESS_XX_VTLAM8_M0";
  else if (i == STRESS_XX_VTLAM8_MT) return "STRESS_XX_VTLAM8_MT";
  else if (i == STRESS_XX_VTLAM9_M0) return "STRESS_XX_VTLAM9_M0";
  else if (i == STRESS_XX_VTLAM9_MT) return "STRESS_XX_VTLAM9_MT";
  else if (i == STRESS_XX_VTLAM10_M0) return "STRESS_XX_VTLAM10_M0";
  else if (i == STRESS_XX_VTLAM10_MT) return "STRESS_XX_VTLAM10_MT";
  else if (i == STRESS_XX_VTLAM11_M0) return "STRESS_XX_VTLAM11_M0";
  else if (i == STRESS_XX_VTLAM11_MT) return "STRESS_XX_VTLAM11_MT";
  else if (i == STRESS_XX_VTLAM12_M0) return "STRESS_XX_VTLAM12_M0";
  else if (i == STRESS_XX_VTLAM12_MT) return "STRESS_XX_VTLAM12_MT";
  // damage
  else if (i == DAMAGE_VTLAM1_M0) return "DAMAGE_VTLAM1_M0";
  else if (i == DAMAGE_VTLAM2_M0) return "DAMAGE_VTLAM2_M0";
  else if (i == DAMAGE_VTLAM3_M0) return "DAMAGE_VTLAM3_M0";
  else if (i == DAMAGE_VTLAM4_M0) return "DAMAGE_VTLAM4_M0";
  else if (i == DAMAGE_VTLAM5_M0) return "DAMAGE_VTLAM5_M0";
  else if (i == DAMAGE_VTLAM6_M0) return "DAMAGE_VTLAM6_M0";
  else if (i == DAMAGE_VTLAM7_M0) return "DAMAGE_VTLAM7_M0";
  else if (i == DAMAGE_VTLAM8_M0) return "DAMAGE_VTLAM8_M0";
  else if (i == DAMAGE_VTLAM9_M0) return "DAMAGE_VTLAM9_M0";
  else if (i == DAMAGE_VTLAM10_M0) return "DAMAGE_VTLAM10_M0";
  else if (i == DAMAGE_VTLAM11_M0) return "DAMAGE_VTLAM11_M0";
  else if (i == DAMAGE_VTLAM12_M0) return "DAMAGE_VTLAM12_M0";
  // for each material phase
  // strain
  else if (i == STRAIN_XX_VTMT1_MTX) return "STRAIN_XX_VTMT1_MTX";
  else if (i == STRAIN_XX_VTMT1_INC) return "STRAIN_XX_VTMT1_INC";
  else if (i == STRAIN_XX_VTMT2_MTX) return "STRAIN_XX_VTMT2_MTX";
  else if (i == STRAIN_XX_VTMT2_INC) return "STRAIN_XX_VTMT2_INC";
  else if (i == STRAIN_XX_VTMT3_MTX) return "STRAIN_XX_VTMT3_MTX";
  else if (i == STRAIN_XX_VTMT3_INC) return "STRAIN_XX_VTMT3_INC";
  else if (i == STRAIN_XX_VTMT4_MTX) return "STRAIN_XX_VTMT4_MTX";
  else if (i == STRAIN_XX_VTMT4_INC) return "STRAIN_XX_VTMT4_INC";
  else if (i == STRAIN_XX_VTMT5_MTX) return "STRAIN_XX_VTMT5_MTX";
  else if (i == STRAIN_XX_VTMT5_INC) return "STRAIN_XX_VTMT5_INC";
  else if (i == STRAIN_XX_VTMT6_MTX) return "STRAIN_XX_VTMT6_MTX";
  else if (i == STRAIN_XX_VTMT6_INC) return "STRAIN_XX_VTMT6_INC";
  else if (i == STRAIN_XX_VTLAM1_MT_MTX) return "STRAIN_XX_VTLAM1_MT_MTX";
  else if (i == STRAIN_XX_VTLAM1_MT_INC) return "STRAIN_XX_VTLAM1_MT_INC";
  else if (i == STRAIN_XX_VTLAM2_MT_MTX) return "STRAIN_XX_VTLAM2_MT_MTX";
  else if (i == STRAIN_XX_VTLAM2_MT_INC) return "STRAIN_XX_VTLAM2_MT_INC";
  else if (i == STRAIN_XX_VTLAM3_MT_MTX) return "STRAIN_XX_VTLAM3_MT_MTX";
  else if (i == STRAIN_XX_VTLAM3_MT_INC) return "STRAIN_XX_VTLAM3_MT_INC";
  else if (i == STRAIN_XX_VTLAM4_MT_MTX) return "STRAIN_XX_VTLAM4_MT_MTX";
  else if (i == STRAIN_XX_VTLAM4_MT_INC) return "STRAIN_XX_VTLAM4_MT_INC";
  else if (i == STRAIN_XX_VTLAM5_MT_MTX) return "STRAIN_XX_VTLAM5_MT_MTX";
  else if (i == STRAIN_XX_VTLAM5_MT_INC) return "STRAIN_XX_VTLAM5_MT_INC";
  else if (i == STRAIN_XX_VTLAM6_MT_MTX) return "STRAIN_XX_VTLAM6_MT_MTX";
  else if (i == STRAIN_XX_VTLAM6_MT_INC) return "STRAIN_XX_VTLAM6_MT_INC";
  else if (i == STRAIN_XX_VTLAM7_MT_MTX) return "STRAIN_XX_VTLAM7_MT_MTX";
  else if (i == STRAIN_XX_VTLAM7_MT_INC) return "STRAIN_XX_VTLAM7_MT_INC";
  else if (i == STRAIN_XX_VTLAM8_MT_MTX) return "STRAIN_XX_VTLAM8_MT_MTX";
  else if (i == STRAIN_XX_VTLAM8_MT_INC) return "STRAIN_XX_VTLAM8_MT_INC";
  else if (i == STRAIN_XX_VTLAM9_MT_MTX) return "STRAIN_XX_VTLAM9_MT_MTX";
  else if (i == STRAIN_XX_VTLAM9_MT_INC) return "STRAIN_XX_VTLAM9_MT_INC";
  else if (i == STRAIN_XX_VTLAM10_MT_MTX) return "STRAIN_XX_VTLAM10_MT_MTX";
  else if (i == STRAIN_XX_VTLAM10_MT_INC) return "STRAIN_XX_VTLAM10_MT_INC";
  else if (i == STRAIN_XX_VTLAM11_MT_MTX) return "STRAIN_XX_VTLAM11_MT_MTX";
  else if (i == STRAIN_XX_VTLAM11_MT_INC) return "STRAIN_XX_VTLAM11_MT_INC";
  else if (i == STRAIN_XX_VTLAM12_MT_MTX) return "STRAIN_XX_VTLAM12_MT_MTX";
  else if (i == STRAIN_XX_VTLAM12_MT_INC) return "STRAIN_XX_VTLAM12_MT_INC";
  // stress
  else if (i == STRESS_XX_VTMT1_MTX) return "STRESS_XX_VTMT1_MTX";
  else if (i == STRESS_XX_VTMT1_INC) return "STRESS_XX_VTMT1_INC";
  else if (i == STRESS_XX_VTMT2_MTX) return "STRESS_XX_VTMT2_MTX";
  else if (i == STRESS_XX_VTMT2_INC) return "STRESS_XX_VTMT2_INC";
  else if (i == STRESS_XX_VTMT3_MTX) return "STRESS_XX_VTMT3_MTX";
  else if (i == STRESS_XX_VTMT3_INC) return "STRESS_XX_VTMT3_INC";
  else if (i == STRESS_XX_VTMT4_MTX) return "STRESS_XX_VTMT4_MTX";
  else if (i == STRESS_XX_VTMT4_INC) return "STRESS_XX_VTMT4_INC";
  else if (i == STRESS_XX_VTMT5_MTX) return "STRESS_XX_VTMT5_MTX";
  else if (i == STRESS_XX_VTMT5_INC) return "STRESS_XX_VTMT5_INC";
  else if (i == STRESS_XX_VTMT6_MTX) return "STRESS_XX_VTMT6_MTX";
  else if (i == STRESS_XX_VTMT6_INC) return "STRESS_XX_VTMT6_INC";
  else if (i == STRESS_XX_VTLAM1_MT_MTX) return "STRESS_XX_VTLAM1_MT_MTX";
  else if (i == STRESS_XX_VTLAM1_MT_INC) return "STRESS_XX_VTLAM1_MT_INC";
  else if (i == STRESS_XX_VTLAM2_MT_MTX) return "STRESS_XX_VTLAM2_MT_MTX";
  else if (i == STRESS_XX_VTLAM2_MT_INC) return "STRESS_XX_VTLAM2_MT_INC";
  else if (i == STRESS_XX_VTLAM3_MT_MTX) return "STRESS_XX_VTLAM3_MT_MTX";
  else if (i == STRESS_XX_VTLAM3_MT_INC) return "STRESS_XX_VTLAM3_MT_INC";
  else if (i == STRESS_XX_VTLAM4_MT_MTX) return "STRESS_XX_VTLAM4_MT_MTX";
  else if (i == STRESS_XX_VTLAM4_MT_INC) return "STRESS_XX_VTLAM4_MT_INC";
  else if (i == STRESS_XX_VTLAM5_MT_MTX) return "STRESS_XX_VTLAM5_MT_MTX";
  else if (i == STRESS_XX_VTLAM5_MT_INC) return "STRESS_XX_VTLAM5_MT_INC";
  else if (i == STRESS_XX_VTLAM6_MT_MTX) return "STRESS_XX_VTLAM6_MT_MTX";
  else if (i == STRESS_XX_VTLAM6_MT_INC) return "STRESS_XX_VTLAM6_MT_INC";
  else if (i == STRESS_XX_VTLAM7_MT_MTX) return "STRESS_XX_VTLAM7_MT_MTX";
  else if (i == STRESS_XX_VTLAM7_MT_INC) return "STRESS_XX_VTLAM7_MT_INC";
  else if (i == STRESS_XX_VTLAM8_MT_MTX) return "STRESS_XX_VTLAM8_MT_MTX";
  else if (i == STRESS_XX_VTLAM8_MT_INC) return "STRESS_XX_VTLAM8_MT_INC";
  else if (i == STRESS_XX_VTLAM9_MT_MTX) return "STRESS_XX_VTLAM9_MT_MTX";
  else if (i == STRESS_XX_VTLAM9_MT_INC) return "STRESS_XX_VTLAM9_MT_INC";
  else if (i == STRESS_XX_VTLAM10_MT_MTX) return "STRESS_XX_VTLAM10_MT_MTX";
  else if (i == STRESS_XX_VTLAM10_MT_INC) return "STRESS_XX_VTLAM10_MT_INC";
  else if (i == STRESS_XX_VTLAM11_MT_MTX) return "STRESS_XX_VTLAM11_MT_MTX";
  else if (i == STRESS_XX_VTLAM11_MT_INC) return "STRESS_XX_VTLAM11_MT_INC";
  else if (i == STRESS_XX_VTLAM12_MT_MTX) return "STRESS_XX_VTLAM12_MT_MTX";
  else if (i == STRESS_XX_VTLAM12_MT_INC) return "STRESS_XX_VTLAM12_MT_INC";
  // damage
  else if (i == DAMAGE_VTMT1_MTX) return "DAMAGE_VTMT1_MTX";
  else if (i == DAMAGE_VTMT1_INC) return "DAMAGE_VTMT1_INC";
  else if (i == DAMAGE_VTMT2_MTX) return "DAMAGE_VTMT2_MTX";
  else if (i == DAMAGE_VTMT2_INC) return "DAMAGE_VTMT2_INC";
  else if (i == DAMAGE_VTMT3_MTX) return "DAMAGE_VTMT3_MTX";
  else if (i == DAMAGE_VTMT3_INC) return "DAMAGE_VTMT3_INC";
  else if (i == DAMAGE_VTMT4_MTX) return "DAMAGE_VTMT4_MTX";
  else if (i == DAMAGE_VTMT4_INC) return "DAMAGE_VTMT4_INC";
  else if (i == DAMAGE_VTMT5_MTX) return "DAMAGE_VTMT5_MTX";
  else if (i == DAMAGE_VTMT5_INC) return "DAMAGE_VTMT5_INC";
  else if (i == DAMAGE_VTMT6_MTX) return "DAMAGE_VTMT6_MTX";
  else if (i == DAMAGE_VTMT6_INC) return "DAMAGE_VTMT6_INC";
  else if (i == DAMAGE_VTLAM1_MT_MTX) return "DAMAGE_VTLAM1_MT_MTX";
  else if (i == DAMAGE_VTLAM1_MT_INC) return "DAMAGE_VTLAM1_MT_INC";
  else if (i == DAMAGE_VTLAM2_MT_MTX) return "DAMAGE_VTLAM2_MT_MTX";
  else if (i == DAMAGE_VTLAM2_MT_INC) return "DAMAGE_VTLAM2_MT_INC";
  else if (i == DAMAGE_VTLAM3_MT_MTX) return "DAMAGE_VTLAM3_MT_MTX";
  else if (i == DAMAGE_VTLAM3_MT_INC) return "DAMAGE_VTLAM3_MT_INC";
  else if (i == DAMAGE_VTLAM4_MT_MTX) return "DAMAGE_VTLAM4_MT_MTX";
  else if (i == DAMAGE_VTLAM4_MT_INC) return "DAMAGE_VTLAM4_MT_INC";
  else if (i == DAMAGE_VTLAM5_MT_MTX) return "DAMAGE_VTLAM5_MT_MTX";
  else if (i == DAMAGE_VTLAM5_MT_INC) return "DAMAGE_VTLAM5_MT_INC";
  else if (i == DAMAGE_VTLAM6_MT_MTX) return "DAMAGE_VTLAM6_MT_MTX";
  else if (i == DAMAGE_VTLAM6_MT_INC) return "DAMAGE_VTLAM6_MT_INC";
  else if (i == DAMAGE_VTLAM7_MT_MTX) return "DAMAGE_VTLAM7_MT_MTX";
  else if (i == DAMAGE_VTLAM7_MT_INC) return "DAMAGE_VTLAM7_MT_INC";
  else if (i == DAMAGE_VTLAM8_MT_MTX) return "DAMAGE_VTLAM8_MT_MTX";
  else if (i == DAMAGE_VTLAM8_MT_INC) return "DAMAGE_VTLAM8_MT_INC";
  else if (i == DAMAGE_VTLAM9_MT_MTX) return "DAMAGE_VTLAM9_MT_MTX";
  else if (i == DAMAGE_VTLAM9_MT_INC) return "DAMAGE_VTLAM9_MT_INC";
  else if (i == DAMAGE_VTLAM10_MT_MTX) return "DAMAGE_VTLAM10_MT_MTX";
  else if (i == DAMAGE_VTLAM10_MT_INC) return "DAMAGE_VTLAM10_MT_INC";
  else if (i == DAMAGE_VTLAM11_MT_MTX) return "DAMAGE_VTLAM11_MT_MTX";
  else if (i == DAMAGE_VTLAM11_MT_INC) return "DAMAGE_VTLAM11_MT_INC";
  else if (i == DAMAGE_VTLAM12_MT_MTX) return "DAMAGE_VTLAM12_MT_MTX";
  else if (i == DAMAGE_VTLAM12_MT_INC) return "DAMAGE_VTLAM12_MT_INC";

  // for LAM_2PLY case (LVM)
  // for each laminate ply
  // strain
  else if (i == STRAIN_XX_LAMM01) return "STRAIN_XX_LAMM01";
  else if (i == STRAIN_XX_LAMVT1) return "STRAIN_XX_LAMVT1";
  // stress
  else if (i == STRESS_XX_LAMM01) return "STRESS_XX_LAMM01";
  else if (i == STRESS_XX_LAMVT1) return "STRESS_XX_LAMVT1";
  // damage
  else if (i == DAMAGE_LAMM01) return "DAMAGE_LAMM01";
  // for each VT grain in laminate ply
  // strain
  else if (i == STRAIN_XX_LAMVTMT1) return "STRAIN_XX_LAMVTMT1";
  else if (i == STRAIN_XX_LAMVTMT2) return "STRAIN_XX_LAMVTMT2";
  else if (i == STRAIN_XX_LAMVTMT3) return "STRAIN_XX_LAMVTMT3";
  else if (i == STRAIN_XX_LAMVTMT4) return "STRAIN_XX_LAMVTMT4";
  else if (i == STRAIN_XX_LAMVTMT5) return "STRAIN_XX_LAMVTMT5";
  else if (i == STRAIN_XX_LAMVTMT6) return "STRAIN_XX_LAMVTMT6";
  else if (i == STRAIN_XX_LAMVTMT7) return "STRAIN_XX_LAMVTMT7";
  else if (i == STRAIN_XX_LAMVTMT8) return "STRAIN_XX_LAMVTMT8";
  else if (i == STRAIN_XX_LAMVTMT9) return "STRAIN_XX_LAMVTMT9";
  else if (i == STRAIN_XX_LAMVTMT10) return "STRAIN_XX_LAMVTMT10";
  else if (i == STRAIN_XX_LAMVTMT11) return "STRAIN_XX_LAMVTMT11";
  else if (i == STRAIN_XX_LAMVTMT12) return "STRAIN_XX_LAMVTMT12";
  else if (i == STRAIN_XX_LAMVTMT13) return "STRAIN_XX_LAMVTMT13";
  else if (i == STRAIN_XX_LAMVTMT14) return "STRAIN_XX_LAMVTMT14";
  else if (i == STRAIN_XX_LAMVTMT15) return "STRAIN_XX_LAMVTMT15";
  else if (i == STRAIN_XX_LAMVTMT16) return "STRAIN_XX_LAMVTMT16";
  else if (i == STRAIN_XX_LAMVTMT17) return "STRAIN_XX_LAMVTMT17";
  else if (i == STRAIN_XX_LAMVTMT18) return "STRAIN_XX_LAMVTMT18";
  else if (i == STRAIN_XX_LAMVTMT19) return "STRAIN_XX_LAMVTMT19";
  else if (i == STRAIN_XX_LAMVTMT20) return "STRAIN_XX_LAMVTMT20";
  // stress
  else if (i == STRESS_XX_LAMVTMT1) return "STRESS_XX_LAMVTMT1";
  else if (i == STRESS_XX_LAMVTMT2) return "STRESS_XX_LAMVTMT2";
  else if (i == STRESS_XX_LAMVTMT3) return "STRESS_XX_LAMVTMT3";
  else if (i == STRESS_XX_LAMVTMT4) return "STRESS_XX_LAMVTMT4";
  else if (i == STRESS_XX_LAMVTMT5) return "STRESS_XX_LAMVTMT5";
  else if (i == STRESS_XX_LAMVTMT6) return "STRESS_XX_LAMVTMT6";
  else if (i == STRESS_XX_LAMVTMT7) return "STRESS_XX_LAMVTMT7";
  else if (i == STRESS_XX_LAMVTMT8) return "STRESS_XX_LAMVTMT8";
  else if (i == STRESS_XX_LAMVTMT9) return "STRESS_XX_LAMVTMT9";
  else if (i == STRESS_XX_LAMVTMT10) return "STRESS_XX_LAMVTMT10";
  else if (i == STRESS_XX_LAMVTMT11) return "STRESS_XX_LAMVTMT11";
  else if (i == STRESS_XX_LAMVTMT12) return "STRESS_XX_LAMVTMT12";
  else if (i == STRESS_XX_LAMVTMT13) return "STRESS_XX_LAMVTMT13";
  else if (i == STRESS_XX_LAMVTMT14) return "STRESS_XX_LAMVTMT14";
  else if (i == STRESS_XX_LAMVTMT15) return "STRESS_XX_LAMVTMT15";
  else if (i == STRESS_XX_LAMVTMT16) return "STRESS_XX_LAMVTMT16";
  else if (i == STRESS_XX_LAMVTMT17) return "STRESS_XX_LAMVTMT17";
  else if (i == STRESS_XX_LAMVTMT18) return "STRESS_XX_LAMVTMT18";
  else if (i == STRESS_XX_LAMVTMT19) return "STRESS_XX_LAMVTMT19";
  else if (i == STRESS_XX_LAMVTMT20) return "STRESS_XX_LAMVTMT20";
  // for each material phase
  // strain
  else if (i == STRAIN_XX_LAMVTMT1_MTX) return "STRAIN_XX_LAMVTMT1_MTX";
  else if (i == STRAIN_XX_LAMVTMT1_INC) return "STRAIN_XX_LAMVTMT1_INC";
  else if (i == STRAIN_XX_LAMVTMT2_MTX) return "STRAIN_XX_LAMVTMT2_MTX";
  else if (i == STRAIN_XX_LAMVTMT2_INC) return "STRAIN_XX_LAMVTMT2_INC";
  else if (i == STRAIN_XX_LAMVTMT3_MTX) return "STRAIN_XX_LAMVTMT3_MTX";
  else if (i == STRAIN_XX_LAMVTMT3_INC) return "STRAIN_XX_LAMVTMT3_INC";
  else if (i == STRAIN_XX_LAMVTMT4_MTX) return "STRAIN_XX_LAMVTMT4_MTX";
  else if (i == STRAIN_XX_LAMVTMT4_INC) return "STRAIN_XX_LAMVTMT4_INC";
  else if (i == STRAIN_XX_LAMVTMT5_MTX) return "STRAIN_XX_LAMVTMT5_MTX";
  else if (i == STRAIN_XX_LAMVTMT5_INC) return "STRAIN_XX_LAMVTMT5_INC";
  else if (i == STRAIN_XX_LAMVTMT6_MTX) return "STRAIN_XX_LAMVTMT6_MTX";
  else if (i == STRAIN_XX_LAMVTMT6_INC) return "STRAIN_XX_LAMVTMT6_INC";
  else if (i == STRAIN_XX_LAMVTMT7_MTX) return "STRAIN_XX_LAMVTMT7_MTX";
  else if (i == STRAIN_XX_LAMVTMT7_INC) return "STRAIN_XX_LAMVTMT7_INC";
  else if (i == STRAIN_XX_LAMVTMT8_MTX) return "STRAIN_XX_LAMVTMT8_MTX";
  else if (i == STRAIN_XX_LAMVTMT8_INC) return "STRAIN_XX_LAMVTMT8_INC";
  else if (i == STRAIN_XX_LAMVTMT9_MTX) return "STRAIN_XX_LAMVTMT9_MTX";
  else if (i == STRAIN_XX_LAMVTMT9_INC) return "STRAIN_XX_LAMVTMT9_INC";
  else if (i == STRAIN_XX_LAMVTMT10_MTX) return "STRAIN_XX_LAMVTMT10_MTX";
  else if (i == STRAIN_XX_LAMVTMT10_INC) return "STRAIN_XX_LAMVTMT10_INC";
  else if (i == STRAIN_XX_LAMVTMT11_MTX) return "STRAIN_XX_LAMVTMT11_MTX";
  else if (i == STRAIN_XX_LAMVTMT11_INC) return "STRAIN_XX_LAMVTMT11_INC";
  else if (i == STRAIN_XX_LAMVTMT12_MTX) return "STRAIN_XX_LAMVTMT12_MTX";
  else if (i == STRAIN_XX_LAMVTMT12_INC) return "STRAIN_XX_LAMVTMT12_INC";
  else if (i == STRAIN_XX_LAMVTMT13_MTX) return "STRAIN_XX_LAMVTMT13_MTX";
  else if (i == STRAIN_XX_LAMVTMT13_INC) return "STRAIN_XX_LAMVTMT13_INC";
  else if (i == STRAIN_XX_LAMVTMT14_MTX) return "STRAIN_XX_LAMVTMT14_MTX";
  else if (i == STRAIN_XX_LAMVTMT14_INC) return "STRAIN_XX_LAMVTMT14_INC";
  else if (i == STRAIN_XX_LAMVTMT15_MTX) return "STRAIN_XX_LAMVTMT15_MTX";
  else if (i == STRAIN_XX_LAMVTMT15_INC) return "STRAIN_XX_LAMVTMT15_INC";
  else if (i == STRAIN_XX_LAMVTMT16_MTX) return "STRAIN_XX_LAMVTMT16_MTX";
  else if (i == STRAIN_XX_LAMVTMT16_INC) return "STRAIN_XX_LAMVTMT16_INC";
  else if (i == STRAIN_XX_LAMVTMT17_MTX) return "STRAIN_XX_LAMVTMT17_MTX";
  else if (i == STRAIN_XX_LAMVTMT17_INC) return "STRAIN_XX_LAMVTMT17_INC";
  else if (i == STRAIN_XX_LAMVTMT18_MTX) return "STRAIN_XX_LAMVTMT18_MTX";
  else if (i == STRAIN_XX_LAMVTMT18_INC) return "STRAIN_XX_LAMVTMT18_INC";
  else if (i == STRAIN_XX_LAMVTMT19_MTX) return "STRAIN_XX_LAMVTMT19_MTX";
  else if (i == STRAIN_XX_LAMVTMT19_INC) return "STRAIN_XX_LAMVTMT19_INC";
  else if (i == STRAIN_XX_LAMVTMT20_MTX) return "STRAIN_XX_LAMVTMT20_MTX";
  else if (i == STRAIN_XX_LAMVTMT20_INC) return "STRAIN_XX_LAMVTMT20_INC";
  // stress
  else if (i == STRESS_XX_LAMVTMT1_MTX) return "STRESS_XX_LAMVTMT1_MTX";
  else if (i == STRESS_XX_LAMVTMT1_INC) return "STRESS_XX_LAMVTMT1_INC";
  else if (i == STRESS_XX_LAMVTMT2_MTX) return "STRESS_XX_LAMVTMT2_MTX";
  else if (i == STRESS_XX_LAMVTMT2_INC) return "STRESS_XX_LAMVTMT2_INC";
  else if (i == STRESS_XX_LAMVTMT3_MTX) return "STRESS_XX_LAMVTMT3_MTX";
  else if (i == STRESS_XX_LAMVTMT3_INC) return "STRESS_XX_LAMVTMT3_INC";
  else if (i == STRESS_XX_LAMVTMT4_MTX) return "STRESS_XX_LAMVTMT4_MTX";
  else if (i == STRESS_XX_LAMVTMT4_INC) return "STRESS_XX_LAMVTMT4_INC";
  else if (i == STRESS_XX_LAMVTMT5_MTX) return "STRESS_XX_LAMVTMT5_MTX";
  else if (i == STRESS_XX_LAMVTMT5_INC) return "STRESS_XX_LAMVTMT5_INC";
  else if (i == STRESS_XX_LAMVTMT6_MTX) return "STRESS_XX_LAMVTMT6_MTX";
  else if (i == STRESS_XX_LAMVTMT6_INC) return "STRESS_XX_LAMVTMT6_INC";
  else if (i == STRESS_XX_LAMVTMT7_MTX) return "STRESS_XX_LAMVTMT7_MTX";
  else if (i == STRESS_XX_LAMVTMT7_INC) return "STRESS_XX_LAMVTMT7_INC";
  else if (i == STRESS_XX_LAMVTMT8_MTX) return "STRESS_XX_LAMVTMT8_MTX";
  else if (i == STRESS_XX_LAMVTMT8_INC) return "STRESS_XX_LAMVTMT8_INC";
  else if (i == STRESS_XX_LAMVTMT9_MTX) return "STRESS_XX_LAMVTMT9_MTX";
  else if (i == STRESS_XX_LAMVTMT9_INC) return "STRESS_XX_LAMVTMT9_INC";
  else if (i == STRESS_XX_LAMVTMT10_MTX) return "STRESS_XX_LAMVTMT10_MTX";
  else if (i == STRESS_XX_LAMVTMT10_INC) return "STRESS_XX_LAMVTMT10_INC";
  else if (i == STRESS_XX_LAMVTMT11_MTX) return "STRESS_XX_LAMVTMT11_MTX";
  else if (i == STRESS_XX_LAMVTMT11_INC) return "STRESS_XX_LAMVTMT11_INC";
  else if (i == STRESS_XX_LAMVTMT12_MTX) return "STRESS_XX_LAMVTMT12_MTX";
  else if (i == STRESS_XX_LAMVTMT12_INC) return "STRESS_XX_LAMVTMT12_INC";
  else if (i == STRESS_XX_LAMVTMT13_MTX) return "STRESS_XX_LAMVTMT13_MTX";
  else if (i == STRESS_XX_LAMVTMT13_INC) return "STRESS_XX_LAMVTMT13_INC";
  else if (i == STRESS_XX_LAMVTMT14_MTX) return "STRESS_XX_LAMVTMT14_MTX";
  else if (i == STRESS_XX_LAMVTMT14_INC) return "STRESS_XX_LAMVTMT14_INC";
  else if (i == STRESS_XX_LAMVTMT15_MTX) return "STRESS_XX_LAMVTMT15_MTX";
  else if (i == STRESS_XX_LAMVTMT15_INC) return "STRESS_XX_LAMVTMT15_INC";
  else if (i == STRESS_XX_LAMVTMT16_MTX) return "STRESS_XX_LAMVTMT16_MTX";
  else if (i == STRESS_XX_LAMVTMT16_INC) return "STRESS_XX_LAMVTMT16_INC";
  else if (i == STRESS_XX_LAMVTMT17_MTX) return "STRESS_XX_LAMVTMT17_MTX";
  else if (i == STRESS_XX_LAMVTMT17_INC) return "STRESS_XX_LAMVTMT17_INC";
  else if (i == STRESS_XX_LAMVTMT18_MTX) return "STRESS_XX_LAMVTMT18_MTX";
  else if (i == STRESS_XX_LAMVTMT18_INC) return "STRESS_XX_LAMVTMT18_INC";
  else if (i == STRESS_XX_LAMVTMT19_MTX) return "STRESS_XX_LAMVTMT19_MTX";
  else if (i == STRESS_XX_LAMVTMT19_INC) return "STRESS_XX_LAMVTMT19_INC";
  else if (i == STRESS_XX_LAMVTMT20_MTX) return "STRESS_XX_LAMVTMT20_MTX";
  else if (i == STRESS_XX_LAMVTMT20_INC) return "STRESS_XX_LAMVTMT20_INC";
  // damage
  else if (i == DAMAGE_LAMVTMT1_MTX) return "DAMAGE_LAMVTMT1_MTX";
  else if (i == DAMAGE_LAMVTMT1_INC) return "DAMAGE_LAMVTMT1_INC";
  else if (i == DAMAGE_LAMVTMT2_MTX) return "DAMAGE_LAMVTMT2_MTX";
  else if (i == DAMAGE_LAMVTMT2_INC) return "DAMAGE_LAMVTMT2_INC";
  else if (i == DAMAGE_LAMVTMT3_MTX) return "DAMAGE_LAMVTMT3_MTX";
  else if (i == DAMAGE_LAMVTMT3_INC) return "DAMAGE_LAMVTMT3_INC";
  else if (i == DAMAGE_LAMVTMT4_MTX) return "DAMAGE_LAMVTMT4_MTX";
  else if (i == DAMAGE_LAMVTMT4_INC) return "DAMAGE_LAMVTMT4_INC";
  else if (i == DAMAGE_LAMVTMT5_MTX) return "DAMAGE_LAMVTMT5_MTX";
  else if (i == DAMAGE_LAMVTMT5_INC) return "DAMAGE_LAMVTMT5_INC";
  else if (i == DAMAGE_LAMVTMT6_MTX) return "DAMAGE_LAMVTMT6_MTX";
  else if (i == DAMAGE_LAMVTMT6_INC) return "DAMAGE_LAMVTMT6_INC";
  else if (i == DAMAGE_LAMVTMT7_MTX) return "DAMAGE_LAMVTMT7_MTX";
  else if (i == DAMAGE_LAMVTMT7_INC) return "DAMAGE_LAMVTMT7_INC";
  else if (i == DAMAGE_LAMVTMT8_MTX) return "DAMAGE_LAMVTMT8_MTX";
  else if (i == DAMAGE_LAMVTMT8_INC) return "DAMAGE_LAMVTMT8_INC";
  else if (i == DAMAGE_LAMVTMT9_MTX) return "DAMAGE_LAMVTMT9_MTX";
  else if (i == DAMAGE_LAMVTMT9_INC) return "DAMAGE_LAMVTMT9_INC";
  else if (i == DAMAGE_LAMVTMT10_MTX) return "DAMAGE_LAMVTMT10_MTX";
  else if (i == DAMAGE_LAMVTMT10_INC) return "DAMAGE_LAMVTMT10_INC";
  else if (i == DAMAGE_LAMVTMT11_MTX) return "DAMAGE_LAMVTMT11_MTX";
  else if (i == DAMAGE_LAMVTMT11_INC) return "DAMAGE_LAMVTMT11_INC";
  else if (i == DAMAGE_LAMVTMT12_MTX) return "DAMAGE_LAMVTMT12_MTX";
  else if (i == DAMAGE_LAMVTMT12_INC) return "DAMAGE_LAMVTMT12_INC";
  else if (i == DAMAGE_LAMVTMT13_MTX) return "DAMAGE_LAMVTMT13_MTX";
  else if (i == DAMAGE_LAMVTMT13_INC) return "DAMAGE_LAMVTMT13_INC";
  else if (i == DAMAGE_LAMVTMT14_MTX) return "DAMAGE_LAMVTMT14_MTX";
  else if (i == DAMAGE_LAMVTMT14_INC) return "DAMAGE_LAMVTMT14_INC";
  else if (i == DAMAGE_LAMVTMT15_MTX) return "DAMAGE_LAMVTMT15_MTX";
  else if (i == DAMAGE_LAMVTMT15_INC) return "DAMAGE_LAMVTMT15_INC";
  else if (i == DAMAGE_LAMVTMT16_MTX) return "DAMAGE_LAMVTMT16_MTX";
  else if (i == DAMAGE_LAMVTMT16_INC) return "DAMAGE_LAMVTMT16_INC";
  else if (i == DAMAGE_LAMVTMT17_MTX) return "DAMAGE_LAMVTMT17_MTX";
  else if (i == DAMAGE_LAMVTMT17_INC) return "DAMAGE_LAMVTMT17_INC";
  else if (i == DAMAGE_LAMVTMT18_MTX) return "DAMAGE_LAMVTMT18_MTX";
  else if (i == DAMAGE_LAMVTMT18_INC) return "DAMAGE_LAMVTMT18_INC";
  else if (i == DAMAGE_LAMVTMT19_MTX) return "DAMAGE_LAMVTMT19_MTX";
  else if (i == DAMAGE_LAMVTMT19_INC) return "DAMAGE_LAMVTMT19_INC";
  else if (i == DAMAGE_LAMVTMT20_MTX) return "DAMAGE_LAMVTMT20_MTX";
  else if (i == DAMAGE_LAMVTMT20_INC) return "DAMAGE_LAMVTMT20_INC";
  else if (i == BODYFORCE_X) return "BODYFORCE_X";
  else if (i == BODYFORCE_Y) return "BODYFORCE_Y";
  else if (i == BODYFORCE_Z) return "BODYFORCE_Z";
  else if (i == dFmdFM_XXXX) return "dFmdFM_XXXX";
  else if (i == dFmdFM_XXXY) return "dFmdFM_XXXY";
  else if (i == dFmdFM_XXXZ) return "dFmdFM_XXXZ";
  else if (i == dFmdFM_XXYX) return "dFmdFM_XXYX";
  else if (i == dFmdFM_XXYY) return "dFmdFM_XXYY";
  else if (i == dFmdFM_XXYZ) return "dFmdFM_XXYZ";
  else if (i == dFmdFM_XXZX) return "dFmdFM_XXZX";
  else if (i == dFmdFM_XXZY) return "dFmdFM_XXZY";
  else if (i == dFmdFM_XXZZ) return "dFmdFM_XXZZ";
  else if (i == dFmdFM_XYXX) return "dFmdFM_XYXX";
  else if (i == dFmdFM_XYXY) return "dFmdFM_XYXY";
  else if (i == dFmdFM_XYXZ) return "dFmdFM_XYXZ";
  else if (i == dFmdFM_XYYX) return "dFmdFM_XYYX";
  else if (i == dFmdFM_XYYY) return "dFmdFM_XYYY";
  else if (i == dFmdFM_XYYZ) return "dFmdFM_XYYZ";
  else if (i == dFmdFM_XYZX) return "dFmdFM_XYZX";
  else if (i == dFmdFM_XYZY) return "dFmdFM_XYZY";
  else if (i == dFmdFM_XYZZ) return "dFmdFM_XYZZ";
  else if (i == dFmdFM_XZXX) return "dFmdFM_XZXX";
  else if (i == dFmdFM_XZXY) return "dFmdFM_XZXY";
  else if (i == dFmdFM_XZXZ) return "dFmdFM_XZXZ";
  else if (i == dFmdFM_XZYX) return "dFmdFM_XZYX";
  else if (i == dFmdFM_XZYY) return "dFmdFM_XZYY";
  else if (i == dFmdFM_XZYZ) return "dFmdFM_XZYZ";
  else if (i == dFmdFM_XZZX) return "dFmdFM_XZZX";
  else if (i == dFmdFM_XZZY) return "dFmdFM_XZZY";
  else if (i == dFmdFM_XZZZ) return "dFmdFM_XZZZ";
  else if (i == dFmdFM_YXXX) return "dFmdFM_YXXX";
  else if (i == dFmdFM_YXXY) return "dFmdFM_YXXY";
  else if (i == dFmdFM_YXXZ) return "dFmdFM_YXXZ";
  else if (i == dFmdFM_YXYX) return "dFmdFM_YXYX";
  else if (i == dFmdFM_YXYY) return "dFmdFM_YXYY";
  else if (i == dFmdFM_YXYZ) return "dFmdFM_YXYZ";
  else if (i == dFmdFM_YXZX) return "dFmdFM_YXZX";
  else if (i == dFmdFM_YXZY) return "dFmdFM_YXZY";
  else if (i == dFmdFM_YXZZ) return "dFmdFM_YXZZ";
  else if (i == dFmdFM_YYXX) return "dFmdFM_YYXX";
  else if (i == dFmdFM_YYXY) return "dFmdFM_YYXY";
  else if (i == dFmdFM_YYXZ) return "dFmdFM_YYXZ";
  else if (i == dFmdFM_YYYX) return "dFmdFM_YYYX";
  else if (i == dFmdFM_YYYY) return "dFmdFM_YYYY";
  else if (i == dFmdFM_YYYZ) return "dFmdFM_YYYZ";
  else if (i == dFmdFM_YYZX) return "dFmdFM_YYZX";
  else if (i == dFmdFM_YYZY) return "dFmdFM_YYZY";
  else if (i == dFmdFM_YYZZ) return "dFmdFM_YYZZ";
  else if (i == dFmdFM_YZXX) return "dFmdFM_YZXX";
  else if (i == dFmdFM_YZXY) return "dFmdFM_YZXY";
  else if (i == dFmdFM_YZXZ) return "dFmdFM_YZXZ";
  else if (i == dFmdFM_YZYX) return "dFmdFM_YZYX";
  else if (i == dFmdFM_YZYY) return "dFmdFM_YZYY";
  else if (i == dFmdFM_YZYZ) return "dFmdFM_YZYZ";
  else if (i == dFmdFM_YZZX) return "dFmdFM_YZZX";
  else if (i == dFmdFM_YZZY) return "dFmdFM_YZZY";
  else if (i == dFmdFM_YZZZ) return "dFmdFM_YZZZ";
  else if (i == dFmdFM_ZXXX) return "dFmdFM_ZXXX";
  else if (i == dFmdFM_ZXXY) return "dFmdFM_ZXXY";
  else if (i == dFmdFM_ZXXZ) return "dFmdFM_ZXXZ";
  else if (i == dFmdFM_ZXYX) return "dFmdFM_ZXYX";
  else if (i == dFmdFM_ZXYY) return "dFmdFM_ZXYY";
  else if (i == dFmdFM_ZXYZ) return "dFmdFM_ZXYZ";
  else if (i == dFmdFM_ZXZX) return "dFmdFM_ZXZX";
  else if (i == dFmdFM_ZXZY) return "dFmdFM_ZXZY";
  else if (i == dFmdFM_ZXZZ) return "dFmdFM_ZXZZ";
  else if (i == dFmdFM_ZYXX) return "dFmdFM_ZYXX";
  else if (i == dFmdFM_ZYXY) return "dFmdFM_ZYXY";
  else if (i == dFmdFM_ZYXZ) return "dFmdFM_ZYXZ";
  else if (i == dFmdFM_ZYYX) return "dFmdFM_ZYYX";
  else if (i == dFmdFM_ZYYY) return "dFmdFM_ZYYY";
  else if (i == dFmdFM_ZYYZ) return "dFmdFM_ZYYZ";
  else if (i == dFmdFM_ZYZX) return "dFmdFM_ZYZX";
  else if (i == dFmdFM_ZYZY) return "dFmdFM_ZYZY";
  else if (i == dFmdFM_ZYZZ) return "dFmdFM_ZYZZ";
  else if (i == dFmdFM_ZZXX) return "dFmdFM_ZZXX";
  else if (i == dFmdFM_ZZXY) return "dFmdFM_ZZXY";
  else if (i == dFmdFM_ZZXZ) return "dFmdFM_ZZXZ";
  else if (i == dFmdFM_ZZYX) return "dFmdFM_ZZYX";
  else if (i == dFmdFM_ZZYY) return "dFmdFM_ZZYY";
  else if (i == dFmdFM_ZZYZ) return "dFmdFM_ZZYZ";
  else if (i == dFmdFM_ZZZX) return "dFmdFM_ZZZX";
  else if (i == dFmdFM_ZZZY) return "dFmdFM_ZZZY";
  else if (i == dFmdFM_ZZZZ) return "dFmdFM_ZZZZ";
  else if (i == S_XX) return "S_XX";
  else if (i == S_XY) return "S_XY";
  else if (i == S_XZ) return "S_XZ";
  else if (i == S_YY) return "S_YY";
  else if (i == S_YZ) return "S_YZ";
  else if (i == S_ZZ) return "S_ZZ";
  else if (i == G_XXX) return "G_XXX";
  else if (i == G_XXY) return "G_XXY";
  else if (i == G_XXZ) return "G_XXZ";
  else if (i == G_XYX) return "G_XYX";
  else if (i == G_XYY) return "G_XYY";
  else if (i == G_XYZ) return "G_XYZ";
  else if (i == G_XZX) return "G_XZX";
  else if (i == G_XZY) return "G_XZY";
  else if (i == G_XZZ) return "G_XZZ";
  else if (i == G_YXX) return "G_YXX";
  else if (i == G_YXY) return "G_YXY";
  else if (i == G_YXZ) return "G_YXZ";
  else if (i == G_YYX) return "G_YYX";
  else if (i == G_YYY) return "G_YYY";
  else if (i == G_YYZ) return "G_YYZ";
  else if (i == G_YZX) return "G_YZX";
  else if (i == G_YZY) return "G_YZY";
  else if (i == G_YZZ) return "G_YZZ";
  else if (i == G_ZXX) return "G_ZXX";
  else if (i == G_ZXY) return "G_ZXY";
  else if (i == G_ZXZ) return "G_ZXZ";
  else if (i == G_ZYX) return "G_ZYX";
  else if (i == G_ZYY) return "G_ZYY";
  else if (i == G_ZYZ) return "G_ZYZ";
  else if (i == G_ZZX) return "G_ZZX";
  else if (i == G_ZZY) return "G_ZZY";
  else if (i == G_ZZZ) return "G_ZZZ";
  else if (i == Q_XXX) return "Q_XXX";
  else if (i == Q_XXY) return "Q_XXY";
  else if (i == Q_XXZ) return "Q_XXZ";
  else if (i == Q_XYX) return "Q_XYX";
  else if (i == Q_XYY) return "Q_XYY";
  else if (i == Q_XYZ) return "Q_XYZ";
  else if (i == Q_XZX) return "Q_XZX";
  else if (i == Q_XZY) return "Q_XZY";
  else if (i == Q_XZZ) return "Q_XZZ";
  else if (i == Q_YXX) return "Q_YXX";
  else if (i == Q_YXY) return "Q_YXY";
  else if (i == Q_YXZ) return "Q_YXZ";
  else if (i == Q_YYX) return "Q_YYX";
  else if (i == Q_YYY) return "Q_YYY";
  else if (i == Q_YYZ) return "Q_YYZ";
  else if (i == Q_YZX) return "Q_YZX";
  else if (i == Q_YZY) return "Q_YZY";
  else if (i == Q_YZZ) return "Q_YZZ";
  else if (i == Q_ZXX) return "Q_ZXX";
  else if (i == Q_ZXY) return "Q_ZXY";
  else if (i == Q_ZXZ) return "Q_ZXZ";
  else if (i == Q_ZYX) return "Q_ZYX";
  else if (i == Q_ZYY) return "Q_ZYY";
  else if (i == Q_ZYZ) return "Q_ZYZ";
  else if (i == Q_ZZX) return "Q_ZZX";
  else if (i == Q_ZZY) return "Q_ZZY";
  else if (i == Q_ZZZ) return "Q_ZZZ";
  else if (i == ISO_YIELD) return "ISO_YIELD";
  else if (i == ISO_YIELD_TENSILE) return "ISO_YIELD_TENSILE";
  else if (i == ISO_YIELD_COMPRESSION) return "ISO_YIELD_COMPRESSION";
  else if (i == ISO_YIELD_SHEAR) return "ISO_YIELD_SHEAR";
  else if (i == KIN_YIELD) return "KIN_YIELD";
  else if (i == MTX_SVM) return "MTX_SVM";
  else if (i == MTX_SIG_XX) return "MTX_SIG_XX";
  else if (i == MTX_SIG_YY) return "MTX_SIG_YY";
  else if (i == MTX_SIG_ZZ) return "MTX_SIG_ZZ";
  else if (i == MTX_SIG_XY) return "MTX_SIG_XY";
  else if (i == MTX_SIG_YZ) return "MTX_SIG_YZ";
  else if (i == MTX_SIG_XZ) return "MTX_SIG_XZ";
  else if (i == MTX_DAMAGE) return "MTX_DAMAGE";
  else if (i == INC_SVM) return "INC_SVM";
  else if (i == INC_SIG_XX) return "INC_SIG_XX";
  else if (i == INC_SIG_YY) return "INC_SIG_YY";
  else if (i == INC_SIG_ZZ) return "INC_SIG_ZZ";
  else if (i == INC_SIG_XY) return "INC_SIG_XY";
  else if (i == INC_SIG_YZ) return "INC_SIG_YZ";
  else if (i == INC_SIG_XZ) return "INC_SIG_XZ";
  else if (i == INC_DAMAGE) return "INC_DAMAGE";
  else if (i == TEMPERATURE) return "TEMPERATURE";
  else if (i == THERMALFLUX_X) return "THERMALFLUX_X";
  else if (i == THERMALFLUX_Y) return "THERMALFLUX_Y";
  else if (i == THERMALFLUX_Z) return "THERMALFLUX_Z";
  else if (i == THERMALSOURCE) return "THERMALSOURCE";
  else if (i == VOLTAGE) return "VOLTAGE";
  else if (i == ELECTRICALFLUX_X) return "ELECTRICALFLUX_X";
  else if (i == ELECTRICALFLUX_Y) return "ELECTRICALFLUX_Y";
  else if (i == ELECTRICALFLUX_Z) return "ELECTRICALFLUX_Z";
  else if (i == ELECTRICALSOURCE) return "ELECTRICALSOURCE";
  else if (i == MAGNETICVECTORPOTENTIAL_X) return "MAGNETICVECTORPOTENTIAL_X";
  else if (i == MAGNETICVECTORPOTENTIAL_Y) return "MAGNETICVECTORPOTENTIAL_Y";
  else if (i == MAGNETICVECTORPOTENTIAL_Z) return "MAGNETICVECTORPOTENTIAL_Z";
  else if (i == MAGNETICINDUCTION_X) return "MAGNETICINDUCTION_X";
  else if (i == MAGNETICINDUCTION_Y) return "MAGNETICINDUCTION_Y";
  else if (i == MAGNETICINDUCTION_Z) return "MAGNETICINDUCTION_Z";
  else if (i == MAGNETICFIELD_X) return "MAGNETICFIELD_X";
  else if (i == MAGNETICFIELD_Y) return "MAGNETICFIELD_Y";
  else if (i == MAGNETICFIELD_Z) return "MAGNETICFIELD_Z";
  else if (i == EMSOURCEVECTORFIELD_X) return "EMSOURCEVECTORFIELD_X";
  else if (i == EMSOURCEVECTORFIELD_Y) return "EMSOURCEVECTORFIELD_Y";
  else if (i == EMSOURCEVECTORFIELD_Z) return "EMSOURCEVECTORFIELD_Z";
  else if (i == INDUCTORSOURCEVECTORFIELD_X) return "INDUCTORSOURCEVECTORFIELD_X";
  else if (i == INDUCTORSOURCEVECTORFIELD_Y) return "INDUCTORSOURCEVECTORFIELD_Y";
  else if (i == INDUCTORSOURCEVECTORFIELD_Z) return "INDUCTORSOURCEVECTORFIELD_Z";
  else if (i == EMFIELDSOURCE) return "EMFIELDSOURCE";
  else if (i == LOCAL_POROSITY) return "LOCAL_POROSITY";
  else if (i == NONLOCAL_POROSITY) return "NONLOCAL_POROSITY";
  else if (i == CORRECTED_POROSITY) return "CORRECTED_POROSITY";
  else if (i == YIELD_POROSITY) return "YIELD_POROSITY";
  else if (i == NUCLEATED_POROSITY_TOT) return "NUCLEATED_POROSITY_TOT";
  else if (i == NUCLEATED_POROSITY_0) return "NUCLEATED_POROSITY_0";
  else if (i == NUCLEATED_POROSITY_1) return "NUCLEATED_POROSITY_1";
  else if (i == NUCLEATED_POROSITY_2) return "NUCLEATED_POROSITY_2";
  else if (i == NUCLEATION_ONSET_0) return "NUCLEATION_ONSET_0";
  else if (i == NUCLEATION_ONSET_1) return "NUCLEATION_ONSET_1";
  else if (i == NUCLEATION_ONSET_2) return "NUCLEATION_ONSET_2";
  else if (i == PRESSION) return "PRESSION";
  else if (i == PLASTIC_POISSON_RATIO) return "PLASTIC_POISSON_RATIO";
  else if (i == PLASTICSTRAIN_COMPRESSION) return "PLASTICSTRAIN_COMPRESSION";
  else if (i == PLASTICSTRAIN_TRACTION) return "PLASTICSTRAIN_TRACTION";
  else if (i == PLASTICSTRAIN_SHEAR) return "PLASTICSTRAIN_SHEAR";
  else if (i == NONLOCAL_PLASTICSTRAIN) return "NONLOCAL_PLASTICSTRAIN";
  else if (i == BACKSTRESS) return "BACKSTRESS";
  else if (i == JACOBIAN) return "JACOBIAN";
  else if (i == ELASTIC_JACOBIAN) return "ELASTIC_JACOBIAN";
  else if (i == PLASTIC_JACOBIAN) return "PLASTIC_JACOBIAN";
  else if (i == PLASTIC_RATE) return "PLASTIC_RATE";
  else if (i == FLOW_FACTOR_RATE) return "FLOW_FACTOR_RATE";
  else if (i == OCTAHEDRAL_SHEAR_STRAIN_RATE) return "OCTAHEDRAL_SHEAR_STRAIN_RATE";
  else if (i == OCTAHEDRAL_NORM_STRAIN_RATE) return "OCTAHEDRAL_NORM_STRAIN_RATE";
  else if (i == K_XX) return "K_XX";
  else if (i == K_YY) return "K_YY";
  else if (i == K_ZZ) return "K_ZZ";
  else if (i == K_XY) return "K_XY";
  else if (i == K_YZ) return "K_YZ";
  else if (i == K_XZ) return "K_XZ";
  else if (i == KVM) return "KVM";
  else if (i == FAILURE_ONSET) return "FAILURE_ONSET";
  else if (i == GL_EQUIVALENT_STRAIN_RATE) return "GL_EQUIVALENT_STRAIN_RATE";
  else if (i == NONLOCAL_FAILURE_PLASTICSTRAIN) return "NONLOCAL_FAILURE_PLASTICSTRAIN";
  else if (i == FAILURE_PLASTICSTRAIN) return "FAILURE_PLASTICSTRAIN";
  else if (i == DAMAGE_0) return "DAMAGE_0";
  else if (i == DAMAGE_1) return "DAMAGE_1";
  else if (i == LOCAL_EQUIVALENT_STRAINS) return "LOCAL_EQUIVALENT_STRAINS";
  else if (i == NONLOCAL_EQUIVALENT_STRAINS)  return "NONLOCAL_EQUIVALENT_STRAINS";
  else if (i == FIRST_PRINCIPAL_STRESS) return "FIRST_PRINCIPAL_STRESS";
  else if (i == SECOND_PRINCIPAL_STRESS) return "SECOND_PRINCIPAL_STRESS";
  else if (i == THIRD_PRINCIPAL_STRESS) return "THIRD_PRINCIPAL_STRESS";
  else if (i == GL_EFFECTIVE_STRAIN_RATE) return "GL_EFFECTIVE_STRAIN_RATE";
  else if (i == DISP_JUMP_X) return "DISP_JUMP_X";
  else if (i == DISP_JUMP_Y) return "DISP_JUMP_Y";
  else if (i == DISP_JUMP_Z) return "DISP_JUMP_Z";
  else if (i == STRESS_TRIAXIALITY) return "STRESS_TRIAXIALITY";
  else if (i == ACTIVE_DISSIPATION) return "ACTIVE_DISSIPATION";
  else if (i == FIELD_CAPACITY) return "FIELD_CAPACITY";
  else if (i == MECHANICAL_SOURCE) return "MECHANICAL_SOURCE";
  else if (i == FIELD_SOURCE) return "FIELD_SOURCE";
  else if (i == INCOMPATIBLE_STRAIN_X) return "INCOMPATIBLE_STRAIN_X";
  else if (i == INCOMPATIBLE_STRAIN_Y) return "INCOMPATIBLE_STRAIN_Y";
  else if (i == INCOMPATIBLE_STRAIN_Z) return "INCOMPATIBLE_STRAIN_Z";
  else if (i == COHESIVE_JUMP_X) return "COHESIVE_JUMP_X";
  else if (i == COHESIVE_JUMP_Y) return "COHESIVE_JUMP_Y";
  else if (i == COHESIVE_JUMP_Z) return "COHESIVE_JUMP_Z";
  else if (i == DAMAGE_IS_BLOCKED) return "DAMAGE_IS_BLOCKED";
  else if (i == DEFO_ENERGY) return "DEFO_ENERGY";
  else if (i == PLASTIC_ENERGY) return "PLASTIC_ENERGY";
  else if (i == DAMAGE_ENERGY) return "DAMAGE_ENERGY";
  else if (i == IRREVERSIBLE_ENERGY) return "IRREVERSIBLE_ENERGY";
  else if (i == MTX_STRAIN_XX) return "MTX_STRAIN_XX";
  else if (i == MTX_STRAIN_YY) return "MTX_STRAIN_YY";
  else if (i == MTX_STRAIN_ZZ) return "MTX_STRAIN_ZZ";
  else if (i == MTX_STRAIN_XY) return "MTX_STRAIN_XY";
  else if (i == MTX_STRAIN_YZ) return "MTX_STRAIN_YZ";
  else if (i == MTX_STRAIN_XZ) return "MTX_STRAIN_XZ";
  else if (i == INC_STRAIN_XX) return "INC_STRAIN_XX";
  else if (i == INC_STRAIN_YY) return "INC_STRAIN_YY";
  else if (i == INC_STRAIN_ZZ) return "INC_STRAIN_ZZ";
  else if (i == INC_STRAIN_XY) return "INC_STRAIN_XY";
  else if (i == INC_STRAIN_YZ) return "INC_STRAIN_YZ";
  else if (i == INC_STRAIN_XZ) return "INC_STRAIN_XZ";
  else if (i == EXTRAFIELD_1) return "EXTRAFIELD_1";
  else if (i == EXTRAFIELD_2) return "EXTRAFIELD_2";
  else if (i == EXTRAFIELD_3) return "EXTRAFIELD_3";
  else if (i == EXTRAFIELD_4) return "EXTRAFIELD_4";
  else if (i == EXTRAFIELD_5) return "EXTRAFIELD_5";
  else if (i == EXTRAFIELD_6) return "EXTRAFIELD_6";
  else if (i == EXTRAFIELD_7) return "EXTRAFIELD_7";
  else if (i == EXTRAFIELD_8) return "EXTRAFIELD_8";
  else if (i == EXTRAFIELD_9) return "EXTRAFIELD_9";
  else if (i == EXTRAFIELD_NORM) return "EXTRAFIELD_NORM";
  else if (i == DELETED) return "DELETED";
  else if (i == LOST_ELLIPTICITY) return "LOST_ELLIPTICITY";
  else if (i == LOCAL_0) return "LOCAL_0";
  else if (i == LOCAL_1) return "LOCAL_1";
  else if (i == LOCAL_2) return "LOCAL_2";
  else if (i == LOCAL_3) return "LOCAL_3";
  else if (i == LOCAL_4) return "LOCAL_4";
  else if (i == LOCAL_5) return "LOCAL_5";
  else if (i == LOCAL_6) return "LOCAL_6";
  else if (i == LOCAL_7) return "LOCAL_7";
  else if (i == LOCAL_8) return "LOCAL_8";
  else if (i == LOCAL_9) return "LOCAL_9";
  else if (i == NONLOCAL_0) return "NONLOCAL_0";
  else if (i == NONLOCAL_1) return "NONLOCAL_1";
  else if (i == NONLOCAL_2) return "NONLOCAL_2";
  else if (i == NONLOCAL_3) return "NONLOCAL_3";
  else if (i == NONLOCAL_4) return "NONLOCAL_4";
  else if (i == NONLOCAL_5) return "NONLOCAL_5";
  else if (i == NONLOCAL_6) return "NONLOCAL_6";
  else if (i == NONLOCAL_7) return "NONLOCAL_7";
  else if (i == NONLOCAL_8) return "NONLOCAL_8";
  else if (i == NONLOCAL_9) return "NONLOCAL_9";
  else if (i == FAILED) return "FAILED";
  else if (i == COALESCENCE_ACTIVE) return "COALESCENCE_ACTIVE";
  else if (i == COALESCENCE) return "COALESCENCE";
  else if (i == COALESCENCE_SHEAR) return "COALESCENCE_SHEAR";
  else if (i == COALESCENCE_NECKING) return "COALESCENCE_NECKING";
  else if (i == COALESCENCE_CRITERION) return "COALESCENCE_CRITERION";
  else if (i == LIGAMENT_RATIO) return "LIGAMENT_RATIO";
  else if (i == PLASTIC_INSTABILITY_VAL) return "PLASTIC_INSTABILITY_VAL";
  else if (i == POROSITY_COALESCENCE_ONSET) return "POROSITY_COALESCENCE_ONSET";
  else if (i == CL_XX) return "CL_XX";
  else if (i == CL_YY) return "CL_YY";
  else if (i == CL_ZZ) return "CL_ZZ";
  else if (i == VOLUMETRIC_PLASTIC_STRAIN) return "VOLUMETRIC_PLASTIC_STRAIN";
  else if (i == LODE_PARAMETER) return "LODE_PARAMETER";
  else if (i == OMEGA_PARAMETER) return "OMEGA_PARAMETER";
  else if (i == DEVIATORIC_PLASTIC_STRAIN) return "DEVIATORIC_PLASTIC_STRAIN";
  else if (i == ASPECT_RATIO) return "ASPECT_RATIO";
  else if (i == SHAPE_FACTOR) return "SHAPE_FACTOR";
  else if (i == LIGAMENT_RATIO_COALESCENCE_ONSET) return "LIGAMENT_RATIO_COALESCENCE_ONSET";
  else if (i == ASPECT_RATIO_COALESCENCE_ONSET) return "ASPECT_RATIO_COALESCENCE_ONSET";
  else if (i == SHAPE_FACTOR_COALESCENCE_ONSET) return "SHAPE_FACTOR_COALESCENCE_ONSET";
  else if (i == INC_VOLUME_RATIO) return "INC_VOLUME_RATIO";
  else if (i == INC_ASPECT_RATIO_1) return "INC_ASPECT_RATIO_1";
  else if (i == INC_ASPECT_RATIO_2) return "INC_ASPECT_RATIO_2";
  else if (i == INC_R_XX) return "INC_R_XX";
  else if (i == INC_R_XY) return "INC_R_XY";
  else if (i == INC_R_XZ) return "INC_R_XZ";
  else if (i == INC_R_YX) return "INC_R_YX";
  else if (i == INC_R_YY) return "INC_R_YY";
  else if (i == INC_R_YZ) return "INC_R_YZ";
  else if (i == INC_R_ZX) return "INC_R_ZX";
  else if (i == INC_R_ZY) return "INC_R_ZY";
  else if (i == INC_R_ZZ) return "INC_R_ZZ";

  //phenomenologicalSMP
  else if (i == FthG_XX) return "FthG_XX";
  else if (i == FthG_YY) return "FthG_YY";
  else if (i == FthG_ZZ) return "FthG_ZZ";
  else if (i == FthG_XY) return "FthG_XY";
  else if (i == FthG_YZ) return "FthG_YZ";
  else if (i == FthG_ZX) return "FthG_ZX";

  else if (i == FthR_XX) return "FthR_XX";
  else if (i == FthR_YY) return "FthR_YY";
  else if (i == FthR_ZZ) return "FthR_ZZ";
  else if (i == FthR_XY) return "FthR_XY";
  else if (i == FthR_YZ) return "FthR_YZ";
  else if (i == FthR_ZX) return "FthR_ZX";

  else if (i == FthI_XX) return "FthCisot_XX";
  else if (i == FthI_YY) return "FthCisot_YY";
  else if (i == FthI_ZZ) return "FthCisot_ZZ";
  else if (i == FthI_XY) return "FthCisot_XY";
  else if (i == FthI_YZ) return "FthCisot_YZ";
  else if (i == FthI_ZX) return "FthCisot_ZX";
#if 0
  else if (i == FthAI_XX) return "FthAI_XX";
  else if (i == FthAI_YY) return "FthAI_YY";
  else if (i == FthAI_ZZ) return "FthAI_ZZ";
  else if (i == FthAI_XY) return "FthAI_XY";
  else if (i == FthAI_YZ) return "FthAI_YZ";
  else if (i == FthAI_ZX) return "FthAI_ZX";
#endif
  else if (i == FthAD_XX) return "FthAD_XX";
  else if (i == FthAD_YY) return "FthAD_YY";
  else if (i == FthAD_ZZ) return "FthAD_ZZ";
  else if (i == FthAD_XY) return "FthAD_XY";
  else if (i == FthAD_YZ) return "FthAD_YZ";
  else if (i == FthAD_ZX) return "FthAD_ZX";

  else if (i == FfG_XX) return "FfG_XX";
  else if (i == FfG_YY) return "FfG_YY";
  else if (i == FfG_ZZ) return "FfG_ZZ";
  else if (i == FfG_XY) return "FfG_XY";
  else if (i == FfG_YZ) return "FfG_YZ";
  else if (i == FfG_ZX) return "FfG_ZX";

  else if (i == FpG_XX) return "FpG_XX";
  else if (i == FpG_YY) return "FpG_YY";
  else if (i == FpG_ZZ) return "FpG_ZZ";
  else if (i == FpG_XY) return "FpG_XY";
  else if (i == FpG_YZ) return "FpG_YZ";
  else if (i == FpG_ZX) return "FpG_ZX";

  else if (i == FveG_XX) return "FveG_XX";
  else if (i == FveG_YY) return "FveG_YY";
  else if (i == FveG_ZZ) return "FveG_ZZ";
  else if (i == FveG_XY) return "FveG_XY";
  else if (i == FveG_YZ) return "FveG_YZ";
  else if (i == FveG_ZX) return "FveG_ZX";

  else if (i == FP_XX) return "FP_XX";
  else if (i == FP_YY) return "FP_YY";
  else if (i == FP_ZZ) return "FP_ZZ";
  else if (i == FP_XY) return "FP_XY";
  else if (i == FP_YX) return "FP_YX";
  else if (i == FP_YZ) return "FP_YZ";
  else if (i == FP_ZY) return "FP_ZY";
  else if (i == FP_ZX) return "FP_ZX";
  else if (i == FP_XZ) return "FP_XZ";

  else if (i == FpR_XX) return "FpR_XX";
  else if (i == FpR_YY) return "FpR_YY";
  else if (i == FpR_ZZ) return "FpR_ZZ";
  else if (i == FpR_XY) return "FpR_XY";
  else if (i == FpR_YZ) return "FpR_YZ";
  else if (i == FpR_ZX) return "FpR_ZX";

  else if (i == FveR_XX) return "FveR_XX";
  else if (i == FveR_YY) return "FveR_YY";
  else if (i == FveR_ZZ) return "FveR_ZZ";
  else if (i == FveR_XY) return "FveR_XY";
  else if (i == FveR_YZ) return "FveR_YZ";
  else if (i == FveR_ZX) return "FveR_ZX";

  else if (i == ZG) return "ZG";
  else if (i == PDF) return "PDF";
  else if (i == TT) return "TT";
  else if (i == WT) return "WT";

  else if (i == FthAM_XX) return "FthAM_XX";
  else if (i == FthAM_YY) return "FthAM_YY";
  else if (i == FthAM_ZZ) return "FthAM_ZZ";
  else if (i == FthAM_XY) return "FthAM_XY";
  else if (i == FthAM_YZ) return "FthAM_YZ";
  else if (i == FthAM_ZX) return "FthAM_ZX";

  else if (i == FfAM_XX) return "FfAM_XX";
  else if (i == FfAM_YY) return "FfAM_YY";
  else if (i == FfAM_ZZ) return "FfAM_ZZ";
  else if (i == FfAM_XY) return "FfAM_XY";
  else if (i == FfAM_YZ) return "FfAM_YZ";
  else if (i == FfAM_ZX) return "FfAM_ZX";

  else if (i == FpAM_XX) return "FpAM_XX";
  else if (i == FpAM_YY) return "FpAM_YY";
  else if (i == FpAM_ZZ) return "FpAM_ZZ";
  else if (i == FpAM_XY) return "FpAM_XY";
  else if (i == FpAM_YZ) return "FpAM_YZ";
  else if (i == FpAM_ZX) return "FpAM_ZX";
  else if (i == FveAM_XX) return "FveAM_XX";
  else if (i == FveAM_YY) return "FveAM_YY";
  else if (i == FveAM_ZZ) return "FveAM_ZZ";
  else if (i == FveAM_XY) return "FveAM_XY";
  else if (i == FveAM_YZ) return "FveAM_YZ";
  else if (i == FveAM_ZX) return "FveAM_ZX";
  else if (i == epsAM)   return "epsAM";
  else if (i == epsR)   return "epsR";
  else if (i == epsG)   return "epsG";

  else if (i == Tcrys)  return "Tcrys";
  else if (i == Tmelt)  return "Tmelt";
  else if (i == Wcrys)  return "Wcrys";
  else if (i == Wmelt)  return "Wmelt";
  else if (i == TRefG)  return "TRefG";
  else if (i == TRefR)  return "TRefR";
  else if (i == TRefAM)  return "TRefAM";
#if 0
  else if (i == AGl_XX) return "AGl_XX";
  else if (i == AGl_YY) return "AGl_YY";
  else if (i == AGl_ZZ) return "AGl_ZZ";
  else if (i == AGl_XY) return "AGl_XY";
  else if (i == AGl_YZ) return "AGl_YZ";
  else if (i == AGl_ZX) return "AGl_ZX";
  else if (i == ARl_XX) return "ARl_XX";
  else if (i == ARl_YY) return "ARl_YY";
  else if (i == ARl_ZZ) return "ARl_ZZ";
  else if (i == ARl_XY) return "ARl_XY";
  else if (i == ARl_YZ) return "ARl_YZ";
  else if (i == ARl_ZX) return "ARl_ZX";
  else if (i == AAMl_XX) return "AAMl_XX";
  else if (i == AAMl_YY) return "AAMl_YY";
  else if (i == AAMl_ZZ) return "AAMl_ZZ";
  else if (i == AAMl_XY) return "AAMl_XY";
  else if (i == AAMl_YZ) return "AAMl_YZ";
  else if (i == AAMl_ZX) return "AAMl_ZX";
#endif
  else if (i == ADl_XX) return "ADl_XX";
  else if (i == ADl_YY) return "ADl_YY";
  else if (i == ADl_ZZ) return "ADl_ZZ";
  else if (i == ADl_XY) return "ADl_XY";
  else if (i == ADl_YZ) return "ADl_YZ";
  else if (i == ADl_ZX) return "ADl_ZX";

  else if (i == EnThG_XX) return "EnThG_XX";
  else if (i == EnThG_YY) return "EnThG_YY";
  else if (i == EnThG_ZZ) return "EnThG_ZZ";
  else if (i == EnThG_XY) return "EnThG_XY";
  else if (i == EnThG_YZ) return "EnThG_YZ";
  else if (i == EnThG_ZX) return "EnThG_ZX";

  else if (i == EnThR_XX) return "EnThR_XX";
  else if (i == EnThR_YY) return "EnThR_YY";
  else if (i == EnThR_ZZ) return "EnThR_ZZ";
  else if (i == EnThR_XY) return "EnThR_XY";
  else if (i == EnThR_YZ) return "EnThR_YZ";
  else if (i == EnThR_ZX) return "EnThR_ZX";

  else if (i == EnThAM_XX) return "EnThAM_XX";
  else if (i == EnThAM_YY) return "EnThAM_YY";
  else if (i == EnThAM_ZZ) return "EnThAM_ZZ";
  else if (i == EnThAM_XY) return "EnThAM_XY";
  else if (i == EnThAM_YZ) return "EnThAM_YZ";
  else if (i == EnThAM_ZX) return "EnThAM_ZX";

  else if (i == appliedEnergy) return "appliedEnergy";
  else if (i == _SMPEnergy) return "_SMPEnergy";
  else if (i == _thermalEnergy) return "_thermalEnergy";
  else if (i == plasticDiss) return "plasticDiss";
  else if (i == freezedDiss) return "freezedDiss";
  else if (i == viscousDiss) return "viscousDiss";
  else if (i == EDiss)       return "EDiss";

  else if (i == Ee_XX) return "Ee_XX";
  else if (i == Ee_YY) return "Ee_YY";
  else if (i == Ee_ZZ) return "Ee_ZZ";
  else if (i == Ee_XY) return "Ee_XY";
  else if (i == Ee_XZ) return "Ee_XZ";
  else if (i == Ee_YZ) return "Ee_YZ";
  else if (i == detEe) return "detEe";
  else if (i == Eve1_XX) return "Eve1_XX";
  else if (i == Eve1_YY) return "Eve1_YY";
  else if (i == Eve1_ZZ) return "Eve1_ZZ";
  else if (i == Eve1_XY) return "Eve1_XY";
  else if (i == Eve1_XZ) return "Eve1_XZ";
  else if (i == Eve1_YZ) return "Eve1_YZ";
  else if (i == Eve2_XX) return "Eve2_XX";
  else if (i == Eve2_YY) return "Eve2_YY";
  else if (i == Eve2_ZZ) return "Eve2_ZZ";
  else if (i == Eve2_XY) return "Eve2_XY";
  else if (i == Eve2_XZ) return "Eve2_XZ";
  else if (i == Eve2_YZ) return "Eve2_YZ";
  else if (i == corKir_XX) return "corKir_XX";
  else if (i == corKir_YY) return "corKir_YY";
  else if (i == corKir_ZZ) return "corKir_ZZ";
  else if (i == corKir_XY) return "corKir_XY";
  else if (i == corKir_XZ) return "corKir_XZ";
  else if (i == corKir_YZ) return "corKir_YZ";
  else if (i == mandel_XX) return "mandel_XX";
  else if (i == mandel_YY) return "mandel_YY";
  else if (i == mandel_ZZ) return "mandel_ZZ";
  else if (i == mandel_XY) return "mandel_XY";
  else if (i == mandel_XZ) return "mandel_XZ";
  else if (i == mandel_YZ) return "mandel_YZ";
  else if (i == mandel_YX) return "mandel_YX";
  else if (i == mandel_ZX) return "mandel_ZX";
  else if (i == mandel_ZY) return "mandel_ZY";
  else if (i == mandelCommuteChecker) return "mandelCommuteChecker";
  else if (i == corKirExtra_XX) return "corKirExtra_XX";
  else if (i == corKirExtra_YY) return "corKirExtra_YY";
  else if (i == corKirExtra_ZZ) return "corKirExtra_ZZ";
  else if (i == corKirExtra_XY) return "corKirExtra_XY";
  else if (i == corKirExtra_XZ) return "corKirExtra_XZ";
  else if (i == corKirExtra_YZ) return "corKirExtra_YZ";
  else if (i == Dev_corKir_Inf_XX) return "Dev_corKir_Inf_XX";
  else if (i == Dev_corKir_Inf_YY) return "Dev_corKir_Inf_YY";
  else if (i == Dev_corKir_Inf_ZZ) return "Dev_corKir_Inf_ZZ";
  else if (i == Dev_corKir_Inf_XY) return "Dev_corKir_Inf_XY";
  else if (i == Dev_corKir_Inf_XZ) return "Dev_corKir_Inf_XZ";
  else if (i == Dev_corKir_Inf_YZ) return "Dev_corKir_Inf_YZ";
  else if (i == Tr_corKir_Inf) return "Tr_corKir_Inf";
  else if (i == hyperElastic_BulkScalar) return "hyperElastic_BulkScalar";
  else if (i == hyperElastic_ShearScalar) return "hyperElastic_ShearScalar";
  else if (i == MAX_DEFO_ENERGY) return "MAX_DEFO_ENERGY";
  else if (i == MULLINS_DAMAGE) return "MULLINS_DAMAGE";
  else if (i == PRESSURE) return "PRESSURE";
  else if (i == viscousDissipatedEnergy) return "viscousDissipatedEnergy";
  else if (i == mullinsDissipatedEnergy) return "mullinsDissipatedEnergy";
  else if (i == USER1) return "USER1";
  else if (i == USER2) return "USER2";
  else if (i == USER3) return "USER3";
  else if (i == USER4) return "USER4";
  else if (i == USER5) return "USER5";
  else if (i == USER6) return "USER6";
  else if (i == USER7) return "USER7";
  else if (i == USER8) return "USER8";
  else if (i == USER9) return "USER9";
  else if (i == BROKEN) return "BROKEN";
  else if (i == EQUIVALENT_EIGENSTRESS) return "EQUIVALENT_EIGENSTRESS";
  else if (i == EQUIVALENT_EIGENSTRAIN) return "EQUIVALENT_EIGENSTRAIN";
  else if (i == EIGENSTRESS_XX) return "EIGENSTRESS_XX";
  else if (i == EIGENSTRESS_YY) return "EIGENSTRESS_YY";
  else if (i == EIGENSTRESS_ZZ) return "EIGENSTRESS_ZZ";
  else if (i == EIGENSTRESS_XY) return "EIGENSTRESS_XY";
  else if (i == EIGENSTRESS_XZ) return "EIGENSTRESS_XZ";
  else if (i == EIGENSTRESS_YZ) return "EIGENSTRESS_YZ";
  else if (i == EIGENSTRAIN_XX) return "EIGENSTRAIN_XX";
  else if (i == EIGENSTRAIN_YY) return "EIGENSTRAIN_YY";
  else if (i == EIGENSTRAIN_ZZ) return "EIGENSTRAIN_ZZ";
  else if (i == EIGENSTRAIN_XY) return "EIGENSTRAIN_XY";
  else if (i == EIGENSTRAIN_XZ) return "EIGENSTRAIN_XZ";
  else if (i == EIGENSTRAIN_YZ) return "EIGENSTRAIN_YZ";
  else if (i == ONE) return "ONE";
  else if (i == _EMEnergy) return "_EMEnergy";
  //local damage Tensor43 (only 6 out of 81 components because we start in 1D)
  else if (i == DAMAGE_TENSOR43_0011) return "DAMAGE_TENSOR43_0011";
  else if (i == DAMAGE_TENSOR43_0022) return "DAMAGE_TENSOR43_0022";
  else if (i == DAMAGE_TENSOR43_1111) return "DAMAGE_TENSOR43_1111";
  else if (i == DAMAGE_TENSOR43_1122) return "DAMAGE_TENSOR43_1122";
  else if (i == DAMAGE_TENSOR43_2222) return "DAMAGE_TENSOR43_2222";
  else if (i == DAMAGE_TENSOR43_0101) return "DAMAGE_TENSOR43_0101";
  //plastic strain
  else if (i == EPLAST_XX) return "EPLAST_XX";
  else if (i == EPLAST_YY) return "EPLAST_YY";
  else if (i == EPLAST_ZZ) return "EPLAST_ZZ";
  else if (i == EPLAST_XY) return "EPLAST_XY";
  else if (i == EPLAST_XZ) return "EPLAST_XZ";
  else if (i == EPLAST_YZ) return "EPLAST_YZ";
  
  //elastic stiffness without damage (only 7 out of 81 components because we start in 1D)
  else if (i == C_EL_0000) return "C_EL_0000";
  else if (i == C_EL_0011) return "C_EL_0011";
  else if (i == C_EL_0022) return "C_EL_0022";
  else if (i == C_EL_1111) return "C_EL_1111";
  else if (i == C_EL_1122) return "C_EL_1122";
  else if (i == C_EL_2222) return "C_EL_2222";
  else if (i == C_EL_0101) return "C_EL_0101";
  
  //DStressDNonLocalVariable
  else if (i == DSIGMA_XX_DNONLOCALVAR) return "DSIGMA_XX_DNONLOCALVAR";
  
  //DLocalVariableDStrain (just epsilon_XX as we start in 1D) 
  else if (i == DLOCALVAR_DEPSILON_XX) return "DLOCALVAR_DEPSILON_XX";
  
  //DStressreconstructedDStrain (we start in 1D so there are just 4 components for the reconstructed stress: XX, YY, ZZ, XY and one component for the strain: XX)
  else if (i == DSIGMAREC_XX_DEPSILON_XX) return "DSIGMAREC_XX_DEPSILON_XX";
  else if (i == DSIGMAREC_YY_DEPSILON_XX) return "DSIGMAREC_YY_DEPSILON_XX";
  else if (i == DSIGMAREC_ZZ_DEPSILON_XX) return "DSIGMAREC_ZZ_DEPSILON_XX";
  else if (i == DSIGMAREC_XY_DEPSILON_XX) return "DSIGMAREC_XY_DEPSILON_XX";
  
  //DEplastDE (we start in 1D so only 4 components for the plastic strain: Eplast_XX, Eplast_YY, Eplast_ZZ, Eplast_XY and 1 component for epsilon: epsilon_XX)
  else if (i == DEPLAST_XX_DEPSILON_XX) return "DEPLAST_XX_DEPSILON_XX";
  else if (i == DEPLAST_YY_DEPSILON_XX) return "DEPLAST_YY_DEPSILON_XX";
  else if (i == DEPLAST_ZZ_DEPSILON_XX) return "DEPLAST_ZZ_DEPSILON_XX";
  else if (i == DEPLAST_XY_DEPSILON_XX) return "DEPLAST_XY_DEPSILON_XX";
  
  else{
    Msg::Warning("This IP field %d is not defined in IPField::ToString(%d)",i,i);
    return "UNDEFINED";
  }
};

bool IPField::compute1state(IPStateBase::whichState ws, bool stiff){
	std::vector<partDomain*>* domainVector = _solver->getDomainVector();
	unknownField* ufield = _solver->getUnknownField();
  for(std::vector<partDomain*>::iterator itdom=domainVector->begin(); itdom!=domainVector->end(); ++itdom){
    partDomain *dom = *itdom;
    bool ok = dom->computeIPVariable(_AIPS,ufield,ws,stiff);
    if (!ok) return false;
  }
  return true;
}


void IPField::setDeformationGradientGradient(const STensor33 &GM,const IPStateBase::whichState ws)
{
  std::vector<partDomain*>* domainVector = _solver->getDomainVector();
  for(std::vector<partDomain*>::iterator itdom=domainVector->begin(); itdom!=domainVector->end(); ++itdom)
  {
    partDomain *dom = *itdom;
    if(dom->hasBodyForceForHO())
    {
      dom->setDeformationGradientGradient(_AIPS,GM,ws);
    }
  }
}

void IPField::setdFmdFM(std::map<Dof, STensor3> &dUdF, const IPStateBase::whichState ws)
{
  std::vector<partDomain*>* domainVector = _solver->getDomainVector();
  for(std::vector<partDomain*>::iterator itdom=domainVector->begin(); itdom!=domainVector->end(); ++itdom)
  {
    partDomain *dom = *itdom;
    if(dom->hasBodyForceForHO())
    {
      dom->setdFmdFM(_AIPS,dUdF,ws);
    }
  }
}

void IPField::setdFmdGM(std::map<Dof, STensor33> &dUdG, const IPStateBase::whichState ws)
{
  std::vector<partDomain*>* domainVector = _solver->getDomainVector();
  for(std::vector<partDomain*>::iterator itdom=domainVector->begin(); itdom!=domainVector->end(); ++itdom)
  {
    partDomain *dom = *itdom;
    if(dom->hasBodyForceForHO())
    {
      dom->setdFmdGM(_AIPS,dUdG,ws);
    }
  }
}


void IPField::setValuesForBodyForce(const IPStateBase::whichState ws)
{
  std::vector<partDomain*>* domainVector = _solver->getDomainVector();
  for(std::vector<partDomain*>::iterator itdom=domainVector->begin(); itdom!=domainVector->end(); ++itdom)
  {
    partDomain *dom = *itdom;
    if(dom->hasBodyForceForHO())
    {
      dom->setValuesForBodyForce(_AIPS,ws);
    }
  }
}

void IPField::checkInternalState(){
  //Msg::Info("internal state checking ...");
  // clear all broken solver
  _solverBrokenIPs.clear();
  std::vector<partDomain*>* domainVector = _solver->getDomainVector();
  for(std::vector<partDomain*>::iterator itdom=domainVector->begin(); itdom!=domainVector->end(); ++itdom){
    partDomain *dom = *itdom;
    dom->checkInternalState(this);
  };

  #if defined(HAVE_MPI)
  if ((_solver->getNumRanks() > 1) and _withMultiscale){
    // all root and otherRanks of solvers
    std::set<int> allOtherRanks;
    _solver->getAllOtherRanks(allOtherRanks);

    std::set<int> allRootRanks;
    _solver->getAllRootRanks(allRootRanks);

    // at root rank, the solver is solved in other rank
    // the solver broken flag must be synchronised
    if (allOtherRanks.find(Msg::GetCommRank()) != allOtherRanks.end()){
      int rootRank;
      _solver->getRootRank(Msg::GetCommRank(),rootRank);
      //
      // send numSize to root ranks
      int bufferSize = _solverBrokenIPs.size();
      int tag = numericalMaterialBase::createTypeWithTwoInts(Msg::GetCommRank(),0);
      MPI_Send(&bufferSize,1,MPI_INT,rootRank,tag,MPI_COMM_WORLD);
      if (bufferSize > 0){
        //
        std::vector<int> buffer(_solverBrokenIPs.begin(),_solverBrokenIPs.end()); // send to rootRankd
        tag = numericalMaterialBase::createTypeWithTwoInts(Msg::GetCommRank(),1);
        MPI_Send(&buffer[0],bufferSize,MPI_INT,rootRank,tag,MPI_COMM_WORLD);
      }
    }
    else if ((allRootRanks.find(Msg::GetCommRank()) != allRootRanks.end()) and allOtherRanks.size() > 0){
      std::set<int> otherRanks;
      _solver->getOtherRanks(Msg::GetCommRank(),otherRanks);
      for (std::set<int>::const_iterator itR = otherRanks.begin(); itR != otherRanks.end(); itR ++){
        int tag = numericalMaterialBase::createTypeWithTwoInts(*itR,0);
        int bufferSize= 0;
        MPI_Status status;
        MPI_Recv(&bufferSize,1,MPI_INT,*itR,tag,MPI_COMM_WORLD,&status);
        if (bufferSize > 0){
          std::vector<int> buffer(bufferSize);
          tag = numericalMaterialBase::createTypeWithTwoInts(*itR,1);
          MPI_Recv(&buffer[0],bufferSize,MPI_INT,*itR,tag,MPI_COMM_WORLD,&status);
          // add data at roots
          _solverBrokenIPs.insert(buffer.begin(),buffer.end());
          // at root rank, now
          for (int i=0; i< bufferSize; i++){
            int elnum,gpnum;
            numericalMaterialBase::getTwoIntsFromType(buffer[i],elnum,gpnum);
            AllIPState::ipstateContainer::iterator it = _AIPS->getAIPSContainer()->find(elnum);
            if (it != _AIPS->getAIPSContainer()->end()){
              AllIPState::ipstateElementContainer& ips = it->second;
              IPVariable* ipv = ips[gpnum]->getState(IPStateBase::current);
              IPVariableMechanics* ipvMecha = static_cast<IPVariableMechanics*>(ipv);
              ipvMecha->brokenSolver(true);
              #ifdef _DEBUG
                printf("rank %d solver broken from other rank at  ele %d gp %d \n",Msg::GetCommRank(),elnum,gpnum);
              #endif // _DEBUG
            }
          }
        }
      }
    }
    Msg::Barrier();
  };
  #endif // HAVE_MPI
  //Msg::Info("done internal state checking");
};

void IPField::checkFailure(){
  if (!_withCohesiveFailure) return; // do nothing without cohesive law

  //Msg::Info("failure checking ...");
  // get data in order to send to other procs
  // only new block and broken ips are filled in the group
  _blockedDissipationIPs.clear();
  _brokenIPs.clear();

  //
  _nonBrokenIPs.clear();
  _nonBlockedDissipationIPs.clear();

  _newlyBlockedDissipationElements.clear();
  _newlyBrokenElements.clear();

  _numOfNewlyBrokenElements = 0;
  _totalNumOfNewlyBrokenElements = 0.;
  //Msg::Info("checking failure all IP"); //too many output in explicit

  // Check cohesive insertion at current time step on all domains
  std::vector<partDomain*>* domainVector = _solver->getDomainVector();
    // Loop on domains
  for (int idom=0; idom < domainVector->size(); idom++)
  {
    partDomain* dom = (*domainVector)[idom];
    dom->checkFailure(this);
  }

  _totalNumOfNewlyBrokenElements = _numOfNewlyBrokenElements;

  #if defined(HAVE_MPI)
  std::set<int> allRootRanks;
  _solver->getAllRootRanks(allRootRanks);
  if (allRootRanks.size() > 0){
    _totalNumOfNewlyBrokenElements = 0;
    MPI_Allreduce((void*)&_numOfNewlyBrokenElements, (void*)&_totalNumOfNewlyBrokenElements, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
  }
  #endif // HAVE_MPI

  //Msg::Info("done failure checking ...");
};

void IPField::flushFailureState(){
  _failureStateIsFlushed = true;
  if (!_withCohesiveFailure) return; // do nothing without cohesive law
  // flush data at root ranks
  // with modified _brokenIPs, _blockedDissipationIPs
  #if defined(HAVE_MPI)
  std::set<int> allOtherRanks;
  _solver->getAllOtherRanks(allOtherRanks);
  if (allOtherRanks.find(Msg::GetCommRank()) == allOtherRanks.end())
  #endif // HAVE_MPI
  {
    // failure IP
    for (std::set<int>::iterator itn = _nonBrokenIPs.begin(); itn != _nonBrokenIPs.end(); itn++){
      _brokenIPs.erase(*itn);
      int elnum,gpnum;
      numericalMaterialBase::getTwoIntsFromType(*itn,elnum,gpnum);
      AllIPState::ipstateContainer::iterator its = _AIPS->getAIPSContainer()->find(elnum);
      if (its != _AIPS->getAIPSContainer()->end()){
        AllIPState::ipstateElementContainer& vips = its->second;
        IPVariable* ipv = vips[gpnum]->getState(IPStateBase::current);
        IPVariable2ForFractureBase* ipvFrac = dynamic_cast<IPVariable2ForFractureBase*>(ipv);
        if (ipvFrac != NULL){
          ipvFrac->nobroken();
          //if (Msg::GetCommSize() > 1)
          //    printf("rank %d IP failure state is modified from broken to nobroken ele %d gp %d \n",Msg::GetCommRank(),elnum,gpnum);
          //else
          //    printf("IP failure state is modified from broken to nobroken ele %d gp %d \n",elnum,gpnum);
        }
      }
      else{
        Msg::Error("IPstate of element %d, GP %d can not be found",elnum,gpnum);
      }
    }

    // block IP
    for (std::set<int>::iterator itn = _nonBlockedDissipationIPs.begin(); itn != _nonBlockedDissipationIPs.end(); itn++){
      _blockedDissipationIPs.erase(*itn);
      int elnum,gpnum;
      numericalMaterialBase::getTwoIntsFromType(*itn,elnum,gpnum);
      AllIPState::ipstateContainer::iterator its = _AIPS->getAIPSContainer()->find(elnum);
      if (its != _AIPS->getAIPSContainer()->end()){
        AllIPState::ipstateElementContainer& vips = its->second;
        IPVariable* ipv = vips[gpnum]->getState(IPStateBase::current);
        IPVariableMechanics* ipvMecha = static_cast<IPVariableMechanics*>(ipv);
        ipvMecha->blockDissipation(false);
        if (Msg::GetCommSize() > 1)
          printf("rank %d  IP dissipation blocked state ele %d gp %d is unblocked \n",Msg::GetCommRank(),elnum,gpnum);
        else
          printf("IP dissipation blocked state ele %d gp %d is unblocked \n",elnum,gpnum);
      }
      else{
        Msg::Error("IPstate of element %d, GP %d can not be found",elnum,gpnum);
      }
    }
  }
  // communication with otherRanks
  // synchronization between rootRanks and otherRanks
  #if defined(HAVE_MPI)
  if (_solver->getNumRanks() > 1){

    std::set<int> allOtherRanks;
    _solver->getAllOtherRanks(allOtherRanks);

    std::set<int> allRootRanks;
    _solver->getAllRootRanks(allRootRanks);
    // send from rootRank to other rank
    if ((allRootRanks.find(Msg::GetCommRank()) != allRootRanks.end()) and (allOtherRanks.size() > 0)){
      std::set<int> otherRanks;
      _solver->getOtherRanks(Msg::GetCommRank(),otherRanks);
      std::vector<int> bufferSize(2);
      bufferSize[0] = _brokenIPs.size();
      bufferSize[1] = _blockedDissipationIPs.size();

      int allBufferSize = bufferSize[0]+bufferSize[1];
      std::vector<int> buffer(_brokenIPs.begin(),_brokenIPs.end());
      buffer.insert(buffer.end(),_blockedDissipationIPs.begin(),_blockedDissipationIPs.end());

      for (std::set<int>::const_iterator itR = otherRanks.begin(); itR != otherRanks.end(); itR ++){
        int tag = numericalMaterialBase::createTypeWithTwoInts(Msg::GetCommRank(),3);
        MPI_Send(&bufferSize[0],2,MPI_INT,*itR,tag,MPI_COMM_WORLD);
        if (allBufferSize >0){
          tag = numericalMaterialBase::createTypeWithTwoInts(Msg::GetCommRank(),4);
          MPI_Send(&buffer[0],allBufferSize,MPI_INT,*itR,tag,MPI_COMM_WORLD);
        }
      }
    }
    else if (allOtherRanks.find(Msg::GetCommRank()) != allOtherRanks.end()){
      int rootRank;
      _solver->getRootRank(Msg::GetCommRank(),rootRank);
      MPI_Status status;
      std::vector<int> bufferSize(2);
      int tag = numericalMaterialBase::createTypeWithTwoInts(rootRank,3);
      MPI_Recv(&bufferSize[0],2,MPI_INT,rootRank,tag,MPI_COMM_WORLD,&status);
      int allBufferSize = bufferSize[0]+bufferSize[1];
      if (allBufferSize > 0){
        std::vector<int> buffer(allBufferSize);
        tag = numericalMaterialBase::createTypeWithTwoInts(rootRank,4);
        MPI_Recv(&buffer[0],allBufferSize,MPI_INT,rootRank,tag,MPI_COMM_WORLD,&status);

        for (int i=0; i< bufferSize[0]; i++){
          _brokenIPs.insert(buffer[i]);
          AllIPState::ipstateContainer::iterator it = _AIPS->getAIPSContainer()->find(buffer[i]);
          if (it != _AIPS->getAIPSContainer()->end()){
            AllIPState::ipstateElementContainer& ips = it->second;
            IPVariable* ipv = ips[0]->getState(IPStateBase::current);
            IPVariable2ForFractureBase* ipvFrac = dynamic_cast<IPVariable2ForFractureBase*>(ipv);
            if (ipvFrac != NULL){
              ipvFrac->broken();
              int elnum,gpnum;
              numericalMaterialBase::getTwoIntsFromType(buffer[i],elnum,gpnum);
              printf("rank %d broken IP ele %d gp %d  from root ranks \n",Msg::GetCommRank(),elnum,gpnum);
            }
          }
        }
        for (int i=bufferSize[0]; i< allBufferSize; i++){
          _blockedDissipationIPs.insert(buffer[i]);
          AllIPState::ipstateContainer::iterator it = _AIPS->getAIPSContainer()->find(buffer[i]);
          if (it != _AIPS->getAIPSContainer()->end()){
            AllIPState::ipstateElementContainer& ips = it->second;
            IPVariable* ipv = ips[0]->getState(IPStateBase::current);
            IPVariableMechanics* ipvMecha = static_cast<IPVariableMechanics*>(ipv);
            ipvMecha->blockDissipation(true);
            int elnum,gpnum;
            numericalMaterialBase::getTwoIntsFromType(buffer[i],elnum,gpnum);
            printf("rank %d blocked IP ele %d gp %d from root ranks\n",Msg::GetCommRank(),elnum,gpnum);
          }
        }
      }
    }
  }
  #endif // HAVE_MPI
};

void IPField::checkAll(){
  // check state
  this->checkInternalState();
  this->checkFailure();
  //
  _failureAndInternalAreChecked = true;
};

void IPField::nextStep(const double time){
  if (!_failureAndInternalAreChecked){
    this->checkAll();
  }
  _failureAndInternalAreChecked = false; // reset

  if (!_failureStateIsFlushed){
    this->flushFailureState();
  }
  _failureStateIsFlushed = false;

  // save tempory data by transfering buffer
    // newly blocked elements
  _blockedDissipationElements.insert(_newlyBlockedDissipationElements.cbegin(),_newlyBlockedDissipationElements.cend());
    // newly broken elements
  _brokenElements.insert(_newlyBrokenElements.cbegin(),_newlyBrokenElements.cend());


  _blockedDissipationIPs.clear();
  _brokenIPs.clear();
  //
  _nonBrokenIPs.clear();
  _nonBlockedDissipationIPs.clear();

  _newlyBlockedDissipationElements.clear();
  _newlyBrokenElements.clear();

  _solverBrokenIPs.clear();

  _numOfNewlyBrokenElements = 0;
  _totalNumOfNewlyBrokenElements = 0.;

  _AIPS->nextStep();
};

void IPField::copy(IPStateBase::whichState source, IPStateBase::whichState dest){
  // reset data
  _failureAndInternalAreChecked = false; // reset
  _failureStateIsFlushed = false;

  _blockedDissipationIPs.clear();
  _brokenIPs.clear();
  //
  _nonBrokenIPs.clear();
  _nonBlockedDissipationIPs.clear();

  _newlyBlockedDissipationElements.clear();
  _newlyBrokenElements.clear();

  _solverBrokenIPs.clear();

  _numOfNewlyBrokenElements = 0;
  _totalNumOfNewlyBrokenElements = 0.;

  _AIPS->copy(source,dest);
};

void IPField::checkActiveDissipation(){
	// check active damage state
	// should be call after
	_numOfActiveDissipationIPs = 0;
	if (_withDissipation){
    for (AllIPState::ipstateContainer::const_iterator it = _AIPS->begin(); it!= _AIPS->end(); it++){
      const std::vector<IPStateBase*> *vips = &((*it).second);
      for (int i = 0; i< vips->size(); i++){
        const IPVariableMechanics* ipvcur = dynamic_cast<const IPVariableMechanics*>((*vips)[i]->getState(IPStateBase::current));
        if (ipvcur != NULL){
          if (ipvcur->dissipationIsActive()){
            _numOfActiveDissipationIPs++;
          }
        }
      }
    }

    #if defined(HAVE_MPI)
    if (_solver->getNumRanks() > 1){
      int totalNumber = 0;
      MPI_Allreduce(&_numOfActiveDissipationIPs,&totalNumber,1,MPI_INT,MPI_SUM,MPI_COMM_WORLD);
      _numOfActiveDissipationIPs = totalNumber;
    }
    #endif //HAVE_MPI

    if (_numOfActiveDissipationIPs > 0){
      _numOfActiveDissipationIPsReached = _numOfActiveDissipationIPs;
      this->copy(IPStateBase::current,IPStateBase::activeDissipation);
    }
  }
};

void IPField::initialBroken(MElement *iele, materialLaw* mlaw )
{
 #ifdef _DEBUG
  Msg::Info("Interface element %d is broken at initialization on rank %d",iele->getNum(),Msg::GetCommRank());
 #else
  Msg::Info("Interface element %d is broken at initialization",iele->getNum());
 #endif // _DEBUG
  materialLaw2LawsInitializer * mflaw = dynamic_cast<materialLaw2LawsInitializer*>(mlaw);
  // get ipstate
  AllIPState::ipstateElementContainer *vips;
  IPStateBase *ips;
  vips = _AIPS->getIPstate(iele->getNum());
  for(int i=0;i<vips->size();i++){
    ips = (*vips)[i];
    mflaw->initialBroken(ips);
  }
};

void IPField::initialBrokenDomain(GModel* pModel, std::vector<std::pair<int,int> > &twoPhys)
{
  std::vector<partDomain*>& domainVector = *(_solver->getDomainVector());
  for(int i=0;i<twoPhys.size();i++)
  {
    int firstNum = twoPhys[i].first;
    int secondNum = twoPhys[i].second;
    dgPartDomain* dgdomFound = NULL;
    for (int idom = 0; idom < domainVector.size(); idom++)
    {
      partDomain* dom = domainVector[idom];
      if (dom->IsInterfaceTerms())
      {
        dgPartDomain* dgdom = static_cast<dgPartDomain*>(dom);
        if (((dgdom->getPlusDomain()->getPhysical() == firstNum) && (dgdom->getMinusDomain()->getPhysical() == secondNum)) ||
            ((dgdom->getPlusDomain()->getPhysical() == secondNum) && (dgdom->getMinusDomain()->getPhysical() == firstNum)))
        {
          // found
          dgdomFound = dgdom;
          break;
        }
      }
    }
    if (dgdomFound == NULL)
    {
      if (firstNum ==secondNum)
      {
        Msg::Warning("domain %d cannot be found for initially broken !!!",firstNum);
      }
      else
      {
        Msg::Warning("interdomain between phys %d and phys %d cannot be found for initially broken !!!",firstNum,secondNum);
      }
    }
    else
    {
      if (firstNum ==secondNum)
      {
        Msg::Info("domain %d is initially broken !!!",firstNum);
      }
      else
      {
        Msg::Info("interdomain between phys %d and phys %d is initially broken !!!",firstNum,secondNum);
      }
      //
      dgdomFound->initiallyBreakAllInterfaceIP(getAips());
    }
  };
};

void IPField::initialBroken(GModel* pModel, std::vector<std::pair<int,int> > &vnumphys)
{
	std::vector<partDomain*>* domainVector = _solver->getDomainVector();
  std::vector<MVertex*> vv;
  for(int i=0;i<vnumphys.size();i++){
    // get the vertex associated to the physical entities LINES ONLY !!
    pModel->getMeshVerticesForPhysicalGroup(vnumphys[i].first,vnumphys[i].second,vv);
    // find the InterfaceElement associated to these vertex (identify interior node as degree 2 min)
    for(std::vector<partDomain*>::iterator itfield = domainVector->begin(); itfield != domainVector->end(); ++itfield){
      if((*itfield)->IsInterfaceTerms())
      {
        dgPartDomain *dgdom = static_cast<dgPartDomain*>(*itfield);
        for(elementGroup::elementContainer::const_iterator it = dgdom->gi->begin(); it!=dgdom->gi->end(); ++it){
          for(int k=0;k<vv.size();k++){
            if(vv[k] == (it->second)->getVertex(2) )
            {
              this->initialBroken(it->second, dgdom->getMaterialLaw());
            }
          }
        }
      }
    }
    vv.clear();
  }
}

double IPField::getIPValueOnPhysical(const int dim, const int physical, const int ipVal) const{
  for (int i=0; i < viparch.size(); i++){
    if ((viparch[i].numphys == physical and viparch[i].dim == dim) and ipVal == viparch[i].comp){
      return viparch[i].value;
    }
  }
  Msg::Error("IPField::getIPValueOnPhysical does not exist in physical %d dim %d",physical,dim);
  return 0.;
};

void IPField::buildDataInterface(FILE* myview, const nonLinearMechSolver* solver, const int cc, const nlsField::ElemValue ev, const int gp, const nlsField::ValType vt) const
{
  const std::vector<partDomain*> &vdom = *(solver->getDomainVector());
  std::vector<double> fieldData;
  for (unsigned int i = 0; i < vdom.size(); ++i)
  {
    const dgPartDomain* dgdom = dynamic_cast<const dgPartDomain*>(vdom[i]);
    if (dgdom != NULL)
    {
      for (elementGroup::elementContainer::const_iterator it = dgdom->gi->begin(); it != dgdom->gi->end(); ++it)
      {
        MElement *ele = it->second;
        fieldData.resize(_numcomp);
        this->get(vdom[i],ele,fieldData,cc,ev,gp,vt);
        fprintf(myview, "%ld",ele->getNum());
        for(int j=0;j<_numcomp;j++)
          fprintf(myview, " %.16g",fieldData[j]);
        fprintf(myview,"\n");
      }
    }
  }
}

void IPField::buildAllView(const nonLinearMechSolver* solver ,const double time, const int numstep, const bool forceView)
{
  int previouslastViewStep = _lastViewStep;
  elementsField::buildAllView(solver,time,numstep,forceView);
  if (_vBuildViewInterface.size() > 0 && _totelemInterface > 0)
  {
    if (numstep > previouslastViewStep)
    {
      if (forceView or (numstep%_numStepBetweenTwoViews == 0))
      {
        // view file
        std::string extension=".msh";
        if (_binary)
        {
          extension = ".dat";
        }
        // file name
        std::string fileName;
        #if defined(HAVE_MPI)
        if (solver->getNumRanks() > 1)
        {
          fileName  = solver->getFileSavingPrefix()+_fileNamePrefixInterface+"_part"+int2str(Msg::GetCommRank())+"_step"+int2str(numstep)+extension;
        }
        else
        #endif //HAVE_MPI
        {
          fileName  = solver->getFileSavingPrefix()+_fileNamePrefixInterface+"_step"+int2str(numstep)+extension;
        }

        // write to file
        if (_binary)
        {
          Msg::Error("Binary write not implemented yet");
          return;
        }
        else
        {
          FILE* file = fopen(fileName.c_str(), "w");
          // start file by format version
          // problem: find how to retrieve the last mesh format version
          fprintf(file, "$MeshFormat\n2.2 0 8\n$EndMeshFormat\n");
          for(std::vector<dataBuildView>::iterator it=_vBuildViewInterface.begin(); it!=_vBuildViewInterface.end(); ++it)
          {
            dataBuildView &dbv = *it;
            nlsField::tag = dbv.tag; // to make tag availbale though export view
            fprintf(file, "$%s\n1\n\"%s\"\n1\n%.16g\n4\n%d\n%d\n%ld\n%d\n",this->dataname().c_str(),dbv.viewname.c_str(),time,numstep,_numcomp,_totelemInterface,Msg::GetCommRank());
            this->buildDataInterface(file,solver,dbv.comp,dbv.ev,dbv.gpNum,dbv.valtype);
            fprintf(file, "$End%s\n",this->dataname().c_str());

            nlsField::tag = -1; // back to default value
          }
          fclose(file);
        }
      }
    }
  }
};

const std::vector<partDomain*>& IPField::getDomainVector() const {return *_solver->getDomainVector();};

void IPField::closeArchivingFiles()
{
  for(std::vector<ip2archive>::iterator it=viparch.begin(); it!=viparch.end(); ++it){
    ip2archive &ipa = *it;
    if (ipa.fp != NULL) fclose(ipa.fp);
    ipa.fp = NULL;
  }
}

void IPField::resetArchiving()
{
  elementsField::resetArchiving();
  _lastSaveStep = -1;
  for(std::vector<ip2archive>::iterator it=viparch.begin(); it!=viparch.end(); ++it){
    ip2archive &ipa = *it;
    if (ipa.fp != NULL) fclose(ipa.fp);
    ipa.openFile(_solver->getFileSavingPrefix());
  }
}
void IPField::archive(const double time, const int step, const bool forceView){
  // msh view
  this->buildAllView(_solver,time,step, forceView);

  bool willView = forceView;
  if (_vBuildView.size() > 0)
  {
    if (step == _lastViewStep)
    {
      willView = true;
    }
  }
  else
  {
    if (step > _lastViewStep)
    {
      if (forceView or (step%_numStepBetweenTwoViews == 0))
      {
        _lastViewStep = step;
        willView = true;
      }
    }
  }


  if (willView)
  {
    AllIPState::ipstateContainer& mapall =*(_AIPS->getAIPSContainer());
    for (AllIPState::ipstateContainer::iterator it = mapall.begin(); it!= mapall.end(); it++)
    {
      std::vector<IPStateBase*> *vips = &((*it).second);
      for (int i = 0; i< vips->size(); i++)
      {
        if (_withMultiscale)
        {
          nonLinearMechSolver* solver = (*vips)[i]->getMicroSolver();
          if (solver!= NULL)
          {
            solver->archiveData(time,step,true);
          }
        }
        //
        IPVariable* ipvCur = (*vips)[i]->getState(IPStateBase::current);
        if (ipvCur->withDeepMN())
        {
          std::vector<DeepMaterialNetwork*> allMN;
          ipvCur->getAllDeepMNs(allMN);
          for (int jj=0; jj< allMN.size(); jj++)
          {
            allMN[jj]->saveIPDataToFile(step);
          }
        }
        //
      }
    }
	};

  // txt archive
  if (step > _lastSaveStep)
  {
    if (step%_numStepBetweenTwoSaves == 0 or forceView )
    {
      _lastSaveStep = step;
      for(std::vector<ip2archive>::iterator it=viparch.begin(); it!=viparch.end(); ++it){
        ip2archive &ipa = *it;
        nlsField::tag = ipa.tag;
        // get ip value
        if( ipa.ele.size() == 1)
        {
          ipa.value = this->getIPcomp(ipa.domain[0],ipa.ele[0],IPStateBase::current,ipa.comp,
                                        ipa.evalue,ipa.numgauss,nlsField::val);
          fprintf(ipa.fp,"%.16g;%.16g\n",time,ipa.value);
          fflush (ipa.fp);
        }
        else
        {
          double totalVolume = 0.;
          double totalValue = 0;
          if(ipa.evalue == nlsField::min) totalValue = 1.e50;
          if(ipa.evalue == nlsField::max) totalValue = -1.e50;
          std::vector<const partDomain *>::const_iterator itd = ipa.domain.begin();
          for(std::vector<MElement *>::const_iterator ite = ipa.ele.begin();
              ite !=  ipa.ele.end(); ite++, itd++)
          {
            double vol = 0.;
            double val = this->getIPcompAndVolume(*itd,*ite, IPStateBase::current, ipa.comp,
                                      ipa.evalue,nlsField::val,vol);
            totalVolume += vol;
            if(ipa.evalue == nlsField::crude)
            {
              Msg::Error("Cannot archive on a non nodal entity crude values");
            }
            else
            {
              switch(ipa.evalue){
                case nlsField::max :
                 if(val>totalValue) totalValue=val;
                 break;
                case nlsField::min :
                 if(val<totalValue) totalValue=val;
                 break;
                case nlsField::mean :
                 totalValue+=val*vol;
                 break;
              }
            }
          }
          if(ipa.evalue==nlsField::mean && totalVolume!=0.) totalValue/=totalVolume;
          ipa.value = totalValue;
          fprintf(ipa.fp,"%.16g;%.16g\n",time,totalValue);
          fflush (ipa.fp);
        }
        // back to default value
        nlsField::tag = -1;
      }
    }
  }
};

double IPField::computeBulkDeformationEnergy(MElement *ele, const partDomain *dom, const IPStateBase::whichState ws) const
{
  IntPt *GP;
  double jac[3][3];
  int npts = dom->getBulkGaussIntegrationRule()->getIntPoints(ele,&GP);
  AllIPState::ipstateElementContainer *vips = _AIPS->getIPstate(ele->getNum());
  double ener =0.;
  for(int i=0;i<npts;i++)
  {
    IPStateBase* ips = (*vips)[i];
    IPVariableMechanics *ipv = dynamic_cast<IPVariableMechanics*>(ips->getState(ws));
    if(ipv != NULL)
    {
      double enerpt = ipv->defoEnergy();
      // gauss point weight
      double weight = GP[i].weight;
      double detJ = ele->getJacobianDeterminant(GP[i].pt[0],GP[i].pt[1],GP[i].pt[2]);
      ener += weight*detJ*enerpt;
    }
  }
  return ener;
}

double IPField::computeInterfaceDeformationEnergy(MElement *ele, const dgPartDomain *dom, const IPStateBase::whichState ws) const
{
  IntPt *GP;
  double jac[3][3];
  int npts = dom->getInterfaceGaussIntegrationRule()->getIntPoints(ele,&GP);
  AllIPState::ipstateElementContainer *vips = _AIPS->getIPstate(ele->getNum());
  double ener =0.;

  for(int i=0;i<npts;i++)
  {
    IPStateBase* ipsm = (*vips)[i];
    IPStateBase* ipsp = (*vips)[i+npts];

    IPVariableMechanics *ipvm = dynamic_cast<IPVariableMechanics*>(ipsm->getState(ws));
    IPVariableMechanics *ipvp = dynamic_cast<IPVariableMechanics*>(ipsp->getState(ws));
    if(ipvm != NULL && ipvp !=NULL)
    {
      IPVariable2ForFractureBase* ipvmFrac = dynamic_cast<IPVariable2ForFractureBase*>(ipvm);
      if (ipvmFrac != NULL){
        if (ipvmFrac->isbroken()){
          double enerpt = 0.5*(ipvm->defoEnergy()+ipvp->defoEnergy());
          // gauss point weight
          double weight = GP[i].weight;
          double detJ = ele->getJacobianDeterminant(GP[i].pt[0],GP[i].pt[1],GP[i].pt[2]);
          ener += weight*detJ*enerpt;
        }
      }

		}
  }
  return ener;
}

double IPField::computeDeformationEnergy(const IPStateBase::whichState ws) const{
	const std::vector<partDomain*>* domainVector = _solver->getDomainVector();
	double ener=0.;
	for(std::vector<partDomain*>::const_iterator itdom=domainVector->begin(); itdom!=domainVector->end(); ++itdom){
		partDomain *dom = *itdom;
		for(elementGroup::elementContainer::const_iterator it=dom->element_begin(); it!=dom->element_end(); ++it){
			MElement *ele = it->second;
			ener += this->computeBulkDeformationEnergy(ele,dom,ws);
		}
		if (dom->IsInterfaceTerms()){
			dgPartDomain* dgdom = static_cast<dgPartDomain*>(dom);
			for(elementGroup::elementContainer::const_iterator it=dgdom->gi->begin(); it!=dgdom->gi->end(); ++it){
				MElement *ele = it->second;
			 #if defined(HAVE_MPI)
				if(Msg::GetCommSize()>1)
				{
					MInterfaceElement *iele = dynamic_cast<MInterfaceElement*>(ele);
					if(iele->getElem(0)->getPartition()==0 or iele->getElem(0)->getPartition() == Msg::GetCommRank()+1) // if - element is ghost not computed to avoid to count twice the contribution
					{
						ener += this->computeInterfaceDeformationEnergy(ele,dgdom,ws);
					}
				}
				else
			 #endif // HAVE_MPI
				{
					ener += this->computeInterfaceDeformationEnergy(ele,dgdom,ws);
				}

			}
		}
	}
	return ener;
}

double IPField::computeBulkPlasticEnergy(MElement *ele, const partDomain *dom, const IPStateBase::whichState ws) const
{
  IntPt *GP;
  double jac[3][3];
  int npts = dom->getBulkGaussIntegrationRule()->getIntPoints(ele,&GP);
  AllIPState::ipstateElementContainer *vips = _AIPS->getIPstate(ele->getNum());
  double ener =0.;
  for(int i=0;i<npts;i++){
    IPStateBase* ips = (*vips)[i];
    IPVariableMechanics *ipv = dynamic_cast<IPVariableMechanics*>(ips->getState(ws));
    if(ipv != NULL)
    {
      double enerpt = ipv->plasticEnergy();
      // gauss point weight
      double weight = GP[i].weight;
      double detJ = ele->getJacobianDeterminant(GP[i].pt[0],GP[i].pt[1],GP[i].pt[2]);
      ener += weight*detJ*enerpt;
    }
  }
  return ener;
}

double IPField::computeInterfacePlasticEnergy(MElement *ele, const dgPartDomain *dom, const IPStateBase::whichState ws) const
{
  IntPt *GP;
  double jac[3][3];
  int npts = dom->getInterfaceGaussIntegrationRule()->getIntPoints(ele,&GP);
  AllIPState::ipstateElementContainer *vips = _AIPS->getIPstate(ele->getNum());
  double ener =0.;
  for(int i=0;i<npts;i++)
  {
    IPStateBase* ipsm = (*vips)[i];
    IPStateBase* ipsp = (*vips)[i+npts];
    IPVariableMechanics *ipvm = dynamic_cast<IPVariableMechanics*>(ipsm->getState(ws));
    IPVariableMechanics *ipvp = dynamic_cast<IPVariableMechanics*>(ipsp->getState(ws));
    if(ipvm != NULL && ipvp != NULL)
    {
      IPVariable2ForFractureBase* ipvmFrac = dynamic_cast<IPVariable2ForFractureBase*>(ipvm);
      if (ipvmFrac != NULL){
        if (ipvmFrac->isbroken()){
          double enerpt = 0.5*(ipvm->plasticEnergy()+ipvp->plasticEnergy());
          // gauss point weight
          double weight = GP[i].weight;
          double detJ = ele->getJacobianDeterminant(GP[i].pt[0],GP[i].pt[1],GP[i].pt[2]);
          ener += weight*detJ*enerpt;
        }
      }
		}
  }
  return ener;
}


double IPField::computePlasticEnergy(const IPStateBase::whichState ws) const{
	const std::vector<partDomain*>* domainVector = _solver->getDomainVector();
	double ener=0.;
	for(std::vector<partDomain*>::const_iterator itdom=domainVector->begin(); itdom!=domainVector->end(); ++itdom){
		partDomain *dom = *itdom;
		for(elementGroup::elementContainer::const_iterator it=dom->element_begin(); it!=dom->element_end(); ++it){
			MElement *ele = it->second;
			ener += this->computeBulkPlasticEnergy(ele,dom,ws);
		}
    if (dom->IsInterfaceTerms()){
			dgPartDomain* dgdom = static_cast<dgPartDomain*>(dom);
			for(elementGroup::elementContainer::const_iterator it=dgdom->gi->begin(); it!=dgdom->gi->end(); ++it){
				MElement *ele = it->second;
			 #if defined(HAVE_MPI)
				if(Msg::GetCommSize()>1)
				{
					MInterfaceElement *iele = dynamic_cast<MInterfaceElement*>(ele);
					if(iele->getElem(0)->getPartition()==0 or iele->getElem(0)->getPartition() == Msg::GetCommRank()+1) // if - element is ghost not computed to avoid to count twice the contribution
					{
						ener += this->computeInterfacePlasticEnergy(ele,dgdom,ws);
					}
				}
				else
			 #endif // HAVE_MPI
				{
					ener += this->computeInterfacePlasticEnergy(ele,dgdom,ws);
				}

			}
		}
	}
	return ener;
}

double IPField::computeBulkDamageEnergy(MElement *ele, const partDomain *dom, const IPStateBase::whichState ws) const
{
  IntPt *GP;
  double jac[3][3];
  int npts = dom->getBulkGaussIntegrationRule()->getIntPoints(ele,&GP);
  AllIPState::ipstateElementContainer *vips = _AIPS->getIPstate(ele->getNum());
  double ener =0.;
  for(int i=0;i<npts;i++){
    IPStateBase* ips = (*vips)[i];
    IPVariableMechanics *ipv = dynamic_cast<IPVariableMechanics*>(ips->getState(ws));
    if(ipv != NULL)
    {
      double enerpt = ipv->damageEnergy();
      // gauss point weight
      double weight = GP[i].weight;
      double detJ = ele->getJacobianDeterminant(GP[i].pt[0],GP[i].pt[1],GP[i].pt[2]);
      ener += weight*detJ*enerpt;
    }
  }
  return ener;
}

double IPField::computeInterfaceDamageEnergy(MElement *ele, const dgPartDomain *dom, const IPStateBase::whichState ws) const
{
  IntPt *GP;
  double jac[3][3];
  int npts = dom->getInterfaceGaussIntegrationRule()->getIntPoints(ele,&GP);
  AllIPState::ipstateElementContainer *vips = _AIPS->getIPstate(ele->getNum());
  double ener =0.;
  for(int i=0;i<npts;i++){
    IPStateBase* ipsm = (*vips)[i];
    IPStateBase* ipsp = (*vips)[i+npts];
    IPVariableMechanics *ipvm = dynamic_cast<IPVariableMechanics*>(ipsm->getState(ws));
    IPVariableMechanics *ipvp = dynamic_cast<IPVariableMechanics*>(ipsp->getState(ws));
    if(ipvm != NULL && ipvp != NULL)
    {
      IPVariable2ForFractureBase* ipvmFrac = dynamic_cast<IPVariable2ForFractureBase*>(ipvm);
      if (ipvmFrac != NULL){
        if (ipvmFrac->isbroken()){
          double enerpt = 0.5*(ipvm->damageEnergy()+ipvp->damageEnergy());
          // gauss point weight
          double weight = GP[i].weight;
          double detJ = ele->getJacobianDeterminant(GP[i].pt[0],GP[i].pt[1],GP[i].pt[2]);
          ener += weight*detJ*enerpt;
        }
      }
		}
  }
  return ener;
}


double IPField::computeDamageEnergy(const IPStateBase::whichState ws) const{
	const std::vector<partDomain*>* domainVector = _solver->getDomainVector();
	double ener=0.;
	for(std::vector<partDomain*>::const_iterator itdom=domainVector->begin(); itdom!=domainVector->end(); ++itdom){
		partDomain *dom = *itdom;
		for(elementGroup::elementContainer::const_iterator it=dom->element_begin(); it!=dom->element_end(); ++it){
			MElement *ele = it->second;
			ener += this->computeBulkDamageEnergy(ele,dom,ws);
		}
    if (dom->IsInterfaceTerms()){
			dgPartDomain* dgdom = static_cast<dgPartDomain*>(dom);
			for(elementGroup::elementContainer::const_iterator it=dgdom->gi->begin(); it!=dgdom->gi->end(); ++it){
				MElement *ele = it->second;
			 #if defined(HAVE_MPI)
				if(Msg::GetCommSize()>1)
				{
					MInterfaceElement *iele = dynamic_cast<MInterfaceElement*>(ele);
					if(iele->getElem(0)->getPartition()==0 or iele->getElem(0)->getPartition() == Msg::GetCommRank()+1) // if - element is ghost not computed to avoid to count twice the contribution
					{
						ener += this->computeInterfaceDamageEnergy(ele,dgdom,ws);
					}
				}
				else
			 #endif // HAVE_MPI
				{
					ener += this->computeInterfaceDamageEnergy(ele,dgdom,ws);
				}

			}
		}
	}
	return ener;
}

double IPField::computeBulkPathFollowingLocalValue(MElement *ele, const partDomain *dom, const IPStateBase::whichState ws) const{
	IntPt *GP;
  double jac[3][3];
  int npts = dom->getBulkGaussIntegrationRule()->getIntPoints(ele,&GP);
  AllIPState::ipstateElementContainer *vips = _AIPS->getIPstate(ele->getNum());
  double ener =0.;
  for(int i=0;i<npts;i++){
    IPStateBase* ips = (*vips)[i];
    IPVariableMechanics *ipv = dynamic_cast<IPVariableMechanics*>(ips->getState(ws));
    if(ipv != NULL)
    {
      double enerpt = ipv->irreversibleEnergy();
      // gauss point weight
      double weight = GP[i].weight;
      double detJ = ele->getJacobianDeterminant(GP[i].pt[0],GP[i].pt[1],GP[i].pt[2]);
      ener += weight*detJ*enerpt;
    }
  }
  return ener;
};

double IPField::computeInterfacePathFollowingLocalValue(MElement *ele, const dgPartDomain *dom, const IPStateBase::whichState ws) const{
	IntPt *GP;
  static double jac[3][3];
  int npts = dom->getInterfaceGaussIntegrationRule()->getIntPoints(ele,&GP);
  AllIPState::ipstateElementContainer *vips = _AIPS->getIPstate(ele->getNum());
	double ener =0.;

  for(int i=0;i<npts;i++){ // loop on minus interface only to compute energy (otherwise it would be computed twice)
    IPStateBase* ips = (*vips)[i];
    IPVariableMechanics *ipv = static_cast<IPVariableMechanics*>(ips->getState(ws));

		IPVariable2ForFractureBase* ipvFrac = dynamic_cast<IPVariable2ForFractureBase*>(ipv);
		if (ipvFrac != NULL){
			// gauss point weight
			if (ipvFrac->isbroken()){
				double enerpt = ipv->irreversibleEnergy();
				double weight = GP[i].weight;
				double detJ = ele->getJacobianDeterminant(GP[i].pt[0],GP[i].pt[1],GP[i].pt[2]);
				ener += weight*detJ*enerpt;
			}

		}
  }
  return ener;
};

double IPField::computePathFollowingLocalValue(const IPStateBase::whichState ws) const{
  const std::vector<partDomain*>* domainVector = _solver->getDomainVector();
	double ener=0.;
  if (_solver->withPathFollowing()){
    if (_solver->getPathFollowingLocation() == pathFollowingManager::BULK_INTERFACE){
      for(std::vector<partDomain*>::const_iterator itdom=domainVector->begin(); itdom!=domainVector->end(); ++itdom){
        partDomain *dom = *itdom;
        for(elementGroup::elementContainer::const_iterator it=dom->element_begin(); it!=dom->element_end(); ++it){
          MElement *ele = it->second;
          ener += this->computeBulkPathFollowingLocalValue(ele,dom,ws);
        }

        // interface energy
        if(dom->IsInterfaceTerms()) // otherwise no fracture
        {
          const dgPartDomain* dgdom = static_cast<const dgPartDomain*>(dom);
          if((dgdom->getMaterialLawMinus()->getType() == materialLaw::fracture) and (dgdom->getMaterialLawPlus()->getType() == materialLaw::fracture))//comment it to get H1 norm in the interface  e.g. LinearThermoMech
          {
            for(elementGroup::elementContainer::const_iterator it=dgdom->gi->begin(); it!=dgdom->gi->end(); ++it)
            {
              MElement *ele = it->second;
             #if defined(HAVE_MPI)
              if(Msg::GetCommSize()>1)
              {
                MInterfaceElement *iele = dynamic_cast<MInterfaceElement*>(ele);
                if(iele->getElem(0)->getPartition()==0 or iele->getElem(0)->getPartition() == Msg::GetCommRank()+1) // if - element is ghost not computed to avoid to count twice the contribution
                {
                  ener += this->computeInterfacePathFollowingLocalValue(ele,dgdom,ws);
                }
              }
              else
             #endif // HAVE_MPI
              {
                ener += this->computeInterfacePathFollowingLocalValue(ele,dgdom,ws);
              }
            }
          }
        }
      }
    }
    else if (_solver->getPathFollowingLocation() == pathFollowingManager::INTERFACE){
      for(std::vector<partDomain*>::const_iterator itdom=domainVector->begin(); itdom!=domainVector->end(); ++itdom){
        partDomain *dom = *itdom;
        // interface energy
        if(dom->IsInterfaceTerms()) // otherwise no fracture
        {
          const dgPartDomain* dgdom = static_cast<const dgPartDomain*>(dom);
          if((dgdom->getMaterialLawMinus()->getType() == materialLaw::fracture) and (dgdom->getMaterialLawPlus()->getType() == materialLaw::fracture))//comment it to get H1 norm in the interface  e.g. LinearThermoMech
          {
            for(elementGroup::elementContainer::const_iterator it=dgdom->gi->begin(); it!=dgdom->gi->end(); ++it)
            {
              MElement *ele = it->second;
             #if defined(HAVE_MPI)
              if(Msg::GetCommSize()>1)
              {
                MInterfaceElement *iele = dynamic_cast<MInterfaceElement*>(ele);
                if(iele->getElem(0)->getPartition()==0 or iele->getElem(0)->getPartition() == Msg::GetCommRank()+1) // if - element is ghost not computed to avoid to count twice the contribution
                {
                  ener += this->computeInterfacePathFollowingLocalValue(ele,dgdom,ws);
                }
              }
              else
             #endif // HAVE_MPI
              {
                ener += this->computeInterfacePathFollowingLocalValue(ele,dgdom,ws);
              }
            }
          }
        }
      }
    }
    else if (_solver->getPathFollowingLocation() == pathFollowingManager::BULK){
      for(std::vector<partDomain*>::const_iterator itdom=domainVector->begin(); itdom!=domainVector->end(); ++itdom){
        partDomain *dom = *itdom;
        for(elementGroup::elementContainer::const_iterator it=dom->element_begin(); it!=dom->element_end(); ++it){
          MElement *ele = it->second;
          ener += this->computeBulkPathFollowingLocalValue(ele,dom,ws);
        }
      }
    }
    else{
      Msg::Error("path following location %d has not been correctly defined",_solver->getPathFollowingLocation());
    }
	}
	return ener;
};


int IPField::computeFractureEnergy(MElement *ele, const dgPartDomain *dom,double* arrayEnergy, const IPStateBase::whichState ws) const
{
  IntPt *GP;
  static double jac[3][3];
  int npts = dom->getInterfaceGaussIntegrationRule()->getIntPoints(ele,&GP);
  AllIPState::ipstateElementContainer *vips = _AIPS->getIPstate(ele->getNum());

  int ncomp=0;
  static double tmpArray[10];
  for(int i=0;i<npts;i++){ // loop on minus interface only to compute energy (otherwise it would be computed twice)
    IPStateBase* ips = (*vips)[i];
    IPVariableMechanics *ipv = static_cast<IPVariableMechanics*>(ips->getState(ws)); // The IPField:computeFractureEnergy(double* &) ensures that this is OK
    IPVariable2ForFractureBase* ipvFrac = dynamic_cast<IPVariable2ForFractureBase*>(ipv);
		if (ipvFrac != NULL){
			if(ipvFrac->isbroken())  //comment it to get H1 norm in the interface  e.g. LinearThermoMech
			{
						// gauss point weight
				double weight = GP[i].weight;
        double detJ = ele->getJacobianDeterminant(GP[i].pt[0],GP[i].pt[1],GP[i].pt[2]);
				const double wJ = weight*detJ;

				ncomp = ipv->fractureEnergy(tmpArray);
				for(int j=0;j<ncomp;j++)
					arrayEnergy[j]+= wJ*tmpArray[j];
			}

		}
  }
  return ncomp;
}

int IPField::computeFractureEnergy(double * arrayEnergy,const IPStateBase::whichState ws) const
{
	static double tmpArray[10];
	int maxcomp = 0;
	for(int i=0;i<10;i++){
		arrayEnergy[i]=0.;
		tmpArray[i]=0.;
	}
	const std::vector<partDomain*>* domainVector =  _solver->getDomainVector();
	for(std::vector<partDomain*>::const_iterator itdom=domainVector->begin(); itdom!=domainVector->end(); ++itdom){
		const partDomain *dom = *itdom;
		if(dom->IsInterfaceTerms()) // otherwise no fracture
		{
			const dgPartDomain* dgdom = static_cast<const dgPartDomain*>(dom);
			if((dgdom->getMaterialLawMinus()->getType() == materialLaw::fracture) and (dgdom->getMaterialLawPlus()->getType() == materialLaw::fracture))//comment it to get H1 norm in the interface  e.g. LinearThermoMech
			{
				for(elementGroup::elementContainer::const_iterator it=dgdom->gi->begin(); it!=dgdom->gi->end(); ++it)
				{
					MElement *ele = it->second;
				 #if defined(HAVE_MPI)
					if(Msg::GetCommSize()>1)
					{
						MInterfaceElement *iele = dynamic_cast<MInterfaceElement*>(ele);
						if(iele->getElem(0)->getPartition()==0 or iele->getElem(0)->getPartition() == Msg::GetCommRank()+1) // if - element is ghost not computed to avoid to count twice the contribution
						{
							int ncomp = this->computeFractureEnergy(ele,dgdom,tmpArray,ws);
							for(int i=0;i<ncomp;i++){
								arrayEnergy[i]+=tmpArray[i];
								tmpArray[i]=0.;
							}
							if(ncomp>maxcomp)
								maxcomp = ncomp;
						}
					}
					else
				 #endif // HAVE_MPI
					{
						int ncomp = this->computeFractureEnergy(ele,dgdom,tmpArray,ws);
						for(int i=0;i<ncomp;i++){
							arrayEnergy[i]+=tmpArray[i];
							tmpArray[i]=0.;
						}
						if(ncomp>maxcomp)
							maxcomp = ncomp;
					}
				}
			}
		}
	}
	return maxcomp;
}


IPField::IPField(nonLinearMechSolver* sl, const std::vector<ip2archive> &vaip,
          std::vector<dataBuildView> &dbview_, std::vector<dataBuildView> &dbviewInterface_,
            std::string filename, const std::string filenameInterface):
          _solver(sl),elementsField(filename,1,dbview_), _vBuildViewInterface(dbviewInterface_), _fileNamePrefixInterface(filenameInterface),
          _numOfActiveDissipationIPs(0),_numOfActiveDissipationIPsReached(0),
          _failureAndInternalAreChecked(false),_numOfNewlyBrokenElements(0),
          _totalNumOfNewlyBrokenElements(0), _failureStateIsFlushed(false),
          _lastSaveStep(-1)
{
  #if defined(HAVE_MPI)
  if (_solver->getNumRanks() > 1){
    std::vector<int> allFlags(3);
    if (_solver->isMultiscale()) allFlags[0] = 1;
    else allFlags[0] = 0;
    if (_solver->withEnergyDissipation()) allFlags[1] = 1;
    else allFlags[1] = 0;
    if (_solver->withFractureLaw()) allFlags[2] = 1;
    else allFlags[2] = 0;

    std::vector<int> maxAllFlags(3);
    MPI_Allreduce(&allFlags[0],&maxAllFlags[0],3,MPI_INT,MPI_MAX,MPI_COMM_WORLD);
    if (maxAllFlags[0] >0) _withMultiscale = true;
    else _withMultiscale = false;
    if (maxAllFlags[1] >0) _withDissipation = true;
    else _withDissipation = false;
    if (maxAllFlags[2] >0) _withCohesiveFailure = true;
    else _withCohesiveFailure = false;
  }
  else
  #endif // HAVE_MPI
  {
    _withMultiscale = _solver->isMultiscale();
    _withDissipation = _solver->withEnergyDissipation();
    _withCohesiveFailure = _solver->withFractureLaw();
  }

  if (_numStepBetweenTwoViews < _solver->getStepBetweenArchiving())
  {
    _numStepBetweenTwoViews = _solver->getStepBetweenArchiving();
  }

  // Creation of storage for IP data
	std::vector<partDomain*>* domainVector = _solver->getDomainVector();
  _AIPS = new AllIPState(*domainVector);
  // compute the number of element
  long int nelem=0;
  long int nelemInterface = 0;
  for(std::vector<partDomain*>::iterator itdom=domainVector->begin(); itdom!=domainVector->end(); ++itdom){
    partDomain *dom = *itdom;
    nelem+= dom->elementGroupSize();
    dgPartDomain* dgdom = dynamic_cast<dgPartDomain*>(dom);
    if (dgdom != NULL)
    {
      nelemInterface += dgdom->gi->size();
    }
  }
  this->setTotElem(nelem);
  this->setTotElemInterface(nelemInterface);

  // Build the viparch vector (find the nearest ipvariable to each given vertex)
  for(std::vector<ip2archive>::const_iterator ita=vaip.begin(); ita!=vaip.end(); ++ita){
    ip2archive aip(*ita);
    int dim = aip.dim;
    bool flagfind = false;
    bool passDomainSearch = false;
    int nummin = 0;
    if (dim == -1){
      if (aip.plusNum == aip.minusNum){
        // bulk case
        for(std::vector<partDomain*>::iterator itdom=domainVector->begin(); itdom!=domainVector->end(); ++itdom)
        {
          partDomain *dom = *itdom;
          for (elementGroup::elementContainer::const_iterator ite = dom->element_begin(); ite!= dom->element_end(); ite++){
            MElement* ele = ite->second;
            passDomainSearch = true;
            if (aip.plusNum == ele->getNum()){
              aip.ele.push_back(ele);
              aip.domain.push_back(dom);
              flagfind= true;
              break;
            }
          }
          if (flagfind) break;
        }
        if (!flagfind and passDomainSearch){
          Msg::Error("Element %d is not found",aip.plusNum);
        }
      }
      else{
        // interface case
        for(std::vector<partDomain*>::iterator itdom=domainVector->begin(); itdom!=domainVector->end(); ++itdom)
        {
          partDomain *dom = *itdom;
          dgPartDomain* dgDom = dynamic_cast<dgPartDomain*>(dom);
          if (dgDom != NULL){
             for (elementGroup::elementContainer::const_iterator ite = dgDom->gi->begin(); ite!= dgDom->gi->end(); ite++){
              MElement* e = ite->second;
              MInterfaceElement* ie = dynamic_cast<MInterfaceElement*>(e);
              int emNum = ie->getElem(0)->getNum();
              int epNum = ie->getElem(1)->getNum();
              passDomainSearch = true;
              if ((aip.plusNum == epNum and aip.minusNum == emNum) or (aip.plusNum == emNum and aip.minusNum == epNum)){
                aip.ele.push_back(e);
                aip.domain.push_back(dom);
                flagfind= true;
                break;
              }
            }
          }
          if (flagfind) break;
        }
        if (!flagfind and passDomainSearch){
          Msg::Warning("Interface Element with positive %d and negative %d is not found",Msg::GetCommRank(),aip.plusNum,aip.minusNum);
        }
      }

      nummin = aip.numgauss;
    }
    else if(dim == 0)
    {
      dgPartDomain *dgdom;
      bool samedim = false;
      for(std::vector<partDomain*>::iterator itdom=domainVector->begin(); itdom!=domainVector->end(); ++itdom)
      {
        partDomain *dom = *itdom;
        if(dom->IsInterfaceTerms())
        { // otherwise the domain doesn't contain interface element
          dgdom = static_cast<dgPartDomain*>(dom);
          for(elementGroup::elementContainer::const_iterator it2 = dgdom->gi->begin(); it2 != dgdom->gi->end(); ++it2 )
          {
            MElement *iele = it2->second; // two elements on interface
            int numv = iele->getNumVertices();
            for(int j=0;j<numv;j++)
            {
              if(aip._v == iele->getVertex(j))
              {
                aip.ele.push_back(iele);
                aip.domain.push_back(dom);
                flagfind = true;
                break;
              }
            }
            if(flagfind) break;
          }
          if(!flagfind)
          { // the interface element are on boundary
            for(elementGroup::elementContainer::const_iterator iti = dgdom->gib->begin(); iti!=dgdom->gib->end(); ++iti){
              MElement *ie = iti->second;
              int numv = ie->getNumVertices();
              for(int j=0;j<numv;j++){
                if(aip._v == ie->getVertex(j)){
                  aip.ele.push_back(ie);
                  aip.domain.push_back(dom);
                  flagfind = true;
                  break;
                }
              }
              if(flagfind) break;
            }
          }
        }
        if(!flagfind){ // no interface element found (Can happend on boundary)
          for(elementGroup::elementContainer::const_iterator it2 = dom->element_begin(); it2 != dom->element_end(); ++it2 ){
            MElement *ele = it2->second;
            int numv = ele->getNumVertices();
            for(int j=0;j<numv;j++){
              if(aip._v == ele->getVertex(j)){
                aip.ele.push_back(ele);
                samedim = true;
                flagfind = true;
                aip.domain.push_back(dom);
                break;
              }
            }
            if(flagfind) break;
          }
        }
        if(flagfind) break;
      }
      if(flagfind){ // If solve on more than one cpu it if possible that the point is not found
        // Now the element where is situated the node is know find the nearest Gauss Point
        IntPt *GP;
        QuadratureBase *gq;
        if(samedim){ //bulk point
          gq = aip.domain[0]->getBulkGaussIntegrationRule();
        }
        else{
          const dgPartDomain *dgfind = static_cast<const dgPartDomain*>(aip.domain[0]);
          gq = dgfind->getInterfaceGaussIntegrationRule();
        }
        int npts = gq->getIntPoints(aip.ele[0],&GP);
        // coordonate in uvw of vertex
        double uvw[3];
        double xyz[3];
        xyz[0] = aip._v->x(); xyz[1] = aip._v->y(); xyz[2] = aip._v->z();
        aip.ele[0]->xyz2uvw(xyz,uvw);
        double distmin = 1.e100; // inf value at itnitialization
        double dist, u,v,w;
        for(int i=0;i<npts;i++){
          u = GP[i].pt[0]; v = GP[i].pt[1]; w = GP[i].pt[2];
          dist = sqrt((u-uvw[0])*(u-uvw[0]) + (v-uvw[1])*(v-uvw[1]) + (w-uvw[2])*(w-uvw[2]));
          if(dist<distmin){
            distmin = dist;
            nummin = i;
          }
        }
      }
    }
    else
    {
      // get the vertex of the node
      elementGroup g(dim,aip.numphys);
      for(elementGroup::elementContainer::const_iterator ite = g.begin(); ite != g.end(); ite++)
      {
        dgPartDomain *dgdom;
        bool samedim = false;
        for(std::vector<partDomain*>::iterator itdom=domainVector->begin(); itdom!=domainVector->end(); ++itdom){
          partDomain *dom = *itdom;
          if(dom->IsInterfaceTerms())
          { // otherwise the domain doesn't contain interface element
            dgdom = static_cast<dgPartDomain*>(dom);
            for(elementGroup::elementContainer::const_iterator it2 = dgdom->gi->begin(); it2 != dgdom->gi->end(); ++it2 )
            {
              if(it2->second == ite->second)
              {
                aip.ele.push_back(ite->second);
                aip.domain.push_back(dom);
                flagfind = true;
              }
            }
            for(elementGroup::elementContainer::const_iterator iti = dgdom->gib->begin(); iti!=dgdom->gib->end(); ++iti)
            {
              if(iti->second == ite->second)
              {
                aip.ele.push_back(ite->second);
                aip.domain.push_back(dom);
                flagfind = true;
              }
            }
          }
          for(elementGroup::elementContainer::const_iterator it2 = dom->element_begin(); it2 != dom->element_end(); ++it2 )
          {
            if(it2->second == ite->second)
            {
              aip.ele.push_back(ite->second);
              aip.domain.push_back(dom);
              flagfind = true;
            }
          }
        }
      }
    }
    if(flagfind){ // If solve on more than one cpu it if possible that the point is not found
      // Now the element where is situated the node is know find the nearest Gauss Point
      // Store information
      aip.numgauss = nummin;
      viparch.push_back(ip2archive(aip));

    }
    else
    {
      #if defined(HAVE_MPI)
      if(Msg::GetCommSize() == 1)
      #endif // HAVE_MPI
      {
        Msg::Error("Unable to find an IPVariable where data is supposed to be archived");
      }
    }
  }

  _numStepBetweenTwoSaves = 1;
  for (int i=0; i< viparch.size(); i++)
  {
    viparch[i].openFile(_solver->getFileSavingPrefix());
    if (viparch[i].nstep > _numStepBetweenTwoSaves)
    {
      _numStepBetweenTwoSaves = viparch[i].nstep;
    }
  }
};

IPField::~IPField(){
  delete _AIPS;
}

double IPField::getIPcompAtGP(const partDomain *ef,MElement *ele, const IPStateBase::whichState ws,
                   const int cmp, const int gp, const nlsField::ValType vt) const
{
  IntPt *GP;
  int npts = 0.;
  MInterfaceElement* ie = dynamic_cast<MInterfaceElement*>(ele);
  if (ie != NULL)
  {
    const dgPartDomain* dgdom = dynamic_cast<const dgPartDomain*>(ef);
    if (dgdom==NULL)
    {
      Msg::Error("dgPartDomain must be used to get ip value");
      return 0;
    }
    else
    {
      npts = dgdom->getInterfaceGaussIntegrationRule()->getIntPoints(ele,&GP);
    }
    if (gp < 0 || gp  > 2*npts-1)
    {
      Msg::Error("gp %d does not exist",gp);
      return 0;
    };
  }
  else
  {
    npts = ef->getBulkGaussIntegrationRule()->getIntPoints(ele,&GP);
    if (gp < 0 || gp  > npts-1)
    {
      Msg::Error("gp %d does not exist",gp);
      return 0;
    };
  }

  IPStateBase* ips = (*_AIPS->getIPstate(ele->getNum()))[gp];
  if (vt == nlsField::val)
  {
    return ips->getState(ws)->get(cmp);
  }
  else if (vt == nlsField::increment)
  {
    const IPVariable* ipvcur = ips->getState(IPStateBase::current);
    const IPVariable* ipvprev = ips->getState(IPStateBase::previous);
    const ipFiniteStrain* ipvcurfinite = dynamic_cast<const ipFiniteStrain*>(ipvcur);
    const ipFiniteStrain* ipvprevfinite = dynamic_cast<const ipFiniteStrain*>(ipvprev);
    if (cmp == U_NORM)
    {
      const STensor3& F1 = ipvcurfinite->getConstRefToDeformationGradient();
      const STensor3& F0 = ipvprevfinite->getConstRefToDeformationGradient();
      static STensor3 U1, R1, U0, R0;
      STensorOperation::RUDecomposition(F1,U1,R1);
      STensorOperation::RUDecomposition(F0,U0,R0);
      U1 -= U0;
      return sqrt(STensorOperation::doubledot(U1,U1));
    }
    else if (cmp == GL_NORM)
    {
      const STensor3& F1 = ipvcurfinite->getConstRefToDeformationGradient();
      const STensor3& F0 = ipvprevfinite->getConstRefToDeformationGradient();
      static STensor3 C1, C0;
      STensorOperation::multSTensor3FirstTranspose(F1,F1,C1);
      STensorOperation::multSTensor3FirstTranspose(F0,F0,C0);
      C1 -= C0;
      return sqrt(0.25*STensorOperation::doubledot(C1,C1));
    }
    else
    {
      return ipvcur->get(cmp) - ipvprev->get(cmp);
    }
  }
  else if (vt == nlsField::rate)
  {
    double timeStep = ef->getMaterialLaw()->getTimeStep();
    const IPVariable* ipvcur = ips->getState(IPStateBase::current);
    const IPVariable* ipvprev = ips->getState(IPStateBase::previous);
    const ipFiniteStrain* ipvcurfinite = dynamic_cast<const ipFiniteStrain*>(ipvcur);
    const ipFiniteStrain* ipvprevfinite = dynamic_cast<const ipFiniteStrain*>(ipvprev);
    if (cmp == U_NORM)
    {
      const STensor3& F1 = ipvcurfinite->getConstRefToDeformationGradient();
      const STensor3& F0 = ipvprevfinite->getConstRefToDeformationGradient();
      static STensor3 U1, R1, U0, R0;
      STensorOperation::RUDecomposition(F1,U1,R1);
      STensorOperation::RUDecomposition(F0,U0,R0);
      U1 -= U0;
      return sqrt(STensorOperation::doubledot(U1,U1))/timeStep;
    }
    else if (cmp == GL_NORM)
    {
      const STensor3& F1 = ipvcurfinite->getConstRefToDeformationGradient();
      const STensor3& F0 = ipvprevfinite->getConstRefToDeformationGradient();
      static STensor3 C1, C0;
      STensorOperation::multSTensor3FirstTranspose(F1,F1,C1);
      STensorOperation::multSTensor3FirstTranspose(F0,F0,C0);
      C1 -= C0;
      return sqrt(0.25*STensorOperation::doubledot(C1,C1))/timeStep;
    }
    else
    {
      return (ipvcur->get(cmp) - ipvprev->get(cmp))/timeStep;
    }
  }
  else
  {
    Msg::Error("valType %d has not been used",vt);
    return 0.;
  };
};

double IPField::getIPcomp(const partDomain *ef,MElement *ele, const IPStateBase::whichState ws,
                            const int cmp,const nlsField::ElemValue ev, const int num, const nlsField::ValType vt) const{



  if(ev == nlsField::crude)
  { // value in a particular gauss point
    return getIPcompAtGP(ef,ele,ws,cmp,num,vt);
  }
  else
  { // loop on all IPVariable of an element and return a particular value (max, min, mean)
    MInterfaceElement* ie = dynamic_cast<MInterfaceElement*>(ele);
    double valcmp =0.;
    double valcmpp;
    IntPt *GP;
    if (ie!=NULL)
    {
      const dgPartDomain* dgdom = dynamic_cast<const dgPartDomain*>(ef);
      int npts = dgdom->getInterfaceGaussIntegrationRule()->getIntPoints(ele,&GP);
      AllIPState::ipstateElementContainer *vips = _AIPS->getIPstate(ele->getNum());
      for(int i=0;i<npts;i++){
        IPStateBase* ips = (*vips)[i];
        IPVariable *ipv = ips->getState(ws);
        valcmpp = 0.5*getIPcompAtGP(ef,ele,ws,cmp,i,vt)+0.5*getIPcompAtGP(ef,ele,ws,cmp,i+npts,vt);;
        if(i==0)
          valcmp = valcmpp;
        else{
          switch(ev){
            case nlsField::max :
              if(valcmpp>valcmp) valcmp=valcmpp;
              break;
            case nlsField::min :
              if(valcmpp<valcmp) valcmp=valcmpp;
              break;
            case nlsField::mean :
              valcmp+=valcmpp;
              break;
          }
        }
      }
      if(ev==nlsField::mean) valcmp/=(double)npts;
      return valcmp;
    }
    else
    {

      int npts = ef->getBulkGaussIntegrationRule()->getIntPoints(ele,&GP);
      AllIPState::ipstateElementContainer *vips = _AIPS->getIPstate(ele->getNum());
      for(int i=0;i<npts;i++){
        IPStateBase* ips = (*vips)[i];
        IPVariable *ipv = ips->getState(ws);
        valcmpp = getIPcompAtGP(ef,ele,ws,cmp,i,vt);
        if(i==0)
          valcmp = valcmpp;
        else{
          switch(ev){
            case nlsField::max :
              if(valcmpp>valcmp) valcmp=valcmpp;
              break;
            case nlsField::min :
              if(valcmpp<valcmp) valcmp=valcmpp;
              break;
            case nlsField::mean :
              valcmp+=valcmpp;
              break;
          }
        }
      }
      if(ev==nlsField::mean) valcmp/=(double)npts;
      return valcmp;
    }
  }
};
double IPField::getIPcompAndVolume(const partDomain *ef,MElement *ele, const IPStateBase::whichState ws,
                           const int cmp, const nlsField::ElemValue ev, const nlsField::ValType vt, double &vol) const
{
  if(ev == nlsField::crude)
  {
     Msg::Error("Cannot archive on a non nodal endity crude values");
     return 0.;
  }
  else
  {
    vol = 0.;
    double valcmp =0.;
    double jac[3][3];
    double valcmpp;
    IntPt *GP;
    int npts = ef->getBulkGaussIntegrationRule()->getIntPoints(ele,&GP);
    AllIPState::ipstateElementContainer *vips = _AIPS->getIPstate(ele->getNum());

    for(int i=0;i<npts;i++){
      double weight = GP[i].weight;
      const double detJ = ele->getJacobianDeterminant(GP[i].pt[0],GP[i].pt[1],GP[i].pt[2]);

      valcmpp = getIPcompAtGP(ef,ele,ws,cmp,i,vt);
      vol += detJ*weight;
      if(i==0)
      {
        switch(ev){
          case nlsField::max :
            valcmp = valcmpp;
            break;
          case nlsField::min :
           valcmp = valcmpp;
            break;
          case nlsField::mean :
            valcmp =valcmpp*detJ*weight;
            break;
        }
      }
      else{
        switch(ev){
          case nlsField::max :
            if(valcmpp>valcmp) valcmp=valcmpp;
            break;
          case nlsField::min :
            if(valcmpp<valcmp) valcmp=valcmpp;
            break;
          case nlsField::mean :
            valcmp+=valcmpp*detJ*weight;
            break;
        }
      }
    }
    if(ev==nlsField::mean) valcmp/=vol;
    return valcmp;
  }
}

void IPField::setFactorOnArchivingFiles(const int fact)
{
  for(int i=0;i<viparch.size();i++)
  {
    viparch[i].nstep*=fact;
  }
  _numStepBetweenTwoSaves *= fact;
}

void IPField::setTime(const double time, const double dtime, const int numstep)
{
  _AIPS->setTime(time,dtime,numstep);
}

void IPField::blockDissipation(const IPStateBase::whichState ws, const bool fl){
  AllIPState::ipstateContainer& allMap =*(_AIPS->getAIPSContainer());
  for(AllIPState::ipstateContainer::iterator it=allMap.begin(); it!=allMap.end();++it){
    std::vector<IPStateBase*>& vips = ((*it).second);
    for(unsigned int i=0;i<vips.size();i++){
      IPVariable* ipv = vips[i]->getState(ws);
			IPVariableMechanics* ipvMeca = dynamic_cast<IPVariableMechanics*>(ipv);
			if (ipvMeca!=NULL){
				ipvMeca->blockDissipation(fl);
			}
    }
  }
}

void IPField::restart()
{
  elementsField::restart();
  // loop on all ipstates
  _AIPS->restart();

  // since the nextStep() is called before creating restart file
  // only the following parameters needs to save
  restartManager::restart(_lastSaveStep);
  restartManager::restart(_numStepBetweenTwoSaves);
  //
  restartManager::restart(_numOfActiveDissipationIPs);
  restartManager::restart(_numOfActiveDissipationIPsReached);

  restartManager::restart(_brokenElements);         // all interface broken element is stored in order to avoid rechecking
  restartManager::restart(_blockedDissipationElements);     // all blocked element is stored in order to avoid rechecking/reblocking
  return;
}


void IPField::addOneBlockedDissipationElement(MElement* ele){
  _newlyBlockedDissipationElements.insert(ele->getNum());
};


bool IPField::dissipationIsBlockedOnElement(MElement* ele) const{
  return _blockedDissipationElements.find(ele->getNum()) != _blockedDissipationElements.end();
};

void IPField::addOneBrokenElement(MElement* ele){
  MInterfaceElement* ie = dynamic_cast<MInterfaceElement*>(ele);
  if (ie == NULL){
    if (_newlyBrokenElements.find(ele->getNum()) == _newlyBrokenElements.end()){
      _newlyBrokenElements.insert(ele->getNum());
      _numOfNewlyBrokenElements ++;
    }
  }
  else{
    if (_newlyBrokenElements.find(ele->getNum()) == _newlyBrokenElements.end()){
      _newlyBrokenElements.insert(ele->getNum());
      if (ie->getElem(0)->getPartition() == ie->getElem(1)->getPartition()){
        _numOfNewlyBrokenElements ++;
      }
      #if defined(HAVE_MPI)
      else if (Msg::GetCommRank()+1 == std::min(ie->getElem(0)->getPartition(),ie->getElem(1)->getPartition())){
        _numOfNewlyBrokenElements ++;
      }
      #endif //HAVE_MPI
    }
  }
};

bool IPField::elementIsBroken(MElement* ele) const{
  return _brokenElements.find(ele->getNum()) != _brokenElements.end();
};

void IPField::blockDissipationIP(const int ip){
	_blockedDissipationIPs.insert(ip);
};
void IPField::brokenIP(const int ip){
	_brokenIPs.insert(ip);
};

void IPField::solverBroken(const int ip){
  _solverBrokenIPs.insert(ip);
};
