//
// Description: storing class for umat interface
// Author:  <L. NOELS>, (C) 2019
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef IPUMATINTERFACE_H_
#define IPUMATINTERFACE_H_
#include "ipvariable.h"
#include "STensor3.h"
#include "fullMatrix.h"
class IPUMATInterface : public IPVariableMechanics
{
 protected:
  int _nsdv;

 public: // All data public to avoid the creation of function to access

// x y z
  fullMatrix<double> _materialTensor;
  fullVector<double> _strain; // epsilon
  fullVector<double> _stress; //cauchy
// state variables
  double* _statev;
  double _elasticEne;
  double _plasticEne;
  double _damageEne;
  double _elementSize;
  bool _dissipationBlocked;

 public:
  IPUMATInterface(int _nsdv,double eleSize) : IPVariableMechanics(), _nsdv(_nsdv), _strain(6), _stress(6),
                            _materialTensor(6,6),_elasticEne(0.),
                            _plasticEne(0.),_damageEne(0.), _elementSize(eleSize), _dissipationBlocked(false)
    {
        _statev = new double[_nsdv];
        for(int i = 0; i<_nsdv; i++) _statev[i]=0.;
    }
  IPUMATInterface(const IPUMATInterface &source) : IPVariableMechanics(source)
    {
        _nsdv = source._nsdv;
        _statev = new double[_nsdv];
        for(int i = 0; i<_nsdv; i++) _statev[i]=source._statev[i];
        _strain =source._strain;
        _stress =source._stress;
	_elasticEne=source._elasticEne;
	_plasticEne=source._plasticEne;
	_damageEne=source._damageEne;
	_elementSize=source._elementSize;
        _dissipationBlocked = source._dissipationBlocked;
    }
  IPUMATInterface &operator = (const IPVariable &_source)
  {
      IPVariableMechanics::operator=(_source);
      const IPUMATInterface *source=static_cast<const IPUMATInterface *> (&_source);
      if(_nsdv !=source->_nsdv) Msg::Error("IPUMATInterface ips do not have the same number of internal variables");
      for(int i = 0; i<_nsdv; i++) _statev[i]=source->_statev[i];
      _strain = source->_strain;
      _stress = source->_stress;
      _materialTensor=source->_materialTensor;
      _elasticEne=source->_elasticEne;
      _plasticEne=source->_plasticEne;
      _dissipationBlocked = source->_dissipationBlocked;
      _damageEne=source->_damageEne;
      _elementSize=source->_elementSize;
      return *this;
  }
  virtual ~IPUMATInterface()
  {
      delete []_statev;
      _statev=NULL;
  }
  // Archiving data
  virtual void blockDissipation(const bool fl){_dissipationBlocked = fl;};
  virtual bool dissipationIsBlocked() const {return _dissipationBlocked;};
  virtual double get(const int i) const;
  virtual double defoEnergy() const;
  virtual double plasticEnergy() const;
  virtual double damageEnergy() const;
  virtual double elementSize() const;
  double &getRefToDefoEnergy() {return _elasticEne;}
  double &getRefToPlasticEnergy() {return _plasticEne;}
  double &getRefToDamageEnergy() {return _damageEne;}
  double &getRefToElementSize() {return _elementSize;}

  virtual double getNsdv() const {return _nsdv;}
  virtual IPVariable* clone() const {return new IPUMATInterface(*this);};
  virtual void restart();
 protected:
 IPUMATInterface(){}
};

#endif // IPUMATINTERFACE_H_
