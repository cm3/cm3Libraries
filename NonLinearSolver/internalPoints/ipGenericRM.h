// Header: IPVariable For mlawGenericRM
// mlawGenericRM is a generic interface that lets
// impose a residual deformation gradient (Fres), to 
// apply the mech constitutive law on a pre strained configuration.
//
// Note: 
//  Build View of 9 Fres Components (XY) are:
//  IPField::FfAM_XY
//
// Created by mohib on 14.08.24.
//

#ifndef IPGENERICRM_H_
#define IPGENERICRM_H_

#include "ipvariable.h"
#include "STensor3.h"
#include "STensorOperations.h"
class IPGenericRM : public IPVariableMechanics
{
protected:
    IPVariableMechanics * _ipMeca;
    // Residual Deformation Gradient to be imposed on the stress free configuration.
    // Note: It will be read from a csv File. @Mohib
    STensor3 FRes; 
public:
    IPGenericRM(const STensor3 &_FRes);

public:
    ~IPGenericRM()
    {
        if(_ipMeca!=NULL)
        {
            delete _ipMeca;
            _ipMeca=NULL;
        }
    }

    IPGenericRM(const IPGenericRM &source);
    virtual IPGenericRM& operator=(const IPVariable &source);
    virtual double defoEnergy() const{return _ipMeca->defoEnergy();};
    virtual double plasticEnergy() const{return _ipMeca->plasticEnergy();}
    virtual double damageEnergy() const{return  _ipMeca->damageEnergy();}
    virtual double irreversibleEnergy() const{return  _ipMeca->irreversibleEnergy();}

    virtual void restart();
    virtual void setValueForBodyForce(const STensor43& K, const int ModuliType){ _ipMeca->setValueForBodyForce(K,ModuliType); }
    virtual const STensor43 &getConstRefToTangentModuli() const{return _ipMeca->getConstRefToTangentModuli();};
    virtual void computeBodyForce(const STensor33& G, SVector3& B) const { _ipMeca->computeBodyForce(G,B); }

    virtual IPVariable* clone() const {return new IPGenericRM(*this);};

    const IPVariableMechanics& getConstRefToIpMeca() const {return *_ipMeca;}
    IPVariableMechanics& getRefToIpMeca() {return *_ipMeca;}
    void setIpMeca(IPVariable& ipMeca)
    {
        if(_ipMeca!=NULL)
        {
            delete _ipMeca;
            _ipMeca=NULL;
        }
        _ipMeca= (dynamic_cast<IPVariableMechanics *>(ipMeca.clone()));
    }

    // Getter for accessing Residual Deformation Gradient @Mohib
    const STensor3 & getConstRefToFRes() const { return FRes;}
    STensor3 & getRefToFRes(){ return FRes;}

    virtual double get(const int comp) const;
    virtual double & getRef(const int comp);
  private:
      IPGenericRM(){};

};

#endif //IPGENERICTM_H_
