//
// Description: storing class for linear thermomechanical law it is a "virtual implementation" you have to define an ipvariable
//              in your project which derive from this one.
// Author:  <L. Homsy>, (C) 2015
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef IPANISOTROPICELECTHERMECH_H_
#define IPANISOTROPICELECTHERMECH_H_
#include "ipvariable.h"

#include "ipAnIsotropicTherMech.h"

class IPAnIsotropicElecTherMech : public IPAnIsotropicTherMech
{
public:
  double _ElecThermEnergy;
public:

  IPAnIsotropicElecTherMech();
  IPAnIsotropicElecTherMech(const IPAnIsotropicElecTherMech &source);
  IPAnIsotropicElecTherMech& operator=(const IPVariable &source);
  virtual IPVariable* clone() const {return new IPAnIsotropicElecTherMech(*this);};
  virtual void restart(); 
};

#endif // IPANISOTROPICELECTHERMECH_H_
