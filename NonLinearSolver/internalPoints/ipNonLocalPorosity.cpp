//
// Description: storing class for gurson model
//
//
// Author:  <L. Noels>, (C) 2013
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "ipNonLocalPorosity.h"
#include "mlawNonLocalPorousCoalescence.h"



IPNonLocalPorosity::IPNonLocalPorosity() : IPVariableMechanics(),  _elasticEnergy(0.), _plasticEnergy(0.), _irreversibleEnergy(0.),
                                                   _eplmatrix(0.), _fV(0),  _fVstar(0), _fVtilde(0.), _yieldfV(0.),
                                                   _dissipationBlocked(false),_NonLocalToLocal(false), _Fp(1.),
                                                   _Ee(0.),_DirreversibleEnergyDF(0.), _DirreversibleEnergyDtildefV(0),
                                                   _DplasticEnergyDF(0.),_DplasticEnergyDtildefV(0), _DplasticEnergyDT(0.),
                                                   _deformationGradient(NULL),
                                                    _currentOutwardNormal(NULL),_referenceOutwardNormal(NULL),_corKir(0.), _yieldStress(0.),
                                                  _failed(false), _hatq(0.),_nonlocalEplmatrix(0.),_nonlocalHatQ(0.), _fVinitial(0.),
                                                  _eplMacro(0.), _eplMacroNonLocal(0.), _lnfV(0.),_lnfVtilde(0.), _T(288.), _dissipationActive(false),
                                                  _thermalEnergy(0.),_ThermoElasticCoupling(0.), _DplasticEnergyDEplmatrix(0.),
                                                  _successFlag(true),
                                                  _anisotropicMatrix(0), _DanisotropicMatrixDF(0.), _DanisotropicMatrixDEp(0.),
                                                  _effectivePressure(0),_effectiveSVM(0.)
{
  // Default constructor without arguments is forbiden as it is incorrect and incomplete
  ipvJ2IsotropicHardening=NULL;
  ipvCL.resize(0);
  ipvNucleation = NULL;
  ipvCoal=NULL;
  ipvVoidState = NULL;
  Msg::Error("IPNonLocalPorosity::IPNonLocalPorosity is not initialized with a characteritsic IP Variable nor a IP CLength, nor IP Damage Nucleation");
};




IPNonLocalPorosity::IPNonLocalPorosity(double fVinitial, const J2IsotropicHardening* j2IH,
                                       const std::vector<CLengthLaw*>* cll,
                                       const NucleationLaw* nuclLaw,
                                       const CoalescenceLaw* coaleslaw,
                                       const voidStateEvolutionLaw* evolLaw):
       IPVariableMechanics(),
       _elasticEnergy(0.), _plasticEnergy(0.), _irreversibleEnergy(0.), _eplmatrix(0.),
       _fV(fVinitial), _fVstar(fVinitial), _fVtilde(fVinitial), _yieldfV(fVinitial),
       _Fp(1.),_Ee(0.), _DirreversibleEnergyDF(0.), _DplasticEnergyDF(0.), _DplasticEnergyDT(0.),
       _dissipationBlocked(false), _NonLocalToLocal(false),
       _currentOutwardNormal(NULL), _referenceOutwardNormal(NULL),_deformationGradient(NULL),
       _corKir(0.),_failed(false),_hatq(0.),_nonlocalEplmatrix(0.),_nonlocalHatQ(0.), _fVinitial(fVinitial),
       _eplMacro(0.),_eplMacroNonLocal(0.),  _lnfV(0.),_lnfVtilde(0.),_T(288.), _dissipationActive(false),
       _thermalEnergy(0.),_ThermoElasticCoupling(0.), _DplasticEnergyDEplmatrix(0.),
       _successFlag(true), _anisotropicMatrix(0), _DanisotropicMatrixDF(0.), _DanisotropicMatrixDEp(0.),
       _effectivePressure(0.),_effectiveSVM(0.)
{
  // Real constructor
  // Initialisation of pointers for internal laws Ipvs and other related variables
  // -Non-local lenght
    // create the IPVariable and fill the container if the law is not empty
    // but empty the container first
  ipvCL.resize(0);
/* why here ???? */
  _DirreversibleEnergyDtildefV.resize(0); 
  _DplasticEnergyDtildefV.resize(0);
  if(cll == NULL){
    Msg::Error("IPNonLocalPorosity::IPNonLocalPorosity has no cll");
  }
  else{
    ipvCL.resize(cll->size());
    for (int i=0; i< cll->size(); i++){
      ipvCL[i] = NULL;
      cll->operator[](i)->createIPVariable(ipvCL[i]);
      _DirreversibleEnergyDtildefV.push_back(0.);
      _DplasticEnergyDtildefV.push_back(0.);
    }
  }


  // -Hardening law
    // create the IPVariable if the law is not empty
  ipvJ2IsotropicHardening = NULL;
  if(j2IH == NULL){
    Msg::Error("IPNonLocalPorosity::IPNonLocalPorosity has no j2IH");
  }
  else{
    j2IH->createIPVariable(ipvJ2IsotropicHardening);
  }

    // set the initial viscoplastic yield stress
  _yieldStress = j2IH->getYield0();


  // -Nucleation law vector:
    // create all the IPVariables and fill the container if the law is not empty
    // but empty the container first
  ipvNucleation = NULL;
  if (nuclLaw == NULL)
  {
    Msg::Error("IPNonLocalPorosity::IPNonLocalPorosity  has no nucleation");
  }
  else{
    nuclLaw->createIPVariable(ipvNucleation);
  }
  
  
  // -Coalescence law and related geometrical parameters
   // create the IPVariable if the law is not empty for:
    // Void state
  ipvVoidState = NULL;
  if (evolLaw == NULL){
    Msg::Error("IPNonLocalPorosity::IPNonLocalPorosity has no void evolution law");
  }
  else{
    evolLaw->createIPVariable(fVinitial,ipvVoidState);
  }
    // Coalescence law
  ipvCoal=NULL;
  if(coaleslaw ==NULL){
    Msg::Error("IPNonLocalPorosity::IPNonLocalPorosity has no Coalescence law");
  }
  else{
    coaleslaw->createIPVariable(ipvCoal);
    if (ipvVoidState!=NULL)
    {
      ipvCoal->setIPvAtCoalescenceOnset(*this);
    }
  }
    
  // Ultimate check on variable compability
    // Check if the initial porosity value is possible
  if(fVinitial < 0. or fVinitial > 1.){Msg::Error("IPNonLocalPorosity::IPNonLocalPorosity wrong initial porosity");};
  
};





IPNonLocalPorosity::IPNonLocalPorosity(const IPNonLocalPorosity &source) :
      IPVariableMechanics(source),
      _elasticEnergy(source._elasticEnergy),
      _plasticEnergy(source._plasticEnergy),
      _irreversibleEnergy(source._irreversibleEnergy),
      _eplmatrix(source._eplmatrix),
      _fV(source._fV),
      _fVtilde(source._fVtilde),
      _fVstar(source._fVstar),
      _yieldfV(source._yieldfV),
      _Fp(source._Fp),
      _Ee(source._Ee),
      _dissipationBlocked(source._dissipationBlocked),
      _NonLocalToLocal(source._NonLocalToLocal),
      _DirreversibleEnergyDF(source._DirreversibleEnergyDF),
      _DirreversibleEnergyDtildefV(source._DirreversibleEnergyDtildefV),
      _DplasticEnergyDF(source._DplasticEnergyDF),_DplasticEnergyDtildefV(source._DplasticEnergyDtildefV),
      _DplasticEnergyDT(source._DplasticEnergyDT),
      _currentOutwardNormal(source._currentOutwardNormal),_referenceOutwardNormal(source._referenceOutwardNormal),
      _deformationGradient(source._deformationGradient),
      _corKir(source._corKir), _yieldStress(source._yieldStress), _failed(source._failed),
      _hatq(source._hatq),_nonlocalEplmatrix(source._nonlocalEplmatrix),_nonlocalHatQ(source._nonlocalHatQ), _fVinitial(source._fVinitial),
      _eplMacro(source._eplMacro), _eplMacroNonLocal(source._eplMacroNonLocal),
      _lnfV(source._lnfV),_lnfVtilde(source._lnfVtilde),_T(source._T), _dissipationActive(source._dissipationActive),
      _thermalEnergy(source._thermalEnergy),_ThermoElasticCoupling(source._ThermoElasticCoupling),
      _DplasticEnergyDEplmatrix(source._DplasticEnergyDEplmatrix),
      _successFlag(source._successFlag),
      _anisotropicMatrix(source._anisotropicMatrix), _DanisotropicMatrixDF(source._DanisotropicMatrixDF), 
      _DanisotropicMatrixDEp(source._DanisotropicMatrixDEp),
      _effectivePressure(source._effectivePressure),_effectiveSVM(source._effectiveSVM)
{

  // Copy all pointers for internal laws Ipvs
  // -Copy clength
  ipvCL.resize(source.ipvCL.size());
  for (int i=0; i< source.ipvCL.size(); i++){
    if(source.ipvCL[i] != NULL){
      ipvCL[i] =source.ipvCL[i]->clone();
    }
    else{
      ipvCL[i] = NULL;
    }
  }
  // -Copy hardening law
  ipvJ2IsotropicHardening = NULL;
  if(source.ipvJ2IsotropicHardening != NULL){
    ipvJ2IsotropicHardening = source.ipvJ2IsotropicHardening->clone();
  }
  
  ipvNucleation = NULL;
  if (source.ipvNucleation != NULL)
  {
    ipvNucleation = source.ipvNucleation->clone();
  }


  // -Copy coalesncence and void state
  ipvCoal = NULL;
  if(source.ipvCoal != NULL){
    ipvCoal =source.ipvCoal->clone();
  }
  ipvVoidState= NULL;
  if (source.ipvVoidState != NULL){
    ipvVoidState = source.ipvVoidState->clone();
  }
};




IPNonLocalPorosity& IPNonLocalPorosity::operator=(const IPVariable &source)
{
  // Copy mother class
  IPVariableMechanics::operator=(source);
  
  // Copy this class
  const IPNonLocalPorosity* src = dynamic_cast<const IPNonLocalPorosity*>(&source);
  if(src != NULL)
  {
    // Copy internal law ipvs
    //-Copy hardening ipv
    if(src->ipvJ2IsotropicHardening != NULL)
    {
      // if the source is not empty, 
      // copy (if the current ipv exist) 
      // or clone (create a new one if the current does not exist) from the source ipv
      if (ipvJ2IsotropicHardening != NULL){
        ipvJ2IsotropicHardening->operator=(*src->ipvJ2IsotropicHardening);
      }
      else{
        ipvJ2IsotropicHardening = src->ipvJ2IsotropicHardening->clone();
      }
    }
    else{
      // Otherwise, if the source is empty, clean the ipv
      if(ipvJ2IsotropicHardening != NULL){delete ipvJ2IsotropicHardening;};
      ipvJ2IsotropicHardening = NULL;
    }
    
    //-Copy Cl ipv-vector
    // Redo the same operation as for the hardening law, but for each element
    // If the vector are not of the same size, clean everything before copying
    if(src->ipvCL.size()  == ipvCL.size()){
      for (int i=0; i<src->ipvCL.size(); i++){
        if (src->ipvCL[i] != NULL){
          if (ipvCL[i] != NULL){
            ipvCL[i]->operator=(*src->ipvCL[i]);
          }
          else
            ipvCL[i]= src->ipvCL[i]->clone();
        }
        else{
          if (ipvCL[i] != NULL) {
            delete ipvCL[i]; ipvCL[i] = NULL;
          }
        }
      }
    }
    else{
      for (int i=0; i<ipvCL.size(); i++){
        if (ipvCL[i] != NULL) delete ipvCL[i];
      };
      ipvCL.resize(src->ipvCL.size());
      for (int i=0; i<src->ipvCL.size(); i++){
        if (src->ipvCL[i] != NULL){
          ipvCL[i]= src->ipvCL[i]->clone();
        }
        else{
          ipvCL[i] = NULL;
        }
      }
    };
  
    // copy nucleation law
    if(src->ipvNucleation != NULL){
      if (ipvNucleation != NULL){
        ipvNucleation->operator=(*src->ipvNucleation);
      }
      else{
        ipvNucleation= src->ipvNucleation->clone();
      }
    }
    else{
      if(ipvNucleation != NULL) delete ipvNucleation; ipvNucleation = NULL;
    }
    


    //-Copy Coalescence and void state IPVs
    // idem
    if(src->ipvCoal != NULL){
      if (ipvCoal != NULL){
        ipvCoal->operator=(*src->ipvCoal);
      }
      else{
        ipvCoal= src->ipvCoal->clone();
      }
    }
    else{
      if(ipvCoal != NULL) delete ipvCoal; ipvCoal = NULL;
    }
    
    // idem
    if (src->ipvVoidState != NULL){
      if (ipvVoidState != NULL){
        ipvVoidState->operator=(*src->ipvVoidState);
      }
      else{
        ipvVoidState = src->ipvVoidState->clone();
      }
    }
    else{
      if (ipvVoidState != NULL) {delete ipvVoidState; ipvVoidState = NULL;};
    }

    
    // Copying proper variables 
    _elasticEnergy=src->_elasticEnergy;
    _plasticEnergy = src->_plasticEnergy;
    _irreversibleEnergy = src->_irreversibleEnergy;
    _eplmatrix=src->_eplmatrix;
    _fV=src->_fV;
    _fVtilde = src->_fVtilde;
    _fVstar=src->_fVstar;
    _yieldfV = src->_yieldfV;
    _Fp=src->_Fp;
    _Ee=src->_Ee;
    _dissipationBlocked = src->_dissipationBlocked;
    _NonLocalToLocal = src->_NonLocalToLocal;
    _DirreversibleEnergyDF = src->_DirreversibleEnergyDF;
    _DirreversibleEnergyDtildefV = src->_DirreversibleEnergyDtildefV;
    _DplasticEnergyDF = src->_DplasticEnergyDF;
    _DplasticEnergyDtildefV = src->_DplasticEnergyDtildefV;
    _DplasticEnergyDT = src->_DplasticEnergyDT;
    _currentOutwardNormal = src->_currentOutwardNormal;
    _referenceOutwardNormal = src->_referenceOutwardNormal;
    _deformationGradient = src->_deformationGradient;
    _corKir = src->_corKir;
    _yieldStress = src->_yieldStress;
    _failed = src->_failed;
    _hatq = src->_hatq;
    _nonlocalEplmatrix = src->_nonlocalEplmatrix;
    _nonlocalHatQ = src->_nonlocalHatQ;
    _fVinitial = src->_fVinitial;
    _eplMacro = src->_eplMacro;
    _eplMacroNonLocal = src->_eplMacroNonLocal;
    _lnfV = src->_lnfV;
    _lnfVtilde = src->_lnfVtilde;
    _T = src->_T;
    _dissipationActive = src->_dissipationActive;
    _thermalEnergy = src->_thermalEnergy;
    _ThermoElasticCoupling = src->_ThermoElasticCoupling;
    _DplasticEnergyDEplmatrix = src->_DplasticEnergyDEplmatrix;
    _successFlag = src->_successFlag;
    _anisotropicMatrix = src->_anisotropicMatrix;
    _DanisotropicMatrixDF = src->_DanisotropicMatrixDF;
    _DanisotropicMatrixDEp = src->_DanisotropicMatrixDEp;
    
    _effectivePressure = src->_effectivePressure;
    _effectiveSVM = src->_effectiveSVM;
  }
  return *this;
};



IPNonLocalPorosity::~IPNonLocalPorosity()
{
  // Delete related internal object to each pointer of internal laws
  // NB: _deformationGradient, _currentOutwardNormal, _referenceOutwardNormal 
  // are stored somewhere else and do not need to be deleted here !
  
  // Delete hardening ipv
  if(ipvJ2IsotropicHardening != NULL) delete ipvJ2IsotropicHardening;

  // Delete cl vector ipv
  for (int i=0; i< ipvCL.size(); i++){
    if(ipvCL[i] != NULL) delete ipvCL[i];
    ipvCL[i] = NULL;
  }
  
  if (ipvNucleation!=NULL) delete ipvNucleation;
  
  
  // Delete coalesnce and void state ipv
  if(ipvCoal != NULL) delete ipvCoal;
  if(ipvVoidState != NULL) delete ipvVoidState;
}



void IPNonLocalPorosity::restart()
{
  // Mother class
  IPVariableMechanics::restart();
  // Internal laws ipv
  restartManager::restart(ipvCL);
  restartManager::restart(ipvJ2IsotropicHardening);
  restartManager::restart(ipvNucleation);
  restartManager::restart(ipvVoidState);
  restartManager::restart(ipvCoal);
  // Proper variables
  restartManager::restart(_elasticEnergy);
  restartManager::restart(_plasticEnergy);
  restartManager::restart(_irreversibleEnergy);
  restartManager::restart(_eplmatrix);
  restartManager::restart(_fV);
  restartManager::restart(_fVtilde);
  restartManager::restart(_fVstar);
  restartManager::restart(_yieldfV);
  restartManager::restart(_Fp);
  restartManager::restart(_Ee);
  restartManager::restart(_dissipationBlocked);
  restartManager::restart(_NonLocalToLocal);
  restartManager::restart(_DirreversibleEnergyDF);
  restartManager::restart(_DirreversibleEnergyDtildefV);
  restartManager::restart(_DplasticEnergyDF);
  restartManager::restart(_DplasticEnergyDtildefV);
  restartManager::restart(_DplasticEnergyDT);
  restartManager::restart(_corKir);
  restartManager::restart(_yieldStress);
  restartManager::restart(_failed);
  restartManager::restart(_hatq);
  restartManager::restart(_nonlocalEplmatrix);
  restartManager::restart(_nonlocalHatQ);
  restartManager::restart(_fVinitial);
  restartManager::restart(_eplMacro);
  restartManager::restart(_eplMacroNonLocal);
  restartManager::restart(_lnfV);
  restartManager::restart(_lnfVtilde);
  restartManager::restart(_T);
  restartManager::restart(_dissipationActive);
  restartManager::restart(_thermalEnergy);
  restartManager::restart(_ThermoElasticCoupling);
  restartManager::restart(_DplasticEnergyDEplmatrix);
  restartManager::restart(_successFlag);
  restartManager::restart(_anisotropicMatrix);
  restartManager::restart(_DanisotropicMatrixDF);
  restartManager::restart(_DanisotropicMatrixDEp);
  restartManager::restart(_effectivePressure);
  restartManager::restart(_effectiveSVM);
  // Pointer to variables store somewhere else
  _currentOutwardNormal = NULL;
  _referenceOutwardNormal = NULL;
  _deformationGradient = NULL;
  return;
}





