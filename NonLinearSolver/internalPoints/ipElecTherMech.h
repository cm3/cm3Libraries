//
// Description: storing class for linear thermomechanical law it is a "virtual implementation" you have to define an ipvariable
//              in your project which derive from this one.
// Author:  <L. Homsy>, (C) 2013
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef IPELECTHERMECH_H_
#define IPELECTHERMECH_H_
#include "ipvariable.h"

#include "ipLinearElecTherMech.h"
#include "STensor3.h"

class IPElecTherMech : public IPLinearElecTherMech
{
 public:

public:
  IPElecTherMech();
  IPElecTherMech(const IPElecTherMech &source);
  IPElecTherMech& operator=(const IPVariable &source);
  virtual double defoEnergy() const;
  virtual IPVariable* clone() const {return new IPElecTherMech(*this);};
  virtual void restart(){IPLinearElecTherMech::restart();}
};

#endif // IPElecTherMech_H_
