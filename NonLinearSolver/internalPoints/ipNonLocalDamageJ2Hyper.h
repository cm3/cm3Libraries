//
// Description: storing class for j2 elasto-plastic law with non-local damag interface
// Author:  <L. Noels>, (C) 2013
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef IPNONLOCALDAMAGEJ2HYPER_H_
#define IPNONLOCALDAMAGEJ2HYPER_H_
#include "ipJ2linear.h"
#include "ipHardening.h"
#include "ipCLength.h"
#include "ipDamage.h"
#include "CLengthLaw.h"
#include "DamageLaw.h"


class IPLocalDamageJ2Hyper : public IPJ2linear
{
 protected:
  IPDamage*  ipvDam;
  double _damageEnergy;

 public:
  IPLocalDamageJ2Hyper();
  IPLocalDamageJ2Hyper(const J2IsotropicHardening *j2IH,  const DamageLaw *dl);
  virtual ~IPLocalDamageJ2Hyper()
  {
    if(ipvDam !=NULL)
    {
      delete ipvDam; ipvDam = NULL;
    }
  }


  IPLocalDamageJ2Hyper(const IPLocalDamageJ2Hyper &source);
  virtual IPLocalDamageJ2Hyper& operator=(const IPVariable &source);
  virtual void restart();
  
  virtual double damageEnergy()  const {return _damageEnergy;};
  virtual double& getRefToDamageEnergy() {return _damageEnergy;};

  virtual IPVariable* clone() const {return new IPLocalDamageJ2Hyper(*this);};

  virtual const IPDamage &getConstRefToIPDamage() const
  {
    if(ipvDam==NULL)
      Msg::Error("IPNonLocalDamageJ2Hyper: ipvDam not initialized");
    return *ipvDam;
  }
  virtual IPDamage &getRefToIPDamage()
  {
    if(ipvDam==NULL)
      Msg::Error("IPNonLocalDamageJ2Hyper: ipvDam not initialized");
    return *ipvDam;
  }

  virtual double getDamage() const
  {
    if(ipvDam==NULL)
      Msg::Error("IPNonLocalDamageJ2Hyper: ipvDam not initialized");
    return ipvDam->getDamage();
  }
  virtual double getMaximalP() const
  {
    if(ipvDam==NULL)
      Msg::Error("IPNonLocalDamageJ2Hyper: ipvDam not initialized");
    return ipvDam->getMaximalP();
  }
  virtual double getDeltaDamage() const
  {
    if(ipvDam==NULL)
      Msg::Error("IPNonLocalDamageJ2Hyper: ipvDam not initialized");
    return ipvDam->getDeltaDamage();
  }
  virtual double getDDamageDp() const
  {
    if(ipvDam==NULL)
      Msg::Error("IPNonLocalDamageJ2Hyper: ipvDam not initialized");
    return ipvDam->getDDamageDp();
  }
  virtual const STensor3 &getConstRefToDDamageDFe() const
  {
    if(ipvDam==NULL)
      Msg::Error("IPNonLocalDamageJ2Hyper: ipvDam not initialized");
    return ipvDam->getConstRefToDDamageDFe();
  }

};


class IPNonLocalDamageJ2Hyper : public IPLocalDamageJ2Hyper
{
 protected:
  IPCLength* ipvCL;

  bool nonLocalToLocal; // allows switching from non-local to local
	double _DirreversibleEnergyDNonLocalVariable;
// bareps
  double _nonlocalPlasticStrain;

 public:
  IPNonLocalDamageJ2Hyper();
  IPNonLocalDamageJ2Hyper(const J2IsotropicHardening *j2IH, const CLengthLaw *cll, const DamageLaw *dl);
  virtual ~IPNonLocalDamageJ2Hyper()
  {
    if(ipvCL != NULL)
    {
      delete ipvCL; ipvCL = NULL;
    }
  }


  IPNonLocalDamageJ2Hyper(const IPNonLocalDamageJ2Hyper &source);
  virtual IPNonLocalDamageJ2Hyper& operator=(const IPVariable &source);
  virtual void restart();

  virtual void setNonLocalToLocal(const bool fl){nonLocalToLocal = fl;};
  virtual bool getNonLocalToLocal() const {return nonLocalToLocal;};
	
	virtual const double& getDIrreversibleEnergyDNonLocalVariable() const {return _DirreversibleEnergyDNonLocalVariable;};
	virtual double& getRefToDIrreversibleEnergyDNonLocalVariable() {return _DirreversibleEnergyDNonLocalVariable;};


  virtual IPVariable* clone() const {return new IPNonLocalDamageJ2Hyper(*this);};
  virtual double getEffectivePlasticStrain() const { return _nonlocalPlasticStrain;}
  virtual double &getRefToEffectivePlasticStrain() { return _nonlocalPlasticStrain;}

 
  virtual const IPCLength &getConstRefToIPCLength() const
  {
    if(ipvCL==NULL)
      Msg::Error("IPNonLocalDamageJ2Hyper: ipvCL not initialized");
    return *ipvCL;
  }
  virtual IPCLength &getRefToIPCLength()
  {
    if(ipvCL==NULL)
      Msg::Error("IPNonLocalDamageJ2Hyper: ipvCL not initialized");
    return *ipvCL;
  }
  virtual const STensor3 &getConstRefToCharacteristicLength() const
  {
    if(ipvCL==NULL)
      Msg::Error("IPNonLocalDamageJ2Hyper: ipvCL not initialized");
    return ipvCL->getConstRefToCL();
  }
  virtual STensor3 &getRefToCharacteristicLength() const
  {
    if(ipvCL==NULL)
      Msg::Error("IPNonLocalDamageJ2Hyper: ipvCL not initialized");
    return ipvCL->getRefToCL();
  }

};

#endif // IPNONLOCALJ2HYPER_H_w
