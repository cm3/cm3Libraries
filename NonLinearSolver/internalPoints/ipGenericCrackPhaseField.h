//
// Created by mohib on 12/6/24.
//

#ifndef IPGENERICCRACKPHASEFIELD_H
#define IPGENERICCRACKPHASEFIELD_H



#include "ipField.h"
#include "ipNonLocalDamage.h"
#include "ipCLength.h"
#include "CLengthLaw.h"
class IPGenericCrackPhaseField : public IPVariableMechanics{
protected:
    IPVariableMechanics * _ipMeca;

    double localDamageVariable;
    double nonLocalDamageVariable;
    //double _lc;
    IPCLength* _ipvCL;
    STensor3 _PMax;
    double _effEner;
    double _defoEnergy;
    double _plasticEnergy;
    double _irreversibleEnergy;
    double _damageEnergy;
    
    STensor3 _DirreversibleEnergyDF;
    double _DirreversibleEnergyDNonLocalVariable;

public:
    IPGenericCrackPhaseField(const CLengthLaw* cLLaw = NULL);

public:
    ~IPGenericCrackPhaseField()
    {
        if(_ipMeca!=NULL)
        {
            delete _ipMeca;
            _ipMeca=NULL;
        }

        if(_ipvCL!=NULL)
        {
            delete _ipvCL;
        }
    }

    IPGenericCrackPhaseField(const IPGenericCrackPhaseField &source);
    virtual IPGenericCrackPhaseField& operator=(const IPVariable &source);
    virtual double defoEnergy() const{return _defoEnergy;};
    virtual double plasticEnergy() const{return _plasticEnergy;}
    virtual double damageEnergy() const{return  _damageEnergy;}
    
    // TODO: @MOHIB
    virtual double irreversibleEnergy() const{return  _irreversibleEnergy;}
    virtual double& getRefToIrreversibleEnergy();

    virtual const STensor3& getConstRefToDIrreversibleEnergyDF() const{return _DirreversibleEnergyDF;};
    virtual STensor3& getRefToDIrreversibleEnergyDF() {return _DirreversibleEnergyDF;};

    virtual const double& getDIrreversibleEnergyDNonLocalVariable() const {return _DirreversibleEnergyDNonLocalVariable;};
    virtual double& getRefToDIrreversibleEnergyDNonLocalVariable() {return _DirreversibleEnergyDNonLocalVariable;};
    
    double &getRefToLocalDamageVariable() {return localDamageVariable;};
    const double &getConstRefToLocalDamageVariable() const {return localDamageVariable;};
    void setlocalDamageVariable(const double d_loc){localDamageVariable = d_loc;};

    double &getRefToNonLocalDamageVariable() {return nonLocalDamageVariable;};
    const double &getConstRefToNonLocalDamageVariable() const {return nonLocalDamageVariable;};

    STensor3 &getRefToPMax() {return _PMax;};
    const STensor3 &getConstRefToPMax() const {return _PMax;};
    void setPMax(const STensor3& PMax){_PMax = PMax;};
    
    double &getRefToeffEner() {return  _effEner;};
    const double &getConstRefToeffEner() const {return _effEner;};
    void seteffEner(const double effEner){_effEner = effEner;};


    void setdefoEnergy(const double defoEnergy){_defoEnergy = defoEnergy;};
    void setplasticEnergy(const double plasticEnergy){_plasticEnergy = plasticEnergy;};
    void setdamageEnergy(const double damageEnergy){_damageEnergy = damageEnergy;};

    virtual const IPCLength &getConstRefToIPCLength() const
    {
        if(_ipvCL==NULL)
            Msg::Error("IPGenericCrackPhaseField: _ipvCL not initialized");
        return *_ipvCL;
    }

    virtual IPCLength &getRefToIPCLength()
    {
        if(_ipvCL==NULL)
            Msg::Error("IPGenericCrackPhaseField: _ipvCL not initialized");
        return *_ipvCL;
    }

    // double &getRefTolc() {return _lc;};
    // const double &getConstRefTolc() const {return _lc;};

    virtual const STensor3 &getConstRefToCharacteristicLength() const
    {
        if(_ipvCL==NULL)
            Msg::Error("IPGenericCrackPhaseField: _ipvCL not initialized");
        return _ipvCL->getConstRefToCL();
    }
    virtual STensor3 &getRefToCharacteristicLength() const
    {
        if(_ipvCL==NULL)
            Msg::Error("IPGenericCrackPhaseField: _ipvCL not initialized");
        return _ipvCL->getRefToCL();
    }

    

    virtual double get(const int comp) const;
    virtual double& getRef(const int comp);

    virtual void restart();
    virtual void setValueForBodyForce(const STensor43& K, const int ModuliType){ _ipMeca->setValueForBodyForce(K,ModuliType); }
    virtual const STensor43 &getConstRefToTangentModuli() const{return _ipMeca->getConstRefToTangentModuli();};
    virtual void computeBodyForce(const STensor33& G, SVector3& B) const { _ipMeca->computeBodyForce(G,B); }

    virtual IPVariable* clone() const {return new IPGenericCrackPhaseField(*this);};

    const IPVariableMechanics& getConstRefToIpMeca() const {return *_ipMeca;}
    IPVariableMechanics& getRefToIpMeca() {return *_ipMeca;}
    void setIpMeca(IPVariable& ipMeca)
    {
        if(_ipMeca!=NULL)
        {
            delete _ipMeca;
            _ipMeca=NULL;
        }
        _ipMeca= (dynamic_cast<IPVariableMechanics *>(ipMeca.clone()));
    }
    
};

#endif //IPGENERICCRACKPHASEFIELD_H
