//
// Description: storing class for kinematic hardening law
//
//
// Author:  <V.D. Nguyen>, (C) 2014
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef IPKINEMATICHARDENING_H_
#define IPKINEMATICHARDENING_H_

#include "ipHardening.h"

class IPKinematicHardening{
  protected:
    double R;
    double dR;   // dR/dg  where, g = gamma (eqv. plastic strain)
    double ddR;  // ddR/dgdg
    double dRdT; // temperature dependence
    double ddRdTT; // ddR/dTdT
    double ddRdT; // ddR/dgdT
    bool _isSaturated;
    

  public:
    IPKinematicHardening();
    IPKinematicHardening(const IPKinematicHardening &source);
    virtual IPKinematicHardening &operator=(const IPKinematicHardening &source);
    virtual ~IPKinematicHardening(){}
    virtual void restart();

    virtual IPKinematicHardening * clone() const {return new IPKinematicHardening(*this);};

    virtual bool& getRefToSaturationState() {return _isSaturated;};
    virtual bool isSaturated() const {return _isSaturated;};

    virtual double getR() const {return R;}
    virtual double getDR() const {return dR;}
    virtual double getDDR() const {return ddR;};
    virtual double getDRDT() const{ return dRdT;};
    virtual double getDDRDTT() const{ return ddRdTT;};
    virtual double getDDRDT() const{ return ddRdT;};
    virtual void set(const double &_r, const double &_dr, const double& _ddr, const double& _dRdT, const double& _ddRdTT, const double& _ddRdT)
    {
      R=_r;
      dR=_dr;
      ddR = _ddr;
      dRdT = _dRdT;
      ddRdTT = _ddRdTT;
      ddRdT = _ddRdT;
    }
};


#endif // IPKINEMATICHARDENING_H_
