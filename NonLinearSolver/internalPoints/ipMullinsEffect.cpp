//
// Description: storing class for Mullins Effect
//
//
// Author:  <Ujwal Kishore J.>, (C) 2023
//
// Copyright: See COPYING file that comes with this distribution
//
//

#include "ipMullinsEffect.h"

IPMullinsEffect::IPMullinsEffect(): eta(1.0), DetaDpsi(0.), DDetaDpsipsi(0.), DetaDT(0.), DDetaDTT(0.), psiMax(0.), 
                                    DpsiNew_DpsiMax(0.), DDpsiNew_DpsiMaxDpsi(0.), DDpsiNew_DpsiMaxDT(0.), DpsiMax_Dpsi(0.)
{
}

IPMullinsEffect::IPMullinsEffect(const IPMullinsEffect &source)
{
  eta      = source.eta;
  DetaDpsi     = source.DetaDpsi;
  DDetaDpsipsi    = source.DDetaDpsipsi;
  DetaDT   = source.DetaDT;
  DDetaDTT   = source.DDetaDTT;
  psiMax = source.psiMax;
  DpsiNew_DpsiMax = source.DpsiNew_DpsiMax;
  DDpsiNew_DpsiMaxDpsi = source.DDpsiNew_DpsiMaxDpsi;
  DDpsiNew_DpsiMaxDT = source.DDpsiNew_DpsiMaxDT;
  DpsiMax_Dpsi = source.DpsiMax_Dpsi;
}

IPMullinsEffect &IPMullinsEffect::operator=(const IPMullinsEffect &source)
{
  eta      = source.eta;
  DetaDpsi     = source.DetaDpsi;
  DDetaDpsipsi    = source.DDetaDpsipsi;
  DetaDT   = source.DetaDT;
  DDetaDTT   = source.DDetaDTT;
  psiMax = source.psiMax;
  DpsiNew_DpsiMax = source.DpsiNew_DpsiMax;
  DDpsiNew_DpsiMaxDpsi = source.DDpsiNew_DpsiMaxDpsi;
  DDpsiNew_DpsiMaxDT = source.DDpsiNew_DpsiMaxDT;
  DpsiMax_Dpsi = source.DpsiMax_Dpsi;
  return *this;
}

void IPMullinsEffect::restart()
{
  restartManager::restart(eta);
  restartManager::restart(DetaDpsi);
  restartManager::restart(DDetaDpsipsi);
  restartManager::restart(DetaDT);
  restartManager::restart(DDetaDTT);
  restartManager::restart(psiMax);
  restartManager::restart(DpsiNew_DpsiMax);
  restartManager::restart(DDpsiNew_DpsiMaxDpsi);
  restartManager::restart(DDpsiNew_DpsiMaxDT);
  restartManager::restart(DpsiMax_Dpsi);
  return;
}
