//
// C++ Interface: ipNucleationFunction
//
// Description: Class for ipNucleationFunction
//
//
// Author:  <J. Leclerc>, (C) 2019
//
// Copyright: See COPYING file that comes with this distribution

#include "ipNucleationFunction.h"



// IPNucleationFunction
// -------------------------------------------
IPNucleationFunction::IPNucleationFunction()
{
  // Nothing needs to be done here.
};


IPNucleationFunction::IPNucleationFunction( const IPNucleationFunction& source )
{
  // Nothing needs to be done here.
};

IPNucleationFunction& IPNucleationFunction::operator=( const IPNucleationFunction& source )
{
  // Nothing needs to be done here.
  return *this;
};


void IPNucleationFunction::restart()
{
  // Nothing needs to be done here.
  return;
}




// IPTrivialNucleationFunction
// -------------------------------------------
IPTrivialNucleationFunction::IPTrivialNucleationFunction() :
  IPNucleationFunction()
{
  // Nothing more needs to be done here.
};


IPTrivialNucleationFunction::IPTrivialNucleationFunction( const IPTrivialNucleationFunction& source ) :
  IPNucleationFunction(source)
{
  // Nothing more needs to be done here.
};


IPTrivialNucleationFunction& IPTrivialNucleationFunction::operator=( const IPNucleationFunction& source )
{
  /// Copy mother class.
  IPNucleationFunction::operator =( source );
  
  /// Copy this class
  //const IPTrivialNucleationFunction* source_casted = dynamic_cast< const IPTrivialNucleationFunction* >( &source );
  //if( source_casted != NULL )
  //{
    // Nothing more needs to be done here.
  //}
  return *this;
};


void IPTrivialNucleationFunction::restart()
{
  IPNucleationFunction::restart();
};






// IPNonTrivialNucleationFunction
// -------------------------------------------
IPNonTrivialNucleationFunction::IPNonTrivialNucleationFunction() :
  IPNucleationFunction(),
  nucleatedfV_(0.), deltafV_(0.), fV_rate_(0.)
{
  // Nothing more needs to be done here.
};


IPNonTrivialNucleationFunction::IPNonTrivialNucleationFunction( const IPNonTrivialNucleationFunction& source ) :
  IPNucleationFunction(source),
  nucleatedfV_(source.nucleatedfV_), deltafV_(source.deltafV_), fV_rate_(source.fV_rate_)
{
  // Nothing more needs to be done here.
};


IPNonTrivialNucleationFunction& IPNonTrivialNucleationFunction::operator=( const IPNucleationFunction& source )
{
  /// Copy mother class
  IPNucleationFunction::operator =( source );
  
  /// Copy this class
  const IPNonTrivialNucleationFunction* source_casted = dynamic_cast< const IPNonTrivialNucleationFunction* >( &source );
  if( source_casted != NULL )
  {
    nucleatedfV_  = source_casted->nucleatedfV_;
    deltafV_      = source_casted->deltafV_;
    fV_rate_      = source_casted->fV_rate_;
  }
  return *this;
};


void IPNonTrivialNucleationFunction::restart()
{
  IPNucleationFunction::restart();
  restartManager::restart( nucleatedfV_ );
  restartManager::restart( deltafV_ );
  restartManager::restart( fV_rate_ );
};




// IPLinearNucleationFunction
// -------------------------------------------
IPLinearNucleationFunction::IPLinearNucleationFunction() :
  IPNonTrivialNucleationFunction()
{
  // Nothing more needs to be done here.
};


IPLinearNucleationFunction::IPLinearNucleationFunction( const IPLinearNucleationFunction& source ) :
  IPNonTrivialNucleationFunction(source)
{
  // Nothing more needs to be done here.
};


IPLinearNucleationFunction& IPLinearNucleationFunction::operator=( const IPNucleationFunction& source )
{
  /// Copy mother class.
  IPNonTrivialNucleationFunction::operator =( source );
  
  /// Copy this class
  //const IPLinearNucleationFunction* source_casted = dynamic_cast< const IPLinearNucleationFunction* >( &source );
  //if( source_casted != NULL )
  //{
  // Nothing needs to be done here
  //}
  return *this;
};


void IPLinearNucleationFunction::restart()
{
  IPNonTrivialNucleationFunction::restart();
  return;
};




// IPGaussianNucleationFunction
// -------------------------------------------
IPGaussianNucleationFunction::IPGaussianNucleationFunction( ) :
  IPNonTrivialNucleationFunction(),
  meanNucleationStrain_(0.0), fvtot_(0.0)
{
  Msg::Error("IPGaussianNucleationFunction:: Default constructor shloud never be called");
};


IPGaussianNucleationFunction::IPGaussianNucleationFunction(  const double meanNucleationStrain, const double fvtot ) :
  IPNonTrivialNucleationFunction(),
  meanNucleationStrain_(meanNucleationStrain), fvtot_(fvtot)
{
  // Nothing more needs to be done here.
};


IPGaussianNucleationFunction::IPGaussianNucleationFunction( const IPGaussianNucleationFunction& source ) :
  IPNonTrivialNucleationFunction(source),
  meanNucleationStrain_(source.meanNucleationStrain_), fvtot_(source.fvtot_)
{
  // Nothing more needs to be done here.
};


IPGaussianNucleationFunction& IPGaussianNucleationFunction::operator=( const IPNucleationFunction& source )
{
  /// Copy mother class.
  IPNonTrivialNucleationFunction::operator =( source );
  
  /// Copy this class.
  const IPGaussianNucleationFunction* source_casted = dynamic_cast< const IPGaussianNucleationFunction* >( &source );
  if( source_casted != NULL )
  {
    meanNucleationStrain_ = source_casted->meanNucleationStrain_;
    fvtot_ = source_casted->fvtot_;
  }
  return *this;
};


void IPGaussianNucleationFunction::restart()
{
  IPNonTrivialNucleationFunction::restart();
  restartManager::restart( meanNucleationStrain_ );
  restartManager::restart( fvtot_ );
  return;
};







