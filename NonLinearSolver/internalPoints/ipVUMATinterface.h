//
// Description: storing class for VUMAT interface law it is a "virtual implementation" you have to define an ipvariable
//              in your project which derive from this one. (all data in this ipvariable have name beginning by _vumat...)
//              so don't do the same in your project...
// Author:  <Antoine JERUSALEM>, (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef IPVUMATINTERFACE_H_
#define IPVUMATINTERFACE_H_
#include "ipvariable.h"
#include "STensor3.h"
class IPVUMATinterface : public IPVariableMechanics
{
 public: // All data public to avoid the creation of function to access
  int _vumatNbStatev;     // number of internal variables
  double _elementSize;    // element size before deformation
  double* _vumatStatev;   // internal variables for vumat
  double _internalEnergy; // internal energy for vumat
  double _inelasticEnergy;// inelastic energy for vumat
 public:
  IPVUMATinterface(int _nsdv, double _size);
  IPVUMATinterface(const IPVUMATinterface &source);
  IPVUMATinterface& operator=(const IPVariable &source);
  virtual double defoEnergy() const;
  virtual double plasticEnergy() const;
  virtual ~IPVUMATinterface() { free(_vumatStatev); }
  virtual double getNsdv() const {return _vumatNbStatev;}
  virtual double get(const int i) const;
  virtual void restart();
  virtual IPVariable* clone() const {return new IPVUMATinterface(*this);};
};

#endif // IPVUMATINTERFACE_H_
