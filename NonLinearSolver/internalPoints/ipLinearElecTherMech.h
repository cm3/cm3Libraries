//
// Description: storing class for linear thermomechanical law it is a "virtual implementation" you have to define an ipvariable
//              in your project which derive from this one.
// Author:  <L. Homsy>, (C) 2013
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef IPLINEARELECTHERMECH_H_
#define IPLINEARELECTHERMECH_H_
#include "ipvariable.h"

#include "ipLinearThermoMechanics.h"
#include "STensor3.h"

class IPLinearElecTherMech : public  IPLinearThermoMechanics
{
 public:

public:
  IPLinearElecTherMech();
  IPLinearElecTherMech(const IPLinearElecTherMech &source);
  IPLinearElecTherMech& operator=(const IPVariable &source);
  virtual double defoEnergy() const;
  virtual IPVariable* clone() const {return new IPLinearElecTherMech(*this);};
  //virtual void restart(){return IPLinearThermoMechanics::restart();}
  virtual void restart();
  virtual double get(const int comp) const;
  virtual double &getRef(const int comp);
};

#endif // IPLinearElecTherMech_H_
