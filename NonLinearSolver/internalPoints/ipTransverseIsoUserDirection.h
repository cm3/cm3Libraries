//
// Description: 
// Author:  <V.-D. Nguyen>, (C) 2021
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef IPTRANSVERSEISOUSERDIRECTION_H_
#define IPTRANSVERSEISOUSERDIRECTION_H_

#include "ipTransverseIsotropic.h"

class IPTransverseIsoUserDirection : public IPTransverseIsotropic
{
 protected:
  SVector3 _A;
 public:
  IPTransverseIsoUserDirection(const SVector3 &dir);
  IPTransverseIsoUserDirection(const IPTransverseIsoUserDirection &source);
  virtual IPTransverseIsoUserDirection& operator=(const IPVariable &source);
  virtual void getTransverseDirection(SVector3 &A) const { A =_A ;};
  virtual void restart();
  virtual IPVariable* clone() const {return new IPTransverseIsoUserDirection(*this);};
 
 private:
  IPTransverseIsoUserDirection();
};


#endif //IPTRANSVERSEISOUSERDIRECTION_H_