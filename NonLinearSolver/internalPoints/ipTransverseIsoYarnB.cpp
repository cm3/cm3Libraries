//
// Description: storing class for j2 linear elasto-plastic law
//
//
// Author:  <Gauthier BECKER>, (C) 2011
// Modified: <Bin YANG, Xing LIU, Ling WU>
// Copyright: See COPYING file that comes with this distribution
//
//
#include "ipTransverseIsoYarnB.h"
#include "restartManager.h"

//-----------------Define some useful function for computation---------------
bool NodeAscending_x(const Node &N1, const Node &N2) { return N1.get_x() < N2.get_x(); };
bool NodeAscending_y(const Node &N1, const Node &N2) { return N1.get_y() < N2.get_y(); };
bool NodeAscending_z(const Node &N1, const Node &N2) { return N1.get_z() < N2.get_z(); };

//TODO: Compute the orthogonal vector of the input vector
//Remarks: The algorithm is simply explained in the following
//         Suppose V1=(a,b,c) is the input vector, and the absolute 
//         value is the largest, then the orghogonalVector is
//         OutV = (-(b+c)/a, 1, 1).  
void OrthogonalVector(SVector3 &V1, SVector3 &OutV)
{
	double v1[3] = {abs(V1[0]),abs(V1[1]),abs(V1[2])};
	int v1_max_pos = maxArray_pos(v1);

	OutV[0]=1.; 
	OutV[1]=1.; 
	OutV[2]=1.; 

	double sum = 0;

	for (int i = 0; i < 3; i++)
	{
		if (i != v1_max_pos)
		{
			sum += V1[i];
		}
	}
	sum = -1.0*sum;
        OutV[v1_max_pos]=sum/V1[v1_max_pos];
        double norm = OutV.normalize();
}

//TODO: Compute the orthogonal vector of the plan spaned by the first two input 
//      vectors, V1 and V2
//Remarks: The classical cross product formula is used here.
void OrthogonalVector(SVector3& V1, SVector3& V2, SVector3& OutV)
{
        OutV[0]=V1[1]*V2[2]-V2[1]*V1[2];
        OutV[1]=-V1[0]*V2[2]+V2[0]*V1[2];
        OutV[2]=V1[0]*V2[1]-V2[0]*V1[1];
        double norm = OutV.normalize();
}


//TODO:Compute the position of the node whose coordinate is closest to the 
//     input coordinate v.
//Input: 
//     vector<Node> NodeSet: Sorted node type vector
//     int stretch_com: Integer indicating the stretching direction
//     double v: the coordinate component
int FirstApproxNode(std::vector<Node> NodeSet, int stretch_com, double v)
{
	int Node_Vec_length = NodeSet.size();
	int Node_Left = 0;
	int Node_Right = Node_Vec_length-1;
	int Node_Middle = floor(0.5*(Node_Left + Node_Right));
	double Point_Distance_record = 10000;

	for (int Account = 0; Account < 10; Account++)
	{
		if (NodeSet[Node_Middle].get_xyz(stretch_com) < v)
		{
			Node_Left = Node_Middle;
			Node_Middle = floor(0.5*(Node_Left + Node_Right));
		}
		else
		{
			Node_Right = Node_Middle;
			Node_Middle = floor(0.5*(Node_Left + Node_Right));
		}

		if (Point_Distance_record > NodeSet[Node_Right].get_xyz(stretch_com) - NodeSet[Node_Left].get_xyz(stretch_com))
		{
			Point_Distance_record = NodeSet[Node_Right].get_xyz(stretch_com) - NodeSet[Node_Left].get_xyz(stretch_com);
		}
		else
		{
			return Node_Middle; //In the case v is between two nodes
		}
	}
	return Node_Middle; //In the case the distance between v and the middle node is less than 2^-10*Yarn's length;
}

//TODO: Locate the index of the max value in a one dimensional array.
//Remark*:Here we assume Array contains three elements. We can improve this function later.
int maxArray_pos(double Array[])
{
	int max_index = 0;
	double max_value = Array[0];
	for (int i = 0; i < 3; i++)
	{
		if (max_value < Array[i])
		{
			max_value = Array[i];
			max_index = i;
		}
	}

	return max_index;
}

//TODO: Class Direction is used to describe the stretch direction of Yarn
Yarn::Yarn(NodesSet &NST,
	       const int SegNum	            
	       )
{
	std::cout << "YarnDirection constructor (Complete) is called!" << endl;
	_physics = NST.get_physic();
	_SegNum = SegNum;
	vector<Node> NodeSet = NST.get_nodesset();
    //+++++++++++++Compute the stretch direction+++++++++++++++++++++++
    vector<vector<double> > max_min_xyz;
    max_min_xyz = NST.get_max_min_xyz();

    vector<double> distance(3);
    distance[0] = max_min_xyz[0][1] - max_min_xyz[0][0];
    distance[1] = max_min_xyz[1][1] - max_min_xyz[1][0];
    distance[2] = max_min_xyz[2][1] - max_min_xyz[1][0];
	
	int StretchAxis;
    StretchAxis = distance[0] > distance[1]?0:1;
    StretchAxis = distance[StretchAxis] > distance[2]?StretchAxis:2;
	_StretchAxis = StretchAxis;

	_PlanOuterNormal = vector<double>(3);
	_PlanOuterNormal[0] = 0;
	_PlanOuterNormal[1] = 0;
	_PlanOuterNormal[2] = 0;
	//************Determine the length on stretch direction************
	switch (StretchAxis)
	{
	    case 0:
		{
	      //Sort the node along the stretch direction
		  std::sort(NodeSet.begin(), NodeSet.end(), NodeAscending_x);		  
		  break;
		}
		case 1:
		{
		  std::sort(NodeSet.begin(), NodeSet.end(), NodeAscending_y);
		  break;
		}
		case 2:
		{
		  std::sort(NodeSet.begin(), NodeSet.end(), NodeAscending_z);
		  break;
		}
	}

	std::cout << "Stretch direction is: " << StretchAxis << endl;
	//********************************************************************

	//******************Compute central point of each segment*************
	vector< vector<double> > Points(3);
	vector<double> tempCoor(3);
    tempCoor[0] = 0; tempCoor[1] = 0; tempCoor[2] = 0;

	vector<double> t;
	double var_start = NodeSet[0].get_xyz(StretchAxis);
	double var_end = NodeSet[NodeSet.size()-1].get_xyz(StretchAxis);

	int account = 0;
	double p_current = 0;
	double p_previous = 0;
	//+++++++++++++Generate the first point+++++++++++++++++++++++++++++++++++++++++++
	for (int i = 0; i < NodeSet.size(); i++)
	{
		if (abs(max_min_xyz[StretchAxis][0]-NodeSet[i].get_xyz(StretchAxis))>1.0e-6)
		{
			break;
		}
		else
		{
			account++;
			tempCoor[0] += NodeSet[i].get_x();
			tempCoor[1] += NodeSet[i].get_y();
			tempCoor[2] += NodeSet[i].get_z();
		}
	}
	tempCoor[0] /= account;
	tempCoor[1] /= account;
	tempCoor[2] /= account;
	Points[0].push_back(tempCoor[0]);
	Points[1].push_back(tempCoor[1]);
	Points[2].push_back(tempCoor[2]);
	t.push_back(tempCoor[StretchAxis]);

	account = 0;
	tempCoor[0] = 0; tempCoor[1] = 0; tempCoor[2] = 0;
	//+++++++++++++Generate the internal points+++++++++++++++++++++++++++++++++++++++
	for (int i = 0; i < NodeSet.size(); i++)
	{
		p_current = (NodeSet[i].get_xyz(StretchAxis)- var_start)/(var_end- var_start);
		if ((p_current - p_previous >= 1.0/SegNum) || i == NodeSet.size()-1)
		{
			tempCoor[0] /= account;
			tempCoor[1] /= account;
			tempCoor[2] /= account;

			Points[0].push_back(tempCoor[0]);
			Points[1].push_back(tempCoor[1]);
			Points[2].push_back(tempCoor[2]);

			t.push_back(tempCoor[StretchAxis]);

			tempCoor[0] = NodeSet[i].get_x();
			tempCoor[1] = NodeSet[i].get_y();
			tempCoor[2] = NodeSet[i].get_z();

			account = 1;
			p_previous = p_current;
		}
		else
		{
			tempCoor[0] += NodeSet[i].get_x();
			tempCoor[1] += NodeSet[i].get_y();
			tempCoor[2] += NodeSet[i].get_z();

			account++;
		}
	}

	account = 0;
	tempCoor[0] = 0; tempCoor[1] = 0; tempCoor[2] = 0;
	//+++++++++++++++++++++Generate the last point++++++++++++++++++++++++++++++++
	for (int i = NodeSet.size()-1; i > 0 ; i--)
	{
		if (abs(max_min_xyz[StretchAxis][1] - NodeSet[i].get_xyz(StretchAxis))>1.0e-6)
		{
			break;
		}
		else
		{
			account++;
			tempCoor[0] += NodeSet[i].get_x();
			tempCoor[1] += NodeSet[i].get_y();
			tempCoor[2] += NodeSet[i].get_z();
		}
	}
	tempCoor[0] /= account;
	tempCoor[1] /= account;
	tempCoor[2] /= account;
	Points[0].push_back(tempCoor[0]);
	Points[1].push_back(tempCoor[1]);
	Points[2].push_back(tempCoor[2]);
	t.push_back(tempCoor[StretchAxis]);

	_s.fulfill(Points,t);
	std::cout << "Yarn of physic num "<< _physics << " has been built." << endl << endl;
}


Yarn::Yarn(string yarninfoPath,
	       int PhysicalId,
	       int SegNum 
           )
{
	ifstream infile;
	infile.open(yarninfoPath.c_str());
	if (!infile)
	{
		std::cout << "Yarn's information file cannot be opened correctly." << endl;
		exit(0);
	}

	std::cout << "YarnDirection constructor (Complete) is called!" << endl;
	_PlanOuterNormal = vector<double>(3);
	//---------Read Nodes information and build node objects----------
	string str;
	int YarnId = 0;
	vector< vector<double> > Points(3);//Coordinates of the central line
	vector<double> t;//Free variable
	vector<double> Coor(3);

	while (!infile.eof())
	{
		std::getline(infile, str);
		string temp = str.c_str();
		//Remark: In windows, the key "Enter" is not visible in the string
		//        In Linux, the key "Enter" is recorded by '\r'as a part of the string.
		if (str.find("Yarn") != -1) //When str=="$Nodes", strcmp returns 0;
		{
			std::getline(infile, str);
			if (str.find("Physic num") != -1)
			{
				std::getline(infile, str);
				istringstream in1(str);
				in1 >> YarnId; //Read yarn's id
			}
			
			if (YarnId != PhysicalId)
			{
				continue;
			}
			else
			{
				std::getline(infile, str);
				if (str.find("Plane out normal") != -1)
				{
					std::getline(infile, str);
					istringstream in3(str);
					in3 >> _PlanOuterNormal[0] >> _PlanOuterNormal[1] >> _PlanOuterNormal[2];
				}
				std::getline(infile, str);
				if (str.find("Central line") != -1)
				{
					std::getline(infile, str);
					while (!str.empty() && strcmp(str.c_str(),"\r"))
					{
						istringstream in3(str);
						in3 >> Coor[0] >> Coor[1] >> Coor[2];
						Points[0].push_back(Coor[0]);
						Points[1].push_back(Coor[1]);
						Points[2].push_back(Coor[2]);
						if (infile.eof())
						{
						  break;
						}
						std::getline(infile, str);
					}
					break;
				}
			}
		}
	}

	if (Points.size() != 0)
	{
		_physics = PhysicalId;
		_SegNum = SegNum;

		vector<vector<double> > max_min_xyz(3,vector<double>(2));
		for (int i = 0; i < Points[0].size(); i++)
		{
			max_min_xyz[0][0] = max_min_xyz[0][0] < Points[0][i] ? max_min_xyz[0][0] : Points[0][i];
			max_min_xyz[1][0] = max_min_xyz[1][0] < Points[1][i] ? max_min_xyz[1][0] : Points[1][i];
			max_min_xyz[2][0] = max_min_xyz[2][0] < Points[2][i] ? max_min_xyz[2][0] : Points[2][i];

			max_min_xyz[0][1] = max_min_xyz[0][1] > Points[0][i] ? max_min_xyz[0][1] : Points[0][i];
			max_min_xyz[1][1] = max_min_xyz[1][1] > Points[1][i] ? max_min_xyz[1][1] : Points[1][i];
			max_min_xyz[2][1] = max_min_xyz[2][1] > Points[2][i] ? max_min_xyz[2][1] : Points[2][i];
		}

		vector<double> distance(3);
		distance[0] = max_min_xyz[0][1] - max_min_xyz[0][0];
		distance[1] = max_min_xyz[1][1] - max_min_xyz[1][0];
		distance[2] = max_min_xyz[2][1] - max_min_xyz[2][0];

		int StretchAxis;
		StretchAxis = distance[0] > distance[1] ? 0 : 1;
		StretchAxis = distance[StretchAxis] > distance[2] ? StretchAxis : 2;
		_StretchAxis = StretchAxis;

		for (int i = 0; i < Points[0].size(); i++)
		{
			t.push_back(Points[StretchAxis][i]);
		}	
		_s.fulfill(Points, t);
	}
	std::cout << "Yarn of physic num " << _physics << " has been built." << endl << endl;
}

vector<double> Yarn::get_dir(vector<double> x)
{
	double t = x[_StretchAxis];
	vector<double> dir;
	dir = _s.get_dir(t);
	return dir;
}

void Yarn::InfoPrint(string filename)
{
	//ofstream out(filename, fstream::app | fstream::out);
	//out << "Yarn_" << _physics << endl;
	//_s.dataprint(filename,_s.get_var());
}

//---------------------------------------------------------------------------
//TODO: Initialize the the member variables _nodes_vec and _elements_vec
//Input: 
//      string filePath: The path of the input mesh file
//Remarks:+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//        _nodes_vec: The vector container of nodes.
//        _elements_vec: The vector container of elements.
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Mesher::Mesher(std::string filePath)
{
   ifstream infile;
   infile.open(filePath.c_str());
   if(!infile)
   {
	cout<<"MSH file could not be open correctly."<<endl;
	exit(0);
   }

   cout<<"Mesher constructor has been called."<<endl;
   //---------Read Nodes information and build node objects----------
   string str;
   int N_Nodes = 0;
   int Node_id = 0;
   int Node_id_max = -100;

   double Node_x = 0;
   double Node_y = 0;
   double Node_z = 0;
   while(!infile.eof())
   {
	   getline(infile,str);
           string temp = str.c_str();
           //Remark: In windows, the key "Enter" is not visible in the string
           //        In Linux, the key "Enter" is recorded by '\r'as a part of the string.
	   if(!strcmp(str.c_str(),"$Nodes")||!strcmp(str.c_str(),"$Nodes\r")) //When str=="$Nodes", strcmp returns 0;
	   {
		 getline(infile,str);
		 istringstream in1(str);
		 in1 >> N_Nodes; //Read total number of nodes
		 cout << "$Nodes" << endl;
		 cout << N_Nodes << endl;

		 for(int i = 0; i < N_Nodes; i++)
		 {
		  getline(infile, str);
		  istringstream in4(str);
		  in4 >> Node_id >> Node_x >> Node_y >> Node_z;

		  Node n(Node_id, Node_x, Node_y, Node_z);
		  _nodes_vec.push_back(n);	

		  if (Node_id > Node_id_max)
		  {
			  Node_id_max = Node_id;
		  }

		 }//Read node's information
		 cout << "$EndNodes!" << endl;
		 break;
       }                                        
   }
   cout << endl;
   //--------Build the Node's id_pos vector-----------------------------
   _nodes_id_pos = vector<int>(Node_id_max+1);
   for (int i = 0; i < _nodes_vec.size(); i++)
   {
	   _nodes_id_pos[_nodes_vec[i].get_tag()] = i;
   }

   //---------Read Elements information and build element objects--------
   int N_Elements = 0;
   int Element_Num = 0;
   int temp_num = 0;
   
   int Element_type = 0;
   int Element_physical = 0;

   int Element_Account = 1;
 
   while(!infile.eof())
   {
	   getline(infile,str);
	   if(!strcmp(str.c_str(),"$Elements")||!strcmp(str.c_str(),"$Elements\r")) //When str=="$Nodes", strcmp returns 0;
	   {
		 getline(infile,str);
		 istringstream in1(str);
		 in1 >> N_Elements; //Read total number of nodes
			
		 cout << "$Elemtns" << endl;
		 cout << N_Elements << endl;
		 for(int i = 0; i < N_Elements; i++)
		 {
		   getline(infile, str);
		   istringstream in_unknown(str); //We cannot expect the number of nodes
		   int j = 0;
		   std::vector<int> Ele_nodes_vec;
		  
		   while(in_unknown >> temp_num)
		   {
		      if(j==0){Element_Num = temp_num;}
                      if(j==1){Element_type = temp_num;}
                      if(j==3){Element_physical = temp_num;}
                      if(j>4){Ele_nodes_vec.push_back(temp_num);}
                      j++;			
		   }

		   if (Element_Account == 1)
		   {
			  _physics_ref.push_back(Element_physical);
		   }
		   else
		   {
			  bool Physicbingo = false;
			  for (int check_id = 0; check_id < _physics_ref.size(); check_id++)
			  {
				  if (_physics_ref[check_id] == Element_physical)
				  {
					  Physicbingo = true;
					  break;
				  }
			  }
			  if (!Physicbingo)
			  {
				 _physics_ref.push_back(Element_physical);
			  }
		    }

		 Element E(Element_type, Element_physical, Element_Num, Ele_nodes_vec);
	         _elements_vec.push_back(E); 

		 Element_Account++;

		 }//Read element's information
		 cout << "$EndElements" << endl;
		 break;
       }                                        
   }

  //+++++++++++++Build the element id physic num table++++++++++
   _ele_id_physical_tab = vector<int>(Element_Account+1);

   for(int i = 0; i<_elements_vec.size();i++)
   {
     _ele_id_physical_tab[_elements_vec[i].get_eNum()] = _elements_vec[i].get_physical();
   }

   //++++++++++++Compute the total physical number+++++++++++++
   _physics_num = _physics_ref.size();
   cout << "FEM Elements have been created!" << endl << endl;
}

//TODO: Print the mesh data to the file designated by Outputfile
void Mesher::MesherPrint(ofstream *Outputfile)
{
	//------------------Print Nodes-----------------------------
	*Outputfile <<"$Nodes"<<endl;
	*Outputfile << _nodes_vec.size() << endl;

	for (int i = 0; i < _nodes_vec.size(); i++)
	{
		*Outputfile << _nodes_vec[i].get_tag() << " " << _nodes_vec[i].get_x()<<" ";
		*Outputfile << _nodes_vec[i].get_y() << " " << _nodes_vec[i].get_z() << endl;
	}
	*Outputfile << "$EndNodes" << endl << endl;

	//------------------Print Elements-----------------------------
	*Outputfile << "$Elements" << endl;
	*Outputfile << _elements_vec.size() << endl;

	for (int i = 0; i < _elements_vec.size(); i++)
	{
		*Outputfile << _elements_vec[i].get_eNum() << " " << _elements_vec[i].get_type() << " ";
		*Outputfile << _elements_vec[i].get_physical() << " ";
		for (int j = 0; j < _elements_vec[i].get_nodes_id().size(); j++)
		{
			*Outputfile << _elements_vec[i].get_nodes_id()[j] << " ";
		}
		*Outputfile << endl;
	}
	*Outputfile << "$EndElements" << endl << endl;
}

Element Mesher::get_element(int ele_position)
{ 
 Element E(_elements_vec[ele_position]);
 return E;
}
//TODO: Get the node indexed by nod_reference
//Remark:++++++++++++++++++++++++++++++++++++++++++++++++++++++
//     In the element, the nodes are refered by their reference
//     number. 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//Input:
//     int node_reference: the reference index of the node
Node &Mesher::get_node(int node_reference)
{
   return _nodes_vec[_nodes_id_pos[node_reference]];
}

//TODO: Gathering all the nodes with the same physic tag.
//Input: 
//      vector<Node> &NodeSet: The vector container of the output nodes
//      int physics: The physic number by which the nodes are gathered
//      Mesher &M: The Mesher object which contains mesh nodes and mesh elements
//Output: vector<Node> &NodeSet
//TODO: Gathering all the nodes with the same physic tag.
//Input: 
//      vector<Node> &NodeSet: The vector container of the output nodes
//      int physics: The physic number by which the nodes are gathered
//      Mesher &M: The Mesher object which contains mesh nodes and mesh elements
//Output: vector<Node> &NodeSet
std::vector<Node> Mesher::NodesGathering(int physics)
{
	std::vector<Node> NodeSet;
        cout << "Physic " << physics << " nodes is gathering!" << endl;
	//-----------Gathering Nodes Index---------------------
	std::vector<int> Node_Index_Set;
	int Current_node_reference;

        //Loop for all elements
	for (int i = 0; i < get_ele_size(); i++)
	{
                //Locate the element with the input physics
		if (_elements_vec[i].get_physical() == physics)
		{
                        //Loop for nodes of the selected element
			for (int j = 0; j < _elements_vec[i].get_nodes_id().size(); j++)
			{
			  Current_node_reference = _elements_vec[i].get_nodes_id()[j];
                          
                          if(Node_Index_Set.size()==0)
                          {Node_Index_Set.push_back(Current_node_reference);}

                           
                          bool isInserted = true;
                          for(int k = 0; k < Node_Index_Set.size(); k++)
                          {
                             if(Node_Index_Set[k] == Current_node_reference)
                             {isInserted = false; break;}
                          }

                          if(isInserted)
                          {
                            Node_Index_Set.push_back(Current_node_reference);
                            NodeSet.push_back(get_node(Current_node_reference));
                          }
			}
		}

		if (i%10000 == 0)
		{
	          cout << i << " elements have been treated!" << endl;
		}
	}
        cout << "Physic " << physics << " nodes have been gathered!" << endl;
        return NodeSet;

}

std::vector<NodesSet> Mesher::NodesGathering()
{
        std::vector<NodesSet> NodesSets;
	//-----------Initialize NodesSets----------------------
	int physicmax = -100;
	for (int i = 0; i <_physics_ref.size(); i++)
	{
		//Compute the maximal physic number to build the physical
		//number index table for accelerating searching speed.
		if (_physics_ref[i] > physicmax)
		{
			physicmax = _physics_ref[i];
		}
		NodesSet nset(_physics_ref[i]);
		NodesSets.push_back(nset);
	}

	std::vector<int> physics_ref_pos(physicmax+1);
	for (int i = 0; i < _physics_ref.size(); i++)
	{
		physics_ref_pos[_physics_ref[i]] = i;
	}

	//-----------Gathering Nodes Index---------------------
	for (int i = 0; i < _elements_vec.size(); i++)
	{
		//Physic number of the element.
		int Ele_physic_num = _elements_vec[i].get_physical(); 

		//NodesIndices of the element.
		vector<int> ElenodesIndex = _elements_vec[i].get_nodes_id();

	        //Only volume element are taken into account.	
		if (ElenodesIndex.size() < 8)
		{
			continue;
		}
 
		int NodesSetIndex = physics_ref_pos[Ele_physic_num];
		for (int k = 0; k < ElenodesIndex.size(); k++)
		{
			Node &NTemp = get_node(ElenodesIndex[k]);
			if (!NTemp.get_is_visited())
			{
                           vector<vector<double> > max_min_xyz;
                           max_min_xyz = NodesSets[NodesSetIndex].get_max_min_xyz();
                           max_min_xyz[0][0] = max_min_xyz[0][0] > NTemp.get_x()? NTemp.get_x():max_min_xyz[0][0];
                           max_min_xyz[0][1] = max_min_xyz[0][1] < NTemp.get_x()? NTemp.get_x():max_min_xyz[0][1];

                           max_min_xyz[1][0] = max_min_xyz[1][0] > NTemp.get_y()? NTemp.get_y():max_min_xyz[1][0];
                           max_min_xyz[1][1] = max_min_xyz[1][1] < NTemp.get_y()? NTemp.get_y():max_min_xyz[1][1];

                           max_min_xyz[2][0] = max_min_xyz[2][0] > NTemp.get_z()? NTemp.get_z():max_min_xyz[2][0];
                           max_min_xyz[2][1] = max_min_xyz[2][1] < NTemp.get_z()? NTemp.get_z():max_min_xyz[2][1];

			   NodesSets[NodesSetIndex].NodePushBack(NTemp);
                           NodesSets[NodesSetIndex].set_max_min_xyz(max_min_xyz);
			   NTemp.set_is_visited(true);
			}
		}
		
		if (i % 10000 == 0)
		{
			cout << i << " elements have been treated!" << endl;
		}
	}
    cout << "All nodes have been gathered! " << endl;
    return NodesSets;
}

//*****************Linear Equation*******************************
//***************************************************************
LinearEqs::LinearEqs(vector<vector<double> >& M, vector<double>& V)
{
  vector<int> dim_M;
  dim_M.push_back(M.size());
  dim_M.push_back(M[0].size());
  int dim_v = V.size();
  
  if(!(dim_M[0]==dim_M[1])||!(dim_M[0]==V.size()))
  {
	  cout << "LHS dim does not match RHS!" << endl;
  }
  _M = M;
  _V = V;
}

//TODO: Use Gaussian Elimination method to solve the 
//      linear equation
void LinearEqs::solve(vector<double>& s)
{
	for (int i = 0; i < _M.size(); i++)
	{
		for (int j = i + 1; j<_M.size(); j++)
		{
			double r = _M[j][i] / _M[i][i];
			for (int k = i; k < _M[0].size(); k++)
			{
				_M[j][k] -= _M[i][k] * r;
			}
			_V[j] -= _V[i] * r;
		}
	}

	vector<double> ans(_V.size());
	int VecSize = _V.size();
	vector<int> MSize;
	MSize.push_back(_M.size());
	MSize.push_back(_M[0].size());

	ans[VecSize - 1] = _V[VecSize - 1] / _M[MSize[0] - 1][MSize[1] - 1];

	for (int i = 1; i < VecSize; i++)
	{
		double r = 0;
		for (int j = 0; j < i; j++)
		{
			r += ans[VecSize - j - 1] * _M[VecSize - i - 1][VecSize - j - 1];
		}
		double a = _V[VecSize - i - 1];
		double b = _M[VecSize - i - 1][VecSize - i - 1];
		ans[VecSize - i - 1] = (_V[VecSize - i - 1] - r) / _M[VecSize - i - 1][VecSize - i - 1];
	}

	s = ans;
}

//*****************************Spline****************************
//***************************************************************
//********************Back ground*****************************
//Let interval is partitioned into sub intervals [xi-1,xi], for
//any x in [x(i-1),x(i)], the cubic spline s(x) is expressed by
//s(x)=M(i-1)*(x(i)-x)/h(i)+M(i)*(x-x(i-1))/h(i)
//with h(i) = x(i)-x(i-1).
//
//Let lam(i)=h(i)/(h(i)+h(i+1)), nu(i)=h(i+1)/(h(i)+h(i+1)),
//f[x(i),x(j)]=(f(x(i))-f(x(j)))/(x(i)-x(j))
//f[x(i),x(j),x(k)]=(f(x(i),x(j))-f((x(j),x(k)))/(x(i)-x(k)),
//then coefficient M is determined by
//2     1                                (f[x0,x1]-m0)/h(1)
//lam1  2     nu1                        6*f[x0,x1,x2]
//     lam2   2     nu2                  6*f[x1,x2,x3]
//            lam3   2    nu3             .
//                .    .     .    =       .
//                  .    .     .
//                  lamn-1 2 nun-1       6*f[x(n-2),x(n-1),x(n)]
//                         1   2         (mn-f[x(n-1),x(n)])/h(n)
//where m0 and mn are first order derivatives at the ends.
Spline::Spline(vector<double> v_ind, const vector<double> v_f)
{
  //=============Initialize coordinates=============
  int dim = v_ind.size();
  _coor = v_ind;
  _f = v_f; 
  //=============Compute coefficient M=============
  //-------Step 1: Compute first order difference 
  //               and second order difference
  vector<double> FirstDiff(dim-1); 
  vector<double> h1(dim-1);
  vector<double> SecondDiff(dim-2);
  vector<double> h2(dim-2);
  
  for(int i = 0; i < dim-1; i++)
  {
    h1[i] = _coor[i+1] - _coor[i];
    FirstDiff[i] = (v_f[i+1] - v_f[i])/h1[i];
    
    if(i+2 < dim)
    {
      h2[i] = _coor[i+2]-_coor[i];
    }
  }

  for(int i = 0; i < FirstDiff.size()-1; i++)
  {
   SecondDiff[i] = (FirstDiff[i+1]-FirstDiff[i])/h2[i];
  }

  //-------Step 2: Compute LHS and RHS of equation M---
  vector<double> lambda(dim-1);
  vector<double> nu(dim-1);

  vector<vector<double> > M(dim,vector<double>(dim));
  vector<double> R(dim);
  
  for(int i = 0; i < lambda.size()-1; i++)
  {
    lambda[i] = h1[i]/(h1[i]+h1[i+1]);
    nu[i] = h1[i+1]/(h1[i]+h1[i+1]);
  }
  
  M[0][0] = 2;
  M[0][1] = 1;
  R[0] = FirstDiff[0]/h1[0];

  M[dim-1][dim-2] = 1;
  M[dim-1][dim-1] = 2;
  R[dim-1] = -1.0*FirstDiff[FirstDiff.size()-1]/h1[h1.size()-1];

  for(int i = 1; i < dim-2; i++)
  {
    M[i][i-1] = lambda[i-1];
    M[i][i] = 2;
    M[i][i+1] = lambda[i-1];
    R[i] = SecondDiff[i];
  }
  M[dim-2][dim-3] = lambda[dim-3];
  M[dim-2][dim-2] = 2;
  M[dim-2][dim-1] = lambda[dim-3];
  //--------Step 3: Compute coefficient _M----------
  LinearEqs LEQ(M,R);
  LEQ.solve(_M);
}

void Spline::operator =(const Spline s)
{
	_M = s.get_M();
	_coor = s.get_coor();
	_f = s.get_f();
}

void Spline::fulfill(vector<double> v_ind, vector<double> v_f)
{
	//=============Initialize coordinates=============
	int dim = v_ind.size();
	_coor = v_ind;
	_f = v_f;
	//=============Compute coefficient M=============
	//-------Step 1: Compute first order difference 
	//               and second order difference
	vector<double> FirstDiff(dim - 1);
	vector<double> h1(dim - 1);
	vector<double> SecondDiff(dim - 2);
	vector<double> h2(dim - 2);

	for (int i = 0; i < dim - 1; i++)
	{
		h1[i] = _coor[i + 1] - _coor[i];
		FirstDiff[i] = (v_f[i + 1] - v_f[i]) / h1[i];

		if (i + 2 < dim)
		{
			h2[i] = _coor[i + 2] - _coor[i];
		}
	}

	for (int i = 0; i < FirstDiff.size() - 1; i++)
	{
		SecondDiff[i] = (FirstDiff[i + 1] - FirstDiff[i]) / h2[i];
	}

	//-------Step 2: Compute LHS and RHS of equation M---
	vector<double> lambda(dim - 1);
	vector<double> nu(dim - 1);

	vector<vector<double> > M(dim, vector<double>(dim));
	vector<double> R(dim);

	for (int i = 0; i < lambda.size() - 1; i++)
	{
		lambda[i] = h1[i] / (h1[i] + h1[i + 1]);
		nu[i] = h1[i + 1] / (h1[i] + h1[i + 1]);
	}

	M[0][0] = 2;
	M[0][1] = 1;
	R[0] = FirstDiff[0] / h1[0];

	M[dim - 1][dim - 2] = 1;
	M[dim - 1][dim - 1] = 2;
	R[dim - 1] = -1.0*FirstDiff[FirstDiff.size() - 1] / h1[h1.size() - 1];

	for (int i = 1; i < dim - 2; i++)
	{
		M[i][i - 1] = lambda[i - 1];
		M[i][i] = 2;
		M[i][i + 1] = lambda[i - 1];
		R[i] = SecondDiff[i];
	}
	M[dim - 2][dim - 3] = lambda[dim - 3];
	M[dim - 2][dim - 2] = 2;
	M[dim - 2][dim - 1] = lambda[dim - 3];
	//--------Step 3: Compute coefficient _M----------
	LinearEqs LEQ(M, R);
	LEQ.solve(_M);
}

//TODO: Compute derivative at point x
double Spline::get_dev(double x)
{
 //============Locate interval of x===================
 int coor_length = _coor.size();
 int Index_Left = 0;
 int Index_Right = coor_length-1;
 int Index_Middle = floor(0.5*(Index_Left + Index_Right));
 double d_record = 10000;

 for(int Account = 0; Account < 10; Account++)
 {
    if(_coor[Index_Middle] < x)
    {
     Index_Left = Index_Middle;
     Index_Middle = floor(0.5*(Index_Left + Index_Right));
    }
    else
    {
     Index_Right = Index_Middle;
     Index_Middle = floor(0.5*(Index_Left + Index_Right));
    }

    if(d_record > _coor[Index_Right]-_coor[Index_Left])
    {
     d_record = _coor[Index_Right]-_coor[Index_Left];
    }
    else
    {
     break; //In the case v is between two nodes
    }
 }

 int ins = Index_Left;
 int inl = Index_Right;
 
 if (inl == ins)
 {
	 if (inl == (coor_length - 1))
	 {
		 ins--;
	 }
	 else if (ins == 0)
	 {
		 inl++;
	 }
 }

 
 //=========Compute derivative========================
 //Remark: Here we use external intepolation method to compute the
 //        boundary value of derivative.
 double dev = 0;
 dev -= _M[ins] * (_coor[inl] - x)*(_coor[inl] - x) / (2.0*(_coor[inl] - _coor[ins]));
 dev += _M[inl] * (x - _coor[ins])*(x - _coor[ins]) / (2.0*(_coor[inl] - _coor[ins]));
 dev += (_f[inl] - _f[ins]) / (_coor[inl] - _coor[ins]);
 dev -= (_M[inl] - _M[ins])*(_coor[inl] - _coor[ins]) / 6.0;
 return dev;

 //Remarks: The following codes are for splines with nonzero derivatives on the boundary
 //if (ins <= 2)
 //{
	// double devs = 0;
	// ins = 2;
	// inl = 3;
	// devs -= _M[ins]*(_coor[inl]-_coor[ins])*(_coor[inl]-_coor[ins])/(2.0*(_coor[inl]-_coor[ins]));
	// devs += (_f[inl]-_f[ins])/(_coor[inl]-_coor[ins]);
	// devs -= (_M[inl]-_M[ins])*(_coor[inl]-_coor[ins])/6.0;

	// double devl = 0;
	// devl += _M[inl] * (_coor[inl] - _coor[ins])*(_coor[inl] - _coor[ins]) / (2.0*(_coor[inl] - _coor[ins]));
	// devl += (_f[inl] - _f[ins]) / (_coor[inl] - _coor[ins]);
	// devl -= (_M[inl] - _M[ins])*(_coor[inl] - _coor[ins]) / 6.0;

	// dev = devs - (devl - devs)*(_coor[ins]-x)/(_coor[inl] - _coor[ins]);
	// return dev;
 //}
 //else if(ins >= (coor_length-3))
 //{
	// ins = coor_length - 5;
	// inl = coor_length - 4;

	// double devs = 0;
	// devs -= _M[ins] * (_coor[inl] - _coor[ins])*(_coor[inl] - _coor[ins]) / (2.0*(_coor[inl] - _coor[ins]));
	// devs += (_f[inl] - _f[ins]) / (_coor[inl] - _coor[ins]);
	// devs -= (_M[inl] - _M[ins])*(_coor[inl] - _coor[ins]) / 6.0;

	// double devl = 0;
	// devl += _M[inl]*(_coor[inl]-_coor[ins])*(_coor[inl]-_coor[ins])/(2.0*(_coor[inl]-_coor[ins]));
	// devl += (_f[inl]-_f[ins])/(_coor[inl]-_coor[ins]);
	// devl -= (_M[inl]-_M[ins])*(_coor[inl]-_coor[ins])/6.0;

	// dev = devl + (devl - devs)*(x-_coor[inl]) / (_coor[inl] - _coor[ins]);
	// return dev;
 //}
 //else
 //{
	// dev -= _M[ins] * (_coor[inl] - x)*(_coor[inl] - x) / (2.0*(_coor[inl] - _coor[ins]));
	// dev += _M[inl] * (x - _coor[ins])*(x - _coor[ins]) / (2.0*(_coor[inl] - _coor[ins]));
	// dev += (_f[inl] - _f[ins]) / (_coor[inl] - _coor[ins]);
	// dev -= (_M[inl] - _M[ins])*(_coor[inl] - _coor[ins]) / 6.0;
	// return dev;
 //}
 
}

//TODO: Compute value at point x
double Spline::get_val(double x)
{
	//============Locate interval of x===================
	int coor_length = _coor.size();
	int Index_Left = 0;
	int Index_Right = coor_length - 1;
	int Index_Middle = floor(0.5*(Index_Left + Index_Right));
	double d_record = 10000;

	for (int Account = 0; Account < 10; Account++)
	{
		if (_coor[Index_Middle] < x)
		{
			Index_Left = Index_Middle;
			Index_Middle = floor(0.5*(Index_Left + Index_Right));
		}
		else
		{
			Index_Right = Index_Middle;
			Index_Middle = floor(0.5*(Index_Left + Index_Right));
		}

		if (d_record > _coor[Index_Right] - _coor[Index_Left])
		{
			d_record = _coor[Index_Right] - _coor[Index_Left];
		}
		else
		{
			break; //In the case v is between two nodes
		}
	}

	int ins = Index_Left;
	int inl = Index_Right;

	if (inl == ins)
	{
		if (inl == (coor_length - 1))
		{
			ins--;
		}
		else if (ins == 0)
		{
			inl++;
		}
	}

	//=========Compute value========================
	//Remark: Since the default boundary value of the derivative 
	//        equals to 0 which is not reasonable, the boundary value
	//        of the spline is not good also. Thus we need to get the 
	//        boundary value by using external interpolationg method.
	//        i.e. x0 = x1-Dt*(dx1_dt)
	double s = 0;
	s += _M[ins] * (_coor[inl] - x)*(_coor[inl] - x)*(_coor[inl] - x) / (6.0*(_coor[inl] - _coor[ins]));
	s += _M[inl] * (x - _coor[ins])*(x - _coor[ins])*(x - _coor[ins]) / (6.0*(_coor[inl] - _coor[ins]));
	s += (_f[ins] - (_M[ins] * (_coor[inl] - _coor[ins])*(_coor[inl] - _coor[ins]) / 6.0))*(_coor[inl] - x) / (_coor[inl] - _coor[ins]);
	s += (_f[inl] - (_M[inl] * (_coor[inl] - _coor[ins])*(_coor[inl] - _coor[ins]) / 6.0))*(x - _coor[ins]) / (_coor[inl] - _coor[ins]);
	return s;

	/*if (ins <= 2)
	{
		ins = 3;
		inl = 4;
		
		double vars = 0;
		vars += _M[ins]*(_coor[inl] - _coor[ins])*(_coor[inl] - _coor[ins])*(_coor[inl] - _coor[ins])/(6.0*(_coor[inl] - _coor[ins]));
		vars += (_f[ins] - (_M[ins] * (_coor[inl] - _coor[ins])*(_coor[inl] - _coor[ins]) / 6.0))*(_coor[inl] - _coor[ins]) / (_coor[inl] - _coor[ins]);

		double varl = 0;
		varl += _M[inl] * (_coor[inl] - _coor[ins])*(_coor[inl] - _coor[ins])*(_coor[inl] - _coor[ins]) / (6.0*(_coor[inl] - _coor[ins]));
		varl += (_f[inl] - (_M[inl] * (_coor[inl] - _coor[ins])*(_coor[inl] - _coor[ins]) / 6.0))*(_coor[inl] - _coor[ins]) / (_coor[inl] - _coor[ins]);

		s = vars - (varl-vars)*(_coor[ins] - x)/(_coor[inl] - _coor[ins]);
		return s;
	}
	else if (ins >= _coor.size() - 3)
	{
		ins = _coor.size() - 5;
		inl = _coor.size() - 4;
		
		double vars = 0;
		vars += _M[ins] * (_coor[inl] - _coor[ins])*(_coor[inl] - _coor[ins])*(_coor[inl] - _coor[ins]) / (6.0*(_coor[inl] - _coor[ins]));
		vars += (_f[ins] - (_M[ins] * (_coor[inl] - _coor[ins])*(_coor[inl] - _coor[ins]) / 6.0))*(_coor[inl] - _coor[ins]) / (_coor[inl] - _coor[ins]);

		double varl = 0;
		varl += _M[inl] * (_coor[inl] - _coor[ins])*(_coor[inl] - _coor[ins])*(_coor[inl] - _coor[ins]) / (6.0*(_coor[inl] - _coor[ins]));
		varl += (_f[inl] - (_M[inl] * (_coor[inl] - _coor[ins])*(_coor[inl] - _coor[ins]) / 6.0))*(_coor[inl] - _coor[ins]) / (_coor[inl] - _coor[ins]);

		s = varl + (varl - vars)*(x-_coor[inl]) / (_coor[inl] - _coor[ins]);
		return s;
	}
	else
	{
		s += _M[ins] * (_coor[inl] - x)*(_coor[inl] - x)*(_coor[inl] - x) / (6.0*(_coor[inl] - _coor[ins]));
		s += _M[inl] * (x - _coor[ins])*(x - _coor[ins])*(x - _coor[ins]) / (6.0*(_coor[inl] - _coor[ins]));
		s += (_f[ins] - (_M[ins] * (_coor[inl] - _coor[ins])*(_coor[inl] - _coor[ins]) / 6.0))*(_coor[inl] - x) / (_coor[inl] - _coor[ins]);
		s += (_f[inl] - (_M[inl] * (_coor[inl] - _coor[ins])*(_coor[inl] - _coor[ins]) / 6.0))*(x - _coor[ins]) / (_coor[inl] - _coor[ins]);
		return s;
	}*/
}

//************************Space Spline***************************
//***************************************************************
//=====================Constructor 1=============================
//Remarks: The array Value is stored as
//x(t1), x(t2), ... x(tn)
//y(t1), y(t2), ... y(tn)
//z(t1), z(t2), ... z(tn)
SpaceSpline::SpaceSpline(const vector<vector<double> > &xyz, const vector<double> &t)
{
   vector<double> x;
   vector<double> y;
   vector<double> z;
   vector<double> fr;

   //Initialize coor vector x, y and z
   x = xyz[0];
   y = xyz[1];
   z = xyz[2];
   fr = t;

   Spline s1;
   Spline s2;
   Spline s3;

   s1.fulfill(fr, x);
   cout << "Spline x(t) has been built!" << endl;
   s2.fulfill(fr, y);
   cout << "Spline y(t) has been built!" << endl;
   s3.fulfill(fr, z);
   cout << "Spline z(t) has been built!" << endl;

   _s1 = s1;
   _s2 = s2;
   _s3 = s3;
   _t = t;
}

//=====================Constructor 2=============================
SpaceSpline::SpaceSpline(const Spline &s1, const Spline &s2, const Spline &s3)
{
 _s1 = s1;
 _s2 = s2;
 _s3 = s3;
 _t = s1.get_coor();
}

void SpaceSpline::fulfill(const vector<vector<double> > &xyz, const vector<double> &t)
{
   vector<double> x;
   vector<double> y;
   vector<double> z;
   vector<double> fr;

   //Initialize coor vector x, y and z
   x = xyz[0];
   y = xyz[1];
   z = xyz[2];
   fr = t;

   Spline s1;
   Spline s2;
   Spline s3;

   s1.fulfill(fr, x);
   cout << "Spline x(t) has been built!" << endl;
   s2.fulfill(fr, y);
   cout << "Spline y(t) has been built!" << endl;
   s3.fulfill(fr, z);
   cout << "Spline z(t) has been built!" << endl;

   _s1 = s1;
   _s2 = s2;
   _s3 = s3;
   _t = t;
}

vector<double> SpaceSpline::get_dir(double t)
{
  vector<double> dir(3);
  dir[0] = _s1.get_dev(t);
  dir[1] = _s2.get_dev(t);
  dir[2] = _s3.get_dev(t);
  double norm = sqrt(dir[0]*dir[0] + dir[1]*dir[1] + dir[2]*dir[2]);
  dir[0] /= norm;
  dir[1] /= norm;
  dir[2] /= norm;
  return dir;
}

vector<double> SpaceSpline::get_val(double t)
{
	vector<double> val(3);
	val[0] = _s1.get_val(t);
	val[1] = _s2.get_val(t);
	val[2] = _s3.get_val(t);

	return val;
}

void SpaceSpline::dataprint(string filename,vector<double> &t)
{
	/*
        ofstream out(filename, fstream::app | fstream::out);
	vector<double> dir;
	vector<double> val;
	for (int i = 0; i < t.size(); i++)
	{
		dir = get_dir(t[i]);
		val = get_val(t[i]);
		out.flags(ios::left);
		out << setw(10) << val[0] << " ";
		out << setw(10) << val[1] << " ";
		out << setw(10) << val[2] << " ";
		
		out << setw(10) << dir[0] << " ";
		out << setw(10) << dir[1] << " ";
		out << setw(10) << dir[2] << " ";

		double norm = 0;
		norm = sqrt(dir[0] * dir[0] + dir[1] * dir[1] + dir[2] * dir[2]);
		out << setw(10) << norm << " ";

		out << setw(10) << t[i] << " ";
		out << endl;
	}
	out << endl;
        */
}


void IPTransverseIsoYarnB::restart()
{
  IPTransverseIsotropic::restart();
  restartManager::restart(_local_x);
  return;
}

IPTransverseIsoYarnB::IPTransverseIsoYarnB(const IPTransverseIsoYarnB& source) : IPTransverseIsotropic()
{
	_local_x = source.getDirection();
	_M = source.getRotMatrix();
	_LS = source.getLStress();     
}

IPTransverseIsoYarnB::IPTransverseIsoYarnB(const SVector3 &GaussP, Yarn YD):IPTransverseIsotropic()
{
  //Add codes for initializing _local_x and _M.
   vector<double> GaussPoint(3);
   GaussPoint[0] = GaussP[0]; GaussPoint[1] = GaussP[1]; GaussPoint[2] = GaussP[2];

   vector<double> dir = YD.get_dir(GaussPoint); //The return value is a normalized vector.
   _local_x[0]=dir[0];
   _local_x[1]=dir[1];
   _local_x[2]=dir[2];
   
   SVector3 local_y;
   SVector3 local_z;
   SVector3 V;

   dir = YD.get_PlanOuterNormal();
   double DirNorm = sqrt(dir[0]*dir[0]+dir[1]*dir[1]+dir[2]*dir[2]);
   if(abs(DirNorm-1)<10e-3)
   {
      local_z[0]=dir[0];
      local_z[1]=dir[1];
      local_z[2]=dir[2];
      //cout<<"local_z dir: "<< dir[0]<<" "<<dir[1]<<" "<<dir[2]<<";"<<endl;
   }
   else
   { 
      OrthogonalVector(_local_x, local_z); //The output V1 is a normalized vector.
      //cout<<"local_z dir: "<< dir[0]<<" "<<dir[1]<<" "<<dir[2]<<";"<<endl;
   }

   OrthogonalVector(local_z, _local_x, local_y);//The output V2 is a normalized vector.

   static STensor3 M;
   M.set_m11(_local_x[0]);
   M.set_m21(_local_x[1]);
   M.set_m31(_local_x[2]);
   M.set_m12(local_y[0]);
   M.set_m22(local_y[1]);
   M.set_m32(local_y[2]);
   M.set_m13(local_z[0]);
   M.set_m23(local_z[1]);
   M.set_m33(local_z[2]);

   _M = M.invert();
   _Coor = GaussP;
}


IPTransverseIsoYarnB::IPTransverseIsoYarnB(const SVector3 &GaussP):IPTransverseIsotropic()
{
  //Add codes for initializing _local_x and _M.
  SVector3 local_y;
  SVector3 local_z;
  _local_x[0]=1.;
  _local_x[1]=0.;
  _local_x[2]=0.;
  local_y[0]=0.;
  local_y[1]=1.;
  local_y[2]=0.;
  local_z[0]=0.;
  local_z[1]=0.;
  local_z[2]=1.;
  
   STensor3 M;
   M.set_m11(_local_x[0]);
   M.set_m21(_local_x[1]);
   M.set_m31(_local_x[2]);
   M.set_m12(local_y[0]);
   M.set_m22(local_y[1]);
   M.set_m32(local_y[2]);
   M.set_m13(local_z[0]);
   M.set_m23(local_z[1]);
   M.set_m33(local_z[2]);
   _M = M.invert();
   _Coor = GaussP;
}

IPTransverseIsoYarnB::IPTransverseIsoYarnB(const SVector3 &GaussP, const vector<double> &Dir):IPTransverseIsotropic()
{
  //Add codes for initializing _A and _M.
  _local_x[0]=Dir[0];
  _local_x[1]=Dir[1];
  _local_x[2]=Dir[2];

   SVector3 local_z;
   OrthogonalVector(_local_x, local_z); //The output V1 is a normalized vector.


   SVector3 local_y;
   OrthogonalVector(local_z, _local_x, local_y);//The output V2 is a normalized vector.

   static STensor3 M;
   M.set_m11(_local_x[0]);
   M.set_m21(_local_x[1]);
   M.set_m31(_local_x[2]);
   M.set_m12(local_y[0]);
   M.set_m22(local_y[1]);
   M.set_m32(local_y[2]);
   M.set_m13(local_z[0]);
   M.set_m23(local_z[1]);
   M.set_m33(local_z[2]);
   _M = M.invert();

   _Coor = GaussP;
}


IPTransverseIsoYarnB& IPTransverseIsoYarnB::operator=(const IPVariable &source)
{
  IPVariableMechanics::operator=(source);
  const IPTransverseIsoYarnB* src = dynamic_cast<const IPTransverseIsoYarnB*>(&source);
	if (src!=NULL){
		_local_x = src->getDirection();
		_M = src->getRotMatrix();
		_LS = src->getLStress();		
	}
	return *this;
}

