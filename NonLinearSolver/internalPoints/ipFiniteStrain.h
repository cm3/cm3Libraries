//
//
// Description: define the IP data for finite strain cases
//
// Author:  <Van Dung NGUYEN>, (C) 2012
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef FSIPVARIABLEBASE_H_
#define FSIPVARIABLEBASE_H_

#include "ipvariable.h"
#include "IntPtData.h"
#include "MInterfaceElement.h"

class ipFiniteStrain : public IPVariableMechanics{
  protected:
    mutable IntPtData<double>* _GPData; // cache
		mutable IntPtData<double>* _interfaceGPData; // cache
    int _tag;

  public:
    ipFiniteStrain();
    ipFiniteStrain(const ipFiniteStrain &source);
    virtual ipFiniteStrain &operator=(const IPVariable &source);
    virtual ~ipFiniteStrain();
    
    int getTag() const {return _tag;};
    void setTag(int tag) {_tag = tag;};

      // get shape function information from ipvariable
    virtual std::vector<TensorialTraits<double>::ValType>& f(const FunctionSpaceBase* space, MElement* ele, IntPt &GP) const;
    virtual std::vector<TensorialTraits<double>::GradType>& gradf(const FunctionSpaceBase* space, MElement* ele, IntPt &GP) const;
    virtual std::vector<TensorialTraits<double>::HessType>& hessf(const FunctionSpaceBase* space, MElement* ele, IntPt &GP) const;
    virtual std::vector<TensorialTraits<double>::ThirdDevType>& thirdDevf(const FunctionSpaceBase* space, MElement* ele, IntPt &GP) const;
    virtual std::vector<CurlTraits<double>::CurlType>& fcurl(const FunctionSpaceBase* space, MElement* ele, IntPt &GP, const std::string & typeFunction) const;
    virtual std::vector<CurlTraits<double>::CurlCurlType>& Curlfcurl(const FunctionSpaceBase* space, MElement* ele, IntPt &GP, const std::string & typeFunction) const;
    virtual double& getJacobianDeterminant(MElement* ele, IntPt &GP) const;

		virtual std::vector<TensorialTraits<double>::ValType>& f(const FunctionSpaceBase* space, MInterfaceElement* ele, IntPt &GP) const;
    virtual std::vector<TensorialTraits<double>::GradType>& gradf(const FunctionSpaceBase* space, MInterfaceElement* ele, IntPt &GP) const;
    virtual std::vector<TensorialTraits<double>::HessType>& hessf(const FunctionSpaceBase* space, MInterfaceElement* ele, IntPt &GP) const;
    virtual std::vector<TensorialTraits<double>::ThirdDevType>& thirdDevf(const FunctionSpaceBase* space, MInterfaceElement* ele, IntPt &GP) const;
    virtual std::vector<CurlTraits<double>::CurlType>& fcurl(const FunctionSpaceBase* space, MInterfaceElement* ele, IntPt &GP, const std::string & typeFunction) const;
    virtual std::vector<CurlTraits<double>::CurlCurlType>& Curlfcurl(const FunctionSpaceBase* space, MInterfaceElement* ele, IntPt &GP, const std::string & typeFunction) const;
    virtual double& getJacobianDeterminant(MInterfaceElement* ele, IntPt &GP) const;

    virtual IPVariable* getInternalData() =0;
    virtual const IPVariable* getInternalData()  const =0;


    // Archiving data
    virtual double get(const int i) const=0;
    virtual double defoEnergy() const=0;
    virtual double plasticEnergy() const=0;
    virtual double vonMises() const=0;
    virtual void getCauchyStress(STensor3 &cauchy) const=0;
    virtual double getJ(int dim=3) const=0;

    virtual const STensor3 &getConstRefToDeformationGradient() const=0;
    virtual STensor3 &getRefToDeformationGradient()=0;

    virtual const STensor3 &getConstRefToFirstPiolaKirchhoffStress() const=0;
    virtual STensor3 &getRefToFirstPiolaKirchhoffStress()=0;

    virtual const STensor43 &getConstRefToTangentModuli() const=0;
    virtual STensor43 &getRefToTangentModuli()=0;

    virtual const SVector3 &getConstRefToReferenceOutwardNormal() const=0;
    virtual SVector3 &getRefToReferenceOutwardNormal()=0;

    virtual const SVector3 &getConstRefToCurrentOutwardNormal() const=0;
    virtual SVector3 &getRefToCurrentOutwardNormal()=0;

    virtual const SVector3 &getConstRefToJump() const=0;
    virtual SVector3 &getRefToJump()=0;

    virtual bool hasBodyForceForHO() const =0;
    virtual const STensor33 &getConstRefToGM()const=0;
    virtual STensor33 &getRefToGM()=0;
    virtual const SVector3 &getConstRefToBm() const=0;
    virtual SVector3 &getRefToBm()=0;
    virtual const STensor33 &getConstRefTodBmdFm() const=0;
    virtual STensor33 &getRefTodBmdFm()=0;
    virtual const STensor43 &getConstRefTodBmdGM() const=0;
    virtual STensor43 &getRefTodBmdGM()=0;
    virtual const STensor43 &getConstRefToDFmdFM() const=0;
    virtual STensor43 &getRefToDFmdFM()=0;
    virtual const STensor53 &getConstRefToDFmdGM() const=0;
    virtual STensor53 &getRefToDFmdGM()=0;
    virtual const STensor63 &getConstRefTodCmdFm() const=0;
    virtual STensor63 &getRefTodCmdFm()=0;
    virtual STensor63 *getPtrTodCmdFm()=0;
    virtual const STensor43 &getConstRefTodPmdFM() const=0;
    virtual STensor43 &getRefTodPmdFM()=0;

    virtual IPVariable* clone() const =0;
    virtual void restart(){
			IPVariableMechanics::restart();
      restartManager::restart(_tag);
			_GPData = NULL;
			_interfaceGPData = NULL;
		}
};


#endif // FSIPVARIABLEBASE_H_
