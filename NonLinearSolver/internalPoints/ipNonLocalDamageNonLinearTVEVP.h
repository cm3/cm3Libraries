//
// C++ Interface: Material Law
//
// Description: IPNonLinearTVEVPNonLocalDamage (IP Thermo-ViscoElasto-ViscoPlasto Law) with Non-Local Damage Interface (Its here!)
//
// Author: <Ujwal Kishore J - FLE_Knight>, (C) 2024
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef IPNONLOCALDAMAGENONLINEARTVEVP_H_
#define IPNONLOCALDAMAGENONLINEARTVEVP_H_

#include "ipNonLinearTVP.h"
#include "ipCLength.h"
#include "ipDamage.h"
#include "CLengthLaw.h"
#include "DamageLaw.h"

class IPNonLinearTVEVPLocalDamage : public IPNonLinearTVP
{
 protected:
  IPDamage*  ipvDam;
  double _damageEnergy;

 public:
  IPNonLinearTVEVPLocalDamage(const J2IsotropicHardening* comp,
                    const J2IsotropicHardening* trac, const kinematicHardening* kin, const int N, const mullinsEffect* mullins,
                    const DamageLaw *dl);
  virtual ~IPNonLinearTVEVPLocalDamage();

  IPNonLinearTVEVPLocalDamage(const IPNonLinearTVEVPLocalDamage &source);
  virtual IPNonLinearTVEVPLocalDamage& operator=(const IPVariable &source);

  virtual IPVariable* clone() const{return new IPNonLinearTVEVPLocalDamage(*this);}

  virtual double damageEnergy() const {return _damageEnergy;};
  virtual double& getRefToDamageEnergy() {return _damageEnergy;};

  virtual void restart();

  virtual const IPDamage &getConstRefToIPDamage() const
  {
    if(ipvDam==NULL)
      Msg::Error("IPNonLinearTVEVPLocalDamage: ipvDam not initialized");
    return *ipvDam;
  }
  virtual IPDamage &getRefToIPDamage()
  {
    if(ipvDam==NULL)
      Msg::Error("IPNonLinearTVEVPLocalDamage: ipvDam not initialized");
    return *ipvDam;
  }

  virtual double getDamage() const
  {
    if(ipvDam==NULL)
      Msg::Error("IPNonLinearTVEVPLocalDamage: ipvDam not initialized");
    return ipvDam->getDamage();
  }
  virtual double getMaximalP() const
  {
    if(ipvDam==NULL)
      Msg::Error("IPNonLinearTVEVPLocalDamage: ipvDam not initialized");
    return ipvDam->getMaximalP();
  }
  virtual double getDeltaDamage() const
  {
    if(ipvDam==NULL)
      Msg::Error("IPNonLinearTVEVPLocalDamage: ipvDam not initialized");
    return ipvDam->getDeltaDamage();
  }
  virtual double getDDamageDp() const
  {
    if(ipvDam==NULL)
      Msg::Error("IPNonLinearTVEVPLocalDamage: ipvDam not initialized");
    return ipvDam->getDDamageDp();
  }
  virtual const STensor3 &getConstRefToDDamageDFe() const
  {
    if(ipvDam==NULL)
      Msg::Error("IPNonLinearTVEVPLocalDamage: ipvDam not initialized");
    return ipvDam->getConstRefToDDamageDFe();
  }

};

class IPNonLinearTVEVPNonLocalDamage : public IPNonLinearTVEVPLocalDamage
{
 protected:
  IPCLength* ipvCL;
  double _nonlocalPlasticStrain;
  double _DirreversibleEnergyDNonLocalVariable;

 public:
  IPNonLinearTVEVPNonLocalDamage(const J2IsotropicHardening* comp,
                    const J2IsotropicHardening* trac, const kinematicHardening* kin, const int N, const mullinsEffect* mullins,
                    const CLengthLaw *cll, const DamageLaw *dl);
  virtual ~IPNonLinearTVEVPNonLocalDamage();

  IPNonLinearTVEVPNonLocalDamage(const IPNonLinearTVEVPNonLocalDamage &source);
  virtual IPNonLinearTVEVPNonLocalDamage& operator=(const IPVariable &source);

  virtual IPVariable* clone() const{return new IPNonLinearTVEVPNonLocalDamage(*this);}

  virtual const double& getDIrreversibleEnergyDNonLocalVariable() const {return _DirreversibleEnergyDNonLocalVariable;};
	virtual double& getRefToDIrreversibleEnergyDNonLocalVariable() {return _DirreversibleEnergyDNonLocalVariable;};

  virtual void restart();
  virtual double getCurrentPlasticStrain() const { return _epspbarre;}
  virtual double &getRefToCurrentPlasticStrain() { return _epspbarre;}
  virtual double getEffectivePlasticStrain() const { return _nonlocalPlasticStrain;}
  virtual double &getRefToEffectivePlasticStrain() { return _nonlocalPlasticStrain;}

  virtual const IPCLength &getConstRefToIPCLength() const
  {
    if(ipvCL==NULL)
      Msg::Error("IPNonLinearTVEVPNonLocalDamage: ipvCL not initialized");
    return *ipvCL;
  }
  virtual IPCLength &getRefToIPCLength()
  {
    if(ipvCL==NULL)
      Msg::Error("IPNonLinearTVEVPNonLocalDamage: ipvCL not initialized");
    return *ipvCL;
  }
  virtual const STensor3 &getConstRefToCharacteristicLength() const
  {
    if(ipvCL==NULL)
      Msg::Error("IPNonLinearTVEVPNonLocalDamage: ipvCL not initialized");
    return ipvCL->getConstRefToCL();
  }
  virtual STensor3 &getRefToCharacteristicLength() const
  {
    if(ipvCL==NULL)
      Msg::Error("IPNonLinearTVEVPNonLocalDamage: ipvCL not initialized");
    return ipvCL->getRefToCL();
  }
};

class IPNonLinearTVEVPMultipleLocalDamage : public IPNonLinearTVP{
  protected:
    int numNonLocalVariable;
    std::vector<IPDamage*> ipvDam;

    double _damageEnergy;

  public:
    IPNonLinearTVEVPMultipleLocalDamage(const J2IsotropicHardening* comp,
                    const J2IsotropicHardening* trac, const kinematicHardening* kin,
                    const int N, const mullinsEffect* mullins, const std::vector<DamageLaw*>& dl);
    virtual ~IPNonLinearTVEVPMultipleLocalDamage();
    IPNonLinearTVEVPMultipleLocalDamage(const IPNonLinearTVEVPMultipleLocalDamage& source);
    virtual IPNonLinearTVEVPMultipleLocalDamage& operator=(const IPVariable& source);

    virtual double damageEnergy() const {return _damageEnergy;};
    virtual double& getRefToDamageEnergy() {return _damageEnergy;};

    virtual int getNumNonLocalVariable() const{return numNonLocalVariable;};
    virtual IPVariable* clone() const{return new IPNonLinearTVEVPMultipleLocalDamage(*this);}

    virtual void restart();

    virtual const IPDamage &getConstRefToIPDamage(const int i) const
    {
      if(ipvDam[i]==NULL)
        Msg::Error("IPNonLinearTVEVPMultipleLocalDamage: ipvCL[%d] not initialized",i);
      return *ipvDam[i];
    }
    virtual IPDamage &getRefToIPDamage(const int i)
    {
      if(ipvDam[i]==NULL)
        Msg::Error("IPNonLinearTVEVPMultipleLocalDamage: ipvCL[%d] not initialized",i);
      return *ipvDam[i];
    }

    virtual double getDamage(const int i) const
    {
      if(ipvDam[i]==NULL)
        Msg::Error("IPNonLinearTVEVPMultipleLocalDamage: ipvCL[%d] not initialized",i);
      return ipvDam[i]->getDamage();
    }
    virtual double getMaximalP(const int i) const
    {
      if(ipvDam[i]==NULL)
        Msg::Error("IPNonLinearTVEVPMultipleLocalDamage: ipvCL[%d] not initialized",i);
      return ipvDam[i]->getMaximalP();
    }
    virtual double getDeltaDamage(const int i) const
    {
      if(ipvDam[i]==NULL)
        Msg::Error("IPNonLinearTVEVPMultipleLocalDamage: ipvCL[%d] not initialized",i);
      return ipvDam[i]->getDeltaDamage();
    }
    virtual double getDDamageDp( const int i) const
    {
      if(ipvDam[i]==NULL)
        Msg::Error("IPNonLinearTVEVPMultipleLocalDamage: ipvCL[%d] not initialized",i);
      return ipvDam[i]->getDDamageDp();
    }
    virtual const STensor3 &getConstRefToDDamageDFe(const int i) const
    {
      if(ipvDam[i]==NULL)
        Msg::Error("IPNonLinearTVEVPMultipleLocalDamage: ipvCL[%d] not initialized",i);
      return ipvDam[i]->getConstRefToDDamageDFe();
    }
};


class IPNonLinearTVEVPMultipleNonLocalDamage : public IPNonLinearTVEVPMultipleLocalDamage{
  protected:
    std::vector<IPCLength*> ipvCL;
    // bareps
    double _nonlocalEqPlasticStrain;
    double _nonlocalFailurePlasticity;
    std::vector<double> _DirreversibleEnergyDNonLocalVariable;

  public:
    IPNonLinearTVEVPMultipleNonLocalDamage(const J2IsotropicHardening* comp,
                    const J2IsotropicHardening* trac, const kinematicHardening* kin,
                    const int N,  const mullinsEffect* mullins, const std::vector<CLengthLaw*>& cll, const std::vector<DamageLaw*>& dl);
    virtual ~IPNonLinearTVEVPMultipleNonLocalDamage();
    IPNonLinearTVEVPMultipleNonLocalDamage(const IPNonLinearTVEVPMultipleNonLocalDamage& source);
    virtual IPNonLinearTVEVPMultipleNonLocalDamage& operator=(const IPVariable& source);

    virtual IPVariable* clone() const{return new IPNonLinearTVEVPMultipleNonLocalDamage(*this);}


    virtual const double& getDIrreversibleEnergyDNonLocalVariable(const int i) const {return _DirreversibleEnergyDNonLocalVariable[i];};
    virtual double& getRefToDIrreversibleEnergyDNonLocalVariable(const int i) {return _DirreversibleEnergyDNonLocalVariable[i];};

    virtual void restart();
    virtual double getCurrentPlasticStrain() const { return _epspbarre;}
    virtual double &getRefToCurrentPlasticStrain() { return _epspbarre;}

    virtual double getEffectivePlasticStrain() const { return _nonlocalEqPlasticStrain;}
    virtual double &getRefToEffectivePlasticStrain() { return _nonlocalEqPlasticStrain;}

    virtual double getNonLocalFailurePlasticity() const {return _nonlocalFailurePlasticity;};
    virtual double & getRefToNonLocalFailurePlasticity() {return _nonlocalFailurePlasticity;};

    virtual const IPCLength &getConstRefToIPCLength(const int i) const
    {
      if(ipvCL[i]==NULL)
        Msg::Error("IPNonLinearTVEVPMultipleNonLocalDamage: ipvCL[%d] not initialized",i);
      return *ipvCL[i];
    }
    virtual IPCLength &getRefToIPCLength(const int i)
    {
      if(ipvCL[i]==NULL)
        Msg::Error("IPNonLinearTVEVPMultipleNonLocalDamage: ipvCL[%d] not initialized",i);
      return *ipvCL[i];
    }
    virtual const STensor3 &getConstRefToCharacteristicLength(const int i) const
    {
      if(ipvCL[i]==NULL)
        Msg::Error("IPNonLinearTVEVPMultipleNonLocalDamage: ipvCL[%d] not initialized",i);
      return ipvCL[i]->getConstRefToCL();
    }
    virtual STensor3 &getRefToCharacteristicLength(const int i) const
    {
      if(ipvCL[i]==NULL)
        Msg::Error("IPNonLinearTVEVPMultipleNonLocalDamage: ipvCL[%d] not initialized",i);
      return ipvCL[i]->getRefToCL();
    }
};

#endif // IPNONLOCALDAMAGENONLINEARTVEVP_H_
