//
// Description: storing class for non local damage law it is a "virtual implementation" you have to define an ipvariable
//              in your project which derive from this one. (all data in this ipvarible have name beggining by _nld...)
//              so don't do the same in your project...
// Author:  <L. NOELS>, (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef IPNONLOCALDAMAGE_H_
#define IPNONLOCALDAMAGE_H_
#include "ipvariable.h"
#include "STensor3.h"
#include "fullMatrix.h"
#include "matrix_operations.h"

class IPNonLocalDamage : public IPVariableMechanics
{
 protected:
  int _nldNsdv;
  int _pos_str_mtx;
  int _pos_str_inc;
  int _pos_stn_mtx;
  int _pos_stn_inc;
  int _pos_inc_vfi;
  int _pos_inc_ar;
  int _pos_inc_R;
  int _pos_inc_Ja;
  int _pos_mtx_Ja;
  int _pos_euler;
  int _pos_inc_Dam;
  int _pos_mtx_Dam;


  int _pos_maxD;
  int _pos_inc_maxD;
  int _pos_mtx_maxD;

// for stochastic
  int pos_vfi;
  int pos_euler;
  int pos_aspR;
  int pos_ME;
  int pos_Mnu;
  int pos_Msy0;
  int pos_Mhmod1;
  int pos_Mhmod2;
  int pos_Malpha_DP;
  int pos_Mm_DP;
  int pos_Mnup;
  int pos_Mhexp;
  int pos_DamParm1;
  int pos_DamParm2;
  int pos_DamParm3;
  int pos_INCDamParm1;
  int pos_INCDamParm2;
  int pos_INCDamParm3;
  int Randnum;


  // for VT case (VM or VLM)
  // for VLM case
  // laminate euler angles
  std::vector<double> _euler_vt_lam;
  // rotate the strain or stress from local to global
  mutable std::vector<double> _str_loc{0.,0.,0.,0.,0.,0.};
  mutable std::vector<double> _str_glo{0.,0.,0.,0.,0.,0.};
  // for each VT phase
  // strain
  std::vector<int> _pos_strn_vt_m0;
  std::vector<int> _pos_strn_vt_mt;
  std::vector<int> _pos_strn_vt_lam;
  // stress
  std::vector<int> _pos_strs_vt_m0;
  std::vector<int> _pos_strs_vt_mt;
  std::vector<int> _pos_strs_vt_lam;
  // damage
  std::vector<int> _pos_dam_vt_m0;
  // for each laminate ply
  // strain
  std::vector<int> _pos_strn_vt_lam_m0;
  std::vector<int> _pos_strn_vt_lam_mt;
  // stress
  std::vector<int> _pos_strs_vt_lam_m0;
  std::vector<int> _pos_strs_vt_lam_mt;
  // damage
  std::vector<int> _pos_dam_vt_lam_m0;
  // for each material phase
  // strain
  std::vector<int> _pos_strn_vt_mt_mtx;
  std::vector<int> _pos_strn_vt_mt_inc;
  std::vector<int> _pos_strn_vt_lam_mt_mtx;
  std::vector<int> _pos_strn_vt_lam_mt_inc;
  // stress
  std::vector<int> _pos_strs_vt_mt_mtx;
  std::vector<int> _pos_strs_vt_mt_inc;
  std::vector<int> _pos_strs_vt_lam_mt_mtx;
  std::vector<int> _pos_strs_vt_lam_mt_inc;
  // damage
  std::vector<int> _pos_dam_vt_mt_mtx;
  std::vector<int> _pos_dam_vt_mt_inc;
  std::vector<int> _pos_dam_vt_lam_mt_mtx;
  std::vector<int> _pos_dam_vt_lam_mt_inc;
  
  // for LAM_2PLY case (LVM)
  // for each laminate ply
  // strain
  std::vector<int> _pos_strn_lam_m0;
  std::vector<int> _pos_strn_lam_vt;
  // stress
  std::vector<int> _pos_strs_lam_m0;
  std::vector<int> _pos_strs_lam_vt;
  // damage
  std::vector<int> _pos_dam_lam_m0;
  // for each VT grain in laminate ply
  // strain
  std::vector<int> _pos_strn_lam_vt_mt;
  // stress
  std::vector<int> _pos_strs_lam_vt_mt;
  // for each material phase
  // strain
  std::vector<int> _pos_strn_lam_vt_mt_mtx;
  std::vector<int> _pos_strn_lam_vt_mt_inc;
  // stress
  std::vector<int> _pos_strs_lam_vt_mt_mtx;
  std::vector<int> _pos_strs_lam_vt_mt_inc;
  // damage
  std::vector<int> _pos_dam_lam_vt_mt_mtx;
  std::vector<int> _pos_dam_lam_vt_mt_inc;


 public: // All data public to avoid the creation of function to access

// x y z
  fullMatrix<double> _nldMaterialTensor;

  fullVector<double> _nldCouplingStressEffectiveStrain;
  fullVector<double> _nldCouplingEffectiveStrainStress;

  fullVector<double> _nldFiberDamdStrain;
  fullVector<double> _nldStressdFiberDam;

  fullVector<double> _nldStrain; // epsilon
  fullVector<double> _nldStress; //cauchy
  STensor3           _nldCharacteristicLengthMatrix;      // by Wu Ling
  STensor3           _nldCharacteristicLengthFiber;      // by Wu Ling
// bareps

  double _nldEffectivePlasticStrain;
  double _nlFiber_d_bar;
  double _nldCurrentPlasticStrain;
  double _nlFiber_loc;
  double _nldDamage;
  double _nldSpBar;
  double _nldFd_d_bar;
  double _nldpdFd;
  double _nldFddp;



// state variables
  double* _nldStatev;
  double _elasticEne;
  double _plasticEne;
  double _stressWork;
  bool _dissipationBlocked;
  
//for TFA
  STensor3 _eigenstrain;
  STensor3 _eigenstress;

 protected:
// parthFollowing
  double _irreversibleEnergy;
  STensor3 _DirreversibleEnergyDF;    // Derivatives in terms of deformation gradient
  double _DirreversibleEnergyDNonLocalVariableMatrix;    // Derivatives in terms of non-local variable mtx
  double _DirreversibleEnergyDNonLocalVariableFiber;    // Derivatives in terms of non-local variable incl


 public:
  IPNonLocalDamage(int _nsdv) : IPVariableMechanics(), _nldNsdv(_nsdv), _nldStrain(6), _nldStress(6),
                            _nldMaterialTensor(6,6), _nldCouplingStressEffectiveStrain(6),
                            _nldCouplingEffectiveStrainStress(6), _nldFiberDamdStrain(6), _nldStressdFiberDam(6),
                            _nldEffectivePlasticStrain(0.), _nlFiber_d_bar(0.), _nldCurrentPlasticStrain(0.),_nlFiber_loc(0),
                            _nldDamage(0.),_nldSpBar(0.),
                            _nldFd_d_bar(0.), _nldpdFd(0.), _nldFddp(0.), _pos_str_mtx(-1), _pos_str_inc(-1),_pos_stn_mtx(-1),
                            _pos_stn_inc(-1), _pos_inc_vfi(-1), _pos_inc_ar(-1), _pos_inc_R (-1), _elasticEne(0.),  _pos_inc_Ja(-1),
                            _pos_mtx_Ja(-1), _pos_euler(-1), _pos_mtx_Dam(-1),_pos_inc_Dam(-1), _plasticEne(0.), _stressWork(0.),
                            _dissipationBlocked(false),  
                            _pos_maxD(-1), _pos_inc_maxD(-1), _pos_mtx_maxD(-1), pos_vfi(0),
                            pos_euler(0), pos_aspR(0), pos_ME(0), pos_Mnu(0), pos_Msy0(0), pos_Mhmod1(0),
                            pos_Mhmod2(0), pos_Malpha_DP(0), pos_Mm_DP(0), pos_Mnup(0), pos_Mhexp(0), pos_DamParm1(0), pos_DamParm2(0), pos_DamParm3(0), 
                            pos_INCDamParm1(0), pos_INCDamParm2(0), pos_INCDamParm3(0), Randnum(0),
                            _irreversibleEnergy(0),  _DirreversibleEnergyDF(0.),  _DirreversibleEnergyDNonLocalVariableMatrix(0.),
                            _DirreversibleEnergyDNonLocalVariableFiber(0.),
                            _eigenstrain(0.), _eigenstress(0.)

    {
       	mallocvector(&_nldStatev,_nldNsdv);

        for(int i = 0; i<_nldNsdv; i++) _nldStatev[i]=0.;
        for(int i=0; i<3; i++)
        {
            for(int j=0;j<3; j++)
            {
               _nldCharacteristicLengthMatrix(i,j) = 0.;
               _nldCharacteristicLengthFiber(i,j) = 0.;
            }
            _nldCharacteristicLengthMatrix(i,i) = 1.;
            _nldCharacteristicLengthFiber(i,i) = 1.;
        }
    }

  IPNonLocalDamage(const IPNonLocalDamage &source) : IPVariableMechanics(source)
    {
        _nldNsdv = source._nldNsdv;
        _pos_str_mtx = source._pos_str_mtx;
        _pos_str_inc = source._pos_str_inc;
        _pos_stn_mtx = source._pos_stn_mtx;
        _pos_stn_inc = source._pos_stn_inc;
        _pos_inc_vfi = source._pos_inc_vfi;
        _pos_inc_ar = source._pos_inc_ar;
        _pos_inc_R = source._pos_inc_R;
        _pos_inc_Ja = source._pos_inc_Ja;
        _pos_mtx_Ja = source._pos_mtx_Ja;        
        _pos_mtx_Dam = source._pos_mtx_Dam;
        _pos_inc_Dam = source._pos_inc_Dam;
        _pos_euler = source._pos_euler;
        _pos_maxD = source._pos_maxD;
        _pos_inc_maxD = source._pos_inc_maxD;
        _pos_mtx_maxD = source._pos_mtx_maxD;

        _nldStrain =source._nldStrain;
        _nldStress =source._nldStress;



        _nldEffectivePlasticStrain=source._nldEffectivePlasticStrain;
         _nlFiber_d_bar=source. _nlFiber_d_bar;
        _nldMaterialTensor=source._nldMaterialTensor;
        _nldCouplingStressEffectiveStrain = source._nldCouplingStressEffectiveStrain;
	_nldCouplingEffectiveStrainStress = source._nldCouplingEffectiveStrainStress;
        _nldFiberDamdStrain = source._nldFiberDamdStrain;
        _nldStressdFiberDam = source._nldStressdFiberDam;
        _nldCharacteristicLengthMatrix = source._nldCharacteristicLengthMatrix;                 //by Wu Ling
        _nldCharacteristicLengthFiber = source._nldCharacteristicLengthFiber;                 //by Wu Ling
        _nldCurrentPlasticStrain=source._nldCurrentPlasticStrain;
        _nlFiber_loc = source._nlFiber_loc;
        _nldDamage=source._nldDamage;
        _nldSpBar = source._nldSpBar;
        _nldFd_d_bar= source._nldFd_d_bar;
        _nldpdFd = source._nldpdFd;
        _nldFddp = source._nldFddp;
	_elasticEne=source._elasticEne;
 	_plasticEne=source._plasticEne;
        _stressWork=source._stressWork;
       	mallocvector(&_nldStatev,_nldNsdv);
        copyvect(source._nldStatev,_nldStatev,_nldNsdv);
        _dissipationBlocked = source._dissipationBlocked;

        _irreversibleEnergy = source._irreversibleEnergy;
        _DirreversibleEnergyDF = source._DirreversibleEnergyDF;  
        _DirreversibleEnergyDNonLocalVariableMatrix=source._DirreversibleEnergyDNonLocalVariableMatrix;
        _DirreversibleEnergyDNonLocalVariableFiber=source._DirreversibleEnergyDNonLocalVariableFiber;

     pos_vfi = source.pos_vfi;
     pos_euler = source.pos_euler;
     pos_aspR = source.pos_aspR; 
     pos_ME = source.pos_ME;
     pos_Mnu = source.pos_Mnu;
     pos_Msy0 = source.pos_Msy0;
     pos_Mhmod1 = source.pos_Mhmod1;
     pos_Mhmod2 = source.pos_Mhmod2;
     pos_Malpha_DP = source.pos_Malpha_DP;
     pos_Mm_DP = source.pos_Mm_DP;
     pos_Mnup = source.pos_Mnup;
     pos_Mhexp = source.pos_Mhexp;
     pos_DamParm1 = source.pos_DamParm1;
     pos_DamParm2 = source.pos_DamParm2;
     pos_DamParm3 = source.pos_DamParm3;

     pos_INCDamParm1 = source.pos_INCDamParm1;
     pos_INCDamParm2 = source.pos_INCDamParm2;
     pos_INCDamParm3 = source.pos_INCDamParm3;

     Randnum = source.Randnum;
     
     _eigenstrain = source._eigenstrain;
     _eigenstress = source._eigenstress;

     // for VT case (VM or VLM)
     // for VLM case
     // laminate euler angles
     _euler_vt_lam = source._euler_vt_lam;
     // rotate the strain or stress from local to global
     _str_loc = source._str_loc;
     _str_glo = source._str_glo;
     // for each VT phase
     // strain
     _pos_strn_vt_m0 = source._pos_strn_vt_m0;
     _pos_strn_vt_mt = source._pos_strn_vt_mt;
     _pos_strn_vt_lam = source._pos_strn_vt_lam;
     // stress
     _pos_strs_vt_m0 = source._pos_strs_vt_m0;
     _pos_strs_vt_mt = source._pos_strs_vt_mt;
     _pos_strs_vt_lam = source._pos_strs_vt_lam;
     // damage
     _pos_dam_vt_m0 = source._pos_dam_vt_m0;
     // for each laminate ply
     // strain
     _pos_strn_vt_lam_m0 = source._pos_strn_vt_lam_m0;
     _pos_strn_vt_lam_mt = source._pos_strn_vt_lam_mt;
     // stress
     _pos_strs_vt_lam_m0 = source._pos_strs_vt_lam_m0;
     _pos_strs_vt_lam_mt = source._pos_strs_vt_lam_mt;
     // damage
     _pos_dam_vt_lam_m0 = source._pos_dam_vt_lam_m0;
     // for each material phase
     // strain
     _pos_strn_vt_mt_mtx = source._pos_strn_vt_mt_mtx;
     _pos_strn_vt_mt_inc = source._pos_strn_vt_mt_inc;
     _pos_strn_vt_lam_mt_mtx = source._pos_strn_vt_lam_mt_mtx;
     _pos_strn_vt_lam_mt_inc = source._pos_strn_vt_lam_mt_inc;
     // stress
     _pos_strs_vt_mt_mtx = source._pos_strs_vt_mt_mtx;
     _pos_strs_vt_mt_inc = source._pos_strs_vt_mt_inc;
     _pos_strs_vt_lam_mt_mtx = source._pos_strs_vt_lam_mt_mtx;
     _pos_strs_vt_lam_mt_inc = source._pos_strs_vt_lam_mt_inc;
     // damage
     _pos_dam_vt_mt_mtx = source._pos_dam_vt_mt_mtx;
     _pos_dam_vt_mt_inc = source._pos_dam_vt_mt_inc;
     _pos_dam_vt_lam_mt_mtx = source._pos_dam_vt_lam_mt_mtx;
     _pos_dam_vt_lam_mt_inc = source._pos_dam_vt_lam_mt_inc;
     
     // for LAM_2PLY case (LVM)
     // for each laminate ply
     // strain
     _pos_strn_lam_m0 = source._pos_strn_lam_m0;
     _pos_strn_lam_vt = source._pos_strn_lam_vt;
     // stress
     _pos_strs_lam_m0 = source._pos_strs_lam_m0;
     _pos_strs_lam_vt = source._pos_strs_lam_vt;
     // damage
     _pos_dam_lam_m0 = source._pos_dam_lam_m0;
     // for each VT grain in laminate ply
     // strain
     _pos_strn_lam_vt_mt = source._pos_strn_lam_vt_mt;
     // stress
     _pos_strs_lam_vt_mt = source._pos_strs_lam_vt_mt;
     // for each material phase
     // strain
     _pos_strn_lam_vt_mt_mtx = source._pos_strn_lam_vt_mt_mtx;
     _pos_strn_lam_vt_mt_inc = source._pos_strn_lam_vt_mt_inc;
     // stress
     _pos_strs_lam_vt_mt_mtx = source._pos_strs_lam_vt_mt_mtx;
     _pos_strs_lam_vt_mt_inc = source._pos_strs_lam_vt_mt_inc;
     // damage
     _pos_dam_lam_vt_mt_mtx = source._pos_dam_lam_vt_mt_mtx;
     _pos_dam_lam_vt_mt_inc = source._pos_dam_lam_vt_mt_inc;
    }

  IPNonLocalDamage &operator = (const IPVariable &_source)
  {
      IPVariableMechanics::operator=(_source);
      const IPNonLocalDamage *source=static_cast<const IPNonLocalDamage *> (&_source);
      _pos_str_mtx = source->_pos_str_mtx;
      _pos_str_inc = source->_pos_str_inc;
      _pos_stn_mtx = source->_pos_stn_mtx;
      _pos_stn_inc = source->_pos_stn_inc;
      _pos_inc_vfi = source->_pos_inc_vfi;
      _pos_inc_ar = source->_pos_inc_ar;
      _pos_inc_R = source->_pos_inc_R;
      _pos_inc_Ja = source->_pos_inc_Ja;
      _pos_mtx_Ja = source->_pos_mtx_Ja;
      _pos_mtx_Dam = source->_pos_mtx_Dam;
      _pos_inc_Dam = source->_pos_inc_Dam;
      _pos_maxD = source->_pos_maxD;
      _pos_inc_maxD = source->_pos_inc_maxD;
      _pos_mtx_maxD = source->_pos_mtx_maxD;
      _pos_euler = source->_pos_euler;
      _nldStrain = source->_nldStrain;
      _nldStress = source->_nldStress;
 

      _nldEffectivePlasticStrain=source->_nldEffectivePlasticStrain;
      _nlFiber_d_bar=source-> _nlFiber_d_bar;
      _nldMaterialTensor=source->_nldMaterialTensor;
      _nldCouplingStressEffectiveStrain = source->_nldCouplingStressEffectiveStrain;
      _nldCouplingEffectiveStrainStress = source->_nldCouplingEffectiveStrainStress;
      _nldFiberDamdStrain = source->_nldFiberDamdStrain;
      _nldStressdFiberDam = source->_nldStressdFiberDam;
      _nldCharacteristicLengthMatrix = source->_nldCharacteristicLengthMatrix;                 //by Wu Ling
      _nldCharacteristicLengthFiber = source->_nldCharacteristicLengthFiber;                 //by Wu Ling
      _nldCurrentPlasticStrain=source->_nldCurrentPlasticStrain;
      _nlFiber_loc = source->_nlFiber_loc;
      _nldDamage=source->_nldDamage;
      _nldSpBar = source->_nldSpBar;
      _nldFd_d_bar= source->_nldFd_d_bar;
      _nldpdFd = source->_nldpdFd;
      _nldFddp = source->_nldFddp;
      _elasticEne=source->_elasticEne;
      _plasticEne=source->_plasticEne;
      _stressWork=source->_stressWork;
      if(_nldNsdv !=source->_nldNsdv) Msg::Error("IPNonLocalDamage do not have the same number of internal variables");
      copyvect(source->_nldStatev,_nldStatev,_nldNsdv);
      _dissipationBlocked = source->_dissipationBlocked;

      _irreversibleEnergy = source->_irreversibleEnergy;
      _DirreversibleEnergyDF = source->_DirreversibleEnergyDF;  
      _DirreversibleEnergyDNonLocalVariableMatrix=source->_DirreversibleEnergyDNonLocalVariableMatrix;
      _DirreversibleEnergyDNonLocalVariableFiber=source->_DirreversibleEnergyDNonLocalVariableFiber;

     pos_vfi = source->pos_vfi;
     pos_euler = source->pos_euler;
     pos_aspR = source->pos_aspR; 
     pos_ME = source->pos_ME;
     pos_Mnu = source->pos_Mnu;
     pos_Msy0 = source->pos_Msy0;
     pos_Mhmod1 = source->pos_Mhmod1;
     pos_Mhmod2 = source->pos_Mhmod2;
     pos_Malpha_DP = source->pos_Malpha_DP;
     pos_Mm_DP = source->pos_Mm_DP;
     pos_Mnup = source->pos_Mnup;
     pos_Mhexp = source->pos_Mhexp;
     pos_DamParm1 = source->pos_DamParm1;
     pos_DamParm2 = source->pos_DamParm2;
     pos_DamParm3 = source->pos_DamParm3;

     pos_INCDamParm1 = source->pos_INCDamParm1;
     pos_INCDamParm2 = source->pos_INCDamParm2;
     pos_INCDamParm3 = source->pos_INCDamParm3;

     Randnum = source->Randnum;
     
     _eigenstrain = source->_eigenstrain;
     _eigenstress = source->_eigenstress;

     // for VT case (VM or VLM)
     // for VLM case
     // laminate euler angles
     _euler_vt_lam = source->_euler_vt_lam;
     // rotate the strain or stress from local to global
     _str_loc = source->_str_loc;
     _str_glo = source->_str_glo;
     // for each VT phase
     // strain
     _pos_strn_vt_m0 = source->_pos_strn_vt_m0;
     _pos_strn_vt_mt = source->_pos_strn_vt_mt;
     _pos_strn_vt_lam = source->_pos_strn_vt_lam;
     // stress
     _pos_strs_vt_m0 = source->_pos_strs_vt_m0;
     _pos_strs_vt_mt = source->_pos_strs_vt_mt;
     _pos_strs_vt_lam = source->_pos_strs_vt_lam;
     // damage
     _pos_dam_vt_m0 = source->_pos_dam_vt_m0;
     // for each laminate ply
     // strain
     _pos_strn_vt_lam_m0 = source->_pos_strn_vt_lam_m0;
     _pos_strn_vt_lam_mt = source->_pos_strn_vt_lam_mt;
     // stress
     _pos_strs_vt_lam_m0 = source->_pos_strs_vt_lam_m0;
     _pos_strs_vt_lam_mt = source->_pos_strs_vt_lam_mt;
     // damage
     _pos_dam_vt_lam_m0 = source->_pos_dam_vt_lam_m0;
     // for each material phase
     // strain
     _pos_strn_vt_mt_mtx = source->_pos_strn_vt_mt_mtx;
     _pos_strn_vt_mt_inc = source->_pos_strn_vt_mt_inc;
     _pos_strn_vt_lam_mt_mtx = source->_pos_strn_vt_lam_mt_mtx;
     _pos_strn_vt_lam_mt_inc = source->_pos_strn_vt_lam_mt_inc;
     // stress
     _pos_strs_vt_mt_mtx = source->_pos_strs_vt_mt_mtx;
     _pos_strs_vt_mt_inc = source->_pos_strs_vt_mt_inc;
     _pos_strs_vt_lam_mt_mtx = source->_pos_strs_vt_lam_mt_mtx;
     _pos_strs_vt_lam_mt_inc = source->_pos_strs_vt_lam_mt_inc;
     // damage
     _pos_dam_vt_mt_mtx = source->_pos_dam_vt_mt_mtx;
     _pos_dam_vt_mt_inc = source->_pos_dam_vt_mt_inc;
     _pos_dam_vt_lam_mt_mtx = source->_pos_dam_vt_lam_mt_mtx;
     _pos_dam_vt_lam_mt_inc = source->_pos_dam_vt_lam_mt_inc;
     
     // for LAM_2PLY case (LVM)
     // for each laminate ply
     // strain
     _pos_strn_lam_m0 = source->_pos_strn_lam_m0;
     _pos_strn_lam_vt = source->_pos_strn_lam_vt;
     // stress
     _pos_strs_lam_m0 = source->_pos_strs_lam_m0;
     _pos_strs_lam_vt = source->_pos_strs_lam_vt;
     // damage
     _pos_dam_lam_m0 = source->_pos_dam_lam_m0;
     // for each VT grain in laminate ply
     // strain
     _pos_strn_lam_vt_mt = source->_pos_strn_lam_vt_mt;
     // stress
     _pos_strs_lam_vt_mt = source->_pos_strs_lam_vt_mt;
     // for each material phase
     // strain
     _pos_strn_lam_vt_mt_mtx = source->_pos_strn_lam_vt_mt_mtx;
     _pos_strn_lam_vt_mt_inc = source->_pos_strn_lam_vt_mt_inc;
     // stress
     _pos_strs_lam_vt_mt_mtx = source->_pos_strs_lam_vt_mt_mtx;
     _pos_strs_lam_vt_mt_inc = source->_pos_strs_lam_vt_mt_inc;
     // damage 
     _pos_dam_lam_vt_mt_mtx = source->_pos_dam_lam_vt_mt_mtx;
     _pos_dam_lam_vt_mt_inc = source->_pos_dam_lam_vt_mt_inc;

      return *this;
  }
  virtual ~IPNonLocalDamage()
  {
      free(_nldStatev);
  }
  // Archiving data
  virtual void blockDissipation(const bool fl){_dissipationBlocked = fl;};
  virtual bool dissipationIsBlocked() const {return _dissipationBlocked;};
  virtual double get(const int i) const;
  virtual double defoEnergy() const;
  virtual double plasticEnergy() const;
  virtual double stressWork() const;
  virtual double getEffectivePlasticStrain() const { return _nldEffectivePlasticStrain;}
  virtual double getCurrentPlasticStrain() const { return _nldCurrentPlasticStrain;}
  virtual double getCurrentlocFd() const { return _nlFiber_loc;}
  virtual double getRefToEffectivePlasticStrain() { return _nldEffectivePlasticStrain;}
  virtual double getRefToCurrentPlasticStrain() { return _nldCurrentPlasticStrain;}
  virtual double getCurrentlocFd() { return _nlFiber_loc;}

  virtual double getFiberDamage() { return _nlFiber_d_bar;}

  virtual double getNsdv() const {return _nldNsdv;}
  virtual void setPosStrMtx(int i) {_pos_str_mtx = i;}
  virtual void setPosStrInc(int i) {_pos_str_inc = i;}
  virtual void setPosStnMtx(int i) {_pos_stn_mtx = i;}
  virtual void setPosStnInc(int i) {_pos_stn_inc = i;}
  virtual void setPosIncVfi(int i) {_pos_inc_vfi = i;}
  virtual void setPosIncAR(int i) {_pos_inc_ar = i;}
  virtual void setPosIncR(int i) {_pos_inc_R = i;}
  virtual void setPosIncJa(int i) {_pos_inc_Ja = i;}
  virtual void setPosMtxJa(int i) {_pos_mtx_Ja = i;}
  virtual void setPosIncDam(int i) {_pos_inc_Dam = i;}
  virtual void setPosMtxDam(int i) {_pos_mtx_Dam = i;}
  virtual void setPosEuler(int i) {_pos_euler = i;}

  virtual void setPosMaxD(int i) {_pos_maxD = i;}
  virtual void setPosIncMaxD(int i) {_pos_inc_maxD = i;}
  virtual void setPosMtxMaxD(int i) {_pos_mtx_maxD = i;}

  virtual int getPosMaxD() const { return _pos_maxD;}
  virtual int getPosIncMaxD() const { return _pos_inc_maxD;}
  virtual int getPosMtxMaxD() const { return _pos_mtx_maxD;}

  virtual void setMaxD(double Dc) {if (_pos_maxD>-1) _nldStatev[_pos_maxD] = Dc; }
  virtual void setIncMaxD(double Dc) {if (_pos_inc_maxD>-1)_nldStatev[_pos_inc_maxD] = Dc; }
  virtual void setMtxMaxD(double Dc) {if (_pos_mtx_maxD>-1)_nldStatev[_pos_mtx_maxD] = Dc; }

  virtual double getMaxD() const {if (_pos_maxD>-1) return _nldStatev[_pos_maxD]; else return 1.; }
  virtual double getIncMaxD() const {if (_pos_inc_maxD>-1) return _nldStatev[_pos_inc_maxD]; else return 1.; }
  virtual double getMtxMaxD() const {if (_pos_mtx_maxD>-1) return _nldStatev[_pos_mtx_maxD]; else return 1.; }

  virtual void setCriticalDamage(const double DT, int idex);
  virtual double getCriticalDamage(int idex) const;
  virtual double getDamageIndicator(int idex) const;

  virtual void setMaxD(double *state, double Dc) const {if (_pos_maxD>-1) state[_pos_maxD] = Dc; }
  virtual void setIncMaxD(double *state, double Dc) const {if (_pos_inc_maxD>-1) state[_pos_inc_maxD] = Dc; }
  virtual void setMtxMaxD(double *state, double Dc) const {if (_pos_mtx_maxD>-1) state[_pos_mtx_maxD] = Dc; }

  virtual double getMaxD(double *state) const {if (_pos_maxD>-1) return state[_pos_maxD]; else return 1.;}
  virtual double getIncMaxD(double *state) const {if (_pos_inc_maxD>-1) return state[_pos_inc_maxD]; else return 1.;}
  virtual double getMtxMaxD(double *state) const {if (_pos_mtx_maxD>-1) return state[_pos_mtx_maxD]; else return 1.; }

  virtual void setCriticalDamage(double *state, const double DT, int idex);
  virtual double getCriticalDamage(double *state, int idex) const;


  virtual IPVariable* clone() const {return new IPNonLocalDamage(*this);};
  virtual void restart();

  // for path following
  virtual double irreversibleEnergy() const {return _irreversibleEnergy;};
  virtual double& getRefToIrreversibleEnergy() {return _irreversibleEnergy;};  
  virtual const STensor3& getConstRefToDIrreversibleEnergyDF() const{return _DirreversibleEnergyDF;};
  virtual STensor3& getRefToDIrreversibleEnergyDF() {return _DirreversibleEnergyDF;};
  virtual const double& getDIrreversibleEnergyDNonLocalVariable(const int i) const 
  {  
     if(i==0)return _DirreversibleEnergyDNonLocalVariableMatrix;
     else return _DirreversibleEnergyDNonLocalVariableFiber;
  };
  virtual double& getRefToDIrreversibleEnergyDNonLocalVariable(const int i)
  {  
     if(i==0)return _DirreversibleEnergyDNonLocalVariableMatrix;
     else return _DirreversibleEnergyDNonLocalVariableFiber;
  };


// for stochastic
  virtual int getPos_vfi() const { return pos_vfi;};
  virtual int getPos_euler() const { return pos_euler;};
  virtual int getPos_aspR() const { return pos_aspR;};
  virtual int getPos_ME() const { return pos_ME;};
  virtual int getPos_Mnu() const { return pos_Mnu;};
  virtual int getPos_Msy0() const { return pos_Msy0;};
  virtual int getPos_Mhmod1() const { return pos_Mhmod1;};
  virtual int getPos_Mhmod2() const { return pos_Mhmod2;};
  virtual int getPos_Malpha_DP() const { return pos_Malpha_DP;};
  virtual int getPos_Mm_DP() const { return pos_Mm_DP;};
  virtual int getPos_Mnup() const { return pos_Mnup;};
  virtual int getPos_Mhexp() const { return pos_Mhexp;};
  virtual int getPos_DamParm1() const { return pos_DamParm1;};
  virtual int getPos_DamParm2() const { return pos_DamParm2;};
  virtual int getPos_DamParm3() const { return pos_DamParm3;};
  virtual int getPos_INCDamParm1() const { return pos_INCDamParm1;};
  virtual int getPos_INCDamParm2() const { return pos_INCDamParm2;};
  virtual int getPos_INCDamParm3() const { return pos_INCDamParm3;};
  virtual int getRandnum() const { return Randnum;};

  virtual void setPos_vfi( int p ) { pos_vfi=p;};
  virtual void setPos_euler( int p ) { pos_euler=p;};
  virtual void setPos_aspR( int p ) { pos_aspR=p;};
  virtual void setPos_ME( int p ) {  pos_ME=p;};
  virtual void setPos_Mnu( int p ) {  pos_Mnu=p;};
  virtual void setPos_Msy0( int p ) {  pos_Msy0=p;};
  virtual void setPos_Mhmod1( int p ) {  pos_Mhmod1=p;};
  virtual void setPos_Mhmod2( int p ) {  pos_Mhmod2=p;};
  virtual void setPos_Malpha_DP( int p ) {  pos_Malpha_DP=p;};
  virtual void setPos_Mm_DP( int p ) {  pos_Mm_DP=p;};
  virtual void setPos_Mnup( int p ) {  pos_Mnup=p;};
  virtual void setPos_Mhexp( int p ) {  pos_Mhexp=p;};
  virtual void setPos_DamParm1( int p ) {  pos_DamParm1=p;};
  virtual void setPos_DamParm2( int p ) {  pos_DamParm2=p;};
  virtual void setPos_DamParm3( int p ) {  pos_DamParm3=p;};
  virtual void setPos_INCDamParm1( int p ) {  pos_INCDamParm1=p;};
  virtual void setPos_INCDamParm2( int p ) {  pos_INCDamParm2=p;};
  virtual void setPos_INCDamParm3( int p ) {  pos_INCDamParm3=p;};
  virtual void setRandnum( int p ) {  Randnum=p;};

  // for VT case (VM or VLM)
  // for VLM case
  // laminate euler angles
  virtual void setEulerVTLam(std::vector<double> &euler_lam)
  {
  	_euler_vt_lam.insert(_euler_vt_lam.end(), euler_lam.begin(), euler_lam.end());
  };
  // rotate the strain or stress from local to global
  virtual void rotLocToGlo(std::vector<double> &str_loc, std::vector<double> &str_glo, double *state, int pos, int grainNb) const
  {
  	double pi = 3.14159265358979323846;
  	double fpi = pi/180.;
  	double sq2 = sqrt(2.);
  	double c1, c2, c3, s1, s2, s3;
  	int i, j;

  	static std::vector<double> euler{0.,0.,0.};
  	static double P[3][3];
  	static double R[6][6];
  	static double Rinv[6][6];

        std::fill(euler.begin(), euler.end(), 0.);
  	euler[0] = _euler_vt_lam[grainNb*3+0];
  	euler[1] = _euler_vt_lam[grainNb*3+1];
  	euler[2] = _euler_vt_lam[grainNb*3+2];

  	std::fill(str_loc.begin(), str_loc.end(), 0.);
  	for(i=0;i<6;i++){
  	    str_loc[i] = state[pos+i];
	}

	// construct rotation matrix local to global (eul_mat33+transpose)
	c1 = cos(euler[0]*fpi);
	s1 = sin(euler[0]*fpi);

	c2 = cos(euler[1]*fpi);
	s2 = sin(euler[1]*fpi);

	c3 = cos(euler[2]*fpi);
	s3 = sin(euler[2]*fpi);

	P[0][0] = c3*c1 - s1*c2*s3;
	P[0][1] = c3*s1 + c1*c2*s3;
	P[0][2] = s2*s3;

	P[1][0] = -s3*c1 - s1*c2*c3;
	P[1][1] = -s3*s1 + c1*c2*c3;
	P[1][2] = s2*c3;

	P[2][0] = s1*s2;
	P[2][1] = -c1*s2;
	P[2][2] = c2;

	for(i=0;i<6;i++){
            for(j=0;j<6;j++){
                R[i][j] = 0.;
                Rinv[i][j] = 0.;
	    }
	}

        for(i=0;i<3;i++){
	    R[i][3] = P[i][0]*P[i][1]*sq2;
	    R[i][4] = P[i][0]*P[i][2]*sq2;
            R[i][5] = P[i][1]*P[i][2]*sq2;
	    R[3][i] = P[0][i]*P[1][i]*sq2;
	    R[4][i] = P[0][i]*P[2][i]*sq2;
	    R[5][i] = P[1][i]*P[2][i]*sq2;

	    for(j=0;j<3;j++){
	        R[i][j] = P[i][j]*P[i][j];
	    }
	}

   	R[3][3] = P[0][0]*P[1][1] + P[0][1]*P[1][0];
   	R[3][4] = P[0][0]*P[1][2] + P[0][2]*P[1][0];
   	R[3][5] = P[0][1]*P[1][2] + P[0][2]*P[1][1];
   	R[4][3] = P[0][0]*P[2][1] + P[0][1]*P[2][0];
   	R[4][4] = P[0][0]*P[2][2] + P[0][2]*P[2][0];
   	R[4][5] = P[0][1]*P[2][2] + P[0][2]*P[2][1];
   	R[5][3] = P[1][0]*P[2][1] + P[1][1]*P[2][0];
   	R[5][4] = P[1][0]*P[2][2] + P[1][2]*P[2][0];
   	R[5][5] = P[1][1]*P[2][2] + P[1][2]*P[2][1];

  	// transpose
  	double tmp;
	for(i=0;i<6;i++){
	    Rinv[i][i] = R[i][i];
	        for(j=i+1;j<6;j++){
	            tmp = R[i][j];
		    Rinv[i][j] = R[j][i];
		    Rinv[j][i] = tmp;
		}
	}

	// rotation
	std::fill(str_glo.begin(), str_glo.end(), 0.);
        for(i=0;i<6;i++){
	    for(j=0;j<6;j++){
	        str_glo[i] = str_glo[i] + Rinv[i][j]*str_loc[j];
	    }
	}

  };
  // for each VT phase
  // strain
  virtual void setPosStrnVTM0(int i) {_pos_strn_vt_m0.emplace_back (i);};
  virtual void setPosStrnVTMT(int i) {_pos_strn_vt_mt.emplace_back (i);};
  virtual void setPosStrnVTLam(int i) {_pos_strn_vt_lam.emplace_back (i);};
  // stress
  virtual void setPosStrsVTM0(int i) {_pos_strs_vt_m0.emplace_back (i);};
  virtual void setPosStrsVTMT(int i) {_pos_strs_vt_mt.emplace_back (i);};
  virtual void setPosStrsVTLam(int i) {_pos_strs_vt_lam.emplace_back (i);};
  // damage
  virtual void setPosDamVTM0(int i) {_pos_dam_vt_m0.emplace_back (i);};
  // for each laminate ply
  // strain
  virtual void setPosStrnVTLamM0(int i) {_pos_strn_vt_lam_m0.emplace_back (i);};
  virtual void setPosStrnVTLamMT(int i) {_pos_strn_vt_lam_mt.emplace_back (i);};
  // stress
  virtual void setPosStrsVTLamM0(int i) {_pos_strs_vt_lam_m0.emplace_back (i);};
  virtual void setPosStrsVTLamMT(int i) {_pos_strs_vt_lam_mt.emplace_back (i);};
  // damage
  virtual void setPosDamVTLamM0(int i) {_pos_dam_vt_lam_m0.emplace_back (i);};
  // for each material phase
  // strain
  virtual void setPosStrnVTMTMtx(int i) {_pos_strn_vt_mt_mtx.emplace_back (i);};
  virtual void setPosStrnVTMTInc(int i) {_pos_strn_vt_mt_inc.emplace_back (i);};
  virtual void setPosStrnVTLamMTMtx(int i) {_pos_strn_vt_lam_mt_mtx.emplace_back (i);};
  virtual void setPosStrnVTLamMTInc(int i) {_pos_strn_vt_lam_mt_inc.emplace_back (i);};
  // stress
  virtual void setPosStrsVTMTMtx(int i) {_pos_strs_vt_mt_mtx.emplace_back (i);};
  virtual void setPosStrsVTMTInc(int i) {_pos_strs_vt_mt_inc.emplace_back (i);};
  virtual void setPosStrsVTLamMTMtx(int i) {_pos_strs_vt_lam_mt_mtx.emplace_back (i);};
  virtual void setPosStrsVTLamMTInc(int i) {_pos_strs_vt_lam_mt_inc.emplace_back (i);};
  // damage
  virtual void setPosDamVTMTMtx(int i) {_pos_dam_vt_mt_mtx.emplace_back (i);};
  virtual void setPosDamVTMTInc(int i) {_pos_dam_vt_mt_inc.emplace_back (i);};
  virtual void setPosDamVTLamMTMtx(int i) {_pos_dam_vt_lam_mt_mtx.emplace_back (i);};
  virtual void setPosDamVTLamMTInc(int i) {_pos_dam_vt_lam_mt_inc.emplace_back (i);};
  
  // for LAM_2PLY case (LVM)
  // for each laminate ply
  // strain
  virtual void setPosStrnLamM0(int i) {_pos_strn_lam_m0.emplace_back (i);};
  virtual void setPosStrnLamVT(int i) {_pos_strn_lam_vt.emplace_back (i);};
  // stress
  virtual void setPosStrsLamM0(int i) {_pos_strs_lam_m0.emplace_back (i);};
  virtual void setPosStrsLamVT(int i) {_pos_strs_lam_vt.emplace_back (i);};
  // damage
  virtual void setPosDamLamM0(int i) {_pos_dam_lam_m0.emplace_back (i);};
  // for each VT grain in laminate ply
  // strain
  virtual void setPosStrnLamVTMT(int i) {_pos_strn_lam_vt_mt.emplace_back (i);};
  // stress
  virtual void setPosStrsLamVTMT(int i) {_pos_strs_lam_vt_mt.emplace_back (i);};
  // for each material phase
  // strain
  virtual void setPosStrnLamVTMTMtx(int i) {_pos_strn_lam_vt_mt_mtx.emplace_back (i);};
  virtual void setPosStrnLamVTMTInc(int i) {_pos_strn_lam_vt_mt_inc.emplace_back (i);};
  // stress
  virtual void setPosStrsLamVTMTMtx(int i) {_pos_strs_lam_vt_mt_mtx.emplace_back (i);};
  virtual void setPosStrsLamVTMTInc(int i) {_pos_strs_lam_vt_mt_inc.emplace_back (i);};
  // damage
  virtual void setPosDamLamVTMTMtx(int i) {_pos_dam_lam_vt_mt_mtx.emplace_back (i);};
  virtual void setPosDamLamVTMTInc(int i) {_pos_dam_lam_vt_mt_inc.emplace_back (i);};


  // MPI function to define value to be commmunicated between processes
   #if defined(HAVE_MPI)
  virtual int numberValuesToCommunicateMPI()const;
  virtual void fillValuesToCommunicateMPI(double *arrayMPI)const;
  virtual void getValuesFromMPI(const double *arrayMPI);
  #endif // HAVE_MPI

};

#endif // IPNONLOCALDAMAGE_H_
