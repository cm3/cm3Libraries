// Source: IPVariable For mlawGenericRM
// mlawGenericRM is a generic (wrapper) material law that lets
// impose a residual deformation gradient, to 
// apply the mech constitutive law on a pre strained configuration.
//
// Note: 
//  Build View of 9 Fres Components (XY) are:
//  IPField::FfAM_XY
//
// Created by mohib on 14.08.24.
//

#include "ipGenericRM.h"
#include "ipField.h"

IPGenericRM::IPGenericRM(const STensor3 &_FRes) : IPVariableMechanics()
{
    _ipMeca=NULL;
    FRes=_FRes;
}

IPGenericRM::IPGenericRM(const IPGenericRM &source) :
IPVariableMechanics(source), _ipMeca(NULL)
{
    if(source._ipMeca!=NULL)
    {
        if(_ipMeca!=NULL)
        {
            delete _ipMeca;
            _ipMeca=NULL;
        }
        _ipMeca = dynamic_cast<IPVariableMechanics *>(source.getConstRefToIpMeca().clone());
    }
    FRes=source.FRes;
}

IPGenericRM& IPGenericRM::operator=(const IPVariable &source)
{
    IPVariableMechanics::operator=(source);
    const IPGenericRM* src = dynamic_cast<const IPGenericRM*>(&source);
    if(src)
    {
        if(src->_ipMeca!=NULL)
        {
            if(_ipMeca!=NULL)
            {
                _ipMeca->operator=(dynamic_cast<const IPVariable &> (*(src->_ipMeca)));
            }
            else
                _ipMeca = dynamic_cast<IPVariableMechanics *>((src->getConstRefToIpMeca()).clone());
        }
        FRes=src->FRes;
    }
    return *this;
}

void IPGenericRM::restart()
{
    _ipMeca->restart();
    restartManager::restart(FRes);
    IPVariableMechanics::restart();
}

// Getter for accessing components of Fres @Mohib
double IPGenericRM::get(const int comp) const
{
   if(comp==IPField::FfAM_XX)
     return FRes(0,0);
   else    if(comp==IPField::FfAM_YY)
     return FRes(1,1);
   else    if(comp==IPField::FfAM_ZZ)
     return FRes(2,2);
   else    if(comp==IPField::FfAM_XY)
     return FRes(0,1);
   else    if(comp==IPField::FfAM_ZX)
     return FRes(0,2);
   else    if(comp==IPField::FfAM_YZ)
     return FRes(1,2);
   else 
       return _ipMeca->get(comp);

}

double & IPGenericRM::getRef(const int comp)
{
  if(comp==IPField::FfAM_XX)
     return FRes(0,0);
   else    if(comp==IPField::FfAM_YY)
     return FRes(1,1);
   else    if(comp==IPField::FfAM_ZZ)
     return FRes(2,2);
   else    if(comp==IPField::FfAM_XY)
     return FRes(0,1);
   else    if(comp==IPField::FfAM_ZX)
     return FRes(0,2);
   else    if(comp==IPField::FfAM_YZ)
     return FRes(1,2);
   else 
       return _ipMeca->getRef(comp);
}
