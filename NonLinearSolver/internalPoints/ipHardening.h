//
// C++ Interface: ipHardening
//
// Description: Base class for ipHardening
//
//
// Author:  <L. Noels>, (C) 2013
//
// Copyright: See COPYING file that comes with this distribution
//
//
// class with the variables of IP (stress, deformation and localBasis)
#ifndef IPHARDENING_H_
#define IPHARDENING_H_
#include <stdlib.h>
#include <stdio.h>
#include "GmshConfig.h"
#include "ipvariable.h"

class IPJ2IsotropicHardening{

protected:
    double R0;         // initital yield stress (could be temperature dependent)
    double R;          // actual yield stress
    double dR;
    double ddR;
    double integR;
    bool _isSaturated; // harderning modulus become 0 under some conditions
    double dRdT; // temeprature dependence
    double ddRdTT; // 
    double ddRdT; // ddR/dgdT 

    double wp;  //yield shrink because of plastic instability
    double dwp;
 
 public:
  IPJ2IsotropicHardening(const double R0_);
  IPJ2IsotropicHardening(const IPJ2IsotropicHardening &source);
  virtual IPJ2IsotropicHardening &operator=(const IPJ2IsotropicHardening &source);
  virtual ~IPJ2IsotropicHardening(){}
  virtual void restart();
  virtual IPJ2IsotropicHardening * clone() const=0;

  virtual bool& getRefToSaturationState() {return _isSaturated;};
  virtual bool isSaturated() const {return _isSaturated;};

  virtual double getR0() const {return R0;}
  virtual double getR() const {return (1-wp)*R;}
  virtual double getDR() const {return (1-wp)*dR;}
  virtual double getDDR() const {return (1-wp)*ddR;}
  virtual double getIntegR() const {return (1-wp)*integR;}
  virtual double getDRDT() const{ return (1-wp)*dRdT;};
  virtual double getDDRDTT() const{ return (1-wp)*ddRdTT;};
  virtual double getDDRDT() const{ return (1-wp)*ddRdT;};
  virtual double getWp() const{ return wp;};
  virtual double getDWp() const{ return dwp;};
  virtual void set(const double &_r0, const double &_r, const double &_dr, const double &_ddr, const double &_integR, 
                    const double& _dRdT, const double& _ddRdTT, const double& _ddRdT) final
  {
    R0 = _r0;
    R=_r;
    dR=_dr;
    ddR=_ddr;
    integR=_integR;
    dRdT = _dRdT;
    ddRdTT = _ddRdTT;
    ddRdT = _ddRdT;
  }
  virtual void setScaleFactor(double _wp, double _dwp) final
  {
    wp=_wp;
    dwp=_dwp;
  }
};

class IPUserJ2IsotropicHardening : public IPJ2IsotropicHardening
{

 protected:

 public:
  IPUserJ2IsotropicHardening(const double R0_);
  IPUserJ2IsotropicHardening(const IPUserJ2IsotropicHardening &source);
  virtual IPUserJ2IsotropicHardening &operator=(const IPJ2IsotropicHardening &source);
  virtual ~IPUserJ2IsotropicHardening(){}
  virtual void restart();
  virtual IPJ2IsotropicHardening * clone() const;
};

class IPPerfectlyPlasticJ2IsotropicHardening : public IPJ2IsotropicHardening
{

 protected:

 public:
  IPPerfectlyPlasticJ2IsotropicHardening(const double R0_);
  IPPerfectlyPlasticJ2IsotropicHardening(const IPPerfectlyPlasticJ2IsotropicHardening &source);
  virtual IPPerfectlyPlasticJ2IsotropicHardening &operator=(const IPJ2IsotropicHardening &source);
  virtual ~IPPerfectlyPlasticJ2IsotropicHardening(){}
  virtual void restart();
  virtual IPJ2IsotropicHardening * clone() const;
};

class IPPowerLawJ2IsotropicHardening : public IPJ2IsotropicHardening
{

 protected:
 public:
  IPPowerLawJ2IsotropicHardening(const double R0_);
  IPPowerLawJ2IsotropicHardening(const IPPowerLawJ2IsotropicHardening &source);
  virtual IPPowerLawJ2IsotropicHardening &operator=(const IPJ2IsotropicHardening &source);
  virtual ~IPPowerLawJ2IsotropicHardening(){}
  virtual void restart();
  virtual IPJ2IsotropicHardening *clone() const;

};

class IPExponentialJ2IsotropicHardening : public IPJ2IsotropicHardening
{

 protected:

 public:
  IPExponentialJ2IsotropicHardening(const double R0_);
  IPExponentialJ2IsotropicHardening(const IPExponentialJ2IsotropicHardening &source);
  virtual IPExponentialJ2IsotropicHardening &operator=(const IPJ2IsotropicHardening &source);
  virtual ~IPExponentialJ2IsotropicHardening(){}
  virtual void restart();
  virtual IPJ2IsotropicHardening * clone() const;

};

class IPSwiftJ2IsotropicHardening : public IPJ2IsotropicHardening
{

 protected:

 public:
  IPSwiftJ2IsotropicHardening(const double R0_);
  IPSwiftJ2IsotropicHardening(const IPSwiftJ2IsotropicHardening &source);
  virtual IPSwiftJ2IsotropicHardening &operator=(const IPJ2IsotropicHardening &source);
  virtual ~IPSwiftJ2IsotropicHardening(){}
  virtual void restart();
  virtual IPJ2IsotropicHardening * clone() const;

};

class IPLinearExponentialJ2IsotropicHardening : public IPJ2IsotropicHardening
{
 protected:

 public:
  IPLinearExponentialJ2IsotropicHardening(const double R0_);
  IPLinearExponentialJ2IsotropicHardening(const IPLinearExponentialJ2IsotropicHardening &source);
  virtual IPLinearExponentialJ2IsotropicHardening &operator=(const IPJ2IsotropicHardening &source);
  virtual ~IPLinearExponentialJ2IsotropicHardening(){}
  virtual void restart();
  virtual IPJ2IsotropicHardening * clone() const;

};



/* IPLinearFollowedByExponentialJ2IsotropicHardening */
class IPLinearFollowedByExponentialJ2IsotropicHardening : public IPJ2IsotropicHardening
{
 protected:
  // nothing to add to IPJ2IsotropicHardening basis
 public:
  IPLinearFollowedByExponentialJ2IsotropicHardening(const double R0_);
  IPLinearFollowedByExponentialJ2IsotropicHardening(const IPLinearFollowedByExponentialJ2IsotropicHardening &source);
  virtual IPLinearFollowedByExponentialJ2IsotropicHardening &operator=(const IPJ2IsotropicHardening &source);
  virtual ~IPLinearFollowedByExponentialJ2IsotropicHardening(){}
  virtual void restart();
  virtual IPJ2IsotropicHardening * clone() const;

};



/* IPLinearFollowedByPowerLawJ2IsotropicHardening */
class IPLinearFollowedByPowerLawJ2IsotropicHardening : public IPJ2IsotropicHardening
{

 protected:
 public:
  IPLinearFollowedByPowerLawJ2IsotropicHardening(const double R0_);
  IPLinearFollowedByPowerLawJ2IsotropicHardening(const IPLinearFollowedByPowerLawJ2IsotropicHardening &source);
  virtual IPLinearFollowedByPowerLawJ2IsotropicHardening &operator=(const IPJ2IsotropicHardening &source);
  virtual ~IPLinearFollowedByPowerLawJ2IsotropicHardening(){}
  virtual void restart();
  virtual IPJ2IsotropicHardening *clone() const;

};

class IPLinearFollowedByMultiplePowerLawJ2IsotropicHardening : public IPJ2IsotropicHardening
{

 protected:
 public:
  IPLinearFollowedByMultiplePowerLawJ2IsotropicHardening(const double R0_);
  IPLinearFollowedByMultiplePowerLawJ2IsotropicHardening(const IPLinearFollowedByMultiplePowerLawJ2IsotropicHardening &source);
  virtual IPLinearFollowedByMultiplePowerLawJ2IsotropicHardening &operator=(const IPJ2IsotropicHardening &source);
  virtual ~IPLinearFollowedByMultiplePowerLawJ2IsotropicHardening(){}
  virtual void restart();
  virtual IPJ2IsotropicHardening *clone() const;

};




class IPPolynomialJ2IsotropicHardening : public IPJ2IsotropicHardening{
  public:
    IPPolynomialJ2IsotropicHardening(const double R0_);
    IPPolynomialJ2IsotropicHardening(const IPPolynomialJ2IsotropicHardening &source);
    virtual IPPolynomialJ2IsotropicHardening &operator=(const IPJ2IsotropicHardening &source);
    virtual ~IPPolynomialJ2IsotropicHardening(){}
    virtual void restart();
    virtual IPJ2IsotropicHardening * clone() const;
};

class IPTwoExpJ2IsotropicHaderning: public IPJ2IsotropicHardening{
  public:
    IPTwoExpJ2IsotropicHaderning(const double R0_);
    IPTwoExpJ2IsotropicHaderning(const IPTwoExpJ2IsotropicHaderning &source);
    virtual IPTwoExpJ2IsotropicHaderning &operator=(const IPJ2IsotropicHardening &source);
    virtual ~IPTwoExpJ2IsotropicHaderning(){}
    virtual void restart();
    virtual IPJ2IsotropicHardening * clone() const;
};
#endif //IPHARDENING_H_

