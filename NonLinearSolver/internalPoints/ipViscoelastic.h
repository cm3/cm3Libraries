//
// Description: storing class for nonlinear viscoelasticity
//
//
// Author:  <Dongli Li, Antoine Jerusalem>, (C) 2014
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef IPViscoelastic_H_
#define IPViscoelastic_H_
#include "ipvariable.h"
#include "STensor3.h"
#include <vector>


class IPViscoelastic : public IPVariableMechanics
{

 public: // All data public to avoid the creation of function to access
	std::vector<STensor3> _Hn;
    STensor3 _Sn;
 public:
  IPViscoelastic();
  IPViscoelastic(const int N);
  IPViscoelastic(const IPViscoelastic &source);
  IPViscoelastic& operator=(const IPVariable &source);
  ~IPViscoelastic()
  {

  }

  virtual  IPVariable * clone() const;
  virtual void restart();
};

#endif // IPViscoelastic_H_

