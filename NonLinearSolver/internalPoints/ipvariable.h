//
// C++ Interface: ipvariable
//
// Description: Base class for ipvariable
//
//
// Author:  <Gauthier BECKER>, (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
//
// class with the variables of IP (stress, deformation and localBasis)

#ifndef IPVARIABLE_H_
#define IPVARIABLE_H_
#include <stdlib.h>
#include <stdio.h>
#include "GmshConfig.h"
#include "restartManager.h"
#include "STensor33.h"
#include "STensor43.h"

class nonLinearMechSolver;
class DeepMaterialNetwork;

class IPVariable
{
 public:
  enum LOCATION{UNDEFINED=0, BULK = 1, INTERFACE_MINUS = 2, INTERFACE_PLUS = 3, VIRTUAL_INTERFACE=4};
 protected:
  LOCATION _onWhat;
	nonLinearMechSolver* _microSolver; // each ipv is associated to a solver, NULL by default
  bool _isDeleted; //

 public :
  IPVariable():_onWhat(UNDEFINED),_microSolver(NULL),_isDeleted(false){}
  virtual ~IPVariable(){}
  // copie constructor
  IPVariable(const IPVariable &source):_onWhat(source._onWhat),_microSolver(source._microSolver),_isDeleted(source._isDeleted){};
  virtual IPVariable &operator=(const IPVariable &source){
    if(source._microSolver!=NULL)
      _microSolver = source._microSolver;
    _isDeleted = source._isDeleted;
    return *this;
  };
  virtual double get(const int i) const{return 0.;} // Allow to get any component defined in the ipvariable
  virtual double & getRef(const int i){ static double temp = 0.0; return temp; }; // Allow to get access to any component value defined in the ipvariable
  virtual void get(const std::vector<int>& comp, std::vector<double>& val) const;
 #if defined(HAVE_MPI)
  // by default in MPI there is no value that are communicate between rank.
  // but you can communicate values by filling these functions for your IPVariable
  virtual int numberValuesToCommunicateMPI()const{return 0;}
  virtual void fillValuesToCommunicateMPI(double *arrayMPI)const{};
  virtual void getValuesFromMPI(const double *arrayMPI){};
 #endif // HAVE_MPI
  virtual void restart(){
    restartManager::restart(_isDeleted);
  } // default don't save values for a restart

  virtual IPVariable* clone() const =0;

  virtual void Delete(const bool fl){_isDeleted = fl;}
  virtual bool isDeleted() const {return _isDeleted;}; // ipvariable is deleted under certain conditions

  virtual LOCATION getLocation() const {return _onWhat;};
  virtual void setLocation(const IPVariable::LOCATION loc);
  virtual void setValueForBodyForce(const STensor43& K, const int ModuliType)=0;
  virtual void computeBodyForce(const STensor33& G, SVector3& B) const=0;
  virtual const STensor43 &getConstRefToTangentModuli() const=0;
  
  virtual bool getConstitutiveSuccessFlag() const {return true;}; // true if contitutive law is successfully passed in GP 

   #if defined(HAVE_MPI)
   // using in multiscale analysis with MPI
  // function to communicate data at IPVariable level
  // In multiscale analysis, these functions allow sending all IPVariable from
  // one processor to other one
  virtual int getMacroNumberElementDataSendToMicroProblem() const {return 0;};
  // get number of values obtained by microscopic analysis to send to macroscopic analysis
  virtual int getMicroNumberElementDataSendToMacroProblem() const {return 0;};
   // get macroscopic kinematic data to send to microscopic problem
  virtual void getMacroDataSendToMicroProblem(double* val) const{};
  // get computed data obtaind by microscopic analysis to send to macroscopic analysis
  virtual void getMicroDataToMacroProblem(double* val) const {};
    // set the received data from microscopic analysis to microscopic analysis
  virtual void setReceivedMacroDataToMicroProblem(const double* val){};
    // set the received data from microscopic analysis to macroscopic analysis
  virtual void setReceivedMicroDataToMacroProblem(const double* val){};
  #endif

	virtual void setMicroSolver(nonLinearMechSolver* s) {_microSolver = s;};
	virtual nonLinearMechSolver* getMicroSolver() {return _microSolver;};
	virtual const nonLinearMechSolver* getMicroSolver() const {return _microSolver;};
  
  virtual bool withDeepMN() const{return false;};
  virtual void getAllDeepMNs(std::vector<DeepMaterialNetwork*>& allDMN) {}
};

class IPVariableMechanics : public IPVariable{
	protected:

 public:
  IPVariableMechanics(): IPVariable(){}
  IPVariableMechanics(const IPVariableMechanics &source) : IPVariable(source){};
  virtual IPVariableMechanics &operator=(const IPVariable &source);
  virtual ~IPVariableMechanics(){}
	virtual bool dissipationIsActive() const {return false;};

	virtual void blockDissipation(const bool bl){}
  virtual bool dissipationIsBlocked() const {return false;}

  virtual void brokenSolver(const bool bl){};
  virtual bool solverIsBroken() const {return false;};

  virtual double defoEnergy() const{return 0.;}
  virtual double plasticEnergy() const{return 0.;} // dissipation by plasticity
  virtual double damageEnergy() const {return 0.;}; // dissipation by damage
	virtual double irreversibleEnergy() const {return 0.;} // irreversible energy for path following
  // Default for fracture (a separate class cannot be defined in order to avoid a double derivation IPVariable2ForFracture)
  virtual int fractureEnergy(double* arrayEnergy) const{return 0;}
  virtual void setValueForBodyForce(const STensor43& K, const int ModuliType){ Msg::Error("setValueForBodyForce is not defined in IPVariableMechanics"); }
  virtual void computeBodyForce(const STensor33& G, SVector3& B) const { Msg::Error("ComputeBodyForce is not defined in IPVariableMechanics"); }
  virtual const STensor43 &getConstRefToTangentModuli() const {Msg::Error("getConstRefToTangentModuli IPVariableMechanics"); static STensor43 dummy; return dummy;};
  
  
  
  virtual void restart(){
    IPVariable::restart();
  }
};

template<class Tbulk,class Tfrac> class IPVariable2Enhanced : public Tbulk{
 protected:
  Tbulk *_ipvbulk;
  Tfrac *_ipvfrac;
 public:
  IPVariable2Enhanced() : Tbulk(),_ipvbulk(NULL), _ipvfrac(NULL){}
  IPVariable2Enhanced(const IPVariable2Enhanced &source) : Tbulk(source)
  {
    _ipvbulk = NULL;
    if (source._ipvbulk!= NULL){
      _ipvbulk = dynamic_cast<Tbulk*>(source._ipvbulk->clone());
    }
    _ipvfrac = NULL;
    if (source._ipvfrac!= NULL){
      _ipvfrac = dynamic_cast<Tfrac*>(source._ipvfrac->clone());
    }
  }
  virtual IPVariable2Enhanced& operator=(const IPVariable &source)
  {
    Tbulk::operator = (source);
    const IPVariable2Enhanced *ipvf =dynamic_cast<const IPVariable2Enhanced *> (&source);
    if (ipvf != NULL){
      // copy bulk ip
      if (_ipvbulk != NULL) {
        if (ipvf->getIPvBulk() != NULL){
          _ipvbulk->operator=(*(dynamic_cast<const IPVariable*>(ipvf->getIPvBulk())));
        }
        else{
          delete _ipvbulk;
          _ipvbulk = NULL;
        }
      }
      else{
        if (ipvf->getIPvBulk() != NULL){
          _ipvbulk = dynamic_cast<Tbulk*>(ipvf->getIPvBulk()->clone());
        }
      }

      // copy interface ip
      if (_ipvfrac != NULL) {
        if (ipvf->getIPvEnhanced() != NULL){
          _ipvfrac->operator=(*(dynamic_cast<const IPVariable*>(ipvf->getIPvEnhanced())));
        }
        else{
          delete _ipvfrac;
          _ipvfrac = NULL;
        }
      }
      else{
        if (ipvf->getIPvEnhanced() != NULL){
          _ipvfrac = dynamic_cast<Tfrac*>(ipvf->getIPvEnhanced()->clone());
        }
      }

    }
    return *this;
  }
  virtual ~IPVariable2Enhanced()
  {
    if(_ipvbulk != NULL){
      delete _ipvbulk; _ipvbulk = NULL;
    }
    if(_ipvfrac != NULL){
      delete _ipvfrac; _ipvfrac = NULL;
    }
  }

	virtual void setLocation(const IPVariable::LOCATION loc) {
		Tbulk::setLocation(loc);
		if (_ipvbulk!=NULL){
			_ipvbulk->setLocation(loc);
		}
		if (_ipvfrac!=NULL){
			_ipvfrac->setLocation(loc);
		}
	};

	virtual void setMicroSolver(nonLinearMechSolver* s) {
		Tbulk::setMicroSolver(s);
		if (_ipvbulk!=NULL){
			_ipvbulk->setMicroSolver(s);
		}
		if (_ipvfrac!=NULL){
			_ipvfrac->setMicroSolver(s);
		}
	};

  Tbulk* getIPvBulk(){return _ipvbulk;}
  const Tbulk* getIPvBulk() const{return _ipvbulk;}
	void setIPvBulk(IPVariable *ipv){ _ipvbulk = dynamic_cast<Tbulk*>(ipv);}

  Tfrac* getIPvEnhanced(){return _ipvfrac;}
  const Tfrac* getIPvEnhanced() const{return _ipvfrac;}
  void setIPvEnhanced(IPVariable *ipv){ _ipvfrac = dynamic_cast<Tfrac*>(ipv);}

  virtual void restart()
  {
    Tbulk::restart();
		if (_ipvbulk!=NULL){
			_ipvbulk->restart();
		}
		if (_ipvfrac!=NULL){
			_ipvfrac->restart();
		}
    return;
  }
  
  virtual bool getConstitutiveSuccessFlag() const
  {
    if (_ipvbulk!=NULL &&  _ipvfrac!=NULL)
    {
      return _ipvbulk->getConstitutiveSuccessFlag() and _ipvfrac->getConstitutiveSuccessFlag();
    } 
    else if (_ipvbulk!=NULL)
    {
      return _ipvbulk->getConstitutiveSuccessFlag();
    }
    else if (_ipvfrac!= NULL)
    {
      return _ipvfrac->getConstitutiveSuccessFlag();
    }
    else
    {
      return true;
    }
  }
  virtual IPVariable* clone() const =0;

	#if defined(HAVE_MPI)
// using in multiscale analysis with MPI

	int getMacroNumberElementDataSendToMicroProblem() const {
		if (_ipvbulk == NULL or _ipvfrac == NULL ) Msg::Error("IPVariable2Enhanced is not correctly initialized");
		int nb = _ipvbulk->getMacroNumberElementDataSendToMicroProblem();
		int nf = _ipvfrac->getMacroNumberElementDataSendToMicroProblem();
		// send
		return nb+nf;
	};

	int getMicroNumberElementDataSendToMacroProblem() const {
		if (_ipvbulk == NULL or _ipvfrac == NULL ) Msg::Error("IPVariable2Enhanced is not correctly initialized");
		int nb = _ipvbulk->getMicroNumberElementDataSendToMacroProblem();
		int nf = _ipvfrac->getMicroNumberElementDataSendToMacroProblem();
		return nb+nf;
	};
	void getMacroDataSendToMicroProblem(double* val) const {
		if (_ipvbulk == NULL or _ipvfrac == NULL ) Msg::Error("IPVariable2Enhanced is not correctly initialized");

		int nb = _ipvbulk->getMacroNumberElementDataSendToMicroProblem();
		double* bufferBulk = NULL;
		if (nb>0){
			bufferBulk = new double[nb];
			_ipvbulk->getMacroDataSendToMicroProblem(bufferBulk);
			for (int idex=0; idex < nb; idex ++){
				val[idex] = bufferBulk[idex];
			}
			delete[] bufferBulk;
		}

		int nf = _ipvfrac->getMacroNumberElementDataSendToMicroProblem();
		double *bufferInterface = NULL;
		if (nf >0){
			bufferInterface = new double[nf];
			_ipvfrac->getMacroDataSendToMicroProblem(bufferInterface);
			for (int idex=nb; idex < nb+nf; idex++){
				val[idex] = bufferInterface[idex-nb];
			}
			delete [] bufferInterface;
		}
	};

	void getMicroDataToMacroProblem(double* val) const {
		if (_ipvbulk == NULL or _ipvfrac == NULL ) Msg::Error("IPVariable2Enhanced is not correctly initialized");

		int nb = _ipvbulk->getMicroNumberElementDataSendToMacroProblem();
		if (nb>0){
			double* bufferBulk = new double[nb];
			_ipvbulk->getMicroDataToMacroProblem(bufferBulk);
			for (int idex=0; idex < nb; idex ++){
				val[idex] = bufferBulk[idex];
			}
			delete[] bufferBulk;
		}

		int nf = _ipvfrac->getMicroNumberElementDataSendToMacroProblem();
		if (nf >0){
			double* bufferInterface = new double[nf];
			_ipvfrac->getMicroDataToMacroProblem(bufferInterface);
			for (int idex=nb; idex < nb+nf; idex++){
				val[idex] = bufferInterface[idex-nb];
			}
			delete [] bufferInterface;
		}
	};
	void setReceivedMacroDataToMicroProblem(const double* val){
		if (_ipvbulk == NULL or _ipvfrac == NULL ) Msg::Error("IPVariable2Enhanced is not correctly initialized");

		int nb = _ipvbulk->getMacroNumberElementDataSendToMicroProblem();
		if (nb>0){
			double* bufferBulk = new double[nb];
			for (int i=0; i< nb; i++){
				bufferBulk[i] = val[i];
			}
			_ipvbulk->setReceivedMacroDataToMicroProblem(bufferBulk);
			delete[] bufferBulk;
		}

		int nf = _ipvfrac->getMacroNumberElementDataSendToMicroProblem();
		if (nf >0){
			double* bufferInterface = new double[nf];
			for (int i=nb; i< nb+nf; i++){
				bufferInterface[i-nb] = val[i];
			}
			_ipvfrac->setReceivedMacroDataToMicroProblem(bufferInterface);
			delete[] bufferInterface;
		}
	};

	void setReceivedMicroDataToMacroProblem(const double* val){
		if (_ipvbulk == NULL or _ipvfrac == NULL ) Msg::Error("IPVariable2Enhanced is not correctly initialized");

		int nb = _ipvbulk->getMicroNumberElementDataSendToMacroProblem();
		if (nb>0){
			double* bufferBulk = new double[nb];
			for (int i=0; i< nb; i++){
				bufferBulk[i] = val[i];
			}
			_ipvbulk->setReceivedMicroDataToMacroProblem(bufferBulk);
			delete[] bufferBulk;
		}

		int nf = _ipvfrac->getMicroNumberElementDataSendToMacroProblem();
		if (nf >0){
			double* bufferInterface = new double[nf];
			for (int i=nb; i< nb+nf; i++){
				bufferInterface[i-nb] = val[i];
			}
			_ipvfrac->setReceivedMicroDataToMacroProblem(bufferInterface);
			delete[] bufferInterface;
		}
	};
	#endif

};

class IPVariable2ForFractureBase {
	protected:
		bool _broken;
		double _onsetCriterion;

	public:
		IPVariable2ForFractureBase():_broken(false),_onsetCriterion(0.){}
		IPVariable2ForFractureBase(const IPVariable2ForFractureBase& src) : _broken(src._broken),_onsetCriterion(src._onsetCriterion){}
		virtual ~IPVariable2ForFractureBase(){};
		virtual IPVariable2ForFractureBase& operator=(const IPVariable& src){
			const IPVariable2ForFractureBase* ipvfracBase = dynamic_cast<const IPVariable2ForFractureBase*>(&src);
			if (ipvfracBase != NULL){
				_broken = ipvfracBase->_broken;
				_onsetCriterion = ipvfracBase->_onsetCriterion;
			}
			return *this;
		}
		virtual double getOnsetCriterion() const {return _onsetCriterion;};
		virtual double& getOnsetCriterion() {return _onsetCriterion;};
		virtual bool isbroken() const {return _broken;}
		virtual void broken(){_broken=true;}
		virtual void nobroken(){_broken=false;}
		virtual void restart(){
			restartManager::restart(_broken);
			restartManager::restart(_onsetCriterion);
		}
};


template<class Tbulk,class Tfrac> class IPVariable2ForFracture : public IPVariable2Enhanced<Tbulk,Tfrac>,
																																 public IPVariable2ForFractureBase{

 public:
  IPVariable2ForFracture() : IPVariable2Enhanced<Tbulk,Tfrac>(),IPVariable2ForFractureBase(){}
  IPVariable2ForFracture(const IPVariable2ForFracture &source) : IPVariable2Enhanced<Tbulk,Tfrac>(source),IPVariable2ForFractureBase(source)
  {

  }
  virtual IPVariable2ForFracture& operator=(const IPVariable &source)
  {
    IPVariable2Enhanced<Tbulk,Tfrac>::operator = (source);
		IPVariable2ForFractureBase::operator =(source);
    const IPVariable2ForFracture *ipvf =dynamic_cast<const IPVariable2ForFracture *> (&source);
    if (ipvf != NULL){

    }
    return *this;
  }
  virtual ~IPVariable2ForFracture()
  {

  }

	Tfrac* getIPvFrac(){return IPVariable2Enhanced<Tbulk,Tfrac>::getIPvEnhanced();}
  const Tfrac* getIPvFrac() const{return IPVariable2Enhanced<Tbulk,Tfrac>::getIPvEnhanced();}
  void setIPvFrac(IPVariable *ipv){ IPVariable2Enhanced<Tbulk,Tfrac>::setIPvEnhanced(ipv);}

 #if defined(HAVE_MPI)
  virtual int numberValuesToCommunicateMPI()const{return 0;}
  // has to return the number of values exactly as the previous one
  virtual void fillValuesToCommunicateMPI(double *arrayMPI)const{}
  virtual void getValuesFromMPI(const double *arrayMPI){}
 #endif // HAVE_MPI
  virtual void restart()
  {
    IPVariable2Enhanced<Tbulk,Tfrac>::restart();
		IPVariable2ForFractureBase::restart();

    return;
  }
  virtual int fractureEnergy(double* arrayEnergy) const=0;
  virtual IPVariable* clone() const =0;
};

#endif //IPVARIABLE_H_

