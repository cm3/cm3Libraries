//
// Description: storing class for kinematic hardening law
//
//
// Author:  <V.D. Nguyen>, (C) 2014
//
// Copyright: See COPYING file that comes with this distribution
//
//

#include "ipKinematicHardening.h"

IPKinematicHardening::IPKinematicHardening(): R(0), dR(0), ddR(0), dRdT(0.), ddRdTT(0.), ddRdT(0.), _isSaturated(false)
{

}

IPKinematicHardening::IPKinematicHardening(const IPKinematicHardening &source)
{
  R      = source.R;
  dR     = source.dR;
  ddR    = source.ddR;
  dRdT   = source.dRdT;
  ddRdTT   = source.ddRdTT;
  ddRdT   = source.ddRdT;
  _isSaturated= source._isSaturated;
}

IPKinematicHardening &IPKinematicHardening::operator=(const IPKinematicHardening &source)
{
  R      = source.R;
  dR     = source.dR;
  ddR    = source.ddR;
  dRdT   = source.dRdT;
  ddRdTT   = source.ddRdTT;
  ddRdT   = source.ddRdT;
  _isSaturated = source._isSaturated;
  return *this;
}

void IPKinematicHardening::restart()
{
  restartManager::restart(R);
  restartManager::restart(dR);
  restartManager::restart(ddR);
  restartManager::restart(dRdT);
  restartManager::restart(ddRdTT);
  restartManager::restart(ddRdT);
  restartManager::restart(_isSaturated);
  return;
}
