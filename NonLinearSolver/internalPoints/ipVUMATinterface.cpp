//
// Description: storing class for VUMAT interface law
//
//
// Author:  <Antoine JERUSALEM>, (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "ipVUMATinterface.h"
#include "matrix_operations.h"
#include "STensor3.h"
#include "restartManager.h"
IPVUMATinterface::IPVUMATinterface(int _nsdv, double _size) : IPVariableMechanics(), _vumatNbStatev(_nsdv), _elementSize(_size),
                                                              _internalEnergy(0.), _inelasticEnergy(0.) {
  mallocvector(&_vumatStatev,_vumatNbStatev);
};
IPVUMATinterface::IPVUMATinterface(const IPVUMATinterface &source) : IPVariableMechanics(source), _vumatNbStatev(source._vumatNbStatev),
								     _elementSize(source._elementSize), _internalEnergy(source._internalEnergy),
                                                                     _inelasticEnergy(source._inelasticEnergy){
  mallocvector(&_vumatStatev,_vumatNbStatev);
  copyvect(source._vumatStatev,_vumatStatev,_vumatNbStatev);
}
IPVUMATinterface& IPVUMATinterface::operator=(const IPVariable &source)
{
  IPVariableMechanics::operator=(source);
  const IPVUMATinterface* src = dynamic_cast<const IPVUMATinterface*>(&source);
	if (src != NULL){
		_vumatNbStatev   = src->_vumatNbStatev;
		_elementSize     = src->_elementSize;
		_internalEnergy  = src->_internalEnergy;
		_inelasticEnergy = src->_inelasticEnergy;
		if(_vumatNbStatev !=src->_vumatNbStatev) Msg::Error("VUMATs do not have the same number of internal variables");
		copyvect(src->_vumatStatev,_vumatStatev,_vumatNbStatev);
	}
	return *this;
}

double IPVUMATinterface::get(int comp) const
{
  if (comp >= 100 && comp < (100+_vumatNbStatev) )
    return _vumatStatev[comp-100];
  else
    return 0.;
}

double IPVUMATinterface::defoEnergy() const
{
  return _internalEnergy;

}
double IPVUMATinterface::plasticEnergy() const
{
  return _inelasticEnergy;

}

void IPVUMATinterface::restart()
{
  IPVariableMechanics::restart();
  restartManager::restart(_vumatNbStatev);
  restartManager::restart(_elementSize);
  restartManager::restart(_vumatStatev,_vumatNbStatev);
  restartManager::restart(_internalEnergy);
  restartManager::restart(_inelasticEnergy);
  return;
}
