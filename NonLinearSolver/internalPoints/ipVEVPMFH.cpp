/*********************************************************************************************/
/** Copyright (C) 2022 - See COPYING file that comes with this distribution	                **/
/** Author: Mohamed HADDAD (MEMA-iMMC-UCLouvain)							                              **/
/** Contact: mohamed.haddad@uclouvain.be					                                      		**/
/**											                                                                  	**/
/** Description: storing class of IP variabales for VE-VP material law (small strains)		  **/
/** VE-VP material law : See B.Miled et I.Doghri 2011                                       **/
/**********************************************************************************************/

#include "ipVEVPMFH.h"
#include "ipField.h"
#include "restartManager.h"


double IPVEVPMFH::defoEnergy() const{
  return _elasticEne;
}

double IPVEVPMFH::plasticEnergy() const{
  return _plasticEne;
}

double IPVEVPMFH::get(int comp) const{
  // Cauchy stress 
  if(comp == IPField::SIG_XX)			
	  return _state.strs[0];
  else if(comp == IPField::SIG_YY)
	  return _state.strs[1];
  else if(comp == IPField::SIG_ZZ)
	  return _state.strs[2];	  
  else if(comp==IPField::SIG_XY) 
	  return (_state.strs[3] / sqrt(2));
  else if(comp==IPField::SIG_XZ) 
	  return (_state.strs[5] / sqrt(2));
  else if(comp==IPField::SIG_YZ) 
	  return (_state.strs[4] / sqrt(2));
  
  // Von Mises equivelent stress
  else if(comp == IPField::SVM){	
     double svm= VEVPMFHSPACE::vonmises(_state.strs);
     return svm;
  }
  
  // Plastic true strain  
  else if(comp == IPField::PLASTICSTRAIN) 	
     return _state.p;

  // Total true strain    
  else if(comp == IPField::STRAIN_XX) 	
	  return _state.strn[0];
  else if(comp == IPField::STRAIN_YY) 
	  return _state.strn[1];
  else if(comp == IPField::STRAIN_ZZ) 
	  return _state.strn[2];
  else if(comp == IPField::STRAIN_XY)
	  return _state.strn[3]/sqrt(2);
  else if(comp == IPField::STRAIN_XZ)
	  return _state.strn[5]/sqrt(2);
  else if(comp == IPField::STRAIN_YZ)
	  return _state.strn[4]/sqrt(2);
  else
     return 0.;
}

void IPVEVPMFH::restart(){
  
  IPVariableMechanics::restart();
  
  restartManager::restart(_state.p); 
  restartManager::restart(_state.strs,6);
  restartManager::restart(_state.strn,6);
  restartManager::restart(_state.pstrn,6);
  

  restartManager::restart(_elasticEne);
  restartManager::restart(_plasticEne);

  restartManager::restart(_irreversibleEnergy);
  restartManager::restart(_DirreversibleEnergyDF);
  
  return;
}

