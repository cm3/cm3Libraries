//
//
// Description: define the IP data for finite strain cases
//
// Author:  <Van Dung NGUYEN>, (C) 2012
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "ipFiniteStrain.h"
#include "highOrderTensor.h"

ipFiniteStrain::ipFiniteStrain():_GPData(NULL),_interfaceGPData(NULL), IPVariableMechanics(), _tag(0){}
ipFiniteStrain::ipFiniteStrain(const ipFiniteStrain &source): IPVariableMechanics(source), _GPData(NULL),_interfaceGPData(NULL),_tag(source._tag){
};
ipFiniteStrain &ipFiniteStrain::operator=(const IPVariable &source){
	// _GPData stores data of shape function, it is never copied
  IPVariableMechanics::operator=(source);
  const ipFiniteStrain* psrc = dynamic_cast<const ipFiniteStrain*>(&source);
  if (psrc != NULL)
  {
    _tag = psrc->_tag;
  }
  return *this;
};
ipFiniteStrain::~ipFiniteStrain(){
  if(_GPData!= NULL){
    delete _GPData;
    _GPData = NULL;
  }
	if (_interfaceGPData!=NULL){
		delete _interfaceGPData;
		_interfaceGPData = NULL;
	}
}
  // get shape function information from ipvariable
std::vector<TensorialTraits<double>::ValType>& ipFiniteStrain::f(const FunctionSpaceBase* space, MElement* ele, IntPt &GP) const {
  if (_GPData == NULL){
		_GPData = new IntPtData<double>();
	};
  return _GPData->f(space,ele,GP);
};
std::vector<TensorialTraits<double>::GradType>& ipFiniteStrain::gradf(const FunctionSpaceBase* space, MElement* ele, IntPt &GP) const {
  if (_GPData == NULL){
		_GPData = new IntPtData<double>();
	};
	return _GPData->gradf(space,ele,GP);
};

std::vector<TensorialTraits<double>::HessType>& ipFiniteStrain::hessf(const FunctionSpaceBase* space, MElement* ele, IntPt &GP) const {
  if (_GPData == NULL){
		_GPData = new IntPtData<double>();
	};
	return _GPData->hessf(space,ele,GP);
};

std::vector<TensorialTraits<double>::ThirdDevType>& ipFiniteStrain::thirdDevf(const FunctionSpaceBase* space, MElement* ele, IntPt &GP) const {
  if (_GPData == NULL){
		_GPData = new IntPtData<double>();
	};
	return _GPData->thirdDevf(space,ele,GP);
};

std::vector<CurlTraits<double>::CurlType>& ipFiniteStrain::fcurl(const FunctionSpaceBase* space, MElement* ele, IntPt &GP, const std::string &typeFunction) const
{
  if (_GPData == NULL)
  {
    _GPData = new IntPtData<double>();
  }
  if (typeFunction == "HcurlLegendre")
    return _GPData->fcurl(space,ele,GP,typeFunction);
  else
  {
    Msg::Error("Wrong function invoked: use Curlfcurl() instead");
    static std::vector<CurlTraits<double>::CurlType> tmp;
    return tmp;
  }
}

std::vector<CurlTraits<double>::CurlCurlType>& ipFiniteStrain::Curlfcurl(const FunctionSpaceBase* space, MElement* ele, IntPt &GP, const std::string &typeFunction) const
{
  if (_GPData == NULL)
  {
    _GPData = new IntPtData<double>();
  }
  if (typeFunction == "CurlHcurlLegendre")
    return _GPData->Curlfcurl(space,ele,GP,typeFunction);
  else
  {
    Msg::Error("Wrong function invoked: use fcurl() instead");
    static std::vector<CurlTraits<double>::CurlCurlType> tmp;
    return tmp;
  }
}

double& ipFiniteStrain::getJacobianDeterminant(MElement* ele, IntPt &GP) const{
  if (_GPData == NULL){
		_GPData = new IntPtData<double>();
	};
	return _GPData->getJacobianDeterminant(ele,GP);
};


std::vector<TensorialTraits<double>::ValType>& ipFiniteStrain::f(const FunctionSpaceBase* space, MInterfaceElement* ele, IntPt &GP) const {
  if (_interfaceGPData == NULL){
		_interfaceGPData = new IntPtData<double>();
	};
	MElement* e = dynamic_cast<MElement*>(ele);
  return _interfaceGPData->f(space,e,GP);
};
std::vector<TensorialTraits<double>::GradType>& ipFiniteStrain::gradf(const FunctionSpaceBase* space, MInterfaceElement* ele, IntPt &GP) const {
  if (_interfaceGPData == NULL){
		_interfaceGPData = new IntPtData<double>();
	};
	MElement* e = dynamic_cast<MElement*>(ele);
	return _interfaceGPData->gradf(space,e,GP);
};

std::vector<TensorialTraits<double>::HessType>& ipFiniteStrain::hessf(const FunctionSpaceBase* space, MInterfaceElement* ele, IntPt &GP) const {
  if (_interfaceGPData == NULL){
		_interfaceGPData = new IntPtData<double>();
	};
	MElement* e = dynamic_cast<MElement*>(ele);
	return _interfaceGPData->hessf(space,e,GP);
};

std::vector<TensorialTraits<double>::ThirdDevType>& ipFiniteStrain::thirdDevf(const FunctionSpaceBase* space, MInterfaceElement* ele, IntPt &GP) const {
  if (_interfaceGPData == NULL){
		_interfaceGPData = new IntPtData<double>();
	};
	MElement* e = dynamic_cast<MElement*>(ele);
	return _interfaceGPData->thirdDevf(space,e,GP);
};

std::vector<CurlTraits<double>::CurlType>& ipFiniteStrain::fcurl(const FunctionSpaceBase* space, MInterfaceElement* ele, IntPt &GP, const std::string & typeFunction) const
{
  if(_interfaceGPData == NULL)
  {
    _interfaceGPData = new IntPtData<double>();
  }
  MElement* e = dynamic_cast<MElement*>(ele);
  if (typeFunction == "HcurlLegendre")
    return _interfaceGPData->fcurl(space,e,GP,typeFunction);
  else
  {
    Msg::Error("Wrong function invoked; use Curlfcurl() instead");
    static std::vector<CurlTraits<double>::CurlType> tmp;
    return tmp;  
  }
}

std::vector<CurlTraits<double>::CurlCurlType>& ipFiniteStrain::Curlfcurl(const FunctionSpaceBase* space, MInterfaceElement* ele, IntPt &GP, const std::string & typeFunction) const
{
  if(_interfaceGPData == NULL)
  {
    _interfaceGPData = new IntPtData<double>();
  }
  MElement* e = dynamic_cast<MElement*>(ele);
  if (typeFunction == "CurlHcurlLegendre")
    return _interfaceGPData->Curlfcurl(space,e,GP,typeFunction);
  else
  {
    Msg::Error("Wrong function invoked; use fcurl() instead");
    static std::vector<CurlTraits<double>::CurlCurlType> tmp;
    return tmp;
  }
}

double& ipFiniteStrain::getJacobianDeterminant(MInterfaceElement* ele, IntPt &GP) const{
  if (_interfaceGPData == NULL){
		_interfaceGPData = new IntPtData<double>();
	};
	MElement* e = dynamic_cast<MElement*>(ele);
	return _interfaceGPData->getJacobianDeterminant(e,GP);
};
