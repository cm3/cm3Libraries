//
// Description: storing class for transverse Isotropic law it is a "virtual implementation" you have to define an ipvariable
//              in your project which derive from this one. (all data in this ipvarible have name beggining by _j2l...)
//              so don't do the same in your project...
// Author:  <L. Noels>, (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef IPTRANSVERSEISOTROPICCURV_H_
#define IPTRANSVERSEISOTROPICCURV_H_
#include "ipvariable.h"
#include "STensor3.h"
#include "ipTransverseIsotropic.h"
class IPTransverseIsoCurvature : public IPTransverseIsotropic
{
 protected:
  SVector3 _A;
 public:
  IPTransverseIsoCurvature(const SVector3 &GaussP, const SVector3 &Centroid, const SVector3 &PointStart1, const SVector3 &PointStart2, const double init_Angle);
  IPTransverseIsoCurvature(const IPTransverseIsoCurvature &source);
  virtual IPTransverseIsoCurvature& operator=(const IPVariable &source);
  virtual void getTransverseDirection(SVector3 &A) const { A =_A ;};
  virtual void restart();
  virtual IPVariable* clone() const {return new IPTransverseIsoCurvature(*this);};
 
 private:
  IPTransverseIsoCurvature();
};

#endif // IPTRANSVERSEISOTROPICCURV_H_
