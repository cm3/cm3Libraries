//
// Description: storing class for nonlinear viscoelasticity
//
//
// Author:  <Dongli Li, Antoine Jerusalem>, (C) 2013
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "ipViscoelastic.h"
#include <iostream>
#include <fstream>
#include <iomanip>
#include <vector>
#include "restartManager.h"
IPViscoelastic::IPViscoelastic() : IPVariableMechanics(), _Hn(std::vector<STensor3>(0)),_Sn(0.)

{

};

IPViscoelastic::IPViscoelastic(const int N) : IPVariableMechanics(),_Sn(0.)
{

    _Hn.resize(N,STensor3(0.)); //initialize the size of the matrix vectors
}

IPViscoelastic::IPViscoelastic(const IPViscoelastic &source) : IPVariableMechanics(source), _Hn(source._Hn),_Sn(source._Sn)
{

};
IPViscoelastic& IPViscoelastic::operator=(const IPVariable &source)
{
    IPVariableMechanics::operator=(source);
    const IPViscoelastic* src = dynamic_cast<const IPViscoelastic*>(&source);
    if(src!=NULL)
    {
        _Hn = src->_Hn;
        _Sn = src->_Sn;
    }
    return *this;
}

IPVariable * IPViscoelastic::clone() const
{

    return new IPViscoelastic(*this);
}

void IPViscoelastic::restart()
{
  IPVariableMechanics::restart();
  size_t sizevec = _Hn.size();
  restartManager::restart(sizevec); // write the size or get the saved one
  _Hn.resize(sizevec); // without effect for write
  for(size_t i=0; i<sizevec;++i)
  {
    restartManager::restart(_Hn[i]);
  }
  restartManager::restart(_Sn);
  return;
}
