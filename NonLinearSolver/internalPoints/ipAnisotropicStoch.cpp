//
// Description: storing class for j2 linear elasto-plastic law
//
//
// Author:  <Gauthier BECKER>, (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "ipAnisotropicStoch.h"
#include "restartManager.h"
#include "fullMatrix.h"
#include <math.h>
IPAnisotropicStoch::IPAnisotropicStoch() : IPVariableMechanics() {Msg::Error("Cannot construct default IPAnisotropicStoch");}

IPAnisotropicStoch::IPAnisotropicStoch(const IPAnisotropicStoch &source) : IPVariableMechanics(source), _elasticEnergy(source._elasticEnergy), 
                                       Ex(source.Ex), Ey(source.Ey), Ez(source.Ez), Vxy(source.Vxy), Vxz(source.Vxz), Vyz(source.Vyz), 
                                       MUxy(source.MUxy), MUxz(source.MUxz), MUyz(source.MUyz){}
IPAnisotropicStoch& IPAnisotropicStoch::operator=(const IPVariable &source)
{
  IPVariableMechanics::operator=(source);
  const IPAnisotropicStoch* src = dynamic_cast<const IPAnisotropicStoch*>(&source);
	if (src != NULL){
	    _elasticEnergy = src->_elasticEnergy;
            Ex = src->Ex;
            Ey = src->Ey;
            Ez = src->Ez;
            Vxy = src->Vxy;
            Vxz = src->Vxz;
            Vyz = src->Vyz;
            MUxy = src->MUxy;
            MUxz = src->MUxz;
            MUyz = src->MUyz;
	}
	return *this;
}
void IPAnisotropicStoch::restart()
{
  IPVariableMechanics::restart();
  restartManager::restart(_elasticEnergy);
  restartManager::restart(Ex);
  restartManager::restart(Ey);
  restartManager::restart(Ez);
  restartManager::restart(Vxy);
  restartManager::restart(Vxz);
  restartManager::restart(Vyz);
  restartManager::restart(MUxy);
  restartManager::restart(MUxz);
  restartManager::restart(MUyz);
  return;
}

IPAnisotropicStoch::IPAnisotropicStoch(const SVector3 &GaussP, const fullMatrix<double> &ExMat, const fullMatrix<double> &EyMat, 
                                       const fullMatrix<double> &EzMat, const fullMatrix<double> &VxyMat, const fullMatrix<double> &VxzMat, 
                                       const fullMatrix<double> &VyzMat, const fullMatrix<double> &MUxyMat, const fullMatrix<double> &MUxzMat, 
                                       const fullMatrix<double> &MUyzMat, const double dx, const double dy, const double OrigX, const double OrigY,
                                       const int intpl ): 
                                       IPVariableMechanics(), _elasticEnergy(0.) {

  double param_x = (GaussP(0)-OrigX)/dx;
  double param_y = (GaussP(1)-OrigY)/dy;

  double x, y;
  double fnx, fny;
  x = modf(param_x , &fnx);
  y = modf(param_y , &fny);
  int nx = (int)fnx;
  int ny = (int)fny; 

  if (intpl == 1){ 
     Ex = ExMat(nx,ny)*(1.0-x)*(1.0-y)+ ExMat(nx+1,ny)*x*(1.0-y)+ ExMat(nx,ny+1)*(1.0-x)*y+ ExMat(nx+1,ny+1)*x*y;
     Ey = EyMat(nx,ny)*(1.0-x)*(1.0-y)+ EyMat(nx+1,ny)*x*(1.0-y)+ EyMat(nx,ny+1)*(1.0-x)*y+ EyMat(nx+1,ny+1)*x*y;
     Ez = EzMat(nx,ny)*(1.0-x)*(1.0-y)+ EzMat(nx+1,ny)*x*(1.0-y)+ EzMat(nx,ny+1)*(1.0-x)*y+ EzMat(nx+1,ny+1)*x*y;

     Vxy = VxyMat(nx,ny)*(1.0-x)*(1.0-y)+ VxyMat(nx+1,ny)*x*(1.0-y)+ VxyMat(nx,ny+1)*(1.0-x)*y+ VxyMat(nx+1,ny+1)*x*y;
     Vxz = VxzMat(nx,ny)*(1.0-x)*(1.0-y)+ VxzMat(nx+1,ny)*x*(1.0-y)+ VxzMat(nx,ny+1)*(1.0-x)*y+ VxzMat(nx+1,ny+1)*x*y;
     Vyz = VyzMat(nx,ny)*(1.0-x)*(1.0-y)+ VyzMat(nx+1,ny)*x*(1.0-y)+ VyzMat(nx,ny+1)*(1.0-x)*y+ VyzMat(nx+1,ny+1)*x*y;

     MUxy = MUxyMat(nx,ny)*(1.0-x)*(1.0-y)+ MUxyMat(nx+1,ny)*x*(1.0-y)+ MUxyMat(nx,ny+1)*(1.0-x)*y+ MUxyMat(nx+1,ny+1)*x*y;
     MUxz = MUxzMat(nx,ny)*(1.0-x)*(1.0-y)+ MUxzMat(nx+1,ny)*x*(1.0-y)+ MUxzMat(nx,ny+1)*(1.0-x)*y+ MUxzMat(nx+1,ny+1)*x*y;
     MUyz = MUyzMat(nx,ny)*(1.0-x)*(1.0-y)+ MUyzMat(nx+1,ny)*x*(1.0-y)+ MUyzMat(nx,ny+1)*(1.0-x)*y+ MUyzMat(nx+1,ny+1)*x*y; 
  }
  else if (intpl ==2 ){
     if(x >= 0.5){nx=nx+1;}
     if(y >= 0.5){ny=ny+1;}

     Ex = ExMat(nx,ny);
     Ey = EyMat(nx,ny);
     Ez = EzMat(nx,ny);

     Vxy = VxyMat(nx,ny);
     Vxz = VxzMat(nx,ny);
     Vyz = VyzMat(nx,ny);

     MUxy = MUxyMat(nx,ny);
     MUxz = MUxzMat(nx,ny);
     MUyz = MUyzMat(nx,ny);
   }

  else if (intpl == 3 ){
     double alpha = 0.1;
     double min = 0.001;
     double max = 0.999;
     if(x > 0.5){nx=nx+1;}
     if(y > 0.5){ny=ny+1;}

     if (x <=min or x >= max){
         if (y <=min or y >= max){
            Ex = ExMat(nx,ny);
            Ey = EyMat(nx,ny);
            Ez = EzMat(nx,ny);

            Vxy = VxyMat(nx,ny);
            Vxz = VxzMat(nx,ny);
            Vyz = VyzMat(nx,ny);

            MUxy = MUxyMat(nx,ny);
            MUxz = MUxzMat(nx,ny);
            MUyz = MUyzMat(nx,ny);
         }
         else{
             int ny0; 
             if( y >0.5 ){
                 ny0 = ny-1;
                 if(ny0 < 0){ny0=0;}
              }
             if( y <=0.5){
                 ny0 = ny;
                 ny = ny+1;
              }   
            double coef = 0.5*(1.0+tanh((y-0.5)/alpha));
            Ex = ExMat(nx,ny0) + (ExMat(nx,ny)-ExMat(nx,ny0))*coef;
            Ey = EyMat(nx,ny0) + (EyMat(nx,ny)-EyMat(nx,ny0))*coef;
            Ez = EzMat(nx,ny0) + (EzMat(nx,ny)-EzMat(nx,ny0))*coef;

            Vxy = VxyMat(nx,ny0) + (VxyMat(nx,ny)-VxyMat(nx,ny0))*coef;
            Vxz = VxzMat(nx,ny0) + (VxyMat(nx,ny)-VxyMat(nx,ny0))*coef;
            Vyz = VyzMat(nx,ny0) + (VyzMat(nx,ny)-VyzMat(nx,ny0))*coef;

            MUxy = MUxyMat(nx,ny0) + (MUxyMat(nx,ny)-MUxyMat(nx,ny0))*coef;
            MUxz = MUxzMat(nx,ny0) + (MUxzMat(nx,ny)-MUxzMat(nx,ny0))*coef;
            MUyz = MUyzMat(nx,ny0) + (MUyzMat(nx,ny)-MUyzMat(nx,ny0))*coef;
         }
     }
     else{
         if (y <=min or y >= max){
            int nx0;
            if( x >0.5 ){
                nx0 = nx-1;
                if(nx0 < 0){nx0=0;}
            }
            if( x <=0.5){
                nx0 = nx;
                nx = nx+1;
            } 
            double coef = 0.5*(1.0+tanh((x-0.5)/alpha));
            Ex = ExMat(nx0,ny) + (ExMat(nx,ny)-ExMat(nx0,ny))*coef;
            Ey = EyMat(nx0,ny) + (EyMat(nx,ny)-EyMat(nx0,ny))*coef;
            Ez = EzMat(nx0,ny) + (EzMat(nx,ny)-EzMat(nx0,ny))*coef;

            Vxy = VxyMat(nx0,ny) + (VxyMat(nx,ny)-VxyMat(nx0,ny))*coef;
            Vxz = VxzMat(nx0,ny) + (VxyMat(nx,ny)-VxyMat(nx0,ny))*coef;
            Vyz = VyzMat(nx0,ny) + (VyzMat(nx,ny)-VyzMat(nx0,ny))*coef;

            MUxy = MUxyMat(nx0,ny) + (MUxyMat(nx,ny)-MUxyMat(nx0,ny))*coef;
            MUxz = MUxzMat(nx0,ny) + (MUxzMat(nx,ny)-MUxzMat(nx0,ny))*coef;
            MUyz = MUyzMat(nx0,ny) + (MUyzMat(nx,ny)-MUyzMat(nx0,ny))*coef;
         }
         else{  
            int nx0, ny0;
            if( x >0.5 ){
                nx0 = nx-1;
                if(nx0 < 0){nx0=0;}
            }
            if( x <=0.5){
                nx0 = nx;
                nx = nx+1;
            } 
            if( y >0.5 ){
                ny0 = ny-1;
                if(ny0 < 0){ny0=0;}
            }
            if( y <=0.5){
                ny0 = ny;
                ny = ny+1;
            }  
            double cx,cy,s,t;
            cx = 0.5*(1.0+tanh((x-0.5)/alpha));
            cy = 0.5*(1.0+tanh((y-0.5)/alpha));
            s = (x-min)/(max-min); 
            t = (y-min)/(max-min); 
            double c00 = (1.0-cx)*(1.0-t) + (1.0-cy)*(1.0-s) - (1.0-s)*(1.0-t);
            double c01 = (1.0-cx)*t + cy*(1.0-s) - (1.0-s)*t;
            double c10 = cx*(1.0-t) + (1.0-cy)*s - s*(1.0-t);
            double c11 = cx*t + cy*s - s*t;

            Ex = c00*ExMat(nx0,ny0) + c01*ExMat(nx0,ny) + c10*ExMat(nx,ny0) + c11*ExMat(nx,ny);
            Ey = c00*EyMat(nx0,ny0) + c01*EyMat(nx0,ny) + c10*EyMat(nx,ny0) + c11*EyMat(nx,ny);
            Ez = c00*EzMat(nx0,ny0) + c01*EzMat(nx0,ny) + c10*EzMat(nx,ny0) + c11*EzMat(nx,ny);

            Vxy = c00*VxyMat(nx0,ny0) + c01*VxyMat(nx0,ny) + c10*VxyMat(nx,ny0) + c11*VxyMat(nx,ny);
            Vxz = c00*VxzMat(nx0,ny0) + c01*VxzMat(nx0,ny) + c10*VxzMat(nx,ny0) + c11*VxzMat(nx,ny);
            Vyz = c00*VyzMat(nx0,ny0) + c01*VyzMat(nx0,ny) + c10*VyzMat(nx,ny0) + c11*VyzMat(nx,ny);

            MUxy = c00*MUxyMat(nx0,ny0) + c01*MUxyMat(nx0,ny) + c10*MUxyMat(nx,ny0) + c11*MUxyMat(nx,ny);
            MUxz = c00*MUxzMat(nx0,ny0) + c01*MUxzMat(nx0,ny) + c10*MUxzMat(nx,ny0) + c11*MUxzMat(nx,ny);
            MUyz = c00*MUyzMat(nx0,ny0) + c01*MUyzMat(nx0,ny) + c10*MUyzMat(nx,ny0) + c11*MUyzMat(nx,ny);
         }
     }
   }

   else {
     printf("Unknown interpolation method: %d\n", intpl);
   }			
}
double IPAnisotropicStoch::defoEnergy() const
{
  return _elasticEnergy;
}

