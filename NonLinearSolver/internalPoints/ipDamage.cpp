//
// Description: storing class for non Local Damage
//
//
// Author:  <L. Noels>, (C) 2013
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "ipDamage.h"
#include "restartManager.h"
IPDamage::IPDamage(const double pinit): IPVariable(), D(0), maximalp (pinit), DeltaD (0), dDdp(0), dDdFe(0),initp(pinit)
{
}

IPDamage::IPDamage(const IPDamage &source) : IPVariable(source), D(source.D), maximalp(source.maximalp), 
  DeltaD(source.DeltaD), dDdp(source.dDdp), dDdFe(source.dDdFe),
  initp(source.initp){
}

IPDamage &IPDamage::operator=(const IPVariable &source)
{
  IPVariable::operator=(source);
  const IPDamage* src = dynamic_cast<const IPDamage*>(&source);
  if(src!=NULL)
  {
   D        = src->D;
   initp    = src->initp;
   maximalp = src->maximalp;
   DeltaD   = src->DeltaD;
   dDdp     = src->dDdp;
   dDdFe    = src->dDdFe;
  }
  return *this;
}

void IPDamage::restart()
{
  IPVariable::restart();
  restartManager::restart(D);
  restartManager::restart(initp);
  restartManager::restart(maximalp);
  restartManager::restart(DeltaD);
  restartManager::restart(dDdp);
  restartManager::restart(dDdFe);
  return;
}

IPLemaitreChabocheDamage::IPLemaitreChabocheDamage(const double pinit) : IPDamage(pinit)
{

}
IPLemaitreChabocheDamage::IPLemaitreChabocheDamage(const IPLemaitreChabocheDamage &source) :IPDamage(source)
{

}

IPLemaitreChabocheDamage &IPLemaitreChabocheDamage::operator=(const IPVariable &source)
{
  IPDamage::operator=(source);
  const IPLemaitreChabocheDamage* src = dynamic_cast<const IPLemaitreChabocheDamage*>(&source);
  if(src!=NULL)
  {
  }
  return *this;
}
void IPLemaitreChabocheDamage::restart()
{
  IPDamage::restart();
}


IPVariable *IPLemaitreChabocheDamage::clone() const
{
  return new IPLemaitreChabocheDamage(*this);
}


IPPowerLaw::IPPowerLaw(const double pinit) : IPDamage(pinit)
{

}
IPPowerLaw::IPPowerLaw(const IPPowerLaw &source) :IPDamage(source)
{

}

IPPowerLaw &IPPowerLaw::operator=(const IPVariable &source)
{
  IPDamage::operator=(source);
  const IPPowerLaw* src = dynamic_cast<const IPPowerLaw*>(&source);
  if(src!=NULL)
  {
  }
  return *this;
}
void IPPowerLaw::restart()
{
  IPDamage::restart();
}

IPVariable *IPPowerLaw::clone() const
{
  return new IPPowerLaw(*this);
}


IPExponentialDamageLaw::IPExponentialDamageLaw(const double pinit) : IPDamage(pinit)
{

}
IPExponentialDamageLaw::IPExponentialDamageLaw(const IPExponentialDamageLaw &source) :IPDamage(source)
{

}

IPExponentialDamageLaw &IPExponentialDamageLaw::operator=(const IPVariable &source)
{
  IPDamage::operator=(source);
  const IPExponentialDamageLaw* src = dynamic_cast<const IPExponentialDamageLaw*>(&source);
  if(src!=NULL)
  {
  }
  return *this;
}
void IPExponentialDamageLaw::restart()
{
  IPDamage::restart();
}

IPVariable *IPExponentialDamageLaw::clone() const
{
  return new IPExponentialDamageLaw(*this);
}

//
IPSigmoidDamageLaw::IPSigmoidDamageLaw(const double pinit) : IPDamage(pinit)
{

}
IPSigmoidDamageLaw::IPSigmoidDamageLaw(const IPSigmoidDamageLaw &source) :IPDamage(source)
{

}

IPSigmoidDamageLaw &IPSigmoidDamageLaw::operator=(const IPVariable &source)
{
  IPDamage::operator=(source);
  const IPSigmoidDamageLaw* src = dynamic_cast<const IPSigmoidDamageLaw*>(&source);
  if(src!=NULL)
  {
  }
  return *this;
}
void IPSigmoidDamageLaw::restart()
{
  IPDamage::restart();
}

IPVariable *IPSigmoidDamageLaw::clone() const
{
  return new IPSigmoidDamageLaw(*this);
}

//

IPThreeParametersExponentialDamageLaw::IPThreeParametersExponentialDamageLaw(const double pinit) : IPDamage(pinit)
{

}
IPThreeParametersExponentialDamageLaw::IPThreeParametersExponentialDamageLaw(const IPThreeParametersExponentialDamageLaw &source) :IPDamage(source)
{

}

IPThreeParametersExponentialDamageLaw &IPThreeParametersExponentialDamageLaw::operator=(const IPVariable &source)
{
  IPDamage::operator=(source);
  const IPThreeParametersExponentialDamageLaw* src = dynamic_cast<const IPThreeParametersExponentialDamageLaw*>(&source);
  if(src!=NULL)
  {
  }
  return *this;
}
void IPThreeParametersExponentialDamageLaw::restart()
{
  IPDamage::restart();
}

IPVariable *IPThreeParametersExponentialDamageLaw::clone() const
{
  return new IPThreeParametersExponentialDamageLaw(*this);
}


IPSimpleSaturateDamageLaw::IPSimpleSaturateDamageLaw(const double pinit) : IPDamage(pinit)
{

}
IPSimpleSaturateDamageLaw::IPSimpleSaturateDamageLaw(const IPSimpleSaturateDamageLaw &source) :IPDamage(source)
{

}

IPSimpleSaturateDamageLaw &IPSimpleSaturateDamageLaw::operator=(const IPVariable &source)
{
  IPDamage::operator=(source);
  const IPSimpleSaturateDamageLaw* src = dynamic_cast<const IPSimpleSaturateDamageLaw*>(&source);
  if(src!=NULL)
  {
  }
  return *this;
}
void IPSimpleSaturateDamageLaw::restart()
{
  IPDamage::restart();
}

IPVariable *IPSimpleSaturateDamageLaw::clone() const
{
  return new IPSimpleSaturateDamageLaw(*this);
}



IPPowerBrittleDamagelaw::IPPowerBrittleDamagelaw(const double pinit) : IPDamage(pinit)
{

}
IPPowerBrittleDamagelaw::IPPowerBrittleDamagelaw(const IPPowerBrittleDamagelaw &source) :IPDamage(source)
{

}

IPPowerBrittleDamagelaw &IPPowerBrittleDamagelaw::operator=(const IPVariable &source)
{
  IPDamage::operator=(source);
  const IPPowerBrittleDamagelaw* src = dynamic_cast<const IPPowerBrittleDamagelaw*>(&source);
  if(src!=NULL)
  {
  }
  return *this;
}
void IPPowerBrittleDamagelaw::restart()
{
  IPDamage::restart();
}

IPVariable *IPPowerBrittleDamagelaw::clone() const
{
  return new IPPowerBrittleDamagelaw(*this);
}


