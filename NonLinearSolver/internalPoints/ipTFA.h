//
// Description: storing class for TFA material law
// Author:  <Kevin Spilker>, (C) 2019
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef IPTFA_H_
#define IPTFA_H_
#include "ipvariable.h"
#include "STensor3.h"
#include "STensorOperations.h"
#include "ClusteringData.h"

class GenericIPTFA : public IPVariableMechanics
{
  protected:
    GenericClusteringData* _genericClData;

  public:


  GenericIPTFA(const std::vector<int>& allCl, const std::vector<double>& allVolume) : IPVariableMechanics()
  {
    _genericClData = new GenericClusteringData(allCl,allVolume);
  }
  
  GenericIPTFA(const std::vector<int>& allClLv0, const std::vector<double>& allVolumeLv0,
               const std::map<int,std::vector<int>>& allClLv1, const std::map<int,std::vector<double>>& allVolumeLv1) 
               : IPVariableMechanics()
  {
    _genericClData = new GenericClusteringData(allClLv0,allVolumeLv0);
  }

  GenericIPTFA(const GenericIPTFA &source) : IPVariableMechanics(source)
  {
    _genericClData = NULL;
    if (source._genericClData != NULL)
    {
      _genericClData = new GenericClusteringData(*(source._genericClData));
    }
  }

  virtual GenericIPTFA& operator=(const IPVariable &source)
  {
    IPVariableMechanics::operator=(source);
    const GenericIPTFA* src = dynamic_cast<const GenericIPTFA*>(&source);
    if (src != NULL)
    {
      if (src->_genericClData != NULL)
      {
        if (_genericClData!=NULL)
        {
          _genericClData->operator=(*(src->_genericClData));
        }
        else
        {
          _genericClData = new GenericClusteringData(*(src->_genericClData)); 
        }
      }
      else
      {
        if (_genericClData!=NULL)
        {
          delete _genericClData;
          _genericClData = NULL;
        }
      }
    }
    return *this;
  }


  
  GenericClusteringData* getClusteringData() 
  {
    return _genericClData;
  };
  const GenericClusteringData* getClusteringData() const 
  {
    return _genericClData;
  };

  virtual double get(const int comp) const;
  
  virtual void restart();
  virtual IPVariable* clone() const {return new GenericIPTFA(*this);};
  virtual ~GenericIPTFA();

 private:
  GenericIPTFA();
};

class IPTFA : public GenericIPTFA
{
  protected:
    ClusteringData* _clData;

  public:


  IPTFA(const std::vector<int>& allCl, const std::vector<double>& allVolume) : GenericIPTFA(allCl,allVolume)
  {
    _clData = new ClusteringData(allCl,allVolume);
  }

  IPTFA(const IPTFA &source) : GenericIPTFA(source)
  {
    _clData = NULL;
    if (source._clData != NULL)
    {
      _clData = new ClusteringData(*(source._clData));
    }
  }

  virtual IPTFA& operator=(const IPVariable &source)
  {
    GenericIPTFA::operator=(source);
    const IPTFA* src = dynamic_cast<const IPTFA*>(&source);
    if (src != NULL)
    {
      if (src->_clData != NULL)
      {
        if (_clData!=NULL)
        {
          _clData->operator=(*(src->_clData));
        }
        else
        {
          _clData = new ClusteringData(*(src->_clData)); 
        }
      }
      else
      {
        if (_clData!=NULL)
        {
          delete _clData;
          _clData = NULL;
        }
      }
    }
    return *this;
  }
  
  ClusteringData* getClusteringData() {return _clData;};
  const ClusteringData* getClusteringData() const {return _clData;};
  
  ClusteringData*& getRefToClusteringData() {return _clData;};

  virtual double get(const int comp) const; 
  virtual void restart();  
  virtual IPVariable* clone() const {return new IPTFA(*this);};
  virtual ~IPTFA();
};


class IPTFA2Levels : public GenericIPTFA
{
  protected:
    ClusteringData* _clDataLevel0;
    std::map<int,ClusteringData*> _clDataLevel1;

  public:
    
  IPTFA2Levels(const std::vector<int>& allCl, const std::vector<double>& allVolume,
               const std::map<int,std::vector<int>>& allClLv1, const std::map<int,std::vector<double>>& allVolumeLv1) 
               : GenericIPTFA(allCl,allVolume)
  {
    _clDataLevel0 = new ClusteringData(allCl,allVolume);
    for(std::map<int,std::vector<int>>::const_iterator itLv0 = allClLv1.begin(); itLv0 != allClLv1.end(); itLv0++)
    {
      int clLv0 = itLv0->first;
      std::vector<int> allClsOnLv1 = itLv0->second;
      const std::vector<double>& allVolumesOnLv1 = (allVolumeLv1.find(clLv0))->second;
      _clDataLevel1[clLv0] = new ClusteringData(allClsOnLv1,allVolumesOnLv1);
      (_clDataLevel1[clLv0]->upperClusterIDs).clear();
      (_clDataLevel1[clLv0]->upperClusterIDs).push_back(clLv0);
    }
  }
    
  IPTFA2Levels(const IPTFA2Levels &source) : GenericIPTFA(source)
  {
    _clDataLevel0 = NULL;
    if (source._clDataLevel0 != NULL)
    {
      _clDataLevel0 = new ClusteringData(*(source._clDataLevel0));
    }
    
    _clDataLevel1.clear();
    if (!((source._clDataLevel1).empty()))
    {
      for(std::map<int,ClusteringData*>::const_iterator itLv0 = (source._clDataLevel1).begin(); itLv0 != (source._clDataLevel1).end(); itLv0++)
      {
        int clLv0 = itLv0->first;
        _clDataLevel1[clLv0] = new ClusteringData(*((source._clDataLevel1.find(clLv0))->second));
      }
    }
  }
    
  virtual IPTFA2Levels& operator=(const IPVariable &source)
  {
    IPVariableMechanics::operator=(source);
    
    const IPTFA2Levels* src = dynamic_cast<const IPTFA2Levels*>(&source);
    if (src != NULL)
    {
      if (src->_clDataLevel0 != NULL)
      {
        if (_clDataLevel0!=NULL)
        {
          _clDataLevel0->operator=(*(src->_clDataLevel0));
        }
        else
        {
          _clDataLevel0 = new ClusteringData(*(src->_clDataLevel0)); 
        }
      }
      else
      {
        if (_clDataLevel0!=NULL)
        {
          delete _clDataLevel0;
          _clDataLevel0 = NULL;
        }
      }
      if (!(src->_clDataLevel1.empty()))
      {
        if (!(_clDataLevel1.empty()))
        {
          for(std::map<int,ClusteringData*>::const_iterator itLv0 = (src->_clDataLevel1).begin(); itLv0 != (src->_clDataLevel1).end(); itLv0++)
          {
            int clLv0 = itLv0->first;
            _clDataLevel1[clLv0]->operator=(*itLv0->second);
          }
        }
        else
        {
          for(std::map<int,ClusteringData*>::const_iterator itLv0 = (src->_clDataLevel1).begin(); itLv0 != (src->_clDataLevel1).end(); itLv0++)
          {
            int clLv0 = itLv0->first;
            _clDataLevel1[clLv0] = new ClusteringData(*itLv0->second);
          }
        }
      }
      else
      {
        if (!(_clDataLevel1.empty()))
        {
          _clDataLevel1.clear();
        }
      }
    }
    return *this;
  }

  ClusteringData* getClusteringDataLevel0() {return _clDataLevel0;};
  const ClusteringData* getClusteringDataLevel0() const {return _clDataLevel0;};

  ClusteringData* getClusteringDataOnLevel1(int clLv0) {return _clDataLevel1[clLv0];};
  const ClusteringData* getClusteringDataOnLevel1(int clLv0) const {return (_clDataLevel1.find(clLv0))->second;};
  
  std::map<int,ClusteringData*> getClusteringDataLevel1() {return _clDataLevel1;};
  const std::map<int,ClusteringData*> getClusteringDataLevel1() const {return _clDataLevel1;};

  virtual double get(const int comp) const;  
  virtual void restart();
  virtual IPVariable* clone() const {return new IPTFA2Levels(*this);};
  virtual ~IPTFA2Levels();
};


/*class IPTFA : public IPVariableMechanics
{
  protected:
    ClusteringData* _clData;

 public:


  IPTFA(const std::vector<int>& allCl, const std::vector<double>& allVolume) : IPVariableMechanics()
  {
    _clData = new ClusteringData(allCl,allVolume);
  }


  IPTFA(const IPTFA &source) : IPVariableMechanics(source)
  {
    _clData = NULL;
    if (source._clData != NULL)
    {
      _clData = new ClusteringData(*(source._clData));
    }
  }


  virtual IPTFA& operator=(const IPVariable &source)
  {
    IPVariableMechanics::operator=(source);
    const IPTFA* src = dynamic_cast<const IPTFA*>(&source);
    if (src != NULL)
    {
      if (src->_clData != NULL)
      {
        if (_clData!=NULL)
        {
          _clData->operator=(*(src->_clData));
        }
        else
        {
          _clData = new ClusteringData(*(src->_clData)); 
        }
      }
      else
      {
        if (_clData!=NULL)
        {
          delete _clData;
          _clData = NULL;
        }
      }
    }
    return *this;
  }


  
  ClusteringData* getClusteringData() {return _clData;};
  const ClusteringData* getClusteringData() const {return _clData;};

  virtual double get(const int comp) const;
  
  virtual void restart();
  virtual IPVariable* clone() const {return new IPTFA(*this);};
  virtual ~IPTFA()
  {
    if (_clData!=NULL) delete _clData;
    _clData = NULL;
  }
  

 private:
  IPTFA();
};*/


#endif // IPTFA_H_
