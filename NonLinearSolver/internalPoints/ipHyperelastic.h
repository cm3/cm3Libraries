//
// C++ Interface: ipvariable
//
// Description: IP ViscoElastoPlastic
//
// Author:  V.D. Nguyen, (C) 2014
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef IPHYPERELASTIC_H_
#define IPHYPERELASTIC_H_

#include "j2IsotropicHardening.h"
#include "kinematicHardening.h"
#include "STensor43.h"

class IPHyperViscoElastic : public IPVariableMechanics{
  public:
    //viscoelastic history
    int _N; // number spring-Dashpot of element
    std::vector<STensor3> _A, _A_rot; // N elements
    std::vector<double> _B; // N elements
    double _elasticEnergy; // elastic energy stored
    STensor3 _Ee; // elastic strain
    STensor3 _kirchhoff; // corotational Kirchhoff stress
    STensor3 _Re; // rotation tensor: Fe = Re . Ue
    STensor3 _Re0; // rotation tensor: Fe0 = Re0 . Ue0
    STensor3 _Ee0; // rotation tensor: Fe0 = Re0 . Ue0

    double _irreversibleEnergy;
    STensor3 _DirreversibleEnergyDF;
    
    double _elasticBulkPropertyScaleFactor;
    double _elasticShearPropertyScaleFactor;
    double _intA; // intA = psi_ExtraBranch/Kinf
    double _intB; // intB = psi_ExtraBranch/Ginf
    double _DintA; // derivative of _intA
    double _DintB; // derivative of _intB

    std::vector<double> _psi_branch; // N elements
    
  protected:
    //For energy sources
    double _viscousEnergyPart;
    STensor3 _dElasticEnergyPartdF, _dViscousEnergyPartdF;

  public:
    IPHyperViscoElastic(const int N);
    IPHyperViscoElastic(const IPHyperViscoElastic& src);
    virtual IPHyperViscoElastic& operator=(const IPVariable& src);
    virtual ~IPHyperViscoElastic(){};

    virtual IPVariable* clone() const{return new IPHyperViscoElastic(*this);};

    virtual void getViscoElasticStrain(int i, STensor3& Ev) const;

    virtual STensor3& getRefToElasticStrain() {return _Ee;};
    virtual const STensor3& getConstRefToElasticStrain() const {return _Ee;};

    virtual STensor3& getRefToCorotationalKirchhoffStress() {return _kirchhoff;};
    virtual const STensor3& getConstRefToCorotationalKirchhoffStress() const {return _kirchhoff;};




    virtual double defoEnergy() const{return _elasticEnergy;}
    virtual double& getRefToElasticEnergy() {return _elasticEnergy;};
    virtual double plasticEnergy() const{return 0.;}

    virtual double irreversibleEnergy() const {return _irreversibleEnergy;};
    virtual double & getRefToIrreversibleEnergy() {return _irreversibleEnergy;};

    virtual STensor3& getRefToDIrreversibleEnergyDF() {return _DirreversibleEnergyDF;};
    virtual const STensor3& getConstRefToDIrreversibleEnergyDF() const{return _DirreversibleEnergyDF;};

    virtual void restart();

    virtual void resetA();
    virtual void resetB();


    virtual double &getRefToElasticEnergyPart() {return _elasticEnergy;};
    virtual const double &getConstRefToElasticEnergyPart() const {return _elasticEnergy;};
    virtual double &getRefToViscousEnergyPart() {return _viscousEnergyPart;};
    virtual const double &getConstRefToViscousEnergyPart() const {return _viscousEnergyPart;};



    virtual STensor3 &getRefToDElasticEnergyPartdF() {return _dElasticEnergyPartdF;}
    virtual const STensor3 &getConstRefToDElasticEnergyPartdF() const {return _dElasticEnergyPartdF;}
    virtual STensor3 &getRefToDViscousEnergyPartdF() {return _dViscousEnergyPartdF;}
    virtual const STensor3 &getConstRefToDViscousEnergyPartdF() const {return _dViscousEnergyPartdF;}
    
    virtual double &getRefToElasticBulkPropertyScaleFactor() {return _elasticBulkPropertyScaleFactor;}
    virtual const double &getConstRefToElasticBulkPropertyScaleFactor() const {return _elasticBulkPropertyScaleFactor;}
    virtual double &getRefToElasticShearPropertyScaleFactor() {return _elasticShearPropertyScaleFactor;}
    virtual const double &getConstRefToElasticShearPropertyScaleFactor() const {return _elasticShearPropertyScaleFactor;}


};

class IPHyperViscoElastoPlastic : public IPHyperViscoElastic{
  public: // defined with public to free access
    IPJ2IsotropicHardening* _ipCompression; // ip in compression
    IPJ2IsotropicHardening* _ipTraction; // ip in traction
    IPKinematicHardening* _ipKinematic;


    STensor3 _DgammaDF; // DequivalentPlasticStrainDF
    STensor3 _backsig; // backstress
    STensor3 _Fe; // elastic strain

    double _Gamma; // flow rule parameter
    double _epspbarre; // equivalent plastic strain
    STensor3 _Fp;    // plastic part of the deformation gradient
    double _nup; // plastic poisson ration

    //plastic eq deformation in compression, traction and shear
    double _epspCompression;
    double _epspTraction;
    double _epspShear;

    double _DgammaDt; // plastic deformation rate

    // failure
    bool _inPostFailureStage;
    double _r;  // failure onset
    double _gF; //
    STensor3 _dgFdF;

    double _plasticEnergy;
    bool _dissipationBlocked;
    bool _dissipationActive;
  protected:
    STensor3 _dPlasticEnergyPartdF; 

  public:
    IPHyperViscoElastoPlastic(const J2IsotropicHardening* comp,
                    const J2IsotropicHardening* trac,
                    const kinematicHardening* kin, const int N);
    IPHyperViscoElastoPlastic(const IPHyperViscoElastoPlastic& src);
    virtual IPHyperViscoElastoPlastic& operator =(const IPVariable &source);
    virtual ~IPHyperViscoElastoPlastic();

    virtual IPVariable* clone() const{return new IPHyperViscoElastoPlastic(*this);};

    virtual bool& getRefToDissipationActive(){return _dissipationActive;};
    virtual bool dissipationIsActive() const {return _dissipationActive;};

    virtual void blockDissipation(const bool fl){_dissipationBlocked = fl;};
    virtual bool dissipationIsBlocked() const{return _dissipationBlocked;};

    virtual double& getRefToPlasticEnergy() {return _plasticEnergy;};
    virtual double plasticEnergy() const{return _plasticEnergy;}
    

    virtual void restart();

    virtual bool inPostFailureStage() const {return _inPostFailureStage;};
    virtual bool& getRefToInPostFailureStage() {return _inPostFailureStage;};


    virtual double getFailureOnset() const {return _r;};
    virtual double& getRefToFailureOnset() {return _r;};

    virtual double getFailurePlasticity() const {return _gF;};
    virtual double& getRefToFailurePlasticity() {return _gF;};

    virtual const STensor3& getConstRefToDFailurePlasticityDF() const {return _dgFdF;};
    virtual STensor3& getRefToDFailurePlasticityDF() {return _dgFdF;};

    virtual const STensor3 & getConstRefToFp() const;
    virtual STensor3& getRefToFp();

    virtual const STensor3& getConstRefToBackStress() const;
    virtual STensor3& getRefToBackStress();

    virtual const STensor3& getConstRefToFe() const;
    virtual STensor3& getRefToFe();

    virtual const double& getConstRefToCompressionPlasticStrain() const;
    virtual double & getRefToCompressionPlasticStrain();

    virtual const double& getConstRefToTractionPlasticStrain() const;
    virtual double & getRefToTractionPlasticStrain();

    virtual const double& getConstRefToShearPlasticStrain() const;
    virtual double & getRefToShearPlasticStrain();

    virtual const double& getConstRefToEqPlasticStrain() const;
    virtual double& getRefToEqPlasticStrain();

    virtual const double getConstRefToPlasticPoissonRatio() const;
    virtual double& getRefToPlasticPoissonRatio();

    virtual const double& getConstRefToPlasticDeformationRate() const;
    virtual double& getRefToPlasticDeformationRate();

    virtual const IPJ2IsotropicHardening& getConstRefToIPCompressionHardening() const;
    virtual IPJ2IsotropicHardening& getRefToIPCompressionHardening();

    virtual const IPJ2IsotropicHardening& getConstRefToIPTractionHardening() const;
    virtual IPJ2IsotropicHardening& getRefToIPTractionHardening();

    virtual const IPKinematicHardening& getConstRefToKinematicHardening() const;
    virtual IPKinematicHardening& getRefToKinematicHardening();

    virtual bool IPKinematicHardeningIsSaturated() const{
      if (_ipKinematic != NULL) return _ipKinematic->isSaturated();
      else return false;
    }
    virtual bool IPJ2IsotropicHardeningIsSaturated() const{
      if (_ipCompression!=NULL) return _ipCompression->isSaturated();
      else if (_ipTraction != NULL) return _ipTraction->isSaturated();
      else return false;
    }
    virtual bool isSaturated() const{
      if (IPKinematicHardeningIsSaturated()) return true;
      if (IPJ2IsotropicHardeningIsSaturated()) return true;
      return false;
    }

    virtual void saturate(const bool fl){
      if (_ipKinematic != NULL){
        _ipKinematic->getRefToSaturationState() = fl;
      }

      if (_ipCompression != NULL){
        _ipCompression->getRefToSaturationState() = fl;
      }

      if (_ipTraction != NULL){
        _ipTraction->getRefToSaturationState() = fl;
      }
    };
    virtual void resetPlasticity();
     virtual double &getRefToPlasticEnergyPart() {return _plasticEnergy;};
    virtual const double &getConstRefToPlasticEnergyPart() const {return _plasticEnergy;};



    virtual STensor3 &getRefToDPlasticEnergyPartdF() {return _dPlasticEnergyPartdF;}
    virtual const STensor3 &getConstRefToDPlasticEnergyPartdF() const {return _dPlasticEnergyPartdF;}




};

#endif // IPHYPERELASTIC_H_
