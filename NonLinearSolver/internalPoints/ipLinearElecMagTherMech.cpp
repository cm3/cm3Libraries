//
// Description: storing class for Linear Electro-Magneto-Thermo-Mechanics
//
//
// Author:  <Vinayak GHOLAP>, (C) 2019
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "ipLinearElecMagTherMech.h"

IPLinearElecMagTherMech::IPLinearElecMagTherMech() : IPLinearElecTherMech(){};
IPLinearElecMagTherMech::IPLinearElecMagTherMech(const IPLinearElecMagTherMech &source)
:  IPLinearElecTherMech(source){}
IPLinearElecMagTherMech& IPLinearElecMagTherMech::operator=(const IPVariable &source)
{
  IPLinearElecTherMech::operator=(source);
  //const IPLinearElecMagTherMech* src = static_cast<const IPLinearElecMagTherMech*>(&source);

  return *this;
}

double IPLinearElecMagTherMech::defoEnergy() const
{
  return  IPLinearElecTherMech::defoEnergy();
}

void IPLinearElecMagTherMech::restart()
{
  IPLinearElecTherMech::restart();
}
double IPLinearElecMagTherMech::get(const int comp) const
{
    return IPLinearElecTherMech::get(comp);
}

double & IPLinearElecMagTherMech::getRef(const int comp)
{
    return IPLinearElecTherMech::getRef(comp);
}