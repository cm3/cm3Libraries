//
// Description: storing class for vevp umat interface
// Author:  <V.D. NGUYEN>, (C) 2023
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef IPVEVPUMAT_H_
#define IPVEVPUMAT_H_
#include "ipUMATInterface.h"
#include "STensor3.h"
class IPVEVPUMAT : public IPUMATInterface
{
 protected:

 public: // All data public to avoid the creation of function to access

 public:
  IPVEVPUMAT(int _nsdv, double eleSize) : IPUMATInterface(_nsdv, eleSize)
  {

    _statev[0]=1;
    _statev[1]=1;
    _statev[2]=1;
  }
  IPVEVPUMAT(const IPVEVPUMAT &source) : IPUMATInterface(source)
  {
  }
  IPVEVPUMAT &operator = (const IPVariable &_source)
  {
      IPUMATInterface::operator=(_source);
      return *this;
  }
  virtual ~IPVEVPUMAT()
  {
  }
  virtual IPVariable* clone() const {return new IPVEVPUMAT(*this);};
  virtual void restart();
  virtual double get(const int i) const;

 protected:
  IPVEVPUMAT(){}
};

#endif // IPVEVPUMAT_H_
