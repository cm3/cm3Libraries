//
// C++ Interface: Material Law
//
// Description: IPNonLinearTVM (IP Thermo-ViscoElasto-ViscoPlasto Law) with Non-Local Damage Interface (soon.....)
//
// Author: <Ujwal Kishore J - FLE_Knight>, (C) 2022
//
// Copyright: See COPYING file that comes with this distribution
//
//

#include "ipNonLinearTVP.h"

IPNonLinearTVP::IPNonLinearTVP(const J2IsotropicHardening* comp, const J2IsotropicHardening* trac,
                                const kinematicHardening* kin, const int N, const mullinsEffect* mullins):IPNonLinearTVM(comp,trac,kin,N,mullins),
                                _DgammaDT(0.),_DGammaDT(0.), _DGammaDF(0.), _DirreversibleEnergyDT(0.),
                                _psiTVM(0.), _DpsiTVMdT(0.), _DDpsiTVMdTT(0.),
								_DModMandelDT(0.), _DbackSigDT(0.), _DModMandelDF(0.), _DbackSigDF(0.), _DphiPDF(0.), _GammaN(0.), _dGammaNdT(0.), _dGammaNdF(0.),
                                _mechSrcTVP(0.),_DmechSrcTVPdT(0.),_DmechSrcTVPdF(0.),
                                _dPlasticEnergyPartdT(0.),_dElasticEnergyPartdT(0.),_dViscousEnergyPartdT(0.),
                                _IsoHardForce_simple(0.), _dIsoHardForcedT_simple(0.), _dIsoHardForcedF_simple(0.), _DphiDF(0.),_DphiDT(0.), _dgFdT(0.), _detEe(0.)
{

    _IsoHardForce.clear(); _dIsoHardForcedT.clear(); _dIsoHardForcedF.clear();
    for (int i=0; i < 3; i++){
      _IsoHardForce.push_back(0.);
      _dIsoHardForcedT.push_back(0.);
      _ddIsoHardForcedTT.push_back(0.);
      STensor3 el(0.);
      _dIsoHardForcedF.push_back(el);
    }

};

IPNonLinearTVP::IPNonLinearTVP(const IPNonLinearTVP& src):IPNonLinearTVM(src),
                                _DgammaDT(src._DgammaDT),_DGammaDT(src._DGammaDT),_DGammaDF(src._DGammaDF),_DirreversibleEnergyDT(src._DirreversibleEnergyDT),
                                _IsoHardForce(src._IsoHardForce), _dIsoHardForcedT(src._dIsoHardForcedT), _dIsoHardForcedF(src._dIsoHardForcedF),
                                _ddIsoHardForcedTT(src._ddIsoHardForcedTT),
                                _psiTVM(src._psiTVM), _DpsiTVMdT(src._DpsiTVMdT), _DDpsiTVMdTT(src._DDpsiTVMdTT),
                                _mechSrcTVP(src._mechSrcTVP),_DmechSrcTVPdT(src._DmechSrcTVPdT),_DmechSrcTVPdF(src._DmechSrcTVPdF),
                                _DModMandelDT(src._DModMandelDT), _DbackSigDT(src._DbackSigDT), _DphiPDF(src._DphiPDF),
                                _GammaN(src._GammaN), _dGammaNdT(src._dGammaNdT), _dGammaNdF(src._dGammaNdF),
                                _DModMandelDF(src._DModMandelDF), _DbackSigDF(src._DbackSigDF),
                                _dPlasticEnergyPartdT(src._dPlasticEnergyPartdT),_dElasticEnergyPartdT(src._dElasticEnergyPartdT),
                                _dViscousEnergyPartdT(src._dViscousEnergyPartdT),
                                _IsoHardForce_simple(src._IsoHardForce_simple),
                                _dIsoHardForcedT_simple(src._dIsoHardForcedT_simple), _dIsoHardForcedF_simple(src._dIsoHardForcedF_simple), _DphiDF(src._DphiDF),_DphiDT(src._DphiDT),
                                _dgFdT(src._dgFdT), _detEe(src._detEe)
{

};

IPNonLinearTVP& IPNonLinearTVP::operator =(const IPVariable &source){
  IPNonLinearTVM::operator=(source);

  const IPNonLinearTVP* ps = dynamic_cast<const IPNonLinearTVP*>(&source);
  if (ps != NULL){

    _DgammaDT = ps->_DgammaDT;
    _DGammaDT = ps->_DGammaDT;
    _DGammaDF = ps->_DGammaDF;
    _DirreversibleEnergyDT = ps->_DirreversibleEnergyDT;
    _IsoHardForce = ps->_IsoHardForce;
    _dIsoHardForcedT = ps->_dIsoHardForcedT;
    _dIsoHardForcedF = ps->_dIsoHardForcedF;
    _ddIsoHardForcedTT = ps->_ddIsoHardForcedTT;
    _psiTVM = ps->_psiTVM;
    _DpsiTVMdT = ps->_DpsiTVMdT;
    _DDpsiTVMdTT = ps->_DDpsiTVMdTT;
    _mechSrcTVP = ps->_mechSrcTVP;
    _DmechSrcTVPdT = ps->_DmechSrcTVPdT;
    _DmechSrcTVPdF = ps->_DmechSrcTVPdF;
    _DModMandelDT = ps->_DModMandelDT;
    _DModMandelDF = ps->_DModMandelDF;
    _DbackSigDT = ps->_DbackSigDT;
    _DbackSigDF = ps->_DbackSigDF;
    _DphiPDF = ps->_DphiPDF;
    _GammaN = ps->_GammaN;
    _dGammaNdT = ps->_dGammaNdT;
    _dGammaNdF = ps->_dGammaNdF;
    _dPlasticEnergyPartdT = ps->_dPlasticEnergyPartdT;
    _dElasticEnergyPartdT = ps->_dElasticEnergyPartdT;
    _dViscousEnergyPartdT = ps->_dViscousEnergyPartdT;
    _IsoHardForce_simple = ps->_IsoHardForce_simple;
    _dIsoHardForcedT_simple = ps->_dIsoHardForcedT_simple;
    _dIsoHardForcedF_simple = ps->_dIsoHardForcedF_simple;
    _DphiDF = ps->_DphiDF;
    _DphiDT = ps->_DphiDT;
    _dgFdT = ps->_dgFdT;
    _detEe = ps->_detEe;
  }
  return *this;
};

IPNonLinearTVP::~IPNonLinearTVP(){

};


void IPNonLinearTVP::restart() {
  IPNonLinearTVM::restart();
  restartManager::restart(_DgammaDT);
  restartManager::restart(_DGammaDT);
  restartManager::restart(_DGammaDF);
  restartManager::restart(_DirreversibleEnergyDT);
  restartManager::restart(_IsoHardForce);
  restartManager::restart(_dIsoHardForcedT);
  restartManager::restart(_dIsoHardForcedF);
  restartManager::restart(_ddIsoHardForcedTT);
  restartManager::restart(_psiTVM);
  restartManager::restart(_DpsiTVMdT);
  restartManager::restart(_DDpsiTVMdTT);
  restartManager::restart(_mechSrcTVP);
  restartManager::restart(_DmechSrcTVPdT);
  restartManager::restart(_DmechSrcTVPdF);
  restartManager::restart(_DModMandelDT);
  restartManager::restart(_DModMandelDF);
  restartManager::restart(_DbackSigDT);
  restartManager::restart(_DbackSigDF);
  restartManager::restart(_DphiPDF);
  restartManager::restart(_GammaN);
  restartManager::restart(_dGammaNdT);
  restartManager::restart(_dGammaNdF);
  restartManager::restart(_dPlasticEnergyPartdT);
  restartManager::restart(_dElasticEnergyPartdT);
  restartManager::restart(_dViscousEnergyPartdT);
  restartManager::restart(_IsoHardForce_simple);
  restartManager::restart(_dIsoHardForcedT_simple);
  restartManager::restart(_dIsoHardForcedF_simple);
  restartManager::restart(_DphiDF);
  restartManager::restart(_DphiDT);
  restartManager::restart(_dgFdT);
  restartManager::restart(_detEe);
  return;
}
