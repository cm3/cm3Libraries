//
// Description: storing class for Linear Thermo Mechanics
//
//
// Author:  <Lina Homsy>, (C) 2013
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "ipLinearThermoMechanics.h"
#include "restartManager.h"
IPLinearThermoMechanics::IPLinearThermoMechanics() : IPVariableMechanics(), _elasticEnergy(0.), _thermalEnergy(0.) , _fracEnergy(0.){


};
IPLinearThermoMechanics::IPLinearThermoMechanics(const IPLinearThermoMechanics &source) : IPVariableMechanics(source), _elasticEnergy(source._elasticEnergy), _thermalEnergy(source._thermalEnergy), _fracEnergy(source._fracEnergy) {


}
IPLinearThermoMechanics& IPLinearThermoMechanics::operator=(const IPVariable &source)
{
  IPVariableMechanics::operator=(source);
  const IPLinearThermoMechanics* src = static_cast<const IPLinearThermoMechanics*>(&source);

  if(src)
  {
     _elasticEnergy=src->_elasticEnergy;
     _thermalEnergy=src->_thermalEnergy;
     _fracEnergy=src->_fracEnergy;
  }
  return *this;
}

double IPLinearThermoMechanics::defoEnergy() const
{
  return _elasticEnergy;
}
double IPLinearThermoMechanics::getThermalEnergy() const
{
  return _thermalEnergy;
}

/*double IPLinearThermoMechanics::fracEnergy() const
{
  return _fracEnergy;
}*/

double IPLinearThermoMechanics::get(const int comp) const
{
  return IPVariableMechanics::get(comp);
}
double & IPLinearThermoMechanics::getRef(const int comp)
{
    return IPVariableMechanics::getRef(comp);
}

void IPLinearThermoMechanics::restart()
{
  IPVariableMechanics::restart();
  restartManager::restart(_elasticEnergy);
  restartManager::restart(_thermalEnergy);
  restartManager::restart(_fracEnergy);
  return;
}
