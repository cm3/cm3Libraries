//
// Description: Define ip data for cohesive layer
// the parameters of coheisve law including Gc, Kp, sigmac, deltac are
// set using function  initializeCohesiveData
// to compute other paramters--> using mlawCohesive
//
// Author:  <V.D. Nguyen>, (C) 2015
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef IPCOHESIVE_H_
#define IPCOHESIVE_H_

#include "ipvariable.h"
#include "STensor3.h"
#include "STensorOperations.h"    

class IPCohesiveDamage
{
  protected:
    double _cohesiveDamage; // cohesive Damage
    double _DcohesiveDamageDdelta;
    
  public:
    IPCohesiveDamage():_cohesiveDamage(0.), _DcohesiveDamageDdelta(0.){}
    IPCohesiveDamage(const IPCohesiveDamage& src): _cohesiveDamage(src._cohesiveDamage), _DcohesiveDamageDdelta(src._DcohesiveDamageDdelta){}
    virtual IPCohesiveDamage& operator = (const IPCohesiveDamage& src)
    {
      _cohesiveDamage = src._cohesiveDamage;
      _DcohesiveDamageDdelta = src._DcohesiveDamageDdelta;
      return *this;
    }
    virtual ~IPCohesiveDamage(){};
    
    virtual IPCohesiveDamage* clone() const  = 0;
    virtual double& getRefToCohesiveDamage() {return _cohesiveDamage;};
    virtual const double& getConstRefToCohesiveDamage()const {return _cohesiveDamage;};

    virtual double& getRefToDCohesiveDamageDEffectiveJump() {return _DcohesiveDamageDdelta;};
    virtual const double& getConstRefToDCohesiveDamageDEffectiveJump() const {return _DcohesiveDamageDdelta;};
    
    virtual void restart()
    {
      restartManager::restart(_cohesiveDamage); // cohesive Damage
      restartManager::restart(_DcohesiveDamageDdelta);
    };
};

class IPZeroCohesiveDamage : public IPCohesiveDamage
{
  public:
    IPZeroCohesiveDamage(): IPCohesiveDamage(){}
    IPZeroCohesiveDamage(const IPZeroCohesiveDamage& src): IPCohesiveDamage(src){}
    virtual IPZeroCohesiveDamage& operator =(const IPCohesiveDamage& src)
    {
      IPCohesiveDamage::operator =(src);
      return *this;
    }
    virtual IPCohesiveDamage* clone() const {return new IPZeroCohesiveDamage(*this);};
    virtual ~IPZeroCohesiveDamage(){};
};


class IPBiLinearCohesiveDamage : public IPCohesiveDamage
{
  public:
    IPBiLinearCohesiveDamage(): IPCohesiveDamage(){}
    IPBiLinearCohesiveDamage(const IPBiLinearCohesiveDamage& src): IPCohesiveDamage(src){}
    virtual IPBiLinearCohesiveDamage& operator =(const IPCohesiveDamage& src)
    {
      IPCohesiveDamage::operator =(src);
      return *this;
    }
    virtual IPCohesiveDamage* clone() const {return new IPBiLinearCohesiveDamage(*this);};
    virtual ~IPBiLinearCohesiveDamage(){};
};

class IPExponentialCohesiveDamage : public IPCohesiveDamage
{
  protected:
    double _xi;
    
  public:
    IPExponentialCohesiveDamage(double x): IPCohesiveDamage(), _xi(x){}
    IPExponentialCohesiveDamage(const IPExponentialCohesiveDamage& src): IPCohesiveDamage(src), _xi(src._xi){}
    virtual IPExponentialCohesiveDamage& operator =(const IPCohesiveDamage& src)
    {
      IPCohesiveDamage::operator =(src);
      const IPExponentialCohesiveDamage* psrc = dynamic_cast<const IPExponentialCohesiveDamage*>(&src);
      if (psrc != NULL)
      {
        _xi = psrc->_xi;
      }
      return *this;
    }
    virtual ~IPExponentialCohesiveDamage(){};
    virtual IPCohesiveDamage* clone() const {return new IPExponentialCohesiveDamage(*this);};
    
    virtual double& getRefToPowerParameter() {return _xi;};
    virtual double getConstRefToPowerParameter() const {return _xi;};
    virtual void restart()
    {
      IPCohesiveDamage::restart(); // cohesive Damage
      restartManager::restart(_xi);
    };
};


class IPCohesive : public IPVariableMechanics{
  protected:
    double _beta; // mixmode parameter
    double _Kp; // compressive penalty
    double _Kn; // normal penalty
    double _sigmac; // critical stress
    double _deltac; // critical effective opening
    double _delta0; // damage onset--> clearly sigmac = sigmac/Kp;
    double _deltamax; // maximal effective crack opening
    double _sigmamax; // traction at _deltamax
 		bool _tension; // true if tension state false if compression
    double _fracEnergy;
		
		double _reversibleEnergy;
		double _irreversibleEnergy;
		SVector3 _DIrreversibleEnergyDJump;
    IPCohesiveDamage* _ipCohesiveDamage;

  public:
    IPCohesive():IPVariableMechanics(),_beta(0.),_Kp(0.), _Kn(0.),_sigmac(0.),_deltac(0.),
                _delta0(0.),_deltamax(0.),_sigmamax(0.),_ipCohesiveDamage(NULL),
                _tension(true),_fracEnergy(0.),
								_reversibleEnergy(0.),_irreversibleEnergy(0.),
								_DIrreversibleEnergyDJump(0.){}
    IPCohesive(const IPCohesive& src):IPVariableMechanics(src),_beta(src._beta),_Kp(src._Kp), _Kn(src._Kn),_sigmac(src._sigmac),_deltac(src._deltac),
                _delta0(src._delta0),_deltamax(src._deltamax),_sigmamax(src._sigmamax),_tension(src._tension),
                _fracEnergy(src._fracEnergy),_reversibleEnergy(src._reversibleEnergy),
								_irreversibleEnergy(src._irreversibleEnergy),
								_DIrreversibleEnergyDJump(src._DIrreversibleEnergyDJump)
                {
                  _ipCohesiveDamage = NULL;
                  if (src._ipCohesiveDamage != NULL)
                  {
                    _ipCohesiveDamage = src._ipCohesiveDamage->clone();
                  }
                }

    virtual IPCohesive& operator=(const IPVariable& src){
      IPVariableMechanics::operator=(src);
      const IPCohesive* psrc = dynamic_cast<const IPCohesive*>(&src);
      if (psrc != NULL){
        _beta = psrc->_beta;
        _Kp = psrc->_Kp;
        _Kn = psrc->_Kn;
        _sigmac =  psrc->_sigmac;
        _deltac =  psrc->_deltac;
        _delta0 =  psrc->_delta0;
        _deltamax =  psrc->_deltamax;
        _sigmamax =  psrc->_sigmamax;
				_tension = psrc->_tension;
        _fracEnergy = psrc->_fracEnergy;
				_reversibleEnergy = psrc->_reversibleEnergy;
				_irreversibleEnergy = psrc->_irreversibleEnergy;
				_DIrreversibleEnergyDJump = psrc->_DIrreversibleEnergyDJump;
        
        if (psrc->_ipCohesiveDamage != NULL)
        {
          if (_ipCohesiveDamage == NULL)
          {
            _ipCohesiveDamage = psrc->_ipCohesiveDamage->clone();
          }
          else
          {
            _ipCohesiveDamage->operator =(*psrc->_ipCohesiveDamage);
          }
        }
        else
        {
          if (_ipCohesiveDamage != NULL)
          {
            delete _ipCohesiveDamage;
            _ipCohesiveDamage = NULL;
          }
        }
      }
      return *this;

    }
    virtual ~IPCohesive()
    {
      if (_ipCohesiveDamage != NULL) delete _ipCohesiveDamage;
    }
    virtual IPCohesiveDamage* & getRefToIPCohesiveDamage() {return _ipCohesiveDamage;};
    virtual const IPCohesiveDamage& getConstRefToIPCohesiveDamage() const {return *_ipCohesiveDamage;};
    
    virtual double defoEnergy() const {return _reversibleEnergy;};
    virtual double plasticEnergy() const {return 0.;};
    virtual double damageEnergy() const {return _irreversibleEnergy;};

    virtual double getMixedModeParameter() const{return _beta;}
    virtual double getCompressivePenality() const{return _Kp;};
    virtual double getNormalPenality() const{return _Kn;};
    virtual double getCriticalEffectiveStress() const {return _sigmac;};
    virtual double getCriticalEffectiveJump() const {return _deltac;};
    virtual double getEffectiveJumpDamageOnset() const {return _delta0;};

    virtual double& getRefToMaximalEffectiveJump() {return _deltamax;};
    virtual const double& getConstRefToMaximalEffectiveJump() const {return _deltamax;};
    virtual double& getRefToMaximalEffectiveStress() {return _sigmamax;};
    virtual const double& getConstRefToMaximalEffectiveStress() const {return _sigmamax;};

    virtual double& getRefToCohesiveDamage() {return _ipCohesiveDamage->getRefToCohesiveDamage();};
    virtual const double& getConstRefToCohesiveDamage()const {return _ipCohesiveDamage->getConstRefToCohesiveDamage();};

    virtual double& getRefToDCohesiveDamageDEffectiveJump() {return _ipCohesiveDamage->getRefToDCohesiveDamageDEffectiveJump();};
    virtual const double& getConstRefToDCohesiveDamageDEffectiveJump() const {return _ipCohesiveDamage->getConstRefToDCohesiveDamageDEffectiveJump();};

		virtual bool ifTension() const {return _tension;};
		virtual bool& getRefToTensionFlag() {return _tension;};

    virtual double& getRefToFractureEnergy() {return _fracEnergy;};
    virtual const double& getConstRefToFractureEnergy() const {return _fracEnergy;};
		
		virtual double& getRefToReversibleEnergy() {return _reversibleEnergy;};
		virtual const double& getConstRefToReversibleEnergy() const {return _reversibleEnergy;};
		
		virtual double& getRefToIrreversibleEnergy() {return _irreversibleEnergy;};
		virtual double irreversibleEnergy() const {return _irreversibleEnergy;};
		
		
		virtual SVector3& getRefToDIrreversibleEnergyDJump() {return _DIrreversibleEnergyDJump;};
		virtual const SVector3& getConstRefToDIrreversibleEnergyDJump() const {return _DIrreversibleEnergyDJump;};
		 
    virtual void initializeCohesiveData(const double beta, // mixmode parameter
                                        const double sigmac, // critical stress
                                        const double deltac, // critical effective opening
                                        const double Kn, // initial normal penalty
                                        const double Kp, // compression penalty
                                        const bool iftension)
    {
       _beta = beta;
       _Kn = Kn; 
       _Kp = Kp;
       _sigmac = sigmac;
       _deltac = deltac;
       _delta0 = sigmac/Kn;
       #ifdef _DEBUG
       if(_delta0<1.e-18) Msg::Warning("IPCohesive::initializeCohesiveData with delta0 = 0");
       #endif //_DEBUG
       _tension = iftension;

       _deltamax = _delta0;
       _sigmamax = sigmac;
       _fracEnergy = 0.;
			 _reversibleEnergy = 0.;
			 _irreversibleEnergy = 0.;
			 STensorOperation::zero(_DIrreversibleEnergyDJump);
       if (_ipCohesiveDamage == NULL)
       {
         Msg::Error("IPCohesiveDamage must be allocated initializeCohesiveData");
       }

    }

    virtual void restart(){
      IPVariableMechanics::restart();
      restartManager::restart(_beta); // mixmode parameter
      restartManager::restart(_Kp); // compressive penalty
      restartManager::restart(_Kn); // normal penalty
      restartManager::restart(_sigmac); // critical stress
      restartManager::restart(_deltac); // critical effective opening
      restartManager::restart(_delta0); // damage onset--> clearly sigmac = sigmac/Kp;
      restartManager::restart(_deltamax); // maximal effective crack opening
      restartManager::restart(_sigmamax); // traction at _deltamax
      restartManager::restart(_tension);
      restartManager::restart(_fracEnergy);
			restartManager::restart(_reversibleEnergy);
			restartManager::restart(_irreversibleEnergy);
			restartManager::restart(_DIrreversibleEnergyDJump);
      if (_ipCohesiveDamage!=NULL)
      {
        _ipCohesiveDamage->restart();
      }
    };
    virtual IPVariable* clone() const {return new IPCohesive(*this);};
};

class IPBiLinearCohesive : public IPCohesive{
  public:
    IPBiLinearCohesive():IPCohesive()
    {
      _ipCohesiveDamage = new IPBiLinearCohesiveDamage();
    }
    IPBiLinearCohesive(const IPBiLinearCohesive& src): IPCohesive(src){}
    virtual IPBiLinearCohesive& operator=(const IPVariable& src){
      IPCohesive::operator=(src);
      return *this;
    }
    virtual ~IPBiLinearCohesive(){}
    virtual IPVariable* clone() const {return new IPBiLinearCohesive(*this);};
};

class IPExponentialCohesive : public IPCohesive{
  public:
    IPExponentialCohesive(double x):IPCohesive()
    {
      _ipCohesiveDamage = new IPExponentialCohesiveDamage(x);
    }
    IPExponentialCohesive(const IPExponentialCohesive& src): IPCohesive(src){}
    virtual IPExponentialCohesive& operator=(const IPVariable& src){
      IPCohesive::operator=(src);
      return *this;
    }
    virtual ~IPExponentialCohesive(){}
    virtual IPVariable* clone() const {return new IPExponentialCohesive(*this);};
};

class IPTransverseIsotropicCohesive : public IPBiLinearCohesive{

  public:
    IPTransverseIsotropicCohesive():IPBiLinearCohesive(){}
    IPTransverseIsotropicCohesive(const IPTransverseIsotropicCohesive& src): IPBiLinearCohesive(src){}
    virtual IPTransverseIsotropicCohesive& operator=(const IPVariable& src){
      IPBiLinearCohesive::operator=(src);
      return *this;
    }
    virtual ~IPTransverseIsotropicCohesive(){}
    virtual IPVariable* clone() const {return new IPTransverseIsotropicCohesive(*this);};
};

class IPTransverseIsoCurvatureCohesive : public IPTransverseIsotropicCohesive{
  protected:
    SVector3 _A;
  public:
    IPTransverseIsoCurvatureCohesive(const SVector3 &GaussP, const SVector3 &Centroid, const SVector3 &PointStart1, const SVector3 &PointStart2, const double init_Angle):IPTransverseIsotropicCohesive()
    {
       SVector3 init_A = PointStart1 - Centroid;
       SVector3 Vec1 = PointStart2 - Centroid;
       SVector3 N_local = crossprod(init_A,Vec1);
       SVector3 Vec2 = GaussP - Centroid;
       SVector3 Proj_A = crossprod(N_local,Vec2);
       Proj_A.normalize(); 
       if(fabs(fabs(init_Angle)-90.)<1.e-12)
       {
          _A = N_local;
       }
       else
       {
          N_local.normalize();
          _A = Proj_A + tan(init_Angle*3.14159/180.)*N_local;
       }
       _A.normalize();
    }
    IPTransverseIsoCurvatureCohesive(const IPTransverseIsoCurvatureCohesive& src): IPTransverseIsotropicCohesive(src)
    {
       _A = src._A;
    }
    virtual IPTransverseIsoCurvatureCohesive& operator=(const IPVariable& src){
      IPTransverseIsotropicCohesive::operator=(src);
      const IPTransverseIsoCurvatureCohesive* psrc = dynamic_cast<const IPTransverseIsoCurvatureCohesive*>(&src);
      if (psrc != NULL){
        _A=psrc->_A;
      }

      return *this;
    }
    virtual ~IPTransverseIsoCurvatureCohesive(){}
    virtual void restart()
    {
      IPTransverseIsotropicCohesive::restart();
      restartManager::restart(_A); // direction
    }
    virtual IPVariable* clone() const {return new IPTransverseIsoCurvatureCohesive(*this);};
    virtual void getTransverseDirection(SVector3 &A) const { A =_A ;};

  private:
    IPTransverseIsoCurvatureCohesive():IPTransverseIsotropicCohesive()
    {
       Msg::Error("Cannot construct default IPTransverseIsoCurvatureCohesive");
    }
};

#endif // IPCOHESIVE_H_
