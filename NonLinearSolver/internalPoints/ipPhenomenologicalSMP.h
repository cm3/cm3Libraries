// Description: storing class for linear thermomechanical law
// it is a "virtual implementation" you have to define an ipvariable in your project which derive from this one.
// Author:  <H. Gulasik, M. Pareja>, (C) 2019
// Copyright: See COPYING file that comes with this distribution

#ifndef IPPHENOMENOLOGICALSMP_H_
#define IPPHENOMENOLOGICALSMP_H_
#include "ipLinearThermoMechanics.h"
#include "STensor3.h"
#include "ipvariable.h"
#include "ipHardening.h"
#include "ipHyperelastic.h"
#include "ipCoupledThermoMechanics.h"

class IPPhenomenologicalSMP : public IPCoupledThermoMechanics //: public IPLinearThermoMechanics  //, public mlawSMP
{
 protected:
  IPHyperViscoElastoPlastic *qG;
  IPHyperViscoElastoPlastic *qR;
  IPHyperViscoElastoPlastic *qAM;

  STensor3 FthG, FfG, FthR, FfR, FthI, FthAD, FthAM, FfAM;
  STensor3 PG, PR, PAM, PAML2, PRL2;

#if 0
    STensor3 FthAI;
#endif 
    double zg, PDF;
    double alphaig, Tdot, T_t, wt;
    double Tmin;

    double zs, Tcrys, Tmelt, Wcrys, Wmelt, TRefR, TRefG, TRefAM;
   
#if 0    
    STensor3 AGl, ARl, AAMl, 
#endif
    STensor3 ADl;   
    STensor3 EnThG, EnThR, EnThAM; 

    //energy
    double appliedEnergy, _SMPEnergy;
    double freezedDiss, viscousDiss, EDiss, plasticDiss;


public:
    
    IPPhenomenologicalSMP(const double t0, const double Tc0, const double Tm0, const double wc0, const double wm0,
                          const J2IsotropicHardening* compG, const J2IsotropicHardening* tracG, const kinematicHardening* kinG, const int NG,
                          const J2IsotropicHardening* compR, const J2IsotropicHardening* tracR, const kinematicHardening* kinR, const int NR,
                          const J2IsotropicHardening* compAM, const J2IsotropicHardening* tracAM, const kinematicHardening* kinAM, const int NAM);
                          
                          
    IPPhenomenologicalSMP(const IPPhenomenologicalSMP &source);
    IPPhenomenologicalSMP& operator=(const IPVariable &source);
    virtual ~IPPhenomenologicalSMP(){
       if(qG!=NULL) {delete qG; qG=NULL;};
       if(qR!=NULL) {delete qR; qR=NULL;};
       if(qAM!=NULL) {delete qAM; qAM=NULL;};   
    };
    
    virtual void resetAG() {qG->resetA();}
    virtual void resetBG() {qG->resetB();}
    virtual void resetAR() {qR->resetA();}
    virtual void resetBR() {qR->resetB();}
    virtual void resetAAM() {qAM->resetA();}
    virtual void resetBAM() {qAM->resetB();}

    virtual void resetPlasticityG() {qG->resetPlasticity();}
    virtual void resetPlasticityR() {qR->resetPlasticity();}
    virtual void resetPlasticityAM() {qAM->resetPlasticity();}
 
    virtual void restart();
    virtual IPVariable* clone() const {return new IPPhenomenologicalSMP(*this);};

    virtual double get(const int comp) const;
    virtual double & getRef(const int comp);

    //energy
    virtual STensor3 &getRefToPG()                      {return PG;};
    virtual const STensor3 &getConstRefToPG() const     {return PG;};
    virtual STensor3 &getRefToPR()                      {return PR;};
    virtual const STensor3 &getConstRefToPR() const     {return PR;};
    virtual STensor3 &getRefToPAM()                     {return PAM;};
    virtual const STensor3 &getConstRefToPAM() const    {return PAM;};
    virtual STensor3 &getRefToPAML2()                   {return PAML2;};
    virtual const STensor3 &getConstRefToPAML2() const  {return PAML2;};
    virtual STensor3 &getRefToPRL2()                   {return PRL2;};
    virtual const STensor3 &getConstRefToPRL2() const  {return PRL2;};

    virtual double &getRefTodefoEnergy()                        {return _SMPEnergy;};
    virtual double defoEnergy() const                           {return _SMPEnergy;}
    virtual double &getRefToThermalEnergy()                     {return _thermalEnergy;};
    virtual double& getRefToAppliedEnergy()                     {return appliedEnergy;};
    virtual const double& getConstRefToAppliedEnergy() const    {return appliedEnergy;};
    virtual double &getRefToFreezedDiss()                       {return freezedDiss;};
    virtual const double& getConstRefToFreezedDiss() const      {return freezedDiss;};
    virtual double &getRefToPlasticDiss()                       {return plasticDiss;};
    virtual const double& getConstRefToPlasticDiss() const      {return plasticDiss;};
    virtual double &getRefToViscousDiss()                       {return viscousDiss;};
    virtual const double& getConstRefToViscousDiss() const      {return viscousDiss;};
    virtual double &getRefToEDiss()                             {return EDiss;};
    virtual const double& getConstRefToEDiss() const            {return EDiss;};

    virtual STensor3 &getRefToFthI()                        {return FthI;};
    virtual const STensor3 &getConstRefToFthI() const       {return FthI;};
#if 0    
    virtual STensor3 &getRefToFthAI()                       {return FthAI;};
    virtual const STensor3 &getConstRefToFthAI() const      {return FthAI;};
#endif
    virtual STensor3 &getRefToFthAD()                       {return FthAD;};
    virtual const STensor3 &getConstRefToFthAD() const      {return FthAD;};

    virtual STensor3 &getRefToFthG()                        {return FthG;};
    virtual const STensor3 &getConstRefToFthG() const       {return FthG;};
    virtual STensor3 &getRefToFfG()                         {return FfG;};
    virtual const STensor3 &getConstRefToFfG() const        {return FfG;};
    virtual STensor3 &getRefToFpG()                         {return qG->getRefToFp();};
    virtual const STensor3 &getConstRefToFpG() const        {return qG->getConstRefToFp();};
    virtual STensor3 &getRefToFveG()                        {return qG->getRefToFe();};
    virtual const STensor3 &getConstRefToFveG() const       {return qG->getConstRefToFe();};

    virtual STensor3 &getRefToFthR()                        {return FthR;};
    virtual const STensor3 &getConstRefToFthR() const       {return FthR;};
    virtual STensor3 &getRefToFfR()                         {return FfR;};
    virtual const STensor3 &getConstRefToFfR() const        {return FfR;};
    virtual STensor3 &getRefToFpR()                         {return qR->getRefToFp();};
    virtual const STensor3 &getConstRefToFpR() const        {return qR->getConstRefToFp();};
    virtual STensor3 &getRefToFveR()                        {return qR->getRefToFe();};
    virtual const STensor3 &getConstRefToFveR() const       {return qR->getConstRefToFe();};


    //virtual STensor3 &getRefToUf()                          {return Uf;};
    //virtual const STensor3 &getConstRefToUf() const         {return Uf;};

    virtual double& getRefToEquivalentPlasticDefoG()                     {return qG->getRefToEqPlasticStrain();};
    virtual const double& getConstRefToEquivalentPlasticDefoG() const    {return qG->getConstRefToEqPlasticStrain();};
    virtual double& getRefToEquivalentPlasticDefoR()                     {return qR->getRefToEqPlasticStrain();};
    virtual const double& getConstRefToEquivalentPlasticDefoR() const    {return qR->getConstRefToEqPlasticStrain();};

    virtual double &getRefTozg()                        {return zg;};
    virtual const double& getzg() const                 {return zg;};
    virtual double &getRefToPDF()                       {return PDF;};
    virtual const double& getConstRefToPDF() const      {return PDF;};
    virtual double& getRefToTmin()                      {return Tmin;};
    virtual const double& getConstRefToTmin() const     {return Tmin;};
    virtual double& getRefToTdot()                      {return Tdot;};
    virtual const double& getConstRefToTdot() const     {return Tdot;};
    virtual double& getRefToTt()                        {return T_t;};
    virtual const double& getConstRefToTt() const       {return T_t;};
    virtual double& getRefToWt()                        {return wt;};
    virtual const double& getConstRefToWt() const       {return wt;};
    virtual double& getRefToZs()                        {return zs;};
    virtual const double& getConstRefToZs() const       {return zs;};

    virtual double& getRefToTRefR()                         {return TRefR;};
    virtual const double& getConstRefToTRefR() const        {return TRefR;};
    virtual double& getRefToTRefG()                         {return TRefG;};
    virtual const double& getConstRefToTRefG() const        {return TRefG;};
    virtual double& getRefToTRefAM()                         {return TRefAM;};
    virtual const double& getConstRefToTRefAM() const        {return TRefAM;};
    virtual double& getRefToTcrys()                         {return Tcrys;};
    virtual const double& getConstRefToTcrys() const        {return Tcrys;};
    virtual double& getRefToTmelt()                         {return Tmelt;};
    virtual const double& getConstRefToTmelt() const        {return Tmelt;};
    virtual double& getRefToWcrys()                         {return Wcrys;};
    virtual const double& getConstRefToWcrys() const        {return Wcrys;};
    virtual double& getRefToWmelt()                         {return Wmelt;};
    virtual const double& getConstRefToWmelt() const        {return Wmelt;};
    virtual double& getRefToAlphaig()                       {return alphaig;};
    virtual const double& getConstRefToAlphaig() const      {return alphaig;};
#if 0
    virtual STensor3& getRefToAGl()                     {return AGl;};
    virtual const STensor3& getConstRefToAGl() const    {return AGl;};
    virtual STensor3& getRefToARl()                     {return ARl;};
    virtual const STensor3& getConstRefToARl() const    {return ARl;};
    virtual STensor3& getRefToAAMl()                    {return AAMl;};
    virtual const STensor3& getConstRefToAAMl() const   {return AAMl;};
#endif

    virtual STensor3& getRefToADl()                     {return ADl;};
    virtual const STensor3& getConstRefToADl() const    {return ADl;};

    virtual STensor3& getRefToEnThG()                     {return EnThG;};
    virtual const STensor3& getConstRefToEnThG() const    {return EnThG;};
    virtual STensor3& getRefToEnThR()                     {return EnThR;};
    virtual const STensor3& getConstRefToEnThR() const    {return EnThR;};
    virtual STensor3& getRefToEnThAM()                     {return EnThAM;};
    virtual const STensor3& getConstRefToEnThAM() const    {return EnThAM;};

    virtual STensor3 &getRefToFthAM()                        {return FthAM;};
    virtual const STensor3 &getConstRefToFthAM() const       {return FthAM;};
    virtual STensor3& getRefToFfAM()                         {return FfAM;};
    virtual const STensor3& getConstRefToFfAM() const        {return FfAM;};
    virtual STensor3 &getRefToFpAM()                         {return qAM->getRefToFp();};
    virtual const STensor3 &getConstRefToFpAM() const        {return qAM->getConstRefToFp();};
    virtual STensor3 &getRefToFveAM()                        {return qAM->getRefToFe();};
    virtual const STensor3 &getConstRefToFveAM() const       {return qAM->getConstRefToFe();};
    virtual double& getRefToEquivalentPlasticDefoAM()                     {return qAM->getRefToEqPlasticStrain();};
    virtual const double& getConstRefToEquivalentPlasticDefoAM() const    {return qAM->getConstRefToEqPlasticStrain();};




    virtual double &getRefToElasticEnergyPartG() {return qG->getRefToElasticEnergyPart();};
    virtual const double &getConstRefToElasticEnergyPartG() const {return qG->getConstRefToElasticEnergyPart();};
    virtual double &getRefToViscousEnergyPartG() {return qG->getRefToViscousEnergyPart();};
    virtual const double &getConstRefToViscousEnergyPartG() const {return qG->getConstRefToViscousEnergyPart();};
    virtual double &getRefToPlasticEnergyPartG() {return qG->getRefToPlasticEnergyPart();};
    virtual const double &getConstRefToPlasticEnergyPartG() const {return qG->getConstRefToPlasticEnergyPart();};

    virtual STensor3 &getRefToDElasticEnergyPartGdF() {return qG->getRefToDElasticEnergyPartdF();}
    virtual const STensor3 &getConstRefToDElasticEnergyPartGdF() const {return qG->getConstRefToDElasticEnergyPartdF();}
    virtual STensor3 &getRefToDViscousEnergyPartGdF() {return qG->getRefToDViscousEnergyPartdF();}
    virtual const STensor3 &getConstRefToDViscousEnergyPartGdF() const {return qG->getConstRefToDViscousEnergyPartdF();}
    virtual STensor3 &getRefToDPlasticEnergyPartGdF() {return qG->getRefToDPlasticEnergyPartdF();}
    virtual const STensor3 &getConstRefToDPlasticEnergyPartGdF() const {return qG->getConstRefToDPlasticEnergyPartdF();}


    virtual double &getRefToElasticEnergyPartR() {return qR->getRefToElasticEnergyPart();};
    virtual const double &getConstRefToElasticEnergyPartR() const {return qR->getConstRefToElasticEnergyPart();};
    virtual double &getRefToViscousEnergyPartR() {return qR->getRefToViscousEnergyPart();};
    virtual const double &getConstRefToViscousEnergyPartR() const {return qR->getConstRefToViscousEnergyPart();};
    virtual double &getRefToPlasticEnergyPartR() {return qR->getRefToPlasticEnergyPart();};
    virtual const double &getConstRefToPlasticEnergyPartR() const {return qR->getConstRefToPlasticEnergyPart();};

    virtual STensor3 &getRefToDElasticEnergyPartRdF() {return qR->getRefToDElasticEnergyPartdF();}
    virtual const STensor3 &getConstRefToDElasticEnergyPartRdF() const {return qR->getConstRefToDElasticEnergyPartdF();}
    virtual STensor3 &getRefToDViscousEnergyPartRdF() {return qR->getRefToDViscousEnergyPartdF();}
    virtual const STensor3 &getConstRefToDViscousEnergyPartRdF() const {return qR->getConstRefToDViscousEnergyPartdF();}
    virtual STensor3 &getRefToDPlasticEnergyPartRdF() {return qR->getRefToDPlasticEnergyPartdF();}
    virtual const STensor3 &getConstRefToDPlasticEnergyPartRdF() const {return qR->getConstRefToDPlasticEnergyPartdF();}

    virtual double &getRefToElasticEnergyPartAM() {return qAM->getRefToElasticEnergyPart();};
    virtual const double &getConstRefToElasticEnergyPartAM() const {return qAM->getConstRefToElasticEnergyPart();};
    virtual double &getRefToViscousEnergyPartAM() {return qAM->getRefToViscousEnergyPart();};
    virtual const double &getConstRefToViscousEnergyPartAM() const {return qAM->getConstRefToViscousEnergyPart();};
    virtual double &getRefToPlasticEnergyPartAM() {return qAM->getRefToPlasticEnergyPart();};
    virtual const double &getConstRefToPlasticEnergyPartAM() const {return qAM->getConstRefToPlasticEnergyPart();};

    virtual STensor3 &getRefToDElasticEnergyPartAMdF() {return qAM->getRefToDElasticEnergyPartdF();}
    virtual const STensor3 &getConstRefToDElasticEnergyPartAMdF() const {return qAM->getConstRefToDElasticEnergyPartdF();}
    virtual STensor3 &getRefToDViscousEnergyPartAMdF() {return qAM->getRefToDViscousEnergyPartdF();}
    virtual const STensor3 &getConstRefToDViscousEnergyPartAMdF() const {return qAM->getConstRefToDViscousEnergyPartdF();}
    virtual STensor3 &getRefToDPlasticEnergyPartAMdF() {return qAM->getRefToDPlasticEnergyPartdF();}
    virtual const STensor3 &getConstRefToDPlasticEnergyPartAMdF() const {return qAM->getConstRefToDPlasticEnergyPartdF();}
    

    virtual const IPHyperViscoElastoPlastic &getConstRefToIPHyperViscoElastoPlasticG() const {if(qG==NULL) Msg::Error("IPPhenomenologicalSMP : IPHyperViscoElastoPlastic G not initialized"); 
          return *qG;}
    virtual IPHyperViscoElastoPlastic &getRefToIPHyperViscoElastoPlasticG() {if(qG==NULL) Msg::Error("IPPhenomenologicalSMP : IPHyperViscoElastoPlastic G not initialized");
          return *qG;}
    virtual const IPHyperViscoElastoPlastic &getConstRefToIPHyperViscoElastoPlasticR() const {if(qR==NULL) Msg::Error("IPPhenomenologicalSMP : IPHyperViscoElastoPlastic R not initialized");
          return *qR;}
    virtual IPHyperViscoElastoPlastic &getRefToIPHyperViscoElastoPlasticR() {if(qR==NULL) Msg::Error("IPPhenomenologicalSMP : IPHyperViscoElastoPlastic R not initialized");
          return *qR;}
    virtual const IPHyperViscoElastoPlastic &getConstRefToIPHyperViscoElastoPlasticAM() const {if(qAM==NULL) Msg::Error("IPPhenomenologicalSMP : IPHyperViscoElastoPlastic AM not initialized");
          return *qAM;}
    virtual IPHyperViscoElastoPlastic &getRefToIPHyperViscoElastoPlasticAM() {if(qAM==NULL) Msg::Error("IPPhenomenologicalSMP : IPHyperViscoElastoPlastic AM not initialized");
          return *qAM;}


protected:
    IPPhenomenologicalSMP()
    {
      qG=NULL;
      qR=NULL;
      qAM=NULL;
      Msg::Error("IPPhenomenologicalSMP : no default constructor");
    }
    IPPhenomenologicalSMP& operator=(const IPPhenomenologicalSMP &source)
    {
      this->operator=((const IPVariable &)source);
      return *this;
    }
};
#endif // IPPHENOMENOLOGICALSMP_H_
