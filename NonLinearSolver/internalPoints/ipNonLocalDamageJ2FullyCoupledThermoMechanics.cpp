//
// Description: storing class for j2 thermo-elasto-plastic law with non local damage
//
//
// Author:  <L. Noels>, (C) 2021
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "ipNonLocalDamageJ2FullyCoupledThermoMechanics.h"
#include "restartManager.h"

IPLocalDamageJ2FullyCoupledThermoMechanics::IPLocalDamageJ2FullyCoupledThermoMechanics() : IPJ2ThermoMechanics(), _damageEnergy(0.)
{
  Msg::Error("IPNonLocalDamageJ2FullyCoupledThermoMechanics::IPNonLocalDamageJ2FullyCoupledThermoMechanics is not initialized with a damage IP Variable nor a IP CLength");
ipvDam=NULL;

};

IPLocalDamageJ2FullyCoupledThermoMechanics::IPLocalDamageJ2FullyCoupledThermoMechanics(const J2IsotropicHardening *j2IH,
                                                 const DamageLaw *daml) :
                                                    IPJ2ThermoMechanics(j2IH), _damageEnergy(0.)
{

  ipvDam=NULL;
  if(daml ==NULL) Msg::Error("IPNonLocalDamageJ2FullyCoupledThermoMechanics::IPNonLocalDamageJ2FullyCoupledThermoMechanics has no daml");
  daml->createIPVariable(ipvDam);

};

IPLocalDamageJ2FullyCoupledThermoMechanics::IPLocalDamageJ2FullyCoupledThermoMechanics(const IPLocalDamageJ2FullyCoupledThermoMechanics &source) : IPJ2ThermoMechanics(source),_damageEnergy(source._damageEnergy)
{
  ipvDam = NULL;
  if(source.ipvDam != NULL)
  {
    ipvDam = dynamic_cast<IPDamage*>(source.ipvDam->clone());
  }
}
IPLocalDamageJ2FullyCoupledThermoMechanics& IPLocalDamageJ2FullyCoupledThermoMechanics::operator=(const IPVariable &source)
{
  IPJ2ThermoMechanics::operator=(source);
  const IPLocalDamageJ2FullyCoupledThermoMechanics* src = dynamic_cast<const IPLocalDamageJ2FullyCoupledThermoMechanics*>(&source);
  if(src != NULL)
  {
    _damageEnergy = src->_damageEnergy;
    if(src->ipvDam != NULL)
    {
      if (ipvDam != NULL)
        ipvDam->operator=(*dynamic_cast<const IPVariable*>(src->ipvDam));
      else
        ipvDam= dynamic_cast<IPDamage*>(src->ipvDam->clone());
    }
    else{
      if(ipvDam != NULL) delete ipvDam; ipvDam = NULL;
    }
  }
  return *this;
}

void IPLocalDamageJ2FullyCoupledThermoMechanics::restart()
{
  IPJ2ThermoMechanics::restart();
  restartManager::restart(ipvDam);
  restartManager::restart(_damageEnergy);
  return;
}



IPNonLocalDamageJ2FullyCoupledThermoMechanics::IPNonLocalDamageJ2FullyCoupledThermoMechanics() : IPLocalDamageJ2FullyCoupledThermoMechanics(),
							_nonlocalPlasticStrain (0), nonLocalToLocal(false),_DirreversibleEnergyDNonLocalVariable(0.)
{
  Msg::Error("IPNonLocalDamageJ2FullyCoupledThermoMechanics::IPNonLocalDamageJ2FullyCoupledThermoMechanics is not initialized with a Characteritsic IP Variable nor a IP CLength");
  ipvCL=NULL;

};

IPNonLocalDamageJ2FullyCoupledThermoMechanics::IPNonLocalDamageJ2FullyCoupledThermoMechanics(const J2IsotropicHardening *j2IH,
                                                 const CLengthLaw *cll, const DamageLaw *daml) :
                                                    IPLocalDamageJ2FullyCoupledThermoMechanics(j2IH,daml),
						_nonlocalPlasticStrain (0),nonLocalToLocal(false),
						_DirreversibleEnergyDNonLocalVariable(0.)
{
  ipvCL=NULL;
  if(cll ==NULL) Msg::Error("IPNonLocalDamageJ2FullyCoupledThermoMechanics::IPNonLocalDamageJ2FullyCoupledThermoMechanics has no cll");
  cll->createIPVariable(ipvCL);
};




IPNonLocalDamageJ2FullyCoupledThermoMechanics::IPNonLocalDamageJ2FullyCoupledThermoMechanics(const IPNonLocalDamageJ2FullyCoupledThermoMechanics &source) : IPLocalDamageJ2FullyCoupledThermoMechanics(source)
{
  _nonlocalPlasticStrain=source._nonlocalPlasticStrain;
  nonLocalToLocal = source.nonLocalToLocal;
  _DirreversibleEnergyDNonLocalVariable = source._DirreversibleEnergyDNonLocalVariable;
  ipvCL = NULL;
  if(source.ipvCL != NULL)
  {
    ipvCL = dynamic_cast<IPCLength*>(source.ipvCL->clone());
  }
}
IPNonLocalDamageJ2FullyCoupledThermoMechanics& IPNonLocalDamageJ2FullyCoupledThermoMechanics::operator=(const IPVariable &source)
{
  IPLocalDamageJ2FullyCoupledThermoMechanics::operator=(source);
  const IPNonLocalDamageJ2FullyCoupledThermoMechanics* src = dynamic_cast<const IPNonLocalDamageJ2FullyCoupledThermoMechanics*>(&source);
  if(src != NULL)
  {
    _nonlocalPlasticStrain=src->_nonlocalPlasticStrain;
    nonLocalToLocal = src->nonLocalToLocal;
    _DirreversibleEnergyDNonLocalVariable = src->_DirreversibleEnergyDNonLocalVariable;
    
    if(src->ipvCL != NULL)
    {
      if (ipvCL != NULL)
        ipvCL->operator=(*dynamic_cast<const IPCLength*>(src->ipvCL));
      else
        ipvCL=dynamic_cast<IPCLength*>(src->ipvCL->clone());
    }
    else
    {
      if(ipvCL != NULL) delete ipvCL; ipvCL = NULL;
    }
  }
  return *this;
}

void IPNonLocalDamageJ2FullyCoupledThermoMechanics::restart()
{
  IPLocalDamageJ2FullyCoupledThermoMechanics::restart();
  restartManager::restart(_nonlocalPlasticStrain);
  restartManager::restart(ipvCL);
  restartManager::restart(nonLocalToLocal);
  restartManager::restart(_DirreversibleEnergyDNonLocalVariable);
  return;
}

