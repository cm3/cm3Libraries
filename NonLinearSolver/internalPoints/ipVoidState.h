//
// C++ Interface: Void State
//
// Description: Storing class for void state behind porosity
//
//
// Author:  <V.-D. Nguyen>, (C) 2019
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef IPVOIDSTATE_H_
#define IPVOIDSTATE_H_

#include "ipvariable.h"

class IPVoidState{
  protected:
    // Geometrical parameters and their derivatives
    double _roughChi; //
    double _Chi;
    double _DChiDyieldfV;
    double _DChiDhatD;
    double _DChiDhatQ;
    double _DChiDhatP;

      // Void aspect ratio
    double _W;
    double _DWDyieldfV;
    double _DWDhatD;
    double _DWDhatQ;
    double _DWDhatP;

      // Void shape factor
    double _gamma;
    double _DgammaDyieldfV;
    double _DgammaDhatD;
    double _DgammaDhatQ;
    double _DgammaDhatP;

      // Void spacing ratio
    double _lambda;
    double _DlambdaDyieldfV;
    double _DlambdaDhatD;
    double _DlambdaDhatQ;
    double _DlambdaDhatP;

    bool _isSaturated;

  public:
    IPVoidState(const double fV0, const double lambda0, const double W0, const double gamma0);
    IPVoidState(const IPVoidState& src);
    virtual IPVoidState& operator=(const IPVoidState& src);
    virtual void copyAndZeroDerivativesFrom(const IPVoidState& src);
    virtual ~IPVoidState(){}

    virtual IPVoidState* clone() const  {return new IPVoidState(*this);};
    virtual void restart();

    virtual bool isSaturated() const {return _isSaturated;}
    virtual void saturate(const bool fl){_isSaturated = fl;};

      // Geometrical parameters and their derivatives
    virtual double getRoughVoidLigamentRatio() const {return _roughChi;};
    virtual double& getRefToRoughVoidLigamentRatio() {return _roughChi;};

    virtual double getVoidLigamentRatio() const {return _Chi;};
    virtual double getDVoidLigamentRatioDYieldPorosity() const {return _DChiDyieldfV;};
    virtual double getDVoidLigamentRatioDVolumetricPlasticDeformation() const {return _DChiDhatQ;};
    virtual double getDVoidLigamentRatioDDeviatoricPlasticDeformation() const {return _DChiDhatD;};
    virtual double getDVoidLigamentRatioDMatrixPlasticDeformation() const {return _DChiDhatP;};

    virtual double& getRefToVoidLigamentRatio() {return _Chi;};
    virtual double& getRefToDVoidLigamentRatioDYieldPorosity() {return _DChiDyieldfV;};
    virtual double& getRefToDVoidLigamentRatioDVolumetricPlasticDeformation() {return _DChiDhatQ;};
    virtual double& getRefToDVoidLigamentRatioDDeviatoricPlasticDeformation() {return _DChiDhatD;};
    virtual double& getRefToDVoidLigamentRatioDMatrixPlasticDeformation() {return _DChiDhatP;};

    virtual double getVoidAspectRatio() const {return _W;};
    virtual double getDVoidAspectRatioDYieldPorosity() const {return _DWDyieldfV;};
    virtual double getDVoidAspectRatioDVolumetricPlasticDeformation() const {return _DWDhatQ;};
    virtual double getDVoidAspectRatioDDeviatoricPlasticDeformation() const {return _DWDhatD;};
    virtual double getDVoidAspectRatioDMatrixPlasticDeformation() const {return _DWDhatP;};

    virtual double& getRefToVoidAspectRatio() {return _W;};
    virtual double& getRefToDVoidAspectRatioDYieldPorosity() {return _DWDyieldfV;};
    virtual double& getRefToDVoidAspectRatioDVolumetricPlasticDeformation() {return _DWDhatQ;};
    virtual double& getRefToDVoidAspectRatioDDeviatoricPlasticDeformation() {return _DWDhatD;};
    virtual double& getRefToDVoidAspectRatioDMatrixPlasticDeformation() {return _DWDhatP;};

    virtual double getVoidSpacingRatio() const {return _lambda;};
    virtual double getDVoidSpacingRatioDYieldPorosity() const {return _DlambdaDyieldfV;};
    virtual double getDVoidSpacingRatioDVolumetricPlasticDeformation() const {return _DlambdaDhatQ;};
    virtual double getDVoidSpacingRatioDDeviatoricPlasticDeformation() const {return _DlambdaDhatD;};
    virtual double getDVoidSpacingRatioDMatrixPlasticDeformation() const {return _DlambdaDhatP;};

    virtual double& getRefToVoidSpacingRatio() {return _lambda;};
    virtual double& getRefToDVoidSpacingRatioDYieldPorosity() {return _DlambdaDyieldfV;};
    virtual double& getRefToDVoidSpacingRatioDVolumetricPlasticDeformation() {return _DlambdaDhatQ;};
    virtual double& getRefToDVoidSpacingRatioDDeviatoricPlasticDeformation() {return _DlambdaDhatD;};
    virtual double& getRefToDVoidSpacingRatioDMatrixPlasticDeformation() {return _DlambdaDhatP;};

    virtual double getVoidShapeFactor() const {return _gamma;};
    virtual double getDVoidShapeFactorDYieldPorosity() const {return _DgammaDyieldfV;};
    virtual double getDVoidShapeFactorDVolumetricPlasticDeformation() const {return _DgammaDhatQ;};
    virtual double getDVoidShapeFactorDDeviatoricPlasticDeformation() const {return _DgammaDhatD;};
    virtual double getDVoidShapeFactorDMatrixPlasticDeformation() const {return _DgammaDhatP;};

    virtual double& getRefToVoidShapeFactor() {return _gamma;};
    virtual double& getRefToDVoidShapeFactorDYieldPorosity() {return _DgammaDyieldfV;};
    virtual double& getRefToDVoidShapeFactorDVolumetricPlasticDeformation() {return _DgammaDhatQ;};
    virtual double& getRefToDVoidShapeFactorDDeviatoricPlasticDeformation() {return _DgammaDhatD;};
    virtual double& getRefToDVoidShapeFactorDMatrixPlasticDeformation() {return _DgammaDhatP;};
};
#endif //IPVOIDSTATE_H_
