//
// C++ Interface: Quadrature Rule
//
// Description: Define 1D Gauss-Lobatto quadrature rule (number of point 3 to 6)
//
//
// Author:  <Gauthier BECKER>, (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef GAUSSLOBATTOQUADRATURE_H_
#define GAUSSLOBATTOQUADRATURE_H_
#include "GaussIntegration.h"
#include <math.h>
// As this rule considers point at extremities the minimum number of point is 3x3
IntPt *getGaussLobattoQuadrature2DPtsTri(int order);
int getNGaussLobattoQuadrature2DPtsTri(int order);
IntPt *getGaussLobattoQuadrature2DPtsQuad(int order);
int getNGaussLobattoQuadrature2DPtsQuad(int order);

// As this rule considers point at extremities the minimum number of point is 3
// number of points = (order+1)/2
IntPt *getGaussLobattoQuadrature1DPts(int order);
int getNGaussLobattoQuadrature1DPts(int order);

// integration on a point
IntPt *getGaussLobattoQuadrature0DPts(int order);
int getNGaussLobattoQuadrature0DPts(int order);
#endif // GAUSSLOBATTOQUADRATURE_H_
