#ifndef MATRIX_OPERATIONS_H
#define MATRIX_OPERATIONS_H 1



/* Sum of two tensor of the second order, each one can be previously multiplied by a scalar*/
void addtens2 (double,double*,double,double*,double*);

/*  Sum of two tensor of the fourth order, each one can be previously multiplied by a scalar*/
void addtens4 (double, double **, double, double **, double **);

/*  Sum of two tensor of the sixth order, each one can be previously multiplied by a scalar*/
void addtens6 (double, double ***, double, double ***, double ***);

/*Contraction of two second-order tensor (3*3) into a second-order tensor  */
void contraction1 (double**A, double** B, double** C);

/* Contraction of two second-order tensor (6*1) into a scalar */
double contraction22 (double*, double*);

/* Contraction of a fourth-order tensor (6*6) and a second-order tensor (6*1)*/
void contraction42 (double**, double*,double*);

/* Contraction of two fourth-order tensors (6*6)*/
void contraction44 (double **, double **, double **);

/* Contraction of a sixth-order tensor (6*6*6) with a second-order tensor (6*1)*/
void contraction62 (double***, double*, double **, int);

/* Tensorial product (6*6) of two second-order tensors*/
void produit_tensoriel (double *, double *, double **);

/* transpose a matrix */
void transpose(double** Tinit, double** Ttrans, int m, int n);

/* Compute the von Mises equivalent stress */
double vonmises(double *);

/* Compute the equivalent strain scalar */
double eqstrn(double *);

/* Compute the elastic stiffness tensor from E and nu */
void compute_Hooke(double, double, double **);

/* COMPUTE THE ANISOTROPIC ELASTIC OPERATOR FROM EX,NUXX and GXX in global coordinates*/
void compute_AnisoOperator(double*, double*, double **);

/* Extract the isotropic part of a fourth-order tensor, general method */
void extractiso (double **, double **);

/* Make an isotropic tensor from given bulk and shear moduli*/
void makeiso(double, double, double**);

/* Copy a vector into another */
void copyvect(double *A, double *B, int size);

/* Copy a matrix into another */
void copymatr(double**, double**, int, int);

/* Compute the trace of a second order tensor */
double trace(double *tens2);

/* Extract the deviatoric part of a second order tensor */
void dev(double *tens2, double *dev_tens2);

/* LU decomposition of a n*n matrix */
void ludcmp(double **, int, int *, double *, int *);

/*forward and backward substitution */
void lubksb (double **, int , int *, double []);

/* Inversion of a n*n matrix*/
void inverse (double **, double **, int *, int );

/* Check if a fourth order matrix is isotropic*/
int isisotropic(double**);

/* Memory allocation for a table of pointers (1D) */
void mallocvector (double**,int);

/* Memory allocation for a table of pointers (2D) */
void mallocmatrix (double ***, int, int);

/* Memory allocation for a table of pointers (3D) */
void malloctens3 (double ****, int, int, int);

/* Memory allocation for a table of pointers (4D) */
void malloctens4 (double *****, int, int, int, int);

/* Free memory of a table of pointers (2D) */
void freematrix (double **, int);

/* Free memory of a table of pointers (3D) */
void freetens3 (double ***, int, int);

/* Free memory of a table of pointers (3D) */
void freetens4 (double ****, int, int, int);

/* Print on the screen values of a m*1 vector */
void printvector(double *,int);

/* Print on the screen values of a m*n matrix */
void printmatr(double **, int, int);

/*check if allc components of a vector are zero*/
int iszero(double*,int,double);

/* reset all components of a vector/tensor to zero */
void cleartens2(double* T);
void cleartens4(double** T);
void cleartens6(double*** T);

/*creat cg -- charateristic length tensor in globle axis*/
void creat_cg(double *pro, double** cg);
void creat_cg(double pro, double** cg);

int Jacobian_Eigen( int n, double** S, double* e, double** U, double*** dedS);
int Jacobian_Eigen( int n, double** S, double* e, double** U);

/* convert vector to matrix */
void vect_to_matrix (int, double*, double**);

/* convert matrix to vector */
void matrix_to_vect (int, double**, double*);

/*rotation of strain/stress vector*/
void Rot_vect (double *R, double*V);

/*compute the determinant of matrix*/
double Determinant(double**,int);


#endif

