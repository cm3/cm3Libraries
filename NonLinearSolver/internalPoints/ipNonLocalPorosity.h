//
// Description: storing class for gurson model
// Author:  <L. Noels>, (C) 2013
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef IPNONLOCALPOROSITY_H_
#define IPNONLOCALPOROSITY_H_
#include "ipvariable.h"
#include "STensor3.h"
#include "ipHardening.h"
#include "j2IsotropicHardening.h"
#include "ipCLength.h"
#include "ipNucleation.h"
#include "ipCoalescence.h"
#include "CLengthLaw.h"
#include "NucleationLaw.h"
#include "CoalescenceLaw.h"
#include "ipDamage.h"
#include "ipCLength.h"
#include "DamageLaw.h"
#include "ipVoidState.h"
#include "voidStateEvolutionLaw.h"


class IPNonLocalPorosity : public IPVariableMechanics
{
protected:
// Internal laws Ipvs for each internal laws
  // -Non-local lenght
  std::vector<IPCLength*> ipvCL;
  // -Hardening law
  IPJ2IsotropicHardening* ipvJ2IsotropicHardening;
  // -Nucleation law vector:
  IPNucleation* ipvNucleation; // contains the IP corresponding to each Gurson damage nucleatin law

  // -Coalesnce law and related geometrical parameters
  IPVoidState* ipvVoidState; // Void state
  IPCoalescence* ipvCoal;    // Coalescence law (concentration factor from void state)


// Proper internal variables

  const STensor3* _deformationGradient;

  const SVector3* _currentOutwardNormal; // current normal at Gauss point
  const SVector3* _referenceOutwardNormal; // refrence nomral at Gauss point

  // Internal variables
  double _fVinitial; // initial porosity
  double _eplmatrix;     // equivalent plastic strain in the matrix (hatp)
  double _nonlocalEplmatrix; // nonlocal equivalent plastic strain
  double _fV;            // local porosity
  double _fVtilde;       // nonlocal porosity
  double _fVstar;        //  corrected porosity for GTN yield surface
  double _yieldfV;       // porosity will be used in yield surface
  double _lnfV; // log of porosity
  double _lnfVtilde;  // nonlocal log of porosity

  double _hatq;  // volumetric plastic deformation = tr(Dp)
  double _nonlocalHatQ; // nonlocal volumetric plastic deformation

  double _eplMacro; // deviatoric plastic equivalent deformation sqrt(2/3*devDp:DevDp)
  double _eplMacroNonLocal; // nonlocal counterpart

  double _yieldStress; // current viscoplastic yield stress = initial part + hardening part+ viscoplastic part

  STensor3 _Fp;    // plastic part of the deformation gradient
  STensor3 _Ee; // elastic natural strain tensor
  STensor3 _corKir; // corotational Kirchhoff stress

  // Damage and transition managing
  bool _dissipationBlocked; // True if dissipation is blocked at the IPv
  bool _NonLocalToLocal;    // True if transition from non-local to local allowed
  bool _failed; // if Gurson is in failure
  bool _dissipationActive; // true if dissipation is active

  // Energies
  double _elasticEnergy; // elastic energy stored
  double _plasticEnergy; // plastic energy
  double _irreversibleEnergy;

  // for path following method // does not used for other variable
  STensor3 _DirreversibleEnergyDF;
  std::vector<double> _DirreversibleEnergyDtildefV;

  // to store value
  STensor3 _DplasticEnergyDF;
  std::vector<double> _DplasticEnergyDtildefV;
  double _DplasticEnergyDT; // thermoplasticity
  double _DplasticEnergyDEplmatrix; // to estimate derivatives of eplmatrix

  double _T; // temperature
  double _thermalEnergy; // thermal energy
  double _ThermoElasticCoupling; //
  
  bool _successFlag; // true if contitutive law is successfully passed in GP 
  
  STensor43 _anisotropicMatrix;
  STensor63 _DanisotropicMatrixDF;
  STensor63 _DanisotropicMatrixDEp;
  
  double _effectiveSVM;
  double _effectivePressure; 

 public:
  // Constructor & destructor
    // Default constructor blocked
  IPNonLocalPorosity();
  IPNonLocalPorosity(const IPNonLocalPorosity &source);
  virtual IPNonLocalPorosity& operator=(const IPVariable &source);
    // Real used constructor
  IPNonLocalPorosity(double fVinitial, const J2IsotropicHardening* j2IH, const std::vector<CLengthLaw*>* cll,
                     const NucleationLaw* nuclLaw,
                     const CoalescenceLaw* coaleslaw, const voidStateEvolutionLaw* evolLaw);

  virtual ~IPNonLocalPorosity();
  virtual IPVariable* clone() const {return new IPNonLocalPorosity(*this);};
  
  // General functions
  virtual void restart();
  
  // MPI function to define value to be commmunicated between processes
   #if defined(HAVE_MPI)
  virtual int numberValuesToCommunicateMPI()const{return 1;}
  virtual void fillValuesToCommunicateMPI(double *arrayMPI)const{arrayMPI[0]=_fV;};
  virtual void getValuesFromMPI(const double *arrayMPI){_fV=arrayMPI[0];};
  #endif // HAVE_MPI
  
  // Acces functions
  virtual double getThermalEnergy() const {return _thermalEnergy;};
  virtual double& getRefToThermalEnergy(){return _thermalEnergy;};

  virtual const double& getConstRefToThermoelasticCoupling() const{return _ThermoElasticCoupling;};
  virtual double& getRefToThermoelasticCoupling() {return _ThermoElasticCoupling;};

  virtual bool& getRefToDissipationActive(){return _dissipationActive;};
  virtual bool dissipationIsActive() const {return _dissipationActive;};

  virtual void setTemperature(const double T) {_T = T;};
  virtual double getTemperature() const {return _T;};

  virtual void setRefToDeformationGradient(const STensor3& F){
		_deformationGradient = &F;
	}
	virtual const STensor3& getConstRefToDeformationGradient() const{
		if (_deformationGradient == NULL) Msg::Error("_deformationGradient must be set IPNonLocalPorosity");
		return *_deformationGradient;
	}

	virtual void setRefToCurrentOutwardNormal(const SVector3& n){
		_currentOutwardNormal = &n;
	}

	virtual const SVector3& getConstRefToCurrentOutwardNormal() const{
		if (_currentOutwardNormal == NULL) Msg::Error("_currentOutwardNormal must be set IPNonLocalPorosity");
		return *_currentOutwardNormal;
	}

	virtual void setRefToReferenceOutwardNormal(const SVector3& n){
		_referenceOutwardNormal = &n;
	}
	virtual const SVector3& getConstRefToReferenceOutwardNormal() const{
		if (_referenceOutwardNormal == NULL) Msg::Error("_referenceOutwardNormal must be set IPNonLocalPorosity");
		return *_referenceOutwardNormal;
	}
	//
  virtual void setFailed(const bool fl) {_failed = fl;};
  virtual bool isFailed() const {return _failed;}
  // Damage managing
  virtual void blockDissipation(const bool fl){_dissipationBlocked = fl;};
  virtual bool dissipationIsBlocked() const {return _dissipationBlocked;};

  // nonlocal to local
  virtual void setNonLocalToLocal(const bool fl){_NonLocalToLocal = fl;};
  virtual bool getNonLocalToLocal() const {return _NonLocalToLocal;};

  // Access functions - Energy
  virtual double &getRefToElasticEnergy() {return _elasticEnergy;};
  virtual double defoEnergy() const { return _elasticEnergy;};

  virtual double& getRefToPlasticEnergy() {return _plasticEnergy;};
  virtual double plasticEnergy() const {return _plasticEnergy;};

  // Those need to be implemented ???
  virtual double damageEnergy() const {return 0.;}; // dissipation by damage
  
  virtual double getConstRefToEffectiveSVM() const {return _effectiveSVM;}
  virtual double& getRefToEffectiveSVM() {return _effectiveSVM;}
  
  virtual double getConstRefToEffectivePressure() const {return _effectivePressure;}
  virtual double& getRefToEffectivePressure() {return _effectivePressure;}

  virtual double irreversibleEnergy() const {return _irreversibleEnergy;};
  virtual double & getRefToIrreversibleEnergy() {return _irreversibleEnergy;};

  virtual const STensor3& getConstRefToDIrreversibleEnergyDF() const{return _DirreversibleEnergyDF;};
  virtual STensor3& getRefToDIrreversibleEnergyDF() {return _DirreversibleEnergyDF;};

  virtual const double& getConstRefToDIrreversibleEnergyDNonLocalVariable(const int i) const{return _DirreversibleEnergyDtildefV[i];};
  virtual double& getRefToDIrreversibleEnergyDNonLocalVariable(const int i) {return _DirreversibleEnergyDtildefV[i];};

  virtual const STensor3& getConstRefToDPlasticEnergyDF() const{return _DplasticEnergyDF;};
  virtual STensor3& getRefToDPlasticEnergyDF() {return _DplasticEnergyDF;};

  virtual const double& getConstRefToDPlasticEnergyDNonLocalVariable(const int i) const{return _DplasticEnergyDtildefV[i];};
  virtual double& getRefToDPlasticEnergyDNonLocalVariable(const int i) {return _DplasticEnergyDtildefV[i];};

  virtual const double& getConstRefToDPlasticEnergyDT() const{ return _DplasticEnergyDT;};//-------added
  virtual double& getRefToDPlasticEnergyDT() {return _DplasticEnergyDT;};//-------added

  virtual const double& getConstRefToDPlasticEnergyDMatrixPlasticStrain() const {return _DplasticEnergyDEplmatrix;};
  virtual double& getRefToDPlasticEnergyDMatrixPlasticStrain() {return _DplasticEnergyDEplmatrix;};

  // Access functions - Internal variables
  virtual double getDamage() const{return getYieldPorosity();};

  virtual double getInitialPorosity() const {return _fVinitial;};
  virtual double& getRefToInitialPorosity(){return _fVinitial;};

  virtual double getLocalMatrixPlasticStrain() const { return _eplmatrix;}
  virtual double &getRefToLocalMatrixPlasticStrain() { return _eplmatrix;}

  virtual double getNonLocalMatrixPlasticStrain() const { return _nonlocalEplmatrix;}
  virtual double &getRefToNonLocalMatrixPlasticStrain() { return _nonlocalEplmatrix;}

  virtual double getLocalVolumetricPlasticStrain() const {return _hatq;};
  virtual double& getRefToLocalVolumetricPlasticStrain() {return _hatq;};
  
  virtual double getNonLocalVolumetricPlasticStrain() const {return _nonlocalHatQ;};
  virtual double& getRefToNonLocalVolumetricPlasticStrain() {return _nonlocalHatQ;};

  virtual double getLocalDeviatoricPlasticStrain() const {return _eplMacro;};
  virtual double& getRefToLocalDeviatoricPlasticStrain() {return _eplMacro;};

  virtual double getNonLocalDeviatoricPlasticStrain() const {return _eplMacroNonLocal;};
  virtual double& getRefToNonLocalDeviatoricPlasticStrain() {return _eplMacroNonLocal;};


  virtual double getLocalLogarithmPorosity() const { return _lnfV;}
  virtual double& getRefToLocalLogarithmPorosity() { return _lnfV;}

  virtual double getNonLocalLogarithmPorosity() const { return _lnfVtilde;}
  virtual double& getRefToNonLocalLogarithmPorosity() { return _lnfVtilde;}

  virtual double getLocalPorosity() const { return _fV;}
  virtual double &getRefToLocalPorosity() { return _fV;}

  virtual double getNonLocalPorosity() const { return _fVtilde;}
  virtual double &getRefToNonLocalPorosity() { return _fVtilde;}

  virtual double getCorrectedPorosity() const { return _fVstar;}
  virtual double &getRefToCorrectedPorosity() { return _fVstar;}

  virtual double getYieldPorosity() const {return _yieldfV;};
  virtual double& getRefToYieldPorosity() {return _yieldfV;};

  virtual double getCurrentViscoplasticYieldStress() const {return _yieldStress;};
  virtual double& getRefToCurrentViscoplasticYieldStress() {return _yieldStress;};

  virtual const STensor3& getConstRefToFp() const {return _Fp;}
  virtual STensor3& getRefToFp(){return _Fp;}

  virtual const STensor3& getConstRefToElasticDeformation() const{return _Ee;}
  virtual STensor3& getRefToElasticDeformation(){return _Ee;}

  virtual const STensor3& getConstRefToCorotationalKirchhoffStress() const {return _corKir;};
  virtual STensor3& getRefToCorotationalKirchhoffStress() {return _corKir;};




  // Access functions - Included IPVs
  // - CL length
  virtual const IPCLength& getConstRefToIPCLength(const int i) const{
    if(ipvCL[i]==NULL)
      Msg::Error("IPNonLocalPorosity: ipvCL %d not initialized",i);
    return *ipvCL[i];
  }
  virtual IPCLength& getRefToIPCLength(const int i){
    if(ipvCL[i]==NULL)
      Msg::Error("IPNonLocalPorosity: ipvCL %d not initialized",i);
    return *ipvCL[i];
  }
  virtual const STensor3& getConstRefToCharacteristicLength(const int i) const{
    if(ipvCL[i]==NULL)
      Msg::Error("IPNonLocalPorosity::ipvCL %e not initialized",i);
    return ipvCL[i]->getConstRefToCL();
  }
  virtual STensor3& getRefToCharacteristicLength(const int i) const{
    if(ipvCL[i]==NULL)
      Msg::Error("IPNonLocalPorosity::ipvCL %d not initialized",i);
    return ipvCL[i]->getRefToCL();
  }
  
  // - J2 Hardening
  virtual const IPJ2IsotropicHardening& getConstRefToIPJ2IsotropicHardening() const{
    if(ipvJ2IsotropicHardening==NULL)
      Msg::Error("IPNonLocalPorosity: ipvJ2IsotropicHardening not initialized");
    return *ipvJ2IsotropicHardening;
  }
  virtual IPJ2IsotropicHardening& getRefToIPJ2IsotropicHardening(){
    if(ipvJ2IsotropicHardening==NULL)
      Msg::Error("IPNonLocalPorosity: ipvJ2IsotropicHardening not initialized");
    return *ipvJ2IsotropicHardening;
  }
  
  // - Nucleation law
  virtual const IPNucleation& getConstRefToIPNucleation() const{
    if(ipvNucleation ==NULL)
      Msg::Error("IPNonLocalPorosity::ipvNucleation not initialized");
    return *ipvNucleation;
  }
  virtual IPNucleation& getRefToIPNucleation(){
    if(ipvNucleation ==NULL)
      Msg::Error("IPNonLocalPorosity::ipvNucleation not initialized");
    return *ipvNucleation;
  }
  
  // - Nucleation function law vector
  virtual const IPNucleationFunction& getConstRefToIPNucleationFunction(const int i) const
  {
    if (&ipvNucleation->getConstRefToIPNucleationFunction(i) == NULL){
      Msg::Error("IPNonLocalPorosity::IPNucleationFunction %d not initialized", i);
    }
    return ipvNucleation->getConstRefToIPNucleationFunction(i);
  };
  
  virtual IPNucleationFunction& getRefToIPNucleationFunction(const int i)
  {
    if (&ipvNucleation->getRefToIPNucleationFunction(i) == NULL){
      Msg::Error("IPNonLocalPorosity::IPNucleationFunction %d not initialized", i);
    }
    return ipvNucleation->getRefToIPNucleationFunction(i);
  };

  // - Nucleation related variables
  virtual double getNucleatedPorosity() const
  {
    if (ipvNucleation == NULL) return 0.;
    else return ipvNucleation->getNucleatedPorosity();
  };
  
  virtual double getNucleatedPorosity(const int i) const
  {
    if (&ipvNucleation->getRefToIPNucleationFunction(i) == NULL) return 0.;
    else return ipvNucleation->getRefToIPNucleationFunction(i).getNucleatedPorosity();
  };
  
  virtual bool getNucleationOnset(const int i) const
  {
    if (&ipvNucleation->getRefToIPNucleationCriterion(i) == NULL) return false;
    else return ipvNucleation->getRefToIPNucleationFunction(i).getNucleatedPorosity();
  };


  virtual bool constitutiveSuccessFlag() const {return _successFlag;};
  virtual bool& getRefToConstitutiveSuccessFlag()  {return _successFlag;};

  // - Coalesncence and void state ipv
  virtual const IPCoalescence& getConstRefToIPCoalescence() const{
    if(ipvCoal==NULL)
      Msg::Error("IPNonLocalPorosity: ipvCoalescence not initialized");
    return *ipvCoal;
  }
  virtual IPCoalescence& getRefToIPCoalescence(){
    if(ipvCoal==NULL)
      Msg::Error("IPNonLocalPorosity: ipvCoalescence not initialized");
    return *ipvCoal;
  }
  virtual const IPVoidState& getConstRefToIPVoidState() const{
    if(ipvVoidState==NULL)
      Msg::Error("IPNonLocalPorosity: ipvVoidState not initialized");
    return *ipvVoidState;
  }
  virtual IPVoidState& getRefToIPVoidState(){
    if(ipvVoidState==NULL)
      Msg::Error("IPNonLocalPorosity: ipvVoidState not initialized");
    return *ipvVoidState;
  }
  
  virtual const STensor43& getConstRefToAnisotropicTensor() const {return _anisotropicMatrix;};
  virtual STensor43& getRefToAnisotropicTensor() {return _anisotropicMatrix;};
  
  virtual const STensor63& getConstRefToDAnisotropicTensorDF() const {return _DanisotropicMatrixDF;};
  virtual STensor63& getRefToDAnisotropicTensorDF() {return _DanisotropicMatrixDF;};
  
  virtual const STensor63& getConstRefToDAnisotropicTensorDEp() const {return _DanisotropicMatrixDEp;};
  virtual STensor63& getRefToDAnisotropicTensorDEp() {return _DanisotropicMatrixDEp;};
};







class IPNonLocalPorosityWithCleavage : public IPNonLocalPorosity
{
protected:
  // What are those ?????
    bool _passOnsetCleavage;
    double _cleavagePlasticDeformation;
    double _nonLocalCleavagePlasticDeformation;
    IPCLength* _ipvCLCleavage; // nonlocal length for cleavage
    IPDamage*  _ipvDamCleavage; // damage state
    double _damageEnergy;

public:
    IPNonLocalPorosityWithCleavage():IPNonLocalPorosity(),_passOnsetCleavage(false),_cleavagePlasticDeformation(0.),
          _nonLocalCleavagePlasticDeformation(0.),_ipvCLCleavage(NULL),_ipvDamCleavage(NULL), _damageEnergy(0.){};
    IPNonLocalPorosityWithCleavage(const IPNonLocalPorosityWithCleavage &src): IPNonLocalPorosity(src),
              _passOnsetCleavage(src._passOnsetCleavage),_cleavagePlasticDeformation(src._cleavagePlasticDeformation),
              _nonLocalCleavagePlasticDeformation(src._nonLocalCleavagePlasticDeformation),_damageEnergy(src._damageEnergy){

      _ipvCLCleavage = NULL;
      if(src._ipvCLCleavage != NULL){
        _ipvCLCleavage = dynamic_cast<IPCLength*>(src._ipvCLCleavage->clone());
      }

      _ipvDamCleavage = NULL;
      if(src._ipvDamCleavage != NULL){
        _ipvDamCleavage = dynamic_cast<IPDamage*>(src._ipvDamCleavage->clone());
      }

    }
    virtual IPNonLocalPorosityWithCleavage& operator=(const IPVariable &source){
      IPNonLocalPorosity::operator=(source);
      const IPNonLocalPorosityWithCleavage* psrc = dynamic_cast<const IPNonLocalPorosityWithCleavage*>(&source);
      if (psrc != NULL){
        _passOnsetCleavage = psrc->_passOnsetCleavage;
        _cleavagePlasticDeformation = psrc->_cleavagePlasticDeformation;
        _nonLocalCleavagePlasticDeformation = psrc->_nonLocalCleavagePlasticDeformation;
        _damageEnergy = psrc->_damageEnergy;

        if(psrc->_ipvCLCleavage != NULL){
          if (_ipvCLCleavage != NULL)
            _ipvCLCleavage->operator=(*dynamic_cast<const IPCLength*>(psrc->_ipvCLCleavage));
          else
            _ipvCLCleavage= dynamic_cast<IPCLength*>(psrc->_ipvCLCleavage->clone());
        }
        else{
          if(_ipvCLCleavage != NULL) delete _ipvCLCleavage; _ipvCLCleavage = NULL;
        }

        if(psrc->_ipvDamCleavage != NULL){
          if (_ipvDamCleavage != NULL)
            _ipvDamCleavage->operator=(*dynamic_cast<const IPVariable*>(psrc->_ipvDamCleavage));
          else
            _ipvDamCleavage= dynamic_cast<IPDamage*>(psrc->_ipvDamCleavage->clone());
        }
        else{
          if(_ipvDamCleavage != NULL) delete _ipvDamCleavage; _ipvDamCleavage = NULL;
        }
      }
      return *this;
    };
    IPNonLocalPorosityWithCleavage(double fVinitial, const J2IsotropicHardening *j2IH, 
                                   const std::vector<CLengthLaw*> *cll,
                                   const NucleationLaw* nuclLaw,
                                   const CoalescenceLaw* coaleslaw,
                                   const voidStateEvolutionLaw* evolLaw,
                                   const CLengthLaw *cllCleavage,  // for cleavage
                                   const DamageLaw *damlCleavage):
         IPNonLocalPorosity(fVinitial,j2IH,cll,nuclLaw,coaleslaw,evolLaw),
         _passOnsetCleavage(false),_cleavagePlasticDeformation(0.),
         _nonLocalCleavagePlasticDeformation(0.),_damageEnergy(0.)
{

      _ipvCLCleavage=NULL;
      if(cllCleavage ==NULL) Msg::Error("IPNonLocalPorosityWithCleavage::IPNonLocalPorosityWithCleavage has no cll");
      cllCleavage->createIPVariable(_ipvCLCleavage);

      _ipvDamCleavage=NULL;
      if(damlCleavage ==NULL) Msg::Error("IPNonLocalPorosityWithCleavage::IPNonLocalPorosityWithCleavage has no daml");
      damlCleavage->createIPVariable(_ipvDamCleavage);
    };

    virtual ~IPNonLocalPorosityWithCleavage(){
        if(_ipvCLCleavage != NULL) delete _ipvCLCleavage;
        _ipvCLCleavage == NULL;
        if(_ipvDamCleavage != NULL) delete _ipvDamCleavage;
        _ipvDamCleavage == NULL;
      };
    // General functions
    virtual void restart(){
      IPNonLocalPorosity::restart();
      restartManager::restart(_passOnsetCleavage);
      restartManager::restart(_cleavagePlasticDeformation);
      restartManager::restart(_nonLocalCleavagePlasticDeformation);
      restartManager::restart(_ipvCLCleavage);
      restartManager::restart(_ipvDamCleavage);
      restartManager::restart(_damageEnergy);
    };
    virtual IPVariable* clone() const {return new IPNonLocalPorosityWithCleavage(*this);};

    virtual bool& getRefToPassOnsetCleavageState() {return _passOnsetCleavage;}
    virtual bool getConstRefToPassOnsetCleavageState() const {return _passOnsetCleavage;};

    virtual double& getRefToCleavageMatrixPlasticStrain() {return _cleavagePlasticDeformation;};
    virtual double getConstRefToCleavageMatrixPlasticStrain() const {return _cleavagePlasticDeformation;};

    virtual double& getRefToNonLocalCleavageMatrixPlasticStrain() {return _nonLocalCleavagePlasticDeformation;};
    virtual double getConstRefToNonLocalCleavageMatrixPlasticStrain() const {return _nonLocalCleavagePlasticDeformation;};

    virtual const IPCLength &getConstRefToIPCLengthCleavage() const{
      if(_ipvCLCleavage==NULL)
        Msg::Error("IPNonLocalPorosityWithCleavage: ipvCL not initialized");
      return *_ipvCLCleavage;
    }
    virtual IPCLength &getRefToIPCLengthCleavage(){
      if(_ipvCLCleavage==NULL)
        Msg::Error("IPNonLocalPorosityWithCleavage: ipvCL not initialized");
      return *_ipvCLCleavage;
    }
    virtual const STensor3 &getConstRefToCharacteristicLengthCleavage() const{
      if(_ipvCLCleavage==NULL)
        Msg::Error("IPNonLocalPorosityWithCleavage: ipvCL not initialized");
      return _ipvCLCleavage->getConstRefToCL();
    }
    virtual STensor3 &getRefToCharacteristicLengthCleavage() const{
      if(_ipvCLCleavage==NULL)
        Msg::Error("IPNonLocalPorosityWithCleavage: ipvCL not initialized");
      return _ipvCLCleavage->getRefToCL();
    }

    virtual const IPDamage &getConstRefToIPDamageCleavage() const{
      if(_ipvDamCleavage==NULL)
        Msg::Error("IPNonLocalPorosityWithCleavage: ipvDam not initialized");
      return *_ipvDamCleavage;
    }
    virtual IPDamage &getRefToIPDamageCleavage(){
      if(_ipvDamCleavage==NULL)
        Msg::Error("IPNonLocalPorosityWithCleavage: ipvDam not initialized");
      return *_ipvDamCleavage;
    }
    virtual double damageEnergy() const{ return _damageEnergy;};
    virtual double&getRefToDamageEnergy() {return _damageEnergy;}
};


#endif // IPNONLOCALPOROSITY_H_
