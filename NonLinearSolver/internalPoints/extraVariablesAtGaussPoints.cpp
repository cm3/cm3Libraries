//
// Description: storing class for nonlinear viscoelasticity
//
//
// Author:  <Dongli Li, Antoine Jerusalem>, (C) 2013
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "extraVariablesAtGaussPoints.h"
#include <iostream>
#include <fstream>
#include <iomanip>
#include <vector>
#include "restartManager.h"

void ExtraVariablesAtGaussPoints::fill(const std::string fname)
{
  Msg::Info("read %s",fname.c_str());
  filled=true;
  locations.clear();
  field.clear();

  std::ifstream inputFile(fname.c_str()); 
  std::vector<double> numbers;
  if (inputFile.is_open()) 
  {
    std::string line;
    while (std::getline(inputFile, line)) 
    {
      std::stringstream ss(line);
      std::string token;
      numbers.clear();
      while (std::getline(ss, token, ';')) {
        double num;
        std::istringstream(token) >> num;
        numbers.push_back(num);
      }
      int numberSize=numbers.size();
      //Msg::Info("Line of %d values",numberSize);
      if(numberSize<4)
      {
        Msg::Error("Not enough numbers per line in %s",fname.c_str());
        Msg::Exit(0);
      }
      SVector3 pt(numbers[0],numbers[1],numbers[2]);
      locations.push_back(pt);
      std::vector<double> localfield=std::vector<double>(numbers.begin()+3,numbers.end());
      field.push_back(localfield);
    }
  }
  else
  {
     Msg::Error("Unable to read %s",fname.c_str());
     Msg::Exit(0);
  }

  inputFile.close();
}
  
const std::vector<double> &ExtraVariablesAtGaussPoints::getFieldAtClosestLocation(const SVector3 &gp) const
{
  std::vector<SVector3>::const_iterator it1min, it1= locations.begin();
  std::vector< std::vector<double> >::const_iterator it2min, it2 = field.begin();
  if (locations.size() == field.size()) {
    double minimumdist = 1.e12;
    SVector3 dist;
    for (; it1 != locations.end() && it2 != field.end(); ++it1, ++it2) {
      dist=gp;
      dist-=*it1;
      double norm=dist.norm();
      if(norm <minimumdist)
      {
        it1min=it1;
        it2min=it2;
        minimumdist=norm;
      }
    }
    //Msg::Info("Values at Gauss point location: %f %f %f", (*it1min)[0],(*it1min)[1],(*it1min)[2]);
    //std::vector<double>::const_iterator it3;
    //for (it3=it2min->begin(); it3 != it2min->end(); ++it3) {      
    //        Msg::Info("Element %f",*it3);
    //}
  } 
  else 
  {
    Msg::Error("ExtraVariablesAtGaussPoints locations and field not of the same size");
    Msg::Exit(0);
  }  
  return *it2min;
}
  
void ExtraVariablesAtGaussPoints::print() const
{
  if (locations.size() == field.size()) {
    std::vector<SVector3>::const_iterator it1 = locations.begin();
    std::vector< std::vector<double> >::const_iterator it2 = field.begin();
    for (; it1 != locations.end() && it2 != field.end(); ++it1, ++it2) {
      Msg::Info("Values at Gauss point location: %f %f %f", (*it1)[0],(*it1)[1],(*it1)[2]);
      std::vector<double>::const_iterator it3;
      for (it3=it2->begin(); it3 != it2->end(); ++it3) {      
            Msg::Info("Element %f",*it3);
      }
    }
  } 
  else 
  {
    Msg::Error("ExtraVariablesAtGaussPoints locations and field not of the same size");
    Msg::Exit(0);
  }
}
