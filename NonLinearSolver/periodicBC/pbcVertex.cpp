//
//
// Description: create pbcElement in PBC problems
//
// Author:  <Van Dung NGUYEN>, (C) 2015
//
// Copyright: See COPYING file that comes with this distribution
//
//

#include "pbcVertex.h"
#include "partDomain.h"
#include "staticDofManager.h"


pbcVertex::pbcVertex(partDomain* dompart, MElement* ele, MVertex* vv):
      _dom(dompart),_e(ele),_v(vv){};


void pbcVertex::getKeys(std::vector<int>& comp, std::vector<Dof>& keys) const{
  getDomain()->getFunctionSpace()->getKeysOnVertex(getElement(),getVertex(),comp,keys);
};

void pbcVertex::createPBCVertices(MVertex* vv, std::vector<partDomain*>& allDom, std::vector<pbcVertex*>& allPBCVertices)
{
  bool found = false;
  for (int idom = 0; idom < allDom.size(); idom++)
  {
    partDomain* dom = allDom[idom];
    for (elementGroup::elementContainer::const_iterator itedom = dom->element_begin(); itedom != dom->element_end(); itedom++)
    {
      MElement* eleBulk = itedom->second;
      for (int iv=0; iv < eleBulk->getNumVertices(); iv++)
      {
        MVertex* vb = eleBulk->getVertex(iv);
        if (vb == vv)
        {
          found = true;
          allPBCVertices.push_back(new pbcVertex(dom,eleBulk,vv));
          break;
        }
      }
      if (found)
      {
        break;
      }
      
    }
    if (found)
    {
      break;
    }
  }
};

void pbcVertex::createPBCVertices(elementGroup& gr, std::vector<partDomain*>& allDom, std::set<pbcVertex*>& allPBCVertices)
{
  for (elementGroup::vertexContainer::const_iterator it = gr.vbegin(); it != gr.vend(); it++)
  {
    MVertex* vv = it->second;
    bool found = false;
    for (int idom = 0; idom < allDom.size(); idom++)
    {
      partDomain* dom = allDom[idom];
      for (elementGroup::elementContainer::const_iterator itedom = dom->element_begin(); itedom != dom->element_end(); itedom++)
      {
        MElement* eleBulk = itedom->second;
        for (int iv=0; iv < eleBulk->getNumVertices(); iv++)
        {
          MVertex* vb = eleBulk->getVertex(iv);
          if (vb == vv)
          {
            found = true;
            allPBCVertices.insert(new pbcVertex(dom,eleBulk,vv));
            break;
          }
        }
        if (found)
        {
          break;
        }
        
      }
      if (found)
      {
        break;
      }
    }
  }
};


pbcElement::pbcElement(partDomain* dompart, MElement* eleBulk, MElement* eleFace):
  _dom(dompart),_eBulk(eleBulk), _eSub(eleFace){}
  
void pbcElement::getKeys(std::vector<int>& comp, std::vector<Dof>& keys) const
{
  for (int i=0; i< comp.size(); i++)
  {
    std::vector<int> compPart(1,comp[i]);
    for (int iv =0; iv < getSubElement()->getNumVertices(); iv++)
    {
      getDomain()->getFunctionSpace()->getKeysOnVertex(getBulkElement(),getSubElement()->getVertex(iv),compPart,keys);
    }
  }
}

void pbcElement::createPBCElements(elementGroup& gr, std::vector<partDomain*>& allDom, std::set<pbcElement*>& allPBCFace)
{
  for (elementGroup::elementContainer::const_iterator it = gr.begin(); it != gr.end(); it++)
  {
    MElement* eleSub = it->second;
    bool found = false;
    for (int idom = 0; idom < allDom.size(); idom++)
    {
      partDomain* dom = allDom[idom];
      for (elementGroup::elementContainer::const_iterator itedom = dom->element_begin(); itedom != dom->element_end(); itedom++)
      {
        MElement* eleBulk = itedom->second;
        if (checkInterfaceElementBelongToBulkElement(eleSub,eleBulk))
        {
          found = true;
          allPBCFace.insert(new pbcElement(dom,eleBulk,eleSub));
          break;
        }
      }
      if (found)
      {
        break;
      }
    }
  }
};