//
//
// Description: class to store linear constraint matrix using fullMatrix
//
// Author:  <Van Dung NGUYEN>, (C) 2012
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "linearConstraintMatrixFullMatrix.h"

linearConstraintMatrixFullMatrix::linearConstraintMatrixFullMatrix(): _isAllocated(false),
                                  _row(0),_firstCol(0),_secondCol(0),
                                  _numKinVars(0),_firstMatrix(NULL),
                                  _secondMatrix(NULL),_kinMatrix(NULL){};
linearConstraintMatrixFullMatrix::~linearConstraintMatrixFullMatrix(){
  clear();
};


void linearConstraintMatrixFullMatrix::clear(){
  if (_isAllocated) {
    if (_firstCol>0) delete _firstMatrix; _firstMatrix = NULL;
    if (_secondCol>0) delete _secondMatrix; _secondMatrix = NULL;
    if (_numKinVars>0) delete _kinMatrix; _kinMatrix = NULL;
    _isAllocated = false;
  };
};

void linearConstraintMatrixFullMatrix::allocate(int row, int first, int second, int third, int numKin){
  _row = row;
  _firstCol = first;
  _secondCol = second;
  _numKinVars = numKin;

  if (!_isAllocated){
    _isAllocated = true;
    if (_firstCol>0)
      _firstMatrix = new fullMatrix<double>(_row,_firstCol);
    if (_secondCol>0)
      _secondMatrix = new fullMatrix<double>(_row,_secondCol);
    if (_numKinVars>0)
      _kinMatrix = new fullMatrix<double>(_row,_numKinVars);
  }

};

void linearConstraintMatrixFullMatrix::addToFirstMatrix(int i, int j, double val){
  if (_firstCol <= 0) return;
  if (val != 0.)
    _firstMatrix->operator()(i,j) += val;
}

void linearConstraintMatrixFullMatrix::addToSecondMatrix(int i, int j, double val){
  if (_secondCol<=0) return;
  if (val!= 0.)
    _secondMatrix->operator()(i,j) += val;
}

void linearConstraintMatrixFullMatrix::addToKinematicalMatrix(int i, int j, double val){
  if (_numKinVars<=0) return;
  if (val!=0.)
    _kinMatrix->operator()(i,j) += val;
}

void linearConstraintMatrixFullMatrix::zeroAllMatrices(){
  if (_firstCol>0) _firstMatrix->setAll(0.);
  if (_secondCol>0) _secondMatrix->setAll(0.);
  if (_numKinVars>0) _kinMatrix->setAll(0.);
};

fullMatrix<double>* linearConstraintMatrixFullMatrix::getFirstMatrix(){return _firstMatrix;};
fullMatrix<double>* linearConstraintMatrixFullMatrix::getSecondMatrix(){return _secondMatrix;};
fullMatrix<double>* linearConstraintMatrixFullMatrix::getKinematicalMatrix(){return _kinMatrix;};

