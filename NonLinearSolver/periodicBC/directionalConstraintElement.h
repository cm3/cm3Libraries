//
//
// Description: directional mixed BC
//
// Author:  <Van Dung NGUYEN>, (C) 2017
//
// Copyright: See COPYING file that comes with this distribution
//
//

#include "pbcConstraintElementBase.h"
#include "pbcCreateConstraints.h"

class directionalPeriodicMeshConstraint : public constraintElement{
  protected:
    MVertex* _vp, *_vn;		// two periodic vertex
    fullMatrix<double> _C, _S, _Cbc; // all constraint matrix
		FunctionSpaceBase* _periodicSpacePlus, *_periodicSpaceMinus, *_multSpace;
		int _positive;
	
  public:
    directionalPeriodicMeshConstraint(nonLinearMicroBC* mbc,FunctionSpaceBase* spacePlus, FunctionSpaceBase* spaceMinus, FunctionSpaceBase* multspace, 
													 MVertex* vp, MVertex* vn, const SVector3& dir);
													 
    virtual ~directionalPeriodicMeshConstraint(){};
    virtual void getConstraintKeys(std::vector<Dof>& key) const;
    virtual void getMultiplierKeys(std::vector<Dof>& key) const;
    virtual void getConstraintMatrix(fullMatrix<double>& m) const;
    virtual void getDependentMatrix(fullMatrix<double>& m) const;
    virtual void print() const;
    virtual void getDependentKeys(std::vector<Dof>& keys) const; // left real dofs
    virtual void getIndependentKeys(std::vector<Dof>& keys) const;
    virtual void getLinearConstraints(const fullVector<double>& g,
                   std::map<Dof,DofAffineConstraint<double> >& con) const;
    virtual void getLinearConstraintsByVertices(std::map<Dof,DofAffineConstraint<double> >& con) const;
    virtual constraintElement::ElementType getType() const {
			return constraintElement::DirectionalPeriodic;
    };
    virtual bool isActive(const MVertex* v) const {
      if (_vp->getNum() == v->getNum()) return true;
      if (_vn) {
        if (_vn->getNum() == v->getNum()) return true;
      }
      return false;
    };

    virtual bool isDirect() const;

    virtual void getKinematicalVector(fullVector<double>& m) const;
		virtual void getKinematicalMatrix(fullMatrix<double>& m) const;
		virtual void getLinearConstraints(std::map<Dof,DofAffineConstraint<double> >& con) const;
};

class directionalSupplementConstraint : public constraintElement{
	protected:
		FunctionSpaceBase* _periodicSpace, *_multSpace;
    elementGroup* _g;
    fullMatrix<double>  _C, _S, _Cbc; // constraint matrix
    std::vector<MVertex*> _v; // all Vertex
    int _positive; // positive position
		int _positiveComp;
		int _positiveVertex;
    
  public:
    directionalSupplementConstraint(nonLinearMicroBC* Mbc, FunctionSpaceBase* space, FunctionSpaceBase* mspace,
																	elementGroup* g, const SVector3& dir);
    virtual ~directionalSupplementConstraint(){
    }
    virtual void getConstraintKeys(std::vector<Dof>& key) const ; // real dofs on constraint elements
    virtual void getMultiplierKeys(std::vector<Dof>& key) const ;	// multiplier dof on constraint element
    virtual void getConstraintMatrix(fullMatrix<double>& m) const ;		// matrix C
    virtual void getDependentMatrix(fullMatrix<double>& m) const;
    virtual void getDependentKeys(std::vector<Dof>& keys) const; // left real dofs
    virtual void getIndependentKeys(std::vector<Dof>& keys) const; // for spline interpolation
    virtual void getLinearConstraints(const fullVector<double>& g,
                   std::map<Dof,DofAffineConstraint<double> >& con) const;
    virtual constraintElement::ElementType getType() const{return constraintElement::Supplement;};
    virtual void print() const;
	
    virtual bool isActive(const MVertex* v) const {
      for (int i=0; i<_v.size(); i++){
        if (_v[i]->getNum() == v->getNum()) return true;
      }
      return false;
    };

    virtual void getKinematicalVector(fullVector<double>& m) const;
		virtual void getKinematicalMatrix(fullMatrix<double>& m) const;
		virtual void getLinearConstraints(std::map<Dof,DofAffineConstraint<double> >& con) const;
};

class directionalPBCSupplementConstraint : public constraintElement{
	protected:
		FunctionSpaceBase* _periodicSpace, *_multSpace;
    elementGroup* _gPlus, *_gMinus;
    fullMatrix<double>  _C, _S, _Cbc; // constraint matrix
    std::vector<MVertex*> _vPlus, _vMinus; // all Vertex
    int _positive; // positive position
		int _positiveComp;
		int _positiveVertex;
      
	public:
    directionalPBCSupplementConstraint(nonLinearMicroBC* Mbc, FunctionSpaceBase* space, FunctionSpaceBase* mspace,
																	elementGroup* gplus, elementGroup* gMinus, const SVector3& dir);
    virtual ~directionalPBCSupplementConstraint(){
    }
    virtual void getConstraintKeys(std::vector<Dof>& key) const ; // real dofs on constraint elements
    virtual void getMultiplierKeys(std::vector<Dof>& key) const ;	// multiplier dof on constraint element
    virtual void getConstraintMatrix(fullMatrix<double>& m) const ;		// matrix C
    virtual void getDependentMatrix(fullMatrix<double>& m) const;
    virtual void getDependentKeys(std::vector<Dof>& keys) const; // left real dofs
    virtual void getIndependentKeys(std::vector<Dof>& keys) const; // for spline interpolation
    virtual void getLinearConstraints(const fullVector<double>& g,
                   std::map<Dof,DofAffineConstraint<double> >& con) const;
    virtual constraintElement::ElementType getType() const{return constraintElement::Supplement;};
    virtual void print() const;
	
    virtual bool isActive(const MVertex* v) const {
      for (int i=0; i<_vPlus.size(); i++){
        if (_vPlus[i]->getNum() == v->getNum()) return true;
      }
			for (int i=0; i<_vMinus.size(); i++){
        if (_vMinus[i]->getNum() == v->getNum()) return true;
      }
      return false;
    };

    virtual void getKinematicalVector(fullVector<double>& m) const;
		virtual void getKinematicalMatrix(fullMatrix<double>& m) const;
		virtual void getLinearConstraints(std::map<Dof,DofAffineConstraint<double> >& con) const;
};

class directionalPBCLagrangeConstraintElement : public constraintElement{
	protected:
		MVertex* _vp;
		std::vector<MVertex*> _vn;  // sequence vertices
		SVector3 _L;
		std::vector<double> _distance;
		double _s;
		fullMatrix<double> _C, _S, _Cbc;
		fullVector<double> _FF;
		
		FunctionSpaceBase* _periodicSpace, *_multSpace;
		int _positiveComp;
		 
	public:
		// "dir" is the projection vector
		// the periodic BC is applied in the direction perpendicular to "dir"
		// this direction is given by the v1 and the projected node --> VecFollowed
		directionalPBCLagrangeConstraintElement(nonLinearMicroBC* mbc, FunctionSpaceBase* space, FunctionSpaceBase* mspace, 
															MVertex* v1, std::vector<MVertex*>& vlist, const SVector3& dir);
		
		virtual ~directionalPBCLagrangeConstraintElement(){};
		virtual void getConstraintKeys(std::vector<Dof>& key) const; // real dofs on constraint elements
		virtual void getMultiplierKeys(std::vector<Dof>& key) const;	// multiplier dof on constraint element
		virtual void getConstraintMatrix(fullMatrix<double>& m) const;		// matrix C
		virtual void getDependentMatrix(fullMatrix<double>& m) const;
		virtual void print() const;
		virtual void getDependentKeys(std::vector<Dof>& keys) const; // left real dofs
		virtual void getIndependentKeys(std::vector<Dof>& keys) const; // left real dofs
    virtual void getLinearConstraints(const fullVector<double>& g,
                   std::map<Dof,DofAffineConstraint<double> >& con) const;
    virtual void getLinearConstraintsByVertices(std::map<Dof,DofAffineConstraint<double> >& con) const;
    virtual constraintElement::ElementType getType() const { return constraintElement::Lagrange;};
    virtual bool isActive(const MVertex* v) const {
      if (_vp->getNum() == v->getNum()) return true;
      for (int i=0; i<_vn.size(); i++){
        if (_vn[i]->getNum() == v->getNum()) return true;
      }
      return false;
    };

    virtual void getKinematicalVector(fullVector<double>& m) const;
		virtual void getKinematicalMatrix(fullMatrix<double>& m) const;
		virtual void getLinearConstraints(std::map<Dof,DofAffineConstraint<double> >& con) const;
};

class directionalFormLagrangeConstraintElement : public constraintElement{
	protected:
		MVertex* _vp;
		std::vector<MVertex*> _vn;  // sequence vertices
		SVector3 _L;
		std::vector<double> _distance;
		double _s;
		fullMatrix<double> _C, _S, _Cbc;
		fullVector<double> _FF;
		
		FunctionSpaceBase* _periodicSpace, *_multSpace;
		int _positiveComp;
		 
public:
		// the periodic BC is applied following "dir"
		// this direction is given by the v1 and the projected node --> VecFollowed
		directionalFormLagrangeConstraintElement(nonLinearMicroBC* mbc, FunctionSpaceBase* space, FunctionSpaceBase* mspace, 
															MVertex* v1, std::vector<MVertex*>& vlist, const SVector3& dir);
		
		virtual ~directionalFormLagrangeConstraintElement(){};
		virtual void getConstraintKeys(std::vector<Dof>& key) const; // real dofs on constraint elements
		virtual void getMultiplierKeys(std::vector<Dof>& key) const;	// multiplier dof on constraint element
		virtual void getConstraintMatrix(fullMatrix<double>& m) const;		// matrix C
		virtual void getDependentMatrix(fullMatrix<double>& m) const;
		virtual void print() const;
		virtual void getDependentKeys(std::vector<Dof>& keys) const; // left real dofs
		virtual void getIndependentKeys(std::vector<Dof>& keys) const; // left real dofs
    virtual void getLinearConstraints(const fullVector<double>& g,
                   std::map<Dof,DofAffineConstraint<double> >& con) const;
    virtual void getLinearConstraintsByVertices(std::map<Dof,DofAffineConstraint<double> >& con) const;
    virtual constraintElement::ElementType getType() const { return constraintElement::Lagrange;};
    virtual bool isActive(const MVertex* v) const {
      if (_vp->getNum() == v->getNum()) return true;
      for (int i=0; i<_vn.size(); i++){
        if (_vn[i]->getNum() == v->getNum()) return true;
      }
      return false;
    };

    virtual void getKinematicalVector(fullVector<double>& m) const;
		virtual void getKinematicalMatrix(fullMatrix<double>& m) const;
		virtual void getLinearConstraints(std::map<Dof,DofAffineConstraint<double> >& con) const;
};

class shiftedLagrangeConstraintElement : public constraintElement{
	protected:
		MVertex* _vp;
		std::vector<MVertex*> _vn;  // sequence vertices
		SVector3 _L;
		STensor3 _XX;
		std::vector<double> _distance;
		double _s;
		fullMatrix<double> _C, _S, _Cbc;
		fullVector<double> _FF;
		
		FunctionSpaceBase* _periodicSpace, *_multSpace;
		 
	public:
		shiftedLagrangeConstraintElement(nonLinearMicroBC* mbc, FunctionSpaceBase* space, FunctionSpaceBase* mspace, 
															const int c, MVertex* v1, std::vector<MVertex*>& vlist, const SVector3& normal);
		
		virtual ~shiftedLagrangeConstraintElement(){};
		virtual void getConstraintKeys(std::vector<Dof>& key) const; // real dofs on constraint elements
		virtual void getMultiplierKeys(std::vector<Dof>& key) const;	// multiplier dof on constraint element
		virtual void getConstraintMatrix(fullMatrix<double>& m) const;		// matrix C
		virtual void getDependentMatrix(fullMatrix<double>& m) const;
		virtual void print() const;
		virtual void getDependentKeys(std::vector<Dof>& keys) const; // left real dofs
		virtual void getIndependentKeys(std::vector<Dof>& keys) const; // left real dofs
    virtual void getLinearConstraints(const fullVector<double>& g,
                   std::map<Dof,DofAffineConstraint<double> >& con) const;
    virtual void getLinearConstraintsByVertices(std::map<Dof,DofAffineConstraint<double> >& con) const;
    virtual constraintElement::ElementType getType() const { return constraintElement::Lagrange;};
    virtual bool isActive(const MVertex* v) const {
      if (_vp->getNum() == v->getNum()) return true;
      for (int i=0; i<_vn.size(); i++){
        if (_vn[i]->getNum() == v->getNum()) return true;
      }
      return false;
    };

    virtual void getKinematicalVector(fullVector<double>& m) const;
		virtual void getKinematicalMatrix(fullMatrix<double>& m) const;
		virtual void getLinearConstraints(std::map<Dof,DofAffineConstraint<double> >& con) const;
};