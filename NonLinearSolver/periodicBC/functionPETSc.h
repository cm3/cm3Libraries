//
//
// Description: PETSC function
// Author:  <Van Dung NGUYEN>, (C) 2010
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef FUNCTIONPETSC_H_
#define FUNCTIONPETSC_H_
#include "GmshConfig.h"
#if defined(HAVE_PETSC)
#include "petscksp.h"
#include "petscmat.h"
#include "petscsys.h"
#include <iostream>


namespace functionPETSc{
  inline void _try(int ierr) { CHKERRABORT(PETSC_COMM_WORLD, ierr); };
  PetscErrorCode MatGetDeterminant(Mat A, PetscReal* realpart, PetscReal* imagpart, PetscInt* ex);
  void MatToFile(Mat const & a, const std::string filename);
  void VecToFile(Vec const & a,const std::string filename);

  /*
  //   A11 A12 A13
  //   A21 A22 A23  --> B22 B23
  //   A31 A32 A33      B32 B33
  // with B22 = A22 - A21*invA11*A12
  //      B23 = A23 - A21*invA11*A13
  //      B32 = A32 - A31*invA11*A12
  //      B33 = A33 - A31*invA11*A13
  */

  PetscErrorCode GetFactorMatrix(Mat A11, Mat* F);

  PetscErrorCode CondenseMatrixBlocks(Mat A11, Mat A12, Mat A13,
                                       Mat A21, Mat A22, Mat A23,
                                       Mat A31, Mat A32, Mat A33,
                                       Mat* B22, Mat* B23,
                                       Mat* B32, Mat* B33);
  PetscErrorCode CondenseMatrixBlocks(Mat A11, Mat A12,
                                      Mat A21, Mat A22, Mat* B22);

  // interface PETSc for compute A22 - A21 inv(A11) A12
  PetscErrorCode GetSchurComplementMat(Mat fA11, Mat A12, Mat A21, Mat A22, Mat* A);
  PetscErrorCode GetSchurComplementMat_Naive(KSP& ksp, Mat A11, Mat A12, Mat A21, Mat A22, Mat* A);

  // interface PETSc for compute B = inv(A)

  PetscErrorCode MatInverse_MatMatSolve(Mat A, Mat* B, MPI_Comm comm = PETSC_COMM_SELF);

  PetscErrorCode MatInverse_Naive(Mat A, Mat* B, MPI_Comm comm = PETSC_COMM_SELF);

  PetscErrorCode MatMatSolve_MultipleRHS(Mat A, Mat RHS, Mat* B);

  PetscErrorCode MatMatSolve_MultipleRHS(Mat A, Mat RHS1, Mat RHS2, Mat* B1, Mat* B2);


  }

#endif // PETSC

#endif // FUNCTIONPETSC_H_
