//
//
// Description: based class for PBC enforcement
//
// Author:  <Van Dung NGUYEN>, (C) 2010
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef PERIODICCONSTRAINTS_H_
#define PERIODICCONSTRAINTS_H_

#include "elementGroup.h"
#include "functionSpace.h"
#include "MPoint.h"
#include "numericalFunctions.h"
#include "partDomain.h"
#include "staticDofManager.h"

class periodicConstraintsBase{
  public:
    typedef std::set<MVertex*> vertexContainer;
    typedef std::set<MElement*> elementContainer;

  protected:
    std::vector<vertexContainer> gVertex; // entity for imposing PBC
    std::vector<elementContainer> gElement;
    FunctionSpaceBase* space; // to creat Dofs
    nlsDofManager* p;    // to manage Dofs
    fullMatrix<double> imposedDef; // imposed deformation  // tensor of def, and grad T
    vertexContainer coinVertex;  //coin vertex ->particularly treatment
    int _tag, _dim, _ndofs; // _dim = 2 or 3 in 2 and 3D, _ndof = _dim + extraDof Diffusion for coupled problems

  protected:
    void getKeysFromVertex(MVertex* ver,std::vector<Dof> &keys);
    MVertex* findNearestVertex(MVertex* vP, vertexContainer container);
    void periodicConditionBase(MVertex* vP, MVertex* vN);
    void nodeConstraint(MVertex* v);
    void cornerConstraint();

  public:
    periodicConstraintsBase(const int tag, const int ndofs, const int dim, nlsDofManager *_p, std::vector<partDomain*>& allDom);
    virtual ~periodicConstraintsBase();

   void setDeformation(const STensor3& def){
     imposedDef.resize(_dim,3);
     for (int i=0; i<_dim; i++){
         for (int j=0; j<3; j++){
	     imposedDef(i,j) = def(i,j);
         };
     };
   };
   void setDeformation(fullMatrix<double> def){
      int size1 = def.size1();
      int size2 = def.size2();
      imposedDef.resize(size1,size2);
      for (int i=0; i<size1; i++){
        for (int j=0; j<size2; j++){
	  imposedDef(i,j) = def(i,j);
	};
     };
    };
    void setDeformation(const STensor3& def, const SVector3 &gradT){
      imposedDef.resize(_ndofs,3);
      for (int i=0; i<_ndofs-1; i++){
         for (int j=0; j<3; j++){
	     imposedDef(i,j) = def(i,j);
         };
      };
      for (int j=0; j<3; j++){
          imposedDef(_ndofs-1,j) = gradT(j);
      };
    };

    virtual void setPeriodicGroup(std::vector<elementGroup*> group,int dim);
    void groupIntersection(vertexContainer g1, vertexContainer g2, vertexContainer &g);
    virtual void applyPeriodicCondition()=0;
};
#endif // PERIODICCONSTRAINTS_H_
