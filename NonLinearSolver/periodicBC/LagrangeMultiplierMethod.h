//
//
// Description: enforce linear constrint by Lagrange multipliers
//
// Author:  <Van Dung NGUYEN>, (C) 2010
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef LAGRANGEMULTIPLIERMETHOD_H_INCLUDED
#define LAGRANGEMULTIPLIERMETHOD_H_INCLUDED

#include "periodicConstraints.h"

class lagrangeMultiplier: public periodicConstraintsBase{

  protected:
    void addMultiplierKeys(MVertex* vp, MVertex* vn,std::vector<Dof> &keys,int flag); //flag for creating multiplier dofs
    void get(MVertex* vp, MVertex* vn, fullMatrix<double> &m, fullVector<double> &g);
    void unitAssemble(MVertex* vp, MVertex* vn,int flag);

  public:
    lagrangeMultiplier(const int tag, const int ndofs, const int dim, nlsDofManager *_p, std::vector<partDomain*>& allDom)
                      : periodicConstraintsBase(tag,ndofs,dim,_p,allDom){};
    virtual ~lagrangeMultiplier(){};

    virtual void applyPeriodicCondition();
    virtual void numberDof();
    virtual void assemble();
};

#endif // LAGRANGEMULTIPLIERMETHOD_H_INCLUDED
