//
//
// Description: patch Coons interpolation elements
//
// Author:  <Van Dung NGUYEN>, (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "pbcCoonsPatchConstraintElement.h"

CoonsPatchCubicSplineConstraintElement::CoonsPatchCubicSplineConstraintElement(nonLinearMicroBC* mbc, FunctionSpaceBase* space, FunctionSpaceBase* mspace,
																					const int c, MVertex* vp, MVertex* vre, MVertex* vx1, MVertex* vx2,
                                          MVertex* vy1, MVertex* vy2, int flagx, int flagy):
																					constraintElement(mbc,c),_periodicSpace(space),_multSpace(mspace),
																				_reference(vre),_vp(vp),_vnx1(vx1),_vnx2(vx2),_vny1(vy1),_vny2(vy2),_flagx(flagx),_flagy(flagy){
  SPoint3 vppoint(_vp->point());
  _x1 = distance(_vnx1->point(),_vny1->point(),_vny2->point());
  _x2 = distance(_vnx2->point(),_vny1->point(),_vny2->point());
  _y1 = distance(_vny1->point(),_vnx1->point(),_vnx2->point());
  _y2 = distance(_vny2->point(),_vnx1->point(),_vnx2->point());
  // get 3rd independent vertex
  MVertex * vtemp(NULL);
  if (_y2>_y1)
    vtemp =_vny2;
  else
    vtemp = _vny1;
  // get projected point on negative surface
  SPoint3 vnpoint= project(vppoint,_vnx1->point(),_vnx2->point(),vtemp->point());
  // get  surface coordinate on negative surface
  _xi = distance(vnpoint,_vny1->point(),_vny2->point());
  _neta = distance(vnpoint,_vnx1->point(),_vnx2->point());
  // left matrix
  _C.resize(_ndofs,10*_ndofs); _C.setAll(0.0);
  _Cbc.resize(_ndofs,9*_ndofs); _Cbc.setAll(0.);
  int col = 0;
  for (int i=0; i<_ndofs; i++){
       _C(i,col) = 1.0;
       col++;
  };
  cubicSplineConstraintElement::getFF(_xi,_x1,_x2,_FFx);
  cubicSplineConstraintElement::getFF(_neta,_y1,_y2,_FFy);
  for (int i=0; i<_FFx.size(); i++){
      for (int j=0; j<_ndofs; j++){
          _C(j,col) = -1.*_FFx(i);
          _Cbc(j, col-_ndofs) = _FFx(i);
	  col++;
      };
  };
  for (int i=0; i<_FFy.size(); i++){
    for (int j=0; j<_ndofs; j++){
      _C(j,col) = -1.*_FFy(i);
      _Cbc(j,col-_ndofs) = _FFy(i);
      col++;
    };
  };
  for (int i=0; i<_ndofs; i++){
    _C(i,col) = 1.0;
    _Cbc(i,col-_ndofs) = -1.;
    col++;
  };


  _L(0) = _vp->x() - _FFx(0)*_vnx1->x() - _FFx(2)*_vnx2->x()
          - _FFy(0)*_vny1->x() - _FFy(2)*_vny2->x()+ _reference->x();
  _L(1) = _vp->y() - _FFx(0)*_vnx1->y() - _FFx(2)*_vnx2->y()
          - _FFy(0)*_vny1->y() - _FFy(2)*_vny2->y()+ _reference->y();
  _L(2) = _vp->z() - _FFx(0)*_vnx1->z() - _FFx(2)*_vnx2->z()
          - _FFy(0)*_vny1->z() - _FFy(2)*_vny2->z()+ _reference->z();
					
	STensorOperation::zero(_XiXj);
	if (_mbc->getOrder() == 2){
		Msg::Error("CoonsPatchCubicSplineConstraintElement is not implemented for order %d",_mbc->getOrder());
	}

  // right matrix
  _mbc->getPBCKinematicMatrix(_component,_L,_XiXj,_S);

  std::vector<Dof> Keys;
  getKeysFromVertex(_periodicSpace,_vp,getComp(),Keys);
  for (int ik=0; ik < Keys.size(); ik++){
    if (constraintElement::allPositiveDof.find(Keys[ik]) == constraintElement::allPositiveDof.end()){
      constraintElement::allPositiveDof.insert(Keys[ik]);
    }
    else{
      Msg::Warning("Dof on vertex was chosen as positive one in other constraint element:");
    }
  }
};


void CoonsPatchCubicSplineConstraintElement::getConstraintKeys(std::vector<Dof>& keys) const{
    getKeysFromVertex(_periodicSpace,_vp,getComp(),keys);
    getKeysFromVertex(_periodicSpace,_vnx1,getComp(),keys);
    cubicSplineConstraintElement::addKeysToVertex(_periodicSpace,_vnx1,getComp(),keys,_flagx);
    getKeysFromVertex(_periodicSpace,_vnx2,getComp(),keys);
    cubicSplineConstraintElement::addKeysToVertex(_periodicSpace,_vnx2,getComp(),keys,_flagx);
    getKeysFromVertex(_periodicSpace,_vny1,getComp(),keys);
    cubicSplineConstraintElement::addKeysToVertex(_periodicSpace,_vny1,getComp(),keys,_flagy);
    getKeysFromVertex(_periodicSpace,_vny2,getComp(),keys);
    cubicSplineConstraintElement::addKeysToVertex(_periodicSpace,_vny2,getComp(),keys,_flagy);
    getKeysFromVertex(_periodicSpace,_reference,getComp(),keys);
};
void CoonsPatchCubicSplineConstraintElement::getMultiplierKeys(std::vector<Dof>& keys) const{
    getKeysFromVertex(_multSpace,_vp,getComp(), keys);
};
void CoonsPatchCubicSplineConstraintElement::getConstraintMatrix(fullMatrix<double>& m) const{
    m = _C;
};
void CoonsPatchCubicSplineConstraintElement::getDependentMatrix(fullMatrix<double>& m) const{
  m = _Cbc;
};

void CoonsPatchCubicSplineConstraintElement::print() const{
  Msg::Info("Patch Coons Cubic Spline constraint element 3D");
  Msg::Info("positive = %d",_vp->getNum());
  Msg::Info("negative x = %d %d",_vnx1->getNum(),_vnx2->getNum());
  Msg::Info("negative y = %d %d",_vny1->getNum(),_vny2->getNum());
};


void CoonsPatchCubicSplineConstraintElement::getDependentKeys(std::vector<Dof>& key) const{
  getKeysFromVertex(_periodicSpace,_vp,getComp(),key);
}

void CoonsPatchCubicSplineConstraintElement::getIndependentKeys(std::vector<Dof>& keys) const{
  getKeysFromVertex(_periodicSpace,_vnx1,getComp(),keys);
	cubicSplineConstraintElement::addKeysToVertex(_periodicSpace,_vnx1,getComp(),keys,_flagx);
	getKeysFromVertex(_periodicSpace,_vnx2,getComp(),keys);
	cubicSplineConstraintElement::addKeysToVertex(_periodicSpace,_vnx2,getComp(),keys,_flagx);
	getKeysFromVertex(_periodicSpace,_vny1,getComp(),keys);
	cubicSplineConstraintElement::addKeysToVertex(_periodicSpace,_vny1,getComp(),keys,_flagy);
	getKeysFromVertex(_periodicSpace,_vny2,getComp(),keys);
	cubicSplineConstraintElement::addKeysToVertex(_periodicSpace,_vny2,getComp(),keys,_flagy);
	getKeysFromVertex(_periodicSpace,_reference,getComp(),keys);
}

void CoonsPatchCubicSplineConstraintElement::getVirtualKeys(std::vector<Dof>& key) const{
  if ((_vnx1) and (_vnx2)){
    cubicSplineConstraintElement::addKeysToVertex(_periodicSpace,_vnx1,getComp(),key,_flagx);
    cubicSplineConstraintElement::addKeysToVertex(_periodicSpace,_vnx2,getComp(),key,_flagx);
  };
  if ((_vny1) and (_vny2)){
    cubicSplineConstraintElement::addKeysToVertex(_periodicSpace,_vny1,getComp(),key,_flagy);
    cubicSplineConstraintElement::addKeysToVertex(_periodicSpace,_vny2,getComp(),key,_flagy);
  };
}


void CoonsPatchCubicSplineConstraintElement::getLinearConstraints(const fullVector<double>& g,
                   std::map<Dof,DofAffineConstraint<double> >& con) const
{
  std::vector<Dof> k, kx1, kx2, keysx1, keysx2;
  getKeysFromVertex(_periodicSpace,_vp,getComp(),k);
  if ((_vnx1) and (_vnx2)){
    getKeysFromVertex(_periodicSpace,_vnx1,getComp(),kx1);
    getKeysFromVertex(_periodicSpace,_vnx2,getComp(),kx2);
    cubicSplineConstraintElement::addKeysToVertex(_periodicSpace,_vnx1,getComp(),keysx1,_flagx);
    cubicSplineConstraintElement::addKeysToVertex(_periodicSpace,_vnx2,getComp(),keysx2,_flagx);
  };

  std::vector<Dof> ky1, ky2, keysy1, keysy2;
  if ((_vny1) and (_vny2)){
    getKeysFromVertex(_periodicSpace,_vny1,getComp(),ky1);
    getKeysFromVertex(_periodicSpace,_vny2,getComp(),ky2);
    cubicSplineConstraintElement::addKeysToVertex(_periodicSpace,_vny1,getComp(),keysy1,_flagy);
    cubicSplineConstraintElement::addKeysToVertex(_periodicSpace,_vny2,getComp(),keysy2,_flagy);
  };

  std::vector<Dof> kref;
  if (_reference){
    getKeysFromVertex(_periodicSpace,_reference,getComp(),kref);
  }

  fullVector<double> FFx, FFy;
  cubicSplineConstraintElement::getFF(_xi,_x1,_x2,FFx);
  cubicSplineConstraintElement::getFF(_neta,_y1,_y2,FFy);
  DofAffineConstraint<double> cons;
  for (int i=0; i<_ndofs; i++){
    cons.shift=g(i);
    if ((_vnx1) and (_vnx2)){
      cons.linear.push_back(std::pair<Dof,double>(kx1[i],FFx(0)));
      cons.linear.push_back(std::pair<Dof,double>(keysx1[i],FFx(1)));
      cons.linear.push_back(std::pair<Dof,double>(kx2[i],FFx(2)));
      cons.linear.push_back(std::pair<Dof,double>(keysx2[i],FFx(3)));
    };
    if ((_vny1) and (_vny2)){
      cons.linear.push_back(std::pair<Dof,double>(ky1[i],FFy(0)));
      cons.linear.push_back(std::pair<Dof,double>(keysy1[i],FFy(1)));
      cons.linear.push_back(std::pair<Dof,double>(ky2[i],FFy(2)));
      cons.linear.push_back(std::pair<Dof,double>(keysy2[i],FFy(3)));
    };
    if (_reference){
      cons.linear.push_back(std::pair<Dof,double>(kref[i],-1.));
    }
    con[k[i]] = cons;
    cons.linear.clear();
  };
};

void CoonsPatchCubicSplineConstraintElement::getKinematicalVector(fullVector<double>& m) const{
	fullVector<double> kinVec;
	_mbc->getKinematicalVector(kinVec);
	m.resize(_ndofs);
	m.setAll(0.);
	_S.mult(kinVec,m);
};
void CoonsPatchCubicSplineConstraintElement::getKinematicalMatrix(fullMatrix<double>& m) const{
  m = _S;
};
void CoonsPatchCubicSplineConstraintElement::getLinearConstraints(std::map<Dof,DofAffineConstraint<double> >& con) const{
  fullVector<double> g;
  getKinematicalVector(g);
  getLinearConstraints(g,con);
};

CoonsPatchLagrangeConstraintElement::CoonsPatchLagrangeConstraintElement(nonLinearMicroBC* mbc, FunctionSpaceBase* lagspace, FunctionSpaceBase* multspace,
																				const int c, MVertex* v1, MVertex* vref,
																				std::vector<MVertex*>& xlist,std::vector<MVertex*>& ylist,
																				MVertex* vrootP, MVertex* vrootN):
																				constraintElement(mbc,c),_periodicSpace(lagspace),_multSpace(multspace),
                                                         _vrootP(vrootP),_vrootN(vrootN),
                                                         _reference(vref),_vp(v1),_vnx(xlist),_vny(ylist){
    int sizex = xlist.size();
    int sizey = ylist.size();
    for (int i=0; i<sizex; i++){
       double dist = distance(_vnx[i]->point(),_vny[0]->point(),_vny[sizey-1]->point());
       _distanceX.push_back(dist);
    };
    for (int i=0; i<sizey; i++){
       double dist = distance(_vny[i]->point(),_vnx[0]->point(),_vnx[sizex-1]->point());
       _distanceY.push_back(dist);
    };
    SPoint3 pp = _vp->point();
    MVertex * vtemp = NULL;
    if (_distanceY[sizey-1] > _distanceY[0])
      vtemp =_vny[sizey-1];
    else
      vtemp = _vny[0];
    SPoint3 point = project(pp,_vnx[0]->point(),_vnx[sizex-1]->point(),vtemp->point());
    _x = distance(point,_vny[0]->point(),_vny[sizey-1]->point());
    _y = distance(point,_vnx[0]->point(),_vnx[sizex-1]->point());

    _C.resize(_ndofs,(2+sizex+sizey)*_ndofs); _C.setAll(0.0);
    _Cbc.resize(_ndofs, (1+sizex+sizey)*_ndofs); _Cbc.setAll(0.);
    int col = 0;
    for (int i=0; i<_ndofs; i++){
        _C(i,col) = 1.0;
        col++;
    };
    lagrangeConstraintElement::getFF(_x,_distanceX,_FFx);
    lagrangeConstraintElement::getFF(_y,_distanceY,_FFy);

    SPoint3 ptref = _reference->point();
    for (int i=0; i<3; i++)
       _L[i] = pp[i] + ptref[i];

    for (int j=0; j<_FFx.size(); j++){
       SPoint3 pt = _vnx[j]->point();
       for (int i=0; i<3; i++){
          _L[i] -= _FFx(j)*pt[i];
       }
    }
    for (int j=0; j<_FFy.size(); j++){
      SPoint3 pt = _vny[j]->point();
      for (int i=0; i<3; i++){
         _L[i] -= _FFy(j)*pt[i];
      }
    }
		
		if (_mbc->getOrder() == 2){
			SPoint3 pp = _vp->point();
			SPoint3 ptref = _reference->point();
			for (int i=0; i<3; i++)
				for (int j=0; j<3; j++)
					_XiXj(i,j) = 0.5*(pp[i]*pp[j] + ptref[i]*ptref[j]);

			for (int k=0; k<_FFx.size(); k++){
				SPoint3 pt = _vnx[k]->point();
				for (int i=0; i<3; i++)
					for (int j=0; j<3; j++)
					 _XiXj(i,j) -= (0.5*_FFx(k)*pt[i]*pt[j]);
			}

			for (int k=0; k<_FFy.size(); k++){
				SPoint3 pt = _vny[k]->point();
				for (int i=0; i<3; i++)
					for (int j=0; j<3; j++)
					 _XiXj(i,j) -= (0.5*_FFy(k)*pt[i]*pt[j]);
			}
		}
		
    for (int i=0; i<_FFx.size(); i++){
        for (int j=0; j<_ndofs; j++){
            _C(j,col) = -1.*_FFx(i);
            _Cbc(j,col-_ndofs) = _FFx(i);
            col++;
        };
    };
    for (int i=0; i<_FFy.size(); i++){
        for (int j=0; j<_ndofs; j++){
            _C(j,col) = -1.*_FFy(i);
            _Cbc(j,col-_ndofs) = _FFy(i);
	    col++;
	};
    };
    for (int i=0; i<_ndofs; i++){
        _C(i,col) = 1.0;
        _Cbc(i,col-_ndofs) = -1.;
        col++;
    };

    _mbc->getPBCKinematicMatrix(_component,_L,_XiXj,_S);

    std::vector<Dof> Keys;
    getKeysFromVertex(_periodicSpace,_vp,getComp(),Keys);
    for (int ik=0; ik < Keys.size(); ik++){
      if (constraintElement::allPositiveDof.find(Keys[ik]) == constraintElement::allPositiveDof.end()){
        constraintElement::allPositiveDof.insert(Keys[ik]);
      }
      else{
        Msg::Warning("Dof %d on vertex %d was chosen as positive one in other constraint element:",this->_component[0], _vp->getNum());
      }
    }
};

void CoonsPatchLagrangeConstraintElement::getConstraintKeys(std::vector<Dof>& keys) const{
	getKeysFromVertex(_periodicSpace,_vp,getComp(), keys);
	for (int i=0; i<_vnx.size(); i++){
		getKeysFromVertex(_periodicSpace,_vnx[i], getComp(), keys);
	};
	for (int i=0; i<_vny.size(); i++){
		getKeysFromVertex(_periodicSpace,_vny[i],getComp(), keys);
	};
	getKeysFromVertex(_periodicSpace,_reference, getComp(), keys);
};

void CoonsPatchLagrangeConstraintElement::getMultiplierKeys(std::vector<Dof>& keys) const{
	getKeysFromVertex(_multSpace,_vp, getComp(), keys);
};

void CoonsPatchLagrangeConstraintElement::getConstraintMatrix(fullMatrix<double>& m)const{
	m = _C;
};
void CoonsPatchLagrangeConstraintElement::getDependentMatrix(fullMatrix<double>& m) const{
  m = _Cbc;
};

void CoonsPatchLagrangeConstraintElement::print() const{
  Msg::Info("Patch Coons Lagrange constraint element 3D");
  Msg::Info("positive = %d",_vp->getNum());
  for (int i=0; i<_vnx.size(); i++)
    Msg::Info("negative x = %d ",_vnx[i]->getNum());
  for (int i=0; i<_vny.size(); i++)
    Msg::Info("negative y = %d ",_vny[i]->getNum());
};

void CoonsPatchLagrangeConstraintElement::getDependentKeys(std::vector<Dof>& keys) const{
  getKeysFromVertex(_periodicSpace,_vp,getComp(),keys);
}; // left real dofs

void CoonsPatchLagrangeConstraintElement::getIndependentKeys(std::vector<Dof>& keys) const{
  for (int i=0; i<_vnx.size(); i++){
		getKeysFromVertex(_periodicSpace,_vnx[i], getComp(), keys);
	};
	for (int i=0; i<_vny.size(); i++){
		getKeysFromVertex(_periodicSpace,_vny[i],getComp(), keys);
	};
	getKeysFromVertex(_periodicSpace,_reference, getComp(), keys);
}


void CoonsPatchLagrangeConstraintElement::getLinearConstraints(const fullVector<double>& g,
                   std::map<Dof,DofAffineConstraint<double> >& con) const
{
  std::vector<std::vector<Dof> > kx, ky;
  for (int i=0; i<_vnx.size(); i++){
    std::vector<Dof> ktemp;
    getKeysFromVertex(_periodicSpace,_vnx[i],getComp(),ktemp);
    kx.push_back(ktemp);
  };
  for (int i=0; i<_vny.size(); i++){
    std::vector<Dof> ktemp;
    getKeysFromVertex(_periodicSpace,_vny[i],getComp(),ktemp);
    ky.push_back(ktemp);
  };

  std::vector<Dof> kp, kref;
  getKeysFromVertex(_periodicSpace,_vp,getComp(),kp);
  getKeysFromVertex(_periodicSpace,_reference,getComp(),kref);

  DofAffineConstraint<double> cons;
  for (int i=0; i<_ndofs; i++){
    for (int j=0; j<_vnx.size(); j++){
      cons.linear.push_back(std::pair<Dof,double>(kx[j][i],_FFx(j)));
    };
    for (int j=0; j<_vny.size(); j++){
      cons.linear.push_back(std::pair<Dof,double>(ky[j][i],_FFy(j)));
    };
    cons.linear.push_back(std::pair<Dof,double>(kref[i],-1.));
    cons.shift=g(i);
    con[kp[i]] = cons;
    cons.linear.clear();
  };
};

void  CoonsPatchLagrangeConstraintElement::getLinearConstraintsByVertices(std::map<Dof,DofAffineConstraint<double> >& con) const{
  if (_vrootN == NULL & _vrootP  == NULL) {
    Msg::Error("the root vertices are not defined CoonsPatchLagrangeConstraintElement::getLinearConstraintsByVertices");
    return;
  }
  else{
    std::vector<std::vector<Dof> > kx, ky;
    for (int i=0; i<_vnx.size(); i++){
      std::vector<Dof> ktemp;
      getKeysFromVertex(_periodicSpace,_vnx[i],getComp(),ktemp);
      kx.push_back(ktemp);
    };
    for (int i=0; i<_vny.size(); i++){
      std::vector<Dof> ktemp;
      getKeysFromVertex(_periodicSpace,_vny[i],getComp(),ktemp);
      ky.push_back(ktemp);
    };

    std::vector<Dof> kp, kref;
    getKeysFromVertex(_periodicSpace,_vp,getComp(),kp);
    getKeysFromVertex(_periodicSpace,_reference,getComp(),kref);

    std::vector<Dof> krootP, krootN;
    getKeysFromVertex(_periodicSpace,_vrootP,getComp(),krootP);
    getKeysFromVertex(_periodicSpace,_vrootN,getComp(),krootN);

    DofAffineConstraint<double> cons;
    for (int i=0; i<_ndofs; i++){
      for (int j=0; j<_vnx.size(); j++){
        cons.linear.push_back(std::pair<Dof,double>(kx[j][i],_FFx(j)));
      };
      for (int j=0; j<_vny.size(); j++){
        cons.linear.push_back(std::pair<Dof,double>(ky[j][i],_FFy(j)));
      };
      cons.linear.push_back(std::pair<Dof,double>(kref[i],-1.));
      cons.linear.push_back(std::pair<Dof,double>(krootP[i],1.0));
      cons.linear.push_back(std::pair<Dof,double>(krootN[i],-1.0));
      cons.shift=0.;
      con[kp[i]] = cons;
      cons.linear.clear();
    };
  }
};

void CoonsPatchLagrangeConstraintElement::getKinematicalVector(fullVector<double>& m) const{
	fullVector<double> kinVec;
	_mbc->getKinematicalVector(kinVec);
	m.resize(_ndofs);
	m.setAll(0.);
	_S.mult(kinVec,m);
};
void CoonsPatchLagrangeConstraintElement::getKinematicalMatrix(fullMatrix<double>& m) const{
  m = _S;
};
void CoonsPatchLagrangeConstraintElement::getLinearConstraints(std::map<Dof,DofAffineConstraint<double> >& con) const{
  fullVector<double> g;
  getKinematicalVector(g);
  getLinearConstraints(g,con);
};
