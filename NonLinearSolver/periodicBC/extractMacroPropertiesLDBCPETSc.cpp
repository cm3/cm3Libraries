//
//
// Description: class using PETSc for extraction of homogenized tangents
// in case of linear displacement BC
//
// Author:  <Van Dung NGUYEN>, (C) 2010
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "extractMacroPropertiesLDBCPETSc.h"
#include "functionPETSc.h"
#include "pbcSystems.h"
#include "highOrderTensor.h"

#if defined(HAVE_PETSC)
using namespace functionPETSc;


stiffnessCondensationLDBCPETSc::stiffnessCondensationLDBCPETSc(nlsDofManager* p, pbcAlgorithm* ma,
                           bool sflag, bool tflag,MPI_Comm com)
                          :_pAssembler(p),_pAl(ma),_bConMat(NULL),
                          _stressflag(sflag), _tangentflag(tflag),
                          _isInitialized(false){
  _comm = com;
};
stiffnessCondensationLDBCPETSc::~stiffnessCondensationLDBCPETSc(){
  clear();
};

void stiffnessCondensationLDBCPETSc::setSplittedDofSystem(pbcAlgorithm* pal){
  pal->getSplittedDof()->setSplittedDofsSystem(pbcDofManager::INTERNAL,pbcDofManager::DIRECT,pbcDofManager::INDIRECT);
};

void stiffnessCondensationLDBCPETSc::init(){
  if (!_isInitialized){
    _isInitialized = true;
    pbcDofManager* pbcAssembler = _pAl->getSplittedDof();
    _bConMat = new linearConstraintMatrixPETSc(_comm);
    int nb = pbcAssembler->sizeOfBoundaryDof();
    int nK = _pAl->getNumberOfMacroToMicroKinematicVariables();
    _pAl->computeLinearConstraintMatrixLDBC(_bConMat);

  }
};

void stiffnessCondensationLDBCPETSc::clear(){
  if (_isInitialized){
    _isInitialized = false;
    if (_bConMat) delete _bConMat;
    _bConMat = NULL;
  }
};

void stiffnessCondensationLDBCPETSc::stressSolve(fullVector<double>& stressVec, const double Vrve){
  if (_stressflag){
    int nK = _pAl->getNumberOfMacroToMicroKinematicVariables();
    stressVec.resize(nK);
    stressVec.setAll(0.);

    if (!_isInitialized){
      this->init();
    };
    pbcDofManager* pbcAssembler = _pAl->getSplittedDof();
    int sizeb = pbcAssembler->sizeOfBoundaryDof();
    Vec lambda;
    _try(VecCreate(_comm,&lambda));
    _try(VecSetSizes(lambda,sizeb,PETSC_DECIDE));
    _try(VecSetFromOptions(lambda));
    std::string str = "A";
    linearSystem<double>* lsys = _pAssembler->getLinearSystem(str);
    nonLinearSystem<double>* linearsys = dynamic_cast<nonLinearSystem<double>*>(lsys);
    pbcSystem<double>* pbcsys = dynamic_cast<pbcSystem<double>*>(lsys);
    if (pbcsys){
      const std::map<Dof,int>& boundaryDofMap = pbcAssembler->getDofMap(pbcDofManager::BOUNDARY);
      for (std::map<Dof,int>::const_iterator it = boundaryDofMap.begin(); it!= boundaryDofMap.end(); it++){
        double val = 0.;
        int num = _pAssembler->getDofNumber(it->first);
        if (num != -1)
          linearsys->getFromRightHandSideMinus(num, val);
        else
          Msg::Error("The pbcNonLinearSystem has to used");
        PetscInt row = it->second;
        _try(VecSetValues(lambda,1,&row,&val,INSERT_VALUES));
      }
    }
    else{
      Msg::Error("pbcNonLinearSystem must be used");
    }
    _try(VecAssemblyBegin(lambda));
    _try(VecAssemblyEnd(lambda));

    Vec P; // stress
    _try(VecCreate(_comm,&P));
    _try(VecSetSizes(P,nK,PETSC_DECIDE));
    _try(VecSetFromOptions(P));

    Mat& S = _bConMat->getKinematicalMatrix();

    _try(MatMultTranspose(S,lambda,P));
    _try(VecAssemblyBegin(P));
    _try(VecAssemblyEnd(P));


    PetscScalar* val;
    _try(VecGetArray(P,&val));
    for (int row = 0; row<nK; row++){
      stressVec(row) =val[row]/Vrve;
    };
    _try(VecDestroy(&lambda));
    _try(VecDestroy(&P));
  }
};

void stiffnessCondensationLDBCPETSc::tangentCondensationSolve(fullMatrix<double>& tangentMat, const double Vrve){
  if (_tangentflag){
    int nK = _pAl->getNumberOfMacroToMicroKinematicVariables();
    tangentMat.resize(nK,nK);
    tangentMat.setAll(0.);

    if (!_isInitialized){
      this->init();
    }

    splitStiffnessMatrixPETSc* _stiffness = dynamic_cast<splitStiffnessMatrixPETSc*>(_pAl->getSplitStiffness());
    if (_stiffness == NULL){
      Msg::Error("splitStiffnessMatrixPETSc has to be used");
    }

    Mat& Kii = _stiffness->getIIMat();
    Mat& Kib = _stiffness->getIBMat();
    Mat& Kbi = _stiffness->getBIMat();
    Mat& Kbb = _stiffness->getBBMat();

    Mat& S = _bConMat->getKinematicalMatrix();
    Mat Kibt, Kbit, Kbbt;
    Mat KbbS;
    _try(MatMatMult(Kbb,S,MAT_INITIAL_MATRIX,PETSC_DECIDE,&KbbS));
    _try(MatTransposeMatMult(S,KbbS,MAT_INITIAL_MATRIX,PETSC_DECIDE,&Kbbt));
    _try(MatDestroy(&KbbS));
    _try(MatMatMult(Kib,S,MAT_INITIAL_MATRIX,PETSC_DECIDE,&Kibt));
    _try(MatTransposeMatMult(S,Kbi,MAT_INITIAL_MATRIX,PETSC_DECIDE,&Kbit));

    Mat L;
    _try(functionPETSc::CondenseMatrixBlocks(Kii,Kibt,Kbit,Kbbt,&L));
    for (int row = 0; row< nK; ++row){
      for (int col = 0; col< nK; ++col){
        PetscScalar val;
        PetscInt ii = row, jj = col;
        _try(MatGetValues(L,1,&ii,1,&jj,&val));
        tangentMat(row,col) = val/Vrve;
      };
    }
    _try(MatDestroy(&L));
    _try(MatDestroy(&Kibt));
    _try(MatDestroy(&Kbit));
    _try(MatDestroy(&Kbbt));
  }
  else{
    printf("Error in rve volume /n");
  };

};
#endif // HAVE_PETSC
