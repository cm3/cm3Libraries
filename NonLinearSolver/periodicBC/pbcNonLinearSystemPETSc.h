//
//
// Description: nonLinearSystem using PETSC for micro BVPs
//
// Author:  <Van Dung NGUYEN>, (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef PBCNONLINEARSYSTEM_H_
#define PBCNONLINEARSYSTEM_H_

#include "pbcSystems.h"
#include "nonLinearSystems.h"
#include "OS.h"

/*
  // class non linear system with linear constraints
  // linear system is considered with K *x = b
  // K = scale*CTC+QT*a*Q
  //

*/

template<class scalar>
class pbcNonLinearSystemPETSc : public pbcSystem<scalar>,
                                public nonLinearSystemPETSc<scalar>{
	#if defined(HAVE_PETSC)
  protected:
    Mat _K; // structural stiffness
    Mat _C; // constraint matrix
    Vec _rc; // constraint residual
    bool _isAllocatedConstraintMatrix; // flag for allocating the matrix C, rc
    bool _constraintMatrixModified;

    // cache of project matrices
    bool _isComputedProjectMatrices;
    Mat _CT; // CT = transpose of C
    Mat _CTC; // CTC = C*CT
    Mat _R; // R = CT*inv(C*CT)
    Mat _Q; // Q = I-R*C = CT*inv(C*CT)*C --> Q is symmetric QT = Q

    int _constraintSize; // number of linear constraint == row of C
    double _scale;

    mutable bool _rhsflag; // flag for right hand side compuutation

    // kinematic matrix
    Mat _KinematicMatrix;
    int _dimKin;
    bool _isModifiedKinematicMatrix;
    bool _isAllocatedKinematicMatrix;

    // stress matrix
    Mat _StressMatrix;
    Mat _BodyForceMatrix;    // dBmdGM
    int _dimStress;
    bool _isModifiedStressMatrix;
    bool _isAllocatedStressMatrix;
    
    bool _isModifiedBodyForceMatrix;
    bool _isAllocatedBodyForceMatrix;
    
    bool _estimatedK;
    
    bool _NeedBodyForceVector;

  protected:
    virtual void createKSPSolver(){
      // create KSP
      if (!this->_kspAllocated)
          this->_kspCreate();
      // assemble stiffness matrix
      this->_assembleMatrixIfNeeded();
      if (this-> _matrixChangedSinceLastSolve and _isComputedProjectMatrices){
        //Msg::Info("recomputing stiffness matrix Pt*a*P");
        /**compute new stiffness matrix **/
        if ( _estimatedK){
          _try(MatDestroy(&this->_K));
        }
        _try(MatPtAP(this->_a,_Q,MAT_INITIAL_MATRIX,2.,&this->_K));
        _try(MatAXPY(this->_K,_scale,_CTC,DIFFERENT_NONZERO_PATTERN));
        _try(MatAssemblyBegin(this->_K,MAT_FINAL_ASSEMBLY));
        _try(MatAssemblyEnd(this->_K,MAT_FINAL_ASSEMBLY));
        _try(KSPSetOperators(this->_ksp, this->_K, this->_K, DIFFERENT_NONZERO_PATTERN));
        this-> _matrixChangedSinceLastSolve = false;
        _estimatedK = true;
      }
    };

  public:
    pbcNonLinearSystemPETSc(MPI_Comm com = PETSC_COMM_SELF) : nonLinearSystemPETSc<scalar>(com),pbcSystem<scalar>(),
						_rhsflag(false),_constraintSize(0),_isAllocatedConstraintMatrix(false),
						_isComputedProjectMatrices(false),_constraintMatrixModified(false),_scale(1.),
						_dimKin(0),_dimStress(0),
						_isModifiedKinematicMatrix(false), _isAllocatedKinematicMatrix(false),
						_isModifiedStressMatrix(false),_isAllocatedStressMatrix(false),
						_estimatedK(false),_isModifiedBodyForceMatrix(false),_isAllocatedBodyForceMatrix(false),
						_NeedBodyForceVector(false){
    };
		virtual ~pbcNonLinearSystemPETSc(){
      clear();
      clearConstraintMatrix();
		};

		virtual bool isAllocatedConstraintMatrix() const {
      return _isAllocatedConstraintMatrix;
		};
 		virtual void clearConstraintMatrix()  {
      if (_isAllocatedConstraintMatrix){
        _isAllocatedConstraintMatrix = false;
        _constraintMatrixModified = false;
        _try(MatDestroy(&_C));
        _try(VecDestroy(&_rc));
      }
      // clear cache
      if (_isComputedProjectMatrices){
 		    _isComputedProjectMatrices = false;
        _try(MatDestroy(&_CT));
        _try(MatDestroy(&_CTC));
        _try(MatDestroy(&_R));
        _try(MatDestroy(&_Q));
 		  }
 		};
		virtual void allocateConstraintMatrix(int nbcon, int systemSize){
      _constraintSize = nbcon;
      _isAllocatedConstraintMatrix = true;
      _constraintMatrixModified = false;
      _try(MatCreate(this->getComm(),&_C));
      _try(MatSetSizes(_C,_constraintSize,systemSize,PETSC_DETERMINE,PETSC_DETERMINE));
      _try(MatSetType(_C,MATSEQAIJ));
      _try(MatSetFromOptions(_C));

      #if (((PETSC_VERSION_MAJOR == 3) && (PETSC_VERSION_MINOR >= 3)))
      _try(MatSetUp(this->_C));
      #endif // (PETSC_VERSION_MAJOR == 3) && (PETSC_VERSION_MINOR >= 3)

      _try(VecCreate(this->getComm(),&_rc));
      _try(VecSetSizes(_rc,_constraintSize,PETSC_DECIDE));
      _try(VecSetFromOptions(_rc));
		};  // number of constraints

		virtual void addToConstraintMatrix(int row, int col, const scalar& val){
      PetscInt ii = row, jj = col;
      PetscScalar s = val;
      _try(MatSetValues(_C,1,&ii,1,&jj,&s,ADD_VALUES));
      _constraintMatrixModified = true;
    };

		virtual void addToConstraintResidual(int row, const scalar& val){
      PetscInt ii = row;
      PetscScalar s = val;
      _try(VecSetValues(_rc,1,&ii,&s,ADD_VALUES));
    };

		virtual void setScaleFactor(double scale){
      _scale = scale;
    };

    virtual void zeroConstraintMatrix(){
      if (_isAllocatedConstraintMatrix){
        _try(MatAssemblyBegin(_C,MAT_FINAL_ASSEMBLY));
        _try(MatAssemblyEnd(_C,MAT_FINAL_ASSEMBLY));
        _try(MatZeroEntries(_C));
        _constraintMatrixModified = false;
      }
    };

    virtual void zeroConstraintResidual() {
      if (_isAllocatedConstraintMatrix){
        _try(VecAssemblyBegin(this->_rc));
        _try(VecAssemblyEnd(this->_rc));
        _try(VecZeroEntries(this->_rc));
      };
		}

		virtual void computeConstraintProjectMatrices(){
      // this function work only when constraint matrix is modified through addToConstraintMatrix
      if (_constraintMatrixModified){
        if (_isComputedProjectMatrices){
          _try(MatDestroy(&_CT));
          _try(MatDestroy(&_CTC));
          _try(MatDestroy(&_R));
          _try(MatDestroy(&_Q));
          Msg::Info("Recomputing all constraint project matrices");
        }
        else{
          Msg::Info("Computing all constraint project matrices");
        }
        _try(MatAssemblyBegin(_C,MAT_FINAL_ASSEMBLY));
        _try(MatAssemblyEnd(_C,MAT_FINAL_ASSEMBLY));

        //get transpose of C
        _try(MatTranspose(_C,MAT_INITIAL_MATRIX,&_CT));
        _try(MatAssemblyBegin(_CT,MAT_FINAL_ASSEMBLY));
        _try(MatAssemblyEnd(_CT,MAT_FINAL_ASSEMBLY));
        //
        Mat CCT, invCCT;
        _try(MatMatMult(_C,_CT,MAT_INITIAL_MATRIX,PETSC_DEFAULT,&CCT));
        
        PetscBool useMatMatSolveToInvertMatrix = PETSC_TRUE;
        _try(PetscOptionsGetBool(PETSC_NULL, "", "-useMatMatSolveToInverseMatrix",
                             &useMatMatSolveToInvertMatrix, PETSC_NULL));                             
        if (useMatMatSolveToInvertMatrix){
          Msg::Info("use MatMatSolve to invert matrix");
          _try(functionPETSc::MatInverse_MatMatSolve(CCT,&invCCT));
        }
        else{
          Msg::Info("use KSP to invert matrix");
          _try(functionPETSc::MatInverse_Naive(CCT,&invCCT));
        }
        _try(MatMatMult(_CT,invCCT,MAT_INITIAL_MATRIX,PETSC_DEFAULT,&_R));
        _try(MatDestroy(&invCCT));
        _try(MatDestroy(&CCT));

        /*Q = I-RC */
        Mat mRC;
        _try(MatMatMult(_R,_C,MAT_INITIAL_MATRIX,PETSC_DEFAULT,&mRC));
        _try(MatScale(mRC,-1));
        
        _try(MatCreate(this->getComm(),&_Q));
        _try(MatSetSizes(_Q,this->_nbRows,this->_nbRows,PETSC_DETERMINE,PETSC_DETERMINE));
        _try(MatSetType(_Q,MATSEQAIJ));
        _try(MatSetFromOptions(_Q));
        PetscInt nz = 1;
        _try(MatSeqAIJSetPreallocation(_Q,nz,PETSC_NULL));
        for (int i=0; i< this->_nbRows; i++)
        {
          PetscScalar s = 1.;
          _try(MatSetValues(_Q, 1, &i, 1, &i, &s, ADD_VALUES));
        }
        _try(MatAssemblyBegin(_Q,MAT_FINAL_ASSEMBLY));
        _try(MatAssemblyEnd(_Q,MAT_FINAL_ASSEMBLY));
        PetscScalar  aa = 1.;
        _try(MatAXPY(_Q,aa,mRC,DIFFERENT_NONZERO_PATTERN));
        
        /*
        _try(MatMatMult(_R,_C,MAT_INITIAL_MATRIX,PETSC_DEFAULT,&_Q));
        _try(MatScale(_Q,-1));
        _try(MatShift(_Q,1.));
        */
        
        _try(MatMatMult(_CT,_C,MAT_INITIAL_MATRIX,PETSC_DEFAULT,&_CTC));
        
        _isComputedProjectMatrices = true;
        _constraintMatrixModified = false;
        Msg::Info("End computing all constraint project matrices");
        _try(MatDestroy(&mRC));
      }

		};

    virtual bool isAllocatedKinematicMatrix() const {return _isAllocatedKinematicMatrix;};
		virtual void clearKinematicMatrix() {
      if (_isAllocatedKinematicMatrix){
        _try(MatDestroy(&_KinematicMatrix));
        _isAllocatedKinematicMatrix = false;
        _isModifiedKinematicMatrix = false;
      }
		};
		virtual void allocateKinematicMatrix(int nbcon, int kinDim) {
      _isAllocatedKinematicMatrix = true;
      _isModifiedKinematicMatrix = false;

      _dimKin = kinDim;

      _try(MatCreate(this->getComm(),&_KinematicMatrix));
      _try(MatSetSizes(_KinematicMatrix,nbcon,_dimKin,PETSC_DETERMINE,PETSC_DETERMINE));
      _try(MatSetType(_KinematicMatrix,MATSEQAIJ));
      _try(MatSetFromOptions(_KinematicMatrix));
      #if (((PETSC_VERSION_MAJOR == 3) && (PETSC_VERSION_MINOR >= 3)))
      _try(MatSetUp(this->_KinematicMatrix));
      #endif // (PETSC_VERSION_MAJOR == 3) && (PETSC_VERSION_MINOR >= 3)
		};
		virtual void addToKinematicMatrix(int row, int col, const scalar& val) {
      PetscInt ii = row, jj = col;
      PetscScalar s = val;
      _try(MatSetValues(_KinematicMatrix,1,&ii,1,&jj,&s,ADD_VALUES));
      _isModifiedKinematicMatrix = true;
		};
		virtual void zeroKinematicMatrix() {
      if (_isAllocatedKinematicMatrix){
        _try(MatAssemblyBegin(_KinematicMatrix,MAT_FINAL_ASSEMBLY));
        _try(MatAssemblyEnd(_KinematicMatrix,MAT_FINAL_ASSEMBLY));
        _try(MatZeroEntries(_KinematicMatrix));
        _isModifiedKinematicMatrix = false;
      }
		};

      virtual bool isAllocatedStressMatrix() const {return _isAllocatedStressMatrix;};
      virtual void clearStressMatrix() {
      if (_isAllocatedStressMatrix){
        _try(MatDestroy(&_StressMatrix));
        _isAllocatedStressMatrix = false;
        _isModifiedStressMatrix = false;
      }
		};
      virtual void allocateStressMatrix(int stressDim, int nbR) {
      _isAllocatedStressMatrix = true;
      _isModifiedStressMatrix = false;
      _dimStress = stressDim;

      _try(MatCreate(this->getComm(),&_StressMatrix));
      _try(MatSetSizes(_StressMatrix,_dimStress,nbR,PETSC_DETERMINE,PETSC_DETERMINE));
      _try(MatSetType(_StressMatrix,MATSEQAIJ));
      _try(MatSetFromOptions(_StressMatrix));
      #if (((PETSC_VERSION_MAJOR == 3) && (PETSC_VERSION_MINOR >= 3)))
      _try(MatSetUp(this->_StressMatrix));
      #endif // (PETSC_VERSION_MAJOR == 3) && (PETSC_VERSION_MINOR >= 3)
    };
    virtual void addToStressMatrix(int row, int col, const scalar& val) {
      PetscInt ii = row, jj = col;
      PetscScalar s = val;
      _try(MatSetValues(_StressMatrix,1,&ii,1,&jj,&s,ADD_VALUES));
      _isModifiedStressMatrix = true;
    };
    virtual void zeroStressMatrix() {
      if (_isAllocatedStressMatrix){
        _try(MatAssemblyBegin(_StressMatrix,MAT_FINAL_ASSEMBLY));
        _try(MatAssemblyEnd(_StressMatrix,MAT_FINAL_ASSEMBLY));
        _try(MatZeroEntries(_StressMatrix));
        _isModifiedStressMatrix = false;
      }
    };
    
    
      virtual bool isAllocatedBodyForceMatrix() const {return _isAllocatedBodyForceMatrix;};
      virtual void clearBodyForceMatrix() {
      if (_isAllocatedBodyForceMatrix){
        _try(MatDestroy(&_BodyForceMatrix));
        _isAllocatedBodyForceMatrix = false;
        _isModifiedBodyForceMatrix = false;
      }
      };
      virtual void allocateBodyForceMatrix(int nbR) {
        _isAllocatedBodyForceMatrix = true;
        _isModifiedBodyForceMatrix = false;

        _try(MatCreate(this->getComm(),&_BodyForceMatrix));
        _try(MatSetSizes(_BodyForceMatrix,nbR,_dimKin,PETSC_DETERMINE,PETSC_DETERMINE));
        _try(MatSetType(_BodyForceMatrix,MATSEQAIJ));
        _try(MatSetFromOptions(_BodyForceMatrix));
      #if (((PETSC_VERSION_MAJOR == 3) && (PETSC_VERSION_MINOR >= 3)))
       _try(MatSetUp(this->_BodyForceMatrix));
      #endif // (PETSC_VERSION_MAJOR == 3) && (PETSC_VERSION_MINOR >= 3)
    };
    virtual void addToBodyForceMatrix(int row, int col, const scalar& val) {
      PetscInt ii = row, jj = col;
      PetscScalar s = val;
      _try(MatSetValues(_BodyForceMatrix,1,&ii,1,&jj,&s,ADD_VALUES));
      _isModifiedBodyForceMatrix = true;
    };
    virtual void zeroBodyForceMatrix(){
      if (_isAllocatedBodyForceMatrix){
        _try(MatAssemblyBegin(_BodyForceMatrix,MAT_FINAL_ASSEMBLY));
        _try(MatAssemblyEnd(_BodyForceMatrix,MAT_FINAL_ASSEMBLY));
        _try(MatZeroEntries(_BodyForceMatrix));
        _isModifiedBodyForceMatrix = false;
      }
    };
    
    virtual void addToBodyForceVector(int row, const scalar& val) {};
    virtual void zeroBodyForceVector(){};
    virtual bool NeedBodyForceVector() const {return _NeedBodyForceVector;};
    virtual void setNeedBodyForceVector(bool needBodyForceVector){_NeedBodyForceVector=needBodyForceVector;}

    virtual void clear(){
      nonLinearSystemPETSc<scalar>::clear();
			if (_estimatedK){
				_try(MatDestroy(&_K));
				 _estimatedK = false;
			};

		};

		virtual void zeroRightHandSide(){
			nonLinearSystemPETSc<scalar>::zeroRightHandSide();
			this->zeroConstraintResidual();
			_rhsflag = false;
		};

		virtual double getInstabilityCriterion() const {
      Msg::Warning("pbcNonLinearSystemPETSc::getInstabilityCriterion is not implemented");
      return 1.;
      //#endif
		};

		virtual int systemSolve(){
      createKSPSolver();
      /** compute new right hand side**/
			if (!_rhsflag){
        Vec Rrc; // Rrc = R*rc;
        Vec RHS; // RHS = fext - fint + KRrc
        Vec CTrc; //CTrc = CT*rc;
        _try(VecDuplicate(this->_x,&Rrc));
        _try(VecDuplicate(this->_x,&RHS));
        _try(VecDuplicate(this->_x,&CTrc));

        _try(MatMult(_R,_rc,Rrc));
        _try(MatMult(this->_a,Rrc,RHS));

        _try(VecAXPY(RHS,1.,this->_Fext));
        _try(VecAXPY(RHS,-1.,this->_Fint));

        _try(MatMult(_CT,_rc,CTrc));
        _try(MatMult(_Q,RHS,this->_b));

        PetscScalar minusscale = -1.*_scale;
        _try(VecAXPY(this->_b,minusscale,CTrc));

        _try(VecAssemblyBegin(this->_b));
        _try(VecAssemblyEnd(this->_b));
        _try(VecDestroy(&Rrc));
        _try(VecDestroy(&RHS));
        _try(VecDestroy(&CTrc));
        _rhsflag = true;
			}
			
			// implementation based on the following work
			// Matthies, Hermann, and Gilbert Strang. 
			// "The solution of nonlinear finite element equations." 
			// International journal for numerical methods in engineering 14.11 (1979): 1613-1626.
			//
      if (this->_withBFGS)
      {
        Msg::Info("BFGS iteration = %d BFGSDeltas.size=%d BFGSGammas.size = %d",
                this->_BFGSCounter, this->_BFGSDeltas.size(), this->_BFGSGammas.size());
        if (this->_BFGSCounter == 0)
        {
          // solve system
          _try(KSPSolve(this->_ksp, this->_b, this->_x));
          if (this->_BFGSDeltas.find(this->_BFGSCounter+1) == this->_BFGSDeltas.end())
          {
            Vec& v = this->_BFGSDeltas[this->_BFGSCounter+1];
            _try(VecDuplicate(this->_x, &v));
          }
          _try(VecCopy(this->_x, this->_BFGSDeltas[this->_BFGSCounter+1]));
          _try(VecCopy(this->_b, this->_lastResidual));
          _try(VecScale(this->_lastResidual, -1.));
        }
        else
        {
          double val;
          Vec q;
          _try(VecDuplicate(this->_b, &q));
          _try(VecCopy(this->_b, q));
          _try(VecScale(q, -1));
          
          if (this->_BFGSGammas.find(this->_BFGSCounter) == this->_BFGSGammas.end())
          {
            Vec& v = this->_BFGSGammas[this->_BFGSCounter];
            _try(VecDuplicate(this->_x, &v));
          }
          _try(VecAXPBYPCZ(this->_BFGSGammas[this->_BFGSCounter], 1., -1., 0., q, this->_lastResidual));
          _try(VecDot(this->_BFGSDeltas[this->_BFGSCounter], this->_BFGSGammas[this->_BFGSCounter], &val));
          this->_BFGSRhos[this->_BFGSCounter] = 1./val;
          
          _try(VecCopy(q, this->_lastResidual));        
          //
          std::vector<double> alphas(this->_BFGSCounter,0.);
          for (int j= this->_BFGSCounter-1; j>-1; j--)
          {
            _try(VecDot(this->_BFGSDeltas[j+1], q, &val));
            alphas[j] = this->_BFGSRhos[j+1]*val;
            _try(VecAXPY(q, -alphas[j], this->_BFGSGammas[j+1]));
          }
          _try(VecCopy(q, this->_b));
          _try(KSPSolve(this->_ksp, this->_b, this->_x));
          for (int j=1; j< this->_BFGSCounter+1; j++)
          {
            _try(VecDot(this->_BFGSGammas[j], this->_x, &val));
            _try(VecAXPY(this->_x, alphas[j-1]-this->_BFGSRhos[j]*val, this->_BFGSDeltas[j]));
          }
           _try(VecDestroy(&q));
           // update solution
          _try(VecScale(this->_x, -1.));
          if (this->_BFGSDeltas.find(this->_BFGSCounter+1) == this->_BFGSDeltas.end())
          {
            Vec& v = this->_BFGSDeltas[this->_BFGSCounter+1];
            _try(VecDuplicate(this->_x, &v));
          }
          _try(VecCopy(this->_x, this->_BFGSDeltas[this->_BFGSCounter+1]));
        }
        this->_BFGSCounter += 1;
      }
      else
      {
        _try(KSPSolve(this->_ksp, this->_b, this->_x));
      }

     //t = Cpu() - t;
     //Msg::Info("Done solving pbc system (%f s)",t);
     _try(VecAXPY(this->_xsol,1.,this->_x));

			return 1;
		};

		virtual double normInfRightHandSide() const{
      /** compute new right hand side**/
			if (!_rhsflag){
			  _try(MatAssemblyBegin(this->_a,MAT_FINAL_ASSEMBLY));
        _try(MatAssemblyEnd(this->_a,MAT_FINAL_ASSEMBLY));
        Vec Rrc; // Rrc = R*rc;
        Vec RHS; // RHS = fext - fint + KRrc
        Vec CTrc; //CTrc = CT*rc;
        _try(VecDuplicate(this->_x,&Rrc));
        _try(VecDuplicate(this->_x,&RHS));
        _try(VecDuplicate(this->_x,&CTrc));

        _try(MatMult(_R,_rc,Rrc));
        _try(MatMult(this->_a,Rrc,RHS));

        _try(VecAXPY(RHS,1.,this->_Fext));
        _try(VecAXPY(RHS,-1.,this->_Fint));

        _try(MatMult(_CT,_rc,CTrc));
        _try(MatMult(_Q,RHS,this->_b));

        PetscScalar minusscale = -1.*_scale;
        _try(VecAXPY(this->_b,minusscale,CTrc));

        _try(VecAssemblyBegin(this->_b));
        _try(VecAssemblyEnd(this->_b));
        _try(VecDestroy(&Rrc));
        _try(VecDestroy(&RHS));
        _try(VecDestroy(&CTrc));
        _rhsflag = true;
			}
			return linearSystemPETSc<scalar>::normInfRightHandSide();
		};


   virtual int condensationSolve(fullMatrix<double>& L, const double rvevolume,const bool needdUdF,const bool needdUdG, nlsDofManager* pAss=NULL,  std::map<Dof, STensor3 >* dUdF=NULL, 
                               std::map<Dof, STensor33 >* dUdG=NULL) {
      L.resize(_dimStress,_dimKin);
      L.setAll(0.);

      if(needdUdF){
           dUdF->clear();
           if(needdUdG)
             dUdG->clear();
      if (_isModifiedBodyForceMatrix){
          _try(MatAssemblyBegin(_BodyForceMatrix,MAT_FINAL_ASSEMBLY));
          _try(MatAssemblyEnd(_BodyForceMatrix,MAT_FINAL_ASSEMBLY));
          _isModifiedBodyForceMatrix = false;
        }
      }
       createKSPSolver();

      if (_isModifiedKinematicMatrix){
        _try(MatAssemblyBegin(_KinematicMatrix,MAT_FINAL_ASSEMBLY));
        _try(MatAssemblyEnd(_KinematicMatrix,MAT_FINAL_ASSEMBLY));
        _isModifiedKinematicMatrix = false;
      }

      if (_isModifiedStressMatrix){
        _try(MatAssemblyBegin(_StressMatrix,MAT_FINAL_ASSEMBLY));
        _try(MatAssemblyEnd(_StressMatrix,MAT_FINAL_ASSEMBLY));
        _isModifiedStressMatrix = false;
      }


      // compute kinematic kinMatRHS
      // MatRHS = (CT-QT*K*R)*_KinematicMatrix
      Mat kinMatRHS;
      Mat KRkMat, QTKRkMat;
      _try(MatMatMult(_CT,_KinematicMatrix,MAT_INITIAL_MATRIX,PETSC_DEFAULT,&kinMatRHS));
      _try(MatScale(kinMatRHS,_scale));
      _try(MatMatMatMult(this->_a,_R,_KinematicMatrix,MAT_INITIAL_MATRIX,PETSC_DEFAULT,&KRkMat));
      _try(MatTransposeMatMult(_Q,KRkMat,MAT_INITIAL_MATRIX,PETSC_DEFAULT,&QTKRkMat));
      _try(MatAXPY(kinMatRHS,-1.,QTKRkMat,DIFFERENT_NONZERO_PATTERN));

      if(needdUdF){
        Mat QTBmdGMat;
        _try(MatTransposeMatMult(_Q,_BodyForceMatrix,MAT_INITIAL_MATRIX,PETSC_DEFAULT,&QTBmdGMat));
        _try(MatAXPY(kinMatRHS,1.,QTBmdGMat,DIFFERENT_NONZERO_PATTERN));
        _try(MatDestroy(&QTBmdGMat));
      }
      _try(MatDestroy(&QTKRkMat));
      _try(MatDestroy(&KRkMat));

      //
      Vec kinematicRHS;
      _try(VecDuplicate(this->_x,&kinematicRHS));
      Vec x;
      _try(VecDuplicate(kinematicRHS,&x));
      //
      Vec StressMatX;
      _try(VecCreate(this->getComm(), &StressMatX));
      _try(VecSetSizes(StressMatX, _dimStress, PETSC_DETERMINE));
      _try(VecSetFromOptions(StressMatX));

      // solve for each column of _KinematicMatrix
      for (int i=0; i< this->_dimKin; i++){
        PetscInt col = i;
        _try(MatGetColumnVector(kinMatRHS,kinematicRHS,col));
        PetscReal NormRHS;
        _try(VecNorm(kinematicRHS,NORM_INFINITY,&NormRHS));

        if (NormRHS > 0.){
          // solve if norm greater than 0
          _try(KSPSolve(this->_ksp, kinematicRHS, x));

          MatMult(_StressMatrix,x,StressMatX);

          PetscScalar* val;
          _try(VecGetArray(StressMatX,&val));
          for (int c=0; c<_dimStress; c++){
            L(c,i) = val[c]/rvevolume;
          }
          //
        }        
        if(needdUdF){
           if(i<9){ 
             int k, l; 
             PetscScalar x_val;
             Tensor23::getIntsFromIndex(i,k,l); // convert vector to matrix
             const std::map<Dof, int>& unknown = pAss->getUnknownMap();

             for (std::map<Dof, int>::const_iterator it= unknown.begin(); it != unknown.end(); it++) {
               STensor3& dUcompdF = dUdF->operator[](it->first);
               PetscInt idof = it->second;
               PetscInt ix[]={idof};
               VecGetValues(x,1,ix,&x_val);
               dUcompdF(k,l) = x_val;                             
             }
           }          
        }
        
        if(needdUdG){
           if(i>=9 and i<36){ 
             int k, l, s; 
             PetscScalar x_val;
             Tensor33::getIntsFromIndex(i,k,l,s); // convert vector to matrix
             const std::map<Dof, int>& unknown = pAss->getUnknownMap();

             for (std::map<Dof, int>::const_iterator it= unknown.begin(); it != unknown.end(); it++) {
               STensor33& dUcompdG = dUdG->operator[](it->first);
               PetscInt idof = it->second;
               PetscInt ix[]={idof};
               VecGetValues(x,1,ix,&x_val);
               dUcompdG(k,l,s) = x_val;                             
             }
           }          
        }       
      }
      _try(MatDestroy(&kinMatRHS));
      _try(VecDestroy(&x));
      _try(VecDestroy(&StressMatX));
      _try(VecDestroy(&kinematicRHS));
      return 1;
      };
      
      
   virtual int condensationdUdF(nlsDofManager* pAss=NULL,  std::map<Dof, STensor3 >* dUdF=NULL) {
      dUdF->clear();
      if (_isModifiedBodyForceMatrix){
          _try(MatAssemblyBegin(_BodyForceMatrix,MAT_FINAL_ASSEMBLY));
          _try(MatAssemblyEnd(_BodyForceMatrix,MAT_FINAL_ASSEMBLY));
          _isModifiedBodyForceMatrix = false;
      }
      
      createKSPSolver();

      if (_isModifiedKinematicMatrix){
        _try(MatAssemblyBegin(_KinematicMatrix,MAT_FINAL_ASSEMBLY));
        _try(MatAssemblyEnd(_KinematicMatrix,MAT_FINAL_ASSEMBLY));
        _isModifiedKinematicMatrix = false;
      }

      // compute kinematic kinMatRHS
      // MatRHS = (CT-QT*K*R)*_KinematicMatrix
      Mat kinMatRHS;
      Mat KRkMat, QTKRkMat;
      _try(MatMatMult(_CT,_KinematicMatrix,MAT_INITIAL_MATRIX,PETSC_DEFAULT,&kinMatRHS));
      _try(MatScale(kinMatRHS,_scale));
      _try(MatMatMatMult(this->_a,_R,_KinematicMatrix,MAT_INITIAL_MATRIX,PETSC_DEFAULT,&KRkMat));
      _try(MatTransposeMatMult(_Q,KRkMat,MAT_INITIAL_MATRIX,PETSC_DEFAULT,&QTKRkMat));
      _try(MatAXPY(kinMatRHS,-1.,QTKRkMat,DIFFERENT_NONZERO_PATTERN));

      Mat QTBmdGMat;
      _try(MatTransposeMatMult(_Q,_BodyForceMatrix,MAT_INITIAL_MATRIX,PETSC_DEFAULT,&QTBmdGMat));
      _try(MatAXPY(kinMatRHS,1.,QTBmdGMat,DIFFERENT_NONZERO_PATTERN));
      _try(MatDestroy(&QTBmdGMat));
      
      _try(MatDestroy(&QTKRkMat));
      _try(MatDestroy(&KRkMat));

      //
      Vec kinematicRHS;
      _try(VecDuplicate(this->_x,&kinematicRHS));
      Vec x;
      _try(VecDuplicate(kinematicRHS,&x));

      // solve for each column of _KinematicMatrix
      for (int i=0; i< 9; i++){
        PetscInt col = i;
        _try(MatGetColumnVector(kinMatRHS,kinematicRHS,col));
        PetscReal NormRHS;
        _try(VecNorm(kinematicRHS,NORM_INFINITY,&NormRHS));

        if (NormRHS > 0.){
          // solve if norm greater than 0
          _try(KSPSolve(this->_ksp, kinematicRHS, x));
        }        
        int k, l; 
        PetscScalar x_val;
        Tensor23::getIntsFromIndex(i,k,l); // convert vector to matrix
        const std::map<Dof, int>& unknown = pAss->getUnknownMap();

        for (std::map<Dof, int>::const_iterator it= unknown.begin(); it != unknown.end(); it++) {
          STensor3& dUcompdF = dUdF->operator[](it->first);
          PetscInt idof = it->second;
          PetscInt ix[]={idof};
          VecGetValues(x,1,ix,&x_val);
          dUcompdF(k,l) = x_val;                             
        }               
      }
        
      _try(MatDestroy(&kinMatRHS));
      _try(VecDestroy(&x));
      _try(VecDestroy(&kinematicRHS));
      return 1;
   };
      
  #else
  public:
    pbcNonLinearSystemPETSc(){Msg::Error("Petsc is not available");};
    virtual ~pbcNonLinearSystemPETSc(){};
    virtual bool isAllocatedConstraintMatrix() const {return false;}; // allocated flag
 		virtual void clearConstraintMatrix()  {}; // clear function
    virtual void allocateConstraintMatrix(int nbcon, int nbR) {};  // constraint dof number, system dof number
    virtual void addToConstraintMatrix(int row, int col, const scalar& val) {}; // add to constraint matrix
		virtual void addToConstraintResidual(int row, const scalar& val) {}; // add to constraint residual
		virtual void setScaleFactor(double scale) {}; // scale factor
		virtual void zeroConstraintMatrix() {}; // zero constraint matrix
		virtual void zeroConstraintResidual() {}; // zero constraint residual

		virtual void computeConstraintProjectMatrices() {}; // compute all constraint projection matrices

		// for estimation of tangent
		virtual bool isAllocatedKinematicMatrix() const {return false;};
		virtual void clearKinematicMatrix() {};
		virtual void allocateKinematicMatrix(int nbcon, int kinDim) {};
		virtual void addToKinematicMatrix(int row, int col, const scalar& val) {};
		virtual void zeroKinematicMatrix() {};

		virtual bool isAllocatedStressMatrix() const {return false;};
		virtual void clearStressMatrix() {};
		virtual void allocateStressMatrix(int stressDim, int nbR) {};
    virtual void addToStressMatrix(int row, int col, const scalar& val) {};
    virtual void zeroStressMatrix(){};
    
    virtual void addToBodyForceMatrix(int row, int col, const scalar& val) {};
    virtual void zeroBodyForceMatrix(){};
    virtual void addToBodyForceVector(int row, const scalar& val) {};
    virtual void zeroBodyForceVector(){};
    virtual bool NeedBodyForceVector() const {return false;};
    virtual void setNeedBodyForceVector(bool needBodyForceVector){}
    

    // tangent solve
    virtual int condensationSolve(fullMatrix<double>& L, const double rvevolume,const bool fl=false, const bool fl1=false, nlsDofManager* pAss=NULL, 
    std::map<Dof, STensor3 >* dUdF=NULL, std::map<Dof, STensor33 >* dUdG=NULL)  {};
    virtual int condensationdUdF(nlsDofManager* pAss=NULL,  std::map<Dof, STensor3 >* dUdF=NULL)  {};

	#endif
};
#endif // PBCNONLINEARSYSTEM_H_
