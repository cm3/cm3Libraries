//
//
// Description: based class for micro BVPs with linear constraints
//
// Author:  <Van Dung NGUYEN>, (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef PBCSYSTEMS_H_
#define PBCSYSTEMS_H_

template<class scalar>
class pbcSystemBase{
  public:
    virtual ~pbcSystemBase(){}
    virtual void computeConstraintProjectMatrices() =0; // compute all constraint projection matrices
    virtual bool isAllocatedStressMatrix() const = 0;
		virtual void clearStressMatrix() = 0;
		virtual void allocateStressMatrix(int stressDim, int nbR) = 0;
    virtual void addToStressMatrix(int row, int col, const scalar& val) = 0;
    virtual void zeroStressMatrix() = 0;
    
    virtual bool isAllocatedBodyForceMatrix() const = 0;
    virtual void clearBodyForceMatrix() = 0;
    virtual void allocateBodyForceMatrix(int nbR) = 0;    
    virtual void addToBodyForceMatrix(int row, int col, const scalar& val) = 0;
    virtual void zeroBodyForceMatrix() = 0;
    virtual void zeroBodyForceVector() = 0;
    virtual void addToBodyForceVector(int row, const scalar& val) = 0;
    virtual bool NeedBodyForceVector() const = 0;
    virtual void setNeedBodyForceVector(bool needBodyForceVector) = 0;

    virtual int condensationSolve(fullMatrix<double>& L, const double rvevolume, const bool fl=false, const bool fl1=false, nlsDofManager* pAss=NULL, 
                 std::map<Dof, STensor3 >* dUdF=NULL,  std::map<Dof, STensor33>* dUdG=NULL)  = 0;
    virtual int condensationdUdF(nlsDofManager* pAss=NULL, std::map<Dof, STensor3 >* dUdF=NULL)  = 0;
};

// interface to introduce linear constraints
template<class scalar>
class pbcSystem : public pbcSystemBase<scalar>{
	public:
		virtual ~pbcSystem(){};
		virtual bool isAllocatedConstraintMatrix() const=0; // allocated flag
 		virtual void clearConstraintMatrix()  = 0; // clear function
    virtual void allocateConstraintMatrix(int nbcon, int nbR) = 0;  // constraint dof number, system dof number
    virtual void addToConstraintMatrix(int row, int col, const scalar& val) = 0; // add to constraint matrix
		virtual void addToConstraintResidual(int row, const scalar& val) = 0; // add to constraint residual
		virtual void setScaleFactor(double scale) = 0; // scale factor
		virtual void zeroConstraintMatrix() =0; // zero constraint matrix
		virtual void zeroConstraintResidual() =0; // zero constraint residual

		virtual void computeConstraintProjectMatrices() =0; // compute all constraint projection matrices

		// for estimation of tangent
		virtual bool isAllocatedKinematicMatrix() const = 0;
		virtual void clearKinematicMatrix() = 0;
		virtual void allocateKinematicMatrix(int nbcon, int kinDim) = 0;
		virtual void addToKinematicMatrix(int row, int col, const scalar& val) = 0;
		virtual void zeroKinematicMatrix() = 0;

		virtual bool isAllocatedStressMatrix() const = 0;
		virtual void clearStressMatrix() = 0;
		virtual void allocateStressMatrix(int stressDim, int nbR) = 0;
    virtual void addToStressMatrix(int row, int col, const scalar& val) = 0;
    virtual void zeroStressMatrix() = 0;
    virtual bool isAllocatedBodyForceMatrix() const = 0;
    virtual void clearBodyForceMatrix() = 0;
    virtual void allocateBodyForceMatrix(int nbR) = 0;
    virtual void addToBodyForceMatrix(int row, int col, const scalar& val) = 0;
    virtual void zeroBodyForceMatrix() = 0;
    virtual void zeroBodyForceVector() = 0;
    virtual void addToBodyForceVector(int row, const scalar& val) = 0;
    virtual bool NeedBodyForceVector() const{};
    virtual void setNeedBodyForceVector(bool needBodyForceVector) {};

    // tangent solve
    virtual int condensationSolve(fullMatrix<double>& L, const double rvevolume, const bool fl=false, const bool fl1=false, nlsDofManager* pAss=NULL, 
		 std::map<Dof, STensor3 >* dUdF=NULL, std::map<Dof, STensor33 >* dUdG=NULL)  = 0;
    virtual int condensationdUdF(nlsDofManager* pAss=NULL, std::map<Dof, STensor3 >* dUdF=NULL)  = 0;
};

template<class scalar>
class pbcSystemConden : public pbcSystemBase<scalar>{
  public:
    virtual ~pbcSystemConden(){}
    virtual void zeroKinematicVector() = 0;
    virtual void addToKinematicVector(int row, const scalar& val) = 0;
    virtual void initialKinematicVector(int row, const scalar& val) = 0;
		virtual void updateSystemUnknown()  = 0;
    virtual void setPBCAlgorithm(pbcAlgorithm* p) = 0;
    virtual void computeConstraintProjectMatrices() =0; // compute all constraint projection matrices

    virtual bool isAllocatedStressMatrix() const = 0;
		virtual void clearStressMatrix() = 0;
		virtual void allocateStressMatrix(int stressDim, int nbR) = 0;
    virtual void addToStressMatrix(int row, int col, const scalar& val) = 0;
    virtual void zeroStressMatrix() = 0;
    virtual bool isAllocatedBodyForceMatrix() const = 0;
    virtual void clearBodyForceMatrix() = 0;
    virtual void allocateBodyForceMatrix(int nbR) = 0;
    virtual void addToBodyForceMatrix(int row, int col, const scalar& val) = 0;
    virtual void zeroBodyForceMatrix() = 0;
    virtual void zeroBodyForceVector() {};
    virtual void addToBodyForceVector(int row, const scalar& val) {};
    virtual bool NeedBodyForceVector() const {};
    virtual void setNeedBodyForceVector(bool needBodyForceVector) {};
    virtual int condensationSolve(fullMatrix<double>& L, const double rvevolume, const bool fl=false, const bool fl1=false,  nlsDofManager* pAss=NULL, 
                 std::map<Dof, STensor3 >* dUdF=NULL, std::map<Dof, STensor33 >* dUdG=NULL)  = 0;
    virtual int condensationdUdF(nlsDofManager* pAss=NULL, std::map<Dof, STensor3 >* dUdF=NULL)  = 0;
};

#endif // PBCSYSTEMS_H_
