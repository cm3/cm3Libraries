//
//
// Description: supplement constraint elements for coupled problems
//
// Author:  <L. Wu>, (C) 2015
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "pbcSupplementConstraintExtraDofDiffusion.h"
//#include "ipFiniteStrain.h"

supplementConstraintAverageExtraDofValue::supplementConstraintAverageExtraDofValue(nonLinearMicroBC* mbc, FunctionSpaceBase* mspace,
                          const int c, const std::vector<partDomain*> *g):
													constraintElement(mbc,c),_multSpace(mspace), _domainVector(g),_macroEnergyDensity(0.)
{

  if(_ndofs != 1) Msg::Error("supplementConstraintAverageExtraDofValue::supplementConstraintAverageExtraDofValue should be built on only one degree of freedom");
  //create the map of all the vertices
	// create map Dof

	_extraDofIndex = _component[0]- _mbc->getTotalNumberOfMechanicalDofs();
	_RowIndex= _mbc->getNumberOfMechanicalKinematicVariables()+ 3+_extraDofIndex*4;

	_mapDof.clear();
	_allDof.clear();
	// number dof based on vertex
	for(int idom =0; idom < _domainVector->size(); idom++){
		partDomain* dom = (*_domainVector)[idom];
		for (elementGroup::vertexContainer::const_iterator itv = dom->vertex_begin(); itv != dom->vertex_end(); itv++){
			MVertex* v = itv->second;
			FunctionSpaceBase* space = dom->getFunctionSpace();
			std::vector<Dof> keys;
			getKeysFromVertex(space,v,getComp(),keys);
			for (int i=0; i< keys.size(); i++){
				if (_mapDof.find(keys[i]) == _mapDof.end()){
					int mapSize = _mapDof.size();
					_mapDof[keys[i]] = mapSize;
					_allDof.push_back(keys[i]);
				}
			}
		}
	};

	_C.resize(1,_allDof.size());
	_C.setAll(0.);
	_macroEnergyDensity  = 0.;
	_RVEVolume = 0.;

	IntPt* GP;
	double jac[3][3];
	double val[256];
	for(std::vector<partDomain*>::const_iterator itdom=_domainVector->begin(); itdom!=_domainVector->end(); ++itdom){
		partDomain* dom = *itdom;
		materialLaw* mlaw = dom->getMaterialLaw();
		QuadratureBase* integrationRule = dom->getBulkGaussIntegrationRule();
		FunctionSpaceBase* space = dom->getFunctionSpace();
		for (elementGroup::elementContainer::const_iterator ite = dom->element_begin(); ite != dom->element_end(); ite++){
			MElement* ele = ite->second;
			_RVEVolume += ele->getVolume();
			if (mlaw->getNumberOfExtraDofsDiffusion() > 0){
				// get all keys on this element to assemble
				int npts = integrationRule->getIntPoints(ele,&GP);
				for (int i=0; i< npts; i++){
					double u = GP[i].pt[0]; double v = GP[i].pt[1]; double w = GP[i].pt[2]; double wgt = GP[i].weight;
					double detJ = ele->getJacobian(u, v, w, jac);
					double extraDofStoredEnergyPerUnitField=mlaw->getInitialExtraDofStoredEnergyPerUnitField(); // cp take its initial value
					double ratio = wgt*detJ*extraDofStoredEnergyPerUnitField;
					_macroEnergyDensity += ratio;
					//
					ele->getShapeFunctions(u,v,w,val);
					for (int j=0; j<ele->getNumVertices(); j++){
						 MVertex* vertex= ele->getVertex(j);
						 std::vector<Dof> key;
						 getKeysFromVertex(space,vertex,getComp(),key);
						 _C(0,_mapDof[key[0]]) += val[j]*ratio;
					}
				}
			}
		}

	}
	double rveVolumeInv = 1./_RVEVolume;
	_macroEnergyDensity *= rveVolumeInv;
	_C.scale(rveVolumeInv);

	// find maximal coefficient
	_positive = -1;
  double maxVal = 0.;
	bool found = false;
  for (int i=0; i< _allDof.size(); i++){
    if ((fabs(_C(0,i))>maxVal) && (constraintElement::allPositiveDof.find(_allDof[i]) == constraintElement::allPositiveDof.end())){
      _positive = i;
			maxVal = fabs(_C(0,i));
			found = true;
    };
  };
	if (!found) Msg::Error("all Dofs in this constraint are numbered as positive dof in other constraints supplementConstraintAverageExtraDofValue::supplementConstraintAverageExtraDofValue");


	double invMaxVal = 1./_C(0,_positive);
	_C.scale(invMaxVal);
	_macroEnergyDensity *= invMaxVal;

	_Cbc.resize(1,_allDof.size()-1);
	_Cbc.setAll(0.);
	int col = 0;
	for (int i=0; i< _allDof.size(); i++){
		if (i!=_positive){
			_Cbc(0,col) = -1.*_C(0,i);
			col++;
		}
	}
  // add positive to positive vertex list
  constraintElement::allPositiveDof.insert(_allDof[_positive]);

	// for mult key generation
	_tempVertex = new MVertex(0.,0.,0.);

	_S.resize(_ndofs,_mbc->getNumberOfMacroToMicroKinematicVariables());
  _S.setAll(0.);
  _S(0,_RowIndex) = _macroEnergyDensity;
};

supplementConstraintAverageExtraDofValue::~supplementConstraintAverageExtraDofValue(){
	if (_tempVertex != NULL){
		delete _tempVertex;
		_tempVertex = NULL;
	}
};

void supplementConstraintAverageExtraDofValue::computeWeightAndMacroEnergyDensity(const IPField* ipf, fullMatrix<double>& newWeight, double& energDensity)
{
  newWeight.resize(1,_allDof.size());
  newWeight.setAll(0.);
  energDensity = 0;

  IntPt* GP;
	double jac[3][3];
	double val[256];
	const IPVariable *vipv[256];
	for(std::vector<partDomain*>::const_iterator itdom=_domainVector->begin(); itdom!=_domainVector->end(); ++itdom){
		partDomain* dom = *itdom;
		materialLaw* mlaw = dom->getMaterialLaw();
		QuadratureBase* integrationRule = dom->getBulkGaussIntegrationRule();
		FunctionSpaceBase* space = dom->getFunctionSpace();
		if (mlaw->getNumberOfExtraDofsDiffusion() > 0){
			for (elementGroup::elementContainer::const_iterator ite = dom->element_begin(); ite != dom->element_end(); ite++){
				MElement* ele = ite->second;
				// get all keys on this element to assemble
				int npts = integrationRule->getIntPoints(ele,&GP);
				if (dom->inMultiscaleSimulation()){
          ipf->getIPv(ele,vipv,IPStateBase::initial);
        }
        else{
          ipf->getIPv(ele,vipv,IPStateBase::previous);
        }
				for (int i=0; i< npts; i++){
					double u = GP[i].pt[0]; double v = GP[i].pt[1]; double w = GP[i].pt[2]; double wgt = GP[i].weight;
					double detJ = ele->getJacobian(u, v, w, jac);
					double T = vipv[i]->get(IPField::TEMPERATURE);
					double extraDofStoredEnergyPerUnitField=mlaw->getExtraDofStoredEnergyPerUnitField(T); // take value at T
					double ratio = wgt*detJ*extraDofStoredEnergyPerUnitField;
					energDensity += ratio;
					//
					ele->getShapeFunctions(u,v,w,val);
					for (int j=0; j<ele->getNumVertices(); j++){
						 MVertex* vertex= ele->getVertex(j);
						 std::vector<Dof> key;
						 getKeysFromVertex(space,vertex,getComp(),key);
						 newWeight(0,_mapDof[key[0]]) += val[j]*ratio;
					}
				}
			}
		}

	}
	double rveVolumeInv = 1./_RVEVolume;
	energDensity *= rveVolumeInv;
	newWeight.scale(rveVolumeInv);

  double invs = 1./(newWeight(0,_positive));
  newWeight.scale(invs);
  energDensity*=invs;

};


void  supplementConstraintAverageExtraDofValue::getConstraintKeys(std::vector<Dof>& key) const {
  for (int i=0; i< _allDof.size(); i++){
		key.push_back(_allDof[i]);
	}
}
void  supplementConstraintAverageExtraDofValue::getMultiplierKeys(std::vector<Dof>& key) const{
  getKeysFromVertex(_multSpace,_tempVertex,getComp(),key);
}
void  supplementConstraintAverageExtraDofValue::getConstraintMatrix(fullMatrix<double>& m) const {
  m = _C;
};		// matrix C
void supplementConstraintAverageExtraDofValue::getDependentMatrix(fullMatrix<double>& m) const{
  m = _Cbc;
}

// for constraint form  u = C u* + xtraDofDiffusionValue*_macroEnergyDensity
void supplementConstraintAverageExtraDofValue::getDependentKeys(std::vector<Dof>& keys) const {
  keys.push_back(_allDof[_positive]);
}; // left real dofs

void supplementConstraintAverageExtraDofValue::getIndependentKeys(std::vector<Dof>& keys) const{
  for (int i=0; i< _allDof.size(); i++){
		 if (i!=_positive)
			keys.push_back(_allDof[i]);
	 }
}


void supplementConstraintAverageExtraDofValue::getLinearConstraints(const fullVector<double>& g,
                   std::map<Dof,DofAffineConstraint<double> >& con) const
{
  DofAffineConstraint<double> cons;
	for (int j=0; j<_allDof.size(); j++){
		if (j!=_positive){
			cons.linear.push_back(std::pair<Dof,double>(_allDof[j],-1.*_C(0,j)));
		}
	};
	cons.shift=g(0);
	con[_allDof[_positive]] = cons;
};

void supplementConstraintAverageExtraDofValue::print() const{
  Msg::Info("supplementConstraintAverageExtraDofValue is created");
};

bool supplementConstraintAverageExtraDofValue::isActive(const MVertex* v) const{
	for(int idom =0; idom < (*_domainVector).size(); idom++){
		partDomain* dom = (*_domainVector)[idom];
		for (elementGroup::vertexContainer::const_iterator itv = dom->vertex_begin(); itv != dom->vertex_end(); itv++){
			const MVertex* vv = itv->second;
			if (v == vv) return true;
		}
	}
	return false;
};

void supplementConstraintAverageExtraDofValue::getKinematicalVector(fullVector<double>& m) const{
  fullVector<double> kinVec;
	_mbc->getKinematicalVector(kinVec);
	m.resize(_ndofs);
	m.setAll(0.);
	_S.mult(kinVec,m);
};
void supplementConstraintAverageExtraDofValue::getKinematicalMatrix(fullMatrix<double>& m) const{
  m = _S;
};
void supplementConstraintAverageExtraDofValue::getLinearConstraints( std::map<Dof,DofAffineConstraint<double> >& con) const{
  fullVector<double> g;
  getKinematicalVector(g);
  getLinearConstraints(g, con);
};

bool supplementConstraintAverageExtraDofValue::update(const IPField* ipf){
  fullMatrix<double> newWeight;
  double newEnergyDensity;
  computeWeightAndMacroEnergyDensity(ipf,newWeight,newEnergyDensity);

  double tol = 1e-6; // constraint will change if tolerance smaller than this value

  if (_macroEnergyDensity < tol) {
    Msg::Error("small Cp = %f very small in supplementConstraintAverageExtraDofValue::update",_macroEnergyDensity);
    return false;
  };



  fullMatrix<double> diffVec(_C);
  diffVec.axpy(newWeight,-1.);

  double refNorm = _C.norm();
  double diffNorm = diffVec.norm();

  double energNorm = fabs(newEnergyDensity-_macroEnergyDensity)/_macroEnergyDensity;


  if ( (energNorm > tol) or (diffNorm/refNorm > tol)){
    Msg::Info("energNorm = %f weightNorm = %f ",energNorm,diffNorm/refNorm );
    Msg::Info("constraint is modified supplementConstraintAverageExtraDofValue");
    _C = newWeight;
    _macroEnergyDensity = newEnergyDensity;

    _S.setAll(0.);
		_S(0,_RowIndex) = _macroEnergyDensity;

    _Cbc.setAll(0.);
		int col = 0;
		for (int i=0; i< _allDof.size(); i++){
			if (i!=_positive){
				_Cbc(0,col) = -1.*_C(0,i);
				col++;
			}
		}

    return true;
  }
  else{
    return false;
  }
};
