//
//
// Description: supplement constraint elements for coupled problems
//
// Author:  <L. Wu>, (C) 2015
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef PBCSUPPLEMENTCONSTRAINTEXTRADOFDIFFUSION_H_
#define PBCSUPPLEMENTCONSTRAINTEXTRADOFDIFFUSION_H_

#include "pbcConstraintElementBase.h"
#include "partDomain.h"
#include "ipField.h"

class supplementConstraintAverageExtraDofValue : public constraintElement{
  protected:
    const std::vector<partDomain*> *_domainVector;
		std::map<Dof,int> _mapDof; // for assemble purpose
		std::vector<Dof> _allDof;
		int _positive;
		FunctionSpaceBase* _multSpace;
		int _extraDofIndex; // extraDof index 
		int _RowIndex; // row position of constrained component in global macrosopic kinematic variable
		MVertex* _tempVertex;
    fullMatrix<double>  _C, _S, _Cbc; // constraint matrix
    double _macroEnergyDensity;
		double _RVEVolume;

  private:
    void computeWeightAndMacroEnergyDensity(const IPField* ipf, fullMatrix<double>& w, double& energ);


  public:
    supplementConstraintAverageExtraDofValue(nonLinearMicroBC* mbc, FunctionSpaceBase* mspace, const int c, const std::vector<partDomain*> *_g);
    virtual ~supplementConstraintAverageExtraDofValue();

    virtual void getConstraintKeys(std::vector<Dof>& key) const ; // real dofs on constraint elements
    virtual void getMultiplierKeys(std::vector<Dof>& key) const ;	// multiplier dof on constraint element
    virtual void getConstraintMatrix(fullMatrix<double>& m) const ;		// matrix C
    virtual void getDependentMatrix(fullMatrix<double>& m) const;
    virtual void getDependentKeys(std::vector<Dof>& keys) const; // left real dofs
    virtual void getIndependentKeys(std::vector<Dof>& keys) const; // for spline interpolation

    virtual void getLinearConstraints(const fullVector<double>& g,
                   std::map<Dof,DofAffineConstraint<double> >& con) const;

    virtual constraintElement::ElementType getType() const{return constraintElement::Supplement;};
    virtual void print() const;
    virtual bool isActive(const MVertex* v) const;

    virtual void getKinematicalVector(fullVector<double>& m) const;
		virtual void getKinematicalMatrix(fullMatrix<double>& m) const;
		virtual void getLinearConstraints(std::map<Dof,DofAffineConstraint<double> >& con) const;
    virtual bool update(const IPField* ipf);
};
#endif // PBCSUPPLEMENTCONSTRAINTEXTRADOFDIFFUSION_H_
