//
//
// Description: class using PETSc for extraction of homogenized tangents
// in case of using polynomial interpolation method to enforce PBC
//
// Author:  <Van Dung NGUYEN>, (C) 2010
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "extractMacroPropertiesInterpolationPETSc.h"
#include "pbcSystems.h"
#include "functionPETSc.h"


#if defined(HAVE_PETSC)

#if (PETSC_VERSION_MAJOR < 3  || (PETSC_VERSION_MAJOR == 3 && PETSC_VERSION_MINOR < 7))
 #define MatCreateVecs(mat, right, left) MatGetVecs(mat, right, left)
#endif

using namespace functionPETSc;

interpolationStiffnessCondensationPETSC::interpolationStiffnessCondensationPETSC(nlsDofManager* p, pbcAlgorithm* ma,
                                        bool sflag, bool tflag, MPI_Comm com):
                                       stiffnessCondensation(),_pAssembler(p),_pAl(ma),_tangentflag(tflag),_stressflag(sflag),
                                       _bConMat(NULL),_cConMat(NULL),_comm(com),
                                       _isInitialized(false){
};
interpolationStiffnessCondensationPETSC::~interpolationStiffnessCondensationPETSC(){
  this->clear();
}

void interpolationStiffnessCondensationPETSC::setSplittedDofSystem(pbcAlgorithm* pal){
  pal->getSplittedDof()->setSplittedDofsSystem(pbcDofManager::INTERNAL,pbcDofManager::DEPENDENT,pbcDofManager::DIRECT);
};

void interpolationStiffnessCondensationPETSC::init(){
  if (!_isInitialized){
    _isInitialized = true;
    pbcDofManager* pbcAssembler = _pAl->getSplittedDof();
    _bConMat = new linearConstraintMatrixPETSc(_comm);
    _pAl->computeIndirectLinearConstraintMatrixInterpolationWithNewVertices(_bConMat);
    _cConMat = new linearConstraintMatrixPETSc(_comm);
    _pAl->computeDirectLinearConstraintMatrix(_cConMat);

    Mat& C = _bConMat->getFirstMatrix();
    Mat& Sb = _bConMat->getKinematicalMatrix();
    Mat& M = _bConMat->getSecondMatrix();
    Mat& Sc = _cConMat->getKinematicalMatrix();

    int nvirt = pbcAssembler->sizeOfVirtualDof();
    if (nvirt > 0){
      _try(MatTranspose(C,MAT_INITIAL_MATRIX,&_CT));
      _try(MatAssemblyBegin(_CT,MAT_FINAL_ASSEMBLY));
      _try(MatAssemblyEnd(_CT,MAT_FINAL_ASSEMBLY));
    }

    _try(MatMatMult(M,Sc,MAT_INITIAL_MATRIX,PETSC_DEFAULT,&_Sbc));
    _try(MatAXPY(_Sbc,1.,Sb,DIFFERENT_NONZERO_PATTERN));
    _try(MatAssemblyBegin(_Sbc,MAT_FINAL_ASSEMBLY));
    _try(MatAssemblyEnd(_Sbc,MAT_FINAL_ASSEMBLY));

    _try(MatTranspose(_Sbc,MAT_INITIAL_MATRIX,&_SbcT));
    _try(MatAssemblyBegin(_SbcT,MAT_FINAL_ASSEMBLY));
    _try(MatAssemblyEnd(_SbcT,MAT_FINAL_ASSEMBLY));

    _try(MatTranspose(Sc,MAT_INITIAL_MATRIX,&_ScT));
    _try(MatAssemblyBegin(_ScT,MAT_FINAL_ASSEMBLY));
    _try(MatAssemblyEnd(_ScT,MAT_FINAL_ASSEMBLY));
  }
};

void interpolationStiffnessCondensationPETSC::clear(){
  if (_isInitialized){
    _isInitialized = false;
    _try(MatDestroy(&_CT));
    _try(MatDestroy(&_Sbc));
    _try(MatDestroy(&_SbcT));
    _try(MatDestroy(&_ScT));
    if (_bConMat) delete _bConMat;
    if (_cConMat) delete _cConMat;
    _bConMat =NULL;
    _cConMat = NULL;
  }
};

void interpolationStiffnessCondensationPETSC::stressSolve(fullVector<double>& stressVec, const double Vrve){
  if (_stressflag){
    int nK = _pAl->getNumberOfMacroToMicroKinematicVariables();
    stressVec.resize(nK);
    stressVec.setAll(0.);

    if (!_isInitialized){
      this->init();
    }
    pbcDofManager* pbcAssembler = _pAl->getSplittedDof();

    Vec fb, fc;
    _try(MatCreateVecs(_SbcT,&fb,PETSC_NULL));
    _try(MatCreateVecs(_ScT,&fc,PETSC_NULL));

    std::string str = "A";
    linearSystem<double>* lsys = _pAssembler->getLinearSystem(str);
    nonLinearSystem<double>* linearsys = dynamic_cast<nonLinearSystem<double>*>(lsys);
    if (linearsys){
      const std::map<Dof,int>& dependentMapDof = pbcAssembler->getDofMap(pbcDofManager::DEPENDENT);
      for (std::map<Dof,int>::const_iterator it = dependentMapDof.begin();it!= dependentMapDof.end(); it++){
        double val = 0.;
        int num = _pAssembler->getDofNumber(it->first);
        if (num != -1)
          linearsys->getFromRightHandSideMinus(num, val);
        else
          Msg::Error("Internal force at this DOFs does not exist, use ofther system MULT_ELIM");
        PetscInt row = it->second;
        _try(VecSetValues(fb,1,&row,&val,INSERT_VALUES));
      }

      _try(VecAssemblyBegin(fb));
      _try(VecAssemblyEnd(fb));

      const std::map<Dof,int>& directMapDof = pbcAssembler->getDofMap(pbcDofManager::DIRECT);
      for (std::map<Dof,int>::const_iterator it = directMapDof.begin(); it!= directMapDof.end(); it++){
        double val = 0.;
        int num = _pAssembler->getDofNumber(it->first);
        if (num != -1)
          linearsys->getFromRightHandSideMinus(num, val);
        else
          Msg::Error("Internal force at this DOFs does not exist, use ofther system MULT_ELIM");
        PetscInt row = it->second;
        _try(VecSetValues(fc,1,&row,&val,INSERT_VALUES));
      }
      _try(VecAssemblyBegin(fc));
      _try(VecAssemblyEnd(fc));

      Vec P;
      _try(MatCreateVecs(_Sbc,&P,PETSC_NULL));
      _try(MatMult(_SbcT,fb,P));
      _try(MatMultAdd(_ScT,fc,P,P));
      _try(VecAssemblyBegin(P));
      _try(VecAssemblyEnd(P));

      PetscScalar* val;
      _try(VecGetArray(P,&val));
      for (int ip=0; ip< nK; ip++){
        stressVec(ip) = val[ip]/Vrve;
      }
      _try(VecDestroy(&fb));
      _try(VecDestroy(&fc));
      _try(VecDestroy(&P));
    }
    else{
      Msg::Error("The pbc nonlinear solver have to used to compute");
    };
  };
}

void interpolationStiffnessCondensationPETSC::tangentCondensationSolve(fullMatrix<double>& tangentMat, const double Vrve){
  if (_tangentflag){
    int nK = _pAl->getNumberOfMacroToMicroKinematicVariables();
    tangentMat.resize(nK,nK);
    tangentMat.setAll(0.);

    if (!_isInitialized){
      this->init();
    }
    pbcDofManager* pbcAssembler = _pAl->getSplittedDof();
    int sizeVirt = pbcAssembler->sizeOfVirtualDof();
    if (sizeVirt == 0)
      Msg::Error("this method cannot be used if  sizeVirt = 0");

    splitStiffnessMatrixPETSc* _stiffness = dynamic_cast<splitStiffnessMatrixPETSc*>(_pAl->getSplitStiffness());
    if (_stiffness == NULL){
      Msg::Error("splitStiffnessMatrixPETSc has to be used");
    }

    Mat& Kii = _stiffness->getIIMat();
    Mat& Kib = _stiffness->getIBMat();
    Mat& Kic = _stiffness->getICMat();

    Mat& Kbi = _stiffness->getBIMat();
    Mat& Kbb = _stiffness->getBBMat();
    Mat& Kbc = _stiffness->getBCMat();

    Mat& Kci = _stiffness->getCIMat();
    Mat& Kcb = _stiffness->getCBMat();
    Mat& Kcc = _stiffness->getCCMat();


    Mat Kbbt, Kbct, Kcbt, Kcct;
    _try(functionPETSc::CondenseMatrixBlocks(Kii,Kib,Kic,Kbi,Kbb,Kbc,Kci,Kcb,Kcc,&Kbbt,&Kbct,&Kcbt,&Kcct));

    Mat& C = _bConMat->getFirstMatrix();
    Mat& Sb = _bConMat->getKinematicalMatrix();
    Mat& M = _bConMat->getSecondMatrix();
    Mat& Sc = _cConMat->getKinematicalMatrix();

    Mat Kbbstar;
    Mat KbbtC;
    _try(MatMatMult(Kbbt,C, MAT_INITIAL_MATRIX,PETSC_DEFAULT,&KbbtC));
    _try(MatMatMult(_CT,KbbtC, MAT_INITIAL_MATRIX,PETSC_DEFAULT,&Kbbstar));
    _try(MatAssemblyBegin(Kbbstar,MAT_FINAL_ASSEMBLY));
    _try(MatAssemblyEnd(Kbbstar,MAT_FINAL_ASSEMBLY));
    _try(MatDestroy(&KbbtC));

    Mat Kbcstar;
    _try(MatMatMult(Kbbt,_Sbc, MAT_INITIAL_MATRIX,PETSC_DEFAULT,&Kbcstar));
    Mat Kbcstar2;
    _try(MatMatMult(Kbct,Sc, MAT_INITIAL_MATRIX,PETSC_DEFAULT,&Kbcstar2));
    _try(MatAXPY(Kbcstar,1.,Kbcstar2,DIFFERENT_NONZERO_PATTERN));
    _try(MatDestroy(&Kbcstar2));


    Mat Kcbstar;
    _try(MatMatMult(_SbcT,Kbbt, MAT_INITIAL_MATRIX,PETSC_DEFAULT,&Kcbstar));
    Mat Kcbstar2;
    _try(MatMatMult(_ScT,Kcbt, MAT_INITIAL_MATRIX,PETSC_DEFAULT,&Kcbstar2));
    _try(MatAXPY(Kcbstar,1.,Kcbstar2,DIFFERENT_NONZERO_PATTERN));
    _try(MatDestroy(&Kcbstar2));

    Mat Sbcstar, Scbstar, Sccstar;
    _try(MatMatMult(_CT,Kbcstar, MAT_INITIAL_MATRIX,PETSC_DEFAULT,&Sbcstar));
    _try(MatMatMult(Kcbstar,C, MAT_INITIAL_MATRIX,PETSC_DEFAULT,&Scbstar));
    _try(MatMatMult(Kcbstar,_Sbc,MAT_INITIAL_MATRIX,PETSC_DEFAULT,&Sccstar));

    Mat SbcTKbct, ScTKcct;
    _try(MatMatMult(_SbcT,Kbct,MAT_INITIAL_MATRIX,PETSC_DEFAULT,&SbcTKbct));
    _try(MatMatMult(_ScT,Kcct,MAT_INITIAL_MATRIX,PETSC_DEFAULT,&ScTKcct));
    _try(MatAXPY(SbcTKbct,1.,ScTKcct,DIFFERENT_NONZERO_PATTERN));
    Mat temp;
    _try(MatMatMult(SbcTKbct,Sc,MAT_INITIAL_MATRIX,PETSC_DEFAULT,&temp));
    _try(MatAXPY(Sccstar,1,temp,DIFFERENT_NONZERO_PATTERN));
    _try(MatDestroy(&temp));
    _try(MatDestroy(&SbcTKbct));
    _try(MatDestroy(&ScTKcct));

    Mat L;
    _try(functionPETSc::CondenseMatrixBlocks(Kbbstar,Sbcstar,Scbstar,Sccstar,&L));

    _try(MatAssemblyBegin(L,MAT_FINAL_ASSEMBLY));
    _try(MatAssemblyEnd(L,MAT_FINAL_ASSEMBLY));

    for (int row = 0; row< nK; ++row){
      for (int col = 0; col< nK; ++col){
        PetscScalar val;
        PetscInt ii = row, jj = col;
        MatGetValues(L,1,&ii,1,&jj,&val);
        tangentMat(row,col) = val/Vrve;
      }
    };

    _try(MatDestroy(&L));
    _try(MatDestroy(&Kbbt));
    _try(MatDestroy(&Kbct));
    _try(MatDestroy(&Kcbt));
    _try(MatDestroy(&Kcct));

    _try(MatDestroy(&Kbbstar));
    _try(MatDestroy(&Kbcstar));
    _try(MatDestroy(&Kcbstar));

    _try(MatDestroy(&Sbcstar));
    _try(MatDestroy(&Scbstar));
    _try(MatDestroy(&Sccstar));

  };
};
#endif // HAVE_PETSC
