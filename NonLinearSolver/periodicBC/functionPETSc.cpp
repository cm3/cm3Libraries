//
//
// Description: PETSC function
// Author:  <Van Dung NGUYEN>, (C) 2010
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "functionPETSc.h"
#include "linearSystemPETSc.hpp"
#include "GmshConfig.h"
#include "OS.h"
#if defined(HAVE_PETSC)

#include "petscpc.h"
#if (PETSC_VERSION_MAJOR < 3  || (PETSC_VERSION_MAJOR == 3 && PETSC_VERSION_MINOR < 7))
 #define MatCreateVecs(mat, right, left) MatGetVecs(mat, right, left)
#endif

void functionPETSc::MatToFile(Mat const & a, const std::string filename){
  FILE* file = fopen(filename.c_str(),"w");
  PetscInt M, N, err;
  PetscBool isAssembled;
  err = MatAssembled(a,&isAssembled); CHKERRABORT(PETSC_COMM_WORLD,err);
  if (!isAssembled){
    err =MatAssemblyBegin(a,MAT_FINAL_ASSEMBLY);CHKERRABORT(PETSC_COMM_WORLD,err);
    err = MatAssemblyEnd(a,MAT_FINAL_ASSEMBLY);CHKERRABORT(PETSC_COMM_WORLD,err);
  }
  err = MatGetSize(a,&M,&N); CHKERRABORT(PETSC_COMM_WORLD,err);
  for (int i=0; i<M; i++){
    for (int j=0; j<N; j++){
      double val;
      err = MatGetValues(a,1,&i,1,&j,&val); CHKERRABORT(PETSC_COMM_WORLD,err);
      fprintf(file,"%.16g ",val);
    };
    fprintf(file,"\n");
  }
  fclose(file);
};

void functionPETSc::VecToFile(Vec const & a,const std::string filename){
  FILE* file = fopen(filename.c_str(),"w");
  PetscInt size, err;
  err = VecGetSize(a,&size);CHKERRABORT(PETSC_COMM_WORLD,err);
  for (int i=0; i<size; i++){
    double val;
    err = VecGetValues(a,1,&i,&val); CHKERRABORT(PETSC_COMM_WORLD,err);
    fprintf(file,"%.16g \n",val);
  }
  fclose(file);
}

/* pertsc function allows estimating the static condenstion of stiffness matrix
// the method based on the MatMatSolve with lu factorization
// for factorization method,
// different method can be used as superlu, supelu-dist, mumpps for sparse matrices
// or petsclu for all matrix types,
// to set one of these method, in .petscrc,
// add -mat_solver_package k, with k is the method identity
// 0 - superlu, 1- superlu-dist, 2-mumps, 3- umfpack petsclu ortherwise
*/

PetscErrorCode functionPETSc::GetFactorMatrix(Mat A11, Mat* F){
  Msg::Info("Begin performing factorize matrix");
 double t = Cpu();
  // convert to aij matrix if necessary
  MatType matType;
  _try(MatGetType(A11,&matType));
  Msg::Info("Factorized mat with %s",matType);

  PetscInt ipack = -1;
  PetscBool isfound = PETSC_FALSE;
  _try(PetscOptionsGetInt(NULL,"-mat_solver_package",&ipack,&isfound));
  if (!isfound){
    Msg::Info("mat_solver_package is not set, use PETSC LU by default");
    ipack = -1;
  }
  switch (ipack) {
    #if defined(PETSC_HAVE_SUPERLU)
    case 0:
      Msg::Info("using SUPERLU LU");
      if (matType != "seqaij"){
        _try(MatConvert(A11,MATSEQAIJ,MAT_INPLACE_MATRIX,&A11));
      }
      _try(MatGetFactor(A11,MATSOLVERSUPERLU,MAT_FACTOR_LU,F));
    break;
    #endif
    #if defined(PETSC_HAVE_SUPERLU_DIST)
    case 1:
      Msg::Info("using SUPERLU_DIST LU");
      if (matType != "seqaij"){
        _try(MatConvert(A11,MATSEQAIJ,MAT_INPLACE_MATRIX,&A11));
      }
      _try(MatGetFactor(A11,MATSOLVERSUPERLU_DIST,MAT_FACTOR_LU,F));
    break;
    #endif
    #if defined(PETSC_HAVE_MUMPS)
    case 2:
      Msg::Info("using MUMPS LU");
      if (matType != "seqaij"){
        _try(MatConvert(A11,MATSEQAIJ,MAT_INPLACE_MATRIX,&A11));
      }
      _try(MatGetFactor(A11,MATSOLVERMUMPS,MAT_FACTOR_LU,F));
      // test mumps options
      PetscInt    icntl;
      PetscReal   cntl;
      icntl = 2;        // sequential matrix ordering
      _try(MatMumpsSetIcntl(*F,7,icntl));
      cntl = 1.e-6; // threshhold for row pivot detection
      _try(MatMumpsSetIcntl(*F,24,1));
      _try(MatMumpsSetCntl(*F,3,cntl));
    break;
    #endif // PETSC_HAVE_MUMPS
    #if defined(PETSC_HAVE_LIBUMFPACK)
    case 3:
      Msg::Info("using UMFPACK LU");
      if (matType != "seqaij"){
        _try(MatConvert(A11,MATSEQAIJ,MAT_INPLACE_MATRIX,&A11));
      }
      _try(MatGetFactor(A11,MATSOLVERUMFPACK,MAT_FACTOR_LU,F));
    break;
    #endif // PETSC_HAVE_LIBUMFPACK
    default:
      Msg::Info("using PETSC LU");
      _try(MatGetFactor(A11,MATSOLVERPETSC,MAT_FACTOR_LU,F));
  }
  MatFactorInfo info;
  _try(MatFactorInfoInitialize(&info));

  info.fill      = 5.0;
  info.shifttype = (PetscReal) MAT_SHIFT_NONE;
  IS perm,  iperm;
  _try(MatGetOrdering(A11,  "natural",  &perm,  &iperm));

  _try(MatLUFactorSymbolic(*F,A11,perm,iperm,&info));
  _try(MatLUFactorNumeric(*F,A11,&info));

  t = Cpu() - t;
  Msg::Info("Done performing factorize matrix (%f s)",t);
  PetscFunctionReturn(0);
};


PetscErrorCode functionPETSc::CondenseMatrixBlocks(Mat A11, Mat A12, Mat A13,
                                     Mat A21, Mat A22, Mat A23,
                                     Mat A31, Mat A32, Mat A33,
                                     Mat* B22, Mat* B23,
                                     Mat* B32, Mat* B33){
  double t = Cpu();
  Msg::Info("Begin performing 3x3 to 2x2 condensation");

  Mat F;
  _try(GetFactorMatrix(A11,&F));

  Mat invA11A12;
  _try(MatDuplicate(A12,MAT_DO_NOT_COPY_VALUES,&invA11A12));
  _try(MatMatSolve(F,A12,invA11A12));
  // estimate B22
  _try(MatMatMult(A21,invA11A12, MAT_INITIAL_MATRIX,PETSC_DECIDE,B22));
  _try(MatScale(*B22,-1.));
	if (A22){
    _try(MatAXPY(*B22,1.,A22,DIFFERENT_NONZERO_PATTERN));
	}
	// estimate B32
  _try(MatMatMult(A31,invA11A12, MAT_INITIAL_MATRIX,PETSC_DECIDE,B32));
  _try(MatScale(*B32,-1.));
	if (A32){
    _try(MatAXPY(*B32,1.,A32,DIFFERENT_NONZERO_PATTERN));
	}
	_try(MatDestroy(&invA11A12));

  Mat invA11A13;
  _try(MatDuplicate(A13,MAT_DO_NOT_COPY_VALUES,&invA11A13));
  _try(MatMatSolve(F,A13,invA11A13));
  _try(MatDestroy(&F));
	// estimate B23
  _try(MatMatMult(A21,invA11A13, MAT_INITIAL_MATRIX,PETSC_DECIDE,B23));
  _try(MatScale(*B23,-1.));
	if (A23){
    _try(MatAXPY(*B23,1.,A23,DIFFERENT_NONZERO_PATTERN));
	}
	// estimate B32
  _try(MatMatMult(A31,invA11A13, MAT_INITIAL_MATRIX,PETSC_DECIDE,B33));
  _try(MatScale(*B33,-1.));
	if (A33){
    _try(MatAXPY(*B33,1.,A33,DIFFERENT_NONZERO_PATTERN));
	}
	//destroy the temp matrix
  _try(MatDestroy(&invA11A13));

  t = Cpu() - t;
  Msg::Info("Done performing 3x3 to 2x2 condensation (%f s)",t);
  PetscFunctionReturn(0);
};

PetscErrorCode functionPETSc::CondenseMatrixBlocks(Mat A11, Mat A12,
                                    Mat A21, Mat A22, Mat* B22){
  Msg::Info("Begin performing 2x2 to 1 condensation");
  double t = Cpu();


  Mat F; // factored matrix
  _try(functionPETSc::GetFactorMatrix(A11,&F));

  //
  Mat invA11A12;
  _try(MatDuplicate(A12,MAT_DO_NOT_COPY_VALUES,&invA11A12));
  _try(MatMatSolve(F,A12,invA11A12));
  _try(MatDestroy(&F));
  // estimate B22
  _try(MatMatMult(A21,invA11A12, MAT_INITIAL_MATRIX,PETSC_DECIDE,B22));
  _try(MatDestroy(&invA11A12));

  _try(MatScale(*B22,-1.));
	if (A22){
    _try(MatAXPY(*B22,1.,A22,DIFFERENT_NONZERO_PATTERN));
	}
  //destroy the temp matrix

  t = Cpu() - t;
  Msg::Info("Done performing 2x2 to 1 condensation (%f s)",t);

  PetscFunctionReturn(0);

};

#ifdef PETSC_HAVE_MUMPS

 #if defined(PETSC_USE_COMPLEX)
  #if defined(PETSC_USE_REAL_SINGLE)
  #include <cmumps_c.h>
  #else
  #include <zmumps_c.h>
  #endif
#else
  #if defined(PETSC_USE_REAL_SINGLE)
  #include <smumps_c.h>
  #else
  #include <dmumps_c.h>
  #endif
  #endif

#if (PETSC_VERSION_RELEASE == 0 || ((PETSC_VERSION_MAJOR == 3) && (PETSC_VERSION_MINOR >= 3) && (PETSC_VERSION_MINOR <= 5))) // petsc-dev
#include "petsc-private/matimpl.h"
#elif (PETSC_VERSION_RELEASE == 0 || ((PETSC_VERSION_MAJOR == 3) && (PETSC_VERSION_MINOR >= 6))) // petsc-dev
#include "petsc/private/matimpl.h"
#else
#include "private/matimpl.h"
#endif

#define ICNTL(I) icntl[(I)-1]
#define CNTL(I) cntl[(I)-1]
#define INFOG(I) infog[(I)-1]
#define INFO(I) info[(I)-1]
#define RINFOG(I) rinfog[(I)-1]
#define RINFO(I) rinfo[(I)-1]

struct MatMUMPS{
  #if defined(PETSC_USE_COMPLEX)
    #if defined(PETSC_USE_REAL_SINGLE)
    CMUMPS_STRUC_C id;
    #else
    ZMUMPS_STRUC_C id;
    #endif
  #else
    #if defined(PETSC_USE_REAL_SINGLE)
    SMUMPS_STRUC_C id;
    #else
    DMUMPS_STRUC_C id;
    #endif
  #endif

/*
  MatStructure   matstruc;
  PetscMPIInt    myid,size;
  PetscInt       *irn,*jcn,nz,sym,nSolve;
  PetscScalar    *val;
  MPI_Comm       comm_mumps;
  VecScatter     scat_rhs, scat_sol;
  PetscBool      isAIJ,CleanUpMUMPS;
  Vec            b_seq,x_seq;
  PetscErrorCode (*Destroy)(Mat);
  PetscErrorCode (*ConvertToTriples)(Mat, int, MatReuse, int*, int**, int**, PetscScalar**);
  */
};
#endif

PetscErrorCode functionPETSc::MatGetDeterminant(Mat A, PetscReal* realpart, PetscReal* imagpart, PetscInt* ex){
  #ifdef PETSC_HAVE_MUMPS
  MatMUMPS* FF = (MatMUMPS*)A->spptr;

  if (FF != NULL){
    *realpart = FF->id.RINFOG(12);
    *imagpart = FF->id.RINFOG(13);
    *ex = FF->id.INFOG(34);
  }
  else
    Msg::Error("Error in petsc matrix conversion");
  #else
  *realpart = 1.;
  *imagpart = 0.;
  *ex = 0;
  #endif
  PetscFunctionReturn(0);
};

// use MatMatSolve to compute schur complement
PetscErrorCode functionPETSc::GetSchurComplementMat(Mat fA11, Mat A12, Mat A21, Mat A22, Mat* A){
  #ifdef _DEBUG
  Msg::Info("Begin computing Schur  complement");
  #endif

  Mat invA11A12;
  _try(MatDuplicate(A12,MAT_DO_NOT_COPY_VALUES,&invA11A12));
  _try(MatMatSolve(fA11,A12,invA11A12));

  _try(MatMatMult(A21,invA11A12, MAT_INITIAL_MATRIX,PETSC_DEFAULT,A));
  _try(MatScale(*A,-1.));

	if (A22){
    _try(MatAXPY(*A,1.,A22,DIFFERENT_NONZERO_PATTERN));
	}

  //destroy the temp matrix
  _try(MatDestroy(&invA11A12));
  #ifdef _DEBUG
  Msg::Info("End computing Schur  complement");
  #endif
	PetscFunctionReturn(0);
};

PetscErrorCode functionPETSc::GetSchurComplementMat_Naive(KSP& ksp, Mat A11, Mat A12, Mat A21, Mat A22, Mat* A){
  MPI_Comm comm = PETSC_COMM_WORLD;
  //Msg::Info("Begin computing Schur  complement");
  PetscInt M21, N21, M12, N12;
  _try(MatGetSize(A12,&M12,&N12));
  _try(MatGetSize(A21,&M21,&N21));

  if (N21 != M12)
    Msg::Error("Error in matrix dimension for stiffness condensation");

  if (A22){
    _try(MatDuplicate(A22,MAT_COPY_VALUES,A));
  }
  else{
    _try(MatCreate(comm,A));
    _try(MatSetSizes(*A,PETSC_DECIDE,PETSC_DECIDE,M21,N12));
    _try(MatSetFromOptions(*A));
  };

  Mat invA11A12;
  _try(MatDuplicate(A12,MAT_DO_NOT_COPY_VALUES,&invA11A12));

  for (int i=0; i<N12; i++){
		 Vec vec, x;
		_try(MatCreateVecs(A11,&vec,PETSC_NULL));
		_try(MatGetColumnVector(A12,vec,i));
		_try(VecAssemblyBegin(vec));
		_try(VecAssemblyEnd(vec));
		_try(VecDuplicate(vec,&x));
		_try(KSPSolve(ksp,vec,x));

		PetscScalar* scalar;
		_try(VecGetArray(x,&scalar));

		PetscScalar norm;
		_try(VecNorm(x,NORM_INFINITY,&norm));
	//	printf(" M= %d, N = %d, nor = %f \n",M,N,norm);

		for (int j=0; j<M12; j++){
			PetscScalar val = scalar[j];
			PetscInt ii = i, jj=j;
			if (fabs(val/norm)>1e-16){
				_try(MatSetValues(invA11A12,1,&jj,1,&ii,&val,INSERT_VALUES));
			};
		};
		_try(VecDestroy(&vec));
		_try(VecDestroy(&x));
	};
  _try(MatAssemblyBegin(invA11A12,MAT_FINAL_ASSEMBLY));
	_try(MatAssemblyEnd(invA11A12,MAT_FINAL_ASSEMBLY));
	Mat A21invA11A12;
	_try(MatDuplicate(*A,MAT_DO_NOT_COPY_VALUES,&A21invA11A12));

	_try(MatMatMult(A21,invA11A12, MAT_INITIAL_MATRIX,PETSC_DEFAULT,&A21invA11A12));
	_try(MatAXPY(*A,-1,A21invA11A12,DIFFERENT_NONZERO_PATTERN));
	//destroy the temp matrix
	_try(MatDestroy(&invA11A12));
  _try(MatDestroy(&A21invA11A12));
  //
  //Msg::Info("End computing Schur  complement");
	PetscFunctionReturn(0);

};

PetscErrorCode functionPETSc::MatInverse_MatMatSolve(Mat A, Mat* B, MPI_Comm comm){
  Msg::Info("Begin inverting matrix MatInverse_MatMatSolve");
  double t = Cpu();
  PetscInt M, N;
  _try(MatGetSize(A,&M,&N));
  if (M != N) Msg::Error("it is not a squared matrix");

  Mat F;
  _try(functionPETSc::GetFactorMatrix(A,&F));

  //  create multiple RHS identity matrix
  Mat I;
  _try(MatDuplicate(A,MAT_DO_NOT_COPY_VALUES,&I));
  _try(MatConvert(I,MATDENSE,MAT_INPLACE_MATRIX,&I));
  _try(MatShift(I,1.));

  Mat invA;
  _try(MatDuplicate(I,MAT_DO_NOT_COPY_VALUES,B));
  _try(MatMatSolve(F,I,*B));
  _try(MatDestroy(&I));
  _try(MatDestroy(&F));
  // create matrix B
  _try(MatConvert(*B,MATAIJ,MAT_INPLACE_MATRIX,B));

  t = Cpu() -t;
  Msg::Info("Done inverting matrix MatInverse_MatMatSolve (%f s)",t);
  PetscFunctionReturn(0);
};



PetscErrorCode functionPETSc::MatInverse_Naive(Mat A, Mat* B, MPI_Comm comm){
  Msg::Info("Begin inverting matrix MatInverse_Naive");
  double t = Cpu();
  PetscInt M,N;
  _try(MatGetSize(A,&M,&N));
  if (M!= N)
    Msg::Error("Can not inverse a rectangular matrix %d %d",M,N);

  _try(MatCreate(comm,B));
  _try(MatSetSizes(*B,PETSC_DECIDE,PETSC_DECIDE,M,N));
  _try(MatSetFromOptions(*B));
  _try(MatSetUp(*B));

  KSP ksp;
  _try(KSPCreate(comm,&ksp));
  _try(KSPSetOperators(ksp,A,A,DIFFERENT_NONZERO_PATTERN));
  PC pc;
  _try(KSPGetPC(ksp,&pc));
  _try(PCSetType(pc,PCLU));
  _try(KSPSetTolerances(ksp, 1.e-8, PETSC_DEFAULT, PETSC_DEFAULT, PETSC_DEFAULT));
  _try(KSPSetFromOptions(ksp));
  _try(PCSetFromOptions(pc));

  for (int i=0; i<N; i++){
    Vec vec, x;
    _try(MatCreateVecs(A,&vec,PETSC_NULL));
    PetscInt col = i;
    PetscScalar s = 1.0;
    _try(VecSetValues(vec,1,&col,&s, INSERT_VALUES));
    _try(VecAssemblyBegin(vec));
    _try(VecAssemblyEnd(vec));
    // KSP solve
    _try(VecDuplicate(vec,&x));
    _try(KSPSolve(ksp,vec,x));

    _try(VecAssemblyBegin(x));
    _try(VecAssemblyEnd(x));

    //PetscScalar norm;
    //_try(VecNorm(x,NORM_INFINITY,&norm));

    PetscScalar* ss;
    _try(VecGetArray(x,&ss));
    for (int j=0; j<M; j++){
      PetscScalar val = ss[j];
      PetscInt ii = i, jj=j;
      if (fabs(val)>0.){
        _try(MatSetValues(*B,1,&jj,1,&ii,&val,INSERT_VALUES));
      };
    };
    _try(VecDestroy(&vec));
    _try(VecDestroy(&x));
  }
  _try(KSPDestroy(&ksp));
  _try(MatAssemblyBegin(*B,MAT_FINAL_ASSEMBLY));
  _try(MatAssemblyEnd(*B,MAT_FINAL_ASSEMBLY));
  t = Cpu() -t;
  Msg::Info("Done inverting matrix MatInverse_Naive (%f s)",t);
  PetscFunctionReturn(0);
};

PetscErrorCode functionPETSc::MatMatSolve_MultipleRHS(Mat A, Mat RHS, Mat* B){
  Msg::Info("Begin performing MatMatSolve_MultipleRHS");
  double t = Cpu();
  PetscInt M, N;
  _try(MatGetSize(A,&M,&N));
  if (M != N) Msg::Error("it is not a squared matrix");

  Mat F;
  _try(functionPETSc::GetFactorMatrix(A,&F));

  //  create multiple RHS dense matrix
  _try(MatDuplicate(RHS,MAT_DO_NOT_COPY_VALUES,B));
  _try(MatMatSolve(F,RHS,*B));
  _try(MatDestroy(&F));
  // create matrix B

  t = Cpu() -t;
  Msg::Info("Done performing MatMatSolve_MultipleRHS (%f s)",t);
  PetscFunctionReturn(0);
};

PetscErrorCode functionPETSc::MatMatSolve_MultipleRHS(Mat A, Mat RHS1, Mat RHS2, Mat* B1, Mat* B2){
  Msg::Info("Begin performing MatMatSolve_MultipleRHS");
  double t = Cpu();
  PetscInt M, N;
  _try(MatGetSize(A,&M,&N));
  if (M != N) Msg::Error("it is not a squared matrix");

  Mat F;
  _try(functionPETSc::GetFactorMatrix(A,&F));

  //  create multiple RHS dense matrix
  _try(MatDuplicate(RHS1,MAT_DO_NOT_COPY_VALUES,B1));
  _try(MatMatSolve(F,RHS1,*B1));

  _try(MatDuplicate(RHS2,MAT_DO_NOT_COPY_VALUES,B2));
  _try(MatMatSolve(F,RHS2,*B2));

  _try(MatDestroy(&F));
  
  t = Cpu() -t;
  Msg::Info("Done performing MatMatSolve_MultipleRHS (%f s)",t);
  PetscFunctionReturn(0);

};


#endif // HAVE_PETSC
