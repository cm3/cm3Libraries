//
//
// Description: nonLinearSystem using PETSC for micro BVPs using constraint elimination method
//
// Author:  <Van Dung NGUYEN>, (C) 2016
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef PBCCONSTRAINTELIMINATIONNONLINEARSYSTEMPETSC_H_
#define PBCCONSTRAINTELIMINATIONNONLINEARSYSTEMPETSC_H_

#include "pbcSystems.h"
#include "linearConstraintMatrixPETSc.h"
#include "nonLinearSystems.h"

template<class scalar>
class pbcConstraintEliminationNonLinearSystemPETSc : public pbcSystemConden<scalar>,
                                                    public nonLinearSystemPETSc<scalar>{
#if defined(HAVE_PETSC)
  protected:
    pbcAlgorithm* _pAl;
    bool _isSolvedPass;
    Mat _K; // structural stiffness
    Mat _T, _S;
    bool _isComputedProjectMatrices;

    Vec _xConden, _bConden;

    Vec _kinematicVariableInitial;
    Vec _kinematicVariable;
    Vec _kinematicVariablePrev;
    Vec _kinematicVariableTemp;

    // stress matrix
    Mat _StressMatrix;
    Mat _BodyForceMatrix;
    int _dimStress;
    bool _isModifiedStressMatrix;
    bool _isAllocatedStressMatrix;
    bool _isModifiedBodyForceMatrix;
    bool _isAllocatedBodyForceMatrix;

  protected:
    virtual void createKSPSolver(){
      computeConstraintProjectMatrices();
      // create KSP
      if (!this->_kspAllocated)
          this->_kspCreate();
      // assemble stiffness matrix
      this->_assembleMatrixIfNeeded();
      if (this-> _matrixChangedSinceLastSolve and _isComputedProjectMatrices){
        //Msg::Info("recomputing stiffness matrix Pt*a*P");
        /**compute new stiffness matrix **/
        if ( !_isSolvedPass){
          //_try(MatDestroy(&this->_K));
          _try(MatPtAP(this->_a,_T,MAT_INITIAL_MATRIX,1.,&this->_K));
        }
        else{
          _try(MatPtAP(this->_a,_T,MAT_REUSE_MATRIX,1.,&this->_K));
        }

        _try(MatAssemblyBegin(this->_K,MAT_FINAL_ASSEMBLY));
        _try(MatAssemblyEnd(this->_K,MAT_FINAL_ASSEMBLY));
        _try(KSPSetOperators(this->_ksp, this->_K, this->_K, DIFFERENT_NONZERO_PATTERN));
        this-> _matrixChangedSinceLastSolve = false;
        _isSolvedPass = true;
      }
      else{
        _try(KSPSetOperators(this->_ksp, this->_K, this->_K, SAME_PRECONDITIONER));
      }
    };

  public:
    pbcConstraintEliminationNonLinearSystemPETSc(MPI_Comm com = PETSC_COMM_SELF) : _pAl(NULL), nonLinearSystemPETSc<scalar>(com),
    pbcSystemConden<scalar>(), _dimStress(0), _isComputedProjectMatrices(false),_isSolvedPass(false),
    _isModifiedStressMatrix(false),_isAllocatedStressMatrix(false),_isModifiedBodyForceMatrix(false),_isAllocatedBodyForceMatrix(false)
    {}
    
    virtual void restart()
    {
      nonLinearSystemPETSc<scalar>::restart();
      int nKin = _pAl->getNumberOfMacroToMicroKinematicVariables();
      nonLinearSystemPETSc<scalar>::restart_petsc_array(_kinematicVariableInitial,nKin);
      nonLinearSystemPETSc<scalar>::restart_petsc_array(_kinematicVariable,nKin);
      nonLinearSystemPETSc<scalar>::restart_petsc_array(_kinematicVariablePrev,nKin);
    }
    
    virtual ~pbcConstraintEliminationNonLinearSystemPETSc() {
      this->clear();
      clearStressMatrix();
      clearBodyForceMatrix();
    }

    virtual void setPBCAlgorithm(pbcAlgorithm* p) {
      _pAl = p;
    }

    Vec& getKinematicState(const IPStateBase::whichState ws) {
      if (ws == IPStateBase::initial) return _kinematicVariableInitial;
      else if (ws == IPStateBase::previous) return _kinematicVariablePrev;
      else if (ws == IPStateBase::current) return _kinematicVariable;
      else if (ws == IPStateBase::temp) return _kinematicVariableTemp;
      else{
        Msg::Error("This state is not defined");
      }
      static Vec a;
      return a;
    }

    virtual void copy(const IPStateBase::whichState source,
                        const IPStateBase::whichState destination){
      nonLinearSystemPETSc<scalar>::copy(source,destination);
      Vec& src = this->getKinematicState(source);
      Vec& dst = this->getKinematicState(destination);
     	_try(VecAssemblyBegin(src));
    	_try(VecAssemblyEnd(src));

    	_try(VecAssemblyBegin(dst));
    	_try(VecAssemblyEnd(dst));


			 _try(VecCopy(src,dst));
    }

    virtual void allocate(int nbRows){
      nonLinearSystemPETSc<scalar>::allocate(nbRows);
      if (_pAl == NULL) Msg::Error("pAl has not been initialized in pbcConstraintEliminationNonLinearSystemPETSc::allocate");
      int condenSystemRow = _pAl->getSplittedDof()->sizeOfInternalDof()+ _pAl->getSplittedDof()->sizeOfIndependentDof();
      _try(VecCreate(this->getComm(), &_xConden));
      _try(VecSetSizes(_xConden, condenSystemRow, PETSC_DETERMINE));
      _try(VecSetFromOptions(_xConden));
      _try(VecDuplicate(_xConden, &_bConden));
      
      _try(VecDestroy(&this->_lastResidual));
      _try(VecDuplicate(_xConden, &this->_lastResidual));

      int nKin = _pAl->getNumberOfMacroToMicroKinematicVariables();
      _try(VecCreate(this->getComm(), &_kinematicVariable));
      _try(VecSetSizes(_kinematicVariable, nKin, PETSC_DETERMINE));
      _try(VecSetFromOptions(_kinematicVariable));

      _try(VecDuplicate(_kinematicVariable, &_kinematicVariablePrev));
      _try(VecDuplicate(_kinematicVariable, &_kinematicVariableInitial));
      _try(VecDuplicate(_kinematicVariable, &_kinematicVariableTemp));
    
    }

    virtual void clear(){
      if (this->isAllocated()){
        nonLinearSystemPETSc<scalar>::clear();
        _try(VecDestroy(&_xConden));
        _try(VecDestroy(&_bConden));
        _try(VecDestroy(&_kinematicVariable));
        _try(VecDestroy(&_kinematicVariableInitial));
        _try(VecDestroy(&_kinematicVariablePrev));
        _try(VecDestroy(&_kinematicVariableTemp));

        if (_isSolvedPass){
          _try(MatDestroy(&_K));
        }

        if (_isComputedProjectMatrices){
          _try(MatDestroy(&_T));
          _try(MatDestroy(&_S));
          _isComputedProjectMatrices = false;
        }
      }

    };

    virtual void resetUnknownsToPreviousTimeStep(){
      nonLinearSystemPETSc<scalar>::resetUnknownsToPreviousTimeStep();
      _try(VecCopy(_kinematicVariablePrev,_kinematicVariable));
    }

    virtual void nextStep(){
      nonLinearSystemPETSc<scalar>::nextStep();
      _try(VecAssemblyBegin(_kinematicVariable));
      _try(VecAssemblyEnd(_kinematicVariable));
		  _try(VecAssemblyBegin(_kinematicVariablePrev));
      _try(VecAssemblyEnd(_kinematicVariablePrev));

     _try(VecCopy(_kinematicVariable,_kinematicVariablePrev));

		}

    virtual void zeroRightHandSide(){
      nonLinearSystemPETSc<scalar>::zeroRightHandSide();
      _try(VecAssemblyBegin(_bConden));
      _try(VecAssemblyEnd(_bConden));
      _try(VecZeroEntries(_bConden));
     }

    virtual bool isAllocatedStressMatrix() const {return _isAllocatedStressMatrix;};
		virtual void clearStressMatrix() {
      if (_isAllocatedStressMatrix){
        _try(MatDestroy(&_StressMatrix));
        _isAllocatedStressMatrix = false;
        _isModifiedStressMatrix = false;
      }
		};
		virtual void allocateStressMatrix(int stressDim, int nbR) {
      _isAllocatedStressMatrix = true;
      _isModifiedStressMatrix = false;
      _dimStress = stressDim;

      _try(MatCreate(this->getComm(),&_StressMatrix));
      _try(MatSetSizes(_StressMatrix,_dimStress,nbR,PETSC_DETERMINE,PETSC_DETERMINE));
      _try(MatSetType(_StressMatrix,MATSEQAIJ));
      _try(MatSetFromOptions(_StressMatrix));
      #if (((PETSC_VERSION_MAJOR == 3) && (PETSC_VERSION_MINOR >= 3)))
      _try(MatSetUp(this->_StressMatrix));
      #endif // (PETSC_VERSION_MAJOR == 3) && (PETSC_VERSION_MINOR >= 3)
    };
    virtual void addToStressMatrix(int row, int col, const scalar& val) {
      PetscInt ii = row, jj = col;
      PetscScalar s = val;
      _try(MatSetValues(_StressMatrix,1,&ii,1,&jj,&s,ADD_VALUES));
      _isModifiedStressMatrix = true;
    };
    virtual void zeroStressMatrix() {
      if (_isAllocatedStressMatrix){
        _try(MatAssemblyBegin(_StressMatrix,MAT_FINAL_ASSEMBLY));
        _try(MatAssemblyEnd(_StressMatrix,MAT_FINAL_ASSEMBLY));
        _try(MatZeroEntries(_StressMatrix));
        _isModifiedStressMatrix = false;
      }
    };

      virtual bool isAllocatedBodyForceMatrix() const {return _isAllocatedBodyForceMatrix;};
      virtual void clearBodyForceMatrix() {
      if (_isAllocatedBodyForceMatrix){
        _try(MatDestroy(&_BodyForceMatrix));
        _isAllocatedBodyForceMatrix = false;
        _isModifiedBodyForceMatrix = false;
      }
      };
      virtual void allocateBodyForceMatrix(int nbR) {
        _isAllocatedBodyForceMatrix = true;
        _isModifiedBodyForceMatrix = false;
        int _dimKin = _pAl->getNumberOfMacroToMicroKinematicVariables();
        _try(MatCreate(this->getComm(),&_BodyForceMatrix));
        _try(MatSetSizes(_BodyForceMatrix,nbR,_dimKin,PETSC_DETERMINE,PETSC_DETERMINE));
        _try(MatSetType(_BodyForceMatrix,MATSEQAIJ));
        _try(MatSetFromOptions(_BodyForceMatrix));
      #if (((PETSC_VERSION_MAJOR == 3) && (PETSC_VERSION_MINOR >= 3)))
       _try(MatSetUp(this->_BodyForceMatrix));
      #endif // (PETSC_VERSION_MAJOR == 3) && (PETSC_VERSION_MINOR >= 3)
    };
    virtual void addToBodyForceMatrix(int row, int col, const scalar& val) {
      PetscInt ii = row, jj = col;
      PetscScalar s = val;
      _try(MatSetValues(_BodyForceMatrix,1,&ii,1,&jj,&s,ADD_VALUES));
      _isModifiedBodyForceMatrix = true;
    };
    virtual void zeroBodyForceMatrix(){
      if (_isAllocatedBodyForceMatrix){
        _try(MatAssemblyBegin(_BodyForceMatrix,MAT_FINAL_ASSEMBLY));
        _try(MatAssemblyEnd(_BodyForceMatrix,MAT_FINAL_ASSEMBLY));
        _try(MatZeroEntries(_BodyForceMatrix));
        _isModifiedBodyForceMatrix = false;
      }
    };

		virtual void computeConstraintProjectMatrices(){
      // this function work only when constraint matrix is modified through addToConstraintMatrix
      if (_pAl == NULL) Msg::Error("pAl has not been initialized in pbcConstraintEliminationNonLinearSystemPETSc::computeConstraintProjectMatrices");
      if (!_pAl->isSplittedDof()) _pAl->splitDofs();
      if (_pAl->isModifiedConstraintMatrix()){
        // if constraint matrix is modifed
        _pAl->setIsModifiedConstraintMatrixFlag(false);
        // set false for this falg as modifs already took into account
        double t = Cpu();

        if (_isComputedProjectMatrices){
          _try(MatDestroy(&_T));
          _try(MatDestroy(&_S));
          Msg::Info("Begin recomputing constraint project matrix T");
        }
        else{
          Msg::Info("Begin computing constraint project matrix T");
        }
        // compute T, S
        linearConstraintMatrixPETSc cConMat(this->getComm());
        linearConstraintMatrixPETSc bConMat(this->getComm());
        _pAl->computeDirectLinearConstraintMatrix(&cConMat);
        _pAl->computeIndirectLinearConstraintMatrixForPBCSystem(&bConMat);


        Mat& Cd = bConMat.getFirstMatrix();
        Mat& Cf = bConMat.getSecondMatrix();
        Mat& Cc = bConMat.getThirdMatrix();
        Mat& Sb = bConMat.getKinematicalMatrix();
        Mat& Sc = cConMat.getKinematicalMatrix();

        Mat Cdf, Sdf; // these Mat exists when depenDofMap is not empty

        if (_pAl->getSplittedDof()->sizeOfDependentDof() > 0){
          Mat Sbc;
          if (_pAl->getSplittedDof()->sizeOfDirectDof() > 0){
            _try(MatMatMult(Cc,Sc,MAT_INITIAL_MATRIX,PETSC_DEFAULT,&Sbc));
            _try(MatScale(Sbc,-1.));
            _try(MatAXPY(Sbc,1.,Sb,DIFFERENT_NONZERO_PATTERN));

          }
          else{
            _try(MatDuplicate(Sb,MAT_COPY_VALUES,&Sbc));
          }
          _try(MatAssemblyBegin(Sbc,MAT_FINAL_ASSEMBLY));
          _try(MatAssemblyEnd(Sbc,MAT_FINAL_ASSEMBLY));

          // from matrix Cd u_d + C_f*u_f = Sbc*FM becomes u_d = Cdf u_f + Sdf*FM
          
          PetscBool useMatMatSolveToInvertMatrix = PETSC_TRUE;
          _try(PetscOptionsGetBool(PETSC_NULL, "", "-useMatMatSolveToInverseMatrix",
                             &useMatMatSolveToInvertMatrix, PETSC_NULL));  
          
          if (useMatMatSolveToInvertMatrix){
            _try(functionPETSc::MatMatSolve_MultipleRHS(Cd,Cf,Sbc,&Cdf,&Sdf));
            _try(MatScale(Cdf,-1.));
          }
          else{
            Mat invCd;
            _try(functionPETSc::MatInverse_Naive(Cd,&invCd,this->getComm()));

						if (_pAl->getSplittedDof()->sizeOfIndependentDof()>0){
							_try(MatMatMult(invCd,Cf,MAT_INITIAL_MATRIX,PETSC_DECIDE,&Cdf));
							_try(MatScale(Cdf,-1.));
						};
            _try(MatMatMult(invCd,Sbc,MAT_INITIAL_MATRIX,PETSC_DECIDE,&Sdf));
            _try(MatDestroy(&invCd));
          }

          _try(MatDestroy(&Sbc));
          bConMat.clear();
        }

        double tT = Cpu();
        // all Dof groups
        const std::map<Dof,int>& internalDofMap = _pAl->getSplittedDof()->getDofMap(pbcDofManager::INTERNAL);
        const std::map<Dof,int>& directDofMap = _pAl->getSplittedDof()->getDofMap(pbcDofManager::DIRECT);
        const std::map<Dof,int>& indirectDofMap = _pAl->getSplittedDof()->getDofMap(pbcDofManager::INDIRECT);
        const std::map<Dof,int>& depenDofMap  = _pAl->getSplittedDof()->getDofMap(pbcDofManager::DEPENDENT);
        const std::map<Dof,int>& indepenDofMap  = _pAl->getSplittedDof()->getDofMap(pbcDofManager::INDEPENDENT);

        int Trow =this->_nbRows;
        int Tcol = _pAl->getSplittedDof()->sizeOfInternalDof() + _pAl->getSplittedDof()->sizeOfIndependentDof();

        _try(MatCreate(this->getComm(),&_T));
        _try(MatSetSizes(_T,Trow,Tcol,PETSC_DETERMINE,PETSC_DETERMINE));
        _try(MatSetType(_T,MATSEQAIJ));
        _try(MatSetFromOptions(_T));
				// preallocate T
        std::vector<int> nByRowDiag (Trow);
        for (std::map<Dof,int>::const_iterator it =internalDofMap.begin(); it != internalDofMap.end(); it++){
          int row = _pAl->getSystemDofNumber(it->first);
          nByRowDiag[row] = 1;
        };

        for (std::map<Dof,int>::const_iterator it =indepenDofMap.begin(); it != indepenDofMap.end(); it++){
          int row = _pAl->getSystemDofNumber(it->first);
          nByRowDiag[row] = 1;
        };

        // depend-independ
        for (std::map<Dof,int>::const_iterator itDepend = depenDofMap.begin(); itDepend != depenDofMap.end(); itDepend++){
          int row = _pAl->getSystemDofNumber(itDepend->first);
          nByRowDiag[row] = indepenDofMap.size();
        }
				_try(MatSeqAIJSetPreallocation(_T, 0,  &nByRowDiag[0]));

        // internal-internal
        for (std::map<Dof,int>::const_iterator it =internalDofMap.begin(); it != internalDofMap.end(); it++){
          PetscInt row = _pAl->getSystemDofNumber(it->first);
          PetscInt col = it->second;
          PetscScalar val =1.;
          _try(MatSetValue(_T,row,col,val,INSERT_VALUES));
        };

        // independ-indepen
        for (std::map<Dof,int>::const_iterator it =indepenDofMap.begin(); it != indepenDofMap.end(); it++){
          PetscInt col = it->second+internalDofMap.size();
          PetscInt row = _pAl->getSystemDofNumber(it->first);
          PetscScalar val =1.;
          _try(MatSetValue(_T,row,col,val,INSERT_VALUES));

        };

        for (std::map<Dof,int>::const_iterator itDepend = depenDofMap.begin(); itDepend != depenDofMap.end(); itDepend++){
          PetscInt row = _pAl->getSystemDofNumber(itDepend->first);
          for (std::map<Dof,int>::const_iterator itIndepend =indepenDofMap.begin(); itIndepend != indepenDofMap.end(); itIndepend++){
            PetscInt col = itIndepend->second+internalDofMap.size();
            PetscScalar val;
            _try(MatGetValue(Cdf,itDepend->second,itIndepend->second,&val));
            if (fabs(val) > 1e-6){
              _try(MatSetValue(_T,row,col,val,INSERT_VALUES));
            }
          }
        }

        _try(MatAssemblyBegin(_T,MAT_FINAL_ASSEMBLY));
        _try(MatAssemblyEnd(_T,MAT_FINAL_ASSEMBLY));


        double tS = Cpu();
        PetscInt Srow = Trow;
        PetscInt Scol = _pAl->getNumberOfMacroToMicroKinematicVariables();
        _try(MatCreate(this->getComm(),&_S));
        _try(MatSetSizes(_S,Srow,Scol,PETSC_DETERMINE,PETSC_DETERMINE));
        _try(MatSetType(_S,MATSEQAIJ));
        _try(MatSetFromOptions(_S));

        nByRowDiag.clear();
        nByRowDiag.resize(Srow,0);
        for (std::map<Dof,int>::const_iterator itDepend = depenDofMap.begin(); itDepend != depenDofMap.end(); itDepend++){
          PetscInt row = _pAl->getSystemDofNumber(itDepend->first);
          nByRowDiag[row] = Scol;
        }
        for (std::map<Dof,int>::const_iterator itDirect= directDofMap.begin(); itDirect != directDofMap.end(); itDirect++){
          PetscInt row = _pAl->getSystemDofNumber(itDirect->first);
          nByRowDiag[row] = Scol;
        };

        _try(MatSeqAIJSetPreallocation(_S, 0, &nByRowDiag[0]));

        for (std::map<Dof,int>::const_iterator itDepend = depenDofMap.begin(); itDepend != depenDofMap.end(); itDepend++){
          PetscInt row = _pAl->getSystemDofNumber(itDepend->first);
          PetscInt localRow = itDepend->second;
          for (PetscInt col = 0; col < Scol; col++){
            PetscScalar val;
            _try(MatGetValue(Sdf,localRow,col,&val));
            if (fabs(val) > 1e-16){
              _try(MatSetValue(_S,row,col,val,INSERT_VALUES));
            }
          }
        }
        for (std::map<Dof,int>::const_iterator itDirect= directDofMap.begin(); itDirect != directDofMap.end(); itDirect++){
          PetscInt row = _pAl->getSystemDofNumber(itDirect->first);
          PetscInt localRow = itDirect->second;
          for (PetscInt col = 0; col < Scol; col++){
            PetscScalar val;
            _try(MatGetValue(Sc,localRow,col,&val));
            if (fabs(val) > 1e-16){
              _try(MatSetValue(_S,row,col,val,INSERT_VALUES));
            }

          };
        };
        _try(MatAssemblyBegin(_S,MAT_FINAL_ASSEMBLY));
        _try(MatAssemblyEnd(_S,MAT_FINAL_ASSEMBLY));
        _isComputedProjectMatrices = true;

        if (depenDofMap.size() >0){
	    if (_pAl->getSplittedDof()->sizeOfIndependentDof()>0){
		_try(MatDestroy(&Cdf));
	    }
          _try(MatDestroy(&Sdf));
        }
        cConMat.clear();
        t = Cpu() - t;
        Msg::Info("End computing constraint project matrix T (%f s)",t);
      };
		};

    virtual void zeroKinematicVector() {
      _try(VecAssemblyBegin(_kinematicVariable));
      _try(VecAssemblyEnd(_kinematicVariable));
      _try(VecZeroEntries(_kinematicVariable));
    };

    virtual void initialKinematicVector(int row, const scalar& val) {
			PetscInt i = row;
      PetscScalar s = val;
      _try(VecSetValue(_kinematicVariableInitial,i,s,INSERT_VALUES));
      _try(VecSetValue(_kinematicVariable,i,s,INSERT_VALUES));
			_try(VecSetValue(_kinematicVariablePrev,i,s,INSERT_VALUES));
		};

    virtual void addToKinematicVector(int row, const scalar& val) {
      PetscInt i = row;
      PetscScalar s = val;
      _try(VecSetValue(_kinematicVariable,i,s,INSERT_VALUES));
    };

		virtual void updateSystemUnknown(){
		  computeConstraintProjectMatrices();
      _try(VecAssemblyBegin(_kinematicVariable));
      _try(VecAssemblyEnd(_kinematicVariable));
      Vec Dkinematic;
      _try(VecDuplicate(_kinematicVariable,&Dkinematic));
      _try(VecWAXPY(Dkinematic,-1.,_kinematicVariablePrev,_kinematicVariable));
      _try(MatMultAdd(_S,Dkinematic,this->_xsol,this->_xsol));
      _try(VecDestroy(&Dkinematic));
		};

    virtual int systemSolve(){
      createKSPSolver();
      /** compute new right hand side**/
			if (!this->_flagb){
        _try(VecCopy(this->_Fext,this->_b));
        _try(VecAXPY(this->_b,-1.,this->_Fint));
        _try(MatMultTranspose(_T,this->_b,this->_bConden));
        this->_flagb = true;
			}
			
			// implementation based on the following work
			// Matthies, Hermann, and Gilbert Strang. 
			// "The solution of nonlinear finite element equations." 
			// International journal for numerical methods in engineering 14.11 (1979): 1613-1626.
			//
			if (this->_withBFGS)
      {
        Msg::Info("BFGS iteration = %d BFGSDeltas.size=%d BFGSGammas.size = %d",
                this->_BFGSCounter, this->_BFGSDeltas.size(), this->_BFGSGammas.size());
        if (this->_BFGSCounter == 0)
        {
          // solve system
          _try(KSPSolve(this->_ksp, this->_bConden, this->_xConden));
          if (this->_BFGSDeltas.find(this->_BFGSCounter+1) == this->_BFGSDeltas.end())
          {
            Vec& v = this->_BFGSDeltas[this->_BFGSCounter+1];
            _try(VecDuplicate(this->_xConden, &v));
          }
          _try(VecCopy(this->_xConden, this->_BFGSDeltas[this->_BFGSCounter+1]));
          _try(VecCopy(this->_bConden, this->_lastResidual));
          _try(VecScale(this->_lastResidual, -1.));
        }
        else
        {
          double val;
          Vec q;
          _try(VecDuplicate(this->_bConden, &q));
          _try(VecCopy(this->_bConden, q));
          _try(VecScale(q, -1));
          
          if (this->_BFGSGammas.find(this->_BFGSCounter) == this->_BFGSGammas.end())
          {
            Vec& v = this->_BFGSGammas[this->_BFGSCounter];
            _try(VecDuplicate(this->_xConden, &v));
          }
          _try(VecAXPBYPCZ(this->_BFGSGammas[this->_BFGSCounter], 1., -1., 0., q, this->_lastResidual));
          _try(VecDot(this->_BFGSDeltas[this->_BFGSCounter], this->_BFGSGammas[this->_BFGSCounter], &val));
          this->_BFGSRhos[this->_BFGSCounter] = 1./val;
          
          _try(VecCopy(q, this->_lastResidual));        
          //
          std::vector<double> alphas(this->_BFGSCounter,0.);
          for (int j= this->_BFGSCounter-1; j>-1; j--)
          {
            _try(VecDot(this->_BFGSDeltas[j+1], q, &val));
            alphas[j] = this->_BFGSRhos[j+1]*val;
            _try(VecAXPY(q, -alphas[j], this->_BFGSGammas[j+1]));
          }
          _try(VecCopy(q, this->_bConden));
          _try(KSPSolve(this->_ksp, this->_bConden, this->_xConden));
          for (int j=1; j< this->_BFGSCounter+1; j++)
          {
            _try(VecDot(this->_BFGSGammas[j], this->_xConden, &val));
            _try(VecAXPY(this->_xConden, alphas[j-1]-this->_BFGSRhos[j]*val, this->_BFGSDeltas[j]));
          }
           _try(VecDestroy(&q));
           // update solution
          _try(VecScale(this->_xConden, -1.));
          if (this->_BFGSDeltas.find(this->_BFGSCounter+1) == this->_BFGSDeltas.end())
          {
            Vec& v = this->_BFGSDeltas[this->_BFGSCounter+1];
            _try(VecDuplicate(this->_xConden, &v));
          }
          _try(VecCopy(this->_xConden, this->_BFGSDeltas[this->_BFGSCounter+1]));
        }
        this->_BFGSCounter += 1;
      }
      else
      {
        //double t= Cpu();
			  //Msg::Info("Begin solving pbc system");
        _try(KSPSolve(this->_ksp, this->_bConden, this->_xConden));
       //t = Cpu() - t;
       //Msg::Info("Done solving pbc system (%f s)",t);
      }
     _try(MatMult(_T,_xConden,this->_x));
     _try(VecAXPY(this->_xsol,1.,this->_x));
			return 1;
		};

		virtual double normInfRightHandSide() const{
      /** compute new right hand side**/
      if (!_isComputedProjectMatrices){
        Msg::Error("Matrix T, TT, S has not been computed pbcConstraintEliminationNonLinearSystemPETSc::normInfRightHandSide");
      }
			if (!this->_flagb){
        _try(VecCopy(this->_Fext,this->_b));
        _try(VecAXPY(this->_b,-1.,this->_Fint));
        _try(MatMultTranspose(_T,this->_b,this->_bConden));
        this->_flagb = true;
			}
			PetscReal nor;
      VecAssemblyBegin(_bConden);
      VecAssemblyEnd(_bConden);
      _try(VecNorm(_bConden, NORM_INFINITY, &nor));
      return nor;

		};

  virtual int condensationSolve(fullMatrix<double>& L, const double rvevolume, const bool needdUdF,const bool needdUdG, nlsDofManager* pAss,  
             std::map<Dof, STensor3 >* dUdF, std::map<Dof, STensor33 >* dUdG) {
      int _dimKin = _pAl->getNumberOfMacroToMicroKinematicVariables();
      L.resize(_dimStress,_dimKin);
      L.setAll(0.);
      if(needdUdF){
         dUdF->clear();
         if(needdUdG)
            dUdG->clear();
         if (_isModifiedBodyForceMatrix){
          _try(MatAssemblyBegin(_BodyForceMatrix,MAT_FINAL_ASSEMBLY));
          _try(MatAssemblyEnd(_BodyForceMatrix,MAT_FINAL_ASSEMBLY));
          _isModifiedBodyForceMatrix = false;
        }
      }  
      createKSPSolver();

      if (_isModifiedStressMatrix){
        _try(MatAssemblyBegin(_StressMatrix,MAT_FINAL_ASSEMBLY));
        _try(MatAssemblyEnd(_StressMatrix,MAT_FINAL_ASSEMBLY));
        _isModifiedStressMatrix = false;
      }
        

      Mat Lmat;
      _try(MatMatMult(_StressMatrix,_S,MAT_INITIAL_MATRIX,PETSC_DECIDE,&Lmat));
      PetscInt M, N;
      MatGetSize(Lmat,&M,&N);
      for (PetscInt m = 0; m< M; m++){
        for (PetscInt n=0; n< N; n++){
          PetscScalar val;
          MatGetValue(Lmat,m,n,&val);
          L(m,n) += val/rvevolume;
        }
      }

      // compute kinematic kinMatRHS
      // MatRHS = (-TT*K*S)
      Mat TTK;
      _try(MatTransposeMatMult(_T,this->_a,MAT_INITIAL_MATRIX,PETSC_DECIDE,&TTK));
      Mat kinMatRHS;
      _try(MatMatMult(TTK,_S,MAT_INITIAL_MATRIX,PETSC_DECIDE,&kinMatRHS));
      if(needdUdF){
        Mat TTBmdGMat;
        _try(MatTransposeMatMult(_T,_BodyForceMatrix,MAT_INITIAL_MATRIX,PETSC_DECIDE,&TTBmdGMat));
        _try(MatAXPY(kinMatRHS,-1.,TTBmdGMat,DIFFERENT_NONZERO_PATTERN));
        _try(MatDestroy(&TTBmdGMat));
      }
      // solve for each column of _KinematicMatrix
      Vec StressMatX;
      _try(VecCreate(this->getComm(), &StressMatX));
      _try(VecSetSizes(StressMatX, _dimStress, PETSC_DETERMINE));
      _try(VecSetFromOptions(StressMatX));
      Vec x, Tx;
      _try(VecDuplicate(_xConden,&x));
      _try(VecDuplicate(this->_xsol,&Tx));

      Vec kinematicRHS;
        _try(VecDuplicate(_xConden,&kinematicRHS));

      for (int i=0; i< _dimKin; i++){
        PetscInt col = i;
        _try(MatGetColumnVector(kinMatRHS,kinematicRHS,col));

        PetscReal NormRHS;
        _try(VecNorm(kinematicRHS,NORM_INFINITY,&NormRHS));

        if (NormRHS > 0.){
          // solve if norm greater than 0
          _try(KSPSolve(this->_ksp, kinematicRHS, x));
          _try(MatMult(_T,x,Tx));
          _try(MatMult(_StressMatrix,Tx,StressMatX));
          PetscScalar* val;
          _try(VecGetArray(StressMatX,&val));
          for (int c=0; c<_dimStress; c++){
            L(c,i) -= val[c]/rvevolume;
          }
        }
        if(needdUdF){
           if(i<9){ 
             int k, l; 
             PetscScalar Sval, Txval;
             Tensor23::getIntsFromIndex(i,k,l); // convert vector to matrix
             const std::map<Dof, int>& unknown = pAss->getUnknownMap();

             for (std::map<Dof, int>::const_iterator it= unknown.begin(); it != unknown.end(); it++) {
               STensor3& dUcompdF = dUdF->operator[](it->first);
               PetscInt idof = it->second;
               MatGetValue(_S,idof,col,&Sval);
               PetscInt ix[]={idof};
               VecGetValues(Tx,1,ix,&Txval);
               dUcompdF(k,l) = Sval-Txval;                             
             }
           }          
        }
        if(needdUdG){
           if(i>=9 and i<36){ 
             int k, l, s; 
             PetscScalar Sval, Txval;
             Tensor33::getIntsFromIndex(i,k,l,s); // convert vector to matrix
             const std::map<Dof, int>& unknown = pAss->getUnknownMap();

             for (std::map<Dof, int>::const_iterator it= unknown.begin(); it != unknown.end(); it++) {
               STensor33& dUcompdG = dUdG->operator[](it->first);
               PetscInt idof = it->second;
               MatGetValue(_S,idof,col,&Sval);
               PetscInt ix[]={idof};
               VecGetValues(Tx,1,ix,&Txval);
               dUcompdG(k,l,s) = Sval-Txval;                             
             }
           }          
        }
        //
      }
      _try(VecDestroy(&StressMatX));
      _try(MatDestroy(&kinMatRHS));
      _try(MatDestroy(&TTK));
      _try(MatDestroy(&Lmat));
      _try(VecDestroy(&x));
      _try(VecDestroy(&Tx));
      _try(VecDestroy(&kinematicRHS));
      return 1;
  };
  
  
  virtual int condensationdUdF(nlsDofManager* pAss,  std::map<Dof, STensor3 >* dUdF) {
      int _dimKin = _pAl->getNumberOfMacroToMicroKinematicVariables();
      dUdF->clear();
      if (_isModifiedBodyForceMatrix){
        _try(MatAssemblyBegin(_BodyForceMatrix,MAT_FINAL_ASSEMBLY));
        _try(MatAssemblyEnd(_BodyForceMatrix,MAT_FINAL_ASSEMBLY));
        _isModifiedBodyForceMatrix = false;
      }
     
      createKSPSolver();  
      
      // compute kinematic kinMatRHS
      // MatRHS = (-TT*K*S)
      Mat TTK;
      _try(MatTransposeMatMult(_T,this->_a,MAT_INITIAL_MATRIX,PETSC_DECIDE,&TTK));
      Mat kinMatRHS;
      _try(MatMatMult(TTK,_S,MAT_INITIAL_MATRIX,PETSC_DECIDE,&kinMatRHS));
      //
      Mat TTBmdGMat;
      _try(MatTransposeMatMult(_T,_BodyForceMatrix,MAT_INITIAL_MATRIX,PETSC_DECIDE,&TTBmdGMat));
      _try(MatAXPY(kinMatRHS,-1.,TTBmdGMat,DIFFERENT_NONZERO_PATTERN));
      _try(MatDestroy(&TTBmdGMat));

      // solve for each column of _KinematicMatrix
      Vec x, Tx;
      _try(VecDuplicate(_xConden,&x));
      _try(VecDuplicate(this->_xsol,&Tx));

      Vec kinematicRHS;
        _try(VecDuplicate(_xConden,&kinematicRHS));

      for (int i=0; i< 9; i++){
        PetscInt col = i;
        _try(MatGetColumnVector(kinMatRHS,kinematicRHS,col));

        PetscReal NormRHS;
        _try(VecNorm(kinematicRHS,NORM_INFINITY,&NormRHS));

        if (NormRHS > 0.){
          // solve if norm greater than 0
          _try(KSPSolve(this->_ksp, kinematicRHS, x));
          _try(MatMult(_T,x,Tx));
        }
        int k, l; 
        PetscScalar Sval, Txval;
        Tensor23::getIntsFromIndex(i,k,l); // convert vector to matrix
        const std::map<Dof, int>& unknown = pAss->getUnknownMap();

        for (std::map<Dof, int>::const_iterator it= unknown.begin(); it != unknown.end(); it++) {
           STensor3& dUcompdF = dUdF->operator[](it->first);
           PetscInt idof = it->second;
           MatGetValue(_S,idof,col,&Sval);
           PetscInt ix[]={idof};
           VecGetValues(Tx,1,ix,&Txval);
           dUcompdF(k,l) = Sval-Txval;                             
        }
     }          
        
     _try(MatDestroy(&kinMatRHS));
     _try(MatDestroy(&TTK));
     _try(VecDestroy(&x));
     _try(VecDestroy(&Tx));
     _try(VecDestroy(&kinematicRHS));
     return 1;
   };

#else
    pbcConstraintEliminationNonLinearSystemPETSc(){Msg::Error("Petsc is not available");};
    virtual ~pbcConstraintEliminationNonLinearSystemPETSc(){};
    virtual void setPBCAlgorith(pbcAlgorithm* p) {};
    virtual void zeroKinematicVector() {};
    virtual void addToKinematicVector(int row, const scalar& val) {};
    virtual void initialKinematicVector(int row, const scalar& val){}
		virtual void updateSystemUnknown()  {};
    virtual void computeConstraintProjectMatrices() {}; // compute all constraint projection matrices
    virtual bool isAllocatedStressMatrix() const { return false;};
		virtual void clearStressMatrix() {};
		virtual void allocateStressMatrix(int stressDim, int nbR) {};
    virtual void addToStressMatrix(int row, int col, const scalar& val) {};
    virtual void zeroStressMatrix() {};
    
    virtual bool isAllocatedBodyForceMatrix() const { return false;};
    virtual void clearBodyForceMatrix() {};
    virtual void allocateBodyForcesMatrix(int stressDim, int nbR) {};
    virtual void addToBodyForceMatrix(int row, int col, const scalar& val) {};
    virtual void zeroBodyForceMatrix() {};
    virtual int condensationSolve(fullMatrix<double>& L, const double rvevolume,const bool needdUdF,const bool needdUdG, nlsDofManager* pAss, 
                std::map<Dof, STensor3 >* dUdF, std::map<Dof, STensor33 >* dUdG)  {};
    virtual int condensationdUdF(nlsDofManager* pAss=NULL,  std::map<Dof, STensor3 >* dUdF=NULL)  {};

#endif // HAVE_PETSC
};
#endif // PBCCONSTRAINTELIMINATIONNONLINEARSYSTEMPETSC_H_
