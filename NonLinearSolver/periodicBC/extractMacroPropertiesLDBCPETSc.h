//
//
// Description: class using PETSc for extraction of homogenized tangents
// in case of linear displacement BC
//
// Author:  <Van Dung NGUYEN>, (C) 2010
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef EXTRACTMACROPROPERTIESLDBCPETSC_H_
#define EXTRACTMACROPROPERTIESLDBCPETSC_H_

#include "extractMacroPropertiesPETSc.h"
#include "functionPETSc.h"

#if defined(HAVE_PETSC)
#include "petscksp.h"
#include "petscmat.h"
#endif

class stiffnessCondensationLDBCPETSc : public stiffnessCondensation{
  #if defined(HAVE_PETSC)
  protected:
		nlsDofManager* _pAssembler; // dof mananger
		linearConstraintMatrixPETSc* _bConMat; // all linear constraint matrix
		pbcAlgorithm* _pAl; // pbc algorithm
		bool _tangentflag,_stressflag; // stress and tangent flag
    bool _isInitialized;
    MPI_Comm _comm; // Comunicator MPI

  public:
    stiffnessCondensationLDBCPETSc(nlsDofManager* p, pbcAlgorithm* ma,
                               bool sflag, bool tflag,MPI_Comm com = PETSC_COMM_SELF);
		virtual ~stiffnessCondensationLDBCPETSc();

		virtual void init();
		virtual void clear();
		virtual void setSplittedDofSystem(pbcAlgorithm* pal);
		virtual void stressSolve(fullVector<double>& stressVec, const double Vrve);
		virtual void tangentCondensationSolve(fullMatrix<double>& tangentMat, const double Vrve);

  #else
  public:
    stiffnessCondensationLDBCPETSc(nlsDofManager* p, pbcAlgorithm* ma,
                               bool sflag, bool tflag){
      Msg::Error("Petsc is required");
    };
    virtual ~stiffnessCondensationLDBCPETSc(){};
    virtual void init(){};
    virtual void clear(){};
    virtual void setSplittedDofSystem(pbcAlgorithm* pal){};
    virtual void stressSolve(fullVector<double>& stressVec, const double Vrve){};
		virtual void tangentCondensationSolve(fullMatrix<double>& tangentMat, const double Vrve){};

  #endif // HAVE_PETSC
};
#endif // EXTRACTMACROPROPERTIESLDBCPETSC_H_
