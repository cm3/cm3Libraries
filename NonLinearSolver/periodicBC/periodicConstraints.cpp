//
//
// Description: based class for PBC enforcement
//
// Author:  <Van Dung NGUYEN>, (C) 2010
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "periodicConstraints.h"

periodicConstraintsBase::periodicConstraintsBase(const int tag, const int ndofs, const int dim, nlsDofManager *_p, std::vector<partDomain*>& allDom):
        p(_p),_tag(tag),_dim(dim), _ndofs(ndofs){
	bool fullDG = false;
	for (int i=0; i< allDom.size(); i++){
		if (allDom[i]->getFormulation()){
			fullDG = true;
			break;
		};
	};
	if (fullDG) Msg::Error("periodicConstraintsBase::periodicConstraintsBase can not considered with DG domain");
	space = allDom[0]->getFunctionSpace();
};

periodicConstraintsBase::~periodicConstraintsBase(){
};

void periodicConstraintsBase::getKeysFromVertex(MVertex* ver,std::vector<Dof> &keys){
  MPoint pt(ver,0,0);
  space->getKeys(&pt,keys);
};

MVertex* periodicConstraintsBase::findNearestVertex(MVertex* vP, vertexContainer container){
  MVertex* vinit=*(container.begin());
  double dist=vinit->distance(vP);
  for (vertexContainer::iterator it=container.begin(); it!=container.end(); it++){
    MVertex* v=*it;
    double tempDist=v->distance(vP);
    if (tempDist<dist) {
      dist=tempDist;
      vinit=v;
    };
  };
  return vinit;
};

void periodicConstraintsBase::setPeriodicGroup(std::vector<elementGroup*> group,int dim){
  if ((group.size()==4)||(group.size()==6)){
    gVertex.clear();
    gElement.clear();
    for (int i=0; i<group.size(); i++){
      vertexContainer container;
      for (elementGroup::vertexContainer::const_iterator it=group[i]->vbegin(); it!=group[i]->vend(); it++){
        container.insert(it->second);
      };
      gVertex.push_back(container);
      container.clear();
      elementContainer eleContainer;
      for (elementGroup::elementContainer::const_iterator it=group[i]->begin(); it!=group[i]->end(); it++){
        eleContainer.insert(it->second);
      };
      gElement.push_back(eleContainer);
    };
    //vertex in corner
    coinVertex.clear();
    if (dim==2){
      vertexContainer v1, v2, v3, v4;
      groupIntersection(gVertex[0],gVertex[1],v1);
      if (v1.begin()!=v1.end()) coinVertex.insert(*(v1.begin()));
      groupIntersection(gVertex[1],gVertex[2],v2);
      if (v2.begin()!=v2.end()) coinVertex.insert(*(v2.begin()));
      groupIntersection(gVertex[2],gVertex[3],v3);
      if (v3.begin()!=v3.end()) coinVertex.insert(*(v3.begin()));
      groupIntersection(gVertex[3],gVertex[0],v4);
      if (v4.begin()!=v4.end()) coinVertex.insert(*(v4.begin()));
    }
    else if (dim==3){
      vertexContainer l02, l12, l32, l42;
      groupIntersection(gVertex[0],gVertex[2],l02);
      groupIntersection(gVertex[1],gVertex[2],l12);
      groupIntersection(gVertex[3],gVertex[2],l32);
      groupIntersection(gVertex[4],gVertex[2],l42);

      vertexContainer l05, l15, l35, l45;
      groupIntersection(gVertex[0],gVertex[5],l05);
      groupIntersection(gVertex[1],gVertex[5],l15);
      groupIntersection(gVertex[3],gVertex[5],l35);
      groupIntersection(gVertex[4],gVertex[5],l45);

      vertexContainer v1, v2, v3, v4, v5, v6,v7,v8;
      groupIntersection(l02,l42,v1); if (v1.begin()!=v1.end()) coinVertex.insert(*(v1.begin()));
      groupIntersection(l02,l12,v2); if (v2.begin()!=v2.end()) coinVertex.insert(*(v2.begin()));
      groupIntersection(l12,l32,v3); if (v3.begin()!=v3.end()) coinVertex.insert(*(v3.begin()));
      groupIntersection(l32,l42,v4); if (v4.begin()!=v4.end()) coinVertex.insert(*(v4.begin()));
      groupIntersection(l05,l45,v5); if (v5.begin()!=v5.end()) coinVertex.insert(*(v5.begin()));
      groupIntersection(l05,l15,v6); if (v6.begin()!=v6.end()) coinVertex.insert(*(v6.begin()));
      groupIntersection(l15,l35,v7); if (v7.begin()!=v7.end()) coinVertex.insert(*(v7.begin()));
      groupIntersection(l35,l45,v8); if (v8.begin()!=v8.end()) coinVertex.insert(*(v8.begin()));
    }
    else{
      std::cout<<"Space dimention error! "<<std::endl;
    };
  }
  else {
    std::cout<<"Periodic input error"<<std::endl;
  };
};

void periodicConstraintsBase::groupIntersection(vertexContainer g1, vertexContainer g2, vertexContainer &g){
  g.clear();
  for (vertexContainer::iterator it1=g1.begin(); it1!=g1.end(); it1++){
    MVertex* v1=*it1;
    for (vertexContainer::iterator it2=g2.begin(); it2!=g2.end(); it2++){
      MVertex* v2=*it2;
      if (v1->getNum()==v2->getNum()){
        g.insert(v1);
      };
    };
  };
};

void periodicConstraintsBase::periodicConditionBase(MVertex* vP, MVertex* vN){
  fullVector<double> pos; pos.resize(3); pos.setAll(0.0);
  pos(0)=vP->x()-vN->x(); pos(1)=vP->y()-vN->y(); pos(2)=vP->z()-vN->z();
  fullVector<double> g; g.resize(_ndofs);
  imposedDef.mult(pos,g);
  std::vector<Dof> kP, kN;
  getKeysFromVertex(vP,kP);
  getKeysFromVertex(vN,kN);
  int ndofs=kP.size();
  if (ndofs != _ndofs) Msg::Error("wrong definition of ndofs in periodicConstraintsBase");
  DofAffineConstraint<double> cons;
  for (int i=0; i<ndofs; i++){
    cons.linear.push_back(std::pair<Dof,double>(kN[i],1.0));
    cons.shift=g(i);
    p->setLinearConstraint(kP[i],cons);
    cons.linear.clear();
  };
};

void periodicConstraintsBase::cornerConstraint(){
  for (vertexContainer::iterator it=coinVertex.begin(); it!=coinVertex.end(); it++){
		nodeConstraint(*it);
		/*
    if (it!=coinVertex.begin()){
      vertexContainer::iterator ittemp=it;
      ittemp--;
      MVertex* v1=*ittemp;
      MVertex* v2=*it;
      periodicConditionBase(v1,v2);
    };

/*
    MVertex* v=*it;
    std::vector<Dof> k;
    getKeysFromVertex(v,k);
    fullVector<double> pos; pos.resize(3);
    pos(0)=v->x(); pos(1)=v->y(); pos(2)=v->z();
    fullVector<double> g; g.resize(_ndofs);
    imposedDef.mult(pos,g);
    for (int ii=0; ii<k.size(); ii++){
      p->fixDof(k[ii],g(ii));
    };
    */
  };
};

void periodicConstraintsBase::nodeConstraint(MVertex* v){
  std::vector<Dof> k;
  getKeysFromVertex(v,k);
  int ndofs=k.size();
  fullVector<double> pos; pos.resize(3); pos.setAll(0.0);
  pos(0)=v->x(); pos(1)=v->y(); pos(2)=v->z();
  DofAffineConstraint<double> cons;
  fullVector<double> g; g.resize(_ndofs); g.setAll(0.0);
  imposedDef.mult(pos,g);
  if (ndofs != _ndofs) Msg::Error("wrong definition of ndofs in periodicConstraintsBase");
  for (int i=0; i<ndofs; i++){
    cons.shift=g(i);
    p->setLinearConstraint(k[i],cons);
  };
};

