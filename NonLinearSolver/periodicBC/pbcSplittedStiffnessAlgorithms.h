//
//
// Description: assemble splitted tangents for condensation tangent procedure
//
// Author:  <Van Dung NGUYEN>, (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef PBCSPLITTEDSTIFFNESSALGORITHMS_H_
#define PBCSPLITTEDSTIFFNESSALGORITHMS_H_

#include "terms.h"
#include "quadratureRules.h"

template<class Iterator, class Assembler> void AssembleSplittedStiffness(BilinearTermBase &term, FunctionSpaceBase &space,
                                                        Iterator itbegin, Iterator itend,
                                                        QuadratureBase &integrator, Assembler &assembler)
  // symmetric
{
  fullMatrix<double> localMatrix;
  std::vector<Dof> R;
  for (Iterator it = itbegin; it != itend; it++){
    MElement *e = it->second;
    R.clear();
    IntPt *GP;
    int npts = integrator.getIntPoints(e, &GP);
    term.get(e, npts, GP, localMatrix); //localMatrix.print();
    space.getKeys(e, R);
    assembler.assemble(R, localMatrix);
  }
}
template<class Iterator, class Assembler> void AssembleSplittedStiffnessDefoDefoContact(BilinearTermBase &term, FunctionSpaceBase &space,
                                                        Iterator itbegin, Iterator itend,
                                                        QuadratureBase &integrator, Assembler &assembler)
  // symmetric
{
  fullMatrix<double> localMatrix;
  std::vector<Dof> R;
  for (Iterator it = itbegin; it != itend; it++){
    contactElement *e = *it;
    R.clear();
    IntPt *GP;
    int npts = integrator.getIntPoints(e->getRefToElementMaster(), &GP);
    defoDefoContactBilinearTermBase<double> *defoDefoTerm = static_cast<defoDefoContactBilinearTermBase<double> *> (&term);
    defoDefoTerm->get(e, npts, GP, localMatrix); 
    //localMatrix.print();
    defoDefoContactSpaceBase *sp=static_cast<defoDefoContactSpaceBase *> (&space);
    sp->getKeys(e, R);
    assembler.assemble(R, localMatrix);
  }
}
template<class Iterator, class Assembler> void SparsityDofsSplittedStiffness(FunctionSpaceBase &space, Iterator itbegin,
                                                          Iterator itend, Assembler &assembler)
{
  for (Iterator it = itbegin; it != itend; ++it){
    MElement *e = it->second;
    std::vector<Dof> R;
    space.getKeys(e, R);
    assembler.sparsityDof(R);
  }
}



#endif // PBCSPLITTEDSTIFFNESSALGORITHMS_H_
