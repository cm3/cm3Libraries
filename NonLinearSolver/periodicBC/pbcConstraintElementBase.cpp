//
//
// Description: PBC constraint element base
//
// Author:  <Van Dung NGUYEN>, (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "pbcConstraintElementBase.h"

std::set<Dof> constraintElement::allPositiveDof;

constraintElement::constraintElement(const nonLinearMicroBC* mbc,const int c): _mbc(mbc){
	// if nc > 1, c is not used
	// if nc = 1, c is considered
	_component.clear();
	const std::set<int>& allConstrainedComps = _mbc->getConstrainedComps();
	if (c== -1){
		// all Dofs are accounted into
		for (std::set<int>::const_iterator it = allConstrainedComps.begin(); it!= allConstrainedComps.end(); it++){
			_component.push_back(*it);
		}
		_ndofs = _component.size();
	}
	else{
		if (allConstrainedComps.find(c) != allConstrainedComps.end()){
			_component.push_back(c);
			_ndofs = 1;
		}
		else{
			Msg::Error("cannot constrain on comp %d constraintElement::constraintElement",c);
		}
	}
}