//
//
// Description: apply PBC by constraint elimination method
//
// Author:  <Van Dung NGUYEN>, (C) 2010
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef CONSTRAINTELIMINATIONMETHOD_H_
#define CONSTRAINTELIMINATIONMETHOD_H_

#include "periodicConstraints.h"

class constraintElimination : public periodicConstraintsBase{
  protected:
    void periodicCondition();

  public:
    constraintElimination(const int tag, const int ndofs, const int dim,nlsDofManager *_p, std::vector<partDomain*>& allDom): 
						periodicConstraintsBase(tag,ndofs,dim,_p,allDom){
    };
    virtual ~constraintElimination(){};

    virtual void applyPeriodicCondition();
};

#endif // CONSTRAINTELIMINATIONMETHOD_H_
