//
//
// Description: PBC algorithm
//
// Author:  <Van Dung NGUYEN>, (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef PBCALGORITHM_H_
#define PBCALGORITHM_H_

#include "nonLinearBC.h"
#include "pbcConstraintElementBase.h"
#include "pbcCreateConstraints.h"
#include "linearConstraintMatrix.h"
#include "splitStiffnessMatrix.h"
#include "pbcDofManager.h"
#include "sparsityPattern.h"

class nonLinearMechSolver;
class pbcAlgorithm{
	protected:
		FunctionSpaceBase* _space, *_mspace;  // function space
		pbcConstraintElementGroup* _pbcConstraint;  // all linear periodic groups
		pbcDofManager* _splittedDofs;
		double _scale;	//factor scale
		bool _isSplitted;
		bool _isModifiedConstraintMatrix;
		nonLinearMechSolver* _solver;

  protected:
    void __int__();

	public:
		pbcAlgorithm(nonLinearMechSolver* solver);
		virtual ~pbcAlgorithm();
		
    pbcDofManager* getSplittedDof() {return _splittedDofs;};
    splitStiffnessMatrix* getSplitStiffness() {return _splittedDofs->getSplitStiffnessMatrix();};
    pbcConstraintElementGroup*  getPBCConstraintGroup() {return _pbcConstraint;};
    nonLinearMicroBC* getMBC();
    bool isSplittedDof() const {return _isSplitted;};
    double getRVEVolume() const {return _pbcConstraint->getRVEVolume();};
    const SVector3 &getRVEGeometry() const {return _pbcConstraint->getRVEGeometry();};
    const STensor3 &getRVEGeometricalInertia() const {return _pbcConstraint->getRVEGeometricalInertia();}
    int getSystemDofNumber(const Dof& key);

    bool isModifiedConstraintMatrix() const {return _isModifiedConstraintMatrix;};
    void setIsModifiedConstraintMatrixFlag(const bool flag){_isModifiedConstraintMatrix = flag;};

    void updateConstraint(const IPField* ipfield);

		int getMicroConstraintSize() const{return _pbcConstraint->size();};

    void splitDofs(); // split dofs

    void numberDof(const int type); // number Dof
    int getNumberDirectConstraints() const {return _pbcConstraint->sizeOfDirectConstraints();};
    int getNumberOfMacroToMicroKinematicVariables() const;;
    int getNumberMicroToMacroHomogenizedVariables() const;
    // for estimating homogenized tangent in system
    void assembleKinematicMatrix();
    void zeroStressMatrix();
    void zeroBodyForceMatrix();
    void zeroBodyForceVector();
    int condensationSolve(fullMatrix<double>& L, double rvevolume, const bool needdUdF=false,const bool needdUdG=false, nlsDofManager* pAss=NULL,  
             std::map<Dof, STensor3 >* dUdF = NULL, std::map<Dof, STensor33 >* dUdG = NULL);
    int condensationdUdF(nlsDofManager* pAss=NULL,  std::map<Dof, STensor3 >* dUdF = NULL);

    // for lagrange multipliers method : multipliers are accounted as normal dofs
    void sparsityLagMultiplierDofs(); //pre allocated
		void assembleConstraintMatrix(); // add to stiffness matrix
		void assembleRightHandSide();	   // assemble external force vector
		void computeLinearConstraints(); // recompute the linear constraint
    void zeroMultipleRHS();
    void assembmeToMultipleRHS();
    bool systemSolveMultipleRHS();
    double getFromMultipleRHSSolution(int Nrow,int col);

    // for lagrange multiplier method, all multipliers are eliminated
		void allocateConstraintMatrixToSystem(int stressDim, int kinematicDim, bool tangentFlag);
		void assembleLinearConstraintMatrixToSystem();
		void assembleConstraintResidualToSystem();

		// apply PBC with constraint elimination
    void applyPBCByConstraintElimination();

    // apply PBC with constraint elimination in system
    void computeIndirectLinearConstraintMatrixForPBCSystem(linearConstraintMatrix* lcon) const;
    void updateSystemUnknown();

		// write data to linear constraint matrix to condense the tangent
    // used for stiffnessCondensationPETScWithDirectConstraints
    void computeDirectLinearConstraintMatrix(linearConstraintMatrix* lcon) const;
    void computeIndirectLinearConstraintMatrix(linearConstraintMatrix* lcon) const;
    void fillIndirectConstraintPattern(sparsityPattern* Cbd, sparsityPattern* Cbi) const;

    // used in interpolationStiffnessCondensationPETSC
    void computeIndirectLinearConstraintMatrixInterpolationWithNewVertices(linearConstraintMatrix* lcon) const;

    // used for stiffnessCondensationPETScWithoutDirectConstraints
    void computeLinearConstraintMatrix(linearConstraintMatrix* lcon) const;
    // used for stiffnessCondensationLDBCPETSc
    void computeLinearConstraintMatrixLDBC(linearConstraintMatrix* lcon) const;

    void computeLoadVector();
};

#endif // PBCALGORITHM_H_
