//
//
// Description: pbc Dof manager
//
// Author:  <Van Dung NGUYEN>, (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "pbcDofManager.h"
#include "splitStiffnessMatrixPETSc.h"
#include "splitStiffnessMatrixFullMatrix.h"

pbcDofManager::pbcDofManager():_splittedStiffness(NULL), Igroup(NULL),
  Bgroup(NULL), Cgroup(NULL){}

pbcDofManager::~pbcDofManager(){
  clear();
};

void pbcDofManager::allocateSplittedMatrix(){
  if (_splittedStiffness == NULL){
    #if defined(HAVE_PETSC)
    _splittedStiffness = new splitStiffnessMatrixPETSc(PETSC_COMM_SELF);
    #else
    _splittedStiffness = new splitStiffnessMatrixFullMatrix();
    #endif // HAVE_PETSC
  }

  if (_splittedStiffness->isAllocated() == false){
    if (Igroup == NULL or Bgroup == NULL or Cgroup == NULL){
      Msg::Error("Dof groups are not initialized when splitting stiffness matrix pbcDofManager::allocateSplittedMatrix");
      return;
    }

    _splittedStiffness->allocateSubMatrix(Igroup->size(),Bgroup->size(),Cgroup->size());
  }
};


splitStiffnessMatrix* pbcDofManager::getSplitStiffnessMatrix(){
  return _splittedStiffness;
}

void pbcDofManager::clear(){
	_fixedDof.clear();
  _internalDof.clear();
  _boundaryDof.clear();
  _directDof.clear();
  _indirectDof.clear();
  _dependentDof.clear();
  _independentDof.clear();
  _virtualDof.clear();
  _realIndependentDof.clear();
  _multiplierDof.clear();
  _directMultiplierDof.clear();
  _indirectMultiplierDof.clear();

  if (_splittedStiffness) delete _splittedStiffness;
  _splittedStiffness = NULL;
};

void pbcDofManager::assembleSubMatrix(const Dof& R, const Dof& C, const double& val){
	if (_fixedDof.find(R) != _fixedDof.end()) return;
	if (_fixedDof.find(C) != _fixedDof.end()) return;
	
  if (Igroup == NULL or Bgroup == NULL or Cgroup == NULL){
    Msg::Error("Dof groups are not initialized when splitting stiffness matrix pbcDofManager::assembleSubMatrix");
  }
  std::map<Dof,int>::const_iterator itRi = Igroup->find(R);
  if (itRi != Igroup->end()){
    int row = itRi->second;
    std::map<Dof,int>::const_iterator itCi = Igroup->find(C);

    if (itCi != Igroup->end()){
      int col = itCi->second;
      _splittedStiffness->addToSubMatrix_II(row,col,val);
      return;
    }

    std::map<Dof,int>::const_iterator itCb = Bgroup->find(C);
    if (itCb != Bgroup->end()){
      int col = itCb->second;
      _splittedStiffness->addToSubMatrix_IB(row,col,val);
      return;
    }

    std::map<Dof,int>::const_iterator itCc = Cgroup->find(C);
    if (itCc != Cgroup->end()){
      int col = itCc->second;
      _splittedStiffness->addToSubMatrix_IC(row,col,val);
      return;
    }
    else{
      Msg::Error("error in dof separation");
    };
  }
  else {
    std::map<Dof,int>::const_iterator itRb = Bgroup->find(R);
    if (itRb != Bgroup->end()){
      int row = itRb->second;
      std::map<Dof,int>::const_iterator itCi = Igroup->find(C);
      if (itCi != Igroup->end()){
        int col = itCi->second;
        _splittedStiffness->addToSubMatrix_BI(row,col,val);
        return;
      }

      std::map<Dof,int>::const_iterator itCb = Bgroup->find(C);
      if (itCb != Bgroup->end()){
        int col = itCb->second;
        _splittedStiffness->addToSubMatrix_BB(row,col,val);
        return;
      }

      std::map<Dof,int>::const_iterator itCc = Cgroup->find(C);
      if (itCc != Cgroup->end()){
        int col = itCc->second;
        _splittedStiffness->addToSubMatrix_BC(row,col,val);
        return;
      }
      else{
        Msg::Error("error in dof separation");
      };
    }
    else {
      std::map<Dof,int>::const_iterator itRc = Cgroup->find(R);
      if (itRc != Cgroup->end()){
        int row = itRc->second;

        std::map<Dof,int>::const_iterator itCi = Igroup->find(C);
        if (itCi != Igroup->end()){
          int col = itCi->second;
          _splittedStiffness->addToSubMatrix_CI(row,col,val);
          return;
        }

        std::map<Dof,int>::const_iterator itCb = Bgroup->find(C);
        if (itCb != Bgroup->end()){
          int col = itCb->second;
          _splittedStiffness->addToSubMatrix_CB(row,col,val);
          return;
        }

        std::map<Dof,int>::const_iterator itCc = Cgroup->find(C);
        if (itCc != Cgroup->end()){
          int col = itCc->second;
          _splittedStiffness->addToSubMatrix_CC(row,col,val);
          return;
        }
        else{
          Msg::Error("error in dof separation in colume");
        };
      }
      else {
        Msg::Error("error in dof separation in row");
      };
    };
  };
};


void pbcDofManager::insertInSparsityPattern(const Dof &R, const Dof &C) {
	if (_fixedDof.find(R) != _fixedDof.end()) return;
	if (_fixedDof.find(C) != _fixedDof.end()) return;
	
  if (Igroup == NULL or Bgroup == NULL or Cgroup == NULL){
    Msg::Error("Dof groups are not initialized when splitting stiffness matrix pbcDofManager::assembleSubMatrix");
  }
  std::map<Dof,int>::const_iterator itRi = Igroup->find(R);
  if (itRi != Igroup->end()){
    int row = itRi->second;
    std::map<Dof,int>::const_iterator itCi = Igroup->find(C);

    if (itCi != Igroup->end()){
      int col = itCi->second;
      _splittedStiffness->insertInSparsityPattern_II(row,col);
      return;
    }

    std::map<Dof,int>::const_iterator itCb = Bgroup->find(C);
    if (itCb != Bgroup->end()){
      int col = itCb->second;
      _splittedStiffness->insertInSparsityPattern_IB(row,col);
      return;
    }

    std::map<Dof,int>::const_iterator itCc = Cgroup->find(C);
    if (itCc != Cgroup->end()){
      int col = itCc->second;
      _splittedStiffness->insertInSparsityPattern_IC(row,col);
      return;
    }
    else{
      Msg::Error("error in dof separation");
    };
  }
  else {
    std::map<Dof,int>::const_iterator itRb = Bgroup->find(R);
    if (itRb != Bgroup->end()){
      int row = itRb->second;
      std::map<Dof,int>::const_iterator itCi = Igroup->find(C);
      if (itCi != Igroup->end()){
        int col = itCi->second;
        _splittedStiffness->insertInSparsityPattern_BI(row,col);
        return;
      }

      std::map<Dof,int>::const_iterator itCb = Bgroup->find(C);
      if (itCb != Bgroup->end()){
        int col = itCb->second;
        _splittedStiffness->insertInSparsityPattern_BB(row,col);
        return;
      }

      std::map<Dof,int>::const_iterator itCc = Cgroup->find(C);
      if (itCc != Cgroup->end()){
        int col = itCc->second;
        _splittedStiffness->insertInSparsityPattern_BC(row,col);
        return;
      }
      else{
        Msg::Error("error in dof separation");
      };
    }
    else {
      std::map<Dof,int>::const_iterator itRc = Cgroup->find(R);
      if (itRc != Cgroup->end()){
        int row = itRc->second;

        std::map<Dof,int>::const_iterator itCi = Igroup->find(C);
        if (itCi != Igroup->end()){
          int col = itCi->second;
          _splittedStiffness->insertInSparsityPattern_CI(row,col);
          return;
        }

        std::map<Dof,int>::const_iterator itCb = Bgroup->find(C);
        if (itCb != Bgroup->end()){
          int col = itCb->second;
          _splittedStiffness->insertInSparsityPattern_CB(row,col);
          return;
        }

        std::map<Dof,int>::const_iterator itCc = Cgroup->find(C);
        if (itCc != Cgroup->end()){
          int col = itCc->second;
          _splittedStiffness->insertInSparsityPattern_CC(row,col);
          return;
        }
        else{
          Msg::Error("error in dof separation in colume");
        };
      }
      else {
        Msg::Error("error in dof separation in row");
      };
    };
  };
};
