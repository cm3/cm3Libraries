//
//
// Description: create linear system from micro BCs
//
// Author:  <Van Dung NGUYEN>, (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef PBCCREATCONSTRAINTS_H_
#define PBCCREATCONSTRAINTS_H_

#include "partDomain.h"
#include "pbcConstraintElementBase.h"
#include "nonLinearBC.h"
#include "nonLinearMicroBC.h"
#include "pbcVertex.h"

class GModel;
class nlsDofManager;

/*
   //This class creates all constraints based on vertices
   //for RVE 2D
   // line periodic l12 (g[0]) - l34 (g[2])
   // line periodic l41 (g[1]) - l23 (g[3])
   // node periodic 1 - 2 -3 -4
   // root control point 1
   // other control points 2 - 4
   // base line 12 - 41

   4--------l34-------3
   |                  |
   |                  |
  l41      RVE 2D    l23
   |                  |
   |                  |
   1-------l12--------2



   // FOR RVE 3D

   // periodic surface 1234 (g[0]) - 5678 (g[3])
   // periodic surface 1562 (g[1]) - 4873 (g[4])
   // periodic surface 1584 (g[2]) - 2673 (g[5])

   // line periodic l12 - l34 - l46 - l78
   // line periodic l41 - l23 - l67 - l85
   // line periodic l15 - l26 - l37 - l48

   // node periodic 1 - 2 - 3 - 4 - 5 - 6 - 7 - 8

   // root control point 1
   // other control points 2 - 4 - 5
   // base line 12 - 41 - 15
   // base surface 1234 - 1562 - 1584

            8--------l78--------7
           *|                  *|
         *  !                *  |
       l85  !              l67  |
      *    l48             *   l37
    *       !            *      |
   5--------!l56--------6       |
   |        4-------l34-|-------3
   |       *            |      *
  l15    *             l26    *
   |   l41              |  l23
   |  *                 |  *
   | *                  |*
   1--------l12---------2
*/

namespace InterpolationOperations{
  void groupIntersection(elementGroup* g1, elementGroup* g2, std::set<MVertex*>& ver);
  void groupIntersection(std::set<MVertex*>& g1, std::set<MVertex*>& g2, std::set<MVertex*>& ver);
  /* create group of line at intersection of two faces*/
  void intersectionBetweenTwoFaces(const elementGroup* face1, const elementGroup* face2, elementGroup& line);
  /* get interpolation points on a line*/
  template<class Iterator>
  void createLinePolynomialBase(const int degree, // degree
                                const bool createNewVer,  // true if new vertices are created false if interpolation base is taken from existing vertices
                                MVertex* vstart, MVertex* vend, // vstart and vend 
                                Iterator itbegin, Iterator itend,  // current line
                                std::vector<MVertex*>& base, // base obtained
                                std::set<MVertex*>& virtualVertices // virtualVertices to store all virtual virtices which has been created
                                );
  template<class Iterator>
  void createLinePolynomialBaseItMap(const int degree, // degree
                                const bool createNewVer,  // true if new vertices are created false if interpolation base is taken from existing vertices
                                MVertex* vstart, MVertex* vend, // vstart and vend 
                                Iterator itbegin, Iterator itend,  // current line
                                std::vector<MVertex*>& base, // base obtained
                                std::set<MVertex*>& virtualVertices // virtualVertices to store all virtual virtices which has been created
                                );                              
  void createLinePolynomialBase(const int degree, // degree
                                const bool createNewVer,  // true if new vertices are created false if interpolation base is taken from existing vertices
                                MVertex* vstart, MVertex* vend, // vstart and vend 
                                std::set<MVertex*>& line,  // current line
                                std::vector<MVertex*>& base, // base obtained
                                std::set<MVertex*>& virtualVertices // virtualVertices to store all virtual virtices which has been created
                                );
  /*
  This function allows to find a nearest vertex in a group vertex to one vertex
  by using the minimal distance flag
  */
  template<class Iterator>
  MVertex* findNearestVertex(Iterator itbegin, Iterator itend, MVertex* vp);
  template<class Iterator>
  MVertex* findNearestVertexItMap(Iterator itbegin, Iterator itend, MVertex* vp);
  MVertex* findNearestVertex(elementGroup* g, MVertex* vp);
  
  
  void createGroupLinearLineElementFromVerticesVector(std::vector<MVertex*>& b, elementGroup& gr);
  void createGroupQuadraticLineElementFromVerticesVector(std::vector<MVertex*>& b, elementGroup& gr, std::set<MVertex*>& virVertices);
  
  /*create a surface group to interpolate form two boundaries for FE method */
  void createLinearElementBCGroup3DFromTwoEdges(std::vector<MVertex*>& vlx, std::vector<MVertex*>& vly,
                       MVertex* vc, elementGroup& g, std::set<MVertex*>& newVer);
  void createQuadraticElementBCGroup3DFromTwoEdges(std::vector<MVertex*>& vlx, std::vector<MVertex*>& vly,
                       MVertex* vc, elementGroup& g, std::set<MVertex*>& newVer);
                       
  void createPolynomialBasePoints(nonLinearPeriodicBC* pbc,
                                  MVertex* v1, MVertex* v2, MVertex* v4, MVertex* v5,
                                  std::set<MVertex*>& l12, std::set<MVertex*>& l14,std::set<MVertex*>& l15,
                                  std::vector<MVertex*>& xbase, std::vector<MVertex*>& ybase, std::vector<MVertex*>& zbase,
                                  std::set<MVertex*>& newVer);
  bool isInside(MVertex* v, MElement* e);
  
  // for 3D
  void create_MQuadrangleN_Base(int order, bool serendip, MVertex* v1, MVertex* v2, MVertex* v3, MVertex* v4, 
                                                    MVertex* v5, MVertex* v6, MVertex* v7, MVertex* v8,
                                                    MElement*& eX, MElement*& eY, MElement*& eZ,
                                                    MElement*& eXY, MElement*& eYZ, MElement*& eZX,
                                                    std::set<MVertex*>& newBounderyVers, 
                                                    std::set<MVertex*>& allNewVers);
  // for 2D
  void create_MLineN_Base(int order, MVertex* v1, MVertex* v2, MVertex* v4, MElement*& eX, MElement*& eY, 
                            std::set<MVertex*>& newVers);
  
  void write_MSH2(elementGroup* gr, std::string filename);
};


class elementGroupDomainDecomposition{
	public:
		int physical;
		elementGroup* g;
		std::map<partDomain*, elementGroup*> decompositionMap;
		
		elementGroupDomainDecomposition(const int phys, elementGroup* g_, std::vector<partDomain*>& allDomain);
		~elementGroupDomainDecomposition();
};


class pbcConstraintElementGroup{
  protected:
    bool _fullDG; // DG flag
		nonLinearMechSolver* _solver;

    FunctionSpaceBase* _LagSpace, *_MultSpace; // lagspace and mult space
    std::set<constraintElement*> _allConstraint; // all constraint
    
    std::set<MVertex*> _allHighOrderVertices;
    std::set<int> _constrainedCompPrimary; // component where microBC is applied with the primary vertices
    std::set<MVertex*> _cVertices;  // corner vertices
    std::set<MVertex*> _bVertices;  // edge vertices, in 2D corner and edger vertices are same
    std::set<MVertex*> _virtualVertices; // new vertices added to systems for polynomial interpolation
    
    elementGroup* _g;
    /*in case of 2D*/
    std::set<MVertex*> l12, l23, l34,l41; // 4 edges    
    //add surfaces of RVE in 3D cases
    elementGroup * f1234, *f5678, *f1562, *f4873, *f1584, *f2673;
    // add 8 edges of RVE in 3D case
    std::set<MVertex*> l56, l67, l78, l85, l15, l26, l37, l48;
    MVertex* _v1, *_v2, *_v4, *_v5; // all control points
    MVertex* _v3, *_v6, *_v7, *_v8;
        
    double _rveVolume;
    STensor3 _rveGeoInertia;    
    SVector3 _periodic12, _periodic14, _periodic15;  // periodicity computed from RVE
    SVector3 _rveGeometry;
    
    std::vector<MVertex*> _xBase, _yBase, _zBase; // default interpolation basis
    elementGroup* _gX, *_gY, *_gZ, *_gXY, *_gYZ, *_gZX; // basis PC based on FE method
		
		std::vector<elementGroupDomainDecomposition*> _boundaryDGMap;
		std::map<partDomain*,FunctionSpaceBase*> _allDGMultSpace; // all multspace for DG case

  private:
    void __init__();
    template<class Iterator>
    void addVertexToGroupItMap(std::set<MVertex*>& l, Iterator itbegin, Iterator itend);
    void getCornerVerticesForBoundaryPhysical(const int phy, std::vector<MVertex*>& v); // vertex 1, 2, 3 , 4
    void getLinesInFace(const int phy, std::vector<std::set<MVertex*> *>& allLine); // compatible with vertex l12, l23, l34, l4
    
    // in this case, vinit and vend using for constructing the distance function
		
    /*
    Enforce periodic boundary condition on conformal meshes
    positive part: posivtiveitbegin-->positiveitend
    negative part: negativeitbegin-->negativeitend
    others = corner vertices
    */
    template<class Iterator>
    void periodicConditionForPeriodicMesh(Iterator posivtiveitbegin, Iterator positiveitend,
                                               Iterator negativeitbegin, Iterator negativeitend,
                                               std::set<MVertex*>& others, MVertex* vrootP=NULL, MVertex* vrootN=NULL);
    template<class Iterator>
    void periodicConditionForPeriodicMeshItMap(Iterator posivtiveitbegin, Iterator positiveitend,
                                               Iterator negativeitbegin, Iterator negativeitend,
                                               std::set<MVertex*>& others, MVertex* vrootP=NULL, MVertex* vrootN=NULL);                                                
		
		template<class Iterator>
    void periodicConditionForPeriodicMesh(const int comp, const double fact, Iterator posivtiveitbegin, Iterator positiveitend,
                                               Iterator negativeitbegin, Iterator negativeitend,
                                               std::set<MVertex*>& others, MVertex* vrootP=NULL, MVertex* vrootN=NULL);

    template<class Iterator>
    void periodicConditionForPeriodicMeshItMap(const int comp, const double fact, Iterator posivtiveitbegin, Iterator positiveitend,
                                               Iterator negativeitbegin, Iterator negativeitend,
                                               std::set<MVertex*>& others, MVertex* vrootP=NULL, MVertex* vrootN=NULL);

    // for cubic spline
		template<class Iterator>
		void cubicSplineFormCondition(Iterator itbegin, Iterator itend,
                                  std::vector<MVertex*>& vlist, int flag);
    template<class Iterator>
		void cubicSplineFormConditionItMap(Iterator itbegin, Iterator itend,
                                  std::vector<MVertex*>& vlist, int flag);
		template<class Iterator>
		void cubicSplinePeriodicCondition(Iterator itbegin, Iterator itend,
                                      std::vector<MVertex*>& vlist, int flag, std::set<MVertex*>& others);
    template<class Iterator>
		void cubicSplinePeriodicConditionItMap(Iterator itbegin, Iterator itend,
                                      std::vector<MVertex*>& vlist, int flag, std::set<MVertex*>& others);
    // for cubic spline
		template<class Iterator>
		void cubicSplineFormConditionCoonsPatch(Iterator itbegin, Iterator itend, MVertex* vref,
                                    std::vector<MVertex*>& xlist, std::vector<MVertex*>& ylist,
                                    int flagx, int flagy,std::set<MVertex*>& others);
    template<class Iterator>
		void cubicSplineFormConditionCoonsPatchItMap(Iterator itbegin, Iterator itend, MVertex* vref,
                                    std::vector<MVertex*>& xlist, std::vector<MVertex*>& ylist,
                                    int flagx, int flagy,std::set<MVertex*>& others);
		template<class Iterator>
		void cubicSplinePeriodicConditionCoonsPatch(Iterator itbegin, Iterator itend, MVertex* vref,
                                        std::vector<MVertex*>& xlist,std::vector<MVertex*>& ylist,
                                        int flagx, int flagy, std::set<MVertex*>& others);
    template<class Iterator>
		void cubicSplinePeriodicConditionCoonsPatchItMap(Iterator itbegin, Iterator itend, MVertex* vref,
                                        std::vector<MVertex*>& xlist,std::vector<MVertex*>& ylist,
                                        int flagx, int flagy, std::set<MVertex*>& others);
		// for lagrange
		template<class Iterator>
		void lagrangeFormCondition(Iterator itbegin, Iterator itend,
                                std::vector<MVertex*>& vlist, MVertex* vrootP=NULL, MVertex* vrootN = NULL,
                                const int comp = -1);
    template<class Iterator>
		void lagrangeFormConditionItMap(Iterator itbegin, Iterator itend,
                                std::vector<MVertex*>& vlist, MVertex* vrootP=NULL, MVertex* vrootN = NULL,
                                const int comp = -1);
		template<class Iterator>
		void lagrangePeriodicCondition(Iterator itbegin, Iterator itend,
                                   std::vector<MVertex*>& vlist,std::set<MVertex*>& others,
                                   MVertex* vrootP=NULL, MVertex* vrootN = NULL,
                                   const int comp = -1);
    template<class Iterator>
		void lagrangePeriodicConditionItMap(Iterator itbegin, Iterator itend,
                                   std::vector<MVertex*>& vlist,std::set<MVertex*>& others,
                                   MVertex* vrootP=NULL, MVertex* vrootN = NULL,
                                   const int comp = -1);
		template<class Iterator>
		void shiftedLagrangePeriodicCondition(Iterator itbegin, Iterator itend,
                                   std::vector<MVertex*>& vlist,std::set<MVertex*>& others, const SVector3& normal);
		template<class Iterator>
		void shiftedLagrangePeriodicConditionItMap(Iterator itbegin, Iterator itend,
                                   std::vector<MVertex*>& vlist,std::set<MVertex*>& others, const SVector3& normal);															 
		// for lagrange
		template<class Iterator>
		void directionalLagrangeFormCondition(Iterator itbegin, Iterator itend,
                                std::vector<MVertex*>& vlist,const SVector3& pbcDir);
    template<class Iterator>
		void directionalLagrangeFormConditionItMap(Iterator itbegin, Iterator itend,
                                std::vector<MVertex*>& vlist,const SVector3& pbcDir);
																
		template<class Iterator>
		void directionalLagrangePeriodicCondition(Iterator itbegin, Iterator itend,
                                   std::vector<MVertex*>& xlist, std::vector<MVertex*>& ylist,
																	 const SVector3& pbcNormal, const bool allComp);
    template<class Iterator>
		void directionalLagrangePeriodicConditionItMap(Iterator itbegin, Iterator itend,
                                   std::vector<MVertex*>& xlist, std::vector<MVertex*>& ylist,
																	 const SVector3& pbcNormal, const bool allComp);
    // for lagrange
		template<class Iterator>
		void lagrangeFormConditionCoonsPatch(Iterator itbegin, Iterator itend, MVertex* vref,
                                 std::vector<MVertex*>& xlist, std::vector<MVertex*>& ylist,
                                 std::set<MVertex*>& others ,MVertex* vrootP=NULL, MVertex* vrootN = NULL, const int comp = -1);
    template<class Iterator>
		void lagrangeFormConditionCoonsPatchItMap(Iterator itbegin, Iterator itend, MVertex* vref,
                                 std::vector<MVertex*>& xlist, std::vector<MVertex*>& ylist,
                                 std::set<MVertex*>& others ,MVertex* vrootP=NULL, MVertex* vrootN = NULL, const int comp = -1);
		template<class Iterator>
		void lagrangePeriodicConditionCoonsPatch(Iterator itbegin, Iterator itend, MVertex* vref,
                                     std::vector<MVertex*>& xlist,std::vector<MVertex*>& ylist,
                                     std::set<MVertex*>& others ,MVertex* vrootP=NULL, MVertex* vrootN = NULL,const int comp = -1);
    template<class Iterator>
		void lagrangePeriodicConditionCoonsPatchItMap(Iterator itbegin, Iterator itend, MVertex* vref,
                                     std::vector<MVertex*>& xlist,std::vector<MVertex*>& ylist,
                                     std::set<MVertex*>& others ,MVertex* vrootP=NULL, MVertex* vrootN = NULL,const int comp = -1);
    
    template<class Iterator>
    void pbcFormFEConstraintElementGroup(Iterator itbegin, Iterator itend, elementGroup* g,
                                     std::set<MVertex*>& others,  MVertex* vrootP=NULL, MVertex* vrootN=NULL, const int comp = -1);
    template<class Iterator>
    void pbcFormFEConstraintElementGroupItMap(Iterator itbegin, Iterator itend, elementGroup* g,
                                     std::set<MVertex*>& others,  MVertex* vrootP=NULL, MVertex* vrootN=NULL, const int comp = -1);
    template<class Iterator>
    void pbcFEConstraintElementGroup(Iterator itbegin, Iterator itend, elementGroup* g,
                                     std::set<MVertex*>& others,  MVertex* vrootP=NULL, MVertex* vrootN=NULL, const int comp = -1);
    template<class Iterator>
    void pbcFEConstraintElementGroupItMap(Iterator itbegin, Iterator itend, elementGroup* g,
                                     std::set<MVertex*>& others,  MVertex* vrootP=NULL, MVertex* vrootN=NULL, const int comp = -1);
    void createPeriodicMeshConstraintElementGroup();
    void createCubicSplineConstraintElementGroup();
    void createLagrangeConstraintElementGroup();
		void createShiftedLagrangeConstraintElementGroup();
    void createGeneralPBCLagrangeConstraintElementGroup();
    void createFEConstraintElementGroup();
    void createLinearDisplacementConstraintElementGroup();
		void createProjectConstraintElementGroup();
    void createMinimalKinematicConstraintElementGroup();
    void createMixBCConstraintElementGroup();

    void createCornerConstraintElementGroup();
		void createCornerConstraintElementGroupShiftedBPC();
		
		// for DG case
		void createLinearDisplacementConstraintElementGroup_DG();
		void createMinimalKinematicConstraintElementGroup_DG();
		void createMixBCConstraintElementGroup_DG();
		
		void createOrthogonalMixBCConstraintElementGroupDirectionFollowing();
    
    void createPBCConstraintGroup();
    void createShiftedPBCConstraintGroup();
    void createGeneralPBCConstraintGroup();

    SVector3 getUniformDisp(MVertex* v);
    SVector3 getPerturbation(nlsDofManager* pmanager, MVertex* v);
    SVector3 getTangentCubicSpline(nlsDofManager* pmanager, MVertex* v,const int dir);
    // write perturbation to FILE
    void cubicSplineToFile(std::vector<MVertex*>& vlist, nlsDofManager* pmanager, FILE* file, const int dir);
    void lagrangeToFile(std::vector<MVertex*>& vlist, nlsDofManager* pmanager, FILE* file);

  public:
    pbcConstraintElementGroup(nonLinearMechSolver* sl,
                              FunctionSpaceBase* lagspace, FunctionSpaceBase* multspace);
    virtual ~pbcConstraintElementGroup();
    void createConstraintGroup();
		void clearAllConstraints();
    std::set<constraintElement*>::iterator constraintGroupBegin(){return _allConstraint.begin();};
		std::set<constraintElement*>::iterator constraintGroupEnd(){return _allConstraint.end();};
		int size() const {return _allConstraint.size();};
		int sizeOfDirectConstraints() const;
    std::set<MVertex*>::iterator cVertexBegin(){return _cVertices.begin();};
    std::set<MVertex*>::iterator cVertexEnd(){return _cVertices.end();};
    std::set<MVertex*>::iterator virtualVertexBegin(){return _virtualVertices.begin();};
    std::set<MVertex*>::iterator virtualVertexEnd(){return _virtualVertices.end();};
    bool isActive(const MVertex* v) const; // check vertex lying in the constraints
    bool isVirtual(const MVertex* v) const;
    int sizeVirtualVertices() const {return _virtualVertices.size();};
    void print() const ;
    const elementGroup* getAllBulkElement() const {return _g;};
    void createPBCFromFile(FILE* fp, GModel* pModel);
    void writePertBoundaryToFile(const int iter =0);


    /** interface for creating linear constraints with PBC**/
    void createConstraintForTest();
    void numberDof_virtualVertices(nlsDofManager* pmanager);
    void applyLinearConstraintsToSystem(nlsDofManager* pmanager);
    MVertex* getCornerVertex(const int i) const;
    void getDofsOfCornerVertex(const int i, std::vector<Dof>& R) const;
    
    double getRVEVolume() const {return _rveVolume;};
    const STensor3& getRVEGeometricalInertia() const {return _rveGeoInertia;} 
    const SVector3& getRVEGeometry() const {return _rveGeometry;} 
		
    SPoint3 getRootPoint();
    void getPeriodicity(SVector3& Lx, SVector3& Ly, SVector3& Lz);
};

#endif // PBCCREATCONSTRAINTS_H_
//
//
// Description: create linear system from micro BCs
//
// Author:  <Van Dung NGUYEN>, (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
//
