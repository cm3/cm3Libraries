//
//
// Description: store splitted stiffness matrix for condensation using PETSc
//
// Author:  <Van Dung NGUYEN>, (C) 2010
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "splitStiffnessMatrixPETSc.h"

#if defined(HAVE_PETSC)

#include "linearSystemPETSc.hpp"


splitStiffnessMatrixPETSc::splitStiffnessMatrixPETSc(MPI_Comm com):
                      _isAllocated(false),_comm(com),_ni(0), _nb(0),_nc(0),
                      _entriesPreAllocated(false){
  _aiiNotAssembled = false;
  _aibNotAssembled = false;
  _aicNotAssembled = false;

  _abiNotAssembled = false;
  _abbNotAssembled = false;
  _abcNotAssembled = false;

  _aciNotAssembled = false;
  _acbNotAssembled = false;
  _accNotAssembled = false;

};

splitStiffnessMatrixPETSc::~splitStiffnessMatrixPETSc(){
  this->clear();
};

bool splitStiffnessMatrixPETSc::isAllocated() const{
  return _isAllocated;
};
void splitStiffnessMatrixPETSc::clear(){
  if (isAllocated()){
  _entriesPreAllocated = false;
  _aiiNotAssembled = false;
  _aibNotAssembled = false;
  _aicNotAssembled = false;

  _abiNotAssembled = false;
  _abbNotAssembled = false;
  _abcNotAssembled = false;

  _aciNotAssembled = false;
  _acbNotAssembled = false;
  _accNotAssembled = false;
    if (_ni>0)
      _try(MatDestroy(&_aii));
    if (_ni>0 and _nb>0){
      _try(MatDestroy(&_aib));
      _try(MatDestroy(&_abi));
    }
    if (_nb>0)
      _try(MatDestroy(&_abb));
    if (_ni>0 and _nc>0){
      _try(MatDestroy(&_aic));
      _try(MatDestroy(&_aci));
    }
    if (_nb>0 and _nc>0){
      _try(MatDestroy(&_abc));
      _try(MatDestroy(&_acb));
    }
    if (_nc>0)
      _try(MatDestroy(&_acc));

    _isAllocated = false;
  };
};

void splitStiffnessMatrixPETSc::allocateSubMatrix(int ni, int nb, int nc){
  _ni = ni;
  _nb = nb;
  _nc = nc;
  this->clear();
  _isAllocated = true;
  if (_ni>0){
    _try(MatCreate(_comm,&_aii));
    _try(MatSetSizes(_aii,ni,ni,PETSC_DETERMINE,PETSC_DETERMINE));
    _try(MatSetType(_aii,MATSEQAIJ));
    _try(MatSetFromOptions(_aii));
  }
  if (_ni>0 and _nb>0){
    _try(MatCreate(_comm,&_aib));
    _try(MatSetSizes(_aib,ni,nb,PETSC_DETERMINE,PETSC_DETERMINE));
    _try(MatSetType(_aib,MATSEQDENSE)); // for condensation, the matdense must be used for MatMatSolve
    _try(MatSetFromOptions(_aib));
    _try(MatSetUp(_aib));

    _try(MatCreate(_comm,&_abi));
    _try(MatSetSizes(_abi,nb,ni,PETSC_DETERMINE,PETSC_DETERMINE));
    _try(MatSetType(_abi,MATSEQDENSE));
    _try(MatSetFromOptions(_abi));
    _try(MatSetUp(_abi));
  }
  if (_nb>0){
    _try(MatCreate(_comm,&_abb));
    _try(MatSetSizes(_abb,nb,nb,PETSC_DETERMINE,PETSC_DETERMINE));
    _try(MatSetType(_abb,MATDENSE));
    _try(MatSetFromOptions(_abb));
    _try(MatSetUp(_abb));
  }
  if (_ni>0 and _nc>0){
    _try(MatCreate(_comm,&_aic));
    _try(MatSetSizes(_aic,ni,nc,PETSC_DETERMINE,PETSC_DETERMINE));
    _try(MatSetType(_aic,MATDENSE));
    _try(MatSetFromOptions(_aic));
    _try(MatSetUp(_aic));

    _try(MatCreate(_comm,&_aci));
    _try(MatSetSizes(_aci,nc,ni,PETSC_DETERMINE,PETSC_DETERMINE));
    _try(MatSetType(_aci,MATDENSE));
    _try(MatSetFromOptions(_aci));
    _try(MatSetUp(_aci));
  }

  if (_nb>0 and _nc>0){
    _try(MatCreate(_comm,&_abc));
    _try(MatSetSizes(_abc,nb,nc,PETSC_DETERMINE,PETSC_DETERMINE));
    _try(MatSetType(_abc,MATDENSE));
    _try(MatSetFromOptions(_abc));
    _try(MatSetUp(_abc));

    _try(MatCreate(_comm,&_acb));
    _try(MatSetSizes(_acb,nc,nb,PETSC_DETERMINE,PETSC_DETERMINE));
    _try(MatSetType(_acb,MATDENSE));
    _try(MatSetFromOptions(_acb));
    _try(MatSetUp(_acb));
  }
  if (_nc>0){
    _try(MatCreate(_comm,&_acc));
    _try(MatSetSizes(_acc,nc,nc,PETSC_DETERMINE,PETSC_DETERMINE));
    _try(MatSetType(_acc,MATDENSE));
    _try(MatSetFromOptions(_acc));
    _try(MatSetUp(_acc));
  }
};
void splitStiffnessMatrixPETSc::zeroSubMatrix(){
  if (_isAllocated and _entriesPreAllocated){
    if (_ni>0){
      if (_aiiNotAssembled){
        _try(MatAssemblyBegin(_aii, MAT_FINAL_ASSEMBLY));
        _try(MatAssemblyEnd(_aii, MAT_FINAL_ASSEMBLY));
        _aiiNotAssembled = false;
      }

      _try(MatZeroEntries(_aii));
    }
    if (_ni>0 and _nb>0){
      if (_aibNotAssembled){
        _try(MatAssemblyBegin(_aib, MAT_FINAL_ASSEMBLY));
        _try(MatAssemblyEnd(_aib, MAT_FINAL_ASSEMBLY));
        _aibNotAssembled = false;
      }
      _try(MatZeroEntries(_aib));

      if (_abiNotAssembled){
        _try(MatAssemblyBegin(_abi, MAT_FINAL_ASSEMBLY));
        _try(MatAssemblyEnd(_abi, MAT_FINAL_ASSEMBLY));
        _abiNotAssembled = false;
      }
      _try(MatZeroEntries(_abi));
    }
    if (_nb>0){
      if (_abbNotAssembled){
        _try(MatAssemblyBegin(_abb, MAT_FINAL_ASSEMBLY));
        _try(MatAssemblyEnd(_abb, MAT_FINAL_ASSEMBLY));
        _abbNotAssembled = false;
      }
      _try(MatZeroEntries(_abb));
    }

    if (_ni>0 and _nc>0){
      if (_aicNotAssembled){
        _try(MatAssemblyBegin(_aic, MAT_FINAL_ASSEMBLY));
        _try(MatAssemblyEnd(_aic, MAT_FINAL_ASSEMBLY));
        _aicNotAssembled = false;
      }

      _try(MatZeroEntries(_aic));

      if (_aciNotAssembled){
        _try(MatAssemblyBegin(_aci, MAT_FINAL_ASSEMBLY));
        _try(MatAssemblyEnd(_aci, MAT_FINAL_ASSEMBLY));
        _aciNotAssembled =false;
      }

      _try(MatZeroEntries(_aci));
    }

    if (_nb>0 and _nc>0){
      if (_abcNotAssembled){
        _try(MatAssemblyBegin(_abc, MAT_FINAL_ASSEMBLY));
        _try(MatAssemblyEnd(_abc, MAT_FINAL_ASSEMBLY));
        _abcNotAssembled = false;
      }

      _try(MatZeroEntries(_abc));

      if (_acbNotAssembled){
        _try(MatAssemblyBegin(_acb, MAT_FINAL_ASSEMBLY));
        _try(MatAssemblyEnd(_acb, MAT_FINAL_ASSEMBLY));
        _acbNotAssembled = false;
      }
      _try(MatZeroEntries(_acb));
    }
    if (_nc>0){
      if (_accNotAssembled){
        _try(MatAssemblyBegin(_acc, MAT_FINAL_ASSEMBLY));
        _try(MatAssemblyEnd(_acc, MAT_FINAL_ASSEMBLY));
        _accNotAssembled = false;
      }
      _try(MatZeroEntries(_acc));
    }
  };
};

void splitStiffnessMatrixPETSc::insertInSparsityPattern_II(int i, int j){
  if (i<0 || i>= _ni) return;
    _sparsity_II.insertEntry(i,j);
};
void splitStiffnessMatrixPETSc::insertInSparsityPattern_IB(int i, int j){

};
void splitStiffnessMatrixPETSc::insertInSparsityPattern_IC(int i, int j){

};
void splitStiffnessMatrixPETSc::insertInSparsityPattern_BI(int i, int j){

};
void splitStiffnessMatrixPETSc::insertInSparsityPattern_BB(int i, int j){

};
void splitStiffnessMatrixPETSc::insertInSparsityPattern_BC(int i, int j){

};
void splitStiffnessMatrixPETSc::insertInSparsityPattern_CI(int i, int j){

};
void splitStiffnessMatrixPETSc::insertInSparsityPattern_CB(int i, int j){

};
void splitStiffnessMatrixPETSc::insertInSparsityPattern_CC(int i, int j){

};


void splitStiffnessMatrixPETSc::preAllocateEntries(){
  if (!_isAllocated) Msg::Error("system must be allocated first");
  if (_entriesPreAllocated) return;
  _entriesPreAllocated = true;
  // aii
  if (_ni> 0){
    std::vector<int> nByRowDiag (_ni);
    if (_sparsity_II.getNbRows() == 0) {
      PetscInt prealloc = 500;
      PetscBool set;
      PetscOptionsGetInt(PETSC_NULL, "-petsc_prealloc", &prealloc, &set);
      prealloc = std::min(prealloc, _ni);
      nByRowDiag.resize(0);
      nByRowDiag.resize(_ni, prealloc);
    }
    else {
      for (int i = 0; i < _ni; i++) {
        int n;
        const int *r = _sparsity_II.getRow(i, n);
        for (int j = 0; j < n; j++) {
          if (r[j] >= 0 && r[j] < _ni)
            nByRowDiag[i] ++;
        }
      }
      _sparsity_II.clear();
    }
    _try(MatSeqAIJSetPreallocation(_aii, 0,  &nByRowDiag[0]));
  };
};

void splitStiffnessMatrixPETSc::addToSubMatrix_II(int row, int col, const double &val){
  if (!_entriesPreAllocated)
    preAllocateEntries();
  PetscInt ii = row, jj = col;
  _try(MatSetValues(_aii,1,&ii,1,&jj,&val,ADD_VALUES));
  _aiiNotAssembled = true;
};

void splitStiffnessMatrixPETSc::addToSubMatrix_IB(int row, int col, const double& val){
  PetscInt ii = row, jj = col;
  _try(MatSetValues(_aib,1,&ii,1,&jj,&val,ADD_VALUES));
  _aibNotAssembled = true;
};

void splitStiffnessMatrixPETSc::addToSubMatrix_BI(int row, int col, const double& val){
  PetscInt ii = row, jj = col;
  _try(MatSetValues(_abi,1,&ii,1,&jj,&val,ADD_VALUES));
  _abiNotAssembled = true;
};

void splitStiffnessMatrixPETSc::addToSubMatrix_BB(int row, int col, const double& val){
  PetscInt ii = row, jj = col;
  _try(MatSetValues(_abb,1,&ii,1,&jj,&val,ADD_VALUES));
  _abbNotAssembled = true;
};

void splitStiffnessMatrixPETSc::addToSubMatrix_IC(int row, int col, const double& val){
  PetscInt ii = row, jj = col;
  _try(MatSetValues(_aic,1,&ii,1,&jj,&val,ADD_VALUES));
  _aicNotAssembled=true;
};

void splitStiffnessMatrixPETSc::addToSubMatrix_BC(int row, int col, const double& val){
  PetscInt ii = row, jj = col;
  _try(MatSetValues(_abc,1,&ii,1,&jj,&val,ADD_VALUES));
  _abcNotAssembled=true;
};

void splitStiffnessMatrixPETSc::addToSubMatrix_CI(int row, int col, const double& val){
  PetscInt ii = row, jj = col;
  _try(MatSetValues(_aci,1,&ii,1,&jj,&val,ADD_VALUES));
  _aciNotAssembled =true;
};

void splitStiffnessMatrixPETSc::addToSubMatrix_CB(int row, int col, const double& val){
  PetscInt ii = row, jj = col;
  _try(MatSetValues(_acb,1,&ii,1,&jj,&val,ADD_VALUES));
  _acbNotAssembled = true;
};

void splitStiffnessMatrixPETSc::addToSubMatrix_CC(int row, int col, const double& val){
  PetscInt ii = row, jj = col;
  _try(MatSetValues(_acc,1,&ii,1,&jj,&val,ADD_VALUES));
  _accNotAssembled =true;
};

Mat& splitStiffnessMatrixPETSc::getIIMat(){
  if (_ni>0){
    if (_aiiNotAssembled){
      _try(MatAssemblyBegin(_aii,MAT_FINAL_ASSEMBLY));
      _try(MatAssemblyEnd(_aii,MAT_FINAL_ASSEMBLY));
      _aiiNotAssembled = false;
    }

  }
  return _aii;
};
Mat& splitStiffnessMatrixPETSc::getIBMat(){
  if (_ni>0 and _nb>0){
    if (_aibNotAssembled){
      _try(MatAssemblyBegin(_aib,MAT_FINAL_ASSEMBLY));
      _try(MatAssemblyEnd(_aib,MAT_FINAL_ASSEMBLY));
      _aibNotAssembled = false;
    }

  }
  return _aib;
};
Mat& splitStiffnessMatrixPETSc::getBIMat(){
  if (_ni>0 and _nb>0){
    if (_abiNotAssembled){
      _try(MatAssemblyBegin(_abi,MAT_FINAL_ASSEMBLY));
      _try(MatAssemblyEnd(_abi,MAT_FINAL_ASSEMBLY));
      _abiNotAssembled = false;
    }

  }
  return _abi;
};
Mat& splitStiffnessMatrixPETSc::getBBMat(){
  if (_nb>0){
    if (_abbNotAssembled){
      _try(MatAssemblyBegin(_abb,MAT_FINAL_ASSEMBLY));
      _try(MatAssemblyEnd(_abb,MAT_FINAL_ASSEMBLY));
      _abbNotAssembled = false;
    }

  }
  return _abb;
};

Mat& splitStiffnessMatrixPETSc::getICMat(){
  if (_nc>0 and _ni>0){
    if (_aicNotAssembled){
      _try(MatAssemblyBegin(_aic,MAT_FINAL_ASSEMBLY));
      _try(MatAssemblyEnd(_aic,MAT_FINAL_ASSEMBLY));
      _aicNotAssembled = false;
    }

  }
  return _aic;
};

Mat& splitStiffnessMatrixPETSc::getBCMat(){
  if (_nb>0 and _nc>0){
    if (_abcNotAssembled){
      _try(MatAssemblyBegin(_abc,MAT_FINAL_ASSEMBLY));
      _try(MatAssemblyEnd(_abc,MAT_FINAL_ASSEMBLY));
      _abcNotAssembled = false;
    }

  }
  return _abc;
};
Mat& splitStiffnessMatrixPETSc::getCIMat(){
  if (_ni>0 and _nc>0){
    if (_aciNotAssembled){
      _try(MatAssemblyBegin(_aci,MAT_FINAL_ASSEMBLY));
      _try(MatAssemblyEnd(_aci,MAT_FINAL_ASSEMBLY));
      _aciNotAssembled = false;
    }
  }
  return _aci;
};
Mat& splitStiffnessMatrixPETSc::getCBMat(){
  if (_nb>0 and _nc>0){
    if (_acbNotAssembled){
      _try(MatAssemblyBegin(_acb,MAT_FINAL_ASSEMBLY));
      _try(MatAssemblyEnd(_acb,MAT_FINAL_ASSEMBLY));
      _acbNotAssembled = false;
    }

  }
  return _acb;
};
Mat& splitStiffnessMatrixPETSc::getCCMat(){
  if (_nc>0){
    if (_accNotAssembled){
      _try(MatAssemblyBegin(_acc,MAT_FINAL_ASSEMBLY));
      _try(MatAssemblyEnd(_acc,MAT_FINAL_ASSEMBLY));
      _accNotAssembled = false;
    }
  }
  return _acc;
};

#endif // HAVE_PETSC
