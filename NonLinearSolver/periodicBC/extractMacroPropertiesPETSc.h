//
//
// Description: class using PETSc for extraction of homogenized tangents
// in case of PBC
//
// Author:  <Van Dung NGUYEN>, (C) 2010
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef EXTRACTMACROPROPERTIESPETSC_H_
#define EXTRACTMACROPROPERTIESPETSC_H_

#include "extractMacroProperties.h"
#include "linearConstraintMatrixPETSc.h"
#include "splitStiffnessMatrixPETSc.h"
#include "GmshConfig.h"


#if defined(HAVE_PETSC)
#include "petscmat.h"
#include "petscvec.h"
#endif

/*
  Constraint bConMat  --> Cb* ub + Cc*uc = Sb*K
  Constraint cConMat --> uc = Sc*K

  Stress vector: P = (SbT*lambdaB+ ScT*lambdaC)/Volume

  From equilibrium state:
  fi = 0
  fb = CbT* lambdaB
  fc = lambdaC+CcT*lambdaB

  => lambdaB = inv(Cb*CbT) * Cb* fb = RT* fb
  and  lambdaC = fc - CcT* lamdaB = fc+ minusCcT* lambdaB

*/

class stiffnessCondensationPETScWithDirectConstraints: public stiffnessCondensation{
	#if defined(HAVE_PETSC)
  protected:
		nlsDofManager* _pAssembler; // dof mananger
		linearConstraintMatrixPETSc* _bConMat, *_cConMat; // all linear constraint matrix
		pbcAlgorithm* _pAl; // pbc algorithm
		bool _tangentflag,_stressflag; // stress and tangent flag
		Mat _CcT, _CbT,_SbT, _ScT, _RT, _minusCcT; // all mat
		bool _isInitialized;
    MPI_Comm _comm; // Comunicator MPI

   public:
     stiffnessCondensationPETScWithDirectConstraints(nlsDofManager* p, pbcAlgorithm* ma,
                               bool sflag, bool tflag,
                               MPI_Comm com = PETSC_COMM_SELF);
     virtual ~stiffnessCondensationPETScWithDirectConstraints();
     virtual void init();
     virtual void clear();
     virtual void setSplittedDofSystem(pbcAlgorithm* pal);
     virtual void stressSolve(fullVector<double>& P, const double Vrve);
     virtual void tangentCondensationSolve(fullMatrix<double>& L, const double Vrve);

  #else
  public:
    stiffnessCondensationPETScWithDirectConstraints(nlsDofManager* p, pbcAlgorithm* ma,bool sflag, bool tflag){
      Msg::Error("Petsc is required");
    };
    virtual ~stiffnessCondensationPETScWithDirectConstraints(){};
    virtual void init(){};
    virtual void clear(){};
    virtual void setSplittedDofSystem(pbcAlgorithm* pal){};
    virtual void stressSolve(fullVector<double>& P, const double Vrve){};
    virtual void tangentCondensationSolve(fullMatrix<double>& L, const double Vrve){};
  #endif //HAVE_PETSC
};

/*
  Constraint bConMat  --> Cb* ub + Cc*uc = S*K
  --> ub = Cbc* uc+  Sbc*K

  Stress:
  P = SbcT*lambda/Volume

  From equilibrium state:
  fi = 0
  fb = lambda
  fc = -CbcT*lambda

*/


class stiffnessCondensationPETScWithoutDirectConstraints : public stiffnessCondensation{
  #if defined(HAVE_PETSC)
  protected:
		nlsDofManager* _pAssembler; // dof mananger
		linearConstraintMatrixPETSc* _bConMat; // all linear constraint matrix
		pbcAlgorithm* _pAl; // pbc algorithm
		bool _tangentflag,_stressflag; // stress and tangent flag
		Mat _Cbc, _Sbc; // all mat
		bool _isInitialized;
    MPI_Comm _comm; // Comunicator MPI

	public:
		stiffnessCondensationPETScWithoutDirectConstraints(nlsDofManager* p, pbcAlgorithm* ma,
                                          bool sflag, bool tflag, MPI_Comm com = PETSC_COMM_SELF);
		virtual ~stiffnessCondensationPETScWithoutDirectConstraints();

		virtual void init();
		virtual void clear();
		virtual void setSplittedDofSystem(pbcAlgorithm* pal);
		virtual void stressSolve(fullVector<double>& P, const double Vrve);
    virtual void tangentCondensationSolve(fullMatrix<double>& L, const double Vrve);

  #else
  public:
    stiffnessCondensationPETScWithoutDirectConstraints(nlsDofManager* p, pbcAlgorithm* ma,bool sflag, bool tflag){
      Msg::Error("Petsc is required");
    };
    virtual ~stiffnessCondensationPETScWithoutDirectConstraints(){};
    virtual void setSplittedDofSystem(pbcAlgorithm* pal){};
    virtual void stressSolve(fullVector<double>& P, const double Vrve){};
    virtual void tangentCondensationSolve(fullMatrix<double>& L, const double Vrve){};
  #endif
};

class stiffnessCondensationPETSc : public stiffnessCondensation{
  #if defined(HAVE_PETSC)
  protected:
		nlsDofManager* _pAssembler; // dof mananger
		linearConstraintMatrixPETSc* _bConMat; // all linear constraint matrix
		pbcAlgorithm* _pAl; // pbc algorithm
		bool _tangentflag,_stressflag; // stress and tangent flag
		bool _isInitialized;
    MPI_Comm _comm; // Comunicator MPI

    Mat _CT, _ST, _RT;

	public:
		stiffnessCondensationPETSc(nlsDofManager* p, pbcAlgorithm* ma,
                                          bool sflag, bool tflag, MPI_Comm com = PETSC_COMM_SELF);
		virtual ~stiffnessCondensationPETSc();

		virtual void init();
		virtual void clear();
		virtual void setSplittedDofSystem(pbcAlgorithm* pal);
		virtual void stressSolve(fullVector<double>& P, const double Vrve);
    virtual void tangentCondensationSolve(fullMatrix<double>& L, const double Vrve);

  #else
  public:
    stiffnessCondensationPETSc(nlsDofManager* p, pbcAlgorithm* ma,bool sflag, bool tflag){
      Msg::Error("Petsc is required");
    };
    virtual ~stiffnessCondensationPETSc(){};
    virtual void init(){};
    virtual void clear(){};
    virtual void setSplittedDofSystem(pbcAlgorithm* pal){};
    virtual void stressSolve(fullVector<double>& P, const double Vrve){};
    virtual void tangentCondensationSolve(fullMatrix<double>& L, const double Vrve){};
  #endif
};
#endif // EXTRACTMACROPROPERTIESPETSC_H_
