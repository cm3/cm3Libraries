//
//
// Description: class using fullMatrix for extraction of homogenized tangents
//
// Author:  <Van Dung NGUYEN>, (C) 2010
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "extractMacroPropertiesFullMatrix.h"
#include "linearSystemFull.h"
#include "fullMatrix.h"
#include "numericalFunctions.h"
#include "linearSystemGmm.h"

void GetMatSchur_naive(fullMatrix<double>* A11, fullMatrix<double>* A12,
                     fullMatrix<double>* A21, fullMatrix<double>* A22, fullMatrix<double>* &A){
  int M21, N21,M12, N12;
  M21 = A21->size1();
  N21 = A21->size2();
  M12 = A12->size1();
  N12 = A12->size2();
  if (A22) A = new fullMatrix<double>(*A22);
  else
    A = new fullMatrix<double>(M21,N12);
  fullMatrix<double>* invA11A12 =new fullMatrix<double>(M12,N12);

  linearSystem<double>* lsys = new linearSystemGmm<double>();
  static_cast<linearSystemGmm<double>*>(lsys)->setNoisy(2);
  lsys->allocate(M12);

   for (int i=0; i<N21; i++)
    for (int j=0; j<M12; j++){
      double val = A11->operator()(i,j);
      if (fabs(val)>0)
      lsys->addToMatrix(i,j,val);
    }


  for (int col=0; col<N12; col++){
    lsys->zeroRightHandSide();
    for (int row=0; row<M12; row++){
      double val = A12->operator()(row,col);
      lsys->addToRightHandSide(row,val);
    };
    // solving
    lsys->systemSolve();
    for (int row=0; row<M12; row++){
      double val;
      lsys->getFromSolution(row,val);
      invA11A12->operator()(row,col) = val;
    }
  };
  delete lsys;

  fullMatrix<double>* A21invA11A12 = new fullMatrix<double>(M21,N12);
  A21->mult(*invA11A12,*A21invA11A12);
  A21invA11A12->scale(-1.);
  A->operator += (*A21invA11A12);

  delete invA11A12;
  delete A21invA11A12;
};

void stiffnessCondensationFullMatrixWithDirectConstraints::setSplittedDofSystem(pbcAlgorithm* pal){
  pal->getSplittedDof()->setSplittedDofsSystem(pbcDofManager::INTERNAL,pbcDofManager::INDIRECT,pbcDofManager::DIRECT);
}


void stiffnessCondensationFullMatrixWithDirectConstraints::init(){
  if (!_isInitialized){
    _isInitialized = true;
    pbcDofManager* pbcAssembler = _pAl->getSplittedDof();
    if (_bConMat ) delete _bConMat;
    _bConMat= new linearConstraintMatrixFullMatrix();
    _pAl->computeIndirectLinearConstraintMatrix(_bConMat);

    if (_cConMat) delete _cConMat;
    _cConMat = new linearConstraintMatrixFullMatrix();
    _pAl->computeDirectLinearConstraintMatrix(_cConMat);

    fullMatrix<double>* Cb = _bConMat->getFirstMatrix();
    fullMatrix<double>* Cc = _bConMat->getSecondMatrix();
    fullMatrix<double>* Sb = _bConMat->getKinematicalMatrix();
    fullMatrix<double>* Sc = _cConMat->getKinematicalMatrix();

    _CbT = new fullMatrix<double>(Cb->transpose());
    _CcT = new fullMatrix<double>(Cc->transpose());
    _SbT = new fullMatrix<double>(Sb->transpose());
    _ScT = new fullMatrix<double>(Sc->transpose());
  }

};

void stiffnessCondensationFullMatrixWithDirectConstraints::clear(){
  if (_isInitialized){
    _isInitialized = false;
    delete _bConMat;
    delete _cConMat;
		delete _CbT;
		delete _SbT;
		delete _ScT;
		delete _RT;
		delete _CcT;
  }
};

void stiffnessCondensationFullMatrixWithDirectConstraints::stressSolve(fullVector<double>& P, const double Vrve){
  if (_stressflag) Msg::Info("Stress averaging is not implemented in stiffnessCondensationFullMatrix");
}

void stiffnessCondensationFullMatrixWithDirectConstraints::tangentCondensationSolve(fullMatrix<double>& tangentmat, const double Vrve){
  if (_tangentflag){
    int nK = _pAl->getNumberOfMacroToMicroKinematicVariables();
    tangentmat.resize(nK,nK);
    tangentmat.setAll(0.);

    if (!_isInitialized){
      this->init();
    }

    splitStiffnessMatrixFullMatrix*  _stiffness = dynamic_cast<splitStiffnessMatrixFullMatrix*>(_pAl->getSplitStiffness());

    fullMatrix<double>* Kii = _stiffness->getIIMat();
    fullMatrix<double>* Kib = _stiffness->getIBMat();
    fullMatrix<double>* Kic = _stiffness->getICMat();

    fullMatrix<double>* Kbi = _stiffness->getBIMat();
    fullMatrix<double>* Kbb = _stiffness->getBBMat();
    fullMatrix<double>* Kbc = _stiffness->getBCMat();

    fullMatrix<double>* Kci = _stiffness->getCIMat();
    fullMatrix<double>* Kcb = _stiffness->getCBMat();
    fullMatrix<double>* Kcc = _stiffness->getCCMat();

    GetMatSchur_naive(Kii,Kib,Kbi,Kbb,Kbbt);
    GetMatSchur_naive(Kii,Kic,Kbi,Kbc,Kbct);
    GetMatSchur_naive(Kii,Kic,Kci,Kcc,Kcct);
    Kcbt = new fullMatrix<double>(Kbct->transpose());

    GetMatSchur_naive(Kbbt,Kbct,Kcbt,Kcct,Kccstar);


    fullMatrix<double>* Cb = _bConMat->getFirstMatrix();
    fullMatrix<double>* Cc = _bConMat->getSecondMatrix();
    fullMatrix<double>* Sb = _bConMat->getKinematicalMatrix();
    fullMatrix<double>* Sc = _cConMat->getKinematicalMatrix();
    GetMatSchur_naive(Kbbt,_CbT,Cb,NULL,Kblamda);

    fullMatrix<double>* Cbc;
    GetMatSchur_naive(Kbbt,Kbct,Cb,Cc,Cbc);
    Cbc->scale(-1.);

    Sbc = new fullMatrix<double>(Sb->size1(),Sb->size2());
    Cbc->mult(*Sc,*Sbc);
    Sbc->operator+=(*Sb);
    SbcT = new fullMatrix<double>(Sbc->transpose());
    delete Cbc;

    GetMatSchur_naive(Kblamda,Sbc,SbcT,NULL, L);
    ScTKccstar = new fullMatrix<double>(_ScT->size1(),_ScT->size2());
    _ScT->mult(*Kccstar,*ScTKccstar);
    L1 = new fullMatrix<double>(Sc->size2(),Sc->size2());
    ScTKccstar->mult(*Sc,*L1);
    L->operator+=(*L1);

    tangentmat = (*L);
    double inv = 1./Vrve;
    tangentmat.scale(inv);

    delete Kbbt;
    delete Kbct;
    delete Kcbt;
    delete Kcct;
    delete Kblamda;
    delete Sbc;
    delete SbcT;
    delete ScTKccstar;
    delete Kccstar;
    delete L1;
    delete L;
  };
};

