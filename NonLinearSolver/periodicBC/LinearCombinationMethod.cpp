//
//
// Description: apply PBC by linear combination method
//
// Author:  <Van Dung NGUYEN>, (C) 2010
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "LinearCombinationMethod.h"

void linearCombination::findReceivedElement(MVertex* vP,elementContainer container,MElement* &ele){
  SPoint3 point=vP->point();
  for (elementContainer::iterator ite=container.begin(); ite!=container.end(); ite++){
    MElement* e=*ite;
    std::vector<MVertex*> lver;
    e->getVertices(lver);
    int num=lver.size();
    SPoint3 A=lver[0]->point();
    SPoint3 B=lver[1]->point();
    SPoint3 P=project(point,A,B);
    if (inside(P,A,B)) {
      ele=e;
      break;
    };
  };
};

void linearCombination::periodicCondition(MVertex* v, MElement* e){
  std::vector<MVertex*> list;
  e->getVertices(list);
  SPoint3 point=v->point();
  SPoint3 A=list[0]->point();
  SPoint3 B=list[1]->point();
  SPoint3 P=project(point,A,B);
  double uvw[3];
  double xyz[3]; xyz[0]=P.x();xyz[1]=P.y();xyz[2]=P.z();
  e->xyz2uvw(xyz,uvw);
  int num=e->getNumVertices();
  double vals[num];
  e->getShapeFunctions(uvw[0],uvw[1],uvw[2],vals);
  fullVector<double> Pos; Pos.resize(3);
  Pos(0)=v->x();Pos(1)=v->y(); Pos(2)=v->z();
  for (int i=0; i<num; i++){
    Pos(0)=Pos(0)-vals[i]*list[i]->x();
    Pos(1)=Pos(1)-vals[i]*list[i]->y();
    Pos(2)=Pos(2)-vals[i]*list[i]->z();
  };
  fullVector<double> g; g.resize(_ndofs);
  imposedDef.mult(Pos,g);

  std::vector<Dof> keys;
  getKeysFromVertex(v,keys);
  DofAffineConstraint<double> cons;
  int ndofs=keys.size();
  for (int i=0; i<ndofs; i++){
    for (int j=0;j<num; j++){
      std::vector<Dof> k;
      getKeysFromVertex(list[j],k);
      cons.linear.push_back(std::pair<Dof,double>(k[i],vals[j]));
    };
    cons.shift=g(i);
    p->setLinearConstraint(keys[i],cons);
    cons.linear.clear();
  };
};

void linearCombination::applyPeriodicCondition(){
  for (vertexContainer::iterator ite=gVertex[2].begin(); ite!=gVertex[2].end(); ite++){
    MVertex* v=*ite;
    MElement* ele=NULL;
    findReceivedElement(v,gElement[0],ele);
    if (ele){
      periodicCondition(v,ele);
    };
  };
  for (vertexContainer::iterator ite=gVertex[3].begin(); ite!=gVertex[3].end(); ite++){
    MVertex* v=*ite;
    MElement* ele=NULL;
    findReceivedElement(v,gElement[1],ele);
    if (ele){
      periodicCondition(v,ele);
    };
  };
};


void linearCombination3D::findReceivedElement(MVertex* vP, elementContainer container ,MElement* &ele){
  SPoint3 P=vP->point();
  for (elementContainer::iterator it=container.begin(); it!=container.end(); it++){
    MElement* e=*it;
    std::vector<MVertex*> vlist;
    e->getVertices(vlist);
    SPoint3 Pproject=project(P,vlist[0]->point(),vlist[1]->point(),vlist[2]->point());
    if (sameSide(Pproject,vlist[0]->point(),vlist[1]->point(),vlist[2]->point())
        &&sameSide(Pproject,vlist[1]->point(),vlist[0]->point(),vlist[2]->point())
        &&sameSide(Pproject,vlist[2]->point(),vlist[0]->point(),vlist[1]->point())){
      ele=e;
      break;
    };
  };
};


