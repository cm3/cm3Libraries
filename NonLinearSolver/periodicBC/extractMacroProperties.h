//
//
// Description: based class for extraction of homogenized tangents
//
// Author:  <Van Dung NGUYEN>, (C) 2010
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef EXTRACTMACROPROPERTIES_H_
#define EXTRACTMACROPROPERTIES_H_

#include "highOrderTensor.h"
#include "pbcAlgorithm.h"
#include "STensor53.h"
#include "STensor63.h"

#if defined(HAVE_PETSC)
#include "petscmat.h"
#include "petscvec.h"
#endif

class stiffnessCondensation{
	public:
    stiffnessCondensation(){};
		virtual ~stiffnessCondensation(){};
		virtual void init() = 0;
    virtual void clear() = 0;
		virtual void setSplittedDofSystem(pbcAlgorithm* pal) = 0;
		virtual void stressSolve(fullVector<double>& P, const double Vrve) = 0;
		virtual void tangentCondensationSolve(fullMatrix<double>& L, const double Vrve) = 0;
};

#endif // EXTRACTMACROPROPERTIES_H_
