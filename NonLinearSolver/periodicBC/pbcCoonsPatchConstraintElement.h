//
//
// Description: patch Coons interpolation elements
//
// Author:  <Van Dung NGUYEN>, (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef PBCCOONSPATCHCONSTRAINTELEMENT_H_
#define PBCCOONSPATCHCONSTRAINTELEMENT_H_

#include "pbcConstraintElement.h"

class CoonsPatchCubicSplineConstraintElement : public constraintElement{
  protected:
    MVertex* _vp, *_reference;
    MVertex* _vnx1, *_vnx2, *_vny1, *_vny2;
    int _flagx, _flagy;
    SVector3 _L;
		STensor3 _XiXj;
    double _xi, _neta, _x1, _x2, _y1, _y2;
    fullMatrix<double> _C, _S, _Cbc;
    fullVector<double> _FFx, _FFy;
		FunctionSpaceBase* _periodicSpace, *_multSpace;

  public:
    CoonsPatchCubicSplineConstraintElement(nonLinearMicroBC* mbc, FunctionSpaceBase* lagspace, FunctionSpaceBase* multspace,
																					const int c, MVertex* vp, MVertex* vre, MVertex* vx1, MVertex* vx2,
                                          MVertex* vy1, MVertex* vy2, int flagx, int flagy);
    virtual ~CoonsPatchCubicSplineConstraintElement(){};
    virtual void getConstraintKeys(std::vector<Dof>& key) const; // real dofs on constraint elements
    virtual void getMultiplierKeys(std::vector<Dof>& key) const;	// multiplier dof on constraint element
    virtual void getConstraintMatrix(fullMatrix<double>& m) const;		// matrix C
    virtual void getDependentMatrix(fullMatrix<double>& m) const;
    virtual void print() const;
    virtual void getDependentKeys(std::vector<Dof>& keys) const; // left real dofs
    virtual void getIndependentKeys(std::vector<Dof>& keys) const; // left real dofs
    virtual void getVirtualKeys(std::vector<Dof>& keys) const ; // for spline interpolation
    virtual void getLinearConstraints(const fullVector<double>& g,
                   std::map<Dof,DofAffineConstraint<double> >& con) const;
    virtual constraintElement::ElementType getType() const { return constraintElement::CubicSpline;};
    virtual bool isActive(const MVertex* v) const {
      if (_reference->getNum() == v->getNum() or _vp->getNum() == v->getNum() or
          _vnx1->getNum() == v->getNum()
          or _vnx2->getNum() == v->getNum() or _vny1->getNum() == _vny2->getNum()) return true;
      else return false;
    };

    virtual void getKinematicalVector(fullVector<double>& m) const;
		virtual void getKinematicalMatrix(fullMatrix<double>& m) const;
		virtual void getLinearConstraints(std::map<Dof,DofAffineConstraint<double> >& con) const;

};


class CoonsPatchLagrangeConstraintElement : public constraintElement{
  protected:
    MVertex* _vp, *_reference;
    std::vector<MVertex*> _vnx, _vny;  // sequence vertices
    SVector3 _L;
		STensor3 _XiXj;
    std::vector<double> _distanceX, _distanceY;
    double _x, _y;
    fullMatrix<double> _C, _S, _Cbc;
    fullVector<double> _FFx, _FFy;
    MVertex* _vrootP, *_vrootN;
		FunctionSpaceBase* _periodicSpace, *_multSpace;

  public:
    CoonsPatchLagrangeConstraintElement(nonLinearMicroBC* mbc, FunctionSpaceBase* lagspace, FunctionSpaceBase* multspace,
																				const int c, MVertex* v1, MVertex* vref,
																				std::vector<MVertex*>& xlist,std::vector<MVertex*>& ylist,
																				MVertex* vrootP = NULL, MVertex* vrootN = NULL);
    virtual ~CoonsPatchLagrangeConstraintElement(){};
    virtual void getConstraintKeys(std::vector<Dof>& key) const; // real dofs on constraint elements
    virtual void getMultiplierKeys(std::vector<Dof>& key) const;	// multiplier dof on constraint element
    virtual void getConstraintMatrix(fullMatrix<double>& m) const;		// matrix C
    virtual void getDependentMatrix(fullMatrix<double>& m) const;
    virtual void print() const;
    virtual void getDependentKeys(std::vector<Dof>& keys) const; // left real dofs
    virtual void getIndependentKeys(std::vector<Dof>& keys) const; // left real dofs
    virtual void getLinearConstraints(const fullVector<double>& g,
                   std::map<Dof,DofAffineConstraint<double> >& con) const;
    virtual void getLinearConstraintsByVertices(std::map<Dof,DofAffineConstraint<double> >& con) const;
    virtual constraintElement::ElementType getType() const { return constraintElement::Lagrange;};
    virtual bool isActive(const MVertex* v) const {
      if (_reference->getNum() == v->getNum()) return true;
      for (int i=0; i<_vnx.size();i++)
        if (_vnx[i]->getNum() == v->getNum()) return true;
      for (int i=0; i<_vny.size();i++)
        if (_vny[i]->getNum() == v->getNum()) return true;
      return false;
    };

    virtual void getKinematicalVector(fullVector<double>& m) const;
		virtual void getKinematicalMatrix(fullMatrix<double>& m) const;
		virtual void getLinearConstraints(std::map<Dof,DofAffineConstraint<double> >& con) const;
};

#endif // PBCCOONSPATCHCONSTRAINTELEMENT_H_
