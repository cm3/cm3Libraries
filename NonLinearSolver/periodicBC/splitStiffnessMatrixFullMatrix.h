//
//
// Description: store splitted stiffness matrix for condensation using fullMatrix
//
// Author:  <Van Dung NGUYEN>, (C) 2010
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef SPLITTEDSTIFFNESSMATRIXFULLMATRIX_H_
#define SPLITTEDSTIFFNESSMATRIXFULLMATRIX_H_

#include "splitStiffnessMatrix.h"
#include "fullMatrix.h"

class splitStiffnessMatrixFullMatrix : public splitStiffnessMatrix{
  protected:
    fullMatrix<double>* _aii, *_aib, *_aic,
                      *_abi, *_abb, *_abc,
                      *_aci, *_acb, *_acc;
    bool _isAllocated;
		int _ni, _nb, _nc;

  public:
    splitStiffnessMatrixFullMatrix();
    virtual ~splitStiffnessMatrixFullMatrix();
		virtual bool isAllocated() const;
		virtual void clear();
		virtual void allocateSubMatrix(int ni, int nb, int nc);
		virtual void zeroSubMatrix();
		virtual void insertInSparsityPattern_II(int row, int col){};
		virtual void insertInSparsityPattern_IB(int row, int col){};
		virtual void insertInSparsityPattern_IC(int row, int col){};
		virtual void insertInSparsityPattern_BI(int row, int col){};
		virtual void insertInSparsityPattern_BB(int row, int col){};
		virtual void insertInSparsityPattern_BC(int row, int col){};
		virtual void insertInSparsityPattern_CI(int row, int col){};
		virtual void insertInSparsityPattern_CB(int row, int col){};
		virtual void insertInSparsityPattern_CC(int row, int col){};
    virtual void preAllocateEntries(){};
		virtual void addToSubMatrix_II(int i, int j, const double &val);
		virtual void addToSubMatrix_IB(int i, int j, const double& val);
		virtual void addToSubMatrix_BI(int i, int j, const double& val);
    virtual void addToSubMatrix_BB(int i, int j, const double& val);
		virtual void addToSubMatrix_IC(int i, int j, const double& val);
		virtual void addToSubMatrix_BC(int i, int j, const double& val);
		virtual void addToSubMatrix_CI(int i, int j, const double& val);
		virtual void addToSubMatrix_CB(int i, int j, const double& val);
		virtual void addToSubMatrix_CC(int i, int j, const double& val);
		fullMatrix<double>* getIIMat();
		fullMatrix<double>* getIBMat();
		fullMatrix<double>* getBIMat();
		fullMatrix<double>* getBBMat();
		fullMatrix<double>* getICMat();
		fullMatrix<double>* getBCMat();
		fullMatrix<double>* getCIMat();
		fullMatrix<double>* getCBMat();
    fullMatrix<double>* getCCMat();

};

#endif // SPLITTEDSTIFFNESSMATRIXFULLMATRIX_H_
