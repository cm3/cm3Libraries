//
//
// Description: PBC constraint element base
//
// Author:  <Van Dung NGUYEN>, (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef PBCCONSTRAINTELEMENTBASE_H_
#define PBCCONSTRAINTELEMENTBASE_H_

#include "functionSpace.h"
#include "fullMatrix.h"
#include "MPoint.h"
#include "simpleFunctionTime.h"
#include "numericalFunctions.h"
#include "nonLinearMicroBC.h"
#include "ipField.h"

/*Interface for constraint element
    Constraint form C* u = S*K
    Constraint form ub = Cbc * uc + S*K
    K - tensor containing all macroscopic kinematic variables FM, GM, TM, gradTM, etc

  Notations:
    Left matrix       = C
    Right matrix      = S
    Right Vector  g   = S*K
    Dependent matrix  = Cbc
    Constraint keys   = u : all keys relate in constraint
    Dependent keys   = ub
    Independent keys = uc
    Virtual keys     = all keys supplement added to approximate constraint
    within this constraint (Spline, ...). they are not the usual dofs
*/

class constraintElement{
  public:
    enum ElementType{Periodic =0, CubicSpline=1, Lagrange=2, PeriodicNode = 3, Supplement=4, FE = 5,
										 DirectionalPeriodic = 6};
		static std::set<Dof> allPositiveDof;
		
		const nonLinearMicroBC* _mbc; // micro BC
		// all constraint is a part of this _mbc
		//
		int _ndofs; // total DOFs per vertex in this constraint element
		std::vector<int> _component;

		
	public:
		constraintElement(const nonLinearMicroBC* _mbc, const int c);
		virtual ~constraintElement(){};
		// get components in constraints
		const std::vector<int>& getComp() const {return _component;};
		
		/** get all keys lie on constraint**/
		virtual void getConstraintKeys(std::vector<Dof>& key) const = 0; //  dofs on constraint elements
		/** get multiplier keys**/
		virtual void getMultiplierKeys(std::vector<Dof>& key) const = 0;	// multiplier dof on constraint element
		/** get constraint matrix**/
		virtual void getConstraintMatrix(fullMatrix<double>& m) const =0;		// matrix C
		/** get depencent matrix**/
		virtual void getDependentMatrix(fullMatrix<double>& m) const = 0;
		/**dependent keys must be defined as unique dofs for each constraint **/
		virtual void getDependentKeys(std::vector<Dof>& keys) const =0; // left dofs
		/** remain keys are independent keys **/
    virtual void getIndependentKeys(std::vector<Dof>& keys) const = 0; // right dofs
    /** new keys add to the constraint, it can lie on dependent or indepent keys **/
    virtual void getVirtualKeys(std::vector<Dof>& keys) const {}; // for spline interpolation
    /**/
    virtual bool isDirect() const {return false;}
    /** get linear constraint by vertices**/
    virtual void getLinearConstraintsByVertices(std::map<Dof,DofAffineConstraint<double> >& con) const {
      Msg::Warning("getLinearConstraintsByVertices must be defined for this kind of constraint");
    };
    /**element type **/
		virtual constraintElement::ElementType getType() const =0; //  get element type
		/**print the constraint**/
		virtual void print() const{
      Msg::Info("Print the constraint ");
		};
		virtual bool isActive(const MVertex* v) const =0;

		// general function
		/** get kinematix vector**/
		virtual void getKinematicalVector(fullVector<double>& m) const = 0;	// matrix g
		/** get kinematrix matrix**/
		virtual void getKinematicalMatrix(fullMatrix<double>& m) const = 0;	// matrix S
		/** linear constraints are defined in terms of dependent and independent keys**/
		virtual void getLinearConstraints(std::map<Dof,DofAffineConstraint<double> >& con) const=0; // linear constraint
    /**modify constraint from internal data**/
    virtual bool update(const IPField* ip) {
      return false;
    }
};
#endif // PBCCONSTRAINTELEMENTBASE_H_
