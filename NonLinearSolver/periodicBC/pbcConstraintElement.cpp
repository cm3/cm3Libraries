//
//
// Description: PBC constraint elements
//
// Author:  <Van Dung NGUYEN>, (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "pbcConstraintElement.h"
#include "STensor33.h"
#include "SPoint3.h"

void periodicMeshConstraint::__init__(const double fact){
	std::vector<Dof> Keys;
	getKeysFromVertex(_periodicSpacePlus,_vp,getComp(),Keys);
	for (int ik=0; ik < Keys.size(); ik++){
		if (constraintElement::allPositiveDof.find(Keys[ik]) == constraintElement::allPositiveDof.end()){
			constraintElement::allPositiveDof.insert(Keys[ik]);
		}
		else{
			Msg::Warning("periodicMeshConstraint Dof on vertex was chosen as positive one in other constraint element:");
		}
	}

	if (_vn){
		_L(0) = _vp->x() - fact*_vn->x();
		_L(1) = _vp->y() - fact*_vn->y();
		_L(2) = _vp->z() - fact*_vn->z();
		
		if (_mbc->getOrder() == 2){
			SPoint3 positive(_vp->point());
			SPoint3 negative(_vn->point());
			for (int i=0; i<3; i++)
				for (int j=0; j<3; j++)
					_XX(i,j) = 0.5*(positive[i]*positive[j]-fact*negative[i]*negative[j]);
		}

		_C.resize(_ndofs,2*_ndofs);
		_C.setAll(0.0);
		_Cbc.resize(_ndofs,_ndofs);
		_Cbc.setAll(0.);
		for (int i=0; i<_ndofs; i++){
			_C(i,i) =1.0;
			_C(i,i+_ndofs) = -1.0*fact;
			_Cbc(i,i) = 1.;
		};
		_mbc->getPBCKinematicMatrix(_component,_L,_XX,_S);
	}
	else {
		_L(0) = _vp->x();
		_L(1) = _vp->y();
		_L(2) = _vp->z();
		
		if (_mbc->getOrder() == 2){
			SPoint3 positive(_vp->point());
			for (int i=0; i<3; i++)
				for (int j=0; j<3; j++)
					_XX(i,j) = 0.5*(positive[i]*positive[j]);
		}
		
		_C.resize(_ndofs,_ndofs);
		_C.setAll(0.0);
		for (int i=0; i<_ndofs; i++){
			_C(i,i) =1.0;
		};
		_mbc->getPointKinematicMatrix(_component,_L,_XX,_S);
	};
};

periodicMeshConstraint::periodicMeshConstraint(nonLinearMicroBC* mbc,FunctionSpaceBase* space,
													FunctionSpaceBase* multspace, const int c, MVertex* vp, MVertex* vn, MVertex* vrootP, MVertex* vrootN,
													const double fact):
												constraintElement(mbc,c), _periodicSpacePlus(space),_periodicSpaceMinus(space), _multSpace(multspace),
												_vp(vp),_vn(vn),_vrootP(vrootP),_vrootN(vrootN),
												_negativeFactor(fact){
	__init__(fact);
};

periodicMeshConstraint::periodicMeshConstraint(nonLinearMicroBC* mbc,FunctionSpaceBase* lagspacePlus, FunctionSpaceBase* lagspaceMinus,
													FunctionSpaceBase* multspace, const int c, MVertex* vp, MVertex* vn, MVertex* vrootP, MVertex* vrootN,
													const double fact):
												constraintElement(mbc,c), _periodicSpacePlus(lagspacePlus),_periodicSpaceMinus(lagspaceMinus), _multSpace(multspace),
												_vp(vp),_vn(vn),_vrootP(vrootP),_vrootN(vrootN),
												_negativeFactor(fact){
	__init__(fact);
};
void periodicMeshConstraint::print() const{
	if (_vn)
		printf("Postive = %ld, negative = %ld \n", _vp->getNum(), _vn->getNum());
	else{
		printf("Periodic point = %ld \n", _vp->getNum());
	}

	if (_vrootP!= NULL & _vrootN != NULL){
    printf("Positive root = %ld, negative root = %ld \n",_vrootP->getNum(),_vrootN->getNum());
	}
};
void periodicMeshConstraint::getConstraintKeys(std::vector<Dof>& key) const{
	getKeysFromVertex(_periodicSpacePlus,_vp, getComp(),key);
	if (_vn) getKeysFromVertex(_periodicSpaceMinus,_vn,getComp(),key);
};
void periodicMeshConstraint::getMultiplierKeys(std::vector<Dof>& key) const{
	getKeysFromVertex(_multSpace,_vp, getComp(),key);
};
void periodicMeshConstraint::getConstraintMatrix(fullMatrix<double>& m) const{
    m = _C;
};

void periodicMeshConstraint::getDependentMatrix(fullMatrix<double>& m) const{
  m = _Cbc;
};

void periodicMeshConstraint::getDependentKeys(std::vector<Dof>& key) const{
  getKeysFromVertex(_periodicSpacePlus,_vp, getComp(),key);
};

void periodicMeshConstraint::getIndependentKeys(std::vector<Dof>& key) const{
  if (_vn)
    getKeysFromVertex(_periodicSpaceMinus,_vn,getComp(),key);
}


void periodicMeshConstraint::getLinearConstraints(const fullVector<double>& g,
                   std::map<Dof,DofAffineConstraint<double> >& con) const
{
  std::vector<Dof> kP, kN;
  getKeysFromVertex(_periodicSpacePlus,_vp, getComp(),kP);
  if (_vn)
    getKeysFromVertex(_periodicSpaceMinus,_vn, getComp(), kN);
  DofAffineConstraint<double> cons;
  for (int i=0; i<_ndofs; i++){
    if (_vn) cons.linear.push_back(std::pair<Dof,double>(kN[i],_negativeFactor));
    cons.shift=g(i);
    con[kP[i]] = cons;
    cons.linear.clear();
  };
}

void periodicMeshConstraint::getLinearConstraintsByVertices(std::map<Dof,DofAffineConstraint<double> >& con) const{
  if (_vn == NULL){
    Msg::Error("this function is not used for periodicMeshConstraint::getLinearConstraintsByVertices");
    return;
  }

  if (_vrootP == NULL & _vrootN == NULL){
    Msg::Error("root vertices are not defined periodicMeshConstraint::getLinearConstraintsByVertices");
  }
  else{
    std::vector<Dof> kP, kN, krootP, krootN;
    getKeysFromVertex(_periodicSpacePlus,_vp, getComp(),kP);
    getKeysFromVertex(_periodicSpaceMinus,_vn, getComp(),kN);

    getKeysFromVertex(_periodicSpacePlus,_vrootP, getComp(),krootP);
    getKeysFromVertex(_periodicSpaceMinus,_vrootN, getComp(),krootN);

    DofAffineConstraint<double> cons;
    for (int i=0; i<_ndofs; i++){
      cons.linear.push_back(std::pair<Dof,double>(kN[i],_negativeFactor));
      cons.linear.push_back(std::pair<Dof,double>(krootP[i],1.0));
      cons.linear.push_back(std::pair<Dof,double>(krootN[i],-1.0*_negativeFactor));
      cons.shift=0.;
      con[kP[i]] = cons;
      cons.linear.clear();
    };
  };
};

void periodicMeshConstraint::getKinematicalVector(fullVector<double>& m) const{
	fullVector<double> kinVec;
	_mbc->getKinematicalVector(kinVec);
	m.resize(_ndofs);
	m.setAll(0.);
	_S.mult(kinVec,m);
};
void periodicMeshConstraint::getKinematicalMatrix(fullMatrix<double>& m) const{
  m = _S;
};
void periodicMeshConstraint::getLinearConstraints(std::map<Dof,DofAffineConstraint<double> >& con) const{
  fullVector<double> g;
  getKinematicalVector(g);
  getLinearConstraints(g, con);
};


cubicSplineConstraintElement::cubicSplineConstraintElement(nonLinearMicroBC* mbc, FunctionSpaceBase* space, FunctionSpaceBase* mspace,
																const int c, MVertex* v1, MVertex* v2, MVertex* v3, int flag):
										constraintElement(mbc, c),_periodicSpace(space),_multSpace(mspace),
										_vp(v1),_vn1(v2),_vn2(v3),_flag(flag){
    std::vector<Dof> Keys;
    getKeysFromVertex(_periodicSpace,_vp, getComp(), Keys);
    for (int ik=0; ik < Keys.size(); ik++){
      if (constraintElement::allPositiveDof.find(Keys[ik]) == constraintElement::allPositiveDof.end()){
        constraintElement::allPositiveDof.insert(Keys[ik]);
      }
      else{
        Msg::Warning("cubicSplineConstraintElement Dof on vertex was chosen as positive one in other constraint element:");
      }
    }

    SPoint3 pp = _vp->point();
    SPoint3 np1 = _vn1->point();
    SPoint3 np2 = _vn2->point();
    SPoint3 point = project(pp,np1,np2);

    _s1 = point.distance(np1);
    _s2 = point.distance(np2);
    _length = np1.distance(np2);
    // left matrix
    _C.resize(_ndofs,5*_ndofs); _C.setAll(0.0);
    int col = 0;
    for (int i=0; i<_ndofs; i++){
       _C(i,col) = 1.0;
       col++;
    };
   _Cbc.resize(_ndofs, 4*_ndofs);
   _Cbc.setAll(0.);
   cubicSplineConstraintElement::getFF(_s1,0,_length,_FF);

   for (int i=0; i<_FF.size(); i++){
      for (int j=0; j<_ndofs; j++){
          _C(j,col) = -1.*_FF(i);
          _Cbc(j,col-_ndofs)= _FF(i);
          col++;
      };
   };

   _L(0) = pp.x() - _FF(0)*np1.x() -_FF(2)*np2.x() ;
   _L(1) = pp.y() - _FF(0)*np1.y() -_FF(2)*np2.y() ;
   _L(2) = pp.z() - _FF(0)*np1.z() -_FF(2)*np2.z() ;
	 
	 if (_mbc->getOrder() == 2){
			SPoint3 positive = _vp->point();
			SPoint3 np1 = _vn1->point();
			SPoint3 np2 = _vn2->point();
			SPoint3 negative = project(positive,np1,np2);
			for (int i=0; i<3; i++){
				for (int j=0; j<3; j++){
					_XX(i,j) = 0.5*(positive[i]*positive[j] - negative[i]*negative[j]);
				}
			}
	 }
	 _mbc->getPBCKinematicMatrix(_component,_L,_XX,_S);
	
   if(cubicSplineConstraintElement::getShiftToCreateKeys()<3) Msg::Error(" The keys of the new degrees of freedom are not created with an unexisting value");
};

void cubicSplineConstraintElement::getConstraintKeys(std::vector<Dof>& keys) const{
	getKeysFromVertex(_periodicSpace,_vp, getComp(),keys);

	getKeysFromVertex(_periodicSpace,_vn1, getComp(),keys);
	cubicSplineConstraintElement::addKeysToVertex(_periodicSpace,_vn1, getComp(),keys,_flag);

	getKeysFromVertex(_periodicSpace,_vn2, getComp(),keys);
	cubicSplineConstraintElement::addKeysToVertex(_periodicSpace,_vn2, getComp(),keys,_flag);
};
void cubicSplineConstraintElement::getMultiplierKeys(std::vector<Dof>& keys) const{
	getKeysFromVertex(_multSpace,_vp, getComp(), keys);
};
void cubicSplineConstraintElement::getConstraintMatrix(fullMatrix<double>& m) const{
  m = _C;
};
void cubicSplineConstraintElement::getDependentMatrix(fullMatrix<double>& m) const{
  m = _Cbc;
};

void cubicSplineConstraintElement::print() const{
  printf("Cubic spline constraint element\n");
  printf("Positive = %ld\n",_vp->getNum());
  printf("Negative = %ld and %ld\n",_vn1->getNum(),_vn2->getNum());
};

void cubicSplineConstraintElement::getDependentKeys(std::vector<Dof>& key) const{
  getKeysFromVertex(_periodicSpace,_vp, getComp(),key);
}; // left real dofs
void cubicSplineConstraintElement::getIndependentKeys(std::vector<Dof>& keys) const{
  getKeysFromVertex(_periodicSpace,_vn1, getComp(),keys);
	cubicSplineConstraintElement::addKeysToVertex(_periodicSpace,_vn1, getComp(),keys,_flag);
	getKeysFromVertex(_periodicSpace,_vn2, getComp(),keys);
	cubicSplineConstraintElement::addKeysToVertex(_periodicSpace,_vn2, getComp(),keys,_flag);
};

void cubicSplineConstraintElement::getVirtualKeys(std::vector<Dof>& key) const{
  if ((_vn1) and (_vn2)){
    cubicSplineConstraintElement::addKeysToVertex(_periodicSpace,_vn1, getComp(),key,_flag);
    cubicSplineConstraintElement::addKeysToVertex(_periodicSpace,_vn2, getComp(),key,_flag);
  };
}


void cubicSplineConstraintElement::getLinearConstraints(const fullVector<double>& g,
                   std::map<Dof,DofAffineConstraint<double> >& con) const
{
  std::vector<Dof> k, k1, k2, keys1, keys2;
  getKeysFromVertex(_periodicSpace,_vp, getComp(),k);
  if ((_vn1) and (_vn2)){
    getKeysFromVertex(_periodicSpace,_vn1, getComp(),k1);
    getKeysFromVertex(_periodicSpace,_vn2, getComp(),k2);
    addKeysToVertex(_periodicSpace,_vn1, getComp(),keys1,_flag);
    addKeysToVertex(_periodicSpace,_vn2, getComp(),keys2,_flag);
  };
  DofAffineConstraint<double> cons;
  for (int i=0; i<_ndofs; i++){
    cons.shift=g(i);
    if ((_vn1) and (_vn2)){
      cons.linear.push_back(std::pair<Dof,double>(k1[i],_FF(0)));
      cons.linear.push_back(std::pair<Dof,double>(keys1[i],_FF(1)));
      cons.linear.push_back(std::pair<Dof,double>(k2[i],_FF(2)));
      cons.linear.push_back(std::pair<Dof,double>(keys2[i],_FF(3)));
    };
    con[k[i]] = cons;
    cons.linear.clear();
  };
};

void cubicSplineConstraintElement::getKinematicalVector(fullVector<double>& m) const{
	fullVector<double> kinVec;
	_mbc->getKinematicalVector(kinVec);
	m.resize(_ndofs);
	m.setAll(0.);
	_S.mult(kinVec,m);
};
void cubicSplineConstraintElement::getKinematicalMatrix(fullMatrix<double>& m) const{
  m = _S;
};
void cubicSplineConstraintElement::getLinearConstraints(std::map<Dof,DofAffineConstraint<double> >& con) const{
  fullVector<double> g;
  getKinematicalVector(g);
  getLinearConstraints(g, con);
};



lagrangeConstraintElement::lagrangeConstraintElement(nonLinearMicroBC* mbc, FunctionSpaceBase* space, FunctionSpaceBase* mspace, 
															const int c, MVertex* v1, std::vector<MVertex*>& vlist, MVertex* vrootP, MVertex* vrootN):
															constraintElement(mbc,c), _periodicSpace(space),_multSpace(mspace),
															_vp(v1),_vn(vlist), _vrootP(vrootP), _vrootN(vrootN){
    std::vector<Dof> Keys;
    getKeysFromVertex(_periodicSpace,_vp, getComp(),Keys);
    for (int ik=0; ik < Keys.size(); ik++){
      if (constraintElement::allPositiveDof.find(Keys[ik]) == constraintElement::allPositiveDof.end()){
        constraintElement::allPositiveDof.insert(Keys[ik]);
      }
      else{
        Msg::Warning("lagrangeConstraintElement Dof on vertex was chosen as positive one in other constraint element:");
      }
    }

    for (int i=0; i<vlist.size(); i++){
      double dist = _vn[i]->distance(_vn[0]);
      _distance.push_back(dist);
    };
    SPoint3 pp = _vp->point();
    SPoint3 np1 = _vn[0]->point();
    SPoint3 np2 = _vn[_vn.size()-1]->point();
    SPoint3 point = project(pp,np1,np2);
    _s = point.distance(np1);
    int size = _vn.size();
    _C.resize(_ndofs,(1+size)*_ndofs); _C.setAll(0.0);
    int col = 0;
    for (int i=0; i<_ndofs; i++){
        _C(i,col) = 1.0;
        col++;
    };
    lagrangeConstraintElement::getFF(_s,_distance,_FF);
    _Cbc.resize(_ndofs,size*_ndofs);
    _Cbc.setAll(0.);
    for (int i=0; i<_FF.size(); i++){
        for (int j=0; j<_ndofs; j++){
            _C(j,col) = -1.*_FF(i);
            _Cbc(j,col-_ndofs) = _FF(i);
            col++;
        };
    };
    for (int j=0; j<3; j++){
       _L[j] = pp[j];
       for (int i=0; i<_FF.size(); i++){
          SPoint3 pt = _vn[i]->point();
          _L[j] -= _FF(i)*pt[j];
       }
    }
		
		if (_mbc->getOrder() == 2){
			SPoint3 positive = _vp->point();
			SPoint3 np1 = _vn[0]->point();
			SPoint3 np2 = _vn[_vn.size()-1]->point();
			SPoint3 negative = project(positive,np1,np2);

			for (int i=0; i<3; i++)
				for (int j=0; j<3; j++)
					_XX(i,j) = 0.5*(positive[i]*positive[j]);

			for (int k=0; k<_FF.size(); k++){
				SPoint3 pt = _vn[k]->point();
				for (int i=0; i<3; i++)
					for (int j=0; j<3; j++)
						_XX(i,j) -= _FF(k)*0.5*(pt[i]*pt[j]);
			}
		}
		
		_mbc->getPBCKinematicMatrix(_component,_L,_XX,_S);
};

lagrangeConstraintElement::lagrangeConstraintElement(nonLinearMicroBC* mbc, FunctionSpaceBase* space, FunctionSpaceBase* mspace, 
															const int c, MVertex* v1, std::vector<MVertex*>& vlist, 
															const SVector3& dir): constraintElement(mbc,c), _periodicSpace(space),_multSpace(mspace),
															_vp(v1),_vn(vlist), _vrootP(NULL), _vrootN(NULL){
	std::vector<Dof> Keys;
	getKeysFromVertex(_periodicSpace,_vp, getComp(),Keys);
	for (int ik=0; ik < Keys.size(); ik++){
		if (constraintElement::allPositiveDof.find(Keys[ik]) == constraintElement::allPositiveDof.end()){
			constraintElement::allPositiveDof.insert(Keys[ik]);
		}
		else{
			Msg::Warning("lagrangeConstraintElement Dof on vertex was chosen as positive one in other constraint element:");
		}
	}

	for (int i=0; i<vlist.size(); i++){
		double dist = _vn[i]->distance(_vn[0]);
		_distance.push_back(dist);
	};
	SPoint3 pp = _vp->point();
	SPoint3 np1 = _vn[0]->point();
	SPoint3 np2 = _vn[_vn.size()-1]->point();
	SPoint3 point = planeDirProject(pp,np1,np2,dir);
	_s = point.distance(np1);
	int size = _vn.size();
	_C.resize(_ndofs,(1+size)*_ndofs); _C.setAll(0.0);
	int col = 0;
	for (int i=0; i<_ndofs; i++){
			_C(i,col) = 1.0;
			col++;
	};
	lagrangeConstraintElement::getFF(_s,_distance,_FF);
	_Cbc.resize(_ndofs,size*_ndofs);
	_Cbc.setAll(0.);
	for (int i=0; i<_FF.size(); i++){
			for (int j=0; j<_ndofs; j++){
					_C(j,col) = -1.*_FF(i);
					_Cbc(j,col-_ndofs) = _FF(i);
					col++;
			};
	};
	for (int j=0; j<3; j++){
		 _L[j] = pp[j];
		 for (int i=0; i<_FF.size(); i++){
				SPoint3 pt = _vn[i]->point();
				_L[j] -= _FF(i)*pt[j];
		 }
	}
	
	if (_mbc->getOrder() == 2){
		SPoint3 positive = _vp->point();
		SPoint3 np1 = _vn[0]->point();
		SPoint3 np2 = _vn[_vn.size()-1]->point();
		SPoint3 negative = project(positive,np1,np2);

		for (int i=0; i<3; i++)
			for (int j=0; j<3; j++)
				_XX(i,j) = 0.5*(positive[i]*positive[j]);

		for (int k=0; k<_FF.size(); k++){
			SPoint3 pt = _vn[k]->point();
			for (int i=0; i<3; i++)
				for (int j=0; j<3; j++)
					_XX(i,j) -= _FF(k)*0.5*(pt[i]*pt[j]);
		}
	}
	
	_mbc->getPBCKinematicMatrix(_component,_L,_XX,_S);
  
  //this->print();
  //Msg::Info("project point %d is %f %f %f",_vp->getNum(),point[0],point[1],point[2]);
};


void lagrangeConstraintElement::getConstraintKeys(std::vector<Dof>& keys) const{
	getKeysFromVertex(_periodicSpace,_vp, getComp(), keys);
	for (int i=0; i<_vn.size(); i++){
		getKeysFromVertex(_periodicSpace,_vn[i], getComp(), keys);
	};
};

void lagrangeConstraintElement::getMultiplierKeys(std::vector<Dof>& keys) const{
	getKeysFromVertex(_multSpace,_vp, getComp(), keys);
};

void lagrangeConstraintElement::getConstraintMatrix(fullMatrix<double>& m)const{
	m = _C;
};
void lagrangeConstraintElement::getDependentMatrix(fullMatrix<double>& m) const{
  m = _Cbc;
};

void lagrangeConstraintElement::print() const{
    printf("Lagrange constraint element \n");
    printf("Positive = %ld\n",_vp->getNum());
    for (int i=0; i<_vn.size(); i++){
      printf("%ld \t", _vn[i]->getNum());
    }
    printf("\n");
    if (_vrootP != NULL & _vrootN != NULL)
      printf("root positive %ld negative %ld \n",_vrootP->getNum(),_vrootN->getNum());
};



void lagrangeConstraintElement::getDependentKeys(std::vector<Dof>& keys) const{
  getKeysFromVertex(_periodicSpace,_vp, getComp(),keys);
}; // left real dofs

void lagrangeConstraintElement::getIndependentKeys(std::vector<Dof>& keys) const{
  for (int i=0; i<_vn.size(); i++){
		getKeysFromVertex(_periodicSpace,_vn[i], getComp(), keys);
	};
};

void lagrangeConstraintElement::getLinearConstraints(const fullVector<double>& g,
                   std::map<Dof,DofAffineConstraint<double> >& con) const
{
  std::vector<std::vector<Dof> > k;
  for (int i=0; i<_vn.size(); i++){
    std::vector<Dof> ktemp;
    getKeysFromVertex(_periodicSpace,_vn[i], getComp(),ktemp);
    k.push_back(ktemp);
  };

  std::vector<Dof> kp;
  getKeysFromVertex(_periodicSpace,_vp, getComp(),kp);

  DofAffineConstraint<double> cons;
  for (int i=0; i<_ndofs; i++){
    for (int j=0; j<_vn.size(); j++){
      cons.linear.push_back(std::pair<Dof,double>(k[j][i],_FF(j)));
    };
    cons.shift=g(i);
    con[kp[i]] = cons;
    cons.linear.clear();
  };
};

void lagrangeConstraintElement::getLinearConstraintsByVertices(std::map<Dof,DofAffineConstraint<double> >& con) const{
  if (_vrootN == NULL & _vrootP  == NULL) {
    Msg::Error("the root vertices are not defined lagrangeConstraintElement::getLinearConstraintsByVertices");
    return;
  }
  else{
    std::vector<std::vector<Dof> > k;
    for (int i=0; i<_vn.size(); i++){
      std::vector<Dof> ktemp;
      getKeysFromVertex(_periodicSpace,_vn[i], getComp(),ktemp);
      k.push_back(ktemp);
    };

    std::vector<Dof> kp;
    getKeysFromVertex(_periodicSpace,_vp, getComp(),kp);

    std::vector<Dof> krootP, krootN;
    getKeysFromVertex(_periodicSpace,_vrootP, getComp(),krootP);
    getKeysFromVertex(_periodicSpace,_vrootN, getComp(),krootN);


    DofAffineConstraint<double> cons;
    for (int i=0; i<_ndofs; i++){
      for (int j=0; j<_vn.size(); j++){
        cons.linear.push_back(std::pair<Dof,double>(k[j][i],_FF(j)));
      };
      cons.linear.push_back(std::pair<Dof,double>(krootP[i],1.0));
      cons.linear.push_back(std::pair<Dof,double>(krootN[i],-1.0));
      cons.shift=0.;
      con[kp[i]] = cons;
      cons.linear.clear();
    };
  }
};

void lagrangeConstraintElement::getKinematicalVector(fullVector<double>& m) const{
	fullVector<double> kinVec;
	_mbc->getKinematicalVector(kinVec);
	m.resize(_ndofs);
	m.setAll(0.);
	_S.mult(kinVec,m);
};
void lagrangeConstraintElement::getKinematicalMatrix(fullMatrix<double>& m) const{
  m = _S;
};
void lagrangeConstraintElement::getLinearConstraints(std::map<Dof,DofAffineConstraint<double> >& con) const{
  fullVector<double> g;
  getKinematicalVector(g);
  getLinearConstraints(g, con);
};

FEConstraintElement::FEConstraintElement(nonLinearMicroBC* mbc, FunctionSpaceBase* space, FunctionSpaceBase* mspace,
												const int c, MVertex* vp, MElement* en, MVertex* vrootP, MVertex* vrootN):
												constraintElement(mbc,c),_periodicSpace(space),_multSpace(mspace),
												_v(vp),_e(en), _vrootP(vrootP),_vrootN(vrootN){
  std::vector<Dof> Keys;
  getKeysFromVertex(_periodicSpace,_v, getComp(),Keys);
  for (int ik=0; ik < Keys.size(); ik++){
    if (constraintElement::allPositiveDof.find(Keys[ik]) == constraintElement::allPositiveDof.end()){
      constraintElement::allPositiveDof.insert(Keys[ik]);
    }
    else{
      Msg::Warning("FEConstraintElement Dof on vertex was chosen as positive one in other constraint element:");
    }
  }
  SPoint3 point;
  std::vector<MVertex*> vv;
  int size = _e->getNumVertices();
  _e->getVertices(vv);
  SPoint3 pp = _v->point();
  if (_e->getDim() == 1){
    point = project(pp,vv[0]->point(),vv[1]->point());
  }
  else if (_e->getDim() == 2){
    point = project(pp,vv[0]->point(),vv[1]->point(),vv[2]->point());
  };

  double xyz[3]={point[0],point[1],point[2]};
  double uvw[3];
  _e->xyz2uvw(xyz,uvw);

  _fVal = new double[size];
  _e->getShapeFunctions(uvw[0],uvw[1],uvw[2],_fVal);


  _C.resize(_ndofs,(1+size)*_ndofs); _C.setAll(0.0);
  int col = 0;
  for (int i=0; i<_ndofs; i++)
  {
      _C(i,col) = 1.0;
      col++;
  };
  _Cbc.resize(_ndofs,size*_ndofs);
  _Cbc.setAll(0.);
  
  for (int i=0; i<size; i++){
    for (int j=0; j<_ndofs; j++){
        _C(j,col) = -1.*_fVal[i];
        _Cbc(j,col-_ndofs) = _fVal[i];
        col++;
    };
  };

	for (int i=0; i<3; i++){
    _L[i] = pp[i];
    for (int j=0; j<size; j++){
      SPoint3 pt = vv[j]->point();
      _L[i] -= (_fVal[j]*pt[i]);
    }
  }
	
	if (_mbc->getOrder() == 2){
		SPoint3 positive = _v->point();
		std::vector<MVertex*> vv;
		_e->getVertices(vv);
		int size = vv.size();

		for (int i=0; i<3; i++)
			for (int j=0; j<3; j++)
				_XX(i,j) = 0.5*(positive[i]*positive[j]);

		for (int k=0; k<size; k++){
			SPoint3 pt = vv[k]->point();
			for (int i=0; i<3; i++)
				for (int j=0; j<3; j++)
					_XX(i,j) -= _fVal[k]*0.5*(pt[i]*pt[j]);
		}
	}
  
	_mbc->getPBCKinematicMatrix(_component,_L,_XX,_S);

};
FEConstraintElement::~FEConstraintElement(){};

void FEConstraintElement::getConstraintKeys(std::vector<Dof>& keys) const{
	getKeysFromVertex(_periodicSpace,_v, getComp(), keys);
  for (int i=0; i< _e->getNumVertices(); i++){
		getKeysFromVertex(_periodicSpace,_e->getVertex(i),getComp(),keys);
	}
};

void FEConstraintElement::getMultiplierKeys(std::vector<Dof>& keys) const{
	getKeysFromVertex(_multSpace,_v,getComp(), keys);
};

void FEConstraintElement::getConstraintMatrix(fullMatrix<double>& m)const{
	m = _C;
};
void FEConstraintElement::getDependentMatrix(fullMatrix<double>& m) const{
  m = _Cbc;
};
void FEConstraintElement::print() const{
  printf("FE constraint element \n");
  printf("Positive = %ld\n",_v->getNum());
  printf("projected element = %ld\n",_e->getNum());

  if (_vrootP != NULL & _vrootN != NULL)
    printf("root positive %ld negative %ld \n",_vrootP->getNum(),_vrootN->getNum());
};

void FEConstraintElement::getDependentKeys(std::vector<Dof>& keys) const{
  getKeysFromVertex(_periodicSpace,_v,getComp(),keys);
}; // left real dofs

void FEConstraintElement::getIndependentKeys(std::vector<Dof>& keys) const{
	for (int i=0; i< _e->getNumVertices(); i++){
		getKeysFromVertex(_periodicSpace,_e->getVertex(i),getComp(),keys);
	}
};


void FEConstraintElement::getLinearConstraints(const fullVector<double>& g,
                   std::map<Dof,DofAffineConstraint<double> >& con) const
{
  std::vector<std::vector<Dof> > k;
  for (int i=0; i<_e->getNumVertices(); i++){
    std::vector<Dof> ktemp;
    getKeysFromVertex(_periodicSpace,_e->getVertex(i),getComp(),ktemp);
    k.push_back(ktemp);
  };

  std::vector<Dof> kp;
  getKeysFromVertex(_periodicSpace,_v,getComp(),kp);

  DofAffineConstraint<double> cons;
  for (int i=0; i<_ndofs; i++){
    for (int j=0; j<_e->getNumVertices(); j++){
      cons.linear.push_back(std::pair<Dof,double>(k[j][i],_fVal[j]));
    };
    cons.shift=g(i);
    con[kp[i]] = cons;
    cons.linear.clear();
  };
};

void FEConstraintElement::getLinearConstraintsByVertices(std::map<Dof,DofAffineConstraint<double> >& con) const{
  if (_vrootN == NULL & _vrootP  == NULL) {
    Msg::Error("the root vertices are not defined FEConstraintElement::getLinearConstraintsByVertices");
    return;
  }
  else{
    std::vector<std::vector<Dof> > k;
    for (int i=0; i<_e->getNumVertices(); i++){
      std::vector<Dof> ktemp;
      getKeysFromVertex(_periodicSpace,_e->getVertex(i),getComp(),ktemp);
      k.push_back(ktemp);
    };

    std::vector<Dof> kp;
    getKeysFromVertex(_periodicSpace,_v,getComp(),kp);

    std::vector<Dof> krootP, krootN;
    getKeysFromVertex(_periodicSpace,_vrootP,getComp(),krootP);
    getKeysFromVertex(_periodicSpace,_vrootN,getComp(),krootN);

    DofAffineConstraint<double> cons;
    for (int i=0; i<_ndofs; i++){
      for (int j=0; j<_e->getNumVertices(); j++){
        cons.linear.push_back(std::pair<Dof,double>(k[j][i],_fVal[j]));
      };

      cons.linear.push_back(std::pair<Dof,double>(krootP[i],1.0));
      cons.linear.push_back(std::pair<Dof,double>(krootN[i],-1.0));
      cons.shift=0.;
      con[kp[i]] = cons;
      cons.linear.clear();
    };
  }
};

void FEConstraintElement::getKinematicalVector(fullVector<double>& m) const{
	fullVector<double> kinVec;
	_mbc->getKinematicalVector(kinVec);
	m.resize(_ndofs);
	m.setAll(0.);
	_S.mult(kinVec,m);
};
void FEConstraintElement::getKinematicalMatrix(fullMatrix<double>& m) const{
  m = _S;
};
void FEConstraintElement::getLinearConstraints(std::map<Dof,DofAffineConstraint<double> >& con) const{
  fullVector<double> g;
  getKinematicalVector(g);
  getLinearConstraints(g, con);
};

