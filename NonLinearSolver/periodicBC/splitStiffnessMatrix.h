//
//
// Description: store splitted stiffness matrix for condensation
//
// Author:  <Van Dung NGUYEN>, (C) 2010
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef SPLITTEDSTIFFNESSMATRIX_H_
#define SPLITTEDSTIFFNESSMATRIX_H_

/*
This class saves three arbitrary parts by splitting the glogal stiffnes matrix
I - B - C is three arbitrary group dofs which gives the parts:

K  == [KII KIB KIC
       KBI KBB KBC
       KCI KCB KCC]
*/

class splitStiffnessMatrix {
	public:
		virtual ~splitStiffnessMatrix(){};
		virtual void clear() = 0;
		virtual bool isAllocated() const =0;
		virtual void allocateSubMatrix(int ni, int nb, int nc )  = 0;
		virtual void zeroSubMatrix() = 0;
		// for preallocating
		virtual void insertInSparsityPattern_II(int row, int col) = 0;
		virtual void insertInSparsityPattern_IB(int row, int col) = 0;
		virtual void insertInSparsityPattern_IC(int row, int col) = 0;
		virtual void insertInSparsityPattern_BI(int row, int col) = 0;
		virtual void insertInSparsityPattern_BB(int row, int col) = 0;
		virtual void insertInSparsityPattern_BC(int row, int col) = 0;
		virtual void insertInSparsityPattern_CI(int row, int col) = 0;
		virtual void insertInSparsityPattern_CB(int row, int col) = 0;
		virtual void insertInSparsityPattern_CC(int row, int col) = 0;
		virtual void preAllocateEntries() = 0;
		// for filling values
		virtual void addToSubMatrix_II(int row, int col, const double& val) = 0;
		virtual void addToSubMatrix_IB(int row, int col, const double& val) = 0;
		virtual void addToSubMatrix_BI(int row, int col, const double& val) = 0;
		virtual void addToSubMatrix_BB(int row, int col, const double& val) = 0;
		virtual void addToSubMatrix_IC(int row, int col, const double& val) =0;
		virtual void addToSubMatrix_BC(int row, int col, const double& val) =0;
		virtual void addToSubMatrix_CI(int row, int col, const double& val) = 0;
		virtual void addToSubMatrix_CB(int row, int col, const double& val) =0;
		virtual void addToSubMatrix_CC(int row, int col, const double& val) =0;
};

#endif // SPLITTEDSTIFFNESSMATRIX_H_
