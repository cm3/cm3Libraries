//
//
// Description: class to store linear constraint matrix using PETSC
//
// Author:  <Van Dung NGUYEN>, (C) 2012
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef LINEARCONSTRAINTMATRIXPETSC_H_
#define LINEARCONSTRAINTMATRIXPETSC_H_

#include "linearConstraintMatrix.h"
#include "GmshConfig.h"
#include "GmshMessage.h"
#include "sparsityPattern.h"

#if defined(HAVE_PETSC)
#include "petscksp.h"
#include "petscmat.h"
#endif

class linearConstraintMatrixPETSc : public linearConstraintMatrix{
	#if defined(HAVE_PETSC)
	protected:
		void _try(int ierr) { CHKERRABORT(_comm, ierr); };

	protected:
		Mat _firstMatrix, _secondMatrix, _thirdMatrix ;
		Mat _kinMatrix; // first matrix, second matrix, _firstorder matrix
		bool _isAllocated;  // allocated flag
		PetscInt _row, _firstCol, _secondCol, _thirdCol; // respectively row, col of first matrix, col of second matrix
		PetscInt _numKinVars;
		MPI_Comm _comm; // MPI comunicator

		sparsityPattern _sparsityFirst;
		bool _entriesPreAllocatedFirst;
		bool _NotAssembledFirst;

		bool _NotAssembledSecond;
		bool _NotAssembledThird;
		bool _NotAssembledKin;

	public:
		linearConstraintMatrixPETSc(MPI_Comm com = PETSC_COMM_SELF);
		virtual ~linearConstraintMatrixPETSc();
		virtual void clear();
		virtual bool isAllocated() const{return _isAllocated;};
		virtual void allocate(int row, int firstcol, int secondcol, int thirdCol, int numKinVar);
		virtual void addToFirstMatrix(int i, int j, double val);
		virtual void addToSecondMatrix(int i, int j, double val);
		virtual void addToThirdMatrix(int i, int j, double val);
    virtual void addToKinematicalMatrix(int i, int j, double val);
    virtual void zeroAllMatrices();

    virtual void insertInSparsityPattern_FirstMatrix(int _row, int _col);
		virtual void preAllocateEntries_FirstMatrix();
		//
		virtual void insertInSparsityPattern_SecondMatrix(int _row, int _col);
		virtual void preAllocateEntries_SecondMatrix();
		//
		virtual void insertInSparsityPattern_ThirdMatrix(int _row, int _col);
		virtual void preAllocateEntries_ThirdMatrix();

		Mat& getFirstMatrix();
		Mat& getSecondMatrix();
		Mat& getThirdMatrix();
		Mat& getKinematicalMatrix();
	#else
	public:
		linearConstraintMatrixPETSc(){ Msg::Error("Petsc is not available!");};
		virtual void clear(){};
		virtual bool isAllocated() const {return false;};
		virtual void allocate(int row, int first, int second, int third, int kinVar) {};
		virtual void addToFirstMatrix(int i, int j, double val){};
		virtual void addToSecondMatrix(int i, int j, double val){};
		virtual void addToThirdMatrix(int i, int j, double val){};
    virtual void addToKinematicalMatrix(int i, int j, double val){};
    virtual void zeroAllMatrices(){};

    virtual void insertInSparsityPattern_FirstMatrix(int _row, int _col){};
		virtual void preAllocateEntries_FirstMatrix(){};
		//
		virtual void insertInSparsityPattern_SecondMatrix(int _row, int _col){};
		virtual void preAllocateEntries_SecondMatrix(){};
    //
		virtual void insertInSparsityPattern_ThirdMatrix(int _row, int _col){};
		virtual void preAllocateEntries_ThirdMatrix(){};

	#endif
};
#endif // LINEARCONSTRAINTMATRIXPETSC_H_
