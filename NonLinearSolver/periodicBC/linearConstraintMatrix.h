//
//
// Description: based class to store linear constraint matrix
//
// Author:  <Van Dung NGUYEN>, (C) 2010
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef LINEARCONSTRAINTMATRIX_H_
#define LINEARCONSTRAINTMATRIX_H_

// interface for linear constraint matrix
// ------------------------------
// For the constraint matrix C_d u_d + C_i u_i + C_c u_c = S*K
// First-matrix == C_d
// Second matrix == C_i
// Third matrix == C_c
// Kinematic matrix = S
//-------------------------
// ------------------------------
// For the constraint matrix C_b u_b+ C_c u_c = S*K
// First-matrix == C_b
// Second matrix == C_c
// Kinematic matrix = S
//-------------------------
// For the constraint matrix u_b = C_bc u_c + S*K
// First matrix == C_bc,
// Kinematic matrix = S
//----------------------------------
// For the constraint matrix  ub = Cu* + S*K + Muc
// First matrix == C
// Second matrix == M
// Kinematic matrix = S
// --------------------------------
// For the constraint matrix u_b = S*K
// Kinematic matrix = S

class linearConstraintMatrix{
	public:
		virtual ~linearConstraintMatrix(){};
		virtual void allocate(int row, int firstcol, int secondcol, int thirdCol, int numKinVar) =0;
		virtual bool isAllocated() const = 0;
		virtual void clear() = 0;
		// first matrix
		virtual void addToFirstMatrix(int i, int j, double val)  = 0;
		// second matrix
		virtual void addToSecondMatrix(int i, int j, double val) = 0;
		// second matrix
		virtual void addToThirdMatrix(int i, int j, double val) = 0;
		// first order kinematical matrix
		virtual void addToKinematicalMatrix(int i, int j, double val) = 0;
		// zero all matrix
		virtual void zeroAllMatrices() = 0;
		// for
		virtual void insertInSparsityPattern_FirstMatrix(int _row, int _col) = 0;
		virtual void preAllocateEntries_FirstMatrix() = 0;
		//
		virtual void insertInSparsityPattern_SecondMatrix(int _row, int _col) = 0;
		virtual void preAllocateEntries_SecondMatrix() = 0;
		//
		virtual void insertInSparsityPattern_ThirdMatrix(int _row, int _col) = 0;
		virtual void preAllocateEntries_ThirdMatrix() = 0;

};

#endif // LINEARCONSTRAINTMATRIX_H_
