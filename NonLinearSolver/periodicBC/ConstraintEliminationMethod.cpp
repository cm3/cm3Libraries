//
//
// Description: apply PBC by constraint elimination method
//
// Author:  <Van Dung NGUYEN>, (C) 2010
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "ConstraintEliminationMethod.h"

void constraintElimination::periodicCondition(){
  int dim=gVertex.size()/2;
  for (int i=0; i<dim; i++){
    for (vertexContainer::iterator it=gVertex[i+dim].begin(); it!=gVertex[i+dim].end(); it++){
      MVertex* vP=*it;
      if (coinVertex.find(vP)==coinVertex.end()){
        MVertex* vN=findNearestVertex(vP,gVertex[i]);
        periodicConditionBase(vP,vN);
      };
    };
  };
};

void constraintElimination::applyPeriodicCondition(){
  periodicCondition();
  cornerConstraint();
};


