//
//
// Description: class to store linear constraint matrix using fullMatrix
//
// Author:  <Van Dung NGUYEN>, (C) 2012
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef LINEARCONSTRAINTMATRIXFULLMATRIX_H_
#define LINEARCONSTRAINTMATRIXFULLMATRIX_H_

#include "linearConstraintMatrix.h"
#include "fullMatrix.h"

// full matrix interface
class linearConstraintMatrixFullMatrix : public linearConstraintMatrix{
	protected:
		fullMatrix<double>* _firstMatrix, *_secondMatrix, *_kinMatrix;
		bool _isAllocated;
		int _row, _firstCol, _secondCol;
		int _numKinVars;

	public:
		linearConstraintMatrixFullMatrix();
		virtual ~linearConstraintMatrixFullMatrix();
		virtual void clear();
		virtual bool isAllocated() const {return _isAllocated;};
		virtual void allocate(int row, int first, int second, int third, int numKin);
		virtual void addToFirstMatrix(int i, int j, double val);
		virtual void addToSecondMatrix(int i, int j, double val);
		virtual void addToThirdMatrix(int i, int j, double val) {Msg::Error("this function is not defined linearConstraintMatrixFullMatrix");}
		virtual void addToKinematicalMatrix(int i, int j, double val);
		virtual void zeroAllMatrices();
		virtual void insertInSparsityPattern_FirstMatrix(int _row, int _col) {};
		virtual void preAllocateEntries_FirstMatrix() {};
		//
		virtual void insertInSparsityPattern_SecondMatrix(int _row, int _col) {};
		virtual void preAllocateEntries_SecondMatrix() {};
		//
		virtual void insertInSparsityPattern_ThirdMatrix(int _row, int _col) {};
		virtual void preAllocateEntries_ThirdMatrix(){};

		fullMatrix<double>* getFirstMatrix();
		fullMatrix<double>* getSecondMatrix();
		fullMatrix<double>* getKinematicalMatrix();
};

#endif // LINEARCONSTRAINTMATRIXFULLMATRIX_H_
