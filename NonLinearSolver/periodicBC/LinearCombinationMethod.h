//
//
// Description: apply PBC by linear combination method
//
// Author:  <Van Dung NGUYEN>, (C) 2010
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef LINEARCOMBINATIONMETHOD_H_
#define LINEARCOMBINATIONMETHOD_H_

#include "periodicConstraints.h"

class linearCombination : public periodicConstraintsBase{

  protected:
    virtual void findReceivedElement(MVertex* vP, elementContainer container,MElement* &ele);
    void periodicCondition(MVertex* v, MElement* e);

  public:
    linearCombination(const int tag, const int ndofs, const int dim, nlsDofManager* _p, std::vector<partDomain*>& allDom)
                      : periodicConstraintsBase(tag,ndofs,dim,_p,allDom){};
    virtual ~linearCombination(){};

    virtual void applyPeriodicCondition();
};

class linearCombination3D : public linearCombination{

  protected:
    virtual void findReceivedElement(MVertex* vP, elementContainer container, MElement* &ele);

  public:
    linearCombination3D(const int tag, const int ndofs, const int dim, nlsDofManager* _p, std::vector<partDomain*>& allDom)
                        :linearCombination(tag,ndofs, dim,_p,allDom){};
    virtual ~linearCombination3D(){};

};

#endif // LINEARCOMBINATIONMETHOD_H_
