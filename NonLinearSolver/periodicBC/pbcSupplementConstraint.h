//
//
// Description: supplement constraint elements
//
// Author:  <Van Dung NGUYEN>, (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef PBCSUPPLEMENTCONSTRAINT_H_
#define PBCSUPPLEMENTCONSTRAINT_H_

#include "pbcConstraintElement.h"
#include "quadratureRules.h"

/**
class interface is used to impose the mean displacement on a group of elements
**/


class elementGroupDomainDecomposition;
class supplementConstraintBase : public constraintElement{
	protected:
		FunctionSpaceBase* _periodicSpace, *_multSpace;
	public:
		supplementConstraintBase(nonLinearMicroBC* mbc, FunctionSpaceBase* space, FunctionSpaceBase* mspace,const int c): 
										constraintElement(mbc,c),_periodicSpace(space),_multSpace(mspace){};
    virtual ~supplementConstraintBase(){};
    virtual void getConstraintKeys(std::vector<Dof>& key) const = 0; //  dofs on constraint elements
    virtual void getMultiplierKeys(std::vector<Dof>& key) const = 0;	// multiplier dof on constraint element
    virtual void getConstraintMatrix(fullMatrix<double>& m) const =0;		// matrix C
    virtual void getDependentMatrix(fullMatrix<double>& m) const = 0;
    virtual void getDependentKeys(std::vector<Dof>& keys) const =0; // left dofs
    virtual void getIndependentKeys(std::vector<Dof>& keys) const = 0; // right dofs
    virtual void getVirtualKeys(std::vector<Dof>& keys) const {}; // for spline interpolation
    virtual constraintElement::ElementType getType() const =0; //  get element type
    virtual void print() const{
      Msg::Info("Print the supplement constraint ");
    };
    virtual double getConstraintFactor() const {return 0;};
    virtual std::vector<MVertex*>& getVertexList() = 0;
    virtual fullVector<double>& getWeight() = 0;
    virtual bool isActive(const MVertex* v) const =0;

    virtual void getKinematicalVector(fullVector<double>& m) const = 0;
		virtual void getKinematicalMatrix(fullMatrix<double>& m) const = 0;
		virtual void getLinearConstraints(std::map<Dof,DofAffineConstraint<double> >& con) const = 0;
};

class scalarWeightFunction {
  public:
    scalarWeightFunction(){}
    scalarWeightFunction(const scalarWeightFunction& src){}
    virtual ~scalarWeightFunction(){}
    virtual double getVal(MVertex* v) const = 0;
    virtual double getVal(double x, double y, double z) const = 0;
    virtual scalarWeightFunction* clone() const = 0;
    virtual void print() const {};
};

class constantScalarWeightFunction : public scalarWeightFunction{
  protected:
    double _val;

  public:
    constantScalarWeightFunction(const double val):scalarWeightFunction(),_val(val){}
    constantScalarWeightFunction(const constantScalarWeightFunction& src):scalarWeightFunction(src),
        _val(src._val){}
    virtual ~constantScalarWeightFunction(){}
    virtual double getVal(MVertex* v) const {return _val;};
    virtual double getVal(double x, double y, double z) const {return _val;};
    virtual scalarWeightFunction* clone() const {return new constantScalarWeightFunction(*this);};
};

class singlePositionScalarWeightFunction: public scalarWeightFunction{
  protected:
    int _comp;
  public:
    singlePositionScalarWeightFunction(const int comp):scalarWeightFunction(),_comp(comp){}
    singlePositionScalarWeightFunction(const singlePositionScalarWeightFunction& src):scalarWeightFunction(src),
                _comp(src._comp){}
    virtual ~singlePositionScalarWeightFunction(){}
    virtual double getVal(MVertex* v) const {
      if (_comp == 0) return v->x();
      else if (_comp == 1) return v->y();
      else if (_comp == 2) return v->z();
      else{
        Msg::Error("component %d is not defined",_comp);
        return 0.;
      }

    };
    virtual double getVal(double x, double y, double z) const {
      if (_comp == 0) return x;
      else if (_comp == 1) return y;
      else if (_comp == 2) return z;
      else{
        Msg::Error("component %d is not defined",_comp);
        return 0.;
      }
    };
    virtual scalarWeightFunction* clone() const {return new singlePositionScalarWeightFunction(*this);};

    virtual void print() const {Msg::Info("weight on comp %d",_comp);};

};


class supplementConstraint : public supplementConstraintBase{
	public:
		static void computeWeight(const elementGroup*g, std::vector<MVertex*>& v, fullVector<double>& weight);
		
  protected:
    elementGroup* _g;
    fullMatrix<double>  _C, _S, _Cbc; // constraint matrix
    SVector3 _Xmean; // int_S{X} dA
		STensor3 _XXmean; // int_S{0.5*X*X} dA
    std::vector<MVertex*> _v; // all Vertex
    fullVector<double> _weight; //
    int _positive; // positive position
    MVertex* _tempVertex;
    double _factor;
    scalarWeightFunction* _weightFunction; // weightFunction

  public:
    supplementConstraint(nonLinearMicroBC* Mbc, FunctionSpaceBase* space, FunctionSpaceBase* mspace,
												const int c,elementGroup* g, const scalarWeightFunction* func=NULL);
    virtual ~supplementConstraint(){
      if (_weightFunction != NULL)  delete _weightFunction; _weightFunction = NULL;
      if (_tempVertex) delete _tempVertex;
    }
    virtual void getConstraintKeys(std::vector<Dof>& key) const ; // real dofs on constraint elements
    virtual void getMultiplierKeys(std::vector<Dof>& key) const ;	// multiplier dof on constraint element
    virtual void getConstraintMatrix(fullMatrix<double>& m) const ;		// matrix C
    virtual void getDependentMatrix(fullMatrix<double>& m) const;
    virtual void getDependentKeys(std::vector<Dof>& keys) const; // left real dofs
    virtual void getIndependentKeys(std::vector<Dof>& keys) const; // for spline interpolation
    virtual void getLinearConstraints(const fullVector<double>& g,
                   std::map<Dof,DofAffineConstraint<double> >& con) const;
    virtual constraintElement::ElementType getType() const{return constraintElement::Supplement;};
    virtual void print() const;
    virtual double getConstraintFactor() const {return _factor;};
    virtual std::vector<MVertex*>& getVertexList() {return _v;};
    virtual fullVector<double>& getWeight() {return _weight;};
    virtual bool isActive(const MVertex* v) const {
      for (int i=0; i<_v.size(); i++){
        if (_v[i]->getNum() == v->getNum()) return true;
      }
      return false;
    };

    virtual void getKinematicalVector(fullVector<double>& m) const;
		virtual void getKinematicalMatrix(fullMatrix<double>& m) const;
		virtual void getLinearConstraints(std::map<Dof,DofAffineConstraint<double> >& con) const;
};

class periodicSupplementConstraint : public supplementConstraintBase{
	protected:
    elementGroup* _gPlus, *_gMinus;
    fullMatrix<double>  _C, _S, _Cbc; // constraint matrix
    std::vector<MVertex*> _v; // all Vertex
    fullVector<double> _weight;
    int _positive; // positive position
    SVector3 _Xmean; // int_S{X} dA
		STensor3 _XXmean; // int_S{0.5*X*X} dA
    MVertex* _tempVertex;
    
	public:
    periodicSupplementConstraint(nonLinearMicroBC* Mbc, FunctionSpaceBase* space, FunctionSpaceBase* mspace,
																	const int c, elementGroup* gplus, elementGroup* gMinus);
    virtual ~periodicSupplementConstraint(){
      if (_tempVertex) delete _tempVertex;
    }
    virtual void getConstraintKeys(std::vector<Dof>& key) const ; // real dofs on constraint elements
    virtual void getMultiplierKeys(std::vector<Dof>& key) const ;	// multiplier dof on constraint element
    virtual void getConstraintMatrix(fullMatrix<double>& m) const ;		// matrix C
    virtual void getDependentMatrix(fullMatrix<double>& m) const;
    virtual void getDependentKeys(std::vector<Dof>& keys) const; // left real dofs
    virtual void getIndependentKeys(std::vector<Dof>& keys) const; // for spline interpolation
    virtual void getLinearConstraints(const fullVector<double>& g,
                   std::map<Dof,DofAffineConstraint<double> >& con) const;
    virtual constraintElement::ElementType getType() const{return constraintElement::Supplement;};
    virtual void print() const;
    
    virtual std::vector<MVertex*>& getVertexList() {return _v;};
    virtual fullVector<double>& getWeight() {return _weight;};
    
    virtual bool isActive(const MVertex* v) const {
      for (int i=0; i<_v.size(); i++){
        if (_v[i]->getNum() == v->getNum()) return true;
      }
      return false;
    };

    virtual void getKinematicalVector(fullVector<double>& m) const;
		virtual void getKinematicalMatrix(fullMatrix<double>& m) const;
		virtual void getLinearConstraints(std::map<Dof,DofAffineConstraint<double> >& con) const;
};

class lagrangeSupplementConstraint : public supplementConstraintBase{
  protected:
    std::vector<MVertex*>& _v; // all Vertex
    fullVector<double> _weight; //
    fullMatrix<double>  _C, _S, _Cbc; // constraint matrix
    int _positive;
    MVertex* _tempVertex;
    SVector3 _Xmean;
		STensor3 _XXmean; // int_S{0.5*X*X} dA
    double _factor;

	public:
    void integrateFF(const std::vector<double>& base, fullVector<double>& integ);

  public:
    lagrangeSupplementConstraint(nonLinearMicroBC*, FunctionSpaceBase* space, FunctionSpaceBase* mspace, 
												const int c, std::vector<MVertex*>& v);
    virtual ~lagrangeSupplementConstraint(){
      if (_tempVertex) delete _tempVertex;
    };
    virtual void getConstraintKeys(std::vector<Dof>& key) const ; // real dofs on constraint elements
    virtual void getMultiplierKeys(std::vector<Dof>& key) const ;	// multiplier dof on constraint element
    virtual void getConstraintMatrix(fullMatrix<double>& m) const ;		// matrix C
    virtual void getDependentMatrix(fullMatrix<double>& m) const;
    virtual void getDependentKeys(std::vector<Dof>& keys) const; // left real dofs
    virtual void getIndependentKeys(std::vector<Dof>& keys) const; // for spline interpolation
    virtual void getLinearConstraints(const fullVector<double>& g,
                   std::map<Dof,DofAffineConstraint<double> >& con) const;
    virtual constraintElement::ElementType getType() const{return constraintElement::Supplement;};
    virtual void print() const;
    virtual double getConstraintFactor() const {return _factor;};
    virtual std::vector<MVertex*>& getVertexList() {return _v;};
    virtual fullVector<double>& getWeight() {return _weight;};
    virtual bool isActive(const MVertex* v) const {
      for (int i=0; i<_v.size(); i++){
        if (_v[i]->getNum() == v->getNum()) return true;
      }
      return false;
    };

    virtual void getKinematicalVector(fullVector<double>& m) const;
		virtual void getKinematicalMatrix(fullMatrix<double>& m) const;
		virtual void getLinearConstraints(std::map<Dof,DofAffineConstraint<double> >& con) const;

};

class cubicSplineSupplementConstraint : public supplementConstraintBase{
   protected:
    std::vector<MVertex*>& _v; // all Vertex
    fullVector<double> _weight; //
    fullMatrix<double>  _C, _S, _Cbc; // constraint matrix
    int _positive;
    MVertex* _tempVertex;
    SVector3 _Xmean;
		STensor3 _XXmean; // int_S{0.5*X*X} dA
    int _flag;
    double _factor;

	protected:
    void integrateFF(double s1, double s2, fullVector<double>& integval) const{
      integval.resize(4);
      double L = s2 - s1;
      integval(0) = L/2.;
      integval(1) = L*L/12.;
      integval(2) = L/2.;
      integval(3) = L*L/12.;
    }

  public:
    cubicSplineSupplementConstraint(nonLinearMicroBC* mbc, FunctionSpaceBase* space, FunctionSpaceBase* mspace, 
												const int c, std::vector<MVertex*>& v, int flag = 0);
    virtual ~cubicSplineSupplementConstraint(){
      if (_tempVertex) delete _tempVertex;
    };
    virtual void getConstraintKeys(std::vector<Dof>& key) const; // real dofs on constraint elements
    virtual void getMultiplierKeys(std::vector<Dof>& key) const;	// multiplier dof on constraint element
    virtual void getConstraintMatrix(fullMatrix<double>& m) const;		// matrix C
    virtual void getDependentMatrix(fullMatrix<double>& m) const;
    virtual void getDependentKeys(std::vector<Dof>& keys) const; // left real dofs
    virtual void getIndependentKeys(std::vector<Dof>& keys) const; // for spline interpolation
    virtual void getLinearConstraints(const fullVector<double>& g,
                   std::map<Dof,DofAffineConstraint<double> >& con) const;
    virtual constraintElement::ElementType getType() const{return constraintElement::Supplement;};
    virtual void print() const{};
    virtual double getConstraintFactor() const {return _factor;};
    virtual std::vector<MVertex*>& getVertexList() {return _v;};
    virtual fullVector<double>& getWeight() {return _weight;};
    virtual bool isActive(const MVertex* v) const {
      for (int i=0; i<_v.size(); i++){
        if (_v[i]->getNum() == v->getNum()) return true;
      }
      return false;
    };

    virtual void getKinematicalVector(fullVector<double>& m) const;
		virtual void getKinematicalMatrix(fullMatrix<double>& m) const;
		virtual void getLinearConstraints(std::map<Dof,DofAffineConstraint<double> >& con) const;
};

class CoonsPatchLagrangeSupplementConstraint : public supplementConstraintBase{
  protected:
    std::vector<MVertex*> _v;
    MVertex* _vroot;
    fullMatrix<double>  _C, _S, _Cbc; // constraint matrix
    MVertex* _tempVertex;
    fullVector<double> _weight;
    int _positive;
    double _factor;
    SVector3 _Xmean;
		STensor3 _XXmean; // int_S{0.5*X*X} dA

  public:
    CoonsPatchLagrangeSupplementConstraint(nonLinearMicroBC* mbc, FunctionSpaceBase* space, FunctionSpaceBase* mspace,
																					const int c, lagrangeSupplementConstraint* conX,
                                           lagrangeSupplementConstraint* conY);
    virtual ~CoonsPatchLagrangeSupplementConstraint(){
      if (_tempVertex) delete _tempVertex;
    };
    virtual void getConstraintKeys(std::vector<Dof>& key) const; // real dofs on constraint elements
    virtual void getMultiplierKeys(std::vector<Dof>& key) const;	// multiplier dof on constraint element
    virtual void getConstraintMatrix(fullMatrix<double>& m) const;		// matrix C
    virtual void getDependentMatrix(fullMatrix<double>& m) const;
    virtual void getDependentKeys(std::vector<Dof>& keys) const; // left real dofs
    virtual void getIndependentKeys(std::vector<Dof>& keys) const; // for spline interpolation
    virtual void getLinearConstraints(const fullVector<double>& g,
                   std::map<Dof,DofAffineConstraint<double> >& con) const;
    virtual constraintElement::ElementType getType() const{return constraintElement::Supplement;};
    virtual void print() const{};
    virtual std::vector<MVertex*>& getVertexList() { return _v;};
    virtual fullVector<double>& getWeight() {return _weight;};
    virtual bool isActive(const MVertex* v) const {
      for (int i=0; i<_v.size(); i++){
        if (_v[i]->getNum() == v->getNum()) return true;
      }
      return false;
    };

    virtual void getKinematicalVector(fullVector<double>& m) const;
		virtual void getKinematicalMatrix(fullMatrix<double>& m) const;
		virtual void getLinearConstraints(std::map<Dof,DofAffineConstraint<double> >& con) const;
};


class supplementConstraintGeneral : public constraintElement{
	protected:
		elementGroupDomainDecomposition* _g;
    fullMatrix<double>  _C, _S, _Cbc; // constraint matrix
		std::vector<Dof> _allDof;
		int _positive;
		MVertex* _tempVertex;
		FunctionSpaceBase* _multSpace;

  public:
    supplementConstraintGeneral(nonLinearMicroBC* Mbc, FunctionSpaceBase* mspace, const int c, elementGroupDomainDecomposition* gr, 
																			const scalarWeightFunction* func=NULL);
    virtual ~supplementConstraintGeneral(){
			if (_tempVertex!= NULL) delete _tempVertex;
			_tempVertex = NULL;
		}
    virtual void getConstraintKeys(std::vector<Dof>& key) const ; // real dofs on constraint elements
    virtual void getMultiplierKeys(std::vector<Dof>& key) const ;	// multiplier dof on constraint element
    virtual void getConstraintMatrix(fullMatrix<double>& m) const ;		// matrix C
    virtual void getDependentMatrix(fullMatrix<double>& m) const;
    virtual void getDependentKeys(std::vector<Dof>& keys) const; // left real dofs
    virtual void getIndependentKeys(std::vector<Dof>& keys) const; // for spline interpolation
    virtual void getLinearConstraints(const fullVector<double>& g,
                   std::map<Dof,DofAffineConstraint<double> >& con) const;
    virtual constraintElement::ElementType getType() const{return constraintElement::Supplement;};
    virtual void print() const;

    virtual bool isActive(const MVertex* v) const;
		
    virtual void getKinematicalVector(fullVector<double>& m) const;
		virtual void getKinematicalMatrix(fullMatrix<double>& m) const;
		virtual void getLinearConstraints(std::map<Dof,DofAffineConstraint<double> >& con) const;
};


#endif // PBCSUPPLEMENTCONSTRAINT_H_
