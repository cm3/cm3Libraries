//
//
// Description: class to store linear constraint matrix using PETSC
//
// Author:  <Van Dung NGUYEN>, (C) 2012
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "linearConstraintMatrixPETSc.h"

#if defined(HAVE_PETSC)
#include "linearSystemPETSc.hpp"

linearConstraintMatrixPETSc::linearConstraintMatrixPETSc(MPI_Comm com):
                    _comm(com),_isAllocated(false),_row(0),_firstCol(0),_secondCol(0),
                    _thirdCol(0),
                    _numKinVars(0){
  _entriesPreAllocatedFirst = false;
  _NotAssembledFirst = false;

  _NotAssembledSecond = false;
  _NotAssembledThird = false;
  _NotAssembledKin = false;
};

linearConstraintMatrixPETSc::~linearConstraintMatrixPETSc(){
  this->clear();
};


void linearConstraintMatrixPETSc::clear(){
  if (_isAllocated){
    _isAllocated= false;
    if (_firstCol>0){
      _try(MatDestroy(&_firstMatrix));
      _entriesPreAllocatedFirst = false;
      _NotAssembledFirst = false;
    }
    if (_secondCol>0){
      _try(MatDestroy(&_secondMatrix));
      _NotAssembledSecond = false;
    }
    if (_thirdCol>0){
      _try(MatDestroy(&_thirdMatrix));
      _NotAssembledThird = false;
    }
    if (_numKinVars>0){
      _try(MatDestroy(&_kinMatrix));
      _NotAssembledKin = false;
    }
  };
};

void linearConstraintMatrixPETSc::allocate(int row, int first, int second, int third, int numKin){
  _row = row;
  _firstCol = first;
  _secondCol = second;
  _thirdCol = third;
  _numKinVars = numKin;

  if (!_isAllocated){
    _isAllocated = true;
    if (_firstCol >0){
      _try(MatCreate(_comm,&_firstMatrix));
      _try(MatSetSizes(_firstMatrix,_row, _firstCol,PETSC_DETERMINE,PETSC_DETERMINE));
      _try(MatSetType(_firstMatrix,MATSEQAIJ));
      _try(MatSetFromOptions(_firstMatrix));
    }
    if (_secondCol>0){
      _try(MatCreate(_comm,&_secondMatrix));
      _try(MatSetSizes(_secondMatrix,_row,_secondCol,PETSC_DETERMINE,PETSC_DETERMINE));
      _try(MatSetType(_secondMatrix,MATDENSE));
      _try(MatSetFromOptions(_secondMatrix));
      _try(MatSetUp(_secondMatrix));
    };
    if (_thirdCol>0){
      _try(MatCreate(_comm,&_thirdMatrix));
      _try(MatSetSizes(_thirdMatrix,_row,_thirdCol,PETSC_DETERMINE,PETSC_DETERMINE));
      _try(MatSetType(_thirdMatrix,MATDENSE));
      _try(MatSetFromOptions(_thirdMatrix));
      _try(MatSetUp(_thirdMatrix));
    };

    if (_numKinVars>0){
      _try(MatCreate(_comm,&_kinMatrix));
      _try(MatSetSizes(_kinMatrix,_row,_numKinVars,PETSC_DETERMINE,PETSC_DETERMINE));
      _try(MatSetType(_kinMatrix,MATDENSE));
      _try(MatSetFromOptions(_kinMatrix));
      _try(MatSetUp(_kinMatrix));
    }
  }
  else{
    Msg::Warning("linearConstraintMatrixPETSc::allocate, call clear before reallocated these matrices");
  }
}

void linearConstraintMatrixPETSc::addToFirstMatrix(int i, int j, double val){
  if (_firstCol <=0) {
    Msg::Error("addToFirstMatrix is not valid ");
  };
  if (!_entriesPreAllocatedFirst){
    preAllocateEntries_FirstMatrix();
  }
  PetscInt ii = i, jj = j;
  PetscScalar s = val;
  _try(MatSetValues(_firstMatrix,1,&ii,1,&jj,&s,ADD_VALUES));
  _NotAssembledFirst = true;
}

void linearConstraintMatrixPETSc::addToSecondMatrix(int i, int j, double val){
  if (_secondCol <=0) {
    Msg::Error("addToSecondMatrix is not valid ");
  };
  PetscInt ii = i, jj = j;
  PetscScalar s = val;
  _try(MatSetValues(_secondMatrix,1,&ii,1,&jj,&s,ADD_VALUES));
  _NotAssembledSecond = true;
}

void linearConstraintMatrixPETSc::addToThirdMatrix(int i, int j, double val){
  if (_thirdCol<=0){
    Msg::Error("addToThirdMatrix is not valid");
  }
  PetscInt ii = i, jj = j;
  PetscScalar s = val;
  _try(MatSetValues(_thirdMatrix,1,&ii,1,&jj,&s,ADD_VALUES));
  _NotAssembledThird = true;
};

void linearConstraintMatrixPETSc::addToKinematicalMatrix(int i, int j, double val){
  if (_numKinVars<=0) {
    Msg::Error("addToKinematicalMatrix is not valid");
  };
  PetscInt ii = i, jj = j;
  PetscScalar s = val;
  _try(MatSetValues(_kinMatrix,1,&ii,1,&jj,&s,ADD_VALUES));
  _NotAssembledKin = true;
}

void linearConstraintMatrixPETSc::zeroAllMatrices(){
  if ( !_isAllocated ) return;
  if (_firstCol >0 and _entriesPreAllocatedFirst){
    if (_NotAssembledFirst){
      _try(MatAssemblyBegin(_firstMatrix,MAT_FINAL_ASSEMBLY));
      _try(MatAssemblyEnd(_firstMatrix,MAT_FINAL_ASSEMBLY));
      _NotAssembledFirst= false;
    }
    _try(MatZeroEntries(_firstMatrix));
  }

  if (_secondCol>0){
    if (_NotAssembledSecond){
      _try(MatAssemblyBegin(_secondMatrix,MAT_FINAL_ASSEMBLY));
      _try(MatAssemblyEnd(_secondMatrix,MAT_FINAL_ASSEMBLY));
      _NotAssembledSecond = false;
    }
    _try(MatZeroEntries(_secondMatrix));
  }
  if (_thirdCol>0 ){
    if (_NotAssembledThird){
      _try(MatAssemblyBegin(_thirdMatrix,MAT_FINAL_ASSEMBLY));
      _try(MatAssemblyEnd(_thirdMatrix,MAT_FINAL_ASSEMBLY));
      _NotAssembledThird = false;
    }
    _try(MatZeroEntries(_thirdMatrix));
  }

  if (_numKinVars>0){
    if (_NotAssembledKin){
      _try(MatAssemblyBegin(_kinMatrix,MAT_FINAL_ASSEMBLY));
      _try(MatAssemblyEnd(_kinMatrix,MAT_FINAL_ASSEMBLY));
      _NotAssembledKin = false;
    }
    _try(MatZeroEntries(_kinMatrix));
  }

};

void linearConstraintMatrixPETSc::insertInSparsityPattern_FirstMatrix(int r, int c){
  _sparsityFirst.insertEntry(r,c);
};
void linearConstraintMatrixPETSc::preAllocateEntries_FirstMatrix(){
  if (_firstCol> 0){
    if (_entriesPreAllocatedFirst) return;
    if (!_isAllocated) Msg::Error("system must be allocated first");
    std::vector<int> nByRowDiag (_row);
    if (_sparsityFirst.getNbRows() == 0) {
      PetscInt prealloc = 500;
      PetscBool set;
      PetscOptionsGetInt(PETSC_NULL, "-petsc_prealloc", &prealloc, &set);
      prealloc = std::min(prealloc, _firstCol);
      nByRowDiag.resize(0);
      nByRowDiag.resize(_row, prealloc);
    }
    else {

      for (int i = 0; i < _row; i++) {
        int n;
        const int *r = _sparsityFirst.getRow(i, n);
        for (int j = 0; j < n; j++) {
          if (r[j] >= 0 && r[j] < _firstCol)
            nByRowDiag[i] ++;
        }
      }
      _sparsityFirst.clear();
    }
    _try(MatSeqAIJSetPreallocation(_firstMatrix, 0,  &nByRowDiag[0]));
    _entriesPreAllocatedFirst = true;
  };
};

void linearConstraintMatrixPETSc::insertInSparsityPattern_SecondMatrix(int row, int col){
};
void linearConstraintMatrixPETSc::preAllocateEntries_SecondMatrix(){

};

void linearConstraintMatrixPETSc::insertInSparsityPattern_ThirdMatrix(int row, int col){
};
void linearConstraintMatrixPETSc::preAllocateEntries_ThirdMatrix(){

};


Mat& linearConstraintMatrixPETSc::getFirstMatrix(){
  if (_firstCol>0 ){
    if (!_entriesPreAllocatedFirst)
      preAllocateEntries_FirstMatrix();
    _try(MatAssemblyBegin(_firstMatrix,MAT_FINAL_ASSEMBLY));
    _try(MatAssemblyEnd(_firstMatrix,MAT_FINAL_ASSEMBLY));
  }
  return _firstMatrix;
};
Mat& linearConstraintMatrixPETSc::getSecondMatrix(){
  if (_secondCol >0){
    _try(MatAssemblyBegin(_secondMatrix,MAT_FINAL_ASSEMBLY));
    _try(MatAssemblyEnd(_secondMatrix,MAT_FINAL_ASSEMBLY));
  }
  return _secondMatrix;
};
Mat& linearConstraintMatrixPETSc::getThirdMatrix(){
  if (_thirdCol >0 ){
    _try(MatAssemblyBegin(_thirdMatrix,MAT_FINAL_ASSEMBLY));
    _try(MatAssemblyEnd(_thirdMatrix,MAT_FINAL_ASSEMBLY));
  }
  return _thirdMatrix;
};
Mat& linearConstraintMatrixPETSc::getKinematicalMatrix(){
  if (_numKinVars>0){
    _try(MatAssemblyBegin(_kinMatrix,MAT_FINAL_ASSEMBLY));
    _try(MatAssemblyEnd(_kinMatrix,MAT_FINAL_ASSEMBLY));
  }
  return _kinMatrix;
};



#endif //HAVE_PETSC
