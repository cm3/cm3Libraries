//
//
// Description: PBC algorithm
//
// Author:  <Van Dung NGUYEN>, (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "pbcAlgorithm.h"
#include "pbcDofManager.h"
#include "numericalFunctions.h"
#include "staticDofManager.h"
#include "GmshConfig.h"
#include "nonLinearMechSolver.h"
#include "pbcSystems.h"

void pbcAlgorithm::__int__(){
  // create function space within microscopic boundary condition
	const std::vector<partDomain*>& allDomain = *(_solver->getDomainVector());
	bool fullDG = false;
  for (int idom = 0; idom< allDomain.size(); idom++){
    if (allDomain[idom]->getFormulation()){
      fullDG = true;
			if (allDomain[idom]->elementGroupSize() > 0) Msg::Error("The pbcAlgorithm is only implemented for interDomain");
      break;
    }
  }

	if (!fullDG){
		_space= allDomain[0]->getFunctionSpace();
		int lagtag = _space->getId()+1; // to make sure lagtag and microtag are different
	// create mspace from space
		_mspace = _space->clone(lagtag);
		if (_mspace == NULL) Msg::Error("FunctionSpaceBase::clone must be defined for the function space considering in domain %d",allDomain[0]->getPhysical());
  }
  else{
    _space = allDomain[0]->getFunctionSpace();
    int maxTag = 0;
    partDomain* oneDom = NULL;
		for (int i=0; i< allDomain.size(); i++)
    {
      if (allDomain[i]->elementGroupSize()>0)
      {
        if (maxTag< allDomain[i]->getFunctionSpace()->getId()){
          maxTag = allDomain[i]->getFunctionSpace()->getId();
          oneDom = allDomain[i];
        }
      }
		}
		maxTag ++; // to create a diffrent id for multspace
    _mspace = oneDom->getFunctionSpace()->clone(maxTag);
		if (_mspace == NULL) Msg::Error("FunctionSpaceBase::clone must be defined for the function space considering in domain %d",oneDom->getPhysical());
  };
	// lagtag is maximal tag of all space


  // create PBC group constraint
  _pbcConstraint  = new pbcConstraintElementGroup(_solver,_space,_mspace);
  // create constraint elements within pbcConstraintElementGroup
	const std::vector<elementGroup*>& gr = _solver->getMicroBC()->getBoundaryElementGroup();
	if (gr.size() >0){
		_pbcConstraint->createConstraintGroup();
	}
	else{
		Msg::Error("physicals for micro BCs are not correctly defined");
	}
  #ifdef _DEBUG
  _pbcConstraint->print();
  #endif

  Msg::Info("size of virtual vertices = %d",_pbcConstraint->sizeVirtualVertices());

  //length scale for multiplier problem
  const elementGroup* g = _pbcConstraint->getAllBulkElement();
  if (g->size()>0){
    MElement* ele = (g->begin()->second);
    double volume = ele->getVolume();
		int dim = ele->getDim();
    double length = volume;
    if (volume <= 0) Msg::Error(" negative element volume");
    else {
      if (dim ==2){
        length = sqrt(volume);
      }
      else if (dim ==3){
        length = pow(volume,1.0/3.0);
      };
    };
    _scale = allDomain[0]->getMaterialLaw()->scaleFactor()*length;
	}
	//#ifdef _DEBUG
	printf("scale = %f\n",_scale);
	//#endif
};

pbcAlgorithm::pbcAlgorithm(nonLinearMechSolver* solver): _solver(solver), _space(NULL),_mspace(NULL),
                  _pbcConstraint(NULL),_splittedDofs(NULL),
									_isSplitted(false),_scale(1.), _isModifiedConstraintMatrix(true){
  this->__int__();
};

pbcAlgorithm::~pbcAlgorithm(){
  if (_pbcConstraint) delete _pbcConstraint;
  if (_splittedDofs) delete _splittedDofs;
  if (_mspace) delete _mspace;
  _isSplitted = false;
};

nonLinearMicroBC* pbcAlgorithm::getMBC(){
	return _solver->getMicroBC();
};

int pbcAlgorithm::getSystemDofNumber(const Dof& key) {return _solver->getDofManager()->getDofNumber(key);};
int pbcAlgorithm::getNumberOfMacroToMicroKinematicVariables() const {return _solver->getMicroBC()->getNumberOfMacroToMicroKinematicVariables();};
int pbcAlgorithm::getNumberMicroToMacroHomogenizedVariables() const{
	// get maximal stress dimension from domain
	const std::vector<partDomain*>& allDomain = *(_solver->getDomainVector());
	int dimStress = 0;
	for (int i=0; i< allDomain.size(); i++){
		if (allDomain[i]->getStressDimension() > dimStress){
			dimStress = allDomain[i]->getStressDimension();
		}
	}
	return dimStress;
}

void pbcAlgorithm::updateConstraint(const IPField* ipfield){
  for (std::set<constraintElement*>::iterator it= _pbcConstraint->constraintGroupBegin();
                                                it!= _pbcConstraint->constraintGroupEnd(); it++){
      constraintElement* cele = *it;
      bool isModif = cele->update(ipfield);
      if (isModif) {
				_isModifiedConstraintMatrix = true;
				break;
			}
  }

  if (_isModifiedConstraintMatrix){
    // in case of Lag mult eliminaton
    this->assembleLinearConstraintMatrixToSystem();
  }
};

void pbcAlgorithm::splitDofs(){
	const std::vector<partDomain*>& allDomain = *(_solver->getDomainVector());
  if (_splittedDofs == NULL)
    _splittedDofs = new pbcDofManager();

  if (_isSplitted) _splittedDofs->clear();
  _isSplitted = true;
	
	#ifdef _DEBUG
  printf("begin splitting dofs\n");
	#endif //_DEBUG
	
	std::set<Dof> fixedDof;
	_solver->getDofManager()->getFixedDof(fixedDof);
	for (std::set<Dof>::const_iterator itdof = fixedDof.begin(); itdof!= fixedDof.end(); itdof++){
		_splittedDofs->fixDof(*itdof);
	}
	// fixed dofs are eliminated from intenal dofs

  // boundary Dofs
  for (std::set<constraintElement*>::iterator it= _pbcConstraint->constraintGroupBegin();
                                                it!= _pbcConstraint->constraintGroupEnd(); it++){
    constraintElement* cele = *it;
    std::vector<Dof> keys;
    cele->getConstraintKeys(keys);
    int ndofs = keys.size();
    for (int i=0; i<ndofs; i++){
      _splittedDofs->numberBoundaryDof(keys[i]);
    }
    keys.clear();
    cele->getMultiplierKeys(keys);
    ndofs = keys.size();
    for (int i=0; i<ndofs; i++){
      _splittedDofs->numberMultiplierDof(keys[i]);
    }
  };
  // internal DOFs
  for (int i=0; i<allDomain.size(); i++){
    partDomain* dom = allDomain[i];
    FunctionSpaceBase* sp = dom->getFunctionSpace();
    for (elementGroup::elementContainer::const_iterator it = dom->element_begin(); it!= dom->element_end(); it++){
      MElement* ele = it->second;
      std::vector<Dof> keys;
      sp->getKeys(ele,keys);
      int ndofs = keys.size();
      for (int j=0; j<ndofs; j++){
				if (fixedDof.find(keys[j]) == fixedDof.end()){
					_splittedDofs->numberInternalDof(keys[j]);
				}
      };
    };
  };

  // Direct DOFs
  for (std::set<constraintElement*>::iterator it= _pbcConstraint->constraintGroupBegin();
                                                  it!= _pbcConstraint->constraintGroupEnd(); it++){
    constraintElement* cel = *it;
    if (cel->isDirect()){
      std::vector<Dof> keys;
      cel->getConstraintKeys(keys);
      int ndofs = keys.size();
      for (int i=0; i<ndofs; i++){
        _splittedDofs->numberDirectDof(keys[i]);
      };
      keys.clear();
      cel->getMultiplierKeys(keys);
      ndofs = keys.size();
      for (int i=0; i<ndofs; i++){
        _splittedDofs->numberDirectMultiplierDof(keys[i]);
      }
    }
  };
  // indirect DOFs
  for (std::set<constraintElement*>::iterator it= _pbcConstraint->constraintGroupBegin();
                                                  it!= _pbcConstraint->constraintGroupEnd(); it++){
    constraintElement* cel = *it;
    if (!cel->isDirect()){
      std::vector<Dof> keys;
      cel->getConstraintKeys(keys);
      int ndofs = keys.size();
      for (int i=0; i<ndofs; i++){
        _splittedDofs->numberIndirectDof(keys[i]);
      };
      keys.clear();
      cel->getMultiplierKeys(keys);
      ndofs = keys.size();
      for (int i=0; i<ndofs; i++){
        _splittedDofs->numberIndirectMultiplierDof(keys[i]);
      }
    }

  };
  // dependent DOFs
  for (std::set<constraintElement*>::iterator it= _pbcConstraint->constraintGroupBegin();
                                              it!= _pbcConstraint->constraintGroupEnd(); it++){
    constraintElement* cel = *it;
    if (!cel->isDirect()){
      std::vector<Dof> keys;
      cel->getDependentKeys(keys);
      int ndofs = keys.size();
      for (int i=0; i<ndofs; i++){
        _splittedDofs->numberDependentDof(keys[i]);
      };
    }
  };
  // independent DOFs
  for (std::set<constraintElement*>::iterator it= _pbcConstraint->constraintGroupBegin();
                                              it!= _pbcConstraint->constraintGroupEnd(); it++){
    constraintElement* cel = *it;
    std::vector<Dof> keys;
    cel->getConstraintKeys(keys);
    int ndofs = keys.size();
    for (int i=0; i<ndofs; i++){
      _splittedDofs->numberIndependentDof(keys[i]);
    };
  };
  // for virtual keys
	std::vector<int> constrainedComp(_solver->getMicroBC()->getConstrainedComps().begin(), _solver->getMicroBC()->getConstrainedComps().end());
  for (std::set<MVertex*>::iterator it = _pbcConstraint->virtualVertexBegin();
                                      it!= _pbcConstraint->virtualVertexEnd(); it++){
    MVertex* v = *it;
    if (_pbcConstraint->isActive(v)){
       #ifdef _DEBUG
      Msg::Info("Virtual vertex = %d", v->getNum());
      #endif
      std::vector<Dof> keys;
      if (_space!=NULL)
        getKeysFromVertex(_space,v,constrainedComp,keys);
      else
        Msg::Error("microscopic boundary has not been implemented in case of existing fullDG domain");
      int ndofs = keys.size();
      for (int i=0; i< ndofs; i++){
        _splittedDofs->numberVirtualDof(keys[i]);
      };
    }
  };
  // virtual dofs from constraint which uses in the interpolation formulation -- slop in spline interpolation
  for (std::set<constraintElement*>::iterator it = _pbcConstraint->constraintGroupBegin();
                                              it!= _pbcConstraint->constraintGroupEnd(); it++){
    constraintElement* cele = *it;
    std::vector<Dof> keys;
    cele->getVirtualKeys(keys);
    int ndofs = keys.size();
    for (int i=0; i< ndofs; i++){
      _splittedDofs->numberVirtualDof(keys[i]);
    };
  };

  // for realindependent dofs
  for (std::set<constraintElement*>::iterator it= _pbcConstraint->constraintGroupBegin();
                                              it!= _pbcConstraint->constraintGroupEnd(); it++){
    constraintElement* cel = *it;
    std::vector<Dof> keys;
    cel->getConstraintKeys(keys);
    int ndofs = keys.size();
    for (int i=0; i<ndofs; i++){
      _splittedDofs->numberRealIndependentDof(keys[i]);
    };
  };
	
	#ifdef _DEBUG
  printf("end splitting dofs\n");
	#endif //_DEBUG

  int ni = _splittedDofs->sizeOfInternalDof();
  int ndirect = _splittedDofs->sizeOfDirectDof();
  int nIndirect = _splittedDofs->sizeOfIndirectDof();
  int ndepend = _splittedDofs->sizeOfDependentDof();
  int nrealIndepen = _splittedDofs->sizeOfRealIndependentDof();
  int nvirt = _splittedDofs->sizeOfVirtualDof();
  int nl = _splittedDofs->sizeOfMultiplierDof();
  int nlDirect = _splittedDofs->sizeOfDirectMultiplierDof();
  int nlIndirect = _splittedDofs->sizeOfIndirectMultiplierDof();
  int nK = this->getNumberOfMacroToMicroKinematicVariables();
  Msg::Info("Partitioned Dofs: ni = %d, ndirect = %d, nIndirect = %d, ndepend = %d, nrealIndepen = %d, nvirt = %d, \n nConstraint=%d, nConstraintDirect=%d, nConstraintIndirect=%d , nK = %d",
            ni,ndirect, nIndirect, ndepend, nrealIndepen, nvirt, nl, nlDirect,nlIndirect,nK);
};

void pbcAlgorithm::numberDof(const int type){
  if (!this->isSplittedDof()) this->splitDofs();
  if (type == nonLinearMechSolver::MULT_ELIM){
    Msg::Info("Number all displacement dofs");
    _splittedDofs->numberDof(pbcDofManager::INTERNAL,_solver->getDofManager());
    _splittedDofs->numberDof(pbcDofManager::BOUNDARY,_solver->getDofManager());
  }
  else if (type == nonLinearMechSolver::DISP_ELIM){
    Msg::Info("Number all displacement dofs accounting for constraints");
    this->applyPBCByConstraintElimination();
    _splittedDofs->numberDof(pbcDofManager::INTERNAL,_solver->getDofManager());
    _splittedDofs->numberDof(pbcDofManager::BOUNDARY,_solver->getDofManager());
  }
  else if (type == nonLinearMechSolver::DISP_MULT){
    Msg::Info("Number all displacement dofs and multiplier dofs");
    _splittedDofs->numberDof(pbcDofManager::INTERNAL,_solver->getDofManager());
    _splittedDofs->numberDof(pbcDofManager::BOUNDARY,_solver->getDofManager());
    _splittedDofs->numberDof(pbcDofManager::MULTIPLIER,_solver->getDofManager());
  }
  else if (type == nonLinearMechSolver::DISP_ELIM_UNIFIED){
    Msg::Info("Number all displacement dofs");
    _splittedDofs->numberDof(pbcDofManager::INTERNAL,_solver->getDofManager());
    _splittedDofs->numberDof(pbcDofManager::BOUNDARY,_solver->getDofManager());
  }
  else{
    Msg::Error("system type used in mechanical solver is not valid");
  };

};

void pbcAlgorithm::assembleKinematicMatrix(){
  std::string name = "A";
  linearSystem<double>* lsys = _solver->getDofManager()->getLinearSystem(name);
  pbcSystem<double>* pbcsys = dynamic_cast<pbcSystem<double>*>(lsys);
  if (!pbcsys){
    printf("periodic non linear system is not used! pbcAlgorithm::assembleKinematicMatrix\n");
  }
  else
  {
    pbcsys->zeroKinematicMatrix();
    for (std::set<constraintElement*>::iterator it = _pbcConstraint->constraintGroupBegin(); it!= _pbcConstraint->constraintGroupEnd(); it++){
      constraintElement* cele = *it;

      fullMatrix<double> S;
      cele->getKinematicalMatrix(S);
      std::vector<Dof> R;
      cele->getMultiplierKeys(R);

      for (int i=0; i<R.size(); i++){
        int row = _splittedDofs->getMultiplierDofNumber(R[i]);
        if (row < 0){
          Msg::Error("error in constraint numbering pbcAlgorithm::assembleKinematicMatrix");
        }
        else{
          for (int j=0; j< S.size2(); j++){
            pbcsys->addToKinematicMatrix(row,j,S(i,j));
          }
        }

      }

    };
  };

};

void pbcAlgorithm::zeroStressMatrix(){
  std::string name = "A";
  linearSystem<double>* lsys = _solver->getDofManager()->getLinearSystem(name);
  pbcSystemBase<double>* pbcsys = dynamic_cast<pbcSystemBase<double>*>(lsys);
  if (pbcsys == NULL){
    Msg::Error("pbc system must be used pbcAlgorithm::zeroInSystemHomogenizedTangentMatrix");
  }
  else{
    pbcsys->zeroStressMatrix();
  }
};

void pbcAlgorithm::zeroBodyForceMatrix(){
  std::string name = "A";
  linearSystem<double>* lsys = _solver->getDofManager()->getLinearSystem(name);
  pbcSystemBase<double>* pbcsys = dynamic_cast<pbcSystemBase<double>*>(lsys);
  if (pbcsys == NULL){
    Msg::Error("pbc system must be used pbcAlgorithm::zeroInSystemHomogenizedTangentMatrix");
  }
  else{
    pbcsys->zeroBodyForceMatrix();
  }
};

void pbcAlgorithm::zeroBodyForceVector(){
  std::string name = "A";
  linearSystem<double>* lsys = _solver->getDofManager()->getLinearSystem(name);
  pbcSystemBase<double>* pbcsys = dynamic_cast<pbcSystemBase<double>*>(lsys);
  if (pbcsys == NULL){
    Msg::Error("pbc system must be used pbcAlgorithm::zeroBodyForceVector");
  }
  else{
    pbcsys->zeroBodyForceVector();
  }
};

int pbcAlgorithm::condensationSolve(fullMatrix<double>& L, double rvevolume, const bool needdUdF,const bool needdUdG, nlsDofManager* pAss,  
             std::map<Dof, STensor3 >* dUdF, std::map<Dof, STensor33 >* dUdG)
{
  std::string name = "A";
  linearSystem<double>* lsys = _solver->getDofManager()->getLinearSystem(name);
  pbcSystemBase<double>* pbcsys = dynamic_cast<pbcSystemBase<double>*>(lsys);
  if (pbcsys == NULL){
    Msg::Error("pbc system must be used pbcAlgorithm::zeroInSystemHomogenizedTangentMatrix");
    return 0;
  }
  else{
    int ok = pbcsys->condensationSolve(L,rvevolume, needdUdF,needdUdG, pAss, dUdF,dUdG);
    return ok;
  }
};

int pbcAlgorithm::condensationdUdF(nlsDofManager* pAss,  std::map<Dof, STensor3>* dUdF ){
  std::string name = "A";
  linearSystem<double>* lsys = _solver->getDofManager()->getLinearSystem(name);
  pbcSystemBase<double>* pbcsys = dynamic_cast<pbcSystemBase<double>*>(lsys);
  if (pbcsys == NULL){
    Msg::Error("pbc system must be used pbcAlgorithm::zeroInSystemHomogenizedTangentMatrix");
    return 0;
  }
  else{
    int ok = pbcsys->condensationdUdF(pAss, dUdF);
    return ok;
  }
};

void pbcAlgorithm::sparsityLagMultiplierDofs(){
  for (std::set<constraintElement*>::iterator it = _pbcConstraint->constraintGroupBegin(); it!= _pbcConstraint->constraintGroupEnd(); it++){
		constraintElement* cele = *it;

		std::vector<Dof> keys;
		cele->getConstraintKeys(keys);
		cele->getMultiplierKeys(keys);

		for (int i=0; i< keys.size(); i++){
		  for (int j=0; j< keys.size(); j++){
        _solver->getDofManager()->insertInSparsityPattern(keys[i],keys[j]);
		  }
		}

	};
};

void pbcAlgorithm::assembleConstraintMatrix(){
	//printf("Assembling constraint matrix to linear systems \n");
	for (std::set<constraintElement*>::iterator it = _pbcConstraint->constraintGroupBegin(); it!= _pbcConstraint->constraintGroupEnd(); it++){
		constraintElement* cele = *it;
		fullMatrix<double> C;
		cele->getConstraintMatrix(C);
		int row = C.size1();
		int col = C.size2();
		int size = row+col;
		fullMatrix<double> m; m.resize(size,size); m.setAll(0.0);
		for (int i =0; i<row; i++){
			for (int j=0; j<col; j++){
				m(i+col,j) = C(i,j)*_scale;
				m(j,i+col) = -C(i,j)*_scale;
			};
		};
		std::vector<Dof> keys;
		cele->getConstraintKeys(keys);
		cele->getMultiplierKeys(keys);
		_solver->getDofManager()->assemble(keys,m);
	};
};
void pbcAlgorithm::assembleRightHandSide(){
   for (std::set<constraintElement*>::iterator it = _pbcConstraint->constraintGroupBegin(); it!= _pbcConstraint->constraintGroupEnd(); it++){
      constraintElement* cele = *it;
      std::vector<Dof> keys;
      cele->getMultiplierKeys(keys);
      fullVector<double> vec;
      cele->getKinematicalVector(vec);
      vec.scale(_scale);
      _solver->getDofManager()->assemble(keys,vec,nonLinearSystem<double>::Fext);
   };
};
void pbcAlgorithm::computeLinearConstraints()
{
  for (std::set<constraintElement*>::iterator it = _pbcConstraint->constraintGroupBegin(); it!= _pbcConstraint->constraintGroupEnd(); it++){
    constraintElement* cele = *it;
    fullMatrix<double> C;
    cele->getConstraintMatrix(C);
    int row = C.size1();
    int col = C.size2();
    int size = row+col;
    fullMatrix<double> m; m.resize(size,size); m.setAll(0.0);
    for (int i =0; i<row; i++){
       for (int j=0; j<col; j++){
          m(i+col,j) = C(i,j)*_scale;
				  m(j,i+col) = -C(i,j)*_scale;
       };
    };
    std::vector<Dof> keys;
    cele->getConstraintKeys(keys);
    cele->getMultiplierKeys(keys);
    fullVector<double> solution(size);
    for (int i=0; i<size; i++){
        _solver->getDofManager()->getDofValue(keys[i],solution(i));
    };
    fullVector<double> val(size);
    m.mult(solution,val);
    fullVector<double> rhs;

    cele->getKinematicalVector(rhs);

    rhs.scale(_scale);
    for (int i=0; i<row; i++) {
        val(i+col) = val(i+col)-rhs(i);
    };
    _solver->getDofManager()->assemble(keys,val,nonLinearSystem<double>::Fint);
  };
};

void pbcAlgorithm::zeroMultipleRHS()
{
  std::string name = "A";
  linearSystem<double>* lsys = _solver->getDofManager()->getLinearSystem(name);
  nonLinearSystemBase* nonsys = dynamic_cast<nonLinearSystemBase*>(lsys);
  if (nonsys != NULL)
  {
    if (!nonsys->isMultipleRHSAllocated())
    {
      nonsys->allocateMultipleRHS(getNumberOfMacroToMicroKinematicVariables());
    }
    nonsys->zeroMultipleRHS();
  }
};

void pbcAlgorithm::assembmeToMultipleRHS()
{
  std::string name = "A";
  linearSystem<double>* lsys = _solver->getDofManager()->getLinearSystem(name);
  nonLinearSystem<double>* nonsys = dynamic_cast<nonLinearSystem<double>*>(lsys);
  if (nonsys != NULL)
  {
    Msg::Info("start assembling assembmeToMultipleRHS");
    for (std::set<constraintElement*>::iterator it = _pbcConstraint->constraintGroupBegin(); it!= _pbcConstraint->constraintGroupEnd(); it++){
      constraintElement* cele = *it;

      fullMatrix<double> S;
      cele->getKinematicalMatrix(S);
      std::vector<Dof> R;
      cele->getMultiplierKeys(R);

      for (int i=0; i<R.size(); i++){
        int row = _solver->getDofManager()->getDofNumber(R[i]);
        if (row < 0)
        {
          Msg::Error("error in constraint numbering pbcAlgorithm::assembmeToMultipleRHS");
        }
        else{
          for (int j=0; j< S.size2(); j++){
            nonsys->addToMultipleRHSMatrix(row,j,S(i,j)*_scale);
          }
        }

      }

    };
    Msg::Info("done assembling assembmeToMultipleRHS");
  }
};

bool pbcAlgorithm::systemSolveMultipleRHS()
{
  std::string name = "A";
  linearSystem<double>* lsys = _solver->getDofManager()->getLinearSystem(name);
  nonLinearSystemBase* nonsys = dynamic_cast<nonLinearSystemBase*>(lsys);
  if (nonsys != NULL)
  {
    return nonsys->systemSolveMultipleRHS();
  }
  return false;
}
double pbcAlgorithm::getFromMultipleRHSSolution(int Nrow, int col)
{
  std::string name("A");
  linearSystem<double>* lsys = _solver->getDofManager()->getLinearSystem(name);
  nonLinearSystem<double>* nonsys = dynamic_cast<nonLinearSystem<double>*>(lsys);
  if (nonsys != NULL)
  {
    double val=0;
    nonsys->getFromMultipleRHSSolution(Nrow,col,val);
    return val;
  }
  else
  {
    return 0.;
  }
};



void pbcAlgorithm::assembleConstraintResidualToSystem(){
   #if _DEBUG
   printf("assemble constraint residuals to system \n");
   #endif //_DEBUG
   std::string name = "A";
   linearSystem<double>* lsys = _solver->getDofManager()->getLinearSystem(name);
   pbcSystem<double>* pbcsys = dynamic_cast<pbcSystem<double>*>(lsys);
   if (!pbcsys){
     printf("periodic non linear system is not used! pbcAlgorithm::assembleConstraintResidualToSystem\n");
   } else {

    pbcsys->zeroConstraintResidual();

     for (std::set<constraintElement*>::iterator it = _pbcConstraint->constraintGroupBegin(); it!= _pbcConstraint->constraintGroupEnd(); it++){
       constraintElement* cele = *it;
       fullMatrix<double> m;
       cele->getConstraintMatrix(m);
       fullVector<double> vec;
       cele->getKinematicalVector(vec);

       std::vector<Dof> keyL, keyB;
       cele->getMultiplierKeys(keyL);
       cele->getConstraintKeys(keyB);
       fullVector<double> result(keyB.size());
       for (int i=0; i<keyB.size(); i++){
           _solver->getDofManager()->getDofValue(keyB[i],result(i));
       };
       fullVector<double> residual(keyL.size());
       residual.setAll(0.0);
       m.mult(result,residual);
       residual.axpy(vec,-1.0);

	     for (int i=0; i<keyL.size();i++){
        int row = _splittedDofs->getMultiplierDofNumber(keyL[i]);
	      pbcsys->addToConstraintResidual(row,residual(i));
	    };
    };
  };
};

void pbcAlgorithm::assembleLinearConstraintMatrixToSystem(){
	#ifdef _DEBUG
	printf("assemble constraint matrix to system \n");
	#endif
	std::string name = "A";
	linearSystem<double>* lsys = _solver->getDofManager()->getLinearSystem(name);
	pbcSystem<double>* pbcsys = dynamic_cast<pbcSystem<double>*>(lsys);
	if (pbcsys !=NULL)
  {
    pbcsys->zeroConstraintMatrix();
		for (std::set<constraintElement*>::iterator it = _pbcConstraint->constraintGroupBegin(); it!= _pbcConstraint->constraintGroupEnd(); it++){
			constraintElement* cele = *it;
			fullMatrix<double> m;
			cele->getConstraintMatrix(m);
			std::vector<Dof> keyL, keyB;
			cele->getMultiplierKeys(keyL);
			cele->getConstraintKeys(keyB);
			for (int i=0; i<keyL.size(); i++){
				int row = _splittedDofs->getMultiplierDofNumber(keyL[i]);
				for (int j=0; j<keyB.size(); j++){
					int col = _solver->getDofManager()->getDofNumber(keyB[j]);
					if (fabs(m(i,j))>0.)
						pbcsys->addToConstraintMatrix(row,col,m(i,j));
				};
			};
		};
    // after modifying the linear constraint matrix, projection matrices need to be computed
    pbcsys->computeConstraintProjectMatrices();
	  _isModifiedConstraintMatrix = false; // as constraint matrix is already modified
	}
  #ifdef _DEBUG
  else
  {
		Msg::Warning("periodic non linear system is not used! pbcAlgorithm::assembleLinearConstraintMatrixToSystem");
	}
  #endif //_DEBUG
};

void pbcAlgorithm::allocateConstraintMatrixToSystem(int kinematicDim, int stressDim, bool tangentFlag){
	std::string name = "A";
	linearSystem<double>* lsys = _solver->getDofManager()->getLinearSystem(name);
	pbcSystem<double>* pbcsys = dynamic_cast<pbcSystem<double>*>(lsys);
	int size = dynamic_cast<pbcDofManager*>(_splittedDofs)->sizeOfMultiplierDof();
	if (pbcsys){
	    if (_solver->computedUdF())
                pbcsys->setNeedBodyForceVector(true);
	    pbcsys->allocateConstraintMatrix(size,_solver->getDofManager()->sizeOfR());
	    pbcsys->setScaleFactor(_scale);
	if (tangentFlag or _solver->computedUdF()){
          if(!pbcsys->isAllocatedKinematicMatrix())
            pbcsys->allocateKinematicMatrix(size,kinematicDim);
          if(!pbcsys->isAllocatedStressMatrix())
            pbcsys->allocateStressMatrix(stressDim,_solver->getDofManager()->sizeOfR());
          if (_solver->computedUdF() and !pbcsys->isAllocatedBodyForceMatrix())
            pbcsys->allocateBodyForceMatrix(_solver->getDofManager()->sizeOfR());
	}
	}
};


void pbcAlgorithm::applyPBCByConstraintElimination(){
  std::map<Dof,DofAffineConstraint<double> > linearConstraint;

  for (std::set<constraintElement*>::iterator it = _pbcConstraint->constraintGroupBegin(); it!= _pbcConstraint->constraintGroupEnd(); it++){
    constraintElement* cele = *it;

    cele->getLinearConstraints(linearConstraint);
    for (std::map<Dof,DofAffineConstraint<double> >::iterator itp = linearConstraint.begin(); itp != linearConstraint.end(); itp++){
      _solver->getDofManager()->setLinearConstraint(itp->first, itp->second);
    };
    linearConstraint.clear();
  };
};

void pbcAlgorithm::computeIndirectLinearConstraintMatrixForPBCSystem(linearConstraintMatrix* lcon) const{
  if (!lcon->isAllocated()){
    int constraintnum = _splittedDofs->sizeOfIndirectMultiplierDof();
    int dsize = _splittedDofs->sizeOfDependentDof();
    int fsize = _splittedDofs->sizeOfIndependentDof();
    int csize = _splittedDofs->sizeOfDirectDof();
    int nK = this->getNumberOfMacroToMicroKinematicVariables();
    lcon->allocate(constraintnum,dsize,fsize,csize,nK);
    // add preallocate pattern
    for (std::set<constraintElement*>::iterator it = _pbcConstraint->constraintGroupBegin();
                                              it!= _pbcConstraint->constraintGroupEnd(); it++){
      constraintElement* cele = *it;
      if (!cele->isDirect()){
        std::vector<Dof> R, C;
        cele->getMultiplierKeys(R);
        cele->getConstraintKeys(C);

        for (int i=0; i< R.size(); i++){
          int row = _splittedDofs->getIndirectMultiplierDofNumber(R[i]);
          if (row <0) Msg::Error("Error in constraint numering pbcAlgorithm::computeIndirectLinearConstraintMatrixForPBCSystem");
          for (int j=0; j< C.size(); j++){
            int colDpend = _splittedDofs->getDependentDofNumber(C[j]);
            if (colDpend >=0){
              lcon->insertInSparsityPattern_FirstMatrix(row,colDpend);
            }
            else{
              int conIndepend = _splittedDofs->getIndependentDofNumber(C[j]);
              if (conIndepend>=0){
                lcon->insertInSparsityPattern_SecondMatrix(row,conIndepend);
              }
              else {
                int conDirect = _splittedDofs->getDirectDofNumber(C[j]);
                if (conDirect >=0){
                  lcon->insertInSparsityPattern_ThirdMatrix(row,conDirect);
                }
                else{
                  Msg::Error("Error in boundary Dof numbering pbcAlgorithm::computeIndirectLinearConstraintMatrixForPBCSystem Preallocate");
                }
              }
            }
          };
        };

      };
    };

    lcon->preAllocateEntries_FirstMatrix();
    lcon->preAllocateEntries_SecondMatrix();
    lcon->preAllocateEntries_ThirdMatrix();
  }
  else{
    lcon->zeroAllMatrices();
  }
  for (std::set<constraintElement*>::iterator it = _pbcConstraint->constraintGroupBegin();
                                              it!= _pbcConstraint->constraintGroupEnd(); it++){
    constraintElement* cele = *it;
    if (!cele->isDirect()){
      fullMatrix<double> mleft, mright;
      cele->getConstraintMatrix(mleft);
      cele->getKinematicalMatrix(mright);
      if (mright.size2() != getNumberOfMacroToMicroKinematicVariables()){
        Msg::Error("error number col of kinematic matrix = %d is not equal to imposed value = %d ",mright.size2(),getNumberOfMacroToMicroKinematicVariables());
        cele->print();
      }
      std::vector<Dof> R, C;
      cele->getMultiplierKeys(R);
      cele->getConstraintKeys(C);

      for (int i=0; i< R.size(); i++){
        int row = _splittedDofs->getIndirectMultiplierDofNumber(R[i]);
        if (row <0) if (row <0) Msg::Error("Error in constraint numering pbcAlgorithm::computeIndirectLinearConstraintMatrixForPBCSystem");

        for (int j=0; j< C.size(); j++){
          int colDepen = _splittedDofs->getDependentDofNumber(C[j]);
          if (colDepen>=0){
            // dependent matrix
            lcon->addToFirstMatrix(row,colDepen,mleft(i,j));
          }
          else {
            int colIndepent = _splittedDofs->getIndependentDofNumber(C[j]);
            if (colIndepent >=0){
              lcon->addToSecondMatrix(row,colIndepent,mleft(i,j));
            }
            else{
              int colDirect = _splittedDofs->getDirectDofNumber(C[j]);
              if (colDirect>=0){
                lcon->addToThirdMatrix(row,colDirect,mleft(i,j));
              }
              else{
                Msg::Error("Error in boundary Dof numbering pbcAlgorithm::computeIndirectLinearConstraintMatrixForPBCSystem");
              }

            }

          }
        }
        int nK = this->getNumberOfMacroToMicroKinematicVariables();
        for (int j=0; j< nK; j++){
          lcon->addToKinematicalMatrix(row,j,mright(i,j));
        }

      }
    };
  };
};

 void pbcAlgorithm::updateSystemUnknown(){
  std::string name = "A";
  linearSystem<double>* lsys = _solver->getDofManager()->getLinearSystem(name);
  pbcSystemConden<double>* pbcsys = dynamic_cast<pbcSystemConden<double>*>(lsys);
  if (pbcsys == NULL){
    Msg::Error("pbc system must be used pbcAlgorithm::updateSystemUnknown");
  }
  else{
    // add new kinematic vector
    pbcsys->zeroKinematicVector();

    fullVector<double> kinVec;
    _solver->getMicroBC()->getKinematicalVector(kinVec);
    for (int i=0; i< kinVec.size(); i++){
      double val = kinVec(i);
      pbcsys->addToKinematicVector(i,val);
    }
    // update system unknown
    pbcsys->updateSystemUnknown();
  }
 };


void pbcAlgorithm::computeDirectLinearConstraintMatrix(linearConstraintMatrix* lcon) const{
  if (!lcon->isAllocated()){
    int csize = _splittedDofs->sizeOfDirectMultiplierDof();
    int nK = this->getNumberOfMacroToMicroKinematicVariables();
    lcon->allocate(csize,0,0,0,nK);
  }
  else{
    lcon->zeroAllMatrices();
  }
  for (std::set<constraintElement*>::iterator it = _pbcConstraint->constraintGroupBegin();
                                              it != _pbcConstraint->constraintGroupEnd(); it++){
    constraintElement* cele = *it;
    if (cele->isDirect()){
      fullMatrix<double> m;
      cele->getKinematicalMatrix(m);

      if (m.size2() != getNumberOfMacroToMicroKinematicVariables()){
        Msg::Error("error number col of kinematic matrix = %d is not equal to imposed value = %d ",m.size2(),getNumberOfMacroToMicroKinematicVariables());
        cele->print();
      }

      std::vector<Dof> R;
      cele->getConstraintKeys(R);

      std::vector<int> NR(R.size());
      for (unsigned int i = 0; i < R.size(); i++){
        NR[i] = _splittedDofs->getDirectDofNumber(R[i]);
      };



      for (unsigned int i = 0; i < R.size(); i++){
        if (NR[i] != -1){
          for (unsigned int j = 0; j < m.size2(); j++){
            lcon->addToKinematicalMatrix(NR[i],j, m(i, j));
          };
        }
        else{
          printf("Error in dof numbering _C matrix \n");
        };
      };
    }
  };
};
void pbcAlgorithm::computeIndirectLinearConstraintMatrix(linearConstraintMatrix* lcon) const{
  if (!lcon->isAllocated()){
    int constraintnum = _splittedDofs->sizeOfIndirectMultiplierDof();
    int bsize = _splittedDofs->sizeOfIndirectDof();
    int csize = _splittedDofs->sizeOfDirectDof();
    int nK = this->getNumberOfMacroToMicroKinematicVariables();
    lcon->allocate(constraintnum,bsize,csize,0,nK);
    // add preallocate pattern
    for (std::set<constraintElement*>::iterator it = _pbcConstraint->constraintGroupBegin();
                                              it!= _pbcConstraint->constraintGroupEnd(); it++){
      constraintElement* cele = *it;
      if (!cele->isDirect()){
        std::vector<Dof> R, C;
        cele->getMultiplierKeys(R);
        cele->getConstraintKeys(C);

        for (int i=0; i< R.size(); i++){
          int row = _splittedDofs->getIndirectMultiplierDofNumber(R[i]);
          for (int j=0; j< C.size(); j++){
            int colIndirect = _splittedDofs->getIndirectDofNumber(C[j]);
            if (colIndirect >=0){
              lcon->insertInSparsityPattern_FirstMatrix(row,colIndirect);
            }
            else{
              int conlDirect = _splittedDofs->getDirectDofNumber(C[j]);
              if (conlDirect>=0){
                lcon->insertInSparsityPattern_SecondMatrix(row,conlDirect);
              }
            }
          };
        };

      };
    };
  }
  else{
    lcon->zeroAllMatrices();
  }
  for (std::set<constraintElement*>::iterator it = _pbcConstraint->constraintGroupBegin();
                                              it!= _pbcConstraint->constraintGroupEnd(); it++){
    constraintElement* cele = *it;
    if (!cele->isDirect()){
      fullMatrix<double> mleft, mright;
      cele->getConstraintMatrix(mleft);
      cele->getKinematicalMatrix(mright);
      if (mright.size2() != getNumberOfMacroToMicroKinematicVariables()){
        Msg::Error("error number col of kinematic matrix = %d is not equal to imposed value = %d ",mright.size2(),getNumberOfMacroToMicroKinematicVariables());
        cele->print();
      }
      std::vector<Dof> R, C;
      cele->getMultiplierKeys(R);
      cele->getConstraintKeys(C);
      std::vector<int> NR(R.size()), NC(C.size());
      for (unsigned int i = 0; i < R.size(); i++){
        NR[i] = _splittedDofs->getIndirectMultiplierDofNumber(R[i]);
      };
      for (unsigned int i = 0; i < C.size(); i++){
        NC[i] = _splittedDofs->getIndirectDofNumber(C[i]);
      };
      for (unsigned int i = 0; i < R.size(); i++){
        if (NR[i] != -1){
          for (unsigned int j = 0; j < C.size(); j++){
            if (NC[j] != -1){
              lcon->addToFirstMatrix(NR[i], NC[j], mleft(i, j));
            }
            else{
              int cnum = _splittedDofs->getDirectDofNumber(C[j]);
              if (cnum != -1){
                lcon->addToSecondMatrix(NR[i],cnum,mleft(i,j));
              }
              else
                Msg::Info("Error in dof numbering C  Dofs  \n");
            };
          };

          for (unsigned int j = 0; j < mright.size2(); j++){
              lcon->addToKinematicalMatrix(NR[i],j, mright(i, j));
          };
        }
        else{
          printf("Error in dof numbering L Dofs \n");
        };
      };
    };
  };
};


void pbcAlgorithm::fillIndirectConstraintPattern(sparsityPattern* Cbd, sparsityPattern* Cbi) const{
	// iterating over all cele
  for (std::set<constraintElement*>::iterator it = _pbcConstraint->constraintGroupBegin();
                                              it!= _pbcConstraint->constraintGroupEnd(); it++){
    constraintElement* cele = *it;
    if (!cele->isDirect()){
      std::vector<Dof> R, C;
      cele->getMultiplierKeys(R);
      cele->getConstraintKeys(C);

			for (int i=0; i< R.size(); i++){
				int row = _splittedDofs->getIndirectMultiplierDofNumber(R[i]);
        for (int j=0; j< C.size(); j++){
					int colD = _splittedDofs->getDependentDofNumber(C[j]);
          if (colD >=0){
						Cbd->insertEntry(row,colD);
					}
					else{
            int colI = _splittedDofs->getIndependentDofNumber(C[j]);
						if (colI>=0){
						  Cbi->insertEntry(row,colI);
						}
					}
        };
      };

    };
  };
};

void pbcAlgorithm::computeIndirectLinearConstraintMatrixInterpolationWithNewVertices(linearConstraintMatrix* lcon) const{
  if (!lcon->isAllocated()){
    int bsize = _splittedDofs->sizeOfDependentDof();
    int csize = _splittedDofs->sizeOfDirectDof();
    int vsize = _splittedDofs->sizeOfVirtualDof();
    int nK = this->getNumberOfMacroToMicroKinematicVariables();
    lcon->allocate(bsize,vsize,csize,0,nK);
  }
  else{
    lcon->zeroAllMatrices();
  }
  for (std::set<constraintElement*>::iterator it = _pbcConstraint->constraintGroupBegin();
                                          it!= _pbcConstraint->constraintGroupEnd(); it++){
    constraintElement* cele = *it;
    if (!cele->isDirect()){
      std::vector<Dof> keys;
      cele->getDependentKeys(keys);
      int ndofs = keys.size();
      fullMatrix<double> mat;
      cele->getKinematicalMatrix(mat);
      int size2 = mat.size2();
      if (size2 != getNumberOfMacroToMicroKinematicVariables()){
        Msg::Error("error number col of kinematic matrix = %d is not equal to imposed value = %d ",size2,getNumberOfMacroToMicroKinematicVariables());
        cele->print();
      }
      for (int i=0; i<ndofs; i++){
        int row = _splittedDofs->getDependentDofNumber(keys[i]);
        for (int j=0; j<size2; j++){
          lcon->addToKinematicalMatrix(row,j,mat(i,j));
        };
      };
      std::map<Dof,DofAffineConstraint<double> > linearConstraint;
      cele->getLinearConstraints(linearConstraint);
      for (std::map<Dof,DofAffineConstraint<double> >::iterator it = linearConstraint.begin(); it != linearConstraint.end(); it++){
        int row  = _splittedDofs->getDependentDofNumber(it->first);
        if (row >=0){
          DofAffineConstraint<double>* cons = &(it->second);
          for (int i=0; i<cons->linear.size(); i++){
            std::pair<Dof,double> pir = cons->linear[i];
            double val = pir.second;
            int col = _splittedDofs->getVirtualDofNumber(pir.first);
            if (col >=0){
              lcon->addToFirstMatrix(row,col,val);
            }
            else{
              int col = _splittedDofs->getDirectDofNumber(pir.first);
              lcon->addToSecondMatrix(row,col,val);
            };
          };
        };
      };
    };
  };

};

void pbcAlgorithm::computeLinearConstraintMatrix(linearConstraintMatrix* lcon) const{
  if (!lcon->isAllocated()){
    int nl = _splittedDofs->sizeOfMultiplierDof();
    int nb = _splittedDofs->sizeOfBoundaryDof();
    int nK = this->getNumberOfMacroToMicroKinematicVariables();
    lcon->allocate(nl,nb,0,0,nK);
  }
  else{
    lcon->zeroAllMatrices();
  }
  for (std::set<constraintElement*>::iterator it = _pbcConstraint->constraintGroupBegin();
                                              it != _pbcConstraint->constraintGroupEnd(); it++){
    constraintElement* cele = *it;
    std::vector<Dof> R, C;
    cele->getMultiplierKeys(R);
    cele->getConstraintKeys(C);
    fullMatrix<double> mleft, mright;
    cele->getConstraintMatrix(mleft);
    cele->getKinematicalMatrix(mright);
    if (mright.size2() != getNumberOfMacroToMicroKinematicVariables()){
      Msg::Error("error number col of kinematic matrix = %d is not equal to imposed value = %d ",mright.size2(),getNumberOfMacroToMicroKinematicVariables());
      cele->print();
    }
    std::vector<int> NR(R.size()), NC(C.size());
    for (unsigned int i = 0; i < R.size(); i++){
      NR[i] = _splittedDofs->getMultiplierDofNumber(R[i]);
    };
    for (unsigned int i = 0; i < C.size(); i++){
      NC[i] = _splittedDofs->getBoundaryDofNumber(C[i]);
    };
    for (int i=0; i<R.size(); i++){
      for (int j=0; j<C.size(); j++){
        lcon->addToFirstMatrix(NR[i],NC[j],mleft(i,j));
      }
      for (int j=0; j<mright.size2(); j++){
        lcon->addToKinematicalMatrix(NR[i],j,mright(i,j));
      }
    }
  }
};

void pbcAlgorithm::computeLinearConstraintMatrixLDBC(linearConstraintMatrix* lcon) const{
  if (!lcon->isAllocated()){
    lcon->allocate(_splittedDofs->sizeOfBoundaryDof(),0,0,0,this->getNumberOfMacroToMicroKinematicVariables());
  }
  else{
    lcon->zeroAllMatrices();
  }
  for (std::set<constraintElement*>::iterator it = _pbcConstraint->constraintGroupBegin();
                                              it != _pbcConstraint->constraintGroupEnd(); it++){
    constraintElement* cele = *it;
    std::vector<Dof> R;
    cele->getConstraintKeys(R);
    fullMatrix<double> m;
    cele->getKinematicalMatrix(m);

    if (m.size2() != getNumberOfMacroToMicroKinematicVariables()){
      Msg::Error("error number col of kinematic matrix = %d is not equal to imposed value = %d ",m.size2(),getNumberOfMacroToMicroKinematicVariables());
      cele->print();
    }

    std::vector<int> NR(R.size());
    for (unsigned int i = 0; i < R.size(); i++){
      NR[i] = _splittedDofs->getBoundaryDofNumber(R[i]);
    };
    for (int i=0; i<R.size(); i++){
      for (int j=0; j<m.size2(); j++){
        lcon->addToKinematicalMatrix(NR[i],j,m(i,j));
      }
    }
  };
};

void pbcAlgorithm::computeLoadVector(){
  std::string name = "A";
  linearSystem<double>* lsys = _solver->getDofManager()->getLinearSystem(name);
  nonLinearSystem<double>* nonsys = dynamic_cast<nonLinearSystem<double>*>(lsys);
  if (nonsys == NULL){
    Msg::Error("The nonlinear system must be used");
    return;
  }
  else{
    nonsys->zeroLoadVector();

    for (std::set<constraintElement*>::iterator it = _pbcConstraint->constraintGroupBegin();
                                              it != _pbcConstraint->constraintGroupEnd(); it++){
      constraintElement* cele = *it;
      std::vector<Dof> R;
      cele->getMultiplierKeys(R);

			double curtime = _solver->getMicroBC()->getTime();
			fullVector<double> m;
			_solver->getMicroBC()->setTime(0.);
			fullVector<double> m0;
			cele->getKinematicalVector(m0);
			_solver->getMicroBC()->setTime(1.);
			cele->getKinematicalVector(m);
			for (int i=0; i< m.size(); i++){
				m(i) -= m0(i);
			}

			// back to current time
			_solver->getMicroBC()->setTime(curtime);

      for (int i=0; i< R.size(); i++){
        int num = _splittedDofs->getMultiplierDofNumber(R[i]);
        if (num <0)
          Msg::Error("Error in number Dof");
        else
          nonsys->addToLoadVector(num,m(i));
      }
    }
  }
};



