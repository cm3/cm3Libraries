//
//
// Description: directional mixed BC
//
// Author:  <Van Dung NGUYEN>, (C) 2017
//
// Copyright: See COPYING file that comes with this distribution
//
//

#include "directionalConstraintElement.h"
#include "pbcConstraintElement.h"
#include "pbcSupplementConstraint.h"

directionalPeriodicMeshConstraint::directionalPeriodicMeshConstraint(nonLinearMicroBC* mbc,FunctionSpaceBase* spacePlus, FunctionSpaceBase* spaceMinus, FunctionSpaceBase* multspace, 
													 MVertex* vp, MVertex* vn, const SVector3& dir): constraintElement(mbc,-1),_periodicSpacePlus(spacePlus),
													 _periodicSpaceMinus(spaceMinus), _multSpace(multspace),_vp(vp),_vn(vn){
	
	if (_mbc->getOrder() == 2){
		Msg::Error("directionalPeriodicMeshConstraint has not been implemented for SecondOrder");	
	}
		
	const std::vector<int>& comp = this->getComp();
	SVector3 L(0.);
	if (_vn){
		L(0) = _vp->x() - _vn->x();
		L(1) = _vp->y() - _vn->y();
		L(2) = _vp->z() - _vn->z();
		
		_C.resize(1,2*_ndofs);
		_C.setAll(0.0);
		for (int i=0; i< _ndofs; i++){
			_C(0,i) = dir[i];
			_C(0,i+_ndofs) = -dir[i];
		}
		
	}
	else{
		L(0) = _vp->x();
		L(1) = _vp->y();
		L(2) = _vp->z();
		_C.resize(1,_ndofs);
		_C.setAll(0.0);
		for (int i=0; i< _ndofs; i++){
			_C(0,i) = dir[i];
		}
	}
		
	// find maximal value for 
	_positive = -1;
	double maxCoeff = 0.;
	bool found = false;
	std::vector<Dof> keys;
	getKeysFromVertex(_periodicSpacePlus,_vp, getComp(),keys); // all keys
	// find all in positive Dofs
	for (int i=0; i< _ndofs; i++){
		if ((fabs(_C(0,i)) > fabs(maxCoeff)) and 
				(constraintElement::allPositiveDof.find(keys[i]) == constraintElement::allPositiveDof.end())){
			maxCoeff = _C(0,i);
			_positive = i;
			found = true;
		}
	}
	if (found){
		constraintElement::allPositiveDof.insert(keys[_positive]);
	}
	else{
		Msg::Error("all positive vertex is considered");
	}
	_C.scale(1./maxCoeff);
	
	_Cbc.resize(1,_C.size2()-1);
	_Cbc.setAll(0.);
	int colCbc = 0;
	for (int i=0; i<_C.size2(); i++){
		if (i !=_positive){
			_Cbc(0,colCbc) = -_C(0,i);
			colCbc++;
		}
	};
		
	_S.resize(1,9);
	_S.scale(0.);
	for (int i=0; i<3; i++){
		for (int j=0; j<3; j++){
			int index = Tensor23::getIndex(i,j);
			_S(0,index) = dir[i]*L(j)/maxCoeff;
		}
	}
};

void directionalPeriodicMeshConstraint::print() const{
	if (_vn)
		printf("directional constraint Postive = %ld, negative = %ld \n", _vp->getNum(), _vn->getNum());
	else{
		printf("directional constraint Periodic point = %ld \n", _vp->getNum());
	}
};

void directionalPeriodicMeshConstraint::getConstraintKeys(std::vector<Dof>& key) const{
	getKeysFromVertex(_periodicSpacePlus,_vp, getComp(),key);
	if (_vn) getKeysFromVertex(_periodicSpaceMinus,_vn,getComp(),key);
};

void directionalPeriodicMeshConstraint::getMultiplierKeys(std::vector<Dof>& key) const{
	std::vector<int> comp;
	comp.push_back(_positive);
	getKeysFromVertex(_multSpace,_vp, comp,key);
};
void directionalPeriodicMeshConstraint::getConstraintMatrix(fullMatrix<double>& m) const{
	m = _C;
};

void directionalPeriodicMeshConstraint::getDependentMatrix(fullMatrix<double>& m) const{
  m = _Cbc;
};

void directionalPeriodicMeshConstraint::getDependentKeys(std::vector<Dof>& key) const{
	std::vector<Dof> allkeys;
	getConstraintKeys(allkeys);
	key.push_back(allkeys[_positive]);
};

void directionalPeriodicMeshConstraint::getIndependentKeys(std::vector<Dof>& key) const{
  std::vector<Dof> allkeys;
	getConstraintKeys(allkeys);
	for (int i=0; i< allkeys.size(); i++){
		if (i != _positive){
			key.push_back(allkeys[i]);
		}
	}
};

bool directionalPeriodicMeshConstraint::isDirect() const {
	return false;
}

void directionalPeriodicMeshConstraint::getLinearConstraints(const fullVector<double>& g,
                   std::map<Dof,DofAffineConstraint<double> >& con) const
{
  std::vector<Dof> kP, kN;
  getDependentKeys(kP);
	getIndependentKeys(kN);
	
  DofAffineConstraint<double> cons;
	for (int i=0; i< kN.size(); i++){
		cons.linear.push_back(std::pair<Dof,double>(kN[i],_Cbc(0,i)));
	}
	cons.shift=g(0);
	con[kP[0]] = cons;
}

void directionalPeriodicMeshConstraint::getLinearConstraintsByVertices(std::map<Dof,DofAffineConstraint<double> >& con) const{
	Msg::Error("this function is not used for directionalPeriodicMeshConstraint::getLinearConstraintsByVertices");
};

void directionalPeriodicMeshConstraint::getKinematicalVector(fullVector<double>& m) const{
	fullVector<double> kinVec;
	_mbc->getKinematicalVector(kinVec);
	m.resize(1);
	m.setAll(0.);
	_S.mult(kinVec,m);
};
void directionalPeriodicMeshConstraint::getKinematicalMatrix(fullMatrix<double>& m) const{
  m = _S;
};
void directionalPeriodicMeshConstraint::getLinearConstraints(std::map<Dof,DofAffineConstraint<double> >& con) const{
  fullVector<double> g;
  getKinematicalVector(g);
  getLinearConstraints(g, con);
};

directionalSupplementConstraint::directionalSupplementConstraint(nonLinearMicroBC* mbc, FunctionSpaceBase* space, FunctionSpaceBase* mspace,
												elementGroup* g, const SVector3& dir): constraintElement(mbc,-1), _periodicSpace(space),_multSpace(mspace),_g(g){
  
	if (_mbc->getOrder() == 2){
		Msg::Error("directionalPeriodicMeshConstraint has not been implemented for SecondOrder");	
	}
	int sizever = _g->vsize();
	fullVector<double> weight;
	supplementConstraint::computeWeight(_g,_v,weight);

	SVector3 Xmean(0.);
  for (int i=0; i<_v.size();i++){
    MVertex* v = _v[i];
    SPoint3 pnt = v->point();
    for( int j=0; j<3; j++){
      Xmean[j] += weight(i)*pnt[j];
    }
  }
	//
	_C.resize(1,sizever*_ndofs);
	_C.setAll(0.);
	int colC = 0;
	for (int i=0; i< sizever; i++){
		for (int j=0; j<_ndofs; j++){
			_C(0,colC) = weight(i)*dir(j);
			colC++;
		}
	}
	
	// find positive in order to obatain a good positive vertex
	std::vector<Dof> keys;
	for (int i=0; i<_v.size(); i++){
		getKeysFromVertex(_periodicSpace,_v[i],getComp(),keys);
	}
			
  _positive = -1;
	double maxVal = 0.;
	bool found = false;
  for (int i=0; i<keys.size(); i++){
    if ((fabs(_C(0,i))> fabs(maxVal)) && 
				(constraintElement::allPositiveDof.find(keys[i]) == constraintElement::allPositiveDof.end())){
      _positive = i;
			maxVal = _C(0,i);
			found = true;
			_positiveVertex = i/_ndofs;
			_positiveComp = i - _positiveVertex*_ndofs;
    }
  };
	if (!found){
		Msg::Error("All Dof are numerated as positive dof in other constraints supplementConstraint::supplementConstraint");
	}
	else{
		constraintElement::allPositiveDof.insert(keys[_positive]);
	}
	
	//Msg::Info("positive vertex %d positive comp %d",_positiveVertex,_positiveComp);

  
  double invs = 1./maxVal;
  _C.scale(invs);
	
	_Cbc.resize(1, keys.size() -1);
	_Cbc.setAll(0.);

	int colCbc = 0;
	for (int i=0; i<keys.size(); i++){
		if (i !=_positive){
			_Cbc(0,colCbc) = -_C(0,i);
			colCbc++;
		}
	};
	
	_S.resize(1,9);
	_S.scale(0.);
	for (int i=0; i<3; i++){
		for (int j=0; j<3; j++){
			int index = Tensor23::getIndex(i,j);
			_S(0,index) = dir[i]*Xmean(j)*invs;
		}
	}
	
	//_C.print("C");
	//_Cbc.print("CBC");
	//_S.print("S");
};


void directionalSupplementConstraint::getConstraintKeys(std::vector<Dof>& key) const {
  for (int i=0; i<_v.size(); i++)
		getKeysFromVertex(_periodicSpace,_v[i],getComp(),key);
}
void directionalSupplementConstraint::getMultiplierKeys(std::vector<Dof>& key) const{
  std::vector<int> comp;
	comp.push_back(_positiveComp);
	getKeysFromVertex(_multSpace,_v[_positiveVertex], comp,key);
}
void directionalSupplementConstraint::getConstraintMatrix(fullMatrix<double>& m) const {
  m = _C;
};		// matrix C
void directionalSupplementConstraint::getDependentMatrix(fullMatrix<double>& m) const{
  m = _Cbc;
}

void directionalSupplementConstraint::getDependentKeys(std::vector<Dof>& key) const{
	std::vector<Dof> allkeys;
	getConstraintKeys(allkeys);
	key.push_back(allkeys[_positive]);
};

void directionalSupplementConstraint::getIndependentKeys(std::vector<Dof>& key) const{
  std::vector<Dof> allkeys;
	getConstraintKeys(allkeys);
	for (int i=0; i< allkeys.size(); i++){
		if (i != _positive){
			key.push_back(allkeys[i]);
		}
	}
};


void directionalSupplementConstraint::getLinearConstraints(const fullVector<double>& g,
                   std::map<Dof,DofAffineConstraint<double> >& con) const
{
	std::vector<Dof> kP, kN;
  getDependentKeys(kP);
	getIndependentKeys(kN);
	
  DofAffineConstraint<double> cons;
	for (int i=0; i< kN.size(); i++){
		cons.linear.push_back(std::pair<Dof,double>(kN[i],_Cbc(0,i)));
	}
	cons.shift=g(0);
	con[kP[0]] = cons;
};

void directionalSupplementConstraint::print() const{
  Msg::Info("directional constraint Positive = %ld",_v[_positiveVertex]->getNum());
  for (int i=0; i<_v.size(); i++){
    printf("%ld \t",_v[i]->getNum());
  }
  printf("\n");
};

void directionalSupplementConstraint::getKinematicalVector(fullVector<double>& m) const{
  fullVector<double> kinVec;
	_mbc->getKinematicalVector(kinVec);
	m.resize(1);
	m.setAll(0.);
	_S.mult(kinVec,m);
};
void directionalSupplementConstraint::getKinematicalMatrix(fullMatrix<double>& m) const{
  m = _S;
};
void directionalSupplementConstraint::getLinearConstraints(std::map<Dof,DofAffineConstraint<double> >& con) const{

  fullVector<double> g;
  getKinematicalVector(g);
  getLinearConstraints(g, con);
};

directionalPBCSupplementConstraint::directionalPBCSupplementConstraint(nonLinearMicroBC* mbc, FunctionSpaceBase* space, FunctionSpaceBase* mspace,
												elementGroup* gPlus, elementGroup* gMinus, const SVector3& dir):
 constraintElement(mbc,-1), _periodicSpace(space),_multSpace(mspace),_gPlus(gPlus),_gMinus(gMinus){
  
	if (_mbc->getOrder() == 2){
		Msg::Error("directionalPBCSupplementConstraint has not been implemented for SecondOrder");	
	}
	
	fullVector<double> weightPlus, weightMinus;
	supplementConstraint::computeWeight(_gPlus,_vPlus,weightPlus);
	supplementConstraint::computeWeight(_gMinus,_vMinus,weightMinus);
	
	SVector3 XmeanPlus(0.);
  for (int i=0; i<_vPlus.size();i++){
    MVertex* vv = _vPlus[i];
    SPoint3 pnt = vv->point();
    for( int j=0; j<3; j++){
      XmeanPlus[j] += weightPlus(i)*pnt[j];
    }
  }
	SVector3 XmeanMinus(0.);
  for (int i=0; i<_vMinus.size();i++){
    MVertex* vv = _vMinus[i];
    SPoint3 pnt = vv->point();
    for( int j=0; j<3; j++){
      XmeanMinus[j] += weightMinus(i)*pnt[j];
    }
  }
	
	int csize = (_vPlus.size() + _vMinus.size())*_ndofs;
	//
	_C.resize(1,csize);
	_C.setAll(0.);
	int colC = 0;
	for (int i=0; i< _vPlus.size(); i++){
		for (int j=0; j<_ndofs; j++){
			_C(0,colC) = weightPlus(i)*dir(j);
			colC++;
		}
	}
	for (int i=0; i< _vMinus.size(); i++){
		for (int j=0; j<_ndofs; j++){
			_C(0,colC) = -weightMinus(i)*dir(j);
			colC++;
		}
	}
	
	
	// find positive in order to obatain a good positive vertex
	std::vector<Dof> keys;
	for (int i=0; i<_vPlus.size(); i++){
		getKeysFromVertex(_periodicSpace,_vPlus[i],getComp(),keys);
	}
	
  _positive = -1;
	double maxVal = 0.;
	bool found = false;
  for (int i=0; i<keys.size(); i++){
    if ((fabs(_C(0,i))> fabs(maxVal)) && 
				(constraintElement::allPositiveDof.find(keys[i]) == constraintElement::allPositiveDof.end())){
      _positive = i;
			maxVal = _C(0,i);
			found = true;
			_positiveVertex = i/_ndofs;
			_positiveComp = i - _positiveVertex*_ndofs;
    }
  };
	if (!found){
		Msg::Error("All Dof are numerated as positive dof in other constraints directionalPBCSupplementConstraint::directionalPBCSupplementConstraint");
	}
	else{
		constraintElement::allPositiveDof.insert(keys[_positive]);
	}
	
	//Msg::Info("positive vertex %d positive comp %d",_positiveVertex,_positiveComp);


  double invs = 1./maxVal;
  _C.scale(invs);
	
	_Cbc.resize(1, csize -1);
	_Cbc.setAll(0.);

	int colCbc = 0;
	for (int i=0; i<csize; i++){
		if (i !=_positive){
			_Cbc(0,colCbc) = -_C(0,i);
			colCbc++;
		}
	};
	
	_S.resize(1,9);
	_S.scale(0.);
	for (int i=0; i<3; i++){
		for (int j=0; j<3; j++){
			int index = Tensor23::getIndex(i,j);
			_S(0,index) = dir[i]*(XmeanPlus(j) - XmeanMinus(j))*invs;
		}
	}
	
	//_C.print("C");
	//_Cbc.print("CBC");
	//_S.print("S");
};


void directionalPBCSupplementConstraint::getConstraintKeys(std::vector<Dof>& key) const {
  for (int i=0; i<_vPlus.size(); i++)
		getKeysFromVertex(_periodicSpace,_vPlus[i],getComp(),key);
	for (int i=0; i<_vMinus.size(); i++)
		getKeysFromVertex(_periodicSpace,_vMinus[i],getComp(),key);
}
void directionalPBCSupplementConstraint::getMultiplierKeys(std::vector<Dof>& key) const{
  std::vector<int> comp;
	comp.push_back(_positiveComp);
	getKeysFromVertex(_multSpace,_vPlus[_positiveVertex], comp,key);
}
void directionalPBCSupplementConstraint::getConstraintMatrix(fullMatrix<double>& m) const {
  m = _C;
};		// matrix C
void directionalPBCSupplementConstraint::getDependentMatrix(fullMatrix<double>& m) const{
  m = _Cbc;
}

void directionalPBCSupplementConstraint::getDependentKeys(std::vector<Dof>& key) const{
	std::vector<Dof> allkeys;
	getConstraintKeys(allkeys);
	key.push_back(allkeys[_positive]);
};

void directionalPBCSupplementConstraint::getIndependentKeys(std::vector<Dof>& key) const{
  std::vector<Dof> allkeys;
	getConstraintKeys(allkeys);
	for (int i=0; i< allkeys.size(); i++){
		if (i != _positive){
			key.push_back(allkeys[i]);
		}
	}
};


void directionalPBCSupplementConstraint::getLinearConstraints(const fullVector<double>& g,
                   std::map<Dof,DofAffineConstraint<double> >& con) const
{
	std::vector<Dof> kP, kN;
  getDependentKeys(kP);
	getIndependentKeys(kN);
	
  DofAffineConstraint<double> cons;
	for (int i=0; i< kN.size(); i++){
		cons.linear.push_back(std::pair<Dof,double>(kN[i],_Cbc(0,i)));
	}
	cons.shift=g(0);
	con[kP[0]] = cons;
};

void directionalPBCSupplementConstraint::print() const{
  Msg::Info("directional constraint Positive = %d",_vPlus[_positiveVertex]->getNum());
  for (int i=0; i<_vPlus.size(); i++){
    printf("%ld \t",_vPlus[i]->getNum());
  }
	for (int i=0; i<_vMinus.size(); i++){
    printf("%ld \t",_vMinus[i]->getNum());
  }
  printf("\n");
};

void directionalPBCSupplementConstraint::getKinematicalVector(fullVector<double>& m) const{
  fullVector<double> kinVec;
	_mbc->getKinematicalVector(kinVec);
	m.resize(1);
	m.setAll(0.);
	_S.mult(kinVec,m);
};
void directionalPBCSupplementConstraint::getKinematicalMatrix(fullMatrix<double>& m) const{
  m = _S;
};
void directionalPBCSupplementConstraint::getLinearConstraints(std::map<Dof,DofAffineConstraint<double> >& con) const{

  fullVector<double> g;
  getKinematicalVector(g);
  getLinearConstraints(g, con);
};


directionalPBCLagrangeConstraintElement::directionalPBCLagrangeConstraintElement(nonLinearMicroBC* mbc, FunctionSpaceBase* space, FunctionSpaceBase* mspace, 
															MVertex* v1, std::vector<MVertex*>& vlist, const SVector3& dir):
															constraintElement(mbc,-1), _periodicSpace(space),_multSpace(mspace),
															_vp(v1),_vn(vlist){
	if (_mbc->getOrder() == 2){
		Msg::Error("directionalPBCLagrangeConstraintElement has not been implemented for SecondOrder");	
	}
	for (int i=0; i<_vn.size(); i++){
		double dist = _vn[i]->distance(_vn[0]);
		_distance.push_back(dist);
	};				
	
	SPoint3 pp = _vp->point();
	SPoint3 np1 = _vn[0]->point();
	SPoint3 np2 = _vn[_vn.size()-1]->point();
	SPoint3 point = planeDirProject(pp,np1,np2,dir);
	
	SVector3 VecFollowed(pp,point);
	
	_s = point.distance(np1);
	
	int csize = (_vn.size()+1)*_ndofs;
	
	_C.resize(1.,csize); _C.setAll(0.0);
	int col = 0;
	for (int i=0; i<_ndofs; i++){
		_C(0,col) = 1.0*VecFollowed[i];
		col++;
	};
	lagrangeConstraintElement::getFF(_s,_distance,_FF);
	for (int i=0; i<_FF.size(); i++){
		for (int j=0; j<_ndofs; j++){
			_C(0,col) = -1.*_FF(i)*VecFollowed[j];
			col++;
		};
	};
	
	// find maximal value for 
	_positiveComp = -1;
	double maxCoeff = 0.;
	bool found = false;
	std::vector<Dof> keys;
	getKeysFromVertex(_periodicSpace,_vp, getComp(),keys); // all keys
	// find all in positive Dofs
	for (int i=0; i< _ndofs; i++){
		if ((fabs(_C(0,i)) > fabs(maxCoeff)) and 
				(constraintElement::allPositiveDof.find(keys[i]) == constraintElement::allPositiveDof.end())){
			maxCoeff = _C(0,i);
			_positiveComp = i;
			found = true;
		}
	}
	if (found){
		constraintElement::allPositiveDof.insert(keys[_positiveComp]);
	}
	else{
		Msg::Error("all positive vertex is considered directionalPBCLagrangeConstraintElement vp = %d",_vp->getNum());
	}
	_C.scale(1./maxCoeff);
	
	_Cbc.resize(1,_C.size2()-1);
	_Cbc.setAll(0.);
	int colCbc = 0;
	for (int i=0; i<_C.size2(); i++){
		if (i !=_positiveComp){
			_Cbc(0,colCbc) = -_C(0,i);
			colCbc++;
		}
	};
	
	// periodic vector
	for (int j=0; j<3; j++){
		_L[j] = pp[j];
		for (int i=0; i<_FF.size(); i++){
			SPoint3 pt = _vn[i]->point();
			_L[j] -= _FF(i)*pt[j];
		}
	}
	
	_S.resize(1,9);
	_S.scale(0.);
	for (int i=0; i<3; i++){
		for (int j=0; j<3; j++){
			int index = Tensor23::getIndex(i,j);
			_S(0,index) = VecFollowed[i]*_L[j]/maxCoeff;
		}
	}
};

void directionalPBCLagrangeConstraintElement::getConstraintKeys(std::vector<Dof>& keys) const{
	getKeysFromVertex(_periodicSpace,_vp, getComp(), keys);
	for (int i=0; i<_vn.size(); i++){
		getKeysFromVertex(_periodicSpace,_vn[i], getComp(), keys);
	};
};

void directionalPBCLagrangeConstraintElement::getConstraintMatrix(fullMatrix<double>& m)const{
	m = _C;
};
void directionalPBCLagrangeConstraintElement::getDependentMatrix(fullMatrix<double>& m) const{
  m = _Cbc;
};

void directionalPBCLagrangeConstraintElement::print() const{
    printf("Directional Lagrange constraint element \n");
    printf("Directional Positive = %ld\n",_vp->getNum());
    for (int i=0; i<_vn.size(); i++){
      printf("%ld \t", _vn[i]->getNum());
    }
    printf("\n");
};

void directionalPBCLagrangeConstraintElement::getMultiplierKeys(std::vector<Dof>& key) const{
	std::vector<int> comp;
	comp.push_back(_positiveComp);
	getKeysFromVertex(_multSpace,_vp, comp,key);
};

void directionalPBCLagrangeConstraintElement::getDependentKeys(std::vector<Dof>& key) const{
	std::vector<Dof> allkeys;
	getConstraintKeys(allkeys);
	key.push_back(allkeys[_positiveComp]);
};

void directionalPBCLagrangeConstraintElement::getIndependentKeys(std::vector<Dof>& key) const{
  std::vector<Dof> allkeys;
	getConstraintKeys(allkeys);
	for (int i=0; i< allkeys.size(); i++){
		if (i != _positiveComp){
			key.push_back(allkeys[i]);
		}
	}
};

void directionalPBCLagrangeConstraintElement::getLinearConstraints(const fullVector<double>& g,
                   std::map<Dof,DofAffineConstraint<double> >& con) const
{
  std::vector<Dof> kP, kN;
  getDependentKeys(kP);
	getIndependentKeys(kN);
	
  DofAffineConstraint<double> cons;
	for (int i=0; i< kN.size(); i++){
		cons.linear.push_back(std::pair<Dof,double>(kN[i],_Cbc(0,i)));
	}
	cons.shift=g(0);
	con[kP[0]] = cons;
}

void directionalPBCLagrangeConstraintElement::getLinearConstraintsByVertices(std::map<Dof,DofAffineConstraint<double> >& con) const{
	Msg::Error("this function is not used for directionalPeriodicMeshConstraint::getLinearConstraintsByVertices");
};

void directionalPBCLagrangeConstraintElement::getKinematicalVector(fullVector<double>& m) const{
	fullVector<double> kinVec;
	_mbc->getKinematicalVector(kinVec);
	m.resize(1);
	m.setAll(0.);
	_S.mult(kinVec,m);
};
void directionalPBCLagrangeConstraintElement::getKinematicalMatrix(fullMatrix<double>& m) const{
  m = _S;
};
void directionalPBCLagrangeConstraintElement::getLinearConstraints(std::map<Dof,DofAffineConstraint<double> >& con) const{
  fullVector<double> g;
  getKinematicalVector(g);
  getLinearConstraints(g, con);
};


directionalFormLagrangeConstraintElement::directionalFormLagrangeConstraintElement(nonLinearMicroBC* mbc, FunctionSpaceBase* space, FunctionSpaceBase* mspace, 
															MVertex* v1, std::vector<MVertex*>& vlist, const SVector3& dir):
															constraintElement(mbc,-1), _periodicSpace(space),_multSpace(mspace),
															_vp(v1),_vn(vlist){
	if (_mbc->getOrder() == 2){
		Msg::Error("directionalPBCLagrangeConstraintElement has not been implemented for SecondOrder");	
	}
	for (int i=0; i<_vn.size(); i++){
		double dist = _vn[i]->distance(_vn[0]);
		_distance.push_back(dist);
	};				
	_s = _vp->distance(_vn[0]);
	
	int csize = (_vn.size()+1)*_ndofs;
	
	_C.resize(1.,csize); _C.setAll(0.0);
	int col = 0;
	for (int i=0; i<_ndofs; i++){
		_C(0,col) = 1.0*dir[i];
		col++;
	};
	lagrangeConstraintElement::getFF(_s,_distance,_FF);
	for (int i=0; i<_FF.size(); i++){
		for (int j=0; j<_ndofs; j++){
			_C(0,col) = -1.*_FF(i)*dir[j];
			col++;
		};
	};
	
	// find maximal value for 
	_positiveComp = -1;
	double maxCoeff = 0.;
	bool found = false;
	std::vector<Dof> keys;
	getKeysFromVertex(_periodicSpace,_vp, getComp(),keys); // all keys
	// find all in positive Dofs
	for (int i=0; i< _ndofs; i++){
		if ((fabs(_C(0,i)) > fabs(maxCoeff)) and 
			(constraintElement::allPositiveDof.find(keys[i]) == constraintElement::allPositiveDof.end())){
			maxCoeff = _C(0,i);
			_positiveComp = i;
			found = true;
		}
	}
	if (found){
		constraintElement::allPositiveDof.insert(keys[_positiveComp]);
	}
	else{
		Msg::Error("all positive vertex is considered directionalFormLagrangeConstraintElement vp = %d",_vp->getNum());
	}
	_C.scale(1./maxCoeff);
	
	_Cbc.resize(1,_C.size2()-1);
	_Cbc.setAll(0.);
	int colCbc = 0;
	for (int i=0; i<_C.size2(); i++){
		if (i !=_positiveComp){
			_Cbc(0,colCbc) = -_C(0,i);
			colCbc++;
		}
	};
	
	// periodic vector
	SPoint3 pp = _vp->point();
	for (int j=0; j<3; j++){
		_L[j] = pp[j];
		for (int i=0; i<_FF.size(); i++){
			SPoint3 pt = _vn[i]->point();
			_L[j] -= _FF(i)*pt[j];
		}
	}
	
	_S.resize(1,9);
	_S.scale(0.);
	for (int i=0; i<3; i++){
		for (int j=0; j<3; j++){
			int index = Tensor23::getIndex(i,j);
			_S(0,index) = dir[i]*_L[j]/maxCoeff;
		}
	}
};

void directionalFormLagrangeConstraintElement::getConstraintKeys(std::vector<Dof>& keys) const{
	getKeysFromVertex(_periodicSpace,_vp, getComp(), keys);
	for (int i=0; i<_vn.size(); i++){
		getKeysFromVertex(_periodicSpace,_vn[i], getComp(), keys);
	};
};

void directionalFormLagrangeConstraintElement::getConstraintMatrix(fullMatrix<double>& m)const{
	m = _C;
};
void directionalFormLagrangeConstraintElement::getDependentMatrix(fullMatrix<double>& m) const{
  m = _Cbc;
};

void directionalFormLagrangeConstraintElement::print() const{
    printf("Directional Lagrange constraint element \n");
    printf("Directional Positive = %ld\n",_vp->getNum());
    for (int i=0; i<_vn.size(); i++){
      printf("%ld \t", _vn[i]->getNum());
    }
    printf("\n");
};

void directionalFormLagrangeConstraintElement::getMultiplierKeys(std::vector<Dof>& key) const{
	std::vector<int> comp;
	comp.push_back(_positiveComp);
	getKeysFromVertex(_multSpace,_vp, comp,key);
};

void directionalFormLagrangeConstraintElement::getDependentKeys(std::vector<Dof>& key) const{
	std::vector<Dof> allkeys;
	getConstraintKeys(allkeys);
	key.push_back(allkeys[_positiveComp]);
};

void directionalFormLagrangeConstraintElement::getIndependentKeys(std::vector<Dof>& key) const{
  std::vector<Dof> allkeys;
	getConstraintKeys(allkeys);
	for (int i=0; i< allkeys.size(); i++){
		if (i != _positiveComp){
			key.push_back(allkeys[i]);
		}
	}
};

void directionalFormLagrangeConstraintElement::getLinearConstraints(const fullVector<double>& g,
                   std::map<Dof,DofAffineConstraint<double> >& con) const
{
  std::vector<Dof> kP, kN;
  getDependentKeys(kP);
	getIndependentKeys(kN);
	
  DofAffineConstraint<double> cons;
	for (int i=0; i< kN.size(); i++){
		cons.linear.push_back(std::pair<Dof,double>(kN[i],_Cbc(0,i)));
	}
	cons.shift=g(0);
	con[kP[0]] = cons;
}

void directionalFormLagrangeConstraintElement::getLinearConstraintsByVertices(std::map<Dof,DofAffineConstraint<double> >& con) const{
	Msg::Error("this function is not used for directionalPeriodicMeshConstraint::getLinearConstraintsByVertices");
};

void directionalFormLagrangeConstraintElement::getKinematicalVector(fullVector<double>& m) const{
	fullVector<double> kinVec;
	_mbc->getKinematicalVector(kinVec);
	m.resize(1);
	m.setAll(0.);
	_S.mult(kinVec,m);
};
void directionalFormLagrangeConstraintElement::getKinematicalMatrix(fullMatrix<double>& m) const{
  m = _S;
};
void directionalFormLagrangeConstraintElement::getLinearConstraints(std::map<Dof,DofAffineConstraint<double> >& con) const{
  fullVector<double> g;
  getKinematicalVector(g);
  getLinearConstraints(g, con);
};

shiftedLagrangeConstraintElement::shiftedLagrangeConstraintElement(nonLinearMicroBC* mbc, FunctionSpaceBase* space, FunctionSpaceBase* mspace, 
															const int c, MVertex* v1, std::vector<MVertex*>& vlist, const SVector3& normal):
															constraintElement(mbc,c), _periodicSpace(space),_multSpace(mspace),
															_vp(v1),_vn(vlist){
    
		std::vector<Dof> Keys;
    getKeysFromVertex(_periodicSpace,_vp, getComp(),Keys);
    for (int ik=0; ik < Keys.size(); ik++){
      if (constraintElement::allPositiveDof.find(Keys[ik]) == constraintElement::allPositiveDof.end()){
        constraintElement::allPositiveDof.insert(Keys[ik]);
      }
			else{
				Msg::Error("shiftedLagrangeConstraintElement Dof on vertex was chosen as positive one in other constraint element: vp = %d",_vp->getNum());
			}
    }

    for (int i=0; i<vlist.size(); i++){
      double dist = _vn[i]->distance(_vn[0]);
      _distance.push_back(dist);
    };
    SPoint3 pp = _vp->point();
    SPoint3 np1 = _vn[0]->point();
    SPoint3 np2 = _vn[_vn.size()-1]->point();
    SPoint3 point = planeDirProject(pp,np1,np2,normal);
		
		SVector3 pnp1(point,np1);
		SVector3 pnp2(point,np2);
		if (dot(pnp1,pnp2)  >= 0.){
			if (pnp2.norm() > pnp1.norm()){
				for (int i=0; i<3; i++){
					point[i] += (np2[i] - np1[i]);
				}
			}
			else{
				for (int i=0; i<3; i++){
					point[i] += (np1[i] - np2[i]);
				}
			}
		}
		_s = point.distance(np1);
    
    int size = _vn.size();
    _C.resize(_ndofs,(1+size)*_ndofs); _C.setAll(0.0);
    int col = 0;
    for (int i=0; i<_ndofs; i++){
        _C(i,col) = 1.0;
        col++;
    };
    lagrangeConstraintElement::getFF(_s,_distance,_FF);
    _Cbc.resize(_ndofs,size*_ndofs);
    _Cbc.setAll(0.);
    for (int i=0; i<_FF.size(); i++){
        for (int j=0; j<_ndofs; j++){
            _C(j,col) = -1.*_FF(i);
            _Cbc(j,col-_ndofs) = _FF(i);
            col++;
        };
    };
    for (int j=0; j<3; j++){
       _L[j] = pp[j];
       for (int i=0; i<_FF.size(); i++){
          SPoint3 pt = _vn[i]->point();
          _L[j] -= _FF(i)*pt[j];
       }
    }
		
		if (_mbc->getOrder() == 2){
			SPoint3 positive = _vp->point();
			SPoint3 np1 = _vn[0]->point();
			SPoint3 np2 = _vn[_vn.size()-1]->point();
			SPoint3 negative = project(positive,np1,np2);

			for (int i=0; i<3; i++)
				for (int j=0; j<3; j++)
					_XX(i,j) = 0.5*(positive[i]*positive[j]);

			for (int k=0; k<_FF.size(); k++){
				SPoint3 pt = _vn[k]->point();
				for (int i=0; i<3; i++)
					for (int j=0; j<3; j++)
						_XX(i,j) -= _FF(k)*0.5*(pt[i]*pt[j]);
			}
		}
		
		_mbc->getPBCKinematicMatrix(_component,_L,_XX,_S);
};

void shiftedLagrangeConstraintElement::getConstraintKeys(std::vector<Dof>& keys) const{
	getKeysFromVertex(_periodicSpace,_vp, getComp(), keys);
	for (int i=0; i<_vn.size(); i++){
		getKeysFromVertex(_periodicSpace,_vn[i], getComp(), keys);
	};
};

void shiftedLagrangeConstraintElement::getMultiplierKeys(std::vector<Dof>& keys) const{
	getKeysFromVertex(_multSpace,_vp, getComp(), keys);
};

void shiftedLagrangeConstraintElement::getConstraintMatrix(fullMatrix<double>& m)const{
	m = _C;
};
void shiftedLagrangeConstraintElement::getDependentMatrix(fullMatrix<double>& m) const{
  m = _Cbc;
};

void shiftedLagrangeConstraintElement::print() const{
    printf("Lagrange constraint element \n");
    printf("Positive = %ld\n",_vp->getNum());
    for (int i=0; i<_vn.size(); i++){
      printf("%ld \t", _vn[i]->getNum());
    }
    printf("\n");
};



void shiftedLagrangeConstraintElement::getDependentKeys(std::vector<Dof>& keys) const{
  getKeysFromVertex(_periodicSpace,_vp, getComp(),keys);
}; // left real dofs

void shiftedLagrangeConstraintElement::getIndependentKeys(std::vector<Dof>& keys) const{
  for (int i=0; i<_vn.size(); i++){
		getKeysFromVertex(_periodicSpace,_vn[i], getComp(), keys);
	};
};

void shiftedLagrangeConstraintElement::getLinearConstraints(const fullVector<double>& g,
                   std::map<Dof,DofAffineConstraint<double> >& con) const
{
  std::vector<std::vector<Dof> > k;
  for (int i=0; i<_vn.size(); i++){
    std::vector<Dof> ktemp;
    getKeysFromVertex(_periodicSpace,_vn[i], getComp(),ktemp);
    k.push_back(ktemp);
  };

  std::vector<Dof> kp;
  getKeysFromVertex(_periodicSpace,_vp, getComp(),kp);

  DofAffineConstraint<double> cons;
  for (int i=0; i<_ndofs; i++){
    for (int j=0; j<_vn.size(); j++){
      cons.linear.push_back(std::pair<Dof,double>(k[j][i],_FF(j)));
    };
    cons.shift=g(i);
    con[kp[i]] = cons;
    cons.linear.clear();
  };
};

void shiftedLagrangeConstraintElement::getLinearConstraintsByVertices(std::map<Dof,DofAffineConstraint<double> >& con) const{
	Msg::Error("the root vertices are not defined shiftedLagrangeConstraintElement::getLinearConstraintsByVertices");
	return;
};

void shiftedLagrangeConstraintElement::getKinematicalVector(fullVector<double>& m) const{
	fullVector<double> kinVec;
	_mbc->getKinematicalVector(kinVec);
	m.resize(_ndofs);
	m.setAll(0.);
	_S.mult(kinVec,m);
};
void shiftedLagrangeConstraintElement::getKinematicalMatrix(fullMatrix<double>& m) const{
  m = _S;
};
void shiftedLagrangeConstraintElement::getLinearConstraints(std::map<Dof,DofAffineConstraint<double> >& con) const{
  fullVector<double> g;
  getKinematicalVector(g);
  getLinearConstraints(g, con);
};