//
//
// Description: supplement constraint elements
//
// Author:  <Van Dung NGUYEN>, (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
//

#include "pbcSupplementConstraint.h"
#include "pbcCreateConstraints.h"

void supplementConstraint::computeWeight(const elementGroup*g, std::vector<MVertex*>& v, fullVector<double>& weight){
	int sizever = g->vsize();
  v.resize(sizever);
  // number all vertices
  std::map<MVertex*,int> allVertices;
  for (elementGroup::vertexContainer::const_iterator it = g->vbegin(); it!= g->vend(); it++){
    MVertex* vertex = it->second;
    int size = allVertices.size();
    allVertices[vertex] = size;
    v[size] = vertex;
  }
	
  weight.resize(sizever);
  weight.setAll(0.);
  // integration rule
  GaussQuadrature integrationRule(GaussQuadrature::Val);

  IntPt* GP;
  double jac[3][3];
  double surface =0;
  double val[256];
  // Use a Space to compute Jacobian and shape Function values
  for (elementGroup::elementContainer::const_iterator it = g->begin(); it!= g->end(); it++){
    MElement* ele = it->second;
    int numVer = ele->getNumVertices();
    int npts = integrationRule.getIntPoints(ele,&GP);
    for (int i=0; i<npts; i++){
      double u = GP[i].pt[0]; double v = GP[i].pt[1]; double w = GP[i].pt[2];
      double wgt = GP[i].weight;
      double detJ = ele->getJacobian(u, v, w, jac);
      double ratio = wgt*detJ;
      surface+= ratio;
      ele->getShapeFunctions(u,v,w,val);
      for (int j=0; j<numVer; j++){
        MVertex* vertex= ele->getVertex(j);
        weight(allVertices[vertex]) += val[j]*ratio;
      }
    }
  }
  weight.scale(1./surface);	
};

supplementConstraint::supplementConstraint(nonLinearMicroBC* mbc, FunctionSpaceBase* space, FunctionSpaceBase* mspace,
												const int c,elementGroup* g, const scalarWeightFunction* fct):
									supplementConstraintBase(mbc, space,mspace,c),_g(g){
  if (fct == NULL){
    _weightFunction = new constantScalarWeightFunction(1.);
  }
  else{
    _weightFunction = fct->clone();
  }
	supplementConstraint::computeWeight(_g,_v,_weight);
	
	int sizever = _v.size();
	
  // update with weightfunction
  for (int i=0; i< sizever; i++){
    _weight(i) *= _weightFunction->getVal(_v[i]);
  }

  // to obatain a good positive vertex
  std::vector<Dof> oneDofPerVertex;
  for (int i=0; i< _v.size(); i++){
    std::vector<Dof> keys;
    getKeysFromVertex(_periodicSpace,_v[i],getComp(),keys);
    oneDofPerVertex.push_back(keys[0]);
  }
  _positive = -1;
	double maxVal = 0.;
	bool found = false;
  for (int i=0; i<sizever; i++){
    if ((fabs(_weight(i))>maxVal) && (constraintElement::allPositiveDof.find(oneDofPerVertex[i]) == constraintElement::allPositiveDof.end())){
      _positive = i;
			maxVal = fabs(_weight(i));
			found = true;
    }
  };
	if (!found){
		Msg::Error("All Dof are numerated as positive dof in other constraints supplementConstraint::supplementConstraint");
	}

  // add positive to positive vertex list
  std::vector<Dof> Keys;
  getKeysFromVertex(_periodicSpace,_v[_positive],getComp(),Keys);
  for (int ik=0; ik < Keys.size(); ik++){
    if (constraintElement::allPositiveDof.find(Keys[ik]) == constraintElement::allPositiveDof.end()){
      constraintElement::allPositiveDof.insert(Keys[ik]);
    }
    else{
      Msg::Warning("Dof on vertex was chosen as positive one in other constraint element:");
    }
  }

  double invs = 1./(_weight(_positive));
  _factor = _weight(_positive);
  _weight.scale(invs);
  STensorOperation::zero(_Xmean);
  for (int i=0; i<_v.size();i++){
    MVertex* v = _v[i];
    SPoint3 pnt = v->point();
    for( int j=0; j<3; j++){
      _Xmean[j] += _weight(i)*pnt[j];
    }
  }
	
	if (_mbc->getOrder() == 2){
		STensorOperation::zero(_XXmean);
		for (int i=0; i<_v.size();i++){
			MVertex* v = _v[i];
			SPoint3 pnt = v->point();
			for( int j=0; j<3; j++){
				for (int k=0; k<3; k++){
					_XXmean(j,k) += _weight(i)*0.5*pnt[j]*pnt[k];
				}
			}
		}
	}
	
  // add a virtual vertex for identify the constraint
  _tempVertex = new MVertex(_v[_positive]->x(),_v[_positive]->y(),_v[_positive]->z());
				
	_C.resize(_ndofs,sizever*_ndofs);
	_C.setAll(0.);

	_Cbc.resize(_ndofs, (sizever-1)*_ndofs);
	_Cbc.setAll(0.);

	int col = 0;
	int colbc = 0;
	for (int i=0; i<_weight.size(); i++){
		for (int j=0; j<_ndofs; j++){
			_C(j,col) = _weight(i);
			if (i != _positive){
				_Cbc(j,colbc) = -1.*_weight(i);
				colbc++;
			}
			col++;
		}
	}
	
	_mbc->getPointKinematicMatrix(_component,_Xmean,_XXmean,_S);
};


void supplementConstraint::getConstraintKeys(std::vector<Dof>& key) const {
  for (int i=0; i<_v.size(); i++)
      getKeysFromVertex(_periodicSpace,_v[i],getComp(),key);
}
void supplementConstraint::getMultiplierKeys(std::vector<Dof>& key) const{
  getKeysFromVertex(_multSpace,_tempVertex,getComp(),key);
}
void supplementConstraint::getConstraintMatrix(fullMatrix<double>& m) const {
  m = _C;
};		// matrix C
void supplementConstraint::getDependentMatrix(fullMatrix<double>& m) const{
  m = _Cbc;
}

// for constraint form  u = C u* + Sb*FM + M uc
void supplementConstraint::getDependentKeys(std::vector<Dof>& keys) const {
  getKeysFromVertex(_periodicSpace,_v[_positive],getComp(),keys);
}; // left real dofs

void supplementConstraint::getIndependentKeys(std::vector<Dof>& keys) const{
  for (int i=0; i<_v.size(); i++)
    if (i!= _positive)
      getKeysFromVertex(_periodicSpace,_v[i],getComp(),keys);
}



void supplementConstraint::getLinearConstraints(const fullVector<double>& g,
                   std::map<Dof,DofAffineConstraint<double> >& con) const
{

  std::vector<std::vector<Dof> > k;
  for (int i=0; i< _v.size(); i++){
    std::vector<Dof> ktemp;
    getKeysFromVertex(_periodicSpace,_v[i],getComp(),ktemp);
    k.push_back(ktemp);
  };

  std::vector<Dof> kp;
  this->getDependentKeys(kp);

  DofAffineConstraint<double> cons;
  for (int i=0; i<_ndofs; i++){
    for (int j=0; j<_v.size(); j++){
      if (j!= _positive)
        cons.linear.push_back(std::pair<Dof,double>(k[j][i],-1.*_weight(j)));
    };
    cons.shift=g(i);
    con[kp[i]] = cons;
    cons.linear.clear();
  };
};

void supplementConstraint::print() const{
  Msg::Info("Positive = %ld",_v[_positive]->getNum());
  Msg::Info("COnstraint numbered = %ld",_tempVertex->getNum());
  for (int i=0; i<_v.size(); i++){
    printf("%ld \t",_v[i]->getNum());
  }
  printf("\n");
};

void supplementConstraint::getKinematicalVector(fullVector<double>& m) const{
  fullVector<double> kinVec;
	_mbc->getKinematicalVector(kinVec);
	m.resize(_ndofs);
	m.setAll(0.);
	_S.mult(kinVec,m);
};
void supplementConstraint::getKinematicalMatrix(fullMatrix<double>& m) const{
  m = _S;
};
void supplementConstraint::getLinearConstraints(std::map<Dof,DofAffineConstraint<double> >& con) const{

  fullVector<double> g;
  getKinematicalVector(g);
  getLinearConstraints(g, con);
};


periodicSupplementConstraint::periodicSupplementConstraint(nonLinearMicroBC* mbc, FunctionSpaceBase* space, FunctionSpaceBase* mspace,
												const int c, elementGroup* gplus, elementGroup* gminus):
									supplementConstraintBase(mbc, space,mspace,c),_gPlus(gplus),_gMinus(gminus){
  std::vector<MVertex*> vPlus, vMinus;
  fullVector<double> weightMinus,weightPlus;
                    
	supplementConstraint::computeWeight(_gPlus,vPlus,weightPlus);
  supplementConstraint::computeWeight(_gMinus,vMinus,weightMinus);
  
  int sizeMinus = vMinus.size();
  int sizePlus = vPlus.size();
  int sizever = sizePlus+sizeMinus;
  
  _v.resize(sizever);
  _weight.resize(sizever);
  for (int i=0; i< sizePlus; i++){
    _v[i] = vPlus[i];
    _weight(i) = weightPlus(i);
  }
  for (int i=0; i< sizeMinus; i++){
    _v[i+sizePlus] = vMinus[i];
    _weight(i+sizePlus) = -weightMinus(i);
  }
	
  // to obatain a good positive vertex
  std::vector<Dof> oneDofPerVertex;
  for (int i=0; i< _v.size(); i++){
    std::vector<Dof> keys;
    getKeysFromVertex(_periodicSpace,_v[i],getComp(),keys);
    oneDofPerVertex.push_back(keys[0]);
  }
  _positive = -1;
	double maxVal = 0.;
	bool found = false;
  for (int i=0; i<sizever; i++){
    if ((fabs(_weight(i))>maxVal) && (constraintElement::allPositiveDof.find(oneDofPerVertex[i]) == constraintElement::allPositiveDof.end())){
      _positive = i;
			maxVal = fabs(_weight(i));
			found = true;
    }
  };
	if (!found){
		Msg::Error("All Dof are numerated as positive dof in other constraints periodicSupplementConstraint::periodicSupplementConstraint");
	}

  // add positive to positive vertex list
  std::vector<Dof> Keys;
  getKeysFromVertex(_periodicSpace,_v[_positive],getComp(),Keys);
  for (int ik=0; ik < Keys.size(); ik++){
    if (constraintElement::allPositiveDof.find(Keys[ik]) == constraintElement::allPositiveDof.end()){
      constraintElement::allPositiveDof.insert(Keys[ik]);
    }
    else{
      Msg::Warning("Dof on vertex was chosen as positive one in other constraint element:");
    }
  }

  double invs = 1./(_weight(_positive));
  _weight.scale(invs);
  STensorOperation::zero(_Xmean);
  for (int i=0; i<_v.size();i++){
    MVertex* v = _v[i];
    SPoint3 pnt = v->point();
    for( int j=0; j<3; j++){
      _Xmean[j] += _weight(i)*pnt[j];
    }
  }
	
	if (_mbc->getOrder() == 2){
		STensorOperation::zero(_XXmean);
		for (int i=0; i<_v.size();i++){
			MVertex* v = _v[i];
			SPoint3 pnt = v->point();
			for( int j=0; j<3; j++){
				for (int k=0; k<3; k++){
					_XXmean(j,k) += _weight(i)*0.5*pnt[j]*pnt[k];
				}
			}
		}
	}
	
  // add a virtual vertex for identify the constraint
  _tempVertex = new MVertex(_v[_positive]->x(),_v[_positive]->y(),_v[_positive]->z());
				
	_C.resize(_ndofs,sizever*_ndofs);
	_C.setAll(0.);

	_Cbc.resize(_ndofs, (sizever-1)*_ndofs);
	_Cbc.setAll(0.);

	int col = 0;
	int colbc = 0;
	for (int i=0; i<_weight.size(); i++){
		for (int j=0; j<_ndofs; j++){
			_C(j,col) = _weight(i);
			if (i != _positive){
				_Cbc(j,colbc) = -1.*_weight(i);
				colbc++;
			}
			col++;
		}
	}
	
	_mbc->getPointKinematicMatrix(_component,_Xmean,_XXmean,_S);
};


void periodicSupplementConstraint::getConstraintKeys(std::vector<Dof>& key) const {
  for (int i=0; i<_v.size(); i++)
      getKeysFromVertex(_periodicSpace,_v[i],getComp(),key);
}
void periodicSupplementConstraint::getMultiplierKeys(std::vector<Dof>& key) const{
  getKeysFromVertex(_multSpace,_tempVertex,getComp(),key);
}
void periodicSupplementConstraint::getConstraintMatrix(fullMatrix<double>& m) const {
  m = _C;
};		// matrix C
void periodicSupplementConstraint::getDependentMatrix(fullMatrix<double>& m) const{
  m = _Cbc;
}

// for constraint form  u = C u* + Sb*FM + M uc
void periodicSupplementConstraint::getDependentKeys(std::vector<Dof>& keys) const {
  getKeysFromVertex(_periodicSpace,_v[_positive],getComp(),keys);
}; // left real dofs

void periodicSupplementConstraint::getIndependentKeys(std::vector<Dof>& keys) const{
  for (int i=0; i<_v.size(); i++)
    if (i!= _positive)
      getKeysFromVertex(_periodicSpace,_v[i],getComp(),keys);
}



void periodicSupplementConstraint::getLinearConstraints(const fullVector<double>& g,
                   std::map<Dof,DofAffineConstraint<double> >& con) const
{

  std::vector<std::vector<Dof> > k;
  for (int i=0; i< _v.size(); i++){
    std::vector<Dof> ktemp;
    getKeysFromVertex(_periodicSpace,_v[i],getComp(),ktemp);
    k.push_back(ktemp);
  };

  std::vector<Dof> kp;
  this->getDependentKeys(kp);

  DofAffineConstraint<double> cons;
  for (int i=0; i<_ndofs; i++){
    for (int j=0; j<_v.size(); j++){
      if (j!= _positive)
        cons.linear.push_back(std::pair<Dof,double>(k[j][i],-1.*_weight(j)));
    };
    cons.shift=g(i);
    con[kp[i]] = cons;
    cons.linear.clear();
  };
};

void periodicSupplementConstraint::print() const{
  Msg::Info("periodicSupplementConstraint Positive = %d",_v[_positive]->getNum());
  Msg::Info("COnstraint numbered = %d",_tempVertex->getNum());
  for (int i=0; i<_v.size(); i++){
    printf("%ld \t",_v[i]->getNum());
  }
  printf("\n");
};

void periodicSupplementConstraint::getKinematicalVector(fullVector<double>& m) const{
  fullVector<double> kinVec;
	_mbc->getKinematicalVector(kinVec);
	m.resize(_ndofs);
	m.setAll(0.);
	_S.mult(kinVec,m);
};
void periodicSupplementConstraint::getKinematicalMatrix(fullMatrix<double>& m) const{
  m = _S;
};
void periodicSupplementConstraint::getLinearConstraints(std::map<Dof,DofAffineConstraint<double> >& con) const{

  fullVector<double> g;
  getKinematicalVector(g);
  getLinearConstraints(g, con);
};




lagrangeSupplementConstraint::lagrangeSupplementConstraint(nonLinearMicroBC* mbc, FunctionSpaceBase* space, FunctionSpaceBase* mspace, 
												const int c, std::vector<MVertex*>& v): 
												supplementConstraintBase(mbc,space,mspace,c),
												_v(v),_tempVertex(0){
  std::vector<double> base;
  int n = _v.size();
  for (int i=0; i<n; i++){
    double val = _v[i]->distance(_v[0]);
    base.push_back(val);
  };
  lagrangeSupplementConstraint::integrateFF(base,_weight);

  // to obatain a good positive vertex
  std::vector<Dof> oneDofPerVertex;
  for (int i=0; i< _v.size(); i++){
    std::vector<Dof> keys;
    getKeysFromVertex(_periodicSpace,_v[i], getComp(),keys);
    oneDofPerVertex.push_back(keys[0]);
  }
  _positive = -1;
	double maxVal = 0.;
	bool found = false;
  for (int i=0; i<n; i++){
   if ((fabs(_weight(i))> maxVal) && (constraintElement::allPositiveDof.find(oneDofPerVertex[i]) == constraintElement::allPositiveDof.end())){
			maxVal = fabs(_weight(i));
      _positive = i;
			found = true;
    }
  };
	if (!found){
		Msg::Error("All Dof are numerated as positive dof in other constraints lagrangeSupplementConstraint::lagrangeSupplementConstraint");
	}

  // add positive to positive vertex list
  std::vector<Dof> Keys;
  getKeysFromVertex(_periodicSpace,_v[_positive],getComp(),Keys);
  for (int ik=0; ik < Keys.size(); ik++){
    if (constraintElement::allPositiveDof.find(Keys[ik]) == constraintElement::allPositiveDof.end()){
      constraintElement::allPositiveDof.insert(Keys[ik]);
    }
    else{
      Msg::Warning("Dof on vertex was chosen as positive one in other constraint element:");
    }
  }

  double invs = 1./(_weight(_positive));
  _factor = _weight(_positive);
  _weight.scale(invs);
  STensorOperation::zero(_Xmean);
  for (int i=0; i<n;i++){
    SPoint3 pnt = _v[i]->point();
    for( int j=0; j<3; j++){
      _Xmean[j] += _weight(i)*pnt[j];
    }
  }
	
	if (_mbc->getOrder() == 2){
		STensorOperation::zero(_XXmean);
		for (int i=0; i<_v.size();i++){
			MVertex* v = _v[i];
			SPoint3 pnt = v->point();
			for( int j=0; j<3; j++){
				for (int k=0; k<3; k++){
					_XXmean(j,k) += _weight(i)*0.5*pnt[j]*pnt[k];
				}
			}
		}
	}
	
  // add a virtual vertex for identify the constraint
  _tempVertex = new MVertex(_v[_positive]->x(),_v[_positive]->y(),_v[_positive]->z());
  _C.resize(_ndofs,n*_ndofs);
  _C.setAll(0.);
  _Cbc.resize(_ndofs, (n-1)*_ndofs);
  _Cbc.setAll(0.);

  int col = 0;
  int colbc = 0;
  for (int i=0; i<n; i++){
    for (int j=0; j<_ndofs; j++){
      _C(j,col) = _weight(i);
      if (i != _positive){
        _Cbc(j,colbc) = -1.*_weight(i);
        colbc++;
      }
      col++;
    }
  }
	
	_mbc->getPointKinematicMatrix(_component,_Xmean,_XXmean, _S);
};

void lagrangeSupplementConstraint::integrateFF(const std::vector<double>& base, fullVector<double>& integ){
  int n = base.size();
  integ.resize(n);
  integ.setAll(0.);

  int N=15;

  double s0 = base[0];
  double sn = base[n-1];

  double delta = (sn-s0)/N;
  fullVector<double> horizon(N+1);
  for (int j=0; j<=N; j++)
    horizon(j) = s0+j*delta;
  std::vector<fullVector<double> > allVals;
  for (int j=0; j<=N; j++){
    fullVector<double> val;
    lagrangeConstraintElement::getFF(horizon(j),base,val);
    allVals.push_back(val);
  }

  for (int i=0; i<n; i++){
    integ(i) = 0;
    for (int j=0; j<N; j++){
      integ(i) += (horizon(j+1)-horizon(j))*(allVals[j+1](i)+allVals[j](i))*0.5;
    }
  }
}


void lagrangeSupplementConstraint::getConstraintKeys(std::vector<Dof>& key) const {
  for (int i=0; i<_v.size(); i++)
    getKeysFromVertex(_periodicSpace,_v[i], getComp(),key);
}
void lagrangeSupplementConstraint::getMultiplierKeys(std::vector<Dof>& key) const{
  getKeysFromVertex(_multSpace,_tempVertex, getComp(), key);
}
void lagrangeSupplementConstraint::getConstraintMatrix(fullMatrix<double>& m) const {
  m = _C;
};		// matrix C
void lagrangeSupplementConstraint::getDependentMatrix(fullMatrix<double>& m) const{
  m = _Cbc;
};

// for constraint form  u = C u* + Sb*FM + M uc
void lagrangeSupplementConstraint::getDependentKeys(std::vector<Dof>& keys) const {
  getKeysFromVertex(_periodicSpace,_v[_positive],getComp(), keys);
}; // left real dofs

void lagrangeSupplementConstraint::getIndependentKeys(std::vector<Dof>& keys) const{
  for (int i=0; i<_v.size(); i++)
    if (i!= _positive)
      getKeysFromVertex(_periodicSpace,_v[i], getComp(),keys);
}


void lagrangeSupplementConstraint::getLinearConstraints(const fullVector<double>& g,
                   std::map<Dof,DofAffineConstraint<double> >& con) const
{
  std::vector<std::vector<Dof> > k;
  for (int i=0; i< _v.size(); i++){
    std::vector<Dof> ktemp;
    getKeysFromVertex(_periodicSpace,_v[i], getComp(),ktemp);
    k.push_back(ktemp);
  };

  std::vector<Dof> kp;
  this->getDependentKeys(kp);

  DofAffineConstraint<double> cons;
  for (int i=0; i<_ndofs; i++){
    for (int j=0; j<_v.size(); j++){
      if (j!= _positive)
        cons.linear.push_back(std::pair<Dof,double>(k[j][i],-1.*_weight(j)));
    };
    cons.shift=g(i);
    con[kp[i]] = cons;
    cons.linear.clear();
  };
};

void lagrangeSupplementConstraint::print() const{
  Msg::Info("Positive = %d",_v[_positive]->getNum());
  Msg::Info("COnstraint numbered = %d",_tempVertex->getNum());
  for (int i=0; i<_v.size(); i++){
    printf("%ld \t",_v[i]->getNum());
  }
  printf("\n");
};

void lagrangeSupplementConstraint::getKinematicalVector(fullVector<double>& m) const{
  fullVector<double> kinVec;
	_mbc->getKinematicalVector(kinVec);
	m.resize(_ndofs);
	m.setAll(0.);
	_S.mult(kinVec,m);
};
void lagrangeSupplementConstraint::getKinematicalMatrix(fullMatrix<double>& m) const{
  m = _S;
};
void lagrangeSupplementConstraint::getLinearConstraints(std::map<Dof,DofAffineConstraint<double> >& con) const{
  fullVector<double> g;
  getKinematicalVector(g);
  getLinearConstraints(g, con);
};


cubicSplineSupplementConstraint::cubicSplineSupplementConstraint(nonLinearMicroBC* mbc, FunctionSpaceBase* space, FunctionSpaceBase* mspace, 
												const int c, std::vector<MVertex*>& v, int flag):
												supplementConstraintBase(mbc, space,mspace,c),
												_v(v),_flag(flag),_tempVertex(NULL){
  std::vector<double> base;
  int n = _v.size();
  for (int i=0; i<n; i++){
    double val = _v[i]->distance(_v[0]);
    base.push_back(val);
  };

  _weight.resize(2*n);
  _weight.setAll(0.);
  for (int i=0; i<n-1; i++){
    fullVector<double> integ;
    cubicSplineSupplementConstraint::integrateFF(base[i],base[i+1],integ);
    _weight(2*i+0) += integ(0);
    _weight(2*i+1) += integ(1);
    _weight(2*i+2) += integ(2);
    _weight(2*i+3) += integ(3);
  }
  //_weight.print("weight");

  // to obatain a good positive vertex
  std::vector<Dof> oneDofPerVertex;
  for (int i=0; i< _v.size(); i++){
    std::vector<Dof> keys;
    getKeysFromVertex(_periodicSpace,_v[i], getComp(),keys);
    oneDofPerVertex.push_back(keys[0]);
  }

  _positive = -1;
	double maxVal = 0.;
	bool found = false;
  for (int i=0; i<n; i++){
    if ((fabs(_weight(2*i))>maxVal) && (constraintElement::allPositiveDof.find(oneDofPerVertex[i]) == constraintElement::allPositiveDof.end())){
      _positive = i;
			maxVal = fabs(_weight(2*i));
			found = true;
    }
  };
	if (!found){
		Msg::Error("All Dof are numerated as positive dof in other constraints lagrangeSupplementConstraint::lagrangeSupplementConstraint");
	}

  std::vector<Dof> Keys;
  getKeysFromVertex(_periodicSpace,_v[_positive], getComp(),Keys);
  for (int ik=0; ik < Keys.size(); ik++){
    if (constraintElement::allPositiveDof.find(Keys[ik]) == constraintElement::allPositiveDof.end()){
      constraintElement::allPositiveDof.insert(Keys[ik]);
    }
    else{
      Msg::Warning("Dof on vertex was chosen as positive one in other constraint element:");
    }
  }


  //Msg::Info("positive= %d",_positive);

  double invs = 1./(_weight(2.*_positive));
  _factor = _weight(2.*_positive);
  _weight.scale(invs);

  //_weight.print("weight");

  STensorOperation::zero(_Xmean);
  for (int i=0; i<n;i++){
    SPoint3 pnt = _v[i]->point();
    for( int j=0; j<3; j++){
      _Xmean[j] += _weight(2*i)*pnt[j];
    }
  }
	if (_mbc->getOrder() == 2){
		STensorOperation::zero(_XXmean);
		for (int i=0; i<_v.size();i++){
			MVertex* v = _v[i];
			SPoint3 pnt = v->point();
			for( int j=0; j<3; j++){
				for (int k=0; k<3; k++){
					_XXmean(j,k) += _weight(2*i)*0.5*pnt[j]*pnt[k];
				}
			}
		}
	}
	
  // add a virtual vertex for identify the constraint
  _tempVertex = new MVertex(_v[_positive]->x(),_v[_positive]->y(),_v[_positive]->z());
  
  _C.resize(_ndofs,2*n*_ndofs);
  _C.setAll(0.);
  _Cbc.resize(_ndofs, (2*n-1)*_ndofs);
  _Cbc.setAll(0.);

  int col = 0;
  int colbc = 0;
  for (int i=0; i<2*n; i++){
    for (int j=0; j<_ndofs; j++){
      _C(j,col) = _weight(i);
      if (i != 2*_positive){
        _Cbc(j,colbc) = -1.*_weight(i);
        colbc++;
      }
      col++;
    }
  }
	
	_mbc->getPointKinematicMatrix(_component,_Xmean,_XXmean,_S);
  //_C.print("Matrix C");
  //_Cbc.print("Matrix Cbc");

};
void cubicSplineSupplementConstraint::getConstraintKeys(std::vector<Dof>& key) const{
  int n = _v.size();
  for (int i=0; i<n; i++){
    getKeysFromVertex(_periodicSpace,_v[i], getComp(),key);
    cubicSplineConstraintElement::addKeysToVertex(_periodicSpace,_v[i], getComp(),key,_flag);
  }
};

void cubicSplineSupplementConstraint::getMultiplierKeys(std::vector<Dof>& key) const{
  getKeysFromVertex(_multSpace,_tempVertex,getComp(),key);
};

void cubicSplineSupplementConstraint::getConstraintMatrix(fullMatrix<double>& m) const{
  m = _C;
};		// matrix C
void cubicSplineSupplementConstraint::getDependentMatrix(fullMatrix<double>& m) const{
  m = _Cbc;
};

void cubicSplineSupplementConstraint::getDependentKeys(std::vector<Dof>& key) const{
  getKeysFromVertex(_periodicSpace,_v[_positive],getComp(),key);
}; // left real dofs

void cubicSplineSupplementConstraint::getIndependentKeys(std::vector<Dof>& key) const{
  int n = _v.size();
  for (int i=0; i<n; i++){
    if (i != _positive)
      getKeysFromVertex(_periodicSpace,_v[i],getComp(),key);
    cubicSplineConstraintElement::addKeysToVertex(_periodicSpace,_v[i],getComp(),key,_flag);
  }
};


void cubicSplineSupplementConstraint::getLinearConstraints(const fullVector<double>& g,
                   std::map<Dof,DofAffineConstraint<double> >& con) const
{
  Msg::Error("This is not implemented");
};

void cubicSplineSupplementConstraint::getKinematicalVector(fullVector<double>& m) const{
  fullVector<double> kinVec;
	_mbc->getKinematicalVector(kinVec);
	m.resize(_ndofs);
	m.setAll(0.);
	_S.mult(kinVec,m);
};
void cubicSplineSupplementConstraint::getKinematicalMatrix(fullMatrix<double>& m) const{
  m = _S;
};
void cubicSplineSupplementConstraint::getLinearConstraints(std::map<Dof,DofAffineConstraint<double> >& con) const{
  fullVector<double> g;
  getKinematicalVector(g);
  getLinearConstraints(g, con);
};

CoonsPatchLagrangeSupplementConstraint::CoonsPatchLagrangeSupplementConstraint(nonLinearMicroBC* mbc, FunctionSpaceBase* space, FunctionSpaceBase* mspace,
																					const int c, lagrangeSupplementConstraint* conX,
                                           lagrangeSupplementConstraint* conY):
																	supplementConstraintBase(mbc,space,mspace,c),_vroot(NULL){
  std::vector<MVertex*>& _vx = conX->getVertexList();
  std::vector<MVertex*>& _vy = conY->getVertexList();

  for (int i=0; i<_vx.size(); i++){
    for (int j =0; j<_vy.size(); j++){
      if (_vy[j] == _vx[i]){
        _vroot = _vx[i];
        break;
      }
    }
    if (_vroot) break;
  }
  for (int i=0; i<_vx.size(); i++){
    _v.push_back(_vx[i]);
  }
  for (int i=0; i<_vy.size(); i++){
    _v.push_back(_vy[i]);
  }
  _v.push_back(_vroot);

  fullVector<double>& weightX = conX->getWeight();
  fullVector<double>& weightY = conY->getWeight();

  double lengthX = _vx[0]->distance(_vx[_vx.size()-1]);
  double lengthY = _vy[0]->distance(_vy[_vy.size()-1]);

  double factorX = conX->getConstraintFactor();
  double factorY = conY->getConstraintFactor();

  _weight.resize(weightX.size()+ weightY.size()+1);
  for (int i=0; i< weightX.size(); i++){
    _weight(i) = weightX(i)* factorX*lengthY;
  }
  for (int i=0; i< weightY.size(); i++){
    _weight(i+weightX.size()) = weightY(i)*factorY*lengthX;
  }
  _weight(weightX.size()+ weightY.size()) = -1.*lengthX*lengthY;


  _tempVertex = new MVertex(_vroot->x(),_vroot->y(), _vroot->z());


  // to obatain a good positive vertex
  std::vector<Dof> oneDofPerVertex;
  for (int i=0; i< _v.size(); i++){
    std::vector<Dof> keys;
    getKeysFromVertex(_periodicSpace,_v[i], getComp(),keys);
    oneDofPerVertex.push_back(keys[0]);
  }

  int n = _v.size();
  _positive = -1;
	double maxVal = 0.;
	bool found = false;
	
  for (int i=0; i<n; i++){
    if ((fabs(_weight(i))>maxVal) &&
        (constraintElement::allPositiveDof.find(oneDofPerVertex[i]) == constraintElement::allPositiveDof.end())){
      _positive = i;
			maxVal = fabs(_weight(i));
			found = true;
    }
  };
	if (!found){
		Msg::Error("All Dof are numerated as positive dof in other constraints CoonsPatchLagrangeSupplementConstraint::CoonsPatchLagrangeSupplementConstraint");
	}

  std::vector<Dof> Keys;
  getKeysFromVertex(_periodicSpace,_v[_positive], getComp(),Keys);
  for (int ik=0; ik < Keys.size(); ik++){
    if (constraintElement::allPositiveDof.find(Keys[ik]) == constraintElement::allPositiveDof.end()){
      constraintElement::allPositiveDof.insert(Keys[ik]);
    }
    else{
      Msg::Warning("Dof on vertex was chosen as positive one in other constraint element:");
    }
  }

  double invs = 1./(_weight(_positive));
  _factor = _weight(_positive);
  _weight.scale(invs);
  STensorOperation::zero(_Xmean);
  for (int i=0; i<n;i++){
    SPoint3 pnt = _v[i]->point();
    for( int j=0; j<3; j++){
      _Xmean[j] += _weight(i)*pnt[j];
    }
  }
	
	if (_mbc->getOrder() == 2){
		STensorOperation::zero(_XXmean);
		for (int i=0; i<_v.size();i++){
			MVertex* v = _v[i];
			SPoint3 pnt = v->point();
			for( int j=0; j<3; j++){
				for (int k=0; k<3; k++){
					_XXmean(j,k) += _weight(i)*0.5*pnt[j]*pnt[k];
				}
			}
		}
	}
	
  // add a virtual vertex for identify the constraint
  _tempVertex = new MVertex(_v[_positive]->x(),_v[_positive]->y(),_v[_positive]->z());
  
  _C.resize(_ndofs,n*_ndofs);
  _C.setAll(0.);
  _Cbc.resize(_ndofs, (n-1)*_ndofs);
  _Cbc.setAll(0.);

  int col = 0;
  int colbc = 0;
  for (int i=0; i<n; i++){
    for (int j=0; j<_ndofs; j++){
      _C(j,col) = _weight(i);
      if (i != _positive){
        _Cbc(j,colbc) = -1.*_weight(i);
        colbc++;
      }
      col++;
    }
  }
	
	_mbc->getPointKinematicMatrix(_component,_Xmean,_XXmean,_S);
}

void CoonsPatchLagrangeSupplementConstraint::getConstraintKeys(std::vector<Dof>& key) const{
  for (int i=0; i<_v.size(); i++)
    getKeysFromVertex(_periodicSpace,_v[i],getComp(),key);
}; // real dofs on constraint elements
void CoonsPatchLagrangeSupplementConstraint::getMultiplierKeys(std::vector<Dof>& key) const{
  getKeysFromVertex(_multSpace,_tempVertex,getComp(),key);
};	// multiplier dof on constraint element
void CoonsPatchLagrangeSupplementConstraint::getConstraintMatrix(fullMatrix<double>& m) const{
  m = _C;
};		// matrix C
void CoonsPatchLagrangeSupplementConstraint::getDependentMatrix(fullMatrix<double>& m) const{
  m = _Cbc;
};

void CoonsPatchLagrangeSupplementConstraint::getDependentKeys(std::vector<Dof>& keys) const {
  getKeysFromVertex(_periodicSpace,_v[_positive],getComp(),keys);
}; // left real dofs

void CoonsPatchLagrangeSupplementConstraint::getIndependentKeys(std::vector<Dof>& keys) const{
  for (int i=0; i<_v.size(); i++)
    if (i!= _positive)
      getKeysFromVertex(_periodicSpace,_v[i],getComp(),keys);
}

void CoonsPatchLagrangeSupplementConstraint::getLinearConstraints(const fullVector<double>& g,
                   std::map<Dof,DofAffineConstraint<double> >& con) const
{
  std::vector<std::vector<Dof> > k;
  for (int i=0; i< _v.size(); i++){
    std::vector<Dof> ktemp;
    getKeysFromVertex(_periodicSpace,_v[i],getComp(),ktemp);
    k.push_back(ktemp);
  };

  std::vector<Dof> kp;
  this->getDependentKeys(kp);

  DofAffineConstraint<double> cons;
  for (int i=0; i<_ndofs; i++){
    for (int j=0; j<_v.size(); j++){
      if (j!= _positive)
        cons.linear.push_back(std::pair<Dof,double>(k[j][i],-1.*_weight(j)));
    };
    cons.shift=g(i);
    con[kp[i]] = cons;
    cons.linear.clear();
  };
};

void CoonsPatchLagrangeSupplementConstraint::getKinematicalVector(fullVector<double>& m) const{
  fullVector<double> kinVec;
	_mbc->getKinematicalVector(kinVec);
	m.resize(_ndofs);
	m.setAll(0.);
	_S.mult(kinVec,m);
};
void CoonsPatchLagrangeSupplementConstraint::getKinematicalMatrix(fullMatrix<double>& m) const{
  m = _S;
};
void CoonsPatchLagrangeSupplementConstraint::getLinearConstraints(std::map<Dof,DofAffineConstraint<double> >& con) const{
  fullVector<double> g;
  getKinematicalVector(g);
  getLinearConstraints(g, con);
};


supplementConstraintGeneral::supplementConstraintGeneral(nonLinearMicroBC* mbc, FunctionSpaceBase* mspace, 
										const int c, elementGroupDomainDecomposition* bgr, const scalarWeightFunction* func): 
						constraintElement(mbc,c),_g(bgr),_multSpace(mspace){
	if (_ndofs > 1) Msg::Error("supplementConstraintGeneral is implemented for 1 Dof only, please use nc =1 and give a value for c");
	
	// create map Dof
	std::map<Dof, int> mapDof; // used for assembling
	std::map<Dof, MVertex*> allVers;
	for (std::map<partDomain*,elementGroup*>::iterator it = _g->decompositionMap.begin(); it!=_g->decompositionMap.end(); it++){
		partDomain* dom = it->first;
		elementGroup* gr = it->second;
		for (elementGroup::vertexContainer::const_iterator itv = gr->vbegin(); itv != gr->vend(); itv++){
			MVertex* v = itv->second;
			std::vector<Dof> keys;
			getKeysFromVertex(dom->getFunctionSpace(),v,getComp(),keys);
			if (mapDof.find(keys[0]) == mapDof.end()){
				int mapsize = mapDof.size();
				mapDof[keys[0] ] = mapsize;
				allVers[keys[0]] = v;
				_allDof.push_back(keys[0]);
			}
		}
	};
	
	// constraint matrix
	_C.resize(1,_allDof.size());
	_C.setAll(0.);
	
	IntPt* GP;
  double jac[3][3];
  double surface =0;
  double val[256];
	GaussQuadrature integrationRule(GaussQuadrature::ValVal);
  // Use a Space to compute Jacobian and shape Function values
  for (std::map<partDomain*,elementGroup*>::iterator it = _g->decompositionMap.begin(); it!=_g->decompositionMap.end(); it++){
		partDomain* dom = it->first;
		elementGroup* gr = it->second;
		for (elementGroup::elementContainer::const_iterator ite = gr->begin(); ite != gr->end(); ite++){
			MElement* ele = ite->second;
			int npts = integrationRule.getIntPoints(ele,&GP);
			for (int i=0; i<npts; i++){
				double u = GP[i].pt[0]; 
				double v = GP[i].pt[1]; 
				double w = GP[i].pt[2];
				double wgt = GP[i].weight;
				double detJ = ele->getJacobian(u, v, w, jac);
				double ratio = wgt*detJ;
				surface+= ratio;
				ele->getShapeFunctions(u,v,w,val);
				for (int j=0; j<ele->getNumVertices(); j++){
					MVertex* vertex= ele->getVertex(j);
					std::vector<Dof> keys;
					getKeysFromVertex(dom->getFunctionSpace(),vertex,getComp(),keys);
					_C(0,mapDof[keys[0]]) += val[j]*ratio;
				}
			}
		}
    
  }
	_C.scale(1./surface);

	if (func != NULL){
		// update with weightfunction
		for (std::map<Dof,int>::iterator it = mapDof.begin(); it!=mapDof.end(); it++){
			_C(0,it->second) *= func->getVal(allVers[it->first]);
		}
	}

	// find positive
	_positive = -1;
  double maxVal = 0.;
	bool found = false;
  for (int i=0; i< _allDof.size(); i++){
    if ((fabs(_C(0,i))>maxVal) && (constraintElement::allPositiveDof.find(_allDof[i]) == constraintElement::allPositiveDof.end())){
      _positive = i;
			maxVal = fabs(_C(0,i));
			found = true;
    };
  };
	if (!found) Msg::Error("all Dofs in this constraint are numbered as positive dof in other constraints supplementConstraintGeneral::supplementConstraintGeneral");
	
	_tempVertex = new MVertex(allVers[_allDof[_positive]]->x(),allVers[_allDof[_positive]]->y(),allVers[_allDof[_positive]]->z());

	
	maxVal = _C(0,_positive);
	_C.scale(1./maxVal);
	
	_Cbc.resize(1,_allDof.size()-1);
	_Cbc.setAll(0.);
	int col = 0;
	for (int i=0; i< _allDof.size(); i++){
		if (i!=_positive){
			_Cbc(0,col) = -1.*_C(0,i);
			col++;
		}
	}

  // add positive to positive vertex list
  constraintElement::allPositiveDof.insert(_allDof[_positive]);


  SVector3 Xmean(0.);
  for (std::map<Dof,MVertex*>::iterator it = allVers.begin(); it!= allVers.end(); it++){
    SPoint3 pnt = it->second->point();
    for( int j=0; j<3; j++){
      Xmean[j] += _C(0,mapDof[it->first])*pnt[j];
    }
  }
	
	STensor3 XXmean(0.);
	if (_mbc->getOrder() == 2){
		for (std::map<Dof,MVertex*>::iterator it = allVers.begin(); it!= allVers.end(); it++){
			SPoint3 pnt = it->second->point();
			for( int j=0; j<3; j++){
				for (int k=0; k<3; k++){
					XXmean(j,k) += _C(0,mapDof[it->first])*0.5*pnt[j]*pnt[k];
				}
			}
		}
	}
	
	_mbc->getPointKinematicMatrix(_component,Xmean,XXmean,_S);

};

void supplementConstraintGeneral::getConstraintKeys(std::vector<Dof>& keys) const {
	 // real dofs on constraint elements
	 for (int i=0; i< _allDof.size(); i++){
		 keys.push_back(_allDof[i]);
	 }
};
void supplementConstraintGeneral::getMultiplierKeys(std::vector<Dof>& key) const {
		// multiplier dof on constraint element
		getKeysFromVertex(_multSpace,_tempVertex,getComp(),key);
};
void supplementConstraintGeneral::getConstraintMatrix(fullMatrix<double>& m) const {
			// matrix C
	 m = _C;
};
void supplementConstraintGeneral::getDependentMatrix(fullMatrix<double>& m) const{
	m = _Cbc;
};
void supplementConstraintGeneral::getDependentKeys(std::vector<Dof>& keys) const{
	 // left real dofs
	 keys.push_back(_allDof[_positive]);
};
void supplementConstraintGeneral::getIndependentKeys(std::vector<Dof>& keys) const{
	 for (int i=0; i< _allDof.size(); i++){
		 if (i!=_positive)
			keys.push_back(_allDof[i]);
	 }
};
void supplementConstraintGeneral::getLinearConstraints(const fullVector<double>& g,
                   std::map<Dof,DofAffineConstraint<double> >& con) const{
	//
	DofAffineConstraint<double> cons;
	for (int j=0; j<_allDof.size(); j++){
		if (j!=_positive){
			cons.linear.push_back(std::pair<Dof,double>(_allDof[j],-1.*_C(0,j)));
		}
	};
	cons.shift=g(0);
	con[_allDof[_positive]] = cons;
};
void supplementConstraintGeneral::print() const{
	Msg::Info("supplement constraint element");
};

bool supplementConstraintGeneral::isActive(const MVertex* v) const{
	for (std::map<partDomain*,elementGroup*>::iterator it = _g->decompositionMap.begin(); it!=_g->decompositionMap.end(); it++){
		partDomain* dom = it->first;
		elementGroup* gr = it->second;
		for (elementGroup::vertexContainer::const_iterator itv = gr->vbegin(); itv != gr->vend(); itv++){
			MVertex* vv = itv->second;
			if (v == vv) return true;
		}
	}
	return false;
};
		
void supplementConstraintGeneral::getKinematicalVector(fullVector<double>& m) const{
	fullVector<double> kinVec;
	_mbc->getKinematicalVector(kinVec);
	m.resize(_ndofs);
	m.setAll(0.);
	_S.mult(kinVec,m);
};
void supplementConstraintGeneral::getKinematicalMatrix(fullMatrix<double>& m) const{
	m = _S;
}
void supplementConstraintGeneral::getLinearConstraints(std::map<Dof,DofAffineConstraint<double> >& con) const{
	fullVector<double> g;
  getKinematicalVector(g);
  getLinearConstraints(g, con);
}
