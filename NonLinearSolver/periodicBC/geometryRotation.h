//
//
// Description: apply rotation operator on RVE
//
// Author:  <Van Dung NGUYEN>, (C) 2016
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef GEOMETRYROTATION_H_INCLUDED
#define GEOMETRYROTATION_H_INCLUDED

#include "GModel.h"


class GeometryRotation{
  #ifndef SWIG
  protected:
    virtual void exportRotatedModel(const SVector3& e1, const SVector3& e2, const SVector3& e3, // reference coordinate system
                                  GModel* pModel, const std::string filePrefix,
                                  const SVector3& n1, const SVector3& n2, const SVector3& n3 // rotated coordinate system
                                  );
  #endif // SWIG
  public:
    GeometryRotation();
    virtual void exportRotatedModel(const double e1x, const double e1y, const double e1z,
                                  const double e2x, const double e2y, const double e2z,
                                  const double e3x, const double e3y, const double e3z,
                                  const std::string inMeshFile,
                                  const double n1x, const double n1y, const double n1z,
                                  const double n2x, const double n2y, const double n2z,
                                  const double n3x, const double n3y, const double n3z);
    #ifndef SWIG
    virtual ~GeometryRotation();

    static void rotateGModel(const std::string filePrefix,
                            GModel* pModel,
                            const SVector3& n1, const SVector3& n2, const SVector3& n3);
    #endif // SWIG
};
#endif // GEOMETRYROTATION_H_INCLUDED
