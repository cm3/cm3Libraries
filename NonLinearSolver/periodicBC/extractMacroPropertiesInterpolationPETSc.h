//
//
// Description: class using PETSc for extraction of homogenized tangents
// in case of using polynomial interpolation method to enforce PBC
//
// Author:  <Van Dung NGUYEN>, (C) 2010
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef EXTRACTMACROPROPERTIESINTERPOLATIONPETSC_H_
#define EXTRACTMACROPROPERTIESINTERPOLATIONPETSC_H_

#include "extractMacroProperties.h"
#include "linearConstraintMatrixPETSc.h"
#include "splitStiffnessMatrixPETSc.h"
#include "GmshConfig.h"


#if defined(HAVE_PETSC)
#include "petscmat.h"
#include "petscvec.h"
#endif

class interpolationStiffnessCondensationPETSC : public stiffnessCondensation{
  #if defined(HAVE_PETSC)
  protected:
    nlsDofManager* _pAssembler;
    linearConstraintMatrixPETSc* _bConMat, *_cConMat;
    pbcAlgorithm* _pAl;
    bool _tangentflag, _stressflag;
    Mat _CT, _Sbc, _SbcT, _ScT;
    bool _isInitialized;
    MPI_Comm _comm;

  public:
    interpolationStiffnessCondensationPETSC(nlsDofManager* p, pbcAlgorithm* ma,
                                          bool sflag, bool tflag, MPI_Comm com = PETSC_COMM_SELF);
    virtual ~interpolationStiffnessCondensationPETSC();

    virtual void init();
    virtual void clear();
    virtual void setSplittedDofSystem(pbcAlgorithm* pal);
    virtual void stressSolve(fullVector<double>& stressVec, const double Vrve);
		virtual void tangentCondensationSolve(fullMatrix<double>& tangentMat, const double Vrve);

  #else
  public:
    interpolationStiffnessCondensationPETSC(nlsDofManager* p, pbcAlgorithm* ma,
                                            bool sflag, bool tflag){
      Msg::Error("Pestc is required");
    }
    virtual ~interpolationStiffnessCondensationPETSC(){}
    virtual void init(){};
    virtual void clear(){};
    virtual void setSplittedDofSystem(pbcAlgorithm* pal){};
    virtual void stressSolve(fullVector<double>& stressVec, const double Vrve){};
		virtual void tangentCondensationSolve(fullMatrix<double>& tangentMat, const double Vrve){};
  #endif //HAVE_PETSC
};

#endif // EXTRACTMACROPROPERTIESINTERPOLATIONPETSC_H_
