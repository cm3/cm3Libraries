//
//
// Description: create pbcElement in PBC problems
//
// Author:  <Van Dung NGUYEN>, (C) 2015
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef PBCELEMENT_H_
#define PBCELEMENT_H_

#include "MElement.h"

class partDomain;
class Dof;
class elementGroup;

class pbcVertex{
  // a class include one vertex _v in element _e inside domain _dom
  protected:
    MVertex* _v;
    MElement* _e;
    partDomain* _dom;

  public:
    pbcVertex(partDomain* dompart, MElement* ele, MVertex* vv);
    ~pbcVertex(){}
    partDomain* getDomain() const {return _dom;};
    MElement* getElement() const {return _e;};
    MVertex* getVertex() const {return _v;};
    void getKeys(std::vector<int>& comp, std::vector<Dof>& keys) const;
    static void createPBCVertices(MVertex* v, std::vector<partDomain*>& allDom, std::vector<pbcVertex*>& allPBCVertices);
    static void createPBCVertices(elementGroup& gr, std::vector<partDomain*>& allDom, std::set<pbcVertex*>& allPBCVertices);
};

class pbcElement
{
  protected:
    MElement* _eSub;
    MElement* _eBulk;
    partDomain* _dom;
  
  public:
    pbcElement(partDomain* dompart, MElement* eleBulk, MElement* eleFace);
    ~pbcElement(){};
    
    partDomain* getDomain() const {return _dom;};
    MElement* getBulkElement() const {return _eBulk;};
    MElement* getSubElement() const {return _eSub;};
    void getKeys(std::vector<int>& comp, std::vector<Dof>& keys) const;
    static void createPBCElements(elementGroup& gr, std::vector<partDomain*>& allDom, std::set<pbcElement*>& allPBCFace);
};

#endif // PBCELEMENT_H_
