//
//
// Description: numerical functions using for PBC
//
// Author:  <Van Dung NGUYEN>, (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef NUMERICALFUNCTIONS_H_
#define NUMERICALFUNCTIONS_H_
#include "SPoint3.h"
#include "SVector3.h"
#include "MVertex.h"
#include "elementGroup.h"
#include "functionSpace.h"
#include "MPoint.h"
#include "highOrderTensor.h"
#include "STensor53.h"
#include "STensor63.h"
#include "InterfaceKeys.h" 

inline bool checkInterfaceElementBelongToBulkElement(MElement* e, MElement* eleBulk){
  int nv=e->getNumVertices();
  std::vector<MVertex*> tabV;
  e->getVertices(tabV);
  int nn = eleBulk->getNumVertices();
  
  if (e->getNum() == eleBulk->getNum()){
    return true;
  }
  else if (e->getDim() == 0){
    for (int i=0; i< nn; i++){
      if (tabV[0]->getNum() == eleBulk->getVertex(i)->getNum()){
        return true;
      }
    }
  }
  else if (e->getDim() == 1){
    int nedge = eleBulk->getNumEdges();
    for(int kk=0;kk<nedge;kk++){
      std::vector<MVertex*> verEdge;
      eleBulk->getEdgeVertices(kk,verEdge);
      if((tabV[0]->getNum() == verEdge[0]->getNum() and tabV[1]->getNum() == verEdge[1]->getNum()) or 
         (tabV[0]->getNum() == verEdge[1]->getNum() and tabV[1]->getNum() == verEdge[0]->getNum())){
        return true;
      }
    }
  }
  else if (e->getDim() == 2){
    int nface = eleBulk->getNumFaces();
    for(int kk=0;kk<nface;kk++){
      std::vector<MVertex*> verFace;
      eleBulk->getFaceVertices(kk,verFace);
      int nFaceVer = verFace.size();
      if (nv == nFaceVer){
        interfaceKeys keyBound, keyVolume;
        if (e->getType() == TYPE_TRI){
          keyBound.setValues(e->getVertex(0)->getNum(),e->getVertex(1)->getNum(),e->getVertex(2)->getNum());
          keyVolume.setValues(verFace[0]->getNum(),verFace[1]->getNum(),verFace[2]->getNum());
        }
        else if (e->getType() == TYPE_QUA){
          keyBound.setValues(e->getVertex(0)->getNum(),e->getVertex(1)->getNum(),e->getVertex(2)->getNum(),e->getVertex(3)->getNum());
          keyVolume.setValues(verFace[0]->getNum(),verFace[1]->getNum(),verFace[2]->getNum(),verFace[3]->getNum());
        }
        else{
          Msg::Error("checkInterfaceElementBelongToBulkElement has been defined for tri and quad elements only, complete list for other element");
        }
        
        if (keyBound == keyVolume){
          return true;
        }
      }
    }
  }
  
  return false;
}

inline SPoint3 computeTwoLineIntersectionOnSurface(const SPoint3& A, const SPoint3& B, const SPoint3& C, const SPoint3& D){
  // Line AB and CD cut at I
  // ABCD on a same plane
  SVector3 AB(A,B);
  SVector3 CD(C,D);
  SVector3 facN = crossprod(AB,CD);
  SVector3 n = crossprod(CD,facN);
  SVector3 AD(A,D);
  double t = dot(AD,n)/dot(AB,n);
  
  double x = A.x() + t*AB.x();
  double y = A.y() + t*AB.y();
  double z = A.z() + t*AB.z();
  return SPoint3(x,y,z);
};

inline void computeSurface(const SPoint3& A, const SPoint3& B, const SPoint3& C,double &a, double& b, double &c, double &d){
  SVector3 AB(A,B);
  SVector3 AC(A,C);
  SVector3 normal = crossprod(AB,AC);
  normal.normalize();
  a = normal[0];
  b = normal[1];
  c = normal[2];
  d = a*A[0]+b*A[1]+c*A[2];
};

inline SPoint3 project(const SPoint3& P, const SPoint3& A, const SPoint3& B){
  SVector3 u(A,B);
  SVector3 Xpa(A,P);
  double t=dot(u,Xpa)/dot(u,u);
  SPoint3 temp(A.x()+t*u.x(),A.y()+t*u.y(),A.z()+t*u.z());
  return temp;
};

inline SPoint3 planeDirProject(const SPoint3& P, const SPoint3& A, const SPoint3& B, const SVector3& n){
	// n is normal of plane
  SVector3 u(A,B);
  SVector3 AP(A,P);
  double t=dot(AP,n)/dot(u,n);
  SPoint3 temp(A.x()+t*u.x(),A.y()+t*u.y(),A.z()+t*u.z());
  return temp;
};

inline SPoint3 project(const SPoint3 &P, const SPoint3 &A, const SPoint3& B, const SPoint3 & C){
  SVector3 vec1(A,B);
  SVector3 vec2(A,C);
  SVector3 normal=crossprod(vec1,vec2);
  SVector3 PA(P,A);
  double t=dot(normal,PA)/dot(normal,normal);
  SPoint3 temp(P.x()+t*normal.x(),P.y()+t*normal.y(),P.z()+t*normal.z());
  return temp;
};

inline SPoint3 project(const SPoint3& P, double a, double b, double c, double d)
{
  double t = -(a*P[0]+b*P[1]+c*P[2]+d)/(a*a+b*b+c*c);
  return SPoint3(P[0]+t*a,P[1]+t*b,P[2]+t*c);
}

inline double distance(const SPoint3& P, const SPoint3 &A, const SPoint3&  B){
  SPoint3 H=project(P,A,B);
  double temp=P.distance(H);
  return temp;
};

inline double distance(const SPoint3& P, const SPoint3 & A, const SPoint3 &B, const SPoint3 & C){
  SPoint3 H=project(P,A,B,C);
  double temp=P.distance(H);
  return temp;
};

// check P inside A, B
inline bool inside(const SPoint3 &P, const SPoint3& A, const SPoint3 &B){
  SVector3 PA(P,A);SVector3 PB(P,B);
  if (dot(PA,PB)<=0) return true;
  else return false;
};

inline SPoint3 midpoint(const SPoint3& A, const SPoint3& B){
  double x = (A.x()+B.x())/2;
  double y = (A.y()+B.y())/2;
  double z = (A.z()+B.z())/2;
  SPoint3 pt(x,y,z);
  return pt;
}

// check same side P, Q via A, B
inline bool sameSide(const SPoint3& P, const SPoint3 &Q, const SPoint3& A, const SPoint3& B){
  SVector3 PA(P,A);SVector3 PB(P,B);
  SVector3 n1=crossprod(PA,PB);

  SVector3 QA(Q,A); SVector3 QB(Q,B);
  SVector3 n2=crossprod(QA,QB);

  if (dot(n1,n2)<0) return false;
  else return true;
};

inline void getFF(double s, double s1, double s2, std::vector<double> &FF){
  FF.clear();
  double L=s2-s1;
  double r=(s-s1)/(s2-s1);
  FF.push_back(2*r*r*r-3*r*r+1);
  FF.push_back((r*r*r-2*r*r+r)*L);
  FF.push_back(3*r*r-2*r*r*r);
  FF.push_back((r*r*r-r*r)*L);
};


inline void getKeysFromVertex(FunctionSpaceBase* space, MVertex* v, const std::vector<int>& comp, std::vector<Dof>& keys){
	MPoint point(v,0,0);
	std::vector<Dof> allKeys;
	space->getKeys(&point,allKeys);
	for (int i=0; i< comp.size(); i++){
    if (comp[i] >= allKeys.size())
      Msg::Error("dof comp %d does not exist",comp[i]);
    else
      keys.push_back(allKeys[comp[i]]);
	}
};

inline double norm(const STensor3& ten){
	double temp = 0;
	for (int i=0; i<3; i++){
		for (int j=0; j<3; j++){
			double val = fabs(ten(i,j));
			if (val>temp){
				temp = val;
			};
		};
	};
	return temp;
};

inline void getMatrixFromVector(const SVector3& vec, fullMatrix<double>& mat){
	mat.resize(3,9);
	mat.setAll(0.0);
	for (int i=0; i<3; i++){
		for (int j=0; j<3; j++){
			int k = Tensor23::getIndex(i,j);
			mat(i,k) = vec(j);
		};
	};
};

inline void getMatrixFromTensor3(const STensor3& in, fullMatrix<double>& out){
	out.resize(3,27);
	out.setAll(0.0);
	for (int i=0; i<3; i++){
		for (int j=0; j<3; j++){
			for (int k=0; k<3; k++){
				int m = Tensor33::getIndex(i,j,k);
				out(i,m) = in(j,k);
			};
		};
	};
};

inline STensor3 vecprod(const SVector3& vec1, const SVector3& vec2){
  STensor3 ten(0.0);
  for (int i=0; i<3; i++){
    for (int j=0; j<3; j++){
      ten(i,j)= vec1(i)*vec2(j);
    };
  };
  return ten;
}

inline void multTensor3Vector(const STensor3& F, const SVector3& X, SVector3& result){
  for (int i=0; i<3; i++){
    result(i) = 0;
    for (int j=0; j<3; j++)
      result(i) += F(i,j)*X(j);
  }
}

static void printToFile(const fullMatrix<double>& mat, const std::string filename){
  FILE* file = fopen(filename.c_str(),"w");
  for (int i=0; i<mat.size1(); i++){
    for (int j=0; j<mat.size2(); j++){
      fprintf(file," \t%.16g\t" ,mat(i,j));
    }
    fprintf(file,"\n");
  }
	fclose(file);
}
static void printToFile(const fullVector<double>& vec, const std::string filename){
  FILE* file = fopen(filename.c_str(),"w");
  for (int i=0; i<vec.size(); i++){
    fprintf(file," \t%.16g\n" ,vec(i));
  }
	fclose(file);
}

static bool isInside(const SPoint3& P, MElement* e){
  int dim = e->getDim();
  std::vector<MVertex*> vv;
  e->getVertices(vv);

  SPoint3 pp;
  if (dim == 1){
    pp = project(P,vv[0]->point(), vv[1]->point());
  }
  else if (dim ==2){
    pp = project(P,vv[0]->point(), vv[1]->point(),vv[2]->point());
  }

  double xyz[3] = {pp[0],pp[1],pp[2]};
  double uvw[3];

  e->xyz2uvw(xyz,uvw);
  if (e->isInside(uvw[0],uvw[1],uvw[2])){
    return true;
  }
  else return false;

};

#endif // NUMERICALFUNCTIONS_H_
