//
//
// Description: create linear constraints for linear displacement BC
//
// Author:  <Van Dung NGUYEN>, (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef LINEARDISPLACEMENTBC_H_
#define LINEARDISPLACEMENTBC_H_

#include "functionSpace.h"
#include "staticDofManager.h"
#include "periodicConstraints.h"

class linearDisplacementBC : public periodicConstraintsBase{
  public:
    typedef std::set<MVertex*> vertexContainer;

  protected:
    vertexContainer allVertex; // entity for imposing PBC

  public:
    linearDisplacementBC(const int tag, const int ndofs, const int dim, nlsDofManager* p, std::vector<partDomain*>& allDom)
                        : periodicConstraintsBase(tag, ndofs,dim,p,allDom){};
    virtual ~linearDisplacementBC(){};
    virtual void setPeriodicGroup(std::vector<elementGroup*> group,int dim){
      gVertex.clear();
      for (int i=0; i<group.size(); i++){
        for (elementGroup::vertexContainer::const_iterator it=group[i]->vbegin(); it!=group[i]->vend(); it++){
          allVertex.insert(it->second);
        };
      };
    };

    virtual void applyPeriodicCondition(){
      Msg::Info("Imposed by linear displacemen boundary condition");
      for (vertexContainer::iterator it=allVertex.begin(); it!= allVertex.end(); it++){
        MVertex* v= *it;
        std::vector<Dof> k;
        getKeysFromVertex(v,k);
        fullVector<double> position; position.resize(3);
        position(0)= v->x();
        position(1)= v->y();
        position(2)= v->z();
        fullVector<double> g; g.resize(_ndofs);
        imposedDef.mult(position,g);
        int ndofs= k.size();
        for (int i=0; i<ndofs; i++)
          p->fixDof(k[i],g(i));
      };
    };

};

#endif // LINEARDISPLACEMENTBC_H_
