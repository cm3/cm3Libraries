//
//
// Description: class using fullMatrix for extraction of homogenized tangents
//
// Author:  <Van Dung NGUYEN>, (C) 2010
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef EXTRACTMACROPROPERTIESFULLMATRIX_H_
#define EXTRACTMACROPROPERTIESFULLMATRIX_H_

#include "extractMacroProperties.h"
#include "splitStiffnessMatrixFullMatrix.h"
#include "linearConstraintMatrixFullMatrix.h"


class stiffnessCondensationFullMatrixWithDirectConstraints : public stiffnessCondensation{
  protected:
    pbcAlgorithm* _pAl;
    bool _stressflag, _tangentflag;
    nlsDofManager* _pAssembler;
    linearConstraintMatrixFullMatrix* _bConMat, *_cConMat;
		fullMatrix<double>* _CbT,*_SbT, *_ScT, *_RT, *_CcT;
		bool _isInitialized;

  private:
    fullMatrix<double>* Kbbt,* Kbct,* Kcbt,*Kcct,*Kblamda,* Kccstar,*Sbc,*SbcT ,* ScTKccstar,*L1, *L;

  public:
    stiffnessCondensationFullMatrixWithDirectConstraints(nlsDofManager* pAssem, pbcAlgorithm* pal,bool sflag = false, bool tflag = true)
                                    : stiffnessCondensation(),_pAssembler(pAssem),_pAl(pal),
                                    _stressflag(sflag),_tangentflag(tflag),_bConMat(0),_cConMat(0),
                                    _isInitialized(false){}
    virtual ~stiffnessCondensationFullMatrixWithDirectConstraints(){clear();};
    virtual void init();
    virtual void clear();
    virtual void setSplittedDofSystem(pbcAlgorithm* pal);
    virtual void stressSolve(fullVector<double>& P, const double Vrve);
    virtual void tangentCondensationSolve(fullMatrix<double>& L, const double Vrve);
};

#endif // EXTRACTMACROPROPERTIESFULLMATRIX_H_
