//
//
// Description: apply rotation operator on RVE
//
// Author:  <Van Dung NGUYEN>, (C) 2016
//
// Copyright: See COPYING file that comes with this distribution
//
//

#include "geometryRotation.h"
#include "SVector3.h"
#include "Context.h"
#include <sstream>

GeometryRotation::GeometryRotation(){

}
GeometryRotation::~GeometryRotation(){
};

void GeometryRotation::exportRotatedModel(const SVector3& e1, const SVector3& e2, const SVector3& e3, // reference coordinate system
                                  GModel* pModel, const std::string filePrefix,
                                  const SVector3& n1, const SVector3& n2, const SVector3& n3 // rotated coordinate system
                                  ){
  STensor3 LeftHS(0.);
  STensor3 RightHS(0.);

  for (int i=0; i<3; i++){
    LeftHS(i,0) = e1(i);
    LeftHS(i,1) = e2(i);
    LeftHS(i,2) = e3(i);

    RightHS(i,0) = n1(i);
    RightHS(i,1) = n2(i);
    RightHS(i,2) = n3(i);
  }

  STensor3 R(0.);
  STensor3 invLeftHS = LeftHS.invert();
  for (int i=0; i<3; i++){
    for (int j=0; j<3; j++){
      for (int k=0; k<3; k++){
        R(i,j) += RightHS(i,k)*invLeftHS(k,j);
      }
    }
  }

  /*
  e1.print("e1");
  e2.print("e2");
  e3.print("e3");
  LeftHS.print("LeftHS");
  RightHS.print("RightHS");
  R.print("Rotation tensor");
  */


  std::vector<GEntity*> entities;
  pModel->getEntities(entities);

  std::set<MVertex*> allVertices;
  for(unsigned int i = 0; i < entities.size(); i++){
    for(unsigned int j = 0; j < entities[i]->mesh_vertices.size(); j++){
      allVertices.insert(entities[i]->mesh_vertices[j]);
    }
  }

  for (std::set<MVertex*>::iterator it = allVertices.begin(); it != allVertices.end(); it++){
    MVertex* v = *it;
    SVector3 P = v->point();
    SVector3 newP(0.,0.,0.);
    for (int i=0; i<3; i++){
      for (int j=0; j<3; j++){
        newP(i) += R(i,j)*P(j);
      }
    }
    v->x() = newP(0);
    v->y() = newP(1);
    v->z() = newP(2);
  }

  std::string newFileName = "";
  if (pModel->getFileName() != ""){
    std::string modelName = pModel->getFileName();
    size_t ext_pos = modelName.find_last_of('.');
    std::string newname(modelName,0,ext_pos);
    newFileName += newname;
  }
  newFileName += "Rotated_ELE"+filePrefix+".msh";
  // write deformed mesh to file
  printf("Begin writing rotated mesh %s\n",filePrefix.c_str());
  pModel->writeMSH(newFileName, CTX::instance()->mesh.mshFileVersion,CTX::instance()->mesh.binary, CTX::instance()->mesh.saveAll,
                  CTX::instance()->mesh.saveParametric, CTX::instance()->mesh.scalingFactor);
	printf("End writing rotated mesh %s \n",filePrefix.c_str());
};

void GeometryRotation::exportRotatedModel(const double e1x, const double e1y, const double e1z,
                                  const double e2x, const double e2y, const double e2z,
                                  const double e3x, const double e3y, const double e3z,
                                  const std::string inMeshFile,
                                  const double n1x, const double n1y, const double n1z,
                                  const double n2x, const double n2y, const double n2z,
                                  const double n3x, const double n3y, const double n3z) {
  SVector3 e1(e1x,e1y,e1z);
  SVector3 e2(e2x,e2y,e2z);
  SVector3 e3(e3x,e3y,e3z);

  SVector3 n1(n1x,n1y,n1z);
  SVector3 n2(n2x,n2y,n2z);
  SVector3 n3(n3x,n3y,n3z);

  e1.normalize();
  e2.normalize();
  e3.normalize();
  n1.normalize();
  n2.normalize();
  n3.normalize();

  GModel* pModel = new GModel();
  pModel->readMSH(inMeshFile.c_str());
  pModel->setFileName(inMeshFile);

  exportRotatedModel(e1,e2,e3,pModel,"",n1,n2,n3);

  delete pModel;
};


void GeometryRotation::rotateGModel(const std::string filePrefix,
                            GModel* pModel,
                            const SVector3& n1, const SVector3& n2, const SVector3& n3){
  SVector3 e1(1.,0.,0.);
  SVector3 e2(0.,1.,0.);
  SVector3 e3(0.,0.,1.);

  GeometryRotation obj;
  obj.exportRotatedModel(e1,e2,e3,pModel,filePrefix,n1,n2,n3);
};
