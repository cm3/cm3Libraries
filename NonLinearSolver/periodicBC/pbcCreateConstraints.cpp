//
//
// Description: create linear system from micro BCs
//
// Author:  <Van Dung NGUYEN>, (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "pbcCreateConstraints.h"
#include "numericalFunctions.h"
#include "MLine.h"
#include <string.h>
#include "MQuadrangle.h"
#include "nonLinearMechSolver.h"
#include "pbcConstraintElement.h"
#include "pbcCoonsPatchConstraintElement.h"
#include "pbcSupplementConstraint.h"
#include "pbcSupplementConstraintExtraDofDiffusion.h"
#include "directionalConstraintElement.h"
#include "MQuadrangle.h"
#include "MLine.h"
#include "staticDofManager.h"
#include "ThreeDLagrangeFunctionSpace.h"


void InterpolationOperations::groupIntersection(elementGroup* g1, elementGroup* g2, std::set<MVertex*>& ver){
  for (elementGroup::vertexContainer::const_iterator it1 = g1->vbegin(); it1!= g1->vend(); it1++){
    MVertex* v1 = it1->second;
    for (elementGroup::vertexContainer::const_iterator it2 = g2->vbegin(); it2!= g2->vend(); it2++){
      MVertex* v2 = it2->second;
      if (v1->getNum() ==v2->getNum()){
        ver.insert(v1);
      };
    };
  };
};

void InterpolationOperations::groupIntersection(std::set<MVertex*>& g1, std::set<MVertex*>& g2, std::set<MVertex*>& ver){
  for (std::set<MVertex*>::iterator it1 = g1.begin(); it1!= g1.end(); it1++){
    MVertex* v1 = *it1;
    for (std::set<MVertex*>::iterator it2 = g2.begin(); it2 != g2.end(); it2++){
      MVertex* v2 = *it2;
      if (v1==v2){
        ver.insert(v1);
      };
    };
  };
};

void InterpolationOperations::intersectionBetweenTwoFaces(const elementGroup* face1, const elementGroup* face2, elementGroup& line){

	std::vector<std::vector<MVertex*> > allEdges;
	for (elementGroup::elementContainer::const_iterator itL = face1->begin(); itL!= face1->end(); itL++){
		MElement* eleL = itL->second;
		if (eleL->getDim() != 2){
			Msg::Error("This fuction is implemented from 2D elements only");
		}
		bool found = false;
		for (int i=0; i< eleL->getNumEdges(); i++){
			std::vector<MVertex*> vvL;
			eleL->getEdgeVertices(i,vvL);
			// found in face3
			for (elementGroup::elementContainer::const_iterator itR = face2->begin(); itR!= face2->end(); itR++){
				MElement* eleR = itR->second;
				for (int j=0; j< eleR->getNumEdges(); j++){
					std::vector<MVertex*> vvR;
					eleR->getEdgeVertices(j,vvR);
					if (((vvL[0]->getNum() == vvR[0]->getNum()) and (vvL[1]->getNum() == vvR[1]->getNum())) or
							((vvL[0]->getNum() == vvR[1]->getNum()) and (vvL[1]->getNum() == vvR[0]->getNum()))){
						allEdges.push_back(vvL);
						found = true;
						break;
					}

				}
				if (found) break;
			}
			if (found) break;

		}
	}

	// add to grEle
	for (int i=0; i< allEdges.size(); i++){
		std::vector<MVertex*>& vv = allEdges[i];
		MElement* ele = new MLineN(vv);
		line.insert(ele);
	}
};
template<class Iterator>
void InterpolationOperations::createLinePolynomialBase(const int degree, // degree
                                const bool createNewVer,  // true if new vertices are created false if interpolation base is taken from existing vertices
                                MVertex* vstart, MVertex* vend, // vstart and vend 
                                Iterator itbegin, Iterator itend,  // current line
                                std::vector<MVertex*>& base, // base obtained
                                std::set<MVertex*>& virtVer // virtualVertices to store all virtual virtices which has been created
                                ){
  
  if (degree > 1){
    if (createNewVer){
      base.resize(degree+1);
      base[0] = vstart;
      base[degree] = vend;
    
      // create (degree-1) new vertices which are equally distributed
      double x0 = vstart->x(); double x1 = vend->x();
      double y0 = vstart->y(); double y1 = vend->y();
      double z0 = vstart->z(); double z1 = vend->z();

      double dx = (x1-x0)/degree;
      double dy = (y1-y0)/degree;
      double dz = (z1-z0)/degree;
      for (int j=0; j<degree-1; j++){
        double xi = x0+ (j+1)*dx;
        double yi = y0+ (j+1)*dy;
        double zi = z0+ (j+1)*dz;
        MVertex* v = new MVertex(xi,yi,zi);
        base[j+1] = v;
        virtVer.insert(v); // as new vertices
      };
    }
    else{
      // get (degree-1) vertices from existing vertices
      std::vector<MVertex*> list;
      std::vector<double> vals;
      for (Iterator it=itbegin; it!=itend; it++){
        MVertex* v=*it;
        if ((v->getNum() != vstart->getNum()) and (v->getNum() != vend->getNum())){
          list.push_back(v);
          vals.push_back(v->distance(vstart));        
        }
      };
      
      // reorder  based on distant to vstart
      int size=list.size();
      for (int i=0; i<size; i++){
        for (int j=i; j<size; j++){
          if (vals[j]<vals[i]){
            double temp=vals[i]; vals[i]=vals[j]; vals[j]=temp;
            MVertex* vtemp=list[i]; list[i]=list[j]; list[j]=vtemp;
          };
        };
      };
      // at leat
      if (degree-1>=size){
        base.resize(size+2);
        base[0] = vstart;
        base[size+1] = vend;
    
        Msg::Warning("order change from %d to %d: all vertices are taken in interpolation basis",degree,size+2);
        for (int i=0; i< size; i++){
          base[i+1] = list[i];
        }
      }
      else{
        base.resize(degree+1);
        base[0] = vstart;
        base[degree] = vend;
        int interval = (size)/(degree-1);
        for (int j=0; j<degree-1; j++){
          base[j+1]  = list[j*interval];
        };
      }
    }
  }
  else{
    base.resize(2);
    base[0] = vstart;
    base[1] = vend;
  }
};

template<class Iterator>
void InterpolationOperations::createLinePolynomialBaseItMap(const int degree, // degree
                                const bool createNewVer,  // true if new vertices are created false if interpolation base is taken from existing vertices
                                MVertex* vstart, MVertex* vend, // vstart and vend 
                                Iterator itbegin, Iterator itend,  // current line
                                std::vector<MVertex*>& base, // base obtained
                                std::set<MVertex*>& virtVer // virtualVertices to store all virtual virtices which has been created
                                ){
  
  if (degree > 1){
    if (createNewVer){
      base.resize(degree+1);
      base[0] = vstart;
      base[degree] = vend;
    
      // create (degree-1) new vertices which are equally distributed
      double x0 = vstart->x(); double x1 = vend->x();
      double y0 = vstart->y(); double y1 = vend->y();
      double z0 = vstart->z(); double z1 = vend->z();

      double dx = (x1-x0)/degree;
      double dy = (y1-y0)/degree;
      double dz = (z1-z0)/degree;
      for (int j=0; j<degree-1; j++){
        double xi = x0+ (j+1)*dx;
        double yi = y0+ (j+1)*dy;
        double zi = z0+ (j+1)*dz;
        MVertex* v = new MVertex(xi,yi,zi);
        base[j+1] = v;
        virtVer.insert(v); // as new vertices
      };
    }
    else{
      // get (degree-1) vertices from existing vertices
      std::vector<MVertex*> list;
      std::vector<double> vals;
      for (Iterator it=itbegin; it!=itend; it++){
        MVertex* v=it->second;
        if ((v->getNum() != vstart->getNum()) and (v->getNum() != vend->getNum())){
          list.push_back(v);
          vals.push_back(v->distance(vstart));        
        }
      };
      
      // reorder  based on distant to vstart
      int size=list.size();
      for (int i=0; i<size; i++){
        for (int j=i; j<size; j++){
          if (vals[j]<vals[i]){
            double temp=vals[i]; vals[i]=vals[j]; vals[j]=temp;
            MVertex* vtemp=list[i]; list[i]=list[j]; list[j]=vtemp;
          };
        };
      };
      // at leat
      if (degree-1>=size){
        base.resize(size+2);
        base[0] = vstart;
        base[size+1] = vend;
    
        Msg::Warning("order change from %d to %d: all vertices are taken in interpolation basis",degree,size+2);
        for (int i=0; i< size; i++){
          base[i+1] = list[i];
        }
      }
      else{
        base.resize(degree+1);
        base[0] = vstart;
        base[degree] = vend;
        int interval = (size)/(degree-1);
        for (int j=0; j<degree-1; j++){
          base[j+1]  = list[j*interval];
        };
      }
    }
  }
  else{
    base.resize(2);
    base[0] = vstart;
    base[1] = vend;
  }
};

void InterpolationOperations::createLinePolynomialBase(const int degree, // degree
                                const bool createNewVer,  // true if new vertices are created false if interpolation base is taken from existing vertices
                                MVertex* vstart, MVertex* vend, // vstart and vend 
                                std::set<MVertex*>& line,  // current line
                                std::vector<MVertex*>& base, // base obtained
                                std::set<MVertex*>& virtVer // virtualVertices to store all virtual virtices which has been created
                                ){
  InterpolationOperations::createLinePolynomialBase(degree,createNewVer,vstart,vend,line.begin(),line.end(),base,virtVer);
};

template<class Iterator>
MVertex* InterpolationOperations::findNearestVertexItMap(Iterator itbegin, Iterator itend, MVertex* vp){
  MVertex* vinit = itbegin->second;
  double dist=vinit->distance(vp);
  for (Iterator it=itbegin; it!=itend; it++){
    MVertex* v=it->second;
    double tempDist=v->distance(vp);
    if (tempDist<dist) {
      dist=tempDist;
      vinit=v;
    };
  };
  return vinit;
};


template<class Iterator>
MVertex* InterpolationOperations::findNearestVertex(Iterator itbegin, Iterator itend, MVertex* vp){
  MVertex* vinit = *itbegin;
  double dist=vinit->distance(vp);
  for (Iterator it=itbegin; it!=itend; it++){
    MVertex* v=*it;
    double tempDist=v->distance(vp);
    if (tempDist<dist) {
      dist=tempDist;
      vinit=v;
    };
  };
  return vinit;
};

MVertex* InterpolationOperations::findNearestVertex(elementGroup* g, MVertex* vp){

  return findNearestVertexItMap(g->vbegin(), g->vend(),vp);
};

void InterpolationOperations::createGroupLinearLineElementFromVerticesVector(std::vector<MVertex*>& b, elementGroup& gr){
  for (int i=0; i<b.size()-1; i++){
    MElement* line = new MLine(b[i],b[i+1]);
    gr.insert(line);
  }
};

void InterpolationOperations::createGroupQuadraticLineElementFromVerticesVector(std::vector<MVertex*>& b, elementGroup& gr, std::set<MVertex*>& virVertices){
  // base will be modified due to create new vertices
  std::vector<MVertex*> xtemp(b);
  b.clear();
  for (int i=0; i<xtemp.size()-1; i++){
    b.push_back(xtemp[i]);
    MVertex* v = new MVertex(0.5*(xtemp[i+1]->x()+xtemp[i]->x()),
                            0.5*(xtemp[i+1]->y()+xtemp[i]->y()),
                            0.5*(xtemp[i+1]->z()+xtemp[i]->z()));
    
    b.push_back(v);
    virVertices.insert(v);
    MElement* line = new MLine3(xtemp[i],xtemp[i+1],v);
    gr.insert(line);
  }
  // add last vertex
  b.push_back(xtemp[xtemp.size()-1]);
};

void InterpolationOperations::createLinearElementBCGroup3DFromTwoEdges( std::vector<MVertex*>& vlx, std::vector<MVertex*>& vly,
        MVertex* vc, elementGroup& g, std::set<MVertex*>& newVer){
  if (vlx[0] != vly[0]){
    Msg::Error("two BC group must be started by same vertex");
    return;
  }
  MVertex* vroot = vlx[0];

  int NX = vlx.size();
  int NY = vly.size();

  std::vector<std::vector<MVertex*> > vmat;
  vmat.push_back(vlx);

  for (int j=1; j<NY; j++){
    std::vector<MVertex*> vv;
    vv.push_back(vly[j]);
    for (int i=1; i< NX; i++){
      MVertex* vertex = NULL;
      if (j == NY-1 && i == NX-1){
        vertex = vc;
      }
      else{
       vertex = new MVertex(0,0,0);
       vertex->x() = vlx[i]->x() - vroot->x()+ vly[j]->x();
       vertex->y() = vlx[i]->y() - vroot->y()+ vly[j]->y();
       vertex->z() = vlx[i]->z() - vroot->z()+ vly[j]->z();
       newVer.insert(vertex);
      }
      vv.push_back(vertex);
    }
    vmat.push_back(vv);
  }


  for (int i=0; i<NX-1; i++){
    for (int j=0; j<NY-1; j++){
      MElement* e = new MQuadrangle(vmat[j][i],vmat[j][i+1],vmat[j+1][i+1],vmat[j+1][i]);
      g.insert(e);
    }
  }
 
};

void InterpolationOperations::createQuadraticElementBCGroup3DFromTwoEdges( std::vector<MVertex*>& vlx, std::vector<MVertex*>& vly,
        MVertex* vc, elementGroup& g, std::set<MVertex*>& newVer){
  if (vlx[0] != vly[0]){
    Msg::Error("two BC group must be started by same vertex");
    return;
  }
  MVertex* vroot = vlx[0];

  int NX = vlx.size();
  int NY = vly.size();

  std::vector<std::vector<MVertex*> > vmat;
  vmat.push_back(vlx);

  for (int j=1; j<NY; j++){
    std::vector<MVertex*> vv;
    vv.push_back(vly[j]);
    for (int i=1; i< NX; i++){
      MVertex* vertex = NULL;
      if (j == NY-1 && i == NX-1){
        vertex = vc;
      }
      else{
       vertex = new MVertex(0,0,0);
       vertex->x() = vlx[i]->x() - vroot->x()+ vly[j]->x();
       vertex->y() = vlx[i]->y() - vroot->y()+ vly[j]->y();
       vertex->z() = vlx[i]->z() - vroot->z()+ vly[j]->z();
       newVer.insert(vertex);
      }
      vv.push_back(vertex);
    }
    vmat.push_back(vv);
  }


  for (int i=0; i<NX-1; i+=2){
    for (int j=0; j<NY-1; j+=2){
      MElement* e = new MQuadrangle9(vmat[j][i],vmat[j][i+2],vmat[j+2][i+2],vmat[j+2][i],
                                    vmat[j][i+1],vmat[j+1][i+2],vmat[j+2][i+1],vmat[j+1][i],
                                    vmat[j+1][i+1]);
      g.insert(e);
    }
  }

};

void InterpolationOperations::createPolynomialBasePoints(nonLinearPeriodicBC* pbc,
                                  MVertex* v1, MVertex* v2, MVertex* v4, MVertex* v5,
                                  std::set<MVertex*>& l12, std::set<MVertex*>& l41,std::set<MVertex*>& l15,
                                  std::vector<MVertex*>& xbase, std::vector<MVertex*>& ybase, std::vector<MVertex*>& zbase,
                                  std::set<MVertex*>& newVertices){
  // direction x, y, z are defined as follows
  // x - 12
  // y - 14
  // z - 15  
  
  int dim = pbc->getDim();
  
  int degreeX = pbc->getPBCPolynomialDegree(0);
  int degreeY = pbc->getPBCPolynomialDegree(1);;
  int degreeZ = pbc->getPBCPolynomialDegree(2);;
  
  // modify degree following available vertices in l12, l14, l15
  if (!pbc->isForcedInterpolationDegree()){
    int sizeX = l12.size();
    int sizeY = l41.size();
    int sizeZ = l15.size();
    Msg::Info("Maximal number of Boundary Nodes in X direction %d, Y direction %d, Z direction %d",sizeX,sizeY,sizeZ);
    
    
    if (pbc->getPBCMethod() == nonLinearPeriodicBC::LIM or
        pbc->getPBCMethod() == nonLinearPeriodicBC::FE_LIN){
      // first order
      if (degreeX>sizeX-1) degreeX = sizeX-1;
      if (degreeY>sizeY-1) degreeY = sizeY-1;
      if (degreeZ>sizeZ-1 && dim == 3) degreeZ = sizeZ-1;
    }
    else if (pbc->getPBCMethod() == nonLinearPeriodicBC::CSIM or
        pbc->getPBCMethod() == nonLinearPeriodicBC::FE_QUA){
      // second-order
      if (degreeX>(sizeX-1)/2) degreeX = (sizeX-1)/2;
      if (degreeY>(sizeY-1)/2) degreeY = (sizeY-1)/2;
      if (degreeZ>(sizeZ-1)/2 && dim == 3) degreeZ = (sizeZ-1)/2;
    }
    else{
      Msg::Error("interpolation type must be defined");
    }
    // at leat, interpolation must be linear
    if (degreeX<1) degreeX = 1;
    if (degreeY<1) degreeY = 1;
    if (degreeZ<1) degreeZ = 1;

    // modify degree
    pbc->setPBCPolynomialDegree(0,degreeX);
    pbc->setPBCPolynomialDegree(1,degreeY);
    pbc->setPBCPolynomialDegree(2,degreeZ);
    
    // force add new vertices
    if (sizeX < 2) pbc->setAddNewVertices(0,true);
    if (sizeY < 2) pbc->setAddNewVertices(1,true);
    if (sizeZ < 2) pbc->setAddNewVertices(2,true);
    
  }

  Msg::Info("Interpolation degree used in X direction %d, Y direction %d, Z direction %d",degreeX,degreeY,degreeZ);

    
  InterpolationOperations::createLinePolynomialBase(degreeX,pbc->addNewVertices(0),v1,v2,l12,xbase,newVertices);
  InterpolationOperations::createLinePolynomialBase(degreeY,pbc->addNewVertices(1),v1,v4,l41,ybase,newVertices);
  if (dim == 3){
    InterpolationOperations::createLinePolynomialBase(degreeZ,pbc->addNewVertices(2),v1,v5,l15,zbase,newVertices);
  }
  
  
};

bool InterpolationOperations::isInside(MVertex* v, MElement* e){
  int dim = e->getDim();
  std::vector<MVertex*> vv;
  e->getVertices(vv);

  SPoint3 pp;
  if (dim == 1){
    pp = project(v->point(),vv[0]->point(), vv[1]->point());
  }
  else if (dim ==2){
    pp = project(v->point(),vv[0]->point(), vv[1]->point(),vv[2]->point());
  }

  double xyz[3] = {pp[0],pp[1],pp[2]};
  double uvw[3];

  e->xyz2uvw(xyz,uvw);
  if (e->isInside(uvw[0],uvw[1],uvw[2])){
    return true;
  }
  else return false;

};

auto creatLineInternalVertices = [](MVertex* vl, MVertex* vr, int order, std::set<MVertex*>& vset)
{
  double dX = vr->x() - vl->x();
  double dY = vr->y() - vl->y();
  double dZ = vr->z() - vl->z();
  double tol = 1e-8;
  
  std::vector<MVertex*> vlist;
  for (int i=0; i< order-1; i++)
  {
    double xp = vl->x()+(i+1.)*dX/(double(order));
    double yp = vl->y()+(i+1.)*dY/(double(order));
    double zp = vl->z()+(i+1.)*dZ/(double(order));
    
    bool newVer = true;
    MVertex* vtemp = new MVertex(xp,yp,zp);
    for (std::set<MVertex*>::iterator it = vset.begin(); it!= vset.end(); it++)
    {
      MVertex* v = *it;
      double l = v->distance(vtemp);
      if (l < tol)
      {
        delete vtemp;
        vtemp = v;
        newVer=  false;
        break;
      }
    }
    if (newVer)
    {
      vset.insert(vtemp);
    }
    vlist.push_back(vtemp);
  }
  return vlist;
};

void InterpolationOperations::create_MQuadrangleN_Base(int order, bool serendip,  MVertex* v1, MVertex* v2, MVertex* v3, MVertex* v4, 
                                                    MVertex* v5, MVertex* v6, MVertex* v7, MVertex* v8,
                                                    MElement*& eX, MElement*& eY, MElement*& eZ,
                                                    MElement*& eXY, MElement*& eYZ, MElement*& eZX,
                                                    std::set<MVertex*>& newBounderyVers, std::set<MVertex*>& allNewVers)
{
  if (order == 1)
  {
    eX = new MLine(v1,v2);
    eY = new MLine(v1,v4);
    eZ = new MLine(v1,v5);
    
    eXY = new MQuadrangle(v1,v4,v3,v2);
    eYZ = new MQuadrangle(v1,v5,v8,v4);
    eZX = new MQuadrangle(v1,v2,v6,v5);
  }
  else
  {
    // new vertex in line 1, 2
    std::vector<MVertex*> vl12, vl14, vl15;
    vl12 = creatLineInternalVertices(v1, v2, order,allNewVers);
    vl14 = creatLineInternalVertices(v1, v4, order,allNewVers);
    vl15 = creatLineInternalVertices(v1, v5, order,allNewVers);
    
    eX = new MLineN(v1,v2,vl12);
    eY = new MLineN(v1,v4,vl14);
    eZ = new MLineN(v1,v5,vl15);
    // for three face
    
    auto fillEdgeVertex = [](MVertex* v1, MVertex* v2, MVertex* v3, MVertex* v4,
                                         int order, std::set<MVertex*>& vset)
    {
      std::vector<MVertex*> vl12, vl23, vl34, vl41;
      vl12 = creatLineInternalVertices(v1, v2, order,vset);
      vl23 = creatLineInternalVertices(v2, v3, order,vset);
      vl34 = creatLineInternalVertices(v3, v4, order,vset);
      vl41 = creatLineInternalVertices(v4, v1, order,vset);
      
      std::vector<MVertex*> vin;
      vin.insert(vin.end(),vl12.begin(),vl12.end());
      vin.insert(vin.end(),vl23.begin(),vl23.end());
      vin.insert(vin.end(),vl34.begin(),vl34.end());
      vin.insert(vin.end(),vl41.begin(),vl41.end());
      
      return vin;
    };
    
    auto creatFaceInternalVertices = [&fillEdgeVertex](MVertex* v1, MVertex* v2, MVertex* v3, MVertex* v4,
                                         int order, bool serendip, std::set<MVertex*>& vset)
    {
      printf("serendip = %d order = %d\n",serendip,order);
      if (serendip)
      {
        return fillEdgeVertex(v1,v2,v3,v4,order,vset);
      }
      else
      {
        std::vector<MVertex*> vin = fillEdgeVertex(v1,v2,v3,v4,order,vset);
        //
        std::vector<MVertex*> vl13, vl24;
        vl13 = creatLineInternalVertices(v1, v3, order,vset);
        vl24 = creatLineInternalVertices(v2, v4, order,vset);
        
        int left=0, right = order-2;
        while (true)
        {
          if (left < right)
          {
            MVertex* v11 = vl13[left];
            MVertex* v22 = vl24[left];
            MVertex* v33 = vl13[right];
            MVertex* v44 = vl24[right];
            vin.push_back(v11);
            vin.push_back(v22);
            vin.push_back(v33);
            vin.push_back(v44);
            //printf("left = %d, right = %d, order-2*left-3 = %d \n",left,right,order-2*left-2);
            std::vector<MVertex*> vvvin = fillEdgeVertex(v11,v22,v33,v44,order-2*left-2,vset);
            vin.insert(vin.end(),vvvin.begin(),vvvin.end());
          }
          else if (left == right)
          {
            vin.push_back(vl13[left]);
          }
          
          left++;
          right--;
          
          if (left>right)
          {
            break;
          }
        }
        return vin;
      }      
    };
    
    //
    //eXY = new MQuadrangle(v1,v4,v3,v2);
    std::vector<MVertex*> vin = creatFaceInternalVertices(v1,v4,v3,v2,order, serendip,allNewVers);
    eXY = new MQuadrangleN(v1,v4,v3,v2,vin,order);
    
    //eYZ = new MQuadrangle(v1,v5,v8,v4);
    vin = creatFaceInternalVertices(v1,v5,v8,v4,order, serendip,allNewVers);
    eYZ = new MQuadrangleN(v1,v5,v8,v4,vin,order);
    
    //eZX = new MQuadrangle(v1,v2,v6,v5);
    vin = creatFaceInternalVertices(v1,v2,v6,v5,order, serendip,allNewVers);
    eZX = new MQuadrangleN(v1,v2,v6,v5,vin,order);
  }
}

// for 2D
void InterpolationOperations::create_MLineN_Base(int order, MVertex* v1, MVertex* v2, MVertex* v4, MElement*& eX, MElement*& eY, 
                          std::set<MVertex*>& allNewVers)
{
  if (order == 1)
  {
    eX = new MLine(v1,v2);
    eY = new MLine(v1,v4);
  }
  else
  {
    std::vector<MVertex*> vl12, vl14;
    vl12 = creatLineInternalVertices(v1, v2, order,allNewVers);
    vl14 = creatLineInternalVertices(v1, v4, order,allNewVers);
    eX = new MLineN(v1,v2,vl12);
    eY = new MLineN(v1,v4,vl14);
  }
};
                                                    

void InterpolationOperations::write_MSH2(elementGroup* gr, std::string filename)
{

	int numnodes = gr->vsize();
	int numel = gr->size();

	FILE* fp = fopen(filename.c_str(),"w");
 	fprintf(fp, "$MeshFormat\n2.2 0 8\n$EndMeshFormat\n");


	fprintf(fp, "$Nodes\n");
	fprintf(fp, "%d\n", numnodes);
	for (elementGroup::vertexContainer::const_iterator it = gr->vbegin(); it!= gr->vend(); it++){
		MVertex* v = it->second;
		fprintf(fp, "%ld %f %f %f\n",v->getNum(),v->x(),v->y(),v->z());
	}
	fprintf(fp, "$EndNodes\n");
 	fprintf(fp, "$Elements\n");
	fprintf(fp, "%d\n", numel);

  for (elementGroup::elementContainer::const_iterator it = gr->begin(); it!= gr->end(); it++){
		MElement* ele = it->second;
    fprintf(fp, "%ld %d 2 %ld %d",ele->getNum(),ele->getTypeForMSH(),ele->getNum(),ele->getTypeForMSH());
    std::vector<MVertex*> vers;
    ele->getVertices(vers);
    for (int i=0; i< vers.size(); i++)
    {
      fprintf(fp, " %ld",vers[i]->getNum());
    }
		fprintf(fp, "\n");
	};
	fprintf(fp, "$EndElements\n");
	fclose(fp);
};

elementGroupDomainDecomposition::elementGroupDomainDecomposition(const int phys, elementGroup* g_, std::vector<partDomain*>& allDomain):
				g(g_),physical(phys){
	// init map
	for (int i=0; i< allDomain.size(); i++){
		partDomain* dom = allDomain[i];
		if (dom->elementGroupSize()>0){
			elementGroup* gr = new elementGroup();
			decompositionMap[dom] = gr;
		}
	}
	for (elementGroup::elementContainer::const_iterator it = g->begin(); it != g->end(); it++){
		MElement* ie = it->second;
		// check if this element belong to what domain
		std::vector<MVertex*> vv;
		for (int iv=0; iv< ie->getNumPrimaryVertices(); iv++){
			vv.push_back(ie->getVertex(iv));
		};

		bool found = false;
		for (int i=0; i< allDomain.size(); i++){
			partDomain* dom = allDomain[i];
			for (elementGroup::elementContainer::const_iterator itgdom = dom->element_begin(); itgdom != dom->element_end(); itgdom++){
				MElement* eV = itgdom->second;
				std::vector<MVertex*> verEle;
				for (int iv=0; iv< eV->getNumPrimaryVertices(); iv++){
					verEle.push_back(eV->getVertex(iv));
				};
				//
				int numFoundVer= 0;
				for (int j=0; j< vv.size(); j++){
					for (int k=0; k< verEle.size(); k++){
						if (vv[j] == verEle[k]){
							numFoundVer++;
							break;
						}
					}
				}
				if (numFoundVer == vv.size()){
					decompositionMap[dom]->insert(ie);
					found = true;
					break;
				}
			}
			if (found) break;
		}
		if (!found) Msg::Error("domain is not found");
	}
};
elementGroupDomainDecomposition::~elementGroupDomainDecomposition(){
	for (std::map<partDomain*, elementGroup*>::iterator it = decompositionMap.begin(); it!= decompositionMap.end(); it++){
		if (it->second != NULL) delete it->second;
	}
	decompositionMap.clear();
}

pbcConstraintElementGroup::pbcConstraintElementGroup(nonLinearMechSolver* sl,
                            FunctionSpaceBase* lagspace, FunctionSpaceBase* multspace)
                               :_solver(sl), _v1(NULL),_v2(NULL),_v4(NULL),_v5(NULL),
                               _v3(NULL),_v6(NULL),_v7(NULL),_v8(NULL), _g(NULL),
                               _LagSpace(lagspace),_MultSpace(multspace),
				 _periodic12(0.),_periodic14(0.),_periodic15(0.),
                              f1234(NULL), f5678(NULL), f1562(NULL),f4873(NULL), f1584(NULL), f2673(NULL),
                              _gX(NULL),_gY(NULL),_gZ(NULL),
                              _gXY(NULL),_gYZ(NULL),_gZX(NULL),
                              _rveVolume(0.), _rveGeoInertia(0.), _rveGeometry(0.0){
  this->__init__();
};

pbcConstraintElementGroup::~pbcConstraintElementGroup(){
  for (std::set<constraintElement*>::iterator it = _allConstraint.begin(); it!= _allConstraint.end(); it++){
    constraintElement* cel = *it;
    if (cel) delete cel;
    cel = NULL;
  }
  for (std::set<MVertex*>::iterator it = _virtualVertices.begin(); it!= _virtualVertices.end(); it++){
    MVertex* v = *it;
    if (v) delete v;
    v = NULL;
  }

  if (_g) delete _g; _g = NULL;
  
  if (_gX) delete _gX;
  if (_gY) delete _gY;
  if (_gZ) delete _gZ;
  if (_gXY) delete _gXY;
  if (_gYZ) delete _gYZ;
  if (_gZX) delete _gZX;

	for (int i=0; i< _boundaryDGMap.size(); i++){
		delete _boundaryDGMap[i];
	}
	for (std::map<partDomain*,FunctionSpaceBase*>::iterator it = _allDGMultSpace.begin(); it!=_allDGMultSpace.end(); it++){
		if (it->second!=NULL) delete it->second;
	}
  
  constraintElement::allPositiveDof.clear();
};

template<class Iterator>
void pbcConstraintElementGroup::addVertexToGroupItMap(std::set<MVertex*>& l, Iterator itbegin, Iterator itend)
{
  for (Iterator it = itbegin; it != itend; it++)
  {
    l.insert(it->second);
  }
}

void pbcConstraintElementGroup::__init__(){
	std::vector<partDomain*>& allDomain = *(_solver->getDomainVector());
  // all higher-order vertices
  _constrainedCompPrimary.clear();
  _allHighOrderVertices.clear();
  const VertexBasedLagrangeFunctionSpace* space = dynamic_cast<const VertexBasedLagrangeFunctionSpace*>(allDomain[0]->getFunctionSpace());
  if (space != NULL)
  {
     _constrainedCompPrimary= space->getCompsWithPrimaryShapeFunction();
  };
  
  if (_constrainedCompPrimary.size() > 0)
  {
    for (int i=0; i< allDomain.size(); i++)
    {
      for (elementGroup::elementContainer::const_iterator it = allDomain[i]->element_begin(); it != allDomain[i]->element_end(); it++)
      {
        MElement* ele = it->second;
        int numVerPrimary = ele->getNumPrimaryVertices();
        int numVer = ele->getNumVertices();
        for (int j=numVerPrimary; j< numVer; j++)
        {
          _allHighOrderVertices.insert(ele->getVertex(j));
        }
      }
    }
  }

  /**get all element within all domain in CG case**/
  _g = new elementGroup();
  _fullDG = false;
  for (int idom = 0; idom< allDomain.size(); idom++){
    partDomain* dom = allDomain[idom];
    if (dom->getFormulation()){
      _fullDG = true;
			if (dom->elementGroupSize() > 0) Msg::Error("The pbcConstraintElementGroup is only implemented for interDomain");
      break;
    }
  }

  for (int idom = 0; idom< allDomain.size(); idom++){
    partDomain* dom = allDomain[idom];
    for (elementGroup::elementContainer::const_iterator it = dom->element_begin(); it!= dom->element_end(); it++){
      _g->insert(it->second);
    }
  }

  if (_g->size()==0)
    Msg::Error("Error in setting domain");
  
  const std::vector<int>& bphysical = _solver->getMicroBC()->getBoundaryPhysicals();
  const std::vector<elementGroup*>& bgroup = _solver->getMicroBC()->getBoundaryElementGroup();
  if (bgroup.size() == 0) Msg::Error("microscopic boundary condition has not any support");
  int dim = _solver->getMicroBC()->getDim();

	if (_fullDG){
		Msg::Info("start decomsing boundary element to domains");
		_boundaryDGMap.clear();
		
		for (int ig=0; ig< bgroup.size(); ig++){
			elementGroup* gr = bgroup[ig];
			elementGroupDomainDecomposition* grDomPart = new  elementGroupDomainDecomposition(bphysical[ig], gr,allDomain);
			_boundaryDGMap.push_back(grDomPart);
		}
		Msg::Info("done decomsing boundary element to domains");

		// create all lag space
		int maxTag = 0;
		for (int i=0; i< allDomain.size(); i++)
    {
      if (allDomain[i]->elementGroupSize()>0)
      {
        if (maxTag< allDomain[i]->getFunctionSpace()->getId())
        {
          maxTag = allDomain[i]->getFunctionSpace()->getId();
        }
      }
		}
		maxTag ++; // to create a diffrent id for multspace
		_allDGMultSpace.clear();
		for (int i=0; i< allDomain.size(); i++){
			partDomain* dom = allDomain[i];
			if (dom->elementGroupSize()>0){
				int newTag = maxTag+i;
				FunctionSpaceBase* sp = dom->getFunctionSpace()->clone(newTag);
				if (sp == NULL){
					Msg::Error("FunctionSpaceBase::clone() of the FunctionSpaceBase on the domain %d is not defined ",dom->getPhysical());
				}
				_allDGMultSpace[dom] = sp;
			}
		}
	}
  
  if (dim == 2){
    if (bgroup.size() != 4) Msg::Error("Boundary support must be 4 edges");
    l12.clear();
    addVertexToGroupItMap(l12,bgroup[0]->vbegin(),bgroup[0]->vend());
    l41.clear();
    addVertexToGroupItMap(l41,bgroup[1]->vbegin(),bgroup[1]->vend());
    l34.clear();
    addVertexToGroupItMap(l34,bgroup[2]->vbegin(),bgroup[2]->vend());
    l23.clear();
    addVertexToGroupItMap(l23,bgroup[3]->vbegin(),bgroup[3]->vend());

    _bVertices.clear();
    InterpolationOperations::groupIntersection(bgroup[0],bgroup[1],_bVertices);
    InterpolationOperations::groupIntersection(bgroup[1],bgroup[2],_bVertices);
    InterpolationOperations::groupIntersection(bgroup[2],bgroup[3],_bVertices);
    InterpolationOperations::groupIntersection(bgroup[3],bgroup[0],_bVertices);

    _cVertices.clear();
    _cVertices.insert(_bVertices.begin(),_bVertices.end());
    #ifdef _DEBUG
    for (std::set<MVertex*>::iterator it = _cVertices.begin(); it!= _cVertices.end(); it++){
      Msg::Info("corner vertex %d ",(*it)->getNum());
    };
    #endif //_DEBUG
  }
  else if (dim == 3){
    if (bgroup.size() != 6) Msg::Error("Boundary support must be 6 faces");
    // all faces
    f1234 = bgroup[0];
    f5678 = bgroup[3];
    f1562 = bgroup[1];
    f4873 = bgroup[4];
    f1584 = bgroup[2];
    f2673 = bgroup[5];
    
    // all edges
    l12.clear(); InterpolationOperations::groupIntersection(f1234,f1562,l12);
    l23.clear(); InterpolationOperations::groupIntersection(f1234,f2673,l23);
    l34.clear(); InterpolationOperations::groupIntersection(f1234,f4873,l34);
    l41.clear(); InterpolationOperations::groupIntersection(f1234,f1584,l41);
    l56.clear(); InterpolationOperations::groupIntersection(f5678,f1562,l56);
    l67.clear(); InterpolationOperations::groupIntersection(f5678,f2673,l67);
    l78.clear(); InterpolationOperations::groupIntersection(f5678,f4873,l78);
    l85.clear(); InterpolationOperations::groupIntersection(f5678,f1584,l85);
    l15.clear(); InterpolationOperations::groupIntersection(f1562,f1584,l15);
    l26.clear(); InterpolationOperations::groupIntersection(f1562,f2673,l26);
    l37.clear(); InterpolationOperations::groupIntersection(f2673,f4873,l37);
    l48.clear(); InterpolationOperations::groupIntersection(f4873,f1584,l48);
    
    
    _bVertices.clear();
    _bVertices.insert(l12.begin(),l12.end());
    _bVertices.insert(l23.begin(),l23.end());
    _bVertices.insert(l34.begin(),l34.end());
    _bVertices.insert(l41.begin(),l41.end());
    _bVertices.insert(l56.begin(),l56.end());
    _bVertices.insert(l67.begin(),l67.end());
    _bVertices.insert(l78.begin(),l78.end());
    _bVertices.insert(l85.begin(),l85.end());
    _bVertices.insert(l15.begin(),l15.end());
    _bVertices.insert(l26.begin(),l26.end());
    _bVertices.insert(l37.begin(),l37.end());
    _bVertices.insert(l48.begin(),l48.end());
    
    
    #ifdef _DEBUG
    for (std::set<MVertex*>::iterator it = l12.begin(); it!= l12.end(); it++){
      Msg::Info("vertex %d 12",(*it)->getNum());
    };    
    for (std::set<MVertex*>::iterator it = l23.begin(); it!= l23.end(); it++){
      Msg::Info("vertex %d 23",(*it)->getNum());
    };
    for (std::set<MVertex*>::iterator it = l34.begin(); it!= l34.end(); it++){
      Msg::Info("vertex %d 34",(*it)->getNum());
    };
    for (std::set<MVertex*>::iterator it = l41.begin(); it!= l41.end(); it++){
      Msg::Info("vertex %d 41",(*it)->getNum());
    };
    for (std::set<MVertex*>::iterator it = l56.begin(); it!= l56.end(); it++){
      Msg::Info("vertex %d 56",(*it)->getNum());
    };
    for (std::set<MVertex*>::iterator it = l67.begin(); it!= l67.end(); it++){
      Msg::Info("vertex %d 67",(*it)->getNum());
    };
    for (std::set<MVertex*>::iterator it = l78.begin(); it!= l78.end(); it++){
      Msg::Info("vertex %d 78",(*it)->getNum());
    };
    for (std::set<MVertex*>::iterator it = l85.begin(); it!= l85.end(); it++){
      Msg::Info("vertex %d 85",(*it)->getNum());
    };
    for (std::set<MVertex*>::iterator it = l15.begin(); it!= l15.end(); it++){
      Msg::Info("vertex %d 15",(*it)->getNum());
    };
    for (std::set<MVertex*>::iterator it = l26.begin(); it!= l26.end(); it++){
      Msg::Info("vertex %d 26",(*it)->getNum());
    };
    for (std::set<MVertex*>::iterator it = l37.begin(); it!= l37.end(); it++){
      Msg::Info("vertex %d 37",(*it)->getNum());
    };
    for (std::set<MVertex*>::iterator it = l48.begin(); it!= l48.end(); it++){
      Msg::Info("vertex %d 48",(*it)->getNum());
    };
    #endif //_DEBUG

    // corner vertices
    _cVertices.clear();
    InterpolationOperations::groupIntersection(l12,l23,_cVertices);
    InterpolationOperations::groupIntersection(l23,l34,_cVertices);
    InterpolationOperations::groupIntersection(l34,l41,_cVertices);
    InterpolationOperations::groupIntersection(l41,l12,_cVertices);
    InterpolationOperations::groupIntersection(l56,l67,_cVertices);
    InterpolationOperations::groupIntersection(l67,l78,_cVertices);
    InterpolationOperations::groupIntersection(l78,l85,_cVertices);
    InterpolationOperations::groupIntersection(l85,l56,_cVertices);
    
    #ifdef _DEBUG
    for (std::set<MVertex*>::iterator it = _cVertices.begin(); it!= _cVertices.end(); it++){
      Msg::Info("corner vertex %d ",(*it)->getNum());
    };
    #endif //_DEBUG
  }

  // add control points, v1-v2-v4 in 2D, v1, v2, v4, v5 in 3D
  std::set<MVertex*> vv;
  if (dim ==2){
    vv.clear();
    InterpolationOperations::groupIntersection(l12,l41,vv);
    if (vv.size() > 0){
      _v1 = *(vv.begin());
    }
    else{
      MElement* ex = (bgroup[0]->begin()->second);
      MElement* ey = (bgroup[1]->begin()->second);
      std::vector<MVertex*> vx, vy;
      ex->getVertices(vx);
      ey->getVertices(vy);
      SPoint3 pp = computeTwoLineIntersectionOnSurface(vx[0]->point(),vx[1]->point(),vy[0]->point(),vy[1]->point());
      _v1  = new MVertex(pp.x(),pp.y(),pp.z());
      Msg::Info("Create root vertex v1 %d: %e %e %e",_v1->getNum(),pp.x(),pp.y(),pp.z());
      _virtualVertices.insert(_v1);
      _cVertices.insert(_v1);
      _bVertices.insert(_v1);
    }
    
    // find v2
    vv.clear();
    InterpolationOperations::groupIntersection(l12,l23,vv);
    if (vv.size() > 0){
      _v2 = *(vv.begin());
    }
    else{
      MElement* ex = (bgroup[0]->begin()->second);
      MElement* ey = (bgroup[3]->begin()->second);
      std::vector<MVertex*> vx, vy;
      ex->getVertices(vx);
      ey->getVertices(vy);
      SPoint3 pp = computeTwoLineIntersectionOnSurface(vx[0]->point(),vx[1]->point(),vy[0]->point(),vy[1]->point());
      _v2  = new MVertex(pp.x(),pp.y(),pp.z());
      Msg::Info("Create root vertex v2 %d: %e %e %e",_v2->getNum(),pp.x(),pp.y(),pp.z());
      _virtualVertices.insert(_v2);
      _cVertices.insert(_v2);
      _bVertices.insert(_v2);
    }
    
    // find v3
    vv.clear();
    InterpolationOperations::groupIntersection(l23,l34,vv);
    if (vv.size()>0){
      _v3 = *(vv.begin());
    }
    else{
      MElement* ex = (bgroup[3]->begin()->second);
      MElement* ey = (bgroup[2]->begin()->second);
      std::vector<MVertex*> vx, vy;
      ex->getVertices(vx);
      ey->getVertices(vy);
      SPoint3 pp = computeTwoLineIntersectionOnSurface(vx[0]->point(),vx[1]->point(),vy[0]->point(),vy[1]->point());
      _v3  = new MVertex(pp.x(),pp.y(),pp.z());
      Msg::Info("Create root vertex v3 %d: %e %e %e",_v3->getNum(),pp.x(),pp.y(),pp.z());
      _virtualVertices.insert(_v3);
      _cVertices.insert(_v3);
      _bVertices.insert(_v3);
    }
    
    // find v4
    vv.clear();
    InterpolationOperations::groupIntersection(l34,l41,vv);
    if (vv.size() > 0){
      _v4 = *(vv.begin());
    }
    else{
      MElement* ex = (bgroup[2]->begin()->second);
      MElement* ey = (bgroup[1]->begin()->second);
      std::vector<MVertex*> vx, vy;
      ex->getVertices(vx);
      ey->getVertices(vy);
      SPoint3 pp = computeTwoLineIntersectionOnSurface(vx[0]->point(),vx[1]->point(),vy[0]->point(),vy[1]->point());
      _v4  = new MVertex(pp.x(),pp.y(),pp.z());
      Msg::Info("Create root vertex v4 %d: %e %e %e",_v4->getNum(),pp.x(),pp.y(),pp.z());
      _virtualVertices.insert(_v4);
      _cVertices.insert(_v4);
      _bVertices.insert(_v4);
    }
    
    _periodic12[0] = _v2->x() - _v1->x();
    _periodic12[1] = _v2->y() - _v1->y();
    _periodic12[2] = _v2->z() - _v1->z();

    _periodic14[0] = _v4->x() - _v1->x();
    _periodic14[1] = _v4->y() - _v1->y();
    _periodic14[2] = _v4->z() - _v1->z();
    
    _periodic15 = crossprod(_periodic12,_periodic14);
    _periodic15.normalize();
  }
  else if (dim == 3){
    vv.clear();
    InterpolationOperations::groupIntersection(l12,l41,vv);
    if (vv.size() == 0){
     InterpolationOperations::groupIntersection(l12,l15,vv);
    }
    if (vv.size() == 0){
      InterpolationOperations::groupIntersection(l41,l15,vv);
    }
    if (vv.size() > 0){
      _v1 = *(vv.begin());
    }
    else{
      MElement* e1 = (f1234->begin()->second);
      MElement* e2 = (f1584->begin()->second);
      MElement* e3 = (f1562->begin()->second);
      fullMatrix<double> m(3,3); m.setAll(0.);
      fullVector<double> b(3); b.setAll(0.);

      std::vector<MVertex*> vv1, vv2, vv3;
      e1->getVertices(vv1);
      e2->getVertices(vv2);
      e3->getVertices(vv3);

      computeSurface(vv1[0]->point(),vv1[1]->point(),vv1[2]->point(),m(0,0),m(0,1),m(0,2),b(0));
      computeSurface(vv2[0]->point(),vv2[1]->point(),vv2[2]->point(),m(1,0),m(1,1),m(1,2),b(1));
      computeSurface(vv3[0]->point(),vv3[1]->point(),vv3[2]->point(),m(2,0),m(2,1),m(2,2),b(2));

      fullMatrix<double> invm(3,3);
      m.invert(invm);
      fullVector<double> f(3);
      invm.mult(b,f);

      _v1  = new MVertex(f(0),f(1),f(2));
      Msg::Info("Create root vertex v1 %d: %e %e %e",_v1->getNum(),f(0),f(1),f(2));
      _virtualVertices.insert(_v1);
      _cVertices.insert(_v1);
      _bVertices.insert(_v1);
    }
    
    // v2
    vv.clear();
    InterpolationOperations::groupIntersection(l12,l23,vv);
    if (vv.size() == 0){
      InterpolationOperations::groupIntersection(l12,l26,vv);
    }
    if (vv.size() == 0){
      InterpolationOperations::groupIntersection(l23,l26,vv);
    }
    if (vv.size() > 0) {
      _v2 = *(vv.begin());
    }
    else{
      MElement* e1 = (f1234->begin()->second);
      MElement* e2 = (f2673->begin()->second);
      MElement* e3 = (f1562->begin()->second);
      fullMatrix<double> m(3,3); m.setAll(0.);
      fullVector<double> b(3); b.setAll(0.);

      std::vector<MVertex*> vv1, vv2, vv3;
      e1->getVertices(vv1);
      e2->getVertices(vv2);
      e3->getVertices(vv3);

      computeSurface(vv1[0]->point(),vv1[1]->point(),vv1[2]->point(),m(0,0),m(0,1),m(0,2),b(0));
      computeSurface(vv2[0]->point(),vv2[1]->point(),vv2[2]->point(),m(1,0),m(1,1),m(1,2),b(1));
      computeSurface(vv3[0]->point(),vv3[1]->point(),vv3[2]->point(),m(2,0),m(2,1),m(2,2),b(2));

      fullMatrix<double> invm(3,3);
      m.invert(invm);
      fullVector<double> f(3);
      invm.mult(b,f);

      _v2  = new MVertex(f(0),f(1),f(2));
      Msg::Info("Create root vertex v2 %d: %e %e %e",_v2->getNum(),f(0),f(1),f(2));
      _virtualVertices.insert(_v2);
      _cVertices.insert(_v2);
      _bVertices.insert(_v2);
    }
    
    // find v4
    vv.clear();
    InterpolationOperations::groupIntersection(l34,l41,vv);
    if (vv.size() == 0){  
      InterpolationOperations::groupIntersection(l34,l48,vv);
    }
    if (vv.size() == 0){
      InterpolationOperations::groupIntersection(l41,l48,vv);
    }
    if (vv.size() > 0) {
      _v4 = *(vv.begin());
    }
    else{
      MElement* e1 = (f1234->begin()->second);
      MElement* e2 = (f1584->begin()->second);
      MElement* e3 = (f4873->begin()->second);
      fullMatrix<double> m(3,3); m.setAll(0.);
      fullVector<double> b(3); b.setAll(0.);

      std::vector<MVertex*> vv1, vv2, vv3;
      e1->getVertices(vv1);
      e2->getVertices(vv2);
      e3->getVertices(vv3);

      computeSurface(vv1[0]->point(),vv1[1]->point(),vv1[2]->point(),m(0,0),m(0,1),m(0,2),b(0));
      computeSurface(vv2[0]->point(),vv2[1]->point(),vv2[2]->point(),m(1,0),m(1,1),m(1,2),b(1));
      computeSurface(vv3[0]->point(),vv3[1]->point(),vv3[2]->point(),m(2,0),m(2,1),m(2,2),b(2));

      fullMatrix<double> invm(3,3);
      m.invert(invm);
      fullVector<double> f(3);
      invm.mult(b,f);

      _v4  = new MVertex(f(0),f(1),f(2));
      Msg::Info("Create root vertex v4 %d: %e %e %e",_v4->getNum(),f(0),f(1),f(2));
      _virtualVertices.insert(_v4);
      _cVertices.insert(_v4);
      _bVertices.insert(_v4);
    }
    
    // find v5
    vv.clear();
    InterpolationOperations::groupIntersection(l85,l15,vv);
    if (vv.size() == 0){
      InterpolationOperations::groupIntersection(l85,l56,vv);
    }
    if (vv.size() == 0){
      InterpolationOperations::groupIntersection(l56,l15,vv);
    }
    if (vv.size() >0) {
      _v5 = *(vv.begin());
    }
    else{
      MElement* e1 = (f1584->begin()->second);
      MElement* e2 = (f5678->begin()->second);
      MElement* e3 = (f1562->begin()->second);
      fullMatrix<double> m(3,3); m.setAll(0.);
      fullVector<double> b(3); b.setAll(0.);

      std::vector<MVertex*> vv1, vv2, vv3;
      e1->getVertices(vv1);
      e2->getVertices(vv2);
      e3->getVertices(vv3);

      computeSurface(vv1[0]->point(),vv1[1]->point(),vv1[2]->point(),m(0,0),m(0,1),m(0,2),b(0));
      computeSurface(vv2[0]->point(),vv2[1]->point(),vv2[2]->point(),m(1,0),m(1,1),m(1,2),b(1));
      computeSurface(vv3[0]->point(),vv3[1]->point(),vv3[2]->point(),m(2,0),m(2,1),m(2,2),b(2));

      fullMatrix<double> invm(3,3);
      m.invert(invm);
      fullVector<double> f(3);
      invm.mult(b,f);

      _v5  = new MVertex(f(0),f(1),f(2));
      Msg::Info("Create root vertex v5 %d: %e %e %e",_v5->getNum(),f(0),f(1),f(2));
      _virtualVertices.insert(_v5);
      _cVertices.insert(_v5);
      _bVertices.insert(_v5);
    }
    
    
    // find v3
    vv.clear();
    InterpolationOperations::groupIntersection(l23,l34,vv);
    if (vv.size()==0){
      InterpolationOperations::groupIntersection(l34,l37,vv);
    }
    if (vv.size() ==0){
      InterpolationOperations::groupIntersection(l23,l37,vv);
    }
    if (vv.size() > 0){
      _v3 = *(vv.begin());
    }
    else{
      double x = _v4->x() + _v2->x() - _v1->x();
      double y = _v4->y() + _v2->y() - _v1->y();
      double z = _v4->z() + _v2->z() - _v1->z();
     _v3  = new MVertex(x,y,z);
      Msg::Info("Create root vertex v3 %d: %e %e %e",_v3->getNum(),x,y,z);
      _virtualVertices.insert(_v3);
      _cVertices.insert(_v3);
      _bVertices.insert(_v3);
    }
    
    // find _v6
    vv.clear();
    InterpolationOperations::groupIntersection(l26,l56,vv);
    if (vv.size() == 0)
      InterpolationOperations::groupIntersection(l56,l67,vv);

    if (vv.size() == 0)
      InterpolationOperations::groupIntersection(l26,l67,vv);

    if (vv.size()==0){
      double x = _v2->x() + _v5->x() - _v1->x();
      double y = _v2->y() + _v5->y() - _v1->y();
      double z = _v2->z() + _v5->z() - _v1->z();
     _v6  = new MVertex(x,y,z);
      Msg::Info("Create root vertex v6 %d: %e %e %e",_v6->getNum(),x,y,z);
      _virtualVertices.insert(_v6);
      _cVertices.insert(_v6);
      _bVertices.insert(_v6);
    }
    else {
      _v6 = *(vv.begin());
    }

     // find _v8
    vv.clear();
    InterpolationOperations::groupIntersection(l85,l48,vv);
    if (vv.size() == 0)
      InterpolationOperations::groupIntersection(l48,l78,vv);

    if (vv.size() == 0)
      InterpolationOperations::groupIntersection(l85,l78,vv);

    if (vv.size()==0){
      double x = _v4->x() + _v5->x() - _v1->x();
      double y = _v4->y() + _v5->y() - _v1->y();
      double z = _v4->z() + _v5->z() - _v1->z();
     _v8  = new MVertex(x,y,z);
      Msg::Info("Create root vertex v8 %d: %e %e %e",_v8->getNum(),x,y,z);
      _virtualVertices.insert(_v8);
      _cVertices.insert(_v8);
      _bVertices.insert(_v8);
    }
    else {
      _v8 = *(vv.begin());
    }

     // find _v7
    vv.clear();
    InterpolationOperations::groupIntersection(l78,l37,vv);
    if (vv.size() == 0)
      InterpolationOperations::groupIntersection(l37,l67,vv);

    if (vv.size() == 0)
      InterpolationOperations::groupIntersection(l78,l67,vv);

    if (vv.size()==0){
      double x = _v3->x() + _v5->x() - _v1->x();
      double y = _v3->y() + _v5->y() - _v1->y();
      double z = _v3->z() + _v5->z() - _v1->z();
     _v7  = new MVertex(x,y,z);
      Msg::Info("Create root vertex v7 %d: %e %e %e",_v7->getNum(),x,y,z);
      _virtualVertices.insert(_v7);
      _cVertices.insert(_v7);
      _bVertices.insert(_v7);
    }
    else {
      _v7 = *(vv.begin());
    }
    
    
    _periodic12[0] = _v2->x() - _v1->x();
    _periodic12[1] = _v2->y() - _v1->y();
    _periodic12[2] = _v2->z() - _v1->z();

    _periodic14[0] = _v4->x() - _v1->x();
    _periodic14[1] = _v4->y() - _v1->y();
    _periodic14[2] = _v4->z() - _v1->z();

    _periodic15[0] = _v5->x() - _v1->x();
    _periodic15[1] = _v5->y() - _v1->y();
    _periodic15[2] = _v5->z() - _v1->z();
    
  }
  
  if (dim == 2)
  {
    printf("v1:  [%f %f %f]\n",_v1->x(),_v1->y(), _v1->z());
    printf("v2:  [%f %f %f]\n",_v2->x(),_v2->y(), _v2->z());
    printf("v3:  [%f %f %f]\n",_v3->x(),_v3->y(), _v3->z());
    printf("v4:  [%f %f %f]\n",_v4->x(),_v4->y(), _v4->z());
  }
  else if (dim == 3)
  {
    printf("v1:  [%f %f %f]\n",_v1->x(),_v1->y(), _v1->z());
    printf("v2:  [%f %f %f]\n",_v2->x(),_v2->y(), _v2->z());
    printf("v3:  [%f %f %f]\n",_v3->x(),_v3->y(), _v3->z());
    printf("v4:  [%f %f %f]\n",_v4->x(),_v4->y(), _v4->z());
    printf("v5:  [%f %f %f]\n",_v5->x(),_v5->y(), _v5->z());
    printf("v6:  [%f %f %f]\n",_v6->x(),_v6->y(), _v6->z());
    printf("v7:  [%f %f %f]\n",_v7->x(),_v7->y(), _v7->z());
    printf("v8:  [%f %f %f]\n",_v8->x(),_v8->y(), _v8->z());
  }
  
  _periodic12.print("periodic 1 --> 2");
  _periodic14.print("periodic 1 --> 4");
  _periodic15.print("periodic 1 --> 5");
  
  SVector3 crossV = crossprod(_periodic12,_periodic14);
  
  _rveVolume = fabs(dot(crossV,_periodic15));
  Msg::Info("RVE volume computed  = %e \n",_rveVolume);
  
  _rveGeoInertia(0,0) = _rveVolume*fabs(dot(_periodic12,_periodic12))/12.0;
  _rveGeoInertia(1,1) = _rveVolume*fabs(dot(_periodic14,_periodic14))/12.0;
  _rveGeoInertia(2,2) = _rveVolume*fabs(dot(_periodic15,_periodic15))/12.0;
 
  _rveGeometry(0) = _periodic12.norm();
  _rveGeometry(1) = _periodic14.norm();
  _rveGeometry(2) = _periodic15.norm();  
};

void pbcConstraintElementGroup::getCornerVerticesForBoundaryPhysical(const int phy, std::vector<MVertex*>& v){
  const std::vector<int>& bphysical = _solver->getMicroBC()->getBoundaryPhysicals();

  if (_solver->getMicroBC()->getDim() == 2){
    // line periodic l12 (g[0]) - l34 (g[2])
   // line periodic l41 (g[1]) - l23 (g[3])
    v.resize(2);
    if (phy == bphysical[0]){
      v[0] = _v1;
      v[1] = _v2;
    }
    else if (phy == bphysical[1]){
      v[0] = _v1;
      v[1] = _v4;
    }
    else if (phy == bphysical[2]){
      v[0] = _v4;
      v[1] = _v3;
    }
    else if (phy == bphysical[3]){
      v[0] = _v2;
      v[1] = _v3;
    }
    else{
      Msg::Error("PBC physical is wrong defined");
    }
  }
  else if (_solver->getMicroBC()->getDim() == 3){
    //periodic surface 1234 (g[0]) - 5678 (g[3])
   // periodic surface 1562 (g[1]) - 4873 (g[4])
   // periodic surface 1584 (g[2]) - 2673 (g[5])
    v.resize(4);
    if (phy == bphysical[0]){
      v[0] = _v1;
      v[1] = _v2;
      v[2] = _v3;
      v[3] = _v4;
    }
    else if (phy == bphysical[1]){
      v[0] = _v1;
      v[1] = _v2;
      v[2] = _v6;
      v[3] = _v5;
    }
    else if (phy == bphysical[2]){
      v[0] = _v1;
      v[1] = _v4;
      v[2] = _v8;
      v[3] = _v5;
    }
    else if (phy == bphysical[3]){
      v[0] = _v5;
      v[1] = _v6;
      v[2] = _v7;
      v[3] = _v8;
    }
    else if (phy == bphysical[4]){
      v[0] = _v4;
      v[1] = _v3;
      v[2] = _v7;
      v[3] = _v8;
    }
    else if (phy == bphysical[5]){
      v[0] = _v2;
      v[1] = _v3;
      v[2] = _v7;
      v[3] = _v6;
    }
    else{
      Msg::Error("PBC physical is wrong defined");
    }
  }
};


void pbcConstraintElementGroup::getLinesInFace(const int phy, std::vector<std::set<MVertex*>* >& allLine){
  const std::vector<int>& bphysical = _solver->getMicroBC()->getBoundaryPhysicals();
  //periodic surface 1234 (g[0]) - 5678 (g[3])
  // periodic surface 1562 (g[1]) - 4873 (g[4])
  // periodic surface 1584 (g[2]) - 2673 (g[5])
  allLine.resize(4);
  if (phy == bphysical[0]){
    allLine[0] = &l12;
    allLine[3] = &l23;
    allLine[2] = &l34;
    allLine[1] = &l41;
  }
  else if (phy == bphysical[1]){
    allLine[0] = &l12;
    allLine[3] = &l26;
    allLine[2] = &l56;
    allLine[1] = &l15;
  }
  else if (phy == bphysical[2]){
    allLine[0] = &l41;
    allLine[3] = &l48;
    allLine[2] = &l85;
    allLine[1] = &l15;
  }
  else if (phy == bphysical[3]){
    allLine[0] = &l56;
    allLine[3] = &l67;
    allLine[2] = &l78;
    allLine[1] = &l85;
  }
  else if (phy == bphysical[4]){
    allLine[0] = &l34;
    allLine[3] = &l37;
    allLine[2] = &l78;
    allLine[1] = &l48;
  }
  else if (phy == bphysical[5]){
    allLine[0] = &l23;
    allLine[3] = &l37;
    allLine[2] = &l67;
    allLine[1] = &l26;
  }
  else{
    Msg::Error("PBC physical is wrong defined");
  }
};


template<class Iterator>
void pbcConstraintElementGroup::periodicConditionForPeriodicMesh(Iterator posivtiveitbegin, Iterator positiveitend,
                                               Iterator negativeitbegin, Iterator negativeitend,
                                               std::set<MVertex*>& others, MVertex* vrootP, MVertex* vrootN){
  for (Iterator itp = posivtiveitbegin; itp!= positiveitend; itp++){
    MVertex* v = *itp;
    if ( others.find(v) == others.end()){
      MVertex* vn = InterpolationOperations::findNearestVertex(negativeitbegin,negativeitend,v);
        constraintElement* cel = new periodicMeshConstraint(_solver->getMicroBC(),_LagSpace,_MultSpace,-1,v,vn,vrootP,vrootN);
        _allConstraint.insert(cel);
    };
  }
};

template<class Iterator>
void pbcConstraintElementGroup::periodicConditionForPeriodicMeshItMap(Iterator posivtiveitbegin, Iterator positiveitend,
                                               Iterator negativeitbegin, Iterator negativeitend,
                                               std::set<MVertex*>& others, MVertex* vrootP, MVertex* vrootN){
  for (Iterator itp = posivtiveitbegin; itp!= positiveitend; itp++){
    MVertex* v = itp->second;
    if ( others.find(v) == others.end()){
      MVertex* vn = InterpolationOperations::findNearestVertexItMap(negativeitbegin,negativeitend,v);
        constraintElement* cel = new periodicMeshConstraint(_solver->getMicroBC(),_LagSpace,_MultSpace,-1,v,vn,vrootP,vrootN);
        _allConstraint.insert(cel);
    };
  }
};

template<class Iterator>
void pbcConstraintElementGroup::periodicConditionForPeriodicMesh(const int comp, const double fact,
																							 Iterator posivtiveitbegin, Iterator positiveitend,
                                               Iterator negativeitbegin, Iterator negativeitend,
                                               std::set<MVertex*>& others, MVertex* vrootP, MVertex* vrootN){
  for (Iterator itp = posivtiveitbegin; itp!= positiveitend; itp++){
    MVertex* v = *itp;
    if ( others.find(v) == others.end()){
      MVertex* vn = InterpolationOperations::findNearestVertex(negativeitbegin,negativeitend,v);
        constraintElement* cel = new periodicMeshConstraint(_solver->getMicroBC(),_LagSpace,_MultSpace,comp,v,vn,vrootP,vrootN,fact);
        _allConstraint.insert(cel);
    };
  }
};

template<class Iterator>
void pbcConstraintElementGroup::periodicConditionForPeriodicMeshItMap(const int comp, const double fact,
																							 Iterator posivtiveitbegin, Iterator positiveitend,
                                               Iterator negativeitbegin, Iterator negativeitend,
                                               std::set<MVertex*>& others, MVertex* vrootP, MVertex* vrootN){
  for (Iterator itp = posivtiveitbegin; itp!= positiveitend; itp++){
    MVertex* v = itp->second;
    if ( others.find(v) == others.end()){
      MVertex* vn = InterpolationOperations::findNearestVertexItMap(negativeitbegin,negativeitend,v);
        constraintElement* cel = new periodicMeshConstraint(_solver->getMicroBC(),_LagSpace,_MultSpace,comp,v,vn,vrootP,vrootN,fact);
        _allConstraint.insert(cel);
    };
  }
};


// for cubic spline
template<class Iterator>
void pbcConstraintElementGroup::cubicSplineFormCondition(Iterator itbegin, Iterator itend,
                                                     std::vector<MVertex*>& vlist, int fl){
  int size = vlist.size();
  for (Iterator it = itbegin; it!= itend;it++){
    MVertex* v = *it;
    bool flag = true;
    // check if this vertex lies on the interpolation groups
    for (int i=0; i<size; i++){
      if (v == vlist[i]) {
        flag = false;
        break;
      }
    };
    // do nothing if this vertex lies on the interpolation groups
    if (flag){
      MVertex* vleft(NULL), *vright(NULL);
      // find segment contains vertex
      for (int j=0; j<size-1; j++){
        if (inside(v->point(),vlist[j]->point(),vlist[j+1]->point())){
          vleft = vlist[j];
          vright = vlist[j+1];
          break;
        };
      };
			constraintElement* cele = new cubicSplineConstraintElement(_solver->getMicroBC(),_LagSpace,_MultSpace,-1,v,vleft,vright,fl);
			_allConstraint.insert(cele);

    };
  };
};
// for cubic spline
template<class Iterator>
void pbcConstraintElementGroup::cubicSplineFormConditionItMap(Iterator itbegin, Iterator itend,
                                                     std::vector<MVertex*>& vlist, int fl){
  int size = vlist.size();
  for (Iterator it = itbegin; it!= itend;it++){
    MVertex* v = it->second;
    bool flag = true;
    // check if this vertex lies on the interpolation groups
    for (int i=0; i<size; i++){
      if (v == vlist[i]) {
        flag = false;
        break;
      }
    };
    // do nothing if this vertex lies on the interpolation groups
    if (flag){
      MVertex* vleft(NULL), *vright(NULL);
      // find segment contains vertex
      for (int j=0; j<size-1; j++){
        if (inside(v->point(),vlist[j]->point(),vlist[j+1]->point())){
          vleft = vlist[j];
          vright = vlist[j+1];
          break;
        };
      };
			constraintElement* cele = new cubicSplineConstraintElement(_solver->getMicroBC(),_LagSpace,_MultSpace,-1,v,vleft,vright,fl);
			_allConstraint.insert(cele);

    };
  };
};
template<class Iterator>
void pbcConstraintElementGroup::cubicSplinePeriodicCondition(Iterator itbegin, Iterator itend,
                                                         std::vector<MVertex*>& vlist, int fl,
                                                         std::set<MVertex*>& others){
  int size = vlist.size();

  MVertex* v1 = vlist[0];
  MVertex* v2 = vlist[size-1];
  for (Iterator it = itbegin; it != itend; it++){
    MVertex* v= *it;
    // because of formulation, particular constraint will be applied on corner vertices
    if (others.find(v) == others.end()){
      // find segment contains vertex
      MVertex* vleft(NULL), *vright(NULL);
      SPoint3 pt=project(v->point(),v1->point(),v2->point());
      for (int j=0; j<size-1; j++){
        if (inside(pt,vlist[j]->point(),vlist[j+1]->point())){
          vleft = vlist[j];
          vright = vlist[j+1];
          break;
        };
      };
      // if found segment
      if ((vleft != NULL) and (vright!= NULL)){
				constraintElement* cele = new cubicSplineConstraintElement(_solver->getMicroBC(),_LagSpace,_MultSpace,-1,v,vleft,vright,fl);
				_allConstraint.insert(cele);

      }
      else{
        Msg::Error("Segment not found");
      }
    };
  };
};
template<class Iterator>
void pbcConstraintElementGroup::cubicSplinePeriodicConditionItMap(Iterator itbegin, Iterator itend,
                                                         std::vector<MVertex*>& vlist, int fl,
                                                         std::set<MVertex*>& others){
  int size = vlist.size();

  MVertex* v1 = vlist[0];
  MVertex* v2 = vlist[size-1];
  for (Iterator it = itbegin; it != itend; it++){
    MVertex* v= it->second;
    // because of formulation, particular constraint will be applied on corner vertices
    if (others.find(v) == others.end()){
      // find segment contains vertex
      MVertex* vleft(NULL), *vright(NULL);
      SPoint3 pt=project(v->point(),v1->point(),v2->point());
      for (int j=0; j<size-1; j++){
        if (inside(pt,vlist[j]->point(),vlist[j+1]->point())){
          vleft = vlist[j];
          vright = vlist[j+1];
          break;
        };
      };
      // if found segment
      if ((vleft != NULL) and (vright!= NULL)){
				constraintElement* cele = new cubicSplineConstraintElement(_solver->getMicroBC(),_LagSpace,_MultSpace,-1,v,vleft,vright,fl);
				_allConstraint.insert(cele);

      }
      else{
        Msg::Error("Segment not found");
      }
    };
  };
};

template<class Iterator>
void pbcConstraintElementGroup::cubicSplineFormConditionCoonsPatch(Iterator itbegin, Iterator itend, MVertex* vref,
                                                         std::vector<MVertex*>& xlist, std::vector<MVertex*>& ylist,
                                                         int flagx, int flagy, std::set<MVertex*>& others){

  int sizex = xlist.size();
  int sizey = ylist.size();

  MVertex* v1x = xlist[0];
  MVertex* v2x = xlist[sizex-1];
  MVertex* v1y = ylist[0];
  MVertex* v2y = ylist[sizey-1];

  for (Iterator it = itbegin; it!= itend;it++){
    MVertex* v = *it;
    // particular constraint for edge vertices, we check for inner vertices
    if (others.find(v)==others.end()){
      MVertex* vleftx(NULL), *vrightx(NULL),* vlefty(NULL), *vrighty(NULL);
      // find segment corresponds to vertex
      SPoint3 xpoint = project(v->point(),v1x->point(),v2x->point());
      SPoint3 ypoint = project(v->point(),v1y->point(),v2y->point());
      for (int j=0; j<sizex-1; j++){
        if (inside(xpoint,xlist[j]->point(),xlist[j+1]->point())){
          vleftx = xlist[j];
          vrightx = xlist[j+1];
          break;
        };
      };
      for (int j=0; j<sizey-1; j++){
        if (inside(ypoint,ylist[j]->point(),ylist[j+1]->point())){
          vlefty = ylist[j];
          vrighty = ylist[j+1];
          break;
        };
      };
      // segment found
      if ((vleftx!=NULL) and (vlefty!=NULL) and (vrightx!=NULL) and (vrighty!=NULL)){
          constraintElement* cele = new CoonsPatchCubicSplineConstraintElement(_solver->getMicroBC(),_LagSpace,_MultSpace,
																								-1,v,vref,vleftx,vrightx,vlefty,vrighty,flagx,flagy);
          _allConstraint.insert(cele);
      }
      else
        Msg::Error("patch element not found");
    };
  };
};

template<class Iterator>
void pbcConstraintElementGroup::cubicSplineFormConditionCoonsPatchItMap(Iterator itbegin, Iterator itend, MVertex* vref,
                                                         std::vector<MVertex*>& xlist, std::vector<MVertex*>& ylist,
                                                         int flagx, int flagy, std::set<MVertex*>& others){

  int sizex = xlist.size();
  int sizey = ylist.size();

  MVertex* v1x = xlist[0];
  MVertex* v2x = xlist[sizex-1];
  MVertex* v1y = ylist[0];
  MVertex* v2y = ylist[sizey-1];

  for (Iterator it = itbegin; it!= itend;it++){
    MVertex* v = it->second;
    // particular constraint for edge vertices, we check for inner vertices
    if (others.find(v)==others.end()){
      MVertex* vleftx(NULL), *vrightx(NULL),* vlefty(NULL), *vrighty(NULL);
      // find segment corresponds to vertex
      SPoint3 xpoint = project(v->point(),v1x->point(),v2x->point());
      SPoint3 ypoint = project(v->point(),v1y->point(),v2y->point());
      for (int j=0; j<sizex-1; j++){
        if (inside(xpoint,xlist[j]->point(),xlist[j+1]->point())){
          vleftx = xlist[j];
          vrightx = xlist[j+1];
          break;
        };
      };
      for (int j=0; j<sizey-1; j++){
        if (inside(ypoint,ylist[j]->point(),ylist[j+1]->point())){
          vlefty = ylist[j];
          vrighty = ylist[j+1];
          break;
        };
      };
      // segment found
      if ((vleftx!=NULL) and (vlefty!=NULL) and (vrightx!=NULL) and (vrighty!=NULL)){
          constraintElement* cele = new CoonsPatchCubicSplineConstraintElement(_solver->getMicroBC(),_LagSpace,_MultSpace,
																								-1,v,vref,vleftx,vrightx,vlefty,vrighty,flagx,flagy);
          _allConstraint.insert(cele);
      }
      else
        Msg::Error("patch element not found");
    };
  };
};

template<class Iterator>
void pbcConstraintElementGroup::cubicSplinePeriodicConditionCoonsPatch(Iterator itbegin, Iterator itend, MVertex* vref,
                                                             std::vector<MVertex*>& xlist,std::vector<MVertex*>& ylist,
                                                             int flagx, int flagy,std::set<MVertex*>& others ){

  int sizex = xlist.size();
  int sizey = ylist.size();

  MVertex* v1x = xlist[0];
  MVertex* v2x = xlist[sizex-1];
  MVertex* v1y = ylist[0];
  MVertex* v2y = ylist[sizey-1];

  MVertex * vtemp;
  if (v1y != v1x and v1y!= v2x)
    vtemp = v1y;
  else
    vtemp = v2y;

  for (Iterator it = itbegin; it!= itend;it++){
    MVertex* v = *it;
    if (others.find(v)==others.end()){
      SPoint3 vnn = project(v->point(),v1x->point(),v2x->point(),vtemp->point());
      MVertex* vleftx, *vrightx,* vlefty, *vrighty;
      SPoint3 xpoint = project(vnn,v1x->point(),v2x->point());
      SPoint3 ypoint = project(vnn,v1y->point(),v2y->point());
      for (int j=0; j<sizex-1; j++){
        if (inside(xpoint,xlist[j]->point(),xlist[j+1]->point())){
          vleftx = xlist[j];
          vrightx = xlist[j+1];
          break;
        };
      };
      for (int j=0; j<sizey-1; j++){
        if (inside(ypoint,ylist[j]->point(),ylist[j+1]->point())){
          vlefty = ylist[j];
          vrighty = ylist[j+1];
          break;
        };
      };
      if ((vleftx!=NULL) and (vlefty!=NULL) and (vrightx!=NULL) and (vrighty!=NULL)){
				constraintElement* cele = new CoonsPatchCubicSplineConstraintElement(_solver->getMicroBC() ,_LagSpace,_MultSpace,
																				-1,v,vref,vleftx,vrightx,vlefty,vrighty,flagx,flagy);
				_allConstraint.insert(cele);

      }
      else
        Msg::Error("patch element not found");
    };
  };
};

template<class Iterator>
void pbcConstraintElementGroup::cubicSplinePeriodicConditionCoonsPatchItMap(Iterator itbegin, Iterator itend, MVertex* vref,
                                                             std::vector<MVertex*>& xlist,std::vector<MVertex*>& ylist,
                                                             int flagx, int flagy,std::set<MVertex*>& others ){

  int sizex = xlist.size();
  int sizey = ylist.size();

  MVertex* v1x = xlist[0];
  MVertex* v2x = xlist[sizex-1];
  MVertex* v1y = ylist[0];
  MVertex* v2y = ylist[sizey-1];

  MVertex * vtemp;
  if (v1y != v1x and v1y!= v2x)
    vtemp = v1y;
  else
    vtemp = v2y;

  for (Iterator it = itbegin; it!= itend;it++){
    MVertex* v = it->second;
    if (others.find(v)==others.end()){
      SPoint3 vnn = project(v->point(),v1x->point(),v2x->point(),vtemp->point());
      MVertex* vleftx, *vrightx,* vlefty, *vrighty;
      SPoint3 xpoint = project(vnn,v1x->point(),v2x->point());
      SPoint3 ypoint = project(vnn,v1y->point(),v2y->point());
      for (int j=0; j<sizex-1; j++){
        if (inside(xpoint,xlist[j]->point(),xlist[j+1]->point())){
          vleftx = xlist[j];
          vrightx = xlist[j+1];
          break;
        };
      };
      for (int j=0; j<sizey-1; j++){
        if (inside(ypoint,ylist[j]->point(),ylist[j+1]->point())){
          vlefty = ylist[j];
          vrighty = ylist[j+1];
          break;
        };
      };
      if ((vleftx!=NULL) and (vlefty!=NULL) and (vrightx!=NULL) and (vrighty!=NULL)){
				constraintElement* cele = new CoonsPatchCubicSplineConstraintElement(_solver->getMicroBC() ,_LagSpace,_MultSpace,
																				-1,v,vref,vleftx,vrightx,vlefty,vrighty,flagx,flagy);
				_allConstraint.insert(cele);

      }
      else
        Msg::Error("patch element not found");
    };
  };
};


// for lagrange
template<class Iterator>
void pbcConstraintElementGroup::lagrangeFormCondition(Iterator itbegin, Iterator itend,
                                                  std::vector<MVertex*>& vlist, MVertex* vrootP, MVertex* vrootN,
                                                  const int comp){
  int size = vlist.size();
  for (Iterator it = itbegin; it!= itend;it++){
    MVertex* v = *it;
    bool flag = true;
    for (int i=0; i<size; i++){
      if (v->getNum() == vlist[i]->getNum()){
        flag = false;
        break;
      }
    };
    if (flag){
			constraintElement* cele = new lagrangeConstraintElement(_solver->getMicroBC(),_LagSpace,_MultSpace,
																			comp,v,vlist,vrootP,vrootN);
			_allConstraint.insert(cele);
    };
  };
};
template<class Iterator>
void pbcConstraintElementGroup::lagrangeFormConditionItMap(Iterator itbegin, Iterator itend,
                                                  std::vector<MVertex*>& vlist, MVertex* vrootP, MVertex* vrootN,
                                                  const int comp){
  int size = vlist.size();
  for (Iterator it = itbegin; it!= itend;it++){
    MVertex* v = it->second;
    bool flag = true;
    for (int i=0; i<size; i++){
      if (v->getNum() == vlist[i]->getNum()){
        flag = false;
        break;
      }
    };
    if (flag){
			constraintElement* cele = new lagrangeConstraintElement(_solver->getMicroBC(),_LagSpace,_MultSpace,
																			comp,v,vlist,vrootP,vrootN);
			_allConstraint.insert(cele);
    };
  };
};
template<class Iterator>
void pbcConstraintElementGroup::lagrangePeriodicCondition(Iterator itbegin, Iterator itend,
                                                      std::vector<MVertex*>& vlist, std::set<MVertex*>& others,
                                                      MVertex* vrootP, MVertex* vrootN,
                                                      const int comp){
	for (Iterator it = itbegin; it != itend; it++){
    MVertex* v= *it;
    if (others.find(v) == others.end()){
			constraintElement* cele = new lagrangeConstraintElement(_solver->getMicroBC(),_LagSpace,_MultSpace,
																					comp,v,vlist,vrootP,vrootN);
			_allConstraint.insert(cele);
    };
  };
};

template<class Iterator>
void pbcConstraintElementGroup::lagrangePeriodicConditionItMap(Iterator itbegin, Iterator itend,
                                                      std::vector<MVertex*>& vlist, std::set<MVertex*>& others,
                                                      MVertex* vrootP, MVertex* vrootN,
                                                      const int comp){
	for (Iterator it = itbegin; it != itend; it++){
    MVertex* v= it->second;
    if (others.find(v) == others.end()){
			constraintElement* cele = new lagrangeConstraintElement(_solver->getMicroBC(),_LagSpace,_MultSpace,
																					comp,v,vlist,vrootP,vrootN);
			_allConstraint.insert(cele);
    };
  };
};

template<class Iterator>
void pbcConstraintElementGroup::shiftedLagrangePeriodicCondition(Iterator itbegin, Iterator itend,
                                   std::vector<MVertex*>& vlist,std::set<MVertex*>& others, const SVector3& normal){

	for (Iterator it = itbegin; it != itend; it++){
    MVertex* v= *it;
    if (others.find(v) == others.end()){
			constraintElement* cele = new shiftedLagrangeConstraintElement(_solver->getMicroBC(),_LagSpace,_MultSpace,
																					-1,v,vlist,normal);
			_allConstraint.insert(cele);
    };
	}
};

template<class Iterator>
void pbcConstraintElementGroup::shiftedLagrangePeriodicConditionItMap(Iterator itbegin, Iterator itend,
                                   std::vector<MVertex*>& vlist,std::set<MVertex*>& others, const SVector3& normal){

	for (Iterator it = itbegin; it != itend; it++){
    MVertex* v= it->second;
    if (others.find(v) == others.end()){
			constraintElement* cele = new shiftedLagrangeConstraintElement(_solver->getMicroBC(),_LagSpace,_MultSpace,
																					-1,v,vlist,normal);
			_allConstraint.insert(cele);
    };
	}
};

template<class Iterator>
void pbcConstraintElementGroup::directionalLagrangeFormCondition(Iterator itbegin, Iterator itend,
                                std::vector<MVertex*>& vlist, const SVector3& pbcDir){

	int vsize = vlist.size();
  for (Iterator it = itbegin; it!= itend;it++){
    MVertex* v = *it;
    bool flag = true;

    for (int i=0; i<vsize; i++){
      if (v->getNum() == vlist[i]->getNum()){
        flag = false;
        break;
      }
    };

    if (flag){
			constraintElement* cele = new directionalFormLagrangeConstraintElement(_solver->getMicroBC(),_LagSpace,_MultSpace,v,vlist,pbcDir);
			_allConstraint.insert(cele);
    };
  };

};
template<class Iterator>
void pbcConstraintElementGroup::directionalLagrangeFormConditionItMap(Iterator itbegin, Iterator itend,
                                std::vector<MVertex*>& vlist, const SVector3& pbcDir){

	int vsize = vlist.size();
  for (Iterator it = itbegin; it!= itend;it++){
    MVertex* v = it->second;
    bool flag = true;

    for (int i=0; i<vsize; i++){
      if (v->getNum() == vlist[i]->getNum()){
        flag = false;
        break;
      }
    };

    if (flag){
			constraintElement* cele = new directionalFormLagrangeConstraintElement(_solver->getMicroBC(),_LagSpace,_MultSpace,v,vlist,pbcDir);
			_allConstraint.insert(cele);
    };
  };

};

template<class Iterator>
void pbcConstraintElementGroup::directionalLagrangePeriodicCondition(Iterator itbegin, Iterator itend,
                                   std::vector<MVertex*>& xlist, std::vector<MVertex*>& ylist,
																	 const SVector3& pbcNormal, const bool allComp){
	if (xlist[0]->getNum() != ylist[0]->getNum()){
		Msg::Error("construction of interplolation basis is not correct pbcConstraintElementGroup::directionalLagrangePeriodicCondition");
	}
	int xsize = xlist.size();
	int ysize = ylist.size();
  for (Iterator it = itbegin; it!= itend;it++){
    MVertex* v = *it;
    bool flag = true;

    for (int i=0; i<xsize; i++){
      if (v->getNum() == xlist[i]->getNum()){
        flag = false;
        break;
      }
    };
		if (flag){
			for (int i=0; i<ysize; i++){
				if (v->getNum() == ylist[i]->getNum()){
					flag = false;
					break;
				}
			};
		}

    if (flag){
			SPoint3 pt = v->point();
			SPoint3 v1 = xlist[0]->point();
			SPoint3 v2 = xlist[xsize-1]->point();

			SPoint3 pp = planeDirProject(pt,v1,v2,pbcNormal);

			SVector3 ppv1(pp,v1);
			SVector3 ppv2(pp,v2);
			if (dot(ppv1,ppv2) <=0.){
				constraintElement* cele;
				if (allComp){
					cele = new lagrangeConstraintElement(_solver->getMicroBC(),_LagSpace,_MultSpace,-1,v,xlist,pbcNormal);
				}
				else{
					cele = new directionalPBCLagrangeConstraintElement(_solver->getMicroBC(),_LagSpace,_MultSpace,v,xlist,pbcNormal);
				}

				_allConstraint.insert(cele);
			}
			else{
				constraintElement* cele;
				if (allComp){
					cele = new lagrangeConstraintElement(_solver->getMicroBC(),_LagSpace,_MultSpace,-1,v,ylist,pbcNormal);
				}
				else{
					cele = new directionalPBCLagrangeConstraintElement(_solver->getMicroBC(),_LagSpace,_MultSpace,v,ylist,pbcNormal);
				}
				_allConstraint.insert(cele);
			}
    };
  };
};

template<class Iterator>
void pbcConstraintElementGroup::directionalLagrangePeriodicConditionItMap(Iterator itbegin, Iterator itend,
                                   std::vector<MVertex*>& xlist, std::vector<MVertex*>& ylist,
																	 const SVector3& pbcNormal, const bool allComp){
	if (xlist[0]->getNum() != ylist[0]->getNum()){
		Msg::Error("construction of interplolation basis is not correct pbcConstraintElementGroup::directionalLagrangePeriodicCondition");
	}
	int xsize = xlist.size();
	int ysize = ylist.size();
  for (Iterator it = itbegin; it!= itend;it++){
    MVertex* v = it->second;
    bool flag = true;

    for (int i=0; i<xsize; i++){
      if (v->getNum() == xlist[i]->getNum()){
        flag = false;
        break;
      }
    };
		if (flag){
			for (int i=0; i<ysize; i++){
				if (v->getNum() == ylist[i]->getNum()){
					flag = false;
					break;
				}
			};
		}

    if (flag){
			SPoint3 pt = v->point();
			SPoint3 v1 = xlist[0]->point();
			SPoint3 v2 = xlist[xsize-1]->point();

			SPoint3 pp = planeDirProject(pt,v1,v2,pbcNormal);

			SVector3 ppv1(pp,v1);
			SVector3 ppv2(pp,v2);
			if (dot(ppv1,ppv2) <=0.){
				constraintElement* cele;
				if (allComp){
					cele = new lagrangeConstraintElement(_solver->getMicroBC(),_LagSpace,_MultSpace,-1,v,xlist,pbcNormal);
				}
				else{
					cele = new directionalPBCLagrangeConstraintElement(_solver->getMicroBC(),_LagSpace,_MultSpace,v,xlist,pbcNormal);
				}

				_allConstraint.insert(cele);
			}
			else{
				constraintElement* cele;
				if (allComp){
					cele = new lagrangeConstraintElement(_solver->getMicroBC(),_LagSpace,_MultSpace,-1,v,ylist,pbcNormal);
				}
				else{
					cele = new directionalPBCLagrangeConstraintElement(_solver->getMicroBC(),_LagSpace,_MultSpace,v,ylist,pbcNormal);
				}
				_allConstraint.insert(cele);
			}
    };
  };
};


// for lagrange
template<class Iterator>
void pbcConstraintElementGroup::lagrangeFormConditionCoonsPatch(Iterator itbegin, Iterator itend, MVertex* vref,
                                                     std::vector<MVertex*>& xlist, std::vector<MVertex*>& ylist,
                                                     std::set<MVertex*>& others, MVertex* vrootP, MVertex* vrootN,
                                                    const int comp){
	for (Iterator it = itbegin; it!= itend;it++){
    MVertex* v = *it;
    if (others.find(v)==others.end()){
			constraintElement* cele = new CoonsPatchLagrangeConstraintElement(_solver->getMicroBC(),_LagSpace,_MultSpace,
																										comp,v,vref,xlist,ylist, vrootP,vrootN);
			_allConstraint.insert(cele);

    };
  };
};
template<class Iterator>
void pbcConstraintElementGroup::lagrangeFormConditionCoonsPatchItMap(Iterator itbegin, Iterator itend, MVertex* vref,
                                                     std::vector<MVertex*>& xlist, std::vector<MVertex*>& ylist,
                                                     std::set<MVertex*>& others, MVertex* vrootP, MVertex* vrootN,
                                                    const int comp){
	for (Iterator it = itbegin; it!= itend;it++){
    MVertex* v = it->second;
    if (others.find(v)==others.end()){
			constraintElement* cele = new CoonsPatchLagrangeConstraintElement(_solver->getMicroBC(),_LagSpace,_MultSpace,
																										comp,v,vref,xlist,ylist, vrootP,vrootN);
			_allConstraint.insert(cele);

    };
  };
};

template<class Iterator>
void pbcConstraintElementGroup::lagrangePeriodicConditionCoonsPatch(Iterator itbegin, Iterator itend, MVertex* vref,
                                                         std::vector<MVertex*>& xlist,std::vector<MVertex*>& ylist,
                                                         std::set<MVertex*>& others,  MVertex* vrootP, MVertex* vrootN, const int comp){
	for (Iterator it = itbegin; it!= itend;it++){
    MVertex* v = *it;
    if (others.find(v)==others.end()){
			constraintElement* cele = new CoonsPatchLagrangeConstraintElement(_solver->getMicroBC(),_LagSpace,_MultSpace,
													comp,v,vref,xlist,ylist, vrootP,vrootN);
			_allConstraint.insert(cele);
    };
  };
};

template<class Iterator>
void pbcConstraintElementGroup::lagrangePeriodicConditionCoonsPatchItMap(Iterator itbegin, Iterator itend, MVertex* vref,
                                                         std::vector<MVertex*>& xlist,std::vector<MVertex*>& ylist,
                                                         std::set<MVertex*>& others,  MVertex* vrootP, MVertex* vrootN, const int comp){
	for (Iterator it = itbegin; it!= itend;it++){
    MVertex* v = it->second;
    if (others.find(v)==others.end()){
			constraintElement* cele = new CoonsPatchLagrangeConstraintElement(_solver->getMicroBC(),_LagSpace,_MultSpace,
													comp,v,vref,xlist,ylist, vrootP,vrootN);
			_allConstraint.insert(cele);
    };
  };
};

template<class Iterator>
void pbcConstraintElementGroup::pbcFormFEConstraintElementGroup(Iterator itbegin, Iterator itend,
                elementGroup* g, std::set<MVertex*>& others,
                 MVertex* vrootP, MVertex* vrootN, const int comp){
  std::set<MVertex*> allCheck(others);
  addVertexToGroupItMap(allCheck,g->vbegin(),g->vend());
  for (Iterator it = itbegin; it!= itend;it++){
    MVertex* v = *it;
    if (allCheck.find(v)==allCheck.end()){
      MElement* e = NULL;
      for (elementGroup::elementContainer::const_iterator itg = g->begin(); itg!= g->end(); itg++){
        MElement* ele = itg->second;
        if (InterpolationOperations::isInside(v,ele)){
          e = ele;
          break;
        }
      }
      if (e == NULL){
        Msg::Error("receive element not found");
      }
      else{
				constraintElement* cele = new FEConstraintElement(_solver->getMicroBC() ,_LagSpace,_MultSpace, comp,v,e,vrootP,vrootN);
				_allConstraint.insert(cele);
      }
    };
  };
};

template<class Iterator>
void pbcConstraintElementGroup::pbcFormFEConstraintElementGroupItMap(Iterator itbegin, Iterator itend,
                elementGroup* g, std::set<MVertex*>& others,
                 MVertex* vrootP, MVertex* vrootN, const int comp){
  std::set<MVertex*> allCheck(others);
  addVertexToGroupItMap(allCheck,g->vbegin(),g->vend());
  for (Iterator it = itbegin; it!= itend;it++){
    MVertex* v = it->second;
    if (allCheck.find(v)==allCheck.end()){
      MElement* e = NULL;
      for (elementGroup::elementContainer::const_iterator itg = g->begin(); itg!= g->end(); itg++){
        MElement* ele = itg->second;
        if (InterpolationOperations::isInside(v,ele)){
          e = ele;
          break;
        }
      }
      if (e == NULL){
        Msg::Error("receive element not found");
      }
      else{
				constraintElement* cele = new FEConstraintElement(_solver->getMicroBC() ,_LagSpace,_MultSpace, comp,v,e,vrootP,vrootN);
				_allConstraint.insert(cele);
      }
    };
  };
};


template<class Iterator>
void pbcConstraintElementGroup::pbcFEConstraintElementGroup(Iterator itbegin, Iterator itend,
                elementGroup* g, std::set<MVertex*>& others,
                 MVertex* vrootP, MVertex* vrootN, const int comp){
  for (Iterator it = itbegin; it!= itend;it++){
    MVertex* v = *it;
    if (others.find(v)==others.end()){
      MElement* e = NULL;
      for (elementGroup::elementContainer::const_iterator itg = g->begin(); itg!= g->end(); itg++){
        MElement* ele = itg->second;
        if (InterpolationOperations::isInside(v,ele)){
          e = ele;
          break;
        }
      }
      if (e == NULL){
        Msg::Error("receive element not found");
      }
      else{
				constraintElement* cele = new FEConstraintElement(_solver->getMicroBC() ,_LagSpace,_MultSpace, comp,v,e,vrootP,vrootN);
				_allConstraint.insert(cele);
      }
    };
  };
};

template<class Iterator>
void pbcConstraintElementGroup::pbcFEConstraintElementGroupItMap(Iterator itbegin, Iterator itend,
                elementGroup* g, std::set<MVertex*>& others,
                 MVertex* vrootP, MVertex* vrootN, const int comp){
  for (Iterator it = itbegin; it!= itend;it++){
    MVertex* v = it->second;
    if (others.find(v)==others.end()){
      MElement* e = NULL;
      for (elementGroup::elementContainer::const_iterator itg = g->begin(); itg!= g->end(); itg++){
        MElement* ele = itg->second;
        if (InterpolationOperations::isInside(v,ele)){
          e = ele;
          break;
        }
      }
      if (e == NULL){
        Msg::Error("receive element not found");
      }
      else{
				constraintElement* cele = new FEConstraintElement(_solver->getMicroBC() ,_LagSpace,_MultSpace, comp,v,e,vrootP,vrootN);
				_allConstraint.insert(cele);
      }
    };
  };
};

void pbcConstraintElementGroup::createFEConstraintElementGroup(){
  int dim = _solver->getMicroBC()->getDim();
  nonLinearPeriodicBC* pbc = static_cast<nonLinearPeriodicBC*>(_solver->getMicroBC());
  const std::vector<elementGroup*>& bgroup = _solver->getMicroBC()->getBoundaryElementGroup();
  // create base
  std::set<MVertex*> newVirVertices;
  if (dim ==2){
    if (_gX == NULL) _gX = new elementGroup();
    else _gX->clearAll();
    
    if (_gY == NULL) _gY = new elementGroup();
    else _gY->clearAll();
    
    if (pbc->getPBCMethod() == nonLinearPeriodicBC::FE_LIN){
      InterpolationOperations::createPolynomialBasePoints(pbc,_v1,_v2,_v4,_v5,l12,l41,l15,_xBase,_yBase,_zBase,newVirVertices);
      InterpolationOperations::createGroupLinearLineElementFromVerticesVector(_xBase,*_gX);
      InterpolationOperations::createGroupLinearLineElementFromVerticesVector(_yBase,*_gY);
    }
    else if (pbc->getPBCMethod() == nonLinearPeriodicBC::FE_QUA){
      InterpolationOperations::createPolynomialBasePoints(pbc,_v1,_v2,_v4,_v5,l12,l41,l15,_xBase,_yBase,_zBase,newVirVertices);
      InterpolationOperations::createGroupQuadraticLineElementFromVerticesVector(_xBase,*_gX,newVirVertices);
      InterpolationOperations::createGroupQuadraticLineElementFromVerticesVector(_yBase,*_gY,newVirVertices);
    }
    else if (pbc->getPBCMethod() == nonLinearPeriodicBC::FE_HIGH_ORDER)
    {
      int order = pbc->getMaxDegree();
      Msg::Info("using order %d nonLinearPeriodicBC::FE_HIGH_ORDER",order);
      
      MElement* eleX(NULL), *eleY(NULL);
      InterpolationOperations::create_MLineN_Base(order,_v1,_v2,_v4,eleX,eleY,newVirVertices);
      _gX->insert(eleX);
      _gY->insert(eleY);
      
    }
    _virtualVertices.insert(newVirVertices.begin(),newVirVertices.end());
    
    this->pbcFormFEConstraintElementGroup(l12.begin(),l12.end(),_gX,_cVertices,_v1,_v1);
    this->pbcFEConstraintElementGroup(l34.begin(),l34.end(),_gX,_cVertices,_v4,_v1);
    this->pbcFormFEConstraintElementGroup(l41.begin(),l41.end(),_gY,_cVertices,_v1,_v1);
    this->pbcFEConstraintElementGroup(l23.begin(),l23.end(),_gY,_cVertices,_v2,_v1);
    
    // to file
    elementGroup gr;
    gr.insert(*_gX);
    gr.insert(*_gY);
    std::string interpolationMeshFileName = "EdgeBasis_PBC_"+std::to_string(pbc->getMaxDegree())+".msh";
    printf("wring interpolation basis to mesh file: %s\n",interpolationMeshFileName.c_str());
    InterpolationOperations::write_MSH2(&gr,interpolationMeshFileName);
    
  }
  else if (dim == 3){
    if (_gX == NULL) _gX = new elementGroup();
    else _gX->clearAll();
    if (_gY == NULL) _gY = new elementGroup();
    else _gY->clearAll();
    if (_gZ == NULL) _gZ = new elementGroup();
    else _gZ->clearAll();
    if (_gXY == NULL) _gXY = new elementGroup();
    else _gXY->clearAll();
    if (_gYZ == NULL) _gYZ = new elementGroup();
    else _gYZ->clearAll();
    if (_gZX == NULL) _gZX = new elementGroup();
    else _gZX->clearAll();
    
    
    if (pbc->getPBCMethod() == nonLinearPeriodicBC::FE_LIN){
      InterpolationOperations::createPolynomialBasePoints(pbc,_v1,_v2,_v4,_v5,l12,l41,l15,_xBase,_yBase,_zBase,newVirVertices);
      InterpolationOperations::createGroupLinearLineElementFromVerticesVector(_xBase,*_gX);
      InterpolationOperations::createGroupLinearLineElementFromVerticesVector(_yBase,*_gY);
      InterpolationOperations::createGroupLinearLineElementFromVerticesVector(_zBase,*_gZ);
      _bVertices.insert(newVirVertices.begin(),newVirVertices.end());
      
      InterpolationOperations::createLinearElementBCGroup3DFromTwoEdges(_xBase,_yBase,_v3,*_gXY,newVirVertices);
      InterpolationOperations::createLinearElementBCGroup3DFromTwoEdges(_yBase,_zBase,_v8,*_gYZ,newVirVertices);
      InterpolationOperations::createLinearElementBCGroup3DFromTwoEdges(_zBase,_xBase,_v6,*_gZX,newVirVertices);
      _virtualVertices.insert(newVirVertices.begin(),newVirVertices.end());
      
    }
    else if (pbc->getPBCMethod() == nonLinearPeriodicBC::FE_QUA){
      InterpolationOperations::createPolynomialBasePoints(pbc,_v1,_v2,_v4,_v5,l12,l41,l15,_xBase,_yBase,_zBase,newVirVertices);
      InterpolationOperations::createGroupQuadraticLineElementFromVerticesVector(_xBase,*_gX,newVirVertices);
      InterpolationOperations::createGroupQuadraticLineElementFromVerticesVector(_yBase,*_gY,newVirVertices);
      InterpolationOperations::createGroupQuadraticLineElementFromVerticesVector(_zBase,*_gZ,newVirVertices);
      _bVertices.insert(newVirVertices.begin(),newVirVertices.end());
      
      InterpolationOperations::createQuadraticElementBCGroup3DFromTwoEdges(_xBase,_yBase,_v3,*_gXY,newVirVertices);
      InterpolationOperations::createQuadraticElementBCGroup3DFromTwoEdges(_yBase,_zBase,_v8,*_gYZ,newVirVertices);
      InterpolationOperations::createQuadraticElementBCGroup3DFromTwoEdges(_zBase,_xBase,_v6,*_gZX,newVirVertices);
      _virtualVertices.insert(newVirVertices.begin(),newVirVertices.end());
    }
    else if (pbc->getPBCMethod() == nonLinearPeriodicBC::FE_HIGH_ORDER)
    {
      int order = pbc->getMaxDegree();
      bool serendip = pbc->usingSerendipFlag();
      Msg::Info("using order %d nonLinearPeriodicBC::FE_HIGH_ORDER",order);
      
      MElement* eX(NULL), *eY(NULL), *eZ(NULL), *eXY(NULL), *eYZ(NULL), *eZX(NULL);
      std::set<MVertex*> newBounderyVers;
      InterpolationOperations::create_MQuadrangleN_Base(order,serendip,_v1,_v2,_v3,_v4,_v5,_v6,_v7,_v8, eX, eY, eZ, eXY, eYZ, eZX, newBounderyVers,newVirVertices);
      _bVertices.insert(newBounderyVers.begin(),newBounderyVers.end());
      _virtualVertices.insert(newVirVertices.begin(),newVirVertices.end());
      
      _gX->insert(eX);
      _gY->insert(eY);
      _gZ->insert(eZ);
      _gXY->insert(eXY);
      _gYZ->insert(eYZ);
      _gZX->insert(eZX);
    }
    
    // to file
    elementGroup gr;
    gr.insert(*_gX);
    gr.insert(*_gY);
    gr.insert(*_gZ);
    gr.insert(*_gXY);
    gr.insert(*_gYZ);
    gr.insert(*_gZX);
    std::string interpolationMeshFileName = "FaceBasis_PBC_"+std::to_string(pbc->getMaxDegree())+".msh";
    printf("wring interpolation basis to mesh file: %s\n",interpolationMeshFileName.c_str());
    InterpolationOperations::write_MSH2(&gr,interpolationMeshFileName);
    

    this->pbcFormFEConstraintElementGroup(l12.begin(),l12.end(),_gX,_cVertices,_v1,_v1);
    this->pbcFEConstraintElementGroup(l34.begin(),l34.end(),_gX,_cVertices,_v4,_v1);
    this->pbcFEConstraintElementGroup(l78.begin(),l78.end(),_gX,_cVertices,_v8,_v1);
    this->pbcFEConstraintElementGroup(l56.begin(),l56.end(),_gX,_cVertices,_v5,_v1);

    this->pbcFormFEConstraintElementGroup(l41.begin(),l41.end(),_gY,_cVertices,_v1,_v1);
    this->pbcFEConstraintElementGroup(l23.begin(),l23.end(),_gY,_cVertices,_v2,_v1);
    this->pbcFEConstraintElementGroup(l67.begin(),l67.end(),_gY,_cVertices,_v6,_v1);
    this->pbcFEConstraintElementGroup(l85.begin(),l85.end(),_gY,_cVertices,_v5,_v1);

    this->pbcFormFEConstraintElementGroup(l15.begin(),l15.end(),_gZ,_cVertices,_v1,_v1);
    this->pbcFEConstraintElementGroup(l26.begin(),l26.end(),_gZ,_cVertices,_v2,_v1);
    this->pbcFEConstraintElementGroup(l37.begin(),l37.end(),_gZ,_cVertices,_v3,_v1);
    this->pbcFEConstraintElementGroup(l48.begin(),l48.end(),_gZ,_cVertices,_v4,_v1);


    this->pbcFormFEConstraintElementGroupItMap(f1234->vbegin(), f1234->vend(),_gXY,_bVertices,_v1,_v1);
    this->pbcFormFEConstraintElementGroupItMap(f1584->vbegin(), f1584->vend(),_gYZ,_bVertices,_v1,_v1);
    this->pbcFormFEConstraintElementGroupItMap(f1562->vbegin(), f1562->vend(),_gZX,_bVertices,_v1,_v1);

    this->pbcFEConstraintElementGroupItMap(f5678->vbegin(), f5678->vend(),_gXY,_bVertices,_v5,_v1);
    this->pbcFEConstraintElementGroupItMap(f2673->vbegin(), f2673->vend(),_gYZ,_bVertices,_v2,_v1);
    this->pbcFEConstraintElementGroupItMap(f4873->vbegin(), f4873->vend(),_gZX,_bVertices,_v4,_v1);
  }

};

void pbcConstraintElementGroup::createProjectConstraintElementGroup(){
  int dim = _solver->getMicroBC()->getDim();
  nonLinearPeriodicBC* pbc = static_cast<nonLinearPeriodicBC*>(_solver->getMicroBC());
  const std::vector<elementGroup*>& bgroup = _solver->getMicroBC()->getBoundaryElementGroup();

	if (dim == 2){
    this->pbcFEConstraintElementGroup(l34.begin(),l34.end(),bgroup[0],_cVertices,_v4,_v1);
    this->pbcFEConstraintElementGroup(l23.begin(),l23.end(),bgroup[1],_cVertices,_v2,_v1);
	}
	else if (dim == 3){
		if (_gX == NULL) _gX = new elementGroup();
    else _gX->clearAll();
    if (_gY == NULL) _gY = new elementGroup();
    else _gY->clearAll();
    if (_gZ == NULL) _gZ = new elementGroup();
    else _gZ->clearAll();
    
		InterpolationOperations::intersectionBetweenTwoFaces(f1234,f1562,*_gX);
		InterpolationOperations::intersectionBetweenTwoFaces(f1234,f1584,*_gY);
		InterpolationOperations::intersectionBetweenTwoFaces(f1562,f1584,*_gZ);

    this->pbcFEConstraintElementGroup(l34.begin(),l34.end(),_gX,_cVertices,_v4,_v1);
    this->pbcFEConstraintElementGroup(l78.begin(),l78.end(),_gX,_cVertices,_v8,_v1);
    this->pbcFEConstraintElementGroup(l56.begin(),l56.end(),_gX,_cVertices,_v5,_v1);

    this->pbcFEConstraintElementGroup(l23.begin(),l23.end(),_gY,_cVertices,_v2,_v1);
    this->pbcFEConstraintElementGroup(l67.begin(),l67.end(),_gY,_cVertices,_v6,_v1);
    this->pbcFEConstraintElementGroup(l85.begin(),l85.end(),_gY,_cVertices,_v5,_v1);

    this->pbcFEConstraintElementGroup(l26.begin(),l26.end(),_gZ,_cVertices,_v2,_v1);
    this->pbcFEConstraintElementGroup(l37.begin(),l37.end(),_gZ,_cVertices,_v3,_v1);
    this->pbcFEConstraintElementGroup(l48.begin(),l48.end(),_gZ,_cVertices,_v4,_v1);

    this->pbcFEConstraintElementGroupItMap(f5678->vbegin(), f5678->vend(),f1234,_bVertices,_v5,_v1);
    this->pbcFEConstraintElementGroupItMap(f2673->vbegin(), f2673->vend(),f1584,_bVertices,_v2,_v1);
    this->pbcFEConstraintElementGroupItMap(f4873->vbegin(), f4873->vend(),f1562,_bVertices,_v4,_v1);

	}
};

void pbcConstraintElementGroup::createPeriodicMeshConstraintElementGroup(){
  nonLinearPeriodicBC* pbc =  static_cast<nonLinearPeriodicBC*>(_solver->getMicroBC());
  int dim = _solver->getMicroBC()->getDim();
  const std::vector<elementGroup*>& bgroup = _solver->getMicroBC()->getBoundaryElementGroup();
    
  if (_constrainedCompPrimary.size() > 0)
  {
    const std::set<int>& constrainedComp = pbc->getConstrainedComps();
    for (std::set<int>::const_iterator itc = constrainedComp.begin(); itc != constrainedComp.end(); itc++)
    {
      int comp = *itc;
      if (_constrainedCompPrimary.find(comp) == _constrainedCompPrimary.end())
      {
        if (dim ==3)
        {
          // periodic of edges
          periodicConditionForPeriodicMesh(comp,1.,l56.begin(),l56.end(),l12.begin(),l12.end(),_cVertices,_v5,_v1);
          periodicConditionForPeriodicMesh(comp,1.,l78.begin(),l78.end(),l12.begin(),l12.end(),_cVertices,_v8,_v1);
          periodicConditionForPeriodicMesh(comp,1.,l34.begin(),l34.end(),l12.begin(),l12.end(),_cVertices,_v4,_v1);

          periodicConditionForPeriodicMesh(comp,1.,l23.begin(),l23.end(),l41.begin(),l41.end(),_cVertices,_v2,_v1);
          periodicConditionForPeriodicMesh(comp,1.,l67.begin(),l67.end(),l41.begin(),l41.end(),_cVertices,_v6,_v1);
          periodicConditionForPeriodicMesh(comp,1.,l85.begin(),l85.end(),l41.begin(),l41.end(),_cVertices,_v5,_v1);

          periodicConditionForPeriodicMesh(comp,1.,l26.begin(),l26.end(),l15.begin(),l15.end(),_cVertices,_v2,_v1);
          periodicConditionForPeriodicMesh(comp,1.,l37.begin(),l37.end(),l15.begin(),l15.end(),_cVertices,_v3,_v1);
          periodicConditionForPeriodicMesh(comp,1.,l48.begin(),l48.end(),l15.begin(),l15.end(),_cVertices,_v4,_v1);


          periodicConditionForPeriodicMeshItMap(comp,1.,bgroup[3]->vbegin(),bgroup[3]->vend(),
                                           bgroup[0]->vbegin(),bgroup[0]->vend(),_bVertices,
                                           _v5,_v1);
          periodicConditionForPeriodicMeshItMap(comp,1.,bgroup[4]->vbegin(),bgroup[4]->vend(),
                                           bgroup[1]->vbegin(),bgroup[1]->vend(),_bVertices,
                                           _v4,_v1);
          periodicConditionForPeriodicMeshItMap(comp,1.,bgroup[5]->vbegin(),bgroup[5]->vend(),
                                           bgroup[2]->vbegin(),bgroup[2]->vend(),_bVertices,
                                           _v2,_v1);

        }
        else if (dim==2)
        {
          periodicConditionForPeriodicMesh(comp,1.,l34.begin(),l34.end(),
                                           l12.begin(),l12.end(),_cVertices,
                                           _v4,_v1);
          periodicConditionForPeriodicMesh(comp,1.,l23.begin(),l23.end(),
                                           l41.begin(),l41.end(),_cVertices,
                                           _v2,_v1);
        }
      }
      else
      {
        if (dim ==3)
        {
          // periodic of edges
          std::set<MVertex*> notAccountedVertices;
          notAccountedVertices.insert(_allHighOrderVertices.begin(), _allHighOrderVertices.end());
          notAccountedVertices.insert(_cVertices.begin(), _cVertices.end());
          periodicConditionForPeriodicMesh(comp,1.,l56.begin(),l56.end(),l12.begin(),l12.end(),notAccountedVertices,_v5,_v1);
          periodicConditionForPeriodicMesh(comp,1.,l78.begin(),l78.end(),l12.begin(),l12.end(),notAccountedVertices,_v8,_v1);
          periodicConditionForPeriodicMesh(comp,1.,l34.begin(),l34.end(),l12.begin(),l12.end(),notAccountedVertices,_v4,_v1);

          periodicConditionForPeriodicMesh(comp,1.,l23.begin(),l23.end(),l41.begin(),l41.end(),notAccountedVertices,_v2,_v1);
          periodicConditionForPeriodicMesh(comp,1.,l67.begin(),l67.end(),l41.begin(),l41.end(),notAccountedVertices,_v6,_v1);
          periodicConditionForPeriodicMesh(comp,1.,l85.begin(),l85.end(),l41.begin(),l41.end(),notAccountedVertices,_v5,_v1);

          periodicConditionForPeriodicMesh(comp,1.,l26.begin(),l26.end(),l15.begin(),l15.end(),notAccountedVertices,_v2,_v1);
          periodicConditionForPeriodicMesh(comp,1.,l37.begin(),l37.end(),l15.begin(),l15.end(),notAccountedVertices,_v3,_v1);
          periodicConditionForPeriodicMesh(comp,1.,l48.begin(),l48.end(),l15.begin(),l15.end(),notAccountedVertices,_v4,_v1);

          
          notAccountedVertices.insert(_bVertices.begin(),_bVertices.end());
          periodicConditionForPeriodicMeshItMap(comp,1.,bgroup[3]->vbegin(),bgroup[3]->vend(),
                                           bgroup[0]->vbegin(),bgroup[0]->vend(),notAccountedVertices,
                                           _v5,_v1);
          periodicConditionForPeriodicMeshItMap(comp,1.,bgroup[4]->vbegin(),bgroup[4]->vend(),
                                           bgroup[1]->vbegin(),bgroup[1]->vend(),notAccountedVertices,
                                           _v4,_v1);
          periodicConditionForPeriodicMeshItMap(comp,1.,bgroup[5]->vbegin(),bgroup[5]->vend(),
                                           bgroup[2]->vbegin(),bgroup[2]->vend(),notAccountedVertices,
                                           _v2,_v1);

        }
        else if (dim==2)
        {
          std::set<MVertex*> notAccountedVertices;
          notAccountedVertices.insert(_allHighOrderVertices.begin(), _allHighOrderVertices.end());
          notAccountedVertices.insert(_cVertices.begin(), _cVertices.end());
          periodicConditionForPeriodicMesh(comp,1.,l34.begin(),l34.end(),
                                           l12.begin(),l12.end(),notAccountedVertices,
                                           _v4,_v1);
          periodicConditionForPeriodicMesh(comp,1.,l23.begin(),l23.end(),
                                           l41.begin(),l41.end(),notAccountedVertices,
                                           _v2,_v1);
        }
      }
    }
  }
  else
  {

    if (dim ==3){
      // periodic of edges
      periodicConditionForPeriodicMesh(l56.begin(),l56.end(),l12.begin(),l12.end(),_cVertices,_v5,_v1);
      periodicConditionForPeriodicMesh(l78.begin(),l78.end(),l12.begin(),l12.end(),_cVertices,_v8,_v1);
      periodicConditionForPeriodicMesh(l34.begin(),l34.end(),l12.begin(),l12.end(),_cVertices,_v4,_v1);

      periodicConditionForPeriodicMesh(l23.begin(),l23.end(),l41.begin(),l41.end(),_cVertices,_v2,_v1);
      periodicConditionForPeriodicMesh(l67.begin(),l67.end(),l41.begin(),l41.end(),_cVertices,_v6,_v1);
      periodicConditionForPeriodicMesh(l85.begin(),l85.end(),l41.begin(),l41.end(),_cVertices,_v5,_v1);

      periodicConditionForPeriodicMesh(l26.begin(),l26.end(),l15.begin(),l15.end(),_cVertices,_v2,_v1);
      periodicConditionForPeriodicMesh(l37.begin(),l37.end(),l15.begin(),l15.end(),_cVertices,_v3,_v1);
      periodicConditionForPeriodicMesh(l48.begin(),l48.end(),l15.begin(),l15.end(),_cVertices,_v4,_v1);


      periodicConditionForPeriodicMeshItMap(bgroup[3]->vbegin(),bgroup[3]->vend(),
                                       bgroup[0]->vbegin(),bgroup[0]->vend(),_bVertices,
                                       _v5,_v1);
      periodicConditionForPeriodicMeshItMap(bgroup[4]->vbegin(),bgroup[4]->vend(),
                                       bgroup[1]->vbegin(),bgroup[1]->vend(),_bVertices,
                                       _v4,_v1);
      periodicConditionForPeriodicMeshItMap(bgroup[5]->vbegin(),bgroup[5]->vend(),
                                       bgroup[2]->vbegin(),bgroup[2]->vend(),_bVertices,
                                       _v2,_v1);

    }
    else if (dim==2){
      periodicConditionForPeriodicMesh(l34.begin(),l34.end(),
                                       l12.begin(),l12.end(),_cVertices,
                                       _v4,_v1);
      periodicConditionForPeriodicMesh(l23.begin(),l23.end(),
                                       l41.begin(),l41.end(),_cVertices,
                                       _v2,_v1);
    }
  }
}

void pbcConstraintElementGroup::createCubicSplineConstraintElementGroup(){
  int dim = _solver->getMicroBC()->getDim();
  nonLinearPeriodicBC* pbc = static_cast<nonLinearPeriodicBC*>(_solver->getMicroBC());
  // create basis
  std::set<MVertex*> newVirVertices; // new vertices created in interpolation basis
  InterpolationOperations::createPolynomialBasePoints(pbc,_v1,_v2,_v4,_v5,l12,l41,l15,_xBase,_yBase,_zBase,newVirVertices);
  _virtualVertices.insert(newVirVertices.begin(),newVirVertices.end());

  if (dim ==2){
    this->cubicSplineFormCondition(l12.begin(),l12.end(),_xBase,0);
    this->cubicSplinePeriodicCondition(l34.begin(),l34.end(),_xBase,0,_cVertices);

    this->cubicSplineFormCondition(l41.begin(),l41.end(),_yBase,1);
    this->cubicSplinePeriodicCondition(l23.begin(),l23.end(),_yBase,1,_cVertices);
  }
  else if (dim ==3){
    // in 3D new ver are b-one
    _bVertices.insert(newVirVertices.begin(),newVirVertices.end());
    
    this->cubicSplineFormCondition(l12.begin(),l12.end(),_xBase,0);
    this->cubicSplineFormCondition(l41.begin(),l41.end(),_yBase,1);
    this->cubicSplineFormCondition(l15.begin(),l15.end(),_zBase,2);

    this->cubicSplinePeriodicCondition(l56.begin(),l56.end(),_xBase,0,_cVertices);
    this->cubicSplinePeriodicCondition(l78.begin(),l78.end(),_xBase,0,_cVertices);
    this->cubicSplinePeriodicCondition(l34.begin(),l34.end(),_xBase,0,_cVertices);

    this->cubicSplinePeriodicCondition(l23.begin(),l23.end(),_yBase,1,_cVertices);
    this->cubicSplinePeriodicCondition(l67.begin(),l67.end(),_yBase,1,_cVertices);
    this->cubicSplinePeriodicCondition(l85.begin(),l85.end(),_yBase,1,_cVertices);

    this->cubicSplinePeriodicCondition(l26.begin(),l26.end(),_zBase,2,_cVertices);
    this->cubicSplinePeriodicCondition(l37.begin(),l37.end(),_zBase,2,_cVertices);
    this->cubicSplinePeriodicCondition(l48.begin(),l48.end(),_zBase,2,_cVertices);


    this->cubicSplineFormConditionCoonsPatchItMap(f1234->vbegin(), f1234->vend(),_v1,_xBase,_yBase,0,1,_bVertices);
    this->cubicSplineFormConditionCoonsPatchItMap(f1562->vbegin(), f1562->vend(),_v1,_xBase,_zBase,0,2,_bVertices);
    this->cubicSplineFormConditionCoonsPatchItMap(f1584->vbegin(), f1584->vend(),_v1,_yBase,_zBase,1,2,_bVertices);

    this->cubicSplinePeriodicConditionCoonsPatchItMap(f5678->vbegin(), f5678->vend(),_v1,_xBase,_yBase,0,1,_bVertices);
    this->cubicSplinePeriodicConditionCoonsPatchItMap(f4873->vbegin(), f4873->vend(),_v1,_xBase,_zBase,0,2,_bVertices);
    this->cubicSplinePeriodicConditionCoonsPatchItMap(f2673->vbegin(), f2673->vend(),_v1,_yBase,_zBase,1,2,_bVertices);
  };
};

void pbcConstraintElementGroup::createShiftedLagrangeConstraintElementGroup(){
  nonLinearShiftedPeriodicBC* pbc = static_cast<nonLinearShiftedPeriodicBC*>(_solver->getMicroBC());

	const SVector3& normal = pbc->getShiftPBCNormal();

	int dim = _solver->getMicroBC()->getDim();
  const std::vector<elementGroup*>& bgroup = _solver->getMicroBC()->getBoundaryElementGroup();
 // create base
  std::set<MVertex*> newVirVertices;
  InterpolationOperations::createPolynomialBasePoints(pbc,_v1,_v2,_v4,_v5,l12,l41,l15,_xBase,_yBase,_zBase,newVirVertices);
  _virtualVertices.insert(newVirVertices.begin(),newVirVertices.end());

  if (dim ==2){
		this->lagrangeFormCondition(l12.begin(),l12.end(),_xBase);
		this->lagrangeFormCondition(l41.begin(),l41.end(),_yBase);

		SPoint3 p1 = _v1->point();
		SPoint3 p2 = _v2->point();
		SPoint3 p3 = _v3->point();
		SPoint3 p4 = _v4->point();
		SVector3 p1p2(p1,p2);

		SPoint3 p = planeDirProject(p3,p1,p2,normal);
		SVector3 p1p3(p1,p3);
		SVector3 p2p4(p2,p4);

		double phi0 = acos(dot(p1p2,p1p3)/(p1p2.norm()*p1p3.norm()));
		double phi1 = acos(dot(p1p2,p2p4)/(p1p2.norm()*p2p4.norm()));

		SVector3 pp3(p,p3);
		double phi = acos(dot(pp3,p1p2)/(pp3.norm()*p1p2.norm()));

		printf("phi = %f phi0 = %f phi1 = %f \n",phi*180./3.14159265359, phi0*180/3.14159265359,phi1*180./3.14159265359);

		if ((phi >=phi0) and (phi<=phi1)){
			Msg::Info("shifted l34");
			std::set<MVertex*> others;
			this->lagrangePeriodicCondition(l23.begin(),l23.end(),_yBase,others);
			others.insert(_v3); // avoid duplicating constraint at v3
			this->shiftedLagrangePeriodicCondition(l34.begin(),l34.end(),_xBase,others,normal);
		}
		else{
			Msg::Info("shifted l23");
			std::set<MVertex*> others;
			this->lagrangePeriodicCondition(l34.begin(),l34.end(),_xBase,others);
			others.insert(_v3); // avoid duplicating constraint at v3
			this->shiftedLagrangePeriodicCondition(l23.begin(),l23.end(),_yBase,others,normal);
		}

  }
	else{
		Msg::Error("createShiftedLagrangeConstraintElementGroup has not been implemented for 3D problems");
	}
};

void pbcConstraintElementGroup::createGeneralPBCLagrangeConstraintElementGroup(){
  nonLinearGeneralPeriodicBC* gpbc = static_cast<nonLinearGeneralPeriodicBC*>(_solver->getMicroBC());
  const std::vector<elementGroup*>& bgroup = _solver->getMicroBC()->getBoundaryElementGroup();
  const SVector3& pbcNormal = gpbc->getPBCNormal();
  int dim = _solver->getMicroBC()->getDim();
  if (dim == 2){
    SPoint3 p1 = _v1->point();
		SPoint3 p2 = _v2->point();
		SPoint3 p3 = _v3->point();
    
    SPoint3 p = planeDirProject(p3,p1,p2,pbcNormal);
    Msg::Info("found p1 = [%f %f %f], p2 = [%f %f %f], p = [%f %f %f]",p1[0],p1[1],p1[2],p2[0],p2[1],p2[2],p[0],p[1],p[2]);
    
    SVector3 vecp2p(p2,p);
    SVector3 vecp2p1(p2,p1);
    if (dot(vecp2p,vecp2p1) < 0){
      // base is constraint following l12, l23
      printf("interpolation based following l12,l23\n");
      printf("PBC following l34,l41\n");
      std::set<MVertex*> newVirVertices;
      InterpolationOperations::createPolynomialBasePoints(gpbc,_v2,_v1,_v3,_v6,l12,l23,l26,_xBase,_yBase,_zBase,newVirVertices);
      _virtualVertices.insert(newVirVertices.begin(),newVirVertices.end());
      
      // form condition
      this->lagrangeFormCondition(l12.begin(),l12.end(),_xBase);
      this->lagrangeFormCondition(l23.begin(),l23.end(),_yBase);
      
      // periodic BC
      std::set<MVertex*> allPositiveVertex;
      allPositiveVertex.insert(l34.begin(),l34.end());
      allPositiveVertex.insert(l41.begin(),l41.end());
      this->directionalLagrangePeriodicCondition(allPositiveVertex.begin(), allPositiveVertex.end(),_xBase,_yBase,pbcNormal,true);
      
      constraintElement* ppc;
      
      ppc = new periodicMeshConstraint(_solver->getMicroBC(),_LagSpace,_MultSpace,-1,_v2);
      _allConstraint.insert(ppc);
      
      ppc = new periodicSupplementConstraint(_solver->getMicroBC(),_LagSpace,_MultSpace,-1,bgroup[1],bgroup[3]);
      _allConstraint.insert(ppc);
      
      ppc = new periodicSupplementConstraint(_solver->getMicroBC(),_LagSpace,_MultSpace,-1,bgroup[0],bgroup[2]);
      _allConstraint.insert(ppc);
      
    }
    else{
      // base is constraint following l12, l41
      printf("interpolation based following l12,l41\n");
      printf("PBC following l23,l34\n");
      std::set<MVertex*> newVirVertices;
      InterpolationOperations::createPolynomialBasePoints(gpbc,_v1,_v2,_v4,_v5,l12,l41,l15,_xBase,_yBase,_zBase,newVirVertices);
      _virtualVertices.insert(newVirVertices.begin(),newVirVertices.end());
      
      // form condition
      this->lagrangeFormCondition(l12.begin(),l12.end(),_xBase);
      this->lagrangeFormCondition(l41.begin(),l41.end(),_yBase);
      
      // periodic BC
      std::set<MVertex*> allPositiveVertex;
      allPositiveVertex.insert(l23.begin(),l23.end());
      allPositiveVertex.insert(l34.begin(),l34.end());
      this->directionalLagrangePeriodicCondition(allPositiveVertex.begin(), allPositiveVertex.end(),_xBase,_yBase,pbcNormal,true);
      
      constraintElement* ppc;
      
      ppc = new periodicMeshConstraint(_solver->getMicroBC(),_LagSpace,_MultSpace,-1,_v1);
      _allConstraint.insert(ppc);
      
      ppc = new periodicSupplementConstraint(_solver->getMicroBC(),_LagSpace,_MultSpace,-1,bgroup[0],bgroup[2]);
      _allConstraint.insert(ppc);
      
      ppc = new periodicSupplementConstraint(_solver->getMicroBC(),_LagSpace,_MultSpace,-1,bgroup[1],bgroup[3]);
      _allConstraint.insert(ppc);
    }
  }
  else {
    Msg::Error("createShiftedLagrangeConstraintElementGroup has not been implemented for 3D problems");
  }
};

void pbcConstraintElementGroup::createLagrangeConstraintElementGroup(){
  int dim = _solver->getMicroBC()->getDim();
  nonLinearPeriodicBC* pbc = static_cast<nonLinearPeriodicBC*>(_solver->getMicroBC());
 // create base
  std::set<MVertex*> newVirVertices;
  InterpolationOperations::createPolynomialBasePoints(pbc,_v1,_v2,_v4,_v5,l12,l41,l15,_xBase,_yBase,_zBase,newVirVertices);
  _virtualVertices.insert(newVirVertices.begin(),newVirVertices.end());

  if (dim ==2){
    this->lagrangeFormCondition(l12.begin(),l12.end(),_xBase,_v1,_v1);
		this->lagrangePeriodicCondition(l34.begin(),l34.end(),_xBase,_cVertices,_v4,_v1);
    this->lagrangeFormCondition(l41.begin(),l41.end(),_yBase,_v1,_v1);
    this->lagrangePeriodicCondition(l23.begin(),l23.end(),_yBase,_cVertices,_v2,_v1);
  }
  else if (dim ==3){
    _bVertices.insert(newVirVertices.begin(),newVirVertices.end());
    
    this->lagrangeFormCondition(l12.begin(),l12.end(),_xBase,_v1,_v1);
    this->lagrangeFormCondition(l41.begin(),l41.end(),_yBase,_v1,_v1);
    this->lagrangeFormCondition(l15.begin(),l15.end(),_zBase,_v1,_v1);

    this->lagrangePeriodicCondition(l56.begin(),l56.end(),_xBase,_cVertices,_v5,_v1);
    this->lagrangePeriodicCondition(l78.begin(),l78.end(),_xBase,_cVertices,_v8,_v1);
    this->lagrangePeriodicCondition(l34.begin(),l34.end(),_xBase,_cVertices,_v4,_v1);

    this->lagrangePeriodicCondition(l23.begin(),l23.end(),_yBase,_cVertices,_v2,_v1);
    this->lagrangePeriodicCondition(l67.begin(),l67.end(),_yBase,_cVertices,_v6,_v1);
    this->lagrangePeriodicCondition(l85.begin(),l85.end(),_yBase,_cVertices,_v5,_v1);

    this->lagrangePeriodicCondition(l26.begin(),l26.end(),_zBase,_cVertices,_v2,_v1);
    this->lagrangePeriodicCondition(l37.begin(),l37.end(),_zBase,_cVertices,_v3,_v1);
    this->lagrangePeriodicCondition(l48.begin(),l48.end(),_zBase,_cVertices,_v4,_v1);

    this->lagrangeFormConditionCoonsPatchItMap(f1234->vbegin(), f1234->vend(),_v1,_xBase,_yBase,_bVertices,_v1,_v1);
    this->lagrangeFormConditionCoonsPatchItMap(f1562->vbegin(), f1562->vend(),_v1,_xBase,_zBase,_bVertices,_v1,_v1);
    this->lagrangeFormConditionCoonsPatchItMap(f1584->vbegin(), f1584->vend(),_v1,_yBase,_zBase,_bVertices,_v1,_v1);

    this->lagrangePeriodicConditionCoonsPatchItMap(f5678->vbegin(), f5678->vend(),_v1,_xBase,_yBase,_bVertices,_v5,_v1);
    this->lagrangePeriodicConditionCoonsPatchItMap(f4873->vbegin(), f4873->vend(),_v1,_xBase,_zBase,_bVertices,_v4,_v1);
    this->lagrangePeriodicConditionCoonsPatchItMap(f2673->vbegin(), f2673->vend(),_v1,_yBase,_zBase,_bVertices,_v2,_v1);
  };

};

void pbcConstraintElementGroup::createLinearDisplacementConstraintElementGroup(){
  const std::vector<elementGroup*>& bgroup = _solver->getMicroBC()->getBoundaryElementGroup();
  std::set<MVertex*> allVertices;
  for (int i=0; i<bgroup.size(); i++){
    elementGroup* g = bgroup[i];
    for (elementGroup::vertexContainer::const_iterator it = g->vbegin(); it!= g->vend(); it++){
      MVertex* v= it->second;
      if (allVertices.find(v) == allVertices.end())
        allVertices.insert(v);
    }
  }

  for (std::set<MVertex*>::iterator it = allVertices.begin(); it!= allVertices.end(); it++){
    MVertex* vp = *it;
		constraintElement* cel = new periodicMeshConstraint(_solver->getMicroBC(),_LagSpace,_MultSpace,-1,vp);
		_allConstraint.insert(cel);
  };
};

void pbcConstraintElementGroup::createMinimalKinematicConstraintElementGroup(){
  const std::vector<elementGroup*>& bgroup = _solver->getMicroBC()->getBoundaryElementGroup();
  int size = bgroup.size();
  if (_solver->getMicroBC()->getOrder() == 2){    
    for (int i=0; i<size; i++){
      constraintElement* ppc = new supplementConstraint(_solver->getMicroBC(),_LagSpace,_MultSpace,-1,bgroup[i]);
      _allConstraint.insert(ppc);
    }
  }
  else{
    for (int i=0; i< size/2; i++){
      elementGroup* gPlus = bgroup[i+size/2];
      elementGroup* gMinus = bgroup[i];

      if (i == 0){
        constraintElement* ppc = new supplementConstraint(_solver->getMicroBC(),_LagSpace,_MultSpace,-1,gPlus);
        _allConstraint.insert(ppc);
        ppc = new supplementConstraint(_solver->getMicroBC(),_LagSpace,_MultSpace,-1,gMinus);
        _allConstraint.insert(ppc);
      }
      else{
         constraintElement* ppc = new periodicSupplementConstraint(_solver->getMicroBC(),_LagSpace,_MultSpace,-1,gPlus,gMinus);
         _allConstraint.insert(ppc);
         
      }
    }
    
  }
};

void pbcConstraintElementGroup::createLinearDisplacementConstraintElementGroup_DG(){
  if (_boundaryDGMap.size() <=0) Msg::Error("_boundaryDGMap has not been initiated");
  int ndofsPerNode = _solver->getMicroBC()->getNumberOfConstrainedDofsPerVertex();
	const std::set<int>& constrainedComp = _solver->getMicroBC()->getConstrainedComps();

  std::set<Dof> allConstraintDof;
  for (int i=0; i<_boundaryDGMap.size(); i++){
		for (std::map<partDomain*,elementGroup*>::iterator it = _boundaryDGMap[i]->decompositionMap.begin();
								it != _boundaryDGMap[i]->decompositionMap.end(); it++){
			partDomain* dom = it->first;
			elementGroup* gr = it->second;
			FunctionSpaceBase* lagSpace = dom->getFunctionSpace();
			FunctionSpaceBase* multspace = _allDGMultSpace[dom];
			for (elementGroup::vertexContainer::const_iterator itgr = gr->vbegin(); itgr!= gr->vend(); itgr++){
				MVertex* v= itgr->second;
				for (std::set<int>::const_iterator itcomp = constrainedComp.begin(); itcomp!= constrainedComp.end(); itcomp++){
					std::vector<int> comp;
					std::vector<Dof> key;
					// get key for comp j
					comp.push_back(*itcomp);
					getKeysFromVertex(dom->getFunctionSpace(),v,comp,key);
					// check if Dof is already constrained
					if (allConstraintDof.find(key[0])== allConstraintDof.end()){
						allConstraintDof.insert(key[0]);
						// create constraint
						constraintElement* cel = new periodicMeshConstraint(_solver->getMicroBC(),lagSpace,multspace,*itcomp,v);
						_allConstraint.insert(cel);

					}
				}

			}
		}
  }
	Msg::Info("Number of constraint %d",_allConstraint.size());
};


void pbcConstraintElementGroup::createMinimalKinematicConstraintElementGroup_DG(){
	if (_boundaryDGMap.size() <=0) Msg::Error("_boundaryDGMap has not been initiated");
	const std::set<int>& constrainedComp = _solver->getMicroBC()->getConstrainedComps();

  for (int i=0; i<_boundaryDGMap.size(); i++){
		for (std::set<int>::const_iterator itcomp = constrainedComp.begin(); itcomp!= constrainedComp.end(); itcomp++){
			int comp = *itcomp;
			constraintElement* ppc = new supplementConstraintGeneral(_solver->getMicroBC(),_MultSpace,comp,_boundaryDGMap[i]);
			_allConstraint.insert(ppc);
		}
  }
};

void pbcConstraintElementGroup::createMixBCConstraintElementGroup(){
  nonLinearMixedBC* mixBC = dynamic_cast<nonLinearMixedBC*>(_solver->getMicroBC());
  
	const std::vector<elementGroup*>& bgroup = _solver->getMicroBC()->getBoundaryElementGroup();
	const std::vector<int>& bphysical = _solver->getMicroBC()->getBoundaryPhysicals();
  
  std::map<int,std::set<MVertex*> > directContraint;
  
  // apply on vertices
  const std::vector<std::pair<int,int> >&  vlist = mixBC->getFixVertices();
  for (int i=0; i< vlist.size(); i++){
    int vernum = vlist[i].first;
    int vercomp = vlist[i].second;
    elementGroup gv(0,vernum);
    MVertex* v = (gv.vbegin()->second);
    Msg::Info("fix Vertex = %d comp = %d",v->getNum(),vercomp);
    std::set<MVertex*>& vv = directContraint[vercomp];
    addVertexToGroupItMap(vv,gv.vbegin(),gv.vend());
  };
  
  const std::vector<std::pair<int,int> >& kphysical = mixBC->getKinematicPhysical();
  for (int i=0; i<kphysical.size(); i++){
		Msg::Info("k bc = %d comp = %d",kphysical[i].first,kphysical[i].second);
		const elementGroup* gr = _solver->getMicroBC()->getBCGroup(kphysical[i].first);
    int compK = kphysical[i].second;
    std::set<MVertex*>& vv = directContraint[compK];
    addVertexToGroupItMap(vv,gr->vbegin(),gr->vend());
  }
  
  for (std::map<int,std::set<MVertex*> >::iterator it = directContraint.begin(); it != directContraint.end(); it++){
    int compK = it->first;
    std::set<MVertex*>& allVer = it->second;
    for (std::set<MVertex*>::iterator itver = allVer.begin(); itver != allVer.end(); itver++){
      MVertex* vp = *itver;
      constraintElement* cel = new periodicMeshConstraint(_solver->getMicroBC(),_LagSpace,_MultSpace,compK,vp);
      _allConstraint.insert(cel);
    }
  }

	// periodic BC
	const std::vector<std::pair<int,int> >& periodicNegative = mixBC->getNegativePhysical();
	const std::vector<std::pair<int,int> >& periodicPositive = mixBC->getPositivePhysical();
  
	double negFact = mixBC->getNegativeFactor();
	for (int i=0; i< periodicNegative.size(); i++){
		int phyPositive = periodicPositive[i].first;
		int phyNegative = periodicNegative[i].first;
		int comp = periodicPositive[i].second;

		Msg::Info("periodic BC negative = %d positive = %d comp = %d",phyNegative,phyPositive,comp);

		elementGroup* gPositive = _solver->getMicroBC()->getBCGroup(phyPositive);
		elementGroup* gNegative = _solver->getMicroBC()->getBCGroup(phyNegative);
    
    std::set<MVertex*>& alreadyCon = directContraint[comp];
    Msg::Info("alreadyConSize = %d",alreadyCon.size());
    if (mixBC->getPBCMethod() == nonLinearPeriodicBC::CEM){
        
      periodicConditionForPeriodicMeshItMap(comp,negFact,gPositive->vbegin(), gPositive->vend(),
                    gNegative->vbegin(), gNegative->vend(),alreadyCon,NULL,NULL);
      
      addVertexToGroupItMap(alreadyCon,gPositive->vbegin(),gPositive->vend());
    }
    else if (mixBC->getPBCMethod() == nonLinearPeriodicBC::LIM){
      int degree = mixBC->getInterpolationDegree();
      bool addNewVer = mixBC->isAddNewVertex();
        
      std::vector<MVertex*> vc;
      this->getCornerVerticesForBoundaryPhysical(phyNegative,vc);
        
      if (mixBC->getDim() == 2){
        std::vector<MVertex*> lagBase;
        std::set<MVertex*> newVirVertices;
        InterpolationOperations::createLinePolynomialBaseItMap(degree,addNewVer,vc[0],vc[1],gNegative->vbegin(),gNegative->vend(),lagBase,newVirVertices);
        _virtualVertices.insert(newVirVertices.begin(),newVirVertices.end());
        // form condition
        this->lagrangeFormConditionItMap(gNegative->vbegin(),gNegative->vend(),lagBase,NULL,NULL,comp);
        this->lagrangePeriodicConditionItMap(gPositive->vbegin(),gPositive->vend(),lagBase,alreadyCon,NULL,NULL,comp);
        addVertexToGroupItMap(alreadyCon,gPositive->vbegin(),gPositive->vend());
      }
      else if (mixBC->getDim() == 3){
        std::vector<std::set<MVertex*>*> allLine;
        getLinesInFace(phyNegative,allLine);
        std::set<MVertex*>& lX = *allLine[0];
        std::set<MVertex*>& lY = *allLine[1];
        
        std::vector<MVertex*> lagBaseX, lagBaseY;
        
        std::set<MVertex*> newVirVertices;
        InterpolationOperations::createLinePolynomialBase(degree,addNewVer,vc[0],vc[1],lX.begin(),lX.end(),lagBaseX,newVirVertices);
        InterpolationOperations::createLinePolynomialBase(degree,addNewVer,vc[0],vc[3],lY.begin(),lY.end(),lagBaseY,newVirVertices);
        _virtualVertices.insert(newVirVertices.begin(),newVirVertices.end());
        
        alreadyCon.insert(lagBaseX.begin(),lagBaseX.end());
        alreadyCon.insert(lagBaseY.begin(),lagBaseY.end());
        
        this->lagrangeFormConditionCoonsPatchItMap(gNegative->vbegin(), gNegative->vend(),vc[0],lagBaseX,lagBaseY,alreadyCon,NULL,NULL,comp);
        addVertexToGroupItMap(alreadyCon,gNegative->vbegin(), gNegative->vend());
        this->lagrangePeriodicConditionCoonsPatchItMap(gPositive->vbegin(), gPositive->vend(),vc[0],lagBaseX,lagBaseY,alreadyCon,NULL,NULL,comp);
        addVertexToGroupItMap(alreadyCon,gPositive->vbegin(),gPositive->vend());
      }
    }
    else if (mixBC->getPBCMethod() == nonLinearPeriodicBC::PROJECT){
      this->pbcFEConstraintElementGroupItMap(gPositive->vbegin(), gPositive->vend(),gNegative,alreadyCon,NULL,NULL,comp);
      addVertexToGroupItMap(alreadyCon,gPositive->vbegin(),gPositive->vend());
    }
    else{
      Msg::Error("method %d has not been implemented in mixed BC for PBC parts");
    }
	}
  
  const std::vector<std::pair<int,int> >& sphysical = mixBC->getStaticPhysical();
	for (int i=0; i<sphysical.size(); i++){
		Msg::Info("s bc = %d comp = %d",sphysical[i].first,sphysical[i].second);
		elementGroup* gr = _solver->getMicroBC()->getBCGroup(sphysical[i].first);
		int compS = sphysical[i].second;

		constraintElement* ppc= new supplementConstraint(_solver->getMicroBC(),_LagSpace,_MultSpace,compS,gr);
		_allConstraint.insert(ppc);
	};

	if (_solver->getMicroBC()->getOrder() == 2){
		const std::vector<additionalSecondOrderElement>& hoCons = mixBC->getAdditionalSecondOrderStaticPhysical();
		for (int i=0; i< hoCons.size(); i++){
			const additionalSecondOrderElement& el = hoCons[i];
			Msg::Info("ho static bc = %d comp = %d direction = %d",el.physical,el.comp,el.direction);
			elementGroup* gr = _solver->getMicroBC()->getBCGroup(el.physical);

			singlePositionScalarWeightFunction fct(el.direction);
			constraintElement* ppc = new supplementConstraint(_solver->getMicroBC(), _LagSpace,_MultSpace,el.comp,gr, &fct);
			_allConstraint.insert(ppc);
		}

	}
  
  // average PBC
  const std::vector<std::pair<int,int> >& periodicStaticPositive = mixBC->getPositiveStaticPhysical();
  const std::vector<std::pair<int,int> >& periodicStaticNegative = mixBC->getNegativeStaticPhysical();
	for (int i=0; i< periodicStaticPositive.size(); i++){
		int phyPositive = periodicStaticPositive[i].first;
		int phyNegative = periodicStaticNegative[i].first;
		int compS = periodicStaticPositive[i].second;

		Msg::Info("average Periodic BC negative = %d positive = %d comp = %d",phyNegative,phyPositive,compS);

		elementGroup* gPositive = _solver->getMicroBC()->getBCGroup(phyPositive);
		elementGroup* gNegative = _solver->getMicroBC()->getBCGroup(phyNegative);

		constraintElement* ppc = new periodicSupplementConstraint(_solver->getMicroBC(),_LagSpace,_MultSpace,compS,gPositive,gNegative);
    _allConstraint.insert(ppc);

	}

};

void pbcConstraintElementGroup::createOrthogonalMixBCConstraintElementGroupDirectionFollowing(){
	nonLinearOrthogonalMixedBCDirectionFollowing* mixBC = dynamic_cast<nonLinearOrthogonalMixedBCDirectionFollowing*>(_solver->getMicroBC());
	const std::vector<SVector3>& kubcDir = mixBC->getKUBCDirection();
	const std::vector<SVector3>& subcDir = mixBC->getSUBCDirection();

	const std::vector<elementGroup*>& bgroup = _solver->getMicroBC()->getBoundaryElementGroup();
	std::set<MVertex*> allVertex;

	for (int i=0; i< bgroup.size(); i++){
		for (elementGroup::vertexContainer::const_iterator it = bgroup[i]->vbegin(); it!= bgroup[i]->vend(); it++){
			allVertex.insert(it->second);
		}
	}

	for (int i=0; i< kubcDir.size(); i++){
		const SVector3& dir = kubcDir[i];
		for (std::set<MVertex*>::iterator it = allVertex.begin(); it!= allVertex.end(); it++){
			MVertex* v = *it;
      constraintElement* ppc = new directionalPeriodicMeshConstraint(_solver->getMicroBC(), _LagSpace,_LagSpace,_MultSpace,v,NULL,dir);
      _allConstraint.insert(ppc);
		}
	}

	int halfOfgsize = bgroup.size()/2;
	for (int i=0; i< subcDir.size(); i++){
		const SVector3& dir = subcDir[i];
		for (int j=0; j< halfOfgsize; j++){
      elementGroup* gPlus = bgroup[j];
			elementGroup* gMinus = bgroup[j+halfOfgsize];
      if ((i == 0) and (j == 0)){
        constraintElement* ppc = new directionalSupplementConstraint(_solver->getMicroBC(), _LagSpace,_MultSpace,gPlus,dir);
        _allConstraint.insert(ppc);
        
        ppc = new directionalSupplementConstraint(_solver->getMicroBC(), _LagSpace,_MultSpace,gMinus,dir);
        _allConstraint.insert(ppc);
      }
      else{
        constraintElement* ppc = new directionalPBCSupplementConstraint(_solver->getMicroBC(), _LagSpace,_MultSpace,gPlus,gMinus,dir);
        _allConstraint.insert(ppc);
      }
		};
	}
  
};



void pbcConstraintElementGroup::createMixBCConstraintElementGroup_DG(){
  nonLinearMixedBC* mixBC = dynamic_cast<nonLinearMixedBC*>(_solver->getMicroBC());

	const std::vector<std::pair<int,int> >& kphysical = mixBC->getKinematicPhysical();
	const std::vector<std::pair<int,int> >& sphysical = mixBC->getStaticPhysical();

	std::set<Dof> allConstraintDof;
	for (int i=0; i<kphysical.size(); i++){
		Msg::Info("k bc = %d comp = %d",kphysical[i].first,kphysical[i].second);
		const elementGroup* grPhy = _solver->getMicroBC()->getBCGroup(kphysical[i].first);
		elementGroupDomainDecomposition* gPartGroup = NULL;
		for (int ig=0; ig< _boundaryDGMap.size(); ig++){
			if (_boundaryDGMap[ig]->physical == kphysical[i].first){
				gPartGroup = _boundaryDGMap[ig];
				break;
			}
		}
		if (gPartGroup == NULL){
			Msg::Error("elementGroupDomainDecomposition for group %d is not found in pbcConstraintElementGroup::createMixBCConstraintElementGroup_DG()",kphysical[i].first);
		}
		int compK = kphysical[i].second;
		std::vector<int> comp;
		comp.push_back(compK);

		for (std::map<partDomain*,elementGroup*>::iterator it = gPartGroup->decompositionMap.begin(); it != gPartGroup->decompositionMap.end(); it++){
			partDomain* dom = it->first;
			elementGroup* gr = it->second;
			FunctionSpaceBase* lagSpace = dom->getFunctionSpace();
			FunctionSpaceBase* multspace = _allDGMultSpace[dom];
			for (elementGroup::vertexContainer::const_iterator itgr = gr->vbegin(); itgr!= gr->vend(); itgr++){
				MVertex* v= itgr->second;
				std::vector<Dof> key;
				getKeysFromVertex(dom->getFunctionSpace(),v,comp,key);
				// check if Dof is already constrained
				if (allConstraintDof.find(key[0])== allConstraintDof.end()){
					allConstraintDof.insert(key[0]);
					// create constraint
					constraintElement* cel = new periodicMeshConstraint(_solver->getMicroBC(),lagSpace,multspace,compK,v);
					_allConstraint.insert(cel);
				}

			}
		}
	};

	for (int i=0; i<sphysical.size(); i++){
		Msg::Info("s bc = %d comp = %d",sphysical[i].first,sphysical[i].second);
		elementGroup* grPhy = _solver->getMicroBC()->getBCGroup(sphysical[i].first);
		elementGroupDomainDecomposition* gPartGroup = NULL;
		for (int ig=0; ig< _boundaryDGMap.size(); ig++){
			if (_boundaryDGMap[ig]->physical == sphysical[i].first){
				gPartGroup = _boundaryDGMap[ig];
				break;
			}
		}
		if (gPartGroup == NULL){
			Msg::Error("elementGroupDomainDecomposition for group %d is not found in pbcConstraintElementGroup::createMixBCConstraintElementGroup_DG()",sphysical[i].first);
		}
		int compS = sphysical[i].second;

		constraintElement* ppc= new supplementConstraintGeneral(_solver->getMicroBC(),_MultSpace,compS,gPartGroup);
		_allConstraint.insert(ppc);
	};

	if (_solver->getMicroBC()->getOrder() == 2){
		const std::vector<additionalSecondOrderElement>& hoCons = mixBC->getAdditionalSecondOrderStaticPhysical();
		for (int i=0; i< hoCons.size(); i++){
			const additionalSecondOrderElement& el = hoCons[i];
			Msg::Info("ho static bc = %d comp = %d direction = %d",el.physical,el.comp,el.direction);
			elementGroup* grPhy = _solver->getMicroBC()->getBCGroup(el.physical);
			elementGroupDomainDecomposition* gPartGroup = NULL;
			for (int ig=0; ig< _boundaryDGMap.size(); ig++){
				if (_boundaryDGMap[ig]->physical == el.physical){
					gPartGroup = _boundaryDGMap[ig];
					break;
				}
			}
			if (gPartGroup == NULL){
				Msg::Error("elementGroupDomainDecomposition for group %d is not found in pbcConstraintElementGroup::createMixBCConstraintElementGroup_DG()",sphysical[i].first);
			}

			singlePositionScalarWeightFunction fct(el.direction);
			constraintElement* ppc = new supplementConstraintGeneral(_solver->getMicroBC(),_MultSpace,el.comp,gPartGroup,&fct);
			_allConstraint.insert(ppc);
		}

	}

	if (mixBC->getPositivePhysical().size() > 0){
		Msg::Error("periodic BC on components is not considered ");
	}
};

void pbcConstraintElementGroup::createCornerConstraintElementGroupShiftedBPC(){
	if (_solver->getMicroBC()->getOrder() == 2){
		Msg::Error("shifted BC is implemented for first order BC only");
	}
  
  nonLinearShiftedPeriodicBC* spbc = static_cast<nonLinearShiftedPeriodicBC*>(_solver->getMicroBC());

  if (spbc->getPBCMethod() == nonLinearPeriodicBC::LIM){
    if (_cVertices.size() > 0){
      constraintElement* cel = new periodicMeshConstraint(_solver->getMicroBC() ,_LagSpace,_MultSpace,-1,_v1);
      _allConstraint.insert(cel);
    }
  }
  else{
    Msg::Error("this method is not implemented to impose shiftedPBC");
  }
  
  
}

void pbcConstraintElementGroup::createCornerConstraintElementGroup(){
  nonLinearPeriodicBC* pbc = static_cast<nonLinearPeriodicBC*>(_solver->getMicroBC());
  int ndofsPerNode = _solver->getMicroBC()->getNumberOfConstrainedDofsPerVertex();
  std::vector<int> mechanics(_solver->getMicroBC()->getMechanicsConstrainedComps().begin(), _solver->getMicroBC()->getMechanicsConstrainedComps().end()); // mechanic Dof only
  std::vector<int> compAddition(_solver->getMicroBC()->getAdditionalConstrainedComps().begin(), _solver->getMicroBC()->getAdditionalConstrainedComps().end());
  std::vector<int> compConstitutive(_solver->getMicroBC()->getConstitutiveExtraConstrainedComps().begin(), _solver->getMicroBC()->getConstitutiveExtraConstrainedComps().end()); // constitutuive extraDofs
  std::vector<int> compNotConstitutive(_solver->getMicroBC()->getNonConstitutiveExtraConstrainedComps().begin(), _solver->getMicroBC()->getNonConstitutiveExtraConstrainedComps().end()); // non-constitutive extraDofs
  
  // for mechanical Dofs
  std::set<MVertex*> activeCornerVertices;
  for (std::set<MVertex*>::iterator it = _cVertices.begin(); it!= _cVertices.end(); it++){
    MVertex* vp1 = *it;
    if (!isVirtual(vp1) or (isVirtual(vp1) and isActive(vp1))){
      activeCornerVertices.insert(vp1);
    }
  }
  if (activeCornerVertices.size() == 0){
    Msg::Info("all corner vertices are not active");
  }
  
  if (compAddition.size() > 0)
  {
    MVertex* vn = *(activeCornerVertices.begin());
    for(std::set<MVertex*>::iterator it = activeCornerVertices.begin(); it!= activeCornerVertices.end(); it++){
      if (it != activeCornerVertices.begin())
      {
        MVertex* vp1 = *it;
        for (int i=0; i< compAddition.size(); i++){
          Msg::Info("constrained addtional Dof at corner %d as positive %d as negative, number of addDofs=%d",vp1->getNum(),vn->getNum(),compAddition[i]);
          constraintElement* cel = new periodicMeshConstraint(_solver->getMicroBC(),_LagSpace,_MultSpace,compAddition[i], vp1,vn);
          _allConstraint.insert(cel);
        }
      }
    }
  }

  
  const std::vector<elementGroup*>& bgroup = _solver->getMicroBC()->getBoundaryElementGroup();
  if  (pbc->getPBCMethod() == nonLinearPeriodicBC::CEM or pbc->getPBCMethod() == nonLinearPeriodicBC::PROJECT){
    if (_solver->getMicroBC()->getOrder() == 1){
      // fix all corners mechanical Dofs
      if (activeCornerVertices.size() > 0){
        for (std::set<MVertex*>::iterator it = activeCornerVertices.begin(); it!= activeCornerVertices.end(); it++){
          MVertex* vp1 = *it;
          for (int i=0; i< mechanics.size(); i++){
            constraintElement* cel = new periodicMeshConstraint(_solver->getMicroBC() ,_LagSpace,_MultSpace,mechanics[i],vp1);
            _allConstraint.insert(cel);
          }
        }          
      }
      else{
        // fix an arbitrary point
        MVertex* v = (bgroup[0]->vbegin()->second);
        for (int i=0; i< mechanics.size(); i++){
          constraintElement* cel = new periodicMeshConstraint(_solver->getMicroBC() ,_LagSpace,_MultSpace,mechanics[i],v);
          _allConstraint.insert(cel);
        }
      }
    }
    else if (_solver->getMicroBC()->getOrder() == 2){
      // periodic between
      if (activeCornerVertices.size() >0){
        MVertex* vn = *(activeCornerVertices.begin());
        for(std::set<MVertex*>::iterator it = activeCornerVertices.begin(); it!= activeCornerVertices.end(); it++){
          if (it != activeCornerVertices.begin())
          {
            MVertex* vp1 = *it;
            for (int i=0; i< mechanics.size(); i++){
              constraintElement* cel = new periodicMeshConstraint(_solver->getMicroBC(),_LagSpace,_MultSpace,mechanics[i], vp1,vn);
              _allConstraint.insert(cel);
            }
          }
        }
      }

      for (int i=0; i<bgroup.size()/2; i++){
        elementGroup* g = bgroup[i];
        for (int k=0; k< mechanics.size(); k++){
          constraintElement* ppc = new supplementConstraint(_solver->getMicroBC(),_LagSpace,_MultSpace,mechanics[k],g);
          _allConstraint.insert(ppc);
        }
      }
    }

    // for constitutive extraDofs
    if (activeCornerVertices.size() > 0){
      MVertex* oneV = *(activeCornerVertices.begin());
      for (std::set<MVertex*>::iterator it = activeCornerVertices.begin(); it!= activeCornerVertices.end(); it++){
        MVertex* vp1 = *it;
        if (vp1 != oneV){
          for (int i=0; i< compConstitutive.size(); i++){
            constraintElement* cel = new periodicMeshConstraint(_solver->getMicroBC(),_LagSpace,_MultSpace,compConstitutive[i],vp1,oneV);
            _allConstraint.insert(cel);
          }
        }
      }
    }
    for (int i=0; i< compConstitutive.size(); i++){
      constraintElement* celExtra = new supplementConstraintAverageExtraDofValue(_solver->getMicroBC() ,_MultSpace,compConstitutive[i],_solver->getDomainVector());
      _allConstraint.insert(celExtra);
    }

    // for non-constitutive extraDofs
    if (activeCornerVertices.size() >0){
      for (std::set<MVertex*>::iterator it = activeCornerVertices.begin(); it!= activeCornerVertices.end(); it++){
        MVertex* vp1 = *it;
        for (int i=0; i< compNotConstitutive.size(); i++){
          constraintElement* cel = new periodicMeshConstraint(_solver->getMicroBC() ,_LagSpace,_MultSpace,compNotConstitutive[i],vp1);
          _allConstraint.insert(cel);
        }
      }        
    }
    else{
      MVertex* v = (bgroup[0]->vbegin()->second);
      for (int i=0; i< compNotConstitutive.size(); i++){
        constraintElement* cel = new periodicMeshConstraint(_solver->getMicroBC() ,_LagSpace,_MultSpace,compNotConstitutive[i],v);
        _allConstraint.insert(cel);
      }
      
    }
  }
  else if (pbc->getPBCMethod() == nonLinearPeriodicBC::CSIM){
    // for mechanics
    if (_solver->getMicroBC()->getOrder() == 1){
      for (std::set<MVertex*>::iterator it = _cVertices.begin(); it!= _cVertices.end(); it++){
        MVertex* vp1 = *it;
        for (int i=0; i< mechanics.size(); i++){
          constraintElement* cel = new periodicMeshConstraint(_solver->getMicroBC() ,_LagSpace,_MultSpace,mechanics[i],vp1);
          _allConstraint.insert(cel);
        }
      }
    }
    else if (_solver->getMicroBC()->getOrder() == 2){
      for(std::set<MVertex*>::iterator it = _cVertices.begin(); it!= _cVertices.end(); it++){
        MVertex* v = *it;
        if ( v != _v1){
          for (int i=0; i< mechanics.size(); i++){
            constraintElement* cel = new periodicMeshConstraint(_solver->getMicroBC() ,_LagSpace,_MultSpace,mechanics[i],v,_v1);
            _allConstraint.insert(cel);
          }
        }
      }


      if (_solver->getMicroBC()->getDim() ==2){
        for (int i=0; i< mechanics.size(); i++){
          constraintElement* ppc = new cubicSplineSupplementConstraint(_solver->getMicroBC(),_LagSpace,_MultSpace,mechanics[i],_xBase,0);
          _allConstraint.insert(ppc);

          ppc = new cubicSplineSupplementConstraint(_solver->getMicroBC(),_LagSpace,_MultSpace,mechanics[i],_yBase,1);
          _allConstraint.insert(ppc);
        }
      }
      else if (_solver->getMicroBC()->getDim() == 3){
        for (int i=0; i<bgroup.size()/2; i++){
          elementGroup* g = bgroup[i];
          for (int k=0; k< mechanics.size(); k++){
            constraintElement* ppc = new supplementConstraint(_solver->getMicroBC(),_LagSpace,_MultSpace,mechanics[k],g);
            _allConstraint.insert(ppc);
          }
        }
      };
    };

    //
    // for constitutive extraDofs
    for (std::set<MVertex*>::iterator it = _cVertices.begin(); it!= _cVertices.end(); it++){
      MVertex* vp1 = *it;
      if (vp1 != _v1){
        for (int i=0; i< compConstitutive.size(); i++){
          constraintElement* cel = new periodicMeshConstraint(_solver->getMicroBC(),_LagSpace,_MultSpace,compConstitutive[i],vp1,_v1);
          _allConstraint.insert(cel);
        }
      }
    }
    for (int i=0; i< compConstitutive.size(); i++){
      constraintElement* celExtra = new supplementConstraintAverageExtraDofValue(_solver->getMicroBC() ,_MultSpace,
                              compConstitutive[i], _solver->getDomainVector());
      _allConstraint.insert(celExtra);
    }

    // for non-constitutive extraDofs
    for (std::set<MVertex*>::iterator it = _cVertices.begin(); it!= _cVertices.end(); it++){
      MVertex* vp1 = *it;
      for (int i=0; i< compNotConstitutive.size(); i++){
        constraintElement* cel = new periodicMeshConstraint(_solver->getMicroBC() ,_LagSpace,_MultSpace,compNotConstitutive[i],vp1);
        _allConstraint.insert(cel);
      }

    }
  }
  else if (pbc->getPBCMethod() == nonLinearPeriodicBC::LIM){
    // for mechanics
    if (_solver->getMicroBC()->getOrder() == 1){
      for (std::set<MVertex*>::iterator it = _cVertices.begin(); it!= _cVertices.end(); it++){
        MVertex* vp1 = *it;
        for (int i=0; i< mechanics.size(); i++){
          constraintElement* cel = new periodicMeshConstraint(_solver->getMicroBC() ,_LagSpace,_MultSpace,mechanics[i],vp1);
          _allConstraint.insert(cel);
        }
      }
    }
    else if (_solver->getMicroBC()->getOrder() == 2){
      if (pbc->getMaxDegree()>1){
        for(std::set<MVertex*>::iterator it = _cVertices.begin(); it!= _cVertices.end(); it++){
          MVertex* v = *it;
          if ( v != _v1){
            for (int i=0; i< mechanics.size(); i++){
              constraintElement* cel = new periodicMeshConstraint(_solver->getMicroBC() ,_LagSpace,_MultSpace,mechanics[i],v,_v1);
              _allConstraint.insert(cel);
            }
          }
        }

        //
        for (int k=0; k< mechanics.size(); k++){
          if (_solver->getMicroBC()->getDim() ==2){
            constraintElement* ppc = new lagrangeSupplementConstraint(_solver->getMicroBC(), _LagSpace,_MultSpace,mechanics[k],_xBase);
            _allConstraint.insert(ppc);
            ppc = new lagrangeSupplementConstraint(_solver->getMicroBC(), _LagSpace,_MultSpace,mechanics[k],_yBase);
            _allConstraint.insert(ppc);
          }
          else if (_solver->getMicroBC()->getDim() ==3){
            std::vector<lagrangeSupplementConstraint*> lconstraint;
            lagrangeSupplementConstraint* con = new lagrangeSupplementConstraint(_solver->getMicroBC(), _LagSpace,_MultSpace,mechanics[k],_xBase);
            lconstraint.push_back(con);
            con = new lagrangeSupplementConstraint(_solver->getMicroBC(),_LagSpace,_MultSpace,mechanics[k],_yBase);
            lconstraint.push_back(con);
            con = new lagrangeSupplementConstraint(_solver->getMicroBC(),_LagSpace,_MultSpace,mechanics[k],_zBase);
            lconstraint.push_back(con);
            //
            constraintElement* ppc = new CoonsPatchLagrangeSupplementConstraint(_solver->getMicroBC(),_LagSpace,_MultSpace,mechanics[k],lconstraint[0],lconstraint[1]);
            _allConstraint.insert(ppc);
            ppc = new CoonsPatchLagrangeSupplementConstraint(_solver->getMicroBC(), _LagSpace,_MultSpace,mechanics[k],lconstraint[1],lconstraint[2]);
            _allConstraint.insert(ppc);
            ppc = new CoonsPatchLagrangeSupplementConstraint(_solver->getMicroBC(), _LagSpace,_MultSpace,mechanics[k],lconstraint[0],lconstraint[2]);
            _allConstraint.insert(ppc);
          }

        }
      }
      else if (pbc->getMaxDegree() == 1) {
        for (std::set<MVertex*>::iterator it = _cVertices.begin(); it!= _cVertices.end(); it++){
          MVertex* vp1 = *it;
          for (int i=0; i< mechanics.size(); i++){
            constraintElement* cel = new periodicMeshConstraint(_solver->getMicroBC(),_LagSpace,_MultSpace,mechanics[i],vp1);
            _allConstraint.insert(cel);
          }
        }
      }

    }

    // for constitutive extraDofs
    for (std::set<MVertex*>::iterator it = _cVertices.begin(); it!= _cVertices.end(); it++){
      MVertex* vp1 = *it;
      if (vp1 != _v1){
        for (int i=0; i< compConstitutive.size(); i++){
          constraintElement* cel = new periodicMeshConstraint(_solver->getMicroBC(),_LagSpace,_MultSpace,compConstitutive[i],vp1,_v1);
          _allConstraint.insert(cel);
        }
      }
    }
    for (int i=0; i< compConstitutive.size(); i++){
      constraintElement* celExtra = new supplementConstraintAverageExtraDofValue(_solver->getMicroBC() ,_MultSpace,
                              compConstitutive[i], _solver->getDomainVector());
      _allConstraint.insert(celExtra);
    }

    // for non-constitutive extraDofs
    for (std::set<MVertex*>::iterator it = _cVertices.begin(); it!= _cVertices.end(); it++){
      MVertex* vp1 = *it;
      for (int i=0; i< compNotConstitutive.size(); i++){
        constraintElement* cel = new periodicMeshConstraint(_solver->getMicroBC() ,_LagSpace,_MultSpace,compNotConstitutive[i],vp1);
        _allConstraint.insert(cel);
      }
    }

  }
  else if (pbc->getPBCMethod() == nonLinearPeriodicBC::FE_LIN or  
           pbc->getPBCMethod() == nonLinearPeriodicBC::FE_QUA or 
           pbc->getPBCMethod() == nonLinearPeriodicBC::FE_HIGH_ORDER){

    std::set<MVertex*> activeCornerVertices;
    // all vertual vertices and not appears in any constraint element do not neen to take into account
    for (std::set<MVertex*>::iterator it = _cVertices.begin(); it!= _cVertices.end(); it++){
      MVertex* vp1 = *it;
      if (!isVirtual(vp1) or (isVirtual(vp1) and isActive(vp1))){
        activeCornerVertices.insert(vp1);
      }
    }
    
    MVertex* vertexToFix = NULL;
    
    if (activeCornerVertices.size() == 0){
      // need to fix some vertices to avoid rigid body mode
      Msg::Info("all corner vertices are virtual and not active");
      for (elementGroup::vertexContainer::const_iterator itg = _gX->vbegin(); itg!= _gX->vend(); itg++){
        MVertex* vg = itg->second;
        if (this->isActive(vg)){
         vertexToFix = vg;
          break;
        }
      }
    }

    // for mechanics
    if (_solver->getMicroBC()->getOrder() == 1){
      if (activeCornerVertices.size() > 0){
        for (std::set<MVertex*>::iterator it = activeCornerVertices.begin(); it!= activeCornerVertices.end(); it++){
          MVertex* vp1 = *it;
          for (int i=0; i< mechanics.size(); i++){
            constraintElement* cel = new periodicMeshConstraint(_solver->getMicroBC() ,_LagSpace,_MultSpace,mechanics[i],vp1);
            _allConstraint.insert(cel);
          }
        }          
      }
      else{
        for (int i=0; i< mechanics.size(); i++){
          constraintElement* cel = new periodicMeshConstraint(_solver->getMicroBC() ,_LagSpace,_MultSpace,mechanics[i],vertexToFix);
          _allConstraint.insert(cel);
        }
      }

    }
    else if (_solver->getMicroBC()->getOrder() == 2){
      if (activeCornerVertices.size() >0){
        MVertex* vn = *(activeCornerVertices.begin());
        for(std::set<MVertex*>::iterator it = activeCornerVertices.begin(); it!= activeCornerVertices.end(); it++){
          if (it != activeCornerVertices.begin())
          {
            MVertex* vp1 = *it;
            for (int i=0; i< mechanics.size(); i++){
              constraintElement* cel = new periodicMeshConstraint(_solver->getMicroBC(),_LagSpace,_MultSpace,mechanics[i],vp1,vn);
              _allConstraint.insert(cel);

            }
          }
        }
      }

      for (int i=0; i< mechanics.size(); i++){
        if (_solver->getMicroBC()->getDim()== 2){
          constraintElement* ppc = new supplementConstraint(_solver->getMicroBC(),_LagSpace,_MultSpace,mechanics[i],_gX);
          _allConstraint.insert(ppc);
          ppc = new supplementConstraint(_solver->getMicroBC(),_LagSpace,_MultSpace,mechanics[i],_gY);
          _allConstraint.insert(ppc);
        }
        else if (_solver->getMicroBC()->getDim() == 3){
          constraintElement* ppc = new supplementConstraint(_solver->getMicroBC(),_LagSpace,_MultSpace,mechanics[i],_gXY);
          _allConstraint.insert(ppc);
          ppc = new supplementConstraint(_solver->getMicroBC(),_LagSpace,_MultSpace,mechanics[i],_gYZ);
          _allConstraint.insert(ppc);
          ppc = new supplementConstraint(_solver->getMicroBC(),_LagSpace,_MultSpace,mechanics[i],_gZX);
          _allConstraint.insert(ppc);
        }
      }
    }

    // for constitutive extraDofs
    if (activeCornerVertices.size() > 0){
      MVertex* vn1 = *(activeCornerVertices.begin());
      for (std::set<MVertex*>::iterator it = activeCornerVertices.begin(); it!= activeCornerVertices.end(); it++){
        MVertex* vp1 = *it;
        if (vp1 != vn1){
          for (int i=0; i< compConstitutive.size(); i++){
            constraintElement* cel = new periodicMeshConstraint(_solver->getMicroBC(),_LagSpace,_MultSpace,compConstitutive[i],vp1,vn1);
            _allConstraint.insert(cel);
          }
        }
      }
    }
    for (int i=0; i< compConstitutive.size(); i++){
      constraintElement* celExtra = new supplementConstraintAverageExtraDofValue(_solver->getMicroBC() ,_MultSpace,
                              compConstitutive[i], _solver->getDomainVector());
      _allConstraint.insert(celExtra);
    }

    // for non-constitutive extraDofs
    if (activeCornerVertices.size() > 0){
      for (int i=0; i< compNotConstitutive.size(); i++){
        for (std::set<MVertex*>::iterator it = activeCornerVertices.begin(); it!= activeCornerVertices.end(); it++){
          MVertex* vp1 = *it;
          constraintElement* cel = new periodicMeshConstraint(_solver->getMicroBC() ,_LagSpace,_MultSpace,compNotConstitutive[i],vp1);
          _allConstraint.insert(cel);
        }

      }        
    }
    else{
      for (int i=0; i< compNotConstitutive.size(); i++){
        constraintElement* cel = new periodicMeshConstraint(_solver->getMicroBC() ,_LagSpace,_MultSpace,compNotConstitutive[i],vertexToFix);
        _allConstraint.insert(cel);

      }    
    }

  }
  else
     Msg::Error("pbcConstraintElementGroup::createCornerConstraintElementGroup: case undefined");

};

void pbcConstraintElementGroup::createPBCConstraintGroup(){
  nonLinearPeriodicBC* pbc = static_cast<nonLinearPeriodicBC*>(_solver->getMicroBC());
			
  if  (pbc->getPBCMethod() == nonLinearPeriodicBC::CEM){
  Msg::Info("Imposing PBC by periodic mesh formulation");
  this->createPeriodicMeshConstraintElementGroup();
  }
  else if (pbc->getPBCMethod() == nonLinearPeriodicBC::CSIM){
    Msg::Info("Imposing PBC by cubic spline formulation");
    this->createCubicSplineConstraintElementGroup();
  }
  else if (pbc->getPBCMethod() == nonLinearPeriodicBC::LIM){
    Msg::Info("Imposing PBC by lagrange formulation");
    if (pbc->getMaxDegree() >1){
      this->createLagrangeConstraintElementGroup();
    }
    else{
      createLinearDisplacementConstraintElementGroup();
    }
  }
  else if (pbc->getPBCMethod() == nonLinearPeriodicBC::FE_LIN or  
           pbc->getPBCMethod() == nonLinearPeriodicBC::FE_QUA) {
    Msg::Info("Imposing PBC by lagrange C0");
    this->createFEConstraintElementGroup();
  }
  else if (pbc->getPBCMethod() == nonLinearPeriodicBC::FE_HIGH_ORDER)
  {
    Msg::Info("Imposing PBC by lagrange C_inf by one high order element ");
    this->createFEConstraintElementGroup();
  }
  else if (pbc->getPBCMethod() == nonLinearPeriodicBC::PROJECT){
    Msg::Info("Imposing PBC by projection method");
    this->createProjectConstraintElementGroup();
  }
  else{
    Msg::Error("this method is not implemented to impose PBC");
  }
  this->createCornerConstraintElementGroup();
};

void pbcConstraintElementGroup::createGeneralPBCConstraintGroup(){
  nonLinearGeneralPeriodicBC* gpbc = static_cast<nonLinearGeneralPeriodicBC*>(_solver->getMicroBC());
  const SVector3& pbcNormal = gpbc->getPBCNormal();
  bool considerUsualPBC = false;
  if (pbcNormal.norm() < 1e-10){
    printf("because pbc vector is null, usual PBC is used\n");
    considerUsualPBC = true;
  }
  
  if (!considerUsualPBC){
    SPoint3 p1 = _v1->point();
		SPoint3 p2 = _v2->point();
		SPoint3 p3 = _v3->point();
    SPoint3 p4 = _v4->point();
    
    SVector3 p1p2(p1,p2);
    SVector3 p1p4(p1,p4);
    
    if ( (fabs(dot(p1p2,pbcNormal))< 1e-6) or (fabs(dot(p1p4,pbcNormal))< 1e-6)){
      considerUsualPBC = true;
      printf("because pbc vector coincide with boundary normal, usual PBC is used\n");
    }
  }
  
  
  if (considerUsualPBC){
   this->createPBCConstraintGroup();
  }
  else{
    printf("general PBC is used with Lagrange interpolation method\n");
    gpbc->setPBCMethod(nonLinearPeriodicBC::LIM);
    
    if (gpbc->getPBCMethod() == nonLinearPeriodicBC::LIM){
      Msg::Info("Imposing PBC by lagrange formulation");
      this->createGeneralPBCLagrangeConstraintElementGroup();
    }
    else{
      Msg::Error("this method is not implemented to impose shiftedPBC");
    };
  }
  
};

void pbcConstraintElementGroup::createShiftedPBCConstraintGroup(){
  nonLinearShiftedPeriodicBC* spbc = static_cast<nonLinearShiftedPeriodicBC*>(_solver->getMicroBC());
  const SVector3& normal = spbc->getShiftPBCNormal();
  if (normal.norm() < 1e-10){
    printf("because shifted vector is null, usual PBC is used\n");
    this->createPBCConstraintGroup();
  }
  else{
    // 
    printf("PBC is shifted using Lagrange interpolation method\n");
    spbc->setPBCMethod(nonLinearPeriodicBC::LIM);
    
    if (spbc->getPBCMethod() == nonLinearPeriodicBC::LIM){
      Msg::Info("Imposing PBC by lagrange formulation");
      this->createShiftedLagrangeConstraintElementGroup();
    }
    else{
      Msg::Error("this method is not implemented to impose shiftedPBC");
    };
      
    this->createCornerConstraintElementGroupShiftedBPC();
  }
};



void pbcConstraintElementGroup::createConstraintGroup(){
  constraintElement::allPositiveDof.clear();
	if (_fullDG){
		if (_solver->getMicroBC()->getType()==nonLinearMicroBC::LDBC){
			Msg::Info("Imposing linear displacement BC");
			this->createLinearDisplacementConstraintElementGroup_DG();
		}
		else if (_solver->getMicroBC()->getType()==nonLinearMicroBC::MKBC){
			Msg::Info("Imposing minimal kinematical BC");
			this->createMinimalKinematicConstraintElementGroup_DG();
		}
		else if (_solver->getMicroBC()->getType() == nonLinearMicroBC::MIXBC){
			Msg::Info("imposing mixed BC");
			this->createMixBCConstraintElementGroup_DG();
		}
    else if (_solver->getMicroBC()->getType() == nonLinearMicroBC::PBC)
    {
      Msg::Warning("****THE USE OF PBC in case of CG domains and their interfaces IS CORRECT if the RVE boundary belongs to the first domain !!!!****");
			this->createPBCConstraintGroup();
		}
		else{
			Msg::Error("this boundary condition type is not implemented");
      Msg::Exit(0);
		};
	}
	else{
		if (_solver->getMicroBC()->getType() == nonLinearMicroBC::PBC){
			this->createPBCConstraintGroup();
		}
    else if (_solver->getMicroBC()->getType() == nonLinearMicroBC::ShiftedPBC){
      this->createShiftedPBCConstraintGroup();
    }
    else if (_solver->getMicroBC()->getType() == nonLinearMicroBC::GeneralPBC){
      this->createGeneralPBCConstraintGroup();
    }
		else if (_solver->getMicroBC()->getType()==nonLinearMicroBC::LDBC){
			Msg::Info("Imposing linear displacement BC");
			this->createLinearDisplacementConstraintElementGroup();
		}
		else if (_solver->getMicroBC()->getType()==nonLinearMicroBC::MKBC){
			Msg::Info("Imposing minimal kinematical BC");
			this->createMinimalKinematicConstraintElementGroup();
		}
		else if (_solver->getMicroBC()->getType() == nonLinearMicroBC::MIXBC){
			Msg::Info("imposing mixed BC");
			this->createMixBCConstraintElementGroup();
		}
		else if (_solver->getMicroBC()->getType() == nonLinearMicroBC::OrthogonalDirectionalMixedBC){
			this->createOrthogonalMixBCConstraintElementGroupDirectionFollowing();
		}
		else{
			Msg::Error("this boundary condition type is not implemented");
		};
	}
  constraintElement::allPositiveDof.clear();
};

void pbcConstraintElementGroup::clearAllConstraints(){
	_allConstraint.clear();
	_virtualVertices.clear();
};


void pbcConstraintElementGroup::createPBCFromFile(FILE* fp, GModel* pModel){
  if (fp == NULL) return;
  while (1){
    if(feof(fp))
      break;
    char what[256];
    if(fscanf(fp, "%s", what) != 1) return;
    if (!strcmp(what, "PeriodicNodes")){
      int numver;
      int ok = fscanf(fp, "%d", &numver);
      for (int i=0; i<numver; i++){
        int num;
        if (fscanf(fp, "%d", &num)){
          MVertex* v = pModel->getMeshVertexByTag(num);
          Msg::Error("periodic node: %d",v->getNum());
					constraintElement* cel = new periodicMeshConstraint(_solver->getMicroBC(),_LagSpace,_MultSpace,-1,v);
					_allConstraint.insert(cel);
        }
      }
    }
    else if (!strcmp(what, "MatchingNodes")){
      int nummat;
      int ok = fscanf(fp, "%d", &nummat);
      for (int i=0; i<nummat; i++){
        int p, n;
        if (fscanf(fp,"%d %d",&p,&n)){
				MVertex* vp = pModel->getMeshVertexByTag(p);
				MVertex* vn = pModel->getMeshVertexByTag(n);

				Msg::Error("matching node: %d %d", vp->getNum(),vn->getNum());
					constraintElement* cel = new periodicMeshConstraint(_solver->getMicroBC(),_LagSpace,_MultSpace,-1,vp,vn);
					_allConstraint.insert(cel);

        }
      }
    }
  }
};

SVector3 pbcConstraintElementGroup::getUniformDisp(MVertex* v){
  const STensor3& FI = _solver->getMicroBC()->getFirstOrderKinematicalVariable();
  const STensor33& G = _solver->getMicroBC()->getSecondOrderKinematicalVariable();
  SVector3 u(0.,0.,0.);
  SPoint3 p = v->point();
  for (int i=0; i<3; i++){
    for (int j=0; j<3; j++){
      u[i]+= FI(i,j)*p[j];
      if (_solver->getMicroBC()->getOrder() == 2){
        for (int k=0; k<3; k++){
          u[i] += 0.5*G(i,j,k)*p[j]*p[k];
        }
      }
    }
  }
  return u;
};
void pbcConstraintElementGroup::cubicSplineToFile(std::vector<MVertex*>& vlist, nlsDofManager* pmanager, FILE* file, const int dir){
  if (vlist.size() ==0) return;
  int N= 100;
  for (int i=0; i<vlist.size()-1; i++){
    MVertex* vinit = vlist[i];
    MVertex* vend = vlist[i+1];

    double L = vend->distance(vinit);
    SVector3 pertInit = this->getPerturbation(pmanager,vinit);
    SVector3 pertEnd = this->getPerturbation(pmanager,vend);

    SVector3 tInit = this->getTangentCubicSpline(pmanager,vinit,dir);
    SVector3 tEnd = this->getTangentCubicSpline(pmanager,vend,dir);

    fprintf(file,"%e %e %e %e %e %e \n",vinit->x(),vinit->y(),vinit->z(),pertInit[0],pertInit[1],pertInit[2]);
    SPoint3 pinit = vinit->point();
    SPoint3 pend = vend->point();

    SPoint3 dp = pend-pinit;
    dp /=(double(N-1));
    for (int j=1; j<N-1; j++){
      SPoint3 P = pinit+ dp*j;
      double l = P.distance(pinit);
      fullVector<double> FF;
      cubicSplineConstraintElement::getFF(l,0,L,FF);
      SVector3 val = pertInit*FF(0)+tInit*FF(1)+pertEnd*FF(2)+tEnd*FF(3);
      fprintf(file,"%e %e %e %e %e %e \n",P.x(),P.y(),P.z(),val[0],val[1],val[2]);
    }
    if (i == vlist.size()-2){
       fprintf(file,"%e %e %e %e %e %e \n",vend->x(),vend->y(),vend->z(),pertEnd[0],pertEnd[1],pertEnd[2]);
    }
  }
};

void pbcConstraintElementGroup::lagrangeToFile(std::vector<MVertex*>& vlist, nlsDofManager* pmanager, FILE* file){
  if (vlist.size() ==0) return;
  int N = 100;
  std::vector<double> base;
  std::vector<SVector3> pert;
  for (int i=0; i<vlist.size(); i++){
    base.push_back(vlist[i]->distance(vlist[0]));
    pert.push_back(this->getPerturbation(pmanager,vlist[i]));
  }

  SPoint3 p0 = vlist[0]->point();

  for (int i=0; i<vlist.size()-1; i++){
    MVertex* vinit = vlist[i];
    MVertex* vend = vlist[i+1];
    double length = vinit->distance(vlist[0]);
    fprintf(file,"%e %e %e %e %e %e \n",vinit->x(),vinit->y(),vinit->z(),pert[i][0],pert[i][1],pert[i][2]);

    SPoint3 pinit = vinit->point();
    SPoint3 pend = vend->point();
    SPoint3 dp = pend-pinit;
    dp /=(double(N-1));
    for (int j=1; j<N-1; j++){
      SPoint3 P = pinit+ dp*j;
      length = P.distance(p0);
      fullVector<double> FF;
      lagrangeConstraintElement::getFF(length,base,FF);
      SVector3 val(0.,0.,0.);
      for (int k=0; k<FF.size(); k++){
        val +=  pert[k]*FF(k);
      }
      fprintf(file,"%e %e %e %e %e %e \n",P.x(),P.y(),P.z(),val[0],val[1],val[2]);
    }
    if (i == vlist.size()-2){
       fprintf(file,"%e %e %e %e %e %e \n",vend->x(),vend->y(),vend->z(),pert[i+1][0],pert[i+1][1],pert[i+1][2]);
    }
  }
};

SVector3 pbcConstraintElementGroup::getPerturbation(nlsDofManager* pmanager, MVertex* v){
  SVector3 ubar = this->getUniformDisp(v);
  std::vector<Dof> keys;
  std::vector<double> val;
	std::vector<int> constrainedComp(_solver->getMicroBC()->getConstrainedComps().begin(), _solver->getMicroBC()->getConstrainedComps().end());
  getKeysFromVertex(_LagSpace,v,constrainedComp,keys);
  pmanager->getDofValue(keys,val);
  ubar *= -1.;
  for (int i=0; i<keys.size(); i++){
    ubar[i] +=  val[i];
  }
  return ubar;
};

SVector3 pbcConstraintElementGroup::getTangentCubicSpline(nlsDofManager* pmanager, MVertex* v, const int dir){
  SVector3 vec(0.,0.,0.);
	std::vector<int> constrainedComp(_solver->getMicroBC()->getConstrainedComps().begin(), _solver->getMicroBC()->getConstrainedComps().end());
  if (_solver->getMicroBC()->getType() != nonLinearMicroBC::PBC) {
    Msg::Warning("this used for periodic BC and cubic spline method");
  }
  else {
    nonLinearPeriodicBC* pbc = dynamic_cast<nonLinearPeriodicBC*>(_solver->getMicroBC());
    if (pbc->getPBCMethod() == nonLinearPeriodicBC::CSIM){
      std::vector<Dof> dofs;
      std::vector<double> vals;
      cubicSplineConstraintElement::addKeysToVertex(_LagSpace,v,constrainedComp,dofs,dir);
      pmanager->getDofValue(dofs,vals);
      for (int i=0; i<vals.size(); i++){
        vec[i] += vals[i];
      }
    }
  }
  return vec;
};



void pbcConstraintElementGroup::writePertBoundaryToFile(const int iter ){
  nlsDofManager* pmanager = _solver->getDofManager();
  if (_solver->getMicroBC()->getType() == nonLinearMicroBC::PBC){
    nonLinearPeriodicBC* pbc = dynamic_cast<nonLinearPeriodicBC*>(_solver->getMicroBC());
    std::string filename = _solver->getFileSavingPrefix()+"_xInterpolation_iter_"+int2str(iter)+ ".csv";
    FILE* file = fopen(filename.c_str(),"w");
    if (pbc->getPBCMethod() == nonLinearPeriodicBC::CSIM)
      cubicSplineToFile(_xBase,pmanager,file,0);
    else
      lagrangeToFile(_xBase,pmanager,file);
    fclose(file);

    filename = _solver->getFileSavingPrefix()+"_yInterpolation_iter_"+int2str(iter)+ ".csv";
    file = fopen(filename.c_str(),"w");
    if (pbc->getPBCMethod() == nonLinearPeriodicBC::CSIM)
      cubicSplineToFile(_yBase,pmanager,file,1);
    else
      lagrangeToFile(_yBase,pmanager,file);
    fclose(file);

    filename = _solver->getFileSavingPrefix()+"_zInterpolation_iter_"+int2str(iter)+ ".csv";
    file = fopen(filename.c_str(),"w");
    if (pbc->getPBCMethod() == nonLinearPeriodicBC::CSIM)
      cubicSplineToFile(_zBase,pmanager,file,2);
    else
      lagrangeToFile(_zBase,pmanager,file);
    fclose(file);
  }

  const std::vector<elementGroup*>& bgr = _solver->getMicroBC()->getBoundaryElementGroup();
  for (int i =0; i< bgr.size(); i++){
    std::string filename =_solver->getFileSavingPrefix()+"_pert_" + int2str(i)+ "_iter_"+int2str(iter)+ ".csv";
    FILE* file = fopen(filename.c_str(),"w");
    const elementGroup* g = bgr[i];
    for (elementGroup::vertexContainer::const_iterator it = g->vbegin(); it!= g->vend(); it++){
      MVertex* v = it->second;
      SVector3 pert = this->getPerturbation(pmanager,v);
      fprintf(file,"%ld %e %e %e %e %e %e\n",v->getNum(), v->x(),v->y(),v->z(),pert[0],pert[1],pert[2]);
    }
    fclose(file);
  }
};

int pbcConstraintElementGroup::sizeOfDirectConstraints() const{
  int numDir =0;
  for (std::set<constraintElement*>::const_iterator it = _allConstraint.begin();
            it!= _allConstraint.end(); it++){
    const constraintElement* cel = *it;
    if (cel->isDirect()){
      numDir++;
    }
  }
  return numDir;
};

bool pbcConstraintElementGroup::isActive(const MVertex* v) const {
  for (std::set<constraintElement*>::const_iterator it = _allConstraint.begin();
            it!= _allConstraint.end(); it++){
    const constraintElement* cel = *it;
    if (cel->isActive(v)) return true;
  }
  return false;
};

bool pbcConstraintElementGroup::isVirtual(const MVertex* v) const{
  for (std::set<MVertex*>::const_iterator it = _virtualVertices.begin(); it!= _virtualVertices.end(); it++){
    MVertex* vv = *it;
    if (vv->getNum() == v->getNum()) return true;
  }

  return false;
};

void pbcConstraintElementGroup::print() const {
  for (std::set<constraintElement*>::iterator it = _allConstraint.begin(); it!= _allConstraint.end(); it++){
    (*it)->print();
  }
}

void pbcConstraintElementGroup::createConstraintForTest(){
  _allConstraint.clear();
  constraintElement::allPositiveDof.clear();
  Msg::Info("apply periodic boundary condition");

	if (_solver->getMicroBC()->getNumberOfConstrainedConstitutiveExtraDofs() > 0){
		Msg::Error("This implementation pbcConstraintElementGroup::createConstraintForTest is not considered with existing constitutive extra Dofs");
	}

  if (_solver->getMicroBC()->getType() == nonLinearMicroBC::PBC){
    nonLinearPeriodicBC* pbc = static_cast<nonLinearPeriodicBC*>(_solver->getMicroBC());
    const std::vector<elementGroup*>& bgroup = _solver->getMicroBC()->getBoundaryElementGroup();

    if (bgroup.size()>0){
      if  (pbc->getPBCMethod() == nonLinearPeriodicBC::CEM){
      Msg::Info("Imposing PBC by periodic mesh formulation");
      this->createPeriodicMeshConstraintElementGroup();
      }
      else if (pbc->getPBCMethod() == nonLinearPeriodicBC::CSIM){
        Msg::Info("Imposing PBC by cubic spline formulation");
        this->createCubicSplineConstraintElementGroup();
      }
      else if (pbc->getPBCMethod() == nonLinearPeriodicBC::LIM){
        Msg::Info("Imposing PBC by lagrange formulation");
        this->createLagrangeConstraintElementGroup();
      }
      else if (pbc->getPBCMethod() == nonLinearPeriodicBC::FE_LIN or  
            pbc->getPBCMethod() == nonLinearPeriodicBC::FE_QUA){
        Msg::Info("Imposing PBC by lagrange C0");
        this->createFEConstraintElementGroup();
      }
      else if (pbc->getPBCMethod() == nonLinearPeriodicBC::FE_HIGH_ORDER){
        Msg::Info("Imposing PBC by lagrange C_inf");
        this->createFEConstraintElementGroup();
      }
      else if (pbc->getPBCMethod() == nonLinearPeriodicBC::PROJECT){
        Msg::Info("Imposing PBC by projection method");
        this->createProjectConstraintElementGroup();
      }
      else{
        Msg::Error("this method is not implemented to impose PBC");
      }
    }
    else{
      Msg::Error("no group elements on boundary");
    }

		if (_v1) Msg::Info("root vertex v1 = %d;",_v1->getNum());
		if (_v2) Msg::Info("root vertex v2 = %d; vec12 = %f %f %f",_v2->getNum(),_v2->x()-_v1->x(),_v2->y()-_v1->y(),_v2->z()-_v1->z());
		if (_v4) Msg::Info("root vertex v4 = %d; vec14 = %f %f %f",_v4->getNum(),_v4->x()-_v1->x(),_v4->y()-_v1->y(),_v4->z()-_v1->z());
		if (_v5) Msg::Info("root vertex v5 = %d; vec15 = %f %f %f",_v5->getNum(),_v5->x()-_v1->x(),_v5->y()-_v1->y(),_v5->z()-_v1->z());


		constraintElement* cele  = new periodicMeshConstraint(_solver->getMicroBC(),_LagSpace,_MultSpace,-1,_v3,_v4,_v2,_v1);
		_allConstraint.insert(cele);

		if (_solver->getMicroBC()->getDim() ==3){
			cele = new periodicMeshConstraint(_solver->getMicroBC(),_LagSpace,_MultSpace,-1,_v7,_v3,_v5,_v1);
			_allConstraint.insert(cele);

			cele = new periodicMeshConstraint(_solver->getMicroBC(),_LagSpace,_MultSpace,-1,_v6,_v2,_v5,_v1);
			_allConstraint.insert(cele);

			cele = new periodicMeshConstraint(_solver->getMicroBC(),_LagSpace,_MultSpace,-1,_v8,_v4,_v5,_v1);
			_allConstraint.insert(cele);
		}

  }
  else{
    Msg::Error("this boundary type is not implemented for these kinds of tests");
  }
};

void pbcConstraintElementGroup::numberDof_virtualVertices(nlsDofManager* pmanager){
  Msg::Info("begin numbering all virtual Dofs from interpolation");
	std::vector<int> constrainedComp(_solver->getMicroBC()->getConstrainedComps().begin(), _solver->getMicroBC()->getConstrainedComps().end());
  for (std::set<MVertex*>::iterator it = _virtualVertices.begin(); it!= _virtualVertices.end(); it++){
    MVertex* v = *it;
    if (this->isActive(v)){
      std::vector<Dof> k;
      getKeysFromVertex(_LagSpace,v,constrainedComp,k);
      for (int i=0; i<k.size(); i++){
        pmanager->numberDof(k[i]);
      }
      #ifdef _DEBUG
      Msg::Info("Vertex %d is active",v->getNum());
      #endif
    }
    else{
      #ifdef _DEBUG
      Msg::Info("Vertex %d is inactive",v->getNum());
      #endif
    }
  }
  Msg::Info("end numbering all virtual Dofs from interpolation");
};

void pbcConstraintElementGroup::applyLinearConstraintsToSystem(nlsDofManager* pmanager){
  Msg::Info("number of constraint elements= %d",_allConstraint.size());
  std::map<Dof,DofAffineConstraint<double> > linearConstraint;
  for (std::set<constraintElement*>::iterator it = this->constraintGroupBegin(); it!= this->constraintGroupEnd(); it++){
    constraintElement* cele = *it;
    cele->getLinearConstraintsByVertices(linearConstraint);
    for (std::map<Dof,DofAffineConstraint<double> >::iterator itp = linearConstraint.begin(); itp != linearConstraint.end(); itp++){
			if (!pmanager->isConstrained(itp->first) and !pmanager->isFixed(itp->first)){
      	pmanager->setLinearConstraint(itp->first, itp->second);
			}
    };
    linearConstraint.clear();
	};
};

MVertex* pbcConstraintElementGroup::getCornerVertex(const int i) const{
  if (i ==1) return _v1;
  else if (i == 2) return _v2;
  else if (i == 3) return _v3;
  else if (i == 4) return _v4;
  else if (i == 5) return _v5;
  else if (i == 6) return _v6;
  else if (i == 7) return _v7;
  else if (i == 8) return _v8;
  else Msg::Error("at corner %d no vertex exits");
  return NULL;
};
void pbcConstraintElementGroup::getDofsOfCornerVertex(const int i, std::vector<Dof>& R) const{
  MVertex* v = getCornerVertex(i);
  if (v != NULL){
    std::vector<int> constrainedComp(_solver->getMicroBC()->getConstrainedComps().begin(), _solver->getMicroBC()->getConstrainedComps().end());
    getKeysFromVertex(_LagSpace,v,constrainedComp,R);
  }
};

SPoint3 pbcConstraintElementGroup::getRootPoint(){
	if (_v1 != NULL) return _v1->point();
	else{
		const std::vector<elementGroup*>& bgroup = _solver->getMicroBC()->getBoundaryElementGroup();
		if (_solver->getMicroBC()->getDim() == 2){
			std::set<MVertex*> vv;
			InterpolationOperations::groupIntersection(bgroup[0],bgroup[1],vv);
			if (vv.size() == 1){
				_v1 = *(vv.begin());
			}
			else{
				SPoint3 oneVp = (bgroup[0]->vbegin()->second)->point();
				elementGroup::vertexContainer::const_iterator it = bgroup[1]->vbegin();
				SPoint3 firstVn = (it->second)->point();
				it++;
				SPoint3 secondVn = (it->second)->point();

				SPoint3 p = project(oneVp,firstVn,secondVn);
				_v1 = new MVertex(p.x(),p.y(),p.z());
			}
			return _v1->point();
		}
		else if (_solver->getMicroBC()->getDim() == 3){

			MElement* e1 = (bgroup[0]->begin()->second);
      MElement* e2 = (bgroup[2]->begin()->second);
      MElement* e3 = (bgroup[1]->begin()->second);
      fullMatrix<double> m(3,3); m.setAll(0.);
      fullVector<double> b(3); b.setAll(0.);

      std::vector<MVertex*> vv1, vv2, vv3;
      e1->getVertices(vv1);
      e2->getVertices(vv2);
      e3->getVertices(vv3);

      computeSurface(vv1[0]->point(),vv1[1]->point(),vv1[2]->point(),m(0,0),m(0,1),m(0,2),b(0));
      computeSurface(vv2[0]->point(),vv2[1]->point(),vv2[2]->point(),m(1,0),m(1,1),m(1,2),b(1));
      computeSurface(vv3[0]->point(),vv3[1]->point(),vv3[2]->point(),m(2,0),m(2,1),m(2,2),b(2));

      fullMatrix<double> invm(3,3);
      m.invert(invm);
      fullVector<double> f(3);
      invm.mult(b,f);

			SPoint3 p(f(0),f(1),f(2));
			_v1 = new MVertex(p.x(),p.y(),p.z());
			return p;
		}
		else{
			Msg::Error("getRootPoint has not been implemented for %d D problems",_solver->getMicroBC()->getDim());
			static SPoint3 pt(0.,0.,0.);
			return pt;
		}
	}
};

void pbcConstraintElementGroup::getPeriodicity(SVector3& Lx, SVector3& Ly, SVector3& Lz){
  Lx = _periodic12;
  Ly = _periodic14;
  Lz = _periodic15;
};

