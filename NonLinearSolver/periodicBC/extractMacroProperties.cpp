//
//
// Description: based class for extraction of homogenized tangents
//
// Author:  <Van Dung NGUYEN>, (C) 2010
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "extractMacroProperties.h"

#if defined(HAVE_PETSC)
#include "petscmat.h"
#include "petscvec.h"
#endif
/*
#if defined(HAVE_PETSC)
void stiffnessCondensationExtraDofDiffusion::fillStress(STensor3& stress, SVector3& flux, double& rvevolume, double &ene, PetscScalar *val) const
{

      for (int row = 0; row<9; row++){
        int i,j;
        Tensor23::getIntsFromIndex(row,i,j);
        stress(i,j) =val[row]/rvevolume;
      };
      for (int row = 9; row<12; row++){
        flux(row-9) = val[row]/rvevolume;
      };
      ene = val[12]/rvevolume;
      //Msg::Error("vol: %e", rvevolume);
}
void stiffnessCondensationExtraDofDiffusion::fillTangent(double& rvevolume, STensor43& tangent, STensor3& tangentExtraDofFluxGradExtraDof,  SVector3& tangentExtraDofFluxExtraDof,
                                                         STensor33& tangentPGradExtraDof, STensor33& tangentExtraDofFluxF, STensor3& tangentPExtraDof, double &tangentInternalEnergyExtraDofExtraDofValue,
                                                         Mat &L) const
{
    PetscInt Row, Col;
    MatGetSize(L,&Row,&Col);
    //Msg::Info("size of system %d %d",Row, Col);
    if(Row<12 or Col <12) Msg::Error(" extraDofDiffusionInterpolationStiffnessCondensationPETSC::fillTangent to be implemented in 2d");

    if (rvevolume >0){
      for (int row = 0; row< Row; ++row){
        for (int col = 0; col< Col; ++col){
          PetscScalar val;
          PetscInt ii = row, jj = col;
          MatGetValues(L,1,&ii,1,&jj,&val);
          //Msg::Error(" %d %d: %e",row, col, val/rvevolume);
          if(row<9)
          {
            int i,j;
            Tensor23::getIntsFromIndex(row,i,j);
            if(col<9)
            {
              int k,l;
              Tensor23::getIntsFromIndex(col,k,l);
              tangent(i,j,k,l) = val/rvevolume;
            }
            else if(col<12)
              tangentPGradExtraDof(i,j,col-9) = val/rvevolume;
            else
              tangentPExtraDof(i,j) = val/rvevolume;
          }
          else if(row<12)
          {
            if(col<9)
            {
              int k,l;
              Tensor23::getIntsFromIndex(col,k,l);
              tangentExtraDofFluxF(row-9,k,l) = val/rvevolume;
            }
            else if(col<12)
              tangentExtraDofFluxGradExtraDof(row-9,col-9) = val/rvevolume;
            else
              tangentExtraDofFluxExtraDof(row-9) = val/rvevolume;
          }
          else if (row==12 and col==12)
            tangentInternalEnergyExtraDofExtraDofValue= val/rvevolume;
        }
      };
    }

};

#endif
*/

