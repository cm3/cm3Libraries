//
//
// Description: class using PETSc for extraction of homogenized tangents
// in case of PBC
//
// Author:  <Van Dung NGUYEN>, (C) 2010
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "extractMacroPropertiesPETSc.h"
#include "pbcSystems.h"
#include "functionPETSc.h"



#if defined(HAVE_PETSC)

#if (PETSC_VERSION_MAJOR < 3  || (PETSC_VERSION_MAJOR == 3 && PETSC_VERSION_MINOR < 7))
 #define MatCreateVecs(mat, right, left) MatGetVecs(mat, right, left)
#endif

using namespace functionPETSc;

stiffnessCondensationPETScWithDirectConstraints::stiffnessCondensationPETScWithDirectConstraints(nlsDofManager* p, pbcAlgorithm* ma,
                               bool sflag, bool tflag, MPI_Comm com)
      :_pAssembler(p),_pAl(ma),_bConMat(NULL),_cConMat(NULL),
      _stressflag(sflag), _tangentflag(tflag), _isInitialized(false){
			_comm = com;
		};
stiffnessCondensationPETScWithDirectConstraints::~stiffnessCondensationPETScWithDirectConstraints(){
  this->clear();
};

void stiffnessCondensationPETScWithDirectConstraints::setSplittedDofSystem(pbcAlgorithm* pal){
  pal->getSplittedDof()->setSplittedDofsSystem(pbcDofManager::INTERNAL,pbcDofManager::INDIRECT,pbcDofManager::DIRECT);
}

void stiffnessCondensationPETScWithDirectConstraints::init(){
  if (!_isInitialized){
    _isInitialized = true;
    pbcDofManager* pbcAssembler = _pAl->getSplittedDof();
    // for constraint form C_b u_b + C_c u_c = S_b*K
    // u_b == indirect dofs u_c == direct dofs

    _bConMat = new linearConstraintMatrixPETSc(_comm);
    _pAl->computeIndirectLinearConstraintMatrix(_bConMat);
    // for constraint form u_c = S_c*K
    _cConMat = new linearConstraintMatrixPETSc(_comm);
    _pAl->computeDirectLinearConstraintMatrix(_cConMat);

    // get all Mat in linear constraint matrix
    Mat& Cb = _bConMat->getFirstMatrix();
    Mat& Cc = _bConMat->getSecondMatrix();
    Mat& Sb = _bConMat->getKinematicalMatrix();
    Mat& Sc = _cConMat->getKinematicalMatrix();

    // inv(Cb*CbT) is necessary if computing stress -->compute from Lagrange multipliers
    if (_stressflag){
      _try(MatTranspose(Cc,MAT_INITIAL_MATRIX,&_minusCcT));
      _try(MatScale(_minusCcT,-1.));
      Mat CbCbT;
      _try(MatMatTransposeMult(Cb,Cb,MAT_INITIAL_MATRIX,PETSC_DEFAULT,&CbCbT));
      Mat invCbCbT;
      _try(functionPETSc::MatInverse_MatMatSolve(CbCbT, &invCbCbT,_comm));
      _try(MatMatMult(invCbCbT,Cb,MAT_INITIAL_MATRIX,PETSC_DEFAULT,&_RT));
      _try(MatDestroy(&CbCbT));
      _try(MatDestroy(&invCbCbT));
    };

    _try(MatConvert(Cb,MATDENSE,MAT_INPLACE_MATRIX,&Cb));
    // all necessary transpose matrix
    _try(MatTranspose(Sb,MAT_INITIAL_MATRIX,&_SbT));
    _try(MatTranspose(Cb,MAT_INITIAL_MATRIX,&_CbT));
    _try(MatTranspose(Sc,MAT_INITIAL_MATRIX,&_ScT));
    _try(MatTranspose(Cc,MAT_INITIAL_MATRIX,&_CcT));
  }

};

void stiffnessCondensationPETScWithDirectConstraints::clear(){
  if (_isInitialized){
    _isInitialized = false;
    _try(MatDestroy(&_SbT));
    _try(MatDestroy(&_CbT));
    _try(MatDestroy(&_ScT));
    _try(MatDestroy(&_CcT));
    if (_stressflag){
      _try(MatDestroy(&_RT));
      _try(MatDestroy(&_minusCcT));
    };

    if (_bConMat) delete _bConMat;
    if (_cConMat) delete _cConMat;
    _bConMat = NULL;
    _cConMat = NULL;
  };
}

void stiffnessCondensationPETScWithDirectConstraints::stressSolve(fullVector<double>& stressVec, const double Vrve){
  if (_stressflag){
    int nK = _pAl->getNumberOfMacroToMicroKinematicVariables();
    stressVec.resize(nK);
    stressVec.setAll(0.);
    if (!_isInitialized){
      this->init();
    };
    // vec get value
    Vec lambdaB, lambdaC;
    _try(MatCreateVecs(_SbT,&lambdaB,PETSC_NULL));
    _try(MatCreateVecs(_ScT,&lambdaC,PETSC_NULL));

    pbcDofManager* pbcAssembler = _pAl->getSplittedDof();
    // get linear system
    std::string str = "A";
    linearSystem<double>* lsys = _pAssembler->getLinearSystem(str);
    nonLinearSystem<double>* linearsys = dynamic_cast<nonLinearSystem<double>*>(lsys);
    if (linearsys){
      Vec fb;
      Mat& Cb = _bConMat->getFirstMatrix();
      _try(MatCreateVecs(Cb,&fb,PETSC_NULL));
      const std::map<Dof,int>& indirectDofMap = pbcAssembler->getDofMap(pbcDofManager::INDIRECT);
      for (std::map<Dof,int>::const_iterator it = indirectDofMap.begin(); it!= indirectDofMap.end(); it++){
        double val = 0.;
        int num = _pAssembler->getDofNumber(it->first);
        if (num >=0)
          linearsys->getFromRightHandSideMinus(num, val);
        else
          Msg::Error("Internal force at this DOFs does not exist, use ofther system MULT_ELIM");
        PetscInt row = it->second;
        _try(VecSetValues(fb,1,&row,&val,INSERT_VALUES));
      }
      _try(VecAssemblyBegin(fb));
      _try(VecAssemblyEnd(fb));

      Vec fc;
      Mat& Cc = _bConMat->getSecondMatrix();
      _try(MatCreateVecs(Cc,&fc,PETSC_NULL));

      const std::map<Dof,int>& directDofMap = pbcAssembler->getDofMap(pbcDofManager::DIRECT);
      for (std::map<Dof,int>::const_iterator it = directDofMap.begin(); it!= directDofMap.end(); it++){
        double val = 0.;
        int num = _pAssembler->getDofNumber(it->first);
        if (num >=0 )
          linearsys->getFromRightHandSideMinus(num, val);
        else
          Msg::Error("Internal force at this DOFs does not exist, use ofther system MULT_ELIM");
        PetscInt row = it->second;
        _try(VecSetValues(fc,1,&row,&val,INSERT_VALUES));
      }
      _try(VecAssemblyBegin(fc));
      _try(VecAssemblyEnd(fc));

      _try(MatMult(_RT,fb,lambdaB));
      _try(MatMultAdd(_minusCcT,lambdaB,fc,lambdaC));

      Vec P;
      Mat& Sb = _bConMat->getKinematicalMatrix();
      _try(MatCreateVecs(Sb,&P,PETSC_NULL));
      _try(MatMult(_SbT,lambdaB,P));
      _try(MatMultAdd(_ScT,lambdaC,P,P));
      #ifdef _DEBUG
      _try(VecView(P,PETSC_VIEWER_STDOUT_SELF));
      #endif

      PetscScalar* val;
      _try(VecGetArray(P,&val));
      for (int ip=0; ip< nK; ip++){
        stressVec(ip) = val[ip]/Vrve;
      }

      _try(VecDestroy(&fb));
      _try(VecDestroy(&fc));
      _try(VecDestroy(&lambdaB));
      _try(VecDestroy(&lambdaC));
      _try(VecDestroy(&P));
    }
    else{
      Msg::Error("The pbc nonlinear solver have to used to compute");
    };
  };
}

void stiffnessCondensationPETScWithDirectConstraints::tangentCondensationSolve(fullMatrix<double>& tangentMat, const double Vrve){
  if (_tangentflag){
    int nK = _pAl->getNumberOfMacroToMicroKinematicVariables();
    tangentMat.resize(nK,nK);
    tangentMat.setAll(0.);

    if (!_isInitialized){
      this->init();
    };
    // all submatrix created within the solver
    splitStiffnessMatrixPETSc* _stiffness = dynamic_cast<splitStiffnessMatrixPETSc*>(_pAl->getSplitStiffness());
    if (_stiffness == NULL){
      Msg::Error("splitStiffnessMatrixPETSc has to be used");
    }

    Mat& Kii = _stiffness->getIIMat();
    Mat& Kib = _stiffness->getIBMat();
    Mat& Kic = _stiffness->getICMat();

    Mat& Kbi = _stiffness->getBIMat();
    Mat& Kbb = _stiffness->getBBMat();
    Mat& Kbc = _stiffness->getBCMat();

    Mat& Kci = _stiffness->getCIMat();
    Mat& Kcb = _stiffness->getCBMat();
    Mat& Kcc = _stiffness->getCCMat();

    // first condensation step
    Mat Kbbt, Kbct, Kcbt, Kcct;
    _try(functionPETSc::CondenseMatrixBlocks(Kii,Kib,Kic,Kbi,Kbb,Kbc,Kci,Kcb,Kcc,&Kbbt,&Kbct,&Kcbt,&Kcct));

    Mat& Cc = _bConMat->getSecondMatrix();
    Mat& Cb = _bConMat->getFirstMatrix();
    Mat& Sb = _bConMat->getKinematicalMatrix();
    Mat& Sc = _cConMat->getKinematicalMatrix();

    Mat Kccstar, Kblambda, Cbc, Ccb;
     _try(MatConvert(Cb,MATDENSE,MAT_INPLACE_MATRIX,&Cb));
     _try(MatConvert(_CbT,MATDENSE,MAT_INPLACE_MATRIX,&_CbT));
    _try(functionPETSc::CondenseMatrixBlocks(Kbbt,Kbct,_CbT,Kcbt,Kcct,_CcT,Cb,Cc,PETSC_NULL,&Kccstar,&Ccb,&Cbc,&Kblambda));


    Mat Sbc, Scb, Scc;
    _try(MatMatMult(Cbc,Sc,MAT_INITIAL_MATRIX,1.,&Sbc));
    _try(MatScale(Sbc,-1.));
    _try(MatAXPY(Sbc,1,Sb,DIFFERENT_NONZERO_PATTERN));

    _try(MatMatMult(_ScT,Ccb,MAT_INITIAL_MATRIX,1.,&Scb));
    _try(MatScale(Scb,-1.));
    _try(MatAXPY(Scb,1,_SbT,DIFFERENT_NONZERO_PATTERN));


    Mat ScTKccstar;
    _try(MatMatMult(_ScT,Kccstar,MAT_INITIAL_MATRIX,1.,&ScTKccstar));
    _try(MatMatMult(ScTKccstar,Sc,MAT_INITIAL_MATRIX,1,&Scc));
    _try(MatDestroy(&ScTKccstar));

    Mat L;
    _try(functionPETSc::CondenseMatrixBlocks(Kblambda,Sbc,Scb,Scc,&L));

    for (int row = 0; row< nK; ++row){
      for (int col = 0; col< nK; ++col){
        PetscScalar val;
        PetscInt ii = row, jj = col;
        MatGetValues(L,1,&ii,1,&jj,&val);
        tangentMat(row,col) = val/Vrve;
      }
    };
    _try(MatDestroy(&L));

    _try(MatDestroy(&Kbbt));
    _try(MatDestroy(&Kbct));
    _try(MatDestroy(&Kcbt));
    _try(MatDestroy(&Kcct));

    _try(MatDestroy(&Kccstar));
    _try(MatDestroy(&Kblambda));
    _try(MatDestroy(&Cbc));
    _try(MatDestroy(&Ccb));

    _try(MatDestroy(&Sbc));
    _try(MatDestroy(&Scb));
    _try(MatDestroy(&Scc));

  }
  else{
    printf("Error in rve volume /n");
  }
}



stiffnessCondensationPETScWithoutDirectConstraints::stiffnessCondensationPETScWithoutDirectConstraints(nlsDofManager* p, pbcAlgorithm* ma,
                                          bool sflag, bool tflag, MPI_Comm com)
																				:_pAssembler(p),_pAl(ma),_bConMat(0),
																				_stressflag(sflag),_tangentflag(tflag),_isInitialized(false){
  _comm = com;
};
stiffnessCondensationPETScWithoutDirectConstraints::~stiffnessCondensationPETScWithoutDirectConstraints(){
  this->clear();
};

void stiffnessCondensationPETScWithoutDirectConstraints::setSplittedDofSystem(pbcAlgorithm* pal){
  pal->getSplittedDof()->setSplittedDofsSystem(pbcDofManager::INTERNAL,pbcDofManager::DEPENDENT,pbcDofManager::INDEPENDENT);
}
void stiffnessCondensationPETScWithoutDirectConstraints::init(){
  if (!_isInitialized){
    _isInitialized = true;
    // linea constraint matrix
    _bConMat = new linearConstraintMatrixPETSc(_comm);
    // compute linear constraint by pbcAlgorithm
    if (_pAl->getSplittedDof()->sizeOfDirectDof() >0){
      Msg::Error("this condensation procedure does not work with considering direct Dofs");
    }
    _pAl->computeIndirectLinearConstraintMatrixForPBCSystem(_bConMat);
    // constraint matrix
    Mat& Cb = _bConMat->getFirstMatrix();
    Mat& Cc = _bConMat->getSecondMatrix();
    // first order matrix
    Mat& S = _bConMat->getKinematicalMatrix();
    //
    _try(functionPETSc::MatMatSolve_MultipleRHS(Cb,Cc,S,&_Cbc,&_Sbc));
    _try(MatScale(_Cbc,-1.));
  }
};

void stiffnessCondensationPETScWithoutDirectConstraints::clear(){
  if (_isInitialized){
    _isInitialized = false;
    _try(MatDestroy(&_Cbc));
    _try(MatDestroy(&_Sbc));
    if (_bConMat) delete _bConMat;
    _bConMat = NULL;
  }
}

void stiffnessCondensationPETScWithoutDirectConstraints::stressSolve(fullVector<double>& stressVec, const double Vrve){
  if (_stressflag){
    int nK = _pAl->getNumberOfMacroToMicroKinematicVariables();
    stressVec.resize(nK);
    stressVec.setAll(0.);

    if (!_isInitialized) {
      this->init();
    };
    pbcDofManager* pbcAssembler = _pAl->getSplittedDof();
    int sizedepend = pbcAssembler->sizeOfDependentDof();
    std::string str = "A";
    linearSystem<double>* lsys = _pAssembler->getLinearSystem(str);
    nonLinearSystem<double>* linearsys = dynamic_cast<nonLinearSystem<double>*>(lsys);
    pbcSystem<double>* pbcsys = dynamic_cast<pbcSystem<double>*>(lsys);
    if (linearsys and pbcsys){
      Vec fc;
      _try(VecCreate(_comm,&fc));
      _try(VecSetSizes(fc,PETSC_DECIDE,sizedepend));
      _try(VecSetFromOptions(fc));
      const std::map<Dof,int>& dependentDofMap = pbcAssembler->getDofMap(pbcDofManager::DEPENDENT);
      for (std::map<Dof,int>::const_iterator it = dependentDofMap.begin();
                                             it!= dependentDofMap.end(); it++){
        double val = 0.;
        int num = _pAssembler->getDofNumber(it->first);
        if (num != -1)
          linearsys->getFromRightHandSideMinus(num, val);
        else
          Msg::Error("The pbcNonLinearSystem has to used");
        PetscInt row = it->second;
        _try(VecSetValues(fc,1,&row,&val,INSERT_VALUES));
      }
      _try(VecAssemblyBegin(fc));
      _try(VecAssemblyEnd(fc));

      Vec P;
      _try(VecCreate(_comm,&P));
      _try(VecSetSizes(P,nK,PETSC_DECIDE));
      _try(VecSetFromOptions(P));

      _try(MatMultTranspose(_Sbc,fc,P));

      PetscScalar* val;
      _try(VecGetArray(P,&val));
      for (int row = 0; row<nK; row++){
        stressVec(row) = val[row]/Vrve;
      };
      _try(VecDestroy(&fc));
      _try(VecDestroy(&P));
    }
    else{
      Msg::Error("The pbc nonlinear solver have to used to compute");
    };
  };
}


void stiffnessCondensationPETScWithoutDirectConstraints::tangentCondensationSolve(fullMatrix<double>& tangentMat, const double Vrve){
  if (_tangentflag){
    int nK = _pAl->getNumberOfMacroToMicroKinematicVariables();
    tangentMat.resize(nK,nK);
    tangentMat.setAll(0.);

    if (!_isInitialized) {
      this->init();
    }
    // get all submatrices on the splitted matrix

    splitStiffnessMatrixPETSc* _stiffness = dynamic_cast<splitStiffnessMatrixPETSc*>(_pAl->getSplitStiffness());

    Mat& Kii = _stiffness->getIIMat();
    Mat& Kib = _stiffness->getIBMat();
    Mat& Kic = _stiffness->getICMat();
    Mat& Kbi = _stiffness->getBIMat();
    Mat& Kbb = _stiffness->getBBMat();
    Mat& Kbc = _stiffness->getBCMat();
    Mat& Kci = _stiffness->getCIMat();
    Mat& Kcb = _stiffness->getCBMat();
    Mat& Kcc = _stiffness->getCCMat();

    pbcDofManager* pbcAssembler = _pAl->getSplittedDof();
    int ni = pbcAssembler->sizeOfInternalDof();
    int nb = pbcAssembler->sizeOfDependentDof();
    int nc = pbcAssembler->sizeOfIndependentDof();

    if ((ni==0) or (nb==0)){
      Msg::Error("Tangent condensation do not work: ni = %d, nb = %d, nc = %d",ni,nb,nc);
    }

    // condense to other matrix by solving Schur problem
    Mat Kbbt, Kbct, Kcbt, Kcct;
    _try(functionPETSc::CondenseMatrixBlocks(Kii,Kib,Kic,Kbi,Kbb,Kbc,Kci,Kcb,Kcc,&Kbbt,&Kbct,&Kcbt,&Kcct));

    Mat Kstar;
    if (nc>0){
      Mat Kccstar, Kcbstar, Kbcstar;
      _try(MatTransposeMatMult(_Cbc,Kbbt,MAT_INITIAL_MATRIX,1.,&Kcbstar));
      _try(MatAXPY(Kcbstar,1,Kcbt,DIFFERENT_NONZERO_PATTERN));

      _try(MatMatMult(Kbbt,_Cbc,MAT_INITIAL_MATRIX,1.,&Kbcstar));
      _try(MatAXPY(Kbcstar,1,Kbct,DIFFERENT_NONZERO_PATTERN));

      Mat mattemp;
      _try(MatTransposeMatMult(_Cbc,Kbct,MAT_INITIAL_MATRIX,PETSC_DEFAULT,&mattemp));
      _try(MatMatMult(Kcbstar,_Cbc,MAT_INITIAL_MATRIX,PETSC_DEFAULT,&Kccstar));
      _try(MatAXPY(Kccstar,1,Kcct,DIFFERENT_NONZERO_PATTERN));
      _try(MatAXPY(Kccstar,1,mattemp,DIFFERENT_NONZERO_PATTERN));

      _try(MatDestroy(&mattemp));

      _try(functionPETSc::CondenseMatrixBlocks(Kccstar,Kcbstar,Kbcstar,Kbbt,&Kstar));
      _try(MatDestroy(&Kccstar));
      _try(MatDestroy(&Kcbstar));
      _try(MatDestroy(&Kbcstar));

    }
    else{
      Kstar = Kbbt;
    }

    Mat KstarSbc;
    _try(MatMatMult(Kstar,_Sbc,MAT_INITIAL_MATRIX,PETSC_DECIDE,&KstarSbc));
    Mat  L;
    _try(MatTransposeMatMult(_Sbc,KstarSbc,MAT_INITIAL_MATRIX,PETSC_DECIDE,&L));
    _try(MatDestroy(&KstarSbc));

    for (int row = 0; row< nK; ++row){
      for (int col = 0; col<nK; ++col){
        PetscScalar val;
        PetscInt ii = row, jj = col;
        _try(MatGetValues(L,1,&ii,1,&jj,&val));
        tangentMat(row,col) = val/Vrve;
      };
    };

    _try(MatDestroy(&Kstar));
    _try(MatDestroy(&L));
    if (nc > 0){
      _try(MatDestroy(&Kbbt));
    }
    _try(MatDestroy(&Kbct));
    _try(MatDestroy(&Kcbt));
    _try(MatDestroy(&Kcct));
  }
  else{
    printf("Error in rve volume /n");
  };
};



stiffnessCondensationPETSc::stiffnessCondensationPETSc(nlsDofManager* p, pbcAlgorithm* ma,
                                          bool sflag, bool tflag, MPI_Comm com)
																				:_pAssembler(p),_pAl(ma),_bConMat(0),
																				_stressflag(sflag),_tangentflag(tflag),_isInitialized(false){
  _comm = com;
};
stiffnessCondensationPETSc::~stiffnessCondensationPETSc(){
  this->clear();
};

void stiffnessCondensationPETSc::setSplittedDofSystem(pbcAlgorithm* pal){
  pal->getSplittedDof()->setSplittedDofsSystem(pbcDofManager::INTERNAL,pbcDofManager::BOUNDARY,pbcDofManager::NONE);
}

void stiffnessCondensationPETSc::init(){
  if (!_isInitialized){
    _isInitialized = true;
    // linea constraint matrix
    _bConMat = new linearConstraintMatrixPETSc(_comm);
    _pAl->computeLinearConstraintMatrix(_bConMat);
    Mat& C = _bConMat->getFirstMatrix();
    Mat& S = _bConMat->getKinematicalMatrix();

     if (_stressflag){
      Mat CCT;
      _try(MatMatTransposeMult(C,C,MAT_INITIAL_MATRIX,PETSC_DECIDE,&CCT));
      Mat invCCT;
      _try(functionPETSc::MatInverse_MatMatSolve(CCT, &invCCT,_comm));
      _try(MatMatMult(invCCT,C,MAT_INITIAL_MATRIX,PETSC_DEFAULT,&_RT));
      _try(MatDestroy(&CCT));
      _try(MatDestroy(&invCCT));
    };
    _try(MatConvert(C,MATDENSE,MAT_INPLACE_MATRIX,&C));
    _try(MatTranspose(C,MAT_INITIAL_MATRIX,&_CT));
    _try(MatTranspose(S,MAT_INITIAL_MATRIX,&_ST));
  }
};

void stiffnessCondensationPETSc::clear(){
  if (_isInitialized){
    _isInitialized = false;
    if (_bConMat) delete _bConMat;
    _bConMat = NULL;
    _try(MatDestroy(&_CT));
    _try(MatDestroy(&_ST));
    _try(MatDestroy(&_RT));
  }
}

void stiffnessCondensationPETSc::stressSolve(fullVector<double>& stressVec, const double Vrve){
  if (_stressflag){
    int nK = _pAl->getNumberOfMacroToMicroKinematicVariables();
    stressVec.resize(nK);
    stressVec.setAll(0.);

    if (!_isInitialized) {
      this->init();
    };

    pbcDofManager* pbcAssembler = _pAl->getSplittedDof();
    std::string str = "A";
    linearSystem<double>* lsys = _pAssembler->getLinearSystem(str);
    nonLinearSystem<double>* linearsys = dynamic_cast<nonLinearSystem<double>*>(lsys);
    pbcSystem<double>* pbcsys = dynamic_cast<pbcSystem<double>*>(lsys);
    if (linearsys and pbcsys){
      Vec fb;
      Mat& C = _bConMat->getFirstMatrix();
      _try(MatCreateVecs(C,&fb,PETSC_NULL));
      const std::map<Dof,int>& boundaryDofMap = pbcAssembler->getDofMap(pbcDofManager::BOUNDARY);
      for (std::map<Dof,int>::const_iterator it = boundaryDofMap.begin();
                                             it!= boundaryDofMap.end(); it++){
        double val = 0.;
        int num = _pAssembler->getDofNumber(it->first);
        if (num != -1)
          linearsys->getFromRightHandSideMinus(num, val);
        else
          Msg::Error("The pbcNonLinearSystem has to used");
        PetscInt row = it->second;
        _try(VecSetValues(fb,1,&row,&val,INSERT_VALUES));
      }
      _try(VecAssemblyBegin(fb));
      _try(VecAssemblyEnd(fb));

      Vec lambda;
      _try(MatCreateVecs(_ST,&lambda,PETSC_NULL));
      _try(MatMult(_RT,fb,lambda));

      Mat& S = _bConMat->getKinematicalMatrix();
      Vec P;
      _try(MatCreateVecs(S,&P,PETSC_NULL));
      _try(MatMult(_ST,lambda,P));

      PetscScalar* val;
      _try(VecGetArray(P,&val));
      for (int row = 0; row<nK; row++){
        stressVec(row) = val[row]/Vrve;
      };
      _try(VecDestroy(&lambda));
      _try(VecDestroy(&P));
    }
    else{
      Msg::Error("The pbc nonlinear solver have to used to compute");
    };
  };
}


void stiffnessCondensationPETSc::tangentCondensationSolve(fullMatrix<double>& tangentMat, const double Vrve){
  if (_tangentflag){
    int nK = _pAl->getNumberOfMacroToMicroKinematicVariables();
    tangentMat.resize(nK,nK);
    tangentMat.setAll(0.);

    if (!_isInitialized) {
      this->init();
    }
    // get all submatrices on the splitted matrix

    splitStiffnessMatrixPETSc* _stiffness = dynamic_cast<splitStiffnessMatrixPETSc*>(_pAl->getSplitStiffness());

    Mat& Kii = _stiffness->getIIMat();
    Mat& Kib = _stiffness->getIBMat();
    Mat& Kbi = _stiffness->getBIMat();
    Mat& Kbb = _stiffness->getBBMat();


    // condense to other matrix by solving Schur problem
    Mat Kbbt;
    _try(functionPETSc::CondenseMatrixBlocks(Kii,Kib,Kbi,Kbb,&Kbbt));

    Mat& C = _bConMat->getFirstMatrix();
    // first order matrix
    Mat& S = _bConMat->getKinematicalMatrix();

    Mat Kstar;
    _try(functionPETSc::CondenseMatrixBlocks(Kbbt,_CT,C,PETSC_NULL,&Kstar));
    Mat L;
    _try(functionPETSc::CondenseMatrixBlocks(Kstar,S,_ST,PETSC_NULL,&L));

    for (int row = 0; row< nK; ++row){
      for (int col = 0; col<nK; ++col){
        PetscScalar val;
        PetscInt ii = row, jj = col;
        _try(MatGetValues(L,1,&ii,1,&jj,&val));
        tangentMat(row,col) = val/Vrve;
      };
    };

    _try(MatDestroy(&Kstar));
    _try(MatDestroy(&L));
    _try(MatDestroy(&Kbbt));

  }
  else{
    printf("Error in rve volume /n");
  };
};

#endif // HAVE_PETSC
