//
//
// Description: function to switch tensor-- vector data
// Author:  <Van Dung NGUYEN>, (C) 2010
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef HIGHORDERTENSOR_H_
#define HIGHORDERTENSOR_H_

#include "STensor3.h"
#include "STensor33.h"
#include "STensor43.h"
#include "STensor53.h"
#include "STensor63.h"

#include <sstream>

/*
Class for switching tensor -vector notations
*/

inline std::string int2str(int number){
   std::stringstream ss;//create a stringstream
   ss << number;//add number to the stream
   return ss.str();//return a string with the contents of the stream
};

class Tensor11{
  public:
    virtual ~Tensor11(){};
    static int getIndex(){
      return 0;
    }
    static void getIntsFromIndex(const int index, int& i){
      i = 0;
    };
    static FILE* createFile(const std::string filename){
      FILE* file = fopen(filename.c_str(),"w");
      std::string str = "Time";
      fprintf(file,"%s",str.c_str());
      str = ";Comp."+ int2str(0);
      fprintf(file,"%s",str.c_str());
      fprintf(file,"\n");
      fflush(file);
      return file;
    }
    static void writeData(FILE*& file, const double& ten, const double time){
      if (file == NULL)
        Msg::Error("File pointer is null Vector3");
      else{
        fprintf(file,"%.16g",time);
        fprintf(file,";%.16g",ten);
        fprintf(file,"\n");
        fflush(file);
      }
    };
};

class Tensor13{
  public:
    virtual ~Tensor13(){};
    static int getIndex(int i){
      return i;
    }
    static void getIntsFromIndex(const int index, int& i){
      i = index%3;
    };
    static FILE* createFile(const std::string filename){
      FILE* file = fopen(filename.c_str(),"w");
      std::string str = "Time";
      fprintf(file,"%s",str.c_str());
      for (int idex = 0; idex <3; idex++){
        int i;
        Tensor13::getIntsFromIndex(idex,i);
        str = "Comp."+ int2str(i);
        fprintf(file,";%s",str.c_str());
      }
      fprintf(file,"\n");
      fflush(file);
      return file;
    }
    static void writeData(FILE*& file, const SVector3& ten, const double time){
      if (file == NULL)
        Msg::Error("File pointer is null Vector3");
      else{
        fprintf(file,"%.16g",time);
        for (int idex=0; idex<3; idex++){
          int i;
          Tensor13::getIntsFromIndex(idex,i);
          fprintf(file,";%.16g",ten(i));
        }
        fprintf(file,"\n");
        fflush(file);
      }
    };
};


class Tensor23{
  public:
    virtual ~Tensor23(){};
    static int getIndex(int i, int j){
      return i+3*j;
    }
    static void getIntsFromIndex(const int index, int& i, int& j){
      int val = index;  i = index%3;
      val = (val-i)/3;  j = val%3;
    };
    static FILE* createFile(const std::string filename){
      FILE* file = fopen(filename.c_str(),"w");
      std::string str = "Time";
      fprintf(file,"%s",str.c_str());
      for (int idex = 0; idex <9; idex++){
        int i,j;
        Tensor23::getIntsFromIndex(idex,i,j);
        str = "Comp."+ int2str(i) + ""+int2str(j);
        fprintf(file,";%s",str.c_str());
      }
      fprintf(file,"\n");
      fflush(file);
      return file;
    }
    static void writeData(FILE*& file, const STensor3& ten, const double time){
      if (file == NULL)
        Msg::Error("File pointer is null Tensor3");
      else{
        fprintf(file,"%.16g",time);
        for (int idex=0; idex<9; idex++){
          int i,j;
          Tensor23::getIntsFromIndex(idex,i,j);
          fprintf(file,";%.16g",ten(i,j));
        }
        fprintf(file,"\n");
        fflush(file);
      }
    };
};

// interface for tensor A_{ijk}
class Tensor33{
  public:
    virtual ~Tensor33(){};
		static int getIndex(int i, int j, int k){
      return i+3*j+9*k;
		};
		static void getIntsFromIndex(const int index, int& i, int& j, int& k){
      int val = index;  i = index%3;
      val  = (val-i)/3; j = val%3;
      val = (val-j)/3;  k = val%3;
		};
		static FILE* createFile(const std::string filename){
      FILE* file = fopen(filename.c_str(),"w");
      std::string str = "Time";
      fprintf(file,"%s",str.c_str());
      for (int idex = 0; idex< 27; idex++){
        int i,j,k;
        Tensor33::getIntsFromIndex(idex,i,j,k);
        str = "Comp."+ int2str(i) + ""+int2str(j)+""+int2str(k);
        fprintf(file,";%s",str.c_str());
      }
      fprintf(file,"\n");
      fflush(file);
      return file;
		}
		static void writeData(FILE*& file, const STensor33& ten, const double time){
		  if (file == NULL)
        Msg::Error("File pointer is null Tensor33");
      else{
        fprintf(file,"%.16g",time);
        for (int idex=0; idex<27; idex++){
          int i,j,k;
          Tensor33::getIntsFromIndex(idex,i,j,k);
          fprintf(file,";%.16g",ten(i,j,k));
        }
        fprintf(file,"\n");
        fflush(file);
      }
		};
};

class Tensor43{
  public:
    virtual ~Tensor43(){};
    static int getIndex(int i, int j, int k, int l){
      return i+3*j+9*k+27*l;
		};
		static void getIntsFromIndex(const int index, int& i, int& j, int& k, int& l){
      int val = index;  i = index%3;
      val = (val-i)/3;  j = val%3;
      val = (val-j)/3;  k = val%3;
      val = (val-k)/3;  l = val%3;
		};
		static FILE* createFile(const std::string filename){
      FILE* file = fopen(filename.c_str(),"w");
      std::string str = "Time";
      fprintf(file,"%s",str.c_str());
      for (int idex=0; idex<81; idex++){
        int i,j,k, l;
        Tensor43::getIntsFromIndex(idex,i,j,k,l);
        str = "Comp."+int2str(i) +""+ int2str(j)+""+int2str(k)+""+int2str(l);
        fprintf(file,";%s",str.c_str());
      }
      fprintf(file,"\n");
      fflush(file);
      return file;
		};
		static void writeData(FILE*& file, const STensor43& ten, const double time){
		  if (file == NULL)
        Msg::Error("File pointer is null Tensor43");
      else{
        fprintf(file,"%.16g",time);
        for (int idex =0; idex<81; idex++){
          int i,j,k,l;
          Tensor43::getIntsFromIndex(idex,i,j,k,l);
          fprintf(file,";%.16g",ten(i,j,k,l));
        }
        fprintf(file,"\n");
        fflush(file);
      }
		};
};



// interface for tensor A_ijklm
class Tensor53{
	public:
    virtual ~Tensor53(){}
		static int getIndex(int i, int j, int k, int l, int m){
      return i+3*j+9*k+27*l+81*m;
		};
		static void getIntsFromIndex(const int index, int& i, int& j, int& k, int& l, int& m){
      int val = index;  i = index%3;
      val = (val-i)/3;  j = val%3;
      val = (val-j)/3;  k = val%3;
      val = (val-k)/3;  l = val%3;
      val = (val-l)/3;  m = val%3;
		};
		static FILE* createFile(const std::string filename){
      FILE* file = fopen(filename.c_str(),"w");
      std::string str = "Time";
      fprintf(file,"%s",str.c_str());
      for (int idex=0; idex<243; idex++){
        int i,j,k, l, m;
        Tensor53::getIntsFromIndex(idex,i,j,k,l,m);
        str = "Comp."+int2str(i) +""+ int2str(j)+""+int2str(k)+""+int2str(l)+""+int2str(m);
        fprintf(file,";%s",str.c_str());
      }
      fprintf(file,"\n");
      fflush(file);
      return file;
		};
		static void writeData(FILE*& file, const STensor53& ten, const double time){
		  if (file == NULL)
        Msg::Error("File pointer is null Tensor53");
      else{
        fprintf(file,"%.16g",time);
        for (int idex =0; idex < 243; idex ++){
          int i,j,k,l,m;
          Tensor53::getIntsFromIndex(idex,i,j,k,l,m);
          fprintf(file,";%.16g",ten(i,j,k,l,m));
        }
        fprintf(file,"\n");
        fflush(file);
      }
		}
};


// interface for tensor A_ijklmn
class Tensor63{
	public:
    virtual ~Tensor63(){}
		static int getIndex(int i, int j, int k, int l, int m, int n){
      return i+3*j+9*k+27*l+81*m+243*n;
    };
    static void getIntsFromIndex(const int index, int &i, int& j, int& k, int& l, int& m, int& n){
      int val = index;  i = index%3;
      val  = (val-i)/3; j = val%3;
      val = (val-j)/3;  k = val%3;
      val = (val-k)/3;  l = val%3;
      val = (val-l)/3;  m = val%3;
      val = (val-m)/3;  n = val%3;
    };
    static FILE* createFile(const std::string filename){
      FILE* file = fopen(filename.c_str(),"w");
      std::string str = "Time";
      fprintf(file,"%s",str.c_str());
      for (int idex=0; idex<729; idex++){
        int i,j,k, l, m, n;
        Tensor63::getIntsFromIndex(idex,i,j,k,l,m,n);
        str= "Comp."+int2str(i) +""+ int2str(j)+""+int2str(k)+""+int2str(l)+""+int2str(m)+""+int2str(n);
        fprintf(file,";%s",str.c_str());
      }
      fprintf(file,"\n");
      fflush(file);
      return file;
    };
    static void writeData(FILE*& file, const STensor63& ten, const double time){
		  if (file == NULL)
        Msg::Error("File pointer is null Tensor63");
      else{
        fprintf(file,"%.16g",time);
        for (int idex=0; idex<729; idex++){
          int i,j,k,l,m,n;
          Tensor63::getIntsFromIndex(idex,i,j,k,l,m,n);
          fprintf(file,";%.16g",ten(i,j,k,l,m,n));
        }
        fprintf(file,"\n");
        fflush(file);
      };
    }
};




#endif // HIGHORDERTENSOR_H_
