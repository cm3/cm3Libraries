//
//
// Description: enforce linear constrint by Lagrange multipliers
//
// Author:  <Van Dung NGUYEN>, (C) 2010
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "LagrangeMultiplierMethod.h"
#include "pbcConstraintElement.h"

void lagrangeMultiplier::addMultiplierKeys(MVertex* vp, MVertex* vn, std::vector<Dof> &keys, int flag){
  // flag is used for creating multiplier dof
  std::vector<Dof> kP, kN;
  getKeysFromVertex(vp,kP);
  getKeysFromVertex(vn,kN);
  int entity=kP[0].getEntity();
  int type=kP[0].getType();
  int ndofs=kP.size();
  keys.clear();
  int i1,i2;
  Dof::getTwoIntsFromType(type,i1,i2);
  for (int j=0; j<ndofs; j++){
    keys.push_back(Dof(entity,Dof::createTypeWithTwoInts(j+cubicSplineConstraintElement::getShiftToCreateKeys()+flag,i2)));
  };
};

void lagrangeMultiplier::get(MVertex* vp, MVertex* vn, fullMatrix<double> &m, fullVector<double> &g){
  std::vector<Dof> k;
  getKeysFromVertex(vp,k);
  int ndofs=k.size();
  g.resize(ndofs);
  fullVector<double> pos; pos.resize(3);
  pos(0)=vp->x()-vn->x(); pos(1)=vp->y()-vn->y(); pos(2)=vp->z()-vn->z();
  g.resize(_ndofs);
  imposedDef.mult(pos,g);
  //
  if(_ndofs>_dim) Msg::Error("lagrangeMultiplier::get check matrix m for coupled problems");
  m.resize(3,3); m.setAll(0.0);
  m(0,2)=m(2,0)=1.;
  m(1,2)=m(2,1)=-1.;

};

void lagrangeMultiplier::unitAssemble(MVertex* vp, MVertex* vn,int flag){
  std::vector<Dof> keys, kP, kN;
  addMultiplierKeys(vp,vn,keys,flag);
  getKeysFromVertex(vp,kP);
  getKeysFromVertex(vn,kN);
  fullMatrix<double> m; fullVector<double> g;
  get(vp,vn,m,g);
  int ndofs=keys.size();
  std::vector<Dof> kAssemble;
  for (int i=0; i<ndofs; i++){
    kAssemble.push_back(kP[i]);
    kAssemble.push_back(kN[i]);
    kAssemble.push_back(keys[i]);
    p->assemble(kAssemble,m);
    kAssemble.clear();
  };
  p->assemble(keys,g);
};


void lagrangeMultiplier::numberDof(){
  int flag=0;
  for (vertexContainer::iterator it=gVertex[2].begin(); it!=gVertex[2].end(); it++){
    MVertex* vp=*it;
    MVertex* vn=findNearestVertex(vp,gVertex[0]);
    std::vector<Dof> keys;
    flag+=1;
    addMultiplierKeys(vp,vn,keys,flag);
    for (int i=0; i<keys.size(); i++){
      p->numberDof(keys[i]);
    };
  };
  for (vertexContainer::iterator it=gVertex[3].begin(); it!=gVertex[3].end(); it++){
    MVertex* vp=*it;
    MVertex* vn=findNearestVertex(vp,gVertex[1]);
    std::vector<Dof> keys;
    flag+=1;
    addMultiplierKeys(vp,vn,keys,flag);
    for (int i=0; i<keys.size(); i++){
      p->numberDof(keys[i]);
    };
  };
};

void lagrangeMultiplier::assemble(){
  int flag=0;
  for (std::set<MVertex*>::iterator it=gVertex[2].begin(); it!=gVertex[2].end(); it++){
    MVertex* vp=*it;
    MVertex* vn=findNearestVertex(vp,gVertex[0]);
    flag+=1;
    unitAssemble(vp,vn,flag);
  };
  for (std::set<MVertex*>::iterator it=gVertex[3].begin(); it!=gVertex[3].end(); it++){
    MVertex* vp=*it;
    MVertex* vn=findNearestVertex(vp,gVertex[1]);
    flag+=1;
    unitAssemble(vp,vn,flag);
  };
};
void lagrangeMultiplier::applyPeriodicCondition(){
};

