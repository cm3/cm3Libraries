//
//
// Description: pbc Dof manager
//
// Author:  <Van Dung NGUYEN>, (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef PBCDOFMANAGER_H_
#define PBCDOFMANAGER_H_

#include "staticDofManager.h"
#include "splitStiffnessMatrix.h"

// dof decomposition strategy
// TOTAL_DOF = INTERNAL + BOUNDARY
//           = INTERNAL + DIRECT + INDIRECT
//           = INTERNAL + DIRECT + DEPENDENT + INDEPENDENT
//           = INTERNAL + DIRECT + DEPENDENT + VIRTUAL + REAL_INDEPENDENT
// BOUNDARY associated with constraint elements
// DIRECT = direct constraint--> no DOF in constraint RHS (PeriodicNode and FixedNode)
// INDIRECT = BOUNDARY - DIRECT
// DEPENDENT = LHS of indirect constraint --> with DOFs in constraint RHS
// INDEPENDENT = DOF in RHS of indirect constraint
// VIRTUAL --> additional DOFs added to system in interpolation method
// REAL_INDEPENDENT = INDEPENDENT - VIRTUAL

// For multipliers to numerate constraints
// MULTIPLIER = DIRECT_MULTIPLIER + INDIRECT_MULTIPLIER
// DIRECT_MULTIPLIER associated with direct constraints --> no DOFs in constraint RHS
// INDIRECT_MULTIPLIER associated with indirect constraints -> with DOFs in constraint RHS


// infeface for dof management in micro-scopic problem

class pbcDofManager{
  public:
		enum DofType {INTERNAL=0, BOUNDARY, DIRECT, INDIRECT,
                  DEPENDENT, INDEPENDENT, VIRTUAL, REAL_INDEPENDENT,
		              MULTIPLIER, DIRECT_MULTIPLIER, INDIRECT_MULTIPLIER,NONE};
	protected:
	  std::set<Dof> _fixedDof;
		// field Dofs
		
    std::map<Dof,int> _internalDof;
    std::map<Dof,int> _boundaryDof;

    std::map<Dof,int> _directDof;
    std::map<Dof,int> _indirectDof;

    std::map<Dof,int> _dependentDof;
		std::map<Dof,int> _independentDof;

		std::map<Dof,int> _virtualDof;
		std::map<Dof,int> _realIndependentDof;

    // multiplier Dofs
    std::map<Dof,int> _multiplierDof;		// for all multiplier dofs, numerated constraints information
    std::map<Dof,int> _directMultiplierDof; // dependent constraints,
    std::map<Dof,int> _indirectMultiplierDof; // independent constraint

    std::map<Dof,int> _noneDof; // always Non but use to avoid  pointer null

    splitStiffnessMatrix* _splittedStiffness; // for  splitted stiffness matrix
    const std::map<Dof,int>* Igroup;
    const std::map<Dof,int>* Bgroup;
    const std::map<Dof,int>* Cgroup;

	public:
		pbcDofManager();
		virtual ~pbcDofManager();

		virtual void clear();

		void allocateSplittedMatrix();
    splitStiffnessMatrix* getSplitStiffnessMatrix();

		int sizeOfInternalDof() const {return _internalDof.size();};
		int sizeOfBoundaryDof() const{return _boundaryDof.size();};
		int sizeOfDirectDof() const{return _directDof.size();};
		int sizeOfIndirectDof() const{return _indirectDof.size();};
    int sizeOfDependentDof() const {return _dependentDof.size();};
		int sizeOfIndependentDof() const {return _independentDof.size();};
		int sizeOfVirtualDof() const {return _virtualDof.size();};
		int sizeOfRealIndependentDof() const{return _realIndependentDof.size();};

		void printDofs(const DofType type) const{
		  const std::map<Dof,int>& dofmap = this->getDofMap(type);
		  Msg::Info("Print dof Map %d",type);
		  for  (std::map<Dof,int>::const_iterator it = dofmap.begin(); it != dofmap.end(); it++){
		    const Dof& D = it->first;
		    int icomp, ifield;
		    Dof::getTwoIntsFromType(D.getType(),icomp,ifield);
		    Msg::Info("Dof = %d, DofEntitity=%d Icomp = %d Ifield = %d",it->second, D.getEntity(), icomp, ifield);
		  }
		  Msg::Info("done printing");

		}

    int sizeOfMultiplierDof() const {return _multiplierDof.size();};
    int sizeOfDirectMultiplierDof() const {return _directMultiplierDof.size();};
    int sizeOfIndirectMultiplierDof() const {return _indirectMultiplierDof.size();};

    int getInternalDofNumber(const Dof& key) const{
			std::map<Dof,int>::const_iterator it = _internalDof.find(key);
			if (it != _internalDof.end()) return it->second;
			else return -1;
		};
		int getBoundaryDofNumber(const Dof& key) const{
      std::map<Dof,int>::const_iterator it = _boundaryDof.find(key);
      if (it!= _boundaryDof.end()) return it->second;
      else return -1;
		};
		int getDirectDofNumber(const Dof& key) const{
			std::map<Dof,int>::const_iterator it = _directDof.find(key);
			if (it != _directDof.end()) return it->second;
			else return -1;
		};
		int getIndirectDofNumber(const Dof& key) const{
			std::map<Dof,int>::const_iterator it = _indirectDof.find(key);
			if (it != _indirectDof.end()) return it->second;
			else return -1;
		};

		int getDependentDofNumber(const Dof& key) const{
			std::map<Dof,int>::const_iterator it = _dependentDof.find(key);
			if (it != _dependentDof.end()) return it->second;
			else return -1;
		};
		int getIndependentDofNumber(const Dof& key) const{
      std::map<Dof,int>::const_iterator it = _independentDof.find(key);
			if (it != _independentDof.end()) return it->second;
			else return -1;
		}
		int getVirtualDofNumber(const Dof& key) const{
      std::map<Dof,int>::const_iterator it = _virtualDof.find(key);
			if (it != _virtualDof.end()) return it->second;
			else return -1;
		}
		int getRealIndependentDofNumber(const Dof& key) const{
      std::map<Dof,int>::const_iterator it = _realIndependentDof.find(key);
      if (it != _realIndependentDof.end()) return it->second;
      else return -1;
		}

		int getMultiplierDofNumber(const Dof& key) const{
			std::map<Dof,int>::const_iterator it = _multiplierDof.find(key);
			if (it!= _multiplierDof.end()) return it->second;
			else return -1;
		};
		int getDirectMultiplierDofNumber(const Dof& key) const{
			std::map<Dof,int>::const_iterator it = _directMultiplierDof.find(key);
			if (it!= _directMultiplierDof.end()) return it->second;
			else return -1;
		};

		int getIndirectMultiplierDofNumber(const Dof& key) const{
			std::map<Dof,int>::const_iterator it = _indirectMultiplierDof.find(key);
			if (it!= _indirectMultiplierDof.end()) return it->second;
			else return -1;
		};
		
		void fixDof(const Dof& key){
			_fixedDof.insert(key);
		};


		inline void numberInternalDof(const Dof& key){
		  if (_boundaryDof.find(key) != _boundaryDof.end()) return;
			std::map<Dof,int>::iterator it = _internalDof.find(key);
			if (it == _internalDof.end()){
				unsigned int size = _internalDof.size();
				_internalDof[key] = size;
			};
		};
		inline void numberBoundaryDof(const Dof& key){
      if (_internalDof.find(key) != _internalDof.end()) return;
      std::map<Dof,int>::iterator it = _boundaryDof.find(key);
      if (it == _boundaryDof.end()){
        unsigned int size = _boundaryDof.size();
        _boundaryDof[key] = size;
      }
		};

		inline void numberDirectDof(const Dof& key){
		  if (_internalDof.find(key) != _internalDof.end()) return;
      if (_indirectDof.find(key) != _indirectDof.end()) return;
      std::map<Dof,int>::iterator it = _directDof.find(key);
      if (it == _directDof.end()){
        unsigned int size = _directDof.size();
        _directDof[key] = size;
      }
		};
		inline void numberIndirectDof(const Dof& key){
		  if (_internalDof.find(key) != _internalDof.end()) return;
      if (_directDof.find(key) != _directDof.end()) return;
      std::map<Dof,int>::iterator it = _indirectDof.find(key);
      if (it == _indirectDof.end()){
        unsigned int size = _indirectDof.size();
        _indirectDof[key] = size;
      }
		}

		inline void numberDependentDof(const Dof& key){
      if (_internalDof.find(key) != _internalDof.end()) return;
      if (_directDof.find(key) != _directDof.end()) return;
      if (_independentDof.find(key)!= _independentDof.end()) return;
      std::map<Dof,int>::iterator it = _dependentDof.find(key);
      if (it == _dependentDof.end()){
        unsigned int size = _dependentDof.size();
        _dependentDof[key] = size;
      };
    };
    inline void numberIndependentDof(const Dof& key){
			if (_internalDof.find(key) != _internalDof.end()) return;
			if (_directDof.find(key) != _directDof.end()) return;
			if (_dependentDof.find(key) != _dependentDof.end()) return;
			std::map<Dof,int>::iterator it = _independentDof.find(key);
			if (it == _independentDof.end()){
				unsigned int size = _independentDof.size();
				_independentDof[key] = size;
			};
		};
		virtual inline void numberVirtualDof(const Dof& key){
			if (_internalDof.find(key) != _internalDof.end()) return;
			if (_directDof.find(key) != _directDof.end()) return;
			if (_dependentDof.find(key) != _dependentDof.end()) return;
			if (_realIndependentDof.find(key) != _realIndependentDof.end()) return;
			std::map<Dof,int>::iterator it = _virtualDof.find(key);
			if (it == _virtualDof.end()){
				unsigned int size = _virtualDof.size();
				_virtualDof[key] = size;
			};
		};
		virtual inline void numberRealIndependentDof( const Dof& key){
      if (_internalDof.find(key) != _internalDof.end()) return;
			if (_directDof.find(key) != _directDof.end()) return;
			if (_dependentDof.find(key) != _dependentDof.end()) return;
      if (_virtualDof.find(key) != _virtualDof.end()) return;
      std::map<Dof,int>::iterator it = _realIndependentDof.find(key);
      if (it == _realIndependentDof.end()){
        unsigned int size = _realIndependentDof.size();
        _realIndependentDof[key] = size;
      }
		};
		virtual inline void numberMultiplierDof(const Dof& key){
			std::map<Dof,int>::iterator it = _multiplierDof.find(key);
			if (it == _multiplierDof.end()){
				unsigned int size = _multiplierDof.size();
				_multiplierDof[key] = size;
			};
		};
		virtual inline void numberDirectMultiplierDof(const Dof& key){
			if (_indirectMultiplierDof.find(key) != _indirectMultiplierDof.end()) return;
			std::map<Dof,int>::iterator it = _directMultiplierDof.find(key);
			if (it == _directMultiplierDof.end()){
				unsigned int size = _directMultiplierDof.size();
				_directMultiplierDof[key] = size;
			};
		};
		virtual inline void numberIndirectMultiplierDof(const Dof& key){
			if (_directMultiplierDof.find(key) != _directMultiplierDof.end()) return;
			std::map<Dof,int>::iterator it = _indirectMultiplierDof.find(key);
			if (it == _indirectMultiplierDof.end()){
				unsigned int size = _indirectMultiplierDof.size();
				_indirectMultiplierDof[key] = size;
			};
		};

		virtual void assembleSubMatrix(const Dof& R, const Dof& C, const double& val);
		virtual void assemble(const std::vector<Dof>& R, const fullMatrix<double>& mat){
			for (int i=0; i<R.size(); i++){
				for (int j=0; j<R.size(); j++){
					assembleSubMatrix(R[i],R[j],mat(i,j));
				};
			};
		};
		const std::map<Dof,int>& getDofMap(const DofType type) const{
		  if (type == INTERNAL) return _internalDof;
		  else if (type == BOUNDARY) return _boundaryDof;
		  else if (type == DIRECT) return _directDof;
		  else if (type == INDIRECT) return _indirectDof;
		  else if (type == DEPENDENT) return _dependentDof;
		  else if (type == INDEPENDENT) return _independentDof;
		  else if (type == VIRTUAL) return _virtualDof;
		  else if (type == REAL_INDEPENDENT) return _realIndependentDof;
		  else if (type == MULTIPLIER) return _multiplierDof;
		  else if (type == DIRECT_MULTIPLIER) return _directMultiplierDof;
		  else if (type == INDIRECT_MULTIPLIER) return _indirectMultiplierDof;
		  else if (type == NONE) return _noneDof;
		  else{
		    Msg::Error("This Dof type does not exist %d",type);
        static const std::map<Dof,int> tmp;
        return tmp;
		  }
		}

		virtual void setSplittedDofsSystem(const DofType typeI, const DofType typeB, const DofType typeC){
		  Igroup = & (getDofMap(typeI));
		  Bgroup = & (getDofMap(typeB));
		  Cgroup = & (getDofMap(typeC));
		}

		virtual void numberDof(const DofType type, nlsDofManager* p) const{
		  const std::map<Dof,int>& mapDof = getDofMap(type);
      for (std::map<Dof,int>::const_iterator it = mapDof.begin(); it!= mapDof.end(); it++){
        p->numberDof(it->first);
      };
		};

		virtual void insertInSparsityPattern(const Dof &R, const Dof &C);

    virtual void sparsityDof(const std::vector<Dof> &R)
    {
      for(int i=0;i<R.size();++i)
      {
        for(int j=0;j<R.size();++j)
        {
          this->insertInSparsityPattern(R[i],R[j]);
        }
      }
    }
};

#endif // PBCDOFMANAGER_H_
