//
// C++ Interface:  deep material networks
//
//
// Author:  <V.-D. Nguyen>, (C) 2021
//
// Copyright: See COPYING file that comes with this distribution
//
//

#include "TrainingArbitraryDeepMaterialNetworks.h"
#include "OS.h"


TrainingArbitraryDeepMaterialNetwork::TrainingArbitraryDeepMaterialNetwork(NetworkInteraction& T, const CoefficientReduction& reduction):
_interaction(&T), _dofsAllComponentsNumbered(false), _fixedWeightsOfMaterialNodes(false), _fixedPhaseFraction(false),
_activationFunction(NULL)
{
  _coeffReduction = reduction.clone();
}

TrainingArbitraryDeepMaterialNetwork::~TrainingArbitraryDeepMaterialNetwork()
{
  if (_coeffReduction != NULL)
  {
    delete _coeffReduction;
    _coeffReduction = NULL;
  }
  if (_activationFunction != NULL)
  {
    delete _activationFunction;
    _activationFunction = NULL;
  }
}



void TrainingArbitraryDeepMaterialNetwork::fixedWeightsOfMaterialNodes(bool fl)
{
  _fixedWeightsOfMaterialNodes = fl;
  if (fl)
  {
    Msg::Info("weights of material nodes are fixed");
  }
}

void TrainingArbitraryDeepMaterialNetwork::fixedPhaseFraction(bool fl, std::string af)
{
  _fixedPhaseFraction = fl;
  if (fl)
  {
    _nodeZValues.clear();
    if (_activationFunction != NULL)
    {
      delete _activationFunction;
    }
    _activationFunction = activationFunction::createActivationFunction(af.c_str());
    Msg::Info("phase fractions are fixed during training");
  }
};


void TrainingArbitraryDeepMaterialNetwork::getMaterialNodeKeys(const MaterialNode* node,  std::vector<Dof>& keys) const
{
  int tag= _interaction->getTag();
  keys.push_back(Dof(node->numberIndex, Dof::createTypeWithTwoInts(0,tag)));
};

void TrainingArbitraryDeepMaterialNetwork::getInteractionCoefficientsKeys(const InteractionMechanism* im, std::vector<Dof>& keys) const
{
  int nbVars = _coeffReduction->getNumberOfCoefficientVars(*_interaction,*im);
  if (nbVars > 0)
  {
    int tag = _interaction->getTag()+1;
    for (int i=0; i< nbVars; i++)
    {    
      keys.push_back(Dof(im->getIndex(), Dof::createTypeWithTwoInts(i,tag)));
    }    
  }
};

void TrainingArbitraryDeepMaterialNetwork::getInteractionDirectionKeys(const InteractionMechanism* im, std::vector<Dof>& keys) const
{
  int tag = _interaction->getTag()+2;
  for (int i=0; i< im->getDirectionVars().size(); i++)
  {
    keys.push_back(Dof(im->getIndex(), Dof::createTypeWithTwoInts(i,tag)));
  }
};

void TrainingArbitraryDeepMaterialNetwork::numberDofsAllComponents()
{
  auto numberDofVec = [](std::vector<Dof>& RR, std::map<Dof,int>& mapDof)
  {
    for (int j=0; j< RR.size(); j++)
    {
      Dof& key = RR[j];
      if(mapDof.find(key) == mapDof.end()) 
      {
        std::size_t size = mapDof.size();
        mapDof[key] = size;
      };
    };
  };
  _dofsAllComponentsNumbered = true;
  _weightDofs.clear();
  _coefficientDofs.clear();
  _normalDofs.clear();
  _nodeZValues.clear();
  
  std::map<int, int> numNodePerPhases;
  if (_fixedPhaseFraction)
  {
    _interaction->normalizeWeights("linear"); // convert to linear, all weight are fractions of nodes
    _interaction->getNumberMaterialNodesPerMat(numNodePerPhases);
  };
  NetworkInteraction::MaterialNodeContainer& allNodes = _interaction->getMaterialNodeContainer();
  for (NetworkInteraction::MaterialNodeContainer::iterator itnode= allNodes.begin(); itnode != allNodes.end(); itnode++)
  {
    std::vector<Dof> R;
    getMaterialNodeKeys(itnode->second,R);
    numberDofVec(R,_weightDofs);
    //
    if (_fixedPhaseFraction)
    {
      const MaterialNode* node = itnode->second;
      double fraction = _interaction->getPhaseFraction(node->matIndex);
      double fi = _interaction->getWeight(node);
      // initial value for each node
      _nodeZValues[node] = _activationFunction->getReciprocalVal(numNodePerPhases[node->matIndex]*fi/fraction);
    };
    //
  };
  // interaction
  NetworkInteraction::InteractionContainer& allInteraction = _interaction->getInteractionContainer();
  for (NetworkInteraction::InteractionContainer::iterator iinter= allInteraction.begin(); iinter != allInteraction.end(); iinter++)
  {
    InteractionMechanism* im = iinter->second;
    std::vector<Dof> R;
    getInteractionCoefficientsKeys(im,R);
    numberDofVec(R,_coefficientDofs);
    // update local Dof in interaction
    _coeffReduction->updateCoefficientVars(*_interaction,*im);
    //
    R.clear();
    getInteractionDirectionKeys(im,R);
    numberDofVec(R,_normalDofs);
  };
  int sizeOfWeights = _weightDofs.size();
  int sizeOfAlphas = _coefficientDofs.size();
  int sizeOfNormals = _normalDofs.size();

  Msg::Info("sizeOfWeights = %d, sizeOfAlphas = %d, sizeOfNormals = %d",sizeOfWeights, sizeOfAlphas, sizeOfNormals);
};

void TrainingArbitraryDeepMaterialNetwork::fixWeightsOfMaterialNodes()
{
  const NetworkInteraction::MaterialNodeContainer& allNodes = _interaction->getMaterialNodeContainer();
  for (NetworkInteraction::MaterialNodeContainer::const_iterator itnode= allNodes.begin(); itnode != allNodes.end(); itnode++)
  {
    std::vector<Dof> R;
    getMaterialNodeKeys(itnode->second,R);
    for (int j=0; j< R.size(); j++)
    {
      if (_fixedPhaseFraction)
      {
        // fixed based on phase fraction
        _fixedDof[R[j]] = _nodeZValues[itnode->second];
      }
      else
      {
        _fixedDof[R[j]] = itnode->second->weight;
      }
    }
  }
};

void TrainingArbitraryDeepMaterialNetwork::fixInteractionDirectionComp(int comp)
{
  const NetworkInteraction::InteractionContainer& allInteraction = _interaction->getInteractionContainer();
  for (NetworkInteraction::InteractionContainer::const_iterator iinter= allInteraction.begin(); iinter != allInteraction.end(); iinter++)
  {
    const InteractionMechanism* im = iinter->second;
    std::vector<Dof> R;
    getInteractionDirectionKeys(im,R);
    if (comp > R.size() -1)
    {
      Msg::Error("comp = %d does not exist",comp);
    }
    else
    {
      _fixedDof[R[comp]] = im->getDirectionVars()[comp] ;
    }
  }
};


void TrainingArbitraryDeepMaterialNetwork::numberDofs(fullVector<double>& unknownValues)
{
  auto numberOneDof= [&](Dof& key)
  {
    if (_fixedDof.find(key) != _fixedDof.end()) return;
    if (_linearConstraints.find(key) != _linearConstraints.end()) return;
    auto it = _unknown.find(key);
    if(it == _unknown.end()) {
      std::size_t size = _unknown.size();
      _unknown[key] = size;
    }
  };
  //
  numberDofsAllComponents();
  //
  _unknown.clear();
  _fixedDof.clear();
  _linearConstraints.clear();
  // fixed Dofs and linear constraints
  if (_fixedWeightsOfMaterialNodes)
  {
    fixWeightsOfMaterialNodes();
  }
  else
  {
    Msg::Info("weight is tuned during training");
  }
  for (int i=0; i< _fixInteractionDirectionComp.size(); i++)
  {
    fixInteractionDirectionComp(_fixInteractionDirectionComp[i]);
  };
  
  std::map<Dof,double> allDofValues;
  // update weights
  const NetworkInteraction::MaterialNodeContainer& allNodes = _interaction->getMaterialNodeContainer();
  for (NetworkInteraction::MaterialNodeContainer::const_iterator itnode= allNodes.begin(); itnode != allNodes.end(); itnode++)
  {
    std::vector<Dof> R;
    getMaterialNodeKeys(itnode->second,R);
    // only one dof per material node
    for (int i=0; i < R.size(); i++)
    {
      if (_fixedPhaseFraction)
      {
        allDofValues[R[i]] = _nodeZValues[itnode->second];
      }
      else
      {
        allDofValues[R[i]] = itnode->second->weight;
      }
      numberOneDof(R[i]);
    }
  };
  // interaction
  const  NetworkInteraction::InteractionContainer& allInteraction = _interaction->getInteractionContainer();
  for (NetworkInteraction::InteractionContainer::const_iterator iinter= allInteraction.begin(); iinter != allInteraction.end(); iinter++)
  {
    InteractionMechanism* im = iinter->second;
    std::vector<Dof> R;
    if (_coeffReduction->getNumberOfCoefficientVars(*_interaction,*im) > 0)
    {
      getInteractionCoefficientsKeys(im,R);
      const std::vector<double>& coefficientsVars = im->getCoeffVars();
      
      for (int i=0; i< R.size(); i++)
      {
        allDofValues[R[i]] = coefficientsVars[i];
        numberOneDof(R[i]);
      }
    }
    //    
    R.clear();
    getInteractionDirectionKeys(im,R);
    for (int i=0; i< R.size(); i++)
    {
      allDofValues[R[i]] = im->getDirectionVars()[i];
      numberOneDof(R[i]);
    }
  };
  
  // allocate and initialize unknown
  int sizeOfR = _unknown.size();
  unknownValues.resize(sizeOfR,true);
  for (std::map<Dof,int>::iterator it = _unknown.begin(); it!= _unknown.end(); it++)
  {
    const Dof& key = it->first;
    int NR = it->second;
    unknownValues(NR) = allDofValues[key];
  }
}

double TrainingArbitraryDeepMaterialNetwork::getDofValue(const fullVector<double>& unknownValues, const Dof& D) const
{
  std::map<Dof,double>::const_iterator itF = _fixedDof.find(D);
  if (itF!= _fixedDof.end())
  {
    return itF->second;
  }
  std::map<Dof,int>::const_iterator itU = _unknown.find(D);
  if (itU!= _unknown.end())
  {
    int posR = itU->second;
    return unknownValues(posR);
  };
  std::map<Dof,DofAffineConstraint<double> >::const_iterator itC = _linearConstraints.find(D);
  if (itC != _linearConstraints.end())
  {
    const DofAffineConstraint<double>& cons = itC->second;
    double val = cons.shift;
    for (int i=0; i< cons.linear.size(); i++)
    {
      const Dof& DD = cons.linear[i].first;
      double coef = cons.linear[i].second;
      val += (coef*getDofValue(unknownValues, DD));
    }
    return val;
  }
  Msg::Error("Dof is not found in TrainingArbitraryDeepMaterialNetwork::getDofValue");
  Msg::Exit(0);
};


void TrainingArbitraryDeepMaterialNetwork::updateFieldFromUnknown(const fullVector<double>& unknownValues)
{
  // get all values before updating
  std::map<int, double> phaseFractions;
  std::map<int, double> sumZPhases;
  if (_fixedPhaseFraction)
  {
    _interaction->getPhaseFractions(phaseFractions);
    for (std::map<int, double>::const_iterator it = phaseFractions.begin(); it != phaseFractions.end(); it++)
    {
      int phase = it->first;
      sumZPhases[phase] = 0.;
      //
      std::vector<int> allIdsPhase;
      _interaction->getAllMaterialNodeIdsForMat(phase,allIdsPhase);
      int Nnodes = allIdsPhase.size();
      for (int j=0; j< Nnodes; j++)
      {
        const MaterialNode* nodej = _interaction->getMaterialNode(allIdsPhase[j]);
        std::vector<Dof> R;
        getMaterialNodeKeys(nodej,R);
        double Zj = getDofValue(unknownValues,R[0]);
        sumZPhases[phase] += _activationFunction->getVal(Zj);
      }
    }
  };
  
  const NetworkInteraction::MaterialNodeContainer& allNodes = _interaction->getMaterialNodeContainer();
  for (NetworkInteraction::MaterialNodeContainer::const_iterator itnode= allNodes.begin(); itnode != allNodes.end(); itnode++)
  {
    MaterialNode* node = itnode->second;
    std::vector<Dof> R;
    getMaterialNodeKeys(node,R);
    if (_fixedPhaseFraction)
    {
      _nodeZValues[node] = getDofValue(unknownValues,R[0]); // updated value
      double fPhase = phaseFractions[node->matIndex];
      double Znode = _activationFunction->getVal(getDofValue(unknownValues,R[0]));
      double sumZ = sumZPhases[node->matIndex];
      double fi = fPhase*Znode/sumZ;
      // update weights
      node->weight = node->af->getReciprocalVal(fi);
    }
    else
    {
      // only one dof per material node
      for (int i=0; i < R.size(); i++)
      {
        node->weight = getDofValue(unknownValues,R[i]);
      }
    }
  };
  
  // interaction
  NetworkInteraction::InteractionContainer& allInteraction = _interaction->getInteractionContainer();
  for (NetworkInteraction::InteractionContainer::iterator iinter= allInteraction.begin(); iinter != allInteraction.end(); iinter++)
  {
    InteractionMechanism* im = iinter->second;
    std::vector<Dof> R;
    if (_coeffReduction->getNumberOfCoefficientVars(*_interaction,*im) > 0)
    {
      getInteractionCoefficientsKeys(im,R);
      std::vector<double>& coefficientsVars = im->getRefToCoeffVars();
      for (int i=0; i< R.size(); i++)
      {
        coefficientsVars[i] = getDofValue(unknownValues,R[i]);
      }      
    }
    // since coefficients depend both weights and coeffVars
    _coeffReduction->updateCoefficientFromCoefficientVars(*_interaction,*im);
    //
    R.clear();
    getInteractionDirectionKeys(im,R);
    for (int i=0; i< R.size(); i++)
    {
      im->getRefToDirectionVars()[i] = getDofValue(unknownValues,R[i]);
    }
  };
  
  bool ok =  _interaction->checkValidity(false);
  if (!ok)
  {
    Msg::Error("interaction is not valide");
    Msg::Exit(0);
  }
};


void TrainingArbitraryDeepMaterialNetwork::updateUnknown(double lr, const fullVector<double>& g, fullVector<double>& Wcur)
{
  Wcur.axpy(g,-lr);
};



TrainingDeepMaterialNetworkLinearElastic::TrainingDeepMaterialNetworkLinearElastic(NetworkInteraction& T, const CoefficientReduction& reduction):
  TrainingArbitraryDeepMaterialNetwork(T,reduction){}
  
bool TrainingDeepMaterialNetworkLinearElastic::homogenizationProcess(DeepMaterialNetwork* Cin, fullMatrix<double>& Chomo, 
                                  bool stiff, std::map<Dof, fullMatrix<double> >* DChomoDunknown)
{
  _interaction->resetIncompatibleDispVector();
  Cin->addInteraction(*_interaction);
  static std::map<const MaterialNode*, fullMatrix<double> > DLDWeights;
  static std::map<const InteractionMechanism*, std::vector<fullMatrix<double> > > DLDCoeffs; 
  static std::map<const InteractionMechanism*, std::vector<fullMatrix<double> > > DLDNormals;
  if (_fixedWeightsOfMaterialNodes)
  {
    bool ok = Cin->initialElasticTangentByStress(Chomo,stiff,_coeffReduction,NULL,&DLDCoeffs,&DLDNormals);
    if (!ok)
    {
      return false;
    }
  }
  else
  {
    bool ok =Cin->initialElasticTangentByStress(Chomo,stiff,_coeffReduction,&DLDWeights,&DLDCoeffs,&DLDNormals);
    if (!ok)
    {
      return false;
    }
  }
  
  if (stiff)
  {
    for (std::map<Dof,int>::iterator itdof = _unknown.begin(); itdof != _unknown.end(); itdof ++)
    {
      fullMatrix<double>& mat = (*DChomoDunknown)[itdof->first];
      fullMatrixOperation::allocateAndMakeZero(mat,Chomo.size1(),Chomo.size2());
    }
    
    if (!_fixedWeightsOfMaterialNodes)
    {
      // get all values before updating
      std::map<int, double> phaseFractions;
      std::map<int, double> sumZPhases;
      if (_fixedPhaseFraction)
      {
        _interaction->getPhaseFractions(phaseFractions);
        for (std::map<int, double>::const_iterator it = phaseFractions.begin(); it != phaseFractions.end(); it++)
        {
          int phase = it->first;
          sumZPhases[phase] = 0.;
          //
          std::vector<int> allIdsPhase;
          _interaction->getAllMaterialNodeIdsForMat(phase,allIdsPhase);
          int Nnodes = allIdsPhase.size();
          for (int j=0; j< Nnodes; j++)
          {
            const MaterialNode* nodej = _interaction->getMaterialNode(allIdsPhase[j]);
            double Zj = _nodeZValues[nodej];
            sumZPhases[phase] += _activationFunction->getVal(Zj);
          }
        }
      };
    
      const NetworkInteraction::MaterialNodeContainer& allNodes = _interaction->getMaterialNodeContainer();
      for (NetworkInteraction::MaterialNodeContainer::const_iterator itnode= allNodes.begin(); itnode != allNodes.end(); itnode++)
      {
        const MaterialNode* node = itnode->second;
        fullMatrix<double>& DLDW = DLDWeights[node];
        if (_fixedPhaseFraction)
        {
          std::vector<Dof> R;
          getMaterialNodeKeys(node,R);
          
          std::vector<int> allIdsPhase;
          _interaction->getAllMaterialNodeIdsForMat(node->matIndex,allIdsPhase);
          int Nnodes = allIdsPhase.size();
          for (int j=0; j< Nnodes; j++)
          {
            const MaterialNode* nodej = _interaction->getMaterialNode(allIdsPhase[j]);
            getMaterialNodeKeys(nodej,R);
          }
          
          double fPhase = phaseFractions[node->matIndex];
          double sumfZ = sumZPhases[node->matIndex];
          double Znode = _nodeZValues[node];
          double fZnode = _activationFunction->getVal(Znode);
          double DZnode = _activationFunction->getDiff(Znode);
          
          // fi = fPhase*fZnode/fPhase
          std::vector<double> DfiDZj(R.size(),0.);
          //
          DfiDZj[0] = fPhase*DZnode/sumfZ;
          for (int j=0; j< Nnodes; j++)
          {
            const MaterialNode* nodej = _interaction->getMaterialNode(allIdsPhase[j]);
            double Zj = _nodeZValues[nodej];
            double DZj = _activationFunction->getDiff(Zj);
            DfiDZj[j+1] = -fPhase*fZnode*DZj/(sumfZ*sumfZ);
          }
          //
          for (int j=0; j< R.size(); j++)
          {
            if (_unknown.find(R[j]) != _unknown.end())
            {
              fullMatrix<double>& DChomoDWVar = (*DChomoDunknown)[R[j]];
              DChomoDWVar.axpy(DLDW, DfiDZj[j]);
            }
            else
            {
              Msg::Error("node not found");
            }
          }
        }
        else
        {
          double DwDWVar = node->af->getDiff(node->weight);
          std::vector<Dof> R;
          getMaterialNodeKeys(node,R);
          for (int j=0; j< R.size(); j++)
          {
            if (_unknown.find(R[j]) != _unknown.end())
            {
              fullMatrix<double>& DChomoDWVar = (*DChomoDunknown)[R[j]];
              DChomoDWVar.axpy(DLDW, DwDWVar);
            }
            else
            {
              Msg::Error("node not found");
            }
          }
        }
      };
    }
    // interaction
    const NetworkInteraction::InteractionContainer& allInteraction = _interaction->getInteractionContainer();
    for (NetworkInteraction::InteractionContainer::const_iterator iinter= allInteraction.begin(); iinter != allInteraction.end(); iinter++)
    {
      const InteractionMechanism* im = iinter->second;
      //
      std::vector<Dof> R;
      if (_coeffReduction->getNumberOfCoefficientVars(*_interaction,*im) > 0)
      {
        getInteractionCoefficientsKeys(im,R);
        std::vector<fullMatrix<double> >& DLDalphas = DLDCoeffs[im];
        for (int k=0; k< R.size(); k++)
        {
          if (_unknown.find(R[k]) != _unknown.end())
          {
            fullMatrix<double>& mat = (*DChomoDunknown)[R[k]];
            mat.axpy(DLDalphas[k],1.);
          }
        }
      };
      //
      std::vector<fullMatrix<double> >& DLDdir = DLDNormals[im];
      R.clear();
      getInteractionDirectionKeys(im,R);
      for (int j=0; j< R.size(); j++)
      {
        if (_unknown.find(R[j]) != _unknown.end())
        {
          fullMatrix<double>& mat = (*DChomoDunknown)[R[j]];
          mat.axpy(DLDdir[j],1.);
        }
      }
    };
  };
  return true;
};

double TrainingDeepMaterialNetworkLinearElastic::computeCostFunction(const LossMeasure& loss, const std::vector<int>& sampleIndex, fullVector<double>& grad)
{
  grad.setAll(0.);
  static fullVector<double> gPart;
  int Ntrain = _XTrain.size();
  int batchSize = sampleIndex.size();
  
  double alpha = 1./(double)(batchSize);
  double err = 0;
  for (int i=0; i< batchSize; i++)
  {
    int sample = sampleIndex[i];
    if (sample > Ntrain-1)
    {
      Msg::Error("sample %d does not exist !!!",sample);
      Msg::Exit(0);
    }
    err += computeError(loss,_XTrain[sample],_YTrain[sample],true,&gPart);
    if (STensorOperation::isnan(err))
    {
      return sqrt(-1.);
    }
    grad.axpy(gPart,alpha);
  }
  err *= (alpha);
  return err;
};

double TrainingDeepMaterialNetworkLinearElastic::evaluateTrainingSet(const LossMeasure& loss)
{
  int Ntrain = _XTrain.size();
  double err = 0;
  for (int i=0; i< Ntrain; i++)
  {
    err += computeError(loss,_XTrain[i],_YTrain[i]);
    if (STensorOperation::isnan(err))
    {
      return sqrt(-1.);
    }
  }
  err *= (1./(double)Ntrain);
  return err;
};


double TrainingDeepMaterialNetworkLinearElastic::evaluateTestSet(const LossMeasure& loss)
{
  int NTest = _XTest.size();
  double err = 0;
  for (int i=0; i< NTest; i++)
  {
    err += computeError(loss,_XTest[i],_YTest[i]);
    if (STensorOperation::isnan(err))
    {
      return sqrt(-1.);
    }
  }
  err *= (1./(double)NTest);
  return err;
};

void TrainingDeepMaterialNetworkLinearElastic::trainingDataSize(int Ns)
{
  printf("size of training set = %d\n",Ns);
  _XTrain.clear();
  _YTrain.clear();
  _XTrain.resize(Ns,NULL);
  _YTrain.resize(Ns);
};

void TrainingDeepMaterialNetworkLinearElastic::testDataSize(int Nt)
{
  printf("size of test set = %d\n",Nt);
  _XTest.clear();
  _YTest.clear();
  _XTest.resize(Nt,NULL);
  _YTest.resize(Nt);
};


void TrainingDeepMaterialNetworkLinearElastic::setTrainingSample(int row, DeepMaterialNetwork& C1, const fullMatrix<double>& Ceff)
{
  _XTrain[row] = &C1;
  if (_interaction->getDimension() == 2)
  {
    fullMatrixOperation::reducePlaneStrain(Ceff,_YTrain[row]);
  }
  else
  {
    fullMatrixOperation::reduceMat99ToMat66(Ceff,_YTrain[row]);
  }
};

void TrainingDeepMaterialNetworkLinearElastic::setTestSample(int row, DeepMaterialNetwork& C1, const fullMatrix<double>& Ceff)
{
  _XTest[row] = &C1;
  if (_interaction->getDimension() == 2)
  {
    fullMatrixOperation::reducePlaneStrain(Ceff,_YTest[row]);
  }
  else
  {
    fullMatrixOperation::reduceMat99ToMat66(Ceff,_YTest[row]);
  }
};

void TrainingDeepMaterialNetworkLinearElastic::testHomogenizationProcess(DeepMaterialNetwork* C1, fullMatrix<double>& Ceff)
{
  homogenizationProcess(C1,Ceff);
  Ceff.print("Ceff");
};


void TrainingDeepMaterialNetworkLinearElastic::computeErrorVector(std::vector<double>& allErors, const std::vector<LossMeasure*>& allLosses, 
      DeepMaterialNetwork* Cin, const fullMatrix<double>& Ceff)
{
  int N = allLosses.size();
  if (N == 0)
  {
    allErors.clear();
    return;
  }
  
  allErors.resize(N);
  for (int i=0; i< N; i++)
  {
    allErors[i] =0.; 
  }
  
  //
  static fullMatrix<double> Chomo;
  static std::map<Dof, fullMatrix<double> > DChomoDunknown;
  bool ok = homogenizationProcess(Cin,Chomo,false,&DChomoDunknown);
  for (int i=0; i< N; i++)
  {
    if (!ok) 
    {
      allErors[i] = sqrt(-1.);
      return;
    }
    else
    {
      allErors[i] = allLosses[i]->get(Ceff,Chomo);
    }
  }
};

double TrainingDeepMaterialNetworkLinearElastic::computeError(const LossMeasure& loss, DeepMaterialNetwork* Cin, const fullMatrix<double>& Ceff, 
                      bool stiff, fullVector<double>* g)
{
  //
  static fullMatrix<double> Chomo;
  static std::map<Dof, fullMatrix<double> > DChomoDunknown;
  bool ok = homogenizationProcess(Cin,Chomo,stiff,&DChomoDunknown);
  if (!ok) return sqrt(-1.);
  //
  if (stiff)
  {
    double Ndof = _unknown.size();
    if (g->size() != Ndof)
    {
      g->resize(Ndof,true);
    }
    else
    {
      g->setAll(0.);
    }
    
    static fullMatrix<double> DnormDDCel;
    double error = loss.get(Ceff,Chomo,stiff,&DnormDDCel);
    for (std::map<Dof, fullMatrix<double> >::const_iterator itD = DChomoDunknown.begin(); itD != DChomoDunknown.end(); itD++)
    {
      std::map<Dof,int>::const_iterator itF = _unknown.find(itD->first);
      if (itF != _unknown.end())
      {
        (*g)(itF->second) = fullMatrixOperation::dot(DnormDDCel,itD->second); 
      }
      else
      {
        Msg::Error("Dof not found");
      }
    }
    
    return error;
  }
  else
  {
    return loss.get(Ceff,Chomo);
  }
};

int TrainingDeepMaterialNetworkLinearElastic::sizeTrainingSet() const
{
  return _XTrain.size();
}
// allow to estimate loss function and derivatives with respect to the fitting paramater
double TrainingDeepMaterialNetworkLinearElastic::computeCostFunction(const std::string& metric, const std::vector<int>& sampleIndex, fullVector<double>& grad)
{
  LossMeasure* loss = LossMeasure::createLossMeasure(metric.c_str());
  double val = computeCostFunction(*loss,sampleIndex,grad);
  delete loss;
  return val;
}
// loss function in whole training set
void TrainingDeepMaterialNetworkLinearElastic::evaluateTrainingSet(const std::vector<std::string>& metrics, std::vector<double>& vals)
{
  int N = metrics.size();
  if (N == 0)
  {
    vals.clear();
    return;
  }
  vals.resize(N);
  for (int i=0; i< N; i++)
  {
    vals[i] = 0.;
  }
  int Ntrain = _XTrain.size();
  if (Ntrain == 0)
  {
    return;
  }
  std::vector<LossMeasure*> allLosses(N,NULL);
  for (int i=0; i< N; i++)
  {
    allLosses[i] = LossMeasure::createLossMeasure(metrics[i].c_str());
  }
  
  for (int i=0; i< Ntrain; i++)
  {
    static std::vector<double> allErrors; 
    computeErrorVector(allErrors, allLosses,_XTrain[i],_YTrain[i]);
    for (int j=0; j< N; j++)
    {
      vals[j] += allErrors[j]/(double)Ntrain;
    }
  }
  for (int i=0; i< N; i++)
  {
    delete allLosses[i];
  };
};
// loss function in test set
void TrainingDeepMaterialNetworkLinearElastic::evaluateTestSet(const std::vector<std::string>& metrics, std::vector<double>& vals)
{
  int N = metrics.size();
  if (N == 0)
  {
    vals.clear();
    return;
  }
  vals.resize(N);
  for (int i=0; i< N; i++)
  {
    vals[i] = 0.;
  }
  int Ntest = _XTest.size();
  if (Ntest == 0)
  {
    return;
  }
  std::vector<LossMeasure*> allLosses(N,NULL);
  for (int i=0; i< N; i++)
  {
    allLosses[i] = LossMeasure::createLossMeasure(metrics[i].c_str());
  }
  
  for (int i=0; i< Ntest; i++)
  {
    static std::vector<double> allErrors; 
    computeErrorVector(allErrors, allLosses,_XTest[i],_YTest[i]);
    for (int j=0; j< N; j++)
    {
      vals[j] += allErrors[j]/(double)Ntest;
    }
  }
  for (int i=0; i< N; i++)
  {
    delete allLosses[i];
  };
}
// save model
void TrainingDeepMaterialNetworkLinearElastic::saveModel(const std::string fname) const
{
  _interaction->saveDataToFile(fname);
}
// start numbering fitting parameters
void TrainingDeepMaterialNetworkLinearElastic::initializeFittingParameters(fullVector<double>& W)
{
  numberDofs(W);
};

// get bounds of fitting parmaters
void TrainingDeepMaterialNetworkLinearElastic::getUnknownBounds(fullVector<double>& WLower, fullVector<double>& WUpper) const
{
  Msg::Error("TrainingDeepMaterialNetworkLinearElastic::getUnknownBounds has not been implemeneted");
  Msg::Exit(1);
};

// update the model with the new fitting parameter
void TrainingDeepMaterialNetworkLinearElastic::updateModelFromFittingParameters(const fullVector<double>& W)
{
  updateFieldFromUnknown(W);
}
//  in case of fails
void TrainingDeepMaterialNetworkLinearElastic::reinitializeModel()
{
  _interaction->reInitialize();
}