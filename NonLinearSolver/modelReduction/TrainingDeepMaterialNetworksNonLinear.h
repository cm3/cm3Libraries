//
// C++ Interface: training  deep material networks
//
//
// Author:  <V.-D. Nguyen>, (C) 2021
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef _TRAININGDEEPMATERIALNETWORKSNONLINEAR_H_
#define _TRAININGDEEPMATERIALNETWORKSNONLINEAR_H_

#ifndef SWIG
#include "DeepMaterialNetworks.h"
#include "TrainingArbitraryDeepMaterialNetworks.h"
#endif //SWIG

class PathDouble;
class PathSTensor3
{
  #ifndef SWIG
  protected:
    int trueSize;
    std::vector<STensor3> value;
    std::vector<std::vector<STensor3> > DvalueDWeight;
    std::vector<std::vector<STensor3> > DvalueDAlpha;
    std::vector<std::vector<STensor3> > DvalueDNormal;
    
  public:
    PathSTensor3& operator = (const PathSTensor3& src);
    STensor3& getValue(int step);
    const STensor3& getValue(int step) const;
    std::vector<STensor3>& getDValueDWeight(int step);
    const std::vector<STensor3>& getDValueDWeight(int step) const;
    std::vector<STensor3>& getDValueDCoefficient(int step);
    const std::vector<STensor3>& getDValueDCoefficient(int step) const;
    std::vector<STensor3>& getDValueDNormal(int step);
    const std::vector<STensor3>& getDValueDNormal(int step) const;
  #endif //SWIG
  public:
    PathSTensor3(int N=0);
    PathSTensor3(const PathSTensor3& src);
    virtual ~PathSTensor3();
    void allocate(int N);
    void allocateWithDerivatives(int N, int sizeW, int sizeAlpha, int sizeNormal);
    int size() const;
    void clear();
    void setVal(int step, int i, int j, double val);
    double getValue(int step, int i, int j) const;
    void fromFile(const std::string filename);
    void toFile(const std::string filename, int num) const;
    void copyTo(PathSTensor3& dest, int space=1) const;
    PathDouble determinant() const;
};


class PathDouble
{
  #ifndef SWIG
  protected:
    int trueSize;
    std::vector<double> value;
    std::vector<std::vector<double> > DvalueDWeight;
    std::vector<std::vector<double> > DvalueDAlpha;
    std::vector<std::vector<double> > DvalueDNormal;
    
  public:
    PathDouble& operator = (const PathDouble& src);
    double& getValue(int step);
    std::vector<double>& getDValueDWeight(int step);
    const std::vector<double>& getDValueDWeight(int step) const;
    std::vector<double>& getDValueDCoefficient(int step);
    const std::vector<double>& getDValueDCoefficient(int step) const;
    std::vector<double>& getDValueDNormal(int step);
    const std::vector<double>& getDValueDNormal(int step) const;
  #endif //SWIG
  public:
    PathDouble(int N=0);
    PathDouble(const PathDouble& src);
    virtual ~PathDouble();
    void allocate(int N);
    void allocateWithDerivatives(int N, int sizeW, int sizeAlpha, int sizeNormal);
    int size() const;
    void clear();
    void addValue(double v);
    double getValue(int step) const;
    void setVal(int step, double val);
    void fromFile(const std::string filename);
    void toFile(const std::string filename, int num) const;
    void copyTo(PathDouble& dest, int space=1) const;
};

class LossPathMeasure
{
  public:
    #ifndef SWIG
    LossPathMeasure(){}
    virtual ~LossPathMeasure(){}
    virtual double get(const PathSTensor3& Pref, const PathSTensor3& P, int N, bool stiff = false, PathSTensor3* DerrorDP=NULL) const = 0;
    virtual double get(const PathDouble& Pref, const PathDouble& P, int N, bool stiff = false, PathDouble* DerrorDP=NULL) const = 0;
     #endif //SWIG
    static LossPathMeasure* createLossMeasure(const char what[]);
};

class SquareLossPathMeasure : public LossPathMeasure
{
  public:
    SquareLossPathMeasure(){}
    virtual ~SquareLossPathMeasure(){}
    virtual double get(const PathSTensor3& Pref, const PathSTensor3& P, int N, bool stiff = false, PathSTensor3* DerrorDP=NULL) const;
    virtual double get(const PathDouble& Pref, const PathDouble& P, int N, bool stiff = false, PathDouble* DerrorDP=NULL) const;
};

class RelativeSquareLossPathMeasure : public LossPathMeasure
{
  public:
    RelativeSquareLossPathMeasure(){}
    virtual ~RelativeSquareLossPathMeasure(){}
    virtual double get(const PathSTensor3& Pref, const PathSTensor3& P, int N, bool stiff = false, PathSTensor3* DerrorDP=NULL) const;
    virtual double get(const PathDouble& Pref, const PathDouble& P, int N, bool stiff = false, PathDouble* DerrorDP=NULL) const;
};


class AbsoluteLossPathMeasure : public LossPathMeasure
{
  public:
    AbsoluteLossPathMeasure(){}
    virtual ~AbsoluteLossPathMeasure(){}
    virtual double get(const PathSTensor3& Pref, const PathSTensor3& P, int N, bool stiff = false, PathSTensor3* DerrorDP=NULL) const;
    virtual double get(const PathDouble& Pref, const PathDouble& P, int N, bool stiff = false, PathDouble* DerrorDP=NULL) const;
};

class RelativeAbsoluteLossPathMeasure : public LossPathMeasure
{
  public:
    RelativeAbsoluteLossPathMeasure(){}
    virtual ~RelativeAbsoluteLossPathMeasure(){}
    virtual double get(const PathSTensor3& Pref, const PathSTensor3& P, int N, bool stiff = false, PathSTensor3* DerrorDP=NULL) const;
    virtual double get(const PathDouble& Pref, const PathDouble& P, int N, bool stiff = false, PathDouble* DerrorDP=NULL) const;
};


/*! \brief Training material networks with nonlinear paths
*/
class TrainingDeepMaterialNetworkNonLinear : public TrainingArbitraryDeepMaterialNetwork 
{
  #ifndef SWIG
  protected:
    DeepMaterialNetwork* _DMN; ///< deep material network as estimator    
    // maximal strain step
    double _maximumStrainStep; ///< allowable strain step when using DMN
    bool _trainingWithPhaseDeformation; ///< activate training with the per-phase average deformation
    int _phaseIndexForTrainingPhaseDeformation;; ///< phase index for training when using the per-phase average deformation
    double _mixedFactor, _mixedFactorIPComp; ///< factor to combine different norm
    std::vector<int> _phaseIndexForIpComForTraining; ///< phase index for training when using an internal variable
    std::vector<IPField::Output> _phaseIpComForTraining; ///< internal variable specification
    std::vector<double> _ipOffsets; ///< offset value for internal variable specification
    // offline data
    std::vector<PathSTensor3> _XTrain; ///< strain path in the training dataset
    std::vector<PathSTensor3> _YTrain; ///< stress path in the training dataset
    std::vector<PathSTensor3> _YTrainPhaseDefo; ///< per-phase average strain path in the training dataset
    std::vector<std::vector<PathDouble> >_YTrainPhaseIPComp; ///< internal variable path in the training dataset, row- sampleIndex
    std::vector<PathSTensor3>_XTest; ///< strain path in the testing dataset
    std::vector<PathSTensor3> _YTest; ///< stress path in the testing dataset
    std::vector<PathSTensor3> _YTestPhaseDefo; ///< per-phase average strain path in testing dataset
    std::vector<std::vector<PathDouble> >_YTestPhaseIPComp; ///< internal variable path in testing dataset , row- sampleIndex
  protected:
    void assembleOneDof(const Dof& key, double val, fullVector<double>& grad) const;
    
    
  #endif //SWIG
  public:
    TrainingDeepMaterialNetworkNonLinear(DeepMaterialNetwork& dmn, const CoefficientReduction& reduction);
    virtual ~TrainingDeepMaterialNetworkNonLinear();
    /*! \brief set the maximum strain step for the DMN evaluation
		@param[in] val maximum step value
	 */
    void setMaximumStrainStep(double val);
    
    /*! \brief set number of samples in the training dataset 
    */
    void trainingDataSize(int Ns);
    
    /*! \brief activate the training with the per-phase average deformation
    @param[in] Ns number of samples
    @param[in] phaseIndex phase index to make the averaging operation
    @param[in] mixFactor factor to combine the corresponding error norm
    */
    void trainingDataSizeWithPhaseDeformation(int Ns, int phaseIndex, double mixFactor=1.);
    
    /*! \brief activate the training with a single internal variable
    @param[in] Ns number of samples
    @param[in] phaseIndex phase index to make the averaging operation
    @param[in] IPComp an internal variable defined by IPField::Output
    @param[in] mixFactor factor to combine the corresponding error norm
    */
    void trainingDataSizeWithPhaseIPComp(int Ns, int phaseIndex, int IPComp, double mixFactor=1., double offset=0);
    
    /*! \brief activate the training with two internal variables
    @param[in] Ns number of samples
    @param[in] phaseIndex phase index to make the averaging operation
    @param[in] IPComp0 an internal variable defined by IPField::Output
    @param[in] IPComp1 an internal variable defined by IPField::Output
    @param[in] mixFactor factor to combine the corresponding error norm
    */
    void trainingDataSizeWithTwoPhaseIPComps(int Ns, int phaseIndex, int IPComp0, int IPComp1, double mixFactor=1., double offset0=0, double offset1=0);
    
    /*! \brief set training sample
    @param[in] row sample index
    @param[in] Fref strain path
    @param[in] Pref stress path
    @param[in] space slicing based on position on the path, values are taken after each "space" values
    */
    void setTrainingSample(int row, const PathSTensor3& Fref, const PathSTensor3& Pref, int space = 1);
    
    /*! \brief set training sample for the per-phase average path
    @param[in] row sample index
    @param[in] FphaseRef per-phase average strain
    @param[in] space slicing based on position on the path, values are taken after each "space" values
    */
    void setTrainingSamplePhaseDeformation(int row, const PathSTensor3& FphaseRef, int space = 1);
    
    /*! \brief set training sample for the internal variable
    @param[in] row sample index
    @param[in] IPCompRef internal variable path
    @param[in] ipLoc location of IP variable in _phaseIpComForTraining
    @param[in] space slicing based on position on the path, values are taken after each "space" values
    */
    void setTrainingSamplePhaseIPComp(int row, const PathDouble& IPCompRef, int ipLoc=0, int space = 1);
    //
    void testDataSize(int Ns);    
    void setTestSample(int row, const PathSTensor3& Fref, const PathSTensor3& Pref, int space = 1);
    void setTestSamplelePhaseDeformation(int row, const PathSTensor3& FphaseRef, int space = 1);
    void setTestSamplelePhaseIPComp(int row, const PathDouble& IPCompRef, int ipLoc=0,  int space = 1);
    //
    #ifndef SWIG
    /*! \brief evaluate DMN response based on a strain path
		@param[in] strainPath strain path as input
    @param[out] stressPath stress path as output
    @param[in] messageActive true if the convergence history is printed in the terminal
    @param[in] stiff true if the the tangent operator is estimated in the DMN
    @param[in] extractPhaseDefo true if the per-phase average deformation is estimated
    @param[out] phaseDefoPath the extracted per-phase average deformation path when extractPhaseDefo = true
    @param[in] extractIPComp true if the internal variable is extracted
    @param[out] IpCompPath the extracted internal variable path when extractIPComp = true
    @return number of the last sucessfuly iteration index at which the simulation converges
	 */
    int evaluatePath(const PathSTensor3& strainPath, PathSTensor3& stressPath, bool messageActive, bool stiff=false, 
                          bool extractPhaseDefo=false, PathSTensor3* phaseDefoPath=NULL,
                          bool extractIPComp = false, std::vector<PathDouble>* IpCompPath=NULL, PathDouble* timeSteps=NULL,
                          int maximumReducedSteps=15, int maxIter=100, int maxSubSteps=100, 
                          double tol =1e-6, double absTol=1e-8);
    void computeErrorVec(std::vector<fullVector<double> >& allErrors, const std::vector<LossPathMeasure*>& lossMeasures, const PathSTensor3& Fref, const PathSTensor3& Pref, 
                                bool extractPhaseDefo=false, PathSTensor3* phaseDefoPathRef=NULL,
                                bool extractIPComp = false, std::vector<PathDouble>* IpCompPathRef=NULL);
    double getEquivalentError(const fullVector<double>& allErr) const;
    double computeError(const LossPathMeasure& lossMeasure, const PathSTensor3& Fref, const PathSTensor3& Pref, 
                        bool diff=false, fullVector<double>* grad=NULL, 
                        bool extractPhaseDefo=false, PathSTensor3* phaseDefoPathRef=NULL,
                        bool extractIPComp = false, std::vector<PathDouble>* IpCompPathRef=NULL);
    double computeCostFunction(const LossPathMeasure& loss, const std::vector<int>& sampleIndex, fullVector<double>& grad);
    double evaluateTrainingSet(const LossPathMeasure& loss);
    
    double evaluateTestSet(const LossPathMeasure& loss);
    #endif //SWIG
  public:
    virtual int sizeTrainingSet() const; 
    // allow to estimate loss function and derivatives with respect to the fitting paramater
    virtual double computeCostFunction(const std::string& metric, const std::vector<int>& sampleIndex, fullVector<double>& grad); 
    // loss function in whole training set
    virtual void evaluateTrainingSet(const std::vector<std::string>& metrics, std::vector<double>& vals);
    // loss function in test set
    virtual void evaluateTestSet(const std::vector<std::string>& metrics, std::vector<double>& vals);
    // save model
    virtual void saveModel(const std::string fname) const;
    // start numbering fitting parameters
    virtual void initializeFittingParameters(fullVector<double>& W); 
    // get bounds of fitting paraeters
    virtual void getUnknownBounds(fullVector<double>& WLower, fullVector<double>& WUpper) const;
    // update the model with the new fitting parameter
    virtual void updateModelFromFittingParameters(const fullVector<double>& W);
    //  in case of fails
    virtual void reinitializeModel();
    
};


int evaluatePath(DeepMaterialNetwork* dmn, double maximumStrainStep, const PathSTensor3& strainPath, PathSTensor3& stressPath, PathDouble* timeSteps=NULL);

#endif //_TRAININGDEEPMATERIALNETWORKSNONLINEAR_H_ 