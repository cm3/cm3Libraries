//
// C++ Interface: binary tree
//
//
// Author:  <V.-D. Nguyen>, (C) 2020
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "DeepMaterialNetworks.h"
#include "TrainingDeepMaterialNetworks.h"
#include <stdio.h>
#include <random>
#include <chrono>
#include <math.h>
#include "OS.h"
#include "ipFiniteStrain.h"
#include "TrainingDeepMaterialNetworksNonLinear.h"
#if defined(HAVE_PETSC)
#include "nonLinearSystemPETSc.h"
#elif defined(HAVE_GMM)
#include "nonLinearSystemGmm.h"
#endif //HAVE_GMM
#include <sys/stat.h>

DeepMaterialNetwork::DeepMaterialNetwork(int tag): _isInitialized(false),  _tag(tag),
_ownInteractionData(false),_networkInteraction(NULL),_enumMinus(-1),_enumPlus(-1), _gnum(0), _lineSearch(false),
_saveDir("")
#if defined(HAVE_PETSC)
,_isAddedSparsityPattern(false)
#endif //HAVE_PETSC
{
  #if defined(HAVE_PETSC) || defined(HAVE_GMM)
  _sys = NULL;
  #endif //
};

DeepMaterialNetwork::DeepMaterialNetwork(const Tree& T, int tag): _isInitialized(false), _tag(tag), _ownInteractionData(true),  _lineSearch(false),
_enumMinus(-1),_enumPlus(-1), _gnum(0), _saveDir("")
#if defined(HAVE_PETSC)
,_isAddedSparsityPattern(false)
#endif //HAVE_PETSC
{
  #if defined(HAVE_PETSC) || defined(HAVE_GMM)
  _sys = NULL;
  #endif //
  _networkInteraction  = new NetworkInteraction();
  _networkInteraction->getInteractionFromTree(T);
};

DeepMaterialNetwork::DeepMaterialNetwork(const std::string fname, int tag ):_isInitialized(false), _tag(tag), _ownInteractionData(true),  _lineSearch(false),
_enumMinus(-1),_enumPlus(-1), _gnum(0), _saveDir("")
#if defined(HAVE_PETSC)
,_isAddedSparsityPattern(false)
#endif //HAVE_PETSC
{
  #if defined(HAVE_PETSC) || defined(HAVE_GMM)
  _sys = NULL;
  #endif //
  _networkInteraction  = new NetworkInteraction();
  _networkInteraction->loadDataFromFile(fname);
};

DeepMaterialNetwork::~DeepMaterialNetwork()
{
  #if defined(HAVE_PETSC) || defined(HAVE_GMM)
  if (_sys != NULL)
  {
    delete _sys;
  }
  #endif //

  if (_ownInteractionData)
  {
    _ownInteractionData = false;
    delete _networkInteraction;
    _networkInteraction = NULL;
  }
};

void DeepMaterialNetwork::copyViewSetting(const DeepMaterialNetwork& src)
{
  _viewIP = src._viewIP;
  _averagePhase = src._averagePhase;
  _saveDir = src._saveDir;
};

void DeepMaterialNetwork::setMicroProblemIndentification(int ele, int gpt){
	_enumMinus = ele;
  _enumPlus = ele;
	_gnum = gpt;
};

void DeepMaterialNetwork::setMicroProblemIndentification(int eleMinus, int elePlus, int gpt){
	_enumMinus = eleMinus;
  _enumPlus = elePlus;
	_gnum = gpt;
};

void DeepMaterialNetwork::setSaveDir(std::string saveDir)
{
  Msg::Info("result will be saved in %s",saveDir.c_str());
  _saveDir = saveDir;  
}

int DeepMaterialNetwork::evaluateByPath(double maximumStrainStep, const PathSTensor3& strainPath, PathSTensor3& stressPath,  PathDouble* timeSteps)
{
  return evaluatePath(this, maximumStrainStep, strainPath, stressPath, timeSteps);
};


std::string DeepMaterialNetwork::getFileSavingPrefix() const
{
  if (_saveDir.size() >0)
  {
    struct stat st;
    if(stat(_saveDir.c_str(),&st) != 0)
    {
      std::string mdir= "mkdir " + _saveDir;
      int oks = system(mdir.c_str());
      Msg::Info("directory %s is created",_saveDir.c_str());
    }
  }
  
  if ((_enumMinus != -1) && (_enumPlus != -1))
  {
    if (_enumMinus == _enumPlus)
    {
      if (_saveDir.size() ==0)
        return "E_"+int2str(_enumMinus)+"_GP_"+int2str(_gnum)+"_";
      else
        return _saveDir+"/E_"+int2str(_enumMinus)+"_GP_"+int2str(_gnum)+"_";
    }
    else
    {
      if (_saveDir.size() ==0)
        return "E_Interface_"+int2str(_enumMinus)+"_"+int2str(_enumPlus)+"_GP_"+int2str(_gnum)+"_";
      else
         return _saveDir+"/E_Interface_"+int2str(_enumMinus)+"_"+int2str(_enumPlus)+"_GP_"+int2str(_gnum)+"_";
    }
  }
  else
  {
    if (_saveDir.size() ==0)
      return "";
    else
      return _saveDir+"/";
  }
};

void DeepMaterialNetwork::setLineSearch(bool flg)
{
  _lineSearch = flg;
}

void DeepMaterialNetwork::addInteraction(NetworkInteraction& inter)
{
  #if defined(HAVE_PETSC)
  _isAddedSparsityPattern = false;
  #endif //HAVE_PETSC
  //
  #if defined(HAVE_PETSC) || defined(HAVE_GMM)
  if (_sys!=NULL) delete _sys;
  _sys = NULL;
  #endif //

  if (_ownInteractionData)
  {
    delete _networkInteraction;
  }
  // create interaction
  _ownInteractionData = false;
  _networkInteraction = &inter;
  _isInitialized = false;
};


void DeepMaterialNetwork::addInteractionFromTree(const Tree& T)
{
  #if defined(HAVE_PETSC)
  _isAddedSparsityPattern = false;
  #endif //HAVE_PETSC
  //
  #if defined(HAVE_PETSC) || defined(HAVE_GMM)
  if (_sys!=NULL) delete _sys;
  _sys = NULL;
  #endif //

  // create interaction

  if (_networkInteraction == NULL)
  {
    _ownInteractionData = true;
    _networkInteraction = new NetworkInteraction();
  }
  else
  {
    _networkInteraction->clearData();
  }
  _networkInteraction->getInteractionFromTree(T);
  _isInitialized = false;
};

void DeepMaterialNetwork::reset()
{
  if (_sys != NULL)
  {
    _sys->copy(IPStateBase::initial,IPStateBase::current);
    _sys->copy(IPStateBase::initial,IPStateBase::previous);
  }
  
  _networkInteraction->resetIncompatibleDispVector();
  //
  NetworkInteraction::MaterialNodeContainer& allNodes = _networkInteraction->getMaterialNodeContainer();
  for (NetworkInteraction::MaterialNodeContainer::iterator it =allNodes.begin(); it!= allNodes.end(); it++)
  {
    MaterialNode* node = it->second;
    if (node->ips != NULL)
    {
      IPVariable *ipvi= node->ips->getState(IPStateBase::initial);
      IPVariable *ipv1= node->ips->getState(IPStateBase::previous);
      IPVariable *ipv2= node->ips->getState(IPStateBase::current);
      *ipv2 = *ipvi;
      *ipv1 = *ipvi;
    }
  };
};

void DeepMaterialNetwork::printPhaseFraction() const
{
  std::map<int, double> allPhase;
  _networkInteraction->getPhaseFractions(allPhase);
  Msg::Info("phase fractions:");
  for (std::map<int, double>::const_iterator it = allPhase.begin(); it != allPhase.end(); it++)
  {
    Msg::Info("phase %d fraction %.16g",it->first,it->second);
  }
}

void DeepMaterialNetwork::saveInteractionToFile(const std::string fname) const
{
  if (_networkInteraction != NULL)
  {
    _networkInteraction->saveDataToFile(fname);
  }
  else
  {
    Msg::Error("interactions does not exist !!!");
  }
}

void DeepMaterialNetwork::addInteractionFromFile(const std::string fname)
{
  #if defined(HAVE_PETSC)
  _isAddedSparsityPattern = false;
  #endif //HAVE_PETSC
  //
  #if defined(HAVE_PETSC) || defined(HAVE_GMM)
  if (_sys!=NULL) delete _sys;
  _sys = NULL;
  #endif //

  // create interaction
  if (_networkInteraction == NULL)
  {
    _ownInteractionData = true;
    _networkInteraction = new NetworkInteraction();
  }
  else
  {
    _networkInteraction->clearData();
  }
  _networkInteraction->loadDataFromFile(fname);
  _isInitialized = false;
};

void DeepMaterialNetwork::viewIPAveragePerPhase(int iphase, int icomp, int eleVal)
{
  _averagePhase.emplace_back(iphase,(IPField::Output)icomp,(nlsField::ElemValue)eleVal);
};
void DeepMaterialNetwork::viewIPAverage(int icomp, int eleVal)
{
  _averagePhase.emplace_back(-1,(IPField::Output)icomp,(nlsField::ElemValue)eleVal);
};


void DeepMaterialNetwork::viewIP(int i, int eleVal)
{
  _viewIP.push_back(std::pair<int,int>(i,eleVal));
}

double DeepMaterialNetwork::get(int comp) const
{
  // get average value
  return _networkInteraction->get(comp,nlsField::mean);
};

double DeepMaterialNetwork::getPerPhase(int phaseIndex, int comp) const
{
  return _networkInteraction->getPerPhase(phaseIndex,comp,nlsField::mean);
}

void DeepMaterialNetwork::printDeformationGradientNodes() const
{
  const NetworkInteraction::MaterialNodeContainer& allNodes = _networkInteraction->getMaterialNodeContainer();
  for (NetworkInteraction::MaterialNodeContainer::const_iterator it = allNodes.begin(); it != allNodes.end(); it++)
  {
    MaterialNode* node = it->second;
    //
    if (node->ips != NULL)
    {
      ipFiniteStrain* ipvf = dynamic_cast<ipFiniteStrain*>(node->ips->getState(IPStateBase::current));
      if (ipvf != NULL)
      {
        const STensor3& F = ipvf->getConstRefToDeformationGradient();
        printf("node = %d",node->numberIndex);
        F.print("deformation gradient");
      };
    }
  };
}

void DeepMaterialNetwork::saveIPDataToFile(int step) const
{
  NetworkInteraction::MaterialNodeContainer& allNodes = _networkInteraction->getMaterialNodeContainer();
  for (int iv=0; iv < _viewIP.size(); iv++)
  {
    int i = _viewIP[iv].first;
    int eleVal = _viewIP[iv].second;
    if (eleVal == nlsField::crude)
    {
      std::string filename = getFileSavingPrefix()+IPField::ToString(i)+".csv";
      FILE * file = NULL;
      if (step == 1)
      {
        file = fopen(filename.c_str(),"w");
        fprintf(file,"STEP");
        for (NetworkInteraction::MaterialNodeContainer::const_iterator it =allNodes.begin(); it!= allNodes.end(); it++)
        {
          MaterialNode* node = it->second;
          fprintf(file," NODE%d",node->numberIndex);
        }
        fflush(file);
      }
      else
      {
        file = fopen(filename.c_str(),"a");
      }
      if (file !=NULL)
      {
        fprintf(file,"\n%d",step);
        for (NetworkInteraction::MaterialNodeContainer::const_iterator it =allNodes.begin(); it!= allNodes.end(); it++)
        {
          MaterialNode* node = it->second;
          if (node->ips != NULL)
          {
            const IPVariable* ipv = node->ips->getState(IPStateBase::current);
            fprintf(file," %.16g",ipv->get(i));
          }
          else
          {
            fprintf(file," %.16g",0.);
          }
        }
        fflush(file);
        // val
        fclose(file);
      }
      else
      {
        Msg::Error("file %s cannot open to write",filename);
      }
    }
    else if (eleVal == nlsField::max || eleVal == nlsField::min || eleVal == nlsField::mean)
    {
      std::string filename;
      if (eleVal == nlsField::max)
        filename = getFileSavingPrefix()+IPField::ToString(i)+"_Max.csv";
      else if (eleVal == nlsField::min)
        filename = getFileSavingPrefix()+IPField::ToString(i)+"_Min.csv";
      else
        filename = getFileSavingPrefix()+IPField::ToString(i)+"_Mean.csv";
      FILE * file = NULL;
      if (step == 1)
      {
        file = fopen(filename.c_str(),"w");
        fprintf(file,"STEP Val");
        fflush(file);
      }
      else
      {
        file = fopen(filename.c_str(),"a");
      }
      if (file !=NULL)
      {
        fprintf(file,"\n%d %.16g",step,_networkInteraction->get(i,eleVal));
        fflush(file);
        // val
        fclose(file);
      }
      else
      {
        Msg::Error("file %s cannot open to write",filename);
      }
    }
    else
    {
      Msg::Error("element val = %d is not defined",eleVal);
    }
  }
  //
  for (int iv = 0; iv< _averagePhase.size(); iv++)
  {
    int phase = _averagePhase[iv].phaseIndex;
    IPField::Output ic = _averagePhase[iv].ipComp;
    nlsField::ElemValue eleVal = _averagePhase[iv].eleVal;
    if (eleVal == nlsField::max || eleVal == nlsField::min || eleVal == nlsField::mean)
    {
      std::string filename;
      if (phase == -1)
      {
        // over all material nodes
        if (eleVal == nlsField::max)
          filename = getFileSavingPrefix()+IPField::ToString(ic)+"_Max.csv";
        else if (eleVal == nlsField::min)
          filename = getFileSavingPrefix()+IPField::ToString(ic)+"_Min.csv";
        else
          filename = getFileSavingPrefix()+IPField::ToString(ic)+"_Mean.csv";
      }
      else
      {
        if (eleVal == nlsField::max)
          filename = getFileSavingPrefix()+IPField::ToString(ic)+"_Phase"+std::to_string(phase)+"_Max.csv";
        else if (eleVal == nlsField::min)
          filename = getFileSavingPrefix()+IPField::ToString(ic)+"_Phase"+std::to_string(phase)+"_Min.csv";
        else
          filename = getFileSavingPrefix()+IPField::ToString(ic)+"_Phase"+std::to_string(phase)+"_Mean.csv";
      }
      FILE * file = NULL;
      if (step == 1)
      {
        file = fopen(filename.c_str(),"w");
        fprintf(file,"STEP Val");
        fflush(file);
      }
      else
      {
        file = fopen(filename.c_str(),"a");
      }
      if (file !=NULL)
      {
        if (phase == -1)
        {
          fprintf(file,"\n%d %.16g",step,_networkInteraction->get(ic,eleVal));
        }
        else
        {
          fprintf(file,"\n%d %.16g",step,_networkInteraction->getPerPhase(phase,ic,eleVal));
        }
        
        fflush(file);
        // val
        fclose(file);
      }
      else
      {
        Msg::Error("file %s cannot open to write",filename);
      }
    }
    else
    {
      Msg::Error("element val = %d is not defined",eleVal);
    }
  }
};

void DeepMaterialNetwork::allocateIPStateMaterialNodes()
{
  //printf("creating ipvariables: number material nodes = %d \n",_networkInteraction->getNumberOfMaterialNodes());
  NetworkInteraction::MaterialNodeContainer& allNodes = _networkInteraction->getMaterialNodeContainer();
  for (NetworkInteraction::MaterialNodeContainer::iterator it =allNodes.begin(); it!= allNodes.end(); it++)
  {
    MaterialNode* node = it->second;
    // create ipvs and initialize
    createIPState(node);
  };
  //printf("done creating ipvariables\n");
};

void DeepMaterialNetwork::getInterfaceKeys(const MaterialNode* node, std::vector<Dof>& keys) const
{
  if (node->withCohesive())
  {
    int nCoh = node->cohesiveCoeffVars.size();
    for (int i=0; i<3; i++)
    {
      for (int j=0; j< nCoh; j++)
      {
        keys.push_back(Dof(node->getNodeId(), Dof::createTypeWithTwoInts(i,_tag+j+1)));
      }
    };  
  }
};

void DeepMaterialNetwork::getKeys(const InteractionMechanism* im, std::vector<Dof>& keys) const
{
  for (int i=0; i<3; i++)
  {
    keys.push_back(Dof(im->getIndex(), Dof::createTypeWithTwoInts(i,_tag)));
  };
};

void DeepMaterialNetwork::numberDof()
{
  _unknown.clear();
  _fixedDof.clear();
  //
  const NetworkInteraction::MaterialNodeContainer& allMaterialNodes = _networkInteraction->getMaterialNodeContainer();
  for (NetworkInteraction::MaterialNodeContainer::const_iterator itnode = allMaterialNodes.begin(); itnode != allMaterialNodes.end(); itnode++)
  {
    const MaterialNode* node = itnode->second;
    if (node->withCohesive())
    {
      int nCoh = node->cohesiveCoeffVars.size();
      for (int j=0; j< _fixedComp.size(); j++)
      {
        for (int k=0; k<nCoh; k++)
        {
          _fixedDof.insert(Dof(node->getNodeId(), Dof::createTypeWithTwoInts(_fixedComp[j], _tag+k+1)));
        }
      }      
    }
  };

  const NetworkInteraction::InteractionContainer& allInteractions = _networkInteraction->getInteractionContainer();
  for (NetworkInteraction::InteractionContainer::const_iterator it = allInteractions.begin(); it != allInteractions.end(); it++)
  {
    const InteractionMechanism* im = it->second;;
    for (int j=0; j< _fixedComp.size(); j++)
    {
      _fixedDof.insert(Dof(im->getIndex(), Dof::createTypeWithTwoInts(_fixedComp[j], _tag)));
    }
    if (im->isTrivial())
    {
      std::vector<Dof> R;
      getKeys(im,R);
      for (int iR=0; iR < R.size(); iR++)
      {
        _fixedDof.insert( R[iR]);
      }
    }
  }
  //
  for (NetworkInteraction::InteractionContainer::const_iterator it = allInteractions.begin(); it != allInteractions.end(); it++)
  {
    const InteractionMechanism* im = it->second;;
    std::vector<Dof> R;
    getKeys(im,R);
    for (int j=0; j< R.size(); j++)
    {
      Dof& key = R[j];
      if (_fixedDof.find(key) == _fixedDof.end())
      {
        if(_unknown.find(key) == _unknown.end())
        {
          std::size_t size = _unknown.size();
          _unknown[key] = size;
        }
      }
    }
  }
  // interface keys
  for (NetworkInteraction::MaterialNodeContainer::const_iterator itnode = allMaterialNodes.begin(); itnode != allMaterialNodes.end(); itnode++)
  {
    const MaterialNode* node = itnode->second;
    if (node->withCohesive())
    {
      std::vector<Dof> R;
      getInterfaceKeys(node,R);
      for (int j=0; j< R.size(); j++)
      {
        Dof& key = R[j];
        if (_fixedDof.find(key) == _fixedDof.end())
        {
          if(_unknown.find(key) == _unknown.end())
          {
            std::size_t size = _unknown.size();
            _unknown[key] = size;
          }
        }
      }
    }
  };
};


void DeepMaterialNetwork::getStress(STensor3& P) const
{
  _networkInteraction->getStress(P);
};

void DeepMaterialNetwork::getDeformationGradientPhase(int phaseIndex, STensor3& F) const
{
  _networkInteraction->getDeformationGradientPhase(phaseIndex,F);
}

void DeepMaterialNetwork::computeDUnknownDFittingParameters(const CoefficientReduction& coeffReduc, bool fixMaterialNodes)
{
  if (!fixMaterialNodes)
  {
    bool ok = computeDUnknownDWeights(coeffReduc, _DaDFittingParameters.allNodeIds, _DaDFittingParameters.DaDweights);
    if (!ok)
    {
      Msg::Error("computeDUnknownDWeights  cannot be computed in DeepMaterialNetwork::computeDUnknownDFittingParameters");
      return;
    }
  }
  
  const NetworkInteraction::InteractionContainer& allInteraction = _networkInteraction->getInteractionContainer();
  for (NetworkInteraction::InteractionContainer::const_iterator iinter= allInteraction.begin(); iinter != allInteraction.end(); iinter++)
  {
    const InteractionMechanism* im = iinter->second; 
    fullMatrix<double>& DaDCoeffVars = _DaDFittingParameters.DaDCoeffVars[im->getIndex()];
    fullMatrix<double>& DaDnormalVars = _DaDFittingParameters.DaDnormalVars[im->getIndex()];
    bool ok = computeDUnknownDInteraction(coeffReduc, *im, DaDCoeffVars, DaDnormalVars);
    if (!ok)
    {
      Msg::Error("computeDUnknownDInteraction for mechanism %d cannot be computed in DeepMaterialNetwork::computeDUnknownDFittingParameters",im->getIndex());
      return;
    };
  };
};

void DeepMaterialNetwork::computeDIPCompDWeights(IPField::Output comp, int phaseIndex, const CoefficientReduction& coeffReduc, 
                      std::vector<int>& allNodeIds, fullMatrix<double>& DIPcompDW)
{
  const fullMatrix<double>& DaDW = _DaDFittingParameters.getDaDweights();
  allNodeIds = _DaDFittingParameters.getAllNodeIds();
  static fullMatrix<double> DIPCompDa;
  computeDIPCompDIncompatibleVector(comp,phaseIndex,DIPCompDa);
  
  if (DIPcompDW.size1() != 1 || DIPcompDW.size2() != allNodeIds.size())
  {
    DIPcompDW.resize(1, allNodeIds.size());
  }
  DIPCompDa.mult(DaDW,DIPcompDW);
  
  std::map<int,int> allNodeIdMapForAssembling;
  for (int i=0; i< allNodeIds.size(); i++)
  {
    allNodeIdMapForAssembling[allNodeIds[i]] = i;
  }
  double WT = _networkInteraction->getTotalWeightPhase(phaseIndex);
  std::vector<int> allNodeIdsPhase;
  _networkInteraction->getAllMaterialNodeIdsForMat(phaseIndex,allNodeIdsPhase);
  for (int i=0; i< allNodeIdsPhase.size(); i++)
  {
    const MaterialNode* node = _networkInteraction->getMaterialNode(allNodeIdsPhase[i]);
    double Wi = _networkInteraction->getWeight(node);
    double fi = Wi/WT;
    static double vali;
    static STensor3 DvalDFi;
    getDIPCompDFLocal(node->ips,comp,vali,DvalDFi);
    //const STensor3& Fi = _networkInteraction->getDeformationGradient(node);
    //
    for (int j=0; j< allNodeIdsPhase.size(); j++)
    {
      int col = allNodeIdMapForAssembling[allNodeIdsPhase[j]];
      double DfiDWj = 0;
      if (j==i)
      {
        DfiDWj = 1./WT - Wi/(WT*WT);
      }
      else
      {
        DfiDWj = - Wi/(WT*WT);
      }
      DIPcompDW(0,col) += DfiDWj*vali;
    };
    //
    static std::vector<STensor3> DFiDalpha;
    _networkInteraction->getDFDCoefficient(node,DFiDalpha);
    const std::vector<const InteractionMechanism*>* alInteractions =  _networkInteraction->getInteractionsForNode(node);
    for (int k=0; k< alInteractions->size(); k++)
    {
      double DvaliDalpha = STensorOperation::doubledot(DvalDFi,DFiDalpha[k]);
      
      const InteractionMechanism* im = (*alInteractions)[k];
      static std::vector<int> allNodes;
      static std::vector<double> DalphaDW;
      coeffReduc.getDCoefficientsDWeights(*_networkInteraction,*im, allNodeIdsPhase[i], allNodes, DalphaDW);
      for (int l= 0; l < allNodes.size(); l++)
      {
        int col = allNodeIdMapForAssembling[allNodes[l]];
        DIPcompDW(0,col) += fi*DvaliDalpha*DalphaDW[l];
      }
    }
  };
};                      

void DeepMaterialNetwork::computeDStrainDWeights(int phaseIndex, const CoefficientReduction& coeffReduc, std::vector<int>& allNodeIds, 
                        fullMatrix<double>& DFDW)
{
  const fullMatrix<double>& DaDW = _DaDFittingParameters.getDaDweights();
  allNodeIds = _DaDFittingParameters.getAllNodeIds();
  static fullMatrix<double> DFDa;
  computeDStrainDIncompatibleVector(phaseIndex,DFDa);
  
  if (DFDW.size1() != 9 || DFDW.size2() != allNodeIds.size())
  {
    DFDW.resize(9, allNodeIds.size());
  }
  DFDa.mult(DaDW,DFDW);
  
  std::map<int,int> allNodeIdMapForAssembling;
  for (int i=0; i< allNodeIds.size(); i++)
  {
    allNodeIdMapForAssembling[allNodeIds[i]] = i;
  }
  double WT = _networkInteraction->getTotalWeightPhase(phaseIndex);
  std::vector<int> allNodeIdsPhase;
  _networkInteraction->getAllMaterialNodeIdsForMat(phaseIndex,allNodeIdsPhase);
  for (int i=0; i< allNodeIdsPhase.size(); i++)
  {
    const MaterialNode* node = _networkInteraction->getMaterialNode(allNodeIdsPhase[i]);
    double Wi = _networkInteraction->getWeight(node);
    double fi = Wi/WT;
    const STensor3& Fi = _networkInteraction->getDeformationGradient(node);
    //
    for (int j=0; j< allNodeIdsPhase.size(); j++)
    {
      int col = allNodeIdMapForAssembling[allNodeIdsPhase[j]];
      double DfiDWj = 0;
      if (j==i)
      {
        DfiDWj = 1./WT - Wi/(WT*WT);
      }
      else
      {
        DfiDWj = - Wi/(WT*WT);
      }
      for (int p=0; p<3; p++)
      {
        for (int q=0; q<3; q++)
        {
          int row = Tensor23::getIndex(p,q);
          DFDW(row, col) += DfiDWj*Fi(p,q);
        }
      };
    };
    //
    static std::vector<STensor3> DFiDalpha;
    _networkInteraction->getDFDCoefficient(node,DFiDalpha);
    const std::vector<const InteractionMechanism*>* alInteractions =  _networkInteraction->getInteractionsForNode(node);
    for (int k=0; k< alInteractions->size(); k++)
    {
      const InteractionMechanism* im = (*alInteractions)[k];
      static std::vector<int> allNodes;
      static std::vector<double> DalphaDW;
      coeffReduc.getDCoefficientsDWeights(*_networkInteraction,*im, allNodeIdsPhase[i], allNodes, DalphaDW);
      for (int l= 0; l < allNodes.size(); l++)
      {
        int col = allNodeIdMapForAssembling[allNodes[l]];
        for (int p=0; p<3; p++)
        {
          for (int q=0; q<3; q++)
          {
            int row = Tensor23::getIndex(p,q);
            DFDW(row, col) += fi*DFiDalpha[k](p,q)*DalphaDW[l];
          }
        }
      }
    }
  };
};

void DeepMaterialNetwork::computeDStressDWeights(const CoefficientReduction& coeffReduc, std::vector<int>& allNodeIds, fullMatrix<double>& DPDDW)
{
  const fullMatrix<double>& DaDW = _DaDFittingParameters.getDaDweights();
  allNodeIds = _DaDFittingParameters.getAllNodeIds();
  static fullMatrix<double> DPDa;
  computeDStressDIncompatibleVector(DPDa);
  
  if (DPDDW.size1() != 9 || DPDDW.size2() != allNodeIds.size())
  {
    DPDDW.resize(9, allNodeIds.size());
  }
  DPDa.mult(DaDW,DPDDW);
  
  std::map<int,int> allNodeIdMapForAssembling;
  for (int i=0; i< allNodeIds.size(); i++)
  {
    allNodeIdMapForAssembling[allNodeIds[i]] = i;
  }
  double WT = _networkInteraction->getTotalWeight();
  for (int i=0; i< allNodeIds.size(); i++)
  {
    const MaterialNode* node = _networkInteraction->getMaterialNode(allNodeIds[i]);
    double Wi = _networkInteraction->getWeight(node);
    double fi = Wi/WT;
    const STensor3& Pi = _networkInteraction->getStress(node);
    //
    for (int j=0; j< allNodeIds.size(); j++)
    {
      double DfiDWj = 0;
      if (j==i)
      {
        DfiDWj = 1./WT - Wi/(WT*WT);
      }
      else
      {
        DfiDWj = - Wi/(WT*WT);
      }
      for (int p=0; p<3; p++)
      {
        for (int q=0; q<3; q++)
        {
          int row = Tensor23::getIndex(p,q);
          DPDDW(row, j) += DfiDWj*Pi(p,q);
        }
      };
    }
    //
    static std::vector<STensor3> DPiDalpha;
    _networkInteraction->getDStressDCoefficient(node,DPiDalpha);
    const std::vector<const InteractionMechanism*>* alInteractions =  _networkInteraction->getInteractionsForNode(node);
    for (int k=0; k< alInteractions->size(); k++)
    {
      const InteractionMechanism* im = (*alInteractions)[k];
      static std::vector<int> allNodes;
      static std::vector<double> DalphaDW;
      coeffReduc.getDCoefficientsDWeights(*_networkInteraction,*im, allNodeIds[i], allNodes, DalphaDW);
      for (int l= 0; l < allNodes.size(); l++)
      {
        int col = allNodeIdMapForAssembling[allNodes[l]];
        for (int p=0; p<3; p++)
        {
          for (int q=0; q<3; q++)
          {
            int row = Tensor23::getIndex(p,q);
            DPDDW(row, col) += fi*DPiDalpha[k](p,q)*DalphaDW[l];
          }
        }
      }
    }
  };
};

void DeepMaterialNetwork::computeDIPCompDInteraction(IPField::Output comp, int phaseIndex, const CoefficientReduction& coeffReduc, const InteractionMechanism& im, 
                                fullMatrix<double>& DIPCompDCoeffVars, fullMatrix<double>& DIPCompDnormalVars)
{
  const fullMatrix<double>& DaDCoeffVars = _DaDFittingParameters.getDaDCoeffVars(im.getIndex());
  const fullMatrix<double>& DaDnormalVars = _DaDFittingParameters.getDaDnormalVars(im.getIndex());
  static fullMatrix<double> DIPCompDa;
  computeDIPCompDIncompatibleVector(comp,phaseIndex,DIPCompDa);

  if (coeffReduc.getNumberOfCoefficientVars(*_networkInteraction,im) > 0)
  {
    fullMatrixOperation::allocateAndMakeZero(DIPCompDCoeffVars,1,DaDCoeffVars.size2());
    DIPCompDa.mult(DaDCoeffVars,DIPCompDCoeffVars);
  }
  fullMatrixOperation::allocateAndMakeZero(DIPCompDnormalVars,1,DaDnormalVars.size2());
  DIPCompDa.mult(DaDnormalVars,DIPCompDnormalVars);

  //
  double WT = _networkInteraction->getTotalWeightPhase(phaseIndex);
  std::vector<int> allNodeIds;
  im.getAllMaterialNodeIds(allNodeIds);
  for (int j=0; j< allNodeIds.size(); j++)
  {
    int nodeId = allNodeIds[j];
    const MaterialNode* node = _networkInteraction->getMaterialNode(nodeId);
    if (node->matIndex == phaseIndex)
    {
      double Wi = _networkInteraction->getWeight(node);
      double fi = Wi/WT;
      static double vali;
      static STensor3 DvalDFi;
      getDIPCompDFLocal(node->ips,comp,vali,DvalDFi);
      
      if (coeffReduc.getNumberOfCoefficientVars(*_networkInteraction,im) > 0)
      {
        static STensor3 DFiDalpha;
        im.getDInducedStrainDCoefficient(nodeId,DFiDalpha);
        double DvaliDalpha = STensorOperation::doubledot(DvalDFi,DFiDalpha);
        
        static std::vector<double> DalphaVars;
        coeffReduc.getDCoefficientsDCoefficientVars(*_networkInteraction,im,allNodeIds[j],DalphaVars);
        
        for (int k=0; k< DalphaVars.size(); k++)
        {
          DIPCompDCoeffVars(0, k) += fi*DvaliDalpha*DalphaVars[k];
        }
      }

      static STensor33 DFiDdir;
      im.getDInducedStrainDDirection(nodeId,DFiDdir);
      static SVector3 DvaliDdir;
      STensorOperation::multSTensor3STensor33(DvalDFi,DFiDdir,DvaliDdir);
      
      static SVector3 direction;
      std::vector<SVector3> DdirectionDdirectionVars;
      im.getDirection(direction,true,&DdirectionDdirectionVars);
      //
      for (int k=0; k< im.getDirectionVars().size(); k++)
      {
        double DvalDvari = STensorOperation::dot(DvaliDdir,DdirectionDdirectionVars[k]);
        DIPCompDnormalVars(0, k) += fi*DvalDvari;
      };
    };
  };
} 

void DeepMaterialNetwork::computeDStrainDInteraction(int phaseIndex, const CoefficientReduction& coeffReduc, const InteractionMechanism& im, 
        fullMatrix<double>& DFDCoeffVars, fullMatrix<double>& DFDnormalVars)
{
  const fullMatrix<double>& DaDCoeffVars = _DaDFittingParameters.getDaDCoeffVars(im.getIndex());
  const fullMatrix<double>& DaDnormalVars = _DaDFittingParameters.getDaDnormalVars(im.getIndex());
  static fullMatrix<double> DFDa;
  computeDStrainDIncompatibleVector(phaseIndex,DFDa);

  if (coeffReduc.getNumberOfCoefficientVars(*_networkInteraction,im) > 0)
  {
    fullMatrixOperation::allocateAndMakeZero(DFDCoeffVars,9,DaDCoeffVars.size2());
    DFDa.mult(DaDCoeffVars,DFDCoeffVars);
  }
  fullMatrixOperation::allocateAndMakeZero(DFDnormalVars,9,DaDnormalVars.size2());
  DFDa.mult(DaDnormalVars,DFDnormalVars);

  //
  double WT = _networkInteraction->getTotalWeightPhase(phaseIndex);
  std::vector<int> allNodeIds;
  im.getAllMaterialNodeIds(allNodeIds);
  for (int j=0; j< allNodeIds.size(); j++)
  {
    int nodeId = allNodeIds[j];
    const MaterialNode* node = _networkInteraction->getMaterialNode(nodeId);
    if (node->matIndex == phaseIndex)
    {
      double Wi = _networkInteraction->getWeight(node);
      double fi = Wi/WT;
      if (coeffReduc.getNumberOfCoefficientVars(*_networkInteraction,im) > 0)
      {
        static STensor3 DFiDalpha;
        im.getDInducedStrainDCoefficient(nodeId,DFiDalpha);
        
        static std::vector<double> DalphaVars;
        coeffReduc.getDCoefficientsDCoefficientVars(*_networkInteraction,im,allNodeIds[j],DalphaVars);
        
        for (int k=0; k< DalphaVars.size(); k++)
        {
          for (int p=0; p<3; p++)
          {
            for (int q=0; q<3; q++)
            {
              int row = Tensor23::getIndex(p,q);
              DFDCoeffVars(row, k) += fi*DFiDalpha(p,q)*DalphaVars[k];
            }
          }
        }
      }

      static STensor33 DFiDdir;
      im.getDInducedStrainDDirection(nodeId,DFiDdir);
      static SVector3 direction;
      std::vector<SVector3> DdirectionDdirectionVars;
      im.getDirection(direction,true,&DdirectionDdirectionVars);
      //
      for (int k=0; k< im.getDirectionVars().size(); k++)
      {
        static STensor3 DFiDnormalVar;
        STensorOperation::multSTensor33SVector3(DFiDdir,DdirectionDdirectionVars[k],DFiDnormalVar);
        //
        for (int p=0; p<3; p++)
        {
          for (int q=0; q<3; q++)
          {
            int row = Tensor23::getIndex(p,q);
            DFDnormalVars(row, k) += fi*DFiDnormalVar(p,q);
          }
        }
      };
    };
  };
};

void DeepMaterialNetwork::computeDStressDInteraction(const CoefficientReduction& coeffReduc, const InteractionMechanism& im, fullMatrix<double>& DPDCoeffVars, fullMatrix<double>& DPDnormalVars)
{
  const fullMatrix<double>& DaDCoeffVars = _DaDFittingParameters.getDaDCoeffVars(im.getIndex());
  const fullMatrix<double>& DaDnormalVars = _DaDFittingParameters.getDaDnormalVars(im.getIndex());
  static fullMatrix<double> DPDa;
  computeDStressDIncompatibleVector(DPDa);

  if (coeffReduc.getNumberOfCoefficientVars(*_networkInteraction,im) > 0)
  {
    fullMatrixOperation::allocateAndMakeZero(DPDCoeffVars,9,DaDCoeffVars.size2());
    DPDa.mult(DaDCoeffVars,DPDCoeffVars);
  }

  fullMatrixOperation::allocateAndMakeZero(DPDnormalVars,9,DaDnormalVars.size2());
  DPDa.mult(DaDnormalVars,DPDnormalVars);

  //
  double WT = _networkInteraction->getTotalWeight();
  std::vector<int> allNodeIds;
  im.getAllMaterialNodeIds(allNodeIds);
  for (int j=0; j< allNodeIds.size(); j++)
  {
    int nodeId = allNodeIds[j];
    const MaterialNode* node = _networkInteraction->getMaterialNode(nodeId);
    double Wi = _networkInteraction->getWeight(node);
    double fi = Wi/WT;
    const STensor43& Li = _networkInteraction->getTangent(node);
    if (coeffReduc.getNumberOfCoefficientVars(*_networkInteraction,im) > 0)
    {
      static STensor3 DFiDalpha, DPiDalpha;
      im.getDInducedStrainDCoefficient(nodeId,DFiDalpha);
      STensorOperation::multSTensor43STensor3(Li,DFiDalpha,DPiDalpha);

      static std::vector<double> DalphaVars;
      coeffReduc.getDCoefficientsDCoefficientVars(*_networkInteraction,im,allNodeIds[j],DalphaVars);

      for (int k=0; k< DalphaVars.size(); k++)
      {
        for (int p=0; p<3; p++)
        {
          for (int q=0; q<3; q++)
          {
            int row = Tensor23::getIndex(p,q);
            DPDCoeffVars(row, k) += fi*DPiDalpha(p,q)*DalphaVars[k];
          }
        }
      }
    }

    static STensor33 DFiDdir, DPiDnormal;
    im.getDInducedStrainDDirection(nodeId,DFiDdir);
    STensorOperation::multSTensor43STensor33(Li,DFiDdir,DPiDnormal);
    static SVector3 direction;
    std::vector<SVector3> DdirectionDdirectionVars;
    im.getDirection(direction,true,&DdirectionDdirectionVars);
    //
    for (int k=0; k< im.getDirectionVars().size(); k++)
    {
      static STensor3 DPiDnormalVar;
      STensorOperation::multSTensor33SVector3(DPiDnormal,DdirectionDdirectionVars[k],DPiDnormalVar);
      //
      for (int p=0; p<3; p++)
      {
        for (int q=0; q<3; q++)
        {
          int row = Tensor23::getIndex(p,q);
          DPDnormalVars(row, k) += fi*DPiDnormalVar(p,q);
        }
      }
    };
  };
  //
};

void DeepMaterialNetwork::computeDIPCompDIncompatibleVector(IPField::Output comp, int phaseIndex, fullMatrix<double>& DIPCompDa)
{
  fullMatrixOperation::allocateAndMakeZero(DIPCompDa,1,_unknown.size());
  std::vector<const MaterialNode*> allNodes;
  _networkInteraction->getAllMaterialNodesForMat(phaseIndex,allNodes);
  double WT = _networkInteraction->getTotalWeightPhase(phaseIndex);
  for (int inode=0; inode < allNodes.size(); inode++)
  {
    const MaterialNode* node = allNodes[inode];
    
    double val;
    static STensor3 DvalDF;
    getDIPCompDFLocal(node->ips,comp,val,DvalDF);
    double Wi = _networkInteraction->getWeight(node);
    double fi = Wi/WT;
    static std::vector<STensor33> DFiDa;
    _networkInteraction->getDFDIncompatibleVector(node,DFiDa);
    const std::vector<const InteractionMechanism*>* alInteractions =  _networkInteraction->getInteractionsForNode(node);
    for (int j=0; j< alInteractions->size(); j++)
    {
      std::vector<Dof> R;
      getKeys((*alInteractions)[j],R); // key for a
      static SVector3 DvalDa;
      STensorOperation::multSTensor3STensor33(DvalDF,DFiDa[j],DvalDa);
      for (int iR= 0; iR < R.size(); iR++)
      {
        std::map<Dof,int>::const_iterator itR =  _unknown.find(R[iR]);
        if (itR != _unknown.end())
        {
          int col = itR->second;
          DIPCompDa(0, col) += fi*DvalDa(iR);
        }
      }
    }
  }
};

void DeepMaterialNetwork::computeDStrainDIncompatibleVector(int phaseIndex, fullMatrix<double>& DFDa)
{
  fullMatrixOperation::allocateAndMakeZero(DFDa,9,_unknown.size());
  std::vector<const MaterialNode*> allNodes;
  _networkInteraction->getAllMaterialNodesForMat(phaseIndex,allNodes);
  double WT = _networkInteraction->getTotalWeightPhase(phaseIndex);
  for (int inode=0; inode < allNodes.size(); inode++)
  {
    const MaterialNode* node = allNodes[inode];
    double Wi = _networkInteraction->getWeight(node);
    double fi = Wi/WT;
    static std::vector<STensor33> DFiDa;
    _networkInteraction->getDFDIncompatibleVector(node,DFiDa);
    const std::vector<const InteractionMechanism*>* alInteractions =  _networkInteraction->getInteractionsForNode(node);
    for (int j=0; j< alInteractions->size(); j++)
    {
      std::vector<Dof> R;
      getKeys((*alInteractions)[j],R); // key for a
      for (int iR= 0; iR < R.size(); iR++)
      {
        std::map<Dof,int>::const_iterator itR =  _unknown.find(R[iR]);
        if (itR != _unknown.end())
        {
          int col = itR->second;
          for (int p=0; p<3; p++)
          {
            for (int q=0; q<3; q++)
            {
              int row = Tensor23::getIndex(p,q);
              DFDa(row, col) += fi*DFiDa[j](p,q,iR);
            }
          }
        }
      }
    }
  }
};

void DeepMaterialNetwork::computeDStressDIncompatibleVector(fullMatrix<double>& DPDa)
{
  fullMatrixOperation::allocateAndMakeZero(DPDa,9,_unknown.size());

  NetworkInteraction::MaterialNodeContainer& allNodes = _networkInteraction->getMaterialNodeContainer();
  double WT = _networkInteraction->getTotalWeight();
  for (NetworkInteraction::MaterialNodeContainer::iterator it =allNodes.begin(); it!= allNodes.end(); it++)
  {
    const MaterialNode* node = it->second;
    double Wi = _networkInteraction->getWeight(node);
    double fi = Wi/WT;
    static std::vector<STensor33> DPiDa;
    _networkInteraction->getDStressDIncompatibleVector(node,DPiDa);
    const std::vector<const InteractionMechanism*>* alInteractions =  _networkInteraction->getInteractionsForNode(node);
    for (int j=0; j< alInteractions->size(); j++)
    {
      std::vector<Dof> R;
      getKeys((*alInteractions)[j],R); // key for a
      for (int iR= 0; iR < R.size(); iR++)
      {
        std::map<Dof,int>::const_iterator itR =  _unknown.find(R[iR]);
        if (itR != _unknown.end())
        {
          int col = itR->second;
          for (int p=0; p<3; p++)
          {
            for (int q=0; q<3; q++)
            {
              int row = Tensor23::getIndex(p,q);
              DPDa(row, col) += fi*DPiDa[j](p,q,iR);
            }
          }
        }
      }
    }
  };
};

void DeepMaterialNetwork::downscale(const STensor3& Fcur)
{
  _networkInteraction->computeLocalDeformationGradient(Fcur);
  // estimate
  NetworkInteraction::MaterialNodeContainer& allNodes = _networkInteraction->getMaterialNodeContainer();
  for (NetworkInteraction::MaterialNodeContainer::iterator it =allNodes.begin(); it!= allNodes.end(); it++)
  {
    MaterialNode* node = it->second;;
    // evaluate constitutive at leaf
    stressLocal(node);
  };
};

void DeepMaterialNetwork::fixDofsByComp(int comp) // fixed all comp
{
  _fixedComp.push_back(comp);
};

bool DeepMaterialNetwork::isInitialized() const
{
  return _isInitialized;
}

void DeepMaterialNetwork::initSolve()
{
  _isInitialized= true;
  //printf("initializing \n");
  // init cache
  allocateIPStateMaterialNodes();
  //
  numberDof();
  //allocate system
  int numR = _unknown.size();
  allocateSystem(numR);
  //
  //printf("done initializing, size of unknowns = %d \n",numR);
};

void DeepMaterialNetwork::elasticTangent(fullMatrix<double>& L)
{
  //
  L.resize(9,9,true);
  // init solve
  initSolve();
  // compute tangent
  assembleDresDu();
  static STensor43 Ltensor;
  bool succ = tangentSolve(Ltensor);
  if (succ)
  {
    for (int row = 0; row <9; row++)
    {
      int i,j;
      Tensor23::getIntsFromIndex(row,i,j);
      for (int col = 0; col <9; col++)
      {
        int k,l;
        Tensor23::getIntsFromIndex(col,k,l);
        L(row,col) = Ltensor(i,j,k,l);
      }
    };
  }
  else
  {
    Msg::Error("tangent solving fails !!!");
  }
};

void DeepMaterialNetwork::elasticTangentByStress(fullMatrix<double>& Lout)
{
  double time = Cpu();
  static std::map<const MaterialNode*, fullMatrix<double> > DLDWeights;
  static std::map<const InteractionMechanism*, std::vector<fullMatrix<double> > > sDLDCoeffs;
  static std::map<const InteractionMechanism*, std::vector<fullMatrix<double> > > DLDCoeffs;

  CoefficientReductionAll reduction;
  bool ok = initialElasticTangentByStress(Lout,true,&reduction,&DLDWeights,&sDLDCoeffs,&DLDCoeffs);
  if (!ok)
  {
    Msg::Error("compute tangent problem !!!");
  }
  /*
  const NetworkInteraction::MaterialNodeContainer& allMaterialNodes = _networkInteraction->getMaterialNodeContainer();
  for (NetworkInteraction::MaterialNodeContainer::const_iterator itnode = allMaterialNodes.begin(); itnode != allMaterialNodes.end(); itnode++)
  {
    const MaterialNode* node = itnode->second;
    fullMatrix<double>& DLDW = DLDWeights[node];
    Msg::Info("at node: %d",node->getNodeId());
    DLDW.print("DLDW");
  };
   */
   Msg::Info("solving time = %.16g s",Cpu()-time);
};

bool DeepMaterialNetwork::initialElasticTangentByStress(fullMatrix<double>& Lmat, bool stiff,
                            const CoefficientReduction* coeffReduc,
                            std::map<const MaterialNode*, fullMatrix<double> >* DLDWeights,
                            std::map<const InteractionMechanism*, std::vector<fullMatrix<double> > >* DLDCoeffVars,
                            std::map<const InteractionMechanism*, std::vector<fullMatrix<double> > >* DLDNormalVars)
{
  const NetworkInteraction::InteractionContainer& allInteractions = _networkInteraction->getInteractionContainer();
  const NetworkInteraction::MaterialNodeContainer& allMaterialNodes = _networkInteraction->getMaterialNodeContainer();
  int dim = _networkInteraction->getDimension();
  int dimL = (dim ==2)? 3 : 6;
  fullMatrixOperation::allocateAndMakeZero(Lmat,dimL,dimL);
  if (stiff)
  {
    //
    if (DLDWeights != NULL)
    {
      for (NetworkInteraction::MaterialNodeContainer::const_iterator itnode = allMaterialNodes.begin(); itnode != allMaterialNodes.end(); itnode++)
      {
        const MaterialNode* node = itnode->second;
        fullMatrix<double>& DLDW = (*DLDWeights)[node];
        fullMatrixOperation::allocateAndMakeZero(DLDW,dimL,dimL);
      };
    }
    //
    for (NetworkInteraction::InteractionContainer::const_iterator it = allInteractions.begin(); it != allInteractions.end(); it++)
    {
      const InteractionMechanism* im = it->second;
      int numberUnknownPerIm = coeffReduc->getNumberOfCoefficientVars(*_networkInteraction,*im);
      if (numberUnknownPerIm > 0)
      {
        std::vector<fullMatrix<double> >& matCoeffs = (*DLDCoeffVars)[im];
        if (matCoeffs.size() != numberUnknownPerIm);
        {
          matCoeffs.resize(numberUnknownPerIm,fullMatrix<double>());
        }
        for (int j=0; j< numberUnknownPerIm; j++)
        {
          fullMatrix<double>& DLDalp = matCoeffs[j];
          fullMatrixOperation::allocateAndMakeZero(DLDalp,dimL,dimL);
        }
      }
      //
      int numNormalsVars = im->getDirectionVars().size();
      std::vector<fullMatrix<double> >& matNormals = (*DLDNormalVars)[im];
      if (matNormals.size() != numNormalsVars)
      {
        matNormals.resize(numNormalsVars, fullMatrix<double>());
      }
      for (int j=0; j< numNormalsVars; j++)
      {
        fullMatrixOperation::allocateAndMakeZero(matNormals[j],dimL,dimL);
      }

    };
  };

  // init solve
  if (!_isInitialized)
  {
    initSolve();
  }
  //
  static STensor3 F, sig;
  static STensor43 Ltensor;
  double fact = 1e-2;
  static std::vector<int> allNodeIds;
  static fullMatrix<double> DsigDW, DsigDCoeffVars, DsigDnormalVars;
  bool message = false;

  if (dim >= 1)
  {
    STensorOperation::unity(F);
    F(0,0) += fact;
    bool ok = stress(F,sig,false,Ltensor,10,1e-6,1e-10,message,false);
    if (!ok)
    {
      return false;
    }
    int row(0), col(0);
    fullMatrixOperation::getRowColTangent(dim,0,0,0,0,row,col);
    Lmat(row,col) = sig(0,0)/fact;

    if (stiff)
    {
      computeDUnknownDFittingParameters(*coeffReduc,DLDWeights == NULL);
      if (DLDWeights != NULL)
      {
        computeDStressDWeights(*coeffReduc,allNodeIds,DsigDW);
        for (int in =0; in < allNodeIds.size(); in++)
        {
          const MaterialNode* node = _networkInteraction->getMaterialNode(allNodeIds[in]);
          fullMatrix<double>& DLDW = (*DLDWeights)[node];
          int comp = Tensor23::getIndex(0,0);
          DLDW(row,col) = DsigDW(comp,in)/fact;
        }
      }

      //
      for (NetworkInteraction::InteractionContainer::const_iterator it = allInteractions.begin(); it != allInteractions.end(); it++)
      {
        const InteractionMechanism* im = it->second;;
        computeDStressDInteraction(*coeffReduc, *im,DsigDCoeffVars,DsigDnormalVars);
        int numberUnknownPerIm = coeffReduc->getNumberOfCoefficientVars(*_networkInteraction,*im);
        if (numberUnknownPerIm > 0)
        {
          std::vector<fullMatrix<double> >& matCoeffs = (*DLDCoeffVars)[im];
          for (int j=0; j< numberUnknownPerIm; j++)
          {
            fullMatrix<double>& DLDalp = matCoeffs[j];
            int comp = Tensor23::getIndex(0,0);
            DLDalp(row,col) = DsigDCoeffVars(comp,j)/fact;
          }
        }
        std::vector<fullMatrix<double> >& matNormals = (*DLDNormalVars)[im];
        for (int j=0; j< im->getDirectionVars().size(); j++)
        {
          fullMatrix<double>& DLDnormal = matNormals[j];
          int comp = Tensor23::getIndex(0,0);
          DLDnormal(row,col) = DsigDnormalVars(comp,j)/fact;
        }
      }
    }
  }

  if (dim >= 2)
  {
    STensorOperation::unity(F);
    F(1,1) += fact;
    bool ok = stress(F,sig,false,Ltensor,10,1e-6,1e-10,message,true);
    if (!ok)
    {
      return false;
    }
    int row, col;
    fullMatrixOperation::getRowColTangent(dim,0,0,1,1,row,col);
    Lmat(row,col) = sig(0,0)/fact;
    Lmat(col,row) = Lmat(row,col);
    fullMatrixOperation::getRowColTangent(dim,1,1,1,1,row,col);
    Lmat(row,col) = sig(1,1)/fact;

    if (stiff)
    {
      computeDUnknownDFittingParameters(*coeffReduc,DLDWeights == NULL);
      if (DLDWeights != NULL)
      {
        computeDStressDWeights(*coeffReduc, allNodeIds, DsigDW);
        for (int in =0; in < allNodeIds.size(); in++)
        {
          const MaterialNode* node = _networkInteraction->getMaterialNode(allNodeIds[in]);
          fullMatrix<double>& DLDW = (*DLDWeights)[node];
          fullMatrixOperation::getRowColTangent(dim,0,0,1,1,row,col);
          int comp = Tensor23::getIndex(0,0);
          DLDW(row,col) = DsigDW(comp,in)/fact;
          DLDW(col,row) = DLDW(row,col);

          fullMatrixOperation::getRowColTangent(dim,1,1,1,1,row,col);
          comp = Tensor23::getIndex(1,1);
          DLDW(row,col) = DsigDW(comp,in)/fact;
        }
      }
      //
      for (NetworkInteraction::InteractionContainer::const_iterator it = allInteractions.begin(); it != allInteractions.end(); it++)
      {
        const InteractionMechanism* im = it->second;;
        computeDStressDInteraction(*coeffReduc, *im,DsigDCoeffVars,DsigDnormalVars);
        int numberUnknownPerIm = coeffReduc->getNumberOfCoefficientVars(*_networkInteraction,*im);
        if (numberUnknownPerIm > 0)
        {
          std::vector<fullMatrix<double> >& matCoeffs = (*DLDCoeffVars)[im];
          for (int j=0; j< numberUnknownPerIm; j++)
          {
            fullMatrix<double>& DLDalp = matCoeffs[j];
            fullMatrixOperation::getRowColTangent(dim,0,0,1,1,row,col);
            int comp = Tensor23::getIndex(0,0);
            DLDalp(row,col) = DsigDCoeffVars(comp,j)/fact;
            DLDalp(col,row) = DLDalp(row,col);

            fullMatrixOperation::getRowColTangent(dim,1,1,1,1,row,col);
            comp = Tensor23::getIndex(1,1);
            DLDalp(row,col) = DsigDCoeffVars(comp,j)/fact;
          }
        }
        std::vector<fullMatrix<double> >& matNormals = (*DLDNormalVars)[im];
        for (int j=0; j< im->getDirectionVars().size(); j++)
        {
          fullMatrix<double>& DLDnormal = matNormals[j];
          fullMatrixOperation::getRowColTangent(dim,0,0,1,1,row,col);
          int comp = Tensor23::getIndex(0,0);
          DLDnormal(row,col) = DsigDnormalVars(comp,j)/fact;
          DLDnormal(col,row) = DLDnormal(row,col);

          fullMatrixOperation::getRowColTangent(dim,1,1,1,1,row,col);
          comp = Tensor23::getIndex(1,1);
          DLDnormal(row,col) = DsigDnormalVars(comp,j)/fact;
        }
      }
    }

    STensorOperation::unity(F);
    F(0,1) += fact;
    ok = stress(F,sig,false,Ltensor,10,1e-6,1e-10,message,true);
    if (!ok)
    {
      return false;
    }

    fullMatrixOperation::getRowColTangent(dim,0,0,0,1,row,col);
    Lmat(row,col) = sig(0,0)/fact;
    Lmat(col,row) = Lmat(row,col);

    fullMatrixOperation::getRowColTangent(dim,1,1,0,1,row,col);
    Lmat(row,col) = sig(1,1)/fact;
    Lmat(col,row) = Lmat(row,col);

    fullMatrixOperation::getRowColTangent(dim,0,1,0,1,row,col);
    Lmat(row,col) = sig(0,1)/fact;
    if (dim == 3)
    {
      fullMatrixOperation::getRowColTangent(dim,2,2,0,1,row,col);
      Lmat(row,col) = sig(2,2)/fact;
      Lmat(col,row) = Lmat(row,col);
    }


    if (stiff)
    {
      computeDUnknownDFittingParameters(*coeffReduc,DLDWeights == NULL);
      if (DLDWeights != NULL)
      {
        computeDStressDWeights(*coeffReduc, allNodeIds,DsigDW);
        for (int in =0; in < allNodeIds.size(); in++)
        {
          const MaterialNode* node = _networkInteraction->getMaterialNode(allNodeIds[in]);
          fullMatrix<double>& DLDW = (*DLDWeights)[node];
          fullMatrixOperation::getRowColTangent(dim,0,0,0,1,row,col);
          int comp = Tensor23::getIndex(0,0);
          DLDW(row,col) = DsigDW(comp,in)/fact;
          DLDW(col,row) = DLDW(row,col);

          fullMatrixOperation::getRowColTangent(dim,1,1,0,1,row,col);
          comp = Tensor23::getIndex(1,1);
          DLDW(row,col) = DsigDW(comp,in)/fact;
          DLDW(col,row) = DLDW(row,col);

          fullMatrixOperation::getRowColTangent(dim,0,1,0,1,row,col);
          comp = Tensor23::getIndex(0,1);
          DLDW(row,col) = DsigDW(comp,in)/fact;

          if (dim == 3)
          {
            fullMatrixOperation::getRowColTangent(dim,2,2,0,1,row,col);
            comp = Tensor23::getIndex(2,2);
            DLDW(row,col) = DsigDW(comp,in)/fact;
            DLDW(col,row) = DLDW(row,col);
          }

        }
      }
      //
      for (NetworkInteraction::InteractionContainer::const_iterator it = allInteractions.begin(); it != allInteractions.end(); it++)
      {
        const InteractionMechanism* im = it->second;;
        computeDStressDInteraction(*coeffReduc, *im,DsigDCoeffVars,DsigDnormalVars);
        int numberUnknownPerIm = coeffReduc->getNumberOfCoefficientVars(*_networkInteraction,*im);
        if (numberUnknownPerIm > 0)
        {
          std::vector<fullMatrix<double> >& matCoeffs = (*DLDCoeffVars)[im];
          for (int j=0; j< numberUnknownPerIm; j++)
          {
            fullMatrix<double>& DLDalp = matCoeffs[j];
            fullMatrixOperation::getRowColTangent(dim,0,0,0,1,row,col);
            int comp = Tensor23::getIndex(0,0);
            DLDalp(row,col) = DsigDCoeffVars(comp,j)/fact;
            DLDalp(col,row) = DLDalp(row,col);

            fullMatrixOperation::getRowColTangent(dim,1,1,0,1,row,col);
            comp = Tensor23::getIndex(1,1);
            DLDalp(row,col) = DsigDCoeffVars(comp,j)/fact;
            DLDalp(col,row) = DLDalp(row,col);

            fullMatrixOperation::getRowColTangent(dim,0,1,0,1,row,col);
            comp = Tensor23::getIndex(0,1);
            DLDalp(row,col) = DsigDCoeffVars(comp,j)/fact;

            if (dim == 3)
            {
              fullMatrixOperation::getRowColTangent(dim,2,2,0,1,row,col);
              comp = Tensor23::getIndex(2,2);
              DLDalp(row,col) = DsigDCoeffVars(comp,j)/fact;
              DLDalp(col,row) = DLDalp(row,col);
            }
          }
        }
        std::vector<fullMatrix<double> >& matNormals = (*DLDNormalVars)[im];
        for (int j=0; j< im->getDirectionVars().size(); j++)
        {
          fullMatrix<double>& DLDnormal = matNormals[j];
          fullMatrixOperation::getRowColTangent(dim,0,0,0,1,row,col);
          int comp = Tensor23::getIndex(0,0);
          DLDnormal(row,col) = DsigDnormalVars(comp,j)/fact;
          DLDnormal(col,row) = DLDnormal(row,col);

          fullMatrixOperation::getRowColTangent(dim,1,1,0,1,row,col);
          comp = Tensor23::getIndex(1,1);
          DLDnormal(row,col) = DsigDnormalVars(comp,j)/fact;
          DLDnormal(col,row) = DLDnormal(row,col);

          fullMatrixOperation::getRowColTangent(dim,0,1,0,1,row,col);
          comp = Tensor23::getIndex(0,1);
          DLDnormal(row,col) = DsigDnormalVars(comp,j)/fact;

          if (dim == 3)
          {
            fullMatrixOperation::getRowColTangent(dim,2,2,0,1,row,col);
            comp = Tensor23::getIndex(2,2);
            DLDnormal(row,col) = DsigDnormalVars(comp,j)/fact;
            DLDnormal(col,row) = DLDnormal(row,col);
          }
        }
      }
    }
  }

  if (dim == 3)
  {
    STensorOperation::unity(F);
    F(2,2) += fact;
    bool ok = stress(F,sig,false,Ltensor,10,1e-6,1e-10,message,true);
    if (!ok)
    {
      return false;
    }
    int row, col;
    fullMatrixOperation::getRowColTangent(dim,0,0,2,2,row,col);
    Lmat(row,col) = sig(0,0)/fact;
    Lmat(col,row) = Lmat(row,col);

    fullMatrixOperation::getRowColTangent(dim,1,1,2,2,row,col);
    Lmat(row,col) = sig(1,1)/fact;
    Lmat(col,row) = Lmat(row,col);

    fullMatrixOperation::getRowColTangent(dim,2,2,2,2,row,col);
    Lmat(row,col) = sig(2,2)/fact;


    if (stiff)
    {
      computeDUnknownDFittingParameters(*coeffReduc,DLDWeights == NULL);
      if (DLDWeights != NULL)
      {
        computeDStressDWeights(*coeffReduc, allNodeIds,DsigDW);
        for (int in =0; in < allNodeIds.size(); in++)
        {
          const MaterialNode* node = _networkInteraction->getMaterialNode(allNodeIds[in]);
          fullMatrix<double>& DLDW = (*DLDWeights)[node];
          fullMatrixOperation::getRowColTangent(dim,0,0,2,2,row,col);
          int comp = Tensor23::getIndex(0,0);
          DLDW(row,col) = DsigDW(comp,in)/fact;
          DLDW(col,row) = DLDW(row,col);

          fullMatrixOperation::getRowColTangent(dim,1,1,2,2,row,col);
          comp = Tensor23::getIndex(1,1);
          DLDW(row,col) = DsigDW(comp,in)/fact;
          DLDW(col,row) = DLDW(row,col);

          fullMatrixOperation::getRowColTangent(dim,2,2,2,2,row,col);
          comp = Tensor23::getIndex(2,2);
          DLDW(row,col) = DsigDW(comp,in)/fact;

        }
      }
      //
      for (NetworkInteraction::InteractionContainer::const_iterator it = allInteractions.begin(); it != allInteractions.end(); it++)
      {
        const InteractionMechanism* im = it->second;;
        computeDStressDInteraction(*coeffReduc,*im,DsigDCoeffVars,DsigDnormalVars);
        int numberUnknownPerIm = coeffReduc->getNumberOfCoefficientVars(*_networkInteraction,*im);
        if (numberUnknownPerIm > 0)
        {
          std::vector<fullMatrix<double> >& matCoeffs = (*DLDCoeffVars)[im];
          for (int j=0; j< numberUnknownPerIm; j++)
          {
            fullMatrix<double>& DLDalp = matCoeffs[j];
            fullMatrixOperation::getRowColTangent(dim,0,0,2,2,row,col);
            int comp = Tensor23::getIndex(0,0);
            DLDalp(row,col) = DsigDCoeffVars(comp,j)/fact;
            DLDalp(col,row) = DLDalp(row,col);

            fullMatrixOperation::getRowColTangent(dim,1,1,2,2,row,col);
            comp = Tensor23::getIndex(1,1);
            DLDalp(row,col) = DsigDCoeffVars(comp,j)/fact;
            DLDalp(col,row) = DLDalp(row,col);

            fullMatrixOperation::getRowColTangent(dim,2,2,2,2,row,col);
            comp = Tensor23::getIndex(2,2);
            DLDalp(row,col) = DsigDCoeffVars(comp,j)/fact;

          }
        }
        std::vector<fullMatrix<double> >& matNormals = (*DLDNormalVars)[im];
        for (int j=0; j< im->getDirectionVars().size(); j++)
        {
          fullMatrix<double>& DLDnormal = matNormals[j];
          fullMatrixOperation::getRowColTangent(dim,0,0,2,2,row,col);
          int comp = Tensor23::getIndex(0,0);
          DLDnormal(row,col) = DsigDnormalVars(comp,j)/fact;
          DLDnormal(col,row) = DLDnormal(row,col);

          fullMatrixOperation::getRowColTangent(dim,1,1,2,2,row,col);
          comp = Tensor23::getIndex(1,1);
          DLDnormal(row,col) = DsigDnormalVars(comp,j)/fact;
          DLDnormal(col,row) = DLDnormal(row,col);

          fullMatrixOperation::getRowColTangent(dim,2,2,2,2,row,col);
          comp = Tensor23::getIndex(2,2);
          DLDnormal(row,col) = DsigDnormalVars(comp,j)/fact;
        }
      }
    }
    //
    STensorOperation::unity(F);
    F(0,2) += fact;
    ok = stress(F,sig,false,Ltensor,10,1e-6,1e-10,message,true);
    if (!ok)
    {
      return false;
    }
    fullMatrixOperation::getRowColTangent(dim,0,0,0,2,row,col);
    Lmat(row,col) = sig(0,0)/fact;
    Lmat(col,row) = Lmat(row,col);

    fullMatrixOperation::getRowColTangent(dim,1,1,0,2,row,col);
    Lmat(row,col) = sig(1,1)/fact;
    Lmat(col,row) = Lmat(row,col);

    fullMatrixOperation::getRowColTangent(dim,2,2,0,2,row,col);
    Lmat(row,col) = sig(2,2)/fact;
    Lmat(col,row) = Lmat(row,col);


    fullMatrixOperation::getRowColTangent(dim,0,1,0,2,row,col);
    Lmat(row,col) = sig(0,1)/fact;
    Lmat(col,row) = Lmat(row,col);


    fullMatrixOperation::getRowColTangent(dim,0,2,0,2,row,col);
    Lmat(row,col) = sig(0,2)/fact;


    if (stiff)
    {
      computeDUnknownDFittingParameters(*coeffReduc,DLDWeights == NULL);
      if (DLDWeights != NULL)
      {
        computeDStressDWeights(*coeffReduc, allNodeIds,DsigDW);
        for (int in =0; in < allNodeIds.size(); in++)
        {
          const MaterialNode* node = _networkInteraction->getMaterialNode(allNodeIds[in]);
          fullMatrix<double>& DLDW = (*DLDWeights)[node];
          fullMatrixOperation::getRowColTangent(dim,0,0,0,2,row,col);
          int comp = Tensor23::getIndex(0,0);
          DLDW(row,col) = DsigDW(comp,in)/fact;
          DLDW(col,row) = DLDW(row,col);

          fullMatrixOperation::getRowColTangent(dim,1,1,0,2,row,col);
          comp = Tensor23::getIndex(1,1);
          DLDW(row,col) = DsigDW(comp,in)/fact;
          DLDW(col,row) = DLDW(row,col);

          fullMatrixOperation::getRowColTangent(dim,2,2,0,2,row,col);
          comp = Tensor23::getIndex(2,2);
          DLDW(row,col) = DsigDW(comp,in)/fact;
          DLDW(col,row) = DLDW(row,col);

          fullMatrixOperation::getRowColTangent(dim,0,1,0,2,row,col);
          comp = Tensor23::getIndex(0,1);
          DLDW(row,col) = DsigDW(comp,in)/fact;
          DLDW(col,row) = DLDW(row,col);

          fullMatrixOperation::getRowColTangent(dim,0,2,0,2,row,col);
          comp = Tensor23::getIndex(0,2);
          DLDW(row,col) = DsigDW(comp,in)/fact;
        }
      }
      //
      for (NetworkInteraction::InteractionContainer::const_iterator it = allInteractions.begin(); it != allInteractions.end(); it++)
      {
        const InteractionMechanism* im = it->second;;
        computeDStressDInteraction(*coeffReduc,*im,DsigDCoeffVars,DsigDnormalVars);
        int numberUnknownPerIm = coeffReduc->getNumberOfCoefficientVars(*_networkInteraction,*im);
        if (numberUnknownPerIm > 0)
        {
          std::vector<fullMatrix<double> >& matCoeffs = (*DLDCoeffVars)[im];
          for (int j=0; j< numberUnknownPerIm; j++)
          {
            fullMatrix<double>& DLDalp = matCoeffs[j];
            fullMatrixOperation::getRowColTangent(dim,0,0,0,2,row,col);
            int comp = Tensor23::getIndex(0,0);
            DLDalp(row,col) = DsigDCoeffVars(comp,j)/fact;
            DLDalp(col,row) = DLDalp(row,col);

            fullMatrixOperation::getRowColTangent(dim,1,1,0,2,row,col);
            comp = Tensor23::getIndex(1,1);
            DLDalp(row,col) = DsigDCoeffVars(comp,j)/fact;
            DLDalp(col,row) = DLDalp(row,col);

            fullMatrixOperation::getRowColTangent(dim,2,2,0,2,row,col);
            comp = Tensor23::getIndex(2,2);
            DLDalp(row,col) = DsigDCoeffVars(comp,j)/fact;
            DLDalp(col,row) = DLDalp(row,col);

            fullMatrixOperation::getRowColTangent(dim,0,1,0,2,row,col);
            comp = Tensor23::getIndex(0,1);
            DLDalp(row,col) = DsigDCoeffVars(comp,j)/fact;
            DLDalp(col,row) = DLDalp(row,col);

            fullMatrixOperation::getRowColTangent(dim,0,2,0,2,row,col);
            comp = Tensor23::getIndex(0,2);
            DLDalp(row,col) = DsigDCoeffVars(comp,j)/fact;
          }
        }
        std::vector<fullMatrix<double> >& matNormals = (*DLDNormalVars)[im];
        for (int j=0; j< im->getDirectionVars().size(); j++)
        {
          fullMatrix<double>& DLDnormal = matNormals[j];
          fullMatrixOperation::getRowColTangent(dim,0,0,0,2,row,col);
          int comp = Tensor23::getIndex(0,0);
          DLDnormal(row,col) = DsigDnormalVars(comp,j)/fact;
          DLDnormal(col,row) = DLDnormal(row,col);

          fullMatrixOperation::getRowColTangent(dim,1,1,0,2,row,col);
          comp = Tensor23::getIndex(1,1);
          DLDnormal(row,col) = DsigDnormalVars(comp,j)/fact;
          DLDnormal(col,row) = DLDnormal(row,col);

          fullMatrixOperation::getRowColTangent(dim,2,2,0,2,row,col);
          comp = Tensor23::getIndex(2,2);
          DLDnormal(row,col) = DsigDnormalVars(comp,j)/fact;
          DLDnormal(col,row) = DLDnormal(row,col);

          fullMatrixOperation::getRowColTangent(dim,0,1,0,2,row,col);
          comp = Tensor23::getIndex(0,1);
          DLDnormal(row,col) = DsigDnormalVars(comp,j)/fact;
          DLDnormal(col,row) = DLDnormal(row,col);

          fullMatrixOperation::getRowColTangent(dim,0,2,0,2,row,col);
          comp = Tensor23::getIndex(0,2);
          DLDnormal(row,col) = DsigDnormalVars(comp,j)/fact;
        }
      }
    }

    //
    STensorOperation::unity(F);
    F(1,2) += fact;
    ok = stress(F,sig,false,Ltensor,10,1e-6,1e-10,message,true);
    if (!ok)
    {
      return false;
    }

    fullMatrixOperation::getRowColTangent(dim,0,0,1,2,row,col);
    Lmat(row,col) = sig(0,0)/fact;
    Lmat(col,row) = Lmat(row,col);

    fullMatrixOperation::getRowColTangent(dim,1,1,1,2,row,col);
    Lmat(row,col) = sig(1,1)/fact;
    Lmat(col,row) = Lmat(row,col);

    fullMatrixOperation::getRowColTangent(dim,2,2,1,2,row,col);
    Lmat(row,col) = sig(2,2)/fact;
    Lmat(col,row) = Lmat(row,col);

    fullMatrixOperation::getRowColTangent(dim,0,1,1,2,row,col);
    Lmat(row,col) = sig(0,1)/fact;
    Lmat(col,row) = Lmat(row,col);

    fullMatrixOperation::getRowColTangent(dim,0,2,1,2,row,col);
    Lmat(row,col) = sig(0,2)/fact;
    Lmat(col,row) = Lmat(row,col);

    fullMatrixOperation::getRowColTangent(dim,1,2,1,2,row,col);
    Lmat(row,col) = sig(1,2)/fact;

    if (stiff)
    {
      computeDUnknownDFittingParameters(*coeffReduc,DLDWeights == NULL);
      if (DLDWeights != NULL)
      {
        computeDStressDWeights(*coeffReduc,allNodeIds,DsigDW);
        for (int in =0; in < allNodeIds.size(); in++)
        {
          const MaterialNode* node = _networkInteraction->getMaterialNode(allNodeIds[in]);
          fullMatrix<double>& DLDW = (*DLDWeights)[node];
          fullMatrixOperation::getRowColTangent(dim,0,0,1,2,row,col);
          int comp = Tensor23::getIndex(0,0);
          DLDW(row,col) = DsigDW(comp,in)/fact;
          DLDW(col,row) = DLDW(row,col);

          fullMatrixOperation::getRowColTangent(dim,1,1,1,2,row,col);
          comp = Tensor23::getIndex(1,1);
          DLDW(row,col) = DsigDW(comp,in)/fact;
          DLDW(col,row) = DLDW(row,col);

          fullMatrixOperation::getRowColTangent(dim,2,2,1,2,row,col);
          comp = Tensor23::getIndex(2,2);
          DLDW(row,col) = DsigDW(comp,in)/fact;
          DLDW(col,row) = DLDW(row,col);

          fullMatrixOperation::getRowColTangent(dim,0,1,1,2,row,col);
          comp = Tensor23::getIndex(0,1);
          DLDW(row,col) = DsigDW(comp,in)/fact;
          DLDW(col,row) = DLDW(row,col);

          fullMatrixOperation::getRowColTangent(dim,0,2,1,2,row,col);
          comp = Tensor23::getIndex(0,2);
          DLDW(row,col) = DsigDW(comp,in)/fact;
          DLDW(col,row) = DLDW(row,col);

          fullMatrixOperation::getRowColTangent(dim,1,2,1,2,row,col);
          comp = Tensor23::getIndex(1,2);
          DLDW(row,col) = DsigDW(comp,in)/fact;

        }
      }
      //
      for (NetworkInteraction::InteractionContainer::const_iterator it = allInteractions.begin(); it != allInteractions.end(); it++)
      {
        const InteractionMechanism* im = it->second;;
        computeDStressDInteraction(*coeffReduc,*im,DsigDCoeffVars,DsigDnormalVars);
        int numberUnknownPerIm = coeffReduc->getNumberOfCoefficientVars(*_networkInteraction,*im);
        if (numberUnknownPerIm > 0)
        {
          std::vector<fullMatrix<double> >& matCoeffs = (*DLDCoeffVars)[im];
          for (int j=0; j< numberUnknownPerIm; j++)
          {
            fullMatrix<double>& DLDalp = matCoeffs[j];
            fullMatrixOperation::getRowColTangent(dim,0,0,1,2,row,col);
            int comp = Tensor23::getIndex(0,0);
            DLDalp(row,col) = DsigDCoeffVars(comp,j)/fact;
            DLDalp(col,row) = DLDalp(row,col);

            fullMatrixOperation::getRowColTangent(dim,1,1,1,2,row,col);
            comp = Tensor23::getIndex(1,1);
            DLDalp(row,col) = DsigDCoeffVars(comp,j)/fact;
            DLDalp(col,row) = DLDalp(row,col);

            fullMatrixOperation::getRowColTangent(dim,2,2,1,2,row,col);
            comp = Tensor23::getIndex(2,2);
            DLDalp(row,col) = DsigDCoeffVars(comp,j)/fact;
            DLDalp(col,row) = DLDalp(row,col);

            fullMatrixOperation::getRowColTangent(dim,0,1,1,2,row,col);
            comp = Tensor23::getIndex(0,1);
            DLDalp(row,col) = DsigDCoeffVars(comp,j)/fact;
            DLDalp(col,row) = DLDalp(row,col);

            fullMatrixOperation::getRowColTangent(dim,0,2,1,2,row,col);
            comp = Tensor23::getIndex(0,2);
            DLDalp(row,col) = DsigDCoeffVars(comp,j)/fact;
            DLDalp(col,row) = DLDalp(row,col);

            fullMatrixOperation::getRowColTangent(dim,1,2,1,2,row,col);
            comp = Tensor23::getIndex(1,2);
            DLDalp(row,col) = DsigDCoeffVars(comp,j)/fact;
          }
        }
        std::vector<fullMatrix<double> >& matNormals = (*DLDNormalVars)[im];
        for (int j=0; j< im->getDirectionVars().size(); j++)
        {
          fullMatrix<double>& DLDnormal = matNormals[j];
          fullMatrixOperation::getRowColTangent(dim,0,0,1,2,row,col);
          int comp = Tensor23::getIndex(0,0);
          DLDnormal(row,col) = DsigDnormalVars(comp,j)/fact;
          DLDnormal(col,row) = DLDnormal(row,col);

          fullMatrixOperation::getRowColTangent(dim,1,1,1,2,row,col);
          comp = Tensor23::getIndex(1,1);
          DLDnormal(row,col) = DsigDnormalVars(comp,j)/fact;
          DLDnormal(col,row) = DLDnormal(row,col);

          fullMatrixOperation::getRowColTangent(dim,2,2,1,2,row,col);
          comp = Tensor23::getIndex(2,2);
          DLDnormal(row,col) = DsigDnormalVars(comp,j)/fact;
          DLDnormal(col,row) = DLDnormal(row,col);

          fullMatrixOperation::getRowColTangent(dim,0,1,1,2,row,col);
          comp = Tensor23::getIndex(0,1);
          DLDnormal(row,col) = DsigDnormalVars(comp,j)/fact;
          DLDnormal(col,row) = DLDnormal(row,col);

          fullMatrixOperation::getRowColTangent(dim,0,2,1,2,row,col);
          comp = Tensor23::getIndex(0,2);
          DLDnormal(row,col) = DsigDnormalVars(comp,j)/fact;
          DLDnormal(col,row) = DLDnormal(row,col);

          fullMatrixOperation::getRowColTangent(dim,1,2,1,2,row,col);
          comp = Tensor23::getIndex(1,2);
          DLDnormal(row,col) = DsigDnormalVars(comp,j)/fact;
        }
      }
    }
  }
  return true;
};

bool DeepMaterialNetwork::solve(fullMatrix<double>& Fmat, fullMatrix<double>& Pmat, int maxIter, double tol, double absTol)
{
  static STensor3 F, P;
  F.setMat(Fmat);
  static STensor43 L;
  //
  bool stiff = false;
  bool ok = stress(F,P,stiff,L,maxIter,tol,absTol,true);
  if (stiff)
  {
    static fullMatrix<double> Lmat(9,9);
    for (int row = 0; row <9; row++)
    {
      int i,j;
      Tensor23::getIntsFromIndex(row,i,j);
      for (int col = 0; col <9; col++)
      {
        int k,l;
        Tensor23::getIntsFromIndex(col,k,l);
        Lmat(row,col) = L(i,j,k,l);
      }
    };
    Lmat.print("Lmat");
  }
  // done
  if (Pmat.size1() != 3 || Pmat.size2() != 3)
  {
    Pmat.resize(3,3);
  }
  P.getMat(Pmat);

  if (ok)
  {
    nextStep();
  }
  else
  {
    resetToPreviousStep();
  }

  return ok;
};

void DeepMaterialNetwork::assembleRes()
{
  zeroRHS();
  const NetworkInteraction::InteractionContainer& allInteractions = _networkInteraction->getInteractionContainer();
  for (NetworkInteraction::InteractionContainer::const_iterator it = allInteractions.begin(); it != allInteractions.end(); it++)
  {
    const InteractionMechanism* im = it->second;;
    static fullVector<double> r;
    _networkInteraction->getLinearTerm(im,r);
    std::vector<Dof> R;
    getKeys(im,R);
    for (int j=0; j<R.size(); j++)
    {
      std::map<Dof,int>::iterator it =  _unknown.find(R[j]);
      if (it != _unknown.end())
      {
        int row = it->second;
        addToRHS(row,r(j));
      }
    }
  }
  // interface keys
  const NetworkInteraction::MaterialNodeContainer& allMaterialNodes = _networkInteraction->getMaterialNodeContainer();
  for (NetworkInteraction::MaterialNodeContainer::const_iterator it = allMaterialNodes.begin(); it != allMaterialNodes.end(); it++)
  {
    const MaterialNode* nn = it->second;
    if (nn->withCohesive())
    {
      static fullVector<double> r;
      _networkInteraction->getLinearInterfaceTerm(nn,r);
      std::vector<Dof> R;
      getInterfaceKeys(nn,R);
      for (int j=0; j<R.size(); j++)
      {
        std::map<Dof,int>::iterator it =  _unknown.find(R[j]);
        if (it != _unknown.end())
        {
          int row = it->second;
          addToRHS(row,r(j));
        }
      }
    }
  }
};

void DeepMaterialNetwork::assembleDresDu()
{
  //Msg::Info("calling assembleDresDu");
  zeroMatrix();
  const NetworkInteraction::InteractionContainer& allInteractions = _networkInteraction->getInteractionContainer();
  for (NetworkInteraction::InteractionContainer::const_iterator it = allInteractions.begin(); it != allInteractions.end(); it++)
  {
    const InteractionMechanism* im = it->second;;
    static fullMatrix<double> Ke, crossKe;
    std::vector<const InteractionMechanism*> ele;
    _networkInteraction->getBilinearTerm(im,ele,Ke);
    std::vector<const MaterialNode*> relatedNodes;
    _networkInteraction->getBilinearCrossTermBulkInterface(im,relatedNodes,crossKe);
    //Ke.print("Ke");
    //
    std::vector<Dof> R, C, crossC;
    getKeys(im,R);
    for (int j=0; j< ele.size(); j++)
    {
      getKeys(ele[j],C);
    }
    for (int j=0; j< relatedNodes.size(); j++)
    {
      getInterfaceKeys(relatedNodes[j],crossC);
    }
    //
    for (int iR=0; iR < R.size(); iR++)
    {
      std::map<Dof,int>::iterator itR =  _unknown.find(R[iR]);
      if (itR != _unknown.end())
      {
        int row = itR->second;
        for (int iC =0; iC < C.size(); iC++)
        {
          std::map<Dof,int>::iterator itC =  _unknown.find(C[iC]);
          if (itC != _unknown.end())
          {
            int col = itC->second;
            addToMatrix(row,col,Ke(iR,iC));
          };
        };
        for (int iC =0; iC < crossC.size(); iC++)
        {
          std::map<Dof,int>::iterator itC =  _unknown.find(crossC[iC]);
          if (itC != _unknown.end())
          {
            int col = itC->second;
            addToMatrix(row,col,crossKe(iR,iC));
          };
        };
      };
    };
  };
  
  const NetworkInteraction::MaterialNodeContainer& allMaterialNodes = _networkInteraction->getMaterialNodeContainer();
  for (NetworkInteraction::MaterialNodeContainer::const_iterator it = allMaterialNodes.begin(); it != allMaterialNodes.end(); it++)
  {
    const MaterialNode* nn = it->second;
    if (nn->withCohesive())
    {
      std::vector<const InteractionMechanism*> ele;
      static fullMatrix<double> Ke, crossKe;
      _networkInteraction->getBiLinearInterfaceTerm(nn,Ke);
      _networkInteraction->getBilinearCrossTermInterfaceBulk(nn,ele,crossKe);
      
      std::vector<Dof> R, C, crossC;
      getInterfaceKeys(nn,R);
      getInterfaceKeys(nn,C);
      for (int j=0; j< ele.size(); j++)
      {
        getKeys(ele[j],crossC);
      }
      for (int iR=0; iR < R.size(); iR++)
      {
        std::map<Dof,int>::iterator itR =  _unknown.find(R[iR]);
        if (itR != _unknown.end())
        {
          int row = itR->second;
          for (int iC =0; iC < C.size(); iC++)
          {
            std::map<Dof,int>::iterator itC =  _unknown.find(C[iC]);
            if (itC != _unknown.end())
            {
              int col = itC->second;
              addToMatrix(row,col,Ke(iR,iC));
            };
          };
          for (int iC =0; iC < crossC.size(); iC++)
          {
            std::map<Dof,int>::iterator itC =  _unknown.find(crossC[iC]);
            if (itC != _unknown.end())
            {
              int col = itC->second;
              addToMatrix(row,col,crossKe(iR,iC));
            };
          };
        };
      };
    }
  }
};

void DeepMaterialNetwork::updateFieldFromUnknown()
{
  NetworkInteraction::InteractionContainer& allInteractions = _networkInteraction->getInteractionContainer();
  for (NetworkInteraction::InteractionContainer::iterator it = allInteractions.begin(); it != allInteractions.end(); it++)
  {
    InteractionMechanism* im = it->second;;
    std::vector<Dof> R;
    getKeys(im,R);
    int Rsize = R.size();
    fullVector<double> val(Rsize);
    for (int j=0; j< Rsize; j++)
    {
      std::map<Dof,int>::iterator itR =  _unknown.find(R[j]);
      if (itR != _unknown.end())
      {
        int row = itR->second;
        im->getRefToIncompatibleVector(IPStateBase::current)(j) = getFromSolution(row);
      }
    }
  };
  NetworkInteraction::MaterialNodeContainer& allMaterialNodes = _networkInteraction->getMaterialNodeContainer();
  for (NetworkInteraction::MaterialNodeContainer::iterator it = allMaterialNodes.begin(); it != allMaterialNodes.end(); it++)
  {
    MaterialNode* nn = it->second;
    if (nn->withCohesive())
    {
      int numCoh = nn->cohesiveCoeffVars.size();
      std::vector<Dof> R;
      getInterfaceKeys(nn,R);
      int Rsize = R.size();
      fullVector<double> val(Rsize);
      for (int j=0; j< Rsize; j++)
      {
        std::map<Dof,int>::iterator itR =  _unknown.find(R[j]);
        if (itR != _unknown.end())
        {
          int row = itR->second;
          int index = j%numCoh;
          int comp = j/numCoh;
          nn->ipsCohesive->getRefToJump(index)(comp) = getFromSolution(row);
          //Msg::Info("index = %d; com = %d, nn->ipsCohesive->getRefToJump=%f",index,comp,nn->ipsCohesive->getRefToJump(index)(comp));
        }
      }
    }
  }
};

void DeepMaterialNetwork::nextStep()
{
  systemNextStep();
  _networkInteraction->nextStep();
};

void DeepMaterialNetwork::resetToPreviousStep()
{
  systemResetToPreviousStep();
  _networkInteraction->resetToPreviousStep();
};

void DeepMaterialNetwork::copy(const IPStateBase::whichState ws1, const IPStateBase::whichState ws2)
{
  #if defined(HAVE_GMM) || defined(HAVE_PETSC)
  _sys->copy(ws1,ws2);
  #else
  fullVector<double>* stat1(NULL), stat2(NULL);
  if (ws1 == IPStateBase::current)
  {
    stat1 = &_solCur;
  }
  else if (ws1 == IPStateBase::previous)
  {
    stat1 = &_solPrev;
  }
  else if (ws1== IPStateBase::initial)
  {
    stat1 = &_solInit;
  }
  else
  {
    Msg::Error("ws1=%d is not defined DeepMaterialNetwork::copy",ws1);
  }
  if (ws2 == IPStateBase::current)
  {
    stat2 = &_solCur;
  }
  else if (ws2 == IPStateBase::previous)
  {
    stat2 = &_solPrev;
  }
  else if (ws2== IPStateBase::initial)
  {
    stat2 = &_solInit;
  }
  else
  {
    Msg::Error("ws2=%d is not defined DeepMaterialNetwork::copy",ws2);
  }
  if (stat1 != NULL && stat2 != NULL)
  {
    (*stat2) = (*stat1);
  }
  #endif

  //
  _networkInteraction->copy(ws1,ws2);

};

bool DeepMaterialNetwork::stress(const STensor3& F, STensor3& P, bool stiff, STensor43& L, int maxIter, double tol, double absTol,
                  bool messageActive, bool reuseStiffnessMatrix)
{
  if (messageActive)
  {
    F.print("------------------------");
  }
  int iter = 0;
  bool converged = true;
  bool stiffnessEstimated = false;
  double norm0 = -1;
  double t = Cpu();
  while (true)
  {
    //evaluation at node
    downscale(F);
    //
    assembleRes();
    //
    double resNorm = getNormRHS();

    if (iter == 0)
    {
      norm0 = resNorm;
    }
    if (messageActive)
    {
      printf("iter: %d res = %e reltol %e\n",iter,resNorm,resNorm/norm0);
    }
    if (STensorOperation::isnan(resNorm))
    {
      converged = false;
      Msg::Warning("norm is nan, solve is not converge!!!");
      break;
    }

    //
    if (resNorm < absTol)
    {
      if (messageActive)
      {
        printf("converged by absolute tolerance = %.4e\n",absTol);
      }
      break;
    };

    if (resNorm > tol*norm0)
    {
      if (!reuseStiffnessMatrix)
      {
        stiffnessEstimated = true;
        assembleDresDu();
      }
      bool isSolved = systemSolve();
      if (!isSolved)
      {
        if (messageActive)
        {
          printf("solver is not successful !!!\n");
        }
        converged = false;
        break;
      }
      updateFieldFromUnknown();
      
      while ((resNorm > tol*norm0) and (_lineSearch))
      {
        int ls = _sys->lineSearch();
        if(ls == 1) break; // processus is converged or failed
        //evaluation at node
        updateFieldFromUnknown();
        downscale(F);
        //
        assembleRes();
        resNorm = getNormRHS();
        if ((resNorm < tol*norm0) or (resNorm < absTol))
        {
          converged = true;
          break;
        }
      }
      
    }
    else
    {
      if (messageActive)
      {
        printf("converged by relative tolerance = %.4e, execution time = %.5f s\n",tol,Cpu()-t);
      }
      break;
    }

    iter++;
    if (iter > maxIter)
    {
      converged = false;
      Msg::Warning("maximum number of iterations is reached, solve is not converge!!!");
      F.print("Fcur");
      break;
    };
  };

  if (converged)
  {
    // get Stress
    getStress(P);
    if (stiff)
    {
      if (!reuseStiffnessMatrix)
      {
        if (!stiffnessEstimated)
        {
          assembleDresDu();
        }
      }
      bool succ = tangentSolve(L);
    }
    //
    return true;
  }
  else
  {
    return false;
  }
};

#if defined(HAVE_GMM) || defined(HAVE_PETSC)
void DeepMaterialNetwork::allocateSystem(int numR)
{
  if (_sys != NULL)
  {
    delete _sys;
  }
  #if defined(HAVE_PETSC)
  _isAddedSparsityPattern = false;
  _sys = new nonLinearSystemPETSc<double>(PETSC_COMM_WORLD, _lineSearch);
  _sys->allocate(numR);
  //printf("PETSc is used\n");
  #else
  _sys = new nonLinearSystemGmm<double>(_lineSearch);
  dynamic_cast<nonLinearSystemGmm<double>*>(_sys)->setNoisy(2);
  _sys->allocate(numR);
  //printf("Gmm is used\n");
  #endif
};

void DeepMaterialNetwork::sparsitySystem()
{
  #if defined(HAVE_PETSC)
  const NetworkInteraction::InteractionContainer& allInteractions = _networkInteraction->getInteractionContainer();
  for (NetworkInteraction::InteractionContainer::const_iterator it = allInteractions.begin(); it != allInteractions.end(); it++)
  {
    const InteractionMechanism* im = it->second;;
    std::vector<const InteractionMechanism*> ele;
    static fullMatrix<double> mat, matCross;
    _networkInteraction->getBilinearTerm(im,ele,mat);
    std::vector<const MaterialNode*> relatedNodes;
    _networkInteraction->getBilinearCrossTermBulkInterface(im,relatedNodes,matCross);
    //
    std::vector<Dof> R, C;
    getKeys(im,R);
    for (int j=0; j< ele.size(); j++)
    {
      getKeys(ele[j],C);
    }
    for (int j=0; j< relatedNodes.size(); j++)
    {
      getInterfaceKeys(relatedNodes[j], C);
    }
    //
    for (int iR=0; iR < R.size(); iR++)
    {
      std::map<Dof,int>::iterator itR =  _unknown.find(R[iR]);
      if (itR != _unknown.end())
      {
        int row = itR->second;
        for (int iC =0; iC < C.size(); iC++)
        {
          std::map<Dof,int>::iterator itC =  _unknown.find(C[iC]);
          if (itC != _unknown.end())
          {
            int col = itC->second;
            _sys->insertInSparsityPattern(row,col);
          };
        };
      };
    };
  };
  
  const NetworkInteraction::MaterialNodeContainer& allMaterialNodes = _networkInteraction->getMaterialNodeContainer();
  for (NetworkInteraction::MaterialNodeContainer::const_iterator it = allMaterialNodes.begin(); it != allMaterialNodes.end(); it++)
  {
    const MaterialNode* nn = it->second;
    if (nn->withCohesive())
    {
      std::vector<const InteractionMechanism*> ele;
      static fullMatrix<double> mat, matCross;
      _networkInteraction->getBiLinearInterfaceTerm(nn,mat);
      _networkInteraction->getBilinearCrossTermInterfaceBulk(nn,ele,matCross);
      std::vector<Dof> R, C;
      getInterfaceKeys(nn,R);
      getInterfaceKeys(nn,C);
      for (int j=0; j< ele.size(); j++)
      {
        getKeys(ele[j],C);
      }
      //
      for (int iR=0; iR < R.size(); iR++)
      {
        std::map<Dof,int>::iterator itR =  _unknown.find(R[iR]);
        if (itR != _unknown.end())
        {
          int row = itR->second;
          for (int iC =0; iC < C.size(); iC++)
          {
            std::map<Dof,int>::iterator itC =  _unknown.find(C[iC]);
            if (itC != _unknown.end())
            {
              int col = itC->second;
              _sys->insertInSparsityPattern(row,col);
            };
          };
        };
      };
    }
  }
  #endif //HAVE_PETSC
};

void DeepMaterialNetwork::zeroRHS()
{
  #if defined(HAVE_PETSC)
  if (!_isAddedSparsityPattern)
  {
    sparsitySystem();
    _isAddedSparsityPattern = true;
  }
  #endif //HAVE_PETSC
  _sys->zeroRightHandSide();
};
void DeepMaterialNetwork::zeroMatrix()
{
  #if defined(HAVE_PETSC)
  if (!_isAddedSparsityPattern)
  {
    sparsitySystem();
    _isAddedSparsityPattern = true;
  }
  #endif //HAVE_PETSC
  _sys->zeroMatrix(nonLinearSystemBase::stiff);
};

void DeepMaterialNetwork::addToRHS(int row, const double& val)
{
  #if defined(HAVE_PETSC)
  if (!_isAddedSparsityPattern)
  {
    sparsitySystem();
    _isAddedSparsityPattern = true;
  }
  #endif //HAVE_PETSC

  _sys->addToRightHandSideMinus(row,val);
};
void DeepMaterialNetwork::addToMatrix(int row, int col, const double& val)
{
  #if defined(HAVE_PETSC)
  if (!_isAddedSparsityPattern)
  {
    sparsitySystem();
    _isAddedSparsityPattern = true;
  }
  #endif //HAVE_PETSC

  _sys->addToMatrix(row,col,val,nonLinearSystemBase::stiff);
}

double DeepMaterialNetwork::getNormRHS() const
{
  return _sys->normInfRightHandSide();
}

double DeepMaterialNetwork::getFromSolution(int row) const
{
  double val;
  _sys->getFromSolution(row,val);
  return val;
};

bool DeepMaterialNetwork::systemSolve()
{
  #if defined(HAVE_PETSC)
  if (!_isAddedSparsityPattern)
  {
    sparsitySystem();
    _isAddedSparsityPattern = true;
  }
  #endif //HAVE_PETSC

  int sol =  _sys->systemSolve();

  if (sol == 1)
    return true;
  return false;
};

bool DeepMaterialNetwork::tangentSolve(STensor43& L)
{
  // allocatioon
  _sys->allocateMultipleRHS(9);
  _sys->zeroMultipleRHS();

  //
  const NetworkInteraction::InteractionContainer& allInteractions = _networkInteraction->getInteractionContainer();
  for (NetworkInteraction::InteractionContainer::const_iterator it = allInteractions.begin(); it != allInteractions.end(); it++)
  {
    const InteractionMechanism* im = it->second;
    static fullMatrix<double> m;
    _networkInteraction->getTangentTerm(im,m);
    std::vector<Dof> R;
    getKeys(im,R);
    for (int j=0; j<R.size(); j++)
    {
      std::map<Dof,int>::iterator it =  _unknown.find(R[j]);
      if (it != _unknown.end())
      {
        int row = it->second;
        for (int col =0; col < m.size2(); col++)
        {
          _sys->addToMultipleRHSMatrix(row,col,m(j,col));
        }
      }
    }
  }
  bool ok = _sys->systemSolveMultipleRHS();
  if (!ok)
  {
    Msg::Error("system multiple RHS is not sucessfully solved");
    return false;
  }

  STensorOperation::zero(L);
  double Wroot = _networkInteraction->getTotalWeight();
  const NetworkInteraction::NodeMapContainer& allNodes = _networkInteraction->getNodeMapContainer();
  for (NetworkInteraction::NodeMapContainer::const_iterator itNode = allNodes.begin(); itNode != allNodes.end(); itNode++)
  {
    const MaterialNode* node = itNode->first;
    double fleaf = _networkInteraction->getWeight(node)/Wroot;

    const STensor43& Lleaf = _networkInteraction->getTangent(node);
    L.axpy(fleaf,Lleaf);

    const std::vector<const InteractionMechanism*>& allRelatedInteractions = itNode->second;
    for (int j=0; j< allRelatedInteractions.size(); j++)
    {
      const InteractionMechanism* im = allRelatedInteractions[j];
      std::vector<Dof> R;
      getKeys(im,R);

      static STensor33 dFlocalDa;
      im->getDInducedStrainDa(node->numberIndex,dFlocalDa);
      //
      static fullMatrix<double> DaDF(3,9);
      DaDF.setAll(0.);
      for (int row = 0; row <3; row++)
      {
        std::map<Dof,int>::const_iterator itR = _unknown.find(R[row]);
        if (itR != _unknown.end())
        {
          int Nrow = itR->second;
          for (int col = 0; col <9; col++)
          {
            double& val = DaDF(row,col);
            _sys->getFromMultipleRHSSolution(Nrow,col,val);
          }
        }
      }

      for (int r=0; r<3; r++)
      {
        for (int t=0; t<3; t++)
        {
          int indexF = Tensor23::getIndex(r,t);
          for (int p=0; p<3; p++)
          {
            for (int q=0; q<3; q++)
            {
              for (int a=0; a<3; a++)
              {
                for (int b=0; b<3; b++)
                {
                  for (int s=0; s<3; s++)
                  {
                    L(p,q,r,t) -= fleaf*Lleaf(p,q,a,b)*dFlocalDa(a,b,s)*DaDF(s,indexF);
                  }
                }
              }
            }
          }
        }
      }
    }
  };
  //L.print("homogenized tangent");
  return true;
}

bool DeepMaterialNetwork::computeDUnknownDWeights(const CoefficientReduction& coeffReduc, std::vector<int>& allNodeIds, fullMatrix<double>& DaDW)
{
  _networkInteraction->getAllMaterialNodeIds(allNodeIds);
  int numRHS = allNodeIds.size();
  // allocatioon
  _sys->allocateMultipleRHS(numRHS);
  _sys->zeroMultipleRHS();

  // for assemblage
  std::map<int, int> nodeMapPosition;
  for (int j=0; j< numRHS; j++)
  {
    nodeMapPosition[allNodeIds[j]] = j;
  };

  const NetworkInteraction::InteractionContainer& allInteractions = _networkInteraction->getInteractionContainer();
  for (NetworkInteraction::InteractionContainer::const_iterator it = allInteractions.begin(); it != allInteractions.end(); it++)
  {
    const InteractionMechanism* im = it->second;
    static std::vector<int> allIds;
    static fullMatrix<double> DrDW;
    _networkInteraction->getDLinearTermDWeights(im,allIds,DrDW);
    // assemble
    std::vector<Dof> R;
    getKeys(im,R);
    for (int j=0; j<R.size(); j++)
    {
      std::map<Dof,int>::iterator itUnknown =  _unknown.find(R[j]);
      if (itUnknown != _unknown.end())
      {
        int row = itUnknown->second;
        // first cols from coefficients
        for (int k =0; k < allIds.size(); k++)
        {
          std::map<int, int>::const_iterator itFind = nodeMapPosition.find(allIds[k]);
          if (itFind == nodeMapPosition.end())
          {
            Msg::Error("ff Material node %d is not found",allIds[k]);
            Msg::Exit(0);
          }
          else
          {
            int col = itFind->second;
            _sys->addToMultipleRHSMatrix(row,col,DrDW(j,k));
          }
        }
      }
    }
    // weights thourgh a cofficient-weight dependency
    for (NetworkInteraction::InteractionContainer::const_iterator itOther = allInteractions.begin(); itOther != allInteractions.end(); itOther++)
    {
      const InteractionMechanism* jm = itOther->second;
      static fullMatrix<double> DrDcoeffs;
      _networkInteraction->getDLinearTermDCoefficient(im,jm,DrDcoeffs);
      static std::vector<int> allNodesJm, allNodesJmDependent;
      jm->getAllMaterialNodeIds(allNodesJm);
      for (int j=0; j< allNodesJm.size(); j++)
      {
        static std::vector<double> DalphaDW;
        coeffReduc.getDCoefficientsDWeights(*_networkInteraction, *jm, allNodesJm[j], allNodesJmDependent,DalphaDW);
        if (allNodesJmDependent.size() > 0)
        {
          for (int p=0; p<R.size(); p++)
          {
            std::map<Dof,int>::iterator itUnknown =  _unknown.find(R[p]);
            if (itUnknown != _unknown.end())
            {
              int row = itUnknown->second;
              for (int q=0; q< allNodesJmDependent.size(); q++)
              {
                std::map<int, int>::const_iterator itFind = nodeMapPosition.find(allNodesJmDependent[q]);
                if (itFind == nodeMapPosition.end())
                {
                  Msg::Error("fff Material node %d is not found",allNodesJmDependent[q]);
                  Msg::Exit(0);
                }
                else
                {
                  double val = DrDcoeffs(p,j)*DalphaDW[q];
                  int col = itFind->second;
                  _sys->addToMultipleRHSMatrix(row,col,val);
                }
              }
            }
          }
        }
      }
    }
    //
  }
  bool ok = _sys->systemSolveMultipleRHS();
  if (!ok)
  {
    Msg::Error("system multiple RHS is not sucessfully solved");
    return false;
  }
  if (DaDW.size1() != _unknown.size() || DaDW.size2() != numRHS)
  {
    DaDW.resize(_unknown.size(), numRHS,true);
  }
  else
  {
    DaDW.setAll(0.);
  };

  for (int row=0; row < _unknown.size(); row++)
  {
    for (int col =0; col < numRHS; col++)
    {
      double val = 0;
      _sys->getFromMultipleRHSSolution(row,col,val);
      DaDW(row,col) = -val;
    }
  };
  return true;
}

bool DeepMaterialNetwork::computeDUnknownDInteraction(const CoefficientReduction& coeffReduc, const InteractionMechanism& im, fullMatrix<double>& DaDCoeffVars, fullMatrix<double>& DaDnormalVars)
{
  int numCoeffVars = coeffReduc.getNumberOfCoefficientVars(*_networkInteraction, im);
  int numDirVars = im.getDirectionVars().size();

  int numRHS = numCoeffVars + numDirVars;

  if (numRHS == 0) return true;

  // allocatioon
  _sys->allocateMultipleRHS(numRHS);
  _sys->zeroMultipleRHS();

  const NetworkInteraction::InteractionContainer& allInteractions = _networkInteraction->getInteractionContainer();
  for (NetworkInteraction::InteractionContainer::const_iterator it = allInteractions.begin(); it != allInteractions.end(); it++)
  {
    const InteractionMechanism* jm = it->second;
    static fullMatrix<double> DrDVars, DrDnormalVars;
    _networkInteraction->getDLinearTermDNormalVars(jm,&im,DrDnormalVars);

    if (numCoeffVars > 0)
    {
      static fullMatrix<double> DrDcoeffs;
      _networkInteraction->getDLinearTermDCoefficient(jm,&im,DrDcoeffs);
      //
      static std::vector<int> allNodeIm;
      im.getAllMaterialNodeIds(allNodeIm);
      static fullMatrix<double> DcoeffsDVars;
      fullMatrixOperation::allocateAndMakeZero(DcoeffsDVars,DrDcoeffs.size2(),numCoeffVars);
      for (int j=0; j< allNodeIm.size(); j++)
      {
        static std::vector<double> DalphaVars;
        coeffReduc.getDCoefficientsDCoefficientVars(*_networkInteraction,im,allNodeIm[j],DalphaVars);
        for (int k=0; k< numCoeffVars; k++)
        {
          DcoeffsDVars(j,k) = DalphaVars[k];
        }
      }
      fullMatrixOperation::allocateAndMakeZero(DrDVars,DrDcoeffs.size1(),numCoeffVars);
      DrDcoeffs.mult(DcoeffsDVars,DrDVars);
    };
    // assemble
    std::vector<Dof> R;
    getKeys(jm,R);
    for (int j=0; j<R.size(); j++)
    {
      std::map<Dof,int>::iterator itUnknown =  _unknown.find(R[j]);
      if (itUnknown != _unknown.end())
      {
        int row = itUnknown->second;
        if (numCoeffVars > 0)
        {
          // first cols from coefficients
          for (int col =0; col < DrDVars.size2(); col++)
          {
            _sys->addToMultipleRHSMatrix(row,col,DrDVars(j,col));
          }
        }
        // net cols from normals
        for (int col =0; col < DrDnormalVars.size2(); col++)
        {
          _sys->addToMultipleRHSMatrix(row,numCoeffVars+col,DrDnormalVars(j,col));
        }
      }
    }
  }
  bool ok = _sys->systemSolveMultipleRHS();
  if (!ok)
  {
    Msg::Error("system multiple RHS is not sucessfully solved");
    return false;
  }

  if (numCoeffVars > 0)
  {
    fullMatrixOperation::allocateAndMakeZero(DaDCoeffVars,_unknown.size(), numCoeffVars);
  }
  fullMatrixOperation::allocateAndMakeZero(DaDnormalVars,_unknown.size(),numDirVars);

  double val = 0;
  for (int row=0; row < _unknown.size(); row++)
  {
    for (int col =0; col < numCoeffVars; col++)
    {
      _sys->getFromMultipleRHSSolution(row,col,val);
      DaDCoeffVars(row,col) = -val;
    }

    for (int col =0; col < numDirVars; col++)
    {
      _sys->getFromMultipleRHSSolution(row,col+numCoeffVars,val);
      DaDnormalVars(row,col) = -val;
    }
  };
  return true;
};

void DeepMaterialNetwork::systemNextStep()
{
  #if defined(HAVE_PETSC)
  if (!_isAddedSparsityPattern)
  {
    sparsitySystem();
    _isAddedSparsityPattern = true;
  }
  #endif //HAVE_PETSC

  _sys->nextStep();
}
void DeepMaterialNetwork::systemResetToPreviousStep()
{
  #if defined(HAVE_PETSC)
  if (!_isAddedSparsityPattern)
  {
    sparsitySystem();
    _isAddedSparsityPattern = true;
  }
  #endif //HAVE_PETSC

  _sys->resetUnknownsToPreviousTimeStep();
}
#else
void DeepMaterialNetwork::allocateSystem(int numR)
{
  _K.resize(numR,numR,true);
  _res.resize(numR,true);
  _solCur.resize(numR,true);
  _solPrev.resize(numR,true);
  _solInit.resize(numR,true);
  _x.resize(numR,true);
  printf("full system lu is used \n");
};

void DeepMaterialNetwork::sparsitySystem()
{

};

void DeepMaterialNetwork::zeroRHS()
{
  _res.setAll(0.);
}
void DeepMaterialNetwork::zeroMatrix()
{
  _K.setAll(0.);
};

void DeepMaterialNetwork::addToRHS(int row, const double& val)
{
  _res(row) += val;
};
void DeepMaterialNetwork::addToMatrix(int row, int col, const double& val)
{
  _K(row,col) += val;
}

double DeepMaterialNetwork::getNormRHS() const
{
  return _res.norm();
}

double DeepMaterialNetwork::getFromSolution(int row) const
{
  return _solCur(row);
};

bool DeepMaterialNetwork::systemSolve()
{
  bool isSolved = _K.luSolve(_res,_x);
  if (!isSolved)
  {
    printf("luSolve is not successful !!!\n");
    return false;
  }
  else
  {
    _solCur.axpy(_x,-1.);
    return true;
  }
};
bool DeepMaterialNetwork::tangentSolve(STensor43& L)
{
  Msg::Error("tangent solver requires using PETSC ");
  return false;
}

bool DeepMaterialNetwork::computeDUnknownDInteraction(const CoefficientReduction& coeffReduc, const InteractionMechanism& im,  fullMatrix<double>& DaDCoeffVars, fullMatrix<double>& DaDnormalVars)
{
  Msg::Error("computeDUnknownDInteraction requires PETSc");
  return false;
};

bool DeepMaterialNetwork::computeDUnknownDWeights(const CoefficientReduction& coeffReduc, std::vector<int>& allNodeIds, fullMatrix<double>& DaDW)
{
  Msg::Error("computeDUnknownDWeights requires PETSc");
  return false;
}

void DeepMaterialNetwork::systemNextStep()
{
  _solPrev = _solCur;
};
void DeepMaterialNetwork::systemResetToPreviousStep()
{
  _solCur = _solPrev;
};
#endif //HAVE_GMM
