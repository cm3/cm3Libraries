
//
// C++ Interface:  optimizer for gradient based training
//
//
// Author:  <V.-D. Nguyen>, (C) 2021
//
// Copyright: See COPYING file that comes with this distribution
//
//

#include "Optimizer.h"
#include <string.h>
#include <regex>
#include "OS.h"
#include <ctime>
#include <algorithm>    // std::shuffle
#include <random>       // std::default_random_engine
#include <chrono>       // std::chrono::system_clock


SGD::SGD(double momentum): _momentum(momentum){}
void SGD::initialize(int N)
{
  printf("initializing SGD optimizer");
  _vt.resize(N,true);
};

void SGD::step(double lr, const fullVector<double>& g, fullVector<double>& Wcur)
{
  if (fabs(_momentum) > 1e-10)
  {
    _vt.scale(_momentum);
    _vt.axpy(g,lr);
    Wcur.axpy(_vt,-1.);
  }
  else
  {
    Wcur.axpy(g,-lr);
  }
};

Adam::Adam(double beta1, double beta2, double eps): _beta1(beta1),_beta2(beta2),_eps(eps),
    _mt(), _vt(), _step(0.){}

void Adam::step(double lr, const fullVector<double>& g, fullVector<double>& Wcur)
{
  _step ++;
  fullVector<double> gg(g.size());
  for (int j=0; j< g.size(); j++)
  {
    gg(j) = g(j)*g(j);
  }
  //
  _mt.scale(_beta1);
  _mt.axpy(g,1.-_beta1);
  _vt.scale(_beta2);
  _vt.axpy(gg, 1.-_beta2);
  //
  static fullVector<double> mt_hat, vt_hat;
  mt_hat = _mt;
  vt_hat = _vt;
  
  double factbeta1t = 1./(1.-pow(_beta1,_step));
  double factbeta2t = 1./(1.-pow(_beta2,_step));
  
  mt_hat.scale(factbeta1t);
  vt_hat.scale(factbeta2t);
  
  for (int j=0; j< g.size(); j++)
  {
    Wcur(j) -= lr*mt_hat(j)/(sqrt(vt_hat(j)) + _eps);
  }
};
void Adam::initialize(int N)
{
  printf("initializing Adam optimizer");
  _mt.resize(N,true); 
  _vt.resize(N,true);
  _step = 0.;
};


GradientDescentTraining::GradientDescentTraining(LossFunction& lf, Optimizer& opt):
  _lossFunc(&lf),_optimizer(&opt), _learningRateScheduler(NULL), _learningRateAdaptivity(false),
  _reduceFactor(0.8), _increaseFactor(1.2)
{
  
}
GradientDescentTraining::GradientDescentTraining(LossFunction& lf, Optimizer& opt, LearningRateScheduler& lrScheduler):
  _lossFunc(&lf),_optimizer(&opt), _learningRateScheduler(&lrScheduler), _learningRateAdaptivity(false),
  _reduceFactor(0.8), _increaseFactor(1.2)
{
  
}

void GradientDescentTraining::learningRateAdaptivity(bool fl, double reduceFactor, double increaseFactor)
{
  _learningRateAdaptivity = fl;
  if (fl)
  {
    Msg::Info("with learning rate adaptivity");
    _reduceFactor = reduceFactor;
    _increaseFactor = increaseFactor;
  }
}

GradientDescentTraining::~GradientDescentTraining(){}
void GradientDescentTraining::fit(std::string lossMetric, 
             double learningRate,
             int numEpochs,
             int numberBatches,
             std::string errorMetric,
             std::string saveModelFileName, 
             std::string historyFileName,
             double lrMin)
{
  //
  auto storepPreviousData = []()
  {
    time_t tt;
    // Declaring variable to store return value of
    // localtime()
    struct tm * ti;

    // Applying time()
    time (&tt);

    // Using localtime()
    ti = localtime(&tt);
    
    //
    std::regex r("\\s+");
    std::string dirName= std::regex_replace(asctime(ti), r, "-");
    printf("now = %s\n",dirName.c_str());
    
    std::string mdir= "mkdir " + dirName;
    int oks = system(mdir.c_str());
    
    std::string mvcsv= "cp *.* " + dirName +"/";
    oks = system(mvcsv.c_str());     
  };
  
  fullVector<double> Wcur, Wprev, WBest, grad;
  int epoch, bestEpoch, Ntrain;
  double costfuncPrev, costfuncBest, lrCur;
  FILE* historyFile =NULL;
  FILE* historyFileFull =NULL;
  auto initializeVariable = [&]()
  {
    printf("start training\n");
    // open history file
    historyFile = fopen(historyFileName.c_str(),"w");
    std::string historyFileFullName = "full_"+historyFileName;
    historyFileFull = fopen(historyFileFullName.c_str(),"w");
    fprintf(historyFile,"Time;Epoch;%s-Train;%s-Test;%s-Train;%s-Test\n",lossMetric.c_str(),lossMetric.c_str(),errorMetric.c_str(),errorMetric.c_str());
    fprintf(historyFileFull,"Time;Epoch;%s-Train;%s-Test;%s-Train;%s-Test\n",lossMetric.c_str(),lossMetric.c_str(),errorMetric.c_str(),errorMetric.c_str());
    //
    _lossFunc->initializeFittingParameters(Wcur);
    int numFittingParams = Wcur.size();
    _optimizer->initialize(numFittingParams);
    printf("number of fitting parameters = %d\n",numFittingParams);
    WBest = Wcur;
    Wprev = Wcur;
    grad.resize(numFittingParams,true);
    //
    epoch = 0;
    bestEpoch = 0;
    std::vector<std::string> allMetrics;
    allMetrics.push_back(lossMetric);
    allMetrics.push_back(errorMetric);
    std::vector<double> vals;
    _lossFunc->evaluateTrainingSet(allMetrics,vals);
    costfuncBest = vals[0];
    costfuncPrev = costfuncBest;
    double errTrain =  vals[1];
    printf("initial cost function %s %s = %e %e\n",allMetrics[0].c_str(), allMetrics[1].c_str(),costfuncBest,vals[1]);
    _lossFunc->evaluateTestSet(allMetrics,vals);    
    fprintf(historyFile,"%.16g;%d;%.16g;%.16g;%.16g;%.16g\n",0.,0,costfuncBest,vals[0],errTrain,vals[1]);
    fflush(historyFile);
    fprintf(historyFileFull,"%.16g;%d;%.16g;%.16g;%.16g;%.16g\n",0.,0,costfuncBest,vals[0],errTrain,vals[1]);
    fflush(historyFileFull);
    //
    Ntrain = _lossFunc->sizeTrainingSet();
    lrCur = learningRate;
    if (_learningRateScheduler != NULL && !_learningRateAdaptivity)
    {
      lrCur = _learningRateScheduler->getLr(0);
      learningRate = lrCur;
      Msg::Info("using lr = %e",lrCur);
    };
    
    if (numberBatches > Ntrain)
    {
      numberBatches = Ntrain;
    }
  };
  
  //storepPreviousData();
  initializeVariable();
  
  double tstart = Cpu();
  
  int maxNumberFails = 100;
  int numFails = 0;
  int numEpochLrMin = 0;
  int numEpochIncrease  = 0;
  
  //sfull
  std::vector<int> allIndex(Ntrain,0);
  for (int i=0; i<Ntrain; i++)
  {
    allIndex[i] = i;
  }
  auto printVector = [](const std::vector<int>& index)
  {
    printf("all Indexes:");
    for (int j=0; j< index.size(); j++)
    {
      printf(" %d",index[j]);
    }
    printf("\n");
  };
  unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
  
  while (true)
  {
    double time = Cpu();
    int idexprint =0;
    bool batchFailed = false;
    if (_learningRateScheduler != NULL && !_learningRateAdaptivity)
    {
      lrCur = _learningRateScheduler->getLr(epoch);
      Msg::Info("using lr = %e",lrCur);
    };
    std::shuffle (allIndex.begin(), allIndex.end(), std::default_random_engine(seed));
    //printVector(allIndex);
    
    for (int ibat = 0; ibat < numberBatches; ibat++)
    {
      int start = Ntrain*ibat/numberBatches;
      int end = Ntrain*(ibat+1)/numberBatches;
      int numSample = end-start;
      std::vector<int> batIndex(numSample,0);
      for (int ii = start; ii < end; ii++)
      {
        batIndex[ii-start]= allIndex[ii];
      }
      //printVector(batIndex);
      
      if ((ibat==0 || ibat == (numberBatches*(idexprint+1)/5-1)) && numberBatches > 1)
      {
        Msg::Info("minibatch %dth from %d to %d , number of samples = %d ",ibat,start,end,numSample);
        idexprint ++;
      }
      double  costfuncBatch = _lossFunc->computeCostFunction(lossMetric,batIndex,grad);
      // modify learning rate if necessagre
      if (std::isnan(grad.norm()) || std::isinf(grad.norm()) || std::isnan(costfuncBatch) || std::isinf(costfuncBatch))
      {
        //_lossFunc->reinitializeModel();        
        _lossFunc->updateModelFromFittingParameters(WBest);
        Wcur = WBest;
        //initializeVariable();
        batchFailed = true;
        numFails ++;
        break;
      }
      else 
      {
        double normWCur = Wcur.norm();
        double gradNorm = grad.norm();
        double fact = std::min(lrCur, normWCur/gradNorm);
        if (fact < 0.99*lrCur)
        {
          Msg::Info("using factor fact = %e lrCur",fact/lrCur);
        }
        _optimizer->step(fact,grad,Wcur);
        _lossFunc->updateModelFromFittingParameters(Wcur);
      }
    };
    if (batchFailed)
    {
      if (numFails < maxNumberFails)
      {
        continue;
      }
      else
      {
        printf("maximum number of fails is reached ! \n");
        break;
      }
    }
    
    std::vector<std::string> allMetrices;
    allMetrices.push_back(lossMetric);
    allMetrices.push_back(errorMetric);
    std::vector<double> vals;
    _lossFunc->evaluateTrainingSet(allMetrices,vals);
    double costfunc =  vals[0];
    double errTrain =  vals[1];
    
    _lossFunc->evaluateTestSet(allMetrices,vals);
    double lossTest = vals[0];
    double errTest = vals[1];
    fprintf(historyFileFull,"%.16g;%d;%.16g;%.16g;%.16g;%.16g\n",Cpu()-tstart,epoch+1,costfunc,lossTest,errTrain,errTest);
    fflush(historyFileFull);
    bool saveEpoch = true;
    if (_learningRateAdaptivity)
    {
      if (costfunc > costfuncPrev)
      {
        numEpochIncrease = 0;
        Msg::Info("costfunc = %e, reduce learning rate from %.5e to %.5e",costfunc,lrCur,_reduceFactor*lrCur);
        // reduce learning rate
        lrCur *= _reduceFactor;
        if (lrCur < lrMin)
        {
          lrCur = lrMin;
          numEpochLrMin ++;
          if (numEpochLrMin > 1)
          {
            Msg::Info("maximal number of iterations %d at minimal learning step is reached !!",numEpochLrMin);
            break;
          }
        }
        //
        Wcur = Wprev;
        _lossFunc->updateModelFromFittingParameters(Wprev);
        saveEpoch = false;
        continue;
      }
      else
      {
        numEpochLrMin = 0;
        numEpochIncrease ++;
        if (numEpochIncrease == 5)
        {
          Msg::Info("increase lr from  %.5e to %.5e ",lrCur,_increaseFactor*lrCur);
          numEpochIncrease = 0;
          lrCur *= _increaseFactor;
          if (lrCur > learningRate)
          {
            lrCur = learningRate;
          }
        }
        costfuncPrev = costfunc;
        Wprev = Wcur;
        saveEpoch = true;
      }
    }
    
    if (saveEpoch)
    {
        
      Msg::Info("time = %.6f, epoch = %d/%d (%.3fs) lrCur=%.5e %s-train =%.5e, %s-test =%.5e %s-train = %.5e %s-test = %.5e",
                      Cpu()-tstart,epoch,numEpochs,Cpu()-time,lrCur,lossMetric.c_str(),costfunc,lossMetric.c_str(),lossTest, 
                      errorMetric.c_str(), errTrain,  errorMetric.c_str(),errTest);
      fprintf(historyFile,"%.16g;%d;%.16g;%.16g;%.16g;%.16g\n",Cpu()-tstart,epoch+1,costfunc,lossTest,errTrain,errTest);
      fflush(historyFile);
    }
    //
    if (costfunc < costfuncBest && epoch>0)
    {
      costfuncBest = costfunc;
      WBest = Wcur;
      bestEpoch =epoch;
       // save tree
      if (saveModelFileName.length() > 0 && epoch > 0)
      {
        FILE* ff = fopen("bestEpochData.csv","w");
        fprintf(ff,"epoch = %d\nloss-train =%.5e\nloss-test =%.5e\nmare-train = %.5e\nmare-test = %.5e\n",epoch,costfuncBest,lossTest,errTrain,errTest);
        fclose(ff);
        _lossFunc->saveModel("best_"+saveModelFileName);
      }
    };
    epoch++;
    if (epoch > numEpochs)
    {
      printf("done training\n");
      break;
    }
  };
  // done training
  fclose(historyFile);
  fclose(historyFileFull);
  // save model at last time
  _lossFunc->saveModel(saveModelFileName);
  printf("done for this path %.6f (s)\n",Cpu()-tstart);
};