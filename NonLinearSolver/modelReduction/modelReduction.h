//
// C++ Interface: reduction interface
//
// Author:   <Kevin Spilker>, (C) 2019, 
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef MODELREDUCTION_H_
#define MODELREDUCTION_H_

#ifndef SWIG
#include "STensorOperations.h"
#include "ipvariable.h"
#include "Clustering.h"
#include "ClusteringData.h"
#include "mlaw.h"
template<class T>
class nonLinearSystemPETSc;
#endif //SWIG

class modelReduction
{
  public:
    enum ReductionMethod{TFA_IncrementalTangent=0, TFA_IncrementalTangentWithFrame, HS_IncrementalTangent, PFA_IncrementalTangent, 
                         TFA_HierarchicalIncrementalTangent};
  #ifndef SWIG
  public:
    modelReduction(){}
    virtual ~modelReduction(){}
    virtual modelReduction::ReductionMethod getMethod() const = 0;
    virtual void evaluate(
            const STensor3& F0,         // initial deformation gradient (input @ time n)
            const STensor3& Fn,         // updated deformation gradient (input @ time n+1)
            STensor3 &P,                // updated 1st Piola-Kirchhoff stress tensor (output)
            const ClusteringData *q0,       // array of initial internal variable
            ClusteringData *q1,             // updated array of internal variable (in ipvcur on output),
            STensor43 &Tangent,         // constitutive tangents (output)
            const bool stiff,          // if true compute the tangents
            STensor43* elasticTangent = NULL) = 0;
  #endif //SWIG
};

class ReductionTFA: public modelReduction
{
  public:
    enum solverType{PETSC=0, FULLMATRIX=1};
    
  #ifndef SWIG
  protected:
    int _solverType;
    int _dim, _tag;
    int _correction;
    bool _withFrame;
    bool _polarization;
    bool _clusterIncOri;
    bool _ownClusteringData; // true if this object has its own data about clustering
    Clustering* _clusteringData; // clustering data, manage load and save data
    std::map<int, const materialLaw*> _matlawptr; //give the material law pointer for cluster i
  #endif //SWIG
  
  public: 
    // cluster data export by Clustering
    void loadClusterSummaryAndInteractionTensorsFromFiles(const std::string clusterSummaryFileName, 
                                        const std::string interactionTensorsFileName);
    void loadClusterSummaryAndInteractionTensorsHomogenizedFrameFromFiles(const std::string clusterSummaryFileName, 
                                        const std::string interactionTensorsFileName);
    void loadClusterStandardDeviationFromFile(const std::string ClusterStrainConcentrationSTD);
    void loadElasticStrainConcentrationDerivativeFromFile(const std::string ElasticStrainConcentrationDerivative);
    void loadEshelbyInteractionTensorsFromFile(const std::string EshelbyInteractionTensorsFileName);

    void loadPlasticEqStrainConcentrationFromFile(const std::string PlasticEqStrainConcentrationFileName,
                                                  const std::string PlasticEqStrainConcentrationGeometricFileName,
                                                  const std::string PlasticEqStrainConcentrationHarmonicFileName);

    void loadInteractionTensorsMatrixFrameELFromFile(const std::string InteractionTensorsMatrixFrameELFileName);
    void loadInteractionTensorsHomogenizedFrameELFromFile(const std::string InteractionTensorsHomogenizedFrameELFileName);
    
    void loadReferenceStiffnessFromFile(const std::string ReferenceStiffnessFileName);
    void loadReferenceStiffnessIsoFromFile(const std::string ReferenceStiffnessIsoFileName);
    
    void loadInteractionTensorIsoFromFile(const std::string InteractionTensorIsoFileName);
    
    void loadClusterFiberOrientationFromFile(const std::string OrientationDataFileName);
    
    void setDim(int d);
    int getDim() const;
    
    void setClusterIncOrientation(){_clusterIncOri=true;}
    
    void addClusterLawToMap(int clusterNb, materialLaw* clusterLaw);
    
    #ifndef SWIG
    ReductionTFA(int solver, int dim, int correction, bool withFrame = false, bool polarization = false, int tag=1000);
    virtual ~ReductionTFA();
    //virtual bool withEnergyDissipation() const {return false;};
    virtual const materialLaw* getMaterialLaw(int cluster) const;
    virtual void getAllClusterIndices(std::vector<int>&allCl) const {_clusteringData->getAllClusterIndices(allCl);}
    virtual void getAllClusterVolumes(std::vector<double>&allVolumes) const;
    virtual int getNumberOfClusters() const {return _clusteringData->getTotalNumberOfClusters();};
    virtual void initializeClusterMaterialMap(const std::map<int,materialLaw*> &maplaw);
    virtual modelReduction::ReductionMethod getMethod() const = 0;
    virtual void evaluate(
            const STensor3& F0,         // initial deformation gradient (input @ time n)
            const STensor3& Fn,         // updated deformation gradient (input @ time n+1)
            STensor3 &P,                // updated 1st Piola-Kirchhoff stress tensor (output)
            const ClusteringData *q0,       // array of initial internal variable
            ClusteringData *q1,             // updated array of internal variable (in ipvcur on output),
            STensor43 &Tangent,         // constitutive tangents (output)
            const bool stiff,          // if true compute the tangents
            STensor43* elasticTangent = NULL);           
            
  protected:
    nonLinearSystemPETSc<double>* _sys;
    bool _isInitialized;
    std::map<Dof,int> _unknown; // 
    std::map<Dof, double> _fixedDof;
    
    //
    // when fullMatrix is used
    fullMatrix<double> _K;
    fullVector<double> _res, _x, _solCur;
    //std::vector<fullVector<double> > _multipleRHS, _multipleSolution;
    fullMatrix<double> _multipleRHS, _multipleSolution;

  protected:
    bool luSolve(fullMatrix<double>& A, const fullMatrix<double> &rhs, fullMatrix<double> &result);
    solverType getSolverType() const {return (solverType)_solverType;}
    void createSystem(int nbR, nonLinearSystemPETSc<double>* &NLsystem);
    void initialZeroSolution(nonLinearSystemPETSc<double>* &NLsystem);
    void addToSolution(int row, double val, nonLinearSystemPETSc<double>* &NLsystem);
    void addToRHS(int row, double val, nonLinearSystemPETSc<double>* &NLsystem);
    void addToMatrix(int row, int cretuol, double val, nonLinearSystemPETSc<double>* &NLsystem);
    double getFromSolution(int row, nonLinearSystemPETSc<double>* &NLsystem) const;
    double normInfRightHandSide(nonLinearSystemPETSc<double>* &NLsystem) const;
    int systemSolve(nonLinearSystemPETSc<double>* &NLsystem);
    void zeroRHS(nonLinearSystemPETSc<double>* &NLsystem);
    void zeroMatrix(nonLinearSystemPETSc<double>* &NLsystem);
    void allocateMultipleRHS(int dim, nonLinearSystemPETSc<double>* &NLsystem);
    void zeroMultipleRHS(nonLinearSystemPETSc<double>* &NLsystem);
    void addToMultipleRHSMatrix(int row,int col,double val, nonLinearSystemPETSc<double>* &NLsystem);
    bool systemSolveMultipleRHS(nonLinearSystemPETSc<double>* &NLsystem);
    double getFromMultipleRHSSolution(int Nrow,int col, nonLinearSystemPETSc<double>* &NLsystem) const;
    // function for system
  protected:
    void initSolve(const ClusteringData *q0,  const ClusteringData *q1, nonLinearSystemPETSc<double>* &NLsystem);
    void preallocateSystem(const ClusteringData *q0,  const ClusteringData *q1, nonLinearSystemPETSc<double>* &NLsystem);
    void numberDof();
    void setInitialState(const STensor3& F0, const STensor3& Fn);
    void assembleRes(const ClusteringData *q0,  const ClusteringData *q1);
    void assembleDresDu(const ClusteringData *q0,  const ClusteringData *q1);
    void updateFieldFromUnknown(const ClusteringData *q0,  ClusteringData *q1);
    bool systemSolve(const STensor3& F0, const STensor3& Fn,
                     const ClusteringData *q0,  ClusteringData *q1,  
                     STensor3& P, bool stiff, STensor43& L, 
                     int maxIter, double tol, double absTol, 
                     bool messageActive=false);
                
    bool tangentSolve(nonLinearSystemPETSc<double>* &NLsystem, std::map<Dof,int> &unknowns, const ClusteringData *q0, const ClusteringData *q1, STensor43& L);
    
    virtual void localConstitutive(const STensor3& F0, const STensor3& Fn, const ClusteringData *q0,  ClusteringData *q1) = 0;
    virtual void fixDofs(); 
    virtual void getKeys(int clusterId, std::vector<Dof>& keys) const;
    virtual void getDLocalStrainDKeys(int clusterId, const ClusteringData *q0,  const ClusteringData *q1, std::vector<STensor3>& keys) const;
    virtual void getInitialDofValue(int clusterId, const STensor3& F0, const STensor3& Fn, fullVector<double>& val) const;
    virtual void updateLocalFieldFromDof(int clusterId, const fullVector<double>& vals, const ClusteringData *q0,  ClusteringData *q1);
    virtual void getLinearTerm(int clusterId, const ClusteringData *q0,  const ClusteringData *q1, fullVector<double>& res) const;
    virtual void getBiLinearTerm(int clusterId, const ClusteringData *q0,  const ClusteringData *q1, std::vector<int>& othersId, fullMatrix<double>& dres) const;
    virtual void getTangentTerm(int clusterId, const ClusteringData *q0,  const ClusteringData *q1, fullMatrix<double>& DresDdefo) const;
    //////////////////////////////////////////////////////////////////////////////////////////
    virtual void getLinearTermWithFrame(int clusterId, const ClusteringData *q0,  const ClusteringData *q1, fullVector<double>& res) const;
    virtual void getBiLinearTermWithFrame(int clusterId, const ClusteringData *q0,  const ClusteringData *q1, std::vector<int>& othersId, fullMatrix<double>& dres) const;
    //////////////////////////////////////////////////////////////////////////////////////////
    virtual void getJac_rs(int r, int s, const ClusteringData *q0,  const ClusteringData *q1, STensor43& Jac_rs) const;
    
    virtual void getLinearTermHS(int clusterId, const ClusteringData *q0,  const ClusteringData *q1, fullVector<double>& res) const;
    virtual void getBiLinearTermHS(int clusterId, const ClusteringData *q0,  const ClusteringData *q1, std::vector<int>& othersId, fullMatrix<double>& dres) const;
    virtual void getJacHS_rs(int r, int s, const ClusteringData *q0,  const ClusteringData *q1, STensor43& Jac_rs) const;
    
    virtual void getLinearTermPFA(int clusterId, const ClusteringData *q0,  const ClusteringData *q1, fullVector<double>& res) const;
    virtual void getBiLinearTermPFA(int clusterId, const ClusteringData *q0,  const ClusteringData *q1, std::vector<int>& othersId, fullMatrix<double>& dres) const;
    virtual void getJacPFA_rs(int r, int s, const ClusteringData *q0,  const ClusteringData *q1, STensor43& Jac_rs) const;
    
    virtual void elasticStiffnessHom(STensor43& CelHom) const;
    virtual void elasticTotalTrialStressHom(const ClusteringData *q, STensor3& PtrHom) const;
    virtual void elasticIncrementalTrialStressHom(const ClusteringData *q0, const ClusteringData *q1, STensor3& DPtrHom) const;
    
    virtual void secantStiffnessHom(const ClusteringData *q0, const ClusteringData *q1, STensor43& CsecHom) const;
    virtual void secantStiffness(int r, const ClusteringData *q0, const ClusteringData *q1, STensor43& Csec) const;
    virtual void dsecantShearModulusOveralldeps(const ClusteringData *q0, const ClusteringData *q1, int s, STensor3& dGHomdeps_s) const;
    virtual void dtangentShearModulusOveralldeps(const ClusteringData *q0, const ClusteringData *q1, int s, STensor3& dGHomdeps_s) const;

    
    virtual double totalVolume() const;
    virtual double clusterVolume(int r) const;
    
    // some functions
    virtual const STensor43& getClusterAverageStrainConcentrationTensor(int cl) const; // matrix A
    virtual const STensor43& getClusterInteractionTensor(int icl, int jcl) const; // matrix D
    virtual const materialLaw* getLawForCluster(int cl) const;
    
    const STensor43& getClusterStrainConcentrationStDTensor(int cl) const;
    const double& getClusterStrainConcentrationStDScalar(int cl) const;
    double getClusterNonUniformCorrection(int cl, const STensor3& macroeps0, const STensor3& macroeps) const;
    
    virtual const STensor43& getClusterAverageStrainConcentrationTensorHierarchical(std::vector<int> clIDsHierarchical) const; // matrix A
    virtual const STensor43& getClusterInteractionTensorHierarchical(std::vector<int> clHostIDs, int icl, int jcl) const; // matrix D
    
    const STensor43& getClusterInteractionTensorIso(int icl, int jcl) const; // matrix Diso
    
    const fullVector<double>& getClusterMeanFiberOrientation(int cl) const;
    
    #endif // SWIG
};

class IncrementalTangentTFA : public ReductionTFA
{
  public:
    IncrementalTangentTFA(int solverType, int dim, int correction, bool withFrame, bool polarization, int tag=1000);
  #ifndef SWIG
    virtual ~IncrementalTangentTFA();
    virtual ReductionMethod getMethod() const {return modelReduction::TFA_IncrementalTangent;};

  protected:
    virtual void localConstitutive(const STensor3& F0, const STensor3& Fn, const ClusteringData *q0,  ClusteringData *q1);
  #endif // SWIG
};
class IncrementalTangentTFAWithFrame : public ReductionTFA
{
  public:
    IncrementalTangentTFAWithFrame(int solverType, int dim, int correction, bool withFrame, bool polarization, int tag=1000);
  #ifndef SWIG
    virtual ~IncrementalTangentTFAWithFrame();
    virtual ReductionMethod getMethod() const {return modelReduction::TFA_IncrementalTangentWithFrame;};

  protected:
    virtual void localConstitutive(const STensor3& F0, const STensor3& Fn, const ClusteringData *q0,  ClusteringData *q1);
  #endif // SWIG
};
class IncrementalTangentHS : public ReductionTFA
{
  public:
    IncrementalTangentHS(int solverType, int dim, int correction, bool withFrame, bool polarization, int tag=1000);
  #ifndef SWIG
    virtual ~IncrementalTangentHS();
    virtual ReductionMethod getMethod() const {return modelReduction::HS_IncrementalTangent;};

  protected:
    virtual void localConstitutive(const STensor3& F0, const STensor3& Fn, const ClusteringData *q0,  ClusteringData *q1);
  #endif // SWIG
};
class IncrementalTangentPFA : public ReductionTFA
{
  public:
    IncrementalTangentPFA(int solverType, int dim, int correction, bool withFrame, bool polarization, int tag=1000);
  #ifndef SWIG
    virtual ~IncrementalTangentPFA();
    virtual ReductionMethod getMethod() const {return modelReduction::PFA_IncrementalTangent;};

  protected:
    virtual void localConstitutive(const STensor3& F0, const STensor3& Fn, const ClusteringData *q0,  ClusteringData *q1);
  #endif // SWIG
};




class HierarchicalReductionTFA : public ReductionTFA
{
  #ifndef SWIG
  protected:
    std::map<int,std::map<int, const materialLaw*>> _matlawptrLevel1; //give the material law pointer for cluster on level 1
  #endif
  
  protected:
    int _nbLevels;
    int _level;
  
  public:
    HierarchicalReductionTFA(int solver, int dim, int correction=0, bool withFrame = false, bool polarization = false, int tag=1000);
    void loadClusterSummaryAndInteractionTensorsLevel1FromFiles(const std::string clusterSummaryFileName, const std::string interactionTensorsFileName);
  
    #ifndef SWIG
    virtual ~HierarchicalReductionTFA();
    virtual void getAllClusterIndicesLevel1(std::map<int,std::vector<int>>& allCl) const {_clusteringData->getAllClusterIndicesLevel1(allCl);}
    virtual void getAllClusterVolumesLevel1(std::map<int,std::vector<double>>&allVolumes) const;
    virtual ReductionMethod getMethod() const {Msg::Error("getMethod() not implemented in HierarchicalReductionTFA");};
    
    virtual const materialLaw* getMaterialLaw(std::vector<int> clIDsHierarchical) const;
    
    virtual int getNumberOfLevels() const {return _clusteringData->getNumberOfLevels();};
    virtual int getNumberOfSubClusters(std::vector<int> clHostIDs) const {return _clusteringData->getNumberOfSubClusters(clHostIDs);};
    
    virtual void initializeClusterTFAMaterialMapLevel0(const std::map<int,materialLaw*> &maplawLv0, int tfaLawID);
    
    //virtual void initializeClusterTFAMaterialMapLevel1(const std::map<int,materialLaw*> &maplawLv0); //in case of >2 levels    
    virtual void initializeClusterMaterialMapLevel1(const std::map<int,materialLaw*> &maplawLv0);  //in case of 2 levels
    
    virtual void evaluate(
            const STensor3& F0,         // initial deformation gradient (input @ time n)
            const STensor3& Fn,         // updated deformation gradient (input @ time n+1)
            STensor3 &P,                // updated 1st Piola-Kirchhoff stress tensor (output)
            const ClusteringData *q0,       // array of initial internal variable
            const std::map<int,ClusteringData*> q0Level1,
            ClusteringData *q1,             // updated array of internal variable (in ipvcur on output),
            std::map<int,ClusteringData*> q1Level1,
            STensor43 &Tangent,         // constitutive tangents (output)
            const bool stiff,          // if true compute the tangents
            STensor43* elasticTangent = NULL);   
    
  protected:
    nonLinearSystemPETSc<double>* _sysHierarchical;
    std::map<Dof, int> _unknownHierarchical; // 
    std::map<Dof, double> _fixedDofHierarchical;
    
    /// to implement: maps of the solution systems on level 1. For more levels, maps need to be extended by one dimension per level. ////
    std::map<int, nonLinearSystemPETSc<double>*> _sysLevel1;
    std::map<int,bool> _isInitializedLevel1;
    std::map<int,std::map<Dof, int>> _unknownLevel1;
    std::map<int,std::map<Dof, double>> _fixedDofLevel1;
    
    nonLinearSystemPETSc<double>*& getRefToNLSystem(std::vector<int> clHostIDs);
    const bool& getConstRefToInitialized(std::vector<int> clHostIDs);
    bool& getRefToInitialized(std::vector<int> clHostIDs);
    std::map<Dof, int>& getRefToUnknown(std::vector<int> clHostIDs);
    const std::map<Dof, double>& getConstRefToFixedDof(std::vector<int> clHostIDs);
    std::map<Dof, double>& getRefToFixedDof(std::vector<int> clHostIDs);
    
    bool systemSolveHierarchical(const STensor3& F0, const STensor3& Fn,
                     const ClusteringData *q0,       // array of initial internal variable
                     const std::map<int,ClusteringData*> q0Level1,
                     ClusteringData *q1,             // updated array of internal variable (in ipvcur on output),
                     std::map<int,ClusteringData*> q1Level1,
                     STensor3& P, bool stiff, STensor43& L, 
                     int maxIter, double tol, double absTol, 
                     bool messageActive=false);
    
    bool systemSolveBottomLevel(const STensor3& F0, const STensor3& Fn,
                                const ClusteringData* q0,
                                ClusteringData* q1,
                                STensor3& P, bool stiff, STensor43& L, 
                                int maxIter, double tol, double absTol, bool messageActive=false);
                                                                  
    void setInitialStateHierarchical(const ClusteringData *q0, const STensor3& F0, const STensor3& Fn);
    virtual void getInitialDofValueHierarchical(std::vector<int> clIDsHierarchical, const STensor3& F0, const STensor3& Fn, fullVector<double>& val) const;
    
    virtual void getLinearTermHierarchical(int clusterId, const ClusteringData *q0,  const ClusteringData *q1, fullVector<double>& res) const;
    virtual void getBiLinearTermHierarchical(int clusterId, const ClusteringData *q0,  const ClusteringData *q1, std::vector<int>& othersId, fullMatrix<double>& dres) const;
    virtual void getJac_rsHierarchical(int r, int s, const ClusteringData *q0,  const ClusteringData *q1, STensor43& Jac_rs) const;
    
    virtual void getLinearTermHSHierarchical(int clusterId, const ClusteringData *q0,  const ClusteringData *q1, fullVector<double>& res) const;
    virtual void getBiLinearTermHSHierarchical(int clusterId, const ClusteringData *q0,  const ClusteringData *q1, std::vector<int>& othersId, fullMatrix<double>& dres) const;
    virtual void getJacHS_rsHierarchical(int r, int s, const ClusteringData *q0,  const ClusteringData *q1, STensor43& Jac_rs) const;
        
    void initSolveHierarchical(const ClusteringData *q0,  const ClusteringData *q1, nonLinearSystemPETSc<double>* &NLsystem);
    void preallocateSystemHierarchical(const ClusteringData *q0,  const ClusteringData *q1, nonLinearSystemPETSc<double>* &NLsystem);
    virtual void fixDofsHierarchical(const ClusteringData *q0);
    void numberDofHierarchical(const ClusteringData *q0);
    
    void assembleResHierarchical(const ClusteringData *q0,  const ClusteringData *q1);
    void assembleDresDuHierarchical(const ClusteringData *q0,  const ClusteringData *q1);
    void updateFieldFromUnknownHierarchical(const ClusteringData *q0,  ClusteringData *q1);
        
    virtual void elasticStiffnessHomHierarchical(std::vector<int> clIDsHierarchical, STensor43& CelHom) const;
    
    virtual const materialLaw* getLawForCluster(std::vector<int> clIDsHierarchical) const;
    virtual void localConstitutive(const STensor3& F0, const STensor3& Fn, const ClusteringData *q0,  ClusteringData *q1)
    {Msg::Error("localConstitutive not implemented in HierarchicalReductionTFA");};
    #endif // SWIG
};

class HierarchicalIncrementalTangentTFA : public HierarchicalReductionTFA
{
  public:
    HierarchicalIncrementalTangentTFA(int solverType, int dim, int correction, bool withFrame, bool polarization, int tag=1000);
  #ifndef SWIG
    virtual ~HierarchicalIncrementalTangentTFA();
    virtual ReductionMethod getMethod() const {return modelReduction::TFA_HierarchicalIncrementalTangent;};

  protected:
    virtual void localConstitutive(const STensor3& F0, const STensor3& Fn, const ClusteringData *q0,  ClusteringData *q1);
  #endif // SWIG
};

#endif //MODELREDUCTION_H_
