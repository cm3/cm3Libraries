//
// C++ Interface: clustering
//
//
// Author:  <V.-D. Nguyen>, (C) 2020
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef CLUSTERING_H_
#define CLUSTERING_H_

#ifndef SWIG
#include <map>
#include <set>
#include "fullMatrix.h"
#include "STensorOperations.h"
#include "scalarFunction.h"
#endif //SWIG

class nonLinearMechSolver;
class IPField;
class partDomain;
class materialLaw;
class MElement;
class Clustering 
{
  public:
    enum Output{A_00=0, A_01, A_02, A_03, A_04, A_05,
                 A_10, A_11, A_12, A_13, A_14, A_15,
                 A_20, A_21, A_22, A_23, A_24, A_25,
                 A_30, A_31, A_32, A_33, A_34, A_35,
                 A_40, A_41, A_42, A_43, A_44, A_45,
                 A_50, A_51, A_52, A_53, A_54, A_55,
                 A_norm, 
                 ClusterIndex, 
                 ClusterIndexHierarchicalGlobal,
                 PFC_0, PFC_1, PFC_2, PFC_3, PFC_4, PFC_5};
    static std::string ToString(const int i);
  protected:
    #ifndef SWIG
    // map of strain concentration tensor
    // key - material number
    // value - a map of  
    // ---------key - EleNumber+GP number using numericalMaterialBase::createTypeWithTwoInts(eleNum, gpNum) to make unique number
    // ---------value- strain concentration tensor (a matrix of size 6x6)
    std::map<int, std::map<int, fullMatrix<double> > > _concentrationTensorMap; 
    std::map<int, std::map<int, fullVector<double> > > _plasticStrainConcentrationVectorMap;
    std::map<int, std::map<int, fullVector<double> > > _plasticStrainConcentrationNormalizedVectorMap;
    std::map<int, std::map<int, fullVector<double> > > _plasticFlowDirConcentrationVectorMap;
    
    std::map<int, std::map<int, fullVector<double> > > _inelasticStrainConcentrationSeparatedVectorMap;
    
    std::map<int, std::map<int, fullVector<double> > > _inelasticStrainConcentrationVectorMap;
    std::map<int, std::map<int, fullVector<double> > > _inelasticStrainConcentrationNormalizedVectorMap;
    std::map<int, std::map<int, fullVector<double> > > _totalStrainConcentrationVectorMap; 
    std::map<int, std::map<int, fullVector<double> > > _totalStrainConcentrationNormalizedVectorMap; 
    std::map<int, std::map<int, fullVector<double> > > _plasticEqStrainConcentrationVectorMap; 
    std::map<int, std::map<int, fullVector<double> > > _plasticEqStrainConcentrationNormalizedVectorMap; 
    std::map<int, std::map<int, fullVector<double> > > _SVMPlasticVectorMap; 
    std::map<int, std::map<int, fullVector<double> > > _eigenstressEqVectorMap; 
    
    std::map<int, std::map<int, fullVector<double> > > _localOrientationVectorMap; 
    
    std::map<int, fullMatrix<double> > _localStiffnessTensorMap; 
   
    //for the ease of acesse
    // direct access of the _concentrationTensorMap
    std::map<int, fullMatrix<double>* > _directMapOfStrainConcentration;
    std::map<int, fullVector<double>* > _directMapOfPlasticStrainConcentration;
    std::map<int, fullVector<double>* > _directMapOfInelasticStrainConcentration;
    std::map<int, fullVector<double>* > _directMapOfTotalStrainConcentration; 
    std::map<int, fullVector<double>* > _directMapOfPlasticEqStrainConcentration; 
    std::map<int, fullVector<double>* > _directMapOfSVMPlastic;
    std::map<int, fullVector<double>* > _directMapOfEigenstressEq;
    
    // material map
    // key - element number and GP number -using numericalMaterialBase::createTypeWithTwoInts(eleNum, gpNum) to make unique number
    // value - material number this GP belongs to
    std::map<int, int> _materialMap;
    
    // map of Gauss point 
    // key - element number and GP number -using numericalMaterialBase::createTypeWithTwoInts(eleNum, gpNum) to make unique number
    // value - X, Y, Z, volume are stored
    std::map<int, fullVector<double> > _mapGaussPoints; 
    
    
    // key - element number and GP number - make unique number
    // value - local euler angles
    std::map<int, fullVector<double> > _mapLocalOrientation; 
    
    // map number of gauss points in each element, 
    // key - element number
    // value - number of Gauss point
    std::map<int, int> _mapElementNumberOfGPs; 
    
    
    int _level;
    
    // for computation of cluster interaction
    std::map<int,STensor3> _allEigenStrains;
    std::map<int,STensor3> _allEigenStresses;
    
    std::map<int,std::map<int,STensor3>> _allEigenStrainsLevel1;
    std::map<int,std::map<int,std::map<int,STensor3>>> _allEigenStrainsLevel2;
    std::map<int,std::map<int,std::map<int,std::map<int,STensor3>>>> _allEigenStrainsLevel3;
    std::map<int,std::map<int,std::map<int,std::map<int,std::map<int,STensor3>>>>> _allEigenStrainsLevel4;
    std::map<int,std::map<int,std::map<int,std::map<int,std::map<int,std::map<int,STensor3>>>>>> _allEigenStrainsLevel5;
    
    std::map<int,std::map<int,STensor3>> _allEigenStressesLevel1;
    std::map<int,std::map<int,std::map<int,STensor3>>> _allEigenStressesLevel2;
    std::map<int,std::map<int,std::map<int,std::map<int,STensor3>>>> _allEigenStressesLevel3;
    std::map<int,std::map<int,std::map<int,std::map<int,std::map<int,STensor3>>>>> _allEigenStressesLevel4;
    std::map<int,std::map<int,std::map<int,std::map<int,std::map<int,std::map<int,STensor3>>>>>> _allEigenStressesLevel5;
    
    // for interaction computation, 0: constrain eigenstrain field, 1: constrain eigenstress field
    int _eigenField;
    
    // cluster map
    // key - element number and GP number using numericalMaterialBase::createTypeWithTwoInts(eleNum, gpNum) to make unique number
    // value - cluster index, note that clusters are numbered from 0, -1 means this part is not clustered
    std::map<int, int> _clusterIndex; 
    
    std::map<int, int> _clusterIndexLevel0; 
    std::map<int, int> _clusterIndexHierarchicalGlobal;
    std::map<int, std::vector<int>> _clusterIndexHierarchical;
    
    // key - element number and GP number using numericalMaterialBase::createTypeWithTwoInts(eleNum, gpNum) to make unique number
    // value - vector including the pfc values
    std::map<int, fullVector<double>> _clusterPFC;
    
    //total number of cluster over all domain
    int _totalNumberOfClusters;
    
    int _totalNumberOfClustersHierarchicalGlobal;
    
    std::map<int, int> _numberOfClustersLevel1;
  
    // map of cluster index - material law number 
    std::map<int, int> _clusterMaterialMap; 
    
    std::map<int, int> _clusterMaterialMapLevel0;
    std::map<int, std::map<int, int>> _clusterMaterialMapLevel1; 
    std::map<int, std::map<int, std::map<int, int>>> _clusterMaterialMapLevel2; 
    std::map<int, std::map<int, std::map<int, std::map<int, int>>>> _clusterMaterialMapLevel3; 
    std::map<int, std::map<int, std::map<int, std::map<int, std::map<int, int>>>>> _clusterMaterialMapLevel4; 
    std::map<int, std::map<int, std::map<int, std::map<int, std::map<int, std::map<int, int>>>>>> _clusterMaterialMapLevel5; 
    
    // map of cluster index - total value in this cluster
    std::map<int, double> _clusterVolumeMap; 
    
    std::map<int, double> _clusterVolumeMapLevel0; 
    std::map<int, std::map<int, double>> _clusterVolumeMapLevel1;
    std::map<int, std::map<int, std::map<int, double>>> _clusterVolumeMapLevel2; 
    std::map<int, std::map<int, std::map<int, std::map<int, double>>>> _clusterVolumeMapLevel3; 
    std::map<int, std::map<int, std::map<int, std::map<int, std::map<int, double>>>>> _clusterVolumeMapLevel4; 
    std::map<int, std::map<int, std::map<int, std::map<int, std::map<int, std::map<int, double>>>>>> _clusterVolumeMapLevel5; 
    
    // map of cluster index - spatial position of this cluster
    std::map<int, fullVector<double>> _clusterPositionMap; 
    
    // map of cluster index - spatial position of this cluster
    std::map<int, fullVector<double>> _clusterMeanFiberOrientationMap; 
    
    // map of cluster index - average strain concentration tensor in each cluster
    std::map<int, STensor43> _clusterAverageStrainConcentrationTensorMap;
    std::map<int, STensor43> _clusterSTDTensorMap;   
    std::map<int, double> _clusterSTDMap;
    std::map<int, double> _clusterNormMap;
    
    std::map<int, std::map<int, STensor43>> _clusterAverageStrainConcentrationTensorMapLevel1;
      
    // map of cluster index - plastic measures (arithmetic mean plasticEq, geometric mean plasticEq, harmonic mean plasticEq, std plasticEq)
    std::map<int, fullVector<double>> _clusterAveragePlasticStrainConcentrationVectorMap;
    std::map<int, fullVector<double>> _clusterAverageTotalStrainConcentrationVectorMap;
    std::map<int, fullVector<double>> _clusterAveragePlasticEqStrainConcentrationVectorMap;
    std::map<int, fullVector<double>> _clusterAverageGeometricPlasticEqStrainConcentrationVectorMap;
    std::map<int, fullVector<double>> _clusterAverageHarmonicPlasticEqStrainConcentrationVectorMap;
    std::map<int, fullVector<double>> _clusterAveragePowerPlasticEqStrainConcentrationVectorMap;
    std::map<int, fullVector<double>> _clusterPlSTDVectorMap;
    std::map<int, fullVector<double>> _clusterPlEqSTDVectorMap;
    
    // map of cluster index pair - interaction tensor
    std::map<std::pair<int,int>, STensor43> _clusterInteractionTensorMap;

    std::map<int, std::map<std::pair<int,int>, STensor43> > _clusterInteractionTensorMapLevel1;

    std::map<std::pair<int,int>, STensor43> _clusterEshelbyInteractionTensorMap; 
    
    std::map<std::pair<int,int>, STensor43> _clusterInteractionTensorMatrixFrameELMap; 
    std::map<std::pair<int,int>, STensor43> _clusterInteractionTensorHomogenizedFrameELMap;
    
    STensor43 _referenceStiffness;
    STensor43 _referenceStiffnessIso;

    std::map<std::pair<int,int>, STensor43> _clusterInteractionTensorIsoMap;
    
    // parameter for clustering
    // key - material law
    // value -number of clusters in this material law
    std::map<int, int> _numberOfClustersPerLaw;
    
    
    int _numberOfLevels;
    std::map<int, std::vector<int>> _numberOfClustersPerLevelPerLaw;
    
    // to compute DClusterAverageStrainConcentrationTensor DG 
    // key - number of law will be perturbated
    // value - perturbatuion law
    std::map<int,materialLaw*> _pertLaw;
    std::map<std::pair<int,int>, STensor43> _DclusterAverageStrainConcentrationTensorMapDLaw;

    #endif //SWIG
  
  public:
    Clustering();
    // solve strain concentrartion tensor at gauss points and save to file
    void solveStrainConcentration(nonLinearMechSolver& solver,
                                  int computeHomStiffness = 0,   // compute homogenized elastic stiffness: 0=no; 1=yes
                                  int homStiffnessFromLocal=0,   // 0= compute hom el. stiffness from hom. stress, strain; 1=compute hom. el. stiffness as local average over C(x):A(x)
                                  int getLocalOrientation = 0,   // for materials with microstructure, e.g. woven composite
                                  bool saveToFile=true, const std::string strainConcentrationFileName = "strainConcentrationData.csv", 
                                  bool saveLocalOrientationDataToFile=true, const std::string localOrientationFileName = "orientationData.csv");
    void solveStrainConcentrationInelastic(nonLinearMechSolver& solverInelastic,bool saveToFile=true, 
                                           const std::string PlasticStrainConcentrationFileName = "plasticStrainConcentrationData.csv",
                                           const std::string InelasticStrainConcentrationFileName = "inelasticStrainConcentrationData.csv",
                                           const std::string TotalStrainConcentrationFileName = "totalStrainConcentrationData.csv",
                                           const std::string PlasticEqStrainConcentrationFileName = "plasticEqstrainConcentrationData.csv");                                           

    // clustering can be applied using existing file of strain concentration data
    void loadStrainConcentrationDataFromFile(const std::string fileName);
    void loadPlasticStrainConcentrationDataFromFile(const std::string fileNamePlastic);
    void loadInelasticStrainConcentrationDataFromFile(const std::string fileNameInelastic);
    void loadTotalStrainConcentrationDataFromFile(const std::string fileNameTotal);
    void loadPlasticEqStrainConcentrationDataFromFile(const std::string fileNamePlasticEq);
    void loadPlasticSVMStrainConcentrationDataFromFile(const std::string fileNamePlasticSVM);
    
    void loadLocalOrientationDataFromFile(const std::string fileNameLocalOrientationData);

    // clustersing with numCluster = number of Cluster
    // clusterFileName is a saved file indicating the map between element number, GP number of this element and 
    // cluster index this element belongs to. Note that, cluster index from 0 to numCluster-1
    // after using solveStrainConcentration or loadStrainConcentrationDataFromFile
    // computeClusters can be used multiple times for clustering
    void setNumberOfClustersForMaterial(int matNum, int nbCluster);
    
    void setNumberOfLevels(int nbLevels=1);
    void setNumberOfClustersForMaterialHierarchical(int matNum, int nbClusterLevel0=1, int nbClusterLevel1=0, int nbClusterLevel2=0, int nbClusterLevel3=0, int nbClusterLevel4=0);
    
    bool computeClustersElasticity(int minNumberElementEachCluster, int maxNbIterations=10000,
                                 bool saveToFile=true, const std::string clusterFileName = "ClusterData.csv", 
                                 bool saveSummaryFile=true, const std::string summaryFileName = "ClusterDataSummary.csv",
                                 bool saveMostStrainIPFile=true, const std::string saveMostStrainIPFileName = "MostStrainingIPSummary.csv",
                                 bool saveClusterMeanFiberOrientationFile=true, const std::string saveClusterMeanFiberOrientationFileName = "ClusterMeanFiberOrientation.csv"
                                 );
                                 
    bool computeClustersElasticityHierarchical(int minNumberElementEachCluster, int maxNbIterations=10000,
                                                      bool saveToFile=true, const std::string clusterFileName = "ClusterDataHierarchical.csv", 
                                                      bool saveSummaryFile=true, const std::string summaryFileName = "ClusterDataSummaryHierarchical.csv"
                                                      );
                                 
    bool computeClustersPlasticity(int minNumberElementEachCluster, int maxNbIterations=10000,
                                 bool saveToFile=true, const std::string clusterFileName = "ClusterData.csv", 
                                 bool saveSummaryFile=true, const std::string summaryFileName = "ClusterDataSummary.csv", 
                                 bool savePlastStrainConcFile=true, std::string plastStrainconcFileName = "ClusterPlasticSummary.csv",
                                 bool savePlastEqStrainConcFile=true, std::string plastEqstrainconcFileName = "ClusterPlasticEqSummary.csv",
                                 bool saveClusterMeanFiberOrientationFile=true, const std::string saveClusterMeanFiberOrientationFileName = "ClusterMeanFiberOrientation.csv"
                                 );
                                 
    bool computeClustersLocalOrientation(int minNumberElementEachCluster, int maxNbIterations=10000,
                                         bool saveToFile=true, const std::string clusterFileName = "ClusterData.csv", 
                                         bool saveSummaryFile=true, const std::string summaryFileName = "ClusterDataSummary.csv",
                                         bool saveClusterMeanFiberOrientationFile=true, const std::string saveClusterMeanFiberOrientationFileName = "ClusterMeanFiberOrientation.csv"
                                        );
                                 
    bool computeClustersPlasticityHierarchical(int minNumberElementEachCluster, int maxNbIterations=10000,
                                                      bool saveToFile=true, const std::string clusterFileName = "ClusterDataHierarchical.csv", 
                                                      bool saveSummaryFile=true, const std::string summaryFileName = "ClusterDataSummaryHierarchical.csv"
                                                      );
    //
    bool clusteringByMeshPartition(nonLinearMechSolver& solver, int nbClusters, bool onlyLawsWithDissipation, bool saveToFile=true, const std::string clusterFileName = "ClusterData.csv");

    // save all quantities describing by Clustering::Output
    // -1 means average over all GPs of element
    // >=0 means the th-number Gauss-point
    void saveAllView(int gp=-1); 
    // each compotent in Clustering::Output can be save separately
    // -1 means average over all GPs of element
    // >=0 means the th-number Gauss-point
    void saveFieldToView(int name, int gp=-1);  // -1 means average over all GPs of element
    
    // compute interation matrix
    void computeInteractionTensor(nonLinearMechSolver& solver,
                                  materialLaw* clusterLaw,
                                  int eigenField=0,
                                  bool saveToFile=true, 
                                  const std::string saveFileName = "InteractionTensorData.csv");
                                  
    void computeInteractionTensorsHierarchical(nonLinearMechSolver& solver,
                                              materialLaw* clusterLaw,
                                              int eigenField=0,
                                              int eigenFieldLevel1=0,
                                              bool saveToFile=true, 
                                              const std::string saveFileName = "InteractionTensorDataHierarchical.csv");

    void computeEshelbyInteractionTensor(nonLinearMechSolver& solver,
                                  materialLaw* clusterLaw,
                                  int eigenField=0,
                                  bool saveToFile=true, 
                                  const std::string saveFileName = "EshelbyInteractionTensorData.csv");
          
    // to set method in ClusterDG3DMaterialLaw                
    int getEigenMethod(){return _eigenField;};
   
    // data are ready to used
    void clearAllClusterData(); // muste be cleanup the existing data before loading files
    void loadClusterSummaryFromFile(const std::string filename);
    void loadClusterSummaryHomogenizedFrameFromFile(const std::string filename);
    void loadClusterStandDevFromFile(const std::string filename);
    void loadInteractionTensorsFromFile(const std::string filename);
    void loadEshelbyInteractionTensorsFromFile(const std::string filename);
    void loadElasticStrainConcentrationDerivativeFromFile(const std::string filename);

    void loadClusterPlasticEqSummaryFromFile(const std::string filename);
    void loadClusterPlasticEqSummaryGeometricFromFile(const std::string filename);
    void loadClusterPlasticEqSummaryHarmonicFromFile(const std::string filename);
    void loadClusterPlasticEqSummaryPowerFromFile(const std::string filename);

    void loadInteractionTensorsHomogenizedFrameELFromFile(const std::string filename);
    void loadInteractionTensorsMatrixFrameELFromFile(const std::string filename);
    
    void loadReferenceStiffnessFromFile(const std::string fileNameRefStiff);
    void loadReferenceStiffnessIsoFromFile(const std::string fileNameRefStiffIso);
    
    void loadInteractionTensorIsoFromFile(const std::string filename);
    
    void loadClusterFiberOrientationFromFile(const std::string fileNameOri);
        
    void loadClusterSummaryLevel1FromFile(const std::string filename);
    void loadInteractionTensorsLevel1FromFile(const std::string filename);
    
    
    #ifndef SWIG
    ~Clustering(){}
    void computeAverageStrainCluster(const IPField* ipf , const std::set<int>& cluster, STensor3& Fhomo) const;
    void computeInteractionTensorForCluster(nonLinearMechSolver& solver, int clusterI, std::map<std::pair<int,int>, fullMatrix<double> >& DJI);
    
    void computeInteractionTensorForClusterLevel1(nonLinearMechSolver& solver, int clusterLv0, int clusterLv1, std::map<int, std::map<std::pair<int,int>, fullMatrix<double> > >& DJI);

    void computeEshelbyInteractionTensorForCluster(nonLinearMechSolver& solver, int clusterI, std::map<std::pair<int,int>, fullMatrix<double> >& DJIEshelby);

    void strainForLoadingCases(int dim, double fact, std::map<int, STensor3>& modes) const;
    void stressForLoadingCases(int dim, double fact, std::map<int, STensor3>& modes) const;
    void strainForLoadingCasesVolumePreserving(int dim, double fact, std::map<int, STensor3>& modes) const;
    void strainForInelasticLoadingCases(int dim, double fact, std::map<int, STensor3>& modes) const;
    void strainForInelasticLoadingCasesPath(int dim, double fact, std::map<int, std::map<int,piecewiseScalarFunction>>& modes) const;
    const fullMatrix<double>& getStrainConcentrationTensor(int ele, int gp) const;
    int getClusterIndex(int ele, int gp) const;
    int getClusterIndexHierarchicalGlobal(int ele, int gp) const;
    std::vector<int> getClusterIndicesHierarchical(int ele, int gp) const;
    int getMaterialLawNumberIncluster(int cluster) const;
    int getTotalNumberOfClusters() const {return _totalNumberOfClusters;};
    int getNumberOfLevels() const {return _numberOfLevels;};
    int getNumberOfSubClusters(std::vector<int> clIDsUpper) const;
    void getAllClusterIndices(std::vector<int>& allIndices) const;
    void getAllClusterIndicesLevel1(std::map<int,std::vector<int>>& allIndices) const;
    const std::map<int,int>& getMapElementNumberOfGPs() const {return _mapElementNumberOfGPs;}
    int getNbOfGPsOnElement(const int ele) const;
    void getCluster(int i, std::set<int>& cluster) const; // return a set of all ele+GP belonging to cluster i    
    
    void getClusterLevel1(int i0, int i1, std::set<int>& cluster) const; // return a set of all ele+GP belonging to cluster i    
    
    const STensor3& getEigenStrain(int GPIndex, bool homogeneous) const;
    const STensor3& getEigenStress(int GPIndex, bool homogeneous) const;
    void unity_Voigt(fullMatrix<double>& mat) const;
    
    const fullVector<double>& getPFC(int ele, int gp) const;

    // get data for reduction analyses
    const std::map<int,int>& getClusterMaterialMap() const{return _clusterMaterialMap;};
    
    const std::map<int,std::map<int,int>>& getClusterMaterialMapLevel1() const{return _clusterMaterialMapLevel1;};

    const std::map<int,double>& getClusterVolumeMap() const {return _clusterVolumeMap;};
    
    const std::map<int,double>& getClusterVolumeMapLevel0() const {return _clusterVolumeMapLevel0;};
    const std::map<int, std::map<int, double>>& getClusterVolumeMapLevel1() const {return _clusterVolumeMapLevel1;};
    
    const std::map<int,STensor43>& getClusterAverageStrainConcentrationTensorMap() const {return _clusterAverageStrainConcentrationTensorMap;};
    
    const std::map<int,std::map<int,STensor43>>& getClusterAverageStrainConcentrationTensorMapLevel1() const {return _clusterAverageStrainConcentrationTensorMapLevel1;};
    
    
    const std::map<int,STensor43>& getClusterSTDTensorMap() const {return _clusterSTDTensorMap;};
    const std::map<int,double>& getClusterSTDMap() const {return _clusterSTDMap;};
    const std::map<int,double>& getClusterNormMap() const {return _clusterNormMap;};

    const std::map<int,fullVector<double>>& getClusterAveragePlasticEqStrainConcentrationVectorMap() const {return _clusterAveragePlasticEqStrainConcentrationVectorMap;};
    const std::map<int,fullVector<double>>& getClusterAverageGeometricPlasticEqStrainConcentrationVectorMap() const {return _clusterAverageGeometricPlasticEqStrainConcentrationVectorMap;};
    const std::map<int,fullVector<double>>& getClusterAverageHarmonicPlasticEqStrainConcentrationVectorMap() const {return _clusterAverageHarmonicPlasticEqStrainConcentrationVectorMap;};
    const std::map<int,fullVector<double>>& getClusterAveragePowerPlasticEqStrainConcentrationVectorMap() const {return _clusterAveragePowerPlasticEqStrainConcentrationVectorMap;};
    const std::map<int,fullVector<double>>& getClusterPlasticEqSTDVectorMap() const {return _clusterPlEqSTDVectorMap;};

    const std::map<std::pair<int,int>,STensor43>& getClusterInteractionTensorMap() const {return _clusterInteractionTensorMap;};
    
    const std::map<int, std::map<std::pair<int,int>, STensor43>>& getClusterInteractionTensorMapLevel1() const {return _clusterInteractionTensorMapLevel1;};
    
    const std::map<std::pair<int,int>,STensor43>& getClusterEshelbyInteractionTensorMap() const {return _clusterEshelbyInteractionTensorMap;};
    const std::map<std::pair<int,int>,STensor43>& getClusterInteractionTensorMatrixFrameELMap() const {return _clusterInteractionTensorMatrixFrameELMap;};
    const std::map<std::pair<int,int>,STensor43>& getClusterInteractionTensorHomogenizedFrameELMap() const {return _clusterInteractionTensorHomogenizedFrameELMap;};

    const std::map<std::pair<int,int>, STensor43>& getDclusterAverageStrainConcentrationTensorMapDLaw() const {return _DclusterAverageStrainConcentrationTensorMapDLaw;};
    
    const STensor43& getReferenceStiffness() const {return _referenceStiffness;};
    const STensor43& getReferenceStiffnessIso() const {return _referenceStiffnessIso;};
    const std::map<std::pair<int,int>,STensor43>& getClusterInteractionTensorIsoMap() const {return _clusterInteractionTensorIsoMap;};
    
    const std::map<int,fullVector<double>>& getClusterMeanFiberOrientation() const {return _clusterMeanFiberOrientationMap;};
    //
    #endif //SWIG    
};

#ifndef SWIG
class viewClustering
{
  protected:
    const Clustering& _clustering;
  public:
    viewClustering(const Clustering& cl):_clustering(cl){};
    ~viewClustering(){}
    double get(int eleNum, const int cmp, const int gp=-1)const;
    void buildData(FILE* myview,const int cmp, const int gp=-1) const;
    void archive(const std::string filename, const double time, const int numstep, const std::vector<int>& view, const int gp=-1); 
};
namespace clutering_impls
{
  double getDiffNorm(const fullMatrix<double>&A, const fullMatrix<double>&B);
  double getDiffNormVec(const fullVector<double>&a, const fullVector<double>&b);
  double getDiffNormVecLong(const fullVector<double>&a, const fullVector<double>&b);
  double getDiffShearNormVecLong(const fullVector<double>&a, const fullVector<double>&b);
  double getDiffNormVecPowerLong(const fullVector<double>&a, const fullVector<double>&b);
  
  double getDiffNormOrientationBased(const fullVector<double>&a, const fullVector<double>&b);
  
  bool kmean_cluster(const std::map<int, fullMatrix<double> >& data, const int nbClusters,  const int minElemEachCluster,
                     const int maxNbIterations,
                     const std::map<int, fullVector<double> >& mapPositionsWeights,
                     std::map<int, std::map<int, const fullMatrix<double>* > >& clusters);
                     
  bool kmean_cluster_FullVectorBased(const std::map<int, fullVector<double> >& dataVec,
                                     const std::map<int, fullMatrix<double> >& data, 
                                     const int nbClusters,  
                                     const int minElemEachCluster,
                                     const int maxNbIterations,
                                     const std::map<int, fullVector<double> >& mapPositionsWeights,
                                     std::map<int, std::map<int, const fullVector<double>* > >& clustersVec,
                                     std::map<int, std::map<int, const fullMatrix<double>* > >& clusters);
                                     
  bool kmean_cluster_orientationBased(const std::map<int, fullVector<double> >& dataVecOri,
                                     const std::map<int, fullMatrix<double> >& data, 
                                     const int nbClusters,  
                                     const int minElemEachCluster,
                                     const int maxNbIterations,
                                     const std::map<int, fullVector<double> >& mapPositionsWeights,
                                     std::map<int, std::map<int, const fullVector<double>* > >& clustersOri,
                                     std::map<int, std::map<int, const fullMatrix<double>* > >& clusters);
                                     
  bool kmean_cluster_hierarchical_orientation_fullVectorBased(const std::map<int, fullVector<double> >& dataOri,
                                                              const std::map<int, fullVector<double> >& dataVec,
                                                              const std::map<int, fullMatrix<double> >& data, 
                                                              int& nbClusters,  
                                                              const int minElemEachCluster,
                                                              const int maxNbIterations,
                                                              const std::map<int, fullVector<double> >& mapPositionsWeights,
                                                              std::map<int, std::map<int, const fullVector<double>* > >& clustersOri,
                                                              std::map<int, std::map<int, const fullVector<double>* > >& clustersVec,
                                                              std::map<int, std::map<int, const fullMatrix<double>* > >& clusters);
                                                              
  bool kmean_cluster_hierarchical_fullMatrix_fullVectorBased(const std::map<int, fullVector<double> >& dataVec,
                                                             const std::map<int, fullMatrix<double> >& data, 
                                                             int& nbClusters,  
                                                             const int minElemEachCluster,
                                                             const int maxNbIterations,
                                                             const std::map<int, fullVector<double> >& mapPositionsWeights,
                                                             std::map<int, std::map<int, const fullVector<double>* > >& clustersVec,
                                                             std::map<int, std::map<int, const fullMatrix<double>* > >& clusters);
                                                             
  bool kmean_cluster_hierarchical_fullVector_fullMatrixBased(const std::map<int, fullVector<double> >& dataVec,
                                                             const std::map<int, fullMatrix<double> >& data, 
                                                             int& nbClusters,  
                                                             const int minElemEachCluster,
                                                             const int maxNbIterations,
                                                             const std::map<int, fullVector<double> >& mapPositionsWeights,
                                                             std::map<int, std::map<int, const fullVector<double>* > >& clustersVec,
                                                             std::map<int, std::map<int, const fullMatrix<double>* > >& clusters);
                                                             
  bool kmean_cluster_hierarchical_fullVectorBased(const std::map<int, fullVector<double> >& dataVec,
                                                  const std::map<int, fullMatrix<double> >& data, 
                                                  int& nbClusters,  
                                                  const int minElemEachCluster,
                                                  const int maxNbIterations,
                                                  const std::map<int, fullVector<double> >& mapPositionsWeights,
                                                  std::map<int, std::map<int, const fullVector<double>* > >& clustersVec,
                                                  std::map<int, std::map<int, const fullMatrix<double>* > >& clusters);

  double meanCluster(const std::map<int, const fullMatrix<double>* >& cluster, 
                     const std::map<int, fullVector<double> >& mapPositionsWeights,
                     fullMatrix<double>& mean);
                     
  void meanClusterVec_FullMatrixBased(const std::map<int, fullVector<double> >& dataVec,
                                        const std::map<int, const fullMatrix<double>* >& cluster, 
                                        const std::map<int, fullVector<double> >& mapPositionsWeights,
                                        fullVector<double>& mean);
                                        
  void meanClusterVecLong_FullMatrixBased(const std::map<int, fullVector<double> >& dataVec,
                                        const std::map<int, const fullMatrix<double>* >& cluster, 
                                        const std::map<int, fullVector<double> >& mapPositionsWeights,
                                        fullVector<double>& mean);

  double meanClusterVec(const std::map<int, const fullVector<double>* >& cluster, 
                        const std::map<int, fullVector<double> >& mapPositionsWeights,
                        fullVector<double>& mean);
                        
  double meanClusterVecLong(const std::map<int, const fullVector<double>* >& cluster, 
                        const std::map<int, fullVector<double> >& mapPositionsWeights,
                        fullVector<double>& mean);
                        
  void meanClusterPosition(const std::map<int, const fullMatrix<double>* >& cluster, 
                        const std::map<int, fullVector<double> >& mapPositionsWeights,
                        double& x_mean, double& y_mean, double& z_mean);
  
  void meanClusterFiberOrientation(const std::map<int, const fullMatrix<double>* >& cluster, 
                                   const std::map<int, fullVector<double> >& mapPositionsWeights,
                                   const std::map<int, fullVector<double> >& mapLocalFiberOrientations,
                                   double& E1_mean, double& E2_mean, double& E3_mean);
                                   
  void meanClusterFiberOrientation(const std::map<int, const fullVector<double>* >& cluster, 
                                   const std::map<int, fullVector<double> >& mapPositionsWeights,
                                   fullVector<double>& mean);
                        
  void meanGeometricClusterVec(const std::map<int, const fullVector<double>* >& cluster, 
                               const std::map<int, fullVector<double> >& mapPositionsWeights,
                               fullVector<double>& mean);                      
                        
  void meanHarmonicClusterVec(const std::map<int, const fullVector<double>* >& cluster, 
                              const std::map<int, fullVector<double> >& mapPositionsWeights,
                              fullVector<double>& mean);
                              
  void meanHarmonicClusterVec_FullMatrixBased(const std::map<int, fullVector<double> >& dataVec,
                                        const std::map<int, const fullMatrix<double>* >& cluster, 
                                        const std::map<int, fullVector<double> >& mapPositionsWeights,
                                        fullVector<double>& mean);
  void meanGeometricClusterVec_FullMatrixBased(const std::map<int, fullVector<double> >& dataVec,
                                        const std::map<int, const fullMatrix<double>* >& cluster, 
                                        const std::map<int, fullVector<double> >& mapPositionsWeights,
                                        fullVector<double>& mean);
                                        
  void meanPowerClusterVec_FullMatrixBased(const std::map<int, fullVector<double> >& dataVec,
                                        const std::map<int, const fullMatrix<double>* >& cluster, 
                                        const std::map<int, fullVector<double> >& mapPositionsWeights,
                                        fullVector<double>& mean);
                        
  int maxConcentrationCluster(const std::map<int, const fullMatrix<double>* >& cluster);
  int maxConcentrationVecCluster(const std::map<int, const fullVector<double>* >& cluster);
  int minConcentrationVecCluster(const std::map<int, const fullVector<double>* >& cluster);

  void printClusters(const std::map<int, std::map<int, const fullMatrix<double>* > >& clusters);
  int getNearestClusterId(const std::map<int, fullMatrix<double> >& clusterMean, const fullMatrix<double>& pt);
  int getNearestClusterIdVec(const std::map<int, fullVector<double> >& clusterMean, const fullVector<double>& pt);
  int getNearestClusterIdVecLong(const std::map<int, fullVector<double> >& clusterMean, const fullVector<double>& pt);
  
  int getNearestClusterIdOrientationBased(const std::map<int, fullVector<double> >& clusterMean, const fullVector<double>& pt);

  void Tensor43STDScalarCluster(const std::map<int, const fullMatrix<double>* >& cluster, 
                       const std::map<int, fullVector<double> >& mapPositionsWeights,
                       const STensor43& AmeanCluster,
                       double &clusterStdev
                       );
  void Tensor43STDCluster(const std::map<int, const fullMatrix<double>* >& cluster, 
                       const std::map<int, fullVector<double> >& mapPositionsWeights,
                       const STensor43& AmeanCluster,
                       STensor43& clusterStdevTensor
                       );
  void VectorSTDCluster(const std::map<int, const fullVector<double>* >& cluster, 
                       const std::map<int, fullVector<double> >& mapPositionsWeights,
                       const fullVector<double>& VecmeanCluster, 
                       fullVector<double>& clusterSTDVector
                       );
                       
  void vecNormalizeByOverallValue(const std::map<int, fullVector<double> >& dataVec,
                     const std::map<int, fullVector<double> >& mapPositionsWeights,
                     std::map<int, fullVector<double> >& dataVecNormalized);
                     
  void vecNormalizeByOverallNorm(const std::map<int, fullVector<double> >& dataVec,
                           const std::map<int, fullVector<double> >& mapPositionsWeights,
                           std::map<int, fullVector<double> >& dataVecNormalized);
                           
  void vecNormalizeByLocalNorm(const std::map<int, fullVector<double> >& dataVec,
                               std::map<int, fullVector<double> >& dataVecNormalized);
                               
  void vecSeparate(const std::map<int, fullVector<double> >& dataVec,
                               std::map<int, fullVector<double> >& dataVecSeparated);
                           
  void vecRatio(const std::map<int, fullVector<double> >& dataVecNumerator,
                const std::map<int, fullVector<double> >& dataVecDenominator,
                std::map<int, fullVector<double> >& dataVecRatio);


}
#endif //SWIG
#endif // CLUSTERING_H_
