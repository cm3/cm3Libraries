//
// C++ Interface: ANN
//
//
// Author:  <V.-D. Nguyen>, (C) 2020
//
// Copyright: See COPYING file that comes with this distribution
//
//

#include "ANNUtils.h"
#include "StringUtils.h"

activationFunction* activationFunction::createActivationFunction(const char what[])
{
  if (strcmp(what, "abs")==0)
  {
    Msg::Info("create AbsActivationFunction");
    return new AbsActivationFunction();
  }
  else if (strcmp(what, "square")==0)
  {
    Msg::Info("create SquareActivationFunction");
    return new SquareActivationFunction();
  }
  else if (strcmp(what, "rectifier")==0)
  {
    Msg::Info("create RectifierActivationFunction");
    return new RectifierActivationFunction(10.);
  }
  else if (strcmp(what, "rectifier_1")==0)
  {
    Msg::Info("create RectifierActivationFunction");
    return new RectifierActivationFunction(1.);
  }
    else if (strcmp(what, "rectifier_5")==0)
  {
    Msg::Info("create RectifierActivationFunction");
    return new RectifierActivationFunction(5.);
  }

  else if (strcmp(what, "linear")==0)
  {
    Msg::Info("create LinearActivationFunction");
    return new LinearActivationFunction();
  }
  else if (strcmp(what, "tanh")==0)
  {
    Msg::Info("create TanhActivationFunction");
    return new TanhActivationFunction();
  }
  else if (strcmp(what, "sigmoid")==0)
  {
    Msg::Info("create SigmoidActivationFunction");
    return new SigmoidActivationFunction();
  }
  else if (strcmp(what, "leakyRelu")==0)
  {
    Msg::Info("create LeakyReluActivationFunction");
    return new LeakyReluActivationFunction(0.1);
  }
  else if (strcmp(what, "relu")==0)
  {
    Msg::Info("create ReluActivationFunction");
    return new ReluActivationFunction();
  }
  else
  {
    Msg::Error("activationFunction %d has not been implemented %s",what);
    return NULL;
  }
};

std::string RectifierActivationFunction::getName() const 
{
  if (fabs(10.-_fact) <1e-6) return "rectifier";
  else if (fabs(5.-_fact) <1e-6) return "rectifier_5";
  else if (fabs(1.-_fact) <1e-6) return "rectifier_1";
  else 
  {
    Msg::Error("fact = %e is not defined in RectifierActivationFunction",_fact);
  }
  return "rectifier_undef";
};

DenseLayer::DenseLayer(int numIn, int numOut): numInput(numIn), numOutput(numOut), af(NULL),W(),b(), WT(NULL)
{
  if (numIn > 0 and numOut > 0)
  {
    W.resize(numIn,numOut);
    b.resize(1,numOut);
  };
};

DenseLayer::DenseLayer(const DenseLayer& src):numInput(src.numInput), numOutput(src.numOutput),W(src.W),b(src.b), WT(NULL)
{
  af = NULL;
  if (src.af != NULL)
  {
    af = src.af->clone();
  }
}

DenseLayer::~DenseLayer()
{
  if (WT) delete WT;
  WT = NULL;
  if (af!=NULL) delete af;
  {
    af = NULL;
  }
}

void DenseLayer::setWeights(const fullMatrix<double>& WW)
{
  W = WW;
  numInput = W.size1();
  numOutput = W.size2();
}
void DenseLayer::setBias(const fullMatrix<double>& bb)
{
  b = bb;
}

void DenseLayer::setWeights(int i, int j, double val)
{
  if (i > W.size1() - 1  || j > W.size2() -1)
  {
    Msg::Error("indexes %d %d exeed the matrix dimensions %d %d",i,j,W.size1(),W.size2());
    Msg::Exit(0);
  }
  else
  {
    W(i,j) = val;
  }
}
void DenseLayer::setBias(int i, double val)
{
  if (i > b.size2() -1)
  {
    Msg::Error("indexes %d exeed the vector dimension %d",i,W.size2());
    Msg::Exit(0);
  }
  else
  {
    b(0,i) = val;
  }
}


void DenseLayer::predict(const fullMatrix<double>& y0, fullMatrix<double>& y1, bool stiff, fullMatrix<double>* Dy1Dy0) const
{  
  // x1 = y0*W + b and y1 = activationFunc(x1)
  static fullMatrix<double> x1;
  if (x1.size1() != 1 || x1.size2() != numOutput)
  {
    x1.resize(1,numOutput);
  }
  //  
  y0.mult(W,x1);
  x1.add(b);
  //
  if (y1.size1() != 1 || y1.size2() != numOutput)
  {
    y1.resize(1,numOutput);
  }
  for (int i=0; i< numOutput; i++)
  {
    y1(0,i) = af->getVal(x1(0,i));
  }
  
  if (stiff)
  {
    if (Dy1Dy0->size1() != numOutput || Dy1Dy0->size2() != numInput)
    {
      Dy1Dy0->resize(numOutput,numInput);
    }
    static fullMatrix<double> Dy1Dx1;
    if (Dy1Dx1.size1() != numOutput || Dy1Dx1.size2() != numOutput)
    {
      Dy1Dx1.resize(numOutput,numOutput);
    }
    Dy1Dx1.setAll(0.);
    for (int i=0; i< numOutput; i++)
    {
      Dy1Dx1(i,i) = af->getDiff(x1(0,i));
    }
    
    if (WT == NULL)
    {
      WT = new fullMatrix<double>(W.transpose());
    };
    Dy1Dx1.mult(*WT,*Dy1Dy0);
  }
};

ReluDenseLayer::ReluDenseLayer(int numIn, int numOut): DenseLayer(numIn,numOut)
{
  if (af!=NULL) delete af;
  af = new ReluActivationFunction();
}
ReluDenseLayer::~ReluDenseLayer()
{
  
};

void ReluDenseLayer::print_infos() const
{
  printf("relu dense layer: nbInput = %d, nbOutput=%d\n",numInput,numOutput);
}
DenseLayer* ReluDenseLayer::clone() const
{
  return new ReluDenseLayer(*this);
}


TanhDenseLayer::TanhDenseLayer(int numIn, int numOut): DenseLayer(numIn,numOut)
{
  if (af!=NULL) delete af;
  af = new TanhActivationFunction();
}
TanhDenseLayer::~TanhDenseLayer()
{
  
};

void TanhDenseLayer::print_infos() const
{
  printf("tanh dense layer: nbInput = %d, nbOutput=%d\n",numInput,numOutput);
}
DenseLayer* TanhDenseLayer::clone() const
{
  return new TanhDenseLayer(*this);
}

SigmoidDenseLayer::SigmoidDenseLayer(int numIn, int numOut): DenseLayer(numIn,numOut)
{
  if (af!=NULL) delete af;
  af = new SigmoidActivationFunction();
}
SigmoidDenseLayer::~SigmoidDenseLayer()
{
  
};

void SigmoidDenseLayer::print_infos() const
{
  printf("sigmoid dense layer: nbInput = %d, nbOutput=%d\n",numInput,numOutput);
}
DenseLayer* SigmoidDenseLayer::clone() const
{
  return new SigmoidDenseLayer(*this);
}

LeakyReluDenseLayer::LeakyReluDenseLayer(int numIn, int numOut, double a): DenseLayer(numIn,numOut)
{
  if (af!=NULL) delete af;
  af = new LeakyReluActivationFunction(a);
}
LeakyReluDenseLayer::~LeakyReluDenseLayer()
{
  
};

void LeakyReluDenseLayer::print_infos() const
{
  printf("leaky relu dense layer: nbInput = %d, nbOutput=%d\n",numInput,numOutput);
}
DenseLayer* LeakyReluDenseLayer::clone() const
{
  return new LeakyReluDenseLayer(*this);
}


LinearDenseLayer::LinearDenseLayer(int numIn, int numOut): DenseLayer(numIn,numOut)
{
  if (af!=NULL) delete af;
  af = new LinearActivationFunction();
}
LinearDenseLayer::~LinearDenseLayer()
{
};
void LinearDenseLayer::print_infos() const
{
  printf("linear dense layer: nbInput = %d, nbOutput=%d\n",numInput,numOutput);
}

DenseLayer* LinearDenseLayer::clone() const
{
  return new LinearDenseLayer(*this);
}

ArtificialNN* ArtificialNN::clone() const
{
  return new ArtificialNN(*this);
};

ArtificialNN::ArtificialNN(): _allLayers(){}
ArtificialNN::ArtificialNN(const ArtificialNN& src)
{
  _allLayers.resize(src._allLayers.size(),NULL);
  for (int i=0; i< src._allLayers.size(); i++)
  {
    _allLayers[i] = src._allLayers[i]->clone();
  }
}

ArtificialNN::~ArtificialNN()
{
  for (int i=0; i< _allLayers.size(); i++)
  {
    delete _allLayers[i];
  }
  _allLayers.clear();
};

DenseLayer& ArtificialNN::getRefToLayer(int index)
{
  if (index > _allLayers.size()-1)
  {
    Msg::Error("layer %d exceed the number of layers %ld",index,_allLayers.size());
  }
  else
  {
    return *_allLayers[index];
  }
};
const DenseLayer& ArtificialNN::getConstRefToLayer(int index) const
{
  if (index > _allLayers.size()-1)
  {
    Msg::Error("layer %d exceed the number of layers %ld",index,_allLayers.size());
  }
  else
  {
    return *_allLayers[index];
  }
}

void ArtificialNN::addLayer(const DenseLayer& layer)
{
  const DenseLayer*  lastLayer = NULL;
  if (_allLayers.size() >0)
  {
    lastLayer = _allLayers.back();
  }
  _allLayers.push_back(layer.clone());
  if (lastLayer != NULL)
  {
    // to make compatibility
    _allLayers.back()->numInput = lastLayer->numOutput;
  }
  //
  Msg::Info("add layer:");
  _allLayers.back()->print_infos();
};

void ArtificialNN::print_infos() const
{
  printf("ANN with %d layers: \n",_allLayers.size());
  for (int i=0; i< _allLayers.size(); i++)
  {
    printf("layer %d:",i);
    if (_allLayers[i] !=NULL)
    {
      _allLayers[i]->print_infos();
    }
    else
    {
      Msg::Error("layer %d is NULL !!!");
      Msg::Exit(0);
    }
  }
}

int ArtificialNN::getNumInputs() const 
{
  if (_allLayers.size() == 0)
  {
    return 0;
  }
  else
  {
    return _allLayers.front()->numInput;
  }
}
int ArtificialNN::getNumOutputs() const 
{
  if (_allLayers.size() == 0)
  {
    return 0;
  }
  else
  {
    return _allLayers.back()->numOutput;
  };
}
    

void ArtificialNN::predict(const fullMatrix<double>& yIn, fullMatrix<double>& yOut, bool stiff, fullMatrix<double>* DyoutDyInt) const
{
  // first layer
  int numInt = _allLayers.front()->numInput;  
  if (yIn.size2() != numInt)
  {
    Msg::Error("input data is not correct, size of input = %ld",yIn.size2());
    Msg::Exit(0);
  }
  yOut = yIn;
  int numLayers = _allLayers.size();
  static std::vector<fullMatrix<double> > all_Dy;
  if (all_Dy.size() != numLayers && stiff)
  {
    all_Dy.resize(numLayers);
  }
  for (int i=0; i< numLayers; i++)
  {
    static fullMatrix<double> y1;
    _allLayers[i]->predict(yOut,y1,stiff,&all_Dy[i]);
    yOut = y1;
  };
  
  if (stiff)
  {
    (*DyoutDyInt) = all_Dy[numLayers-1];
    for (int i= numLayers-2; i >-1; i--)
    {
      static fullMatrix<double> temp;
      if (temp.size1() != DyoutDyInt->size1()|| temp.size2() != all_Dy[i].size2())
      {
        temp.resize(DyoutDyInt->size1(),all_Dy[i].size2());
      }
      DyoutDyInt->mult(all_Dy[i],temp);
      (*DyoutDyInt) = temp;
    }
  }
};
