//
// C++ Interface: ANN
//
//
// Author:  <V.-D. Nguyen>, (C) 2020
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef ANNUTILS_H_
#define ANNUTILS_H_

#ifndef SWIG
#include "fullMatrix.h"
#include <math.h> 
#endif //SWIG

class activationFunction
{
  public:
    static activationFunction* createActivationFunction(const char what[]);
  public:
    enum functionType{undef=0, linear=1,leakyRelu=2,relu=3,tanHyperbolic=4,sigmoid=5, rectifier=6, square=7, abs=8};
  
  #ifndef SWIG
  public:
    activationFunction(){}
    activationFunction(const activationFunction& src){}
    virtual ~activationFunction(){}
    virtual functionType getType() const = 0;
    virtual std::string getName() const = 0;
    virtual double getVal(double x) const  = 0;
    virtual double getReciprocalVal(double y) const = 0; // inverse function
    virtual double getDiff(double x) const = 0;
    virtual activationFunction* clone() const = 0;
  #endif //SWIG
};

class AbsActivationFunction: public activationFunction
{
  public:
    AbsActivationFunction(){}
    AbsActivationFunction(const AbsActivationFunction& src): activationFunction(src){}
    virtual ~AbsActivationFunction(){}
    virtual activationFunction::functionType getType() const {return activationFunction::abs;};
    virtual std::string getName() const {return "abs";};
    virtual double getVal(double x) const  {return fabs(x);};
    virtual double getReciprocalVal(double y) const {return y;}; // inverse function, take positive only
    virtual double getDiff(double x) const {if (x>=0) return 1; else return -1;};
    virtual activationFunction* clone() const {return new AbsActivationFunction(*this);};
};


class SquareActivationFunction: public activationFunction
{
  public:
    SquareActivationFunction(){}
    SquareActivationFunction(const SquareActivationFunction& src): activationFunction(src){}
    virtual ~SquareActivationFunction(){}
    virtual activationFunction::functionType getType() const {return activationFunction::square;};
    virtual std::string getName() const {return "square";};
    virtual double getVal(double x) const  {return 0.5*x*x;};
    virtual double getReciprocalVal(double y) const {return sqrt(2.*y);}; // inverse function, take positive only
    virtual double getDiff(double x) const {return x;};
    virtual activationFunction* clone() const {return new SquareActivationFunction(*this);};
};


class RectifierActivationFunction: public activationFunction
{
  protected:
    double _fact;
    double _offset;
  public:
    RectifierActivationFunction(double f=10.): activationFunction(),_fact(f),_offset(0.){}
    RectifierActivationFunction(const RectifierActivationFunction& src): activationFunction(src),_fact(src._fact),_offset(src._offset){}
    virtual ~RectifierActivationFunction(){}
    virtual activationFunction::functionType getType() const {return activationFunction::rectifier;};
    virtual std::string getName() const;
    virtual double getVal(double x) const  {return (_offset+log(1.+exp(_fact*x)))/_fact;};
    virtual double getReciprocalVal(double y) const {return (log(exp(_fact*(y-_offset))-1.))/_fact;}; // inverse function
    virtual double getDiff(double x) const {return exp(_fact*x)/(1.+exp(_fact*x));};
    virtual activationFunction* clone() const {return new RectifierActivationFunction(*this);};
};


class LinearActivationFunction: public activationFunction
{
  public:
    LinearActivationFunction(){}
    LinearActivationFunction(const LinearActivationFunction& src): activationFunction(src){}
    virtual ~LinearActivationFunction(){}
    virtual activationFunction::functionType getType() const {return activationFunction::linear;};
    virtual std::string getName() const {return "linear";};
    virtual double getVal(double x) const  {return x;};
    virtual double getReciprocalVal(double y) const {return y;}; // inverse function
    virtual double getDiff(double x) const {return 1.;};
    virtual activationFunction* clone() const {return new LinearActivationFunction(*this);};
};

class TanhActivationFunction: public activationFunction
{
  public:
    TanhActivationFunction(){}
    TanhActivationFunction(const TanhActivationFunction& src): activationFunction(src){}
    virtual ~TanhActivationFunction(){}
    virtual activationFunction::functionType getType() const {return activationFunction::tanHyperbolic;};
    virtual std::string getName() const {return "tanh";};
    virtual double getVal(double x) const  {return tanh(x);};
    virtual double getReciprocalVal(double y) const {return atanh(y);}; // inverse function
    virtual double getDiff(double x) const {return 1.-tanh(x)*tanh(x);};
    virtual activationFunction* clone() const {return new TanhActivationFunction(*this);};
};

class SigmoidActivationFunction: public activationFunction
{
  public:
    SigmoidActivationFunction(){}
    SigmoidActivationFunction(const SigmoidActivationFunction& src): activationFunction(src){}
    virtual ~SigmoidActivationFunction(){}
    virtual activationFunction::functionType getType() const {return activationFunction::sigmoid;};
    virtual std::string getName() const {return "sigmoid";};
    virtual double getVal(double x) const  {return 1./(1.+exp(-x));};
    virtual double getReciprocalVal(double y) const {return -log(1./y-1.);}; // inverse function
    virtual double getDiff(double x) const {return exp(x)/(1.+exp(x))/(1.+exp(x));};
    virtual activationFunction* clone() const {return new SigmoidActivationFunction(*this);};
};


class LeakyReluActivationFunction : public activationFunction
{
  protected:
    double alpha;
    
  public:
    LeakyReluActivationFunction(double a): alpha(a){}
    LeakyReluActivationFunction(const LeakyReluActivationFunction& src): activationFunction(src),alpha(src.alpha){}
    virtual ~LeakyReluActivationFunction(){}
    virtual activationFunction::functionType getType() const {return activationFunction::leakyRelu;};
    virtual std::string getName() const {return "leakyRelu";};
    virtual double getVal(double x) const  {if (x>=0) return x; else return alpha*x;};
    virtual double getReciprocalVal(double y) const {if (y>= 0) return y; else return y/alpha;}; // inverse function
    virtual double getDiff(double x) const {if (x>=0) return 1; else return alpha;};
    virtual activationFunction* clone() const {return new LeakyReluActivationFunction(*this);};
};

class ReluActivationFunction : public activationFunction
{
  public:
    ReluActivationFunction(){}
    ReluActivationFunction(const ReluActivationFunction& src): activationFunction(src){}
    virtual ~ReluActivationFunction(){}
    virtual activationFunction::functionType getType() const {return activationFunction::relu;};
    virtual std::string getName() const {return "relu";};
    virtual double getVal(double x) const  {if (x>=0) return x; else return 0;};
    virtual double getReciprocalVal(double y) const {if (y>=0) return y; else return (1./0.);}; // inverse function
    virtual double getDiff(double x) const {if (x>=0) return 1; else return 0;};
    virtual activationFunction* clone() const {return new ReluActivationFunction(*this);};
};

class DenseLayer
{
  /** general dense layer 
   *  y = f(b + x*W), the notations should be compatible with Perceptron in tensorflow
   *  y - output whose dimensions are 
   *  x - input whose dimensions are 1 - numInput
   *  f - elementwise activation function
   *  W - weight matrix whose dimensions are numInput - numOutput
   *  b - bias matrix whose dimensions are 1 - numOutput
   * */
  public:
    #ifndef SWIG
    int numInput, numOutput;
    fullMatrix<double> W; // size
    mutable fullMatrix<double>* WT; //cache to avoid  realloaction
    fullMatrix<double> b;
    activationFunction* af;
    #endif //SWIG
    
  public:
    void setWeights(const fullMatrix<double>& WW);
    void setBias(const fullMatrix<double>& BB);
    void setWeights(int i, int j, double val);
    void setBias(int i, double val);
    // prediction with 
    void predict(const fullMatrix<double>& y0, fullMatrix<double>& y1, bool stiff=false, fullMatrix<double>* Dy1Dy0=NULL) const;
    #ifndef SWIG
    DenseLayer(int numIn, int numOut);
    DenseLayer(const DenseLayer& src);
    virtual ~DenseLayer();
    // using matrix as vector 1xsize
    virtual void print_infos() const = 0;
    virtual DenseLayer* clone() const = 0;
    #endif //SWIG
};

class ReluDenseLayer : public DenseLayer
{
  public:
    ReluDenseLayer(int numIn, int numOut);
    virtual void print_infos() const;
    ReluDenseLayer(const ReluDenseLayer& src):DenseLayer(src){}
    virtual ~ReluDenseLayer();
    virtual DenseLayer* clone() const;
};

class TanhDenseLayer : public DenseLayer
{
  public:
    TanhDenseLayer(int numIn, int numOut);
    virtual void print_infos() const;
    TanhDenseLayer(const TanhDenseLayer& src):DenseLayer(src){}
    virtual ~TanhDenseLayer();
    virtual DenseLayer* clone() const;
};

class SigmoidDenseLayer : public DenseLayer
{
  public:
    SigmoidDenseLayer(int numIn, int numOut);
    virtual void print_infos() const;
    SigmoidDenseLayer(const SigmoidDenseLayer& src):DenseLayer(src){}
    virtual ~SigmoidDenseLayer();
    virtual DenseLayer* clone() const;
};

class LeakyReluDenseLayer : public DenseLayer
{
  public:
    LeakyReluDenseLayer(int numIn, int numOut, double a);
    virtual void print_infos() const;
    LeakyReluDenseLayer(const LeakyReluDenseLayer& src):DenseLayer(src){}
    virtual ~LeakyReluDenseLayer();
    virtual DenseLayer* clone() const;
};


class LinearDenseLayer : public DenseLayer
{
  public:
    LinearDenseLayer(int numIn, int numOut);
    virtual void print_infos() const;
    LinearDenseLayer(const LinearDenseLayer& src): DenseLayer(src){}
    virtual ~LinearDenseLayer();
    virtual DenseLayer* clone() const;
};

class ArtificialNN 
{
  //
  // x[0]|y[0] -W[0],b[0]->x[1]|y[1] -W[1],b[1]->x[2]|y[2] ... 
  //
  protected:
    #ifndef SWIG
    std::vector<DenseLayer*> _allLayers;
    #endif //SWIG
  
  public:
    ArtificialNN();
    ArtificialNN(const ArtificialNN& src);
    ~ArtificialNN();
    ArtificialNN* clone() const;
    //
    void addLayer(const DenseLayer& l);
    DenseLayer& getRefToLayer(int index);
    const DenseLayer& getConstRefToLayer(int index) const;
    int getNumInputs() const;
    int getNumOutputs() const;
    void print_infos() const;
    void predict(const fullMatrix<double>& yIn, fullMatrix<double>& yOut, bool stiff=false, fullMatrix<double>* DyoutDyInt=NULL) const;
};

#endif // ANNUTILS_H_
