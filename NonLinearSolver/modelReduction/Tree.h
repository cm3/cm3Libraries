//
// C++ Interface:  tree
//
//
// Author:  <V.-D. Nguyen>, (C) 2020
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef _TREE_H_
#define _TREE_H_ 

#ifndef SWIG
#include "ANNUtils.h"
#include <vector>
#include "SVector3.h"
#endif //SWIG
#include "MaterialNode.h"

class TreeNode : public MaterialNode
{
  public:
    #ifndef SWIG
    std::vector<double> direction; // direction data = (nuChilds-1) in 2D and (nuChilds-1)*2 in 3D 
    int depth; // in layer 0 - root, 
    int location; // location in vector of childs of parent node
    int childOrder; // order in childlist of paraent
    TreeNode* parent; // parent node (NULL for root node)
    std::vector<TreeNode*> childs; // child nodes (NULL for last nodes)
    #endif //SWIG
    TreeNode();
    TreeNode(const TreeNode& src);
    virtual MaterialNode* clone() const {return new TreeNode(*this);}
    TreeNode(int k, int c, TreeNode* p, int o, const std::string affunc="relu");
    virtual ~TreeNode();
    virtual int getNodeId() const{return location*1000+depth;};
    virtual void printData() const;
    #ifndef SWIG
    virtual void saveToFile(FILE* file) const;
    virtual void saveToFileCohesive(FILE* f) const;
    void getNormal(std::vector<SVector3>& vec, bool stiff=false, std::vector<std::vector<SVector3> >* DnormalDunknown=NULL) const;
    #endif //SWIG
};

class Tree
{
  public:
    typedef std::vector<const TreeNode*> nodeContainer;
  
  public:
    Tree();  
    ~Tree();
    void createPerfectTree(int maxDepth, int numChild, int numDirVars, const std::string affunc="relu");
    void createPerfectTreeBis(int maxDepth, int numChild, int numDirVars, const std::string affunc="relu");
    void createPerfectTree(int maxDepth, int numChildRegular, int numChildLeaf, int numDirVars, const std::string affunc="relu", int lastRepeat=1);
    void createMixedTree(int maxDepth, int numChildMax, int leafPerNode, int numDirVars,const std::string affunc="relu");
    void createRandomTree(int nbLeaves, int numChildMax, int leafPerNode, int numDirVars,const std::string affunc="relu", bool randomSubdivion=true, bool randomChild=true, bool sameNbChildLayer=true);
    void createRandomTreeSameDepthForNodes(int nbLeaves, int numChildMax, int leafPerNode, int numDirVars,const std::string affunc="relu", bool randomChild=true, bool randomMat=true);
    void createSpecialBinaryTree(int numNodes, int numDirVars,const std::string affunc="relu");
    void createSpecialTripleTree(int numNodes, int numDirVars,const std::string affunc="relu");
    void printTree() const;
    void printTreeInteraction(const std::string fname="treeInteraction.txt", bool colorMat = true, int dir=1) const;
    void clear();
    void initialize(bool rand=true, bool normalizedWeight=true);
    void initializeCohesiveParameters(double minCoef, double maxCoef);
    void initializeWithInitialFractions(const fullVector<double>& phaseFractions);
    const TreeNode* getRootNode() const {return _root;};
    
    void assignMaterialLaws(int numPhases);
    void cohesiveEnrichement(int phaseIndex, int numCoh, int numDirVars, const std::string affunc="relu", bool init=true);
    void setCohesiveParametersFromInteraction(int phaseIndex, double minCoef, double maxCoef, const std::string affunc="relu");
    void printAllRegularNodes() const;
    
    void printAssociatedLeavesForNode(const TreeNode* node) const;
    void printBackTrackingPath(const TreeNode* node) const;
    
    void saveDataToFile(std::string filename) const;
    void loadCohesiveDataFromFile(std::string filename);
    void loadDataFromFile(std::string filename);
    int getNumberOfNodes() const;
    bool checkDuplicate() const;
    double getPhaseFraction(int i) const;
    void printPhaseFraction() const;
    void printLeafFraction() const;
    void printNodeFraction() const;
    
    bool removeLeavesWithZeroContribution(double tol=1e-6);
    bool removeZeroBranches(double tol);
    bool removeZeroLeaves(double tol, bool iteration=true);
    bool treeMerging();
    
    #ifndef SWIG
    void getAllRegularNodes(nodeContainer& allRegNodes) const;
    void getRefToAllRegularNodes(std::vector<TreeNode*>& allRegNodes);
    void getAllLeaves(nodeContainer& allLeaves) const;
    void getAllNodes(nodeContainer& allNodes) const;
    void getAllNodesByDepth(std::map<int,nodeContainer>& allNodes) const;
    void getMaxLocationByDepth(std::vector<int>& loc) const;
    void getAllNodesByDepth(std::map<int,std::vector<TreeNode*> >& allNodes);
    void getAssociatedLeavesForNode(const TreeNode* node, nodeContainer& allLeaves) const;
    void getAssociatedLeavesForNode(TreeNode* node, std::vector<TreeNode*>& allLeaves);
    void getBackTrackingPath(const TreeNode* node, nodeContainer& path) const;
    double getNonNegativeWeight(const TreeNode* node) const;
    void getRefToAllLeaves(std::vector<TreeNode*>& allLeaves);
    void getRefToAllNodes(std::vector<TreeNode*>& allNodes);  
    void getRefToAssociatedNodes(TreeNode* node, std::vector<TreeNode*>& allNodes);
    #endif //SWIG
  
  protected:
    #ifndef SWIG
    TreeNode* _root;
    #endif //SWIG
};
#endif //_TREE_H_
