// Description: class of function with multiple input, multiple output
//
//
// Author:  <Van Dung NGUYEN>, (C) 2019
//
// Copyright: See COPYING file that comes with this distribution
//

#ifndef MIMOFUNCTION_H_
#define MIMOFUNCTION_H_

#ifndef SWIG
#include "fullMatrix.h"
#include "ANNUtils.h"
#endif // SWIG

class MIMOFunction
{
  #ifndef SWIG
  public:
    MIMOFunction(){}
    MIMOFunction(const MIMOFunction& src){}
    virtual ~MIMOFunction(){}

    virtual MIMOFunction* clone() const = 0;
    //
    // function [S,q1,DSDE] = funct(E,E0,q0)
    //  other =  other input parameters than E
    // output E1- strain, S-stress, DSDE- tangent operator, q1 -history
    // other and q1 are not necessarily the same size
    virtual void predict(const fullMatrix<double>& E1,
                         const fullMatrix<double>& E0,
                         const fullMatrix<double>& q0,
                         const fullMatrix<double>& S0,
                         fullMatrix<double>& q1,
                         fullMatrix<double>& S,
                         bool diff,
                         fullMatrix<double>* DSDE) const = 0;
  #endif // SWIG
};

class Scaler
{
  // bijective mapping for data transformation
  #ifndef SWIG
  public:
    Scaler(){}
    Scaler(const Scaler& src){}
    virtual ~Scaler(){}
    virtual void transform(const fullMatrix<double>& XReal, fullMatrix<double>& Xnormed, 
                bool stiff =false, fullMatrix<double>* DXnormedDXReal=NULL) const = 0;
    virtual void inverse_transform(const fullMatrix<double>& Xnormed, fullMatrix<double>& XReal, 
                                   bool stiff=false, fullMatrix<double>* DXRealDXnormed=NULL) const = 0;
    virtual Scaler* clone() const = 0;
  #endif // SWIG
};

class TrivialScaler : public Scaler
{
  public:
    TrivialScaler();
    TrivialScaler(const TrivialScaler& src);
    virtual ~TrivialScaler(){}
    virtual void transform(const fullMatrix<double>& XReal, fullMatrix<double>& Xnormed,
                           bool stiff =false, fullMatrix<double>* DXnormedDXReal=NULL) const;
    virtual void inverse_transform(const fullMatrix<double>& Xnormed, fullMatrix<double>& XReal,
                                   bool stiff =false, fullMatrix<double>* DXRealDXnormed=NULL) const;
    virtual Scaler* clone() const {return new TrivialScaler();}
};

class LinearMIMOFunction : public MIMOFunction
{
  fullMatrix<double> _K;

  public:
    LinearMIMOFunction(int numR, int numC);
    void setValue(int i, int j, double val);
    LinearMIMOFunction(const LinearMIMOFunction& src): MIMOFunction(src),_K(src._K){}
    virtual ~LinearMIMOFunction(){}
    virtual MIMOFunction* clone() const {return new LinearMIMOFunction(*this);};
    virtual void predict(const fullMatrix<double>& E1,
                         const fullMatrix<double>& E0,
                         const fullMatrix<double>& q0,
                         const fullMatrix<double>& S0,
                         fullMatrix<double>& q1,
                         fullMatrix<double>& S,
                         bool diff,
                         fullMatrix<double>* DSDE) const;
};

class ANNMIMOFunction : public MIMOFunction
{
  // no internal state is considered
  // sig = ann (eps)
  protected:
    #ifndef SWIG
    ArtificialNN* _ann;
    Scaler* _scalerIn;
    Scaler* _scalerOut;
    std::vector<int> _locEVars, _locSVars;
    #endif //SWIG
    
  public:
    ANNMIMOFunction(const ArtificialNN& a);
    ANNMIMOFunction(const ANNMIMOFunction& src);
    virtual ~ANNMIMOFunction();
    virtual MIMOFunction* clone() const {return new ANNMIMOFunction(*this);};
    //
    void setNumberOfStrainVariables(int numEVar);
    void setNumberOfStressVariables(int numSVar);
    //
    void setStrainLocation(int index, int loc);
    void setStressLocation(int index, int loc);
    
    void setScalerInputs(const Scaler& s);
    void setScalerOutputs(const Scaler& s);
    //
    virtual void predict(const fullMatrix<double>& E1,
                         const fullMatrix<double>& E0,
                         const fullMatrix<double>& q0,
                         const fullMatrix<double>& S0,
                         fullMatrix<double>& q1,
                         fullMatrix<double>& S,
                         bool diff,
                         fullMatrix<double>* DSDE) const;
};


class TwoANNMIMOFunction : public MIMOFunction
{
  /* two ANNs for stress-strain evaluation with the following steps
   * Deps = eps1 - eps0
   * Neps = Deps/|Deps|
   * DqDl = ann_q(Neps, eps0, q0)
   * q1 = q0 + DqDl*|Deps|
   * sig1 = ann_sig(eps,q1)
   *
   * */
  protected:
    #ifndef SWIG
    ArtificialNN* _annSig; // stress ann
    ArtificialNN* _annQ; // internal ann
    Scaler* _sigScalerIn;
    Scaler* _sigScalerOut;
    Scaler* _qScalerIn;
    Scaler* _qScalerOut;
    std::vector<int> _locEVars, _locSVars;
    double _tol;
    bool _blockInternalState; // to consider only _annSig
    bool _internalANNWithStress;
    bool _internalANNWithCurrentStrain;
    #endif //SWIG
        
  public:
    TwoANNMIMOFunction(const ArtificialNN& sig, const ArtificialNN& q, double tol = 1e-8);
    TwoANNMIMOFunction(const TwoANNMIMOFunction& src);
    virtual ~TwoANNMIMOFunction();
    virtual MIMOFunction* clone() const {return new TwoANNMIMOFunction(*this);};
    
    void useInternalANNWithStress(bool fl);
    void useInternalANNWithCurrentStrain(bool fl);
    void blockInternalState(bool fl);
    
    
    void setNumberOfStrainVariables(int numEVar);
    void setNumberOfStressVariables(int numSVar);
    //
    void setStrainLocation(int index, int loc);
    void setStressLocation(int index, int loc);
    
    void setScalerInputs(int annIndex, const Scaler& s);
    void setScalerOutputs(int annIndex, const Scaler& s);
    
    
    virtual void predict(const fullMatrix<double>& E1,
                         const fullMatrix<double>& E0,
                         const fullMatrix<double>& q0,
                         const fullMatrix<double>& S0,
                         fullMatrix<double>& q1,
                         fullMatrix<double>& S,
                         bool diff,
                         fullMatrix<double>* DSDE) const;
};
#endif // MIMOFUNCTION_H_