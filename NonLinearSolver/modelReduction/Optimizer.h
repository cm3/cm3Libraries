//
// C++ Interface:  optimizer for gradient based training
//
//
// Author:  <V.-D. Nguyen>, (C) 2021
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef _OPTIMIZER_H_
#define _OPTIMIZER_H_ 

#ifndef SWIG
#include "fullMatrix.h"
#include <math.h>
#endif //SWIG

/*! \brief Base class to define a loss function readily to be used in the Gradient Descent optimization
 */
class LossFunction
{
  public: 
    #ifndef SWIG
    virtual ~LossFunction(){};
    
    /*! @return the size of the training dataset
    */
    virtual int sizeTrainingSet() const = 0; 
    
    /*! \brief allow to estimate loss function and derivatives with respect to the fitting paramater
      @param[in] metric the metric used to define the loss function
      @param[in] sampleIndex a random training dataset (order of the sample is randomized)
      @param[out] grad the gradient of the loss function with respect to the fitting parameters
     */
    virtual double computeCostFunction(const std::string& metric, const std::vector<int>& sampleIndex, fullVector<double>& grad) = 0; 
    
    /*! \brief allow to estimate loss function in the whole training data set
      @param[in] metrics metrics used to define the loss function
      @param[out] vals all the values of the loss functions with respect to the metrics
     */
    virtual void evaluateTrainingSet(const std::vector<std::string>& metrics, std::vector<double>& vals) = 0;
    
    /*! \brief allow to estimate loss function in the whole test data set
      @param[in] metrics metrics used to define the loss function
      @param[out] vals all the values of the loss functions with respect to the metrics
     */
    virtual void evaluateTestSet(const std::vector<std::string>& metrics, std::vector<double>& vals) = 0;
    
    /*! @param[in] fname name of file to save the trained model during training
     */
    virtual void saveModel(const std::string fname) const = 0;
    
    /*! @param[out] W initialize the fitting parameters 
     */
    virtual void initializeFittingParameters(fullVector<double>& W) = 0; 
    
    /*! @param[out] WLower lower bound of the fitting parameters 
     *  @param[out] WUpper upper bound of the fitting parameters 
     */
    virtual void getUnknownBounds(fullVector<double>& WLower, fullVector<double>& WUpper) const = 0;
    
    /*! \brief update the model with the new fitting parameter
      @param[in] W the fitting parameters
     */
    virtual void updateModelFromFittingParameters(const fullVector<double>& W) = 0;
    
    /*! \brief restart the model in case of a failed training
     */
    virtual void reinitializeModel() = 0;
    #endif //SWIG
};


/*! \brief Base class to define an optimizer in the Gradient Descent optimization
 */
class Optimizer
{
  public:
    #ifndef SWIG
    virtual ~Optimizer(){}
    
    /*! \brief Base class to define an optimizer in the Gradient Descent optimization
     @param[in] lr the learning rate
     @param[in] g the gradient of the loss function
     @param[inout] Wcur the fitting parameters will be updated
    */
    virtual void step(double lr, const fullVector<double>& g, fullVector<double>& Wcur) = 0;
    
    /*! \brief initialize data in an optimizer
     @param[in] N size of the fitting parameters
    */
    virtual void initialize(int N) = 0;
    #endif //SWIG
};

/*! \brief Stochastic gradient descent optimizer
 */
class SGD : public Optimizer
{
  protected:
    double _momentum;
    fullVector<double> _vt;
    
  public:
    SGD(double momentum=0.);
    virtual ~SGD(){};
    
    /*! \brief Base class to define an optimizer in the Gradient Descent optimization
     @param[in] lr the learning rate
     @param[in] g the gradient of the loss function
     @param[inout] Wcur the fitting parameters will be updated
    */
    virtual void step(double lr, const fullVector<double>& g, fullVector<double>& Wcur);
    
    /*! \brief initialize data in an optimizer
     @param[in] N size of the fitting parameters
    */
    virtual void initialize(int N);
};

/*! \brief Adam optimizer
 */
class Adam : public Optimizer
{
  protected:
    double _beta1, _beta2, _eps;
    fullVector<double> _mt, _vt;
    double _step;
    
  public:
    Adam(double beta1=0.9, double beta2=0.999, double eps=1e-8);
    virtual ~Adam(){}
    
    /*! \brief Base class to define an optimizer in the Gradient Descent optimization
     @param[in] lr the learning rate
     @param[in] g the gradient of the loss function
     @param[inout] Wcur the fitting parameters will be updated
    */
    virtual void step(double lr, const fullVector<double>& g, fullVector<double>& Wcur);
    
    /*! \brief initialize data in an optimizer
     @param[in] N size of the fitting parameters
    */
    virtual void initialize(int N);
};

/*! \brief Base class for the learning rate decay
 */
class LearningRateScheduler
{
  #ifndef SWIG
  public:
    virtual ~LearningRateScheduler(){}
    virtual double getLr(int epochs) const = 0;
  #endif //SWIG
};

/*! \brief  Learning rate decay by a exponential law 
 */
class ExponentialLearningRateDecay  : public LearningRateScheduler
{
  protected:
      double _initialLr; ///< initial learing rate
      double _decay_rate; ///< the decay rate
      
  public:
    ExponentialLearningRateDecay(double lr0, double decay_rate, double decay_steps): _initialLr(lr0),_decay_rate(decay_rate){}
    ExponentialLearningRateDecay(const ExponentialLearningRateDecay& src):
      _initialLr(src._initialLr),_decay_rate(src._decay_rate){}
    virtual ~ExponentialLearningRateDecay(){}
    
    /*! To get the learning rate
    @param[in] step current epoch
    @return the learning rate
    */
    virtual double getLr(int step) const {
      return _initialLr * exp(-_decay_rate*step);
    }
};

/*! \brief  Learning rate decay by a power law 
 */
class PowerLearningRateDecay  : public LearningRateScheduler
{
  protected:
      double _initialLr;  ///< initial learing rate
      double _decay_rate;  ///< the decay rate
      double _decay_steps;  ///< the decay step
      
  public:
    PowerLearningRateDecay(double lr0, double decay_rate, double decay_steps): _initialLr(lr0),_decay_rate(decay_rate),_decay_steps(decay_steps){}
    PowerLearningRateDecay(const PowerLearningRateDecay& src):
      _initialLr(src._initialLr),_decay_rate(src._decay_rate),_decay_steps(src._decay_steps){}
    virtual ~PowerLearningRateDecay(){}
    
    
    /*! To get the learning rate
    @param[in] step current epoch
    @return the learning rate
    */
    virtual double getLr(int step) const {
      return _initialLr * pow(_decay_rate,step/_decay_steps);
    };
};

/*! \brief  Learning rate modifiable by a cosin
 */
class CosineLearningRateAnnealing : public LearningRateScheduler
{
  protected:
      double _lrMin, _lrMax;  ///< max and min learing rate
      double _M;  ///< cosine denominator
      
  public:
    CosineLearningRateAnnealing(double lrmin, double lrMax, double M): _lrMin(lrmin),_lrMax(lrMax),_M(M){}
    CosineLearningRateAnnealing(const CosineLearningRateAnnealing& src):
      _lrMin(src._lrMin),_lrMax(src._lrMax),_M(src._M){}
    virtual ~CosineLearningRateAnnealing(){}
    
    
    /*! To get the learning rate
    @param[in] step current epoch
    @return the learning rate
    */
    virtual double getLr(int step) const {
      return _lrMin + 0.5*(_lrMax - _lrMin)*(1.+ cos(3.14159265359*double(step)/_M));
    };
};

/*! \brief  Learning rate modifiable by a linear
 */
class LinearLearningRateDecay : public LearningRateScheduler
{
  protected:
      double _lrMin, _lrMax;  ///< max and min learing rate
      double _M;  ///< maximal denominator
      
  public:
    LinearLearningRateDecay(double lrmin, double lrMax, double M): _lrMin(lrmin),_lrMax(lrMax),_M(M){}
    LinearLearningRateDecay(const LinearLearningRateDecay& src):
      _lrMin(src._lrMin),_lrMax(src._lrMax),_M(src._M){}
    virtual ~LinearLearningRateDecay(){}
    
    
    /*! To get the learning rate
    @param[in] step current epoch
    @return the learning rate
    */
    virtual double getLr(int step) const {
      return _lrMax +(_lrMin - _lrMax)*double(step)/_M;
    };
};



/*! \brief  Training procedure based on Gradient Descent 
 */
class GradientDescentTraining
{
  #ifndef SWIG
  protected:
    Optimizer* _optimizer; // to find 
    LossFunction* _lossFunc; // to define L = diff(y(W), ypred)   
    LearningRateScheduler* _learningRateScheduler;
    bool _learningRateAdaptivity;
    double _reduceFactor, _increaseFactor;
   #endif //SWIG
   
  public: 
    GradientDescentTraining(LossFunction& lf, Optimizer& opt);
    GradientDescentTraining(LossFunction& lf, Optimizer& opt, LearningRateScheduler& lrScheduler);
    virtual ~GradientDescentTraining();

    /*! Iterative procedure in the gradient descent algorithm
    @param[in] lossMetric the loss metric
    @param[in] learningRate initial learning rate
    @param[in] numberBatches number of batches: 1 - stochastic training, size of training set - batch training, otherwise - mini-batch training
    @param[in] errorMetric the error matric
    @param[in] saveModelFileName the file name of the model saved during traiing
    @param[in] historyFileName the file name of the training history
    @param[in] lrMin minimal learning rate when adaptive learning rate is employed
    */
    void fit(std::string lossMetric, 
             double learningRate,
             int numEpochs,
             int numberBatches,
             std::string errorMetric,
             std::string saveModelFileName, 
             std::string historyFileName,
             double lrMin = 1e-10);
    
    /*! Function to activate the learning rate adaptivity
    @param[in] fl true if activate, false if deastivate the adaptivity
    @param[in] reduceFactor a factor smaller than 1 used to reduce the learning rate
    @param[in] increaseFactor a factor greter than 1 used to increate the learning rate
    */
    void learningRateAdaptivity(bool fl, double reduceFactor=0.8, double increaseFactor =1.2);
};

#endif //_OPTIMIZER_H_