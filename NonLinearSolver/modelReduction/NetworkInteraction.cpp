//
// C++ Interface: network interactions in deep material networks
//
//
// Author:  <V.-D. Nguyen>, (C) 2021
//
// Copyright: See COPYING file that comes with this distribution
//
//

#include "NetworkInteraction.h"
#include "ipFiniteStrain.h"
#include "highOrderTensor.h"
#include <random> 
#include <chrono>
#include <math.h>
#include "nlsField.h"
#include "Tree.h"
#include "MaterialNode.h"

void InteractionMechanism::getNormalVectorFromVars(const std::vector<double>& vars, SVector3& vec, bool stiff, std::vector<SVector3>* DvecDdirectionVars)
{
  static double pi = 3.14159265359;
  int numberVarsPerNormal = vars.size();
  if (numberVarsPerNormal == 1)
  {
    double theta = 2.*pi*vars[0];
    vec[0] = cos(theta);
    vec[1] = sin(theta);
    vec[2] = 0.;
    if (stiff)
    {
      if ((*DvecDdirectionVars).size() != 1)
      {
        (*DvecDdirectionVars).resize(1);
      }
      (*DvecDdirectionVars)[0][0] = -sin(theta)*2.*pi;
      (*DvecDdirectionVars)[0][1] = cos(theta)*2.*pi;
      (*DvecDdirectionVars)[0][2] = 0.;
      
    }
  }
  else if (numberVarsPerNormal == 2)
  {
    // cylindrical coordinates
    double phi = 2.*pi*vars[0];
    double theta = pi*vars[1];
    vec[0] = sin(theta)*cos(phi);
    vec[1] = sin(theta)*sin(phi);
    vec[2] = cos(theta);
    if (stiff)
    {
      if ((*DvecDdirectionVars).size() != 2)
      {
        (*DvecDdirectionVars).resize(2);
      }
      (*DvecDdirectionVars)[0][0] = -sin(theta)*sin(phi)*2*pi;
      (*DvecDdirectionVars)[0][1] = sin(theta)*cos(phi)*2*pi;
      (*DvecDdirectionVars)[0][2] = 0.;
      
      (*DvecDdirectionVars)[1][0] = cos(theta)*cos(phi)*pi;
      (*DvecDdirectionVars)[1][1] = cos(theta)*sin(phi)*pi;
      (*DvecDdirectionVars)[1][2] = -sin(theta)*pi;
    }
  }
  else
  {
    Msg::Error("number of vars per normal must be 1 or 2, a value of %d is impossible",numberVarsPerNormal);
    Msg::Exit(0);
  }
};

void InteractionMechanism::standardizeDirectionVariables(std::vector<double>& vars)
{
  int numberVarsPerNormal = vars.size();
  if (numberVarsPerNormal == 1)
  {
    double val = floor(vars[0]);
    vars[0] -= val;
  }
  else if (numberVarsPerNormal == 2)
  {
    SVector3 n;
    InteractionMechanism::getNormalVectorFromVars(vars,n);
    double z = n[2];
    bool ok = false;
    if (z > 1.) 
    {
      z = 1;
      ok = true;
    }
    else if (z < -1)
    {
      z = -1;
      ok = true;
    }
    //
    static double pi = 3.14159265359;
    vars[1] = acos(z)/pi;
    if (ok)
    {
      vars[0] = 0.;
    }
    else
    {
      double sinZ= sin(vars[1]*pi);
      double cosVal = n[0]/sinZ;
      double sinVal = n[1]/sinZ;
      double angle = atan2(sinVal,cosVal);
      if (angle < 0)
      {
        vars[0] = 1.+(angle)/(2*pi);
      }      
      else
      {
        vars[0] = (angle)/(2*pi);
      }
    }
  }
  else
  {
    Msg::Error("number of vars per normal must be 1 or 2, a value of %d is impossible",numberVarsPerNormal);
    Msg::Exit(0);
  }
}

const SVector3& InteractionMechanism::getConstRefToIncompatibleVector(const IPStateBase::whichState ws) const
{
  if (ws == IPStateBase::current)
  {
    return acur;
  }
  else if (ws == IPStateBase::previous)
  {
    return aprev;
  }
  else if (ws == IPStateBase::initial)
  {
    return ainit;
  }
  else
  {
    Msg::Error("state %s has not been defined in InteractionMechanism::getConstRefToIncompatibleVector",ws);
    Msg::Exit(0);
  }
};

SVector3& InteractionMechanism::getRefToIncompatibleVector(const IPStateBase::whichState ws)
{
  if (ws == IPStateBase::current)
  {
    return acur;
  }
  else if (ws == IPStateBase::previous)
  {
    return aprev;
  }
  else if (ws == IPStateBase::initial)
  {
    return ainit;
  }
  else
  {
    Msg::Error("state %s has not been defined in InteractionMechanism::getRefToIncompatibleVector",ws);
    Msg::Exit(0);
  }
};

int InteractionMechanism::maximumIndex = 0;

InteractionMechanism::InteractionMechanism():coefficients(),directionVars(0),acur(), aprev(), ainit(), coeffVars()
{
  index = InteractionMechanism::maximumIndex;
  InteractionMechanism::maximumIndex ++;
};
InteractionMechanism::~InteractionMechanism()
{
};

void InteractionMechanism::standardizeDirectionVars()
{
  InteractionMechanism::standardizeDirectionVariables(directionVars);
};

void InteractionMechanism::seperateNegativeAndPositive(std::vector<int>&allNegatives, std::vector<int>& allPositives) const
{
  allPositives.clear();
  allNegatives.clear();
  
  for (std::map<int,double>::const_iterator it = coefficients.begin(); it!= coefficients.end(); it++)
  {
    if (it->second >0)
    {
      allPositives.push_back(it->first);
    }
    else
    {
      allNegatives.push_back(it->first);
    }
  }
  
}

void InteractionMechanism::getDirection(SVector3& vec, bool stiff, std::vector<SVector3>* DvecDdirectionVars) const
{
  InteractionMechanism::getNormalVectorFromVars(directionVars,vec,stiff,DvecDdirectionVars);
};

void InteractionMechanism::printData() const
{
  printf("Interaction %d: \n",index);
  for (std::map<int,double>::const_iterator it = coefficients.begin(); it != coefficients.end(); it++)
  {
    int nodeId = it->first;
    printf("%d %16g\n",nodeId,it->second);
  };
  acur.print("current incompatible vector");
  aprev.print("previous incompatible vector");
  ainit.print("initial incompatible vector");
  
  static SVector3 vec;
  static std::vector<SVector3> DvecDDir;
  getDirection(vec,true,&DvecDDir);
  vec.print("direction");
  for (int j=0; j< DvecDDir.size(); j++)
  {
    std::string str = "derivative variable "+std::to_string(j)+"th";
    DvecDDir[j].print(str.c_str());
  }
};

bool InteractionMechanism::isTrivial() const
{
  for (std::map<int,double>::const_iterator it = coefficients.begin(); it != coefficients.end(); it++)
  {
    double alpha = it->second;
    if (fabs(alpha) > 0) return false;
  };
  return true;
}

void InteractionMechanism::saveToFile(FILE* file) const
{
  fprintf(file,"%d",index);
  int numberVarsPerNormal = directionVars.size();
  fprintf(file," %d",numberVarsPerNormal);
  for (int i=0; i< numberVarsPerNormal; i++)
  {
    fprintf(file, " %.16g",directionVars[i]);
  }
  fprintf(file," %ld",coefficients.size());
  for (std::map<int,double>::const_iterator it = coefficients.begin(); it != coefficients.end(); it++)
  {
    int nodeId = it->first;
    double alpha = it->second;
    fprintf(file," %d %.16g",nodeId,alpha);
  };
};
void InteractionMechanism::resetIncompatibleDispVector()
{
  STensorOperation::zero(acur);
  STensorOperation::zero(aprev);
  STensorOperation::zero(ainit);
};

void InteractionMechanism::nextStep()
{
  aprev = acur;
};
void InteractionMechanism::resetToPreviousStep()
{
  acur = aprev;
};

void InteractionMechanism::copy(const IPStateBase::whichState ws1, const IPStateBase::whichState ws2)
{
  getRefToIncompatibleVector(ws2) = getRefToIncompatibleVector(ws1);
};

void InteractionMechanism::getAllCoefficients(std::vector<double>& coeffs) const
{
  coeffs.clear();
  for (std::map<int,double>::const_iterator it = coefficients.begin(); it != coefficients.end(); it++)
  {
    coeffs.push_back(it->second);
  };
};

void InteractionMechanism::getAllMaterialNodeIds(std::vector<int>& nodes) const
{
  nodes.clear();
  for (std::map<int,double>::const_iterator it = coefficients.begin(); it != coefficients.end(); it++)
  {
    nodes.push_back(it->first);
  }
}
bool InteractionMechanism::inInteraction(int nodeIndex) const
{
  return coefficients.find(nodeIndex) != coefficients.end();
}
double InteractionMechanism::getCoefficientForNode(int nodeIndex) const
{
  std::map<int,double>::const_iterator itF = coefficients.find(nodeIndex);
  if (itF != coefficients.end())
  {
    return itF->second;
  }
  else
  {
    Msg::Error("node %d is not found in InteractionMechanism::getCoefficient", nodeIndex);
    return 0.;
  }
};

void InteractionMechanism::getInducedStrain(int nodeId, STensor3& Fi) const
{
  std::map<int,double>::const_iterator itF = coefficients.find(nodeId);
  if (itF != coefficients.end())
  {
    static SVector3 direction;
    getDirection(direction);
    double alpha = itF->second;
    for (int i=0; i<3; i++)
    {
      for (int j=0; j<3; j++)
      {
        Fi(i,j) = alpha*acur(i)*direction(j);
      }
    }
  }
  else
  {
    STensorOperation::zero(Fi);
  }
};

void InteractionMechanism::getDInducedStrainDa(int nodeId, STensor33& DFiDa) const
{
  std::map<int,double>::const_iterator itF = coefficients.find(nodeId);
  if (itF != coefficients.end())
  {
    static SVector3 direction;
    getDirection(direction);
    static STensor3 I(1.);
    double alpha = itF->second;
    for (int r=0; r<3; r++)
    {
      for (int c=0; c<3; c++)
      {
        for (int t=0; t< 3; t++)
        {
          DFiDa(r,c,t) = I(r,t)*direction(c)*alpha;
        }
      }
    };
  }
  else
  {
    STensorOperation::zero(DFiDa);
  }
};

void InteractionMechanism::getDInducedStrainDDirection(int nodeId, STensor33& DFiDdir) const
{
  std::map<int,double>::const_iterator itF = coefficients.find(nodeId);
  if (itF != coefficients.end())
  {
    static STensor3 I(1.);
    double alpha = itF->second;
    for (int r=0; r<3; r++)
    {
      for (int c=0; c<3; c++)
      {
        for (int t=0; t< 3; t++)
        {
          DFiDdir(r,c,t) = alpha*acur(r)*I(c,t);
        }
      }
    };
  }
  else
  {
    STensorOperation::zero(DFiDdir);
  }
};

void InteractionMechanism::getDInducedStrainDCoefficient(int nodeId, STensor3& DFiDalpha) const
{
  std::map<int,double>::const_iterator itF = coefficients.find(nodeId);
  if (itF != coefficients.end())
  {
    static SVector3 direction;
    getDirection(direction);
    static STensor3 I(1.);
    double alpha = itF->second;
    for (int r=0; r<3; r++)
    {
      for (int c=0; c<3; c++)
      {
        DFiDalpha(r,c) = acur(r)*direction(c);
      }
    };
  }
  else
  {
    STensorOperation::zero(DFiDalpha);
  }
};



NetworkInteraction::NetworkInteraction(int tag): _tag(tag)
{
  
}
NetworkInteraction::~NetworkInteraction()
{
  clearData();
}

int NetworkInteraction::getTag() const
{
  return _tag;
}

NetworkInteraction::NetworkInteraction(const NetworkInteraction& src): _tag(src._tag), _allMaterialNodes(), _allInteractions(), _nodeInteractionMap()
{
  for (MaterialNodeContainer::const_iterator it =src._allMaterialNodes.begin(); it!= src._allMaterialNodes.end(); it++)
  {
    _allMaterialNodes[it->first] = it->second->clone();
  }
  for (InteractionContainer::const_iterator it =src._allInteractions.begin(); it != src._allInteractions.end(); it++)
  {
    _allInteractions[it->first] = it->second->clone();
  };
  
  for (NodeMapContainer::const_iterator it = src._nodeInteractionMap.begin(); it != src._nodeInteractionMap.end(); it++)
  {
    const MaterialNode* nodeSrc = it->first;
    const std::vector<const InteractionMechanism*>& allMechanisms = it->second;
    
    const MaterialNode* curNode = _allMaterialNodes[nodeSrc->getNodeId()];
    std::vector<const InteractionMechanism*>& allCurMec = _nodeInteractionMap[curNode];
    allCurMec.resize(allMechanisms.size(),NULL);
    for (int j=0; j< allMechanisms.size(); j++)
    {
      allCurMec[j] = _allInteractions[allMechanisms[j]->getIndex()];      
    }
  }
};

void NetworkInteraction::copyFrom(const NetworkInteraction& src)
{
  // clear everything
  clearData();
  _tag = src._tag;
  _allMaterialNodes.clear();
  _allInteractions.clear(); 
  _nodeInteractionMap.clear();

  for (MaterialNodeContainer::const_iterator it =src._allMaterialNodes.begin(); it!= src._allMaterialNodes.end(); it++)
  {
    _allMaterialNodes[it->first] = it->second->clone();
  }
  for (InteractionContainer::const_iterator it =src._allInteractions.begin(); it != src._allInteractions.end(); it++)
  {
    _allInteractions[it->first] = it->second->clone();
  };
  
  for (NodeMapContainer::const_iterator it = src._nodeInteractionMap.begin(); it != src._nodeInteractionMap.end(); it++)
  {
    const MaterialNode* nodeSrc = it->first;
    const std::vector<const InteractionMechanism*>& allMechanisms = it->second;
    const MaterialNode* curNode = _allMaterialNodes[nodeSrc->getNodeId()];
    std::vector<const InteractionMechanism*>& allCurMec = _nodeInteractionMap[curNode];
    allCurMec.resize(allMechanisms.size(),NULL);
    for (int j=0; j< allMechanisms.size(); j++)
    {
      allCurMec[j] = _allInteractions[allMechanisms[j]->getIndex()];      
    }
  }
};

void NetworkInteraction::mergeNodesAllInteractions(const std::vector<int>& allNodes)
{
  for (InteractionContainer::iterator it =_allInteractions.begin(); it != _allInteractions.end(); it++)
  {
    InteractionMechanism* ie = it->second;
    mergeNodesInteraction(*ie,allNodes);
  };
};

void NetworkInteraction::mergeNodesInteraction(InteractionMechanism& ie, const std::vector<int>& allNodes) const
{
  for (int i=0; i< allNodes.size(); i++)
  {
    if (!ie.inInteraction(allNodes[i]))
    {
      return;
    }
  }
  std::map<int,double>& coef = ie.getRefToCoefficients();
  double Wtotal = 0.;
  double alpha = 0.;
  for (int i=0; i< allNodes.size(); i++)
  {
    double Wi = getWeight(allNodes[i]);
    Wtotal += Wi;
    alpha += coef[allNodes[i]]*Wi;
  };
  
  alpha /= Wtotal;
  coef[allNodes[0]] = alpha;
  for (int i=1; i< allNodes.size(); i++)
  {
    auto itF = coef.find(allNodes[i]);
    coef.erase(itF);
  }
};

void NetworkInteraction::mergeNodes(int phaseIndex,  // phase index 
                    int numberNodes,  // only interactions with numberNodes nodes
                    NetworkInteraction& newInteraction)
{
  newInteraction.copyFrom(*this);
  //
  InteractionContainer allMergedInteractions;
  for (InteractionContainer::iterator it =newInteraction._allInteractions.begin(); it != newInteraction._allInteractions.end(); it++)
  {
    InteractionMechanism* ie = it->second;
    if (ie->getNumMaterialNodes() == numberNodes)
    {
      allMergedInteractions[it->first] = it->second;
    }
  }
  Msg::Info("number of merged interactions = %d",allMergedInteractions.size());
  //
  MaterialNodeContainer allRemovedNodes;
  std::vector<int> allNodes;
  std::vector<MaterialNode*> allNodePhase;
  std::vector<int> allNodePhaseIndices;
  for (InteractionContainer::const_iterator it =allMergedInteractions.begin(); it != allMergedInteractions.end(); it++)
  {
    allNodes.clear();
    allNodePhase.clear();
    allNodePhaseIndices.clear();
    
    InteractionMechanism* ie = it->second;
    ie->getAllMaterialNodeIds(allNodes);
    for (int j=0; j< allNodes.size(); j++)
    {
      MaterialNode* node = newInteraction.getMaterialNode(allNodes[j]);
      if (node->matIndex == phaseIndex)
      {
        allNodePhase.push_back(node);
        allNodePhaseIndices.push_back(allNodes[j]);
      }
    };
    if (allNodePhase.size() > 1)
    {
      MaterialNode* firstNode = allNodePhase[0];
      double W0 = newInteraction.getWeight(firstNode);
      for (int k=1; k< allNodePhase.size(); k++)
      {
        double Wk  = newInteraction.getWeight(allNodePhase[k]);
        W0 += Wk;
        allRemovedNodes[allNodePhaseIndices[k]] = allNodePhase[k];
      }
      newInteraction.mergeNodesAllInteractions(allNodePhaseIndices);
      firstNode->weight = firstNode->af->getReciprocalVal(W0);
    };
  };
  
  for (MaterialNodeContainer::iterator it =allRemovedNodes.begin(); it!= allRemovedNodes.end(); it++)
  {
    auto itF = newInteraction._allMaterialNodes.find(it->first);
    newInteraction._allMaterialNodes.erase(itF);
    auto itFlist = newInteraction._nodeInteractionMap.find(it->second);
    newInteraction._nodeInteractionMap.erase(itFlist);
    delete it->second;
  }
  // check validity
  newInteraction.checkValidity();
};
void NetworkInteraction::clearData()
{
  //clear data because of new
  for (InteractionContainer::iterator it =_allInteractions.begin(); it != _allInteractions.end(); it++)
  {
    delete it->second;
  }
  _allInteractions.clear();
  //clear data because of new
  for (MaterialNodeContainer::iterator it =_allMaterialNodes.begin(); it!= _allMaterialNodes.end(); it++)
  {
    delete it->second;
  }
  _allMaterialNodes.clear();
  _nodeInteractionMap.clear();
  
  InteractionMechanism::maximumIndex = 0;
  MaterialNode::maxIndex = 0;
};


void NetworkInteraction::getNumberMaterialNodesPerMat(std::map<int, int>& nodePerMat) const
{
  nodePerMat.clear();
  for (MaterialNodeContainer::const_iterator it =_allMaterialNodes.begin(); it!= _allMaterialNodes.end(); it++)
  {
    const MaterialNode* node = it->second;
    if (nodePerMat.find(node->matIndex) == nodePerMat.end())
    {
      nodePerMat[node->matIndex] = 0;
    }
    int& numNodes = nodePerMat[node->matIndex];
    numNodes ++;
  }
};

void NetworkInteraction::getAllMaterialNodeIds(std::vector<int>& allIds) const
{
  allIds.clear();
  for (MaterialNodeContainer::const_iterator it =_allMaterialNodes.begin(); it!= _allMaterialNodes.end(); it++)
  {
    allIds.push_back(it->second->getNodeId());
  }
};

void NetworkInteraction::getAllMaterialNodeIdsForMat(int matNum, std::vector<int>& allIds) const
{
  allIds.clear();
  for (MaterialNodeContainer::const_iterator it =_allMaterialNodes.begin(); it!= _allMaterialNodes.end(); it++)
  {
    const MaterialNode* node = it->second;
    if (node->matIndex == matNum)
    {
      allIds.push_back(node->getNodeId());
    }
  }
};

void NetworkInteraction::getAllMaterialNodesForMat(int matNum, std::vector<const MaterialNode*>& allNode) const
{
  allNode.clear();
  for (MaterialNodeContainer::const_iterator it =_allMaterialNodes.begin(); it!= _allMaterialNodes.end(); it++)
  {
    const MaterialNode* node = it->second;
    if (node->matIndex == matNum)
    {
      allNode.push_back(node);
    }
  };
}

void NetworkInteraction::getAllMaterialNodes(std::vector<const MaterialNode*>& allNode) const
{
  allNode.clear();
  for (MaterialNodeContainer::const_iterator it =_allMaterialNodes.begin(); it!= _allMaterialNodes.end(); it++)
  {
    const MaterialNode* node = it->second;
    allNode.push_back(node);
  };
}
void NetworkInteraction::getAllMaterialNodes(std::vector<MaterialNode*>& allNode)
{
  allNode.clear();
  for (MaterialNodeContainer::iterator it =_allMaterialNodes.begin(); it!= _allMaterialNodes.end(); it++)
  {
    MaterialNode* node = it->second;
    allNode.push_back(node);
  };
}
    
void NetworkInteraction::getMaterialNodesByMaterialIndex(int matIndex, std::vector<MaterialNode*>& allNode)
{
  allNode.clear();
  for (MaterialNodeContainer::iterator it =_allMaterialNodes.begin(); it!= _allMaterialNodes.end(); it++)
  {
    MaterialNode* node = it->second;
    if (node->matIndex == matIndex)
    {
      allNode.push_back(node);
    }
  };
}

void NetworkInteraction::getMaterialNodesByMaterialIndex(int matIndex, std::vector<const MaterialNode*>& allNode) const
{
  allNode.clear();
  for (MaterialNodeContainer::const_iterator it =_allMaterialNodes.begin(); it!= _allMaterialNodes.end(); it++)
  {
    const MaterialNode* node = it->second;
    if (node->matIndex == matIndex)
    {
      allNode.push_back(node);
    }
  };
};


MaterialNode* NetworkInteraction::getMaterialNode(const int number)
{
  MaterialNodeContainer::iterator itF = _allMaterialNodes.find(number);
  if (itF != _allMaterialNodes.end())
  {
    return itF->second;
  }
  Msg::Error("material node %d is not found",number);
  return NULL;
};
const MaterialNode* NetworkInteraction::getMaterialNode(const int number) const
{
  MaterialNodeContainer::const_iterator itF = _allMaterialNodes.find(number);
  if (itF != _allMaterialNodes.end())
  {
    return itF->second;
  }
  Msg::Error("material node %d is not found",number);
  return NULL;
};

const std::vector<const InteractionMechanism*>* NetworkInteraction::getInteractionsForNode(const MaterialNode* node) const
{
  NodeMapContainer::const_iterator itFind = _nodeInteractionMap.find(node);
  if (itFind != _nodeInteractionMap.end())
  {
    return &(itFind->second);
  }
  Msg::Error("material node %d is not found",node->numberIndex);
  Msg::Exit(0);
  return NULL;
};

void NetworkInteraction::getDFDIncompatibleVector(const MaterialNode* node, std::vector<STensor33>& DFiDa) const
{
  NodeMapContainer::const_iterator itFind = _nodeInteractionMap.find(node);
  if (itFind != _nodeInteractionMap.end())
  {
    const std::vector<const InteractionMechanism*>& allInteractions = itFind->second;
    int numInteractions = allInteractions.size();
    if (DFiDa.size() < numInteractions)
    {
      DFiDa.resize(numInteractions,STensor33(0.));
    }
    for (int i=0; i< numInteractions; i++)
    {
      const InteractionMechanism* im = allInteractions[i];
      im->getDInducedStrainDa(node->numberIndex, DFiDa[i]);
    };
  };
};
void NetworkInteraction::getDFDNormal(const MaterialNode* node, std::vector<STensor33>& DFiDn) const
{
  NodeMapContainer::const_iterator itFind = _nodeInteractionMap.find(node);
  if (itFind != _nodeInteractionMap.end())
  {
    const std::vector<const InteractionMechanism*>& allInteractions = itFind->second;
    int numInteractions = allInteractions.size();
    if (DFiDn.size() < numInteractions)
    {
      DFiDn.resize(numInteractions,STensor33(0.));
    }
    for (int i=0; i< numInteractions; i++)
    {
      const InteractionMechanism* im = allInteractions[i];
      im->getDInducedStrainDDirection(node->numberIndex, DFiDn[i]);
    };
  };
}
void NetworkInteraction::getDFDCoefficient(const MaterialNode* node, std::vector<STensor3>& DFiDalpha) const
{
  NodeMapContainer::const_iterator itFind = _nodeInteractionMap.find(node);
  if (itFind != _nodeInteractionMap.end())
  {
    const std::vector<const InteractionMechanism*>& allInteractions = itFind->second;
    int numInteractions = allInteractions.size();
    if (DFiDalpha.size() < numInteractions)
    {
      DFiDalpha.resize(numInteractions,STensor3(0.));
    }
    for (int i=0; i< numInteractions; i++)
    {
      const InteractionMechanism* im = allInteractions[i];
      im->getDInducedStrainDCoefficient(node->numberIndex, DFiDalpha[i]);
    };
  };
}

void NetworkInteraction::getDStressDIncompatibleVector(const MaterialNode* node, std::vector<STensor33>& DPiDa) const
{
  NodeMapContainer::const_iterator itFind = _nodeInteractionMap.find(node);
  if (itFind != _nodeInteractionMap.end())
  {
    const STensor43& Li = getTangent(node);
    const std::vector<const InteractionMechanism*>& allInteractions = itFind->second;
    int numInteractions = allInteractions.size();
    if (DPiDa.size() < numInteractions)
    {
      DPiDa.resize(numInteractions,STensor33(0.));
    }
    for (int i=0; i< numInteractions; i++)
    {
      const InteractionMechanism* im = allInteractions[i];
      static STensor33 DFiDa;
      im->getDInducedStrainDa(node->numberIndex, DFiDa);
      STensorOperation::multSTensor43STensor33(Li,DFiDa,DPiDa[i]);
    };
  };
};
void NetworkInteraction::getDStressDNormal(const MaterialNode* node, std::vector<STensor33>& DPiDn) const
{
  NodeMapContainer::const_iterator itFind = _nodeInteractionMap.find(node);
  if (itFind != _nodeInteractionMap.end())
  {
    const STensor43& Li = getTangent(node);
    const std::vector<const InteractionMechanism*>& allInteractions = itFind->second;
    int numInteractions = allInteractions.size();
    if (DPiDn.size() < numInteractions)
    {
      DPiDn.resize(numInteractions,STensor33(0.));
    }
    for (int i=0; i< numInteractions; i++)
    {
      const InteractionMechanism* im = allInteractions[i];
      static STensor33 DFiDn;
      im->getDInducedStrainDDirection(node->numberIndex, DFiDn);
      STensorOperation::multSTensor43STensor33(Li,DFiDn,DPiDn[i]);
    };
  };
}
void NetworkInteraction::getDStressDCoefficient(const MaterialNode* node, std::vector<STensor3>& DPiDalpha) const
{
  NodeMapContainer::const_iterator itFind = _nodeInteractionMap.find(node);
  if (itFind != _nodeInteractionMap.end())
  {
    const STensor43& Li = getTangent(node);
    const std::vector<const InteractionMechanism*>& allInteractions = itFind->second;
    int numInteractions = allInteractions.size();
    if (DPiDalpha.size() < numInteractions)
    {
      DPiDalpha.resize(numInteractions,STensor3(0.));
    }
    for (int i=0; i< numInteractions; i++)
    {
      const InteractionMechanism* im = allInteractions[i];
      static STensor3 DFiDalpha;
      im->getDInducedStrainDCoefficient(node->numberIndex, DFiDalpha);
      STensorOperation::multSTensor43STensor3(Li,DFiDalpha,DPiDalpha[i]);
    };
  };
}

const STensor3& NetworkInteraction::getDeformationGradient(const MaterialNode* node) const
{
  if (node->ips != NULL)
  {
    const ipFiniteStrain* ipvf = dynamic_cast<const ipFiniteStrain*>(node->ips->getState(IPStateBase::current));
    if (ipvf != NULL)
    {
      return ipvf->getConstRefToDeformationGradient();
    }
    Msg::Error("ipFiniteStrain must be used");    
  }
  else
  {
    Msg::Error("this function should be called");
    static STensor3 F(1.);
    return F;
  }
};

const STensor3& NetworkInteraction::getStress(const MaterialNode* node) const
{
  if (node->ips != NULL)
  {
    const ipFiniteStrain* ipvf = dynamic_cast<const ipFiniteStrain*>(node->ips->getState(IPStateBase::current));
    if (ipvf != NULL)
    {
      return ipvf->getConstRefToFirstPiolaKirchhoffStress();
    }
    Msg::Error("ipFiniteStrain must be used");    
  }
  else
  {
    static STensor3 P(0.);
    return P;
  }
}
const STensor43& NetworkInteraction::getTangent(const MaterialNode* node ) const
{
  if (node->ips != NULL)
  {
    const ipFiniteStrain* ipvf = dynamic_cast<const ipFiniteStrain*>(node->ips->getState(IPStateBase::current));
    if (ipvf != NULL)
    {
      return ipvf->getConstRefToTangentModuli();
    }
    Msg::Error("ipFiniteStrain must be used");
  }
  else
  {
    static STensor43 L(0.);
    return L;
  }
};

void NetworkInteraction::getStress(STensor3& P) const
{
  STensorOperation::zero(P);
  double Wtotal = 0.;
  for (MaterialNodeContainer::const_iterator it =_allMaterialNodes.begin(); it!= _allMaterialNodes.end(); it++)
  {
    const MaterialNode* node = it->second;
    double Wi = getWeight(node);
    const STensor3& Plocal = getStress(node);
    P.daxpy(Plocal,Wi);
    Wtotal += Wi;
  }
  P *= (1./Wtotal);
};

void NetworkInteraction::getDeformationGradientPhase(int phaseIndex, STensor3& F) const
{
  STensorOperation::zero(F);
  bool haveNodes = false;
  double Wtotal = 0.;
  for (MaterialNodeContainer::const_iterator it =_allMaterialNodes.begin(); it!= _allMaterialNodes.end(); it++)
  {
    const MaterialNode* node = it->second;
    if (node->matIndex == phaseIndex)
    {
      haveNodes = true;
      double Wi = getWeight(node);
      const STensor3& Flocal = getDeformationGradient(node);
      F.daxpy(Flocal,Wi);
      Wtotal += Wi;
    }
  }
  if (haveNodes)
  {
    F *= (1./Wtotal);
  }
  else
  {
    STensorOperation::unity(F);
  }
};

int NetworkInteraction::getNumberOfMaterialNodes() const
{
  return _allMaterialNodes.size();
};

int NetworkInteraction::getDimension() const
{
  int dim = 0;
  for (InteractionContainer::const_iterator it = _allInteractions.begin(); it != _allInteractions.end(); it++)
  {
    const InteractionMechanism* im = it->second;
    int numDirVars= im->getDirectionVars().size();
    if (dim < numDirVars+1)
    {
      dim = numDirVars+1;
    }
  }
  return dim;
}

double NetworkInteraction::getTotalWeight() const
{
  double Wtotal = 0.;
  for (MaterialNodeContainer::const_iterator it =_allMaterialNodes.begin(); it!= _allMaterialNodes.end(); it++)
  {
    const MaterialNode* node = it->second;
    Wtotal += getWeight(node);
  }
  return Wtotal;
};

double NetworkInteraction::getPhaseFraction(int phase) const
{
  double Wtotal = 0.;
  double Wphase = 0.;
  for (MaterialNodeContainer::const_iterator it =_allMaterialNodes.begin(); it!= _allMaterialNodes.end(); it++)
  {
    const MaterialNode* node = it->second;
    Wtotal += getWeight(node);
    if (node->matIndex == phase)
    {
      Wphase += getWeight(node);
    }
  }
  return (Wphase/Wtotal);
}

void NetworkInteraction::getPhaseFractions(std::map<int, double>& allPhase) const
{
  //Msg::Info("fraction of all phases:");
  allPhase.clear();
  double WT = 0.;
  for (MaterialNodeContainer::const_iterator it =_allMaterialNodes.begin(); it!= _allMaterialNodes.end(); it++)
  {
    const MaterialNode* node = it->second;
    if (allPhase.find(node->matIndex) == allPhase.end())
    {
      allPhase[node->matIndex] = 0.;
    }
    WT += getWeight(node);
    allPhase[node->matIndex] += getWeight(node);
  }
  for (std::map<int, double>::iterator it = allPhase.begin(); it!= allPhase.end(); it++)
  {
    it->second *= (1./WT);
    //Msg::Info("phase %d fraction %.16g",it->first,it->second);
  }
}

void NetworkInteraction::getTotalWeightAllPhases(std::map<int, double>& allPhase) const
{
  Msg::Info("sum of weights of all phases:");
  allPhase.clear();
  for (MaterialNodeContainer::const_iterator it =_allMaterialNodes.begin(); it!= _allMaterialNodes.end(); it++)
  {
    const MaterialNode* node = it->second;
    allPhase[node->matIndex] = 0.;
  }
  for (std::map<int, double>::iterator it = allPhase.begin(); it!= allPhase.end(); it++)
  {
    it->second = getTotalWeightPhase(it->first);
    Msg::Info("phase %d sum of weights = %.16g",it->first,it->second);
  }
};

double NetworkInteraction::getTotalWeightPhase(int iphase) const
{;
  double Wtotal = 0.;
  for (MaterialNodeContainer::const_iterator it =_allMaterialNodes.begin(); it!= _allMaterialNodes.end(); it++)
  {
    const MaterialNode* node = it->second;
    if (iphase == node->matIndex)
    {
      Wtotal += getWeight(node);
    }
  }
  return Wtotal;
};

int NetworkInteraction::getPhaseIndex(int nodeId) const
{
  const MaterialNode* node = getMaterialNode(nodeId);
  if (node) return node->matIndex;
  else
  {
    Msg::Error("node %d does not exist",nodeId);
    return 0;
  }
}

double NetworkInteraction::getWeight(const MaterialNode* node) const
{
  return node->af->getVal(node->weight);
};

double NetworkInteraction::getWeight(int nodeId) const
{
  return getWeight(getMaterialNode(nodeId));
}

void NetworkInteraction::getWeights(const InteractionMechanism* im, std::vector<double>& allWeights) const
{
  std::vector<int> allIds;
  im->getAllMaterialNodeIds(allIds);
  int size = allIds.size();
  allWeights.resize(size);
  for (int i=0; i< size; i++)
  {
    const MaterialNode* node = getMaterialNode(allIds[i]);
    allWeights[i] = getWeight(node);
  }
};

void NetworkInteraction::saveDataToFile(std::string filename) const
{
  printf("saving data to file %s\n",filename.c_str());
  //
  FILE * file = fopen(filename.c_str(),"w");
  //
  fprintf(file,"%ld",_allMaterialNodes.size());
  std::map<int, const MaterialNode*> nodeMap;
  for (MaterialNodeContainer::const_iterator it =_allMaterialNodes.begin(); it!= _allMaterialNodes.end(); it++)
  {
    fprintf(file,"\n");
    it->second->saveToFile(file);
  };
  fprintf(file,"\n");
  //
  fprintf(file,"%ld",_allInteractions.size());
  for (InteractionContainer::const_iterator it = _allInteractions.begin(); it != _allInteractions.end(); it++)
  {
    fprintf(file,"\n");
    it->second->saveToFile(file);
  }
  //
  fclose(file);
  //
  printf("done saving data to file %s\n",filename.c_str());
  // cohesive
  
  std::map<int, const MaterialNode*> allNodesWithCohesive;
  for (MaterialNodeContainer::const_iterator it = _allMaterialNodes.begin(); it != _allMaterialNodes.end(); it++)
  {
    if (it->second->withCohesive())
    {
      allNodesWithCohesive[it->first] = it->second;
    }
  }
  if (allNodesWithCohesive.size() > 0)
  {
    filename  = "cohesive_"+filename;
    printf("saving data to file %s\n",filename.c_str());
    FILE * fileCohesive = fopen(filename.c_str(),"w");
    fprintf(file,"%ld\n",allNodesWithCohesive.size());
    for (std::map<int, const MaterialNode*>::iterator  it = allNodesWithCohesive.begin(); it != allNodesWithCohesive.end(); it++)
    {
      it->second->saveToFileCohesive(fileCohesive);
    }
    fclose(fileCohesive);
    printf("done saving data to file %s\n",filename.c_str());
  }
};

void NetworkInteraction::createInteractionFromPairsData(std::string filename, int dim, std::string affunc)
{
  // random weight for leaves
  unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
  std::default_random_engine generator(seed);
  std::uniform_real_distribution<double> distribution(0., 1.); 
  
  auto getVal = [&](double min, double max)
  {
    double x = 0.5;
    if (rand)
    {
      x= distribution(generator);
    }
    return (max-min)*x + min;
  };
  
  clearData();
  printf("create interactions from pairs data in file %s\n",filename.c_str());
  FILE* fp = fopen(filename.c_str(),"r");
  if (fp !=NULL)
  {
    printf("start reading file: %s\n",filename.c_str());
    while (1)
    {
      if(feof(fp))
        break;
        
      int numberMNodes= 0;
      if( fscanf(fp, "%d",&numberMNodes)==1)
      {
        printf("number of material nodes = %d\n",numberMNodes);
      }
      else
      {
        Msg::Error("error A reading file in NetworkInteraction::createInteractionFromPairsData");
        fclose(fp);
        Msg::Exit(0);
      }
      activationFunction* af = activationFunction::createActivationFunction(affunc.c_str());
      std::vector<double> randomPositives(numberMNodes,0.);
      double random = true;
      double sumW = 0.;
      for (int i=0; i< numberMNodes; i++)
      {
        double val = 0.5;
        if (random)
        {
          val = getVal(0.1,1.);
        }
        randomPositives[i] = af->getReciprocalVal(val);
        sumW += val;
      };
      
      for (int in=0; in < numberMNodes; in++)
      {
        MaterialNode* node = new MaterialNode();
        if (fscanf(fp, "%d %d", &(node->numberIndex),&(node->matIndex))==2)
        {
          printf("--------id=%d, mat index = %d \n", node->numberIndex,node->matIndex);
          node->af = activationFunction::createActivationFunction(affunc.c_str());
          node->weight = randomPositives[in]/sumW;
          _allMaterialNodes[node->numberIndex] = node;
        }
        else
        {
          Msg::Error("error B reading file in NetworkInteraction::createInteractionFromPairsData");
          fclose(fp);
          Msg::Exit(0);
        }
      }
      
      // interaction
      int numberInteraction= 0;
      if( fscanf(fp, "%d",&numberInteraction)==1)
      {
        printf("number of interaction = %d\n",numberInteraction);
      }
      else
      {
        Msg::Error("error C reading file in NetworkInteraction::createInteractionFromPairsData");
        fclose(fp);
        Msg::Exit(0);
      }
      for (int in=0; in < numberInteraction; in++)
      {
        InteractionMechanism* im = new InteractionMechanism();
        im->getRefToIndex() = in;
        //
        std::vector<double>& directionVars = im->getRefToDirectionVars();
        if (dim ==2)
        {
          directionVars.resize(1);
          directionVars[0] = getVal(0.,1.);
        }
        else if (dim == 3)
        {
          directionVars.resize(2);
          directionVars[0] = getVal(0.,1.);
          directionVars[1] = getVal(0.,1.);
        }
        else
        {
          Msg::Error("dim = %d is not correctly defined",dim);
          Msg::Exit(0);
        }
        //
        std::map<int,double>& coefficients = im->getRefToCoefficients();
        int node1(0), node2(0);
        if (fscanf(fp, "%d %d", &node1, &node2)==2)
        {
          printf(" node left = %d, node right = %d\n", node1,node2);
          MaterialNode* matNodeLeft = _allMaterialNodes[node1];
          MaterialNode* matNodeRight = _allMaterialNodes[node2];
          double Wleft = matNodeLeft->af->getVal(matNodeLeft->weight);
          double Wright = matNodeRight->af->getVal(matNodeRight->weight);
          
          coefficients[node1] = Wright/(Wleft+Wright);
          coefficients[node2] = -Wleft/(Wleft+Wright);
          
          _allInteractions[im->getIndex()] = im;
          _nodeInteractionMap[matNodeLeft].push_back(im);
          _nodeInteractionMap[matNodeRight].push_back(im);
        }
        else
        {
          Msg::Error("error H reading file in NetworkInteraction::createInteractionFromPairsData");
          fclose(fp);
          Msg::Exit(0);
        }
      }
      
      auto getSumWeights = [&](const std::vector<int>& nodeIdList)
      {
        double val = 0.;
        for (int jj=0; jj < nodeIdList.size(); jj++)
        {
          const MaterialNode* node = getMaterialNode(nodeIdList[jj]);
          val += getWeight(node);
        }
        return val;
      };
      
      // obtain a time-based seed:
      auto devideByTwo = [](const std::vector<int>& foo, std::vector<int>& left, std::vector<int>& right)
      {
        unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
        left = foo;
        right.clear();
        while (right.size()*2 < foo.size() && (!left.empty()))
        {
          std::shuffle (left.begin(), left.end(), std::default_random_engine(seed));
          right.push_back(left.back());
          left.pop_back();
        }
      };
      
      // add some full interfaction
      int numberInteractionsFull = 10;      
      std::vector<int> allNodeIds;
      //getAllMaterialNodeIds(allNodeIds);
      getAllMaterialNodeIdsForMat(1,allNodeIds);
      for (int i=0; i< numberInteractionsFull; i++)
      {
        InteractionMechanism* im = new InteractionMechanism();
        std::vector<int> left, right;
        devideByTwo(allNodeIds,left,right);
        double Wleft = getSumWeights(left);
        double Wright = getSumWeights(right);
        
        std::vector<double>& directionVars = im->getRefToDirectionVars();
        if (dim ==2)
        {
          directionVars.resize(1);
          directionVars[0] = getVal(0.,1.);
        }
        else if (dim == 3)
        {
          directionVars.resize(2);
          directionVars[0] = getVal(0.,1.);
          directionVars[1] = getVal(0.,1.);
        }
        else
        {
          Msg::Error("dim = %d is not correctly defined",dim);
          Msg::Exit(0);
        }
        std::map<int,double>& coefficients = im->getRefToCoefficients();
        coefficients.clear();
        for (int jj=0; jj< left.size(); jj++)
        {
          coefficients[left[jj]] = Wright/(Wleft+Wright);
        }
        for (int jj=0; jj< right.size(); jj++)
        {
          coefficients[right[jj]] = -Wleft/(Wleft+Wright);
        }
        //
        _allInteractions[im->getIndex()] = im;
        for (MaterialNodeContainer::const_iterator it=_allMaterialNodes.begin(); it != _allMaterialNodes.end(); it++)
        {
          _nodeInteractionMap[it->second].push_back(im);
        }
      }
      
      break;
    }
    fclose(fp);
    //
    printf("done loading data from file %s\n",filename.c_str());
  }
  else
  {
    Msg::Error("File %s does not exist",filename.c_str());
    Msg::Exit(0);
  }
  bool ok = checkValidity();
  if (!ok)
  {
    Msg::Exit(0);
  }
  printf("done creating interactions from pairs data in file %s\n",filename.c_str());
};

void NetworkInteraction::createInteractionFullByDivison(int numPhases, int nodePerPhase, int numInteraction, int dim, std::string affunc)
{
  // random weight for leaves
  unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
  std::default_random_engine generator(seed);
  std::uniform_real_distribution<double> distribution(0., 1.); 
  
  auto getVal = [&](double min, double max)
  {
    double x = 0.5;
    if (rand)
    {
      x= distribution(generator);
    }
    return (max-min)*x + min;
  };
  
  clearData();
  for (int p = 0; p< numPhases; p++)
  {
    for (int in=0; in < nodePerPhase; in++)
    {
      MaterialNode* node = new MaterialNode();
      node->af = activationFunction::createActivationFunction(affunc.c_str());
      node->weight = getVal(1.,2.);
      node->matIndex = p;
      _allMaterialNodes[node->numberIndex] = node;
    };
  }
  //
  auto getSumWeights = [&](const std::vector<int>& nodeIdList)
  {
    double val = 0.;
    for (int jj=0; jj < nodeIdList.size(); jj++)
    {
      const MaterialNode* node = getMaterialNode(nodeIdList[jj]);
      val += getWeight(node);
    }
    return val;
  };
  
  // obtain a time-based seed:
  auto devideByTwo = [](const std::vector<int>& foo, std::vector<int>& left, std::vector<int>& right)
  {
    unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
    left = foo;
    right.clear();
    while (right.size()*2 < foo.size() && (!left.empty()))
    {
      std::shuffle (left.begin(), left.end(), std::default_random_engine(seed));
      right.push_back(left.back());
      left.pop_back();
    }
  };
  
  // add some full interfaction
  std::vector<int> allNodeIds;
  getAllMaterialNodeIds(allNodeIds);
  for (int i=0; i< numInteraction; i++)
  {
    InteractionMechanism* im = new InteractionMechanism();
    std::vector<int> left, right;
    devideByTwo(allNodeIds,left,right);
    double Wleft = getSumWeights(left);
    double Wright = getSumWeights(right);
    
    std::vector<double>& directionVars = im->getRefToDirectionVars();
    if (dim ==2)
    {
      directionVars.resize(1);
      directionVars[0] = getVal(0.,1.);
    }
    else if (dim == 3)
    {
      directionVars.resize(2);
      directionVars[0] = getVal(0.,1.);
      directionVars[1] = getVal(0.,1.);
    }
    else
    {
      Msg::Error("dim = %d is not correctly defined",dim);
      Msg::Exit(0);
    }
    std::map<int,double>& coefficients = im->getRefToCoefficients();
    coefficients.clear();
    for (int jj=0; jj< left.size(); jj++)
    {
      coefficients[left[jj]] = Wright/(Wleft+Wright);
    }
    for (int jj=0; jj< right.size(); jj++)
    {
      coefficients[right[jj]] = -Wleft/(Wleft+Wright);
    }
    //
    _allInteractions[im->getIndex()] = im;
    for (MaterialNodeContainer::const_iterator it=_allMaterialNodes.begin(); it != _allMaterialNodes.end(); it++)
    {
      _nodeInteractionMap[it->second].push_back(im);
    }
  }
};


void NetworkInteraction::loadCohesiveDataFromFile(std::string filename)
{
  printf("loading cohesive data from file %s\n",filename.c_str());
  FILE* fp = fopen(filename.c_str(),"r");
  if (fp !=NULL)
  {
    printf("start reading file: %s\n",filename.c_str());
    while (1)
    {
      if(feof(fp))
        break;
        
      int numberCohesiveNodes= 0;
      if( fscanf(fp, "%d",&numberCohesiveNodes)==1)
      {
        printf("number of cohesive nodes = %d\n",numberCohesiveNodes);
      }
      else
      {
        Msg::Error("error reading file in NetworkInteraction::loadCohesiveDataFromFile");
        fclose(fp);
        Msg::Exit(0);
      }
      for (int i=0; i< numberCohesiveNodes; i++)
      {
        
        int nodeId= 0;
        int cohMatIndex = 0;
        int numCoh = 0;
        int numCohDirVars = 0;
        if( fscanf(fp, "%d %d %d %d",&nodeId,&cohMatIndex,&numCoh,&numCohDirVars)==4)
        {
          printf("material node Id = %d cohMatIndex=%d numCoh = %d numDirVars = %d\n",nodeId,cohMatIndex,numCoh,numCohDirVars);
        }
        else
        {
          Msg::Error("error A reading file in NetworkInteraction::loadCohesiveDataFromFile");
          fclose(fp);
          Msg::Exit(0);
        }
        MaterialNode* node = getMaterialNode(nodeId);
        if (node == NULL)
        {
          Msg::Exit(0);
        }
        if (numCoh > 0 && numCohDirVars > 0)
        {
          node->cohesiveMatIndex = cohMatIndex;
          node->allocateCohesiveParameters(numCoh,numCohDirVars);
          fullVector<double>& cohCoefs = node->cohesiveCoeffVars;
          fullMatrix<double>& cohDirVars = node->cohesiveDirectionVars;
          for (int j=0; j< numCoh; j++)
          {
            if (fscanf(fp, "%lf",&(cohCoefs(j)))==1)
            {
              printf("%.16g ",cohCoefs(j));
            }
            else
            {
              Msg::Error("error E reading file in NetworkInteraction::loadCohesiveDataFromFile");
              fclose(fp);
              Msg::Exit(0);
            }
          }
          
          for (int j=0; j< numCoh; j++)
          {
            for (int k=0; k< numCohDirVars; k++)
            {
              if (fscanf(fp, "%lf",&(cohDirVars(j,k)))==1)
              {
                printf("%.16g ",cohDirVars(j,k));
              }
              else
              {
                Msg::Error("error E reading file in NetworkInteraction::loadCohesiveDataFromFile");
                fclose(fp);
                Msg::Exit(0);
              }
            }
          }
          char what[256];
          if (fscanf(fp, "%s", what)==1)
          {
            printf("activation function=%s \n", what);
            node->cohesiveAf = activationFunction::createActivationFunction(what);
          }
          else
          {
            Msg::Error("error reading file in NetworkInteraction::loadCohesiveDataFromFile");
            fclose(fp);
            Msg::Exit(0);
          }
        }
      }
      break;
    }
    fclose(fp);
    //
    printf("done loading cohesive data from file %s\n",filename.c_str());
  }
  else
  {
    Msg::Error("File %s does not exist",filename.c_str());
    Msg::Exit(0);
  }
};

void NetworkInteraction::loadDataFromFile(std::string filename)
{
  clearData();
  printf("loading data from file %s\n",filename.c_str());
  //
  FILE* fp = fopen(filename.c_str(),"r");
  if (fp !=NULL)
  {
    printf("start reading file: %s\n",filename.c_str());
    while (1)
    {
      if(feof(fp))
        break;
        
      int numberMNodes= 0;
      if( fscanf(fp, "%d",&numberMNodes)==1)
      {
        printf("number of material nodes = %d\n",numberMNodes);
      }
      else
      {
        Msg::Error("error A reading file in NetworkInteraction::loadDataFromFile");
        fclose(fp);
        Msg::Exit(0);
      }
      for (int in=0; in < numberMNodes; in++)
      {
        MaterialNode* node = new MaterialNode();
        char what[256];
        if (fscanf(fp, "%d %d %lf %s", &(node->numberIndex),&(node->matIndex), &(node->weight), what)==4)
        {
          printf("--------id=%d, mat index = %d, weight=%f, what=%s \n", node->numberIndex,node->matIndex,node->weight,what);
          node->af = activationFunction::createActivationFunction(what);
          _allMaterialNodes[node->numberIndex] = node;
        }
        else
        {
          Msg::Error("error B reading file in NetworkInteraction::loadDataFromFile");
          fclose(fp);
          Msg::Exit(0);
        }
      }
      
      // interaction
      int numberInteraction= 0;
      if( fscanf(fp, "%d",&numberInteraction)==1)
      {
        printf("number of interaction = %d\n",numberInteraction);
      }
      else
      {
        Msg::Error("error C reading file in NetworkInteraction::loadDataFromFile");
        fclose(fp);
        Msg::Exit(0);
      }
      for (int in=0; in < numberInteraction; in++)
      {
        InteractionMechanism* im = new InteractionMechanism();
        if (fscanf(fp, "%d",&(im->getRefToIndex())) == 1)
        {
          printf("index = %d, ",im->getIndex());
        }
        else
        {
          Msg::Error("error D reading file in NetworkInteraction::loadDataFromFile");
          fclose(fp);
          Msg::Exit(0);
        }
        int numberDirVars = 0;
        if (fscanf(fp, "%d",&numberDirVars) == 1)
        {
          printf("number of dir vars =%d: ",numberDirVars);
          std::vector<double>& directionVars = im->getRefToDirectionVars();
          directionVars.resize(numberDirVars);
          for (int j=0; j< numberDirVars; j++)
          {
            if (fscanf(fp, "%lf",&(directionVars[j]))==1)
            {
              printf("%.16g",directionVars[j]);
            }
            else
            {
              Msg::Error("error E reading file in NetworkInteraction::loadDataFromFile");
              fclose(fp);
              Msg::Exit(0);
            }
          }
        }
        else
        {
          Msg::Error("error F reading file in NetworkInteraction::loadDataFromFile");
          fclose(fp);
          Msg::Exit(0);
        }
        int numberNodes = 0;
        if (fscanf(fp, "%d", &numberNodes)==1)
        {
          printf(" numNodes=%d\n", numberNodes);
          std::map<int,double>& coefficients = im->getRefToCoefficients();
          for (int jj=0; jj< numberNodes; jj++)
          {
            int nodeIndex;
            double alpha;
            if (fscanf(fp, "%d %lf",&nodeIndex,&alpha) == 2)
            {
              MaterialNode* mnode = _allMaterialNodes[nodeIndex];
              coefficients[nodeIndex] = alpha;
              _allInteractions[im->getIndex()] = im;
              _nodeInteractionMap[mnode].push_back(im);
            }
            else
            {
              Msg::Error("error G reading file in NetworkInteraction::loadDataFromFile");
              fclose(fp);
              Msg::Exit(0);
            }
          }
        }
        else
        {
          Msg::Error("error H reading file in NetworkInteraction::loadDataFromFile");
          fclose(fp);
          Msg::Exit(0);
        }
      }
      break;
    }
    fclose(fp);
    //
    printf("done loading data from file %s\n",filename.c_str());
  }
  else
  {
    Msg::Error("File %s does not exist",filename.c_str());
    Msg::Exit(0);
  }
  bool ok = checkValidity();
  if (!ok)
  {
    Msg::Exit(0);
  }
};

void NetworkInteraction::convertToLinearActivation(std::string outputFileName)
{
  for (MaterialNodeContainer::iterator it =_allMaterialNodes.begin(); it!= _allMaterialNodes.end(); it++)
  {
    MaterialNode* node = it->second;
    double Wreal = node->af->getVal(node->weight);
    delete node->af;
    node->af = activationFunction::createActivationFunction("linear");
    node->weight = Wreal;
  };
  saveDataToFile(outputFileName);
}

void NetworkInteraction::createTreeBasedFullInteraction(int Nlayers, int nodePerPhaseFullInteraction, int numFullInteraction,
                                         int dim, const fullVector<double>& phaseFraction, 
                                         const std::string affunc, bool random, bool triple)
{
  // random weight for leaves
  unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
  std::default_random_engine generator(seed);
  std::uniform_real_distribution<double> distribution(0., 1.); 
  
  auto getVal = [&](double min, double max)
  {
    double  x= distribution(generator);
    return (max-min)*x + min;
  };
  //
  //
  int numLeaves = 1;
  for (int j=0; j< Nlayers-1; j++)
  {
    numLeaves *= 2;
  }
  int numPhases = phaseFraction.size();
  int numNodesFullInterfaction, totalNumberNodes;
  
  if (triple)
  {
    numNodesFullInterfaction = numPhases-1+nodePerPhaseFullInteraction;
    totalNumberNodes = numLeaves*numNodesFullInterfaction;
  }
  else
  {
    numNodesFullInterfaction = numPhases*nodePerPhaseFullInteraction;
    totalNumberNodes = numLeaves*numPhases*nodePerPhaseFullInteraction;
  }
  
  Tree tree;
  tree.createPerfectTree(Nlayers,2,numNodesFullInterfaction,dim-1,affunc.c_str(),1);
  
  std::vector<TreeNode*> allLeaves;
  tree.getRefToAllLeaves(allLeaves);
  for (int j=0; j< allLeaves.size(); j++)
  {
    TreeNode* treeNode = allLeaves[j];
    if (triple)
    {
      if (treeNode->matIndex >= numPhases)
      {
        treeNode->matIndex = 0;
      }
    }
    else
    {
      while (treeNode->matIndex >= numPhases)
      {
        treeNode->matIndex -= numPhases;
      }      
    }
  }
  tree.printTreeInteraction("treeView.txt",true);
  
  // give all weigts
  // create N material node
  double minWeight = 0.1;
  double maxWeight = 1.;
  std::vector<double> randomPositives(totalNumberNodes,0.);
  for (int i=0; i< totalNumberNodes; i++)
  {
    if (random)
    {
      randomPositives[i] = getVal(minWeight,maxWeight);
    }
    else
    {
      randomPositives[i]  = 0.5;
    }
  };
  std::vector<double> sumPhaseWeights(numPhases,0.);
  for (int i = 0; i < allLeaves.size(); i++)
  {
    sumPhaseWeights[allLeaves[i]->matIndex] += randomPositives[i];
  };
  
  std::map<const TreeNode*, MaterialNode*> allocatedMaterialNodes;  
  for (int i = 0; i < allLeaves.size(); i++)
  {
    TreeNode* node = allLeaves[i];
    int iphase = node->matIndex;
    node->weight = node->af->getReciprocalVal(phaseFraction(iphase)*randomPositives[i]/sumPhaseWeights[iphase]);
    //
    // create material node
    MaterialNode* newnode = new MaterialNode();
    newnode->af = activationFunction::createActivationFunction(affunc.c_str());
    newnode->weight = node->weight;
    newnode->matIndex = iphase;
    _allMaterialNodes[newnode->numberIndex] = newnode;
    allocatedMaterialNodes[node] = newnode;
  };
  
  auto generateRandomCoeffsAndNormals = [&](const TreeNode& treeNodeInter, InteractionMechanism& interac)
  {
    Tree::nodeContainer allLeaves;
    tree.getAssociatedLeavesForNode(&treeNodeInter,allLeaves);
    std::map<int,double>& allAlpha = interac.getRefToCoefficients();
    int numberMaterialNodes = allLeaves.size();
    std::vector<double> allValBeta(numberMaterialNodes-1,0.);
    double sumallVa = 0;
    for (int j=0; j< numberMaterialNodes-1; j++)
    {
      allValBeta[j] = getVal(-1.,1.);
      sumallVa += allValBeta[j];
    }
    //int randMaterialNode = rand() % numberMaterialNodes;
    int randMaterialNode = 0;
    const TreeNode* child0 = allLeaves[randMaterialNode];
    MaterialNode* matNode0 = allocatedMaterialNodes[child0];
    double alpha0 = getVal(0.1,1.);
    double W0  = getWeight(matNode0);
    allAlpha[matNode0->getNodeId()] = alpha0;
    for (int j=0; j<numberMaterialNodes; j++)
    {
      const TreeNode* childj = allLeaves[j];
      MaterialNode* matNodej = allocatedMaterialNodes[childj];
      double Wj = getWeight(matNodej);
      if ( j < randMaterialNode )
      {
        allAlpha[matNodej->getNodeId()] = -(W0*alpha0*allValBeta[j])/(Wj*sumallVa);
      }
      else if ( j > randMaterialNode)
      {
        allAlpha[matNodej->getNodeId()] = -(W0*alpha0*allValBeta[j-1])/(Wj*sumallVa);
      }
    }
    std::vector<double>& directionVars = interac.getRefToDirectionVars();
    if (dim ==2)
    {
      directionVars.resize(1);
      directionVars[0] = getVal(0.,1.);
    }
    else if (dim == 3)
    {
      directionVars.resize(2);
      directionVars[0] = getVal(0.,1.);
      directionVars[1] = getVal(0.,1.);
    }
    else
    {
      Msg::Error("dim = %d is not correctly defined",dim);
      Msg::Exit(0);
    }
  };
  
  
  std::vector<int> allNodeIds;
  getAllMaterialNodeIds(allNodeIds);  
  Tree::nodeContainer regularNodes;
  tree.getAllRegularNodes(regularNodes);
  
  std::vector<int> fullInteractions, twoGroupInteractions;
  for (int l=0; l< regularNodes.size(); l++)
  {
    const TreeNode* treeNode = regularNodes[l];
    if (treeNode->childs.size() > 2)
    {
      for (int k=0; k <numFullInteraction; k++)
      {
        InteractionMechanism* interaction = new InteractionMechanism();
        fullInteractions.push_back(interaction->getIndex());
        generateRandomCoeffsAndNormals(*treeNode,*interaction);
        for (int j=0; j<treeNode->childs.size(); j++)
        { 
          const TreeNode* childj = treeNode->childs[j];
          MaterialNode* matNodej = allocatedMaterialNodes[childj];
          _nodeInteractionMap[matNodej].push_back(interaction);
        }
        // add to interaction set
        _allInteractions[interaction->getIndex()] = interaction;
      }
    }
    else
    {
      InteractionMechanism* interaction = new InteractionMechanism();
      twoGroupInteractions.push_back(interaction->getIndex());
      // all value of interaction coefficients
      if (random)
      {
        generateRandomCoeffsAndNormals(*treeNode,*interaction);
        Tree::nodeContainer allLeaves;
        tree.getAssociatedLeavesForNode(treeNode,allLeaves);
        for (int j=0; j<allLeaves.size(); j++)
        { 
          const TreeNode* childj = allLeaves[j];
          MaterialNode* matNodej = allocatedMaterialNodes[childj];
          _nodeInteractionMap[matNodej].push_back(interaction);
        }
        // add to interaction set
        _allInteractions[interaction->getIndex()] = interaction;
      }
      else
      {
        std::map<int,double>& allAlpha = interaction->getRefToCoefficients();
        TreeNode* left = treeNode->childs[0];
        TreeNode* right = treeNode->childs[1];
        Tree::nodeContainer leftLeaves, rightLeaves;
        tree.getAssociatedLeavesForNode(left,leftLeaves);
        tree.getAssociatedLeavesForNode(right,rightLeaves);
        //
        double Wleft = tree.getNonNegativeWeight(left);
        double Wright = tree.getNonNegativeWeight(right);
        for (int j=0; j< leftLeaves.size(); j++)
        {
          MaterialNode* matNode = allocatedMaterialNodes[leftLeaves[j]];
          allAlpha.insert(std::pair<int,double>(matNode->numberIndex,Wright/(Wleft+Wright)));
          _nodeInteractionMap[matNode].push_back(interaction);
        }
        for (int j=0; j< rightLeaves.size(); j++)
        {
          MaterialNode* matNode = allocatedMaterialNodes[rightLeaves[j]];
          allAlpha.insert(std::pair<int,double>(matNode->numberIndex,-Wleft/(Wleft+Wright)));
          _nodeInteractionMap[matNode].push_back(interaction);
        }
        // normal
        std::vector<double>& directionVars = interaction->getRefToDirectionVars();
        if (dim ==2)
        {
          directionVars.resize(1);
          directionVars[0] = getVal(0.,1.);
        }
        else if (dim == 3)
        {
          directionVars.resize(2);
          directionVars[0] = getVal(0.,1.);
          directionVars[1] = getVal(0.,1.);
        }
        else
        {
          Msg::Error("dim = %d is not correctly defined",dim);
          Msg::Exit(0);
        }
        // add to interaction set
        _allInteractions[interaction->getIndex()] = interaction;
      }
    };
  };
  
  printf("full interaction:[");
  for (int i=0; i< fullInteractions.size(); i++)
  {
    if (i>0)
    {
      printf(",");
    }
    printf("%d",fullInteractions[i]); 
  }
  printf("]\n");
  printf("two-group interaction:[");
  for (int i=0; i< twoGroupInteractions.size(); i++)
  {
    if (i>0)
    {
      printf(",");
    }
    printf("%d",twoGroupInteractions[i]); 
  }
  printf("]\n");
  
  FILE* file = fopen("interactionInfos.txt","w");
  fprintf(file,"full interaction:[");
  for (int i=0; i< fullInteractions.size(); i++)
  {
    if (i>0)
    {
      fprintf(file,",");
    }
    fprintf(file,"%d",fullInteractions[i]); 
  }
  fprintf(file,"]\n");
  fprintf(file,"two-group interaction:[");
  for (int i=0; i< twoGroupInteractions.size(); i++)
  {
    if (i>0)
    {
      fprintf(file,",");
    }
    fprintf(file,"%d",twoGroupInteractions[i]); 
  }
  fprintf(file,"]\n");
  fclose(file);
  
  // save to file
  //saveDataToFile("DMNFromTree.txt");
  bool ok = checkValidity();
  if (!ok)
  {
    Msg::Exit(0);
  }
};

void NetworkInteraction::initializeRandomFullInteraction(int numberMaterialNodes, int numberInteractions, int dim, 
                                                        const fullVector<double>& phaseFraction, const std::string affunc,
                                                        bool random)
{
  // random weight for leaves
  unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
  std::default_random_engine generator(seed);
  std::uniform_real_distribution<double> distribution(0., 1.); 
  
  auto getVal = [&](double min, double max)
  {
    double  x= distribution(generator);
    return (max-min)*x + min;
  };
  
  // create N material node
  double minWeight = 0.1;
  double maxWeight = 1.;
  std::vector<double> randomPositives(numberMaterialNodes,0.);
  for (int i=0; i< numberMaterialNodes; i++)
  {
    if (random)
    {
      randomPositives[i] = getVal(minWeight,maxWeight);
    }
    else
    {
      randomPositives[i]  = 0.5;
    }
  };
  int numPhases = phaseFraction.size();
  std::vector<double> sumPhaseWeights(numPhases,0.);
  for (int iphase = 0; iphase < numPhases; iphase++)
  {
    int start = numberMaterialNodes*iphase/numPhases;
    int end = numberMaterialNodes*(iphase+1)/numPhases;
    sumPhaseWeights[iphase] = 0;
    for (int j=start; j< end; j++)
    {
      sumPhaseWeights[iphase] += randomPositives[j];
    }
  };
  
  for (int iphase = 0; iphase < numPhases; iphase++)
  {
    int start = numberMaterialNodes*iphase/numPhases;
    int end = numberMaterialNodes*(iphase+1)/numPhases;
    Msg::Info("create material node from index %d to index %d",start,end-1);
    for (int i=start; i< end; i++)
    {
      MaterialNode* node = new MaterialNode();
      node->af = activationFunction::createActivationFunction(affunc.c_str());
      node->weight = node->af->getReciprocalVal(phaseFraction(iphase)*randomPositives[i]/sumPhaseWeights[iphase]);
      node->matIndex = iphase;
      _allMaterialNodes[node->numberIndex] = node;
      Msg::Info("create node %d, mat index = %d",node->getNodeId(),node->matIndex);
      //
    };
  };
  
  //
  auto getSumWeights = [&](const std::vector<int>& nodeIdList)
  {
    double val = 0.;
    for (int jj=0; jj < nodeIdList.size(); jj++)
    {
      const MaterialNode* node = getMaterialNode(nodeIdList[jj]);
      val += getWeight(node);
    }
    return val;
  };
  
  // obtain a time-based seed:
  auto devideByTwo = [](const std::vector<int>& foo, std::vector<int>& left, std::vector<int>& right)
  {
    unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
    left = foo;
    right.clear();
    while (right.size()*2 < foo.size() && (!left.empty()))
    {
      std::shuffle (left.begin(), left.end(), std::default_random_engine(seed));
      right.push_back(left.back());
      left.pop_back();
    }
  };
  
  
  // build interaction
  double minAlpha = -1;
  double maxAlpha = 1.;
  std::vector<int> allNodeIds;
  getAllMaterialNodeIds(allNodeIds);
  for (int i=0; i< numberInteractions; i++)
  {
    InteractionMechanism* im = new InteractionMechanism();
    std::vector<double>& directionVars = im->getRefToDirectionVars();
    std::map<int,double>& coefficients = im->getRefToCoefficients();
    if (dim ==2)
    {
      directionVars.resize(1);
      directionVars[0] = getVal(0.,1.);
    }
    else if (dim == 3)
    {
      directionVars.resize(2);
      directionVars[0] = getVal(0.,1.);
      directionVars[1] = getVal(0.,1.);
    }
    else
    {
      Msg::Error("dim = %d is not correctly defined",dim);
      Msg::Exit(0);
    }
    
    /*
    std::vector<int> left, right;
    devideByTwo(allNodeIds,left,right);
    double Wleft = getSumWeights(left);
    double Wright = getSumWeights(right);
    coefficients.clear();
    for (int jj=0; jj< left.size(); jj++)
    {
      coefficients[left[jj]] = Wright/(Wleft+Wright);
    }
    for (int jj=0; jj< right.size(); jj++)
    {
      coefficients[right[jj]] = -Wleft/(Wleft+Wright);
    }
    */
    
    std::vector<double> allValBeta(numberMaterialNodes-1,0.);
    double sumallVa = 0;
    for (int j=0; j< numberMaterialNodes-1; j++)
    {
      allValBeta[j] = getVal(minAlpha,maxAlpha);
      sumallVa += allValBeta[j];
    }
    
    int randMaterialNode = rand() % numberMaterialNodes;
    double alpha0 = getVal(0.1,1.);
    double W0  = getWeight(getMaterialNode(allNodeIds[randMaterialNode]));
    coefficients[allNodeIds[randMaterialNode]] = alpha0;
    for (int j=0; j<numberMaterialNodes; j++)
    {
      double Wj = getWeight(getMaterialNode(allNodeIds[j]));
      if ( j < randMaterialNode )
      {
        coefficients[allNodeIds[j]] = -(W0*alpha0*allValBeta[j])/(Wj*sumallVa);
      }
      else if ( j > randMaterialNode)
      {
        coefficients[allNodeIds[j]] = -(W0*alpha0*allValBeta[j-1])/(Wj*sumallVa);
      }
    }
    
    //
    _allInteractions[im->getIndex()] = im;
    for (MaterialNodeContainer::const_iterator it=_allMaterialNodes.begin(); it != _allMaterialNodes.end(); it++)
    {
      _nodeInteractionMap[it->second].push_back(im);
    }
  }
  bool ok = checkValidity();
  if (!ok)
  {
    Msg::Exit(0);
  }
};

void NetworkInteraction::initializeRandomPointwiseInteraction(int numberMaterialNodes, int numPhases, int dim, bool rand,std::string affunc)
{
  // random weight for leaves
  unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
  std::default_random_engine generator(seed);
  std::uniform_real_distribution<double> distribution(0., 1.); 
  
  auto getVal = [&](double min, double max)
  {
    double x = 0.5;
    if (rand)
    {
      x= distribution(generator);
    }
    return (max-min)*x + min;
  };
  
  // create N material node
  double minWeight = 0.1;
  double maxWeight = 1.;
   
  std::vector< std::vector<MaterialNode*> >allNodes(numPhases,std::vector<MaterialNode*>());
  for (int i=0; i< numberMaterialNodes; i++)
  {
    MaterialNode* node = new MaterialNode();
    node->af = activationFunction::createActivationFunction(affunc.c_str());
    node->weight = getVal(minWeight,maxWeight);
    if (getVal(0.,1.) > 0.5)
    { 
      node->matIndex = 1;
    }
    else
    {
      node->matIndex = 0;
    }
    _allMaterialNodes[node->numberIndex] = node;
    allNodes[node->matIndex].push_back(node);
  };
  
  // build interaction
  for (int i=0; i< allNodes[0].size(); i++)
  {
    MaterialNode* nodeI = allNodes[0][i];
    std::vector<MaterialNode*> allNodesInteract;
    for (int j=i+1; j< allNodes[0].size(); j++)
    {
      allNodesInteract.push_back(allNodes[0][j]);
    }
    for (int j=1; j< numPhases; j++)
    {
      allNodesInteract.insert(allNodesInteract.end(),allNodes[j].begin(), allNodes[j].end());
    }
    for (int j=0; j< allNodesInteract.size(); j++)
    {
      MaterialNode* nodeJ = allNodesInteract[j];
      InteractionMechanism* im = new InteractionMechanism();
      std::vector<double>& directionVars = im->getRefToDirectionVars();
      std::map<int,double>& coefficients = im->getRefToCoefficients();
      if (dim ==2)
      {
        directionVars.resize(1);
        directionVars[0] = getVal(0.,1.);
      }
      else if (dim == 3)
      {
        directionVars.resize(2);
        directionVars[0] = getVal(0.,1.);
        directionVars[1] = getVal(0.,1.);
      }
      else
      {
        Msg::Error("dim = %d is not correctly defined",dim);
        Msg::Exit(0);
      }
      
      double WI = getWeight(nodeI);
      double WJ = getWeight(nodeJ);
      
      coefficients[nodeI->numberIndex] = WJ/(WI+WJ);
      coefficients[nodeJ->numberIndex] = -WI/(WI+WJ);
      
      _allInteractions[im->getIndex()] = im;
      _nodeInteractionMap[nodeI].push_back(im);
      _nodeInteractionMap[nodeJ].push_back(im);
    };
  };
  bool ok = checkValidity();
  if (!ok)
  {
    Msg::Exit(0);
  }
};

bool NetworkInteraction::reInitialize()
{
  Msg::Info("interaction needs to be initialized !!!");
  
  
  return false;
};

void NetworkInteraction::normalizeWeights(std::string newaffunc)
{
  Msg::Info("start normalizing all the weights");
  double WT = getTotalWeight();
  for (MaterialNodeContainer::iterator it =_allMaterialNodes.begin(); it!= _allMaterialNodes.end(); it++)
  {
    MaterialNode* node = it->second;
    double Wi = getWeight(node);
    if (node->af)
    {
      delete node->af;
    }
    node->af = activationFunction::createActivationFunction(newaffunc.c_str());
    node->weight = node->af->getReciprocalVal(Wi/WT);
  };
  Msg::Info("done normalizing all the weights");
};

void NetworkInteraction::standardizeInteractionDirections()
{
  Msg::Info("start stardardizing all the interaction directions");
  for (InteractionContainer::const_iterator it = _allInteractions.begin(); it != _allInteractions.end(); it++)
  {
    InteractionMechanism* im = it->second;
    im->standardizeDirectionVars();
  };
  Msg::Info("done stardardizing all the interaction directions");
};

void NetworkInteraction::normalizeAllInteractionCoefficients()
{
  Msg::Info("start normalizing all the interactions");
  for (InteractionContainer::const_iterator it = _allInteractions.begin(); it != _allInteractions.end(); it++)
  {
    InteractionMechanism* im = it->second;
    std::vector<int> allNodeIds;
    im->getAllMaterialNodeIds(allNodeIds);
    int numNodes = allNodeIds.size();
    std::vector<double> allBeta(numNodes-1,0);
    double W0 = getWeight(getMaterialNode(allNodeIds[0]));
    double alpha0 = im->getCoefficientForNode(allNodeIds[0]);
    for (int i=0; i< numNodes-1; i++)
    {
      double Wip1 = getWeight(getMaterialNode(allNodeIds[i+1]));
      double alphaip1 = im->getCoefficientForNode(allNodeIds[i+1]);
      allBeta[i] = - Wip1*alphaip1/alpha0/W0;
    }
    
    std::map<int,double>& coefficients = im->getRefToCoefficients();
    coefficients[allNodeIds[0]] = 1.; // alpha 0
    for (int i=0; i< numNodes-1; i++)
    {
      double Wip1 = getWeight(getMaterialNode(allNodeIds[i+1]));
      coefficients[allNodeIds[i+1]] =  - W0*allBeta[i]/Wip1;
    }
    im->standardizeDirectionVars();
  }
  Msg::Info("done normalizing all the interactions");
};

bool NetworkInteraction::checkValidity(bool message) const
{
  bool oneFailed = false;
  if (message)
    Msg::Info("start checking the validatity of all the interactions");
  for (InteractionContainer::const_iterator it = _allInteractions.begin(); it != _allInteractions.end(); it++)
  {
    const InteractionMechanism* im = it->second;
    double Walpha = 0;
    double maxVal = 0;
    double minVal = 1e10;
    //im->printData();
    for (std::map<int, double>::const_iterator itCoe = im->getCoefficients().begin(); itCoe != im->getCoefficients().end(); itCoe++)
    {
      //Msg::Info("material node %d",itCoe->first);
      const MaterialNode* mnode = getMaterialNode(itCoe->first);
      double alpha = itCoe->second;
      double weight = getWeight(mnode);
      Walpha += (weight*alpha);
      if (fabs(weight*alpha) > maxVal)
      {
        maxVal = fabs(weight*alpha);
      }
      if (fabs(weight*alpha) < fabs(minVal))
      {
        minVal =  weight*alpha;
      }
    }
    if (message)
      Msg::Info("interaction %d: sum-Walpha = %.16g, maxVal = %.16g minVal=%.16g",im->getIndex(), Walpha,maxVal, minVal);
    if (fabs(Walpha) > 1e-8*maxVal)
    {
      Msg::Error("interaction %d is not valid",im->getIndex());
      im->printData();
      oneFailed = true;
    }
  }
  
  if (oneFailed)
  {
    Msg::Exit(0);
    return false;
  }
  else
  {  
    if (message)
      Msg::Info("all interactions are valid");
    return true;
  }
};

double NetworkInteraction::getPerPhase(int phaseIndex, int comp, int eleVal) const
{
  bool first = true;
  double val= 0.;
  double Wtotal = 0.;
  for (MaterialNodeContainer::const_iterator it =_allMaterialNodes.begin(); it!= _allMaterialNodes.end(); it++)
  {
    const MaterialNode* node = it->second;
    if (node->matIndex == phaseIndex)
    {
      double Wi = getWeight(node);
      if (node->ips != NULL)
      {
        const IPVariable* ipv = node->ips->getState(IPStateBase::current);
        double ss = ipv->get(comp);
        if (eleVal == nlsField::mean)
        {
          val += ss*Wi;
        }
        else if (eleVal == nlsField::max)
        {
          if (first)
          {
            val = ss;
          }
          else
          {
            if (ss > val)
            {
              val = ss;
            }
          }
        }
        else if (eleVal == nlsField::min)
        {
          if (first)
          {
            val = ss;
          }
          else
          {
            if (ss < val)
            {
              val = ss;
            }
          }
        }
        else
        {
          Msg::Error("element val %d is not defined",eleVal);
          return 0;
        }
      }
      Wtotal += Wi;
      first = false;
    }
  }
  if (eleVal == nlsField::mean)
  {
    val *= (1./Wtotal);
  }
  return val;
};


double NetworkInteraction::get(int comp, int eleVal) const
{
  bool first = true;
  double val= 0.;
  double Wtotal = 0.;
  for (MaterialNodeContainer::const_iterator it =_allMaterialNodes.begin(); it!= _allMaterialNodes.end(); it++)
  {
    const MaterialNode* node = it->second;
    double Wi = getWeight(node);
    if (node->ips != NULL)
    {
      const IPVariable* ipv = node->ips->getState(IPStateBase::current);
      double ss = ipv->get(comp);
      if (eleVal == nlsField::mean)
      {
        val += ss*Wi;
      }
      else if (eleVal == nlsField::max)
      {
        if (first)
        {
          val = ss;
        }
        else
        {
          if (ss > val)
          {
            val = ss;
          }
        }
      }
      else if (eleVal == nlsField::min)
      {
        if (first)
        {
          val = ss;
        }
        else
        {
          if (ss < val)
          {
            val = ss;
          }
        }
      }
      else
      {
        Msg::Error("element val %d is not defined",eleVal);
        return 0;
      }
    }
    Wtotal += Wi;
    first = false;
  }
  if (eleVal == nlsField::mean)
  {
    val *= (1./Wtotal);
  }
  return val;
};

void NetworkInteraction::nextStep()
{
  for (MaterialNodeContainer::iterator it = _allMaterialNodes.begin(); it != _allMaterialNodes.end(); it++)
  {
    it->second->nextStep();
  };
  for (InteractionContainer::iterator it = _allInteractions.begin(); it != _allInteractions.end(); it++)
  {
    it->second->nextStep();
  }
};

void NetworkInteraction::resetToPreviousStep()
{
  for (MaterialNodeContainer::iterator it = _allMaterialNodes.begin(); it != _allMaterialNodes.end(); it++)
  {
    it->second->resetToPreviousStep();
  };
  for (InteractionContainer::iterator it = _allInteractions.begin(); it != _allInteractions.end(); it++)
  {
    it->second->resetToPreviousStep();
  }
};

void NetworkInteraction::copy(const IPStateBase::whichState ws1, const IPStateBase::whichState ws2)
{
  for (MaterialNodeContainer::iterator it = _allMaterialNodes.begin(); it != _allMaterialNodes.end(); it++)
  {
    it->second->copy(ws1,ws2);
  };
  for (InteractionContainer::iterator it = _allInteractions.begin(); it != _allInteractions.end(); it++)
  {
    it->second->copy(ws1,ws2);
  }
};

void NetworkInteraction::resetIncompatibleDispVector()
{
  for (InteractionContainer::iterator it = _allInteractions.begin(); it != _allInteractions.end(); it++)
  {
    it->second->resetIncompatibleDispVector();
  }
  for (MaterialNodeContainer::iterator it = _allMaterialNodes.begin(); it != _allMaterialNodes.end(); it++)
  {
    it->second->resetCohesiveJumps();
  };
};

void NetworkInteraction::getInteractionFromTree(const Tree& T, bool standardize)
{
  clearData();
  //
  // get all leaves
  Tree::nodeContainer allLeaves;
  T.getAllLeaves(allLeaves);
  
  std::set<const TreeNode*> smallNodes;
  double WT = T.getNonNegativeWeight(T.getRootNode());
  double tol = 1e-16;
  
  std::map<const TreeNode*, MaterialNode*> allocatedMaterialNodes;
  for (int i=0; i< allLeaves.size(); i++)
  {
    const TreeNode* leaf = allLeaves[i];
    MaterialNode* node = new MaterialNode();
    // copy necessary data
    node->af = leaf->af->clone();
    node->weight = leaf->weight;
    node->matIndex = leaf->matIndex;
    node->withCohesiveFlag = leaf->withCohesiveFlag;
    if (!standardize)
    {
      node->numberIndex = leaf->getNodeId();
    }
    if (node->withCohesiveFlag)
    {
      int numCoh = leaf->cohesiveCoeffVars.size();
      int numDirVars = leaf->cohesiveDirectionVars.size2();
      node->cohesiveMatIndex = leaf->cohesiveMatIndex;
      node->allocateCohesiveParameters(numCoh,numDirVars);
      node->cohesiveCoeffVars = leaf->cohesiveCoeffVars;
      node->cohesiveDirectionVars = leaf->cohesiveDirectionVars;
      if (standardize)
      {
        node->standardizeCohesiveDirectionVars();
      }
      //
      if (node->cohesiveAf) delete node->cohesiveAf;
      node->cohesiveAf = leaf->cohesiveAf->clone();
    };
    //
    _allMaterialNodes[node->numberIndex] = node;
    allocatedMaterialNodes[leaf] = node;
    if (getWeight(node) < tol*WT)
    {
      smallNodes.insert(leaf);
    };
  };
  
  // each regular node of the tree corresponds to multiple interaction mechanisms 
  Tree::nodeContainer regularNodes;
  T.getAllRegularNodes(regularNodes);
  for (int l=0; l< regularNodes.size(); l++)
  {
    const TreeNode* treeNode = regularNodes[l];
    int nbInteractionMechanisms= treeNode->childs.size()-1;
    for (int i=0; i< nbInteractionMechanisms; i++)
    {
      // add one interaction
      InteractionMechanism* interaction = new InteractionMechanism();
      // all value of interaction coefficients
      std::map<int,double>& allAlpha = interaction->getRefToCoefficients();
      TreeNode* left = treeNode->childs[i];
      TreeNode* right = treeNode->childs[i+1];
      
      Tree::nodeContainer leftLeaves, rightLeaves;
      T.getAssociatedLeavesForNode(left,leftLeaves);
      T.getAssociatedLeavesForNode(right,rightLeaves);
      //
      double Wleft = T.getNonNegativeWeight(left);
      double Wright = T.getNonNegativeWeight(right);
      
      for (int j=0; j< leftLeaves.size(); j++)
      {
        if (smallNodes.find(leftLeaves[j]) != smallNodes.end())
        {
          Wleft -= getWeight(allocatedMaterialNodes[leftLeaves[j]]);
        }
      }
      for (int j=0; j< rightLeaves.size(); j++)
      {
        if (smallNodes.find(rightLeaves[j]) != smallNodes.end())
        {
          Wright -= getWeight(allocatedMaterialNodes[rightLeaves[j]]);
        }
      }
      
      if (Wleft < 1e-16 || Wright < 1e-16)
      {
        for (int j=0; j< leftLeaves.size(); j++)
        {
          MaterialNode* matNode = allocatedMaterialNodes[leftLeaves[j]];
          //allAlpha.insert(std::pair<int,double>(matNode->numberIndex,Wright/(Wleft+Wright)));
          allAlpha.insert(std::pair<int,double>(matNode->numberIndex,0.));
          _nodeInteractionMap[matNode].push_back(interaction);
        }
        for (int j=0; j< rightLeaves.size(); j++)
        {
          MaterialNode* matNode = allocatedMaterialNodes[rightLeaves[j]];
          //allAlpha.insert(std::pair<int,double>(matNode->numberIndex,-Wleft/(Wleft+Wright)));
          allAlpha.insert(std::pair<int,double>(matNode->numberIndex,0.));
          _nodeInteractionMap[matNode].push_back(interaction);
        }
      }
      else
      {
        for (int j=0; j< leftLeaves.size(); j++)
        {
          
          MaterialNode* matNode = allocatedMaterialNodes[leftLeaves[j]];
          if (smallNodes.find(leftLeaves[j]) == smallNodes.end())
          {
             //allAlpha.insert(std::pair<int,double>(matNode->numberIndex,Wright/(Wleft+Wright)));
            allAlpha.insert(std::pair<int,double>(matNode->numberIndex,1./(Wleft)));
          }
          else
          {
            allAlpha.insert(std::pair<int,double>(matNode->numberIndex,0.));
          }
          _nodeInteractionMap[matNode].push_back(interaction);
          
        }
        for (int j=0; j< rightLeaves.size(); j++)
        {
          
          MaterialNode* matNode = allocatedMaterialNodes[rightLeaves[j]];
          if (smallNodes.find(rightLeaves[j]) == smallNodes.end())
          {
            //allAlpha.insert(std::pair<int,double>(matNode->numberIndex,-Wleft/(Wleft+Wright)));
            allAlpha.insert(std::pair<int,double>(matNode->numberIndex,-1./Wright));
          }
          else
          {
            allAlpha.insert(std::pair<int,double>(matNode->numberIndex,0.));
          }
          _nodeInteractionMap[matNode].push_back(interaction);
        }
      }
      // normal
      int totalNumberDirVars =  treeNode->direction.size();
      int numberVarsPerNormal = totalNumberDirVars/nbInteractionMechanisms;
      
      std::vector<double>& directionVars = interaction->getRefToDirectionVars();
      directionVars.resize(numberVarsPerNormal);
      for (int j=0; j< numberVarsPerNormal; j++)
      {
        directionVars[j] = treeNode->direction[j];
      }
      //
      if (standardize)
      {
        interaction->standardizeDirectionVars();
      }
      // add to interaction set
      _allInteractions[interaction->getIndex()] = interaction;
    };
  };
  
  // save to file
  //saveDataToFile("DMNFromTree.txt");
  bool ok = checkValidity();
  if (!ok)
  {
    Msg::Exit(0);
  }
};



void NetworkInteraction::getDLinearTermDNormalVars(const InteractionMechanism* im, const InteractionMechanism* jm, fullMatrix<double>& DvecDNormalVars) const
{
  int nbNormalVars = jm->getDirectionVars().size();
  if (DvecDNormalVars.size1() != 3 || DvecDNormalVars.size2() != nbNormalVars)
  {
    DvecDNormalVars.resize(3,nbNormalVars,true);
  }
  else
  {
    DvecDNormalVars.setAll(0.);
  }
  
  if (jm->getIndex() == im->getIndex())
  {
    static SVector3 normal;
    static std::vector<SVector3> DnormalDVars;
    im->getDirection(normal,true,&DnormalDVars);
    
    //normal.print("normal");
    for (std::map<int,double>::const_iterator it = im->getCoefficients().begin(); it != im->getCoefficients().end(); it++)
    {
      const MaterialNode* node = getMaterialNode(it->first);
      if (node->ips != NULL)
      {
        double alphaij = it->second;
        double Wi = getWeight(node);
        //Msg::Info("alpha = %e, Wi = %e",alpha,Wi);
        const ipFiniteStrain* ipvf = dynamic_cast<const ipFiniteStrain*>(node->ips->getState(IPStateBase::current));
        if (ipvf != NULL)
        {
          const STensor3& Plocal = ipvf->getConstRefToFirstPiolaKirchhoffStress();
          const STensor43& DPlocalDFlocal = ipvf->getConstRefToTangentModuli();
          static STensor33 dFlocalDnormal;
          im->getDInducedStrainDDirection(it->first,dFlocalDnormal);
          for (int kk=0; kk< nbNormalVars; kk++)
          {
            for (int i=0; i< 3; i++)
            {
              for (int j=0; j<3; j++)
              {
                DvecDNormalVars(i,kk) += (Wi*alphaij*Plocal(i,j)*DnormalDVars[kk](j));
                for (int p=0; p<3; p++)
                {
                  for (int q=0; q<3; q++)
                  {
                    for (int r=0; r<3; r++)
                    {
                      DvecDNormalVars(i,kk)  += (Wi*alphaij*DPlocalDFlocal(i,j,p,q)*dFlocalDnormal(p,q,r)*DnormalDVars[kk](r)*normal(j));
                    }
                  }
                }
              }
            } 
          }
        }
        else
        {
          Msg::Error("ipFiniteStrain must be used in NetworkInteraction::getLinearTerm");
        }
      }
    }
  }
  else
  {
    static SVector3 normal;
    im->getDirection(normal);
    static SVector3 normalJ;
    static std::vector<SVector3> DnormalJDVars;
    jm->getDirection(normalJ,true,&DnormalJDVars);
    
    //normal.print("normal");
    for (std::map<int,double>::const_iterator it = im->getCoefficients().begin(); it != im->getCoefficients().end(); it++)
    {
      if (jm->inInteraction(it->first))
      {
        const MaterialNode* node = getMaterialNode(it->first);
        if (node->ips != NULL)
        {
          double alphaij = it->second;
          double Wi = getWeight(node);
          //Msg::Info("alpha = %e, Wi = %e",alpha,Wi);
          const ipFiniteStrain* ipvf = dynamic_cast<const ipFiniteStrain*>(node->ips->getState(IPStateBase::current));
          if (ipvf != NULL)
          {
            const STensor3& Plocal = ipvf->getConstRefToFirstPiolaKirchhoffStress();
            const STensor43& DPlocalDFlocal = ipvf->getConstRefToTangentModuli();
            static STensor33 dFlocalDnormalJ;
            jm->getDInducedStrainDDirection(it->first,dFlocalDnormalJ);
            for (int kk=0; kk< nbNormalVars; kk++)
            {
              for (int i=0; i< 3; i++)
              {
                for (int j=0; j<3; j++)
                {
                  for (int p=0; p<3; p++)
                  {
                    for (int q=0; q<3; q++)
                    {
                      for (int r=0; r<3; r++)
                      {
                        DvecDNormalVars(i,kk)  += (Wi*alphaij*DPlocalDFlocal(i,j,p,q)*dFlocalDnormalJ(p,q,r)*DnormalJDVars[kk](r)*normal(j));
                      }
                    }
                  }
                }
              } 
            }
          }
          else
          {
            Msg::Error("ipFiniteStrain must be used in NetworkInteraction::getLinearTerm");
          }
        }
      }
    }
  }
};

void NetworkInteraction::getDLinearTermDCoefficient(const InteractionMechanism* im, const InteractionMechanism* jm, fullMatrix<double>& DvecDcoeffs) const
{
  int nbCoeffJ = jm->getCoefficients().size();
  if (DvecDcoeffs.size1() != 3 || DvecDcoeffs.size2() != nbCoeffJ)
  {
    DvecDcoeffs.resize(3,nbCoeffJ,true);
  }
  else
  {
    DvecDcoeffs.setAll(0.);
  }
  
  static SVector3 normal;
  im->getDirection(normal);
  std::vector<int> allNodeJ;
  jm->getAllMaterialNodeIds(allNodeJ);
  if (jm->getIndex() == im->getIndex())
  {
    //normal.print("normal");
    for (int in = 0; in < allNodeJ.size(); in++)
    {
      const MaterialNode* node = getMaterialNode(allNodeJ[in]);
      if (node->ips != NULL)
      {
        double alphaij = im->getCoefficientForNode(allNodeJ[in]);
        double Wi = getWeight(node);
        //Msg::Info("alpha = %e, Wi = %e",alpha,Wi);
        const ipFiniteStrain* ipvf = dynamic_cast<const ipFiniteStrain*>(node->ips->getState(IPStateBase::current));
        if (ipvf != NULL)
        {
          const STensor3& Plocal = ipvf->getConstRefToFirstPiolaKirchhoffStress();
          const STensor43& DPlocalDFlocal = ipvf->getConstRefToTangentModuli();
          static STensor3 DFlocalDalpha;
          im->getDInducedStrainDCoefficient(allNodeJ[in],DFlocalDalpha);
          for (int i=0; i< 3; i++)
          {
            for (int j=0; j<3; j++)
            {
              DvecDcoeffs(i,in) += (Wi*Plocal(i,j)*normal(j));
              for (int p=0; p<3; p++)
              {
                for (int q=0; q<3; q++)
                {
                  DvecDcoeffs(i,in) += (Wi*alphaij*DPlocalDFlocal(i,j,p,q)*DFlocalDalpha(p,q)*normal(j));
                }
              }
            }
          }    
        }
        else
        {
          Msg::Error("ipFiniteStrain must be used in NetworkInteraction::getLinearTerm");
        }
      }
    }
  }
  else
  {
    //normal.print("normal");
    for (int jn = 0; jn < allNodeJ.size(); jn++)
    {
      if (im->inInteraction(allNodeJ[jn]))
      {
        const MaterialNode* node = getMaterialNode(allNodeJ[jn]);
        if (node->ips != NULL)
        {
          double alphaij = im->getCoefficientForNode(allNodeJ[jn]);
          double Wi = getWeight(node);
          //Msg::Info("alpha = %e, Wi = %e",alpha,Wi);
          const ipFiniteStrain* ipvf = dynamic_cast<const ipFiniteStrain*>(node->ips->getState(IPStateBase::current));
          if (ipvf != NULL)
          {
            const STensor43& DPlocalDFlocal = ipvf->getConstRefToTangentModuli();
            static STensor3 DFlocalDalpha;
            jm->getDInducedStrainDCoefficient(allNodeJ[jn],DFlocalDalpha);
            for (int i=0; i< 3; i++)
            {
              for (int j=0; j<3; j++)
              {
                for (int p=0; p<3; p++)
                {
                  for (int q=0; q<3; q++)
                  {
                    DvecDcoeffs(i,jn) += (Wi*alphaij*DPlocalDFlocal(i,j,p,q)*DFlocalDalpha(p,q)*normal(j));
                  }
                }
              }
            }    
          }
          else
          {
            Msg::Error("ipFiniteStrain must be used in NetworkInteraction::getLinearTerm");
          }
        }
      }
    }
  }
};

// with repsecto to material weights
void NetworkInteraction::getDLinearTermDWeights(const InteractionMechanism* im, std::vector<int>& materialIds, fullMatrix<double>& DvecDWeights) const
{
  int numMaterialNodes = im->getNumMaterialNodes();
  if (DvecDWeights.size1() != 3 || DvecDWeights.size2() != numMaterialNodes)
  {
    DvecDWeights.resize(3,numMaterialNodes,true);
  }
  else
  {
    DvecDWeights.setAll(0.);
  }
  im->getAllMaterialNodeIds(materialIds);

  static SVector3 normal;
  im->getDirection(normal);
  //normal.print("normal");
  for (int in=0; in< numMaterialNodes; in++)
  {
    const MaterialNode* node = getMaterialNode(materialIds[in]);
    if (node->ips != NULL)
    {
      double alphaij = im->getCoefficientForNode(materialIds[in]);
      //Msg::Info("alpha = %e, Wi = %e",alpha,Wi);
      const ipFiniteStrain* ipvf = dynamic_cast<const ipFiniteStrain*>(node->ips->getState(IPStateBase::current));
      if (ipvf != NULL)
      {
        const STensor3& Plocal = ipvf->getConstRefToFirstPiolaKirchhoffStress();
        for (int i=0; i< 3; i++)
        {
          for (int j=0; j<3; j++)
          {
            DvecDWeights(i,in) += (alphaij*Plocal(i,j)*normal(j));
          }
        }    
      }
      else
      {
        Msg::Error("ipFiniteStrain must be used in NetworkInteraction::getLinearTerm");
      }
    }
  }
};

void NetworkInteraction::computeLocalDeformationGradient(const STensor3& Fmacro)
{
  for (MaterialNodeContainer::iterator it = _allMaterialNodes.begin(); it != _allMaterialNodes.end(); it++)
  { 
    MaterialNode* node = it->second;
    //
    if (node->ips != NULL)
    {
      ipFiniteStrain* ipvf = dynamic_cast<ipFiniteStrain*>(node->ips->getState(IPStateBase::current));
      if (ipvf != NULL)
      {
        STensor3& F = ipvf->getRefToDeformationGradient();
        F = Fmacro;
        NodeMapContainer::const_iterator itFind = _nodeInteractionMap.find(node);
        if (itFind != _nodeInteractionMap.end())
        {
          const std::vector<const InteractionMechanism*>& allInteractions = itFind->second;
          int numInteractions = allInteractions.size();
          for (int i=0; i< numInteractions; i++)
          {
            static STensor3 Fi;
            allInteractions[i]->getInducedStrain(node->numberIndex,Fi);
            F += Fi;
          };
        }
        // cohesive contribution
        if (node->withCohesive())
        {
          int nCoh = node->cohesiveCoeffVars.size();
          static std::vector<STensor3> allFi;
          node->getInducedCohesiveStrain(allFi);
          for (int i=0; i< nCoh; i++)
          {
            F  -= allFi[i];
          }
        };
      };
    }
  };
};


void NetworkInteraction::getLinearTerm(const InteractionMechanism* im, fullVector<double>& vec) const
{
  static SVector3 normal;
  im->getDirection(normal);
  if (vec.size() != 3)
  {
    vec.resize(3,true);
  }
  else
  {
    vec.setAll(0.);
  }
  //normal.print("normal");
  for (std::map<int,double>::const_iterator it = im->getCoefficients().begin(); it != im->getCoefficients().end(); it++)
  {
    const MaterialNode* node = getMaterialNode(it->first);
    double alphaij = it->second;
    double Wi = getWeight(node);
    if (node->ips != NULL)
    {
      //Msg::Info("alpha = %e, Wi = %e",alpha,Wi);
      const ipFiniteStrain* ipvf = dynamic_cast<const ipFiniteStrain*>(node->ips->getState(IPStateBase::current));
      if (ipvf != NULL)
      {
        const STensor3& Plocal = ipvf->getConstRefToFirstPiolaKirchhoffStress();
        for (int i=0; i< 3; i++)
        {
          for (int j=0; j<3; j++)
          {
            vec(i) += (Wi*alphaij*Plocal(i,j)*normal(j));
          }
        }    
      }
      else
      {
        Msg::Error("ipFiniteStrain must be used in NetworkInteraction::getLinearTerm");
      }
    }
  }
  //vec.print("vec");
};

void NetworkInteraction::getBilinearTerm(const InteractionMechanism* im, std::vector<const InteractionMechanism*>& others, fullMatrix<double>& mat) const
{
  others.clear();
  int nbOthers = 0;
  std::map<const MaterialNode*, const std::vector<const InteractionMechanism*>* > localNodeMap;
  for (std::map<int,double>::const_iterator it = im->getCoefficients().begin(); it != im->getCoefficients().end(); it++)
  {
    const MaterialNode* node = getMaterialNode(it->first);
    NodeMapContainer::const_iterator itFound = _nodeInteractionMap.find(node);
    if (itFound != _nodeInteractionMap.end())
    {
      localNodeMap[node] = &(itFound->second);
      const std::vector<const InteractionMechanism*>& allInter = itFound->second;
      nbOthers += allInter.size();
    }
    else
    {
      Msg::Error("list interaction is not found in NetworkInteraction::getBilinearTerm");
      Msg::Exit(0);
    }
  };
  if (mat.size1() != 3 || mat.size2() != 3*nbOthers)
  {
    mat.resize(3,3*nbOthers,true);
  }
  else
  {
    mat.setAll(0.);
  }
  static SVector3 normal;
  im->getDirection(normal);
  for (std::map<int,double>::const_iterator it = im->getCoefficients().begin(); it != im->getCoefficients().end(); it++)
  {
    const MaterialNode* node = getMaterialNode(it->first);
    const std::vector<const InteractionMechanism*>& allInteractions = *(localNodeMap[node]);
    int nbInteractions = allInteractions.size();
    nbOthers = others.size(); 
    //
    double alpha = it->second;
    double Wi = getWeight(node);
    if (node->ips != NULL)
    {
      const ipFiniteStrain* ipvf = dynamic_cast<const ipFiniteStrain*>(node->ips->getState(IPStateBase::current));
      if (ipvf != NULL)
      {
        const STensor43& DPDFlocal = ipvf->getConstRefToTangentModuli();
        for (int p=0; p< nbInteractions; p++)
        {
          const InteractionMechanism* pm = allInteractions[p];
          others.push_back(pm);
          static STensor33 DFlocalDa;
          pm->getDInducedStrainDa(node->numberIndex,DFlocalDa);
          for (int i=0; i< 3; i++)
          {
            for (int j=0; j<3; j++)
            {
              for (int q=0; q<3; q++)
              {
                for (int r=0; r<3; r++)
                {
                  for (int s=0; s<3; s++)
                  {
                    mat(i,3*nbOthers+3*p+s) += (Wi*alpha*DPDFlocal(i,j,q,r)*DFlocalDa(q,r,s)*normal(j));
                  }
                }
              }
            }
          }
        }    
      }
      else
      {
        Msg::Error("ipFiniteStrain must be used in NetworkInteraction::getBilinearTerm");
      }
    }
  }
  
};

void NetworkInteraction::getTangentTerm(const InteractionMechanism* im, fullMatrix<double>& mat) const
{
  if (mat.size1() != 3 || mat.size2() != 9)
  {
    mat.resize(3,9,true);
  }
  else
  {
    mat.setAll(0.);
  }
  static SVector3 normal;
  im->getDirection(normal);
  for (std::map<int,double>::const_iterator it = im->getCoefficients().begin(); it != im->getCoefficients().end(); it++)
  {
    const MaterialNode* node = getMaterialNode(it->first);
    double alpha = it->second;
    double Wi = getWeight(node);
    if (node->ips != NULL)
    {
      const ipFiniteStrain* ipvf = dynamic_cast<const ipFiniteStrain*>(node->ips->getState(IPStateBase::current));
      if (ipvf != NULL)
      {
        const STensor43& DPDFlocal = ipvf->getConstRefToTangentModuli();
        for (int i=0; i< 3; i++)
        {
          for (int j=0; j<3; j++)
          {
            for (int p=0; p< 3; p++)
            {
              for (int q=0; q<3; q++)
              {
                mat(i,Tensor23::getIndex(p,q)) += (Wi*alpha*DPDFlocal(i,j,p,q)*normal(j));
              }
            }
          }
        }    
      }
      else
      {
        Msg::Error("ipFiniteStrain must be used in NetworkInteraction::getTangentTerm");
      }
    }
  }
};

void NetworkInteraction::getLinearInterfaceTerm(const MaterialNode* node, fullVector<double>& vec) const
{
  if (node->withCohesive())
  {
    int numCoh = node->cohesiveCoeffVars.size();
    if (vec.size() != numCoh*3)
    {
      vec.resize(numCoh*3,true);
    }
    else
    {
      vec.setAll(0.);
    }
    
    if (node->ips != NULL)
    {
      const ipFiniteStrain* ipvf = dynamic_cast<const ipFiniteStrain*>(node->ips->getState(IPStateBase::current));
      const STensor3& P = ipvf->getConstRefToFirstPiolaKirchhoffStress();
      static std::vector<SVector3> normal;
      node->getNormal(normal);
      for (int i=0; i< numCoh; i++)
      {
        const SVector3& T = node->ipsCohesive->getConstRefToInterfaceForce(i);
        for (int j=0; j< 3; j++)
        {
          vec(i+numCoh*j) -= T(j);
          for (int k=0; k<3; k++)
          {
            vec(i+numCoh*j) +=  P(j,k)*normal[i](k);
          }
        }
      }
    }
  };
};

void NetworkInteraction::getBiLinearInterfaceTerm(const MaterialNode* node, fullMatrix<double>& mat) const
{
  if (node->withCohesive())
  {
    int numCoh = node->cohesiveCoeffVars.size();
    if (mat.size1() != numCoh*3 || mat.size2() != numCoh*3)
    {
      mat.resize(numCoh*3,numCoh*3,true);
    }
    else
    {
      mat.setAll(0.);
    }
    if (node->ips != NULL)
    {
      const ipFiniteStrain* ipvf = dynamic_cast<const ipFiniteStrain*>(node->ips->getState(IPStateBase::current));
      const STensor43& L = ipvf->getConstRefToTangentModuli();
      static std::vector<SVector3> normal;
      node->getNormal(normal);
      static std::vector<STensor33> DFiDjump;
      node->getDInducedCohesiveStrainDjump(DFiDjump);
      
      for (int i=0; i< numCoh; i++)
      {
        const STensor3& Q = node->ipsCohesive->getConstRefToDInterfaceForceDJump(i);
        for (int j=0; j< 3; j++)
        {
          for (int k=0; k<3; k++)
          {
            mat(i+numCoh*j,i+numCoh*k) -= Q(j,k);
          }
          for (int k=0; k<3; k++)
          {
            for (int p=0; p< numCoh; p++)
            {
              for (int q=0; q< 3; q++)
              {
                for (int r=0; r<3; r++)
                {
                  for (int t=0; t< 3; t++)
                  {
                    mat(i+numCoh*j, p+numCoh*q) -=  L(j,k,t,r)*normal[i](k)*DFiDjump[p](t,r,q);
                  }
                }
              }
            }
          }
        }
      }
    }
  };
};

void NetworkInteraction::getBilinearCrossTermBulkInterface(const InteractionMechanism* im,  
                                     std::vector<const MaterialNode*>& materialNodes, fullMatrix<double>& mat) const
{
  materialNodes.clear();
  int totalNumCoh = 0;
  for (std::map<int,double>::const_iterator it = im->getCoefficients().begin(); it != im->getCoefficients().end(); it++)
  {
    const MaterialNode* node = getMaterialNode(it->first);
    if (node->withCohesive())
    {
      materialNodes.push_back(node);
      totalNumCoh += node->cohesiveCoeffVars.size();
    }
  };
  if (totalNumCoh == 0) 
  {
    return;
  }
  if (mat.size1() != 3 || mat.size2() != 3*totalNumCoh)
  {
    mat.resize(3,3*totalNumCoh,true);
  }
  else
  {
    mat.setAll(0.);
  }
  static SVector3 normal;
  im->getDirection(normal);
  
  //normal.print("normal");
  totalNumCoh = 0; // start
  for (std::map<int,double>::const_iterator it = im->getCoefficients().begin(); it != im->getCoefficients().end(); it++)
  {
    const MaterialNode* node = getMaterialNode(it->first);
    double alphaij = it->second;
    double Wi = getWeight(node);
    if ((node->ips != NULL) and node->withCohesive())
    {
      //Msg::Info("alpha = %e, Wi = %e",alpha,Wi);
      const ipFiniteStrain* ipvf = dynamic_cast<const ipFiniteStrain*>(node->ips->getState(IPStateBase::current));
      if (ipvf != NULL)
      {
        int nCohNode = node->cohesiveCoeffVars.size();
        const STensor43& Llocal = ipvf->getConstRefToTangentModuli();
        
        static std::vector<STensor33> DFiDjump;
        node->getDInducedCohesiveStrainDjump(DFiDjump);
        
        for (int i=0; i< 3; i++)
        {
          for (int j=0; j<3; j++)
          {
            for (int p=0; p< nCohNode; p++)
            {
              for (int q=0; q<3; q++)
              {
                for (int r=0; r<3; r++)
                {
                  for (int t=0; t<3; t++)
                  {
                    mat(i,3*totalNumCoh+p+nCohNode*t) -= (Wi*alphaij*Llocal(i,j,q,r)*DFiDjump[p](q,r,t)*normal(j));
                  }
                }
              }
            }
          }
        }  
        totalNumCoh += nCohNode;
      }
      else
      {
        Msg::Error("ipFiniteStrain must be used in NetworkInteraction::getBilinearCrossTermBulkInterface");
      }
    }
  }
}
//
void NetworkInteraction::getBilinearCrossTermInterfaceBulk(const MaterialNode* node, std::vector<const InteractionMechanism*>& others, 
                                 fullMatrix<double>& mat) const
{
  if (node->withCohesive())
  {
    NodeMapContainer::const_iterator itFound = _nodeInteractionMap.find(node);
    if (itFound == _nodeInteractionMap.end())
    {
      Msg::Error("list interaction is not found in NetworkInteraction::getBilinearCrossTermInterfaceBulk");
      Msg::Exit(0);
    }
    others = itFound->second;
    int numCoh = node->cohesiveCoeffVars.size();
    int numInteraction = others.size();
    if (mat.size1() != numCoh*3 || mat.size2() != 3*numInteraction)
    {
      mat.resize(numCoh*3, 3*numInteraction,true);
    }
    else
    {
      mat.setAll(0.);
    }
    
    if (node->ips != NULL)
    {
      const ipFiniteStrain* ipvf = dynamic_cast<const ipFiniteStrain*>(node->ips->getState(IPStateBase::current));
      const STensor43& L = ipvf->getConstRefToTangentModuli();
      static std::vector<SVector3> normal;
      node->getNormal(normal);
      static std::vector<STensor33> DFlocalDa;
      DFlocalDa.resize(numInteraction);
      for (int p=0; p< numInteraction; p++)
      {
        others[p]->getDInducedStrainDa(node->numberIndex,DFlocalDa[p]);
      }
      for (int i=0; i< numCoh; i++)
      {
        for (int j=0; j< 3; j++)
        {
          for (int k=0; k<3; k++)
          {
            for (int p=0; p< numInteraction; p++)
            {
              for (int q=0; q<3; q++)
              {
                for (int r=0; r<3; r++)
                {
                  for (int t=0; t<3; t++)
                  {
                    mat(i+numCoh*j,3*p+t) +=  L(j,k,q,r)*DFlocalDa[p](q,r,t)*normal[i](k);
                  }
                }
              }
            }
          }
        }
      }
    }
  };
}

CoefficientReductionAll::CoefficientReductionAll(): CoefficientReduction(){}

void CoefficientReductionAll::updateCoefficientVars(const NetworkInteraction& interaction,  InteractionMechanism& im) const
{
  // no vars
  im.getRefToCoeffVars().clear();
}
void CoefficientReductionAll::updateCoefficientFromCoefficientVars(const NetworkInteraction& interaction, InteractionMechanism& im) const
{
  std::vector<int> allNegatives, allPositives;
  im.seperateNegativeAndPositive(allNegatives,allPositives);
  
  double sumNegative = 0.;
  for (int j=0; j< allNegatives.size(); j++)
  {
    const MaterialNode* node = interaction.getMaterialNode(allNegatives[j]);
    sumNegative += interaction.getWeight(node);
  }
  double sumPositive = 0.;
  for (int j=0; j< allPositives.size(); j++)
  {
    const MaterialNode* node = interaction.getMaterialNode(allPositives[j]);
    sumPositive += interaction.getWeight(node);
  }
  
  for (int j=0; j< allNegatives.size(); j++)
  {
    im.getRefToCoefficients()[allNegatives[j]] = -sumPositive/(sumPositive+sumNegative);
  }
  for (int j=0; j< allPositives.size(); j++)
  {
    im.getRefToCoefficients()[allPositives[j]] = sumNegative/(sumPositive+sumNegative);
  }
  // no modify
  return;
}
void CoefficientReductionAll::getDCoefficientsDWeights(const NetworkInteraction& interaction, const InteractionMechanism& im, int nodeId, std::vector<int>&allNodes, std::vector<double>& DalphaDW) const
{
  DalphaDW.clear();
  allNodes.clear();
  const std::map<int, double>& allCoeffs = im.getCoefficients();
  if (allCoeffs.find(nodeId) == allCoeffs.end())
  {
    return;
  }
  // 
  std::vector<int> allNegatives, allPositives;
  im.seperateNegativeAndPositive(allNegatives,allPositives);
  double sumNegative = 0.;
  for (int j=0; j< allNegatives.size(); j++)
  {
    const MaterialNode* node = interaction.getMaterialNode(allNegatives[j]);
    sumNegative += interaction.getWeight(node);
  }
  double sumPositive = 0.;
  for (int j=0; j< allPositives.size(); j++)
  {
    const MaterialNode* node = interaction.getMaterialNode(allPositives[j]);
    sumPositive += interaction.getWeight(node);
  }
 
  allNodes.resize(im.getNumMaterialNodes());
  DalphaDW.resize(im.getNumMaterialNodes());
  double alpha = im.getCoefficientForNode(nodeId);
  if (alpha >0)
  {
    // val = sumNegative/(sumPositive+sumNegative);
    for (int j=0; j< allNegatives.size(); j++)
    {
      allNodes[j] = allNegatives[j];
      DalphaDW[j] = 1./(sumPositive+sumNegative) - sumNegative/(sumPositive+sumNegative)/(sumPositive+sumNegative);
    }
    for (int j=0; j< allPositives.size(); j++)
    {
      allNodes[j+allNegatives.size()] = allPositives[j];
      DalphaDW[j+allNegatives.size()] = - sumNegative/(sumPositive+sumNegative)/(sumPositive+sumNegative);
    }
  }
  else
  {
    // val = -sumPositive/(sumPositive+sumNegative);
    for (int j=0; j< allNegatives.size(); j++)
    {
      allNodes[j] = allNegatives[j];
      DalphaDW[j] = sumPositive/(sumPositive+sumNegative)/(sumPositive+sumNegative);
    }
    for (int j=0; j< allPositives.size(); j++)
    {
      allNodes[j+allNegatives.size()] = allPositives[j];
      DalphaDW[j+allNegatives.size()] = -1./(sumPositive+sumNegative) + sumPositive/(sumPositive+sumNegative)/(sumPositive+sumNegative);
    };
  };
};
void CoefficientReductionAll::getDCoefficientsDCoefficientVars(const NetworkInteraction& interaction, const InteractionMechanism& im, int nodeId, std::vector<double>& DalphaVars) const
{
  // do nothing
  DalphaVars.clear();
};


CoefficientReductionFullCoefficient::CoefficientReductionFullCoefficient(): CoefficientReductionAll(){}

void CoefficientReductionFullCoefficient::reductionAllInteraction(int id)
{
  Msg::Info("add reduction all interaction for mechanism %d",id);
  _reductionAllInteraction.insert(id);
}

bool CoefficientReductionFullCoefficient::withCoefficientVars(const NetworkInteraction& interaction, const InteractionMechanism& im) const 
{
  if (_reductionAllInteraction.find(im.getIndex()) != _reductionAllInteraction.end())
  {
    return false;
  }
  if (im.getNumMaterialNodes() > 2)
  {
    return true;
  }
  else
  {
    return false;
  }
};
int CoefficientReductionFullCoefficient::getNumberOfCoefficientVars(const NetworkInteraction& interaction, const InteractionMechanism& im) const 
{
  if (_reductionAllInteraction.find(im.getIndex()) != _reductionAllInteraction.end())
  {
    return 0;
  }
  if (im.getNumMaterialNodes() > 2)
  {
    return im.getNumMaterialNodes()-1;
  }
  else
  {
    return 0;
  }
};

void CoefficientReductionFullCoefficient::updateCoefficientVars(const NetworkInteraction& interaction, InteractionMechanism& im) const
{
  // no vars
  std::vector<double>& vars = im.getRefToCoeffVars();
  vars.clear();
  int numVars = getNumberOfCoefficientVars(interaction,im);
  if (numVars > 0)
  {
    vars.resize(numVars);
    static std::vector<int> allNodeIds;
    static std::vector<double> allCoeffs;
    im.getAllMaterialNodeIds(allNodeIds);
    im.getAllCoefficients(allCoeffs);
    // alph0 is fixed
    double alpha0 = allCoeffs[0];
    const MaterialNode* v0 = interaction.getMaterialNode(allNodeIds[0]);
    double W0 = interaction.getWeight(v0);
    for (int j=0; j< numVars; j++)
    {
      const MaterialNode* vw = interaction.getMaterialNode(allNodeIds[j+1]);
      double Ww = interaction.getWeight(vw);
      double alphaw= allCoeffs[j+1];
      vars[j] = -(alphaw*Ww)/(alpha0*W0);
    };
  }
  else
  {
    CoefficientReductionAll::updateCoefficientVars(interaction,im);
  }
};
void CoefficientReductionFullCoefficient::updateCoefficientFromCoefficientVars(const NetworkInteraction& interaction, InteractionMechanism& im) const
{
  const std::vector<double>& vars = im.getCoeffVars();
  int numVars = getNumberOfCoefficientVars(interaction,im);
  if (numVars > 0)
  {
    if (vars.size() != numVars)
    {
      Msg::Error("wrong size in CoefficientReductionFullCoefficient::updateCoefficientFromCoefficientVars");
      Msg::Exit(0);
    };
    
    double sumVars = 0;
    for (int j=0; j< vars.size(); j++)
    {
      sumVars += vars[j];
    }
    static std::vector<int> allNodeIds;
    im.getAllMaterialNodeIds(allNodeIds);
    double alpha0 = im.getCoefficientForNode(allNodeIds[0]);
    const MaterialNode* v0 = interaction.getMaterialNode(allNodeIds[0]);
    double W0 = interaction.getWeight(v0);
    for (int j=0; j< numVars; j++)
    {
      const MaterialNode* vw = interaction.getMaterialNode(allNodeIds[j+1]);
      double Ww = interaction.getWeight(vw);
      im.getRefToCoefficients()[allNodeIds[j+1]] = -(alpha0*W0*vars[j])/(Ww*sumVars);
    };
  }
  else
  {
    //Msg::Info("interaction = %d",im.getIndex());
    // two node case
    CoefficientReductionAll::updateCoefficientFromCoefficientVars(interaction,im);
  };
};
void CoefficientReductionFullCoefficient::getDCoefficientsDWeights(const NetworkInteraction& interaction, const InteractionMechanism& im, 
                      int nodeId, std::vector<int>&allNodes, std::vector<double>& DalphaDW) const
{
  DalphaDW.clear();
  allNodes.clear();
  const std::map<int, double>& allCoeffs = im.getCoefficients();
  if (allCoeffs.find(nodeId) == allCoeffs.end())
  {
    return;
  }
  // 
  const std::vector<double>& vars = im.getCoeffVars();
  int numVars = getNumberOfCoefficientVars(interaction,im);
  if (numVars > 0)
  {
    if (vars.size() != numVars)
    {
      Msg::Error("wrong size in CoefficientReductionFullCoefficient::getDCoefficientsDWeights");
      Msg::Exit(0);
    };
    static std::vector<int> allNodeIds;
    im.getAllMaterialNodeIds(allNodeIds);
    if (nodeId == allNodeIds[0])
    {
      return;
    }
    
    double sumVars = 0;
    for (int j=0; j< vars.size(); j++)
    {
      sumVars += vars[j];
    }
    double alpha0 = im.getCoefficientForNode(allNodeIds[0]);
    const MaterialNode* v0 = interaction.getMaterialNode(allNodeIds[0]);
    double W0 = interaction.getWeight(v0);
    //
    allNodes.resize(2);
    DalphaDW.resize(2);
    for (int j=0; j< numVars; j++)
    {
      if (allNodeIds[j+1] == nodeId)
      {
        const MaterialNode* vw = interaction.getMaterialNode(allNodeIds[j+1]);
        double Ww = interaction.getWeight(vw);
        //im.getRefToCoefficients()[j+1] = -(alpha0*W0*vars[j])/(Ww*sumVars);
        allNodes[0] = allNodeIds[0];
        allNodes[1] = allNodeIds[j+1];
                
        DalphaDW[0] = -(alpha0*vars[j])/(Ww*sumVars);
        DalphaDW[1] = (alpha0*W0*vars[j])/(Ww*Ww*sumVars);
        return;
      }
    };
  }
  else 
  {
    CoefficientReductionAll::getDCoefficientsDWeights(interaction,im,nodeId,allNodes,DalphaDW);
  }
};
void CoefficientReductionFullCoefficient::getDCoefficientsDCoefficientVars(const NetworkInteraction& interaction, const InteractionMechanism& im, int nodeId, std::vector<double>& DalphaVars) const
{
  // do nothing
  DalphaVars.clear();
  int numVars = getNumberOfCoefficientVars(interaction,im);
  if (numVars > 0)
  {
    const std::vector<double>& vars = im.getCoeffVars();
    if (vars.size() != numVars)
    {
      Msg::Error("wrong size in CoefficientReductionFullCoefficient::getDCoefficientsDWeights");
      Msg::Exit(0);
    };
    
    DalphaVars.resize(numVars);
    for (int j=0; j< numVars; j++)
    {
      DalphaVars[j] = 0;
    }
    static std::vector<int> allNodeIds;
    im.getAllMaterialNodeIds(allNodeIds);
    if (nodeId == allNodeIds[0])
    {
      return;
    }
    
    
    double sumVars = 0;
    for (int j=0; j< vars.size(); j++)
    {
      sumVars += vars[j];
    }
    
    double alpha0 = im.getCoefficientForNode(allNodeIds[0]);
    const MaterialNode* v0 = interaction.getMaterialNode(allNodeIds[0]);
    double W0 = interaction.getWeight(v0);
    for (int j=0; j< numVars; j++)
    {
      if (allNodeIds[j+1] == nodeId)
      {
        const MaterialNode* vw = interaction.getMaterialNode(allNodeIds[j+1]);
        double Ww = interaction.getWeight(vw);
        //im.getRefToCoefficients()[j+1] = -(alpha0*W0*vars[j])/(Ww*sumVars);
        for (int k=0; k< numVars; k++)
        {
          if (k==j)
          {
            DalphaVars[k] = -(alpha0*W0)/(Ww*sumVars)+(alpha0*W0*vars[j])/(Ww*sumVars*sumVars);
          }
          else
          {
            DalphaVars[k] = (alpha0*W0*vars[j])/(Ww*sumVars*sumVars);
          }
        }
        return;
      }
    };
    
  }
  else 
  {
    CoefficientReductionAll::getDCoefficientsDCoefficientVars(interaction,im,nodeId,DalphaVars);
  }
};