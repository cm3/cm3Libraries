//
// C++ Interface: clustering
//
//
// Author:  <V.-D. Nguyen>, (C) 2020
//
// Copyright: See COPYING file that comes with this distribution
//
//

#include "Clustering.h"
#include "nonLinearMechSolver.h"
#include "ipField.h"
#include "ipFiniteStrain.h"
#include "ipJ2linear.h"
#include <chrono>
#include <random>
#include "StringUtils.h"
#include "OS.h"
#include "numericalMaterial.h"
#include "STensorOperations.h"
#include "scalarFunction.h"
#include "meshPartition.h"
#include "Context.h"
#include "GModel.h"
#include "TFATools.h"

#include <iostream>

using namespace std;

std::string Clustering::ToString(const int i)
{
  if (i == A_00) return "A_00";
  else if (i == A_01) return "A_01";
  else if (i == A_02) return "A_02";
  else if (i == A_03) return "A_03";
  else if (i == A_04) return "A_04";
  else if (i == A_05) return "A_05";
  else if (i == A_10) return "A_10";
  else if (i == A_11) return "A_11";
  else if (i == A_12) return "A_12";
  else if (i == A_13) return "A_13";
  else if (i == A_14) return "A_14";
  else if (i == A_15) return "A_15";
  else if (i == A_20) return "A_20";
  else if (i == A_21) return "A_21";
  else if (i == A_22) return "A_22";
  else if (i == A_23) return "A_23";
  else if (i == A_24) return "A_24";
  else if (i == A_25) return "A_25";
  else if (i == A_30) return "A_30";
  else if (i == A_31) return "A_31";
  else if (i == A_32) return "A_32";
  else if (i == A_33) return "A_33";
  else if (i == A_34) return "A_34";
  else if (i == A_35) return "A_35";
  else if (i == A_40) return "A_40";
  else if (i == A_41) return "A_41";
  else if (i == A_42) return "A_42";
  else if (i == A_43) return "A_43";
  else if (i == A_44) return "A_44";
  else if (i == A_45) return "A_45";
  else if (i == A_50) return "A_50";
  else if (i == A_51) return "A_51";
  else if (i == A_52) return "A_52";
  else if (i == A_53) return "A_53";
  else if (i == A_54) return "A_54";
  else if (i == A_55) return "A_55";
  else if (i == A_norm) return "A_norm";
  else if (i == ClusterIndex) return "ClusterIndex";
  else if (i == ClusterIndexHierarchicalGlobal) return "ClusterIndexHierarchicalGlobal";
  else if (i == PFC_0) return "PFC_0";
  else if (i == PFC_1) return "PFC_1";
  else if (i == PFC_2) return "PFC_2";
  else if (i == PFC_3) return "PFC_3";
  else if (i == PFC_4) return "PFC_4";
  else if (i == PFC_5) return "PFC_5";
  else 
  {
    return "undefined";
  }
};

Clustering::Clustering(): _totalNumberOfClusters(0)
{
};

int Clustering::getMaterialLawNumberIncluster(int cluster) const
{
  std::map<int, int >::const_iterator itF = _clusterMaterialMap.find(cluster);
  if (itF == _clusterMaterialMap.end())
  {
    Msg::Error("material law for cluster %d cannot be found",cluster);
    return 0;
  }
  else
  {
    return itF->second;
  }
}


int Clustering::getClusterIndex(int ele, int gp) const
{
  int type = numericalMaterialBase::createTypeWithTwoInts(ele,gp);
  std::map<int, int >::const_iterator itF = _clusterIndex.find(type);
  if (itF == _clusterIndex.end())
  {
    #ifdef _DEBUG
    Msg::Warning("ele %d gp %d does not exist in Clustering::getClusterIndex",ele,gp);
    #endif //_DEBUG
    return -1;
  }
  else
  {
    return itF->second;
  }
}

void Clustering::getAllClusterIndices(std::vector<int>& allIndices) const
{
  allIndices.clear();
  for (std::map<int, int>::const_iterator it = _clusterMaterialMap.begin(); it!= _clusterMaterialMap.end(); it++)
  {
    allIndices.push_back(it->first);
  }
}


int Clustering::getClusterIndexHierarchicalGlobal(int ele, int gp) const
{
  int type = numericalMaterialBase::createTypeWithTwoInts(ele,gp);
  std::map<int, int >::const_iterator itFGlobal = _clusterIndexHierarchicalGlobal.find(type);
  if (itFGlobal == _clusterIndexHierarchicalGlobal.end())
  {
    std::map<int, int >::const_iterator itF = _clusterIndex.find(type);
    if(itF == _clusterIndex.end())
    {
      #ifdef _DEBUG
      Msg::Warning("ele %d gp %d does not exist in Clustering::getClusterIndexHierarchicalGlobal",ele,gp);
      #endif //_DEBUG
      return -1;
    }
    else
    {
      itF->second;
    }
  }
  else
  {
    return itFGlobal->second;
  }
}

std::vector<int> Clustering::getClusterIndicesHierarchical(int ele, int gp) const
{
  int type = numericalMaterialBase::createTypeWithTwoInts(ele,gp);
  std::map<int, std::vector<int>>::const_iterator itF = _clusterIndexHierarchical.find(type);
  if (itF == _clusterIndexHierarchical.end())
  {
    #ifdef _DEBUG
    Msg::Warning("ele %d gp %d does not exist in Clustering::getClusterIndices",ele,gp);
    #endif //_DEBUG
    std::vector<int> a;
    a[0] = -1;
    return a;
  }
  else
  {
    return itF->second;
  }
}

void Clustering::getAllClusterIndicesLevel1(std::map<int,std::vector<int>>& allIndices) const
{
  allIndices.clear();
  for (std::map<int, std::map<int, int>>::const_iterator itLv0 = _clusterMaterialMapLevel1.begin(); itLv0!= _clusterMaterialMapLevel1.end(); itLv0++)
  {
    const int& clusterLv0 = itLv0->first;
    const std::map<int, int>& clusterMaterialMapOnLevel1 = itLv0->second;
    std::vector<int>& indicesLv1 = allIndices[clusterLv0];
    for (std::map<int, int>::const_iterator itLv1 = clusterMaterialMapOnLevel1.begin(); itLv1!= clusterMaterialMapOnLevel1.end(); itLv1++)
    {
      indicesLv1.push_back(itLv1->first);
    }
  }
}

int Clustering::getNumberOfSubClusters(std::vector<int> clHostIDs) const
{
  int clLv0 = clHostIDs[0];
  std::map<int, int>::const_iterator itLv0 = _numberOfClustersLevel1.find(clLv0);
  return itLv0->second;
}


const fullVector<double>& Clustering::getPFC(int ele, int gp) const
{
  int type = numericalMaterialBase::createTypeWithTwoInts(ele,gp);
  std::map<int, fullVector<double>>::const_iterator itF = _clusterPFC.find(type);
  if (itF == _clusterPFC.end())
  {
    #ifdef _DEBUG
    Msg::Warning("ele %d gp %d does not exist in Clustering::getPFC",ele,gp);
    #endif //_DEBUG
    return -1;
  }
  return (itF->second);
}

const fullMatrix<double>& Clustering::getStrainConcentrationTensor(int ele, int gp) const
{
  int type = numericalMaterialBase::createTypeWithTwoInts(ele,gp);
  std::map<int, fullMatrix<double>*>::const_iterator itF = _directMapOfStrainConcentration.find(type);
  if (itF == _directMapOfStrainConcentration.end())
  {
    Msg::Error("ele %d gp %d does not exist in Clustering::getStrainConcentrationTensor",ele,gp);
    Msg::Exit(0);
  }
  return *(itF->second);
}

int Clustering::getNbOfGPsOnElement(const int ele) const
{
  if (_mapElementNumberOfGPs.size() == 0)
  {
    int GPnum = 0;
    int numGP = 1;
    while (true)
    {
      GPnum++;
      int type = numericalMaterialBase::createTypeWithTwoInts(ele,GPnum);
      if (_clusterIndex.find(type) != _clusterIndex.end())
      {
        numGP++;
      }
      else
      {
        break;
      }
    }
    return numGP;    
  }
  else
  {
    std::map<int,int>::const_iterator itF = _mapElementNumberOfGPs.find(ele);
    if (itF != _mapElementNumberOfGPs.end())
    {
      return itF->second;
    }
    else
    {
      Msg::Error("number GP of element %d is not found",ele);
      return 0;
    }
  }
};

void Clustering::clearAllClusterData()
{
  _clusterIndex.clear(); 
  _totalNumberOfClusters = 0;
  _clusterMaterialMap.clear();
  _clusterVolumeMap.clear();

  _clusterAverageStrainConcentrationTensorMap.clear();
  _clusterSTDMap.clear();
  _clusterSTDTensorMap.clear();

  _clusterAveragePlasticStrainConcentrationVectorMap.clear();
  _clusterAverageTotalStrainConcentrationVectorMap.clear();
  _clusterAveragePlasticEqStrainConcentrationVectorMap.clear();
  _clusterAverageGeometricPlasticEqStrainConcentrationVectorMap.clear();
  _clusterAverageHarmonicPlasticEqStrainConcentrationVectorMap.clear();
  _clusterAveragePowerPlasticEqStrainConcentrationVectorMap.clear();
  _clusterPlSTDVectorMap.clear();
  
  _clusterPFC.clear();

  _clusterInteractionTensorMap.clear();
  _clusterEshelbyInteractionTensorMap.clear();   
  _clusterInteractionTensorMatrixFrameELMap.clear();    
  _clusterInteractionTensorHomogenizedFrameELMap.clear();
  
  _clusterPositionMap.clear();
  
  
  // For hierarchical clustering
  _clusterIndexHierarchicalGlobal.clear(); 
  _clusterIndexHierarchical.clear(); 
  _clusterIndexLevel0.clear(); 
  
  _clusterMaterialMapLevel0.clear();
  _clusterMaterialMapLevel1.clear();
  
  _clusterVolumeMapLevel0.clear();
  _clusterVolumeMapLevel1.clear();
  
  _clusterAverageStrainConcentrationTensorMapLevel1.clear();
  _clusterInteractionTensorMapLevel1.clear();
  
  _numberOfLevels = 0;
  _level = 0;
}

void Clustering::strainForLoadingCases(int dim, double fact, std::map<int, STensor3>& modes) const
{
  // xx = 0, yy = 1, zz = 2, xy = yx = 3, xz = zx = 4, yz = zy = 5
  modes.clear();
  //double fact = 1e-4;
  if (dim == 2)
  {
    STensor3 eps;
    STensorOperation::unity(eps);
    eps(0,0) += 1.*fact;
    modes[0] = eps;
    
    STensorOperation::unity(eps);
    eps(1,1) += 1.*fact;
    modes[1] = eps;
    
    STensorOperation::unity(eps);
    eps(0,1) += 0.5*fact;
    eps(1,0) += 0.5*fact;
    modes[3] = eps;
  }
  else if (dim == 3)
  {
    STensor3 eps;
    STensorOperation::unity(eps);
    eps(0,0) += 1.*fact;
    modes[0] = eps;
    
    STensorOperation::unity(eps);
    eps(1,1) += 1.*fact;
    modes[1] = eps;
    
    STensorOperation::unity(eps);
    eps(2,2) += 1.*fact;
    modes[2] = eps;
    
    STensorOperation::unity(eps);
    eps(0,1) += 0.5*fact;
    eps(1,0) += 0.5*fact;
    modes[3] = eps;
    
    STensorOperation::unity(eps);
    eps(0,2) += 0.5*fact;
    eps(2,0) += 0.5*fact;
    modes[4] = eps;
    
    STensorOperation::unity(eps);
    eps(1,2) += 0.5*fact;
    eps(2,1) += 0.5*fact;
    modes[5] = eps;
  }
  else
  {
    Msg::Error("missing cases in Clustering::strainForLoadingCase");
    Msg::Exit(0);
  }
};

void Clustering::stressForLoadingCases(int dim, double fact, std::map<int, STensor3>& modes) const
{
  // xx = 0, yy = 1, zz = 2, xy = yx = 3, xz = zx = 4, yz = zy = 5
  modes.clear();
  //double fact = 1e-4;
  fact *= -1.;
  if (dim == 2)
  {
    STensor3 sig;
    STensorOperation::zero(sig);
    sig(0,0) += 1.*fact;
    modes[0] = sig;
    
    STensorOperation::zero(sig);
    sig(1,1) += 1.*fact;
    modes[1] = sig;
    
    STensorOperation::zero(sig);
    sig(0,1) += 0.5*fact;
    sig(1,0) += 0.5*fact;
    modes[3] = sig;
  }
  else if (dim == 3)
  {
    STensor3 sig;
    STensorOperation::zero(sig);
    sig(0,0) += 1.*fact;
    modes[0] = sig;
    
    STensorOperation::zero(sig);
    sig(1,1) += 1.*fact;
    modes[1] = sig;
    
    STensorOperation::zero(sig);
    sig(2,2) += 1.*fact;
    modes[2] = sig;
    
    STensorOperation::zero(sig);
    sig(0,1) += 0.5*fact;
    sig(1,0) += 0.5*fact;
    modes[3] = sig;
    
    STensorOperation::zero(sig);
    sig(0,2) += 0.5*fact;
    sig(2,0) += 0.5*fact;
    modes[4] = sig;
    
    STensorOperation::zero(sig);
    sig(1,2) += 0.5*fact;
    sig(2,1) += 0.5*fact;
    modes[5] = sig;
  }
  else
  {
    Msg::Error("missing cases in Clustering::stressForLoadingCases");
    Msg::Exit(0);
  }
};

void Clustering::strainForLoadingCasesVolumePreserving(int dim, double fact, std::map<int, STensor3>& modes) const
{
  // xx = 0, yy = 1, zz = 2, xy = yx = 3, xz = zx = 4, yz = zy = 5
  modes.clear();
  //double fact = 1e-4;
  if (dim == 2)
  {
    STensor3 eps;
    STensorOperation::unity(eps);
    eps(0,0) += 1.*fact;
    eps(1,1) -= 1.*fact;
    modes[0] = eps;
    
    STensorOperation::unity(eps);
    eps(1,1) += 1.*fact;
    eps(0,0) -= 1.*fact;
    modes[1] = eps;
    
    STensorOperation::unity(eps);
    eps(0,1) += 0.5*fact;
    eps(1,0) += 0.5*fact;
    modes[3] = eps;
  }
  else if (dim == 3)
  {
    STensor3 eps;
    STensorOperation::unity(eps);
    eps(0,0) += 1.*fact;
    eps(1,1) -= 0.5*fact;
    eps(2,2) -= 0.5*fact;
    modes[0] = eps;
    
    STensorOperation::unity(eps);
    eps(1,1) += 1.*fact;
    eps(0,0) -= 0.5*fact;
    eps(2,2) -= 0.5*fact;
    modes[1] = eps;
    
    STensorOperation::unity(eps);
    eps(2,2) += 1.*fact;
    eps(0,0) -= 0.5*fact;
    eps(1,1) -= 0.5*fact;
    modes[2] = eps;
    
    STensorOperation::unity(eps);
    eps(0,1) += 0.5*fact;
    eps(1,0) += 0.5*fact;
    modes[3] = eps;
    
    STensorOperation::unity(eps);
    eps(0,2) += 0.5*fact;
    eps(2,0) += 0.5*fact;
    modes[4] = eps;
    
    STensorOperation::unity(eps);
    eps(1,2) += 0.5*fact;
    eps(2,1) += 0.5*fact;
    modes[5] = eps;
  }
  else
  {
    Msg::Error("missing cases in Clustering::strainForLoadingCase");
    Msg::Exit(0);
  }
};

void Clustering::strainForInelasticLoadingCases(int dim, double fact, std::map<int, STensor3>& modes) const
{
  // xx = 0, yy = 1, zz = 2, xy = yx = 3, xz = zx = 4, yz = zy = 5
  modes.clear();
  if (dim == 2)
  {
    STensor3 eps;

    STensorOperation::unity(eps);
    eps(0,0) += 1.0*fact;
    eps(1,1) -= 1.0*fact;
    modes[0] = eps;
    
    STensorOperation::unity(eps);
    eps(0,1) += 1.0*fact;
    eps(1,0) += 1.0*fact;
    modes[1] = eps;
    
    /*double fact_mixed = fact*sqrt(2.)/sqrt(2.5);
    STensorOperation::unity(eps);
    eps(0,0) += 0.5*fact_mixed;
    eps(1,1) -= 0.5*fact_mixed;
    eps(0,1) += 1.0*fact_mixed;
    eps(1,0) += 1.0*fact_mixed;
    modes[2] = eps;
    
    STensorOperation::unity(eps);
    eps(0,0) += 1.0*fact_mixed;
    eps(1,1) -= 1.0*fact_mixed;
    eps(0,1) += 0.5*fact_mixed;
    eps(1,0) += 0.5*fact_mixed;
    modes[3] = eps;
    
    double fact_mixed2 = fact/2.;
    STensorOperation::unity(eps);
    eps(0,0) += 1.0*fact_mixed2;
    eps(1,1) -= 1.0*fact_mixed2;
    eps(0,1) += 1.0*fact_mixed2;
    eps(1,0) += 1.0*fact_mixed2;
    modes[4] = eps;
    */
  }
  else if (dim == 3)
  {
    STensor3 eps;

    STensorOperation::unity(eps);
    eps(0,0) += 1.0*fact;
    eps(1,1) -= 0.5*fact;
    eps(2,2) -= 0.5*fact;
    modes[0] = eps;
    
    STensorOperation::unity(eps);
    eps(1,1) += 1.0*fact;
    eps(0,0) -= 0.5*fact;
    eps(2,2) -= 0.5*fact;
    modes[1] = eps;
    
    STensorOperation::unity(eps);
    eps(0,1) += 1.0*fact;
    eps(1,0) += 1.0*fact;
    modes[2] = eps;
    
    /*
    STensorOperation::unity(eps);
    eps(2,2) += 1.0*fact;
    eps(1,1) -= 0.5*fact;
    eps(0,0) -= 0.5*fact;
    modes[2] = eps;  
    
    STensorOperation::unity(eps);
    eps(0,2) += 1.0*fact;
    eps(2,0) += 1.0*fact;
    modes[4] = eps;
    
    STensorOperation::unity(eps);
    eps(2,1) += 1.0*fact;
    eps(1,2) += 1.0*fact;
    modes[5] = eps;
    */

  }
  else
  {
    Msg::Error("missing cases in Clustering::strainForInelasticLoadingCases");
    Msg::Exit(0);
  }
};

void Clustering::strainForInelasticLoadingCasesPath(int dim, double fact, std::map<int, std::map<int,piecewiseScalarFunction>>& modes) const
{
  // xx = 0, yy = 1, zz = 2, xy = yx = 3, xz = zx = 4, yz = zy = 5
  modes.clear();
  double ftime = 1.;
  if (dim == 2)
  { 
    std::map<int,piecewiseScalarFunction>& fctSet_0 = modes[0];
    piecewiseScalarFunction fc00_0;
    piecewiseScalarFunction fc11_0;
    piecewiseScalarFunction fc01_0;
    
    fc00_0.put(0.,0.);
    fc00_0.put(ftime/2.,fact);
    fc00_0.put(ftime,fact);

    fc11_0.put(0.,0.);
    fc11_0.put(ftime/2.,-fact);   
    fc11_0.put(ftime,-fact);

    fc01_0.put(0.,0.);
    fc01_0.put(ftime/2.,0.);
    fc01_0.put(ftime,fact);
    
    fctSet_0[0] = fc00_0;
    fctSet_0[1] = fc11_0;
    fctSet_0[2] = fc01_0; 
    
    
    std::map<int,piecewiseScalarFunction>& fctSet_1 = modes[1];
    piecewiseScalarFunction fc00_1;
    piecewiseScalarFunction fc11_1;
    piecewiseScalarFunction fc01_1;
    
    fc00_1.put(0.,0.);
    fc00_1.put(ftime/2.,0.);
    fc00_1.put(ftime,fact);

    fc11_1.put(0.,0.);
    fc11_1.put(ftime/2.,0.);   
    fc11_1.put(ftime,-fact);

    fc01_1.put(0.,0.);
    fc01_1.put(ftime/2.,fact);
    fc01_1.put(ftime,fact);
    
    fctSet_1[0] = fc00_1;
    fctSet_1[1] = fc11_1;
    fctSet_1[2] = fc01_1;     
  }
  else if (dim == 3)
  {
    std::map<int,piecewiseScalarFunction>& fctSet_0 = modes[0];
    std::map<int,piecewiseScalarFunction>& fctSet_1 = modes[1];
    
    piecewiseScalarFunction fc00_0;
    piecewiseScalarFunction fc11_0;
    piecewiseScalarFunction fc01_0;
    
    piecewiseScalarFunction fc00_1;
    piecewiseScalarFunction fc11_1;
    piecewiseScalarFunction fc01_1;
  
    fc00_0.put(0.,0.);
    fc00_0.put(ftime/4.,fact);
    fc00_0.put(ftime/2.,fact);
    fc00_0.put(3.*ftime/4.,0.);
    fc00_0.put(ftime,0.);

    fc11_0.put(0.,0.);
    fc11_0.put(ftime/4.,-fact);
    fc11_0.put(ftime/2.,-fact);   
    fc11_0.put(3.*ftime/4.,0.);
    fc11_0.put(ftime,0.);

    fc01_0.put(0.,0.);
    fc01_0.put(ftime/4.,0.);
    fc01_0.put(ftime/2.,fact);
    fc01_0.put(3.*ftime/4.,fact);
    fc01_0.put(ftime,0.);
    
    fctSet_0[0] = fc00_0;
    fctSet_0[1] = fc11_0;
    fctSet_0[2] = fc01_0;
    
    
    fc00_1.put(0.,0.);
    fc00_1.put(ftime/4.,-fact);
    fc00_1.put(ftime/2.,-fact);
    fc00_1.put(3.*ftime/4.,0.);
    fc00_1.put(ftime,0.);

    fc11_1.put(0.,0.);
    fc11_1.put(ftime/4.,fact);
    fc11_1.put(ftime/2.,fact);   
    fc11_1.put(3.*ftime/4.,0.);
    fc11_1.put(ftime,0.);

    fc01_1.put(0.,0.);
    fc01_1.put(ftime/4.,0.);
    fc01_1.put(ftime/2.,fact);
    fc01_1.put(3.*ftime/4.,fact);
    fc01_1.put(ftime,0.);
    
    fctSet_1[0] = fc00_1;
    fctSet_1[1] = fc11_1;
    fctSet_1[2] = fc01_1;   
  }
  else
  {
    Msg::Error("missing cases in Clustering::strainForInelasticLoadingCasesPath");
    Msg::Exit(0);
  }
};


void Clustering::setNumberOfClustersForMaterial(int matNum, int nbCluster)
{
  _numberOfClustersPerLaw[matNum] = nbCluster;
  Msg::Info("material %d, number of clusters = %d",matNum,nbCluster);
}

void Clustering::setNumberOfLevels(int nbLevels)
{
  _numberOfLevels = nbLevels;
  Msg::Info("number of levels = %d",nbLevels);
}

void Clustering::setNumberOfClustersForMaterialHierarchical(int matNum, int nbClusterLevel0, int nbClusterLevel1, int nbClusterLevel2, int nbClusterLevel3, int nbClusterLevel4)
{
  switch(_numberOfLevels) {
    case 1: 
    {
      std::vector<int>& nbClusterPerLevel = _numberOfClustersPerLevelPerLaw[matNum];
      nbClusterPerLevel.resize(1);
      nbClusterPerLevel[0] = nbClusterLevel0;
    }
    break;
    case 2: 
    { 
      std::vector<int>& nbClusterPerLevel = _numberOfClustersPerLevelPerLaw[matNum];
      nbClusterPerLevel.resize(2);
      nbClusterPerLevel[0] = nbClusterLevel0;
      nbClusterPerLevel[1] = nbClusterLevel1;
    }
    break;
    case 3: 
    {  
      std::vector<int>& nbClusterPerLevel = _numberOfClustersPerLevelPerLaw[matNum];
      nbClusterPerLevel.resize(3);
      nbClusterPerLevel[0] = nbClusterLevel0;
      nbClusterPerLevel[1] = nbClusterLevel1;
      nbClusterPerLevel[2] = nbClusterLevel2;
    }
    break;
    case 4: 
    {
      std::vector<int>& nbClusterPerLevel = _numberOfClustersPerLevelPerLaw[matNum];
      nbClusterPerLevel.resize(4);
      nbClusterPerLevel[0] = nbClusterLevel0;
      nbClusterPerLevel[1] = nbClusterLevel1;
      nbClusterPerLevel[2] = nbClusterLevel2;
      nbClusterPerLevel[3] = nbClusterLevel3;
    }
    break;
    case 5: 
    {
      std::vector<int>& nbClusterPerLevel = _numberOfClustersPerLevelPerLaw[matNum];
      nbClusterPerLevel.resize(5);
      nbClusterPerLevel[0] = nbClusterLevel0;
      nbClusterPerLevel[1] = nbClusterLevel1;
      nbClusterPerLevel[2] = nbClusterLevel2;
      nbClusterPerLevel[3] = nbClusterLevel3;
      nbClusterPerLevel[4] = nbClusterLevel4;
    }
    break;
  }
}

void Clustering::solveStrainConcentration(nonLinearMechSolver& solver, 
                                          int computeHomStiffness, 
                                          int homStiffnessFromLocal, 
                                          int getLocalOrientation,
                                          bool saveToFile, const std::string strainConcentrationFileName, 
                                          bool saveLocalOrientationDataToFile, const std::string localOrientationFileName)
{
  // initialize solve if it has not
  if (!solver.isInitialized())
  {
    solver.initMicroSolver();
  }
  
  clearAllClusterData();
  
  // compute from solver
  _concentrationTensorMap.clear();
  _directMapOfStrainConcentration.clear();
  _materialMap.clear();
  _mapElementNumberOfGPs.clear();
  _mapGaussPoints.clear();
  _localStiffnessTensorMap.clear();
  _mapLocalOrientation.clear();
  
  // done
  int dim = solver.getDim();
  std::map<int, STensor3> allStrain;
  double fact_BC = 1.0e-3;
  strainForLoadingCases(dim,fact_BC,allStrain);
  
  STensor43 Cel_hom;
  double Gel_hom, Kel_hom, nuel_hom;
  double Vtot;
  
  for (std::map<int, STensor3>::const_iterator it = allStrain.begin(); it!= allStrain.end(); it++)
  {    
    int i = it->first;
    // one can solve consecutively since linear elastic problems are considered
    const STensor3& F = it->second;
    Msg::Info("loading case: %d",i);
    F.print("strain tensor: ");
    solver.getMicroBC()->setDeformationGradient(F);
      // set tangent averaging flag
    solver.tangentAveragingFlag(false);
    
    // solve micro problem
    // set view name
    std::string filenamPrefix = "disp_strainMode"+std::to_string(i);
    solver.getUnknownField()->setFileNamePrefix(filenamPrefix);
    filenamPrefix = "stress_strainMode"+std::to_string(i);
    solver.getIPField()->setFileNamePrefix(filenamPrefix);

    //
    bool success = solver.microSolve();
    if (!success)
    {
      Msg::Error("solver cannot be solved !!!");
      Msg::Exit(0);
    }
    
    else
    {
      // get data from IP field
      const IPField* ipf = solver.getIPField();
      const AllIPState* aips = ipf->getAips();
      const std::vector<partDomain*>& domainVector = *(solver.getDomainVector());
      
      STensor3 sig_hom;
      STensorOperation::zero(sig_hom);
      
      Vtot = 0.;
      
      for (int idom=0; idom < domainVector.size(); idom++)
      {
        const partDomain* dom = domainVector[idom];
        const materialLaw* matLaw = dom->getMaterialLaw();
        
        if (it == allStrain.begin() and computeHomStiffness == 1 and homStiffnessFromLocal==1)
        {
          const materialLaw* ml = matLaw->getConstNonLinearSolverMaterialLaw();        
          STensor43 CelLoc;
          ml->ElasticStiffness(&CelLoc);
          CelLoc.print("CelLoc");
          fullMatrix<double>& CelLoc_mat = _localStiffnessTensorMap[matLaw->getNum()];
          STensorOperation::fromSTensor43ToFullMatrix(CelLoc,CelLoc_mat);
        }
        
        std::map<int, fullMatrix<double> >& mapGPLaw = _concentrationTensorMap[matLaw->getNum()];
        IntPt* GP;
        double V_phase;
        V_phase = 0.;
        STensor3 sig_phase;
        for (elementGroup::elementContainer::const_iterator ite = dom->element_begin(); ite!= dom->element_end(); ite++)
        {
          MElement* ele = ite->second;
          int npts = dom->getBulkGaussIntegrationRule()->getIntPoints(ele,&GP);
          const AllIPState::ipstateElementContainer *vips = aips->getIPstate(ele->getNum());
          // data of Gauss points and element numbers are set at the first solution,
          if (it == allStrain.begin())
          {
            _mapElementNumberOfGPs[ele->getNum()] = npts;
          }
          for (int j=0; j< npts; j++)
          {  
            // data of Gauss points and element numbers are set at the first solution,
            //
            // create a unique number of element number and GP number
            int type = numericalMaterialBase::createTypeWithTwoInts(ele->getNum(),j);
            if (it == allStrain.begin())
            {
              _materialMap[type] = matLaw->getNum();
              // store gauss point
              double u = GP[j].pt[0]; double v = GP[j].pt[1]; double w = GP[j].pt[2];
              double weight = GP[j].weight;
              double detJ = ele->getJacobianDeterminant(u,v,w);
              SPoint3 pnt;
              ele->pnt(u,v,w,pnt);
              fullVector<double>& vec = _mapGaussPoints[type];
              vec.resize(4);
              vec(0) =pnt.x();
              vec(1) =pnt.y();
              vec(2) =pnt.z();
              vec(3) =weight*detJ;
              
              fullVector<double>& eulerLoc = _mapLocalOrientation[type];
              eulerLoc.resize(3);              
              for(int eul=0; eul<3; eul++)
              {
                eulerLoc(eul) = 0.;
              }              
              fullVector<double> vecGPLocation(3);
              vecGPLocation(0) = pnt.x();
              vecGPLocation(1) = pnt.y();
              vecGPLocation(2) = pnt.z();
              
              if(getLocalOrientation == 1)
              {
                const materialLaw* ml = matLaw->getConstNonLinearSolverMaterialLaw();
	        if(ml->materialLawHasLocalOrientation())
                  ml->getLocalOrientation(vecGPLocation,eulerLoc);
	        else
		  Msg::Info("Clustering::solveStrainConcentration - Material law has no getLocalOrientation function defined");
              }
            }
            fullVector<double> &GP = _mapGaussPoints[type];
            double weight_local = GP(3);
            //Vtot += weight_local;
            V_phase += weight_local;
            
            // strain at Gauss point
            const IPVariable* ipv = (*vips)[j]->getState(IPStateBase::current);
            const ipFiniteStrain* ipvfinite = dynamic_cast<const ipFiniteStrain*>(ipv);
            const STensor3& Flocal = ipvfinite->getConstRefToDeformationGradient();
            // small strain measure
            static STensor3 eps;
            static STensor3 I(1.);
            for (int ii =0; ii<3; ii++)
            {
              for (int jj=0; jj<3; jj++)
              {
                eps(ii,jj) = 0.5*(Flocal(ii,jj)+Flocal(jj,ii)) - I(ii,jj);
              }
            }
            double sigVM_local = ipv->get(IPField::SVM);
            static STensor3 sig_local;
            ipvfinite->getCauchyStress(sig_local);
            
            //sig_hom += weight_local*sig_local;
            sig_phase += weight_local*sig_local;

            // save to map
            if (mapGPLaw.find(type) == mapGPLaw.end())
            {
              // resize if it not exist before
              fullMatrix<double>& matloc = mapGPLaw[type];
              matloc.resize(6,6,true);
              _directMapOfStrainConcentration[type] = &matloc;
            }
            fullMatrix<double>& mat = mapGPLaw[type];
            if( i==allStrain.begin()->first ){unity_Voigt(mat);}
            if (i == 0 || i==1 || i==2)
            {
              mat(0,i)  = eps(0,0)/fact_BC; // A(00)(00 if i==0, 11 if i==1, 22 if i ==2)
              mat(1,i)  = eps(1,1)/fact_BC; // A(11)(00 if i==0, 11 if i==1, 22 if i ==2)
              mat(3,i)  = eps(0,1)/fact_BC; // A(01)(00 if i==0, 11 if i==1, 22 if i ==2)
              if (dim==3)
              {
              mat(2,i)  = eps(2,2)/fact_BC; // A(22)(00 if i==0, 11 if i==1, 22 if i ==2)
              mat(4,i)  = eps(0,2)/fact_BC; // A(02)(00 if i==0, 11 if i==1, 22 if i ==2)
              mat(5,i)  = eps(1,2)/fact_BC; // A(12)(00 if i==0, 11 if i==1, 22 if i ==2)
              }
            }
            else
            {
              // factor 2 for conversion in STensorOperation::fromSTensor43ToFullMatrix
              mat(0,i)  = 2.*eps(0,0)/fact_BC; // 2A(00) (01 if i==3, 02 if i==4, 12 if i ==5)
              mat(1,i)  = 2.*eps(1,1)/fact_BC; // 2A(11) (01 if i==3, 02 if i==4, 12 if i ==5)
              mat(3,i)  = 2.*eps(0,1)/fact_BC; // 2A(01) (01 if i==3, 02 if i==4, 12 if i ==5)
              if (dim==3)
              {
              mat(2,i)  = 2.*eps(2,2)/fact_BC; // 2A(22) (01 if i==3, 02 if i==4, 12 if i ==5)
              mat(4,i)  = 2.*eps(0,2)/fact_BC; // 2A(02) (01 if i==3, 02 if i==4, 12 if i ==5)
              mat(5,i)  = 2.*eps(1,2)/fact_BC; // 2A(12) (01 if i==3, 02 if i==4, 12 if i ==5)   
              }
            }
          }
        }
        sig_phase *= (1./V_phase);
        //sig_phase.print("STRESS PHASE");
        //printf("Vphase %.16g \n", V_phase);
        sig_hom += sig_phase*V_phase;
        Vtot += V_phase;
      }
      //printf("Vtot %.16g \n", Vtot);
      sig_hom *= (1./Vtot); 
      if(homStiffnessFromLocal==0)
      {  
        if(i==0)
        {
          Cel_hom(0,0,0,0) = sig_hom(0,0)/fact_BC;
          Cel_hom(1,1,0,0) = sig_hom(1,1)/fact_BC;
          Cel_hom(2,2,0,0) = sig_hom(2,2)/fact_BC;
          Cel_hom(0,1,0,0) = Cel_hom(1,0,0,0) = sig_hom(0,1)/fact_BC;
          Cel_hom(0,2,0,0) = Cel_hom(2,0,0,0) = sig_hom(0,2)/fact_BC;
          Cel_hom(2,1,0,0) = Cel_hom(1,2,0,0) = sig_hom(2,1)/fact_BC;
        }
        if(i==1)
        {
          Cel_hom(0,0,1,1) = sig_hom(0,0)/fact_BC;
          Cel_hom(1,1,1,1) = sig_hom(1,1)/fact_BC;
          Cel_hom(2,2,1,1) = sig_hom(2,2)/fact_BC;
          Cel_hom(0,1,1,1) = Cel_hom(1,0,1,1) = sig_hom(0,1)/fact_BC;
          Cel_hom(0,2,1,1) = Cel_hom(2,0,1,1) = sig_hom(0,2)/fact_BC;
          Cel_hom(2,1,1,1) = Cel_hom(1,2,1,1) = sig_hom(2,1)/fact_BC;
        }
        if(i==2)
        {
          Cel_hom(0,0,2,2) = sig_hom(0,0)/fact_BC;
          Cel_hom(1,1,2,2) = sig_hom(1,1)/fact_BC;
          Cel_hom(2,2,2,2) = sig_hom(2,2)/fact_BC;
          Cel_hom(0,1,2,2) = Cel_hom(1,0,2,2) = sig_hom(0,1)/fact_BC;
          Cel_hom(0,2,2,2) = Cel_hom(2,0,2,2) = sig_hom(0,2)/fact_BC;
          Cel_hom(2,1,2,2) = Cel_hom(1,2,2,2) = sig_hom(2,1)/fact_BC;
        }
        if(i==3)
        {
          Cel_hom(0,0,0,1) = Cel_hom(0,0,1,0) = sig_hom(0,0)/fact_BC;
          Cel_hom(1,1,0,1) = Cel_hom(1,1,1,0) = sig_hom(1,1)/fact_BC;
          Cel_hom(2,2,0,1) = Cel_hom(2,2,1,0) = sig_hom(2,2)/fact_BC;
          Cel_hom(0,1,0,1) = Cel_hom(1,0,0,1) = Cel_hom(0,1,1,0) = Cel_hom(1,0,1,0) = sig_hom(0,1)/fact_BC; 
          Cel_hom(0,2,0,1) = Cel_hom(2,0,0,1) = Cel_hom(0,2,1,0) = Cel_hom(2,0,1,0) = sig_hom(0,2)/fact_BC;
          Cel_hom(2,1,0,1) = Cel_hom(1,2,0,1) = Cel_hom(2,1,1,0) = Cel_hom(1,2,1,0) = sig_hom(2,1)/fact_BC;
        }
        if(i==4)
        {
          Cel_hom(0,0,0,2) = Cel_hom(0,0,2,0) = sig_hom(0,0)/fact_BC;
          Cel_hom(1,1,0,2) = Cel_hom(1,1,2,0) = sig_hom(1,1)/fact_BC;
          Cel_hom(2,2,0,2) = Cel_hom(2,2,2,0) = sig_hom(2,2)/fact_BC;
          Cel_hom(0,1,0,2) = Cel_hom(1,0,0,2) = Cel_hom(0,1,2,0) = Cel_hom(1,0,2,0) = sig_hom(0,1)/fact_BC;
          Cel_hom(0,2,0,2) = Cel_hom(2,0,0,2) = Cel_hom(0,2,2,0) = Cel_hom(2,0,2,0) = sig_hom(0,2)/fact_BC;
          Cel_hom(2,1,0,2) = Cel_hom(1,2,0,2) = Cel_hom(2,1,2,0) = Cel_hom(1,2,2,0) = sig_hom(2,1)/fact_BC;
        }
        if(i==5)
        {
          Cel_hom(0,0,1,2) = Cel_hom(0,0,2,1) = sig_hom(0,0)/fact_BC;
          Cel_hom(1,1,1,2) = Cel_hom(1,1,2,1) = sig_hom(1,1)/fact_BC;
          Cel_hom(2,2,1,2) = Cel_hom(2,2,2,1) = sig_hom(2,2)/fact_BC;
          Cel_hom(0,1,1,2) = Cel_hom(1,0,1,2) = Cel_hom(0,1,2,1) = Cel_hom(1,0,2,1) = sig_hom(0,1)/fact_BC;
          Cel_hom(0,2,1,2) = Cel_hom(2,0,1,2) = Cel_hom(0,2,2,1) = Cel_hom(2,0,2,1) = sig_hom(0,2)/fact_BC;
          Cel_hom(2,1,1,2) = Cel_hom(1,2,1,2) = Cel_hom(2,1,2,1) = Cel_hom(1,2,2,1) = sig_hom(2,1)/fact_BC;
        }  
      }
    
      // next step
      // save view
      solver.archiveData(i+1.,i+1,false);
      solver.nextStep(i+1.,i+1);     
    }
  }
  Msg::Info("done estimating concentration tensors");
  
  if(computeHomStiffness == 1 and homStiffnessFromLocal==1)
  { 
    fullMatrix<double> Cel_hom_mat(6,6);
    Vtot = 0.;
    for (std::map<int, std::map<int, fullMatrix<double> > >::const_iterator it = _concentrationTensorMap.begin();  it!= _concentrationTensorMap.end(); it++)
    { 
      int matNum = it->first;
      const fullMatrix<double>& Cel_phase = _localStiffnessTensorMap[matNum];
      //Cel_phase.print("Cel_phase");
      fullMatrix<double> A_phase(6,6);
      double V_phase;
      V_phase = 0.;
      const std::map<int, fullMatrix<double> >& mapIPLaw = it->second;
      for (std::map<int, fullMatrix<double> >::const_iterator itIPMap = mapIPLaw.begin(); itIPMap!=mapIPLaw.end(); itIPMap++)
      {
        int type = itIPMap->first;
        fullVector<double> &GP = _mapGaussPoints[type];
        double weight_local = GP(3);
        const fullMatrix<double>& mat = itIPMap->second;
        A_phase.add(mat,weight_local);
        V_phase += weight_local;
      }
      A_phase.scale(1./V_phase);
      //A_phase.print("A_phase");
      //printf("Vp %.16g",V_phase);
      fullMatrix<double> Cel_hom_mat_summand(6,6);
      Cel_phase.mult(A_phase,Cel_hom_mat_summand);
      Cel_hom_mat.add(Cel_hom_mat_summand,V_phase);
      Vtot += V_phase;
    }
    Cel_hom_mat.scale(1./Vtot);
    Cel_hom_mat.print("Cel_hom_mat"); 
    STensorOperation::fromFullMatrixToSTensor43(Cel_hom_mat,Cel_hom);
  }
  
  
  Cel_hom.print("CEL");
  
  STensor43 Cel_hom_iso, Cel_hom_iso_muFix;
  STensorOperation::STensor43Isotropization(Cel_hom,Cel_hom_iso);
  STensor43 Cel_hom_transverseIso;
  int polarization = 0;
  STensorOperation::STensor43TransverseIsotropization_Norris(polarization,Cel_hom,Cel_hom_transverseIso);

  Gel_hom = Cel_hom_iso(0,1,0,1);
  Kel_hom = Cel_hom_iso(0,0,0,0) - 4./3.*Gel_hom;
  nuel_hom = (3.*Kel_hom - 2.*Gel_hom)/(2.*(3.*Kel_hom + Gel_hom));
  printf("isotropic elastic props G: %g, K: %g, nu: %g \n", Gel_hom, Kel_hom, nuel_hom);  
    
  Cel_hom_iso_muFix(0,0,0,0) = Cel_hom_iso_muFix(1,1,1,1) = Cel_hom_iso_muFix(2,2,2,2) = (1.-nuel_hom)/(1.-2.*nuel_hom);
  Cel_hom_iso_muFix(0,0,1,1) = Cel_hom_iso_muFix(1,1,0,0) = Cel_hom_iso_muFix(0,0,2,2) = Cel_hom_iso_muFix(2,2,0,0) = Cel_hom_iso_muFix(2,2,1,1) = Cel_hom_iso_muFix(1,1,2,2) 
                             = nuel_hom/(1.-2.*nuel_hom);
  Cel_hom_iso_muFix(0,1,0,1) = Cel_hom_iso_muFix(0,1,1,0) = Cel_hom_iso_muFix(1,0,1,0) = Cel_hom_iso_muFix(1,0,0,1) 
                             = Cel_hom_iso_muFix(0,2,0,2) = Cel_hom_iso_muFix(0,2,2,0) = Cel_hom_iso_muFix(2,0,2,0) = Cel_hom_iso_muFix(2,0,0,2) 
                             = Cel_hom_iso_muFix(1,2,1,2) = Cel_hom_iso_muFix(1,2,2,1) = Cel_hom_iso_muFix(2,1,1,2) = Cel_hom_iso_muFix(2,1,2,1) 
                             = 1./2.;
  Cel_hom_iso_muFix *= 2.*Gel_hom;
  
  fullMatrix<double> Cel_hom_mat(6,6), Cel_hom_iso_mat(6,6), Cel_hom_iso_muFix_mat(6,6);
  STensorOperation::fromSTensor43ToFullMatrix(Cel_hom,Cel_hom_mat);
  STensorOperation::fromSTensor43ToFullMatrix(Cel_hom_iso,Cel_hom_iso_mat);
  STensorOperation::fromSTensor43ToFullMatrix(Cel_hom_iso_muFix,Cel_hom_iso_muFix_mat);
  fullMatrix<double> Cel_hom_transverseIso_mat(6,6);
  STensorOperation::fromSTensor43ToFullMatrix(Cel_hom_transverseIso,Cel_hom_transverseIso_mat);
  
  Cel_hom_iso.print("CEL_ISO");
  //Cel_hom_iso_muFix.print("CEL_ISO_muFix");
  Cel_hom_transverseIso.print("CEL_TRANSVERSEISO");
  
  printf("start writing elastic stiffness tensor to file\n");
  std::string CelFileName = "ElasticStiffnessHom.csv";
  FILE* fCel = fopen(CelFileName.c_str(),"w");
  for (int i=0; i<6; i++)
  {
    for (int j=0; j<6; j++)
    {
      std::string strCij = "C_el_"+std::to_string(i)+std::to_string(j);
      if(i==5 and j==5)
      {
        fprintf(fCel, "%s\n",strCij.c_str());
      }
      else
      {      
        fprintf(fCel, "%s ",strCij.c_str());
      }
    }
  }
  for (int i=0; i<6; i++)
  {
    for (int j=0; j<6; j++)
    {
      if(i==5 and j==5)
      {
      fprintf(fCel, "%.16g\n",Cel_hom_mat(i,j));
      }
      else{
      fprintf(fCel, "%.16g ",Cel_hom_mat(i,j));
      }
    }
  }
  fclose(fCel);
  printf("done writing elastic stiffness tensor to file\n");
  
  printf("start writing isotropic elastic stiffness tensor to file\n");
  std::string CelIsoFileName = "ElasticStiffnessHomIso.csv";
  FILE* fCelIso = fopen(CelIsoFileName.c_str(),"w");
  for (int i=0; i<6; i++)
  {
    for (int j=0; j<6; j++)
    {
      std::string strCij = "C_el_"+std::to_string(i)+std::to_string(j);
      if(i==5 and j==5)
      {
        fprintf(fCelIso, "%s\n",strCij.c_str());
      }
      else
      {      
        fprintf(fCelIso, "%s ",strCij.c_str());
      }
    }
  }
  for (int i=0; i<6; i++)
  {
    for (int j=0; j<6; j++)
    {
      if(i==5 and j==5)
      {
      fprintf(fCelIso, "%.16g\n",Cel_hom_iso_mat(i,j));
      }
      else{
      fprintf(fCelIso, "%.16g ",Cel_hom_iso_mat(i,j));
      }
    }
  }
  fclose(fCelIso);
  printf("done writing isotropic elastic stiffness tensor to file\n");
  
  printf("start writing isotropic elastic stiffness tensor with G fix to file\n");
  std::string CelIsoMuFixFileName = "ElasticStiffnessHomIso_muFix.csv";
  FILE* fCelIsoMuFix = fopen(CelIsoMuFixFileName.c_str(),"w");
  for (int i=0; i<6; i++)
  {
    for (int j=0; j<6; j++)
    {
      std::string strCij = "C_el_"+std::to_string(i)+std::to_string(j);
      if(i==5 and j==5)
      {
        fprintf(fCelIsoMuFix, "%s\n",strCij.c_str());
      }
      else
      {      
        fprintf(fCelIsoMuFix, "%s ",strCij.c_str());
      }
    }
  }
  for (int i=0; i<6; i++)
  {
    for (int j=0; j<6; j++)
    {
      if(i==5 and j==5)
      {
      fprintf(fCelIsoMuFix, "%.16g\n",Cel_hom_iso_muFix_mat(i,j));
      }
      else{
      fprintf(fCelIsoMuFix, "%.16g ",Cel_hom_iso_muFix_mat(i,j));
      }
    }
  }
  fclose(fCelIsoMuFix);
  printf("done writing isotropic elastic stiffness tensor with G fix to file\n");
  
  printf("start writing transverse isotropic elastic stiffness tensor to file\n");
  std::string CelTransverseIsoFileName = "ElasticStiffnessHomTransverseIso.csv";
  FILE* fCelTransverseIso = fopen(CelTransverseIsoFileName.c_str(),"w");
  for (int i=0; i<6; i++)
  {
    for (int j=0; j<6; j++)
    {
      std::string strCij = "C_el_"+std::to_string(i)+std::to_string(j);
      if(i==5 and j==5)
      {
        fprintf(fCelTransverseIso, "%s\n",strCij.c_str());
      }
      else
      {      
        fprintf(fCelTransverseIso, "%s ",strCij.c_str());
      }
    }
  }
  for (int i=0; i<6; i++)
  {
    for (int j=0; j<6; j++)
    {
      if(i==5 and j==5)
      {
      fprintf(fCelTransverseIso, "%.16g\n",Cel_hom_transverseIso_mat(i,j));
      }
      else{
      fprintf(fCelTransverseIso, "%.16g ",Cel_hom_transverseIso_mat(i,j));
      }
    }
  }
  fclose(fCelTransverseIso);
  printf("done writing transverse isotropic elastic stiffness tensor to file\n");
  
  if (saveToFile)
  {
    printf("start writing strain concentration tensor to file\n");
    FILE* f = fopen(strainConcentrationFileName.c_str(),"w");
    std::string str ="ELE_NUM GP_NUM MAT_NUM X Y Z VOLUME";
    for (int i=0; i<6; i++)
    {
      for (int j=0; j<6; j++)
      {
        str += " A_"+std::to_string(i)+std::to_string(j);
      }
    }
    fprintf(f,"%s\n",str.c_str());
    // write data
    for (std::map<int, std::map<int, fullMatrix<double> > >::const_iterator it = _concentrationTensorMap.begin();  it!= _concentrationTensorMap.end(); it++)
    { 
      int matNum = it->first;
      const std::map<int, fullMatrix<double> >& mapIPLaw = it->second;
      for (std::map<int, fullMatrix<double> >::const_iterator itIPMap = mapIPLaw.begin(); itIPMap!=mapIPLaw.end(); itIPMap++)
      {
        int type = itIPMap->first;
        const fullMatrix<double>& mat = itIPMap->second;
        int ele, gp;
        numericalMaterialBase::getTwoIntsFromType(type,ele,gp);
        fprintf(f,"%d %d %d",ele,gp,matNum);
        fullVector<double>& pw = _mapGaussPoints[type];
        fprintf(f," %.16g %.16g %.16g %.16g",pw(0),pw(1),pw(2),pw(3));
        for (int i=0; i<6; i++)
        {
          for (int j=0; j<6; j++)
          {
            fprintf(f, " %.16g",mat(i,j));
          }
        }
        fprintf(f, "\n");
      }
    }
    fclose(f);
    printf("done writing strain concentration tensor to file\n");
  }
  if (saveLocalOrientationDataToFile)
  {
    printf("start writing local orientation to file\n");
    FILE* f = fopen(localOrientationFileName.c_str(),"w");
    std::string str ="ELE_NUM GP_NUM MAT_NUM E1 E2 E3";
    fprintf(f,"%s\n",str.c_str());
    
    for (std::map<int, fullVector<double> >::const_iterator itOriMap = _mapLocalOrientation.begin(); itOriMap!=_mapLocalOrientation.end(); itOriMap++)
    {
      int type = itOriMap->first;
      int ele, gp;
      numericalMaterialBase::getTwoIntsFromType(type,ele,gp);
      int& matNum = _materialMap[type];    
      const fullVector<double>& eulerLoc = itOriMap->second;  
      fprintf(f,"%d %d %d %.16g %.16g %.16g",ele,gp,matNum,eulerLoc(0),eulerLoc(1),eulerLoc(2));
      fprintf(f, "\n");
    }
    fclose(f);
    printf("done writing local orientation to file\n");
  }
};

void Clustering::loadStrainConcentrationDataFromFile(const std::string fileName)
{
  clearAllClusterData();
  _concentrationTensorMap.clear();
  _directMapOfStrainConcentration.clear();
  _materialMap.clear();
  _mapElementNumberOfGPs.clear();
  _mapGaussPoints.clear();
  
  FILE* fp = fopen(fileName.c_str(),"r");
  if (fp !=NULL) {
    printf("start reading file: %s\n",fileName.c_str());
    if(!feof(fp))
    {
      char what[256];
      int ok = fscanf(fp, "%s", what);
      printf("what = %s\n",what);
      ok = fscanf(fp, "%s", what);
      printf("what = %s\n",what);
      ok = fscanf(fp, "%s", what);
      printf("what = %s\n",what);
      ok = fscanf(fp, "%s", what);
      printf("what = %s\n",what);
      ok = fscanf(fp, "%s", what);
      printf("what = %s\n",what);
      ok = fscanf(fp, "%s", what);
      printf("what = %s\n",what);
      ok = fscanf(fp, "%s", what);
      printf("what = %s\n",what);
      for (int i=0; i<6; i++)
      {
        for (int j=0; j<6; j++)
        {
          ok = fscanf(fp, "%s", what);
          printf("what = %s\n",what);
        }
      }
      if(strcmp(what, "A_55"))
      {
        Msg::Error("file format is not correct\n");
        Msg::Exit(0);
      }
    }
    while (1){
      if(feof(fp))
        break;
      int ele, gp, matNum;
      int type = 0;
      if (fscanf(fp, "%d %d %d", &ele,&gp,&matNum)==3)
      {
        type = numericalMaterialBase::createTypeWithTwoInts(ele,gp);
      }
      fullVector<double>& val = _mapGaussPoints[type];
      val.resize(4);
      int ok = fscanf(fp, "%lf %lf %lf %lf", &(val.getDataPtr()[0]),&(val.getDataPtr()[1]),&(val.getDataPtr()[2]),&(val.getDataPtr()[3]));
      static fullMatrix<double> matTensor(6,6);
      for (int i=0; i<6; i++)
      {
        for (int j=0; j<6; j++)
        {
          double val=0;
          if (fscanf(fp,"%lf",&val)==1)
            matTensor(i,j) = val;
        }
      }
      if (type >0 && matNum > 0)
      {
        std::map<int,int>::const_iterator itGP = _mapElementNumberOfGPs.find(ele);
        if (itGP == _mapElementNumberOfGPs.end())
        {
          _mapElementNumberOfGPs[ele] = 0;
        }
        _mapElementNumberOfGPs[ele]++;
        std::map<int, fullMatrix<double> >& mapGPLaw = _concentrationTensorMap[matNum];
        mapGPLaw[type] = matTensor;
        _directMapOfStrainConcentration[type] = &mapGPLaw[type];
        _materialMap[type] = matNum;
        //printf("type = %d\n",type);
        //matTensor.print("mat");
      }
    }
    printf("done reading file: %s\n",fileName.c_str());
    fclose(fp);
  }
  else
  {
    Msg::Error("File %s does not exist !!!",fileName.c_str());
    Msg::Exit(0);
  }
}




void Clustering::loadLocalOrientationDataFromFile(const std::string fileNameLocalOrientationData)
{
  clearAllClusterData();
  _mapLocalOrientation.clear();
  _localOrientationVectorMap.clear();
  
  FILE* fp = fopen(fileNameLocalOrientationData.c_str(),"r");
  if (fp !=NULL) {
    printf("start reading file: %s\n",fileNameLocalOrientationData.c_str());
    if(!feof(fp))
    {
      char what[256];
      int ok = fscanf(fp, "%s", what);
      printf("what = %s\n",what);
      ok = fscanf(fp, "%s", what);
      printf("what = %s\n",what);
      ok = fscanf(fp, "%s", what);
      printf("what = %s\n",what);
      for (int i=0; i<3; i++)
      {
        ok = fscanf(fp, "%s", what);
        printf("what = %s\n",what);
      }
      if(strcmp(what, "E3"))
      {
        Msg::Error("file format is not correct\n");
        Msg::Exit(0);
      }
    }
    while (1){
      if(feof(fp))
        break;
      int ele, gp, matNum;
      int type = 0;
      if (fscanf(fp, "%d %d %d", &ele,&gp,&matNum)==3)
      {
        type = numericalMaterialBase::createTypeWithTwoInts(ele,gp);
      }
      static fullVector<double> vec(3);
      for (int i=0; i<3; i++)
      {
        double val=0;
        if (fscanf(fp,"%lf",&val)==1)
          vec(i) = val;
      }
      if (type >0 && matNum > 0)
      {
        fullVector<double>& mapLocOri = _mapLocalOrientation[type];
        mapLocOri = vec;
        
        std::map<int, fullVector<double> >& mapGPLaw = _localOrientationVectorMap[matNum];
        mapGPLaw[type] = vec;
      }
    }
    printf("done reading file: %s\n",fileNameLocalOrientationData.c_str());
    fclose(fp);
  }
  else
  {
    Msg::Error("File %s does not exist !!!",fileNameLocalOrientationData.c_str());
    Msg::Exit(0);
  }
}






void Clustering::solveStrainConcentrationInelastic(nonLinearMechSolver& solverInelastic1, bool saveToFile,
                                                   const std::string PlasticStrainConcentrationFileName,
                                                   const std::string InelasticStrainConcentrationFileName,
                                                   const std::string TotalStrainConcentrationFileName,
                                                   const std::string PlasticEqStrainConcentrationFileName)
{ 
  clearAllClusterData();
  
  // compute from solver
  _directMapOfPlasticStrainConcentration.clear();
  _plasticStrainConcentrationVectorMap.clear();
  
  _directMapOfInelasticStrainConcentration.clear();
  _inelasticStrainConcentrationVectorMap.clear();
  
  _directMapOfTotalStrainConcentration.clear();
  _totalStrainConcentrationVectorMap.clear();
  
  _directMapOfPlasticEqStrainConcentration.clear();
  _plasticEqStrainConcentrationVectorMap.clear();
  
  _SVMPlasticVectorMap.clear();
  _directMapOfSVMPlastic.clear();
  
  _eigenstressEqVectorMap.clear();
  _directMapOfEigenstressEq.clear();

  _materialMap.clear();
  _mapElementNumberOfGPs.clear();
  _mapGaussPoints.clear();

  // done
  int dim = solverInelastic1.getDim();
  std::map<int, STensor3> allStrain;
  //std::map<int, std::map<int,piecewiseScalarFunction>> allStrain;
  double fact_BC = 2.e-2;
  strainForInelasticLoadingCases(dim,fact_BC,allStrain);
  //strainForInelasticLoadingCasesPath(dim,fact_BC,allStrain);
  int numberCases = allStrain.size();
  for (std::map<int, STensor3>::const_iterator it = allStrain.begin(); it!= allStrain.end(); it++)
  //for (std::map<int, std::map<int,piecewiseScalarFunction>>::const_iterator it = allStrain.begin(); it!= allStrain.end(); it++)
  {    
    int i = it->first;
    // crate a new solver for each deformation mode due to inelastictity
    nonLinearMechSolver* solverInelastic = solverInelastic1.clone(solverInelastic1.getMeshFileName(),solverInelastic1.getTag());
    // specify all output file with mode index, i, for example, the files E_i_0_* will be generated
    
    solverInelastic->setControlType(0);
    solverInelastic->stiffnessModification(true);
    solverInelastic->iterativeProcedure(true);
    solverInelastic->setMessageView(true);
    solverInelastic->setMicroProblemIndentification(i+1,0);
    
    // prescribe homogenized strain
    const STensor3& F = it->second;
    solverInelastic->getMicroBC()->setDeformationGradient(F);
    /*const std::map<int,piecewiseScalarFunction>& Ffct = it->second;
    const piecewiseScalarFunction fct00 = Ffct.find(0)->second;
    const piecewiseScalarFunction fct11 = Ffct.find(1)->second;
    const piecewiseScalarFunction fct01 = Ffct.find(2)->second;
    STensor3 F(1.);
    if(dim==2)
    {
      F(0,0)=F(1,1) = 2.;
      F(0,1)=F(1,0) = 1.;
    }

    solverInelastic->getMicroBC()->setDeformationGradient(F);
    solverInelastic->getMicroBC()->setPathFunctionDeformationGradient(0,0,fct00);
    solverInelastic->getMicroBC()->setPathFunctionDeformationGradient(1,1,fct11);
    solverInelastic->getMicroBC()->setPathFunctionDeformationGradient(0,1,fct01);
    solverInelastic->getMicroBC()->setPathFunctionDeformationGradient(1,0,fct01);*/
    
    Msg::Info("inelastic loading case: %d",i);
    F.print("strain tensor: ");
    
    // deactivate tangent averaging flag
    solverInelastic->tangentAveragingFlag(false);
    // solve problem
    solverInelastic->solve();
    // get data from IP field
    const IPField* ipf = solverInelastic->getIPField();
    const AllIPState* aips = ipf->getAips();
    const std::vector<partDomain*>& domainVector = *(solverInelastic->getDomainVector());
    for (int idom=0; idom < domainVector.size(); idom++)
    {
      const partDomain* dom = domainVector[idom];
      const materialLaw* matLaw = dom->getMaterialLaw();
      std::map<int, fullVector<double> >& mapGPPlasticStrainLaw = _plasticStrainConcentrationVectorMap[matLaw->getNum()];
      std::map<int, fullVector<double> >& mapGPInelasticStrainLaw = _inelasticStrainConcentrationVectorMap[matLaw->getNum()];
      std::map<int, fullVector<double> >& mapGPTotalStrainLaw = _totalStrainConcentrationVectorMap[matLaw->getNum()];
      std::map<int, fullVector<double> >& mapGPPlasticEqStrainLaw = _plasticEqStrainConcentrationVectorMap[matLaw->getNum()];
      IntPt* GP;
      
      for (elementGroup::elementContainer::const_iterator ite = dom->element_begin(); ite!= dom->element_end(); ite++)
      {
        MElement* ele = ite->second;
        int npts = dom->getBulkGaussIntegrationRule()->getIntPoints(ele,&GP);
        const AllIPState::ipstateElementContainer *vips = aips->getIPstate(ele->getNum());
        // data of Gauss points and element numbers are set at the first solution,
        if (it == allStrain.begin())
        {
          _mapElementNumberOfGPs[ele->getNum()] = npts;
        }

        for (int j=0; j< npts; j++)
        {    
          // data of Gauss points and element numbers are set at the first solution,
          //
          // create a unique number of element number and GP number
          int type = numericalMaterialBase::createTypeWithTwoInts(ele->getNum(),j);
          if (it == allStrain.begin())
          {
            _materialMap[type] = matLaw->getNum();
            // store gauss point
            double u = GP[j].pt[0]; double v = GP[j].pt[1]; double w = GP[j].pt[2];
            double weight = GP[j].weight;
            double detJ = ele->getJacobianDeterminant(u,v,w);
            SPoint3 pnt;
            ele->pnt(u,v,w,pnt);
            fullVector<double>& vec = _mapGaussPoints[type];
            vec.resize(4);
            vec(0) =pnt.x();
            vec(1) =pnt.y();
            vec(2) =pnt.z();
            vec(3) =weight*detJ;
          }
          
          // strain at Gauss point
          const IPVariable* ipv = (*vips)[j]->getState(IPStateBase::current);
          const ipFiniteStrain* ipvf = dynamic_cast<const ipFiniteStrain*>(ipv);

          // deformation gradient can get directly from ipFiniteStrain
          const STensor3& Flocal = ipvf->getConstRefToDeformationGradient();          
          // local small strain measure
          static STensor3 eps;
          static STensor3 I(1.);
          for (int ii =0; ii<3; ii++)
          {
            for (int jj=0; jj<3; jj++)
            {
              eps(ii,jj) = 0.5*(Flocal(ii,jj)+Flocal(jj,ii)) - I(ii,jj);
            }
          }
          STensor3 plasticStrain_local;
          plasticStrain_local(0,0) = ipv->get(IPField::FP_XX)-1.;
          plasticStrain_local(1,1) = ipv->get(IPField::FP_YY)-1.;
          plasticStrain_local(2,2) = ipv->get(IPField::FP_ZZ)-1.;
          plasticStrain_local(0,1) = ipv->get(IPField::FP_XY);
          plasticStrain_local(0,2) = ipv->get(IPField::FP_XZ);
          plasticStrain_local(1,2) = ipv->get(IPField::FP_YZ);
          
          STensor3 eigenstrain_local;
          eigenstrain_local(0,0) = ipv->get(IPField::EIGENSTRAIN_XX);
          eigenstrain_local(1,1) = ipv->get(IPField::EIGENSTRAIN_YY);
          eigenstrain_local(2,2) = ipv->get(IPField::EIGENSTRAIN_ZZ);
          eigenstrain_local(0,1) = ipv->get(IPField::EIGENSTRAIN_XY);
          eigenstrain_local(0,2) = ipv->get(IPField::EIGENSTRAIN_XZ);
          eigenstrain_local(1,2) = ipv->get(IPField::EIGENSTRAIN_YZ);

          double plasticEqStrain_local = ipv->get(IPField::PLASTICSTRAIN);
          double stress_eq_local = ipv->get(IPField::SVM);
          double eigenstress_eq_local = ipv->get(IPField::EQUIVALENT_EIGENSTRESS);
          
          // save to map
          if (mapGPPlasticEqStrainLaw.find(type) == mapGPPlasticEqStrainLaw.end())
          {
            // resize if it not exist before 
            fullVector<double>& veclocPlasticStrain = mapGPPlasticStrainLaw[type];
            veclocPlasticStrain.resize(36,true);
            _directMapOfPlasticStrainConcentration[type] = &veclocPlasticStrain;
            
            fullVector<double>& veclocInelasticStrain = mapGPInelasticStrainLaw[type];
            veclocInelasticStrain.resize(36,true);
            _directMapOfInelasticStrainConcentration[type] = &veclocInelasticStrain;
            
            fullVector<double>& veclocTotalStrain = mapGPTotalStrainLaw[type];
            veclocTotalStrain.resize(36,true);
            _directMapOfTotalStrainConcentration[type] = &veclocTotalStrain;
            
            fullVector<double>& veclocPlasticEqStrain = mapGPPlasticEqStrainLaw[type];
            veclocPlasticEqStrain.resize(6,true);
            _directMapOfPlasticEqStrainConcentration[type] = &veclocPlasticEqStrain;
          }
          fullVector<double>& veclocPlasticStrain = mapGPPlasticStrainLaw[type];
          veclocPlasticStrain(i*6+0)  = plasticStrain_local(0,0); // A(00)(00 if i==0, 11 if i==1, 22 if i ==2)
          veclocPlasticStrain(i*6+1)  = plasticStrain_local(1,1); // A(11)(00 if i==0, 11 if i==1, 22 if i ==2)
          veclocPlasticStrain(i*6+2)  = plasticStrain_local(0,1); // A(01)(00 if i==0, 11 if i==1, 22 if i ==2)
          veclocPlasticStrain(i*6+3)  = plasticStrain_local(2,2); // A(22)(00 if i==0, 11 if i==1, 22 if i ==2)
          veclocPlasticStrain(i*6+4)  = plasticStrain_local(0,2); // A(02)(00 if i==0, 11 if i==1, 22 if i ==2)
          veclocPlasticStrain(i*6+5)  = plasticStrain_local(1,2); // A(12)(00 if i==0, 11 if i==1, 22 if i ==2) 
          
          fullVector<double>& veclocInelasticStrain = mapGPInelasticStrainLaw[type];
          veclocInelasticStrain(i*6+0)  = eigenstrain_local(0,0); // A(00)(00 if i==0, 11 if i==1, 22 if i ==2)
          veclocInelasticStrain(i*6+1)  = eigenstrain_local(1,1); // A(11)(00 if i==0, 11 if i==1, 22 if i ==2)
          veclocInelasticStrain(i*6+2)  = eigenstrain_local(0,1); // A(01)(00 if i==0, 11 if i==1, 22 if i ==2)
          veclocInelasticStrain(i*6+3)  = eigenstrain_local(2,2); // A(22)(00 if i==0, 11 if i==1, 22 if i ==2)
          veclocInelasticStrain(i*6+4)  = eigenstrain_local(0,2); // A(02)(00 if i==0, 11 if i==1, 22 if i ==2)
          veclocInelasticStrain(i*6+5)  = eigenstrain_local(1,2); // A(12)(00 if i==0, 11 if i==1, 22 if i ==2)     
          
          fullVector<double>& veclocTotalStrain = mapGPTotalStrainLaw[type];
          veclocTotalStrain(i*6+0)  = eps(0,0); // A(00)(00 if i==0, 11 if i==1, 22 if i ==2)
          veclocTotalStrain(i*6+1)  = eps(1,1); // A(11)(00 if i==0, 11 if i==1, 22 if i ==2)
          veclocTotalStrain(i*6+2)  = eps(0,1); // A(01)(00 if i==0, 11 if i==1, 22 if i ==2)
          veclocTotalStrain(i*6+3)  = eps(2,2); // A(22)(00 if i==0, 11 if i==1, 22 if i ==2)
          veclocTotalStrain(i*6+4)  = eps(0,2); // A(02)(00 if i==0, 11 if i==1, 22 if i ==2)
          veclocTotalStrain(i*6+5)  = eps(1,2); // A(12)(00 if i==0, 11 if i==1, 22 if i ==2)
  
          fullVector<double>& veclocPlasticEqStrain = mapGPPlasticEqStrainLaw[type];
          veclocPlasticEqStrain(i) = plasticEqStrain_local;
        }
      }
    }
    delete solverInelastic;
  };
 
  Msg::Info("done estimating concentration tensors in inelastic properties");
  if (saveToFile)
  {
    printf("start writing plastic strain concentration tensor to file\n");
    FILE* f = fopen(PlasticStrainConcentrationFileName.c_str(),"w");
    std::string str ="ELE_NUM GP_NUM MAT_NUM X Y Z VOLUME";
    for (int i=0; i<36; i++)
    {
        str += " A_pl_"+std::to_string(i);
    }
    fprintf(f,"%s\n",str.c_str());
    // write data
    for (std::map<int, std::map<int, fullVector<double> > >::const_iterator it = _plasticStrainConcentrationVectorMap.begin();  it!= _plasticStrainConcentrationVectorMap.end(); it++)
    { 
      int matNum = it->first;
      const std::map<int, fullVector<double> >& mapIPLaw = it->second;
      for (std::map<int, fullVector<double> >::const_iterator itIPMap = mapIPLaw.begin(); itIPMap!=mapIPLaw.end(); itIPMap++)
      {
        int type = itIPMap->first;
        const fullVector<double>& vec = itIPMap->second;
        int ele, gp;
        numericalMaterialBase::getTwoIntsFromType(type,ele,gp);
        fprintf(f,"%d %d %d",ele,gp,matNum);
        fullVector<double>& pw = _mapGaussPoints[type];
        fprintf(f," %.16g %.16g %.16g %.16g",pw(0),pw(1),pw(2),pw(3));
        for (int i=0; i<36; i++)
        {
          fprintf(f, " %.16g",vec(i));
        }
        fprintf(f, "\n");
      }
    }
    fclose(f);
    printf("done writing plastic strain concentration tensor to file\n");  
  
    printf("start writing total strain concentration tensor to file\n");
    FILE* f2 = fopen(TotalStrainConcentrationFileName.c_str(),"w");
    std::string str2 ="ELE_NUM GP_NUM MAT_NUM X Y Z VOLUME";
    for (int i=0; i<36; i++)
    {
        str2 += " A_tot_"+std::to_string(i);
    }
    fprintf(f2,"%s\n",str2.c_str());
    // write data
    for (std::map<int, std::map<int, fullVector<double> > >::const_iterator it = _totalStrainConcentrationVectorMap.begin();  it!= _totalStrainConcentrationVectorMap.end(); it++)
    { 
      int matNum = it->first;
      const std::map<int, fullVector<double> >& mapIPLaw = it->second;
      for (std::map<int, fullVector<double> >::const_iterator itIPMap = mapIPLaw.begin(); itIPMap!=mapIPLaw.end(); itIPMap++)
      {
        int type = itIPMap->first;
        const fullVector<double>& vec = itIPMap->second;
        int ele, gp;
        numericalMaterialBase::getTwoIntsFromType(type,ele,gp);
        fprintf(f2,"%d %d %d",ele,gp,matNum);
        fullVector<double>& pw = _mapGaussPoints[type];
        fprintf(f2," %.16g %.16g %.16g %.16g",pw(0),pw(1),pw(2),pw(3));
        for (int i=0; i<36; i++)
        {
          fprintf(f2, " %.16g",vec(i));
        }
        fprintf(f2, "\n");
      }
    }
    fclose(f2);
    printf("done writing total strain concentration tensor to file\n");  
    
    printf("start writing eigenstrain concentration tensor to file\n");
    FILE* f3 = fopen(InelasticStrainConcentrationFileName.c_str(),"w");
    std::string str3 ="ELE_NUM GP_NUM MAT_NUM X Y Z VOLUME";
    for (int i=0; i<36; i++)
    {
        str3 += " A_in_"+std::to_string(i);
    }
    fprintf(f3,"%s\n",str3.c_str());
    // write data
    for (std::map<int, std::map<int, fullVector<double> > >::const_iterator it = _inelasticStrainConcentrationVectorMap.begin();  it!= _inelasticStrainConcentrationVectorMap.end(); it++)
    { 
      int matNum = it->first;
      const std::map<int, fullVector<double> >& mapIPLaw = it->second;
      for (std::map<int, fullVector<double> >::const_iterator itIPMap = mapIPLaw.begin(); itIPMap!=mapIPLaw.end(); itIPMap++)
      {
        int type = itIPMap->first;
        const fullVector<double>& vec = itIPMap->second;
        int ele, gp;
        numericalMaterialBase::getTwoIntsFromType(type,ele,gp);
        fprintf(f3,"%d %d %d",ele,gp,matNum);
        fullVector<double>& pw = _mapGaussPoints[type];
        fprintf(f3," %.16g %.16g %.16g %.16g",pw(0),pw(1),pw(2),pw(3));
        for (int i=0; i<36; i++)
        {
          fprintf(f3, " %.16g",vec(i));
        }
        fprintf(f3, "\n");
      }
    }
    fclose(f3);
    printf("done writing eigenstrain concentration tensor to file\n");  
  
    printf("start writing plastic eq. strain concentration to file\n");
    FILE* fpl = fopen(PlasticEqStrainConcentrationFileName.c_str(),"w");
    std::string strpl ="ELE_NUM GP_NUM MAT_NUM X Y Z VOLUME";
    for (int i=0; i<6; i++)
    {
      strpl += " a_pl_"+std::to_string(i);
    }
    fprintf(fpl,"%s\n",strpl.c_str());
    // write data
    for (std::map<int, std::map<int, fullVector<double> > >::const_iterator it = _plasticEqStrainConcentrationVectorMap.begin();  it!= _plasticEqStrainConcentrationVectorMap.end(); it++)
    { 
      int matNum = it->first;
      const std::map<int, fullVector<double> >& mapIPLaw = it->second;
      for (std::map<int, fullVector<double> >::const_iterator itIPMap = mapIPLaw.begin(); itIPMap!=mapIPLaw.end(); itIPMap++)
      {
        int type = itIPMap->first;
        const fullVector<double>& vec = itIPMap->second;
        int ele, gp;
        numericalMaterialBase::getTwoIntsFromType(type,ele,gp);
        fprintf(fpl,"%d %d %d",ele,gp,matNum);
        fullVector<double>& pw = _mapGaussPoints[type];
        fprintf(fpl," %.16g %.16g %.16g %.16g",pw(0),pw(1),pw(2),pw(3));
        for (int i=0; i<6; i++)
        {
          fprintf(fpl, " %.16g",vec(i));
        }
        fprintf(fpl, "\n");
      }
    }
    fclose(fpl);
    printf("done writing plastic eq. strain concentration to file\n");    
  }
};


void Clustering::loadPlasticStrainConcentrationDataFromFile(const std::string fileNamePlastic)
{
  clearAllClusterData();
  _plasticStrainConcentrationVectorMap.clear();
  _directMapOfPlasticStrainConcentration.clear();
  
  FILE* fpPlastic = fopen(fileNamePlastic.c_str(),"r");
  if (fpPlastic !=NULL) {
    printf("start reading file: %s\n",fileNamePlastic.c_str());
    if(!feof(fpPlastic))
    {
      char what[256];
      int ok = fscanf(fpPlastic, "%s", what);
      printf("what = %s\n",what);
      ok = fscanf(fpPlastic, "%s", what);
      printf("what = %s\n",what);
      ok = fscanf(fpPlastic, "%s", what);
      printf("what = %s\n",what);
      ok = fscanf(fpPlastic, "%s", what);
      printf("what = %s\n",what);
      ok = fscanf(fpPlastic, "%s", what);
      printf("what = %s\n",what);
      ok = fscanf(fpPlastic, "%s", what);
      printf("what = %s\n",what);
      ok = fscanf(fpPlastic, "%s", what);
      printf("what = %s\n",what);
      for (int i=0; i<36; i++)
      {
        ok = fscanf(fpPlastic, "%s", what);
        printf("what = %s\n",what);
      }
      if(strcmp(what, "A_pl_35"))
      {
        Msg::Error("file format is not correct\n");
        Msg::Exit(0);
      }
    }
    while (1){
      if(feof(fpPlastic))
        break;
      int ele, gp, matNum;
      int type = 0;
      if (fscanf(fpPlastic, "%d %d %d", &ele,&gp,&matNum)==3)
      {
        type = numericalMaterialBase::createTypeWithTwoInts(ele,gp);
      }
      fullVector<double> val;
      val.resize(4);
      int ok = fscanf(fpPlastic, "%lf %lf %lf %lf", &(val.getDataPtr()[0]),&(val.getDataPtr()[1]),&(val.getDataPtr()[2]),&(val.getDataPtr()[3]));
      static fullVector<double> vec(36);
      for (int i=0; i<36; i++)
      {
        double val=0;
        if (fscanf(fpPlastic,"%lf",&val)==1)
          vec(i) = val;
      }
      if (type >0 && matNum > 0)
      {
        std::map<int, fullVector<double> >& mapGPLaw = _plasticStrainConcentrationVectorMap[matNum];
        mapGPLaw[type] = vec;
        _directMapOfPlasticStrainConcentration[type] = &mapGPLaw[type];
      }
    }
    printf("done reading file: %s\n",fileNamePlastic.c_str());
    fclose(fpPlastic);
  }
  else
  {
    Msg::Error("File %s does not exist !!!",fileNamePlastic.c_str());
    Msg::Exit(0);
  }
}

void Clustering::loadInelasticStrainConcentrationDataFromFile(const std::string fileNamePlastic)
{
  clearAllClusterData();
  _inelasticStrainConcentrationVectorMap.clear();
  _directMapOfInelasticStrainConcentration.clear();
  
  FILE* fpPlastic = fopen(fileNamePlastic.c_str(),"r");
  if (fpPlastic !=NULL) {
    printf("start reading file: %s\n",fileNamePlastic.c_str());
    if(!feof(fpPlastic))
    {
      char what[256];
      int ok = fscanf(fpPlastic, "%s", what);
      printf("what = %s\n",what);
      ok = fscanf(fpPlastic, "%s", what);
      printf("what = %s\n",what);
      ok = fscanf(fpPlastic, "%s", what);
      printf("what = %s\n",what);
      ok = fscanf(fpPlastic, "%s", what);
      printf("what = %s\n",what);
      ok = fscanf(fpPlastic, "%s", what);
      printf("what = %s\n",what);
      ok = fscanf(fpPlastic, "%s", what);
      printf("what = %s\n",what);
      ok = fscanf(fpPlastic, "%s", what);
      printf("what = %s\n",what);
      for (int i=0; i<36; i++)
      {
        ok = fscanf(fpPlastic, "%s", what);
        printf("what = %s\n",what);
      }
      if(strcmp(what, "A_in_35"))
      {
        Msg::Error("file format is not correct\n");
        Msg::Exit(0);
      }
    }
    while (1){
      if(feof(fpPlastic))
        break;
      int ele, gp, matNum;
      int type = 0;
      if (fscanf(fpPlastic, "%d %d %d", &ele,&gp,&matNum)==3)
      {
        type = numericalMaterialBase::createTypeWithTwoInts(ele,gp);
      }
      fullVector<double> val;
      val.resize(4);
      int ok = fscanf(fpPlastic, "%lf %lf %lf %lf", &(val.getDataPtr()[0]),&(val.getDataPtr()[1]),&(val.getDataPtr()[2]),&(val.getDataPtr()[3]));
      static fullVector<double> vec(36);
      for (int i=0; i<36; i++)
      {
        double val=0;
        if (fscanf(fpPlastic,"%lf",&val)==1)
          vec(i) = val;
      }
      if (type >0 && matNum > 0)
      {
        std::map<int, fullVector<double> >& mapGPLaw = _inelasticStrainConcentrationVectorMap[matNum];
        mapGPLaw[type] = vec;
        _directMapOfInelasticStrainConcentration[type] = &mapGPLaw[type];
      }
    }
    printf("done reading file: %s\n",fileNamePlastic.c_str());
    fclose(fpPlastic);
  }
  else
  {
    Msg::Error("File %s does not exist !!!",fileNamePlastic.c_str());
    Msg::Exit(0);
  }
}

void Clustering::loadTotalStrainConcentrationDataFromFile(const std::string fileNameTotal)
{
  clearAllClusterData();
  _totalStrainConcentrationVectorMap.clear();
  _directMapOfTotalStrainConcentration.clear();
  
  FILE* fpPlastic = fopen(fileNameTotal.c_str(),"r");
  if (fpPlastic !=NULL) {
    printf("start reading file: %s\n",fileNameTotal.c_str());
    if(!feof(fpPlastic))
    {
      char what[256];
      int ok = fscanf(fpPlastic, "%s", what);
      printf("what = %s\n",what);
      ok = fscanf(fpPlastic, "%s", what);
      printf("what = %s\n",what);
      ok = fscanf(fpPlastic, "%s", what);
      printf("what = %s\n",what);
      ok = fscanf(fpPlastic, "%s", what);
      printf("what = %s\n",what);
      ok = fscanf(fpPlastic, "%s", what);
      printf("what = %s\n",what);
      ok = fscanf(fpPlastic, "%s", what);
      printf("what = %s\n",what);
      ok = fscanf(fpPlastic, "%s", what);
      printf("what = %s\n",what);
      for (int i=0; i<36; i++)
      {
        ok = fscanf(fpPlastic, "%s", what);
        printf("what = %s\n",what);
      }
      if(strcmp(what, "A_tot_35"))
      {
        Msg::Error("file format is not correct\n");
        Msg::Exit(0);
      }
    }
    while (1){
      if(feof(fpPlastic))
        break;
      int ele, gp, matNum;
      int type = 0;
      if (fscanf(fpPlastic, "%d %d %d", &ele,&gp,&matNum)==3)
      {
        type = numericalMaterialBase::createTypeWithTwoInts(ele,gp);
      }
      fullVector<double> val;
      val.resize(4);
      int ok = fscanf(fpPlastic, "%lf %lf %lf %lf", &(val.getDataPtr()[0]),&(val.getDataPtr()[1]),&(val.getDataPtr()[2]),&(val.getDataPtr()[3]));
      static fullVector<double> vec(36);
      for (int i=0; i<36; i++)
      {
        double val=0;
        if (fscanf(fpPlastic,"%lf",&val)==1)
          vec(i) = val;
      }
      if (type >0 && matNum > 0)
      {
        std::map<int, fullVector<double> >& mapGPLaw = _totalStrainConcentrationVectorMap[matNum];
        mapGPLaw[type] = vec;
        _directMapOfTotalStrainConcentration[type] = &mapGPLaw[type];
      }
    }
    printf("done reading file: %s\n",fileNameTotal.c_str());
    fclose(fpPlastic);
  }
  else
  {
    Msg::Error("File %s does not exist !!!",fileNameTotal.c_str());
    Msg::Exit(0);
  }
}



void Clustering::loadPlasticEqStrainConcentrationDataFromFile(const std::string fileNamePlasticEq)
{
  clearAllClusterData();
  _plasticEqStrainConcentrationVectorMap.clear();
  _directMapOfPlasticEqStrainConcentration.clear();
  
  FILE* fpPlasticEq = fopen(fileNamePlasticEq.c_str(),"r");
  if (fpPlasticEq !=NULL) {
    printf("start reading file: %s\n",fileNamePlasticEq.c_str());
    if(!feof(fpPlasticEq))
    {
      char what[256];
      int ok = fscanf(fpPlasticEq, "%s", what);
      printf("what = %s\n",what);
      ok = fscanf(fpPlasticEq, "%s", what);
      printf("what = %s\n",what);
      ok = fscanf(fpPlasticEq, "%s", what);
      printf("what = %s\n",what);
      ok = fscanf(fpPlasticEq, "%s", what);
      printf("what = %s\n",what);
      ok = fscanf(fpPlasticEq, "%s", what);
      printf("what = %s\n",what);
      ok = fscanf(fpPlasticEq, "%s", what);
      printf("what = %s\n",what);
      ok = fscanf(fpPlasticEq, "%s", what);
      printf("what = %s\n",what);
      for (int i=0; i<6; i++)
      {
        ok = fscanf(fpPlasticEq, "%s", what);
        printf("what = %s\n",what);
      }
      if(strcmp(what, "a_pl_5"))
      {
        Msg::Error("file format is not correct\n");
        Msg::Exit(0);
      }
    }
    while (1){
      if(feof(fpPlasticEq))
        break;
      int ele, gp, matNum;
      int type = 0;
      if (fscanf(fpPlasticEq, "%d %d %d", &ele,&gp,&matNum)==3)
      {
        type = numericalMaterialBase::createTypeWithTwoInts(ele,gp);
      }
      fullVector<double> val;
      val.resize(4);
      int ok = fscanf(fpPlasticEq, "%lf %lf %lf %lf", &(val.getDataPtr()[0]),&(val.getDataPtr()[1]),&(val.getDataPtr()[2]),&(val.getDataPtr()[3]));
      static fullVector<double> vec(6);
      for (int i=0; i<6; i++)
      {
        double val=0;
        if (fscanf(fpPlasticEq,"%lf",&val)==1)
          vec(i) = val;
      }
      if (type >0 && matNum > 0)
      {
        std::map<int, fullVector<double> >& mapGPLaw = _plasticEqStrainConcentrationVectorMap[matNum];
        mapGPLaw[type] = vec;
        _directMapOfPlasticEqStrainConcentration[type] = &mapGPLaw[type];
      }
    }
    printf("done reading file: %s\n",fileNamePlasticEq.c_str());
    fclose(fpPlasticEq);
  }
  else
  {
    Msg::Error("File %s does not exist !!!",fileNamePlasticEq.c_str());
    Msg::Exit(0);
  }
}


void Clustering::loadPlasticSVMStrainConcentrationDataFromFile(const std::string fileNamePlasticSVM)
{
  clearAllClusterData();
  _SVMPlasticVectorMap.clear();
  _directMapOfSVMPlastic.clear();
  
  FILE* fpPlasticSVM = fopen(fileNamePlasticSVM.c_str(),"r");
  if (fpPlasticSVM !=NULL) {
    printf("start reading file: %s\n",fileNamePlasticSVM.c_str());
    if(!feof(fpPlasticSVM))
    {
      char what[256];
      int ok = fscanf(fpPlasticSVM, "%s", what);
      printf("what = %s\n",what);
      ok = fscanf(fpPlasticSVM, "%s", what);
      printf("what = %s\n",what);
      ok = fscanf(fpPlasticSVM, "%s", what);
      printf("what = %s\n",what);
      ok = fscanf(fpPlasticSVM, "%s", what);
      printf("what = %s\n",what);
      ok = fscanf(fpPlasticSVM, "%s", what);
      printf("what = %s\n",what);
      ok = fscanf(fpPlasticSVM, "%s", what);
      printf("what = %s\n",what);
      ok = fscanf(fpPlasticSVM, "%s", what);
      printf("what = %s\n",what);
      for (int i=0; i<6; i++)
      {
        ok = fscanf(fpPlasticSVM, "%s", what);
        printf("what = %s\n",what);
      }
      if(strcmp(what, "SVM_5"))
      {
        Msg::Error("file format is not correct\n");
        Msg::Exit(0);
      }
    }
    while (1){
      if(feof(fpPlasticSVM))
        break;
      int ele, gp, matNum;
      int type = 0;
      if (fscanf(fpPlasticSVM, "%d %d %d", &ele,&gp,&matNum)==3)
      {
        type = numericalMaterialBase::createTypeWithTwoInts(ele,gp);
      }
      fullVector<double> val;
      val.resize(4);
      int ok = fscanf(fpPlasticSVM, "%lf %lf %lf %lf", &(val.getDataPtr()[0]),&(val.getDataPtr()[1]),&(val.getDataPtr()[2]),&(val.getDataPtr()[3]));
      static fullVector<double> vec(6);
      for (int i=0; i<6; i++)
      {
        double val=0;
        if (fscanf(fpPlasticSVM,"%lf",&val)==1)
          vec(i) = val;
      }
      if (type >0 && matNum > 0)
      {
        std::map<int, fullVector<double> >& mapGPLaw = _SVMPlasticVectorMap[matNum];
        mapGPLaw[type] = vec;
        _directMapOfSVMPlastic[type] = &mapGPLaw[type];
      }
    }
    printf("done reading file: %s\n",fileNamePlasticSVM.c_str());
    fclose(fpPlasticSVM);
  }
  else
  {
    Msg::Error("File %s does not exist !!!",fileNamePlasticSVM.c_str());
    Msg::Exit(0);
  }
}


bool Clustering::clusteringByMeshPartition(nonLinearMechSolver& solver, int nbClusters, bool onlyLawsWithDissipation, bool saveToFile, const std::string clusterFileName)
{
  //
  std::vector<partDomain*>& domainVector = *(solver.getDomainVector());
  
  for(std::vector<partDomain*>::iterator it = domainVector.begin(); it!=domainVector.end(); ++it)
  {
    partDomain *dom = *it;
    if (dom->getBulkGaussIntegrationRule() == NULL)
    {
      dom->setGaussIntegrationRule();
    }
  }
   // init cluster 
  clearAllClusterData();
  _materialMap.clear();
  
  std::map<int,int> dissipaionMat;
  
  for (int idom=0; idom < domainVector.size(); idom++)
  {
    const partDomain* dom = domainVector[idom];
    const materialLaw* mlaw =  solver.getMaterialLaw(dom->getLawNum());
    IntPt* GP;
    std::map<int, fullMatrix<double> >& mapGPLaw = _concentrationTensorMap[dom->getLawNum()];
    for (elementGroup::elementContainer::const_iterator ite = dom->element_begin(); ite!= dom->element_end(); ite++)
    {
      MElement* ele = ite->second;
      int npts = dom->getBulkGaussIntegrationRule()->getIntPoints(ele,&GP);
      _mapElementNumberOfGPs[ele->getNum()] = npts;
      
      for (int j=0; j< npts; j++)
      {
        int type = numericalMaterialBase::createTypeWithTwoInts(ele->getNum(),j);
        _materialMap[type] = dom->getLawNum();
         fullMatrix<double>& matloc = mapGPLaw[type];
        matloc.resize(6,6,true);
        _directMapOfStrainConcentration[type] = &matloc;
        //
        std::map<int,int>::const_iterator itnb = _numberOfClustersPerLaw.find(dom->getLawNum());
        if (itnb !=_numberOfClustersPerLaw.end())
        {
          if (itnb->second > 0)
          {
            dissipaionMat[type] = dom->getLawNum();
          }
        }
      }
    }
  }
  
  for (std::map<int,int>::const_iterator it = _materialMap.begin(); it != _materialMap.end(); it++)
  {
    _clusterIndex[it->first] = -1;
  }
  
  
  Msg::Info("done creating material map, dissipaionMat.size = %d",dissipaionMat.size());
  
  _totalNumberOfClusters = nbClusters;
  GModel* pModel = solver.getGModel();
  UnpartitionMesh(pModel);
  CTX::instance()->mesh.numPartitions = nbClusters;
  PartitionMesh(pModel, nbClusters);
  if (nbClusters > 1)
  {
    for (std::map<int,int>::const_iterator it = dissipaionMat.begin(); it != dissipaionMat.end(); it++)
    {
      int type = it->first;
      int elNum, gpNum;
      numericalMaterialBase::getTwoIntsFromType(type,elNum,gpNum);
      MElement* ele = pModel->getMeshElementByTag(elNum);
      _clusterIndex[it->first] = ele->getPartition()-1;
    }
  }
  else
  {
    for (std::map<int,int>::const_iterator it = dissipaionMat.begin(); it != dissipaionMat.end(); it++)
    {
      int type = it->first;
      int elNum, gpNum;
      numericalMaterialBase::getTwoIntsFromType(type,elNum,gpNum);
      MElement* ele = pModel->getMeshElementByTag(elNum);
      _clusterIndex[it->first] = 0;
    }
  }
  
  if (saveToFile)
  {
    std::vector<std::string> splitFileName = SplitFileName(clusterFileName);
    std::string fname = splitFileName[1]+"_nbClusters_"+std::to_string(_totalNumberOfClusters)+splitFileName[2];
    FILE* f = fopen(fname.c_str(),"w");
    printf("start writing cluster data to file: %s\n",fname.c_str());
    std::string str ="ELE_NUM GP_NUM CLUSTER_ID MAT_NUM";
    fprintf(f,"%s\n",str.c_str());
    // write data
    for (std::map<int, int>::const_iterator it = _clusterIndex.begin();  it!= _clusterIndex.end(); it++)
    { 
      int type = it->first;
      int clusterId = it->second;
      int ele, gp;
      numericalMaterialBase::getTwoIntsFromType(type,ele,gp);
      fprintf(f, "%d %d %d %d\n",ele,gp,clusterId,_materialMap[type]);
    }
    fclose(f);
    printf("done writing cluster data to file: %s \n",fname.c_str());
  }
  return true;
}

bool Clustering::computeClustersElasticity(int minNumberElementEachCluster, int maxNbIterations,
                                 bool saveToFile, std::string clusterFileName,
                                 bool saveSummaryFile, std::string summaryFileName,
                                 bool saveMostStrainIPFile, const std::string saveMostStrainIPFileName,
                                 bool saveClusterMeanFiberOrientationFile, const std::string saveClusterMeanFiberOrientationFileName
                                 )
{
  // init cluster 
  clearAllClusterData();
  for (std::map<int,int>::const_iterator it = _materialMap.begin(); it != _materialMap.end(); it++)
  {
    _clusterIndex[it->first] = -1;
  }

  std::map<int, int > mostStrainIP;
  
  //
  _totalNumberOfClusters = 0;
  for (std::map<int, std::map<int, fullMatrix<double> > >::const_iterator  itMat = _concentrationTensorMap.begin(); 
                      itMat != _concentrationTensorMap.end(); itMat++)
  {
    int matNum = itMat->first;
    int nbCluster = 0;
    std::map<int,int>::const_iterator itnb = _numberOfClustersPerLaw.find(matNum);
    if (itnb !=_numberOfClustersPerLaw.end())
    {
      nbCluster = itnb->second;
    }
    if (nbCluster > 0)
    {
      const std::map<int, fullMatrix<double> >& dataInMat = itMat->second;

      Msg::Info("start clustering for material %d \n number of clusters = %d, min number in each cluster = %d",matNum, nbCluster,minNumberElementEachCluster);

      std::map<int, std::map<int, const fullMatrix<double>*> > clusterInMat;

      bool ok= clutering_impls::kmean_cluster(dataInMat,nbCluster,minNumberElementEachCluster,maxNbIterations,_mapGaussPoints,clusterInMat);

      if (!ok)
      {
        return false;
      }
      for (std::map<int, std::map<int, const fullMatrix<double>*> >::const_iterator it = clusterInMat.begin(); it!= clusterInMat.end(); it++)
      {
        int clusterNum = it->first + _totalNumberOfClusters;
        const std::map<int, const fullMatrix<double>*>& cluster = it->second;
        //
        for (std::map<int, const fullMatrix<double>*>::const_iterator itset = cluster.begin(); itset != cluster.end(); itset++)
        {
          // update cluster index at each material point
          _clusterIndex[itset->first] = clusterNum;
        }
        // average concentration tensor in this cluster
        static fullMatrix<double> Amean(6,6);
        _clusterVolumeMap[clusterNum] = clutering_impls::meanCluster(cluster,_mapGaussPoints,Amean);
        STensorOperation::fromFullMatrixToSTensor43(Amean,_clusterAverageStrainConcentrationTensorMap[clusterNum]);
        
        static STensor43 Astd;
        clutering_impls::Tensor43STDCluster(cluster,_mapGaussPoints,_clusterAverageStrainConcentrationTensorMap[clusterNum],Astd);
        _clusterSTDTensorMap[clusterNum] = Astd;
        
        static double astd;
        clutering_impls::Tensor43STDScalarCluster(cluster,_mapGaussPoints,_clusterAverageStrainConcentrationTensorMap[clusterNum],astd);
        _clusterSTDMap[clusterNum] = astd;

        _clusterMaterialMap[clusterNum] = matNum;
        
        Msg::Info("cluster %d, matNum = %d, volume = %e, stdev %e",clusterNum,matNum,_clusterVolumeMap[clusterNum], _clusterSTDMap[clusterNum]);
        Amean.print("average strain concentration tensor");

        // most strain IP
        mostStrainIP[clusterNum] = clutering_impls::maxConcentrationCluster(cluster);
        Msg::Info("most strain in cluster = %d is IP %d",clusterNum,mostStrainIP[clusterNum]);       
        
        /*fullVector<double> cluster_fiberOrientation(3);
        double E1_mean(0.), E2_mean(0.), E3_mean(0.);
        clutering_impls::meanClusterFiberOrientation(cluster,_mapGaussPoints,_mapLocalOrientation,E1_mean,E2_mean,E3_mean);
        cluster_fiberOrientation(0) = E1_mean;
        cluster_fiberOrientation(1) = E2_mean;
        cluster_fiberOrientation(2) = E3_mean;
        _clusterMeanFiberOrientationMap[clusterNum] = cluster_fiberOrientation;
        Msg::Info("inclusion angle in cluster = %d is %.16g,%.16g,%.16g ",clusterNum,E1_mean,E2_mean,E3_mean);*/
        
      }
      _totalNumberOfClusters += nbCluster;
      Msg::Info("done clustering for material %d",matNum);
    }
    else
    {
      Msg::Info("material %d is not clustered",matNum);
    }
  }
  //
  if (saveMostStrainIPFile)
  {
    std::vector<std::string> splitFileName = SplitFileName(saveMostStrainIPFileName);
    std::string fname = splitFileName[1]+"_nbClusters_"+std::to_string(_totalNumberOfClusters)+splitFileName[2];
    FILE* f = fopen(fname.c_str(),"w");
    std::string str ="CLUSTER_ID ELE_NUM GP_NUM";
    fprintf(f,"%s\n",str.c_str());
    for (std::map<int, int >::const_iterator it = mostStrainIP.begin();  it!= mostStrainIP.end(); it++)
    { 
      int clusterId = it->first;
      int ipType = it->second;
      int ele, gp;
      numericalMaterialBase::getTwoIntsFromType(ipType,ele,gp);
      fprintf(f, "%d %d %d\n",clusterId, ele, gp);
    }
    fclose(f);
    printf("done writing data to file: %s \n",fname.c_str());
  }

  if (saveClusterMeanFiberOrientationFile)
  {
    std::vector<std::string> splitFileName = SplitFileName(saveClusterMeanFiberOrientationFileName);
    std::string fname = splitFileName[1]+"_nbClusters_"+std::to_string(_totalNumberOfClusters)+splitFileName[2];
    FILE* f = fopen(fname.c_str(),"w");
    printf("start writing cluster mean fiber orientation to file: %s\n",fname.c_str());
    std::string str ="CLUSTER_ID MAT_NUM E1 E2 E3";
    fprintf(f,"%s\n",str.c_str());
    for (std::map<int, fullVector<double> >::const_iterator it = _clusterMeanFiberOrientationMap.begin();  it!= _clusterMeanFiberOrientationMap.end(); it++)
    { 
      int clusterId = it->first;
      int matNum = _clusterMaterialMap[clusterId];
      fullVector<double> angles = it->second;
      double E1 = angles(0);
      double E2 = angles(1);
      double E3 = angles(2);
      fprintf(f, "%d %d %.16g %.16g %.16g\n",clusterId,matNum,E1,E2,E3);
    }
    fclose(f);
    printf("done writing cluster mean fiber orientation to file: %s \n",fname.c_str());
  }
 
  if (saveToFile)
  {
    std::vector<std::string> splitFileName = SplitFileName(clusterFileName);
    std::string fname = splitFileName[1]+"_nbClusters_"+std::to_string(_totalNumberOfClusters)+splitFileName[2];
    FILE* f = fopen(fname.c_str(),"w");
    printf("start writing cluster data to file: %s\n",fname.c_str());
    std::string str ="ELE_NUM GP_NUM CLUSTER_ID MAT_NUM";
    fprintf(f,"%s\n",str.c_str());
    // write data
    for (std::map<int, int>::const_iterator it = _clusterIndex.begin();  it!= _clusterIndex.end(); it++)
    { 
      int type = it->first;
      int clusterId = it->second;
      int ele, gp;
      numericalMaterialBase::getTwoIntsFromType(type,ele,gp);
      fprintf(f, "%d %d %d %d\n",ele,gp,clusterId,_materialMap[type]);
    }
    fclose(f);
    printf("done writing cluster data to file: %s \n",fname.c_str());
  }
  
  if (saveSummaryFile)
  {
    std::vector<std::string> splitFileName = SplitFileName(summaryFileName);
    std::string fname = splitFileName[1]+"_nbClusters_"+std::to_string(_totalNumberOfClusters)+splitFileName[2];
    FILE* f = fopen(fname.c_str(),"w");
    printf("start writing cluster summary to file: %s\n",fname.c_str());
    std::string str ="CLUSTER_ID MAT_NUM VOLUME STD";
    // write data
    for (int i=0; i<6; i++)
    {
      for (int j=0; j<6; j++)
      {
        str += " A_"+std::to_string(i)+std::to_string(j);
      }
    }
    fprintf(f,"%s\n",str.c_str());
    // write data
    for (std::map<int, STensor43>::const_iterator it = _clusterAverageStrainConcentrationTensorMap.begin();  it!= _clusterAverageStrainConcentrationTensorMap.end(); it++)
    { 
      int cluster = it->first;
      int matNum = _clusterMaterialMap[cluster];
      double volume = _clusterVolumeMap[cluster];
      double stdev = _clusterSTDMap[cluster];
      fprintf(f,"%d %d %.16g %.16g",cluster, matNum, volume, stdev);
      const STensor43& Amean = it->second;
      static fullMatrix<double> mat(6,6);
      STensorOperation::fromSTensor43ToFullMatrix(Amean,mat);
      printf("writing data for cluster %d\n",cluster);
      mat.print("average strain concentration tensor");
      for (int i=0; i<6; i++)
      {
        for (int j=0; j<6; j++)
        {
          fprintf(f, " %.16g",mat(i,j));
        }
      }
      fprintf(f, "\n");
    }
    fclose(f);
    printf("done writing cluster summary to file: %s \n",fname.c_str());
  }
  return true;
};


bool Clustering::computeClustersElasticityHierarchical(int minNumberElementEachCluster, int maxNbIterations,
                                                       bool saveToFile, std::string clusterFileName,
                                                       bool saveSummaryFile, std::string summaryFileName
                                                       )
{
  // init cluster 
  clearAllClusterData();
  for (std::map<int,int>::const_iterator it = _materialMap.begin(); it != _materialMap.end(); it++)
  {
    _clusterIndex[it->first] = -1;
    _clusterIndexLevel0[it->first] = -1;
  }
  
  //
  _totalNumberOfClusters = 0;
  for (std::map<int, std::map<int, fullMatrix<double> > >::const_iterator  itMat = _concentrationTensorMap.begin(); 
                      itMat != _concentrationTensorMap.end(); itMat++)
  {
    int matNum = itMat->first;
    std::map<int,std::vector<int>>::const_iterator itnbLevels = _numberOfClustersPerLevelPerLaw.find(matNum);
    const std::vector<int>& clusterNbLevels = itnbLevels->second;
    
    _numberOfLevels = clusterNbLevels.size();
    
    std::vector<int> nbClusterLevels;
    
    if (itnbLevels !=_numberOfClustersPerLevelPerLaw.end())
    { 
      nbClusterLevels.push_back(clusterNbLevels[0]);
      if(_numberOfLevels>1)
      {
        nbClusterLevels.push_back(clusterNbLevels[1]);
      }
      if(_numberOfLevels>2)
      {
        nbClusterLevels.push_back(clusterNbLevels[2]);
      }
       if(_numberOfLevels>3)
      {
        nbClusterLevels.push_back(clusterNbLevels[3]);
      }
      if(_numberOfLevels>4)
      {
        nbClusterLevels.push_back(clusterNbLevels[4]);
      }
    }
        
    if (nbClusterLevels[0] > 0)
    {
      const std::map<int, fullMatrix<double> >& dataInMat = itMat->second;

      Msg::Info("start clustering for material %d on level 0 \n number of clusters = %d, min number in each cluster = %d",matNum, nbClusterLevels[0], minNumberElementEachCluster);
      
      typedef std::map<int, std::map<int, const fullMatrix<double>*> > clusters_perLevel;
      clusters_perLevel clustersInMatLevel0;
      std::map<int, clusters_perLevel > clustersInMatLevel1;
      std::map<int, std::map<int, clusters_perLevel > > clustersInMatLevel2;
      std::map<int, std::map<int,std::map<int, clusters_perLevel > > > clustersInMatLevel3;
      std::map<int, std::map<int, std::map<int,std::map<int, clusters_perLevel > > > > clustersInMatLevel4;
      
      bool ok = false;
      ok= clutering_impls::kmean_cluster(dataInMat,nbClusterLevels[0],minNumberElementEachCluster,maxNbIterations,_mapGaussPoints,clustersInMatLevel0);  
      if (!ok)
      {
        return false;
      }
      
      for (std::map<int, std::map<int, const fullMatrix<double>*> >::const_iterator it = clustersInMatLevel0.begin(); it!= clustersInMatLevel0.end(); it++)
      {
        int clusterNumLevel0 = it->first + _totalNumberOfClusters;
        const std::map<int, const fullMatrix<double>*>& cluster = it->second;
        //
        for (std::map<int, const fullMatrix<double>*>::const_iterator itset = cluster.begin(); itset != cluster.end(); itset++)
        {
          // update cluster index at each material point
          _clusterIndexLevel0[itset->first] = clusterNumLevel0;
        }
        // average concentration tensor in this cluster
        static fullMatrix<double> Amean(6,6);
        _clusterVolumeMapLevel0[clusterNumLevel0] = clutering_impls::meanCluster(cluster,_mapGaussPoints,Amean);
        STensorOperation::fromFullMatrixToSTensor43(Amean,_clusterAverageStrainConcentrationTensorMap[clusterNumLevel0]);

        _clusterMaterialMapLevel0[clusterNumLevel0] = matNum;
        
        Msg::Info("cluster %d, matNum = %d, volume = %e ",clusterNumLevel0,matNum,_clusterVolumeMapLevel0[clusterNumLevel0]);
        Amean.print("average strain concentration tensor level 0");        
        
        if(_numberOfLevels>1)
        {
          const std::map<int, const fullMatrix<double>* >& dataInClPointer = it->second;    
          std::map<int, fullMatrix<double> > dataInCl;    
          for (std::map<int, const fullMatrix<double>* >::const_iterator  itp = dataInClPointer.begin(); itp != dataInClPointer.end(); itp++)
          {
            fullMatrix<double>& mat = dataInCl[itp->first];
            mat = *(itp->second);
          }
          clusters_perLevel& clusterInCl_pnt = clustersInMatLevel1[clusterNumLevel0];    
          ok = false;
          ok= clutering_impls::kmean_cluster(dataInCl,nbClusterLevels[1],minNumberElementEachCluster,maxNbIterations,_mapGaussPoints,clusterInCl_pnt);
          if (!ok)
          {
            return false;
          }

          std::map<int, STensor43>& strainConcMapLevel1 = _clusterAverageStrainConcentrationTensorMapLevel1[clusterNumLevel0];
          std::map<int, double>& volumeMapLevel1 = _clusterVolumeMapLevel1[clusterNumLevel0];
          std::map<int, int>& materialMapLevel1 = _clusterMaterialMapLevel1[clusterNumLevel0];
        
          for (std::map<int, std::map<int, const fullMatrix<double>*> >::const_iterator itLevel1 = clusterInCl_pnt.begin(); itLevel1!= clusterInCl_pnt.end(); itLevel1++)
          {
            int clusterNumLevel1 = itLevel1->first;
            const std::map<int, const fullMatrix<double>*>& cluster = itLevel1->second;
          
            for (std::map<int, const fullMatrix<double>*>::const_iterator itset = cluster.begin(); itset != cluster.end(); itset++)
            {
              // update cluster index at each material point
              std::vector<int>& clusterIndices = _clusterIndexHierarchical[itset->first];
              clusterIndices.resize(2,true);
              clusterIndices[0] = clusterNumLevel0;
              clusterIndices[1] = clusterNumLevel1;
            }
            // average concentration tensor in this cluster
            static fullMatrix<double> Amean(6,6);
            volumeMapLevel1[clusterNumLevel1] = clutering_impls::meanCluster(cluster,_mapGaussPoints,Amean);
            //volumeMapLevel1[clusterNumLevel1] *= (1./_clusterVolumeMapLevel0[clusterNumLevel0]);
            STensor43 Amean_tensor;
            STensorOperation::fromFullMatrixToSTensor43(Amean,Amean_tensor);
          
            const STensor43& Amean_host = _clusterAverageStrainConcentrationTensorMap[clusterNumLevel0];
            STensor43 Amean_host_inv;
            STensorOperation::inverseSTensor43(Amean_host,Amean_host_inv);
          
            STensorOperation::multSTensor43InPlace(Amean_tensor,Amean_host_inv);
            strainConcMapLevel1[clusterNumLevel1] = Amean_tensor;
            materialMapLevel1[clusterNumLevel1] = matNum;
          
            Msg::Info("level 0 cluster index: %d, level 1 cluster index: %d, matNum = %d, volume = %e ",clusterNumLevel0,clusterNumLevel1,matNum,volumeMapLevel1[clusterNumLevel1]);
            Amean.print("average strain concentration tensor level 1");  
          }

        }
        
      }
            
      _totalNumberOfClusters += nbClusterLevels[0];
      Msg::Info("done clustering for material %d",matNum);
    }
    else
    {
      Msg::Info("material %d is not clustered",matNum);
    }
  }
  
  _clusterIndex = _clusterIndexLevel0;
  _clusterMaterialMap = _clusterMaterialMapLevel0;
  
  if (saveToFile)
  {
    std::vector<std::string> splitFileName = SplitFileName(clusterFileName);
    std::string fname = splitFileName[1]+"_nbClusters_"+std::to_string(_totalNumberOfClusters)+splitFileName[2];
    FILE* f = fopen(fname.c_str(),"w");
    printf("start writing cluster data to file: %s\n",fname.c_str());
    std::string str ="ELE_NUM GP_NUM CLUSTER_ID MAT_NUM";
    fprintf(f,"%s\n",str.c_str());
    // write data
    for (std::map<int, int>::const_iterator it = _clusterIndex.begin();  it!= _clusterIndex.end(); it++)
    { 
      int type = it->first;
      int clusterId = it->second;
      int ele, gp;
      numericalMaterialBase::getTwoIntsFromType(type,ele,gp);
      fprintf(f, "%d %d %d %d\n",ele,gp,clusterId,_materialMap[type]);
    }
    fclose(f);
    printf("done writing cluster data to file: %s \n",fname.c_str());    
  }
  
  if (saveSummaryFile)
  { 
    std::vector<std::string> splitFileName = SplitFileName(summaryFileName);
    std::string fname = splitFileName[1]+"_nbClusters_"+std::to_string(_totalNumberOfClusters)+splitFileName[2];
    FILE* f0 = fopen(fname.c_str(),"w");
    printf("start writing cluster summary level 0 to file: %s\n",fname.c_str());
    std::string str0 ="CLUSTER_ID MAT_NUM VOLUME STD";
    // write data
    for (int i=0; i<6; i++)
    {
      for (int j=0; j<6; j++)
      {
        str0 += " A_"+std::to_string(i)+std::to_string(j);
      }
    }
    fprintf(f0,"%s\n",str0.c_str());
    // write data
    for (std::map<int, STensor43>::const_iterator it = _clusterAverageStrainConcentrationTensorMap.begin();  it!= _clusterAverageStrainConcentrationTensorMap.end(); it++)
    { 
      int cluster = it->first;
      int matNum = _clusterMaterialMapLevel0[cluster];
      double volume = _clusterVolumeMapLevel0[cluster];
      double stdev = 0.;
      fprintf(f0,"%d %d %.16g %.16g",cluster, matNum, volume, stdev);
      const STensor43& Amean = it->second;
      static fullMatrix<double> mat(6,6);
      STensorOperation::fromSTensor43ToFullMatrix(Amean,mat);
      printf("writing data for cluster %d\n",cluster);
      mat.print("average strain concentration tensor");
      for (int i=0; i<6; i++)
      {
        for (int j=0; j<6; j++)
        {
          fprintf(f0, " %.16g",mat(i,j));
        }
      }
      fprintf(f0, "\n");
    }
    fclose(f0);
    printf("done writing cluster summary level 0 to file: %s \n",fname.c_str());
    
    if (_numberOfLevels>1)
    {
      std::vector<std::string> splitFileName = SplitFileName(summaryFileName);
      std::string fname = splitFileName[1]+"_level1_nbClusters_"+std::to_string(_totalNumberOfClusters)+splitFileName[2];
      FILE* f1 = fopen(fname.c_str(),"w");
      printf("start writing cluster summary level 1 to file: %s\n",fname.c_str());
      std::string str1 ="CLUSTER_LV0 CLUSTER_LV1 MAT_NUM VOLUME";
      // write data
      for (int i=0; i<6; i++)
      {
        for (int j=0; j<6; j++)
        {
          str1 += " A_"+std::to_string(i)+std::to_string(j);
        }
      }
      fprintf(f1,"%s\n",str1.c_str());
      // write data
      for (std::map<int, std::map<int, STensor43> >::const_iterator itLv0 = _clusterAverageStrainConcentrationTensorMapLevel1.begin();  
                                                                    itLv0!= _clusterAverageStrainConcentrationTensorMapLevel1.end(); itLv0++)
      { 
        int clusterLv0 = itLv0->first;
        const std::map<int, STensor43>& clusterStrainConcOnLevel1 = itLv0->second;
        std::map<int, int>& clusterMaterialMapOnLevel1 = _clusterMaterialMapLevel1[clusterLv0];
        std::map<int, double>& clusterVolumeMapOnLevel1 = _clusterVolumeMapLevel1[clusterLv0];        
        for (std::map<int, STensor43>::const_iterator itLv1 = clusterStrainConcOnLevel1.begin();  itLv1!= clusterStrainConcOnLevel1.end(); itLv1++)
        { 
          int clusterLv1 = itLv1->first;
          const int& matNum = clusterMaterialMapOnLevel1[clusterLv1];
          const double& volume = clusterVolumeMapOnLevel1[clusterLv1];
          fprintf(f1,"%d %d %d %.16g",clusterLv0, clusterLv1, matNum, volume);
          const STensor43& Amean = itLv1->second;
          static fullMatrix<double> mat(6,6);
          STensorOperation::fromSTensor43ToFullMatrix(Amean,mat);
          printf("writing data for clusters level 0, level 1: %d %d\n",clusterLv0,clusterLv1);
          mat.print("average strain concentration tensor");
          for (int i=0; i<6; i++)
          {
            for (int j=0; j<6; j++)
            {
              fprintf(f1, " %.16g",mat(i,j));
            }
          }
          fprintf(f1, "\n");
        }
      }
      fclose(f1);
      printf("done writing cluster summary level 1 to file: %s \n",fname.c_str());
    }    
  }  
  return true;
};



bool Clustering::computeClustersPlasticity(int minNumberElementEachCluster, int maxNbIterations,
                                 bool saveToFile, std::string clusterFileName,
                                 bool saveSummaryFile, std::string summaryFileName,
                                 bool savePlastStrainConcFile, std::string plastStrainconcFileName,
                                 bool savePlastEqStrainConcFile, std::string plastEqstrainconcFileName,
                                 bool saveClusterMeanFiberOrientationFile, const std::string saveClusterMeanFiberOrientationFileName
                                 )
{
  // init cluster 
  clearAllClusterData();
  for (std::map<int,int>::const_iterator it = _materialMap.begin(); it != _materialMap.end(); it++)
  {
    _clusterIndex[it->first] = -1;
  }

  std::map<int, int > mostStrainIP;
  std::map<int, int > mostPlastStrainIP;
  
  //
  _totalNumberOfClusters = 0;
  for (std::map<int, std::map<int, fullMatrix<double> > >::const_iterator  itMat = _concentrationTensorMap.begin(); 
                      itMat != _concentrationTensorMap.end(); itMat++)
  {
    int matNum = itMat->first;
    int nbCluster = 0;
    std::map<int,int>::const_iterator itnb = _numberOfClustersPerLaw.find(matNum);
    if (itnb !=_numberOfClustersPerLaw.end())
    {
      nbCluster = itnb->second;
    }
    if (nbCluster > 0)
    {
      const std::map<int, fullMatrix<double> >& dataInMat = itMat->second;
      const std::map<int, fullVector<double> >& dataInMatPlasticStrainConc = _plasticStrainConcentrationVectorMap[itMat->first];
      const std::map<int, fullVector<double> >& dataInMatInelasticStrainConc = _inelasticStrainConcentrationVectorMap[itMat->first];
      const std::map<int, fullVector<double> >& dataInMatTotalStrainConc = _totalStrainConcentrationVectorMap[itMat->first];
      const std::map<int, fullVector<double> >& dataInMatPlasticEqStrainConc = _plasticEqStrainConcentrationVectorMap[itMat->first];
      
      const std::map<int, fullVector<double> >& dataInMatLocOri = _localOrientationVectorMap[itMat->first];
      
      std::map<int, fullVector<double> >& dataInMatPlasticStrainConcNormalized = _plasticStrainConcentrationNormalizedVectorMap[itMat->first];
      clutering_impls::vecNormalizeByOverallNorm(dataInMatPlasticStrainConc,_mapGaussPoints,dataInMatPlasticStrainConcNormalized); 
      
      //std::map<int, fullVector<double> >& dataInMatInelasticStrainConcNormalized = _inelasticStrainConcentrationNormalizedVectorMap[itMat->first];
      //clutering_impls::vecNormalizeByOverallNorm(dataInMatInelasticStrainConc,_mapGaussPoints,dataInMatInelasticStrainConcNormalized);
      
      //std::map<int, fullVector<double> >& dataInMatTotalStrainConcNormalized = _totalStrainConcentrationNormalizedVectorMap[itMat->first];
      //clutering_impls::vecNormalizeByOverallNorm(dataInMatTotalStrainConc,_mapGaussPoints,dataInMatTotalStrainConcNormalized); 
      
      std::map<int, fullVector<double> >& dataInMatPlasticEqStrainConcNormalized = _plasticEqStrainConcentrationNormalizedVectorMap[itMat->first];
      clutering_impls::vecNormalizeByOverallValue(dataInMatPlasticEqStrainConc,_mapGaussPoints,dataInMatPlasticEqStrainConcNormalized);
   
      //Msg::Info("start clustering for material %d \n number of clusters = %d, min number in each cluster = %d",matNum, nbCluster,minNumberElementEachCluster);
      std::map<int, std::map<int, const fullMatrix<double>*> > clusterInMat;
      std::map<int, std::map<int, const fullVector<double>*> > clusterInMatPlasticTensor;
      std::map<int, std::map<int, const fullVector<double>*> > clusterInMatInelasticTensor;
      std::map<int, std::map<int, const fullVector<double>*> > clusterInMatTotalStrainTensor;
      
      std::map<int, std::map<int, const fullVector<double>*> > clusterInMatLocOri;

      bool ok=false;
                                                          
      ok= clutering_impls::kmean_cluster_FullVectorBased(dataInMatPlasticStrainConcNormalized,dataInMat,nbCluster,minNumberElementEachCluster,maxNbIterations,_mapGaussPoints,
                                                         clusterInMatPlasticTensor,clusterInMat);
                                                              
      //ok= clutering_impls::kmean_cluster_hierarchical_fullVector_fullMatrixBased(dataInMatPlasticStrainConcNormalized,dataInMat,
        //                                                                         nbCluster,minNumberElementEachCluster,maxNbIterations,_mapGaussPoints,
          //                                                                       clusterInMatPlasticTensor,clusterInMat);
          
      //ok= clutering_impls::kmean_cluster_hierarchical_fullVectorBased(dataInMatPlasticStrainConcNormalized,dataInMat,
        //                                                              nbCluster,minNumberElementEachCluster,maxNbIterations,_mapGaussPoints,
          //                                                            clusterInMatPlasticTensor,clusterInMat);
        
      //ok= clutering_impls::kmean_cluster_FullVectorBased(dataInMatInelasticStrainConcNormalized,dataInMat,nbCluster,minNumberElementEachCluster,maxNbIterations,_mapGaussPoints,
        //                                                     clusterInMatInelasticTensor,clusterInMat);
      
      /////// for polarization clustering, because elastic phases can be clustered too /////// 
      /*
      ok= clutering_impls::kmean_cluster_FullVectorBased(dataInMatTotalStrainConcNormalized,dataInMat,nbCluster,minNumberElementEachCluster,maxNbIterations,_mapGaussPoints,
                                                         clusterInMatTotalStrainTensor,clusterInMat);
      */
      ////////////////////////////////////////////////////////////////////////////////////////

                                                             
      //////// for woven /////////
      /*if(matNum==11)
      {                                                        
      ok= clutering_impls::kmean_cluster_FullVectorBased(dataInMatInelasticStrainConcNormalized,dataInMat,nbCluster,minNumberElementEachCluster,maxNbIterations,_mapGaussPoints,
                                                              clusterInMatInelasticTensor,clusterInMat);
      }
      if(matNum==12 or matNum==13)
      {                                                        
      //ok= clutering_impls::kmean_cluster_orientationBased(dataInMatLocOri,dataInMat,nbCluster,minNumberElementEachCluster,maxNbIterations,_mapGaussPoints,
        //                                                       clusterInMatLocOri,clusterInMat);
        
      ok= clutering_impls::kmean_cluster_hierarchical_orientation_fullVectorBased(dataInMatLocOri,dataInMatInelasticStrainConcNormalized,dataInMat,
                                                                                  nbCluster,minNumberElementEachCluster,maxNbIterations,_mapGaussPoints,
                                                                                  clusterInMatLocOri,clusterInMatInelasticTensor,clusterInMat);
                                                                                  
      //ok= clutering_impls::kmean_cluster_FullVectorBased(dataInMatInelasticStrainConcNormalized,dataInMat,nbCluster,minNumberElementEachCluster,maxNbIterations,_mapGaussPoints,
        //                                                      clusterInMatInelasticTensor,clusterInMat);
      }*/
      //////////////////////////
      
      if (!ok)
      {
        return false;
      }
      for (std::map<int, std::map<int, const fullMatrix<double>*> >::const_iterator it = clusterInMat.begin(); it!= clusterInMat.end(); it++)
      {
        int clusterNum = it->first + _totalNumberOfClusters;
        const std::map<int, const fullMatrix<double>*>& cluster = it->second;
        //
        for (std::map<int, const fullMatrix<double>*>::const_iterator itset = cluster.begin(); itset != cluster.end(); itset++)
        {
          // update cluster index at each material point
          _clusterIndex[itset->first] = clusterNum;
        }
        // average concentration tensor in this cluster
        static fullMatrix<double> Amean(6,6);
        _clusterVolumeMap[clusterNum] = clutering_impls::meanCluster(cluster,_mapGaussPoints,Amean);
        STensorOperation::fromFullMatrixToSTensor43(Amean,_clusterAverageStrainConcentrationTensorMap[clusterNum]);

        /*const std::map<int, const fullVector<double>*>& clusterPlTensor = (clusterInMatPlasticTensor.find(it->first))->second;
        static fullVector<double> PlTensorMean(36);
        _clusterVolumeMap[clusterNum] = clutering_impls::meanClusterVecLong(clusterPlTensor,_mapGaussPoints,PlTensorMean);
        _clusterAveragePlasticStrainConcentrationVectorMap[clusterNum] = PlTensorMean;*/
           
        static fullVector<double> PlEqmean(6);
        clutering_impls::meanClusterVec_FullMatrixBased(dataInMatPlasticEqStrainConcNormalized,cluster,_mapGaussPoints,PlEqmean);
        _clusterAveragePlasticEqStrainConcentrationVectorMap[clusterNum] = PlEqmean;

        static fullVector<double> PlEqGeometricMeanVec(6);
        clutering_impls::meanGeometricClusterVec_FullMatrixBased(dataInMatPlasticEqStrainConcNormalized,cluster,_mapGaussPoints,PlEqGeometricMeanVec);
        _clusterAverageGeometricPlasticEqStrainConcentrationVectorMap[clusterNum] = PlEqGeometricMeanVec;

        static fullVector<double> PlEqHarmonicMeanVec(6);
        clutering_impls::meanHarmonicClusterVec_FullMatrixBased(dataInMatPlasticEqStrainConcNormalized,cluster,_mapGaussPoints,PlEqHarmonicMeanVec);
        _clusterAverageHarmonicPlasticEqStrainConcentrationVectorMap[clusterNum] = PlEqHarmonicMeanVec;
        
        static fullVector<double> PlEqPowerMeanVec(6);
        clutering_impls::meanPowerClusterVec_FullMatrixBased(dataInMatPlasticEqStrainConcNormalized,cluster,_mapGaussPoints,PlEqPowerMeanVec);
        _clusterAveragePowerPlasticEqStrainConcentrationVectorMap[clusterNum] = PlEqPowerMeanVec;
        
        for (std::map<int, const fullMatrix<double>*>::const_iterator itset = cluster.begin(); itset != cluster.end(); itset++)
        {
          fullVector<double>& pfc= _clusterPFC[itset->first];
          pfc.resize(6,true);
          for(int i=0;i<6;i++)
          {
            if(PlEqmean(i)>1. and PlEqHarmonicMeanVec(i)>0.8)
            {
              pfc(i) = PlEqmean(i)/PlEqHarmonicMeanVec(i);
            }
            else
            {
              pfc(i) = 1.;
            }
          }
        _clusterPFC[itset->first] = pfc;
        }

        static fullVector<double> stdPlVec(36);
        //clutering_impls::VectorSTDCluster(clusterPlTensor,_mapGaussPoints,_clusterAveragePlasticStrainConcentrationVectorMap[clusterNum],stdPlVec);
        _clusterPlSTDVectorMap[clusterNum] = stdPlVec;
                                            
        _clusterMaterialMap[clusterNum] = matNum;
        
        Msg::Info("cluster %d, matNum = %d, volume = %e, stdev %e",clusterNum,matNum,_clusterVolumeMap[clusterNum],_clusterSTDMap[clusterNum]);
        Amean.print("average strain concentration tensor");
        
        mostStrainIP[clusterNum] = 0;
                
        //////// for woven /////////        
        /*
        fullVector<double> cluster_fiberOrientation(3);
        double E1_mean(0.), E2_mean(0.), E3_mean(0.);
        clutering_impls::meanClusterFiberOrientation(cluster,_mapGaussPoints,_mapLocalOrientation,E1_mean,E2_mean,E3_mean);
        cluster_fiberOrientation(0) = E1_mean;
        cluster_fiberOrientation(1) = E2_mean;
        cluster_fiberOrientation(2) = E3_mean;
        _clusterMeanFiberOrientationMap[clusterNum] = cluster_fiberOrientation;
        Msg::Info("mean fiber orientation of cluster = %d is %.16g,%.16g,%.16g ",clusterNum,E1_mean,E2_mean,E3_mean);
        */
        //////////////////////////////
               
      }
      _totalNumberOfClusters += nbCluster;
      Msg::Info("done clustering for material %d",matNum);
    }
    else
    {
      Msg::Info("material %d is not clustered",matNum);
    }
  }
  //
  
  if (saveClusterMeanFiberOrientationFile)
  {
    std::vector<std::string> splitFileName = SplitFileName(saveClusterMeanFiberOrientationFileName);
    std::string fname = splitFileName[1]+"_nbClusters_"+std::to_string(_totalNumberOfClusters)+splitFileName[2];
    FILE* f = fopen(fname.c_str(),"w");
    printf("start writing cluster mean fiber orientation to file: %s\n",fname.c_str());
    std::string str ="CLUSTER_ID MAT_NUM E1 E2 E3";
    fprintf(f,"%s\n",str.c_str());
    for (std::map<int, fullVector<double> >::const_iterator it = _clusterMeanFiberOrientationMap.begin();  it!= _clusterMeanFiberOrientationMap.end(); it++)
    { 
      int clusterId = it->first;
      int matNum = _clusterMaterialMap[clusterId];
      fullVector<double> angles = it->second;
      double E1 = angles(0);
      double E2 = angles(1);
      double E3 = angles(2);
      fprintf(f, "%d %d %.16g %.16g %.16g\n",clusterId,matNum,E1,E2,E3);
    }
    fclose(f);
    printf("done writing cluster mean fiber orientation to file: %s \n",fname.c_str());
  }
  
  if (saveToFile)
  {
    std::vector<std::string> splitFileName = SplitFileName(clusterFileName);
    std::string fname = splitFileName[1]+"_nbClusters_"+std::to_string(_totalNumberOfClusters)+splitFileName[2];
    FILE* f = fopen(fname.c_str(),"w");
    printf("start writing cluster data to file: %s\n",fname.c_str());
    std::string str ="ELE_NUM GP_NUM CLUSTER_ID MAT_NUM";
    fprintf(f,"%s\n",str.c_str());
    // write data
    for (std::map<int, int>::const_iterator it = _clusterIndex.begin();  it!= _clusterIndex.end(); it++)
    { 
      int type = it->first;
      int clusterId = it->second;
      int ele, gp;
      numericalMaterialBase::getTwoIntsFromType(type,ele,gp);
      fprintf(f, "%d %d %d %d\n",ele,gp,clusterId,_materialMap[type]);
    }
    fclose(f);
    printf("done writing cluster data to file: %s \n",fname.c_str());
  }
  
  if (saveSummaryFile)
  {
    std::vector<std::string> splitFileName = SplitFileName(summaryFileName);
    std::string fname = splitFileName[1]+"_nbClusters_"+std::to_string(_totalNumberOfClusters)+splitFileName[2];
    FILE* f = fopen(fname.c_str(),"w");
    printf("start writing cluster summary to file: %s\n",fname.c_str());
    std::string str ="CLUSTER_ID MAT_NUM VOLUME STD";
    // write data
    for (int i=0; i<6; i++)
    {
      for (int j=0; j<6; j++)
      {
        str += " A_"+std::to_string(i)+std::to_string(j);
      }
    }
    fprintf(f,"%s\n",str.c_str());
    // write data
    for (std::map<int, STensor43>::const_iterator it = _clusterAverageStrainConcentrationTensorMap.begin();  it!= _clusterAverageStrainConcentrationTensorMap.end(); it++)
    { 
      int cluster = it->first;
      int matNum = _clusterMaterialMap[cluster];
      double volume = _clusterVolumeMap[cluster];
      double stdev = _clusterSTDMap[cluster];
      fprintf(f,"%d %d %.16g %.16g",cluster, matNum, volume, stdev);
      const STensor43& Amean = it->second;
      static fullMatrix<double> mat(6,6);
      STensorOperation::fromSTensor43ToFullMatrix(Amean,mat);
      //printf("writing data for cluster %d\n",cluster);
      //mat.print("average strain concentration tensor");
      for (int i=0; i<6; i++)
      {
        for (int j=0; j<6; j++)
        {
          fprintf(f, " %.16g",mat(i,j));
        }
      }
      fprintf(f, "\n");
    }
    fclose(f);
    printf("done writing cluster summary to file: %s \n",fname.c_str());
  }
      
  if (savePlastEqStrainConcFile)
  {
    std::vector<std::string> splitFileName = SplitFileName(plastEqstrainconcFileName);
    
    std::string fnamePl = splitFileName[1]+"_nbClusters_"+std::to_string(_totalNumberOfClusters)+splitFileName[2];
    FILE* fpl = fopen(fnamePl.c_str(),"w");
    printf("start writing cluster eq. plastic summary to file: %s\n",fnamePl.c_str());
    std::string strpl ="CLUSTER_ID MAT_NUM VOLUME";
    // write data
    for (int i=0; i<6; i++)
    {
        strpl += " a_pl_"+std::to_string(i);
    }
    fprintf(fpl,"%s\n",strpl.c_str());
    // write data
    for (std::map<int, fullVector<double>>::const_iterator it = _clusterAveragePlasticEqStrainConcentrationVectorMap.begin();  it!= _clusterAveragePlasticEqStrainConcentrationVectorMap.end(); it++)
    { 
      int cluster = it->first;
      int matNum = _clusterMaterialMap[cluster];
      double volume = _clusterVolumeMap[cluster];
      fprintf(fpl,"%d %d %.16g",cluster, matNum, volume);
      const fullVector<double>& plasticmean = it->second;
      //printf("writing eq. plastic data for cluster %d\n",cluster);
      //plasticmean.print("plastic eq. average strain concentration");
      for (int i=0; i<6; i++)
      {
        fprintf(fpl, " %.16g",plasticmean(i));
      }
      fprintf(fpl, "\n");
    }
    fclose(fpl);
    printf("done writing cluster plastic eq. mean summary to file: %s \n",fnamePl.c_str());
    
    std::string fnamePlGeoMean = splitFileName[1]+"Geo_nbClusters_"+std::to_string(_totalNumberOfClusters)+splitFileName[2];
    FILE* fplGeoMean = fopen(fnamePlGeoMean.c_str(),"w");
    printf("start writing cluster eq. plastic geometric mean summary to file: %s\n",fnamePlGeoMean.c_str());
    std::string strplGeoMean ="CLUSTER_ID MAT_NUM VOLUME";
    // write data
    for (int i=0; i<6; i++)
    {
        strplGeoMean += " a_pl_"+std::to_string(i);
    }
    fprintf(fplGeoMean,"%s\n",strplGeoMean.c_str());
    // write data
    for (std::map<int, fullVector<double>>::const_iterator it = _clusterAverageGeometricPlasticEqStrainConcentrationVectorMap.begin();
         it!= _clusterAverageGeometricPlasticEqStrainConcentrationVectorMap.end(); it++)
    { 
      int cluster = it->first;
      int matNum = _clusterMaterialMap[cluster];
      double volume = _clusterVolumeMap[cluster];
      fprintf(fplGeoMean,"%d %d %.16g",cluster, matNum, volume);
      const fullVector<double>& plasticmean = it->second;
      //printf("writing geometric mean eq. plastic data for cluster %d\n",cluster);
      //plasticmean.print("geometric mean plastic eq. average strain concentration tensor");
      for (int i=0; i<6; i++)
      {
        fprintf(fplGeoMean, " %.16g",plasticmean(i));
      }
      fprintf(fplGeoMean, "\n");
    }
    fclose(fplGeoMean);
    printf("done writing cluster plastic eq. geometric mean summary to file: %s \n",fnamePlGeoMean.c_str());
    
    std::string fnamePlHarMean = splitFileName[1]+"Har_nbClusters_"+std::to_string(_totalNumberOfClusters)+splitFileName[2];
    FILE* fplHarMean = fopen(fnamePlHarMean.c_str(),"w");
    printf("start writing cluster eq. plastic harmonic mean summary to file: %s\n",fnamePlHarMean.c_str());
    std::string strplHarMean ="CLUSTER_ID MAT_NUM VOLUME";
    // write data
    for (int i=0; i<6; i++)
    {
        strplHarMean += " a_pl_"+std::to_string(i);
    }
    fprintf(fplHarMean,"%s\n",strplHarMean.c_str());
    // write data
    for (std::map<int, fullVector<double>>::const_iterator it = _clusterAverageHarmonicPlasticEqStrainConcentrationVectorMap.begin();
         it!= _clusterAverageHarmonicPlasticEqStrainConcentrationVectorMap.end(); it++)
    { 
      int cluster = it->first;
      int matNum = _clusterMaterialMap[cluster];
      double volume = _clusterVolumeMap[cluster];
      fprintf(fplHarMean,"%d %d %.16g",cluster, matNum, volume);
      const fullVector<double>& plasticmean = it->second;
      //printf("writing harmonic mean eq. plastic data for cluster %d\n",cluster);
      //plasticmean.print("harmonic mean plastic eq. average strain concentration tensor");
      for (int i=0; i<6; i++)
      {
        fprintf(fplHarMean, " %.16g",plasticmean(i));
      }
      fprintf(fplHarMean, "\n");
    }
    fclose(fplHarMean);
    printf("done writing cluster plastic eq. harmonic mean summary to file: %s \n",fnamePlHarMean.c_str());
    
    std::string fnamePlPowMean = splitFileName[1]+"Pow_nbClusters_"+std::to_string(_totalNumberOfClusters)+splitFileName[2];
    FILE* fplPowMean = fopen(fnamePlPowMean.c_str(),"w");
    printf("start writing cluster eq. plastic power mean summary to file: %s\n",fnamePlPowMean.c_str());
    std::string strplPowMean ="CLUSTER_ID MAT_NUM VOLUME";
    // write data
    for (int i=0; i<6; i++)
    {
        strplPowMean += " a_pl_"+std::to_string(i);
    }
    fprintf(fplPowMean,"%s\n",strplPowMean.c_str());
    // write data
    for (std::map<int, fullVector<double>>::const_iterator it = _clusterAveragePowerPlasticEqStrainConcentrationVectorMap.begin();
         it!= _clusterAveragePowerPlasticEqStrainConcentrationVectorMap.end(); it++)
    { 
      int cluster = it->first;
      int matNum = _clusterMaterialMap[cluster];
      double volume = _clusterVolumeMap[cluster];
      fprintf(fplPowMean,"%d %d %.16g",cluster, matNum, volume);
      const fullVector<double>& plasticmean = it->second;
      //printf("writing power mean eq. plastic data for cluster %d\n",cluster);
      //plasticmean.print("power mean plastic eq. average strain concentration tensor");
      for (int i=0; i<6; i++)
      {
        fprintf(fplPowMean, " %.16g",plasticmean(i));
      }
      fprintf(fplPowMean, "\n");
    }
    fclose(fplPowMean);
    printf("done writing cluster plastic eq. power mean summary to file: %s \n",fnamePlPowMean.c_str());
  }
  return true;
};

bool Clustering::computeClustersLocalOrientation(int minNumberElementEachCluster, int maxNbIterations,
                                                 bool saveToFile, std::string clusterFileName,
                                                 bool saveSummaryFile, std::string summaryFileName,
                                                 bool saveClusterMeanFiberOrientationFile, const std::string saveClusterMeanFiberOrientationFileName
                                                )
{
  // init cluster 
  clearAllClusterData();
  for (std::map<int,int>::const_iterator it = _materialMap.begin(); it != _materialMap.end(); it++)
  {
    _clusterIndex[it->first] = -1;
  }
  
  //
  _totalNumberOfClusters = 0;
  for (std::map<int, std::map<int, fullMatrix<double> > >::const_iterator  itMat = _concentrationTensorMap.begin(); 
                      itMat != _concentrationTensorMap.end(); itMat++)
  {
    int matNum = itMat->first;
    int nbCluster = 0;
    std::map<int,int>::const_iterator itnb = _numberOfClustersPerLaw.find(matNum);
    if (itnb !=_numberOfClustersPerLaw.end())
    {
      nbCluster = itnb->second;
    }
    if (nbCluster > 0)
    {
      const std::map<int, fullMatrix<double> >& dataInMat = itMat->second;
      const std::map<int, fullVector<double> >& dataInMatInelasticStrainConc = _inelasticStrainConcentrationVectorMap[itMat->first];      
      const std::map<int, fullVector<double> >& dataInMatLocOri = _localOrientationVectorMap[itMat->first];
      
      std::map<int, fullVector<double> >& dataInMatInelasticStrainConcNormalized = _inelasticStrainConcentrationNormalizedVectorMap[itMat->first];
      clutering_impls::vecNormalizeByOverallNorm(dataInMatInelasticStrainConc,_mapGaussPoints,dataInMatInelasticStrainConcNormalized);
   
      //Msg::Info("start clustering for material %d \n number of clusters = %d, min number in each cluster = %d",matNum, nbCluster,minNumberElementEachCluster);
      std::map<int, std::map<int, const fullMatrix<double>*> > clusterInMat;
      std::map<int, std::map<int, const fullVector<double>*> > clusterInMatInelasticTensor;      
      std::map<int, std::map<int, const fullVector<double>*> > clusterInMatLocOri;

      bool ok=false;
                                                             
      //////// for woven /////////
      if(matNum==11)
      {                                                        
      ok= clutering_impls::kmean_cluster_FullVectorBased(dataInMatInelasticStrainConcNormalized,dataInMat,nbCluster,minNumberElementEachCluster,maxNbIterations,_mapGaussPoints,
                                                              clusterInMatInelasticTensor,clusterInMat);
      }
      if(matNum==12 or matNum==13)
      {                                                        
      //ok= clutering_impls::kmean_cluster_orientationBased(dataInMatLocOri,dataInMat,nbCluster,minNumberElementEachCluster,maxNbIterations,_mapGaussPoints,
        //                                                       clusterInMatLocOri,clusterInMat);
        
      ok= clutering_impls::kmean_cluster_hierarchical_orientation_fullVectorBased(dataInMatLocOri,dataInMatInelasticStrainConcNormalized,dataInMat,
                                                                                  nbCluster,minNumberElementEachCluster,maxNbIterations,_mapGaussPoints,
                                                                                  clusterInMatLocOri,clusterInMatInelasticTensor,clusterInMat);
                                                                                  
      //ok= clutering_impls::kmean_cluster_FullVectorBased(dataInMatInelasticStrainConcNormalized,dataInMat,nbCluster,minNumberElementEachCluster,maxNbIterations,_mapGaussPoints,
        //                                                      clusterInMatInelasticTensor,clusterInMat);
      }
      
      if (!ok)
      {
        return false;
      }
      for (std::map<int, std::map<int, const fullMatrix<double>*> >::const_iterator it = clusterInMat.begin(); it!= clusterInMat.end(); it++)
      {
        int clusterNum = it->first + _totalNumberOfClusters;
        const std::map<int, const fullMatrix<double>*>& cluster = it->second;
        //
        for (std::map<int, const fullMatrix<double>*>::const_iterator itset = cluster.begin(); itset != cluster.end(); itset++)
        {
          // update cluster index at each material point
          _clusterIndex[itset->first] = clusterNum;
        }
        // average concentration tensor in this cluster
        static fullMatrix<double> Amean(6,6);
        _clusterVolumeMap[clusterNum] = clutering_impls::meanCluster(cluster,_mapGaussPoints,Amean);
        STensorOperation::fromFullMatrixToSTensor43(Amean,_clusterAverageStrainConcentrationTensorMap[clusterNum]);
                                            
        _clusterMaterialMap[clusterNum] = matNum;
        
        Msg::Info("cluster %d, matNum = %d, volume = %e, stdev %e",clusterNum,matNum,_clusterVolumeMap[clusterNum],0.);
        Amean.print("average strain concentration tensor");
                
        fullVector<double> cluster_fiberOrientation(3);
        double E1_mean(0.), E2_mean(0.), E3_mean(0.);
        clutering_impls::meanClusterFiberOrientation(cluster,_mapGaussPoints,_mapLocalOrientation,E1_mean,E2_mean,E3_mean);
        cluster_fiberOrientation(0) = E1_mean;
        cluster_fiberOrientation(1) = E2_mean;
        cluster_fiberOrientation(2) = E3_mean;
        _clusterMeanFiberOrientationMap[clusterNum] = cluster_fiberOrientation;
        Msg::Info("mean fiber orientation of cluster = %d is %.16g,%.16g,%.16g ",clusterNum,E1_mean,E2_mean,E3_mean);
               
      }
      _totalNumberOfClusters += nbCluster;
      Msg::Info("done clustering for material %d",matNum);
    }
    else
    {
      Msg::Info("material %d is not clustered",matNum);
    }
  }
  
  if (saveClusterMeanFiberOrientationFile)
  {
    std::vector<std::string> splitFileName = SplitFileName(saveClusterMeanFiberOrientationFileName);
    std::string fname = splitFileName[1]+"_nbClusters_"+std::to_string(_totalNumberOfClusters)+splitFileName[2];
    FILE* f = fopen(fname.c_str(),"w");
    printf("start writing cluster mean fiber orientation to file: %s\n",fname.c_str());
    std::string str ="CLUSTER_ID MAT_NUM E1 E2 E3";
    fprintf(f,"%s\n",str.c_str());
    for (std::map<int, fullVector<double> >::const_iterator it = _clusterMeanFiberOrientationMap.begin();  it!= _clusterMeanFiberOrientationMap.end(); it++)
    { 
      int clusterId = it->first;
      int matNum = _clusterMaterialMap[clusterId];
      fullVector<double> angles = it->second;
      double E1 = angles(0);
      double E2 = angles(1);
      double E3 = angles(2);
      fprintf(f, "%d %d %.16g %.16g %.16g\n",clusterId,matNum,E1,E2,E3);
    }
    fclose(f);
    printf("done writing cluster mean fiber orientation to file: %s \n",fname.c_str());
  }
  
  if (saveToFile)
  {
    std::vector<std::string> splitFileName = SplitFileName(clusterFileName);
    std::string fname = splitFileName[1]+"_nbClusters_"+std::to_string(_totalNumberOfClusters)+splitFileName[2];
    FILE* f = fopen(fname.c_str(),"w");
    printf("start writing cluster data to file: %s\n",fname.c_str());
    std::string str ="ELE_NUM GP_NUM CLUSTER_ID MAT_NUM";
    fprintf(f,"%s\n",str.c_str());
    // write data
    for (std::map<int, int>::const_iterator it = _clusterIndex.begin();  it!= _clusterIndex.end(); it++)
    { 
      int type = it->first;
      int clusterId = it->second;
      int ele, gp;
      numericalMaterialBase::getTwoIntsFromType(type,ele,gp);
      fprintf(f, "%d %d %d %d\n",ele,gp,clusterId,_materialMap[type]);
    }
    fclose(f);
    printf("done writing cluster data to file: %s \n",fname.c_str());
  }
  
  if (saveSummaryFile)
  {
    std::vector<std::string> splitFileName = SplitFileName(summaryFileName);
    std::string fname = splitFileName[1]+"_nbClusters_"+std::to_string(_totalNumberOfClusters)+splitFileName[2];
    FILE* f = fopen(fname.c_str(),"w");
    printf("start writing cluster summary to file: %s\n",fname.c_str());
    std::string str ="CLUSTER_ID MAT_NUM VOLUME STD";
    // write data
    for (int i=0; i<6; i++)
    {
      for (int j=0; j<6; j++)
      {
        str += " A_"+std::to_string(i)+std::to_string(j);
      }
    }
    fprintf(f,"%s\n",str.c_str());
    // write data
    for (std::map<int, STensor43>::const_iterator it = _clusterAverageStrainConcentrationTensorMap.begin();  it!= _clusterAverageStrainConcentrationTensorMap.end(); it++)
    { 
      int cluster = it->first;
      int matNum = _clusterMaterialMap[cluster];
      double volume = _clusterVolumeMap[cluster];
      double stdev = 0.;
      fprintf(f,"%d %d %.16g %.16g",cluster, matNum, volume, stdev);
      const STensor43& Amean = it->second;
      static fullMatrix<double> mat(6,6);
      STensorOperation::fromSTensor43ToFullMatrix(Amean,mat);
      //printf("writing data for cluster %d\n",cluster);
      //mat.print("average strain concentration tensor");
      for (int i=0; i<6; i++)
      {
        for (int j=0; j<6; j++)
        {
          fprintf(f, " %.16g",mat(i,j));
        }
      }
      fprintf(f, "\n");
    }
    fclose(f);
    printf("done writing cluster summary to file: %s \n",fname.c_str());
  }

  return true;
};


bool Clustering::computeClustersPlasticityHierarchical(int minNumberElementEachCluster, int maxNbIterations,
                                                       bool saveToFile, std::string clusterFileName,
                                                       bool saveSummaryFile, std::string summaryFileName
                                                       )
{
  // init cluster 
  clearAllClusterData();
  for (std::map<int,int>::const_iterator it = _materialMap.begin(); it != _materialMap.end(); it++)
  {
    _clusterIndex[it->first] = -1;
    _clusterIndexLevel0[it->first] = -1;
    _clusterIndexHierarchicalGlobal[it->first] = -1;
    (_clusterIndexHierarchical[it->first]).clear();
  }
  
  //
  _totalNumberOfClusters = 0;
  _totalNumberOfClustersHierarchicalGlobal = 0;
  for (std::map<int, std::map<int, fullMatrix<double> > >::const_iterator  itMat = _concentrationTensorMap.begin(); 
                      itMat != _concentrationTensorMap.end(); itMat++)
  {
    int matNum = itMat->first;
    std::map<int,std::vector<int>>::const_iterator itnbLevels = _numberOfClustersPerLevelPerLaw.find(matNum);
    const std::vector<int>& clusterNbLevels = itnbLevels->second;
    
    _numberOfLevels = clusterNbLevels.size();
    
    std::vector<int> nbClusterLevels;
    nbClusterLevels.clear();
    
    if (itnbLevels !=_numberOfClustersPerLevelPerLaw.end())
    { 
      nbClusterLevels.push_back(clusterNbLevels[0]);
      if(_numberOfLevels>1)
      {
        nbClusterLevels.push_back(clusterNbLevels[1]);
      }
    }
        
    if (nbClusterLevels[0] > 0)
    {
      const std::map<int, fullMatrix<double> >& dataInMat = itMat->second;
      
      const std::map<int, fullVector<double> >& dataInMatPlasticStrainConc = _plasticStrainConcentrationVectorMap[itMat->first];      
      std::map<int, fullVector<double> >& dataInMatPlasticStrainConcNormalized = _plasticStrainConcentrationNormalizedVectorMap[itMat->first];
      clutering_impls::vecNormalizeByOverallNorm(dataInMatPlasticStrainConc,_mapGaussPoints,dataInMatPlasticStrainConcNormalized); 

      Msg::Info("start clustering for material %d on level 0 \n number of clusters = %d, min number in each cluster = %d",matNum, nbClusterLevels[0], minNumberElementEachCluster);
      
      typedef std::map<int, std::map<int, const fullMatrix<double>*> > clusters_perLevel;
      clusters_perLevel clustersInMatLevel0;
      std::map<int, clusters_perLevel > clustersInMatLevel1;
      
      typedef std::map<int, std::map<int, const fullVector<double>*> > clustersVec_perLevel;
      clustersVec_perLevel clustersInMatPlasticTensorLevel0;
      std::map<int, clustersVec_perLevel > clustersInMatPlasticTensorLevel1;
      
      bool ok=false;                                                          
      ok= clutering_impls::kmean_cluster_FullVectorBased(dataInMatPlasticStrainConcNormalized,dataInMat,nbClusterLevels[0],minNumberElementEachCluster,maxNbIterations,_mapGaussPoints,
                                                         clustersInMatPlasticTensorLevel0,clustersInMatLevel0);
                                                         
      if (!ok)
      {
        return false;
      }
      
      for (std::map<int, std::map<int, const fullMatrix<double>*> >::const_iterator it = clustersInMatLevel0.begin(); it!= clustersInMatLevel0.end(); it++)
      {
        int clusterNumLevel0 = it->first + _totalNumberOfClusters;
        const std::map<int, const fullMatrix<double>*>& cluster = it->second;
        //
        for (std::map<int, const fullMatrix<double>*>::const_iterator itset = cluster.begin(); itset != cluster.end(); itset++)
        {
          // update cluster index at each material point
          _clusterIndexLevel0[itset->first] = clusterNumLevel0;
        }
        // average concentration tensor in this cluster
        static fullMatrix<double> Amean(6,6);
        _clusterVolumeMapLevel0[clusterNumLevel0] = clutering_impls::meanCluster(cluster,_mapGaussPoints,Amean);
        STensorOperation::fromFullMatrixToSTensor43(Amean,_clusterAverageStrainConcentrationTensorMap[clusterNumLevel0]);

        _clusterMaterialMapLevel0[clusterNumLevel0] = matNum;
        
        Msg::Info("cluster %d, matNum = %d, volume = %e ",clusterNumLevel0,matNum,_clusterVolumeMapLevel0[clusterNumLevel0]);
        Amean.print("average strain concentration tensor level 0");        
        
        if(_numberOfLevels>1)
        {
          const std::map<int, const fullMatrix<double>* >& dataInClPointer = it->second;    
          std::map<int, fullMatrix<double> > dataInCl;    
          for (std::map<int, const fullMatrix<double>* >::const_iterator  itp = dataInClPointer.begin(); itp != dataInClPointer.end(); itp++)
          {
            fullMatrix<double>& mat = dataInCl[itp->first];
            mat = *(itp->second);
          }
          clusters_perLevel& clusterInCl_pnt = clustersInMatLevel1[clusterNumLevel0];  
          
          const std::map<int, const fullVector<double>* >& dataInClPlasticStrainConcNormalizedPointer = clustersInMatPlasticTensorLevel0[it->first];
          std::map<int, fullVector<double> > dataInClPlasticStrainConcNormalized;
          for (std::map<int, const fullVector<double>* >::const_iterator  itp = dataInClPlasticStrainConcNormalizedPointer.begin(); itp != dataInClPlasticStrainConcNormalizedPointer.end(); itp++)
          {
            fullVector<double>& vec = dataInClPlasticStrainConcNormalized[itp->first];
            vec = *(itp->second);
          }
          clustersVec_perLevel& clusterInClPlasticTensor_pnt = clustersInMatPlasticTensorLevel1[clusterNumLevel0];  
            
          int minNumberElementEachClusterLevel1 = 1;  
          ok = false;
          ok= clutering_impls::kmean_cluster_FullVectorBased(dataInClPlasticStrainConcNormalized,dataInCl,nbClusterLevels[1],
                                                             minNumberElementEachClusterLevel1,maxNbIterations,_mapGaussPoints,clusterInClPlasticTensor_pnt,clusterInCl_pnt);
          if (!ok)
          {
            return false;
          }

          std::map<int, STensor43>& strainConcMapLevel1 = _clusterAverageStrainConcentrationTensorMapLevel1[clusterNumLevel0];
          std::map<int, double>& volumeMapLevel1 = _clusterVolumeMapLevel1[clusterNumLevel0];
          std::map<int, int>& materialMapLevel1 = _clusterMaterialMapLevel1[clusterNumLevel0];
        
          for (std::map<int, std::map<int, const fullMatrix<double>*> >::const_iterator itLevel1 = clusterInCl_pnt.begin(); itLevel1!= clusterInCl_pnt.end(); itLevel1++)
          {
            int clusterNumLevel1 = itLevel1->first;
            //int clusterNumLevel1Global = _totalNumberOfClustersHierarchicalGlobal+clusterNumLevel0*nbClusterLevels[1]+clusterNumLevel1;
            int clusterNumLevel1Global = _totalNumberOfClustersHierarchicalGlobal+clusterNumLevel1;
            const std::map<int, const fullMatrix<double>*>& cluster = itLevel1->second;
          
            for (std::map<int, const fullMatrix<double>*>::const_iterator itset = cluster.begin(); itset != cluster.end(); itset++)
            {
              // update cluster index at each material point
              std::vector<int>& clusterIndices = _clusterIndexHierarchical[itset->first];
              clusterIndices.resize(2,true);
              clusterIndices[0] = clusterNumLevel0;
              clusterIndices[1] = clusterNumLevel1;
              
              _clusterIndexHierarchicalGlobal[itset->first] = clusterNumLevel1Global;
            }
            // average concentration tensor in this cluster
            static fullMatrix<double> Amean(6,6);
            volumeMapLevel1[clusterNumLevel1] = clutering_impls::meanCluster(cluster,_mapGaussPoints,Amean);
            STensor43 Amean_tensor;
            STensorOperation::fromFullMatrixToSTensor43(Amean,Amean_tensor);
          
            const STensor43& Amean_host = _clusterAverageStrainConcentrationTensorMap[clusterNumLevel0];
            STensor43 Amean_host_inv;
            STensorOperation::inverseSTensor43(Amean_host,Amean_host_inv);
          
            STensorOperation::multSTensor43InPlace(Amean_tensor,Amean_host_inv);
            strainConcMapLevel1[clusterNumLevel1] = Amean_tensor;
            materialMapLevel1[clusterNumLevel1] = matNum;
          
            Msg::Info("level 0 cluster index: %d, level 1 cluster index: %d, matNum = %d, volume = %e ",clusterNumLevel0,clusterNumLevel1,matNum,volumeMapLevel1[clusterNumLevel1]);
            Amean.print("average strain concentration tensor level 1");  
          }
          _totalNumberOfClustersHierarchicalGlobal += nbClusterLevels[1];
        } 
      }            
      _totalNumberOfClusters += nbClusterLevels[0];
      Msg::Info("done clustering for material %d",matNum);
    }
    else
    {
      Msg::Info("material %d is not clustered",matNum);
    }
  }
  
  _clusterIndex = _clusterIndexLevel0;
  _clusterMaterialMap = _clusterMaterialMapLevel0;
  
  if (saveToFile)
  {
    std::vector<std::string> splitFileName = SplitFileName(clusterFileName);
    std::string fname = splitFileName[1]+"_nbClusters_"+std::to_string(_totalNumberOfClusters)+splitFileName[2];
    FILE* f = fopen(fname.c_str(),"w");
    printf("start writing cluster data to file: %s\n",fname.c_str());
    std::string str ="ELE_NUM GP_NUM CLUSTER_ID MAT_NUM";
    fprintf(f,"%s\n",str.c_str());
    // write data
    for (std::map<int, int>::const_iterator it = _clusterIndex.begin();  it!= _clusterIndex.end(); it++)
    { 
      int type = it->first;
      int clusterId = it->second;
      int ele, gp;
      numericalMaterialBase::getTwoIntsFromType(type,ele,gp);
      fprintf(f, "%d %d %d %d\n",ele,gp,clusterId,_materialMap[type]);
    }
    fclose(f);
    printf("done writing cluster data to file: %s \n",fname.c_str());   
    
    if (_numberOfLevels>1)
    {
      std::vector<std::string> splitFileName = SplitFileName(clusterFileName);
      std::string fname = splitFileName[1]+"_level1_nbClusters_"+std::to_string(_totalNumberOfClusters)+splitFileName[2];
      FILE* f1 = fopen(fname.c_str(),"w");
      printf("start writing cluster data level 1 to file: %s\n",fname.c_str());
      std::string str ="ELE_NUM GP_NUM CL_ID_LV0 CL_ID_LV1 MAT_NUM";
      fprintf(f1,"%s\n",str.c_str());
      // write data
      for (std::map<int, std::vector<int>>::const_iterator itLv0 = _clusterIndexHierarchical.begin();  itLv0!= _clusterIndexHierarchical.end(); itLv0++)
      { 
        std::vector<int> clusterIds = itLv0->second;
        int type = itLv0->first;
        int ele, gp;
        numericalMaterialBase::getTwoIntsFromType(type,ele,gp);
        fprintf(f1, "%d %d %d %d\n",ele,gp,clusterIds[0],clusterIds[1],_materialMap[type]);
      }
      fclose(f1);
      printf("done writing cluster data level 1 to file: %s \n",fname.c_str());       
    }     
  }
  
  if (saveSummaryFile)
  { 
    std::vector<std::string> splitFileName = SplitFileName(summaryFileName);
    std::string fname = splitFileName[1]+"_nbClusters_"+std::to_string(_totalNumberOfClusters)+splitFileName[2];
    FILE* f0 = fopen(fname.c_str(),"w");
    printf("start writing cluster summary level 0 to file: %s\n",fname.c_str());
    std::string str0 ="CLUSTER_ID MAT_NUM VOLUME STD";
    // write data
    for (int i=0; i<6; i++)
    {
      for (int j=0; j<6; j++)
      {
        str0 += " A_"+std::to_string(i)+std::to_string(j);
      }
    }
    fprintf(f0,"%s\n",str0.c_str());
    // write data
    for (std::map<int, STensor43>::const_iterator it = _clusterAverageStrainConcentrationTensorMap.begin();  it!= _clusterAverageStrainConcentrationTensorMap.end(); it++)
    { 
      int cluster = it->first;
      int matNum = _clusterMaterialMapLevel0[cluster];
      double volume = _clusterVolumeMapLevel0[cluster];
      double stdev = 0.;
      fprintf(f0,"%d %d %.16g %.16g",cluster, matNum, volume, stdev);
      const STensor43& Amean = it->second;
      static fullMatrix<double> mat(6,6);
      STensorOperation::fromSTensor43ToFullMatrix(Amean,mat);
      printf("writing data for cluster %d\n",cluster);
      mat.print("average strain concentration tensor");
      for (int i=0; i<6; i++)
      {
        for (int j=0; j<6; j++)
        {
          fprintf(f0, " %.16g",mat(i,j));
        }
      }
      fprintf(f0, "\n");
    }
    fclose(f0);
    printf("done writing cluster summary level 0 to file: %s \n",fname.c_str());
    
    if (_numberOfLevels>1)
    {
      std::vector<std::string> splitFileName = SplitFileName(summaryFileName);
      std::string fname = splitFileName[1]+"_level1_nbClusters_"+std::to_string(_totalNumberOfClusters)+splitFileName[2];
      FILE* f1 = fopen(fname.c_str(),"w");
      printf("start writing cluster summary level 1 to file: %s\n",fname.c_str());
      std::string str1 ="CLUSTER_LV0 CLUSTER_LV1 MAT_NUM VOLUME";
      // write data
      for (int i=0; i<6; i++)
      {
        for (int j=0; j<6; j++)
        {
          str1 += " A_"+std::to_string(i)+std::to_string(j);
        }
      }
      fprintf(f1,"%s\n",str1.c_str());
      // write data
      for (std::map<int, std::map<int, STensor43> >::const_iterator itLv0 = _clusterAverageStrainConcentrationTensorMapLevel1.begin();  
                                                                    itLv0!= _clusterAverageStrainConcentrationTensorMapLevel1.end(); itLv0++)
      { 
        int clusterLv0 = itLv0->first;
        const std::map<int, STensor43>& clusterStrainConcOnLevel1 = itLv0->second;
        std::map<int, int>& clusterMaterialMapOnLevel1 = _clusterMaterialMapLevel1[clusterLv0];
        std::map<int, double>& clusterVolumeMapOnLevel1 = _clusterVolumeMapLevel1[clusterLv0];        
        for (std::map<int, STensor43>::const_iterator itLv1 = clusterStrainConcOnLevel1.begin();  itLv1!= clusterStrainConcOnLevel1.end(); itLv1++)
        { 
          int clusterLv1 = itLv1->first;
          const int& matNum = clusterMaterialMapOnLevel1[clusterLv1];
          const double& volume = clusterVolumeMapOnLevel1[clusterLv1];
          fprintf(f1,"%d %d %d %.16g",clusterLv0, clusterLv1, matNum, volume);
          const STensor43& Amean = itLv1->second;
          static fullMatrix<double> mat(6,6);
          STensorOperation::fromSTensor43ToFullMatrix(Amean,mat);
          printf("writing data for clusters level 0, level 1: %d %d\n",clusterLv0,clusterLv1);
          mat.print("average strain concentration tensor");
          for (int i=0; i<6; i++)
          {
            for (int j=0; j<6; j++)
            {
              fprintf(f1, " %.16g",mat(i,j));
            }
          }
          fprintf(f1, "\n");
        }
      }
      fclose(f1);
      printf("done writing cluster summary level 1 to file: %s \n",fname.c_str());
    }    
  }  
  return true;
};



void Clustering::loadClusterSummaryFromFile(const std::string fileName)
{
  FILE* f = fopen(fileName.c_str(),"r");
  if (f !=NULL) {
    printf("start reading file: %s\n",fileName.c_str());
    if(!feof(f))
    {
      char what[256];
      fscanf(f, "%s", what);
      printf("what = %s\n",what);
      fscanf(f, "%s", what);
      printf("what = %s\n",what);
      fscanf(f, "%s", what);
      printf("what = %s\n",what);
      fscanf(f, "%s", what);
      printf("what = %s\n",what);

      for (int i=0; i<6; i++)
      {
        for (int j=0; j<6; j++)
        {
          fscanf(f, "%s", what);
          printf("what = %s\n",what);
        }
      }
      if(strcmp(what, "A_55"))
      {
        Msg::Error("file format in %s is not correct\n",fileName.c_str());
        Msg::Exit(0);
      }
    }
    while (1){
      if(feof(f))
        break;
      int cluster =0;
      int matNum = 0;
      double volume = 0;
      double stdev = 0.;
      if (fscanf(f, "%d %d %lf %lf", &cluster,&matNum,&volume,&stdev)==4)
      {
        static fullMatrix<double> matTensor(6,6);
        for (int i=0; i<6; i++)
        {
          for (int j=0; j<6; j++)
          {
            double val=0;
            if (fscanf(f,"%lf",&val)==1)
              matTensor(i,j) = val;
          }
        }
        if (matNum >0)
        {
          printf("load data in cluster %d matnum = %d volume = %e stdev = %e \n",cluster,matNum,volume,stdev);
          matTensor.print("matTensor");
          
          _clusterMaterialMap[cluster] = matNum;
          _clusterVolumeMap[cluster] = volume;
          _clusterSTDMap[cluster] = stdev;
          STensor43& T = _clusterAverageStrainConcentrationTensorMap[cluster];
          STensorOperation::fromFullMatrixToSTensor43(matTensor,T);
        }
      }
    }
    printf("done reading file: %s\n",fileName.c_str());
    fclose(f);
    
    _totalNumberOfClusters = _clusterVolumeMap.size();
    printf("total number of cluster from file %s = %d\n",fileName.c_str(),_totalNumberOfClusters);
  }
  else
  {
    Msg::Error("File %s does not exist !!!",fileName.c_str());
    Msg::Exit(0);
  }
}



void Clustering::loadClusterSummaryLevel1FromFile(const std::string fileName)
{
  _numberOfLevels = 2;
  FILE* f = fopen(fileName.c_str(),"r");
  if (f !=NULL) {
    printf("start reading file: %s\n",fileName.c_str());
    if(!feof(f))
    {
      char what[256];
      fscanf(f, "%s", what);
      printf("what = %s\n",what);
      fscanf(f, "%s", what);
      printf("what = %s\n",what);
      fscanf(f, "%s", what);
      printf("what = %s\n",what);
      fscanf(f, "%s", what);
      printf("what = %s\n",what);

      for (int i=0; i<6; i++)
      {
        for (int j=0; j<6; j++)
        {
          fscanf(f, "%s", what);
          printf("what = %s\n",what);
        }
      }
      if(strcmp(what, "A_55"))
      {
        Msg::Error("file format in %s is not correct\n",fileName.c_str());
        Msg::Exit(0);
      }
    }
    while (1){
      if(feof(f))
        break;
      int clusterLv0 =0;
      int clusterLv1 =0;
      int matNum = 0;
      double volume = 0;
      if (fscanf(f, "%d %d %d %lf", &clusterLv0,&clusterLv1,&matNum,&volume)==4)
      {
        static fullMatrix<double> matTensor(6,6);
        for (int i=0; i<6; i++)
        {
          for (int j=0; j<6; j++)
          {
            double val=0;
            if (fscanf(f,"%lf",&val)==1)
              matTensor(i,j) = val;
          }
        }
        if (matNum >0)
        {
          printf("load data in cluster level 0, level 1: %d %d matnum = %d volume = %e \n",clusterLv0,clusterLv1,matNum,volume);
          matTensor.print("matTensor");
          
          std::map<int, int>& clusterMaterialMapOnLevel1 = _clusterMaterialMapLevel1[clusterLv0];
          clusterMaterialMapOnLevel1[clusterLv1] = matNum;
          
          std::map<int, double>& clusterVolumeMapOnLevel1 = _clusterVolumeMapLevel1[clusterLv0];
          clusterVolumeMapOnLevel1[clusterLv1] = volume;
          
          std::map<int, STensor43>& clusterAverageStrainConcentrationTensorMapOnLevel1 = _clusterAverageStrainConcentrationTensorMapLevel1[clusterLv0];
          
          STensor43& T = clusterAverageStrainConcentrationTensorMapOnLevel1[clusterLv1];
          STensorOperation::fromFullMatrixToSTensor43(matTensor,T);
        }
      }
    }
    printf("done reading file: %s\n",fileName.c_str());
    fclose(f);
    
    for (std::map<int, std::map<int, double>>::const_iterator itLv0 = _clusterVolumeMapLevel1.begin();  itLv0!= _clusterVolumeMapLevel1.end(); itLv0++)
    { 
      int clLv0 = itLv0->first;
      std::map<int, double> clusterVolumeMapOnLevel1 = itLv0->second;
      _numberOfClustersLevel1[clLv0] = clusterVolumeMapOnLevel1.size();
      printf("cluster no. level 0: clLv0. Total number of subclusters from file %s = %d\n",fileName.c_str(),_numberOfClustersLevel1[clLv0]);
    }
  }
  else
  {
    Msg::Error("File %s does not exist !!!",fileName.c_str());
    Msg::Exit(0);
  }
}



void Clustering::loadReferenceStiffnessFromFile(const std::string fileName)
{
  FILE* f = fopen(fileName.c_str(),"r");
  if (f !=NULL) {
    printf("start reading file: %s\n",fileName.c_str());
    if(!feof(f))
    {
      char what[256];
      for (int i=0; i<6; i++)
      {
        for (int j=0; j<6; j++)
        {
          fscanf(f, "%s", what);
          printf("what = %s\n",what);
        }
      }
      if(strcmp(what, "C_el_55"))
      {
        Msg::Error("file format in %s is not correct\n",fileName.c_str());
        Msg::Exit(0);
      }
    }
    while (1){
      if(feof(f))
        break;

      static fullMatrix<double> matTensor(6,6);
      for (int i=0; i<6; i++)
      {
        for (int j=0; j<6; j++)
        {
          double val=0;
          if (fscanf(f,"%lf",&val)==1)
            matTensor(i,j) = val;
        }
      }
      STensor43& T = _referenceStiffness;
      STensorOperation::fromFullMatrixToSTensor43(matTensor,T);
    }
    printf("done reading file: %s\n",fileName.c_str());
    fclose(f);
  }
  else
  {
    Msg::Error("File %s does not exist !!!",fileName.c_str());
    Msg::Exit(0);
  }
}

void Clustering::loadReferenceStiffnessIsoFromFile(const std::string fileName)
{
  FILE* f = fopen(fileName.c_str(),"r");
  if (f !=NULL) {
    printf("start reading file: %s\n",fileName.c_str());
    if(!feof(f))
    {
      char what[256];
      for (int i=0; i<6; i++)
      {
        for (int j=0; j<6; j++)
        {
          fscanf(f, "%s", what);
          printf("what = %s\n",what);
        }
      }
      if(strcmp(what, "C_el_55"))
      {
        Msg::Error("file format in %s is not correct\n",fileName.c_str());
        Msg::Exit(0);
      }
    }
    while (1){
      if(feof(f))
        break;

      static fullMatrix<double> matTensor(6,6);
      for (int i=0; i<6; i++)
      {
        for (int j=0; j<6; j++)
        {
          double val=0;
          if (fscanf(f,"%lf",&val)==1)
            matTensor(i,j) = val;
        }
      }
      STensor43& T = _referenceStiffnessIso;
      STensorOperation::fromFullMatrixToSTensor43(matTensor,T);
    }
    printf("done reading file: %s\n",fileName.c_str());
    fclose(f);
  }
  else
  {
    Msg::Error("File %s does not exist !!!",fileName.c_str());
    Msg::Exit(0);
  }
}

void Clustering::loadClusterSummaryHomogenizedFrameFromFile(const std::string fileName)
{
  FILE* f = fopen(fileName.c_str(),"r");
  if (f !=NULL) {
    printf("start reading file: %s\n",fileName.c_str());
    if(!feof(f))
    {
      char what[256];
      fscanf(f, "%s", what);
      printf("what = %s\n",what);
      fscanf(f, "%s", what);
      printf("what = %s\n",what);
      fscanf(f, "%s", what);
      printf("what = %s\n",what);
      fscanf(f, "%s", what);
      printf("what = %s\n",what);

      for (int i=0; i<6; i++)
      {
        for (int j=0; j<6; j++)
        {
          fscanf(f, "%s", what);
          printf("what = %s\n",what);
        }
      }
      if(strcmp(what, "A_55"))
      {
        Msg::Error("file format in %s is not correct\n",fileName.c_str());
        Msg::Exit(0);
      }
    }
    while (1){
      if(feof(f))
        break;
      int cluster =0;
      int matNum = 0;
      double volume = 0;
      double stdev = 0.;
      if (fscanf(f, "%d %d %lf %lf", &cluster,&matNum,&volume,&stdev)==4)
      {
        static fullMatrix<double> matTensor(6,6);
        for (int i=0; i<6; i++)
        {
          for (int j=0; j<6; j++)
          {
            double val=0;
            if (fscanf(f,"%lf",&val)==1)
              matTensor(i,j) = val;
          }
        }
        if (matNum >0)
        {
          printf("load data in cluster %d matnum = %d volume = %e stdev = %e \n",cluster,matNum,volume,stdev);
          matTensor.print("matTensor");
          
          _clusterMaterialMap[cluster] = matNum;
          _clusterVolumeMap[cluster] = volume;
          _clusterSTDMap[cluster] = stdev;
          STensor43& T = _clusterAverageStrainConcentrationTensorMap[cluster];
          STensorOperation::fromFullMatrixToSTensor43(matTensor,T);
        }
      }
    }
    printf("done reading file: %s\n",fileName.c_str());
    fclose(f);
    _totalNumberOfClusters = _clusterVolumeMap.size();
    printf("total number of cluster from file %s = %d\n",fileName.c_str(),_totalNumberOfClusters);
    _clusterMaterialMap.erase(_totalNumberOfClusters-1);
    _clusterVolumeMap.erase(_totalNumberOfClusters-1);
    _clusterSTDMap.erase(_totalNumberOfClusters-1);
    _clusterAverageStrainConcentrationTensorMap.erase(_totalNumberOfClusters-1);
    _totalNumberOfClusters = _clusterVolumeMap.size();
    printf("total number of cluster from file %s = %d\n",fileName.c_str(),_totalNumberOfClusters);
  }
  else
  {
    Msg::Error("File %s does not exist !!!",fileName.c_str());
    Msg::Exit(0);
  }
}

void Clustering::loadClusterStandDevFromFile(const std::string fileName)
{
  FILE* f = fopen(fileName.c_str(),"r");
  if (f !=NULL) {
    printf("start reading file: %s\n",fileName.c_str());
    if(!feof(f))
    {
      char what[256];
      fscanf(f, "%s", what);
      printf("what = %s\n",what);
      fscanf(f, "%s", what);
      printf("what = %s\n",what);
      fscanf(f, "%s", what);
      printf("what = %s\n",what);
      for (int i=0; i<6; i++)
      {
        for (int j=0; j<6; j++)
        {
          fscanf(f, "%s", what);
          printf("what = %s\n",what);
        }
      }
      if(strcmp(what, "STD_A_55"))
      {
        Msg::Error("file format in %s is not correct\n",fileName.c_str());
        Msg::Exit(0);
      }
    }
    while (1){
      if(feof(f))
        break;
      int cluster =0;
      int matNum = 0;
      double volume = 0;
      if (fscanf(f, "%d %d %lf", &cluster,&matNum,&volume)==3)
      {
        static fullMatrix<double> matTensor(6,6);
        for (int i=0; i<6; i++)
        {
          for (int j=0; j<6; j++)
          {
            double val=0;
            if (fscanf(f,"%lf",&val)==1)
              matTensor(i,j) = val;
          }
        }
        if (matNum >0)
        {
          printf("load data in cluster %d matnum = %d \n",cluster,matNum);
          matTensor.print("matTensor");
          STensor43& T = _clusterSTDTensorMap[cluster];
          STensorOperation::fromFullMatrixToSTensor43(matTensor,T);
        }
      }
    }
    printf("done reading file: %s\n",fileName.c_str());
    fclose(f);
  }
  else
  {
    Msg::Error("File %s does not exist !!!",fileName.c_str());
    Msg::Exit(0);
  }
}


void Clustering::loadClusterPlasticEqSummaryFromFile(const std::string fileName)
{
  FILE* fp = fopen(fileName.c_str(),"r");
  if (fp !=NULL) {
    printf("start reading file: %s\n",fileName.c_str());
    if(!feof(fp))
    {
      char what[256];
      fscanf(fp, "%s", what);
      printf("what = %s\n",what);
      fscanf(fp, "%s", what);
      printf("what = %s\n",what);
      fscanf(fp, "%s", what);
      printf("what = %s\n",what);

      for (int i=0; i<6; i++)
      {
        fscanf(fp, "%s", what);
        printf("what = %s\n",what);
      }
      if(strcmp(what, "a_pl_5"))
      {
        Msg::Error("file format in %s is not correct\n",fileName.c_str());
        Msg::Exit(0);
      }
    }
    while (1){
      if(feof(fp))
        break;
      int cluster =0;
      int matNum = 0;
      double volume = 0;
      if (fscanf(fp, "%d %d %lf", &cluster,&matNum,&volume)==3)
      {
        static fullVector<double> vec(6);
        for (int i=0; i<6; i++)
        {
          double val=0;
          if (fscanf(fp,"%lf",&val)==1)
            vec(i) = val;
        }
        if (matNum >0)
        {
          printf("load data in cluster %d matnum = %d volume = %e \n",cluster,matNum,volume);
          vec.print("vec");

          fullVector<double>& t = _clusterAveragePlasticEqStrainConcentrationVectorMap[cluster];
          t = vec;
        }
      }
    }
    printf("done reading file: %s\n",fileName.c_str());
    fclose(fp);
  }
  else
  {
    Msg::Error("File %s does not exist !!!",fileName.c_str());
    Msg::Exit(0);
  }
}

void Clustering::loadClusterPlasticEqSummaryGeometricFromFile(const std::string fileName)
{
  FILE* fp = fopen(fileName.c_str(),"r");
  if (fp !=NULL) {
    printf("start reading file: %s\n",fileName.c_str());
    if(!feof(fp))
    {
      char what[256];
      fscanf(fp, "%s", what);
      printf("what = %s\n",what);
      fscanf(fp, "%s", what);
      printf("what = %s\n",what);
      fscanf(fp, "%s", what);
      printf("what = %s\n",what);

      for (int i=0; i<6; i++)
      {
        fscanf(fp, "%s", what);
        printf("what = %s\n",what);
      }
      if(strcmp(what, "a_pl_5"))
      {
        Msg::Error("file format in %s is not correct\n",fileName.c_str());
        Msg::Exit(0);
      }
    }
    while (1){
      if(feof(fp))
        break;
      int cluster =0;
      int matNum = 0;
      double volume = 0;
      if (fscanf(fp, "%d %d %lf", &cluster,&matNum,&volume)==3)
      {
        static fullVector<double> vec(6);
        for (int i=0; i<6; i++)
        {
          double val=0;
          if (fscanf(fp,"%lf",&val)==1)
            vec(i) = val;
        }
        if (matNum >0)
        {
          printf("load data in cluster %d matnum = %d volume = %e \n",cluster,matNum,volume);
          vec.print("vec");
          
          fullVector<double>& t = _clusterAverageGeometricPlasticEqStrainConcentrationVectorMap[cluster];
          t = vec;
        }
      }
    }
    printf("done reading file: %s\n",fileName.c_str());
    fclose(fp);
  }
  else
  {
    Msg::Error("File %s does not exist !!!",fileName.c_str());
    Msg::Exit(0);
  }
}

void Clustering::loadClusterPlasticEqSummaryHarmonicFromFile(const std::string fileName)
{
  FILE* fp = fopen(fileName.c_str(),"r");
  if (fp !=NULL) {
    printf("start reading file: %s\n",fileName.c_str());
    if(!feof(fp))
    {
      char what[256];
      fscanf(fp, "%s", what);
      printf("what = %s\n",what);
      fscanf(fp, "%s", what);
      printf("what = %s\n",what);
      fscanf(fp, "%s", what);
      printf("what = %s\n",what);

      for (int i=0; i<6; i++)
      {
        fscanf(fp, "%s", what);
        printf("what = %s\n",what);
      }
      if(strcmp(what, "a_pl_5"))
      {
        Msg::Error("file format in %s is not correct\n",fileName.c_str());
        Msg::Exit(0);
      }
    }
    while (1){
      if(feof(fp))
        break;
      int cluster =0;
      int matNum = 0;
      double volume = 0;
      if (fscanf(fp, "%d %d %lf", &cluster,&matNum,&volume)==3)
      {
        static fullVector<double> vec(6);
        for (int i=0; i<6; i++)
        {
          double val=0;
          if (fscanf(fp,"%lf",&val)==1)
            vec(i) = val;
        }
        if (matNum >0)
        {
          printf("load data in cluster %d matnum = %d volume = %e \n",cluster,matNum,volume);
          vec.print("vec");
          
          fullVector<double>& t = _clusterAverageHarmonicPlasticEqStrainConcentrationVectorMap[cluster];
          t = vec;
        }
      }
    }
    printf("done reading file: %s\n",fileName.c_str());
    fclose(fp);
  }
  else
  {
    Msg::Error("File %s does not exist !!!",fileName.c_str());
    Msg::Exit(0);
  }
}


void Clustering::loadClusterPlasticEqSummaryPowerFromFile(const std::string fileName)
{
  FILE* fp = fopen(fileName.c_str(),"r");
  if (fp !=NULL) {
    printf("start reading file: %s\n",fileName.c_str());
    if(!feof(fp))
    {
      char what[256];
      fscanf(fp, "%s", what);
      printf("what = %s\n",what);
      fscanf(fp, "%s", what);
      printf("what = %s\n",what);
      fscanf(fp, "%s", what);
      printf("what = %s\n",what);

      for (int i=0; i<6; i++)
      {
        fscanf(fp, "%s", what);
        printf("what = %s\n",what);
      }
      if(strcmp(what, "a_pl_5"))
      {
        Msg::Error("file format in %s is not correct\n",fileName.c_str());
        Msg::Exit(0);
      }
    }
    while (1){
      if(feof(fp))
        break;
      int cluster =0;
      int matNum = 0;
      double volume = 0;
      if (fscanf(fp, "%d %d %lf", &cluster,&matNum,&volume)==3)
      {
        static fullVector<double> vec(6);
        for (int i=0; i<6; i++)
        {
          double val=0;
          if (fscanf(fp,"%lf",&val)==1)
            vec(i) = val;
        }
        if (matNum >0)
        {
          printf("load data in cluster %d matnum = %d volume = %e \n",cluster,matNum,volume);
          vec.print("vec");
          
          fullVector<double>& t = _clusterAveragePowerPlasticEqStrainConcentrationVectorMap[cluster];
          t = vec;
        }
      }
    }
    printf("done reading file: %s\n",fileName.c_str());
    fclose(fp);
  }
  else
  {
    Msg::Error("File %s does not exist !!!",fileName.c_str());
    Msg::Exit(0);
  }
}


void Clustering::loadClusterFiberOrientationFromFile(const std::string fileNameOri)
{
  FILE* fp = fopen(fileNameOri.c_str(),"r");
  if (fp !=NULL) {
    printf("start reading file: %s\n",fileNameOri.c_str());
    if(!feof(fp))
    {
      char what[256];
      fscanf(fp, "%s", what);
      printf("what = %s\n",what);
      fscanf(fp, "%s", what);
      printf("what = %s\n",what);
      for (int i=0; i<3; i++)
      {
        fscanf(fp, "%s", what);
        printf("what = %s\n",what);
      }
      if(strcmp(what, "E3"))
      {
        Msg::Error("file format in %s is not correct\n",fileNameOri.c_str());
        Msg::Exit(0);
      }
    }
    while (1){
      if(feof(fp))
        break;
      int cluster =0;
      int matNum = 0;
      if (fscanf(fp, "%d %d", &cluster,&matNum)==2)
      {
        static fullVector<double> vec(3);
        for (int i=0; i<3; i++)
        {
          double val=0;
          if (fscanf(fp,"%lf",&val)==1)
            vec(i) = val;
        }
        if (matNum >0)
        {
          printf("load data in cluster %d matnum = %d \n",cluster,matNum);
          vec.print("vec");

          fullVector<double>& t = _clusterMeanFiberOrientationMap[cluster];
          t = vec;
        }
      }
    }
    printf("done reading file: %s\n",fileNameOri.c_str());
    fclose(fp);
  }
  else
  {
    Msg::Error("File %s does not exist !!!",fileNameOri.c_str());
    Msg::Exit(0);
  }
}




void Clustering::saveAllView(int gp)
{
  std::string id = "";
  if (gp >=0)
  {
    id = "_GP"+std::to_string(gp);
  }
  else
  {
    id = "_MeanByElement";
  }
  std::string fileName = "AllView"+id+"_nbClusters_"+std::to_string(_totalNumberOfClusters)+".msh";
  Msg::Info("start saving all views to file: %s",fileName.c_str());
  viewClustering view(*this);
  std::vector<int> allView;
  for (int i=0; i< 38; i++)
  {
    allView.push_back(i);
  }
  view.archive(fileName,1.,1, allView,gp);
  Msg::Info("done saving all views  to file:: %s",fileName.c_str());
}


void Clustering::saveFieldToView(int name, int gp)
{
  Msg::Info("start saving %s to file",Clustering::ToString(name).c_str());
  std::string id = "";
  if (gp >=0)
  {
    id = "_GP"+std::to_string(gp);
  }
  else
  {
    id = "_MeanByElement";
  }
  std::string fileName = Clustering::ToString(name)+id+"_nbClusters_"+std::to_string(_totalNumberOfClusters)+".msh";
  viewClustering view(*this);
  std::vector<int> allView(1,name);
  view.archive(fileName,1.,1, allView,gp);
  Msg::Info("done saving %s to file",Clustering::ToString(name).c_str());
};

void Clustering::getCluster(int i, std::set<int>& cluster) const
{
  cluster.clear();
  for (std::map<int,int>::const_iterator it = _clusterIndex.begin(); it != _clusterIndex.end(); it++)
  {
    if (it->second == i)
    {
      cluster.insert(it->first);
    }
  }
}

void Clustering::getClusterLevel1(int clusterLv0, int clusterLv1, std::set<int>& cluster) const
{
  cluster.clear();
  for (std::map<int, std::vector<int>>::const_iterator itGP = _clusterIndexHierarchical.begin(); itGP != _clusterIndexHierarchical.end(); itGP++)
  {
    const std::vector<int>& clusterNbsPerLevel = itGP->second;
    if (clusterNbsPerLevel[0] == clusterLv0 and clusterNbsPerLevel[1] == clusterLv1)
    {
      cluster.insert(itGP->first);
    }
  }
}

const STensor3& Clustering::getEigenStrain(int GPIndex, bool homogeneous) const
{
  if (homogeneous)
  {  
    int eleNum, gpt;
    numericalMaterialBase::getTwoIntsFromType(GPIndex,eleNum,gpt);
    
    if(_level == 0)
    {
      int cluster = getClusterIndex(eleNum,gpt);
      std::map<int,STensor3>::const_iterator itF = _allEigenStrains.find(cluster);
    
      if (itF == _allEigenStrains.end())
      {
        Msg::Error("eigen strain for cluster %d is not found",cluster);
        static STensor3 a;
        return a;
      }
      else
      {
        return itF->second;
      }
    }
    else if(_level == 1)
    {
      std::vector<int> clusterIndices = getClusterIndicesHierarchical(eleNum,gpt);    
      std::map<int,std::map<int,STensor3>>::const_iterator itLv0 = _allEigenStrainsLevel1.find(clusterIndices[0]);
      const std::map<int,STensor3>& eigenstrainsOnLevel1 = itLv0->second;
      std::map<int,STensor3>::const_iterator itF = eigenstrainsOnLevel1.find(clusterIndices[1]);
    
      if (itLv0 == _allEigenStrainsLevel1.end() or itF == eigenstrainsOnLevel1.end())
      {
        Msg::Error("eigen strain for cluster %d %d is not found",clusterIndices[0],clusterIndices[1]);
        static STensor3 a;
        return a;
      }
      else
      {
        return itF->second;
      }
    }    
  }
  else
  {  
    int eleNum, gpt;
    numericalMaterialBase::getTwoIntsFromType(GPIndex,eleNum,gpt);
    int cluster = getClusterIndex(eleNum,gpt);
    std::map<int,STensor3>::const_iterator itF = _allEigenStrains.find(cluster);
    if (itF == _allEigenStrains.end())
    {
      Msg::Error("eigen strain for cluster %d is not found",cluster);
      static STensor3 a;
      return a;
    }      
  }
};

const STensor3& Clustering::getEigenStress(int GPIndex, bool homogeneous) const
{
  if (homogeneous)
  {  
    int eleNum, gpt;
    numericalMaterialBase::getTwoIntsFromType(GPIndex,eleNum,gpt);
    
    if(_level == 0)
    {
      int cluster = getClusterIndex(eleNum,gpt);
      std::map<int,STensor3>::const_iterator itF = _allEigenStresses.find(cluster);
      if (itF == _allEigenStresses.end())
      {
        Msg::Error("eigen stress for cluster %d is not found",cluster);
        static STensor3 a;
        return a;
      }
      else
      {
        return itF->second;
      }
    }
    else if(_level == 1)
    {
      std::vector<int> clusterIndices = getClusterIndicesHierarchical(eleNum,gpt);    
      std::map<int,std::map<int,STensor3>>::const_iterator itLv0 = _allEigenStressesLevel1.find(clusterIndices[0]);
      const std::map<int,STensor3>& eigenstressesOnLevel1 = itLv0->second;
      std::map<int,STensor3>::const_iterator itF = eigenstressesOnLevel1.find(clusterIndices[1]);
    
      if (itLv0 == _allEigenStressesLevel1.end() or itF == eigenstressesOnLevel1.end())
      {
        Msg::Error("eigen stress for cluster %d %d is not found",clusterIndices[0],clusterIndices[1]);
        static STensor3 a;
        return a;
      }
      else
      {
        return itF->second;
      }
    }
  }
  else
  {  
    int eleNum, gpt;
    numericalMaterialBase::getTwoIntsFromType(GPIndex,eleNum,gpt);
    int cluster = getClusterIndex(eleNum,gpt);
    std::map<int,STensor3>::const_iterator itF = _allEigenStresses.find(cluster);
    if (itF == _allEigenStresses.end())
    {
      Msg::Error("eigen stress for cluster %d is not found",cluster);
      static STensor3 a;
      return a;
    }      
  }
};

void Clustering::computeAverageStrainCluster(const IPField* ipf , const std::set<int>& cluster, STensor3& Fhomo) const
{
  STensorOperation::zero(Fhomo);
  double volume = 0.;
  for (std::set<int>::const_iterator it = cluster.begin(); it != cluster.end(); it++)
  {
    int type = *it;
    int eleNum, gp;
    numericalMaterialBase::getTwoIntsFromType(type,eleNum,gp);
    const AllIPState::ipstateElementContainer *vips = ipf->getAips()->getIPstate(eleNum);
    const IPVariable* ipv = (*vips)[gp]->getState(IPStateBase::current);
    const ipFiniteStrain* ipfinite = static_cast<const ipFiniteStrain*>(ipv);
    
    const STensor3& Flocal = ipfinite->getConstRefToDeformationGradient();
    std::map<int, fullVector<double> >::const_iterator itMapGau = _mapGaussPoints.find(type);
    const fullVector<double>& gpweightCor = itMapGau->second;
    Fhomo.daxpy(Flocal,gpweightCor(3));
    volume += gpweightCor(3);
  }
  if (volume ==0.) 
  {
    STensorOperation::unity(Fhomo);
  }
  else
  {
    Fhomo *= (1./volume);
  } 
}


void Clustering::computeInteractionTensorForCluster(nonLinearMechSolver& solver, int clusterI, std::map<std::pair<int,int>, fullMatrix<double> >& DJI)
{
  if (!solver.isInitialized())
  {
    solver.initMicroSolver();
  }
    
  int dim = solver.getDim();
   
  std::map<int, STensor3> allEigenField;
  allEigenField.clear();
  double fact_eig = 0.;
  if(_eigenField==0)
  {
    fact_eig = 1.0e-3;
    strainForLoadingCases(dim,fact_eig,allEigenField); 
  }
  else if(_eigenField==1)
  {
    fact_eig = 1.0; 
    stressForLoadingCases(dim,fact_eig,allEigenField);
  }

  for (std::map<int, STensor3>::const_iterator it = allEigenField.begin(); it!= allEigenField.end(); it++)
  {    
    int i = it->first;    
    _allEigenStrains.clear();
    _allEigenStresses.clear();
    // get eigen strain/stress
    for (std::map<int, int>::const_iterator itMapMat = _clusterMaterialMap.begin(); itMapMat != _clusterMaterialMap.end(); itMapMat++)
    {
      if (clusterI == itMapMat->first)
      {
        if(_eigenField==0)
        {
          const STensor3& eigenStrain = it->second;
          Msg::Info("for Cluster %d loading case: %d", clusterI,i);
          eigenStrain.print("eigenstrain tensor: ");
          _allEigenStrains.insert(std::pair<int,STensor3>(itMapMat->first,eigenStrain));
          _allEigenStresses.insert(std::pair<int,STensor3>(itMapMat->first,STensor3(0.)));
        }
        else if(_eigenField==1)
        {
          const STensor3& eigenStress = it->second;
          Msg::Info("for Cluster %d loading case: %d", clusterI,i);
          eigenStress.print("eigenstress tensor: ");
          _allEigenStresses.insert(std::pair<int,STensor3>(itMapMat->first,eigenStress));
          _allEigenStrains.insert(std::pair<int,STensor3>(itMapMat->first,STensor3(1.)));
        }
      }
      else
      {
        _allEigenStrains.insert(std::pair<int,STensor3>(itMapMat->first,STensor3(1.)));
        _allEigenStresses.insert(std::pair<int,STensor3>(itMapMat->first,STensor3(0.)));
      }
    }
    
    static STensor3 Funity(1.);
    solver.getMicroBC()->setDeformationGradient(Funity);

      // set tangent averaging flag
    solver.tangentAveragingFlag(false);
    // set eigven strain to material law
    // set view name
    // save view
    //std::string filenamPrefix = "disp_eigenStrainMode_cluster"+std::to_string(clusterI)+"Mode"+std::to_string(i);
    //solver.getUnknownField()->setFileNamePrefix(filenamPrefix);
    //filenamPrefix = "stress_eigenStrainMode_cluster"+std::to_string(clusterI)+"Mode"+std::to_string(i);
    //solver.getIPField()->setFileNamePrefix(filenamPrefix);
    
    // solve micro problem
    bool success = solver.microSolve(true);
    // compute the average value in each cluster
    if (!success)
    {
      Msg::Error("solver cannot be solved !!!");
      Msg::Exit(0);
    }
    else
    {
      for (std::map<int, int>::const_iterator itMatMap = _clusterMaterialMap.begin(); itMatMap != _clusterMaterialMap.end(); itMatMap++)
      {
        int clusterJ = itMatMap->first;
        std::set<int> clusterJIPSet;
        getCluster(clusterJ,clusterJIPSet);
        // compute homogenized value for cluster J
        static STensor3 Fhomo;
        computeAverageStrainCluster(solver.getIPField(),clusterJIPSet,Fhomo);
        fullMatrix<double>& DIJval = DJI[std::pair<int,int>(clusterJ,clusterI)];
        if (DIJval.size1() != 6 and DIJval.size2() != 6)
        {
          //Msg::Warning("DIG is not yet allocated");
          DIJval.resize(6,6,true);
        }
        static STensor3 eps;
        static STensor3 I(1.);
        for (int ii =0; ii<3; ii++)
        {
          for (int jj=0; jj<3; jj++)
          {
            eps(ii,jj) = 0.5*(Fhomo(ii,jj)+Fhomo(jj,ii)) - I(ii,jj);
          }
        }
        if (i == 0 || i==1 || i==2)
        {
          DIJval(0,i)  = eps(0,0)/fact_eig; // A(00)(00 if i==0, 11 if i==1, 22 if i ==2)
          DIJval(1,i)  = eps(1,1)/fact_eig; // A(11)(00 if i==0, 11 if i==1, 22 if i ==2)
          DIJval(3,i)  = eps(0,1)/fact_eig; // A(01)(00 if i==0, 11 if i==1, 22 if i ==2)
          if (dim==3)
          {
          DIJval(2,i)  = eps(2,2)/fact_eig; // A(22)(00 if i==0, 11 if i==1, 22 if i ==2)
          DIJval(4,i)  = eps(0,2)/fact_eig; // A(02)(00 if i==0, 11 if i==1, 22 if i ==2)
          DIJval(5,i)  = eps(1,2)/fact_eig; // A(12)(00 if i==0, 11 if i==1, 22 if i ==2)    
          }         
        }
        else
        {
          // factor 2 for conversion in STensorOperation::fromSTensor43ToFullMatrix
          DIJval(0,i)  = 2.*eps(0,0)/fact_eig; // 2A(00) (01 if i==3, 02 if i==4, 12 if i ==5)
          DIJval(1,i)  = 2.*eps(1,1)/fact_eig; // 2A(11) (01 if i==3, 02 if i==4, 12 if i ==5)
          DIJval(3,i)  = 2.*eps(0,1)/fact_eig; // 2A(01) (01 if i==3, 02 if i==4, 12 if i ==5)
          if (dim==3)
          {
          DIJval(2,i)  = 2.*eps(2,2)/fact_eig; // 2A(22) (01 if i==3, 02 if i==4, 12 if i ==5)
          DIJval(4,i)  = 2.*eps(0,2)/fact_eig; // 2A(02) (01 if i==3, 02 if i==4, 12 if i ==5)
          DIJval(5,i)  = 2.*eps(1,2)/fact_eig; // 2A(12) (01 if i==3, 02 if i==4, 12 if i ==5)   
          }
        }
      }
      static int num=0;
      num ++;
      //solver.archiveData(num,num,false);
      solver.nextStep(num,num);
    };
  }
};

void Clustering::computeInteractionTensorForClusterLevel1(nonLinearMechSolver& solver, int clusterLv0, int clusterLv1, 
                                                          std::map<int, std::map<std::pair<int,int>, fullMatrix<double> > >& D_hierarchical)
{
  if (!solver.isInitialized())
  {
    solver.initMicroSolver();
  }
    
  int dim = solver.getDim();
 
  int clusterHost = clusterLv0;
  int clusterS = clusterLv1;
   
  std::map<int, STensor3> allEigenField;
  allEigenField.clear();
  double fact_eig_S = 0.;
  if(_eigenField==0)
  {
    fact_eig_S = 1.0e-3;
    strainForLoadingCases(dim,fact_eig_S,allEigenField); 
  }
  else if(_eigenField==1)
  {
    fact_eig_S = 1.0; 
    stressForLoadingCases(dim,fact_eig_S,allEigenField);
  }

  for (std::map<int, STensor3>::const_iterator it = allEigenField.begin(); it!= allEigenField.end(); it++)
  {    
    int i = it->first;    
    _allEigenStrains.clear();
    _allEigenStresses.clear();
    _allEigenStrainsLevel1.clear();
    _allEigenStressesLevel1.clear();
    // get eigen strain/stress
    for (std::map<int, std::map<int, int>>::const_iterator itMapMat = _clusterMaterialMapLevel1.begin(); itMapMat != _clusterMaterialMapLevel1.end(); itMapMat++)
    { 
      std::map<int, STensor3>& allEigenStrainsOnLevel1 = _allEigenStrainsLevel1[itMapMat->first];
      std::map<int, STensor3>& allEigenStressesOnLevel1 = _allEigenStressesLevel1[itMapMat->first];
      const std::map<int, int>& clusterMaterialMapOnLevel1 = itMapMat->second;
      for (std::map<int, int>::const_iterator itMapMatLevel1 = clusterMaterialMapOnLevel1.begin(); itMapMatLevel1 != clusterMaterialMapOnLevel1.end(); itMapMatLevel1++)
      {
        const int& cluster = itMapMatLevel1->first;
        if(itMapMat->first == clusterHost and cluster == clusterS)
        {
          if(_eigenField==0)
          {
            const STensor3& eigenStrain = it->second;
            Msg::Info("for Cluster %d %d loading case: %d", clusterHost, clusterS,i);
            eigenStrain.print("eigenstrain tensor: ");              
            allEigenStrainsOnLevel1.insert(std::pair<int,STensor3> (cluster,eigenStrain));
            allEigenStressesOnLevel1.insert(std::pair<int,STensor3> (cluster,STensor3(0.)));
          }
          else if(_eigenField==1)
          {
            const STensor3& eigenStress = it->second;
            Msg::Info("for Cluster %d %d loading case: %d", clusterHost, clusterS,i);
            eigenStress.print("eigenstress tensor: ");
            allEigenStressesOnLevel1.insert(std::pair<int,STensor3> (cluster,eigenStress));
            allEigenStrainsOnLevel1.insert(std::pair<int,STensor3> (cluster,STensor3(1.)));
          }
        }
        else
        {
          allEigenStrainsOnLevel1.insert(std::pair<int,STensor3> (cluster,STensor3(1.)));
          allEigenStressesOnLevel1.insert(std::pair<int,STensor3> (cluster,STensor3(0.)));
        }
      }
    }
    
    static STensor3 Funity(1.);
    solver.getMicroBC()->setDeformationGradient(Funity);
      // set tangent averaging flag
    solver.tangentAveragingFlag(false);    
    // solve micro problem
    bool success = solver.microSolve(true);
    // compute the average value in each cluster
    if (!success)
    {
      Msg::Error("solver cannot be solved !!!");
      Msg::Exit(0);
    }
    else
    {
      for (std::map<int, std::map<int, int>>::const_iterator itMapMat = _clusterMaterialMapLevel1.begin(); itMapMat != _clusterMaterialMapLevel1.end(); itMapMat++)
      {
        if(clusterHost == itMapMat->first)
        {
          std::set<int> clusterHost_IPSet;
          getCluster(clusterHost,clusterHost_IPSet);
          static STensor3 F_host;
          computeAverageStrainCluster(solver.getIPField(),clusterHost_IPSet,F_host);
          static STensor3 eps_host;
          static STensor3 I(1.);
          for (int ii =0; ii<3; ii++)
          {
            for (int jj=0; jj<3; jj++)
            {
              eps_host(ii,jj) = 0.5*(F_host(ii,jj)+F_host(jj,ii)) - I(ii,jj);
            }
          }
           
          const std::map<int, int>& clusterMaterialMapOnLevel1 = itMapMat->second;
          std::map<std::pair<int,int>, fullMatrix<double>>& D_level1 = D_hierarchical[clusterHost];
          for (std::map<int, int>::const_iterator itMapMatLevel1 = clusterMaterialMapOnLevel1.begin(); itMapMatLevel1 != clusterMaterialMapOnLevel1.end(); itMapMatLevel1++)
          {
            int clusterR = itMapMatLevel1->first;
        
            std::set<int> clusterR_IPSet;
            getClusterLevel1(clusterHost,clusterR,clusterR_IPSet);
        
            // compute homogenized value for cluster J
            static STensor3 F_R;
            computeAverageStrainCluster(solver.getIPField(),clusterR_IPSet,F_R);
            fullMatrix<double>& D_RS = D_level1[std::pair<int,int>(clusterR,clusterS)];
            if (D_RS.size1() != 6 and D_RS.size2() != 6)
            {
              //Msg::Warning("DIG is not yet allocated");
              D_RS.resize(6,6,true);
            }
            static STensor3 eps_R;
            static STensor3 I(1.);
            for (int ii =0; ii<3; ii++)
            {
              for (int jj=0; jj<3; jj++)
              {
                eps_R(ii,jj) = 0.5*(F_R(ii,jj)+F_R(jj,ii)) - I(ii,jj);
              }
            }
            
            std::map<int, std::map<int, STensor43> >::const_iterator itLv0 = _clusterAverageStrainConcentrationTensorMapLevel1.find(clusterHost); 
            const std::map<int, STensor43>& clusterStrainConcOnLevel1 = itLv0->second;
            std::map<int, STensor43>::const_iterator itLv1 = clusterStrainConcOnLevel1.find(clusterR);
            const STensor43& A_R_inHost = itLv1->second;
            STensor3 eps_R_induced(0.);
            
            if(_eigenField==0)
            {
              STensorOperation::multSTensor43STensor3(A_R_inHost,eps_host,eps_R_induced);
              eps_R -= eps_R_induced;
            }
            else if(_eigenField==1)
            {
              eps_R -= eps_host;
            }
            
            if (i == 0 || i==1 || i==2)
            {
              D_RS(0,i)  = eps_R(0,0)/fact_eig_S; // A(00)(00 if i==0, 11 if i==1, 22 if i ==2)
              D_RS(1,i)  = eps_R(1,1)/fact_eig_S; // A(11)(00 if i==0, 11 if i==1, 22 if i ==2)
              D_RS(3,i)  = eps_R(0,1)/fact_eig_S; // A(01)(00 if i==0, 11 if i==1, 22 if i ==2)
              if (dim==3)
              {
              D_RS(2,i)  = eps_R(2,2)/fact_eig_S; // A(22)(00 if i==0, 11 if i==1, 22 if i ==2)
              D_RS(4,i)  = eps_R(0,2)/fact_eig_S; // A(02)(00 if i==0, 11 if i==1, 22 if i ==2)
              D_RS(5,i)  = eps_R(1,2)/fact_eig_S; // A(12)(00 if i==0, 11 if i==1, 22 if i ==2)    
              }         
            }
            else
            {
              // factor 2 for conversion in STensorOperation::fromSTensor43ToFullMatrix
              D_RS(0,i)  = 2.*eps_R(0,0)/fact_eig_S; // 2A(00) (01 if i==3, 02 if i==4, 12 if i ==5)
              D_RS(1,i)  = 2.*eps_R(1,1)/fact_eig_S; // 2A(11) (01 if i==3, 02 if i==4, 12 if i ==5)
              D_RS(3,i)  = 2.*eps_R(0,1)/fact_eig_S; // 2A(01) (01 if i==3, 02 if i==4, 12 if i ==5)
              if (dim==3)
              {
              D_RS(2,i)  = 2.*eps_R(2,2)/fact_eig_S; // 2A(22) (01 if i==3, 02 if i==4, 12 if i ==5)
              D_RS(4,i)  = 2.*eps_R(0,2)/fact_eig_S; // 2A(02) (01 if i==3, 02 if i==4, 12 if i ==5)
              D_RS(5,i)  = 2.*eps_R(1,2)/fact_eig_S; // 2A(12) (01 if i==3, 02 if i==4, 12 if i ==5)   
              }
            }
          }
        }
      }
      static int num=0;
      num ++;
      //solver.archiveData(num,num,false);
      solver.nextStep(num,num);
    };
  }
};


void Clustering::computeEshelbyInteractionTensorForCluster(nonLinearMechSolver& solver, int clusterI, std::map<std::pair<int,int>, fullMatrix<double> >& DJI)
{
  int dim = solver.getDim();  
  
  std::map<int, STensor3> allEigenField;
  allEigenField.clear();
  double fact_eig = 0.;
  if(_eigenField==0)
  {
    fact_eig = 1.0e-3;
    strainForLoadingCases(dim,fact_eig,allEigenField); 
  }
  else if(_eigenField==1)
  {
    fact_eig = 1.0; 
    stressForLoadingCases(dim,fact_eig,allEigenField);
  }

  for (std::map<int, STensor3>::const_iterator it = allEigenField.begin(); it!= allEigenField.end(); it++)
  {    
    int i = it->first;
    _allEigenStrains.clear();
    _allEigenStresses.clear();
    // get eigen strain/stress for clusters
    for (std::map<int, int>::const_iterator itMapMat = _clusterMaterialMap.begin(); itMapMat != _clusterMaterialMap.end(); itMapMat++)
    {
      if (clusterI == itMapMat->first)
      {
        if(_eigenField==0)
        {
          const STensor3& eigenStrain = it->second;
          Msg::Info("for Cluster %d loading case: %d", clusterI,i);
          eigenStrain.print("eigenstrain tensor: ");
          _allEigenStrains.insert(std::pair<int,STensor3>(itMapMat->first,eigenStrain));
          _allEigenStresses.insert(std::pair<int,STensor3>(itMapMat->first,STensor3(0.)));
        }
        else if(_eigenField==1)
        {
          const STensor3& eigenStress = it->second;
          Msg::Info("for Cluster %d loading case: %d", clusterI,i);
          eigenStress.print("eigenstress tensor: ");
          _allEigenStresses.insert(std::pair<int,STensor3>(itMapMat->first,eigenStress));
          _allEigenStrains.insert(std::pair<int,STensor3>(itMapMat->first,STensor3(1.)));
        }
      }
      else
      {
        _allEigenStrains.insert(std::pair<int,STensor3>(itMapMat->first,STensor3(1.)));
        _allEigenStresses.insert(std::pair<int,STensor3>(itMapMat->first,STensor3(0.)));
      }
    }
   
    // set tangent averaging flag
    //solver.tangentAveragingFlag(false);
    // set eigven strain to material law
    // set view name
    // save view
    //std::string filenamPrefix = "disp_eigenStrainModeEshelby_cluster"+std::to_string(clusterI)+"Mode"+std::to_string(i);
    //solver.getUnknownField()->setFileNamePrefix(filenamPrefix);
    //filenamPrefix = "stress_eigenStrainModeEshelby_cluster"+std::to_string(clusterI)+"Mode"+std::to_string(i);
    //solver.getIPField()->setFileNamePrefix(filenamPrefix);
    
    // solve problem
    static int num=0;
    num ++;
    solver.oneStaticStep(double(num),1.,num);
    //
    bool success = true;
    // compute the average value in each cluster
    if (!success)
    {
      Msg::Error("solver cannot be solved !!!");
      Msg::Exit(0);
    }
    else
    {
      for (std::map<int, int>::const_iterator itMatMap = _clusterMaterialMap.begin(); itMatMap != _clusterMaterialMap.end(); itMatMap++)
      {
        int clusterJ = itMatMap->first;
        std::set<int> clusterJIPSet;
        getCluster(clusterJ,clusterJIPSet);
        // compute homogenized value for cluster J
        static STensor3 Fhomo;
        computeAverageStrainCluster(solver.getIPField(),clusterJIPSet,Fhomo);
        fullMatrix<double>& DIJval = DJI[std::pair<int,int>(clusterJ,clusterI)];
        if (DIJval.size1() != 6 and DIJval.size2() != 6)
        {
          //Msg::Warning("DIG is not yet allocated");
          DIJval.resize(6,6,true);
        }
        static STensor3 eps;
        static STensor3 I(1.);
        for (int ii =0; ii<3; ii++)
        {
          for (int jj=0; jj<3; jj++)
          {
            eps(ii,jj) = 0.5*(Fhomo(ii,jj)+Fhomo(jj,ii)) - I(ii,jj);
          }
        }
        if (i == 0 || i==1 || i==2)
        {
          DIJval(0,i)  = eps(0,0)/fact_eig; // A(00)(00 if i==0, 11 if i==1, 22 if i ==2)
          DIJval(1,i)  = eps(1,1)/fact_eig; // A(11)(00 if i==0, 11 if i==1, 22 if i ==2)
          DIJval(3,i)  = eps(0,1)/fact_eig; // A(01)(00 if i==0, 11 if i==1, 22 if i ==2)
          if (dim==3)
          {
          DIJval(2,i)  = eps(2,2)/fact_eig; // A(22)(00 if i==0, 11 if i==1, 22 if i ==2)
          DIJval(4,i)  = eps(0,2)/fact_eig; // A(02)(00 if i==0, 11 if i==1, 22 if i ==2)
          DIJval(5,i)  = eps(1,2)/fact_eig; // A(12)(00 if i==0, 11 if i==1, 22 if i ==2)    
          }         
        }
        else
        {
          // factor 2 for conversion in STensorOperation::fromSTensor43ToFullMatrix
          DIJval(0,i)  = 2.*eps(0,0)/fact_eig; // 2A(00) (01 if i==3, 02 if i==4, 12 if i ==5)
          DIJval(1,i)  = 2.*eps(1,1)/fact_eig; // 2A(11) (01 if i==3, 02 if i==4, 12 if i ==5)
          DIJval(3,i)  = 2.*eps(0,1)/fact_eig; // 2A(01) (01 if i==3, 02 if i==4, 12 if i ==5)
          if (dim==3)
          {
          DIJval(2,i)  = 2.*eps(2,2)/fact_eig; // 2A(22) (01 if i==3, 02 if i==4, 12 if i ==5)
          DIJval(4,i)  = 2.*eps(0,2)/fact_eig; // 2A(02) (01 if i==3, 02 if i==4, 12 if i ==5)
          DIJval(5,i)  = 2.*eps(1,2)/fact_eig; // 2A(12) (01 if i==3, 02 if i==4, 12 if i ==5)   
          }
        }
      }
      solver.oneStepPostSolve((double)num,num);
    }
  }
};


void Clustering::computeInteractionTensor(nonLinearMechSolver& solver,
                                          materialLaw* clusterLaw,
                                          int eigenField,
                                          bool saveToFile, const std::string fileName)
{
  nonLinearMechSolver* newSolver = solver.clone(solver.getMeshFileName(),solver.getTag());
  std::vector<partDomain*>& allDom  =  *(newSolver->getDomainVector());
  newSolver->addMaterialLaw(clusterLaw);  
  for (int i=0; i< allDom.size(); i++)
  {
    partDomain* dom  = allDom[i];
    dom->setMaterialLawNumber(clusterLaw->getNum());
  }

  _eigenField = eigenField;
  
  // init micro solver
  
  _allEigenStrains.clear();
  _allEigenStresses.clear();
  for (std::map<int, int>::const_iterator itMapMat = _clusterMaterialMap.begin(); itMapMat != _clusterMaterialMap.end(); itMapMat++)
  {
    if(_eigenField==1)
    {
    _allEigenStresses.insert(std::pair<int,STensor3>(itMapMat->first,STensor3(0.)));
    }
    else if(_eigenField==0)
    {
    _allEigenStrains.insert(std::pair<int,STensor3>(itMapMat->first,STensor3(1.)));
    }
    else
    {
    Msg::Error("this not implemented");
    }
  };
  newSolver->initMicroSolver();
  
  std::map<std::pair<int,int>, fullMatrix<double> > allDIJMapFullMatrix;

  for (std::map<int, int>::const_iterator itMatMap = _clusterMaterialMap.begin(); itMatMap != _clusterMaterialMap.end(); itMatMap++)
  {
    int clusterI = itMatMap->first;
    computeInteractionTensorForCluster(*newSolver,clusterI,allDIJMapFullMatrix);
  };
  
  _clusterInteractionTensorMap.clear();
  for (std::map<std::pair<int,int>, fullMatrix<double> >::const_iterator it = allDIJMapFullMatrix.begin(); it!= allDIJMapFullMatrix.end(); it++)
  {
    STensor43 tensor43;
    STensorOperation::fromFullMatrixToSTensor43(it->second,tensor43);
    _clusterInteractionTensorMap[it->first] = tensor43;
  }

  
  if (saveToFile)
  {
    printf("start writing interation tensor data to file\n");
    std::vector<std::string> splitFileName = SplitFileName(fileName);
    std::string saveFileName = splitFileName[1]+"_nbClusters_"+std::to_string(_totalNumberOfClusters)+splitFileName[2];
    
    FILE* f = fopen(saveFileName.c_str(),"w");
    std::string str ="CLUSTER_ID CLUSTER_ID STD";
    for (int i=0; i<6; i++)
    {
      for (int j=0; j<6; j++)
      {
        str += " D_"+std::to_string(i)+std::to_string(j);
      }
    }
    fprintf(f,"%s\n",str.c_str());
    // write data
    for (std::map<std::pair<int,int>, STensor43>::const_iterator it = _clusterInteractionTensorMap.begin();  it!= _clusterInteractionTensorMap.end(); it++)
    { 
      const std::pair<int,int>& pairij = it->first;
      const STensor43& Dij = it->second;
      static fullMatrix<double> mat(6,6);
      STensorOperation::fromSTensor43ToFullMatrix(Dij,mat);
      fprintf(f,"%d %d %.16g",pairij.first, pairij.second, 0.);
      for (int i=0; i<6; i++)
      {
        for (int j=0; j<6; j++)
        {
          fprintf(f, " %.16g",mat(i,j));
        }
      }
      fprintf(f,"\n");
    }
    fclose(f);
    printf("done writing interation tensor data to file\n");
  }
};

void Clustering::computeInteractionTensorsHierarchical(nonLinearMechSolver& solver,
                                                       materialLaw* clusterLaw,
                                                       int eigenField, int eigenFieldLevel1,
                                                       bool saveToFile, const std::string fileName)
{
  _level = 0;

  nonLinearMechSolver* newSolver = solver.clone(solver.getMeshFileName(),solver.getTag());
  std::vector<partDomain*>& allDom  =  *(newSolver->getDomainVector());
  newSolver->addMaterialLaw(clusterLaw);  
  for (int i=0; i< allDom.size(); i++)
  {
    partDomain* dom  = allDom[i];
    dom->setMaterialLawNumber(clusterLaw->getNum());
  }

  _eigenField = eigenField;
  
  // init micro solver
  
  _allEigenStrains.clear();
  _allEigenStresses.clear();
  for (std::map<int, int>::const_iterator itMapMat = _clusterMaterialMap.begin(); itMapMat != _clusterMaterialMap.end(); itMapMat++)
  {
    if(_eigenField==1)
    {
    _allEigenStresses.insert(std::pair<int,STensor3>(itMapMat->first,STensor3(0.)));
    }
    else if(_eigenField==0)
    {
    _allEigenStrains.insert(std::pair<int,STensor3>(itMapMat->first,STensor3(1.)));
    }
    else
    {
    Msg::Error("this not implemented");
    }
  };
  newSolver->initMicroSolver();
  
  std::map<std::pair<int,int>, fullMatrix<double> > allDIJMapFullMatrix;
  for (std::map<int, int>::const_iterator itMatMap = _clusterMaterialMap.begin(); itMatMap != _clusterMaterialMap.end(); itMatMap++)
  {
    int clusterI = itMatMap->first;
    computeInteractionTensorForCluster(*newSolver,clusterI,allDIJMapFullMatrix);
  };
  
  _clusterInteractionTensorMap.clear();
  for (std::map<std::pair<int,int>, fullMatrix<double> >::const_iterator it = allDIJMapFullMatrix.begin(); it!= allDIJMapFullMatrix.end(); it++)
  {
    STensor43 tensor43;
    STensorOperation::fromFullMatrixToSTensor43(it->second,tensor43);
    _clusterInteractionTensorMap[it->first] = tensor43;
  }
  
  if(_numberOfLevels>1)
  { 
    _level += 1;
    
    _eigenField = eigenFieldLevel1;
  
    _allEigenStrains.clear();
    _allEigenStresses.clear();
    _allEigenStrainsLevel1.clear();
    _allEigenStressesLevel1.clear();
        
    for (std::map<int, std::map<int, int>>::const_iterator itMapMat = _clusterMaterialMapLevel1.begin(); itMapMat != _clusterMaterialMapLevel1.end(); itMapMat++)
    { 
      std::map<int, STensor3>& allEigenStrainsOnLevel1 = _allEigenStrainsLevel1[itMapMat->first];
      std::map<int, STensor3>& allEigenStressesOnLevel1 = _allEigenStressesLevel1[itMapMat->first];
      const std::map<int, int>& clusterMaterialMapOnLevel1 = itMapMat->second;
      for (std::map<int, int>::const_iterator itMapMatLevel1 = clusterMaterialMapOnLevel1.begin(); itMapMatLevel1 != clusterMaterialMapOnLevel1.end(); itMapMatLevel1++)
      {
        if(_eigenField==0)
        {          
        allEigenStrainsOnLevel1.insert(std::pair<int,STensor3> (itMapMatLevel1->first,STensor3(1.)));
        }
        else if(_eigenField==1)
        {
        allEigenStressesOnLevel1.insert(std::pair<int,STensor3> (itMapMatLevel1->first,STensor3(0.)));
        }
        else
        {
        Msg::Error("this not implemented");
        }
      }
    }
    
    //newSolver->initMicroSolver();
  
    std::map<int, std::map<std::pair<int,int>, fullMatrix<double> > > allDIJMapFullMatrixLevel1;
    for (std::map<int, std::map<int, int>>::const_iterator itMapMat = _clusterMaterialMapLevel1.begin(); itMapMat != _clusterMaterialMapLevel1.end(); itMapMat++)
    {
      const int& clusterLv0 =   itMapMat->first;
    
      const std::map<int, int>& clusterMaterialMapOnLevel1 = itMapMat->second;
      for (std::map<int, int>::const_iterator itMapMatLevel1 = clusterMaterialMapOnLevel1.begin(); itMapMatLevel1 != clusterMaterialMapOnLevel1.end(); itMapMatLevel1++)
      {
        int clusterS = itMapMatLevel1->first;
        computeInteractionTensorForClusterLevel1(*newSolver,clusterLv0,clusterS,allDIJMapFullMatrixLevel1);   
      }
    }
    _clusterInteractionTensorMapLevel1.clear();
    for (std::map<int, std::map<std::pair<int,int>, fullMatrix<double> > >::const_iterator itLv0 = allDIJMapFullMatrixLevel1.begin(); itLv0!= allDIJMapFullMatrixLevel1.end(); itLv0++)
    {
      const std::map<std::pair<int,int>, fullMatrix<double> >& allDIJMapFullMatrixOnLevel1 = itLv0->second;
      std::map<std::pair<int,int>, STensor43 >& clusterInteractionTensorMapLv1 = _clusterInteractionTensorMapLevel1[itLv0->first];
      for (std::map<std::pair<int,int>, fullMatrix<double> >::const_iterator itLv1 = allDIJMapFullMatrixOnLevel1.begin(); itLv1!= allDIJMapFullMatrixOnLevel1.end(); itLv1++)
      {
        STensor43 tensor43;
        STensorOperation::fromFullMatrixToSTensor43(itLv1->second,tensor43);
        clusterInteractionTensorMapLv1[itLv1->first] = tensor43;
      }
    }
  }

  
  if (saveToFile)
  {
    printf("start writing interation tensor data level 0 to file\n");
    std::vector<std::string> splitFileName = SplitFileName(fileName);
    std::string saveFileName = splitFileName[1]+"_nbClusters_"+std::to_string(_totalNumberOfClusters)+splitFileName[2];
    
    FILE* f0 = fopen(saveFileName.c_str(),"w");
    std::string str0 ="CLUSTER_ID CLUSTER_ID STD";
    for (int i=0; i<6; i++)
    {
      for (int j=0; j<6; j++)
      {
        str0 += " D_"+std::to_string(i)+std::to_string(j);
      }
    }
    fprintf(f0,"%s\n",str0.c_str());
    // write data
    for (std::map<std::pair<int,int>, STensor43>::const_iterator it = _clusterInteractionTensorMap.begin();  it!= _clusterInteractionTensorMap.end(); it++)
    { 
      const std::pair<int,int>& pairij = it->first;
      const STensor43& Dij = it->second;
      static fullMatrix<double> mat(6,6);
      STensorOperation::fromSTensor43ToFullMatrix(Dij,mat);
      fprintf(f0,"%d %d %.16g",pairij.first, pairij.second, 0.);
      for (int i=0; i<6; i++)
      {
        for (int j=0; j<6; j++)
        {
          fprintf(f0, " %.16g",mat(i,j));
        }
      }
      fprintf(f0,"\n");
    }
    fclose(f0);
    printf("done writing interation tensor data level 0 to file\n");
    
    
    if(_numberOfLevels>1)
    {
      printf("start writing interation tensor data level 1 to file\n");
      std::vector<std::string> splitFileName = SplitFileName(fileName);
      std::string saveFileName = splitFileName[1]+"_level1_nbClusters_"+std::to_string(_totalNumberOfClusters)+splitFileName[2];
    
      FILE* f1 = fopen(saveFileName.c_str(),"w");
      std::string str1 ="CLUSTER_LV0 CLUSTER_ID CLUSTER_ID";
      for (int i=0; i<6; i++)
      {
        for (int j=0; j<6; j++)
        {
          str1 += " D_"+std::to_string(i)+std::to_string(j);
        }
      }
      fprintf(f1,"%s\n",str1.c_str());
      // write data
      for (std::map<int, std::map<std::pair<int,int>, STensor43> >::const_iterator itLv0 = _clusterInteractionTensorMapLevel1.begin();  itLv0!= _clusterInteractionTensorMapLevel1.end(); itLv0++)
      { 
        const std::map<std::pair<int,int>, STensor43 >& clusterInteractionTensorMapLv1 = itLv0->second;
        const int& clusterLv0 = itLv0->first;        
        for (std::map<std::pair<int,int>, STensor43>::const_iterator itLv1 = clusterInteractionTensorMapLv1.begin();  itLv1!= clusterInteractionTensorMapLv1.end(); itLv1++)
        {       
          const std::pair<int,int>& pairij = itLv1->first;
          const STensor43& Dij = itLv1->second;
          static fullMatrix<double> mat(6,6);
          STensorOperation::fromSTensor43ToFullMatrix(Dij,mat);
          fprintf(f1,"%d %d %d", clusterLv0, pairij.first, pairij.second);
          for (int i=0; i<6; i++)
          {
            for (int j=0; j<6; j++)
            {
              fprintf(f1, " %.16g",mat(i,j));
            }
          }
          fprintf(f1,"\n");
        }
      }
      fclose(f1);
      printf("done writing interation tensor data level 1 to file\n");   
    }
  }
};

void Clustering::computeEshelbyInteractionTensor(nonLinearMechSolver& solver,
                                          materialLaw* clusterLaw,
                                          int eigenField,
                                          bool saveToFile, const std::string fileName)
{
  nonLinearMechSolver* newSolver = solver.clone(solver.getMeshFileName(),solver.getTag());
  std::vector<partDomain*>& allDom  =  *(newSolver->getDomainVector());
  newSolver->addMaterialLaw(clusterLaw);
  for (int i=0; i< allDom.size(); i++)
  {
    partDomain* dom  = allDom[i];
    dom->setMaterialLawNumber(clusterLaw->getNum());
  }
  
  _eigenField = eigenField;
  
  _allEigenStrains.clear();
  _allEigenStresses.clear();
  for (std::map<int, int>::const_iterator itMapMat = _clusterMaterialMap.begin(); itMapMat != _clusterMaterialMap.end(); itMapMat++)
  {
    if(_eigenField==1)
    {
    _allEigenStresses.insert(std::pair<int,STensor3>(itMapMat->first,STensor3(0.)));
    }
    else if(_eigenField==0)
    {
    _allEigenStrains.insert(std::pair<int,STensor3>(itMapMat->first,STensor3(1.)));
    }
    else
    {
    Msg::Error("this not implemented");
    }
  };
  
  // init solver
  newSolver->initializeStaticScheme();
  
  std::map<std::pair<int,int>, fullMatrix<double> > allDIJEshelbyMapFullMatrix;

  for (std::map<int, int>::const_iterator itMatMap = _clusterMaterialMap.begin(); itMatMap != _clusterMaterialMap.end(); itMatMap++)
  {
    int clusterI = itMatMap->first;
    computeEshelbyInteractionTensorForCluster(*newSolver,clusterI,allDIJEshelbyMapFullMatrix);
  };

  _clusterEshelbyInteractionTensorMap.clear();
  for (std::map<std::pair<int,int>, fullMatrix<double> >::const_iterator it = allDIJEshelbyMapFullMatrix.begin(); it!= allDIJEshelbyMapFullMatrix.end(); it++)
  {
    STensor43 tensor43;
    STensorOperation::fromFullMatrixToSTensor43(it->second,tensor43);
    _clusterEshelbyInteractionTensorMap[it->first] = tensor43;
  }

  if (saveToFile)
  {
    printf("start writing eshelby interation tensor data to file\n");
    std::vector<std::string> splitFileNameEshelby = SplitFileName(fileName);
    std::string saveFileName = splitFileNameEshelby[1]+"_nbClusters_"+std::to_string(_totalNumberOfClusters)+splitFileNameEshelby[2];
    
    FILE* fEshelby = fopen(saveFileName.c_str(),"w");
    std::string str ="CLUSTER_ID CLUSTER_ID";
    for (int i=0; i<6; i++)
    {
      for (int j=0; j<6; j++)
      {
        str += " D_"+std::to_string(i)+std::to_string(j);
      }
    }
    fprintf(fEshelby,"%s\n",str.c_str());
    // write data
    for (std::map<std::pair<int,int>, STensor43>::const_iterator it = _clusterEshelbyInteractionTensorMap.begin();  it!= _clusterEshelbyInteractionTensorMap.end(); it++)
    { 
      const std::pair<int,int>& pairij = it->first;
      const STensor43& DijEshelby = it->second;
      static fullMatrix<double> mat(6,6);
      STensorOperation::fromSTensor43ToFullMatrix(DijEshelby,mat);
      fprintf(fEshelby,"%d %d",pairij.first, pairij.second);
      for (int i=0; i<6; i++)
      {
        for (int j=0; j<6; j++)
        {
          fprintf(fEshelby, " %.16g",mat(i,j));
        }
      }
      fprintf(fEshelby,"\n");
    }
    fclose(fEshelby);
    printf("done writing eshelby interation tensor data to file\n");
  }
};


void Clustering::loadInteractionTensorsFromFile(const std::string fileName)
{
  FILE* fp = fopen(fileName.c_str(),"r");
  if (fp !=NULL) {
    printf("start reading file: %s\n",fileName.c_str());
    if(!feof(fp))
    {
      char what[256];
      fscanf(fp, "%s", what);
      printf("what = %s\n",what);
      fscanf(fp, "%s", what);
      printf("what = %s\n",what);
      fscanf(fp, "%s", what);
      printf("what = %s\n",what);

      for (int i=0; i<6; i++)
      {
        for (int j=0; j<6; j++)
        {
          fscanf(fp, "%s", what);
          printf("what = %s\n",what);
        }
      }
      if(strcmp(what, "D_55"))
      {
        Msg::Error("file format in %s is not correct\n",fileName.c_str());
        Msg::Exit(0);
      }
    }
    while (1){
      if(feof(fp))
        break;
      int clusterI(0), clusterJ(0);
      double a;
      if (fscanf(fp, "%d %d %lf", &clusterI,&clusterJ,&a)==3)
      {
        static fullMatrix<double> matTensor(6,6);
        for (int i=0; i<6; i++)
        {
          for (int j=0; j<6; j++)
          {
            double val=0;
            if (fscanf(fp,"%lf",&val)==1)
              matTensor(i,j) = val;
          }
        }
        if (clusterI >=0 && clusterJ>=0)
        {
          //printf("load data interaction %d %d \n",clusterI,clusterJ);
          //matTensor.print("matTensor");
          STensor43& T = _clusterInteractionTensorMap[std::pair<int,int>(clusterI,clusterJ)];
          STensorOperation::fromFullMatrixToSTensor43(matTensor,T);
        }
      }
    }
    printf("done reading file: %s\n",fileName.c_str());
    fclose(fp);
    
    _totalNumberOfClusters = (int)(sqrt(_clusterInteractionTensorMap.size()));
    printf("total number of cluster from file %s = %d\n",fileName.c_str(),_totalNumberOfClusters);
  }
  else
  {
    Msg::Error("File %s does not exist !!!",fileName.c_str());
    Msg::Exit(0);
  }
}

void Clustering::loadInteractionTensorsLevel1FromFile(const std::string fileName)
{
  FILE* fp = fopen(fileName.c_str(),"r");
  if (fp !=NULL) {
    printf("start reading file: %s\n",fileName.c_str());
    if(!feof(fp))
    {
      char what[256];
      fscanf(fp, "%s", what);
      printf("what = %s\n",what);
      fscanf(fp, "%s", what);
      printf("what = %s\n",what);
      fscanf(fp, "%s", what);
      printf("what = %s\n",what);

      for (int i=0; i<6; i++)
      {
        for (int j=0; j<6; j++)
        {
          fscanf(fp, "%s", what);
          printf("what = %s\n",what);
        }
      }
      if(strcmp(what, "D_55"))
      {
        Msg::Error("file format in %s is not correct\n",fileName.c_str());
        Msg::Exit(0);
      }
    }
    while (1){
      if(feof(fp))
        break;
      int clusterHost(0), clusterI(0), clusterJ(0);
      if (fscanf(fp, "%d %d %d",&clusterHost,&clusterI,&clusterJ)==3)
      {
        static fullMatrix<double> matTensor(6,6);
        for (int i=0; i<6; i++)
        {
          for (int j=0; j<6; j++)
          {
            double val=0;
            if (fscanf(fp,"%lf",&val)==1)
              matTensor(i,j) = val;
          }
        }
        if (clusterI >=0 && clusterJ>=0)
        {
          //printf("load data interaction %d %d \n",clusterI,clusterJ);
          //matTensor.print("matTensor");
          std::map<std::pair<int,int>, STensor43>& clusterInteractionTensorMapOnLevel1 = _clusterInteractionTensorMapLevel1[clusterHost];
          STensor43& T = clusterInteractionTensorMapOnLevel1[std::pair<int,int>(clusterI,clusterJ)];
          STensorOperation::fromFullMatrixToSTensor43(matTensor,T);
        }
      }
    }
    printf("done reading file: %s\n",fileName.c_str());
    fclose(fp);
    
    _totalNumberOfClusters = (int)(sqrt(_clusterInteractionTensorMap.size()));
    printf("total number of cluster from file %s = %d\n",fileName.c_str(),_totalNumberOfClusters);
  }
  else
  {
    Msg::Error("File %s does not exist !!!",fileName.c_str());
    Msg::Exit(0);
  }
}

void Clustering::loadEshelbyInteractionTensorsFromFile(const std::string fileName)
{
  FILE* fp = fopen(fileName.c_str(),"r");
  if (fp !=NULL) {
    printf("start reading file: %s\n",fileName.c_str());
    if(!feof(fp))
    {
      char what[256];
      fscanf(fp, "%s", what);
      printf("what = %s\n",what);
      fscanf(fp, "%s", what);
      printf("what = %s\n",what);

      for (int i=0; i<6; i++)
      {
        for (int j=0; j<6; j++)
        {
          fscanf(fp, "%s", what);
          printf("what = %s\n",what);
        }
      }
      if(strcmp(what, "D_55"))
      {
        Msg::Error("file format in %s is not correct\n",fileName.c_str());
        Msg::Exit(0);
      }
    }
    while (1){
      if(feof(fp))
        break;
      int clusterI(0), clusterJ(0);
      if (fscanf(fp, "%d %d", &clusterI,&clusterJ)==2)
      {
        static fullMatrix<double> matTensor(6,6);
        for (int i=0; i<6; i++)
        {
          for (int j=0; j<6; j++)
          {
            double val=0;
            if (fscanf(fp,"%lf",&val)==1)
              matTensor(i,j) = val;
          }
        }
        if (clusterI >=0 && clusterJ>=0)
        {
          printf("load data interaction %d %d \n",clusterI,clusterJ);
          matTensor.print("matTensor");
          STensor43& T = _clusterEshelbyInteractionTensorMap[std::pair<int,int>(clusterI,clusterJ)];
          STensorOperation::fromFullMatrixToSTensor43(matTensor,T);
        }
      }
    }
    printf("done reading file: %s\n",fileName.c_str());
    fclose(fp);
    
    _totalNumberOfClusters = (int)(sqrt(_clusterEshelbyInteractionTensorMap.size()));
    printf("total number of cluster from file %s = %d\n",fileName.c_str(),_totalNumberOfClusters);
  }
  else
  {
    Msg::Error("File %s does not exist !!!",fileName.c_str());
    Msg::Exit(0);
  }
}


void Clustering::loadInteractionTensorsMatrixFrameELFromFile(const std::string fileName)
{
  FILE* fp = fopen(fileName.c_str(),"r");
  if (fp !=NULL) {
    printf("start reading file: %s\n",fileName.c_str());
    if(!feof(fp))
    {
      char what[256];
      fscanf(fp, "%s", what);
      printf("what = %s\n",what);
      fscanf(fp, "%s", what);
      printf("what = %s\n",what);
      fscanf(fp, "%s", what);
      printf("what = %s\n",what);

      for (int i=0; i<6; i++)
      {
        for (int j=0; j<6; j++)
        {
          fscanf(fp, "%s", what);
          printf("what = %s\n",what);
        }
      }
      if(strcmp(what, "D_55"))
      {
        Msg::Error("file format in %s is not correct\n",fileName.c_str());
        Msg::Exit(0);
      }
    }
    while (1){
      if(feof(fp))
        break;
      int clusterI(0), clusterJ(0);
      double DIJstdev(0.);
      if (fscanf(fp, "%d %d %lf", &clusterI,&clusterJ,&DIJstdev)==3)
      {
        static fullMatrix<double> matTensor(6,6);
        for (int i=0; i<6; i++)
        {
          for (int j=0; j<6; j++)
          {
            double val=0;
            if (fscanf(fp,"%lf",&val)==1)
              matTensor(i,j) = val;
          }
        }
        if (clusterI >=0 && clusterJ>=0)
        {
          printf("load data interaction %d %d \n",clusterI,clusterJ);
          matTensor.print("matTensor");
          STensor43& T = _clusterInteractionTensorMatrixFrameELMap[std::pair<int,int>(clusterI,clusterJ)];
          STensorOperation::fromFullMatrixToSTensor43(matTensor,T);
        }
      }
    }
    printf("done reading file: %s\n",fileName.c_str());
    fclose(fp);
    
    _totalNumberOfClusters = (int)(sqrt(_clusterInteractionTensorMatrixFrameELMap.size())-1);
    printf("total number of cluster from file %s = %d\n",fileName.c_str(),_totalNumberOfClusters);
  }
  else
  {
    Msg::Error("File %s does not exist !!!",fileName.c_str());
    Msg::Exit(0);
  }
}


void Clustering::loadInteractionTensorsHomogenizedFrameELFromFile(const std::string fileName)
{
  FILE* fp = fopen(fileName.c_str(),"r");
  if (fp !=NULL) {
    printf("start reading file: %s\n",fileName.c_str());
    if(!feof(fp))
    {
      char what[256];
      fscanf(fp, "%s", what);
      printf("what = %s\n",what);
      fscanf(fp, "%s", what);
      printf("what = %s\n",what);
      fscanf(fp, "%s", what);
      printf("what = %s\n",what);

      for (int i=0; i<6; i++)
      {
        for (int j=0; j<6; j++)
        {
          fscanf(fp, "%s", what);
          printf("what = %s\n",what);
        }
      }
      if(strcmp(what, "D_55"))
      {
        Msg::Error("file format in %s is not correct\n",fileName.c_str());
        Msg::Exit(0);
      }
    }
    while (1){
      if(feof(fp))
        break;
      int clusterI(0), clusterJ(0);
      double DIJstdev(0.);
      if (fscanf(fp, "%d %d %lf", &clusterI,&clusterJ,&DIJstdev)==3)
      {
        static fullMatrix<double> matTensor(6,6);
        for (int i=0; i<6; i++)
        {
          for (int j=0; j<6; j++)
          {
            double val=0;
            if (fscanf(fp,"%lf",&val)==1)
              matTensor(i,j) = val;
          }
        }
        if (clusterI >=0 && clusterJ>=0)
        {
          printf("load data interaction %d %d \n",clusterI,clusterJ);
          //matTensor.print("matTensor");
          STensor43& T = _clusterInteractionTensorHomogenizedFrameELMap[std::pair<int,int>(clusterI,clusterJ)];
          STensorOperation::fromFullMatrixToSTensor43(matTensor,T);
        }
      }
    }
    printf("done reading file: %s\n",fileName.c_str());
    fclose(fp);
    
    //std::map<std::pair<int,int>,STensor43>::const_iterator Dfind = getClusterInteractionTensorMap().find(std::pair<int,int>(0,0));
    
    _totalNumberOfClusters = (int)(sqrt(_clusterInteractionTensorHomogenizedFrameELMap.size())-1);
    printf("total number of cluster from file %s = %d\n",fileName.c_str(),_totalNumberOfClusters);
  }
  else
  {
    Msg::Error("File %s does not exist !!!",fileName.c_str());
    Msg::Exit(0);
  }
}


void Clustering::loadInteractionTensorIsoFromFile(const std::string fileName)
{
  FILE* fp = fopen(fileName.c_str(),"r");
  if (fp !=NULL) {
    printf("start reading file: %s\n",fileName.c_str());
    if(!feof(fp))
    {
      char what[256];
      fscanf(fp, "%s", what);
      printf("what = %s\n",what);
      fscanf(fp, "%s", what);
      printf("what = %s\n",what);
      fscanf(fp, "%s", what);
      printf("what = %s\n",what);

      for (int i=0; i<6; i++)
      {
        for (int j=0; j<6; j++)
        {
          fscanf(fp, "%s", what);
          printf("what = %s\n",what);
        }
      }
      if(strcmp(what, "D_55"))
      {
        Msg::Error("file format in %s is not correct\n",fileName.c_str());
        Msg::Exit(0);
      }
    }
    while (1){
      if(feof(fp))
        break;
      int clusterI(0), clusterJ(0);
      double a;
      if (fscanf(fp, "%d %d %lf", &clusterI,&clusterJ,&a)==3)
      {
        static fullMatrix<double> matTensor(6,6);
        for (int i=0; i<6; i++)
        {
          for (int j=0; j<6; j++)
          {
            double val=0;
            if (fscanf(fp,"%lf",&val)==1)
              matTensor(i,j) = val;
          }
        }
        if (clusterI >=0 && clusterJ>=0)
        {
          STensor43& T = _clusterInteractionTensorIsoMap[std::pair<int,int>(clusterI,clusterJ)];
          STensorOperation::fromFullMatrixToSTensor43(matTensor,T);
        }
      }
    }
    printf("done reading file: %s\n",fileName.c_str());
    fclose(fp);
    
    _totalNumberOfClusters = (int)(sqrt(_clusterInteractionTensorIsoMap.size()));
    printf("total number of cluster from file %s = %d\n",fileName.c_str(),_totalNumberOfClusters);
  }
  else
  {
    Msg::Error("File %s does not exist !!!",fileName.c_str());
    Msg::Exit(0);
  }
}


void Clustering::loadElasticStrainConcentrationDerivativeFromFile(const std::string fileName)
{
  FILE* fp = fopen(fileName.c_str(),"r");
  if (fp !=NULL) {
    printf("start reading file: %s\n",fileName.c_str());
    if(!feof(fp))
    {
      char what[256];
      fscanf(fp, "%s", what);
      printf("what = %s\n",what);
      fscanf(fp, "%s", what);
      printf("what = %s\n",what);

      for (int i=0; i<6; i++)
      {
        for (int j=0; j<6; j++)
        {
          fscanf(fp, "%s", what);
          printf("what = %s\n",what);
        }
      }
      if(strcmp(what, "A_55"))
      {
        Msg::Error("file format in %s is not correct\n",fileName.c_str());
        Msg::Exit(0);
      }
    }
    while (1){
      if(feof(fp))
        break;
      int clusterI(0), clusterJ(0);
      if (fscanf(fp, "%d %d", &clusterI,&clusterJ)==2)
      {
        static fullMatrix<double> matTensor(6,6);
        for (int i=0; i<6; i++)
        {
          for (int j=0; j<6; j++)
          {
            double val=0;
            if (fscanf(fp,"%lf",&val)==1)
              matTensor(i,j) = val;
          }
        }
        if (clusterI >=0 && clusterJ>=0)
        {
          printf("load data elastic strain concentration derivative %d %d \n",clusterI,clusterJ);
          matTensor.print("matTensor");
          STensor43& T = _DclusterAverageStrainConcentrationTensorMapDLaw[std::pair<int,int>(clusterI,clusterJ)];
          STensorOperation::fromFullMatrixToSTensor43(matTensor,T);
        }
      }
    }
    printf("done reading file: %s\n",fileName.c_str());
    fclose(fp);
    
    _totalNumberOfClusters = (int)(sqrt(_DclusterAverageStrainConcentrationTensorMapDLaw.size()));
    printf("total number of cluster from file %s = %d\n",fileName.c_str(),_totalNumberOfClusters);
  }
  else
  {
    Msg::Error("File %s does not exist !!!",fileName.c_str());
    Msg::Exit(0);
  }
}


void Clustering::unity_Voigt(fullMatrix<double>& mat) const
{
  mat.set(0,0,1.);
  mat.set(1,1,1.);
  mat.set(2,2,1.);
  mat.set(3,3,1.);
  mat.set(4,4,1.);
  mat.set(5,5,1.);
};


void viewClustering::archive(const std::string filename, const double time, const int numstep, const std::vector<int>& view, const int gp)
{
  std::string dataname = "ElementData";
  int totelem = _clustering.getMapElementNumberOfGPs().size();
  
  FILE* file = fopen(filename.c_str(), "w");
  // start file by format version
  // problem: find how to retrieve the last mesh format version
  fprintf(file, "$MeshFormat\n2.2 0 8\n$EndMeshFormat\n"); 
  for(std::vector<int>::const_iterator it=view.begin(); it!=view.end(); ++it)
  {
    int cmp = *it;
    std::string viewname = Clustering::ToString(cmp);
    if (cmp == Clustering::ClusterIndex)
    {
      viewname += "_nbClusters_"+std::to_string(_clustering.getTotalNumberOfClusters());
    }
    fprintf(file, "$%s\n1\n\"%s\"\n1\n%.16g\n4\n%d\n%d\n%d\n%d\n",dataname.c_str(),viewname.c_str(),time,numstep,1,totelem,Msg::GetCommRank());
    buildData(file,cmp,gp);
    fprintf(file, "$End%s\n",dataname.c_str());
  }
  fclose(file);
}


void viewClustering::buildData(FILE* myview, const int cmp, const int gp) const
{
  
  const std::map<int, int>& allElNum = _clustering.getMapElementNumberOfGPs();
  for (std::map<int,int>::const_iterator it = allElNum.begin(); it!= allElNum.end(); it++)
  {
    int ele = it->first;
    double val = get(ele,cmp,gp);
    fprintf(myview, "%d",ele);
    fprintf(myview, " %.16g\n",val);
  }
}


double viewClustering::get(int eleNum, const int cmp, const int gp)const
{
  // if gp = -1, return average value on element
  // else return value at gp
  static fullMatrix<double> A;
  A.resize(6,6,true);
  double clusterIndex = 0;
  double clusterIndexGlobal = 0;
  
  //static fullVector<double> pfc;
  //pfc.resize(6,true);
  
  int npts = _clustering.getNbOfGPsOnElement(eleNum);
  if (gp == -1)
  {
    // find number of GPs in ele
    for(int i=0;i<npts;i++)
    {
      const fullMatrix<double>& A_i =  _clustering.getStrainConcentrationTensor(eleNum,i);
      int clusterIndex_i = _clustering.getClusterIndex(eleNum,i);
      int clusterIndexGlobal_i = _clustering.getClusterIndexHierarchicalGlobal(eleNum,i);
      A += A_i;
      clusterIndex += clusterIndex_i;    
      clusterIndexGlobal += clusterIndexGlobal_i;  
      //const fullVector<double>& pfc_i = _clustering.getPFC(eleNum,i);
      //for(int h=0;h<6;h++)
      //{
        //pfc(h) += pfc_i(h);
      //}
    }
    double fact = 1./double(npts);
    A.scale(fact);
    clusterIndex *= (fact);
    clusterIndexGlobal *= (fact);
    //pfc.scale(fact);
  }
  else if (gp >= npts)
  {
    return -1e10;
  }
  else
  {
    // get at Gausspoint
    A =  _clustering.getStrainConcentrationTensor(eleNum,gp);
    clusterIndex = _clustering.getClusterIndex(eleNum,gp);
    clusterIndexGlobal = _clustering.getClusterIndexHierarchicalGlobal(eleNum,gp);
  }
  
  
  if (cmp == Clustering::A_00) return A(0,0);
  else if (cmp == Clustering::A_01) return A(0,1);
  else if (cmp == Clustering::A_02) return A(0,2);
  else if (cmp == Clustering::A_03) return A(0,3);
  else if (cmp == Clustering::A_04) return A(0,4);
  else if (cmp == Clustering::A_05) return A(0,5);
  else if (cmp == Clustering::A_10) return A(1,0);
  else if (cmp == Clustering::A_11) return A(1,1);
  else if (cmp == Clustering::A_12) return A(1,2);
  else if (cmp == Clustering::A_13) return A(1,3);
  else if (cmp == Clustering::A_14) return A(1,4);
  else if (cmp == Clustering::A_15) return A(1,5);
  else if (cmp == Clustering::A_20) return A(2,0);
  else if (cmp == Clustering::A_21) return A(2,1);
  else if (cmp == Clustering::A_22) return A(2,2);
  else if (cmp == Clustering::A_23) return A(2,3);
  else if (cmp == Clustering::A_24) return A(2,4);
  else if (cmp == Clustering::A_25) return A(2,5);
  else if (cmp == Clustering::A_30) return A(3,0);
  else if (cmp == Clustering::A_31) return A(3,1);
  else if (cmp == Clustering::A_32) return A(3,2);
  else if (cmp == Clustering::A_33) return A(3,3);
  else if (cmp == Clustering::A_34) return A(3,4);
  else if (cmp == Clustering::A_35) return A(3,5);
  else if (cmp == Clustering::A_40) return A(4,0);
  else if (cmp == Clustering::A_41) return A(4,1);
  else if (cmp == Clustering::A_42) return A(4,2);
  else if (cmp == Clustering::A_43) return A(4,3);
  else if (cmp == Clustering::A_44) return A(4,4);
  else if (cmp == Clustering::A_45) return A(4,5);
  else if (cmp == Clustering::A_50) return A(5,0);
  else if (cmp == Clustering::A_51) return A(5,1);
  else if (cmp == Clustering::A_52) return A(5,2);
  else if (cmp == Clustering::A_53) return A(5,3);
  else if (cmp == Clustering::A_54) return A(5,4);
  else if (cmp == Clustering::A_55) return A(5,5);
  else if (cmp == Clustering::A_norm)
  {
    double val = 0;
    for (int i=0; i<6; i++)
    {
      for (int j=0; j<6; j++)
      {
        val += A(i,j)*A(i,j);
      }
    }
    return sqrt(val);
  }
  else if (cmp ==Clustering::ClusterIndex)
  {
    return clusterIndex;
  }
  else if (cmp ==Clustering::ClusterIndexHierarchicalGlobal)
  {
    return clusterIndexGlobal;
  }
  /*else if (cmp == Clustering::PFC_0) return pfc(0);
  else if (cmp == Clustering::PFC_1) return pfc(1);
  else if (cmp == Clustering::PFC_2) return pfc(2);
  else if (cmp == Clustering::PFC_3) return pfc(3);
  else if (cmp == Clustering::PFC_4) return pfc(4);
  else if (cmp == Clustering::PFC_5) return pfc(5);*/
  else
  {
    Msg::Error("field %d does not exist !!!",cmp);
    return 0;
  }
  
};


double clutering_impls::getDiffNorm(const fullMatrix<double>&A, const fullMatrix<double>&B)
{
  double val = 0;
  for (int i=0; i< 6; i++)
  {
    for (int j=0; j<6; j++)
    {
      val += (A(i,j)-B(i,j))*(A(i,j)-B(i,j));
    }
  }
  return sqrt(val);
};

double clutering_impls::getDiffNormVec(const fullVector<double>&a, const fullVector<double>&b)
{
  double val = 0;
  for (int i=0; i< 6; i++)
  {
    val += 1.*(a(i)-b(i))*(a(i)-b(i));
  }
  return sqrt(val);
};

double clutering_impls::getDiffNormOrientationBased(const fullVector<double>&locOri, const fullVector<double>&meanOri)
{
  double PI = 3.14159265;
  
  double e1,e2,e3;
  e1 = locOri(0);
  if(abs(e1)<1.-10 and e1<0.)
  {
  e2 = -locOri(1);
  }
  else
  {
  e2 = locOri(1);
  }
  e3 = locOri(2);
  
  double e1_mean,e2_mean,e3_mean;
  e1_mean = meanOri(0);
  if(abs(e1_mean)<1.-10 and e1_mean<0.)
  {
  e2_mean = -meanOri(1);
  }
  else
  {
  e2_mean = meanOri(1);
  }
  e3_mean = meanOri(2);        
  
  double e1_diff, e2_diff, sin_e1_diff, sin_e2_diff;  
  e1_diff = e1 - e1_mean;
  e2_diff = e2 - e2_mean;
  sin_e1_diff = sin(e1_diff*PI/180.);
  sin_e2_diff = sin(e2_diff*PI/180.);
      
  double val = 0;
  val += 1.*(sin_e2_diff)*(sin_e2_diff);
  return sqrt(val);
};

double clutering_impls::getDiffNormVecLong(const fullVector<double>&a, const fullVector<double>&b)
{
  double val = 0;
  for (int i=0; i< 6; i++)
  {
    for (int j=0; j< 6; j++)
    {
      val += 1.*(a(i*6+j)-b(i*6+j))*(a(i*6+j)-b(i*6+j));
    }
  }
  return sqrt(val);
};

double clutering_impls::getDiffNormVecPowerLong(const fullVector<double>&a, const fullVector<double>&b)
{
  double val = 0;
  double exp = 1.2;
  
  double aVal, bVal;
  
  for (int i=0; i< 6; i++)
  {
    for (int j=0; j< 6; j++)
    {
      if(a(i*6+j) < 0.)
      {
        aVal = pow(-1.*a(i*6+j),exp);
        aVal *= -1.;
      }
      else
      {
        aVal = pow(a(i*6+j),exp);
      }
      if(b(i*6+j) < 0.)
      {
        bVal = pow(-1.*b(i*6+j),exp);
        bVal *= -1.;
      }
      else
      {
        bVal = pow(b(i*6+j),exp);
      }      
      val += 1.*(aVal-bVal)*(aVal-bVal);
    }
  }
  return sqrt(val);
};

double clutering_impls::getDiffShearNormVecLong(const fullVector<double>&a, const fullVector<double>&b)
{
  double val = 0;
  for (int i=0; i< 6; i++)
  {
    for (int j=0; j< 6; j++)
    {
      if(j==2 or j==4 or j==5){val += 1.*(a(i*6+j)-b(i*6+j))*(a(i*6+j)-b(i*6+j));}
    }
  }
  return sqrt(val);
};

double clutering_impls::meanCluster(const std::map<int, const fullMatrix<double>* >& cluster,
                            const std::map<int, fullVector<double> >& mapPositionsWeights,
                            fullMatrix<double>& mean)
{
  mean.resize(6,6,true);
  double totalWeight = 0;
  int n = cluster.size();
  if (n > 0)
  {
    for (std::map<int, const fullMatrix<double>* >::const_iterator its = cluster.begin(); its != cluster.end(); its ++)
    {
      std::map<int, fullVector<double> >::const_iterator itFind = mapPositionsWeights.find(its->first);
      if (itFind==mapPositionsWeights.end())
      {
        printf("wrong map of Gauss poing as GP %d is not found in this map",its->first);
      }
      else
      {
        const fullVector<double>& gpCorr = itFind->second;
        totalWeight += gpCorr(3);
        mean.axpy(*its->second, gpCorr(3));
      }
    }
    mean.scale(1./totalWeight);    
  }
  else
  {
    Msg::Error("empty cluster is found");
  }
  return totalWeight;
}

void clutering_impls::meanClusterVec_FullMatrixBased(const std::map<int, fullVector<double> >& dataVec,
                                                       const std::map<int, const fullMatrix<double>* >& cluster,
                                                       const std::map<int, fullVector<double> >& mapPositionsWeights,
                                                       fullVector<double>& mean)
{
  mean.resize(6,true);
  double totalWeight = 0;
  int n = cluster.size();
  if (n > 0)
  {
    for (std::map<int, const fullMatrix<double>* >::const_iterator its = cluster.begin(); its != cluster.end(); its ++)
    {
      std::map<int, fullVector<double> >::const_iterator itFind = mapPositionsWeights.find(its->first);
      std::map<int, fullVector<double> >::const_iterator itVec = dataVec.find(its->first);
      const fullVector<double>& Vec = itVec->second;
      //Vec.print("UUUUU");
      if (itFind==mapPositionsWeights.end())
      {
        printf("wrong map of Gauss poing as GP %d is not found in this map",its->first);
      }
      else
      {
        const fullVector<double>& gpCorr = itFind->second;
        totalWeight += gpCorr(3);
        mean.axpy(Vec, gpCorr(3));
      }
    }
    mean.scale(1./totalWeight);    
  }
  else
  {
    Msg::Error("empty cluster is found");
  }
  //return totalWeight;
}

void clutering_impls::meanClusterVecLong_FullMatrixBased(const std::map<int, fullVector<double> >& dataVec,
                                                       const std::map<int, const fullMatrix<double>* >& cluster,
                                                       const std::map<int, fullVector<double> >& mapPositionsWeights,
                                                       fullVector<double>& mean)
{
  mean.resize(36,true);
  double totalWeight = 0;
  int n = cluster.size();
  if (n > 0)
  {
    for (std::map<int, const fullMatrix<double>* >::const_iterator its = cluster.begin(); its != cluster.end(); its ++)
    {
      std::map<int, fullVector<double> >::const_iterator itFind = mapPositionsWeights.find(its->first);
      std::map<int, fullVector<double> >::const_iterator itVec = dataVec.find(its->first);
      const fullVector<double>& Vec = itVec->second;
      //Vec.print("UUUUU");
      if (itFind==mapPositionsWeights.end())
      {
        printf("wrong map of Gauss poing as GP %d is not found in this map",its->first);
      }
      else
      {
        const fullVector<double>& gpCorr = itFind->second;
        totalWeight += gpCorr(3);
        mean.axpy(Vec, gpCorr(3));
      }
    }
    mean.scale(1./totalWeight);    
  }
  else
  {
    Msg::Error("empty cluster is found");
  }
  //return totalWeight;
}

double clutering_impls::meanClusterVec(const std::map<int, const fullVector<double>* >& cluster,
                            const std::map<int, fullVector<double> >& mapPositionsWeights,
                            fullVector<double>& mean)
{
  mean.resize(6,true);
  double totalWeight = 0;
  int n = cluster.size();
  if (n > 0)
  {
    for (std::map<int, const fullVector<double>* >::const_iterator its = cluster.begin(); its != cluster.end(); its ++)
    {
      std::map<int, fullVector<double> >::const_iterator itFind = mapPositionsWeights.find(its->first);
      if (itFind==mapPositionsWeights.end())
      {
        printf("wrong map of Gauss poing as GP %d is not found in this map",its->first);
      }
      else
      {
        const fullVector<double>& gpCorr = itFind->second;
        totalWeight += gpCorr(3);
        mean.axpy(*its->second, gpCorr(3));
      }
    }
    mean.scale(1./totalWeight);    
  }
  else
  {
    Msg::Error("empty cluster is found");
  }
  return totalWeight;
}

double clutering_impls::meanClusterVecLong(const std::map<int, const fullVector<double>* >& cluster,
                            const std::map<int, fullVector<double> >& mapPositionsWeights,
                            fullVector<double>& mean)
{
  mean.resize(36,true);
  double totalWeight = 0;
  int n = cluster.size();
  if (n > 0)
  {
    for (std::map<int, const fullVector<double>* >::const_iterator its = cluster.begin(); its != cluster.end(); its ++)
    {
      std::map<int, fullVector<double> >::const_iterator itFind = mapPositionsWeights.find(its->first);
      if (itFind==mapPositionsWeights.end())
      {
        printf("wrong map of Gauss poing as GP %d is not found in this map",its->first);
      }
      else
      {
        const fullVector<double>& gpCorr = itFind->second;
        totalWeight += gpCorr(3);
        mean.axpy(*its->second, gpCorr(3));
      }
    }
    mean.scale(1./totalWeight);    
  }
  else
  {
    Msg::Error("empty cluster is found");
  }
  return totalWeight;
}

void clutering_impls::meanClusterPosition(const std::map<int, const fullMatrix<double>* >& cluster,
                            const std::map<int, fullVector<double> >& mapPositionsWeights,
                            double& x_mean, double& y_mean, double& z_mean)
{
  double totalWeight = 0;
  int n = cluster.size();
  if (n > 0)
  {
    for (std::map<int, const fullMatrix<double>* >::const_iterator its = cluster.begin(); its != cluster.end(); its ++)
    {
      std::map<int, fullVector<double> >::const_iterator itFind = mapPositionsWeights.find(its->first);
      if (itFind==mapPositionsWeights.end())
      {
        printf("wrong map of Gauss poing as GP %d is not found in this map",its->first);
      }
      else
      {
        const fullVector<double>& gpCorr = itFind->second;
        totalWeight += gpCorr(3);
        x_mean += gpCorr(0)*gpCorr(3);
        y_mean += gpCorr(1)*gpCorr(3);
        z_mean += gpCorr(2)*gpCorr(3);
      }
    }
    x_mean *= (1./totalWeight);    
    y_mean *= (1./totalWeight);      
    z_mean *= (1./totalWeight);  
  }
  else
  {
    Msg::Error("empty cluster is found");
  }
}

void clutering_impls::meanClusterFiberOrientation(const std::map<int, const fullMatrix<double>* >& cluster,
                                                  const std::map<int, fullVector<double> >& mapPositionsWeights,
                                                  const std::map<int, fullVector<double> >& mapLocalFiberOrientations,
                                                  double& E1_mean, double& E2_mean, double& E3_mean)
{
  double totalWeight = 0;
  int n = cluster.size();
  
  double sinE1_double_mean=0., sinE2_double_mean=0., sinE3_double_mean=0., cosE1_double_mean=0., cosE2_double_mean=0., cosE3_double_mean=0.;
  
  double PI = 3.14159265;
  
  if (n > 0)
  {
    for (std::map<int, const fullMatrix<double>* >::const_iterator its = cluster.begin(); its != cluster.end(); its ++)
    {
      std::map<int, fullVector<double> >::const_iterator itFind = mapPositionsWeights.find(its->first);
      std::map<int, fullVector<double> >::const_iterator itLocOri = mapLocalFiberOrientations.find(its->first);
      if (itFind==mapPositionsWeights.end())
      {
        printf("wrong map of Gauss poing as GP %d is not found in this map",its->first);
      }
      else
      {
        const fullVector<double>& gpCorr = itFind->second;
        const fullVector<double>& locOri = itLocOri->second;
        totalWeight += gpCorr(3);
                
        double e1,e2,e3;
        e1 = locOri(0);
        e2 = locOri(1);
        e3 = locOri(2);
        
        double e1_double,e2_double,e3_double;        
        e1_double = 2.*e1;
        e2_double = 2.*e2;
        e3_double = 2.*e3;     
        
        double sinE1_double, sinE2_double, sinE3_double, cosE1_double, cosE2_double, cosE3_double;
        sinE1_double = sin(e1_double*PI/180.);
        cosE1_double = cos(e1_double*PI/180.);
        sinE2_double = sin(e2_double*PI/180.);
        cosE2_double = cos(e2_double*PI/180.);
        sinE3_double = sin(e3_double*PI/180.);
        cosE3_double = cos(e3_double*PI/180.);
        
        sinE1_double_mean += sinE1_double*gpCorr(3);
        cosE1_double_mean += cosE1_double*gpCorr(3);
        sinE2_double_mean += sinE2_double*gpCorr(3);
        cosE2_double_mean += cosE2_double*gpCorr(3);
        sinE3_double_mean += sinE3_double*gpCorr(3);        
        cosE3_double_mean += cosE3_double*gpCorr(3);                    
      }
    }
    
    E1_mean = atan2(sinE1_double_mean,cosE1_double_mean)*180./PI/2.;
    E2_mean = atan2(sinE2_double_mean,cosE2_double_mean)*180./PI/2.;
    E3_mean = atan2(sinE3_double_mean,cosE3_double_mean)*180./PI/2.;
  }
  else
  {
    Msg::Error("empty cluster is found");
  }
}

void clutering_impls::meanClusterFiberOrientation(const std::map<int, const fullVector<double>* >& cluster,
                                                  const std::map<int, fullVector<double> >& mapPositionsWeights,
                                                  fullVector<double>& mean
                                                  )
{
  mean.resize(3,true);
  double totalWeight = 0;
  int n = cluster.size();
  
  double E1_mean=0., E2_mean=0., E3_mean=0., sinE1_double_mean=0., sinE2_double_mean=0., sinE3_double_mean=0., cosE1_double_mean=0., cosE2_double_mean=0., cosE3_double_mean=0.;
  
  double PI = 3.14159265;
  
  if (n > 0)
  {
    for (std::map<int, const fullVector<double>* >::const_iterator its = cluster.begin(); its != cluster.end(); its ++)
    {
      std::map<int, fullVector<double> >::const_iterator itFind = mapPositionsWeights.find(its->first);
      if (itFind==mapPositionsWeights.end())
      {
        printf("wrong map of Gauss poing as GP %d is not found in this map",its->first);
      }
      else
      {
        const fullVector<double>& gpCorr = itFind->second;
        const fullVector<double>& locOri = *its->second;
        totalWeight += gpCorr(3);
                
        double e1,e2,e3;
        e1 = locOri(0);
        e2 = locOri(1);
        e3 = locOri(2);        
        
        double e1_double,e2_double,e3_double;       
        e1_double = 2.*e1;
        e2_double = 2.*e2;
        e3_double = 2.*e3;     
        
        double sinE1_double, sinE2_double, sinE3_double, cosE1_double, cosE2_double, cosE3_double;
        sinE1_double = sin(e1_double*PI/180.);
        cosE1_double = cos(e1_double*PI/180.);
        sinE2_double = sin(e2_double*PI/180.);
        cosE2_double = cos(e2_double*PI/180.);
        sinE3_double = sin(e3_double*PI/180.);
        cosE3_double = cos(e3_double*PI/180.);
        
        sinE1_double_mean += sinE1_double*gpCorr(3);
        cosE1_double_mean += cosE1_double*gpCorr(3);
        sinE2_double_mean += sinE2_double*gpCorr(3);
        cosE2_double_mean += cosE2_double*gpCorr(3);
        sinE3_double_mean += sinE3_double*gpCorr(3);        
        cosE3_double_mean += cosE3_double*gpCorr(3);    
      }
    }
    
    E1_mean = atan2(sinE1_double_mean,cosE1_double_mean)*180./PI/2.;
    E2_mean = atan2(sinE2_double_mean,cosE2_double_mean)*180./PI/2.;
    E3_mean = atan2(sinE3_double_mean,cosE3_double_mean)*180./PI/2.;
    
    mean(0) = E1_mean;
    mean(1) = E2_mean;
    mean(2) = E3_mean;
  }
  else
  {
    Msg::Error("empty cluster is found");
  }
}

void clutering_impls::meanGeometricClusterVec(const std::map<int, const fullVector<double>* >& cluster,
                                              const std::map<int, fullVector<double> >& mapPositionsWeights,
                                              fullVector<double>& mean)
{
  mean.resize(6,true);
  double totalWeight = 0;
  int n = cluster.size();
  fullVector<double> meanLn(6);
  fullVector<double> plastEqLoc(6);
  double val;
  if (n > 0)
  {
    for (std::map<int, const fullVector<double>* >::const_iterator its = cluster.begin(); its != cluster.end(); its ++)
    {
      std::map<int, fullVector<double> >::const_iterator itFind = mapPositionsWeights.find(its->first);
      if (itFind==mapPositionsWeights.end())
      {
        printf("wrong map of Gauss poing as GP %d is not found in this map",its->first);
      }
      else
      {
        const fullVector<double>& gpCorr = itFind->second;
        totalWeight += gpCorr(3);
        for(int i=0; i<6; i++)
        {
          plastEqLoc = *its->second;
          val = plastEqLoc(i);
          meanLn(i) += log(val+1.)*gpCorr(3);
        }
      }
    }
    meanLn.scale(1./totalWeight);
    for(int i=0; i<6; i++)
    {
      mean(i) = exp(meanLn(i));
      mean(i) -= 1.;
    } 
  }
  else
  {
    Msg::Error("empty cluster is found");
  }
}

void clutering_impls::meanHarmonicClusterVec(const std::map<int, const fullVector<double>* >& cluster,
                                             const std::map<int, fullVector<double> >& mapPositionsWeights,
                                             fullVector<double>& mean)
{
  mean.resize(6,true);
  double totalWeight = 0;
  int n = cluster.size();
  fullVector<double> meanInv(6);
  fullVector<double> plastEqLoc(6);
  double val;
  if (n > 0)
  {
    for (std::map<int, const fullVector<double>* >::const_iterator its = cluster.begin(); its != cluster.end(); its ++)
    {
      std::map<int, fullVector<double> >::const_iterator itFind = mapPositionsWeights.find(its->first);
      if (itFind==mapPositionsWeights.end())
      {
        printf("wrong map of Gauss poing as GP %d is not found in this map",its->first);
      }
      else
      {
        const fullVector<double>& gpCorr = itFind->second;
        totalWeight += gpCorr(3);
        for(int i=0; i<6; i++)
        {
          plastEqLoc = *its->second;
          val = plastEqLoc(i);
          if(val==0){val=1.e-16;}
          meanInv(i) += gpCorr(3)/val;
        }
      }
    }
    meanInv.scale(1./totalWeight);
    for(int i=0; i<6; i++)
    {
      mean(i) = 1./meanInv(i);
    }   
  }
  else
  {
    Msg::Error("empty cluster is found");
  }
}

void clutering_impls::meanHarmonicClusterVec_FullMatrixBased(const std::map<int, fullVector<double> >& dataVec,
                                             const std::map<int, const fullMatrix<double>* >& cluster,
                                             const std::map<int, fullVector<double> >& mapPositionsWeights,
                                             fullVector<double>& mean)
{
  mean.resize(6,true);
  double totalWeight = 0;
  int n = cluster.size();
  fullVector<double> meanInv(6);
  fullVector<double> plastEqLoc(6);
  double val;
  if (n > 0)
  {
    for (std::map<int, const fullMatrix<double>* >::const_iterator its = cluster.begin(); its != cluster.end(); its ++)
    {
      std::map<int, fullVector<double> >::const_iterator itFind = mapPositionsWeights.find(its->first);
      std::map<int, fullVector<double> >::const_iterator itVec = dataVec.find(its->first);
      const fullVector<double>& Vec = itVec->second;
      if (itFind==mapPositionsWeights.end())
      {
        printf("wrong map of Gauss poing as GP %d is not found in this map",its->first);
      }
      else
      {
        const fullVector<double>& gpCorr = itFind->second;
        totalWeight += gpCorr(3);
        for(int i=0; i<6; i++)
        {
          val = Vec(i);
          if(val==0){val=1.e-16;}
          meanInv(i) += gpCorr(3)/val;
        }
      }
    }
    meanInv.scale(1./totalWeight);
    for(int i=0; i<6; i++)
    {
      mean(i) = 1./meanInv(i);
    }   
  }
  else
  {
    Msg::Error("empty cluster is found");
  }
}

void clutering_impls::meanGeometricClusterVec_FullMatrixBased(const std::map<int, fullVector<double> >& dataVec,
                                             const std::map<int, const fullMatrix<double>* >& cluster,
                                             const std::map<int, fullVector<double> >& mapPositionsWeights,
                                             fullVector<double>& mean)
{
  mean.resize(6,true);
  double totalWeight = 0;
  int n = cluster.size();
  fullVector<double> meanLn(6);
  fullVector<double> plastEqLoc(6);
  double val;
  if (n > 0)
  {
    for (std::map<int, const fullMatrix<double>* >::const_iterator its = cluster.begin(); its != cluster.end(); its ++)
    {
      std::map<int, fullVector<double> >::const_iterator itFind = mapPositionsWeights.find(its->first);
      std::map<int, fullVector<double> >::const_iterator itVec = dataVec.find(its->first);
      const fullVector<double>& Vec = itVec->second;
      if (itFind==mapPositionsWeights.end())
      {
        printf("wrong map of Gauss poing as GP %d is not found in this map",its->first);
      }
      else
      {
        const fullVector<double>& gpCorr = itFind->second;
        totalWeight += gpCorr(3);
        for(int i=0; i<6; i++)
        {
          val = Vec(i);
          meanLn(i) += log(val+1.)*gpCorr(3);
        }
      }
    }
    meanLn.scale(1./totalWeight);
    for(int i=0; i<6; i++)
    {
      mean(i) = exp(meanLn(i));
      mean(i) -= 1.;
    } 
  }
  else
  {
    Msg::Error("empty cluster is found");
  }
}


void clutering_impls::meanPowerClusterVec_FullMatrixBased(const std::map<int, fullVector<double> >& dataVec,
                                             const std::map<int, const fullMatrix<double>* >& cluster,
                                             const std::map<int, fullVector<double> >& mapPositionsWeights,
                                             fullVector<double>& mean)
{
  mean.resize(6,true);
  double totalWeight = 0;
  int n = cluster.size();
  fullVector<double> meanPower(6);
  fullVector<double> plastEqLoc(6);
  double exponent = 2.;
  double val;
  if (n > 0)
  {
    for (std::map<int, const fullMatrix<double>* >::const_iterator its = cluster.begin(); its != cluster.end(); its ++)
    {
      std::map<int, fullVector<double> >::const_iterator itFind = mapPositionsWeights.find(its->first);
      std::map<int, fullVector<double> >::const_iterator itVec = dataVec.find(its->first);
      const fullVector<double>& Vec = itVec->second;
      if (itFind==mapPositionsWeights.end())
      {
        printf("wrong map of Gauss poing as GP %d is not found in this map",its->first);
      }
      else
      {
        const fullVector<double>& gpCorr = itFind->second;
        totalWeight += gpCorr(3);
        for(int i=0; i<6; i++)
        {
          meanPower(i) += pow(Vec(i),exponent)*gpCorr(3);
        }
      }
    }
    meanPower.scale(1./totalWeight);
    for(int i=0; i<6; i++)
    {
      mean(i) = pow(meanPower(i),1./exponent);
    } 
  }
  else
  {
    Msg::Error("empty cluster is found");
  }
}

void clutering_impls::Tensor43STDScalarCluster(const std::map<int, const fullMatrix<double>* >& cluster, 
                       const std::map<int, fullVector<double> >& mapPositionsWeights,
                       const STensor43& AmeanCluster,
                       double &clusterStdev)
{
  double totalWeight = 0.;
  double clusterVariance = 0;
  clusterStdev = 0.;
  double clusterNorm = 0;
  int n = cluster.size();
  STensorOperation::contract4STensor43(AmeanCluster,AmeanCluster,clusterNorm);
  clusterNorm = sqrt(clusterNorm);
  for (std::map<int, const fullMatrix<double>* >::const_iterator its = cluster.begin(); its != cluster.end(); its ++)
  {
    std::map<int, fullVector<double> >::const_iterator itFind = mapPositionsWeights.find(its->first);
    if (itFind==mapPositionsWeights.end())
    {
      printf("wrong map of Gauss poing as GP %d is not found in this map",its->first);
    }
    else
    {
      static STensor43 A_diff;
      double A_diff_norm;
      STensorOperation::fromFullMatrixToSTensor43(*its->second,A_diff);
      A_diff -= AmeanCluster;
      STensorOperation::contract4STensor43(A_diff,A_diff,A_diff_norm);
      const fullVector<double>& gpCorr = itFind->second;
      totalWeight += gpCorr(3);
      clusterVariance += gpCorr(3)*A_diff_norm;
    }
  }
  clusterVariance *= 1./totalWeight;
  clusterStdev = sqrt(clusterVariance);
  clusterStdev *= 1./clusterNorm;
}


void clutering_impls::Tensor43STDCluster(const std::map<int, const fullMatrix<double>* >& cluster, 
                       const std::map<int, fullVector<double> >& mapPositionsWeights,
                       const STensor43& AmeanCluster,
                       STensor43 &clusterStdevTensor)
{
  double totalWeight = 0.;
  STensorOperation::zero(clusterStdevTensor);
  fullMatrix<double> clusterSTD_matrix(6,6);
  fullMatrix<double> clusterVar_matrix(6,6);
  int n = cluster.size();
  fullMatrix<double> AmeanCluster_matrix(6,6);
  STensorOperation::fromSTensor43ToFullMatrix(AmeanCluster,AmeanCluster_matrix);
  for (std::map<int, const fullMatrix<double>* >::const_iterator its = cluster.begin(); its != cluster.end(); its ++)
  {
    std::map<int, fullVector<double> >::const_iterator itFind = mapPositionsWeights.find(its->first);
    if (itFind==mapPositionsWeights.end())
    {
      printf("wrong map of Gauss poing as GP %d is not found in this map",its->first);
    }
    else
    {
      fullMatrix<double> A_diff_matrix(6,6), A_diff_squ_matrix(6,6);
      A_diff_matrix = *its->second;
      A_diff_matrix.add(AmeanCluster_matrix,-1.);
      for(int i=0; i<6; i++){
        for(int j=0; j<6; j++){
          A_diff_squ_matrix(i,j) = A_diff_matrix(i,j)*A_diff_matrix(i,j);
        }
      }
      const fullVector<double>& gpCorr = itFind->second;
      totalWeight += gpCorr(3);
      clusterVar_matrix.add(A_diff_squ_matrix,gpCorr(3));
    }
  }
  for(int i=0; i<6; i++){
    for(int j=0; j<6; j++){
      clusterVar_matrix(i,j) *= 1./totalWeight;
      clusterSTD_matrix(i,j) = sqrt(clusterVar_matrix(i,j));
    }
  }
  STensorOperation::fromFullMatrixToSTensor43(clusterSTD_matrix,clusterStdevTensor);
}


void clutering_impls::VectorSTDCluster(const std::map<int, const fullVector<double>* >& cluster, 
                       const std::map<int, fullVector<double> >& mapPositionsWeights,
                       const fullVector<double>& VecmeanCluster,
                       fullVector<double> &clusterSTDVector)
{
  double totalWeight = 0.;
  fullVector<double> clusterVar_vector(36);
  double val_diff_squ;
  int n = cluster.size();
  for (std::map<int, const fullVector<double>* >::const_iterator its = cluster.begin(); its != cluster.end(); its ++)
  {
    std::map<int, fullVector<double> >::const_iterator itFind = mapPositionsWeights.find(its->first);
    if (itFind==mapPositionsWeights.end())
    {
      printf("wrong map of Gauss poing as GP %d is not found in this map",its->first);
    }
    else
    {
      fullVector<double> vec_loc(36);
      vec_loc = *its->second;
      const fullVector<double>& gpCorr = itFind->second;
      totalWeight += gpCorr(3);
      for(int i=0; i<36; i++){
        val_diff_squ = (vec_loc(i)-VecmeanCluster(i))*(vec_loc(i)-VecmeanCluster(i));
        clusterVar_vector(i) += (gpCorr(3)*val_diff_squ);
      }
    }
  }
  for(int i=0; i<6; i++){
    clusterVar_vector(i) *= 1./totalWeight;

    if(abs(VecmeanCluster(i)) > 1.e-12){
    clusterVar_vector(i) *= (1./(VecmeanCluster(i)*VecmeanCluster(i)));
    }
    else{
    clusterVar_vector(i) = 0.;
    } 
    clusterSTDVector(i) = sqrt(clusterVar_vector(i));   
  }
}




int clutering_impls::maxConcentrationCluster(const std::map<int, const fullMatrix<double>* >& cluster)
{
  int maxIP = -1;
  double maxVal = -1.;
  for (std::map<int, const fullMatrix<double>* >::const_iterator it = cluster.begin(); it != cluster.end(); it++)
  {
    const fullMatrix<double>* A = it->second;
    double Anorm = A->norm();
    if (Anorm > maxVal)
    {
      maxIP = it->first;
      maxVal = Anorm;
    }
  }
  return maxIP;
};

int clutering_impls::maxConcentrationVecCluster(const std::map<int, const fullVector<double>* >& cluster)
{
  int maxIP = -1;
  double maxVal = -1.;
  for (std::map<int, const fullVector<double>* >::const_iterator it = cluster.begin(); it != cluster.end(); it++)
  {
    const fullVector<double>* A = it->second;
    double Anorm = A->norm();
    if (Anorm > maxVal)
    {
      maxIP = it->first;
      maxVal = Anorm;
    }
  }
  return maxIP;
};

int clutering_impls::minConcentrationVecCluster(const std::map<int, const fullVector<double>* >& cluster)
{
  int minIP = -1;
  double minVal = 10e6;
  for (std::map<int, const fullVector<double>* >::const_iterator it = cluster.begin(); it != cluster.end(); it++)
  {
    const fullVector<double>* A = it->second;
    double Anorm = A->norm();
    if (Anorm < minVal)
    {
      minIP = it->first;
      minVal = Anorm;
    }
  }
  return minIP;
};


void clutering_impls::printClusters(const std::map<int, std::map<int, const fullMatrix<double>* > >& clusters)
{
  printf("number of clusters= %d\n",clusters.size());
  for (std::map<int, std::map<int, const fullMatrix<double>*> >::const_iterator it = clusters.begin(); it!= clusters.end(); it++)
  {
    int clusterNum = it->first;
    const std::map<int, const fullMatrix<double>*>& cl = it->second;
    Msg::Info("CLUSTER = %d, clusterSize = %d",clusterNum,cl.size());
    for (std::map<int, const fullMatrix<double>*>::const_iterator itset = cl.begin(); itset != cl.end(); itset++)
    {
      printf(" %d",itset->first);
    }
    printf("\n");
  }
};


int clutering_impls::getNearestClusterId(const std::map<int, fullMatrix<double> >& clusterMean, const fullMatrix<double>& pt)
{
  double dist = 0;
  int clusterIndex = -1;
  for (std::map<int, fullMatrix<double> >::const_iterator it = clusterMean.begin(); it!= clusterMean.end(); it++)
  {
    int newClusterIndex = it->first;
    double newDist = clutering_impls::getDiffNorm(pt,it->second);
    if (clusterIndex <0 or newDist<dist)
    {
      dist = newDist;
      clusterIndex = newClusterIndex;
    }
  }
  return clusterIndex;
}


int clutering_impls::getNearestClusterIdVec(const std::map<int, fullVector<double> >& clusterMean, const fullVector<double>& pt)
{
  double dist = 0;
  int clusterIndex = -1;
  for (std::map<int, fullVector<double> >::const_iterator it = clusterMean.begin(); it!= clusterMean.end(); it++)
  {
    int newClusterIndex = it->first;
    double newDist = clutering_impls::getDiffNormVec(pt,it->second);
    if (clusterIndex <0 or newDist<dist)
    {
      dist = newDist;
      clusterIndex = newClusterIndex;
    }
  }
  return clusterIndex;
}

int clutering_impls::getNearestClusterIdVecLong(const std::map<int, fullVector<double> >& clusterMean, const fullVector<double>& pt)
{
  double dist = 0;
  int clusterIndex = -1;
  for (std::map<int, fullVector<double> >::const_iterator it = clusterMean.begin(); it!= clusterMean.end(); it++)
  {
    int newClusterIndex = it->first;
    double newDist = clutering_impls::getDiffNormVecLong(pt,it->second);
    //double newDist = clutering_impls::getDiffShearNormVecLong(pt,it->second);
    //double newDist = clutering_impls::getDiffNormVecPowerLong(pt,it->second);
    if (clusterIndex <0 or newDist<dist)
    {
      dist = newDist;
      clusterIndex = newClusterIndex;
    }
  }
  return clusterIndex;
}

int clutering_impls::getNearestClusterIdOrientationBased(const std::map<int, fullVector<double> >& clusterMean, const fullVector<double>& pt)
{
  double dist = 0;
  int clusterIndex = -1;
  for (std::map<int, fullVector<double> >::const_iterator it = clusterMean.begin(); it!= clusterMean.end(); it++)
  {
    int newClusterIndex = it->first;
    double newDist = clutering_impls::getDiffNormOrientationBased(pt,it->second);
    if (clusterIndex <0 or newDist<dist)
    {
      dist = newDist;
      clusterIndex = newClusterIndex;
    }
  }
  return clusterIndex;
}


bool clutering_impls::kmean_cluster(const std::map<int, fullMatrix<double> >& data, const int nbClusters, const int minElemEachCluster,
                                    const int maxNbIterations,
                                    const std::map<int, fullVector<double> >& mapPositionsWeights,
                                    std::map<int, std::map<int, const fullMatrix<double>* > >& clusters)
{
  
  if (nbClusters == 1)
  {
    clusters.clear();
    std::map<int, const fullMatrix<double>* >& cluster_i = clusters[0];
    for (std::map<int, fullMatrix<double> >::const_iterator it = data.begin();  it!= data.end(); it++)
    {
      cluster_i[it->first] = &(it->second);
    }
    return true;
  }
 
  unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
  std::default_random_engine generator (seed);
  std::uniform_int_distribution<int> distribution(0, nbClusters-1);
  
  std::map<int,int> IdMapToClusterId;
  // initialization to nbClusters, 
  // empty cluster is avoided
  clusters.clear();
  std::map<int, fullMatrix<double> >::const_iterator itAss = data.begin();
  for (int i=0; i< nbClusters; i++)
  {
    std::map<int, const fullMatrix<double>* >& cluster_i = clusters[i];
    for (int j=0; j< std::max(1,minElemEachCluster); j++)
    {
      cluster_i[itAss->first] = &(itAss->second);
      IdMapToClusterId[itAss->first] = i;
      itAss++;
    }
  }
  
  for (std::map<int, fullMatrix<double> >::const_iterator it = itAss;  it!= data.end(); it++)
  {
    int cl  = distribution(generator);  
    std::map<int, const fullMatrix<double>* >& cluster_i = clusters[cl];
    cluster_i[it->first] = &(it->second);
    IdMapToClusterId[it->first] = cl;
  }
  
  // print cluster
  //clutering_impls::printClusters(clusters);
  // initialized k-means
  std::map<int, fullMatrix<double> > A_clustermean;
  for (int i=0; i< nbClusters; i++)
  {
    fullMatrix<double>& Amean = A_clustermean[i];
    clutering_impls::meanCluster(clusters[i],mapPositionsWeights,Amean);
    //Amean.print("Amean");
  }
  
  //clustering
  int iter=0;
  while (true)
  {
    iter++;
    if (iter> maxNbIterations)
    {
      Msg::Info("maximal iteration exceeds");
      return false;
      break;
    }
    int numberChange = 0;
    // at each point, find neareast cluster
    std::map<int,int> previousMapId=IdMapToClusterId;
    IdMapToClusterId.clear();
    
    for (std::map<int, fullMatrix<double> >::const_iterator it = data.begin();  it!= data.end(); it++)
    { 
      int type = it->first;
      const fullMatrix<double>& mat = it->second;
      int currentCluserId = previousMapId[type];
      int newCluserId = clutering_impls::getNearestClusterId(A_clustermean,mat);
      // start as previous
      IdMapToClusterId[type] = previousMapId[type];
      if (currentCluserId != newCluserId)
      {
        // remove from current cluster and add to the new one
        std::map<int, const fullMatrix<double>* >& cluster_old = clusters[currentCluserId];
        if (cluster_old.size() > minElemEachCluster)
        {
          numberChange ++;
          cluster_old.erase(type);
          std::map<int, const fullMatrix<double>* >& cluster_new = clusters[newCluserId];
          cluster_new[type] = &mat;
          IdMapToClusterId[type] = newCluserId;
        }
      }
    }
    
    Msg::Info("iter = %d, number moving = %d",iter,numberChange);
    
    if (numberChange ==0 and iter>1)
    {
      Msg::Info("Done clustering as clusters are not modified !!!");
      break;
    }
    else
    {
      // re compute centering
      for (int i=0; i< nbClusters; i++)
      {
        fullMatrix<double>& Amean = A_clustermean[i];
        clutering_impls::meanCluster(clusters[i], mapPositionsWeights, Amean);
      }
    }
  }
  
  //clutering_impls::printClusters(clusters);
  return true;
}


bool clutering_impls::kmean_cluster_FullVectorBased(const std::map<int, fullVector<double> >& dataVec, 
                                                    const std::map<int, fullMatrix<double> >& data, 
                                                    const int nbClusters, 
                                                    const int minElemEachCluster,
                                                    const int maxNbIterations,
                                                    const std::map<int, fullVector<double> >& mapPositionsWeights,
                                                    std::map<int, std::map<int, const fullVector<double>* > >& clustersVec,
                                                    std::map<int, std::map<int, const fullMatrix<double>* > >& clusters)
{ 
  if (nbClusters == 1)
  {
    clustersVec.clear();
    std::map<int, const fullVector<double>* >& clusterVec_i = clustersVec[0];
    clusters.clear();
    std::map<int, const fullMatrix<double>* >& cluster_i = clusters[0];
    for (std::map<int, fullVector<double> >::const_iterator it = dataVec.begin();  it!= dataVec.end(); it++)
    {
      clusterVec_i[it->first] = &(it->second);
    }
    for (std::map<int, fullMatrix<double> >::const_iterator it = data.begin();  it!= data.end(); it++)
    {
      cluster_i[it->first] = &(it->second);
    }
    return true;
  }
 
  unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
  std::default_random_engine generator (seed);
  std::uniform_int_distribution<int> distribution(0, nbClusters-1);
  
  std::map<int,int> IdMapToClusterId;
  // initialization to nbClusters, 
  // empty cluster is avoided
  clustersVec.clear();
  clusters.clear();
  std::map<int, fullVector<double> >::const_iterator itAssVec = dataVec.begin();
  int type = itAssVec->first;
  std::map<int, fullMatrix<double> >::const_iterator itAss = data.find(type);

  for (int i=0; i< nbClusters; i++)
  {
    std::map<int, const fullVector<double>* >& clusterVec_i = clustersVec[i];
    std::map<int, const fullMatrix<double>* >& cluster_i = clusters[i];
    for (int j=0; j< std::max(1,minElemEachCluster); j++)
    {
      type = itAssVec->first;
      std::map<int, fullMatrix<double> >::const_iterator itAss = data.find(type);
      clusterVec_i[type] = &(itAssVec->second);
      cluster_i[type] = &(itAss->second);
      IdMapToClusterId[type] = i;
      itAssVec++;
    }
  }

  for (std::map<int, fullVector<double> >::const_iterator itVec = itAssVec;  itVec!= dataVec.end(); itVec++)
  {
    int cl  = distribution(generator);  
    std::map<int, const fullVector<double>* >& clusterVec_i = clustersVec[cl];
    std::map<int, const fullMatrix<double>* >& cluster_i = clusters[cl];
    clusterVec_i[itVec->first] = &(itVec->second);
    type = itVec->first;
    std::map<int, fullMatrix<double> >::const_iterator it = data.find(type);
    cluster_i[type] = &(it->second);
    IdMapToClusterId[type] = cl;
  }

  // print cluster
  //clutering_impls::printClusters(clusters);
  // initialized k-means
  std::map<int, fullVector<double> > Vec_clustermean;
  for (int i=0; i< nbClusters; i++)
  {
    fullVector<double>& Vecmean = Vec_clustermean[i];
    clutering_impls::meanClusterVecLong(clustersVec[i],mapPositionsWeights,Vecmean);
  }
  
  //clustering
  int iter=0;
  while (true)
  {
    iter++;
    if (iter> maxNbIterations)
    {
      Msg::Info("maximal iteration exceeds");
      return false;
      break;
    }
    int numberChange = 0;
    // at each point, find neareast cluster
    std::map<int,int> previousMapId=IdMapToClusterId;
    IdMapToClusterId.clear();
    
    for (std::map<int, fullVector<double> >::const_iterator itVec = dataVec.begin();  itVec!= dataVec.end(); itVec++)
    { 
      int type = itVec->first;
      const fullVector<double>& vec = itVec->second;
      std::map<int, fullMatrix<double> >::const_iterator it = data.find(type);
      const fullMatrix<double>& mat = it->second;
      int currentCluserId = previousMapId[type];
      int newCluserId = clutering_impls::getNearestClusterIdVecLong(Vec_clustermean,vec);
      // start as previous
      IdMapToClusterId[type] = previousMapId[type];
      if (currentCluserId != newCluserId)
      {
        // remove from current cluster and add to the new one
        std::map<int, const fullVector<double>* >& clusterVec_old = clustersVec[currentCluserId];
        std::map<int, const fullMatrix<double>* >& cluster_old = clusters[currentCluserId];
        if (clusterVec_old.size() > minElemEachCluster)
        {
          numberChange ++;
          IdMapToClusterId[type] = newCluserId;
          clusterVec_old.erase(type);
          cluster_old.erase(type);
          std::map<int, const fullVector<double>* >& clusterVec_new = clustersVec[newCluserId];
          std::map<int, const fullMatrix<double>* >& cluster_new = clusters[newCluserId];
          clusterVec_new[type] = &vec;
          cluster_new[type] = &mat;
        }
      }
    }
    
    Msg::Info("iter = %d, number moving = %d",iter,numberChange);
    
    if (numberChange ==0 and iter>1)
    {
      Msg::Info("Done clustering as clusters are not modified !!!");
      break;
    }
    else
    {
      // re compute centering
      for (int i=0; i< nbClusters; i++)
      {
        fullVector<double>& Vecmean = Vec_clustermean[i];
        clutering_impls::meanClusterVecLong(clustersVec[i], mapPositionsWeights, Vecmean);
      }
    }
  }
  
  //clutering_impls::printClusters(clusters);
  return true;
}

bool clutering_impls::kmean_cluster_orientationBased(const std::map<int, fullVector<double> >& dataVecOri, 
                                                     const std::map<int, fullMatrix<double> >& data, 
                                                     const int nbClusters, 
                                                     const int minElemEachCluster,
                                                     const int maxNbIterations,
                                                     const std::map<int, fullVector<double> >& mapPositionsWeights,
                                                     std::map<int, std::map<int, const fullVector<double>* > >& clustersOri,
                                                     std::map<int, std::map<int, const fullMatrix<double>* > >& clusters)
{ 
  if (nbClusters == 1)
  {
    clustersOri.clear();
    std::map<int, const fullVector<double>* >& clusterVec_i = clustersOri[0];
    clusters.clear();
    std::map<int, const fullMatrix<double>* >& cluster_i = clusters[0];
    for (std::map<int, fullVector<double> >::const_iterator it = dataVecOri.begin();  it!= dataVecOri.end(); it++)
    {
      clusterVec_i[it->first] = &(it->second);
    }
    for (std::map<int, fullMatrix<double> >::const_iterator it = data.begin();  it!= data.end(); it++)
    {
      cluster_i[it->first] = &(it->second);
    }
    return true;
  }
 
  unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
  std::default_random_engine generator (seed);
  std::uniform_int_distribution<int> distribution(0, nbClusters-1);
  
  std::map<int,int> IdMapToClusterId;
  // initialization to nbClusters, 
  // empty cluster is avoided
  clustersOri.clear();
  clusters.clear();
  std::map<int, fullVector<double> >::const_iterator itAssVec = dataVecOri.begin();
  int type = itAssVec->first;
  std::map<int, fullMatrix<double> >::const_iterator itAss = data.find(type);

  for (int i=0; i< nbClusters; i++)
  {
    std::map<int, const fullVector<double>* >& clusterVec_i = clustersOri[i];
    std::map<int, const fullMatrix<double>* >& cluster_i = clusters[i];
    for (int j=0; j< std::max(1,minElemEachCluster); j++)
    {
      type = itAssVec->first;
      std::map<int, fullMatrix<double> >::const_iterator itAss = data.find(type);
      clusterVec_i[type] = &(itAssVec->second);
      cluster_i[type] = &(itAss->second);
      IdMapToClusterId[type] = i;
      itAssVec++;
    }
  }

  for (std::map<int, fullVector<double> >::const_iterator itVec = itAssVec;  itVec!= dataVecOri.end(); itVec++)
  {
    int cl  = distribution(generator);  
    std::map<int, const fullVector<double>* >& clusterVec_i = clustersOri[cl];
    std::map<int, const fullMatrix<double>* >& cluster_i = clusters[cl];
    clusterVec_i[itVec->first] = &(itVec->second);
    type = itVec->first;
    std::map<int, fullMatrix<double> >::const_iterator it = data.find(type);
    cluster_i[type] = &(it->second);
    IdMapToClusterId[type] = cl;
  }

  // print cluster
  //clutering_impls::printClusters(clusters);
  // initialized k-means
  std::map<int, fullVector<double> > Vec_clustermean;
  for (int i=0; i< nbClusters; i++)
  {
    fullVector<double>& Vecmean = Vec_clustermean[i];
    clutering_impls::meanClusterFiberOrientation(clustersOri[i],mapPositionsWeights,Vecmean);
  }
    
  //clustering
  int iter=0;
  while (true)
  {
    iter++;
    if (iter> maxNbIterations)
    {
      Msg::Info("maximal iteration exceeds");
      return false;
      break;
    }
    int numberChange = 0;
    // at each point, find neareast cluster
    std::map<int,int> previousMapId=IdMapToClusterId;
    IdMapToClusterId.clear();
    
    for (std::map<int, fullVector<double> >::const_iterator itVec = dataVecOri.begin();  itVec!= dataVecOri.end(); itVec++)
    { 
      int type = itVec->first;
      const fullVector<double>& vec = itVec->second;
      std::map<int, fullMatrix<double> >::const_iterator it = data.find(type);
      const fullMatrix<double>& mat = it->second;
      int currentCluserId = previousMapId[type];
      int newCluserId = clutering_impls::getNearestClusterIdOrientationBased(Vec_clustermean,vec);
      // start as previous
      IdMapToClusterId[type] = previousMapId[type];
      if (currentCluserId != newCluserId)
      {
        // remove from current cluster and add to the new one
        std::map<int, const fullVector<double>* >& clusterVec_old = clustersOri[currentCluserId];
        std::map<int, const fullMatrix<double>* >& cluster_old = clusters[currentCluserId];
        if (clusterVec_old.size() > minElemEachCluster)
        {
          numberChange ++;
          IdMapToClusterId[type] = newCluserId;
          clusterVec_old.erase(type);
          cluster_old.erase(type);
          std::map<int, const fullVector<double>* >& clusterVec_new = clustersOri[newCluserId];
          std::map<int, const fullMatrix<double>* >& cluster_new = clusters[newCluserId];
          clusterVec_new[type] = &vec;
          cluster_new[type] = &mat;
        }
      }
    }
    
    Msg::Info("iter = %d, number moving = %d",iter,numberChange);
    
    if (numberChange ==0 and iter>1)
    {
      Msg::Info("Done clustering as clusters are not modified !!!");
      break;
    }
    else
    {
      // re compute centering
      for (int i=0; i< nbClusters; i++)
      {
        fullVector<double>& Vecmean = Vec_clustermean[i];
        clutering_impls::meanClusterFiberOrientation(clustersOri[i], mapPositionsWeights, Vecmean);
      }
    }
  }
  
  //clutering_impls::printClusters(clusters);
  return true;
}


bool clutering_impls::kmean_cluster_hierarchical_orientation_fullVectorBased(const std::map<int, fullVector<double> >& dataOri, 
                                                                             const std::map<int, fullVector<double> >& dataVec, 
                                                                             const std::map<int, fullMatrix<double> >& data, 
                                                                             int& nbClusters, 
                                                                             const int minElemEachCluster,
                                                                             const int maxNbIterations,
                                                                             const std::map<int, fullVector<double> >& mapPositionsWeights,
                                                                             std::map<int, std::map<int, const fullVector<double>* > >& clustersOri,
                                                                             std::map<int, std::map<int, const fullVector<double>* > >& clustersVec,
                                                                             std::map<int, std::map<int, const fullMatrix<double>* > >& clusters)
{ 
  if (nbClusters == 1)
  {
    clustersOri.clear();
    std::map<int, const fullVector<double>* >& clusterOri_i = clustersOri[0];
    clusters.clear();
    std::map<int, const fullMatrix<double>* >& cluster_i = clusters[0];
    clustersVec.clear();
    std::map<int, const fullVector<double>* >& clusterVec_i = clustersVec[0];
    for (std::map<int, fullVector<double> >::const_iterator it = dataOri.begin();  it!= dataOri.end(); it++)
    {
      clusterOri_i[it->first] = &(it->second);
    }
    for (std::map<int, fullMatrix<double> >::const_iterator it = data.begin();  it!= data.end(); it++)
    {
      cluster_i[it->first] = &(it->second);
    }
    for (std::map<int, fullVector<double> >::const_iterator it = dataVec.begin();  it!= dataVec.end(); it++)
    {
      clusterVec_i[it->first] = &(it->second);
    }
    //return true;
  }
 
  unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
  std::default_random_engine generator (seed);
  std::uniform_int_distribution<int> distribution(0, nbClusters-1);
  
  std::map<int,int> IdMapToClusterId;
  // initialization to nbClusters, 
  // empty cluster is avoided
  clustersOri.clear();
  clusters.clear();
  clustersVec.clear();
  std::map<int, fullVector<double> >::const_iterator itAssOri = dataOri.begin();
  int type = itAssOri->first;
  std::map<int, fullMatrix<double> >::const_iterator itAss = data.find(type);
  std::map<int, fullVector<double> >::const_iterator itAssVec = dataVec.begin();

  for (int i=0; i< nbClusters; i++)
  {
    std::map<int, const fullVector<double>* >& clusterOri_i = clustersOri[i];
    std::map<int, const fullMatrix<double>* >& cluster_i = clusters[i];
    std::map<int, const fullVector<double>* >& clusterVec_i = clustersVec[i];
    for (int j=0; j< std::max(1,minElemEachCluster); j++)
    {
      type = itAssOri->first;
      std::map<int, fullMatrix<double> >::const_iterator itAss = data.find(type);
      std::map<int, fullVector<double> >::const_iterator itAssVec = dataVec.find(type);
      clusterOri_i[type] = &(itAssOri->second);
      cluster_i[type] = &(itAss->second);
      clusterVec_i[type] = &(itAssVec->second);
      IdMapToClusterId[type] = i;
      itAssOri++;
    }
  }

  for (std::map<int, fullVector<double> >::const_iterator itOri = itAssOri;  itOri!= dataOri.end(); itOri++)
  {
    int cl  = distribution(generator);  
    std::map<int, const fullVector<double>* >& clusterOri_i = clustersOri[cl];
    std::map<int, const fullMatrix<double>* >& cluster_i = clusters[cl];
    std::map<int, const fullVector<double>* >& clusterVec_i = clustersVec[cl];
    clusterOri_i[itOri->first] = &(itOri->second);
    type = itOri->first;
    std::map<int, fullMatrix<double> >::const_iterator it = data.find(type);
    cluster_i[type] = &(it->second);
    std::map<int, fullVector<double> >::const_iterator itVec = dataVec.find(type);
    clusterVec_i[type] = &(itVec->second);
    IdMapToClusterId[type] = cl;
  }

  // print cluster
  //clutering_impls::printClusters(clusters);
  // initialized k-means
  std::map<int, fullVector<double> > Ori_clustermean;
  for (int i=0; i< nbClusters; i++)
  {
    fullVector<double>& orimean = Ori_clustermean[i];
    clutering_impls::meanClusterFiberOrientation(clustersOri[i],mapPositionsWeights,orimean);
  }
    
  //clustering
  int iter=0;
  while (true)
  {
    iter++;
    if (iter> maxNbIterations)
    {
      Msg::Info("maximal iteration exceeds");
      return false;
      break;
    }
    int numberChange = 0;
    // at each point, find neareast cluster
    std::map<int,int> previousMapId=IdMapToClusterId;
    IdMapToClusterId.clear();
    
    for (std::map<int, fullVector<double> >::const_iterator itOri = dataOri.begin();  itOri!= dataOri.end(); itOri++)
    { 
      int type = itOri->first;
      const fullVector<double>& ori = itOri->second;
      std::map<int, fullMatrix<double> >::const_iterator it = data.find(type);
      const fullMatrix<double>& mat = it->second;
      std::map<int, fullVector<double> >::const_iterator itVec = dataVec.find(type);
      const fullVector<double>& vec = itVec->second;
      int currentCluserId = previousMapId[type];
      int newCluserId = clutering_impls::getNearestClusterIdOrientationBased(Ori_clustermean,ori);
      // start as previous
      IdMapToClusterId[type] = previousMapId[type];
      if (currentCluserId != newCluserId)
      {
        // remove from current cluster and add to the new one
        std::map<int, const fullVector<double>* >& clusterOri_old = clustersOri[currentCluserId];
        std::map<int, const fullMatrix<double>* >& cluster_old = clusters[currentCluserId];
        std::map<int, const fullVector<double>* >& clusterVec_old = clustersVec[currentCluserId];
        if (clusterOri_old.size() > minElemEachCluster)
        {
          numberChange ++;
          IdMapToClusterId[type] = newCluserId;
          clusterOri_old.erase(type);
          cluster_old.erase(type);
          clusterVec_old.erase(type);
          std::map<int, const fullVector<double>* >& clusterOri_new = clustersOri[newCluserId];
          std::map<int, const fullMatrix<double>* >& cluster_new = clusters[newCluserId];
          std::map<int, const fullVector<double>* >& clusterVec_new = clustersVec[newCluserId];
          clusterOri_new[type] = &ori;
          cluster_new[type] = &mat;
          clusterVec_new[type] = &vec;
        }
      }
    }
    
    Msg::Info("iter = %d, number moving = %d",iter,numberChange);
    
    if (numberChange ==0 and iter>1)
    {
      Msg::Info("Done clustering as clusters are not modified !!!");
      break;
    }
    else
    {
      // re compute centering
      for (int i=0; i< nbClusters; i++)
      {
        fullVector<double>& orimean = Ori_clustermean[i];
        clutering_impls::meanClusterFiberOrientation(clustersOri[i], mapPositionsWeights, orimean);
      }
    }
  }
  
  std::map<int, std::map<int, const fullMatrix<double>* > > clusters_new;
  clusters_new.clear();
  std::map<int, std::map<int, const fullVector<double>* > > clustersVec_new;
  clustersVec_new.clear();
  
  //clutering_impls::printClusters(clusters);
  
  int nbClustersLevel2 = 2;
  nbClusters *= nbClustersLevel2;
  
  int minElemEachClusterLevel1 = 1;
  
  for (std::map<int, std::map<int, const fullMatrix<double>* > >::const_iterator  itCl = clusters.begin(); itCl != clusters.end(); itCl++)
  {
    int clNum = itCl->first;
    
    const std::map<int, const fullMatrix<double>* >& dataInClPointer = itCl->second;
    const std::map<int, const fullVector<double>* >& dataInClVecPointer = clustersVec[itCl->first];
    
    std::map<int, fullMatrix<double> > dataInCl;
    std::map<int, fullVector<double> > dataInClVec;
    
    for (std::map<int, const fullMatrix<double>* >::const_iterator  itp = dataInClPointer.begin(); itp != dataInClPointer.end(); itp++)
    {
      fullMatrix<double>& mat = dataInCl[itp->first];
      mat = *(itp->second);
    }
    
    for (std::map<int, const fullVector<double>* >::const_iterator  itp = dataInClVecPointer.begin(); itp != dataInClVecPointer.end(); itp++)
    {
      fullVector<double>& vec = dataInClVec[itp->first];
      vec = *(itp->second);
    }
    
    std::map<int, std::map<int, const fullMatrix<double>*> > clusterInCl_pnt;
    std::map<int, std::map<int, const fullVector<double>*> > clusterInClVec_pnt;
    
    bool ok = false;
    ok= clutering_impls::kmean_cluster_FullVectorBased(dataInClVec,dataInCl,nbClustersLevel2,minElemEachClusterLevel1,maxNbIterations,mapPositionsWeights,clusterInClVec_pnt,clusterInCl_pnt);
    
    for (std::map<int, std::map<int, const fullMatrix<double>*> >::const_iterator  itCl = clusterInCl_pnt.begin(); itCl != clusterInCl_pnt.end(); itCl++)
    {
      int clNumInCluster = itCl->first;
      std::map<int, const fullMatrix<double>*> clusterMat = itCl->second;
      std::map<int, const fullMatrix<double>* >& clusterMat_new = clusters_new[clNum*nbClustersLevel2+clNumInCluster];
      std::map<int, const fullVector<double>* >& clusterVec_new = clustersVec_new[clNum*nbClustersLevel2+clNumInCluster];
      
      for (std::map<int, const fullMatrix<double>*>::const_iterator  itgp = clusterMat.begin(); itgp != clusterMat.end(); itgp++)
      {
        std::map<int, fullMatrix<double> >::const_iterator itMat = data.find(itgp->first);
        const fullMatrix<double>& mat = itMat->second;
        clusterMat_new[itgp->first] = &mat;
        
        std::map<int, fullVector<double> >::const_iterator itVec = dataVec.find(itgp->first);
        const fullVector<double>& vec = itVec->second;
        clusterVec_new[itgp->first] = &vec;
      }   
    }
  }
  
  clusters.clear();
  clustersVec.clear();
  
  clusters = clusters_new;
  clustersVec = clustersVec_new;
  
  //clutering_impls::printClusters(clusters);
  return true;
}


bool clutering_impls::kmean_cluster_hierarchical_fullMatrix_fullVectorBased(const std::map<int, fullVector<double> >& dataVec, 
                                                                            const std::map<int, fullMatrix<double> >& data, 
                                                                            int& nbClusters, 
                                                                            const int minElemEachCluster,
                                                                            const int maxNbIterations,
                                                                            const std::map<int, fullVector<double> >& mapPositionsWeights,
                                                                            std::map<int, std::map<int, const fullVector<double>* > >& clustersVec,
                                                                            std::map<int, std::map<int, const fullMatrix<double>* > >& clusters)
{
  if (nbClusters == 1)
  {
    clusters.clear();
    std::map<int, const fullMatrix<double>* >& cluster_i = clusters[0];
    clustersVec.clear();
    std::map<int, const fullVector<double>* >& clusterVec_i = clustersVec[0];
    for (std::map<int, fullMatrix<double> >::const_iterator it = data.begin();  it!= data.end(); it++)
    {
      cluster_i[it->first] = &(it->second);
    }
    for (std::map<int, fullVector<double> >::const_iterator it = dataVec.begin();  it!= dataVec.end(); it++)
    {
      clusterVec_i[it->first] = &(it->second);
    }
    //return true;
  }
 
  unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
  std::default_random_engine generator (seed);
  std::uniform_int_distribution<int> distribution(0, nbClusters-1);
  
  std::map<int,int> IdMapToClusterId;
  // initialization to nbClusters, 
  // empty cluster is avoided
  clusters.clear();
  clustersVec.clear();
  std::map<int, fullMatrix<double> >::const_iterator itAss = data.begin();
  int type = itAss->first;
  std::map<int, fullVector<double> >::const_iterator itAssVec = dataVec.find(type);

  for (int i=0; i< nbClusters; i++)
  {
    std::map<int, const fullMatrix<double>* >& cluster_i = clusters[i];
    std::map<int, const fullVector<double>* >& clusterVec_i = clustersVec[i];
    for (int j=0; j< std::max(1,minElemEachCluster); j++)
    {
      type = itAss->first;
      std::map<int, fullVector<double> >::const_iterator itAssVec = dataVec.find(type);
      cluster_i[type] = &(itAss->second);
      clusterVec_i[type] = &(itAssVec->second);
      IdMapToClusterId[type] = i;
      itAss++;
    }
  }

  for (std::map<int, fullMatrix<double> >::const_iterator it = itAss;  it!= data.end(); it++)
  {
    int cl  = distribution(generator);  
    std::map<int, const fullMatrix<double>* >& cluster_i = clusters[cl];
    std::map<int, const fullVector<double>* >& clusterVec_i = clustersVec[cl];
    type = it->first;
    cluster_i[type] = &(it->second);
    std::map<int, fullVector<double> >::const_iterator itVec = dataVec.find(type);
    clusterVec_i[type] = &(itVec->second);
    IdMapToClusterId[type] = cl;
  }

  // print cluster
  //clutering_impls::printClusters(clusters);
  // initialized k-means
  std::map<int, fullMatrix<double> > A_clustermean;
  for (int i=0; i< nbClusters; i++)
  {
    fullMatrix<double>& Amean = A_clustermean[i];
    clutering_impls::meanCluster(clusters[i],mapPositionsWeights,Amean);
  }
    
  //clustering
  int iter=0;
  while (true)
  {
    iter++;
    if (iter> maxNbIterations)
    {
      Msg::Info("maximal iteration exceeds");
      return false;
      break;
    }
    int numberChange = 0;
    // at each point, find neareast cluster
    std::map<int,int> previousMapId=IdMapToClusterId;
    IdMapToClusterId.clear();
    
    for (std::map<int, fullMatrix<double> >::const_iterator it = data.begin();  it!= data.end(); it++)
    { 
      int type = it->first;
      const fullMatrix<double>& mat = it->second;
      std::map<int, fullVector<double> >::const_iterator itVec = dataVec.find(type);
      const fullVector<double>& vec = itVec->second;
      int currentCluserId = previousMapId[type];
      int newCluserId = clutering_impls::getNearestClusterId(A_clustermean,mat);
      // start as previous
      IdMapToClusterId[type] = previousMapId[type];
      if (currentCluserId != newCluserId)
      {
        // remove from current cluster and add to the new one
        std::map<int, const fullMatrix<double>* >& cluster_old = clusters[currentCluserId];
        std::map<int, const fullVector<double>* >& clusterVec_old = clustersVec[currentCluserId];
        if (cluster_old.size() > minElemEachCluster)
        {
          numberChange ++;
          IdMapToClusterId[type] = newCluserId;
          cluster_old.erase(type);
          clusterVec_old.erase(type);
          std::map<int, const fullMatrix<double>* >& cluster_new = clusters[newCluserId];
          std::map<int, const fullVector<double>* >& clusterVec_new = clustersVec[newCluserId];
          cluster_new[type] = &mat;
          clusterVec_new[type] = &vec;
        }
      }
    }
    
    Msg::Info("iter = %d, number moving = %d",iter,numberChange);
    
    if (numberChange ==0 and iter>1)
    {
      Msg::Info("Done clustering as clusters are not modified !!!");
      break;
    }
    else
    {
      // re compute centering
      for (int i=0; i< nbClusters; i++)
      {
        fullMatrix<double>& Amean = A_clustermean[i];
        clutering_impls::meanCluster(clusters[i], mapPositionsWeights, Amean);
      }
    }
  }
  
  std::map<int, std::map<int, const fullMatrix<double>* > > clusters_new;
  clusters_new.clear();
  std::map<int, std::map<int, const fullVector<double>* > > clustersVec_new;
  clustersVec_new.clear();
  
  //clutering_impls::printClusters(clusters);
  
  int nbClustersLevel2 = 4;
  if(nbClusters==1){nbClustersLevel2=1;}
  nbClusters *= nbClustersLevel2;
  
  int minElemEachClusterLevel1 = 1;
  
  for (std::map<int, std::map<int, const fullMatrix<double>* > >::const_iterator  itCl = clusters.begin(); itCl != clusters.end(); itCl++)
  {
    int clNum = itCl->first;
    
    const std::map<int, const fullMatrix<double>* >& dataInClPointer = itCl->second;
    const std::map<int, const fullVector<double>* >& dataInClVecPointer = clustersVec[itCl->first];
    
    std::map<int, fullMatrix<double> > dataInCl;
    std::map<int, fullVector<double> > dataInClVec;
    
    for (std::map<int, const fullMatrix<double>* >::const_iterator  itp = dataInClPointer.begin(); itp != dataInClPointer.end(); itp++)
    {
      fullMatrix<double>& mat = dataInCl[itp->first];
      mat = *(itp->second);
    }
    
    for (std::map<int, const fullVector<double>* >::const_iterator  itp = dataInClVecPointer.begin(); itp != dataInClVecPointer.end(); itp++)
    {
      fullVector<double>& vec = dataInClVec[itp->first];
      vec = *(itp->second);
    }
    
    std::map<int, std::map<int, const fullMatrix<double>*> > clusterInCl_pnt;
    std::map<int, std::map<int, const fullVector<double>*> > clusterInClVec_pnt;
    
    bool ok = false;
    ok= clutering_impls::kmean_cluster_FullVectorBased(dataInClVec,dataInCl,nbClustersLevel2,minElemEachClusterLevel1,maxNbIterations,mapPositionsWeights,clusterInClVec_pnt,clusterInCl_pnt);
    
    for (std::map<int, std::map<int, const fullMatrix<double>*> >::const_iterator  itCl = clusterInCl_pnt.begin(); itCl != clusterInCl_pnt.end(); itCl++)
    {
      int clNumInCluster = itCl->first;
      std::map<int, const fullMatrix<double>*> clusterMat = itCl->second;
      std::map<int, const fullMatrix<double>* >& clusterMat_new = clusters_new[clNum*nbClustersLevel2+clNumInCluster];
      std::map<int, const fullVector<double>* >& clusterVec_new = clustersVec_new[clNum*nbClustersLevel2+clNumInCluster];
      
      for (std::map<int, const fullMatrix<double>*>::const_iterator  itgp = clusterMat.begin(); itgp != clusterMat.end(); itgp++)
      {
        std::map<int, fullMatrix<double> >::const_iterator itMat = data.find(itgp->first);
        const fullMatrix<double>& mat = itMat->second;
        clusterMat_new[itgp->first] = &mat;
        
        std::map<int, fullVector<double> >::const_iterator itVec = dataVec.find(itgp->first);
        const fullVector<double>& vec = itVec->second;
        clusterVec_new[itgp->first] = &vec;
      }   
    }
  }
  
  clusters.clear();
  clustersVec.clear();
  
  clusters = clusters_new;
  clustersVec = clustersVec_new;
  
  //clutering_impls::printClusters(clusters);
  return true;
}

bool clutering_impls::kmean_cluster_hierarchical_fullVector_fullMatrixBased(const std::map<int, fullVector<double> >& dataVec, 
                                                                            const std::map<int, fullMatrix<double> >& data, 
                                                                            int& nbClusters, 
                                                                            const int minElemEachCluster,
                                                                            const int maxNbIterations,
                                                                            const std::map<int, fullVector<double> >& mapPositionsWeights,
                                                                            std::map<int, std::map<int, const fullVector<double>* > >& clustersVec,
                                                                            std::map<int, std::map<int, const fullMatrix<double>* > >& clusters)
{
  if (nbClusters == 1)
  {
    clusters.clear();
    std::map<int, const fullMatrix<double>* >& cluster_i = clusters[0];
    clustersVec.clear();
    std::map<int, const fullVector<double>* >& clusterVec_i = clustersVec[0];
    for (std::map<int, fullMatrix<double> >::const_iterator it = data.begin();  it!= data.end(); it++)
    {
      cluster_i[it->first] = &(it->second);
    }
    for (std::map<int, fullVector<double> >::const_iterator it = dataVec.begin();  it!= dataVec.end(); it++)
    {
      clusterVec_i[it->first] = &(it->second);
    }
    //return true;
  }
 
  unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
  std::default_random_engine generator (seed);
  std::uniform_int_distribution<int> distribution(0, nbClusters-1);
  
  std::map<int,int> IdMapToClusterId;
  // initialization to nbClusters, 
  // empty cluster is avoided
  clusters.clear();
  clustersVec.clear();
  std::map<int, fullVector<double> >::const_iterator itAssVec = dataVec.begin();
  int type = itAssVec->first;
  std::map<int, fullMatrix<double> >::const_iterator itAss = data.find(type);

  for (int i=0; i< nbClusters; i++)
  {
    std::map<int, const fullMatrix<double>* >& cluster_i = clusters[i];
    std::map<int, const fullVector<double>* >& clusterVec_i = clustersVec[i];
    for (int j=0; j< std::max(1,minElemEachCluster); j++)
    {
      type = itAssVec->first;
      std::map<int, fullMatrix<double> >::const_iterator itAss = data.find(type);
      cluster_i[type] = &(itAss->second);
      clusterVec_i[type] = &(itAssVec->second);
      IdMapToClusterId[type] = i;
      itAssVec++;
    }
  }

  for (std::map<int, fullVector<double> >::const_iterator itVec = itAssVec;  itVec!= dataVec.end(); itVec++)
  {
    int cl  = distribution(generator);  
    std::map<int, const fullMatrix<double>* >& cluster_i = clusters[cl];
    std::map<int, const fullVector<double>* >& clusterVec_i = clustersVec[cl];
    type = itVec->first;
    clusterVec_i[type] = &(itVec->second);
    std::map<int, fullMatrix<double> >::const_iterator it = data.find(type);
    cluster_i[type] = &(it->second);
    IdMapToClusterId[type] = cl;
  }

  // print cluster
  //clutering_impls::printClusters(clusters);
  // initialized k-means
  std::map<int, fullVector<double> > Vec_clustermean;
  for (int i=0; i< nbClusters; i++)
  {
    fullVector<double>& Vecmean = Vec_clustermean[i];
    clutering_impls::meanClusterVecLong(clustersVec[i],mapPositionsWeights,Vecmean);
  }
    
  //clustering
  int iter=0;
  while (true)
  {
    iter++;
    if (iter> maxNbIterations)
    {
      Msg::Info("maximal iteration exceeds");
      return false;
      break;
    }
    int numberChange = 0;
    // at each point, find neareast cluster
    std::map<int,int> previousMapId=IdMapToClusterId;
    IdMapToClusterId.clear();
    
    for (std::map<int, fullVector<double> >::const_iterator itVec = dataVec.begin();  itVec!= dataVec.end(); itVec++)
    { 
      int type = itVec->first;
      const fullVector<double>& vec = itVec->second;
      std::map<int, fullMatrix<double> >::const_iterator it = data.find(type);
      const fullMatrix<double>& mat = it->second;
      int currentCluserId = previousMapId[type];
      int newCluserId = clutering_impls::getNearestClusterIdVecLong(Vec_clustermean,vec);
      // start as previous
      IdMapToClusterId[type] = previousMapId[type];
      if (currentCluserId != newCluserId)
      {
        // remove from current cluster and add to the new one
        std::map<int, const fullMatrix<double>* >& cluster_old = clusters[currentCluserId];
        std::map<int, const fullVector<double>* >& clusterVec_old = clustersVec[currentCluserId];
        if (cluster_old.size() > minElemEachCluster)
        {
          numberChange ++;
          IdMapToClusterId[type] = newCluserId;
          cluster_old.erase(type);
          clusterVec_old.erase(type);
          std::map<int, const fullMatrix<double>* >& cluster_new = clusters[newCluserId];
          std::map<int, const fullVector<double>* >& clusterVec_new = clustersVec[newCluserId];
          cluster_new[type] = &mat;
          clusterVec_new[type] = &vec;
        }
      }
    }
    
    Msg::Info("iter = %d, number moving = %d",iter,numberChange);
    
    if (numberChange ==0 and iter>1)
    {
      Msg::Info("Done clustering as clusters are not modified !!!");
      break;
    }
    else
    {
      // re compute centering
      for (int i=0; i< nbClusters; i++)
      {
        fullVector<double>& Vecmean = Vec_clustermean[i];
        clutering_impls::meanClusterVecLong(clustersVec[i],mapPositionsWeights,Vecmean);
      }
    }
  }
  
  std::map<int, std::map<int, const fullMatrix<double>* > > clusters_new;
  clusters_new.clear();
  std::map<int, std::map<int, const fullVector<double>* > > clustersVec_new;
  clustersVec_new.clear();
  
  //clutering_impls::printClusters(clusters);
  
  int nbClustersLevel2 = 4;
  if(nbClusters==1){nbClustersLevel2=1;}
  nbClusters *= nbClustersLevel2;
  
  int minElemEachClusterLevel1 = 1;
  
  for (std::map<int, std::map<int, const fullMatrix<double>* > >::const_iterator  itCl = clusters.begin(); itCl != clusters.end(); itCl++)
  {
    int clNum = itCl->first;
    
    const std::map<int, const fullMatrix<double>* >& dataInClPointer = itCl->second;
    const std::map<int, const fullVector<double>* >& dataInClVecPointer = clustersVec[itCl->first];
    
    std::map<int, fullMatrix<double> > dataInCl;
    std::map<int, fullVector<double> > dataInClVec;
    
    for (std::map<int, const fullMatrix<double>* >::const_iterator  itp = dataInClPointer.begin(); itp != dataInClPointer.end(); itp++)
    {
      fullMatrix<double>& mat = dataInCl[itp->first];
      mat = *(itp->second);
    }
    
    for (std::map<int, const fullVector<double>* >::const_iterator  itp = dataInClVecPointer.begin(); itp != dataInClVecPointer.end(); itp++)
    {
      fullVector<double>& vec = dataInClVec[itp->first];
      vec = *(itp->second);
    }
    
    std::map<int, std::map<int, const fullMatrix<double>*> > clusterInCl_pnt;
    std::map<int, std::map<int, const fullVector<double>*> > clusterInClVec_pnt;
    
    bool ok = false;
    ok= clutering_impls::kmean_cluster(dataInCl,nbClustersLevel2,minElemEachClusterLevel1,maxNbIterations,mapPositionsWeights,clusterInCl_pnt);
    
    for (std::map<int, std::map<int, const fullMatrix<double>*> >::const_iterator  itCl = clusterInCl_pnt.begin(); itCl != clusterInCl_pnt.end(); itCl++)
    {
      int clNumInCluster = itCl->first;
      std::map<int, const fullMatrix<double>*> clusterMat = itCl->second;
      std::map<int, const fullMatrix<double>* >& clusterMat_new = clusters_new[clNum*nbClustersLevel2+clNumInCluster];
      std::map<int, const fullVector<double>* >& clusterVec_new = clustersVec_new[clNum*nbClustersLevel2+clNumInCluster];
      
      for (std::map<int, const fullMatrix<double>*>::const_iterator  itgp = clusterMat.begin(); itgp != clusterMat.end(); itgp++)
      {
        std::map<int, fullMatrix<double> >::const_iterator itMat = data.find(itgp->first);
        const fullMatrix<double>& mat = itMat->second;
        clusterMat_new[itgp->first] = &mat;
        
        std::map<int, fullVector<double> >::const_iterator itVec = dataVec.find(itgp->first);
        const fullVector<double>& vec = itVec->second;
        clusterVec_new[itgp->first] = &vec;
      }   
    }
  }
  
  clusters.clear();
  clustersVec.clear();
  
  clusters = clusters_new;
  clustersVec = clustersVec_new;
  
  //clutering_impls::printClusters(clusters);
  return true;
}

bool clutering_impls::kmean_cluster_hierarchical_fullVectorBased(const std::map<int, fullVector<double> >& dataVec, 
                                                                 const std::map<int, fullMatrix<double> >& data, 
                                                                 int& nbClusters, 
                                                                 const int minElemEachCluster,
                                                                 const int maxNbIterations,
                                                                 const std::map<int, fullVector<double> >& mapPositionsWeights,
                                                                 std::map<int, std::map<int, const fullVector<double>* > >& clustersVec,
                                                                 std::map<int, std::map<int, const fullMatrix<double>* > >& clusters)
{
  if (nbClusters == 1)
  {
    clusters.clear();
    std::map<int, const fullMatrix<double>* >& cluster_i = clusters[0];
    clustersVec.clear();
    std::map<int, const fullVector<double>* >& clusterVec_i = clustersVec[0];
    for (std::map<int, fullMatrix<double> >::const_iterator it = data.begin();  it!= data.end(); it++)
    {
      cluster_i[it->first] = &(it->second);
    }
    for (std::map<int, fullVector<double> >::const_iterator it = dataVec.begin();  it!= dataVec.end(); it++)
    {
      clusterVec_i[it->first] = &(it->second);
    }
    //return true;
  }
 
  unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
  std::default_random_engine generator (seed);
  std::uniform_int_distribution<int> distribution(0, nbClusters-1);
  
  std::map<int,int> IdMapToClusterId;
  // initialization to nbClusters, 
  // empty cluster is avoided
  clusters.clear();
  clustersVec.clear();
  std::map<int, fullVector<double> >::const_iterator itAssVec = dataVec.begin();
  int type = itAssVec->first;
  std::map<int, fullMatrix<double> >::const_iterator itAss = data.find(type);

  for (int i=0; i< nbClusters; i++)
  {
    std::map<int, const fullMatrix<double>* >& cluster_i = clusters[i];
    std::map<int, const fullVector<double>* >& clusterVec_i = clustersVec[i];
    for (int j=0; j< std::max(1,minElemEachCluster); j++)
    {
      type = itAssVec->first;
      std::map<int, fullMatrix<double> >::const_iterator itAss = data.find(type);
      cluster_i[type] = &(itAss->second);
      clusterVec_i[type] = &(itAssVec->second);
      IdMapToClusterId[type] = i;
      itAssVec++;
    }
  }

  for (std::map<int, fullVector<double> >::const_iterator itVec = itAssVec;  itVec!= dataVec.end(); itVec++)
  {
    int cl  = distribution(generator);  
    std::map<int, const fullMatrix<double>* >& cluster_i = clusters[cl];
    std::map<int, const fullVector<double>* >& clusterVec_i = clustersVec[cl];
    type = itVec->first;
    clusterVec_i[type] = &(itVec->second);
    std::map<int, fullMatrix<double> >::const_iterator it = data.find(type);
    cluster_i[type] = &(it->second);
    IdMapToClusterId[type] = cl;
  }

  // print cluster
  //clutering_impls::printClusters(clusters);
  // initialized k-means
  std::map<int, fullVector<double> > Vec_clustermean;
  for (int i=0; i< nbClusters; i++)
  {
    fullVector<double>& Vecmean = Vec_clustermean[i];
    clutering_impls::meanClusterVecLong(clustersVec[i],mapPositionsWeights,Vecmean);
  }
    
  //clustering
  int iter=0;
  while (true)
  {
    iter++;
    if (iter> maxNbIterations)
    {
      Msg::Info("maximal iteration exceeds");
      return false;
      break;
    }
    int numberChange = 0;
    // at each point, find neareast cluster
    std::map<int,int> previousMapId=IdMapToClusterId;
    IdMapToClusterId.clear();
    
    for (std::map<int, fullVector<double> >::const_iterator itVec = dataVec.begin();  itVec!= dataVec.end(); itVec++)
    { 
      int type = itVec->first;
      const fullVector<double>& vec = itVec->second;
      std::map<int, fullMatrix<double> >::const_iterator it = data.find(type);
      const fullMatrix<double>& mat = it->second;
      int currentCluserId = previousMapId[type];
      int newCluserId = clutering_impls::getNearestClusterIdVecLong(Vec_clustermean,vec);
      // start as previous
      IdMapToClusterId[type] = previousMapId[type];
      if (currentCluserId != newCluserId)
      {
        // remove from current cluster and add to the new one
        std::map<int, const fullMatrix<double>* >& cluster_old = clusters[currentCluserId];
        std::map<int, const fullVector<double>* >& clusterVec_old = clustersVec[currentCluserId];
        if (cluster_old.size() > minElemEachCluster)
        {
          numberChange ++;
          IdMapToClusterId[type] = newCluserId;
          cluster_old.erase(type);
          clusterVec_old.erase(type);
          std::map<int, const fullMatrix<double>* >& cluster_new = clusters[newCluserId];
          std::map<int, const fullVector<double>* >& clusterVec_new = clustersVec[newCluserId];
          cluster_new[type] = &mat;
          clusterVec_new[type] = &vec;
        }
      }
    }
    
    Msg::Info("iter = %d, number moving = %d",iter,numberChange);
    
    if (numberChange ==0 and iter>1)
    {
      Msg::Info("Done clustering as clusters are not modified !!!");
      break;
    }
    else
    {
      // re compute centering
      for (int i=0; i< nbClusters; i++)
      {
        fullVector<double>& Vecmean = Vec_clustermean[i];
        clutering_impls::meanClusterVecLong(clustersVec[i],mapPositionsWeights,Vecmean);
      }
    }
  }
  
  std::map<int, std::map<int, const fullMatrix<double>* > > clusters_new;
  clusters_new.clear();
  std::map<int, std::map<int, const fullVector<double>* > > clustersVec_new;
  clustersVec_new.clear();
  
  //clutering_impls::printClusters(clusters);
  
  int nbClustersLevel1 = 4;
  if(nbClusters==1){nbClustersLevel1=1;}
  nbClusters *= nbClustersLevel1;
  
  int minElemEachClusterLevel1 = 1;
  
  for (std::map<int, std::map<int, const fullMatrix<double>* > >::const_iterator  itCl = clusters.begin(); itCl != clusters.end(); itCl++)
  {
    int clNum = itCl->first;
    
    const std::map<int, const fullMatrix<double>* >& dataInClPointer = itCl->second;
    const std::map<int, const fullVector<double>* >& dataInClVecPointer = clustersVec[itCl->first];
    
    std::map<int, fullMatrix<double> > dataInCl;
    std::map<int, fullVector<double> > dataInClVec;
    
    for (std::map<int, const fullMatrix<double>* >::const_iterator  itp = dataInClPointer.begin(); itp != dataInClPointer.end(); itp++)
    {
      fullMatrix<double>& mat = dataInCl[itp->first];
      mat = *(itp->second);
    }
    
    for (std::map<int, const fullVector<double>* >::const_iterator  itp = dataInClVecPointer.begin(); itp != dataInClVecPointer.end(); itp++)
    {
      fullVector<double>& vec = dataInClVec[itp->first];
      vec = *(itp->second);
    }
    
    std::map<int, std::map<int, const fullMatrix<double>*> > clusterInCl_pnt;
    std::map<int, std::map<int, const fullVector<double>*> > clusterInClVec_pnt;
    
    bool ok = false;
    ok= clutering_impls::kmean_cluster_FullVectorBased(dataInClVec,dataInCl,nbClustersLevel1,minElemEachClusterLevel1,maxNbIterations,mapPositionsWeights,clusterInClVec_pnt,clusterInCl_pnt);
    
    for (std::map<int, std::map<int, const fullMatrix<double>*> >::const_iterator  itCl = clusterInCl_pnt.begin(); itCl != clusterInCl_pnt.end(); itCl++)
    {
      int clNumInCluster = itCl->first;
      std::map<int, const fullMatrix<double>*> clusterMat = itCl->second;
      std::map<int, const fullMatrix<double>* >& clusterMat_new = clusters_new[clNum*nbClustersLevel1+clNumInCluster];
      std::map<int, const fullVector<double>* >& clusterVec_new = clustersVec_new[clNum*nbClustersLevel1+clNumInCluster];
      
      for (std::map<int, const fullMatrix<double>*>::const_iterator  itgp = clusterMat.begin(); itgp != clusterMat.end(); itgp++)
      {
        std::map<int, fullMatrix<double> >::const_iterator itMat = data.find(itgp->first);
        const fullMatrix<double>& mat = itMat->second;
        clusterMat_new[itgp->first] = &mat;
        
        std::map<int, fullVector<double> >::const_iterator itVec = dataVec.find(itgp->first);
        const fullVector<double>& vec = itVec->second;
        clusterVec_new[itgp->first] = &vec;
      }   
    }
  }
  
  clusters.clear();
  clustersVec.clear();
  
  clusters = clusters_new;
  clustersVec = clustersVec_new;
  
  //clutering_impls::printClusters(clusters);
  return true;
}

void clutering_impls::vecNormalizeByOverallValue(const std::map<int, fullVector<double> >& dataVec,
                                    const std::map<int, fullVector<double> >& mapPositionsWeights,
                                    std::map<int, fullVector<double> >& dataVecNormalized)
{
  std::map<int, fullVector<double> >::const_iterator dataVec0 = dataVec.find(0);
  int nb = (dataVec0->second).size();
  fullVector<double> mean(nb);
  double totalWeight = 0;
  int n = dataVec.size();
  if (n > 0)
  {
    for (std::map<int, fullVector<double> >::const_iterator its = dataVec.begin(); its != dataVec.end(); its ++)
    {
      std::map<int, fullVector<double> >::const_iterator itFind = mapPositionsWeights.find(its->first);
      if (itFind==mapPositionsWeights.end())
      {
        printf("wrong map of Gauss poing as GP %d is not found in this map",its->first);
      }
      else
      {
        const fullVector<double>& gpCorr = itFind->second;
        totalWeight += gpCorr(3);
        mean.axpy(its->second, gpCorr(3));
      }
    }
    mean.scale(1./totalWeight);
    
    for (std::map<int, fullVector<double> >::const_iterator its = dataVec.begin(); its != dataVec.end(); its ++)
    {  
      fullVector<double> vecLocnormalized(nb);
      const fullVector<double> &vecLoc = its->second;
      for(int i=0; i<nb; i++)
      {
        if(mean(i)!=0.){
          vecLocnormalized(i) = vecLoc(i)/mean(i);
        }
      }
      dataVecNormalized[its->first] = vecLocnormalized;
    }
  }
  else
  {
    Msg::Error("empty vector is found");
  }
}

void clutering_impls::vecNormalizeByOverallNorm(const std::map<int, fullVector<double> >& dataVec,
                                          const std::map<int, fullVector<double> >& mapPositionsWeights,
                                          std::map<int, fullVector<double> >& dataVecNormalized)
{
  //std::map<int, fullVector<double> >::const_iterator dataVec0 = dataVec.find(0);
  //int nb = (dataVec0->second).size();
  int nb = 36;
  fullVector<double> mean(nb);
  int nb_Norm = nb/6;
  fullVector<double> meanNorm(nb_Norm);
  double totalWeight = 0;
  int n = dataVec.size();
  if (n > 0)
  {
    for (std::map<int, fullVector<double> >::const_iterator its = dataVec.begin(); its != dataVec.end(); its ++)
    {
      std::map<int, fullVector<double> >::const_iterator itFind = mapPositionsWeights.find(its->first);
      if (itFind==mapPositionsWeights.end())
      {
        printf("wrong map of Gauss poing as GP %d is not found in this map",its->first);
      }
      else
      {
        const fullVector<double>& gpCorr = itFind->second;
        totalWeight += gpCorr(3);
        mean.axpy(its->second, gpCorr(3));
      }
    }
    mean.scale(1./totalWeight);
    for(int i=0; i<nb_Norm; i++)
    {
      //meanNorm(i) = sqrt(mean(i*6+0)*mean(i*6+0)+mean(i*6+1)*mean(i*6+1)+mean(i*6+2)*mean(i*6+2)+2.*mean(i*6+3)*mean(i*6+3)+2.*mean(i*6+4)*mean(i*6+4)+2.*mean(i*6+5)*mean(i*6+5));
      meanNorm(i) = sqrt(mean(i*6+0)*mean(i*6+0)+mean(i*6+1)*mean(i*6+1)+mean(i*6+2)*mean(i*6+2));
    }
    
    for (std::map<int, fullVector<double> >::const_iterator its = dataVec.begin(); its != dataVec.end(); its ++)
    {  
      fullVector<double> vecLocnormalized(nb);
      const fullVector<double> &vecLoc = its->second;
      for(int i=0; i<nb; i++)
      {
        if(mean(i)!=0.){
          if(i<6)
            {vecLocnormalized(i) = vecLoc(i)/meanNorm(0);}
          else if(i>=6 and i<12)
            {vecLocnormalized(i) = vecLoc(i)/meanNorm(1);}
          else if(i>=12 and i<18)
            {vecLocnormalized(i) = vecLoc(i)/meanNorm(2);}
          else if(i>=18 and i<24)
            {vecLocnormalized(i) = vecLoc(i)/meanNorm(3);}
          else if(i>=24 and i<30)
            {vecLocnormalized(i) = vecLoc(i)/meanNorm(4);}   
          else if(i>=30 and i<36)
            {vecLocnormalized(i) = vecLoc(i)/meanNorm(5);}      
        }
      }
      dataVecNormalized[its->first] = vecLocnormalized;
    }
  }
  else
  {
    Msg::Error("empty vector is found");
  }
}

void clutering_impls::vecNormalizeByLocalNorm(const std::map<int, fullVector<double> >& dataVec,
                                              std::map<int, fullVector<double> >& dataVecNormalized)
{
  int nb = 36;
  int nb_Norm = nb/6;
  int n = dataVec.size();
  if (n > 0)
  {   
    for (std::map<int, fullVector<double> >::const_iterator its = dataVec.begin(); its != dataVec.end(); its ++)
    {
      fullVector<double> vecLocnormalized(nb);
      const fullVector<double> &vecLoc = its->second;
      for(int i=0; i<nb_Norm; i++)
      { 
        double normLoc = 0.;
        normLoc = sqrt(vecLoc(6*i+0)*vecLoc(6*i+0)+vecLoc(6*i+1)*vecLoc(6*i+1)+vecLoc(6*i+2)*vecLoc(6*i+2));
        if(normLoc!=0.){
          vecLocnormalized(6*i+0) = vecLoc(6*i+0)/normLoc;
          vecLocnormalized(6*i+1) = vecLoc(6*i+1)/normLoc;
          vecLocnormalized(6*i+2) = vecLoc(6*i+2)/normLoc;
          vecLocnormalized(6*i+3) = vecLoc(6*i+3)/normLoc;
          vecLocnormalized(6*i+4) = vecLoc(6*i+4)/normLoc;
          vecLocnormalized(6*i+5) = vecLoc(6*i+5)/normLoc;
        }
      }
      dataVecNormalized[its->first] = vecLocnormalized;
    }
  }
  else
  {
    Msg::Error("empty vector is found");
  }
}

void clutering_impls::vecSeparate(const std::map<int, fullVector<double> >& dataVec,
                                              std::map<int, fullVector<double> >& dataVecSeparated)
{
  int nb = 36;
  int nb_Norm = nb/6;
  int n = dataVec.size();
  if (n > 0)
  {   
    for (std::map<int, fullVector<double> >::const_iterator its = dataVec.begin(); its != dataVec.end(); its ++)
    {
      fullVector<double> vecLocSeparated(36);
      const fullVector<double> &vecLoc = its->second;
      for(int i=0; i<nb_Norm; i++)
      { 
        double normLoc = 0.;
        normLoc = sqrt(vecLoc(6*i+0)*vecLoc(6*i+0)+vecLoc(6*i+1)*vecLoc(6*i+1)+vecLoc(6*i+2)*vecLoc(6*i+2));
        if(normLoc!=0.){
          vecLocSeparated(6*i+0) = vecLoc(6*i+0)/normLoc;
          vecLocSeparated(6*i+1) = vecLoc(6*i+1)/normLoc;
          vecLocSeparated(6*i+2) = vecLoc(6*i+2)/normLoc;
          vecLocSeparated(6*i+3) = vecLoc(6*i+3)/normLoc;
          vecLocSeparated(6*i+4) = vecLoc(6*i+4)/normLoc;
          vecLocSeparated(6*i+5) = vecLoc(6*i+5)/normLoc;
        }
        vecLocSeparated(6*i+5) = 6.*normLoc;
      }
      dataVecSeparated[its->first] = vecLocSeparated;
    }
  }
  else
  {
    Msg::Error("empty vector is found");
  }
}

void clutering_impls::vecRatio(const std::map<int, fullVector<double> >& dataVecNumerator,
                               const std::map<int, fullVector<double> >& dataVecDenominator,
                               std::map<int, fullVector<double> >& dataVecRatio)
{
  int nb = 6;
  double totalWeight = 0;
  int n = dataVecNumerator.size();
  if (n > 0)
  {
    for (std::map<int, fullVector<double> >::const_iterator its = dataVecNumerator.begin(); its != dataVecNumerator.end(); its ++)
    {
      std::map<int, fullVector<double> >::const_iterator itFind = dataVecDenominator.find(its->first);
      if (itFind==dataVecDenominator.end())
      {
        printf("wrong map of Gauss points as GP %d is not found in this map",its->first);
      }
      else
      {
        fullVector<double> dataVecRatioLoc(nb);
        for(int i=0; i<nb; i++)
        {
          const fullVector<double>& numeratorLoc = its->second;
          const fullVector<double>& denominatorLoc = itFind->second;
          dataVecRatioLoc(i) = numeratorLoc(i)/denominatorLoc(i);
        }
        dataVecRatio[its->first] = dataVecRatioLoc;
      }
    }
  }
  else
  {
    Msg::Error("empty vector is found");
  }
}


