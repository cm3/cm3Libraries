
//
// C++ Interface: reduction interface
//
// Author:   <Kevin Spilker>, (C) 2019, 
// Copyright: See COPYING file that comes with this distribution
//
//

#include "modelReduction.h"
#include "ipTFA.h"
#include "TFATools.h"
#include "nonLinearSystemPETSc.h"
#include "OS.h"
#include <iostream>
using namespace std;

#if !defined(F77NAME)
#define F77NAME(x) (x##_)
#endif

#if defined(HAVE_LAPACK)
extern "C" {
void F77NAME(dgesv)(int *N, int *nrhs, double *A, int *lda, int *ipiv,
                    double *b, int *ldb, int *info);
};
#endif //HAVE_LAPACK
bool ReductionTFA::luSolve(fullMatrix<double>& A, const fullMatrix<double> &rhs, fullMatrix<double> &result)
{
  #if defined(HAVE_LAPACK)
  int N = A.size1(), nrhs = rhs.size2(), lda = N, ldb = N, info;
  int *ipiv = new int[N];
  for(int i = 0; i < N; i++) 
    for (int j=0; j< nrhs; j++)
      result(i,j) = rhs(i,j);
  F77NAME(dgesv)(&N, &nrhs, A.getDataPtr(), &lda, ipiv, result.getDataPtr(), &ldb, &info);
  delete[] ipiv;
  if(info == 0) return true;
  return false;
  #else
    Msg::Error(" LU solve multiple rhs requires LAPACK");
    return false;
  #endif //HAVE_LAPACK
};
  

const materialLaw* ReductionTFA::getMaterialLaw(int cluster) const
{
  std::map<int, const materialLaw*>::const_iterator itMat =  _matlawptr.find(cluster);
  if (itMat == _matlawptr.end())
  {
    Msg::Error("material is not found for cluster %d", cluster);
    Msg::Exit(0);
  }
  else
  {
    return itMat->second;
  }
  return NULL;
};

void ReductionTFA::getAllClusterVolumes(std::vector<double>&allVolumes) const {
  std::vector<int> allCl;
  _clusteringData->getAllClusterIndices(allCl);
  const std::map<int,double>& volumeMap = _clusteringData->getClusterVolumeMap();
  allVolumes.resize(allCl.size());
  for (int ii=0; ii < allCl.size(); ii++)
  {
    allVolumes[ii] = (volumeMap.find(allCl[ii]))->second;
  }
}

void ReductionTFA::addClusterLawToMap(int clusterNb, materialLaw* clusterLaw)
{
  _matlawptr[clusterNb] = clusterLaw;
}


void ReductionTFA::initializeClusterMaterialMap(const std::map<int,materialLaw*> &maplaw)
{
  const std::map<int, int>& _matlawnb = _clusteringData->getClusterMaterialMap();
  for(std::map<int,int>::const_iterator itcluster=_matlawnb.begin(); itcluster!=_matlawnb.end(); itcluster++)
  {
    int clusternb=itcluster->first;
    int matlawnb=itcluster->second;

    bool findlaw=false;
    for(std::map<int,materialLaw*>::const_iterator it = maplaw.begin(); it != maplaw.end(); ++it)
    {
      int num = it->first;
      if(num == matlawnb){
        findlaw=true;
        it->second->initLaws(maplaw);
        _matlawptr.insert(std::pair<int, const materialLaw*>(clusternb,it->second->getConstNonLinearSolverMaterialLaw()));
        break;
      }
    }
    if(!findlaw) Msg::Error("The law is not initialize for cluster number %d and material law number %d",clusternb,matlawnb);
  }
}

ReductionTFA::ReductionTFA(int solver, int d, int correction, bool withFrame, bool polarization, int tag) : _solverType(solver), _tag(tag), _ownClusteringData(false), _clusteringData(NULL), 
_dim(d), _sys(NULL), _isInitialized(false), _correction(correction), _withFrame(withFrame), _polarization(polarization), _clusterIncOri(false) {}
ReductionTFA::~ReductionTFA()
{
  if (_ownClusteringData)
  {
    _ownClusteringData = false;
    if (_clusteringData != NULL)
    {
      delete _clusteringData; _clusteringData = NULL;
    }
  }
  if (_sys != NULL)
  {
    delete _sys;
    _sys = NULL;
  }
  _correction = false;
  _withFrame = false;
  _polarization = false;
  _clusterIncOri = false;
}

void ReductionTFA::setDim(int d)
{
  _dim = d;
  
}
int ReductionTFA::getDim() const
{
  return _dim;
}

void ReductionTFA::loadClusterSummaryAndInteractionTensorsFromFiles(const std::string clusterSummaryFileName, 
                                        const std::string interactionTensorsFileName)
{
  if (_ownClusteringData)
  {
    if (_clusteringData != NULL) delete _clusteringData;
  }
  _ownClusteringData = true;
  _clusteringData = new Clustering();
  _clusteringData->loadClusterSummaryFromFile(clusterSummaryFileName);
  _clusteringData->loadInteractionTensorsFromFile(interactionTensorsFileName);
};

void ReductionTFA::loadClusterSummaryAndInteractionTensorsHomogenizedFrameFromFiles(const std::string clusterSummaryFileName, 
                                        const std::string interactionTensorsFileName)
{
  if (_ownClusteringData)
  {
    if (_clusteringData != NULL) delete _clusteringData;
  }
  _ownClusteringData = true;
  _clusteringData = new Clustering();
  _clusteringData->loadClusterSummaryHomogenizedFrameFromFile(clusterSummaryFileName);
  _clusteringData->loadInteractionTensorsHomogenizedFrameELFromFile(interactionTensorsFileName);
};

void ReductionTFA::loadClusterStandardDeviationFromFile(const std::string ClusterStrainConcentrationSTD)
{
  _clusteringData->loadClusterStandDevFromFile(ClusterStrainConcentrationSTD);
};

void ReductionTFA::loadEshelbyInteractionTensorsFromFile(const std::string EshelbyInteractionTensorsFileName)
{
  _clusteringData->loadEshelbyInteractionTensorsFromFile(EshelbyInteractionTensorsFileName);
};

void ReductionTFA::loadPlasticEqStrainConcentrationFromFile(const std::string PlasticEqStrainConcentrationFileName,
                                                            const std::string PlasticEqStrainConcentrationGeometricFileName,
                                                            const std::string PlasticEqStrainConcentrationHarmonicFileName)
{
  _clusteringData->loadClusterPlasticEqSummaryFromFile(PlasticEqStrainConcentrationFileName);
  _clusteringData->loadClusterPlasticEqSummaryGeometricFromFile(PlasticEqStrainConcentrationGeometricFileName);
  _clusteringData->loadClusterPlasticEqSummaryHarmonicFromFile(PlasticEqStrainConcentrationHarmonicFileName);
};



void ReductionTFA::loadInteractionTensorsMatrixFrameELFromFile(const std::string InteractionTensorsMatrixFrameELFileName)
{
  _clusteringData->loadInteractionTensorsMatrixFrameELFromFile(InteractionTensorsMatrixFrameELFileName);
};

void ReductionTFA::loadInteractionTensorsHomogenizedFrameELFromFile(const std::string InteractionTensorsHomogenizedFrameELFileName)
{
  _clusteringData->loadInteractionTensorsHomogenizedFrameELFromFile(InteractionTensorsHomogenizedFrameELFileName);
};



void ReductionTFA::loadElasticStrainConcentrationDerivativeFromFile(const std::string ElasticStrainConcentrationDerivative)
{
  _clusteringData->loadElasticStrainConcentrationDerivativeFromFile(ElasticStrainConcentrationDerivative);
};


void ReductionTFA::loadReferenceStiffnessFromFile(const std::string ReferenceStiffnessFileName)
{
  _clusteringData->loadReferenceStiffnessFromFile(ReferenceStiffnessFileName);
};

void ReductionTFA::loadReferenceStiffnessIsoFromFile(const std::string ReferenceStiffnessIsoFileName)
{
  _clusteringData->loadReferenceStiffnessIsoFromFile(ReferenceStiffnessIsoFileName);
};

void ReductionTFA::loadInteractionTensorIsoFromFile(const std::string InteractionTensorIsoFileName)
{
  _clusteringData->loadInteractionTensorIsoFromFile(InteractionTensorIsoFileName);
};

void ReductionTFA::loadClusterFiberOrientationFromFile(const std::string OrientationDataFileName)
{
  _clusteringData->loadClusterFiberOrientationFromFile(OrientationDataFileName);
};

const STensor43& ReductionTFA::getClusterAverageStrainConcentrationTensor(int cl) const
{
  std::map<int,STensor43>::const_iterator Afind= _clusteringData->getClusterAverageStrainConcentrationTensorMap().find(cl);
  #ifdef _DEBUG
  if (Afind == _clusteringData->getClusterAverageStrainConcentrationTensorMap().end())
  {
    Msg::Error("cluster %d does not exist!");
    Msg::Exit(0);
  }
  #endif //_
  return Afind->second;
};
double ReductionTFA::getClusterNonUniformCorrection(int cl, const STensor3& macroeps0, const STensor3& macroeps) const
{
  std::map<int,fullVector<double>>::const_iterator plasticEqMeanMap_find = _clusteringData->getClusterAveragePlasticEqStrainConcentrationVectorMap().find(cl);
  std::map<int,fullVector<double>>::const_iterator plasticEqHarmonicMeanMap_find = _clusteringData->getClusterAverageHarmonicPlasticEqStrainConcentrationVectorMap().find(cl); 
  //std::map<int,fullVector<double>>::const_iterator plasticEqPowerMeanMap_find = _clusteringData->getClusterAveragePowerPlasticEqStrainConcentrationVectorMap().find(cl); 
  #ifdef _DEBUG
  if (plasticEqMeanMap_find == _clusteringData->getClusterAveragePlasticEqStrainConcentrationVectorMap().end())
  {
    Msg::Error("cluster %d does not exist!");
    Msg::Exit(0);
  }
  #endif //_
  const fullVector<double> p_offline_mean_loc_vector = (plasticEqMeanMap_find)->second;
  const fullVector<double> p_offline_harmean_loc_vector = (plasticEqHarmonicMeanMap_find)->second;
  //const fullVector<double> p_offline_powermean_loc_vector = (plasticEqPowerMeanMap_find)->second;
  fullVector<double> clusterCorrections(2); 
  for(int j=0; j < 2; j++)
  {
    if(p_offline_mean_loc_vector(j)>=1. and p_offline_harmean_loc_vector(j)>0.5){
    clusterCorrections(j) = sqrt(p_offline_mean_loc_vector(j)/p_offline_harmean_loc_vector(j));
    }
    else{clusterCorrections(j) = 1.;}
    //clusterCorrections(j) = p_offline_powermean_loc_vector(j)/p_offline_mean_loc_vector(j);
  }
  STensor3 macrodeps = macroeps;
  macrodeps -= macroeps0; 
  int macrodeps_mainDir = 0;
  double macrodeps_mainDir_magnitude = 0.;
  fullVector<double> macrodeps_vec(6);
  STensorOperation::fromSTensor3ToFullVector(macrodeps,macrodeps_vec);
  for(int i=0; i<6; i++)
  {
    if(abs(macrodeps_vec(i)) > macrodeps_mainDir_magnitude)
    {
      macrodeps_mainDir = i;
      macrodeps_mainDir_magnitude = macrodeps_vec(i);
    }
  }
  double corr=1.;
  if(macrodeps_mainDir==0 or macrodeps_mainDir==1 or macrodeps_mainDir==2){
    corr=clusterCorrections(0);
  }
  else if(macrodeps_mainDir==3 or macrodeps_mainDir==4 or macrodeps_mainDir==5){
    corr=clusterCorrections(1);
  }
  //printf("CORR %d \n", macrodeps_mainDir);
  return corr;
};

const STensor43& ReductionTFA::getClusterInteractionTensor(int icl, int jcl) const
{
  std::map<std::pair<int,int>,STensor43>::const_iterator Dfind;
  if(_withFrame)
  {
  Dfind = _clusteringData->getClusterInteractionTensorHomogenizedFrameELMap().find(std::pair<int,int>(icl,jcl));
  #ifdef _DEBUG
  if (Dfind == _clusteringData->getClusterInteractionTensorHomogenizedFrameELMap().end())
  {
    Msg::Error("cluster %d does not exist!");
    Msg::Exit(0);
  }
  #endif //_  
  }
  else
  {
  Dfind = _clusteringData->getClusterInteractionTensorMap().find(std::pair<int,int>(icl,jcl));
  #ifdef _DEBUG
  if (Dfind == _clusteringData->getClusterInteractionTensorMap().end())
  {
    Msg::Error("cluster %d does not exist!");
    Msg::Exit(0);
  }
  #endif //_
  }
  return Dfind->second;
};
const materialLaw* ReductionTFA::getLawForCluster(int cl) const
{
  std::map<int,const materialLaw *>::const_iterator itmlaw=_matlawptr.find(cl);
  #ifdef _DEBUG
  if (itmlaw == _matlawptr.end())
  {
    Msg::Error("law for cluster %d cannot be found",cl);
    Msg::Exit(0);
  }
  #endif
  return itmlaw->second;
};

const STensor43& ReductionTFA::getClusterAverageStrainConcentrationTensorHierarchical(std::vector<int> clIDsHierarchical) const
{
  int level = (clIDsHierarchical.size())-1;
  
  if(level == 0)
  {
    int clLv0 = clIDsHierarchical[0];
    std::map<int,STensor43>::const_iterator Afind= _clusteringData->getClusterAverageStrainConcentrationTensorMap().find(clLv0);
    #ifdef _DEBUG
    if (Afind == _clusteringData->getClusterAverageStrainConcentrationTensorMap().end())
    {
      Msg::Error("cluster %d does not exist!",clLv0);
      Msg::Exit(0);
    }
    #endif //_
    return Afind->second;
  }
  else if(level == 1)
  {
    int clLv0 = clIDsHierarchical[0];
    int clLv1 = clIDsHierarchical[1];
    std::map<int,std::map<int,STensor43>>::const_iterator AfindLv0= _clusteringData->getClusterAverageStrainConcentrationTensorMapLevel1().find(clLv0);
    #ifdef _DEBUG
    if (AfindLv0 == _clusteringData->getClusterAverageStrainConcentrationTensorMapLevel1().end())
    {
      Msg::Error("cluster level 0: %d does not exist!",clLv0);
      Msg::Exit(0);
    }
    #endif //_
    const std::map<int,STensor43> AmapLv1 = AfindLv0->second;
    std::map<int,STensor43>::const_iterator AfindLv1= AmapLv1.find(clLv1);
    #ifdef _DEBUG
    if (AfindLv1 == AmapLv1.end())
    {
      Msg::Error("cluster level 0, level 1: %d, %d does not exist!",clLv0,clLv1);
      Msg::Exit(0);
    }
    #endif //_
    return AfindLv1->second;
  }
};



const STensor43& ReductionTFA::getClusterInteractionTensorHierarchical(std::vector<int> clHostIDs, int icl, int jcl) const
{
  int nbUpperLevels = clHostIDs.size();
  if(nbUpperLevels == 0)
  {
    std::map<std::pair<int,int>,STensor43>::const_iterator Dfind = _clusteringData->getClusterInteractionTensorMap().find(std::pair<int,int>(icl,jcl));
    #ifdef _DEBUG
    if (Dfind == _clusteringData->getClusterInteractionTensorMap().end())
    {
      Msg::Error("cluster %d does not exist!");
      Msg::Exit(0);
    }
    #endif //_
    return Dfind->second; 
  }
  else if(nbUpperLevels == 1)
  {
    int clLv0 = clHostIDs[0];
    std::map<int,std::map<std::pair<int,int>,STensor43>>::const_iterator DfindLv0 = _clusteringData->getClusterInteractionTensorMapLevel1().find(clLv0);
    #ifdef _DEBUG
    if (DfindLv0 == _clusteringData->getClusterInteractionTensorMapLevel1().end())
    {
      Msg::Error("interaction map cluster level 0: %d does not exist!",clLv0);
      Msg::Exit(0);
    }
    #endif //_
    const std::map<std::pair<int,int>,STensor43> DmapLv1 = DfindLv0->second;
    std::map<std::pair<int,int>,STensor43>::const_iterator DfindLv1 = DmapLv1.find(std::pair<int,int>(icl,jcl));
    #ifdef _DEBUG
    if (DfindLv1 == DmapLv1.end())
    {
      Msg::Error("interaction tensor in cluster level 0, clusters: %d,%d does not exist!",clLv0,icl,jcl);
      Msg::Exit(0);
    }
    #endif //_
    return DfindLv1->second;
  } 
};


const STensor43& ReductionTFA::getClusterStrainConcentrationStDTensor(int cl) const
{
  std::map<int,STensor43>::const_iterator Astdfind= _clusteringData->getClusterSTDTensorMap().find(cl);
  #ifdef _DEBUG
  if (Astdfind == _clusteringData->getClusterSTDTensorMap().end())
  {
    Msg::Error("cluster %d does not exist!");
    Msg::Exit(0);
  }
  #endif //_
  return Astdfind->second;
};
const double& ReductionTFA::getClusterStrainConcentrationStDScalar(int cl) const
{
  std::map<int,double>::const_iterator astdfind= _clusteringData->getClusterSTDMap().find(cl);
  #ifdef _DEBUG
  if (astdfind == _clusteringData->getClusterSTDMap().end())
  {
    Msg::Error("cluster %d does not exist!");
    Msg::Exit(0);
  }
  #endif //_
  return astdfind->second;
};


const STensor43& ReductionTFA::getClusterInteractionTensorIso(int icl, int jcl) const
{
  std::map<std::pair<int,int>,STensor43>::const_iterator Dfind;

  Dfind = _clusteringData->getClusterInteractionTensorIsoMap().find(std::pair<int,int>(icl,jcl));
  #ifdef _DEBUG
  if (Dfind == _clusteringData->getClusterInteractionTensorIsoMap().end())
  {
    Msg::Error("cluster %d does not exist!");
    Msg::Exit(0);
  }
  #endif //_
  return Dfind->second;
};


const fullVector<double>& ReductionTFA::getClusterMeanFiberOrientation(int cl) const
{
  std::map<int,fullVector<double>>::const_iterator ori;

  ori = _clusteringData->getClusterMeanFiberOrientation().find(cl);
  #ifdef _DEBUG
  if (ori == _clusteringData->getClusterMeanFiberOrientation().end())
  {
    Msg::Error("cluster %d does not exist!");
    Msg::Exit(0);
  }
  #endif //_
  return ori->second;
};

void ReductionTFA::evaluate(
            const STensor3& F0,         // initial deformation gradient (input @ time n)
            const STensor3& Fn,         // updated deformation gradient (input @ time n+1)
            STensor3 &P,                // updated 1st Piola-Kirchhoff stress tensor (output)
            const ClusteringData *q0,       // array of initial internal variable
            ClusteringData *q1,             // updated array of internal variable (in ipvcur on output),
            STensor43 &Tangent,         // constitutive tangents (output)
            const bool stiff,          // if true compute the tangents
            STensor43* elasticTangent)
{
  int maxIter = 500;
  double tol = 1e-6;
  double absTol = 1e-12;
  bool messageActive=false;
                  
  bool ok =  systemSolve(F0, Fn, q0, q1, P, stiff, Tangent, 
                maxIter, tol, absTol, messageActive);
                
  if (!ok)
  {
    // to force sub-stepping at macroscopic solver
    P(0,0) = P(1,1) = P(2,2) = sqrt(-1);
  };
};

void ReductionTFA::initSolve(const ClusteringData *q0,  const ClusteringData *q1, nonLinearSystemPETSc<double>* &NLsystem)
{
  _isInitialized= true;
  //printf("initializing \n");
  //
  fixDofs();
  numberDof();
  //allocate system
  int numR = _unknown.size();
  //
  createSystem(numR,NLsystem);
  preallocateSystem(q0,q1,NLsystem);
  //printf("done initializing, size of unknowns = %d \n",numR);
};

void ReductionTFA::numberDof()
{
  _unknown.clear();
  int nbClusters = _clusteringData->getTotalNumberOfClusters();
  if(_withFrame)
  {
    nbClusters += 1;
  }
  Msg::Info("dof numbering, number of clusters = %d",nbClusters);
  for (int icl =0; icl < nbClusters; icl ++)
  {
    std::vector<Dof> R;
    getKeys(icl,R);
    for (int j=0; j< R.size(); j++)
    {
      Dof& key = R[j];
      if (_fixedDof.find(key) == _fixedDof.end())
      {
        if(_unknown.find(key) == _unknown.end())
        {
          std::size_t size = _unknown.size();
          _unknown[key] = size;
        }
      }
    }
  }
  Msg::Info("done dof numbering, fixedDof = %d number of DOFS = %d",_fixedDof.size(),_unknown.size());
};

void ReductionTFA::setInitialState(const STensor3& F0, const STensor3& Fn)
{
  int nbClusters = _clusteringData->getTotalNumberOfClusters();
  if(_withFrame)
  {
    nbClusters += 1;
  }
  for (int icl = 0; icl < nbClusters; icl++)
  {
    std::vector<Dof> R;
    getKeys(icl,R);
    int Rsize = R.size();
    fullVector<double> val(Rsize);
    getInitialDofValue(icl,F0,Fn,val);
    for (int j=0; j< Rsize; j++)
    {
      std::map<Dof,int>::iterator itR =  _unknown.find(R[j]);
      if (itR != _unknown.end())
      {
        int row = itR->second;
        addToSolution(row,val(j),_sys);
      }
    }
  }
};

void ReductionTFA::createSystem(int numR, nonLinearSystemPETSc<double>* &NLsystem)
{
  if (getSolverType() == PETSC)
  {
    #if defined(HAVE_PETSC)
    //if (_sys != NULL)
    //{
      //delete _sys;
    //}
    //_sys = new nonLinearSystemPETSc<double>();
    //_sys->allocate(numR);
    if (NLsystem != NULL)
    {
      delete NLsystem;
    }
    NLsystem = new nonLinearSystemPETSc<double>();
    NLsystem->allocate(numR);
    //printf("PETSc is used\n");
    #else
    Msg::Error("PETSC is not installed");
    Msg::Exit(0);
    #endif
  }
  else if (getSolverType() == FULLMATRIX)
  {
    _K.resize(numR,numR,true);
    _res.resize(numR,true);
    _x.resize(numR,true); 
    _solCur.resize(numR,true); 
    printf("fullMatrix is used\n");    
  }
  else
  {
    Msg::Error("method %d does not exist",getSolverType());
    Msg::Exit(0);
  }
};

void ReductionTFA::preallocateSystem(const ClusteringData *q0,  const ClusteringData *q1, nonLinearSystemPETSc<double>* &NLsystem)
{
  // spasiry 
  if (NLsystem != NULL)
  {
    int nbClusters = _clusteringData->getTotalNumberOfClusters();
    if(_withFrame)
    {
      nbClusters += 1;
    }
    for (int icl = 0; icl < nbClusters; icl++)
    {
      static fullMatrix<double> m;
      std::vector<int> othersId;
      if(_withFrame)
      {
        getBiLinearTermWithFrame(icl,q0,q1, othersId, m);
      }
      else if(_polarization)
      {
        getBiLinearTermHS(icl,q0,q1, othersId, m);
      }
      else
      {
        getBiLinearTerm(icl,q0,q1, othersId, m);
      }
      //printf("Sizes %d x %d \n",m.size1(),m.size2());
      std::vector<Dof> R, C;
      getKeys(icl,R);
      for (int jcl=0; jcl< othersId.size(); jcl++)
      {
        getKeys(othersId[jcl],C);
      }
      //
      for (int iR=0; iR < R.size(); iR++)
      {
        std::map<Dof,int>::iterator itR =  _unknown.find(R[iR]);
        if (itR != _unknown.end())
        {
          int row = itR->second;
          for (int iC =0; iC < C.size(); iC++)
          {
            std::map<Dof,int>::iterator itC =  _unknown.find(C[iC]);
            if (itC != _unknown.end())
            {
              int col = itC->second;
              NLsystem->insertInSparsityPattern(row,col);
            };
          };
        };
      };
    };
  }
};

void ReductionTFA::initialZeroSolution(nonLinearSystemPETSc<double>* &NLsystem)
{
  if (NLsystem != NULL)
  {
    NLsystem->zeroSolution();
    NLsystem->copy(IPStateBase::current,IPStateBase::previous);
    NLsystem->copy(IPStateBase::current,IPStateBase::initial);
  }
  else
  {
    _solCur.setAll(0.);
  }
}

void ReductionTFA::addToSolution(int row, double val, nonLinearSystemPETSc<double>* &NLsystem)
{
  if (NLsystem!=NULL)
  {
    NLsystem->addToSolution(row,val);
  }
  else
  {
    _solCur(row) += val;
  }
}

void ReductionTFA::addToMatrix(int row, int col, double val, nonLinearSystemPETSc<double>* &NLsystem)
{
  if (NLsystem != NULL)
  {
    NLsystem->addToMatrix(row,col,val,nonLinearSystemBase::stiff);
  }
  else
  {
    _K(row,col) += val;
  }
}
 
int ReductionTFA::systemSolve(nonLinearSystemPETSc<double>* &NLsystem)
{
  
  if (NLsystem!= NULL)
  {
    int occ = NLsystem->systemSolve();
    return occ;
  }
  else
  {
    fullMatrix<double> Kcopy(_K);
    bool isSolved = Kcopy.luSolve(_res,_x);
    if (!isSolved)
    {
      printf("luSolve is not successful !!!\n");
      return 0;
    }
    else
    {
      _solCur.axpy(_x,-1.);
      return 1;
    }
  }
  
};
void ReductionTFA::zeroRHS(nonLinearSystemPETSc<double>* &NLsystem)
{
  if (NLsystem != NULL)
  {
    NLsystem->zeroRightHandSide();
  }
  else
  {
    _res.setAll(0.);
  }
}
void ReductionTFA::zeroMatrix(nonLinearSystemPETSc<double>* &NLsystem)
{
  if (NLsystem != NULL)
  {
    NLsystem->zeroMatrix(nonLinearSystemBase::stiff);
  }
  else
  {
    _K.setAll(0.);
  }
};

void ReductionTFA::addToRHS(int row, double val, nonLinearSystemPETSc<double>* &NLsystem)
{
  if (NLsystem != NULL)
  {
    NLsystem->addToRightHandSideMinus(row,val);
  }
  else
  {
    _res(row) += val;
  }
}

void ReductionTFA::allocateMultipleRHS(int dim, nonLinearSystemPETSc<double>* &NLsystem)
{
  if (NLsystem != NULL)
  {
    NLsystem->allocateMultipleRHS(dim);
  }
  else
  {
    int numRow = _x.size();
    _multipleRHS.resize(numRow,dim,true);
    _multipleSolution.resize(numRow,dim,true);
  }
};
void ReductionTFA::zeroMultipleRHS(nonLinearSystemPETSc<double>* &NLsystem)
{
  if (NLsystem != NULL)
  {
    NLsystem->zeroMultipleRHS();
  }
  else
  {
     _multipleRHS.setAll(0.);
     _multipleSolution.setAll(0.);    
  }
};

double ReductionTFA::getFromSolution(int row, nonLinearSystemPETSc<double>* &NLsystem) const
{
  if (NLsystem != NULL)
  {
    double val;
    NLsystem->getFromSolution(row,val);
    return val;
  }
  else
  {
    return _solCur(row);
  }
};

double ReductionTFA::normInfRightHandSide(nonLinearSystemPETSc<double>* &NLsystem) const
{
  if (NLsystem != NULL)
  {
    return NLsystem->normInfRightHandSide();
  }
  else
  {
    return _res.norm();
  }
};

void ReductionTFA::addToMultipleRHSMatrix(int row,int col,double val, nonLinearSystemPETSc<double>* &NLsystem)
{
  if (NLsystem != NULL)
  {
    NLsystem->addToMultipleRHSMatrix(row,col,val);
  }
  else
  {
    _multipleRHS(row,col) += val;
  }
}
bool ReductionTFA::systemSolveMultipleRHS(nonLinearSystemPETSc<double>* &NLsystem)
{
  if (NLsystem != NULL)
  {
    return NLsystem->systemSolveMultipleRHS();
  }
  else
  {
    fullMatrix<double> Kcopy(_K);
    bool ok = luSolve(Kcopy, _multipleRHS,_multipleSolution);
    if (!ok) 
    {
      printf("luSolve multiple rhs is not successful!!!\n");
      return false;
    }    
    return true;
  }
};

double ReductionTFA::getFromMultipleRHSSolution(int Nrow,int col, nonLinearSystemPETSc<double>* &NLsystem) const
{ 
  if (NLsystem != NULL)
  {
    double val;
    NLsystem->getFromMultipleRHSSolution(Nrow,col,val);
    return val;
  }
  else
  {
    return _multipleSolution(Nrow,col);
  }
}

void ReductionTFA::assembleRes(const ClusteringData *q0,  const ClusteringData *q1)
{
  zeroRHS(_sys);
  int nbClusters = q0->clustersNb;
  static fullVector<double> r;
  if(_withFrame)
  {
    for (int icl = 0; icl < (nbClusters+1); icl++)
    {
      getLinearTermWithFrame(icl,q0,q1,r);
      std::vector<Dof> R;
      getKeys(icl,R);
      for (int j=0; j<R.size(); j++)
      {
        std::map<Dof,int>::iterator it =  _unknown.find(R[j]);
        if (it != _unknown.end())
        {
          int row = it->second;
          addToRHS(row,r(j),_sys);
        }
      }
    } 
  }
  else if(_polarization)
  {
    for (int icl = 0; icl < nbClusters; icl++)
    {
      getLinearTermHS(icl,q0,q1,r);
      std::vector<Dof> R;
      getKeys(icl,R);
      for (int j=0; j<R.size(); j++)
      {
        std::map<Dof,int>::iterator it =  _unknown.find(R[j]);
        if (it != _unknown.end())
        {
          int row = it->second;
          addToRHS(row,r(j),_sys);
        }
      }
    }
  }
  else
  {
    for (int icl = 0; icl < nbClusters; icl++)
    {
      getLinearTerm(icl,q0,q1,r);
      std::vector<Dof> R;
      getKeys(icl,R);
      for (int j=0; j<R.size(); j++)
      {
        std::map<Dof,int>::iterator it =  _unknown.find(R[j]);
        if (it != _unknown.end())
        {
          int row = it->second;
          addToRHS(row,r(j),_sys);
        }
      }
    }
  }
};
void ReductionTFA::assembleDresDu(const ClusteringData *q0,  const ClusteringData *q1)
{
  //Msg::Info("calling assembleDresDu");
  zeroMatrix(_sys);
  int nbClusters = q0->clustersNb;
  static fullMatrix<double> Ke;
  std::vector<int> othersId;
  
  if(_withFrame)
  {
    for (int icl = 0; icl < (nbClusters+1); icl++)
    {
      getBiLinearTermWithFrame(icl,q0,q1,othersId,Ke);
      ///Ke.print("KKKKKKK");
      std::vector<Dof> R, C;
      getKeys(icl,R);
      for (int jcl=0; jcl< othersId.size(); jcl++)
      {
        getKeys(othersId[jcl],C);
      }
      for (int iR=0; iR < R.size(); iR++)
      {
        std::map<Dof,int>::iterator itR =  _unknown.find(R[iR]);
        if (itR != _unknown.end())
        {
          int row = itR->second;
          for (int iC =0; iC < C.size(); iC++)
          {
            std::map<Dof,int>::iterator itC =  _unknown.find(C[iC]);
            if (itC != _unknown.end())
            {
              int col = itC->second;
              addToMatrix(row,col,Ke(iR,iC),_sys);
            }
          }
        }
      }
    }
  }
  else if(_polarization)
  {
    for (int icl = 0; icl < nbClusters; icl++)
    {
      getBiLinearTermHS(icl,q0,q1,othersId,Ke);
      //Ke.print("Ke");
      //
      std::vector<Dof> R, C;
      getKeys(icl,R);
      for (int jcl=0; jcl< othersId.size(); jcl++)
      {
        getKeys(othersId[jcl],C);
      }
      for (int iR=0; iR < R.size(); iR++)
      {
        std::map<Dof,int>::iterator itR =  _unknown.find(R[iR]);
        if (itR != _unknown.end())
        {
          int row = itR->second;
          for (int iC =0; iC < C.size(); iC++)
          {
            std::map<Dof,int>::iterator itC =  _unknown.find(C[iC]);
            if (itC != _unknown.end())
            {
              int col = itC->second;
              addToMatrix(row,col,Ke(iR,iC),_sys);
            }
          }
        }
      }
    }
  }
  else
  {
    for (int icl = 0; icl < nbClusters; icl++)
    {
      getBiLinearTerm(icl,q0,q1,othersId,Ke);
      //Ke.print("Ke");
      //
      std::vector<Dof> R, C;
      getKeys(icl,R);
      for (int jcl=0; jcl< othersId.size(); jcl++)
      {
        getKeys(othersId[jcl],C);
      }
      for (int iR=0; iR < R.size(); iR++)
      {
        std::map<Dof,int>::iterator itR =  _unknown.find(R[iR]);
        if (itR != _unknown.end())
        {
          int row = itR->second;
          for (int iC =0; iC < C.size(); iC++)
          {
            std::map<Dof,int>::iterator itC =  _unknown.find(C[iC]);
            if (itC != _unknown.end())
            {
              int col = itC->second;
              addToMatrix(row,col,Ke(iR,iC),_sys);
            }
          }
        }
      }
    }
  }
};
void ReductionTFA::updateFieldFromUnknown(const ClusteringData *q0,  ClusteringData *q1)
{
  int nbClusters = q0->clustersNb;
  if(_withFrame)
  {
    nbClusters += 1;
  }
  for (int icl = 0; icl < nbClusters; icl++)
  {
    std::vector<Dof> R;
    getKeys(icl,R);
    int Rsize = R.size();
    fullVector<double> val(Rsize);
    for (int j=0; j< Rsize; j++)
    {
      std::map<Dof,int>::iterator itR =  _unknown.find(R[j]);
      if (itR != _unknown.end())
      {
        int row = itR->second;
        val(j) = getFromSolution(row,_sys);
      }
      else
      {
        val(j) = _fixedDof[R[j]];
      }
    }
    updateLocalFieldFromDof(icl,val,q0,q1);
  };
};
bool ReductionTFA::systemSolve(const STensor3& F0, const STensor3& F1,
                const ClusteringData *q0,  ClusteringData *q1,  
                STensor3& P, bool stiff, STensor43& L, 
                int maxIter, double tol, double absTol, 
                bool messageActive)
{
  if (!_isInitialized)
  {
    initSolve(q0,q1,_sys);
  };
  
  // zero solution first
  initialZeroSolution(_sys);
  //
  setInitialState(F0,F1);
  updateFieldFromUnknown(q0,q1);
  //estimate strains at each clusters and evaluate constitutive relation
  localConstitutive(F0,F1,q0,q1);

  if (messageActive)
  {
    F1.print("------------------------");
  }
  int iter = 0;
  bool converged = true;
  bool stiffnessEstimated = false;
  double norm0 = -1;
  double t = Cpu();
  while (true)
  {    //
    assembleRes(q0,q1);
    //
    double resNorm = normInfRightHandSide(_sys);
    //printf("resNorm0000000 = %.4e\n",resNorm);
    if (iter == 0)
    {
      norm0 = resNorm;
    }
    if (messageActive)
    {
      printf("iter: %d res = %e reltol %e\n",iter,resNorm,resNorm/norm0);
    }
    if (STensorOperation::isnan(resNorm))
    {
      converged = false;
      Msg::Warning("norm is nan, solve is not converge!!!");
      break;
    }
    //
    if (resNorm < absTol)
    {
      if (messageActive)
      {
        printf("converged by absolute tolerance = %.4e\n",absTol);
      }
      break;
    };
    if (resNorm > tol*norm0)
    {
      stiffnessEstimated = true;
      assembleDresDu(q0,q1);
      int isSolved =  systemSolve(_sys);
      if (isSolved==0)
      {
        if (messageActive)
        {
          printf("solver is not successful !!!\n");
        }
        converged = false;
        break;
      }
      updateFieldFromUnknown(q0,q1);
      // constitutive
      localConstitutive(F0,F1,q0,q1);
    }
    else
    {
      if (messageActive)
      {
        printf("converged by relative tolerance = %.4e, execution time = %.5f s\n",tol,Cpu()-t);
      }
      break;
    }

    iter++;
    if (iter > maxIter)
    {
      converged = false;
      Msg::Warning("maximum number of iterations is reached, solve is not converge!!!");
      F1.print("Fcur");
      break;
    };
  };
  //printf("it = %d \n",iter);

  if (converged)
  {
    // get Stress
    q1->computeHomogenizedStress(P);
    STensor3& PHom = q1->getRefToMacroStress();
    
    PHom = P;
    if (stiff)
    {
      if (!stiffnessEstimated)
      {
        assembleDresDu(q0,q1);
      }
      bool succ = tangentSolve(_sys,_unknown,q0,q1,L);
      q1->getRefToMacroTangent() = L;
    }
    //
    return true;
  }
  else
  {
    return false;
  }
};

bool ReductionTFA::tangentSolve(nonLinearSystemPETSc<double>* &NLsystem, std::map<Dof,int> &unknowns, const ClusteringData *q0, const ClusteringData *q1, STensor43& L)
{  
  // allocatioon
  int strainDim = 0;
  bool isAllocated=false;
    //
  //int nbClusters = _clusteringData->getTotalNumberOfClusters();
  int nbClusters = q0->clustersNb;
  if (_withFrame)
  {
    nbClusters += 1;
  }
  for (int icl=0; icl < nbClusters; icl++)
  {
    static fullMatrix<double> m;   
    getTangentTerm(icl,q0,q1,m);

    if (!isAllocated)
    {
      strainDim = m.size2();
      isAllocated= true;
      //allocateMultipleRHS(strainDim,_sys);
      //zeroMultipleRHS(_sys);
      allocateMultipleRHS(strainDim,NLsystem);
      zeroMultipleRHS(NLsystem);
    }
    
    std::vector<Dof> R;
    getKeys(icl,R);
    for (int j=0; j<R.size(); j++)
    {
      //std::map<Dof,int>::iterator it =  _unknown.find(R[j]);
      //if (it != _unknown.end())
      std::map<Dof,int>::iterator it =  unknowns.find(R[j]);
      if (it != unknowns.end())
      {
        int row = it->second;
        for (int col =0; col < m.size2(); col++)
        {
          //addToMultipleRHSMatrix(row,col,m(j,col),_sys);
          addToMultipleRHSMatrix(row,col,m(j,col),NLsystem);
        }
      }
    }
  }
  //bool ok = systemSolveMultipleRHS(_sys);
  bool ok = systemSolveMultipleRHS(NLsystem);
  if (!ok)
  {
    Msg::Error("system multiple RHS is not sucessfully solved");
    return false;
  }

  
  static fullMatrix<double> Lmat;
  Lmat.resize(strainDim,strainDim, true);
  const std::map<int,double>& cluterVolumeMap= _clusteringData->getClusterVolumeMap();
  
  const std::vector<double>& cluterVolumeVector= q0->allClusterVolumes;
  const std::vector<int>& cluterIDVector= q0->allClusterIndexes;
  
  double vtotal = 0.;
  //for (std::map<int,double>::const_iterator itc = cluterVolumeMap.begin(); itc != cluterVolumeMap.end(); itc++)
  for (int itc = 0; itc != nbClusters; itc++)
  {
    //int icl = itc->first;
    //double vCluster = itc->second;
    
    int icl = cluterIDVector[itc];
    double vCluster = cluterVolumeVector[itc];
    
    vtotal += vCluster;
    static fullMatrix<double> Licluster;
    STensorOperation::fromSTensor43ToFullMatrix( q1->getConstRefToTangent(icl),Licluster);
    //printf("vCluster: %d \n", icl);
    //
    std::vector<Dof> R;
    getKeys(icl,R);

    static std::vector<STensor3> dFlocalDunknown;
    getDLocalStrainDKeys(icl,q0,q1,dFlocalDunknown);
    //
    static std::vector<fullVector<double> > DunknownDF;
    if (DunknownDF.size() < R.size())
    {
      DunknownDF.resize(R.size(),fullVector<double>());
    }
    for (int row = 0; row <R.size(); row++)
    {
      DunknownDF[row].resize(strainDim,true);
      //std::map<Dof,int>::const_iterator itR = _unknown.find(R[row]);
      //if (itR != _unknown.end())
      std::map<Dof,int>::const_iterator itR = unknowns.find(R[row]);
      if (itR != unknowns.end())
      {
        int Nrow = itR->second;
        for (int col = 0; col <strainDim; col++)
        {
          double& val = DunknownDF[row](col);
          //val = getFromMultipleRHSSolution(Nrow,col,_sys);
          val = getFromMultipleRHSSolution(Nrow,col,NLsystem);
        }
      }
    }
    
    for (int idof=0; idof < R.size(); idof++)
    {
      static fullVector<double> DlocalDUvec;
      STensorOperation::fromSTensor3ToFullVector(dFlocalDunknown[idof],DlocalDUvec);
      for (int i=0; i< strainDim; i++)
      {
        for (int j=0; j<strainDim; j++)
        {
          for (int k=0; k<strainDim; k++)
          {
            Lmat(i,j) -= vCluster*Licluster(i,k)*DlocalDUvec(k)*DunknownDF[idof](j);
          }
        }
      }
    }
  };
  Lmat.scale(1./vtotal);
  STensorOperation::fromFullMatrixToSTensor43(Lmat,L);
  return true;
};

void ReductionTFA::fixDofs()
{
  if (getDim() == 2)
  {
    _fixedDof.clear();
    int nbClusters = _clusteringData->getTotalNumberOfClusters();
    if(_withFrame)
    {
      nbClusters += 1;
    }
    for (int clusterId=0; clusterId< nbClusters; clusterId++)
    {
      _fixedDof[Dof(clusterId, Dof::createTypeWithTwoInts(2,_tag))] = 0.;
      _fixedDof[Dof(clusterId, Dof::createTypeWithTwoInts(4,_tag))] = 0.;
      _fixedDof[Dof(clusterId, Dof::createTypeWithTwoInts(5,_tag))] = 0.;
    }
  }
}


void ReductionTFA::getKeys(int clusterId, std::vector<Dof>& keys) const
{
  // in small strain setting
  for (int i=0; i<6; i++)
  {
    keys.push_back(Dof(clusterId, Dof::createTypeWithTwoInts(i,_tag)));
  };
};

void ReductionTFA::getDLocalStrainDKeys(int clusterId, const ClusteringData *q0,  const ClusteringData *q1, std::vector<STensor3>& DlocalEkeys) const
{
  DlocalEkeys.resize(6);
  for (int i=0; i< 6; i++)
  {
    STensorOperation::zero(DlocalEkeys[i]);
  }
  DlocalEkeys[0](0,0)=1.;
  DlocalEkeys[1](1,1)=1.;
  DlocalEkeys[2](2,2)=1.;
  DlocalEkeys[3](0,1)=1.;
  DlocalEkeys[3](1,0)=1.;
  DlocalEkeys[4](0,2)=1.;
  DlocalEkeys[4](2,0)=1.;
  DlocalEkeys[5](1,2)=1.;
  DlocalEkeys[5](2,1)=1.;
};
void ReductionTFA::getInitialDofValue(int clusterId, const STensor3& F0, const STensor3& Fn, fullVector<double>& val) const
{
  //val.resize(6,true); // set to zero  
  STensor3 Deps;
  Deps = Fn;
  Deps -= F0;  
  int nbClusters = _clusteringData->getTotalNumberOfClusters();
  if(clusterId == nbClusters)
  {
    STensorOperation::fromSTensor3ToFullVector(Deps,val);
  }
  else
  {
    const STensor43 &Ai = getClusterAverageStrainConcentrationTensor(clusterId);
    static STensor3 temp;
    STensorOperation::multSTensor43STensor3(Ai,Deps,temp);
    STensorOperation::fromSTensor3ToFullVector(temp,val);
  }
};
void ReductionTFA::updateLocalFieldFromDof(int clusterId, const fullVector<double>& vals, const ClusteringData *q0,  ClusteringData *q1)
{
  int nbClusters = q0->clustersNb;
  if(clusterId == nbClusters)
  {
    const STensor3& eps0 = q0->getConstRefToFrameStrain();
    STensor3& eps = q1->getRefToFrameStrain();
    eps = eps0;   
    static STensor3 Deps;
    STensorOperation::fromFullVectorToSTensor3(vals,Deps);
    eps += Deps;
  }
  else
  {
    const STensor3& eps0 = q0->getConstRefToStrain(clusterId);
    STensor3& eps = q1->getRefToStrain(clusterId);
    eps = eps0;
    static STensor3 Deps;
    STensorOperation::fromFullVectorToSTensor3(vals,Deps);
    eps += Deps;
  }
};


void ReductionTFA::getLinearTerm(int clusterId, const ClusteringData *q0,  const ClusteringData *q1, fullVector<double>& res) const
{
  int nbClusters = _clusteringData->getTotalNumberOfClusters();
  const STensor3& eps0i = q0->getConstRefToStrain(clusterId);
  const STensor3& eps1i = q1->getConstRefToStrain(clusterId);
  //
  const STensor3& macroeps0 = q0->getConstRefToMacroStrain();
  const STensor3& macroeps1 = q1->getConstRefToMacroStrain();
  
  const STensor43 &Ai = getClusterAverageStrainConcentrationTensor(clusterId);
  static STensor3 resTen;
  resTen = eps1i;
  resTen -= eps0i;
  STensorOperation::multSTensor43STensor3Add(Ai,macroeps0,1.,resTen);
  STensorOperation::multSTensor43STensor3Add(Ai,macroeps1,-1.,resTen);
  
  for (int jcl =0; jcl < nbClusters; jcl++)
  {
    const STensor43 &Dij =  getClusterInteractionTensor(clusterId,jcl);    
    
    const STensor3& epsEig0j = q0->getConstRefToEigenStrain(jcl);
    const STensor3& epsEig1j = q1->getConstRefToEigenStrain(jcl);
    
    if(_correction==1){
    double corr_j = getClusterNonUniformCorrection(jcl,q0->getConstRefToMacroStrain(),q1->getConstRefToMacroStrain());
    STensorOperation::multSTensor43STensor3Add(Dij,epsEig1j,1.*corr_j,resTen);
    STensorOperation::multSTensor43STensor3Add(Dij,epsEig0j,-1.*corr_j,resTen);
    }
    else
    {
    STensorOperation::multSTensor43STensor3Add(Dij,epsEig1j,1.,resTen);
    STensorOperation::multSTensor43STensor3Add(Dij,epsEig0j,-1.,resTen);    
    }
  };
  STensorOperation::fromSTensor3ToFullVector(resTen,res);
};
void ReductionTFA::getBiLinearTerm(int clusterId, const ClusteringData *q0,  const ClusteringData *q1, std::vector<int>& othersId, fullMatrix<double>& dres) const 
{
  int nbClusters = _clusteringData->getTotalNumberOfClusters();
  othersId.resize(nbClusters);
  for (int j=0; j< nbClusters; j++)
  {
    othersId[j] = j;
  }
  dres.resize(6,6*nbClusters,true);
  
  double vtot;
  vtot = totalVolume();
  
  for (int jcl=0; jcl< nbClusters; jcl++)
  {
    static STensor43 J_ij;
    getJac_rs(clusterId,jcl,q0,q1,J_ij);
    static fullMatrix<double> J_ij_matrix;
    STensorOperation::fromSTensor43ToFullMatrix(J_ij,J_ij_matrix);
    for (int row=0; row<6; row++)
    {
      for (int col=0; col<6; col++)
      {
        dres(row, col+6*jcl) += J_ij_matrix(row,col);
      }
    }
  }
}

void ReductionTFA::getJac_rs(int r, int s, const ClusteringData *q0,  const ClusteringData *q1, STensor43& Jac_rs) const 
{
  if (r==s)
  {
    STensorOperation::unity(Jac_rs);
  }
  else
  {
    STensorOperation::zero(Jac_rs);
  }
  
  const STensor43 &Drs =  getClusterInteractionTensor(r,s);
  const STensor43 &depsEig1dstrains =  q1->getConstRefTodEigenStraindStrain(s);
  if(_correction==1){
  double corr_s = getClusterNonUniformCorrection(s,q0->getConstRefToMacroStrain(),q1->getConstRefToMacroStrain());
  STensorOperation::multSTensor43Add(Drs,depsEig1dstrains,1.*corr_s,Jac_rs);
  }
  else{
  STensorOperation::multSTensor43Add(Drs,depsEig1dstrains,1.,Jac_rs);
  }
}

void ReductionTFA::getTangentTerm(int clusterId, const ClusteringData *q0,  const ClusteringData *q1, fullMatrix<double>& DresDdefo) const
{
  if(_withFrame)
  {
    int nbClusters = _clusteringData->getTotalNumberOfClusters();
    STensor43 dResdDeps;
    if(clusterId == nbClusters)
    {
      STensorOperation::unity(dResdDeps);
    }
    else
    {
      STensorOperation::zero(dResdDeps);
    }
    STensorOperation::fromSTensor43ToFullMatrix(dResdDeps,DresDdefo);
  }
  else if(_polarization)
  {
    STensor43 dResdDeps;
    STensorOperation::unity(dResdDeps);
    STensorOperation::fromSTensor43ToFullMatrix(dResdDeps,DresDdefo);
    DresDdefo.scale(-1.);
  }
  else if(getMethod() == TFA_HierarchicalIncrementalTangent)
  {
    std::vector<int> clIDsHierarchical = q0->upperClusterIDs;
    clIDsHierarchical.push_back(clusterId);
    const STensor43 &Ai = getClusterAverageStrainConcentrationTensorHierarchical(clIDsHierarchical);
    STensorOperation::fromSTensor43ToFullMatrix(Ai,DresDdefo);
    DresDdefo.scale(-1.);
  }
  else
  {
    const STensor43 &Ai = getClusterAverageStrainConcentrationTensor(clusterId);
    STensorOperation::fromSTensor43ToFullMatrix(Ai,DresDdefo);
    DresDdefo.scale(-1.);
  }
};


void ReductionTFA::getLinearTermHS(int clusterId, const ClusteringData *q0,  const ClusteringData *q1, fullVector<double>& res) const
{
  int nbClusters = _clusteringData->getTotalNumberOfClusters();
  
  const STensor3& eps1i = q1->getConstRefToStrain(clusterId);
  const STensor3& eps0i = q0->getConstRefToStrain(clusterId);
  
  const STensor3& macroeps1 = q1->getConstRefToMacroStrain();
  const STensor3& macroeps0 = q0->getConstRefToMacroStrain();

  static STensor3 resTen;  
  resTen = eps1i;
  resTen -= eps0i;
  resTen -= macroeps1;
  resTen += macroeps0;
  
  const STensor43& C0iso = _clusteringData->getReferenceStiffnessIso();
  double G0 = C0iso(0,1,0,1);
  const double& Gtan = q1->getConstRefToTangentShearModulus();
  
  for (int jcl =0; jcl < nbClusters; jcl++)
  {
    const STensor43 &Dij =  getClusterInteractionTensor(clusterId,jcl);
    
    const STensor3& sig1j = q1->getConstRefToStress(jcl);
    const STensor3& eps1j = q1->getConstRefToStrain(jcl);      
    const STensor3& sig0j = q0->getConstRefToStress(jcl);
    const STensor3& eps0j = q0->getConstRefToStrain(jcl);
    
    STensor3 dsigj(sig1j);
    dsigj -= sig0j;
    STensor3 depsj(eps1j);
    depsj -= eps0j; 
    
    STensor3 fac;    
    fac = dsigj;
    fac *= G0/Gtan;
    STensorOperation::multSTensor43STensor3Add(C0iso,depsj,-1.,fac);         
    STensorOperation::multSTensor43STensor3Add(Dij,fac,-1.,resTen);          
  }
  STensorOperation::fromSTensor3ToFullVector(resTen,res);
};
void ReductionTFA::getBiLinearTermHS(int clusterId, const ClusteringData *q0,  const ClusteringData *q1, std::vector<int>& othersId, fullMatrix<double>& dres) const 
{
  int nbClusters = _clusteringData->getTotalNumberOfClusters();
  othersId.resize(nbClusters);
  for (int j=0; j< nbClusters; j++)
  {
    othersId[j] = j;
  }
  dres.resize(6,6*nbClusters,true);
  
  double vtot;
  vtot = totalVolume();
  
  for (int jcl=0; jcl< nbClusters; jcl++)
  {
    static STensor43 J_ij;
    getJacHS_rs(clusterId,jcl,q0,q1,J_ij);
    static fullMatrix<double> J_ij_matrix;
    STensorOperation::fromSTensor43ToFullMatrix(J_ij,J_ij_matrix);
    for (int row=0; row<6; row++)
    {
      for (int col=0; col<6; col++)
      {
        dres(row, col+6*jcl) += J_ij_matrix(row,col);
      }
    }
  }
}
void ReductionTFA::getJacHS_rs(int r, int s, const ClusteringData *q0,  const ClusteringData *q1, STensor43& Jac_rs) const 
{
  if (r==s)
  {
    STensorOperation::unity(Jac_rs);
  }
  else
  {
    STensorOperation::zero(Jac_rs);
  }
  
  const STensor3& macroeps1 = q1->getConstRefToMacroStrain(); 
  
  const STensor43& C0iso = _clusteringData->getReferenceStiffnessIso();
  double G0 = C0iso(0,1,0,1);
  const double& Gtan = q1->getConstRefToTangentShearModulus(); 
  const STensor43 &Drs =  getClusterInteractionTensor(r,s); 
  const STensor43 &Calgs = q1->getConstRefToTangent(s);
  
  const STensor3& sig1s = q1->getConstRefToStress(s);
  const STensor3& sig0s = q0->getConstRefToStress(s);   
  STensor3 dsigs(sig1s);
  dsigs -= sig0s;
   
  STensor43 fac; 
  fac = Calgs;
  fac *= G0/Gtan;
  fac -= C0iso; 
  STensorOperation::multSTensor43Add(Drs,fac,-1.,Jac_rs);
  
  /*STensor3 dGtandeps;
  dtangentShearModulusOveralldeps(q0,q1,s,dGtandeps);
  STensor3 fac2_part;
  int nbClusters = _clusteringData->getTotalNumberOfClusters(); 
  for (int p =0; p < nbClusters; p++)
  {
    const STensor43 &Drp =  getClusterInteractionTensor(r,p);
    const STensor3& sig1p = q1->getConstRefToStress(p);
    const STensor3& sig0p = q0->getConstRefToStress(p); 
    STensor3 Dsigp(sig1p);
    Dsigp -= sig0p; 
    STensorOperation::multSTensor43STensor3Add(Drp,Dsigp,1.,fac2_part);
  }
  fac2_part *= G0/(Gtan*Gtan);
  STensorOperation::prodAdd(fac2_part,dGtandeps,1.,Jac_rs);*/
}


void ReductionTFA::getLinearTermPFA(int clusterId, const ClusteringData *q0,  const ClusteringData *q1, fullVector<double>& res) const
{
  int nbClusters = _clusteringData->getTotalNumberOfClusters();
  
  const STensor3& eps1i = q1->getConstRefToStrain(clusterId);
  const STensor3& eps0i = q0->getConstRefToStrain(clusterId);
  
  const STensor3& macroeps1 = q1->getConstRefToMacroStrain();
  const STensor3& macroeps0 = q0->getConstRefToMacroStrain();
  
  double epsEq = TFANumericTools::EquivalentStrain(macroeps1);

  static STensor3 resTen;  
  resTen = eps1i;
  resTen -= eps0i;
  resTen -= macroeps1;
  resTen += macroeps0;
  
  const STensor43& C0iso = _clusteringData->getReferenceStiffnessIso();
  //const STensor43& C0 = _clusteringData->getReferenceStiffness();
  double G0 = C0iso(0,1,0,1);
  const double& Gtan = q1->getConstRefToTangentShearModulus();
  
  //STensor43 Ciso_inv, C_inv;
  //if(epsEq>1.e-10)
  //{
    //const STensor43& CisoUpdate_inv = q1->getConstRefToIsotropicStiffnessInverted();
    //const STensor43& CanisoUpdate_inv = q1->getConstRefToAnisotropicStiffnessInverted();
    //Ciso_inv = CisoUpdate_inv;
    //C_inv = CanisoUpdate_inv;
  //}
  //else
  //{
    //STensor43 C0iso_inv;
    //STensorOperation::inverseSTensor43(C0iso,C0iso_inv); 
    //Ciso_inv = C0iso_inv;
    
    //STensor43 C0_inv;
    //STensorOperation::inverseSTensor43(C0,C0_inv); 
    //C_inv = C0_inv;
  //}
  
  for (int jcl =0; jcl < nbClusters; jcl++)
  {
    const STensor43 &Dij =  getClusterInteractionTensor(clusterId,jcl);
    
    const STensor3& sig1j = q1->getConstRefToStress(jcl);
    const STensor3& eps1j = q1->getConstRefToStrain(jcl);      
    const STensor3& sig0j = q0->getConstRefToStress(jcl);
    const STensor3& eps0j = q0->getConstRefToStrain(jcl);
    
    STensor3 dsigj(sig1j);
    dsigj -= sig0j;
    STensor3 depsj(eps1j);
    depsj -= eps0j; 
    
    STensor3 fac;    
    fac = dsigj;
    fac *= G0/Gtan;
    //STensor43 prefac;
    //STensorOperation::multSTensor43(C0iso,Ciso_inv,prefac);
    //STensorOperation::multSTensor43STensor3(prefac,dsigj,fac);
    STensorOperation::multSTensor43STensor3Add(C0iso,depsj,-1.,fac);

    //STensorOperation::multSTensor43(C0,C_inv,prefac);
    //STensorOperation::multSTensor43STensor3(prefac,dsigj,fac);
    //STensorOperation::multSTensor43STensor3Add(C0,depsj,-1.,fac);
            
    STensorOperation::multSTensor43STensor3Add(Dij,fac,-1.,resTen);          
  }
  STensorOperation::fromSTensor3ToFullVector(resTen,res);
};
void ReductionTFA::getBiLinearTermPFA(int clusterId, const ClusteringData *q0,  const ClusteringData *q1, std::vector<int>& othersId, fullMatrix<double>& dres) const 
{
  int nbClusters = _clusteringData->getTotalNumberOfClusters();
  othersId.resize(nbClusters);
  for (int j=0; j< nbClusters; j++)
  {
    othersId[j] = j;
  }
  dres.resize(6,6*nbClusters,true);
  
  double vtot;
  vtot = totalVolume();
  
  for (int jcl=0; jcl< nbClusters; jcl++)
  {
    static STensor43 J_ij;
    getJacHS_rs(clusterId,jcl,q0,q1,J_ij);
    static fullMatrix<double> J_ij_matrix;
    STensorOperation::fromSTensor43ToFullMatrix(J_ij,J_ij_matrix);
    for (int row=0; row<6; row++)
    {
      for (int col=0; col<6; col++)
      {
        dres(row, col+6*jcl) += J_ij_matrix(row,col);
      }
    }
  }
}
void ReductionTFA::getJacPFA_rs(int r, int s, const ClusteringData *q0,  const ClusteringData *q1, STensor43& Jac_rs) const 
{
  if (r==s)
  {
    STensorOperation::unity(Jac_rs);
  }
  else
  {
    STensorOperation::zero(Jac_rs);
  }
  
  const STensor3& macroeps1 = q1->getConstRefToMacroStrain(); 
  double epsEq = TFANumericTools::EquivalentStrain(macroeps1);
  
  const STensor43& C0iso = _clusteringData->getReferenceStiffnessIso();
  //const STensor43& C0 = _clusteringData->getReferenceStiffness();
  double G0 = C0iso(0,1,0,1);
  const double& Gtan = q1->getConstRefToTangentShearModulus(); 
  const STensor43 &Drs =  getClusterInteractionTensor(r,s); 
  const STensor43 &Calgs = q1->getConstRefToTangent(s);
  
  //STensor43 Ciso_inv, C_inv;
  //if(epsEq>1.e-10)
  //{
    //const STensor43& CisoUpdate_inv = q1->getConstRefToIsotropicStiffnessInverted();
    //const STensor43& CanisoUpdate_inv = q1->getConstRefToAnisotropicStiffnessInverted();   
    //Ciso_inv = CisoUpdate_inv;
    //C_inv = CanisoUpdate_inv;
  //}
  //else
  //{
    //STensor43 C0iso_inv;
    //STensorOperation::inverseSTensor43(C0iso,C0iso_inv); 
    //Ciso_inv = C0iso_inv;
    
    //STensor43 C0_inv;
    //STensorOperation::inverseSTensor43(C0,C0_inv); 
    //C_inv = C0_inv;
  //}
  
  const STensor3& sig1s = q1->getConstRefToStress(s);
  const STensor3& sig0s = q0->getConstRefToStress(s);   
  STensor3 dsigs(sig1s);
  dsigs -= sig0s;
   
  STensor43 fac; 
  fac = Calgs;
  fac *= G0/Gtan;
  //STensor43 prefac;
  //STensorOperation::multSTensor43(C0iso,Ciso_inv,prefac);
  //STensorOperation::multSTensor43(prefac,Calgs,fac);
  fac -= C0iso;
  
  //STensorOperation::multSTensor43(C0,C_inv,prefac);
  //STensorOperation::multSTensor43(prefac,Calgs,fac);
  //fac -= C0;
  
  STensorOperation::multSTensor43Add(Drs,fac,-1.,Jac_rs);
  
  /*STensor3 dGtandeps;
  dtangentShearModulusOveralldeps(q0,q1,s,dGtandeps);
  STensor3 fac2_part;
  int nbClusters = _clusteringData->getTotalNumberOfClusters(); 
  for (int p =0; p < nbClusters; p++)
  {
    const STensor43 &Drp =  getClusterInteractionTensor(r,p);
    const STensor3& sig1p = q1->getConstRefToStress(p);
    const STensor3& sig0p = q0->getConstRefToStress(p); 
    STensor3 Dsigp(sig1p);
    Dsigp -= sig0p; 
    STensorOperation::multSTensor43STensor3Add(Drp,Dsigp,1.,fac2_part);
  }
  fac2_part *= G0/(Gtan*Gtan);
  STensorOperation::prodAdd(fac2_part,dGtandeps,1.,Jac_rs);*/
}


void ReductionTFA::elasticStiffnessHom(STensor43& CelHom) const 
{
  int nbClusters = _clusteringData->getTotalNumberOfClusters(); 

  STensorOperation::zero(CelHom);
  double vtot;
  vtot = 0.;
  for (int icl =0; icl < nbClusters; icl++)
  {
    const std::map<int,double>& cluterVolumeMap= _clusteringData->getClusterVolumeMap();
    std::map<int,double>::const_iterator itc = cluterVolumeMap.find(icl);
    double vi = itc->second;
    vtot += vi;    
    const materialLaw *law = getLawForCluster(icl); 
    STensor43 Celi;
    STensorOperation::zero(Celi);
    law->ElasticStiffness(&Celi);
    const STensor43 &Aeli = getClusterAverageStrainConcentrationTensor(icl);
    STensorOperation::multSTensor43Add(Celi,Aeli,vi,CelHom);
  }
  CelHom *= (1./vtot);
}

void ReductionTFA::elasticTotalTrialStressHom(const ClusteringData *q,STensor3& PtrHom) const 
{
  int nbClusters = _clusteringData->getTotalNumberOfClusters(); 
  
  //const STensor3& macroEps = q->getConstRefToMacroStrain();

  STensorOperation::zero(PtrHom);
  double vtot;
  vtot = 0.;
  for (int icl =0; icl < nbClusters; icl++)
  {
    const std::map<int,double>& cluterVolumeMap= _clusteringData->getClusterVolumeMap();
    std::map<int,double>::const_iterator itc = cluterVolumeMap.find(icl);
    double vi = itc->second;
    vtot += vi;
    
    /*const STensor43 &Aeli = getClusterAverageStrainConcentrationTensor(icl);
    STensor3 epsi;
    STensorOperation::multSTensor43STensor3(Aeli,macroEps,eps1i);*/
    
    const STensor3& eps1i = q->getConstRefToStrain(icl);
    
    const materialLaw *law = getLawForCluster(icl); 
    STensor43 Celi;
    STensorOperation::zero(Celi);
    law->ElasticStiffness(&Celi);
    STensorOperation::multSTensor43STensor3Add(Celi,eps1i,vi,PtrHom);
  }
  PtrHom *= (1./vtot);
}

void ReductionTFA::elasticIncrementalTrialStressHom(const ClusteringData *q0, const ClusteringData *q1, STensor3& DPtrHom) const 
{
  int nbClusters = _clusteringData->getTotalNumberOfClusters(); 

  STensorOperation::zero(DPtrHom);
  double vtot;
  vtot = 0.;
  for (int icl =0; icl < nbClusters; icl++)
  {
    const std::map<int,double>& cluterVolumeMap= _clusteringData->getClusterVolumeMap();
    std::map<int,double>::const_iterator itc = cluterVolumeMap.find(icl);
    double vi = itc->second;
    vtot += vi;
    
    const STensor3& eps0i = q0->getConstRefToStrain(icl);
    const STensor3& eps1i = q1->getConstRefToStrain(icl);
    STensor3 depsi;
    depsi = eps1i;
    depsi -= eps0i;
    
    const materialLaw *law = getLawForCluster(icl); 
    STensor43 Celi;
    STensorOperation::zero(Celi);
    law->ElasticStiffness(&Celi);
    STensorOperation::multSTensor43STensor3Add(Celi,depsi,vi,DPtrHom);
  }
  DPtrHom *= (1./vtot);
}


void ReductionTFA::dsecantShearModulusOveralldeps(const ClusteringData *q0, const ClusteringData *q1, int s, STensor3& dGHomdeps_s) const
{

  const STensor3& macroEps1 = q1->getConstRefToMacroStrain();
  const STensor3& macroEps0 = q0->getConstRefToMacroStrain();
  STensor3 DmacroEps = macroEps1;
  DmacroEps -= macroEps0;
  double DepsEq = TFANumericTools::EquivalentStrain(DmacroEps);
  
  const STensor3& epsEigMacro0 = q0->getConstRefToMacroEigenStrain();
  const STensor3& epsEigMacro1 = q1->getConstRefToMacroEigenStrain();
  STensor3 DepsEigMacro;
  DepsEigMacro = epsEigMacro1;
  DepsEigMacro -= epsEigMacro0;
  double DepsEigEqHom = TFANumericTools::EquivalentStrain(DepsEigMacro);
  
  STensor43 CelHom,SelHom;
  elasticStiffnessHom(CelHom);
  STensorOperation::inverseSTensor43(CelHom,SelHom);
  
  const double& Gel = q1->getConstRefToElasticShearModulus();

  double vtot, vfs;
  vtot = totalVolume();
  vfs = clusterVolume(s);
  vfs *= 1./vtot; 
  
  const materialLaw *law = getLawForCluster(s); 
  STensor43 Cels;
  law->ElasticStiffness(&Cels);
  const STensor43& Calgs = q1->getConstRefToTangent(s);
   
  STensor3 epsTrMacro,epsTrMacro_dev;
  epsTrMacro = macroEps1;
  epsTrMacro -= epsEigMacro0;
  epsTrMacro_dev = epsTrMacro.dev();
  double epsTrMacroEq = sqrt(2./3.*epsTrMacro_dev.dotprod());
  
  STensor43 Cdiff,ShomCdiff;
  Cdiff = Calgs;
  Cdiff -= Cels;
  STensorOperation::multSTensor43(SelHom,Cdiff,ShomCdiff);
  STensor3 p1, p2;
  STensorOperation::multSTensor3STensor43(DepsEigMacro,ShomCdiff,p1);
  p1 *= 2./3./DepsEigEqHom/epsTrMacroEq;
  p2 = epsTrMacro_dev;
  p2 *= 2.*DepsEigEqHom/(3.*epsTrMacroEq*epsTrMacroEq*epsTrMacroEq);
  
  if(DepsEigEqHom>1.e-10)
  {
  dGHomdeps_s = p1;
  dGHomdeps_s += p2;
  dGHomdeps_s *= Gel*vfs;
  } 
}

void ReductionTFA::dtangentShearModulusOveralldeps(const ClusteringData *q0, const ClusteringData *q1, int s, STensor3& dGHomdeps_s) const
{
  STensorOperation::zero(dGHomdeps_s);

  const STensor3& macroEps1 = q1->getConstRefToMacroStrain();
  const STensor3& macroEps0 = q0->getConstRefToMacroStrain();
  STensor3 DmacroEps = macroEps1;
  DmacroEps -= macroEps0;
  STensor3 DmacroEps_dev = DmacroEps.dev();
  double DmacroEpsEq = sqrt(2./3.*DmacroEps_dev.dotprod());
  
  /*const STensor3& macroSig1 = q1->getConstRefToMacroStress();
  const STensor3& macroSig0 = q0->getConstRefToMacroStress();
  STensor3 DmacroSig = macroSig1;
  DmacroSig -= macroSig0;
  STensor3 DmacroSig_dev = DmacroSig.dev();
  double DmacroSigEq = sqrt(3./2.*DmacroSig_dev.dotprod());*/
  
  const double& Gtan = q1->getConstRefToTangentShearModulus(); 
  STensor3 DmacroSig_dev(DmacroEps_dev);
  DmacroSig_dev *= 2.*Gtan;
  double DmacroSigEq = 3.*Gtan*DmacroEpsEq;
  
  STensor3 DmacroSigTr;
  elasticIncrementalTrialStressHom(q0,q1,DmacroSigTr);
  STensor3 DmacroSigTr_dev = DmacroSigTr.dev();
  double DmacroSigTrEq = sqrt(3./2.*DmacroSigTr_dev.dotprod());

  double vtot, vfs;
  vtot = totalVolume();
  vfs = clusterVolume(s);
  vfs *= 1./vtot; 
  
  const STensor43& Calgs = q1->getConstRefToTangent(s);

  if(DmacroEpsEq>1.e-10)
  {
  STensor3 dDmacroEpsEqdeps,dDsigMacroEqdeps;
  dDmacroEpsEqdeps = DmacroEps_dev;
  dDmacroEpsEqdeps *= 2./3.*vfs/DmacroEpsEq;
  
  STensorOperation::multSTensor3STensor43(DmacroSig_dev,Calgs,dDsigMacroEqdeps);
  dDsigMacroEqdeps *= 3./2.*vfs/DmacroSigEq;
  
  //STensorOperation::multSTensor3STensor43(DmacroSigTr_dev,Calgs,dDsigMacroEqdeps);
  //dDsigMacroEqdeps *= 3./2.*vfs/DmacroSigTrEq;
   
  dGHomdeps_s = dDsigMacroEqdeps;
  dGHomdeps_s *= 1./(3.*DmacroEpsEq);
  //dGHomdeps_s.print("dGHomdeps_s");
  dGHomdeps_s -= dDmacroEpsEqdeps*DmacroSigEq*(1./(3.*DmacroEpsEq*DmacroEpsEq));
  }
}


void ReductionTFA::secantStiffnessHom(const ClusteringData *q0, const ClusteringData *q1, STensor43& CsecHom) const 
{
  int nbClusters = _clusteringData->getTotalNumberOfClusters(); 

  STensorOperation::zero(CsecHom);
  double vtot;
  vtot = 0.;
  for (int icl =0; icl < nbClusters; icl++)
  {
    const std::map<int,double>& cluterVolumeMap= _clusteringData->getClusterVolumeMap();
    std::map<int,double>::const_iterator itc = cluterVolumeMap.find(icl);
    double vi = itc->second;
    vtot += vi;    
    const materialLaw *law = getLawForCluster(icl); 
    STensor43 Aseci;
    //secantStiffness(icl,q0,q1,Cseci);
    const STensor43& Cseci = q1->getConstRefToSecant(icl);
    STensorOperation::multSTensor43Add(Cseci,Aseci,vi,CsecHom);
  }
  CsecHom *= (1./vtot);
}

void ReductionTFA::secantStiffness(int r, const ClusteringData *q0, const ClusteringData *q1, STensor43& Csec) const 
{
  const materialLaw *law = getLawForCluster(r); 
  STensor43 Celr;
  STensorOperation::zero(Celr);
  law->ElasticStiffness(&Celr);
  
  STensor3 P1tr;
  const STensor3& eps1 = q1->getConstRefToStrain(r);
  double eps1Eq = TFANumericTools::EquivalentStrain(eps1);
  const STensor3& eps0 = q0->getConstRefToStrain(r);
  STensor3 Deps = eps1;
  Deps -= eps0;
  double DepsEq = TFANumericTools::EquivalentStrain(Deps);
  
  const STensor3& P1 = q1->getConstRefToStress(r);
  STensorOperation::multSTensor43STensor3(Celr,eps1,P1tr);
  
  Csec = Celr;
  
  double fac;
  fac = 1.;
  if(DepsEq>1.e-10)
  {
    STensor3 Ptr_dev = P1tr.dev();
    double sigEqtr = TFANumericTools::EquivalentStress(P1tr);
    double shearmodulus_elastic = sigEqtr/(3.*eps1Eq);
    
    double sigEq = TFANumericTools::EquivalentStress(P1);
    double shearmodulus_secant = sigEq/(3.*eps1Eq);
    
    fac = shearmodulus_secant/shearmodulus_elastic;
  }  
  Csec *= fac;
  
  // or
  // SecantStiffnessIsotropic(q1->getRefToIPv(r),Csec);
}

double ReductionTFA::totalVolume() const 
{
  int nbClusters = _clusteringData->getTotalNumberOfClusters(); 
  double vtot;
  vtot = 0.;
  for (int icl =0; icl < nbClusters; icl++)
  {
    const std::map<int,double>& cluterVolumeMap= _clusteringData->getClusterVolumeMap();
    std::map<int,double>::const_iterator itc = cluterVolumeMap.find(icl);
    double vi = itc->second;
    vtot += vi;
  }
  return vtot;
}

double ReductionTFA::clusterVolume(int r) const 
{
  const std::map<int,double>& cluterVolumeMap= _clusteringData->getClusterVolumeMap();
  std::map<int,double>::const_iterator itcr = cluterVolumeMap.find(r);
  double vr = itcr->second;
  return vr;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void ReductionTFA::getLinearTermWithFrame(int clusterId, const ClusteringData *q0,  const ClusteringData *q1, fullVector<double>& res) const
{
  int nbClusters = _clusteringData->getTotalNumberOfClusters(); 
  static STensor3 resTen;
  STensorOperation::zero(resTen);
  
  if(clusterId == nbClusters)
  {
    const STensor3& macroEps0 = q0->getConstRefToMacroStrain();
    const STensor3& macroEps1 = q1->getConstRefToMacroStrain();
    double vtot;
    vtot = 0.;
    for (int jcl =0; jcl < nbClusters; jcl++)
    {  
      const STensor3& eps0j = q0->getConstRefToStrain(jcl);
      const STensor3& eps1j = q1->getConstRefToStrain(jcl);
      STensor3 depsj = eps1j;
      depsj -= eps0j;
      const std::map<int,double>& cluterVolumeMap= _clusteringData->getClusterVolumeMap();
      std::map<int,double>::const_iterator itc = cluterVolumeMap.find(jcl);
      double vj = itc->second;
      vtot += vj;
      resTen.daxpy(depsj,-vj);
    }
    resTen *= (1./vtot);
    resTen += macroEps1;
    resTen -= macroEps0;
  }  
  else
  {
    const STensor3& eps0i = q0->getConstRefToStrain(clusterId);
    const STensor3& eps1i = q1->getConstRefToStrain(clusterId);
    const STensor3& frameEps0 = q0->getConstRefToFrameStrain();
    const STensor3& frameEps1 = q1->getConstRefToFrameStrain();
    
    const STensor43 &Ai = getClusterAverageStrainConcentrationTensor(clusterId);

    resTen = eps1i;
    resTen -= eps0i;
    STensorOperation::multSTensor43STensor3Add(Ai,frameEps0,1.,resTen);
    STensorOperation::multSTensor43STensor3Add(Ai,frameEps1,-1.,resTen);
    //resTen += frameEps0;
    //resTen -= frameEps1;
    for (int jcl =0; jcl < nbClusters; jcl++)
    {
      const STensor43 &Dij =  getClusterInteractionTensor(clusterId,jcl);
      const STensor3& epsEig0j = q0->getConstRefToEigenStrain(jcl);
      const STensor3& epsEig1j = q1->getConstRefToEigenStrain(jcl);
      STensorOperation::multSTensor43STensor3Add(Dij,epsEig1j,1.,resTen);
      STensorOperation::multSTensor43STensor3Add(Dij,epsEig0j,-1.,resTen);
    };
    const STensor43 &Di0 =  getClusterInteractionTensor(clusterId,nbClusters);
    const STensor3& epsEig0Frame = q0->getConstRefToFrameEigenStrain();
    const STensor3& epsEig1Frame = q1->getConstRefToFrameEigenStrain();

    STensorOperation::multSTensor43STensor3Add(Di0,epsEig1Frame,1.,resTen);
    STensorOperation::multSTensor43STensor3Add(Di0,epsEig0Frame,-1.,resTen);
  }
  STensorOperation::fromSTensor3ToFullVector(resTen,res);
};

void ReductionTFA::getBiLinearTermWithFrame(int clusterId, const ClusteringData *q0,  const ClusteringData *q1, std::vector<int>& othersId, fullMatrix<double>& dres) const 
{
  int nbClusters = _clusteringData->getTotalNumberOfClusters();
  othersId.resize(nbClusters+1);
  for (int j=0; j< (nbClusters+1); j++)
  {
    othersId[j] = j;
  }
  dres.resize(6,6*(nbClusters+1),true);
   
  if(clusterId == nbClusters)
  { 
    for (int jcl=0; jcl< nbClusters; jcl++)
    { 
      static STensor43 J_Kj;
      STensorOperation::unity(J_Kj);
      const std::map<int,double>& cluterVolumeMap= _clusteringData->getClusterVolumeMap();
      std::map<int,double>::const_iterator itcj = cluterVolumeMap.find(jcl);
      double vj = itcj->second;  
      J_Kj *= -vj;  
      static fullMatrix<double> J_Kj_matrix;
      STensorOperation::fromSTensor43ToFullMatrix(J_Kj,J_Kj_matrix);
      for (int row=0; row<6; row++)
      {
        for (int col=0; col<6; col++)
        {
          dres(row, col+6*jcl) = J_Kj_matrix(row,col);
        }
      }
    }
    
    double vtot;
    vtot = 0.;
    static STensor43 J_KK;
    STensorOperation::zero(J_KK);
    for (int jcl=0; jcl< nbClusters; jcl++)
    {
      const STensor43 &Aelj = getClusterAverageStrainConcentrationTensor(jcl);
      const std::map<int,double>& cluterVolumeMap= _clusteringData->getClusterVolumeMap();
      std::map<int,double>::const_iterator itc = cluterVolumeMap.find(jcl);
      double vj = itc->second;
      vtot += vj;
      static STensor43 J_KK_term;
      STensorOperation::zero(J_KK_term);
      J_KK_term.axpy(-1.,Aelj);
      J_KK_term *= vj;
      J_KK.axpy(1.,J_KK_term);
    }
    fullMatrix<double> J_KK_matrix;
    STensorOperation::zero(J_KK);
    STensorOperation::fromSTensor43ToFullMatrix(J_KK,J_KK_matrix);
    for (int row=0; row<6; row++)
    {
      for (int col=0; col<6; col++)
      {
        dres(row, col+6*nbClusters) = J_KK_matrix(row,col);
      }
    }   
    dres.scale(1./vtot); 
  }
  else
  {
    for (int jcl=0; jcl< nbClusters; jcl++)
    {
      static STensor43 J_ij;
      getJac_rs(clusterId,jcl,q0,q1,J_ij);
      static fullMatrix<double> J_ij_matrix;
      STensorOperation::fromSTensor43ToFullMatrix(J_ij,J_ij_matrix);
      for (int row=0; row<6; row++)
      {
        for (int col=0; col<6; col++)
        {
          dres(row, col+6*jcl) = J_ij_matrix(row,col);
        }
      }
    }
 
    STensor43 J_iK;
    STensorOperation::zero(J_iK); 
    const STensor43 &Di0 =  getClusterInteractionTensor(clusterId,nbClusters);
    const STensor43& depsEig1FramedepsFrame = q1->getConstRefToFramedEigenStraindStrain();
    STensorOperation::multSTensor43(Di0,depsEig1FramedepsFrame,J_iK);
    J_iK *= -1.;
    const STensor43 &Aeli = getClusterAverageStrainConcentrationTensor(clusterId);
    J_iK.axpy(-1.,Aeli);
    fullMatrix<double> J_iK_matrix;
    STensorOperation::fromSTensor43ToFullMatrix(J_iK,J_iK_matrix);
    for (int row=0; row<6; row++)
    {
      for (int col=0; col<6; col++)
      {
        dres(row, col+6*nbClusters) = J_iK_matrix(row,col);
      }
    }
  }  
};
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


IncrementalTangentTFA::IncrementalTangentTFA(int solverType, int dim, int correction, bool withFrame, bool polarization, int tag): ReductionTFA(solverType, dim, correction, withFrame, polarization, tag){}
IncrementalTangentTFA::~IncrementalTangentTFA(){}

void IncrementalTangentTFA::localConstitutive(const STensor3& F0, const STensor3& Fn, const ClusteringData *q0,  ClusteringData *q1)
{
  int nbClusters = _clusteringData->getTotalNumberOfClusters();
  //
  STensor3& eps1 = q1->getRefToMacroStrain();
  for  (int i=0; i<3; i++)
  {
    for (int j=0; j<3; j++)
    {
      eps1(i,j) = (Fn(i,j)+Fn(j,i))*0.5;
    }
  }
  eps1(0,0) -= 1.;
  eps1(1,1) -= 1.;
  eps1(2,2) -= 1.;
    
  // local constitutive behavior
  static STensor3 Isecond(1.);
  for (int icl =0; icl < nbClusters; icl++)
  {
    const materialLaw *law = getLawForCluster(icl); 
    static STensor3 F0i, F1i;
    F0i = q0->getConstRefToStrain(icl);
    F1i = q1->getConstRefToStrain(icl);
    F0i += Isecond;
    F1i += Isecond;
    STensor3& P1i = q1->getRefToStress(icl);
    STensor43& Tangenti = q1->getRefToTangent(icl);
    STensor43& depsEig1depsi = q1->getRefTodEigenStraindStrain(icl);
    STensor3& epsEig1i = q1->getRefToEigenStrain(icl);
    //
    const IPVariable* ipv0i = q0->getConstRefToIPv(icl);
    IPVariable* ipv1i = q1->getRefToIPv(icl);
    
    if(_clusterIncOri)
    {
      const fullVector<double>& euler_i = getClusterMeanFiberOrientation(icl);
      law->constitutiveTFA_incOri(F0i,F1i,euler_i,P1i,ipv0i,ipv1i,Tangenti,epsEig1i,depsEig1depsi);
    }
    else
    {
      law->constitutiveTFA(F0i,F1i,P1i,ipv0i,ipv1i,Tangenti,epsEig1i,depsEig1depsi);
    }
  }
};

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
IncrementalTangentTFAWithFrame::IncrementalTangentTFAWithFrame(int solverType, int dim, int correction, bool withFrame, bool polarization, int tag): ReductionTFA(solverType, dim, correction, withFrame, polarization, tag){}
IncrementalTangentTFAWithFrame::~IncrementalTangentTFAWithFrame(){}
void IncrementalTangentTFAWithFrame::localConstitutive(const STensor3& F0, const STensor3& Fn, const ClusteringData *q0,  ClusteringData *q1)
{
  //
  STensor3& eps1 = q1->getRefToMacroStrain();
  for  (int i=0; i<3; i++)
  {
    for (int j=0; j<3; j++)
    {
      eps1(i,j) = (Fn(i,j)+Fn(j,i))*0.5;
    }
  }
  eps1(0,0) -= 1.;
  eps1(1,1) -= 1.;
  eps1(2,2) -= 1.;
    
  // local constitutive behavior
  int nbClusters = _clusteringData->getTotalNumberOfClusters();
  static STensor3 Isecond(1.);
  
  STensor3& epsEig1Frame = q1->getRefToFrameEigenStrain();
  const STensor3& eps1Frame = q1->getConstRefToFrameStrain();
  STensor43& depsEig1FramedepsFrame = q1->getRefToFramedEigenStraindStrain();
  
  static STensor43 Ifourth(1.,1.);

  double vtot;
  vtot = totalVolume();
  
  STensor3 sig1Frame;
  
  for (int icl =0; icl < nbClusters; icl++)
  {
    const materialLaw *law = getLawForCluster(icl); 
    static STensor3 F0i, F1i;
    F0i = q0->getConstRefToStrain(icl);
    F1i = q1->getConstRefToStrain(icl);
    F0i += Isecond;
    F1i += Isecond;
    STensor3& P1i = q1->getRefToStress(icl);
    STensor43& Tangenti = q1->getRefToTangent(icl);
    STensor43& depsEig1depsi = q1->getRefTodEigenStraindStrain(icl);
    STensor3& epsEig1i = q1->getRefToEigenStrain(icl);
    //
    const IPVariable* ipv0i = q0->getConstRefToIPv(icl);
    IPVariable* ipv1i = q1->getRefToIPv(icl);
    law->constitutiveTFA(F0i,F1i,P1i,ipv0i,ipv1i,Tangenti,epsEig1i,depsEig1depsi);
  }
  q1->computeHomogenizedStrain(epsEig1Frame);
  q1->computeHomogenizedStress(sig1Frame);
  STensor43 CelHom, SelHom, CsecHom, SsecHom;
  elasticStiffnessHom(CelHom);
  STensorOperation::inverseSTensor43(CelHom,SelHom);
  secantStiffnessHom(q0,q1,CsecHom);
  STensorOperation::inverseSTensor43(CsecHom,SsecHom);
  STensorOperation::multSTensor43STensor3Add(SsecHom,sig1Frame,-1.,epsEig1Frame);
  depsEig1FramedepsFrame = SsecHom;
};



//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
IncrementalTangentHS::IncrementalTangentHS(int solverType, int dim, int correction, bool withFrame, bool polarization, int tag): ReductionTFA(solverType, dim, correction, withFrame, polarization, tag){}
IncrementalTangentHS::~IncrementalTangentHS(){}


void IncrementalTangentHS::localConstitutive(const STensor3& F0, const STensor3& Fn, const ClusteringData *q0,  ClusteringData *q1)
{
  int nbClusters = _clusteringData->getTotalNumberOfClusters();
  
  const STensor43& C0 = _clusteringData->getReferenceStiffness();
  const STensor43& C0iso = _clusteringData->getReferenceStiffnessIso();
  double G0 = C0iso(0,1,0,1);
  
  const STensor3& eps0 = q0->getConstRefToMacroStrain();
  STensor3& eps1 = q1->getRefToMacroStrain();
  for  (int i=0; i<3; i++)
  {
    for (int j=0; j<3; j++)
    {
      eps1(i,j) = (Fn(i,j)+Fn(j,i))*0.5;
    }
  }
  eps1(0,0) -= 1.;
  eps1(1,1) -= 1.;
  eps1(2,2) -= 1.;
  double epsEq = TFANumericTools::EquivalentStrain(eps1);
  STensor3 Deps;
  Deps = eps1;
  Deps -= eps0;
  double DepsEq = TFANumericTools::EquivalentStrain(Deps);
  
  const STensor3& P0hom = q0->getConstRefToMacroStress();
  STensor3& Phom = q1->getRefToMacroStress();
    
  // local constitutive behavior
  static STensor3 Isecond(1.);
  for (int icl =0; icl < nbClusters; icl++)
  {
    const materialLaw *law = getLawForCluster(icl); 
    static STensor3 F0i, F1i;
    F0i = q0->getConstRefToStrain(icl);
    F1i = q1->getConstRefToStrain(icl);
    F0i += Isecond;
    F1i += Isecond;
    STensor3& P1i = q1->getRefToStress(icl);
    STensor43& Tangenti = q1->getRefToTangent(icl);
    STensor43& depsEig1depsi = q1->getRefTodEigenStraindStrain(icl);
    STensor3& epsEig1i = q1->getRefToEigenStrain(icl);
    //
    const IPVariable* ipv0i = q0->getConstRefToIPv(icl);
    IPVariable* ipv1i = q1->getRefToIPv(icl);
    if(_clusterIncOri)
    {
      const fullVector<double>& euler_i = getClusterMeanFiberOrientation(icl);
      law->constitutiveTFA_incOri(F0i,F1i,euler_i,P1i,ipv0i,ipv1i,Tangenti,epsEig1i,depsEig1depsi);
    }
    else
    {
      law->constitutiveTFA(F0i,F1i,P1i,ipv0i,ipv1i,Tangenti,epsEig1i,depsEig1depsi);
    }
  }
  
  STensor3 DPhom;
  double DsigEqHom;  
  q1->computeHomogenizedStress(Phom);
  DPhom = Phom;
  DPhom -= P0hom;
  DsigEqHom = TFANumericTools::EquivalentStress(DPhom);
  
  double& Gel = q1->getRefToElasticShearModulus();
  Gel = G0;
  double& Gtan = q1->getRefToTangentShearModulus();
  Gtan = G0;
  if(DepsEq>1.e-10)
  {
    Gtan = DsigEqHom/(3.*DepsEq);
  }
  if(Gtan < 5.){Gtan=5.;};
};

IncrementalTangentPFA::IncrementalTangentPFA(int solverType, int dim, int correction, bool withFrame, bool polarization, int tag): ReductionTFA(solverType, dim, correction, withFrame, polarization, tag){}
IncrementalTangentPFA::~IncrementalTangentPFA(){}


void IncrementalTangentPFA::localConstitutive(const STensor3& F0, const STensor3& Fn, const ClusteringData *q0,  ClusteringData *q1)
{
  int nbClusters = _clusteringData->getTotalNumberOfClusters();
  
  const STensor43& C0 = _clusteringData->getReferenceStiffness();
  const STensor43& C0iso = _clusteringData->getReferenceStiffnessIso();
  double G0 = C0iso(0,1,0,1);
  double k0 = C0iso(0,0,0,0) - 4./3.*G0;
  
  //
  const STensor3& eps0 = q0->getConstRefToMacroStrain();
  STensor3& eps1 = q1->getRefToMacroStrain();
  for  (int i=0; i<3; i++)
  {
    for (int j=0; j<3; j++)
    {
      eps1(i,j) = (Fn(i,j)+Fn(j,i))*0.5;
    }
  }
  eps1(0,0) -= 1.;
  eps1(1,1) -= 1.;
  eps1(2,2) -= 1.;
  double epsEq = TFANumericTools::EquivalentStrain(eps1);
  STensor3 Deps;
  Deps = eps1;
  Deps -= eps0;
  double DepsEq = TFANumericTools::EquivalentStrain(Deps);
  
  const STensor3& P0hom = q0->getConstRefToMacroStress();
  STensor3& Phom = q1->getRefToMacroStress();
    
  // local constitutive behavior
  static STensor3 Isecond(1.);
  for (int icl =0; icl < nbClusters; icl++)
  {
    const materialLaw *law = getLawForCluster(icl); 
    static STensor3 F0i, F1i;
    F0i = q0->getConstRefToStrain(icl);
    F1i = q1->getConstRefToStrain(icl);
    F0i += Isecond;
    F1i += Isecond;
    STensor3& P1i = q1->getRefToStress(icl);
    STensor43& Tangenti = q1->getRefToTangent(icl);
    STensor43& depsEig1depsi = q1->getRefTodEigenStraindStrain(icl);
    STensor3& epsEig1i = q1->getRefToEigenStrain(icl);
    //
    const IPVariable* ipv0i = q0->getConstRefToIPv(icl);
    IPVariable* ipv1i = q1->getRefToIPv(icl); 
    if(_clusterIncOri)
    {
      const fullVector<double>& euler_i = getClusterMeanFiberOrientation(icl);
      law->constitutiveTFA_incOri(F0i,F1i,euler_i,P1i,ipv0i,ipv1i,Tangenti,epsEig1i,depsEig1depsi);
    }
    else
    {
      law->constitutiveTFA(F0i,F1i,P1i,ipv0i,ipv1i,Tangenti,epsEig1i,depsEig1depsi);
    }
  }
  
  STensor3 DPhom,DepsEigMacro;
  double DsigEqHom;
  
  q1->computeHomogenizedStress(Phom);
  DPhom = Phom;
  DPhom -= P0hom;
  DsigEqHom = TFANumericTools::EquivalentStress(DPhom);
  
  double& Gel = q1->getRefToElasticShearModulus();
  Gel = G0;
  double& Gtan = q1->getRefToTangentShearModulus();
  Gtan = G0;
  if(DepsEq>1.e-10)
  {
    Gtan = DsigEqHom/(3.*DepsEq);
  }
  if(Gtan < 5){Gtan=5;};
  
  STensor43& Ciso = q1->getRefToIsotropicStiffness();
  STensor43& Ciso_inv = q1->getRefToIsotropicStiffnessInverted();
  STensor43 Ivol, Idev;
  STensorOperation::sphericalfunction(Ivol);
  STensorOperation::deviatorfunction(Idev);
  Ciso = 3.*k0*Ivol;
  Ciso += 2.*Gtan*Idev;
  STensorOperation::inverseSTensor43(Ciso,Ciso_inv);
  
  //STensor43& Caniso = q1->getRefToAnisotropicStiffness();
  //STensor43& Caniso_inv = q1->getRefToAnisotropicStiffnessInverted();
};


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
HierarchicalReductionTFA::HierarchicalReductionTFA(int solverType, int dim, int correction, bool withFrame, bool polarization, int tag): 
                                                                     ReductionTFA(solverType, dim, 0, false, polarization, tag), _sysHierarchical(NULL) {}
                                                                     
HierarchicalReductionTFA::~HierarchicalReductionTFA()
{
  if (_ownClusteringData)
  {
    _ownClusteringData = false;
    if (_clusteringData != NULL)
    {
      delete _clusteringData; _clusteringData = NULL;
    }
  }
  if (_sys != NULL)
  {
    delete _sys;
    _sys = NULL;
  }
  if (_sysHierarchical != NULL)
  {
    delete _sysHierarchical;
    _sysHierarchical = NULL;
  }
  if (!(_sysLevel1.empty()))
  {
    _sysLevel1.clear();
  }
  if (!(_unknownLevel1.empty()))
  {
    _unknownLevel1.clear();
  }
  if (!(_fixedDofLevel1.empty()))
  {
    _fixedDofLevel1.clear();
  }
  _correction = false;
  _withFrame = false;
  _polarization = false;
  _clusterIncOri = false;
}

void HierarchicalReductionTFA::initializeClusterTFAMaterialMapLevel0(const std::map<int,materialLaw*> &maplawLv0, int tfaLawID)
{
  std::map<int,materialLaw*>::const_iterator itLawTFA = maplawLv0.find(tfaLawID);
  materialLaw* lawTFA = itLawTFA->second->getNonLinearSolverMaterialLaw();

  std::map<int,materialLaw*> mapTFALawLv0;
  
  const std::map<int, int>& _matlawnb = _clusteringData->getClusterMaterialMap();
  for(std::map<int,int>::const_iterator itcluster=_matlawnb.begin(); itcluster!=_matlawnb.end(); itcluster++)
  {
    int clusternb=itcluster->first;
    int matlawnb=itcluster->second;
    _matlawptr.insert(std::pair<int, const materialLaw*>(clusternb,lawTFA));
  }  
}

void HierarchicalReductionTFA::initializeClusterMaterialMapLevel1(const std::map<int,materialLaw*> &maplawLv0)
{
  const std::map<int,std::map<int, int>>& _matlawnbLv1 = _clusteringData->getClusterMaterialMapLevel1();
  for(std::map<int,std::map<int, int>>::const_iterator itLv0=_matlawnbLv1.begin(); itLv0!=_matlawnbLv1.end(); itLv0++)
  {
    int clusterLv0=itLv0->first;
    std::map<int, int> matlawnbOnLv1 = itLv0->second;
    
    std::map<int, const materialLaw*>& matlawptrOnLevel1 = _matlawptrLevel1[clusterLv0];
    
    for(std::map<int, int>::const_iterator itLv1=matlawnbOnLv1.begin(); itLv1!=matlawnbOnLv1.end(); itLv1++)
    { 
      int clusterLv1=itLv1->first;      
      int matlawnb=itLv1->second;      
      bool findlaw=false;
      for(std::map<int,materialLaw*>::const_iterator itLaws = maplawLv0.begin(); itLaws != maplawLv0.end(); ++itLaws)
      {
        int numLaw = itLaws->first;
        if(numLaw == matlawnb){
          findlaw=true;
          itLaws->second->initLaws(maplawLv0);
          matlawptrOnLevel1.insert(std::pair<int, const materialLaw*>(clusterLv1,itLaws->second->getConstNonLinearSolverMaterialLaw()));
          break;
        }
      }
      if(!findlaw) Msg::Error("The law is not initialize for cluster level 0, level 1: %d, %d, and material law number %d",clusterLv0,clusterLv1,matlawnb);
    }    
  }
}

const materialLaw* HierarchicalReductionTFA::getMaterialLaw(std::vector<int> clIDsHierarchical) const
{
  int level = clIDsHierarchical.size();
  
  if(level == 1)
  {
    int clLv0 = clIDsHierarchical[0];
    std::map<int, const materialLaw*>::const_iterator itMat =  _matlawptr.find(clLv0);
    if (itMat == _matlawptr.end())
    {
      Msg::Error("material is not found for cluster %d", clLv0);
      Msg::Exit(0);
    }
    else
    {
      return itMat->second;
    }
    return NULL;
  }
  else if(level == 2)
  {
    int clLv0 = clIDsHierarchical[0];
    int clLv1 = clIDsHierarchical[1];
    
    std::map<int,std::map<int, const materialLaw*>>::const_iterator itLv0 = _matlawptrLevel1.find(clLv0);
    std::map<int, const materialLaw*> _matlawptrOnLevel1;
    if (itLv0 == _matlawptrLevel1.end())
    {
      Msg::Error("material is not found for cluster level 0: %d,%d", clLv0);
      Msg::Exit(0);
    }
    else
    {
      _matlawptrOnLevel1 = itLv0->second;
    }
    std::map<int, const materialLaw*>::const_iterator itMatLv1 = _matlawptrOnLevel1.find(clLv1);
    if (itMatLv1 == _matlawptrOnLevel1.end())
    {
      Msg::Error("material is not found for cluster level 0, level 1: %d,%d", clLv0,clLv1);
      Msg::Exit(0);
    }
    else
    {
      return itMatLv1->second;
    }
  }
  return NULL;
};



const materialLaw* HierarchicalReductionTFA::getLawForCluster(std::vector<int> clIDsHierarchical) const
{
  int level = clIDsHierarchical.size();
  if(level == 1)
  {
    int clLv0 = clIDsHierarchical[0];
    std::map<int,const materialLaw *>::const_iterator itMat=_matlawptr.find(clLv0);
    #ifdef _DEBUG    
    if (itMat == _matlawptr.end())
    {
      Msg::Error("material is not found for cluster %d", clLv0);
      Msg::Exit(0);
    }    
    #endif
    return itMat->second;
  }
  else if(level == 2)
  {
    int clLv0 = clIDsHierarchical[0];
    int clLv1 = clIDsHierarchical[1];
    
    std::map<int,std::map<int, const materialLaw*>>::const_iterator itLv0 = _matlawptrLevel1.find(clLv0);
    std::map<int, const materialLaw*> _matlawptrOnLevel1;
    if (itLv0 == _matlawptrLevel1.end())
    {
      Msg::Error("material is not found for cluster level 0: %d,%d", clLv0);
      Msg::Exit(0);
    }
    else
    {
      _matlawptrOnLevel1 = itLv0->second;
    }
    std::map<int, const materialLaw*>::const_iterator itMatLv1 = _matlawptrOnLevel1.find(clLv1);
    #ifdef _DEBUG
    if (itMatLv1 == _matlawptrOnLevel1.end())
    {
      Msg::Error("material is not found for cluster level 0, level 1: %d,%d", clLv0,clLv1);
      Msg::Exit(0);
    }
    #endif
    return itMatLv1->second;
  }
};




void HierarchicalReductionTFA::evaluate(
            const STensor3& F0,         // initial deformation gradient (input @ time n)
            const STensor3& Fn,         // updated deformation gradient (input @ time n+1)
            STensor3 &P,                // updated 1st Piola-Kirchhoff stress tensor (output)
            const ClusteringData *q0,       // array of initial internal variable
            const std::map<int,ClusteringData*> q0Level1,
            ClusteringData *q1,             // updated array of internal variable (in ipvcur on output),
            std::map<int,ClusteringData*> q1Level1,
            STensor43 &Tangent,         // constitutive tangents (output)
            const bool stiff,          // if true compute the tangents
            STensor43* elasticTangent)
{
  _nbLevels = _clusteringData->getNumberOfLevels();
  int maxIter = 150;
  double tol = 1e-6;
  double absTol = 1e-12;
  bool messageActive=false;
  bool ok = false;
  if(_nbLevels == 1)
  {
    ok =  systemSolve(F0, Fn, q0, q1, P, stiff, Tangent, maxIter, tol, absTol, messageActive);
  }
  else if(_nbLevels == 2)
  {
    ok =  systemSolveHierarchical(F0, Fn, q0, q0Level1, q1, q1Level1, P, stiff, Tangent, maxIter, tol, absTol, messageActive);
  }   
  //else if ....           
  if (!ok)
  {
    // to force sub-stepping at macroscopic solver
    P(0,0) = P(1,1) = P(2,2) = sqrt(-1);
  };
};

bool HierarchicalReductionTFA::systemSolveHierarchical(const STensor3& F0, const STensor3& F1,
                                                       const ClusteringData *q0,       // array of initial internal variable
                                                       const std::map<int,ClusteringData*> q0Level1,
                                                       ClusteringData *q1,             // updated array of internal variable (in ipvcur on output),
                                                       std::map<int,ClusteringData*> q1Level1,
                                                       STensor3& P, bool stiff, STensor43& L, 
                                                       int maxIter, double tol, double absTol, 
                                                       bool messageActive)
{
  if (!_isInitialized)
  {
    initSolve(q0,q1,_sys);
  };
  
  // zero solution first
  initialZeroSolution(_sys);
  setInitialState(F0,F1);
  updateFieldFromUnknown(q0,q1);
  //estimate strains at each clusters and evaluate constitutive relation

  //localConstitutive(F0,F1,q0,q1);
  STensor3& epsMacro1 = q1->getRefToMacroStrain();
  for  (int i=0; i<3; i++)
  {
    for (int j=0; j<3; j++)
    {
      epsMacro1(i,j) = (F1(i,j)+F1(j,i))*0.5;
    }
  }
  epsMacro1(0,0) -= 1.;
  epsMacro1(1,1) -= 1.;
  epsMacro1(2,2) -= 1.;
  
  int nbClLv0 = _clusteringData->getTotalNumberOfClusters();
  
  _level = 0;
  
  static STensor3 Isecond(1.);
  
  STensor3 Deps(F1);
  Deps -= F0;
  
  for(std::map<int,ClusteringData*>::const_iterator itLv0=q0Level1.begin(); itLv0!=q0Level1.end(); itLv0++)
  {
    _level=1;
    int clLv0 = itLv0->first;
    std::vector<int> clIDsHierarchical;
    clIDsHierarchical.push_back(clLv0);
    
    STensor3 Depsi_lv0;
    const STensor43 &Ai_lv0 = getClusterAverageStrainConcentrationTensorHierarchical(clIDsHierarchical);
    STensorOperation::multSTensor43STensor3(Ai_lv0,Deps,Depsi_lv0);
    
    const STensor3& eps0i_lv0 = q0->getConstRefToStrain(clLv0);
    STensor3& eps1i_lv0 = q1->getRefToStrain(clLv0);
    eps1i_lv0 = eps0i_lv0;
    eps1i_lv0 += Depsi_lv0;
    
    STensor3 FMacro0_lv1(eps0i_lv0);
    FMacro0_lv1 += Isecond;
    STensor3 FMacro1_lv1(eps1i_lv0);
    FMacro1_lv1 += Isecond;
           
    ClusteringData* q0_lv1 = itLv0->second;
    ClusteringData* q1_lv1 = q1Level1[clLv0];
    
    q0_lv1->getRefToMacroStrain() = eps0i_lv0;    
    q1_lv1->getRefToMacroStrain() = eps1i_lv0;
    
    STensor3& PMacro1_lv1 = q1->getRefToStress(clLv0);
    STensor43& TangentMacro_lv1 = q1->getRefToTangent(clLv0);
    STensor43& elasticTangenti_lv0 = q1_lv1->getRefToMacroElasticTangent();
    elasticStiffnessHomHierarchical(clIDsHierarchical, elasticTangenti_lv0);
    STensor43 elasticCompliancei_lv0;
    STensorOperation::inverseSTensor43(elasticTangenti_lv0,elasticCompliancei_lv0);
        
    bool ok =  systemSolveBottomLevel(FMacro0_lv1, FMacro1_lv1, q0_lv1, q1_lv1, PMacro1_lv1, stiff, TangentMacro_lv1, maxIter, tol, absTol, messageActive);
    q1_lv1->getRefToMacroStress() = PMacro1_lv1;
    q1_lv1->getRefToMacroTangent() = TangentMacro_lv1;
    
    STensor3& epsEig1i_lv0 = q1->getRefToEigenStrain(clLv0);
    STensor43& depsEig1depsi_lv0 = q1->getRefTodEigenStraindStrain(clLv0);
    epsEig1i_lv0 = eps1i_lv0;
    STensorOperation::multSTensor43STensor3Add(elasticCompliancei_lv0,PMacro1_lv1,-1.,epsEig1i_lv0);
    STensorOperation::unity(depsEig1depsi_lv0);
    STensorOperation::multSTensor43Add(elasticCompliancei_lv0,TangentMacro_lv1,-1.,depsEig1depsi_lv0);
  }

  if (messageActive)
  {
    F1.print("------------------------");
  }
  int iter = 0;
  bool converged = true;
  bool stiffnessEstimated = false;
  double norm0 = -1;
  double t = Cpu();
  while (true)
  {    //
    assembleRes(q0,q1);
    //
    double resNorm = normInfRightHandSide(_sys);
    //printf("resNorm0000000 = %.4e\n",resNorm);
    if (iter == 0)
    {
      norm0 = resNorm;
    }
    if (messageActive)
    {
      printf("iter: %d res = %e reltol %e\n",iter,resNorm,resNorm/norm0);
    }
    if (STensorOperation::isnan(resNorm))
    {
      converged = false;
      Msg::Warning("norm is nan, solve is not converge!!!");
      break;
    }
    //
    if (resNorm < absTol)
    {
      if (messageActive)
      {
        printf("converged by absolute tolerance = %.4e\n",absTol);
      }
      break;
    };
    if (resNorm > tol*norm0)
    {
      stiffnessEstimated = true;
      assembleDresDu(q0,q1);
      int isSolved =  systemSolve(_sys);
      if (isSolved==0)
      {
        if (messageActive)
        {
          printf("solver is not successful !!!\n");
        }
        converged = false;
        break;
      }
      updateFieldFromUnknown(q0,q1);
      
      // instead of calling local constitutive for the subdomains, a TFA system for the sub-subdomains is solved in each subdomain  //
      // variables on level 0 follow from TFA homogenization procedure on level 1 //
      //localConstitutiveLevel0(F0,F1,q0,q1);      
      for(std::map<int,ClusteringData*>::const_iterator itLv0=q0Level1.begin(); itLv0!=q0Level1.end(); itLv0++)
      {
        _level=1;
        int clLv0 = itLv0->first;
        
        std::vector<int> clIDsHierarchical;
        clIDsHierarchical.push_back(clLv0);
        
        const STensor3& eps0i_lv0 = q0->getConstRefToStrain(clLv0);
        const STensor3& eps1i_lv0 = q1->getConstRefToStrain(clLv0);
        
        STensor3 FMacro0_lv1 = eps0i_lv0;
        FMacro0_lv1 += Isecond;
        STensor3 FMacro1_lv1 = eps1i_lv0;
        FMacro1_lv1 += Isecond;
    
        ClusteringData* q0_lv1 = itLv0->second;
        ClusteringData* q1_lv1 = q1Level1[clLv0];
    
        q0_lv1->getRefToMacroStrain() = eps0i_lv0;    
        q1_lv1->getRefToMacroStrain() = eps1i_lv0;
    
        STensor3& PMacro1_lv1 = q1->getRefToStress(clLv0);
        STensor43& TangentMacro_lv1 = q1->getRefToTangent(clLv0);
        const STensor43& elasticTangenti_lv0 = q1_lv1->getConstRefToMacroElasticTangent();
        STensor43 elasticCompliancei_lv0;
        STensorOperation::inverseSTensor43(elasticTangenti_lv0,elasticCompliancei_lv0);
        
        bool ok =  systemSolveBottomLevel(FMacro0_lv1, FMacro1_lv1, q0_lv1, q1_lv1, PMacro1_lv1, stiff, TangentMacro_lv1, maxIter, tol, absTol, messageActive);
        q1_lv1->getRefToMacroStress() = PMacro1_lv1;
        q1_lv1->getRefToMacroTangent() = TangentMacro_lv1;
    
        STensor3& epsEig1i_lv0 = q1->getRefToEigenStrain(clLv0);
        STensor43& depsEig1depsi_lv0 = q1->getRefTodEigenStraindStrain(clLv0);
        epsEig1i_lv0 = eps1i_lv0;
        STensorOperation::multSTensor43STensor3Add(elasticCompliancei_lv0,PMacro1_lv1,-1.,epsEig1i_lv0);
        STensorOperation::unity(depsEig1depsi_lv0);
        STensorOperation::multSTensor43Add(elasticCompliancei_lv0,TangentMacro_lv1,-1.,depsEig1depsi_lv0);
      }
    }
    else
    {
      if (messageActive)
      {
        printf("converged by relative tolerance = %.4e, execution time = %.5f s\n",tol,Cpu()-t);
      }
      break;
    }

    iter++;
    if (iter > maxIter)
    {
      converged = false;
      Msg::Warning("maximum number of iterations is reached, solve is not converge!!!");
      F1.print("Fcur");
      break;
    };
  };
  //printf("it = %d \n",iter);

  if (converged)
  {
    // get Stress
    q1->computeHomogenizedStress(P);
    STensor3& PHom = q1->getRefToMacroStress();
    PHom = P;
    
    if (stiff)
    {
      if (!stiffnessEstimated)
      {
        assembleDresDu(q0,q1);
      }
      bool succ = tangentSolve(_sys,_unknown,q0,q1,L);
      q1->getRefToMacroTangent() = L;
    }
    //
    return true;
  }
  else
  {
    return false;
  }
};

void HierarchicalReductionTFA::assembleResHierarchical(const ClusteringData *q0,  const ClusteringData *q1)
{
  std::vector<int> clHostIDs;
  clHostIDs = q0->upperClusterIDs;
  
  //std::map<Dof, int> unknownLocal;  
  //int nbUpperLevels = clHostIDs.size();
  //if(nbUpperLevels == 1)
  //{
    //int clLv0 = clHostIDs[0];
    //unknownLocal = _unknownLevel1[clLv0];
    //nonLinearSystemPETSc<double>* &sysLocal = _sysLevel1[clLv0];
  //}
  //else
  //{
    //Msg::Error("Function assembleResHierarchical not yet implemented for more than two levels !!!");
  //}

  zeroRHS(_sysHierarchical);
  //zeroRHS(sysLocal);
  int nbClusters = q0->clustersNb;
  static fullVector<double> r;
  
  for (int icl = 0; icl < nbClusters; icl++)
  {
    getLinearTermHierarchical(icl,q0,q1,r);
    std::vector<Dof> R;
    getKeys(icl,R);
    for (int j=0; j<R.size(); j++)
    {
      std::map<Dof,int>::iterator it =  _unknownHierarchical.find(R[j]);
      //std::map<Dof,int>::iterator it =  unknownLocal.find(R[j]);
      if (it != _unknownHierarchical.end())
      //if (it != unknownLocal.end())
      {
        int row = it->second;
        addToRHS(row,r(j),_sysHierarchical);
        //addToRHS(row,r(j),sysLocal);
      }
    }
  }
};
void HierarchicalReductionTFA::assembleDresDuHierarchical(const ClusteringData *q0,  const ClusteringData *q1)
{
  //Msg::Info("calling assembleDresDuHierarchical");
  
  std::vector<int> clHostIDs;
  clHostIDs = q0->upperClusterIDs;
  
  //std::map<Dof, int> unknownLocal; 
  //int nbUpperLevels = clHostIDs.size();
  //if(nbUpperLevels == 1)
  //{
    //int clLv0 = clHostIDs[0];
    //fixedDofLocal = _fixedDofLevel1[clLv0];
    //unknownLocal = _unknownLevel1[clLv0];
    //nonLinearSystemPETSc<double>* &sysLocal = _sysLevel1[clLv0];
  //}
  //else
  //{
    //Msg::Error("Function assembleDresDuHierarchical not yet implemented for more than two levels !!!");
  //}
    
  zeroMatrix(_sysHierarchical);
  //zeroMatrix(sysLocal);
  int nbClusters = q0->clustersNb;
  static fullMatrix<double> Ke;
  std::vector<int> othersId;
    
  for (int icl = 0; icl < nbClusters; icl++)
  {
    getBiLinearTermHierarchical(icl,q0,q1,othersId,Ke);
    //Ke.print("Ke");
    //
    std::vector<Dof> R, C;
    getKeys(icl,R);
    for (int jcl=0; jcl< othersId.size(); jcl++)
    {
      getKeys(othersId[jcl],C);
    }
    for (int iR=0; iR < R.size(); iR++)
    {
      std::map<Dof,int>::iterator itR =  _unknownHierarchical.find(R[iR]);
      //std::map<Dof,int>::iterator itR =  unknownLocal.find(R[iR]);
      if (itR != _unknownHierarchical.end())
      //if (itR != unknownLocal.end())
      {
        int row = itR->second;
        for (int iC =0; iC < C.size(); iC++)
        {
          std::map<Dof,int>::iterator itC =  _unknownHierarchical.find(C[iC]);
          //std::map<Dof,int>::iterator itC =  unknownLocal.find(C[iC]);
          if (itC != _unknownHierarchical.end())
          //if (itC != unknownLocal.end())
          {
            int col = itC->second;
            addToMatrix(row,col,Ke(iR,iC),_sysHierarchical);
            //addToMatrix(row,col,Ke(iR,iC),sysLocal);
          }
        }
      }
    }
  }
};

void HierarchicalReductionTFA::updateFieldFromUnknownHierarchical(const ClusteringData *q0,  ClusteringData *q1)
{
  std::vector<int> clHostIDs;
  clHostIDs = q0->upperClusterIDs;
  //nonLinearSystemPETSc<double>* sysLocal;
  //std::map<Dof, int> unknownLocal;
  //std::map<Dof, double> fixedDofLocal;
  
  //int nbUpperLevels = clHostIDs.size();
  //if(nbUpperLevels == 1)
  //{
    //int clLv0 = clHostIDs[0];
    //fixedDofLocal = _fixedDofLevel1[clLv0];
    //unknownLocal = _unknownLevel1[clLv0];
    //sysLocal = _sysLevel1[clLv0];
  //}
  //else
  //{
    //Msg::Error("Function updateFieldFromUnknownHierarchical not yet implemented for more than two levels !!!");
  //}

  int nbClusters = q0->clustersNb;
  for (int icl = 0; icl < nbClusters; icl++)
  {
    std::vector<Dof> R;
    getKeys(icl,R);
    int Rsize = R.size();
    fullVector<double> val(Rsize);
    for (int j=0; j< Rsize; j++)
    {
      std::map<Dof,int>::iterator itR =  _unknownHierarchical.find(R[j]);
      //std::map<Dof,int>::iterator itR =  unknownLocal.find(R[j]);
      if (itR != _unknownHierarchical.end())
      //if (itR != unknownLocal.end())
      {
        int row = itR->second;
        val(j) = getFromSolution(row,_sysHierarchical);
        //val(j) = getFromSolution(row,sysLocal);
      }
      else
      {
        val(j) = _fixedDofHierarchical[R[j]];
        //val(j) = fixedDofLocal[R[j]];
      }
    }
    updateLocalFieldFromDof(icl,val,q0,q1);
  };
};


nonLinearSystemPETSc<double>*& HierarchicalReductionTFA::getRefToNLSystem(std::vector<int> clHostIDs)
{
  int nbUpperLevels = clHostIDs.size();
  if(nbUpperLevels == 1)
  {
    int clLv0 = clHostIDs[0];
    std::map<int, nonLinearSystemPETSc<double>*>::iterator NLsystemFind = _sysLevel1.find(clLv0);    
    return NLsystemFind->second;
  }
  else
  {
    Msg::Error("Function getRefToNLSystem not yet implemented for more than two levels !!!");
  }
}

const bool& HierarchicalReductionTFA::getConstRefToInitialized(std::vector<int> clHostIDs)
{
  int nbUpperLevels = clHostIDs.size();
  if(nbUpperLevels == 1)
  {
    int clLv0 = clHostIDs[0];
    std::map<int, bool>::const_iterator isInitializedFind = _isInitializedLevel1.find(clLv0);    
    return isInitializedFind->second;
  }
  else
  {
    Msg::Error("Function getConstRefToInitialized not yet implemented for more than two levels !!!");
  }
}

bool& HierarchicalReductionTFA::getRefToInitialized(std::vector<int> clHostIDs)
{
  int nbUpperLevels = clHostIDs.size();
  if(nbUpperLevels == 1)
  {
    int clLv0 = clHostIDs[0];
    std::map<int, bool>::iterator isInitializedFind = _isInitializedLevel1.find(clLv0);    
    return isInitializedFind->second;
  }
  else
  {
    Msg::Error("Function getRefToInitialized not yet implemented for more than two levels !!!");
  }
}

std::map<Dof, int>& HierarchicalReductionTFA::getRefToUnknown(std::vector<int> clHostIDs)
{
  int nbUpperLevels = clHostIDs.size();
  if(nbUpperLevels == 1)
  {
    int clLv0 = clHostIDs[0];
    std::map<int,std::map<Dof, int>>::iterator unknownFind = _unknownLevel1.find(clLv0);    
    return unknownFind->second;
  }
  else
  {
    Msg::Error("Function getRefToUnknown not yet implemented for more than two levels !!!");
  }
}

const std::map<Dof, double>& HierarchicalReductionTFA::getConstRefToFixedDof(std::vector<int> clHostIDs)
{
  int nbUpperLevels = clHostIDs.size();
  if(nbUpperLevels == 1)
  {
    int clLv0 = clHostIDs[0];
    std::map<int,std::map<Dof, double>>::iterator fixedDofFind = _fixedDofLevel1.find(clLv0);    
    return fixedDofFind->second;
  }
  else
  {
    Msg::Error("Function getConstRefToFixedDof not yet implemented for more than two levels !!!");
  }
}

std::map<Dof, double>& HierarchicalReductionTFA::getRefToFixedDof(std::vector<int> clHostIDs)
{
  int nbUpperLevels = clHostIDs.size();
  if(nbUpperLevels == 1)
  {
    int clLv0 = clHostIDs[0];
    std::map<int,std::map<Dof, double>>::iterator fixedDofFind = _fixedDofLevel1.find(clLv0);    
    return fixedDofFind->second;
  }
  else
  {
    Msg::Error("Function getRefToFixedDof not yet implemented for more than two levels !!!");
  }
}


bool HierarchicalReductionTFA::systemSolveBottomLevel(const STensor3& F0, const STensor3& F1,
                                                      const ClusteringData* q0, ClusteringData* q1, 
                                                      STensor3& P, bool stiff, STensor43& L, 
                                                      int maxIter, double tol, double absTol, 
                                                      bool messageActive)
{
  std::vector<int> clHostIDs = q0->upperClusterIDs;
  //nonLinearSystemPETSc<double>* sysLocal = getRefToNLSystem(clHostIDs);
  //std::map<Dof, int>& unknownLocal = getRefToUnknown(clHostIDs);
  //const bool& isInitializedLocal = getConstRefToInitialized(clHostIDs);
  //if(!isInitializedLocal)
  //{
    //sysLocal = NULL;
    //initSolveHierarchical(q0,q1,sysLocal);
  //}

  initSolveHierarchical(q0,q1,_sysHierarchical);
  initialZeroSolution(_sysHierarchical);
  //initialZeroSolution(sysLocal);
  setInitialStateHierarchical(q0,F0,F1);
  updateFieldFromUnknownHierarchical(q0,q1);
  localConstitutive(F0,F1,q0,q1);

  if (messageActive)
  {
    F1.print("------------------------");
  }
  int iter = 0;
  bool converged = true;
  bool stiffnessEstimated = false;
  double norm0 = -1;
  double t = Cpu();
  while (true)
  {
    assembleResHierarchical(q0,q1);
    //
    double resNorm = normInfRightHandSide(_sysHierarchical);
    //double resNorm = normInfRightHandSide(sysLocal);
    //printf("resNorm0000000 = %.4e\n",resNorm);
    if (iter == 0)
    {
      norm0 = resNorm;
    }
    if (messageActive)
    {
      printf("iter: %d res = %e reltol %e\n",iter,resNorm,resNorm/norm0);
    }
    if (STensorOperation::isnan(resNorm))
    {
      converged = false;
      Msg::Warning("norm is nan, solve is not converge!!!");
      break;
    }
    //
    if (resNorm < absTol)
    {
      if (messageActive)
      {
        printf("converged by absolute tolerance = %.4e\n",absTol);
      }
      break;
    };
    if (resNorm > tol*norm0)
    {
      stiffnessEstimated = true;
      assembleDresDuHierarchical(q0,q1);
      int isSolved =  systemSolve(_sysHierarchical);
      //int isSolved =  systemSolve(sysLocal);
      if (isSolved==0)
      {
        if (messageActive)
        {
          printf("solver is not successful !!!\n");
        }
        converged = false;
        break;
      }
      updateFieldFromUnknownHierarchical(q0,q1);
      // constitutive
      localConstitutive(F0,F1,q0,q1);
    }
    else
    {
      if (messageActive)
      {
        printf("converged by relative tolerance = %.4e, execution time = %.5f s\n",tol,Cpu()-t);
      }
      break;
    }

    iter++;
    if (iter > maxIter)
    {
      converged = false;
      Msg::Warning("maximum number of iterations is reached, solve is not converge!!!");
      F1.print("Fcur");
      break;
    };
  };
  //printf("it = %d \n",iter);

  if (converged)
  {
    // get Stress
    q1->computeHomogenizedStress(P);
    STensor3& PHom = q1->getRefToMacroStress();
    PHom = P;
    //STensor43& CtanHom = q1->getRefToMacroTangent();
    if (stiff)
    {
      if (!stiffnessEstimated)
      {
        assembleDresDuHierarchical(q0,q1);
      }
      bool succ = tangentSolve(_sysHierarchical,_unknownHierarchical,q0,q1,L);
      //bool succ = tangentSolve(sysLocal,unknownLocal,q0,q1,L);
    }
    //
    return true;
  }
  else
  {
    return false;
  }
};

void HierarchicalReductionTFA::initSolveHierarchical(const ClusteringData *q0,  const ClusteringData *q1, nonLinearSystemPETSc<double>* &NLsystem)
{
  std::vector<int> clHostIDs;
  clHostIDs = q0->upperClusterIDs;
  //std::map<Dof, int>& unknownLocal = getRefToUnknown(clHostIDs);
  //bool& isInitializedLocal = getRefToInitialized(clHostIDs);
  //isInitializedLocal = true;

  fixDofsHierarchical(q0);
  numberDofHierarchical(q0);
  //allocate system
  int numR = _unknownHierarchical.size();
  //int numR = unknownLocal.size();
  //
  createSystem(numR,NLsystem);
  preallocateSystemHierarchical(q0,q1,NLsystem);
  //printf("done initializing, size of unknowns = %d \n",numR);
};

void HierarchicalReductionTFA::fixDofsHierarchical(const ClusteringData *q0)
{
  std::vector<int> clHostIDs;
  clHostIDs = q0->upperClusterIDs;

  //std::map<Dof, double> fixedDofLocalSet;

  if (getDim() == 2)
  {
    _fixedDofHierarchical.clear();
    int nbClusters = q0->clustersNb;
    for (int clusterId=0; clusterId< nbClusters; clusterId++)
    {
      _fixedDofHierarchical[Dof(clusterId, Dof::createTypeWithTwoInts(2,_tag))] = 0.;
      _fixedDofHierarchical[Dof(clusterId, Dof::createTypeWithTwoInts(4,_tag))] = 0.;
      _fixedDofHierarchical[Dof(clusterId, Dof::createTypeWithTwoInts(5,_tag))] = 0.;
      //fixedDofLocalSet[Dof(clusterId, Dof::createTypeWithTwoInts(2,_tag))] = 0.;
      //fixedDofLocalSet[Dof(clusterId, Dof::createTypeWithTwoInts(4,_tag))] = 0.;
      //fixedDofLocalSet[Dof(clusterId, Dof::createTypeWithTwoInts(5,_tag))] = 0.;
    }
  }
  
  //int nbUpperLevels = clHostIDs.size();
  //if(nbUpperLevels == 1)
  //{
    //int clLv0 = clHostIDs[0];
    //_fixedDofLevel1[clLv0] = fixedDofLocalSet;
  //}
  //else
  //{
    //Msg::Error("Function fixDofsHierarchical not yet implemented for more than two levels !!!");
  //}  
};
void HierarchicalReductionTFA::numberDofHierarchical(const ClusteringData *q0)
{
  std::vector<int> clHostIDs;
  clHostIDs = q0->upperClusterIDs;
  int nbUpperLevels = clHostIDs.size();
  //std::map<Dof, int>& unknownLocal = getRefToUnknown(clHostIDs);
  //std::map<Dof, int> unknownLocalSet;
  //const std::map<Dof, double>& fixedDofLocal = getConstRefToFixedDof(clHostIDs);

  _unknownHierarchical.clear();
  //unknownLocalSet.clear();
  int nbClusters = q0->clustersNb;
  //Msg::Info("dof numbering, number of clusters = %d",nbClusters);
  for (int icl =0; icl < nbClusters; icl ++)
  {
    std::vector<Dof> R;
    getKeys(icl,R);
    for (int j=0; j< R.size(); j++)
    {
      Dof& key = R[j];
      if (_fixedDofHierarchical.find(key) == _fixedDofHierarchical.end())
      //if (fixedDofLocal.find(key) == fixedDofLocal.end())
      {
        if(_unknownHierarchical.find(key) == _unknownHierarchical.end())
        //if(unknownLocal.find(key) == unknownLocal.end())
        {
          std::size_t size = _unknownHierarchical.size();
          _unknownHierarchical[key] = size;
          //std::size_t size = unknownLocal.size();
          //unknownLocal[key] = size;
        }
      }
    }
  }
  
  //unknownLocal = unknownLocalSet;
  
  //Msg::Info("done dof numbering, _fixedDofHierarchical = %d number of DOFS = %d",_fixedDofHierarchical.size(),_unknownHierarchical.size());
};


void HierarchicalReductionTFA::setInitialStateHierarchical(const ClusteringData *q0, const STensor3& F0, const STensor3& Fn)
{
  std::vector<int> clHostIDs;
  clHostIDs = q0->upperClusterIDs;
  //nonLinearSystemPETSc<double>*& sysLocal = getRefToNLSystem(clHostIDs);
  //std::map<Dof, int>& unknownLocal = getRefToUnknown(clHostIDs);

  int nbClusters = q0->clustersNb;
  for (int icl = 0; icl < nbClusters; icl++)
  {
    std::vector<Dof> R;
    getKeys(icl,R);
    int Rsize = R.size();
    fullVector<double> val(Rsize);
    
    std::vector<int> clIDsHierarchical = q0->upperClusterIDs;
    clIDsHierarchical.push_back(icl);    
    getInitialDofValueHierarchical(clIDsHierarchical,F0,Fn,val);
    
    for (int j=0; j< Rsize; j++)
    {
      std::map<Dof,int>::iterator itR =  _unknownHierarchical.find(R[j]);
      //std::map<Dof,int>::iterator itR =  unknownLocal.find(R[j]);
      if (itR != _unknownHierarchical.end())
      //if (itR != unknownLocal.end())
      {
        int row = itR->second;
        addToSolution(row,val(j),_sysHierarchical);
        //addToSolution(row,val(j),sysLocal);
      }
    }
  }
};
void HierarchicalReductionTFA::getInitialDofValueHierarchical(std::vector<int> clIDsHierarchical, const STensor3& F0, const STensor3& Fn, fullVector<double>& val) const
{
  //val.resize(6,true); // set to zero  
  STensor3 Deps;
  Deps = Fn;
  Deps -= F0;
  const STensor43 &Ai = getClusterAverageStrainConcentrationTensorHierarchical(clIDsHierarchical);
  static STensor3 temp;
  STensorOperation::multSTensor43STensor3(Ai,Deps,temp);
  STensorOperation::fromSTensor3ToFullVector(temp,val);
};

void HierarchicalReductionTFA::preallocateSystemHierarchical(const ClusteringData *q0,  const ClusteringData *q1, nonLinearSystemPETSc<double>* &NLsystem)
{
  std::vector<int> clHostIDs;
  clHostIDs = q0->upperClusterIDs;
  std::map<Dof, int>& unknownLocal = getRefToUnknown(clHostIDs);

  // spasiry 
  if (NLsystem != NULL)
  {
    int nbClusters = q0->clustersNb;
    for (int icl = 0; icl < nbClusters; icl++)
    {
      static fullMatrix<double> m;
      std::vector<int> othersId;
      getBiLinearTermHierarchical(icl,q0,q1, othersId, m);
      //printf("Sizes %d x %d \n",m.size1(),m.size2());
      std::vector<Dof> R, C;
      getKeys(icl,R);
      for (int jcl=0; jcl< othersId.size(); jcl++)
      {
        getKeys(othersId[jcl],C);
      }
      //
      for (int iR=0; iR < R.size(); iR++)
      {
        std::map<Dof,int>::iterator itR =  _unknownHierarchical.find(R[iR]);
        //std::map<Dof,int>::iterator itR =  unknownLocal.find(R[iR]);
        if (itR != _unknownHierarchical.end())
        //if (itR != unknownLocal.end())
        {
          int row = itR->second;
          for (int iC =0; iC < C.size(); iC++)
          {
            std::map<Dof,int>::iterator itC =  _unknownHierarchical.find(C[iC]);
            //std::map<Dof,int>::iterator itC =  unknownLocal.find(C[iC]);
            if (itC != _unknownHierarchical.end())
            //if (itC != unknownLocal.end())
            {
              int col = itC->second;
              NLsystem->insertInSparsityPattern(row,col);
            }
          }
        }
      }
    }
  }
}

void HierarchicalReductionTFA::getLinearTermHierarchical(int clusterId, const ClusteringData *q0,  const ClusteringData *q1, fullVector<double>& res) const
{
  int nbClusters = q0->clustersNb;
  
  const STensor3& eps0i = q0->getConstRefToStrain(clusterId);
  const STensor3& eps1i = q1->getConstRefToStrain(clusterId);
  //
  const STensor3& macroeps0 = q0->getConstRefToMacroStrain();
  const STensor3& macroeps1 = q1->getConstRefToMacroStrain();
  
  std::vector<int> clHostIDs = q0->upperClusterIDs;
  std::vector<int> clIDsHierarchical = clHostIDs;
  clIDsHierarchical.push_back(clusterId);
  
  const STensor43 &Ai = getClusterAverageStrainConcentrationTensorHierarchical(clIDsHierarchical);
  static STensor3 resTen;
  resTen = eps1i;
  resTen -= eps0i;
  STensorOperation::multSTensor43STensor3Add(Ai,macroeps0,1.,resTen);
  STensorOperation::multSTensor43STensor3Add(Ai,macroeps1,-1.,resTen);
  
  for (int jcl =0; jcl < nbClusters; jcl++)
  {
    const STensor43 &Dij =  getClusterInteractionTensorHierarchical(clHostIDs,clusterId,jcl);    
    const STensor3& epsEig0j = q0->getConstRefToEigenStrain(jcl);
    const STensor3& epsEig1j = q1->getConstRefToEigenStrain(jcl);   
    STensorOperation::multSTensor43STensor3Add(Dij,epsEig1j,1.,resTen);
    STensorOperation::multSTensor43STensor3Add(Dij,epsEig0j,-1.,resTen);    
  };
  STensorOperation::fromSTensor3ToFullVector(resTen,res);
};

void HierarchicalReductionTFA::getBiLinearTermHierarchical(int clusterId, const ClusteringData *q0,  const ClusteringData *q1, 
                                                           std::vector<int>& othersId, fullMatrix<double>& dres) const 
{
  int nbClusters = q0->clustersNb;
  
  othersId.resize(nbClusters);
  for (int j=0; j< nbClusters; j++)
  {
    othersId[j] = j;
  }
  dres.resize(6,6*nbClusters,true);
  
  double vtot;
  vtot = totalVolume();  
  for (int jcl=0; jcl< nbClusters; jcl++)
  {
    static STensor43 J_ij;
    getJac_rsHierarchical(clusterId,jcl,q0,q1,J_ij);
    static fullMatrix<double> J_ij_matrix;
    STensorOperation::fromSTensor43ToFullMatrix(J_ij,J_ij_matrix);
    for (int row=0; row<6; row++)
    {
      for (int col=0; col<6; col++)
      {
        dres(row, col+6*jcl) += J_ij_matrix(row,col);
      }
    }
  }
}

void HierarchicalReductionTFA::getJac_rsHierarchical(int r, int s, const ClusteringData *q0,  const ClusteringData *q1, STensor43& Jac_rs) const 
{
  std::vector<int> clHostIDs = q0->upperClusterIDs;
  
  if (r==s)
  {
    STensorOperation::unity(Jac_rs);
  }
  else
  {
    STensorOperation::zero(Jac_rs);
  }  
  const STensor43 &Drs =  getClusterInteractionTensorHierarchical(clHostIDs,r,s);
  const STensor43 &depsEig1dstrains =  q1->getConstRefTodEigenStraindStrain(s);
  STensorOperation::multSTensor43Add(Drs,depsEig1dstrains,1.,Jac_rs);
}

void HierarchicalReductionTFA::getLinearTermHSHierarchical(int clusterId, const ClusteringData *q0,  const ClusteringData *q1, fullVector<double>& res) const
{
  int nbClusters = q0->clustersNb;
  
  std::vector<int> clHostIDs = q0->upperClusterIDs;
  std::vector<int> clIDsHierarchical = clHostIDs;
  clIDsHierarchical.push_back(clusterId);
  
  const STensor3& eps1i = q1->getConstRefToStrain(clusterId);
  const STensor3& eps0i = q0->getConstRefToStrain(clusterId);
  
  const STensor3& macroeps1 = q1->getConstRefToMacroStrain();
  const STensor3& macroeps0 = q0->getConstRefToMacroStrain();
  
  double epsEq = TFANumericTools::EquivalentStrain(macroeps1);

  static STensor3 resTen;  
  resTen = eps1i;
  resTen -= eps0i;
  resTen -= macroeps1;
  resTen += macroeps0;
  
  const STensor43& C0iso = _clusteringData->getReferenceStiffnessIso();
  double G0 = C0iso(0,1,0,1);
  const double& Gtan = q1->getConstRefToTangentShearModulus();
  
  for (int jcl =0; jcl < nbClusters; jcl++)
  {
    const STensor43 &Dij =  getClusterInteractionTensorHierarchical(clHostIDs,clusterId,jcl);    
    
    const STensor3& sig1j = q1->getConstRefToStress(jcl);
    const STensor3& eps1j = q1->getConstRefToStrain(jcl);      
    const STensor3& sig0j = q0->getConstRefToStress(jcl);
    const STensor3& eps0j = q0->getConstRefToStrain(jcl);
    
    STensor3 dsigj(sig1j);
    dsigj -= sig0j;
    STensor3 depsj(eps1j);
    depsj -= eps0j; 
    
    STensor3 fac;    
    fac = dsigj;
    fac *= G0/Gtan;
    STensorOperation::multSTensor43STensor3Add(C0iso,depsj,-1.,fac);
         
    STensorOperation::multSTensor43STensor3Add(Dij,fac,-1.,resTen);          
  }
  STensorOperation::fromSTensor3ToFullVector(resTen,res);
};
void HierarchicalReductionTFA::getBiLinearTermHSHierarchical(int clusterId, const ClusteringData *q0,  const ClusteringData *q1, std::vector<int>& othersId, fullMatrix<double>& dres)
const 
{
  int nbClusters = q0->clustersNb;
  othersId.resize(nbClusters);
  for (int j=0; j< nbClusters; j++)
  {
    othersId[j] = j;
  }
  dres.resize(6,6*nbClusters,true);
  
  double vtot;
  vtot = totalVolume();
  
  for (int jcl=0; jcl< nbClusters; jcl++)
  {
    static STensor43 J_ij;
    getJacHS_rsHierarchical(clusterId,jcl,q0,q1,J_ij);
    static fullMatrix<double> J_ij_matrix;
    STensorOperation::fromSTensor43ToFullMatrix(J_ij,J_ij_matrix);
    for (int row=0; row<6; row++)
    {
      for (int col=0; col<6; col++)
      {
        dres(row, col+6*jcl) += J_ij_matrix(row,col);
      }
    }
  }
}
void HierarchicalReductionTFA::getJacHS_rsHierarchical(int r, int s, const ClusteringData *q0,  const ClusteringData *q1, STensor43& Jac_rs) const 
{
  std::vector<int> clHostIDs = q0->upperClusterIDs;

  if (r==s)
  {
    STensorOperation::unity(Jac_rs);
  }
  else
  {
    STensorOperation::zero(Jac_rs);
  }
  
  const STensor3& macroeps1 = q1->getConstRefToMacroStrain(); 
  double epsEq = TFANumericTools::EquivalentStrain(macroeps1);
  
  const STensor43& C0iso = _clusteringData->getReferenceStiffnessIso();
  double G0 = C0iso(0,1,0,1);
  const double& Gtan = q1->getConstRefToTangentShearModulus(); 
  const STensor43 &Drs =  getClusterInteractionTensorHierarchical(clHostIDs,r,s);
  const STensor43 &Calgs = q1->getConstRefToTangent(s);
  
  const STensor3& sig1s = q1->getConstRefToStress(s);
  const STensor3& sig0s = q0->getConstRefToStress(s);   
  STensor3 dsigs(sig1s);
  dsigs -= sig0s;
   
  STensor43 fac; 
  fac = Calgs;
  fac *= G0/Gtan;
  fac -= C0iso;
  
  STensorOperation::multSTensor43Add(Drs,fac,-1.,Jac_rs);
}

void HierarchicalReductionTFA::elasticStiffnessHomHierarchical(std::vector<int> clIDsHierarchical, STensor43& CelHom) const
{
  int nbSubClusters = getNumberOfSubClusters(clIDsHierarchical);
  int clLv0 = clIDsHierarchical[0];
  
  const std::map<int,std::map<int,double>>& cluterVolumeMapLv1 = _clusteringData->getClusterVolumeMapLevel1();
  std::map<int,std::map<int,double>>::const_iterator itcLv0 = cluterVolumeMapLv1.find(clLv0);
  const std::map<int,double>& cluterVolumeMapOnLv1 = itcLv0->second;

  STensorOperation::zero(CelHom);
  double vtot;
  vtot = 0.;
  for (int itCl =0; itCl < nbSubClusters; itCl++)
  {    
    std::map<int,double>::const_iterator itVolLv1 = cluterVolumeMapOnLv1.find(itCl);
    double vi = itVolLv1->second;    
    vtot += vi;
    std::vector<int> clIDsHierarchical;
    clIDsHierarchical.push_back(clLv0);
    clIDsHierarchical.push_back(itCl);
    const materialLaw *law = getLawForCluster(clIDsHierarchical); 
    STensor43 Celi;
    STensorOperation::zero(Celi);
    law->ElasticStiffness(&Celi);
    const STensor43 &Aeli = getClusterAverageStrainConcentrationTensorHierarchical(clIDsHierarchical);
    STensorOperation::multSTensor43Add(Celi,Aeli,vi,CelHom);
  }
  CelHom *= (1./vtot);
}

void HierarchicalReductionTFA::loadClusterSummaryAndInteractionTensorsLevel1FromFiles(const std::string clusterSummaryFileName, 
                                                                                      const std::string interactionTensorsFileName)
{
  _clusteringData->loadClusterSummaryLevel1FromFile(clusterSummaryFileName);
  _clusteringData->loadInteractionTensorsLevel1FromFile(interactionTensorsFileName);
}

void HierarchicalReductionTFA::getAllClusterVolumesLevel1(std::map<int,std::vector<double>>&allVolumes) const {
  std::map<int,std::vector<int>> allCl;
  _clusteringData->getAllClusterIndicesLevel1(allCl);
  const std::map<int,std::map<int,double>>& volumeMap = _clusteringData->getClusterVolumeMapLevel1();
  
  for (std::map<int, std::vector<int>>::const_iterator itLv0 = allCl.begin(); itLv0!= allCl.end(); itLv0++)
  {
    const std::vector<int>& allClLv1 = itLv0->second;
    
    std::vector<double>& allVolumesLv1 = allVolumes[itLv0->first];
    allVolumesLv1.resize(allClLv1.size());
    
    std::map<int,std::map<int,double>>::const_iterator itVolumeMapLv1 = volumeMap.find(itLv0->first);
    const std::map<int,double>& volumeMapLv1 = itVolumeMapLv1->second;
    
    for (int itLv1=0; itLv1 < allClLv1.size(); itLv1++)
    {
      int clusterLv1 = allClLv1[itLv1];
      allVolumesLv1[itLv1] = (volumeMapLv1.find(clusterLv1))->second;
    }
  }
}


HierarchicalIncrementalTangentTFA::HierarchicalIncrementalTangentTFA(int solverType, int dim, int correction, bool withFrame, bool polarization, int tag): 
                                                                     HierarchicalReductionTFA(solverType, dim, 0, false, false, tag){}
HierarchicalIncrementalTangentTFA::~HierarchicalIncrementalTangentTFA(){}

void HierarchicalIncrementalTangentTFA::localConstitutive(const STensor3& F0, const STensor3& Fn, const ClusteringData *q0,  ClusteringData *q1)
{
  int nbClusters = q0->clustersNb;
  std::vector<int> clHostIDs = q0->upperClusterIDs;
  std::vector<int> clIDs;
  //
  STensor3& eps1 = q1->getRefToMacroStrain();
  for  (int i=0; i<3; i++)
  {
    for (int j=0; j<3; j++)
    {
      eps1(i,j) = (Fn(i,j)+Fn(j,i))*0.5;
    }
  }
  eps1(0,0) -= 1.;
  eps1(1,1) -= 1.;
  eps1(2,2) -= 1.;
    
  // local constitutive behavior
  static STensor3 Isecond(1.);
  for (int icl =0; icl < nbClusters; icl++)
  {
    clIDs = clHostIDs;
    clIDs.push_back(icl);
    const materialLaw *law = getLawForCluster(clIDs); 
    static STensor3 F0i, F1i;
    F0i = q0->getConstRefToStrain(icl);
    F1i = q1->getConstRefToStrain(icl);
    F0i += Isecond;
    F1i += Isecond;
    STensor3& P1i = q1->getRefToStress(icl);
    STensor43& Tangenti = q1->getRefToTangent(icl);
    STensor43& depsEig1depsi = q1->getRefTodEigenStraindStrain(icl);
    STensor3& epsEig1i = q1->getRefToEigenStrain(icl);
    //
    const IPVariable* ipv0i = q0->getConstRefToIPv(icl);
    IPVariable* ipv1i = q1->getRefToIPv(icl);
    
    law->constitutiveTFA(F0i,F1i,P1i,ipv0i,ipv1i,Tangenti,epsEig1i,depsEig1depsi);
  } 
};

