//
// C++ Interface: training  deep material networks
//
//
// Author:  <V.-D. Nguyen>, (C) 2021
//
// Copyright: See COPYING file that comes with this distribution
//
//

#include "TrainingDeepMaterialNetworksNonLinear.h"
#include "highOrderTensor.h"
#include "OS.h"


PathSTensor3::PathSTensor3(int N): trueSize(N), value(N,STensor3(0.)){}
PathSTensor3::~PathSTensor3(){};

PathSTensor3::PathSTensor3(const PathSTensor3& src): trueSize(src.trueSize), value(src.value),
      DvalueDWeight(src.DvalueDWeight),DvalueDAlpha(src.DvalueDAlpha),DvalueDNormal(src.DvalueDNormal){};
PathSTensor3& PathSTensor3::operator = (const PathSTensor3& src)
{
  trueSize = src.trueSize;
  value = src.value;
  DvalueDWeight = src.DvalueDWeight;
  DvalueDAlpha = src.DvalueDAlpha;
  DvalueDNormal = src.DvalueDNormal;
  return *this;
};


STensor3& PathSTensor3::getValue(int step) 
{
  if (step > trueSize-1)
  {
    Msg::Error("size %d exceed the maximum index %d",step,trueSize-1);
    Msg::Exit(0);
  }
  return value[step];
};
const STensor3& PathSTensor3::getValue(int step) const 
{
  if (step > trueSize-1)
  {
    Msg::Error("size %d exceed the maximum index %d",step,trueSize-1);
    Msg::Exit(0);
  }
  return value[step];
};
std::vector<STensor3>& PathSTensor3::getDValueDWeight(int step) 
{
  if (step > trueSize-1)
  {
    Msg::Error("size %d exceed the maximum index %d",step,trueSize-1);
    Msg::Exit(0);
  }
  return DvalueDWeight[step];
};
const std::vector<STensor3>& PathSTensor3::getDValueDWeight(int step) const 
{
  if (step > trueSize-1)
  {
    Msg::Error("size %d exceed the maximum index %d",step,trueSize-1);
    Msg::Exit(0);
  }
  return DvalueDWeight[step];  
};
std::vector<STensor3>& PathSTensor3::getDValueDCoefficient(int step) 
{
  if (step > trueSize-1)
  {
    Msg::Error("size %d exceed the maximum index %d",step,trueSize-1);
    Msg::Exit(0);
  }
  return DvalueDAlpha[step];
};
const std::vector<STensor3>& PathSTensor3::getDValueDCoefficient(int step) const 
{
  if (step > trueSize-1)
  {
    Msg::Error("size %d exceed the maximum index %d",step,trueSize-1);
    Msg::Exit(0);
  }
  return DvalueDAlpha[step];
};
std::vector<STensor3>& PathSTensor3::getDValueDNormal(int step) 
{
  if (step > trueSize-1)
  {
    Msg::Error("size %d exceed the maximum index %d",step,trueSize-1);
    Msg::Exit(0);
  }
  return DvalueDNormal[step];
};
const std::vector<STensor3>& PathSTensor3::getDValueDNormal(int step) const 
{
  if (step > trueSize-1)
  {
    Msg::Error("size %d exceed the maximum index %d",step,trueSize-1);
    Msg::Exit(0);
  }
  return DvalueDNormal[step];
};

void PathSTensor3::clear()
{
  trueSize = 0;
  value.clear();
  DvalueDWeight.clear();
  DvalueDAlpha.clear();
  DvalueDNormal.clear();
};

int PathSTensor3::size() const
{
  return trueSize;
}

void PathSTensor3::allocateWithDerivatives(int N, int sizeW, int sizeAlpha, int sizeNormal)
{
  if (N > value.size())
  {
    value.resize(N,STensor3(0.));
  }
  if (N > DvalueDWeight.size())
  {
    DvalueDWeight.resize(N,std::vector<STensor3>(sizeW,STensor3(0.)));
    DvalueDAlpha.resize(N,std::vector<STensor3>(sizeAlpha,STensor3(0.)));
    DvalueDNormal.resize(N,std::vector<STensor3>(sizeNormal,STensor3(0.)));
  }
  trueSize = N;
};

void PathSTensor3::allocate(int N)
{
  if (N > value.size())
  {
    value.resize(N,STensor3(0.));
  }
  trueSize = N;
};

void PathSTensor3::setVal(int step, int i, int j, double val)
{
  if (step > trueSize-1)
  {
    Msg::Error("step %s exceeds the path length %d",step,trueSize);
    return;
  }
  STensor3& F = value[step];
  F(i,j) = val;
};

double PathSTensor3::getValue(int step, int i, int j) const
{
  if (step > trueSize-1)
  {
    Msg::Error("step %s exceeds the path length %d",step,trueSize);
    return 0;
  }
  return value[step](i,j);
};

void PathSTensor3::fromFile(const std::string filename)
{
  clear();
  FILE* fp = fopen(filename.c_str(),"r");
  if (fp !=NULL)
  {
    while (1)
    {
      if(feof(fp))
        break;
      static std::vector<double> val(9);
      if (fscanf(fp, "%lf %lf %lf %lf %lf %lf %lf %lf %lf",
                        &val[0],&val[1],&val[2],&val[3],&val[4],&val[5],&val[6],&val[7],&val[8])==9)
      {
        static STensor3 F;
        for (int i=0; i< 3; i++)
        {
          for (int j=0; j<3; j++)
          {
            F(i,j) = val[Tensor23::getIndex(i,j)];
          }
        }
        value.push_back(F);
      }
    }
    fclose(fp);
  }
  else
  {
    Msg::Error("File %s does not exist!!!",filename.c_str());
  }
  trueSize = value.size();
};
void PathSTensor3::toFile(const std::string filename, int num) const
{
  FILE * file = fopen(filename.c_str(),"w");
  for (int in=0; in< std::min(num,trueSize); in++)
  {
    if (in >0)
    {
      fprintf(file,"\n");
    }
    for (int row=0; row<9; row++)
    {
      int i,j;
      Tensor23::getIntsFromIndex(row,i,j);
      if (row == 0)
      {
        fprintf(file,"%.16g",value[in](i,j));
      }
      else
        fprintf(file," %.16g",value[in](i,j));
    }
  }
  fclose(file);
};

void PathSTensor3::copyTo(PathSTensor3& dest, int space) const
{
  dest.clear();
  if (this->size() == 0)
  {
    return;
  }
  if (space == 1)
  {
    dest = (*this);
  }
  else
  {
    dest.value.push_back(value[0]);
    if (this->size() == 1 )
    {
      return;
    }
    int loc = space;
    while (loc < trueSize)
    {
      if (loc < trueSize-1)
      {
        dest.value.push_back(value[loc]);
      }
      loc += space;
    }
    dest.value.push_back(value[trueSize-1]);
    dest.trueSize = dest.value.size();
  }
};

PathDouble PathSTensor3::determinant() const
{
  int Nelements = this->size();
  PathDouble det(Nelements);
  for (int i=0; i< Nelements; i++)
  {
    det.getValue(i) = STensorOperation::determinantSTensor3(this->getValue(i));
  };
  return det;
};

//
PathDouble::PathDouble(int N): trueSize(N), value(N,0.){}
PathDouble::~PathDouble(){};

PathDouble::PathDouble(const PathDouble& src): trueSize(src.trueSize), value(src.value),
      DvalueDWeight(src.DvalueDWeight),DvalueDAlpha(src.DvalueDAlpha),DvalueDNormal(src.DvalueDNormal){};
PathDouble& PathDouble::operator = (const PathDouble& src)
{
  trueSize = src.trueSize;
  value = src.value;
  DvalueDWeight = src.DvalueDWeight;
  DvalueDAlpha = src.DvalueDAlpha;
  DvalueDNormal = src.DvalueDNormal;
  return *this;
};


double& PathDouble::getValue(int step) 
{
  if (step > trueSize-1)
  {
    Msg::Error("size %d exceed the maximum index %d",step,trueSize-1);
    Msg::Exit(0);
  }
  return value[step];
};

std::vector<double>& PathDouble::getDValueDWeight(int step) 
{
  if (step > trueSize-1)
  {
    Msg::Error("size %d exceed the maximum index %d",step,trueSize-1);
    Msg::Exit(0);
  }
  return DvalueDWeight[step];
};
const std::vector<double>& PathDouble::getDValueDWeight(int step) const 
{
  if (step > trueSize-1)
  {
    Msg::Error("size %d exceed the maximum index %d",step,trueSize-1);
    Msg::Exit(0);
  }
  return DvalueDWeight[step];  
};
std::vector<double>& PathDouble::getDValueDCoefficient(int step) 
{
  if (step > trueSize-1)
  {
    Msg::Error("size %d exceed the maximum index %d",step,trueSize-1);
    Msg::Exit(0);
  }
  return DvalueDAlpha[step];
};
const std::vector<double>& PathDouble::getDValueDCoefficient(int step) const 
{
  if (step > trueSize-1)
  {
    Msg::Error("size %d exceed the maximum index %d",step,trueSize-1);
    Msg::Exit(0);
  }
  return DvalueDAlpha[step];
};
std::vector<double>& PathDouble::getDValueDNormal(int step) 
{
  if (step > trueSize-1)
  {
    Msg::Error("size %d exceed the maximum index %d",step,trueSize-1);
    Msg::Exit(0);
  }
  return DvalueDNormal[step];
};
const std::vector<double>& PathDouble::getDValueDNormal(int step) const 
{
  if (step > trueSize-1)
  {
    Msg::Error("size %d exceed the maximum index %d",step,trueSize-1);
    Msg::Exit(0);
  }
  return DvalueDNormal[step];
};

void PathDouble::clear()
{
  trueSize = 0;
  value.clear();
  DvalueDWeight.clear();
  DvalueDAlpha.clear();
  DvalueDNormal.clear();
};

int PathDouble::size() const
{
  return trueSize;
}

void PathDouble::allocateWithDerivatives(int N, int sizeW, int sizeAlpha, int sizeNormal)
{
  if (N > value.size())
  {
    value.resize(N,0.);
  }
  if (N > DvalueDWeight.size())
  {
    DvalueDWeight.resize(N,std::vector<double>(sizeW,0.));
    DvalueDAlpha.resize(N,std::vector<double>(sizeAlpha,0.));
    DvalueDNormal.resize(N,std::vector<double>(sizeNormal,0.));
  }
  trueSize = N;
};

void PathDouble::allocate(int N)
{
  if (N > value.size())
  {
    value.resize(N,0.);
  }
  trueSize = N;
};

void PathDouble::setVal(int step, double val)
{
  if (step > trueSize-1)
  {
    Msg::Error("step %s exceeds the path length %d",step,trueSize);
    return;
  }
  value[step] = val;
};

void PathDouble::addValue(double v)
{
  for (int i=0; i< trueSize; i++)
  {
    value[i] += v;
  }
}

double PathDouble::getValue(int step) const
{
  if (step > trueSize-1)
  {
    Msg::Error("step %s exceeds the path length %d",step,trueSize);
    return 0;
  }
  return value[step];
}

void PathDouble::fromFile(const std::string filename)
{
  clear();
  FILE* fp = fopen(filename.c_str(),"r");
  if (fp !=NULL)
  {
    while (1)
    {
      if(feof(fp))
        break;
      double val;
      if (fscanf(fp, "%lf",&val)==1)
      {
        value.push_back(val);
      }
    }
    fclose(fp);
  }
  else
  {
    Msg::Error("File %s does not exist!!!",filename.c_str());
  }
  trueSize = value.size();
};
void PathDouble::toFile(const std::string filename, int num) const
{
  FILE * file = fopen(filename.c_str(),"w");
  for (int in=0; in< std::min(num,trueSize); in++)
  {
    if (in >0)
    {
      fprintf(file,"\n");
    }
    fprintf(file,"%.16g",value[in]);
  }
  fclose(file);
};

void PathDouble::copyTo(PathDouble& dest, int space) const
{
  dest.clear();
  if (this->size() == 0)
  {
    return;
  }
  if (space == 1)
  {
    dest = (*this);
  }
  else
  {
    dest.value.push_back(value[0]);
    if (this->size() == 1 )
    {
      return;
    }
    int loc = space;
    while (loc < trueSize)
    {
      if (loc < trueSize-1)
      {
        dest.value.push_back(value[loc]);
      }
      loc += space;
    }
    dest.value.push_back(value[trueSize-1]);
    dest.trueSize = dest.value.size();
  }
};
//

LossPathMeasure* LossPathMeasure::createLossMeasure(const char what[])
{
  if (strcmp(what, "mse")==0)
  {
    //Msg::Info("create SquareLossPathMeasure");
    return new SquareLossPathMeasure();
  }
  else if (strcmp(what, "rmse")==0)
  {
    //Msg::Info("create RelativeSquareLossPathMeasure");
    return new RelativeSquareLossPathMeasure();
  }
  else if (strcmp(what, "mae")==0)
  {
    //Msg::Info("create AbsoluteLossPathMeasure");
    return new AbsoluteLossPathMeasure();
  }
  else if (strcmp(what, "rmae")==0)
  {
    //Msg::Info("create RelativeAbsoluteLossPathMeasure");
    return new RelativeAbsoluteLossPathMeasure();
  }
  else
  {
    Msg::Error("%s has not been implemented",what);
    return NULL;
  }
};

double RelativeSquareLossPathMeasure::get(const PathSTensor3& Pref, const PathSTensor3& P, int N, bool stiff, PathSTensor3* DerrorDP) const
{
  int sizePref = Pref.size();
  int sizeP = P.size();
  int size = std::min(N,std::min(sizePref,sizeP));
  double error = 0;
  if (stiff)
  {
    DerrorDP->allocate(size);
  }
  double averageVal = 0;
  for (int i=0; i< sizePref; i++)
  {
    averageVal += 0.5*STensorOperation::doubledot(Pref.getValue(i),Pref.getValue(i));
  }
  averageVal/=double(sizePref);
  if (averageVal ==0)
  {
    averageVal = 1.; // to avoid any reference value
  }
  //
  for (int i=0; i< size; i++)
  {
    static STensor3 dP;
    dP = P.getValue(i);
    dP -= Pref.getValue(i);
    
    double valSq = 0.5*STensorOperation::doubledot(dP,dP);
    double vR = 0.5*STensorOperation::doubledot(Pref.getValue(i),Pref.getValue(i));
    if (vR > 0)
      error += valSq/(size*vR);
    else
      error += valSq/(size*averageVal);
    if (stiff)
    {
      STensor3& DerrorDPj = DerrorDP->getValue(i);
      DerrorDPj = dP;
      if (vR >0)
        DerrorDPj *= (1./(vR*size));
      else
        DerrorDPj *= (1./(averageVal*size));
    }
  };
  return error;
};

double RelativeSquareLossPathMeasure::get(const PathDouble& Pref, const PathDouble& P, int N, bool stiff, PathDouble* DerrorDP) const
{
  int sizePref = Pref.size();
  int sizeP = P.size();
  int size = std::min(N,std::min(sizePref,sizeP));
  double error = 0;
  if (stiff)
  {
    DerrorDP->allocate(size);
  }
  //
  double averageVal = 0;
  for (int i=0; i< sizePref; i++)
  {
    averageVal += 0.5*(Pref.getValue(i)*Pref.getValue(i));
  }
  averageVal/=double(sizePref);
  if (averageVal ==0)
  {
    averageVal = 1.; // to avoid any reference value
  }
    
  for (int i=0; i< size; i++)
  {
    double dP = P.getValue(i)-Pref.getValue(i);
    double valSq = 0.5*dP*dP;
    double vR = 0.5*(Pref.getValue(i)*Pref.getValue(i));
    if (vR >0)
      error += valSq/(size*vR);
    else
      error += valSq/(size*averageVal);
    if (stiff)
    {
      double& DerrorDPj = DerrorDP->getValue(i);
      DerrorDPj = dP;
      if (vR >0)
        DerrorDPj *= (1./(vR*size));
      else
        DerrorDPj *= (1./(averageVal*size));
    }
  };
  return error;
};

double SquareLossPathMeasure::get(const PathSTensor3& Pref, const PathSTensor3& P, int N, bool stiff, PathSTensor3* DerrorDP) const
{
  int sizePref = Pref.size();
  int sizeP = P.size();
  int size = std::min(N,std::min(sizePref,sizeP));
  double error = 0;
  if (stiff)
  {
    DerrorDP->allocate(size);
  }
  //
  for (int i=0; i< size; i++)
  {
    static STensor3 dP;
    dP = P.getValue(i);
    dP -= Pref.getValue(i);
    error += (0.5)*STensorOperation::doubledot(dP,dP)/size;
    if (stiff)
    {
      STensor3& DerrorDPj = DerrorDP->getValue(i);
      DerrorDPj = dP;
      DerrorDPj *= (1./size);
    }
  }
  return error;
};

double SquareLossPathMeasure::get(const PathDouble& Pref, const PathDouble& P, int N, bool stiff, PathDouble* DerrorDP) const
{
  int sizePref = Pref.size();
  int sizeP = P.size();
  int size = std::min(N,std::min(sizePref,sizeP));
  double error = 0;
  if (stiff)
  {
    DerrorDP->allocate(size);
  }
  //
  for (int i=0; i< size; i++)
  {
    double dP = P.getValue(i) - Pref.getValue(i);
    error += (0.5)*dP*dP/size;
    if (stiff)
    {
      double& DerrorDPj = DerrorDP->getValue(i);
      DerrorDPj = dP;
      DerrorDPj *= (1./size);
    }
  }
  return error;
};

double RelativeAbsoluteLossPathMeasure::get(const PathSTensor3& Pref, const PathSTensor3& P, int N, bool stiff, PathSTensor3* DerrorDP) const
{
  int sizePref = Pref.size();
  int sizeP = P.size();
  int size = std::min(N,std::min(sizePref,sizeP));
  double error = 0;
  if (stiff)
  {
    DerrorDP->allocate(size);
  }
  //
  double averageVal = 0;
  for (int i=0; i< sizePref; i++)
  {
    averageVal += sqrt(0.5*STensorOperation::doubledot(Pref.getValue(i),Pref.getValue(i)));
  };
  averageVal /= double(sizePref);
  if (averageVal ==0)
  {
    averageVal = 1.; // to avoid any reference value
  }
  
  for (int i=0; i< size; i++)
  {
    static STensor3 dP;
    dP = P.getValue(i);
    dP -= Pref.getValue(i);
    double val = sqrt(0.5*STensorOperation::doubledot(dP,dP));
    double vR = sqrt(0.5*STensorOperation::doubledot(Pref.getValue(i),Pref.getValue(i)));
    if (val > 0)
    {
      if (vR >0)
        error += val/(vR*size);
      else
        error += val/(averageVal*size);  
    }
    if (stiff)
    {
      STensor3& DerrorDPj = DerrorDP->getValue(i);
      DerrorDPj = dP;
      if (val > 0)
      {
        if (vR >0)
          DerrorDPj *= (1./(2.*val*vR*size));
        else
          DerrorDPj *= (1./(2.*val*averageVal*size));
      }
    };
  };
  
  return error;
};

double RelativeAbsoluteLossPathMeasure::get(const PathDouble& Pref, const PathDouble& P, int N, bool stiff, PathDouble* DerrorDP) const
{
  int sizePref = Pref.size();
  int sizeP = P.size();
  int size = std::min(N,std::min(sizePref,sizeP));
  double error = 0;
  if (stiff)
  {
    DerrorDP->allocate(size);
  }
  //
  double averageVal = 0;
  for (int i=0; i< sizePref; i++)
  {
    averageVal += sqrt(0.5*(Pref.getValue(i)*Pref.getValue(i)));
  };
  averageVal /= double(sizePref);
  if (averageVal ==0)
  {
    averageVal = 1.; // to avoid any reference value
  }
  
  for (int i=0; i< size; i++)
  {
    double dP = P.getValue(i) -  Pref.getValue(i);
    double val = sqrt(0.5*dP*dP);
    double vR = sqrt(0.5*(Pref.getValue(i)*Pref.getValue(i)));
    if (val > 0)
    {
      if (vR > 0)
        error += val/(vR*size);
      else
        error += val/(averageVal*size);
    }
    if (stiff)
    {
      double& DerrorDPj = DerrorDP->getValue(i);
      DerrorDPj = dP;
      if (val > 0)
      {
        if (vR >0)
          DerrorDPj *= (1./(2.*val*vR*size));
        else
          DerrorDPj *= (1./(2.*val*averageVal*size));
      }
    };
  };
  
  return error;
};

double AbsoluteLossPathMeasure::get(const PathSTensor3& Pref, const PathSTensor3& P, int N, bool stiff, PathSTensor3* DerrorDP) const
{
  int sizePref = Pref.size();
  int sizeP = P.size();
  int size = std::min(N,std::min(sizePref,sizeP));
  double error = 0;
  if (stiff)
  {
    DerrorDP->allocate(size);
  }
  //
  for (int i=0; i< size; i++)
  {
    static STensor3 dP;
    dP = P.getValue(i);
    dP -= Pref.getValue(i);
    double val = sqrt(0.5*STensorOperation::doubledot(dP,dP));
    error += val/size;
    if (stiff)
    {
      STensor3& DerrorDPj = DerrorDP->getValue(i);
      DerrorDPj = dP;
      if (val  >0)
        DerrorDPj *= (1./(2.*val*size));
    }
  }
  return error;
};    

double AbsoluteLossPathMeasure::get(const PathDouble& Pref, const PathDouble& P, int N, bool stiff, PathDouble* DerrorDP) const
{
  int sizePref = Pref.size();
  int sizeP = P.size();
  int size = std::min(N,std::min(sizePref,sizeP));
  double error = 0;
  if (stiff)
  {
    DerrorDP->allocate(size);
  }
  //
  for (int i=0; i< size; i++)
  {
    double dP = P.getValue(i) - Pref.getValue(i);
    double val = sqrt(0.5*(dP*dP));
    error += val/size;
    if (stiff)
    {
      double& DerrorDPj = DerrorDP->getValue(i);
      DerrorDPj = dP;
      if (val > 0)
        DerrorDPj *= (1./(2.*val*size));
    }
  }
  return error;
};    

TrainingDeepMaterialNetworkNonLinear::TrainingDeepMaterialNetworkNonLinear(DeepMaterialNetwork& dmn, const CoefficientReduction& reduction): 
TrainingArbitraryDeepMaterialNetwork(dmn.getRefToNetworkInteraction(),reduction), 
    _trainingWithPhaseDeformation(false), 
    _phaseIpComForTraining(),
    _phaseIndexForIpComForTraining(),
    _phaseIndexForTrainingPhaseDeformation(0),
    _mixedFactor(0.), 
    _mixedFactorIPComp(0.),
    _DMN(&dmn),
    _maximumStrainStep(1e6)
{

}
    
TrainingDeepMaterialNetworkNonLinear::~TrainingDeepMaterialNetworkNonLinear()
{

};


void TrainingDeepMaterialNetworkNonLinear::setMaximumStrainStep(double val)
{
  _maximumStrainStep = val;
  Msg::Info("maximum strain step = %e",val);
};


int TrainingDeepMaterialNetworkNonLinear::evaluatePath(const PathSTensor3& strainPath, PathSTensor3& stressPath, bool messageActive, bool stiff, 
                                                      bool extractPhaseDefo, PathSTensor3* phaseDefoPath,
                                                      bool extractIPComp, std::vector<PathDouble>* IpCompPath, PathDouble* timeSteps,
                                                      int maximumNumberOfReducedSteps,
                                                      int maxIter,
                                                      int maxSubSteps,
                                                      double tol, 
                                                      double absTol)
{
  double tstart = Cpu();
  //
  if (_DMN->isInitialized())
  {
    _DMN->reset();
  }
  else
  {
    _DMN->initSolve();
  }
  //
  static STensor43 L;
  int pathLength  =  strainPath.size();
  int sizeOfWeights = 0;
  int sizeOfAlphas = 0;
  int sizeOfNormals = 0;
  int sizeIpCompPath= _phaseIpComForTraining.size();
    
  if (stiff)
  {
    if (!_dofsAllComponentsNumbered)
    {
      numberDofsAllComponents();
    }
    sizeOfWeights = _weightDofs.size();
    sizeOfAlphas = _coefficientDofs.size();
    sizeOfNormals = _normalDofs.size();
    stressPath.allocateWithDerivatives(pathLength,sizeOfWeights, sizeOfAlphas, sizeOfNormals);
    if (extractPhaseDefo && fabs(_mixedFactor) > 0.)
    {
      phaseDefoPath->allocateWithDerivatives(pathLength,sizeOfWeights, sizeOfAlphas, sizeOfNormals);
    }
    if (extractIPComp && fabs(_mixedFactorIPComp)> 0.)
    {
      for (int k=0; k< sizeIpCompPath; k++)
      {
        (*IpCompPath)[k].allocateWithDerivatives(pathLength,sizeOfWeights, sizeOfAlphas, sizeOfNormals);
      }
    }
  }
  else
  {
    stressPath.allocate(pathLength);
    if (extractPhaseDefo && fabs(_mixedFactor) > 0.)
    {
      phaseDefoPath->allocate(pathLength);
    }
    if (extractIPComp && fabs(_mixedFactorIPComp) > 0.)
    {
      for (int k=0; k< sizeIpCompPath; k++)
      {
        (*IpCompPath)[k].allocate(pathLength);
      }
    }
  }

  double tn=0;
  for (int ip = 0; ip <pathLength; ip++)
  {    
    static STensor3 F0, dF;
    const STensor3& F1 = strainPath.getValue(ip);
    if (ip == 0)
    {
      STensorOperation::unity(F0);
    }
    else
    {
      F0 = strainPath.getValue(ip-1);
    }
    //
    dF = F1;
    dF -= F0;
    //
    double strainStep = sqrt(STensorOperation::doubledot(dF,dF));
    
    double timeStep = 1.;
    if(timeSteps!=NULL)
    {
        timeStep=timeSteps->getValue(ip);
    }

    if (strainStep > _maximumStrainStep)
    {
      int numsubstep =1+trunc(strainStep/_maximumStrainStep);
      if (messageActive)
      {
        Msg::Info("using %d substeps",numsubstep);
      }
      timeStep = timeStep/double(numsubstep);
    };
    //
    double initialTimeStep = timeStep;
    double tprev=tn;
    double tcur=tn;
    double ttau=tn+timeStep;
    //Msg::Info("tn=%g, time step=%g, tn+1=%g, tprev=%g, tcur=%g",tn,timeStep,ttau,tprev,tcur);

    STensor3& Pcur = stressPath.getValue(ip);
    std::vector<STensor3>* DPcurDW = NULL;
    std::vector<STensor3>* DPcurDAlpha = NULL;
    std::vector<STensor3>* DPcurDNormal = NULL;
    
    std::vector<STensor3>* DFPhasecurDW = NULL;
    std::vector<STensor3>* DFPhasecurDAlpha = NULL;
    std::vector<STensor3>* DFPhasecurDNormal = NULL;
    
    std::vector<std::vector<double>*> DIPCompValcurDW(sizeIpCompPath,NULL);
    std::vector<std::vector<double>*> DIPCompValcurDAlpha(sizeIpCompPath,NULL);
    std::vector<std::vector<double>*> DIPCompValcurDNormal(sizeIpCompPath,NULL);
    if (stiff)
    {
      DPcurDW = &(stressPath.getDValueDWeight(ip));
      DPcurDAlpha = &(stressPath.getDValueDCoefficient(ip));
      DPcurDNormal = &(stressPath.getDValueDNormal(ip));
      if (extractPhaseDefo && fabs(_mixedFactor) > 0.)
      {
        DFPhasecurDW = &(phaseDefoPath->getDValueDWeight(ip));
        DFPhasecurDAlpha = &(phaseDefoPath->getDValueDCoefficient(ip));
        DFPhasecurDNormal = &(phaseDefoPath->getDValueDNormal(ip));
      }
      if (extractIPComp && fabs(_mixedFactorIPComp)> 0.)
      {
        for (int k=0; k< sizeIpCompPath; k++)
        {
          DIPCompValcurDW[k] = &((*IpCompPath)[k].getDValueDWeight(ip));
          DIPCompValcurDAlpha[k] = &((*IpCompPath)[k].getDValueDCoefficient(ip));
          DIPCompValcurDNormal[k] = &((*IpCompPath)[k].getDValueDNormal(ip));
        }
      }
    };
    
    int numberOfReducedSteps=0;
    
    int iter = 0;
    double timeStep0 = timeStep;
    while (fabs(tcur-ttau) > 1e-6)    
    {
      tcur = tprev + timeStep;
      tn=tcur;
      //  
      static STensor3 Fcur;
      Fcur = F0;
      Fcur.daxpy(dF,timeStep/timeStep0);
      //
      if (messageActive)
      {
        Msg::Info("iteration %d time = %f: ",iter, tcur);
      }
      _DMN->setTime(tcur,timeStep);
      bool ok = _DMN->stress(Fcur,Pcur,false,L,maxIter,tol,absTol,messageActive);
      if (!ok)
      {
        _DMN->resetToPreviousStep();
        numberOfReducedSteps++;
        if ( numberOfReducedSteps > maximumNumberOfReducedSteps)
        {
          Msg::Error("maximum number of reduced steps %d is reached",maximumNumberOfReducedSteps);
          STensorOperation::zero(Pcur);
          return ip-1;
        }
        else
        {
          // reduced time step
          double oldTimeStep = timeStep;
          timeStep *= 0.5;
          tcur = tprev;
          Msg::Info("reduce time step from %e to %e",oldTimeStep,timeStep);
        }
      }
      else
      {
        // everything is good
        Msg::Info("tn=%g, time step=%g, tn+1=%g",tn,timeStep,ttau);
        tprev = tcur;
        if (timeStep < initialTimeStep)
        {
          timeStep *= 2.;
          if (timeStep > initialTimeStep)
          {
            timeStep = initialTimeStep;
          }
          numberOfReducedSteps --;
          if (numberOfReducedSteps < 0)
          {
            numberOfReducedSteps = 0;
          }
        }
        
        if (tcur+timeStep > ttau)
        {
          timeStep = ttau-tprev;
        };
        _DMN->nextStep();
        //
      }
      
      iter ++;
      if (iter > maxSubSteps)
      {
        Msg::Error("solve cannot be solved as maximum number of sub steps %d is reached",maxSubSteps);
        STensorOperation::zero(Pcur);
        // save data before existing
        return ip-1;
      }
    }
    //
    _DMN->saveIPDataToFile(ip+1);
    // derivatives should be here
    if (extractPhaseDefo && fabs(_mixedFactor) > 0.)
    {
      _DMN->getDeformationGradientPhase(_phaseIndexForTrainingPhaseDeformation,phaseDefoPath->getValue(ip));
      phaseDefoPath->getValue(ip)(0,0) -= 1.;
      phaseDefoPath->getValue(ip)(1,1) -= 1.;
      phaseDefoPath->getValue(ip)(2,2) -= 1.;
    };
    
    if (extractIPComp && fabs(_mixedFactorIPComp) > 0.)
    {
      for (int k=0; k< sizeIpCompPath; k++)
      {
        (*IpCompPath)[k].getValue(ip) = _ipOffsets[k]+_DMN->getPerPhase(_phaseIndexForIpComForTraining[k],_phaseIpComForTraining[k]);
      }
    };
    
    if (stiff)
    {
      ///
      // Phomo = sum_{W} 
      // compute all derivaties
      // with respect to material list
      // for std::vector<STensor3>* DPcurDW
      for (int iv=0; iv < sizeOfWeights; iv++)
      {
        STensorOperation::zero((*DPcurDW)[iv]);
        if (extractPhaseDefo && fabs(_mixedFactor) > 0.) 
        {
          STensorOperation::zero((*DFPhasecurDW)[iv]);
        }
        if (extractIPComp && fabs(_mixedFactorIPComp) > 0.)
        {
          for (int k=0; k< sizeIpCompPath; k++)
          {
            STensorOperation::zero((*DIPCompValcurDW[k])[iv]);
          }
        }
      }
    
      // for std::vector<STensor3>* DPcurDAlpha
      // zero first
      for (int iv=0; iv < sizeOfAlphas; iv++)
      {
        STensorOperation::zero((*DPcurDAlpha)[iv]);
        if (extractPhaseDefo && fabs(_mixedFactor) > 0.)
        {
          STensorOperation::zero((*DFPhasecurDAlpha)[iv]);
        }
        if (extractIPComp && fabs(_mixedFactorIPComp) > 0.)
        {
          for (int k=0; k< sizeIpCompPath; k++)
          {
            STensorOperation::zero((*DIPCompValcurDAlpha[k])[iv]);
          }
        }
      }
      // zero first
      // for std::vector<STensor3>* DPcurDNormal
      // based interaction
      for (int iv=0; iv < sizeOfNormals; iv++)
      {
        STensorOperation::zero((*DPcurDNormal)[iv]);
        if (extractPhaseDefo && fabs(_mixedFactor) > 0.)
        {
          STensorOperation::zero((*DFPhasecurDNormal)[iv]);
        }
        if (extractIPComp && fabs(_mixedFactorIPComp) > 0.)
        {
          for (int k=0; k< sizeIpCompPath; k++)
          {
            STensorOperation::zero((*DIPCompValcurDNormal[k])[iv]);
          }
        }
      };
      //
      // compute DUnknownDFittingParameters
      _DMN->computeDUnknownDFittingParameters(*_coeffReduction,_fixedWeightsOfMaterialNodes);
      
      if (!_fixedWeightsOfMaterialNodes)
      {
        static fullMatrix<double> DPDW, DFiDW;
        std::vector<fullMatrix<double> > DIPCompDW(sizeIpCompPath);
        static std::vector<int> allNodeId;
        _DMN->computeDStressDWeights(*_coeffReduction,allNodeId,DPDW);
        if (extractPhaseDefo && fabs(_mixedFactor) > 0.)
        {
          _DMN->computeDStrainDWeights(_phaseIndexForTrainingPhaseDeformation,*_coeffReduction,allNodeId,DFiDW);
        }
        if (extractIPComp && fabs(_mixedFactorIPComp) > 0.)
        {
          for (int k=0; k< sizeIpCompPath; k++)
          {
            _DMN->computeDIPCompDWeights(_phaseIpComForTraining[k],_phaseIndexForIpComForTraining[k],*_coeffReduction,allNodeId,DIPCompDW[k]);
          }
        }
        if (_fixedPhaseFraction)
        {
           // get all values before updating
          std::map<int, double> phaseFractions;
          std::map<int, double> sumZPhases;
          _interaction->getPhaseFractions(phaseFractions);
          for (std::map<int, double>::const_iterator it = phaseFractions.begin(); it != phaseFractions.end(); it++)
          {
            int phase = it->first;
            sumZPhases[phase] = 0.;
            //
            std::vector<int> allIdsPhase;
            _interaction->getAllMaterialNodeIdsForMat(phase,allIdsPhase);
            int Nnodes = allIdsPhase.size();
            for (int j=0; j< Nnodes; j++)
            {
              const MaterialNode* nodej = _interaction->getMaterialNode(allIdsPhase[j]);
              double Zj = _nodeZValues[nodej];
              sumZPhases[phase] += _activationFunction->getVal(Zj);
            }
          }
          //
          for (int i=0; i<allNodeId.size(); i++)
          {
            const MaterialNode* node = _interaction->getMaterialNode(allNodeId[i]);
            std::vector<Dof> R;
            getMaterialNodeKeys(node,R);
            
            std::vector<int> allIdsPhase;
            _interaction->getAllMaterialNodeIdsForMat(node->matIndex,allIdsPhase);
            int Nnodes = allIdsPhase.size();
            for (int j=0; j< Nnodes; j++)
            {
              const MaterialNode* nodej = _interaction->getMaterialNode(allIdsPhase[j]);
              getMaterialNodeKeys(nodej,R);
            }
            
            double fPhase = phaseFractions[node->matIndex];
            double sumfZ = sumZPhases[node->matIndex];
            double Znode = _nodeZValues[node];
            double fZnode = _activationFunction->getVal(Znode);
            double DZnode = _activationFunction->getDiff(Znode);
            
            // fi = fPhase*fZnode/fPhase
            std::vector<double> DfiDZj(R.size(),0.);
            //
            DfiDZj[0] = fPhase*DZnode/sumfZ;
            for (int j=0; j< Nnodes; j++)
            {
              const MaterialNode* nodej = _interaction->getMaterialNode(allIdsPhase[j]);
              double Zj = _nodeZValues[nodej];
              double DZj = _activationFunction->getDiff(Zj);
              DfiDZj[j+1] = -fPhase*fZnode*DZj/(sumfZ*sumfZ);
            }
            //
            for (int j=0; j< R.size(); j++)
            {
              if (_weightDofs.find(R[j]) == _weightDofs.end())
              {
                Msg::Error("Dof is not in _weightDofs");
                Msg::Exit(0);
              }
              int num = _weightDofs[R[j]];
              for (int pp=0; pp< 3; pp++)
              {
                for (int qq=0; qq<3; qq++)
                {
                  int indexx = Tensor23::getIndex(pp,qq);
                  (*DPcurDW)[num](pp,qq) += DPDW(indexx,j)*DfiDZj[j];
                  if (extractPhaseDefo&& fabs(_mixedFactor) > 0.)
                  {
                    (*DFPhasecurDW)[num](pp,qq) += DFiDW(indexx,j)*DfiDZj[j]/fPhase;
                  }
                }
              }
              if (extractIPComp && fabs(_mixedFactorIPComp) > 0.)
              {
                for (int k=0; k< sizeIpCompPath; k++)
                {
                  (*DIPCompValcurDW[k])[num] += DIPCompDW[k](0,j)*DfiDZj[j]/fPhase;
                }
              }
            }
          }
        }
        else
        {
          for (int j=0; j<allNodeId.size(); j++)
          {
            const MaterialNode* node = _interaction->getMaterialNode(allNodeId[j]);
            double DWDVar = node->af->getDiff(node->weight);
            std::vector<Dof> R;
            getMaterialNodeKeys(node,R);
            for (int k=0; k< R.size(); k++)
            {
              if (_weightDofs.find(R[k]) == _weightDofs.end())
              {
                Msg::Error("Dof is not in _weightDofs");
                Msg::Exit(0);
              }
              int num = _weightDofs[R[k]];
              for (int pp=0; pp< 3; pp++)
              {
                for (int qq=0; qq<3; qq++)
                {
                  int indexx = Tensor23::getIndex(pp,qq);
                  (*DPcurDW)[num](pp,qq) += DPDW(indexx,j)*DWDVar;
                  if (extractPhaseDefo&& fabs(_mixedFactor) > 0.)
                  {
                    (*DFPhasecurDW)[num](pp,qq) += DFiDW(indexx,j)*DWDVar;
                  }
                }
              }
              if (extractIPComp && fabs(_mixedFactorIPComp) > 0.)
              {
                for (int k=0; k< sizeIpCompPath; k++)
                {
                  (*DIPCompValcurDW[k])[num] += DIPCompDW[k](0,j)*DWDVar;
                }
              }
            };
          }
        };
      };
      
      // based on interaction
      const NetworkInteraction::InteractionContainer& allInteraction = _interaction->getInteractionContainer();
      for (NetworkInteraction::InteractionContainer::const_iterator iinter= allInteraction.begin(); iinter != allInteraction.end(); iinter++)
      {
        const InteractionMechanism* im = iinter->second; 
        static fullMatrix<double> DPcurDCoeffVars, DPcurDNorVars;
        static fullMatrix<double> DFPhasecurDCoeffVars, DFPhasecurDNorVars;
        std::vector<fullMatrix<double> >DIPCompValcurDCoeffVars(sizeIpCompPath), DIPCompValcurDNorVars(sizeIpCompPath);
        _DMN->computeDStressDInteraction(*_coeffReduction, *im, DPcurDCoeffVars,DPcurDNorVars);
        if (extractPhaseDefo&& fabs(_mixedFactor) > 0.)
        {
          _DMN->computeDStrainDInteraction(_phaseIndexForTrainingPhaseDeformation, *_coeffReduction, *im, DFPhasecurDCoeffVars,DFPhasecurDNorVars);
        }
        if (extractIPComp && fabs(_mixedFactorIPComp) > 0.)
        {
          for (int k=0; k< sizeIpCompPath; k++)
          {
            _DMN->computeDIPCompDInteraction(_phaseIpComForTraining[k],_phaseIndexForIpComForTraining[k], *_coeffReduction, *im, DIPCompValcurDCoeffVars[k],DIPCompValcurDNorVars[k]);
          }
        }
        std::vector<Dof> R;
        if (_coeffReduction->getNumberOfCoefficientVars(*_interaction, *im) > 0)
        {
          getInteractionCoefficientsKeys(im,R);
          for (int j=0; j< R.size(); j++)
          {
            if (_coefficientDofs.find(R[j]) == _coefficientDofs.end())
            {
              Msg::Error("Dof is not in _coefficientDofs 222222");
              Msg::Exit(0);
            }
            //
            int num = _coefficientDofs[R[j]];
            for (int pp=0; pp< 3; pp++)
            {
              for (int qq=0; qq<3; qq++)
              {
                int indexx = Tensor23::getIndex(pp,qq);
                (*DPcurDAlpha)[num](pp,qq) += DPcurDCoeffVars(indexx,j);
                if (extractPhaseDefo&& fabs(_mixedFactor) > 0.)
                {
                  (*DFPhasecurDAlpha)[num](pp,qq) += DFPhasecurDCoeffVars(indexx,j);
                }
              }
            }
            if (extractIPComp && fabs(_mixedFactorIPComp) > 0.)
            {
              for (int k=0; k< sizeIpCompPath; k++)
              {
                (*DIPCompValcurDAlpha[k])[num] += DIPCompValcurDCoeffVars[k](0,j);
              }
            }
          }          
        }
        //
        
        R.clear();
        getInteractionDirectionKeys(im,R);
        for (int j=0; j< R.size(); j++)
        {
          if (_normalDofs.find(R[j]) == _normalDofs.end())
          {
            Msg::Error("Dof is not in _normalDofs");
            Msg::Exit(0);
          }
          int num = _normalDofs[R[j]];
          for (int pp=0; pp< 3; pp++)
          {
            for (int qq=0; qq<3; qq++)
            {
              int indexx = Tensor23::getIndex(pp,qq);
              (*DPcurDNormal)[num](pp,qq) += DPcurDNorVars(indexx,j);
              if (extractPhaseDefo&& fabs(_mixedFactor) > 0.)
              {
                (*DFPhasecurDNormal)[num](pp,qq) += DFPhasecurDNorVars(indexx,j);
              }
            }
          }
          if (extractIPComp && fabs(_mixedFactorIPComp) > 0.)
          {
            for (int k=0; k< sizeIpCompPath; k++)
            {
              (*DIPCompValcurDNormal[k])[num] += DIPCompValcurDNorVars[k](0,j);
            }
          }
        };
      }//
    };
  }
  Msg::Info("done for this path %.6f (s)",Cpu()-tstart);
  return pathLength-1;
};

void TrainingDeepMaterialNetworkNonLinear::assembleOneDof(const Dof& key, double val, fullVector<double>& grad) const
{
  if (_fixedDof.find(key) != _fixedDof.end()) return;
  std::map<Dof,int>::const_iterator itUnk = _unknown.find(key);
  if (itUnk != _unknown.end())
  {
    int itR = itUnk->second;
    grad(itR) += val;
    return;
  }
  std::map<Dof,DofAffineConstraint<double> >::const_iterator itC = _linearConstraints.find(key);
  if (itC != _linearConstraints.end())
  {
    const DofAffineConstraint<double>& cons = itC->second;
    for (int i=0; i< cons.linear.size(); i++)
    {
      const Dof& DD = cons.linear[i].first;
      double coef = val*cons.linear[i].second;
      assembleOneDof(DD,coef,grad);
    };
  }
};


void TrainingDeepMaterialNetworkNonLinear::computeErrorVec(std::vector<fullVector<double> >& allErrors, 
      const std::vector<LossPathMeasure*>& lossMeasures, const PathSTensor3& Fref, const PathSTensor3& Pref,
      bool extractPhaseDefo, PathSTensor3* phaseDefoPathRef,
      bool extractIPComp, std::vector<PathDouble>* IpCompPathRef)
{
  if (lossMeasures.size() == 0)
  {
    allErrors.clear();
    return;
  }
  int sizeIpCompPath= _phaseIpComForTraining.size(); 
  allErrors.resize(lossMeasures.size(),fullVector<double>(3));
  static PathSTensor3 P, Fi;
  std::vector<PathDouble> IPCompi(sizeIpCompPath);
  bool messageActive = false;
  int lastConvergedIter = evaluatePath(Fref,P,messageActive,false,extractPhaseDefo,&Fi, extractIPComp, &IPCompi);
  
  //Msg::Info("done evaluatePath lastConvergedIter = %d",lastConvergedIter);
  for (int j=0; j< lossMeasures.size(); j++)
  {
    allErrors[j].setAll(0.);
    allErrors[j](0) = lossMeasures[j]->get(Pref,P,lastConvergedIter+1);
    if (extractPhaseDefo && fabs(_mixedFactor) > 0.)
    {
      allErrors[j](1) = lossMeasures[j]->get(*phaseDefoPathRef,Fi,lastConvergedIter+1);
    }
    if (extractIPComp && fabs(_mixedFactorIPComp) > 0.)
    {
      for (int k=0; k< sizeIpCompPath; k++)
      {
        allErrors[j](2) += (lossMeasures[j]->get((*IpCompPathRef)[k],IPCompi[k],lastConvergedIter+1))/(double)sizeIpCompPath;
      }
    }
  }
};

double TrainingDeepMaterialNetworkNonLinear::getEquivalentError(const fullVector<double>& allErr) const
{
  double val = allErr(0);
  if (_trainingWithPhaseDeformation && fabs(_mixedFactor) > 0.)
  {
    val += _mixedFactor*allErr(1);
  }
  if (_phaseIpComForTraining.size() >0 && fabs(_mixedFactorIPComp) > 0.)
  {
    val += _mixedFactorIPComp*allErr(2);
  }
  return val;
}

double TrainingDeepMaterialNetworkNonLinear::computeError(const LossPathMeasure& lossMeasure, const PathSTensor3& Fref, const PathSTensor3& Pref,
        bool diff, fullVector<double>* grad, 
        bool extractPhaseDefo, PathSTensor3* phaseDefoPathRef,
        bool extractIPComp, std::vector<PathDouble>* IpCompPathRef)
{
  // evaluate path
  int sizeIpCompPath= _phaseIpComForTraining.size(); 
  static PathSTensor3 P, phaseDefo;
  std::vector<PathDouble> IPcomp(sizeIpCompPath);
  bool messageActive = false;
  int lastConvergedIter = evaluatePath(Fref,P,messageActive,diff,extractPhaseDefo,&phaseDefo, extractIPComp,&IPcomp);
    
  //Msg::Info("done evaluatePath lastConvergedIter = %d",lastConvergedIter);
  static PathSTensor3 DerrorDP, DerrorDFi;
  std::vector<PathDouble> DerrorDIPcomp(sizeIpCompPath);
  double error =lossMeasure.get(Pref,P,lastConvergedIter+1,diff,&DerrorDP);
  //Msg::Info(" error first = %.16g",error);
  if (extractPhaseDefo && fabs(_mixedFactor) > 0.)
  {
    double nextError = lossMeasure.get(*phaseDefoPathRef,phaseDefo,lastConvergedIter+1,diff,&DerrorDFi);
    error += (_mixedFactor)*nextError;
    //Msg::Info(" error second = %.16g",nextError);
  }
  
  if (extractIPComp && fabs(_mixedFactorIPComp) > 0.)
  {
    for (int k=0; k< sizeIpCompPath; k++)
    {
      double nextError = lossMeasure.get((*IpCompPathRef)[k],IPcomp[k],lastConvergedIter+1,diff,&DerrorDIPcomp[k]);
      error += (_mixedFactorIPComp)*nextError/(double)sizeIpCompPath;
    }
    //Msg::Info(" error third = %.16g",nextError);
  }
  //Msg::Info(" error total = %.16g",error);
  if (diff)
  {
    // gradient of error with respect to fitting parameters
    double Ndof = _unknown.size();
    if (grad->size() != Ndof)
    {
      grad->resize(Ndof,true);
    }
    else
    {
      grad->setAll(0.);
    }
    //
    for (int i=0; i< lastConvergedIter+1; i++)
    {
      const STensor3& DerrorDPi = DerrorDP.getValue(i);
      const std::vector<STensor3>& DPiDWeights = P.getDValueDWeight(i);
      const std::vector<STensor3>& DPiDAlphas = P.getDValueDCoefficient(i);
      const std::vector<STensor3>& DPiDnormals = P.getDValueDNormal(i);
      if (!_fixedWeightsOfMaterialNodes)
      {
        for (std::map<Dof,int>::const_iterator it = _weightDofs.begin(); it != _weightDofs.end(); it++)
        {
          const Dof& key = it->first;
          int itR = it->second;
          double val = STensorOperation::doubledot(DerrorDPi,DPiDWeights[itR]);
          if (extractPhaseDefo && fabs(_mixedFactor) > 0.)
          {
            val += (_mixedFactor)*STensorOperation::doubledot(DerrorDFi.getValue(i),phaseDefo.getDValueDWeight(i)[itR]);
          }
          if (extractIPComp && fabs(_mixedFactorIPComp) > 0.)
          {
            for (int k=0; k< sizeIpCompPath; k++)
            {
              val += (_mixedFactorIPComp)*DerrorDIPcomp[k].getValue(i)*IPcomp[k].getDValueDWeight(i)[itR]/(double)sizeIpCompPath;
            }
          }
          assembleOneDof(key,val,*grad);
        };        
      }
      
      for (std::map<Dof,int>::const_iterator it = _coefficientDofs.begin(); it != _coefficientDofs.end(); it++)
      {
        const Dof& key = it->first;
        int itR = it->second;
        double val = STensorOperation::doubledot(DerrorDPi,DPiDAlphas[itR]);
        if (extractPhaseDefo && fabs(_mixedFactor) > 0.)
        {
          val += (_mixedFactor)*STensorOperation::doubledot(DerrorDFi.getValue(i),phaseDefo.getDValueDCoefficient(i)[itR]);
        }
        if (extractIPComp && fabs(_mixedFactorIPComp) > 0.)
        {
          for (int k=0; k< sizeIpCompPath; k++)
          {
            val += (_mixedFactorIPComp)*DerrorDIPcomp[k].getValue(i)*IPcomp[k].getDValueDCoefficient(i)[itR]/(double)sizeIpCompPath;
          }
        }
        assembleOneDof(key,val,*grad);
      };
      
      for (std::map<Dof,int>::const_iterator it = _normalDofs.begin(); it != _normalDofs.end(); it++)
      {
        const Dof& key = it->first;
        int itR = it->second;
        double val = STensorOperation::doubledot(DerrorDPi,DPiDnormals[itR]);
        if (extractPhaseDefo && fabs(_mixedFactor) > 0.)
        {
          val += (_mixedFactor)*STensorOperation::doubledot(DerrorDFi.getValue(i),phaseDefo.getDValueDNormal(i)[itR]);
        }
        if (extractIPComp && fabs(_mixedFactorIPComp) > 0.)
        {
          for (int k=0; k< sizeIpCompPath; k++)
          {
            val += (_mixedFactorIPComp)*DerrorDIPcomp[k].getValue(i)*IPcomp[k].getDValueDNormal(i)[itR]/(double)sizeIpCompPath;
          }
        }
        assembleOneDof(key,val,*grad);
      };
    };
  }
  return error;
};

double TrainingDeepMaterialNetworkNonLinear::computeCostFunction(const LossPathMeasure& loss,  const std::vector<int>& sampleIndex, fullVector<double>& grad)
{
  if (grad.size() != _unknown.size())
  {
    grad.resize(_unknown.size());
  }
  else
  {
    grad.setAll(0.);
  }
  static fullVector<double> gPart;
  int Ntrain = _XTrain.size();
  int batchSize = sampleIndex.size();
  
  double alpha = 1./(double)(batchSize);
  double err = 0;
  for (int j=0; j< batchSize; j++)
  {
    int sample = sampleIndex[j];
    if (sample > Ntrain-1)
    {
      Msg::Error("sample %s does not exist !!!",sample);
      Msg::Exit(0);
    }
    if (_trainingWithPhaseDeformation && _phaseIpComForTraining.size() > 0)
    {
      err += computeError(loss,_XTrain[sample],_YTrain[sample],true,&gPart,true,&_YTrainPhaseDefo[sample], true, &_YTrainPhaseIPComp[sample]);
    }
    else if (_trainingWithPhaseDeformation)
    {
      err += computeError(loss,_XTrain[sample],_YTrain[sample],true,&gPart,true,&_YTrainPhaseDefo[sample]);
    }
    else if (_phaseIpComForTraining.size() >0)
    {
      err += computeError(loss,_XTrain[sample],_YTrain[sample],true,&gPart,false,NULL, true, &_YTrainPhaseIPComp[sample]);
    }
    else
    {
      err += computeError(loss,_XTrain[sample],_YTrain[sample],true,&gPart);
    }
    grad.axpy(gPart,alpha);
  }
  err *= (alpha);
  return err;
};

double TrainingDeepMaterialNetworkNonLinear::evaluateTrainingSet(const LossPathMeasure& loss)
{
  int Ntrain = _XTrain.size();
  if (Ntrain > 0)
  {
    double err = 0;
    for (int i=0; i< Ntrain; i++)
    {
      if (_trainingWithPhaseDeformation && _phaseIpComForTraining.size() > 0)
      {
        err += computeError(loss,_XTrain[i],_YTrain[i],false,NULL,true,&_YTrainPhaseDefo[i],true,&_YTrainPhaseIPComp[i]);
      }
      else if (_trainingWithPhaseDeformation)
      {
        err += computeError(loss,_XTrain[i],_YTrain[i],false,NULL,true,&_YTrainPhaseDefo[i]);
      }
      else if (_phaseIpComForTraining.size() > 0)
      {
        err += computeError(loss,_XTrain[i],_YTrain[i],false,NULL,false,NULL,true,&_YTrainPhaseIPComp[i]);
      }
      else
      {
        err += computeError(loss,_XTrain[i],_YTrain[i]);
      }
    }
    err *= (1./(double)Ntrain);
    return err;
  }
  return 0.;
};


double TrainingDeepMaterialNetworkNonLinear::evaluateTestSet(const LossPathMeasure& loss)
{
  int NTest = _XTest.size();
  if (NTest > 0)
  {
    double err = 0;
    for (int i=0; i< NTest; i++)
    {
      if (_trainingWithPhaseDeformation && _phaseIpComForTraining.size() > 0)
      {
        err += computeError(loss,_XTest[i],_YTest[i],false,NULL,true,&_YTestPhaseDefo[i],true,&_YTestPhaseIPComp[i]);
      }
      else if (_trainingWithPhaseDeformation)
      {
        err += computeError(loss,_XTest[i],_YTest[i],false,NULL,true,&_YTestPhaseDefo[i]);
      }
      else if (_phaseIpComForTraining.size() > 0)
      {
        err += computeError(loss,_XTest[i],_YTest[i],false,NULL,false,NULL,true,&_YTestPhaseIPComp[i]);
      }
      else
      {
        err += computeError(loss,_XTest[i],_YTest[i]);
      }
    }
    err *= (1./(double)NTest);
    return err;
  }
  return 0.;
};

void TrainingDeepMaterialNetworkNonLinear::trainingDataSize(int Ns)
{
  _XTrain.resize(Ns,PathSTensor3());
  _YTrain.resize(Ns,PathSTensor3());
};

void TrainingDeepMaterialNetworkNonLinear::trainingDataSizeWithPhaseDeformation(int Ns, int phaseIndex, double mixFactor)
{
  _XTrain.resize(Ns,PathSTensor3());
  _YTrain.resize(Ns,PathSTensor3());
  _YTrainPhaseDefo.resize(Ns,PathSTensor3());
  //
  _trainingWithPhaseDeformation = true;
  _phaseIndexForTrainingPhaseDeformation = phaseIndex;
  _mixedFactor = mixFactor;
};

void TrainingDeepMaterialNetworkNonLinear::trainingDataSizeWithPhaseIPComp(int Ns, int phaseIndex, int IPComp, double mixFactor, double offset)
{
  _XTrain.resize(Ns,PathSTensor3());
  _YTrain.resize(Ns,PathSTensor3());
  _YTrainPhaseIPComp.clear();
  _YTrainPhaseIPComp.resize(Ns, std::vector<PathDouble>(1, PathDouble()));
  
  _phaseIndexForIpComForTraining.clear();
  _phaseIndexForIpComForTraining.resize(1,phaseIndex);
  _phaseIpComForTraining.clear();
  _phaseIpComForTraining.resize(1,(IPField::Output)IPComp);
  _mixedFactorIPComp = mixFactor;
  
  _ipOffsets.clear();
  _ipOffsets.resize(1,offset);
}

void TrainingDeepMaterialNetworkNonLinear::trainingDataSizeWithTwoPhaseIPComps(int Ns, int phaseIndex, int IPComp0, int IPComp1, double mixFactor, double offset0, double offset1)
{
  _XTrain.resize(Ns,PathSTensor3());
  _YTrain.resize(Ns,PathSTensor3());
  _YTrainPhaseIPComp.clear();
  _YTrainPhaseIPComp.resize(Ns, std::vector<PathDouble>(2,PathDouble()));
  
  
  _phaseIndexForIpComForTraining.clear();
  _phaseIndexForIpComForTraining.push_back(phaseIndex);
  _phaseIndexForIpComForTraining.push_back(phaseIndex);
  _phaseIpComForTraining.clear();
  _phaseIpComForTraining.push_back((IPField::Output)IPComp0);
  _phaseIpComForTraining.push_back((IPField::Output)IPComp1);
  _mixedFactorIPComp = mixFactor;
  
  _ipOffsets.clear();
  _ipOffsets.push_back(offset0);
  _ipOffsets.push_back(offset1);
};

void TrainingDeepMaterialNetworkNonLinear::setTrainingSample(int row, const PathSTensor3& Fref, const PathSTensor3& Pref, int space)
{
  if (row > _XTrain.size()-1)
  {
    Msg::Error("row %d exceed the training data size %d",row,_XTrain.size());
    Msg::Exit(0);
  }
  //
  if (space == 1)
  {
    _XTrain[row] = Fref;
    _YTrain[row] = Pref;
  }
  else
  {
    Fref.copyTo(_XTrain[row],space);
    Pref.copyTo(_YTrain[row],space); 
  }
};

void TrainingDeepMaterialNetworkNonLinear::setTrainingSamplePhaseDeformation(int row, const PathSTensor3& FphaseRef, int space)
{
  if (_trainingWithPhaseDeformation)
  {
    if (row > _YTrainPhaseDefo.size()-1)
    {
      Msg::Error("row %d exceed the training data size %d",row,_YTrainPhaseDefo.size());
      Msg::Exit(0);
    }
    //
    if (space == 1)
    {
      _YTrainPhaseDefo[row] = FphaseRef;
    }
    else
    {
      FphaseRef.copyTo(_YTrainPhaseDefo[row],space);
    }
  }
};

void TrainingDeepMaterialNetworkNonLinear::setTrainingSamplePhaseIPComp(int row, const PathDouble& IPCompRef, int ipLoc, int space)
{
  if (_phaseIpComForTraining.size() > 0)
  {
    if (row > _YTrainPhaseIPComp.size()-1)
    {
      Msg::Error("row %d exceed the training data size %d",row,_YTrainPhaseIPComp.size());
      Msg::Exit(0);
    }
    //
    if (space == 1)
    {
      _YTrainPhaseIPComp[row][ipLoc] = IPCompRef;
    }
    else
    {
      IPCompRef.copyTo(_YTrainPhaseIPComp[row][ipLoc],space);
    }
    
    Msg::Info("apply offset = %e to %s", _ipOffsets[ipLoc], IPField::ToString(_phaseIpComForTraining[ipLoc]));
    _YTrainPhaseIPComp[row][ipLoc].addValue(_ipOffsets[ipLoc]);
  }
  else
  {
    Msg::Error("internal variables for training are empty");
    Msg::Exit(0);
  }
};

void TrainingDeepMaterialNetworkNonLinear::testDataSize(int Ns)
{
  _XTest.resize(Ns,PathSTensor3());
  _YTest.resize(Ns,PathSTensor3());
  if (_trainingWithPhaseDeformation)
  {
    _YTestPhaseDefo.resize(Ns,PathSTensor3());
  }
  if (_phaseIpComForTraining.size() > 0)
  {
    _YTestPhaseIPComp.clear();
    _YTestPhaseIPComp.resize(Ns, std::vector<PathDouble>(_phaseIpComForTraining.size(),PathDouble()));
  }
};

void TrainingDeepMaterialNetworkNonLinear::setTestSamplelePhaseIPComp(int row, const PathDouble& IPCompRef, int ipLoc, int space)
{
  if (_phaseIpComForTraining.size() > 0)
  {
    if (row > _YTestPhaseIPComp.size()-1)
    {
      Msg::Error("row %d exceed the training data size %d",row,_YTestPhaseIPComp.size());
      Msg::Exit(0);
    }
    //
    if (space == 1)
    {
      _YTestPhaseIPComp[row][ipLoc] = IPCompRef;
    }
    else
    {
      IPCompRef.copyTo(_YTestPhaseIPComp[row][ipLoc],space);
    }
    
    _YTestPhaseIPComp[row][ipLoc].addValue(_ipOffsets[ipLoc]);
  }
  else
  {
    Msg::Error("internal variables for testing are empty");
    Msg::Exit(0);
  }
};

void TrainingDeepMaterialNetworkNonLinear::setTestSamplelePhaseDeformation(int row, const PathSTensor3& FphaseRef, int space)
{
  if (_trainingWithPhaseDeformation)
  {
    if (row > _YTestPhaseDefo.size()-1)
    {
      Msg::Error("row %d exceed the testing data size %d",row,_YTestPhaseDefo.size());
      Msg::Exit(0);
    }
    if (space == 1)
    {
      _YTestPhaseDefo[row] = FphaseRef;
    }
    else
    {
      FphaseRef.copyTo(_YTestPhaseDefo[row],space);
    };
  }
};

void TrainingDeepMaterialNetworkNonLinear::setTestSample(int row, const PathSTensor3& Fref, const PathSTensor3& Pref, int space)
{
  if (row > _XTest.size()-1)
  {
    Msg::Error("row %d exceed the testing data size %d",row,_XTest.size());
    Msg::Exit(0);
  }
  if (space == 1)
  {
    _XTest[row] = Fref;
    _YTest[row] = Pref;
  }
  else
  {
    Fref.copyTo(_XTest[row],space);
    Pref.copyTo(_YTest[row],space);
  }
};


int TrainingDeepMaterialNetworkNonLinear::sizeTrainingSet() const
{
  return _XTrain.size();
}
// allow to estimate loss function and derivatives with respect to the fitting paramater
double TrainingDeepMaterialNetworkNonLinear::computeCostFunction(const std::string& metric, const std::vector<int>& sampleIndex, fullVector<double>& grad)
{
  LossPathMeasure* loss = LossPathMeasure::createLossMeasure(metric.c_str());
  double val = computeCostFunction(*loss,sampleIndex,grad);
  delete loss;
  return val;
}
// loss function in whole training set
void TrainingDeepMaterialNetworkNonLinear::evaluateTrainingSet(const std::vector<std::string>& metrics, std::vector<double>& vals)
{
  if (metrics.size() == 0)
  {
    vals.clear();
    return;
  }
  
  int N = metrics.size();
  vals.resize(N);
  for (int i=0; i< N; i++)
  {
    vals[i] = 0.;
  }
  
  int Ntrain = _XTrain.size();
  if (Ntrain == 0)
  {
    Msg::Error("no sample in training dataset!!!");
    return;
  }
  std::vector<LossPathMeasure*> allMetrics(N,NULL);
  std::vector<fullVector<double> > allErrorsAverage(N,fullVector<double>(3));
  for (int i=0; i< N; i++)
  {
    allMetrics[i] = LossPathMeasure::createLossMeasure(metrics[i].c_str());
    allErrorsAverage[i].setAll(0.);
  }
  for (int i=0; i< Ntrain; i++)
  {
    static std::vector<fullVector<double> > allErrors;
    if (_trainingWithPhaseDeformation && _phaseIpComForTraining.size() >0)
    {
      computeErrorVec(allErrors,allMetrics,_XTrain[i],_YTrain[i],true,&_YTrainPhaseDefo[i],true,&_YTrainPhaseIPComp[i]);
    }
    else if (_trainingWithPhaseDeformation)
    {
      computeErrorVec(allErrors,allMetrics,_XTrain[i],_YTrain[i],true,&_YTrainPhaseDefo[i]);
    }
    else if (_phaseIpComForTraining.size() > 0)
    {
      computeErrorVec(allErrors,allMetrics,_XTrain[i],_YTrain[i],false,NULL,true,&_YTrainPhaseIPComp[i]);
    }
    else
    {
      computeErrorVec(allErrors,allMetrics,_XTrain[i],_YTrain[i]);
    }
    for (int j=0; j< N; j++)
    {
      vals[j] += getEquivalentError(allErrors[j])/(double)Ntrain;
      allErrorsAverage[j].axpy(allErrors[j],1./(double)Ntrain);
    }
  }  
  
  for (int i=0; i< N; i++)
  {
    delete allMetrics[i];
     Msg::Info("training dataset - for measure %s: %.16g %.16g %.16g %.16g",metrics[i].c_str(),
       allErrorsAverage[i](0),allErrorsAverage[i](1),allErrorsAverage[i](2),getEquivalentError(allErrorsAverage[i]));
  }
}
// loss function in test set
void TrainingDeepMaterialNetworkNonLinear::evaluateTestSet(const std::vector<std::string>& metrics, std::vector<double>& vals)
{
  if (metrics.size() == 0)
  {
    vals.clear();
    return;
  }
  
  
  int N = metrics.size();
  vals.resize(N);
  for (int i=0; i< N; i++)
  {
    vals[i] = 0.;
  }
  
  int NTest = _XTest.size();
  if (NTest == 0)
  {
    Msg::Error("no sample in testing dataset!!!");
    return;
  }
  std::vector<LossPathMeasure*> allMetrics(N,NULL);
  std::vector<fullVector<double> > allErrorsAverage(N,fullVector<double>(3));
  for (int i=0; i< N; i++)
  {
    allMetrics[i] = LossPathMeasure::createLossMeasure(metrics[i].c_str());
    allErrorsAverage[i].setAll(0.);
  }
  for (int i=0; i< NTest; i++)
  {
    static std::vector<fullVector<double> > allErrors;
    if (_trainingWithPhaseDeformation && _phaseIpComForTraining.size() >0)
    {
      computeErrorVec(allErrors,allMetrics,_XTest[i],_YTest[i],true,&_YTestPhaseDefo[i], true, &_YTestPhaseIPComp[i]);
    }
    else if (_trainingWithPhaseDeformation)
    {
      computeErrorVec(allErrors,allMetrics,_XTest[i],_YTest[i],true,&_YTestPhaseDefo[i]);
    }
    else if (_phaseIpComForTraining.size() >0)
    {
      computeErrorVec(allErrors,allMetrics,_XTest[i],_YTest[i],false,NULL, true, &_YTestPhaseIPComp[i]);
    }
    else
    {
      computeErrorVec(allErrors,allMetrics,_XTest[i],_YTest[i]);
    }
    for (int j=0; j< N; j++)
    {
      vals[j] += getEquivalentError(allErrors[j])/(double)NTest;
      allErrorsAverage[j].axpy(allErrors[j],1./(double)NTest);
    }
  }
  for (int i=0; i< N; i++)
  {
    delete allMetrics[i];
    Msg::Info("testing dataset - for measure %s: %.16g %.16g %.16g %.16g",metrics[i].c_str(),
       allErrorsAverage[i](0),allErrorsAverage[i](1),allErrorsAverage[i](2),getEquivalentError(allErrorsAverage[i]));
  }
};
// save model
void TrainingDeepMaterialNetworkNonLinear::saveModel(const std::string fname) const
{
  _interaction->saveDataToFile(fname);
}
// start numbering fitting parameters
void TrainingDeepMaterialNetworkNonLinear::initializeFittingParameters(fullVector<double>& W)
{
  numberDofs(W);
}

// get bounds of fitting parmaters
void TrainingDeepMaterialNetworkNonLinear::getUnknownBounds(fullVector<double>& WLower, fullVector<double>& WUpper) const
{
  Msg::Error("TrainingDeepMaterialNetworkNonLinear::getUnknownBounds has not been implemeneted");
  Msg::Exit(1);
};
// update the model with the new fitting parameter
void TrainingDeepMaterialNetworkNonLinear::updateModelFromFittingParameters(const fullVector<double>& W)
{
  updateFieldFromUnknown(W);
}
//  in case of fails
void TrainingDeepMaterialNetworkNonLinear::reinitializeModel()
{
  _interaction->reInitialize();
};

int evaluatePath(DeepMaterialNetwork* dmn, double maximumStrainStep, const PathSTensor3& strainPath, PathSTensor3& stressPath, PathDouble* timeSteps)
{
  CoefficientReductionVoid reduction;
  TrainingDeepMaterialNetworkNonLinear offTraining(*dmn,reduction);
  offTraining.setMaximumStrainStep(maximumStrainStep);
  int nb = offTraining.evaluatePath(strainPath,stressPath,true,false,false,NULL,false,NULL,timeSteps);
  return nb;
};