//
// C++ Interface:  deep material networks
//
//
// Author:  <V.-D. Nguyen>, (C) 2020
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef _DEEPMATERIALNETWORKS_H_
#define _DEEPMATERIALNETWORKS_H_ 

#ifndef SWIG
#include "ANNUtils.h"
#include <vector>
#include <map>
#include "highOrderTensor.h"
#include "STensorOperations.h"
#include "dofManager.h"
#include "ipField.h"
#if defined(HAVE_PETSC)
template<class T>
class nonLinearSystemPETSc;
#elif defined(HAVE_GMM)
template<class T>
class nonLinearSystemGmm;
#endif //HAVE_GMM
#include "nlsField.h"
class PathSTensor3;
class PathDouble;
#endif //SWIG
#include "Tree.h"
#include "NetworkInteraction.h"

class DeepMaterialNetwork
{
  #ifndef SWIG
  public:
    struct AveragePhase
    {
      int phaseIndex; // -1 implies all phases averaging
      IPField::Output ipComp;
      nlsField::ElemValue eleVal;
      AveragePhase(int iphase, IPField::Output icomp, nlsField::ElemValue ival): phaseIndex(iphase),ipComp(icomp),eleVal(ival){}
      AveragePhase(const AveragePhase& src): phaseIndex(src.phaseIndex),ipComp(src.ipComp),eleVal(src.eleVal){}
      AveragePhase& operator=(const AveragePhase& src)
      {
        phaseIndex = (src.phaseIndex);
        ipComp = (src.ipComp);
        eleVal = (src.eleVal);
        return *this;
      }
      ~AveragePhase(){}
    };
    struct DUnknownDFittingParameters
    {
      // particular structure to store the derivatives of the unknown with respect to the fitting parameters
      fullMatrix<double> DaDweights; 
      std::vector<int> allNodeIds; // to make correct node ID order in DaDweights
      std::map<int, fullMatrix<double> > DaDCoeffVars; // map using mechanism index as key
      std::map<int, fullMatrix<double> > DaDnormalVars; // map using mechanism index  as key
      DUnknownDFittingParameters(){}
      DUnknownDFittingParameters(const DUnknownDFittingParameters& src): 
            DaDweights(src.DaDweights), allNodeIds(src.allNodeIds), 
            DaDCoeffVars(src.DaDCoeffVars), DaDnormalVars(src.DaDnormalVars){}
      ~DUnknownDFittingParameters(){};
      const fullMatrix<double>& getDaDweights() const {return DaDweights;};
      const std::vector<int>& getAllNodeIds() const {return allNodeIds;};
      const fullMatrix<double>& getDaDCoeffVars(int mechanismIndex) const 
      {
        std::map<int, fullMatrix<double> >::const_iterator itF = DaDCoeffVars.find(mechanismIndex);
        if (itF != DaDCoeffVars.end())
        {
          return itF->second;
        }
        else
        {
          Msg::Error("mechanism %d is not found in DUnknownDFittingParameters",mechanismIndex);
        }
      }
      const fullMatrix<double>& getDaDnormalVars(int mechanismIndex) const 
      {
        std::map<int, fullMatrix<double> >::const_iterator itF = DaDnormalVars.find(mechanismIndex);
        if (itF != DaDnormalVars.end())
        {
          return itF->second;
        }
        else
        {
          Msg::Error("mechanism %d is not found in DUnknownDFittingParameters",mechanismIndex);
        }
      }
    };
  protected:
    NetworkInteraction* _networkInteraction;
    bool _ownInteractionData; // true if _networkInteraction is allocated inside the classe
    int _tag; //
    bool _lineSearch;
    //
    std::map<Dof,int> _unknown;
    std::set<Dof> _fixedDof;
    std::vector<int> _fixedComp;
    
    // when using the material network to train 
    DUnknownDFittingParameters _DaDFittingParameters;
    
    #if defined(HAVE_PETSC)
    nonLinearSystemPETSc<double>* _sys;
    bool _isAddedSparsityPattern; //cache
    #elif defined(HAVE_GMM)
    nonLinearSystemGmm<double>* _sys;
    #else
    fullMatrix<double> _K;
    fullVector<double> _res, _x, _solCur, _solPrev, _solInit;
    #endif //HAVE_GMM
    bool _isInitialized;
    std::vector<std::pair<int, int> > _viewIP;
    std::vector<AveragePhase> _averagePhase;
    // Element number and Integration point number
    int _enumMinus,_enumPlus, _gnum;  
    std::string _saveDir;
  #endif //SWIG
  
  public:
    #ifndef SWIG
    DeepMaterialNetwork(int tag = 1000);
    DeepMaterialNetwork(const Tree& T, int tag = 1000);
    DeepMaterialNetwork(const std::string fname, int tag = 1000);
    virtual ~DeepMaterialNetwork();
    void copyViewSetting(const DeepMaterialNetwork& src);
    #endif //SWIG
    //
    NetworkInteraction& getRefToNetworkInteraction() {return *_networkInteraction;};
    const NetworkInteraction& getConstRefToNetworkInteraction() const {return *_networkInteraction;};
    
    std::string getFileSavingPrefix() const;
    void setMicroProblemIndentification(int ele, int gpt);
    void setMicroProblemIndentification(int eleMinus, int elePlus, int gpt);
    void setSaveDir(std::string saveDir);
    int evaluateByPath(double maximumStrainStep, const PathSTensor3& strainPath, PathSTensor3& stressPath,  PathDouble* timeSteps=NULL);
    
    void setLineSearch(bool flg);
    void addInteraction(NetworkInteraction& inter);
    void saveInteractionToFile(const std::string fname) const;
    void addInteractionFromTree(const Tree& T);
    void addInteractionFromFile(const std::string fname);
    void reset();
    void printPhaseFraction() const;
    void fixDofsByComp(int comp); // fixed all comp
    bool isInitialized() const;
    void initSolve();
    bool solve(fullMatrix<double>& Fmat, fullMatrix<double>& Pmat, int maxIter=15, double tol=1e-6, double absTol=1e-12);
    void elasticTangent(fullMatrix<double>& L);
    void elasticTangentByStress(fullMatrix<double>& L);
    double get(int comp) const;
    double getPerPhase(int phaseIndex, int comp) const;
    void viewIPAveragePerPhase(int iphase, int icomp, int eleVal=nlsField::mean);
    void viewIPAverage(int icomp, int eleVal=nlsField::mean);
    void viewIP(int icomp, int eleVal=nlsField::crude);
    void saveIPDataToFile(int step) const;
    void printDeformationGradientNodes() const;
    #ifndef SWIG
    int getTag() const{return _tag;}
    bool stress(const STensor3& F, STensor3& P, bool stiff, STensor43& L, int maxIter, double tol, double absTol, 
                bool messageActive=false, bool reuseStiffnessMatrix=false);
    virtual void setTime(double tcur,double timeStep)=0;
    bool initialElasticTangentByStress(fullMatrix<double>& Lmat, 
                            bool stiff=false, 
                            const CoefficientReduction* coeffReduc=NULL,
                            std::map<const MaterialNode*, fullMatrix<double> >* DLDWeights=NULL, 
                            std::map<const InteractionMechanism*, std::vector<fullMatrix<double> > >* DLDCoeffVars=NULL, 
                            std::map<const InteractionMechanism*, std::vector<fullMatrix<double> > >* DLDNormalVars=NULL);
    void copy(const IPStateBase::whichState ws1, const IPStateBase::whichState ws2);
    void nextStep();
    void resetToPreviousStep();
    void restart()
    {
      Msg::Error("DeepMaterialNetwork::restart has not been implemented");
    }
    #endif //
    void computeDStressDInteraction(const CoefficientReduction& coeffReduc, const InteractionMechanism& im, 
                                fullMatrix<double>& DPDCoeffVars, fullMatrix<double>& DPDnormalVars);
    void computeDStrainDInteraction(int phaseIndex, const CoefficientReduction& coeffReduc, const InteractionMechanism& im, 
                                fullMatrix<double>& DFDCoeffVars, fullMatrix<double>& DFDnormalVars);
    void computeDIPCompDInteraction(IPField::Output comp, int phaseIndex, const CoefficientReduction& coeffReduc, const InteractionMechanism& im, 
                                fullMatrix<double>& DFDCoeffVars, fullMatrix<double>& DFDnormalVars);                                
    void computeDStressDWeights(const CoefficientReduction& coeffReduc, std::vector<int>& allNodeIds, fullMatrix<double>& DPDW);
    void computeDStrainDWeights(int phaseIndex, const CoefficientReduction& coeffReduc, std::vector<int>& allNodeIds, fullMatrix<double>& DPDW);
    void computeDIPCompDWeights(IPField::Output comp, int phaseIndex, const CoefficientReduction& coeffReduc, 
                      std::vector<int>& allNodeIds, fullMatrix<double>& DPDW);
  #ifndef SWIG
    void computeDUnknownDFittingParameters(const CoefficientReduction& coeffReduc, bool fixMaterialNodes);
    void getStress(STensor3& P) const;
    void getDeformationGradientPhase(int phaseIndex, STensor3& F) const;
  protected:
    virtual void createIPState(MaterialNode* node) = 0;
    virtual void stressLocal(MaterialNode* node) = 0;
    virtual void getDIPCompDFLocal(const IPStateBase* ips, IPField::Output comp, double& val, STensor3& DvalDF) = 0;
    
    void allocateIPStateMaterialNodes();
    void downscale(const STensor3& Fcur);
    void getKeys(const InteractionMechanism* im, std::vector<Dof>& keys) const;
    void getInterfaceKeys(const MaterialNode* node, std::vector<Dof>& keys) const;
    void numberDof();
    void assembleRes();
    void assembleDresDu();
    void updateFieldFromUnknown();
    void computeDStressDIncompatibleVector(fullMatrix<double>& DPDa);
    void computeDStrainDIncompatibleVector(int phaseIndex, fullMatrix<double>& DPDa);
    void computeDIPCompDIncompatibleVector(IPField::Output comp, int phaseIndex, fullMatrix<double>& DPDa);
    
    // linear system operations
    void allocateSystem(int nR);
    void zeroRHS();
    void zeroMatrix();
    void addToRHS(int row, const double& val);
    void addToMatrix(int row, int col, const double& val);
    double getNormRHS() const;
    double getFromSolution(int row) const;
    bool systemSolve();
    bool tangentSolve(STensor43& L);
    bool computeDUnknownDInteraction(const CoefficientReduction& coeffReduc, const InteractionMechanism& im,  fullMatrix<double>& DaDCoeffVars, fullMatrix<double>& DaDnormalVars);
    bool computeDUnknownDWeights(const CoefficientReduction& coeffReduc, std::vector<int>& allNodeIds, fullMatrix<double>& DaDW);
    void systemNextStep();
    void systemResetToPreviousStep();
    void sparsitySystem();
  #endif //SWIG
};
#endif //_DEEPMATERIALNETWORKS_H_
