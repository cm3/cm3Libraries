//
// C++ Interface: clustering data
//
//
// Author:  <V.-D. Nguyen>, (C) 2020
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef CLUSTERINGDATA_H_
#define CLUSTERINGDATA_H_

#include "ipvariable.h"
#include "STensor3.h"
#include "ipField.h"
#include "STensorOperations.h"

class GenericClusteringData
{
  public:
    int clustersNb; //total number of clusters
    std::vector<int> allClusterIndexes;
    std::vector<double> allClusterVolumes;
    std::map<int, IPVariable*> allIpvs; //underlying law ipv, a map should be used as the cluster numbers are not necessary ordered
    std::map<int, STensor3> allStrains; //underlying law ipv
    std::map<int, STensor3> allStresses; // local stress in cluster
    std::map<int, STensor43> allTangents; // local tangent in cluster
    std::map<int, STensor3> allEigenStrains; // eigenstrains used in reduction formulation
    std::map<int, STensor43> alldEigenStrainsdStrains; // derivarives of eigenstrains
    std::map<int, STensor43> alldEigenStrainFramedStrainsCluster; // derivarives of eigenstrains
    std::map<int, STensor43> allSecants;
    
    STensor3 macroStrain;
    STensor3 macroStress;
    STensor3 macroEigenStrain;
    
    STensor43 macroTangent;
    
    STensor43 macroElasticTangent;
    
    STensor3 strainFrame;
    STensor3 eigenStrainFrame;
    
    STensor43 stiffnessIso;
    double secantShearModulus;
    double tangentShearModulus;
    STensor43 stiffnessIsoInv;

    STensor43 stiffnessAniso;
    STensor43 stiffnessAnisoInv;
    
    STensor43 dEigenStraindStrainFrame;
    
    double elasticShearModulus;
    
    std::vector<int> upperClusterIDs;
    
    GenericClusteringData(const std::vector<int>& allCl, const std::vector<double>& allVf): clustersNb(allCl.size()), 
          allClusterIndexes(allCl), allClusterVolumes(allVf),
          macroStrain(0.),macroStress(0.),macroEigenStrain(0.),
          strainFrame(0.), eigenStrainFrame(0.), dEigenStraindStrainFrame(0.),
          stiffnessIso(0.),
          stiffnessIsoInv(0.),
          stiffnessAniso(0.),
          stiffnessAnisoInv(0.),
          secantShearModulus(0.),
          tangentShearModulus(0.),
          elasticShearModulus(0.),
          macroTangent(0.),
          macroElasticTangent(0.)
    {
      for (int i=0; i< clustersNb; i++)
      {
        allIpvs.insert(std::pair<int,IPVariable*>(allClusterIndexes[i],NULL));
        allStrains.insert(std::pair<int,STensor3>(allClusterIndexes[i],STensor3(0.)));
        allStresses.insert(std::pair<int,STensor3>(allClusterIndexes[i],STensor3(0.)));
        allTangents.insert(std::pair<int,STensor43>(allClusterIndexes[i],STensor43(0.)));
        allEigenStrains.insert(std::pair<int,STensor3>(allClusterIndexes[i],STensor3(0.)));
        alldEigenStrainsdStrains.insert(std::pair<int,STensor43>(allClusterIndexes[i],STensor43(0.)));
        alldEigenStrainFramedStrainsCluster.insert(std::pair<int,STensor43>(allClusterIndexes[i],STensor43(0.)));
        allSecants.insert(std::pair<int,STensor43>(allClusterIndexes[i],STensor43(0.)));
      }
    }
    GenericClusteringData(const GenericClusteringData& src):clustersNb(src.clustersNb), allClusterIndexes(src.allClusterIndexes), allClusterVolumes(src.allClusterVolumes),
                                              allStrains(src.allStrains),allStresses(src.allStresses), allTangents(src.allTangents),
                                              allEigenStrains(src.allEigenStrains),
                                              alldEigenStrainsdStrains(src.alldEigenStrainsdStrains),
                                              macroStrain(src.macroStrain), macroStress(src.macroStress), macroEigenStrain(src.macroEigenStrain),
                                              alldEigenStrainFramedStrainsCluster(src.alldEigenStrainFramedStrainsCluster),
                                              allSecants(src.allSecants),
                                              strainFrame(src.strainFrame),eigenStrainFrame(src.eigenStrainFrame),
                                              dEigenStraindStrainFrame(src.dEigenStraindStrainFrame),
                                              stiffnessIso(src.stiffnessIso),
                                              stiffnessIsoInv(src.stiffnessIsoInv),
                                              stiffnessAniso(src.stiffnessAniso),
                                              stiffnessAnisoInv(src.stiffnessAnisoInv),
                                              secantShearModulus(src.secantShearModulus),
                                              tangentShearModulus(src.tangentShearModulus),
                                              elasticShearModulus(src.elasticShearModulus),
                                              macroTangent(src.macroTangent),
                                              macroElasticTangent(src.macroElasticTangent),
                                              upperClusterIDs(src.upperClusterIDs)
    {
      allIpvs.clear();
      for (std::map<int, IPVariable*>::const_iterator it = src.allIpvs.begin(); it!=src.allIpvs.end(); it++)
      {
        if (it->second != NULL)
        {
          allIpvs[it->first] = it->second->clone();
        }
        else
        {
          allIpvs[it->first] = NULL;
        }
      }
    }
    GenericClusteringData& operator=(const GenericClusteringData& src)
    {
      bool compatible = true;
      if (clustersNb != src.clustersNb)
      {
        compatible = false;
      }
      else
      {
        // check if two ClusteringDatas are compatible
        for (std::map<int,IPVariable*>::const_iterator it = src.allIpvs.begin(); it!= src.allIpvs.end(); it++)
        {
          if (allIpvs.find(it->first) == allIpvs.end())
          {
            compatible = false;
            break;
          }
        }
      }
      if (!compatible)
      {
        Msg::Error("make an equal operator between two different clustering data num clusters left = %d  num clusters right = %d!!!",clustersNb,src.clustersNb);
        Msg::Exit(0);
      }
      else
      {
        allClusterIndexes = src.allClusterIndexes;
        allClusterVolumes = src.allClusterVolumes;
        macroStrain = src.macroStrain;
        macroStress = src.macroStress;
        macroEigenStrain = src.macroEigenStrain;
        strainFrame = src.strainFrame;
        eigenStrainFrame = src.eigenStrainFrame;
        dEigenStraindStrainFrame = src.dEigenStraindStrainFrame;
        stiffnessIso = src.stiffnessIso;
        stiffnessIsoInv = src.stiffnessIsoInv;
        stiffnessAniso = src.stiffnessAniso;
        stiffnessAnisoInv = src.stiffnessAnisoInv;
        secantShearModulus = src.secantShearModulus;
        tangentShearModulus = src.tangentShearModulus;
        elasticShearModulus = src.elasticShearModulus;
        macroTangent = src.macroTangent;
        macroElasticTangent = src.macroElasticTangent;
        upperClusterIDs = src.upperClusterIDs;
        for (int i=0; i< clustersNb; i++)
        {
          const IPVariable* ipvsrc = (src.allIpvs.find(allClusterIndexes[i]))->second;
          const STensor3& strainSrc = (src.allStrains.find(allClusterIndexes[i]))->second;
          const STensor3& stressSrc = (src.allStresses.find(allClusterIndexes[i]))->second;
          const STensor43& tangentSrc = (src.allTangents.find(allClusterIndexes[i]))->second;
          const STensor3& eigenStrainSrc = (src.allEigenStrains.find(allClusterIndexes[i]))->second;
          const STensor43& deigenStrainSrc = (src.alldEigenStrainsdStrains.find(allClusterIndexes[i]))->second;
          const STensor43& deigenStrainFramedStrainsClusterSrc = (src.alldEigenStrainFramedStrainsCluster.find(allClusterIndexes[i]))->second;
          const STensor43& secantSrc = (src.allSecants.find(allClusterIndexes[i]))->second;
          
          if (ipvsrc!=NULL)
          {
            if (allIpvs[allClusterIndexes[i]] != NULL)
            {
              allIpvs[allClusterIndexes[i]]->operator=(*ipvsrc);
            }
            else
            {
              allIpvs[allClusterIndexes[i]] = ipvsrc->clone();
            }
          }
          else
          {
            if (allIpvs[allClusterIndexes[i]] != NULL)
            {
              delete allIpvs[allClusterIndexes[i]];
              allIpvs[allClusterIndexes[i]] = NULL;
            }
          }
          allStrains[allClusterIndexes[i]] = strainSrc;
          allStresses[allClusterIndexes[i]] = stressSrc;
          allTangents[allClusterIndexes[i]] = tangentSrc;
          allEigenStrains[allClusterIndexes[i]] = eigenStrainSrc;
          alldEigenStrainsdStrains[allClusterIndexes[i]] = deigenStrainSrc;
          alldEigenStrainFramedStrainsCluster[allClusterIndexes[i]] = deigenStrainFramedStrainsClusterSrc;
          allSecants[allClusterIndexes[i]] = secantSrc;
        }
      }
      return *this;
    };
    
    GenericClusteringData(const std::map<int,std::vector<int>>& allClLv1, const std::map<int,std::vector<double>>& allVfLv1)
    {
    
    }
    
    virtual void computeHomogenizedStrain(STensor3& eps) const
    {
      STensorOperation::zero(eps);
      if (clustersNb==0) return;
      double vToltal = 0;
      for (int icl=0; icl< clustersNb; icl++)
      {
        double vf = allClusterVolumes[icl];
        vToltal += vf;
        const STensor3& epsCl = getConstRefToStrain(allClusterIndexes[icl]);
        eps.daxpy(epsCl, vf);
      }
      eps *= (1./vToltal);
    }
    virtual void computeHomogenizedStress(STensor3& sig) const
    {
      STensorOperation::zero(sig);
      if (clustersNb==0) return;
      double vToltal = 0;
      for (int icl=0; icl< clustersNb; icl++)
      {
        double vf = allClusterVolumes[icl];
        vToltal += vf;
        const STensor3& sigCl = getConstRefToStress(allClusterIndexes[icl]);
        sig.daxpy(sigCl, vf);
      }
      sig *= (1./vToltal);
    };
    
    virtual double get(const int i) const
    {
      if (clustersNb==0) return 0.;
      double val = 0;
      double vToltal = 0;
      for (int icl=0; icl< clustersNb; icl++)
      {
        double vf = allClusterVolumes[icl];
        vToltal += vf;
        const IPVariable* ipCluster = getConstRefToIPv(allClusterIndexes[icl]);
        val += vf*ipCluster->get(i);
      }
      return val/vToltal;
    };
    
    virtual void restart()
    {
      restartManager::restart(clustersNb);
      restartManager::restart(allClusterIndexes); 
      for (int i=0; i< clustersNb; i++)
      {
        IPVariable*& ipv = allIpvs[allClusterIndexes[i]];
        restartManager::restart(ipv);
      }
      restartManager::restart(macroStrain);
      restartManager::restart(macroStress);
      restartManager::restart(macroEigenStrain);
      restartManager::restart(allStrains);
      restartManager::restart(allStresses);
      restartManager::restart(allTangents);
      restartManager::restart(allEigenStrains);
      restartManager::restart(alldEigenStrainsdStrains);
      restartManager::restart(alldEigenStrainFramedStrainsCluster);
      restartManager::restart(allSecants);
      restartManager::restart(strainFrame);
      restartManager::restart(eigenStrainFrame);
      restartManager::restart(dEigenStraindStrainFrame);
      restartManager::restart(stiffnessIso);
      restartManager::restart(stiffnessIsoInv);
      restartManager::restart(stiffnessAniso);
      restartManager::restart(stiffnessAnisoInv);
      restartManager::restart(secantShearModulus);
      restartManager::restart(tangentShearModulus);
      restartManager::restart(elasticShearModulus);
      restartManager::restart(macroTangent);
      restartManager::restart(macroElasticTangent);
      restartManager::restart(upperClusterIDs);
    };
    
    virtual int getClustersNb() const {return clustersNb;};
    
    virtual const STensor3& getConstRefToMacroStrain() const {return macroStrain;};
    virtual STensor3& getRefToMacroStrain() {return macroStrain;};
    
    virtual const STensor3& getConstRefToMacroStress() const {return macroStress;};
    virtual STensor3& getRefToMacroStress() {return macroStress;};
    
    virtual const STensor43& getConstRefToMacroTangent() const {return macroTangent;};
    virtual STensor43& getRefToMacroTangent() {return macroTangent;};
    
    virtual const STensor43& getConstRefToMacroElasticTangent() const {return macroElasticTangent;};
    virtual STensor43& getRefToMacroElasticTangent() {return macroElasticTangent;};
    
    virtual const STensor3& getConstRefToMacroEigenStrain() const {return macroEigenStrain;};
    virtual STensor3& getRefToMacroEigenStrain() {return macroEigenStrain;};
    
    virtual const STensor3& getConstRefToFrameStrain() const {return strainFrame;};
    virtual STensor3& getRefToFrameStrain() {return strainFrame;};
    
    virtual const STensor3& getConstRefToFrameEigenStrain() const {return eigenStrainFrame;};
    virtual STensor3& getRefToFrameEigenStrain() {return eigenStrainFrame;};
    
    virtual const STensor43& getConstRefToFramedEigenStraindStrain() const {return dEigenStraindStrainFrame;};
    virtual STensor43& getRefToFramedEigenStraindStrain() {return dEigenStraindStrainFrame;};
    
    virtual const STensor43& getConstRefToIsotropicStiffness() const {return stiffnessIso;};
    virtual STensor43& getRefToIsotropicStiffness() {return stiffnessIso;};
    
    virtual const STensor43& getConstRefToIsotropicStiffnessInverted() const {return stiffnessIsoInv;};
    virtual STensor43& getRefToIsotropicStiffnessInverted() {return stiffnessIsoInv;};
    
    virtual const STensor43& getConstRefToAnisotropicStiffness() const {return stiffnessAniso;};
    virtual STensor43& getRefToAnisotropicStiffness() {return stiffnessAniso;};
    
    virtual const STensor43& getConstRefToAnisotropicStiffnessInverted() const {return stiffnessAnisoInv;};
    virtual STensor43& getRefToAnisotropicStiffnessInverted() {return stiffnessAnisoInv;};
    
    virtual const double& getConstRefToSecantShearModulus() const {return secantShearModulus;};
    virtual double& getRefToSecantShearModulus() {return secantShearModulus;};
    
    virtual const double& getConstRefToTangentShearModulus() const {return tangentShearModulus;};
    virtual double& getRefToTangentShearModulus() {return tangentShearModulus;};
    
    virtual const double& getConstRefToElasticShearModulus() const {return elasticShearModulus;};
    virtual double& getRefToElasticShearModulus() {return elasticShearModulus;};
    
    virtual const IPVariable* getConstRefToIPv(int cl) const
    {
      std::map<int,IPVariable*>::const_iterator it = allIpvs.find(cl);
      if (it == allIpvs.end())
      {
        Msg::Error("ClusteringData::getConstRefToIPv cluster %d is not found",cl);
        return NULL;
      }
      else
      {
        if(it->second == NULL)  Msg::Error("ClusteringData::getConstRefToIPv cluster %d is not initialized",cl);
        return it->second;
      }
    }
    virtual IPVariable*& getRefToIPv(int cl)
    {
      std::map<int,IPVariable*>::iterator it = allIpvs.find(cl);
      if (it == allIpvs.end())
      {
        Msg::Error("ClusteringData::getRefToIPv cluster %d is not found",cl);
        Msg::Exit(0);
      }
      return it->second;
    }

    const STensor3 &getConstRefToStrain(int cl) const
    {
      std::map<int,STensor3>::const_iterator it = allStrains.find(cl);
      if (it == allStrains.end())
      {
        Msg::Error("ClusteringData::getConstRefToStrain cluster %d is not found",cl);
        static STensor3 tmp;
        return tmp;
      }
      else
      {
        return it->second;
      }
    }
    STensor3 &getRefToStrain(int cl)
    {
      std::map<int,STensor3>::iterator it = allStrains.find(cl);
      if (it == allStrains.end())
      {
        Msg::Error("ClusteringData::getRefToStrain cluster %d is not found",cl);
        static STensor3 tmp;
        return tmp;
      }
      else
      {
        return it->second;
      }
    }

    const STensor3 &getConstRefToStress(int cl) const
    {
      std::map<int,STensor3>::const_iterator it = allStresses.find(cl);
      if (it == allStresses.end())
      {
        Msg::Error("ClusteringData::getConstRefToStress cluster %d is not found",cl);
        static STensor3 tmp;
        return tmp;
      }
      else
      {
        return it->second;
      }
    }

    STensor3 &getRefToStress(int cl)
    {
      std::map<int,STensor3>::iterator it = allStresses.find(cl);
      if (it == allStresses.end())
      {
        Msg::Error("ClusteringData::getRefToStress cluster %d is not found",cl);
        static STensor3 tmp;
        return tmp;
      }
      else
      {
        return it->second;
      }
    }
    
    const STensor43 &getConstRefToTangent(int cl) const
    {
      std::map<int,STensor43>::const_iterator it = allTangents.find(cl);
      if (it == allTangents.end())
      {
        Msg::Error("ClusteringData::getConstRefToTangent cluster %d is not found",cl);
        static STensor43 tmp;
        return tmp;
      }
      else
      {
        return it->second;
      }
    }
    STensor43 &getRefToTangent(int cl)
    {
      std::map<int,STensor43>::iterator it = allTangents.find(cl);
      if (it == allTangents.end())
      {
        Msg::Error("ClusteringData::getRefToTangent cluster %d is not found",cl);
        static STensor43 tmp;
        return tmp;
      }
      else
      {
        return it->second;
      }
    }
    
    const STensor43 &getConstRefToSecant(int cl) const
    {
      std::map<int,STensor43>::const_iterator it = allSecants.find(cl);
      if (it == allSecants.end())
      {
        Msg::Error("ClusteringData::getConstRefToSecant cluster %d is not found",cl);
        static STensor43 tmp;
        return tmp;
      }
      else
      {
        return it->second;
      }
    }
    STensor43 &getRefToSecant(int cl)
    {
      std::map<int,STensor43>::iterator it = allSecants.find(cl);
      if (it == allSecants.end())
      {
        Msg::Error("ClusteringData::getRefToSecant cluster %d is not found",cl);
        static STensor43 tmp;
        return tmp;
      }
      else
      {
        return it->second;
      }
    }
    
    const STensor3 &getConstRefToEigenStrain(int cl) const
    {
      std::map<int,STensor3>::const_iterator it = allEigenStrains.find(cl);
      if (it == allEigenStrains.end())
      {
        Msg::Error("ClusteringData::getConstRefToEigenStrain cluster %d is not found",cl);
        static STensor3 tmp;
        return tmp;
      }
      else
      {
        return it->second;
      }
    }
    STensor3 &getRefToEigenStrain(int cl)
    {
      std::map<int,STensor3>::iterator it = allEigenStrains.find(cl);
      if (it == allEigenStrains.end())
      {
        Msg::Error("ClusteringData::getRefToEigenStrain cluster %d is not found",cl);
        static STensor3 tmp;
        return tmp;
      }
      else
      {
        return it->second;
      }
    }

    const STensor43 &getConstRefTodEigenStraindStrain(int cl) const
    {
      std::map<int,STensor43>::const_iterator it = alldEigenStrainsdStrains.find(cl);
      if (it == alldEigenStrainsdStrains.end())
      {
        Msg::Error("ClusteringData::getConstRefTodEigenStraindStrain cluster %d is not found",cl);
        static STensor43 tmp;
        return tmp;
      }
      else
      {
        return it->second;
      }
    }
    STensor43 &getRefTodEigenStraindStrain(int cl)
    {
      std::map<int,STensor43>::iterator it = alldEigenStrainsdStrains.find(cl);
      if (it == alldEigenStrainsdStrains.end())
      {
        Msg::Error("ClusteringData::getRefTodEigenStraindStrain cluster %d is not found",cl);
        static STensor43 tmp;
        return tmp;
      }
      else
      {
        return it->second;
      }
    }   
    
    const STensor43 &getConstRefTodEigenStrainFramedStrainCluster(int cl) const
    {
      std::map<int,STensor43>::const_iterator it = alldEigenStrainFramedStrainsCluster.find(cl);
      if (it == alldEigenStrainFramedStrainsCluster.end())
      {
        Msg::Error("ClusteringData::getConstRefTodEigenStrainFramedStrainCluster cluster %d is not found",cl);
        static STensor43 tmp;
        return tmp;
      }
      else
      {
        return it->second;
      }
    }
    STensor43 &getRefTodEigenStrainFramedStrainCluster(int cl)
    {
      std::map<int,STensor43>::iterator it = alldEigenStrainFramedStrainsCluster.find(cl);
      if (it == alldEigenStrainFramedStrainsCluster.end())
      {
        Msg::Error("ClusteringData::getRefTodEigenStrainFramedStrainCluster cluster %d is not found",cl);
        static STensor43 tmp;
        return tmp;
      }
      else
      {
        return it->second;
      }
    }
};


class ClusteringData: public GenericClusteringData
{
  protected:
    
  public:
    ClusteringData(const std::vector<int>& allCl, const std::vector<double>& allVf): GenericClusteringData(allCl,allVf) {}
    ClusteringData(const ClusteringData& src): GenericClusteringData(src) {}
    ClusteringData& operator=(const ClusteringData& src)
    {
      bool compatible = true;
      if (clustersNb != src.clustersNb)
      {
        compatible = false;
      }
      else
      {
        // check if two ClusteringDatas are compatible
        for (std::map<int,IPVariable*>::const_iterator it = src.allIpvs.begin(); it!= src.allIpvs.end(); it++)
        {
          if (allIpvs.find(it->first) == allIpvs.end())
          {
            compatible = false;
            break;
          }
        }
      }
      if (!compatible)
      {
        Msg::Error("make an equal operator between two different clustering data num clusters left = %d  num clusters right = %d!!!",clustersNb,src.clustersNb);
        Msg::Exit(0);
      }
      else
      {
        allClusterIndexes = src.allClusterIndexes;
        allClusterVolumes = src.allClusterVolumes;
        macroStrain = src.macroStrain;
        macroStress = src.macroStress;
        macroEigenStrain = src.macroEigenStrain;
        strainFrame = src.strainFrame;
        eigenStrainFrame = src.eigenStrainFrame;
        dEigenStraindStrainFrame = src.dEigenStraindStrainFrame;
        stiffnessIso = src.stiffnessIso;
        stiffnessIsoInv = src.stiffnessIsoInv;
        stiffnessAniso = src.stiffnessAniso;
        stiffnessAnisoInv = src.stiffnessAnisoInv;
        secantShearModulus = src.secantShearModulus;
        tangentShearModulus = src.tangentShearModulus;
        elasticShearModulus = src.elasticShearModulus;
        macroTangent = src.macroTangent;
        macroElasticTangent = src.macroElasticTangent;
        upperClusterIDs = src.upperClusterIDs;
        for (int i=0; i< clustersNb; i++)
        {
          const IPVariable* ipvsrc = (src.allIpvs.find(allClusterIndexes[i]))->second;
          const STensor3& strainSrc = (src.allStrains.find(allClusterIndexes[i]))->second;
          const STensor3& stressSrc = (src.allStresses.find(allClusterIndexes[i]))->second;
          const STensor43& tangentSrc = (src.allTangents.find(allClusterIndexes[i]))->second;
          const STensor3& eigenStrainSrc = (src.allEigenStrains.find(allClusterIndexes[i]))->second;
          const STensor43& deigenStrainSrc = (src.alldEigenStrainsdStrains.find(allClusterIndexes[i]))->second;
          const STensor43& deigenStrainFramedStrainsClusterSrc = (src.alldEigenStrainFramedStrainsCluster.find(allClusterIndexes[i]))->second;
          const STensor43& secantSrc = (src.allSecants.find(allClusterIndexes[i]))->second;
          
          if (ipvsrc!=NULL)
          {
            if (allIpvs[allClusterIndexes[i]] != NULL)
            {
              allIpvs[allClusterIndexes[i]]->operator=(*ipvsrc);
            }
            else
            {
              allIpvs[allClusterIndexes[i]] = ipvsrc->clone();
            }
          }
          else
          {
            if (allIpvs[allClusterIndexes[i]] != NULL)
            {
              delete allIpvs[allClusterIndexes[i]];
              allIpvs[allClusterIndexes[i]] = NULL;
            }
          }
          allStrains[allClusterIndexes[i]] = strainSrc;
          allStresses[allClusterIndexes[i]] = stressSrc;
          allTangents[allClusterIndexes[i]] = tangentSrc;
          allEigenStrains[allClusterIndexes[i]] = eigenStrainSrc;
          alldEigenStrainsdStrains[allClusterIndexes[i]] = deigenStrainSrc;
          alldEigenStrainFramedStrainsCluster[allClusterIndexes[i]] = deigenStrainFramedStrainsClusterSrc;
          allSecants[allClusterIndexes[i]] = secantSrc;
        }
      }
      return *this;
    };
};

/*class ClusteringDataLevel1 : public GenericClusteringData
{    
  protected:
    std::map<int,int> clustersNbsLv1; //total numbers of subclusters per upper domain cluster
    std::map<int,std::vector<int>> allClusterIndexesLv1;
    std::map<int,std::vector<double>> allClusterVolumesLv1;
    std::map<int,std::map<int, IPVariable*>> allIpvsLv1;
    std::map<int,std::map<int, STensor3>> allStrainsLv1; //underlying law ipv
    std::map<int,std::map<int, STensor3>> allStressesLv1; // local stress in cluster
    std::map<int,std::map<int, STensor43>> allTangentsLv1; // local tangent in cluster
    std::map<int,std::map<int, STensor3>> allEigenStrainsLv1; // eigenstrains used in reduction formulation
    std::map<int,std::map<int, STensor43>> alldEigenStrainsdStrainsLv1; // derivarives of eigenstrains
    
    std::map<int,STensor3> macroStrainsLv1;
    std::map<int,STensor3> macroStressesLv1;
    std::map<int,STensor3> macroEigenStrainsLv1;    
    std::map<int,STensor43> macroTangentsLv1;
    std::map<int,STensor43> macroElasticTangentsLv1;
    
  public:
  
    ClusteringDataLevel1(const std::map<int,std::vector<int>>& allClLv1, const std::map<int,std::vector<double>>& allVfLv1): GenericClusteringData(allClLv1,allVfLv1), 
    allClusterIndexesLv1(allClLv1), allClusterVolumesLv1(allVfLv1)
    {
      for (std::map<int,std::vector<int>>::const_iterator itLv0=allClLv1.begin(); itLv0!=allClLv1.end(); itLv0++)
      {
        int clLv0 = itLv0->first;
        std::vector<int> allClOnLv1 = itLv0->second;
        int clusterNbLv1 = allClOnLv1.size();
        clustersNbsLv1[clLv0] = clusterNbLv1;
        macroStrainsLv1.insert(std::pair<int,STensor3>(clLv0,STensor3(0.)));
        macroStressesLv1.insert(std::pair<int,STensor3>(clLv0,STensor3(0.)));
        macroEigenStrainsLv1.insert(std::pair<int,STensor3>(clLv0,STensor3(0.)));
        macroTangentsLv1.insert(std::pair<int,STensor43>(clLv0,STensor43(0.)));
        macroElasticTangentsLv1.insert(std::pair<int,STensor43>(clLv0,STensor43(0.)));
        std::vector<int> allClusterIndexesOnLv1 = allClusterIndexesLv1[clLv0];
        std::map<int, IPVariable*> allIpvsOnLv1 = allIpvsLv1[clLv0];
        std::map<int, STensor3> allStrainsOnLv1 = allStrainsLv1[clLv0];
        std::map<int, STensor3> allStressesOnLv1 = allStressesLv1[clLv0];
        std::map<int, STensor3> allEigenStrainsOnLv1 = allEigenStrainsLv1[clLv0];
        std::map<int, STensor43> allTangentsOnLv1 = allTangentsLv1[clLv0];
        std::map<int, STensor43> alldEigenStrainsdStrainsOnLv1 = alldEigenStrainsdStrainsLv1[clLv0];
        for (int itLv1=0; itLv1< clustersNb; itLv1++)
        {
          int clLv1 = allClusterIndexesOnLv1[itLv1];
          allIpvsOnLv1.insert(std::pair<int,IPVariable*>(clLv1,NULL));
          allStrainsOnLv1.insert(std::pair<int,STensor3>(clLv1,STensor3(0.)));
          allStressesOnLv1.insert(std::pair<int,STensor3>(clLv1,STensor3(0.)));
          allEigenStrainsOnLv1.insert(std::pair<int,STensor3>(clLv1,STensor3(0.)));
          allTangentsOnLv1.insert(std::pair<int,STensor43>(clLv1,STensor43(0.)));
          alldEigenStrainsdStrainsOnLv1.insert(std::pair<int,STensor43>(clLv1,STensor43(0.)));
        }
      }
    }
    
    ClusteringDataLevel1(const ClusteringDataLevel1& src): GenericClusteringData(src), 
    clustersNbsLv1(src.clustersNbsLv1), macroStrainsLv1(src.macroStrainsLv1),
    macroStressesLv1(src.macroStressesLv1), macroEigenStrainsLv1(src.macroEigenStrainsLv1), macroTangentsLv1(src.macroTangentsLv1), macroElasticTangentsLv1(src.macroElasticTangentsLv1),
    allStrainsLv1(src.allStrainsLv1), 
    allStressesLv1(src.allStressesLv1), allEigenStrainsLv1(src.allEigenStrainsLv1), allTangentsLv1(src.allTangentsLv1), alldEigenStrainsdStrainsLv1(src.alldEigenStrainsdStrainsLv1)
    {
      allIpvs.clear();
      for (std::map<int,std::map<int, IPVariable*>>::const_iterator itLv0 = src.allIpvsLv1.begin(); itLv0!=src.allIpvsLv1.end(); itLv0++)
      {
        std::map<int, IPVariable*> allIpvsOnLv1 = itLv0->second;
        for(std::map<int, IPVariable*>::const_iterator itLv1 = allIpvsOnLv1.begin(); itLv1!=allIpvsOnLv1.end(); itLv1++)
        {
          if (itLv1->second != NULL)
          {
            allIpvsOnLv1[itLv1->first] = itLv1->second->clone();
          }
          else
          {
            allIpvsOnLv1[itLv1->first] = NULL;
          }
        }
      }
    }
    
    
    ClusteringDataLevel1& operator=(const ClusteringDataLevel1& src)
    {
      bool compatible = true;
      if (clustersNb != src.clustersNb)
      {
        compatible = false;
      }
      else
      {
        // check if two ClusteringDatas are compatible
        for (std::map<int,IPVariable*>::const_iterator it = src.allIpvs.begin(); it!= src.allIpvs.end(); it++)
        {
          if (allIpvs.find(it->first) == allIpvs.end())
          {
            compatible = false;
            break;
          }
        }
        for(std::map<int,int>::const_iterator itLv0 = src.clustersNbsLv1.begin(); itLv0 != src.clustersNbsLv1.end(); itLv0++)
        {
          if(clustersNbsLv1[itLv0->first] != itLv0->second)
          {
            compatible = false;
            break;
          }
        }
        for (std::map<int,std::map<int,IPVariable*>>::const_iterator itLv0 = src.allIpvsLv1.begin(); itLv0!= src.allIpvsLv1.end(); itLv0++)
        {
          std::map<int,IPVariable*> allIpvsOnLv1Src = itLv0->second;
          std::map<int,IPVariable*>& allIpvsOnLv1 = allIpvsLv1[itLv0->first];
          for(std::map<int,IPVariable*>::const_iterator itLv1 = allIpvsOnLv1Src.begin(); itLv1 != allIpvsOnLv1Src.end(); itLv1++)
          {
            if (allIpvsOnLv1.find(itLv1->first) == allIpvsOnLv1.end())
            {
              compatible = false;
              break;
            }
          }
        }
      }
      if (!compatible)
      {
        Msg::Error("make an equal operator between two different level 1 clustering data !!!");
        Msg::Exit(0);
      }
      else
      {
        allClusterIndexes = src.allClusterIndexes;        
        allClusterVolumes = src.allClusterVolumes;
        macroStrain = src.macroStrain;
        macroStress = src.macroStress;
        macroEigenStrain = src.macroEigenStrain;
        macroTangent = src.macroTangent;
               
        for (int itLv0=0; itLv0< clustersNb; itLv0++)
        {
          int clLv0 = allClusterIndexes[itLv0];
          
          macroStrainsLv1[clLv0] = (src.macroStrainsLv1.find(clLv0))->second;
          macroStressesLv1[clLv0] = (src.macroStressesLv1.find(clLv0))->second;
          macroEigenStrainsLv1[clLv0] = (src.macroEigenStrainsLv1.find(clLv0))->second;
          macroTangentsLv1[clLv0] = (src.macroTangentsLv1.find(clLv0))->second;
          macroElasticTangentsLv1[clLv0] = (src.macroElasticTangentsLv1.find(clLv0))->second;
          
          allClusterIndexesLv1[clLv0] = (src.allClusterIndexesLv1.find(clLv0))->second;
          allClusterVolumesLv1[clLv0] = (src.allClusterVolumesLv1.find(clLv0))->second;
          
          const std::vector<int>& allClusterIndexesOnLv1 = (src.allClusterIndexesLv1.find(clLv0))->second;
          
          const std::map<int, IPVariable*>& allIpvsOnLv1FromSrc = (src.allIpvsLv1.find(clLv0))->second;
          const std::map<int, STensor3>& allStrainsOnLv1FromSrc = (src.allStrainsLv1.find(clLv0))->second;
          const std::map<int, STensor3>& allStressesOnLv1FromSrc = (src.allStressesLv1.find(clLv0))->second;
          const std::map<int, STensor3>& allEigenStrainsOnLv1FromSrc = (src.allEigenStrainsLv1.find(clLv0))->second;
          const std::map<int, STensor43>& allTangentsOnLv1FromSrc = (src.allTangentsLv1.find(clLv0))->second;
          const std::map<int, STensor43>& alldEigenStrainsdStrainsOnLv1FromSrc = (src.alldEigenStrainsdStrainsLv1.find(clLv0))->second;
          
          std::map<int, IPVariable*>& allIpvsOnLv1 = allIpvsLv1[clLv0];
          std::map<int, STensor3>& allStrainsOnLv1 = allStrainsLv1[clLv0];
          std::map<int, STensor3>& allStressesOnLv1 = allStressesLv1[clLv0];
          std::map<int, STensor3>& allEigenStrainsOnLv1 = allEigenStrainsLv1[clLv0];
          std::map<int, STensor43>& allTangentsOnLv1 = allTangentsLv1[clLv0];
          std::map<int, STensor43>& alldEigenStrainsdStrainsOnLv1 = alldEigenStrainsdStrainsLv1[clLv0];
          
          int clustersNbOnLv1 = clustersNbsLv1[clLv0];          
          for(int itLv1=0; itLv1<clustersNbOnLv1; itLv1++)
          {
            int clLv1 = allClusterIndexesOnLv1[itLv1];
            
            const IPVariable* ipvsrc = (allIpvsOnLv1FromSrc.find(clLv1))->second;      
            const STensor3& strainSrc = (allStrainsOnLv1FromSrc.find(clLv1))->second;
            const STensor3& stressSrc = (allStressesOnLv1FromSrc.find(clLv1))->second;
            const STensor3& eigenStrainSrc = (allEigenStrainsOnLv1FromSrc.find(clLv1))->second;
            const STensor43& tangentSrc = (allTangentsOnLv1FromSrc.find(clLv1))->second;
            const STensor43& dEigenStrainSrc = (alldEigenStrainsdStrainsOnLv1FromSrc.find(clLv1))->second;
            
            if (ipvsrc!=NULL)
            {
              if (allIpvsOnLv1[clLv1] != NULL)
              {
                allIpvsOnLv1[clLv1]->operator=(*ipvsrc);
              }
              else
              {
                allIpvsOnLv1[clLv1] = ipvsrc->clone();
              }
            }
            else
            {
              if (allIpvsOnLv1[clLv1] != NULL)
              {
                delete allIpvsOnLv1[clLv1];
                allIpvsOnLv1[clLv1] = NULL;
              }
            }
            allStrainsOnLv1[clLv1] = strainSrc;
            allStressesOnLv1[clLv1] = stressSrc;
            allTangentsOnLv1[clLv1] = tangentSrc;
            allEigenStrainsOnLv1[clLv1] = eigenStrainSrc;
            alldEigenStrainsdStrainsOnLv1[clLv1] = dEigenStrainSrc;          
          }         
        }
      }
      return *this;
    };
    
    virtual const IPVariable* getConstRefToIPv(int clLv0, int clLv1) const
    {
      std::map<int,std::map<int,IPVariable*>>::const_iterator itLv0 = allIpvsLv1.find(clLv0);
      if (itLv0 == allIpvsLv1.end())
      {
        Msg::Error("ClusteringDataLevel1::getConstRefToIPv cluster level 0: %d is not found",clLv0);
        return NULL;
      }
      else
      {
        std::map<int,IPVariable*> allIpvsOnLv1 = itLv0->second;
        std::map<int,IPVariable*>::const_iterator itLv1 = allIpvsOnLv1.find(clLv1);
        if (itLv1 == allIpvsOnLv1.end())
        {
          Msg::Error("ClusteringDataLevel1::getConstRefToIPv cluster level 0, level 1: %d,%d is not found",clLv0,clLv1);
          return NULL;
        }
        else
        {
          if(itLv1->second == NULL)  Msg::Error("ClusteringDataLevel1::getConstRefToIPv cluster level 0, level 1: %d,%d is not initialized",clLv0,clLv1);
          return itLv1->second;
        }
      }
    }
    virtual IPVariable*& getRefToIPv(int clLv0, int clLv1)
    {
      std::map<int,std::map<int,IPVariable*>>::iterator itLv0 = allIpvsLv1.find(clLv0);
      if (itLv0 == allIpvsLv1.end())
      {
        Msg::Error("ClusteringDataLevel1::getConstRefToIPv cluster level 0: %d is not found",clLv0);
        Msg::Exit(0);
      }
      else
      {
        std::map<int,IPVariable*> allIpvsOnLv1 = itLv0->second;
        std::map<int,IPVariable*>::iterator itLv1 = allIpvsOnLv1.find(clLv1);
        if (itLv1 == allIpvsOnLv1.end())
        {
          Msg::Error("ClusteringDataLevel1::getRefToIPv cluster level 0, level 1: %d,%d is not found",clLv0,clLv1);
          Msg::Exit(0);
        }
        return itLv1->second;
      }
    }
};*/
#endif // CLUSTERINGDATA_H_
