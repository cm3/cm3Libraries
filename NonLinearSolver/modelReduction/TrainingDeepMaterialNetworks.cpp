//
// C++ Interface: binary tree
//
//
// Author:  <V.-D. Nguyen>, (C) 2020
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "TrainingDeepMaterialNetworks.h"
#include <stdio.h>
#include <random> 
#include <chrono>
#include <math.h>
#include "OS.h"
#include "elasticPotential.h"
#if defined(HAVE_PETSC)
#include "linearSystemPETSc.h"
#endif //HAVE_PETSC
#if defined(HAVE_GMM)
#include "linearSystemGmm.h"
#endif //HAVE_GMM

hyperFullMatrix::hyperFullMatrix(): _data(), _dimFirst(0), _dimSecond(0){}

hyperFullMatrix::hyperFullMatrix(int M, int N, double v): _data(N, std::vector<fullMatrix<double> >(N, fullMatrix<double>(M,M,true))), _dimFirst(M), _dimSecond(N)
{
  for (int i=0; i<_dimFirst; i++)
  {
    for (int j=0; j<_dimFirst; j++)
    {    
      for (int k=0; k<_dimSecond; k++)
      {
        for (int l=0; l < _dimSecond; l++)
        {
          if((i == k) && (j == l)) _data[k][l](i,j)  += 0.5*v;
          if((i == l) && (j == k)) _data[k][l](i,j)  += 0.5*v;
        }
      }
    }
  } 
};

hyperFullMatrix& hyperFullMatrix::operator=(const hyperFullMatrix& src)
{
  if (_dimFirst != src._dimFirst || _dimSecond != src._dimSecond)
  {
    clear();
    _dimFirst = src._dimFirst;
    _dimSecond = src._dimSecond;
    _data.resize(_dimSecond, std::vector<fullMatrix<double> >(_dimSecond, fullMatrix<double>(_dimFirst,_dimFirst,true)));
  }
  for (int i=0; i< _dimFirst; i++)
  {
    for (int j=0; j< _dimFirst; j++)
    {
      for (int k=0; k< _dimSecond; k++)
      {
        for (int l=0; l< _dimSecond; l++)
        {
          (*this)(i,j,k,l) = src(i,j,k,l);
        }
      }
    }
  }
  return *this;
};

void hyperFullMatrix::allocate(int M, int N, double v)
{
  
  if (_dimFirst != M || _dimSecond != N)
  {
    clear();
    _dimFirst = M;
    _dimSecond  =  N;
    _data.resize(N, std::vector<fullMatrix<double> >(N, fullMatrix<double>(M,M,true)));
  }
  else
  {
    for (int k=0; k< _dimSecond; k++)
    {
      for (int l=0; l< _dimSecond; l++)
      {
        _data[k][l].setAll(0.);
      }
    }
  }
  
  if (v != 0.)
  {
    // set values
    for (int i=0; i<_dimFirst; i++)
    {
      for (int j=0; j<_dimFirst; j++)
      {    
        for (int k=0; k<_dimSecond; k++)
        {
          for (int l=0; l < _dimSecond; l++)
          {
            if((i == k) && (j == l)) _data[k][l](i,j)  += 0.5*v;
            if((i == l) && (j == k)) _data[k][l](i,j)  += 0.5*v;
          }
        }
      }
    } 
  }
}

int hyperFullMatrix::getFirstDim() const {return _dimFirst;};
int hyperFullMatrix::getSecondDim() const {return _dimSecond;};
    
void hyperFullMatrix::clear()
{
  _dimFirst = 0;
  _dimSecond = 0;
  _data.clear();
}
const fullMatrix<double>& hyperFullMatrix::getMatrix(int k, int l) const {return _data[k][l];};
fullMatrix<double>& hyperFullMatrix::getMatrix(int k, int l) {return _data[k][l];};

void hyperFullMatrix::axpy(const hyperFullMatrix& x, double a)
{
  if (x.getFirstDim() != _dimFirst || x.getSecondDim() != _dimSecond)
  {
    Msg::Error("operation hyperFullMatrix::axpy cannot be done");
    Msg::Exit(0);
  }
  for (int k=0; k< _dimSecond; k++)
  {
    for (int l=0; l< _dimSecond; l++)
    {
      for (int i=0; i< _dimFirst; i++)
      {
        for (int j=0; j< _dimFirst; j++)
        {
          (*this)(i,j,k,l) += a*x(i,j,k,l);
        }
      }
    }
  };
  
}

void hyperFullMatrix::setAll(double a)
{
  for (int k=0; k< _dimSecond; k++)
  {
    for (int l=0; l< _dimSecond; l++)
    {
      _data[k][l].setAll(a);
    }
  }
}
void hyperFullMatrix::scale(double a)
{
  for (int k=0; k< _dimSecond; k++)
  {
    for (int l=0; l< _dimSecond; l++)
    {
      _data[k][l].scale(a);
    }
  }
};

void hyperFullMatrix::printData(std::string str, bool sym) const
{
  if (sym)
  {
    printf("SYM hyperFullMatrix data : %s \n",str.c_str());
    auto getIndex = [](int d, std::vector<std::pair<int,int> >& all)
    {
      all.clear();
      for (int i=0; i< d; i++)
      {
        all.push_back(std::pair<int,int>(i,i));
      }
      for (int i=0; i< d; i++)
      {
        for (int j=i+1; j< d; j++)
        {
          all.push_back(std::pair<int,int>(i,j));
        }
      }
    };
    
    std::vector<std::pair<int,int> > firstIndexes, secondIndexes;
    getIndex(_dimFirst,firstIndexes);
    getIndex(_dimSecond,secondIndexes);
    for (int row = 0; row < firstIndexes.size(); row++)
    {
      int i = firstIndexes[row].first;
      int j = firstIndexes[row].second;
      for (int col =0; col < secondIndexes.size(); col++)
      {
        int k = secondIndexes[col].first;
        int l = secondIndexes[col].second;
        if (k==l)
        {
          printf(" %12.5E", (*this)(i,j,k,l));
        }
        else
        {
          printf(" %12.5E", (*this)(i,j,k,l)+(*this)(i,j,l,k));
        }
      }
      printf("\n");
    };
  }
  else
  {
    printf("hyperFullMatrix data : %s \n",str.c_str());
    auto getIndexFull = [](int d, std::vector<std::pair<int,int> >& all)
    {
      all.clear();
      for (int i=0; i< d; i++)
      {
        for (int j=0; j< d; j++)
        {
          all.push_back(std::pair<int,int>(i,j));
        }
      }
    };
    std::vector<std::pair<int,int> > firstIndexes, secondIndexes;
    getIndexFull(_dimFirst,firstIndexes);
    getIndexFull(_dimSecond,secondIndexes);
    for (int row = 0; row < firstIndexes.size(); row++)
    {
      int i = firstIndexes[row].first;
      int j = firstIndexes[row].second;
      for (int col =0; col < secondIndexes.size(); col++)
      {
        int k = secondIndexes[col].first;
        int l = secondIndexes[col].second;
        printf(" %12.5E", (*this)(i,j,k,l));
      }
      printf("\n");
    };
  }
};

void fullMatrixOperation::allocateAndMakeZero(fullVector<double>& v, int row)
{
  if (v.size()!=row )
  {
    v.resize(row, true);
  }
  else
  {
    v.setAll(0.);
  }
}

void fullMatrixOperation::allocateAndMakeZero(fullMatrix<double>& A, int row, int col)
{
  if (A.size1()!=row || A.size2()!=col)
  {
    A.resize(row,col, true);
  }
  else
  {
    A.setAll(0.);
  }
};
void fullMatrixOperation::transpose(const fullMatrix<double>& A, fullMatrix<double>& AT)
{
  allocateAndMakeZero(AT,A.size2(),A.size1());
  for (int i=0; i< AT.size1(); i++)
  {
    for (int j=0; j< AT.size2(); j++)
    {
      AT(i,j) = A(j,i);
    }
  }
}
bool fullMatrixOperation::invertMatrix(const fullMatrix<double>& a, fullMatrix<double>& inva, bool stiff, hyperFullMatrix& DinvaDa)
{
  int N = a.size1();
  allocateAndMakeZero(inva,N,N);
  bool ok = true;
  if (N==1)
  {
    inva(0,0) = 1./a(0,0);
  }
  else if (N == 2)
  {
    double det = a(0,0)*a(1,1) -a(0,1)*a(1,0); 
    if(fabs(det) > 0.) 
    {
      double ud = 1. / det;
      inva(0,0) = a(1,1) * ud;
      inva(1,0) = -a(1,0) * ud;
      inva(0,1) = -a(0,1) * ud;
      inva(1,1) = a(0,0) * ud;
    }
    else 
    {
      ok = false;
      Msg::Error("matrix is not invertible, detmat = %.16g",det);
    }
  }
  else if (N == 3)
  {
    double detA =  (a(0,0) * (a(1,1) * a(2,2) - a(1,2) * a(2,1)) -
            a(0,1) * (a(1,0) * a(2,2) - a(1,2) * a(2,0)) +
            a(0,2) * (a(1,0) * a(2,1) - a(1,1) * a(2,0)));
            
    if (fabs(detA) > 0.)
    {
      double udet = 1./detA;
      inva(0,0) =  (a(1,1) * a(2,2) - a(1,2) * a(2,1))* udet;
      inva(1,0) = -(a(1,0) * a(2,2) - a(1,2) * a(2,0))* udet;
      inva(2,0) =  (a(1,0) * a(2,1) - a(1,1) * a(2,0))* udet;
      inva(0,1) = -(a(0,1) * a(2,2) - a(0,2) * a(2,1))* udet;
      inva(1,1) =  (a(0,0) * a(2,2) - a(0,2) * a(2,0))* udet;
      inva(2,1) = -(a(0,0) * a(2,1) - a(0,1) * a(2,0))* udet;
      inva(0,2) =  (a(0,1) * a(1,2) - a(0,2) * a(1,1))* udet;
      inva(1,2) = -(a(0,0) * a(1,2) - a(0,2) * a(1,0))* udet;
      inva(2,2) =  (a(0,0) * a(1,1) - a(0,1) * a(1,0))* udet;
    }
    else
    {
      ok = false;
    }
  }
  else
  {
    ok = a.invert(inva);
  }
  
  if (stiff)
  {
    DinvaDa.allocate(N,N,0.);
    for (int k=0; k<N; k++)
    {
      for (int l=0; l<N; l++)
      {          
        for (int i=0; i<N; i++)
        {
          for (int j=0; j<N; j++)
          {
            DinvaDa(i,j,k,l) = -0.5*(inva(i,k)*inva(j,l)+inva(i,l)*inva(j,k));
          }
        }
      }
    }
  }
  return ok;
};

void fullMatrixOperation::mult(const fullMatrix<double>& A, const fullMatrix<double>& B, const fullVector<double>& C, fullVector<double>& R)
{
  // R = A*B*C
  static fullMatrix<double> AB;
  allocateAndMakeZero(AB, A.size1(),B.size2());
  allocateAndMakeZero(R,A.size1());
  A.mult(B,AB);
  AB.mult(C,R);
};

void fullMatrixOperation::multPtAP(const fullMatrix<double>& P, const fullMatrix<double>& A, fullMatrix<double>& R)
{
  fullMatrix<double> PT = P.transpose();
  mult(PT,A,P,R);
}

void fullMatrixOperation::mult(const fullMatrix<double>& A, const fullMatrix<double>& B, fullMatrix<double>& R)
{
  allocateAndMakeZero(R,A.size1(),B.size2());
  A.mult(B,R);
}

void fullMatrixOperation::mult(const fullMatrix<double>& A, const fullMatrix<double>& B, const fullMatrix<double>& C, fullMatrix<double>& R)
{
  // R = A*B*C
  static fullMatrix<double> AB;
  allocateAndMakeZero(AB, A.size1(),B.size2());
  allocateAndMakeZero(R,A.size1(),C.size2());
  A.mult(B,AB);
  AB.mult(C,R);
};
void fullMatrixOperation::multAdd(const fullMatrix<double>& A, const fullMatrix<double>& B, const fullMatrix<double>& C, fullMatrix<double>& R, double fact)
{
  // R += A*B*C
  static fullMatrix<double> ABC;
  mult(A,B,C,ABC);
  R.axpy(ABC,fact);
};

double fullMatrixOperation::dot(const fullMatrix<double>& A, const fullMatrix<double>& B)
{
  if (A.size1() == B.size1() && A.size2() == B.size2())
  {
    double val =  0;
    for (int i=0; i< A.size1(); i++)
    {
      for (int j=0; j< A.size2(); j++)
      {
        val += A(i,j)*B(i,j);
      }
    }
    return val;
  }
  else
  {
    Msg::Error("two matrix is not compatible in dot operator");
    return 0.;
  }
}
void fullMatrixOperation::mult(const hyperFullMatrix& A, const hyperFullMatrix& B, hyperFullMatrix& C, double fact)
{
  C.allocate(A.getFirstDim(),B.getSecondDim());
  for (int i=0; i< A.getFirstDim(); i++)
  {
    for (int j=0; j< A.getFirstDim(); j++)
    {
      for (int k=0; k < A.getSecondDim(); k++)
      {
        for (int l=0; l< A.getSecondDim(); l++)
        {
          for (int p=0; p < B.getSecondDim(); p++)
          {
            for (int q=0; q< B.getSecondDim(); q++)
            {
              C(i,j,p,q) += fact*A(i,j,k,l)*B(k,l,p,q);
            }
          }
        }
      }
    }
  }
};
void fullMatrixOperation::multAdd(const hyperFullMatrix& A, const fullMatrix<double>& B, fullMatrix<double>& C, double fact)
{
  for (int i=0; i< A.getFirstDim(); i++)
  {
    for (int j=0; j< A.getFirstDim(); j++)
    {
      for (int k=0; k < A.getSecondDim(); k++)
      {
        for (int l=0; l< A.getSecondDim(); l++)
        {
          C(i,j) += fact*A(i,j,k,l)*B(k,l);
        }
      }
    }
  }
};


void fullMatrixOperation::mult(const hyperFullMatrix& A, const fullMatrix<double>& B, fullMatrix<double>& C)
{
  allocateAndMakeZero(C,A.getFirstDim(),A.getFirstDim());
  multAdd(A,B,C,1.);
};

void fullMatrixOperation::reduceMat99ToMat66(const fullMatrix<double>& mat9x9, fullMatrix<double>& mat6x6)
{
  if (mat9x9.size1() != 9 || mat9x9.size2() != 9)
  {
    Msg::Error("9x9 matrix must be used to reduce to 6x6 matrix");
  }
  allocateAndMakeZero(mat6x6,6,6);
  auto getIndex = [](int k, int& i, int& j)
  {
    if (k == 0) {i=0; j=0;}
    else if (k == 1) {i=1; j=1;}
    else if (k == 2) {i=2; j=2;}
    else if (k == 3) {i=0; j=1;}
    else if (k == 4) {i=0; j=2;}
    else if (k == 5) {i=1; j=2;}
    else
    {
      Msg::Error("index %d cannot larger than 5");
    }
  };
  for (int row = 0; row <6; row++)
  {
    int i,j;
    getIndex(row,i,j);
    int fullRow = Tensor23::getIndex(i,j);
    for (int col = 0; col <6; col++)
    {
      int k,l;
      getIndex(col,k,l);
      int fullCol= Tensor23::getIndex(k,l);
      mat6x6(row,col) = mat9x9(fullRow,fullCol);
    }
  };
};

void fullMatrixOperation::getRow(const fullMatrix<double>& src, int row, fullVector<double>& rowVec)
{
  fullMatrixOperation::allocateAndMakeZero(rowVec,src.size2());
  for (int i=0; i< src.size2(); i++)
  {
    rowVec(i) = src(row,i);
  };
};

void fullMatrixOperation::getCol(const fullMatrix<double>& src, int col, fullVector<double>& colVec)
{
  fullMatrixOperation::allocateAndMakeZero(colVec,src.size1());
  for (int i=0; i< src.size1(); i++)
  {
    colVec(i) = src(i, col);
  };
}

void fullMatrixOperation::getRowColTangent(int dim, int i, int j, int k, int l, int& row, int& col)
{
  if (dim == 2)
  {
    // [0000 0011 0001 1111 1101 0101]
    if (i==0 && j==0 && k==0 &&l==0) 
    {
      row = 0;
      col = 0;
    }
    else if (i==0 && j==0 && k==1 &&l==1)
    {
      row = 0; 
      col = 1;
    }
    else if (i==0 && j==0 && k==0 &&l==1)
    {
      row = 0;
      col = 2;
    }
    else if (i==1 && j==1 && k==1 &&l==1)
    {
      row = 1;
      col = 1;
    }
    else if (i==1 && j==1 && k==0 &&l==1)
    {
      row = 1;
      col = 2;
    }
    else if (i==0 && j==1 && k==0 &&l==1)
    {
      row = 2;
      col = 2;
    }
    else
    {
      Msg::Error("wrong index %d %d %d %d",i,j,k,l);
      Msg::Exit(0);
    }
  }
  else if (dim == 3)
  {
    // [0000 0011 0022 0001 0002 0012 1111 1122 1101 1102 1112 2222 2201 2202 2212 0101 0102 0112 0202 0212 1212]
    if (i==0 && j==0 && k==0 &&l==0)
    {
      row = 0;
      col = 0;
    }
    else if (i==0 && j==0 && k==1 &&l==1)
    {
      row = 0;
      col = 1;
    }
    else if (i==0 && j==0 && k==2 &&l==2)
    {
      row = 0;
      col = 2;
    }
    else if (i==0 && j==0 && k==0 &&l==1)
    {
      row = 0;
      col = 3;
    }
    else if (i==0 && j==0 && k==0 &&l==2){
      row = 0;
      col = 4;
    }
    else if (i==0 && j==0 && k==1 &&l==2)
    {
      row = 0;
      col = 5;
    }
    else if (i==1 && j==1 && k==1 &&l==1){
      row = 1;
      col = 1;
    }
    else if (i==1 && j==1 && k==2 &&l==2)
    {
      row = 1;
      col = 2;
    }
    else if (i==1 && j==1 && k==0 &&l==1)
    {
      row = 1;
      col = 3;
    }
    else if (i==1 && j==1 && k==0 &&l==2)
    {
      row = 1;
      col = 4;
    }
    else if (i==1 && j==1 && k==1 &&l==2)
    {
      row = 1;
      col = 5;
    }
    else if (i==2 && j==2 && k==2 &&l==2)
    {
      row = 2;
      col = 2;
    }
    else if (i==2 && j==2 && k==0 &&l==1)
    {
      row = 2;
      col = 3;
    }
    else if (i==2 && j==2 && k==0 &&l==2)
    {
      row = 2;
      col = 4;
    }
    else if (i==2 && j==2 && k==1 &&l==2)
    {
      row = 2;
      col = 5;
    }
    else if (i==0 && j==1 && k==0 &&l==1)
    {
      row = 3;
      col = 3;
    }
    else if (i==0 && j==1 && k==0 &&l==2)
    {
      row = 3;
      col = 4;
    }
    else if (i==0 && j==1 && k==1 &&l==2)
    {
      row = 3;
      col = 5;
    }
    else if (i==0 && j==2 && k==0 &&l==2)
    {
      row = 4;
      col = 4;
    }
    else if (i==0 && j==2 && k==1 &&l==2)
    {
      row = 4;
      col = 5;
    }
    else if (i==1 && j==2 && k==1 &&l==2)
    {
      row = 5;
      col = 5;
    }
    else
    {
      Msg::Error("wrong index");
      Msg::Exit(0);
    }
  }
  else
  {
    Msg::Error("dim %d has not been implemented !!!",dim);
    Msg::Exit(0);
  }
};

void fullMatrixOperation::copyRow(const fullMatrix<double>& Src, int rowSrc, fullMatrix<double>& Dest, int rowDest, double fact)
{
  if (Src.size2() != Dest.size2())
  {
    Msg::Error("row is not equal between two matix");
    return;
  }
  
  for (int c=0; c< Src.size2(); c++)
  {
    Dest(rowDest,c) = Src(rowSrc,c)*fact;
  }
};

void fullMatrixOperation::copyMultipleRows(const fullMatrix<double>& Src, const std::vector<int>& rowSrc, 
                                          fullMatrix<double>& Dest, const std::vector<int>& rowDest, double fact)
{
  if (Src.size2() != Dest.size2() || rowSrc.size() != rowDest.size())
  {
    Msg::Error("row is not equal between two matix");
    return;
  }
  for (int r=0; r< rowSrc.size(); r++)
  {
    for (int c=0; c< Src.size2(); c++)
    {
      Dest(rowDest[r],c) = Src(rowSrc[r],c)*fact;
    }
  }
}

void fullMatrixOperation::getMatrixN(bool planeStrain, double n0, double n1, double n2, fullMatrix<double>& N)
{
  if (planeStrain)
  {
    fullMatrixOperation::allocateAndMakeZero(N,3,2);
    N(0,0) = n0;
    N(1,1) = n1;
    N(2,0) = n1;
    N(2,1) = n0;
  }
  else
  {
    fullMatrixOperation::allocateAndMakeZero(N,6,3);
    N(0,0) = n0;
    N(1,1) = n1;
    N(2,2) = n2;
    N(3,0) = n1;
    N(3,1) = n0;
    N(4,0) = n2;
    N(4,2) = n0;
    N(5,1) = n2;
    N(5,2) = n1;
  };
};

void fullMatrixOperation::getMatrixN(bool planeStrain, double n0, double n1, double n2, fullMatrix<double>& N, 
            bool stiff,
            fullMatrix<double>& DNDn0, 
            fullMatrix<double>& DNDn1, 
            fullMatrix<double>& DNDn2)
{
  if (planeStrain)
  {
    fullMatrixOperation::allocateAndMakeZero(N,3,2);
    N(0,0) = n0;
    N(1,1) = n1;
    N(2,0) = n1;
    N(2,1) = n0;
    
    if (stiff)
    {
      fullMatrixOperation::allocateAndMakeZero(DNDn0,3,2);
      DNDn0(0,0) = 1.;
      DNDn0(2,1) = 1.;
      fullMatrixOperation::allocateAndMakeZero(DNDn1,3,2);
      DNDn1(1,1) = 1.;
      DNDn1(2,0) = 1.;
      fullMatrixOperation::allocateAndMakeZero(DNDn2,3,2);
    }
  }
  else
  {
    fullMatrixOperation::allocateAndMakeZero(N,6,3);
    N(0,0) = n0;
    N(1,1) = n1;
    N(2,2) = n2;
    N(3,0) = n1;
    N(3,1) = n0;
    N(4,0) = n2;
    N(4,2) = n0;
    N(5,1) = n2;
    N(5,2) = n1;
    
    if (stiff)
    {
      fullMatrixOperation::allocateAndMakeZero(DNDn0,6,3);
      DNDn0(0,0) = 1.;
      DNDn0(3,1) = 1.;
      DNDn0(4,2) = 1;
      
      fullMatrixOperation::allocateAndMakeZero(DNDn1,6,3);
      DNDn1(1,1) = 1.;
      DNDn1(3,0) = 1.;
      DNDn1(5,2) = 1;
      
      fullMatrixOperation::allocateAndMakeZero(DNDn2,6,3);
      DNDn2(2,2) = 1.;
      DNDn2(4,0) = 1.;
      DNDn2(5,1) = 1;
    }
  }
};

void fullMatrixOperation::convertMat66ToMat99(const fullMatrix<double>& mat6x6, fullMatrix<double>& mat9x9)
{
  if (mat6x6.size1() != 6 || mat6x6.size2() != 6)
  {
    Msg::Error("9x9 matrix must be used to reduce to 6x6 matrix");
  }
  allocateAndMakeZero(mat9x9,9,9);
  auto getIndex = [](int i, int j)
  {
    if (i==j) return i;
    else if ((i==0 && j==1) || (i==1 && j==0)) return 3;
    else if ((i==0 && j==2) || (i==2 && j==0)) return 4;
    else if ((i==1 && j==2) || (i==2 && j==1)) return 5;
    {
      Msg::Error("index %d %d does not exst",i,j);
      return -1;
    }
  };
  for (int i=0; i< 3; i++)
  {
    for (int j=0; j<3; j++)
    {
      for (int k=0; k<3; k++)
      {
        for (int l=0; l<3; l++)
        {
          mat9x9(Tensor23::getIndex(i,j),Tensor23::getIndex(k,l)) = mat6x6(getIndex(i,j),getIndex(k,l));
        }
      }
    }
  }
};

void fullMatrixOperation::reducePlaneStrain(const fullMatrix<double>& mat9x9, fullMatrix<double>& mat3x3)
{
  if (mat9x9.size1() != 9 || mat9x9.size2() != 9)
  {
    Msg::Error("9x9 matrix must be used to reduce to 3x3 matrix");
  }
  allocateAndMakeZero(mat3x3,3,3);
  auto getIndex = [](int k, int& i, int& j)
  {
    if (k == 0) {i=0; j=0;}
    else if (k == 1) {i=1; j=1;}
    else if (k == 2) {i=0; j=1;}
    else
    {
      Msg::Error("index %d cannot larger than 3");
    }
  };
  for (int row = 0; row <3; row++)
  {
    int i,j;
    getIndex(row,i,j);
    int fullRow = Tensor23::getIndex(i,j);
    for (int col = 0; col <3; col++)
    {
      int k,l;
      getIndex(col,k,l);
      int fullCol= Tensor23::getIndex(k,l);
      mat3x3(row,col) = mat9x9(fullRow,fullCol);
    }
  };
};

void fullMatrixOperation::matToVec(const fullMatrix<double>& mat, fullVector<double>& v)
{
  v.resize(mat.size1()*mat.size2());
  int row=0;
  for (int i=0; i< mat.size1(); i++)
  {
    for (int j=0; j< mat.size2(); j++)
    {
      v(row) = mat(i,j);
      row++;
    }
  }
};

void fullMatrixOperation::vecToMat(const fullVector<double>& vec, int nR, int nC, fullMatrix<double>& mat)
{
  allocateAndMakeZero(mat,nR,nC);
  int row=0;
  for (int i=0; i< mat.size1(); i++)
  {
    for (int j=0; j< mat.size2(); j++)
    {
      mat(i,j) = vec(row);
      row++;
    }
  }
};

void fullMatrixOperation::convertSTensor43ToFullMatrix(const STensor43& ten, fullMatrix<double>& fullMat)
{
  allocateAndMakeZero(fullMat,9,9);
  for (int row = 0; row <9; row++)
  {
    int i,j;
    Tensor23::getIntsFromIndex(row,i,j);
    for (int col = 0; col <9; col++)
    {
      int k,l;
      Tensor23::getIntsFromIndex(col,k,l);
      fullMat(row,col) = ten(i,j,k,l);
    }
  };
};

void fullMatrixOperation::convertFullMatrixToSTensor43(const fullMatrix<double>& fullMat, STensor43& ten)
{
  if (fullMat.size1() != 9 || fullMat.size2() != 9)
  {
    Msg::Error("9x9 matrix must be used");
    Msg::Exit(0);
  }
  for (int row = 0; row <9; row++)
  {
    int i,j;
    Tensor23::getIntsFromIndex(row,i,j);
    for (int col = 0; col <9; col++)
    {
      int k,l;
      Tensor23::getIntsFromIndex(col,k,l);
      ten(i,j,k,l) = fullMat(row,col);
    }
  };
}

void fullMatrixOperation::isotropicElasticTensor(double E, double nu, STensor43& K)
{
  double lambla = E*nu/(1.+nu)/(1.-2.*nu);
  double mu = E/(2.*(1+nu));
  
  STensor43 I4(1.,1.);
  K = I4;
  K *= (2.*mu);
  STensor3 I(1.);
  STensorOperation::prodAdd(I,I,lambla,K);
};


void fullMatrixOperation::isotropicElasticTensor(double E, double nu, fullMatrix<double>& fullMat)
{
  double lambla = E*nu/(1.+nu)/(1.-2.*nu);
  double mu = E/(2.*(1+nu));
  
  STensor43 K(1.,1.);
  K *= (2.*mu);
  STensor3 I(1.);
  STensorOperation::prodAdd(I,I,lambla,K);
  
  allocateAndMakeZero(fullMat,9,9);
  for (int row = 0; row <9; row++)
  {
    int i,j;
    Tensor23::getIntsFromIndex(row,i,j);
    for (int col = 0; col <9; col++)
    {
      int k,l;
      Tensor23::getIntsFromIndex(col,k,l);
      fullMat(row,col) = K(i,j,k,l);
    }
  };
};

void fullMatrixOperation::anisotropicElasticTensor(double Ex, double Ey, double Ez,
                                  double Vxy, double Vxz, double Vyz,
                                  double MUxy, double MUxz, double MUyz,
                                  double alpha, double beta, double gamma,
                                  fullMatrix<double>& fullMat)
{
  static STensor43 K;
  smallStrainLinearElasticPotential::createElasticTensor(Ex,Ey, Ez,Vxy,Vxz,Vyz,MUxy,MUxz,MUyz,alpha,beta,gamma,K);
  allocateAndMakeZero(fullMat,9,9);
  for (int row = 0; row <9; row++)
  {
    int i,j;
    Tensor23::getIntsFromIndex(row,i,j);
    for (int col = 0; col <9; col++)
    {
      int k,l;
      Tensor23::getIntsFromIndex(col,k,l);
      fullMat(row,col) = K(i,j,k,l);
    }
  };
};

void fullMatrixOperation::planeStrainElasticTensor(double C00, double C11, double G12, double C12, fullMatrix<double>& fullMat)
{
  allocateAndMakeZero(fullMat,9,9);
  int index00 = Tensor23::getIndex(0,0);
  int index01 = Tensor23::getIndex(0,1);
  int index10 = Tensor23::getIndex(1,0);
  int index11 = Tensor23::getIndex(1,1);
  int index22 = Tensor23::getIndex(2,2);
    
  fullMat(index00,index00) = C00;
  fullMat(index11,index11) = C11;
  fullMat(index01,index01) = G12;
  fullMat(index01,index10) = G12;
  fullMat(index10,index01) = G12;
  fullMat(index10,index10) = G12;
  fullMat(index00,index11) = C12;
  fullMat(index11,index00) = C12;
};    

void fullMatrixOperation::convertToSTensor43Compliance66(const STensor43& S, fullMatrix<double>& Smat)
{
  fullMatrixOperation:allocateAndMakeZero(Smat,6,6);
  Smat(0,0) = S(0,0,0,0);
  Smat(0,1) = S(0,0,1,1);
  Smat(0,2) = S(0,0,2,2);
  Smat(0,3) = 2*S(0,0,0,1);
  Smat(0,4) = 2*S(0,0,0,2);
  Smat(0,5) = 2*S(0,0,1,2);
  
  Smat(1,0) = S(1,1,0,0);
  Smat(1,1) = S(1,1,1,1);
  Smat(1,2) = S(1,1,2,2);
  Smat(1,3) = 2*S(1,1,0,1);
  Smat(1,4) = 2*S(1,1,0,2);
  Smat(1,5) = 2*S(1,1,1,2);
  
  Smat(2,0) = S(2,2,0,0);
  Smat(2,1) = S(2,2,1,1);
  Smat(2,2) = S(2,2,2,2);
  Smat(2,3) = 2*S(2,2,0,1);
  Smat(2,4) = 2*S(2,2,0,2);
  Smat(2,5) = 2*S(2,2,1,2);
  
  Smat(3,0) = 2*S(0,1,0,0);
  Smat(3,1) = 2*S(0,1,1,1);
  Smat(3,2) = 2*S(0,1,2,2);
  Smat(3,3) = 4*S(0,1,0,1);
  Smat(3,4) = 4*S(0,1,0,2);
  Smat(3,5) = 4*S(0,1,1,2);
  
  Smat(4,0) = 2*S(0,2,0,0);
  Smat(4,1) = 2*S(0,2,1,1);
  Smat(4,2) = 2*S(0,2,2,2);
  Smat(4,3) = 4*S(0,2,0,1);
  Smat(4,4) = 4*S(0,2,0,2);
  Smat(4,5) = 4*S(0,2,1,2);
  
  Smat(5,0) = 2*S(1,2,0,0);
  Smat(5,1) = 2*S(1,2,1,1);
  Smat(5,2) = 2*S(1,2,2,2);
  Smat(5,3) = 4*S(1,2,0,1);
  Smat(5,4) = 4*S(1,2,0,2);
  Smat(5,5) = 4*S(1,2,1,2);
}
void fullMatrixOperation::convertCompliance66ToSTensor43(const fullMatrix<double>& Smat, STensor43& S)
{
  S(0,0,0,0) = Smat(0,0);
  S(0,0,1,1) = Smat(0,1);
  S(0,0,2,2) = Smat(0,2);
  S(0,0,0,1) = S(0,0,1,0) = Smat(0,3)/2.;
  S(0,0,0,2) = S(0,0,2,0) = Smat(0,4)/2.;
  S(0,0,1,2) = S(0,0,2,1) = Smat(0,5)/2.;
  
  S(1,1,0,0) = Smat(1,0);
  S(1,1,1,1) = Smat(1,1);
  S(1,1,2,2) = Smat(1,2);
  S(1,1,0,1) = S(1,1,1,0) = Smat(1,3)/2.;
  S(1,1,0,2) = S(1,1,2,0) = Smat(1,4)/2.;
  S(1,1,1,2) = S(1,1,2,1) = Smat(1,5)/2.;
  
  S(2,2,0,0) = Smat(2,0);
  S(2,2,1,1) = Smat(2,1);
  S(2,2,2,2) = Smat(2,2);
  S(2,2,0,1) = S(2,2,1,0) = Smat(2,3)/2.;
  S(2,2,0,2) = S(2,2,2,0) = Smat(2,4)/2.;
  S(2,2,1,2) = S(2,2,2,1) = Smat(2,5)/2.;
  
  S(0,1,0,0) = Smat(3,0)/2.;
  S(0,1,1,1) = Smat(3,1)/2.;
  S(0,1,2,2) = Smat(3,2)/2.;
  S(0,1,0,1) = S(0,1,1,0) = Smat(3,3)/4.;
  S(0,1,0,2) = S(0,1,2,0) = Smat(3,4)/4.;
  S(0,1,1,2) = S(0,1,2,1) = Smat(3,5)/4.;
  
  S(1,0,0,0) = Smat(3,0)/2.;
  S(1,0,1,1) = Smat(3,1)/2.;
  S(1,0,2,2) = Smat(3,2)/2.;
  S(1,0,0,1) = S(1,0,1,0) = Smat(3,3)/4.;
  S(1,0,0,2) = S(1,0,2,0) = Smat(3,4)/4.;
  S(1,0,1,2) = S(1,0,2,1) = Smat(3,5)/4.;
  
  S(0,2,0,0) = Smat(4,0)/2.;
  S(0,2,1,1) = Smat(4,1)/2.;
  S(0,2,2,2) = Smat(4,2)/2.;
  S(0,2,0,1) = S(0,2,1,0) = Smat(4,3)/4.;
  S(0,2,0,2) = S(0,2,2,0) = Smat(4,4)/4.;
  S(0,2,1,2) = S(0,2,2,1) = Smat(4,5)/4.;
  
  S(2,0,0,0) = Smat(4,0)/2.;
  S(2,0,1,1) = Smat(4,1)/2.;
  S(2,0,2,2) = Smat(4,2)/2.;
  S(2,0,0,1) = S(2,0,1,0) = Smat(4,3)/4.;
  S(2,0,0,2) = S(2,0,2,0) = Smat(4,4)/4.;
  S(2,0,1,2) = S(2,0,2,1) = Smat(4,5)/4.;
  
  S(1,2,0,0) = Smat(5,0)/2.;
  S(1,2,1,1) = Smat(5,1)/2.;
  S(1,2,2,2) = Smat(5,2)/2.;
  S(1,2,0,1) = S(1,2,1,0) = Smat(5,3)/4.;
  S(1,2,0,2) = S(1,2,2,0) = Smat(5,4)/4.;
  S(1,2,1,2) = S(1,2,2,1) = Smat(5,5)/4.;
  
  S(2,1,0,0) = Smat(5,0)/2.;
  S(2,1,1,1) = Smat(5,1)/2.;
  S(2,1,2,2) = Smat(5,2)/2.;
  S(2,1,0,1) = S(2,1,1,0) = Smat(5,3)/4.;
  S(2,1,0,2) = S(2,1,2,0) = Smat(5,4)/4.;
  S(2,1,1,2) = S(2,1,2,1) = Smat(5,5)/4.;
};

void fullMatrixOperation::homogenizedTangentBimaterial(const fullMatrix<double>& C0, const fullMatrix<double>& C1, double f,
                                      double n0, double n1, double n2, fullMatrix<double>& Chomo)
{
  double f0 = f;
  double f1 = 1-f;
  static SVector3 nor;
  nor(0) = n0;
  nor(1) = n1;
  nor(2) = n2;
  nor.normalize();
  
  fullMatrix<double> C0_9x9, C1_9x9;
  fullMatrixOperation::convertMat66ToMat99(C0,C0_9x9);
  fullMatrixOperation::convertMat66ToMat99(C1,C1_9x9);
  
  static STensor43 C0ten, C1ten, f0C1f1C0;
  fullMatrixOperation::convertFullMatrixToSTensor43(C0_9x9,C0ten);
  fullMatrixOperation::convertFullMatrixToSTensor43(C1_9x9,C1ten);
  
  STensorOperation::zero(f0C1f1C0);
  STensorOperation::axpy(f0C1f1C0,C0ten,f1);
  STensorOperation::axpy(f0C1f1C0,C1ten,f0);
  
  static STensor3 Q, invQ;
  
  STensorOperation::zero(Q);
  for (int i=0; i<3; i++)
  {
    for (int j=0; j<3; j++)
    {
      for (int k=0; k<3; k++)
      {
        for (int l=0; l<3; l++)
        {
          Q(i,k) += nor(j)*f0C1f1C0(i,j,k,l)*nor(l);
        }
      }
    }
  }
  STensorOperation::inverseSTensor3(Q,invQ);
  static STensor43 DC;
  DC = C0ten;
  DC -= C1ten;
  static STensor33 DCn, nDC;
  STensorOperation::zero(DCn);
  STensorOperation::zero(nDC);
  for (int i=0; i<3; i++)
  {
    for (int j=0; j<3; j++)
    {
      for (int k=0; k<3; k++)
      {
        for (int l=0; l<3; l++)
        {
          DCn(i,j,k) += DC(i,j,k,l)*nor(l);
          nDC(i,j,k) += DC(i,l,j,k)*nor(l);
        }
      }
    }
  }
  
  static STensor43 ChomoTen;
  STensorOperation::zero(ChomoTen);
  STensorOperation::axpy(ChomoTen,C0ten,f0);
  STensorOperation::axpy(ChomoTen,C1ten,f1);
  for (int i=0; i<3; i++)
  {
    for (int j=0; j<3; j++)
    {
      for (int k=0; k<3; k++)
      {
        for (int l=0; l<3; l++)
        {
          for (int p=0; p<3; p++)
          {
            for (int q=0; q<3; q++)
            {
              ChomoTen(i,j,k,l) -= f0*f1*DCn(i,j,p)*invQ(p,q)*nDC(q,k,l);
            }
          }
        }
      }
    }
  }
  fullMatrix<double> Chomo_9x9;
  fullMatrixOperation::convertSTensor43ToFullMatrix(ChomoTen,Chomo_9x9);
  fullMatrixOperation::reduceMat99ToMat66(Chomo_9x9,Chomo);
};

void fullMatrixOperation::homogenizedComplianceBimaterial(const fullMatrix<double>& C0, const fullMatrix<double>& C1, double f, 
                                   double n0, double n1, double n2, double t0, double t1, double t2, fullMatrix<double>& Chomo)
{
  double f0 = f;
  double f1 = 1.-f;
  static SVector3 n, b, t;
  n(0) = n0;
  n(1) = n1;
  n(2) = n2;
  n.normalize();
  t(0) = t0;
  t(1) = t1;
  t(2) = t2;
  t.normalize();
  
  double tnDot =dot(n,t); 
  if (tnDot> 0.)
  {
    t(0) -= tnDot*n(0);
    t(1) -= tnDot*n(1);
    t(2) -= tnDot*n(2);
    t.normalize();
  };
  
  b = crossprod(n,t);
  b.normalize();
  n.print("vector n");
  t.print("vector t");
  b.print("vector b");
    
  
  fullMatrix<double> S0_6x6, S1_6x6;
  fullMatrixOperation::allocateAndMakeZero(S0_6x6,6,6);
  fullMatrixOperation::allocateAndMakeZero(S1_6x6,6,6);
  C0.invert(S0_6x6);
  C1.invert(S1_6x6);
  static STensor43 S0ten, S1ten, f0S1f1S0, DS;
  fullMatrixOperation::convertCompliance66ToSTensor43(S0_6x6,S0ten);
  fullMatrixOperation::convertCompliance66ToSTensor43(S1_6x6,S1ten);

  //S0ten.print("S0ten");
  //S1ten.print("S1ten");
  STensorOperation::zero(f0S1f1S0);
  STensorOperation::axpy(f0S1f1S0,S0ten,f1);
  STensorOperation::axpy(f0S1f1S0,S1ten,f0);
  
  //f0S1f1S0.print("f0S1f1S0");
  
  DS = S0ten;
  DS -= S1ten;
  
  auto multVecVecTen= [](const SVector3& a, const SVector3& b, const STensor43& T, STensor3& abT)
  {
    STensorOperation::zero(abT);
    for (int i=0; i<3; i++)
    {
      for (int j=0; j<3; j++)
      {
        for (int k=0; k<3; k++)
        {
          for (int l=0; l<3; l++)
          {
            abT(k,l) += a(i)*b(j)*T(i,j,k,l);
          }
        }
      }
    }
  };
  
  auto multVecVecTenVecVec = [](const SVector3& a, const SVector3& b, const STensor43& T, const SVector3& c, const SVector3& d)
  {
    double val = 0;
    for (int i=0; i<3; i++)
    {
      for (int j=0; j<3; j++)
      {
        for (int k=0; k<3; k++)
        {
          for (int l=0; l<3; l++)
          {
            val += a(i)*b(j)*T(i,j,k,l)*c(k)*d(l);
          }
        }
      }
    }
    return val;
  };
  
  STensor3 Amat;
  Amat(0,0) = multVecVecTenVecVec(t,t,f0S1f1S0,t,t);
  Amat(0,1) = multVecVecTenVecVec(t,t,f0S1f1S0,t,b);
  Amat(0,2) = multVecVecTenVecVec(t,t,f0S1f1S0,b,b);
  
  Amat(1,0) = multVecVecTenVecVec(t,b,f0S1f1S0,t,t);
  Amat(1,1) = multVecVecTenVecVec(t,b,f0S1f1S0,t,b);
  Amat(1,2) = multVecVecTenVecVec(t,b,f0S1f1S0,b,b);
  
  Amat(2,0) = multVecVecTenVecVec(b,b,f0S1f1S0,t,t);
  Amat(2,1) = multVecVecTenVecVec(b,b,f0S1f1S0,t,b);
  Amat(2,2) = multVecVecTenVecVec(b,b,f0S1f1S0,b,b);

  //Amat.print("Amat");
  STensor3 invAmat;
  STensorOperation::inverseSTensor3(Amat,invAmat);
  
  
  static STensor3 R0, R1, R2;
  multVecVecTen(t,t,DS,R0);
  multVecVecTen(t,b,DS,R1);
  multVecVecTen(b,b,DS,R2);
    
  
  static STensor43 S;
  STensorOperation::zero(S);
  STensorOperation::axpy(S,S0ten,f0);
  STensorOperation::axpy(S,S1ten,f1);
  
  STensorOperation::prodAdd(R0,R0,-invAmat(0,0)*f0*f1,S);
  STensorOperation::prodAdd(R0,R1,-invAmat(0,1)*f0*f1,S);
  STensorOperation::prodAdd(R0,R2,-invAmat(0,2)*f0*f1,S);
  
  STensorOperation::prodAdd(R1,R0,-invAmat(1,0)*f0*f1,S);
  STensorOperation::prodAdd(R1,R1,-invAmat(1,1)*f0*f1,S);
  STensorOperation::prodAdd(R1,R2,-invAmat(1,2)*f0*f1,S);
  
  STensorOperation::prodAdd(R2,R0,-invAmat(2,0)*f0*f1,S);
  STensorOperation::prodAdd(R2,R1,-invAmat(2,1)*f0*f1,S);
  STensorOperation::prodAdd(R2,R2,-invAmat(2,2)*f0*f1,S);
  
  convertToSTensor43Compliance66(S,Chomo);
  Chomo.invertInPlace();
};


void BimaterialHomogenization::compute(const fullMatrix<double>& C1, const fullMatrix<double>& C2, double f1, double normedtheta, fullMatrix<double>& Ceff,
          bool stiff, hyperFullMatrix& DCeffDC1, hyperFullMatrix& DCeffDC2,
          fullMatrix<double>& DCeffDf1, fullMatrix<double>& DCeffDf2  ) const
{
  if (_planeStrain && C1.size1() != 3)
  {
    Msg::Error("tangent must be in 3x3 matrix");
    Msg::Exit(0);
  }
  static std::vector<fullMatrix<double> > Cin(2);
  static std::vector<double> fin(2);
  static std::vector<hyperFullMatrix> DCeffDCin;
  static std::vector<fullMatrix<double> > DCeffDfin;
  static std::vector<fullMatrix<double> > DCeffDnormal;
  static SVector3 normal;
  //
  double pi = 3.14159265359;
  double theta = 2.*pi*normedtheta;
  normal[0] = cos(theta);
  normal[1] = sin(theta);
  normal[2] = 0.;
  
  Cin[0] = C1;
  Cin[1] = C2;
  fin[0] = f1;
  fin[1] = 1-f1;
  compute(Cin,fin,normal,Ceff,stiff,DCeffDCin,DCeffDfin,DCeffDnormal);
  if (stiff)
  {
    DCeffDC1 = DCeffDCin[0];
    DCeffDC2 = DCeffDCin[1];
    
    DCeffDf1 = DCeffDfin[0];
    DCeffDf2 = DCeffDfin[1];
    
    DCeffDnormal[0].print("DCeffDnormal-0");
    DCeffDnormal[1].print("DCeffDnormal-1");
    DCeffDnormal[2].print("DCeffDnormal-2");
  };
};

    
void BimaterialHomogenization::printInfos() const
{
  printf("using Voigt, Reuss with bimaterial laminate \n");
};

void BimaterialHomogenization::computeBimaterial(const fullMatrix<double>& C1, const fullMatrix<double>& C2, double f1, double f2, const SVector3& normal,
                        fullMatrix<double>& Ceff, 
                        bool stiff, 
                        hyperFullMatrix* DCeffDC1, hyperFullMatrix* DCeffDC2,
                        fullMatrix<double>* DCeffDf1, fullMatrix<double>* DCeffDf2, 
                        fullMatrix<double>* DCeffDnormal1,fullMatrix<double>* DCeffDnormal2, fullMatrix<double>* DCeffDnormal3) const
{
  double tol = 1e-6;
  bool voidPhase1 = false;
  bool voidPhase2 = false;
  if (C1.norm() < tol*C2.norm())
  {
    voidPhase1 =true;
  }
  else if (C2.norm() < tol*C1.norm())
  {
    voidPhase2 = true;
  }
 
  // for normal
  static fullMatrix<double> N, DNDn0, DNDn1, DNDn2;
  fullMatrixOperation::getMatrixN(_planeStrain, normal[0],normal[1],normal[2],N,stiff,DNDn0, DNDn1, DNDn2);
  //
  static fullMatrix<double> NT;
  fullMatrixOperation::transpose(N,NT);
  //
  static fullMatrix<double> f2C1f1C2;
  fullMatrixOperation::allocateAndMakeZero(f2C1f1C2,C1.size1(),C1.size2());
  f2C1f1C2.axpy(C1,f2);
  f2C1f1C2.axpy(C2,f1);
  //
  static fullMatrix<double> A;
  fullMatrixOperation::mult(NT,f2C1f1C2,N,A);
  //
  static fullMatrix<double> invA;
  static hyperFullMatrix DinvADA;
  bool ok = fullMatrixOperation::invertMatrix(A,invA,stiff,DinvADA);
  if (!ok)
  {
    A.print("A");
  }
  //

  fullMatrixOperation::allocateAndMakeZero(Ceff,C1.size1(),C1.size2());
  Ceff.axpy(C1,f1);
  Ceff.axpy(C2,f2);
  //
  static fullMatrix<double> B;
  fullMatrixOperation::mult(N,invA,NT,B);
  //
  static fullMatrix<double> DC;
  fullMatrixOperation::allocateAndMakeZero(DC,C1.size1(),C1.size2());
  DC.axpy(C1,1.);
  DC.axpy(C2,-1.);
  // homogenized stiffness
  fullMatrixOperation::multAdd(DC,B,DC,Ceff,-f1*f2);
  //
  if (stiff)
  {
    // compute DinvADC1, DinvADC2
    hyperFullMatrix  I6(C1.size1(),C1.size1(),1); // as DC_ij/DC_kl -idenitity matrix of matrix
    static hyperFullMatrix DADC1, DADC2, DinvADC1, DinvADC2;
    //
    DADC1.allocate(A.size1(),C1.size1(),0.);
    DADC2.allocate(A.size1(),C2.size1(),0.);
    for (int k=0; k< C1.size1(); k++)
    {
      for (int l=0; l< C1.size2(); l++)
      {
        static fullMatrix<double> temp;
        fullMatrixOperation::mult(NT,I6.getMatrix(k,l),N,temp);
        if (!voidPhase1)
          DADC1.getMatrix(k,l).axpy(temp,f2);
          
        if (!voidPhase2)
          DADC2.getMatrix(k,l).axpy(temp,f1);
      }
    }
    
    fullMatrixOperation::mult(DinvADA,DADC1,DinvADC1,1.);
    fullMatrixOperation::mult(DinvADA,DADC2,DinvADC2,1.);
    
    // compute DinvADf1, DinvADf2
    static fullMatrix<double> DADf1, DADf2, DinvADf1, DinvADf2;
    fullMatrixOperation::mult(NT,C2,N,DADf1);
    fullMatrixOperation::mult(NT,C1,N,DADf2);
    fullMatrixOperation::mult(DinvADA,DADf1,DinvADf1);
    fullMatrixOperation::mult(DinvADA,DADf2,DinvADf2);
    
    // compute DinvADn0, DinvADn1, DinvADn2
    static fullMatrix<double> DinvADn0, DinvADn1, DinvADn2;
    //
    static fullMatrix<double> DNTDn0, DNTDn1, DNTDn2;
    fullMatrixOperation::transpose(DNDn0,DNTDn0);
    fullMatrixOperation::transpose(DNDn1,DNTDn1);
    fullMatrixOperation::transpose(DNDn2,DNTDn2);
    //
    static fullMatrix<double> DADn0, DADn1, DADn2;
    fullMatrixOperation::mult(DNTDn0,f2C1f1C2,N,DADn0);
    fullMatrixOperation::multAdd(NT,f2C1f1C2,DNDn0,DADn0,1.);
    fullMatrixOperation::mult(DNTDn1,f2C1f1C2,N,DADn1);
    fullMatrixOperation::multAdd(NT,f2C1f1C2,DNDn1,DADn1,1.);
    fullMatrixOperation::mult(DNTDn2,f2C1f1C2,N,DADn2);
    fullMatrixOperation::multAdd(NT,f2C1f1C2,DNDn2,DADn2,1.);
    
    fullMatrixOperation::mult(DinvADA,DADn0,DinvADn0);
    fullMatrixOperation::mult(DinvADA,DADn1,DinvADn1);
    fullMatrixOperation::mult(DinvADA,DADn2,DinvADn2);
    
    // for tensor B
    // compute DBDC1, DBDC2
    static hyperFullMatrix DBDC1, DBDC2;
    DBDC1.allocate(B.size1(),C1.size1(),0.);
    DBDC2.allocate(B.size1(),C2.size1(),0.);
    for (int k=0; k< C1.size1(); k++)
    {
      for (int l=0; l< C1.size2(); l++)
      {
        fullMatrixOperation::mult(N,DinvADC1.getMatrix(k,l),NT,DBDC1.getMatrix(k,l));
        fullMatrixOperation::mult(N,DinvADC2.getMatrix(k,l),NT,DBDC2.getMatrix(k,l));
      }
    }
    // compute DBDf1, DBDf2;
    static fullMatrix<double> DBDf1, DBDf2;
    fullMatrixOperation::mult(N,DinvADf1,NT,DBDf1);
    fullMatrixOperation::mult(N,DinvADf2,NT,DBDf2);
    // compute DBDn0, DBDn1, DBDn2
    static fullMatrix<double> DBDn0, DBDn1, DBDn2;
    fullMatrixOperation::mult(N,DinvADn0,NT,DBDn0);
    fullMatrixOperation::multAdd(DNDn0,invA,NT,DBDn0,1);
    fullMatrixOperation::multAdd(N,invA,DNTDn0,DBDn0,1);
    fullMatrixOperation::mult(N,DinvADn1,NT,DBDn1);
    fullMatrixOperation::multAdd(DNDn1,invA,NT,DBDn1,1);
    fullMatrixOperation::multAdd(N,invA,DNTDn1,DBDn1,1);
    fullMatrixOperation::mult(N,DinvADn2,NT,DBDn2);
    fullMatrixOperation::multAdd(DNDn2,invA,NT,DBDn2,1);
    fullMatrixOperation::multAdd(N,invA,DNTDn2,DBDn2,1);
    //
    
    DCeffDC1->allocate(Ceff.size1(),C1.size1(),0.);
    DCeffDC2->allocate(Ceff.size1(),C2.size1(),0.);
    
    if (!voidPhase1)
      DCeffDC1->axpy(I6,f1);
    if (!voidPhase2)
      DCeffDC2->axpy(I6,f2);
    
    for (int k=0; k< C1.size1(); k++)
    {
      for (int l=0; l< C1.size2(); l++)
      {
        if (!voidPhase1)
        {
          fullMatrixOperation::multAdd(I6.getMatrix(k,l),B,DC,DCeffDC1->getMatrix(k,l),-f1*f2);
          fullMatrixOperation::multAdd(DC,B,I6.getMatrix(k,l),DCeffDC1->getMatrix(k,l),-f1*f2);
          fullMatrixOperation::multAdd(DC,DBDC1.getMatrix(k,l),DC,DCeffDC1->getMatrix(k,l),-f1*f2);
          
        }
        //
        if (!voidPhase2)
        {
          fullMatrixOperation::multAdd(I6.getMatrix(k,l),B,DC,DCeffDC2->getMatrix(k,l),f1*f2);
          fullMatrixOperation::multAdd(DC,B,I6.getMatrix(k,l),DCeffDC2->getMatrix(k,l),f1*f2);
          fullMatrixOperation::multAdd(DC,DBDC2.getMatrix(k,l),DC,DCeffDC2->getMatrix(k,l),-f1*f2);
        }
      }
    }
    //
    (*DCeffDf1) = C1;
    fullMatrixOperation::multAdd(DC,B,DC,*DCeffDf1,-f2);
    fullMatrixOperation::multAdd(DC,DBDf1,DC,*DCeffDf1,-f1*f2);
    
    (*DCeffDf2) = C2;
    fullMatrixOperation::multAdd(DC,B,DC,*DCeffDf2,-f1);
    fullMatrixOperation::multAdd(DC,DBDf2,DC,*DCeffDf2,-f1*f2);
    //
    fullMatrixOperation::allocateAndMakeZero(*DCeffDnormal1,Ceff.size1(),Ceff.size2());
    fullMatrixOperation::multAdd(DC,DBDn0,DC,*DCeffDnormal1,-f1*f2);
    
    fullMatrixOperation::allocateAndMakeZero(*DCeffDnormal2,Ceff.size1(),Ceff.size2());
    fullMatrixOperation::multAdd(DC,DBDn1,DC,*DCeffDnormal2,-f1*f2);
    
    fullMatrixOperation::allocateAndMakeZero(*DCeffDnormal3,Ceff.size1(),Ceff.size2());
    fullMatrixOperation::multAdd(DC,DBDn2,DC,*DCeffDnormal3,-f1*f2);
    
  };
};

BlockMatrix::BlockMatrix(int numRow, int numCol, int rowElem, int colElem):
  _numCol(numCol),_numRow(numRow), _numRowElem(rowElem), _numColElem(colElem), _data(numRow*rowElem,numCol*colElem,true),_isModified(true),_lsys(NULL){}
BlockMatrix::BlockMatrix(const BlockMatrix& src): _numCol(src._numCol),_numRow(src._numRow), 
  _numRowElem(src._numRowElem), _numColElem(src._numColElem),_data(src._data),_isModified(src._isModified), _lsys(NULL){}
  
BlockMatrix::~BlockMatrix(){if (_lsys) delete _lsys;}

void BlockMatrix::setElem(int row, int col, const fullMatrix<double>& mat, double fact)
{
  _isModified = true;
  if (row < 0 || row > _numRow-1 || col < 0 || col > _numCol-1)
  {
    Msg::Error("row %d col %d exceeds numRows %d, numCols %d", row,col,_numRow,_numCol);
    Msg::Exit(0);
  }
  //
  // index for mat in fullMatrix
  if (mat.size1() != _numRowElem || mat.size2() != _numColElem)
  {
    Msg::Error("the matrix is not compatible in BlockMatrix::setElem");
    Msg::Exit(0);
  }
  
  int rowPos = row*_numRowElem;
  int colPos = col*_numColElem;
  for (int i=0; i< mat.size1(); i++)
  {
    for  (int j=0; j< mat.size2(); j++)
    {
      _data(rowPos+i,colPos+j) = fact*mat(i,j);
    }
  };
};
void BlockMatrix::setElemAdd(int row, int col, const fullMatrix<double>& mat, double fact)
{
  _isModified = true;
  if (row < 0 || row > _numRow-1 || col < 0 || col > _numCol-1)
  {
    Msg::Error("row %d col %d exceeds numRows %d, numCols %d", row,col,_numRow,_numCol);
    Msg::Exit(0);
  }
  //
  // index for mat in fullMatrix
  if (mat.size1() != _numRowElem || mat.size2() != _numColElem)
  {
    Msg::Error("the matrix is not compatible in BlockMatrix::setElemAdd");
    mat.print("mat");
    _data.print("data");
    Msg::Exit(0);
  }
  
  int rowPos = row*_numRowElem;
  int colPos = col*_numColElem;
  for (int i=0; i< mat.size1(); i++)
  {
    for  (int j=0; j< mat.size2(); j++)
    {
      _data(rowPos+i,colPos+j) += fact*mat(i,j);
    }
  };
};
void BlockMatrix::getElem(int row, int col, fullMatrix<double>& mat) const
{
  if (row < 0 || row > _numRow-1 || col < 0 || col > _numCol-1)
  {
    Msg::Error("row %d col %d exceeds numRows %d, numCols %d", row,col,_numRow,_numCol);
    Msg::Exit(0);
  }
  fullMatrixOperation::allocateAndMakeZero(mat,_numRowElem,_numColElem);
  int rowPos = row*_numRowElem;
  int colPos = col*_numColElem;
  for (int i=0; i< mat.size1(); i++)
  {
    for  (int j=0; j< mat.size2(); j++)
    {
      mat(i,j) = _data(rowPos+i,colPos+j);
    }
  };
};

void BlockMatrix::setAll(double v)
{
  _data.setAll(v);
}

void BlockMatrix::transpose(BlockMatrix& vT) const
{
  vT._numRow = _numCol;
  vT._numCol = _numRow;
  vT._numRowElem = _numColElem;
  vT._numColElem = _numRowElem;
  vT._data = _data.transpose();
  vT._isModified = true;
};

void BlockMatrix::transpose(fullMatrix<double>& vT) const
{
  vT = _data.transpose();
}

void BlockMatrix::luSolve(const fullMatrix<double>& rhs, fullMatrix<double>& result)
{
  
  int numR = _data.size1();
  fullMatrixOperation::allocateAndMakeZero(result, rhs.size1(),rhs.size2());
  if (_isModified || _lsys == NULL)
  {
    if (_lsys) delete _lsys;
    #if defined(HAVE_PETSC)
    _lsys = new linearSystemPETSc<double>();
    _lsys->allocate(numR);
    // preallocated
    for (int i=0; i< numR; i++)
    {
      for (int j=0; j< numR; j++)
      {
        _lsys->insertInSparsityPattern(i,j);
      }
    };
    _lsys->preAllocateEntries();
    #else
    _lsys = new linearSystemGmm<double>();
    dynamic_cast<linearSystemGmm<double>*>(_lsys)->setNoisy(2);
    _lsys->allocate(numR);
    #endif //HAVE_PETSC
  }
  for (int i=0; i< numR; i++)
  {
    for (int j=0; j< numR; j++)
    {
      _lsys->addToMatrix(i,j,_data(i,j));
    }
  };
  
  for (int j=0; j< rhs.size2(); j++)
  {
    _lsys->zeroRightHandSide();
    for (int i=0; i< numR; i++)
    {
      _lsys->addToRightHandSide(i,rhs(i,j));
    };
    _lsys->systemSolve();
    for (int i=0; i< numR; i++)
    {
      _lsys->getFromSolution(i,result(i,j));
    }
  };
  _isModified = false;
  /*

  #if 1
  fullMatrix<double> invA(_data.size1(),_data.size2());
  _data.invert(invA);
  invA.mult(rhs,result);
  #else
  static fullVector<double> rhsCol, res;
  rhsCol.resize(rhs.size1());
  res.resize(rhs.size1());
  for (int j=0; j< rhs.size2(); j++)
  {
    for (int i=0; i< res.size(); i++)
    {
      rhsCol(i) = rhs(i,j);
    };
    fullMatrix<double> newMat;
    newMat.copy(_data);
    //_data.print("row ---");
    newMat.luSolve(rhsCol,res);
    for (int i=0; i< res.size(); i++)
    {
      result(i,j) = res(i);
    }
  };
  #endif//
   */
};


void BlockMatrix::luSolve(const BlockMatrix& rhs, fullMatrix<double>& result)
{
  luSolve(rhs._data,result);
};

void BlockMatrix::PtAP(const BlockMatrix& P, fullMatrix<double>& PtAPres) const
{
  PtAP(P._data,PtAPres);
};
    

void BlockMatrix::PtAP(const fullMatrix<double>& P, fullMatrix<double>& PtAPres) const
{
  fullMatrix<double> PT = P.transpose();
  fullMatrixOperation::mult(PT,_data,P,PtAPres);
};

void HomogenizationWithCohesive::getInterfaceStiffness(double Kn, double Kt, double n0, double n1, double n2, fullMatrix<double>& Q,
              bool stiff, fullMatrix<double>* DQDn0, fullMatrix<double>* DQDn1, fullMatrix<double>* DQDn2) const
{
  int dim = _planeStrain? 2:3;
  Q.resize(dim,dim,true);
  if (stiff)
  {
    DQDn0->resize(dim,dim,true);
    DQDn1->resize(dim,dim,true);
    DQDn2->resize(dim,dim,true);
  };
      
  Q(0,0) = Kt + (Kn-Kt)*n0*n0;
  Q(0,1) = (Kn-Kt)*n0*n1;
  Q(1,0) = (Kn-Kt)*n0*n1;
  Q(1,1) = Kt + (Kn-Kt)*n1*n1;
  
  if (stiff)
  {
    (*DQDn0)(0,0) = 2.*(Kn-Kt)*n0;
    (*DQDn0)(0,1) = (Kn-Kt)*n1;
    (*DQDn0)(1,0) = (Kn-Kt)*n1;
    (*DQDn0)(1,1) = 0.;
    
    (*DQDn1)(0,0) = 0.;
    (*DQDn1)(0,1) = (Kn-Kt)*n0;
    (*DQDn1)(1,0) = (Kn-Kt)*n0;
    (*DQDn1)(1,1) = 2.*(Kn-Kt)*n1;
  }
    
  if (dim == 3)
  {
    Msg::Error("this is not implemeneted");
    Msg::Exit(0);
  }
};
              
void HomogenizationWithCohesive::getNormal(const fullVector<double>& normalVars, double& n0, double& n1, double& n2, 
            bool stiff, fullMatrix<double>* DnormalDVars) const
{
  double pi = 3.14159265359;
  if (normalVars.size() == 1)
  {
    double theta = 2.*pi*normalVars(0);
    n0 = cos(theta);
    n1 = sin(theta);
    n2 = 0.;
    if (stiff)
    {
      if ((*DnormalDVars).size1() != 3 || (*DnormalDVars).size2() != 1)
      {
        (*DnormalDVars).resize(3,1);
      }
      (*DnormalDVars)(0,0) = -sin(theta)*2.*pi;
      (*DnormalDVars)(1,0) = cos(theta)*2.*pi;
      (*DnormalDVars)(2,0) = 0.;
    }
  }
  else if (normalVars.size() == 2)
  {
    // cylindrical coordinates
    double phi = 2.*pi*normalVars(0);
    double theta = pi*normalVars(1);
    n0 = sin(theta)*cos(phi);
    n1 = sin(theta)*sin(phi);
    n2 = cos(theta);
    if (stiff)
    {
      if ((*DnormalDVars).size1() != 3 || (*DnormalDVars).size2() != 2)
      {
        (*DnormalDVars).resize(3,2);
      }
      
      (*DnormalDVars)(0,0) = -sin(theta)*sin(phi)*2*pi;
      (*DnormalDVars)(1,0) = sin(theta)*cos(phi)*2*pi;
      (*DnormalDVars)(2,0) = 0.;
      
      (*DnormalDVars)(0,1) = cos(theta)*cos(phi)*pi;
      (*DnormalDVars)(1,1) = cos(theta)*sin(phi)*pi;
      (*DnormalDVars)(2,1) = -sin(theta)*pi;
    }
  }
  else
  {
    Msg::Error("number of vars per normal must be 1 or 2, a value of %d is impossible",normalVars.size());
    Msg::Exit(0);
  }
}

void HomogenizationWithCohesive::compute(const fullMatrix<double>& Cin, // elastic stiffness
                         const fullVector<double>& K,  // interface stiffness
                         const fullVector<double>& cohesiveCoeffs, // all coeffcients of interface terms contribution
                         const fullMatrix<double>& allNormalVars, // all normal variables, each row contains varialbe for each normal
                         fullMatrix<double>& Ceff, // equivalent stiffness
                         bool stiff,
                         std::vector<fullMatrix<double> >* DCeffDcohesiveCoeffs,
                         std::vector<std::vector<fullMatrix<double> > >* DCeffDnormalVars // each normal, each normal var
                       ) const
{
  int numCoh = cohesiveCoeffs.size();
  int numDirVars = allNormalVars.size2();
  int HnumRow, HnumCol;
  if (_planeStrain)
  {
    HnumRow = 3;
    HnumCol = 2;
  }
  else
  {
    HnumRow = 6;
    HnumCol = 3;
  }
  
  static std::vector<fullMatrix<double> > allH, allHTranspose,allDHDn0,allDHDn1,allDHDn2, allDHTransposeDn0,allDHTransposeDn1,allDHTransposeDn2, DnormalDVars;
  static std::vector<fullMatrix<double> > allQin, allDQinDn0,allDQinDn1,allDQinDn2;
  if (allH.size() < numCoh)
  {
    allH.resize(numCoh);
    allHTranspose.resize(numCoh);
    allDHDn0.resize(numCoh);
    allDHDn1.resize(numCoh);
    allDHDn2.resize(numCoh);
    DnormalDVars.resize(numCoh);
    allDHTransposeDn0.resize(numCoh);
    allDHTransposeDn1.resize(numCoh);
    allDHTransposeDn2.resize(numCoh);
    //
    allQin.resize(numCoh);
    allDQinDn0.resize(numCoh);
    allDQinDn1.resize(numCoh);
    allDQinDn2.resize(numCoh);
  };
  

  for (int k=0; k< numCoh; k++)
  {
    static fullVector<double> normalVars;
    fullMatrixOperation::getRow(allNormalVars,k,normalVars);
    double n0, n1, n2;
    getNormal(normalVars,n0, n1, n2,stiff,&DnormalDVars[k]);
    fullMatrixOperation::getMatrixN(_planeStrain,n0,n1,n2,allH[k],stiff,allDHDn0[k],allDHDn1[k],allDHDn2[k]);
    allHTranspose[k] = allH[k].transpose();
    allDHTransposeDn0[k] = allDHDn0[k].transpose();
    allDHTransposeDn1[k] = allDHDn1[k].transpose();
    allDHTransposeDn2[k] = allDHDn2[k].transpose();
    //
    getInterfaceStiffness(K(0),K(1),n0,n1,n2,allQin[k],stiff,&allDQinDn0[k],&allDQinDn1[k],&allDQinDn2[k]);
  };
  //
  BlockMatrix matE(1,numCoh,HnumRow,HnumCol);
  BlockMatrix matK(numCoh,numCoh,HnumCol,HnumCol);
  std::map<std::pair<int,int>, fullMatrix<double> > allmlk;
  //
  for (int l=0; l< numCoh; l++)
  {
    matE.setElemAdd(0,l,allH[l],cohesiveCoeffs(l));
    matK.setElemAdd(l,l,allQin[l],cohesiveCoeffs(l));
    //allQin[l].print("allQin[l]");
    for (int k=0; k< numCoh; k++)
    {
      fullMatrix<double>& mlk =  allmlk[std::pair<int,int>(l,k)];
      fullMatrixOperation::mult(allHTranspose[l],Cin,allH[k],mlk);
      matK.setElemAdd(l,k,mlk,cohesiveCoeffs(l)*cohesiveCoeffs(k));
    };
  };
  //matK.getData().print("matK");
  static fullMatrix<double> matET, matZ, matRi;
  matE.transpose(matET);
  // solution
  matK.luSolve(matET,matZ);
  /*matK.getData().print("matK ffffffffffffffffff");
  matET.print("matET");
  matZ.print("matZ");*/
  //
  fullMatrixOperation::allocateAndMakeZero(matRi,matE.size1(),matZ.size2());
  matE.getData().mult(matZ,matRi);
  
  Ceff = Cin;
  fullMatrixOperation::multAdd(Cin,matRi,Cin,Ceff,-1.);
  //
  //Ceff.print("Ceff");
  if (stiff)
  {
    if (DCeffDcohesiveCoeffs->size() < numCoh)
    {
      DCeffDcohesiveCoeffs->resize(numCoh,fullMatrix<double>());
    }
     //
    if (DCeffDnormalVars->size() < numCoh)
    {
      DCeffDnormalVars->resize(numCoh);
    }
    for (int k=0; k< numCoh; k++)
    {
      if ((*DCeffDnormalVars)[k].size() < numDirVars)
      {
        (*DCeffDnormalVars)[k].resize(numDirVars,fullMatrix<double>());
      }
    }
    
    bool pert = false;
    if (pert)
    {
      double pertTol = 1e-3;
      for (int k=0; k< numCoh; k++)
      {
        fullMatrix<double>& DCeffDXk = (*DCeffDcohesiveCoeffs)[k];
        fullVector<double> cohesiveCoeffsPlus = cohesiveCoeffs;
        cohesiveCoeffsPlus(k) += pertTol;
        fullMatrix<double> CeffPlus;
        compute(Cin,  K,  cohesiveCoeffsPlus, allNormalVars, CeffPlus);
        DCeffDXk = CeffPlus;
        DCeffDXk.scale(1./pertTol);
        DCeffDXk.axpy(Ceff,-1./pertTol);
      }
      
      for (int k =0; k< numCoh; k++)
      {
        // for normal K
        std::vector<fullMatrix<double> >& DCeffDvarsk =  (*DCeffDnormalVars)[k];
        for (int iv=0; iv < numDirVars; iv++)
        {
          fullMatrix<double>& DCeffDXk = DCeffDvarsk[iv];          
          fullMatrix<double>  allNormalVarsPlus = allNormalVars;
          allNormalVarsPlus(k,iv) += pertTol;
          fullMatrix<double> CeffPlus;
          compute(Cin,  K,  cohesiveCoeffs, allNormalVarsPlus, CeffPlus);
          DCeffDXk = CeffPlus;
          DCeffDXk.scale(1./pertTol);
          DCeffDXk.axpy(Ceff,-1./pertTol);
        }
      }
    }
    else
    {
      //
      BlockMatrix DmatEDXk(1,numCoh,HnumRow,HnumCol);
      BlockMatrix DmatKDXk(numCoh,numCoh,HnumCol,HnumCol);
      
      static fullMatrix<double> DmatRiDX, temp;
      for (int k =0; k< numCoh; k++)
      {
        fullMatrix<double>& DCeffDXk = (*DCeffDcohesiveCoeffs)[k];
        fullMatrixOperation::allocateAndMakeZero(DCeffDXk,Ceff.size1(),Ceff.size2());
        
        DmatEDXk.setAll(0);
        DmatEDXk.setElemAdd(0,k,allH[k],1.);
        //
        DmatKDXk.setAll(0.);
        DmatKDXk.setElemAdd(k,k,allQin[k],1.);
        // row k and column k are non-zero
        for (int l=0; l< numCoh; l++)
        {
          fullMatrix<double>& mlk =  allmlk[std::pair<int,int>(l,k)];
          fullMatrix<double>& mkl =  allmlk[std::pair<int,int>(k,l)];        
          DmatKDXk.setElemAdd(l,k,mlk,cohesiveCoeffs(l));
          DmatKDXk.setElemAdd(k,l,mkl,cohesiveCoeffs(l));          
        };
        
        // compute DmatRiDX
        DmatKDXk.PtAP(matZ,DmatRiDX);
        DmatRiDX.scale(-1.);
        fullMatrixOperation::mult(DmatEDXk.getData(),matZ,temp);
        DmatRiDX += temp;
        temp.transposeInPlace();
        DmatRiDX += temp;
        //
        fullMatrixOperation::mult(Cin,DmatRiDX,Cin,DCeffDXk);
        DCeffDXk.scale(-1.);
      };
     
      for (int k =0; k< numCoh; k++)
      {
        // for normal K
        std::vector<fullMatrix<double> >& DCeffDvarsk =  (*DCeffDnormalVars)[k];
        for (int iv=0; iv < numDirVars; iv++)
        {
          fullMatrix<double>& DCeffDXk = DCeffDvarsk[iv];
          fullMatrixOperation::allocateAndMakeZero(DCeffDXk,Ceff.size1(),Ceff.size2());
          //
          DmatEDXk.setAll(0);
          //
          DmatEDXk.setElemAdd(0,k,allDHDn0[k],DnormalDVars[k](0,iv)*cohesiveCoeffs(k));
          DmatEDXk.setElemAdd(0,k,allDHDn1[k],DnormalDVars[k](1,iv)*cohesiveCoeffs(k));
          DmatEDXk.setElemAdd(0,k,allDHDn2[k],DnormalDVars[k](2,iv)*cohesiveCoeffs(k));
          
          DmatKDXk.setAll(0.);
          static fullMatrix<double> DQinDVar;
          DQinDVar = allDQinDn0[k];
          DQinDVar.scale(DnormalDVars[k](0,iv));
          DQinDVar.axpy(allDQinDn1[k],DnormalDVars[k](1,iv));
          DQinDVar.axpy(allDQinDn2[k],DnormalDVars[k](2,iv));
          DmatKDXk.setElemAdd(k,k,DQinDVar,cohesiveCoeffs(k));
           // row k and column k are non-zero
          for (int l=0; l< numCoh; l++)
          {
            static fullMatrix<double> DmlkDvar;
            fullMatrixOperation::mult(allHTranspose[l],Cin,allDHDn0[k],DmlkDvar);
            DmlkDvar.scale(DnormalDVars[k](0,iv));
            fullMatrixOperation::multAdd(allHTranspose[l],Cin,allDHDn1[k],DmlkDvar,DnormalDVars[k](1,iv));
            fullMatrixOperation::multAdd(allHTranspose[l],Cin,allDHDn2[k],DmlkDvar,DnormalDVars[k](2,iv));
            //
            static fullMatrix<double> DmklDvar;
            fullMatrixOperation::mult(allDHTransposeDn0[k],Cin,allH[l],DmklDvar);
            DmklDvar.scale(DnormalDVars[k](0,iv));
            fullMatrixOperation::multAdd(allDHTransposeDn1[k],Cin,allH[l],DmklDvar,DnormalDVars[k](1,iv));
            fullMatrixOperation::multAdd(allDHTransposeDn2[k],Cin,allH[l],DmklDvar,DnormalDVars[k](2,iv));
            //
            DmatKDXk.setElemAdd(l,k,DmlkDvar,cohesiveCoeffs(k)*cohesiveCoeffs(l));
            DmatKDXk.setElemAdd(k,l,DmklDvar,cohesiveCoeffs(k)*cohesiveCoeffs(l));          
          };
          
          
          // compute DmatRiDX
          DmatKDXk.PtAP(matZ,DmatRiDX);
          DmatRiDX.scale(-1.);
          fullMatrixOperation::mult(DmatEDXk.getData(),matZ,temp);
          DmatRiDX += temp;
          temp.transposeInPlace();
          DmatRiDX += temp;
          //
          fullMatrixOperation::mult(Cin,DmatRiDX,Cin,DCeffDXk);
          DCeffDXk.scale(-1.);
        }
      }
    }
  }
};

void BimaterialHomogenization::compute(const fullMatrix<double>& C1, const fullMatrix<double>& C2, double f1, double normedtheta, fullMatrix<double>& Ceff) const
{
  static hyperFullMatrix DCeffDC1, DCeffDC2; 
  static fullMatrix<double> DCeffDf1, DCeffDf2;
  
  compute(C1,C2,f1,normedtheta,Ceff,false,DCeffDC1,DCeffDC2,DCeffDf1,DCeffDf2);
};

void BimaterialHomogenization::compute(const fullMatrix<double>& C1, const fullMatrix<double>& C2, double f1, double n0, double n1, double n2, fullMatrix<double>& Ceff) const
{
  SVector3 normal;
  normal[0] = n0;
  normal[1] = n1;
  normal[2] = n2;
  normal.normalize();
  normal.print("normal");
  
  computeBimaterial(C1,C2,f1,1-f1,normal,Ceff);
};


void BimaterialHomogenization::compute(const std::vector<fullMatrix<double> >& Cin, const std::vector<double>& f, const SVector3& normal,
                        fullMatrix<double>& Ceff, 
                        bool stiff, 
                        std::vector<hyperFullMatrix>& DCeffDcin, 
                        std::vector<fullMatrix<double> >& DCeffDf, 
                        std::vector<fullMatrix<double> >&DCeffDnormal) const
{
  if (Cin.size() != 2)
  {
    Msg::Error("wroing number of input %d",Cin.size());
    Msg::Exit(0);
  };
  // allocate
  if (stiff)
  {
    if (DCeffDcin.size() != 2)
    {
      DCeffDcin.resize(2);
    }
    if (DCeffDf.size() != 2)
    {
      DCeffDf.resize(2);
    }
    if (DCeffDnormal.size() != 3)
    {
      DCeffDnormal.resize(3);
    }   
    computeBimaterial(Cin[0],Cin[1],f[0],f[1],normal,Ceff,
                    stiff,&DCeffDcin[0],&DCeffDcin[1],&DCeffDf[0],&DCeffDf[1],&DCeffDnormal[0],&DCeffDnormal[1],&DCeffDnormal[2]); 
  }
  else
  {
    computeBimaterial(Cin[0],Cin[1],f[0],f[1],normal,Ceff);
  }
};

void LaminateHomogenization::compute(const fullMatrix<double>& C1, const fullMatrix<double>& C2, const fullMatrix<double>& C3, 
                        double f1, double f2, double normedtheta, fullMatrix<double>& Ceff) const
{
  if (_planeStrain && C1.size1() != 3)
  {
    Msg::Error("tangent must be in 3x3 matrix");
    Msg::Exit(0);
  }
  static std::vector<fullMatrix<double> > Cin(3);
  static std::vector<double> fin(3);
  static std::vector<hyperFullMatrix> DCeffDCin;
  static std::vector<fullMatrix<double> > DCeffDfin;
  static std::vector<fullMatrix<double> > DCeffDnormal;
  static SVector3 normal;
  //
  double pi = 3.14159265359;
  double theta = 2.*pi*normedtheta;
  normal[0] = cos(theta);
  normal[1] = sin(theta);
  normal[2] = 0.;
  
  Cin[0] = C1;
  Cin[1] = C2;
  Cin[2] = C3;
  fin[0] = f1;
  fin[1] = f2;
  fin[2] = 1-f1-f2;
  compute(Cin,fin,normal,Ceff,true,DCeffDCin,DCeffDfin,DCeffDnormal);
  for (int i=0; i< DCeffDCin.size(); i++)
  {
    std::string mess = "DCeffDCin "+std::to_string(i);
    DCeffDCin[i].printData(mess);
    mess = "DCeffDfin "+std::to_string(i);
    DCeffDfin[i].print(mess);
  }
  DCeffDnormal[0].print("DCeffDnormal-0");
  DCeffDnormal[1].print("DCeffDnormal-1");
  DCeffDnormal[2].print("DCeffDnormal-2");
};

void LaminateHomogenization::compute(const fullMatrix<double>& C1, const fullMatrix<double>& C2, const fullMatrix<double>& C3, const fullMatrix<double>& C4, 
                        double f1, double f2, double f3, double normedtheta, fullMatrix<double>& Ceff) const
{
  if (_planeStrain && C1.size1() != 3)
  {
    Msg::Error("tangent must be in 3x3 matrix");
    Msg::Exit(0);
  }
  static std::vector<fullMatrix<double> > Cin(4);
  static std::vector<double> fin(4);
  static std::vector<hyperFullMatrix> DCeffDCin;
  static std::vector<fullMatrix<double> > DCeffDfin;
  static std::vector<fullMatrix<double> > DCeffDnormal;
  static SVector3 normal;
  //
  double pi = 3.14159265359;
  double theta = 2.*pi*normedtheta;
  normal[0] = cos(theta);
  normal[1] = sin(theta);
  normal[2] = 0.;
  
  Cin[0] = C1;
  Cin[1] = C2;
  Cin[2] = C3;
  Cin[3] = C4;
  fin[0] = f1;
  fin[1] = f2;
  fin[2] = f3;
  fin[3] = 1-f1-f2-f3;
  compute(Cin,fin,normal,Ceff,true,DCeffDCin,DCeffDfin,DCeffDnormal);
  for (int i=0; i< DCeffDCin.size(); i++)
  {
    std::string mess = "DCeffDCin "+std::to_string(i);
    DCeffDCin[i].printData(mess);
    mess = "DCeffDfin "+std::to_string(i);
    DCeffDfin[i].print(mess);
  }
  DCeffDnormal[0].print("DCeffDnormal-0");
  DCeffDnormal[1].print("DCeffDnormal-1");
  DCeffDnormal[2].print("DCeffDnormal-2");
};

void LaminateHomogenization::compute(const std::vector<fullMatrix<double> >& Cin, const std::vector<double>& f, const SVector3& normal,
                        fullMatrix<double>& Ceff, 
                        bool stiff, 
                        std::vector<hyperFullMatrix>& DCeffDcin, 
                        std::vector<fullMatrix<double> >& DCeffDf, 
                        std::vector<fullMatrix<double> >&DCeffDnormal) const
{
  int numPhase = Cin.size();
  if (stiff)
  {
    if (DCeffDcin.size() != numPhase)
    {
      DCeffDcin.resize(numPhase);
    }
    if (DCeffDf.size() != numPhase)
    {
      DCeffDf.resize(numPhase);
    }
    if (DCeffDnormal.size() != 3)
    {
      DCeffDnormal.resize(3);
    }
  };
  
  if (numPhase ==1) 
  {
    Ceff = Cin[0];
    if (stiff)
    {
      DCeffDcin[0].allocate(Ceff.size1(),Cin[0].size1(),1.);
      fullMatrixOperation::allocateAndMakeZero(DCeffDf[0],Cin[0].size1(),Cin[0].size2());
      fullMatrixOperation::allocateAndMakeZero(DCeffDnormal[0],Cin[0].size1(),Cin[0].size2());
      fullMatrixOperation::allocateAndMakeZero(DCeffDnormal[1],Cin[0].size1(),Cin[0].size2());
      fullMatrixOperation::allocateAndMakeZero(DCeffDnormal[2],Cin[0].size1(),Cin[0].size2());
    }
  }
  else if (numPhase == 2)
  {
    if (stiff)
    {
      computeBimaterial(Cin[0],Cin[1],f[0],f[1],normal,Ceff,
                    stiff,&DCeffDcin[0],&DCeffDcin[1],
                    &DCeffDf[0],&DCeffDf[1],&DCeffDnormal[0],
                    &DCeffDnormal[1],&DCeffDnormal[2]);
    }
    else
    {
      computeBimaterial(Cin[0],Cin[1],f[0],f[1],normal,Ceff);
    }
  }
  else
  {
    //
    auto getSumFaction = [](const std::vector<double>& ff, int i, fullVector<double>& DoutDf)
    {
      DoutDf.resize(ff.size());
      DoutDf.setAll(0.);
      double v= 0;
      for (int k=0; k< i+1; k++)
      {
        v += ff[k];
        DoutDf(k) = 1.;
      }
      return v;
    };
    
    static std::vector<hyperFullMatrix> A, B;
    static std::vector<fullMatrix<double> > CC, F0, F1, N0, N1, N2;
    static fullMatrix<double> Df0iDf, Df1iDf;
    
    if (CC.size() != numPhase)
    {
      CC.resize(numPhase);
    };
    //
    if (stiff)
    {
      // resallocate for static variable
      if (A.size() != numPhase-1) A.resize(numPhase-1);
      if (B.size() != numPhase-1) B.resize(numPhase-1);
      if (F0.size() != numPhase-1) F0.resize(numPhase-1);
      if (F1.size() != numPhase-1) F1.resize(numPhase-1);
      if (N0.size() != numPhase-1) N0.resize(numPhase-1);
      if (N1.size() != numPhase-1) N1.resize(numPhase-1);
      if (N2.size() != numPhase-1) N2.resize(numPhase-1);
      
      fullMatrixOperation::allocateAndMakeZero(Df0iDf,numPhase-1,numPhase);
      fullMatrixOperation::allocateAndMakeZero(Df1iDf,numPhase-1,numPhase);
      
      for (int j=0; j< numPhase; j++)
      {
        fullMatrixOperation::allocateAndMakeZero(DCeffDf[j],Cin[0].size1(),Cin[0].size2());
      }
    }
    //
    CC[0] = Cin[0];
    for (int i=0; i< numPhase-1; i++)
    {
      static fullVector<double> DftolDf, DftolPrevDf;
      double ftol = getSumFaction(f,i+1,DftolDf);
      double ftolPrev = getSumFaction(f,i,DftolPrevDf);
      
      double f0i = ftolPrev/ftol;
      double f1i = 1.-f0i;
      if (stiff)
      {     
        for (int r=0; r< numPhase; r++)
        {
          Df0iDf(i,r) = (-ftolPrev/ftol/ftol)*DftolDf(r) + (1./ftol)*DftolPrevDf(r);
          Df1iDf(i,r) = -Df0iDf(i,r);
        };
      }
      // two phase
      if (stiff)
      {
        computeBimaterial(CC[i],Cin[i+1],f0i,f1i,normal,CC[i+1],stiff,&A[i],&B[i],&F0[i],&F1[i],&N0[i],&N1[i],&N2[i]);
      }
      else
      {
        computeBimaterial(CC[i],Cin[i+1],f0i,f1i,normal,CC[i+1]);
      }
    };
    //
    Ceff = CC[numPhase-1];
    //
    if (stiff)
    {
      
      DCeffDcin[numPhase-1] = B[numPhase-2];
      //
      for (int j=0; j< numPhase; j++)
      {
        DCeffDf[j].axpy(F0[numPhase-2],Df0iDf(numPhase-2,j));
        DCeffDf[j].axpy(F1[numPhase-2],Df1iDf(numPhase-2,j));
      }
      //
      DCeffDnormal[0] = N0[numPhase-2];
      DCeffDnormal[1] = N1[numPhase-2];
      DCeffDnormal[2] = N2[numPhase-2];
      
      static hyperFullMatrix AA;
      AA = A[numPhase-2];
      for (int i=numPhase-2; i>0; i--)
      {
        fullMatrixOperation::mult(AA,B[i-1],DCeffDcin[i],1.);
        
        static fullMatrix<double> AAF0, AAF1;
        fullMatrixOperation::mult(AA,F0[i-1],AAF0);
        fullMatrixOperation::mult(AA,F1[i-1],AAF1);
        //
        for (int j=0; j< numPhase; j++)
        {
          DCeffDf[j].axpy(AAF0,Df0iDf(i-1,j));
          DCeffDf[j].axpy(AAF1,Df1iDf(i-1,j));
        }
        //
        fullMatrixOperation::multAdd(AA,N0[i-1],DCeffDnormal[0],1.);
        fullMatrixOperation::multAdd(AA,N1[i-1],DCeffDnormal[1],1.);
        fullMatrixOperation::multAdd(AA,N2[i-1],DCeffDnormal[2],1.);
        //
        hyperFullMatrix temp;
        fullMatrixOperation::mult(AA,A[i-1],temp,1.);
        AA = temp;
      }
      DCeffDcin[0]=AA;
    }
  }
}
void LaminateHomogenization::printInfos() const
{
  printf("using Voigt, Reuss with laminate \n");
};

LossMeasure* LossMeasure::createLossMeasure(const char what[])
{
  if (strcmp(what, "mare")==0)
  {
    //Msg::Info("create SquareRootError");
    return new SquareRootError();
  }
  else if (strcmp(what, "msre")==0)
  {
    //Msg::Info("create SquareError");
    return new SquareError();
  }
  else
  {
    Msg::Error("%s has not been implemented",what);
    return NULL;
  }
};

double SquareError::get(const fullMatrix<double>& Cref, const fullMatrix<double>& C, bool stiff, fullMatrix<double>* DnormDC) const
{
  if (stiff)
  {
    fullMatrixOperation::allocateAndMakeZero(*DnormDC,C.size1(),C.size2());
  }
  
  double y = 0.;
  double yref = 0;
  for (int i=0; i< C.size1(); i++)
  {
    for (int j=0; j<C.size2(); j++)
    {
      y += 0.5*(Cref(i,j) - C(i,j))*(Cref(i,j) - C(i,j));
      yref += 0.5*Cref(i,j)*Cref(i,j);
      if (stiff)
      {
        (*DnormDC)(i,j) = (C(i,j) - Cref(i,j));
      }
    }
  }
  if (stiff)
  {
    DnormDC->scale(1./yref);
  }
  return y/yref;
};

double SquareRootError::get(const fullMatrix<double>& Cref, const fullMatrix<double>& C, bool stiff, fullMatrix<double>* DnormDC) const
{
  if (stiff)
  {
    fullMatrixOperation::allocateAndMakeZero(*DnormDC,C.size1(),C.size2());
  }
  
  double y = 0.;
  double yref = 0;
  for (int i=0; i< C.size1(); i++)
  {
    for (int j=0; j<C.size2(); j++)
    {
      y += 0.5*(Cref(i,j) - C(i,j))*(Cref(i,j) - C(i,j));
      yref += 0.5*Cref(i,j)*Cref(i,j);
      if (stiff)
      {
        (*DnormDC)(i,j) = (C(i,j) - Cref(i,j));
      }
    }
  }
  if (stiff)
  {
    DnormDC->scale(1./sqrt(yref)/2./sqrt(y));
  }
  return sqrt(y/yref);
};

TrainingDeepMaterialNetwork::TrainingDeepMaterialNetwork(Tree& T, const Homogenization& ho): _T(&T), _homo(&ho),_lambda(0.), _cohesiveHomo(NULL)
{
  Tree::nodeContainer allNodes;
  _T->getAllNodes(allNodes);
  for (int i=0; i< allNodes.size(); i++)
  {
    _offlineData[allNodes[i]] = new OfflineData();
  };
};

TrainingDeepMaterialNetwork::~TrainingDeepMaterialNetwork()
{
  for (std::map<const TreeNode*, OfflineData*>::iterator it = _offlineData.begin(); it != _offlineData.end(); it++)
  {
    delete it->second;
  }
  _offlineData.clear();
}

void TrainingDeepMaterialNetwork::setPenalty(double l)
{
  _lambda = l;
  Msg::Info("using lambda = %e",l);
};

void TrainingDeepMaterialNetwork::fixAllWeights()
{
  Tree::nodeContainer allNodes;
  _T->getAllNodes(allNodes);
  for (int i=0; i< allNodes.size(); i++)
  {
    const TreeNode& node = *(allNodes[i]);
    std::vector<Dof> R;
    getKeys(&node,R);
    if (node.childs.size() ==0)
    {
      _fixedDof[R[0]] = node.weight;
    }
  };
};

void TrainingDeepMaterialNetwork::fixAllLaminateDirections()
{
  Tree::nodeContainer allNodes;
  _T->getAllNodes(allNodes);
  for (int i=0; i< allNodes.size(); i++)
  {
    const TreeNode& node = *(allNodes[i]);
    std::vector<Dof> R;
    getKeys(&node,R);
    if (node.childs.size() > 0)
    {
      for (int j=0; j< R.size(); j++)
      {
        _fixedDof[R[j]] = node.direction[j];
      }
    }
  };  
};

void TrainingDeepMaterialNetwork::fixAllCohesiveDirections()
{
  Tree::nodeContainer allNodes;
  _T->getAllNodes(allNodes);
  for (int i=0; i< allNodes.size(); i++)
  {
    const TreeNode& node = *(allNodes[i]);
    std::vector<Dof> R;
    getKeys(&node,R);
      // leaf
    if (node.withCohesive() && node.childs.size() ==0 )
    {
      int star = 1+node.cohesiveDirectionVars.size1();
      for (int k=0; k< node.cohesiveDirectionVars.size1(); k++)
      {
        for (int l=0; l< node.cohesiveDirectionVars.size2(); l++)
        {
          _fixedDof[R[star]] = node.cohesiveDirectionVars(k,l);
          star ++;
        }
      }
    }
  };
}

void TrainingDeepMaterialNetwork::setCohesiveEnrichement(const HomogenizationWithCohesive& coh)
{
  //
  _cohesiveHomo =  &coh;
};

void TrainingDeepMaterialNetwork::trainingDataSize(int Ns, int numPhase)
{
  printf("size of training set = %d, numPhase =%d\n",Ns,numPhase);
  _XTrain.clear();
  _YTrain.clear();
  _XTrain.resize(Ns,std::vector<fullMatrix<double> >(numPhase));
  _YTrain.resize(Ns);
  _QTrain.clear();
  _QTrain.resize(Ns);
};

void TrainingDeepMaterialNetwork::setTrainingSampleCohesive(int row, const fullVector<double>& Q)
{
  _QTrain[row] = Q;
}
void TrainingDeepMaterialNetwork::setTestSampleCohesive(int row, const fullVector<double>& Q)
{
  _QTest[row] = Q;
}

void TrainingDeepMaterialNetwork::setTrainingSample(int row, const fullMatrix<double>& C1, const fullMatrix<double>& Ceff)
{
  static fullMatrix<double> C2;
  fullMatrixOperation::allocateAndMakeZero(C2,C1.size1(),C1.size2());
  if (_homo->withPlaneStrain())
  {
    fullMatrixOperation::reducePlaneStrain(C1,_XTrain[row][0]);
    fullMatrixOperation::reducePlaneStrain(C2,_XTrain[row][1]);
    fullMatrixOperation::reducePlaneStrain(Ceff,_YTrain[row]);
  }
  else
  {
    fullMatrixOperation::reduceMat99ToMat66(C1,_XTrain[row][0]);
    fullMatrixOperation::reduceMat99ToMat66(C2,_XTrain[row][1]);
    fullMatrixOperation::reduceMat99ToMat66(Ceff,_YTrain[row]);
  }
};

void TrainingDeepMaterialNetwork::setTrainingSample(int row, const fullMatrix<double>& C1, const fullMatrix<double>& C2, const fullMatrix<double>& Ceff)
{
  if (_homo->withPlaneStrain())
  {
    fullMatrixOperation::reducePlaneStrain(C1,_XTrain[row][0]);
    fullMatrixOperation::reducePlaneStrain(C2,_XTrain[row][1]);
    fullMatrixOperation::reducePlaneStrain(Ceff,_YTrain[row]);
  }
  else
  {
    fullMatrixOperation::reduceMat99ToMat66(C1,_XTrain[row][0]);
    fullMatrixOperation::reduceMat99ToMat66(C2,_XTrain[row][1]);
    fullMatrixOperation::reduceMat99ToMat66(Ceff,_YTrain[row]);
  }
};



void TrainingDeepMaterialNetwork::setTrainingSample(int row, const fullMatrix<double>& C1, const fullMatrix<double>& C2,const fullMatrix<double>& C3, const fullMatrix<double>& Ceff)
{
  if (_homo->withPlaneStrain())
  {
    fullMatrixOperation::reducePlaneStrain(C1,_XTrain[row][0]);
    fullMatrixOperation::reducePlaneStrain(C2,_XTrain[row][1]);
    fullMatrixOperation::reducePlaneStrain(C3,_XTrain[row][2]);
    fullMatrixOperation::reducePlaneStrain(Ceff,_YTrain[row]);
  }
  else
  {
    fullMatrixOperation::reduceMat99ToMat66(C1,_XTrain[row][0]);
    fullMatrixOperation::reduceMat99ToMat66(C2,_XTrain[row][1]);
    fullMatrixOperation::reduceMat99ToMat66(C3,_XTrain[row][2]);
    fullMatrixOperation::reduceMat99ToMat66(Ceff,_YTrain[row]);
  }
};

void TrainingDeepMaterialNetwork::setTestSample(int row, const fullMatrix<double>& C1, const fullMatrix<double>& Ceff)
{
  static fullMatrix<double> C2;
  fullMatrixOperation::allocateAndMakeZero(C2,C1.size1(),C1.size2());

  if (_homo->withPlaneStrain())
  {
    fullMatrixOperation::reducePlaneStrain(C1,_XTest[row][0]);
    fullMatrixOperation::reducePlaneStrain(C2,_XTest[row][1]);
    fullMatrixOperation::reducePlaneStrain(Ceff,_YTest[row]);
  }
  else
  {
    fullMatrixOperation::reduceMat99ToMat66(C1,_XTest[row][0]);
    fullMatrixOperation::reduceMat99ToMat66(C2,_XTest[row][1]);
    fullMatrixOperation::reduceMat99ToMat66(Ceff,_YTest[row]);
  }
};


void TrainingDeepMaterialNetwork::setTestSample(int row, const fullMatrix<double>& C1, const fullMatrix<double>& C2, const fullMatrix<double>& Ceff)
{
  if (_homo->withPlaneStrain())
  {
    fullMatrixOperation::reducePlaneStrain(C1,_XTest[row][0]);
    fullMatrixOperation::reducePlaneStrain(C2,_XTest[row][1]);
    fullMatrixOperation::reducePlaneStrain(Ceff,_YTest[row]);
  }
  else
  {
    fullMatrixOperation::reduceMat99ToMat66(C1,_XTest[row][0]);
    fullMatrixOperation::reduceMat99ToMat66(C2,_XTest[row][1]);
    fullMatrixOperation::reduceMat99ToMat66(Ceff,_YTest[row]);
  }
};

void TrainingDeepMaterialNetwork::setTestSample(int row, const fullMatrix<double>& C1, const fullMatrix<double>& C2, const fullMatrix<double>& C3, const fullMatrix<double>& Ceff)
{
  if (_homo->withPlaneStrain())
  {
    fullMatrixOperation::reducePlaneStrain(C1,_XTest[row][0]);
    fullMatrixOperation::reducePlaneStrain(C2,_XTest[row][1]);
    fullMatrixOperation::reducePlaneStrain(C3,_XTest[row][2]);
    fullMatrixOperation::reducePlaneStrain(Ceff,_YTest[row]);
  }
  else
  {
    fullMatrixOperation::reduceMat99ToMat66(C1,_XTest[row][0]);
    fullMatrixOperation::reduceMat99ToMat66(C2,_XTest[row][1]);
    fullMatrixOperation::reduceMat99ToMat66(C3,_XTest[row][2]);
    fullMatrixOperation::reduceMat99ToMat66(Ceff,_YTest[row]);
  }
};

void TrainingDeepMaterialNetwork::testDataSize(int Nt, int numPhase)
{
  printf("size of test set = %d numPhase = %d\n",Nt,numPhase);
  
  _XTest.clear();
  _YTest.clear();
  _XTest.resize(Nt,std::vector<fullMatrix<double> >(numPhase));
  _YTest.resize(Nt);
  _QTest.clear();
  _QTest.resize(Nt);
};

#if 0
void TrainingDeepMaterialNetwork::train(double lr, int maxEpoch, std::string loss, std::string historyFileName, std::string saveTreeFileName, int saveEpoch, int numberBatches, double lrmin)
{
  
  /*
   * 
   * */
  
  printf("start training\n");
  FILE* f = fopen(historyFileName.c_str(),"w");
  fprintf(f,"Epoch;LossTrain;LossTest;mareTrain;mareTest\n");
  // error mesuare
  LossMeasure* errorEval = LossMeasure::createLossMeasure("mare");
  // loss mesure
  LossMeasure* lossEval = LossMeasure::createLossMeasure(loss.c_str());
  
  fullVector<double> Wcur, Wprev, WBest, g;
  //
  numberDofs(Wcur);
  Msg::Info("number of Nodes = %d",_T->getNumberOfNodes());
  Msg::Info("number of unknowns = %d",_unknown.size());
  int numDof = Wcur.size();
  WBest = Wcur;
  Wprev = Wcur;
  g.resize(numDof,true);
  double costfuncPrev = evaluateTrainingSet(*lossEval);
  Msg::Info("costfuncPrev = %e",costfuncPrev);
  
  //OfflineData
  int epoch = 0;
  double costfuncBest = 0;
  int bestEpoch = 0;
  int Ntrain = _XTrain.size();
  double lrCur = lr;
  int numEpochIncrease = 0;
  int numEpochDecrease = 0;
  while (true)
  {
    double time = Cpu();
    int idexprint =0;
    bool batchFailed = false;
    for (int ibat = 0; ibat < numberBatches; ibat++)
    {
      int start = Ntrain*ibat/numberBatches;
      int end = Ntrain*(ibat+1)/numberBatches;
      int numSample = end-start;
      if ((ibat==0 || ibat == (numberBatches*(idexprint+1)/5-1)) && numberBatches > 1)
      {
        Msg::Info("minibatch %dth from %d to %d , number of samples = %d ",ibat,start,end,numSample);
        idexprint ++;
      }
      std::vector<int> allIndex(numSample,0);
      for (int ii=start; ii< end; ii++)
      {
        allIndex[ii] = ii;
      }
      double  costfuncBatch = computeCostFunction(*lossEval,allIndex,g);
      // modify learning rate if necessagre
      if (std::isnan(g.norm()) || std::isinf(g.norm()) || std::isnan(costfuncBatch) || std::isinf(costfuncBatch))
      {
        if (fabs(_lambda) >1e-6)
        {
          Msg::Info("penalty too high, reduce penalty lambda from %.5e to %.5e",_lambda,0.5*_lambda);
          _lambda *= 0.5;
          //
          Wcur = Wprev;
          updateFieldFromUnknown(Wprev);
        }
        else
        {
          Msg::Info("reinitializing tree");
          _T->initialize();
          numberDofs(Wcur);
          Msg::Info("number of Nodes = %d",_T->getNumberOfNodes());
          Msg::Info("number of unknowns = %d",_unknown.size());
          numDof = Wcur.size();
          WBest = Wcur;
          Wprev = Wcur;
          g.resize(numDof,true);
          costfuncPrev =evaluateTrainingSet(*lossEval);
          
          //OfflineData
          epoch = 0;
          costfuncBest = 0;
          bestEpoch = 0;
          Ntrain = _XTrain.size();
          lr *=0.5;
          lrCur = lr;
          numEpochIncrease = 0;
          numEpochDecrease = 0;
        }
        batchFailed = true;
        break;
      }
      else 
      {
        updateUnknown(lrCur,g,Wcur);
        //
        updateFieldFromUnknown(Wcur);
      }
    }
    if (batchFailed)
    {
      continue;
    }
    double costfunc = evaluateTrainingSet(*lossEval);
    if (numberBatches==Ntrain)
    {
      if ((costfunc > costfuncPrev))
      {
        numEpochIncrease ++;
        if (numEpochIncrease == 1)
        {
          numEpochIncrease = 0;
          Msg::Info("reduce learning rate from %.5e to %.5e",lrCur,0.8*lrCur);
          lrCur *= 0.8;
          if (lrCur < lrmin)
          {
            lrCur = lrmin;
          }
          numEpochDecrease = 0;
          Wcur = Wprev;
          updateFieldFromUnknown(Wprev);
          continue;        
        }
      }
      else
      {
        numEpochIncrease = 0;
        numEpochDecrease ++;
        if (numEpochDecrease == 5)
        {
          Msg::Info("increase lr from  %.5e to %.5e ",lrCur,1.2*lrCur);
          numEpochDecrease = 0;
          lrCur *= 1.2;
          if (lrCur > lr)
          {
            lrCur = lr;
          }
        }
        costfuncPrev = costfunc;
        Wprev = Wcur;
      }
    }
    double lossTest = evaluateTestSet(*lossEval);
    //
    double errTrain = evaluateTrainingSet(*errorEval);
    double errTest = evaluateTestSet(*errorEval);
    Msg::Info("epoch = %d/%d (%.3fs) lrCur=%.5e loss-train =%.5e, loss-test =%.5e mare-train = %.5e mare-test = %.5e",epoch,maxEpoch,Cpu()-time,lrCur,costfunc,lossTest,errTrain,errTest);
    fprintf(f,"%d;%.16g;%.16g;%.16g;%.16g\n",epoch,costfunc,lossTest,errTrain,errTest);
    fflush(f);
    if ((epoch > 0) && (epoch%saveEpoch==0))
    {
      if (saveTreeFileName.length() > 0)
      {
        _T->saveDataToFile(saveTreeFileName);
      }
    };
    //
    if (costfunc < costfuncBest || epoch==0)
    {
      costfuncBest = costfunc;
      WBest = Wcur;
      bestEpoch =epoch;
       // save tree
      if (saveTreeFileName.length() > 0 && epoch > 0)
      {
        FILE* ff = fopen("lastBestEpochData.csv","w");
        fprintf(ff,"epoch = %d\nloss-train =%.5e\nloss-test =%.5e\nmare-train = %.5e\nmare-test = %.5e\n",epoch,costfuncBest,lossTest,errTrain,errTest);
        fclose(ff);
        _T->saveDataToFile("best_"+saveTreeFileName);
      }
    };
    
    epoch++;
    if (epoch > maxEpoch)
    {
      break;
    }
  };
  // done training
  fclose(f);
  
  // save tree
  if (saveTreeFileName.length() > 0)
  {
    _T->saveDataToFile(saveTreeFileName);
  }
};

#else
void TrainingDeepMaterialNetwork::train(double lr, int maxEpoch, std::string loss, std::string historyFileName, 
                      std::string saveTreeFileName, int saveEpoch, int numberBatches, double lrmin,
                      bool removeZeroContribution, int numStepRemove, double tolRemove)
{
  
  /*
   * 
   * */
  
  printf("start training\n");
  FILE* f = fopen(historyFileName.c_str(),"w");
  fprintf(f,"Epoch;LossTrain;LossTest;mareTrain;mareTest\n");
  // error mesuare
  LossMeasure* errorEval = LossMeasure::createLossMeasure("mare");
  // loss mesure
  LossMeasure* lossEval = LossMeasure::createLossMeasure(loss.c_str());
  
  fullVector<double> Wcur, Wprev, WBest, g;
  //
  numberDofs(Wcur);
  Msg::Info("number of Nodes = %d",_T->getNumberOfNodes());
  Msg::Info("number of unknowns = %d",_unknown.size());
  int numDof = Wcur.size();
  WBest = Wcur;
  Wprev = Wcur;
  g.resize(numDof,true);
  double costfuncPrev = evaluateTrainingSet(*lossEval);
  Msg::Info("costfuncPrev = %e",costfuncPrev);
  
  //OfflineData
  int epoch = 0;
  double costfuncBest = 0;
  int bestEpoch = 0;
  int Ntrain = _XTrain.size();
  double lrCur = lr;
  int numEpochIncrease = 0;
  int numEpochLrMin = 0;
  int numEpochDecrease = 0;
  double tStart = Cpu();
  while (true)
  {
    double time = Cpu();
    int idexprint =0;
    bool batchFailed = false;
    for (int ibat = 0; ibat < numberBatches; ibat++)
    {
      int start = Ntrain*ibat/numberBatches;
      int end = Ntrain*(ibat+1)/numberBatches;
      int numSample = end-start;
      if ((ibat==0 || ibat == (numberBatches*(idexprint+1)/5-1)) && numberBatches > 1)
      {
        Msg::Info("minibatch %dth from %d to %d , number of samples = %d ",ibat,start,end,numSample);
        idexprint ++;
      }
      std::vector<int> allIndex(numSample,0);
      for (int ii=start; ii< end; ii++)
      {
        allIndex[ii-start] = ii;
      }
      double  costfuncBatch = computeCostFunction(*lossEval,allIndex,g);
      // modify learning rate if necessagre
      if (std::isnan(g.norm()) || std::isinf(g.norm()) || std::isnan(costfuncBatch) || std::isinf(costfuncBatch))
      {
        if (fabs(_lambda) >1e-6)
        {
          Msg::Info("penalty too high, reduce penalty lambda from %.5e to %.5e",_lambda,0.5*_lambda);
          _lambda *= 0.5;
          //
          Wcur = Wprev;
          updateFieldFromUnknown(Wprev);
        }
        else
        {
          Msg::Info("reinitializing tree");
          _T->initialize();
          numberDofs(Wcur);
          Msg::Info("number of Nodes = %d",_T->getNumberOfNodes());
          Msg::Info("number of unknowns = %d",_unknown.size());
          numDof = Wcur.size();
          WBest = Wcur;
          Wprev = Wcur;
          g.resize(numDof,true);
          costfuncPrev = 0.;
          
          //OfflineData
          epoch = 0;
          costfuncBest = 0;
          bestEpoch = 0;
          Ntrain = _XTrain.size();
          lr *=0.5;
          lrCur = lr;
          numEpochIncrease = 0;
          numEpochDecrease = 0;
          tStart = Cpu();
        }
        batchFailed = true;
        break;
      }
      else 
      {
        updateUnknown(lrCur,g,Wcur);
        //
        updateFieldFromUnknown(Wcur);
      }
    }
    if (batchFailed)
    {
      continue;
    }
    double costfunc = evaluateTrainingSet(*lossEval);
    if (numberBatches==Ntrain)
    {
      if (costfunc > costfuncPrev)
      {
        numEpochIncrease ++;
        if (numEpochIncrease == 1)
        {
          numEpochIncrease = 0;
          Msg::Info("costfunc = %e, reduce learning rate from %.5e to %.5e",costfunc,lrCur,0.8*lrCur);
          lrCur *= 0.8;
          if (lrCur < lrmin)
          {
            lrCur = lrmin;
            numEpochLrMin ++;
            if (numEpochLrMin>5)
            {
              Msg::Info("maximal number of iterations %d at minimal learning step is reached !!",numEpochLrMin);
              break;
            }
          }
          numEpochDecrease = 0;
          Wcur = Wprev;
          updateFieldFromUnknown(Wprev);
          continue;        
        }
      }
      else
      {
        numEpochIncrease = 0;
        numEpochDecrease ++;
        if (numEpochDecrease == 5)
        {
          Msg::Info("increase lr from  %.5e to %.5e ",lrCur,1.2*lrCur);
          numEpochDecrease = 0;
          lrCur *= 1.2;
          if (lrCur > lr)
          {
            lrCur = lr;
          }
        }
        costfuncPrev = costfunc;
        Wprev = Wcur;
      }
    }
    double lossTest = evaluateTestSet(*lossEval);
    //
    double errTrain = evaluateTrainingSet(*errorEval);
    double errTest = evaluateTestSet(*errorEval);
    
    
    Msg::Info("epoch = %d/%d (%.3fs/total time =%.3fs) lrCur=%.5e loss-train =%.5e, loss-test =%.5e mare-train = %.5e mare-test = %.5e",
                                        epoch,maxEpoch,Cpu()-time,Cpu()-tStart,lrCur,costfunc,lossTest,errTrain,errTest);
    fprintf(f,"%d;%.16g;%.16g;%.16g;%.16g\n",epoch,costfunc,lossTest,errTrain,errTest);
    fflush(f);
    
    if ((epoch > 0) && (epoch%saveEpoch==0))
    {
      if (saveTreeFileName.length() > 0)
      {
        _T->saveDataToFile(saveTreeFileName);
      }
    };
    //
    if (costfunc < costfuncBest || epoch==0)
    {
      costfuncBest = costfunc;
      WBest = Wcur;
      bestEpoch =epoch;
       // save tree
      if (saveTreeFileName.length() > 0 && epoch > 0)
      {
        FILE* ff = fopen("lastBestEpochData.csv","w");
        fprintf(ff,"epoch = %d\nloss-train =%.5e\nloss-test =%.5e\nmare-train = %.5e\nmare-test = %.5e\n",epoch,costfuncBest,lossTest,errTrain,errTest);
        fclose(ff);
        _T->saveDataToFile("best_"+saveTreeFileName);
      }
    };
    
    if (removeZeroContribution)
    {
      
      if ((epoch+1) %numStepRemove==0)
      {
        double costfuncPrev_pre = evaluateTrainingSet(*lossEval);
        Msg::Info("tree removing");
        bool removed =  _T->removeLeavesWithZeroContribution(tolRemove);
        if (removed)
        {
          numberDofs(Wcur);
          Msg::Info("number of Nodes = %d",_T->getNumberOfNodes());
          Msg::Info("number of unknowns = %d",_unknown.size());
          int numDof = Wcur.size();
          WBest = Wcur;
          Wprev = Wcur;
          g.resize(numDof,true);
        }
        costfuncPrev = evaluateTrainingSet(*lossEval);
        Msg::Info("pre-removing costfuncPrev = %e and after removing costfuncPrev = %e",costfuncPrev_pre,costfuncPrev);
      }
      
    }
    
    epoch++;
    if (epoch > maxEpoch)
    {
      break;
    }
  };
  // done training
  fclose(f);
  
  // save tree
  if (saveTreeFileName.length() > 0)
  {
    _T->saveDataToFile(saveTreeFileName);
  }
};

#endif //0
void TrainingDeepMaterialNetwork::getKeys(const TreeNode* node, std::vector<Dof>& keys) const
{
  if (node->childs.size() ==0)
  {
    // leaf
    keys.push_back(Dof(node->location, Dof::createTypeWithTwoInts(0, node->depth+1)));
    if (node->withCohesive())
    {
      int star = 1;
      for (int i=0; i< node->cohesiveCoeffVars.size(); i++)
      {
        keys.push_back(Dof(node->location, Dof::createTypeWithTwoInts(star, node->depth+1)));
        star ++;
      }
      for (int i=0; i< node->cohesiveDirectionVars.size1(); i++)
      {
        for (int j=0; j< node->cohesiveDirectionVars.size2(); j++)
        {
          keys.push_back(Dof(node->location, Dof::createTypeWithTwoInts(star, node->depth+1)));
          star ++;
        }
      }
    }
  }
  else
  {
    int numberNormal = node->childs.size()-1;
    int totalNumberDirVars = node->direction.size();
    int numberVarsPerNormal = totalNumberDirVars/numberNormal;
    for (int i=0; i< numberVarsPerNormal; i++)
    {
      keys.push_back(Dof(node->location, Dof::createTypeWithTwoInts(i, node->depth+1)));
    }
  }
};

int TrainingDeepMaterialNetwork::getDofNumber(Dof key) const
{
  std::map<Dof,int>::const_iterator itF =_unknown.find(key);
  if (itF == _unknown.end())
  {
    printf("wrong dof initialization TrainingDeepMaterialNetwork::homogenizationProcess\n");
    return -1;
  }
  else
  {
    return itF->second;
  };
}

void TrainingDeepMaterialNetwork::numberDofs(fullVector<double>& ufield)
{
  Msg::Info("start dof numbering");
  _unknown.clear();  
  // Dof of all nodes
  Tree::nodeContainer allNodes;
  _T->getAllNodes(allNodes);
  for (int i=0; i< allNodes.size(); i++)
  {
    std::vector<Dof> R;
    getKeys(allNodes[i],R);
    for (int j=0; j< R.size(); j++)
    {
      Dof& key = R[j];
      if((_unknown.find(key) == _unknown.end()) and (_fixedDof.find(key) == _fixedDof.end()))
      {
        std::size_t size = _unknown.size();
        _unknown[key] = size;
      }
    }
  }
  // initialize data from tree
  //
  int sizeOfR = _unknown.size();
  ufield.resize(sizeOfR,true);
    //
  for (int i=0; i< allNodes.size(); i++)
  {
    const TreeNode& node = *(allNodes[i]);
    std::vector<Dof> R;
    getKeys(&node,R);
    int Rsize = R.size();
    if (node.childs.size() ==0)
    {
      int numCoh = 0; 
      int numCohDirVars = 0;
      if (node.withCohesive())
      {
        numCoh = node.cohesiveCoeffVars.size();
        numCohDirVars = node.cohesiveDirectionVars.size2();
      }
      for (int j=0; j< Rsize; j++)
      {
        std::map<Dof,int>::iterator itR =  _unknown.find(R[j]);
        if (itR != _unknown.end())
        {
          if (j == 0)
          {
            // leaf
            ufield(itR->second) = node.weight;
          }
          else if ((j > 0) && (j< numCoh+1))
          {
            ufield(itR->second) = node.cohesiveCoeffVars(j-1);
          }
          else if ((j >= numCoh+1) && (j < numCoh*numCohDirVars+numCoh+1))
          {
            int index = j -(numCoh+1);
            int k = index/numCohDirVars;
            int l = index%numCohDirVars;
            //Msg::Info("index = %d k=%d, l=%d",index,k,l);
            ufield(itR->second) = node.cohesiveDirectionVars(k,l);
          }
          else
          {
            Msg::Error("wrong position");
            Msg::Exit(0);
          }
        };
      }
    }
    else
    {
      for (int j=0; j< Rsize; j++)
      {
        std::map<Dof,int>::iterator itR =  _unknown.find(R[j]);
        if (itR != _unknown.end())
        {
          ufield(itR->second) = node.direction[j];
        };
      }
    }
  };  
  //
  // allocate
  for (int i=0; i< allNodes.size(); i++)
  {
    const TreeNode& node = *(allNodes[i]);
    OfflineData& data = *(_offlineData[&node]);
    if (_homo->withPlaneStrain())
    {
      fullMatrixOperation::allocateAndMakeZero(data.Cel,3,3);
      data.DCeffDunknown.clear();
    }
    else
    {
      fullMatrixOperation::allocateAndMakeZero(data.Cel,6,6);
      data.DCeffDunknown.clear();
    }
    // cohesive
    if (node.withCohesive())
    {
      fullMatrixOperation::allocateAndMakeZero(data.Qel,2);
    }
  } 
  Msg::Info("done dof numbering");
};

double TrainingDeepMaterialNetwork::getDofValue(Dof key, const fullVector<double>& ufield) const
{
  std::map<Dof,double>::const_iterator itFixed = _fixedDof.find(key); 
  if (itFixed != _fixedDof.end())
  {
    return itFixed->second;
  }
  std::map<Dof,int>::const_iterator itR =  _unknown.find(key);
  if (itR != _unknown.end())
  {
    return ufield(itR->second);
  }
  Msg::Error("Dof is not found in TrainingDeepMaterialNetwork::getDofValue!!!");
  Msg::Exit(0);
}

void TrainingDeepMaterialNetwork::updateFieldFromUnknown(const fullVector<double>& ufield)
{
  std::vector<TreeNode*> allNodes;
  _T->getRefToAllNodes(allNodes);
  //
  for (int i=0; i< allNodes.size(); i++)
  {
    TreeNode& node = *(allNodes[i]);
    std::vector<Dof> R;
    getKeys(&node,R);
    int Rsize = R.size();
    fullVector<double> val(Rsize);
    for (int j=0; j< Rsize; j++)
    {
      val(j) = getDofValue(R[j],ufield);
    }
    //
    if (node.childs.size() ==0)
    {
      // leaf
      node.weight = val(0);
      if (node.withCohesive())
      {
        int numCoh = node.cohesiveCoeffVars.size();
        int numCohDirVars = node.cohesiveDirectionVars.size2();
        for (int k=0; k< numCoh; k++)
        {
          node.cohesiveCoeffVars(k)= val(k+1);
        }
        int star = 0;
        for (int k=0; k< numCoh; k++)
        {
          for (int l=0; l< numCohDirVars; l++)
          {
            node.cohesiveDirectionVars(k,l) = val(numCoh+1+star);
            star ++;
          }
        }
      }
    }
    else
    {
      int numberNormal = node.childs.size()-1;
      int totalNumberDirVars = node.direction.size();
      int numberVarsPerNormal = totalNumberDirVars/numberNormal;
      for (int j=0; j< numberNormal; j++)
      {
        for (int k=0; k< numberVarsPerNormal; k++)
        {
          node.direction[j*numberVarsPerNormal+k] = val(k);
        }
      }
    };
  };
  
  // update weigt
  for (int i=0; i< allNodes.size(); i++)
  {
    TreeNode& node = *(allNodes[i]);
    if (node.childs.size() >0)
    {
      Tree::nodeContainer associatedLeaves;
      _T->getAssociatedLeavesForNode(&node,associatedLeaves);
      node.weight = 0.;
      for (int j=0; j< associatedLeaves.size(); j++)
      {
        const TreeNode& leaf = *(associatedLeaves[j]);
        node.weight += leaf.af->getVal(leaf.weight);
      };      
    }
  };
};

void TrainingDeepMaterialNetwork::getNormal(const TreeNode* node, SVector3& vec, bool stiff, std::vector<SVector3>* DnormalDunknown) const
{
  double pi = 3.14159265359;
  int numberNormal = node->childs.size()-1;
  int totalNumberDirVars = node->direction.size();
  int numberVarsPerNormal = totalNumberDirVars/numberNormal;
  
  if (numberVarsPerNormal == 1)
  {
    double theta = 2.*pi*node->direction[0];
    vec[0] = cos(theta);
    vec[1] = sin(theta);
    vec[2] = 0.;
    if (stiff)
    {
      if ((*DnormalDunknown).size() != 1)
      {
        (*DnormalDunknown).resize(1);
      }
      (*DnormalDunknown)[0][0] = -sin(theta)*2.*pi;
      (*DnormalDunknown)[0][1] = cos(theta)*2.*pi;
      (*DnormalDunknown)[0][2] = 0.;
      
    }
  }
  else if (numberVarsPerNormal == 2)
  {
    // cylindrical coordinates
    double phi = 2.*pi*node->direction[0];
    double theta = pi*node->direction[1];
    vec[0] = sin(theta)*cos(phi);
    vec[1] = sin(theta)*sin(phi);
    vec[2] = cos(theta);
    
    if (stiff)
    {
      if ((*DnormalDunknown).size() != 2)
      {
        (*DnormalDunknown).resize(2);
      }
      
      (*DnormalDunknown)[0][0] = -sin(theta)*sin(phi)*2*pi;
      (*DnormalDunknown)[0][1] = sin(theta)*cos(phi)*2*pi;
      (*DnormalDunknown)[0][2] = 0.;
      
      (*DnormalDunknown)[1][0] = cos(theta)*cos(phi)*pi;
      (*DnormalDunknown)[1][1] = cos(theta)*sin(phi)*pi;
      (*DnormalDunknown)[1][2] = -sin(theta)*pi;
    }
  }
  else
  {
    printf("other dimension %d has not been implemented \n",numberVarsPerNormal);
  }
};

void TrainingDeepMaterialNetwork::homogenizationProcess(bool stiff)
{
  // start with leaves
  if (_cohesiveHomo != NULL)
  {
    Tree::nodeContainer allLeaves;
    _T->getAllLeaves(allLeaves);
    for (int i=0; i< allLeaves.size(); i++)
    {
      const TreeNode* leaf = allLeaves[i];
      if (leaf->withCohesive())
      {
        OfflineData& dataLeave = *(_offlineData[leaf]);
        
        // copy Cin as the one of solid
        static fullMatrix<double> Cin;
        Cin = dataLeave.Cel;
        
        static fullVector<double> trueCohesiveCoeffVars;
        int numCoh = leaf->cohesiveCoeffVars.size();
        trueCohesiveCoeffVars.resize(numCoh);
        for (int j=0; j< numCoh; j++)
        {
          trueCohesiveCoeffVars(j) = leaf->cohesiveAf->getVal(leaf->cohesiveCoeffVars(j));
        }
        
        if (stiff)
        {
          static std::vector<fullMatrix<double> > DCeffDcohesiveCoeffs;
          static std::vector<std::vector<fullMatrix<double> > > DCeffDnormalVars; 
          _cohesiveHomo->compute(Cin, dataLeave.Qel, trueCohesiveCoeffVars, leaf->cohesiveDirectionVars, dataLeave.Cel,
                                 stiff, &DCeffDcohesiveCoeffs, &DCeffDnormalVars);
                                 
          // update  dataLeave.DCeffDunknown
          dataLeave.zeroDCeffDunknown(); // zero first
          std::vector<Dof> R;
          getKeys(leaf, R);
          // coefficients
          for (int j=0; j< numCoh; j++)
          {
            if (_unknown.find(R[j+1]) != _unknown.end())
            {
              double DtrueDval = leaf->cohesiveAf->getDiff(leaf->cohesiveCoeffVars(j));
              fullMatrix<double>& DCeffDkey = dataLeave.getDCeffDunknown(R[j+1],_cohesiveHomo->withPlaneStrain());
              DCeffDkey.axpy(DCeffDcohesiveCoeffs[j],DtrueDval);            
            }
          };
          // normal vars
          int numCohDirVars = leaf->cohesiveDirectionVars.size2();
          int star = 0;
          for (int j=0; j< numCoh; j++)
          {
            for (int k=0; k< numCohDirVars; k++)
            {
              if (_unknown.find(R[star+numCoh+1]) != _unknown.end())
              {
                fullMatrix<double>& DCeffDkey = dataLeave.getDCeffDunknown(R[star+numCoh+1],_cohesiveHomo->withPlaneStrain());
                DCeffDkey = DCeffDnormalVars[j][k];
              }
              star ++;
            }
          }
        }
        else
        {
          _cohesiveHomo->compute(Cin, dataLeave.Qel, trueCohesiveCoeffVars, leaf->cohesiveDirectionVars, dataLeave.Cel);
        }
        //dataLeave.Cel.print("dataLeave.Cel");
      }
    }
  };
  
  
  std::map<int,Tree::nodeContainer> allNodesByDepth;
  _T->getAllNodesByDepth(allNodesByDepth);
  int maxDepth = allNodesByDepth.size()-1; // maximum depth
  
  // cache to avoid reallocated
  static std::vector<fullMatrix<double> > Cin;
  static std::vector<double> fin;
  static std::vector<hyperFullMatrix> DCeffDCin;
  static std::vector<fullMatrix<double> > DCeffDfin;
  static std::vector<fullMatrix<double> > DCeffDnormal;
  static SVector3 normal;
  static std::vector<SVector3> DnormalDunknown;
  //
  for (int i=0; i< maxDepth; i++)
  {
    // compute from maxDepth -1
    int depth = maxDepth-1-i;
    Tree::nodeContainer& layer = allNodesByDepth[depth]; // at node at current depth
    for (int inode = 0; inode < layer.size(); inode++)
    {
      //Msg::Info("depth = %d",depth);
      // for each node in this depth
      const TreeNode* node = layer[inode];
      const std::vector<TreeNode*>& allChilds = node->childs;
      int numChildren = allChilds.size();
      // cheack if node is regular one
      if (numChildren > 0)
      {
        Tree::nodeContainer leaves;
        _T->getAssociatedLeavesForNode(node,leaves);
        
        double W= 0;
        for (int j=0; j< leaves.size(); j++)
        {
          W += leaves[j]->af->getVal(leaves[j]->weight);
        };
        // data on node
        OfflineData& data = *(_offlineData[node]);
        std::vector<const OfflineData*> childData(numChildren,NULL);
        for (int j=0; j<numChildren; j++)
        {
          childData[j] = _offlineData[allChilds[j]];
        }
        // reallocate static vector
        if (Cin.size() != numChildren)
        {
          Cin.resize(numChildren);
          fin.resize(numChildren);
        };
        //
        for (int j=0; j<numChildren; j++)
        {
          Cin[j] =  childData[j]->Cel;
          //
          Tree::nodeContainer leafChild;
          _T->getAssociatedLeavesForNode(allChilds[j],leafChild);
          //
          double Wchild = 0;
          for (int k=0; k< leafChild.size(); k++)
          {
            Wchild += leafChild[k]->af->getVal(leafChild[k]->weight);
          };
          fin[j] = Wchild/W;
        };
        // direction parameters
        getNormal(node,normal,stiff,&DnormalDunknown);
        // homogenized behavior
        _homo->compute(Cin,fin,normal,data.Cel,stiff,DCeffDCin,DCeffDfin,DCeffDnormal);
        if (stiff)
        {
          // update  data.DCeffDunknown
          data.zeroDCeffDunknown(); // zero first
          // reset to zero
          // from child
          for (int j=0; j<numChildren; j++)
          {
            const std::map<Dof, fullMatrix<double> >& DCeffDunknownChild = childData[j]->DCeffDunknown;
            for (std::map<Dof, fullMatrix<double> >::const_iterator itchild = DCeffDunknownChild.begin(); itchild != DCeffDunknownChild.end(); itchild++)
            {
              fullMatrix<double>& DCeffDnormalDof = data.getDCeffDunknown(itchild->first,_homo->withPlaneStrain());
              fullMatrixOperation::multAdd(DCeffDCin[j],itchild->second,DCeffDnormalDof,1.);
            }
          }
          // keys inside node - rotation dofs
          std::vector<Dof> Rrotation;
          getKeys(node,Rrotation);
          // the first Dofs are rotation
          for (int j=0; j< Rrotation.size(); j++)
          {
            if (_unknown.find(Rrotation[j]) != _unknown.end())
            {
              fullMatrix<double>& DCeffDnormalDof = data.getDCeffDunknown(Rrotation[j],_homo->withPlaneStrain());
              DCeffDnormalDof.axpy(DCeffDnormal[0],DnormalDunknown[j][0]); // comp 0
              DCeffDnormalDof.axpy(DCeffDnormal[1],DnormalDunknown[j][1]); // comp 1
              DCeffDnormalDof.axpy(DCeffDnormal[2],DnormalDunknown[j][2]); // comp 2
            }
          };
          
          // for phase percentage
          std::vector<double> DWDWeight(leaves.size(),0.);
          for (int j=0; j< leaves.size(); j++)
          {
            DWDWeight[j] = leaves[j]->af->getDiff(leaves[j]->weight);
          };
          //
          for (int j=0; j<numChildren; j++)
          {
            // weights at leaves
            const TreeNode* child = allChilds[j];
            Tree::nodeContainer leafChild;
            _T->getAssociatedLeavesForNode(child,leafChild);
            //
            double Wchild = 0;
            std::vector<double> DWchildDWleaf(leafChild.size(),0.);
            //
            for (int k=0; k< leafChild.size(); k++)
            {
              Wchild += leafChild[k]->af->getVal(leafChild[k]->weight);
              DWchildDWleaf[k] = leafChild[k]->af->getDiff(leafChild[k]->weight);
            };
            double f = Wchild/W;
            //Msg::Info("child %d f = %.5e",j,f);
            double DfDWchild = 1./W;
            double DfDW = -Wchild/(W*W);
           
            static fullMatrix<double> DCeffDWchild, DCeffDW;
            //
            DCeffDWchild = DCeffDfin[j];
            DCeffDWchild.scale(DfDWchild);
            //
            DCeffDW = DCeffDfin[j];
            DCeffDW.scale(DfDW);
            //
            for (int k=0; k<leaves.size(); k++)
            {
              std::vector<Dof> Rleaf;
              getKeys(leaves[k],Rleaf);
              for (int jj=0; jj < Rleaf.size(); jj++)
              {
                if (_unknown.find(Rleaf[jj]) != _unknown.end())
                {
                  fullMatrix<double>& DCeffDnormalDof = data.getDCeffDunknown(Rleaf[jj],_homo->withPlaneStrain());
                  DCeffDnormalDof.axpy(DCeffDW,DWDWeight[k]);
                }
              };
            };
            for (int k=0; k<leafChild.size(); k++)
            {
              std::vector<Dof> Rleaf;
              getKeys(leafChild[k],Rleaf);
              for (int jj=0; jj < Rleaf.size(); jj++)
              {
                if (_unknown.find(Rleaf[jj]) != _unknown.end())
                {
                  fullMatrix<double>& DCeffDnormalDof = data.getDCeffDunknown(Rleaf[jj],_homo->withPlaneStrain());
                  DCeffDnormalDof.axpy(DCeffDWchild,DWchildDWleaf[k]);
                };
              }
            };
          };
          
          //Msg::Error("size of data.DCeffDunknown = %d",data.DCeffDunknown.size());
        };
      };
    };
  };
};

void TrainingDeepMaterialNetwork::updateUnknown(double lr, const fullVector<double>& g, fullVector<double>& Wcur)
{
  Wcur.axpy(g,-lr);
};

void TrainingDeepMaterialNetwork::testHomogenizationProcess(fullMatrix<double>& C1, const fullMatrix<double>& C2, fullMatrix<double>& Ceff)
{
  //
  fullVector<double> Wcur;
  numberDofs(Wcur);
  // set data to leaves
  Tree::nodeContainer allLeaves;
  _T->getAllLeaves(allLeaves);
  for (int i=0; i< allLeaves.size(); i++)
  {
    const TreeNode& leaf = *(allLeaves[i]);
    OfflineData& data = *(_offlineData[&leaf]);
    if (leaf.childOrder == 0)
    {
      if (_homo->withPlaneStrain())
      {
        fullMatrixOperation::reducePlaneStrain(C1,data.Cel);
      }
      else
      {
        fullMatrixOperation::reduceMat99ToMat66(C1,data.Cel);
      }
    }
    else if (leaf.childOrder == 1)
    {
      if (_homo->withPlaneStrain())
      {
        fullMatrixOperation::reducePlaneStrain(C2,data.Cel);
      }
      else
      {
        fullMatrixOperation::reduceMat99ToMat66(C2,data.Cel);
      }
    }
  };
  
  homogenizationProcess(true);
  const OfflineData& rootData = *(_offlineData[_T->getRootNode()]);
  Ceff = rootData.Cel;
};

void TrainingDeepMaterialNetwork::testHomogenizationProcess(fullMatrix<double>& C1, const fullMatrix<double>& C2, double Kn, double Kt, fullMatrix<double>& Ceff)
{
  fullVector<double> Wcur;
  numberDofs(Wcur);
  // set data to leaves
  Tree::nodeContainer allLeaves;
  _T->getAllLeaves(allLeaves);
  for (int i=0; i< allLeaves.size(); i++)
  {
    const TreeNode& leaf = *(allLeaves[i]);
    OfflineData& data = *(_offlineData[&leaf]);
    if (leaf.childOrder == 0)
    {
      if (_homo->withPlaneStrain())
      {
        fullMatrixOperation::reducePlaneStrain(C1,data.Cel);
      }
      else
      {
        fullMatrixOperation::reduceMat99ToMat66(C1,data.Cel);
      }
    }
    else if (leaf.childOrder == 1)
    {
      if (_homo->withPlaneStrain())
      {
        fullMatrixOperation::reducePlaneStrain(C2,data.Cel);
      }
      else
      {
        fullMatrixOperation::reduceMat99ToMat66(C2,data.Cel);
      }
    }
    
    if ((_cohesiveHomo != NULL) and (leaf.withCohesive()))
    {
      data.Qel.resize(2);
      data.Qel(0) = Kn;
      data.Qel(1) = Kt;
    }
  };
  homogenizationProcess(true);
  const OfflineData& rootData = *(_offlineData[_T->getRootNode()]);
  Ceff = rootData.Cel;
};

void TrainingDeepMaterialNetwork::computeErrorVector(std::vector<double>& allErors, const std::vector<LossMeasure*>& allLosses, 
      const std::vector<fullMatrix<double> >& Cin, const fullVector<double>& Qin,  const fullMatrix<double>& Ceff)
{
  int N = allLosses.size();
  if (N == 0)
  {
    allErors.clear();
    return;
  }
  
   Tree::nodeContainer allLeaves;
  _T->getAllLeaves(allLeaves);
  for (int i=0; i< allLeaves.size(); i++)
  {
    const TreeNode& leaf = *(allLeaves[i]);
    OfflineData& data = *(_offlineData[&leaf]);
    data.Cel =  Cin[leaf.matIndex];
    if ((_cohesiveHomo != NULL) and (leaf.withCohesive()))
    {
      data.Qel = Qin;
    }
  }
  //
  homogenizationProcess(false);
  //
  const OfflineData& rootData = *(_offlineData[_T->getRootNode()]);
  
  allErors.resize(N);
  for (int i=0; i< N; i++)
  {
    allErors[i] = allLosses[i]->get(Ceff,rootData.Cel);
  }
};

double TrainingDeepMaterialNetwork::computeError(const LossMeasure& loss, const std::vector<fullMatrix<double> >& Cin, const fullVector<double>& Qin,
                                 const fullMatrix<double>& Ceff, bool stiff, fullVector<double>* g)
{
  // data at leaves
  Tree::nodeContainer allLeaves;
  _T->getAllLeaves(allLeaves);
  for (int i=0; i< allLeaves.size(); i++)
  {
    const TreeNode& leaf = *(allLeaves[i]);
    OfflineData& data = *(_offlineData[&leaf]);
    data.Cel =  Cin[leaf.matIndex];
    if ((_cohesiveHomo != NULL) and (leaf.withCohesive()))
    {
      data.Qel = Qin;
    }
  };
  //
  homogenizationProcess(stiff);
  //
  const OfflineData& rootData = *(_offlineData[_T->getRootNode()]);
  
  if (stiff)
  {
    double Ndof = _unknown.size();
    if (g->size() != Ndof)
    {
      g->resize(Ndof,true);
    }
    else
    {
      g->setAll(0.);
    }
    
    static fullMatrix<double> DnormDDCel;
    double error = loss.get(Ceff,rootData.Cel,stiff,&DnormDDCel);
    for (std::map<Dof, fullMatrix<double> >::const_iterator itD = rootData.DCeffDunknown.begin(); itD != rootData.DCeffDunknown.end(); itD++)
    {
      std::map<Dof,int>::const_iterator itF = _unknown.find(itD->first);
      if (itF != _unknown.end())
      {
        (*g)(itF->second) = fullMatrixOperation::dot(DnormDDCel,itD->second); 
      }
      else
      {
        Msg::Error("Dof not found");
      }
    }
    
    return error;
  }
  else
  {
    return loss.get(Ceff,rootData.Cel);
  }
};

double TrainingDeepMaterialNetwork::computeCostFunction(const LossMeasure& loss, const std::vector<int>& sampleIndex, fullVector<double>& grad)
{
  grad.setAll(0.);
  static fullVector<double> gPart;
  int Ntrain = _XTrain.size();
  int batchSize = sampleIndex.size();
  
  double alpha = 1./(double)(batchSize);
  double err = 0;
  for (int i=0; i< batchSize; i++)
  {
    int sample = sampleIndex[i];
    if (sample > Ntrain-1)
    {
      Msg::Error("sample %d does not exist !!!",sample);
      Msg::Exit(0);
    }
    err += computeError(loss,_XTrain[sample], _QTrain[sample],_YTrain[sample],true,&gPart);
    grad.axpy(gPart,alpha);
  }
  err *= (alpha);
  
  if (fabs(_lambda) >0)
  {
    // penalty term 
    Tree::nodeContainer allLeaves;
    _T->getAllLeaves(allLeaves);
    double sumWeight = 0.;
    for (int i=0; i< allLeaves.size(); i++)
    {
      const TreeNode* ll = allLeaves[i];
      sumWeight += ll->af->getVal(ll->weight);
    }
    err += _lambda*(sumWeight-1.)*(sumWeight-1);
    static fullVector<double> gPenalty;
    if (gPenalty.size() != grad.size())
    {
      gPenalty.resize(grad.size(),true);
    }
    else
    {
      gPenalty.setAll(0.);
    }
    std::vector<Dof> R;
    for (int i=0; i< allLeaves.size(); i++)
    {
      const TreeNode* ll = allLeaves[i];
      R.clear();
      getKeys(ll,R);
      int row = getDofNumber(R[0]);
      gPenalty(row) = 2.*_lambda*(sumWeight-1.)*(ll->af->getDiff(ll->weight));
    }
    grad.axpy(gPenalty,1.);
  }
  return err;
};

double TrainingDeepMaterialNetwork::evaluateTrainingSet(const LossMeasure& loss)
{
  int Ntrain = _XTrain.size();
  double err = 0;
  for (int i=0; i< Ntrain; i++)
  {
    err += computeError(loss,_XTrain[i],_QTrain[i],_YTrain[i]);
  }
  err *= (1./(double)Ntrain);
  return err;
};


double TrainingDeepMaterialNetwork::evaluateTestSet(const LossMeasure& loss)
{
  int NTest = _XTest.size();
  double err = 0;
  for (int i=0; i< NTest; i++)
  {
    err += computeError(loss,_XTest[i],_QTest[i],_YTest[i]);
  }
  err *= (1./(double)NTest);
  return err;
};


int TrainingDeepMaterialNetwork::sizeTrainingSet() const
{
  return _XTrain.size();
}
// allow to estimate loss function and derivatives with respect to the fitting paramater
double TrainingDeepMaterialNetwork::computeCostFunction(const std::string& metric, const std::vector<int>& sampleIndex, fullVector<double>& grad)
{
  LossMeasure* loss = LossMeasure::createLossMeasure(metric.c_str());
  double val = computeCostFunction(*loss,sampleIndex,grad);
  //grad.print("gradient");
  delete loss;
  return val;
}
// loss function in whole training set
void TrainingDeepMaterialNetwork::evaluateTrainingSet(const std::vector<std::string>& metrics, std::vector<double>& vals)
{
  int N = metrics.size();
  if (N == 0)
  {
    vals.clear();
    return;
  }
  vals.resize(N);
  for (int i=0; i< N; i++)
  {
    vals[i] = 0.;
  }
  int Ntrain = _XTrain.size();
  if (Ntrain == 0)
  {
    return;
  }
  std::vector<LossMeasure*> allLosses(N,NULL);
  for (int i=0; i< N; i++)
  {
    allLosses[i] = LossMeasure::createLossMeasure(metrics[i].c_str());
  }
  
  for (int i=0; i< Ntrain; i++)
  {
    static std::vector<double> allErrors; 
    computeErrorVector(allErrors, allLosses,_XTrain[i],_QTrain[i],_YTrain[i]);
    for (int j=0; j< N; j++)
    {
      vals[j] += allErrors[j]/(double)Ntrain;
    }
  }
  for (int i=0; i< N; i++)
  {
    delete allLosses[i];
  };
};
// loss function in test set
void TrainingDeepMaterialNetwork::evaluateTestSet(const std::vector<std::string>& metrics, std::vector<double>& vals)
{
  int N = metrics.size();
  if (N == 0)
  {
    vals.clear();
    return;
  }
  vals.resize(N);
  for (int i=0; i< N; i++)
  {
    vals[i] = 0.;
  }
  int Ntest = _XTest.size();
  if (Ntest == 0)
  {
    return;
  }
  std::vector<LossMeasure*> allLosses(N,NULL);
  for (int i=0; i< N; i++)
  {
    allLosses[i] = LossMeasure::createLossMeasure(metrics[i].c_str());
  }
  
  for (int i=0; i< Ntest; i++)
  {
    static std::vector<double> allErrors; 
    computeErrorVector(allErrors, allLosses,_XTest[i],_QTest[i],_YTest[i]);
    for (int j=0; j< N; j++)
    {
      vals[j] += allErrors[j]/(double)Ntest;
    }
  }
  for (int i=0; i< N; i++)
  {
    delete allLosses[i];
  };
}
// save model
void TrainingDeepMaterialNetwork::saveModel(const std::string fname) const
{
  _T->saveDataToFile(fname);
}
// start numbering fitting parameters
void TrainingDeepMaterialNetwork::initializeFittingParameters(fullVector<double>& W)
{
  numberDofs(W);
};

void TrainingDeepMaterialNetwork::getUnknownBounds(fullVector<double>& WLower, fullVector<double>& WUpper) const
{
  // Dof of all nodes
  Tree::nodeContainer allNodes;
  _T->getAllNodes(allNodes);

  // initialize data from tree
  //
  int sizeOfR = _unknown.size();
  WLower.resize(sizeOfR,true);
  WUpper.resize(sizeOfR,true);
    //
  for (int i=0; i< allNodes.size(); i++)
  {
    const TreeNode& node = *(allNodes[i]);
    std::vector<Dof> R;
    getKeys(&node,R);
    int Rsize = R.size();
    if (node.childs.size() ==0)
    {
      int numCoh = 0; 
      int numCohDirVars = 0;
      if (node.withCohesive())
      {
        numCoh = node.cohesiveCoeffVars.size();
        numCohDirVars = node.cohesiveDirectionVars.size2();
      }
      for (int j=0; j< Rsize; j++)
      {
        std::map<Dof,int>::const_iterator itR =  _unknown.find(R[j]);
        if (itR != _unknown.end())
        {
          if (j == 0)
          {
            // leaf
            WLower(itR->second) =  node.af->getReciprocalVal(0.001);
            WUpper(itR->second) = node.af->getReciprocalVal(1);
          }
          else if ((j > 0) && (j< numCoh+1))
          {
            WLower(itR->second) = 0.1;
            WUpper(itR->second) = 0.9;
          }
          else if ((j >= numCoh+1) && (j < numCoh*numCohDirVars+numCoh+1))
          {
            int index = j -(numCoh+1);
            int k = index/numCohDirVars;
            int l = index%numCohDirVars;
            //Msg::Info("index = %d k=%d, l=%d",index,k,l);
            WLower(itR->second) = 0;
            WUpper(itR->second) = 1.;
          }
          else
          {
            Msg::Error("wrong position");
            Msg::Exit(0);
          }
        };
      }
    }
    else
    {
      for (int j=0; j< Rsize; j++)
      {
        std::map<Dof,int>::const_iterator itR =  _unknown.find(R[j]);
        if (itR != _unknown.end())
        {
          WLower(itR->second) = 0;
          WUpper(itR->second) = 1.;
        };
      }
    }
  };  
};


// update the model with the new fitting parameter
void TrainingDeepMaterialNetwork::updateModelFromFittingParameters(const fullVector<double>& W)
{
  updateFieldFromUnknown(W);
}
//  in case of fails
void TrainingDeepMaterialNetwork::reinitializeModel()
{
  _T->initialize();
}