//
// C++ Interface: reduction interface
//
// Author:   <Kevin Spilker>, (C) 2020, 
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef TFATOOLS_H_
#define TFATOOLS_H_


#include "STensorOperations.h"
#include "fullMatrix.h"
#include <cmath>

namespace TFANumericTools{

  inline void STensor3SymmetricFill(STensor3& tensor, double& val_xx, double& val_yy, double& val_zz, double& val_xy, double& val_xz, double& val_yz)
  {
    tensor(0,0) = val_xx;
    tensor(1,1) = val_yy;
    tensor(2,2) = val_zz;
    tensor(0,1) = val_xy;
    tensor(1,0) = val_xy;
    tensor(0,2) = val_xz;
    tensor(2,0) = val_xz;
    tensor(1,2) = val_yz;
    tensor(2,1) = val_yz;
  }
  
  inline void STensor3Normalize(const STensor3& tensor, STensor3& tensorNormalized)
  {
    double tensor_contract = 0.;
    for(int i=0; i<3; i++){
      for(int j=0; j<3; j++){
        tensor_contract += tensor(i,j)*tensor(i,j);
      }
    }
    for(int i=0; i<3; i++){
      for(int j=0; j<3; j++){
        tensorNormalized(i,j) = tensor(i,j)/tensor_contract;
      }
    } 
  }

  inline double EquivalentStrain(const STensor3& strainTensor)
  {
    STensor3 strainDev = strainTensor.dev();
    double strainDev_contract = 0.;
    for(int i=0; i<3; i++){
      for(int j=0; j<3; j++){
        strainDev_contract += strainDev(i,j)*strainDev(i,j);
      }
    }
    double strainEq = sqrt(2./3.*strainDev_contract);
    
    return strainEq;  
  }
  
  inline double Equivalent2DStrain(const STensor3& strainTensor)
  {
    STensor3 strainDev = strainTensor;
    STensor3 I(1.);
    double eps_mean = (strainTensor(0,0) + strainTensor(1,1))/2.;
    strainDev -= eps_mean*I;
    strainDev(0,2) = strainDev(1,2) = strainDev(2,0) = strainDev(2,1) = strainDev(2,2) = 0.;
    double strainDev_contract = 0.;
    for(int i=0; i<2; i++){
      for(int j=0; j<2; j++){
        strainDev_contract += strainDev(i,j)*strainDev(i,j);
      }
    }
    double strainEq = sqrt(2./3.*strainDev_contract);
    
    return strainEq;  
  }
  
  inline double EquivalentStress(const STensor3& stressTensor)
  {
    STensor3 stressDev = stressTensor.dev();
    double stressDev_contract = 0.;
    for(int i=0; i<3; i++){
      for(int j=0; j<3; j++){
        stressDev_contract += stressDev(i,j)*stressDev(i,j);
      }
    }
    double stressEq = sqrt(1.5*stressDev_contract);
    
    return stressEq;  
  }
  
  inline double Equivalent2DStress(const STensor3& stressTensor)
  {
    STensor3 stressDev = stressTensor;
    STensor3 I(1.);
    double p = (stressTensor(0,0) + stressTensor(1,1))/2.;
    stressDev -= p*I;
    stressDev(0,2) = stressDev(1,2) = stressDev(2,0) = stressDev(2,1) = stressDev(2,2) = 0.;
    double stressDev_contract = 0.;
    for(int i=0; i<2; i++){
      for(int j=0; j<2; j++){
        stressDev_contract += stressDev(i,j)*stressDev(i,j);
      }
    }
    double stressEq = sqrt(1.5*stressDev_contract);
    
    return stressEq;  
  }

  inline void StiffnessIsotropic(const double& G, const double& K, STensor43& IsotropicStiffness)
  {
    STensor43 dev_part;
    STensorOperation::sphericalfunction(IsotropicStiffness);
    STensorOperation::deviatorfunction(dev_part);
    IsotropicStiffness *= 3.;
    IsotropicStiffness *= K;
    dev_part *= 2.;
    dev_part *= G;
    IsotropicStiffness += dev_part;
  };


  inline void IsotropizationSTensor43(const STensor43& a, STensor43& a_iso){
    STensor43 I_vol, I_dev;
    STensorOperation::sphericalfunction(I_vol);
    STensorOperation::deviatorfunction(I_dev);

    double a_iso_vol_fac,a_iso_dev_fac;
    STensorOperation::contract4STensor43(I_vol,a,a_iso_vol_fac);
    STensorOperation::contract4STensor43(I_dev,a,a_iso_dev_fac);

    STensor43 a_iso_vol(I_vol);
    a_iso_vol *= a_iso_vol_fac;

    a_iso=I_dev;
    a_iso *= 1./5.*a_iso_dev_fac;
    a_iso += a_iso_vol;
  };


  inline void StiffnessIsotropicDerivative(const STensor63& StiffnessDerivative, STensor63& IsotropicStiffnessDerivative){
    STensor3 sphere_scalar_derivative, deviator_scalar_derivative;
    STensor43 I_vol, I_dev;
    STensorOperation::sphericalfunction(I_vol);
    STensorOperation::deviatorfunction(I_dev);
    STensor63 sphere_derivative;

    STensorOperation::contract4STensor43STensor63(I_vol,StiffnessDerivative,sphere_scalar_derivative);
    STensorOperation::contract4STensor43STensor63(I_dev,StiffnessDerivative,deviator_scalar_derivative);

    STensorOperation::TensorProdSTensor43STensor3(I_vol,sphere_scalar_derivative,sphere_derivative);
    STensorOperation::TensorProdSTensor43STensor3(I_dev,deviator_scalar_derivative,IsotropicStiffnessDerivative);
    IsotropicStiffnessDerivative *= 1./5.;
    IsotropicStiffnessDerivative += sphere_derivative;
  };
  
  
  inline void solveClosestTransverseIsotropicTensor(const int& p, const STensor3& deps, const STensor3& dsig, STensor43& C_TI)
  {
    // Norris, 2018; Sec. 4.4
    // define basis tensors
    STensor3 P, Q(1.), U, V, W, X, Y, Z;
    P(p,p) = 1.;
    Q -= P;
    Q *= 1./sqrt(2.);
    
    U(0,2) = U(2,0) = 1.;
    U *= 1./sqrt(2.);
    V(1,2) = V(2,1) = 1.;
    V *= 1./sqrt(2.);
    W(0,1) = W(1,0) = 1.;
    W *= 1./sqrt(2.);
    
    X(2,2) = 1.;
    X(0,0) = -1.;
    Y(1,1) = 1.;
    Y(2,2) = -1.;
    Z(0,0) = 1.;
    Z(1,1) = -1.;
    
    STensor43 E1, E2, E3, E4, F, G;  
    STensorOperation::prod(P,P,1.,E1);
    STensorOperation::prod(Q,Q,1.,E2);
    STensorOperation::prod(P,Q,1.,E3);
    STensorOperation::prod(Q,P,1.,E4);
    
    if(p==0)
    {
    STensorOperation::prod(V,V,1.,F);
    STensorOperation::prodAdd(Y,Y,1.,F);
    STensorOperation::prod(U,U,1.,G);
    STensorOperation::prodAdd(W,W,1.,G);
    }
    if(p==1)
    {
    STensorOperation::prod(U,U,1.,F);
    STensorOperation::prodAdd(X,X,1.,F);
    STensorOperation::prod(V,V,1.,G);
    STensorOperation::prodAdd(W,W,1.,G);
    }
    if(p==2)
    {
    STensorOperation::prod(W,W,1.,F);
    STensorOperation::prodAdd(Z,Z,1.,F);
    STensorOperation::prod(U,U,1.,G);
    STensorOperation::prodAdd(V,V,1.,G);
    }
    
    // unknown moduli
    fullVector<double> u(5);
    double a,b,c,f,g;
    
    // define system matrix
    fullMatrix<double> M(5,5);
    STensor3 E1deps, E2deps, E3_E4_deps, Fdeps, Gdeps;
    STensorOperation::multSTensor43STensor3(E1,deps,E1deps);
    STensorOperation::multSTensor43STensor3(E2,deps,E2deps);
    STensor43 E3_E4(E3);
    E3_E4 += E4;
    STensorOperation::multSTensor43STensor3(E3_E4,deps,E3_E4_deps);
    STensorOperation::multSTensor43STensor3(F,deps,Fdeps);
    STensorOperation::multSTensor43STensor3(G,deps,Gdeps);
    
    STensorOperation::doubleContractionSTensor3(E1deps,E1deps,M(0,0));
    STensorOperation::doubleContractionSTensor3(E1deps,E2deps,M(0,1));
    STensorOperation::doubleContractionSTensor3(E1deps,E3_E4_deps,M(0,2));
    STensorOperation::doubleContractionSTensor3(E1deps,Fdeps,M(0,3));
    STensorOperation::doubleContractionSTensor3(E1deps,Gdeps,M(0,4));
    
    STensorOperation::doubleContractionSTensor3(E2deps,E1deps,M(1,0));
    STensorOperation::doubleContractionSTensor3(E2deps,E2deps,M(1,1));
    STensorOperation::doubleContractionSTensor3(E2deps,E3_E4_deps,M(1,2));
    STensorOperation::doubleContractionSTensor3(E2deps,Fdeps,M(1,3));
    STensorOperation::doubleContractionSTensor3(E2deps,Gdeps,M(1,4));
    
    STensorOperation::doubleContractionSTensor3(E3_E4_deps,E1deps,M(2,0));
    STensorOperation::doubleContractionSTensor3(E3_E4_deps,E2deps,M(2,1));
    STensorOperation::doubleContractionSTensor3(E3_E4_deps,E3_E4_deps,M(2,2));
    STensorOperation::doubleContractionSTensor3(E3_E4_deps,Fdeps,M(2,3));
    STensorOperation::doubleContractionSTensor3(E3_E4_deps,Gdeps,M(2,4));
    
    STensorOperation::doubleContractionSTensor3(Fdeps,E1deps,M(3,0));
    STensorOperation::doubleContractionSTensor3(Fdeps,E2deps,M(3,1));
    STensorOperation::doubleContractionSTensor3(Fdeps,E3_E4_deps,M(3,2));
    STensorOperation::doubleContractionSTensor3(Fdeps,Fdeps,M(3,3));
    STensorOperation::doubleContractionSTensor3(Fdeps,Gdeps,M(3,4));
    
    STensorOperation::doubleContractionSTensor3(Gdeps,E1deps,M(4,0));
    STensorOperation::doubleContractionSTensor3(Gdeps,E2deps,M(4,1));
    STensorOperation::doubleContractionSTensor3(Gdeps,E3_E4_deps,M(4,2));
    STensorOperation::doubleContractionSTensor3(Gdeps,Fdeps,M(4,3));
    STensorOperation::doubleContractionSTensor3(Gdeps,Gdeps,M(4,4));
    for(int i=0; i<5; i++){
      for(int j=0; j<5; j++){
        M(i,j) *= 2.;
      }
    }
    
    // define RHS
    fullVector <double> RHS(5);
    double dsig_E1deps, E1deps_dsig, dsig_E2deps, E2deps_dsig, dsig_E3_E4_deps, E3_E4_deps_dsig, dsig_Fdeps, Fdeps_dsig, dsig_Gdeps, Gdeps_dsig;
    STensorOperation::doubleContractionSTensor3(dsig,E1deps,dsig_E1deps);
    STensorOperation::doubleContractionSTensor3(E1deps,dsig,E1deps_dsig);
    STensorOperation::doubleContractionSTensor3(dsig,E2deps,dsig_E2deps);
    STensorOperation::doubleContractionSTensor3(E2deps,dsig,E2deps_dsig);
    STensorOperation::doubleContractionSTensor3(dsig,E3_E4_deps,dsig_E3_E4_deps);
    STensorOperation::doubleContractionSTensor3(E3_E4_deps,dsig,E3_E4_deps_dsig);
    STensorOperation::doubleContractionSTensor3(dsig,Fdeps,dsig_Fdeps);
    STensorOperation::doubleContractionSTensor3(Fdeps,dsig,Fdeps_dsig);
    STensorOperation::doubleContractionSTensor3(dsig,Gdeps,dsig_Gdeps);
    STensorOperation::doubleContractionSTensor3(Gdeps,dsig,Gdeps_dsig);
    
    RHS(0) = dsig_E1deps + E1deps_dsig;
    RHS(1) = dsig_E2deps + E2deps_dsig;
    RHS(2) = dsig_E3_E4_deps + E3_E4_deps_dsig;
    RHS(3) = dsig_Fdeps + Fdeps_dsig;
    RHS(4) = dsig_Gdeps + Gdeps_dsig;
    
    M.print("M");
    //RHS.print("RHS");
    
    // solve for unknowns
    fullMatrix<double> Minv(5,5);
    bool isInverted = M.invert(Minv);
    if (!isInverted)
    {
      Msg::Error("M Cannot be inverted");
    }
    Minv.mult(RHS,u);
    a = u(0);
    b = u(1);
    c = u(2);
    f = u(3);
    g = u(4);
    
    // express closest transverse isotropic tensor
    C_TI = a*E1;
    C_TI += b*E2;
    C_TI += c*(E3+E4);
    C_TI += f*F;
    C_TI += g*G;  
  };

};

#endif  //TFATOOLS_H_
