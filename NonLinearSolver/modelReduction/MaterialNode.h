//
// C++ Interface: material Node
//
//
// Author:  <V.-D. Nguyen>, (C) 2021
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef _MATRIALNODE_H_
#define _MATRIALNODE_H_ 

#ifndef SWIG
#include "ANNUtils.h"
#include "ipstate.h"
#include "STensorOperations.h"
#endif //SWIG

#ifndef SWIG
class cohesiveIPState
{    
  public:
    cohesiveIPState(){};
    cohesiveIPState(const cohesiveIPState& src){};
    virtual ~cohesiveIPState(){};
    
    virtual void reset() = 0;
    virtual SVector3& getRefToJump(int index) = 0;
    virtual const SVector3& getConstRefToJump(int index) const = 0;
    virtual const SVector3& getConstRefToInterfaceForce(int index) const = 0;
    virtual const STensor3& getConstRefToDInterfaceForceDJump(int index) const = 0;
    
    virtual void nextStep() = 0;
    virtual void resetToPreviousStep() = 0;
    virtual void copy(const IPStateBase::whichState ws1, const IPStateBase::whichState ws2) = 0;
};
#endif //SWIG

class MaterialNode
{
  public:
    static int maxIndex;
    
  public:
    int numberIndex;
    int matIndex; // specify material number -1 by default
    double weight; 
     // for cohesive failure
    int cohesiveMatIndex;
    fullVector<double> cohesiveCoeffVars;  
    fullMatrix<double> cohesiveDirectionVars;  
    bool withCohesiveFlag;
    #ifndef SWIG
    activationFunction* af; // to make sure weight is posible when upscale 
    activationFunction* cohesiveAf; // to make sure weight is posible when upscale 
    IPStateBase* ips; // current and previous ipv
    cohesiveIPState* ipsCohesive; // incompatible disp vector
    #endif //SWIG
    
  public:
    MaterialNode();
    MaterialNode(const MaterialNode& src);
    virtual MaterialNode* clone() const {return new MaterialNode(*this);}
    virtual ~MaterialNode();
    virtual int getNodeId() const {return numberIndex;};
    virtual bool withCohesive() const  {return withCohesiveFlag;};
    void allocateCohesiveParameters(int numCoh, int numDirVars);
    #ifndef SWIG
    void nextStep();
    void resetToPreviousStep();
    void copy(const IPStateBase::whichState ws1, const IPStateBase::whichState ws2);
    
    virtual void saveToFile(FILE* f) const;
    virtual void saveToFileCohesive(FILE* f) const;
    //
    void standardizeCohesiveDirectionVars();
    void resetCohesiveJumps();
    void getNormal(std::vector<SVector3>& normal) const;
    void getInducedCohesiveStrain(std::vector<STensor3>& Fi) const;
    void getDInducedCohesiveStrainDjump(std::vector<STensor33>& DFiDa) const;
    #endif //SWIG
};

#endif //_MATRIALNODE_H_
