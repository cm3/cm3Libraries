//
// C++ Interface:  deep material networks
//
//
// Author:  <V.-D. Nguyen>, (C) 2021
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef _TRAININGARBITRARYDEEPMATERIALNETWORKS_H_
#define _TRAININGARBITRARYDEEPMATERIALNETWORKS_H_ 

#include "TrainingDeepMaterialNetworks.h"

class TrainingArbitraryDeepMaterialNetwork : public LossFunction
{
  #ifndef SWIG
  protected:
    NetworkInteraction* _interaction; // to be optimized
    CoefficientReduction* _coeffReduction;
    //
    std::map<Dof, int> _unknown;
    std::map<Dof, double> _fixedDof;
    std::map<Dof, DofAffineConstraint<double> > _linearConstraints;
    bool _fixedWeightsOfMaterialNodes; // true if all weights are fixed, false if all coefficient interactions are estimated from weights
    std::vector<int> _fixInteractionDirectionComp;
    //
    bool _fixedPhaseFraction;
    activationFunction* _activationFunction;
    std::map<const MaterialNode*, double> _nodeZValues;
    
    //
    bool _dofsAllComponentsNumbered;
    std::map<Dof,int> _weightDofs;
    std::map<Dof,int> _coefficientDofs;
    std::map<Dof,int> _normalDofs;
        
  #endif //SWIG  
  public:
    void fixedWeightsOfMaterialNodes(bool fl);
    void fixedPhaseFraction(bool fl, std::string af = "relu");
  
  #ifndef SWIG
    TrainingArbitraryDeepMaterialNetwork(NetworkInteraction& T, const CoefficientReduction& reduction);
    virtual ~TrainingArbitraryDeepMaterialNetwork();
   
  protected:
    void getMaterialNodeKeys(const MaterialNode* node,  std::vector<Dof>& keys) const;
    void getInteractionCoefficientsKeys(const InteractionMechanism* im, std::vector<Dof>& keys) const;
    void getInteractionDirectionKeys(const InteractionMechanism* im, std::vector<Dof>& keys) const;
    void numberDofsAllComponents();
    //
    void fixWeightsOfMaterialNodes(); // node weights are fixed
    void fixInteractionDirectionComp(int comp); //
    void numberDofs(fullVector<double>& ufield);
    double getDofValue(const fullVector<double>& unknownValues, const Dof& D) const;
    void updateFieldFromUnknown(const fullVector<double>& ufield);
    void updateUnknown(double lr, const fullVector<double>& g, fullVector<double>& Wcur);
  #endif //SWIG
};

class TrainingDeepMaterialNetworkLinearElastic : public TrainingArbitraryDeepMaterialNetwork
{
  protected:
    #ifndef SWIG
        // num phase
    std::vector<DeepMaterialNetwork* >_XTrain;
    std::vector<fullMatrix<double> > _YTrain;
    std::vector<DeepMaterialNetwork* >_XTest;
    std::vector<fullMatrix<double> > _YTest;
    #endif //SWIG
  public:
    TrainingDeepMaterialNetworkLinearElastic(NetworkInteraction& T, const CoefficientReduction& reduction);
    virtual ~TrainingDeepMaterialNetworkLinearElastic(){}
    //
    void trainingDataSize(int Ns);
    void setTrainingSample(int row, DeepMaterialNetwork& dmn, const fullMatrix<double>& Ceff);
    void testDataSize(int Ns);
    void setTestSample(int row, DeepMaterialNetwork& dmn, const fullMatrix<double>& Ceff);
    //
    #ifndef SWIG
    double computeCostFunction(const LossMeasure& loss, const std::vector<int>& sampleIndex, fullVector<double>& grad);
    double evaluateTrainingSet(const LossMeasure& loss);
    double evaluateTestSet(const LossMeasure& loss);
    
    
    void testHomogenizationProcess(DeepMaterialNetwork* C1, fullMatrix<double>& Ceff);
    void computeErrorVector(std::vector<double>& allErors, const std::vector<LossMeasure*>& allLosses, 
                         DeepMaterialNetwork* Cin, const fullMatrix<double>& Ceff);
    double computeError(const LossMeasure& loss, DeepMaterialNetwork* Cin, const fullMatrix<double>& Ceff,
                        bool stiff=false, fullVector<double>* g=NULL);
    
    bool homogenizationProcess(DeepMaterialNetwork* Cin,  fullMatrix<double>& Chomo, 
                              bool stiff=false, std::map<Dof, fullMatrix<double> >* DChomoDunknown=NULL);
                              
    #endif //SWIG
  public:
    virtual int sizeTrainingSet() const; 
    // allow to estimate loss function and derivatives with respect to the fitting paramater
    virtual double computeCostFunction(const std::string& metric, const std::vector<int>& sampleIndex, fullVector<double>& grad); 
    // loss function in whole training set
    virtual void evaluateTrainingSet(const std::vector<std::string>& metrics, std::vector<double>& vals);
    // loss function in test set
    virtual void evaluateTestSet(const std::vector<std::string>& metrics, std::vector<double>& vals);
    // save model
    virtual void saveModel(const std::string fname) const;
    // start numbering fitting parameters
    virtual void initializeFittingParameters(fullVector<double>& W); 
    // get bounds of fitting parmaters
    virtual void getUnknownBounds(fullVector<double>& WLower, fullVector<double>& WUpper) const;
    // update the model with the new fitting parameter
    virtual void updateModelFromFittingParameters(const fullVector<double>& W);
    //  in case of fails
    virtual void reinitializeModel();
};
#endif //_TRAININGARBITRARYDEEPMATERIALNETWORKS_H_