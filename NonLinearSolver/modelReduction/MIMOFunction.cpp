// Description: class of function with multiple input, multiple output
//
//
// Author:  <Van Dung NGUYEN>, (C) 2019
//
// Copyright: See COPYING file that comes with this distribution
//


#include "MIMOFunction.h"
#include "STensorOperations.h"

TrivialScaler::TrivialScaler(): Scaler(){}
TrivialScaler::TrivialScaler(const TrivialScaler& src) : Scaler(src){};

void TrivialScaler::transform(const fullMatrix<double>& XReal, fullMatrix<double>& Xnormed, 
                              bool stiff, fullMatrix<double>* DXnormedDXReal) const
{
  Xnormed = XReal;
  if (stiff)
  {
    if (DXnormedDXReal->size1() != Xnormed.size2() || DXnormedDXReal->size2() != XReal.size2())
    {
      DXnormedDXReal->resize(Xnormed.size2(), XReal.size2(), true);
    }
    else
    {
      DXnormedDXReal->setAll(0.);
    }
    for (int i=0; i< DXnormedDXReal->size1(); i++)
    {
      (*DXnormedDXReal)(i,i) = 1.;
    }
  }
}
void TrivialScaler::inverse_transform(const fullMatrix<double>& Xnormed, fullMatrix<double>& XReal,
                                      bool stiff, fullMatrix<double>* DXRealDXnormed) const
{
  XReal = Xnormed;
  if (stiff)
  {
    if (DXRealDXnormed->size1() != XReal.size2() || DXRealDXnormed->size2() != Xnormed.size2())
    {
      DXRealDXnormed->resize(XReal.size2(), Xnormed.size2(), true);
    }
    else
    {
      DXRealDXnormed->setAll(0.);
    }
    for (int i=0; i< DXRealDXnormed->size1(); i++)
    {
      (*DXRealDXnormed)(i,i) = 1.;
    }
  }
};
    
    
LinearMIMOFunction::LinearMIMOFunction(int numR, int numC): MIMOFunction(), _K(numR,numC){};
void LinearMIMOFunction::setValue(int i, int j, double val)
{
  if (i>= _K.size1() or j >= _K.size2())
  {
    Msg::Error("wrong input in LinearMIMOFunction::setValue");
    Msg::Exit(0);
  }
  _K(i,j) = val;
}

void LinearMIMOFunction::predict(const fullMatrix<double>& E1,
                         const fullMatrix<double>& E0,
                         const fullMatrix<double>& q0,
                         const fullMatrix<double>& S0,
                         fullMatrix<double>& q1,
                         fullMatrix<double>& S,
                         bool diff,
                         fullMatrix<double>* DSDE) const
{
  static fullMatrix<double> tmp;
  if (tmp.size1() != E1.size2() || tmp.size2() != 1)
  {
    tmp.resize(E1.size2(),1);
  }
  _K.mult(E1.transpose(),tmp);
  S = tmp.transpose();
  if (diff)
  {
    if (DSDE->size1()!= S.size2() || DSDE->size2()!=E1.size2())
    {
      DSDE->resize(S.size2(),E1.size2());
    }
    DSDE->setAll(_K);
    //DSDE->print("kkkkk");
  }
}

ANNMIMOFunction::ANNMIMOFunction(const ArtificialNN& a): MIMOFunction(), _locEVars(),_locSVars()
{
  _ann = a.clone();
  _scalerIn = new TrivialScaler();
  _scalerOut= new TrivialScaler();
};

ANNMIMOFunction::ANNMIMOFunction(const ANNMIMOFunction& src): MIMOFunction(src), _locEVars(src._locEVars),_locSVars(src._locSVars)
{
  if (src._ann != NULL)
  {
    _ann = src._ann->clone();
  }
  else
  {
    _ann = NULL;
  }
  if (src._scalerIn != NULL)
  {
    _scalerIn = src._scalerIn->clone();
  }
  else
  {
    _scalerIn = NULL;
  }
  if (src._scalerOut != NULL)
  {
    _scalerOut = src._scalerOut->clone();
  }
  else
  {
    _scalerOut = NULL;
  }
  
}
ANNMIMOFunction::~ANNMIMOFunction()
{
  if (_ann != NULL) delete _ann; _ann = NULL;
  if (_scalerIn != NULL) delete _scalerIn; _scalerIn = NULL;
  if (_scalerOut != NULL) delete _scalerOut; _scalerOut = NULL;
}

void ANNMIMOFunction::setNumberOfStrainVariables(int numEVar)
{
  _locEVars.resize(numEVar);
}
void ANNMIMOFunction::setNumberOfStressVariables(int numSVar)
{
  _locSVars.resize(numSVar);
}

void ANNMIMOFunction::setStrainLocation(int index, int loc)
{
  if (index < _locEVars.size())
  {
    _locEVars[index] = loc;
  }
  else
  {
    Msg::Error("index %d exceeds the size of the strain location vector",index);
  }
}

void ANNMIMOFunction::setStressLocation(int index, int loc)
{
  if (index < _locSVars.size())
  {
    _locSVars[index] = loc;
  }
  else
  {
    Msg::Error("index %d exceeds the size of the stress location vector",index);
  }
}

void ANNMIMOFunction::setScalerInputs(const Scaler& s)
{
  if (_scalerIn != NULL) delete _scalerIn;
  _scalerIn = s.clone();
}
void ANNMIMOFunction::setScalerOutputs(const Scaler& s)
{
  if (_scalerOut != NULL) delete _scalerOut;
  _scalerOut = s.clone();
}


void ANNMIMOFunction::predict(const fullMatrix<double>& E1,
                         const fullMatrix<double>& E0,
                         const fullMatrix<double>& q0,
                         const fullMatrix<double>& S0,
                         fullMatrix<double>& q1,
                         fullMatrix<double>& S,
                         bool diff,
                         fullMatrix<double>* DSDE) const
{
  auto allocateFullMatrix = [](fullMatrix<double>& a, int newRow, int newCol)
  {
    if (a.size1() != newRow || a.size2() != newCol)
    {
      a.resize(newRow,newCol, true);
    }
    else
    {
      a.setAll(0.);
    }
  };
  
  /**for stress **/
  static fullMatrix<double> inputSig, normedInputSig, DnormedInputSig_DinputSig, outputSig, normedOutputSig, DoutputSig_DnormedOutputSig, DnormedOutputSig_DnormedInputSig;
  int nbInputsSig = _ann->getNumInputs();
  int nbOutputsSig = _ann->getNumOutputs();
  int ESize = _locEVars.size();
  int SSize = _locSVars.size();
  if (nbOutputsSig != SSize || nbInputsSig != ESize)
  {
    Msg::Error("number of variables is not compatible between material law and stress ANN");
    Msg::Exit(0);
  };
  allocateFullMatrix(inputSig,1,nbInputsSig);
  allocateFullMatrix(normedInputSig,1,nbInputsSig);
  allocateFullMatrix(outputSig,1,nbOutputsSig);
  allocateFullMatrix(normedOutputSig,1,nbOutputsSig);
  // previous state
  if (diff)
  {
    allocateFullMatrix(DnormedInputSig_DinputSig,nbInputsSig,nbInputsSig);
    allocateFullMatrix(DoutputSig_DnormedOutputSig,nbOutputsSig,nbOutputsSig);
    allocateFullMatrix(DnormedOutputSig_DnormedInputSig,nbOutputsSig,nbInputsSig);
  };
  // input E
  for (int i=0; i < ESize; i++)
  {
    int locE = _locEVars[i];
    inputSig(0,i) = E1(0,locE); // current strain
  };
  _scalerIn->transform(inputSig,normedInputSig,diff,&DnormedInputSig_DinputSig);
  _ann->predict(normedInputSig,normedOutputSig,diff,&DnormedOutputSig_DnormedInputSig);
  _scalerOut->inverse_transform(normedOutputSig,outputSig,diff,&DoutputSig_DnormedOutputSig);
  
  S = S0;
  for (int i=0; i< SSize; i++)
  {
    int locS = _locSVars[i];
    S(0,locS) = outputSig(0,i);
  };
  
  if (diff)
  {
    auto mult3Mat = [&allocateFullMatrix](const fullMatrix<double>& A, const fullMatrix<double>& B, const fullMatrix<double>& C, fullMatrix<double>& ABC)
    {
      static fullMatrix<double> AB;
      if (AB.size1() != A.size1() || AB.size2() != B.size2())
      {
        allocateFullMatrix(AB,A.size1(),B.size2());
      }
      A.mult(B,AB);
      if (ABC.size1() != A.size1() || ABC.size2() != C.size2())
      {
        allocateFullMatrix(ABC,A.size1(),C.size2());
      }
      AB.mult(C, ABC);
    };
    static fullMatrix<double> DoutputSig_DE;
    allocateFullMatrix(DoutputSig_DE,SSize,ESize);
    mult3Mat(DoutputSig_DnormedOutputSig,DnormedOutputSig_DnormedInputSig,DnormedInputSig_DinputSig,DoutputSig_DE);
    allocateFullMatrix(*DSDE,6,6);
    for (int i=0; i< SSize; i++)
    {
      int locS = _locSVars[i];
      for (int j=0; j< ESize; j++)
      {
        int locE = _locEVars[j];
        (*DSDE)(locS,locE) = DoutputSig_DE(i,j);
      }
    }
    
  }
};


TwoANNMIMOFunction::TwoANNMIMOFunction(const ArtificialNN& sig, const ArtificialNN& q, double tol): MIMOFunction(),_locEVars(), _locSVars(),
      _tol(tol), _blockInternalState(false), _internalANNWithStress(false), _internalANNWithCurrentStrain(false)
{
  _annSig = sig.clone();
  _annQ = q.clone();
  _sigScalerIn = new TrivialScaler();
  _sigScalerOut = new TrivialScaler();
  _qScalerIn = new TrivialScaler();
  _qScalerOut = new TrivialScaler();
};


TwoANNMIMOFunction::TwoANNMIMOFunction(const TwoANNMIMOFunction& src): MIMOFunction(src), _locEVars(src._locEVars), _locSVars(src._locSVars), 
  _tol(src._tol), _blockInternalState(src._blockInternalState), _internalANNWithStress(src._internalANNWithStress), 
  _internalANNWithCurrentStrain(src._internalANNWithCurrentStrain)
{
  if (src._annSig != NULL)
  {
    _annSig = src._annSig->clone();
  }
  else
  {
    _annSig = NULL;
  }
  if (src._annQ != NULL)
  {
    _annQ = src._annQ->clone();
  }
  else
  {
    _annQ = NULL;
  }
  if (src._sigScalerIn != NULL)
  {
    _sigScalerIn = src._sigScalerIn->clone();
  }
  else
  {
    _sigScalerIn = NULL;
  }
  if (src._sigScalerOut != NULL)
  {
    _sigScalerOut = src._sigScalerOut->clone();
  }
  else
  {
    _sigScalerOut = NULL;
  }
  if (src._qScalerIn != NULL)
  {
    _qScalerIn = src._qScalerIn->clone();
  }
  else
  {
    _qScalerIn = NULL;
  }
  if (src._qScalerOut != NULL)
  {
    _qScalerOut = src._qScalerOut->clone();
  }
  else
  {
    _qScalerOut = NULL;
  }  
};

TwoANNMIMOFunction::~TwoANNMIMOFunction()
{
  if (_annSig) delete _annSig; _annSig = NULL;
  if (_annQ) delete _annQ; _annQ = NULL;
  if (_sigScalerIn) delete _sigScalerIn; _sigScalerIn = NULL;
  if (_sigScalerOut) delete _sigScalerOut; _sigScalerOut = NULL;
  if (_qScalerIn) delete _qScalerIn; _qScalerIn = NULL;
  if (_qScalerOut) delete _qScalerOut; _qScalerOut = NULL;
};

void TwoANNMIMOFunction::useInternalANNWithCurrentStrain(bool fl)
{
  _internalANNWithCurrentStrain = fl;
  if (fl)
  {
    Msg::Info("internal ann is used with current strain");
  }
}

void TwoANNMIMOFunction::useInternalANNWithStress(bool fl)
{
  _internalANNWithStress = fl;
  if (fl)
  {
    Msg::Info("internal ann is used with stress");
  }
}

void TwoANNMIMOFunction::blockInternalState(bool fl)
{
  _blockInternalState = fl;
  if (fl)
  {
    Msg::Info("internal state is blocked as its initial state");
  }
}

void TwoANNMIMOFunction::setNumberOfStrainVariables(int numEVar)
{
  _locEVars.resize(numEVar);
}
void TwoANNMIMOFunction::setNumberOfStressVariables(int numSVar)
{
  _locSVars.resize(numSVar);
}

void TwoANNMIMOFunction::setStrainLocation(int index, int loc)
{
  if (index < _locEVars.size())
  {
    _locEVars[index] = loc;
  }
  else
  {
    Msg::Error("index %d exceeds the size of the strain location vector",index);
  }
}

void TwoANNMIMOFunction::setStressLocation(int index, int loc)
{
  if (index < _locSVars.size())
  {
    _locSVars[index] = loc;
  }
  else
  {
    Msg::Error("index %d exceeds the size of the stress location vector",index);
  }
}

void TwoANNMIMOFunction::setScalerInputs(int annIndex, const Scaler& s)
{
  if (annIndex == 0)
  {
    if (_sigScalerIn != NULL) delete _sigScalerIn;
    _sigScalerIn = s.clone();
  }
  else if (annIndex == 1)
  {
    if (_qScalerIn != NULL) delete _qScalerIn;
    _qScalerIn = s.clone();
  }
  else
  {
    Msg::Error("annIndex %d does not exist!",annIndex);
  }
}
void TwoANNMIMOFunction::setScalerOutputs(int annIndex, const Scaler& s)
{
  if (annIndex == 0)
  {
    if (_sigScalerOut != NULL) delete _sigScalerOut;
    _sigScalerOut = s.clone();
  }
  else if (annIndex == 1)
  {
    if (_qScalerOut != NULL) delete _qScalerOut;
    _qScalerOut = s.clone();
  }
  else
  {
    Msg::Error("annIndex %d does not exist!",annIndex);
  }
}


void TwoANNMIMOFunction::predict(const fullMatrix<double>& E1,
                         const fullMatrix<double>& E0,
                         const fullMatrix<double>& q0,
                         const fullMatrix<double>& S0,
                         fullMatrix<double>& q1,
                         fullMatrix<double>& S,
                         bool diff,
                         fullMatrix<double>* DSDE) const
{
  auto allocateFullMatrix = [](fullMatrix<double>& a, int newRow, int newCol)
  {
    if (a.size1() != newRow || a.size2() != newCol)
    {
      a.resize(newRow,newCol, true);
    }
    else
    {
      a.setAll(0.);
    }
  };
    
  // loading step
  double stepE = sqrt((E1(0,0)-E0(0,0))*(E1(0,0)-E0(0,0))+
                 (E1(0,1)-E0(0,1))*(E1(0,1)-E0(0,1))+
                 (E1(0,2)-E0(0,2))*(E1(0,2)-E0(0,2))+
              2.*(E1(0,3)-E0(0,3))*(E1(0,3)-E0(0,3))+
              2.*(E1(0,4)-E0(0,4))*(E1(0,4)-E0(0,4))+
              2.*(E1(0,5)-E0(0,5))*(E1(0,5)-E0(0,5) ));
  //
  int nbInputsQ = _annQ->getNumInputs();
  int nbOutputsQ = _annQ->getNumOutputs();
  int ESize = _locEVars.size();
  int SSize = _locSVars.size();
  int QSize = q0.size2();
  
  if (_internalANNWithStress)
  {
    if (nbOutputsQ != QSize || nbInputsQ != QSize+2*ESize+SSize)
    {
      Msg::Error("number of variables is not compatible between material law and internal ANN");
      Msg::Exit(0);
    };
  }
  else
  {  
    if (nbOutputsQ != QSize || nbInputsQ != QSize+2*ESize)
    {
      Msg::Error("number of variables is not compatible between material law and internal ANN");
      Msg::Exit(0);
    };
  }
  
  /**for internal variables **/
  static fullMatrix<double> inputQ, normedInputQ, DnormedInputQ_DinputQ, outputQ, normedOutputQ, DoutputQ_DnormedOutputQ, DnormedOutputQ_DnormedInputQ;
  //
  bool withInternalState = true;
  if (stepE < _tol ||_blockInternalState)
  {
    withInternalState = false;
  }
  if (withInternalState)
  {
    allocateFullMatrix(inputQ,1,nbInputsQ);
    allocateFullMatrix(normedInputQ,1,nbInputsQ);
    allocateFullMatrix(outputQ,1,nbOutputsQ);
    allocateFullMatrix(normedOutputQ,1,nbOutputsQ);
    if (diff)
    {
      allocateFullMatrix(DnormedInputQ_DinputQ,nbInputsQ,nbInputsQ);
      allocateFullMatrix(DoutputQ_DnormedOutputQ,nbOutputsQ,nbOutputsQ);
      allocateFullMatrix(DnormedOutputQ_DnormedInputQ,nbOutputsQ,nbInputsQ);
    }
    // input DE
    for (int i=0; i < ESize; i++)
    {
      int locE = _locEVars[i];
      inputQ(0,i)  =  (E1(0,locE)-E0(0,locE))/stepE; // direction
      if (_internalANNWithCurrentStrain)
      {
        inputQ(0,i+ESize) = E1(0,locE); // current strain
      }
      else
      {
        inputQ(0,i+ESize) = E0(0,locE); // previous strain
      }
    };
    for (int i=0; i< QSize; i++)
    {
      inputQ(0,2*ESize+i) = q0(0,i);
    };
    if (_internalANNWithStress)
    {
      for (int i=0; i< SSize; i++)
      {
        int locS = _locSVars[i];
        inputQ(0,2*ESize+QSize+i) = S0(0,i);
      };
    };
    // estimation the internal ann
    _qScalerIn->transform(inputQ,normedInputQ,diff,&DnormedInputQ_DinputQ);
    _annQ->predict(normedInputQ,normedOutputQ,diff,&DnormedOutputQ_DnormedInputQ);
    _qScalerOut->inverse_transform(normedOutputQ,outputQ,diff,&DoutputQ_DnormedOutputQ);
    
    // output
    q1 = q0; // start from q0
    q1.axpy(outputQ,stepE);//
  }
  else
  {
    q1 = q0;
  }

  //
  //
  /**for stress **/
  static fullMatrix<double> inputSig, normedInputSig, DnormedInputSig_DinputSig, outputSig, normedOutputSig, DoutputSig_DnormedOutputSig, DnormedOutputSig_DnormedInputSig;
  static fullMatrix<double> inputSigPrev, normedInputSigPrev, outputSigPrev, normedOutputSigPrev;
  //
  // to avoid the artificial stress jump
  // S = S0 + ann_sig(E1,q1) - ann_sig(E0,q0)
  int nbInputsSig = _annSig->getNumInputs();
  int nbOutputsSig = _annSig->getNumOutputs();
  if (nbOutputsSig != SSize || nbInputsSig != QSize+ESize)
  {
    Msg::Error("number of variables is not compatible between material law and stress ANN");
    Msg::Exit(0);
  };
  allocateFullMatrix(inputSig,1,nbInputsSig);
  allocateFullMatrix(normedInputSig,1,nbInputsSig);
  allocateFullMatrix(outputSig,1,nbOutputsSig);
  allocateFullMatrix(normedOutputSig,1,nbOutputsSig);
  // previous state
  allocateFullMatrix(inputSigPrev,1,nbInputsSig);
  allocateFullMatrix(normedInputSigPrev,1,nbInputsSig);
  allocateFullMatrix(outputSigPrev,1,nbOutputsSig);
  allocateFullMatrix(normedOutputSigPrev,1,nbOutputsSig);
  if (diff)
  {
    allocateFullMatrix(DnormedInputSig_DinputSig,nbInputsSig,nbInputsSig);
    allocateFullMatrix(DoutputSig_DnormedOutputSig,nbOutputsSig,nbOutputsSig);
    allocateFullMatrix(DnormedOutputSig_DnormedInputSig,nbOutputsSig,nbInputsSig);
  };
  // input E
  for (int i=0; i < ESize; i++)
  {
    int locE = _locEVars[i];
    inputSig(0,i) = E1(0,locE); // current strain
    inputSigPrev(0,i) = E0(0,locE); // previous strain
  };
  for (int i=0; i< QSize; i++)
  {
    inputSig(0,ESize+i) = q1(0,i); // current internal state
    inputSigPrev(0,ESize+i) = q0(0,i); // previous internal state
  };
  _sigScalerIn->transform(inputSig,normedInputSig,diff,&DnormedInputSig_DinputSig);
  _annSig->predict(normedInputSig,normedOutputSig,diff,&DnormedOutputSig_DnormedInputSig);
  _sigScalerOut->inverse_transform(normedOutputSig,outputSig,diff,&DoutputSig_DnormedOutputSig);
  // previous
  _sigScalerIn->transform(inputSigPrev,normedInputSigPrev);
  _annSig->predict(normedInputSigPrev,normedOutputSigPrev);
  _sigScalerOut->inverse_transform(normedOutputSigPrev,outputSigPrev);
  
  S = S0;
  for (int i=0; i< SSize; i++)
  {
    int locS = _locSVars[i];
    S(0,locS) += outputSig(0,i)- outputSigPrev(0,i);
  };
  // for stiffness estimation
  if (diff)
  {
    auto mult3Mat = [&allocateFullMatrix](const fullMatrix<double>& A, const fullMatrix<double>& B, const fullMatrix<double>& C, fullMatrix<double>& ABC)
    {
      static fullMatrix<double> AB;
      allocateFullMatrix(AB,A.size1(),B.size2());
      A.mult(B,AB);
      allocateFullMatrix(ABC,A.size1(),C.size2());
      AB.mult(C, ABC);
    };
    
    auto prodMatrixAdd = [](const fullMatrix<double>& a, const fullMatrix<double>& b, double fact, fullMatrix<double>& prod_ab)
    {
      for (int i=0; i< a.size2(); i++)
      {
        for (int j=0; j< b.size2(); j++)
        {
          prod_ab(i,j) += fact*a(0,i)*b(0,j);
        }
      }
    };
    
    //compute dq1 dE
    // from N = (E1 - E0)/step
    static fullMatrix<double> Dq1_DE;
    allocateFullMatrix(Dq1_DE,nbOutputsQ,ESize);
    if (withInternalState)
    {
      static fullMatrix<double> DoutputQ_DinputQ, DDoutputQ_DN;
      allocateFullMatrix(DoutputQ_DinputQ,nbOutputsQ,nbInputsQ);
      allocateFullMatrix(DDoutputQ_DN,nbOutputsQ,ESize);
      mult3Mat(DoutputQ_DnormedOutputQ, DnormedOutputQ_DnormedInputQ, DnormedInputQ_DinputQ,DoutputQ_DinputQ);
            
      for (int irow = 0; irow <nbOutputsQ; irow ++)
      {
        for (int icol =0; icol < ESize; icol ++)
        {
          DDoutputQ_DN(irow, icol) = DoutputQ_DinputQ(irow,icol);
        }
      }
            
      static fullMatrix<double> DstepE_DE;
      allocateFullMatrix(DstepE_DE,1,ESize);
      for (int ie =0; ie < ESize; ie++)
      {
        int locE = _locEVars[ie];
        if (locE > 2)
        {
          DstepE_DE(0,ie) = 2*(E1(0,locE) - E0(0,locE))/stepE;
        }
        else
        {
          DstepE_DE(0,ie) = (E1(0,locE) - E0(0,locE))/stepE;
        }
      }
      
      static fullMatrix<double> N, DNDE; // N = (E1-E0)/stepE
      allocateFullMatrix(N,1,ESize);
      allocateFullMatrix(DNDE,ESize,ESize);
      for (int ie =0; ie < ESize; ie++)
      {
        DNDE(ie,ie) = 1./stepE;
        int locE = _locEVars[ie];
        N(0,ie) = (E1(0,locE) - E0(0,locE))/stepE;
      }
      prodMatrixAdd(N,DstepE_DE,-1./(stepE),DNDE);
      DDoutputQ_DN.mult(DNDE,Dq1_DE);
      Dq1_DE.scale(stepE);
      prodMatrixAdd(outputQ,DstepE_DE,1.,Dq1_DE);
    }
    //
    static fullMatrix<double> DoutputSig_DinputSig, DinputSig_DE;
    allocateFullMatrix(DoutputSig_DinputSig,nbOutputsSig,nbInputsSig);
    allocateFullMatrix(DinputSig_DE,nbInputsSig,ESize);
    mult3Mat(DoutputSig_DnormedOutputSig,DnormedOutputSig_DnormedInputSig,DnormedInputSig_DinputSig,DoutputSig_DinputSig);
    for (int j=0; j< ESize; j++)
    {
      DinputSig_DE(j,j) = 1.;
      for (int i=0; i< QSize; i++)
      {
        DinputSig_DE(i+ESize,j) = Dq1_DE(i,j);
      }
    }
    
    static fullMatrix<double> DoutputSig_DE;
    allocateFullMatrix(DoutputSig_DE,SSize,ESize);
    
    DoutputSig_DinputSig.mult(DinputSig_DE,DoutputSig_DE);
    allocateFullMatrix(*DSDE,6,6);
    for (int i=0; i< SSize; i++)
    {
      int locS = _locSVars[i];
      for (int j=0; j< ESize; j++)
      {
        int locE = _locEVars[j];
        (*DSDE)(locS,locE) = DoutputSig_DE(i,j);
      }
    }
  }
};
