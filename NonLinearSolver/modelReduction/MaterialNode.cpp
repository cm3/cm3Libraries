//
// C++ Interface: material Node
//
//
// Author:  <V.-D. Nguyen>, (C) 2021
//
// Copyright: See COPYING file that comes with this distribution
//
//

#include "MaterialNode.h"
#include "NetworkInteraction.h"

int MaterialNode::maxIndex = 0;
MaterialNode::MaterialNode(): numberIndex(0), matIndex(-1), weight(0.), af(NULL), ips(NULL), 
        cohesiveCoeffVars(),cohesiveDirectionVars(), cohesiveMatIndex(-1), withCohesiveFlag(false), cohesiveAf(NULL),
        ipsCohesive(NULL)
{
  numberIndex = MaterialNode::maxIndex;
  MaterialNode::maxIndex ++;
};
MaterialNode::MaterialNode(const MaterialNode& src): numberIndex(src.numberIndex), matIndex(src.matIndex),weight(src.weight),
af(NULL),ips(NULL), ipsCohesive(NULL), cohesiveCoeffVars(src.cohesiveCoeffVars),cohesiveDirectionVars(src.cohesiveDirectionVars), 
cohesiveMatIndex(src.cohesiveMatIndex),
withCohesiveFlag(src.withCohesiveFlag), cohesiveAf(NULL)
{
  if (src.af != NULL)
  {
    af = src.af->clone();
  }
  if (src.cohesiveAf != NULL)
  {
    cohesiveAf = src.cohesiveAf->clone();
  }
  if (src.ips != NULL)
  {
    Msg::Error("cannot copy material node when ips is set MaterialNode::MaterialNode(MaterialNode)");
    Msg::Exit(0);
  }
  
  if (src.ipsCohesive != NULL)
  {
    Msg::Error("cannot copy material node when ipsCohesive is set MaterialNode::MaterialNode(MaterialNode)");
    Msg::Exit(0);
  }
};

void MaterialNode::allocateCohesiveParameters(int numCoh, int numDirVars)
{
  if (numCoh > 0 && numDirVars > 0)
  {
    withCohesiveFlag = true;
    cohesiveCoeffVars.resize(numCoh,true);
    cohesiveDirectionVars.resize(numCoh,numDirVars,true);
  }
  else
  {
    withCohesiveFlag = false;
  }
};

MaterialNode::~MaterialNode()
{
  if (af) delete af;
  af = NULL;
  if (cohesiveAf) delete cohesiveAf;
  cohesiveAf = NULL;
  if (ips) delete ips;
  ips = NULL;
  if (ipsCohesive) delete ipsCohesive;
  ipsCohesive = NULL;
};

void MaterialNode::nextStep()
{
  if (ips != NULL)
  {
    IPVariable* ipvprev = ips->getState(IPStateBase::previous);
    IPVariable* ipvcur = ips->getState(IPStateBase::current);
    ipvprev->operator =(*ipvcur);
  }
  if (withCohesiveFlag)
  {
    ipsCohesive->nextStep();
  }
};
void MaterialNode::resetToPreviousStep()
{
  if (ips != NULL)
  {
    IPVariable* ipvprev = ips->getState(IPStateBase::previous);
    IPVariable* ipvcur = ips->getState(IPStateBase::current);
    ipvcur->operator =(*ipvprev);
  }
  if (withCohesiveFlag)
  {
     ipsCohesive->resetToPreviousStep();
  }
};

void MaterialNode::copy(const IPStateBase::whichState ws1, const IPStateBase::whichState ws2)
{
  if (ips != NULL)
  {
    IPVariable* ipv1 = ips->getState(ws1);
    IPVariable* ipv2 = ips->getState(ws2);
    ipv2->operator =(*ipv1);
  }  
  if (withCohesiveFlag)
  {
     ipsCohesive->copy(ws1, ws2);
  }
};

void MaterialNode::saveToFile(FILE* f) const
{
  // index - mat index - weight - activation function
  fprintf(f,"%d %d %.16g %s",numberIndex,matIndex,weight,af->getName().c_str());
  fflush(f);
};

void MaterialNode::saveToFileCohesive(FILE* f) const
{
  // woll be consider
  if (withCohesiveFlag)
  {
    int nCoh = cohesiveCoeffVars.size();
    int nDirVars = cohesiveDirectionVars.size2();
    fprintf(f,"%d %d %d %d",numberIndex,cohesiveMatIndex,nCoh, nDirVars);
    for (int i=0; i< nCoh; i++)
    {
      fprintf(f," %.16g",cohesiveCoeffVars(i));
    }
    for (int i=0; i< nCoh; i++)
    {
      for (int j=0; j< nDirVars; j++)
      {
        fprintf(f," %.16g",cohesiveDirectionVars(i,j));
      };
    }
    fprintf(f," %s\n",cohesiveAf->getName().c_str());
    fflush(f);
  }
};

void MaterialNode::standardizeCohesiveDirectionVars()
{
  if (withCohesiveFlag)
  {
    int nCoh = cohesiveCoeffVars.size();
    int nDirVars = cohesiveDirectionVars.size2();
    for (int i=0; i< nCoh; i++)
    {
      if (cohesiveCoeffVars(i) < 0.)
      {
        cohesiveCoeffVars(i) *= -1.;
        if (nDirVars == 1)
        {
          cohesiveDirectionVars(i,0) += 0.5;
        }
        else
        {
          Msg::Error("case is not defined");
          Msg::Exit(0);
        }
      }
      
      std::vector<double> vars(nDirVars);
      for (int j=0; j< nDirVars; j++)
      {
        vars[j] = cohesiveDirectionVars(i,j);
      }
      InteractionMechanism::standardizeDirectionVariables(vars);
      for (int j=0; j< nDirVars; j++)
      {
        cohesiveDirectionVars(i,j) = vars[j];
      }
    }
  };
};


void MaterialNode::resetCohesiveJumps()
{
  if (withCohesiveFlag)
  {
    ipsCohesive->reset();
  }
};

void MaterialNode::getNormal(std::vector<SVector3>& normal) const
{
  if (withCohesiveFlag)
  {
    int numCoh = cohesiveCoeffVars.size();
    normal.resize(numCoh);
    static std::vector<double> dirVars;
    for (int j=0; j< numCoh; j++)
    {
      dirVars.resize(cohesiveDirectionVars.size2());
      for (int i=0; i< cohesiveDirectionVars.size2(); i++)
      {
        dirVars[i] = cohesiveDirectionVars(j,i);
      }
      InteractionMechanism::getNormalVectorFromVars(dirVars,normal[j]);
    }
  }
};

void MaterialNode::getInducedCohesiveStrain(std::vector<STensor3>& Fi) const
{
  if (withCohesiveFlag)
  {
    int numCoh = cohesiveCoeffVars.size();
    Fi.resize(numCoh);
    static SVector3 direction;
    static std::vector<double> dirVars;
    for (int k=0; k< numCoh; k++)
    {
      dirVars.resize(cohesiveDirectionVars.size2());
      for (int i=0; i< cohesiveDirectionVars.size2(); i++)
      {
        dirVars[i] = cohesiveDirectionVars(k,i);
      }
      InteractionMechanism::getNormalVectorFromVars(dirVars,direction);
      double fact = cohesiveAf->getVal(cohesiveCoeffVars(k));
      const SVector3& jumpCur = ipsCohesive->getConstRefToJump(k);
      
      for (int i=0; i<3; i++)
      {
        for (int j=0; j<3; j++)
        {
          Fi[k](i,j) = fact*jumpCur(i)*direction(j);
        }
      }
    }
  }
}
void MaterialNode::getDInducedCohesiveStrainDjump(std::vector<STensor33>& DFiDa) const
{
  if (withCohesiveFlag)
  {
    int numCoh = cohesiveCoeffVars.size();
    DFiDa.resize(numCoh);
    static SVector3 direction;
    static std::vector<double> dirVars;
    for (int k=0; k< numCoh; k++)
    {
      dirVars.resize(cohesiveDirectionVars.size2());
      for (int i=0; i< cohesiveDirectionVars.size2(); i++)
      {
        dirVars[i] = cohesiveDirectionVars(k,i);
      }
      InteractionMechanism::getNormalVectorFromVars(dirVars,direction);
      static STensor3 I(1.);
      double fact = cohesiveAf->getVal(cohesiveCoeffVars(k));
      for (int i=0; i<3; i++)
      {
        for (int j=0; j<3; j++)
        {
          for (int t=0; t< 3; t++)
          {
            DFiDa[k](i,j,t) = fact*I(i,t)*direction(j);
          }
        }
      }
    }
  }
};