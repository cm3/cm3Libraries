//
// C++ Interface: network interactions in deep material networks
//
//
// Author:  <V.-D. Nguyen>, (C) 2021
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef _NETWORKINTERACTION_H_
#define _NETWORKINTERACTION_H_ 
#ifndef SWIG
#include <map>
#include <set>
#include <vector>
#include "SVector3.h"
#include "ipstate.h"
#endif //SWIG
#include "Tree.h"
#include "STensorOperations.h"

class MaterialNode;
class InteractionMechanism
{
  public:
    static int maximumIndex;
    static void getNormalVectorFromVars(const std::vector<double>& vars, SVector3& vec, bool stiff=false, std::vector<SVector3>* DvecDdirectionVars=NULL);
    static void standardizeDirectionVariables(std::vector<double>& vars);
  protected:
    #ifndef SWIG
    std::map<int,double> coefficients; // interaction coefficients
    std::vector<double> coeffVars; // variables of coefficients used in training
    std::vector<double> directionVars; // direction data, size = 1 in 2D and 2 in 3D 
    SVector3 acur, aprev, ainit; // incompatible disp vector
    int index;
    #endif //SWIG
  public:
    InteractionMechanism();
    void printData() const;
    
    #ifndef SWIG
    InteractionMechanism(const InteractionMechanism& src): coefficients(src.coefficients), directionVars(src.directionVars),
              acur(src.acur),aprev(src.aprev),ainit(src.ainit),index(src.index),coeffVars(src.coeffVars){}
    InteractionMechanism* clone() const {return new InteractionMechanism(*this);}
    ~InteractionMechanism();
    void standardizeDirectionVars();
    void seperateNegativeAndPositive(std::vector<int>&allNegatives, std::vector<int>& allPositives) const;
    const std::map<int,double>& getCoefficients() const {return coefficients;};
    const std::vector<double>& getDirectionVars() const {return directionVars;};
    const SVector3& getConstRefToIncompatibleVector(const IPStateBase::whichState ws) const;
    int getIndex() const {return index;};
    int getNumMaterialNodes() const {return coefficients.size();};
    bool isTrivial() const;
    
    std::vector<double>& getRefToCoeffVars(){return coeffVars;};
    const std::vector<double>& getCoeffVars() const {return coeffVars;};
    std::map<int,double>& getRefToCoefficients() {return coefficients;};
    std::vector<double>& getRefToDirectionVars() {return directionVars;};
    SVector3& getRefToIncompatibleVector(const IPStateBase::whichState ws);
    int& getRefToIndex() {return index;};
    
    void saveToFile(FILE* f) const;
    void nextStep();
    void resetIncompatibleDispVector();
    void resetToPreviousStep();
    void copy(const IPStateBase::whichState ws1, const IPStateBase::whichState ws2);
    // induce strain from interaction
    double getCoefficientForNode(int nodeIndex) const;
    bool inInteraction(int nodeIndex) const;
    void getDirection(SVector3& vec, bool stiff=false, std::vector<SVector3>* DvecDdirectionVars=NULL) const;
    void getInducedStrain(int nodeIndex, STensor3& Fi) const;
    void getDInducedStrainDa(int nodeIndex, STensor33& DFiDa) const;
    void getDInducedStrainDDirection(int nodeIndex, STensor33& DFiDdir) const;
    void getDInducedStrainDCoefficient(int nodeIndex, STensor3& DFiDalpha) const;
    void getAllMaterialNodeIds(std::vector<int>& nodes) const;
    void getAllCoefficients(std::vector<double>& coeffs) const;
    #endif //SWIG
};

class NetworkInteraction
{
  #ifndef SWIG
  public:
    typedef std::map<int, MaterialNode*> MaterialNodeContainer;
    typedef std::map<int, InteractionMechanism*> InteractionContainer;
    typedef std::map<const MaterialNode*,std::vector<const InteractionMechanism*> > NodeMapContainer;
  
  protected:
    MaterialNodeContainer _allMaterialNodes; // all material nodes
    InteractionContainer _allInteractions; // all interactions
    NodeMapContainer _nodeInteractionMap; // a material node participating into the multiple interactions
    int _tag;
    
  #endif //SWIG
  public:
    NetworkInteraction(int tag = 1000);
    NetworkInteraction(const NetworkInteraction& src);
    NetworkInteraction* clone() const {return new NetworkInteraction(*this);};
    ~NetworkInteraction();
    int getTag() const;
    void clearData();
    void getInteractionFromTree(const Tree& T,  bool standardize=true);
    void resetIncompatibleDispVector();
    void copyFrom(const NetworkInteraction& src);
    void mergeNodesInteraction(InteractionMechanism& ie, const std::vector<int>& allNodes) const;
    void mergeNodesAllInteractions(const std::vector<int>& allNodes);
    void mergeNodes(int phaseIndex,  // phase index 
                    int numberNodes,  // only interactions with numberNodes nodes
                    NetworkInteraction& newInteraction);
    int getNumberOfMaterialNodes() const;
    int getDimension() const;
    double getTotalWeight() const;
    double getTotalWeightPhase(int iphase) const;
    void getTotalWeightAllPhases(std::map<int, double>& allPhase) const;
    void getPhaseFractions(std::map<int, double>& allPhase) const;
    double getPhaseFraction(int phase) const;
    int getPhaseIndex(int nodeId) const;
    double getWeight(const MaterialNode* node) const;
    double getWeight(int nodeId) const;
    void getWeights(const InteractionMechanism* im, std::vector<double>& allWeights) const;
    double get(int comp, int eleVal) const;
    double getPerPhase(int phaseIndex, int comp, int eleVal) const;
    
    void saveDataToFile(std::string filename) const;
    void loadDataFromFile(std::string filename);
    void loadCohesiveDataFromFile(std::string filename);
    void convertToLinearActivation(std::string outputFileName);
    void createInteractionFromPairsData(std::string filename, int dim, std::string affunc="relu");
    void createInteractionFullByDivison(int numPhases, int nodePerPhase, int numInteraction, int dim, std::string affunc="relu");
    void initializeRandomFullInteraction(int numberMaterialNodes, int numberInteractions, int dim,
                                         const fullVector<double>& phaseFraction, const std::string affunc="relu",
                                        bool rand=true);
                                        
    void createTreeBasedFullInteraction(int Nlayers, int nodePerPhaseFullInteraction, 
                                        int numFullInteraction, int dim,
                                         const fullVector<double>& phaseFraction, 
                                         const std::string affunc="relu",
                                        bool rand=true, bool triple=false);
    void initializeRandomPointwiseInteraction(int numberMaterialNodes, int numPhases, int dim, bool rand=true, std::string affunc="relu");
    void normalizeWeights(std::string newaffunc="linear");
    void standardizeInteractionDirections();
    void normalizeAllInteractionCoefficients();
    bool reInitialize();
    bool checkValidity(bool message=true) const;
    
    #ifndef SWIG
    void getMaterialNodesByMaterialIndex(int matIndex, std::vector<const MaterialNode*>& allNode) const;
    void getMaterialNodesByMaterialIndex(int matIndex, std::vector<MaterialNode*>& allNode);
    MaterialNodeContainer& getMaterialNodeContainer(){return _allMaterialNodes;}
    const MaterialNodeContainer& getMaterialNodeContainer() const {return _allMaterialNodes;}
    InteractionContainer& getInteractionContainer() {return _allInteractions;};
    const InteractionContainer& getInteractionContainer() const {return _allInteractions;};
    NodeMapContainer& getNodeMapContainer() {return _nodeInteractionMap;}
    const NodeMapContainer& getNodeMapContainer() const {return _nodeInteractionMap;}
    
    void getAllMaterialNodes(std::vector<const MaterialNode*>& allNode) const;
    void getAllMaterialNodesForMat(int matNum, std::vector<const MaterialNode*>& allNode) const;
    void getAllMaterialNodes(std::vector<MaterialNode*>& allNode);
    
    MaterialNode* getMaterialNode(const int number);
    const MaterialNode* getMaterialNode(const int number) const;
    void getAllMaterialNodeIds(std::vector<int>& allIds) const;
    void getAllMaterialNodeIdsForMat(int matNum, std::vector<int>& allIds) const;
    void getNumberMaterialNodesPerMat(std::map<int, int>& nodePerMat) const;
    
    const std::vector<const InteractionMechanism*>* getInteractionsForNode(const MaterialNode* node) const;
    void getDFDIncompatibleVector(const MaterialNode* node, std::vector<STensor33>& DFiDa) const;
    void getDFDNormal(const MaterialNode* node, std::vector<STensor33>& DFiDn) const;
    void getDFDCoefficient(const MaterialNode* node, std::vector<STensor3>& DFiDalpha) const;
    
    void getDStressDIncompatibleVector(const MaterialNode* node, std::vector<STensor33>& DPiDa) const;
    void getDStressDNormal(const MaterialNode* node, std::vector<STensor33>& DPiDn) const;
    void getDStressDCoefficient(const MaterialNode* node, std::vector<STensor3>& DPiDalpha) const;
    //
    const STensor3& getStress(const MaterialNode* node) const;
    const STensor3& getDeformationGradient(const MaterialNode* node) const;
    const STensor43& getTangent(const MaterialNode* node ) const;
    void getStress(STensor3& P) const;
    void getDeformationGradientPhase(int phaseIndex, STensor3& F) const;
    //
    void nextStep();
    void resetToPreviousStep();
    void copy(const IPStateBase::whichState ws1, const IPStateBase::whichState ws2);
    //
    // linear term for im with respect to jm
    void getDLinearTermDCoefficient(const InteractionMechanism* im, const InteractionMechanism* jm, fullMatrix<double>& mat) const;
    // linear term for im with respect to jm
    void getDLinearTermDNormalVars(const InteractionMechanism* im, const InteractionMechanism* jm, fullMatrix<double>& mat) const;
    // with repsecto to material weights
    void getDLinearTermDWeights(const InteractionMechanism* im, std::vector<int>& materialIds, fullMatrix<double>& DvecDWeights) const;
    //
    void computeLocalDeformationGradient(const STensor3& Fmacro);
    // get force
    void getLinearTerm(const InteractionMechanism* im, fullVector<double>& vec) const;
    // get DforceDa
    void getBilinearTerm(const InteractionMechanism* im, std::vector<const InteractionMechanism*>& others, fullMatrix<double>& mat) const;    
    // get DforceDFbar
    void getTangentTerm(const InteractionMechanism* im, fullMatrix<double>& mat) const;
    //
    void getLinearInterfaceTerm(const MaterialNode* node, fullVector<double>& vec) const;
    //
    void getBiLinearInterfaceTerm(const MaterialNode* node, fullMatrix<double>& mat) const;
    //
    void getBilinearCrossTermBulkInterface(const InteractionMechanism* im, std::vector<const MaterialNode*>& materialNodes, fullMatrix<double>& mat) const;
    //
    void getBilinearCrossTermInterfaceBulk(const MaterialNode* node, std::vector<const InteractionMechanism*>& others, 
                                     fullMatrix<double>& mat) const;
    #endif //SWIG
};


class CoefficientReduction
{
  public:
    #ifndef SWIG
    CoefficientReduction(){}
    CoefficientReduction(const CoefficientReduction& src){}
    virtual ~CoefficientReduction(){}    
    virtual bool withCoefficientVars(const NetworkInteraction& interaction, const InteractionMechanism& im) const =0;
    virtual int getNumberOfCoefficientVars(const NetworkInteraction& interaction, const InteractionMechanism& im) const =0;
    virtual void updateCoefficientVars(const NetworkInteraction& interaction, InteractionMechanism& im) const =0;
    virtual void updateCoefficientFromCoefficientVars(const NetworkInteraction& interaction, InteractionMechanism& im) const =0;
    virtual void getDCoefficientsDWeights(const NetworkInteraction& interaction, const InteractionMechanism& im, int nodeId, std::vector<int>&allNodes, std::vector<double>& DalphaDW) const = 0;
    virtual void getDCoefficientsDCoefficientVars(const NetworkInteraction& interaction, const InteractionMechanism& im, int nodeId, std::vector<double>& DalphaVars) const= 0;
    virtual CoefficientReduction* clone() const = 0;
    #endif //SWIG
};


class CoefficientReductionVoid: public CoefficientReduction
{
  public:
    #ifndef SWIG
    CoefficientReductionVoid(){}
    CoefficientReductionVoid(const CoefficientReductionVoid& src){}
    virtual ~CoefficientReductionVoid(){}    
    virtual bool withCoefficientVars(const NetworkInteraction& interaction, const InteractionMechanism& im) const {return false;};
    virtual int getNumberOfCoefficientVars(const NetworkInteraction& interaction, const InteractionMechanism& im) const {return 0;};
    virtual void updateCoefficientVars(const NetworkInteraction& interaction, InteractionMechanism& im) const {};
    virtual void updateCoefficientFromCoefficientVars(const NetworkInteraction& interaction, InteractionMechanism& im) const {};
    virtual void getDCoefficientsDWeights(const NetworkInteraction& interaction, const InteractionMechanism& im, int nodeId, std::vector<int>&allNodes, std::vector<double>& DalphaDW) const {};
    virtual void getDCoefficientsDCoefficientVars(const NetworkInteraction& interaction, const InteractionMechanism& im, int nodeId, std::vector<double>& DalphaVars) const {};
    virtual CoefficientReduction* clone() const {return new CoefficientReductionVoid();};
    #endif //SWIG
};

class CoefficientReductionAll : public CoefficientReduction
{  
  public:
    CoefficientReductionAll();
    virtual ~CoefficientReductionAll(){};
    #ifndef SWIG
    CoefficientReductionAll(const CoefficientReductionAll& src): CoefficientReduction(src){}
    virtual bool withCoefficientVars(const NetworkInteraction& interaction, const InteractionMechanism& im) const {return false;};
    virtual int getNumberOfCoefficientVars(const NetworkInteraction& interaction, const InteractionMechanism& im) const {return 0;};
    virtual void updateCoefficientVars(const NetworkInteraction& interaction, InteractionMechanism& im) const;
    virtual void updateCoefficientFromCoefficientVars(const NetworkInteraction& interaction, InteractionMechanism& im) const;
    virtual void getDCoefficientsDWeights(const NetworkInteraction& interaction, const InteractionMechanism& im, int nodeId, std::vector<int>&allNodes, std::vector<double>& DalphaDW) const;
    virtual void getDCoefficientsDCoefficientVars(const NetworkInteraction& interaction, const InteractionMechanism& im, int nodeId, std::vector<double>& DalphaVars) const;
    virtual CoefficientReduction* clone() const {return new CoefficientReductionAll(*this);}
    #endif //SWIG
};

class CoefficientReductionFullCoefficient : public CoefficientReductionAll
{
  #ifndef SWIG
  protected:
    std::set<int> _reductionAllInteraction;
  #endif //SWIG
  public:
    CoefficientReductionFullCoefficient();
    virtual ~CoefficientReductionFullCoefficient(){};
    void reductionAllInteraction(int id);
    #ifndef SWIG
    CoefficientReductionFullCoefficient(const CoefficientReductionFullCoefficient& src) : CoefficientReductionAll(src), _reductionAllInteraction(src._reductionAllInteraction){}
    virtual bool withCoefficientVars(const NetworkInteraction& interaction, const InteractionMechanism& im) const;
    virtual int getNumberOfCoefficientVars(const NetworkInteraction& interaction, const InteractionMechanism& im) const;
    virtual void updateCoefficientVars(const NetworkInteraction& interaction, InteractionMechanism& im) const;
    virtual void updateCoefficientFromCoefficientVars(const NetworkInteraction& interaction, InteractionMechanism& im) const;
    virtual void getDCoefficientsDWeights(const NetworkInteraction& interaction, const InteractionMechanism& im, int nodeId, std::vector<int>&allNodes, std::vector<double>& DalphaDW) const;
    virtual void getDCoefficientsDCoefficientVars(const NetworkInteraction& interaction, const InteractionMechanism& im, int nodeId, std::vector<double>& DalphaVars) const;
    virtual CoefficientReduction* clone() const {return new CoefficientReductionFullCoefficient(*this);}
    #endif //SWIG
};
#endif //_NETWORKINTERACTION_H_