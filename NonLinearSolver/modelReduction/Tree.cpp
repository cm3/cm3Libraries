//
// C++ Interface: binary tree
//
//
// Author:  <V.-D. Nguyen>, (C) 2020
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "Tree.h"
#include <stdio.h>
#include <random> 
#include <chrono>
#include <math.h>
#include "OS.h"
#include "elasticPotential.h"
#include "NetworkInteraction.h"


TreeNode::TreeNode(): MaterialNode(), depth(-1), location(-1), childs(0),parent(NULL),
direction(),  childOrder(-1)
{
  
}

TreeNode::TreeNode(const TreeNode& src): MaterialNode(src)
{
  Msg::Error("copy constructor TreeNode::TreeNode cannot be called");
  Msg::Exit(0);
};

TreeNode::TreeNode(int k, int c, TreeNode* p, int o, const std::string affunc): MaterialNode(), depth(k), location(c), childs(0),parent(p),
direction(0), childOrder(o)
{
  af = activationFunction::createActivationFunction(affunc.c_str());
};
TreeNode::~TreeNode()
{
  
};

void TreeNode::printData() const
{
  printf("MaterialNode: depth %d- loc %d  -mat index %d- child order %d - weight = %e - nb of direction vars %ld",depth,location,matIndex,childOrder,weight,direction.size());
  for (int i=0; i< direction.size(); i++)
  {
    printf(" %e",direction[i]);
  }
  printf("\n");
};

void TreeNode::saveToFile(FILE* f) const
{
  // depth location childOrder parentIdLoc numChild, numDir, child1Loc, ..., childNloc, weight,  dir1, dir2 ...,, afFunctionName
  int parentloc = -1;
  if (parent!=NULL)
  {
    parentloc = parent->location;
  }
  fprintf(f,"%d %d %d %d %d %ld %ld",depth,location, matIndex, childOrder,parentloc,childs.size(), direction.size());
  //
  if (childs.size() > 0)
  {
    for (int i=0; i< childs.size(); i++)
    {
      fprintf(f," %d",childs[i]->location);
    }
  }
  fprintf(f," %.16g", weight);
  for (int i=0; i< direction.size(); i++)
  {
    fprintf(f," %.16g",direction[i]);
  }
  fprintf(f," %s\n",af->getName().c_str());
  fflush(f);
};

void TreeNode::saveToFileCohesive(FILE* f) const
{
  if (withCohesive())
  {
    int nCoh = cohesiveCoeffVars.size();
    int nDirVars = cohesiveDirectionVars.size2();
    fprintf(f,"%d %d %d %d %d",depth,location,cohesiveMatIndex,nCoh, nDirVars);
    for (int i=0; i< nCoh; i++)
    {
      fprintf(f," %.16g",cohesiveCoeffVars(i));
    }
    for (int i=0; i< nCoh; i++)
    {
      for (int j=0; j< nDirVars; j++)
      {
        fprintf(f," %.16g",cohesiveDirectionVars(i,j));
      };
    }
    fprintf(f," %s\n",cohesiveAf->getName().c_str());
    fflush(f);
  }
};

void TreeNode::getNormal(std::vector<SVector3>& allVec, bool stiff, std::vector<std::vector<SVector3> >* allDnormalDunknown) const
{
  // number of normal
  double pi = 3.14159265359;
  int numberNormal = childs.size()-1;
  int totalNumberDirVars = direction.size();
  int numberVarsPerNormal = totalNumberDirVars/numberNormal;
  // reallocated
  if (numberNormal > 0)
  {
    if (allVec.size() != numberNormal)
    {
      allVec.resize(numberNormal);
    }
    
    if (stiff)
    {
      if (allDnormalDunknown->size() != numberNormal)
      {
        allDnormalDunknown->resize(numberNormal,std::vector<SVector3>(numberVarsPerNormal,SVector3()));
      }
    }
  }
  
  for (int i=0; i< numberNormal; i++)
  {
    SVector3& vec = allVec[i];
    std::vector<SVector3>* DnormalDunknown = NULL;
    if (stiff)
    {
      DnormalDunknown = &(*allDnormalDunknown)[i];
    }
    if (numberVarsPerNormal == 1)
    {
      double theta = 2.*pi*direction[i];
      vec[0] = cos(theta);
      vec[1] = sin(theta);
      vec[2] = 0.;
      if (stiff)
      {
        if ((*DnormalDunknown).size() != 1)
        {
          (*DnormalDunknown).resize(1);
        }
        (*DnormalDunknown)[0][0] = -sin(theta)*2.*pi;
        (*DnormalDunknown)[0][1] = cos(theta)*2.*pi;
        (*DnormalDunknown)[0][2] = 0.;
        
      }
    }
    else if (numberVarsPerNormal == 2)
    {
      // cylindrical coordinates
      double phi = 2.*pi*direction[i*numberVarsPerNormal];
      double theta = pi*direction[i*numberVarsPerNormal+1];
      vec[0] = sin(theta)*cos(phi);
      vec[1] = sin(theta)*sin(phi);
      vec[2] = cos(theta);
      if (stiff)
      {
        if ((*DnormalDunknown).size() != 2)
        {
          (*DnormalDunknown).resize(2);
        }
        
        (*DnormalDunknown)[0][0] = -sin(theta)*sin(phi)*2*pi;
        (*DnormalDunknown)[0][1] = sin(theta)*cos(phi)*2*pi;
        (*DnormalDunknown)[0][2] = 0.;
        
        (*DnormalDunknown)[1][0] = cos(theta)*cos(phi)*pi;
        (*DnormalDunknown)[1][1] = cos(theta)*sin(phi)*pi;
        (*DnormalDunknown)[1][2] = -sin(theta)*pi;
      }
    }
    else
    {
      Msg::Error("number of vars per normal must be 1 or 2, a value of %d is impossible",numberVarsPerNormal);
      Msg::Exit(0);
    }
  }
};


Tree::~Tree()
{
  clear();
};


Tree::Tree(): _root(NULL)
{
};

void Tree::createSpecialBinaryTree(int numNodes, int numDirVars, const std::string affunc)
{
  clear();
  _root = new TreeNode(0,0,NULL,0);
  
  TreeNode* np = _root;
  int depth = 1;
  int numAllocatedNodes = 1;
  int matStart = 0;
  while (numAllocatedNodes < numNodes)
  {
    numAllocatedNodes += 2;
    //
    np->childs.resize(2);
    np->direction.resize(numDirVars);
    np->childs[0] = new TreeNode(depth,0,np,0,affunc);
    np->childs[0]->matIndex = matStart;
    
    if (numAllocatedNodes >= numNodes)
    {
      np->childs[1] = new TreeNode(depth,1,np,1,affunc);
      if (np->childs[0]->matIndex == 0)
      {
        np->childs[1]->matIndex = 1;
      }
      else
      {
        np->childs[1]->matIndex = 0;
      }
    }
    else
    {
      np->childs[1] = new TreeNode(depth,1,np,1);
    }
    if (matStart == 0) matStart = 1;
    else matStart= 0;
    depth++;
    np = np->childs[1];
  }
};

void Tree::createSpecialTripleTree(int numNodes, int numDirVars, const std::string affunc)
{
  clear();
  _root = new TreeNode(0,0,NULL,0);  
  TreeNode* np = _root;
  int depth = 1;
  int numAllocatedNodes = 1;
  while (numAllocatedNodes < numNodes)
  {
    //
    numAllocatedNodes += 3;
    if (numAllocatedNodes < numNodes)
    {
      np->childs.resize(3);
      np->direction.resize(2*numDirVars);
    }
    else
    {
      np->childs.resize(2);
      np->direction.resize(numDirVars);
    }
    np->childs[0] = new TreeNode(depth,0,np,0,affunc);
    np->childs[0]->matIndex = 0;
    np->childs[1] = new TreeNode(depth,1,np,1,affunc);
    np->childs[1]->matIndex = 1;
    
    if (numAllocatedNodes < numNodes)
    {
      np->childs[2] = new TreeNode(depth,2,np,2);
      np = np->childs[2];
    }
    
    depth++;
  }
};

void Tree::createRandomTreeSameDepthForNodes(int nbLeaves, int numChildMax, int leafPerNode, int numDirVars,const std::string affunc, bool randomChild, bool randomMat)
{ 
  auto makeGroup = [&](const std::vector<int>& input, std::vector<int>& outPut, bool first)
  {
    outPut.clear();
    int numInPut = input.size();
    int iter = 0;
    while (true)
    {
      if (numInPut == 2 || numInPut == 3)
      {
        outPut.push_back(numInPut);
        break;
      }
      else if (numInPut == 4)
      {
        outPut.push_back(2);
        outPut.push_back(2);
        break;
      }
            
      int numChilds;
      if (first)
      {
        numChilds = leafPerNode;
        if (randomMat)
        {
          numChilds =  rand() % (leafPerNode-1) + 2; // number takes a random value from 2 to leafPerNode 
        }
      }
      else
      {
        numChilds = numChildMax;
        if (randomChild)
        {
          numChilds =  rand() % (numChildMax-1) + 2; // number takes a random value from 2 to numChildMax 
        }
      }
      if (numInPut <= numChilds)
      {
        outPut.push_back(numInPut);
        break;
      }
      int remaining = numInPut - iter - numChilds;
      if (remaining > 1)
      {
        outPut.push_back(numChilds);
        iter += numChilds;
      }
      else 
      {
        if (first)
        {
          if (numChilds+remaining > leafPerNode)
          {
            int num1 = leafPerNode;
            int num2 =numChilds+remaining -leafPerNode;
            while (num2 < 2)
            {
              num1 --;
              num2 ++;
            }
            outPut.push_back(num1);
            outPut.push_back(num2);
            break;
          }
          else
          {
            outPut.push_back(numChilds+remaining);
            break;
          }
        }
        else
        {
          if (numChilds+remaining < 4)
          {
            outPut.push_back(numChilds+remaining);
          }
          else
          {
            outPut.push_back(2);
            outPut.push_back(numChilds+remaining-2);
          }
          iter += numChilds+remaining;
        }
      };
      if (iter >= numInPut) break;
    }
  };
  
  auto printVector = [](const std::vector<int>& vv)
  {
    printf("%ld:",vv.size());
    for (int i=0; i< vv.size(); i++)
    {
      printf("%d ",vv[i]);
    }
    printf("\n");
  }; 
  
  
  std::vector<std::vector<int> > allListByDepth;
  
  // first list
  allListByDepth.resize(1);
  for (int i=0; i< nbLeaves; i++)
  {
    allListByDepth[0].push_back(1);
  }
  std::vector<int> intPut = allListByDepth[0];
  printVector(intPut);
  int iter=0;
  while (true)
  {
    std::vector<int> outPut;
    makeGroup(intPut,outPut,iter==0);
    allListByDepth.push_back(outPut);
    printVector(outPut);
    intPut = outPut;
    if (outPut.size() == 1)
    {
      break;
    }
    iter++;
  };
  
  clear();
  if (nbLeaves <0) return;
  srand (time(NULL));
  
  int depthMax = allListByDepth.size()-1;
  
  std::vector<TreeNode*> listPrev, listCur;  
  _root = new TreeNode(0,0,NULL,0);
  
  listPrev.push_back(_root);
  int depth = 1;
  while (true)
  {
    listCur.clear();
    int inode=0;
    
    for (int i =0; i< listPrev.size(); i++)
    {
      TreeNode* np = listPrev[i];          
      //
      printVector(allListByDepth[depthMax-depth+1]);
      int trueNbChilds = allListByDepth[depthMax-depth+1][i];
      Msg::Info("i=%d/%d,trueNbChilds = %d",i,listPrev.size(),trueNbChilds);
      np->childs.resize(trueNbChilds);
      np->direction.resize((trueNbChilds-1)*numDirVars);
      for (int j=0; j< trueNbChilds; j++)
      {
        if (depth == depthMax)
        {
          // leaf
          np->childs[j] = new TreeNode(depth,inode,np,j,affunc);
        }
        else
        {
          np->childs[j] = new TreeNode(depth,inode,np,j);
          listCur.push_back(np->childs[j]);
        }
        inode++;
      }
    }
    depth ++;
    listPrev = listCur;
    if (listCur.size() ==0)
    {
      break;
    }
  };
  std::vector<TreeNode*> allLeaves;
  getRefToAllLeaves(allLeaves);
  std::set<TreeNode*> setNodes;
  for (int i=0; i< allLeaves.size(); i++)
  {
    TreeNode* leaf = allLeaves[i];
    if (setNodes.find(leaf) == setNodes.end())
    {
      TreeNode* parent = leaf->parent;
      std::vector<TreeNode*> ll;
      for (int j=0; j< parent->childs.size(); j++)
      {
        TreeNode* nj = parent->childs[j];
        if (nj->childs.size() == 0)
        {
          ll.push_back(nj);
          setNodes.insert(nj);
        }
      }
      if (ll.size() == leafPerNode)
      {
        for (int j=0; j< ll.size(); j++)
        {
          ll[j]->matIndex  = j;
        }
      }
      else
      {
        std::vector<int> v(leafPerNode,0);
        for (int j=0; j< leafPerNode; j++)
        {
          v[j] = j;
        }
        for (int j=0; j< ll.size(); j++)
        {
          int pos =  rand() % (v.size()); // random from 0 to v.size() -1
          TreeNode* otherNode = ll[j];
          otherNode->matIndex = v[pos];
          v.erase(v.begin() + pos);
        }
      }
    }
  }
  
    
};

void Tree::createRandomTree(int nbLeaves, int numChildMax, int leafPerNode, int numDirVars,const std::string affunc, bool randomSubdivion, bool randomChild, bool sameNbChildLayer)
{
  
  auto randomDistribution = [&randomSubdivion](int inSize, int numPart, int leafPerNode, std::vector<int>& out)
  {
    out.clear();
    bool suc = true;
    if (inSize <= numPart)
    {
      if (inSize > leafPerNode)
      {
        out.resize(2);
        out[0] = leafPerNode;
        out[1] = inSize - leafPerNode;
      }
      else
      {
        out.resize(inSize);
        for (int i=0; i< inSize; i++)
        {
          out[i] = 1;
        }
      }
    }
    else
    {
      
      int maxIter = 0;
      while (maxIter <10)
      {
        out.resize(numPart,0);
        int remaining = inSize;
        int remainingPart = numPart;
        int numOneSize = 0;
        for (int i=0; i< numPart-1; i++)
        {
          if (randomSubdivion)
          {
            out[i] = rand()%(remaining-remainingPart)+1;
          }
          else
          {
            out[i] = inSize/numPart;
          }
          remainingPart--;
          remaining -= out[i];    
          if (out[i] == 1) numOneSize++;
        };
        out[numPart-1] = remaining;
        if (remaining == 1) numOneSize++;
        if (remaining == 0 || numOneSize > leafPerNode)
        {
          Msg::Error("re distribution remaining = %d  numOneSize = %d",remaining,numOneSize);
          numPart --;
          suc = false;
        }
        else
        {
          suc = true;
          break;
        }
        maxIter++;
      }
    }
    
    Msg::Info("partition %d into %d parts: ",inSize,out.size());
    for (int i=0; i< out.size(); i++)
    {
      Msg::Info("num part %d = %d",i,out[i]);
    }
    if (!suc)
    {
      Msg::Exit(0);
    }
    
  };
  
  clear();
  if (nbLeaves <0) return;
  srand (time(NULL));

  std::vector<TreeNode*> listPrev, listCur;
  std::vector<int> listCurLeaves, listPrevLeaves;

  _root = new TreeNode(0,0,NULL,0);
  listPrev.push_back(_root);
  listPrevLeaves.push_back(nbLeaves);
  
  int depth = 1;
  while (true)
  {
    listCur.clear();
    listCurLeaves.clear();
    int inode=0;
    int numChilds= numChildMax;
    if (sameNbChildLayer)
    {
      if (randomChild)
      {
        numChilds =  rand() % (numChildMax-1) + 2;
      }
    }
    
    for (int i =0; i< listPrev.size(); i++)
    {
      TreeNode* np = listPrev[i];
      int indexes = listPrevLeaves[i];
      
      if (!sameNbChildLayer)
      {
        if (randomChild)
        {
          numChilds =  rand() % (numChildMax-1) + 2;
        }
      }
      
      std::vector<int > partIndexes;
      randomDistribution(indexes, numChilds, leafPerNode, partIndexes);
      
      //
      int trueNbChilds = partIndexes.size();
      Msg::Info("trueNbChilds = %d",trueNbChilds);
      np->childs.resize(trueNbChilds);
      np->direction.resize((trueNbChilds-1)*numDirVars);
      for (int j=0; j< trueNbChilds; j++)
      {
        if (partIndexes[j] == 1)
        {
          // leaf
          np->childs[j] = new TreeNode(depth,inode,np,j,affunc);
        }
        else if (partIndexes[j] > 1)
        {
          np->childs[j] = new TreeNode(depth,inode,np,j);
          listCurLeaves.push_back(partIndexes[j]);
          listCur.push_back(np->childs[j]);
        }
        inode++;
      }
    }
    depth ++;
    listPrev = listCur;
    listPrevLeaves = listCurLeaves;
    if (listCur.size() ==0)
    {
      break;
    }
  };
  std::vector<TreeNode*> allLeaves;
  getRefToAllLeaves(allLeaves);
  std::set<TreeNode*> setNodes;
  for (int i=0; i< allLeaves.size(); i++)
  {
    TreeNode* leaf = allLeaves[i];
    if (setNodes.find(leaf) == setNodes.end())
    {
      TreeNode* parent = leaf->parent;
      std::vector<TreeNode*> ll;
      for (int j=0; j< parent->childs.size(); j++)
      {
        TreeNode* nj = parent->childs[j];
        if (nj->childs.size() == 0)
        {
          ll.push_back(nj);
          setNodes.insert(nj);
        }
      }
      if (ll.size() == leafPerNode)
      {
        for (int j=0; j< ll.size(); j++)
        {
          ll[j]->matIndex  = j;
        }
      }
      else
      {
        std::vector<int> v(leafPerNode,0);
        for (int j=0; j< leafPerNode; j++)
        {
          v[j] = j;
        }
        for (int j=0; j< ll.size(); j++)
        {
          int pos =  rand() % (v.size()); // random from 0 to v.size() -1
          TreeNode* otherNode = ll[j];
          otherNode->matIndex = v[pos];
          v.erase(v.begin() + pos);
        }
      }
    }
  }
};

void Tree::createMixedTree(int maxDepth, int numChildMax, int leafPerNode, int numDirVars,const std::string affunc)
{
  clear();
  if (maxDepth <0) return;
  int depth = 0;
  std::vector<TreeNode*> listPrev, listCur;
  srand (time(NULL));
  while (depth <= maxDepth)
  {
    if (depth == 0)
    {
      _root = new TreeNode(0,0,NULL,0);
      listPrev.push_back(_root);
    }
    else
    {
      listCur.clear();
      int inode=0;
      for (int i =0; i< listPrev.size(); i++)
      {
        TreeNode* np = listPrev[i];
        int numChilds;
        if (depth == maxDepth)
        {
          numChilds = leafPerNode;
        }
        else
        {
          numChilds= rand() % (numChildMax-1) + 2;
        }
        //Msg::Info("numchild = %d",numChilds);
        np->childs.resize(numChilds);
        np->direction.resize((numChilds-1)*numDirVars);
        for (int j=0; j< numChilds; j++)
        {
          if (depth == maxDepth)
          {
            np->childs[j] = new TreeNode(depth,inode,np,j,affunc);
          }
          else
          {
            np->childs[j] = new TreeNode(depth,inode,np,j);
          }
          listCur.push_back(np->childs[j]);
          inode++;
        }
      }
      listPrev = listCur;
    }
    depth ++;
  };
  
  std::vector<TreeNode*> allLeaves;
  getRefToAllLeaves(allLeaves);
  for (int i=0; i< allLeaves.size(); i++)
  {
    TreeNode* leaf = allLeaves[i];
    leaf->matIndex = leaf->childOrder;
  }
};

void Tree::createPerfectTree(int maxDepth, int numChildRegular, int numChildLeaf, int numDirVars,const std::string affunc, int lastRepeat)
{
  clear();
  if (maxDepth <0) return;
  int depth = 0;
  std::vector<TreeNode*> listPrev, listCur;
  while (depth <= maxDepth)
  {
    if (depth == 0)
    {
      _root = new TreeNode(0,0,NULL,0);
      listPrev.push_back(_root);
    }
    else
    {
      listCur.clear();
      int inode=0;
      for (int i =0; i< listPrev.size(); i++)
      {
        TreeNode* np = listPrev[i];
        int numChilds = numChildRegular;
        if (depth >= maxDepth-lastRepeat+1)
        {
          numChilds = numChildLeaf;
        }
        np->childs.resize(numChilds);
        np->direction.resize((numChilds-1)*numDirVars);
        for (int j=0; j< numChilds; j++)
        {
          if (depth == maxDepth)
          {
            np->childs[j] = new TreeNode(depth,inode,np,j,affunc);
          }
          else
          {
            np->childs[j] = new TreeNode(depth,inode,np,j);
          }
          listCur.push_back(np->childs[j]);
          inode++;
        }
      }
      listPrev = listCur;
    }
    depth ++;
  };
  
  std::vector<TreeNode*> allLeaves;
  getRefToAllLeaves(allLeaves);
  for (int i=0; i< allLeaves.size(); i++)
  {
    TreeNode* leaf = allLeaves[i];
    leaf->matIndex = leaf->childOrder;
  }
};


void Tree::createPerfectTree(int maxDepth, int numChilds, int numDirVars,const std::string affunc)
{
  clear();
  if (maxDepth <0) return;
  int depth = 0;
  std::vector<TreeNode*> listPrev, listCur;
  while (depth <= maxDepth)
  {
    if (depth == 0)
    {
      _root = new TreeNode(0,0,NULL,0);
      listPrev.push_back(_root);
    }
    else
    {
      listCur.clear();
      int inode=0;
      for (int i =0; i< listPrev.size(); i++)
      {
        TreeNode* np = listPrev[i];
        np->childs.resize(numChilds);
        np->direction.resize((numChilds-1)*numDirVars);
        for (int j=0; j< numChilds; j++)
        {
          if (depth == maxDepth)
          {
            np->childs[j] = new TreeNode(depth,inode,np,j,affunc);
          }
          else
          {
            np->childs[j] = new TreeNode(depth,inode,np,j);
          }
          listCur.push_back(np->childs[j]);
          inode++;
        }
      }
      listPrev = listCur;
    }
    depth ++;
  };
  
  std::vector<TreeNode*> allLeaves;
  getRefToAllLeaves(allLeaves);
  for (int i=0; i< allLeaves.size(); i++)
  {
    TreeNode* leaf = allLeaves[i];
    leaf->matIndex = leaf->childOrder;
  }
};

void Tree::createPerfectTreeBis(int maxDepth, int numChilds, int numDirVars,const std::string affunc)
{
  clear();
  if (maxDepth <0) return;
  int depth = 0;
  std::vector<TreeNode*> listPrev, listCur;
  while (depth <= maxDepth)
  {
    if (depth == 0)
    {
      _root = new TreeNode(0,0,NULL,0);
      listPrev.push_back(_root);
    }
    else
    {
      listCur.clear();
      int inode=0;
      for (int i =0; i< listPrev.size(); i++)
      {
        TreeNode* np = listPrev[i];
        np->childs.resize(numChilds);
        np->direction.resize((numChilds-1)*numDirVars);
        for (int j=0; j< numChilds; j++)
        {
          if (depth == maxDepth || (j==0 && depth == maxDepth-1))
          {
            np->childs[j] = new TreeNode(depth,inode,np,j,affunc);
          }
          else
          {
            np->childs[j] = new TreeNode(depth,inode,np,j);
            listCur.push_back(np->childs[j]);
          }
          inode++;
        }
      }
      listPrev = listCur;
    }
    depth ++;
  };
  
  std::vector<TreeNode*> allLeaves;
  getRefToAllLeaves(allLeaves);
  for (int i=0; i< allLeaves.size(); i++)
  {
    TreeNode* leaf = allLeaves[i];
    leaf->matIndex = leaf->childOrder;
  }
};

void Tree::saveDataToFile(std::string filename) const
{
  printf("saving data to file %s\n",filename.c_str());
  //
  FILE * file = fopen(filename.c_str(),"w");
  //
  std::vector< TreeNode*> listPrev, listCur;
  listPrev.push_back(_root);
  int depth = 0;
  while (true)
  {
    listCur.clear();
    for (int i =0; i< listPrev.size(); i++)
    {
      TreeNode& np = *(listPrev[i]);
      np.saveToFile(file);
      for (int j=0; j< np.childs.size(); j++)
      {
        listCur.push_back(np.childs[j]);
      }
    }
    listPrev = listCur;
    if (listCur.size() == 0)
    {
      break;
    }
    depth++;
  };
  fclose(file);
  printf("done saving data to file %s\n",filename.c_str());
  
  
  std::map<int, const TreeNode*> nodeWithCohesive;
  nodeContainer allLeaves;
  getAllLeaves(allLeaves);
  for (int i=0; i< allLeaves.size(); i++)
  {
    if (allLeaves[i]->withCohesive())
    {
      nodeWithCohesive[allLeaves[i]->getNodeId()] = allLeaves[i];
    }
  }
  if (nodeWithCohesive.size() > 0)
  {
    filename  = "cohesive_"+filename;
    printf("saving data to file %s\n",filename.c_str());
    FILE * fileCohesive = fopen(filename.c_str(),"w");
    for (std::map<int, const TreeNode*>::iterator  it = nodeWithCohesive.begin(); it != nodeWithCohesive.end(); it++)
    {
      const TreeNode* leaf = it->second;
      leaf->saveToFileCohesive(fileCohesive);
    }
    printf("done saving data to file %s\n",filename.c_str());
  }
  //
};

void Tree::loadCohesiveDataFromFile(std::string filename)
{
  printf("loading cohsiec data from file %s\n",filename.c_str());
  FILE* fp = fopen(filename.c_str(),"r");
  if (fp !=NULL)
  {
    std::vector<TreeNode*> allLeaves;
    getRefToAllLeaves(allLeaves);
    std::map<std::pair<int,int>, TreeNode*> nodeMap;
    for (int i=0; i< allLeaves.size(); i++)
    {
      nodeMap[std::pair<int,int>(allLeaves[i]->depth,allLeaves[i]->location)] = allLeaves[i];
    }
    
    while (1)
    {
      if(feof(fp))
        break;
        
      int depth = 0;
      int location = 0;
      int cohMatIndex = 0;
      int numCoh = 0;
      int numCohDirVars = 0;
      if( fscanf(fp, "%d %d %d %d %d",&depth,&location,&cohMatIndex,&numCoh,&numCohDirVars)==5)
      {
        printf("material node depth = %d loc = %d cohMatIndex=%d numCoh = %d numDirVars = %d\n",depth,location,cohMatIndex,numCoh,numCohDirVars);
      }
      else
      {
        break;
      }
      
      TreeNode * node = NULL;
      std::map<std::pair<int,int>, TreeNode*>::iterator nodeMapFind = nodeMap.find(std::pair<int,int>(depth,location));
      if (nodeMapFind == nodeMap.end())
      {
        Msg::Error("node %d %d does not exist",depth,location);
        Msg::Exit(0);
      }
      else
      {
        node = nodeMapFind->second;
      }
      if (numCoh > 0 && numCohDirVars > 0)
      {
        node->cohesiveMatIndex = cohMatIndex;
        node->allocateCohesiveParameters(numCoh,numCohDirVars);
        fullVector<double>& cohCoefs = node->cohesiveCoeffVars;
        fullMatrix<double>& cohDirVars = node->cohesiveDirectionVars;
        for (int j=0; j< numCoh; j++)
        {
          if (fscanf(fp, "%lf",&(cohCoefs(j)))==1)
          {
            printf("%.16g ",cohCoefs(j));
          }
          else
          {
            Msg::Error("error E reading file in Tree::loadCohesiveDataFromFile");
            fclose(fp);
            Msg::Exit(0);
          }
        }
        
        for (int j=0; j< numCoh; j++)
        {
          for (int k=0; k< numCohDirVars; k++)
          {
            if (fscanf(fp, "%lf",&(cohDirVars(j,k)))==1)
            {
              printf("%.16g ",cohDirVars(j,k));
            }
            else
            {
              Msg::Error("error E reading file in Tree::loadCohesiveDataFromFile");
              fclose(fp);
              Msg::Exit(0);
            }
          }
        }
        char what[256];
        if (fscanf(fp, "%s", what)==1)
        {
          printf("activation function=%s \n", what);
          node->cohesiveAf = activationFunction::createActivationFunction(what);
        }
        else
        {
          Msg::Error("error reading file in Tree::loadCohesiveDataFromFile");
          fclose(fp);
          Msg::Exit(0);
        }
      }
    }
    
    fclose(fp);
    //
    printf("done loading data from file %s\n",filename.c_str()); 
  }
  else
  {
    Msg::Error("File %s does not exist",filename.c_str());
    Msg::Exit(0);
  }
}

void Tree::loadDataFromFile(std::string filename)
{
  // clear tree
  clear();
  std::map<std::pair<int,int>, TreeNode* > nodeMap;
  printf("loading data from file %s\n",filename.c_str());
  //
  FILE* fp = fopen(filename.c_str(),"r");
  if (fp !=NULL)
  {
    printf("start reading file: %s\n",filename.c_str());
    while (1)
    {
      if(feof(fp))
        break;
      int depth, location, matIndex, childOrder, parentIdLoc, numChild, numDir;
      if (fscanf(fp, "%d %d %d %d %d %d %d", &depth, &location, &matIndex, &childOrder, &parentIdLoc, &numChild, &numDir)==7)
      {
        printf("--------depth=%d, location=%d, childOrder=%d, parentIdLoc=%d, numChild=%d, numDir=%d \n",
                depth, location, childOrder, parentIdLoc, numChild, numDir);
      }
      else
      {
        break;
      }
      std::vector<int> allChilds(numChild);
      for (int i=0; i< numChild; i++)
      {
        int fl=fscanf(fp, "%d",&allChilds[i]);
        printf("child %d location %d\n",i,allChilds[i]);
      }
      double weight;
      int fl=fscanf(fp, "%lf",&weight);
      printf("weight = %f\n",weight);
      std::vector<double> direction(numDir);
      for (int i=0; i< numDir; i++)
      {
        fl=fscanf(fp, "%lf",&direction[i]);
        printf("direction %d value %f\n",i,direction[i]);
      }
      char what[256];
      fl=fscanf(fp, "%s", what);
      printf("aftype = %s\n",what);
      
      // create node
      if (depth == 0)
      {
        TreeNode* node = new TreeNode();
        node->af = activationFunction::createActivationFunction(what);
        node->direction = direction;
        node->weight = weight;
        node->depth = depth;
        node->location = location;
        node->childOrder = childOrder;
        node->parent = NULL;
        node->matIndex = matIndex;
        if (numChild > 0)
        {
          node->childs.resize(numChild,NULL);
        }
        else
        {
          node->childs.clear();
        }
        _root = node;
        nodeMap[std::pair<int,int>(depth,location)]=node;
      }
      else
      {
        TreeNode* node = new TreeNode();
        node->af = activationFunction::createActivationFunction(what);
        node->direction = direction;
        node->weight = weight;
        node->depth = depth;
        node->location = location;
        node->childOrder = childOrder;
        std::pair<int,int> twoNum(depth-1,parentIdLoc);
        if (nodeMap.find(twoNum) == nodeMap.end())
        {
          Msg::Error("node %d %d is not found !!!",depth-1,parentIdLoc);
          Msg::Exit(0);
        }
        node->parent = nodeMap[twoNum];
        node->parent->childs[childOrder] = node;
        node->matIndex = matIndex;
        if (numChild > 0)
        {
          node->childs.resize(numChild,NULL);
        }
        else
        {
          node->childs.clear();
        }
        printf("node %d %d:\n",depth,location);
        node->parent->printData();
        nodeMap[std::pair<int,int>(depth,location)]=node;
      }
    }
    fclose(fp);
    //
    printf("done loading data from file %s\n",filename.c_str());
  }
  else
  {
    Msg::Error("File %s does not exist",filename.c_str());
    Msg::Exit(0);
  }
};

int Tree::getNumberOfNodes() const
{
  nodeContainer allNodes;
  getAllNodes(allNodes);
  return allNodes.size();
};

bool Tree::checkDuplicate() const
{
  bool ok = false;
  nodeContainer allNodes;
  getAllNodes(allNodes);
  std::set<int> nodeId;
  for (int i=0; i< allNodes.size(); i++)
  {
    const TreeNode* node = allNodes[i];
    std::set<int>::const_iterator itF = nodeId.find(node->getNodeId());
    if (itF == nodeId.end())
    {
      nodeId.insert(node->getNodeId());
    }
    else
    {
      Msg::Error("node duplicating:");
      node->printData();
      ok = true;
    }
  }
  if (!ok)
  {
    Msg::Info("no duplicatint node is found !!!");
  }
  return ok;
}

bool Tree::removeZeroLeaves(double tol, bool iteration)
{
  bool ok = false;
  int maxInter = 100;
  int it = 0;
  while (true)
  {
    it ++;
    if (it == maxInter)
    {
      Msg::Error("removing duplicating leaves: maximal number of iterations (%d) reaches", maxInter);
      Msg::Exit(0);
    }
    std::vector<TreeNode*> allLeaves;
    getRefToAllLeaves(allLeaves);
    std::vector<TreeNode*> allLeavesWillRemove;
    for (int i=0; i< allLeaves.size(); i++)
    {
      TreeNode* leaf = allLeaves[i];
      if (leaf->af->getVal(leaf->weight) <tol*_root->weight)
      {
        allLeavesWillRemove.push_back(leaf);
      }
    }
    if (allLeavesWillRemove.size() == 0)
    {
      Msg::Info("done removing !!! ");
      return ok; 
    }
    else
    {
      Msg::Info("iter = %d /%d",it,maxInter);
      ok = true;
    }
    for (int i=0; i< allLeavesWillRemove.size(); i++)
    {
      TreeNode* leaf = allLeavesWillRemove[i];
      TreeNode* parent = leaf->parent;
      if (parent->childs.size() ==2)
      {
        TreeNode* grandParent = parent->parent;
        TreeNode* otherChild = NULL;
        if (leaf->childOrder == 0)
        {
          otherChild =  parent->childs[1];
        }
        else
        {
          otherChild =  parent->childs[0];
        }
        
        otherChild->depth = parent->depth;
        otherChild->location = parent->location;
        otherChild->parent = parent->parent;
        otherChild->childOrder = parent->childOrder;
        grandParent->childs[parent->childOrder] = otherChild;
        
        // decrease all associated nodes by 1
        std::vector<TreeNode*> allAssociatedNodesToChild;
        getRefToAssociatedNodes(otherChild,allAssociatedNodesToChild);
        for (int j=0; j< allAssociatedNodesToChild.size(); j++)
        {
          std::vector<int> loc;
          getMaxLocationByDepth(loc);
          allAssociatedNodesToChild[j]->depth--;
          allAssociatedNodesToChild[j]->location=loc[allAssociatedNodesToChild[j]->depth]+1;
        };
        
        Msg::Info("child weight = %e otherChild weight = %e parent weight = %e",leaf->af->getVal(leaf->weight),otherChild->af->getVal(otherChild->weight),parent->weight);
      }
      else if (parent->childs.size() > 2)
      {
        Msg::Info("----------------------------------");
        parent->printData();
        //
        std::vector<TreeNode*> newChilds;
        std::vector<double> newDirection;
        //
        int numberNormal = parent->childs.size()-1;
        int totalNumberDirVars = parent->direction.size();
        int numberVarsPerNormal = totalNumberDirVars/numberNormal;
        
        for (int j=0; j< parent->childs.size(); j++)
        {
          if (parent->childs[j]->location != leaf->location)
          {
            newChilds.push_back(parent->childs[j]);
            if (newChilds.size() > 1)
            {
              if (numberVarsPerNormal == 1)
              {
                newDirection.push_back(parent->direction[j-1]);
              }
              else if (numberVarsPerNormal == 2)
              {
                newDirection.push_back(parent->direction[(j-1)*numberVarsPerNormal]);
                newDirection.push_back(parent->direction[(j-1)*numberVarsPerNormal+1]);
              }
              else
              {
                Msg::Error("numberVarsPerNormal = %d is not correct !!!",numberVarsPerNormal);
                Msg::Exit(0);
              }
            }
          }
        }
        Msg::Info("\nSTART removing ");
        parent->printData();
        parent->childs = newChilds;
        for (int j=0; j <parent->childs.size(); j++)
        {
          parent->childs[j]->childOrder = j;
        }
        parent->direction = newDirection;   
        Msg::Info("------------------");
        parent->printData();     
        Msg::Info("DONE removing\n");
      }
      else
      {
        Msg::Error("num childs must be larger than 2");
        Msg::Exit(0);
      }
    };
    
    if (!iteration)
    {
      break;
    }
  };
  return false;
};

bool Tree::treeMerging()
{
  bool ok = false;
  int itMerge = 0;
  int maxInter = 100;
  while (true)
  {
    itMerge ++;
    if (itMerge == maxInter)
    {
      Msg::Error("maximal number of iterations (%d) reaches", maxInter);
      Msg::Exit(0);
    }
    std::vector<TreeNode*> allLeaves;
    getRefToAllLeaves(allLeaves);
    std::set<TreeNode*> allParents;
    std::set<TreeNode*> allParentsOneChild;
    for (int i=0; i< allLeaves.size(); i++)
    {
      TreeNode* leaf = allLeaves[i];
      TreeNode* parent = leaf->parent;
      if (parent->childs.size() > 1)
      {
        std::map<int, std::vector<TreeNode*> > mapIndexes;
        for (int j=0; j< parent->childs.size(); j++)
        {
          std::vector<TreeNode*>& cc = mapIndexes[parent->childs[j]->matIndex];      
          cc.push_back(parent->childs[j]);
        }
        for (std::map<int, std::vector<TreeNode*> >::iterator it = mapIndexes.begin(); it != mapIndexes.end(); it++)
        {
          int mIndex = it->first;
          if (mIndex >=0)
          {
            std::vector<TreeNode*>& sameMat = it->second;
            if (sameMat.size()> 1)
            {
              allParents.insert(parent);
            }
          }
        }
      }
      else if (parent->childs.size() == 1)
      {
        allParentsOneChild.insert(parent);
      }
    }
    
    if (allParents.size() == 0 && allParentsOneChild.size() == 0)
    {
      Msg::Info("done tree merging");
      return ok;
    }
    else
    {
      Msg::Info("tree merging %d / %d",itMerge,maxInter);
      ok = true;
    }
    
    for (std::set<TreeNode*>::iterator itp = allParentsOneChild.begin(); itp != allParentsOneChild.end(); itp++)
    {
      TreeNode* parent = *itp;
      TreeNode* node = parent->childs[0];
      TreeNode* grandParent = parent->parent;
      //
      node->depth = parent->depth;
      node->location = parent->location;
      node->parent = parent->parent;
      node->childOrder = parent->childOrder;
      grandParent->childs[parent->childOrder] = node;
      
      //
      // decrease all associated nodes by 1
      std::vector<TreeNode*> allAssociatedNodesToChild;
      getRefToAssociatedNodes(node,allAssociatedNodesToChild);
      for (int j=0; j< allAssociatedNodesToChild.size(); j++)
      {
        std::vector<int> loc;
        getMaxLocationByDepth(loc);
        allAssociatedNodesToChild[j]->depth--;
        allAssociatedNodesToChild[j]->location=loc[allAssociatedNodesToChild[j]->depth]+1;
      };
    };
    
    for (std::set<TreeNode*>::iterator itp = allParents.begin(); itp != allParents.end(); itp++)
    {
      TreeNode* parent = *itp;
      std::map<int, std::vector<std::pair<TreeNode*, int> > > mapIndexes;
      for (int j=0; j< parent->childs.size(); j++)
      {
        std::vector<std::pair<TreeNode*, int > >& cc = mapIndexes[parent->childs[j]->matIndex];      
        cc.push_back(std::pair<TreeNode*, int>(parent->childs[j],j));
      }
      for (std::map<int, std::vector<std::pair<TreeNode*, int> > >::iterator it = mapIndexes.begin(); it != mapIndexes.end(); it++)
      {
        int mIndex = it->first;
        std::vector<std::pair<TreeNode*, int> >& sameMat = it->second;
        if ((mIndex >=0) && (sameMat.size()> 1))
        {
          std::vector<TreeNode*> nodes;
          std::set<int> position;
          for (int k=0; k< sameMat.size(); k++)
          {
            nodes.push_back(sameMat[k].first);
            if (k > 0)
            {
              position.insert(sameMat[k].second);
            }
          };
          
          double totalW = 0.;
          for (int k=0; k< nodes.size(); k++)
          {
            totalW += nodes[k]->af->getVal(nodes[k]->weight);
          };
          nodes[0]->weight = nodes[0]->af->getReciprocalVal(totalW);
          
          int numberNormal = parent->childs.size()-1;
          int totalNumberDirVars = parent->direction.size();
          int numberVarsPerNormal = totalNumberDirVars/numberNormal;
        
          std::vector<TreeNode*> newChilds;
          std::vector<double> newNormals;
          for (int j=0; j< parent->childs.size(); j++)
          {
            if (position.find(j) == position.end())
            {
              newChilds.push_back(parent->childs[j]);
              if (j > 0)
              {
                if (numberVarsPerNormal == 1)
                {
                  newNormals.push_back(parent->direction[j-1]);
                }
                else if (numberVarsPerNormal == 2)
                {
                  newNormals.push_back(parent->direction[(j-1)*numberVarsPerNormal]);
                  newNormals.push_back(parent->direction[(j-1)*numberVarsPerNormal+1]);
                }              
              }
            }
          };
          
          Msg::Info("\nSTART merging ");
          parent->printData();
          printf("merge loc (number of phases = %ld): %d (mat %d) ",parent->childs.size(),sameMat[0].second, (sameMat[0].first)->matIndex);
          for (std::set<int>::iterator itpos = position.begin(); itpos != position.end(); itpos++)
          {
            printf(" %d (mat %d)",*itpos,parent->childs[*itpos]->matIndex);
          }
          printf("\n----------------------\n");
          parent->childs = newChilds;
          for (int j=0; j <parent->childs.size(); j++)
          {
            parent->childs[j]->childOrder = j;
          }
          parent->direction = newNormals;
          parent->printData();
          Msg::Info("DONE merging\n");
          //
          break;
        }
      }
    };
  }
  return false;
};

bool Tree::removeZeroBranches(double tol)
{
  int depth = 0;
  bool ok = false;
  while (true)
  {
    std::map<int,std::vector<TreeNode*> > allNodesByDepth;
    getAllNodesByDepth(allNodesByDepth);
    std::map<int,std::vector<TreeNode*> >::iterator itF= allNodesByDepth.find(depth);
    if ( itF!= allNodesByDepth.end())
    {
      std::vector<TreeNode*>& depthNodes = itF->second;
      for (int i=0; i<depthNodes.size(); i++)
      {
        TreeNode* node = depthNodes[i];
        double Wt = getNonNegativeWeight(node);
        //
        bool removed = false;
        std::vector<TreeNode*> newChilds;
        std::vector<double> newDirection;
        std::vector<int> removeLoc;
        //
        int numberNormal = node->childs.size()-1;
        int totalNumberDirVars = node->direction.size();
        int numberVarsPerNormal = totalNumberDirVars/numberNormal;
        
        for (int j=0; j< node->childs.size(); j++)
        {
          double Wchild = getNonNegativeWeight(node->childs[j]);
          if (Wchild < tol* Wt)
          {
            removed = true;
            removeLoc.push_back(j);
          }
          else
          {
            newChilds.push_back(node->childs[j]);
            if (newChilds.size() > 1)
            {
              if (numberVarsPerNormal == 1)
              {
                newDirection.push_back(node->direction[j-1]);
              }
              else if (numberVarsPerNormal == 2)
              {
                newDirection.push_back(node->direction[(j-1)*numberVarsPerNormal]);
                newDirection.push_back(node->direction[(j-1)*numberVarsPerNormal+1]);
              }
              else
              {
                Msg::Error("numberVarsPerNormal = %d is not correct !!!",numberVarsPerNormal);
                Msg::Exit(0);
              }
            }
          }
        }
        if (removed)
        {
          Msg::Info("\nSTART removing ");
          node->printData();
          ok = true;
          printf("pos (number of phases= %ld): ",node->childs.size());
          for (int j=0; j< removeLoc.size(); j++)
          {
            printf("%d (f=%e) ",removeLoc[j],getNonNegativeWeight(node->childs[removeLoc[j]])/Wt);
          }
          printf("\n");
          if (newChilds.size() >1)
          {
            node->childs = newChilds;
            for (int j=0; j <node->childs.size(); j++)
            {
              node->childs[j]->childOrder = j;
            }
            node->direction = newDirection;   
            Msg::Info("------------------");
            node->printData();     
          }
          else
          {
            TreeNode* grandParent = node->parent;
            TreeNode* child = newChilds[0];
            //
            child->depth = node->depth;
            child->location = node->location;
            child->parent = node->parent;
            child->childOrder = node->childOrder;
            grandParent->childs[node->childOrder] = child;
            // decrease all associated nodes by 1
            std::vector<TreeNode*> allAssociatedNodesToChild;
            getRefToAssociatedNodes(child,allAssociatedNodesToChild);
            for (int j=0; j< allAssociatedNodesToChild.size(); j++)
            {
              std::vector<int> loc;
              getMaxLocationByDepth(loc);
              allAssociatedNodesToChild[j]->depth--;
              allAssociatedNodesToChild[j]->location=loc[allAssociatedNodesToChild[j]->depth]+1;
            };
            
            Msg::Info("------------------");
            Msg::Info("node is replaced by child");
          }
          Msg::Info("DONE removing\n");
        }
      }
    }
    else
    {
      break;
    }
    depth++;
  };
  return ok;
};

bool Tree::removeLeavesWithZeroContribution(double tol)
{
  int iterRemove = 0;
  int maxIterRemove = 100;
  bool ok = false;
  while (true)
  {
    iterRemove ++;
    if (iterRemove == maxIterRemove)
    {
      Msg::Error("removeLeavesWithZeroContribution: maximal number of iterations (%d) reaches", maxIterRemove);
      Msg::Exit(0);
    }
    //bool okLeaf = removeZeroBranches(tol);
    bool okLeaf = removeZeroLeaves(tol,true);
    bool okMerge = treeMerging();
    // check
    if (okLeaf || okLeaf)
    {
      ok = true;
    }
    
    if ((!okLeaf)&& (!okMerge))
    {
      Msg::Info("done removing");
      return ok;
    }
    else
    {
      Msg::Info("removing %d / %d",iterRemove,maxIterRemove);
    }
    
  }
  return false;
};

double Tree::getPhaseFraction(int i) const
{
  nodeContainer allLeaves;
  getAllLeaves(allLeaves);
  std::map<int, double> mapFraction;
  double total = 0.;
  for (int i=0; i< allLeaves.size(); i++)
  {
    const TreeNode* node = allLeaves[i];
    mapFraction[node->matIndex] += node->af->getVal(node->weight);
    total += node->af->getVal(node->weight);
  }
  return mapFraction[i]/total;
};

void Tree::printNodeFraction() const
{
  nodeContainer allLeaves;
  getAllLeaves(allLeaves);
  double total = 0;
  for (int i=0; i< allLeaves.size(); i++)
  {
    const TreeNode* node = allLeaves[i];
    Msg::Info("fraction node %d = %e",node->getNodeId(), node->af->getVal(node->weight)/_root->weight);
    total += node->af->getVal(node->weight);
  }
  Msg::Info("Wroot = %e, total = %e",_root->weight,total);
};

void Tree::printPhaseFraction() const
{
  nodeContainer allLeaves;
  getAllLeaves(allLeaves);
  std::map<int, double> mapFraction;
  double total = 0.;
  for (int i=0; i< allLeaves.size(); i++)
  {
    const TreeNode* node = allLeaves[i];
    mapFraction[node->matIndex] += node->af->getVal(node->weight);
    total += node->af->getVal(node->weight);
  }
  fullVector<double> allPhase(mapFraction.size());
  for (int j=0; j< allPhase.size(); j++)
  {
    allPhase(j) = mapFraction[j]/total;
  }
  allPhase.print("all phase fraction:"); 
};

void Tree::printLeafFraction() const
{
  nodeContainer allLeaves;
  getAllLeaves(allLeaves);
  double total = getNonNegativeWeight(_root);
  printf("print fraction at leaf:\n");
  int ord=0;
  for (int i=0; i< allLeaves.size(); i++)
  {
    const TreeNode* leaf = allLeaves[i];
    printf("loc %d: %d %d, fraction = %.5e\n",ord,leaf->depth,leaf->location,getNonNegativeWeight(leaf)/total);
    ord++;
  }
  printf("done printing:\n"); 
};

void Tree::printTree() const
{
  printf("Tree print \n");
  if (_root == NULL) return;
  std::vector< TreeNode*> listPrev(0), listCur(0);
  listPrev.push_back(_root);
  int depth = 0;
  while (true)
  {
    listCur.clear();
    printf("depth %d:\n",depth);
    for (int i =0; i< listPrev.size(); i++)
    {
      TreeNode& np = *(listPrev[i]);
      np.printData();
      for (int j=0; j< np.childs.size(); j++)
      {
        listCur.push_back(np.childs[j]);
      }
    }
    printf("\n");
    listPrev = listCur;
    if (listCur.size() == 0)
    {
      break;
    }
    depth++;
  };
  
  nodeContainer allLeaves;
  getAllLeaves(allLeaves);
  printf("all leaves = %ld\n",allLeaves.size());
  for (int i=0; i< allLeaves.size(); i++)
  {
    allLeaves[i]->printData();
  }
  printf("\n");
};

void Tree::printTreeInteraction(const std::string fname, bool colorMat, int dir) const
{
  printf("Tree print \n");
  if (_root == NULL) return;
  std::vector< TreeNode*> listPrev, listCur;
  listPrev.push_back(_root);
  nodeContainer allLeaves;
  getAssociatedLeavesForNode(_root,allLeaves);
  printf("*|%ld|*\n",allLeaves.size());
  while (true)
  {
    listCur.clear();
    for (int i =0; i< listPrev.size(); i++)
    {
      printf("*|");
      TreeNode& np = *(listPrev[i]);
      for (int j=0; j< np.childs.size(); j++)
      {
        TreeNode* nj = np.childs[j];
        allLeaves.clear();
        getAssociatedLeavesForNode(nj,allLeaves);
        if (allLeaves.size() == 1)
        {
          if (j==0)
          {
            printf("%ld(%d)",allLeaves.size(),nj->matIndex);
          }
          else
          {
            printf("-%ld(%d)",allLeaves.size(),nj->matIndex);
          }
        }
        else
        {
          if (j==0)
          {
            printf("%ld",allLeaves.size());
          }
          else
          {
            printf("-%ld",allLeaves.size());
          }
          listCur.push_back(nj);
        }
      }
      printf("|*");
    }
    printf("\n");
    listPrev = listCur;
    if (listCur.size() == 0)
    {
      break;
    }
  };
  
  
  std::map<const TreeNode*, std::string> nameMap;
  nodeContainer allNodes;
  getAllNodes(allNodes);
  for (int i=0; i< allNodes.size(); i++)
  {
    nameMap[allNodes[i]] = "n00"+std::to_string(i+2);
  };
  
  std::string txt ="##Command to get the layout: \"dot  -Teps "+fname+" > treeInteraction.eps\"\n";
  txt += "graph \"\" \n{node [fontsize=60];\ngraph[fontsize=50,style=invis,ratio=1];\nsubgraph cluster01\n{";
  
  
  for (std::map<const TreeNode*, std::string>::iterator it = nameMap.begin(); it != nameMap.end(); it++)
  {
    allLeaves.clear();
    const TreeNode* node = it->first;
    getAssociatedLeavesForNode(node,allLeaves);
    std::string label;
    if (node->childs.size() > 0)
    {
      label = " [label=\""+ std::to_string(allLeaves.size())+"\"];\n";
    }
    else
    {
      if (colorMat)
      {
        if (node->matIndex == 0)
          label = " [label=\""+ std::to_string(allLeaves.size()) +"\",color=red,style=filled];\n";
        else if (node->matIndex == 1)
          label = " [label=\""+ std::to_string(allLeaves.size()) +"\",color=green,style=filled];\n";
        else if (node->matIndex == 2)
          label = " [label=\""+ std::to_string(allLeaves.size()) +"\",color=cyan,style=filled];\n";
        else if (node->matIndex == 3)
          label = " [label=\""+ std::to_string(allLeaves.size()) +"\",color=orange,style=filled];\n";
        else
        {
          label = " [label=\""+ std::to_string(allLeaves.size()) +"\",color=blue,style=filled];\n";
        }
      }
      else
      {
        label = " [label=\""+ std::to_string(allLeaves.size()) +"\",color=azure4,style=filled];\n";
      }
    }
    txt += it->second+label;
  }
  // connection
  txt += nameMap[_root]+"\n";
  for (std::map<const TreeNode*, std::string>::iterator it = nameMap.begin(); it != nameMap.end(); it++)
  {
    const TreeNode* node = it->first;
    for (int i=0; i< node->childs.size(); i++)
    {
      const TreeNode* conn = node->childs[i];
      if (dir == 0)
      {
        if (conn->childs.size() > 0)
          txt += nameMap[node] + " -- " + nameMap[conn]+"[dir=forward,arrowsize=2];\n";
        else
          txt += nameMap[node] + " -- " + nameMap[conn]+"[dir=forward,arrowsize=2];\n";
      }
      else
      {
        if (conn->childs.size() > 0)
          txt += nameMap[node] + " -- " + nameMap[conn]+"[dir=back,arrowsize=2];\n";
        else
          txt += nameMap[node] + " -- " + nameMap[conn]+"[dir=back,arrowsize=2];\n";
      }
    }
  }
 
  txt += "}\n}";
  FILE* ff = fopen(fname.c_str(),"w");
  fprintf(ff,"%s",txt.c_str());
  fclose(ff); 
  
};

void Tree::getAllLeaves(nodeContainer& allLeaves) const
{
  allLeaves.clear();
  if (_root == NULL) return;
  nodeContainer listPrev, listCur;
  listPrev.push_back(_root);
  while (true)
  {
    listCur.clear();    
    for (int i =0; i< listPrev.size(); i++)
    {
      const TreeNode* np = listPrev[i];
      if (np->childs.size() == 0)
      {
        allLeaves.push_back(np);
      }
      for (int j=0; j< np->childs.size(); j++)
      {
        listCur.push_back(np->childs[j]);
      }
    }
    listPrev = listCur;
    if (listCur.size() == 0)
    {
      break;
    }
  };
};

void Tree::getRefToAllLeaves(std::vector<TreeNode*>& allLeaves)
{
  allLeaves.clear();
  if (_root == NULL) return;
  std::vector<TreeNode*> listPrev, listCur;
  listPrev.push_back(_root);
  while (true)
  {
    listCur.clear();    
    for (int i =0; i< listPrev.size(); i++)
    {
      TreeNode* np = listPrev[i];
      if (np->childs.size() == 0)
      {
        allLeaves.push_back(np);
      }
      for (int j=0; j< np->childs.size(); j++)
      {
        listCur.push_back(np->childs[j]);
      }
    }
    listPrev = listCur;
    if (listCur.size() == 0)
    {
      break;
    }
  };
};

void Tree::getAllNodesByDepth(std::map<int,std::vector<TreeNode*> >& allNodes)
{
  allNodes.clear();
  if (_root == NULL) return;
  std::vector<TreeNode*> listPrev, listCur;
  listPrev.push_back(_root);
  //
  allNodes[_root->depth].push_back(_root);
  while (true)
  {
    listCur.clear();    
    for (int i =0; i< listPrev.size(); i++)
    {
      TreeNode* np = listPrev[i];
      for (int j=0; j< np->childs.size(); j++)
      {
        listCur.push_back(np->childs[j]);
        allNodes[np->childs[j]->depth].push_back(np->childs[j]);
      }
    }
    listPrev = listCur;
    if (listCur.size() == 0)
    {
      break;
    }
  };
};

void Tree::getMaxLocationByDepth(std::vector<int>& loc) const
{
  std::map<int,nodeContainer > allNodes;
  getAllNodesByDepth(allNodes);
  loc.resize(allNodes.size(),0);
  for (std::map<int,nodeContainer >::const_iterator it = allNodes.begin(); it != allNodes.end(); it++)
  {
    const nodeContainer& depthNode = it->second;
    for (int j=0; j< depthNode.size(); j++)
    {
      const TreeNode* node = depthNode[j];
      if (node->location >loc[it->first])
      {
        loc[it->first]= node->location;
      }
    }
  }
};

void Tree::getAllNodesByDepth(std::map<int,nodeContainer >& allNodes) const
{
  allNodes.clear();
  if (_root == NULL) return;
  nodeContainer listPrev, listCur;
  listPrev.push_back(_root);
  //
  allNodes[_root->depth].push_back(_root);
  while (true)
  {
    listCur.clear();    
    for (int i =0; i< listPrev.size(); i++)
    {
      const TreeNode* np = listPrev[i];
      for (int j=0; j< np->childs.size(); j++)
      {
        listCur.push_back(np->childs[j]);
        allNodes[np->childs[j]->depth].push_back(np->childs[j]);
      }
    }
    listPrev = listCur;
    if (listCur.size() == 0)
    {
      break;
    }
  };
};


void Tree::printBackTrackingPath(const TreeNode* node) const
{
  nodeContainer allNodes;
  getBackTrackingPath(node,allNodes);
  printf("-----------------------------\n");
  node->printData();
  printf("backtracking laves to node: %ld \n",allNodes.size());
  for (int i=0; i< allNodes.size(); i++)
  {
    allNodes[i]->printData();
  }
  printf("-----------------------------\n");
};

void Tree::printAssociatedLeavesForNode(const TreeNode* node) const
{
  nodeContainer allLeaves;
  getAssociatedLeavesForNode(node,allLeaves);
  
  printf("-----------------------------\n");
  node->printData();
  printf("associated laves to node: %ld \n",allLeaves.size());
  for (int i=0; i< allLeaves.size(); i++)
  {
    allLeaves[i]->printData();
  }
  printf("-----------------------------\n");
};

void Tree::getBackTrackingPath(const TreeNode* node, nodeContainer& allNodes) const
{
  allNodes.clear();
  const TreeNode* v = node;
  allNodes.push_back(v);
  while (v->parent != NULL)
  {
    allNodes.push_back(v->parent);
    v = v->parent;
  };
};

void Tree::getAssociatedLeavesForNode(TreeNode* node, std::vector<TreeNode*>& allLeaves)
{
  allLeaves.clear();
  std::vector<TreeNode*> listPrev, listCur;
  listPrev.push_back(node);
  while (true)
  {
    listCur.clear();    
    for (int i =0; i< listPrev.size(); i++)
    {
      TreeNode* np = listPrev[i];
      if (np->childs.size() == 0)
      {
        allLeaves.push_back(np);
      }
      for (int j=0; j< np->childs.size(); j++)
      {
        listCur.push_back(np->childs[j]);
      }
    }
    listPrev = listCur;
    if (listCur.size() == 0)
    {
      break;
    }
  };
};

double Tree::getNonNegativeWeight(const TreeNode* node) const
{
  if (node->childs.size() == 0)
  {
    return node->af->getVal(node->weight);
  }
  else
  {
    nodeContainer allLeaves;
    getAssociatedLeavesForNode(node,allLeaves);
    double W = 0;
    for (int i=0; i< allLeaves.size(); i++)
    {
      const TreeNode* leaf = allLeaves[i];
      W += leaf->af->getVal(leaf->weight);
    };
    return W;
  }
}

void Tree::getAssociatedLeavesForNode(const TreeNode* node, nodeContainer& allLeaves) const
{
  allLeaves.clear();
  nodeContainer listPrev, listCur;
  listPrev.push_back(node);
  while (true)
  {
    listCur.clear();    
    for (int i =0; i< listPrev.size(); i++)
    {
      const TreeNode* np = listPrev[i];
      if (np->childs.size() == 0)
      {
        allLeaves.push_back(np);
      }
      for (int j=0; j< np->childs.size(); j++)
      {
        listCur.push_back(np->childs[j]);
      }
    }
    listPrev = listCur;
    if (listCur.size() == 0)
    {
      break;
    }
  };
};


void Tree::initializeCohesiveParameters(double minCoef, double maxCoef)
{
  // random weight for leaves
  unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
  std::default_random_engine generator(seed);
  std::uniform_real_distribution<double> distribution(0., 1.); 
  
  auto getVal = [&](double min, double max)
  {
    double  x= distribution(generator);
    return (max-min)*x + min;
  };
  
  std::vector<TreeNode*> allLeaves;
  getRefToAllLeaves(allLeaves);
  for (int i=0; i< allLeaves.size(); i++)
  {
    TreeNode* leaf = allLeaves[i];
    if (leaf->withCohesive())
    {
      int numCoh = leaf->cohesiveCoeffVars.size();
      int numDirVars = leaf->cohesiveDirectionVars.size2();
      for (int k=0; k< numCoh; k++)
      {
        leaf->cohesiveCoeffVars(k) = getVal(minCoef,maxCoef);     
        for (int j=0; j< numDirVars; j++)
        {
           leaf->cohesiveDirectionVars(k,j) = getVal(0,1.);
        };
      }
    }
  }
};

void Tree::setCohesiveParametersFromInteraction(int phaseIndex, double minCoef, double maxCoef, const std::string affunc)
{
  // random weight for leaves
  unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
  std::default_random_engine generator(seed);
  std::uniform_real_distribution<double> distribution(0., 1.); 
  
  auto getVal = [&](double min, double max)
  {
    double  x= distribution(generator);
    return (max-min)*x + min;
  };
  //
  NetworkInteraction iteraction;
  iteraction.getInteractionFromTree(*this,false);
  const NetworkInteraction::NodeMapContainer& nodeMapInteraction = iteraction.getNodeMapContainer();  
  std::vector<TreeNode*> allLeaves;
  getRefToAllLeaves(allLeaves);
  int maxLaw = 0;
  std::vector<TreeNode*> cohesiveLeaves;
  for (int i=0; i< allLeaves.size(); i++)
  {
    TreeNode* leaf = allLeaves[i];
    if (maxLaw < leaf->matIndex) maxLaw=leaf->matIndex;
    if (leaf->matIndex == phaseIndex)
    {
      cohesiveLeaves.push_back(leaf);
      const MaterialNode* nodeInter = iteraction.getMaterialNode(leaf->getNodeId());
      #if 0
      const std::vector<const InteractionMechanism*>& interactionForNode = *(iteraction.getInteractionsForNode(nodeInter));
      #else
      const std::vector<const InteractionMechanism*>& interactionForNodeBB = *(iteraction.getInteractionsForNode(nodeInter));
      std::vector<const InteractionMechanism*> interactionForNode;
      for (int i=0; i< interactionForNodeBB.size(); i++)
      {
        if (interactionForNodeBB[i]->getNumMaterialNodes() == 2)
        {
          interactionForNode.push_back(interactionForNodeBB[i]);
        }
      }
      #endif //0
      int numCoh = interactionForNode.size();
      if (numCoh > 0)
      {
        const InteractionMechanism* inter0 = interactionForNode[0];
        int numCohVars = inter0->getDirectionVars().size();
        leaf->allocateCohesiveParameters(numCoh,numCohVars);
        for (int k=0; k< numCoh; k++)
        {
          leaf->cohesiveCoeffVars(k) = getVal(minCoef,maxCoef); 
          for (int l=0; l< numCohVars; l++)
          {
            leaf->cohesiveDirectionVars(k,l) = interactionForNode[k]->getDirectionVars()[l];
          }
        }
        if (leaf->cohesiveAf != NULL)
        {
          delete leaf->cohesiveAf;
        }
        leaf->cohesiveAf = activationFunction::createActivationFunction(affunc.c_str());
      }
    }
  }
  for (int i=0; i< cohesiveLeaves.size(); i++)
  {
    cohesiveLeaves[i]->cohesiveMatIndex = maxLaw+1;
  }
};

void Tree::cohesiveEnrichement(int phaseIndex, int numCoh, int numDirVars, const std::string affunc, bool init)
{
  //
  std::vector<TreeNode*> allLeaves;
  getRefToAllLeaves(allLeaves);
  
  int maxLaw = 0;
  std::vector<TreeNode*> cohesiveLeaves;
  for (int i=0; i< allLeaves.size(); i++)
  {
    TreeNode* leaf = allLeaves[i];
    if (maxLaw < leaf->matIndex)
    {
      maxLaw = leaf->matIndex;
    }
    if (leaf->matIndex == phaseIndex)
    {
      cohesiveLeaves.push_back(leaf);
      leaf->allocateCohesiveParameters(numCoh,numDirVars);
      
      if (leaf->cohesiveAf != NULL)
      {
        delete leaf->cohesiveAf;
      }
      leaf->cohesiveAf = activationFunction::createActivationFunction(affunc.c_str());
    }
  }
  if (init)
  {
    initializeCohesiveParameters(1e-1,1e1);
  }
  for (int i=0; i< cohesiveLeaves.size(); i++)
  {
    cohesiveLeaves[i]->cohesiveMatIndex = maxLaw+1;
  }
};

void Tree::assignMaterialLaws(int numPhases)
{
  std::vector<TreeNode*> allLeaves;
  getRefToAllLeaves(allLeaves);
  
  for (int i=0; i< allLeaves.size(); i++)
  {
    TreeNode* leaf = allLeaves[i];
    leaf->matIndex = -1;
  }
  
  std::set<TreeNode*> setNodes;
  for (int i=0; i< allLeaves.size(); i++)
  {
    TreeNode* leaf = allLeaves[i];
    if (setNodes.find(leaf) == setNodes.end())
    {
      TreeNode* parent = leaf->parent;
      std::vector<TreeNode*> ll;
      for (int j=0; j< parent->childs.size(); j++)
      {
        TreeNode* nj = parent->childs[j];
        if (nj->childs.size() == 0)
        {
          ll.push_back(nj);
          setNodes.insert(nj);
        }
      }
      if (ll.size() == numPhases)
      {
        for (int j=0; j< ll.size(); j++)
        {
          ll[j]->matIndex  = j;
        }
      }
      else
      {
        std::vector<int> v(numPhases,0);
        for (int j=0; j< numPhases; j++)
        {
          v[j] = j;
        }
        for (int j=0; j< ll.size(); j++)
        {
          int pos =  rand() % (v.size()); // random from 0 to v.size() -1
          TreeNode* otherNode = ll[j];
          otherNode->matIndex = v[pos];
          v.erase(v.begin() + pos);
        }
      }
    }
  }
}

void Tree::printAllRegularNodes() const
{
  nodeContainer allNodes;
  getAllRegularNodes(allNodes);
  printf("-----------------------------\n");
  printf("all regular nodes: %ld \n",allNodes.size());
  for (int i=0; i< allNodes.size(); i++)
  {
    allNodes[i]->printData();
  }
  printf("-----------------------------\n");
  
};

void Tree::getRefToAllRegularNodes(std::vector<TreeNode*>& allNodes)
{
  allNodes.clear();
  if (_root == NULL) return;
  std::vector<TreeNode*> listPrev, listCur;
  listPrev.push_back(_root);
  //
  while (true)
  {
    listCur.clear();    
    for (int i =0; i< listPrev.size(); i++)
    {
      TreeNode* np = listPrev[i];
      if (np->childs.size() > 0)
      {
        allNodes.push_back(np);
      }
      //
      for (int j=0; j< np->childs.size(); j++)
      {
        listCur.push_back(np->childs[j]);
      }
    }
    listPrev = listCur;
    if (listCur.size() == 0)
    {
      break;
    }
  };
}

void Tree::getAllRegularNodes(nodeContainer& allNodes) const
{
  allNodes.clear();
  if (_root == NULL) return;
  nodeContainer listPrev, listCur;
  listPrev.push_back(_root);
  //
  while (true)
  {
    listCur.clear();    
    for (int i =0; i< listPrev.size(); i++)
    {
      const TreeNode* np = listPrev[i];
      if (np->childs.size() > 0)
      {
        allNodes.push_back(np);
      }
      //
      for (int j=0; j< np->childs.size(); j++)
      {
        listCur.push_back(np->childs[j]);
      }
    }
    listPrev = listCur;
    if (listCur.size() == 0)
    {
      break;
    }
  };
};

void Tree::getAllNodes(nodeContainer& allNodes) const
{
  allNodes.clear();
  if (_root == NULL) return;
  nodeContainer listPrev, listCur;
  listPrev.push_back(_root);
  //
  allNodes.push_back(_root);
  while (true)
  {
    listCur.clear();    
    for (int i =0; i< listPrev.size(); i++)
    {
      const TreeNode* np = listPrev[i];
      for (int j=0; j< np->childs.size(); j++)
      {
        listCur.push_back(np->childs[j]);
      }
    }
    listPrev = listCur;
    if (listCur.size() == 0)
    {
      break;
    }
    else
    {
      allNodes.insert(allNodes.end(),listCur.begin(),listCur.end());
    }
  };
};

void Tree::getRefToAssociatedNodes(TreeNode* node, std::vector<TreeNode*>& allNodes)
{
  allNodes.clear();
  std::vector<TreeNode*> listPrev, listCur;
  listPrev.push_back(node);
  //
  while (true)
  {
    listCur.clear();    
    for (int i =0; i< listPrev.size(); i++)
    {
      TreeNode* np = listPrev[i];
      for (int j=0; j< np->childs.size(); j++)
      {
        listCur.push_back(np->childs[j]);
      }
    }
    listPrev = listCur;
    if (listCur.size() == 0)
    {
      break;
    }
    else
    {
      allNodes.insert(allNodes.end(),listCur.begin(),listCur.end());
    }
  };
}

void Tree::getRefToAllNodes(std::vector<TreeNode*>& allNodes)
{
  allNodes.clear();
  if (_root == NULL) return;
  std::vector<TreeNode*> listPrev, listCur;
  listPrev.push_back(_root);
  //
  allNodes.push_back(_root);
  while (true)
  {
    listCur.clear();    
    for (int i =0; i< listPrev.size(); i++)
    {
      TreeNode* np = listPrev[i];
      for (int j=0; j< np->childs.size(); j++)
      {
        listCur.push_back(np->childs[j]);
      }
    }
    listPrev = listCur;
    if (listCur.size() == 0)
    {
      break;
    }
    else
    {
      allNodes.insert(allNodes.end(),listCur.begin(),listCur.end());
    }
  };
};


void Tree::clear()
{
  // clear
  std::vector<TreeNode*> allNodes;
  getRefToAllNodes(allNodes);
  for (int i=0; i< allNodes.size(); i++)
  {
    delete allNodes[i];
  }
  _root = NULL;
};

void Tree::initializeWithInitialFractions(const fullVector<double>& phaseFraction)
{
  // random weight for leaves
  unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
  std::default_random_engine generator(seed);
  std::uniform_real_distribution<double> distribution(0., 1.); 
  
  auto getVal = [&](double min, double max)
  {
    double  x= distribution(generator);
    return (max-min)*x + min;
  };
  
  //
  std::vector<TreeNode*> allLeaves;
  getRefToAllLeaves(allLeaves);
  int numberMaterialNodes = allLeaves.size();
  // create N material node
  double minWeight = 0.1;
  double maxWeight = 1.;
  bool random_ = true;
  std::vector<double> randomPositives(numberMaterialNodes,0.);
  for (int i=0; i< numberMaterialNodes; i++)
  {
    if (random_)
    {
      randomPositives[i] = getVal(minWeight,maxWeight);
    }
    else
    {
      randomPositives[i]  = 0.5;
    }
  };
  int numPhases = phaseFraction.size();
  std::vector<double> sumPhaseWeights(numPhases,0.);
  for (int i=0; i< allLeaves.size(); i++)
  {
    TreeNode* n = allLeaves[i];
    int iphase = n->matIndex;
    sumPhaseWeights[iphase] += randomPositives[i];
  }
  
  for (int i=0; i< allLeaves.size(); i++)
  {
    TreeNode* n = allLeaves[i];
    int iphase = n->matIndex;
    n->weight = n->af->getReciprocalVal(phaseFraction(iphase)*randomPositives[i]/sumPhaseWeights[iphase]);
  }
  
  for (int i=0; i< allLeaves.size(); i++)
  {
    TreeNode* n = allLeaves[i];
    // propagate data
    double w = n->weight;
    TreeNode* parent = n->parent;
    while (parent != NULL)
    {
      parent->weight += n->af->getVal(w);
      parent = parent->parent;
    };
  };
  //printf("pi = %e\n",pi);
  double minAlpha = 0.;
  double maxAlpha = 1;
  //
  std::vector<TreeNode*> allNodes;
  getRefToAllNodes(allNodes);
  printf("num of nodes = %ld\n",allNodes.size());
  for (int i=0; i< allNodes.size(); i++)
  {
    TreeNode& node = *(allNodes[i]);
    for (int j=0; j<node.direction.size(); j++)
    {
      node.direction[j] = getVal(minAlpha,maxAlpha);   
    }
  };
  
  // for cohesive
  for (int in=0; in< allLeaves.size(); in++)
  {
    TreeNode* leaf = allLeaves[in];
    if (leaf->withCohesive())
    {
      int nCoh = leaf->cohesiveCoeffVars.size();
      int nDirVars = leaf->cohesiveDirectionVars.size2();
      for (int i=0; i< nCoh; i++)
      {
        leaf->cohesiveCoeffVars(i) = getVal(1.,100.);   
      }
      for (int i=0; i< nCoh; i++)
      {
        for (int j=0; j< nDirVars; j++)
        {
           leaf->cohesiveDirectionVars(i,j) = getVal(minAlpha,maxAlpha);
        };
      }
    }
  }
};

void Tree::initialize(bool rand, bool normalizedWeight)
{
  // random weight for leaves
  unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
  std::default_random_engine generator(seed);
  std::uniform_real_distribution<double> distribution(0., 1.); 
  
  auto getVal = [&](double min, double max)
  {
    double x = 0.5;
    if (rand)
    {
      x= distribution(generator);
    }
    return (max-min)*x + min;
  };
  
  
  double minWeight = 0.2;
  double maxWeight = 0.8;
  std::vector<TreeNode*> allLeaves;
  getRefToAllLeaves(allLeaves);
  
  double toltalWeightLeaves = 0.;
  for (int i=0; i< allLeaves.size(); i++)
  {
    TreeNode* n = allLeaves[i];
    n->weight = getVal(minWeight,maxWeight);
    toltalWeightLeaves += n->af->getVal(n->weight);
  }
  
  for (int i=0; i< allLeaves.size(); i++)
  {
    TreeNode* n = allLeaves[i];
    if (normalizedWeight)
    {
      double vv = (n->af->getVal(n->weight))/toltalWeightLeaves;
      //n->weight = n->af->getReciprocalVal(vv);   
      n->weight = vv;
    }
    // propagate data
    double w = n->weight;
    TreeNode* parent = n->parent;
    while (parent != NULL)
    {
      parent->weight += n->af->getVal(w);
      parent = parent->parent;
    };
  };
  //printf("pi = %e\n",pi);
  double minAlpha = 0.;
  double maxAlpha = 1;
  //
  std::vector<TreeNode*> allNodes;
  getRefToAllNodes(allNodes);
  printf("num of nodes = %ld\n",allNodes.size());
  for (int i=0; i< allNodes.size(); i++)
  {
    TreeNode& node = *(allNodes[i]);
    for (int j=0; j<node.direction.size(); j++)
    {
      node.direction[j] = getVal(minAlpha,maxAlpha);   
    }
  };
   
  initializeCohesiveParameters(1e-1,1e1);
};
