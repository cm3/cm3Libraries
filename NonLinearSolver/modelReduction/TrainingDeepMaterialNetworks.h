//
// C++ Interface:  deep material networks
//
//
// Author:  <V.-D. Nguyen>, (C) 2020
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef _TRAININGDEEPMATERIALNETWORKS_H_
#define _TRAININGDEEPMATERIALNETWORKS_H_ 

#include "DeepMaterialNetworks.h"
#include "Tree.h"
#include "Optimizer.h"
#ifndef SWIG
#include "linearSystem.h"
#endif //SWIG
class hyperFullMatrix
{
  /** to store DmatADmatB 
  // DA_ij/DB_kl = << A A_ij>_l >_k = DmatADmatB[k][l](i,j);
   * **/
  #ifndef SWIG
  protected:
    std::vector<std::vector<fullMatrix<double> > > _data;
    int _dimFirst, _dimSecond; 
  #endif //SWIG
  public:
    hyperFullMatrix();
    hyperFullMatrix(int M, int N, double v);
    ~hyperFullMatrix(){}
    void allocate(int M, int N, double v = 0);
    int getFirstDim() const;
    int getSecondDim() const;
    void clear();
    const fullMatrix<double>& getMatrix(int k, int l) const;
    fullMatrix<double>& getMatrix(int k, int l);
    void axpy(const hyperFullMatrix& x, double a);
    void setAll(double a);
    void scale(double a);
    void printData(std::string str="", bool sym=false) const;
    #ifndef SWIG
    double& operator()(int i, int j, int k, int l) {return _data[k][l](i,j);}
    double operator()(int i, int j, int k, int l) const {return _data[k][l](i,j);}
    hyperFullMatrix& operator=(const hyperFullMatrix& src);
    #endif //SWIG
};


namespace fullMatrixOperation
{
    void allocateAndMakeZero(fullVector<double>& v, int row);
    void allocateAndMakeZero(fullMatrix<double>& A, int row, int col);
    void transpose(const fullMatrix<double>& A, fullMatrix<double>& AT);
    bool invertMatrix(const fullMatrix<double>& A, fullMatrix<double>& invA, bool stiff, hyperFullMatrix& DinvADA);
    void mult(const fullMatrix<double>& A, const fullMatrix<double>& B, const fullMatrix<double>& C, fullMatrix<double>& R);
    void multPtAP(const fullMatrix<double>& P, const fullMatrix<double>& A, fullMatrix<double>& R);
    void mult(const fullMatrix<double>& A, const fullMatrix<double>& B, fullMatrix<double>& R);
    void mult(const fullMatrix<double>& A, const fullMatrix<double>& B, const fullVector<double>& C, fullVector<double>& R);
    void multAdd(const fullMatrix<double>& A, const fullMatrix<double>& B, const fullMatrix<double>& C, fullMatrix<double>& R, double fact);
    double dot(const fullMatrix<double>& A, const fullMatrix<double>& B);
    void mult(const hyperFullMatrix& A, const hyperFullMatrix& B, hyperFullMatrix& C, double fact);
    void multAdd(const hyperFullMatrix& A, const fullMatrix<double>& B, fullMatrix<double>& C, double fact);
    void mult(const hyperFullMatrix& A, const fullMatrix<double>& B, fullMatrix<double>& C);
    void reduceMat99ToMat66(const fullMatrix<double>& mat9x9, fullMatrix<double>& mat6x6);
    void reducePlaneStrain(const fullMatrix<double>& mat9x9, fullMatrix<double>& mat3x3);
    void convertSTensor43ToFullMatrix(const STensor43& ten, fullMatrix<double>& fullMat);
    void convertFullMatrixToSTensor43(const fullMatrix<double>& fullMat, STensor43& ten);
    void isotropicElasticTensor(double E, double nu, STensor43& K);
    void isotropicElasticTensor(double E, double nu, fullMatrix<double>& fullMat);
    void anisotropicElasticTensor(double Ex, double Ey, double Ez,
                                  double Vxy, double Vxz, double Vyz,
                                  double MUxy, double MUxz, double MUyz,
                                  double alpha, double beta, double gamma,
                                  fullMatrix<double>& fullMat);
    void planeStrainElasticTensor(double C00, double C11, double G12, double C12, fullMatrix<double>& C);
    void matToVec(const fullMatrix<double>& mat, fullVector<double>& v);
    void vecToMat(const fullVector<double>& vec, int nR, int nC, fullMatrix<double>& mat);
    void convertMat66ToMat99(const fullMatrix<double>& mat6x6, fullMatrix<double>& mat9x9);
    void getRowColTangent(int dim, int i, int j, int k, int l, int& row, int& col);
    void getRow(const fullMatrix<double>& src, int row, fullVector<double>& rowVec);
    void getCol(const fullMatrix<double>& src, int col, fullVector<double>& colVec);
    void copyRow(const fullMatrix<double>& Src, int rowSrc, fullMatrix<double>& Dest, int rowDest, double fact);
    void copyMultipleRows(const fullMatrix<double>& Src, const std::vector<int>& rowSrc, 
                          fullMatrix<double>& Dest, const std::vector<int>& rowDest, double fact);
    void getMatrixN(bool planeStrain, double n0, double n1, double n2, fullMatrix<double>& N);
    void getMatrixN(bool planeStrain, double n0, double n1, double n2, fullMatrix<double>& N, 
            bool stiff,
            fullMatrix<double>& DNDn0, 
            fullMatrix<double>& DNDn1, 
            fullMatrix<double>& DNDn2);
    
    // make compliance compatible with stress and strain vectors notations
    void convertToSTensor43Compliance66(const STensor43& S, fullMatrix<double>& Smat);
    void convertCompliance66ToSTensor43(const fullMatrix<double>& Smat, STensor43& S);
    // some homogenization test
    void homogenizedTangentBimaterial(const fullMatrix<double>& C0, const fullMatrix<double>& C1, double f,
                                      double n0, double n1, double n2, fullMatrix<double>& Chomo);
    void homogenizedComplianceBimaterial(const fullMatrix<double>& C0, const fullMatrix<double>& C1, double f, 
                                   double n0, double n1, double n2, double t0, double t1, double t2, fullMatrix<double>& Chomo);
};

class Homogenization 
{
  public:
    #ifndef SWIG
    virtual ~Homogenization(){}
    virtual void compute(const std::vector<fullMatrix<double> >& Cin, const std::vector<double>& f, const SVector3& normal,
                        fullMatrix<double>& Ceff, 
                        bool stiff, 
                        std::vector<hyperFullMatrix>& DCeffDcin, 
                        std::vector<fullMatrix<double> >& DCeffDf, 
                        std::vector<fullMatrix<double> >&DCeffDnormal) const = 0; 
    virtual void printInfos() const = 0;
    virtual bool withPlaneStrain() const  = 0;
    #endif //SWIG
};

class BlockMatrix
{
  protected:
    fullMatrix<double> _data;
    int _numRow;
    int _numCol;
    int _numRowElem;
    int _numColElem;
    bool _isModified;
    #ifndef SWIG
    linearSystem<double>* _lsys;
    #endif //SWIG
  public:
    BlockMatrix(int numRow=0, int numCol=0, int rowElem=0, int colElem=0);
    BlockMatrix(const BlockMatrix& src);
    ~BlockMatrix();
    
    void setElem(int row, int col, const fullMatrix<double>& mat, double fact = 1.);
    void setElemAdd(int row, int col, const fullMatrix<double>& mat, double fact = 1.);
    void getElem(int row, int col, fullMatrix<double>& mat) const;
    void transpose(BlockMatrix& vT) const;
    void transpose(fullMatrix<double>& vT) const;
    void setAll(double v);
    int size1() const {return _data.size1();};
    int size2() const {return _data.size2();};
    
    void luSolve(const fullMatrix<double>& rhs, fullMatrix<double>& result);
    void luSolve(const BlockMatrix& rhs, fullMatrix<double>& result);
    void PtAP(const BlockMatrix& P, fullMatrix<double>& PtAPres) const;
    void PtAP(const fullMatrix<double>& P, fullMatrix<double>& PtAPres) const;
    
    const fullMatrix<double>& getData() const {return _data;};
    fullMatrix<double>& getRefToData() {return _data;};
};

class HomogenizationWithCohesive
{
  protected:
    bool _planeStrain;
    
  public:
    HomogenizationWithCohesive(bool planeStrainState):_planeStrain(planeStrainState){}
    virtual ~HomogenizationWithCohesive(){}
  
    virtual void compute(const fullMatrix<double>& Cin, // elastic stiffness
                         const fullVector<double>& K,  // interface stiffness
                         const fullVector<double>& cohesiveCoeffs, // all coeffcients of interface terms contribution
                         const fullMatrix<double>& allNormalVars, // all normal variables, each row contains varialbe for each normal
                         fullMatrix<double>& Ceff, // equivalent stiffness
                         bool stiff=false,
                         std::vector<fullMatrix<double> >* DCeffDcohesiveCoeffs = NULL,
                         std::vector<std::vector<fullMatrix<double> > >* DCeffDnormalVars = NULL 
                         ) const;
   virtual bool withPlaneStrain() const  {return _planeStrain;};                      
  #ifndef SWIG
  private:
    void getNormal(const fullVector<double>& normalVars, double& n0, double& n1, double& n2, 
                   bool stiff=false, fullMatrix<double>* DnormalDVars = NULL) const;
    void getInterfaceStiffness(double Kn, double Kt, double n0, double n1, double n2, fullMatrix<double>& Q,
              bool stiff = false, fullMatrix<double>* DQDn0=NULL, fullMatrix<double>* DQDn1=NULL, fullMatrix<double>* DQDn2=NULL) const;
  #endif //SWIG
};

class BimaterialHomogenization : public Homogenization
{
  protected:
    bool _planeStrain;
    
  public:
    BimaterialHomogenization(bool planeStrainState ):_planeStrain(planeStrainState){};
    
    virtual void compute(const fullMatrix<double>& C1, const fullMatrix<double>& C2, double f1, double normedtheta, fullMatrix<double>& Ceff) const;
    virtual void compute(const fullMatrix<double>& C1, const fullMatrix<double>& C2, double f1, 
                         double n0, double n1, double n2, fullMatrix<double>& Ceff) const;
    
    virtual void compute(const fullMatrix<double>& C1, const fullMatrix<double>& C2, double f1, double normedtheta, fullMatrix<double>& Ceff,
                        bool stiff, hyperFullMatrix& DCeffDC1, hyperFullMatrix& DCeffDC2, 
                        fullMatrix<double>& DCeffDf1, fullMatrix<double>& DCeffDf2 ) const;
                        
    virtual void printInfos() const;
    virtual bool withPlaneStrain() const  {return _planeStrain;};
    
    virtual ~BimaterialHomogenization(){}
    virtual void compute(const std::vector<fullMatrix<double> >& Cin, const std::vector<double>& f, const SVector3& normal,
                        fullMatrix<double>& Ceff, 
                        bool stiff, 
                        std::vector<hyperFullMatrix>& DCeffDcin, 
                        std::vector<fullMatrix<double> >& DCeffDf, 
                        std::vector<fullMatrix<double> >&DCeffDnormal) const;
    
    #ifndef SWIG
    protected:
      virtual void computeBimaterial(const fullMatrix<double>& C1, const fullMatrix<double>& C2, double f1, double f2, const SVector3& normal,
                        fullMatrix<double>& Ceff, 
                        bool stiff=false, 
                        hyperFullMatrix* DCeffDC1=NULL, hyperFullMatrix* DCeffDC2=NULL,
                        fullMatrix<double>* DCeffDf1=NULL, fullMatrix<double>* DCeffDf2=NULL, 
                        fullMatrix<double>* DCeffDnormal1=NULL,fullMatrix<double>* DCeffDnormal2=NULL, fullMatrix<double>* DCeffDnormal3=NULL) const;
    #endif //SWIG
};

class LaminateHomogenization : public BimaterialHomogenization
{
  public:
    LaminateHomogenization(bool planeStrainState ):BimaterialHomogenization(planeStrainState){}
    virtual void compute(const fullMatrix<double>& C1, const fullMatrix<double>& C2, const fullMatrix<double>& C3, 
                        double f1, double f2, double normedtheta, fullMatrix<double>& Ceff) const;
    virtual void compute(const fullMatrix<double>& C1, const fullMatrix<double>& C2, const fullMatrix<double>& C3, const fullMatrix<double>& C4, 
                        double f1, double f2, double f3, double normedtheta, fullMatrix<double>& Ceff) const;
                    
    virtual ~LaminateHomogenization(){}
    virtual void compute(const std::vector<fullMatrix<double> >& Cin, const std::vector<double>& f, const SVector3& normal,
                        fullMatrix<double>& Ceff, 
                        bool stiff, 
                        std::vector<hyperFullMatrix>& DCeffDcin, 
                        std::vector<fullMatrix<double> >& DCeffDf, 
                        std::vector<fullMatrix<double> >&DCeffDnormal) const;
    
    virtual void printInfos() const;
};

class LossMeasure
{
  #ifndef SWIG
  public:
    virtual ~LossMeasure(){}
    virtual double get(const fullMatrix<double>& Cref, const fullMatrix<double>& C, bool stiff=false, fullMatrix<double>* DnormDC=NULL) const = 0;
    static LossMeasure* createLossMeasure(const char what[]);
  #endif //SWIG
};

class SquareError : public LossMeasure
{ 
  public:
    SquareError(){}
    virtual ~SquareError(){}
    virtual double get(const fullMatrix<double>& Cref, const fullMatrix<double>& C, bool stiff=false, fullMatrix<double>* DnormDC=NULL) const;
};

class SquareRootError : public LossMeasure
{ 
  public:
    SquareRootError(){}
    virtual ~SquareRootError(){}
    virtual double get(const fullMatrix<double>& Cref, const fullMatrix<double>& C, bool stiff=false, fullMatrix<double>* DnormDC=NULL) const;
};



class TrainingDeepMaterialNetwork : public LossFunction
{
  #ifndef SWIG
  public:
    class OfflineData
    {
      public:
        fullMatrix<double> Cel;
        fullVector<double> Qel;   // homogenized
        std::map<Dof, fullMatrix<double> > DCeffDunknown; // derivatives with respect to unknown
        OfflineData():Cel(), Qel(),DCeffDunknown(){}
        fullMatrix<double>& getDCeffDunknown(Dof k, bool isPlaneStrain)
        {
          std::map<Dof, fullMatrix<double> >::iterator itF = DCeffDunknown.find(k);
          if ( itF!= DCeffDunknown.end())
          {
            return itF->second;
          }
          else
          {
            fullMatrix<double>& A = DCeffDunknown[k];
            if (isPlaneStrain)
            {
              fullMatrixOperation::allocateAndMakeZero(A,3,3);
            }
            else
            {
              fullMatrixOperation::allocateAndMakeZero(A,6,6);
            }
            return A;
          }
        }
        void zeroDCeffDunknown()
        {
          for (std::map<Dof, fullMatrix<double> >::iterator it =  DCeffDunknown.begin(); it != DCeffDunknown.end(); it++)
          {
            fullMatrix<double>& A = (it->second);
            A.setAll(0.);
          }
        }
        ~OfflineData(){};
    };

  protected:
    Tree* _T; // to train
    const Homogenization* _homo;
    const HomogenizationWithCohesive* _cohesiveHomo;
    std::map<const TreeNode*, OfflineData*> _offlineData;
    std::map<Dof, int> _unknown;
    std::map<Dof, double> _fixedDof;
    //
    double _lambda; // penalty parameter
    // num phase
    std::vector<std::vector<fullMatrix<double> > >_XTrain;
    std::vector<fullVector<double> > _QTrain;
    std::vector<fullMatrix<double> > _YTrain;
    std::vector<std::vector<fullMatrix<double> > >_XTest;
    std::vector<fullVector<double> > _QTest;
    std::vector<fullMatrix<double> > _YTest;
    
  #endif //SWIG  
  public:
    TrainingDeepMaterialNetwork(Tree& T, const Homogenization& ho);
    ~TrainingDeepMaterialNetwork();
    void setPenalty(double l);
    void fixAllWeights();
    void fixAllLaminateDirections();
    void fixAllCohesiveDirections();
    void setCohesiveEnrichement(const HomogenizationWithCohesive& coh);
    void trainingDataSize(int Ns, int numPhase);
    void setTrainingSample(int row, const fullMatrix<double>& C1, const fullMatrix<double>& Ceff);
    void setTrainingSampleCohesive(int row, const fullVector<double>& Q);
    void setTrainingSample(int row, const fullMatrix<double>& C1, const fullMatrix<double>& C2, const fullMatrix<double>& Ceff);
    void setTrainingSample(int row, const fullMatrix<double>& C1, const fullMatrix<double>& C2, const fullMatrix<double>& C3, const fullMatrix<double>& Ceff);
    void testDataSize(int Ns, int numPhase);
    void setTestSample(int row, const fullMatrix<double>& C1, const fullMatrix<double>& Ceff);
    void setTestSampleCohesive(int row, const fullVector<double>& Q);
    void setTestSample(int row, const fullMatrix<double>& C1, const fullMatrix<double>& C2, const fullMatrix<double>& Ceff);
    void setTestSample(int row, const fullMatrix<double>& C1, const fullMatrix<double>& C2, const fullMatrix<double>& C3, const fullMatrix<double>& Ceff);
    #if 0
    void train(double lr, int maxEpoch, 
              std::string loss = "mare", 
              std::string historyFileName="history.csv", 
              std::string saveTreeFileName="tree_trained.txt", 
              int saveEpoch=50, 
              int numberBatches=1,
              double lrmin=1e-4);
    #else
     void train(double lr, int maxEpoch, 
              std::string loss = "mare", 
              std::string historyFileName="history.csv", 
              std::string saveTreeFileName="tree_trained.txt", 
              int saveEpoch=50, 
              int numberBatches=1,
              double lrmin=1e-4,
              bool removeZeroContribution = false,
              int numStepRemove = 3,
              double tolRemove=1e-4);      
    #endif //
    // test homogenization procedure
    void testHomogenizationProcess(fullMatrix<double>& C1, const fullMatrix<double>& C2, fullMatrix<double>& Ceff);
    void testHomogenizationProcess(fullMatrix<double>& C1, const fullMatrix<double>& C2, double Kn, double Kt, fullMatrix<double>& Ceff);
    
  #ifndef SWIG
  //
  protected:
    void getKeys(const TreeNode* node, std::vector<Dof>& keys) const;
    void numberDofs(fullVector<double>& ufield);
    int getDofNumber(Dof key) const;
    double getDofValue(Dof key, const fullVector<double>& ufield) const;
    void updateFieldFromUnknown(const fullVector<double>& ufield);
    
    void getNormal(const TreeNode* node, SVector3& vec, bool stiff, std::vector<SVector3>* DnormalDunknown) const;
    void homogenizationProcess(bool stiff);
    void computeErrorVector(std::vector<double>& allErors, const std::vector<LossMeasure*>& allLosses, 
                         const std::vector<fullMatrix<double> >& Cin, const fullVector<double>& Qin,  const fullMatrix<double>& Ceff);
    double computeError(const LossMeasure& loss, const std::vector<fullMatrix<double> >& Cin, const fullVector<double>& Qin, 
                          const fullMatrix<double>& Ceff, bool stiff=false, fullVector<double>* g=NULL);
                 
    void updateUnknown(double lr, const fullVector<double>& g, fullVector<double>& Wcur);
    double computeCostFunction(const LossMeasure& loss, const std::vector<int>& sampleIndex, fullVector<double>& grad);
    double evaluateTrainingSet(const LossMeasure& loss);
    double evaluateTestSet(const LossMeasure& loss);
  #endif //SWIG
  
  public:
    virtual int sizeTrainingSet() const; 
    // allow to estimate loss function and derivatives with respect to the fitting paramater
    virtual double computeCostFunction(const std::string& metric, const std::vector<int>& sampleIndex, fullVector<double>& grad); 
    // loss function in whole training set
    virtual void evaluateTrainingSet(const std::vector<std::string>& metrics, std::vector<double>& vals);
    // loss function in test set
    virtual void evaluateTestSet(const std::vector<std::string>& metrics, std::vector<double>& vals);
    // save model
    virtual void saveModel(const std::string fname) const;
    // start numbering fitting parameters
    virtual void initializeFittingParameters(fullVector<double>& W); 
    // get bounds of fitting paraeters
    virtual void getUnknownBounds(fullVector<double>& WLower, fullVector<double>& WUpper) const;
    // update the model with the new fitting parameter
    virtual void updateModelFromFittingParameters(const fullVector<double>& W);
    //  in case of fails
    virtual void reinitializeModel();
};
#endif //_TRAININGDEEPMATERIALNETWORKS_H_
