//
// C++ Interface: terms
//
// Description: Class with the displacement field
//
//
// Author:  <Gauthier BECKER>, (C) 2010
// <Van Dung NGUYEN>, (C) 2019
//
// Copyright: See COPYING file that comes with this distribution
//
//

#include "unknownField.h"
#include "nonLinearMechSolver.h"
#include "nlsolAlgorithms.h"
#include "MPoint.h"
// constructor

unknownField::unknownField(nonLinearMechSolver* sl,const int nc, 
													 const std::vector<unknownField::archiveNode> &archiving,
                           std::vector<nlsField::dataBuildView> &dbview_,
                           const std::string filen): _solver(sl),nodesField(filen,nc,dbview_),
                           _lastSaveStep(-1)
{
	std::vector<partDomain*>& vdom = *_solver->getDomainVector();
	std::vector<partDomain*>& vghostdom = *_solver->getGhostDomainMPI();
  if (_numStepBetweenTwoViews < _solver->getStepBetweenArchiving())
  {
    _numStepBetweenTwoViews = _solver->getStepBetweenArchiving();
  }
	
  std::vector<bool> alfind;
  for(int i=0;i<archiving.size(); i++)
    alfind.push_back(false);
  long int totelem=0;
  for(std::vector<partDomain*>::iterator itdom=vdom.begin(); itdom!=vdom.end(); ++itdom){
    partDomain *dom = *itdom;
    bool fullDg = dom->getFormulation();
    FunctionSpaceBase *sp = dom->getFunctionSpace();
    std::vector<Dof> R;
    totelem += dom->elementGroupSize();
    if(!fullDg){
			int partition = 0;
			if (dom->elementGroupSize()>0){
				MElement* ele = dom->element_begin()->second;
				partition = ele->getPartition();
			}
      for(elementGroup::vertexContainer::const_iterator it=dom->vertex_begin(); it!=dom->vertex_end(); ++it){
        MVertex *ver = it->second;
        MPoint ele = ((Msg::GetCommSize() == 1) or (partition == 0)) ? MPoint(ver,-1) : MPoint(ver,-1,Msg::GetCommRank()+1); // We don't care about element number it is just to have the vertex keys;
        sp->getKeys(&ele,R);
        // make dof for archiving node
        for(int i=0;i<archiving.size();i++){
          if( (alfind[i] == false) and (ver->getNum() == archiving[i].nodenum) and (archiving[i].iscontact==false)){
            FilterDof* filter = dom->createFilterDof(archiving[i]._comp);
            for(int j=0;j<R.size();j++){
              if(filter->operator()(R[j])){
                alfind[i] = true;
                #if defined(HAVE_MPI)
                if(R[j].getType() > 0)
                #endif // HAVE_MPI
                {
                  varch.push_back(archiveNode(R[j],archiving[i].physnum, ver->getNum(),archiving[i]._comp,archiving[i].wc,archiving[i].nstep));
                  // also define the dof
                }
              }
            }
            delete filter;
          }
        }
        R.clear();
      }
    }
    else{
      // loop on element (entity of formulation)
      for(elementGroup::elementContainer::const_iterator it=dom->element_begin(); it!=dom->element_end(); ++it){
        MElement *ele = it->second;
        // loop on vertex element (for archiving node)
        int nbvertex = ele->getNumVertices();
        for(int j=0;j<nbvertex;j++){
          MVertex *ver = ele->getVertex(j);
          for(int i=0; i<archiving.size(); i++){
            if((alfind[i] == false) and(ver->getNum() == archiving[i].nodenum) and(archiving[i].iscontact==false)){
              // get the comp of the archiving node
              alfind[i] = true;
              int comp = archiving[i]._comp;
              std::vector<Dof> RR;
              sp->getKeysOnVertex(ele,ver,std::vector<int>(1,comp),RR);
              #if defined(HAVE_MPI)
              if(RR[0].getType() > 0)
              #endif // HAVE_MPI
              {
                varch.emplace_back(RR[0],archiving[i].physnum,ver->getNum(),comp,archiving[i].wc,archiving[i].nstep);
              }
            }
          }
        }  
        /*
        sp->getKeys(ele,R);
        // loop on vertex element (for archiving node)
        int nbvertex = ele->getNumVertices();
        for(int j=0;j<nbvertex;j++){
          MVertex *ver = ele->getVertex(j);
          for(int i=0; i<archiving.size(); i++){
            if((alfind[i] == false) and(ver->getNum() == archiving[i].nodenum) and(archiving[i].iscontact==false)){
              // get the comp of the archiving node
              alfind[i] = true;
              int comp = archiving[i]._comp;
              #if defined(HAVE_MPI)
              if(R[j+comp*nbvertex].getType() > 0)
              #endif // HAVE_MPI
              {
                varch.emplace_back(R[j+comp*nbvertex],archiving[i].physnum,ver->getNum(),comp,archiving[i].wc,archiving[i].nstep);
              }
            }
          }
        }
        R.clear();
        */
      }
    }
  }
  // increment the total element with rigid contact and add archiving if necessary

  for(nonLinearMechSolver::contactContainer::iterator it = _solver->getAllContactDomain()->begin(); it!=_solver->getAllContactDomain()->end(); ++it){
    contactDomain *cdom = *it;
    if(cdom->isRigid()){
      totelem += cdom->gMaster->size();
      int pMaster = cdom->getPhys();
      for(int i=0;i<archiving.size(); i++){
        partDomain* domf=NULL;
        if((archiving[i].iscontact==true) and ((int)archiving[i].nodenum == pMaster)){
          int physDom = cdom->getPhysSlave();
          for(std::vector<partDomain*>::iterator itdom = vdom.begin(); itdom!=vdom.end(); ++itdom){
            partDomain *dom = *itdom;
            if(dom->getPhysical() == physDom){
              domf = dom;
              break;
            }
          }
         #if defined(HAVE_MPI)
          if(domf == NULL)
          {
            for(std::vector<partDomain*>::iterator itdom = vghostdom.begin(); itdom!=vghostdom.end(); ++itdom)
            {
              partDomain *dom = *itdom;
              if(dom->getPhysical() == physDom)
              {
                domf = dom;
                break;
              }
            }
          }
         #endif // HAVE_MPI
          if(domf!=NULL){
            rigidContactSpaceBase *sp = static_cast<rigidContactSpaceBase*>(cdom->getSpace());
            std::vector<Dof> R;
            sp->getKeysOfGravityCenter(R);
            FilterDof *fdof = domf->createFilterDof(archiving[i]._comp);
            for(int j=0;j<R.size(); j++)
              if(fdof->operator()(R[j])){
                varch.emplace_back(R[j],archiving[i].physnum,pMaster,archiving[i]._comp,archiving[i].wc,archiving[i].nstep,true);
              }
            delete fdof;
          }
        }
      }
    }
  }
  
  // find in corner 
  for (int i=0; i< alfind.size(); i++){
    if ((!alfind[i]) and (archiving[i].nodenum == 0)){
       std::vector<Dof> R;
      _solver->getDofOnControlNode(archiving[i].physnum,R);
      if (R.size() > 0){
        varch.emplace_back(R[archiving[i]._comp],archiving[i].physnum, 0,archiving[i]._comp,archiving[i].wc,archiving[i].nstep);
      }
      else{
        Msg::Error("corner %d does not exist",archiving[i].physnum);
      }
      alfind[i] = true;
    }
  }
  
  
  // open files
  _numStepBetweenTwoSaves = 1;
  for(int i=0;i<varch.size();i++)
  {
    varch[i].openFile(_solver->getFileSavingPrefix());
    if (varch[i].nstep > _numStepBetweenTwoSaves)
    {
      _numStepBetweenTwoSaves = varch[i].nstep;
    }
  }

  this->setTotElem(totelem);
}

void unknownField::buildData(FILE* myview, const nonLinearMechSolver* solver,const int cc, const nlsField::ElemValue ev, const int gp, const nlsField::ValType vt) const
{
  const std::vector<partDomain*> &vdom = *(solver->getDomainVector());
  nodesField::buildData(myview,solver,cc,ev,gp,vt);
  // add contact domain
  std::vector<double> fieldData;
  for(nonLinearMechSolver::contactContainer::iterator it=_solver->getAllContactDomain()->begin(); it!=_solver->getAllContactDomain()->end(); ++it){
    contactDomain *cdom = *it;
    if(cdom->isRigid()){
      for (elementGroup::elementContainer::const_iterator it = cdom->gMaster->begin(); it != cdom->gMaster->end(); ++it){
        MElement *ele = it->second;
        int numv = ele->getNumVertices();
        rigidContactSpaceBase *spgc = static_cast<rigidContactSpaceBase*>(cdom->getSpace());
        std::vector<Dof> R;
        spgc->getKeysOfGravityCenter(R);
        this->get(R,fieldData);
        fprintf(myview, "%ld %d",ele->getNum(),numv);
        for(int i=0;i<numv;i++)
          for(int j=0;j<_numcomp;j++)
            fprintf(myview, " %.16g",fieldData[j]);
        fprintf(myview,"\n");
        fieldData.clear();
      }
    }
  }
}


unknownField::~unknownField()
{
}

void unknownField::get(Dof &D,double &udof) const{
	_solver->getDofManager()->getDofValue(D,udof);
}

void unknownField::get(Dof &D, double &udof, nonLinearBoundaryCondition::whichCondition wv) const {
  _solver->getDofManager()->getDofValue(D,udof,wv);
}

void unknownField::get(std::vector<Dof> &R, std::vector<double> &disp) const{
  double du = 0.;
  for(int i=0;i<R.size(); i++){
    this->get(R[i],du);
    disp.push_back(du);
  }
}

void unknownField::get(std::vector<Dof> &R, fullVector<double> &disp) const{
  double du = 0.;
  for(int i=0;i<R.size(); i++){
    this->get(R[i],du);
    disp(i) = du;
  }
}

void unknownField::get(std::vector<Dof> &R, std::vector<double> &udofs, nonLinearBoundaryCondition::whichCondition wv) const {
  double du = 0.;
  for(int i=0;i<R.size(); i++){
    this->get(R[i],du,wv);
    udofs.push_back(du);
  }
}
void unknownField::get(std::vector<Dof> &R, fullVector<double> &udofs, nonLinearBoundaryCondition::whichCondition wv) const {
  double du = 0.;
  for(int i=0;i<R.size(); i++){
    this->get(R[i],du,wv);
    udofs(i) = du;
  }
}

void unknownField::get(partDomain *dom, MElement *ele,std::vector<double> &udofs, const int cc, const nlsField::ElemValue ev, const int gp, const nlsField::ValType vt)const{
  if (cc <0)
  {
    int dispCom = -cc -1;
    std::vector<Dof> R;
    for (int i=0; i< ele->getNumVertices(); i++)
    {
      dom->getFunctionSpace()->getKeysOnVertex(ele,ele->getVertex(i),std::vector<int>(1,dispCom),R);
    }
    this->get(R,udofs);
  }
  else
  {
    std::vector<Dof> R;
    FunctionSpaceBase *sp = dom->getFunctionSpace();
    if (dom->getFunctionSpaceType() == functionSpaceType::Lagrange)
    {
      sp->getKeys(ele,R);
    }
    else if (dom->getFunctionSpaceType() == functionSpaceType::Hierarchical)
    {
      for (int c =0; c<3; c++)
      {
        for (int i=0; i< ele->getNumVertices(); i++)
        {
          sp->getKeysOnVertex(ele,ele->getVertex(i),std::vector<int>(1,c),R);
        }
      }
    }
    if(cc==1 or cc == 2)
    {
      nonLinearBoundaryCondition::whichCondition wv;
      if (cc==1)
        wv= nonLinearBoundaryCondition::velocity;
      else if(cc==2)
        wv= nonLinearBoundaryCondition::acceleration;
      this->get(R,udofs,wv);
    }
    else
      this->get(R,udofs);
  }
}

void unknownField::setFactorOnArchivingFiles(const int fact)
{
  for(int i=0;i<varch.size();i++)
  {
    varch[i].nstep*=fact;
  }
}

void unknownField::resetArchiving()
{
  nodesField::resetArchiving();
  _lastSaveStep = -1;
  for(std::vector<archiveNode>::iterator it = varch.begin(); it!=varch.end();++it)
  {
    archiveNode& an = *it;
    if (an.fp != NULL) fclose(an.fp);
    an.openFile(_solver->getFileSavingPrefix());
  }
}

void unknownField::closeArchivingFiles()
{
  for(std::vector<archiveNode>::iterator it = varch.begin(); it!=varch.end();++it)
  {
    archiveNode& an = *it;
    if (an.fp != NULL) fclose(an.fp);
    an.fp = NULL;
  };
};


void unknownField::archive(const double time,const int step, const bool forceView)
{
  // msh view
  this->buildAllView(_solver,time,step,forceView);
  // sative to file
  if (step > _lastSaveStep)
  {
    if ( (step%_numStepBetweenTwoSaves == 0) or forceView)
    {
      _lastSaveStep = step;
      for(std::vector<archiveNode>::iterator it = varch.begin(); it!=varch.end();++it){
        const Dof D = it->D;
        double u;
        _solver->getDofManager()->getDofValue(D,u,it->wc);
        // write to file
        fprintf(it->fp,"%.16g;%.16g\n",time,u);
        fflush(it->fp);
      }
    }
  }
}

void unknownField::valuesForOnelab(fullVector<double> &nodalValues) const
{
  nodalValues.resize(varch.size());
  int index = 0;
  double u;
  for(std::vector<archiveNode>::const_iterator it = varch.begin(); it!=varch.end();++it,++index){
    const Dof D = it->D;
    _solver->getDofManager()->getDofValue(D,u,it->wc);
    nodalValues.set(index,u);
  }
}

void unknownField::restart()
{
  nodesField::restart();
  restartManager::restart(_lastSaveStep);
  restartManager::restart(_numStepBetweenTwoSaves);
  return;
}
