//
// C++ Interface: field
//
// Description: Class for a field on elements (specific functions)
// Author:  <Gauthier BECKER>, (C) 2012
// <Van Dung NGUYEN>, (C) 2019
// Copyright: See COPYING file that comes with this distribution
//
//
#include "elementsField.h"
#include "nonLinearMechSolver.h"
elementsField::elementsField(const std::string fnn, const int ncomp,
               std::vector<dataBuildView> &dbview_) : nlsField(fnn,ncomp,dbview_){}

void elementsField::buildData(FILE* myview, const nonLinearMechSolver* solver, const int cc, const nlsField::ElemValue ev, const int gp, const nlsField::ValType vt) const
{
   const std::vector<partDomain*> &vdom = *(solver->getDomainVector());
  std::vector<double> fieldData;
  for (unsigned int i = 0; i < vdom.size(); ++i)
    for (elementGroup::elementContainer::const_iterator it = vdom[i]->element_begin(); it != vdom[i]->element_end(); ++it){
      MElement *ele = it->second;
      fieldData.resize(_numcomp);
      this->get(vdom[i],ele,fieldData,cc,ev,gp,vt);
      fprintf(myview, "%ld",ele->getNum());
      for(int j=0;j<_numcomp;j++)
        fprintf(myview, " %.16g",fieldData[j]);
      fprintf(myview,"\n");
    }
}