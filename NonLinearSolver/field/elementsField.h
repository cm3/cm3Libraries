//
// C++ Interface: field
//
// Description: Class for a field on elements (specific functions)
// Author:  <Gauthier BECKER>, (C) 2012
// <Van Dung NGUYEN>, (C) 2019
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef ELEMENTSFIELD_H_
#define ELEMENTSFIELD_H_
#include "nlsField.h"
class elementsField : public nlsField
{
#ifndef SWIG
  protected:
    virtual void buildData(FILE* myview, const nonLinearMechSolver* solver, const int cc, const nlsField::ElemValue ev, const int gp, const nlsField::ValType vt) const;
    virtual std::string dataname() const {return "ElementData";};
  public:
    elementsField(const std::string fnn, const int ncomp, std::vector<dataBuildView> &dbview_);
    virtual ~elementsField(){}
#endif // SWIG
};
#endif // ELEMENTSFIELD_H_
