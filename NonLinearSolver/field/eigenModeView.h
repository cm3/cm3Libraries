//
//
// Description: eigen mode view data
//
// Author:  <Van Dung NGUYEN>, (C) 2013
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef EIGENMODEFIELD_H_
#define EIGENMODEFIELD_H_

#include "unknownField.h"
#include "numericalFunctions.h"
#include "eigenSolver.h"

class eigenVectorField : public nodesField
{
  protected:
    nonLinearMechSolver* _solver;
    eigenSolver* _eigenSolver;
  
  protected:
    // For archiving
    virtual void get(const Dof &D,double &udof,int modeNumber) const;
    virtual void get(std::vector<Dof> &R, std::vector<double> &disp,int modeNumber) const;
    virtual void get(partDomain *dom, MElement *ele,std::vector<double> &udofs, const int cc,
                     const nlsField::ElemValue ev, const int gp, const nlsField::ValType vt)const;
                     
  public:
    eigenVectorField(nonLinearMechSolver* sl, eigenSolver* eigS, const int nc,
                     std::vector<nlsField::dataBuildView> &dbview_,
                     const std::string file = "eigenMode");
    virtual ~eigenVectorField();
    virtual void closeArchivingFiles() {};
    virtual void archive(const double time,const int step, const bool forceView);
};



#endif // EIGENMODEFIELD_H_
