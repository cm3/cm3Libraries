//
// C++ Interface: energetic field
//
// Description: Class derived from element field to manage energetic balance
//
//
// Author:  <Gauthier BECKER>, (C) 2010
// <Van Dung NGUYEN>, (C) 2019
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef ENERGETICFIELD_H_
#define ENERGETICFIELD_H_

#include "elementsField.h"
#include "staticDofManager.h"
#include "solverAlgorithms.h"
class nonLinearMechSolver;
class energeticField : public elementsField{
 protected:
  const bool _dynamics; // account for kinetics energy in the total energy
	nonLinearMechSolver* _solver;
  std::string _energyFilePrefix, eneryFractureFilePrefix;
	int _energyComputation,_fractureEnergyComputation;
  int _systemSizeWithoutRigidContact;
  FILE *fpenergy;
  FILE *fpfrac;
  mutable FilterDofSet _rigidContactFilter; // + mpi ghost dof as bool filterDof::operator() is not a const function !!!!
  mutable std::map<Dof,double> _fextn;
  mutable std::map<Dof,double> _dispn;
  mutable std::map<Dof,double> _fextnp1;
  mutable std::map<Dof,double> _dispnp1;
  mutable double _wext;
  double _energy[7]; // keep in memory to get it
  
  int _lastSaveEnergyStep;
  int _lastSaveFractureEnergyStep;
  
  
  void get(partDomain *dom,MElement *ele,std::vector<double> &ener, const int cc,
           const nlsField::ElemValue ev, const int gp, const nlsField::ValType vt)const;
  void openFiles(const std::string energyFile, const std::string eneryFractureFile);
 public:
  enum whichEnergy{
    kinetic=0,
    deformation=1,
    plastic=2,
    external=3,
		damage=4,
    total =5,
    localPathFollowing=6
  };
 #ifndef SWIG
  energeticField(nonLinearMechSolver* sl, std::vector<dataBuildView> &dbview_, 
                 const int energyComp,const int fracComp,
                 nonLinearMechSolver::scheme myscheme,
                 const std::string fileName="energyField",
                 const std::string energyFile="energy", 
                 const std::string eneryFractureFile="fractureEnergy");

  virtual ~energeticField();

  // functions to compute the different parts of energy
  double kineticEnergy() const; // More efficient than a loop on element thanks to vector operation via PETSC
  double kineticEnergy(MElement *ele, const partDomain *dom) const;
  // deformation part
  double deformationEnergy(MElement *ele, const partDomain *dom) const;
  double deformationEnergy() const;
  // plastic part
  double plasticEnergy(MElement *ele, const partDomain *dom) const;
  double plasticEnergy() const;	
  // damage part
  double damageEnergy(MElement *ele, const partDomain *dom) const;
  double damageEnergy() const;
  // pathfollowing part
	double pathFollowingLocalValue(MElement *ele, const partDomain *dom) const;
  double pathFollowingLocalValue() const;
	
  void externalWork()const; // it is stored in a internal variable _wext
  int fractureEnergy(double* arrayEnergy) const; // int return the number of components to archive (max 12) the first one is the total energy

  void setFactorOnArchivingFiles(const int fact);
  double get(whichEnergy we = total) const;
  virtual void resetArchiving();
  virtual void closeArchivingFiles();
  virtual void archive(const double time,const int step, const bool forceView);
  virtual void restart();
 #endif // SWIG
};
#endif // ENERGETICFIELD_H_

