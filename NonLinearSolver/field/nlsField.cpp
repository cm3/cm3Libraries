//
// C++ Interface: field
//
// Description: Base class to manage a field on nodes or elements
// The base class contains archiving data (which is not dependent of the field)
// Author:  <Gauthier BECKER>, (C) 2010
// <Van Dung NGUYEN>, (C) 2019
//
// Copyright: See COPYING file that comes with this distribution
//
//

#include "nlsField.h"
#include <sstream>
#include "nonLinearMechSolver.h"
#include "highOrderTensor.h"

int nlsField::tag = -1;

nlsField::nlsField(const std::string fnn, const int ncomp, std::vector<dataBuildView> &dbview, const bool bn) : 
                _totelem(0), _totelemInterface(0), _numcomp(ncomp), _vBuildView(dbview), _fileNamePrefix(fnn), _binary(bn), 
                _lastViewStep(0)
{
  _numStepBetweenTwoViews = 1;
  // maximal value of the one of all view view
  for(std::vector<dataBuildView>::iterator it=_vBuildView.begin(); it!=_vBuildView.end(); ++it)
  {
    if (it->nbstepArch > _numStepBetweenTwoViews)
    {
      _numStepBetweenTwoViews = it->nbstepArch;
    }
  }
}

nlsField::~nlsField(){}

void nlsField::buildAllView(const nonLinearMechSolver* solver ,const double time, const int numstep, const bool forceView){
  if (_vBuildView.size() > 0)
  {
    if (numstep > _lastViewStep)
    {
      if (forceView or (numstep%_numStepBetweenTwoViews == 0))
      {
        _lastViewStep = numstep;
        // view file
        std::string extension=".msh";
        if (_binary)
        {
          extension = ".dat";
        }
        // file name
        std::string fileName;
        #if defined(HAVE_MPI)
        if (solver->getNumRanks() > 1)
        {
          fileName  = solver->getFileSavingPrefix()+_fileNamePrefix+"_part"+int2str(Msg::GetCommRank())+"_step"+int2str(numstep)+extension;
        }
        else
        #endif //HAVE_MPI
        {
          fileName  = solver->getFileSavingPrefix()+_fileNamePrefix+"_step"+int2str(numstep)+extension;
        }
        
        // write to file
        if (_binary)
        {
          Msg::Error("Binary write not implemented yet");
          return;
        }
        else
        {
          FILE* file = fopen(fileName.c_str(), "w");
          // start file by format version
          // problem: find how to retrieve the last mesh format version
          fprintf(file, "$MeshFormat\n2.2 0 8\n$EndMeshFormat\n"); 
          for(std::vector<dataBuildView>::iterator it=_vBuildView.begin(); it!=_vBuildView.end(); ++it)
          {
            dataBuildView &dbv = *it;
            nlsField::tag = dbv.tag; // to make tag availbale though export view
            fprintf(file, "$%s\n1\n\"%s\"\n1\n%.16g\n4\n%d\n%d\n%ld\n%d\n",this->dataname().c_str(),dbv.viewname.c_str(),time,numstep,_numcomp,_totelem,Msg::GetCommRank());
            this->buildData(file,solver,dbv.comp,dbv.ev,dbv.gpNum,dbv.valtype);
            fprintf(file, "$End%s\n",this->dataname().c_str());
            
            nlsField::tag = -1; // back to default value
          }
          fclose(file);
        }
      }
    }
  }
}

void nlsField::resetArchiving()
{
  _lastViewStep = 0;
}

void nlsField::stepBetweenArchiving(const int nsba)
{
  _numStepBetweenTwoViews = nsba;
}

void nlsField::restart()
{
  restartManager::restart(_lastViewStep);
  restartManager::restart(_numStepBetweenTwoViews);
}

void
nlsField::onelabView(const std::string &meshFileName, const std::string &fname,
                     const nonLinearMechSolver* solver,const std::string &valuename,
                     const int comp,const double time,const int nstep) const
{
  // The onelab file has to contains the mesh info
  // Copy first the mesh file
  std::string copyCommand("cp "); // LINUX ONLY
  copyCommand += meshFileName + " " + fname;
  int ok = system(copyCommand.c_str());

  // add the data at the end of the file
  FILE* fpview = fopen(fname.c_str(),"a");

  // Put the data
  fprintf(fpview, "$%s\n1\n\"%s\"\n1\n%.16g\n4\n%d\n%d\n%ld\n%d\n",this->dataname().c_str(),valuename.c_str(),time,nstep, _numcomp, _totelem,Msg::GetCommRank());

  this->buildData(fpview,solver,comp,nlsField::crude,0,nlsField::val);
  fprintf(fpview, "$End%s\n",this->dataname().c_str());

  fclose(fpview);
  return;
}
