//
//
// Description: eigen mode view data
//
// Author:  <Van Dung NGUYEN>, (C) 2013
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "eigenModeView.h"
#include "partDomain.h"
#include "staticDofManager.h"

eigenVectorField::eigenVectorField(nonLinearMechSolver* sl, eigenSolver* eigS, const int nc,
                   std::vector<nlsField::dataBuildView> &dbview_,
                   const std::string file): nodesField(file,nc,dbview_),_solver(sl), _eigenSolver(eigS)
{
  if (_numStepBetweenTwoViews < _solver->getStepBetweenArchiving())
  {
    _numStepBetweenTwoViews = _solver->getStepBetweenArchiving();
  }

  long int totelem=0;
  std::vector<partDomain*>& allDom = *(_solver->getDomainVector());
  for(std::vector<partDomain*>::iterator itdom=allDom.begin(); itdom!=allDom.end(); ++itdom){
    partDomain *dom = *itdom;
    bool fullDg = dom->getFormulation();
    FunctionSpaceBase *sp = dom->getFunctionSpace();
    std::vector<Dof> R;
    totelem += dom->elementGroupSize();
  }
  this->setTotElem(totelem);
};
eigenVectorField::~eigenVectorField(){
}

void eigenVectorField::get(const Dof &D,double &udof, int modeNumber) const{
  #if defined(HAVE_SLEPC)
  if (_eigenSolver==NULL)
  {
    udof = 0.;
    Msg::Error("eigen solver has not been created");
    return;
  }
  
	if (_solver->getDofManager()->isFixed(D)){
	  _solver->getDofManager()->getFixedDofValue(D,udof);
	  return;
  };
	int comp = _solver->getDofManager()->getDofNumber(D);
	if (comp>=0){
		std::complex<double> val = _eigenSolver->getEigenVectorComp(modeNumber,comp);
		udof = val.real();
	}
	else{
	  std::map<const Dof, const DofAffineConstraint<double>* > constraints;
    _solver->getDofManager()->getAllLinearConstraints(constraints);
	  std::map<const Dof, const DofAffineConstraint<double>* >::iterator it =constraints.find(D);
    if (it != constraints.end())
    {
      udof =  it->second->shift;
      for (unsigned i = 0; i < (it->second)->linear.size(); i++){
        double tmp = 0;
        this->get(((it->second)->linear[i]).first, tmp,modeNumber);
        udof += ((it->second)->linear[i]).second * tmp;
      }
    }
	};
  #else
  udof = 0.;
  #endif //HAVE_SLEPC
};

void eigenVectorField::get(std::vector<Dof> &R, std::vector<double> &disp, int modeNumber) const{
	double du;
	for (int i=0; i<R.size(); i++){
		this->get(R[i],du,modeNumber);
		disp.push_back(du);
	};
};

void eigenVectorField::get(partDomain *dom, MElement *ele,std::vector<double> &udofs, const int cc,
                    const nlsField::ElemValue ev, const int gp, const nlsField::ValType vt)const
{
  std::vector<Dof> R;
  FunctionSpaceBase *sp = dom->getFunctionSpace();
  sp->getKeys(ele,R);
  get(R,udofs,cc);
};


void eigenVectorField::archive(const double time,const int step, const bool forceView) {
  // 
  // solve eigenvalue problem
  if (step > _lastViewStep)
  {
    if (forceView or (step%_numStepBetweenTwoViews == 0))
    {
      if (_eigenSolver!=NULL)
      {
        this->buildAllView(_solver,time,step,forceView);
      }
    }
  }
  //
};