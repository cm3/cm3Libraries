//
// C++ Interface: terms
//
// Description: Class with the displacement field
//
//
// Author:  <Gauthier BECKER>, (C) 2010 <Van Dung NGUYEN>, (C) 2019
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef UNKNOWNFIELD_H_
#define UNKNOWNFIELD_H_
#include <stdint.h>
#include <stdlib.h>
#include "nonLinearBC.h"
#include "nodesField.h"
#include "MInterfaceElement.h"

class contactDomain;
class nonLinearMechSolver;
class unknownField : public nodesField{
  public:
    struct archiveNode{
      int physnum;
      long int nodenum;
      Dof D;
      int _comp;
      int nstep; // number of step between to archive
      nonLinearBoundaryCondition::whichCondition wc;
      FILE *fp;
      bool iscontact;
      // "solver" constructor
      archiveNode(const int pnum, const int nnum,const int comp,const nonLinearBoundaryCondition::whichCondition wv=nonLinearBoundaryCondition::position,
                  const int nstep_=1,bool isc=false ) : physnum(pnum), nodenum(nnum), _comp(comp), wc(wv), nstep(nstep_), fp(NULL),
                                                        iscontact(isc), D(0,0){} // D is initialized after
      // "internal unknownField" constructor
      archiveNode(Dof R, const int pnum, int n, int comp,nonLinearBoundaryCondition::whichCondition wv=nonLinearBoundaryCondition::position,
                  int nstep_=1,bool isc=false) : physnum(pnum),nodenum(n), D(R),_comp(comp), wc(wv), nstep(nstep_), iscontact(isc),fp(NULL)
      {
      }
      archiveNode(const archiveNode &source) : physnum(source.physnum), nodenum(source.nodenum), D(source.D), _comp(source._comp),
                                               nstep(source.nstep), wc(source.wc), fp(source.fp), iscontact(source.iscontact){}
      ~archiveNode(){
        if (fp!=NULL) fclose(fp); fp=NULL;
      };
      void openFile(const std::string prefix){
         // open File
         std::ostringstream oss;
         oss.str("");

         std::string s("");
         if (nodenum == 0){
           oss << physnum;
           s = "AtCorner"+oss.str();
         }
         else{
           if (physnum > 0){
             oss<<physnum;
             s = "Physical"+oss.str();
             oss.str("");
             oss<<nodenum;
             s += "Num"+oss.str();
           }
           else{
            oss<<nodenum;
            s += "Num"+oss.str();
           }
         }
         // component of displacement
         oss.str("");
         oss << _comp;
         std::string s2 = oss.str();
         #if defined(HAVE_MPI)
         if(Msg::GetCommSize() != 1){
           oss.str("");
           oss << Msg::GetCommRank();
           s2 += "_part"+oss.str();
         }
         #endif // HAVE_MPI
         std::string fname;
         switch(wc){
          case nonLinearBoundaryCondition::position:
           fname = prefix+"NodalDisplacement"+s+"comp"+s2+".csv";
           break;
          case nonLinearBoundaryCondition::velocity:
           fname = prefix+"NodalVelocity"+s+"comp"+s2+".csv";
           break;
          case nonLinearBoundaryCondition::acceleration:
           fname = prefix+"NodalAcceleration"+s+"comp"+s2+".csv";
           break;
         }
         if (restartManager::available())
         {
           fp = fopen(fname.c_str(),"a");
         }
         else
         {
          fp = fopen(fname.c_str(),"w");
         }
      };
    };
	protected:
    nonLinearMechSolver* _solver;
    std::vector<archiveNode> varch;
    // for saving
    int _lastSaveStep;
    int _numStepBetweenTwoSaves; 
    
  protected:
    virtual void buildData(FILE* myview, const nonLinearMechSolver* solver ,const int cc, const nlsField::ElemValue ev, const int gp, const nlsField::ValType vt) const;
    // For archiving
    virtual void get(partDomain *dom, MElement *ele,std::vector<double> &udofs, const int cc,
                     const nlsField::ElemValue ev, const int gp, const nlsField::ValType vt)const;
    
  public:
    // update all displacement value
    unknownField(nonLinearMechSolver* s, const int nc, const std::vector<archiveNode> &archiving,
                        std::vector<nlsField::dataBuildView> &dbview_, const std::string="disp");
    virtual ~unknownField();
    
    const std::vector<archiveNode>& getArchiveNodeContainer() const {return varch;};
    // get Operation
    virtual void get(Dof &D,double &udof) const;
    virtual void get(Dof &D, double &udof, nonLinearBoundaryCondition::whichCondition wv) const ;
    virtual void get(std::vector<Dof> &R, std::vector<double> &disp) const;
    virtual void get(std::vector<Dof> &R, fullVector<double> &disp) const;
    virtual void get(std::vector<Dof> &R, std::vector<double> &disp,nonLinearBoundaryCondition::whichCondition wv) const ;
    virtual void get(std::vector<Dof> &R, fullVector<double> &disp,nonLinearBoundaryCondition::whichCondition wv) const ;
    
    virtual void resetArchiving();
    virtual void closeArchivingFiles();
    void setFactorOnArchivingFiles(const int fact);
    virtual void archive(const double time,const int step, const bool force);
    virtual void valuesForOnelab(fullVector<double> &nodalValues) const;
    virtual void restart();
};
#endif // _UNKNOWNFIELD_H_
