//
// C++ Interface: field
//
// Description: Class for a field on nodes (specific functions)
// Author:  <Gauthier BECKER>, (C) 2012,  
// <Van Dung NGUYEN>, (C) 2019
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef NODESFIELD_H_
#define NODESFIELD_H_
#include "nlsField.h"
class nodesField : public nlsField
{
#ifndef SWIG
  protected:
    virtual void buildData(FILE* myview,const nonLinearMechSolver* solver,const int cc,const nlsField::ElemValue ev, const int gp, const nlsField::ValType vt) const;
    virtual std::string dataname() const {return "ElementNodeData";};
 
 public:
    nodesField(const std::string fnn, const int ncomp,std::vector<dataBuildView> &dbview_);
    virtual ~nodesField(){}
#endif // SWIG
};
#endif // NODESFIELD_H_
