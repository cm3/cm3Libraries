//
// C++ Interface: field
//
// Description: Base class to manage a field on nodes or elements
// The base class contains archiving data (which is not dependent of the field)
// Author:  <Gauthier BECKER>, (C) 2010
// <Van Dung NGUYEN>, (C) 2019
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef nlsField_H_
#define nlsField_H_
#ifndef SWIG
#include <string>
#include <stdint.h>
#include "MElement.h"
#include <vector>
#include <iostream>
#include <fstream>
class partDomain;
class nonLinearMechSolver;
#include "elementGroup.h"
#endif // SWIG
class nlsField{
public:
    typedef enum {val=1, increment=2, rate=3} ValType;
    typedef enum {crude=10000, mean=10001, max=10002, min=10003} ElemValue; // enum to select a particular value on element
#ifndef SWIG
    // struct to keep necessary data to build a view
    struct dataBuildView
    {
      int tag; 
      std::string viewname;
      int comp;
      int nbstepArch;
      nlsField::ElemValue ev;
      int gpNum; // gauss point if ev = crude
      nlsField::ValType valtype; // val type, which can be val, increment, or rate
      dataBuildView(const std::string vn, const int cmp,const nlsField::ElemValue ev_, const int gp, const nlsField::ValType vt_,
                    const int nbsa_, int t_=-1): viewname((t_<0)?vn:vn+std::to_string(t_)), comp(cmp), ev(ev_), gpNum(gp), valtype(vt_), nbstepArch(nbsa_), tag(t_){}
      dataBuildView(const dataBuildView &source) : viewname(source.viewname), comp(source.comp),
                                                    ev(source.ev), gpNum(source.gpNum), valtype(source.valtype) , 
                                                    nbstepArch(source.nbstepArch),
                                                    tag(source.tag){}
      dataBuildView& operator=(const dataBuildView &source)
      {
        viewname = source.viewname;
        comp = source.comp;
        ev = source.ev;
        gpNum = source.gpNum;
        valtype = source.valtype;
        nbstepArch=source.nbstepArch;
        tag = source.tag;
        return *this;
      }
    };
    static int tag; 
    
  protected :
    // data needed to build a view
    std::string _fileNamePrefix; // name of file where the displacement are store
    long int _totelem, _totelemInterface; // total number of element present in all elasticFields
    int _numcomp; // number of component per node (ElementNodeData) or element (ElementData), for dataname    
    bool _binary;
    // set with view to archive
    std::vector<dataBuildView>& _vBuildView;    
    // for viewing
    int _lastViewStep;
    int _numStepBetweenTwoViews;
    
  protected:    
    // Specific to elements or nodes
    virtual void buildData(FILE* myview,const nonLinearMechSolver* solver,const int cmp,const nlsField::ElemValue ev_, const int gp, const nlsField::ValType vt_) const=0;
    // func to get value on to archive on an element
    virtual void get(partDomain* dom, MElement *ele, std::vector<double> &fieldData,
                   const int comp,const nlsField::ElemValue ev, const int gp, const nlsField::ValType vt_)const=0; 
    // data name , ElementNodeData or ElementData
    virtual std::string dataname() const=0;
    
     virtual void buildAllView(const nonLinearMechSolver* solver ,const double time, const int stepnum, const bool forceView);
  

  public:
    nlsField(const std::string fnn, const int ncomp, std::vector<dataBuildView> &dbview_, const bool binary_=false);
    virtual ~nlsField();
    void setTotElem(const int ne){_totelem=ne;}
    void setTotElemInterface(const int ne){_totelemInterface=ne;}
    const std::string getFilenamPrefix() const {return _fileNamePrefix;}
    void setFileNamePrefix(const std::string fn){_fileNamePrefix = fn;};
    // in derivate class to choose which component to save
    virtual void archive(const double time,const int step, const bool force)=0;
    virtual void closeArchivingFiles() = 0;
    virtual void stepBetweenArchiving(const int nsba);
    virtual void resetArchiving();
    
    virtual void restart();
    // Onelab
    virtual void onelabView(const std::string &meshFileName, const std::string &fname,
                          const nonLinearMechSolver* solver,const std::string &valuename,
                          const int comp,const double time, const int nstep) const;
#endif // SWIG
};
#endif // nlsField_H_

