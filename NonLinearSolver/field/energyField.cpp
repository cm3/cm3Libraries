//
// C++ Interface: energetic field
//
// Description: Class derived from element field to manage energetic balance
//
//
// Author:  <Gauthier BECKER>, (C) 2010
// <Van Dung NGUYEN>, (C) 2019
// Copyright: See COPYING file that comes with this distribution
//
//
#include "energyField.h"
#include "explicitHulbertChung.h"
#include "ipField.h"
#include "SVector3.h"
#include "unknownField.h"
#include "dofManagerMultiSystems.h"
#include "nonLinearMechSolver.h"
double dot(const std::vector<double> &a, const fullVector<double> &b){
  double c = 0.;
  for(int i =0; i<a.size(); i++){
    c += a[i]*b(i);
  }
  return c;
}

double dot(const std::vector<double> &a, const std::vector<double> &b){
  double c=0.;
  for(int i =0; i<a.size(); i++){
    c += a[i]*b[i];
  }
  return c;
}

void energeticField::openFiles(const std::string energyFile, const std::string eneryFractureFile)
{
  if(_energyComputation>0)
	{
    std::string fname;
    #if defined(HAVE_MPI)
    if(_solver->getNumRanks() > 1){
      std::ostringstream oss;
      oss << Msg::GetCommRank();
      fname =_solver->getFileSavingPrefix()+energyFile+"_part" + oss.str() + ".csv";
    }
    else
    #endif // HAVE_MPI
    {
      fname = _solver->getFileSavingPrefix()+energyFile+".csv";
    }
    // initialize file to store energy
    if (restartManager::available())
    {
      fpenergy = fopen(fname.c_str(),"a");
    }
    else
    {
      fpenergy = fopen(fname.c_str(),"w");
      fprintf(fpenergy,"Time;Kinetic;Deformation;Plastic;Wext;Damage;Total;LocalPathFollowing\n");
    }
  }

  if(_fractureEnergyComputation>0)
	{
    std::string fname;
    #if defined(HAVE_MPI)
    if(_solver->getNumRanks() > 1){
      std::ostringstream oss;
      oss << Msg::GetCommRank();
      fname = _solver->getFileSavingPrefix()+eneryFractureFile+"_part" + oss.str() + ".csv";
    }
    else
    #endif // HAVE_MPI
    {
      fname = _solver->getFileSavingPrefix()+eneryFractureFile+".csv";
    }
    if (restartManager::available())
    {
      fpfrac = fopen(fname.c_str(),"a");
    }
    else
    {
      fpfrac = fopen(fname.c_str(),"w");
      fprintf(fpfrac,"Time;Total;Array of value\n");
    }
  }
}


energeticField::energeticField(nonLinearMechSolver* sl, std::vector<dataBuildView> &dbview_,
                               const int energyComp,const int fracComp,
                               nonLinearMechSolver::scheme myscheme,
                               const std::string fileName,
                               const std::string energyFile,
                               const std::string eneryFractureFile) : elementsField(fileName,1,dbview_),
                                                       _dynamics((myscheme == nonLinearMechSolver::StaticLinear ||
                                                                  myscheme == nonLinearMechSolver::StaticNonLinear) ? false : true),
                                                       _energyComputation(energyComp),_fractureEnergyComputation(fracComp),
                                                        _wext(0.), _rigidContactFilter(),fpenergy(NULL), _systemSizeWithoutRigidContact(0),
                                                        fpfrac(NULL), _solver(sl), _lastSaveEnergyStep(0),_lastSaveFractureEnergyStep(0),
                                                        _energyFilePrefix(energyFile), eneryFractureFilePrefix(eneryFractureFile)
{
	std::vector<partDomain*> &domvec = *(_solver->getDomainVector());
	nonLinearMechSolver::contactContainer &vcdom = *(_solver->getAllContactDomain());
  if (_numStepBetweenTwoViews < _solver->getStepBetweenArchiving())
  {
    _numStepBetweenTwoViews = _solver->getStepBetweenArchiving();
  }

  openFiles(_energyFilePrefix,eneryFractureFilePrefix);

	// set system size without rigid contact
  _systemSizeWithoutRigidContact = _solver->getDofManager()->getManager(0)->getFirstRigidContactUnknowns();
	
	// avoid this HOW ??
	long int nelem=0;
	for(std::vector<partDomain*>::iterator itdom=domvec.begin(); itdom!= domvec.end();++itdom){
		partDomain *dom = *itdom;
		nelem+=dom->elementGroupSize();
	}
	this->setTotElem(nelem);

	// init non linear system to get the external work
	std::string Aname("A");
	linearSystem<double> *lsys = _solver->getDofManager()->getManager(0)->getLinearSystem(Aname);
	nonLinearSystem<double>* nlsys = dynamic_cast<nonLinearSystem<double>*>(lsys);
	if(nlsys!=NULL)
	{
		// Wext for prescribed displacement (but without rigid contact body)
		std::vector<Dof> R;
		for(nonLinearMechSolver::contactContainer::iterator itC = vcdom.begin(); itC!=vcdom.end();++itC)
		{
			contactDomain *cdom = *itC;
			rigidContactSpaceBase *rspace = static_cast<rigidContactSpaceBase*>(cdom->getSpace());
			rspace->getKeysOfGravityCenter(R);
			_rigidContactFilter.addDof(R);
		}
		R.clear();
		_solver->getDofManager()->getManager(0)->getFixedDof(R);
		for(int i=0; i<R.size(); i++){
			if(!_rigidContactFilter(R[i])){
			 #if defined(HAVE_MPI)
				if(R[i].getType()<0)  // dof another rank
				{
					_rigidContactFilter.addDof(R[i]);
				}
				else
			 #endif // HAVE_MPI
				{
					_fextn.insert(std::pair<Dof,double>(R[i],0.));
					_dispn.insert(std::pair<Dof,double>(R[i],0.));
					_fextnp1.insert(std::pair<Dof,double>(R[i],0.));
					_dispnp1.insert(std::pair<Dof,double>(R[i],0.));
				}
			}
		}
	}
	else
	{
		Msg::Info("Wext cannot be computed for a linear system! The value will be set to 0");
	}
}

energeticField::~energeticField()
{
  if(fpenergy!=NULL) fclose(fpenergy); fpenergy= NULL;
  if(fpfrac!=NULL) fclose(fpfrac);  fpfrac = NULL;
}


double energeticField::kineticEnergy() const {
  if(!_dynamics)  return 0.; // system is not dynamic --> kinetic energy = 0
  else 
    return _solver->getDofManager()->getManager(0)->getKineticEnergy(_systemSizeWithoutRigidContact,_rigidContactFilter);
}

double energeticField::kineticEnergy(MElement *ele, const partDomain *dom) const
{
  double ener=0.;
  if(_dynamics)
  {
    // 0.5 uDotT*M*uDot
    fullMatrix<double> mass;
    BilinearTermBase* massterm = dom->getBilinearMassTerm();
    IntPt* GP;
    int npts = dom->getBulkGaussIntegrationRule()->getIntPoints(ele,&GP);
    massterm->get(ele,npts,GP,mass);
    // get Dof
    std::vector<Dof> R;
    std::vector<double> velocities;
    const FunctionSpaceBase *sp = dom->getFunctionSpace();
    sp->getKeys(ele,R);    
    _solver->getDofManager()->getDofValue(R,velocities,nonLinearBoundaryCondition::velocity);
    
    for (int i=0; i< R.size(); i++)
    {
      for (int j=0; j< R.size(); j++)
      {
        ener += 0.5*velocities[i]*mass(i,j)*velocities[j];
      }
    }

  }
  return ener;
}

double energeticField::deformationEnergy(MElement *ele, const partDomain *dom) const{
  MInterfaceElement* iele = dynamic_cast<MInterfaceElement*>(ele);
  if (iele){
    const dgPartDomain* dgdom = dynamic_cast<const dgPartDomain*>(dom);
    return _solver->getIPField()->computeInterfaceDeformationEnergy(ele,dgdom,IPStateBase::current);
  }
  else{
    return _solver->getIPField()->computeBulkDeformationEnergy(ele,dom,IPStateBase::current);
  }
}

double energeticField::deformationEnergy() const{
  return _solver->getIPField()->computeDeformationEnergy(IPStateBase::current);
}

double energeticField::plasticEnergy(MElement *ele, const partDomain *dom) const{
  MInterfaceElement* iele = dynamic_cast<MInterfaceElement*>(ele);
  if (iele){
    const dgPartDomain* dgdom = dynamic_cast<const dgPartDomain*>(dom);
    return _solver->getIPField()->computeInterfacePlasticEnergy(ele,dgdom,IPStateBase::current);
  }
  else{
    return _solver->getIPField()->computeBulkPlasticEnergy(ele,dom,IPStateBase::current);
  }
}

double energeticField::plasticEnergy() const{
  return _solver->getIPField()->computePlasticEnergy(IPStateBase::current);
}


double energeticField::damageEnergy(MElement *ele, const partDomain *dom) const{
  MInterfaceElement* iele = dynamic_cast<MInterfaceElement*>(ele);
  if (iele){
    const dgPartDomain* dgdom = dynamic_cast<const dgPartDomain*>(dom);
    return _solver->getIPField()->computeInterfaceDamageEnergy(ele,dgdom,IPStateBase::current);
  }
  else{
    return _solver->getIPField()->computeBulkDamageEnergy(ele,dom,IPStateBase::current);
  }
}

double energeticField::damageEnergy() const{
  return _solver->getIPField()->computeDamageEnergy(IPStateBase::current);
}


double energeticField::pathFollowingLocalValue(MElement *ele, const partDomain *dom) const{
  if (_solver->withPathFollowing()){
    MInterfaceElement* iele = dynamic_cast<MInterfaceElement*>(ele);
    if (iele){
      const dgPartDomain* dgdom = dynamic_cast<const dgPartDomain*>(dom);
      return _solver->getIPField()->computeInterfacePathFollowingLocalValue(ele,dgdom,IPStateBase::current);
    }
    else{
      return _solver->getIPField()->computeBulkPathFollowingLocalValue(ele,dom,IPStateBase::current);
    }
  }
  else{
    return 0.;
  }
}

double energeticField::pathFollowingLocalValue() const{
  if (_solver->withPathFollowing()){
    return _solver->getIPField()->computePathFollowingLocalValue(IPStateBase::current);
  }
  else{
    return 0.;
  }
};

void energeticField::externalWork()const{ // for multiSystem takes only the first one (displacement)
  double Deltawext=0.;
	std::string Aname("A");
	linearSystem<double> *lsys = _solver->getDofManager()->getManager(0)->getLinearSystem(Aname);
	nonLinearSystem<double>* nlsys = dynamic_cast<nonLinearSystem<double>*>(lsys);

  if(nlsys!=NULL){
    Deltawext = 2*nlsys->getExternalWork(_systemSizeWithoutRigidContact); // as we divided the value by 2 later !!!

    // Now compute the work of external forces due to prescribed displacement (-Wint - Winertia)
    // Do the scalar product on vector (more efficient) TO DO THE SCALAR PRODUCT WITH BLAS HOW ??
    std::vector<double> disp;
    std::vector<double> acc;
    std::vector<double> _mass;
    std::vector<double> forceval;
    std::vector<Dof> R2;
    std::vector<double> forceval2;
    _solver->getDofManager()->getManager(0)->getFixedRightHandSide(R2,forceval2);

    std::vector<Dof> R;
    for(int i=0;i<R2.size();i++){
      if(!_rigidContactFilter(R2[i])){
        R.push_back(R2[i]);
        forceval.push_back(forceval2[i]);
      }
    }
    R2.clear();
    forceval2.clear();
    _solver->getUnknownField()->get(R,disp);
    // Compute inertial forces if needed
    if(_solver->getDofManager()->getManager(0)->getScheme() == nonLinearMechSolver::Explicit ||
       _solver->getDofManager()->getManager(0)->getScheme() == nonLinearMechSolver::Implicit)
    { // otherwise static cases and no inertial forces
      _solver->getUnknownField()->get(R,acc,nonLinearBoundaryCondition::acceleration);
      _solver->getDofManager()->getManager(0)->getVertexMass(R,_mass);
      for(int i=0; i<forceval.size(); i++)
        forceval[i] -= _mass[i]*acc[i];
    }
    for(int i=0;i<R.size(); i++){
      _fextnp1[R[i]] = forceval[i];
      forceval[i] += _fextn[R[i]];
      _dispnp1[R[i]] = disp[i];
      disp[i] -= _dispn[R[i]];
    }
    Deltawext += dot(forceval,disp);
    // swap value (next step)
    _fextn.swap(_fextnp1);
    _dispn.swap(_dispnp1);

    // New value of Wext W = 0.5*F*u
    _wext += 0.5*Deltawext;
  }
}

int energeticField::fractureEnergy(double*  arrayEnergy) const
{
  return _solver->getIPField()->computeFractureEnergy(arrayEnergy,IPStateBase::current);
}


void energeticField::get(partDomain *dom,MElement *ele,std::vector<double> &ener, const int cc,
                         const nlsField::ElemValue ev, const int gp, const nlsField::ValType vt)const{
  switch(cc){
   case 0:
    ener[0] = this->kineticEnergy(ele,dom);
    break;
   case 1:
    ener[0] = this->deformationEnergy(ele,dom);
    break;
   case 2:
    ener[0] = this->plasticEnergy(ele,dom);
    break;
   case 3:
    ener[0] = _wext; // false fix this (How and Interrest ?)
		break;
   case 4:
    ener[0] = this->damageEnergy(ele,dom);
		break;
	 case 5:
		ener[0] = this->kineticEnergy(ele,dom) + this->deformationEnergy(ele,dom);
		break;
   case 6:
    ener[0] = this->pathFollowingLocalValue(ele,dom);
    break;
   default:
    Msg::Error("energy type %d is not defined",cc);
		break;
  }
};

double energeticField::get(whichEnergy we) const
{
  switch(we)
  {
    case kinetic: case deformation:
    case plastic: case external: case damage: case total: case localPathFollowing:
      return _energy[we];
    default:
      Msg::Error("You try to get an energy that does not exist");
  }
  return 0.;
}

void energeticField::closeArchivingFiles()
{
  if (fpenergy!=NULL) fclose(fpenergy);
  if (fpfrac!=NULL) fclose(fpfrac);
  fpenergy = NULL;
  fpfrac = NULL;
};

void energeticField::resetArchiving()
{
  elementsField::resetArchiving();
  _lastSaveEnergyStep = 0;
  _lastSaveFractureEnergyStep = 0;
  if (fpenergy!=NULL) fclose(fpenergy);
  if (fpfrac!=NULL) fclose(fpfrac);
  // open new files
  openFiles(_energyFilePrefix,eneryFractureFilePrefix);
}


void energeticField::archive(const double time,const int step, const bool forceView) {
  // The increment has to be computed each step to have a good approximation of the integral !!
  if(_energyComputation >0)
    this->externalWork();
  // msh file
  this->buildAllView(_solver,time,step,forceView);
  if ((step > _lastSaveEnergyStep) and (_energyComputation > 0))
  {
    if (forceView or (step%_energyComputation == 0))
    {
      _lastSaveEnergyStep = step;
       // without MPI transfert user has to sum the contribution of all processors at the end
      _energy[0] = this->kineticEnergy();
      _energy[1] = this->deformationEnergy();
      _energy[2] = this->plasticEnergy();
      _energy[3] = _wext;
      _energy[4] = this->damageEnergy();
      _energy[5] = _energy[0]+_energy[1];
			_energy[6] = this->pathFollowingLocalValue();

      fprintf(fpenergy,"%.16g;%.16g;%.16g;%.16g;%.16g;%.16g;%.16g;%.16g\n",time,_energy[0], _energy[1], _energy[2], _energy[3],_energy[4],_energy[5],_energy[6]);
      fflush(fpenergy);
    }
  }

  if ((step > _lastSaveFractureEnergyStep) and (_fractureEnergyComputation > 0))
  {
    if (forceView or (step%_fractureEnergyComputation==0))
    {
      static double arrayEnergy[10];
      int ncomp = this->fractureEnergy(arrayEnergy);
      if(ncomp>10)
      {
        Msg::Error("You try to archive more than 10 values for the fracture energy. Please update the variable ncomp and arrayEnergy and the IPField::computeFractureEnergy function. Only the 10 first variables will be stored");
        ncomp=10;
      }
      fprintf(fpfrac,"%.16g;",time);
      if(ncomp==0)
      {
        fprintf(fpfrac,"0;");
      }
      for(int i=0;i<ncomp;i++)
      {
        fprintf(fpfrac,"%.16g;",arrayEnergy[i]);
      }
      fprintf(fpfrac,"\n");
      fflush(fpfrac);
    }
  }
}

void energeticField::setFactorOnArchivingFiles(const int fact)
{
  _energyComputation *= fact;
  _fractureEnergyComputation *= fact;
}

void energeticField::restart()
{
  elementsField::restart();
  restartManager::restart(_lastSaveEnergyStep);
  restartManager::restart(_lastSaveFractureEnergyStep);
  return;
}
