//
// C++ Interface: field
//
// Description: Class for a field on nodes (specific functions)
// Author:  <Gauthier BECKER>, (C) 2012, 
//<Van Dung NGUYEN>, (C) 2019
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "nodesField.h"
#include "nonLinearMechSolver.h"
nodesField::nodesField(const std::string fnn, const int ncomp,
               std::vector<dataBuildView> &dbview_) : 
               nlsField(fnn,ncomp,dbview_){};

void nodesField::buildData(FILE* myview, const nonLinearMechSolver* solver, const int cc, const nlsField::ElemValue ev, const int gp, const nlsField::ValType vt) const
{
  const std::vector<partDomain*>& vdom = *(solver->getDomainVector());
  std::vector<double> fieldData;
  for(std::vector<partDomain*>::const_iterator itdom=vdom.begin(); itdom!=vdom.end(); ++itdom){
    partDomain *dom = *itdom;
    for (elementGroup::elementContainer::const_iterator it = dom->element_begin(); it != dom->element_end(); ++it){
      MElement *ele = it->second;
      int numv = ele->getNumVertices();
      this->get(dom,ele,fieldData,cc,ev,gp,vt);
      fprintf(myview, "%ld %d",ele->getNum(),numv);
      for(int i=0;i<numv;i++)
        for(int j=0;j<_numcomp;j++){
          int ij = i+j*numv;
					if (ij < fieldData.size())
						fprintf(myview, " %.16g",fieldData[i+j*numv]);
					else
						fprintf(myview, " %.16g",0.0);
        }
      fprintf(myview,"\n");
      fieldData.clear();
    }
  }
}
