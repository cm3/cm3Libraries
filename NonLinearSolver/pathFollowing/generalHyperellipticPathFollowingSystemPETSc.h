//
//
// Description: base class for path following system using PETSc
//
// Author:  <Van Dung NGUYEN>, (C) 2018
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef GENERALHYPERELLIPTICPATHFOLLOWINGSYSTEMPETSC_H_
#define GENERALHYPERELLIPTICPATHFOLLOWINGSYSTEMPETSC_H_

#include "pathFollowingSystemPETSc.h"

template<class scalar>
class generalHyperellipticPathFollowingSystemPETSC : public pathFollowingSystemPETSC<scalar>{
  
  #if defined(HAVE_PETSC)
  private:
    PetscErrorCode  VecScale_Vec(Vec x, Vec fact) const {
      int N;
      _try(VecGetSize(x,&N));
      PetscScalar* array;
      _try(VecGetArray(x,&array));
      
      PetscScalar* alpha;
      _try(VecGetArray(fact,&alpha));
      
      for (int i=0; i< N; i++){
        array[i] *= alpha[i];
      }
      
      PetscFunctionReturn(0); 
    }
    
    PetscErrorCode  VecDot_Vec(Vec x, Vec y, Vec fact, scalar* res) const {
      int NX, NY;
      _try(VecGetSize(x,&NX));
      _try(VecGetSize(y,&NY));
      if (NX != NY) Msg::Error("VecDot_Vec must be performed with two vectors with same sizes");
      
      PetscScalar* arrayX, *arrayY;
      _try(VecGetArray(x,&arrayX));
      _try(VecGetArray(y,&arrayY));
      
      PetscScalar* alpha;
      _try(VecGetArray(fact,&alpha));
      
      (*res) = 0.;
      for (int i=0; i< NX; i++){
        (*res) += alpha[i]*arrayX[i]*arrayY[i];
      }
      PetscFunctionReturn(0); 
    }
  
  protected:
    Vec _H; // diagonal elliptic matrix
    bool _unassambledH;
      
  public:
    generalHyperellipticPathFollowingSystemPETSC(MPI_Comm com = PETSC_COMM_WORLD):pathFollowingSystemPETSC<scalar>(com),_unassambledH(false){}
    virtual ~generalHyperellipticPathFollowingSystemPETSC(){};
    
    virtual void allocate(int nbRows){
      pathFollowingSystemPETSC<scalar>::allocate(nbRows);
      _try(VecDuplicate(this->_b,&_H));
      _try(VecAssemblyBegin(_H));
			_try(VecAssemblyEnd(_H));
			_try(VecZeroEntries(_H));
    };

    virtual void clear(){
      if (pathFollowingSystemPETSC<scalar>::isAllocated()){
        pathFollowingSystemPETSC<scalar>::clear();
				_try(VecDestroy(&_H));
      };
    };
    
    virtual int systemSolve(){
      if (_unassambledH)
      {
        _unassambledH = false;
        _try(VecAssemblyBegin(this->_H));
        _try(VecAssemblyEnd(this->_H));
      }
      
      
      #if defined HAVE_MPI
      int comSize = 1;
      MPI_Comm_size(this->getComm(),&comSize);
			if (Msg::GetCommRank() == this->_rootRank)
      #endif //HAVE_MPI
			{	
				if (this->_iter == 0){
					// PREDICTOR 
					/** solve to find v now is _x**/
					_try(VecAssemblyBegin(this->_q));
					_try(VecAssemblyEnd(this->_q));
					_try(VecCopy(this->_q,this->_b));
					int ok = linearSystemPETSc<scalar>::systemSolve();
					if (ok != 1)
          {
            return 0;
          }
					scalar vTv;
					_try(VecDot_Vec(this->_x,this->_x,_H,&vTv));
					if (!this->_setScale){
						this->_scale = sqrt(vTv);
						this->scale2  = vTv;
						this->_setScale = true;
					}
					scalar A =  this->_aU*vTv/this->scale2+this->_aL;
					scalar sqrtA = sqrt(A);
					
					if (this->getTranversalCriterion() == pathFollowingSystemBase::POSITIVE_EXTERNAL_WORK){
						scalar qTv;
						_try(VecDot(this->_q,this->_x,&qTv));
						qTv *= this->_controlParameterPrev;
						if (qTv < 0){
							this->_controlStep = -this->_pseudoTimeIncrement/sqrtA;
						}
						else{
							this->_controlStep = this->_pseudoTimeIncrement/sqrtA;
						}
					}
					else if (this->getTranversalCriterion() == pathFollowingSystemBase::SMALLEST_DIRECTION_CHANGE){
						scalar uprevTu;
						_try(VecDot(this->_stateStepPrevious,this->_x,&uprevTu));
						if (uprevTu<0.){
							this->_controlStep = -this->_pseudoTimeIncrement/sqrtA;
						}
						else {
							this->_controlStep = this->_pseudoTimeIncrement/sqrtA;
						}
						
					}
					else{
						Msg::Error("getTranversalCriterion() = %d has not been implemented",this->getTranversalCriterion());
					}

					// update
					this->_controlParameter += this->_controlStep;
					_try(VecAXPY(this->_stateStep,this->_controlStep,this->_x));
					_try(VecAXPY(this->_xsol,this->_controlStep,this->_x));
				}
				else{
					// CORRECTOR
					// compute RHS
					if(!this->_flagb){
						#if defined(HAVE_MPI)
						if(comSize > 1)
						{
							_try(VecAssemblyBegin(this->_q));
							_try(VecAssemblyEnd(this->_q));
							_try(VecAssemblyBegin(this->_Fint));
							_try(VecAssemblyEnd(this->_Fint));
							_try(VecAssemblyBegin(this->_Fext));
							_try(VecAssemblyEnd(this->_Fext));
						}
						#endif // HAVE_MPI
						_try(VecCopy(this->_Fext,this->_b));
						_try(VecAXPY(this->_b,-1.,this->_Fint));
						this->_flagb = true;
					}
					// solve system
					int ok = linearSystemPETSc<scalar>::systemSolve();
          if (ok != 1)
          {
            return 0;
          }
					// copy _x to dr
					_try(VecCopy(this->_x,this->dr));
					/** solve to find v now is _x**/
					_try(VecCopy(this->_q,this->_b));
					ok = linearSystemPETSc<scalar>::systemSolve();
          if (ok != 1)
          {
            return 0;
          }
					
					if (this->getCorrectionMethod() == pathFollowingSystemBase::DIRECT_NR){							
						scalar vTdu, drTdu, duTdu;
						_try(VecDot_Vec(this->_x,this->_stateStep,_H,&vTdu));
						_try(VecDot_Vec(this->dr,this->_stateStep,_H,&drTdu));
						_try(VecDot_Vec(this->_stateStep,this->_stateStep,_H,&duTdu));
						
						scalar c = this->_aU*duTdu/this->scale2+this->_aL*this->_controlStep*this->_controlStep - this->_pseudoTimeIncrement*this->_pseudoTimeIncrement;
						scalar dlambda = - (c+2.*this->_aU*drTdu/this->scale2)/(2.*this->_aU*vTdu/this->scale2+2.*this->_aL*this->_controlStep);
						
						this->_controlStep += dlambda;
						this->_controlParameter += dlambda;
						
						_try(VecAXPY(this->_stateStep,1.,this->dr));
						_try(VecAXPY(this->_xsol,1.,this->dr));
						
						_try(VecAXPY(this->_stateStep,dlambda,this->_x));
						_try(VecAXPY(this->_xsol,dlambda,this->_x));
						
					}
					else if (this->getCorrectionMethod() == pathFollowingSystemBase::CONSTRAINT_BASED){
						scalar A;
						_try(VecDot(this->_x,this->_x,&A));

						A *= this->_aU;
						A /= this->scale2;
						A += this->_aL;

						/** compute D, E, F**/

						scalar vTdu,drTdr, vTdr, duTdr, duTdu;
						_try(VecDot_Vec(this->_x,this->_stateStep,_H,&vTdu));
						_try(VecDot_Vec(this->dr,this->dr,_H,&drTdr));
						_try(VecDot_Vec(this->_x,this->dr,_H,&vTdr));
						_try(VecDot_Vec(this->_stateStep,this->dr,_H,&duTdr));
						_try(VecDot_Vec(this->_stateStep,this->_stateStep,_H,&duTdu));

						/** du += dr**/
						/** xsol += dr**/
						_try(VecAXPY(this->_stateStep,1.,this->dr));
						_try(VecAXPY(this->_xsol,1.,this->dr));

						/** compute B = dlambda + vT*(du + dr)/scale^2**/

						scalar B;
						_try(VecDot_Vec(this->_x,this->_stateStep,_H,&B));
						B *= this->_aU;
						B /= this->scale2;
						B += (this->_controlStep*this->_aL);


						/** compute C **/
						scalar C;
						_try(VecDot_Vec(this->_stateStep,this->_stateStep,_H,&C));
						C*= this->_aU;
						C /= this->scale2;
						double dlamda2 = this->_controlStep*this->_controlStep*this->_aL - this->_pseudoTimeIncrement*this->_pseudoTimeIncrement;

						C += dlamda2;

						/** solve equation Ax*x + 2*B*x +C = 0 **/
						scalar delta = B*B - A*C;
						if (delta <0)  {
							Msg::Error("path following has not solution because delta = %e ",delta);
							return 0;
						}

						scalar rdelta = sqrt(delta);
						scalar a1 = (-1.*B - rdelta)/A;
						scalar a2 = (-1.*B + rdelta)/A;
						scalar theta1 = a1*vTdu;
						scalar theta2 = a2*vTdu;

						scalar s = 0.;
						if (theta1>=theta2){
							s = a1;
						}
						else
							s = a2;

						this->_controlStep += s;
						this->_controlParameter += s;
						_try(VecAXPY(this->_stateStep,s,this->_x));
						_try(VecAXPY(this->_xsol,s,this->_x));
						
					}
					else if (this->getCorrectionMethod() == pathFollowingSystemBase::SAFE_CONSTRAINT_BASED){
						scalar A;
						_try(VecDot(this->_x,this->_x,&A));

						A *= this->_aU;
						A /= this->scale2;
						A += this->_aL;

						/** compute D, E, F**/

						scalar vTdu,drTdr, vTdr, duTdr, duTdu;
						_try(VecDot_Vec(this->_x,this->_stateStep,_H,&vTdu));
						_try(VecDot_Vec(this->dr,this->dr,_H,&drTdr));
						_try(VecDot_Vec(this->_x,this->dr,_H,&vTdr));
						_try(VecDot_Vec(this->_stateStep,this->dr,_H,&duTdr));
						_try(VecDot_Vec(this->_stateStep,this->_stateStep,_H,&duTdu));


						scalar D = this->_aU*this->_aU*(vTdr/this->scale2)*(vTdr/this->scale2) - A*drTdr/this->scale2*this->_aU;
						scalar E = (this->_aL*this->_controlStep+ this->_aU*vTdu/this->scale2)*(vTdr/this->scale2)*this->_aU - A*duTdr/this->scale2*this->_aU;
						scalar F = (this->_aL*this->_controlStep+ this->_aU*vTdu/this->scale2)*(this->_aL*this->_controlStep+ this->_aU*vTdu/this->scale2)
														-A*(this->_aU*duTdu/this->scale2+this->_aL*this->_controlStep*this->_controlStep - this->_pseudoTimeIncrement*this->_pseudoTimeIncrement);

						double delta = E*E -D*F;
						scalar _beta = 1.;

						if (fabs(D)>0){
							_beta = 0.99*(-E-sqrt(delta))/D;
							//Msg::Error("b1 = %f, b2 = %f",(-E-sqrt(delta))/D,_beta);
						}
						else
							_beta = F/(2.*E);

						if (_beta>1) _beta = 1.;



						/** du += beta*dr**/
						/** xsol += beta*dr**/
						_try(VecAXPY(this->_stateStep,_beta,this->dr));
						_try(VecAXPY(this->_xsol,_beta,this->dr));

						/** compute B = dlambda + vT*(du + dr)/scale^2**/

						scalar B;
						_try(VecDot_Vec(this->_x,this->_stateStep,_H,&B));
						B *= this->_aU;
						B /= this->scale2;
						B += (this->_controlStep*this->_aL);


						/** compute C **/
						scalar C;
						_try(VecDot_Vec(this->_stateStep,this->_stateStep,_H,&C));
						C*= this->_aU;
						C /= this->scale2;
						double dlamda2 = this->_controlStep*this->_controlStep*this->_aL - this->_pseudoTimeIncrement*this->_pseudoTimeIncrement;

						C += dlamda2;

						/** solve equation Ax*x + 2*B*x +C = 0 **/
						delta = B*B - A*C;
						if (delta <0)  {
							Msg::Error("relaxation error %e ",delta);
							return 0;
						}

						double rdelta = sqrt(delta);
						double a1 = (-1.*B - rdelta)/A;
						double a2 = (-1.*B + rdelta)/A;
						double theta1 = a1*vTdu;
						double theta2 = a2*vTdu;

						double s = 0.;
						if (theta1>=theta2){
							s = a1;
						}
						else
							s = a2;

						this->_controlStep += s;
						this->_controlParameter += s;
						_try(VecAXPY(this->_stateStep,s,this->_x));
						_try(VecAXPY(this->_xsol,s,this->_x));
					}
					else{
						Msg::Error("this->getCorrectionMethod() = %d has not been implemented",this->getCorrectionMethod());
					}
			
				}
				this->_iter++;
				
			}
      return 1;
    }
    
    virtual double getPathFollowingConstraintRelativeRedidual() const {
			if (this->getCorrectionMethod() == pathFollowingSystemBase::DIRECT_NR){
				scalar duTdu;
				_try(VecDot_Vec(this->_stateStep,this->_stateStep,_H,&duTdu));
				double RelCr = fabs(this->_aU*duTdu/this->scale2+this->_aL*this->_controlStep*this->_controlStep - this->_pseudoTimeIncrement*this->_pseudoTimeIncrement)/(this->_pseudoTimeIncrement*this->_pseudoTimeIncrement);
				return RelCr;
			}
			else{
				return 0.;
			}
		};
    
    virtual void addToHyperellipticMatrix(const int row, const int col, const double val) {
      if (row != col) {
        Msg::Error("only digonal hyperelliptic matrix is allowed");
      }
      else{
        //Msg::Info("row = %d val = %e",row,val);
        PetscInt i = row;
        scalar s = val;
        _try(VecSetValues(_H, 1, &i, &s, INSERT_VALUES));       
        _unassambledH = true;
      }
    };
  #else 
  public:
    generalHyperellipticPathFollowingSystemPETSC():pathFollowingSystemPETSC<scalar>(){};
    virtual ~generalHyperellipticPathFollowingSystemPETSC(){}
  #endif // HAVE_PETSC
};
#endif // GENERALHYPERELLIPTICPATHFOLLOWINGSYSTEMPETSC_H_



