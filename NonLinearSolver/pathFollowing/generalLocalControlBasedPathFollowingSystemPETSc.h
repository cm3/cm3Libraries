//
//
// Description: general class for path following system using PETSc
//
// Author:  <Van Dung NGUYEN>, (C) 2016
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef GENERALLOCALCONTROLBASEDPATHFOLLOWINGSYSTEMPETSC_H
#define GENERALLOCALCONTROLBASEDPATHFOLLOWINGSYSTEMPETSC_H

// solve path following system
// fint(u) - lambda* q = 0
// phi(u,lambda) - s = 0;
// _g = dphi/du
// _h = dphi/dlambda

#include "pathFollowingSystem.h"
#include "nonLinearSystems.h"


template<class scalar>
class generalLocalControlBasedPathFollowingSystemPETSc :  public nonLinearSystemPETSc<scalar>,
																				 public pathFollowingSystem<scalar>{
	#if defined(HAVE_PETSC)
	protected:
		Vec _q;  									// force vector
		scalar _PFC; // path following constraint
		Vec _g;
		scalar _h; //
		//
    double _controlParameter; // current control variable
    double _controlParameterPrev; // previous control variable
		double _pseudoTimeIncrement; 	// arc length

	public:
		generalLocalControlBasedPathFollowingSystemPETSc(MPI_Comm com = PETSC_COMM_WORLD):nonLinearSystemPETSc<scalar>(com),
						pathFollowingSystem<scalar>(),
					_PFC(0.),_h(0.),_controlParameter(0.),_controlParameterPrev(0.),
					_pseudoTimeIncrement(0.){};
		virtual ~generalLocalControlBasedPathFollowingSystemPETSc(){}
    
    virtual void restart()
    {
      nonLinearSystemPETSc<scalar>::restart();
      restartManager::restart(_controlParameter);
      restartManager::restart(_controlParameterPrev);
    }

    virtual void setPathFollowingIncrement(const double dt) 
    {
      _pseudoTimeIncrement = dt;
      if (this->_controlType == pathFollowingSystemBase::LOAD_CONTROL)
      {
         // if load control, _pseudoTimeIncrement is the prescribed loa
	       _controlParameter += _pseudoTimeIncrement;
	    }
    };

    virtual void allocate(int nbRows)
    {
      nonLinearSystemPETSc<scalar>::allocate(nbRows);
      _try(VecDuplicate(this->_b,&_q));
      _try(VecDuplicate(this->_b,&_g));
    };

    virtual void clear()
    {
      if (nonLinearSystemPETSc<scalar>::isAllocated())
      {
        nonLinearSystemPETSc<scalar>::clear();
        _try(VecDestroy(&_q));
	      _try(VecDestroy(&_g));
	     }
    }

    virtual void addToLoadVector(int row, const scalar& val){
      PetscInt i = row;
      PetscScalar s = val;
      _try(VecSetValues(_q, 1, &i, &s, ADD_VALUES));
    }
    virtual void zeroLoadVector()
    {
	    _try(VecAssemblyBegin(_q));
	    _try(VecAssemblyEnd(_q));
    	_try(VecZeroEntries(_q));
    };

    virtual double getControlParameter() const 
    {
      return _controlParameter;
    };
    virtual double getControlParameterStep() const 
    {
	    return (_controlParameter - _controlParameterPrev);
    };

    virtual double getStateParameter() const 
    {
      PetscReal nor;
      _try(VecAssemblyBegin(this->_xsol));
      _try(VecAssemblyEnd(this->_xsol));
      _try(VecNorm(this->_xsol, NORM_INFINITY, &nor));
      return nor;
    };

    virtual void addToPathFollowingContraint(const scalar& val)
    {
	    _PFC += val;
    };

    virtual void zeroMatrix()
    {
      nonLinearSystemPETSc<scalar>::zeroMatrix();
      _try(VecAssemblyBegin(_g));
      _try(VecAssemblyEnd(_g));
      _try(VecZeroEntries(_g));
      _h = 0.;
    };

    virtual void nextStep()
    {
      nonLinearSystemPETSc<scalar>::nextStep();
      _controlParameterPrev = _controlParameter;
      _PFC = 0.;
    }

    virtual void resetControlParameter() {
			_controlParameter = 0;
			_controlParameterPrev = 0;
		};

    virtual void resetUnknownsToPreviousTimeStep()
    {
      nonLinearSystemPETSc<scalar>::resetUnknownsToPreviousTimeStep();
      _controlParameter = _controlParameterPrev;
      _PFC = 0.;
    }

    virtual void zeroRightHandSide()
    {
	    nonLinearSystemPETSc<scalar>::zeroRightHandSide();
	    _PFC = 0.;
    };

    virtual void addToPathFollowingConstraint(const scalar& val){
	    _PFC += val;
    };
    virtual void addToDPathFollowingConstraintDUnknown(const int row, const scalar& val){
      PetscInt i = row;
      PetscScalar s = val;
      _try(VecSetValues(_g, 1, &i, &s, ADD_VALUES));
    };
    virtual void addToDPathFollowingConstraintDControlParameter(const scalar& val) {
	    _h += val;
    };

    virtual int systemSolve()
    {
      #if defined HAVE_MPI
	    int comSize = 1;
	    MPI_Comm_size(this->getComm(),&comSize);
      if (Msg::GetCommRank() == this->_rootRank)
      #endif //HAVE_MPI
	    {
	      if (this->getControlType() == pathFollowingSystemBase::LOAD_CONTROL)
	      {
	        // solve system
	        if(!this->_flagb)
	        {
	          #if defined(HAVE_MPI)
            if(comSize > 1){
		          _try(VecAssemblyBegin(this->_q));
		          _try(VecAssemblyEnd(this->_q));
		          _try(VecAssemblyBegin(this->_Fint));
		          _try(VecAssemblyEnd(this->_Fint));
		          _try(VecAssemblyBegin(this->_Fext));
		          _try(VecAssemblyEnd(this->_Fext));
	          }
	          #endif // HAVE_MPI
	          _try(VecCopy(this->_Fext,this->_b));
	          _try(VecAXPY(this->_b,-1.,this->_Fint));
	          this->_flagb = true;
	        }
	        int ok = linearSystemPETSc<scalar>::systemSolve();
          if (ok != 1)
          {
            return 0;
          }
	        _try(VecAXPY(this->_xsol,1.,this->_x));
	      }
	      else
	      {
	        // other control than load one
	        if(!this->_flagb)
	        {
	          #if defined(HAVE_MPI)
	          if(comSize > 1){
              _try(VecAssemblyBegin(this->_q));
              _try(VecAssemblyEnd(this->_q));
              _try(VecAssemblyBegin(this->_Fint));
              _try(VecAssemblyEnd(this->_Fint));
              _try(VecAssemblyBegin(this->_Fext));
              _try(VecAssemblyEnd(this->_Fext));
	          }
	          #endif // HAVE_MPI
	          _try(VecCopy(this->_Fext,this->_b));
	          _try(VecAXPY(this->_b,-1.,this->_Fint));
	          this->_flagb = true;
	        }
	        int ok = linearSystemPETSc<scalar>::systemSolve();
          if (ok != 1)
          {
            return 0;
          }
	        _try(VecAXPY(this->_xsol,1,this->_x));
          
          _try(VecAssemblyBegin(this->_g));
          _try(VecAssemblyEnd(this->_g));
	        PetscScalar gTdr(0.);
	        _try(VecDot(_g,this->_x,&gTdr));

	        // compute v;
	        _try(VecCopy(_q,this->_b));
	        ok = linearSystemPETSc<scalar>::systemSolve();
          if (ok != 1)
          {
            return 0;
          }
	        PetscScalar gTv=0.;
	        _try(VecDot(_g,this->_x,&gTv));

	        PetscScalar totalPFC = _PFC;
	        #if defined(HAVE_MPI)
	        if (comSize > 1){
	        // if comSize > 1 it means that MPI_COMM_WORLD is used
	           MPI_Allreduce(&_PFC,&totalPFC,1,MPI_DOUBLE,MPI_SUM,MPI_COMM_WORLD);
	        }
	        #endif //HAVE_MPI

	        PetscScalar dlambda = -(totalPFC-_pseudoTimeIncrement+gTdr)/(gTv+_h);

	        _controlParameter += dlambda;
	        _try(VecAXPY(this->_xsol,dlambda,this->_x));

	        //Msg::Info("_PFC = %e, pseudo incre = %e dlambda = %e",_PFC,_pseudoTimeIncrement,dlambda);
	        }
	      }
	    return 1;
    }
    virtual double normInfRightHandSide() const
    {
	    double normInfRHS = 0.;
	    #if defined(HAVE_MPI)
	    int comSize = 1;
      MPI_Comm_size(this->getComm(),&comSize);
	    if (Msg::GetCommRank() == this->_rootRank)
      #endif //HAVE_MPI
	    {
	      if(!this->_flagb)
	      {
          #if defined(HAVE_MPI)
          if(comSize > 1)
          {
            _try(VecAssemblyBegin(this->_q));
            _try(VecAssemblyEnd(this->_q));
            _try(VecAssemblyBegin(this->_Fint));
            _try(VecAssemblyEnd(this->_Fint));
            _try(VecAssemblyBegin(this->_Fext));
            _try(VecAssemblyEnd(this->_Fext));
          }
          #endif // HAVE_MPI
	        _try(VecCopy(this->_Fext,this->_b));
	        _try(VecAXPY(this->_b,-1.,this->_Fint));
          this->_flagb = true;
        }
        normInfRHS = linearSystemPETSc<scalar>::normInfRightHandSide();
	    }
      #if defined HAVE_MPI
	    if (comSize == 1 and this->_otherRanks.size() > 0)
	    {
	      MPI_Bcast(&normInfRHS,1,MPI_DOUBLE,0,MPI_COMM_WORLD);
	    }
	    #endif // HAVE_MPI
	    return normInfRHS;
    }
    virtual double norm0Inf() const
    {
      double norm0RHS= 0.;
      #if defined(HAVE_MPI)
	    int comSize = 1;
      MPI_Comm_size(this->getComm(),&comSize);

			if (Msg::GetCommRank() == this->_rootRank)
			#endif //HAVE_MPI
			{
				PetscReal norq;
        PetscReal norFint;
        _try(VecNorm(_q, NORM_INFINITY, &norq));
        _try(VecNorm(this->_Fint, NORM_INFINITY, &norFint));
        norm0RHS = norFint + fabs(this->getControlParameter())*norq;
			}

      #if defined HAVE_MPI
			if ((comSize == 1) and (this->_otherRanks.size() > 0)){
				MPI_Bcast(&norm0RHS,1,MPI_DOUBLE,0,MPI_COMM_WORLD);
			}
			#endif //HAVE_MPI
			return norm0RHS;
    }

		virtual double getPathFollowingConstraintRelativeRedidual() const {
			if (this->getControlType() == pathFollowingSystemBase::LOAD_CONTROL){
				return 0.;
			}
			else{
				double totalPFC = _PFC;
				#if defined(HAVE_MPI)
				if (Msg::GetCommSize() >1){
					MPI_Allreduce(&_PFC,&totalPFC,1,MPI_DOUBLE,MPI_SUM,MPI_COMM_WORLD);
				}
				#endif // HAVE_MPI

				return fabs(totalPFC- _pseudoTimeIncrement)/_pseudoTimeIncrement;
			}
		};
    virtual void addToHyperellipticMatrix(const int row, const int col, const double val) {};
        virtual void resetScaleParameter(const double val){};
	#else
		generalLocalControlBasedPathFollowingSystemPETSc(): nonLinearSystemPETSc<scalar>(),pathFollowingSystem<scalar>(){};
		virtual ~generalLocalControlBasedPathFollowingSystemPETSc(){}
		virtual double getControlParameter() const {return 0;};
		virtual double getControlParameterStep() const {return 0.;};
    virtual void setPathFollowingIncrement(const double dt) {};
    virtual double getStateParameter() const {return 0.;};
		virtual void resetControlParameter() {};
		virtual void resetScaleParameter(const double val){};
		virtual void addToPathFollowingContraint(const scalar& val) {};
		virtual void addToDPathFollowingConstraintDUnknown(const int col, const scalar& val) {};
		virtual void addToDPathFollowingConstraintDControlParameter(const scalar& val) {};
		virtual double getPathFollowingConstraintRelativeRedidual() const {return 0.;};
    virtual void addToHyperellipticMatrix(const int row, const int col, const double val) {};
	#endif // HAVE_PETSC
};

#endif // GENERALLOCALCONTROLBASEDPATHFOLLOWINGSYSTEMPETSC_H


