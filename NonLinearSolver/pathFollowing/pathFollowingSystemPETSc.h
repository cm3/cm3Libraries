//
//
// Description: base class for path following system using PETSc
//
// Author:  <Van Dung NGUYEN>, (C) 2012
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef PATHFOLLOWINGSYSTEMPETSC_H_
#define PATHFOLLOWINGSYSTEMPETSC_H_

#include "pathFollowingSystem.h"
#include "nonLinearSystemPETSc.h"

/**
  // pathfollowing method with unknown-based control based on hyperspherical constraint
  // _aU*_stateStep^T*_stateStep/scale2 + _aL*_controlStep*_controlStep = _pseudoTimeIncrement*_pseudoTimeIncrement
	// _aU=1, _aL=1 general hyperspherical constraint
	// _aU=1, _aL=0 state control
	// _aU=0, _al=1 load control
**/

template<class scalar>
class pathFollowingSystemPETSC : public nonLinearSystemPETSc<scalar>,
                                  public pathFollowingSystem<scalar>{
	
		
  #if defined(HAVE_PETSC)
  protected:
    Vec _q;  // force vector
    Vec _stateStep;  // state step
    Vec _stateStepPrevious;

    double _controlParameter; // current control variable
    double _controlParameterPrev; // previous control variable

    double _controlStep; // control step
		double _pseudoTimeIncrement; // radius of hyperspherical plane

		double _aU, _aL;  // parameter for control type
		int _iter; // to specify preditor phase (iter=0) or corrector phase (iter>0)
		//  scaling value used in pathfollowing constraint in order to obtained non-dimensional equation
		bool _setScale; // flag scale
		double _scale; // scale parameter in constraint
		double scale2; // _scale*_scale
				
		// temp vectors to avoid reallocated
		Vec dr; // dr = invK*(Fext-Fint)

  public:
    pathFollowingSystemPETSC(MPI_Comm com = PETSC_COMM_WORLD):nonLinearSystemPETSc<scalar>(com),
                       pathFollowingSystem<scalar>(),_controlParameterPrev(0.),_controlParameter(0.),
                       _scale(1.),scale2(1.),_controlStep(0),_setScale(false),_iter(0), _aU(1.),_aL(1.),
                       _pseudoTimeIncrement(0.){}
    virtual ~pathFollowingSystemPETSC(){
      this->clear();
    }
    
    virtual void restart()
    {
      nonLinearSystemPETSc<scalar>::restart();
      restartManager::restart(_controlParameter);
      restartManager::restart(_controlParameterPrev);
      
      restart_petsc_array(_stateStep,this->_nbRows);
      restart_petsc_array(_stateStepPrevious,this->_nbRows);
    }

    virtual void setControlType(const int i){
			pathFollowingSystemBase::setControlType(i);
      if (i == 0){
        // state control
        _aU = 1.;
        _aL = 0.;
      }
      else if (i == 1){
        // arc_length control
        _aU = 1.;
        _aL = 1.;
      }
      else if (i== 2){
        // load control
        _aU = 0.;
        _aL = 1;
      }
      else{
        Msg::Error(" this method is not valid");
      }
    }
		
    virtual void setPathFollowingIncrement(const double dt) {
       _pseudoTimeIncrement = dt;
    };

    virtual void allocate(int nbRows){
      nonLinearSystemPETSc<scalar>::allocate(nbRows);
      _try(VecDuplicate(this->_b,&_q));
      _try(VecDuplicate(this->_b,&_stateStep));
      _try(VecDuplicate(this->_b,&_stateStepPrevious));
			
			// temp vecs
			_try(VecDuplicate(this->_b,&dr));
    };

    virtual void clear(){
      if (nonLinearSystemPETSc<scalar>::isAllocated()){
        nonLinearSystemPETSc<scalar>::clear();
        _try(VecDestroy(&_q));
        _try(VecDestroy(&_stateStep));
        _try(VecDestroy(&_stateStepPrevious));
				
				_try(VecDestroy(&dr));
      };
    };

    virtual void addToLoadVector(int row, const scalar& val){
      PetscInt i = row;
      scalar s = val;
      _try(VecSetValues(_q, 1, &i, &s, ADD_VALUES));
    }
		virtual void zeroLoadVector(){
			_try(VecAssemblyBegin(_q));
			_try(VecAssemblyEnd(_q));
			_try(VecZeroEntries(_q));
		};

    virtual double getControlParameter() const {
      return _controlParameter;
    };
		virtual double getControlParameterStep() const {
			return _controlStep;
		};

    virtual double getStateParameter() const {
      PetscReal nor;
      VecAssemblyBegin(this->_xsol);
      VecAssemblyEnd(this->_xsol);
      _try(VecNorm(this->_xsol, NORM_INFINITY, &nor));
      return nor;
    };
		
		// constraint is defined from unknown spaces
		virtual void addToPathFollowingContraint(const scalar& val){};
		virtual void addToDPathFollowingConstraintDUnknown(const int col, const scalar& val){};
		virtual void addToDPathFollowingConstraintDControlParameter(const scalar& val) {};

    virtual void nextStep(){
      nonLinearSystemPETSc<scalar>::nextStep();
      _controlParameterPrev = _controlParameter;
      _try(VecCopy(_stateStep,_stateStepPrevious));

      _try(VecAssemblyBegin(_stateStep));
      _try(VecAssemblyEnd(_stateStep));
      _try(VecZeroEntries(_stateStep));

      _controlStep = 0.;
      _iter = 0;
    }
		
		virtual void resetControlParameter() {
			_controlParameterPrev = 0.;
			_controlParameter = 0.;
		};

    virtual void resetUnknownsToPreviousTimeStep(){
      nonLinearSystemPETSc<scalar>::resetUnknownsToPreviousTimeStep();
      _controlParameter = _controlParameterPrev;
      _try(VecAssemblyBegin(_stateStep));
      _try(VecAssemblyEnd(_stateStep));
      _try(VecZeroEntries(_stateStep));
			
      _controlStep = 0.;
      _iter = 0;
    }

    virtual int systemSolve(){
      #if defined HAVE_MPI
      int comSize = 1;
      MPI_Comm_size(this->getComm(),&comSize);
			if (Msg::GetCommRank() == this->_rootRank)
      #endif //HAVE_MPI
			{	
				if (_iter == 0){
					// PREDICTOR 
					/** solve to find v now is _x**/
					_try(VecAssemblyBegin(_q));
					_try(VecAssemblyEnd(_q));
					_try(VecCopy(_q,this->_b));
					int ok = linearSystemPETSc<scalar>::systemSolve();
          if (ok!=1)
          {
            return 0;
          }
					
					scalar vTv;
					_try(VecDot(this->_x,this->_x,&vTv));
					if (!_setScale){
						_scale = sqrt(vTv);
						scale2  = vTv;
						_setScale = true;
					}
					scalar A =  _aU*vTv/scale2+_aL;
					scalar sqrtA = sqrt(A);
					
					if (this->getTranversalCriterion() == pathFollowingSystemBase::POSITIVE_EXTERNAL_WORK){
						scalar qTv;
						_try(VecDot(_q,this->_x,&qTv));
						qTv *= _controlParameterPrev;
						if (qTv < 0){
							_controlStep = -_pseudoTimeIncrement/sqrtA;
						}
						else{
							_controlStep = _pseudoTimeIncrement/sqrtA;
						}
					}
					else if (this->getTranversalCriterion() == pathFollowingSystemBase::SMALLEST_DIRECTION_CHANGE){
						scalar uprevTu;
						_try(VecDot(_stateStepPrevious,this->_x,&uprevTu));
						if (uprevTu<0.){
							_controlStep = -_pseudoTimeIncrement/sqrtA;
						}
						else {
							_controlStep = _pseudoTimeIncrement/sqrtA;
						}
						
					}
					else{
						Msg::Error("getTranversalCriterion() = %d has not been implemented",this->getTranversalCriterion());
					}

					// update
					_controlParameter += _controlStep;
					_try(VecAXPY(_stateStep,_controlStep,this->_x));
					_try(VecAXPY(this->_xsol,_controlStep,this->_x));
					
					if (this->_withBFGS)
          {
            Msg::Info("BFGS iteration = %d BFGSDeltas.size=%d BFGSGammas.size = %d",
                    this->_BFGSCounter, this->_BFGSDeltas.size(),this->_BFGSGammas.size());
            if (this->_BFGSCounter == 0)
            {
              if (this->_BFGSDeltas.find(this->_BFGSCounter+1) == this->_BFGSDeltas.end())
              {
                Vec& v = this->_BFGSDeltas[this->_BFGSCounter+1];
                _try(VecDuplicate(_stateStep, &v));
              }
              _try(VecCopy(_stateStep, this->_BFGSDeltas[this->_BFGSCounter+1]));
              _try(VecCopy(this->_Fint, this->_lastResidual));
              _try(VecAXPY(this->_Fext,-1.,this->_lastResidual));
            }
            else
            {
              Msg::Error("BFGS must be restarted at the beginning of time step");
            }
            this->_BFGSCounter += 1;
          }
          
				}
				else{
					// CORRECTOR
					// compute RHS
					if(!this->_flagb){
						#if defined(HAVE_MPI)
						if(comSize > 1)
						{
							_try(VecAssemblyBegin(this->_q));
							_try(VecAssemblyEnd(this->_q));
							_try(VecAssemblyBegin(this->_Fint));
							_try(VecAssemblyEnd(this->_Fint));
							_try(VecAssemblyBegin(this->_Fext));
							_try(VecAssemblyEnd(this->_Fext));
						}
						#endif // HAVE_MPI
						_try(VecCopy(this->_Fext,this->_b));
						_try(VecAXPY(this->_b,-1.,this->_Fint));
						this->_flagb = true;
					}
					
					if (this->_withBFGS)
          {
            Msg::Info("BFGS iteration = %d BFGSDeltas.size=%d BFGSGammas.size = %d iter=%d",
                    this->_BFGSCounter, this->_BFGSDeltas.size(), this->_BFGSGammas.size(),_iter);
            if (this->_BFGSCounter == 0)
            {
              // solve system
					    int ok = linearSystemPETSc<scalar>::systemSolve();
              if (ok!=1)
              {
                return 0;
              }
					    // copy _x to dr
					    _try(VecCopy(this->_x,dr));
					    if (this->_BFGSDeltas.find(this->_BFGSCounter+1) == this->_BFGSDeltas.end())
              {
                Vec& v = this->_BFGSDeltas[this->_BFGSCounter+1];
                _try(VecDuplicate(_stateStep, &v));
              }
              _try(VecCopy(this->_x, this->_BFGSDeltas[this->_BFGSCounter+1]));
              _try(VecCopy(this->_Fint, this->_lastResidual));
              _try(VecAXPY(this->_Fext,-1.,this->_lastResidual));
					    
					    
					    /** solve to find v now is _x**/
					    _try(VecCopy(_q,this->_b));
					    ok = linearSystemPETSc<scalar>::systemSolve();
					    
              if (ok != 1)
              {
                return 0;
              }
            }
            else
            {
              double val;
              Vec qTemp;
              _try(VecDuplicate(this->_b, &qTemp));
              _try(VecCopy(this->_b, qTemp));
              _try(VecScale(qTemp, -1));        
              
              if (this->_BFGSGammas.find(this->_BFGSCounter) == this->_BFGSGammas.end())
              {
                Vec& v = this->_BFGSGammas[this->_BFGSCounter];
                _try(VecDuplicate(this->_x, &v));
              }
              _try(VecAXPBYPCZ(this->_BFGSGammas[this->_BFGSCounter], 1., -1., 0., qTemp, this->_lastResidual));
              _try(VecDot(this->_BFGSDeltas[this->_BFGSCounter], this->_BFGSGammas[this->_BFGSCounter], &val));
              this->_BFGSRhos[this->_BFGSCounter] = 1./val;
              
              _try(VecCopy(qTemp, this->_lastResidual));        
              //
              std::vector<double> alphas(this->_BFGSCounter,0.);
              for (int j= this->_BFGSCounter-1; j>-1; j--)
              {
                _try(VecDot(this->_BFGSDeltas[j+1], qTemp, &val));
                alphas[j] = this->_BFGSRhos[j+1]*val;
                _try(VecAXPY(qTemp, -alphas[j], this->_BFGSGammas[j+1]));
              }
              _try(VecCopy(qTemp, this->_b));
              int ok = linearSystemPETSc<scalar>::systemSolve();
              if (ok != 1)
              {
                return 0;
              }
              for (int j=1; j< this->_BFGSCounter+1; j++)
              {
                _try(VecDot(this->_BFGSGammas[j], this->_x, &val));
                _try(VecAXPY(this->_x, alphas[j-1]-this->_BFGSRhos[j]*val, this->_BFGSDeltas[j]));
              }
               // update solution
              _try(VecScale(this->_x, -1.));
              if (this->_BFGSDeltas.find(this->_BFGSCounter+1) == this->_BFGSDeltas.end())
              {
                Vec& v = this->_BFGSDeltas[this->_BFGSCounter+1];
                _try(VecDuplicate(this->_x, &v));
              }
              _try(VecCopy(this->_x, this->_BFGSDeltas[this->_BFGSCounter+1]));
              
              // copy _x to dr
					    _try(VecCopy(this->_x,dr));
					    //
					    
					    // second solve
              _try(VecCopy(this->_q, qTemp));        
              for (int j= this->_BFGSCounter-1; j>-1; j--)
              {
                _try(VecDot(this->_BFGSDeltas[j+1], qTemp, &val));
                alphas[j] = this->_BFGSRhos[j+1]*val;
                _try(VecAXPY(qTemp, -alphas[j], this->_BFGSGammas[j+1]));
              }
              _try(VecCopy(qTemp, this->_b));
              ok = linearSystemPETSc<scalar>::systemSolve();
              if (ok != 1)
              {
                return 0;
              }
              for (int j=1; j< this->_BFGSCounter+1; j++)
              {
                _try(VecDot(this->_BFGSGammas[j], this->_x, &val));
                _try(VecAXPY(this->_x, alphas[j-1]-this->_BFGSRhos[j]*val, this->_BFGSDeltas[j]));
              }
              _try(VecDestroy(&qTemp));
            }
            this->_BFGSCounter += 1;
          }
          else
          { 
					  // solve system
					  int ok = linearSystemPETSc<scalar>::systemSolve();
            if (ok!=1)
            {
              return 0;
            }
					  // copy _x to dr
					  _try(VecCopy(this->_x,dr));
					  /** solve to find v now is _x**/
					  _try(VecCopy(_q,this->_b));
					  ok = linearSystemPETSc<scalar>::systemSolve();
					  
            if (ok != 1)
            {
              return 0;
            }
          }
					
					if (this->getCorrectionMethod() == pathFollowingSystemBase::DIRECT_NR){							
						scalar vTdu, drTdu, duTdu;
						_try(VecDot(this->_x,_stateStep,&vTdu));
						_try(VecDot(dr,_stateStep,&drTdu));
						_try(VecDot(_stateStep,_stateStep,&duTdu));
						
						scalar c = _aU*duTdu/scale2+_aL*_controlStep*_controlStep - _pseudoTimeIncrement*_pseudoTimeIncrement;
						scalar dlambda = - (c+2.*_aU*drTdu/scale2)/(2.*_aU*vTdu/scale2+2.*_aL*_controlStep);
						
						_controlStep += dlambda;
						_controlParameter += dlambda;
						
						_try(VecAXPY(_stateStep,1.,dr));
						_try(VecAXPY(this->_xsol,1.,dr));
						
						_try(VecAXPY(_stateStep,dlambda,this->_x));
						_try(VecAXPY(this->_xsol,dlambda,this->_x));
						
					}
					else if (this->getCorrectionMethod() == pathFollowingSystemBase::CONSTRAINT_BASED){
						scalar A;
						_try(VecDot(this->_x,this->_x,&A));

						A *= _aU;
						A /= scale2;
						A += _aL;

						/** compute D, E, F**/

						scalar vTdu,drTdr, vTdr, duTdr, duTdu;
						_try(VecDot(this->_x,_stateStep,&vTdu));
						_try(VecDot(dr,dr,&drTdr));
						_try(VecDot(this->_x,dr,&vTdr));
						_try(VecDot(_stateStep,dr,&duTdr));
						_try(VecDot(_stateStep,_stateStep,&duTdu));

						/** du += dr**/
						/** xsol += dr**/
						_try(VecAXPY(_stateStep,1.,dr));
						_try(VecAXPY(this->_xsol,1.,dr));

						/** compute B = dlambda + vT*(du + dr)/scale^2**/

						scalar B;
						_try(VecDot(this->_x,_stateStep,&B));
						B *= _aU;
						B /= scale2;
						B += (_controlStep*_aL);


						/** compute C **/
						scalar C;
						_try(VecDot(_stateStep,_stateStep,&C));
						C*= _aU;
						C /= scale2;
						double dlamda2 = _controlStep*_controlStep*_aL - _pseudoTimeIncrement*_pseudoTimeIncrement;

						C += dlamda2;

						/** solve equation Ax*x + 2*B*x +C = 0 **/
						scalar delta = B*B - A*C;
						if (delta <0)  {
							Msg::Error("path following has not solution because delta = %e ",delta);
							return 0;
						}

						scalar rdelta = sqrt(delta);
						scalar a1 = (-1.*B - rdelta)/A;
						scalar a2 = (-1.*B + rdelta)/A;
						scalar theta1 = a1*vTdu;
						scalar theta2 = a2*vTdu;

						scalar s = 0.;
						if (theta1>=theta2){
							s = a1;
						}
						else
							s = a2;

						_controlStep += s;
						_controlParameter += s;
						_try(VecAXPY(_stateStep,s,this->_x));
						_try(VecAXPY(this->_xsol,s,this->_x));
						
					}
					else if (this->getCorrectionMethod() == pathFollowingSystemBase::SAFE_CONSTRAINT_BASED){
						scalar A;
						_try(VecDot(this->_x,this->_x,&A));

						A *= _aU;
						A /= scale2;
						A += _aL;

						/** compute D, E, F**/

						scalar vTdu,drTdr, vTdr, duTdr, duTdu;
						_try(VecDot(this->_x,_stateStep,&vTdu));
						_try(VecDot(dr,dr,&drTdr));
						_try(VecDot(this->_x,dr,&vTdr));
						_try(VecDot(_stateStep,dr,&duTdr));
						_try(VecDot(_stateStep,_stateStep,&duTdu));


						scalar D = _aU*_aU*(vTdr/scale2)*(vTdr/scale2) - A*drTdr/scale2*_aU;
						scalar E = (_aL*_controlStep+ _aU*vTdu/scale2)*(vTdr/scale2)*_aU - A*duTdr/scale2*_aU;
						scalar F = (_aL*_controlStep+ _aU*vTdu/scale2)*(_aL*_controlStep+ _aU*vTdu/scale2)
														-A*(_aU*duTdu/scale2+_aL*_controlStep*_controlStep - _pseudoTimeIncrement*_pseudoTimeIncrement);

						double delta = E*E -D*F;
						scalar _beta = 1.;

						if (fabs(D)>0){
							_beta = 0.99*(-E-sqrt(delta))/D;
							//Msg::Error("b1 = %f, b2 = %f",(-E-sqrt(delta))/D,_beta);
						}
						else
							_beta = F/(2.*E);

						if (_beta>1) _beta = 1.;



						/** du += beta*dr**/
						/** xsol += beta*dr**/
						_try(VecAXPY(_stateStep,_beta,dr));
						_try(VecAXPY(this->_xsol,_beta,dr));

						/** compute B = dlambda + vT*(du + dr)/scale^2**/

						scalar B;
						_try(VecDot(this->_x,_stateStep,&B));
						B *= _aU;
						B /= scale2;
						B += (_controlStep*_aL);


						/** compute C **/
						scalar C;
						_try(VecDot(_stateStep,_stateStep,&C));
						C*= _aU;
						C /= scale2;
						double dlamda2 = _controlStep*_controlStep*_aL - _pseudoTimeIncrement*_pseudoTimeIncrement;

						C += dlamda2;

						/** solve equation Ax*x + 2*B*x +C = 0 **/
						delta = B*B - A*C;
						if (delta <0)  {
							Msg::Error("relaxation error %e ",delta);
							return 0;
						}

						double rdelta = sqrt(delta);
						double a1 = (-1.*B - rdelta)/A;
						double a2 = (-1.*B + rdelta)/A;
						double theta1 = a1*vTdu;
						double theta2 = a2*vTdu;

						double s = 0.;
						if (theta1>=theta2){
							s = a1;
						}
						else
							s = a2;

						_controlStep += s;
						_controlParameter += s;
						_try(VecAXPY(_stateStep,s,this->_x));
						_try(VecAXPY(this->_xsol,s,this->_x));
					}
					else{
						Msg::Error("this->getCorrectionMethod() = %d has not been implemented",this->getCorrectionMethod());
					}
			
				}
				_iter++;
				
			}
      return 1;
    }

    virtual double normInfRightHandSide() const{
      double normInfRHS = 0.;
      #if defined(HAVE_MPI)
      int comSize = 1;
      MPI_Comm_size(this->getComm(),&comSize);
			if (Msg::GetCommRank() == this->_rootRank)
      #endif //HAVE_MPI
      {
				// compute right hand side
        if(!this->_flagb){
          #if defined(HAVE_MPI)
          if(comSize > 1)
          {
            _try(VecAssemblyBegin(this->_q));
            _try(VecAssemblyEnd(this->_q));
            _try(VecAssemblyBegin(this->_Fint));
						_try(VecAssemblyEnd(this->_Fint));
						_try(VecAssemblyBegin(this->_Fext));
						_try(VecAssemblyEnd(this->_Fext));
					}
					#endif // HAVE_MPI
					_try(VecCopy(this->_Fext,this->_b));
					_try(VecAXPY(this->_b,-1.,this->_Fint));
          this->_flagb = true;
        }
        normInfRHS = linearSystemPETSc<scalar>::normInfRightHandSide();
      }

      #if defined(HAVE_MPI)
      if ((comSize ==1) and (this->_otherRanks.size() > 0)){
        MPI_Bcast(&normInfRHS,1,MPI_DOUBLE,0,MPI_COMM_WORLD);
      }
      #endif // HAVE_MPI
      return normInfRHS;
    }

    virtual double norm0Inf() const{
      double norm0RHS= 0.;
      #if defined(HAVE_MPI)
      int comSize = 1;
      MPI_Comm_size(this->getComm(),&comSize);
      if (Msg::GetCommRank() == this->_rootRank)
      #endif //HAVE_MPI
      {
        PetscReal norq;
        PetscReal norFint;
        _try(VecNorm(_q, NORM_INFINITY, &norq));
        _try(VecNorm(this->_Fint, NORM_INFINITY, &norFint));
        norm0RHS = norFint + fabs(this->getControlParameter())*norq;
      }

      #if defined(HAVE_MPI)
      if ((comSize ==1) and (this->_otherRanks.size() > 0)){
         MPI_Bcast(&norm0RHS,1,MPI_DOUBLE,0,MPI_COMM_WORLD);
      }
      #endif // HAVE_MPI
      return norm0RHS;
    }
		
		virtual double getPathFollowingConstraintRelativeRedidual() const {
			double RelCr = 0;
			#if defined HAVE_MPI
			int comSize = 1;
      MPI_Comm_size(this->getComm(),&comSize);
      if (Msg::GetCommRank() == this->_rootRank)
      #endif //HAVE_MPI
      {
        if (this->getCorrectionMethod() == pathFollowingSystemBase::DIRECT_NR){
					scalar duTdu;
					_try(VecDot(_stateStep,_stateStep,&duTdu));
					RelCr = fabs(_aU*duTdu/scale2+_aL*_controlStep*_controlStep - _pseudoTimeIncrement*_pseudoTimeIncrement)/(_pseudoTimeIncrement*_pseudoTimeIncrement);
				}
      }

      #if defined(HAVE_MPI)
      if ((comSize ==1) and (this->_otherRanks.size() > 0)){
         MPI_Bcast(&RelCr,1,MPI_DOUBLE,0,MPI_COMM_WORLD);
      }
      #endif // HAVE_MPI			
			return RelCr;
		};
    
    virtual void addToHyperellipticMatrix(const int row, const int col, const double val) {};
    virtual void resetScaleParameter(const double val){};

  #else
  public:
    pathFollowingSystemPETSC():nonLinearSystemPETSc<scalar>(),pathFollowingSystem<scalar>(){};
		virtual ~pathFollowingSystemPETSC(){}
    virtual double getControlParameter() const {return 0;};
		virtual double getControlParameterStep() const {return 0.;};
    virtual void setPathFollowingIncrement(const double dt) {};
    virtual double getStateParameter() const {return 0.;};
		virtual void resetControlParameter() {};
		virtual void resetScaleParameter(const double val){};
		virtual void addToPathFollowingContraint(const scalar& val) {};
		virtual void addToDPathFollowingConstraintDUnknown(const int col, const scalar& val) {};
		virtual void addToDPathFollowingConstraintDControlParameter(const scalar& val) {};
		virtual double getPathFollowingConstraintRelativeRedidual() const {return 0.;};
    virtual void addToHyperellipticMatrix(const int row, const int col, const double val) {};
  #endif //HAVE_PETSC
};

#endif // PATHFOLLOWINGSYSTEMPETSC_H_
