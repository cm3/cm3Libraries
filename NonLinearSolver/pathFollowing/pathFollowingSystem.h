//
//
// Description: base class for path following system
//
// Author:  <Van Dung NGUYEN>, (C) 2012
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef PATHFOLLOWINGSYSTEM_H_
#define PATHFOLLOWINGSYSTEM_H_

// general pathfollowing system to solve problem
// f_int - lambda*q = 0
// Phi - s = 0;
// lambda == getControlParameter
// q == load vector
// s == setPathFollowingIncrement (arclength)
// Phi == path following left hand side
// dPhi/du == path following constraint vector

class pathFollowingSystemBase{
	public:
		enum ControlType {STATE_CONTROL=0, ARC_LENGTH_CONTROL=1, LOAD_CONTROL=2, LOCAL_CONTROL = 3};
		//different path-following control
		enum CorrectorMethod{DIRECT_NR=0, CONSTRAINT_BASED=1,SAFE_CONSTRAINT_BASED=2};
		// DIRECT_NR --> direct correction with NR 
		// CONSTRAINT_BASED --> correction by resolving the path following constraint leading a quadratic equation of load increment
		// SAFE_CONSTRAINT_BASED-> CONSTRAINT_BASED with a safe parameter 
		
		enum TraversalCriterion{POSITIVE_EXTERNAL_WORK=0,SMALLEST_DIRECTION_CHANGE=1};
		// POSITIVE_EXTERNAL_WORK proposed by Bergan et al 1978
		// SMALLEST_DIRECTION_CHANGE proposed by Bellini and Chulya 1987
		
		enum SolverType{ONE_RHS,TWO_RHS}; // only for LOCAL_CONTROL, otherwise TWO_RHS is used
		// ONE_RHS--> only one righ-hand-side for displacement and load factor
		// TWO_RHS--> two RHSs are seperately considered, displacement is condensed first, load factor is solved
		
	protected:
		ControlType _controlType;
		CorrectorMethod _correctionMethod;
		TraversalCriterion _tranversalCriterion;
		SolverType _solverType;
		double _pathFollowingEqRatio;
	
	public:
		
		pathFollowingSystemBase():_controlType(ARC_LENGTH_CONTROL),
															_correctionMethod(CONSTRAINT_BASED),
															_tranversalCriterion(SMALLEST_DIRECTION_CHANGE),
															_solverType(TWO_RHS),
															_pathFollowingEqRatio(1.)
															{}
		virtual ~pathFollowingSystemBase(){};
		
		virtual void setControlType(const int i) {_controlType = (ControlType)i;};
    virtual ControlType getControlType() const {return _controlType;};
		
		virtual void setCorrectionMethod(const int i){_correctionMethod = (CorrectorMethod)i;};
		virtual CorrectorMethod getCorrectionMethod() const{return _correctionMethod;};
		
		virtual void setTranversalCriterion(const int i) {_tranversalCriterion = (TraversalCriterion)i;};
		virtual TraversalCriterion getTranversalCriterion() const {return _tranversalCriterion;};
		
		virtual void setSolverType( const int i, const double eqRatio){_solverType = (SolverType)i; _pathFollowingEqRatio = eqRatio;};
		virtual SolverType getSolverType() const {return _solverType;};
		virtual double getPathFollowingEqRatio() const {return _pathFollowingEqRatio;};
		
		virtual double getControlParameter() const = 0;
		virtual double getControlParameterStep() const = 0;
    /**arc-length increment or load increment depend on control type **/
    virtual void setPathFollowingIncrement(const double dt) = 0;
    /**state parameter to draw the equilibrium path **/
    virtual double getStateParameter() const = 0;
    virtual void resetControlParameter()  = 0;
    virtual void resetScaleParameter(const double val) =0;
    virtual int solvePerturbedSystem(){return 0.;};
		virtual double getPathFollowingConstraintRelativeRedidual() const = 0;
    
    virtual void addToHyperellipticMatrix(const int row, const int col, const double val) = 0;
};

template<class scalar>
class pathFollowingSystem : public pathFollowingSystemBase{
public:
		pathFollowingSystem():pathFollowingSystemBase(){};
		virtual ~pathFollowingSystem(){}
    
		virtual void addToPathFollowingContraint(const scalar& val) = 0;
		virtual void addToDPathFollowingConstraintDUnknown(const int col, const scalar& val)= 0;
		virtual void addToDPathFollowingConstraintDControlParameter(const scalar& val) = 0;
};
#endif // PATHFOLLOWINGSYSTEM_H_
