//
// C++ Interface: matrialLaw
//
// Description: Class with definition of materialLaw of fracture for shell (Has to be in a separated file to avoid SWIG warning)
//
//
// Author:  <Gauthier BECKER>, (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef SHELLFRACTURELAW_H_
#define SHELLFRACTURELAW_H_
#include "shellMaterialLaw.h"
class shellFractureByCohesiveLaw : public shellMaterialLaw, public  fractureBy2Laws<shellMaterialLaw,shellCohesiveLaw>
{
 public:
  shellFractureByCohesiveLaw(const int num,const int nbulk, const int ncoh);
#ifndef SWIG
  shellFractureByCohesiveLaw(const shellFractureByCohesiveLaw &source);
  ~shellFractureByCohesiveLaw(){}
  virtual matname getType() const{return materialLaw::fracture;}
  virtual void createIPState(IPStateBase* &ips,bool hasBodyForce, const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const IntPt *GP=NULL, const int gpt=0) const;
  virtual void createIPVariable(IPVariable* &ipv,bool hasBodyForce,  const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const;
  virtual void initialBroken(IPStateBase *ips) const;
  virtual void initLaws(const std::map<int,materialLaw*> &maplaw)
  {
    if(!_initialized){
      fractureBy2Laws<shellMaterialLaw,shellCohesiveLaw>::initLaws(maplaw);
      _initialized = true;
    }
  }
  virtual void stress(IPStateBase *ips, bool stiff, const bool checkfrac=true);
  virtual void reduction(IPLinearCohesiveShell *ipv,const IPLinearCohesiveShell *ipvprev,
                          const double deltan,const double deltat,
                          const double delta,reductionElement &nhat, reductionElement &mhat) const
  {
    _mfrac->reduction(ipv,ipvprev,deltan,deltat,delta,nhat,mhat);
  }
  // Virtual pure function of shellMaterialLaw used _mbulk
  virtual short int getNumberofSimpsonPoint() const{return _mbulk->getNumberofSimpsonPoint();}
  virtual void shellBendingStiffness(const IPVariableShell *ipv, LinearElasticShellHookeTensor &Hbend) const
  {_mbulk->shellBendingStiffness(ipv,Hbend);}
  virtual void shellMembraneStiffness(const IPVariableShell *ipv, LinearElasticShellHookeTensor &Htens) const
  {_mbulk->shellMembraneStiffness(ipv,Htens);}
  virtual void shellShearingStiffness(const IPVariableShell *ipv, linearElasticShellShearingHookeTensor &Hshear) const
  {_mbulk->shellShearingStiffness(ipv,Hshear);};
  // for non linear cases (delete the previous one)
  virtual void reduction(const IPStateBase *ips,std::vector<SVector3> &n_,
                         std::vector<SVector3> &m_,bool fdg=true) const
  {
    IPVariableShell *ipvcur = static_cast<IPVariableShell*>(ips->getState(IPStateBase::current));
    IPVariableShell *ipvprev = static_cast<IPVariableShell*>(ips->getState(IPStateBase::previous));
    IPShellFractureCohesive *ipvc = dynamic_cast<IPShellFractureCohesive*>(ipvcur);
    const IPShellFractureCohesive *ipvcp = static_cast<const IPShellFractureCohesive*>(ipvprev);

		bool isbroken = false;
		if (ipvc != NULL){
			isbroken = ipvc->isbroken();
		}

    if(isbroken){
      const IPShellFractureCohesive *ipvfprev = static_cast<const IPShellFractureCohesive*>(ipvprev);
      reductionElement nalphabeta, malphabeta;
      IPLinearCohesiveShell *ipvccur = static_cast<IPLinearCohesiveShell*>(ipvc->getIPvFrac());
      const IPLinearCohesiveShell *ipvcprev = static_cast<const IPLinearCohesiveShell*>(ipvcp->getIPvFrac());
      _mfrac->reduction(ipvccur,ipvcprev,nalphabeta,malphabeta);
      const shellLocalBasis* lbs = ipvcur->getshellLocalBasisOfInterface();
      for(int alpha=0;alpha<2;alpha++){
        n_[alpha] = nalphabeta(alpha,0)*lbs->basisVector(0)+nalphabeta(alpha,1)*lbs->basisVector(1);
        m_[alpha] = malphabeta(alpha,0)*lbs->basisVector(0)+malphabeta(alpha,1)*lbs->basisVector(1);
      }
      if(n_.size() == 3) // if n3 (non linear case only)
        n_[2] = SVector3(0.,0.,0.); // value of n3 in case of fracture ??
    }
    else
      _mbulk->reduction(ips,n_,m_,fdg);
  }
  virtual bool fullBroken(const IPVariable *ipv) const
  {
    const IPLinearCohesiveShell *ipvc = (static_cast<const IPShellFractureCohesive*>(ipv))->getIPvFrac();
    if(ipvc !=NULL)  return _mfrac->fullBroken(ipvc); // not initialized --> no fracture
    return false;
  }
  virtual double timeInitBroken(const IPVariable *ipv)const
  {
    const IPShellFractureCohesive*ipvc = static_cast<const IPShellFractureCohesive*>(ipv);
    if(ipvc !=NULL)  return ipvc->getInitializationTime(); // not initialized --> no fracture
    return 0.;
  }

  virtual double density() const{return _mbulk->density();}
  virtual double getThickness() const{return _mbulk->getThickness();}
  virtual double soundSpeed() const{return _mbulk->soundSpeed();}

	virtual materialLaw* clone() const {return new shellFractureByCohesiveLaw(*this);};
	virtual void checkInternalState(IPVariable* ipv, const IPVariable* ipvprev) const{
    getBulkLaw()->checkInternalState(ipv,ipvprev);
	};
  virtual bool withEnergyDissipation() const {
    return (getBulkLaw()->withEnergyDissipation() or getFractureLaw()->withEnergyDissipation());
  };
#endif // SWIG
};

#endif //SHELLFRACTURELAW_H_
