%feature("autodoc","1");
#ifdef _DEBUG
  %module dgshellpyDebug
#else
  %module dgshellpy
#endif
%include std_string.i
%include std_vector.i
%include std_map.i

%include nlmechsolpy.i

%{
  #include "shellMaterialLaw.h"
  #include "shellFractureLaw.h"
  #include "dgNonLinearShellMaterial.h"
  #include "dgShellDomain.h"
  #include "nonLinearInterDomainShell.h"
  #include "dgC0ShellRigidContactDomain.h"
%}
%nodefaultctor shellMaterialLaw;
%nodefaultctor shellCohesiveLaw;
%nodefaultctor dgNonLinearShellMaterialLaw;
%nodefaultctor fractureBy2Laws<shellMaterialLaw,shellCohesiveLaw>;

%include "shellMaterialLaw.h"
%template(fractureBy2LawsShellMaterialLawShellCohesiveLaw) fractureBy2Laws<shellMaterialLaw,shellCohesiveLaw>;
%include "shellFractureLaw.h"
%include "dgNonLinearShellMaterial.h"
%include "dgShellDomain.h"
%include "nonLinearInterDomainShell.h"
%include "dgC0ShellRigidContactDomain.h"
