//
// C++ Interface: terms
//
// Description: Class of terms for dg non linear shell
//
//
// Author:  <Gauthier BECKER>, (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef DGNONLINEARSHELLTERMS_H_
#define DGNONLINEARSHELLTERMS_H_
#include "dgC0ShellTerms.h"
#include "dgNonLinearShellMaterial.h"
#include "dgNonLinearShellIPVariable.h"
class dgNonLinearShellForceBulk : public DgC0LinearTerm<double>
{
 protected:
  const shellMaterialLaw* _mlaw;
  const IPField *_ipf;
  const bool _fullDg;
 private: // cache data
  mutable std::vector<SVector3> vn_;
  mutable std::vector<SVector3> vm_;
  mutable normalVariation nvar;
  mutable std::vector<fullMatrix<double> >dnvar1;
  mutable std::vector<fullMatrix<double> >dnvar2;
  mutable SVector3 dDeltaDt1Tmtilde1;
  mutable SVector3 dDeltaDt2Tmtilde2;
  mutable SVector3 dDeltatTn3;
 public:
  dgNonLinearShellForceBulk(FunctionSpace<double>& space1_,const materialLaw* mlaw,bool FullDG,
                                       const IPField *ip) : DgC0LinearTerm<double>(space1_),
                                                                                 _fullDg(FullDG),
                                                                                 _ipf(ip), vn_(3),
                                                                                 vm_(2), nvar(false),
                                                                                 _mlaw(static_cast<const shellMaterialLaw*>(mlaw))
  {
    fullMatrix<double> mm(3,3);
    for(int i=0;i<256;i++){
      dnvar1.push_back(mm);
      dnvar2.push_back(mm);
    }
  }
  virtual ~dgNonLinearShellForceBulk(){}
  virtual void get(MElement *ele,int npts,IntPt *GP,fullVector<double> &m) const;
  virtual const bool isData() const {return false;}
  virtual void set(const fullVector<double> *datafield){}
  virtual LinearTermBase<double>* clone () const
  {
    return new dgNonLinearShellForceBulk(space1,_mlaw,_fullDg,_ipf);
  }
};

class dgNonLinearShellStiffnessBulk : public BilinearTerm<double,double>
{
 protected:
  const shellMaterialLaw* _mlaw;
  const IPField *_ipf;
  const bool _fullDg;
  bool sym;
 public:
  dgNonLinearShellStiffnessBulk(FunctionSpace<double>& space1_,FunctionSpace<double>& space2_,materialLaw* mlaw,
                                  bool FullDG, IPField *ip) : BilinearTerm<double,double>(space1_,space2_),
                                                            _fullDg(FullDG), _ipf(ip),
                                                            _mlaw(static_cast<shellMaterialLaw*>(mlaw))

  {
    sym=(&space1_==&space2_);
  }
  dgNonLinearShellStiffnessBulk(FunctionSpace<double>& space1_, const materialLaw* mlaw,
                                  bool FullDG,const IPField *ip) : BilinearTerm<double,double>(space1_,space1_),
                                                             _fullDg(FullDG), _ipf(ip),
                                                             _mlaw(static_cast<const shellMaterialLaw*>(mlaw))
  {
    sym=true;
  }
  virtual ~dgNonLinearShellStiffnessBulk(){}
  virtual void get(MElement *ele,int npts,IntPt *GP,fullMatrix<double> &m) const{Msg::Error("Implement get for dgNonLinearShellStiffnessBulk");}
  virtual void get(MElement *ele, int npts, IntPt *GP, std::vector<fullMatrix<double> > &mv) const
  {
    Msg::Error("Define me get by gauss point IsotropicStiffBulkTermC0DgShell");
  }
  virtual BilinearTermBase* clone () const
  {
    return new dgNonLinearShellStiffnessBulk(space1,_mlaw,_fullDg,_ipf);
  }
};

class dgNonLinearShellForceInter : public DgC0LinearTerm<double>
{
 protected:
  // To get displacement
  const fullVector<double> *_data;
  nlsFunctionSpace<double> *_minusSpace;
  nlsFunctionSpace<double> *_plusSpace;
  const shellMaterialLaw *_mlawMinus;
  const shellMaterialLaw *_mlawPlus;
  interfaceQuadratureBase *_interQuad;
  IPField *_ipf;
  double _beta1,_beta2,_beta3;
  const bool _fullDg;
 private: // cache for get function
  mutable std::vector<TensorialTraits<double>::GradType> Gradshatm;
  mutable std::vector<TensorialTraits<double>::GradType> Gradshatp;
  mutable std::vector<SVector3> vnm_;
  mutable std::vector<SVector3> vnp_;
  mutable std::vector<SVector3> vmm_;
  mutable std::vector<SVector3> vmp_;
  mutable SVector3 mtildelamdamean;
  mutable SVector3 ntildemean;
  mutable SVector3 DtTmtildelamdamean;
  mutable normalVariation nvar;
  mutable std::vector<fullMatrix<double> >dnvar1m;
  mutable std::vector<fullMatrix<double> >dnvar2m;
  mutable std::vector<fullMatrix<double> >dnvar1p;
  mutable std::vector<fullMatrix<double> >dnvar2p;
  mutable std::vector<fullMatrix<double> >dnvar1hatm;
  mutable std::vector<fullMatrix<double> >dnvar2hatm;
  mutable std::vector<fullMatrix<double> >dnvar1hatp;
  mutable std::vector<fullMatrix<double> >dnvar2hatp;
  mutable SVector3 jDt;
  mutable LinearElasticShellHookeTensor Hm, Hp,Hmean, Hlambdam, Hlambdap;
  mutable linearElasticShellShearingHookeTensor Hs_m,Hs_p,Hsmean;
  mutable fullMatrix<double> comp11plus;
  mutable fullMatrix<double> comp12plus;
  mutable fullMatrix<double> comp11minus;
  mutable fullMatrix<double> comp12minus;
  mutable fullMatrix<double> comp21plus;
  mutable fullMatrix<double> comp22plus;
  mutable fullMatrix<double> comp21minus;
  mutable fullMatrix<double> comp22minus;
  mutable fullMatrix<double> compMembrane311plus;
  mutable fullMatrix<double> compMembrane312plus;
  mutable fullMatrix<double> compMembrane311minus;
  mutable fullMatrix<double> compMembrane312minus;
  mutable fullMatrix<double> compMembrane321plus;
  mutable fullMatrix<double> compMembrane322plus;
  mutable fullMatrix<double> compMembrane321minus;
  mutable fullMatrix<double> compMembrane322minus;
  mutable fullMatrix<double> tensprodtmp;
  mutable fullMatrix<double> comptmpj;
  mutable fullVector<double> compAssVector;
  mutable fullVector<double> jDtvector;
  mutable fullVector<double> jphi;
  mutable SVector3 jphiVect3;
  mutable reductionElement jlambdahtildemhat_minus, jlambdahtildemhat_plus;
  mutable fullVector<double> compMembrane41minus;
  mutable fullVector<double> compMembrane42minus;
  mutable fullVector<double> compMembrane41plus;
  mutable fullVector<double> compMembrane42plus;
 public:
  dgNonLinearShellForceInter(FunctionSpace<double>& space1_, FunctionSpace<double> *space2_,
                                       const materialLaw *mlawMinus, const materialLaw *mlawPlus,interfaceQuadratureBase *iquad,
                                       double beta1_, double beta2_, double beta3_,
                                       bool fdg, IPField *ip) : DgC0LinearTerm<double>(space1_),
                                                                _minusSpace(static_cast<nlsFunctionSpace<double>*>(&space1_)),
                                                                _plusSpace(static_cast<nlsFunctionSpace<double>*>(space2_)),
                                                                _beta1(beta1_), _beta2(beta2_),_beta3(beta3_),
                                                                _ipf(ip),
                                                                _mlawMinus(static_cast<const shellMaterialLaw*>(mlawMinus)),
                                                                _mlawPlus(static_cast<const shellMaterialLaw*>(mlawPlus)),
                                                                _interQuad(iquad),_fullDg(fdg),Gradshatm(256), Gradshatp(256),
                                                                vnm_(3), vnp_(3), vmm_(2), vmp_(2), nvar(true),
                                                                comp11minus(3,3), comp12minus(3,3), comp11plus(3,3),
                                                                comp12plus(3,3), comp21minus(3,3), comp22minus(3,3),
                                                                comp21plus(3,3), comp22plus(3,3),
                                                                compMembrane311minus(3,3), compMembrane312minus(3,3), compMembrane311plus(3,3),
                                                                compMembrane312plus(3,3), compMembrane321minus(3,3), compMembrane322minus(3,3),
                                                                compMembrane321plus(3,3), compMembrane322plus(3,3),
                                                                tensprodtmp(3,3), comptmpj(3,3), compAssVector(3),
                                                                jDtvector(3), jphi(3), jlambdahtildemhat_minus(), jlambdahtildemhat_plus(),
                                                                compMembrane41minus(3), compMembrane42minus(3),
                                                                compMembrane41plus(3), compMembrane42plus(3)
  {
    fullMatrix<double> mm(3,3);
    for(int i=0;i<256;i++){
      dnvar1m.push_back(mm);
      dnvar2m.push_back(mm);
      dnvar1p.push_back(mm);
      dnvar2p.push_back(mm);
      dnvar1hatm.push_back(mm);
      dnvar2hatm.push_back(mm);
      dnvar1hatp.push_back(mm);
      dnvar2hatp.push_back(mm);
    }
  }
  virtual ~dgNonLinearShellForceInter() {}
  virtual const bool isData() const{return true;}
  virtual void set(const fullVector<double> *datafield){_data = datafield;}
  virtual void get(MElement *ele, int npts, IntPt *GP, fullVector<double> &v)const;
  virtual LinearTermBase<double>* clone () const
  {
    return new dgNonLinearShellForceInter(*_minusSpace,_plusSpace,_mlawMinus,_mlawPlus,_interQuad,_beta1,_beta2,_beta3,_fullDg,_ipf);
  }
};

class dgNonLinearShellStiffnessInter : public BilinearTerm<double,double>
{
 protected:
  double _beta1,_beta2,_beta3;
  const shellMaterialLaw *_mlawMinus;
  const shellMaterialLaw *_mlawPlus;
  interfaceQuadratureBase *_interQuad;
  unknownField *_ufield; // used for fracture. In that case the matrix is computed by perturbation
  IPField *_ipf;
  const bool _fullDg;
  const double _perturbation;
 public:
  dgNonLinearShellStiffnessInter(FunctionSpace<double>& space1_,FunctionSpace<double>& space2_,
                                    const materialLaw* mlawMinus, const materialLaw* mlawPlus,interfaceQuadratureBase *iquad,
                                    double beta1_,double beta2_, double beta3_, IPField *ip, unknownField *uf,
                                    bool fulldg,double eps=1.e-6) : BilinearTerm<double,double>(space1_,space2_),
                                                                     _beta1(beta1_),_beta2(beta2_),_beta3(beta3_),
                                                                     _ipf(ip), _ufield(uf),
                                                                     _mlawMinus(static_cast<const shellMaterialLaw*>(mlawMinus)),
                                                                     _mlawPlus(static_cast<const shellMaterialLaw*>(mlawPlus)),
                                                                     _interQuad(iquad),
                                                                     _fullDg(fulldg), _perturbation(eps){}
  virtual ~dgNonLinearShellStiffnessInter() {}
  virtual void get(MElement *ele,int npts,IntPt *GP, fullMatrix<double> &m)const{Msg::Error("Implement dgNonLinearShellStiffnessInter get function");}
  virtual void get(MElement *ele, int npts, IntPt *GP, std::vector<fullMatrix<double> > &mv) const
  {
    Msg::Error("Define me ?? get by integration point IsotropicStiffIntertermC0DgShell");
  }
  virtual BilinearTermBase* clone () const
  {
    return new dgNonLinearShellStiffnessInter(space1,space2,_mlawMinus,_mlawPlus,_interQuad,_beta1,
                                              _beta2,_beta3,_ipf,_ufield,_fullDg,_perturbation);
  }
};

class dgNonLinearShellForceVirtInter : public DgC0LinearTerm<double>
{
 protected:
  const double _beta1;
  const bool _fullDg;
  shellMaterialLaw *_mlaw;
  interfaceQuadratureBase *_interQuad;
  IPField *_ipf;
  const fullVector<double> *_data;
  // cache get function
 private:
  mutable std::vector<TensorialTraits<double>::GradType> Gradshatm;
  mutable std::vector<SVector3> vnm_;
  mutable std::vector<SVector3> vmm_;
  mutable SVector3 mtildelamdamean;
  mutable SVector3 DtTmtildelamdamean;
  mutable normalVariation nvar;
  mutable std::vector<fullMatrix<double> >dnvar1m;
  mutable std::vector<fullMatrix<double> >dnvar2m;
  mutable std::vector<fullMatrix<double> >dnvar1hatm;
  mutable std::vector<fullMatrix<double> >dnvar2hatm;
  mutable SVector3 jDt;
  mutable LinearElasticShellHookeTensor Hm, Hp,Hmean;
  mutable fullMatrix<double> comp11minus;
  mutable fullMatrix<double> comp12minus;
  mutable fullMatrix<double> comp21minus;
  mutable fullMatrix<double> comp22minus;
  mutable fullMatrix<double> tensprodtmp;
  mutable fullMatrix<double> comptmpj;
  mutable fullVector<double> compAssVector;
  mutable fullVector<double> jDtvector;
 public:
  dgNonLinearShellForceVirtInter(FunctionSpace<double>& space1_,materialLaw *mlaw, interfaceQuadratureBase *iquad,
                                 double beta1_,bool FullDg_,IPField  *ip) : DgC0LinearTerm<double>(space1_),
                                                                            _beta1(beta1_), _fullDg(FullDg_),
                                                                            _ipf(ip),_mlaw(static_cast<shellMaterialLaw*>(mlaw)),
                                                                            _interQuad(iquad),_data(NULL), vnm_(3), vmm_(2), Gradshatm(256),
                                                                            comp11minus(3,3), comp12minus(3,3), comp21minus(3,3),
                                                                            comp22minus(3,3), tensprodtmp(3,3), comptmpj(3,3),
                                                                            compAssVector(3),jDtvector(3)
  {
    fullMatrix<double> mm(3,3);
    for(int i=0;i<256;i++){
      dnvar1m.push_back(mm);
      dnvar2m.push_back(mm);
      dnvar1hatm.push_back(mm);
      dnvar2hatm.push_back(mm);
    }
  }
  virtual ~dgNonLinearShellForceVirtInter() {}
  virtual void set(const fullVector<double> *datafield){_data = datafield;}
  virtual void get(MElement *iele,int npts,IntPt *GP, fullVector<double> &v) const;
  virtual const bool isData() const{return false;}
  virtual LinearTermBase<double>* clone () const
  {
    return new dgNonLinearShellForceVirtInter(space1,_mlaw,_interQuad,_beta1,_fullDg,_ipf);
  }
};

class dgNonLinearShellStiffnessVirtInter : public BilinearTerm<double,double>
{
 protected:
  shellMaterialLaw *_mlaw;
  interfaceQuadratureBase *_interQuad;
  const double _beta1;
  unknownField *_ufield;
  IPField *_ipf;
  const bool _fullDg;
  bool sym;
 public:
  dgNonLinearShellStiffnessVirtInter(FunctionSpace<double>& space1_,FunctionSpace<double>& space2_,
                                     materialLaw *mlaw,interfaceQuadratureBase *iquad,double beta1_,
                                     unknownField *uf, IPField *ipf_,
                                     bool FullDg_) : BilinearTerm<double,double>(space1_,space2_),
                                                     _beta1(beta1_),_fullDg(FullDg_),
                                                     _ipf(ipf_),_interQuad(iquad),
                                                     _mlaw(static_cast<shellMaterialLaw*>(mlaw)),
                                                     sym(false)

  {
    sym = (&space1_ == &space2_) ;
  }
  dgNonLinearShellStiffnessVirtInter(FunctionSpace<double>& space1_,materialLaw* mlaw,
                                     interfaceQuadratureBase *iquad,double beta1_,
                                     unknownField *uf, IPField *ipf_,
                                     bool FullDg_) : BilinearTerm<double,double>(space1_,space1_),
                                                     _beta1(beta1_),_fullDg(FullDg_) , _ufield(uf), _interQuad(iquad),
                                                     _ipf(ipf_), _mlaw(static_cast<dgNonLinearShellMaterialLaw*>(mlaw)), sym(true){}
  virtual ~dgNonLinearShellStiffnessVirtInter() {}
  virtual void get(MElement *iele,int npts,IntPt *GP, fullMatrix<double> &m) const{Msg::Error("Implement dgNonLinearShellStiffnessVirtInter get function");}
  virtual void get(MElement *ele, int npts, IntPt *GP, std::vector<fullMatrix<double> > &mv) const
  {
    Msg::Error("Define me get by integration point IsotropicStiffVirtualInterfaceTermC0DgShell");
  }
  virtual BilinearTermBase* clone () const
  {
    return new dgNonLinearShellStiffnessVirtInter(space1,_mlaw,_interQuad,_beta1,_ufield,_ipf,_fullDg);
  }

};

#endif // DGNONLINEARSHELLTERMS_H_
