//
// C++ Interface: partDomain
//
// Description: Interface class for dg shell
//
// Author:  <Gauthier BECKER>, (C) 2010
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef DGSHELLDOMAIN_H_
#define DGSHELLDOMAIN_H_
#ifndef SWIG
#include "dgC0ShellFunctionSpace.h"
#include "dgC0ShellNormalVariation.h"
#include "shellMaterialLaw.h"
#include "dgNonLinearShellMaterial.h"
#include "dgNonLinearShellIPVariable.h"
#include "partDomain.h"
#include "interfaceQuadrature.h"
#endif
class dgLinearShellDomain : public dgPartDomain{
 public :
  enum GaussType { Gauss, Lobatto};
 protected:
  materialLaw *_mlaw;
  const int _lnum;
  double _beta1,_beta2,_beta3;
  double _sts; // variable to scale the explicit time step by 1/sqrt(beta) for dg stabilization
  FunctionSpaceBase *_space;
  FunctionSpaceBase *_spaceMinus;
  FunctionSpaceBase *_spacePlus;
  int _gaussorderbulk, _gaussorderbound; // To allow user to change the number of Gauss Point default =-1 (not used) if != -1 user's value
  GaussType _gqt; // classic or Lobatto
  interfaceQuadratureBase *_interQuad; // To compute the GaussPoint on minus and plus elements from the interface value; Put in dgPartDomain ??
  bool _sameValueInterfaceGaussPoints; // To prescribed the same values on an interface with a random distribution
 protected:
  normalVariation _nvarDomain; // used to compute ipvarible and avoid consume time multiple allocation of this class
  virtual void ensureSameValuesBothSideIPvariable(IPField *ip);
 public:
  virtual void stabilityParameters(const double b1=10., const double b2=10., const double b3=10.);
  virtual void matrixByPerturbation(const int ibulk, const int iinter, const int ivirt,const double eps=1e-8);
  virtual void sameStatisticValuesOnInterfaceGaussPoints();
  dgLinearShellDomain(const int tag, const int phys, const int sp_, const int lnum, const int fdg);
  dgLinearShellDomain(const dgLinearShellDomain &source);
  virtual ~dgLinearShellDomain(){ if(_interQuad) delete _interQuad;} //If I delete mat here plante why ??
  virtual void gaussIntegration(const GaussType gqt,const int orderbulk, const int orderbound);
#ifndef SWIG
  virtual int getDim() const {return 2;}
  virtual void setDeformationGradientGradient(AllIPState *aips, const STensor33 &GM,const IPStateBase::whichState ws){};    
  virtual void setdFmdFM(AllIPState *aips, std::map<Dof, STensor3> &dUdF,const IPStateBase::whichState ws){};
  virtual void setdFmdGM(AllIPState *aips, std::map<Dof, STensor33> &dUdG,const IPStateBase::whichState ws){};

  virtual void setValuesForBodyForce(AllIPState *aips, const IPStateBase::whichState ws){};
  virtual bool computeIpv(AllIPState *aips,MElement *e, IPStateBase::whichState ws,
                                  materialLaw *mlaw,fullVector<double> &disp, bool stiff);
  virtual bool computeIpv(AllIPState *aips,MInterfaceElement *ie, IntPt *GP,const IPStateBase::whichState ws,
                                  partDomain* efMinus, partDomain *efPlus,materialLaw *mlawminus,
                                  materialLaw *mlawplus,fullVector<double> &dispm,
                                  fullVector<double> &dispp,
                                  const bool virt, bool stiff, const bool checkfrac=true);
  virtual bool computeIPVariable(AllIPState *aips,const unknownField *ufield,const IPStateBase::whichState ws, bool stiff);
  virtual void checkFailure(IPField* ipf) const{
    // Msg::Warning("This function checkFailure is not defined ");
  }
  virtual void checkInternalState(IPField* ipf) const{
    // Msg::Warning("This function checkInternalState is not defined ");
  }
  virtual FunctionSpaceBase* getSpaceForBC(const nonLinearBoundaryCondition::type bc_type, 
                                           const nonLinearBoundaryCondition::location bc_location,
                                           const nonLinearNeumannBC::NeumannBCType neumann_type,
                                           const int dof_comp,
                                           const mixedFunctionSpaceBase::DofType dofType,
                                           const elementGroup *groupBC) const;
  virtual QuadratureBase* getQuadratureRulesForNeumannBC(nonLinearNeumannBC &neu) const;
  virtual void setMaterialLaw(const std::map<int,materialLaw*>& maplaw);
  virtual materialLaw* getMaterialLaw(){return _mlaw;}
  virtual const materialLaw* getMaterialLaw() const{return _mlaw;}
  virtual materialLaw* getMaterialLawMinus(){return _mlaw;}
  virtual const materialLaw* getMaterialLawMinus() const{return _mlaw;}
  virtual materialLaw* getMaterialLawPlus(){return _mlaw;}
  virtual const materialLaw* getMaterialLawPlus() const{return _mlaw;}
  virtual int getLawNum() const{return _lnum;}
  virtual int getMinusLawNum() const{return _lnum;}
  virtual int getPlusLawNum() const{return _lnum;}
  virtual void setGaussIntegrationRule();
  virtual void initializeTerms(unknownField *uf,IPField *ip);
  virtual LinearTermBase<double>* createNeumannTerm(FunctionSpace<double> *spneu, const elementGroup* g,
                                                    const simpleFunctionTime<double>* f, const unknownField *uf,
                                                    const IPField * ip, const nonLinearBoundaryCondition::location onWhat,
                                                    const nonLinearNeumannBC::NeumannBCType neumann_type,
                                                    const int comp) const;
  virtual double scaleTimeStep() const {return _sts;}
  virtual FunctionSpaceBase* getFunctionSpace(){return _space;}
  virtual const FunctionSpaceBase* getFunctionSpace() const{return _space;}
  virtual const partDomain* getMinusDomain() const{return this;}
  virtual const partDomain* getPlusDomain() const{return this;}
  virtual partDomain* getMinusDomain() {return this;}
  virtual partDomain* getPlusDomain() {return this;}
  virtual void createInterface(manageInterface &maninter);
  virtual MElement* createVirtualInterface(IElement *ie) const;
  virtual MElement* createInterface(IElement *ie1, IElement *ie2) const;
  virtual const double stabilityParameter(const int i) const;
  virtual void createInterfaceMPI(const int rankOtherPart, const elementGroup &groupOtherPart,dgPartDomain* &dgdom);
  bool sameValueGaussPoint(){return _sameValueInterfaceGaussPoints;}
  // Add for BC
  virtual FilterDof* createFilterDof(const int comp) const;
	virtual interfaceQuadratureBase* getInterfaceQuadrature() const{return _interQuad;};
#endif
};

// Only few function are not the same so non linear can be derived
// from linear. Maybe create a virtual base class dgShellDomain
class dgNonLinearShellDomain : public dgLinearShellDomain{
 protected:
  bool _evalStiff; // = true when compute matrix by perturbation
  virtual void initialIPVariable(AllIPState *aips,const unknownField *ufield,const IPStateBase::whichState ws);
 public:
  dgNonLinearShellDomain(const int tag, const int phys, const int sp_, const int lnum, const int fdg);
  dgNonLinearShellDomain(const dgNonLinearShellDomain &source);
  ~dgNonLinearShellDomain(){}
#ifndef SWIG
  virtual bool computeIPVariable(AllIPState *aips,const unknownField *ufield,const IPStateBase::whichState ws, bool stiff);
  virtual void setDeformationGradientGradient(AllIPState *aips, const STensor33 &GM,const IPStateBase::whichState ws){};      
  virtual void setdFmdFM(AllIPState *aips, std::map<Dof, STensor3> &dUdF,const IPStateBase::whichState ws){};    
  virtual void setdFmdGM(AllIPState *aips, std::map<Dof, STensor33> &dUdG,const IPStateBase::whichState ws){};

  virtual bool computeIpv(AllIPState *aips,MElement *e, IPStateBase::whichState ws,
                                  materialLaw *mlaw,fullVector<double> &disp, bool stiff);
  virtual bool computeIpv(AllIPState *aips,MInterfaceElement *ie, IntPt *GP,const IPStateBase::whichState ws,
                                  partDomain* efMinus, partDomain *efPlus,materialLaw *mlawminus,
                                  materialLaw *mlawplus,fullVector<double> &dispm,
                                  fullVector<double> &dispp,
                                  const bool virt, bool stiff,const bool checkfrac=true);
  virtual void initializeTerms(unknownField *uf,IPField *ip);
  virtual void setGaussIntegrationRule();
  virtual void createInterfaceMPI(const int rankOtherPart, const elementGroup &groupOtherPart,dgPartDomain* &dgdom);
#endif
};

class interDomainBetweenShell : public dgLinearShellDomain{
 protected :
  BiNonLinearTermBase *bterm;
  nonLinearTermBase<double> *lterm;
  partDomain *_domMinus;
  partDomain *_domPlus;
  materialLaw* _mlawMinus;
  materialLaw* _mlawPlus;
 public:
  interDomainBetweenShell(const int tag, partDomain *dom1, partDomain *dom2, const int lnum=0);
  interDomainBetweenShell(const interDomainBetweenShell &source);
  ~interDomainBetweenShell(){}
  virtual void stabilityParameters(const double b1=10., const double b2=10., const double b3=10.);
  virtual void matrixByPerturbation(const int iinter, const double eps=1e-8);
#ifndef SWIG
//  void initializeTerms(FunctionSpaceBase* space1_,FunctionSpaceBase* space2_,unknownField *uf,IPField*ip);
  virtual void setGaussIntegrationRule();
  virtual bool computeIPVariable(AllIPState *aips,const unknownField *ufield,const IPStateBase::whichState ws, bool stiff);
  virtual void setMaterialLaw(const std::map<int,materialLaw*>& maplaw);
  virtual materialLaw* getMaterialLaw(){return _mlaw;}
  virtual const materialLaw* getMaterialLaw() const{return _mlaw;}
  virtual materialLaw* getMaterialLawMinus(){return _mlawMinus;}
  virtual const materialLaw* getMaterialLawMinus() const{return _mlawMinus;}
  virtual materialLaw* getMaterialLawPlus(){return _mlawPlus;}
  virtual const materialLaw* getMaterialLawPlus() const{return _mlawPlus;}
  virtual int getLawNum() const{return _lnum;}
  virtual int getMinusLawNum() const{return _domMinus->getLawNum();}
  virtual int getPlusLawNum() const{return _domPlus->getLawNum();}
  virtual const partDomain* getMinusDomain() const{return _domMinus;}
  virtual const partDomain* getPlusDomain() const{return _domPlus;}
  virtual partDomain* getMinusDomain(){return _domMinus;}
  virtual partDomain* getPlusDomain() {return _domPlus;}
  void initializeTerms(unknownField *uf,IPField *ip);
  // as there is no elerment on a interDomain empty function. The interface element on an interDomain
  // has to be created via its two domains.
  virtual void createInterface(manageInterface &maninter){}
#endif
};

#endif // DGSHELLDOMAIN_H_
