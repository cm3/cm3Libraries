//
// C++ Interface:
//
// Description: Allow to manage the computation of normal variation in DG shell
//
//
// Author:  <Gauthier BECKER>, (C) 2010
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "dgC0ShellNormalVariation.h"
