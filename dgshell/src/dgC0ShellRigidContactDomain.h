//
// C++ Interface: terms
//
// Description: Domain of contact for rigid contact for shell. Has to be here to create the good functional space
//
//
// Author:  <Gauthier BECKER>, (C) 2010
//
// Copyright: See COPYING file that comes with this distribution
//
#ifndef _DGC0SHELLRIGIDCONTACTDOMAIN_H_
#define _DGC0SHELLRIGIDCONTACTDOMAIN_H_
#include "contactDomain.h"
class dgC0ShellRigidCylinderContact : public rigidCylinderContactDomain{
 public:
  dgC0ShellRigidCylinderContact() : rigidCylinderContactDomain(){}
  dgC0ShellRigidCylinderContact(const int tag, const int physMaster, const int physSlave, const int physpt1,
                       const int physpt2,const double penalty,const double h,const double rho,elementFilter *fslave=NULL) :
                           rigidCylinderContactDomain(tag,physMaster,physSlave,physpt1,physpt2,penalty,h,rho,fslave){}
  dgC0ShellRigidCylinderContact(const dgC0ShellRigidCylinderContact &source) : rigidCylinderContactDomain(source){}
  ~dgC0ShellRigidCylinderContact(){}
#ifndef SWIG
  virtual void setDomainAndFunctionSpace(partDomain *dom);
#endif // SWIG
};

// physptBot is the center of cylindrical part at its end opposite to the cone
class dgC0ShellRigidConeContact : public rigidConeContactDomain{
 public:
  dgC0ShellRigidConeContact(const int tag, const int physMaster, const int physSlave,const int physptBase,
                            const int physptTop, const int physptBot,const double penalty,const double bradius,const double h,
                            const double rho,elementFilter *fslave=NULL) : rigidConeContactDomain(tag,physMaster,physSlave,physptBase,
                                                                       physptTop,physptBot,penalty,bradius,h,rho,fslave){}
 #ifndef SWIG
  dgC0ShellRigidConeContact(const dgC0ShellRigidConeContact &source) : rigidConeContactDomain(source){}
  ~dgC0ShellRigidConeContact(){}
  virtual void setDomainAndFunctionSpace(partDomain *dom);
 #endif // SWIG
};

#endif //_DGC0SHELLRIGIDCONTACTDOMAIN_H_
