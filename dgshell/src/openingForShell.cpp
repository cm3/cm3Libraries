
//
// C++ Interface: opening for shell
//
// Description: compute opening for shell formulation
//
//
// Author:  <Gauthier BECKER>, (C) 2010
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "SVector3.h"
#include "shellIPVariable.h"
#include "openingForShell.h"
void openingForShell(const SVector3 &ujump, const double rjump[3], const IPShellFractureCohesive *ipv,
                      double &deltan, double &deltat, double &delta)
{
  const shellLocalBasis *lbs = ipv->getshellLocalBasisOfInterface();
  const double h = ipv->getThickness();
  const IPLinearCohesiveShell *ipvc = static_cast<const IPLinearCohesiveShell*>(ipv->getIPvFrac());
  openingForShell(ujump,rjump,ipvc,lbs,h,deltan,deltat,delta);
}

void openingForShell(const SVector3 &ujump_, const double rjump_[3], const IPLinearCohesiveShell *ipv,
                     const shellLocalBasis *lbs,const double h,double &deltan, double &deltat, double &delta)
{
  const double etaI = ipv->getEtaI();
  const double etaII= ipv->getEtaII();
  const double beta = ipv->getBeta();
  const double unjump0= ipv->getunj0();
  const double utjump0= ipv->getutj0();
  const double rnjump0= ipv->getrnj0();
  const double rtjump0= ipv->getrtj0();
  const double heqI = ipv->getThickeqI();
  const double heqII= ipv->getThickeqII();
  const reductionElement& m0 = ipv->getm0();
  double unor = - scaldot(ujump_,lbs->basisVector(1))/lbs->basisVector(1).norm(); // minus because normal of shellLocalBasis of interface = - phi0,1
  double ut = scaldot(ujump_,lbs->basisVector(0))/lbs->basisVector(0).norm();
  double rnor =  - scaldot(rjump_,lbs->basisVector(1))/lbs->basisVector(1).norm();
  double rt = scaldot(rjump_,lbs->basisVector(0))/lbs->basisVector(0).norm();
  // I have to change the convention rnor:=-rnor and rt :=-rt ??
  if(m0(1,1)>0) deltan = (1-etaI)*(unor-unjump0) + etaI*heqI*(rnor-rnjump0);
  else deltan = (1-etaI)*(unor-unjump0) + etaI*heqI*(-rnor-rnjump0);
  if(m0(0,1)>0) deltat = (1-etaII)*(ut-utjump0) + etaII*heqII*(rt-rtjump0);
  else deltat = (1-etaII)*(ut-utjump0) + etaII*heqII*(-rt-rtjump0);
  if(deltan > 0.)
    delta = sqrt(deltan*deltan+beta*beta*deltat*deltat);
  else
    delta = beta*deltat;
/*
  const double etaI = ipv->getEtaI();
  const double etaII= ipv->getEtaII();
  const double alpha= 6.;
  const double beta = ipv->getBeta();
  const double unjump0= ipv->getunj0();
  const double utjump0= ipv->getutj0();
  const double rnjump0= ipv->getrnj0();
  const double rtjump0= ipv->getrtj0();
  const reductionElement& m0 = ipv->getm0();
  double unor = - scaldot(ujump_,lbs->basisVector(1))/lbs->basisVector(1).norm(); // minus because normal of shellLocalBasis of interface = - phi0,1
  double ut = scaldot(ujump_,lbs->dualBasisVector(0))/lbs->basisVector(0).norm();
  double rnor =  - scaldot(rjump_,lbs->basisVector(1))/lbs->basisVector(1).norm();
  double rt = scaldot(rjump_,lbs->dualBasisVector(0))/lbs->basisVector(0).norm();
  // I have to change the convention rnor:=-rnor and rt :=-rt ??
  if(m0(1,1)>0) deltan = (1-etaI)*(unor-unjump0) + etaI*h/6.*(rnor-rnjump0);
  else deltan = (1-etaI)*(unor-unjump0) + etaI*h/6.*(-rnor-rnjump0);
  if(m0(0,1)>0) deltat = (1-etaII)*(ut-utjump0) + etaII*h/alpha*(rt-rtjump0);
  else deltat = (1-etaII)*(ut-utjump0) + etaII*h/alpha*(-rt-rtjump0);
  if(deltan > 0.)
    delta = sqrt(deltan*deltan+beta*beta*deltat*deltat);
  else
    delta = beta*deltat;
*/
}
