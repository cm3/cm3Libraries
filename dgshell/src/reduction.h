//
// C++ Interface: terms
//
// Description: Function needed to compute element of reduction n^alpha and m^alpha for thin bodies
//
//
// Author:  <Gauthier BECKER>, (C) 2010
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef REDUCTION_H_
#define REDUCTION_H_
#include "LinearElasticShellHookeTensor.h"
#include "shellLocalBasis.h"
#include "SVector3.h"
#include "dgC0ShellManageBvector.h"
#include "restartManager.h"
// class for store reduction element m(alpha,beta) and n(alpha,beta)
class stressTensor;
class strainTensor;
class reductionElement{
 protected :
   double mat[2][2];
 public :

  // constructors and destructor
  reductionElement(){for(int i=0;i<2;i++) for(int j=0;j<2;j++) mat[i][j]=0.;}
  ~reductionElement(){}
  reductionElement(const reductionElement &source){
    for(int i=0;i<2;i++)
      for(int j=0;j<2;j++)
        mat[i][j] = source.mat[i][j];
  }
  reductionElement(const stressTensor &stress, const shellLocalBasis *lb);
  reductionElement(const strainTensor &strain, const shellLocalBasis *lb);
  reductionElement & operator = (const reductionElement &source){
    for(int i=0;i<2;i++)
      for(int j=0;j<2;j++)
        mat[i][j] = source.mat[i][j];
    return *this;
  }
  void init(const stressTensor &stress, const shellLocalBasis *lb);
  // hat
  void hat(const shellLocalBasisBulk *lb, reductionElement &nhat) const;
  // access to data
  double& operator() (const int i, const int j){return mat[i][j];}
  double operator() (const int i, const int j) const {return mat[i][j];}
  void operator *= (const double dd)
  {
    if(dd != 0){
      for(int i=0;i<2;i++)
        for(int j=0;j<2;j++)
          mat[i][j]*=dd;
    }
    else
    {
      for(int i=0;i<2;i++)
        for(int j=0;j<2;j++)
          mat[i][j]=0.;
    }
  }
  void operator +=(const reductionElement &re){for(int i=0;i<2;i++) for(int j=0;j<2;j++) mat[i][j]+=re(i,j);}
  void setAll(const double d){for(int i=0;i<2;i++) for(int j=0;j<2;j++) mat[i][j]=d;}
  void print() const
  {
    printf("%e %e\n",mat[0][0],mat[0][1]);
    printf("%e %e\n",mat[1][0],mat[1][1]);
  }
  void restart()
  {
    for(int i=0;i<2;i++)
      for(int j=0;j<2;j++)
        restartManager::restart(mat[i][j]);
  }
};

// class for stressTensor
class stressTensor;
static void matvectprod2(const stressTensor &st, const SVector3 &v1, SVector3 &v2);
class stressTensor : public STensor3{
 public :
  stressTensor(const double v =0.0) : STensor3(v){}
  stressTensor(const reductionElement &red, const shellLocalBasis *lbs, const double v =0.0) : STensor3(v){
    STensor3 temp;
    for(int alpha = 0; alpha<2;alpha++)
      for(int beta=0; beta<2;beta++){
        tensprod(lbs->basisVector(alpha),lbs->basisVector(beta),temp);
        temp *= red(alpha,beta);
        *this += temp;
      }
  }
  void init(const reductionElement &red, const shellLocalBasis *lbs){
    static STensor3 temp;
    for(int i=0;i<9;i++) this->_val[i] = 0.;
    for(int alpha = 0; alpha<2;alpha++)
      for(int beta=0; beta<2;beta++){
        tensprod(lbs->basisVector(alpha),lbs->basisVector(beta),temp);
        temp *= red(alpha,beta);
        for(int i=0;i<3;i++)
          for(int j=0;j<3;j++) _val[getIndex(i,j)] += temp(i,j);
      }
  }

  ~stressTensor(){}
  stressTensor(const stressTensor & source) : STensor3(source){}
  stressTensor &operator= (const stressTensor &source){
    this->STensor3::operator=(source);
    return *this;
  }
  double getComponent(const SVector3 &n1, const SVector3 &n2) const{
    SVector3 temp(0., 0., 0.);
    matvectprod2(*this,n2,temp);
    return dot(temp,n1);
  }
};

// strain tensor
class strainTensor;
static void matvectprod(const strainTensor &st, const SVector3 &v1, SVector3 &v2);
class strainTensor : public STensor3{
 public :
  strainTensor(const double v =0.0) : STensor3(v){}
  strainTensor(const reductionElement &red, const shellLocalBasis *lbs, const double v =0.0) : STensor3(v){
    STensor3 temp;
    for(int alpha = 0; alpha<2;alpha++)
      for(int beta=0; beta<2;beta++){
        tensprod(lbs->dualBasisVector(alpha),lbs->dualBasisVector(beta),temp);
        temp *= red(alpha,beta);
        *this += temp;
      }
  }
  void init(const reductionElement &red, const shellLocalBasis *lbs){
    static STensor3 temp;
    for(int i=0;i<9;i++) this->_val[i] = 0.;
    for(int alpha = 0; alpha<2;alpha++)
      for(int beta=0; beta<2;beta++){
        tensprod(lbs->dualBasisVector(alpha),lbs->dualBasisVector(beta),temp);
        temp *= red(alpha,beta);
        for(int i=0;i<3;i++)
          for(int j=0;j<3;j++) _val[getIndex(i,j)] += temp(i,j);
      }
  }

  ~strainTensor(){}
  strainTensor(const strainTensor & source) : STensor3(source){}
  strainTensor &operator= (const strainTensor &source){
    this->STensor3::operator=(source);
    return *this;
  }
  double getComponent(const SVector3 &n1, const SVector3 &n2) const{
    SVector3 temp(0., 0., 0.);
    matvectprod(*this,n2,temp);
    return dot(temp,n1);
  }
};

static void matvectprod2(const stressTensor &st, const SVector3 &v1, SVector3 &v2){
  SVector3 temp;
  for(int i=0;i<3;i++){
    temp(0) = st(i,0); temp(1) = st(i,1); temp(2) = st(i,2);
    v2(i) = dot(temp,v1);
  }
}

static inline void diff(const SVector3 &a,const SVector3 &b,SVector3 &c);

// Should be somewhere else ??
void displacementjump(const std::vector<TensorialTraits<double>::ValType> &Val_m,const int n_m,
                      const std::vector<TensorialTraits<double>::ValType> &Val_p,const int n_p,
                      const fullVector<double> & disp,fullVector<double> &ujump);

void displacementjump(const std::vector<TensorialTraits<double>::ValType> &Val_m,const int n_m,
                      const std::vector<TensorialTraits<double>::ValType> &Val_p,
                      const int n_p,const fullVector<double> & dispm,const fullVector<double> & dispp,SVector3 &ujump);
 #endif // reduction.h
