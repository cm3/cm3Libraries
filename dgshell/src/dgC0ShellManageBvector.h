//
// C++ Interface: terms
//
// Description: Manage what is call B vector in gmsh
//
//
// Author:  <Gauthier BECKER>, (C) 2010
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef DGC0SHELLMANAGEBVECTOR
#define DGC0SHELLMANAGEBVECTOR

#include "fullMatrix.h"
#include "SVector3.h"
#include "shellLocalBasis.h"
#include "restartManager.h"
static inline double scaldot(const SVector3 &a,const SVector3 &b){
  double c=0.;
  for(int i=0;i<3;i++)
    c+= a[i]*b[i];
  return c;
}

class Bvector{
 protected:
  fullMatrix<double> _Bm;
  fullMatrix<double> _Bn;
  int _nbFF;
  // Data used by function (Allocated once)
 private:
  mutable double pft1[16];
  mutable double pft2[16];
  mutable double vt1[8];
  mutable double vt2[8];
  mutable double vt3[8];
  mutable double *_vgrad;
  mutable double *_vhess;
  mutable fullMatrix<double> mbres;
  mutable fullVector<double> vres;
 public:
  Bvector(const int nbFF=256) : _nbFF(nbFF), _Bm(4,3*nbFF), _Bn(4,3*nbFF),
                                 mbres(4,nbFF),
                                 vres(4), _vgrad(NULL), _vhess(NULL)
  {
    _vgrad = new double[2*nbFF];
    _vhess = new double[4*nbFF];
  }

  Bvector& operator=(const Bvector &source)
  {
    _Bm = source._Bm;
    _Bn = source._Bn;
    _nbFF = source._nbFF;
    if(_vgrad==NULL) _vgrad = new double[2*_nbFF];
    if(_vhess==NULL) _vhess = new double[4*_nbFF];
    mbres.resize(4,_nbFF,false);
    vres.resize(4);
		return *this;
  }
  Bvector(const Bvector &source) : _Bm(source._Bm), _Bn(source._Bn), _nbFF(source._nbFF)
  {
    // otherdata is just used internal
    if(_vgrad==NULL) _vgrad = new double[2*_nbFF];
    if(_vhess==NULL) _vhess = new double[4*_nbFF];
    mbres.resize(4,_nbFF,false);
    vres.resize(4);
  }
  virtual ~Bvector()
  {
    if(_vgrad!=NULL){ delete[] _vgrad; _vgrad=NULL;}
    if(_vhess!=NULL){ delete[] _vhess; _vhess=NULL;}
  }

  void resize(const int nbFF){
    if(_nbFF!=nbFF)
    {
      _Bm.resize(4,3*nbFF,false);
      _Bn.resize(4,3*nbFF,false);
      if(nbFF>_nbFF)
      {
        if(_vgrad!=NULL){ delete[] _vgrad; _vgrad=new double[2*nbFF];}
        if(_vhess!=NULL){ delete[] _vhess; _vhess=new double[4*nbFF];}
      }
      mbres.resize(4,nbFF,false); //set after
      _nbFF = nbFF;
    }
  }
  void bending(const linearShellLocalBasisBulk *lb,const  std::vector<TensorialTraits<double>::GradType> &Grads,
               const std::vector<TensorialTraits<double>::HessType> &Hess)
  {
    const double _invJ = 1./lb->getJacobian();
    const SVector3 &t0 = lb->basisNormal();
    SVector3 t1,t2;
    crossprod(lb->basisVector(0),lb->basisNormal(),t1);
    crossprod(lb->basisVector(1),lb->basisNormal(),t2);

    // put gradient and hessian in matrix
    for(int i=0;i<_nbFF;i++){
      _vgrad[i+i] = Grads[i](0);
      _vgrad[i+i+1] = Grads[i](1);
      int i4 = 4*i;
      _vhess[i4] = Hess[i](0,0);
      _vhess[i4+1] = Hess[i](0,1);
      _vhess[i4+2] = Hess[i](1,0);
      _vhess[i4+3] = Hess[i](1,1);
    }
    const fullMatrix<double> mgrad(_vgrad,2,_nbFF);
    const fullMatrix<double> mhess(_vhess,4,_nbFF);

    int index =0;
    SVector3 v31,v32;
    for(int alpha=0;alpha<2;alpha++){
      for(int beta=0;beta<2;beta++){
        double scalt = scaldot(lb->basisDeriVector(alpha,beta),t0);
        crossprod(lb->basisDeriVector(alpha,beta),lb->basisVector(0),v31);
        crossprod(lb->basisDeriVector(alpha,beta),lb->basisVector(1),v32);
/*
        mvt1.set(index,0,t2[0]*scalt+v32[0]);
        mvt1.set(index,1,-t1[0]*scalt-v31[0]);
        mvt2.set(index,0,t2[1]*scalt+v32[1]);
        mvt2.set(index,1,-t1[1]*scalt-v31[1]);
        mvt3.set(index,0,t2[2]*scalt+v32[2]);
        mvt3.set(index,1,-t1[2]*scalt-v31[2]);
*/
        vt1[index] = t2[0]*scalt+v32[0];
        vt1[index+4] = -t1[0]*scalt-v31[0];
        vt2[index] = t2[1]*scalt+v32[1];
        vt2[index+4] = -t1[1]*scalt-v31[1];
        vt3[index] = t2[2]*scalt+v32[2];
        vt3[index+4] = -t1[2]*scalt-v31[2];
        index++;
      }
    }
    fullMatrix<double> mvt1(vt1,4,2);
    fullMatrix<double> mvt2(vt2,4,2);
    fullMatrix<double> mvt3(vt3,4,2);

    mvt1.scale(_invJ);
    mvt2.scale(_invJ);
    mvt3.scale(_invJ);

    mvt1.mult(mgrad,mbres);
    mbres.axpy(mhess,-t0(0));
    _Bm.copy(mbres,0,4,0,_nbFF,0,0);
    mvt2.mult(mgrad,mbres);
    mbres.axpy(mhess,-t0(1));
    _Bm.copy(mbres,0,4,0,_nbFF,0,_nbFF);
    mvt3.mult(mgrad,mbres);
    mbres.axpy(mhess,-t0(2));
    _Bm.copy(mbres,0,4,0,_nbFF,0,_nbFF+_nbFF);
  }


  void membrane(const shellLocalBasis *lb, const std::vector<TensorialTraits<double>::GradType> &Grads)
  {
    const SVector3 &v31 = lb->basisVector(0);
    const SVector3 &v32 = lb->basisVector(1);
    for(int i=0;i<_nbFF;i++){
      for(int j=0;j<3;j++){
        int jnbFFi = j*_nbFF+i;
        double B01 = 0.5*(v31[j]*Grads[i](1) + v32[j]*Grads[i](0));
        _Bn.set(0,jnbFFi,v31[j]*Grads[i](0));
        _Bn.set(1,jnbFFi,B01);
        _Bn.set(2,jnbFFi,B01);
        _Bn.set(3,jnbFFi,v32[j]*Grads[i](1));
      }
    }
  }

  void set(const linearShellLocalBasisBulk *lb,const std::vector<TensorialTraits<double>::GradType> &Grads,
           const std::vector<TensorialTraits<double>::HessType> &Hess){
    this->bending(lb,Grads,Hess);
    this->membrane(lb,Grads);
  }

  void hat(const linearShellLocalBasisBulk *lb, const fullMatrix<double> &B, fullMatrix<double> &Bh) const
  {
    // create multiplication matrix from push_forward tensor
    // optimal implementation avoiding use of fullMatrix<T>::operator()(const int i,const int j) but finally it is not quicker this way
/*
    mt(0,0) = lb->basisInvPushTensor(0,0);
    mt(0,1) = lb->basisInvPushTensor(0,1);
    mt(1,0) = lb->basisInvPushTensor(1,0);
    mt(1,1) = lb->basisInvPushTensor(1,1);
*/
    const double *mt = lb->basisInvPushTensor();
/*
    mpft1(0,0) = mpft1(0,1) = mpft1(0,2) = mt[0]; mpft1(0,3) = mt[2];
    mpft1(1,0) = mpft1(1,1) = mt[0]; mpft1(1,2) = mpft1(1,3) = mt[2];
    mpft1(2,0) = mpft1(2,2) = mt[0]; mpft1(2,1) = mpft1(2,3) = mt[2];
    mpft1(3,0) = mpft1(3,1) = mpft1(3,2) = mt[1]; mpft1(3,3) = mt[3];
*/
    pft1[0] = pft1[4] = pft1[8] = mt[0];  pft1[12] = mt[2];
    pft1[1] = pft1[5] = mt[0]; pft1[9]  = pft1[13] = mt[2];
    pft1[2] = pft1[10] = mt[0]; pft1[6] = pft1[14] = mt[2];
    pft1[3] = pft1[7] = pft1[11] = mt[1]; pft1[15] = mt[3];
    fullMatrix<double> mpft1(pft1,4,4);
/*
    mpft2(0,0) = mt[0]; mpft2(0,1) = mpft2(0,2) = mpft2(0,3) = mt[2];
    mpft2(1,0) = mpft2(1,2) = mt[1]; mpft2(1,1) = mpft2(1,3) = mt[3];
    mpft2(2,0) = mpft2(2,1) = mt[1]; mpft2(2,2) = mpft2(2,3) = mt[3];
    mpft2(3,0) = mt[1]; mpft2(3,1) = mpft2(3,2) = mpft2(3,3) = mt[3];
*/
    pft2[0] = mt[0]; pft2[4] = pft2[8]  = pft2[12] = mt[2];
    pft2[1] = pft2[9] = mt[1]; pft2[5]  = pft2[13] = mt[3];
    pft2[2] = pft2[6] = mt[1]; pft2[10] = pft2[14] = mt[3];
    pft2[3] = mt[1]; pft2[7] = pft2[11] = pft2[15] = mt[3];
    fullMatrix<double> mpft2(pft2,4,4);

    mpft1.multTByT(mpft2);
    mpft1.mult(B,Bh);
  }

  void hatBending(const linearShellLocalBasisBulk *lb, Bvector &Bhat) const{
    Bhat.resize(_nbFF);
    this->hat(lb,_Bm,Bhat._Bm);
  }

  void hatMembrane(const linearShellLocalBasisBulk *lb, Bvector &Bhat) const{
    Bhat.resize(_nbFF);
    this->hat(lb,_Bn,Bhat._Bn);
  }

  inline double getBending(int isf,int comp,int a, int b) const {
    int apb = a+b;
    switch(apb){
     case 0 :
      return _Bm(0,isf+_nbFF*comp);
      break;
     case 1:
       if(a==0)  return _Bm(1,isf+_nbFF*comp); else return _Bm(2,isf+_nbFF*comp);
      break;
     case 2:
      return _Bm(3,isf+_nbFF*comp);
      break;
    }
    return 0.;
  }
  inline const fullMatrix<double>& getBending() const{return _Bm;}
  inline double getMembrane(int isf,int comp,int a, int b) const {
    int apb = a+b;
    switch(apb){
     case 0 :
      return _Bn(0,isf+_nbFF*comp);
      break;
     case 1:
       if(a==0)  return _Bn(1,isf+_nbFF*comp); else return _Bn(2,isf+_nbFF*comp);
      break;
     case 2:
      return _Bn(3,isf+_nbFF*comp);
      break;
    }
    return 0.;
  }
  inline const fullMatrix<double>& getMembrane() const{return _Bn;}

  inline void rhostrain(const fullVector<double> &disp,fullVector<double> &vres){
    _Bm.mult(disp,vres);
  }
  inline void epsilonstrain(const fullVector<double> &disp, fullVector<double> &vres){
    _Bn.mult(disp,vres);
  }
  inline void strain(const fullVector<double> &disp, double *eps, double *rho){
    _Bm.mult(disp,vres);
    rho[0] = vres(0);
    rho[1] = vres(3);
    rho[3] = vres(1);
    _Bn.mult(disp,vres);
    eps[0] = vres(0);
    eps[1] = vres(3);
    eps[3] = vres(1);
  }
  void restart()
  {
    restartManager::restart(_nbFF);
    _Bm.resize(4,3*_nbFF); // no effect for write case
    _Bn.resize(4,3*_nbFF); // no effect for write case
    restartManager::restart(_Bm.getDataPtr(),12*_nbFF);
    restartManager::restart(_Bn.getDataPtr(),12*_nbFF);
    return;
  }
};
#endif // DGC0SHELLMANAGEBVECTOR

// Old commented functions (school lean)
/*

static void  Computebm(const shellLocalBasis *lb,const double Grads[][3],
                 const double Hess[][3][3], const int &n, double B[256][3][2][2]){
  SVector3 t1(0.,0.,0.), t2(0.,0.,0.), p1(0.,0.,0.), p2(0.,0.,0.);
  SVector3 t1t(0.,0.,0.),t2t(0.,0.,0.),p1t(0.,0.,0.),p2t(0.,0.,0.);
  double scalt,scalt1, scalt2, scalp1, scalp2;
  double invJ = 1./lb->getJacobian();
  t1 = crossprod(lb->basisVector(0),lb->basisNormal());
  t2 = crossprod(lb->basisVector(1),lb->basisNormal());
  for(int alpha=0;alpha<2;alpha++){
    for(int beta=0;beta<2;beta++){
      scalt = invJ * scaldot(lb->basisDeriVector(alpha,beta),lb->basisNormal());
      p1 = crossprod(lb->basisDeriVector(alpha,beta),lb->basisVector(0));
      p2 = crossprod(lb->basisDeriVector(alpha,beta),lb->basisVector(1));
      for(int i=0;i<n;i++){
        scalt1 = scalt * Grads[i][0];
        scalt2 = scalt * Grads[i][1];
        scalp1 = invJ*Grads[i][0];
        scalp2 = invJ*Grads[i][1];
        t1t = scalt2 * t1;
        t2t = scalt1 * t2;
        p1t = scalp2 * p1;
        p2t = scalp1 * p2;
        t1t+=p1t;
        t2t+=p2t;
        t2t-=t1t;
        for(int ii=0;ii<3;ii++){
          B[i][ii][alpha][beta] = t2t(ii)-lb->basisNormal(ii)*Hess[i][alpha][beta];
        }
      }
    }
  }
}

static void computebn(const shellLocalBasis *lb, const double Grads[][3], const int n, double B[][3][2][2]){
  for(int i=0;i<n;i++){
    for(int a=0;a<2;a++)
      for(int b=0;b<2;b++){
        B[i][0][a][b] = 0.5*(lb->basisVector(a,0)*Grads[i][b]+lb->basisVector(b,0)*Grads[i][a] );
        B[i][1][a][b] = 0.5*(lb->basisVector(a,1)*Grads[i][b]+lb->basisVector(b,1)*Grads[i][a] );
        B[i][2][a][b] = 0.5*(lb->basisVector(a,2)*Grads[i][b]+lb->basisVector(b,2)*Grads[i][a] );
      }
  }

}

static void computeBhat(const shellLocalBasis *lb, const int n, const double B[256][3][2][2], double Bhat[256][3][2][2]){
  for(int i=0;i<n;i++){
    for(int ii=0;ii<3;ii++){
      Bhat[i][ii][0][0] =0.; Bhat[i][ii][0][1]=0.; Bhat[i][ii][1][0]=0.; Bhat[i][ii][1][1]=0.;
      for(int j=0;j<2;j++)
        for(int k=0;k<2;k++){
          Bhat[i][ii][0][0] += lb->gett(0,j)*B[i][ii][j][k]*lb->gett(0,k);
          Bhat[i][ii][0][1] += lb->gett(0,j)*B[i][ii][j][k]*lb->gett(1,k);
          Bhat[i][ii][1][0] += lb->gett(1,j)*B[i][ii][j][k]*lb->gett(0,k);
          Bhat[i][ii][1][1] += lb->gett(1,j)*B[i][ii][j][k]*lb->gett(1,k);
        }
    }
  }
}







*/
