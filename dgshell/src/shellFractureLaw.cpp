//
// C++ Interface: matrialLaw
//
// Description: Class with definition of materialLaw of fracture for shell (Has to be in a separated file to avoid SWIG warning)
//
//
// Author:  <Gauthier BECKER>, (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "shellFractureLaw.h"

shellFractureByCohesiveLaw::shellFractureByCohesiveLaw(const int num,const int nbulk,
                                                       const int ncoh) : shellMaterialLaw(num,0,0,0,false), // unused thickness and simpson's point use bulk material Law
                                                                          fractureBy2Laws<shellMaterialLaw,shellCohesiveLaw>(num,nbulk,ncoh)
                                                                          {}
shellFractureByCohesiveLaw::shellFractureByCohesiveLaw(const shellFractureByCohesiveLaw &source) :
                                                                          shellMaterialLaw(source),
                                                                          fractureBy2Laws<shellMaterialLaw,shellCohesiveLaw>(source)
                                                                          {}

void shellFractureByCohesiveLaw::createIPState(IPStateBase* &ips,bool hasBodyForce, const bool* state_,
                                          const MElement *ele, const int nbFF_, const IntPt *GP,const int gpt) const
{
  const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
  if(iele!=NULL) // no fracture for bulk element -->use bulk only
  {
    double fsc = _mfrac->getFractureStrengthFactor();
    IPVariable* ipvi = new IPShellFractureCohesive(fsc);
    IPVariable* ipv1 = new IPShellFractureCohesive(fsc);
    IPVariable* ipv2 = new IPShellFractureCohesive(fsc);
    IPShellFractureCohesive *ipvf = static_cast<IPShellFractureCohesive*>(ipvi);
    IPVariable* ipv=NULL;
    _mbulk->createIPVariable(ipv,hasBodyForce,ele,nbFF_,GP,gpt);
    ipvf->setIPvBulk(ipv);
    ipvf = static_cast<IPShellFractureCohesive*>(ipv1);
    ipv=NULL;
    _mbulk->createIPVariable(ipv,hasBodyForce,ele,nbFF_,GP,gpt);
    ipvf->setIPvBulk(ipv);
    ipvf = static_cast<IPShellFractureCohesive*>(ipv2);
    ipv=NULL;
    _mbulk->createIPVariable(ipv,hasBodyForce,ele,nbFF_,GP,gpt);
    ipvf->setIPvBulk(ipv);
    if(ips != NULL) delete ips;
    ips = new IP3State(state_,ipvi,ipv1,ipv2);
    // frac ipv has to be created later in fracture initialization BE AWARE IN YOUR MATERIAL LAW
  }
  else
  {
    _mbulk->createIPState(ips,hasBodyForce,state_,ele,nbFF_,GP,gpt);
  }
}

void shellFractureByCohesiveLaw::createIPVariable(IPVariable* &ipv, bool hasBodyForce, const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
  const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
  if(iele!=NULL) // No fracture on bulk points
  {
    if(ipv != NULL) delete ipv;
    ipv = new IPShellFractureCohesive();
    IPShellFractureCohesive *ipvf = static_cast<IPShellFractureCohesive*>(ipv);
    IPVariable* ipv2;
    _mbulk->createIPVariable(ipv2,hasBodyForce,ele,nbFF_,GP,gpt);
    ipvf->setIPvBulk(ipv2);
    // frac ipv has to be created later in fracture initialization BE AWARE IN YOUR MATERIAL LAW
  }
  else
  {
    _mbulk->createIPVariable(ipv,hasBodyForce,ele,nbFF_,GP,gpt);
  }

}

void shellFractureByCohesiveLaw::initialBroken(IPStateBase *ips) const
{
  IPShellFractureCohesive *ipvfcur = static_cast<IPShellFractureCohesive*>(ips->getState(IPStateBase::current));
  IPShellFractureCohesive *ipvfprev = static_cast<IPShellFractureCohesive*>(ips->getState(IPStateBase::previous));
  IPShellFractureCohesive *ipvfinit = static_cast<IPShellFractureCohesive*>(ips->getState(IPStateBase::initial));
  IPLinearCohesiveShell *ipvccur = ipvfcur->getIPvFrac();
  IPLinearCohesiveShell *ipvcinit = ipvfinit->getIPvFrac();
  IPLinearCohesiveShell *ipvcprev = ipvfprev->getIPvFrac();
  ipvfcur->broken();
  ipvfinit->broken();
  ipvfprev->broken();
  IPVariable *ipvccur2 = static_cast<IPVariable*>(ipvccur);
  IPVariable *ipvcinit2 = static_cast<IPVariable*>(ipvcinit);
  IPVariable *ipvcprev2 = static_cast<IPVariable*>(ipvcprev);
  _mfrac->createIPVariable(ipvccur2);
  _mfrac->createIPVariable(ipvcinit2);
  _mfrac->createIPVariable(ipvcprev2);
  ipvfcur->setIPvFrac(ipvccur2);
  ipvfprev->setIPvFrac(ipvcprev2);
  ipvfinit->setIPvFrac(ipvcinit2);
  ipvccur = ipvfcur->getIPvFrac();
  ipvcprev = ipvfprev->getIPvFrac();
  ipvcinit = ipvfinit->getIPvFrac();
  ipvccur->setDataFromLaw(_mfrac->getSigmac(),_mfrac->getSigmac(),0.,_mfrac->getGc(),_mfrac->getBeta(),true);
  ipvcprev->setDataFromLaw(_mfrac->getSigmac(),_mfrac->getSigmac(),0.,_mfrac->getGc(),_mfrac->getBeta(),true);
  ipvcinit->setDataFromLaw(_mfrac->getSigmac(),_mfrac->getSigmac(),0.,_mfrac->getGc(),_mfrac->getBeta(),true);
}

void shellFractureByCohesiveLaw::stress(IPStateBase *ips, bool stiff, const bool checkfrac)
{
  _mbulk->stress(ips, stiff);
  IPVariableShell *ipvcur = static_cast<IPVariableShell*>(ips->getState(IPStateBase::current));
  IPVariableShell *ipvprev = static_cast<IPVariableShell*>(ips->getState(IPStateBase::previous));
  // Now check for fracture on interface
  if((ipvcur->isInterface()) and (checkfrac))
  {
    const shellLocalBasisInter* lbscur = static_cast<shellLocalBasisInter*>(ipvcur->getshellLocalBasisOfInterface());
    IPShellFractureCohesive *ipvfprev = static_cast<IPShellFractureCohesive*>(ipvprev);
    IPShellFractureCohesive *ipvf = static_cast<IPShellFractureCohesive*>(ipvcur);
    if(!ipvfprev->isbroken()){ // no check if already broken
      bool isb = _mfrac->checkBroken(ipvcur,ipvprev);
      // Compute fracture data if broken SHOULD BE SOMEWHERE ELSE ??
      if(isb){
        // reduction element
        IPLinearCohesiveShell *ipvccur = ipvf->getIPvFrac();
        std::vector<SVector3> vn(2);
        std::vector<SVector3> vm(2);
        reductionElement &n0 = ipvccur->getn0();
        reductionElement &m0 = ipvccur->getm0();
        _mbulk->reduction(ips,vn,vm,true);
        // init reduction element of previous ipvariable (needed for matrix computation by perturbation)
        IPLinearCohesiveShell *ipvcprev = ipvfprev->getIPvFrac();
        reductionElement &n0prev = ipvcprev->getn0();
        reductionElement &m0prev = ipvcprev->getm0();
        for(int alpha=0;alpha<2;alpha++){
          for(int beta=0;beta<2;beta++){
            n0(alpha,beta) = dot(vn[alpha],lbscur->dualBasisVector(beta));
            m0(alpha,beta) = dot(vm[alpha],lbscur->dualBasisVector(beta));
            n0prev(alpha,beta) = dot(vn[alpha],lbscur->dualBasisVector(beta));
            m0prev(alpha,beta) = dot(vm[alpha],lbscur->dualBasisVector(beta));
          }
        }
      }
      else{
        ipvf->nobroken();
      }
    }
    else{
      ipvf->broken();
    }
  }
}

