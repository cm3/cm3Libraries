//
// C++ Interface: opening for shell
//
// Description: compute opening for shell formulation
//
//
// Author:  <Gauthier BECKER>, (C) 2010
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef OPENINGFORSHELL_H_
#define OPENINGFORSHELL_H_
#include "shellIPVariable.h"
void openingForShell(const SVector3 &ujump, const double rjump[3],const IPShellFractureCohesive *ipv,
                     double &deltan, double &deltat, double &delta);
void openingForShell(const SVector3 &ujump, const double rjump[3],const IPLinearCohesiveShell *ipv,
                     const shellLocalBasis *lbs,const double h,double &deltan, double &deltat, double &delta);
#endif //OPENINGFORSHELL_H_
