//
// C++ Interface: terms
//
// Description: Hooke Tensor for shell (linear elastic)
//
//
// Author:  <Gauthier BECKER>, (C) 2010
//
// Copyright: See COPYING file that comes with this distribution
//
//

# ifndef LINEARELASTICSHELLHOOKETENSOR_H_
# define LINEARELASTICSHELLHOOKETENSOR_H_
#include <stdio.h>
#include "shellLocalBasis.h"
#include <math.h>
#include "restartManager.h"
// tensor of dimension 4 in 2D (used for Hooke tensor)
class Tensor4dim2{
 protected :
  double tensor[2][2][2][2];
 public :
  Tensor4dim2(){
    for(int a=0;a<2;a++)
      for(int b=0;b<2;b++)
        for(int c=0;c<2;c++)
          for(int d=0;d<2;d++)
            tensor[a][b][c][d]=0.;
  }

  Tensor4dim2(const Tensor4dim2 &_in){
    for(int i=0;i<2;i++)
      for(int j=0;j<2;j++)
        for(int ii=0;ii<2;ii++)
          for(int jj=0;jj<2;jj++){
            this->set(i,j,ii,jj,_in(i,j,ii,jj));
          }
  }
  virtual ~Tensor4dim2(){}

  void set(const int a,const int b,const int c,const int d,const double dd){tensor[a][b][c][d]=dd;}
  void setAll(const double dd){
    for(int a=0;a<2;a++)
      for(int b=0;b<2;b++)
        for(int c=0;c<2;c++)
          for(int d=0;d<2;d++)
            tensor[a][b][c][d]=dd;
   }

  Tensor4dim2 & operator = (const Tensor4dim2 &_in){
    for(int i=0;i<2;i++)
      for(int j=0;j<2;j++)
        for(int ii=0;ii<2;ii++)
          for(int jj=0;jj<2;jj++){
            this->set(i,j,ii,jj,_in(i,j,ii,jj));
          }
    return *this;
  }

  double operator() (const int a,const int b,const int c,const int d) const {return tensor[a][b][c][d];}
  double& operator() (const int a,const int b,const int c,const int d) {return tensor[a][b][c][d];}
  double get(const int a,const int b,const int c,const int d) const {return tensor[a][b][c][d];}

  Tensor4dim2 operator+ (Tensor4dim2 &H){
    Tensor4dim2 Hsum;
    double temp;
    for(int a=0;a<2;a++)
      for(int b=0;b<2;b++)
        for(int c=0;c<2;c++)
          for(int d=0;d<2;d++){
            temp=tensor[a][b][c][d]+H(a,b,c,d);
            Hsum.set(a,b,c,d,temp);
          }
    return Hsum;
  }

  Tensor4dim2  operator* (const double dd){
    Tensor4dim2 dH;
    double temp;
    for(int a=0;a<2;a++)
      for(int b=0;b<2;b++)
        for(int c=0;c<2;c++)
          for(int d=0;d<2;d++){
            temp=dd*tensor[a][b][c][d];
            dH.set(a,b,c,d,temp);
          }
    return dH;
  }

  void operator+= (const Tensor4dim2 &H){
    double temp;
    for(int a=0;a<2;a++)
      for(int b=0;b<2;b++)
        for(int c=0;c<2;c++)
          for(int d=0;d<2;d++){
            temp=tensor[a][b][c][d]+H(a,b,c,d);
            this->set(a,b,c,d,temp);
          }
  }
  void operator*= (const double dd){
    double temp;
    for(int a=0;a<2;a++)
      for(int b=0;b<2;b++)
        for(int c=0;c<2;c++)
          for(int d=0;d<2;d++){
            temp=dd*tensor[a][b][c][d];
            this->set(a,b,c,d,temp);
          }
  }
  void print(){
    for(int a=0;a<2;a++)
      for(int b=0;b<2;b++)
        for(int c=0;c<2;c++)
          for(int d=0;d<2;d++)
            printf("(%d,%d,%d,%d)= %f\n",a,b,c,d,tensor[a][b][c][d]);

  }

  void restart()
  {
    for(int a=0;a<2;a++)
      for(int b=0;b<2;b++)
        for(int c=0;c<2;c++)
          for(int d=0;d<2;d++)
            restartManager::restart(tensor[a][b][c][d]);
    return;
  }
};
class hookeTensor;
class LinearElasticShellHookeTensor : public Tensor4dim2{
 public :
  LinearElasticShellHookeTensor(){};
  LinearElasticShellHookeTensor(const LinearElasticShellHookeTensor &source) : Tensor4dim2(source){}
  LinearElasticShellHookeTensor& operator=(const Tensor4dim2 &source)
  {
    Tensor4dim2::operator=(source);
		return *this;
  }
  virtual ~LinearElasticShellHookeTensor(){};
  void set(const shellLocalBasis *lb,const double &C11,const double &nu);
  void set(const shellLocalBasis *lb,const double &nu){this->set(lb,1.,nu);}
  void lambdahat(const LinearElasticShellHookeTensor &source, const shellLocalBasisBulk *lb);
//  void hat(const shellLocalBasisBulk *lb, const double C11, const double nu); // unused Check optimized implementation!
//  void hat(const shellLocalBasisBulk *lb,const LinearElasticShellHookeTensor *H);
  void hat(const shellLocalBasisBulk *lb);
  void mean(const LinearElasticShellHookeTensor *Hp, const LinearElasticShellHookeTensor *Hm);
};

// rotation matrix
class rotMatrix : public fullMatrix<double>{
  public:
    rotMatrix(const double theta) : fullMatrix<double>(2,2){
      double c = cos(theta);
      double s = sin(theta);
      this->set(0,0,c);
      this->set(0,1,s);
      this->set(1,0,-s);
      this->set(1,1,c);
    }

    rotMatrix(const double theta, const double phi , const double psi) : fullMatrix<double>(3,3){
      double ct = cos(theta);
      double st = sin(theta);
      double cphi = cos(phi);
      double sphi = sin(phi);
      double cpsi = cos(psi);
      double spsi = sin(psi);
 //     this->set(0,0,ct*cpsi);
 //     this->set(0,1,-cphi*spsi+sphi*st*cpsi);
 //     this->set(0,2,sphi*spsi+cphi*st*cpsi);
 //     this->set(1,0,ct*spsi);
 //     this->set(1,1,cphi*cpsi+sphi*st*spsi);
 //     this->set(1,2,-sphi*cpsi+cphi*st*spsi);
 //     this->set(2,0,-st);
 //     this->set(2,1,sphi*ct);
 //     this->set(2,2,cphi*ct);
 ///////////////////////////////////////////////////////////////////
 // the old one
 ///////////////////////////////////////////////////////////////////

 //      this->set(0,0,cphi*cphi);
 //      this->set(0,1,sphi*sphi);
 //     this->set(0,2,cphi*sphi);
 //     this->set(1,0,sphi*sphi);
 //     this->set(1,1,cphi*cphi);
 //     this->set(1,2,-sphi*cphi);
 //     this->set(2,0,-2.*cphi*sphi);
 //     this->set(2,1,2.*cphi*sphi);
 //     this->set(2,2,cphi*cphi-sphi*sphi);

 ///////////////////////////////////////////////////////////////////
 // the one of 30-05-11
 ///////////////////////////////////////////////////////////////////

        this->set(0,0,cpsi*cphi);
        this->set(0,1,-spsi*ct+cpsi*sphi*st);
        this->set(0,2,spsi*st+cpsi*sphi*ct);
        this->set(1,0,spsi*cphi);
        this->set(1,1,cpsi*ct+spsi*sphi*st);
        this->set(1,2,-cpsi*st+spsi*sphi*ct);
        this->set(2,0,-sphi);
        this->set(2,1,cphi*st);
        this->set(2,2,ct*cphi);
 /////////////////////////////////////////////////////////////////////
 /////////////////////////////////////////////////////////////////////
    }

    void setAngle(const double theta,const double phi, const double psi){
      double ct = cos(theta);
      double st = sin(theta);
      double cphi = cos(phi);
      double sphi = sin(phi);
      double cpsi = cos(psi);
      double spsi = sin(psi);
//      this->set(0,0,ct*cpsi);
//      this->set(0,1,-cphi*spsi+sphi*st*cpsi);
//      this->set(0,2,sphi*spsi+cphi*st*cpsi);
//      this->set(1,0,ct*spsi);
//      this->set(1,1,cphi*cpsi+sphi*st*spsi);
//      this->set(1,2,-sphi*cpsi+cphi*st*spsi);
//      this->set(2,0,-st);
//      this->set(2,1,sphi*ct);
//      this->set(2,2,cphi*ct);
//      this->set(0,0,cphi*cphi);
//      this->set(0,1,sphi*sphi);
//      this->set(0,2,cphi*sphi);
//      this->set(1,0,sphi*sphi);
//      this->set(1,1,cphi*cphi);
//      this->set(1,2,-sphi*cphi);
//      this->set(2,0,-2.*cphi*sphi);
//      this->set(2,1,2.*cphi*sphi);
//      this->set(2,2,cphi*cphi-sphi*sphi);

//////////////////////////////////////////////////////////////
// the old one
//////////////////////////////////////////////////////////////
//      this->set(0,0,cphi);
//      this->set(0,1,sphi);
//      this->set(0,2,0.);
//      this->set(1,0,-sphi);
//      this->set(1,1,cphi);
//      this->set(1,2,0.);
//      this->set(2,0,0.);
//      this->set(2,1,0.);
//      this->set(2,2,1.);
//////////////////////////////////////////////////////////////
// the one of 30-05-11
//////////////////////////////////////////////////////////////

        this->set(0,0,cpsi*cphi);
        this->set(0,1,-spsi*ct+cpsi*sphi*st);
        this->set(0,2,spsi*st+cpsi*sphi*ct);
        this->set(1,0,spsi*cphi);
        this->set(1,1,cpsi*ct+spsi*sphi*st);
        this->set(1,2,-cpsi*st+spsi*sphi*ct);
        this->set(2,0,-sphi);
        this->set(2,1,cphi*st);
        this->set(2,2,ct*cphi);


    }

    ~rotMatrix(){}
    rotMatrix(const rotMatrix &source) : fullMatrix<double>(source){}

};

class Tensor4dim3{
  protected:
    double comp[3][3][3][3];
  public :
    Tensor4dim3(){
      for(int a=0;a<3;a++)
        for(int b=0;b<3;b++)
          for(int c=0;c<3;c++)
            for(int d=0;d<3;d++)
              comp[a][b][c][d]=0.;
    }

    // Add operator
    double& operator() (const int a, const int b, const int c, const int d){return comp[a][b][c][d];}
    double operator() (const int a, const int b, const int c, const int d) const {return comp[a][b][c][d];}
    Tensor4dim3(const Tensor4dim3 &_in){
      for(int i=0;i<3;i++)
        for(int j=0;j<3;j++)
          for(int ii=0;ii<3;ii++)
            for(int jj=0;jj<3;jj++){
              comp[i][j][ii][jj] = _in(i,j,ii,jj);
            }
    }
    ~Tensor4dim3(){}

    void set(const int a,const int b,const int c,const int d,const double dd){comp[a][b][c][d]=dd;}
    void setAll(const double dd){
      for(int a=0;a<3;a++)
        for(int b=0;b<3;b++)
          for(int c=0;c<3;c++)
            for(int d=0;d<3;d++)
              comp[a][b][c][d]=dd;

    }

    Tensor4dim3 & operator = (const Tensor4dim3 &_in){
      for(int i=0;i<3;i++)
        for(int j=0;j<3;j++)
          for(int ii=0;ii<3;ii++)
            for(int jj=0;jj<3;jj++){
              comp[i][j][ii][jj] = _in(i,j,ii,jj);
            }
      return *this;
    }

    Tensor4dim3 operator+ (Tensor4dim3 &H){
      Tensor4dim3 Hsum;
      for(int a=0;a<3;a++)
        for(int b=0;b<3;b++)
          for(int c=0;c<3;c++)
            for(int d=0;d<3;d++){
              Hsum(a,b,c,d)=comp[a][b][c][d]+H(a,b,c,d);
            }
    return Hsum;
    }

    Tensor4dim3  operator* (const double dd){
      Tensor4dim3 dH;
      for(int a=0;a<3;a++)
        for(int b=0;b<3;b++)
          for(int c=0;c<3;c++)
            for(int d=0;d<3;d++){
              dH(a,b,c,d)=dd*comp[a][b][c][d];
            }
      return dH;
    }

    void operator+= (const Tensor4dim3 &H){
      for(int a=0;a<3;a++)
        for(int b=0;b<3;b++)
          for(int c=0;c<3;c++)
            for(int d=0;d<3;d++){
              comp[a][b][c][d]+=H(a,b,c,d);
            }
    }
    void operator*= (const double dd){
      for(int a=0;a<3;a++)
        for(int b=0;b<3;b++)
          for(int c=0;c<3;c++)
            for(int d=0;d<3;d++){
              comp[a][b][c][d]*=dd;
            }
    }
    void print(){
      for(int a=0;a<3;a++)
        for(int b=0;b<3;b++)
          for(int c=0;c<3;c++)
            for(int d=0;d<3;d++)
              printf("(%d,%d,%d,%d)= %f\n",a,b,c,d,comp[a][b][c][d]);

    }
};


// Full Hooke tensor (write with the 81 components because a rotation matrix can be applied on it)
class hookeTensor : public Tensor4dim3{
  public:
    // For an orthotropic law
    hookeTensor(const double Ex, const double Ey, const double Ez,
                const double nuxy, const double nuxz, const double nuyz,
                const double nuyx, const double nuzx, const double nuzy,
                const double muxy, const double muxz, const double muyz,
                const double D) : Tensor4dim3(){
      // multiplication of yound moduli
      double EyEzD = Ey*Ez*D;
      double EyExD = Ey*Ex*D;
      double ExEzD = Ex*Ez*D;
      // no zero components
      comp[0][0][0][0] = (1. - nuyz*nuzy) / EyEzD;
      comp[0][0][1][1] = (nuyx + nuzx*nuyz) / EyEzD;
      comp[0][0][2][2] = (nuzx + nuyx*nuzy) / EyEzD;
      comp[1][1][0][0] = (nuxy + nuzy*nuxz) / ExEzD;
      comp[1][1][1][1] = (1. - nuxz*nuzx) / ExEzD;
      comp[1][1][2][2] = (nuzy + nuxy*nuzx) / ExEzD;
      comp[2][2][0][0] = (nuxz + nuxy*nuyz) / EyExD;
      comp[2][2][1][1] = (nuyz + nuxz*nuyx) / EyExD;
      comp[2][2][2][2] = (1. - nuyx*nuxy) / EyExD;
      comp[1][2][1][2] = comp[1][2][2][1] = comp[2][1][2][1] = comp[2][1][1][2] = muyz;
      comp[0][1][0][1] = comp[0][1][1][0] = comp[1][0][0][1] = comp[1][0][1][0] = muxy;
      comp[0][2][0][2] = comp[0][2][2][0] = comp[2][0][2][0] = comp[2][0][0][2] = muxz;
    }
    hookeTensor() : Tensor4dim3(){}

    void set(const double Ex, const double Ey, const double Ez,
                const double nuxy, const double nuxz, const double nuyz,
                const double nuyx, const double nuzx, const double nuzy,
                const double muxy, const double muxz, const double muyz,
                const double D){
      // multiplication of yound moduli
      double EyEzD = Ey*Ez*D;
      double EyExD = Ey*Ex*D;
      double ExEzD = Ex*Ez*D;
      // no zero components
      comp[0][0][0][0] = (1. - nuyz*nuzy) / EyEzD;
      comp[0][0][1][1] = (nuyx + nuzx*nuyz) / EyEzD;
      comp[0][0][2][2] = (nuzx + nuyx*nuzy) / EyEzD;
      comp[1][1][0][0] = (nuxy + nuzy*nuxz) / ExEzD;
      comp[1][1][1][1] = (1. - nuxz*nuzx) / ExEzD;
      comp[1][1][2][2] = (nuzy + nuxy*nuzx) / ExEzD;
      comp[2][2][0][0] = (nuxz + nuxy*nuyz) / EyExD;
      comp[2][2][1][1] = (nuyz + nuxz*nuyx) / EyExD;
      comp[2][2][2][2] = (1. - nuyx*nuyx) / EyExD;
      comp[1][2][1][2] = comp[1][2][2][1] = comp[2][1][2][1] = comp[2][1][1][2] = muyz;
      comp[0][1][0][1] = comp[0][1][1][0] = comp[1][0][0][1] = comp[1][0][1][0] = muxy;
      comp[0][2][0][2] = comp[0][2][2][0] = comp[2][0][2][0] = comp[2][0][0][2] = muxz;
    }

    void rotate(const rotMatrix &R){
      // creation of a tempory Tensor
      Tensor4dim3 copyHooke(*this);
      this->setAll(0.);
      for(int i=0;i<3;i++)
        for(int j=0;j<3;j++)
          for(int k=0;k<3;k++)
            for(int l=0;l<3;l++)
              for(int m=0;m<3;m++)
                for(int n=0;n<3;n++)
                  for(int p=0;p<3;p++)
                    for(int q=0;q<3;q++)
                    ///////////////////////////////////////////////////////////
                    // the old one
                    ///////////////////////////////////////////////////////////

                     comp[i][j][k][l] += R(m,i)*R(n,j)*copyHooke(m,n,p,q)*R(p,k)*R(q,l);

                    ////////////////////////////////////////////////////////////
                    // the one of 30-05-11
                    ////////////////////////////////////////////////////////////


    }
};

// Tensor for shearing (out of plane continuity in full dg shell)
// tensor 2 dim 2
class Tensor2dim2{
  protected :
    double tensor[2][2];
  public :
    Tensor2dim2(){
      for(int a=0;a<2;a++)
        for(int b=0;b<2;b++)
          tensor[a][b]=0.;
    }

    Tensor2dim2(const Tensor2dim2 &_in){
      for(int i=0;i<2;i++)
        for(int j=0;j<2;j++)
          this->set(i,j,_in(i,j));
    }
    ~Tensor2dim2(){}
    void set(const int a,const int b,const double dd){tensor[a][b]=dd;}
    void setAll(const double dd){
      for(int a=0;a<2;a++)
        for(int b=0;b<2;b++)
          this->tensor[a][b]=dd;

    }
    Tensor2dim2 & operator = (const Tensor2dim2 &_in){
      for(int i=0;i<2;i++)
        for(int j=0;j<2;j++)
          this->set(i,j,_in(i,j));
      return *this;
    }

    double operator() (const int a,const int b) const {return tensor[a][b];}
    double& operator() (const int a,const int b){return tensor[a][b];}

    Tensor2dim2 operator+ (Tensor2dim2 &H){
      Tensor2dim2 Hsum;
      for(int a=0;a<2;a++)
        for(int b=0;b<2;b++)
          Hsum(a,b) = tensor[a][b]+H(a,b);
      return Hsum;
    }

    Tensor2dim2  operator* (const double dd){
      Tensor2dim2 dH;
      for(int a=0;a<2;a++)
        for(int b=0;b<2;b++)
          dH(a,b) = dd*tensor[a][b];
      return dH;
    }

    void operator+= (const Tensor2dim2 &H){
      for(int a=0;a<2;a++)
        for(int b=0;b<2;b++)
          this->set(a,b,tensor[a][b]+H(a,b));
    }
    void operator*= (const double dd){
      for(int a=0;a<2;a++)
        for(int b=0;b<2;b++)
          this->set(a,b,dd*tensor[a][b]);
    }
    void print(){
      for(int a=0;a<2;a++)
        for(int b=0;b<2;b++)
          printf("(%d,%d)= %f\n",a,b,tensor[a][b]);
    }
};

class linearElasticShellShearingHookeTensor : public Tensor2dim2{
  public:
    linearElasticShellShearingHookeTensor() : Tensor2dim2(){}
    ~linearElasticShellShearingHookeTensor(){}
    void set(const shellLocalBasis *lb,const double &C11=1.);
    void set(const hookeTensor &H,const double thickfactor, const shellLocalBasis *lb);
    void mean(const linearElasticShellShearingHookeTensor &Hp, const linearElasticShellShearingHookeTensor &Hm);
};

#endif // LinearElasticShellHooketensor
