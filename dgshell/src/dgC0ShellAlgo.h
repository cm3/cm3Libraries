//
// C++ Interface: terms
//
// Description: FilterDof for Dg Shell
//
//
// Author:  <Gauthier BECKER>, (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
#ifndef _DGC0SHELLALGO_H_
#define _DGC0SHELLALGO_H_
#include "Dof3IntType.h"
class DgC0ShellFilterDofComponent :public FilterDof
{
  int comp;
 public :
  DgC0ShellFilterDofComponent(int comp_): comp(comp_) {}
  virtual bool operator()(Dof key)
  {
    int type=key.getType();
    int icomp,iphys,inode;
    Dof3IntType::getThreeIntsFromType(type, icomp, iphys,inode);
    if (icomp==comp) return true;
    return false;
  }
  virtual void filter(const std::vector<Dof> R, std::vector<Dof> &Rfilter){
    Rfilter.clear();
    for(int i=0; i<R.size();i++)
      if(this->operator()(R[i])) Rfilter.push_back(R[i]);
  }
};

#endif
