//
// C++ Interface: Quadrature Rule
//
// Description: Define 1D Gauss-Lobatto quadrature rule (number of point 3 to 6)
//
//
// Author:  <Gauthier BECKER>, (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef GAUSSLOBATTOQUADRATURERULES_H_
#define GAUSSLOBATTOQUADRATURERULES_H_
#include "quadratureRules.h"
class GaussLobatto1DQuadrature : public QuadratureBase
{
 private:
  int _order; // npts = (order+1)/2
 public:
  GaussLobatto1DQuadrature(int order=5) : _order(order){}
  GaussLobatto1DQuadrature(const GaussLobatto1DQuadrature &other) : _order(other._order){}
  virtual ~GaussLobatto1DQuadrature(){}
  virtual int getIntPoints(MElement *e, IntPt **GP);
};
#endif // GAUSSLOBATTOQUADRATURERULES_H_
