//
// C++ Interface: terms
//
// Description: Class of local basis for shells element
//
//
// Author:  <Gauthier BECKER>, (C) 2010
//
// Copyright: See COPYING file that comes with this distribution
//
//

#include "shellLocalBasis.h"
#include "currentConfig.h"
#include "restartManager.h"
shellLocalBasisBulk& shellLocalBasisBulk::operator=(const shellLocalBasis &source)
{
  shellLocalBasis::operator=(source);
  return *this;
}

void linearShellLocalBasisBulk::setphiall(const double d){
  for(int i=0;i<2;i++){
    phi0[i]=d;
    phi0d[i]=d;
    derivt0[i]=d;
    for(int jj=0;jj<2;jj++)
      derivphi0[i][jj]= d;
  }
}

linearShellLocalBasisBulk::linearShellLocalBasisBulk() : shellLocalBasisBulk()
{
  phi0.resize(2);
  phi0d.resize(2);
  derivt0.resize(2);
  this->setphiall(0.);
}

// Copy Constructor
linearShellLocalBasisBulk& linearShellLocalBasisBulk::operator=(const shellLocalBasis &source)
{
  shellLocalBasisBulk::operator=(source);
  const linearShellLocalBasisBulk*src = static_cast<const linearShellLocalBasisBulk*>(&source);
  phi0.resize(2);
  phi0d.resize(2);
  derivt0.resize(2);
  for(int i=0;i<2;i++){
    phi0[i] = src->phi0[i];
    phi0d[i] = src->phi0d[i];
    derivt0[i] = src->derivt0[i];
  }
  t0= src->t0;
  detJ0 = src->detJ0;
  for(int i=0;i<2;i++){
    for(int j=0;j<2;j++){
      derivphi0[i][j] = src->derivphi0[i][j];
      _lambda[i][j]=src->_lambda[i][j];
    }
  }
  for(int i=0;i<4;i++)
  {
    T[i] = src->T[i];
    t[i] = src->t[i];
  }
  return *this;
}

linearShellLocalBasisBulk::linearShellLocalBasisBulk(const linearShellLocalBasisBulk &source) : shellLocalBasisBulk(source)
{
  phi0.resize(2);
  phi0d.resize(2);
  derivt0.resize(2);
  for(int i=0;i<2;i++){
    phi0[i] = source.phi0[i];
    phi0d[i] = source.phi0d[i];
    derivt0[i] = source.derivt0[i];
  }
  t0 = source.t0;
  detJ0 = source.detJ0;
  for(int i=0;i<2;i++){
    for(int j=0;j<2;j++){
      derivphi0[i][j] = source.derivphi0[i][j];
      _lambda[i][j]=source._lambda[i][j];
    }
  }
  for(int i=0;i<4;i++)
  {
    T[i] = source.T[i];
    t[i] = source.t[i];
  }
}

void linearShellLocalBasisBulk::set(MElement *ele, const std::vector<TensorialTraits<double>::GradType> &Grads,
                              const std::vector<TensorialTraits<double>::HessType> &Hess)
{
  const int nbFF = ele->getNumVertices();
  // Compute local basis vector
  this->setphiall(0.);
  double x,y,z;
  MVertex *ver;
  for(int j=0;j<nbFF;j++){
    ver = ele->getVertex(j);
    x = ver->x();
    y = ver->y();
    z = ver->z();
    for(int i=0;i<2;i++){
      phi0[i](0) += Grads[j](i)*x;
      phi0[i](1) += Grads[j+nbFF](i)*y;
      phi0[i](2) += Grads[j+nbFF+nbFF](i)*z;
      for(int jj=0;jj<2;jj++){
        derivphi0[i][jj](0) += Hess[j](i,jj)*x;
        derivphi0[i][jj](1) += Hess[j+nbFF](i,jj)*y;
        derivphi0[i][jj](2) += Hess[j+nbFF+nbFF](i,jj)*z;
      }
    }
  }
  // Compute normal and Jacobian
  crossprod(phi0[0],phi0[1],t0);
  detJ0=t0.norm();
  t0.normalize();
  // Compute dual basis vector
  double cos;
  for(int i=0;i<2;i++){
    crossprod(phi0[1-i],t0,phi0d[i]);
    cos =  dot(phi0d[i],phi0[i]);
    phi0d[i] *= (1./cos);
  }
  // Derivation of normal
  SVector3 temp;
  double t0dottemp;
  double invJ = 1./detJ0;
  for(int g=0;g<2;g++){
    crossprod(derivphi0[0][g],phi0[1],temp);
    t0dottemp = dot(temp,t0);
    derivt0[g] = temp + t0*t0dottemp;
    crossprod(derivphi0[1][g],phi0[0],temp);
    t0dottemp = dot(temp,t0);
    derivt0[g]-= temp + t0*t0dottemp;
    derivt0[g]*=invJ;
  }
  // curvature
  for(int i=0;i<2;i++)
    for(int j=0;j<2;j++)
      _lambda[i][j] = dot(derivt0[j],phi0d[i]);
}

void linearShellLocalBasisBulk::setBasisPushTensor(const linearShellLocalBasisInter *lbs)
{
  // push forward tensor T and inverse push forward tensor t
  for(int i=0;i<2;i++)
    for(int j=0;j<2;j++){
      T[i+j+j]=dot(phi0[i],lbs->dualBasisVector(j));
      t[i+j+j]=dot(phi0d[j],lbs->basisVector(i));
    }
}

void linearShellLocalBasisBulk::restart()
{
  shellLocalBasis::restart();
  for(int i=0;i<2;++i)
  {
    restartManager::restart(phi0[i]);
    restartManager::restart(phi0d[i]);
    restartManager::restart(derivt0[i]);
    for(int j=0;j<2;++j){
      restartManager::restart(derivphi0[i][j]);
      restartManager::restart(_lambda[i][j]);
    }
  }
  restartManager::restart(&T,4);
  restartManager::restart(&t,4);
  restartManager::restart(t0);
  restartManager::restart(detJ0);
  return;
}


void shellLocalBasisInter::setMinusNormal()
{
  for(int a=0;a<2;a++)
   minusNormal[a] = phi0[1][0]*phi0[a][0] + phi0[1][1]*phi0[a][1] + phi0[1][2]*phi0[a][2];
}

void shellLocalBasisInter::setphiall(const double d){
  for(int i=0;i<2;i++){
    phi0[i]=d;
    phi0d[i]=d;
  }
}

shellLocalBasisInter::shellLocalBasisInter(): shellLocalBasis()
{
  phi0.resize(2);
  phi0d.resize(2);
  this->setphiall(0.);
}

shellLocalBasisInter& shellLocalBasisInter::operator=(const shellLocalBasis &source){
  shellLocalBasis::operator=(source);
  const shellLocalBasisInter* src = static_cast<const shellLocalBasisInter*>(&source);
  phi0.resize(2);
  phi0d.resize(2);
  for(int i=0;i<2;i++){
    phi0[i] = src->phi0[i];
    phi0d[i] = src->phi0d[i];
  }
  t0= src->t0;
  detJ0 = src->detJ0;
  for(int i=0;i<2;i++){
    minusNormal[i] = src->minusNormal[i];
  }
  return *this;
}
shellLocalBasisInter::shellLocalBasisInter(const shellLocalBasisInter &source) : shellLocalBasis(source){
  phi0.resize(2);
  phi0d.resize(2);
  for(int i=0;i<2;i++){
    phi0[i] = source.phi0[i];
    phi0d[i] = source.phi0d[i];
  }
  t0 = source.t0;
  detJ0 = source.detJ0;
  for(int i=0;i<2;i++){
    minusNormal[i] = source.minusNormal[i];
  }
}

void shellLocalBasisInter::restart()
{
  shellLocalBasis::restart();
  for(int i=0;i<2;++i)
  {
    restartManager::restart(phi0[i]);
    restartManager::restart(phi0d[i]);
  }
  restartManager::restart(t0);
  restartManager::restart(detJ0);
  restartManager::restart(&minusNormal,2);
  return;
}

linearShellLocalBasisInter& linearShellLocalBasisInter::operator=(const shellLocalBasis &source){
  shellLocalBasisInter::operator=(source);
  return *this;
}

void linearShellLocalBasisInter::set(MInterfaceLine *iele, const std::vector<TensorialTraits<double>::GradType> &Grads, const SVector3 &t0p, const SVector3 &t0m){
  const int nbFF = iele->getNumVertices();
  // Compute of normal
  t0=t0p+t0m;
 #ifdef _DEBUG
  if(t0.norm() < 1e-8) Msg::Error("Two domains are oriented in oppossite direction. Please define one of the two domains in the other sens");
 #endif // DEBUG

  t0.normalize();

  // Compute local basis vector
  this->setphiall(0.);
  MVertex *ver;
  for(int j=0;j<nbFF;j++){
    ver = iele->getVertex(j);
    phi0[0](0) += Grads[j](0)*ver->x();
    phi0[0](1) += Grads[j+nbFF](0)*ver->y();
    phi0[0](2) += Grads[j+nbFF+nbFF](0)*ver->z();
  }
  phi0[1] = crossprod(t0,phi0[0]);
  phi0[1].normalize();

  // Compute normal and Jacobian
  detJ0=crossprod(phi0[0],phi0[1]).norm();

  // Compute dual basis vector
  double cos;
  for(int i=0;i<2;i++){
    phi0d[i] = crossprod(phi0[1-i],t0);
    cos =  dot(phi0d[i],phi0[i]);
    phi0d[i] *= (1./cos);
  }
  this->setMinusNormal();
}

void linearShellLocalBasisInter::set(MInterfaceLine *iele, const std::vector<TensorialTraits<double>::GradType> &Grads, const SVector3 &t0m){
  const int nbFF = iele->getNumVertices();
  // Compute of normal
  t0=t0m;
  //assert(t0.norm()>1.e-8);
  t0.normalize();

  // Compute local basis vector
  this->setphiall(0.);
  MVertex *ver;
  for(int j=0;j<nbFF;j++){
    ver = iele->getVertex(j);
    phi0[0](0) += Grads[j](0)*ver->x();
    phi0[0](1) += Grads[j+nbFF](0)*ver->y();
    phi0[0](2) += Grads[j+nbFF+nbFF](0)*ver->z();
  }
  phi0[1] = crossprod(t0,phi0[0]);
  phi0[1].normalize();

  // Compute normal and Jacobian
  detJ0=crossprod(phi0[0],phi0[1]).norm();

  // Compute dual basis vector
  double cos;
  for(int i=0;i<2;i++){
    phi0d[i] = crossprod(phi0[1-i],t0);
    cos =  dot(phi0d[i],phi0[i]);
    phi0d[i] *= (1./cos);
  }
  this->setMinusNormal();
}

void nonLinearShellLocalBasisBulk::resetAll()
{
  for(int i=0;i<2;i++){
    _phi[i] = 0.;
    _dualPhi[i]=0;
    _deriTnormal[i]=0.;
    for(int j=0;j<2;j++){
      _derivPhi[i][j]=0.;
      _lambda[i][j]=0.;
    }
    T[i] = T[i+2] = 0.;
    t[i] = t[i+2] = 0.;
  }
  _tnormal=0.;
  for(int i=0;i<_nbsimp;i++){
    _detJ[i]=0.;
    // not _detJ0 because the value is fixed by an other function ??
    _g_1[i] = 0.;
    _g_2[i] = 0.;
    _g_3[i] = 0.;
    _g1[i] = 0.;
    _g2[i] = 0.;
    _g3[i] = 0.;
  }
}
nonLinearShellLocalBasisBulk::nonLinearShellLocalBasisBulk(const short int nbsimp) : shellLocalBasisBulk(),
                                                                                       _nbsimp(nbsimp), _detJbarre(0.)
{
  _g_1.resize(nbsimp);
  _g_2.resize(nbsimp);
  _g_3.resize(nbsimp);
  _g1.resize(nbsimp);
  _g2.resize(nbsimp);
  _g3.resize(nbsimp);
  _detJ.resize(nbsimp);
  _detJ0.resize(nbsimp);
  this->resetAll();
}
nonLinearShellLocalBasisBulk:: nonLinearShellLocalBasisBulk(const nonLinearShellLocalBasisBulk &source) : shellLocalBasisBulk(source),
                                                                                                          _nbsimp(source._nbsimp),
                                                                                                          _detJbarre(source._detJbarre)
{
  for(int i=0;i<2;i++){
    _phi[i] = source._phi[i];
    _dualPhi[i]= source._dualPhi[i];
    _deriTnormal[i]= source._deriTnormal[i];
    for(int j=0;j<2;j++){
      _derivPhi[i][j] = source._derivPhi[i][j];
      _lambda[i][j] = source._lambda[i][j];
    }
    T[i] = source.T[i];
    t[i] = source.t[i];
  }
  T[2] = source.T[2];
  T[3] = source.T[3];
  t[2] = source.t[2];
  t[3] = source.t[3];
  _tnormal = source._tnormal;
  _g_1.resize(_nbsimp);
  _g_2.resize(_nbsimp);
  _g_3.resize(_nbsimp);
  _g1.resize(_nbsimp);
  _g2.resize(_nbsimp);
  _g3.resize(_nbsimp);
  _detJ.resize(_nbsimp);
  _detJ0.resize(_nbsimp);
  for(int i=0;i<_nbsimp;i++){
    _g_1[i] = source._g_1[i];
    _g_2[i] = source._g_2[i];
    _g_3[i] = source._g_3[i];
    _g1[i] = source._g1[i];
    _g2[i] = source._g2[i];
    _g3[i] = source._g3[i];
    _detJ[i] = source._detJ[i];
    _detJ0[i]=source._detJ0[i];
  }
}
nonLinearShellLocalBasisBulk&  nonLinearShellLocalBasisBulk::operator=(const shellLocalBasis &source)
{
  shellLocalBasisBulk::operator=(source);
  const nonLinearShellLocalBasisBulk *src = static_cast<const nonLinearShellLocalBasisBulk*>(&source);
  _nbsimp = src->_nbsimp;
  _detJbarre = src->_detJbarre;
  for(int i=0;i<2;i++){
    _phi[i] = src->_phi[i];
    _dualPhi[i]= src->_dualPhi[i];
    _deriTnormal[i]= src->_deriTnormal[i];
    for(int j=0;j<2;j++){
      _derivPhi[i][j] = src->_derivPhi[i][j];
      _lambda[i][j] = src->_lambda[i][j];
    }
    T[i] = src->T[i];
    t[i] = src->t[i];
  }
  T[2] = src->T[2];
  T[3] = src->T[3];
  t[2] = src->t[2];
  t[3] = src->t[3];
  _tnormal = src->_tnormal;
  _g_1.resize(_nbsimp);
  _g_2.resize(_nbsimp);
  _g_3.resize(_nbsimp);
  _g1.resize(_nbsimp);
  _g2.resize(_nbsimp);
  _g3.resize(_nbsimp);
  _detJ.resize(_nbsimp);
  _detJ0.resize(_nbsimp);
  for(int i=0;i<_nbsimp;i++){
    _g_1[i] = src->_g_1[i];
    _g_2[i] = src->_g_2[i];
    _g_3[i] = src->_g_3[i];
    _g1[i] = src->_g1[i];
    _g2[i] = src->_g2[i];
    _g3[i] = src->_g3[i];
    _detJ[i]= src->_detJ[i];
    _detJ0[i]=src->_detJ0[i];
  }
  return *this;
}
void nonLinearShellLocalBasisBulk::set(MElement *ele, const std::vector<TensorialTraits<double>::GradType> &Grads,
                                       const std::vector<TensorialTraits<double>::HessType> &Hess,
                                       const fullVector<double> &unknowns, const std::vector<double> &lambdah,
                                       const std::vector<double> &zsimp)
{
  this->resetAll();
  // varphi,alpha
  std::vector<double> elecurpos;
  currentConfig::elementPositionXYZ(ele,unknowns,elecurpos);
  int nbFF = ele->getNumShapeFunctions();
  for(int j=0;j<nbFF;j++){
    for(int i=0;i<2;i++){
      _phi[i](0) += Grads[j](i)*elecurpos[j];
      _phi[i](1) += Grads[j+nbFF](i)*elecurpos[j+nbFF];
      _phi[i](2) += Grads[j+nbFF+nbFF](i)*elecurpos[j+nbFF+nbFF];
      for(int jj=0;jj<2;jj++){
        _derivPhi[i][jj](0) += Hess[j](i,jj)*elecurpos[j];
        _derivPhi[i][jj](1) += Hess[j+nbFF](i,jj)*elecurpos[j+nbFF];
        _derivPhi[i][jj](2) += Hess[j+nbFF+nbFF](i,jj)*elecurpos[j+nbFF+nbFF];
      }
    }
  }
  // normal & Jacobian
  crossprod(_phi[0],_phi[1],_tnormal);
  double lambdahbarre = lambdah[lambdah.size()/2];
  _detJbarre=_tnormal.norm()*lambdahbarre;
  _tnormal.normalize();
  // normal derivative t,alpha
  SVector3 temp;
  double tempdott;
  double oneOnjbarre = 1./_detJbarre;
  for(int i=0;i<2;i++){
    crossprod(_derivPhi[0][i],_phi[1],temp);
    tempdott = dot(temp,_tnormal);
    _deriTnormal[i]+=(temp-tempdott*_tnormal);
    crossprod(_derivPhi[1][i],_phi[0],temp);
    tempdott = dot(temp,_tnormal);
    _deriTnormal[i]-=(temp-tempdott*_tnormal);
    _deriTnormal[i]*=oneOnjbarre;
  }

  // varphi^,alpha
  double cos;
  for(int i=0;i<2;i++){
    crossprod(_phi[1-i],_tnormal,_dualPhi[i]);
    cos =  dot(_dualPhi[i],_phi[i]);
    _dualPhi[i] *= (1./cos);
  }

  // g
  double xi3lambdah;
  for(int i=0;i<_nbsimp;i++){
    xi3lambdah = zsimp[i]*lambdah[i];
    _g_1[i] = _phi[0]+xi3lambdah*_deriTnormal[0];
    _g_2[i] = _phi[1]+xi3lambdah*_deriTnormal[1];
    _g_3[i] = lambdah[i]*_tnormal;
    this->dualGbasis(_g_1[i],_g_2[i],_g_3[i],_g1[i],_g2[i],_g3[i]);
    // jacobian (current to have J use initial configuration)
    crossprod(_g_1[i],_g_2[i],temp);
    _detJ[i] = dot(_g_3[i],temp);
  }
}

void nonLinearShellLocalBasisBulk::setBasisVectorG(const int npoint,const double zk,const double lambdah)
{
  double xi3lambdah = zk*lambdah;
  _g_1[npoint] = _phi[0]+xi3lambdah*_deriTnormal[0];
  _g_2[npoint] = _phi[1]+xi3lambdah*_deriTnormal[1];
  _g_3[npoint] = lambdah*_tnormal;
  this->dualGbasis(_g_1[npoint],_g_2[npoint],_g_3[npoint],_g1[npoint],_g2[npoint],_g3[npoint]);
  // jacobian (current to have J use initial configuration)
  _detJ[npoint] = dot(_g_3[npoint],crossprod(_g_1[npoint],_g_2[npoint]));
}

void shellLocalBasisBulk::dualGbasis(const SVector3 &g_1,const SVector3 &g_2,const SVector3 &g_3,
                                              SVector3 &g1,SVector3 &g2,SVector3 &g3) const
{
  // dual basis inverse a 3x3 matrix
  double ud = 1. / ((g_1[0] * (g_2[1] * g_3[2] - g_3[1] * g_2[2]) -
                     g_2[0] * (g_1[1] * g_3[2] - g_3[1] * g_1[2]) +
                     g_3[0] * (g_1[1] * g_2[2] - g_2[1] * g_1[2])));
  g1[0] =  (g_2[1] * g_3[2] - g_3[1] * g_2[2]) * ud;
  g2[0] = -(g_1[1] * g_3[2] - g_3[1] * g_1[2]) * ud;
  g3[0] =  (g_1[1] * g_2[2] - g_2[1] * g_1[2]) * ud;
  g1[1] = -(g_2[0] * g_3[2] - g_3[0] * g_2[2]) * ud;
  g2[1] =  (g_1[0] * g_3[2] - g_3[0] * g_1[2]) * ud;
  g3[1] = -(g_1[0] * g_2[2] - g_2[0] * g_1[2]) * ud;
  g1[2] =  (g_2[0] * g_3[1] - g_3[0] * g_2[1]) * ud;
  g2[2] = -(g_1[0] * g_3[1] - g_3[0] * g_1[1]) * ud;
  g3[2] =  (g_1[0] * g_2[1] - g_2[0] * g_1[1]) * ud;

}

void nonLinearShellLocalBasisBulk::setBasisPushTensor(const shellLocalBasisInter *lbs)
{
  // push forward tensor T and inverse push forward tensor t
  for(int i=0;i<2;i++)
    for(int j=0;j<2;j++){
      T[i+j+j]=dot(_phi[i],lbs->dualBasisVector(j));
      t[i+j+j]=dot(_dualPhi[j],lbs->basisVector(i));
    }
}

void nonLinearShellLocalBasisBulk::resetNbSimp(const short int nbsimp)
{
  _nbsimp = nbsimp;
  _g_1.resize(nbsimp);
  _g_2.resize(nbsimp);
  _g_3.resize(nbsimp);
  _g1.resize(nbsimp);
  _g2.resize(nbsimp);
  _g3.resize(nbsimp);
  _detJ.resize(nbsimp);
  _detJ0.resize(nbsimp);
  this->resetAll();
}

void nonLinearShellLocalBasisBulk::setLambda(const double lh){
  // curvature
  for(int i=0;i<2;i++)
    for(int j=0;j<2;j++)
      _lambda[i][j] = lh*dot(_deriTnormal[j],_dualPhi[i]);
}

void nonLinearShellLocalBasisBulk::setInitialJacobian(const std::vector<double> &detj0)
{
  for(int i=0;i<_nbsimp;i++)
    _detJ0[i] = detj0[i];
}

void nonLinearShellLocalBasisBulk::restart()
{
  shellLocalBasisBulk::restart();
  for(int i=0;i<2;++i)
  {
    restartManager::restart(_phi[i]);
    restartManager::restart(_dualPhi[i]);
    for(int j=0;j<2;++j){
      restartManager::restart(_derivPhi[i][j]);
      restartManager::restart(_lambda[i][j]);
    }
    restartManager::restart(_deriTnormal[i]);
  }
  restartManager::restart(_tnormal);
  restartManager::restart(_nbsimp);
  restartManager::restart(_detJbarre);
  restartManager::restart(&T,4);
  restartManager::restart(&t,4);
  restartManager::restart(_detJ);
  restartManager::restart(_detJ0);

  for(int i=0;i<_nbsimp;i++)
  {
    restartManager::restart(_g_1[i]);
    restartManager::restart(_g_2[i]);
    restartManager::restart(_g_3[i]);
    restartManager::restart(_g1[i]);
    restartManager::restart(_g2[i]);
    restartManager::restart(_g3[i]);
  }
  return;
}

nonLinearShellLocalBasisInter::nonLinearShellLocalBasisInter(const nonLinearShellLocalBasisInter &source) : shellLocalBasisInter(source)
{
  for(int i=0;i<2;i++){
    _deriTnormal[i] = source._deriTnormal[i];
    for(int j=0;j<2;j++)
      _derivPhi[i][j] = source._derivPhi[i][j];
  }
}

nonLinearShellLocalBasisInter& nonLinearShellLocalBasisInter::operator=(const shellLocalBasis &source)
{
  shellLocalBasisInter::operator=(source);
  const nonLinearShellLocalBasisInter* src = static_cast<const nonLinearShellLocalBasisInter*>(&source);
  for(int i=0;i<2;i++){
    _deriTnormal[i] = src->_deriTnormal[i];
    for(int j=0;j<2;j++)
      _derivPhi[i][j] = src->_derivPhi[i][j];
  }
  return *this;
}

void nonLinearShellLocalBasisInter::setphiall(const double d){
  shellLocalBasisInter::setphiall(d);
  for(int i=0;i<2;i++){
    _deriTnormal[i]=d;
    for(int j=0;j<2;j++){
      _derivPhi[i][j]=d;
    }
  }
}

void nonLinearShellLocalBasisInter::set(MInterfaceLine *iele, const std::vector<TensorialTraits<double>::GradType> &Grads,
                                        const fullVector<double> &unknowns,
                                        nonLinearShellLocalBasisBulk *lbp, const double lambdahm,
                                        nonLinearShellLocalBasisBulk *lbm, const double lambdahp){
  const int nbFF = iele->getNumVertices();
  // Compute of normal
  t0=lbp->basisNormal() + lbm->basisNormal();
 #ifdef _DEBUG
  if(t0.norm() < 1e-8) Msg::Error("Two domains are oriented in oppossite direction. Please define one of the two domains in the other sens");
 #endif // DEBUG
  t0.normalize();

  // get current position of element
  std::vector<double> elempos;
  currentConfig::elementPositionXYZ(iele,unknowns,elempos);

  // Compute local basis vector
  this->setphiall(0.);

  // Use same container as linear so the container as a 0 but current pos
  for(int j=0;j<nbFF;j++){
    phi0[0](0) += Grads[j](0)*elempos[j];
    phi0[0](1) += Grads[j+nbFF](0)*elempos[j+nbFF];
    phi0[0](2) += Grads[j+nbFF+nbFF](0)*elempos[j+nbFF+nbFF];
  }
  phi0[1] = crossprod(t0,phi0[0]);
  phi0[1].normalize();

  // Compute Jacobian
  double lambadhmean = 0.5*(lambdahm+lambdahp);
  detJ0=crossprod(phi0[0],phi0[1]).norm()*lambadhmean;

  // Compute dual basis vector
  double cos;
  for(int i=0;i<2;i++){
    phi0d[i] = crossprod(phi0[1-i],t0);
    cos =  dot(phi0d[i],phi0[i]);
    phi0d[i] *= (1./cos);
  }

  // rotation tensor on bulk basis
  lbm->setBasisPushTensor(this);
  lbp->setBasisPushTensor(this);

  // Derivative of normal on interaface basis
  // The component of derivative of bulk basis vector have to be rotate
//        mtMath::Matr2 invRPlus = (GP[ptg]->getContravariantRotationPlus()).getInverse();
//        invRPlus.setToTranspose();
  _derivPhi[0][0]= lbp->basisInvPushTensor(0,0)*lbp->basisDeriVector(0,0)*lbp->basisInvPushTensor(0,0)+
                   lbp->basisInvPushTensor(1,0)*lbp->basisDeriVector(1,0)*lbp->basisInvPushTensor(0,0)+
                   lbp->basisInvPushTensor(0,0)*lbp->basisDeriVector(0,1)*lbp->basisInvPushTensor(0,1)+
                   lbp->basisInvPushTensor(1,0)*lbp->basisDeriVector(1,1)*lbp->basisInvPushTensor(0,1);
  _derivPhi[1][0]= lbp->basisInvPushTensor(0,0)*lbp->basisDeriVector(0,0)*lbp->basisInvPushTensor(1,0)+
                   lbp->basisInvPushTensor(1,0)*lbp->basisDeriVector(1,0)*lbp->basisInvPushTensor(1,0)+
                   lbp->basisInvPushTensor(0,0)*lbp->basisDeriVector(0,1)*lbp->basisInvPushTensor(1,1)+
                   lbp->basisInvPushTensor(1,0)*lbp->basisDeriVector(1,1)*lbp->basisInvPushTensor(1,1);
  _derivPhi[0][1]= lbp->basisInvPushTensor(0,1)*lbp->basisDeriVector(0,0)*lbp->basisInvPushTensor(0,0)+
                   lbp->basisInvPushTensor(1,1)*lbp->basisDeriVector(1,0)*lbp->basisInvPushTensor(0,0)+
                   lbp->basisInvPushTensor(0,1)*lbp->basisDeriVector(0,1)*lbp->basisInvPushTensor(0,1)+
                   lbp->basisInvPushTensor(1,1)*lbp->basisDeriVector(1,1)*lbp->basisInvPushTensor(0,1);
  _derivPhi[1][1]= lbp->basisInvPushTensor(0,1)*lbp->basisDeriVector(0,0)*lbp->basisInvPushTensor(1,0)+
                   lbp->basisInvPushTensor(1,1)*lbp->basisDeriVector(1,0)*lbp->basisInvPushTensor(1,0)+
                   lbp->basisInvPushTensor(0,1)*lbp->basisDeriVector(0,1)*lbp->basisInvPushTensor(1,1)+
                   lbp->basisInvPushTensor(1,1)*lbp->basisDeriVector(1,1)*lbp->basisInvPushTensor(1,1);


  _derivPhi[0][0]+=lbm->basisInvPushTensor(0,0)*lbm->basisDeriVector(0,0)*lbm->basisInvPushTensor(0,0)+
                   lbm->basisInvPushTensor(1,0)*lbm->basisDeriVector(1,0)*lbm->basisInvPushTensor(0,0)+
                   lbm->basisInvPushTensor(0,0)*lbm->basisDeriVector(0,1)*lbm->basisInvPushTensor(0,1)+
                   lbm->basisInvPushTensor(1,0)*lbm->basisDeriVector(1,1)*lbm->basisInvPushTensor(0,1);
  _derivPhi[1][0]+=lbm->basisInvPushTensor(0,0)*lbm->basisDeriVector(0,0)*lbm->basisInvPushTensor(1,0)+
                   lbm->basisInvPushTensor(1,0)*lbm->basisDeriVector(1,0)*lbm->basisInvPushTensor(1,0)+
                   lbm->basisInvPushTensor(0,0)*lbm->basisDeriVector(0,1)*lbm->basisInvPushTensor(1,1)+
                   lbm->basisInvPushTensor(1,0)*lbm->basisDeriVector(1,1)*lbm->basisInvPushTensor(1,1);
  _derivPhi[0][1]+=lbm->basisInvPushTensor(0,1)*lbm->basisDeriVector(0,0)*lbm->basisInvPushTensor(0,0)+
                   lbm->basisInvPushTensor(1,1)*lbm->basisDeriVector(1,0)*lbm->basisInvPushTensor(0,0)+
                   lbm->basisInvPushTensor(0,1)*lbm->basisDeriVector(0,1)*lbm->basisInvPushTensor(0,1)+
                   lbm->basisInvPushTensor(1,1)*lbm->basisDeriVector(1,1)*lbm->basisInvPushTensor(0,1);
  _derivPhi[1][1]+=lbm->basisInvPushTensor(0,1)*lbm->basisDeriVector(0,0)*lbm->basisInvPushTensor(1,0)+
                   lbm->basisInvPushTensor(1,1)*lbm->basisDeriVector(1,0)*lbm->basisInvPushTensor(1,0)+
                   lbm->basisInvPushTensor(0,1)*lbm->basisDeriVector(0,1)*lbm->basisInvPushTensor(1,1)+
                   lbm->basisInvPushTensor(1,1)*lbm->basisDeriVector(1,1)*lbm->basisInvPushTensor(1,1);
  for(int a=0;a<2;a++)
    for(int b=0;b<2;b++)
      _derivPhi[a][b]*=0.5; // To have the mean value

  // Derivative of normal
  // t,alpha
  SVector3 temp;
  double tempdott;
  double oneOnjbarre = 1./detJ0; //
  for(int i=0;i<2;i++){
    crossprod(_derivPhi[0][i],phi0[1],temp);
    tempdott = dot(temp,t0);
    _deriTnormal[i]+=(temp-tempdott*t0);
    crossprod(_derivPhi[1][i],phi0[0],temp);
    tempdott = dot(temp,t0);
    _deriTnormal[i]-=(temp-tempdott*t0);
    _deriTnormal[i]*=oneOnjbarre;
  }

  for(int a=0;a<2;a++)
    minusNormal[a] = phi0[1][0]*phi0[a][0] + phi0[1][1]*phi0[a][1] + phi0[1][2]*phi0[a][2];
}

void nonLinearShellLocalBasisInter::set(MInterfaceLine *iele, const std::vector<TensorialTraits<double>::GradType> &Grads,
                                        const fullVector<double> &unknowns,
                                        nonLinearShellLocalBasisBulk *lbm, const double lambdahm){
  const int nbFF = iele->getNumVertices();
  // Compute of normal
  t0=lbm->basisNormal();
  //assert(t0.norm()>1.e-8);
  t0.normalize();

  // Compute local basis vector
  this->setphiall(0.);
  // get current position of element
  std::vector<double> elempos;
  currentConfig::elementPositionXYZ(iele,unknowns,elempos);

  // Use same container as linear so the container as a 0 but current pos
  for(int j=0;j<nbFF;j++){
    phi0[0](0) += Grads[j](0)*elempos[j];
    phi0[0](1) += Grads[j+nbFF](0)*elempos[j+nbFF];
    phi0[0](2) += Grads[j+nbFF+nbFF](0)*elempos[j+nbFF+nbFF];
  }
  phi0[1] = crossprod(t0,phi0[0]);
  phi0[1].normalize();

  // Compute normal and Jacobian
  detJ0=crossprod(phi0[0],phi0[1]).norm()*lambdahm;

  // Compute dual basis vector
  double cos;
  for(int i=0;i<2;i++){
    phi0d[i] = crossprod(phi0[1-i],t0);
    cos =  dot(phi0d[i],phi0[i]);
    phi0d[i] *= (1./cos);
  }
  // set rotation tensor on bulk basis
  lbm->setBasisPushTensor(this);

  // derivate of phi (validate via MetaSheel)
  _derivPhi[0][0]=lbm->basisInvPushTensor(0,0)*lbm->basisDeriVector(0,0)*lbm->basisInvPushTensor(0,0)+
                  lbm->basisInvPushTensor(1,0)*lbm->basisDeriVector(1,0)*lbm->basisInvPushTensor(0,0)+
                  lbm->basisInvPushTensor(0,0)*lbm->basisDeriVector(0,1)*lbm->basisInvPushTensor(0,1)+
                  lbm->basisInvPushTensor(1,0)*lbm->basisDeriVector(1,1)*lbm->basisInvPushTensor(0,1);
  _derivPhi[1][0]=lbm->basisInvPushTensor(0,0)*lbm->basisDeriVector(0,0)*lbm->basisInvPushTensor(1,0)+
                  lbm->basisInvPushTensor(1,0)*lbm->basisDeriVector(1,0)*lbm->basisInvPushTensor(1,0)+
                  lbm->basisInvPushTensor(0,0)*lbm->basisDeriVector(0,1)*lbm->basisInvPushTensor(1,1)+
                  lbm->basisInvPushTensor(1,0)*lbm->basisDeriVector(1,1)*lbm->basisInvPushTensor(1,1);
  _derivPhi[0][1]=lbm->basisInvPushTensor(0,1)*lbm->basisDeriVector(0,0)*lbm->basisInvPushTensor(0,0)+
                  lbm->basisInvPushTensor(1,1)*lbm->basisDeriVector(1,0)*lbm->basisInvPushTensor(0,0)+
                  lbm->basisInvPushTensor(0,1)*lbm->basisDeriVector(0,1)*lbm->basisInvPushTensor(0,1)+
                  lbm->basisInvPushTensor(1,1)*lbm->basisDeriVector(1,1)*lbm->basisInvPushTensor(0,1);
  _derivPhi[1][1]=lbm->basisInvPushTensor(0,1)*lbm->basisDeriVector(0,0)*lbm->basisInvPushTensor(1,0)+
                  lbm->basisInvPushTensor(1,1)*lbm->basisDeriVector(1,0)*lbm->basisInvPushTensor(1,0)+
                  lbm->basisInvPushTensor(0,1)*lbm->basisDeriVector(0,1)*lbm->basisInvPushTensor(1,1)+
                  lbm->basisInvPushTensor(1,1)*lbm->basisDeriVector(1,1)*lbm->basisInvPushTensor(1,1);
  // derivative of normal
  // t,alpha
  SVector3 temp;
  double tempdott;
  double oneOnjbarre = 1./detJ0;
  for(int i=0;i<2;i++){
    crossprod(_derivPhi[0][i],phi0[1],temp);
    tempdott = dot(temp,t0);
    _deriTnormal[i]+=(temp-tempdott*t0);
    crossprod(_derivPhi[1][i],phi0[0],temp);
    tempdott = dot(temp,t0);
    _deriTnormal[i]-=(temp-tempdott*t0);
    _deriTnormal[i]*=oneOnjbarre;
  }

  for(int a=0;a<2;a++)
    minusNormal[a] = phi0[1][0]*phi0[a][0] + phi0[1][1]*phi0[a][1] + phi0[1][2]*phi0[a][2];
}

void nonLinearShellLocalBasisInter::restart()
{
  shellLocalBasisInter::restart();
  for(int i=0;i<2;++i)
  {
    for(int j=0;j<2;++j)
      restartManager::restart(_derivPhi[i][j]);
    restartManager::restart(_deriTnormal[i]);
  }
  return;
}
