//
// C++ Interface: terms
//
// Description: Function needed to compute element of reduction n^alpha and m^alpha for thin bodies
//
//
// Author:  <Gauthier BECKER>, (C) 2010
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "reduction.h"
#include <math.h>
reductionElement::reductionElement(const stressTensor &stress, const shellLocalBasis *lb){
  for(int m=0;m<2;m++)
    for(int n=0;n<2;n++){
      mat[m][n] =0.;
      for(int i=0;i<3;i++)
        for(int j=0;j<3;j++)
        mat[m][n]+= stress(i,j)*lb->dualBasisVector(m,i)*lb->dualBasisVector(n,j);
    }
}
reductionElement::reductionElement(const strainTensor &strain, const shellLocalBasis *lb){
  for(int m=0;m<2;m++)
    for(int n=0;n<2;n++){
      mat[m][n] =0.;
      for(int i=0;i<3;i++)
        for(int j=0;j<3;j++)
        mat[m][n]+= strain(i,j)*lb->basisVector(m,i)*lb->basisVector(n,j);
    }
}

void reductionElement::init(const stressTensor &stress, const shellLocalBasis *lb){
  for(int m=0;m<2;m++)
    for(int n=0;n<2;n++){
      mat[m][n] =0.;
      for(int i=0;i<3;i++)
        for(int j=0;j<3;j++)
        mat[m][n]+= stress(i,j)*lb->dualBasisVector(m,i)*lb->dualBasisVector(n,j);
    }
}

void reductionElement::hat(const shellLocalBasisBulk *lb, reductionElement &nhat) const{
  nhat.setAll(0.);
  for(int alpha=0;alpha<2;alpha++)
    for(int beta=0;beta<2;beta++){
      for(int gamma=0;gamma<2;gamma++)
        for(int delta=0;delta<2;delta++)
          nhat(alpha,beta)+=lb->basisPushTensor(gamma,alpha)*mat[gamma][delta]*lb->basisPushTensor(delta,beta);
    }
}
void displacementjump(const std::vector<TensorialTraits<double>::ValType> &Val_m,const int n_m,
                      const std::vector<TensorialTraits<double>::ValType> &Val_p,const int n_p,
                      const fullVector<double> &disp,fullVector<double> &ujump){
  ujump.scale(0.);
  for(int i=0;i<3;i++){
    for(int j=0;j<n_m;j++)
      ujump(i)-=Val_m[j]*disp(j+i*n_m);
    for(int j=0;j<n_p;j++)
      ujump(i)+=Val_p[j]*disp(j+i*n_p+3*n_m);
  }
}

void displacementjump(const std::vector<TensorialTraits<double>::ValType> &Val_m,const int n_m,
                      const std::vector<TensorialTraits<double>::ValType> &Val_p,
                      const int n_p,const fullVector<double> &dispm,
                      const fullVector<double> &dispp,SVector3 &ujump){
  for(int i=0;i<3;i++){
    ujump(i) =0.;
    for(int j=0;j<n_m;j++)
      ujump(i)-=Val_m[j]*dispm(j+i*n_m);
    for(int j=0;j<n_p;j++)
      ujump(i)+=Val_p[j]*dispp(j+i*n_p);
  }
}
