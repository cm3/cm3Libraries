//
// C++ Interface:
//
// Description: Allow to manage the computation of normal variation in DG shell
//
//
// Author:  <Gauthier BECKER>, (C) 2010
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef DGC0SHELLNORMALVARIATION_H_
#define DGC0SHELLNORMALVARIATION_H_
#include "SVector3.h"
#include "fullMatrix.h"
#include "shellLocalBasis.h"
static inline void crossprod(const SVector3 &a, const SVector3 &b, fullVector<double> &c){
 c(0) = a.y() * b.z() - b.y() * a.z();
 c(1) = -(a.x() * b.z() - b.x() * a.z());
 c(2) = a.x() * b.y() - b.x() * a.y();
}

static inline void tensprodSubstract(const SVector3 &a, const fullVector<double> &b, fullMatrix<double> &c)
{
    for (int i = 0; i < 3; i++)
      for (int j = 0; j < 3; j++)
        c(i,j)-=a(i)*b(j);
}

static inline void diaprod(const fullMatrix<double> &a, const fullMatrix<double> &b, fullMatrix<double> &mbt){
  for(int i=0;i<3;i++)
    for(int j=0;j<3;j++)
      mbt(i,j)=a(0,i)*b(0,j);
}

class normalVariation{
 protected:
  std::vector< fullMatrix<double> > _Deltat_tilde_minus;
  std::vector< fullMatrix<double> > _Deltat_tilde_plus;
  int _nbFFm,_nbFFp,_nbdofm;
  bool _inter; // true on interface use minus and plus. Use only minus if false
 private:
  // data that is used in set function (Allocation 1 time)
  fullVector<double> v1,v2;
  fullMatrix<double> m1,m2;
  fullMatrix<double> mv1,mv2;
  double invJ0;
  fullVector<double> _dispOneDof;
  SVector3 v31;

  void dttilde(const shellLocalBasis *lb, const std::vector<TensorialTraits<double>::GradType> &Grads, bool minus)
  {
    crossprod(lb->basisNormal(),lb->basisVector(0),v1);
    crossprod(lb->basisNormal(),lb->basisVector(1),v2);

    m1(0,0) =      0.           ;     m1(0,1) = - lb->basisVector(0,2)  ;  m1(0,2) =  lb->basisVector(0,1);
    m1(1,0) = lb->basisVector(0,2)  ;    m1(1,1) = 0.                  ;   m1(1,2) = -lb->basisVector(0,0);
    m1(2,0) = -lb->basisVector(0,1) ;    m1(2,1) = lb->basisVector(0,0)    ;  m1(2,2) = 0.               ;

    m2(0,0) =      0.           ;     m2(0,1) = - lb->basisVector(1,2)  ;  m2(0,2) = lb->basisVector(1,1);
    m2(1,0) = lb->basisVector(1,2)  ;    m2(1,1) = 0.                  ;   m2(1,2) = -lb->basisVector(1,0);
    m2(2,0) = -lb->basisVector(1,1) ;    m2(2,1) = lb->basisVector(1,0)    ;  m2(2,2) = 0.;


    tensprodSubstract(lb->basisNormal(),v1,m1);
    tensprodSubstract(lb->basisNormal(),v2,m2);
    invJ0 = 1./lb->getJacobian();
    m1.scale(invJ0);
    m2.scale(invJ0);

    if(minus){
      for(int i=0;i<_nbFFm;i++){
        _Deltat_tilde_minus[i].scale(0);
        _Deltat_tilde_minus[i].axpy(m1,Grads[i][1]);
        _Deltat_tilde_minus[i].axpy(m2,-Grads[i][0]);
      }
    }
    else{
      for(int i=0;i<_nbFFp;i++){
        _Deltat_tilde_plus[i].scale(0);
        _Deltat_tilde_plus[i].axpy(m1,Grads[i][1]);
        _Deltat_tilde_plus[i].axpy(m2,-Grads[i][0]);
      }
    }
  }
 public:
  // on element (non linear case) only minus is used. Plus is resize to 0
  normalVariation(const bool inter_=true) :v1(3), v2(3), m1(3,3), m2(3,3), v31(0.,0.,0.), mv1(1,3), mv2(1,3),
                     invJ0(0.), _nbFFm(0), _nbFFp(0), _nbdofm(0), _dispOneDof(3), _inter(inter_){
    // max 256 shape functions
    fullMatrix<double> m(3,3);
    m.scale(0.);
    for(int i=0;i<256;i++){
      _Deltat_tilde_minus.push_back(m);
    }
    if(_inter){
      for(int i=0;i<256;i++)
        _Deltat_tilde_plus.push_back(m);
    }
    else
      _Deltat_tilde_plus.resize(0);
  }
  ~normalVariation(){}
  // set the normal component
  void setDeltatTilde(const shellLocalBasis *lbm, const std::vector<TensorialTraits<double>::GradType> &Grads_m,
                      const int nbFF_m,const shellLocalBasis *lbp,
                      const std::vector<TensorialTraits<double>::GradType> &Grads_p, const int nbFF_p){
    _nbFFm = nbFF_m;
    _nbFFp = nbFF_p;
    _nbdofm = 3*_nbFFm;
    this->dttilde(lbm,Grads_m,true);
#ifdef _DEBUG
    if(!_inter)
      Msg::Error("Try Compute normal variation on plus and minus side for an element. It can be done only for interface element!");
    else
#endif
    this->dttilde(lbp,Grads_p,false);
  }
  void setDeltatTilde(const shellLocalBasis *lbm, const std::vector<TensorialTraits<double>::GradType> &Grads_m,
                      const int nbFF_m){
    _nbFFm = nbFF_m;
    _nbFFp = 0;
    _nbdofm = 3*_nbFFm;
    this->dttilde(lbm,Grads_m,true);
  }
  void bound(const shellLocalBasisInter *lbs){
    // only minus on boundary
    v31 = lbs->basisVector(0);
    v31.normalize();
    mv1(0,0) = v31(0); mv1(0,1) = v31(1); mv1(0,2) = v31(2);
    for(int i=0;i<_nbFFm;i++){
      mv1.mult(_Deltat_tilde_minus[i],mv2); 
      diaprod(mv1,mv2,m1);
      m1.scale(-1);
      _Deltat_tilde_minus[i].add(m1);
    }
  }
  // on a element return _Delta_tilde_minus
  inline const fullMatrix<double>& deltatTilde(const int i) const {return _Deltat_tilde_minus[i];}
  inline const std::vector<fullMatrix<double> >& deltatTilde() const {return _Deltat_tilde_minus;}
  inline const std::vector<fullMatrix<double> >& deltatTildeMinus() const {return _Deltat_tilde_minus;}
  inline const std::vector<fullMatrix<double> >& deltatTildePlus()  {return _Deltat_tilde_plus;}
  inline fullMatrix<double>& deltatTilde(const int i) {return _Deltat_tilde_minus[i];}
  inline fullMatrix<double>& deltatTildeMinus(const int i) {return _Deltat_tilde_minus[i];}
  inline fullMatrix<double>& deltatTildePlus(const int i)  {return _Deltat_tilde_plus[i];}
  inline double deltatTilde(const int i, const int j, const int k) const{return _Deltat_tilde_minus[i](j,k);}
  inline double deltatTildeMinus(const int i, const int j, const int k) const{return _Deltat_tilde_minus[i](j,k);}
  inline double deltatTildePlus(const int i, const int j, const int k) const{return _Deltat_tilde_plus[i](j,k);}
  inline void scaleminus(const double dd){
    for(int i=0;i<_nbFFm;i++)
      _Deltat_tilde_minus[i].scale(dd);
  }
  void jump(const std::vector<double> &disp, fullVector<double> &rjump){
    rjump.scale(0.);
    for(int j=0;j<_nbFFm;j++){
        _dispOneDof(0) = disp[j]; _dispOneDof(1) = disp[j+_nbFFm]; _dispOneDof(2) = disp[j+_nbFFm+_nbFFm];
        _Deltat_tilde_minus[j].multAddy(_dispOneDof,rjump);
    }
    rjump.scale(-1); // because jump = + - -
    for(int j=0;j<_nbFFp;j++){
      _dispOneDof(0) = disp[j+_nbdofm]; _dispOneDof(1) = disp[j+_nbFFp+_nbdofm]; _dispOneDof(2) = disp[j+_nbFFp+_nbFFp+_nbdofm];
      _Deltat_tilde_plus[j].multAddy(_dispOneDof,rjump);
    }
  }
  void jump(const fullVector<double> &disp, fullVector<double> &rjump){
    rjump.scale(0.);
    for(int j=0;j<_nbFFm;j++){
        _dispOneDof(0) = disp(j); _dispOneDof(1) = disp(j+_nbFFm); _dispOneDof(2) = disp(j+_nbFFm+_nbFFm);
        _Deltat_tilde_minus[j].multAddy(_dispOneDof,rjump);
    }
    rjump.scale(-1); // because jump = + - -
    for(int j=0;j<_nbFFp;j++){
      _dispOneDof(0) = disp(j+_nbdofm); _dispOneDof(1) = disp(j+_nbFFp+_nbdofm); _dispOneDof(2) = disp(j+_nbFFp+_nbFFp+_nbdofm);
      _Deltat_tilde_plus[j].multAddy(_dispOneDof,rjump);
    }
  }


  void jump(const fullVector<double> &dispm, const fullVector<double> &dispp, fullVector<double> &rjump){
    rjump.scale(0.);
    for(int j=0;j<_nbFFm;j++){
        _dispOneDof(0) = dispm(j); _dispOneDof(1) = dispm(j+_nbFFm); _dispOneDof(2) = dispm(j+_nbFFm+_nbFFm);
        _Deltat_tilde_minus[j].multAddy(_dispOneDof,rjump);
    }
    rjump.scale(-1); // because jump = + - -
    for(int j=0;j<_nbFFp;j++){
      _dispOneDof(0) = dispp(j); _dispOneDof(1) = dispp(j+_nbFFp); _dispOneDof(2) = dispp(j+_nbFFp+_nbFFp);
      _Deltat_tilde_plus[j].multAddy(_dispOneDof,rjump);
    }
  }

  void jump(const std::vector<double> &disp,double *rjump){
    this->jump(disp,v1);
    for(int i=0;i<3;i++)
      rjump[i] = v1(i);
  }
  void jump(const fullVector<double> &disp,double *rjump){
    this->jump(disp,v1);
    for(int i=0;i<3;i++)
      rjump[i] = v1(i);
  }
  void jump(const fullVector<double> &dispm, const fullVector<double> &dispp,double *rjump){
    this->jump(dispm,dispp,v1);
    for(int i=0;i<3;i++)
      rjump[i] = v1(i);
  }
};

// old computation of Deltat tilde (scholar)
/*
static void compute_Deltat_tilde(const shellLocalBasis *lb, const std::vector<TensorialTraits<double>::GradType> &Grads, const int &n, double Deltat[256][3][3]){
  SVector3 vect(0.,0.,0.),vect2(0.,0.,0.);
  vect = crossprod(lb->basisNormal(),lb->basisVector(0));
  vect2= crossprod(lb->basisNormal(),lb->basisVector(1));
  double invJ0 = 1./lb->getJacobian();
  STensor3 mat1;
  STensor3 mat2;
  tensprod(lb->basisNormal(),vect,mat1);
  tensprod(lb->basisNormal(),vect2,mat2);

  double scr1[3][3];
  double scr2[3][3];
  scr1[0][0] =      0.;           scr1[0][1] = - lb->basisVector(0,2);  scr1[0][2] = lb->basisVector(0,1);
  scr1[1][0] = lb->basisVector(0,2);  scr1[1][1] = 0.  ;                scr1[1][2] = -lb->basisVector(0,0);
  scr1[2][0] = -lb->basisVector(0,1); scr1[2][1] = lb->basisVector(0,0);    scr1[2][2] = 0.;

  scr2[0][0] =      0.;           scr2[0][1] = - lb->basisVector(1,2);  scr2[0][2] = lb->basisVector(1,1);
  scr2[1][0] = lb->basisVector(1,2);  scr2[1][1] = 0.  ;                scr2[1][2] = -lb->basisVector(1,0);
  scr2[2][0] = -lb->basisVector(1,1); scr2[2][1] = lb->basisVector(1,0);    scr2[2][2] = 0.;
  for(int i=0;i<n;i++){
    for(int j=0;j<3;j++)
      for(int k=0;k<3;k++)
        Deltat[i][j][k] = invJ0*(Grads[i](1)*(scr1[j][k]-mat1(j,k))-Grads[i](0)*(scr2[j][k]-mat2(j,k)));
  }
}



*/


#endif // DGC0SHELLNORMALVARIATION
