//
// C++ Interface: terms
//
// Description: Class of terms for dg non linear shell
//
//
// Author:  <Gauthier BECKER>, (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "dgNonLinearShellTerms.h"
#include "dgNonLinearShellElementaryTerms.h"
// have to be rewriten with matrix vector product format
void dgNonLinearShellForceBulk::get(MElement *ele,int npts,IntPt *GP,fullVector<double> &m) const
{
  int nbFF = ele->getNumVertices();
  int nbdof = DgC0LinearTerm<double>::space1.getNumKeys(ele);
  m.resize(nbdof);
  m.scale(0.);
  // get IPVariable of element
  const AllIPState::ipstateElementContainer* vips = _ipf->getAips()->getIPstate(ele->getNum());
  // Shape functions values at Gauss points
  nlsFunctionSpace<double>* sp1 = static_cast<nlsFunctionSpace<double>*>(&(this->space1));
  std::vector<GaussPointSpaceValues<double>*> vgps;
  sp1->get(ele,npts,GP,vgps);
  for (int i = 0; i < npts; i++)
  {
    // Coordonate of Gauss' point i
    //const double u = GP[i].pt[0]; const double v = GP[i].pt[1]; const double w = GP[i].pt[2];
    // Weight of Gauss' point i and Jacobian's value at this point
    const double weight = GP[i].weight;

     // LocalBasis
     const IPStateBase *ips = (*vips)[i];
     const IPVariableShell *ipv = static_cast<const IPVariableShell*>(ips->getState(IPStateBase::current));
     const nonLinearShellLocalBasisBulk *lb = static_cast<const nonLinearShellLocalBasisBulk*>(ipv->getshellLocalBasis());
     const double lambdah = ipv->getGlobalStretch();
    // value of reduction element
    _mlaw->reduction(ips,vn_,vm_,true);
    // Compute of Hessian of SF. Each component of the vector in link to a shape function.
    //Grads.resize(0); Hess.resize(0);
    //DgC0LinearTerm<double>::space1.gradf(ele,u, v, w, Grads); // a optimiser
    //DgC0LinearTerm<double>::space1.hessf(ele,u, v, w, Hess);
    std::vector<TensorialTraits<double>::GradType> &Grads = vgps[i]->_vgrads;
    std::vector<TensorialTraits<double>::HessType> &Hess  = vgps[i]->_vhess;
    // Compute delta normal
    nvar.setDeltatTilde(lb,Grads,nbFF);
    // Compute delta normal,alpha
    fillVirtualDeltaConvectedDNormal(nbFF,lb,Grads,Hess,nvar.deltatTilde(),dnvar1,dnvar2);

    double wJ = lb->getJacobian() * weight;
    for(int alpha=0;alpha<2;alpha++){
      vn_[alpha]*=wJ;
      vm_[alpha]*=wJ;
    }
    vn_[2]*=wJ;
    for(int j=0; j<nbFF;j++){
      matTvectprod(dnvar1[j],vm_[0],dDeltaDt1Tmtilde1);
      matTvectprod(dnvar2[j],vm_[1],dDeltaDt2Tmtilde2);
      matTvectprod(nvar.deltatTilde(j),vn_[2],dDeltatTn3);
      for(int k=0;k<3;k++)
          m(j+k*nbFF)+=Grads[j](0)*vn_[0][k]+Grads[j](1)*vn_[1][k]+lambdah*(dDeltaDt1Tmtilde1[k]+dDeltaDt2Tmtilde2[k]+dDeltatTn3[k]);
    }
  }
 #ifdef _DEBUG
  for(int j=0;j<nbdof; j++)
    if(std::isnan(m(j)))
      Msg::Error("nan value bulk force");
 #endif // _DEBUG
}

void dgNonLinearShellForceInter::get(MElement *ele, int npts, IntPt *GP, fullVector<double> &m)const
{
  const fullVector<double> &datafield = *_data;
  MInterfaceLine *ieline = dynamic_cast<MInterfaceLine*>(ele);
  const int nbFFm = ieline->getElem(0)->getNumVertices();
  const int nbFFp = ieline->getElem(1)->getNumVertices();
  const int nbdofm = _minusSpace->getNumKeys(ieline->getElem(0));
  const int nbdofp = _plusSpace->getNumKeys(ieline->getElem(1));
  m.resize(nbdofm+nbdofp);
  m.scale(0.);
  // characteristic size
  const double hs = ieline->getCharacteristicSize();
  const double b1hs = _beta1/hs;
  const double b2hs = _beta2/hs;
  const double b3hs = _beta3/hs;

  // get value at gauss's point
  AllIPState::ipstateElementContainer *vips = _ipf->getAips()->getIPstate(ieline->getNum());
  // Get the values of Gauss points on minus and plus elements
  IntPt *GPm; IntPt *GPp;
  _interQuad->getIntPoints(ieline,GP,&GPm,&GPp);
  // Shape functions values at Gauss points
  std::vector<GaussPointSpaceValues<double>*> vgpsm;
  _minusSpace->get(ieline->getElem(0),npts,GPm,vgpsm);
  std::vector<GaussPointSpaceValues<double>*> vgpsp;
  _plusSpace->get(ieline->getElem(1),npts,GPp,vgpsp);
  // loop on Gauss Point
  for(int i=0;i<npts; i++){
    // Coordonate of Gauss' point i
    //const double u = GP[i].pt[0]; const double v = GP[i].pt[1]; const double w = GP[i].pt[2];
    // Weight of Gauss' point i and Jacobian's value at this point
    const double weight = GP[i].weight;

     // LocalBasis
     const IPStateBase *ipsm = (*vips)[i];
     const IPVariableShell *ipvm = static_cast<const IPVariableShell*>(ipsm->getState(IPStateBase::current));
     const IPVariableShell *ipvmprev = static_cast<const IPVariableShell*>(ipsm->getState(IPStateBase::previous));
     const IPVariableShell* ipvm0= static_cast<const IPVariableShell*>(ipsm->getState(IPStateBase::initial));
     const nonLinearShellLocalBasisBulk *lbm = static_cast<const nonLinearShellLocalBasisBulk*>(ipvm->getshellLocalBasis());
     const nonLinearShellLocalBasisBulk *lbm0 = static_cast<const nonLinearShellLocalBasisBulk*>(ipvm0->getshellLocalBasis());
     const double lambdahm = ipvm->getGlobalStretch();
     const IPStateBase *ipsp = (*vips)[i+npts];
     const IPVariableShell *ipvp = static_cast<const IPVariableShell*>(ipsp->getState(IPStateBase::current));
     const IPVariableShell *ipvp0 = static_cast<const IPVariableShell*>(ipsp->getState(IPStateBase::initial));
     const nonLinearShellLocalBasisBulk *lbp = static_cast<const nonLinearShellLocalBasisBulk*>(ipvp->getshellLocalBasis());
     const nonLinearShellLocalBasisBulk *lbp0 = static_cast<const nonLinearShellLocalBasisBulk*>(ipvp0->getshellLocalBasis());
     const double lambdahp = ipvp->getGlobalStretch();
     // interface (same on minus and plus element take the minus one)
     const nonLinearShellLocalBasisInter* lbs = static_cast<const nonLinearShellLocalBasisInter*>(ipvm->getshellLocalBasisOfInterface());
     const nonLinearShellLocalBasisInter* lbs0= static_cast<const nonLinearShellLocalBasisInter*>(ipvm0->getshellLocalBasisOfInterface());
     const double jbarre = lbs->getJacobian();
     const double jbarre0=lbs0->getJacobian();
    // value of reduction element
    _mlawMinus->reduction(ipsm,vnm_,vmm_,_fullDg); // no need of nalpha for Cg/Dg
    _mlawPlus->reduction(ipsp,vnp_,vmp_,_fullDg);  // no need of nalpha for Cg/Dg
    // Compute of Hessian of SF. Each component of the vector is link to a shape function.
//    Gradsm.resize(0); Hessm.resize(0); Gradsp.resize(0); Hessp.resize(0);
    //double uem = GPm[i].pt[0]; double vem = GPm[i].pt[1];
    //double uep = GPp[i].pt[0]; double vep = GPp[i].pt[1];
//    ieline->getuvwOnElem(u,v,uem,vem,wem,uep,vep,wep);
//    _minusSpace->gradf(ieline->getElem(0),uem, vem, w, Gradsm);
//    _minusSpace->hessf(ieline->getElem(0),uem, vem, w, Hessm);
//    _plusSpace->gradf(ieline->getElem(1),uep, vep, w, Gradsp); // a optimiser
//    _plusSpace->hessf(ieline->getElem(1),uep, vep, w, Hessp);

    std::vector<TensorialTraits<double>::GradType> &Gradsm = vgpsm[i]->_vgrads;
    std::vector<TensorialTraits<double>::HessType> &Hessm  = vgpsm[i]->_vhess;
    std::vector<TensorialTraits<double>::GradType> &Gradsp = vgpsp[i]->_vgrads;
    std::vector<TensorialTraits<double>::HessType> &Hessp  = vgpsp[i]->_vhess;
    // Compute delta normal
    nvar.setDeltatTilde(lbm,Gradsm,nbFFm,lbp,Gradsp,nbFFp);
    // Compute delta normal,alpha
    fillVirtualDeltaConvectedDNormal(nbFFm,lbm,Gradsm,Hessm,nvar.deltatTildeMinus(),dnvar1m,dnvar2m);
    fillVirtualDeltaConvectedDNormal(nbFFp,lbp,Gradsp,Hessp,nvar.deltatTildePlus(),dnvar1p,dnvar2p);
    rotateVirtualDeltaConvectedDNormal(nbFFm,lbm,dnvar1m,dnvar2m,dnvar1hatm,dnvar2hatm);
    rotateVirtualDeltaConvectedDNormal(nbFFp,lbp,dnvar1p,dnvar2p,dnvar1hatp,dnvar2hatp);
    // Rotation of Derivative of Shape Function (to have virtual phi_,alpha)
    fillAndRotateVirtualConvectedBase(nbFFm,lbm,Gradsm,Gradshatm);
    fillAndRotateVirtualConvectedBase(nbFFp,lbp,Gradsp,Gradshatp);
    // possibility of fracture
    bool broken = false;
//    bool prevbroken = false;
    if(_mlawMinus->getType() == materialLaw::fracture ){
      const IPShellFractureCohesive *ipvwf = static_cast<const IPShellFractureCohesive*>(ipvm); // broken via minus (OK broken on both sides)
      const IPShellFractureCohesive *ipvwfprev = static_cast<const IPShellFractureCohesive*>(ipvmprev);
      broken = ipvwfprev->isbroken(); // use continuity principle at fracture initialization to avoid instability
//      prevbroken = false; //ipvwfprev->broken();
    }
    const double wJ = jbarre* weight;
    double wJnu[2];
    for(int alpha=0;alpha<2;alpha++)
        wJnu[alpha]=wJ*lbs->getMinusNormal(alpha);

    // consistency term
    // DeltatT^j \mean{\tilde{m}^a lambda} \nu_a
    mtildelamdamean = 0.5*((vmp_[0]*lambdahp+vmm_[0]*lambdahm)*wJnu[0]
                          +(vmp_[1]*lambdahp+vmm_[1]*lambdahm)*wJnu[1]);
    // cache data
    double halfwj0barrenu[2];
    double jphidotphi[2];
    double halfwjbarrenu[2];
    if(!broken){ // if broken only consistency terms (that are the cohesive terms in this case)
      // stability term
      // normal jump
      jDt = (lbp->basisNormal()-lbp0->basisNormal())-(lbm->basisNormal()-lbm0->basisNormal());
      for(int j=0;j<3;j++)
        jDtvector(j) = jDt[j]; // Other format for compatibility change this
      // Tangent moduli tensor. For stability it's constant and the same than for the linear case
      _mlawMinus->shellBendingStiffness(ipvm0,Hm);
      _mlawPlus->shellBendingStiffness(ipvp0,Hp);
      if(_fullDg){
        Hlambdam.lambdahat(Hm,lbm);
        Hlambdap.lambdahat(Hp,lbp);
      }
      // Hat operation on bending stiffness
      Hm.hat(lbm);
      Hp.hat(lbp);
      Hmean.mean(&Hm,&Hp);

      // multiplicative factor
      for(int alpha=0;alpha<2;alpha++)
        halfwj0barrenu[alpha] = 0.5*weight*jbarre0*lbs->getMinusNormal(alpha);

      double stabBendingfactor[2];
      for(int c=0;c<2;c++){
        stabBendingfactor[c]=0.;
        for(int a=0;a<2;a++)
          for(int b=0;b<2;b++)
            for(int d=0;d<2;d++)
              stabBendingfactor[c]+=2*halfwj0barrenu[a]*Hmean(a,b,c,d)*dot(jDt,lbs->basisVector(b))*lbs->getMinusNormal(d);
      }
      for(int c=0;c<2;c++){
        stabBendingfactor[c]*=2*b1hs; // WHY *2 in MetaShells
      }
      // ADD TO CONSISTENCY
      mtildelamdamean += stabBendingfactor[0]*lbs->basisVector(0);
      mtildelamdamean += stabBendingfactor[1]*lbs->basisVector(1);
    }
    // loop on shape function
    for(int j=0;j<nbFFm;j++){
      matTvectprod(nvar.deltatTildeMinus(j),mtildelamdamean,DtTmtildelamdamean);
      for(int k=0;k<3;k++)
        m(j+k*nbFFm)-=DtTmtildelamdamean[k]; // minus because it is the jump on minus element
    }
    for(int j=0;j<nbFFp;j++){
      matTvectprod(nvar.deltatTildePlus(j),mtildelamdamean,DtTmtildelamdamean);
      for(int k=0;k<3;k++)
        m(j+nbdofm+k*nbFFp)+=DtTmtildelamdamean[k];
    }
    if(!broken){
      // Compatibility
      // The compatibility term is composed of 3 terms (2 mat3x3 that are multiplied
      // by jDt and a sclara that multiply jDt). These terms depend of gradient of shape function that have 2 components
      // So compute the undependent part. There are 2 by terms.

      // set to 0
      comp11minus.scale(0.);
      comp12minus.scale(0.);
      comp11plus.scale(0.);
      comp12plus.scale(0.);
      comp21minus.scale(0.);
      comp22minus.scale(0.);
      comp21plus.scale(0.);
      comp22plus.scale(0.);
      // Two first terms
      for(int beta=0;beta<2;beta++){
        for(int delta=0;delta<2;delta++){
          tensprod(lbs->basisVector(beta),lbs->basisDnormal(delta),tensprodtmp);
          comp11plus.axpy(tensprodtmp,halfwj0barrenu[0]*Hp(0,beta,0,delta)+halfwj0barrenu[1]*Hp(1,beta,0,delta));
          comp11minus.axpy(tensprodtmp,halfwj0barrenu[0]*Hm(0,beta,0,delta)+halfwj0barrenu[1]*Hm(1,beta,0,delta));
          comp12plus.axpy(tensprodtmp,halfwj0barrenu[0]*Hp(0,beta,1,delta)+halfwj0barrenu[1]*Hp(1,beta,1,delta));
          comp12minus.axpy(tensprodtmp,halfwj0barrenu[0]*Hm(0,beta,1,delta)+halfwj0barrenu[1]*Hm(1,beta,1,delta));
          tensprod(lbs->basisVector(beta),lbs->basisVector(delta),tensprodtmp);
          comp21plus.axpy(tensprodtmp,halfwj0barrenu[0]*Hp(0,beta,delta,0)+halfwj0barrenu[1]*Hp(1,beta,delta,0));
          comp21minus.axpy(tensprodtmp,halfwj0barrenu[0]*Hm(0,beta,delta,0)+halfwj0barrenu[1]*Hm(1,beta,delta,0));
          comp22plus.axpy(tensprodtmp,halfwj0barrenu[0]*Hp(0,beta,delta,1)+halfwj0barrenu[1]*Hp(1,beta,delta,1));
          comp22minus.axpy(tensprodtmp,halfwj0barrenu[0]*Hm(0,beta,delta,1)+halfwj0barrenu[1]*Hm(1,beta,delta,1));
        }
      }
      // last one
      for(int a=0;a<2;a++)
        halfwjbarrenu[a]= 0.5*weight*jbarre*lbs->getMinusNormal(a);
      SVector3 wjnuama = halfwjbarrenu[0]*vmp_[0]+halfwjbarrenu[1]*vmp_[1];
      double comp31plus = lambdahp*dot(lbs->dualBasisVector(0),wjnuama);
      double comp32plus = lambdahp*dot(lbs->dualBasisVector(1),wjnuama);
      wjnuama = halfwjbarrenu[0]*vmm_[0]+halfwjbarrenu[1]*vmm_[1];
      double comp31minus= lambdahm*dot(lbs->dualBasisVector(0),wjnuama);
      double comp32minus= lambdahm*dot(lbs->dualBasisVector(1),wjnuama);

      // loop on shape functions
      for(int j=0;j<nbFFm;j++){
        comptmpj.scale(0.);
        comptmpj.axpy(comp11minus,Gradshatm[j](0));
        comptmpj.axpy(comp12minus,Gradshatm[j](1));
        comptmpj.gemm(comp21minus,dnvar1hatm[j],1,1);
        comptmpj.gemm(comp22minus,dnvar2hatm[j],1,1);
        comptmpj.multWithATranspose(jDtvector,1,0,compAssVector);
        for(int k=0;k<3;k++)
          m(j+k*nbFFm) += compAssVector(k) + (comp31minus*Gradshatm[j](0)+comp32minus*Gradshatm[j](1))*jDt[k];
      }
      for(int j=0;j<nbFFp;j++){
        comptmpj.scale(0.);
        comptmpj.axpy(comp11plus,Gradshatp[j](0));
        comptmpj.axpy(comp12plus,Gradshatp[j](1));
        comptmpj.gemm(comp21plus,dnvar1hatp[j],1,1);
        comptmpj.gemm(comp22plus,dnvar2hatp[j],1,1);
        comptmpj.multWithATranspose(jDtvector,1,0,compAssVector);
        for(int k=0;k<3;k++)
          m(j+nbdofm+k*nbFFp)+= compAssVector(k) + (comp31plus*Gradshatp[j](0)+comp32plus*Gradshatp[j](1))*jDt[k];
      }
    }
    // Add  terms for full DG
    if(_fullDg){
      // shape functions (no rotation ?)
//      Valsm.clear(); Valsp.clear();
//      _minusSpace->f(ieline->getElem(0),uem, vem, w, Valsm);
//      _plusSpace->f(ieline->getElem(1),uep, vep, w, Valsp);
      std::vector<TensorialTraits<double>::ValType> Valsm = vgpsm[i]->_vvals;
      std::vector<TensorialTraits<double>::ValType> Valsp = vgpsp[i]->_vvals;
      // consistency \mean{j n^\alpha \nu^-_{\alpha} (Assembly with stability)
      ntildemean = 0.5*((vnp_[0]+vnm_[0])*wJnu[0]
                       +(vnp_[1]+vnm_[1])*wJnu[1]);

      if(!broken){
        // Stability (membrane) same expression as linear terms
        // displacement jump
        displacementjump(Valsm,nbFFm,Valsp,nbFFp,datafield,jphi);
        // on SVector3 form
        for(int k=0;k<3;k++){
          jphiVect3[k] = jphi(k);
        }
        // jump*phi
        for(int gamma=0;gamma<2;gamma++){
          jphidotphi[gamma]= dot(lbs->basisVector(gamma),jphiVect3);
        }
        // membrane stiffness
        _mlawMinus->shellMembraneStiffness(ipvm0,Hm);
        _mlawPlus->shellMembraneStiffness(ipvp0,Hp);
        Hm.hat(lbm);
        Hp.hat(lbp);
        Hmean.mean(&Hm,&Hp);
        // stab factor
        double stabfactormembrane[2];
        for(int b=0;b<2;b++){
          stabfactormembrane[b]=0.;
          for(int a=0;a<2;a++){
            for(int g=0;g<2;g++){
              for(int d=0;d<2;d++){
                stabfactormembrane[b]+=Hmean(a,b,g,d)*jphidotphi[g]*lbs->getMinusNormal(d)*halfwj0barrenu[a];
              }
            }
          }
          stabfactormembrane[b]*=b2hs; // factor 2 as bending ?
        }
        // add to consistency before assemble
        ntildemean += stabfactormembrane[0]*lbs->basisVector(0);
        ntildemean += stabfactormembrane[1]*lbs->basisVector(1);
      }
      // Assembly consistency + stability (if not broken)
      for(int j=0;j<nbFFm;j++){
        for(int k=0;k<3;k++){
          m(j+nbFFm*k) -= ntildemean[k]*Valsm[j];
        }
      }
      for(int j=0;j<nbFFp;j++){
        for(int k=0;k<3;k++){
          m(j+3*nbFFm+k*nbFFp) += ntildemean[k]*Valsp[j];
        }
      }
      if(!broken){
        // compatibility membrane (4 terms to assembly)
        SVector3 comp1membraneMinus[2];
        SVector3 comp1membranePlus[2];
        // loop on alpha,beta
        double fact00minus = 0.;
        double fact01minus = 0.;
        double fact11minus = 0.;
        double fact00plus = 0.;
        double fact01plus = 0.;
        double fact11plus = 0.;
        for(int alpha=0;alpha<2;alpha++){
          for(int beta=0;beta<2;beta++){
            double hwJ0bnualphajphidotphib = halfwj0barrenu[alpha]*jphidotphi[beta];
            fact00minus += hwJ0bnualphajphidotphib * Hm(alpha,beta,0,0);
            fact01minus += hwJ0bnualphajphidotphib *(Hm(alpha,beta,0,1)+Hm(alpha,beta,1,0));
            fact11minus += hwJ0bnualphajphidotphib * Hm(alpha,beta,1,1);
            fact00plus += hwJ0bnualphajphidotphib * Hp(alpha,beta,0,0);
            fact01plus += hwJ0bnualphajphidotphib * (Hp(alpha,beta,0,1)+Hp(alpha,beta,1,0));
            fact11plus += hwJ0bnualphajphidotphib * Hp(alpha,beta,1,1);
          }
        }
        // minus
        fact01minus*=0.5; // take half for 01 factor (see developments)
        comp1membraneMinus[0] = fact00minus*lbs->basisVector(0);
        comp1membraneMinus[0]+= fact01minus*lbs->basisVector(1);
        comp1membraneMinus[1] = fact01minus*lbs->basisVector(0);
        comp1membraneMinus[1]+= fact11minus*lbs->basisVector(1);
        // plus
        fact01plus*=0.5; // take half for 01 factor (see developments)
        comp1membranePlus[0] = fact00plus*lbs->basisVector(0);
        comp1membranePlus[0]+= fact01plus*lbs->basisVector(1);
        comp1membranePlus[1] = fact01plus*lbs->basisVector(0);
        comp1membranePlus[1]+= fact11plus*lbs->basisVector(1);

        // term 2 (geometric part)
        SVector3 wjnuana = halfwjbarrenu[0]*vnp_[0]+halfwjbarrenu[1]*vnp_[1];
        double comp2Membraneplus[2];
        comp2Membraneplus[0] = dot(lbs->dualBasisVector(0),wjnuana);
        comp2Membraneplus[1] = dot(lbs->dualBasisVector(1),wjnuana);
        wjnuana = halfwjbarrenu[0]*vnm_[0]+halfwjbarrenu[1]*vnm_[1];
        double comp2Membraneminus[2];
        comp2Membraneminus[0] = dot(lbs->dualBasisVector(0),wjnuana);
        comp2Membraneminus[1] = dot(lbs->dualBasisVector(1),wjnuana);

        SVector3 comp2VectMembraneminus[2];
        SVector3 comp2VectMembraneplus[2];
        for(int alpha=0;alpha<2;alpha++){
          comp2VectMembraneminus[alpha] = comp2Membraneminus[alpha]*jphiVect3;
          comp2VectMembraneplus[alpha] = comp2Membraneplus[alpha]*jphiVect3;
        }

        // term3 (coupling term) when matrix form can be regroup with bending compatibility (assemble in same place in matrix)
        compMembrane311minus.scale(0.);
        compMembrane312minus.scale(0.);
        compMembrane311plus.scale(0.);
        compMembrane312plus.scale(0.);
        compMembrane321minus.scale(0.);
        compMembrane322minus.scale(0.);
        compMembrane321plus.scale(0.);
        compMembrane322plus.scale(0.);
        double halfwj0nuOnlambdaMinus[2];
        double halfwj0nuOnlambdaPlus[2];
        for(int alpha=0;alpha<2;alpha++){
          halfwj0nuOnlambdaMinus[alpha] = halfwj0barrenu[alpha]/ipvm->getGlobalStretch();
          halfwj0nuOnlambdaPlus[alpha] = halfwj0barrenu[alpha]/ipvp->getGlobalStretch();
        }
        for(int beta=0;beta<2;beta++){
          for(int delta=0;delta<2;delta++){
            tensprod(lbs->basisVector(beta),lbs->basisDnormal(delta),tensprodtmp);
            compMembrane311plus.axpy(tensprodtmp,halfwj0nuOnlambdaPlus[0]*Hlambdap(0,beta,0,delta)+halfwj0nuOnlambdaPlus[1]*Hlambdap(1,beta,0,delta));
            compMembrane311minus.axpy(tensprodtmp,halfwj0nuOnlambdaMinus[0]*Hlambdam(0,beta,0,delta)+halfwj0nuOnlambdaMinus[1]*Hlambdam(1,beta,0,delta));
            compMembrane312plus.axpy(tensprodtmp,halfwj0nuOnlambdaPlus[0]*Hlambdap(0,beta,1,delta)+halfwj0nuOnlambdaPlus[1]*Hlambdap(1,beta,1,delta));
            compMembrane312minus.axpy(tensprodtmp,halfwj0nuOnlambdaMinus[0]*Hlambdam(0,beta,1,delta)+halfwj0nuOnlambdaMinus[1]*Hlambdam(1,beta,1,delta));
            tensprod(lbs->basisVector(beta),lbs->basisVector(delta),tensprodtmp);
            compMembrane321plus.axpy(tensprodtmp,halfwj0nuOnlambdaPlus[0]*Hlambdap(0,beta,delta,0)+halfwj0nuOnlambdaPlus[1]*Hlambdap(1,beta,delta,0));
            compMembrane321minus.axpy(tensprodtmp,halfwj0nuOnlambdaMinus[0]*Hlambdam(0,beta,delta,0)+halfwj0nuOnlambdaMinus[1]*Hlambdam(1,beta,delta,0));
            compMembrane322plus.axpy(tensprodtmp,halfwj0nuOnlambdaPlus[0]*Hlambdap(0,beta,delta,1)+halfwj0nuOnlambdaPlus[1]*Hlambdap(1,beta,delta,1));
            compMembrane322minus.axpy(tensprodtmp,halfwj0nuOnlambdaMinus[0]*Hlambdam(0,beta,delta,1)+halfwj0nuOnlambdaMinus[1]*Hlambdam(1,beta,delta,1));
          }
        }
        // term 4
        // common to both parts
        // \jacPlane \lambdah \tilde{m}^{\alpha\mu}
        for(int alpha=0;alpha<2;alpha++){
          for(int mu=0;mu<2;mu++){
            jlambdahtildemhat_minus(alpha,mu) = dot(vmm_[alpha],lbs->dualBasisVector(mu));
            jlambdahtildemhat_plus(alpha,mu) = dot(vmp_[alpha],lbs->dualBasisVector(mu));
          }
        }
        jlambdahtildemhat_minus*=(lambdahm*jbarre);
        jlambdahtildemhat_plus*=(lambdahp*jbarre);

        SVector3 jphidotphidPhi(lbs->dualBasisVector(0));
        jphidotphidPhi*=jphidotphi[0];
        jphidotphidPhi+= jphidotphi[1]*lbs->dualBasisVector(1);
        double wjnualambdahmaminus[2];
        double wjnualambdahmaplus[2];
        for(int mu=0;mu<2;mu++){
          wjnualambdahmaminus[mu] = 0.5*weight*(jlambdahtildemhat_minus(0,mu)*lbs->getMinusNormal(0)+jlambdahtildemhat_minus(1,mu)*lbs->getMinusNormal(1));
          wjnualambdahmaplus[mu] = 0.5*weight*(jlambdahtildemhat_plus(0,mu)*lbs->getMinusNormal(0)+jlambdahtildemhat_plus(1,mu)*lbs->getMinusNormal(1));
        }
        for(int k=0;k<3;k++){
          compMembrane41minus(k) = wjnualambdahmaminus[0]*jphidotphidPhi[k];
          compMembrane42minus(k) = wjnualambdahmaminus[1]*jphidotphidPhi[k];
          compMembrane41plus(k) = wjnualambdahmaplus[0]*jphidotphidPhi[k];
          compMembrane42plus(k) = wjnualambdahmaplus[1]*jphidotphidPhi[k];
        }
        // specific to second parts
        double compMembrane4minus[2];
        double compMembrane4plus[2];
        for(int zeta=0;zeta<2;zeta++){
          compMembrane4minus[zeta] = - (lbm->getlambda(zeta,0)*wjnualambdahmaminus[0] + lbm->getlambda(zeta,1)*wjnualambdahmaminus[1]);
          compMembrane4minus[zeta]/= lambdahm;
          compMembrane4plus[zeta] = - (lbp->getlambda(zeta,0)*wjnualambdahmaplus[0] + lbp->getlambda(zeta,1)*wjnualambdahmaplus[1]);
          compMembrane4plus[zeta]/= lambdahp;
        }
        SVector3 compMembrane4Vectminus[2];
        SVector3 compMembrane4Vectplus[2];
        for(int zeta=0;zeta<2;zeta++){
          compMembrane4Vectminus[zeta] = compMembrane4minus[zeta]*jphidotphidPhi;
          compMembrane4Vectplus[zeta] = compMembrane4plus[zeta]*jphidotphidPhi;
        }

        // Assembly (loop on shape function)
        for(int j=0;j<nbFFm;j++){
          SVector3 compMembAss(comp1membraneMinus[0]+comp2VectMembraneminus[0]+compMembrane4Vectminus[0]);
          compMembAss*=Gradshatm[j][0];
          compMembAss += Gradshatm[j][1]*(comp1membraneMinus[1]+comp2VectMembraneminus[1]+compMembrane4Vectminus[1]);
          comptmpj.scale(0.);
          comptmpj.axpy(compMembrane311minus,Gradshatm[j](0));
          comptmpj.axpy(compMembrane312minus,Gradshatm[j](1));
          comptmpj.gemm(compMembrane321minus,dnvar1hatm[j],1,1);
          comptmpj.gemm(compMembrane322minus,dnvar2hatm[j],1,1);
          comptmpj.multWithATranspose(jphi,1,0,compAssVector);
          // 4th terms first part
          dnvar1hatm[j].multWithATranspose(compMembrane41minus,1,1,compAssVector);
          dnvar2hatm[j].multWithATranspose(compMembrane42minus,1,1,compAssVector);
          for(int k=0;k<3;k++){
            m(j+k*nbFFm) += compMembAss[k] + compAssVector(k);
          }
        }
        for(int j=0;j<nbFFp;j++){
          SVector3 compMembAss(comp1membranePlus[0]+ comp2VectMembraneplus[0] + compMembrane4Vectplus[0]);
          compMembAss*=Gradshatp[j][0];
          compMembAss += Gradshatp[j][1]*(comp1membranePlus[1]+comp2VectMembraneplus[1] + compMembrane4Vectplus[1]);
          comptmpj.scale(0.);
          comptmpj.axpy(compMembrane311plus,Gradshatp[j](0));
          comptmpj.axpy(compMembrane312plus,Gradshatp[j](1));
          comptmpj.gemm(compMembrane321plus,dnvar1hatp[j],1,1);
          comptmpj.gemm(compMembrane322plus,dnvar2hatp[j],1,1);
          comptmpj.multWithATranspose(jphi,1,0,compAssVector);
          // 4th terms first part
          dnvar1hatp[j].multWithATranspose(compMembrane41plus,1,1,compAssVector);
          dnvar2hatp[j].multWithATranspose(compMembrane42plus,1,1,compAssVector);
          for(int k=0;k<3;k++){
            m(j+k*nbFFp+3*nbFFm) += compMembAss[k] + compAssVector(k);
          }
        }

        // stability out of plate (shearing)
        _mlawMinus->shellShearingStiffness(ipvm0,Hs_m);
        _mlawPlus->shellShearingStiffness(ipvp0,Hs_p);
        Hsmean.mean(Hs_m,Hs_p);
        // jphi*t
        double jphidotnormal = dot(jphi,lbs->basisNormal());
        double stabShearingFactor=0;
        for(int a=0;a<2;a++){
          for(int b=0;b<2;b++){
            stabShearingFactor += Hsmean(a,b)*halfwj0barrenu[a]*lbs->getMinusNormal(b);
          }
        }
        stabShearingFactor *= (b3hs*jphidotnormal); // factor 2 as others ?
        SVector3 stabshearterm(lbs->basisNormal());
        stabshearterm*=stabShearingFactor;
        // Assembly
        for(int j=0;j<nbFFm;j++)
          for(int k=0;k<3;k++)
            m(j+k*nbFFm) -= Valsm[j]*stabshearterm[k];
        for(int j=0;j<nbFFp;j++)
          for(int k=0;k<3;k++)
            m(j+3*nbFFm+k*nbFFp) += Valsp[j]*stabshearterm[k];
      }
    }
  }
}

// Interface term on interface for weak BC
void dgNonLinearShellForceVirtInter::get(MElement *ele, int npts, IntPt *GP, fullVector<double> &m)const
{
  MInterfaceLine *ieline = dynamic_cast<MInterfaceLine*>(ele);
  const int nbFFm = ieline->getElem(0)->getNumVertices();
  const int nbdofm = DgC0LinearTerm<double>::space1.getNumKeys(ieline->getElem(0));
  m.resize(nbdofm);
  m.scale(0.);
  // characteristic size
  const double hs = ieline->getCharacteristicSize();
  const double b1hs = _beta1/hs;

  // get value at gauss's point
  AllIPState::ipstateElementContainer *vips = _ipf->getAips()->getIPstate(ieline->getNum());
  // Get the values of Gauss points on minus element
  IntPt *GPm; IntPt *GPp;
  _interQuad->getIntPoints(ieline,GP,&GPm,&GPp);
  // Shape functions values at Gauss points
  nlsFunctionSpace<double>* minusSpace = static_cast<nlsFunctionSpace<double>*>(&this->space1);
  std::vector<GaussPointSpaceValues<double>*> vgpsm;
  minusSpace->get(ieline->getElem(0),npts,GPm,vgpsm);

  // loop on Gauss Point
  for(int i=0;i<npts; i++){
    // Coordonate of Gauss' point i
    //const double u = GP[i].pt[0]; const double v = GP[i].pt[1]; const double w = GP[i].pt[2];
    // Weight of Gauss' point i and Jacobian's value at this point
    const double weight = GP[i].weight;

     // LocalBasis
     const IPStateBase *ipsm = (*vips)[i];
     const IPVariableShell *ipvm = static_cast<const IPVariableShell*>(ipsm->getState(IPStateBase::current));
     const IPVariableShell *ipvm0 = static_cast<const IPVariableShell*>(ipsm->getState(IPStateBase::initial));
     const nonLinearShellLocalBasisBulk *lbm = static_cast<const nonLinearShellLocalBasisBulk*>(ipvm->getshellLocalBasis());
     const nonLinearShellLocalBasisBulk *lbm0 = static_cast<const nonLinearShellLocalBasisBulk*>(ipvm0->getshellLocalBasis());
     const double lambdahm = ipvm->getGlobalStretch();

     // interface (same on minus and plus element take the minus one)
     const nonLinearShellLocalBasisInter* lbs = static_cast<const nonLinearShellLocalBasisInter*>(ipvm->getshellLocalBasisOfInterface());
     const nonLinearShellLocalBasisInter* lbs0= static_cast<const nonLinearShellLocalBasisInter*>(ipvm0->getshellLocalBasisOfInterface());
     const double jbarre = lbs->getJacobian();
     const double jbarre0=lbs0->getJacobian();
    // value of reduction element
    _mlaw->reduction(ipsm,vnm_,vmm_,_fullDg); // no need of nalpha for Cg/Dg
    // Compute of Hessian of SF. Each component of the vector is link to a shape function.
    //Gradsm.resize(0); Hessm.resize(0);
    //double uem = GPm[i].pt[0]; double vem = GPm[i].pt[1];
//    ieline->getuvwOnElem(u,v,uem,vem,wem,uep,vep,wep);
//    DgC0LinearTerm<double>::space1.gradf(ieline->getElem(0),uem, vem, w, Gradsm);
//    DgC0LinearTerm<double>::space1.hessf(ieline->getElem(0),uem, vem, w, Hessm);
    std::vector<TensorialTraits<double>::GradType> &Gradsm = vgpsm[i]->_vgrads;
    std::vector<TensorialTraits<double>::HessType> &Hessm  = vgpsm[i]->_vhess;
    // Compute delta normal
    nvar.setDeltatTilde(lbm,Gradsm,nbFFm);
    // Compute delta normal,alpha
    fillVirtualDeltaConvectedDNormal(nbFFm,lbm,Gradsm,Hessm,nvar.deltatTildeMinus(),dnvar1m,dnvar2m);
    rotateVirtualDeltaConvectedDNormal(nbFFm,lbm,dnvar1m,dnvar2m,dnvar1hatm,dnvar2hatm);
    // Rotation of Derivative of Shape Function (to have virtual phi_,alpha)
    fillAndRotateVirtualConvectedBase(nbFFm,lbm,Gradsm,Gradshatm);

    nvar.bound(lbs); // has to be done after fillVirtualDeltaConvectedDNormal
    const double wJ = jbarre* weight;
    double wJnu[2];
    for(int alpha=0;alpha<2;alpha++)
        wJnu[alpha]=wJ*lbs->getMinusNormal(alpha);

    // consistency term
    // DeltatT^j \mean{\tilde{m}^a lambda} \nu_a
    mtildelamdamean = (vmm_[0]*lambdahm*wJnu[0]+vmm_[1]*lambdahm*wJnu[1]);
    // stability term
    // normal jump
    jDt = lbm->basisNormal()-lbm0->basisNormal();
    jDt*=-1;
    // Contrained fill for bound
    fillConstrainedBoundNormalDirection(lbs,jDt);
    for(int j=0;j<3;j++)
      jDtvector(j) = jDt[j]; // Other format for compatibility change this
    // Tangent moduli tensor. For stability it's constant and the same than for the linear case
    // BE AWARE OF COMBINATION WITH Hn FOR FULLDG
    _mlaw->shellBendingStiffness(ipvm0,Hm);
    // Hat operation
    Hm.hat(lbm);

    // multiplicative factor
    double stabBendingfactor[2];
    for(int c=0;c<2;c++){
      stabBendingfactor[c]=0.;
      for(int a=0;a<2;a++)
        for(int b=0;b<2;b++)
          for(int d=0;d<2;d++)
            stabBendingfactor[c]+=wJnu[a]*Hm(a,b,c,d)*dot(jDt,lbs->basisVector(b))*lbs->getMinusNormal(d);
    }
    for(int c=0;c<2;c++){
      stabBendingfactor[c]*=2*b1hs; // WHY *2 ?? (MetaSheel)
    }
    // ADD TO CONSISTENCY
    mtildelamdamean += stabBendingfactor[0]*lbs->basisVector(0);
    mtildelamdamean += stabBendingfactor[1]*lbs->basisVector(1);

    // loop on shape function
    for(int j=0;j<nbFFm;j++){
      matTvectprod(nvar.deltatTildeMinus(j),mtildelamdamean,DtTmtildelamdamean);
      for(int k=0;k<3;k++)
        m(j+k*nbFFm)-=DtTmtildelamdamean[k]; // minus because it is the jump on minus element (MetaSheel OK)
    }

    // Compatibility
    // The compatibility term is composed of 3 terms (2 mat3x3 that are multiplied
    // by jDt and a sclara that multiply jDt). These terms depend of gradient of shape function that have 2 components
    // So compute the undependent part. There are 2 by terms.
    double wj0barrenu[2];
    for(int alpha=0;alpha<2;alpha++)
      wj0barrenu[alpha] = weight*jbarre0*lbs->getMinusNormal(alpha);
    // set to 0
    comp11minus.scale(0.);
    comp12minus.scale(0.);
    comp21minus.scale(0.);
    comp22minus.scale(0.);
    // Two first terms
    for(int beta=0;beta<2;beta++){
      for(int delta=0;delta<2;delta++){
        tensprod(lbs->basisVector(beta),lbs->basisDnormal(delta),tensprodtmp);
        comp11minus.axpy(tensprodtmp,wj0barrenu[0]*Hm(0,beta,0,delta)+wj0barrenu[1]*Hm(1,beta,0,delta));
        comp12minus.axpy(tensprodtmp,wj0barrenu[0]*Hm(0,beta,1,delta)+wj0barrenu[1]*Hm(1,beta,1,delta));
        tensprod(lbs->basisVector(beta),lbs->basisVector(delta),tensprodtmp);
        comp21minus.axpy(tensprodtmp,wj0barrenu[0]*Hm(0,beta,delta,0)+wj0barrenu[1]*Hm(1,beta,delta,0));
        comp22minus.axpy(tensprodtmp,wj0barrenu[0]*Hm(0,beta,delta,1)+wj0barrenu[1]*Hm(1,beta,delta,1));
      }
    }

    // last one
    double wjbarrenu[2];
    for(int a=0;a<2;a++)
      wjbarrenu[a]= weight*jbarre*lbs->getMinusNormal(a);
      SVector3 wjnuama = wjbarrenu[0]*vmm_[0]+wjbarrenu[1]*vmm_[1];
      double comp31minus= lambdahm*dot(lbs->dualBasisVector(0),wjnuama);
      double comp32minus= lambdahm*dot(lbs->dualBasisVector(1),wjnuama);

    // loop on shape functions
    for(int j=0;j<nbFFm;j++){
      comptmpj.scale(0.);
      comptmpj.axpy(comp11minus,Gradshatm[j](0));
      comptmpj.axpy(comp12minus,Gradshatm[j](1));
      comptmpj.gemm(comp21minus,dnvar1hatm[j],1,1);
      comptmpj.gemm(comp22minus,dnvar2hatm[j],1,1);
      comptmpj.multWithATranspose(jDtvector,1,0,compAssVector);
      for(int k=0;k<3;k++)
        m(j+k*nbFFm) += compAssVector(k)+(comp31minus*Gradshatm[j](0)+comp32minus*Gradshatm[j](1))*jDt[k];
    }

/*
  // Do like Metasheel to compare It works
  for(int jj=0;jj<nbFFm;jj++){
    double tmp1[3][3];
    double tmp2[3][3];
    double tmpp[3][3];
    double tmp3=0;
    for(int j=0;j<3;j++)
    for(int k=0;k<3;k++){
      tmp1[j][k] = tmp2[j][k]=0.;
    }
    for(int a=0;a<2;a++){
      for(int b=0;b<2;b++){
        for(int g=0;g<2;g++){
          for(int d=0;d<2;d++){
            tensprod(lbs->basisVector(b),lbs->basisDnormal(d),tmpp);
            for(int j=0;j<3;j++)
              for(int k=0;k<3;k++)
                tmp1[j][k] +=tmpp[j][k]*lbs->getMinusNormal(a)*lbs0->getJacobian()/lbs->getJacobian()*Hm(a,b,g,d)*Gradshatm[jj](g);

            SVector3 vecttmp2;
            if(d==0){
              matTvectprod(dnvar1hatm[jj],lbs->basisVector(g),vecttmp2);
              dnvar1hatm[jj].print("1");
            }
            else{
              matTvectprod(dnvar2hatm[jj],lbs->basisVector(g),vecttmp2);
              dnvar2hatm[jj].print("2");
            }
            tensprod(lbs->basisVector(b),vecttmp2,tmpp);
            for(int j=0;j<3;j++)
              for(int k=0;k<3;k++)
                tmp2[j][k] += tmpp[j][k]*lbs->getMinusNormal(a)*lbs0->getJacobian()/lbs->getJacobian()*Hm(a,b,g,d);

          }
        }
        tmp3 += lbs->getMinusNormal(a)*lambdahm*dot(vmm_[a],lbs->dualBasisVector(b))*Gradshatm[jj](b);
      }
    }
    double mAss[3][3];
    for(int j=0;j<3;j++)
      for(int k=0;k<3;k++)
        mAss[j][k] = tmp1[j][k]+tmp2[j][k];
    SVector3 cAss;
    matTvectprod(mAss,jDt,cAss);
    SVector3 cAsstmp;
    for(int k=0;k<3;k++)
      cAsstmp[k] = weight*jbarre*(cAss[k]+tmp3*jDt[k]);
    for(int k=0;k<3;k++)
      m(jj+k*nbFFm) += weight*jbarre*(cAss[k]+tmp3*jDt[k]);
  }
*/

  }
 # ifdef _DEBUG
  for(int j=0;j<nbdofm; j++)
    if(std::isnan(m(j)))
      Msg::Error("nan value virtual interface force");
 #endif // _DEBUG
}
