//
// C++ Interface: terms
//
// Description: Functional space for rigid contact in case of dgshell has to be here due to dof creation
//
//
// Author:  <Gauthier BECKER>, (C) 2010
//
// Copyright: See COPYING file that comes with this distribution
//

#ifndef _DGC0SHELLRIGIDCONTACTSPACE_H_
#define _DGC0SHELLRIGIDCONTACTSPACE_H_
#include "contactFunctionSpace.h"
#include "Dof3IntType.h"
class dgC0ShellRigidContactSpace : public rigidContactSpaceBase{
 public:
  dgC0ShellRigidContactSpace(const int id, FunctionSpace<double> *sp, MVertex *ver) : rigidContactSpaceBase(id,sp,ver){}
  dgC0ShellRigidContactSpace(const int id, FunctionSpace<double> *sp, MVertex *ver, const int comp1,
                   const int comp2 = -1, const int comp3 =-1) : rigidContactSpaceBase(id,sp,ver,comp1,comp2,comp3){}
  virtual void getKeys(MElement *ele,std::vector<Dof> &keys) const{
    if(ele->getDim() != 0){ // if dim 0 return the key of gravity center only !!
      getConstRefToSpaceSlave()->getKeys(ele,keys);
    }
    for(int i=0;i<getConstRefToComp().size();i++){
      keys.push_back(Dof(_gc->getNum(),Dof3IntType::createTypeWithThreeInts(getConstRefToComp()[i],getId())));
    }
  }
  virtual int getNumKeys(MElement *ele) const{
    int nkeysele = getConstRefToSpaceSlave()->getNumKeys(ele);
    return nkeysele + getConstRefToComp().size();
  }
  virtual void getKeysOfGravityCenter(std::vector<Dof> &keys) const{
    for(int i=0;i<getConstRefToComp().size(); i++)
      keys.push_back(Dof(_gc->getNum(),Dof3IntType::createTypeWithThreeInts(getConstRefToComp()[i],getId())));
  }
  virtual int getNumKeysOfGravityCenter() const{
    return getConstRefToComp().size();
  }
  virtual void getKeys(MElement *ele, const int ind, std::vector<Dof> &keys) const{
    // generate keys of element and select the good ones after LAGRANGE OK ?? CHANGE THIS HOW TODO
    std::vector<Dof> tkeys;
    this->getKeys(ele,tkeys);
    int nkeys = this->getNumKeys(ele);
    int ncomp = getConstRefToComp().size();
    int nbver = nkeys/ncomp-1; // because GC is in the nkeys
    for(int i=0;i<ncomp; i++)
      keys.push_back(tkeys[ind+i*nbver]);
    this->getKeysOfGravityCenter(keys);
  }
  virtual int getNumKeys(MElement *ele, int ind) const{
    return 2*getConstRefToComp().size();
  }
};

#endif // _DGC0SHELLRIGIDCONTACTSPACE_H_
