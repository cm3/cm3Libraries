//
// C++ Interface: terms
//
// Description: Define the localbasis of element for shells
//
//
// Author:  <Gauthier BECKER>, (C) 2010
//
// Copyright: See COPYING file that comes with this distribution
//
//


#ifndef SHELLLOCALBASIS_H_
#define SHELLLOCALBASIS_H_
#include "SVector3.h"
#include "MElement.h"
#include "functionSpace.h"
#include "MInterfaceElement.h"
#include "MInterfaceLine.h"
static inline void crossprod(const SVector3 &a, const SVector3 &b, SVector3 &c){
 c(0) = a.y() * b.z() - b.y() * a.z();
 c(1) = -(a.x() * b.z() - b.x() * a.z());
 c(2) = a.x() * b.y() - b.x() * a.y();
}

// base class (virtual with common function)
class shellLocalBasis{
 public:
  shellLocalBasis(){}
  shellLocalBasis(const shellLocalBasis &source){}
  shellLocalBasis& operator=(const shellLocalBasis &source){return *this;}
  virtual ~shellLocalBasis(){}
  // Common function (get vector and jacobian)
  virtual double getJacobian() const=0;
  virtual const SVector3& basisNormal() const=0;
  virtual double basisNormal(const int i)const=0;
  virtual const SVector3& basisVector(const int i) const=0;
  virtual const SVector3& dualBasisVector(const int i)const=0;
  virtual double basisVector(const int i,const int j) const=0;
  virtual double dualBasisVector(const int i,const int j) const=0;
  virtual void restart(){};
};

class shellLocalBasisBulk : public shellLocalBasis{
 public:
  shellLocalBasisBulk() : shellLocalBasis(){}
  shellLocalBasisBulk(const shellLocalBasisBulk &source) : shellLocalBasis(source){}
  shellLocalBasisBulk& operator=(const shellLocalBasis &source);
  virtual ~shellLocalBasisBulk(){}
  virtual double basisPushTensor(const int i, const int j) const=0;
  virtual double basisInvPushTensor(const int i, const int j) const=0;
  virtual const double* basisPushTensor() const=0;
  virtual const double* basisInvPushTensor() const=0;
  virtual const double getlambda(const int i,const int j) const=0;
  void dualGbasis(const SVector3 &g_1,const SVector3 &g_2,const SVector3 &g_3,
                  SVector3 &g1,SVector3 &g2,SVector3 &g3)const;
  virtual void restart(){};
};

class linearShellLocalBasisInter;
class linearShellLocalBasisBulk : public shellLocalBasisBulk
{
 protected :
  std::vector<SVector3> phi0;
  std::vector<SVector3> phi0d;
  std::vector<SVector3> derivt0;
  SVector3 derivphi0[2][2];
  double T[4];
  double t[4];
  double _lambda[2][2];
  SVector3 t0;
  double detJ0;
  void setphiall(const double d);
 public :
  linearShellLocalBasisBulk();
  virtual ~linearShellLocalBasisBulk(){}
  // Copy Constructor
  linearShellLocalBasisBulk& operator=(const shellLocalBasis &source);
  linearShellLocalBasisBulk(const linearShellLocalBasisBulk &source);
  void set(MElement *ele, const std::vector<TensorialTraits<double>::GradType> &Grads,
                            const std::vector<TensorialTraits<double>::HessType> &Hess);
  void setBasisPushTensor(const linearShellLocalBasisInter *lbs);
  // get operation
  inline double getJacobian() const {return detJ0;}
  const SVector3& basisNormal() const {return t0;}
  inline double basisNormal(const int i) const {return t0(i);}
  const SVector3& basisVector(const int i) const {if(i<2) return phi0[i]; else return t0;}
  const SVector3& dualBasisVector(const int i)const {if (i<2) return phi0d[i]; else return t0;}
  inline double basisVector(const int i,const int j) const {return phi0[i](j);}
  inline double dualBasisVector(const int i,const int j) const { if(i<2) return phi0d[i](j); else return t0[j];}
  inline double basisPushTensor(const int i, const int j) const {return T[i+j+j];}
  inline double basisInvPushTensor(const int i, const int j) const {return t[i+j+j];}
  inline const double* basisPushTensor() const{return T;}
  inline const double* basisInvPushTensor() const{return t;}
  // get a orthonormal basis
  inline SVector3 basisOrthonormalVector(const int i) const{
  if(i<2)
    return phi0[i]*(1/phi0[i].norm());
  else
    return t0*(1/t0.norm());
  }
  inline const SVector3& basisDeriVector(const int i, const int j) const {return derivphi0[i][j];}
  inline const double basisDeriVector(const int i, const int j, const int k) const {return derivphi0[i][j](k);}
  inline const double basisDeriNormal(const int i,const int j) const{return derivt0[i][j];}
  inline const SVector3& basisDeriNormal(const int i) const{return derivt0[i];}
  const double getlambda(const int i,const int j) const{return _lambda[i][j];}
  virtual void restart();
};

class shellLocalBasisInter : public shellLocalBasis{
 protected :
  std::vector<SVector3> phi0;
  std::vector<SVector3> phi0d;
  SVector3 t0;
  double detJ0;
  double minusNormal[2];
  void setMinusNormal();
  void setphiall(const double d);

 public :
  shellLocalBasisInter();
  virtual ~shellLocalBasisInter(){}
  // Copy Constructor
  shellLocalBasisInter& operator=(const shellLocalBasis &source);
  shellLocalBasisInter(const shellLocalBasisInter &source);
  // get operation
  inline double getMinusNormal(const int i) const{ return minusNormal[i];}
  inline double getJacobian() const {return detJ0;}
  const SVector3& basisNormal() const {return t0;}
  inline double basisNormal(const int i) const {return t0(i);}
  const SVector3& basisVector(const int i) const {if(i<2) return phi0[i]; else return t0;}
  const SVector3& dualBasisVector(const int i)const {if (i<2) return phi0d[i]; else return t0;}
  inline double basisVector(const int i,const int j) const {return phi0[i](j);}
  inline double dualBasisVector(const int i,const int j) const {if(i<2) return phi0d[i](j); else return t0[j];}
  // get a orthonormal basis
  inline SVector3 basisOrthonormalVector(const int i) const{
  if(i<2)
    return phi0[i]*(1/phi0[i].norm());
  else
    return t0*(1/t0.norm());
  }
  virtual void restart();
};

class linearShellLocalBasisInter : public shellLocalBasisInter{
 public:
  linearShellLocalBasisInter() : shellLocalBasisInter(){}
  ~linearShellLocalBasisInter(){}
  // Copy Constructor
  linearShellLocalBasisInter& operator=(const shellLocalBasis &source);
  linearShellLocalBasisInter(const linearShellLocalBasisInter &source) : shellLocalBasisInter(source){};
  void set(MInterfaceLine *iele, const std::vector<TensorialTraits<double>::GradType> &Grads, const SVector3 &t0p,
           const SVector3 &t0m);
  void set(MInterfaceLine *iele, const std::vector<TensorialTraits<double>::GradType> &Grads, const SVector3 &t0m);
  virtual void restart(){ return shellLocalBasisInter::restart();}
};

class nonLinearShellLocalBasisBulk : public shellLocalBasisBulk
{
 // phi0 is not kept to access to this value the initial ipvariable has to be used
 // one local basis by gauss point and it's the same for all Simpson's point
 protected:
  SVector3 _phi[2];
  SVector3 _dualPhi[2];
  SVector3 _derivPhi[2][2];
  SVector3 _tnormal;
  SVector3 _deriTnormal[2];
  double _lambda[2][2];
  std::vector<double> _detJ;
  std::vector<double> _detJ0; // need to return Cauchy stress Tensor (and not the KirchoffStressTensor) for fracture evaluation
  std::vector<SVector3> _g_1;
  std::vector<SVector3> _g_2;
  std::vector<SVector3> _g_3;
  std::vector<SVector3> _g1;
  std::vector<SVector3> _g2;
  std::vector<SVector3> _g3;
  short int _nbsimp; // number of integration points on thickness
  void resetAll(); // SVector3 =0 and _lambdah=1 _detJ0 unchanged
  double _detJbarre;
  // push tensor
  double T[4];
  double t[4];
 public:
  nonLinearShellLocalBasisBulk(const short int nbsimp);
  nonLinearShellLocalBasisBulk(const nonLinearShellLocalBasisBulk &source);
  nonLinearShellLocalBasisBulk& operator=(const shellLocalBasis &source);
  virtual ~nonLinearShellLocalBasisBulk(){}
  void set(MElement *ele, const std::vector<TensorialTraits<double>::GradType> &Grads,
            const std::vector<TensorialTraits<double>::HessType> &Hess,const fullVector<double> &unknowns,
            const std::vector<double> &lambdah,const std::vector<double> &zsimp);
  void setBasisVectorG(const int npoint,const double zk,const double lambdah);
  void setBasisPushTensor(const shellLocalBasisInter *lbs);
  void resetNbSimp(const short int nbsimp);
  virtual void setLambda(const double lh);
  virtual void setInitialJacobian(const std::vector<double> &detj0);
  // get
  virtual double getJacobian() const{return _detJbarre;}
  double getJacobian(const int i){return _detJ[i];}
  const double getJacobian(const int i)const {return _detJ[i];}
  virtual const SVector3& basisNormal() const{return _tnormal;}
  virtual double basisNormal(const int i)const{return _tnormal[i];}
  virtual const SVector3& basisDnormal(const int i) const{return _deriTnormal[i];}
  virtual const SVector3& basisVector(const int i) const{return _phi[i];}
  virtual inline const SVector3& basisDeriVector(const int i, const int j) const {return _derivPhi[i][j];}
  virtual inline const double basisDeriVector(const int i, const int j, const int k) const {return _derivPhi[i][j](k);}
  virtual const SVector3& dualBasisVector(const int i)const{return _dualPhi[i];}
  virtual double basisVector(const int i,const int j) const{return _phi[i][j];}
  virtual double dualBasisVector(const int i,const int j) const{return _dualPhi[i][j];}
  const SVector3& basisVectorG1(const int npt) const{return _g_1[npt];}
  const SVector3& basisVectorG2(const int npt) const{return _g_2[npt];}
  const SVector3& basisVectorG3(const int npt) const{return _g_3[npt];}
  const SVector3& dualBasisVectorG1(const int npt) const{return _g1[npt];}
  const SVector3& dualBasisVectorG2(const int npt) const{return _g2[npt];}
  const SVector3& dualBasisVectorG3(const int npt) const{return _g3[npt];}
  const double basisVectorG1(const int npt,const int i) const{return _g_1[npt][i];}
  const double basisVectorG2(const int npt, const int i) const{return _g_2[npt][i];}
  const double basisVectorG3(const int npt, const int i) const{return _g_3[npt][i];}
  const double dualBasisVectorG1(const int npt, const int i) const{return _g1[npt][i];}
  const double dualBasisVectorG2(const int npt, const int i) const{return _g2[npt][i];}
  const double dualBasisVectorG3(const int npt, const int i) const{return _g3[npt][i];}
  const double dualBasisVectorG(const int npt,const int i,const int j)
  {
    switch(i)
    {
     case 0:
      return _g1[npt][j];
     case 1:
      return _g2[npt][j];
     case 2:
      return _g3[npt][j];
     default:
      Msg::Error("Local basis has only three base vectors");
      return 0.;
    }
  }
  inline double basisPushTensor(const int i, const int j) const {return T[i+j+j];}
  inline double basisInvPushTensor(const int i, const int j) const {return t[i+j+j];}
  inline const double* basisPushTensor() const{return T;}
  inline const double* basisInvPushTensor() const{return t;}
  virtual const std::vector<double>& getInitialJacobian()const{return _detJ0;}
  virtual const std::vector<double>& getJacobianOfSimpsonPoints()const{return _detJ;}
  virtual double getInitialJacobian(const int i)const{return _detJ0[i];}
  const double getlambda(const int i, const int j) const{return _lambda[i][j];}
  virtual void restart();
};

// only thing that change betwwen linear and non linear case
// is the used of phi current and not phi0. copy all class
// this changement is taken into account in the set operation.
// the container keep the 0 but it is the current that is computed
class nonLinearShellLocalBasisInter : public shellLocalBasisInter{
 protected:
  SVector3 _derivPhi[2][2];
  SVector3 _deriTnormal[2];
 public:
  nonLinearShellLocalBasisInter() : shellLocalBasisInter(){}
  virtual ~nonLinearShellLocalBasisInter(){}
  nonLinearShellLocalBasisInter& operator=(const shellLocalBasis &source);
  nonLinearShellLocalBasisInter(const nonLinearShellLocalBasisInter &source);
  void set(MInterfaceLine *iele, const std::vector<TensorialTraits<double>::GradType> &Grads,
           const fullVector<double> &unknown, nonLinearShellLocalBasisBulk *lbm, const double lambdahm,
           nonLinearShellLocalBasisBulk *lbp, const double lambdahp);
  void set(MInterfaceLine *iele, const std::vector<TensorialTraits<double>::GradType> &Grads,
           const fullVector<double> &unknown, nonLinearShellLocalBasisBulk *lbm, const double lambdahm);
  void setphiall(const double d);
  // inline get
  inline const SVector3& basisDeriVector(const int i, const int j) const {return _derivPhi[i][j];}
  inline const double basisDeriVector(const int i, const int j, const int k) const {return _derivPhi[i][j](k);}
  virtual const SVector3& basisDnormal(const int i) const{return _deriTnormal[i];}
  virtual void restart();
};

# endif // SHELLLOCALBASIS
