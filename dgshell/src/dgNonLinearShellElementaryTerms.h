//
// C++ Interface: terms
//
// Description: Inline function called to build dg non linear shell terms
//
//
// Author:  <Gauthier BECKER>, (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
//

void fillScrewMatrix(const SVector3 &a, fullMatrix<double> &mscrew)
{
  mscrew(0,0) =      0. ;  mscrew(0,1) =  -a[2];   mscrew(0,2) =   a[1];
  mscrew(1,0) =     a[2];  mscrew(1,1) =    0. ;   mscrew(1,2) =  -a[0];
  mscrew(2,0) =    -a[1];  mscrew(2,1) =   a[0];   mscrew(2,2) =    0. ;
}
/* // unused
void fillScrewMatrixAdd(const SVector3 &a, fullMatrix<double> &mscrew)
{
  mscrew(0,0) +=      0. ;  mscrew(0,1) +=  -a[2];   mscrew(0,2) +=   a[1];
  mscrew(1,0) +=     a[2];  mscrew(1,1) +=    0. ;   mscrew(1,2) +=  -a[0];
  mscrew(2,0) +=    -a[1];  mscrew(2,1) +=   a[0];   mscrew(2,2) +=    0. ;
}
*/
void alphaCrossprodAdd(const SVector3 &a, const SVector3 &b,const double alpha, SVector3 &c)
{
 c(0) += alpha*(a.y() * b.z() - b.y() * a.z());
 c(1) -= alpha*(a.x() * b.z() - b.x() * a.z());
 c(2) += alpha*(a.x() * b.y() - b.x() * a.y());
}

void tensprod(const SVector3 &a, const SVector3 &b, fullMatrix<double> &c)
{
  c(0,0) = a(0)*b(0);   c(0,1) = a(0)*b(1);   c(0,2) = a(0)*b(2);
  c(1,0) = a(1)*b(0);   c(1,1) = a(1)*b(1);   c(1,2) = a(1)*b(2);
  c(2,0) = a(2)*b(0);   c(2,1) = a(2)*b(1);   c(2,2) = a(2)*b(2);
}

void tensprodAdd(const SVector3 &a, const SVector3 &b, fullMatrix<double> &c)
{
  c(0,0) += a(0)*b(0);   c(0,1) += a(0)*b(1);   c(0,2) += a(0)*b(2);
  c(1,0) += a(1)*b(0);   c(1,1) += a(1)*b(1);   c(1,2) += a(1)*b(2);
  c(2,0) += a(2)*b(0);   c(2,1) += a(2)*b(1);   c(2,2) += a(2)*b(2);
}

void matTvectprodAdd(const fullMatrix<double> &mbuildterm, const SVector3 &v, SVector3 &v2){
  for(int i=0;i<3;i++)
    v2[i] += mbuildterm(0,i)*v[0] + mbuildterm(1,i)*v[1] + mbuildterm(2,i)*v[2];
}

void fillVirtualDeltaConvectedDNormal(const int nbFF,const nonLinearShellLocalBasisBulk *lb,
                                      const std::vector<TensorialTraits<double>::GradType> &Grads,
                                      const std::vector<TensorialTraits<double>::HessType> &Hesss,
                                      const std::vector<fullMatrix<double> > &nvar,
                                      std::vector<fullMatrix<double> > &Dnvar1,
                                      std::vector<fullMatrix<double> > &Dnvar2)
{
  // cache data for the function
  static SVector3 vect1;
  static SVector3 vect2;
  static SVector3 vect3;
  static SVector3 vect4;
  static SVector3 vect5;
  static SVector3 vect6;
  static SVector3 vect7;
  static SVector3 tp11;
  static SVector3 tp12;
  static SVector3 tp21;
  static SVector3 tp22;
  static SVector3 vectTensTnor1;
  static SVector3 vectTensTnor2;

  // get Vector from LocalBasis
  const SVector3 &phi1 = lb->basisVector(0);
  const SVector3 &phi2 = lb->basisVector(1);
  const SVector3 &phi11= lb->basisDeriVector(0,0);
  const SVector3 &phi12= lb->basisDeriVector(0,1);
  const SVector3 &phi21= lb->basisDeriVector(1,0);
  const SVector3 &phi22= lb->basisDeriVector(1,1);
  const SVector3 &tnor = lb->basisNormal();
  const SVector3 &dt1 = lb->basisDnormal(0);
  const SVector3 &dt2 = lb->basisDnormal(1);
  const double oneOnjbarre = 1./lb->getJacobian();


  // common to all shape Functions
  crossprod(phi2,tnor,vect1);
  crossprod(phi1,tnor,vect2);
  double scalenvar1 = dot(phi11,vect1)-dot(phi21,vect2);
  double scalenvar2 = dot(phi12,vect1)-dot(phi22,vect2);

  crossprod(phi21,phi1,vect3);
  alphaCrossprodAdd(phi11,phi2,-1,vect3);
  crossprod(phi22,phi1,vect4);
  alphaCrossprodAdd(phi12,phi2,-1,vect4);
//  crossprod(tnor,phi1,vect5);
//  crossprod(tnor,phi2,vect6);
  vect5 = vect2; vect5*=-1;
  vect6 = vect1; vect6*=-1;

  crossprod(tnor,phi11,tp11);
  crossprod(tnor,phi12,tp12);
  crossprod(tnor,phi21,tp21);
  crossprod(tnor,phi22,tp22);

  // loop on shape function
  for(int j=0;j<nbFF;j++){
    const double dN1 = Grads[j](0);
    const double dN2 = Grads[j](1);
    const double ddN11=Hesss[j](0,0);
    const double ddN12=Hesss[j](0,1);
    const double ddN21=Hesss[j](1,0);
    const double ddN22=Hesss[j](1,1);

    const fullMatrix<double> &nvarj = nvar[j];
    fullMatrix<double> &dnvarj1 = Dnvar1[j];
    fullMatrix<double> &dnvarj2 = Dnvar2[j];
//    // set operation after
//    dnvarj1.scale(0.);
//    dnvarj2.scale(0.);

    // sign and values OK (debug with MetaSheels)
    vect7 = dN2*phi11-ddN11*phi2+ddN12*phi1-dN1*phi21;
    fillScrewMatrix(vect7,dnvarj1);
    vect7 = -ddN12*phi2+ddN22*phi1+dN2*phi12-dN1*phi22;
    fillScrewMatrix(vect7,dnvarj2);
    /*
    fillScrewMatrixAdd(dN2*phi11,dnvarj1);
    fillScrewMatrixAdd(-ddN11*phi2,dnvarj1);
    fillScrewMatrixAdd(ddN12*phi1,dnvarj1);
    fillScrewMatrixAdd(-dN1*phi21,dnvarj1);
    fillScrewMatrixAdd(-ddN12*phi2,dnvarj2);
    fillScrewMatrixAdd(ddN22*phi1,dnvarj2);
    fillScrewMatrixAdd(dN2*phi12,dnvarj2);
    fillScrewMatrixAdd(-dN1*phi22,dnvarj2);
    */

    dnvarj1.axpy(nvarj,-scalenvar1);
    dnvarj2.axpy(nvarj,-scalenvar2);

    vectTensTnor1 = -ddN11*vect1 + ddN12*vect2 - dN2*tp11 + dN1*tp21;
    vectTensTnor2 = -ddN21*vect1 + ddN22*vect2 - dN2*tp12 + dN1*tp22;
    //alphaCrossprodAdd(tnor,phi11,-dN2,vectTensTnor1);
    //alphaCrossprodAdd(tnor,phi21,dN1,vectTensTnor1);
    //alphaCrossprodAdd(tnor,phi12,-dN2,vectTensTnor2);
    //alphaCrossprodAdd(tnor,phi22,dN1,vectTensTnor2);
    // No assembly because the 2 tensorial product with tnor are regrouped

    matTvectprodAdd(nvarj,vect3,vectTensTnor1);
    matTvectprodAdd(nvarj,vect4,vectTensTnor2);
    tensprodAdd(tnor,vectTensTnor1,dnvarj1);
    tensprodAdd(tnor,vectTensTnor2,dnvarj2);

    vectTensTnor1 = -dN2*vect5 + dN1*vect6;
    vectTensTnor2 = vectTensTnor1;
    tensprodAdd(dt1,vectTensTnor1,dnvarj1);
    tensprodAdd(dt2,vectTensTnor2,dnvarj2);

    dnvarj1.scale(oneOnjbarre);
    dnvarj2.scale(oneOnjbarre);
  }
}

void rotateVirtualDeltaConvectedDNormal(const int nbFF,const nonLinearShellLocalBasisBulk *lb,
                                        const std::vector<fullMatrix<double> > &dnvar1,
                                        const std::vector<fullMatrix<double> > &dnvar2,
                                        std::vector<fullMatrix<double> > &dnvarhat1,
                                        std::vector<fullMatrix<double> > &dnvarhat2)
{
  for(int i=0;i<nbFF;i++){
    dnvarhat1[i] = dnvar1[i];
    dnvarhat1[i].scale(lb->basisInvPushTensor(0,0));
    dnvarhat1[i].axpy(dnvar2[i],lb->basisInvPushTensor(0,1));
    dnvarhat2[i] = dnvar1[i];
    dnvarhat2[i].scale(lb->basisInvPushTensor(1,0));
    dnvarhat2[i].axpy(dnvar2[i],lb->basisInvPushTensor(1,1));
  }
}

void fillAndRotateVirtualConvectedBase(const int nbFF, const nonLinearShellLocalBasisBulk *lb,
                                       const std::vector<TensorialTraits<double>::GradType> &Grads,
                                       std::vector<TensorialTraits<double>::GradType> &Gradshat)
{
  for(int i=0;i<nbFF;i++){
    Gradshat[i](0) = Grads[i](0)*lb->basisInvPushTensor(0,0)+Grads[i](1)*lb->basisInvPushTensor(0,1);
    Gradshat[i](1) = Grads[i](0)*lb->basisInvPushTensor(1,0)+Grads[i](1)*lb->basisInvPushTensor(1,1);
  }
}

void fillConstrainedBoundNormalDirection(const nonLinearShellLocalBasisInter *lbs,SVector3 &jDt)
{
  SVector3 dir = lbs->dualBasisVector(0)*lbs->getMinusNormal(0)+lbs->dualBasisVector(1)*lbs->getMinusNormal(1);
  double scaldir = dot(jDt,dir);
  jDt = scaldir*dir;
}
