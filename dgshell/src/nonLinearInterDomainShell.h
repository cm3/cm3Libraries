//
// C++ Interface: inter domain between 2 nonLinearDomain HAS TO BE REMOVED AFTER FIX PROBLEM OF INTERDOMAIN MANAGEMENT (computation of ipv)
//                for now +/- copy-paste of interDomainBetweenShell
// Description: Interface class for dg shell
//
// Author:  <Gauthier BECKER>, (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
//

// BEAWARE copy paste of interDomainBetweenShell (will be removed)
#ifndef NONLINEARINTERDOMAINSHELL_H_
#define NONLINEARINTERDOMAINSHELL_H_

#include "dgShellDomain.h"
class nonLinearInterDomainShell : public dgNonLinearShellDomain{
 protected :
  BiNonLinearTermBase *bterm;
  nonLinearTermBase<double> *lterm;
  partDomain *_domMinus;
  partDomain *_domPlus;
  int _physMinus;
  int _physPlus;
  materialLaw* _mlawMinus;
  materialLaw* _mlawPlus;
 public:
  nonLinearInterDomainShell(const int tag, partDomain *dom1, partDomain *dom2, const int lnum=0);
 #ifndef SWIG
  nonLinearInterDomainShell(const nonLinearInterDomainShell &source);
  ~nonLinearInterDomainShell(){}
//  void initializeTerms(FunctionSpaceBase* space1_,FunctionSpaceBase* space2_,unknownField *uf,IPField*ip);
  virtual void setGaussIntegrationRule();
  virtual bool computeIPVariable(AllIPState *aips,const unknownField *ufield,const IPStateBase::whichState ws, bool stiff);
  virtual void setMaterialLaw(const std::map<int,materialLaw*>& maplaw);
  virtual materialLaw* getMaterialLaw(){return _mlaw;}
  virtual const materialLaw* getMaterialLaw() const{return _mlaw;}
  virtual materialLaw* getMaterialLawMinus(){return _mlawMinus;}
  virtual const materialLaw* getMaterialLawMinus() const{return _mlawMinus;}
  virtual materialLaw* getMaterialLawPlus(){return _mlawPlus;}
  virtual const materialLaw* getMaterialLawPlus() const{return _mlawPlus;}
  virtual int getLawNum() const{return _lnum;}
  virtual int getMinusLawNum() const{return _domMinus->getLawNum();}
  virtual int getPlusLawNum() const{return _domPlus->getLawNum();}
  virtual const partDomain* getMinusDomain() const{return _domMinus;}
  virtual const partDomain* getPlusDomain() const{return _domPlus;}
  virtual partDomain* getMinusDomain(){return _domMinus;}
  virtual partDomain* getPlusDomain() {return _domPlus;}
  void initializeTerms(unknownField *uf,IPField *ip);
  // as there is no elerment on a interDomain empty function. The interface element on an interDomain
  // has to be created via its two domains.
  virtual void createInterface(manageInterface &maninter){}
 #endif // SWIG
};

#endif // NONLINEARINTERDOMAINSHELL_H_
