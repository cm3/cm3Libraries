//
//
// Description: Class of materials for non linear dg shell
//
//
// Author:  <Gauthier BECKER>, (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "dgNonLinearShellMaterial.h"
#include "dgNonLinearShellIPVariable.h"
#include <math.h>
double SimpsonIntegration(const std::vector<double> &y, const std::vector<double> &zk);
double SimpsonIntegration(const std::vector<double> &y, const double bminusa);
static void alphaTensprodSym(const double alpha,const SVector3 &a,const SVector3 &b,tab6& msym)
{
  msym[0]+=2*alpha*a[0]*b[0];
  msym[1]+=2*alpha*a[1]*b[1];
  msym[2]+=2*alpha*a[2]*b[2];
  msym[3]+=alpha*(a[0]*b[1]+a[1]*b[0]);
  msym[4]+=alpha*(a[1]*b[2]+a[2]*b[1]);
  msym[5]+=alpha*(a[0]*b[2]+a[2]*b[0]);
}

static void alphaTensprodSym(const double alpha,const SVector3 &a,tab6& msym)
{
  msym[0]+=alpha*a[0]*a[0];
  msym[1]+=alpha*a[1]*a[1];
  msym[2]+=alpha*a[2]*a[2];
  msym[3]+=alpha*a[0]*a[1];
  msym[4]+=alpha*a[1]*a[2];
  msym[5]+=alpha*a[0]*a[2];
}

static void alphaTensprodAdd(const double alpha,const SVector3 &a,const SVector3 &b,STensor3 &sigma)
{
  if(alpha !=0.){
    for(int i=0;i<3;i++){
      for(int j=0;j<3;j++){
        sigma(i,j) += (alpha*a[i]*b[j]);
      }
    }
  }
}

static double xyz2sh1comp(const SVector3 &v1,const SVector3 &v2, const STensor3 &tensXYZ)
{
  static STensor3 tens3tmp;
  tensprod(v1,v2,tens3tmp);
  double contract=0.;
  for(int i=0;i<3;i++)
    for(int j=0;j<3;j++)
      contract+= tensXYZ(i,j)*tens3tmp(i,j);
  return contract;
}

void stressTensorShellToXYZbasis(const int npoint,const STensor3 &PKshell,const nonLinearShellLocalBasisBulk *lbcur,STensor3 &PKxyz__)
{
  const SVector3& g_1 = lbcur->basisVectorG1(npoint);
  const SVector3& g_2 = lbcur->basisVectorG2(npoint);
  const SVector3& g_3 = lbcur->basisVectorG3(npoint);
  if(PKshell(0,0) != 0){
    tensprod(g_1,g_1,PKxyz__);
    PKxyz__*=PKshell(0,0);
  }
  else
  {
    for(int ii=0;ii<9;ii++) PKxyz__[ii] = 0.;
  }
  alphaTensprodAdd(PKshell(0,1),g_1,g_2,PKxyz__);
  alphaTensprodAdd(PKshell(0,2),g_1,g_3,PKxyz__);
  alphaTensprodAdd(PKshell(1,0),g_2,g_1,PKxyz__);
  alphaTensprodAdd(PKshell(1,1),g_2,g_2,PKxyz__);
  alphaTensprodAdd(PKshell(1,2),g_2,g_3,PKxyz__);
  alphaTensprodAdd(PKshell(2,0),g_3,g_1,PKxyz__);
  alphaTensprodAdd(PKshell(2,1),g_3,g_2,PKxyz__);
  alphaTensprodAdd(PKshell(2,2),g_3,g_3,PKxyz__);
}

void stressTensorXYZToShellbasis(const int npoint,const STensor3 &PKxyz__,const nonLinearShellLocalBasisBulk *lbcur,STensor3 &PKsh__)
{
  const SVector3& g1 = lbcur->dualBasisVectorG1(npoint);
  const SVector3& g2 = lbcur->dualBasisVectorG2(npoint);
  const SVector3& g3 = lbcur->dualBasisVectorG3(npoint);

  PKsh__(0,0) = xyz2sh1comp(g1,g1,PKxyz__);
  PKsh__(0,1) = xyz2sh1comp(g1,g2,PKxyz__);
  PKsh__(0,2) = xyz2sh1comp(g1,g3,PKxyz__);
  PKsh__(1,0) = xyz2sh1comp(g2,g1,PKxyz__);
  PKsh__(1,1) = xyz2sh1comp(g2,g2,PKxyz__);
  PKsh__(1,2) = xyz2sh1comp(g2,g3,PKxyz__);
  PKsh__(2,0) = xyz2sh1comp(g3,g1,PKxyz__);
  PKsh__(2,1) = xyz2sh1comp(g3,g2,PKxyz__);
  PKsh__(2,2) = xyz2sh1comp(g3,g3,PKxyz__);
}


static double transposeDeterminantSTensor3(const STensor3 &a)
{
  return (a(0,0) * (a(1,1) * a(2,2) - a(2,1) * a(1,2)) -
          a(1,0) * (a(0,1) * a(2,2) - a(2,1) * a(0,2)) +
          a(2,0) * (a(0,1) * a(1,2) - a(1,1) * a(0,2)));
}

static void transposeInverseSTensor3(const STensor3 &a, STensor3 &ainv)
{
  double udet = 1./transposeDeterminantSTensor3(a);
  ainv(0,0) =  (a(1,1) * a(2,2) - a(2,1) * a(1,2))* udet;
  ainv(1,0) = -(a(0,1) * a(2,2) - a(2,1) * a(0,2))* udet;
  ainv(2,0) =  (a(0,1) * a(1,2) - a(1,1) * a(0,2))* udet;
  ainv(0,1) = -(a(1,0) * a(2,2) - a(2,0) * a(1,2))* udet;
  ainv(1,1) =  (a(0,0) * a(2,2) - a(2,0) * a(0,2))* udet;
  ainv(2,1) = -(a(0,0) * a(1,2) - a(1,0) * a(0,2))* udet;
  ainv(0,2) =  (a(1,0) * a(2,1) - a(2,0) * a(1,1))* udet;
  ainv(1,2) = -(a(0,0) * a(2,1) - a(2,0) * a(0,1))* udet;
  ainv(2,2) =  (a(0,0) * a(1,1) - a(1,0) * a(0,1))* udet;
}

static void multSTensor3(const STensor3 &a, const STensor3 &b, STensor3 &c)
{
  for(int i=0;i<3;i++)
    for(int j=0;j<3;j++)
    {
      c(i,j) = a(i,0)*b(0,j)+a(i,1)*b(1,j)+a(i,2)*b(2,j);
    }
}

static void multSTensor3SecondTranspose(const STensor3 &a, const STensor3 &b, STensor3 &c)
{
  for(int i=0;i<3;i++)
    for(int j=0;j<3;j++)
      c(i,j) = a(i,0)*b(j,0)+a(i,1)*b(j,1)+a(i,2)*b(j,2);
}

void dgNonLinearShellMaterialLaw::ensurePlaneStress(IPStateBase *ips, const bool nonEvalStiffness)
{
  IPVariableShell *ipvcur = static_cast<IPVariableShell*>(ips->getState(IPStateBase::current));
  IPVariableShell *ipvprev = static_cast<IPVariableShell*>(ips->getState(IPStateBase::previous));
  IPVariableShell *ipvinit= static_cast<IPVariableShell*>(ips->getState(IPStateBase::initial));
  nonLinearShellLocalBasisBulk *lbcur = static_cast<nonLinearShellLocalBasisBulk*>(ipvcur->getshellLocalBasis());
  const nonLinearShellLocalBasisBulk *lbinit= static_cast<nonLinearShellLocalBasisBulk*>(ipvinit->getshellLocalBasis());
  //  double lambdak;
  double lambdakp1;
  int nsimp = ipvcur->getNumSimp();
  double t33,t33rel,dkdl;
  double t33prec,lambdak; // for DICHOTOMIE ONLY
  // loop on Simpson's points
  for(int np=0;np<nsimp;np++){
    if(nonEvalStiffness)
      lambdakp1 = ipvprev->getThicknessStretch(np);
    else
      lambdakp1 = ipvcur->getThicknessStretch(np);
    double zksimp = ipvcur->getSimpsonPoint(np);
    bool convergence = false;
    for(int ite=0;ite<_itemax;ite++){
      lbcur->setBasisVectorG(np,zksimp,lambdakp1);
      this->stresst33(ips,np,t33,t33rel);
  #ifdef _DEBUG
      if(std::isnan(t33)){
        Msg::Error("NanValue for t33 iteration %d",ite);
        break;
      }
  #endif // _DEBUG
      if(fabs(t33rel) < _itetol && ite>0){
        ipvcur->setThicknessStretch(np,lambdakp1);
        convergence = true;
        break;
      }
      else{
    #if 0 // NR
        this->dg_ijDstretch(ips,np,_dg_ij);
        this->materialTensor33(ips,np,_C33ij);
        dkdl=0;
        for(int i=0;i<3;i++)
          for(int j=0;j<3;j++)
            dkdl += _C33ij[i][j]*_dg_ij[i][j];
        dkdl*=0.5; // because dkdl = C33ij*dg_ij/2
        double dl = -t33/dkdl;
    #else  // Dichotomie
        double dl = 0.01;
        if(ite>0)
        {
          double a = (t33-t33prec)/(lambdakp1-lambdak);
          dl = -t33/a;
        }
        lambdak = lambdakp1;
        t33prec = t33;
    #endif // 0 or 1
        lambdakp1+=dl;
      }
    }
    if(!convergence){
      ipvcur->setThicknessStretch(np,lambdakp1);
      lbcur->setLambda(ipvcur->getGlobalStretch());
    #ifdef _DEBUG
      Msg::Error("Max number iteration reached for ensure plane stress! Used value of %d iterations t33=%e t33rel=%e stiff=%d",_itemax,t33,t33rel,nonEvalStiffness);
    #else
      Msg::Error("Max number iteration reached for ensure plane stress! Used value of %d iterations t33=%e t33rel=%e stiff=%d",_itemax,t33,t33rel,nonEvalStiffness);
    #endif //_DEBUG
    }
  }
  // compute the resultant lambdah
  ipvcur->setGlobalStretch();
}

void dgNonLinearShellMaterialLaw::dg_ijDstretch(const IPStateBase *ips,const int npoint, double dg_ij[3][3]) const
{
  IPVariableShell *ipvcur = static_cast<IPVariableShell*>(ips->getState(IPStateBase::current));
  const nonLinearShellLocalBasisBulk *lbcur = static_cast<nonLinearShellLocalBasisBulk*>(ipvcur->getshellLocalBasis());
  const SVector3& g_1 = lbcur->basisVectorG1(npoint);
  const SVector3& g_2 = lbcur->basisVectorG2(npoint);
  const SVector3& g_3 = lbcur->basisVectorG3(npoint);
  const SVector3& t_1 = lbcur->basisDnormal(0);
  const SVector3& t_2 = lbcur->basisDnormal(1);
  const SVector3& t_  = lbcur->basisNormal();
  double xi3 = ipvcur->getSimpsonPoint(npoint);
  double twoxi3 = 2.*xi3;
  // Always symetric
  dg_ij[0][0]               = twoxi3*dot(t_1,g_1);
  dg_ij[1][1]               = twoxi3*dot(t_2,g_2);
  dg_ij[2][2]               = 2.*dot(t_,g_3);
  dg_ij[0][1] = dg_ij[1][0] = xi3*(dot(t_1,g_2)+ dot(t_2,g_1));
  dg_ij[0][2] = dg_ij[2][0] = xi3*dot(t_1,g_3) + dot(t_,g_1);
  dg_ij[1][2] = dg_ij[2][1] = xi3*dot(t_2,g_3) + dot(t_,g_2);
}

void dgNonLinearShellMaterialLaw::reduction(const IPStateBase *ips,std::vector<SVector3> &n_,
                           std::vector<SVector3> &mtilde_,bool fdg) const
{
  IPVariableShell *ipvcur = static_cast<IPVariableShell*>(ips->getState(IPStateBase::current));
  IPVariableShell *ipvinit= static_cast<IPVariableShell*>(ips->getState(IPStateBase::initial));
  const nonLinearShellLocalBasisBulk *lbcur = static_cast<nonLinearShellLocalBasisBulk*>(ipvcur->getshellLocalBasis());
  const nonLinearShellLocalBasisBulk *lbinit= static_cast<nonLinearShellLocalBasisBulk*>(ipvinit->getshellLocalBasis());
  short int nsimp_ = ipvcur->getNumSimp();
  std::vector<double> npcontrib1g1(nsimp_);
  std::vector<double> npcontrib2g1(nsimp_);
  std::vector<double> npcontrib3g1(nsimp_);
  std::vector<double> npcontrib1g2(nsimp_);
  std::vector<double> npcontrib2g2(nsimp_);
  std::vector<double> npcontrib3g2(nsimp_);
  std::vector<double> npcontrib1g3(nsimp_);
  std::vector<double> npcontrib2g3(nsimp_);
  std::vector<double> npcontrib3g3(nsimp_);
  const std::vector<double>& zsimp = ipvcur->getSimpsonPoint();
  const double thick0 = zsimp.back()-zsimp.front(); // h0 or h??
  const double oneDivjbarre = 1./lbcur->getJacobian();
  for(int np=0;np<nsimp_;np++){
    const tab6& kir = ipvcur->getSigma(np);
    const SVector3 &g1 = lbcur->dualBasisVectorG1(np);
    const SVector3 &g2 = lbcur->dualBasisVectorG2(np);
    const SVector3 &g3 = lbcur->dualBasisVectorG3(np);
    const double j0_ = lbinit->getJacobian(np);
    // mat vect prod of kirchhoff tensor with basis vector and time j0
    // g1
    npcontrib1g1[np] = j0_*(kir[0]*g1[0]+kir[3]*g1[1]+kir[5]*g1[2]);
    npcontrib2g1[np] = j0_*(kir[3]*g1[0]+kir[1]*g1[1]+kir[4]*g1[2]);
    npcontrib3g1[np] = j0_*(kir[5]*g1[0]+kir[4]*g1[1]+kir[2]*g1[2]);
    // g2
    npcontrib1g2[np] = j0_*(kir[0]*g2[0]+kir[3]*g2[1]+kir[5]*g2[2]);
    npcontrib2g2[np] = j0_*(kir[3]*g2[0]+kir[1]*g2[1]+kir[4]*g2[2]);
    npcontrib3g2[np] = j0_*(kir[5]*g2[0]+kir[4]*g2[1]+kir[2]*g2[2]);
    // g3
    npcontrib1g3[np] = j0_*(kir[0]*g3[0]+kir[3]*g3[1]+kir[5]*g3[2]);
    npcontrib2g3[np] = j0_*(kir[3]*g3[0]+kir[1]*g3[1]+kir[4]*g3[2]);
    npcontrib3g3[np] = j0_*(kir[5]*g3[0]+kir[4]*g3[1]+kir[2]*g3[2]);
  }
  n_.resize(3);
  mtilde_.resize(2);
  // simpson's integration group npcontrib (try to maximize cache hit)
  n_[0][0]      = oneDivjbarre*SimpsonIntegration(npcontrib1g1,thick0);
  mtilde_[0][0] = oneDivjbarre*SimpsonIntegration(npcontrib1g1,zsimp);
  mtilde_[0][1] = oneDivjbarre*SimpsonIntegration(npcontrib2g1,zsimp);
  n_[0][1]      = oneDivjbarre*SimpsonIntegration(npcontrib2g1,thick0);
  n_[0][2]      = oneDivjbarre*SimpsonIntegration(npcontrib3g1,thick0);
  mtilde_[0][2] = oneDivjbarre*SimpsonIntegration(npcontrib3g1,zsimp);
  n_[1][0]      = oneDivjbarre*SimpsonIntegration(npcontrib1g2,thick0);
  mtilde_[1][0] = oneDivjbarre*SimpsonIntegration(npcontrib1g2,zsimp);
  mtilde_[1][1] = oneDivjbarre*SimpsonIntegration(npcontrib2g2,zsimp);
  n_[1][1]      = oneDivjbarre*SimpsonIntegration(npcontrib2g2,thick0);
  n_[1][2]      = oneDivjbarre*SimpsonIntegration(npcontrib3g2,thick0);
  mtilde_[1][2] = oneDivjbarre*SimpsonIntegration(npcontrib3g2,zsimp);
  n_[2][0]      = oneDivjbarre*SimpsonIntegration(npcontrib1g3,thick0);
  n_[2][1]      = oneDivjbarre*SimpsonIntegration(npcontrib2g3,thick0);
  n_[2][2]      = oneDivjbarre*SimpsonIntegration(npcontrib3g3,thick0);

  // Rotation on interface (nhat rotation only 1 and 2, 3 is perpendicular?)
  if(ipvcur->isInterface()){
    SVector3 vhat1;
    SVector3 vhat2;
    if(fdg){
      vhat1 = n_[0];
      vhat2 = n_[1];
      n_[0] = lbcur->basisPushTensor(0,0)*vhat1 + lbcur->basisPushTensor(1,0)*vhat2;
      n_[1] = lbcur->basisPushTensor(0,1)*vhat1 + lbcur->basisPushTensor(1,1)*vhat2;
    }
    vhat1 = mtilde_[0];
    vhat2 = mtilde_[1];
    mtilde_[0] = lbcur->basisPushTensor(0,0)*vhat1 + lbcur->basisPushTensor(1,0)*vhat2;
    mtilde_[1] = lbcur->basisPushTensor(0,1)*vhat1 + lbcur->basisPushTensor(1,1)*vhat2; // BEAWARE T(alpha^-,alpha) = dot(phi^,a,phi_,a-) is inverted in MetaSheel
  }
}

neoHookeanShellLaw::neoHookeanShellLaw(const int num, const double E,
                                       const double nu, const double thick,
                                       const int nsimp,
                                       const double rho) : dgNonLinearShellMaterialLaw(num,thick,nsimp,rho,true),
                                                           _K0(E/(3.*(1.-2.*nu))), _G0(0.5*E/(1.+nu)),
                                                           _C11(E/((1.+nu)*(1.-nu))),_nu(nu), _K0minus2G0div3(_K0-2.*_G0/3.)
{

}
neoHookeanShellLaw::neoHookeanShellLaw(const neoHookeanShellLaw &source) : dgNonLinearShellMaterialLaw(source),
                                                                    _K0(source._K0), _G0(source._G0),
                                                                    _C11(source._C11), _nu(source._nu),
                                                                    _K0minus2G0div3(source._K0minus2G0div3)
{

}
void neoHookeanShellLaw::createIPState(IPStateBase* &ips,bool hasBodyForce, const bool* state_,const MElement *ele,
                              const int nbFF_, const IntPt *GP, const int gpt) const
{
  // check interface element
  bool inter=true;
  const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
  if(iele==NULL) inter=false;
  IPVariable* ipvi = new neoHookeanShellIPv(_h0,_nsimp,inter);
  IPVariable* ipv1 = new neoHookeanShellIPv(_h0,_nsimp,inter);
  IPVariable* ipv2 = new neoHookeanShellIPv(_h0,_nsimp,inter);
  if(ips != NULL) delete ips;
  ips = new IP3State(state_,ipvi,ipv1,ipv2);
}

void neoHookeanShellLaw::createIPVariable(IPVariable* &ipv, bool hasBodyForce, const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
  if(ipv !=NULL) delete ipv;
  bool inter=true;
  const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
  if(iele == NULL){
    inter=false;
  }
  ipv = new neoHookeanShellIPv(_h0,_nsimp,inter);
}

double neoHookeanShellLaw::soundSpeed() const
{
  return sqrt(2.*_G0*(1.-_nu)/(1.-2.*_nu)/_rho);
}
void neoHookeanShellLaw::stress(IPStateBase* ips, bool stiff, const bool checkfrac)
{
  IPVariableShell *ipvcur = static_cast<IPVariableShell*>(ips->getState(IPStateBase::current));
  IPVariableShell *ipvinit= static_cast<IPVariableShell*>(ips->getState(IPStateBase::initial));
  const nonLinearShellLocalBasisBulk *lbcur = static_cast<nonLinearShellLocalBasisBulk*>(ipvcur->getshellLocalBasis());
  const nonLinearShellLocalBasisBulk *lbinit= static_cast<nonLinearShellLocalBasisBulk*>(ipvinit->getshellLocalBasis());
  // loop on simpson's point (t33=0 is ensure before here during the computation of ipv)
  int nsimp_ = ipvcur->getNumSimp();
  double kcov[6]; // Symetric Matrix of Piola-Kirchoff stress in the convected bases where t33=0
  // for computation of strain energy
  std::vector<double> vEpsEnergy(nsimp_);
  for(int np=0; np<nsimp_;np++){
    const SVector3 &g01 = lbinit->dualBasisVectorG1(np);
    const SVector3 &g02 = lbinit->dualBasisVectorG2(np);
    const SVector3 &g03 = lbinit->dualBasisVectorG3(np);
    const SVector3 &g_1 = lbcur->basisVectorG1(np);
    const SVector3 &g_2 = lbcur->basisVectorG2(np);
    const SVector3 &g_3 = lbcur->basisVectorG3(np);
    const SVector3 &g1 = lbcur->dualBasisVectorG1(np);
    const SVector3 &g2 = lbcur->dualBasisVectorG2(np);
    const SVector3 &g3 = lbcur->dualBasisVectorG3(np);

    double g11 = dot(g1,g1);
    double g12 = dot(g1,g2);
    double g13 = dot(g1,g3);
    double g22 = dot(g2,g2);
    double g23 = dot(g2,g3);
    double g33 = dot(g3,g3);
    double g011 = dot(g01,g01);
    double g012 = dot(g01,g02);
    double g013 = dot(g01,g03);
    double g022 = dot(g02,g02);
    double g023 = dot(g02,g03);
    double g033 = dot(g03,g03);
    double j_ = lbcur->getJacobian(np);
    double j0_ = lbinit->getJacobian(np);
    double J = j_/j0_;
    double lnJ = log(J);
    double K0m2G0d3lnJmG0 = _K0minus2G0div3*lnJ - _G0;
    kcov[0] = K0m2G0d3lnJmG0*g11 + _G0*g011;
    kcov[1] = K0m2G0d3lnJmG0*g22 + _G0*g022;
    kcov[2] = 0.; // enforce strickly t33=0.
    kcov[3] = K0m2G0d3lnJmG0*g12 + _G0*g012;
    kcov[4] = K0m2G0d3lnJmG0*g23 + _G0*g023;
    kcov[5] = K0m2G0d3lnJmG0*g13 + _G0*g013;

    // get stress vector
    tab6& kirchhoff = ipvcur->getSigma(np);
    for(int i=0;i<6;i++) kirchhoff[i]=0.;
    // fill
    alphaTensprodSym(kcov[0],g_1,kirchhoff);
    alphaTensprodSym(kcov[1],g_2,kirchhoff);
    alphaTensprodSym(kcov[2],g_3,kirchhoff);
    alphaTensprodSym(kcov[3],g_1,g_2,kirchhoff);
    alphaTensprodSym(kcov[4],g_2,g_3,kirchhoff);
    alphaTensprodSym(kcov[5],g_1,g_3,kirchhoff);

    // strain energy (for archiving)
    double trC = dot(g_1,g_1)*g011 + dot(g_2,g_2)*g022 + dot(g_3,g_3)*g033
             + 2*dot(g_1,g_2)*g012 + 2*dot(g_1,g_3)*g013 + 2*dot(g_2,g_3)*g023;
    vEpsEnergy[np] = 0.5*_K0minus2G0div3*lnJ*lnJ - _G0 *lnJ + 0.5*_G0*(trC-3);
  }
  double strainEnergy = SimpsonIntegration(vEpsEnergy,ipvinit->getThickness());
  ipvcur->setStrainEnergy(strainEnergy);
}

// Initial value for compatibility and stability
// Do some other functions for current value
// here the initial ipv has to be given
void neoHookeanShellLaw::shellBendingStiffness(const IPVariableShell *ipv, LinearElasticShellHookeTensor &Hbend) const
{
  const nonLinearShellLocalBasisBulk *lb = static_cast<const nonLinearShellLocalBasisBulk*>(ipv->getshellLocalBasis());
  double h = ipv->getThickness();
  double bendfactor = h*h*h/12.*_C11;
  Hbend.set(lb,bendfactor,_nu);
}
void neoHookeanShellLaw::shellMembraneStiffness(const IPVariableShell *ipv, LinearElasticShellHookeTensor &Htens) const
{
  const nonLinearShellLocalBasisBulk *lb = static_cast<const nonLinearShellLocalBasisBulk*>(ipv->getshellLocalBasis());
  double h = ipv->getThickness();
  double membFactor = h*_C11;
  Htens.set(lb,membFactor,_nu);
}
void neoHookeanShellLaw::shellShearingStiffness(const IPVariableShell *ipv, linearElasticShellShearingHookeTensor &Hshear) const
{
  const nonLinearShellLocalBasisBulk *lb = static_cast<const nonLinearShellLocalBasisBulk*>(ipv->getshellLocalBasis());
  double h = ipv->getThickness();
  double shearFactor = h*_G0;
  Hshear.set(lb,shearFactor);
}

void neoHookeanShellLaw::materialTensor33(const IPStateBase *ips,const int npoint,double C33ij[3][3]) const
{
  IPVariableShell *ipvcur = static_cast<IPVariableShell*>(ips->getState(IPStateBase::current));
  IPVariableShell *ipvinit= static_cast<IPVariableShell*>(ips->getState(IPStateBase::initial));
  const nonLinearShellLocalBasisBulk *lbcur = static_cast<nonLinearShellLocalBasisBulk*>(ipvcur->getshellLocalBasis());
  const nonLinearShellLocalBasisBulk *lbinit= static_cast<nonLinearShellLocalBasisBulk*>(ipvinit->getshellLocalBasis());

  double j0_ = lbinit->getJacobian(npoint); // constant value by Simpson's point
  double j_ = lbcur->getJacobian(npoint);
  double J_ = j_/j0_;
  const SVector3 &g1 = lbcur->dualBasisVectorG1(npoint);
  const SVector3 &g2 = lbcur->dualBasisVectorG2(npoint);
  const SVector3 &g3 = lbcur->dualBasisVectorG3(npoint);
  double g33 = dot(g3,g3);
  double g11 = dot(g1,g1);
  double g22 = dot(g2,g2);
  double g12 = dot(g1,g2);
  double g13 = dot(g1,g3);
  double g23 = dot(g2,g3);
  double lnJ = log(J_);
  double K0G0lnJ = 2*_K0minus2G0div3*lnJ-_G0; // last term are 2 times the same as g3 appears in each dot product
  double K0minus2G0div3g33 = _K0minus2G0div3*g33;
  C33ij[0][0]               = K0minus2G0div3g33*g11 - K0G0lnJ*(g13*g13);
  C33ij[1][1]               = K0minus2G0div3g33*g22 - K0G0lnJ*(g23*g23);
  C33ij[0][1] = C33ij[1][0] = K0minus2G0div3g33*g12 - K0G0lnJ*(g13*g23);
  C33ij[0][2] = C33ij[2][0] = K0minus2G0div3g33*g13 - K0G0lnJ*(g33*g13);
  C33ij[1][2] = C33ij[2][1] = K0minus2G0div3g33*g23 - K0G0lnJ*(g33*g23);
  C33ij[2][2]               = (_K0minus2G0div3*(1.-2.*lnJ)+2*_G0)*(g33*g33);
}

void neoHookeanShellLaw::stresst33(const IPStateBase *ips,const int npoint, double &t33, double &t33rel) const
{
  IPVariableShell *ipvcur = static_cast<IPVariableShell*>(ips->getState(IPStateBase::current));
  IPVariableShell *ipvinit= static_cast<IPVariableShell*>(ips->getState(IPStateBase::initial));
  const nonLinearShellLocalBasisBulk *lbcur = static_cast<nonLinearShellLocalBasisBulk*>(ipvcur->getshellLocalBasis());
  const nonLinearShellLocalBasisBulk *lbinit= static_cast<nonLinearShellLocalBasisBulk*>(ipvinit->getshellLocalBasis());
  const SVector3 &g3 = lbcur->dualBasisVectorG3(npoint);
  const SVector3 &g03=lbinit->dualBasisVectorG3(npoint);
  double g33 = dot(g3,g3);
  double g033= dot(g03,g03);
  double j_ = lbcur->getJacobian(npoint);
  double j0_= lbinit->getJacobian(npoint);
  double lnJ = log(j_/j0_);

  t33 = (_K0minus2G0div3*lnJ-_G0)*g33+_G0*g033;
  t33rel= t33/(_K0+_G0);
}

J2linearShellLaw::J2linearShellLaw(const int num,const double thick,const int nsimp,const double rho,
                                   double E,const double nu,const double sy0,const double h,
                                   const double tol) : dgNonLinearShellMaterialLaw(num,thick,nsimp,rho,true),
                                                       _j2law(num,E,nu,rho,sy0,h,tol), _C11(E/((1-nu)*(1+nu))),
                                                       _ISTensor3(1.)
{
  _tolt33factor = 1./(_j2law.bulkModulus()+_j2law.shearModulus());
}

J2linearShellLaw::J2linearShellLaw(const int num,const double thick,const int nsimp,const double rho,
                                   double E,const double nu,J2IsotropicHardening &j2IH,
                                   const double tol) : dgNonLinearShellMaterialLaw(num,thick,nsimp,rho,true),
                                                     _j2law(num,E,nu,rho,j2IH,tol), _C11(E/((1-nu)*(1+nu))),
                                                     _ISTensor3(1.)
{
  _tolt33factor = 1./(_j2law.bulkModulus()+_j2law.shearModulus());
}

J2linearShellLaw::J2linearShellLaw(const J2linearShellLaw &source) : dgNonLinearShellMaterialLaw(source), _j2law(source._j2law),
                                                             _C11(source._C11), _tolt33factor(source._tolt33factor),
                                                             _ISTensor3(source._ISTensor3){}

void J2linearShellLaw::createIPState(IPStateBase* &ips,bool hasBodyForce, const bool* state_,const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
  // check interface element
  bool inter=true;
  const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
  if(iele==NULL) inter=false;
  IPVariable* ipvi = new J2linearShellIPv(_h0,_nsimp,_j2law,inter);
  IPVariable* ipv1 = new J2linearShellIPv(_h0,_nsimp,_j2law,inter);
  IPVariable* ipv2 = new J2linearShellIPv(_h0,_nsimp,_j2law,inter);
  if(ips != NULL) delete ips;
  ips = new IP3State(state_,ipvi,ipv1,ipv2);
}

void J2linearShellLaw::createIPVariable(IPVariable* &ipv,bool hasBodyForce,  const MElement *ele, const int nbFF_, const IntPt *GP, const int gpt) const
{
  if(ipv !=NULL) delete ipv;
  bool inter=true;
  const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
  if(iele == NULL){
    inter=false;
  }
  ipv = new J2linearShellIPv(_h0,_nsimp,_j2law,inter);
}


void J2linearShellLaw::stress(IPStateBase* ips, bool stiff, const bool checkfrac)
{
  /* get ipvariable */
  J2linearShellIPv* ipvcur;
  const J2linearShellIPv* ipvprev;
  const J2linearShellIPv* ipvinit;
  IPShellFractureCohesive* ipvtmp = dynamic_cast<IPShellFractureCohesive*>(ips->getState(IPStateBase::current));
  if(ipvtmp!=NULL)
  {
    ipvcur = static_cast<J2linearShellIPv*>(ipvtmp->getIPvBulk());
    const IPShellFractureCohesive *ipvtmp2 = static_cast<const IPShellFractureCohesive*>(ips->getState(IPStateBase::previous));
    ipvprev = static_cast<const J2linearShellIPv*>(ipvtmp2->getIPvBulk());
    ipvtmp2 = static_cast<const IPShellFractureCohesive*>(ips->getState(IPStateBase::initial));
    ipvinit = static_cast<const J2linearShellIPv*>(ipvtmp2->getIPvBulk());
  }
  else
  {
    ipvcur = static_cast<J2linearShellIPv*>(ips->getState(IPStateBase::current));
    ipvprev = static_cast<const J2linearShellIPv*>(ips->getState(IPStateBase::previous));
    ipvinit = static_cast<const J2linearShellIPv*>(ips->getState(IPStateBase::initial));
  }

  /* local basis */
  nonLinearShellLocalBasisBulk* lbcur = static_cast<nonLinearShellLocalBasisBulk*>(ipvcur->getshellLocalBasis());
  const nonLinearShellLocalBasisBulk* lbprev = static_cast<const nonLinearShellLocalBasisBulk*>(ipvprev->getshellLocalBasis());
  const nonLinearShellLocalBasisBulk* lbinit = static_cast<const nonLinearShellLocalBasisBulk*>(ipvinit->getshellLocalBasis());

  /* loop on Simpson's integration point accross the thickness */
  int nsimp_ = ipvcur->getNumSimp();
  for(int np=0; np<nsimp_;np++){
    /* current strain in shell basis */
    const fullVector<double> disp; // not used in strain function but we had to give one
    ipvcur->strain(disp,np,lbinit);

    /* strain xyz */
    const STensor3& F1 = ipvcur->deformationGradient(np);
    const STensor3& F0 = ipvprev->deformationGradient(np);

    /* data for J2 law */
    IPJ2linear* q1 = ipvcur->getIPJ2linear(np);
    const IPJ2linear* q0 = ipvprev->getIPJ2linear(np);

    /* compute stress */
    _j2law.constitutive(F0,F1,PKxyz,q0,q1,_tangent3D, stiff); // no need to init PKxyz
    // tau tensor
    multSTensor3SecondTranspose(PKxyz,F1,tau);
    tab6& taunp = ipvcur->getSigma(np);
    taunp[0] = tau(0,0);
    taunp[1] = tau(1,1);
    taunp[2] = tau(2,2);
    taunp[3] = tau(0,1);
    taunp[4] = tau(1,2);
    taunp[5] = tau(0,2);
  }
}

// linear elastic value ?? (or return elasto-plastic ??)
void J2linearShellLaw::shellBendingStiffness(const IPVariableShell *ipv, LinearElasticShellHookeTensor &Hbend) const
{
  const nonLinearShellLocalBasisBulk *lb = static_cast<const nonLinearShellLocalBasisBulk*>(ipv->getshellLocalBasis());
  double h = ipv->getThickness();
  double bendfactor = h*h*h/12.*_C11;
  Hbend.set(lb,bendfactor,_j2law.poissonRatio());
}
void J2linearShellLaw::shellMembraneStiffness(const IPVariableShell *ipv, LinearElasticShellHookeTensor &Htens) const
{
  const nonLinearShellLocalBasisBulk *lb = static_cast<const nonLinearShellLocalBasisBulk*>(ipv->getshellLocalBasis());
  double h = ipv->getThickness();
  double membFactor = h*_C11;
  Htens.set(lb,membFactor,_j2law.poissonRatio());
}
void J2linearShellLaw::shellShearingStiffness(const IPVariableShell *ipv, linearElasticShellShearingHookeTensor &Hshear) const
{
  const nonLinearShellLocalBasisBulk *lb = static_cast<const nonLinearShellLocalBasisBulk*>(ipv->getshellLocalBasis());
  double h = ipv->getThickness();
  double shearFactor = h*_j2law.shearModulus();
  Hshear.set(lb,shearFactor);
}

void J2linearShellLaw::materialTensor33(const IPStateBase *ips, const int npoint,double C33ij[3][3]) const
{
  /* get IPVariable */
  J2linearShellIPv* ipvcur;
  const J2linearShellIPv* ipvprev;
  const J2linearShellIPv* ipvinit;
  IPShellFractureCohesive* ipvtmp = dynamic_cast<IPShellFractureCohesive*>(ips->getState(IPStateBase::current));
  if(ipvtmp !=NULL)
  {
    ipvcur = static_cast<J2linearShellIPv*>(ipvtmp->getIPvBulk());
    const IPShellFractureCohesive *ipvtmp2 = static_cast<const IPShellFractureCohesive*>(ips->getState(IPStateBase::previous));
    ipvprev = static_cast<const J2linearShellIPv*>(ipvtmp2->getIPvBulk());
    ipvtmp2 = static_cast<const IPShellFractureCohesive*>(ips->getState(IPStateBase::initial));
    ipvinit = static_cast<const J2linearShellIPv*>(ipvtmp2->getIPvBulk());
  }
  else
  {
    ipvcur = static_cast<J2linearShellIPv*>(ips->getState(IPStateBase::current));
    ipvprev = static_cast<const J2linearShellIPv*>(ips->getState(IPStateBase::previous));
    ipvinit = static_cast<const J2linearShellIPv*>(ips->getState(IPStateBase::initial));
  }

  /* local basis */
  nonLinearShellLocalBasisBulk *lbcur = static_cast<nonLinearShellLocalBasisBulk*>(ipvcur->getshellLocalBasis());
  const nonLinearShellLocalBasisBulk *lbprev = static_cast<const nonLinearShellLocalBasisBulk*>(ipvprev->getshellLocalBasis());
  const nonLinearShellLocalBasisBulk *lbinit = static_cast<const nonLinearShellLocalBasisBulk*>(ipvinit->getshellLocalBasis());

  /* strain XYZ */
  const fullVector<double> disp; // not used in strain function but we had to give one
  ipvcur->strain(disp,npoint,lbinit);
  const STensor3& F1 = ipvcur->deformationGradient(npoint);

  /*  stress xyz */
  const tab6& taunp = ipvcur->getSigma(npoint);
  // tau -> first PK
  tau(0,0) = taunp[0];
  tau(1,1) = taunp[1];
  tau(2,2) = taunp[2];
  tau(0,1) = tau(1,0) = taunp[3];
  tau(1,2) = tau(2,1) = taunp[4];
  tau(0,2) = tau(2,0) = taunp[5];
  transposeInverseSTensor3(F1,invF);
  multSTensor3(tau,invF,PKxyz);

  /* data for J2 law */
  IPJ2linear* q1 = ipvcur->getIPJ2linear(npoint);
  const IPJ2linear* q0 = ipvprev->getIPJ2linear(npoint);

  /* plastic Tensor */
  Fp0 += q0->_j2lepsp; // != for j2linear q1 = q1j2linear + I
  Fp1 += q1->_j2lepsp; // != for j2linear q1 = q1j2linear + I

  /* compute tangent */
  _j2law.tangent_full(_tangent3D,PKxyz,F1,Fp0,Fp1,q0,q1);

  /* extract part 33 and put in shell local basis */
  const SVector3& g3 = lbcur->dualBasisVectorG3(npoint);
  for(int o=0;o<3;o++)
    for(int p=0;p<3;p++){
      C33ij[o][p] =0.;
      for(int i=0;i<3;i++)
        for(int j=0;j<3;j++)
          for(int k=0;k<3;k++)
            for(int l=0;l<3;l++)
              C33ij[o][p] += _tangent3D(i,j,k,l)*g3[i]*g3[j]*lbcur->dualBasisVectorG(npoint,o,k)*lbcur->dualBasisVectorG(npoint,p,l);
    }
}

// BE AWARE COPY-PASTE stress function
void J2linearShellLaw::stresst33(const IPStateBase *ips,const int npoint, double &t33, double &t33rel) const
{
  /* get ipvariable */
  J2linearShellIPv* ipvcur;
  const J2linearShellIPv* ipvprev;
  const J2linearShellIPv* ipvinit;
  IPShellFractureCohesive* ipvtmp = dynamic_cast<IPShellFractureCohesive*>(ips->getState(IPStateBase::current));
  if(ipvtmp!=NULL)
  {
    ipvcur = static_cast<J2linearShellIPv*>(ipvtmp->getIPvBulk());
    const IPShellFractureCohesive *ipvtmp2 = static_cast<const IPShellFractureCohesive*>(ips->getState(IPStateBase::previous));
    ipvprev = static_cast<const J2linearShellIPv*>(ipvtmp2->getIPvBulk());
    ipvtmp2 = static_cast<const IPShellFractureCohesive*>(ips->getState(IPStateBase::initial));
    ipvinit = static_cast<const J2linearShellIPv*>(ipvtmp2->getIPvBulk());
  }
  else
  {
    ipvcur = static_cast<J2linearShellIPv*>(ips->getState(IPStateBase::current));
    ipvprev = static_cast<const J2linearShellIPv*>(ips->getState(IPStateBase::previous));
    ipvinit = static_cast<const J2linearShellIPv*>(ips->getState(IPStateBase::initial));
  }

  /* local basis */
  nonLinearShellLocalBasisBulk* lbcur = static_cast<nonLinearShellLocalBasisBulk*>(ipvcur->getshellLocalBasis());
  const nonLinearShellLocalBasisBulk* lbprev = static_cast<const nonLinearShellLocalBasisBulk*>(ipvprev->getshellLocalBasis());
  const nonLinearShellLocalBasisBulk* lbinit = static_cast<const nonLinearShellLocalBasisBulk*>(ipvinit->getshellLocalBasis());

  /* current strain in shell basis */
  const fullVector<double> disp; // not used in strain function but we had to give one
  ipvcur->strain(disp,npoint,lbinit);

  /* strain xyz */
  const STensor3& F1 = ipvcur->deformationGradient(npoint);
  const STensor3& F0 = ipvprev->deformationGradient(npoint);

  /* data for J2 law */
  IPJ2linear* q1 = ipvcur->getIPJ2linear(npoint);
  const IPJ2linear* q0 = ipvprev->getIPJ2linear(npoint);

  /* compute stress */
  _j2law.constitutive(F0,F1,PKxyz,q0,q1,_tangent3D,false); // no need to init PKxyz
  /* tau tensor */
  multSTensor3SecondTranspose(PKxyz,F1,tauXYZ);
  // put in shell basis
  stressTensorXYZToShellbasis(npoint,tauXYZ,lbcur,tau);


  t33 = tau(2,2);
  t33rel = t33*_tolt33factor;
}
