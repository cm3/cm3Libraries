//
// C++ Interface: ipvariable
//
// Description: Class with definition of ipvarible for shell
//
//
// Author:  <Gauthier BECKER>, (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef SHELLIPVARIABLE_H_
#define SHELLIPVARIABLE_H_
#include "ipvariable.h"
#include "shellLocalBasis.h"
#include "dgC0ShellManageBvector.h"
#include "reduction.h"
#include "ipField.h"
// delete when clean
#include "LinearElasticShellHookeTensor.h"
// type array for store in vector
struct tab6{
  protected :
    double a[6];
  public :
    tab6(){for(int i=0;i<6;i++) a[i]=0.;}
		tab6(const tab6& src){
			for(int i=0;i<6;i++) a[i]=src.a[i];
		}
    ~tab6(){};
    tab6& operator= (tab6 source) {for(int i=0;i<6;i++) a[i]=source[i]; return *this;}
    double& operator[](const int i){return a[i];}
    double operator[](const int i)const {return a[i];}
    const double * data() const {return a;}
    double * data() {return a;}
};
class IPVariableShell : public IPVariableMechanics{
 public:
  IPVariableShell() : IPVariableMechanics(){} // an empty constructor is needed to build IPShellFractureCohesive!
  IPVariableShell(const IPVariableShell &source) : IPVariableMechanics(source){}
  IPVariableShell &operator = (const IPVariable &source){IPVariableMechanics::operator=(source);return *this;}
  virtual ~IPVariableShell(){}
  virtual double getThickness() const=0;
  virtual shellLocalBasis * getshellLocalBasis()=0;
  virtual const shellLocalBasis * getshellLocalBasis() const =0;
  virtual void setB(const std::vector<TensorialTraits<double>::GradType> &Grads,
                    const std::vector<TensorialTraits<double>::HessType> &Hess)=0;
  virtual const Bvector* getBvector() const=0;
  virtual Bvector* getBvector()=0;
  virtual shellLocalBasis* getshellLocalBasisOfInterface()=0;
  virtual void setshellLocalBasisOfInterface(const shellLocalBasis *lb)=0;
  virtual const shellLocalBasis* getshellLocalBasisOfInterface() const=0;
  virtual void strain(const fullVector<double> &disp, const int np=-1,const shellLocalBasisBulk* lbinit=NULL)=0;
  virtual const bool isInterface() const=0;
  virtual double getSigma(const int i,const int j) const=0;
  virtual double getEpsilon(const int i,const int j)const=0;
  virtual tab6& getSigma(const int i)=0;
  virtual const tab6& getSigma(const int i) const=0;
  virtual tab6& getEpsilon(const int i)=0;
  virtual const double getSimpsonPoint(const int i) const=0;
  virtual const std::vector<double>& getSimpsonPoint() const=0;
  virtual short int getNumSimp() const=0;
  virtual void getUpperStressTensorBrokenEval(stressTensor &sigma_)const=0;
  virtual void getLowerStressTensorBrokenEval(stressTensor &sigma_)const=0;
  // just for non linear for now but this form of storage for strain energy may be used in linear too ?
  // because the values exist but are known so no need to store
  virtual double getGlobalStretch() const=0; // global thickness stretch (obtained by thickness integration)
  virtual double getThicknessStretch(const int i) const=0;
  virtual const std::vector<double>* getThicknessStretch() const=0; // change this because it forces the storage of lambdah
  virtual void  setThicknessStretch(const int np,const double l)=0;
  virtual void setGlobalStretch()=0; // has to be called at the end of ensurePlaneStress
  virtual void setStrainEnergy(const double se)=0;
  // Archiving data
  virtual double vonMises(const int pos) const=0;
  virtual double get(const int i) const=0;
  virtual double defoEnergy() const=0;
  virtual double plasticEnergy() const=0;
  // restart
  virtual void restart(){IPVariableMechanics::restart();}
};

// for linear shell with thickness integration
class IPLinearShell : public IPVariableShell{
 protected :
  linearShellLocalBasisBulk _lb;
  linearShellLocalBasisInter *_lbs; // shellLocalBasis of interface // has to be allocated on interface only
  double _h;
  Bvector _B;
  std::vector<tab6> sigma;
  std::vector<tab6> epsilon; // eps + xi^3*rho
  std::vector<double> zsimp;
  short int nsimp; // Give the number of Simpson's points avoid vect.size()
  bool _oninter;
 public :
  // constructor
  IPLinearShell(const double h,const int nbFF,const short int ns,const bool inter);
  virtual ~IPLinearShell(){
    if(_lbs!=NULL)
      delete _lbs;
  }
  IPLinearShell& operator=(const IPVariable &source);
  IPLinearShell(const IPLinearShell& source);

  // get operation
  virtual short int getNumSimp() const{return nsimp;}
  double getSigma(const int i,const int j) const {return sigma[i][j];}
  double getEpsilon(const int i,const int j)const {return epsilon[i][j];}
  double getSigmaNeutral(const int i) const
  {
  #ifdef _DEBUG
    if(nsimp == 1) Msg::Debug("Use function with only one Simpson's point. See IPLinearShell");
  #endif
    return sigma[nsimp/2][i];
  }
  double getEpsilonNeutral(const int i) const
  {
    #ifdef _DEBUG
      if(nsimp == 1) Msg::Debug("Use function with only one Simpson's point. See IPLinearShell");
    #endif
    return epsilon[nsimp/2][i];
  }
  double getSigmaUpper(const int i) const
  {
    #ifdef _DEBUG
      if( nsimp == 1)Msg::Debug("Use function with only one Simpson's point. See IPLinearShell");
    #endif
    return sigma[nsimp-1][i];
  }
  double getEpsilonUpper(const int i) const
  {
    #ifdef _DEBUG
      if(nsimp == 1) Msg::Debug("Use function with only one Simpson's point. See IPLinearShell");
    #endif
    return epsilon[nsimp-1][i];
  }
  double getSigmaLower(const int i) const
  {
    #ifdef _DEBUG
      if(nsimp == 1) Msg::Debug("Use function with only one Simpson's point. See IPLinearShell");
    #endif
    return sigma[0][i];
  }
  double getEpsilonLower(const int i) const
  {
    #ifdef _DEBUG
      if(nsimp == 1) Msg::Debug("Use function with only one Simpson's point. See IPLinearShell");
    #endif
    return epsilon[0][i];
  }
  virtual void getUpperStressTensorBrokenEval(stressTensor &sigma_)const;
  virtual void getLowerStressTensorBrokenEval(stressTensor &sigma_)const;
  virtual tab6& getSigma(const int i){ // if only 1 simpson point i=0 Membrane i=1 Bending
    return sigma[i];
  }
  virtual const tab6& getSigma(const int i) const{ // if only 1 simpson point i=0 Membrane i=1 Bending
    return sigma[i];
  }
  virtual tab6& getEpsilon(const int i){ // if only 1 simpson point i=0 Membrane i=1 Bending
    return epsilon[i];
  }
  virtual const double getSimpsonPoint(const int i) const{return zsimp[i];}
  virtual const std::vector<double>& getSimpsonPoint() const{return zsimp;}

  virtual double getThickness() const {return _h;}
  virtual shellLocalBasis * getshellLocalBasis(){return &_lb;}
  virtual const shellLocalBasis * getshellLocalBasis() const {return &_lb;}
  virtual void setB(const std::vector<TensorialTraits<double>::GradType> &Grads,
                    const std::vector<TensorialTraits<double>::HessType> &Hess){_B.set(&_lb,Grads,Hess);}
  virtual const Bvector* getBvector() const{ return &_B;}
  virtual Bvector* getBvector() { return &_B;}
  virtual const shellLocalBasis* getshellLocalBasisOfInterface() const
  {
    #ifdef _DEBUG
      if(_lbs == NULL) Msg::Debug("Use an empty shellLocalBasis of Interface");
    #endif
    return _lbs;
  }
  virtual shellLocalBasis* getshellLocalBasisOfInterface();
  virtual void setshellLocalBasisOfInterface(const shellLocalBasis *lb);
  virtual void strain(const fullVector<double> &disp,const int npoint=-1,const shellLocalBasisBulk* lbinit=NULL);
  virtual const bool isInterface() const {return _oninter;}
  virtual double getGlobalStretch() const{return 1.;} // global thickness stretch (obtained by thickness integration)
  virtual double getThicknessStretch(const int i) const{return 1.;}
  virtual const std::vector<double>* getThicknessStretch() const
  {
    #ifdef _DEBUG
    Msg::Debug("Thickness Stretch is not store by default for linear case. So return a NULL vector's address");
    #endif //_DEBUG
    return NULL;
  } // no storage on vector form
  virtual void  setThicknessStretch(const int np,const double l)
  {
    #ifdef _DEBUG
    Msg::Debug("Thickness Stretch is not store by default for linear case (because its value (=1) is known");
    #endif // _DEBUG
  }
  virtual void setGlobalStretch()
  {
    #ifdef _DEBUG
    Msg::Debug("Thickness Stretch is not store by default for linear case (because its value (=1) is known");
    #endif // _DEBUG
  }
  virtual void setStrainEnergy(const double se)
  {
    #ifdef _DEBUG
    Msg::Debug("setStrainEnergy does nothing in linear");
    #endif // _DEBUG
  }
  virtual double vonMises(const int pos) const;
  virtual double get(const int i) const;
  virtual double defoEnergy() const;
  virtual double plasticEnergy() const{return 0.;}
  virtual IPVariable* clone() const {return new IPLinearShell(*this);};
  virtual void restart();
};

// To take into account damage
class IPLinearShellWD: public IPLinearShell{
 protected:
 std::vector<double> _D;
 std::vector<double> Bifurcation;
 double D0;
 public:
  IPLinearShellWD(const double h,const int nbFF,const short int numsimp,const double d0,const bool inter);
  virtual ~IPLinearShellWD(){}
  IPLinearShellWD(const IPLinearShellWD &source);
  IPLinearShellWD &operator = (const IPVariable &source);
  // Get Damage value for stiffness mass computation
  virtual double getDamage(const int i=0) const {return _D[i];}
  virtual double& getDamage(const int i=0){return _D[i];}
  virtual double& getBifurcation(const int i=0) {return Bifurcation[i];}
  virtual double get(const int i)const;
  virtual void restart();
};

class IPLinearOrthotropicShell : public IPLinearShell
{
 protected:
  LinearElasticShellHookeTensor Htens;
  bool initHtens;
 public:
  IPLinearOrthotropicShell(const double h,const int nbFF,const short int numsimp,const bool inter);
  ~IPLinearOrthotropicShell(){}
  IPLinearOrthotropicShell(const IPLinearOrthotropicShell &source);
  IPLinearOrthotropicShell &operator = (const IPVariable &source);
  virtual bool& getHooke(LinearElasticShellHookeTensor* &hooke){hooke = &Htens; return initHtens;}
  virtual const LinearElasticShellHookeTensor& getHooke() const{return Htens;}
  virtual void restart();
};

class IPLinearCohesiveShell : public IPVariableMechanics{
 protected :
  // variables use by cohesive law
  reductionElement n0, m0, _n, _m; // _n and _m are needed to compute energy (access to previous time step value)
  bool tension;
  double unjump, rnjump, unjump0, rnjump0, delta,deltan, deltat, deltac, deltan0, deltat0, deltamax, sigmac, etaI;
  double utjump, rtjump, utjump0, rtjump0,sigI,tauII, etaII;
  double heqI,heqII;
  double beta; // Normally in material Law
  double lengthphi1,lengthphi2; // for energy
 public :
  double _energyMembrane,_energyBending;
  // no initial fracture (for initial fracture use ipfield)
  IPLinearCohesiveShell();// no initial fracture (for initial fracture use ipfield)
  ~IPLinearCohesiveShell(){}
  IPLinearCohesiveShell(const IPLinearCohesiveShell &source);
  virtual IPLinearCohesiveShell &operator = (const IPVariable &source);
  virtual void setFracture(const IPLinearCohesiveShell *ipvprev,const SVector3 &ujump_, const double rjump_[3],
                   const shellLocalBasis *lbs, const double h);
  void initfrac(const SVector3 &ujump_, const double rjump_[3],const shellLocalBasis *lbs, const double h_);
  virtual void initmean(IPLinearCohesiveShell *ipv2);
  void setnm(const reductionElement &n, const reductionElement &m);
  // get variable
  double getDeltac() const{return deltac;}
  double getDeltan() const {return deltan;}
  double getDeltat() const {return deltat;}
  const reductionElement& getm0() const{return m0;}
  const reductionElement& getn0() const{return n0;}
  const reductionElement& getm() const{return _m;}
  const reductionElement& getn() const{return _n;}
  reductionElement& getm0() {return m0;}
  reductionElement& getn0() {return n0;}
  reductionElement& getm() {return _m;}
  reductionElement& getn() {return _n;}
  double getDeltamax() const{return deltamax;}
  double getDelta() const{return delta;}
  bool ifTension() const{return tension;}
  double getEtaI() const{return etaI;}
  double getEtaII() const{return etaII;}
  double getBeta() const {return beta;}
  double getThickeqI() const{return heqI;}
  double getThickeqII()const{return heqII;}
  double getunj0() const {return unjump0;}
  double getutj0() const {return utjump0;}
  double getrnj0() const {return rnjump0;}
  double getrtj0() const {return rtjump0;}
  double getDeltaun() const{return unjump-unjump0;}
  double getDeltaut() const{return utjump-utjump0;}
  double getDeltarn() const{return rnjump-rnjump0;}
  double getDeltart() const{return rtjump-rtjump0;}
  const double getSigmac() const {return sigmac;}
  const double getSigI() const{return sigI;}
  const double getTauII() const{return tauII;}
  const double getNormPhi1() const{return lengthphi1;}
  const double getNormPhi2() const{return lengthphi2;}
  void setDataFromLaw(const double smax, const double sI, const double tII, const double Gc, const double beta_, const bool ift)
  {
    sigmac = smax;
    sigI = sI;
    tauII= tII;
    deltac = 2*Gc/sigmac;
    beta = beta_;
    tension = ift;
  }
  virtual int fractureEnergy(double* arrayEnergy) const
  {
    arrayEnergy[0] = _energyMembrane+_energyBending;
    arrayEnergy[1] = _energyMembrane;
    arrayEnergy[2] = _energyBending;
    return 3;
  }
  virtual IPVariable* clone() const {return new IPLinearCohesiveShell(*this);};
  virtual void restart();
};

class IPLinearCohesiveShellWithOrientation : public IPLinearCohesiveShell
{
protected:
    double _rotation_angle;
    double _effGc; //here Gc can change Added by Shantanu
public:
    IPLinearCohesiveShellWithOrientation();
    IPLinearCohesiveShellWithOrientation(const double &_rot_angle);
    ~IPLinearCohesiveShellWithOrientation(){}
    IPLinearCohesiveShellWithOrientation(const IPLinearCohesiveShellWithOrientation &source);
    void set_rotation_angle(const double &_angle);
    double get_rotation_angle() const;
    void setDataFromLaw(const double smax, const double sI, const double tII, const double Gc, const double beta_, const bool ift,
                        const double angle_);
    virtual void setFracture(const IPLinearCohesiveShell*ipvprev,const SVector3 &ujump_,
                             const double rjump_[3], const shellLocalBasis *lbs, const double h);
    virtual void initmean(IPLinearCohesiveShell *ipv2);
    virtual double getEffGc() const {return _effGc;}
    virtual IPLinearCohesiveShellWithOrientation &operator = (const IPVariable &source);
    virtual IPVariable* clone() const {return new IPLinearCohesiveShellWithOrientation(*this);};
    virtual void restart();
} ;


class IPShellFractureCohesive : public IPVariable2ForFracture<IPVariableShell,IPLinearCohesiveShell>
{
 protected:
  double _facSigmac;
  double _initTime; // time of intialization of fracture
 public:
  IPShellFractureCohesive(const double facSigmac=1.);
  IPShellFractureCohesive(const IPShellFractureCohesive &source);
  virtual IPShellFractureCohesive& operator=(const IPVariable &source);
  ~IPShellFractureCohesive(){}
  virtual bool isbroken() const {return IPVariable2ForFracture<IPVariableShell,IPLinearCohesiveShell>::isbroken();}
  virtual const double fractureStrengthFactor() const{return _facSigmac;}
  virtual void setFractureStrengthFactor(const double fsf){_facSigmac = fsf;}
  virtual const double getInitializationTime() const{return _initTime;}
  virtual void setInitializationTime(const double t){_initTime = t;}
  // Method of IPVariableShell use ipvariableshell here
  virtual double getThickness() const{return _ipvbulk->getThickness();}
  virtual shellLocalBasis * getshellLocalBasis(){return _ipvbulk->getshellLocalBasis();}
  virtual const shellLocalBasis * getshellLocalBasis() const {return _ipvbulk->getshellLocalBasis();}
  virtual void setB(const std::vector<TensorialTraits<double>::GradType> &Grads,
                    const std::vector<TensorialTraits<double>::HessType> &Hess){_ipvbulk->setB(Grads,Hess);}
  virtual const Bvector* getBvector() const{return _ipvbulk->getBvector();};
  virtual Bvector* getBvector(){return _ipvbulk->getBvector();}
  virtual shellLocalBasis* getshellLocalBasisOfInterface(){return _ipvbulk->getshellLocalBasisOfInterface();}
  virtual const shellLocalBasis* getshellLocalBasisOfInterface() const {return _ipvbulk->getshellLocalBasisOfInterface();}
  virtual void setshellLocalBasisOfInterface(const shellLocalBasis *lb){_ipvbulk->setshellLocalBasisOfInterface(lb);}
  virtual const bool isInterface() const{return _ipvbulk->isInterface();}
  virtual void strain(const fullVector<double> &disp,const int npoint=-1,const shellLocalBasisBulk *lbinit=NULL){_ipvbulk->strain(disp,npoint,lbinit);}
  virtual double getSigma(const int i,const int j) const{return _ipvbulk->getSigma(i,j);}
  virtual double getEpsilon(const int i,const int j)const{return _ipvbulk->getEpsilon(i,j);}
  virtual tab6& getSigma(const int i){return _ipvbulk->getSigma(i);}
  virtual const tab6& getSigma(const int i) const {return _ipvbulk->getSigma(i);}
  virtual tab6& getEpsilon(const int i){return _ipvbulk->getEpsilon(i);}
  virtual const double getSimpsonPoint(const int i) const{return _ipvbulk->getSimpsonPoint(i);}
  virtual const std::vector<double>& getSimpsonPoint() const{return _ipvbulk->getSimpsonPoint();}
  virtual short int getNumSimp() const{return _ipvbulk->getNumSimp();}
  virtual double getGlobalStretch() const{return _ipvbulk->getGlobalStretch();}
  virtual double getThicknessStretch(const int i) const{return _ipvbulk->getThicknessStretch(i);}
  virtual const std::vector<double>* getThicknessStretch() const{return _ipvbulk->getThicknessStretch();}
  virtual void  setThicknessStretch(const int np,const double l){_ipvbulk->setThicknessStretch(np,l);}
  virtual void setGlobalStretch(){_ipvbulk->setGlobalStretch();}
  virtual void setStrainEnergy(const double se){_ipvbulk->setStrainEnergy(se);}
  virtual double vonMises(const int pos) const{return _ipvbulk->vonMises(pos);}
  virtual double get(const int i) const{return _ipvbulk->get(i);}
  virtual double defoEnergy() const{return _ipvbulk->defoEnergy();}
  virtual double plasticEnergy() const{return _ipvbulk->plasticEnergy();}
  virtual int fractureEnergy(double* arrayEnergy) const{return _ipvfrac->fractureEnergy(arrayEnergy);} // called only if _broken==true
  virtual void getUpperStressTensorBrokenEval(stressTensor &sigma_)const{_ipvbulk->getUpperStressTensorBrokenEval(sigma_);}
  virtual void getLowerStressTensorBrokenEval(stressTensor &sigma_)const{_ipvbulk->getLowerStressTensorBrokenEval(sigma_);}
 #if defined(HAVE_MPI)
  virtual int numberValuesToCommunicateMPI()const{return 1;}
  // has to return the number of values exactly as the previous one
  virtual void fillValuesToCommunicateMPI(double *arrayMPI)const{arrayMPI[0] = _facSigmac;}
  virtual void getValuesFromMPI(const double *arrayMPI){_facSigmac = arrayMPI[0];}
 #endif // HAVE_MPI
  virtual IPVariable* clone() const {return new IPShellFractureCohesive(*this);};
  virtual void restart();
};
#endif //IPVARIABLESHELL
