//
// C++ Interface: terms
//
// Description: Elementary matrix terms for C0 Dg Shell
//
//
// Author:  <Gauthier BECKER>, (C) 2010
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "dgC0ShellTerms.h"
#include "dgC0ShellElementaryTerms.h" // contains some function used in this file to compute elementary matrix
#include "nlsFunctionSpace.h"
void massC0DgShell::get(MElement *ele,int npts,IntPt *GP,fullMatrix<double> &m) const{
  // For 6 nodes triangle the traditional integration does not work
  // the weight of 6 nodes has to be prescribed 1/12 at corner and 1/4 for middle nodes
  if(ele->getTypeForMSH() == MSH_TRI_6){
    // integration on Gauss' points to know area
    double area = 0.;
    double onediv12 = 1./12.;
    nbdof = BilinearTerm<double,double>::space1.getNumKeys(ele);
    nbFF = nbdof/3;
    m.resize(nbdof, nbdof,true); // true --> setAll 0.

    // get the shape function (and derivative values)
    nlsFunctionSpace<double>* sp1 = static_cast<nlsFunctionSpace<double>*>(&(this->space1));
    std::vector<GaussPointSpaceValues<double>*> vgps;
    sp1->get(ele,npts,GP,vgps);

    for (int i = 0; i < npts; i++)
    {
      weight = GP[i].weight;
      std::vector<TensorialTraits<double>::HessType> &Hess = vgps[i]->_vhess;
      std::vector<TensorialTraits<double>::GradType> &Grad = vgps[i]->_vgrads;
      lb.set(ele,Grad,Hess);
      detJ = lb.getJacobian();
      area+=weight*detJ;
    }
    double massele = area*_rho; // rho is the density by unit thickness
    // mass in three directions
    for(int kk=0;kk<3;kk++){
       m(kk*nbFF,kk*nbFF) += onediv12*massele;
       m(1+kk*nbFF,1+kk*nbFF) += onediv12*massele;
       m(2+kk*nbFF,2+kk*nbFF) += onediv12*massele;
       m(3+kk*nbFF,3+kk*nbFF) += 0.25*massele;
       m(4+kk*nbFF,4+kk*nbFF) += 0.25*massele;
       m(5+kk*nbFF,5+kk*nbFF) += 0.25*massele;
    }
  }
  // Other cqse for which integration does not work. 2nd order incomplete element.
  else if(ele->getTypeForMSH() == MSH_QUA_8)
    {
    const double ratioCornerNode = 3./76.;
    const double ratioMidNode = 16./76.;
    // integration on Gauss' points to know area
    double area = 0.;
    nbdof = BilinearTerm<double,double>::space1.getNumKeys(ele);
    nbFF = nbdof/3;
    m.resize(nbdof, nbdof,true); // true --> setAll 0.

    // get the shape function (and derivative values)
    nlsFunctionSpace<double>* sp1 = static_cast<nlsFunctionSpace<double>*>(&(this->space1));
    std::vector<GaussPointSpaceValues<double>*> vgps;
    sp1->get(ele,npts,GP,vgps);

    for (int i = 0; i < npts; i++)
    {
      weight = GP[i].weight;
      std::vector<TensorialTraits<double>::HessType> &Hess = vgps[i]->_vhess;
      std::vector<TensorialTraits<double>::GradType> &Grad = vgps[i]->_vgrads;
      lb.set(ele,Grad,Hess);
      detJ = lb.getJacobian();
      area+=weight*detJ;
    }
    double massele = area*_rho; // rho is the density by unit thickness
    // mass in three directions
    for(int kk=0;kk<3;kk++){
       m(kk*nbFF,kk*nbFF) += ratioCornerNode*massele;
       m(1+kk*nbFF,1+kk*nbFF) += ratioCornerNode*massele;
       m(2+kk*nbFF,2+kk*nbFF) += ratioCornerNode*massele;
       m(3+kk*nbFF,3+kk*nbFF) += ratioCornerNode*massele;
       m(4+kk*nbFF,4+kk*nbFF) += ratioMidNode*massele;
       m(5+kk*nbFF,5+kk*nbFF) += ratioMidNode*massele;
       m(6+kk*nbFF,6+kk*nbFF) += ratioMidNode*massele;
       m(7+kk*nbFF,7+kk*nbFF) += ratioMidNode*massele;
    }
  }
  else if(ele->getType() == TYPE_TRI && ele->getPolynomialOrder()>3)
  {

    // integration on Gauss' points to know area
    double area = 0.;
    nbdof = BilinearTerm<double,double>::space1.getNumKeys(ele);
    nbFF = nbdof/3;
    m.resize(nbdof, nbdof,true); // true --> setAll 0.

    // get the shape function (and derivative values)
    nlsFunctionSpace<double>* sp1 = static_cast<nlsFunctionSpace<double>*>(&(this->space1));
    std::vector<GaussPointSpaceValues<double>*> vgps;
    sp1->get(ele,npts,GP,vgps);
    // Use the "Adrian Rosolen" method to compute the weight associated to each nodes for the lumped mass
    std::vector<double> weight_mass_node;
    CalculateTriangleWeightNodeLumpedMassMatrix(ele->getPolynomialOrder(),weight_mass_node);
    // Compute the mass of the element
    for (int i = 0; i < npts; i++)
    {
      weight = GP[i].weight;
      std::vector<TensorialTraits<double>::HessType> &Hess = vgps[i]->_vhess;
      std::vector<TensorialTraits<double>::GradType> &Grad = vgps[i]->_vgrads;
      lb.set(ele,Grad,Hess);
      detJ = lb.getJacobian();
      area+=weight*detJ;
    }
    double massele = area*_rho; // rho is the density by unit thickness

    for(int i = 0; i < nbFF; i++)
      for(int k = 0; k < 3; k++)
          m(i+k*nbFF,i+k*nbFF) = massele*weight_mass_node[k];
  }
  else{
    if(sym){
      // Initialization of some data
      nbdof = BilinearTerm<double,double>::space1.getNumKeys(ele);
      nbFF = nbdof/3;
      m.resize(nbdof, nbdof,true); // true --> setAll 0.

      // get the shape function (and derivative values)
      nlsFunctionSpace<double>* sp1 = static_cast<nlsFunctionSpace<double>*>(&(this->space1));
      std::vector<GaussPointSpaceValues<double>*> vgps;
      sp1->get(ele,npts,GP,vgps);
      // sum on Gauss' points
      for (int i = 0; i < npts; i++)
      {
        // Coordonate of Gauss' point i
        //u = GP[i].pt[0]; v = GP[i].pt[1]; w = GP[i].pt[2];
        // Weight of Gauss' point i
        weight = GP[i].weight; //const double detJ2 = ele->getJacobian(u, v, w, jac);
//        Vals.clear(); Grad.clear(); Hess.clear();
//        BilinearTerm<double,double>::space1.f(ele,u, v, w, Vals);
        std::vector<TensorialTraits<double>::ValType> &Vals  = vgps[i]->_vvals;
        std::vector<TensorialTraits<double>::HessType> &Hess = vgps[i]->_vhess;
        std::vector<TensorialTraits<double>::GradType> &Grad = vgps[i]->_vgrads;
//        BilinearTerm<double,double>::space1.gradf(ele,u, v, w, Grad);
//        BilinearTerm<double,double>::space1.hessf(ele,u, v, w, Hess);
        // Value of jacobian
        lb.set(ele,Grad,Hess);
        detJ = lb.getJacobian();
        for(int j=0;j<nbFF; j++)
          for(int k=0;k<nbFF;k++){
            // same mass in the 3 directions
            mij = detJ*weight*_rho*Vals[j]*Vals[k]; // _rho is the mass by thickness unit
            for(int kk=0;kk<3;kk++)
              m(j+kk*nbFF,k+kk*nbFF) += mij;
          }
      }
 //     m.print("mass");
   }
   else
     Msg::Error("Mass matrix is not implemented for dg shell with two different function spaces");
  }
 #ifdef _DEBUG
  for(int i=0;i<nbdof;i++)
    for(int j=0;j<nbdof;j++)
      if(std::isnan(m(i,j)))
        Msg::Error("here");
 #endif // _DEBUG
}

void IsotropicStiffBulkTermC0DgShell::get(MElement *ele,int npts,IntPt *GP,fullMatrix<double> &m) const
{
  if (sym)
  {
    // Initialization of some data
    nbdof = BilinearTerm<double,double>::space1.getNumKeys(ele);
    nbFF = nbdof/3;
    m.resize(nbdof, nbdof,true); // true --> setAll(0.)
    mbuild1.resize(4,nbdof,false); // set after

    // get ipvariables
    ipf->getIPv(ele,vipv);

    // sum on Gauss' points
    for (int i = 0; i < npts; i++)
    {
      // Coordonate of Gauss' point i
      //u = GP[i].pt[0]; v = GP[i].pt[1]; w = GP[i].pt[2];
      // Weight of Gauss' point i and Jacobian's value at this point
      weight = GP[i].weight;

      const shellLocalBasis *lb = vipv[i]->getshellLocalBasis();
      const Bvector *B = vipv[i]->getBvector();

      // multiplication of constant by detJ and weight
      wJ = lb->getJacobian() * weight;

      // compute of Hooke tensor
      _mlaw->shellBendingStiffness(vipv[i],Hbend);
      _mlaw->shellMembraneStiffness(vipv[i],Htens);

      // bending
      mbuild3(0,0) = Hbend(0,0,0,0);
      mbuild3(0,1) = Hbend(0,0,0,1);
      mbuild3(0,2) = Hbend(0,0,1,0);
      mbuild3(0,3) = Hbend(0,0,1,1);
      mbuild3(1,0) = Hbend(0,1,0,0);
      mbuild3(1,1) = Hbend(0,1,0,1);
      mbuild3(1,2) = Hbend(0,1,1,0);
      mbuild3(1,3) = Hbend(0,1,1,1);
      mbuild3(2,0) = Hbend(1,0,0,0);
      mbuild3(2,1) = Hbend(1,0,0,1);
      mbuild3(2,2) = Hbend(1,0,1,0);
      mbuild3(2,3) = Hbend(1,0,1,1);
      mbuild3(3,0) = Hbend(1,1,0,0);
      mbuild3(3,1) = Hbend(1,1,0,1);
      mbuild3(3,2) = Hbend(1,1,1,0);
      mbuild3(3,3) = Hbend(1,1,1,1);

      mbuild1.gemm(mbuild3,B->getBending(),wJ,0);
      m.gemm(B->getBending(),mbuild1,1.,1.,true);

      // membrane
      mbuild3(0,0) = Htens(0,0,0,0);
      mbuild3(0,1) = Htens(0,0,0,1);
      mbuild3(0,2) = Htens(0,0,1,0);
      mbuild3(0,3) = Htens(0,0,1,1);
      mbuild3(1,0) = Htens(0,1,0,0);
      mbuild3(1,1) = Htens(0,1,0,1);
      mbuild3(1,2) = Htens(0,1,1,0);
      mbuild3(1,3) = Htens(0,1,1,1);
      mbuild3(2,0) = Htens(1,0,0,0);
      mbuild3(2,1) = Htens(1,0,0,1);
      mbuild3(2,2) = Htens(1,0,1,0);
      mbuild3(2,3) = Htens(1,0,1,1);
      mbuild3(3,0) = Htens(1,1,0,0);
      mbuild3(3,1) = Htens(1,1,0,1);
      mbuild3(3,2) = Htens(1,1,1,0);
      mbuild3(3,3) = Htens(1,1,1,1);

      mbuild1.gemm(mbuild3,B->getMembrane(),wJ,0);
      m.gemm(B->getMembrane(),mbuild1,1.,1.,true);
    }
//   m.print("bulk");
  }
  else
    printf("not implemented\n");
}

void IsotropicForceBulkTermC0DgShell::get(MElement *ele,int npts,IntPt *GP,fullVector<double> &m) const
{
  // Initialization of some data
  nbdof = DgC0LinearTerm<double>::space1.getNumKeys(ele);
  nbFF = nbdof/3;
  m.resize(nbdof);
  m.scale(0.);

  // resize matrix normally the realocation is limited if element are the same...
  m1.resize(1,6*npts,false); // set operation --> no need to setAll 0 here
  m2.resize(6*npts,nbdof,false); // set operation --> no need to setAll 0 here

  // get gauss' point data
  const AllIPState::ipstateElementContainer* vips = ipf->getAips()->getIPstate(ele->getNum());
  // set matrix m1 and m2 (one set by component x,y,z for m1)

  // get the shape function (and derivative values)
  nlsFunctionSpace<double>* sp1 = static_cast<nlsFunctionSpace<double>*>(&(this->space1));
  std::vector<GaussPointSpaceValues<double>*> vgps;
  sp1->get(ele,npts,GP,vgps);

  int dnpts = 2*npts;
  for (int i = 0; i < npts; i++)
  {
    // Coordonate of Gauss' point i
//    u = GP[i].pt[0]; v = GP[i].pt[1]; w = GP[i].pt[2];
    // ipv data
    const IPStateBase* ips = (*vips)[i];
    const IPVariableShell *ipv = static_cast<const IPVariableShell*>(ips->getState(IPStateBase::current));
    const shellLocalBasis *lb = ipv->getshellLocalBasis();
    const Bvector *B = ipv->getBvector();

    _mlaw->reduction(ips,vnalpha[i],malpha);
    // malpha will be replace by malpha-l, l is vnalpha[2]
    for(int a=0;a<2;a++)
      for(int b=0;b<2;b++){
        malphabeta(a,b) = dot(malpha[a],lb->dualBasisVector(b));

//        malphabeta(a,b) = dot(malpha[a],lb->dualBasisVector(b)) - vnalpha[i][2]; //???????????????,
    }
    // Compute of Hessian of SF. Each component of the vector in link to a shape function.
//    Grad.clear();
//    std::vector<TensorialTraits<double>::GradType> Grad;
//    DgC0LinearTerm<double>::space1.gradf(ele,u, v, w, Grad); // a optimiser
    std::vector<TensorialTraits<double>::GradType> &Grad = vgps[i]->_vgrads;
    // Weight of Gauss' point i and Jacobian's value at this point
    weight = GP[i].weight;
    wJ = lb->getJacobian() * weight;
    vnalpha[i][0]*=wJ; vnalpha[i][1]*=wJ;


  //   malphabeta = malphabeta - vnalpha[i][2]; //????????????????


    malphabeta*=wJ;

    int di = i+i;
    int foi = di+di;
    for(int j = 0; j < nbFF; j++)
    {
      m2(di,j) = Grad[j](0);
      m2(di+1,j) = Grad[j](1);
      m2(di,j+nbFF) = Grad[j](0);
      m2(di+1,j+nbFF) = Grad[j](1);
      m2(di,j+nbFF+nbFF) = Grad[j](0);
      m2(di+1,j+nbFF+nbFF) = Grad[j](1);
    }
    m2.copy(B->getBending(),0,4,0,nbdof,dnpts+foi,0);
    // m1 (independant of component)
    m1(0,dnpts+foi)   = malphabeta(0,0);
    m1(0,dnpts+foi+1) = malphabeta(0,1);
    m1(0,dnpts+foi+2) = malphabeta(1,0);
    m1(0,dnpts+foi+3) = malphabeta(1,1);
  }
  for (int k = 0; k < 3; k++)
  {
    for (int i = 0; i < npts; i++)
    {
      m1(0,2*i)   = vnalpha[i][0](k);
      m1(0,2*i+1) = vnalpha[i][1](k);
    }
    m1.multOnBlock(m2,nbFF,k*nbFF,1,1,m);
  }

// sum on Gauss' points (old way scholar)
/*  for (int i = 0; i < npts; i++)
  {
    // Coordonate of Gauss' point i
    u = GP[i].pt[0]; v = GP[i].pt[1]; w = GP[i].pt[2];
    // Weight of Gauss' point i and Jacobian's value at this point
    weight = GP[i].weight;

    // Compute of Hessian of SF. Each component of the vector in link to a shape function.
    DgC0LinearTerm<SVector3>::space1.gradfuvw(ele,u, v, w, Grad); // a optimiser
    wJ = vlb[i]->getJacobian() * weight;
    for(int j=0; j<nbFF;j++){
      for(int k=0;k<3;k++)
        for(int alpha=0;alpha<2;alpha++)
          for(int beta=0;beta<2;beta++)
            m(j+k*nbFF)+=wJ*(Grad[j][alpha]*vnalpha[i](alpha,beta)*vlb[i]->basisVector(beta,k)+ vBm[i]->getBending(j,k,alpha,beta)*vmalpha[i](alpha,beta));
    }
  }
*/
}

void IsotropicStiffInterTermC0DgShell::get(MElement *ele,int npts,IntPt *GP, fullMatrix<double> &m) const
{
  // data initialization
  iele = dynamic_cast<MInterfaceLine*>(ele);
  nbdof_m = BilinearTerm<double,double>::space1.getNumKeys(iele->getElem(0));
  nbdof_p = BilinearTerm<double,double>::space2.getNumKeys(iele->getElem(1));
  nbFF_m = nbdof_m/3;
  nbFF_p = nbdof_p/3;
  // Initialization
  m.resize(nbdof_m+nbdof_p, nbdof_m+nbdof_p,true); // true --> setAll(0.)
 //  m.setAll(0.);
  R.clear();
  // Characteristic size of interface element
  h_s = iele->getCharacteristicSize();
  Bhs = beta1/h_s;
  B2hs= beta2/h_s;
  B3hs= beta3/h_s;

  // get data at gauss' points
  ipf->getIPv(iele,vipvm,vipvprevm,vipvp,vipvprevp);
  // get Gauss point on minus and plus elements
  IntPt *GPm; IntPt *GPp;
  _interQuad->getIntPoints(iele,GP,&GPm,&GPp);

  // get the shape function (and derivative values)
  nlsFunctionSpace<double>* sp1 = static_cast<nlsFunctionSpace<double>*>(&(this->space1));
  std::vector<GaussPointSpaceValues<double>*> vgpsm;
  sp1->get(iele->getElem(0),npts,GPm,vgpsm);
  // get the shape function (and derivative values)
  nlsFunctionSpace<double>* sp2 = static_cast<nlsFunctionSpace<double>*>(&(this->space2));
  std::vector<GaussPointSpaceValues<double>*> vgpsp;
  sp1->get(iele->getElem(1),npts,GPp,vgpsp);

  // sum on Gauss' points
  for (int i = 0; i < npts; i++)
  {
    // Coordonate of Gauss' point i
    //u = GP[i].pt[0]; v = GP[i].pt[1]; w = GP[i].pt[2];
    // Weight of Gauss' point i
    weight = GP[i].weight;
    const linearShellLocalBasisBulk *lbm = static_cast<const linearShellLocalBasisBulk*>(vipvm[i]->getshellLocalBasis());
    const linearShellLocalBasisBulk *lbp = static_cast<linearShellLocalBasisBulk*>(vipvp[i]->getshellLocalBasis());
    const linearShellLocalBasisInter *lbsm = static_cast<linearShellLocalBasisInter*>(vipvm[i]->getshellLocalBasisOfInterface()); // shellLocalBasis of minus element change this ??
    Bvector *Bm = vipvm[i]->getBvector();
    Bvector *Bp = vipvp[i]->getBvector();
    // broken ??
    bool broken = false;
    if(_mlawMinus->getType() == materialLaw::fracture){ // fracture possibility Two elem are broken
      const IPShellFractureCohesive * ipvfm = static_cast<const IPShellFractureCohesive*>(vipvm[i]);
      broken = ipvfm->isbroken();
    }

    //printf("Abscisse of gauss point %f\n",u);
    //Compute the position (u,v) in the element of the Gauss point (to know where evaluate the shape functions)
//    double uem = GPm[i].pt[0]; double vem = GPm[i].pt[1]; //double wem = GPm[i].pt[2];
//    double uep = GPp[i].pt[0]; double vep = GPp[i].pt[1]; //double wep = GPp[i].pt[2];
//    iele->getuvwOnElem(u,v,uem,vem,wem,uep,vep,wep);
     // Compute of gradient and hessian of shape functions on interface element and on elements
     // ATTENTION used space1 on interface change this (own space for interface)
//     Grads_m.clear(); Grads_p.clear();
//     BilinearTerm<double,double>::space1.gradf(iele->getElem(0),uem, vem, w, Grads_m); // w = 0
//     BilinearTerm<double,double>::space2.gradf(iele->getElem(1),uep, vep, w, Grads_p);
    std::vector<TensorialTraits<double>::GradType> &Grads_m = vgpsm[i]->_vgrads;
    std::vector<TensorialTraits<double>::GradType> &Grads_p = vgpsp[i]->_vgrads;
      // basis of elements and interface element
     wJ = weight * lbsm->getJacobian();

     // Compute of Deltat_tilde
     _nvar.setDeltatTilde(lbm,Grads_m,nbFF_m,lbp,Grads_p,nbFF_p);
     if(!broken){
       // Compute of Bhat vector (1 component for now because 1 dof (z) ) --> it's a vector of length == nbFF
       Bm->hatBending(lbm,Bhat_m);
       Bp->hatBending(lbp,Bhat_p);
       // Compute of Hooke hat tensor in bending on minus and plus element
       _mlawMinus->shellBendingStiffness(vipvm[i],H_m);
       _mlawPlus->shellBendingStiffness(vipvp[i],H_p);
        // coupling (Full Dg Only)
       if(_fulldg){
         Hlambda_m.lambdahat(H_m,lbm);
         Hlambda_p.lambdahat(H_p,lbp);
       }
       // hat operation
       H_m.hat(lbm);
       H_p.hat(lbp);
       Hmean.mean(&H_m,&H_p);
       double meconscoup[3][3];
       double mecompcoup[3][3];
       for(int j=0;j<nbFF_m;j++){
         for(int k=0;k<nbFF_m;k++){
           consC0ShellStiffnessTerms(&H_m,k,Bhat_m,_nvar.deltatTildeMinus(j),lbm,lbsm,me_cons);
           compC0ShellStiffnessTerms(&H_m,j,Bhat_m,_nvar.deltatTildeMinus(k),lbm,lbsm,me_comp);
           stabilityC0ShellStiffnessTerms(&Hmean,_nvar.deltatTildeMinus(j), _nvar.deltatTildeMinus(k),lbsm,me_stab);
           for(int jj=0;jj<3;jj++)
             for(int kk=0;kk<3;kk++)
               m(j+jj*nbFF_m,k+kk*nbFF_m) += wJ*(- me_cons[jj][kk] - me_comp[jj][kk] + Bhs * me_stab[jj][kk] );
         }
         for(int k=0;k<nbFF_p;k++){
           consC0ShellStiffnessTerms(&H_p,k,Bhat_p,_nvar.deltatTildeMinus(j),lbp,lbsm,me_cons);
           compC0ShellStiffnessTerms(&H_m,j,Bhat_m,_nvar.deltatTildePlus(k),lbm,lbsm,me_comp);
           stabilityC0ShellStiffnessTerms(&Hmean,_nvar.deltatTildeMinus(j), _nvar.deltatTildePlus(k),lbsm,me_stab);
           for(int jj=0;jj<3;jj++)
             for(int kk=0;kk<3;kk++)
               m(j+jj*nbFF_m,k+nbdof_m+kk*nbFF_p) += wJ*( - me_cons[jj][kk] + me_comp[jj][kk] - Bhs * me_stab[jj][kk] );
         }
       }
       for(int j=0;j<nbFF_p;j++){
         for(int k=0;k<nbFF_m;k++){
           consC0ShellStiffnessTerms(&H_m,k,Bhat_m,_nvar.deltatTildePlus(j),lbm,lbsm,me_cons);
           compC0ShellStiffnessTerms(&H_p,j,Bhat_p,_nvar.deltatTildeMinus(k),lbp,lbsm,me_comp);
           stabilityC0ShellStiffnessTerms(&Hmean,_nvar.deltatTildePlus(j), _nvar.deltatTildeMinus(k),lbsm,me_stab);
           for(int jj=0;jj<3;jj++)
             for(int kk=0;kk<3;kk++)
               m(j+(jj*nbFF_p)+nbdof_m,k+kk*nbFF_m) += wJ*(me_cons[jj][kk] - me_comp[jj][kk] - Bhs * me_stab[jj][kk]);
         }
         for(int k=0;k<nbFF_p;k++){
           consC0ShellStiffnessTerms(&H_p,k,Bhat_p,_nvar.deltatTildePlus(j),lbp,lbsm,me_cons);
           compC0ShellStiffnessTerms(&H_p,j,Bhat_p,_nvar.deltatTildePlus(k),lbp,lbsm,me_comp);
           stabilityC0ShellStiffnessTerms(&Hmean,_nvar.deltatTildePlus(j),_nvar.deltatTildePlus(k),lbsm,me_stab);
           for(int jj=0;jj<3;jj++)
             for(int kk=0;kk<3;kk++)
               m(j+(jj*nbFF_p)+nbdof_m,k+(kk*nbFF_p)+nbdof_m) += wJ*( me_cons[jj][kk] + me_comp[jj][kk] + Bhs * me_stab[jj][kk]);
         }
       }
       // Extra terms of full dg formulation
       if(_fulldg){
//         Val_m.clear(); Val_p.clear();
//         BilinearTerm<double,double>::space1.f(iele->getElem(0),uem, vem, w, Val_m);
//         BilinearTerm<double,double>::space2.f(iele->getElem(1),uep, vep, w, Val_p);
        std::vector<TensorialTraits<double>::ValType> &Val_m = vgpsm[i]->_vvals;
        std::vector<TensorialTraits<double>::ValType> &Val_p = vgpsp[i]->_vvals;
          // coupling terms
         for(int j=0;j<nbFF_m;j++){
           for(int k=0;k<nbFF_m;k++){
             consC0ShellStiffnessCouplingTerms(Hlambda_m,k,Val_m[j],Bhat_m,lbm,lbsm,me_cons);
             compC0ShellStiffnessCouplingTerms(Hlambda_m,j,Val_m[k],Bhat_m,lbm,lbsm,me_comp);
             for(int jj=0;jj<3;jj++)
               for(int kk=0;kk<3;kk++)
                 m(j+jj*nbFF_m,k+kk*nbFF_m) += wJ*(- me_cons[jj][kk] - me_comp[jj][kk]);
           }
           for(int k=0;k<nbFF_p;k++){
             consC0ShellStiffnessCouplingTerms(Hlambda_p,k,Val_m[j],Bhat_p,lbp,lbsm,me_cons);
             compC0ShellStiffnessCouplingTerms(Hlambda_m,j,Val_p[k],Bhat_m,lbm,lbsm,me_comp);
             for(int jj=0;jj<3;jj++)
               for(int kk=0;kk<3;kk++)
                 m(j+jj*nbFF_m,k+nbdof_m+kk*nbFF_p) += wJ*( - me_cons[jj][kk] + me_comp[jj][kk]);
           }
         }
         for(int j=0;j<nbFF_p;j++){
           for(int k=0;k<nbFF_m;k++){
             consC0ShellStiffnessCouplingTerms(Hlambda_m,k,Val_p[j],Bhat_m,lbm,lbsm,me_cons);
             compC0ShellStiffnessCouplingTerms(Hlambda_p,j,Val_m[k],Bhat_p,lbp,lbsm,me_comp);
             for(int jj=0;jj<3;jj++)
               for(int kk=0;kk<3;kk++)
                 m(j+(jj*nbFF_p)+nbdof_m,k+kk*nbFF_m) += wJ*(me_cons[jj][kk] - me_comp[jj][kk]);
           }
           for(int k=0;k<nbFF_p;k++){
             consC0ShellStiffnessCouplingTerms(Hlambda_p,k,Val_p[j],Bhat_p,lbp,lbsm,me_cons);
             compC0ShellStiffnessCouplingTerms(Hlambda_p,j,Val_p[k],Bhat_p,lbp,lbsm,me_comp);
             for(int jj=0;jj<3;jj++)
               for(int kk=0;kk<3;kk++)
                 m(j+(jj*nbFF_p)+nbdof_m,k+(kk*nbFF_p)+nbdof_m) += wJ*( me_cons[jj][kk] + me_comp[jj][kk]);
           }
         }
         // Compute Bnhat vector (used previous Bhat_m and Bhat_p)
         Bm->hatMembrane(lbm,Bhat_m);
         Bp->hatMembrane(lbp,Bhat_p);
         // Compute Hooke tensor in membrane and shearing
         _mlawMinus->shellMembraneStiffness(vipvm[i],H_m);
         _mlawMinus->shellShearingStiffness(vipvm[i],Hs_m);
         _mlawPlus->shellMembraneStiffness(vipvp[i],H_p);
         _mlawPlus->shellShearingStiffness(vipvp[i],Hs_p);
         H_m.hat(lbm);
         H_p.hat(lbp);
         Hmean.mean(&H_m,&H_p);
         Hsmean.mean(Hs_p,Hs_m);


 //for(int iii=0;iii<3;iii++) for(int jjj=0;jjj<3;jjj++){me_comp[iii][jjj]=me_cons[iii][jjj]=0.;}
         // Assembly (group all ??)
         for(int j=0;j<nbFF_m;j++){
           for(int k=0;k<nbFF_m;k++){
             consC0ShellStiffnessMembraneTerms(&H_m,k,Bhat_m,Val_m[j],lbsm,me_cons);
             compC0ShellStiffnessMembraneTerms(&H_m,j,Bhat_m,Val_m[k],lbsm,me_comp);
             stabilityC0ShellStiffnessMembraneTerms(&Hmean,Val_m[j], Val_m[k],lbsm,me_stab);
             for(int jj=0;jj<3;jj++)
               for(int kk=0;kk<3;kk++)
                 m(j+jj*nbFF_m,k+kk*nbFF_m) += wJ*(- me_cons[jj][kk] - me_comp[jj][kk] + B2hs * me_stab[jj][kk] );
           }
           for(int k=0;k<nbFF_p;k++){
             consC0ShellStiffnessMembraneTerms(&H_m,k,Bhat_p,Val_m[j],lbsm,me_cons);
             compC0ShellStiffnessMembraneTerms(&H_p,j,Bhat_m,Val_p[k],lbsm,me_comp);
             stabilityC0ShellStiffnessMembraneTerms(&Hmean,Val_m[j], Val_p[k],lbsm,me_stab);
             for(int jj=0;jj<3;jj++)
               for(int kk=0;kk<3;kk++)
                 m(j+jj*nbFF_m,k+nbdof_m+kk*nbFF_p) += wJ*( - me_cons[jj][kk] + me_comp[jj][kk]- B2hs * me_stab[jj][kk] );
           }
         }
         for(int j=0;j<nbFF_p;j++){
           for(int k=0;k<nbFF_m;k++){
             consC0ShellStiffnessMembraneTerms(&H_p,k,Bhat_m,Val_p[j],lbsm,me_cons);
             compC0ShellStiffnessMembraneTerms(&H_m,j,Bhat_p,Val_m[k],lbsm,me_comp);
             stabilityC0ShellStiffnessMembraneTerms(&Hmean,Val_p[j], Val_m[k],lbsm,me_stab);
             for(int jj=0;jj<3;jj++)
               for(int kk=0;kk<3;kk++)
                 m(j+(jj*nbFF_p)+nbdof_m,k+kk*nbFF_m) += wJ*(me_cons[jj][kk] - me_comp[jj][kk] - B2hs * me_stab[jj][kk]);
           }
           for(int k=0;k<nbFF_p;k++){
             consC0ShellStiffnessMembraneTerms(&H_p,k,Bhat_p,Val_p[j],lbsm,me_cons);
             compC0ShellStiffnessMembraneTerms(&H_p,j,Bhat_p,Val_p[k],lbsm,me_comp);
             stabilityC0ShellStiffnessMembraneTerms(&Hmean,Val_p[j], Val_p[k],lbsm,me_stab);
             for(int jj=0;jj<3;jj++)
               for(int kk=0;kk<3;kk++)
                 m(j+(jj*nbFF_p)+nbdof_m,k+(kk*nbFF_p)+nbdof_m) += wJ*( me_cons[jj][kk] + me_comp[jj][kk] + B2hs * me_stab[jj][kk]);
           }
         }
         // Out of Plane term
         Cst = wJ*B3hs; // Hooke tensor in "shearing" 1 component = Cs

         // compute B vector
         compute_Bs(lbsm,Val_m,nbFF_m,Bs_m);
         compute_Bs(lbsm,Val_p,nbFF_p,Bs_p);
         for(int j=0;j<nbFF_m;j++){
           for(int k=0;k<nbFF_m;k++){
             stabilityC0ShellStiffnessShearingTerms(Bs_m[j], Bs_m[k],lbsm,Hsmean,me_stab);
             for(int jj=0;jj<3;jj++)
               for(int kk=0;kk<3;kk++)
                 {m(j+jj*nbFF_m,k+kk*nbFF_m) += Cst * me_stab[jj][kk]; }//printf("-- %d %d %f\n",jj,kk,Cst * me_stab[jj][kk]);
           }
           for(int k=0;k<nbFF_p;k++){
             stabilityC0ShellStiffnessShearingTerms(Bs_m[j], Bs_p[k],lbsm,Hsmean,me_stab);
             for(int jj=0;jj<3;jj++)
               for(int kk=0;kk<3;kk++)
                 {m(j+jj*nbFF_m,k+nbdof_m+kk*nbFF_p) += -Cst * me_stab[jj][kk]; }//printf("-+ %d %d %f\n",jj,kk,Cst * me_stab[jj][kk]);
           }
         }
         for(int j=0;j<nbFF_p;j++){
           for(int k=0;k<nbFF_m;k++){
             stabilityC0ShellStiffnessShearingTerms(Bs_p[j], Bs_m[k],lbsm,Hsmean,me_stab);
             for(int jj=0;jj<3;jj++)
               for(int kk=0;kk<3;kk++)
                 {m(j+(jj*nbFF_p)+nbdof_m,k+kk*nbFF_m) += -Cst * me_stab[jj][kk]; }//printf("+- %d %d %f\n",jj,kk,Cst * me_stab[jj][kk]);
           }
           for(int k=0;k<nbFF_p;k++){
             stabilityC0ShellStiffnessShearingTerms(Bs_p[j], Bs_p[k],lbsm,Hsmean,me_stab);
             for(int jj=0;jj<3;jj++)
               for(int kk=0;kk<3;kk++)
                 {m(j+(jj*nbFF_p)+nbdof_m,k+(kk*nbFF_p)+nbdof_m) += Cst * me_stab[jj][kk]; }//printf("++ %d %d %f\n",jj,kk,Cst * me_stab[jj][kk]);
           }
         }
       }
     }
     else{
       // get matrix by perturbation
       // WARNING : as the distinction between cg/dg and full dg formulation is only made for the assembling
       // the perturbation must be applied on cg/dg case as it is a full dg formulation
//       Val_m.clear(); Val_p.clear();
//       BilinearTerm<double,double>::space1.f(iele->getElem(0),uem, vem, w, Val_m);
//       BilinearTerm<double,double>::space2.f(iele->getElem(1),uep, vep, w, Val_p);
       std::vector<TensorialTraits<double>::ValType> &Val_m = vgpsm[i]->_vvals;
       std::vector<TensorialTraits<double>::ValType> &Val_p = vgpsp[i]->_vvals;
       //init
       R.clear();
       fp.resize(nbdof_m+nbdof_p);
       fm.resize(nbdof_m+nbdof_p);
       disp.resize(nbdof_m+nbdof_p);
       // get displacement vector
       BilinearTerm<double,double>::space1.getKeys(iele->getElem(0),R);
       BilinearTerm<double,double>::space2.getKeys(iele->getElem(1),R);
       _ufield->get(R,disp);
        // pertubation on displacement vector
        // Here fracture law is used. This one is supposed to be THE SAME
        // on both sides
       for(int j=0;j<disp.size();j++){
         fm.scale(0.);
         disp(j) -= perturbation;
         // compute fm with a function for now FIX IT TODO ??
         // compute the jumps (which depend on displacement)
         computeFintFrac(disp,vipvm[i],vipvprevm[i], weight,_mlawMinus,nbFF_m,Val_m,Grads_m,
                         nbdof_m,nbFF_p,Val_p,Grads_p,_nvar,fm);

         disp(j)+=perturbation;
         fp.scale(0.);
         disp(j)+=perturbation;
         // compute fp with a function for now FIX IT TODO ??
         computeFintFrac(disp,vipvp[i],vipvprevp[i], weight,_mlawMinus,nbFF_m,Val_m,Grads_m,
                         nbdof_m,nbFF_p,Val_p,Grads_p,_nvar,fp);
         disp(j)-=perturbation;
         // add in the matrix
         for(int k=0;k<nbdof_m+nbdof_p;k++)
           m(k,j) += (fp(k)-fm(k))/(perturbation+perturbation);
       }
    }
  }
  //    Msg::Info("size matrix %d %d",m.size1(),m.size2());
  //    m.print("Interface Domain");
}

void IsotropicForceInterTermC0DgShell::get(MElement *ele,int npts,IntPt *GP, fullVector<double> &m) const
{
  // data initialization
  // Retrieve of the element link to the interface element velem[0] == minus elem ; velem == plus elem
  const fullVector<double> &datafield = *_data;
  iele = dynamic_cast<MInterfaceLine*>(ele);
  nbdof_m = _minusSpace->getNumKeys(iele->getElem(0));
  nbdof_p = _plusSpace->getNumKeys(iele->getElem(1));
  nbFF_m=nbdof_m/3;
  nbFF_p=nbdof_p/3;
  int knbFFm=0;
  int knbFFp=0;

  // Initialization
  m.resize(nbdof_m+nbdof_p);
  // Characteristic size of interface element
  h_s = iele->getCharacteristicSize();

  Bhs = beta1/h_s;
  B2hs= beta2/h_s;
  B3hs= beta3/h_s;

  // get gauss' points data
  AllIPState::ipstateElementContainer* vips = ipf->getAips()->getIPstate(iele->getNum());
  // Coordinates of Gauss points on minus and plus elements
  IntPt *GPm; IntPt *GPp;
  _interQuad->getIntPoints(iele,GP,&GPm,&GPp);
  // Shape functions values (with derivatives)
  std::vector<GaussPointSpaceValues<double>*> vgpsm;
  _minusSpace->get(iele->getElem(0),npts,GPm,vgpsm);
  std::vector<GaussPointSpaceValues<double>*> vgpsp;
  _minusSpace->get(iele->getElem(1),npts,GPp,vgpsp);

  // build term with a matrix vector product
  // size depend on full dg or not (more terms if full dg)
  int npts3 = npts+npts+npts;
  int npts4 = npts3+npts;
  int npts5 = npts4+npts;
  int npts6 = npts3+npts3;
  int npts8 = npts4+npts4;
  if(!_fulldg){
    mbuild.resize(npts3,nbdof_m+nbdof_p,false); // set operation after
    vbuild.resize(npts3);
    mbuildblock.resize(npts4,nbdof_m+nbdof_p,false); // CgDg no fracture allow
    mbuildminus.resize(1,npts4);
    mbuildplus.resize(1,npts4);
  }
  else{
    mbuild.resize(npts6,nbdof_m+nbdof_p,false); // set operation after
    vbuild.resize(npts6);
    mbuildblock.resize(npts8,nbdof_m+nbdof_p,true); // hast to be 0 if fracture
    mbuildminus.resize(1,npts8);
    mbuildplus.resize(1,npts8);
  }
  for (int i = 0; i < npts; i++)
  {
    int i2 = i+i;
    int i3 = i2+i;
    int i4 = i3+i;
    // Coordonate of Gauss' point i
    //u = GP[i].pt[0]; v = GP[i].pt[1]; w = GP[i].pt[2];
    // Weight of Gauss' point i
    weight = GP[i].weight;
    const IPStateBase* ipsm = (*vips)[i];
    const IPStateBase* ipsp = (*vips)[i+npts];
    const IPVariableShell *ipvm = static_cast<IPVariableShell*>(ipsm->getState(IPStateBase::current));
    const IPVariableShell *ipvp = static_cast<IPVariableShell*>(ipsp->getState(IPStateBase::current));

    const linearShellLocalBasisBulk *lbm = static_cast<const linearShellLocalBasisBulk*>(ipvm->getshellLocalBasis());
    const linearShellLocalBasisBulk *lbp = static_cast<const linearShellLocalBasisBulk*>(ipvp->getshellLocalBasis());
    const linearShellLocalBasisInter *lbsm= static_cast<const linearShellLocalBasisInter*>(ipvm->getshellLocalBasisOfInterface()); // Local Basis of interface via minus element change this ??
    const Bvector *Bm = ipvm->getBvector();
    const Bvector *Bp = ipvp->getBvector();
    // compute reduction effort
    broken = false;
    if(_mlawMinus->getType() == materialLaw::fracture ){ // possibility of fracture
      const IPShellFractureCohesive *ipvwf = static_cast<const IPShellFractureCohesive*>(ipvm); // broken via minus (OK broken on both sides)
      broken = ipvwf->isbroken();
    }
    _mlawMinus->reduction(ipsm,_vnhatmean,_vmhatmean,_fulldg);
    _mlawPlus->reduction(ipsp,_vnhatp,_vmhatp,_fulldg);

    for(int a=0;a<2;a++){
      _vmhatmean[a]+=_vmhatp[a];
      _vmhatmean[a]*=0.5;
      if(_fulldg){
        _vnhatmean[a]+=_vnhatp[a];
        _vnhatmean[a]*=0.5;
        for(int b=0;b<2;b++)
          nhatmean(a,b) = dot(_vnhatmean[a],lbsm->dualBasisVector(b));
      }
    }
    //Compute the position (u,v) in the element of the Gauss point (to know where evaluate the shape functions)
//    double uem = GPm[i].pt[0]; double vem = GPm[i].pt[1];
//    double uep = GPp[i].pt[0]; double vep = GPp[i].pt[1];
//    iele->getuvwOnElem(u,v,uem,vem,wem,uep,vep,wep);
    // Compute of gradient of shape functions on interface element and on elements
// For now compute grad on interface as for minus elem CHANGE THIS (own space on interface)
//    Grads_m.clear(); Grads_p.clear();
//    _minusSpace->gradf(iele->getElem(0),uem, vem, w, Grads_m); // w = 0
//    _plusSpace->gradf(iele->getElem(1),uep, vep, w, Grads_p);
    std::vector<TensorialTraits<double>::GradType> &Grads_m = vgpsm[i]->_vgrads;
    std::vector<TensorialTraits<double>::GradType> &Grads_p = vgpsp[i]->_vgrads;
    wJ = weight*lbsm->getJacobian();
    // Compute of Deltat_tilde
    _nvar.setDeltatTilde(lbm,Grads_m,nbFF_m,lbp,Grads_p,nbFF_p);
    _nvar.jump(datafield,rjump); // compute jump in rotation
    _nvar.scaleminus(-1); // minus sign for assembly

    for(int alpha=0;alpha<2;alpha++)
       wJnucomp[alpha] = -wJ*lbsm->getMinusNormal(alpha);    //scaldot(lb[2]->basisVector(1),lb[2]->basisVector(alpha));

    SVector3 rHphi0c(0.,0.,0.);
    if(!broken){
      // bending term
      for(int a=0;a<2;a++)
        rjumpdotphi0[a] = dot(rjump,lbsm->basisVector(a));

      // Compute of Hooke hat tensor in bending on minus and plus element
      _mlawMinus->shellBendingStiffness(ipvm,H_m);
      _mlawPlus->shellBendingStiffness(ipvp,H_p);
      // coupling (full dg only)
      if(_fulldg){
        Hlambda_m.lambdahat(H_m,lbm);
        Hlambda_p.lambdahat(H_p,lbp);
      }
      // hat operation
      H_m.hat(lbm);
      H_p.hat(lbp);
      Hmean.mean(&H_m,&H_p);

      rHphi0c = (((rjumpdotphi0[0]*Hmean(0,0,0,0)+rjumpdotphi0[1]*Hmean(1,0,0,0))*wJnucomp[0]+
                  (rjumpdotphi0[0]*Hmean(0,0,0,1)+rjumpdotphi0[1]*Hmean(1,0,0,1))*wJnucomp[1])*lbsm->getMinusNormal(0)+
                 ((rjumpdotphi0[0]*Hmean(0,1,0,0)+rjumpdotphi0[1]*Hmean(1,1,0,0))*wJnucomp[0]+
                  (rjumpdotphi0[0]*Hmean(0,1,0,1)+rjumpdotphi0[1]*Hmean(1,1,0,1))*wJnucomp[1])*lbsm->getMinusNormal(1)) * lbsm->basisVector(0) +
                (((rjumpdotphi0[0]*Hmean(0,0,1,0)+rjumpdotphi0[1]*Hmean(1,0,1,0))*wJnucomp[0]+
                  (rjumpdotphi0[0]*Hmean(0,0,1,1)+rjumpdotphi0[1]*Hmean(1,0,1,1))*wJnucomp[1])*lbsm->getMinusNormal(0)+
                 ((rjumpdotphi0[0]*Hmean(0,1,1,0)+rjumpdotphi0[1]*Hmean(1,1,1,0))*wJnucomp[0]+
                  (rjumpdotphi0[0]*Hmean(0,1,1,1)+rjumpdotphi0[1]*Hmean(1,1,1,1))*wJnucomp[1])*lbsm->getMinusNormal(1)) * lbsm->basisVector(1); // -- = + for the two getMinusNormal
      rHphi0c*=-Bhs; // - due to v^alpha- = -vlb->getMinusNormal
      // assembly bending compatibility
      Bp->hatBending(lbp,_Bphat);
      Bm->hatBending(lbm,_Bmhat);
// BEAWARE INDICES ALPHA AND BETA ARE INVERTED ????
      mbuildminus(0,i4)   = 0.5*((rjumpdotphi0[0]*H_m(0,0,0,0)+rjumpdotphi0[1]*H_m(0,1,0,0))*wJnucomp[0]+
                                 (rjumpdotphi0[0]*H_m(1,0,0,0)+rjumpdotphi0[1]*H_m(1,1,0,0))*wJnucomp[1]);

      mbuildminus(0,i4+1) = 0.5*((rjumpdotphi0[0]*H_m(0,0,0,1)+rjumpdotphi0[1]*H_m(0,1,0,1))*wJnucomp[0]+
                                 (rjumpdotphi0[0]*H_m(1,0,0,1)+rjumpdotphi0[1]*H_m(1,1,0,1))*wJnucomp[1]);

      mbuildminus(0,i4+2) = 0.5*((rjumpdotphi0[0]*H_m(0,0,1,0)+rjumpdotphi0[1]*H_m(0,1,1,0))*wJnucomp[0]+
                                 (rjumpdotphi0[0]*H_m(1,0,1,0)+rjumpdotphi0[1]*H_m(1,1,1,0))*wJnucomp[1]);

      mbuildminus(0,i4+3) = 0.5*((rjumpdotphi0[0]*H_m(0,0,1,1)+rjumpdotphi0[1]*H_m(0,1,1,1))*wJnucomp[0]+
                                 (rjumpdotphi0[0]*H_m(1,0,1,1)+rjumpdotphi0[1]*H_m(1,1,1,1))*wJnucomp[1]);


      mbuildplus(0,i4)   = 0.5*((rjumpdotphi0[0]*H_p(0,0,0,0)+rjumpdotphi0[1]*H_p(0,1,0,0))*wJnucomp[0]+
                                (rjumpdotphi0[0]*H_p(1,0,0,0)+rjumpdotphi0[1]*H_p(1,1,0,0))*wJnucomp[1]);

      mbuildplus(0,i4+1) = 0.5*((rjumpdotphi0[0]*H_p(0,0,0,1)+rjumpdotphi0[1]*H_p(0,1,0,1))*wJnucomp[0]+
                                (rjumpdotphi0[0]*H_p(1,0,0,1)+rjumpdotphi0[1]*H_p(1,1,0,1))*wJnucomp[1]);

      mbuildplus(0,i4+2) = 0.5*((rjumpdotphi0[0]*H_p(0,0,1,0)+rjumpdotphi0[1]*H_p(0,1,1,0))*wJnucomp[0]+
                                (rjumpdotphi0[0]*H_p(1,0,1,0)+rjumpdotphi0[1]*H_p(1,1,1,0))*wJnucomp[1]);

      mbuildplus(0,i4+3) = 0.5*((rjumpdotphi0[0]*H_p(0,0,1,1)+rjumpdotphi0[1]*H_p(0,1,1,1))*wJnucomp[0]+
                                (rjumpdotphi0[0]*H_p(1,0,1,1)+rjumpdotphi0[1]*H_p(1,1,1,1))*wJnucomp[1]);

      mbuildblock.copy(_Bmhat.getBending(),0,4,0,nbdof_m,i4,0);
      mbuildblock.copy(_Bphat.getBending(),0,4,0,nbdof_p,i4,nbdof_m);
    }
    // Assembly bending consistency and stability
    for(int j=0;j<3;j++)
      vbuild(i3+j) = wJnucomp[0]*_vmhatmean[0](j) + wJnucomp[1]*_vmhatmean[1](j) + rHphi0c[j];
    knbFFm=0;
    knbFFp=0;
    for(int k=0;k<3;k++){
      for(int j=0;j<nbFF_m;j++)
        mbuild.copy(_nvar.deltatTildeMinus(j),0,3,k,1,i3,knbFFm+j);
      for(int j=0;j<nbFF_p;j++)
        mbuild.copy(_nvar.deltatTildePlus(j),0,3,k,1,i3,knbFFp+nbdof_m+j);
      knbFFm+=nbFF_m;
      knbFFp+=nbFF_p;
    }
    if(_fulldg){
      // shape function values
//      Val_m.clear(); Val_p.clear();
//      _minusSpace->f(iele->getElem(0),uem, vem, w, Val_m);
//      _plusSpace->f(iele->getElem(1),uep, vep, w, Val_p);
      std::vector<TensorialTraits<double>::ValType> &Val_m = vgpsm[i]->_vvals;
      std::vector<TensorialTraits<double>::ValType> &Val_p = vgpsp[i]->_vvals;
      // consistency
      vbuild(i2+npts3)   = (wJnucomp[0]*nhatmean(0,0)+wJnucomp[1]*nhatmean(1,0));
      vbuild(i2+npts3+1) = (wJnucomp[0]*nhatmean(0,1)+wJnucomp[1]*nhatmean(1,1));
      if(!broken){
        // displacement jump
        displacementjump(Val_m,nbFF_m,Val_p,nbFF_p,datafield,ujump);
        for(int a=0;a<2;a++)
          ujumpphi0[a] = dot(ujump,lbsm->basisVector(a));

        // coupling terms (compatibility)
        // BEAWARE INDICES ALPHA AND BETA ARE INVERTED ?????????? For bending part
        mbuildminus(0,i4)   += 0.5*((ujumpphi0[0]*Hlambda_m(0,0,0,0)+ujumpphi0[1]*Hlambda_m(0,1,0,0))*wJnucomp[0]+
                                    (ujumpphi0[0]*Hlambda_m(1,0,0,0)+ujumpphi0[1]*Hlambda_m(1,1,0,0))*wJnucomp[1]);

        mbuildminus(0,i4+1) += 0.5*((ujumpphi0[0]*Hlambda_m(0,0,0,1)+ujumpphi0[1]*Hlambda_m(0,1,0,1))*wJnucomp[0]+
                                    (ujumpphi0[0]*Hlambda_m(1,0,0,1)+ujumpphi0[1]*Hlambda_m(1,1,0,1))*wJnucomp[1]);

        mbuildminus(0,i4+2) += 0.5*((ujumpphi0[0]*Hlambda_m(0,0,1,0)+ujumpphi0[1]*Hlambda_m(0,1,1,0))*wJnucomp[0]+
                                    (ujumpphi0[0]*Hlambda_m(1,0,1,0)+ujumpphi0[1]*Hlambda_m(1,1,1,0))*wJnucomp[1]);

        mbuildminus(0,i4+3) += 0.5*((ujumpphi0[0]*Hlambda_m(0,0,1,1)+ujumpphi0[1]*Hlambda_m(0,1,1,1))*wJnucomp[0]+
                                    (ujumpphi0[0]*Hlambda_m(1,0,1,1)+ujumpphi0[1]*Hlambda_m(1,1,1,1))*wJnucomp[1]);


        mbuildplus(0,i4)   += 0.5*((ujumpphi0[0]*Hlambda_p(0,0,0,0)+ujumpphi0[1]*Hlambda_p(0,1,0,0))*wJnucomp[0]+
                                   (ujumpphi0[0]*Hlambda_p(1,0,0,0)+ujumpphi0[1]*Hlambda_p(1,1,0,0))*wJnucomp[1]);

        mbuildplus(0,i4+1) += 0.5*((ujumpphi0[0]*Hlambda_p(0,0,0,1)+ujumpphi0[1]*Hlambda_p(0,1,0,1))*wJnucomp[0]+
                                   (ujumpphi0[0]*Hlambda_p(1,0,0,1)+ujumpphi0[1]*Hlambda_p(1,1,0,1))*wJnucomp[1]);

        mbuildplus(0,i4+2) += 0.5*((ujumpphi0[0]*Hlambda_p(0,0,1,0)+ujumpphi0[1]*Hlambda_p(0,1,1,0))*wJnucomp[0]+
                                   (ujumpphi0[0]*Hlambda_p(1,0,1,0)+ujumpphi0[1]*Hlambda_p(1,1,1,0))*wJnucomp[1]);

        mbuildplus(0,i4+3) += 0.5*((ujumpphi0[0]*Hlambda_p(0,0,1,1)+ujumpphi0[1]*Hlambda_p(0,1,1,1))*wJnucomp[0]+
                                   (ujumpphi0[0]*Hlambda_p(1,0,1,1)+ujumpphi0[1]*Hlambda_p(1,1,1,1))*wJnucomp[1]);
        // Bhat vector
        Bp->hatMembrane(lbp,_Bphat);
        Bm->hatMembrane(lbm,_Bmhat);
        // Hooke tensor in tension (symmetrization is for elastic part)
        _mlawMinus->shellMembraneStiffness(ipvm,H_m);
        _mlawPlus->shellMembraneStiffness(ipvp,H_p);
        _mlawMinus->shellShearingStiffness(ipvm,Hs_m);
        _mlawPlus->shellShearingStiffness(ipvp,Hs_p);
        // hat operation
        H_m.hat(lbm);
        H_p.hat(lbp);
        Hmean.mean(&H_m,&H_p);
        Hsmean.mean(Hs_m,Hs_p);
        // Assembly membrane stability
        vbuild(i2+npts3)  += - B2hs*(wJnucomp[0]*(lbsm->getMinusNormal(0)*(Hmean(0,0,0,0)*ujumpphi0[0]+Hmean(0,0,0,1)*ujumpphi0[1]) +
                                                  lbsm->getMinusNormal(1)*(Hmean(0,0,1,0)*ujumpphi0[0]+Hmean(0,0,1,1)*ujumpphi0[1])) +
                                     wJnucomp[1]*(lbsm->getMinusNormal(0)*(Hmean(1,0,0,0)*ujumpphi0[0]+Hmean(1,0,0,1)*ujumpphi0[1]) +
                                                  lbsm->getMinusNormal(1)*(Hmean(1,0,1,0)*ujumpphi0[0]+Hmean(1,0,1,1)*ujumpphi0[1]))) ;
        vbuild(i2+npts3+1)+= - B2hs*(wJnucomp[0]*(lbsm->getMinusNormal(0)*(Hmean(0,1,0,0)*ujumpphi0[0]+Hmean(0,1,0,1)*ujumpphi0[1]) +
                                                  lbsm->getMinusNormal(1)*(Hmean(0,1,1,0)*ujumpphi0[0]+Hmean(0,1,1,1)*ujumpphi0[1])) +
                                     wJnucomp[1]*(lbsm->getMinusNormal(0)*(Hmean(1,1,0,0)*ujumpphi0[0]+Hmean(1,1,0,1)*ujumpphi0[1]) +
                                                  lbsm->getMinusNormal(1)*(Hmean(1,1,1,0)*ujumpphi0[0]+Hmean(1,1,1,1)*ujumpphi0[1]))) ;
        // Assembly shearing stability
        // Out of Plane term
        double Cst = wJ*B3hs ;
        // compute B vector
        compute_Bs(lbsm,Val_m,nbFF_m,Bs_m);
        compute_Bs(lbsm,Val_p,nbFF_p,Bs_p);
        // compute normal displacement jump
        double buildfactor1=0.;
        for(int j=0;j<nbFF_m;j++)
          buildfactor1-=Bs_m[j][0]*datafield(j)+Bs_m[j][1]*datafield(j+nbFF_m)+Bs_m[j][2]*datafield(j+nbFF_m+nbFF_m);
        for(int j=0;j<nbFF_p;j++)
          buildfactor1+=Bs_p[j][0]*datafield(j+nbdof_m)+Bs_p[j][1]*datafield(j+nbFF_p+nbdof_m)+Bs_p[j][2]*datafield(j+nbFF_p+nbFF_p+nbdof_m);
        double buildfactor2=0.;
        for(int alpha=0;alpha<2;alpha++)
          for(int beta=0;beta<2;beta++)
            buildfactor2+= Hsmean(alpha,beta)*lbsm->getMinusNormal(alpha)*lbsm->getMinusNormal(beta);
        vbuild(npts5+i) = buildfactor1*buildfactor2*Cst;
        knbFFm=0;
        knbFFp=0;
        for(int k=0;k<3;k++){
          for(int j=0;j<nbFF_m;j++)
            mbuild(npts5+i,j+knbFFm) = - Bs_m[j][k];
          for(int j=0;j<nbFF_p;j++)
            mbuild(npts5+i,j+knbFFp+nbdof_m) = Bs_p[j][k];
          knbFFm+=nbFF_m;
          knbFFp+=nbFF_p;
        }

        // Assembly membrane compatibility
        int inpts4 = npts4+i4;
        mbuildminus(0,inpts4)   = 0.5*((wJnucomp[0]*H_m(0,0,0,0) + wJnucomp[1]*H_m(1,0,0,0))*ujumpphi0[0]+
                                       (wJnucomp[0]*H_m(0,1,0,0) + wJnucomp[1]*H_m(1,1,0,0))*ujumpphi0[1]);
        mbuildminus(0,inpts4+1) = 0.5*((wJnucomp[0]*H_m(0,0,0,1) + wJnucomp[1]*H_m(1,0,0,1))*ujumpphi0[0]+
                                       (wJnucomp[0]*H_m(0,1,0,1) + wJnucomp[1]*H_m(1,1,0,1))*ujumpphi0[1]);
        mbuildminus(0,inpts4+2) = 0.5*((wJnucomp[0]*H_m(0,0,1,0) + wJnucomp[1]*H_m(1,0,1,0))*ujumpphi0[0]+
                                       (wJnucomp[0]*H_m(0,1,1,0) + wJnucomp[1]*H_m(1,1,1,0))*ujumpphi0[1]);
        mbuildminus(0,inpts4+3) = 0.5*((wJnucomp[0]*H_m(0,0,1,1) + wJnucomp[1]*H_m(1,0,1,1))*ujumpphi0[0]+
                                       (wJnucomp[0]*H_m(0,1,1,1) + wJnucomp[1]*H_m(1,1,1,1))*ujumpphi0[1]);

        mbuildplus(0,inpts4) =   0.5*((wJnucomp[0]*H_p(0,0,0,0) + wJnucomp[1]*H_p(1,0,0,0))*ujumpphi0[0]+
                                      (wJnucomp[0]*H_p(0,1,0,0) + wJnucomp[1]*H_p(1,1,0,0))*ujumpphi0[1]);
        mbuildplus(0,inpts4+1) = 0.5*((wJnucomp[0]*H_p(0,0,0,1) + wJnucomp[1]*H_p(1,0,0,1))*ujumpphi0[0]+
                                      (wJnucomp[0]*H_p(0,1,0,1) + wJnucomp[1]*H_p(1,1,0,1))*ujumpphi0[1]);
        mbuildplus(0,inpts4+2) = 0.5*((wJnucomp[0]*H_p(0,0,1,0) + wJnucomp[1]*H_p(1,0,1,0))*ujumpphi0[0]+
                                      (wJnucomp[0]*H_p(0,1,1,0) + wJnucomp[1]*H_p(1,1,1,0))*ujumpphi0[1]);
        mbuildplus(0,inpts4+3) = 0.5*((wJnucomp[0]*H_p(0,0,1,1) + wJnucomp[1]*H_p(1,0,1,1))*ujumpphi0[0]+
                                      (wJnucomp[0]*H_p(0,1,1,1) + wJnucomp[1]*H_p(1,1,1,1))*ujumpphi0[1]);
        mbuildblock.copy(_Bmhat.getMembrane(),0,4,0,nbdof_m,inpts4,0);
        mbuildblock.copy(_Bphat.getMembrane(),0,4,0,nbdof_p,inpts4,nbdof_m);
      }
      else{
        knbFFm=0;
        knbFFp=0;
        for(int k=0;k<3;k++){
          for(int j=0;j<nbFF_m;j++)
            mbuild(npts5+i,j+knbFFm) = 0.; // must be 0 if broken
          for(int j=0;j<nbFF_p;j++)
            mbuild(npts5+i,j+knbFFp+nbdof_m) = 0.; // must be 0 if broken
          knbFFm+=nbFF_m;
          knbFFp+=nbFF_p;
        }
      }
      knbFFm=0;
      knbFFp=0;
      double phi00k;
      double phi01k;
      for(int k=0;k<3;k++){
        phi00k = lbsm->basisVector(0,k);
        phi01k = lbsm->basisVector(1,k);
        for(int j=0;j<nbFF_m;j++){
          mbuild(npts3+i2,j+knbFFm)   = - Val_m[j]*phi00k;
          mbuild(npts3+i2+1,j+knbFFm) = - Val_m[j]*phi01k;
        }
        for(int j=0;j<nbFF_p;j++){
          mbuild(npts3+i2,j+knbFFp+nbdof_m)   = Val_p[j]*phi00k;
          mbuild(npts3+i2+1,j+knbFFp+nbdof_m) = Val_p[j]*phi01k;
        }
        knbFFm+=nbFF_m;
        knbFFp+=nbFF_p;
      }
    }
  }
  mbuild.multWithATranspose(vbuild,1,0,m);
  mbuildminus.multOnBlock(mbuildblock,nbdof_m,0,1,1,m);
  mbuildplus.multOnBlock(mbuildblock,nbdof_p,nbdof_m,1,1,m);
 //below portion added by Shantanu: to be commented later on //
/*
  if((iele->getElem(0)->getNum() == 14 and iele->getElem(1)->getNum() == 18) or
     (iele->getElem(1)->getNum() == 14 and iele->getElem(0)->getNum() == 18))
 {
     //Msg::Info("It is the interface I search");
     char *_forcefilename = "force_in_cohesive.csv";
     FILE *tmp = fopen(_forcefilename, "a");
     for (int __itr = 0; __itr<m.size(); __itr++)
     {
         fprintf(tmp, "%12.5E ", (m)(__itr));
     }
     fprintf(tmp, "\n");
     fclose(tmp);
  }
 */
 //above portion added by Shantanu on 18-06-2013: to be commented later on //

 #ifdef _DEBUG
  for(int i=0;i<m.size();i++)
    if(std::isnan(m(i)))
      Msg::Error("nan force");
 #endif // _DEBUG
}

/*
void IsotropicForceInterTermC0DgShell::get(MElement *ele,int npts,IntPt *GP,fullVector<double> &m) const
{
  // data initialization
  static reductionElement mhatmean;
  // Retrieve of the element link to the interface element velem[0] == minus elem ; velem == plus elem
  const fullVector<double> &datafield = *_data;
  iele = dynamic_cast<MInterfaceLine*>(ele);
  nbdof_m = _minusSpace->getNumKeys(iele->getElem(0));
  nbdof_p = _plusSpace->getNumKeys(iele->getElem(1));
  nbFF_m=nbdof_m/3;
  nbFF_p=nbdof_p/3;
  // Initialization
  m.resize(nbdof_m+nbdof_p);
  m.scale(0.);
  // Characteristic size of interface element
  h_s = iele->getCharacteristicSize();

  Bhs = beta1/h_s;
  B2hs= beta2/h_s;
  B3hs= beta3/h_s;
  // displacement
  double dt[3];
  SVector3 malpha;
  SVector3 nalpha;
  fullMatrix<double> mbuildterm(3,3);
  fullMatrix<double> mbuildterm2(3,3);
  fullVector<double> vbuild1(3);
  // get gauss' points data
//  ipf->getIPv(iele,vipvm,vipvp);
   AllIPState::ipstateElementContainer* vips = ipf->getAips()->getIPstate(iele->getNum());
  // sum on Gauss' points
  for (int i = 0; i < npts; i++)
  {
    // Coordonate of Gauss' point i
    u = GP[i].pt[0]; v = GP[i].pt[1]; w = GP[i].pt[2];
    // Weight of Gauss' point i
    weight = GP[i].weight;

    const IPVariableShell* ipvm = static_cast<IPVariableShell*>((*vips)[i]->getState(IPStateBase::current));
    const IPVariableShell* ipvp = static_cast<IPVariableShell*>((*vips)[i+npts]->getState(IPStateBase::current));
    const IPVariableShell* ipvmprev = static_cast<IPVariableShell*>((*vips)[i]->getState(IPStateBase::previous));
    const IPVariableShell* ipvpprev = static_cast<IPVariableShell*>((*vips)[i+npts]->getState(IPStateBase::previous));

    const linearShellLocalBasisBulk *lbm = static_cast<const linearShellLocalBasisBulk*>(ipvm->getshellLocalBasis());
    const linearShellLocalBasisBulk *lbp = static_cast<const linearShellLocalBasisBulk*>(ipvp->getshellLocalBasis());
    const linearShellLocalBasisInter *lbsm= static_cast<const linearShellLocalBasisInter*>(ipvm->getshellLocalBasisOfInterface()); // Local Basis of interface via minus element change this ??

    const Bvector *Bm = ipvm->getBvector();
    const Bvector *Bp = ipvp->getBvector();
    // compute reduction effort
    broken = false;
    if(_mlawMinus->getType() == materialLaw::fracture){ // possibility of fracture
      const IPShellFractureCohesive *ipvwf = dynamic_cast<const IPShellFractureCohesive*>(ipvm); // broken via minus (OK broken on both sides)
      broken = ipvwf->broken();
    }
    _mlawMinus->reduction((*vips)[i],_vnhatmean,_vmhatmean,_fulldg);
    _mlawPlus->reduction((*vips)[i+npts],_vnhatp,_vmhatp,_fulldg);
    // mean value
    for(int a=0;a<2;a++){
      _vmhatmean[a]+=_vmhatp[a];
      _vmhatmean[a]*=0.5;
      for(int b=0;b<2;b++)
        mhatmean(a,b) = dot(_vmhatmean[a],lbsm->dualBasisVector(b));
      if(_fulldg){
        _vnhatmean[a]+=_vnhatp[a];
        _vnhatmean[a]*=0.5;
        for(int b=0;b<2;b++)
          nhatmean(a,b) = dot(_vnhatmean[a],lbsm->dualBasisVector(b));
      }
    }
    //Compute the position (u,v) in the element of the Gauss point (to know where evaluate the shape functions)
    iele->getuvOnElem(u,uem,vem,uep,vep);
    // Compute of gradient and hessian of shape functions on interface element and on elements
// For now compute grad on interface as for minus elem CHANGE THIS (own space on interface)
    Grads_m.clear(); Grads_p.clear();
    _minusSpace->gradfuvw(iele->getElem(0),uem, vem, w, Grads_m); // w = 0
    _plusSpace->gradfuvw(iele->getElem(1),uep, vep, w, Grads_p);

    wJ = weight*lbsm->getJacobian();
    // Compute of Deltat_tilde
    _nvar.setDeltatTilde(lbm,Grads_m,nbFF_m,lbp,Grads_p,nbFF_p);

    double wJnucomp[2];
    for(int alpha=0;alpha<2;alpha++)
       wJnucomp[alpha] = -wJ*lbsm->getMinusNormal(alpha);    //scaldot(lb[2]->basisVector(1),lb[2]->basisVector(alpha));
    // Assemblage
    malpha = wJnucomp[0]*(mhatmean(0,0)*lbsm->basisVector(0)+mhatmean(1,0)*lbsm->basisVector(1)) +
             wJnucomp[1]*(mhatmean(0,1)*lbsm->basisVector(0)+mhatmean(1,1)*lbsm->basisVector(1));

    for(int j=0;j<nbFF_m;j++){
      matTvectprod(_nvar.deltatTildeMinus(j),malpha,dt);
      for(int k=0;k<3;k++)
        m(j+k*nbFF_m)-= dt[k];
    }
    for(int j=0;j<nbFF_p;j++){
      matTvectprod(_nvar.deltatTildePlus(j),malpha,dt);
      for(int k=0;k<3;k++)
        m(j+k*nbFF_p+nbdof_m)+= dt[k];
    }

    // Compatibility and stability (if not broken)
    if(!broken){
      Bp->hatBending(lbp,_Bphat);
      Bm->hatBending(lbm,_Bmhat);

      // Compute of Hooke hat tensor in bending on minus and plus element
      _mlawMinus->shellBendingStiffness(ipvm,H_m);
      _mlawPlus->shellBendingStiffness(ipvp,H_p);
      // hat operation
      H_m.hat(lbm);
      H_p.hat(lbp);
      Hmean.mean(&H_m,&H_p);

      // Assemblage (compatibility and stability)
      // v = (diaprod(a,b) *c + diaprod(a,d)*e) --> v_i = a_i*dot(b,c)*dot(d,e)
      // Moreover (m*a) dot(c) + (n*a) dot(d) = (mijcj+nijdj)ai
      // be aware that matrix have to be transpose ?? Why [k][0] and not [0][k] ??
      _nvar.jump(datafield,rjump);

      // dot product of rjump and local vector of interface's basis and multiplications by jacobian and weight
      for(int a=0;a<2;a++)
        rjumpdotphi0[a] = wJ*dot(rjump,lbsm->basisVector(a));

      // Assembly compatibility
      double buildfactor[2][2];
      // minus
      for(int c=0;c<2;c++)
        for(int d=0;d<2;d++)
          buildfactor[c][d] = -0.5*((rjumpdotphi0[0]*H_m(0,0,c,d)+rjumpdotphi0[1]*H_m(1,0,c,d))*lbsm->getMinusNormal(0)+
                                    (rjumpdotphi0[0]*H_m(0,1,c,d)+rjumpdotphi0[1]*H_m(1,1,c,d))*lbsm->getMinusNormal(1)); // minus because -nu^-(beta) with the chosen convention
      for(int j=0;j<nbFF_m;j++){
        vbuild1.scale(0.);
        for(int c=0;c<2;c++)
          for(int d=0;d<2;d++){
            vbuild1(0)+= buildfactor[c][d]*_Bmhat.getBending(j,0,c,d) ; vbuild1(1)+=buildfactor[c][d]*_Bmhat.getBending(j,1,c,d); vbuild1(2)+=buildfactor[c][d]*_Bmhat.getBending(j,2,c,d);
          }
        for(int k=0;k<3;k++)
          m(j+k*nbFF_m) += vbuild1(k);
      }

      // plus
      for(int c=0;c<2;c++)
        for(int d=0;d<2;d++)
          buildfactor[c][d] = -0.5*((rjumpdotphi0[0]*H_p(0,0,c,d)+rjumpdotphi0[1]*H_p(1,0,c,d))*lbsm->getMinusNormal(0)+
                                    (rjumpdotphi0[0]*H_p(0,1,c,d)+rjumpdotphi0[1]*H_p(1,1,c,d))*lbsm->getMinusNormal(1)); // minus because -nu^-(beta) with the chosen convention
      for(int j=0;j<nbFF_p;j++){
        vbuild1.scale(0.);
        for(int c=0;c<2;c++)
          for(int d=0;d<2;d++){
            vbuild1(0)+= buildfactor[c][d]*_Bphat.getBending(j,0,c,d) ; vbuild1(1)+=buildfactor[c][d]*_Bphat.getBending(j,1,c,d); vbuild1(2)+=buildfactor[c][d]*_Bphat.getBending(j,2,c,d);
        }
        for(int k=0;k<3;k++)
          m(j+k*nbFF_p+nbdof_m) += vbuild1(k);
      }

      // Assembly stability
      for(int a=0;a<2;a++)
        rjumpdotphi0[a]*=-Bhs;
        SVector3 rHphi0c = (((rjumpdotphi0[0]*Hmean(0,0,0,0)+rjumpdotphi0[1]*Hmean(1,0,0,0))*lbsm->getMinusNormal(0)+
                             (rjumpdotphi0[0]*Hmean(0,0,0,1)+rjumpdotphi0[1]*Hmean(1,0,0,1))*lbsm->getMinusNormal(1))*lbsm->getMinusNormal(0)+
                            ((rjumpdotphi0[0]*Hmean(0,1,0,0)+rjumpdotphi0[1]*Hmean(1,1,0,0))*lbsm->getMinusNormal(0)+
                             (rjumpdotphi0[0]*Hmean(0,1,0,1)+rjumpdotphi0[1]*Hmean(1,1,0,1))*lbsm->getMinusNormal(1))*lbsm->getMinusNormal(1)) * lbsm->basisVector(0) +
                           (((rjumpdotphi0[0]*Hmean(0,0,1,0)+rjumpdotphi0[1]*Hmean(1,0,1,0))*lbsm->getMinusNormal(0)+
                             (rjumpdotphi0[0]*Hmean(0,0,1,1)+rjumpdotphi0[1]*Hmean(1,0,1,1))*lbsm->getMinusNormal(1))*lbsm->getMinusNormal(0)+
                            ((rjumpdotphi0[0]*Hmean(0,1,1,0)+rjumpdotphi0[1]*Hmean(1,1,1,0))*lbsm->getMinusNormal(0)+
                             (rjumpdotphi0[0]*Hmean(0,1,1,1)+rjumpdotphi0[1]*Hmean(1,1,1,1))*lbsm->getMinusNormal(1))*lbsm->getMinusNormal(1)) * lbsm->basisVector(1); // -- = + for the two getMinusNormal

      // Minus
      for(int j=0;j<nbFF_m;j++){
        matTvectprod(_nvar.deltatTildeMinus(j),rHphi0c,vbuild1);
        for(int k=0;k<3;k++)
          m(j+k*nbFF_m) += vbuild1(k);
      }
      // Plus
      for(int j=0;j<nbFF_p;j++){
        matTvectprod(_nvar.deltatTildePlus(j),rHphi0c,vbuild1);
       for(int k=0;k<3;k++)
         m(j+k*nbFF_p+nbdof_m) -= vbuild1(k);
      }
    }

    // Add extra terms for fullDg formulation
    if(_fulldg){
      // Shape functions evaluated in u,v
      Val_m.clear(); Val_p.clear();
      _minusSpace->fuvw(iele->getElem(0),uem, vem, w, Val_m);
      _plusSpace->fuvw(iele->getElem(1),uep, vep, w, Val_p);

      // n^a terms
      // Assemblage (Consistency)
      nalpha = wJnucomp[0]*(nhatmean(0,0)*lbsm->basisVector(0) + nhatmean(0,1)*lbsm->basisVector(1)) +
               wJnucomp[1]*(nhatmean(1,0)*lbsm->basisVector(0) + nhatmean(1,1)*lbsm->basisVector(1));
      for(int j=0;j<nbFF_m;j++)
        for(int k=0;k<3;k++)
           m(j+k*nbFF_m) -= nalpha(k)*Val_m[j];
      for(int j=0;j<nbFF_p;j++)
        for(int k=0;k<3;k++)
          m(j+k*nbFF_p+nbdof_m)+= nalpha(k)*Val_p[j];

      // Bnhat
      Bp->hatMembrane(lbp,_Bphat);
      Bm->hatMembrane(lbm,_Bmhat);


      // Compatibility and stability (if not broken)
      if(!broken){
        // compute jump of u
        displacementjump(Val_m,nbFF_m,Val_p,nbFF_p,datafield,ujump);
        // Hooke tensor in tension (symmetrization is for elastic part)
        _mlawMinus->shellMembraneStiffness(ipvm,H_m);
        _mlawMinus->shellShearingStiffness(ipvm,Hs_m);
        _mlawPlus->shellMembraneStiffness(ipvp,H_p);
        _mlawPlus->shellShearingStiffness(ipvp,Hs_p);
        // hat operation
        H_m.hat(lbm);
        H_p.hat(lbp);
        Hmean.mean(&H_m,&H_p);
        Hsmean.mean(Hs_m,Hs_p);

        // Put basis vector of interface under fullVector<double> format
        double ujumpphi0[2];
        for(int beta=0;beta<2;beta++)
          ujumpphi0[beta] = dot(ujump,lbsm->basisVector(beta));
        // buildfactor indepandent of shape function
        double buildfactor[2][2];
        for(int gamma=0;gamma<2;gamma++)
          for(int delta=0;delta<2;delta++)
            buildfactor[gamma][delta] = 0.5*((wJnucomp[0]*H_m(0,0,gamma,delta) + wJnucomp[1]*H_m(1,0,gamma,delta))*ujumpphi0[0]+
                                             (wJnucomp[0]*H_m(0,1,gamma,delta) + wJnucomp[1]*H_m(1,1,gamma,delta))*ujumpphi0[1]);

        // Assemblage Compatibility minus
        for(int j=0;j<nbFF_m;j++){
          vbuild1.scale(0.);
          for(int gamma=0;gamma<2;gamma++){
            for(int delta=0;delta<2;delta++){
              vbuild1(0) += buildfactor[gamma][delta]*_Bmhat.getMembrane(j,0,gamma,delta);
              vbuild1(1) += buildfactor[gamma][delta]*_Bmhat.getMembrane(j,1,gamma,delta);
              vbuild1(2) += buildfactor[gamma][delta]*_Bmhat.getMembrane(j,2,gamma,delta);
            }
          }
          for(int jj=0;jj<3;jj++)
            m(j+jj*nbFF_m)+=vbuild1(jj);
        }
        // Assemblage Compatibility plus
        // factor independant of shape functions
        for(int gamma=0;gamma<2;gamma++)
          for(int delta=0;delta<2;delta++)
            buildfactor[gamma][delta] = 0.5*((wJnucomp[0]*H_p(0,0,gamma,delta) + wJnucomp[1]*H_p(1,0,gamma,delta))*ujumpphi0[0]+
                                             (wJnucomp[0]*H_p(0,1,gamma,delta) + wJnucomp[1]*H_p(1,1,gamma,delta))*ujumpphi0[1]);
        for(int j=0;j<nbFF_p;j++){
          vbuild1.scale(0.);
          for(int gamma=0;gamma<2;gamma++){
            for(int delta=0;delta<2;delta++){
                vbuild1(0) += buildfactor[gamma][delta]*_Bphat.getMembrane(j,0,gamma,delta);
                vbuild1(1) += buildfactor[gamma][delta]*_Bphat.getMembrane(j,1,gamma,delta);
                vbuild1(2) += buildfactor[gamma][delta]*_Bphat.getMembrane(j,2,gamma,delta);
              }
          }
          for(int jj=0;jj<3;jj++)
            m(j+jj*nbFF_p+nbdof_m)+=vbuild1(jj);
        }

        // Assembly stability
        mbuildterm.scale(0.);
        for(int alpha=0;alpha<2;alpha++){
          for(int beta=0;beta<2;beta++)
            for(int gamma=0;gamma<2;gamma++)
              for(int delta=0;delta<2;delta++){
                diaprod(lbsm->basisVector(beta),lbsm->basisVector(delta),mbuildterm2);
                double stabfactor = Hmean(alpha,beta,gamma,delta)*lbsm->getMinusNormal(alpha)*lbsm->getMinusNormal(gamma);      // -- (-scaldot(lb[2]->basisVector(1),lb[2]->basisVector(alpha)))*(-scaldot(lb[2]->basisVector(1),lb[2]->basisVector(gamma)));
                mbuildterm.axpy(mbuildterm2,stabfactor);
              }
        }
        double buildfactor1 = wJ*B2hs;
        mbuildterm.scale(buildfactor1); // multiplication by weight, jacobian, and Beta/h_s
        mbuildterm.mult(ujump,vbuild1);
        for(int j=0;j<nbFF_m;j++)
          for(int jj=0;jj<3;jj++)
            m(j+jj*nbFF_m)-= Val_m[j]*vbuild1(jj);
        for(int j=0;j<nbFF_p;j++)
          for(int jj=0;jj<3;jj++)
            m(j+jj*nbFF_p+nbdof_m)+= Val_p[j]*vbuild1(jj);


        // Out of Plane term
        double Cst = wJ*B3hs ;
        // compute B vector
        compute_Bs(lbsm,Val_m,nbFF_m,Bs_m);
        compute_Bs(lbsm,Val_p,nbFF_p,Bs_p);

        // compute normal displacement jump
        buildfactor1=0.;
        for(int j=0;j<nbFF_m;j++)
          buildfactor1+=Bs_m[j][0]*datafield(j)+Bs_m[j][1]*datafield(j+nbFF_m)+Bs_m[j][2]*datafield(j+nbFF_m+nbFF_m);
        buildfactor1= - buildfactor1;
        for(int j=0;j<nbFF_p;j++)
          buildfactor1+=Bs_p[j][0]*datafield(j+nbdof_m)+Bs_p[j][1]*datafield(j+nbFF_p+nbdof_m)+Bs_p[j][2]*datafield(j+nbFF_p+nbFF_p+nbdof_m);

        double buildfactor2=0.;
        for(int alpha=0;alpha<2;alpha++)
          for(int beta=0;beta<2;beta++)
            buildfactor2+= Hsmean(alpha,beta)*lbsm->getMinusNormal(alpha)*lbsm->getMinusNormal(beta);
        buildfactor1*=buildfactor2*Cst;

        for(int j=0;j<nbFF_m;j++){
          vbuild1(0) = Bs_m[j][0]; vbuild1(1) = Bs_m[j][1]; vbuild1(2) = Bs_m[j][2];
          vbuild1.scale(buildfactor1);
          for(int jj=0;jj<3;jj++)
            m(j+jj*nbFF_m) -= vbuild1(jj);
        }
        for(int j=0;j<nbFF_p;j++){
          vbuild1(0) = Bs_p[j][0]; vbuild1(1) = Bs_p[j][1]; vbuild1(2) = Bs_p[j][2];
          vbuild1.scale(buildfactor1);
          for(int jj=0;jj<3;jj++)
            m(j+jj*nbFF_p+nbdof_m) += vbuild1(jj);
        }
      }
    }
  }
}
*/
void IsotropicStiffVirtualInterfaceTermC0DgShell::get(MElement *ele,int npts,IntPt *GP,fullMatrix<double> &m) const
 {
  if (sym)
  {
    // data initialization
    iele = dynamic_cast<MInterfaceLine*>(ele);
    nbdof_m = BilinearTerm<double,double>::space1.getNumKeys(iele->getElem(0));
    nbFF_m = nbdof_m/3;
    // Initialization
    m.resize(nbdof_m, nbdof_m,true); // true --> setAll(0.)
    // Characteristic size of interface element
    h_s = iele->getCharacteristicSize();
    Bhs = beta1/h_s;
    // get data of gauss' points
//    ipf->getIpvData(iele,et,IPState::current,vlbm,vlbs,vBm);
    ipf->getIPv(iele,vipvm);
    // Gauss point on minus element;
    IntPt *GPm; IntPt *GPp;
    _interQuad->getIntPoints(iele,GP,&GPm,&GPp);
    // get the shape function (and derivative values)
    nlsFunctionSpace<double>* sp1 = static_cast<nlsFunctionSpace<double>*>(&(this->space1));
    std::vector<GaussPointSpaceValues<double>*> vgpsm;
    sp1->get(iele->getElem(0),npts,GPm,vgpsm);
    // sum on Gauss' points
    for (int i = 0; i < npts; i++)
    {
      // Coordonate of Gauss' point i
      //u = GP[i].pt[0]; v = GP[i].pt[1]; w = GP[i].pt[2];
      // Weight of Gauss' point i
      weight = GP[i].weight;

      const linearShellLocalBasisBulk *lbm = static_cast<linearShellLocalBasisBulk*>(vipvm[i]->getshellLocalBasis());
      const linearShellLocalBasisInter *lbs = static_cast<linearShellLocalBasisInter*>(vipvm[i]->getshellLocalBasisOfInterface());
      Bvector *Bm = vipvm[i]->getBvector();
      //printf("Abscisse of gauss point %f\n",u);
      //Compute the position (u,v) in the element of the Gauss point (to know where evaluate the shape functions)
 //     double uem = GPm[i].pt[0]; double vem = GPm[i].pt[1];
 //     iele->getuvwOnElem(u,v,uem,vem,wem,uep,vep,wep);
      //printf("Position (u,v) of minus element (%f,%f)\n",uem,vem);
      // Compute of gradient and hessian of shape functions on interface element and on elements
      // ATTENTION after multiplication by multipliers (functionspace 276) components are in the "second line of tensor change this ??
 //     Grads_m.clear();
 //     BilinearTerm<double,double>::space1.gradf(iele->getElem(0),uem, vem, w, Grads_m); // w = 0
      std::vector<TensorialTraits<double>::GradType> &Grads_m = vgpsm[i]->_vgrads;
      wJ = weight * lbs->getJacobian();
      Bm->hatBending(lbm,Bhat_m);
      // Compute of Hooke hat tensor in bending on minus element
      _mlaw->shellBendingStiffness(vipvm[i],H_m);
      H_m.hat(lbm);
      // Compute of Deltat_tilde
    _nvar.setDeltatTilde(lbm,Grads_m,nbFF_m);
    _nvar.bound(lbs);

      // Assembly consistency
      for(int j=0;j<nbFF_m;j++){
        for(int k=0;k<nbFF_m;k++){
//          consC0ShellStiffnessTerms(&H_m,k,Bhat_m,_nvar.deltatTildeMinus(j),vlbm[i],vlbs[i],me_cons);
            for(int ii=0;ii<3;ii++) for(int j=0;j<3;j++) me_cons[ii][j]=0.;
            double v1[3], v2[3],v3[3];
            double temp2=0.;
            for(int beta=0;beta<2;beta++){
              matTvectprod(_nvar.deltatTildeMinus(j) ,lbs->basisVector(beta),v2);
              v1[0]=0.; v1[1]=0.; v1[2]=0.;
              v3[0]=0.; v3[1]=0.; v3[2]=0.;
              for(int gamma=0;gamma<2;gamma++)
                for(int delta=0;delta<2;delta++){
                  temp2 = -0.5*(lbs->getMinusNormal(0)*H_m(0,beta,gamma,delta)+lbs->getMinusNormal(1)*H_m(1,beta,gamma,delta));
                  v1[0] += temp2*Bhat_m.getBending(k,0,gamma,delta); v1[1] += temp2*Bhat_m.getBending(k,1,gamma,delta); v1[2] += temp2*Bhat_m.getBending(k,2,gamma,delta);
                }
              diaprodAdd(v2,v1,me_cons);
            }

          for(int jj=0;jj<3;jj++)
            for(int kk=0;kk<3;kk++)
              m(j+jj*nbFF_m,k+kk*nbFF_m) += -wJ*me_cons[jj][kk];
        }
      }

      // Assembly compatibility and stability
      for(int j=0;j<nbFF_m;j++){
        for(int k=0;k<nbFF_m;k++){
          compC0ShellStiffnessTerms(&H_m,j,Bhat_m,_nvar.deltatTildeMinus(k),lbm,lbs,me_comp);
          stabilityC0ShellStiffnessTerms(&H_m,_nvar.deltatTildeMinus(j), _nvar.deltatTildeMinus(k),lbs,me_stab);
          for(int jj=0;jj<3;jj++)
            for(int kk=0;kk<3;kk++)
              m(j+jj*nbFF_m,k+kk*nbFF_m) += -wJ*(me_comp[jj][kk] - Bhs * me_stab[jj][kk]);
        }
      }
  }

//   m.print("Virtual InterfaceBound");
  }
  else
    printf("not implemented\n");
}

void IsotropicElasticForceVirtualInterfaceTermC0DgShell::get(MElement *ele,int npts,IntPt *GP,fullVector<double> &m) const
{
  // data initialization
  // Retrieve of the element link to the interface element velem[0] == minus elem ; velem == plus elem
  const fullVector<double> &datafield = *_data;
  iele = dynamic_cast<MInterfaceLine*>(ele);
  nbdof_m = DgC0LinearTerm<double>::space1.getNumKeys(iele->getElem(0));
  nbFF_m = nbdof_m/3;
  // Initialization
  m.resize(nbdof_m);
  m.scale(0.);

  // resize matrix m1 and m2 (normally reallocation OK because same element ...)
  int tnpts = 3*npts;
  int septnpts = tnpts+tnpts+npts;
  m1.resize(septnpts); // set operation after --> no need to set to zero here
  m2.resize(septnpts,nbdof_m,false);  // set operation after --> no need to set to zero here

  // Characteristic size of interface element
  h_s = iele->getCharacteristicSize();
  Bhs = beta1/h_s;

  // get IPv data
  AllIPState::ipstateElementContainer* vips = ipf->getAips()->getIPstate(iele->getNum());
  // Gauss Points values on minus element
  IntPt *GPm; IntPt *GPp;
  _interQuad->getIntPoints(iele,GP,&GPm,&GPp);

  // get the shape function (and derivative values)
  nlsFunctionSpace<double>* sp1 = static_cast<nlsFunctionSpace<double>*>(&(this->space1));
  std::vector<GaussPointSpaceValues<double>*> vgpsm;
  sp1->get(iele->getElem(0),npts,GPm,vgpsm);

  // build m1 and m2
  for(int i = 0; i < npts; i++)
  {
    // Coordonate of Gauss' point i
    //u = GP[i].pt[0]; v = GP[i].pt[1]; w = GP[i].pt[2];
    // Weight of Gauss' point i
    weight = GP[i].weight;
    const IPStateBase *ips = (*vips)[i];
    const IPVariableShell *ipv = static_cast<IPVariableShell*>(ips->getState(IPStateBase::current));
    const linearShellLocalBasisBulk *lbm = static_cast<const linearShellLocalBasisBulk*>(ipv->getshellLocalBasis());
    const linearShellLocalBasisInter *lbs = static_cast<const linearShellLocalBasisInter*>(ipv->getshellLocalBasisOfInterface());
    const Bvector *Bm = ipv->getBvector();

    _mlaw->reduction(ips,vnhat,vmhat,false);
    // To have mean must be divided by 2
    for(int alpha=0;alpha<2;alpha++){
      vmhat[alpha]*=0.5;
    }
    //printf("Abscisse of gauss point %f\n",u);
    //Compute the position (u,v) in the element of the Gauss point (to know where evaluate the shape functions)
    //double uem = GPm[i].pt[0]; double vem = GPm[i].pt[1];
//    iele->getuvwOnElem(u,v,uem,vem,wem,uep,vep,wep);
    // Compute of gradient and hessian of shape functions on interface element and on elements
    // ATTENTION after multiplication by multipliers (functionspace 276) components are in the "second line of tensor change this ??
    //Grads_m.clear();
    //DgC0LinearTerm<double>::space1.gradf(iele->getElem(0),uem, vem, w, Grads_m); // w = 0
    std::vector<TensorialTraits<double>::GradType> &Grads_m = vgpsm[i]->_vgrads;
     // get data
     wJ=weight * lbs->getJacobian();

    // Compute of Deltat_tilde
    nvar.setDeltatTilde(lbm,Grads_m,nbFF_m);
    nvar.bound(lbs);
    nvar.jump(datafield,rjump);


    // Hooke tensor
    _mlaw->shellBendingStiffness(ipv,H_m);
    H_m.hat(lbm);

    // Compute of Bhat vector (1 component for now because 1 dof (z) ) --> it's a vector of length == nbFF
    Bm->hatBending(lbm,Bhat_m);

    // Consistency and stability (regrouped because multiplication by Deltat for both)
    double wJnucomp[2];
    for(int alpha=0;alpha<2;alpha++){
      wJnucomp[alpha] = wJ*lbs->getMinusNormal(alpha);
      rjumpdotphi0[alpha] = dot(rjump,lbs->basisVector(alpha));
    }
    // multiplication for stability and compatibility
    SVector3 bphi0c;
    double buildfactor2;
    buildfactor2 = (rjumpdotphi0[0]*H_m(0,0,0,0)+ rjumpdotphi0[1]*H_m(0,1,0,0))*wJnucomp[0] +
                   (rjumpdotphi0[0]*H_m(1,0,0,0)+ rjumpdotphi0[1]*H_m(1,1,0,0))*wJnucomp[1];
    m1(tnpts+4*i) = -0.5*buildfactor2;
    buildfactor1 = buildfactor2*lbs->getMinusNormal(0);
    buildfactor2 = (rjumpdotphi0[0]*H_m(0,0,0,1)+ rjumpdotphi0[1]*H_m(0,1,0,1))*wJnucomp[0] +
                   (rjumpdotphi0[0]*H_m(1,0,0,1)+ rjumpdotphi0[1]*H_m(1,1,0,1))*wJnucomp[1];
    m1(tnpts+4*i+1) = -0.5*buildfactor2;
    buildfactor1+=buildfactor2*lbs->getMinusNormal(1);
    bphi0c = lbs->basisVector(0)*buildfactor1;

    buildfactor2 = (rjumpdotphi0[0]*H_m(0,0,1,0)+ rjumpdotphi0[1]*H_m(0,1,1,0))*wJnucomp[0] +
                   (rjumpdotphi0[0]*H_m(1,0,1,0)+ rjumpdotphi0[1]*H_m(1,1,1,0))*wJnucomp[1];
    m1(tnpts+4*i+2) = -0.5*buildfactor2;
    buildfactor1 = buildfactor2*lbs->getMinusNormal(0);
    buildfactor2 = (rjumpdotphi0[0]*H_m(0,0,1,1)+ rjumpdotphi0[1]*H_m(0,1,1,1))*wJnucomp[0] +
                   (rjumpdotphi0[0]*H_m(1,0,1,1)+ rjumpdotphi0[1]*H_m(1,1,1,1))*wJnucomp[1];
    m1(tnpts+4*i+3) = -0.5*buildfactor2;
    buildfactor1+=buildfactor2*lbs->getMinusNormal(1);
    bphi0c += lbs->basisVector(1)*buildfactor1;
    bphi0c*=(-Bhs);

    for(int k=0;k<3;k++){
      m1(3*i+k) = wJnucomp[0]*vmhat[0](k) + wJnucomp[1]*vmhat[1](k) + bphi0c[k];
      for(int j=0;j<nbFF_m;j++){
        m2.copy(nvar.deltatTildeMinus(j),0,3,k,1,3*i,k*nbFF_m+j);
      }
    }
    m2.copy(Bhat_m.getBending(),0,4,0,nbdof_m,tnpts+4*i,0);
  }
  m2.multWithATranspose(m1,1,1,m);
};


/* Old formulation of interface force terms. keep for school understanding
void IsotropicForceInterfaceTermC0DgShell::get(MElement *ele,int npts,IntPt *GP,fullVector<double> &m)
{
  // data initialization
  // Retrieve of the element link to the interface element velem[0] == minus elem ; velem == plus elem
  MInterfaceElement *iele = dynamic_cast<MInterfaceElement*>(ele);
  MElement ** velem = iele->getElem();
  const int nbdof_m = DgC0LinearTerm<SVector3>::space1.getNumKeys(velem[0]);
  const int nbdof_p = DgC0LinearTerm<SVector3>::space1.getNumKeys(velem[1]);
  const int nbFF_m=nbdof_m/3;
  const int nbFF_p=nbdof_p/3;
  // Initialization
  m.resize(nbdof_m+nbdof_p);
  m.scale(0.);
  double uem,uep,vem,vep;
  std::vector<TensorialTraits<double>::ValType> Val_m, Val_p;
  std::vector<TensorialTraits<double>::GradType> Grads_m;
  std::vector<TensorialTraits<double>::GradType> Grads_p;
  std::vector<TensorialTraits<double>::GradType> Grads;
  std::vector<TensorialTraits<double>::HessType> Hess_m;
  std::vector<TensorialTraits<double>::HessType> Hess_p;
  const shellLocalBasis* lb[3];
  double Bhat_p[256][3][2][2],Bhat_m[256][3][2][2], Bm_p[256][3][2][2],Bm_m[256][3][2][2];
  double Bs_p[256][3],Bs_m[256][3];
  LinearElasticShellHookeTensor H_p, H_m,Hmean;
  linearElasticShellShearingHookeTensor Hs_m,Hs_p,Hsmean;
  double Deltat_m[256][3][3], Deltat_p[256][3][3];
  double wJ;
  double me_cons[3];
  double me_comp[3];
  double me_stab[3];
  reductionElement nhatmean, mhatmean;
  SVector3 ujump;
  std::vector<bool> vbroken;
  std::vector<bool> vDeltanNeg;

  // Characteristic size of interface element
  double h_s = iele->getCharacteristicSize();

  const double Bhs = beta1/h_s;
  const double B2hs= beta2/h_s;
  const double B3hs= beta3/h_s;
  // displacement
  std::vector<double> disp;
  if(MatrixByPerturbation){
    ufield->getForPerturbation(DgC0LinearTerm<SVector3>::space1,iele,minus,pertDof,pert,disp);
  }
  else{
    std::vector<Dof> R;
    DgC0LinearTerm<SVector3>::space1.getKeys(iele,R);
    ufield->get(R,disp);
  }

  // sum on Gauss' points
  ipf->getBroken(iele,npts,_elemtype,vbroken,vDeltanNeg);
  //for(int i=0;i<npts;i++){ if(vbroken[i]) printf("%d true\n",iele->getNum()); else printf("%d false\n",iele->getNum());}
  for (int i = 0; i < npts; i++)
  {
    // Coordonate of Gauss' point i
    const double u = GP[i].pt[0]; const double v = GP[i].pt[1]; const double w = GP[i].pt[2];
    // Weight of Gauss' point i
    const double weight = GP[i].weight;
    //Compute the position (u,v) in the element of the Gauss point (to know where evaluate the shape functions)
    iele->getuvOnElem(u,uem,vem,uep,vep);
    // Compute of gradient and hessian of shape functions on interface element and on elements
    // ATTENTION after multiplication by multipliers (functionspace 276) components are in the "second line of tensor change this ??
    DgC0LinearTerm<SVector3>::space1.gradfuvw(iele,u, v, w, Grads); // grad of shape fonction on interface element
    DgC0LinearTerm<SVector3>::space1.gradfuvw(velem[0],uem, vem, w, Grads_m); // w = 0
    DgC0LinearTerm<SVector3>::space1.gradfuvw(velem[1],uep, vep, w, Grads_p);
    DgC0LinearTerm<SVector3>::space1.hessfuvw(velem[0],uem, vem, w, Hess_m);
    DgC0LinearTerm<SVector3>::space1.hessfuvw(velem[1],uep, vep, w, Hess_p);

    // m^a terms (Consistency) and get data
    if(!fullDg) ipf->getMomentReductionAndshellLocalBasis(iele,i,npts,_elemtype,IPState::current,mhatmean,lb);
    else ipf->getReductionAndshellLocalBasis(iele,i,npts,_elemtype,IPState::current,nhatmean,mhatmean,lb,_mlaw);

    wJ = weight*lb[2]->getJacobian();
    // Compute of Deltat_tilde
    compute_Deltat_tilde(lb[1],Grads_p,nbFF_p,Deltat_p);
    compute_Deltat_tilde(lb[0],Grads_m,nbFF_m,Deltat_m);

    // Assemblage
    double dt[3];
    for(int alpha=0;alpha<2;alpha++){
      SVector3 malpha = mhatmean(0,alpha)*lb[2]->basisVector(0)+mhatmean(1,alpha)*lb[2]->basisVector(1);
      for(int j=0;j<nbFF_m;j++){
        matTvectprod(Deltat_m[j],malpha,dt);
        for(int k=0;k<3;k++)
          m(j+k*nbFF_m)+= - (wJ*dt[k]*(-scaldot(lb[2]->basisVector(1),lb[2]->basisVector(alpha))));
      }
      for(int j=0;j<nbFF_p;j++){
        matTvectprod(Deltat_p[j],malpha,dt);
        for(int k=0;k<3;k++)
          m(j+k*nbFF_p+nbdof_m)+= (wJ*dt[k]*(-scaldot(lb[2]->basisVector(1),lb[2]->basisVector(alpha))));
      }
    }

    // Compatibility and stability (if not broken)
    if(!vbroken[i]){
      Compute_Bm(lb[1],Grads_p,Hess_p,nbFF_p,Bm_p);
      Compute_Bm(lb[0],Grads_m,Hess_m,nbFF_m,Bm_m);
      compute_Bhat(lb[1],nbFF_p,Bm_p,Bhat_p);
      compute_Bhat(lb[0],nbFF_m,Bm_m,Bhat_m);

      // Compute of Hooke hat tensor in bending on minus and plus element
      double tbf = h*h*h/12.;
      if(_mlaw->getType() == materialLaw::linearElasticPlaneStress){
        linearElasticShellLaw *mlt = static_cast<linearElasticShellLaw*>(_mlaw);
        mlt->shellStiffness(tbf,lb[0],H_m);
        mlt->shellStiffness(tbf,lb[1],H_p);
      }
      else if(_mlaw->getType() == materialLaw::linearElasticPlaneStressWithFracture){
        linearElasticShellLaw *mlt = static_cast<linearElasticShellLaw*>(_mlaw);
        mlt->shellStiffness(tbf,lb[0],H_m);
        mlt->shellStiffness(tbf,lb[1],H_p);
      }
      else if(_mlaw->getType() == materialLaw::linearElasticOrthotropicPlaneStress){
        linearElasticOrthotropicShellLaw* mlt = static_cast<linearElasticOrthotropicShellLaw*>(_mlaw);
        mlt->shellStiffness(tbf,lb[0],H_m);
        mlt->shellStiffness(tbf,lb[1],H_p);
      }
      else{
        Msg::Error("Impossible to compute stiffness because method are not given for material law %d",_mlaw->getType());
        m.setAll(0.);
        return;
      }
      // hat operation
      H_m.hat(lb[0]);
      H_p.hat(lb[1]);
      Hmean.mean(&H_m,&H_p);

      // Assemblage (compatibility and stability)
      for(int j=0;j<nbFF_m;j++){
        compC0ShellForceTerms(&H_m,nbFF_m,nbFF_p,Bhat_m[j],Deltat_m,Deltat_p,lb[2],disp,me_comp);
        stabilityC0ShellForceTerms(nbFF_m,nbFF_p,&Hmean,Deltat_m[j],Deltat_m,Deltat_p,lb[2],disp,me_stab);
        for(int jj=0;jj<3;jj++)
          m(j+jj*nbFF_m) += wJ*( me_comp[jj]- Bhs * me_stab[jj] );
      }
      for(int j=0;j<nbFF_p;j++){
        compC0ShellForceTerms(&H_p,nbFF_m,nbFF_p,Bhat_p[j],Deltat_m,Deltat_p,lb[2],disp,me_comp);
        stabilityC0ShellForceTerms(nbFF_m,nbFF_p,&Hmean,Deltat_p[j],Deltat_m,Deltat_p,lb[2],disp,me_stab);
        for(int jj=0;jj<3;jj++)
          m(j+(jj*nbFF_p)+nbdof_m) += wJ*( me_comp[jj] + Bhs * me_stab[jj]);
      }
    }
    // Add extra terms for fullDg formulation
    if(fullDg){
      // Shape functions evaluated in u,v
      DgC0LinearTerm<SVector3>::space1.fuvw(velem[0],uem, vem, w, Val_m);
      DgC0LinearTerm<SVector3>::space1.fuvw(velem[1],uep, vep, w, Val_p);

      // n^a terms
      //ipf->getStressReduction(iele,i,npts,_elemtype,IPState::current,nhatmean); did before
      // Assemblage (Consistency)
      for(int alpha=0;alpha<2;alpha++){
        SVector3 nalpha = nhatmean(alpha,0) * lb[2]->basisVector(0) + nhatmean(alpha,1)*lb[2]->basisVector(1);
        for(int j=0;j<nbFF_m;j++)
          for(int k=0;k<3;k++)
            m(j+k*nbFF_m)+=- (wJ*nalpha(k)*Val_m[j]*(-scaldot(lb[2]->basisVector(1),lb[2]->basisVector(alpha))));
        for(int j=0;j<nbFF_p;j++)
          for(int k=0;k<3;k++)
            m(j+k*nbFF_p+nbdof_m)+=(wJ*nalpha(k)*Val_p[j]*(-scaldot(lb[2]->basisVector(1),lb[2]->basisVector(alpha))));
      }

      // Compatibility and stability (if not broken)
      if(!vbroken[i]){
        // compute jump of u
        displacementjump(Val_m,nbFF_m,Val_p,nbFF_p,disp,ujump);
        double tmf = h;
        double thick = h;
        // Hooke tensor in membrane and shearing
        if(_mlaw->getType() == materialLaw::linearElasticPlaneStress){
          linearElasticShellLaw *mlt = static_cast<linearElasticShellLaw*>(_mlaw);
          mlt->shellStiffness(tmf,lb[0],H_m);
          mlt->shellStiffness(tmf,lb[1],H_p);
          mlt->shellShearingStiffness(thick,lb[0],Hs_m);
          mlt->shellShearingStiffness(thick,lb[1],Hs_p);
        }
        else if(_mlaw->getType() == materialLaw::linearElasticPlaneStressWithFracture){
          linearElasticShellLaw *mlt = static_cast<linearElasticShellLaw*>(_mlaw);
          mlt->shellStiffness(tmf,lb[0],H_m);
          mlt->shellStiffness(tmf,lb[1],H_p);
          mlt->shellShearingStiffness(thick,lb[0],Hs_m);
          mlt->shellShearingStiffness(thick,lb[1],Hs_p);
        }
        else if(_mlaw->getType() == materialLaw::linearElasticOrthotropicPlaneStress){
          linearElasticOrthotropicShellLaw* mlt = static_cast<linearElasticOrthotropicShellLaw*>(_mlaw);
          mlt->shellStiffness(tmf,lb[0],H_m);
          mlt->shellStiffness(tmf,lb[1],H_p);
          mlt->shellShearingStiffness(thick,lb[0],Hs_m);
          mlt->shellShearingStiffness(thick,lb[1],Hs_p);
        }
        else{
          Msg::Error("Impossible to compute stiffness because method are not given for material law %d",_mlaw->getType());
          m.setAll(0.);
          return;
        }
        // hat operation
        H_m.hat(lb[0]);
        H_p.hat(lb[1]);
        Hmean.mean(&H_m,&H_p);
        Hsmean.mean(Hs_m,Hs_p);

        // Assemblage (Compatibility and stability)
        for(int alpha=0;alpha<2;alpha++){
          for(int beta=0;beta<2;beta++)
            for(int gamma=0;gamma<2;gamma++)
              for(int delta=0;delta<2;delta++){
                stabilityC0ShellForceMembraneTerms(beta,delta,lb[2],ujump,me_stab);
                double tempStab = wJ*Hmean(alpha,beta,gamma,delta)*B2hs*(-scaldot(lb[2]->basisVector(1),lb[2]->basisVector(alpha)))*(-scaldot(lb[2]->basisVector(1),lb[2]->basisVector(gamma)));
                double tempCompm = 0.5*wJ*H_m(alpha,beta,gamma,delta)*(-scaldot(lb[2]->basisVector(1),lb[2]->basisVector(alpha)));
                double tempCompp = 0.5*wJ*H_p(alpha,beta,gamma,delta)*(-scaldot(lb[2]->basisVector(1),lb[2]->basisVector(alpha)));
                for(int j=0;j<nbFF_m;j++){
                  compC0ShellForceMembraneTerms(beta,gamma,delta,lb[0],lb[2],Grads_m[j],ujump,me_comp);
                  for(int jj=0;jj<3;jj++)
                    m(j+jj*nbFF_m)+=(tempCompm*me_comp[jj]-tempStab*Val_m[j]*me_stab[jj]);
                }
                for(int j=0;j<nbFF_p;j++){
                  compC0ShellForceMembraneTerms(beta,gamma,delta,lb[1],lb[2],Grads_p[j],ujump,me_comp);
                  for(int jj=0;jj<3;jj++)
                    m(j+jj*nbFF_p+nbdof_m)+=(tempCompp*me_comp[jj]+tempStab*Val_p[j]*me_stab[jj]);
                }
              }
        }
        // Out of Plane term
        double Cst = wJ*B3hs ;

        // compute B vector
        compute_Bs(lb[2],Val_m,nbFF_m,Bs_m);
        compute_Bs(lb[2],Val_p,nbFF_p,Bs_p);
        for(int j=0;j<nbFF_m;j++){
          stabilityC0ShellForceShearingTerms(Bs_m[j], Bs_m, Bs_p,nbFF_m,nbFF_p,lb[2],Hsmean,disp,me_stab);
          for(int jj=0;jj<3;jj++)
            m(j+jj*nbFF_m) += -Cst * me_stab[jj];
        }
          for(int j=0;j<nbFF_p;j++){
            stabilityC0ShellForceShearingTerms(Bs_p[j],Bs_m,Bs_p,nbFF_m,nbFF_p,lb[2],Hsmean,disp,me_stab);
            for(int jj=0;jj<3;jj++)
              m(j+(jj*nbFF_p)+nbdof_m) += Cst * me_stab[jj];
          }
      }
      Val_m.clear(); Val_p.clear();
    }
    // Because component are push_back in Grads in gradfuvw idem for hess
    Grads_m.clear(); Grads_p.clear(); Hess_m.clear(); Hess_p.clear(); Grads.clear();
  }
}


void IsotropicStiffBulkTermC0DgShell::get(MElement *ele,int npts,IntPt *GP,fullMatrix<double> &m)
{
  if (sym)
  {
    // Initialization of some data
    nbdof = DgC0BilinearTerm<SVector3,SVector3>::space1.getNumKeys(ele);
    nbFF = nbdof/3;
    m.resize(nbdof, nbdof,true); // true --> setAll(0.)

    // get ipvariable data
    ipf->getIpvData(ele,_elt,IPState::current,vlb,vB);

    // sum on Gauss' points
    for (int i = 0; i < npts; i++)
    {
      // Coordonate of Gauss' point i
      u = GP[i].pt[0]; v = GP[i].pt[1]; w = GP[i].pt[2];
      // Weight of Gauss' point i and Jacobian's value at this point
      weight = GP[i].weight;

      // multiplication of constant by detJ and weight
      wJ = vlb[i]->getJacobian() * weight;

    // compute of Hooke tensor
    if(_mlaw->getType() == materialLaw::linearElasticPlaneStress){
      linearElasticShellLaw *mlt = static_cast<linearElasticShellLaw*>(_mlaw);
      mlt->shellStiffness(Cm,vlb[i],Hbend);
      mlt->shellStiffness(Cn,vlb[i],Htens);
    }
    else if(_mlaw->getType() == materialLaw::linearElasticPlaneStressWithFracture){
      linearElasticShellLaw *mlt = static_cast<linearElasticShellLaw*>(_mlaw);
      mlt->shellStiffness(Cm,vlb[i],Hbend);
      mlt->shellStiffness(Cn,vlb[i],Htens);
    }
    else if(_mlaw->getType() == materialLaw::linearElasticOrthotropicPlaneStress){
      linearElasticOrthotropicShellLaw* mlt = static_cast<linearElasticOrthotropicShellLaw*>(_mlaw);
      mlt->shellStiffness(Cm,vlb[i],Hbend);
      mlt->shellStiffness(Cn,vlb[i],Htens);
    }
    else if(_mlaw->getType() == materialLaw::linearElasticPlaneStressWithDamage){
      linearElasticShellLawWithDamage *mlt = static_cast<linearElasticShellLawWithDamage*>(_mlaw);
      IPVariableShellWTIWD *ipv = dynamic_cast<IPVariableShellWTIWD*>(ipf->getIPv(ele,i));
      mlt->shellBendingStiffness(ipv,Hbend);
      mlt->shellTensionStiffness(ipv,Htens);
    }
    else{
      Msg::Error("Impossible to compute stiffness because method are not given for material law %d",_mlaw->getType());
      m.setAll(0.);
      return;
    }

     // loop on SF to construct the elementary (bulk) stiffness matrix at the Gauss' point i
     for(int j=0; j<nbFF;j++)
       for(int k=0;k<nbFF;k++){
        // bending
        for(int alpha=0;alpha<2;alpha++)
          for(int beta=0;beta<2;beta++){
            B1[0]= vB[i]->getBending(k,0,alpha,beta); B1[1]= vB[i]->getBending(k,1,alpha,beta); B1[2]= vB[i]->getBending(k,2,alpha,beta);
            B2[0] = Hbend(alpha,beta,0,0)*vB[i]->getBending(j,0,0,0)+
                    Hbend(alpha,beta,0,1)*vB[i]->getBending(j,0,0,1)+
                    Hbend(alpha,beta,1,0)*vB[i]->getBending(j,0,1,0)
                    Hbend(alpha,beta,1,1)*vB[i]->getBending(j,0,1,1);
            B2[1] = Hbend(alpha,beta,0,0)*vB[i]->getBending(j,1,0,0)+
                    Hbend(alpha,beta,0,1)*vB[i]->getBending(j,1,0,1)+
                    Hbend(alpha,beta,1,0)*vB[i]->getBending(j,1,1,0)
                    Hbend(alpha,beta,1,1)*vB[i]->getBending(j,1,1,1);
            B2[2] = Hbend(alpha,beta,0,0)*vB[i]->getBending(j,2,0,0)+
                    Hbend(alpha,beta,0,1)*vB[i]->getBending(j,2,0,1)+
                    Hbend(alpha,beta,1,0)*vB[i]->getBending(j,2,1,0)
                    Hbend(alpha,beta,1,1)*vB[i]->getBending(j,2,1,1);
            diaprodAdd(B2,B1,wJ,nbFF,j,k,m);
          }
        // membrane
        for(int a=0;a<2;a++)
          for(int b=0;b<2;b++){
            B2[0] = vB[i]->getMembrane(k,0,a,b); B2[1] = vB[i]->getMembrane(k,1,a,b); B2[2]= vB[i]->getMembrane(k,2,a,b); // make a method
            B1[0] = 0.; B1[1] = 0.; B1[2]=0.;
        for(int c=0;c<2;c++)
          for(int d=0;d<2;d++){
            B1[0] += Htens(a,b,c,d)*vB[i]->getMembrane(j,0,c,d); B1[1]+=Htens(a,b,c,d)*vB[i]->getMembrane(j,1,c,d); B1[2]+=Htens(a,b,c,d)*vB[i]->getMembrane(j,2,c,d); //make a method
          }
        diaprodAdd(B1,B2,wJ,nbFF,j,k,m);
      }
       }
    }
//   m.print("bulk");
  }
  else
    printf("not implemented\n");
}


*/
