// Description: Quadrature rules for mixed triangle and quadrangle formulation
//
// Author:  <Gauthier BECKER>, (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
#ifndef QUADRATURETRIQUADMIX_H_
#define QUADRATURETRIQUADMIX_H_
#include "quadratureRules.h"

class quadratureGaussTriQuadMix : public QuadratureBase
{
 protected:
  GaussQuadrature* _quadrule;
  GaussQuadrature* _trirule;
 public:
  quadratureGaussTriQuadMix(const int ntri,const int nquad)
  {
    _trirule = new GaussQuadrature(ntri);
    _quadrule= new GaussQuadrature(nquad);
  }
  quadratureGaussTriQuadMix(const quadratureGaussTriQuadMix &source)
  {
    _quadrule = new GaussQuadrature(*source._quadrule);
    _trirule = new GaussQuadrature(*source._trirule);
  }
  ~quadratureGaussTriQuadMix()
  {
    delete _quadrule;
    delete _trirule;
  }
  virtual int getIntPoints(MElement *e, IntPt **GP)
  {
    // check tri or quad (use the number of primary vertices 3 for triangle and 4 for quadrangle)
    if(e->getNumPrimaryVertices() ==3)
    {
      return _trirule->getIntPoints(e,GP);
    }
    else
    {
      return _quadrule->getIntPoints(e,GP);
    }
  }
} ;

#endif // QUADRATURETRIQUADMIX_H_
