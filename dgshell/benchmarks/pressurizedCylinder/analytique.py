#-*-coding:Utf-8-*-

# formula see "cylindre mince sous pression" dans google
p = 5000
r = 0.3
t = 0.003
E = 3e6

print "radial displacement [m]: %e"%(p*r*r/t/E)
