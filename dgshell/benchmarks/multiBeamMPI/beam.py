#-*-coding:Utf-8-*-
from gmshpy import *
from dgshellpy import *
#script to launch beam problem with a python script

# material law
lawnum = 1 # unique number of the law
E = 10000.e6 # Young's modulus
nu = 0.   # Poisson's ratio
rho = 10000. # Bulk mass

# geometry
h = 0.1  # thickness
geofile="beam.geo" # name of mesh file
meshfile="beam.msh" # name of mesh file
# integration
nsimp = 1 # number of Simpson's points (odd)

# solver
sol =  2 #Gmm=0 (default) Taucs=1 PETsc=2
beta1 = 10. # value of stabilization parameter
beta2 = 10.
beta3 = 0.0001
soltype = 3 # StaticLinear=0 (default) StaticNonLinear=1 Explicit=2 Multi=3
nstepimpl = 50
nstepexpl = 50
ftime =0.01  # Final time (used only if soltype=1 or 2)
tol=1.e-4   # relative tolerance for NR scheme (used only if soltype=1)
nstepArch=50 # Number of step between 2 archiving (used only if soltype=1 or 2)
# explicit scheme
#beta=0.5 #0.5
#gamma=1. #1.
gamma_s =0.8 # 0.8666666667
#alpham=0.5 #0.5
rhoinfty =1. 

# creation of law
law1 = linearElasticShellLaw(lawnum,E,nu,h,nsimp,rho)
# creation of ElasticField
nfield =99 # number of the field (physical number of surface)
fullDg = 1 #  formulation CgDg=0 fullDg =1
space1 = 0 # function space Lagrange=0
myfield1 = dgLinearShellDomain(1000,nfield,space1,lawnum,fullDg)
myfield1.stabilityParameters(beta1,beta2,beta3)
# creation of Solver
mysolver = nonLinearMechSolver(1000)
mysolver.createModel(geofile,meshfile,2,3)
#mysolver.loadModel(meshfile)
mysolver.addDomain(myfield1)
mysolver.addMaterialLaw(law1)
mysolver.Scheme(soltype)
mysolver.Solver(sol)
mysolver.addSystem(2,2)
mysolver.addSystem(1,1)
mysolver.options("-pc_type ksp -ksp_gmres_restart 200")
mysolver.snlData(nstepimpl,ftime,tol)
mysolver.explicitSpectralRadius(ftime,gamma_s,rhoinfty)
mysolver.explicitTimeStepEvaluation(nstepexpl)
mysolver.stepBetweenArchiving(nstepArch)
# BC
mysolver.displacementBC("Edge",41,0,0.)
mysolver.displacementBC("Edge",41,1,0.)
mysolver.displacementBC("Edge",41,2,0.)
mysolver.thetaBC(41)
mysolver.initialBC("Face","Velocity",99,0,-1.)
mysolver.forceBC("Edge",21,2,1e8)
# Archive
mysolver.archivingNodeVelocity(22,0,3)
mysolver.archivingNodeDisplacement(22,0,3)
mysolver.archivingNodeDisplacement(22,2,nstepimpl)
mysolver.archivingNodeIP(22,0,0)
mysolver.archivingForceOnPhysicalGroup("Node",22,0,3)
#mysolver.unknownBuildView(1,nstepArch)
mysolver.solve()

# perform some check (pay attention that only rank 0 really perform the check)
try:
    import linecache
    linedisp = linecache.getline('NodalDisplacementPhysical22Num2comp2_part2.csv',24)
    linevelo = linecache.getline('NodalVelocityPhysical22Num2comp0_part2.csv',24)
except:
    print('Cannot read the results in the files')
    import os
    os._exit(1)
else:
    check = TestCheck()
    check.equal(3.967536e-01,float(linedisp.split(';')[1]))
    check.equal(1.152648e+00,float(linevelo.split(';')[1]))
