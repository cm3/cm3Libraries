# test file

set(PYFILE beam.py)

set(FILES2DELETE 
  *.csv
  disp*
)

add_cm3python_mpi_test(4 ${PYFILE} "${FILES2DELETE}")
