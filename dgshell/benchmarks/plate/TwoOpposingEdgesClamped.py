#coding-Utf-8-*-
from gmshpy import *
from dgshellpy import*

#script to launch beam problem with a python script

# material law
lawnum = 1 # unique number of law
E = 1.e6 # Young's modulus
nu = 0.3   # Poisson's ratio
rho = 7850. # Bulk mass

# geometry
h = 0.1  # thickness
geofile="plate.geo"
meshfile="plate.msh" # name of mesh file
# integration
nsimp = 1 # number of Simpson's points (odd)

# solver
sol = 2  # Gmm=0 (default) Taucs=1 PETsc=2
beta1 = 100. # value of stabilization parameter
beta2 = 100.
beta3 = 100.
soltype = 1 # StaticLinear=0 (default) StaticNonLinear=1
nstep = 2   # number of step (used only if soltype=1)
ftime =1.   # Final time (used only if soltype=1)
tol=1.e-6   # relative tolerance for NR scheme (used only if soltype=1)
nstepArch=1 # Number of step between 2 archiving (used only if soltype=1)

#  compute solution and BC (given directly to the solver
# creation of law
law1 = linearElasticShellLaw(lawnum,E,nu,h,nsimp,rho)

# creation of ElasticField
nfield =99 # number of the field (physical number of surface)
fullDg = 1 #  formulation CgDg=0 fullDg =1
space1 = 0 # function space (Lagrange=0)
myfield1 = dgLinearShellDomain(1000,nfield,space1,lawnum,fullDg)
myfield1.stabilityParameters(beta1,beta2,beta3)
# creation of Solver
mysolver = nonLinearMechSolver(1000)
mysolver.createModel(geofile,meshfile,2,2,True)
mysolver.addDomain(myfield1)
mysolver.addMaterialLaw(law1)
mysolver.Scheme(soltype)
mysolver.Solver(sol)
mysolver.snlData(nstep,ftime,tol)
mysolver.stepBetweenArchiving(nstepArch)
# BC
mysolver.displacementBC("Edge",11,0,0.)
mysolver.displacementBC("Edge",11,1,0.)
mysolver.displacementBC("Edge",21,0,0.)
mysolver.displacementBC("Edge",21,1,0.)
mysolver.displacementBC("Edge",21,2,0.)
mysolver.displacementBC("Edge",31,2,0.)
mysolver.displacementBC("Edge",41,0,0.)
mysolver.displacementBC("Edge",41,1,0.)
mysolver.forceBC("Node",111,2,-50.)
mysolver.thetaBC(11)
mysolver.thetaBC(21)
mysolver.thetaBC(41)
mysolver.archivingNodeDisplacement(111,2)
mysolver.solve()

# check the archived value into the file
check = TestCheck()
try:
    import linecache
    ldisp1 = linecache.getline('NodalDisplacementPhysical111Num1comp2.csv',1)
    ldisp2 = linecache.getline('NodalDisplacementPhysical111Num1comp2.csv',2)
except:
    print('Cannot get the values in the file')
    import os
    os._exit(1)
else:
    disp1 = float(ldisp1.split(';')[1])
    disp2 = float(ldisp2.split(';')[1])
    check.equal(disp2,2*disp1,1.e-5)
