--[[
    Script to launch beam problem with a lua script
  ]]
--[[
    data 
  ]]
-- material law
lawnum = 1 -- unique number of the law
E = 100.e9 -- Young's modulus
nu = 0.3   -- Poisson's ratio
rho =7850. 
-- geometry
h = 0.01  -- thickness
meshfile="beam50.msh" -- name of mesh file
-- integration
nsimp = 3 -- number of Simpson's points (odd)

-- solver
sol = 2 --Gmm=0 (default) Taucs=1 PETsc=2
beta1 = 10. -- value of stabilization parameter
beta2 = 10.
beta3 = 1.
soltype = 1 -- StaticLinear=0 (default) StaticNonLinear=1
nstep = 5   -- number of step (used only if soltype=1)
ftime =1.   -- Final time (used only if soltype=1)
tol=1.e-4   -- relative tolerance for NR scheme (used only if soltype=1)
nstepArch=1 -- Number of step between 2 archiving (used only if soltype=1)

--[[
    compute solution and BC (given directly to the solver
  ]]

-- creation of law
law1 = linearElasticLawPlaneStress(lawnum,E,nu,rho)

-- creation of ElasticField
nfield =99 -- number of the field (physical number of surface)
fullDg = 1 --  formulation CgDg=0 fullDg =1
space1 = 0 -- function space Lagrange=0
myfield1 = dgLinearShellDomain()
myfield1:tag(1000)
myfield1:thickness(h)
myfield1:lawnumber(lawnum)
myfield1:simpsonPoints(nsimp)
myfield1:formulation(fullDg)
myfield1:functionSpace(space1);

-- creation of Solver
mysolver = dgC0ShellSolver(1000)
mysolver:formulation(fullDg)
mysolver:readmsh(meshfile)
mysolver:addDgLinearElasticShellDomain(myfield1,nfield,2)
mysolver:AddLinearElasticLawPlaneStress(law1)
mysolver:setScheme(soltype)
mysolver:whichSolver(sol)
mysolver:SNLData(nstep,ftime,tol)
mysolver:stepBetweenArchiving(nstepArch)
mysolver:stabilityParameters(beta1,beta2,beta3)
-- BC
mysolver:prescribedDisplacement("Edge",41,0,0.)
mysolver:prescribedDisplacement("Edge",41,1,0.)
mysolver:prescribedDisplacement("Edge",41,2,0.)
--mysolver:prescribedDisplacement("Edge",21,0,0.)
--mysolver:prescribedDisplacement("Edge",21,1,0.)
--mysolver:prescribedDisplacement("Edge",21,2,0.)
--mysolver:prescribedDisplacement("Edge",21,2,0.4)
--mysolver:prescribedForce("Face",99,0.,0.,1000.)
mysolver:prescribedForce("Edge",21,0.,0.,-1.e6)
--mysolver:pressureOnPhysicalGroup(99,1000.)
mysolver:AddThetaConstraint(41)
--mysolver:AddThetaConstraint(21)
mysolver:ArchivingForceOnPhysicalGroup("Edge",41,2)
mysolver:ArchivingNodalDisplacement(22,2)
mysolver:solve()
