//This file is modified by Shantanu on 8-11-2012
//all units are in microm. length = 0.2 microm, height = 0.1 microm

x = 0.1;
y = 0.2;
l = Sqrt(x^2 + y^2);

height_reqd = 0.1;

x1=-x;
y1=y;
x0 = 0.0;
y0 = y;
x2 = x;
y2 = y;
x3 = x;
y3 =  y + height_reqd;
x5 = -x;
y5 = y + height_reqd;
x111 = 0;
y111 = 0.5;
x4 = 0;
y4 = y111 - l;

//-------lower half------------------------

dely = y5 - y4;
height = y4 - y0;
total_height = l + height;

x6 = 0.66*x1;
y6 = y1;
x7 = 0.33*x1;
y7 = y1;
x8 = 0.33*x2;
y8 = y1;
x9 = 0.84*x2;
y9 = y1;

//------upper half---------------------------------

x11 = 0.66*x3;
y11 = y111 - Sqrt(l^2 - (x11 - x111)^2);
x19 = 0.2*x3;
y19 = y111 - Sqrt(l^2 - (x19 - x111)^2);
x12 = 0.2*x5;
y12 = y111 - Sqrt(l^2 - (x12 - x111)^2);

//------In-between points------------------------------------

x10 = x;
y10 = y0 + 0.5*height;
x20 = -x;
y20 = y0 + 0.66*height;
x14 = 0.48*x1;
y14 = y0 + 0.2*height;
x13 = 0.57*x1;
y13 = y0 + 0.48*height;
x16 = 0.21*x2;
y16 = y0 + 0.3*height;
x17 = 0.75*x2;
y17 = y0 + 0.1*height;
x18 = 0.75*x2;
y18 = y0 + 0.66*height;
x15 = 0.1*x1;
y15 = y0 + 0.54*height;
x21 = 0.3*x2;
y21 = y0 + 0.50*height;

// Characteristic length
Lc1=0.005;
Lc2= 0.003;

// definition of points
Point(1) = { x1 , y1 , 0.0 , Lc1};
Point(2) = { x2 , y2 , 0.0 , Lc1};
Point(3) = { x3 , y3 , 0.0 , Lc1};
Point(4) = { x4 , y4 , 0.0 , Lc2};
Point(5) = { x5 , y5 , 0.0 , Lc1};
Point(6) = { x6 , y6 , 0.0 , Lc2};
Point(7) = { x7 , y7 , 0.0 , Lc2};
Point(8) = { x8 , y8 , 0.0 , Lc2};
Point(9) = { x9 , y9 , 0.0 , Lc1};
Point(10) = { x10 , y10 , 0.0 , Lc1};
Point(11) = { x11 , y11 , 0.0 , Lc1};
Point(12) = { x12 , y12 , 0.0 , Lc2};
Point(13) = { x13 , y13 , 0.0 , Lc2};
Point(14) = { x14 , y14 , 0.0 , Lc2};
Point(15) = { x15 , y15 , 0.0 , Lc2};
Point(16) = { x16 , y16 , 0.0 , Lc2};
Point(17) = { x17 , y17 , 0.0 , Lc1};
Point(18) = { x18 , y18 , 0.0 , Lc1};
Point(19) = { x19 , y19 , 0.0 , Lc2};
Point(20) = { x20 , y20 , 0.0 , Lc1};
Point(21) = { x21 , y21 , 0.0 , Lc2};

Point(111)={ x0 , y0 , 0.0 , Lc1};
Point(112)={x111 , y111 , 0.0 , Lc1};

// Line between points
Line(1) = {1, 6};
Line(2) = {6, 7};
Line(3) = {7, 8};
Line(4) = {8, 9};
Line(5) = {9, 2};
Line(6) = {2, 10};
Line(7) = {10, 3};
Circle(8) = {3, 112, 11};
Circle(9) = {11, 112, 19};
Circle(10) = {19, 112, 12};
Circle(11) = {12, 112, 5};
Line(12) = {5, 20};
Line(13) = {20, 1};
Line(14) = {6, 14};
Line(15) = {7, 14};
Line(16) = {8, 16};
Line(17) = {13, 20};
Line(18) = {13, 12};
Line(19) = {15, 13};
Line(20) = {19, 15};
Line(21) = {16, 14};
Line(22) = {16, 15};
Line(23) = {21, 19};
Line(24) = {9, 17};
Line(25) = {17, 16};
Line(26) = {18, 21};
Line(27) = {18, 17};
Line(28) = {10, 18};
Line(29) = {11, 18};
Line(30) = {14, 13};
Line(31) = {16, 21};

//generate line loops
Line Loop(41) = {18, 11, 12, -17}; 
Line Loop(42) = {1, 14, 30, 17, 13};
Line Loop(43) = {2, 15, -14};
Line Loop(44) = {-21, 22, 19, -30};
Line Loop(45) = {-19, -20, 10, -18};
Line Loop(46) = {-22, 31, 23, 20};
Line Loop(47) = {3, 16, 21, -15};
Line Loop(48) = {4, 24, 25, -16};
Line Loop(49) = {-25, -27, 26, -31};
Line Loop(50) = {-26, -29, 9, -23};
Line Loop(51) = {5, 6, 28, 27, -24};
Line Loop(52) = {7, 8, 29, -28};

// Surface definition
Plane Surface(61) = {41};
Plane Surface(62) = {42};
Plane Surface(63) = {43};
Plane Surface(64) = {44};
Plane Surface(65) = {45};
Plane Surface(66) = {46};
Plane Surface(67) = {47};
Plane Surface(68) = {48};
Plane Surface(69) = {49};
Plane Surface(70) = {50};
Plane Surface(71) = {51};
Plane Surface(72) = {52};

//Physical surface definition
Physical Surface(81) = {61};
Physical Surface(82) = {62};
Physical Surface(83) = {63};
Physical Surface(84) = {64};
Physical Surface(85) = {65};
Physical Surface(86) = {66};
Physical Surface(87) = {67};
Physical Surface(88) = {68};
Physical Surface(89) = {69};
Physical Surface(90) = {70};
Physical Surface(91) = {71};
Physical Surface(92) = {72};

//physical line definition
//Physical Line(1001) = {1};
//Physical Line(1002) = {2};
//Physical Line(1003) = {3};
//Physical Line(1004) = {4};
//Physical Line(1005) = {5};
//Physical Line(1006) = {6};
//Physical Line(1007) = {7};
//Physical Line(1008) = {8};
//Physical Line(1009) = {9};
//Physical Line(1010) = {10};
//Physical Line(1011) = {11};
//Physical Line(1012) = {12};
//Physical Line(1013) = {13};
//Physical Line(1014) = {14};
//Physical Line(1015) = {15};
//Physical Line(1016) = {16};
//Physical Line(1017) = {17};
//Physical Line(1018) = {18};
//Physical Line(1019) = {19};
//Physical Line(1020) = {20};
//Physical Line(1021) = {21};
//Physical Line(1022) = {22};
//Physical Line(1023) = {23};
//Physical Line(1024) = {24};
//Physical Line(1025) = {25};
//Physical Line(1026) = {26};
//Physical Line(1027) = {27};
//Physical Line(1028) = {28};
//Physical Line(1029) = {29};
//Physical Line(1030) = {30};
//Physical Line(1031) = {31};

Physical Line(1032) = {12, 13};
Physical Line(1033) = {1, 2, 3, 4, 5};
Physical Line(1034) = {6, 7};
Physical Line(1035) = {8, 9, 10, 11};
Physical Line(1036) = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31};




