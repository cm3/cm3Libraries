#-*-coding:Utf-8-*-
from gmshpy import *
#from dgshellpyDebug import *
from dgshellpy import *
#script to launch beam problem with a lua script
# all values are in the units of length = microm, time = sec, mass = KiloTonne, Force = N, stress = TeraPa, energy = 10^(6) N-microm

# Density
rho =2.33e-21 # KiloTonne/(microm^3), original unit is gm/cm^3

# material law
lawnum1 = 1 # unique number of the law
lawnum2 =2
lawnum3 =3
lawnum4 =4
lawnum5 =5
lawnum6 =6
lawnum7 =7
lawnum8 =8
lawnum9 =9
lawnum10 =10
lawnum11 =11
lawnum12 =12

lawnum21 = 21 # unique number of the law
lawnum22 =22
lawnum23 =23
lawnum24 =24
lawnum25 =25
lawnum26 =26
lawnum27 =27
lawnum28 =28
lawnum29 =29
lawnum30 =30
lawnum31 =31
lawnum32 =32

lawnum41 =41
lawnum42 =42
lawnum43 =43
lawnum44 =44
lawnum45 =45
lawnum46 =46
lawnum47 =47
lawnum48 =48
lawnum49 =49
lawnum50 =50
lawnum51 =51
lawnum52 =52

lawnumInt1 = 61

#Surface energy along different planes of symmetry
Gc100 = 2.54e-6*2 # N/microm, original unit is N/m
Gc110 = 2.10e-6*2 # N/microm, original unit is N/m
Gc111 = 1.28e-6*2 # N/microm, original unit is N/m

#Fracture strengths along different planes of symmetry
sigmac100=1.53e-3 #TeraPa, original unit is GPa
sigmac110=1.21e-3 #TeraPa, original unit is GPa
sigmac111=0.868e-3 #TeraPa, original unit is GPa

beta = 0.87

# geometry
h = 0.05  # thickness in microm
meshfile="beam-9dom.msh" # name of mesh file
# integration
nsimp1 = 3 # number of Simpson's points (odd)
nsimp2 = 3 

# solver
sol = 2  #Gmm=0 (default) Taucs=1 PETsc=2
beta1 = 10. # value of stabilization parameter
beta2 = 10.
beta3 = beta1*(h/0.4)**2
soltype = 2 # StaticLinear=0 (default) StaticNonLinear=1 slotype=2 dynamic
nstep = 10 # number of step (used only if soltype=1)
ftime =5.25e-6   # Final time (used only if soltype=1)    this time is less better 
tol=1.e-5   # relative tolerance for NR scheme (used only if soltype=1)
nstepArch=700 # Number of step between 2 archiving 

# compute solution and BC (given directly to the solver)

h0 = 0.05  # thickness in microm
nsimp = 3 #  number of Simpson's points (odd)
oEx=144e-3 # Young's modulus of x direction  #TeraPa, original unit is GPa
oEy=144e-3 # Young's modulus of y direction #TeraPa, original unit is GPa
oEz=144e-3 # Young's modulus of z direction #TeraPa, original unit is GPa

onuxy=0.27826 #  Poisson's ratio of xy 
onuxz=0.27826 #  Poisson's ratio of xz
onuyz=0.27826 #  Poisson's ratio of yz

omuxy =80e-3 # shear modulus xy?? #TeraPa, original unit is GPa            
omuxz =80e-3 # shear modulus xz?? #TeraPa, original unit is GPa 
omuyz =80e-3 # shear modulus yz?? #TeraPa, original unit is GPa

theta=0.0# rotation around x
phi=1.57080# rotation with y
psi=0.0# rotation with z

theta1=0.0# rotation around x
phi1=1.57080# rotation with y
psi1=0.0# rotation with z

mu=omuxy 
theta2=0.0# rotation around x
phi2=1.57080# rotation with y
psi2=0.0# rotation with z

theta3=0.0# rotation around x
phi3=1.57080# rotation with y
psi3=0.0# rotation with z

theta4=0.0# rotation around x
phi4=1.57080# rotation with y
psi4=0.0# rotation with z

theta5=0.0# rotation around x
phi5=1.57080# rotation with y
psi5=0.0# rotation with z

theta6=0.0# rotation around x
phi6=1.57080# rotation with y
psi6=0.0# rotation with z

theta7=0.0# rotation around x
phi7=1.57080# rotation with y
psi7=0.0# rotation with z

theta8=0.0# rotation around x
phi8=1.57080# rotation with y
psi8=0.0# rotation with z

theta9=0.0# rotation around x
phi9=1.57080# rotation with y
psi9=0.0# rotation with z

theta10=0.0# rotation around x
phi10=1.57080# rotation with y
psi10=0.0# rotation with z

theta11=0.0# rotation around x
phi11=1.57080# rotation with y
psi11=0.0# rotation with z

theta12=0.0# rotation around x
phi12=1.57080# rotation with y
psi12=0.0# rotation with z

#linearElasticOrthotropicShellLaw:- constitutive relation material law for the deformation of bulk (shell) material
law1=linearElasticOrthotropicShellLaw(lawnum1,rho,h0,nsimp,oEx,oEy,oEz,onuxy,onuxz,onuyz,omuxy,omuxz,omuyz,theta1,phi1,psi1)
law2=linearElasticOrthotropicShellLaw(lawnum2,rho,h0,nsimp,oEx,oEy,oEz,onuxy,onuxz,onuyz,omuxy,omuxz,omuyz,theta2,phi2,psi2)
law3=linearElasticOrthotropicShellLaw(lawnum3,rho,h0,nsimp,oEx,oEy,oEz,onuxy,onuxz,onuyz,omuxy,omuxz,omuyz,theta3,phi3,psi3)
law4=linearElasticOrthotropicShellLaw(lawnum4,rho,h0,nsimp,oEx,oEy,oEz,onuxy,onuxz,onuyz,omuxy,omuxz,omuyz,theta4,phi4,psi4)
law5=linearElasticOrthotropicShellLaw(lawnum5,rho,h0,nsimp,oEx,oEy,oEz,onuxy,onuxz,onuyz,omuxy,omuxz,omuyz,theta5,phi5,psi5)
law6=linearElasticOrthotropicShellLaw(lawnum6,rho,h0,nsimp,oEx,oEy,oEz,onuxy,onuxz,onuyz,omuxy,omuxz,omuyz,theta6,phi6,psi6)
law7=linearElasticOrthotropicShellLaw(lawnum7,rho,h0,nsimp,oEx,oEy,oEz,onuxy,onuxz,onuyz,omuxy,omuxz,omuyz,theta7,phi7,psi7)
law8=linearElasticOrthotropicShellLaw(lawnum8,rho,h0,nsimp,oEx,oEy,oEz,onuxy,onuxz,onuyz,omuxy,omuxz,omuyz,theta8,phi8,psi8)
law9=linearElasticOrthotropicShellLaw(lawnum9,rho,h0,nsimp,oEx,oEy,oEz,onuxy,onuxz,onuyz,omuxy,omuxz,omuyz,theta9,phi9,psi9)
law10=linearElasticOrthotropicShellLaw(lawnum10,rho,h0,nsimp,oEx,oEy,oEz,onuxy,onuxz,onuyz,omuxy,omuxz,omuyz,theta10,phi10,psi10)
law11=linearElasticOrthotropicShellLaw(lawnum11,rho,h0,nsimp,oEx,oEy,oEz,onuxy,onuxz,onuyz,omuxy,omuxz,omuyz,theta11,phi11,psi11)
law12=linearElasticOrthotropicShellLaw(lawnum12,rho,h0,nsimp,oEx,oEy,oEz,onuxy,onuxz,onuyz,omuxy,omuxz,omuyz,theta12,phi12,psi12)

#shellLinearCohesiveLawWithOrientation:- cohesive law for interface elements in-between two bulk elements
law21 = shellLinearCohesiveLawWithOrientation(lawnum21,Gc100, Gc110, Gc111,sigmac100,sigmac110,sigmac111,beta,mu,theta1,phi1,psi1,0.95,1.05)
law22 = shellLinearCohesiveLawWithOrientation(lawnum22,Gc100, Gc110, Gc111,sigmac100,sigmac110,sigmac111,beta,mu,theta2,phi2,psi2,0.95,1.05)
law23 = shellLinearCohesiveLawWithOrientation(lawnum23,Gc100, Gc110, Gc111,sigmac100,sigmac110,sigmac111,beta,mu,theta3,phi3,psi3,0.95,1.05)
law24 = shellLinearCohesiveLawWithOrientation(lawnum24,Gc100, Gc110, Gc111,sigmac100,sigmac110,sigmac111,beta,mu,theta4,phi4,psi4,0.95,1.05)
law25 = shellLinearCohesiveLawWithOrientation(lawnum25,Gc100, Gc110, Gc111,sigmac100,sigmac110,sigmac111,beta,mu,theta5,phi5,psi5,0.95,1.05)
law26 = shellLinearCohesiveLawWithOrientation(lawnum26,Gc100, Gc110, Gc111,sigmac100,sigmac110,sigmac111,beta,mu,theta6,phi6,psi6,0.95,1.05)
law27 = shellLinearCohesiveLawWithOrientation(lawnum27,Gc100, Gc110, Gc111,sigmac100,sigmac110,sigmac111,beta,mu,theta7,phi7,psi7,0.95,1.05)
law28 = shellLinearCohesiveLawWithOrientation(lawnum28,Gc100, Gc110, Gc111,sigmac100,sigmac110,sigmac111,beta,mu,theta8,phi8,psi8,0.95,1.05)
law29 = shellLinearCohesiveLawWithOrientation(lawnum29,Gc100, Gc110, Gc111,sigmac100,sigmac110,sigmac111,beta,mu,theta9,phi9,psi9,0.95,1.05)
law30 = shellLinearCohesiveLawWithOrientation(lawnum30,Gc100, Gc110, Gc111,sigmac100,sigmac110,sigmac111,beta,mu,theta10,phi10,psi10,0.95,1.05)
law31 = shellLinearCohesiveLawWithOrientation(lawnum31,Gc100, Gc110, Gc111,sigmac100,sigmac110,sigmac111,beta,mu,theta11,phi11,psi11,0.95,1.05)
law32 = shellLinearCohesiveLawWithOrientation(lawnum32,Gc100, Gc110, Gc111,sigmac100,sigmac110,sigmac111,beta,mu,theta12,phi12,psi12,0.95,1.05)

#shellFractureByCohesiveLaw:- Class with definition of materialLaw of fracture for (shell) bulk material
law41 = shellFractureByCohesiveLaw(lawnum41,lawnum1,lawnum21)
law42 = shellFractureByCohesiveLaw(lawnum42,lawnum2,lawnum22)
law43 = shellFractureByCohesiveLaw(lawnum43,lawnum3,lawnum23)
law44 = shellFractureByCohesiveLaw(lawnum44,lawnum4,lawnum24)
law45 = shellFractureByCohesiveLaw(lawnum45,lawnum5,lawnum25)
law46 = shellFractureByCohesiveLaw(lawnum46,lawnum6,lawnum26)
law47 = shellFractureByCohesiveLaw(lawnum47,lawnum7,lawnum27)
law48 = shellFractureByCohesiveLaw(lawnum48,lawnum8,lawnum28)
law49 = shellFractureByCohesiveLaw(lawnum49,lawnum9,lawnum29)
law50 = shellFractureByCohesiveLaw(lawnum50,lawnum10,lawnum30)
law51 = shellFractureByCohesiveLaw(lawnum51,lawnum11,lawnum31)
law52 = shellFractureByCohesiveLaw(lawnum52,lawnum12,lawnum32)

lawInt1 = shellLinearCohesiveLawWithOrientation(lawnumInt1,Gc100, Gc110, Gc111,sigmac100,sigmac110,sigmac111,beta,mu,theta,phi,psi,0.95,1.05)

# creation of ElasticField
nfield1 =81 # number of the field (physical number of surface)
fullDg = 1 #  formulation CgDg=0 fullDg =1
space1 = 0 # function space Lagrange=0, Inter = 1
#dgLinearShellDomain:- class for physical surfaces in the geometry of model
myfield1 = dgLinearShellDomain(1000,nfield1,space1,lawnum41,fullDg)
myfield1.stabilityParameters(beta1,beta2,beta3)

nfield2 =82 # number of the field (physical number of surface)
fullDg2 = 1 #  formulation CgDg=0 fullDg =1
space2 = 0 # function space Lagrange=0
myfield2 = dgLinearShellDomain(1000,nfield2,space2,lawnum42,fullDg2)
myfield2.stabilityParameters(beta1,beta2,beta3)

nfield3 =83 # number of the field (physical number of surface)
fullDg3 = 1 #  formulation CgDg=0 fullDg =1
space2 = 0 # function space Lagrange=0
myfield3 = dgLinearShellDomain(1000,nfield3,space2,lawnum43,fullDg3)
myfield3.stabilityParameters(beta1,beta2,beta3)


nfield4 =84 # number of the field (physical number of surface)
fullDg4 = 1 #  formulation CgDg=0 fullDg =1
space2 = 0 # function space Lagrange=0
myfield4 = dgLinearShellDomain(1000,nfield4,space2,lawnum44,fullDg4)
myfield4.stabilityParameters(beta1,beta2,beta3)

nfield5 =85 # number of the field (physical number of surface)
fullDg5 = 1 #  formulation CgDg=0 fullDg =1
space2 = 0 # function space Lagrange=0
myfield5 = dgLinearShellDomain(1000,nfield5,space2,lawnum45,fullDg5)
myfield5.stabilityParameters(beta1,beta2,beta3)

nfield6 =86 # number of the field (physical number of surface)
fullDg6 = 1 #  formulation CgDg=0 fullDg =1
space2 = 0 # function space Lagrange=0
myfield6 = dgLinearShellDomain(1000,nfield6,space2,lawnum46,fullDg6)
myfield6.stabilityParameters(beta1,beta2,beta3)

nfield7 =87 # number of the field (physical number of surface)
fullDg7 = 1 #  formulation CgDg=0 fullDg =1
space2 = 0 # function space Lagrange=0
myfield7 = dgLinearShellDomain(1000,nfield7,space2,lawnum47,fullDg7)
myfield7.stabilityParameters(beta1,beta2,beta3)

nfield8 =88 # number of the field (physical number of surface)
fullDg8 = 1 #  formulation CgDg=0 fullDg =1
space2 = 0 # function space Lagrange=0
myfield8 = dgLinearShellDomain(1000,nfield8,space2,lawnum48,fullDg8)
myfield8.stabilityParameters(beta1,beta2,beta3)

nfield9 =89 # number of the field (physical number of surface)
fullDg9 = 1 #  formulation CgDg=0 fullDg =1
space2 = 0 # function space Lagrange=0
myfield9 = dgLinearShellDomain(1000,nfield9,space2,lawnum49,fullDg9)
myfield9.stabilityParameters(beta1,beta2,beta3)

nfield10 =90 # number of the field (physical number of surface)
fullDg10 = 1 #  formulation CgDg=0 fullDg =1
space2 = 0 # function space Lagrange=0
myfield10 = dgLinearShellDomain(1000,nfield10,space2,lawnum50,fullDg10)
myfield10.stabilityParameters(beta1,beta2,beta3)

nfield11 =91 # number of the field (physical number of surface)
fullDg11 = 1 #  formulation CgDg=0 fullDg =1
space2 = 0 # function space Lagrange=0
myfield11 = dgLinearShellDomain(1000,nfield11,space2,lawnum51,fullDg11)
myfield11.stabilityParameters(beta1,beta2,beta3)

nfield12 =92 # number of the field (physical number of surface)
fullDg12 = 1 #  formulation CgDg=0 fullDg =1
space2 = 0 # function space Lagrange=0
myfield12 = dgLinearShellDomain(1000,nfield12,space2,lawnum52,fullDg12)
myfield12.stabilityParameters(beta1,beta2,beta3)


#interDomainBetweenShell:- class for interfaces between different crystal lattices of polycrystalline silicon
#interface 1
myinterfield1 = interDomainBetweenShell(1000,myfield1,myfield2,lawnumInt1)
myinterfield1.stabilityParameters(beta1,beta2,beta3)
myinterfield1.matrixByPerturbation(1,1e-8)

#interface 2
myinterfield2 = interDomainBetweenShell(1000,myfield1,myfield5,lawnumInt1)
myinterfield2.stabilityParameters(beta1,beta2,beta3)
myinterfield2.matrixByPerturbation(1,1e-8)

#interface 3
myinterfield3 = interDomainBetweenShell(1000,myfield2,myfield3,lawnumInt1)
myinterfield3.stabilityParameters(beta1,beta2,beta3)
myinterfield3.matrixByPerturbation(1,1e-8)

#interface 4
myinterfield4 = interDomainBetweenShell(1000,myfield2,myfield4,lawnumInt1)
myinterfield4.stabilityParameters(beta1,beta2,beta3)
myinterfield4.matrixByPerturbation(1,1e-8)

#interface 5
myinterfield5 = interDomainBetweenShell(1000,myfield3,myfield7,lawnumInt1)
myinterfield5.stabilityParameters(beta1,beta2,beta3)
myinterfield5.matrixByPerturbation(1,1e-8)

#interface 6
myinterfield6 = interDomainBetweenShell(1000,myfield4,myfield5,lawnumInt1)
myinterfield6.stabilityParameters(beta1,beta2,beta3)
myinterfield6.matrixByPerturbation(1,1e-8)

#interface 7
myinterfield7 = interDomainBetweenShell(1000,myfield4,myfield6,lawnumInt1)
myinterfield7.stabilityParameters(beta1,beta2,beta3)
myinterfield7.matrixByPerturbation(1,1e-8)

#interface 8
myinterfield8 = interDomainBetweenShell(1000,myfield4,myfield7,lawnumInt1)
myinterfield8.stabilityParameters(beta1,beta2,beta3)
myinterfield8.matrixByPerturbation(1,1e-8)

#interface 9
myinterfield9 = interDomainBetweenShell(1000,myfield5,myfield6,lawnumInt1)
myinterfield9.stabilityParameters(beta1,beta2,beta3)
myinterfield9.matrixByPerturbation(1,1e-8)

#interface 10
myinterfield10 = interDomainBetweenShell(1000,myfield6,myfield9,lawnumInt1)
myinterfield10.stabilityParameters(beta1,beta2,beta3)
myinterfield10.matrixByPerturbation(1,1e-8)

#interface 11
myinterfield11 = interDomainBetweenShell(1000,myfield6,myfield10,lawnumInt1)
myinterfield11.stabilityParameters(beta1,beta2,beta3)
myinterfield11.matrixByPerturbation(1,1e-8)

#interface 12
myinterfield12 = interDomainBetweenShell(1000,myfield7,myfield8,lawnumInt1)
myinterfield12.stabilityParameters(beta1,beta2,beta3)
myinterfield12.matrixByPerturbation(1,1e-8)

#interface 13
myinterfield13 = interDomainBetweenShell(1000,myfield8,myfield9,lawnumInt1)
myinterfield13.stabilityParameters(beta1,beta2,beta3)
myinterfield13.matrixByPerturbation(1,1e-8)

#interface 14
myinterfield14 = interDomainBetweenShell(1000,myfield8,myfield11,lawnumInt1)
myinterfield14.stabilityParameters(beta1,beta2,beta3)
myinterfield14.matrixByPerturbation(1,1e-8)

#interface 15
myinterfield15 = interDomainBetweenShell(1000,myfield9,myfield10,lawnumInt1)
myinterfield15.stabilityParameters(beta1,beta2,beta3)
myinterfield15.matrixByPerturbation(1,1e-8)

#interface 16
myinterfield16 = interDomainBetweenShell(1000,myfield9,myfield11,lawnumInt1)
myinterfield16.stabilityParameters(beta1,beta2,beta3)
myinterfield16.matrixByPerturbation(1,1e-8)

#interface 17
myinterfield17 = interDomainBetweenShell(1000,myfield10,myfield12,lawnumInt1)
myinterfield17.stabilityParameters(beta1,beta2,beta3)
myinterfield17.matrixByPerturbation(1,1e-8)

#interface 18
myinterfield18 = interDomainBetweenShell(1000,myfield11,myfield12,lawnumInt1)
myinterfield18.stabilityParameters(beta1,beta2,beta3)
myinterfield18.matrixByPerturbation(1,1e-8)

# creation of Solver
mysolver = nonLinearMechSolver(1000)
mysolver.loadModel(meshfile)
mysolver.addDomain(myfield1)
mysolver.addDomain(myfield2)
mysolver.addDomain(myfield3)
mysolver.addDomain(myfield4)
mysolver.addDomain(myfield5)
mysolver.addDomain(myfield6)
mysolver.addDomain(myfield7)
mysolver.addDomain(myfield8)
mysolver.addDomain(myfield9)
mysolver.addDomain(myfield10)
mysolver.addDomain(myfield11)
mysolver.addDomain(myfield12)
mysolver.addDomain(myinterfield1)
mysolver.addDomain(myinterfield2)
mysolver.addDomain(myinterfield3)
mysolver.addDomain(myinterfield4)
mysolver.addDomain(myinterfield5)
mysolver.addDomain(myinterfield6)
mysolver.addDomain(myinterfield7)
mysolver.addDomain(myinterfield8) 
mysolver.addDomain(myinterfield9) 
mysolver.addDomain(myinterfield10) 
mysolver.addDomain(myinterfield11) 
mysolver.addDomain(myinterfield12) 
mysolver.addDomain(myinterfield13) 
mysolver.addDomain(myinterfield14) 
mysolver.addDomain(myinterfield15) 
mysolver.addDomain(myinterfield16) 
mysolver.addDomain(myinterfield17) 
mysolver.addDomain(myinterfield18) 
mysolver.addMaterialLaw(law1)
mysolver.addMaterialLaw(law2)
mysolver.addMaterialLaw(law3)
mysolver.addMaterialLaw(law4)
mysolver.addMaterialLaw(law5)
mysolver.addMaterialLaw(law6)
mysolver.addMaterialLaw(law7)
mysolver.addMaterialLaw(law8)
mysolver.addMaterialLaw(law9)
mysolver.addMaterialLaw(law10)
mysolver.addMaterialLaw(law11)
mysolver.addMaterialLaw(law12)

mysolver.addMaterialLaw(law21)
mysolver.addMaterialLaw(law22)
mysolver.addMaterialLaw(law23)
mysolver.addMaterialLaw(law24)
mysolver.addMaterialLaw(law25)
mysolver.addMaterialLaw(law26)
mysolver.addMaterialLaw(law27)
mysolver.addMaterialLaw(law28)
mysolver.addMaterialLaw(law29)
mysolver.addMaterialLaw(law30)
mysolver.addMaterialLaw(law31)
mysolver.addMaterialLaw(law32)

mysolver.addMaterialLaw(law41)
mysolver.addMaterialLaw(law42)
mysolver.addMaterialLaw(law43)
mysolver.addMaterialLaw(law44)
mysolver.addMaterialLaw(law45)
mysolver.addMaterialLaw(law46)
mysolver.addMaterialLaw(law47)
mysolver.addMaterialLaw(law48)
mysolver.addMaterialLaw(law49)
mysolver.addMaterialLaw(law50)
mysolver.addMaterialLaw(law51)
mysolver.addMaterialLaw(law52)

mysolver.addMaterialLaw(lawInt1)

mysolver.Scheme(soltype)
mysolver.Solver(sol)
#mysolver.snlData(nstep,ftime,tol)
mysolver.stepBetweenArchiving(nstepArch)
# interdomain

mysolver.explicitSpectralRadius(ftime,0.5,0.)
mysolver.explicitTimeStepEvaluation(nstep)
mysolver.stepBetweenArchiving(nstepArch)

# BC
mysolver.displacementBC("Edge",1032,0,0.)
mysolver.displacementBC("Edge",1033,1,0.)
mysolver.displacementBC("Edge",1036,2,0.)

dfinal = 600.
cyclicFunction1=cycleFunctionTime(ftime/10., 0.8*dfinal/ftime, ftime, dfinal/ftime,2*ftime, dfinal);  

mysolver.displacementBC("Edge",1034,0,600000.) 

mysolver.archivingForceOnPhysicalGroup("Edge",1032,0,nstepArch/10)
mysolver.archivingForceOnPhysicalGroup("Edge",1034,0,nstepArch/10)

mysolver.internalPointBuildView("svm",IPField.SVM, 1, 1)
mysolver.internalPointBuildView("sig_xx",IPField.SIG_XX, 1, 1)
mysolver.internalPointBuildView("sig_yy",IPField.SIG_YY, 1, 1)
mysolver.internalPointBuildView("sig_zz",IPField.SIG_ZZ, 1, 1)
mysolver.internalPointBuildView("sig_xy",IPField.SIG_XY, 1, 1)
mysolver.internalPointBuildView("sig_yz",IPField.SIG_YZ, 1, 1)
mysolver.internalPointBuildView("sig_xz",IPField.SIG_XZ, 1, 1)
mysolver.solve()
