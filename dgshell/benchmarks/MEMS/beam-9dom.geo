// Test case a SCB with a vertical load at its free extremity
// Size
//x1=(-0.2, 0.24); x10=(-0.2,0.33) x5=(0.2,0.24)
// lenthe=x5-x1=0.4     width=0.07 mm


x=0.1;
y=0.24;
l=Sqrt(x*x+y*y);

withy=0.01;

dely=2*l+5*withy;

// draw the low circle point 2,12,3,13,14,4 
x2=-x;
y2=y;

x3=0;
y3=l;

x4=x;
y4=y;

x12=-0.5*x;
y12=Sqrt(l*l-x12*x12);


x13=0.2*x;
y13=Sqrt(l*l-x13*x13);

x14=0.6*x;
y14=Sqrt(l*l-x14*x14);

//draw the up circle point 9,21,8,22,23,7
// the basic idea is draw a circle which has two parts (up, low), then move the low circle with dely distance.
x9=-x;
y9=-y+dely;

x21=-0.4*x;
y21=-Sqrt(l*l-x21*x21)+dely;

x8=0;
y8=-l+dely;

x22=0.15*x;
y22=-Sqrt(l*l-x22*x22)+dely;

x23=0.5*x;
y23=-Sqrt(l*l-x23*x23)+dely;

x7=x;
y7=-y+dely;

// draw the other points
x1=x2-x;
y1=y;

x11=x2-0.1*x;
y11=y;

x15=x4+0.2*x;
y15=y;

x5=x4+x;
y5=y;

x6=x5;
y6=y7;

x24=x7+0.15*x;
y24=y7;

x20=x9-0.15*x;
y20=y9;

x10=x1;
y10=y9;

x19=x2-0.3*x;
y19=y2+6*withy;

x18=x2+0.6*x;
y18=y19-1.5*withy;

x17=x3+0.1*x;
y17=y18-0.5*withy;

x16=x3+0.7*x;
y16=y17-0.5*withy;


// Characteristic length
Lc1=0.02;
Lc2=0.01;

// definition of points
Point(1) = { x1 , y1 , 0.0 , 2*Lc1};
Point(2) = { x2 , y2 , 0.0 , Lc1};
Point(3) = { x3 , y3 , 0.0 , Lc2};
Point(4) = { x4 , y4 , 0.0 , Lc1};
Point(5) = { x5 , y5 , 0.0 , 2*Lc1};
Point(6) = { x6 , y6 , 0.0 , 2*Lc1};
Point(7) = { x7 , y7 , 0.0 , Lc1};
Point(8) = { x8 , y8 , 0.0 , Lc2};
Point(9) = { x9 , y9 , 0.0 , Lc1};
Point(10) = { x10 , y10 , 0.0 , 2*Lc1};

Point(11) = { x11 , y11 , 0.0 , Lc1};
Point(12) = { x12 , y12 , 0.0 , Lc2};
Point(13) = { x13 , y13 , 0.0 , Lc2};
Point(14) = { x14 , y14 , 0.0 , Lc2};
Point(15) = { x15 , y15 , 0.0 , Lc1};
Point(16) = { x16 , y16 , 0.0 , Lc2};
Point(17) = { x17 , y17 , 0.0 , Lc2};
Point(18) = { x18 , y18 , 0.0 , Lc2};
Point(19) = { x19 , y19 , 0.0 , Lc1};
Point(20) = { x20 , y20 , 0.0 , Lc1};

Point(21) = { x21 , y21 , 0.0 , Lc2};
Point(22) = { x22 , y22 , 0.0 , Lc2};
Point(23) = { x23 , y23 , 0.0 , Lc2};
Point(24) = { x24 , y24 , 0.0 , Lc1};

Point(111)={ 0 , 0 , 0.0 , Lc1};
Point(112)={ 0 , y8+l , 0.0 , Lc1};

// Line between points


Line(1) = {1,11};
Line(2) = {2,11};
Circle(3) = {2,111,12};
Circle(4) = {12,111,13};
Circle(5) = {13,111,14};
Circle(6) = {4,111,14};
Line(7) = {4,15};
Line(8) = {5,15};
Line(9) = {5,6};

Line(10) = {6,24};
Line(11) = {7,24};
Circle(12) = {7,112,23};
Circle(13) = {22,112,23};
Circle(14) = {21,112,22};
Circle(15) = {9,112,21};
Line(16) = {9,20};
Line(17) = {10,20};
Line(18) = {1,10};
Line(19) = {11,19};
Line(20) = {12,18};

Line(21) = {13,17};
Line(22) = {14,16};
Line(23) = {15,16};
Line(24) = {16,24};
Line(25) = {16,23};
Line(26) = {16,17};
Line(27) = {17,22};
Line(28) = {17,18};
Line(29) = {18,21};
Line(30) = {19,20};


// Surface definition

Line Loop(31) = {1,19,30,-17,-18}; // why this is negetive (-17,-18), because for the line loop is ask for the same direction like one close circle,everyline has each direction
Line Loop(32) = {-2,3,20,29,-15,16,-30,-19};
Line Loop(33) = {4,21,28,-20};
Line Loop(34) = {-28,27,-14,-29};
Line Loop(35) = {-21,5,22,26};
Line Loop(36) = {-26,25,-13,-27};
Line Loop(37) = {-6,7,23,-22};
Line Loop(38) = {24,-11,12,-25};
Line Loop(39) = {-8,9,10,-24,-23};


Plane Surface(40) = {31};
Plane Surface(41) = {32};
Plane Surface(42) = {33};
Plane Surface(43) = {34};
Plane Surface(44) = {35};
Plane Surface(45) = {36};
Plane Surface(46) = {37};
Plane Surface(47) = {38};
Plane Surface(48) = {39};

Physical Surface(99) = {48};
Physical Surface(98) = {47};
Physical Surface(97) = {46};
Physical Surface(96) = {45};
Physical Surface(95) = {44};
Physical Surface(94) = {43};
Physical Surface(93) = {42};
Physical Surface(92) = {41};
Physical Surface(91) = {40};

Physical Line(1001) = {1};
Physical Line(1002) = {2};
Physical Line(1003) = {3};
Physical Line(1004) = {4};
Physical Line(1005) = {5};
Physical Line(1006) = {6};

Physical Line(1007) = {7};
Physical Line(1008) = {8};
Physical Line(1009) = {9};
Physical Line(1010) = {10};
Physical Line(1011) = {11};

Physical Line(1012) = {12};
Physical Line(1013) = {13};
Physical Line(1014) = {14};
Physical Line(1015) = {15};

Physical Line(1016) = {16};
Physical Line(1017) = {17};
Physical Line(1018) = {18};
Physical Line(1019) = {19};
Physical Line(1020) = {20};
Physical Line(1021) = {21};
Physical Line(1022) = {22};
Physical Line(1023) = {23};
Physical Line(1024) = {24};
Physical Line(1025) = {25};
Physical Line(1026) = {26};
Physical Line(1027) = {27};
Physical Line(1028) = {28};
Physical Line(1029) = {29};
Physical Line(1030) = {30};

Physical Point(2006) = {6};
Physical Point(2005) = {5};
Physical Point(2001) = {1};
Physical Point(2010) = {10};

/*



//Transfinite Line {1, 2, 4, 5, 8, 10, 11, 13, 14, 16, 17, 19, 20, 22, 23, 25, 26, 28} = 5 Using Progression 1;
//Transfinite Line {6, 7, 3, 9, 12, 15, 18, 21, 24, 27} = 2 Using Progression 1;
//Transfinite Surface {38};
//Recombine Surface {38};
//Transfinite Surface {39};
//Recombine Surface {39};
//Transfinite Surface {40};
//Recombine Surface {40};
//Transfinite Surface {41};
//Recombine Surface {41};
//Transfinite Surface {42};
//Recombine Surface {42};
//Transfinite Surface {43};
//Recombine Surface {43};
//Transfinite Surface {44};
//Recombine Surface {44};
//Transfinite Surface {45};
//Recombine Surface {45};
//Transfinite Surface {46};
//Recombine Surface {46};

//Mesh.Smoothing = 100;

*/
