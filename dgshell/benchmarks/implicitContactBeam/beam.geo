// Test case a SCB with a vertical load at its free extremity
// Size
x=0.1;
y=0.01;

// Cylinder
R = y/2.;
L = 0.02;
xc = x/2.+0.0001;
yc = y/2.+0.0001;
zz = 0.0001;
zc = R+zz;


// Characteristic length
Lc1=0.01;
Lc2=0.01;

// definition of points
Point(1) = { 0.0 , 0.0 , 0.0 , Lc1};
Point(2) = {  x  , 0.0 , 0.0 , Lc1};
Point(3) = {  x  ,  y  , 0.0 , Lc1};
Point(4) = { 0.0 ,  y  , 0.0 , Lc1};
Point(5) = {  xc ,yc+L/2., zc, Lc2};
Point(6) = {xc+R ,yc+L/2., zc, Lc2};
Point(7) = {xc-R ,yc+L/2., zc, Lc2};
Point(8) = { xc  ,yc-L/2., zc, Lc2};
Point(9) = {xc+R ,yc-L/2., zc, Lc2};
Point(10)= {xc-R ,yc-L/2., zc, Lc2};
Point(11)= { xc  ,yc+L/2., zz, Lc2};
Point(12)= { xc  ,yc+L/2.,zc+R,Lc2};
Point(13)= { xc  ,yc-L/2., zz ,Lc2};
Point(14)= { xc  ,yc-L/2.,zc+R,Lc2};
Point(15)= { x/2.,  0.   ,0.0, Lc1};
Point(16)= { x/2.,  y    ,0.0, Lc1}; 

// Line between points
Line(1) = {1,15};
Line(2) = {15,2};
Line(3) = {2,3};
Line(4) = {3,16};
Circle(5)={6,5,12};
Circle(6)={7,5,11};
Circle(7)={9,8,14};
Circle(8)={10,8,13};
Circle(9)={11,5,6};
Circle(10)={12,5,7};
Circle(11)={14,8,10};
Circle(12)={13,8,9};
Line(13)={14,12};
Line(14)={9,6};
Line(15)={10,7};
Line(16)={13,11};
Line(17)={16,4};
Line(18)={4,1};
Line(19)={15,16};
// Surface definition
Line Loop(1) = {1,19,17,18};
Plane Surface(1) = {1};
Line Loop(2) = {2,3,4,-19};
Plane Surface(2) = {2};

Line Loop(11) = {13,-5,-14,7};
Ruled Surface(11) = {11};
Line Loop(12) = {15,-10,-13,11};
Ruled Surface(12) = {12};
Line Loop(13) = {16,-6,-15,8};
Ruled Surface(13) = {13};
Line Loop(14) = {14,-9,-16,12};
Ruled Surface(14) = {14};

// Physical objects to applied BC and material
Physical Surface(99) = {1,2};
Physical Surface(1000) = {11,12,13,14};
Physical Line(103) = {3};
Physical Line(118) = {18};
Physical Point(202) ={2};
Physical Point(205) ={5};
Physical Point(208) ={8};
Physical Point(215) = {15};
Physical Point(216) = {16};
Transfinite Line {18, 19, 3} = 2 Using Progression 1;
Transfinite Line {1, 2,4,17} = 6 Using Progression 1;
Transfinite Line {5,6,7,8,9,10,11,12} = 4 Using Progression 1;
Transfinite Line {13,14,15,16} = 2 Using Progression 1;
Transfinite Surface {1,2,11,12,13,14};
Recombine Surface {1,2,11,12,13,14};
Mesh.Smoothing = 100;
