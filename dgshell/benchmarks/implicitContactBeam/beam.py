#-*-coding:Utf-8-*-
from gmshpy import *
from dgshellpy import *

#script to launch beam problem with a lua script

# material law
lawnum = 1 #  unique number of the law
E = 100.e9 # Young's modulus
nu = 0.3   # Poisson's ratio
rho =7850. 
# geometry
h = 0.01  # thickness
geofile = "beam.geo"
meshfile="beam.msh" # name of mesh file
# integration
nsimp = 3 # number of Simpson's points (odd)

# cylinder
cyl_thick = 0.01
c_penalty = 1.e13
rhocyl = 7850.
# solver
sol = 2 # Gmm=0 (default) Taucs=1 PETsc=2
beta1 = 10. # value of stabilization parameter
beta2 = 10.
beta3 = 10.
soltype = 1 # StaticLinear=0 (default) StaticNonLinear=1
nstep = 100   # number of step (used only if soltype=1)
ftime =1.  # Final time (used only if soltype=1)
tol=1e-3   # relative tolerance for NR scheme (used only if soltype=1)
nstepArch=1 # Number of step between 2 archiving (used only if soltype=1)
rhoinfty=1.
gamma_s = 0.036666667

#  compute solution and BC (given directly to the solver


# creation of law
law1 = linearElasticShellLaw(lawnum,E,nu,h,nsimp,rho)

# creation of ElasticField
nfield =99 # number of the field (physical number of surface)
fullDg = 1 #  formulation CgDg=0 fullDg =1
space1 = 0 # function space Lagrange=0
myfield1 = dgLinearShellDomain(1000,nfield,space1,lawnum,fullDg)
myfield1.stabilityParameters(beta1,beta2,beta3)
myfield1.gaussIntegration(dgLinearShellDomain.Lobatto,-1,5)
# creation of Solver
mysolver = nonLinearMechSolver(1000)
mysolver.createModel(geofile,meshfile,2,3,False)
mysolver.addDomain(myfield1)
mysolver.addMaterialLaw(law1)
mysolver.Scheme(soltype)
mysolver.Solver(sol)
mysolver.snlData(nstep,ftime,tol)
# contact
contact1 = dgC0ShellRigidCylinderContact(1000,1000,99,205,208,c_penalty,cyl_thick,rhocyl)
mysolver.contactInteraction(contact1)

# BC
mysolver.displacementBC("Edge",103,0,0.0000010)
mysolver.displacementBC("Edge",103,1,0.)
mysolver.displacementBC("Edge",103,2,0.)
mysolver.displacementBC("Edge",118,0,0.)
mysolver.displacementBC("Edge",118,1,0.)
mysolver.displacementBC("Edge",118,2,0.)
mysolver.displacementRigidContactBC(1000,2,-0.001)
mysolver.displacementRigidContactBC(1000,1,0.)
mysolver.displacementRigidContactBC(1000,0,0.)
mysolver.thetaBC(103)
mysolver.thetaBC(118)

mysolver.archivingForceOnPhysicalGroup("Edge",103,2)
mysolver.archivingNodeDisplacement(215,2)
mysolver.archivingNodeDisplacement(216,2)
mysolver.archivingRigidContact(1000,2,0)
mysolver.archivingRigidContact(1000,2,1)
mysolver.archivingRigidContact(1000,2,2)
mysolver.solve()

# check
check = TestCheck()
check.equal(7.542667e+03,mysolver.getArchivedForceOnPhysicalGroup('Edge',103,2),1e-2)
check.equal(mysolver.getArchivedNodalValue(215,2,mysolver.displacement),mysolver.getArchivedNodalValue(216,2,mysolver.displacement),1e-6)
