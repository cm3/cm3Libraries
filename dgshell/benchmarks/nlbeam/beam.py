#-*-coding:Utf-8-*-
from gmshpy import *
from dgshellpy import *
# bending beam

# material law
lawnum = 1 #  unique number of the law
E = 1.2e7 # Young's modulus
nu = 0.3   #  Poisson's ratio
rho =7850. 
yieldstress = 2.4e8 
harden = 1.2e5 # same for testing
# geometry
h = 0.1  # thickness
geofile="beam.geo"
meshfile="beam.msh" # name of mesh file
# integration
nsimp = 11 #  number of Simpson's points (odd)

# solver
sol = 2 # Gmm=0 (default) Taucs=1 PETsc=2
beta1 = 10. # value of stabilization parameter
beta2 = 10.
beta3 = 0.1 
soltype = 1 # StaticLinear=0 (default) StaticNonLinear=1
nstep = 13   # number of step (used only if soltype=1)
ftime =1.   #  Final time (used only if soltype=1)
tol=1.e-3  #  relative tolerance for NR scheme (used only if soltype=1)
nstepArch=1 # Number of step between 2 archiving (used only if soltype=1)

 # compute solution and BC (given directly to the solver

#  creation of law
#law1 = neoHookeanShellLaw(lawnum,E,nu,h,nsimp,rho)
law1 = J2linearShellLaw(lawnum,h,nsimp,rho,E,nu,yieldstress,harden)
law1.iterationData(1000,0.00001)
#  creation of ElasticField
nfield =99 # number of the field (physical number of surface)
fullDg = 0 #  formulation CgDg=0 fullDg =1
space1 = 0 # function space Lagrange=0
myfield1 = dgNonLinearShellDomain(1000,nfield,space1,lawnum,fullDg)
myfield1.stabilityParameters(beta1,beta2,beta3)
myfield1.matrixByPerturbation(1,1,1,1e-8)
#myfield1.gaussIntegration(dgLinearShellDomain.Gauss,4,5) #allow to choose the number og GP
#  creation of Solver
mysolver = nonLinearMechSolver(1000)
mysolver.createModel(geofile,meshfile,2,2,False)
mysolver.addDomain(myfield1)
mysolver.addMaterialLaw(law1)
mysolver.Scheme(soltype)
mysolver.Solver(sol)
mysolver.snlData(nstep,ftime,tol)
mysolver.stepBetweenArchiving(nstepArch)
# BC
mysolver.displacementBC("Edge",41,0,0.)
mysolver.displacementBC("Edge",41,1,0.)
mysolver.displacementBC("Edge",41,2,0.)
#mysolver.displacementBC("Edge",21,0,0.)
#mysolver.displacementBC("Edge",21,1,0.)
#mysolver.displacementBC("Edge",21,2,0.)
#mysolver.displacementBC("Edge",21,0,0.02)
#mysolver.forceBC("Face",99,2,1000.)
mysolver.forceBC("Edge",21,2,7.)
# mysolver.pressureOnPhysicalGroupBC(99,1000.)
mysolver.thetaBC(41)
#mysolver:AddThetaConstraint(21)
mysolver.archivingForceOnPhysicalGroup("Edge",41,2)
mysolver.archivingForceOnPhysicalGroup("Edge",21,2)
mysolver.archivingNodeDisplacement(22,2)
# view
for loc in range(nsimp):
  mysolver.internalPointBuildViewShell(loc,"PLASTICSTRAIN",IPField.PLASTICSTRAIN, 1, 1)
  mysolver.internalPointBuildViewShell(loc,"svm",IPField.SVM, 1, 1)
  mysolver.internalPointBuildViewShell(loc,"sig_xx",IPField.SIG_XX, 1, 1)
  mysolver.internalPointBuildViewShell(loc,"sig_yy",IPField.SIG_YY, 1, 1)
  mysolver.internalPointBuildViewShell(loc,"sig_zz",IPField.SIG_ZZ, 1, 1)
  mysolver.internalPointBuildViewShell(loc,"sig_xy",IPField.SIG_XY, 1, 1)
  mysolver.internalPointBuildViewShell(loc,"sig_yz",IPField.SIG_YZ, 1, 1)
  mysolver.internalPointBuildViewShell(loc,"sig_xz",IPField.SIG_XZ, 1, 1)
  
mysolver.solve()

# check by taking the value in the files (reducing the time step in case the NR does not converge is not (yet) allowed)
check = TestCheck()
try:
    import linecache
    lineforce = linecache.getline('force41comp2.csv',7)
    linedisp = linecache.getline('NodalDisplacementPhysical22Num2comp2.csv',3)
    lineener = linecache.getline('energy.csv',7)
except:
    print('Cannot get the results in the files')
    import os
    os._exit(1)
else:
    force = float(lineforce.split(';')[1])
    disp = float(linedisp.split(';')[1])
    ener = float(lineener.split(';')[4])
    check.equal(-3.769269e+00,force)
    check.equal(5.455080e-01,disp)
    check.equal(1.736274e+00,ener)
