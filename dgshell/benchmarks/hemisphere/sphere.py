#-*-coding:Utf-8-*-
from gmshpy import *
from dgshellpy import *
#  Script for open hemisphere benchmark

#material law
lawnum = 1
E = 68.25e6
nu = 0.3
rho = 1000.

# geometry
h = 0.04
geofile = "sphere.geo"
meshfile = "sphere.msh"
# integration
nsimp = 1 

# solver
sol = 2 
beta1 = 1000.
beta2 = 100.
beta3 = 1000.
soltype = 1 
nstep = 1 
ftime = 1.
tol = 2.e-5
nstepArch = 1

#Compute solution and BC

# creation of material law
law1 = linearElasticShellLaw(lawnum,E,nu,h,nsimp,rho)

# creation of field
nfield = 99
fullDg = 1 
space1=0
myfield1 = dgLinearShellDomain(1000,nfield,space1,lawnum,fullDg)
myfield1.stabilityParameters(beta1,beta2,beta3)
# creation of Solver
mysolver = nonLinearMechSolver(1000)
mysolver.createModel(geofile,meshfile,2,2,True)
mysolver.addDomain(myfield1)
mysolver.addMaterialLaw(law1)
mysolver.Scheme(soltype)
mysolver.Solver(sol)
mysolver.snlData(nstep,ftime,tol)
mysolver.stepBetweenArchiving(nstepArch)

# BC
mysolver.displacementBC("Edge",12,0,0.)
mysolver.displacementBC("Edge",14,1,0.)
mysolver.displacementBC("Node",222,2,0.)

mysolver.forceBC("Node",222,1,-20.)
mysolver.forceBC("Node",111,0,20.)

mysolver.thetaBC(12)
mysolver.thetaBC(14)

# view
mysolver.internalPointBuildView("svm",IPField.SVM, 1, 1)
mysolver.internalPointBuildView("sig_xx",IPField.SIG_XX, 1, 1)
mysolver.internalPointBuildView("sig_yy",IPField.SIG_YY, 1, 1)
mysolver.internalPointBuildView("sig_zz",IPField.SIG_ZZ, 1, 1)
mysolver.internalPointBuildView("sig_xy",IPField.SIG_XY, 1, 1)
mysolver.internalPointBuildView("sig_yz",IPField.SIG_YZ, 1, 1)
mysolver.internalPointBuildView("sig_xz",IPField.SIG_XZ, 1, 1)
mysolver.internalPointBuildView("epl",IPField.PLASTICSTRAIN, 1, 1)

# archiving
mysolver.archivingNodeDisplacement(111,0)
mysolver.archivingNodeDisplacement(222,1)
# solve
mysolver.solve()

# check
check = TestCheck()
check.equal(mysolver.getArchivedNodalValue(111,0,mysolver.displacement),-mysolver.getArchivedNodalValue(222,1,mysolver.displacement),2.0e-4)
check.equal(2.490671e+05,mysolver.getEnergy(energeticField.deformation))
check.equal(3.458674e+01,mysolver.getEnergy(energeticField.external))
