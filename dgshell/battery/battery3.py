#-*-coding:Utf-8-*-

# Program to check benchmark of dgshell

# import
from os import system, listdir, path , chdir, getcwd
import sys
import getopt

# function used
def getinfoline(rf,fline,fcolu,fsepa):
    try:
        for i in range(fline) :
            tline = rf.readline()
        compline = tline.split(fsepa)
        valline = float(compline[fcolu-1])
        return [1,valline]
    except:
        return [0,0]
        
def readoneline(afn,wf):
    try:
        rf = open(afn,'r')
    except:
        print("The file %s doesn't exist")
    else :
        print("File line First one = 1")
        while 1:
            try:
                fline = int(input())
            except:
                print("Please give an integer")
            else:
                break
        print("File column. First one = 1")
        while 1:
            try:
                fcolu = int(input())
            except:
                print("Please give an integer")
            else:
                break
        while 1:
            print("separator Warning cannot be @@")
            fsepa = input()
            if fsepa != "@@":
                break
        flag =  getinfoline(rf,fline,fcolu,fsepa)
        if flag[0]:
            print("The value is: %f"%flag[1])
            while 1:
                print("write value in reference file. yes or no")
                ff = input()
                if ff == "yes":
                    wf.write("%s@@%d@@%d@@%s@@%e\n"%(afn,fline,fcolu,fsepa,flag[1]))
                    break
                elif ff == "no":
                    break
  
class Battery() :
    def __init__(self) :
        # in relative the benchmarks are always situated at 
        # the same place of battery
        self.benchpath = "../benchmarks"
        self.reportname= "report.csv"
    def printPath(self) :
        print("Path of benchmarks %s"%self.benchpath)
    def addBenchmark(self,bfname):
        if(bfname == "") :
            print("Please give the benchmark's folder name")
            bfname = input()
        # check if the benchmark already exists
        try:
            rf = open(bfname,"r")
            fsvn="no" # already exist so it has not be added to svn
        except:
            # Check if the benchmark exists
            ldir = listdir(self.benchpath)
            lfolder =[]
            for fna in ldir :
                if( not ( path.isfile(self.benchpath+"/"+fna))):
                    lfolder.append(fna)
            if(bfname in lfolder):
                flag="yes"
                # check if the benchmark is under svn
                ldirb = listdir(self.benchpath+"/"+bfname)
                if not (".svn" in ldirb):
                    while 1:
                        print("This benchmark is not under svn. Add to svn ? yes or no")
                        fsvn = input()
                        if fsvn == "yes":
                            # remove results
                            print("svn operations:")
                            system("rm "+self.benchpath+"/"+bfname+"/*.csv")
                            system("rm "+self.benchpath+"/"+bfname+"/disp*")
                            system("rm "+self.benchpath+"/"+bfname+"/stress*")
                            system("rm "+self.benchpath+"/"+bfname+"/energy*")
                            system("rm "+self.benchpath+"/"+bfname+"/*.*~")
                            # add svn
                            system("svn add "+self.benchpath+"/"+bfname)
                            break
                        elif fsvn== "no":
                            break
                else:
                    fsvn="yes"               
            else:
                flag="no" 
                print("The benchmark %s seems to be missing"%bfname) 
        else :
            rf.close()
            flag=""
            while((flag != "yes") and (flag != "no")):
                print("Warning:    This benchmark already exists. Do you want to redifine it ? yes or no")
                flag = input()
        if(flag=="yes"):              
            print("Enter name of data file (.py)")
            dfile = input()
            # check existance of file
            try:
                rdf = open(self.benchpath+"/"+bfname+"/"+dfile,'r')
            except:
                print("Error:    This data file does not exist")
            else:
                rdf.close()
                wf = open(bfname,"w")
                wf.write(dfile+"\n")
                # launch test case
                # remove old data file (.csv)
                system("rm "+self.benchpath+"/"+bfname+"/*.csv")
                print("Perform benchmark. Please wait")
                curfolder = getcwd()
                # change folder to execute dgshell
                chdir(self.benchpath+"/"+bfname)
                system("python3 "+dfile) 
                print("benchmark terminated")
                print("You can now give the reference value store in a text file")
                while 1:
                    print("Give a file name. <Return> to finish")
                    afn = input()
                    if(afn == ""):
                        break
                    else:
                        readoneline(afn,wf)                        
                # current folder
                chdir(curfolder)
                wf.close()
                # add to svn
                if fsvn == "yes":
                    print("svn operations:")
                    system("svn add "+bfname)
    def launch(self,bfname,freplace) :
        curdir = getcwd()
        # write report
        wf = open(self.reportname,'w')
        wf.write("Battery's report: \n\n")
        wf.close()
        if(bfname == ""): # Do all
            # list with benchmark's name
            ldir = listdir(curdir)
            lfile=[]
            for fna in ldir:
                if((path.isfile(fna)) and (fna != "battery.py") and ( fna != "%s"%self.reportname)):
                    lfile.append(fna)
            i=1
            nbench=len(lfile)
            for fna in lfile:
                print("benchmark %d on %d"%(i,nbench))
                self.launchOne(fna,freplace)
                i=i+1
        else:
            # check if the benchmark already exists
            try:
                rf = open(bfname,"r")
            except:
                print("This benchmark does not exist")
            else:
                rf.close()
                self.launchOne(bfname,freplace)        
        print("All selected benchmarks are done. See %s for details"%self.reportname)
    def launchOne(self,bfname,freplace):
        # Check that the benchmark file exist is made before this function
        # but here check the existence of benchmark
        ldir = listdir(self.benchpath)
        lfolder =[]
        for fna in ldir :
            if( not ( path.isfile(self.benchpath+"/"+fna))):
               lfolder.append(fna)
        if(bfname in lfolder):
            # go to benchmark directory
            curdir = getcwd()
            rf = open(bfname,'r')
            dfile = rf.readline()[:-1]
            if freplace:
              wf = open(bfname+".temp",'w')
              wf.write(dfile+"\n")
            chdir(self.benchpath+"/"+bfname)
            system("rm *.csv")
            print("Perform benchmark. Please Wait")
            system("python3 "+dfile)
            print("benchmark terminated")
            chdir(curdir)
            # write report
            wrf = open(self.reportname,'a')
            wrf.write("benchmark's name: %s\n\n"%bfname)
            wrf.write("File result    ;    line number    ;    column number    ;    reference value    ;    this time value    ;    relative error [%]\n")
            while 1:
                temp = rf.readline()
                if temp == "":
                    break
                lch = temp[:-1].split("@@")
                try:
                    rrf = open(self.benchpath+"/"+bfname+"/"+lch[0],'r')
                except:
                        print("Warning a result doesn't exist. see %s for more information"%self.reportname)
                        wrf.write("%s    ;    %d    ;    %d    ;    %e    ;   missing    ;    --\n"%(lch[0],int(lch[1]),int(lch[2]),float(lch[4])))
                        if freplace:
                            wf.write(temp)
                else :
                    tr = getinfoline(rrf,int(lch[1]),int(lch[2]),lch[3])
                    if tr[0] == 0:
                        print("Warning a result doesn't exist. see %s for more information"%self.reportname)
                        wrf.write("%s    ;    %d    ;    %d    ;    %e    ;   missing    ;    --\n"%(lch[0],int(lch[1]),int(lch[2]),float(lch[4])))
                        if freplace:
                            wf.write(temp)
                    else:
                        wrf.write("%s    ;    %d    ;    %d    ;    %e    ;   %e    ;    %e\n"%(lch[0],int(lch[1]),int(lch[2]),float(lch[4]),tr[1],100.*(float(lch[4])-tr[1])/float(lch[4])))
                        if freplace:
                            tch = "@@".join(lch[:-1])+"%e"%tr[1]
                    rrf.close()
            wrf.write("\n")
            rf.close()
            wrf.close()
            if freplace:
                system("mv %s.temp %s"%(bfname,bfname))
        else:
            print("Can't execute benchmark %s because it missing"%bfname)
    def addBenchTosvn(self,bfname):
        if(bfname == "") :
            print("Please give the benchmark's folder name")
            bfname = input()
        # check if the benchmark already exists
        try:
            rf = open(bfname,"r")
        except:
            print("This benchmark does not exist")
        else:
            # check if the folder exist
            ldir = listdir(self.benchpath)
            lfolder =[]
            for fna in ldir :
                if( not ( path.isfile(self.benchpath+"/"+fna))):
                    lfolder.append(fna)
            if(bfname in lfolder):
                # check if the benchmark is under svn
                ldirb = listdir(self.benchpath+"/"+bfname)
                if not (".svn" in ldirb):
                    # remove results
                    print("svn operations:")
                    system("rm "+self.benchpath+"/"+bfname+"/*.csv")
                    system("rm "+self.benchpath+"/"+bfname+"/disp*")
                    system("rm "+self.benchpath+"/"+bfname+"/stress*")
                    system("rm "+self.benchpath+"/"+bfname+"/energy*")
                    system("rm "+self.benchpath+"/"+bfname+"/*.*~")
                    # add svn
                    system("svn add "+self.benchpath+"/"+bfname)
                    system("svn add "+bfname)
                else:
                    print("This benchmark is already under svn")
            else:
                print("No benchmark name %s in %s"%(bfname,self.benchpath))

    def addData2Benchmark(self,bfname):
        if(bfname == "") :
            print("Please give the benchmark's folder name")
            bfname = input()
        # check if the benchmark already exists
        try:
            rf = open(bfname,"r")
        except:
            print("This benchmark does not exist")
        else:
            # check if the folder exist
            ldir = listdir(self.benchpath)
            lfolder =[]
            for fna in ldir :
                if( not ( path.isfile(self.benchpath+"/"+fna))):
                    lfolder.append(fna)
            if(bfname in lfolder):
                dfile = rf.readline()[:-1]
                rf.close()
                wf = open(bfname,'a')
                print("Perform benchmark. Please wait")
                curfolder = getcwd()
                # change folder to execute dgshell
                chdir(self.benchpath+"/"+bfname)
                system("rm *.csv")
                system("python3 "+dfile)
                print("benchmark terminated")
                print("You can now give the reference value store in a text file")
                while 1:
                    print("Give a file name. <Return> to finish")
                    afn = input()
                    if(afn == ""):
                        break
                    else:
                        readoneline(afn,wf)
                # current folder
                chdir(curfolder)
                wf.close()
            else:
                print("This benchmark does not exist")

    def removeData2Benchmark(self,bfname):
        if(bfname == "") :
            print("Please give the benchmark's folder name")
            bfname = input()
        # check if the benchmark already exists
        try:
            rf = open(bfname,"r")
        except:
            print("This benchmark does not exist")
        else:
            # data to remove
            while 1:
                print("File data <Enter> to quit")
                afn = input()
                if afn =="":
                    break
                print("File line First one = 1")
                while 1:
                    try:
                        fline = int(input())
                    except:
                        print("Please give an integer")
                    else:
                        break
                print("File column. First one = 1")
                while 1:
                   try:
                       fcolu = int(input())
                   except:
                       print("Please give an integer")
                   else:
                       break
                print("separator")
                fsepa = input()
                # string with data
                rdata = "%s@@%d@@%d@@%s"%(afn,fline,fcolu,fsepa)
                # writing file to copy rfile
                wf = open(bfname+".temp","w")
                # cover rf file and compare to rdata
                wf.write(rf.readline()) # name of .dat or .lua
                mflag=0
                while 1: 
                    temp = rf.readline()
                    if temp =="":
                        break
                    else:
                        lch = temp[:-1].split("@@")
                        ch = "@@".join(lch[:-1])
                        if ch != rdata :
                            wf.write(temp)
                        else:
                            mflag = 1
                system("mv %s.temp %s"%(bfname,bfname))
                if not  mflag:
                    print("Warning:    Your data does not exist")
    def removeBenchmark(self,bfname):

        if(bfname == "") :
            print("Please give the benchmark's folder name")
            bfname = input()
        # check if the benchmark already exists
        try:
            rf = open(bfname,"r")
        except:
            print("This benchmark does not exist")
        else:
            # check if the folder exist
            ldir = listdir(self.benchpath)
            lfolder =[]
            for fna in ldir :
                if( not ( path.isfile(self.benchpath+"/"+fna))):
                    lfolder.append(fna)
            if(bfname in lfolder):
                # check if the benchmark is under svn
                ldirb = listdir(self.benchpath+"/"+bfname)
                fsvn = 1 
                if not (".svn" in ldirb):
                    fsvn = 0  
                # remove file
                if fsvn:
                    system("svn rm --force %s"%bfname)
                else:
                    system("rm -f %s"%bfname)
                # ask before remove of benchmark
                while 1:
                    print("The benchmark %s is not included in battery"%bfname)
                    print("Do you want to remove benchmark from disc")
                    print("yes or no")
                    flag = input()
                    if flag == "yes":
                        # check if the benchmark is under svn
                        ldirb = listdir(self.benchpath+"/"+bfname)
                        if not fsvn:
                            system("rm -rf "+self.benchpath+"/"+bfname)
                        else:
                            system("svn rm --force "+self.benchpath+"/"+bfname)
                        break
                    elif flag == "no":
                        break 
            else:
                print("This benchmark does not exist")

# Write help messages
def bathelp():
    print("Battery is a program to manage and launch benchmarks for dgshell program")
    print("The following options are available")
    print("help : display this message")
    print("print_path : display the path of chosen and relative benchmarks path")
    print("add_result=[benchmark's folder name] : Add a result in to a registred benchmark")
    print("remove_result=[benchmark's folder name]: Remove a result from a benchmark")
    print("reset_result=[benchmark's folder name] : Perform benchmark and replace the values of reference. If the name is not give do all benchmark")
    print("new=[benchmark's folder name] : Allow to add a new benchmark")
    print("remove=[benchmark's folder name]: Remove a benchmark")
    print("launch=[benchmark's folder name] : launch the benchmarks name. if the name is not given perform all benchmarks") 
    print("add_svn=[benchmark's folder name]: Put the benchmark under svn") 

# lancement de l'application
if __name__=='__main__' :
    # lecture des arguments passés 
    # si argument -s =--save alors synchronisation
    # sans interface graphique
    opts,args=None,None
    try :
        opts,args=getopt.getopt(sys.argv[1:],"",["launch=",
                                                 "add_result=",
                                                 "remove_result=",
                                                 "reset_result=",
                                                 "new=",
                                                 "add_svn=",
                                                 "remove=",
                                                 "print_path",
                                                 "help"])
    except getopt.GetoptError as e :
        print('Unknown argument "%s" in command line.'%(e.opt))
        bathelp()
    else:
        # Mise des arguments dans un dictionnaire (+ facile à utiliser)
        option={}
        for opt,val in opts :
            option[opt]=val
        # launch
        bat = Battery()
        if((len(option)==0) or ("--help" in list(option.keys()))) :
            bathelp()       
        if "--print_path" in list(option.keys()) :
            bat.printPath()
        if "--new" in list(option.keys()) :
            bat.addBenchmark(option["--new"]) 
        if "--add_svn" in list(option.keys()) :
            bat.addBenchTosvn(option["--add_svn"])
        if "--add_result" in list(option.keys()) :
            bat.addData2Benchmark(option["--add_result"])
        if "--remove" in list(option.keys()):
            bat.removeBenchmark(option["--remove"])
        if "--launch" in list(option.keys()):
            bat.launch(option["--launch"],0)
        if "--reset_result" in list(option.keys()):
            bat.launch(option["--reset_result"],1)
        if "--remove_result" in list(option.keys()):
            bat.removeData2Benchmark(option["--remove_result"])
