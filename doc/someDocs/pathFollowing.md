# Options for path following method

## Generalities

In the path following method, the external loading is parameterized by a scalar, so-called load factor $\lambda$, which is considered as an additional unknown of the problem. The balance equation reads 

$`\mathbf{F}_{int}(\mathbf{U}, \mathbf{Z}) = \lambda\mathbf{F}_{ext},`$

 where $`\mathbf{U}`$ is the unknown vector and $`\mathbf{Z}`$ is the vector including all historical variables to follow path-dependent processes. To close the problem statement, an additional equation is added: 
 
 $`\Phi(\mathbf{U},\lambda,...)=0.`$ 
 
 In the code, there exists two different possibilities:

* Hyper-spherical control: the addition equation is built directly based on the degree of freedom under a hyper-spherical form: 

$`\Phi = a_\mathbf{U} {\Delta \mathbf{U}}^T {\Delta \mathbf{U}} + a_{\lambda} \Delta\lambda^2 - \Delta s^2.`$ 

where $\Delta s$ is the arc-length increment, and where $a_\mathbf{U}$ and $a_\lambda$ are two constants. There also exists different possibilities:

  - Arc-length control: this name is abused for three different cases:
      + $`a_\mathbf{U}=1`$ and $`a_\lambda=0`$: state control
      + $`a_\mathbf{U}=1`$ and $`a_\lambda=1`$: arc-length control
      + $`a_\mathbf{U}=0`$ and $`a_\lambda=1`$: load control (this case corresponds to the usual load increment Newton-Raphson procedure).
  - Hyperelliptic control: in general, each DOF in $`\mathbf{U}`$ has different nature (displacement, nonlocal, etc.). In this case, we can apply the path following control for several components (defined by the component of a DOF in the solver, e.g. 0,1,2 corresponding to displacement field, etc) 
  
$`\Phi = \sum_i a_\mathbf{U}^i {\Delta \mathbf{U}}_i^T {\Delta \mathbf{U}}_i + a_{\lambda} \Delta\lambda^2 - \Delta s^2.`$

The value of $`a_\mathbf{U}^i`$ can be specified to obtain a desired control.

* Irreversible energy-based control: the addition equation is built based on the dissipation energy. It is advantageous for problems involving dissipation. This method must be combine with a load control at the begining at which there is no dissipation.

* Automatic time stepping: the path following should be used with the automatic time stepping. The automatic time step is estimated by

\pagebreak
# Some useful options

## Arc-length control

## Irreversible energy-based control

## Hyperelliptic control

\pagebreak
# More details about path following options

## To activate the path following method

By supposing the solver ```mysolver``` has been created by
```python
#------------------------------
mysolver=nonLinearMechSolver(tag)
# tag can be an arbitrary integer
```
The path following can be activated using
```python
#------------------------------
mysolver.pathFollowing(const bool p, const int method)
# p must be True to activate the path following
# method must be chosen between 0 and 1
#        =0 if a global arc-length control is used
#        =1 if a dissipation-based control is used
#        =2 if a hyper elliptic control is used
```
Once a method is actived, the options for each method need to be specified as follows.

## Global arc-length control

In case of the global arc-length control, the following options can be considered:

* The kind of constraints
```python
#------------------------------
mysolver.setPathFollowingControlType(const int type)
# type (1 by default) must be speficified between 0, 1, or 2 as
#   = 0 for a pure state control
#   = 1 for an arc-length control
#   = 2 for a pure load control
```
* When considering ```method=0 or 1```, we can specify the correction method by
```python
#------------------------------
mysolver.setPathFollowingCorrectionMethod(const int corrmethod)
# corrmethod (1 by default) must be speficified between O, 1, 2
#            = 0 if the main system (i.e. Fint = Fext)
#                and path following constrol are solved simultanously
#            = 1 if the path following is solved after the main system
#            = 2 if the path following is solved after the main system
#                and a safe condition for the control equation is considered.
```
