SetFactory("OpenCASCADE");
Geometry.NumSubEdges = 100; // nicer display of curve
//Mesh.CharacteristicLengthMin = 0.24;
//Mesh.CharacteristicLengthMax = 0.40;

Mesh.CharacteristicLengthMin = 0.40;
Mesh.CharacteristicLengthMax = 0.60;

//ellips geomtry information ----------
b0 = 0.04875;
a = 0.12/b0/3.14159265;
e1 = 0.08;
e2 = 0.005;
L = 4*a + 2*e1;
l = 30.0/L;
cl=b0/1.0;

theta =  1.0/(1.0+Exp(l*L/4.0))-0.5;
b = b0*1.1;
z1 = -2.0*b*theta;

// mesh size --------------
lc1 = 0.100;
lc2 = 0.050;
lc3 = 0.05;

// origin of coordinate

P0 = newp;
Point(P0) = {0.0, 0, 0.0, cl};

//first wire ------------
npts = 30;
dl = L/(2*npts);
start_numP = newp;
For i In {start_numP:start_numP+npts}
  theta =  1.0/(1.0+Exp(-l*((i-start_numP) * dl-L/4.0)))-0.5;
  Point(i) = {(i-start_numP) * dl, 0, -2.0*b*theta};
EndFor
For i In {start_numP+npts+1:start_numP+2*npts+2}
  theta =  1.0/(1.0+Exp(-l*((i-start_numP) * dl-3.0*L/4.0)))-0.5;
  Point(i) = {(i-start_numP) * dl, 0, 2.0*b*theta};
EndFor
Spline(1) = {start_numP:start_numP+2*npts+2};
Wire(1) = {1};

//second wire ------------
start_numP = newp;
For i In {start_numP:start_numP+npts}
  theta =  1.0/(1.0+Exp(-l*((i-start_numP) * dl-L/4.0)))-0.5;
  Point(i) = {(i-start_numP) * dl, L/2.0, 2.0*b*theta};
EndFor
For i In {start_numP+npts+1:start_numP+2*npts+2}
  theta =  1.0/(1.0+Exp(-l*((i-start_numP) * dl-3.0*L/4.0)))-0.5;
  Point(i) = {(i-start_numP) * dl, L/2.0, -2.0*b*theta};
EndFor
Spline(2) = {start_numP:start_numP+2*npts+2};
Wire(2) = {2};

//third wire ------------
start_numP = newp;
For i In {start_numP:start_numP+npts}
  theta =  1.0/(1.0+Exp(-l*((i-start_numP) * dl-L/4.0)))-0.5;
  Point(i) = {(i-start_numP) * dl, -L/2.0, 2.0*b*theta};
EndFor
For i In {start_numP+npts+1:start_numP+2*npts+2}
  theta =  1.0/(1.0+Exp(-l*((i-start_numP) * dl-3.0*L/4.0)))-0.5;
  Point(i) = {(i-start_numP) * dl, -L/2.0, -2.0*b*theta};
EndFor
Spline(3) = {start_numP:start_numP+2*npts+2};
Wire(3) = {3};

//forth wire ------------
start_numP = newp;
For i In {start_numP:start_numP+npts}
  theta =  1.0/(1.0+Exp(-l*((i-start_numP) * dl-L/4.0)))-0.5;
  Point(i) = {L, (i-start_numP) * dl-L/2.0, -2.0*b*theta};
EndFor
For i In {start_numP+npts+1:start_numP+2*npts+2}
  theta =  1.0/(1.0+Exp(-l*((i-start_numP) * dl-3.0*L/4.0)))-0.5;
  Point(i) = {L, (i-start_numP) * dl-L/2.0, 2.0*b*theta};
EndFor
Spline(4) = {start_numP:start_numP+2*npts+2};
Wire(4) = {4};

//fifth wire ------------
start_numP = newp;
For i In {start_numP:start_numP+npts}
  theta =  1.0/(1.0+Exp(-l*((i-start_numP) * dl-L/4.0)))-0.5;
  Point(i) = {L/2.0, (i-start_numP) * dl-0.5*L,  2.0*b*theta};
EndFor
For i In {start_numP+npts+1:start_numP+2*npts+2}
  theta =  1.0/(1.0+Exp(-l*((i-start_numP) * dl-3.0*L/4.0)))-0.5;
  Point(i) = {L/2.0, (i-start_numP) * dl-0.5*L,  -2.0*b*theta};
EndFor
Spline(5) = {start_numP:start_numP+2*npts+2};
Wire(5) = {5};

//sixth wire ------------
start_numP = newp;
For i In {start_numP:start_numP+npts}
  theta =  1.0/(1.0+Exp(-l*((i-start_numP) * dl-L/4.0)))-0.5;
  Point(i) = {0, (i-start_numP) * dl-L/2.0, -2.0*b*theta};
EndFor
For i In {start_numP+npts+1:start_numP+2*npts+2}
  theta =  1.0/(1.0+Exp(-l*((i-start_numP) * dl-3.0*L/4.0)))-0.5;
  Point(i) = {0, (i-start_numP) * dl-L/2.0, 2.0*b*theta};
EndFor
Spline(6) = {start_numP:start_numP+2*npts+2};
Wire(6) = {6};

//ellipse---------------------------------------------------
P0 = newp;
Point(P0) = {0,0,z1,cl};
P1 = newp;
Point(P1) = {0,-b0,z1,cl};
P2 = newp;
Point(P2) = {a,0,z1,cl};
P3 = newp;
Point(P3) = {0,b0,z1,cl};
P4 = newp;
Point(P4) = {-a,0,z1,cl};

l1=newreg;
Ellipse(l1) = {P1,P0,P2,P2};
l2=newreg;
Ellipse(l2) = {P2,P0,P2,P3};
l3=newreg;
Ellipse(l3) = {P3,P0,P4,P4};
l4=newreg;
Ellipse(l4) = {P4,P0,P4,P1};
l5=newreg;
Curve Loop(l5) = {l1,l2,l3,l4};   
Plane Surface(1) = {l5};


Rotate {{0,1, 0}, {0,0,z1}, Pi/2} { Surface{1}; }
Rotate {{1,0, 0}, {0, 0, z1}, Pi/2} { Surface{1}; }
Translate {0.0,-L/2.0,-2.0*z1} {Duplicata{Surface{1};} }
Translate {0.0,L/2.0,-2.0*z1} {Duplicata{Surface{1};} }

Rotate {{0,0,1}, {0,-L/2.0,0}, Pi/2} {Duplicata{Surface{1,2,3};} }
Translate {1.5*L, 0, 0} { Surface{4}; }
Translate {L/2, 0, 0} { Surface{5}; }
Translate {L, 0, 2.0*z1} {Surface{6}; }

//--extrude yarn -------------

Extrude { Surface{1}; } Using Wire {1}
Delete{ Surface{1}; }

Extrude { Surface{2}; } Using Wire {2}
Delete{ Surface{2}; }

Extrude { Surface{3}; } Using Wire {3}
Delete{ Surface{3}; }

Extrude { Surface{4}; } Using Wire {4}
Delete{ Surface{4}; }

Extrude { Surface{5}; } Using Wire {5}
Delete{ Surface{5}; }

Extrude { Surface{6}; } Using Wire {6}
Delete{ Surface{6}; }


//c()=BooleanUnion{Volume{ 1,2,3 }; Delete;}{Volume{ 4,5,6 }; Delete;};
b1=newv;
Box(b1) = {0.0,-0.5*L,-2.0*b-e2, L, L, 4.0*b+2.0*e2};
//c1() = BooleanIntersection {Volume{c()}; Delete; }{ Volume{b1}; };
c1() = BooleanIntersection {Volume{1,2,3,4,5,6}; Delete; }{ Volume{b1}; };

fr() = BooleanFragments{ Volume{b1}; Delete; }{Volume{c1()};Delete; };


//--------------BC surfaces ---------------

tolBB=5.e-3;

xMin = 0.;
xMax = L;
yMin=-L/2.;
yMax=L/2.;
zMin=-2.*b-e2;
zMax= 2.*b+e2;


surfX0() = Surface In BoundingBox {xMin-tolBB, yMin-tolBB, zMin-tolBB, xMin+tolBB, yMax+tolBB, zMax+tolBB};
Printf("Surface X = 0 ", surfX0());
surfXL() = Surface In BoundingBox {xMax-tolBB, yMin-tolBB, zMin-tolBB, xMax+tolBB, yMax+tolBB, zMax+tolBB};
Printf("Surface X = L ", surfXL());

surfY0() = Surface In BoundingBox {xMin-tolBB, yMin-tolBB, zMin-tolBB, xMax+tolBB, yMin+tolBB, zMax+tolBB};
Printf("Surface Y = 0 ", surfY0());
surfYL() = Surface In BoundingBox {xMin-tolBB, yMax-tolBB, zMin-tolBB, xMax+tolBB, yMax+tolBB, zMax+tolBB};
Printf("Surface Y = L ", surfYL());

surfZ0() = Surface In BoundingBox {xMin-tolBB, yMin-tolBB, zMin-tolBB, xMax+tolBB, yMax+tolBB, zMin+tolBB};
Printf("Surface Z = 0 ", surfZ0());
surfZL() = Surface In BoundingBox {xMin-tolBB, yMin-tolBB, zMax-tolBB, xMax+tolBB, yMax+tolBB, zMax+tolBB};
Printf("Surface Z = L ", surfZL());

edZ0Y0() = Line In BoundingBox {xMin-tolBB, yMin-tolBB, zMin-tolBB, xMax+tolBB, yMin+tolBB, zMin+tolBB};
Printf("Edge Z = 0 Y=0", edZ0Y0());
edZ0YL() = Line In BoundingBox {xMin-tolBB, yMax-tolBB, zMin-tolBB,  xMax+tolBB, yMax+tolBB, zMin+tolBB};
Printf("Edge Z = 0 Y=L", edZ0YL());
edZLY0() = Line In BoundingBox {xMin-tolBB, yMin-tolBB, zMax-tolBB, xMax+tolBB, yMin+tolBB, zMax+tolBB};
Printf("Edge Z = L Y=0", edZLY0());
edZLYL() = Line In BoundingBox {xMin-tolBB, yMax-tolBB, zMax-tolBB, xMax+tolBB, yMax+tolBB, zMax+tolBB};
Printf("Edge Z = L Y=L", edZLYL());

edX0Y0() = Line In BoundingBox {xMin-tolBB, yMin-tolBB, zMin-tolBB, xMin+tolBB, yMin+tolBB, zMax+tolBB};
Printf("Edge X = 0 Y=0", edX0Y0());
edX0YL() = Line In BoundingBox {xMin-tolBB, yMax-tolBB, zMin-tolBB, xMin+tolBB, yMax+tolBB, zMax+tolBB};
Printf("Edge X = 0 Y=L", edX0YL());
edXLY0() = Line In BoundingBox {xMax-tolBB, yMin-tolBB, zMin-tolBB, xMax+tolBB, yMin+tolBB, zMax+tolBB};
Printf("Edge X = L Y=0", edX0Y0());
edXLYL() = Line In BoundingBox {xMax-tolBB, yMax-tolBB, zMin-tolBB, xMax+tolBB, yMax+tolBB, zMax+tolBB};
Printf("Edge X = L Y=L", edXLYL());

edX0Z0() = Line In BoundingBox {xMin-tolBB, yMin-tolBB, zMin-tolBB, xMin+tolBB, yMax+tolBB, zMin+tolBB};
Printf("Edge X = 0 Z=0", edX0Z0());
edX0ZL() = Line In BoundingBox {xMin-tolBB, yMin-tolBB, zMax-tolBB, xMin+tolBB, yMax+tolBB, zMax+tolBB};
Printf("Edge X = 0 Y=L", edX0ZL());
edXLZ0() = Line In BoundingBox {xMax-tolBB, yMin-tolBB, zMin-tolBB, xMax+tolBB, yMax+tolBB, zMin+tolBB};
Printf("Edge X = L Y=0", edX0Z0());
edXLZL() = Line In BoundingBox {xMax-tolBB, yMin-tolBB, zMax-tolBB, xMax+tolBB, yMax+tolBB, zMax+tolBB};
Printf("Edge X = L Y=L", edXLZL());


ptX0Y0Z0() = Point In BoundingBox {xMin-tolBB, yMin-tolBB, zMin-tolBB, xMin+tolBB, yMin+tolBB, zMin+tolBB};
Printf("Point X = 0 Y=0 Z=0", ptX0Y0Z0());
ptXLY0Z0() = Point In BoundingBox {xMax-tolBB, yMin-tolBB, zMin-tolBB, xMax+tolBB, yMin+tolBB, zMin+tolBB};
Printf("Point X = L Y=0 Z=0", ptXLY0Z0());
ptX0YLZ0() = Point In BoundingBox {xMin-tolBB, yMax-tolBB, zMin-tolBB, xMin+tolBB, yMax+tolBB, zMin+tolBB};
Printf("Point X = 0 Y=0 Z=0", ptX0Y0Z0());
ptXLYLZ0() = Point In BoundingBox {xMax-tolBB, yMax-tolBB, zMin-tolBB, xMax+tolBB, yMax+tolBB, zMin+tolBB};
Printf("Point X = L Y=0 Z=0", ptXLY0Z0());

ptX0Y0ZL() = Point In BoundingBox {xMin-tolBB, yMin-tolBB, zMax-tolBB, xMin+tolBB, yMin+tolBB, zMax+tolBB};
Printf("Point X = 0 Y=0 Z=L", ptX0Y0ZL());
ptXLY0ZL() = Point In BoundingBox {xMax-tolBB, yMin-tolBB, zMax-tolBB, xMax+tolBB, yMin+tolBB, zMax+tolBB};
Printf("Point X = L Y=0 Z=L", ptXLY0ZL());
ptX0YLZL() = Point In BoundingBox {xMin-tolBB, yMax-tolBB, zMax-tolBB, xMin+tolBB, yMax+tolBB, zMax+tolBB};
Printf("Point X = 0 Y=0 Z=L", ptX0Y0ZL());
ptXLYLZL() = Point In BoundingBox {xMax-tolBB, yMax-tolBB, zMax-tolBB, xMax+tolBB, yMax+tolBB, zMax+tolBB};
Printf("Point X = L Y=0 Z=L", ptXLY0ZL());

Physical Surface(100) = { surfX0() }; //x=0
Physical Surface(101) = { surfXL() }; //x=L
Physical Surface(110) = { surfY0()  }; //y=0
Physical Surface(111) = { surfYL() }; //y=L
Physical Surface(120) = { surfZ0() }; //z=0
Physical Surface(121) = { surfZL() }; //z=L

Physical Line(100) = { edZ0Y0() }; //y = 0, z = 0
Physical Line(101) = { edZ0YL() }; //y = L, z = 0
Physical Line(102) = { edZLY0() }; //y = 0, z = L
Physical Line(103) = { edZLYL() }; //y = L, z = L
Physical Line(110) = { edX0Z0() }; //x = 0, z = 0
Physical Line(111) = { edX0ZL() }; //x = 0, z = L
Physical Line(112) = { edXLZ0() }; //x = L, z = 0
Physical Line(113) = { edXLZL() }; //x = L, z = L
Physical Line(120) = { edX0Y0() }; //x = 0, y = 0
Physical Line(121) = { edX0YL() }; //x = 0, y = L
Physical Line(122) = { edXLY0() }; //x = L, y = 0
Physical Line(123) = { edXLYL() }; //x = L, y = L


Physical Point(1)= { ptX0Y0Z0() };
Physical Point(2)= { ptXLY0Z0() };
Physical Point(3)= { ptXLYLZ0() };
Physical Point(4)= { ptX0YLZ0() };
Physical Point(5)= { ptX0Y0ZL() };
Physical Point(6)= { ptXLY0ZL() };
Physical Point(7)= { ptXLYLZL() };
Physical Point(8)= { ptX0YLZL() };



Physical Volume(51) ={7};

//Physical Volume(52) ={1,2,3,4,5,6};
Physical Volume(52) ={1,2,3};
Physical Volume(53) ={4,5,6};


