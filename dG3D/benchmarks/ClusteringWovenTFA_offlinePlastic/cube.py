from gmshpy import *
from dG3Dpy import*

import os

################################################################################ OFFLINE ##########################################################################

rho   = 1000.e-9

propertiesMatrix_el = "propertiesEL_mat.i01"
propertiesYarnX_el = "propertiesEL_an0.i01"
propertiesYarnY_el = "propertiesEL_an1.i01"

# material law
lawnumMatrix = 11 # unique number of law
lawnumYarnX = 12 # unique number of law
lawnumYarnY = 13 # unique number of law

#  creation of law
b0 = 0.04875
a = 0.12/b0/3.14159265
b= b0*1.1

e1 = 0.08
e2 = 0.005

Lx = 4.*a + 2.*e1
Ly = 4.*a + 2.*e1
Lz = 4.0*b+ 2.0*e2

l = 30.0/Lx

Ori_x = Lx/2.
Ori_y = 0.0
Ori_z = 0.0

law1 = NonLocalDamageDG3DMaterialLaw_Stoch(lawnumMatrix,rho,propertiesMatrix_el, Ori_x, Ori_y, Ori_z, Lx, Ly, Lz, a, b, l,90.0,90.0,0.0,"Logistic")
law2 = NonLocalDamageDG3DMaterialLaw_Stoch(lawnumYarnX,rho,propertiesYarnX_el, Ori_x, Ori_y, Ori_z, Lx, Ly, Lz, a, b, l,90.0,90.0,0.0,"Logistic")
law3 = NonLocalDamageDG3DMaterialLaw_Stoch(lawnumYarnY,rho,propertiesYarnY_el, Ori_x, Ori_y, Ori_z, Lx, Ly, Lz, a, b, l,90.0,90.0,0.0,"Logistic")

# geometry
meshfile="WLogC2.msh" # name of mesh file

matrix = 51
yarnX = 52
yarnY = 53

myfield1 = dG3DDomain(1000,matrix,0,lawnumMatrix,0,3)
myfield2 = dG3DDomain(1000,yarnX,0,lawnumYarnX,0,3)
myfield3 = dG3DDomain(1000,yarnY,0,lawnumYarnY,0,3)

myfield1.stabilityParameters(1000)
myfield2.stabilityParameters(1000)
myfield3.stabilityParameters(1000)

# solver
sol = 2  # Gmm=0 (default) Taucs=1 PETsc=2
soltype =1 # StaticLinear=0 (default) StaticNonLinear=1
nstep = 1  # number of step (used only if soltype=1)
ftime =1  # Final time (used only if soltype=1)
tol=1.e-8  # relative tolerance for NR scheme (used only if soltype=1)
nstepArch=1 # Number of step between 2 archiving (used only if soltype=1)



# creation of Solver
mysolver = nonLinearMechSolver(1000)
mysolver.loadModel(meshfile)

mysolver.addDomain(myfield1)
mysolver.addMaterialLaw(law1)
mysolver.addDomain(myfield2)
mysolver.addMaterialLaw(law2)
mysolver.addDomain(myfield3)
mysolver.addMaterialLaw(law3)

mysolver.Scheme(soltype)
mysolver.Solver(sol)
mysolver.snlData(nstep,ftime,tol)
mysolver.stepBetweenArchiving(nstepArch)

# micro solver without reestimating stiffness matrix
system =3
mysolver.setSystemType(system)
mysolver.stiffnessModification(False)
mysolver.setMessageView(True)

#BC
bcType= "MIXED"
if bcType=="PBC":
	microBC = nonLinearPeriodicBC(1000,3) 
	microBC.setOrder(1)
	microBC.setBCPhysical(120,100,110,121,101,111)

	method = 5     # Periodic mesh = 0, Langrange interpolation = 1, Cubic spline interpolation =2,  FE linear= 3, FE Quad = 4
	degree = 3      # Order used for polynomial interpolation 
	addvertex = 0   # Polynomial interpolation by mesh vertex = 0, Polynomial interpolation by virtual vertex 
	microBC.setPeriodicBCOptions(method, degree, bool(addvertex))
elif bcType=="MIXED":
	microBC = nonLinearMixedBC(1000,3) 
	microBC.setOrder(1)
	microBC.setBCPhysical(120,100,110,121,101,111)
	
	microBC.setKinematicPhysical(120,2)
	microBC.setKinematicPhysical(121,2)
	microBC.setPeriodicPhysicals(121,120,0)
	microBC.setPeriodicPhysicals(121,120,1)
	
	microBC.setPeriodicPhysicals(101,100,0)
	microBC.setPeriodicPhysicals(101,100,1)
	microBC.setPeriodicPhysicals(101,100,2)
	
	microBC.setPeriodicPhysicals(111,110,0)
	microBC.setPeriodicPhysicals(111,110,1)
	microBC.setPeriodicPhysicals(111,110,2)
	
	method = 5     # Periodic mesh = 0, Langrange interpolation = 1, Cubic spline interpolation =2,  FE linear= 3, FE Quad = 4
	degree = 3      # Order used for polynomial interpolation 
	addvertex = 0   # Polynomial interpolation by mesh vertex = 0, Polynomial interpolation by virtual vertex 
	microBC.setPeriodicBCOptions(method, degree, bool(addvertex))

mysolver.addMicroBC(microBC)
mysolver.stressAveragingFlag(False) # set stress averaging ON- 0 , OFF-1
mysolver.tangentAveragingFlag(False) # set tangent averaging ON -0, OFF -1

cl = Clustering()
cl.solveStrainConcentration(mysolver,1)


#--------------------------PLASTICITY----------------------------------

# materials
propertiesMatrix = "properties_mat.i01"
propertiesYarnX = "properties_an0.i01"
propertiesYarnY = "properties_an1.i01"

law1plast = NonLocalDamageDG3DMaterialLaw_Stoch(lawnumMatrix,rho,propertiesMatrix, Ori_x, Ori_y, Ori_z, Lx, Ly, Lz, a, b, l,90.0,90.0,0.0,"Logistic")
law2plast = NonLocalDamageDG3DMaterialLaw_Stoch(lawnumYarnX,rho,propertiesYarnX, Ori_x, Ori_y, Ori_z, Lx, Ly, Lz, a, b, l,90.0,90.0,0.0,"Logistic")
law3plast = NonLocalDamageDG3DMaterialLaw_Stoch(lawnumYarnY,rho,propertiesYarnY, Ori_x, Ori_y, Ori_z, Lx, Ly, Lz, a, b, l,90.0,90.0,0.0,"Logistic")

law1plast.setUseBarF(True)
law2plast.setUseBarF(True)
law3plast.setUseBarF(True)

myfield1plast = dG3DDomain(1000,matrix,0,lawnumMatrix,0,3)
myfield2plast = dG3DDomain(1000,yarnX,0,lawnumYarnX,0,3)
myfield3plast = dG3DDomain(1000,yarnY,0,lawnumYarnY,0,3)

myfield1plast.stabilityParameters(1000)
myfield2plast.stabilityParameters(1000)
myfield3plast.stabilityParameters(1000)

# creation of Solver
nstep_plast = 100
tol_plast = 1.e-6
nstepArch_plast = 20
mysolverPlast = nonLinearMechSolver(1002)
mysolverPlast.loadModel(meshfile)

mysolverPlast.addDomain(myfield1plast)
mysolverPlast.addMaterialLaw(law1plast)

mysolverPlast.addDomain(myfield2plast)
mysolverPlast.addMaterialLaw(law2plast)
mysolverPlast.addDomain(myfield3plast)
mysolverPlast.addMaterialLaw(law3plast)

mysolverPlast.Scheme(soltype)
mysolverPlast.Solver(sol)
mysolverPlast.snlData(nstep_plast,ftime,tol_plast)
mysolverPlast.stepBetweenArchiving(nstepArch_plast)

# micro solver without reestimating stiffness matrix
system3 =3
mysolverPlast.setSystemType(system3)

#BC
bcType= "MIXED"
if bcType=="PBC":
	microBC3 = nonLinearPeriodicBC(1002,3) 
	microBC3.setOrder(1)
	microBC3.setBCPhysical(120,100,110,121,101,111)

	method = 5     # Periodic mesh = 0, Langrange interpolation = 1, Cubic spline interpolation =2,  FE linear= 3, FE Quad = 4
	degree = 3      # Order used for polynomial interpolation 
	addvertex = 0   # Polynomial interpolation by mesh vertex = 0, Polynomial interpolation by virtual vertex 
	microBC3.setPeriodicBCOptions(method, degree, bool(addvertex))
elif bcType=="MIXED":
	microBC3 = nonLinearMixedBC(1002,3) 
	microBC3.setOrder(1)
	microBC3.setBCPhysical(120,100,110,121,101,111)
	
	microBC3.setKinematicPhysical(120,2)
	microBC3.setKinematicPhysical(121,2)
	microBC3.setPeriodicPhysicals(121,120,0)
	microBC3.setPeriodicPhysicals(121,120,1)
	
	microBC3.setPeriodicPhysicals(101,100,0)
	microBC3.setPeriodicPhysicals(101,100,1)
	microBC3.setPeriodicPhysicals(101,100,2)
	
	microBC3.setPeriodicPhysicals(111,110,0)
	microBC3.setPeriodicPhysicals(111,110,1)
	microBC3.setPeriodicPhysicals(111,110,2)
	
	method = 5     # Periodic mesh = 0, Langrange interpolation = 1, Cubic spline interpolation =2,  FE linear= 3, FE Quad = 4
	degree = 3      # Order used for polynomial interpolation 
	addvertex = 0   # Polynomial interpolation by mesh vertex = 0, Polynomial interpolation by virtual vertex 
	microBC3.setPeriodicBCOptions(method, degree, bool(addvertex))


mysolverPlast.addMicroBC(microBC3)
mysolverPlast.stressAveragingFlag(True) # set stress averaging ON- 0 , OFF-1
mysolverPlast.tangentAveragingFlag(False) # set tangent averaging ON -0, OFF -1

mysolverPlast.internalPointBuildView("Eq. stress",IPField.SVM, 1, 1);
mysolverPlast.internalPointBuildView("Eq. plastic strain",IPField.PLASTICSTRAIN, 1, 1);

mysolverPlast.archivingAverageValue(IPField.SVM)
mysolverPlast.archivingAverageValue(IPField.PLASTICSTRAIN)
mysolverPlast.archivingAverageValue(IPField.SIG_XX)
mysolverPlast.archivingAverageValue(IPField.SIG_YY)
mysolverPlast.archivingAverageValue(IPField.SIG_ZZ)
mysolverPlast.archivingAverageValue(IPField.SIG_XY)
mysolverPlast.archivingAverageValue(IPField.SIG_YZ)
mysolverPlast.archivingAverageValue(IPField.SIG_XZ)
mysolverPlast.archivingAverageValue(IPField.STRAIN_XX)
mysolverPlast.archivingAverageValue(IPField.STRAIN_YY)
mysolverPlast.archivingAverageValue(IPField.STRAIN_ZZ)
mysolverPlast.archivingAverageValue(IPField.STRAIN_XY)
mysolverPlast.archivingAverageValue(IPField.STRAIN_YZ)
mysolverPlast.archivingAverageValue(IPField.STRAIN_XZ)

cl.solveStrainConcentrationInelastic(mysolverPlast)


#--------------------------Clustering----------------------------------

cl.loadStrainConcentrationDataFromFile("strainConcentrationData.csv")
cl.loadInelasticStrainConcentrationDataFromFile("inelasticStrainConcentrationData.csv")
cl.loadLocalOrientationDataFromFile("orientationData.csv")

cl.setNumberOfClustersForMaterial(lawnumMatrix,2)
cl.setNumberOfClustersForMaterial(lawnumYarnX,2)
cl.setNumberOfClustersForMaterial(lawnumYarnY,2)

ok = cl.computeClustersLocalOrientation(3) # min number of element in each cluster
cl.saveAllView()
cl.saveAllView(0)
cl.saveAllView(1)
cl.saveAllView(2)
cl.saveFieldToView(Clustering.ClusterIndex)
cl.saveFieldToView(Clustering.ClusterIndex,0)
cl.saveFieldToView(Clustering.ClusterIndex,1)
cl.saveFieldToView(Clustering.ClusterIndex,2)


#--------------------------Interaction----------------------------------
law3num = 3
clusterLaw = ClusterDG3DMaterialLaw(law3num,cl)
cl.computeInteractionTensor(mysolver,clusterLaw)


################################################################################ ONLINE ##########################################################################
# TFA law
lawTFA =  3
TFAmethod = 0     # 0: Incremental Tangent  2: Polarization Tangent
fluc_corr = 0     # 0: no correction   1: with fluctuation correction
incOri = 1
dim=3

rho   = 1000.e-9

b0 = 0.04875
a = 0.12/b0/3.14159265
e1 = 0.08
Lx = 4.*a + 2.*e1

#material parameters
propertiesMatrix = "propertiesOnline_mat.i01"
propertiesYarnX = "propertiesOnline_an0.i01"
propertiesYarnY = "propertiesOnline_an1.i01"

# material laws
lawnumMatrix = 11 # unique number of law
lawnumYarnX = 12 # unique number of law
lawnumYarnY = 13 # unique number of law
law1 = NonLocalDamageDG3DMaterialLaw(lawnumMatrix,rho,propertiesMatrix)
law2 = NonLocalDamageDG3DMaterialLaw(lawnumYarnX,rho,propertiesYarnX)
law3 = NonLocalDamageDG3DMaterialLaw(lawnumYarnY,rho,propertiesYarnY)
lawRed = TFADG3DMaterialLaw(lawTFA, rho)
lawRed.setTFAMethod(TFAmethod,dim,fluc_corr)   ## TFA method, dimension, correction: 0 without correction (default) or 1 with correction
if incOri == 1:
  lawRed.setClusterIncOrientation()
  
if TFAmethod==0:
  lawRed.loadClusterSummaryAndInteractionTensorsFromFiles('ClusterDataSummary_nbClusters_10.csv',
                                                        'InteractionTensorData_nbClusters_10.csv')
  if incOri == 1:
    lawRed.loadClusterFiberOrientationFromFile('ClusterMeanFiberOrientation_nbClusters_10.csv')
if TFAmethod==2:
  lawRed.loadClusterSummaryAndInteractionTensorsFromFiles('ClusterDataSummary_nbClusters_10.csv',
                                                        'InteractionTensorData_nbClusters_10.csv')
  lawRed.loadReferenceStiffnessFromFile('ElasticStiffnessHom.csv')
  lawRed.loadReferenceStiffnessIsoFromFile('ElasticStiffnessHomIso_muFix.csv')
  if incOri == 1:
    lawRed.loadClusterFiberOrientationFromFile('ClusterMeanFiberOrientation_nbClusters_10.csv')
  

# geometry
geofile="cube.geo"
meshfile="cube.msh" # name of mesh file

# creation of ElasticField
nfield = 10 # number of the field (physical number of surface)
fullDg = 0 # = CG, 1 = DG
space = 0 # function space (Lagrange=0)
numNlv = 0
myfield = dG3DDomain(1000,nfield,space,lawTFA,fullDg,dim,numNlv)
myfield.stabilityParameters(1000.)
#myfield.matrixByPerturbation(1,1,1,1e-8)

# solver
sol = 2  # Gmm=0 (default) Taucs=1 PETsc=2
soltype = 1 # StaticLinear=0 (default) StaticNonLinear=1
nstep = 50   # number of step (used only if soltype=1)
ftime =1.   # Final time (used only if soltype=1)
tol=1.e-6   # relative tolerance for NR scheme (used only if soltype=1)
nstepArch=5 # Number of step between 2 archiving (used only if soltype=1)

# creation of Solver
mysolver = nonLinearMechSolver(1000)
mysolver.createModel(geofile,meshfile,dim,1)
#mysolver.loadModel(meshfile)
mysolver.addDomain(myfield)
mysolver.addMaterialLaw(law1)
mysolver.addMaterialLaw(law2)
mysolver.addMaterialLaw(law3)
mysolver.addMaterialLaw(lawRed)

mysolver.Scheme(soltype)
mysolver.Solver(sol)
mysolver.snlData(nstep,ftime,tol)
mysolver.stepBetweenArchiving(nstepArch)


# BC
microBC = nonLinearPeriodicBC(1000,3)

method = 1	   # Periodic mesh = 0, Langrange interpolation = 1, Cubic spline interpolation =2,  FE linear= 3, FE Quad = 4
degree = 3	    # Order used for polynomial interpolation 
addvertex = 1   # Polynomial interpolation by mesh vertex = 0, Polynomial interpolation by virtual vertex 
microBC.setPeriodicBCOptions(method, degree, bool(addvertex)) 

microBC.setOrder(1)

#microBC.setBCPhysical(5678,4158,3487,2376,1265,1234)
microBC.setBCPhysical(1234,4158,1265,5678,2376,3487)

# Deformation gradient
mysolver.setMicroProblemIndentification(0, 0);
#stress averaging flag and averaging method 0- VOLUME, 1- SURFACE
mysolver.stressAveragingFlag(bool(1)) # set stress averaging ON- 0 , OFF-1
mysolver.setStressAveragingMethod(0) # 0 -volume 1- surface
#tangent averaging flag
mysolver.tangentAveragingFlag(bool(1)) # set tangent averaging ON -0, OFF -1
mysolver.setTangentAveragingMethod(2,1e-6) # 0- perturbation 1- condensation

##tension along x
mysolver.displacementBC("Face",4158,0,0.)
a=3.2941281137
eps_xx_max=0.02
#d1=eps_xx_max*a
d1=eps_xx_max*Lx
cyclicFunction1=cycleFunctionTime(0.,0., ftime,d1/2.)
mysolver.displacementBC("Face",2376,0,cyclicFunction1)
mysolver.displacementBC("Face",1234,2,0.)
mysolver.displacementBC("Face",1265,1,0.)

mysolver.archivingAverageValue(IPField.SIG_XX)
mysolver.archivingAverageValue(IPField.SIG_YY)
mysolver.archivingAverageValue(IPField.SIG_ZZ)
mysolver.archivingAverageValue(IPField.SIG_XY)
mysolver.archivingAverageValue(IPField.SIG_YZ)
mysolver.archivingAverageValue(IPField.SIG_XZ)

mysolver.archivingAverageValue(IPField.P_XX)
mysolver.archivingAverageValue(IPField.P_YY)
mysolver.archivingAverageValue(IPField.P_ZZ)
mysolver.archivingAverageValue(IPField.P_XY)
mysolver.archivingAverageValue(IPField.P_YZ)
mysolver.archivingAverageValue(IPField.P_XZ)

mysolver.archivingAverageValue(IPField.STRAIN_XX)
mysolver.archivingAverageValue(IPField.STRAIN_YY)
mysolver.archivingAverageValue(IPField.STRAIN_ZZ)
mysolver.archivingAverageValue(IPField.STRAIN_XY)
mysolver.archivingAverageValue(IPField.STRAIN_YZ)
mysolver.archivingAverageValue(IPField.STRAIN_XZ)
mysolver.archivingAverageValue(IPField.PLASTICSTRAIN)
mysolver.archivingAverageValue(IPField.DAMAGE)

mysolver.solve()

check = TestCheck()
import csv
data = csv.reader(open('E_1_GP_0_stress.csv'), delimiter=';')
strs = list(data)
check.equal(3.133567e+01,float(strs[-1][1]),1e-6)




