#coding-Utf-8-*-
from gmshpy import*
# from dG3DpyDebug import*
from dG3Dpy import*



# Script for plane plate problem

# Options
# ===================================================================================
Flag_Fracture = 1	# Activate cracks and fracture laws ? (1=yes, 0=no)
Solving_mode = 1	# QStatic (=1) vs. Implicit (=2)
LoadingMode = 0


# Material law creation
# ===================================================================================
# numbering of material law (has to be unique for each law !)
BulkLawNum1 = 1 
ClLawNum1 = 2
DamLawNum1 = 3
HardenLawNum1 = 4
InterfaceLawNum1 = 5
FracLawNum1 = 6
lawnum2 = 7
lawnum3 = 8

# material parameters - bulk law
rho = 2450.0	# kg/m3 :	density
E = 210.0e9		# Pa : 		Young modulus
nu = 0.3 		# - : 		poisson coefficient

cl = (0.015*1.0e-3)*(0.015*1.0e-3)#*1.10*1.10	# m2: non-local parameter (== NL-length^2) 




# # material parameters (2) - cohesive law
# sigmac = 0.028*E #0.09*E		# Pa		critical effective stress value
# Dc = 0.9		# - 		critical damage value
# damageCheck = bool(0)	# bool 		actived crack insertion by damage value
# h =  5.4*0.015*1.10*1e-3   	# m 		band thickness
# mu = -10000. 		# - 		friction coefficient
# fmin = 1.00 		# - 		spread min bond
# fmax =1.01		# - 		spread max bond
# betac = 0.87		# -
# Kp = 5.e12		# Pa/m2		penatly parameter (F = -K_p u_n^2)
# 			# ==O(E * beta_DG / mesh_size^2)
# GradFactor = 0.5	# -		multiply grad of jump
# useFc = bool(1) 	# -		activate normal comp. blocking of F
# delta0 = 1.0e-6

# material law creation
Cl_Law1 = IsotropicCLengthLaw(ClLawNum1,cl)
# Dam_Law1 = PowerDamageLaw(DamLawNum1, pi, pc, alpha, beta)
# #Dam_Law1 =ExponentialDamageLaw(DamLawNum1,1.e-4,0.1,500.)
# BulkLaw1 = NonLocalDamageIsotropicElasticityDG3DMaterialLaw(BulkLawNum1,rho,E,nu,Cl_Law1,Dam_Law1)

BulkLaw1 = NeoHookeanElasticPotential(BulkLawNum1, E, nu)
law1 = dG3DHyperelasticMaterialLaw(lawnum2,rho,BulkLaw1)
# BulkLaw1.setNonLocalLimitingMethod(0)
law1.setUseBarF(False)

# InterfaceLaw1 = CohesiveBand3DLaw(InterfaceLawNum1,sigmac,Dc,h,mu,fmin,fmax, betac,Kp,damageCheck,GradFactor,useFc,delta0)

#InterfaceLaw1 = NonLocalDamageLinearCohesive3DLaw(InterfaceLawNum1,21.4*1.0e3,sigmac,Dc,betac,fmin, fmax,1e7,damageCheck);

# FracLaw1 = FractureByCohesive3DLaw(FracLawNum1,BulkLawNum1,InterfaceLawNum1)

mylaw1 = GenericCrackPhaseFieldDG3DMaterialLaw(lawnum3, rho, Cl_Law1, 2700)
mylaw1.setMechanicalMaterialLaw(law1)

# Solver parameters
# ===================================================================================
sol = 2  		# Library for solving: Gmm=0 (default) Taucs=1 PETsc=2
if Solving_mode == 1:
	soltype = 1 		# StaticLinear=0 (default, StaticNonLinear=1, Explicit=2, 
				# Multi=3, Implicit=4, Eigen=5)
	nstep = 10000   		# Number of step
	ftime =1.0   		# Final time
	tol=1.e-5  		# Relative tolerance for NR scheme
	tolAbs = 1.e-6		# Absolute tolerance for NR scheme
	nstepArch= 4	# Number of step between 2 archiving
	nstepArchEnergy = 1	# Number of step between 2 energy computation and archiving
	nstepArchForce = 1	# Number of step between 2 force archiving
	MaxIter = 200		# Maximum number of iterations

elif Solving_mode == 2:
	# soltype = 3 		# StaticLinear=0 (default, StaticNonLinear=1, Explicit=2, 
				# Multi=3, Implicit=4, Eigen=5
	soltype = 4 #@Mohib changed to 4 from 2 
	nstep = 500   		# Number of step between implicit iterations
	nstepExpl = 2500	# Number of step between time step evaluation
	# ftime =(1.0e-4)*0.1   	# Final time
	ftime =1.0e-2   	# Final time
	tol=1.e-4   		# Relative tolerance for NR scheme
	tolAbs = 1.e-6		# Absolute tolerance for NR scheme
	nstepArch= 1 	# Number of step between 2 archiving
	nstepArchEnergy = 1 # Number of step between 2 energy computation and archiving
	nstepArchForce = 1	# Number of step between 2 force archiving
	MaxIter = 200		# Maximum number of iterations

StepIncrease = 2	# Number of successfull timestep before reincreasing it
StepReducFactor = 5.0 	# Timestep reduction factor
NumberReduction = 4	# Maximum number of timespep reduction: max reduction = pow(StepReducFactor,this)
fullDg = False        # O = CG, 1 = DG
dgnl = False		# DG for non-local variables inside a domain (only if fullDg)
eqRatio = 1.0 #1.0e6 @Mohib changed from a large number 	# Ratio between "elastic" and non-local equations 
space1 = 0 		# Function space (Lagrange=0)
beta1  = 30.0		# Penality parameter for DG


# Domain creation
## ===================================================================================
# Example : field1 = dG3DDomain(numDomain, numPhysVolume, space, LawNum, fullDgFlag, dim=3, numNonlocalVar=1)
# with:	- numDomain : domain tag
# 	- numPhysVolume : number of physical entities
# 	- space : function space (Lagrange=0)
#	- LawNum : number of material law 
# 	- fullDgFlag :  O = CG, 1 = DG
# 	- nonLocalEqRatio : ratio between elastic and non-local system
# 	- dim : dimension of displacment
# 	- numNonlocalVar : numbver of non-local variable

numPhysVolume1 = 100 		# Number of a physical volume of the model in .geo
numDomain1 = 1001 		# Number of the domain
# if Flag_Fracture == 0 :
# 	field1 = dG3DDomain(numDomain1,numPhysVolume1,space1,BulkLawNum1,fullDg,2,1)
# elif 
# Flag_Fracture == 1 :
# field1 = dG3DDomain(numDomain1,numPhysVolume1,space1,FracLawNum1,fullDg,2,1)
field1 = dG3DDomain(numDomain1,numPhysVolume1,space1,lawnum3,fullDg,2,1)
field1.stabilityParameters(beta1) 			# Adding stability parameters (for DG)
field1.setNonLocalStabilityParameters(beta1,dgnl) 		# Adding stability parameters (for DG)
field1.setNonLocalEqRatio(eqRatio)
# field1.gaussIntegration(0,-1,-1)
# field1.setBulkDamageBlockedMethod(1)
# field1.forceCohesiveInsertionAllIPs(bool(1),0.7)
field1.usePrimaryShapeFunction(3)



# Solver creation
# ===================================================================================
mysolver = nonLinearMechSolver(numDomain1) 		# Solver associated with numSolver1
# Geometry and mesh loading
# ===================================================================================
meshfile="SEN.msh" 			# name of mesh file
mysolver.loadModel(meshfile)		# add mesh
mysolver.addDomain(field1) 		# add domain
# mysolver.addMaterialLaw(BulkLaw1) 	# add material law
# mysolver.addMaterialLaw(InterfaceLaw1)
# mysolver.addMaterialLaw(FracLaw1)
mysolver.addMaterialLaw(mylaw1)

# solver parametrisation
if Solving_mode == 1 :
	mysolver.Scheme(soltype) 			# solver scheme
	mysolver.Solver(sol) 				# solver choice
	mysolver.snlData(nstep,ftime,tol,tolAbs) 	# solver parameters
	mysolver.snlManageTimeStep(MaxIter,StepIncrease,StepReducFactor,NumberReduction) #timestep
	mysolver.lineSearch(bool(0))		# lineSearch activation
	# solver archiving
	mysolver.stepBetweenArchiving(nstepArch) 	# archiving frequency for IPField
	# mysolver.energyComputation(nstepArchEnergy)	# archiving frequency for energy

elif Solving_mode == 2 :	
	mysolver.Scheme(soltype) 			# solver scheme
	mysolver.Solver(sol) 				# solver choice
	mysolver.snlData(nstep,ftime,tol,tolAbs) 	# solver parameters
	mysolver.snlManageTimeStep(MaxIter,StepIncrease,StepReducFactor,NumberReduction) #timestep
	mysolver.lineSearch(bool(0))		# LineSearch activation
	# mysolver.explicitSpectralRadius(ftime,0.5,0.0)	# Explicit integration parameters
	mysolver.implicitSpectralRadius(0.05)	# Implicit
	# mysolver.explicitTimeStepEvaluation(nstepExpl)
	# mysolver.addSystem(3,2)				# adding system (numb. of var., solv. scheme)
	# mysolver.addSystem(1,1)

	# solver archiving
	mysolver.stepBetweenArchiving(nstepArch) 	# archiving frequency
	mysolver.energyComputation(nstepArchEnergy)	# archiving frequency for energy
	stiffModif = BFGSStiffnessModificationMonitoring(20, False) # stiffness will be re-computed after 20 iterations in each step
	mysolver.stiffnessModification(stiffModif)



		
# Boundary conditions
# ===============================
# Example with constrained displacement : mysolver.displacementBC("type", numphys, comp, value)
# with:	- type : Node / Edge / Face / Volume
# 	- numphys : number of physical entities
# 	- comp : number of variables on which it's applied (0 = u_x; 1 = u_y; 2 = u_z)
# 	- value : value at the end of the simulation
# NB: if explicit solving: displacment is replaced by a speed !
# Example with applied forces : mysolver.forceBC("type", numphys, comp, value)

if LoadingMode == 0:
	#tot_disp = 0.01* 1.0*1.e-3
	mysolver.displacementBC("Edge",101,0,0.)		# face x = 0
	mysolver.displacementBC("Edge",101,1,0.)		# face x = 0
	
	mysolver.displacementBC("Face",100,2,0.)

	mysolver.forceBC("Edge",103,1, 1.0e8)
	mysolver.constraintBC("Edge",103,1)
	#mysolver.displacementBC("Edge",103,1,tot_disp/ftime)	# face x = L
	mysolver.displacementBC("Edge",103,0,0.)		# face x = L

	#mysolver.displacementBC("Edge",104,0,0.)		# face y = 0
	
	method = 2

	mysolver.pathFollowing(True,method)

	if method==0:
		mysolver.setPathFollowingIncrementAdaptation(True,4,0.3)
		mysolver.setPathFollowingControlType(0)
		mysolver.setPathFollowingCorrectionMethod(0)
		mysolver.setPathFollowingArcLengthStep(1e-1)
		mysolver.setBoundsOfPathFollowingArcLengthSteps(0,0.1);
	elif method==1:
		mysolver.setPathFollowingIncrementAdaptationLocal(False,5,7)
		mysolver.setPathFollowingLocalSteps(10.,1e-3) 
		mysolver.setPathFollowingLocalIncrementType(1); 
		mysolver.setPathFollowingSwitchCriterion(0.3)
		mysolver.setBoundsOfPathFollowingLocalSteps(1e-13,4.)
	elif method==2:
		mysolver.setPathFollowingIncrementAdaptation(True,4,0.3)
		mysolver.setPathFollowingControlType(0)
		mysolver.setPathFollowingCorrectionMethod(0)
		mysolver.setPathFollowingArcLengthStep(1e-1)
		mysolver.setBoundsOfPathFollowingArcLengthSteps(0,2.);


	# mysolver.pathFollowing(True,1)
	# mysolver.setPathFollowingIncrementAdaptation(False,4)
	# mysolver.setPathFollowingLocalSteps(9.0e-1,1.0e-3)
	# mysolver.setPathFollowingLocalIncrementType(1);

elif LoadingMode == 1:
	tot_disp = 0.02* 1.0*1.e-3
	mysolver.displacementBC("Edge",101,0,0.)		# face x = 0
	mysolver.displacementBC("Edge",101,1,0.)		# face x = 0

	mysolver.displacementBC("Face",100,2,0.)

	#mysolver.forceBC("Edge",103,0, 1.e8)
	#mysolver.constraintBC("Edge",103,0)
	mysolver.displacementBC("Edge",103,0,tot_disp/ftime)	# face x = L
	mysolver.displacementBC("Edge",103,1,0.)		# face x = L

	mysolver.displacementBC("Edge",102,1,0.)
	mysolver.displacementBC("Edge",104,1,0.)







# Variable storage
# ===============================
# Example : mysolver.internalPointBuildView("file_name", "comp", nbstep, ev)
# with:	- file_name : the name of variable storage file 
# 	- comp : saved values (IPField.SVM, IPField.SIG_XX, IPField::STRAIN_XZ,... 
#  	see ipField.h for possibilities)
# 	- nbstep : storage frequency ?
# 	- ev : value type (default : mean)
#
# Example : mysolver.archivingForceOnPhysicalGroup("type", numphys, comp, nstep)
# with:	- type : Node / Edge / Face / Volume
# 	- numphys : number of physical entities
# 	- comp : number of variables on which it's applied (0 = u_x; 1 = u_y; 2 = u_z)
# 	- nstep : step number between force archivingmysolver.internalPointBuildView("svm",IPField.SVM, 1, 1)
#
mysolver.internalPointBuildView("svm",IPField.SVM, 1, 1)
mysolver.internalPointBuildView("sig_xx",IPField.SIG_XX, 1, 1)
mysolver.internalPointBuildView("sig_yy",IPField.SIG_YY, 1, 1)
mysolver.internalPointBuildView("sig_zz",IPField.SIG_ZZ, 1, 1)
mysolver.internalPointBuildView("sig_xy",IPField.SIG_XY, 1, 1)
mysolver.internalPointBuildView("sig_yz",IPField.SIG_YZ, 1, 1)
mysolver.internalPointBuildView("sig_xz",IPField.SIG_XZ, 1, 1)
mysolver.internalPointBuildView("damage",IPField.DAMAGE,1,1)
# mysolver.internalPointBuildView("eff_eps",IPField.LOCAL_EQUIVALENT_STRAINS,1,1)
# mysolver.internalPointBuildView("NL_eps",IPField.NONLOCAL_EQUIVALENT_STRAINS,1,1)
# mysolver.internalPointBuildView("Blocked_element",IPField.DAMAGE_IS_BLOCKED,1,1)


mysolver.archivingForceOnPhysicalGroup("Edge", 103, 0, nstepArchForce)
mysolver.archivingForceOnPhysicalGroup("Edge", 103, 1, nstepArchForce)
mysolver.archivingForceOnPhysicalGroup("Edge", 101, 1, nstepArchForce)
mysolver.archivingForceOnPhysicalGroup("Edge", 101, 0, nstepArchForce)
mysolver.archivingNodeDisplacement(111,0, nstepArchForce)
mysolver.archivingNodeDisplacement(111,1, nstepArchForce)




# Solving
# ===========
mysolver.solve()





