mm = 1.;
L = 1.*mm;
R = 0.15*L;
lsca1 = 0.2*L;
lsca2 = 0.4*lsca1;

Point(1) = {0,0,0,lsca1};
Point(2) = {-R,0,0,lsca2};
Point(3) = {-0.5*L,0,0,lsca2};
Point(4) = {-0.5*L,-0.5*L,0,lsca1};
Point(5) = {0,-0.5*L,0,lsca2};
Point(6) = {0,-R,0,lsca2};


Line(1) = {2, 3};
Line(2) = {3, 4};
Line(3) = {4, 5};
Line(4) = {5, 6};
Circle(5) = {2, 1, 6};
Line Loop(6) = {2, 3, 4, -5, 1};
Plane Surface(7) = {6};
Symmetry {1, 0, 0, 0} {
  Duplicata { Surface{7}; }
}
Symmetry {0, 1, 0, 0} {
  Duplicata { Surface{7, 8}; }
}

/*
Translate {0.1, 0, 0} {
  Line{17, 24, 18, 5, 12, 4};
}

Translate {0, 0.1, 0} {
  Line{18, 24, 13, 12, 5, 1};
}
*/
//+
Extrude {L, 0, 0} {
  Line{21}; Line{9}; 
}
//+
Extrude {-L, 0, 0} {
  Line{15}; Line{2}; 
}

//+
Physical Surface(11) = {14, 20, 28, 32, 8, 7, 40, 36};
//+
Physical Line(1) = {39, 3, 10, 31};
//+
Physical Line(2) = {29, 25};
//+
Physical Line(3) = {27, 22, 16, 35};
//+
Physical Line(4) = {33, 37};
//+
Physical Point(1) = {52};
//+
Physical Point(2) = {48};
//+
Physical Point(3) = {46};
//+
Physical Point(4) = {50};
