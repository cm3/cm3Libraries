#coding-Utf-8-*-
from gmshpy import *
from dG3Dpy import*

#script to launch PBC problem with a python script

#DEFINE MICRO PROBLEM

# micro-material law
lawnum1 = 11 # unique number of law


rho   = 7850e-9
young = 28.9e3
nu    = 0.3 
sy0   = 99.
h     = young/20.

harden = LinearExponentialJ2IsotropicHardening(1, sy0, h, 0., 10.)
cl     = IsotropicCLengthLaw(1, 1e-2)
damlaw = SimpleSaturateDamageLaw(1, 50.,1.,0.,1.)
law1   = NonLocalDamageJ2HyperDG3DMaterialLaw(lawnum1,rho,young,nu,harden,cl,damlaw)

# creation of part Domain
nfield = 11 # number of the field (physical number of entity)
myfield1 = dDG3DDomain(1000,nfield,0,lawnum1,0,2,1)
#myfield1.setNonLocalStabilityParameters(beta1,True)
myfield1.setNonLocalEqRatio(1.e3)


macromeshfile="full.msh" # name of mesh file

# solver
sol = 2  # Gmm=0 (default) Taucs=1 PETsc=2
soltype =1 # StaticLinear=0 (default) StaticNonLinear=1
nstep = 100  # number of step (used only if soltype=1)
ftime =1.   # Final time (used only if soltype=1)
tol=1.e-6  # relative tolerance for NR scheme (used only if soltype=1)
nstepArch=1 # Number of step between 2 archiving (used only if soltype=1)

# creation of  macro part Domain

# creation of Solver
mysolver = nonLinearMechSolver(1000)
mysolver.loadModel(macromeshfile)
mysolver.addDomain(myfield1)
mysolver.addMaterialLaw(law1)
mysolver.Scheme(soltype)
mysolver.Solver(sol)
mysolver.snlData(nstep,ftime,tol)

# boundary condition
mysolver.displacementBC("Face",11,2,0.0)
mysolver.displacementBC("Edge",1,1,0.0)
mysolver.displacementBC("Edge",3,1,0.0)
mysolver.displacementBC("Edge",4,0,0.0)

umax = 0.015
fct = PiecewiseLinearFunction()
fct.put(0.,0.)
fct.put(0.4,0.9*umax)
fct.put(0.6,-0.2*umax)
fct.put(1.,umax)

mysolver.displacementBC("Edge",2,0,fct)


# archivage
mysolver.internalPointBuildView("Green-Lagrange_xx",IPField.STRAIN_XX, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_yy",IPField.STRAIN_YY, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_zz",IPField.STRAIN_ZZ, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_xy",IPField.STRAIN_XY, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_yz",IPField.STRAIN_YZ, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_xz",IPField.STRAIN_XZ, 1, 1);
mysolver.internalPointBuildView("sig_xx",IPField.SIG_XX, 1, 1);
mysolver.internalPointBuildView("sig_yy",IPField.SIG_YY, 1, 1);
mysolver.internalPointBuildView("sig_zz",IPField.SIG_ZZ, 1, 1);
mysolver.internalPointBuildView("sig_xy",IPField.SIG_XY, 1, 1);
mysolver.internalPointBuildView("sig_yz",IPField.SIG_YZ, 1, 1);
mysolver.internalPointBuildView("sig_xz",IPField.SIG_XZ, 1, 1);
mysolver.internalPointBuildView("sig_VM",IPField.SVM, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange equivalent strain",IPField.GL_EQUIVALENT_STRAIN, 1, 1);
mysolver.internalPointBuildView("Equivalent plastic strain",IPField.PLASTICSTRAIN, 1, 1);
mysolver.internalPointBuildView("Damage",IPField.DAMAGE, 1, 1);

mysolver.archivingForceOnPhysicalGroup("Edge",4,0)
mysolver.archivingNodeDisplacement(2,0)

# solve
mysolver.solve()

check = TestCheck()
check.equal(-1.477192e+01,mysolver.getArchivedForceOnPhysicalGroup("Edge", 4, 0),1.e-4)

