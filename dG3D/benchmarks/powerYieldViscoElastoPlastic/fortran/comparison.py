import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

plt.figure()

u = pd.read_csv("NodalDisplacementPhysical5Num5comp2.csv",sep=";",header=None)
f = pd.read_csv("force1comp2.csv",sep=";",header=None)
plt.plot(u.values[:,1],f.values[:,1],"-",label="CUR -fortran")

u = pd.read_csv("../NodalDisplacementPhysical5Num5comp2.csv",sep=";",header=None)
f = pd.read_csv("../force1comp2.csv",sep=";",header=None)
plt.plot(u.values[:,1],f.values[:,1],"--",label="ref")


plt.xlabel("disp")
plt.ylabel("force")

plt.legend()

plt.show()

