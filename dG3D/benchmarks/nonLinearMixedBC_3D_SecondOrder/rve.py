#coding-Utf-8-*-

from gmshpy import *
from dG3Dpy import*

#script to launch PBC problem with a python script

# material law
lawnum = 11 # unique number of law


E = 70E3
nu = 0.3
K = E/3./(1.-2.*nu)	# Bulk mudulus
mu =E/2./(1.+nu)	  # Shear mudulus
rho = 2.7 # Bulk mass
sy0 = 507.e100
h = 200

# creation of material law

law1 = dG3DLinearElasticMaterialLaw(11,rho,E,nu)

# geometry
#geofile="rve.geo" # name of mesh file
meshfile="rve.msh" # name of mesh file


# creation of part Domain
nfield = 11 # number of the field (physical number of entity)
dim =3
myfield1 = dG3DDomain(10,nfield,0,lawnum,0,dim)

# solver
sol = 2  # Gmm=0 (default) Taucs=1 PETsc=2
soltype =1 # StaticLinear=0 (default) StaticNonLinear=1
nstep = 1  # number of step (used only if soltype=1)
ftime =1.   # Final time (used only if soltype=1)
tol=1.e-6  # relative tolerance for NR scheme (used only if soltype=1)
nstepArch=1 # Number of step between 2 archiving (used only if soltype=1)
system = 1 # Displacement elimination =0 Multiplier elimination = 1 Displacement+ multiplier = 2
control = 0 # load control = 0 arc length control euler = 1

# creation of Solver
mysolver = nonLinearMechSolver(1000)
#mysolver.createModel(geofile,meshfile,3,2)
mysolver.loadModel(meshfile)
mysolver.addDomain(myfield1)
mysolver.addMaterialLaw(law1)
mysolver.Scheme(soltype)
mysolver.Solver(sol)
mysolver.snlData(nstep,ftime,tol)
mysolver.setSystemType(system)
mysolver.setControlType(control)
mysolver.stiffnessModification(bool(1))
mysolver.iterativeProcedure(bool(1))
mysolver.setMessageView(bool(1))


#boundary condition
microBC = nonLinearMixedBC(10,3)
microBC.setOrder(2)
microBC.setBCPhysical(1,3,5,2,4,6) #mixed boundary condition

microBC.setKinematicPhysical(1,0)
#microBC.setKinematicPhysical(1,1)
#microBC.setKinematicPhysical(1,2)

microBC.setKinematicPhysical(2,0)
#microBC.setKinematicPhysical(2,1)
#microBC.setKinematicPhysical(2,2)

#microBC.setKinematicPhysical(3,0)
microBC.setKinematicPhysical(3,1)
#microBC.setKinematicPhysical(3,2)

#microBC.setKinematicPhysical(4,0)
microBC.setKinematicPhysical(4,1)
#microBC.setKinematicPhysical(4,2)

#microBC.setKinematicPhysical(5,0)
#microBC.setKinematicPhysical(5,1)
microBC.setKinematicPhysical(5,2)

#microBC.setKinematicPhysical(6,0)
#microBC.setKinematicPhysical(6,1)
microBC.setKinematicPhysical(6,2)


#microBC.setStaticPhysical(1,0)
microBC.setStaticPhysical(1,1)
microBC.setAdditionalSecondOrderStaticPhysical(1,1,1)
microBC.setAdditionalSecondOrderStaticPhysical(1,1,2)
microBC.setStaticPhysical(1,2)
microBC.setAdditionalSecondOrderStaticPhysical(1,2,1)
microBC.setAdditionalSecondOrderStaticPhysical(1,2,2)

#mysolver.addStaticPhysical(2,0)
microBC.setStaticPhysical(2,1)
microBC.setAdditionalSecondOrderStaticPhysical(2,1,1)
microBC.setAdditionalSecondOrderStaticPhysical(2,1,2)
microBC.setStaticPhysical(2,2)
microBC.setAdditionalSecondOrderStaticPhysical(2,2,1)
microBC.setAdditionalSecondOrderStaticPhysical(2,2,2)

microBC.setStaticPhysical(3,0)
microBC.setAdditionalSecondOrderStaticPhysical(3,0,0)
microBC.setAdditionalSecondOrderStaticPhysical(3,0,2)
#microBC.setStaticPhysical(3,1)
microBC.setStaticPhysical(3,2)
microBC.setAdditionalSecondOrderStaticPhysical(3,2,0)
microBC.setAdditionalSecondOrderStaticPhysical(3,2,2)

microBC.setStaticPhysical(4,0)
microBC.setAdditionalSecondOrderStaticPhysical(4,0,0)
microBC.setAdditionalSecondOrderStaticPhysical(4,0,2)
#microBC.setStaticPhysical(4,1)
microBC.setStaticPhysical(4,2)
microBC.setAdditionalSecondOrderStaticPhysical(4,2,0)
microBC.setAdditionalSecondOrderStaticPhysical(4,2,2)

microBC.setStaticPhysical(5,0)
microBC.setAdditionalSecondOrderStaticPhysical(5,0,0)
microBC.setAdditionalSecondOrderStaticPhysical(5,0,1)
microBC.setStaticPhysical(5,1)
microBC.setAdditionalSecondOrderStaticPhysical(5,1,0)
microBC.setAdditionalSecondOrderStaticPhysical(5,1,1)
#microBC.setStaticPhysical(5,2)

microBC.setStaticPhysical(6,0)
microBC.setAdditionalSecondOrderStaticPhysical(6,0,0)
microBC.setAdditionalSecondOrderStaticPhysical(6,0,1)
microBC.setStaticPhysical(6,1)
microBC.setAdditionalSecondOrderStaticPhysical(6,1,0)
microBC.setAdditionalSecondOrderStaticPhysical(6,1,1)
#microBC.setStaticPhysical(6,2)

 # Deformation gradient
microBC.setDeformationGradient(1.01,0.0,0.0,0,1,0,0,0,1)
microBC.setGradientOfDeformationGradient(1,0,2,0.01) 
microBC.setGradientOfDeformationGradient(1,2,0,0.01) 

mysolver.addMicroBC(microBC)


#stress averaging flag and averaging method 0- VOLUME, 1- SURFACE
mysolver.stressAveragingFlag(bool(1)) # set stress averaging ON- 0 , OFF-1
mysolver.setStressAveragingMethod(0) # 0 -volume 1- surface
#tangent averaging fla
mysolver.tangentAveragingFlag(bool(1)) # set tangent averaging ON -0, OFF -1
mysolver.setTangentAveragingMethod(2,1e-6) # 0- perturbation 1- condensation


# build view
mysolver.internalPointBuildView("Green-Lagrange_xx",IPField.STRAIN_XX, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_yy",IPField.STRAIN_YY, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_zz",IPField.STRAIN_ZZ, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_xy",IPField.STRAIN_XY, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_yz",IPField.STRAIN_YZ, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_xz",IPField.STRAIN_XZ, 1, 1);
mysolver.internalPointBuildView("sig_xx",IPField.SIG_XX, 1, 1);
mysolver.internalPointBuildView("sig_yy",IPField.SIG_YY, 1, 1);
mysolver.internalPointBuildView("sig_zz",IPField.SIG_ZZ, 1, 1);
mysolver.internalPointBuildView("sig_xy",IPField.SIG_XY, 1, 1);
mysolver.internalPointBuildView("sig_yz",IPField.SIG_YZ, 1, 1);
mysolver.internalPointBuildView("sig_xz",IPField.SIG_XZ, 1, 1);
mysolver.internalPointBuildView("sig_VM",IPField.SVM, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange equivalent strain",IPField.GL_EQUIVALENT_STRAIN, 1, 1);
mysolver.internalPointBuildView("Equivalent plastic strain",IPField.PLASTICSTRAIN, 1, 1);

# solve
mysolver.solve()

# test check
check = TestCheck()
check.equal(6.688552e+02,mysolver.getHomogenizedStress(0,0),1.e-4)
check.equal(6.688552e+04,mysolver.getHomogenizedTangent(0,0,0,0),1.e-4)
check.equal(2.539224e+04,mysolver.getHomogenizedTangent(1,1,0,0),1.e-4)
check.equal(2.024147e+04,mysolver.getHomogenizedTangent(0,1,0,1),1.e-4)

check.equal(3.558001e+03,mysolver.getHomogenizedSecondTangent(0,0,0,0,0,0),1.e-4)
check.equal(1.676463e+03,mysolver.getHomogenizedSecondTangent(1,0,0,1,0,0),1.e-4)


