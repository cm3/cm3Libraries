SetFactory("OpenCASCADE");

Mesh.Optimize = 1;

cm = 1e-2; // Unit

pp  = "Input/10Geometric dimensions/0";

close_menu = 1;
colorpp = "Ivory";

// Smp
RintSMP = 0.95*cm;
RextSMP = 1*cm;
H = 2*cm;

DefineConstant[
RintSMP  = {0.95*cm, Name StrCat[pp, "4Sample inner radius [m]"], Highlight Str[colorpp]}
RextSMP  = {1*cm, Name StrCat[pp, "5Sample outer radius [m]"], Highlight Str[colorpp]}
H     = {2*cm, Name StrCat[pp, "6Sample height [m]"], Highlight Str[colorpp]}
];

ECORE = 1000;
SKINECORE = 11110;
REFCORE = 11111;
REFCOREBOTTOM = 11115;
REFCOREPOINT = 11112;
REFCOREOUT = 11113;
REFCOREIN = 11114;

// Smp 
vE()+=newv; Cylinder(newv) = {0,-H/2,0, 0,H,0, RintSMP};
vE()+=newv; Cylinder(newv) = {0,-H/2,0, 0,H,0, RextSMP};

vCoreE()+=newv; BooleanDifference(newv) = { Volume{vE(1)}; Delete; }{ Volume{vE(0)}; Delete; };

lc2  = RextSMP/4; // smp
Characteristic Length { PointsOf{ Volume{vCoreE()}; } } = lc2;

Recursive Color SteelBlue {Volume{vCoreE()};}

Physical Volume(ECORE) = vCoreE();
bnd_vCoreE() = CombinedBoundary{Volume{vCoreE()};};
Physical Surface(SKINECORE) = bnd_vCoreE();

Physical Surface(REFCORE)= bnd_vCoreE(1);
Physical Surface(REFCOREBOTTOM)= bnd_vCoreE(2);

Physical Surface(REFCOREOUT)= bnd_vCoreE(0);
Physical Surface(REFCOREIN)= bnd_vCoreE(3);

pts() = PointsOf {Surface { bnd_vCoreE(1) }; };
Physical Point(REFCOREPOINT)= pts(0);
