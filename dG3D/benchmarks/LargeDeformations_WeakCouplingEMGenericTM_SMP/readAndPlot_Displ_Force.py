import pandas as pd
import os
import numpy as np
import matplotlib.pyplot as plt

Forcefilename = "/force11111comp1.csv"
Displacementfilename = "/NodalDisplacementPhysical11112Num1comp1.csv"
allDir = os.listdir("./TMresults/")

for d in allDir[:]:
    temppath = "./TMresults/"+d
    if os.path.isdir(temppath) == False:
        allDir.remove(d)
    if os.path.isfile(d) == True:
        allDir.remove(d)

allDir.sort()

print(allDir)

force = "./TMresults/"+allDir[0]+Forcefilename
displ = "./TMresults/"+allDir[0]+Displacementfilename

newdataframeForce = pd.read_csv(force,sep=";",header=None).values
print(newdataframeForce.shape)
print()

newdataframeDispl = pd.read_csv(displ,sep=";",header=None).values
print(newdataframeDispl.shape)
print()

for d in allDir[1:]:
    force = "./TMresults/"+d+Forcefilename
    tempframeForce = pd.read_csv(force,sep=";",header=None).values
    print(tempframeForce.shape)
    print()
    newdataframeForce = np.concatenate((newdataframeForce, tempframeForce), axis=0)

    displ = "./TMresults/"+d+Displacementfilename
    tempframeDispl = pd.read_csv(displ,sep=";",header=None).values
    print(tempframeDispl.shape)
    print()
    newdataframeDispl = np.concatenate((newdataframeDispl, tempframeDispl), axis=0)
    
print(newdataframeForce.shape)
print(newdataframeDispl.shape)

# convert array into dataframe
DF = pd.DataFrame({'displ':newdataframeDispl[:,1],'force':newdataframeForce[:,1]})

# save the dataframe as a csv file
DF.to_csv("./TMresults/DisplvsForce.csv",sep=";",header=False,index=False)

plt.figure()
plt.title('Nodal displacement vs Nodal force (Y component) on top face in SMP', size=12)
plt.xlabel('Nodal displacement [m]', size=14)
plt.ylabel('Nodal force [N]', size=14)
plt.plot(newdataframeDispl[:,1],newdataframeForce[:,1])
plt.savefig('./TMresults/DisplvsForce.png', format='png')
plt.show()
    
