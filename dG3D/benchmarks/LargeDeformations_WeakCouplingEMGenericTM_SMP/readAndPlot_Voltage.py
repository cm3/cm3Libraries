import pandas as pd
import os
import numpy as np
import matplotlib.pyplot as plt

filename = "/IPVolume1000val_VOLTAGEMean.csv"
allDir = os.listdir("./EMresults/")

for d in allDir[:]:
    temppath = "./EMresults/"+d
    if os.path.isdir(temppath) == False:
        allDir.remove(d)
    if os.path.isfile(d) == True:
        allDir.remove(d)

allDir.sort()

print(allDir)

f = "./EMresults/"+allDir[0]+filename
newdataframe = pd.read_csv(f,sep=";",header=None).values
print(newdataframe.shape)
print()

for d in allDir[1:]:
    f = "./EMresults/"+d+filename
    tempframe = pd.read_csv(f,sep=";",header=None).values
    print(tempframe.shape)
    print()
    newdataframe = np.concatenate((newdataframe, tempframe), axis=0)
    
print(newdataframe.shape)

# convert array into dataframe
DF = pd.DataFrame(newdataframe)

# save the dataframe as a csv file
DF.to_csv("./EMresults/VOLTAGEMean.csv",sep=";",header=False,index=False)

plt.figure()
plt.title('Voltage Mean vs Time', size=14)
plt.xlabel('Time', size=14)
plt.ylabel('Voltage Mean', size=14)
plt.plot(newdataframe[:,0],newdataframe[:,1])
plt.savefig('./EMresults/VoltageMean.png', format='png')
plt.show()
    
