import pandas as pd
import os
import numpy as np
import matplotlib.pyplot as plt

filename = "/force11111comp1.csv"
allDir = os.listdir("./TMresults/")

for d in allDir[:]:
    temppath = "./TMresults/"+d
    if os.path.isdir(temppath) == False:
        allDir.remove(d)
    if os.path.isfile(d) == True:
        allDir.remove(d)

allDir.sort()

print(allDir)

f = "./TMresults/"+allDir[0]+filename
newdataframe = pd.read_csv(f,sep=";",header=None).values
print(newdataframe.shape)
print()

for d in allDir[1:]:
    f = "./TMresults/"+d+filename
    tempframe = pd.read_csv(f,sep=";",header=None).values
    print(tempframe.shape)
    print()
    newdataframe = np.concatenate((newdataframe, tempframe), axis=0)
    
print(newdataframe.shape)

# convert array into dataframe
DF = pd.DataFrame(newdataframe)

# save the dataframe as a csv file
DF.to_csv("./TMresults/ForcevsTime.csv",sep=";",header=False,index=False)

plt.figure()
plt.title('Nodal force on top face in SMP vs Time', size=14)
plt.xlabel('Time', size=14)
plt.ylabel('Nodal force', size=14)
plt.plot(newdataframe[:,0],newdataframe[:,1])
plt.savefig('./TMresults/ForcevsTime.png', format='png')
plt.show()
    
