#coding-Utf-8-*-
from gmshpy import *
from dG3Dpy import*

import tensorflow as tf
import numpy as np
import time

def relu_leaky_one(x):
  if x >0:
    return x
  else:
    return 0.01*x
def diff_relu_leaky_one(x):
  if x >0:
    return 1
  else:
    return 0.01

def relu_leaky(x):
  y = x.copy()
  ss = x.shape
  for i in range(0,ss[0]):
    for j in range(0,ss[1]):
      y[i][j] = relu_leaky_one(x[i][j])
  return y

def diff_relu_leaky(x):
  y = x.copy()
  ss = x.shape
  for i in range(0,ss[0]):
    for j in range(0,ss[1]):
      y[i][j] = diff_relu_leaky_one(x[i][j])
  return y

def linear(x):
  return x.copy()
  
def diff_linear(x):
  y = x.copy()
  ss = x.shape
  for i in range(0,ss[0]):
    for j in range(0,ss[1]):
      y[i][j] = 1
  return y

def activation_Val(x,funcType):
  if funcType == 'leakyRelu':
    return relu_leaky(x)
  elif funcType == 'linear':
    return linear(x)
  else:
    print('function type is not defind')
    
def activation_Diff(x,funcType):
  if funcType == 'leakyRelu':
    return diff_relu_leaky(x)
  elif funcType == 'linear':
    return diff_linear(x)
  else:
    print('function type is not defind')

def get_result(W, x0, activationFunctionType, diff):
  nlayers = len(activationFunctionType)
  idex = 0
  x = x0.copy()
  y = x0.copy()
  all_x = list()
  all_y = list()
  all_x.append(x)
  all_y.append(y)
  all_W = list()
  for i in range(nlayers):
    all_W.append(W[idex])
    x = y.dot(W[idex])+W[idex+1]
    y = activation_Val(x,activationFunctionType[i])
    idex = idex +2
    all_x.append(x)
    all_y.append(y)
  
  if diff:  
    all_diff = list()
    for i in range(nlayers-1,-1,-1):
      dydx = activation_Diff(all_x[i+1],activationFunctionType[i])
      dydxMat = np.diag(dydx[0])
      dydyprev = dydxMat.dot(all_W[i].transpose())
      all_diff.append(dydyprev)
      
    dydx =all_diff[0]
    for i in range(1,nlayers):
      dydxTemp = dydx.dot(all_diff[i])
      dydx = dydxTemp
      
    return (y, dydx)
  else:
    return (y, np.zeros(6,6))
  

E = 70e3
nu = 0.3
K = E/(3.*(1.-2.*nu))
mu = E/(2.*(1.+nu))
lb = K-2.*mu/3.

A = np.zeros((6,6))
A[0][0] = 2.*mu+lb
A[1][1] = 2.*mu+lb
A[2][2] = 2.*mu+lb
A[0][1] = lb
A[1][0] = lb
A[0][2] = lb
A[2][0] = lb
A[1][2] = lb
A[2][1] = lb
A[3][3] = 2.*mu
A[4][4] = 2.*mu
A[5][5] = 2.*mu
B = np.zeros(6)
numInput = 6
numOutput = 6
m = tf.keras.Sequential()
m.add(tf.keras.layers.Dense(6, input_shape=(numInput,),activation='linear'))
m.get_layer(index=0).set_weights([A, B])

W = m.get_weights()

def testFunc_ann(x,x0,q0,S0,diff):
  #print x, other
  xinput = np.array([x])
  (y, dydx) =   get_result(W,xinput,['linear'],diff)
  q1 = np.random.rand(10)
  return (y[0].tolist(),q1.tolist(), dydx.tolist())

# python function is embeded in a c++ function  
fct1 = MIMOFunctionPython(testFunc_ann)

# test

fct2 = LinearMIMOFunction(6,6)
fct2.setValue(0,0,2.*mu+lb)
fct2.setValue(1,1,2.*mu+lb)
fct2.setValue(2,2,2.*mu+lb)
fct2.setValue(0,1,lb)
fct2.setValue(1,0,lb)
fct2.setValue(0,2,lb)
fct2.setValue(2,0,lb)
fct2.setValue(1,2,lb)
fct2.setValue(2,1,lb)
fct2.setValue(3,3,2.*mu)
fct2.setValue(4,4,2.*mu)
fct2.setValue(5,5,2.*mu)


layer1 = LinearDenseLayer(6,6)
for i in range(0,6):
  for j in range(0,6):
    s = float(W[0][i,j])
    layer1.setWeights(i,j,s)
    
ann = ArtificialNN()
ann.addLayer(layer1)

ann.print_infos()
fct3 = ANNMIMOFunction(ann)
fct3.setNumberOfStrainVariables(6)  
fct3.setNumberOfStressVariables(6)
for i in range(6):
  fct3.setStrainLocation(i,i)
  fct3.setStressLocation(i,i)


#DEFINE MICRO PROBLEM
lnum1= 11
numInterVar = 10
rho = 1e-9
macromat1 = ANNBasedDG3DMaterialLaw(lnum1,rho,numInterVar,fct3)


macromeshfile="macro.msh" # name of mesh file
macrogeofile="macro.geo"
# solver
sol = 2  # Gmm=0 (default) Taucs=1 PETsc=2
soltype =1 # StaticLinear=0 (default) StaticNonLinear=1
nstep = 10  # number of step (used only if soltype=1)
ftime =1.   # Final time (used only if soltype=1)
tol=1.e-8  # relative tolerance for NR scheme (used only if soltype=1)
nstepArch=1 # Number of step between 2 archiving (used only if soltype=1)


# creation of  macro part Domain

dim =3
beta1 = 50;
fullDG = False;

averageStrainBased = True

# non DG domain
nfield1 = 11
macrodomain1 = dG3DDomain(10,11,0,lnum1,fullDG,3)
macrodomain1.stabilityParameters(beta1)
#macrodomain1.matrixByPerturbation(1,1,1,1e-8)

# creation of Solver
mysolver = nonLinearMechSolver(1000)
mysolver.loadModel(macromeshfile)
mysolver.addDomain(macrodomain1)
mysolver.addMaterialLaw(macromat1)

mysolver.Scheme(soltype)
mysolver.Solver(sol)
mysolver.snlData(nstep,ftime,tol)

# boundary condition
mysolver.displacementBC("Face",45,0,0.0)
mysolver.displacementBC("Face",45,1,0.0)
mysolver.displacementBC("Face",45,2,0.0)

mysolver.displacementBC("Face",46,1,10.)
#mysolver.displacementBC("Face",46,0,0.)

# archivage
mysolver.internalPointBuildView("Green-Lagrange_xx",IPField.STRAIN_XX, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_yy",IPField.STRAIN_YY, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_zz",IPField.STRAIN_ZZ, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_xy",IPField.STRAIN_XY, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_yz",IPField.STRAIN_YZ, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_xz",IPField.STRAIN_XZ, 1, 1);
mysolver.internalPointBuildView("sig_xx",IPField.SIG_XX, 1, 1);
mysolver.internalPointBuildView("sig_yy",IPField.SIG_YY, 1, 1);
mysolver.internalPointBuildView("sig_zz",IPField.SIG_ZZ, 1, 1);
mysolver.internalPointBuildView("sig_xy",IPField.SIG_XY, 1, 1);
mysolver.internalPointBuildView("sig_yz",IPField.SIG_YZ, 1, 1);
mysolver.internalPointBuildView("sig_xz",IPField.SIG_XZ, 1, 1);
mysolver.internalPointBuildView("sig_VM",IPField.SVM, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange equivalent strain",IPField.GL_EQUIVALENT_STRAIN, 1, 1);
mysolver.internalPointBuildView("Equivalent plastic strain",IPField.PLASTICSTRAIN, 1, 1);
mysolver.internalPointBuildView("Damage",IPField.DAMAGE,1,1);
mysolver.internalPointBuildView("USER1",IPField.USER1,1,1);
mysolver.internalPointBuildView("USER2",IPField.USER2,1,1);
mysolver.internalPointBuildView("USER3",IPField.USER3,1,1);
mysolver.internalPointBuildView("USER4",IPField.USER4,1,1);
mysolver.internalPointBuildView("USER9",IPField.USER9,1,1);

mysolver.archivingForceOnPhysicalGroup("Face",45,0)
mysolver.archivingForceOnPhysicalGroup("Face",45,1)


# solve
mysolver.solve()

check = TestCheck()
check.equal(-7.696534e+05,mysolver.getArchivedForceOnPhysicalGroup("Face", 45, 1),1.e-4)

