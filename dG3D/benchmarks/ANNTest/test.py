#coding-Utf-8-*-
from gmshpy import *
from dG3Dpy import*
from sklearn.preprocessing import MinMaxScaler

data = [[-1, 2], [-0.5, 6], [0, 10], [1, 18]]
scaler = MinMaxScaler()
scaler.fit(data)
A = scaler.transform([[0,1],[0,2]])

class MyScale:
  def __init__(self, scaler):
    self.scaler= scaler

  def transform(self,A, a = False):
    B = self.scaler.transform(A)
    if not(a):
      return B.tolist(), None
    else:
      return B.tolist(),B.tolist()
    
  def inverse_transform(self,A, a= False):
    B = self.scaler.inverse_transform(A)
    if not(a):
      return B.tolist(), None
    else:
      return B.tolist(),B.tolist()
    
m = MyScale(scaler)
B  = m.transform(A)
print(B)

pyS =  ScalerPython(m)
a = fullMatrixDouble(2,2)
a.set(0,0,A[0,0])
a.set(0,1,A[0,1])
a.set(1,0,A[1,0])
a.set(1,1,A[1,1])

b = fullMatrixDouble()
DbDa = fullMatrixDouble()
pyS.transform(a,b,True,DbDa)
b._print()
DbDa._print("DBDA")

c = fullMatrixDouble()
DcDb = fullMatrixDouble()
pyS.inverse_transform(b,c, True, DcDb)
c._print()
DcDb._print("DcDb")

