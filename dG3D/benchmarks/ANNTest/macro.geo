mm = 1.;
L1 = 200*mm;
L2 = 200*mm;
L3 = 200*mm;
L4 = 200*mm;

lsca1 = L1/5.;
lsca2 = 0.2*lsca1;

Point(1) = {0,0,0,lsca1};
Point(2) = {L1,0,0,lsca1};
Point(3) = {L1,L2+0.7*L4,0,lsca2};
Point(4) = {L1,L2+L4,0,lsca2*2};
Point(5) = {-L3,L2+L4,0,lsca1};
Point(6) = {-L3,L2,0,lsca1};
Point(7) = {0,L2,0,lsca2};
//+
Line(1) = {1, 2};
//+
Line(2) = {2, 3};
//+
Line(3) = {3, 4};
//+
Line(4) = {4, 5};
//+
Line(5) = {5, 6};
//+
Line(6) = {6, 7};
//+
Line(7) = {7, 1};
//+
Line Loop(1) = {5, 6, 7, 1, 2, 3, 4};
//+
Plane Surface(1) = {1};
//+
Extrude {0, 0, 50*mm} {
  Surface{1}; Layers{2}; Recombine;
}
//+
Physical Volume(11) = {1};
//+
Physical Surface(45) = {31};
//+
Physical Surface(46) = {19};
//+
Physical Surface(47) = {43};
