mm = 1.;
R = 1*mm;
L = 15*mm;
H = 25*mm;
t = 1*mm;

sl1 = 0.1*mm;
sl2 = 0.4*sl1;
sl3 = 0.8*sl1;

p1 = newp; Point(p1)={0,0,0,sl1};
p2 = newp; Point(p2)={R,0,0,sl2};
p3 = newp; Point(p3)={H,0,0,sl2};
p4 = newp; Point(p4)={H,L,0,sl1};
p5 = newp; Point(p5)={0,L,0,sl1};
p6 = newp; Point(p6)={0,R,0,sl3};

//+
Line(1) = {2, 3};
//+
Line(2) = {3, 4};
//+
Line(3) = {4, 5};
//+
Line(4) = {5, 6};
//+
Circle(5) = {6, 1, 2};
//+
Line Loop(6) = {3, 4, 5, 1, 2};
//+
Plane Surface(7) = {6};
//+
Symmetry {0, 1, 0, 0} {
  Duplicata { Surface{7}; }
}

Transfinite Line {9, 3} = 10 Using Progression 1;
//+
Transfinite Line {1} = 40 Using Progression 1.05;

//+
Transfinite Line {5, 11} = 7 Using Progression 1;
//+
Transfinite Line {-4, 2, 13, -10} = 15 Using Progression 1.15;
Transfinite Line {2, 13} = 10 Using Progression 1.01;
//+
Extrude {0, 0, t} {
  Surface{7, 8}; Layers{1}; Recombine;
}
//+
Physical Volume(11) = {1};
//+
Physical Volume(12) = {2};
//+
Physical Surface(1) = {50};
//+
Physical Surface(3) = {23};
//+
Physical Point(5) = {14};
//+
Physical Surface(4) = {27, 54};
