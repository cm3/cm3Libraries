#coding-Utf-8-*-

from gmshpy import *
from dG3Dpy import*

from math import*

#script to launch PBC problem with a python script

# material law
lawnumMatrix = 51 # unique number of law

E = 2.45E9 #MPa
nu = 0.39
K = E/3./(1.-2.*nu)	# Bulk mudulus
mu =E/2./(1.+nu)	  # Shear mudulus
rho = 1.4e3   # Bulk mass ton/mm3
sy0c = 48.e6
fact = 0.8;
sy0t = sy0c*fact;
hardenc = ExponentialJ2IsotropicHardening(1,sy0c,55.e6,76.)
hardent = ExponentialJ2IsotropicHardening(2,sy0t,55.e6*fact,76.)
hardenk = PolynomialKinematicHardening(3,4)
hardenk.setCoefficients(2,170.E6)
hardenk.setCoefficients(3,-440.E6)
hardenk.setCoefficients(4,1100.E6)

damlaw1 = SimpleSaturateDamageLaw(1, 45, 0.2,0.,2.)
cl1     = IsotropicCLengthLaw(1, 2.e-6)

damlaw2 = PowerBrittleDamagelaw(2,0,5e-2,1.,0.3)
cl2     = IsotropicCLengthLaw(2, 16.e-6)

lawMatrix   = NonLocalDamagePowerYieldHyperDG3DMaterialLawWithFailure(lawnumMatrix,rho,E,nu,hardenc,hardent,hardenk,cl1,cl2,damlaw1,damlaw2,1e-6,False,1e-8)
lawMatrix.setYieldPowerFactor(3.5)
lawMatrix.setNonAssociatedFlow(True)
lawMatrix.nonAssociatedFlowRuleFactor(0.3)
lawMatrix.setStrainOrder(3)
lawMatrix.setViscoelasticMethod(0)
N = 4;
lawMatrix.setViscoElasticNumberOfElement(N)
lawMatrix.setViscoElasticData(1,380e6,7202.)
lawMatrix.setViscoElasticData(2,190e6,71.6)
lawMatrix.setViscoElasticData(3,95e6,0.73)
lawMatrix.setViscoElasticData(4,48e6,0.073)
# failure
#Xc = PowerFailureLaw(1,253.e6,38.e6,0.14)
#Xt = PowerFailureLaw(2,92.e6,18.e6,0.14)
Xc = ConstantFailureLaw(1,262.5e6)
Xt = ConstantFailureLaw(2,100e6)

failureCr = PowerFailureCriterion(1,Xt,Xc,2.)
lawMatrix.setFailureCriterion(failureCr)

eta = constantViscosityLaw(1,3e10)
lawMatrix.setViscosityEffect(eta,0.21)

# geometry
meshfile="model.msh" # name of mesh file


# creation of part Domain
dim =3
fullDg = 0 #O = CG, 1 = DG
space1 = 0 # function space (Lagrange=0)
beta1  = 100
# creation of part Domain
nfieldMatrix = 11 # number of the field (physical number of surface)
myfieldMatrix = dG3DDomain(1000,nfieldMatrix,space1,lawnumMatrix,fullDg,3,2)
#myfieldMatrix.matrixByPerturbation(1,1,1,1e-6)
myfieldMatrix.stabilityParameters(beta1)
myfieldMatrix.setNonLocalStabilityParameters(beta1,True)
myfieldMatrix.setNonLocalEqRatio(1.e6)


# solver
sol = 2  # Gmm=0 (default) Taucs=1 PETsc=2
soltype =1 # StaticLinear=0 (default) StaticNonLinear=1
nstep = 150  # number of step (used only if soltype=1)
ftime =1000.   # Final time (used only if soltype=1)
tol=1.e-6 # relative tolerance for NR scheme (used only if soltype=1)
nstepArch=1 # Number of step between 2 archiving (used only if soltype=1)

# creation of Solver
mysolver = nonLinearMechSolver(1000)
mysolver.loadModel(meshfile)
mysolver.addDomain(myfieldMatrix)
mysolver.addMaterialLaw(lawMatrix)


mysolver.Scheme(soltype)
mysolver.Solver(sol)
mysolver.snlData(nstep,ftime,tol)
mysolver.stepBetweenArchiving(nstepArch)

mysolver.displacementBC("Volume",11,2,0.)
mysolver.displacementBC("Face",111,1,0.)
mysolver.displacementBC("Face",111,0,0.)

mysolver.displacementBC("Face",112,0,0.)
mysolver.displacementBC("Face",112,1,2.e-6)




# build view
mysolver.internalPointBuildView("Green-Lagrange_xx",IPField.STRAIN_XX, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_yy",IPField.STRAIN_YY, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_zz",IPField.STRAIN_ZZ, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_xy",IPField.STRAIN_XY, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_yz",IPField.STRAIN_YZ, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_xz",IPField.STRAIN_XZ, 1, 1);
mysolver.internalPointBuildView("sig_xx",IPField.SIG_XX, 1, 1);
mysolver.internalPointBuildView("sig_yy",IPField.SIG_YY, 1, 1);
mysolver.internalPointBuildView("sig_zz",IPField.SIG_ZZ, 1, 1);
mysolver.internalPointBuildView("sig_xy",IPField.SIG_XY, 1, 1);
mysolver.internalPointBuildView("sig_yz",IPField.SIG_YZ, 1, 1);
mysolver.internalPointBuildView("sig_xz",IPField.SIG_XZ, 1, 1);
mysolver.internalPointBuildView("sig_VM",IPField.SVM, 1, 1);
mysolver.internalPointBuildView("Equivalent plastic strain",IPField.PLASTICSTRAIN, 1, 1);
mysolver.internalPointBuildView("pression",IPField.PRESSION,1,1)
mysolver.internalPointBuildView("plastic possio ratio",IPField.PLASTIC_POISSON_RATIO,1,1)
mysolver.internalPointBuildView("Damage",IPField.DAMAGE,1,1)
mysolver.internalPointBuildView("failure onset",IPField.FAILURE_ONSET,1,1)
mysolver.internalPointBuildView("nonlocal Plastic strain",IPField.NONLOCAL_PLASTICSTRAIN,1,1)

mysolver.internalPointBuildView("Saturation Damage",IPField.DAMAGE_0,1,1)
mysolver.internalPointBuildView("Failure Damage",IPField.DAMAGE_1,1,1) 
mysolver.internalPointBuildView("Nonlocal Failure plasticity",IPField.NONLOCAL_FAILURE_PLASTICSTRAIN,1,1) 

mysolver.internalPointBuildView("LOCAL 0",IPField.LOCAL_0,1,1) 
mysolver.internalPointBuildView("LOCAL 1",IPField.LOCAL_1,1,1) 
mysolver.internalPointBuildView("NONLOCAL 0",IPField.NONLOCAL_0,1,1) 
mysolver.internalPointBuildView("NONLOCAL 1",IPField.NONLOCAL_1,1,1) 


#archiving
mysolver.archivingForceOnPhysicalGroup('Face',111,0)
mysolver.archivingForceOnPhysicalGroup('Face',111,1)
mysolver.archivingForceOnPhysicalGroup('Face',112,0)
mysolver.archivingForceOnPhysicalGroup('Face',112,1)
mysolver.archivingNodeDisplacement(113,1)

# solve
mysolver.solve()

check = TestCheck()
check.equal(-4.881520,mysolver.getArchivedForceOnPhysicalGroup("Face", 111, 1),1.e-4)

