mm = 1e-3;

L = 10*mm;
H = 20*mm;
Z = 0.3*mm;

H1 = 0.6*H;
R1 = 2*mm;
H2 = 0.4*H;
R2 = 2*mm;

lc1 =0.08*L;
lc2 = 0.7*lc1;

Point(1) = {0,0,0,lc1};
Point(2) = {L,0,0,lc1};
Point(3) = {L,H1-R1,0,lc2};
Point(4) = {L-R1,H1,0,lc2};
Point(5) = {L,H1+R1,0,lc2};
Point(6) = {L,H,0,lc1};
Point(7) = {0,H,0,lc1};
Point(8) = {0,H2+R2,0,lc2};
Point(9) = {R2,H2,0,lc2};
Point(10) = {0,H2-R2,0,lc2};
Point(11) = {L,H1,0};
Point(12) = {0,H2,0};
Line(1) = {1, 2};
Line(2) = {2, 3};
Line(3) = {5, 6};
Line(4) = {6, 7};
Line(5) = {7, 8};
Line(6) = {10, 1};
Circle(7) = {5, 11, 4};
Circle(8) = {4, 11, 3};
Circle(9) = {8, 12, 9};
Circle(10) = {9, 12, 10};
Line Loop(11) = {5, 9, 10, 6, 1, 2, -8, -7, 3, 4};
Plane Surface(12) = {11};

//Recombine Surface {12};
//+
Extrude {0, 0, 1*mm} {
  Surface{12}; Layers{1}; Recombine;
}
//+
Physical Volume(11) = {1};

//+
Physical Surface(111) = {43};
//+
Physical Surface(112) = {63};
//+
Physical Point(113) = {7};
