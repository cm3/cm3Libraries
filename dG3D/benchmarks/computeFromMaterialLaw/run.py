#coding-Utf-8-*-

from gmshpy import *
from dG3Dpy import*

from math import*

def ToList(A):
  n = A.size()
  a = np.zeros(n)
  for i in range(n):
    a[i] = A.get(i)
  return a.tolist()

def writeToFile(f, l):
  lsize = l.size()
  for idex in range(lsize-1):
    f.write("%s;"%str(l.get(idex)))
  f.write("%s\n"%str(l.get(lsize-1)))
  
def writeHeaderToFile(f, l, okTimeStep=False, okTime=False):
  lsize = l.size()
  lastEle = 0
  for idex in range(lsize-1):
    if (l.getState(idex)):
      f.write("%s;"%IPField.ToString(l.get(idex)))
    else:
      f.write("%s_prev;"%IPField.ToString(l.get(idex)))
    lastEle = lastEle+1
  if l.getState(lastEle):
    f.write("%s"%IPField.ToString(l.get(lastEle)))
  else:
    f.write("%s_prev"%IPField.ToString(l.get(lastEle)))
  lastEle = lastEle+1
  if (okTimeStep):
    f.write(";TimeStep");
  if (okTime):
    f.write(";Time");
  f.write("\n");
    
   

lawnum1 = 1 # unique number of law
rho   = 7850
young = 28.9e9
nu    = 0.3 
sy0   = 150.e6
h     = young*1e-1

harden = LinearExponentialJ2IsotropicHardening(1, sy0, h, 0, 10.)
law1   = J2LinearDG3DMaterialLaw(lawnum1,rho,young,nu,harden)


comMat = computeMaterialLaw(law1)

import numpy as np
N = 100
x = np.sin(np.linspace(0.001,10,N))
y = np.zeros(N)
pl = np.zeros(N)

inputComp = VecInt()
inputComp.set(0,IPField.GL_XX,1)
inputComp.set(1,IPField.GL_XY,1)
inputComp.set(2,IPField.GL_XZ,1)
inputComp.set(3,IPField.GL_YY,1)
inputComp.set(4,IPField.GL_YZ,1)
inputComp.set(5,IPField.GL_ZZ,1)
inputComp.set(6,IPField.GL_XX,0)
inputComp.set(7,IPField.GL_XY,0)
inputComp.set(8,IPField.GL_XZ,0)
inputComp.set(9,IPField.GL_YY,0)
inputComp.set(10,IPField.GL_YZ,0)
inputComp.set(11,IPField.GL_ZZ,0)
inputComp.set(12,IPField.FP_XX,0)
inputComp.set(13,IPField.FP_YX,0)
inputComp.set(14,IPField.FP_ZX,0)
inputComp.set(15,IPField.FP_XY,0)
inputComp.set(16,IPField.FP_YY,0)
inputComp.set(17,IPField.FP_ZY,0)
inputComp.set(18,IPField.FP_XZ,0)
inputComp.set(19,IPField.FP_YZ,0)
inputComp.set(20,IPField.FP_ZZ,0)
inputComp.set(21,IPField.PLASTICSTRAIN,0)
inputComp.printInfo()

outputComp = VecInt()
outputComp.set(0,IPField.S_XX,1)
outputComp.set(1,IPField.S_XY,1)
outputComp.set(2,IPField.S_XZ,1)
outputComp.set(3,IPField.S_YY,1)
outputComp.set(4,IPField.S_YZ,1)
outputComp.set(5,IPField.S_ZZ,1)
outputComp.set(6,IPField.FP_XX,1)
outputComp.set(7,IPField.FP_YX,1)
outputComp.set(8,IPField.FP_ZX,1)
outputComp.set(9,IPField.FP_XY,1)
outputComp.set(10,IPField.FP_YY,1)
outputComp.set(11,IPField.FP_ZY,1)
outputComp.set(12,IPField.FP_XZ,1)
outputComp.set(13,IPField.FP_YZ,1)
outputComp.set(14,IPField.FP_ZZ,1)
outputComp.set(15,IPField.PLASTICSTRAIN,1)
outputComp.printInfo()


withTime=True
withTimeStep=True

inputData = VecDouble()
outputData = VecDouble()

fi = open("inputData.csv","w")
fi.write("%s\n"%inputComp.getHeader(withTimeStep,withTime))
fo = open("outputData.csv","w")
fo.write("%s\n"%outputComp.getHeader(withTimeStep,withTime))

for i in range(0,N):
  comMat.setTimeStep(i+1.,1.)
  comMat.setDeformationGradient(0,0,1+x[i])
  comMat.computeStressState()
  comMat.getValue(inputComp,inputData,withTimeStep,withTime)
  comMat.getValue(outputComp,outputData,withTimeStep,withTime)
  
  writeToFile(fo,outputData)
  writeToFile(fi,inputData)
  
  #
  y[i] = outputData.get(0)
  pl[i] = outputData.get(6)

fo.close()
fi.close()

import matplotlib.pyplot as plt
plt.plot(x,pl,'r+-')
plt.xlabel('pl (-)')
plt.ylabel('P (Pa)')
plt.savefig('test_result.pdf')
plt.show()

