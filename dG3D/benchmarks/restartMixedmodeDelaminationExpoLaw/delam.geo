mm = 1e-3;
L = 2*mm;
H = 0.5*mm;
t = 0.1*mm;

lsca = t;

Point(1) = {0,0,0,lsca};
Point(2) = {H,0,0,lsca};
Line(1) = {1, 2};
Extrude {0, L, 0} {
  Line{1}; Layers{25}; 
}
Extrude {0, H, 0} {
  Line{2}; Layers{5};
}
Extrude {0, 0, -1.5*t} {
  Surface{5}; Layers{1};
}
Extrude {0, 0, t} {
  Surface{5, 9}; Layers{1};
}
Physical Volume(51) = {2, 3};
Physical Volume(52) = {1};
Physical Surface(53) = {40, 18};
Physical Surface(56) = {70};
Physical Point(58) = {36};
