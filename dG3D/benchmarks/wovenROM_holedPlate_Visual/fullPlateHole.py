# coding-Utf-8-*-
from gmshpy import *
# from dG3DpyDebug import*
from dG3Dpy import *

# script to launch composite problem with a python script

# geometry -------------------------------------------------------------------------------------------------------------
dimDomain = 3  # dimension of domain
order = 2
geofile = "fullPlateHole.geo"
meshfile = "fullPlateHole.msh"

# key material properties ----------------------------------------------------------------------------------------------
ROMMod = "VM"
if ROMMod == "VM":
    propROM = "propVM.i01"
elif ROMMod == "VLM":
    propROM = "propVLM.i01"
nbNonLocalVar = 1
pert = False
nstep = 3000  # number of step (used only if soltype=1) # 1000
DmaxMtx = 0.975  # 0.95 / 0.975 / 0.99
DmaxInc = 0.975  # 0.95 / 0.975 / 0.99
if ROMMod == "VM":
    nstepArch = 100
elif ROMMod == "VLM":
    nstepArch = 200
nstepArchForce = 1
nstepRes = 5000

# unique physical numbers ----------------------------------------------------------------------------------------------
# elasto-plastic field with non-local damage of unit cell with ROMs
numDomainROM = 51
# unique law tags ------------------------------------------------------------------------------------------------------
# elasto-plastic law with non-local damage of unit cell with ROMs
tagLawROM = 1
# unique field tags ----------------------------------------------------------------------------------------------------
# elasto-plastic field with non-local damage of unit cell with ROMs
tagDomain = 10
# unique solver tag ----------------------------------------------------------------------------------------------------
tagSolver = 1000

# parameters for material law ------------------------------------------------------------------------------------------
rhoROM = 1000.  # Bulk mass for the slave (density in kg/m3)
if order == 1:
    useBarF = True  # useBarF to avoid locking when order=1
elif order == 2:
    useBarF = False
if pert:
    pertFactor = 1e-8  # law with tangent by perturbation
# parameters for domains and interdomains ------------------------------------------------------------------------------
space1 = 0  # function space (Lagrange=0)
fulldg = False  # False = CG, True = DG inside a domain
if order == 1:
    beta1 = 100.  # stability/penalty parameter for displacement when DG
elif order == 2:
    beta1 = 60.
# parameters for solver ------------------------------------------------------------------------------------------------
sol = 2  # Gmm=0 (default) Taucs=1 PETsc=2
soltype = 1  # StaticLinear=0 (default); StaticNonLinear=1; Explicit=2; Multi=3; Implicit=4; Eigen=5
ftime = 1.  # final time (used only if soltype=1)
tol = 1.e-5  # relative tolerance for NR scheme (used only if soltype=1) # 1.e-6
tolAbs = 1.e-9  # absolu tolerance for NR scheme (used only if soltype=1) # 1.e-12
# manage time steps & archivage steps
maxIter = 10
stepIncNb = 3
stepRedFac = 2
maxRedNb = 30

# creation of law
lawROM = NonLocalDamageDG3DMaterialLaw(tagLawROM, rhoROM, propROM)
lawROM.setUseBarF(useBarF)
lawROM.setInitialDmax_Inc(DmaxInc)  # fibres in MT for VT or LAM_2PLY
lawROM.setInitialDmax_Mtx(DmaxMtx)  # matrix in MT or EP for VT or LAM_2PLY
if pert:
    pertLawROM = dG3DMaterialLawWithTangentByPerturbation(lawROM, pertFactor)

# creation of field
domainROM = dG3DDomain(tagDomain, numDomainROM, space1, tagLawROM, fulldg, dimDomain, nbNonLocalVar)
domainROM.stabilityParameters(beta1)

# creation of solver
mysolver = nonLinearMechSolver(tagSolver)
mysolver.createModel(geofile, meshfile, dimDomain, order)
# mysolver.loadModel(meshfile)

mysolver.Solver(sol)
mysolver.Scheme(soltype)
mysolver.snlData(nstep, ftime, tol, tolAbs)  # tolAbs
mysolver.snlManageTimeStep(maxIter, stepIncNb, stepRedFac, maxRedNb)
mysolver.createRestartBySteps(nstepRes)

if pert:
    mysolver.addMaterialLaw(pertLawROM)
elif not pert:
    mysolver.addMaterialLaw(lawROM)
mysolver.addDomain(domainROM)

# BC
# tension along x
d3 = 1.5e-3
d1 = 0.2 * d3
d2 = 0.6 * d3
ft1 = 0.2 * ftime
ft2 = 0.6 * ftime
cyclicFunction1 = cycleFunctionTime(0., 0., ft1, d1, ft2, d2, ftime, d3)

mysolver.displacementBC("Face", 102, 0, cyclicFunction1)
mysolver.displacementBC("Face", 101, 0, 0.)
mysolver.displacementBC("Face", 121, 2, 0.)
mysolver.displacementBC("Node", 21, 1, 0.)
mysolver.displacementBC("Node", 23, 1, 0.)

# archiving
mysolver.stepBetweenArchiving(nstepArch)

mysolver.internalPointBuildView("eps_xx", IPField.STRAIN_XX, nstepArch, 1)
mysolver.internalPointBuildView("eps_yy", IPField.STRAIN_YY, nstepArch, 1)
mysolver.internalPointBuildView("eps_zz", IPField.STRAIN_ZZ, nstepArch, 1)
mysolver.internalPointBuildView("eps_xy", IPField.STRAIN_XY, nstepArch, 1)
mysolver.internalPointBuildView("eps_yz", IPField.STRAIN_YZ, nstepArch, 1)
mysolver.internalPointBuildView("eps_xz", IPField.STRAIN_XZ, nstepArch, 1)
mysolver.internalPointBuildView("sig_xx", IPField.SIG_XX, nstepArch, 1)
mysolver.internalPointBuildView("sig_yy", IPField.SIG_YY, nstepArch, 1)
mysolver.internalPointBuildView("sig_zz", IPField.SIG_ZZ, nstepArch, 1)
mysolver.internalPointBuildView("sig_xy", IPField.SIG_XY, nstepArch, 1)
mysolver.internalPointBuildView("sig_yz", IPField.SIG_YZ, nstepArch, 1)
mysolver.internalPointBuildView("sig_xz", IPField.SIG_XZ, nstepArch, 1)

if ROMMod == "VM":
    # damage for each VT phase
    mysolver.internalPointBuildView("dam_vtm01", IPField.DAMAGE_VTM01, nstepArch, 1)
    # damage for each material phase
    mysolver.internalPointBuildView("dam_vtmt1_mtx", IPField.DAMAGE_VTMT1_MTX, nstepArch, 1)
    mysolver.internalPointBuildView("dam_vtmt1_inc", IPField.DAMAGE_VTMT1_INC, nstepArch, 1)
    mysolver.internalPointBuildView("dam_vtmt2_mtx", IPField.DAMAGE_VTMT2_MTX, nstepArch, 1)
    mysolver.internalPointBuildView("dam_vtmt2_inc", IPField.DAMAGE_VTMT2_INC, nstepArch, 1)
    mysolver.internalPointBuildView("dam_vtmt3_mtx", IPField.DAMAGE_VTMT3_MTX, nstepArch, 1)
    mysolver.internalPointBuildView("dam_vtmt3_inc", IPField.DAMAGE_VTMT3_INC, nstepArch, 1)
    mysolver.internalPointBuildView("dam_vtmt4_mtx", IPField.DAMAGE_VTMT4_MTX, nstepArch, 1)
    mysolver.internalPointBuildView("dam_vtmt4_inc", IPField.DAMAGE_VTMT4_INC, nstepArch, 1)
    mysolver.internalPointBuildView("dam_vtmt5_mtx", IPField.DAMAGE_VTMT5_MTX, nstepArch, 1)
    mysolver.internalPointBuildView("dam_vtmt5_inc", IPField.DAMAGE_VTMT5_INC, nstepArch, 1)
    mysolver.internalPointBuildView("dam_vtmt6_mtx", IPField.DAMAGE_VTMT6_MTX, nstepArch, 1)
    mysolver.internalPointBuildView("dam_vtmt6_inc", IPField.DAMAGE_VTMT6_INC, nstepArch, 1)
    # strain for each VT phase
    mysolver.internalPointBuildView("eps_xx_vtm01", IPField.STRAIN_XX_VTM01, nstepArch, 1)
    mysolver.internalPointBuildView("eps_xx_vtmt1", IPField.STRAIN_XX_VTMT1, nstepArch, 1)
    mysolver.internalPointBuildView("eps_xx_vtmt2", IPField.STRAIN_XX_VTMT2, nstepArch, 1)
    mysolver.internalPointBuildView("eps_xx_vtmt3", IPField.STRAIN_XX_VTMT3, nstepArch, 1)
    mysolver.internalPointBuildView("eps_xx_vtmt4", IPField.STRAIN_XX_VTMT4, nstepArch, 1)
    mysolver.internalPointBuildView("eps_xx_vtmt5", IPField.STRAIN_XX_VTMT5, nstepArch, 1)
    mysolver.internalPointBuildView("eps_xx_vtmt6", IPField.STRAIN_XX_VTMT6, nstepArch, 1)
    # stress for each VT phase
    mysolver.internalPointBuildView("sig_xx_vtm01", IPField.STRESS_XX_VTM01, nstepArch, 1)
    mysolver.internalPointBuildView("sig_xx_vtmt1", IPField.STRESS_XX_VTMT1, nstepArch, 1)
    mysolver.internalPointBuildView("sig_xx_vtmt2", IPField.STRESS_XX_VTMT2, nstepArch, 1)
    mysolver.internalPointBuildView("sig_xx_vtmt3", IPField.STRESS_XX_VTMT3, nstepArch, 1)
    mysolver.internalPointBuildView("sig_xx_vtmt4", IPField.STRESS_XX_VTMT4, nstepArch, 1)
    mysolver.internalPointBuildView("sig_xx_vtmt5", IPField.STRESS_XX_VTMT5, nstepArch, 1)
    mysolver.internalPointBuildView("sig_xx_vtmt6", IPField.STRESS_XX_VTMT6, nstepArch, 1)
    # strain for each material phase
    mysolver.internalPointBuildView("eps_xx_vtmt1_mtx", IPField.STRAIN_XX_VTMT1_MTX, nstepArch, 1)
    mysolver.internalPointBuildView("eps_xx_vtmt1_inc", IPField.STRAIN_XX_VTMT1_INC, nstepArch, 1)
    mysolver.internalPointBuildView("eps_xx_vtmt2_mtx", IPField.STRAIN_XX_VTMT2_MTX, nstepArch, 1)
    mysolver.internalPointBuildView("eps_xx_vtmt2_inc", IPField.STRAIN_XX_VTMT2_INC, nstepArch, 1)
    mysolver.internalPointBuildView("eps_xx_vtmt3_mtx", IPField.STRAIN_XX_VTMT3_MTX, nstepArch, 1)
    mysolver.internalPointBuildView("eps_xx_vtmt3_inc", IPField.STRAIN_XX_VTMT3_INC, nstepArch, 1)
    mysolver.internalPointBuildView("eps_xx_vtmt4_mtx", IPField.STRAIN_XX_VTMT4_MTX, nstepArch, 1)
    mysolver.internalPointBuildView("eps_xx_vtmt4_inc", IPField.STRAIN_XX_VTMT4_INC, nstepArch, 1)
    mysolver.internalPointBuildView("eps_xx_vtmt5_mtx", IPField.STRAIN_XX_VTMT5_MTX, nstepArch, 1)
    mysolver.internalPointBuildView("eps_xx_vtmt5_inc", IPField.STRAIN_XX_VTMT5_INC, nstepArch, 1)
    mysolver.internalPointBuildView("eps_xx_vtmt6_mtx", IPField.STRAIN_XX_VTMT6_MTX, nstepArch, 1)
    mysolver.internalPointBuildView("eps_xx_vtmt6_inc", IPField.STRAIN_XX_VTMT6_INC, nstepArch, 1)
    # stress for each material phase
    mysolver.internalPointBuildView("sig_xx_vtmt1_mtx", IPField.STRESS_XX_VTMT1_MTX, nstepArch, 1)
    mysolver.internalPointBuildView("sig_xx_vtmt1_inc", IPField.STRESS_XX_VTMT1_INC, nstepArch, 1)
    mysolver.internalPointBuildView("sig_xx_vtmt2_mtx", IPField.STRESS_XX_VTMT2_MTX, nstepArch, 1)
    mysolver.internalPointBuildView("sig_xx_vtmt2_inc", IPField.STRESS_XX_VTMT2_INC, nstepArch, 1)
    mysolver.internalPointBuildView("sig_xx_vtmt3_mtx", IPField.STRESS_XX_VTMT3_MTX, nstepArch, 1)
    mysolver.internalPointBuildView("sig_xx_vtmt3_inc", IPField.STRESS_XX_VTMT3_INC, nstepArch, 1)
    mysolver.internalPointBuildView("sig_xx_vtmt4_mtx", IPField.STRESS_XX_VTMT4_MTX, nstepArch, 1)
    mysolver.internalPointBuildView("sig_xx_vtmt4_inc", IPField.STRESS_XX_VTMT4_INC, nstepArch, 1)
    mysolver.internalPointBuildView("sig_xx_vtmt5_mtx", IPField.STRESS_XX_VTMT5_MTX, nstepArch, 1)
    mysolver.internalPointBuildView("sig_xx_vtmt5_inc", IPField.STRESS_XX_VTMT5_INC, nstepArch, 1)
    mysolver.internalPointBuildView("sig_xx_vtmt6_mtx", IPField.STRESS_XX_VTMT6_MTX, nstepArch, 1)
    mysolver.internalPointBuildView("sig_xx_vtmt6_inc", IPField.STRESS_XX_VTMT6_INC, nstepArch, 1)
elif ROMMod == "VLM":
    # damage for each laminate ply
    mysolver.internalPointBuildView("dam_vtlam1_m0", IPField.DAMAGE_VTLAM1_M0, nstepArch, 1)
    mysolver.internalPointBuildView("dam_vtlam2_m0", IPField.DAMAGE_VTLAM2_M0, nstepArch, 1)
    mysolver.internalPointBuildView("dam_vtlam3_m0", IPField.DAMAGE_VTLAM3_M0, nstepArch, 1)
    mysolver.internalPointBuildView("dam_vtlam4_m0", IPField.DAMAGE_VTLAM4_M0, nstepArch, 1)
    mysolver.internalPointBuildView("dam_vtlam5_m0", IPField.DAMAGE_VTLAM5_M0, nstepArch, 1)
    mysolver.internalPointBuildView("dam_vtlam6_m0", IPField.DAMAGE_VTLAM6_M0, nstepArch, 1)
    mysolver.internalPointBuildView("dam_vtlam7_m0", IPField.DAMAGE_VTLAM7_M0, nstepArch, 1)
    mysolver.internalPointBuildView("dam_vtlam8_m0", IPField.DAMAGE_VTLAM8_M0, nstepArch, 1)
    mysolver.internalPointBuildView("dam_vtlam9_m0", IPField.DAMAGE_VTLAM9_M0, nstepArch, 1)
    mysolver.internalPointBuildView("dam_vtlam10_m0", IPField.DAMAGE_VTLAM10_M0, nstepArch, 1)
    mysolver.internalPointBuildView("dam_vtlam11_m0", IPField.DAMAGE_VTLAM11_M0, nstepArch, 1)
    mysolver.internalPointBuildView("dam_vtlam12_m0", IPField.DAMAGE_VTLAM12_M0, nstepArch, 1)
    # damage for each material phase
    mysolver.internalPointBuildView("dam_vtlam1_mt_mtx", IPField.DAMAGE_VTLAM1_MT_MTX, nstepArch, 1)
    mysolver.internalPointBuildView("dam_vtlam1_mt_inc", IPField.DAMAGE_VTLAM1_MT_INC, nstepArch, 1)
    mysolver.internalPointBuildView("dam_vtlam2_mt_mtx", IPField.DAMAGE_VTLAM2_MT_MTX, nstepArch, 1)
    mysolver.internalPointBuildView("dam_vtlam2_mt_inc", IPField.DAMAGE_VTLAM2_MT_INC, nstepArch, 1)
    mysolver.internalPointBuildView("dam_vtlam3_mt_mtx", IPField.DAMAGE_VTLAM3_MT_MTX, nstepArch, 1)
    mysolver.internalPointBuildView("dam_vtlam3_mt_inc", IPField.DAMAGE_VTLAM3_MT_INC, nstepArch, 1)
    mysolver.internalPointBuildView("dam_vtlam4_mt_mtx", IPField.DAMAGE_VTLAM4_MT_MTX, nstepArch, 1)
    mysolver.internalPointBuildView("dam_vtlam4_mt_inc", IPField.DAMAGE_VTLAM4_MT_INC, nstepArch, 1)
    mysolver.internalPointBuildView("dam_vtlam5_mt_mtx", IPField.DAMAGE_VTLAM5_MT_MTX, nstepArch, 1)
    mysolver.internalPointBuildView("dam_vtlam5_mt_inc", IPField.DAMAGE_VTLAM5_MT_INC, nstepArch, 1)
    mysolver.internalPointBuildView("dam_vtlam6_mt_mtx", IPField.DAMAGE_VTLAM6_MT_MTX, nstepArch, 1)
    mysolver.internalPointBuildView("dam_vtlam6_mt_inc", IPField.DAMAGE_VTLAM6_MT_INC, nstepArch, 1)
    mysolver.internalPointBuildView("dam_vtlam7_mt_mtx", IPField.DAMAGE_VTLAM7_MT_MTX, nstepArch, 1)
    mysolver.internalPointBuildView("dam_vtlam7_mt_inc", IPField.DAMAGE_VTLAM7_MT_INC, nstepArch, 1)
    mysolver.internalPointBuildView("dam_vtlam8_mt_mtx", IPField.DAMAGE_VTLAM8_MT_MTX, nstepArch, 1)
    mysolver.internalPointBuildView("dam_vtlam8_mt_inc", IPField.DAMAGE_VTLAM8_MT_INC, nstepArch, 1)
    mysolver.internalPointBuildView("dam_vtlam9_mt_mtx", IPField.DAMAGE_VTLAM9_MT_MTX, nstepArch, 1)
    mysolver.internalPointBuildView("dam_vtlam9_mt_inc", IPField.DAMAGE_VTLAM9_MT_INC, nstepArch, 1)
    mysolver.internalPointBuildView("dam_vtlam10_mt_mtx", IPField.DAMAGE_VTLAM10_MT_MTX, nstepArch, 1)
    mysolver.internalPointBuildView("dam_vtlam10_mt_inc", IPField.DAMAGE_VTLAM10_MT_INC, nstepArch, 1)
    mysolver.internalPointBuildView("dam_vtlam11_mt_mtx", IPField.DAMAGE_VTLAM11_MT_MTX, nstepArch, 1)
    mysolver.internalPointBuildView("dam_vtlam11_mt_inc", IPField.DAMAGE_VTLAM11_MT_INC, nstepArch, 1)
    mysolver.internalPointBuildView("dam_vtlam12_mt_mtx", IPField.DAMAGE_VTLAM12_MT_MTX, nstepArch, 1)
    mysolver.internalPointBuildView("dam_vtlam12_mt_inc", IPField.DAMAGE_VTLAM12_MT_INC, nstepArch, 1)
    # strain for each VT phase
    mysolver.internalPointBuildView("eps_xx_vtlam1", IPField.STRAIN_XX_VTLAM1, nstepArch, 1)
    mysolver.internalPointBuildView("eps_xx_vtlam2", IPField.STRAIN_XX_VTLAM2, nstepArch, 1)
    mysolver.internalPointBuildView("eps_xx_vtlam3", IPField.STRAIN_XX_VTLAM3, nstepArch, 1)
    mysolver.internalPointBuildView("eps_xx_vtlam4", IPField.STRAIN_XX_VTLAM4, nstepArch, 1)
    mysolver.internalPointBuildView("eps_xx_vtlam5", IPField.STRAIN_XX_VTLAM5, nstepArch, 1)
    mysolver.internalPointBuildView("eps_xx_vtlam6", IPField.STRAIN_XX_VTLAM6, nstepArch, 1)
    mysolver.internalPointBuildView("eps_xx_vtlam7", IPField.STRAIN_XX_VTLAM7, nstepArch, 1)
    mysolver.internalPointBuildView("eps_xx_vtlam8", IPField.STRAIN_XX_VTLAM8, nstepArch, 1)
    mysolver.internalPointBuildView("eps_xx_vtlam9", IPField.STRAIN_XX_VTLAM9, nstepArch, 1)
    mysolver.internalPointBuildView("eps_xx_vtlam10", IPField.STRAIN_XX_VTLAM10, nstepArch, 1)
    mysolver.internalPointBuildView("eps_xx_vtlam11", IPField.STRAIN_XX_VTLAM11, nstepArch, 1)
    mysolver.internalPointBuildView("eps_xx_vtlam12", IPField.STRAIN_XX_VTLAM12, nstepArch, 1)
    # stress for each VT phase
    mysolver.internalPointBuildView("sig_xx_vtlam1", IPField.STRESS_XX_VTLAM1, nstepArch, 1)
    mysolver.internalPointBuildView("sig_xx_vtlam2", IPField.STRESS_XX_VTLAM2, nstepArch, 1)
    mysolver.internalPointBuildView("sig_xx_vtlam3", IPField.STRESS_XX_VTLAM3, nstepArch, 1)
    mysolver.internalPointBuildView("sig_xx_vtlam4", IPField.STRESS_XX_VTLAM4, nstepArch, 1)
    mysolver.internalPointBuildView("sig_xx_vtlam5", IPField.STRESS_XX_VTLAM5, nstepArch, 1)
    mysolver.internalPointBuildView("sig_xx_vtlam6", IPField.STRESS_XX_VTLAM6, nstepArch, 1)
    mysolver.internalPointBuildView("sig_xx_vtlam7", IPField.STRESS_XX_VTLAM7, nstepArch, 1)
    mysolver.internalPointBuildView("sig_xx_vtlam8", IPField.STRESS_XX_VTLAM8, nstepArch, 1)
    mysolver.internalPointBuildView("sig_xx_vtlam9", IPField.STRESS_XX_VTLAM9, nstepArch, 1)
    mysolver.internalPointBuildView("sig_xx_vtlam10", IPField.STRESS_XX_VTLAM10, nstepArch, 1)
    mysolver.internalPointBuildView("sig_xx_vtlam11", IPField.STRESS_XX_VTLAM11, nstepArch, 1)
    mysolver.internalPointBuildView("sig_xx_vtlam12", IPField.STRESS_XX_VTLAM12, nstepArch, 1)
    # strain for each laminate ply
    mysolver.internalPointBuildView("eps_xx_vtlam1_m0", IPField.STRAIN_XX_VTLAM1_M0, nstepArch, 1)
    mysolver.internalPointBuildView("eps_xx_vtlam1_mt", IPField.STRAIN_XX_VTLAM1_MT, nstepArch, 1)
    mysolver.internalPointBuildView("eps_xx_vtlam2_m0", IPField.STRAIN_XX_VTLAM2_M0, nstepArch, 1)
    mysolver.internalPointBuildView("eps_xx_vtlam2_mt", IPField.STRAIN_XX_VTLAM2_MT, nstepArch, 1)
    mysolver.internalPointBuildView("eps_xx_vtlam3_m0", IPField.STRAIN_XX_VTLAM3_M0, nstepArch, 1)
    mysolver.internalPointBuildView("eps_xx_vtlam3_mt", IPField.STRAIN_XX_VTLAM3_MT, nstepArch, 1)
    mysolver.internalPointBuildView("eps_xx_vtlam4_m0", IPField.STRAIN_XX_VTLAM4_M0, nstepArch, 1)
    mysolver.internalPointBuildView("eps_xx_vtlam4_mt", IPField.STRAIN_XX_VTLAM4_MT, nstepArch, 1)
    mysolver.internalPointBuildView("eps_xx_vtlam5_m0", IPField.STRAIN_XX_VTLAM5_M0, nstepArch, 1)
    mysolver.internalPointBuildView("eps_xx_vtlam5_mt", IPField.STRAIN_XX_VTLAM5_MT, nstepArch, 1)
    mysolver.internalPointBuildView("eps_xx_vtlam6_m0", IPField.STRAIN_XX_VTLAM6_M0, nstepArch, 1)
    mysolver.internalPointBuildView("eps_xx_vtlam6_mt", IPField.STRAIN_XX_VTLAM6_MT, nstepArch, 1)
    mysolver.internalPointBuildView("eps_xx_vtlam7_m0", IPField.STRAIN_XX_VTLAM7_M0, nstepArch, 1)
    mysolver.internalPointBuildView("eps_xx_vtlam7_mt", IPField.STRAIN_XX_VTLAM7_MT, nstepArch, 1)
    mysolver.internalPointBuildView("eps_xx_vtlam8_m0", IPField.STRAIN_XX_VTLAM8_M0, nstepArch, 1)
    mysolver.internalPointBuildView("eps_xx_vtlam8_mt", IPField.STRAIN_XX_VTLAM8_MT, nstepArch, 1)
    mysolver.internalPointBuildView("eps_xx_vtlam9_m0", IPField.STRAIN_XX_VTLAM9_M0, nstepArch, 1)
    mysolver.internalPointBuildView("eps_xx_vtlam9_mt", IPField.STRAIN_XX_VTLAM9_MT, nstepArch, 1)
    mysolver.internalPointBuildView("eps_xx_vtlam10_m0", IPField.STRAIN_XX_VTLAM10_M0, nstepArch, 1)
    mysolver.internalPointBuildView("eps_xx_vtlam10_mt", IPField.STRAIN_XX_VTLAM10_MT, nstepArch, 1)
    mysolver.internalPointBuildView("eps_xx_vtlam11_m0", IPField.STRAIN_XX_VTLAM11_M0, nstepArch, 1)
    mysolver.internalPointBuildView("eps_xx_vtlam11_mt", IPField.STRAIN_XX_VTLAM11_MT, nstepArch, 1)
    mysolver.internalPointBuildView("eps_xx_vtlam12_m0", IPField.STRAIN_XX_VTLAM12_M0, nstepArch, 1)
    mysolver.internalPointBuildView("eps_xx_vtlam12_mt", IPField.STRAIN_XX_VTLAM12_MT, nstepArch, 1)
    # stress for each laminate ply
    mysolver.internalPointBuildView("sig_xx_vtlam1_m0", IPField.STRESS_XX_VTLAM1_M0, nstepArch, 1)
    mysolver.internalPointBuildView("sig_xx_vtlam1_mt", IPField.STRESS_XX_VTLAM1_MT, nstepArch, 1)
    mysolver.internalPointBuildView("sig_xx_vtlam2_m0", IPField.STRESS_XX_VTLAM2_M0, nstepArch, 1)
    mysolver.internalPointBuildView("sig_xx_vtlam2_mt", IPField.STRESS_XX_VTLAM2_MT, nstepArch, 1)
    mysolver.internalPointBuildView("sig_xx_vtlam3_m0", IPField.STRESS_XX_VTLAM3_M0, nstepArch, 1)
    mysolver.internalPointBuildView("sig_xx_vtlam3_mt", IPField.STRESS_XX_VTLAM3_MT, nstepArch, 1)
    mysolver.internalPointBuildView("sig_xx_vtlam4_m0", IPField.STRESS_XX_VTLAM4_M0, nstepArch, 1)
    mysolver.internalPointBuildView("sig_xx_vtlam4_mt", IPField.STRESS_XX_VTLAM4_MT, nstepArch, 1)
    mysolver.internalPointBuildView("sig_xx_vtlam5_m0", IPField.STRESS_XX_VTLAM5_M0, nstepArch, 1)
    mysolver.internalPointBuildView("sig_xx_vtlam5_mt", IPField.STRESS_XX_VTLAM5_MT, nstepArch, 1)
    mysolver.internalPointBuildView("sig_xx_vtlam6_m0", IPField.STRESS_XX_VTLAM6_M0, nstepArch, 1)
    mysolver.internalPointBuildView("sig_xx_vtlam6_mt", IPField.STRESS_XX_VTLAM6_MT, nstepArch, 1)
    mysolver.internalPointBuildView("sig_xx_vtlam7_m0", IPField.STRESS_XX_VTLAM7_M0, nstepArch, 1)
    mysolver.internalPointBuildView("sig_xx_vtlam7_mt", IPField.STRESS_XX_VTLAM7_MT, nstepArch, 1)
    mysolver.internalPointBuildView("sig_xx_vtlam8_m0", IPField.STRESS_XX_VTLAM8_M0, nstepArch, 1)
    mysolver.internalPointBuildView("sig_xx_vtlam8_mt", IPField.STRESS_XX_VTLAM8_MT, nstepArch, 1)
    mysolver.internalPointBuildView("sig_xx_vtlam9_m0", IPField.STRESS_XX_VTLAM9_M0, nstepArch, 1)
    mysolver.internalPointBuildView("sig_xx_vtlam9_mt", IPField.STRESS_XX_VTLAM9_MT, nstepArch, 1)
    mysolver.internalPointBuildView("sig_xx_vtlam10_m0", IPField.STRESS_XX_VTLAM10_M0, nstepArch, 1)
    mysolver.internalPointBuildView("sig_xx_vtlam10_mt", IPField.STRESS_XX_VTLAM10_MT, nstepArch, 1)
    mysolver.internalPointBuildView("sig_xx_vtlam11_m0", IPField.STRESS_XX_VTLAM11_M0, nstepArch, 1)
    mysolver.internalPointBuildView("sig_xx_vtlam11_mt", IPField.STRESS_XX_VTLAM11_MT, nstepArch, 1)
    mysolver.internalPointBuildView("sig_xx_vtlam12_m0", IPField.STRESS_XX_VTLAM12_M0, nstepArch, 1)
    mysolver.internalPointBuildView("sig_xx_vtlam12_mt", IPField.STRESS_XX_VTLAM12_MT, nstepArch, 1)
    # strain for each material phase
    mysolver.internalPointBuildView("eps_xx_vtlam1_mt_mtx", IPField.STRAIN_XX_VTLAM1_MT_MTX, nstepArch, 1)
    mysolver.internalPointBuildView("eps_xx_vtlam1_mt_inc", IPField.STRAIN_XX_VTLAM1_MT_INC, nstepArch, 1)
    mysolver.internalPointBuildView("eps_xx_vtlam2_mt_mtx", IPField.STRAIN_XX_VTLAM2_MT_MTX, nstepArch, 1)
    mysolver.internalPointBuildView("eps_xx_vtlam2_mt_inc", IPField.STRAIN_XX_VTLAM2_MT_INC, nstepArch, 1)
    mysolver.internalPointBuildView("eps_xx_vtlam3_mt_mtx", IPField.STRAIN_XX_VTLAM3_MT_MTX, nstepArch, 1)
    mysolver.internalPointBuildView("eps_xx_vtlam3_mt_inc", IPField.STRAIN_XX_VTLAM3_MT_INC, nstepArch, 1)
    mysolver.internalPointBuildView("eps_xx_vtlam4_mt_mtx", IPField.STRAIN_XX_VTLAM4_MT_MTX, nstepArch, 1)
    mysolver.internalPointBuildView("eps_xx_vtlam4_mt_inc", IPField.STRAIN_XX_VTLAM4_MT_INC, nstepArch, 1)
    mysolver.internalPointBuildView("eps_xx_vtlam5_mt_mtx", IPField.STRAIN_XX_VTLAM5_MT_MTX, nstepArch, 1)
    mysolver.internalPointBuildView("eps_xx_vtlam5_mt_inc", IPField.STRAIN_XX_VTLAM5_MT_INC, nstepArch, 1)
    mysolver.internalPointBuildView("eps_xx_vtlam6_mt_mtx", IPField.STRAIN_XX_VTLAM6_MT_MTX, nstepArch, 1)
    mysolver.internalPointBuildView("eps_xx_vtlam6_mt_inc", IPField.STRAIN_XX_VTLAM6_MT_INC, nstepArch, 1)
    mysolver.internalPointBuildView("eps_xx_vtlam7_mt_mtx", IPField.STRAIN_XX_VTLAM7_MT_MTX, nstepArch, 1)
    mysolver.internalPointBuildView("eps_xx_vtlam7_mt_inc", IPField.STRAIN_XX_VTLAM7_MT_INC, nstepArch, 1)
    mysolver.internalPointBuildView("eps_xx_vtlam8_mt_mtx", IPField.STRAIN_XX_VTLAM8_MT_MTX, nstepArch, 1)
    mysolver.internalPointBuildView("eps_xx_vtlam8_mt_inc", IPField.STRAIN_XX_VTLAM8_MT_INC, nstepArch, 1)
    mysolver.internalPointBuildView("eps_xx_vtlam9_mt_mtx", IPField.STRAIN_XX_VTLAM9_MT_MTX, nstepArch, 1)
    mysolver.internalPointBuildView("eps_xx_vtlam9_mt_inc", IPField.STRAIN_XX_VTLAM9_MT_INC, nstepArch, 1)
    mysolver.internalPointBuildView("eps_xx_vtlam10_mt_mtx", IPField.STRAIN_XX_VTLAM10_MT_MTX, nstepArch, 1)
    mysolver.internalPointBuildView("eps_xx_vtlam10_mt_inc", IPField.STRAIN_XX_VTLAM10_MT_INC, nstepArch, 1)
    mysolver.internalPointBuildView("eps_xx_vtlam11_mt_mtx", IPField.STRAIN_XX_VTLAM11_MT_MTX, nstepArch, 1)
    mysolver.internalPointBuildView("eps_xx_vtlam11_mt_inc", IPField.STRAIN_XX_VTLAM11_MT_INC, nstepArch, 1)
    mysolver.internalPointBuildView("eps_xx_vtlam12_mt_mtx", IPField.STRAIN_XX_VTLAM12_MT_MTX, nstepArch, 1)
    mysolver.internalPointBuildView("eps_xx_vtlam12_mt_inc", IPField.STRAIN_XX_VTLAM12_MT_INC, nstepArch, 1)
    # stress
    mysolver.internalPointBuildView("sig_xx_vtlam1_mt_mtx", IPField.STRESS_XX_VTLAM1_MT_MTX, nstepArch, 1)
    mysolver.internalPointBuildView("sig_xx_vtlam1_mt_inc", IPField.STRESS_XX_VTLAM1_MT_INC, nstepArch, 1)
    mysolver.internalPointBuildView("sig_xx_vtlam2_mt_mtx", IPField.STRESS_XX_VTLAM2_MT_MTX, nstepArch, 1)
    mysolver.internalPointBuildView("sig_xx_vtlam2_mt_inc", IPField.STRESS_XX_VTLAM2_MT_INC, nstepArch, 1)
    mysolver.internalPointBuildView("sig_xx_vtlam3_mt_mtx", IPField.STRESS_XX_VTLAM3_MT_MTX, nstepArch, 1)
    mysolver.internalPointBuildView("sig_xx_vtlam3_mt_inc", IPField.STRESS_XX_VTLAM3_MT_INC, nstepArch, 1)
    mysolver.internalPointBuildView("sig_xx_vtlam4_mt_mtx", IPField.STRESS_XX_VTLAM4_MT_MTX, nstepArch, 1)
    mysolver.internalPointBuildView("sig_xx_vtlam4_mt_inc", IPField.STRESS_XX_VTLAM4_MT_INC, nstepArch, 1)
    mysolver.internalPointBuildView("sig_xx_vtlam5_mt_mtx", IPField.STRESS_XX_VTLAM5_MT_MTX, nstepArch, 1)
    mysolver.internalPointBuildView("sig_xx_vtlam5_mt_inc", IPField.STRESS_XX_VTLAM5_MT_INC, nstepArch, 1)
    mysolver.internalPointBuildView("sig_xx_vtlam6_mt_mtx", IPField.STRESS_XX_VTLAM6_MT_MTX, nstepArch, 1)
    mysolver.internalPointBuildView("sig_xx_vtlam6_mt_inc", IPField.STRESS_XX_VTLAM6_MT_INC, nstepArch, 1)
    mysolver.internalPointBuildView("sig_xx_vtlam7_mt_mtx", IPField.STRESS_XX_VTLAM7_MT_MTX, nstepArch, 1)
    mysolver.internalPointBuildView("sig_xx_vtlam7_mt_inc", IPField.STRESS_XX_VTLAM7_MT_INC, nstepArch, 1)
    mysolver.internalPointBuildView("sig_xx_vtlam8_mt_mtx", IPField.STRESS_XX_VTLAM8_MT_MTX, nstepArch, 1)
    mysolver.internalPointBuildView("sig_xx_vtlam8_mt_inc", IPField.STRESS_XX_VTLAM8_MT_INC, nstepArch, 1)
    mysolver.internalPointBuildView("sig_xx_vtlam9_mt_mtx", IPField.STRESS_XX_VTLAM9_MT_MTX, nstepArch, 1)
    mysolver.internalPointBuildView("sig_xx_vtlam9_mt_inc", IPField.STRESS_XX_VTLAM9_MT_INC, nstepArch, 1)
    mysolver.internalPointBuildView("sig_xx_vtlam10_mt_mtx", IPField.STRESS_XX_VTLAM10_MT_MTX, nstepArch, 1)
    mysolver.internalPointBuildView("sig_xx_vtlam10_mt_inc", IPField.STRESS_XX_VTLAM10_MT_INC, nstepArch, 1)
    mysolver.internalPointBuildView("sig_xx_vtlam11_mt_mtx", IPField.STRESS_XX_VTLAM11_MT_MTX, nstepArch, 1)
    mysolver.internalPointBuildView("sig_xx_vtlam11_mt_inc", IPField.STRESS_XX_VTLAM11_MT_INC, nstepArch, 1)
    mysolver.internalPointBuildView("sig_xx_vtlam12_mt_mtx", IPField.STRESS_XX_VTLAM12_MT_MTX, nstepArch, 1)
    mysolver.internalPointBuildView("sig_xx_vtlam12_mt_inc", IPField.STRESS_XX_VTLAM12_MT_INC, nstepArch, 1)

mysolver.archivingForceOnPhysicalGroup("Face", 101, 0, nstepArchForce)
mysolver.archivingForceOnPhysicalGroup("Face", 102, 0, nstepArchForce)
mysolver.archivingNodeDisplacement(21, 0, nstepArchForce)
mysolver.archivingNodeDisplacement(23, 0, nstepArchForce)


mysolver.solve()
