Include "original_smp_data.geo";

SetFactory("OpenCASCADE");

Mesh.Optimize = 1;

// coil (4 parallelepipedic parts)
xc = -Lx/2-wcoil; dxc = wcoil;
yc = -Ly/2 ; dyc = Ly;
zc = -Lz/2;  dzc = Lz;

vBc()+=newv; Box(newv) = {xc, yc, zc, dxc, dyc, dzc};
vBc()+=Translate {Lx+wcoil,0,0} { Duplicata {Volume{vBc(0)};}};

xc = -Lx/2; dxc = Lx;
yc = -Ly/2 ; dyc = Ly;
zc = -Lz/2-wcoil;  dzc = wcoil;

vBc()+=newv; Box(newv) = {xc, yc, zc, dxc, dyc, dzc};
vBc()+=Translate {0, 0,Lz+wcoil} { Duplicata {Volume{vBc(2)};}};

// Coil (add corners)
vaux=newv; Cylinder(newv) = { 0,-Ly/2,0,  0,Ly,0,  wcoil};
cutsurf()=news; Rectangle(news) = {-wcoil,-Ly/2,0,  2*wcoil,Ly,0};
cutsurf()+=Rotate {{0, 1, 0}, {0, 0, 0}, Pi/2} { Duplicata { Surface{cutsurf(0)}; } };

vCc() = BooleanFragments{ Volume{vaux}; Surface{cutsurf()}; Delete; }{}; //corners

Translate {-Lx/2, 0,  Lz/2} { Volume{vCc(0)};}
Translate { Lx/2, 0,  Lz/2} { Volume{vCc(1)};}
Translate {-Lx/2, 0, -Lz/2} { Volume{vCc(2)};}
Translate { Lx/2, 0, -Lz/2} { Volume{vCc(3)};}

vCoil()+=newv; BooleanUnion(newv) = { Volume{vBc()}; Delete; }{ Volume{vCc()}; Delete; };

// Smp 
vCoreE()=newv; Cylinder(newv) = { 0,-H/2,0,  0,H,0,  R};

// Air around
vA()+=newv; Sphere(newv) = {0,0,0, Rint};
vA()+=newv; Sphere(newv) = {0,0,0, Rext};

vAir()+=newv; BooleanDifference(newv) = { Volume{vA(1)}; Delete; }{ Volume{vA(0)}; };
vAir()+=newv; BooleanDifference(newv) = { Volume{vA(0)}; Delete; }{ Volume{vCoreE(),vCoil()}; };

If(Flag_Symmetry)
  xa =  -Rext;   ya =  -Rext;  za =  -Rext;
  dxa =  2*Rext; dya = 2*Rext; dza = 2*Rext;

  If(Flag_Symmetry==1)
    vAux=newv; Box(newv) = {xa, ya, 0*za, dxa, dya, -dza/2};
  EndIf
  If(Flag_Symmetry==2)
    vAux=newv; Box(newv) = {0*xa, ya, 0*za, dxa/2, dya, -dza/2};
  EndIf

  For k In {0:#vCoil()-1}
    vvv=newv; BooleanIntersection(newv) = { Volume{vCoil(k)}; Delete; }{ Volume{vAux}; };
    vCoil(k) = vvv;
  EndFor
  For k In {0:#vAir()-1}
    vvv=newv; BooleanIntersection(newv) = { Volume{vAir(k)}; Delete; }{ Volume{vAux}; };
    vAir(k) = vvv;
  EndFor

  vvv=newv; BooleanIntersection(newv) = { Volume{vCoreE(0)}; Delete; }{ Volume{vAux}; Delete; };
  vCoreE(0) = vvv;
EndIf

BooleanFragments{ Volume{vAir(), vCoreE(), vCoil()}; Delete; }{} // This needs to be done at the end

// Adapting mesh size
// characteristic lengths
lc0 = Pi*Rint/10; // air
lc1  = wcoil/3; // coil 
lc2  = R/6; // smp

Characteristic Length { PointsOf{ Volume{vAir(0)}; } } = lc0;
Characteristic Length { PointsOf{ Volume{vCoil()}; } } = lc1;
Characteristic Length { PointsOf{ Volume{vCoreE()}; } } = lc2;


// Boundary conditions
tol = 1e-5;
cut_xy() = Surface In BoundingBox {-Rext-tol,-Rext-tol,-tol, 2*(Rext+tol), 2*(Rext+tol), 2*tol}; // 1/2, 1/4
cut_yz() = Surface In BoundingBox {-tol,-Rext-tol,-Rext-tol, 2*tol, 2*(Rext+tol), 2*(Rext+tol)}; // 1/4

all_vol() = Volume '*';
Printf("all_vol()=", all_vol());
bndAir() = CombinedBoundary{Volume{all_vol()};};
bndAir() -= {cut_xy(),cut_yz()};


//=================================================
// Some colors... for aesthetics :-)
//=================================================
Recursive Color SkyBlue {Volume{vAir()};}
Recursive Color SteelBlue {Volume{vCoreE()};}
Recursive Color Red {Volume{vCoil()};}

//=================================================
// Physical regions for FE analysis with GetDP
//=================================================

Physical Volume(ECORE) = vCoreE();
bnd_vCoreE() = CombinedBoundary{Volume{vCoreE()};};
bnd_vCoreE() -= {cut_xy(),cut_yz()};
Physical Surface(SKINECORE) = bnd_vCoreE();

Physical Surface(REFCORE)= bnd_vCoreE(1);
pts() = PointsOf {Surface { bnd_vCoreE(1) }; };
Physical Point(REFCOREPOINT)= pts(0);

Physical Volume(COIL) = vCoil();
bnd_vCoil() = CombinedBoundary{Volume{vCoil()};};
bnd_vCoil() -= {cut_xy(),cut_yz()};
Physical Surface(SKINCOIL) = bnd_vCoil();

If(Flag_Infinity==0)
  Physical Volume(AIR) = {vAir()};
EndIf
If(Flag_Infinity==1)
  Physical Volume(AIR) = vAir(1);
  Physical Volume(AIRINF) = vAir(0);
EndIf
Physical Surface(SURF_AIROUT) = bndAir();

Physical Surface(CUT_XY) = {cut_xy()};
Physical Surface(CUT_YZ) = {cut_yz()}; // BC if symmetry
