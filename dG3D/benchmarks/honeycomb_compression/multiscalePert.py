#coding-Utf-8-*-
from gmshpy import *
from dG3Dpy import*

import math

#script to launch PBC problem with a python script
#---------------------------------------------------------------------------------------------------------------#
#DEFINE MICRO PROBLEM

# micro-material law
E = 68.9E3 #Young modulus
nu = 0.33 #Poisson ratio
K = E/3./(1.-2.*nu)
mu = E/2./(1+nu)
sy0 = 276. #Yield stress
h = E/100 # hardening modulus
rho = 1.  #density
# micro-geometry

# creation of material law
harden1 = LinearExponentialJ2IsotropicHardening(11, sy0, h,  0., 10.)
law1 = J2LinearDG3DMaterialLaw(11,rho,E,nu,harden1)

# creation of  micro part Domain
myfield1 = dG3DDomain(1000,11,0,11,0,2)



microBC = nonLinearPeriodicBC(1000,2)
microBC.setBCPhysical(1,2,3,4)
method =1	# Periodic mesh = 0
degree =11 # Order used for polynomial interpolation 
addvertex = True  # Polynomial interpolation by mesh vertex = 0, Polynomial interpolation by virtual vertex 
microBC.setPeriodicBCOptions(method, degree, bool(addvertex))

# DEFINE MACROPROBLEM
matnum1 = 1;
macromat1 = hoDGMultiscaleMaterialLaw(matnum1, 1000)
macromat1.loadAllRVEMeshes("rve")

microSolver1 = macromat1.getMicroSolver()
microSolver1.loadModel("rve1.msh")
microSolver1.addDomain(myfield1)
microSolver1.addMaterialLaw(law1);

microSolver1.addMicroBC(microBC)

#microSolver1.pathFollowing(True,0)
#microSolver1.setPathFollowingControlType(1)
#microSolver1.setPathFollowingArcLengthStep(1e-3)


microSolver1.snlData(1,1.,1e-5,1e-10)
microSolver1.setSystemType(1)
microSolver1.Solver(2)


microSolver1.Scheme(1)
microSolver1.setSameStateCriterion(1e-16)
microSolver1.stressAveragingFlag(True) # set stress averaging ON- 0 , OFF-1
microSolver1.setStressAveragingMethod(0)
microSolver1.tangentAveragingFlag(True) # set tangent averaging ON -0, OFF -1
microSolver1.setTangentAveragingMethod(2,1e-6);

microSolver1.internalPointBuildView("Green-Lagrange_xx",IPField.GL_XX);
microSolver1.internalPointBuildView("Green-Lagrange_yy",IPField.GL_YY);
microSolver1.internalPointBuildView("Green-Lagrange_xy",IPField.GL_XY);
microSolver1.internalPointBuildView("sig_xx",IPField.SIG_XX);
microSolver1.internalPointBuildView("sig_yy",IPField.SIG_YY);
microSolver1.internalPointBuildView("sig_xy",IPField.SIG_XY);
microSolver1.internalPointBuildView("sig_VM",IPField.SVM);

microSolver1.internalPointBuildView("Equivalent plastic strain",IPField.PLASTICSTRAIN);
microSolver1.internalPointBuildView("Deformation energy",IPField.DEFO_ENERGY);
microSolver1.internalPointBuildView("Plastic energy",IPField.PLASTIC_ENERGY);

microSolver1.displacementBC("Face",11,2,0.)


#------------------------------------------
macromeshfile="macro4.msh" # name of mesh file

# solver
sol = 2  # Gmm=0 (default) Taucs=1 PETsc=2
soltype =1 # StaticLinear=0 (default) StaticNonLinear=1
nstep = 200  # number of step (used only if soltype=1)
ftime =1.   # Final time (used only if soltype=1)
tol=1.e-5  # relative tolerance for NR scheme (used only if soltype=1)
nstepArch=1 # Number of step between 2 archiving (used only if soltype=1)


# creation of  macro part Domain

dim =2
fulldg = 1
beta1 = 1000

#macrodomain1 = FSDomain(1000,11,matnum1,dim)
macrodomain1 =hoDGDomain(1000,11,0,matnum1,fulldg,dim)
macrodomain1.stabilityParameters(beta1)
#macrodomain1.distributeOnRootRank(1)
macrodomain1.matrixByPerturbation(1,1,1,1.e-8)

# creation of Solver
mysolver = nonLinearMechSolver(1000)
mysolver.loadModel(macromeshfile)
mysolver.addDomain(macrodomain1)
mysolver.addMaterialLaw(macromat1)

mysolver.Scheme(soltype)
mysolver.Solver(sol)
mysolver.snlData(nstep,ftime,tol)

#mysolver.pathFollowing(True,0)
#mysolver.setPathFollowingControlType(1)
#mysolver.setPathFollowingArcLengthStep(1e-3)

# boundary condition
mysolver.displacementBC("Face",11,2,0.0)

mysolver.displacementBC("Node",4,0,0)
mysolver.displacementBC("Edge",1,1,0)

#mysolver.constraintBC("Edge",3,1)
#mysolver.forceBC("Edge",3,1,-0.25e2)
mysolver.displacementBC("Edge",3,1,-5.)

# archivage
mysolver.internalPointBuildView("Green-Lagrange_xx",IPField.STRAIN_XX, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_yy",IPField.STRAIN_YY, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_zz",IPField.STRAIN_ZZ, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_xy",IPField.STRAIN_XY, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_yz",IPField.STRAIN_YZ, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_xz",IPField.STRAIN_XZ, 1, 1);
mysolver.internalPointBuildView("sig_xx",IPField.SIG_XX, 1, 1);
mysolver.internalPointBuildView("sig_yy",IPField.SIG_YY, 1, 1);
mysolver.internalPointBuildView("sig_zz",IPField.SIG_ZZ, 1, 1);
mysolver.internalPointBuildView("sig_xy",IPField.SIG_XY, 1, 1);
mysolver.internalPointBuildView("sig_yz",IPField.SIG_YZ, 1, 1);
mysolver.internalPointBuildView("sig_xz",IPField.SIG_XZ, 1, 1);
mysolver.internalPointBuildView("sig_VM",IPField.SVM, 1, 1);
mysolver.internalPointBuildView("Equivalent plastic strain",IPField.PLASTICSTRAIN, 1, 1);
mysolver.internalPointBuildView("High-order strain norm",IPField.HIGHSTRAIN_NORM, 1, 1);
mysolver.internalPointBuildView("High-order stress norm",IPField.HIGHSTRESS_NORM, 1, 1);
mysolver.internalPointBuildView("Green -Lagrange strain norm",IPField.GL_NORM, 1, 1);
mysolver.internalPointBuildView("PK2 stress norm",IPField.STRESS_NORM, 1, 1);

mysolver.archivingForceOnPhysicalGroup("Edge",1,1)
mysolver.archivingNodeDisplacement(4,1,1)

# solve
mysolver.solve()
# test check
check = TestCheck()
check.equal(1.947727e+02,mysolver.getArchivedForceOnPhysicalGroup("Edge", 1, 1),1e-3)

