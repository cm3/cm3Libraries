# test file

set(PYFILE multiscale.py)

set(FILES2DELETE 
  *.csv
  disp*
  stress*
)

add_cm3python_mpi_test(4 ${PYFILE} "${FILES2DELETE}")
