# run multiscale simulation with multiple RVEs

- create a list of meshes with a prefix:
	- all RVE must use same physicals
	- the names of RVEs must defined with a prefix as "prefix"+ "a number" +.msh 
	load all micro-mesh 
	
	    ```macromat1.loadAllRVEMeshes("prefix used in the RVE mesh names")```
  
- prepare a multiscale simulation with a reference RVE mesh (eg. with rve1.msh)

	```microSolver1.loadModel("rve1.msh")```
		
- Run simulation. 

## Notes

- If a list of meshes with a prefix is NOT present, the multiscale simulation will be performed with the REFERENCE mesh

- If a list of meshes with a prefix is present, a micro-solver is created at each GP by replacing the reference mesh by a mesh in the list of meshes identified by prefix. 

- see idMap0.txt to find which RVE is assigned for each GP. In the first column,  the value is given as: "element number"*100+"GP number"

- A RVE mesh can be directly assigned in a GP by

    ```macromat1.addPertutationalMeshIP(ele-number, GP-number, mesh-file-name)```
