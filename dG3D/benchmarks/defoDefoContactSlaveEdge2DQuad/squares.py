#coding-Utf-8-*-
from gmshpy import *
from dG3Dpy import*

#script to launch beam problem with a python script

# material law
lawnum   = 1 # unique number of law
rho      = 7850
E        = 100.e9
nu       = 0.2 #

penalty  =10.*E
thick    =1.e-4

# geometry
geofile="squares.geo"
meshfile="squares.msh" # name of mesh file

# solver
sol = 2  # Gmm=0 (default) Taucs=1 PETsc=2
soltype = 1 # StaticLinear=0 (default) StaticNonLinear=1
nstep = 1000   # number of step (used only if soltype=1)
ftime =1.   # Final time (used only if soltype=1)
tol=1.e-6   # relative tolerance for NR scheme (used only if soltype=1)
nstepArch=1 # Number of step between 2 archiving (used only if soltype=1)
fullDg = 0 #O = CG, 1 = DG
space1 = 0 # function space (Lagrange=0)


#  compute solution and BC (given directly to the solver
# creation of law

law1 = dG3DLinearElasticMaterialLaw(lawnum,rho,E,nu)
law1.setUseBarF(True)

# creation of ElasticField
nfield1 = 1265 # number of the field (physical number of surface)
myfield1 = dG3DDomain(1000,nfield1,space1,lawnum,fullDg,2)
nfield2 = 1365 # number of the field (physical number of surface)
myfield2 = dG3DDomain(1100,nfield2,space1,lawnum,fullDg,2)
#myfield1.matrixByPerturbation(1,1,1,1e-8)



# creation of Solver
mysolver = nonLinearMechSolver(1200)
mysolver.createModel(geofile,meshfile,2,1)
#mysolver.loadModel(meshfile)
mysolver.addDomain(myfield1)
mysolver.addDomain(myfield2)
mysolver.addMaterialLaw(law1)
mysolver.Scheme(soltype)
mysolver.Solver(sol)
mysolver.snlData(nstep,ftime,tol)
mysolver.stepBetweenArchiving(nstepArch)

mysolver.displacementBC("Face",nfield1,2,0.)
mysolver.displacementBC("Face",nfield2,2,0.)
#compression along z
mysolver.displacementBC("Edge",12,0,0.)
mysolver.displacementBC("Edge",12,1,0.)
d1=-0.00025
cyclicFunction1=cycleFunctionTime(0.,0.,ftime/4., d1/2., ftime/2., 0., 3.*ftime/4., d1/2., ftime, d1);
#cyclicFunction1=cycleFunctionTime(0.,0.,ftime, d1);
mysolver.displacementBC("Edge",156,0,0.)
mysolver.displacementBC("Edge",156,1,cyclicFunction1)

#contact
defoDefoContact1=dG3DNodeOnSurfaceContactDomain(1200, 1, 56, 1,112, penalty, thick)
mysolver.defoDefoContactInteraction(defoDefoContact1)
defoDefoContact2=dG3DNodeOnSurfaceContactDomain(1200, 1, 112, 1, 56, penalty, thick)
mysolver.defoDefoContactInteraction(defoDefoContact2)



mysolver.internalPointBuildView("svm",IPField.SVM, 1, 1)
mysolver.internalPointBuildView("sig_xx",IPField.SIG_XX, 1, 1)
mysolver.internalPointBuildView("sig_yy",IPField.SIG_YY, 1, 1)
mysolver.internalPointBuildView("sig_zz",IPField.SIG_ZZ, 1, 1)
mysolver.internalPointBuildView("sig_xy",IPField.SIG_XY, 1, 1)
mysolver.internalPointBuildView("sig_yz",IPField.SIG_YZ, 1, 1)
mysolver.internalPointBuildView("sig_xz",IPField.SIG_XZ, 1, 1)
mysolver.internalPointBuildView("epl",IPField.PLASTICSTRAIN, 1, 1)
mysolver.archivingForceOnPhysicalGroup("Edge", 12, 1)
mysolver.archivingForceOnPhysicalGroup("Edge", 56, 1)
mysolver.archivingForceOnPhysicalGroup("Edge", 112, 1)
mysolver.archivingForceOnPhysicalGroup("Edge", 156, 1)

#mysolver.createRestartBySteps(2);


mysolver.solve()

check = TestCheck()
check.equal(2.450488e+07,mysolver.getArchivedForceOnPhysicalGroup("Edge", 12, 1),1.e-6)


