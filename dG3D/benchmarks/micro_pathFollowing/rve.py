#coding-Utf-8-*-

from gmshpy import *
from dG3Dpy import*

import math

# material law
lawnum1 = 11 # unique number of law
E = 68.9E3 #Young modulus
nu = 0.33 #Poisson ratio
K = E/3./(1.-2.*nu)
mu = E/2./(1+nu)
sy0 = 276. #Yield stress
h = E/100 # hardening modulus
rho = 1.  #density

# creation of material law
harden = LinearExponentialJ2IsotropicHardening(1, sy0, h, 0., 10.)
law1   = J2LinearDG3DMaterialLaw(lawnum1,rho,E,nu,harden)


#type = 0;
#law1 = FSElasticMaterialLaw(lawnum,type,K,mu,rho)

# geometry
meshfile="rve1.msh" # name of mesh file

# creation of part Domain
nfield = 11 # number of the field (physical number of entity)
dim =2


myfield1 = dG3DDomain(10,11,0,lawnum1,0,2)


# solver
sol = 2  # Gmm=0 (default) Taucs=1 PETsc=2
soltype =1 # StaticLinear=0 (default) StaticNonLinear=1
nstep = 50  # number of step (used only if soltype=1)
ftime =1.   # Final time (used only if soltype=1)
tol=1.e-5  # relative tolerance for NR scheme (used only if soltype=1)
nstepArch=1 # Number of step between 2 archiving (used only if soltype=1)
system = 3 # Displacement elimination =0 Multiplier elimination = 1 Displacement+ multiplier = 2
control = 0 # load control = 0 arc length control euler = 1

# creation of Solver
mysolver = nonLinearMechSolver(1000)

mysolver.loadModel(meshfile)

mysolver.addDomain(myfield1)
mysolver.addMaterialLaw(law1)
mysolver.Scheme(soltype)
mysolver.Solver(sol)
mysolver.snlData(nstep,ftime,tol)
mysolver.setSystemType(system)
mysolver.setControlType(control)
mysolver.stiffnessModification(True)
mysolver.iterativeProcedure(True)
mysolver.setMessageView(True)

mysolver.setTimeIncrementAdaptation(True,5,1,0.2,0.,100)

mysolver.setMicroProblemIndentification(1,0)

mysolver.pathFollowing(True,0)
mysolver.setPathFollowingControlType(1)

microBC = nonLinearPeriodicBC(10,2)
microBC.setOrder(2)
microBC.setBCPhysical(1,2,3,4)
microBC.setPeriodicBCOptions(1, 9,True) 
 # Deformation gradient
microBC.setDeformationGradient(1.0,0.0,0.0,0.98)

 # Gradient of deformation gradient
#mysolver.setGradientOfDeformationGradient(0,1,0,0.1)
#mysolver.setGradientOfDeformationGradient(0,0,1,0.1) 
#mysolver.setGradientOfDeformationGradient(1,0,1,0.1) 
#mysolver.setGradientOfDeformationGradient(1,1,1,0.1) 
#mysolver.setGradientOfDeformationGradient(0,0,1,-0.1) 
#mysolver.setGradientOfDeformationGradient(0,0,0,0.1)

mysolver.addMicroBC(microBC)


#stress averaging flag and averaging method 0- VOLUME, 1- SURFACE
mysolver.stressAveragingFlag(True) # set stress averaging ON- 0 , OFF-1
mysolver.setStressAveragingMethod(0) # 0 -volume 1- surface
#tangent averaging flag
mysolver.tangentAveragingFlag(True) # set tangent averaging ON -0, OFF -1
mysolver.setTangentAveragingMethod(2,1e-6) # 0- perturbation 1- condensation


# build view
mysolver.internalPointBuildView("Green-Lagrange_xx",IPField.STRAIN_XX, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_yy",IPField.STRAIN_YY, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_zz",IPField.STRAIN_ZZ, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_xy",IPField.STRAIN_XY, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_yz",IPField.STRAIN_YZ, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_xz",IPField.STRAIN_XZ, 1, 1);
mysolver.internalPointBuildView("sig_xx",IPField.SIG_XX, 1, 1);
mysolver.internalPointBuildView("sig_yy",IPField.SIG_YY, 1, 1);
mysolver.internalPointBuildView("sig_zz",IPField.SIG_ZZ, 1, 1);
mysolver.internalPointBuildView("sig_xy",IPField.SIG_XY, 1, 1);
mysolver.internalPointBuildView("sig_yz",IPField.SIG_YZ, 1, 1);
mysolver.internalPointBuildView("sig_xz",IPField.SIG_XZ, 1, 1);
mysolver.internalPointBuildView("sig_VM",IPField.SVM, 1, 1);
mysolver.internalPointBuildView("Equivalent plastic strain",IPField.PLASTICSTRAIN, 1, 1);

mysolver.internalPointBuildView("F_xx",IPField.F_XX, 1, 1);
mysolver.internalPointBuildView("F_xy",IPField.F_XY, 1, 1);
mysolver.internalPointBuildView("F_yx",IPField.F_YX, 1, 1);
mysolver.internalPointBuildView("F_yy",IPField.F_YY, 1, 1);

mysolver.internalPointBuildView("G_xxx",IPField.G_XXX, 1, 1);
mysolver.internalPointBuildView("G_xyy",IPField.G_XYY, 1, 1);
mysolver.internalPointBuildView("G_xxy",IPField.G_XXY, 1, 1);
mysolver.internalPointBuildView("G_yxx",IPField.G_YXX, 1, 1);
mysolver.internalPointBuildView("G_yyy",IPField.G_YYY, 1, 1);
mysolver.internalPointBuildView("G_yxy",IPField.G_YXY, 1, 1);


mysolver.internalPointBuildView("High-order strain norm",IPField.HIGHSTRAIN_NORM, 1, 1);
mysolver.internalPointBuildView("High-order stress norm",IPField.HIGHSTRESS_NORM, 1, 1);
mysolver.internalPointBuildView("Green -Lagrange strain norm",IPField.GL_NORM, 1, 1);


# solve
mysolver.solve()
# test check
check = TestCheck()
check.equal(-4.525161e+00,mysolver.getHomogenizedStress(1,1),1.e-4)
check.equal(-1.053390e+02,mysolver.getHomogenizedTangent(1,1,1,1),1.e-4)

