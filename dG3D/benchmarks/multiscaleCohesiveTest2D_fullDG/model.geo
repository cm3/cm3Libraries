
mm=1.0;
L=1.*mm;
H = 1*mm; 
sl1=0.5*L;
Point(1)={0,0,0,sl1};
Point(2)={L,0,0,sl1};
Point(3)={L,H,0,sl1};
Point(4)={0,H,0,sl1};
Line(1)={1,2};
Line(2)={2,3};
Line(3)={3,4};
Line(4)={4,1};

l[0]=newreg;
Line Loop(l[0])={1,2,3,4};
Plane Surface(11)={l[0]};

Physical Surface(11)={11};
Physical Point(1)={1}; 
Physical Point(2)={2};
Physical Point(3)={3};
Physical Point(4)={4}; 

Transfinite Line {4, 2} = 2 Using Progression 1;
Transfinite Line {3, 1} = 2 Using Progression 1;
Transfinite Surface {11};
Recombine Surface {11};

Extrude {L, 0, 0} {
  Line{2}; Layers{1}; Recombine;
}
Physical Surface(12) = {15};
Physical Line(1) = {13, 1};
Physical Line(2) = {12};
Physical Line(3) = {14, 3};
Physical Line(4) = {4};
