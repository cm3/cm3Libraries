#coding-Utf-8-*-

from gmshpy import *
from dG3Dpy import*

#script to launch PBC problem with a python script

# material law
lawnum = 11 # unique number of law


rho   = 7850e-9
young = 28.9e3
nu    = 0.3 
sy0   = 100.
h     = young/50.

harden = LinearExponentialJ2IsotropicHardening(1, sy0, h, 0., 10.)
cl     = IsotropicCLengthLaw(1, 4e-2)
damlaw = SimpleSaturateDamageLaw(1, 300.,1.,0.,2.)


law1   = NonLocalDamageJ2HyperDG3DMaterialLaw(lawnum,rho,young,nu,harden,cl,damlaw)


meshfile="idealHole.msh" # name of mesh file


# creation of part Domain
nfield = 60 # number of the field (physical number of entity)
myfield1 = dG3DDomain(1000,nfield,0,lawnum,0,3,1)
myfield1.setNonLocalEqRatio(1.e6)

# solver
sol = 2  # Gmm=0 (default) Taucs=1 PETsc=2
soltype = 1 # StaticLinear=0 (default) StaticNonLinear=1
nstep = 50   # number of step (used only if soltype=1)
ftime =1.   # Final time (used only if soltype=1)
tol=1.e-4   # relative tolerance for NR scheme (used only if soltype=1)
nstepArch=10 # Number of step between 2 archiving (used only if soltype=1)
system = 2 # Displacement elimination =0 Multiplier elimination = 1 Displacement+ multiplier = 2
control = 1 # load control = 0 arc length control euler = 1

# creation of Solver
mysolver = nonLinearMechSolver(1000)
mysolver.loadModel(meshfile)
mysolver.addDomain(myfield1)
mysolver.addMaterialLaw(law1)
mysolver.Scheme(soltype)
mysolver.Solver(sol)
mysolver.snlData(nstep,ftime,tol)
mysolver.stepBetweenArchiving(nstepArch)
mysolver.setSystemType(system)
mysolver.setControlType(control)
mysolver.stiffnessModification(True)
mysolver.iterativeProcedure(True)
mysolver.setMessageView(True)
mysolver.options("-ksp_type preonly -pc_type lu -pc_factor_mat_solver_type mumps -mat_mumps_icntl_14 800 -mat_mumps_icntl_24 1 -mat_mumps_icntl_13 1")

#boundary condition
microBC = nonLinearPeriodicBC(10,3)
microBC.setOrder(1)
microBC.setBCPhysical(54,56,58,55,57,59)

method =0	# Periodic mesh = 0, Langrange interpolation = 1, Cubic spline interpolation =2,  FE linear= 3, FE Quad = 4
degree = 3	# Order used for polynomial interpolation 
addvertex = False # Polynomial interpolation by mesh vertex = 0, Polynomial interpolation by virtual vertex 
microBC.setPeriodicBCOptions(method, degree,bool(addvertex)) 

 # Deformation gradient
microBC.setDeformationGradient(1.015,0.0,0.0,0.,1.,0.,0.,0.,1.)
mysolver.addMicroBC(microBC)

#stress averaging flag and averaging method 0- VOLUME, 1- SURFACE
mysolver.stressAveragingFlag(True) # set stress averaging ON- 0 , OFF-1
mysolver.setStressAveragingMethod(0) # 0 -volume 1- surface
#tangent averaging flag
mysolver.tangentAveragingFlag(False) # set tangent averaging ON -0, OFF -1
mysolver.setTangentAveragingMethod(2,1e-6) # 0- perturbation 1- condensation

# build view
mysolver.internalPointBuildView("Green-Lagrange_xx",IPField.STRAIN_XX, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_yy",IPField.STRAIN_YY, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_zz",IPField.STRAIN_ZZ, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_xy",IPField.STRAIN_XY, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_yz",IPField.STRAIN_YZ, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_xz",IPField.STRAIN_XZ, 1, 1);

mysolver.internalPointBuildView("svm",IPField.SVM, 1, 1)
mysolver.internalPointBuildView("sig_xx",IPField.SIG_XX, 1, 1)
mysolver.internalPointBuildView("sig_yy",IPField.SIG_YY, 1, 1)
mysolver.internalPointBuildView("sig_zz",IPField.SIG_ZZ, 1, 1)
mysolver.internalPointBuildView("sig_xy",IPField.SIG_XY, 1, 1)
mysolver.internalPointBuildView("sig_yz",IPField.SIG_YZ, 1, 1)
mysolver.internalPointBuildView("sig_xz",IPField.SIG_XZ, 1, 1)
mysolver.internalPointBuildView("epl",IPField.PLASTICSTRAIN, 1, 1)
mysolver.internalPointBuildView("Damage",IPField.DAMAGE, 1, 1)
mysolver.internalPointBuildView("active Damage zone",IPField.ACTIVE_DISSIPATION, 1, 1)

# solve
mysolver.solve()

# test check
check = TestCheck()
check.equal(1.367830e+02,mysolver.getHomogenizedStress(0,0),1.e-4)


