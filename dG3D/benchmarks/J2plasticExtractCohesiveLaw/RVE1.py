#coding-Utf-8-*-

from gmshpy import *
from dG3Dpy import*

#script to launch PBC problem with a python script

# material law
lawnum = 2 
rho   = 1e-9
young = 3e3
nu    = 0.3
sy0   = 100.
h     = 20.

law1 = J2LinearDG3DMaterialLaw(lawnum,rho,young,nu,sy0,h)
law1.setStrainOrder(-1)


geofile="RVE1.geo"
meshfile="RVE1.msh" # name of mesh file


# creation of part Domain
myfield1 = dG3DDomain(11,11,0,lawnum,0,2,0)
myfield1.stabilityParameters(10)


# solver
sol = 2  # Gmm=0 (default) Taucs=1 PETsc=2
soltype = 1 # StaticLinear=0 (default) StaticNonLinear=1
nstep = 100   # number of step (used only if soltype=1)
ftime =1.   # Final time (used only if soltype=1)
tol=1.e-5   # relative tolerance for NR scheme (used only if soltype=1)
nstepArch=1 # Number of step between 2 archiving (used only if soltype=1)
system = 1 # Displacement elimination =0 Multiplier elimination = 1 Displacement+ multiplier = 2
control = 0 # load control = 0 arc length control euler = 1

# creation of Solver
mysolver = nonLinearMechSolver(1000)
mysolver.createModel(geofile,meshfile,2,2)
mysolver.addDomain(myfield1)
mysolver.addMaterialLaw(law1)
mysolver.Scheme(soltype)
mysolver.Solver(sol)
mysolver.snlData(nstep,ftime,tol)
mysolver.setSystemType(system)
mysolver.setControlType(control)
mysolver.stiffnessModification(True)
mysolver.iterativeProcedure(True)
mysolver.setMessageView(True)

microBC = nonLinearPeriodicBC(10,2)
microBC.setOrder(1)
microBC.setBCPhysical(1,2,3,4)

method =0	# Periodic mesh = 0, Langrange interpolation = 1, Cubic spline interpolation =2,  FE linear= 3, FE Quad = 4
degree = 3	# Order used for polynomial interpolation 
addvertex = False # Polynomial interpolation by mesh vertex = 0, Polynomial interpolation by virtual vertex 
microBC.setPeriodicBCOptions(method, degree,bool(addvertex)) 

 # Deformation gradient
microBC.setDeformationGradient(1.1,0.0,0.0,0.,1.,0.,0.,0.,1.)

mysolver.addMicroBC(microBC)

mysolver.setExtractCohesiveLawFromMicroDamage(True)
mysolver.setLocalizationNormal(1.,0.,0.)
mysolver.setRVELengthInNormalDirection(0.3)

#stress averaging flag and averaging method 0- VOLUME, 1- SURFACE
mysolver.stressAveragingFlag(True) # set stress averaging ON- 0 , OFF-1
mysolver.setStressAveragingMethod(0) # 0 -volume 1- surface
#tangent averaging flag
mysolver.tangentAveragingFlag(True) # set tangent averaging ON -0, OFF -1
mysolver.setTangentAveragingMethod(2,1e-6) # 0- perturbation 1- condensation

mysolver.options("-ksp_type preonly -pc_type lu -pc_factor_shift_type NONZERO -pc_factor_mat_solver_type petsc")

# build view
mysolver.internalPointBuildView("Green-Lagrange_xx",IPField.STRAIN_XX, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_yy",IPField.STRAIN_YY, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_zz",IPField.STRAIN_ZZ, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_xy",IPField.STRAIN_XY, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_yz",IPField.STRAIN_YZ, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_xz",IPField.STRAIN_XZ, 1, 1);

mysolver.internalPointBuildView("svm",IPField.SVM, 1, 1)
mysolver.internalPointBuildView("sig_xx",IPField.SIG_XX, 1, 1)
mysolver.internalPointBuildView("sig_yy",IPField.SIG_YY, 1, 1)
mysolver.internalPointBuildView("sig_zz",IPField.SIG_ZZ, 1, 1)
mysolver.internalPointBuildView("sig_xy",IPField.SIG_XY, 1, 1)
mysolver.internalPointBuildView("sig_yz",IPField.SIG_YZ, 1, 1)
mysolver.internalPointBuildView("sig_xz",IPField.SIG_XZ, 1, 1)
mysolver.internalPointBuildView("epl",IPField.PLASTICSTRAIN, 1, 1)
mysolver.internalPointBuildView("Damage",IPField.DAMAGE, 1, 1)
mysolver.internalPointBuildView("active Damage zone",IPField.ACTIVE_DISSIPATION, 1, 1)




# solve
mysolver.solve()

# test check
check = TestCheck()
check.equal(2.284228e+01,mysolver.getHomogenizedStress(0,0),1.e-4)
check.equal(-1.100243e+02,mysolver.getHomogenizedTangent(0,0,0,0),1.e-4)
check.equal(5.254545e-03,mysolver.getHomogenizedCohesiveJump(0),1.e-4)

