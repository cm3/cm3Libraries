import os,csv
#from gmshpy import *
#from dG3Dpy import*
import sys,subprocess

def checkEqual(ref, cur, tol):
  if(abs(ref-cur)>abs(tol*ref)):
    print("Error : reference  current value ",ref," current value ",cur," relative error ", (ref-cur)/ref," tolerance ",tol)

if sys.version_info[0] < 3:
  os.system('python taylorTetDGDyn.py')
else:
  os.system('python3 taylorTetDGDyn.py')
  #out=subprocess.Popen(["python3", "taylorTetDGDyn.py"], capture_output=False)
  #out.terminate()
data1 = csv.reader(open('force3comp2.csv'), delimiter=';')
force = list(data1)
checkEqual(8.846480e+03,float(force[-1][1]),1e-5)
data2 = csv.reader(open('NodalDisplacementPhysical5Num1comp0.csv'), delimiter=';')
disp = list(data2)
checkEqual(1.263813e-03,float(disp[-1][1]),1e-5)

if sys.version_info[0] < 3:
  os.system('python taylorTetDGDyn.py')
else:
  os.system('python3 taylorTetDGDyn.py')

data21 = csv.reader(open('force3comp2.csv'), delimiter=';')
force2 = list(data21)
checkEqual(8.890661e+03,float(force2[-1][1]),1e-5)
data22 = csv.reader(open('NodalDisplacementPhysical5Num1comp0.csv'), delimiter=';')
disp2 = list(data22)
checkEqual(1.263813e-03,float(disp2[-1][1]),1e-5)

