
import os
import pandas as pd
import numpy as np

def extractData(folder,f0,Lr0,Lz0,Rr0,Rz0,T):
  uz = pd.read_csv(os.path.join(folder, "NodalDisplacementPhysical3Num3comp2.csv"), sep=";", header=None)
  ur = pd.read_csv(os.path.join(folder, "NodalDisplacementPhysical3Num3comp0.csv"), sep=";", header=None)
  fz = pd.read_csv(os.path.join(folder, "force1comp2.csv"), sep=";", header=None)
  JV = pd.read_csv(os.path.join(folder, "VolumeIntegral_JACOBIAN.csv"), sep=";", header=None)
  Wpl =pd.read_csv(os.path.join(folder, "VolumeIntegral_PLASTIC_ENERGY.csv"), sep=";", header=None)
  uRz = pd.read_csv(os.path.join(folder, "NodalDisplacementPhysical5Num5comp2.csv"), sep=";", header=None)
  uRr = pd.read_csv(os.path.join(folder, "NodalDisplacementPhysical1Num1comp0.csv"), sep=";", header=None)
  
  Lz = Lz0+uz.values[:,1];
  Lr = Lr0+ur.values[:,1];

  Rz = Rz0+uRz.values[:,1];
  Rr = Rr0+uRr.values[:,1];

  Er = np.log(Lr/Lr0);
  Ez = np.log(Lz/Lz0);
  Ee = 2.*np.abs(Ez-Er)/3.;

  lamb = Lz/Lr; 
  W = Rz/Rr;
  Chi = Rr/Lr;

  sigZ = -fz.values[:,1]/(np.pi*Lr*Lr);
  sigR = (T-1./3.)/(T+2./3.)*sigZ;

  sigE = np.abs(sigZ-sigR);
  sigH = (2*sigR+sigZ)/3.;

  V0 = np.pi*Lr0*Lr0*Lz0;
  V = np.pi*Lr*Lr*Lz;

  f = (V-JV.values[:,1])/V;
  return sigE, sigH, f, lamb, W, Chi, Er, Ez, Ee, sigZ
