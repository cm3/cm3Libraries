unit = 1.;
W0 = 1.;
lambda0 = 1.;
f0 = 1e-3;
Chi0 = Exp((1./3.)*Log(1.5*f0*lambda0/W0));

Lr0 = 1*unit;
Lz0 = lambda0*Lr0;
Rr0 = Chi0*Lr0;
Rz0 = W0*Rr0;

f0 = 2*Rr0*Rr0*Rz0/(3*Lr0*Lr0*Lz0);
Printf("initial porosity = %e",f0);

//Mesh.RecombineAll = 1;

lsca = 0.07*unit;
Point(1) = {0,0,0,lsca};
Point(2) = {Rr0,0,0,0.05*lsca};
Point(3) = {Lr0,0,0,0.3*lsca};
Point(4) = {Lr0,0,Lz0,lsca};
Point(5) = {0,0,Lz0,lsca};
Point(6) = {0,0,Rz0,lsca*0.5};

z7 = 2*Rz0;
Point(7) = {0,0,z7,lsca};

x8 = 2*Rr0;
Point(8) = {x8,0,0,lsca};

z9 = Sqrt(0.5)*Rz0;
x9 = Rr0*Sqrt(1.-z9*z9/Rz0/Rz0);
Point(9) = {x9,0,z9,lsca*0.05};

x10 = 2*Rr0-0.4*Rr0;
z10 = z7 - 0.4*Rz0;
Point(10) = {x10,0,z10,lsca*0.05};

x11 = 2*Rr0;
Point(11) = {x11,0,Lz0,lsca*0.05};

z12 = z7;
Point(12) = {Lr0,0,z12,lsca*0.05};


//+
Ellipse(1) = {2, 1, 2, 9};
Ellipse(2) = {9, 1, 6, 6};

//+
Line(3) = {6, 7};
//+
Line(4) = {7, 5};
//+
Line(5) = {5, 11};
//+
Line(6) = {11, 4};
//+
Line(7) = {4, 12};
//+
Line(8) = {12, 3};
//+
Line(9) = {3, 8};
//+
Line(10) = {8, 2};
//+
Line(11) = {12, 10};
//+
Line(12) = {10, 11};
//+
Line(13) = {10, 7};
//+
Line(14) = {10, 9};
//+
Line(15) = {10, 8};
//+
Line Loop(1) = {6, 7, 11, 12};
//+
Plane Surface(1) = {1};
//+
Line Loop(2) = {12, -5, -4, -13};
//+
Plane Surface(2) = {2};
//+
Line Loop(3) = {3, -13, 14, 2};
//+
Plane Surface(3) = {3};
//+
Line Loop(4) = {10, 1, -14, 15};
//+
Plane Surface(4) = {4};
//+
Line Loop(5) = {15, -9, -8, 11};
//+
Plane Surface(5) = {5};
//+
Physical Surface(11) = {2, 1, 5, 4, 3};
//+
Physical Line(1) = {10, 9};
//+
Physical Line(2) = {8, 7};
//+
Physical Line(3) = {6, 5};
//+
Physical Line(4) = {4, 3};
//+
Physical Point(1) = {2};
//+
Physical Point(2) = {3};
//+
Physical Point(3) = {4};
//+
Physical Point(4) = {5};
//+
Physical Point(5) = {6};
//+
Physical Point(6) = {9};
//+
Transfinite Line {6, 11, 9} = 20 Using Progression 1;
//+
Transfinite Line {7, 12, 4} = 11 Using Progression 1;
//+
Transfinite Line {5, 13, 2} = 11 Using Progression 1.;
//+
Transfinite Line {-8, -15, 1} = 16 Using Progression 1.08;
//+
Transfinite Line {-10, -14, 3} = 11 Using Progression 1.04;
//+
Transfinite Surface {2, 1, 5, 4, 3};
Recombine Surface{2, 1, 5, 4, 3};
