import os
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from extractData import *

plt.rcParams.update({'font.size': 16})


unit = 1.;
W0 = 1.;
lambda0 = 1.;
f0 = 1e-3;
Chi0 = np.exp((1./3.)*np.log(1.5*f0*lambda0/W0));

Lr0 = 1*unit;
Lz0 = lambda0*Lr0;
Rr0 = Chi0*Lr0;
Rz0 = W0*Rr0;

f0 = 2*Rr0*Rr0*Rz0/(3*Lr0*Lr0*Lz0)
print(f"initial porosity f0 = {f0}")


fig, axes = plt.subplots(1,2, figsize=(8,5))
plt.tight_layout()

for folder, ltpe in zip(["J2","J2-ani-impl","J2-newImp"],["r-","gs","b+--"]):
  sigE, sigH, f, lamb, W, Chi, Er, Ez, Ee, sigZ = extractData(folder,f0,Lr0,Lz0,Rr0,Rz0,T=2)
  axes[0].plot(Ee, sigE, ltpe, label=folder)
  axes[1].plot(Ee, f, ltpe,label=folder)
  
axes[0].set_xlabel(r"$E_{eq}$ [-]")
axes[0].set_ylabel(r"$\Sigma_{eq}$ (MPa)")
axes[1].set_xlabel(r"$E_{eq}$ [-]")
axes[1].set_ylabel(r"f [-]")

axes[0].legend(loc="lower center",framealpha=0)
axes[1].legend(framealpha=0)

fig, axes = plt.subplots(1,2, figsize=(8,5))
plt.tight_layout()

for folder, ltpe in zip(["old",".","porous"],["rx-","g+--","kv"]):
  sigE, sigH, f, lamb, W, Chi, Er, Ez, Ee, sigZ = extractData(folder,f0,Lr0,Lz0,Rr0,Rz0,T=2)
  axes[0].plot(Ee, sigE, ltpe, label=folder)
  axes[1].plot(Ee, f, ltpe,label=folder)
  
axes[0].set_xlabel(r"$E_{eq}$ [-]")
axes[0].set_ylabel(r"$\Sigma_{eq}$ (MPa)")
axes[1].set_xlabel(r"$E_{eq}$ [-]")
axes[1].set_ylabel(r"f [-]")

axes[0].legend(loc="lower center",framealpha=0)
axes[1].legend(framealpha=0)

plt.savefig("comparison.svg",bbox_inches="tight")
plt.show()
