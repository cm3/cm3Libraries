#### Data preprocess, from the basic simulation resulte to obtaint the 

import numpy as npy
import pickle
from matplotlib import pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import pdb
from matplotlib import cm
import matplotlib 

outfile1 = "FE_MATprop.csv"    # for two level computational homogenization
outfile2 = "MF_MATprop.csv"    # for stochastic MFH

###### 1. read data from file ###########
with open('./listC_mfh_sch2.dat','rb') as data:
    list_C = pickle.load(data)
    list_Cmfh2 = pickle.load(data)
    PropMat = pickle.load(data)
    Mat_Pmfh = pickle.load(data)
    MFH_Pram = pickle.load(data)
     
n_total = len(MFH_Pram)
####  2. Prepair random properties for sample of Lx*Ly ################### 
Lx = 1000.0
Ly = 250.0
lsve = 25.0
nx = npy.int(Lx/lsve)+2
ny = npy.int(Ly/lsve)+2

num = nx*ny

Rmat = npy.random.randint(0, n_total, num)

 ### set starting data as out put

############### first file 
fid = open(outfile1, "w")
fid.write("%i %i\n" % (nx, ny))
fid.write("%.9f %.9f\n" % (lsve*1.0e-6, lsve*1.0e-6))

l=0
for i in range(nx):
    for j in range(ny):
        fid.write("%f %f %f %f %f %f %f %f %f\n" % (PropMat[0][Rmat[l]],PropMat[1][Rmat[l]],PropMat[2][Rmat[l]],PropMat[3][Rmat[l]],PropMat[4][Rmat[l]],PropMat[5][Rmat[l]],PropMat[6][Rmat[l]],PropMat[7][Rmat[l]],PropMat[8][Rmat[l]]))
        l = l+1
fid.close()


############### second file 

fid = open(outfile2, "w")
fid.write("%i %i %i\n" % (nx, ny,1))
fid.write("%.9f %.9f %.9f\n" % (lsve*1.0e-6, lsve*1.0e-6, 1.0e-6))

l=0
for i in range(nx):
    for j in range(ny):
        fid.write("%f %f %f %f %f %f %f %f\n" % (PropMat[9][Rmat[l]],MFH_Pram[Rmat[l]][2],MFH_Pram[Rmat[l]][0],MFH_Pram[Rmat[l]][1],0.0,0.0,0.0,0.0))
        l = l+1
fid.close()



