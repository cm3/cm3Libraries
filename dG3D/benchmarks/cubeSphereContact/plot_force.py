import numpy as npy
import pickle

from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
from matplotlib import cm
import matplotlib
from mpl_toolkits.axes_grid1.inset_locator import zoomed_inset_axes
from mpl_toolkits.axes_grid1.inset_locator import mark_inset

unit = 1.e-3
Lx = 100 * unit
Ly = 100 * unit
Lz = 50 * unit
surf = Ly * Lz

fig, ax = plt.subplots()
# matplotlib.rc('xtick', labelsize=20)
# matplotlib.rc('ytick', labelsize=20)
plt.xticks(fontsize=20)
plt.yticks(fontsize=20)
strn = []
strs = []

strnexp = []
strnexp.append([[0.00, 0],
                [0.05, 20.1],
                [0.10, 40.5],
                [0.15, 63.92],
                [0.20, 84.32],
                [0.25, 104.4],
                [0.30, 124.4],
                [0.35, 144.32],
                [0.40, 164.33],
                [0.45, 184.17],
                [0.50, 203.79],
                [0.55, 223.13],
                [0.60, 242.08],
                [0.65, 259.08],
                [0.70, 276.76],
                [0.75, 293.93],
                [0.80, 311.26],
                [0.85, 329.6],
                [0.90, 347],
                [0.95, 364.74],
                [1.00, 382.73],
                [1.05, 400.33],
                [1.10, 418.],
                [1.15, 435.28],
                [1.20, 452.27],
                [1.25, 467.11],
                [1.30, 481.73],
                [1.35, 494.3],
                [1.40, 470],
                [1.45, 440]])

strnexp.append([[0.00, 0],
                [0.05, 20.4],
                [0.10, 41.1],
                [0.15, 61.61],
                [0.20, 82],
                [0.25, 102.29],
                [0.30, 122.44],
                [0.35, 142.63],
                [0.40, 162.67],
                [0.45, 182.67],
                [0.50, 202.12],
                [0.55, 221.32],
                [0.60, 239.83],
                [0.65, 256.98],
                [0.70, 275.39],
                [0.75, 294.53],
                [0.80, 312.78],
                [0.85, 331.32],
                [0.90, 348.99],
                [0.95, 367.56],
                [1.00, 385.86],
                [1.05, 403.97],
                [1.10, 421.36],
                [1.15, 439.12],
                [1.20, 456.6],
                [1.25, 473.7],
                [1.28, 482.4],
                [1.35, 399]])

strnexp.append([[0.00, 0],
                [0.05, 20.4],
                [0.10, 40.8],
                [0.15, 61.36],
                [0.20, 81.44],
                [0.25, 101.58],
                [0.30, 121.47],
                [0.35, 141.327],
                [0.40, 160.99],
                [0.45, 180.75],
                [0.50, 201.82],
                [0.55, 220.94],
                [0.60, 239.62],
                [0.65, 257.83],
                [0.70, 277.09],
                [0.75, 295.29],
                [0.80, 313.12],
                [0.85, 333.6],
                [0.90, 351.99],
                [0.95, 369.65],
                [1.00, 387.36],
                [1.05, 405.44],
                [1.10, 422.83],
                [1.15, 439.44],
                [1.20, 454.84],
                [1.24, 461],
                [1.30, 450]])

filename = './force87comp2.csv'
dispname = './NodalDisplacementPhysical87Num87comp2.csv'

try:
    with open(filename, 'r') as data:
        B_string = data.read()
except:
    print('Force file not found')
else:
    B_string = B_string.split('\n')
    force = []
    for i in range(len(B_string) - 1):
        row = B_string[i]
        row = row.split(';')
        force.append(float(row[1]))
    stress = []
    for i in range(len(force)):
        if (force[i] >= 0.0):
            stress.append(force[i] / surf / 1.e6)
    strs.append(stress)

try:
    with open(dispname, 'r') as data:
        A_string = data.read()
except:
    print('Displacement file not found')
else:
    A_string = A_string.split('\n')
    disp = []
    for i in range(len(A_string) - 1):
        row = A_string[i]
        row = row.split(';')
        disp.append(float(row[1]))
    strain = []
    for i in range(len(disp)):
        if (disp[i] < 0.0):
            strain.append(disp[i] / Lx)
    strn.append(strain)

totalStress = []
#print(strs)
#print(strn)
for j in range(len(strs[0])):
    val = 0.
    for i in range(len(strs)):
        val = val + strs[i][j]
    totalStress.append(float(val))

plt.ylim([0.0, 50.0])
plt.xlim([-0.1, -0.001])
plt.locator_params(axis='x', nbins=5)
# plt.plot(x,y,'mx', markeredgewidth =3.0,markersize= 13)

# handles, labels = ax.get_legend_handles_labels()
# ax.legend(handles, labels)

plt.legend(fontsize=22, framealpha=0.0)
plt.xlabel('$\\frac{\Delta x}{L_0}$', fontsize=22)
plt.ylabel('$\\frac{F}{S_0}$ in MPa', fontsize=22)

print(max(strn[0]))
print(max(totalStress))

line1, = plt.plot(strn[0], totalStress, linewidth=1.0, color='blue', label="Simulation")
#for j in range(len(strnexp)):
#    if (j == 0):
#        line2, = plt.plot([strnexp[j][i][0] / 100. for i in range(len(strnexp[j]))],
#                          [strnexp[j][i][1] for i in range(len(strnexp[j]))], linewidth=0.50, color='black',
#                          label="Experimental")
#    else:
#        line2, = plt.plot([strnexp[j][i][0] / 100. for i in range(len(strnexp[j]))],
#                          [strnexp[j][i][1] for i in range(len(strnexp[j]))], linewidth=0.50, color='black')
# plt.annotate('Conf. #1', xy=(0.00227, 583), xytext=(0.0015, 650),
#             arrowprops=dict(arrowstyle='->'),fontsize=22)
# plt.annotate('Conf. #2', xy=(0.0029, 653), xytext=(0.0020, 700),
#             arrowprops=dict(arrowstyle='->'),fontsize=22)

plt.yticks(visible=True)
plt.xticks(visible=True)
# legend([line1,line2], ['Simulation', 'Experimental'])
plt.legend(loc='lower right', prop={'size': 22})

plt.savefig('NotchedForce.pdf', bbox_inches='tight')
plt.show()
