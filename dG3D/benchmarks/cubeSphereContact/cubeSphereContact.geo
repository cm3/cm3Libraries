// GEOMETRY AND MESH PARAMETERS
lays = 5; // number of layers of the composite (must be an integer, in z direction)
mm=1e-3; // unit for geometry
Lx = 100*mm; // dimension in x direction
Ly = 100*mm; // dimension in y direction
Lz = 50*mm; // dimension in z direction
R = 15*mm; // sphere radius
dis = 0.1*mm; // initial contact distance 
slc = 10*mm; // cube characteristic length
sls = 10*mm; // sphere characteristic length
cT = 10; // panel transfinite line value (control nodes number by line)
sT = 3; // sphere transfinite line value (control nodes number by line)
//
//////////////////////////
//    CompositeCube	//
//////////////////////////
// GEOMETRY
Point(1)={-Lx/2,-Ly/2,0,slc};
Point(2)={-Lx/2,Ly/2,0,slc};
Point(3)={Lx/2,Ly/2,0,slc};
Point(4)={Lx/2,-Ly/2,0,slc};

Line(1)={1,2};
Line(2)={2,3};
Line(3)={3,4};
Line(4)={4,1};

// surfaces
Line Loop(1)={1,2,3,4};
Plane Surface(1)={1};

// replace unstructured grid by structured grid (with controlled nodes number by line)
Transfinite Line {1,2,3,4} = cT Using Progression 1;
// transfinite surface guarantee a structured grid on a surface
Transfinite Surface {1};

// replace tri/tetra by quad/hexa elements
Recombine Surface{1};

// extrusion of surfaces along z direction
my_plate[]=Extrude {0, 0, -Lz} {
  Surface{1};
  Layers{lays};
  Recombine; 
};

// ? transfinite volume is used to define the ordering of the nodes/elements in the mesh ?
// ? transfinite volume not needed when using extrusion ? (no effet)
// Transfinite Volume { my_plate[] };

// PHYSICAL GROUPS
Physical Surface(81) = {13};
Physical Surface(82) = {17};
Physical Surface(83) = {21};
Physical Surface(84) = {25};
Physical Volume(85) = { my_plate[] };
Physical Surface(10) = {1};


//
//////////////////////////
//      RigidSphere  	//
//////////////////////////
// GEOMETRY
Point(41) = {0,0,R+dis,sls};
Point(42) = {R,0,R+dis,sls};
Point(43) = {-R,0,R+dis,sls};
Point(44) = {0,R,R+dis,sls};
Point(45) = {0,-R,R+dis,sls};
Point(46) = {0,0,2*R+dis,sls};
Point(47) = {0,0,dis,sls};

Circle(51) = {42,41,44};
Circle(52) = {44,41,43};
Circle(53) = {43,41,45};
Circle(54) = {45,41,42};
Circle(55) = {42,41,46};
Circle(56) = {46,41,43};
Circle(57) = {43,41,47};
Circle(58) = {47,41,42};
Circle(59) = {44,41,46};
Circle(60) = {46,41,45};
Circle(61) = {45,41,47};
Circle(62) ={47,41,44};

//+ surfaces
Curve Loop(3) = {51, -62, 58};
//+
Surface(73) = {3};
//+
Curve Loop(4) = {51, 59, -55};
//+
Surface(74) = {4};
//+
Curve Loop(5) = {52, -56, -59};
//+
Surface(75) = {5};
//+
Curve Loop(6) = {52, 57, 62};
//+
Surface(76) = {6};
//+
Curve Loop(7) = {53, 61, -57};
//+
Surface(77) = {7};
//+
Curve Loop(8) = {53, -60, 56};
//+
Surface(78) = {8};
//+
Curve Loop(9) = {54, 55, 60};
//+
Surface(79) = {9};
//+
Curve Loop(10) = {54, -58, -61};
//+
Surface(80) = {10};
//+
Surface Loop(1) = {78, 77, 80, 79, 74, 73, 76, 75};
//+ volume
Volume(3) = {1};

//replace unstructured grid by structured grid (with controlled nodes number by line)
Transfinite Line {51,52,53,54,55,56,57,58,59,60,61,62} = sT Using Progression 1;

// transfinite surface guarantee a structured grid on a surface (not obvious for a sphere, especially when sT is small)
Transfinite Surface {73,74,75,76,77,78,79,80};
// transfinite algorithm only available for 5- and 6-face volumes
// Transfinite Volume {3};

// replace tri/tetra by quad/hexa elements (? seems to be difficult for a sphere, especially when sT is small ?)
//Recombine Surface {73,74,75,76,77,78,79,80};
// ? seems not available for a sphere ?
// Recombine Volume {3};

// PHYSICAL GROUPS
Physical Point(86) ={41}; // sphere center
Physical Surface(87) ={78, 77, 80, 79, 74, 73, 76, 75}; // sphere surface


