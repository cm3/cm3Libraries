#coding-Utf-8-*-
import os,csv
from gmshpy import *
from dG3Dpy import*
#from dG3DpyDebug import*

#script to launch PBC problem with a python script 
#(! radius need to be the same value as in .geo !)

# parameters for material law
lawnum = 11 # unique law number for the slave
rho = 100   # Bulk mass for the slave (density in kg/m3)
E = 3.e9    # Young's modulus for the slave (Pa)
nu= 0.4     # Poisson ratio for the slave
sy0 = 120e6 # initial yield stress for the slave (Pa)
h = E/10.   # parameter in law LinearExponentialJ2IsotropicHardening: R = yield0 +h1 * p + h2* (1- exp(-hexp*p))

# creation of material law
law1 = J2LinearDG3DMaterialLaw(lawnum,rho,E,nu,sy0,h)

# geometry & mesh
geofile="cubeSphereContact.geo"  # name of the geometry with physical groups
meshfile="cubeSphereContact.msh" # name of mesh file (to save directly in gmsh shift+ctrl+s)

# parameters for part domain
nfield = 85 # unique physical number of field (defined in gmsh)
#myfield1 = FSDomain(1000,nfield,lawnum,3) # (?)
fullDg = 0  # False = CG, True = DG inside a domain
beta1 = 1e2 # stability/penality parameter for displacement when DG

# creation of part domain
myfield1 = dG3DDomain(1000,nfield,0,lawnum,fullDg,3) # (tag=1000: tag number for slave domains with no linked ddls; ws=0: functionSpaceType::Lagrange; dim=3)
myfield1.stabilityParameters(beta1)

# parameters for solver
sol = 2          # Gmm=0 (default) Taucs=1 PETsc=2 (?)
soltype = 1      # StaticLinear=0 (default) StaticNonLinear=1
nstep = 1500     # number of step (used only if soltype=1)
ftime =1.        # Final time (used only if soltype=1)
tol=1.e-4        # relative tolerance for NR scheme (used only if soltype=1)
tolAbs = 1e-12
nstepArch=15     # Number of step between 2 archiving (used only if soltype=1)

# creation of solver
mysolver = nonLinearMechSolver(1000) # tag=1000
mysolver.createModel(geofile,meshfile,3,2)
mysolver.addDomain(myfield1)
mysolver.addMaterialLaw(law1)

mysolver.Scheme(soltype)
mysolver.Solver(sol)
mysolver.snlData(nstep,ftime,tol,tolAbs)
mysolver.stepBetweenArchiving(nstepArch)

# BC panel
mysolver.displacementBC("Face",81,0,0.) # zero displacement in x direction for Physical Surface(81)
mysolver.displacementBC("Face",81,1,0.) # zero displacement in y direction for Physical Surface(81)
mysolver.displacementBC("Face",81,2,0.) # zero displacement in z direction for Physical Surface(81)
mysolver.displacementBC("Face",82,0,0.)
mysolver.displacementBC("Face",82,1,0.)
mysolver.displacementBC("Face",82,2,0.)
mysolver.displacementBC("Face",83,0,0.)
mysolver.displacementBC("Face",83,1,0.)
mysolver.displacementBC("Face",83,2,0.)
mysolver.displacementBC("Face",84,0,0.)
mysolver.displacementBC("Face",84,1,0.)
mysolver.displacementBC("Face",84,2,0.)

# parameters for contact definition
radius=15.e-3           # ! same value as in .geo !
penalty=1.e10
rhocontact=7800.
sphereDispl = -10.e-3
contactDom1 = 1000      # (tag number for master domain)

# creation of contact definition
flaw = CoulombFrictionLaw(2,0.,0.,penalty,penalty/1000.) # (normal to the contact, static friction coeff, dynamic friction coeff)
contact1 = dG3DRigidSphereContactDomain(contactDom1, 2, 87, 3, nfield, 86,radius,penalty,rhocontact)
contact1.setFriction(bool(0))
contact1.addFrictionLaw(flaw)
mysolver.contactInteraction(contact1)

# BC sphere
mysolver.displacementRigidContactBC(87,0,0.)
mysolver.displacementRigidContactBC(87,1,0.)
mysolver.displacementRigidContactBC(87,2,sphereDispl)
#mysolver.displacementRigidContactBC(86,0,0.)
#mysolver.displacementRigidContactBC(86,1,0.)
#mysolver.displacementRigidContactBC(86,2,sphereDispl)

#
## Outputs
#mysolver.archivingNodeDisplacement(1,0,nstepArch)
#mysolver.archivingNodeDisplacement(2,0,nstepArch)
#mysolver.archivingNodeDisplacement(1,1,nstepArch)
#mysolver.archivingNodeDisplacement(2,1,nstepArch)
mysolver.internalPointBuildView("Green-Lagrange_xx",IPField.STRAIN_XX, 1, 1)
mysolver.internalPointBuildView("Green-Lagrange_yy",IPField.STRAIN_YY, 1, 1)
mysolver.internalPointBuildView("Green-Lagrange_zz",IPField.STRAIN_ZZ, 1, 1)
mysolver.internalPointBuildView("Green-Lagrange_xy",IPField.STRAIN_XY, 1, 1)
mysolver.internalPointBuildView("Green-Lagrange_yz",IPField.STRAIN_YZ, 1, 1)
mysolver.internalPointBuildView("Green-Lagrange_xz",IPField.STRAIN_XZ, 1, 1)
mysolver.internalPointBuildView("sig_xx",IPField.SIG_XX, 1, 1)
mysolver.internalPointBuildView("sig_yy",IPField.SIG_YY, 1, 1)
mysolver.internalPointBuildView("sig_zz",IPField.SIG_ZZ, 1, 1)
mysolver.internalPointBuildView("sig_xy",IPField.SIG_XY, 1, 1)
mysolver.internalPointBuildView("sig_yz",IPField.SIG_YZ, 1, 1)
mysolver.internalPointBuildView("sig_xz",IPField.SIG_XZ, 1, 1)
mysolver.internalPointBuildView("sig_VM",IPField.SVM, 1, 1)
mysolver.internalPointBuildView("Green-Lagrange equivalent strain",IPField.GL_EQUIVALENT_STRAIN, 1, 1)
mysolver.internalPointBuildView("Equivalent plastic strain",IPField.PLASTICSTRAIN, 1, 1)

mysolver.archivingRigidContact(87,2,0,nstepArch)
mysolver.archivingNodeDisplacement(86,2,nstepArch)
mysolver.archivingRigidContactForce(87,2,nstepArch)
mysolver.archivingForceOnPhysicalGroup("Face",10,2,nstepArch)


mysolver.solve()

check = TestCheck()
dataF = csv.reader(open('force87comp2.csv'), delimiter=';')
force = list(dataF)
check.equal(2.090191e+05,float(force[-1][1]),1e-5)


