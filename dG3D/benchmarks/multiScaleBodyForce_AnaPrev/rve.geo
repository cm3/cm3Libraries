//Mesh.SecondOrderLinear = 1;  
l1 = 1.2;
l2 = l1/2.5;
L = 9.97;
R= 8.67/2.0;
r1 = R/ratio1;
r2 = R*ratio1;
err = 0.0*r1;
e = 0.2*l2;
ox=0;
oy=0;
// exterior cube
Point(1) = {-L,-L,0,l1};
Point(2) = {L,-L,0,l1};
Point(3) = {L,L,0,l1};
Point(4) = {-L,L,0,l1};

Line(1) = {1,2};
Line(2) = {2,3};
Line(3) = {3,4};
Line(4) = {4,1};
Line Loop(5) = {1,2,3,4};

// interior sphere
Point(101) = {-L/2,-L/2,0,l2};
Point(102) = {-L/2+r2,-L/2,0,l2};
Point(103) = {-L/2, -L/2+r1,0,l2};
Point(104) = {-L/2-r2,-L/2,0,l2};
Point(105) = {-L/2,-L/2-r1,0,l2};
Ellipse(29) = {102,101,103};
Ellipse(30) = {104,101,103};
Ellipse(31) = {104,101,105};
Ellipse(32) = {102,101,105};

Rotate{{0,0,1}, {-L/2,-L/2,0}, -Pi/2}{ Duplicata{Line{29,30,31,32};}}
Translate {err, L,0.0}{Line{33,34,35,36};}


//other two holes
r1 = R*ratio2;
r2 = R/ratio2;
Point(201) = {L/2,-L/2,0,l};
Point(202) = {L/2+r2,-L/2,0,l};
Point(203) = {L/2, -L/2+r1,0,l};
Point(204) = {L/2-r2,-L/2,0,l};
Point(205) = {L/2,-L/2-r1,0,l};
Ellipse(37) = {203,201,202};
Ellipse(38) = {203,201,204};
Ellipse(39) = {205,201,204};
Ellipse(40) = {205,201,202};

Rotate{{0,0,1}, {L/2,-L/2,0}, -Pi/2}{ Duplicata{Line{37,38,39,40};}}
Translate {0.0, L, 0.0}{Line{41,42,43,44};}

Line Loop(101) = {29,-30,31,-32};
Line Loop(102) = {33,-34,35,-36};
Line Loop(103) = {37,-38,39,-40};
Line Loop(104) = {41,-42,43,-44};

Plane Surface(100) ={5,101,102,103,104};

Physical Surface(11) ={100};

Physical Line(1) = {1};
Physical Line(2) = {2};
Physical Line(3) = {3};
Physical Line(4) = {4};

Recombine Surface {100};


