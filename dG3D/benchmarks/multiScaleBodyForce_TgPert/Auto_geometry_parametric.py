import numpy as np

import sys
import os

GeoFile = "rve.geo" 
for i in range(200):
  mesh= "rve"+str(i)+".msh"
  ratio1 = 1.002 + 0.005*np.random.uniform()
  ratio2 = 1.001 + 0.005*np.random.uniform()

  HeadLine="""SetFactory("OpenCASCADE");
  Geometry.AutoCoherence=0.0001;
  ratio1 = """+str(ratio1)+""";
  ratio2 = """+str(ratio2)+""";\n"""

  f = open(GeoFile, "r")
  contents = f.readlines()
  f.close()
  contents.insert(0, HeadLine)

  GeoFile_i = "rve"+str(1)+".geo" 
  f = open(GeoFile_i, "w")
  contents = "".join(contents)
  f.write(contents)
  f.close()
  os.system("gmsh -2 -order 2 "+GeoFile_i+" -o "+mesh)
    


