#coding-Utf-8-*-
from gmshpy import *
from dG3Dpy import*
#from dG3DpyDebug import*
#script to launch PBC problem with a python script

#DEFINE MICRO PROBLEM
E = 32.0
nu = 0.4
K = E/3./(1.-2.*nu)	# Bulk mudulus
mu =E/2./(1.+nu)	  # Shear mudulus
rho = 2.7e-9 # Bulk mass

bulkpert =1
interpert =1
perturbation = 1e-8

# creation of material law
appOrder = 11 # approximation order for exp and log operator of a tensor
potential1 = biLogarithmicElasticPotential(11,E,nu,appOrder)

law1 = dG3DHyperelasticMaterialLaw(11,rho,potential1)

# geometry
geofile="rve.geo" 

PlaneStress = True
# creation of part Domain
myfield1 = dG3DDomain(1000,11,0,11,0,2,0,0,0,True,0)
myfield1.setPlaneStressState(PlaneStress)
#myfield1.matrixByPerturbation(bulkpert,interpert,interpert,perturbation)


microBC = nonLinearPeriodicBC(1000,2)
microBC.setOrder(2)
microBC.setBCPhysical(1,2,3,4)

# periodiodic BC

method = 5 # Periodic mesh = 0 Langrange interpolation = 1 Cubic spline interpolation =2
degree = 2 
addVertex = 1
microBC.setPeriodicBCOptions(method, degree,bool(addVertex))

# DEFINE MACROPROBLEM
matnum = 1;
macromat = hoDGMultiscaleMaterialLaw(matnum , 1000)

macromat.setViewAllMicroProblems(bool(1),0)
#macromat.loadAllRVEMeshes("rve")



microSolver = macromat.getMicroSolver()
#microSolver.createMicroModel(geofile, 2,2)
microSolver.loadModel("rve1.msh");
microSolver.addDomain(myfield1)
microSolver.addMaterialLaw(law1);
microSolver.addMicroBC(microBC)

#microSolver.pathFollowing(True,0)
#microSolver.setPathFollowingControlType(1)
#microSolver.setPathFollowingArcLengthStep(1e-4)

microSolver.snlData(1,1.,1e-4,1e-5)
microSolver.setSystemType(1) 
microSolver.Solver(2)
microSolver.Scheme(1)
microSolver.setSameStateCriterion(1e-16)
microSolver.stressAveragingFlag(bool(1)) 
microSolver.setStressAveragingMethod(0)
microSolver.tangentAveragingFlag(bool(1))
microSolver.setTangentAveragingMethod(0,1e-4)

microSolver.stiffnessModification(bool(1));
microSolver.iterativeProcedure(bool(1));

MaxIter=25
StepIncrease= 3
StepReducFactor=2.
NumberReduction=10

microSolver.snlManageTimeStep(MaxIter,StepIncrease,StepReducFactor,NumberReduction)

#-------------------------------------------------------------
macromeshfile="Sample6_24.msh" # name of mesh file

# solver
sol = 2  # Gmm=0 (default) Taucs=1 PETsc=2
soltype =1 # StaticLinear=0 (default) StaticNonLinear=1
nstep = 50  # number of step (used only if soltype=1)
ftime =1.   # Final time (used only if soltype=1)
tol=1.e-4  # relative tolerance for NR scheme (used only if soltype=1)
nstepArch=3 # Number of step between 2 archiving (used only if soltype=1)


# creation of  macro part Domain
fulldg= 0
beta1  = 400.

nfield1 = 51 # number of the field (physical number of entity)
dim =2
macrodomain1 =hoDGDomain(1000,nfield1,0,matnum,fulldg,2)
macrodomain1.stabilityParameters(beta1)
#macrodomain1.matrixByPerturbation(bulkpert,interpert,interpert,perturbation)
macrodomain1.distributeOnRootRank(1)

# creation of Solver
mysolver = nonLinearMechSolver(1000)
mysolver.loadModel(macromeshfile)
mysolver.addDomain(macrodomain1)

#mysolver.addDomain(iterdomain1)
mysolver.addMaterialLaw(macromat)
#mysolver.setMultiscaleFlag(bool(1))
mysolver.Scheme(soltype)
mysolver.Solver(sol)
mysolver.snlData(nstep,ftime,tol,tol/10.)

#mysolver.pathFollowing(True,0)
#mysolver.setPathFollowingControlType(1)
#mysolver.setPathFollowingArcLengthStep(1e-3)
#mysolver.setPathFollowingCorrectionMethod(2)

MaxIter=25
StepIncrease= 2
StepReducFactor=2.
NumberReduction=10

mysolver.snlManageTimeStep(MaxIter,StepIncrease,StepReducFactor,NumberReduction)



L = 9.97
dis = 5 #(14.0*L-120)/2.0

'''d1 = 0.17*dis
d2 = 0.185*dis
d3 = 0.2305*dis
d4 = 0.5*dis
t1 = 0.035
t2 = 0.051
t3 = 0.2
t4 = 0.57'''



#cyclicFunction1=cycleFunctionTime(0.0, 0., t1,d1, t2,d2, t3,d3, t4,d4, ftime,dis)
#cyclicFunction3=cycleFunctionTime(0.0, 0., t1,-d1, t2,-d2, t3,-d3, t4,-d4, ftime,-dis)
    
mysolver.displacementBC("Face",51,2,0.)
mysolver.displacementBC("Edge",1,0,0)
mysolver.displacementBC("Edge",3,0,0)
mysolver.displacementBC("Edge",1,1,dis)
#mysolver.displacementBC("Edge",1,1,0)
mysolver.displacementBC("Edge",3,1,-dis)
#mysolver.constraintBC("Edge",3,1)
#mysolver.forceBC("Edge",3,1,-0.6e1) #-0.14e2)

# archivage
mysolver.internalPointBuildView("epsilon_xx",IPField.STRAIN_XX, 1, 1);
mysolver.internalPointBuildView("epsilon_yy",IPField.STRAIN_YY, 1, 1);
mysolver.internalPointBuildView("epsilon_xy",IPField.STRAIN_XY, 1, 1);
mysolver.internalPointBuildView("sig_xx",IPField.SIG_XX, 1, 1);
mysolver.internalPointBuildView("sig_yy",IPField.SIG_YY, 1, 1);
mysolver.internalPointBuildView("sig_xy",IPField.SIG_XY, 1, 1);
mysolver.internalPointBuildView("sig_VM",IPField.SVM, 1, 1);


mysolver.internalPointBuildView("High-order strain norm",IPField.HIGHSTRAIN_NORM, 1, 1);
mysolver.internalPointBuildView("High-order stress norm",IPField.HIGHSTRESS_NORM, 1, 1);
mysolver.internalPointBuildView("Green -Lagrange strain norm",IPField.GL_NORM, 1, 1);
mysolver.internalPointBuildView("PK2 stress norm",IPField.STRESS_NORM, 1, 1);

mysolver.archivingForceOnPhysicalGroup("Edge",3,1)
mysolver.archivingNodeDisplacement(5,1,1)
# solve
mysolver.solve()

# test check
try:
      import linecache
      total_line_number = sum(1 for line in open('force3comp1_part0.csv'))
      linedisp = linecache.getline('force3comp1_part0.csv',total_line_number)
except:
      print('Cannot read the results in the files')
      import os
      os._exit(1)
else:
      check = TestCheck()
      check.equal(-1.257469e+01,float(linedisp.split(';')[1]))



