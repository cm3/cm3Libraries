#coding-Utf-8-*-
from gmshpy import *
from dG3Dpy import*

#script to launch PBC problem with a python script

#DEFINE MICRO PROBLEM

# micro-material law

lawnum1 = 11
Em = 75.e9 #MPa
num = 0.33
sy0m = 300.e116
Hm = 150.e6;

alpm = 2.36e-5# thermal expansion
kappam =247. #thermal conductivity
rhom = 2700.
cpm = rhom*9.e2
TQfm = 0.9   # convert plastic to heat

Tr = 293. # reference temperature
'''
law1 =  FullJ2ThermoMechanicsDG3DMaterialLaw(lawnum1,Em,num,rhom,sy0m,Hm,1e-6,0,1e-8)
law1.setStrainOrder(1)
law1.setReferenceTemperature(Tr)
law1.setReferenceThermalExpansionCoefficient(alpm)
law1.setReferenceThermalConductivity(kappam)
law1.setReferenceCp(cpm)
law1.setTaylorQuineyFactor(TQfm)
'''

law1 = LinearThermoMechanicsDG3DMaterialLaw(lawnum1,rhom,Em,Em,Em,num,num,num,Em/(2.*(1.+num)),Em/(2.*(1.+num)),Em/(2.*(1.+num)),
			0.,0.,0., Tr, kappam, kappam, kappam, alpm,  alpm, alpm,cpm)


lawnum2 = 12
Ef = 385.e9 #MPa
nuf = 0.2
sy0f = 300.e100
Hf = 150.e100;

alpf = 5e-6# thermal expansion
kappaf =38. #thermal conductivity
rhof = 2600.
cpf = rhof*1.3e3
TQff = 0.   # convert plastic to heat
'''
law2 =  FullJ2ThermoMechanicsDG3DMaterialLaw(lawnum2,Ef,nuf,rhof,sy0f,Hf,1e-6,0,1e-8)
law2.setStrainOrder(1)
law2.setReferenceTemperature(Tr)
law2.setReferenceThermalExpansionCoefficient(alpf)
law2.setReferenceThermalConductivity(kappaf)
law2.setReferenceCp(cpf)
law2.setTaylorQuineyFactor(TQff)
'''
law2 = LinearThermoMechanicsDG3DMaterialLaw(lawnum2,rhof,Ef,Ef,Ef,nuf,nuf,nuf,Ef/(2.*(1.+nuf)),Ef/(2.*(1.+nuf)),Ef/(2.*(1.+nuf)),
			0.,0.,0., Tr, kappaf, kappaf, kappaf, alpf,  alpf, alpf,cpf)


# geometry
micromeshfile="micro.msh" # name of mesh file

# creation of part Domain
ExtraEqRatio = 1.
myfield1 = ThermoMechanicsDG3DDomain(1000,11,0,lawnum1,0,ExtraEqRatio,2)
myfield1.setConstitutiveExtraDofDiffusionAccountSource(False,False)
myfield2 = ThermoMechanicsDG3DDomain(1000,12,0,lawnum2,0,ExtraEqRatio,2)
myfield2.setConstitutiveExtraDofDiffusionAccountSource(False,False)

microBC = nonLinearPeriodicBC(1000,2)
microBC.setOrder(1)
microBC.setBCPhysical(1,2,3,4)
microBC.setDofPartition(3,1,0)
microBC.setPeriodicBCOptions(0, 5,False)
microBC.setInitialConstitutiveExtraDofDiffusionValue(0,Tr); #intial value for conExtraDof

# DEFINE MACROPROBLEM
matnum = 100;
macromat = MultiscaleThermoMechanicsDG3DMaterialLaw(matnum, 1000)
#macromat.setViewAllMicroProblems(bool(1),0)
#macromat.addViewMicroSolver(36,0)
#macromat.addViewMicroSolver(84,0)

baseSolver1 = macromat.getMicroSolver()
baseSolver1.loadModel(micromeshfile);
baseSolver1.addDomain(myfield1)
baseSolver1.addMaterialLaw(law1);
baseSolver1.addDomain(myfield2)
baseSolver1.addMaterialLaw(law2);
baseSolver1.addMicroBC(microBC)
baseSolver1.snlData(1,1.,1e-6,1e-16)
baseSolver1.setSystemType(3)
baseSolver1.Solver(2)
baseSolver1.Scheme(1)
baseSolver1.stressAveragingFlag(True)
baseSolver1.tangentAveragingFlag(True)
baseSolver1.setStressAveragingMethod(0);
baseSolver1.setTangentAveragingMethod(2,1e-6);
baseSolver1.initialBC("Face","Position",11,3,Tr)
baseSolver1.initialBC("Face","Position",12,3,Tr)
baseSolver1.displacementBC("Face",11,2,0.)
baseSolver1.displacementBC("Face",12,2,0.)
baseSolver1.internalPointBuildView("Green-Lagrange_xx",IPField.STRAIN_XX);
baseSolver1.internalPointBuildView("Green-Lagrange_yy",IPField.STRAIN_YY);
baseSolver1.internalPointBuildView("Green-Lagrange_xy",IPField.STRAIN_XY);
baseSolver1.internalPointBuildView("sig_xx",IPField.SIG_XX);
baseSolver1.internalPointBuildView("sig_yy",IPField.SIG_YY);
baseSolver1.internalPointBuildView("sig_xy",IPField.SIG_XY);
baseSolver1.internalPointBuildView("sig_VM",IPField.SVM);
baseSolver1.internalPointBuildView("Equivalent plastic strain",IPField.PLASTICSTRAIN);
baseSolver1.internalPointBuildView("temperature",IPField.TEMPERATURE)
baseSolver1.internalPointBuildView("qx",IPField.THERMALFLUX_X)
baseSolver1.internalPointBuildView("qy",IPField.THERMALFLUX_Y)
baseSolver1.internalPointBuildView("qz",IPField.THERMALFLUX_Z)


macromeshfile="macro.msh" # name of mesh file

# solver
sol = 2  # Gmm=0 (default) Taucs=1 PETsc=2
soltype =1 # StaticLinear=0 (default) StaticNonLinear=1
nstep = 1  # number of step (used only if soltype=1)
ftime =10.   # Final time (used only if soltype=1)
tol=1.e-6 # relative tolerance for NR scheme (used only if soltype=1)
nstepArch=1 # Number of step between 2 archiving (used only if soltype=1)


# creation of  macro part Domain
macroExtraEqRatio=1.e6
macrodomain = ThermoMechanicsDG3DDomain(1000,11,0,matnum,0,macroExtraEqRatio,2)
macrodomain.setConstitutiveExtraDofDiffusionAccountSource(False,False)
#macrodomain.distributeOnRootRank(0)
#macrodomain.gaussIntegration(4)

# creation of Solver
mysolver = nonLinearMechSolver(1000)
mysolver.loadModel(macromeshfile)
mysolver.addDomain(macrodomain)
mysolver.addMaterialLaw(macromat)
mysolver.Scheme(soltype)
mysolver.Solver(sol)
mysolver.snlData(nstep,ftime,tol)
mysolver.setMultiscaleFlag(True)

# boundary condition
mysolver.displacementBC("Face",11,2,0.0)
mysolver.displacementBC("Face",12,2,0.0)

mysolver.displacementBC("Edge",7,0,0.0)
mysolver.displacementBC("Edge",7,1,0.0)
mysolver.displacementBC("Edge",8,0,0.0)
mysolver.displacementBC("Edge",8,1,-10e-4)

fct1 = LinearFunctionTime(0,Tr,ftime,Tr)
fct2 = LinearFunctionTime(0,Tr,ftime,Tr+100.)
mysolver.displacementBC('Edge',10,3,fct1)
mysolver.displacementBC('Edge',9,3,fct2)

mysolver.initialBC("Face","Position",11,3,Tr)


# archivage
mysolver.internalPointBuildView("Green-Lagrange_xx",IPField.STRAIN_XX, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_yy",IPField.STRAIN_YY, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_zz",IPField.STRAIN_ZZ, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_xy",IPField.STRAIN_XY, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_yz",IPField.STRAIN_YZ, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_xz",IPField.STRAIN_XZ, 1, 1);
mysolver.internalPointBuildView("sig_xx",IPField.SIG_XX, 1, 1);
mysolver.internalPointBuildView("sig_yy",IPField.SIG_YY, 1, 1);
mysolver.internalPointBuildView("sig_zz",IPField.SIG_ZZ, 1, 1);
mysolver.internalPointBuildView("sig_xy",IPField.SIG_XY, 1, 1);
mysolver.internalPointBuildView("sig_yz",IPField.SIG_YZ, 1, 1);
mysolver.internalPointBuildView("sig_xz",IPField.SIG_XZ, 1, 1);
mysolver.internalPointBuildView("sig_VM",IPField.SVM, 1, 1);
mysolver.internalPointBuildView("temperature",IPField.TEMPERATURE, 1, 1)
mysolver.internalPointBuildView("qx",IPField.THERMALFLUX_X, 1, 1)
mysolver.internalPointBuildView("qy",IPField.THERMALFLUX_Y, 1, 1)
mysolver.internalPointBuildView("qz",IPField.THERMALFLUX_Z, 1, 1)

mysolver.archivingForceOnPhysicalGroup("Edge",7,0)
mysolver.archivingForceOnPhysicalGroup("Edge",7,1)

# solve
mysolver.solve()

check = TestCheck()
check.equal(4.693026e+06,mysolver.getArchivedForceOnPhysicalGroup("Edge",7,0),1.e-5)
check.equal(8.078873e+05,mysolver.getArchivedForceOnPhysicalGroup("Edge",7,1),1.e-5)

