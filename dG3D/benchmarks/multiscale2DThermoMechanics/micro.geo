mm = 1e-3;

L = 0.165*mm;
D = 0.102*mm;

lsca = D*0.1;
Point(1) = {0, 0,0, lsca};
Point(2) = {0.5*D, 0,0, lsca};
Point(3) = {0.5*L, 0,0, lsca};
Point(4) = {0.5*L, 0.5*L,0, lsca};
Point(5) = {0, 0.5*L,0, lsca};
Point(6) = {0, 0.5*D,0, lsca};
Point(7) = {0.5*D/Sqrt(2.), 0.5*D/Sqrt(2.),0, lsca};
Point(8) = {0.7*0.5*D/Sqrt(2.), 0.,0, lsca};
Point(9) = {0.7*0.5*D/Sqrt(2.),0.7* 0.5*D/Sqrt(2.),0, lsca};
Point(10) = {0,0.7*0.5*D/Sqrt(2.),0, lsca};

Line(1) = {5, 4};
Line(2) = {4, 3};
Line(3) = {3, 2};
Line(4) = {2, 8};
Line(5) = {8, 1};
Line(6) = {1, 10};
Line(7) = {10, 6};
Line(8) = {6, 5};
Line(9) = {10, 9};
Line(10) = {9, 8};
Circle(11) = {6, 1, 7};
Circle(12) = {7, 1, 2};
Line(13) = {9, 7};
Line(14) = {7, 4};
Line Loop(15) = {1, -14, -11, 8};
Plane Surface(16) = {15};
Line Loop(17) = {14, 2, 3, -12};
Plane Surface(18) = {17};
Line Loop(19) = {12, 4, -10, 13};
Plane Surface(20) = {19};
Line Loop(21) = {13, -11, -7, 9};
Plane Surface(22) = {21};
Line Loop(23) = {9, 10, 5, 6};
Plane Surface(24) = {23};

Symmetry {1, 0, 0, 0} {
  Duplicata { Surface{16, 18, 22, 20, 24}; }
}
Symmetry {0, 1, 0, 0} {
  Duplicata { Surface{30, 25, 35, 40, 45, 24, 22, 16, 20, 18}; }
}
Physical Line(1) = {32, 51};
Physical Line(3) = {2, 96};
Physical Line(2) = {85, 55};
Physical Line(4) = {26, 1};
Physical Surface(11) = {25, 16, 18, 94, 84, 54, 49, 30};
Physical Surface(12) = {22, 35, 40, 45, 24, 20, 89, 74, 69, 59, 64, 79};
Transfinite Line {26, 28, 39, 48, 63, 57, 55, 32, 43, 10, 2, 1, 9, 5, 75, 81, 85, 12, 11, 34, 53, 76, 67, 90, 96, 51, 6, 73} = 5 Using Progression 1;
Transfinite Line {36, 7, 13, 4, 80, 62, 60, 42} = 3 Using Progression 1;
Transfinite Line {-27, 8, 14, -3, -86, 58, 50, -33} = 5 Using Progression 1.1;

Transfinite Surface "*";
//Recombine Surface "*";
