mm = 1e-3;
L = 600.*mm;
H = 30*mm;

lsca = H;
Point(1)= {0,0,0,lsca};
Point(2)= {L,0,0,lsca};
Point(3)= {L,H,0,lsca};
Point(4)= {0,H,0,lsca};



Line(1) = {1, 2};
Line(2) = {2, 3};
Line(3) = {3, 4};
Line(4) = {4, 1};
Line Loop(5) = {3, 4, 1, 2};
Plane Surface(6) = {5};
Transfinite Line {4, 2} = 4 Using Progression 1;
Transfinite Line {3, 1} = 11 Using Progression 1;
Transfinite Surface {6};
//Recombine Surface {6};
Physical Line(7) = {4};
Physical Line(8) = {2};
Physical Line(9) = {3};
Physical Line(10) = {1};
Physical Surface(11) = {6};
Physical Point(12) = {4};
Physical Point(13) = {3};
Physical Point(14) = {2};
Physical Point(15) = {1};
