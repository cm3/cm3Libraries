SetFactory("OpenCASCADE");
Geometry.AutoCoherence=0.0001;

l=28.0;
L = 9.97;
W = 6.0*L;
H = 34.0*L;
e = l*0.15;
// exterior cube
Point(1) = {0,0,0,l};
Point(2) = {W,0,0,l};
Point(3) = {W,H,0,l};
Point(4) = {0,H,0,l};

Line(1) = {1,2};
Line(2) = {2,3};
Line(3) = {3,4};
Line(4) = {4,1};

Line Loop(5) = {1,2,3,4};

Plane Surface(100) ={5};

Physical Surface(51) ={100};

Physical Line(1) = {1};
Physical Line(2) = {2};
Physical Line(3) = {3};
Physical Line(4) = {4};
Physical Point(5)={3};

Transfinite Line {2,4} = 3 Using Progression 1;
Transfinite Line {1,3} = 2 Using Progression 1;
Transfinite Surface {100};

Recombine Surface {100};
