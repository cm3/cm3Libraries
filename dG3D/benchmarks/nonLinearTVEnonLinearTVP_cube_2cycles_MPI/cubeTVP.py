#coding-Utf-8-*-
from gmshpy import *
# from dG3DpyDebug import*
from dG3Dpy import*
from math import*
import csv, os, shutil
import numpy as np
import pandas as pd

# Script to launch dogbone problem with a python script
lawnum = 1 # unique number of law

# Load the csv data for relaxation spectrum
with open('TPU_relaxationSpectrum_Et_N27_31_07_24_Trefm30.txt', newline='') as csvfile:
    reader = csv.reader(csvfile, delimiter=';')
    relSpec = np.zeros((1,))
    i = 0
    for row in reader:
        if i == 0:
            relSpec[i] = float(' '.join(row))
        else:
            relSpec = np.append(relSpec, float(' '.join(row)))
        i += 1
# print(relSpec)
N = int(0.5*(i-1))
relSpec[0:N+1] *= 1e-6    # convert to MPa

E = relSpec[0] #MPa
nu = 0.4
K = E/3./(1.-2.*nu)	# Bulk mudulus
mu = E/2./(1.+nu)	  # Shear mudulus
rho = 1250e-12 # Bulk mass   905 kg/m3 -> 905e-12 tonne/mm3
Cp = rho*1500e+6 # 1900 J/kg/K -> 1900e+6 Nmm/tonne/K
KThCon = 0.14  # 0.14 W/m/K -> 0.14 Nmm/s/mm/K
Alpha = 1.1e-4 # 1/K

Tinitial = 273.15 + 23 # K
C1 = 524.39721767 #
C2 = 699.76815543 #

Tref = 273.15-30.
Tg = Tref

# isoHard and tempfuncs
hardenc = LinearExponentialJ2IsotropicHardening(1, 4.5, 5.0, 5., 5.)
hardent = LinearExponentialJ2IsotropicHardening(2, 4.5, 5.0, 7.5, 12.5)

hardenk = PolynomialKinematicHardening(3,1)
hardenk.setCoefficients(0,20.)
hardenk.setCoefficients(1,25.)

law1 = NonLinearTVENonLinearTVPDG3DMaterialLaw(lawnum,rho,E,nu,1e-6,Tinitial,Alpha,KThCon,Cp,False,1e-8,1e-6)
law1.setCompressionHardening(hardenc)
law1.setTractionHardening(hardent)
law1.setKinematicHardening(hardenk)

law1.setYieldPowerFactor(3.5) # .5)
law1.setNonAssociatedFlow(True)
law1.nonAssociatedFlowRuleFactor(0.64) # 5)

law1.setStrainOrder(-1)
law1.setViscoelasticMethod(0)
law1.setShiftFactorConstantsWLF(C1,C2)
law1.setReferenceTemperature(Tref)

law1.useRotationCorrectionBool(False,2) # bool, int_Scheme = 0 -> R0, 1 -> R1

# extraHyper
law1.setExtraBranchNLType(5)
law1.useExtraBranchBool(True)
law1.useExtraBranchBool_TVE(True,5)
law1.setVolumeCorrection(0.15, 1500.0, 0.75, 0.15, 150.0, 0.75)
law1.setAdditionalVolumeCorrections(8.5, 0.75, 0.028, 8.5, .075, 0.028)
law1.setExtraBranch_CompressionParameter(1.,0.,0.) # 1.5
law1.setTensionCompressionRegularisation(100.)
law1.setViscoElasticNumberOfElement(N)
if N > 0:
    for i in range(1, N+1):
        law1.setViscoElasticData(i, relSpec[i], relSpec[i+N])
        law1.setCorrectionsAllBranchesTVE(i, 0.15, 1500.0, 0.75, 0.15, 150.0, 0.75)
        law1.setCompressionCorrectionsAllBranchesTVE(i, 1.0)
        law1.setAdditionalCorrectionsAllBranchesTVE(i, 8.5, 0.75, 0.028, 8.5, .075, 0.028)

# Mullins Effect
mullins = linearScaler(4, 0.99)
law1.setMullinsEffect(mullins)

# Viscosity - TVP
eta = constantViscosityLaw(1,1.e+3)
law1.setViscosityEffect(eta,0.21)

# geometry
geofile="cube.geo"
meshfile="cube.msh" # name of mesh file

strainrate = 7.37e-4

# solver
sol = 2  # Gmm=0 (default) Taucs=1 PETsc=2
soltype = 1 # StaticLinear=0 (default) StaticNonLinear=1
nstep = 25   # number of step (used only if soltype=1)
ftime = 0.7105640373197628/strainrate   # Final time (used only if soltype=1)
tol=1.e-5 # relative tolerance for NR scheme (used only if soltype=1)
nstepArch=5 # Number of step between 2 archiving (used only if soltype=1)
fullDg = 0 #O = CG, 1 = DG
space1 = 0 # function space (Lagrange=0)
beta1  = 100
averageStrainBased = False

# creation of ElasticField
# myfield1 = dG3DDomain(1000,83,0,lawnum,fullDG,3,0)
# myfield1.stabilityParameters(beta1)
#myfield1.averageStrainBased(averageStrainBased)

nfield = 29

pertFactor = 1e-8
pertLaw1 = dG3DMaterialLawWithTangentByPerturbation(law1,pertFactor)
ThermoMechanicsEqRatio = 1.e1 # setConstitutiveExtraDofDiffusionEqRatio(ThermoMechanicsEqRatio)
thermalSource = True
mecaSource = True
myfield1 = ThermoMechanicsDG3DDomain(1000,nfield,space1,lawnum,fullDg,ThermoMechanicsEqRatio)
myfield1.setConstitutiveExtraDofDiffusionAccountSource(thermalSource,mecaSource)
myfield1.stabilityParameters(beta1)
myfield1.ThermoMechanicsStabilityParameters(beta1,bool(1))
# myfield1.strainSubstep(2, 10)

# creation of Solver
mysolver = nonLinearMechSolver(1000)
# mysolver.createModel(geofile,meshfile,3,2)
mysolver.loadModel(meshfile)
mysolver.addDomain(myfield1)
mysolver.addMaterialLaw(law1)
mysolver.Scheme(soltype)
mysolver.Solver(sol)
mysolver.snlData(nstep,ftime,tol,1.e-7)
mysolver.stepBetweenArchiving(nstepArch)
mysolver.options("-ksp_type preonly -pc_type lu -pc_factor_mat_solver_type mumps")

mysolver.snlManageTimeStep(25,5,2.,100)
# maxIterations # timeStepLimit below which increase time-step if reduced before for iterations more than maximal # factor of reduction/increase # no.of fails allowed
optimalNumber = 5
maximalStep = 3.e-1
minimalStep = 1e-3
power = 1
maximalFailStep = 10
# mysolver.setTimeIncrementAdaptation(True,optimalNumber,power,maximalStep,minimalStep,maximalFailStep) # automatic time stepping

# Dimension
face_to_face_length = 1.

# BC
fu_L = PiecewiseLinearFunction()
fu_L.put(0,0)
fu_L.put(ftime,0.7105640373197628*face_to_face_length) # strain rate = 1e-2

mysolver.displacementBC("Face",30,0,0.)		        # face x = 0   - Left Face fixed
mysolver.displacementBC("Face",31,0,fu_L)         # face x = L   - Right Face moving
mysolver.displacementBC("Face",34,1,0.)		    # face y = 0
mysolver.displacementBC("Face",32,2,0.)		    # face z = 0

mysolver.initialBC("Volume","Position",nfield,3,Tinitial)

## Field-Outputs
mysolver.internalPointBuildView("svm",IPField.SVM, 1, nstepArch)
mysolver.internalPointBuildView("epl",IPField.PLASTICSTRAIN, 1, nstepArch)
mysolver.internalPointBuildView("temperature",IPField.TEMPERATURE, 1, nstepArch)
mysolver.internalPointBuildView("mechSrc",IPField.MECHANICAL_SOURCE, 1, nstepArch)

mysolver.archivingForceOnPhysicalGroup("Face", 30, 0, nstepArch)
mysolver.archivingForceOnPhysicalGroup("Face", 31, 0, nstepArch)
mysolver.archivingNodeDisplacement(43,0,nstepArch)
mysolver.archivingNodeDisplacement(43,3,nstepArch)
mysolver.archivingNodeDisplacement(44,0,nstepArch)
mysolver.archivingNodeDisplacement(44,3,nstepArch)

# mysolver.createRestartBySteps(5)
mysolver.solve()

#shutil.copy('NodalDisplacementPhysical43Num2comp0_part2.csv', 'NodalDisplacementPhysical43Num2comp0_part2_step0.txt')
#shutil.copy('NodalDisplacementPhysical43Num2comp3_part2.csv', 'NodalDisplacementPhysical43Num2comp3_part2_step0.txt')
#shutil.copy('force31comp0_part1.csv', 'force31comp0_part1_step0.txt')
#shutil.copy('force31comp0_part2.csv', 'force31comp0_part2_step0.txt')
#shutil.copy('force31comp0_part3.csv', 'force31comp0_part3_step0.txt')

lastDisp_step0 = mysolver.getArchivedNodalValue(43,0,mysolver.displacement)
lastTemp_step0 = mysolver.getArchivedNodalValue(43,3,mysolver.displacement)


# ===[CONTINUE SIMULATION - First Unload]=========================================================================
# ===Reset boundary conditions=====================================================================
LastForce= mysolver.getArchivedForceOnPhysicalGroup("Face", 31, 0)
mysolver.resetBoundaryConditions()

print("First Unload")

# creation of Solver
nstep1=25;
mysolver.Scheme(soltype)
mysolver.Solver(sol)
mysolver.snlData(nstep1,ftime/1.,tol,1e-7)
mysolver.snlManageTimeStep(25,5,2.,100)
mysolver.stepBetweenArchiving(nstepArch)
mysolver.options("-ksp_type preonly -pc_type lu -pc_factor_mat_solver_type mumps")

## Force BCs
cyclicFunctionForce1= cycleFunctionTime(0., LastForce,   ftime/2., LastForce/2,   ftime, 0.)
mysolver.displacementBC("Face",30,0,0.)		        # face x = 0   - Left Face fixed
mysolver.forceBC("Face",31,0,cyclicFunctionForce1)         # face x = L   - Right Face moving
mysolver.displacementBC("Face",34,1,0.)		    # face y = 0
mysolver.displacementBC("Face",32,2,0.)		    # face z = 0

## Field-Outputs
mysolver.internalPointBuildView("svm",IPField.SVM, 1, nstepArch)
mysolver.internalPointBuildView("epl",IPField.PLASTICSTRAIN, 1, nstepArch)
mysolver.internalPointBuildView("temperature",IPField.TEMPERATURE, 1, nstepArch)
mysolver.internalPointBuildView("mechSrc",IPField.MECHANICAL_SOURCE, 1, nstepArch)

mysolver.archivingForceOnPhysicalGroup("Face", 30, 0, nstepArch)
mysolver.archivingForceOnPhysicalGroup("Face", 31, 0, nstepArch)
mysolver.archivingNodeDisplacement(43,0,nstepArch)
mysolver.archivingNodeDisplacement(43,3,nstepArch)
mysolver.archivingNodeDisplacement(44,0,nstepArch)
mysolver.archivingNodeDisplacement(44,3,nstepArch)

# mysolver.createRestartBySteps(5)
mysolver.solve()

#shutil.copy('NodalDisplacementPhysical43Num2comp0_part2.csv', 'NodalDisplacementPhysical43Num2comp0_part2_step1.txt')
#shutil.copy('NodalDisplacementPhysical43Num2comp3_part2.csv', 'NodalDisplacementPhysical43Num2comp3_part2_step1.txt')
#shutil.copy('force31comp0_part1.csv', 'force31comp0_part1_step1.txt')
#shutil.copy('force31comp0_part2.csv', 'force31comp0_part2_step1.txt')
#shutil.copy('force31comp0_part3.csv', 'force31comp0_part3_step1.txt')

#path = "/home/ujwal/Desktop/Code/TVP_52_17_06_24_3DTests_Round2_TPU/Test8_cube_2Cycles_noRot_restart_MPI_TPU/"
path =os.getcwd()
print('Path: ',path)
to_dir = os.path.join(path,"previousScheme_step1")
from_dir = os.path.join(path,"previousScheme")
try:
  destination = shutil.copytree(from_dir, to_dir, dirs_exist_ok = True)

  if(os.path.exists(path)):
    shutil.rmtree(from_dir, ignore_errors=True)
except:
  print("already copied")
# ===[CONTINUE SIMULATION - First Reload]=========================================================================
# ===Reset boundary conditions=====================================================================


#pd.read_csv("NodalDisplacementPhysical43Num2comp0_part2_step0.txt",\
#                        sep =";",header=None).to_numpy()
#lastDisp_step1 = pd.read_csv("NodalDisplacementPhysical43Num2comp0_part2_step1.txt",\
#                        sep =";",header=None).to_numpy()
lastDisp_step1 = mysolver.getArchivedNodalValue(43,0,mysolver.displacement)
lastTemp_step1 = mysolver.getArchivedNodalValue(43,3,mysolver.displacement)


lastStrain = (lastDisp_step0-lastDisp_step1)/(1)
print('Last strain',lastStrain)
mysolver.resetBoundaryConditions()

# creation of Solver
nstep2=25;
ftime2 = (1.508091603053435-lastStrain)/strainrate
mysolver.Scheme(soltype)
mysolver.Solver(sol)
mysolver.snlData(nstep2,ftime2,tol,1e-7)
mysolver.snlManageTimeStep(25,5,2.,50)
mysolver.stepBetweenArchiving(nstepArch)

## Disp BCs
cyclicFunctionDisp = cycleFunctionTime(0.,lastDisp_step1, ftime2, 1.508091603053435*face_to_face_length)
mysolver.displacementBC("Face",30,0,0.)		        # face x = 0   - Left Face fixed
mysolver.displacementBC("Face",31,0,cyclicFunctionDisp)         # face x = L   - Right Face moving
mysolver.displacementBC("Face",34,1,0.)		    # face y = 0
mysolver.displacementBC("Face",32,2,0.)		    # face z = 0

## Thermal BCs - disps are already stored

## Field-Outputs
mysolver.internalPointBuildView("svm",IPField.SVM, 1, nstepArch)
mysolver.internalPointBuildView("epl",IPField.PLASTICSTRAIN, 1, nstepArch)
mysolver.internalPointBuildView("temperature",IPField.TEMPERATURE, 1, nstepArch)
mysolver.internalPointBuildView("mechSrc",IPField.MECHANICAL_SOURCE, 1, nstepArch)

mysolver.archivingForceOnPhysicalGroup("Face", 30, 0, nstepArch)
mysolver.archivingForceOnPhysicalGroup("Face", 31, 0, nstepArch)
mysolver.archivingNodeDisplacement(43,0,nstepArch)
mysolver.archivingNodeDisplacement(43,3,nstepArch)
mysolver.archivingNodeDisplacement(44,0,nstepArch)
mysolver.archivingNodeDisplacement(44,3,nstepArch)

# mysolver.createRestartBySteps(5)
mysolver.solve()

#shutil.copy('NodalDisplacementPhysical43Num2comp0_part2.csv', 'NodalDisplacementPhysical43Num2comp0_part2_step2.txt')
#shutil.copy('NodalDisplacementPhysical43Num2comp3_part2.csv', 'NodalDisplacementPhysical43Num2comp3_part2_step2.txt')
#shutil.copy('force31comp0_part1.csv', 'force31comp0_part1_step2.txt')
#shutil.copy('force31comp0_part2.csv', 'force31comp0_part2_step2.txt')
#shutil.copy('force31comp0_part3.csv', 'force31comp0_part3_step2.txt')

#path = "/home/ujwal/Desktop/Code/TVP_52_17_06_24_3DTests_Round2_TPU/Test8_cube_2Cycles_noRot_restart_MPI_TPU/"
to_dir = os.path.join(path,"previousScheme_step2")
from_dir = os.path.join(path,"previousScheme")
try:
  destination = shutil.copytree(from_dir, to_dir, dirs_exist_ok = True)
  if(os.path.exists(path)):
    shutil.rmtree(from_dir, ignore_errors=True)
except:
  print("already copied")


lastDisp_step2 = mysolver.getArchivedNodalValue(43,0,mysolver.displacement)
lastTemp_step2 = mysolver.getArchivedNodalValue(43,3,mysolver.displacement)

check = TestCheck()
check.equal(299.0484466080293,mysolver.getArchivedNodalValue(43,3,mysolver.displacement),1.e-6)
check.equal(-8.632986e+00,mysolver.getArchivedForceOnPhysicalGroup("Face", 30, 0),1.e-6)


"""
# ===[CONTINUE SIMULATION - 2nd Unload]=========================================================================
# ===Reset boundary conditions=====================================================================
LastForce2 = mysolver.getArchivedForceOnPhysicalGroup("Face", 84, 1)
mysolver.resetBoundaryConditions()

print("Second Unload")
shutil.copy('force86comp1_part0.csv', 'force86comp1_part0_step2.txt')
shutil.copy('force84comp1_part2.csv', 'force84comp1_part2_step2.txt')
shutil.copy('NodalDisplacementPhysical2Num11comp1_part0.csv', 'NodalDisplacementPhysical2Num11comp1_part0_step2.txt')
shutil.copy('NodalDisplacementPhysical3Num6comp1_part2.csv', 'NodalDisplacementPhysical3Num6comp1_part2_step2.txt')
shutil.copy('NodalDisplacementPhysical3Num6comp3_part2.csv', 'NodalDisplacementPhysical3Num6comp3_part2_step2.txt')
shutil.copy('NodalDisplacementPhysical4Num7comp1_part2.csv', 'NodalDisplacementPhysical4Num7comp1_part2_step2.txt')
shutil.copy('NodalDisplacementPhysical5Num3comp1_part2.csv', 'NodalDisplacementPhysical5Num3comp1_part2_step2.txt')
shutil.copy('NodalDisplacementPhysical5Num3comp3_part2.csv', 'NodalDisplacementPhysical5Num3comp3_part2_step2.txt')
shutil.copy('NodalDisplacementPhysical6Num9comp1_part1.csv', 'NodalDisplacementPhysical6Num9comp1_part1_step2.txt')
shutil.copy('NodalDisplacementPhysical6Num9comp3_part1.csv', 'NodalDisplacementPhysical6Num9comp3_part1_step2.txt')
shutil.copy('NodalDisplacementPhysical7Num1comp3_part1.csv', 'NodalDisplacementPhysical7Num1comp3_part1_step2.txt')
shutil.copy('NodalDisplacementPhysical8Num2comp3_part1.csv', 'NodalDisplacementPhysical8Num2comp3_part1_step2.txt')

# creation of Solver
nstep3=500;
mysolver.Scheme(soltype)
mysolver.Solver(sol)
mysolver.snlData(nstep3,ftime2,tol,1e-7)
mysolver.snlManageTimeStep(25,5,2.,50)
mysolver.stepBetweenArchiving(nstepArch)

## Force BCs
cyclicFunctionForce2= cycleFunctionTime(0.,LastForce2,ftime2/2, LastForce2/2,ftime2, 0.)
mysolver.displacementBC("Face",86,0,0.)
mysolver.displacementBC("Face",86,1,0.)
mysolver.displacementBC("Face",86,2,0.)
mysolver.displacementBC("Face",84,0,0.)
mysolver.forceBC("Face",84,1,cyclicFunctionForce2)
mysolver.displacementBC("Face",84,2,0.)

## Thermal BCs - disps are already stored

## Field-Outputs
mysolver.internalPointBuildView("svm",IPField.SVM, 1, nstepArch)
mysolver.internalPointBuildView("sig_xx",IPField.SIG_XX, 1, nstepArch)
mysolver.internalPointBuildView("sig_yy",IPField.SIG_YY, 1, nstepArch)
mysolver.internalPointBuildView("sig_zz",IPField.SIG_ZZ, 1, nstepArch)
mysolver.internalPointBuildView("sig_xy",IPField.SIG_XY, 1, nstepArch)
mysolver.internalPointBuildView("sig_yz",IPField.SIG_YZ, 1, nstepArch)
mysolver.internalPointBuildView("sig_xz",IPField.SIG_XZ, 1, nstepArch)
mysolver.internalPointBuildView("epl",IPField.PLASTICSTRAIN, 1, nstepArch)
mysolver.internalPointBuildView("temperature",IPField.TEMPERATURE, 1, nstepArch)
mysolver.internalPointBuildView("mechSrc",IPField.MECHANICAL_SOURCE, 1, nstepArch)
mysolver.internalPointBuildView("plastic possion ratio",IPField.PLASTIC_POISSON_RATIO,1,nstepArch)

mysolver.archivingForceOnPhysicalGroup("Face", 84, 1)
mysolver.archivingForceOnPhysicalGroup("Face", 86, 1)
# mysolver.archivingForceOnPhysicalGroup("Node", 5, 1)
mysolver.archivingNodeDisplacement(2,1,1)
mysolver.archivingNodeDisplacement(3,1,1)
mysolver.archivingNodeDisplacement(4,1,1)
mysolver.archivingNodeDisplacement(5,1,1)
mysolver.archivingNodeDisplacement(6,1,1)
mysolver.archivingNodeDisplacement(3,3,1)
mysolver.archivingNodeDisplacement(5,3,1)
mysolver.archivingNodeDisplacement(6,3,1)
mysolver.archivingNodeDisplacement(7,3,1)
mysolver.archivingNodeDisplacement(8,3,1)

mysolver.createRestartBySteps(20)
mysolver.solve()

shutil.copy('force86comp1_part0.csv', 'force86comp1_part0_step3.txt')
shutil.copy('force84comp1_part2.csv', 'force84comp1_part2_step3.txt')
shutil.copy('NodalDisplacementPhysical2Num11comp1_part0.csv', 'NodalDisplacementPhysical2Num11comp1_part0_step3.txt')
shutil.copy('NodalDisplacementPhysical3Num6comp1_part2.csv', 'NodalDisplacementPhysical3Num6comp1_part2_step3.txt')
shutil.copy('NodalDisplacementPhysical3Num6comp3_part2.csv', 'NodalDisplacementPhysical3Num6comp3_part2_step3.txt')
shutil.copy('NodalDisplacementPhysical4Num7comp1_part2.csv', 'NodalDisplacementPhysical4Num7comp1_part2_step3.txt')
shutil.copy('NodalDisplacementPhysical5Num3comp1_part2.csv', 'NodalDisplacementPhysical5Num3comp1_part2_step3.txt')
shutil.copy('NodalDisplacementPhysical5Num3comp3_part2.csv', 'NodalDisplacementPhysical5Num3comp3_part2_step3.txt')
shutil.copy('NodalDisplacementPhysical6Num9comp1_part1.csv', 'NodalDisplacementPhysical6Num9comp1_part1_step3.txt')
shutil.copy('NodalDisplacementPhysical6Num9comp3_part1.csv', 'NodalDisplacementPhysical6Num9comp3_part1_step3.txt')
shutil.copy('NodalDisplacementPhysical7Num1comp3_part1.csv', 'NodalDisplacementPhysical7Num1comp3_part1_step3.txt')
shutil.copy('NodalDisplacementPhysical8Num2comp3_part1.csv', 'NodalDisplacementPhysical8Num2comp3_part1_step3.txt')
"""
