#coding-Utf-8-*-
from gmshpy import *
from dG3Dpy import*
from math import*


# Script for notched specimen with Gurson/Thomason w/wo cracks
# -> reproduce tests of Mecar
# Julien L. 04/2019

# Options
# ==========================================================
transitionFlag=0   	# choose type of law. 
			# ==0 no crack
			# ==1 with crack
cl_law=1


# Material law creation
# ==========================================================
# material parameters - bulk law
bulkLawNum1 = 11
rho   = 7850 	# density
young = 205.4e9	# module de young
nu    = 0.3	# poisson coefficient

# hardening law
sy0   = 862.4e6
h = 3399. *1e6
p1 = 0.012
n1 = 0.084
p2 = 0.056
n2 = 0.058
harden1 = LinearFollowedByMultiplePowerLawJ2IsotropicHardening(bulkLawNum1,sy0,h,p1,n1,p2,n2)

# n-l law
l = 50.*1.e-6
cl = l*l
lengthLaw1 = IsotropicCLengthLaw(bulkLawNum1, cl)

# nucleation
A = 0.015
nucl1 = LinearNucleationFunctionLaw(0,A)
bebere = BereminNucleationCriterionLaw(0, sy0*1.5, sy0*1.5, sy0*2.2, 0.6, 0.6, 1.6)
bebere.rotateMaterialAxes(0.,90.,0.)

# growth law
q1    = 1.414
q2    = 1.
q3    = q1
fVinitial = 1.e-3



# coalescence
lambda0 = 1.
kappa = 1.
#coalesLaw1 = ThomasonCoalescenceLaw(bulkLawNum1,lambda0,kappa)
#fctloc = fourVariableExponentialSaturationScalarFunction(5.e-5, 1.e-5, 0.95, 0.99)
#fctcor = fourVariableExponentialSaturationScalarFunction(5.e-5, 1.e-5, 0.655, 0.662)
#coalesLaw1.setRegularizedFunction(fctloc)

BulkLaw1 = NonLocalPorousCoupledDG3DMaterialLaw(bulkLawNum1,young,nu,rho,q1,q2,q3,fVinitial,lambda0,kappa,harden1,lengthLaw1,1.e-9)
BulkLaw1.setYieldSurfaceExponent(10)
BulkLaw1.setOrderForLogExp(9)
BulkLaw1.setSubStepping(bool(1),8)
BulkLaw1.setStressFormulation(1)

BulkLaw1.setNucleationLaw(nucl1,bebere)



if cl_law == 2:
	BulkLaw1.setNonLocalMethod(2)
	BulkLaw1.clearCLengthLaw()
	BulkLaw1.setCLengthLaw(lengthLaw1)
	BulkLaw1.setCLengthLaw(lengthLaw1)
	BulkLaw1.setCLengthLaw(lengthLaw1)

if transitionFlag == 1:
	BulkLaw1.setCrackTransition(True,0.)
	BulkLaw1.setCfTOffsetMethod(2)
	BulkLaw1.setCouplingBehaviour(True,True)
	BulkLaw1.setPostBlockedDissipationBehavior(2)

	interfaceLawNum1 = 2
	lBand = 12.*1e-6 # m
	Kp = 1.e17 #N/m2
	cohLaw1 = PorosityCohesiveBand3DLaw(interfaceLawNum1,lBand,Kp,True)
	cohLaw1.setPorousMaterialLaw(BulkLaw1)
	cohLaw1.setMaximalAdmissiblePenetrationJump(True, -1.e-8)
	cohLaw1.setDeletionOfBadConditionnedIPV(True)
	# fracture law creation
	fracLawNum1 = 6
	fracLaw1 = FractureByCohesive3DLaw(fracLawNum1,bulkLawNum1,interfaceLawNum1)


# Domain creation
meshfile = "model.msh" 	# name of mesh file
## ===================================================================================
eqRatio = 1e8		# Ratio between "elastic" and non-local equations 
space1 = 0 		# Function space (Lagrange=0)
beta1  = 175.0		# Penality parameter for DG

if transitionFlag == 0:
	fullDg = False
	field1 = axisymmetricDG3DDomain(10,11,0,bulkLawNum1,fullDg,2,1)
	field1.stabilityParameters(beta1)
	field1.setNonLocalStabilityParameters(beta1,fullDg)
	field1.setNonLocalEqRatio(eqRatio)
if transitionFlag == 1:
	fullDg = True
	field1 = axisymmetricDG3DDomain(10,11,0,fracLawNum1,fullDg,2,1)
	field1.stabilityParameters(beta1)
	field1.setNonLocalStabilityParameters(beta1,fullDg)
	field1.setNonLocalEqRatio(eqRatio)
	field1.setBulkDamageBlockedMethod(1) 
	field1.forceCohesiveInsertionAllIPs(True,0.0)
	field1.setIncrementNonlocalBased(True)


# Solver creation
# ===================================================================================
ftime = 100. 		# Final time
tot_disp = 3.8*1.e-3	# Final displ.

soltype = 4 		# StaticLinear=0 (default, StaticNonLinear=1, Explicit=2, # Multi=3, Implicit=4, Eigen=5)
nstep = 2000 		# Number of step
tol=1.e-7 		# Relative tolerance for NR scheme
tolAbs = 1.e-4		# Absolute tolerance for NR scheme
nstepArchIP=10		# Number of step between 2 archiving
nstepArchForce = 10	# Number of step between 2 force archiving
nstepArchEnergy = nstepArchForce # Number of step between 2 energy computation and archiving

MaxIter = 16		# Maximum number of iterations
StepIncrease = 3	# Number of successfull timestep before reincreasing it
StepReducFactor = 4. 	# Timestep reduction factor
NumberReduction = 20	# Maximum number of timespep reduction: max reduction = pow(StepReducFactor,this)

# solver parametrisation
mysolver = nonLinearMechSolver(1000)
mysolver.loadModel(meshfile)
mysolver.addDomain(field1)
mysolver.addMaterialLaw(BulkLaw1)
if transitionFlag == 1:
	mysolver.addMaterialLaw(BulkLaw1)
	mysolver.addMaterialLaw(cohLaw1)
	mysolver.addMaterialLaw(fracLaw1)
mysolver.Scheme(soltype) 		# solver scheme
mysolver.Solver(2) 			# Library solver: Gmm=0 (default) Taucs=1 PETsc=2
mysolver.snlData(nstep,ftime,tol,tolAbs) # solver parameters for imp. solving (numb. of step, final time, rel. tol. and absolute one)
mysolver.snlManageTimeStep(MaxIter,StepIncrease,StepReducFactor,NumberReduction) # setting iterations control
mysolver.implicitSpectralRadius(0.0)
#mysolver.setMaxAllowedCrackInsertionAtOneStep(1) # number of crack allowed in one step
mysolver.stepBetweenArchiving(nstepArchIP) 	# archiving frequency
mysolver.energyComputation(nstepArchEnergy)	# archiving frequency for energy

# Boundary conditions
# ===============================
mysolver.initialBC("Face","Position",11,3,fVinitial) #init cond.
mysolver.displacementBC("Face",11,1,0.) # Face blocking
mysolver.displacementBC("Edge",10,1,0.) # axis

fct_up = LinearFunctionTime(0.,0.,ftime,tot_disp*0.5)
fct_down = LinearFunctionTime(0.,0.,ftime,-tot_disp*0.5)
mysolver.displacementBC("Edge",4,2,fct_up)
mysolver.displacementBC("Edge",5,2,fct_down)


# Variable storage
# ===============================
mysolver.internalPointBuildView("svm",IPField.SVM, 1, 1)
mysolver.internalPointBuildView("sig_xx",IPField.SIG_XX, 1, 1)
mysolver.internalPointBuildView("sig_yy",IPField.SIG_YY, 1, 1)
mysolver.internalPointBuildView("sig_zz",IPField.SIG_ZZ, 1, 1)
mysolver.internalPointBuildView("sig_xz",IPField.SIG_XZ, 1, 1)
mysolver.internalPointBuildView("epl",IPField.PLASTICSTRAIN, 1, 1)
mysolver.internalPointBuildView("triaxiality",IPField.STRESS_TRIAXIALITY, 1, 1)
mysolver.internalPointBuildView("lode",IPField.LODE_PARAMETER, 1, 1)  
mysolver.internalPointBuildView("coales",IPField.COALESCENCE, 1, 1)
mysolver.internalPointBuildView("local_fV",IPField.LOCAL_POROSITY,1,1)
mysolver.internalPointBuildView("local_fV_max",IPField.LOCAL_POROSITY,1,IPField.MAX_VALUE)
mysolver.internalPointBuildView("non_local_porosity",IPField.NONLOCAL_POROSITY, 1, 1)
mysolver.internalPointBuildView("chi max",IPField.LIGAMENT_RATIO, 1, IPField.MAX_VALUE)
mysolver.internalPointBuildView("damage_is_blocked",IPField.DAMAGE_IS_BLOCKED, 1, IPField.MAX_VALUE)
mysolver.internalPointBuildView("lost_ellipticity",IPField.LOST_ELLIPTICITY, 1, IPField.MIN_VALUE)
mysolver.internalPointBuildView("nucleation_onset",IPField.NUCLEATION_ONSET_0, 1, 1)

mysolver.archivingForceOnPhysicalGroup("Edge", 4, 2, nstepArchForce) # applied force
mysolver.archivingNodeDisplacement(42,0,nstepArchForce)
mysolver.archivingNodeDisplacement(42,2,nstepArchForce)
mysolver.archivingNodeDisplacement(2,0,nstepArchForce)

mysolver.archivingIPOnPhysicalGroup("Face",11,IPField.NUCLEATION_ONSET_0,IPField.MEAN_VALUE);



mysolver.solve()

check = TestCheck()
check.equal(1.970223e+04, mysolver.getArchivedForceOnPhysicalGroup("Edge", 4, 2),1.e-4)


import csv
data = csv.reader(open('IPFace11val_NUCLEATION_ONSET_0Mean.csv'), delimiter=';')
variables = list(data)
check.equal(1.823946e-01,float(variables[-1][1]),1e-6)





