// Notched axis sym bar with mesh aspect ratio
// Geometry file


// Options
factorX = 1.; // scaling ratio
mm = 1.e-3;

// Geometrical parameters
Rout = 3.*mm; // outter radius
Lhalf = 10*mm; // half of the total lenght
Lbox = Lhalf*0.5; // box lenght
Lbox2 = Lhalf*0.175; // position of bar parameter

// Mesh size parameters
lsca1 = 150.*1.e-3*mm;
lsca2 = Rout/4.;

// Geometrical description - upper part
// Central axis
Point(1) = {0,0,0,lsca1};
Point(2) = {0,0,Lbox2,lsca1*3.};
Point(3) = {0,0,Lbox,lsca2}; // box for mesh
Point(4) = {0,0,Lhalf,lsca2}; // tip

// External border
Point(5) = {Rout,0,Lhalf,lsca2}; //tip
Point(6) = {Rout,0,Lbox,lsca2};


// Notch
Point(8) = {Rout,0,Lbox2,lsca1*3.};
Point(9) = {Rout*0.999,0,0,lsca1};


// Geometrical description - bottom part
// Central axis
Point(12) = {0,0,-Lbox2,lsca1*3.};
Point(13) = {0,0,-Lbox,lsca2}; // box for mesh
Point(14) = {0,0,-Lhalf,lsca2}; // tip

// External border
Point(15) = {Rout,0,-Lhalf,lsca2}; //tip
Point(16) = {Rout,0,-Lbox,lsca2};

// Notch
Point(18) = {Rout,0,-Lbox2,lsca1*3.};



// Lines
Line(1) = {1,2};
Line(2) = {2,3};
Line(3) = {3,4};
Line(4) = {4,5};
Line(5) = {3,6};
Line(6) = {5,6};
Line(7) = {6,8};
Line(8) = {8,9};
Line(10) = {8,2};

Line(11) = {1,12};
Line(12) = {12,13};
Line(13) = {13,14};
Line(14) = {14,15};
Line(15) = {13,16};
Line(16) = {15,16};
Line(17) = {16,18};
Line(18) = {18,9};
Line(20) = {18,12};


// Surface
Curve Loop(1) = {1, -10, 8, -18, 20, -11};
Curve Loop(2) = {2, 5, 7, 10};
Curve Loop(3) = {3, 4, 6, -5};
Curve Loop(12) = {12, 15, 17, 20};
Curve Loop(13) = {13, 14, 16, -15};
Plane Surface(1) = {1};
Plane Surface(2) = {2};
Plane Surface(3) = {3};
Plane Surface(12) = {12};
Plane Surface(13) = {13};


// Mesh definition
Transfinite Curve {4, 5, 14, 15} = 6;
Transfinite Curve {3, 6, 13, 16} = 12;
Transfinite Surface{3,13};



// Physicals
Physical Surface(11) = {1,2,3,12,13};

Physical Curve(10) = {13,12,11,1,2,3}; // axis
Physical Curve(4) = {4};	// top
Physical Curve(5) = {14}; 	// bottom

// Points: // index of type x1/2, 
// x: depond on physical curve, x1: on the axis, x2 : on external border;
Physical Point(1) = {1};	// center of axis	
Physical Point(2) = {9};	// center of notch
Physical Point(41) = {4};	// on upper tip
Physical Point(42) = {5};
Physical Point(51) = {14};	// on bottom tip
Physical Point(52) = {15};





