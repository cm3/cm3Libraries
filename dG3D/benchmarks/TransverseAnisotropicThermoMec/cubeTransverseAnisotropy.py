#coding-Utf-8-*-
from gmshpy import *
from dG3Dpy import*

#script to launch beam problem with Debuga python script

# material law
lawnum   = 1 # unique number of law
rho      = 7850
young    = 40.e9
youngA   = 230.e9;
nu       = 0.2 #
nu_minor = 0.256*young/youngA 
GA       = 24.e9
Ax       = 0.; #direction of anisotropy
Ay       = 0.;
Az       = 1.;

alpha = beta=gamma=0.
kxcf=kycf=kzcf= 2.
alphaxcf=2.e-6
lxcf=lycf=lzcf=33333.#59880.carbon  59880.  !!
cpcf=712.*rho #for carbon graphit 712 or 544.  CF is 1780
tinitial=273.
# geometry
geofile="cube.geo"
meshfile="cube.msh" # name of mesh file

# solver
sol = 2  # Gmm=0 (default) Taucs=1 PETsc=2
soltype = 1 # StaticLinear=0 (default) StaticNonLinear=1
nstep = 2   # number of step (used only if soltype=1)
ftime =1.   # Final time (used only if soltype=1)
tol=1.e-8   # relative tolerance for NR scheme (used only if soltype=1)
nstepArch=1 # Number of step between 2 archiving (used only if soltype=1)
fullDg = True #O = CG, 1 = DG
space1 = 0 # function space (Lagrange=0)
eqratio=1.e8


#  compute solution and BC (given directly to the solver
# creation of law
law1 =  mlawAnIsotropicTherMechDG3DMaterialLaw(lawnum,young,nu,rho,youngA,GA,nu_minor,Ax,Ay,Az,alpha, beta,gamma,tinitial,kxcf, kycf,kzcf,cpcf,alphaxcf) 

# creation of ElasticField
nfield = 10 # number of the field (physical number of surface)
myfield1 = ThermoMechanicsDG3DDomain(1000,nfield,space1,lawnum,fullDg,eqratio)
myfield1.stabilityParameters(30.)
myfield1.ThermoMechanicsStabilityParameters(30.,fullDg)
myfield1.matrixByPerturbation(0,0,0,1e-8)
# creation of Solver
mysolver = nonLinearMechSolver(1000)
mysolver.createModel(geofile,meshfile,3,2)
#mysolver.loadModel(meshfile)
mysolver.addDomain(myfield1)
mysolver.addMaterialLaw(law1)
mysolver.Scheme(soltype)
mysolver.Solver(sol)
mysolver.snlData(nstep,ftime,tol)
mysolver.stepBetweenArchiving(nstepArch)
# BC

mysolver.displacementBC("Face",1234,2,0.)
d1=0.0005
cyclicFunction1=cycleFunctionTime(0.,0.,ftime/4., d1/2., ftime/2., 0., 3.*ftime/4., d1/2., ftime, d1);
#mysolver.displacementBC("Face",5678,2,cyclicFunction1)
mysolver.displacementBC("Face",2376,0,0.)
mysolver.displacementBC("Face",1265,1,0.)

Temp1=tinitial+2000.
cyclicFunctionTemp1=cycleFunctionTime(0.,tinitial,ftime, Temp1);

mysolver.initialBC("Volume","Position",nfield,3,tinitial)
mysolver.displacementBC("Face",1234,3,cyclicFunctionTemp1)
mysolver.displacementBC("Face",5678,3,cyclicFunctionTemp1)

mysolver.internalPointBuildView("svm",IPField.SVM, 1, 1)
mysolver.internalPointBuildView("sig_xx",IPField.SIG_XX, 1, 1)
mysolver.internalPointBuildView("sig_yy",IPField.SIG_YY, 1, 1)
mysolver.internalPointBuildView("sig_zz",IPField.SIG_ZZ, 1, 1)
mysolver.internalPointBuildView("sig_xy",IPField.SIG_XY, 1, 1)
mysolver.internalPointBuildView("sig_yz",IPField.SIG_YZ, 1, 1)
mysolver.internalPointBuildView("sig_xz",IPField.SIG_XZ, 1, 1)
mysolver.internalPointBuildView("epl",IPField.PLASTICSTRAIN, 1, 1)
mysolver.internalPointBuildView("temperature",IPField.TEMPERATURE, 1, 1)
mysolver.internalPointBuildView("qx",IPField.THERMALFLUX_X, 1, 1)
mysolver.internalPointBuildView("qy",IPField.THERMALFLUX_Y, 1, 1)
mysolver.internalPointBuildView("qz",IPField.THERMALFLUX_Z, 1, 1)
mysolver.internalPointBuildView("epl",IPField.PLASTICSTRAIN, 1, 1)

mysolver.archivingForceOnPhysicalGroup("Face", 1234, 2)
mysolver.archivingNodeDisplacement(8,0)
mysolver.archivingNodeDisplacement(8,1)
mysolver.archivingNodeDisplacement(8,2)
mysolver.archivingNodeDisplacement(8,3)


mysolver.solve()

check = TestCheck()
check.equal(-2.123032e-06,mysolver.getArchivedNodalValue(8,0,mysolver.displacement),1.e-5)
check.equal(2.124099e-06,mysolver.getArchivedNodalValue(8,1,mysolver.displacement),1.e-5)
check.equal(4.520618e-07,mysolver.getArchivedNodalValue(8,2,mysolver.displacement),1.e-4)
check.equal(2.272999e+03,mysolver.getArchivedNodalValue(8,3,mysolver.displacement),1.e-5)
check.equal(-4.356476e-08,mysolver.getArchivedForceOnPhysicalGroup("Face", 1234, 2),1.e-4)


