
mm=1.0e-3;
n=1;
L=6e-2*mm;
sl1=0.08*L/n;
Point(1)={0,0,0,sl1};
Point(2)={L,0,0,sl1};
Point(3)={L,L,0,sl1};
Point(4)={0,L,0,sl1};
Line(1)={1,2};
Line(2)={2,3};
Line(3)={3,4};
Line(4)={4,1};
x0=0.5*L/n; y0=0.5*L/n; r=0.3*L/n; 
num=0;
moyen=0;
surf=0;
For i In {0:n-1}
  	For j In {0:n-1}
		x=x0*(2*i+1);
		y=y0*(2*j+1);
		sl2=0.5*sl1;
		moyen=moyen+r;
		surf=surf+Pi*r*r;

		p1=newp; Point(p1)={x-r,y,0,sl2};
		p2=newp; Point(p2)={x,y+r,0,sl2};
		p3=newp; Point(p3)={x+r,y,0,sl2};
		p4=newp; Point(p4)={x,y-r,0,sl2};
		pc=newp; Point(pc)={x,y,0,sl2};

		c1 = newreg; Circle(c1) = {p1,pc,p2};
		c2 = newreg; Circle(c2) = {p2,pc,p3};
		c3 = newreg; Circle(c3) = {p3,pc,p4};
		c4 = newreg; Circle(c4) = {p4,pc,p1};

//		Transfinite Line {c3, c2, c4, c1} = 4 Using Progression 1;
		num+=1;
		l[num]=newreg; Line Loop(l[num]) = {c1,c2,c3,c4}; 
	EndFor	
EndFor
Line(10) = {9, 6};
Line(11) = {9, 7};
Line(12) = {9, 8};
Line(13) = {5, 9};
Line Loop(14) = {5, -10, -13};
Plane Surface(15) = {14};
Line Loop(16) = {10, 6, -11};
Plane Surface(17) = {16};
Line Loop(18) = {7, -12, 11};
Plane Surface(19) = {18};
Line Loop(20) = {12, 8, 13};
Plane Surface(21) = {20};
Line Loop(22) = {8, 5, 6, 7, 8, 5, 6, 7};
Plane Surface(23) = {22};
Line Loop(24) = {1, 2, 3, 4};
Plane Surface(25) = {9, 24};
Extrude {0, 0, 0.5*L} {
  Surface{17, 15, 21, 19}; Layers{4};Recombine;
}
Extrude {0, 0, 0.5*L} {
  Surface{25}; Layers{4}; Recombine;
}
Physical Volume(77) = {4, 1, 2, 3};
Physical Volume(78) = {5};
Physical Surface(79) = {134};
Physical Surface(80) = {130};
Physical Surface(81) = {126};
Physical Surface(82) = {122};
Physical Surface(83) = {17, 15, 21, 19,25};
Physical Point(86) = {3};
Characteristic Length {10, 9} = 3*sl1;
