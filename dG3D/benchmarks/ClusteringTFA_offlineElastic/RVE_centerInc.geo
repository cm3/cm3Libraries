//SetFactory("OpenCASCADE");

//Fiber reinforced composite cross-section
Mesh.SecondOrderLinear = 1;
Geometry.AutoCoherence=1e-9;
//defination of unit
unit = 1.;
//characteristic mesh size
cl = 0.08*unit;
clc = 0.2*unit;
h_x = 1. * unit;
h_y = 1. * unit;
h_x0 = 0.000000 * unit;
h_y0 = 0.000000 * unit;
//h_z = 2.000000 * unit;

A_tot = h_x*h_y;

//radius = 7.1365 * unit;

vfI = 0.3;
radius = Sqrt(vfI*h_x*h_y/(3.14159265359));

A_inc = 3.14159265359*radius*radius;

Printf("vfI", A_inc/A_tot);

//defination of fibers line Loop list in zone 0
t=-1;
p1 = newp; Point(p1) = { h_x/2, h_y/2, 0.0, clc};
p2 = newp; Point(p2) = { h_x/2 - radius, h_y/2, 0.0, cl};
p3 = newp; Point(p3) = { h_x/2, h_y/2 - radius, 0.0, cl};
p4 = newp; Point(p4) = { h_x/2 + radius, h_y/2, 0.0, cl};
p5 = newp; Point(p5) = { h_x/2, h_y/2 + radius, 0.0, cl};
c1 = newreg; Circle(c1) = {p2,p1,p3};
c2 = newreg; Circle(c2) = {p3,p1,p4};
c3 = newreg; Circle(c3) = {p4,p1,p5};
c4 = newreg; Circle(c4) = {p5,p1,p2};


p6 = newp; Point(p6) = { h_x, h_y, 0.0, cl};
p9 = newp; Point(p9) = { h_x0, h_y, 0.0, cl};
p12 = newp; Point(p12) = { h_x, h_y0, 0.0, cl};
p15 = newp; Point(p15) = { h_x0, h_y0, 0.0, cl};



//defination of edge points and line

l1 = newreg; Line(l1) = {p9,p15};
l2 = newreg; Line(l2) = {p15,p12};
l3 = newreg; Line(l3) = {p12,p6};
l4 = newreg; Line(l4) = {p6,p9};


Line Loop(1) = {c1,c2,c3,c4};

Plane Surface(1) = {1};
Line Loop(2) = {l1,l2,l3,l4,1,2,3,4};
Plane Surface(2) = {2};

Point{1} In Surface{1};
Characteristic Length { 1 } = clc;

Physical Surface(52) = {1};                   //{ 7 };
Physical Surface(51) = {2};

Physical Line(100) = {l1};
Physical Line(110) = {l2};
Physical Line(101) = {l3};
Physical Line(111) = {l4};

Physical Point(5)= {p6};
Physical Point(7)= {p9};
Physical Point(3)= {p12};
Physical Point(1)= {p15};


Coherence;
