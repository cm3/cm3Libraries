from gmshpy import *
from dG3Dpy import*


################################################################################ OFFLINE ##########################################################################

# material law
lawnum1 = 11 # unique number of law
muEP = 3.0e3
kEP = 10.0e3
E = 9.*kEP*muEP/(3.*kEP+muEP) #MPa
nu = E/(2.*muEP)-1.
rho = 7850e-9 # Bulk mass
law1   = J2SmallStrainDG3DMaterialLaw(lawnum1,rho,E,nu,1e10,1e3) #dG3DLinearElasticMaterialLaw(lawnum1,rho,E,nu)

lawnum2 = 12 # unique number of law
muCF = 6.0e3
kCF = 20.0e3
E2 = 9.*kCF*muCF/(3.*kCF+muCF)
nu2 = E2/(2.*muCF)-1.
law2   = J2SmallStrainDG3DMaterialLaw(lawnum2,rho,E2,nu2,1e10,1e3) #dG3DLinearElasticMaterialLaw(lawnum2,rho,E2,nu2)

# geometry
meshfile="RVE_centerInc.msh" # name of mesh file

myfield1 = dG3DDomain(1000,51,0,lawnum1,0,2)
myfield2 = dG3DDomain(1000,52,0,lawnum2,0,2)
myfield1.stabilityParameters(1000)

# solver
sol = 2  # Gmm=0 (default) Taucs=1 PETsc=2
soltype =1 # StaticLinear=0 (default) StaticNonLinear=1
nstep = 1  # number of step (used only if soltype=1)
ftime =1  # Final time (used only if soltype=1)
tol=1.e-8  # relative tolerance for NR scheme (used only if soltype=1)
nstepArch=1 # Number of step between 2 archiving (used only if soltype=1)



# creation of Solver
mysolver = nonLinearMechSolver(1000)
mysolver.loadModel(meshfile)
mysolver.addDomain(myfield1)
mysolver.addMaterialLaw(law1)
mysolver.addDomain(myfield2)
mysolver.addMaterialLaw(law2)
mysolver.Scheme(soltype)
mysolver.Solver(sol)
mysolver.snlData(nstep,ftime,tol)
mysolver.stepBetweenArchiving(nstepArch)

# micro solver without reestimating stiffness matrix
system =3
mysolver.setSystemType(system)
mysolver.stiffnessModification(False)
mysolver.setMessageView(True)


microBC = nonLinearPeriodicBC(1000,2)
microBC.setOrder(1)
microBC.setBCPhysical(100,110,101,111)
method =5	# Periodic mesh = 0, Langrange interpolation = 1, Cubic spline interpolation =2,  FE linear= 3, FE Quad = 4, projection = 5
degree = 6	# Order used for polynomial interpolation 
addvertex = False # Polynomial interpolation by mesh vertex = 0, Polynomial interpolation by virtual vertex 
microBC.setPeriodicBCOptions(method, degree,False) 

mysolver.addMicroBC(microBC)
mysolver.stressAveragingFlag(False) # set stress averaging ON- 0 , OFF-1
mysolver.tangentAveragingFlag(False) # set tangent averaging ON -0, OFF -1

cl = Clustering()
cl.solveStrainConcentration(mysolver)

#--------------------------Clustering----------------------------------

#cl.loadStrainConcentrationDataFromFile("strainConcentrationData.csv")

cl.setNumberOfClustersForMaterial(lawnum1,3)
cl.setNumberOfClustersForMaterial(lawnum2,1)

ok = cl.computeClustersElasticity(10) # min number of element in each cluster
cl.saveAllView()
cl.saveAllView(0)
cl.saveAllView(1)
cl.saveAllView(2)
cl.saveFieldToView(Clustering.ClusterIndex)
cl.saveFieldToView(Clustering.ClusterIndex,0)
cl.saveFieldToView(Clustering.ClusterIndex,1)
cl.saveFieldToView(Clustering.ClusterIndex,2)

#check = TestCheck()
#check.equal(1,ok,1.e-6)


#--------------------------Interaction----------------------------------
law3num = 3
clusterLaw = ClusterDG3DMaterialLaw(law3num,cl)
cl.computeInteractionTensor(mysolver,clusterLaw)


################################################################################ ONLINE ##########################################################################
# material law
lawTFA =  3
TFAmethod = 0     # 0: Incremental Tangent TFA  2: Incremental Tangent HS             
fluc_corr = 0     # 0: no correction, 1: with correction


###material parameters
# material law
lawEP = 11     # unique number of law
rhoEP   = 1000.e-9
muEP = 3.0e3
kEP = 10.0e3
youngEP = 9.*kEP*muEP/(3.*kEP+muEP) #MPa
nuEP = youngEP/(2.*muEP)-1.
sy0EP   = 100.0
H_p     = 1.
m = 1.
harden = PowerLawJ2IsotropicHardening(1,  sy0EP, H_p, m)

lawCF = 12      # unique number of law
rhoCF   = 1780.e-9
muCF = 6.0e3
kCF = 20.0e3
youngCF = 9.*kCF*muCF/(3.*kCF+muCF)
nuCF = youngCF/(2.*muCF)-1.
sy0EP2   = 1000000.0
H_p2    = 100.
m2 = 1.
harden2 = PowerLawJ2IsotropicHardening(2,  sy0EP2, H_p2, m2)


# geometry
geofile="cube.geo"
meshfile="cube.msh" # name of mesh file
dim=2

# solver
sol = 2  # Gmm=0 (default) Taucs=1 PETsc=2
soltype = 1 # StaticLinear=0 (default) StaticNonLinear=1
nstep = 50   # number of step (used only if soltype=1)
ftime =1.   # Final time (used only if soltype=1)
tol=1.e-6   # relative tolerance for NR scheme (used only if soltype=1)
nstepArch=5 # Number of step between 2 archiving (used only if soltype=1)
fullDg = 0 # = CG, 1 = DG
space1 = 0 # function space (Lagrange=0)


law1 = J2SmallStrainDG3DMaterialLaw(lawEP,rhoEP,youngEP,nuEP,harden)
law2 = J2SmallStrainDG3DMaterialLaw(lawCF, rhoCF, youngCF, nuCF, harden2)

# material law
law3 = TFADG3DMaterialLaw(lawTFA, rhoEP)
law3.setTFAMethod(TFAmethod,2,fluc_corr)   ## TFA method, dimension, correction: 0 without correction (default) or 1 with correction
if TFAmethod==0:
  law3.loadClusterSummaryAndInteractionTensorsFromFiles('ClusterDataSummary_nbClusters_4.csv','InteractionTensorData_nbClusters_4.csv')
if fluc_corr==1:
  law3.loadPlasticEqStrainConcentrationFromFile('ClusterPlasticEqSummary_nbClusters_4.csv','ClusterPlasticEqSummaryGeo_nbClusters_4.csv','ClusterPlasticEqSummaryHar_nbClusters_4.csv')

  
# creation of ElasticField
nfield = 1234 # number of the field (physical number of surface)

#myfield = dG3DDomain(1000,nfield,space1,lawTFA,fullDg,3,2)
myfield = dG3DDomain(1000,nfield,space1,lawTFA,fullDg,2)
myfield.stabilityParameters(30.)
#myfield.matrixByPerturbation(1,1,1,1e-8)

# creation of Solver
mysolver = nonLinearMechSolver(1000)
mysolver.createModel(geofile,meshfile,2,2)
#mysolver.loadModel(meshfile)
mysolver.addDomain(myfield)
mysolver.addMaterialLaw(law1)
mysolver.addMaterialLaw(law2)
mysolver.addMaterialLaw(law3)

mysolver.Scheme(soltype)
mysolver.Solver(sol)
mysolver.snlData(nstep,ftime,tol)
mysolver.stepBetweenArchiving(nstepArch)

# BC
##tension along x
mysolver.displacementBC("Face",nfield,2,0.)

mysolver.displacementBC("Edge",41,0,0.)
a=1.
eps_xx_max=0.04
d1=eps_xx_max*a
cyclicFunction1=cycleFunctionTime(0.,0.,ftime/4., d1/2., ftime/2., d1, 3.*ftime/4., d1/2., ftime, 0.)
mysolver.displacementBC("Edge",23,0,cyclicFunction1)
mysolver.displacementBC("Edge",12,1,0.)




mysolver.archivingAverageValue(IPField.SIG_XX)
mysolver.archivingAverageValue(IPField.SIG_YY)
mysolver.archivingAverageValue(IPField.SIG_ZZ)
mysolver.archivingAverageValue(IPField.SIG_XY)
mysolver.archivingAverageValue(IPField.SIG_YZ)
mysolver.archivingAverageValue(IPField.SIG_XZ)
mysolver.archivingAverageValue(IPField.STRAIN_XX)
mysolver.archivingAverageValue(IPField.STRAIN_YY)
mysolver.archivingAverageValue(IPField.STRAIN_ZZ)
mysolver.archivingAverageValue(IPField.STRAIN_XY)
mysolver.archivingAverageValue(IPField.STRAIN_YZ)
mysolver.archivingAverageValue(IPField.STRAIN_XZ)
mysolver.archivingAverageValue(IPField.PLASTICSTRAIN)


mysolver.solve()

check = TestCheck()
import csv
data = csv.reader(open('E_0_GP_0_stress.csv'), delimiter=';')
strs = list(data)
check.equal(3.133567e+01,float(strs[-1][1]),1e-6)




