#coding-Utf-8-*-
from gmshpy import *
from dG3Dpy import*

#script to launch beam problem with a python script

# material law
lawnum   = 1 # unique number of law
lawcoh   = 2 # unique number of law
lawfrac  = 3 # unique number of law
rho      = 7850

GcT      = 277.;
sigmacT  = 60.e6#71.e6;
GcL      = 1.0e4# 5.4e4;
sigmacL  = 600.e6 #2205.e6;
beta     = 0.87; # ratio KII/KI
mu       = -1.#-1.; # no fracture in compression 0.41 # friction coefficient ??


fsmin    = 0.999;
fsmax    = 1.001;
lx=100.0
ly= 100.0


#longitudinal loading
Ax       = 1.; #direction of anisotropy
Ay       = 0.;
Az       = 0.;
d1=0.8e-5



# geometry
geofile="cube.geo"
meshfile="cube.msh" # name of mesh file
propertiesC2="properties_an90.i01"
PropMatfile2 = 'MF_MATprop.csv'   # For stochastic MFH

# solver
sol = 2  # Gmm=0 (default) Taucs=1 PETsc=2
soltype = 1 # StaticLinear=0 (default) StaticNonLinear=1
nstep = 100   # number of step (used only if soltype=1)
ftime =1.   # Final time (used only if soltype=1)
tol=1.e-6   # relative tolerance for NR scheme (used only if soltype=1)
nstepArch=10 # Number of step between 2 archiving (used only if soltype=1)
fullDg = 1 #O = CG, 1 = DG
space1 = 0 # function space (Lagrange=0)
nstepArchIP=nstepArch

#  compute solution and BC (given directly to the solver
# creation of law
law1 = NonLocalDamageDG3DMaterialLaw_Stoch(lawnum,rho,propertiesC2,12.5,12.5, 0.0,lx, ly, 0.0, PropMatfile2, 3) 
#law1 = NonLocalDamageDG3DMaterialLaw(lawnum,rho,propertiesC2) 
lawcoh1  = NonLocalDamageTransverseIsotropicLinearCohesive3DLaw(lawcoh,GcL, sigmacL, 0.999, GcT, sigmacT, 0.999, beta, mu, Ax, Ay, Az, fsmin, fsmax, 1.e16)
lawfrac1 = FractureByCohesive3DLaw(lawfrac,lawnum,lawcoh)


# creation of ElasticField
nfield = 10 # number of the field (physical number of surface)
myfield1 = dG3DDomain(1000,nfield,space1,lawfrac,fullDg,3,1)
myfield1.stabilityParameters(30.)
#myfield1.matrixByPerturbation(1,1,1,1e-8)
myfield1.forceCohesiveInsertionAllIPs(True,0.)
myfield1.setBulkDamageBlockedMethod(1)
# creation of Solver
mysolver = nonLinearMechSolver(1000)
mysolver.createModel(geofile,meshfile,3,2)
#mysolver.loadModel(meshfile)
mysolver.addDomain(myfield1)
mysolver.addMaterialLaw(law1)
mysolver.addMaterialLaw(lawcoh1)
mysolver.addMaterialLaw(lawfrac1)
mysolver.Scheme(soltype)
mysolver.Solver(sol)
mysolver.snlData(nstep,ftime,tol)
mysolver.stepBetweenArchiving(nstepArch)
# BC
#shearing
#mysolver.displacementBC("Face",1234,0,0.)
#mysolver.displacementBC("Face",1234,1,0.)
#mysolver.displacementBC("Face",1234,2,0.)
#mysolver.displacementBC("Face",5678,1,0.00)
#mysolver.displacementBC("Face",5678,0,0.00001)
#mysolver.forceBC("Face",2376,2,100000000)
#mysolver.forceBC("Face",4158,2,-100000000)
#mysolver.forceBC("Face",5678,0,100000000)

selUpdate=cohesiveCrackCriticalIPUpdate()
mysolver.setSelectiveUpdate(selUpdate)


#tension along z
cyclicFunction1=cycleFunctionTime(0.,0.,ftime/4., 3.*d1/4., ftime/2., d1/2., 3.*ftime/4., 3.*d1/4., ftime, d1);

mysolver.displacementBC("Face",1234,2,0.)
mysolver.displacementBC("Face",5678,2,cyclicFunction1)
mysolver.displacementBC("Face",2376,0,0.)
mysolver.displacementBC("Face",1265,1,0.)


mysolver.internalPointBuildView("svm",IPField.SVM, 1, nstepArchIP)
mysolver.internalPointBuildView("sig_xx",IPField.SIG_XX, 1, nstepArchIP)
mysolver.internalPointBuildView("sig_yy",IPField.SIG_YY, 1, nstepArchIP)
mysolver.internalPointBuildView("sig_zz",IPField.SIG_ZZ, 1, nstepArchIP)
mysolver.internalPointBuildView("sig_xy",IPField.SIG_XY, 1, nstepArchIP)
mysolver.internalPointBuildView("sig_yz",IPField.SIG_YZ, 1, nstepArchIP)
mysolver.internalPointBuildView("sig_xz",IPField.SIG_XZ, 1, nstepArchIP)
mysolver.internalPointBuildView("epl",IPField.PLASTICSTRAIN, 1, nstepArchIP)
mysolver.internalPointBuildView("damage",IPField.DAMAGE, 1, nstepArchIP)
mysolver.internalPointBuildView("epl",IPField.PLASTICSTRAIN, 1, nstepArchIP)
mysolver.internalPointBuildView("FiberDamage",IPField.INC_DAMAGE, 1, nstepArchIP)
mysolver.internalPointBuildView("MatrixDamage",IPField.MTX_DAMAGE, 1, nstepArchIP)
mysolver.archivingForceOnPhysicalGroup("Face", 1234, 2)
mysolver.archivingForceOnPhysicalGroup("Face", 5678, 2)

mysolver.archivingIPOnPhysicalGroup("Volume",nfield, IPField.INC_DAMAGE,IPField.MAX_VALUE,1)
mysolver.archivingIPOnPhysicalGroup("Volume",nfield, IPField.MTX_DAMAGE,IPField.MAX_VALUE,1)

mysolver.solve()

check = TestCheck()
check.equal(4.340306e+00,mysolver.getArchivedForceOnPhysicalGroup("Face", 5678, 2),1.e-3)

try:
  import linecache
  linesIncDam = linecache.getline('IPVolume10val_INC_DAMAGEMax.csv',100)
  linesMtxDam = linecache.getline('IPVolume10val_MTX_DAMAGEMax.csv',100)
except:
  print('Cannot read the results in the files')
  import os
  os._exit(1)
else:
  check.equal(0,float(linesIncDam.split(';')[1]))
  check.equal(2.653206e-04,float(linesMtxDam.split(';')[1]))

