mm = 1.;
L = 1*mm;
lsca = L;
Point(1) = {0,0,0,lsca};
Point(2) = {L,0,0,lsca};

//+
Line(1) = {1, 2};
//+
Physical Line(11) = {1};
//+
Physical Point(1) = {1};
//+
Physical Point(2) = {2};
//+
Transfinite Line {1} = 2 Using Progression 1.03;
