Mesh.RecombineAll = 1;
unit = 1.e-3;
r = 5*unit;
R = 10*unit;
Rs= 20*unit;

L1 = 40*unit;
L2 = 17.5*unit;

thick = 5*unit;

lsca1 = 0.1*L1;
lsca2 = 1.*R;

// SetFactory("OpenCASCADE");

Point(1) = {0,0,0,lsca1};
Point(2) = {r,0,0,lsca1};
Point(3) = {r,0.75*L1,0,lsca1};
Point(4) = {r,L1,0,lsca1};
Point(5) = {0,L1,0,lsca1};
Point(6) = {0,0.75*L1,0,lsca1};
//+
Line(1) = {1, 2};
//+
Line(2) = {2, 3};
//+
Line(3) = {3, 4};
//+
Line(4) = {4, 5};
//+
Line(5) = {5, 6};
//+
Line(6) = {6, 1};
//+
Line(7) = {6, 3};
//+
Curve Loop(1) = {5, 7, 3, 4};
//+
Surface(1) = {1};
//+
Curve Loop(2) = {6, 1, 2, -7};
//+
Surface(2) = {2};
//+
Symmetry {0, 1, 0, 0} {
   Duplicata { Surface{2}; Surface{1}; }
}
Symmetry {1, 0, 0, 0} {
  Duplicata { Surface{2}; Surface{1}; Surface{8}; Surface{13}; }
}
Extrude {0, 0, thick} {
  Surface{1,2,8,13,18,23,28,33}; Layers{2}; Recombine;
}
//+
Transfinite Curve {63, 2, 6, 61, 129, 21, 85, 11, 9, 83, 31, 173} = 9 Using Progression 1;
//+
Transfinite Curve {41, 3, 5, 39, 151, 26, 107, 16, 14, 105, 36, 195} = 5 Using Progression 1;
//+

// Physicals
Physical Volume(83) = {1,2,3,4,5,6,7,8};
//+
Physical Surface(86) = {124,212}; // lower surface
Physical Surface(84) = {58,168}; // upper surface
//+
Physical Point(1) = {23}; // centre-back lower surface
//+
Physical Point(2) = {32}; // right-back lower surface
//+
Physical Point(3) = {4}; // right-back upper surface
//+
Physical Point(4) = {5}; // centre-back upper surface
//+
Physical Point(5) = {3}; // right-back gauge-top surface
//+
Physical Point(6) = {6}; // right-back middle of gauge-top and top surface
//+
Physical Point(7) = {1}; // centre-back middle surface
//+
Physical Point(8) = {2}; // centre-right middle surface
