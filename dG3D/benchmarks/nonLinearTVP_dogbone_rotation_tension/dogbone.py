#coding-Utf-8-*-
from gmshpy import *
# from dG3DpyDebug import*
from dG3Dpy import*
from math import*
import csv, os
import numpy as np

# Script to launch dogbone problem with a python script
lawnum = 1 # unique number of law

# Load the csv data for relaxation spectrum
with open('TPU_relaxationSpectrum_Et_N27_31_07_24_Trefm30.txt', newline='') as csvfile:
    reader = csv.reader(csvfile, delimiter=';')
    relSpec = np.zeros((1,))
    i = 0
    for row in reader:
        if i == 0:
            relSpec[i] = float(' '.join(row))
        else:
            relSpec = np.append(relSpec, float(' '.join(row)))
        i += 1
# print(relSpec)
N = int(0.5*(i-1))
relSpec[0:N+1] *= 1e-6    # convert to MPa

E = relSpec[0] #MPa # 2700
nu = 0.4 #0.33
K = E/3./(1.-2.*nu)	# Bulk mudulus
mu = E/2./(1.+nu)	  # Shear mudulus
rho = 1110e-12 # Bulk mass   1250 kg/m3 -> 1250e-12 tonne/mm3
Cp = rho*1850e+6 # 1500 J/kg/K -> 1500e+6 Nmm/tonne/K
KThCon = 0.2332  # 0.14 W/m/K -> 0.14 Nmm/s/mm/K
Alpha = 1.5e-4 # 1/K

Tinitial = 273.15 + 23 # K
C1 = 524.39721767 #
C2 = 699.76815543 #

Tref = 273.15-30.

# Default tempfuncs
Cp_G = rho*1700e+6
Cp_R = rho*2100e+6
Alpha_G = 0.2e-6
Alpha_R = 1.e-6
KThCon_G = 0.10
KThCon_R = 1.5*0.12
k_gibson = 3.36435338e-02
m_gibson = 0.0
Tg = Tref
Alpha_tempfunc = GlassTransitionScalarFunction(1, Alpha_R/Alpha_G, Tg, k_gibson, m_gibson, 0)
Cp_tempfunc = GlassTransitionScalarFunction(1, Cp_R/Cp_G, Tg, k_gibson, m_gibson, 0)
KThCon_tempfunc = GlassTransitionScalarFunction(1, KThCon_R/KThCon_G, Tg, k_gibson, m_gibson, 0)
ShiftFactor_tempfunc = negExpWLFshiftFactor(C1,C2,Tref)

C = 0.0228422
sy0c = 6.02534075/0.78
negExp_tempfunc = negativeExponentialFunction(C,Tref)

a = -0.9/(90.)
x0 = 273.15-20
linear_tempfunc = linearScalarFunction(x0,0.9,a)

# isoHard and tempfuncs
hardenc = LinearExponentialJ2IsotropicHardening(1, 4.5, 5.0, 5., 5.)
hardent = LinearExponentialJ2IsotropicHardening(2, 4.5, 5.0, 7.5, 12.5)
# hardenc.setTemperatureFunction_h1(linear_tempfunc)
# hardenc.setTemperatureFunction_h2(linear_tempfunc)
# hardenc.setTemperatureFunction_hexp(ShiftFactor_tempfunc)
# hardent.setTemperatureFunction_h1(linear_tempfunc)
# hardent.setTemperatureFunction_h2(linear_tempfunc)
# hardent.setTemperatureFunction_hexp(ShiftFactor_tempfunc)

hardenk = PolynomialKinematicHardening(3,1)
hardenk.setCoefficients(0,20.)
hardenk.setCoefficients(1,25.)
# hardenk.setTemperatureFunction_K(linear_tempfunc)


law1 = NonLinearTVENonLinearTVPDG3DMaterialLaw(lawnum,rho,E,nu,1e-6,Tinitial,Alpha,KThCon,Cp,False,1e-8,1e-6)
law1.setCompressionHardening(hardenc)
law1.setTractionHardening(hardent)
law1.setKinematicHardening(hardenk)

law1.setYieldPowerFactor(3.5) # .5)
law1.setNonAssociatedFlow(True)
law1.nonAssociatedFlowRuleFactor(0.5*0.64) # 5)

law1.setStrainOrder(-1)
law1.setViscoelasticMethod(0)
law1.setShiftFactorConstantsWLF(C1,C2)
law1.setReferenceTemperature(Tref)

law1.useRotationCorrectionBool(True,2) # bool, int_Scheme = 0 -> R0, 1 -> R1

# extraHyper

law1.setExtraBranchNLType(5)
law1.useExtraBranchBool(True)
law1.useExtraBranchBool_TVE(True,5)
law1.setVolumeCorrection(0.15, 1500.0, 0.75, 0.15, 150.0, 0.75)
law1.setAdditionalVolumeCorrections(8.5, 0.75, 0.028, 8.5, .075, 0.028)
law1.setExtraBranch_CompressionParameter(1.,0.,0.) # 1.5
law1.setTensionCompressionRegularisation(1000.)
law1.setViscoElasticNumberOfElement(N)
if N > 0:
    for i in range(1, N+1):
        law1.setViscoElasticData(i, relSpec[i], relSpec[i+N])
        law1.setCorrectionsAllBranchesTVE(i, 0.15, 1500.0, 0.75, 0.15, 150.0, 0.75)
        law1.setCompressionCorrectionsAllBranchesTVE(i, 1.0)
        law1.setAdditionalCorrectionsAllBranchesTVE(i, 8.5, 0.75, 0.028, 8.5, .075, 0.028)


# Mullins Effect
mullins = linearScaler(4, 0.99)
a = -0.9/(90.)
x0 = 273.15-20
linear_tempfunc = linearScalarFunction(x0,0.55,a)
# mullins.setTemperatureFunction_r(linear_tempfunc)
law1.setMullinsEffect(mullins)

# Viscosity - TVP
eta = constantViscosityLaw(1,1.e+3)
law1.setViscosityEffect(eta,0.21)
# eta.setTemperatureFunction(ShiftFactor_tempfunc)

# geometry
geofile="dogbone_ISO527.geo"
meshfile="dogbone_ISO527.msh" # name of mesh file

# solver
sol = 2  # Gmm=0 (default) Taucs=1 PETsc=2
soltype = 1 # StaticLinear=0 (default) StaticNonLinear=1
nstep = 1500   # number of step (used only if soltype=1)
ftime = 1.8/7.37e-4 # 0.001   # Final time (used only if soltype=1)
#reduce for battery
nstep = 500
ftime = 1.8/7.37e-4/20.
tol=1.e-5 # relative tolerance for NR scheme (used only if soltype=1)
nstepArch=10 # Number of step between 2 archiving (used only if soltype=1)
fullDg = 0 #O = CG, 1 = DG
space1 = 0 # function space (Lagrange=0)
beta1  = 100
averageStrainBased = False

# creation of ElasticField
# myfield1 = dG3DDomain(1000,83,0,lawnum,fullDG,3,0)
# myfield1.stabilityParameters(beta1)
#myfield1.averageStrainBased(averageStrainBased)

nfield = 83

pertFactor = 1e-8
pertLaw1 = dG3DMaterialLawWithTangentByPerturbation(law1,pertFactor)
ThermoMechanicsEqRatio = 1.e1 # setConstitutiveExtraDofDiffusionEqRatio(ThermoMechanicsEqRatio)
thermalSource = True
mecaSource = True
myfield1 = ThermoMechanicsDG3DDomain(1000,nfield,space1,lawnum,fullDg,ThermoMechanicsEqRatio)
myfield1.setConstitutiveExtraDofDiffusionAccountSource(thermalSource,mecaSource)
myfield1.stabilityParameters(beta1)
myfield1.ThermoMechanicsStabilityParameters(beta1,bool(1))
myfield1.strainSubstep(2, 20)

# creation of Solver
mysolver = nonLinearMechSolver(1000)
mysolver.createModel(geofile,meshfile,3,2)
# mysolver.loadModel(meshfile)
mysolver.addDomain(myfield1)
mysolver.addMaterialLaw(pertLaw1)
mysolver.Scheme(soltype)
mysolver.Solver(sol)
mysolver.snlData(nstep,ftime,tol,tol/100.)
mysolver.stepBetweenArchiving(nstepArch)
mysolver.options("-ksp_type preonly -pc_type lu -pc_factor_mat_solver_type mumps")
stiffModif = BFGSStiffnessModificationMonitoring(20, True)
mysolver.stiffnessModification(stiffModif)

mysolver.snlManageTimeStep(500,2,2.,100)
# maxIterations # timeStepLimit below which increase time-step if reduced before for iterations more than maximal # factor of reduction/increase # no.of fails allowed
optimalNumber = 5
maximalStep = 3.e-1
minimalStep = 1e-3
power = 1
maximalFailStep = 10
# mysolver.setTimeIncrementAdaptation(True,optimalNumber,power,maximalStep,minimalStep,maximalFailStep) # automatic time stepping

# Dimension
face_to_face_length = 0.07073*2

# BC
fu_L = PiecewiseLinearFunction()
fu_L.put(0,0)
#reduce for battery
fu_L.put(ftime,1.8*face_to_face_length/20.)
#fu_L.put(ftime,1.8*face_to_face_length) # strain rate = 1e-2

mysolver.displacementBC("Face",86,0,0.)
mysolver.displacementBC("Face",86,1,0.)
mysolver.displacementBC("Face",86,2,0.)
mysolver.displacementBC("Face",84,0,0.)
method = 0
withPF = False
mysolver.pathFollowing(withPF,method)
if method == 0:
    mysolver.setPathFollowingIncrementAdaptation(True,4)
    mysolver.setPathFollowingControlType(0)
    mysolver.setPathFollowingCorrectionMethod(0)
    mysolver.setPathFollowingArcLengthStep(1e-3)
    mysolver.setBoundsOfPathFollowingArcLengthSteps(0,1.);
else:
    # time-step adaptation by number of NR iterations
    mysolver.setPathFollowingIncrementAdaptation(True,4)
    mysolver.setPathFollowingLocalSteps(1e-2,1.)
    mysolver.setPathFollowingSwitchCriterion(0.)
    mysolver.setPathFollowingLocalIncrementType(1);
    mysolver.setBoundsOfPathFollowingLocalSteps(1.,3.)

if withPF == True:
    mysolver.sameDisplacementBC("Face",84,3,1)
    mysolver.forceBC("Node",3,1,1e0)
else:
    mysolver.displacementBC("Face",84,1,fu_L)
mysolver.displacementBC("Face",84,2,0.)

# Added
#mysolver.forceBC("Face",85,1,2e4)
#mysolver.forceBC("Face",86,0,2e4)
#mysolver.displacementBC("Face",87,0,0.)
mysolver.initialBC("Volume","Position",nfield,3,Tinitial)
fT = LinearFunctionTime(0,Tinitial,ftime,Tinitial);    # Linear Displacement with time
# mysolver.displacementBC("Volume",nfield,3,fT)


mysolver.internalPointBuildView("svm",IPField.SVM, 1, nstepArch)
mysolver.internalPointBuildView("sig_xx",IPField.SIG_XX, 1, nstepArch)
mysolver.internalPointBuildView("sig_yy",IPField.SIG_YY, 1, nstepArch)
mysolver.internalPointBuildView("sig_zz",IPField.SIG_ZZ, 1, nstepArch)
mysolver.internalPointBuildView("sig_xy",IPField.SIG_XY, 1, nstepArch)
mysolver.internalPointBuildView("sig_yz",IPField.SIG_YZ, 1, nstepArch)
mysolver.internalPointBuildView("sig_xz",IPField.SIG_XZ, 1, nstepArch)
mysolver.internalPointBuildView("epl",IPField.PLASTICSTRAIN, 1, nstepArch)
mysolver.internalPointBuildView("temperature",IPField.TEMPERATURE, 1, nstepArch)
mysolver.internalPointBuildView("mechSrc",IPField.MECHANICAL_SOURCE, 1, nstepArch)
mysolver.internalPointBuildView("plastic possion ratio",IPField.PLASTIC_POISSON_RATIO,1,nstepArch)

mysolver.archivingForceOnPhysicalGroup("Face", 84, 1)
mysolver.archivingForceOnPhysicalGroup("Face", 86, 1)
# mysolver.archivingForceOnPhysicalGroup("Node", 5, 1)
mysolver.archivingNodeDisplacement(2,1,1)
mysolver.archivingNodeDisplacement(4,1,1)
mysolver.archivingNodeDisplacement(5,1,1)
mysolver.archivingNodeDisplacement(3,1,1)
mysolver.archivingNodeDisplacement(6,1,1)
mysolver.archivingNodeDisplacement(5,3,1)
mysolver.archivingNodeDisplacement(3,3,1)
mysolver.archivingNodeDisplacement(6,3,1)
mysolver.archivingNodeDisplacement(7,3,1)
mysolver.archivingNodeDisplacement(8,3,1)

mysolver.solve()

check = TestCheck()
check.equal(-2.218809e-04,mysolver.getArchivedForceOnPhysicalGroup("Face", 86, 1),1.e-4)
