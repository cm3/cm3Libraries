# test file

set(PYFILE twoHole.py)

set(FILES2DELETE 
  disp*.msh  
  *.csv
  stress*.msh
  twoHole.msh
)

add_cm3python_test(${PYFILE} "${FILES2DELETE}")
