import os,csv
#from gmshpy import *
#from dG3Dpy import*
import sys
def checkEqual(ref, cur, tol):
  if(abs(ref-cur)>abs(tol*ref)):
    print("Error : reference  current value ",ref," current value ",cur," relative error ", (ref-cur)/ref," tolerance ",tol)


if sys.version_info[0] < 3:
  os.system('python idealHole.py')
else:
  os.system('python3 idealHole.py')

data1 = csv.reader(open('E_0_GP_0_stress.csv'), delimiter=';')
force = list(data1)
checkEqual(136.2346828408006,float(force[-1][1]),1e-5)
data2 = csv.reader(open('E_0_GP_0_tangent.csv'), delimiter=';')
disp = list(data2)
checkEqual(453.2326749671896,float(disp[-1][1]),1e-5)

if sys.version_info[0] < 3:
  os.system('python idealHole.py')
else:
  os.system('python3 idealHole.py')

data1 = csv.reader(open('E_0_GP_0_stress.csv'), delimiter=';')
force = list(data1)
checkEqual(136.2346828408006,float(force[-1][1]),1e-5)
data2 = csv.reader(open('E_0_GP_0_tangent.csv'), delimiter=';')
disp = list(data2)
checkEqual(453.2326749671896,float(disp[-1][1]),1e-5)
