clear
clc
clf
close all
%graphics_toolkit fltk 

%---TEMPERATURE-----------------------------------------------------------------
XYZ =dlmread('IPVolume10val_TEMPERATUREMean.csv');
dlmwrite('temp.csv',XYZ,'delimiter',';');
XYZ =dlmread('temp.csv');
t= XYZ(:, 1);
T_Mean= XYZ(:, 2)-273;
delete('temp.csv')

XYZ =dlmread('NodalDisplacementPhysical7Num7comp3.csv');
dlmwrite('temp.csv',XYZ,'delimiter',';');
XYZ =dlmread('temp.csv');
t= XYZ(:, 1);
T_7= XYZ(:, 2)-273;
delete('temp.csv')


XYZ =dlmread('IPVolume10val_FthG_YYMean.csv');
dlmwrite('temp.csv',XYZ,'delimiter',';');
XYZ =dlmread('temp.csv');
FthG_YYMean=     XYZ(:, 2);
delete('temp.csv')

XYZ =dlmread('IPVolume10val_FthG_ZZMean.csv');
dlmwrite('temp.csv',XYZ,'delimiter',';');
XYZ =dlmread('temp.csv');
FthG_ZZMean=     XYZ(:, 2);
delete('temp.csv')

XYZ =dlmread('IPVolume10val_FthR_YYMean.csv');
dlmwrite('temp.csv',XYZ,'delimiter',';');
XYZ =dlmread('temp.csv');
FthR_YYMean=     XYZ(:, 2);
delete('temp.csv')

XYZ =dlmread('IPVolume10val_FthR_ZZMean.csv');
dlmwrite('temp.csv',XYZ,'delimiter',';');
XYZ =dlmread('temp.csv');
FthR_ZZMean=     XYZ(:, 2);
delete('temp.csv')

XYZ =dlmread('IPVolume10val_FthAM_YYMean.csv');
dlmwrite('temp.csv',XYZ,'delimiter',';');
XYZ =dlmread('temp.csv');
FthAM_YYMean=     XYZ(:, 2);
delete('temp.csv')

XYZ =dlmread('IPVolume10val_FthAM_ZZMean.csv');
dlmwrite('temp.csv',XYZ,'delimiter',';');
XYZ =dlmread('temp.csv');
FthAM_ZZMean=     XYZ(:, 2);
delete('temp.csv')


XYZ =dlmread('IPVolume10val_FthCisot_YYMean.csv');
dlmwrite('temp.csv',XYZ,'delimiter',';');
XYZ =dlmread('temp.csv');
FthI_YYMean=     XYZ(:, 2);
delete('temp.csv')

XYZ =dlmread('IPVolume10val_FthCisot_ZZMean.csv');
dlmwrite('temp.csv',XYZ,'delimiter',';');
XYZ =dlmread('temp.csv');
FthI_ZZMean=     XYZ(:, 2);
delete('temp.csv')


XYZ =dlmread('IPVolume10val_FthAD_YYMean.csv');
dlmwrite('temp.csv',XYZ,'delimiter',';');
XYZ =dlmread('temp.csv');
FthAD_YYMean=     XYZ(:, 2);
delete('temp.csv')

XYZ =dlmread('IPVolume10val_FthAD_ZZMean.csv');
dlmwrite('temp.csv',XYZ,'delimiter',';');
XYZ =dlmread('temp.csv');
FthAD_ZZMean=     XYZ(:, 2);
delete('temp.csv')

%---thermal flux-----------------------------------------------------------------
XYZ =dlmread('IPVolume10val_THERMALFLUX_XMean.csv');
dlmwrite('temp.csv',XYZ,'delimiter',';');
XYZ =dlmread('temp.csv');
q_x= XYZ(:, 2);
delete('temp.csv')

XYZ =dlmread('IPVolume10val_THERMALFLUX_YMean.csv');
dlmwrite('temp.csv',XYZ,'delimiter',';');
XYZ =dlmread('temp.csv');
q_y= XYZ(:, 2);
delete('temp.csv')

XYZ =dlmread('IPVolume10val_THERMALFLUX_ZMean.csv');
dlmwrite('temp.csv',XYZ,'delimiter',';');
XYZ =dlmread('temp.csv');
q_z= XYZ(:, 2);
delete('temp.csv')


XYZ =dlmread('force4158comp3.csv');
dlmwrite('temp.csv',XYZ,'delimiter',';');
XYZ =dlmread('temp.csv');
F4158_T= XYZ(:, 2);
delete('temp.csv')

XYZ =dlmread('force1265comp3.csv');
dlmwrite('temp.csv',XYZ,'delimiter',';');
XYZ =dlmread('temp.csv');
F1265_T= XYZ(:, 2);
delete('temp.csv')

XYZ =dlmread('force1234comp3.csv');
dlmwrite('temp.csv',XYZ,'delimiter',';');
XYZ =dlmread('temp.csv');
F1234_T= XYZ(:, 2);
delete('temp.csv')


XYZ =dlmread('force2376comp3.csv');
dlmwrite('temp.csv',XYZ,'delimiter',';');
XYZ =dlmread('temp.csv');
F2376_T= XYZ(:, 2);
delete('temp.csv')

XYZ =dlmread('force3487comp3.csv');
dlmwrite('temp.csv',XYZ,'delimiter',';');
XYZ =dlmread('temp.csv');
F3487_T= XYZ(:, 2);
delete('temp.csv')

%---NODAL TEMP-----------------------------------------------------------------------
XYZ =dlmread('NodalDisplacementPhysical1Num1comp3.csv');
dlmwrite('temp.csv',XYZ,'delimiter',';');
XYZ =dlmread('temp.csv');
T_Pt1= XYZ(:, 2);
delete('temp.csv')

XYZ =dlmread('NodalDisplacementPhysical2Num2comp3.csv');
dlmwrite('temp.csv',XYZ,'delimiter',';');
XYZ =dlmread('temp.csv');
T_Pt2= XYZ(:, 2);
delete('temp.csv')

XYZ =dlmread('NodalDisplacementPhysical3Num3comp3.csv');
dlmwrite('temp.csv',XYZ,'delimiter',';');
XYZ =dlmread('temp.csv');
T_Pt3= XYZ(:, 2);
delete('temp.csv')

XYZ =dlmread('NodalDisplacementPhysical4Num4comp3.csv');
dlmwrite('temp.csv',XYZ,'delimiter',';');
XYZ =dlmread('temp.csv');
T_Pt4= XYZ(:, 2);
delete('temp.csv')

XYZ =dlmread('NodalDisplacementPhysical5Num5comp3.csv');
dlmwrite('temp.csv',XYZ,'delimiter',';');
XYZ =dlmread('temp.csv');
T_Pt5= XYZ(:, 2);
delete('temp.csv')

XYZ =dlmread('NodalDisplacementPhysical6Num6comp3.csv');
dlmwrite('temp.csv',XYZ,'delimiter',';');
XYZ =dlmread('temp.csv');
T_Pt6= XYZ(:, 2);
delete('temp.csv')

XYZ =dlmread('NodalDisplacementPhysical7Num7comp3.csv');
dlmwrite('temp.csv',XYZ,'delimiter',';');
XYZ =dlmread('temp.csv');
T_Pt7= XYZ(:, 2);
delete('temp.csv')

XYZ =dlmread('NodalDisplacementPhysical8Num8comp3.csv');
dlmwrite('temp.csv',XYZ,'delimiter',';');
XYZ =dlmread('temp.csv');
T_Pt8= XYZ(:, 2);
delete('temp.csv')


%---DISPLACEMENT-----------------------------------------------------------------
XYZ =dlmread('NodalDisplacementPhysical7Num7comp0.csv');
dlmwrite('temp.csv',XYZ,'delimiter',';');
XYZ =dlmread('temp.csv');
UX_Pt7=     XYZ(:, 2)*1e3;
delete('temp.csv')

XYZ =dlmread('NodalDisplacementPhysical7Num7comp1.csv');
dlmwrite('temp.csv',XYZ,'delimiter',';');
XYZ =dlmread('temp.csv');
UY_Pt7= XYZ(:, 2)*1e3;
delete('temp.csv')

XYZ =dlmread('NodalDisplacementPhysical7Num7comp2.csv');
dlmwrite('temp.csv',XYZ,'delimiter',';');
XYZ =dlmread('temp.csv');
UZ_Pt7= XYZ(:, 2)*1e3;
delete('temp.csv')

%---PLASTIC STRAIN--------------------------------------------------------------
XYZ =dlmread('IPVolume10val_PLASTICSTRAINMean.csv');
dlmwrite('temp.csv',XYZ,'delimiter',';');
XYZ =dlmread('temp.csv');
PLASTICSTRAINMean=  XYZ(:, 2);
delete('temp.csv')

XYZ =dlmread('IPVolume10val_epsRMean.csv');
dlmwrite('temp.csv',XYZ,'delimiter',';');
XYZ =dlmread('temp.csv');
PLASTICSTRAINRMean=  XYZ(:, 2);
delete('temp.csv')

XYZ =dlmread('IPVolume10val_epsAMMean.csv');
dlmwrite('temp.csv',XYZ,'delimiter',';');
XYZ =dlmread('temp.csv');
PLASTICSTRAINAMMean=  XYZ(:, 2);
delete('temp.csv')


%---transition--------------------------------------------------------------
XYZ =dlmread('IPVolume10val_TcrysMean.csv');
dlmwrite('temp.csv',XYZ,'delimiter',';');
XYZ =dlmread('temp.csv');
TcrysMean=  XYZ(:, 2);
delete('temp.csv')

XYZ =dlmread('IPVolume10val_TmeltMean.csv');
dlmwrite('temp.csv',XYZ,'delimiter',';');
XYZ =dlmread('temp.csv');
TmeltMean=  XYZ(:, 2);
delete('temp.csv')

XYZ =dlmread('IPVolume10val_WcrysMean.csv');
dlmwrite('temp.csv',XYZ,'delimiter',';');
XYZ =dlmread('temp.csv');
WcrysMean=  XYZ(:, 2);
delete('temp.csv')

XYZ =dlmread('IPVolume10val_WmeltMean.csv');
dlmwrite('temp.csv',XYZ,'delimiter',';');
XYZ =dlmread('temp.csv');
WmeltMean=  XYZ(:, 2);
delete('temp.csv')


XYZ =dlmread('IPVolume10val_EnThG_YYMean.csv');
dlmwrite('temp.csv',XYZ,'delimiter',';');
XYZ =dlmread('temp.csv');
EnThG_YYMean=  XYZ(:, 2);
delete('temp.csv')

XYZ =dlmread('IPVolume10val_EnThG_ZZMean.csv');
dlmwrite('temp.csv',XYZ,'delimiter',';');
XYZ =dlmread('temp.csv');
EnThG_ZZMean=  XYZ(:, 2);
delete('temp.csv')

XYZ =dlmread('IPVolume10val_EnThR_YYMean.csv');
dlmwrite('temp.csv',XYZ,'delimiter',';');
XYZ =dlmread('temp.csv');
EnThR_YYMean=  XYZ(:, 2);
delete('temp.csv')

XYZ =dlmread('IPVolume10val_EnThR_ZZMean.csv');
dlmwrite('temp.csv',XYZ,'delimiter',';');
XYZ =dlmread('temp.csv');
EnThR_ZZMean=  XYZ(:, 2);
delete('temp.csv')

XYZ =dlmread('IPVolume10val_ADl_YYMean.csv');
dlmwrite('temp.csv',XYZ,'delimiter',';');
XYZ =dlmread('temp.csv');
ADl_YYMean=  XYZ(:, 2);
delete('temp.csv')

XYZ =dlmread('IPVolume10val_ADl_ZZMean.csv');
dlmwrite('temp.csv',XYZ,'delimiter',';');
XYZ =dlmread('temp.csv');
ADl_ZZMean=  XYZ(:, 2);
delete('temp.csv')

XYZ =dlmread('IPVolume10val_EnThAM_YYMean.csv');
dlmwrite('temp.csv',XYZ,'delimiter',';');
XYZ =dlmread('temp.csv');
EnThAM_YYMean=  XYZ(:, 2);
delete('temp.csv')

XYZ =dlmread('IPVolume10val_EnThAM_ZZMean.csv');
dlmwrite('temp.csv',XYZ,'delimiter',';');
XYZ =dlmread('temp.csv');
EnThAM_ZZMean=  XYZ(:, 2);
delete('temp.csv')

%---SVM-------------------------------------------------------------------------
XYZ =dlmread('IPVolume10val_SVMMean.csv');
dlmwrite('temp.csv',XYZ,'delimiter',';');
XYZ =dlmread('temp.csv');
SVM=  XYZ(:, 2)/1e6;
delete('temp.csv')

%---STRAIN----------------------------------------------------------------------
XYZ =dlmread('IPVolume10val_STRAIN_XXMean.csv');
dlmwrite('temp.csv',XYZ,'delimiter',';');
XYZ =dlmread('temp.csv');
STRAIN_XXMean=     XYZ(:, 2);
delete('temp.csv')

XYZ =dlmread('IPVolume10val_STRAIN_YYMean.csv');
dlmwrite('temp.csv',XYZ,'delimiter',';');
XYZ =dlmread('temp.csv');
STRAIN_YYMean= XYZ(:, 2);
delete('temp.csv')

XYZ =dlmread('IPVolume10val_STRAIN_ZZMean.csv');
dlmwrite('temp.csv',XYZ,'delimiter',';');
XYZ =dlmread('temp.csv');
STRAIN_ZZMean= XYZ(:, 2);
delete('temp.csv')

XYZ =dlmread('IPVolume10val_STRAIN_XYMean.csv');
dlmwrite('temp.csv',XYZ,'delimiter',';');
XYZ =dlmread('temp.csv');
STRAIN_XYMean= XYZ(:, 2);
delete('temp.csv')

XYZ =dlmread('IPVolume10val_STRAIN_YZMean.csv');
dlmwrite('temp.csv',XYZ,'delimiter',';');
XYZ =dlmread('temp.csv');
STRAIN_YZMean= XYZ(:, 2);
delete('temp.csv')

XYZ =dlmread('IPVolume10val_STRAIN_XZMean.csv');
dlmwrite('temp.csv',XYZ,'delimiter',';');
XYZ =dlmread('temp.csv');
STRAIN_XZMean= XYZ(:, 2);
delete('temp.csv')

%---STRESS----------------------------------------------------------------------
XYZ =dlmread('IPVolume10val_SIG_XXMean.csv');
dlmwrite('temp.csv',XYZ,'delimiter',';');
XYZ =dlmread('temp.csv');
SIG_XXMean=     XYZ(:, 2)/1e6;
delete('temp.csv')

XYZ =dlmread('IPVolume10val_SIG_YYMean.csv');
dlmwrite('temp.csv',XYZ,'delimiter',';');
XYZ =dlmread('temp.csv');
SIG_YYMean= XYZ(:, 2)/1e6;
delete('temp.csv')

XYZ =dlmread('IPVolume10val_SIG_ZZMean.csv');
dlmwrite('temp.csv',XYZ,'delimiter',';');
XYZ =dlmread('temp.csv');
SIG_ZZMean= XYZ(:, 2)/1e6;
delete('temp.csv')

XYZ =dlmread('IPVolume10val_SIG_XYMean.csv');
dlmwrite('temp.csv',XYZ,'delimiter',';');
XYZ =dlmread('temp.csv');
SIG_XYMean= XYZ(:, 2)/1e6;
delete('temp.csv')

XYZ =dlmread('IPVolume10val_SIG_YZMean.csv');
dlmwrite('temp.csv',XYZ,'delimiter',';');
XYZ =dlmread('temp.csv');
SIG_YZMean= XYZ(:, 2)/1e6;
delete('temp.csv')

XYZ =dlmread('IPVolume10val_SIG_XZMean.csv');
dlmwrite('temp.csv',XYZ,'delimiter',';');
XYZ =dlmread('temp.csv');
SIG_XZMean= XYZ(:, 2)/1e6;
delete('temp.csv')

%%---FORCE-----------------------------------------------------------------------
XYZ =dlmread('force1234comp0.csv');
dlmwrite('temp.csv',XYZ,'delimiter',';');
XYZ =dlmread('temp.csv');
F1234_X=     XYZ(:, 2);
delete('temp.csv')

XYZ =dlmread('force1234comp1.csv');
dlmwrite('temp.csv',XYZ,'delimiter',';');
XYZ =dlmread('temp.csv');
F1234_Y= XYZ(:, 2);
delete('temp.csv')

XYZ =dlmread('force1234comp2.csv');
dlmwrite('temp.csv',XYZ,'delimiter',';');
XYZ =dlmread('temp.csv');
F1234_Z= XYZ(:, 2);
delete('temp.csv')

XYZ =dlmread('force5678comp3.csv');
dlmwrite('temp.csv',XYZ,'delimiter',';');
XYZ =dlmread('temp.csv');
F5678_T= XYZ(:, 2);
delete('temp.csv')

%---PRESSURE--------------------------------------------------------------------
XYZ =dlmread('IPVolume10val_P_XXMean.csv');
dlmwrite('temp.csv',XYZ,'delimiter',';');
XYZ =dlmread('temp.csv');
P_XXMean=     XYZ(:, 2)/1e6;
delete('temp.csv')

XYZ =dlmread('IPVolume10val_P_YYMean.csv');
dlmwrite('temp.csv',XYZ,'delimiter',';');
XYZ =dlmread('temp.csv');
P_YYMean= XYZ(:, 2)/1e6;
delete('temp.csv')

XYZ =dlmread('IPVolume10val_P_ZZMean.csv');
dlmwrite('temp.csv',XYZ,'delimiter',';');
XYZ =dlmread('temp.csv');
P_ZZMean= XYZ(:, 2)/1e6;
delete('temp.csv')

%---SMP----------------------------------------------------------------------
XYZ =dlmread('IPVolume10val_FfG_YYMean.csv');
dlmwrite('temp.csv',XYZ,'delimiter',';');
XYZ =dlmread('temp.csv');
FfG_YYMean= XYZ(:, 2);
delete('temp.csv')

XYZ =dlmread('IPVolume10val_FpG_YYMean.csv');
dlmwrite('temp.csv',XYZ,'delimiter',';');
XYZ =dlmread('temp.csv');
FpG_YYMean= XYZ(:, 2);
delete('temp.csv')

XYZ =dlmread('IPVolume10val_FveG_YYMean.csv');
dlmwrite('temp.csv',XYZ,'delimiter',';');
XYZ =dlmread('temp.csv');
FveG_YYMean= XYZ(:, 2);
delete('temp.csv')

XYZ =dlmread('IPVolume10val_FP_YYMean.csv');
dlmwrite('temp.csv',XYZ,'delimiter',';');
XYZ =dlmread('temp.csv');
FfR_YYMean= XYZ(:, 2);
delete('temp.csv')

XYZ =dlmread('IPVolume10val_FpR_YYMean.csv');
dlmwrite('temp.csv',XYZ,'delimiter',';');
XYZ =dlmread('temp.csv');
FpR_YYMean= XYZ(:, 2);
delete('temp.csv')


XYZ =dlmread('IPVolume10val_FveR_YYMean.csv');
dlmwrite('temp.csv',XYZ,'delimiter',';');
XYZ =dlmread('temp.csv');
FveR_YYMean= XYZ(:, 2);
delete('temp.csv')



XYZ =dlmread('IPVolume10val_FfAM_YYMean.csv');
dlmwrite('temp.csv',XYZ,'delimiter',';');
XYZ =dlmread('temp.csv');
FfAM_YYMean= XYZ(:, 2);
delete('temp.csv')

XYZ =dlmread('IPVolume10val_FpAM_YYMean.csv');
dlmwrite('temp.csv',XYZ,'delimiter',';');
XYZ =dlmread('temp.csv');
FpAM_YYMean= XYZ(:, 2);
delete('temp.csv')

XYZ =dlmread('IPVolume10val_FveAM_YYMean.csv');
dlmwrite('temp.csv',XYZ,'delimiter',';');
XYZ =dlmread('temp.csv');
FveAM_YYMean= XYZ(:, 2);
delete('temp.csv')

XYZ =dlmread('IPVolume10val_F_XXMean.csv');
dlmwrite('temp.csv',XYZ,'delimiter',';');
XYZ =dlmread('temp.csv');
F_XXMean= XYZ(:, 2);
delete('temp.csv')

XYZ =dlmread('IPVolume10val_F_YYMean.csv');
dlmwrite('temp.csv',XYZ,'delimiter',';');
XYZ =dlmread('temp.csv');
F_YYMean= XYZ(:, 2);
delete('temp.csv')



XYZ =dlmread('IPVolume10val_FfG_ZZMean.csv');
dlmwrite('temp.csv',XYZ,'delimiter',';');
XYZ =dlmread('temp.csv');
FfG_ZZMean= XYZ(:, 2);
delete('temp.csv')

XYZ =dlmread('IPVolume10val_FpG_ZZMean.csv');
dlmwrite('temp.csv',XYZ,'delimiter',';');
XYZ =dlmread('temp.csv');
FpG_ZZMean= XYZ(:, 2);
delete('temp.csv')

XYZ =dlmread('IPVolume10val_FveG_ZZMean.csv');
dlmwrite('temp.csv',XYZ,'delimiter',';');
XYZ =dlmread('temp.csv');
FveG_ZZMean= XYZ(:, 2);
delete('temp.csv')

XYZ =dlmread('IPVolume10val_FP_ZZMean.csv');
dlmwrite('temp.csv',XYZ,'delimiter',';');
XYZ =dlmread('temp.csv');
FfR_ZZMean= XYZ(:, 2);
delete('temp.csv')

XYZ =dlmread('IPVolume10val_FpR_ZZMean.csv');
dlmwrite('temp.csv',XYZ,'delimiter',';');
XYZ =dlmread('temp.csv');
FpR_ZZMean= XYZ(:, 2);
delete('temp.csv')

XYZ =dlmread('IPVolume10val_FveR_ZZMean.csv');
dlmwrite('temp.csv',XYZ,'delimiter',';');
XYZ =dlmread('temp.csv');
FveR_ZZMean= XYZ(:, 2);
delete('temp.csv')




XYZ =dlmread('IPVolume10val_FfAM_ZZMean.csv');
dlmwrite('temp.csv',XYZ,'delimiter',';');
XYZ =dlmread('temp.csv');
FfAM_ZZMean= XYZ(:, 2);
delete('temp.csv')

XYZ =dlmread('IPVolume10val_FpAM_ZZMean.csv');
dlmwrite('temp.csv',XYZ,'delimiter',';');
XYZ =dlmread('temp.csv');
FpAM_ZZMean= XYZ(:, 2);
delete('temp.csv')

XYZ =dlmread('IPVolume10val_FveAM_ZZMean.csv');
dlmwrite('temp.csv',XYZ,'delimiter',';');
XYZ =dlmread('temp.csv');
FveAM_ZZMean= XYZ(:, 2);
delete('temp.csv')


XYZ =dlmread('IPVolume10val_F_ZZMean.csv');
dlmwrite('temp.csv',XYZ,'delimiter',';');
XYZ =dlmread('temp.csv');
F_ZZMean= XYZ(:, 2);
delete('temp.csv')

XYZ =dlmread('IPVolume10val_ZGMean.csv');
dlmwrite('temp.csv',XYZ,'delimiter',';');
XYZ =dlmread('temp.csv');
ZGMean= XYZ(:, 2);
delete('temp.csv')

XYZ =dlmread('IPVolume10val_TTMean.csv');
dlmwrite('temp.csv',XYZ,'delimiter',';');
XYZ =dlmread('temp.csv');
TTMean= XYZ(:, 2);
delete('temp.csv')

XYZ =dlmread('IPVolume10val_WTMean.csv');
dlmwrite('temp.csv',XYZ,'delimiter',';');
XYZ =dlmread('temp.csv');
WTMean= XYZ(:, 2);
delete('temp.csv')

%---SMP----------------------------------------------------------------------
XYZ =dlmread('IPVolume10val_JACOBIANMean.csv');
dlmwrite('temp.csv',XYZ,'delimiter',';');
XYZ =dlmread('temp.csv');
DetF=     XYZ(:, 2);
delete('temp.csv')

for i=1:length(t)
AreaXX(i,1)= F_YYMean(i,1)*F_ZZMean(i,1);
AreaYY(i,1)= F_XXMean(i,1)*F_ZZMean(i,1);
AreaZZ(i,1)= F_XXMean(i,1)*F_YYMean(i,1);
STRAIN_ZZEng(i,1)= (F_ZZMean(i,1)-1);
end

%%---ENERGY-----------------------------------------------------------------------
XYZ =dlmread('IPVolume10val_appliedEnergyMean.csv');
dlmwrite('temp.csv',XYZ,'delimiter',';');
XYZ =dlmread('temp.csv');
appliedEnergy=  XYZ(:, 2)/1e6;
delete('temp.csv')

XYZ =dlmread('IPVolume10val_DEFO_ENERGYMean.csv');
dlmwrite('temp.csv',XYZ,'delimiter',';');
XYZ =dlmread('temp.csv');
elasticEnergy=  XYZ(:, 2)/1e6;
delete('temp.csv')

XYZ =dlmread('IPVolume10val__thermalEnergyMean.csv');
dlmwrite('temp.csv',XYZ,'delimiter',';');
XYZ =dlmread('temp.csv');
thermalEnergy=  XYZ(:, 2)/1e6;
delete('temp.csv')

XYZ =dlmread('IPVolume10val_freezedDissMean.csv');
dlmwrite('temp.csv',XYZ,'delimiter',';');
XYZ =dlmread('temp.csv');
freezedDiss=  XYZ(:, 2)/1e6;
delete('temp.csv')

XYZ =dlmread('IPVolume10val_plasticDissMean.csv');
dlmwrite('temp.csv',XYZ,'delimiter',';');
XYZ =dlmread('temp.csv');
plasticDiss=  XYZ(:, 2)/1e6;
delete('temp.csv')

XYZ =dlmread('IPVolume10val_viscousDissMean.csv');
dlmwrite('temp.csv',XYZ,'delimiter',';');
XYZ =dlmread('temp.csv');
viscousDiss=  XYZ(:, 2)/1e6;
delete('temp.csv')

XYZ =dlmread('IPVolume10val_EDissMean.csv');
dlmwrite('temp.csv',XYZ,'delimiter',';');
XYZ =dlmread('temp.csv')/1e6;
EDiss=  XYZ(:, 2);
delete('temp.csv')

totalDiss= freezedDiss + plasticDiss + viscousDiss +EDiss;


t_MOD=[0;t];
P_ZZMean_MOD=[0;P_ZZMean];
STRAIN_ZZEng_MOD=[0;STRAIN_ZZEng];
energy_StimesE=zeros(length(t)+1,1);
F1234_T_MOD=[0;F1234_T];
F1265_T_MOD=[0;F1265_T];
F4158_T_MOD=[0;F4158_T];
F5678_T_MOD=[0;F5678_T];
F2376_T_MOD=[0;F2376_T];
F3487_T_MOD=[0;F3487_T];
FLUX_1234=zeros(length(t)+1,1);
FLUX_1265=zeros(length(t)+1,1);
FLUX_4158=zeros(length(t)+1,1);
FLUX_5678=zeros(length(t)+1,1);
FLUX_2376=zeros(length(t)+1,1);
FLUX_3487=zeros(length(t)+1,1);
for i=1:length(t)
  energy_StimesE(i+1,1)= energy_StimesE(i,1) + (P_ZZMean_MOD(i+1,1) + P_ZZMean_MOD(i,1))/2*(STRAIN_ZZEng_MOD(i+1,1)-STRAIN_ZZEng_MOD(i,1)); 
  FLUX_1234(i+1,1)= FLUX_1234(i,1) + 1*(F1234_T_MOD(i+1,1) + F1234_T_MOD(i,1))/2*(t_MOD(i+1,1)-t_MOD(i,1))*1e3;
  FLUX_1265(i+1,1)= FLUX_1265(i,1) + 1*(F1265_T_MOD(i+1,1) + F1265_T_MOD(i,1))/2*(t_MOD(i+1,1)-t_MOD(i,1))*1e3;
  FLUX_4158(i+1,1)= FLUX_4158(i,1) + 1*(F4158_T_MOD(i+1,1) + F4158_T_MOD(i,1))/2*(t_MOD(i+1,1)-t_MOD(i,1))*1e3;
  FLUX_5678(i+1,1)= FLUX_5678(i,1) + 1*(F5678_T_MOD(i+1,1) + F5678_T_MOD(i,1))/2*(t_MOD(i+1,1)-t_MOD(i,1))*1e3;
  FLUX_2376(i+1,1)= FLUX_2376(i,1) + 1*(F2376_T_MOD(i+1,1) + F2376_T_MOD(i,1))/2*(t_MOD(i+1,1)-t_MOD(i,1))*1e3;
  FLUX_3487(i+1,1)= FLUX_3487(i,1) + 1*(F3487_T_MOD(i+1,1) + F3487_T_MOD(i,1))/2*(t_MOD(i+1,1)-t_MOD(i,1))*1e3;
end
TOTAL_FLUX=FLUX_1234+FLUX_1265;%+FLUX_4158+FLUX_5678+FLUX_2376+FLUX_3487;
%--------------
figure (10,"position",get(0,"screensize"))
##set(gcf, 'name', 'U:displacement, F:force, P:pressure ')
subplot (3, 6, 1)
plot(t, UX_Pt7, 'linewidth',2,'b-')
set(gca,'FontSize',14)
xlabel '\bft';
ylabel '\bfUX-Pt7 [mm]';
grid on

subplot (3, 6, 2)
plot(t, UY_Pt7, 'linewidth',2,'b-')
set(gca,'FontSize',14)
xlabel '\bft';
ylabel '\bfUY-Pt7 [mm]';
grid on

subplot (3, 6, 3)
plot(t, UZ_Pt7, 'linewidth',2,'b-')
set(gca,'FontSize',14)
xlabel '\bft';
ylabel '\bfUZ-Pt7 [mm]';
grid on

subplot (3, 6, 4)
plot(t, F1234_X, 'linewidth',2,'b-')
set(gca,'FontSize',14)
xlabel '\bft';
ylabel '\bf F1234-X [N]';
grid on

subplot (3, 6, 5)
plot(t, F1234_Y, 'linewidth',2,'b-')
set(gca,'FontSize',14)
xlabel '\bft';
ylabel '\bfF1234-Y [N]';
grid on

subplot (3, 6, 6)
plot(t, F1234_Z, 'linewidth',2,'b-')
set(gca,'FontSize',14)
xlabel '\bft';
ylabel '\bfF1234-Z [N]';
grid on

subplot (3, 6, 7)
plot(t, STRAIN_XXMean, 'linewidth',2,'b-')
set(gca,'FontSize',14)
xlabel '\bft';
ylabel '\bf\epsilonXX-Mean';
grid on

subplot (3, 6, 8)
plot(t, STRAIN_YYMean, 'linewidth',2,'b-')
set(gca,'FontSize',14)
xlabel '\bft';
ylabel '\bf\epsilonYY-Mean';
grid on

subplot (3, 6, 9)
plot(t, STRAIN_ZZMean, 'linewidth',2,'b-')
set(gca,'FontSize',14)
xlabel '\bft';
ylabel '\bf\epsilonZZ-Mean';
grid on

subplot (3, 6, 10)
plot(t, STRAIN_XYMean, 'linewidth',2,'b-')
set(gca,'FontSize',14)
xlabel '\bft';
ylabel '\bf\epsilonXY-Mean';
grid on

subplot (3, 6, 11)
plot(t, STRAIN_YZMean, 'linewidth',2,'b-')
set(gca,'FontSize',14)
xlabel '\bft';
ylabel '\bf\epsilonYZ-Mean';
grid on

subplot (3, 6, 12)
plot(t, STRAIN_XZMean, 'linewidth',2,'b-')
set(gca,'FontSize',14)
xlabel '\bft';
ylabel '\bf\epsilonXZ-Mean';
grid on

subplot (3, 6, 13)
plot(t, SIG_XXMean, 'linewidth',2,'b-')
set(gca,'FontSize',14)
xlabel '\bft';
ylabel '\bf\sigmaXX-Mean [MPa]';
grid on

subplot (3, 6, 14)
plot(t, SIG_YYMean, 'linewidth',2,'b-')
set(gca,'FontSize',14)
xlabel '\bft';
ylabel '\bf\sigmaYY-Mean [MPa]';
grid on

subplot (3, 6, 15)
plot(t, SIG_ZZMean, 'linewidth',2,'b-')
set(gca,'FontSize',14)
xlabel '\bft';
ylabel '\bf\sigmaZZ-Mean [MPa]';
grid on

subplot (3, 6, 16)
plot(t, SIG_XYMean, 'linewidth',2,'b-')
set(gca,'FontSize',14)
xlabel '\bft';
ylabel '\bf\sigmaXY-Mean [MPa]';
grid on

subplot (3, 6, 17)
plot(t, SIG_YZMean, 'linewidth',2,'b-')
set(gca,'FontSize',14)
xlabel '\bft';
ylabel '\bf\sigmaYZ-Mean [MPa]';
grid on

subplot (3, 6, 18)
plot(t, SIG_XZMean, 'linewidth',2,'b-')
set(gca,'FontSize',14)
xlabel '\bft';
ylabel '\bf\sigmaXZ-Mean [MPa]';
grid on
###saveas (10, "10.pdf");


%%-----------------------------------------------------------
figure (20,"position",get(0,"screensize"))
subplot (5, 5, 1)
plot(t, T_Mean, 'linewidth',2,'b-',  t, T_7, 'linewidth',3,'r--')
set(gca,'FontSize',14)
%title ("t & T-Mean");
xlabel '\bft';
ylabel '\bfT-Mean, \bfT-7 [C]';
grid on


subplot (5, 5, 3)
plot(t, UZ_Pt7, 'linewidth',2,'b-')
set(gca,'FontSize',14)
xlabel '\bft';
ylabel '\bfUZ-Pt7 [mm]';
grid on

subplot (5, 5, 4)
plot(t, -F1234_Z, 'linewidth',2,'b-')
set(gca,'FontSize',14)
xlabel '\bft';
ylabel '\bf-FZ-Face1234 [N]';
grid on

subplot (5, 5, 6)
plot(t, PLASTICSTRAINMean, 'linewidth',2,'b-', t, PLASTICSTRAINAMMean, 'linewidth',3,'r--', t, PLASTICSTRAINRMean, 'linewidth',4,'c..')
%h = legend ({'\bf\epsilon_PG', '\bf\epsilon_PAM'});%,"location", "southoutside", "orientation", "horizontal");
set(gca,'FontSize',14)
xlabel '\bft';
ylabel '\bfplastic \epsilon-Mean';
grid on

subplot (5, 5, 7)
plot(t, SVM, 'linewidth',2,'b-')
set(gca,'FontSize',14)
xlabel '\bft';
ylabel '\bfSVM [MPa]';
grid on

subplot (5, 5, 8)
plot(t, STRAIN_ZZMean, 'linewidth',2,'b-', t, STRAIN_ZZEng, 'linewidth',3,'m--')
set(gca,'FontSize',14)
xlabel '\bft';
ylabel '\bf\epsilonZZ-Mean';
grid on

subplot (5, 5, 9)
plot(t, SIG_ZZMean, 'linewidth',2,'b-',t, P_ZZMean, 'linewidth',3,'m--')
set(gca,'FontSize',14)
xlabel '\bft';
ylabel '\bf\sigmaZZ-Mean [MPa]';
grid on

subplot (5, 5, 11)
plot(STRAIN_ZZMean, SIG_ZZMean, 'linewidth',2,'b-',STRAIN_ZZEng, P_ZZMean, 'linewidth',3,'m--')
set(gca,'FontSize',14)
xlabel '\bf\epsilonZZ-Mean';
ylabel '\bf\sigmaZZ-Mean [MPa]';
%xlim ([0. 2.])
%ylim ([0. 2.])
grid on

subplot (5, 5, 12)
plot(T_Mean, SIG_ZZMean, 'linewidth',2,'b-',T_Mean, P_ZZMean, 'linewidth',3,'m--')
set(gca,'FontSize',14)
xlabel '\bfT-Mean [K]';
ylabel '\bf\sigmaZZ-Mean [MPa]';
grid on

subplot (5, 5, 13)
plot(T_Mean, STRAIN_ZZMean, 'linewidth',2,'b-', T_7, UZ_Pt7, 'linewidth',3,'r--')
set(gca,'FontSize',14)
xlabel '\bfT-Mean [K]';
ylabel '\bf\epsilonZZ-Mean';
grid on

subplot (5, 5, 14)
plot(t, ZGMean, 'linewidth',2,'b-')
set(gca,'FontSize',14)
xlabel '\bft';
ylabel '\bf ZGMean';
grid on

subplot (5, 5, 16)
plot(t, TTMean, 'linewidth',2,'b-', t, TcrysMean, 'linewidth',2,'c--',t, TmeltMean, 'linewidth',2,'r.-')
set(gca,'FontSize',14)
xlabel '\bft';
ylabel '\bf Tt/Tc/Tm [K]';
grid on

subplot (5, 5, 17)
plot(t, WTMean, 'linewidth',2,'b-', t, WcrysMean, 'linewidth',2,'c--',t, WmeltMean, 'linewidth',2,'r.-')
set(gca,'FontSize',14)
xlabel '\bft';
ylabel '\bf Wt/Wc/Wm';
grid on

subplot (5, 5, 18)
plot(t, DetF, 'linewidth',2,'b-')
set(gca,'FontSize',14)
xlabel '\bft';
ylabel '\bf DetF';
grid on

subplot (5, 5, 19)
plot(t, AreaXX, 'linewidth',1,'k-', t, AreaYY, 'linewidth',1,'k--', t, AreaZZ, 'linewidth',3,'b-')
set(gca,'FontSize',14)
xlabel '\bft';
ylabel '\bf Area[mm^2]';
grid on

subplot (5, 5, 20)
plot(t, appliedEnergy, 'linewidth',2,'k-', t, elasticEnergy, 'linewidth',1,'k--',
     t, freezedDiss, 'linewidth',1,'g-', t, plasticDiss, 'linewidth',1,'b-', 
     t, viscousDiss, 'linewidth',1,'m-', t, EDiss, 'linewidth',1,'c-')
h4520 = legend ({'applied','elastic','freezed', 'plastic', 'viscous', 'EDiss'});%,"location", "southoutside", "orientation", "horizontal");
%h4520 = legend ("show");
set(h4520,'position',[0.92 0.25 0.1 0.1])%,"NumColumns",2)
set(gca,'FontSize',14)
xlabel '\bft';
ylabel '\bf energy [MPa]';
grid on

subplot (5, 5, 21)
plot(t, T_Pt1, 'linewidth',1,'k-',    t, T_Pt2, 'linewidth',1,'k--',   t, T_Pt3, 'linewidth',1,'k:',...
     t, T_Pt4, 'linewidth',1,'k-.',    t, T_Pt5, 'linewidth',2,'b-',   t, T_Pt6, 'linewidth',2,'b--',...
     t, T_Pt7, 'linewidth',2,'b:',    t, T_Pt8, 'linewidth',2,'b-.')
##h = legend ({"", "T_Pt6", "T_Pt7", "T_Pt8"});%,"location", "southoutside", "orientation", "horizontal");
##set(h,'FontSize',10)
##set(gca,'FontSize',14)
xlabel '\bft';
ylabel '\bf nodal T [K]';
grid on

subplot (5, 5, 22)
plot(t, q_x, 'linewidth',1,'k-',t, q_y, 'linewidth',2,'b--',t, q_z, 'linewidth',3,'g-.')
##h = legend ({"q_x", "q_y", "q_z"});%,"location", "southoutside", "orientation", "horizontal");
##set(h,'FontSize',10)
##set(gca,'FontSize',14)
xlabel '\bft';
ylabel '\bf mean flux q_x/q_y/q_z [W]';
grid on

subplot (5, 5, 23)
plot(t, F4158_T, 'linewidth',2,'k-',t, F1265_T, 'linewidth',2,'b--',t, F1234_T, 'linewidth',2,'g-.')
set(gca,'FontSize',14)
xlabel '\bft';
ylabel '\bf face flux [W]';
grid on

subplot (5, 5, 25)
plot(t, totalDiss, 'linewidth',3,'b-', t_MOD, TOTAL_FLUX,'linewidth',2,'r-')
h4520 = legend ({'plastic+viscous', 'heat flux'});%,"location", "southoutside", "orientation", "horizontal");
%h4520 = legend ("show");
set(h4520,'position',[0.92 0.1 0.1 0.1])%,"NumColumns",2)
set(gca,'FontSize',14)
xlabel '\bft';
ylabel '\bf energy[N.mm]';
grid on
saveas (20, "figure20.jpg");



%%-------------------------------------------------------------------------------
##row30=6;
##col30=9;
##figure (30,"position",get(0,"screensize"))
##subplot (row30, col30, 1)
##plot(t, FthG_YYMean, 'linewidth',2,'b-')
##set(gca,'FontSize',10)
##xlabel '\bft';
##ylabel '\bf FthG-YY';
##grid on
##subplot (row30, col30, 2)
##plot(t, FthI_YYMean, 'linewidth',2,'b-')
##set(gca,'FontSize',10)
##xlabel '\bft';
##ylabel '\bf FthI-YY';
##grid on
##
##subplot (row30, col30, 3)
##plot(t, FthAD_YYMean, 'linewidth',2,'b-')
##set(gca,'FontSize',10)
##xlabel '\bft';
##ylabel '\bf FthAD-YY';
##grid on
##
##subplot (row30, col30, 4)
##plot(t, FfG_YYMean, 'linewidth',2,'b-')
##set(gca,'FontSize',10)
##xlabel '\bft';
##ylabel '\bf FfG-YY-Mean';
##grid on
##
##subplot (row30, col30, 5)
##plot(t, FpG_YYMean, 'linewidth',2,'b-')
##set(gca,'FontSize',10)
##xlabel '\bft';
##ylabel '\bf FpG-YY-Mean';
##grid on
##
##subplot (row30, col30, 6)
##plot(t, FveG_YYMean, 'linewidth',2,'b-')
##set(gca,'FontSize',10)
##xlabel '\bft';
##ylabel '\bf FveG-YY-Mean';
##grid on
##
##subplot (row30, col30, 7)
##plot(t, TcrysMean, 'linewidth',2,'b-')
##set(gca,'FontSize',10)
##xlabel '\bft';
##ylabel '\bf Trycs';
##grid on
##
##subplot (row30, col30, 8)
##plot(t, EnThG_YYMean, 'linewidth',2,'b-')
##set(gca,'FontSize',10)
##xlabel '\bft';
##ylabel '\bf EnThG-YY';
##grid on
##
##subplot (row30, col30, 9)
##plot(t, EnThG_ZZMean, 'linewidth',2,'b-')
##set(gca,'FontSize',10)
##xlabel '\bft';
##ylabel '\bf EnThG-ZZ';
##grid on
##
##
##
##
##subplot (row30, col30, 10)
##plot(t, FthG_ZZMean, 'linewidth',2,'b-')
##set(gca,'FontSize',10)
##xlabel '\bft';
##ylabel '\bf FthG-ZZ';
##grid on
##
##subplot (row30, col30, 11)
##plot(t, FthI_ZZMean, 'linewidth',2,'b-')
##set(gca,'FontSize',10)
##xlabel '\bft';
##ylabel '\bf FthI-ZZ';
##grid on
##
##subplot (row30, col30, 12)
##plot(t, FthAD_ZZMean, 'linewidth',2,'b-')
##set(gca,'FontSize',10)
##xlabel '\bft';
##ylabel '\bf FthAD-ZZ';
##grid on
##
##subplot (row30, col30, 13)
##plot(t, FfG_ZZMean, 'linewidth',2,'b-')
##set(gca,'FontSize',10)
##xlabel '\bft';
##ylabel '\bf FfG-ZZ-Mean';
##grid on
##
##subplot (row30, col30, 14)
##plot(t, FpG_ZZMean, 'linewidth',2,'b-')
##set(gca,'FontSize',10)
##xlabel '\bft';
##ylabel '\bf FpG-ZZ-Mean';
##grid on
##
##subplot (row30, col30, 15)
##plot(t, FveG_ZZMean, 'linewidth',2,'b-')
##set(gca,'FontSize',10)
##xlabel '\bft';
##ylabel '\bf FveG-ZZ-Mean';
##grid on
##
##subplot (row30, col30, 16)
##plot(t, TmeltMean, 'linewidth',2,'b-')
##set(gca,'FontSize',10)
##xlabel '\bft';
##ylabel '\bf Tmelt';
##grid on
##
##subplot (row30, col30, 17)
##plot(t, EnThR_YYMean, 'linewidth',2,'b-')
##set(gca,'FontSize',10)
##xlabel '\bft';
##ylabel '\bf EnThR-YY';
##grid on
##
##subplot (row30, col30, 18)
##plot(t, EnThR_ZZMean, 'linewidth',2,'b-')
##set(gca,'FontSize',10)
##xlabel '\bft';
##ylabel '\bf EnThR-ZZ';
##grid on
##
##
##
##subplot (row30, col30, 19)
##plot(t, FthR_YYMean, 'linewidth',2,'b-')
##set(gca,'FontSize',10)
##xlabel '\bft';
##ylabel '\bf FthR-YY';
##grid on
##
##subplot (row30, col30, 20)
##plot(t, 1, 'linewidth',2,'b-')
##set(gca,'FontSize',10)
##xlabel '\bft';
##ylabel '\bf -YY';
##grid on
##
##subplot (row30, col30, 21)
##plot(t, 1, 'linewidth',2,'b-')
##set(gca,'FontSize',10)
##xlabel '\bft';
##ylabel '\bf -YY';
##grid on
##
##subplot (row30, col30, 22)
##plot(t, FfR_YYMean, 'linewidth',2,'b-')
##set(gca,'FontSize',10)
##xlabel '\bft';
##ylabel '\bf FfR-YY-Mean';
##grid on
##
##subplot (row30, col30, 23)
##plot(t, FpR_YYMean, 'linewidth',2,'b-')
##set(gca,'FontSize',10)
##xlabel '\bft';
##ylabel '\bf FpR-YY-Mean';
##grid on
##
##subplot (row30, col30, 24)
##plot(t, FveR_YYMean, 'linewidth',2,'b-')
##set(gca,'FontSize',10)
##xlabel '\bft';
##ylabel '\bf FveR-YY-Mean';
##grid on
##
##subplot (row30, col30, 25)
##plot(t, WcrysMean, 'linewidth',2,'b-')
##set(gca,'FontSize',10)
##xlabel '\bft';
##ylabel '\bf Wrycs';
##grid on
##
##subplot (row30, col30, 26)
##plot(t, EnThAM_YYMean, 'linewidth',2,'b-')
##set(gca,'FontSize',10)
##xlabel '\bft';
##ylabel '\bf EnThAM-YY';
##grid on
##
##subplot (row30, col30, 27)
##plot(t, EnThAM_ZZMean, 'linewidth',2,'b-')
##set(gca,'FontSize',10)
##xlabel '\bft';
##ylabel '\bf EnThAM-ZZ';
##grid on
##
##
##
##
##subplot (row30, col30, 28)
##plot(t, FthR_ZZMean, 'linewidth',2,'b-')
##set(gca,'FontSize',10)
##xlabel '\bft';
##ylabel '\bf FthR-ZZ';
##grid on
##
##subplot (row30, col30, 29)
##plot(t, 1, 'linewidth',2,'b-')
##set(gca,'FontSize',10)
##xlabel '\bft';
##ylabel '\bf -ZZ';
##grid on
##
##subplot (row30, col30, 30)
##plot(t, 1, 'linewidth',2,'b-')
##set(gca,'FontSize',10)
##xlabel '\bft';
##ylabel '\bf -ZZ';
##grid on
##
##subplot (row30, col30, 31)
##plot(t, FfR_ZZMean, 'linewidth',2,'b-')
##set(gca,'FontSize',10)
##xlabel '\bft';
##ylabel '\bf FfR-ZZ-Mean';
##grid on
##
##subplot (row30, col30, 32)
##plot(t, FpR_ZZMean, 'linewidth',2,'b-')
##set(gca,'FontSize',10)
##xlabel '\bft';
##ylabel '\bf FpR-ZZ-Mean';
##grid on
##
##subplot (row30, col30, 33)
##plot(t, FveR_ZZMean, 'linewidth',2,'b-')
##set(gca,'FontSize',10)
##xlabel '\bft';
##ylabel '\bf FveR-ZZ-Mean';
##grid on
##
##
##subplot (row30, col30, 34)
##plot(t, WmeltMean, 'linewidth',2,'b-')
##set(gca,'FontSize',10)
##xlabel '\bft';
##ylabel '\bf Wmelt';
##grid on
##
##subplot (row30, col30, 35)
##plot(t, ADl_YYMean, 'linewidth',2,'b-')
##set(gca,'FontSize',10)
##xlabel '\bft';
##ylabel '\bf ADl-YY';
##grid on
##
##subplot (row30, col30, 36)
##plot(t, ADl_ZZMean, 'linewidth',2,'b-')
##set(gca,'FontSize',10)
##xlabel '\bft';
##ylabel '\bf ADl-ZZ';
##grid on
##
##
##
##
##subplot (row30, col30, 37)
##plot(t, FthAM_YYMean, 'linewidth',2,'b-')
##set(gca,'FontSize',10)
##xlabel '\bft';
##ylabel '\bf FthAM-YY';
##grid on
##
##subplot (row30, col30, 39)
##plot(t, 1, 'linewidth',2,'b-')
##set(gca,'FontSize',10)
##xlabel '\bft';
##ylabel '\bf -YY';
##grid on
##
##subplot (row30, col30, 40)
##plot(t, FfAM_YYMean, 'linewidth',2,'b-')
##set(gca,'FontSize',10)
##xlabel '\bft';
##ylabel '\bf FfAM-YY-Mean';
##grid on
##
##subplot (row30, col30, 41)
##plot(t, FpAM_YYMean, 'linewidth',2,'b-')
##set(gca,'FontSize',10)
##xlabel '\bft';
##ylabel '\bf FpAM-YY-Mean';
##grid on
##
##subplot (row30, col30, 42)
##plot(t, FveAM_YYMean, 'linewidth',2,'b-')
##set(gca,'FontSize',10)
##xlabel '\bft';
##ylabel '\bf FveAM-YY-Mean';
##grid on
##
##subplot (row30, col30, 43)
##plot(t, F_YYMean, 'linewidth',2,'b-')
##set(gca,'FontSize',10)
##xlabel '\bft';
##ylabel '\bf F-YY-Mean';
##grid on
##
##subplot (row30, col30, 44)
##plot(t, DetF, 'linewidth',2,'b-')
##set(gca,'FontSize',10)
##xlabel '\bft';
##ylabel '\bf DetF';
##grid on
##
##subplot (row30, col30, 45)
##plot(t, ZGMean, 'linewidth',2,'b-')
##set(gca,'FontSize',10)
##xlabel '\bft';
##ylabel '\bf ZGMean';
##grid on
##
##
##
##
##subplot (row30, col30, 46)
##plot(t, FthAM_ZZMean, 'linewidth',2,'b-')
##set(gca,'FontSize',10)
##xlabel '\bft';
##ylabel '\bf FthAM-ZZ';
##grid on
##
##subplot (row30, col30, 48)
##plot(t, 1, 'linewidth',2,'b-')
##set(gca,'FontSize',10)
##xlabel '\bft';
##ylabel '\bf -ZZ';
##grid on
##
##subplot (row30, col30, 49)
##plot(t, FfAM_ZZMean, 'linewidth',2,'b-')
##set(gca,'FontSize',10)
##xlabel '\bft';
##ylabel '\bf FfAM-ZZ-Mean';
##grid on
##
##subplot (row30, col30, 50)
##plot(t, FpAM_ZZMean, 'linewidth',2,'b-')
##set(gca,'FontSize',10)
##xlabel '\bft';
##ylabel '\bf FpAM-ZZ-Mean';
##grid on
##
##subplot (row30, col30, 51)
##plot(t, FveAM_ZZMean, 'linewidth',2,'b-')
##set(gca,'FontSize',10)
##xlabel '\bft';
##ylabel '\bf FveAM-ZZ-Mean';
##grid on
##
##subplot (row30, col30, 52)
##plot(t, F_ZZMean, 'linewidth',2,'b-')
##set(gca,'FontSize',10)
##xlabel '\bft';
##ylabel '\bf F-ZZ-Mean';
##grid on
##
##subplot (row30, col30, 53)
##plot(t, AreaXX, 'linewidth',1,'k-', t, AreaYY, 'linewidth',1,'k--', t, AreaZZ, 'linewidth',3,'b-')
##set(gca,'FontSize',10)
##xlabel '\bft';
##ylabel '\bf area';
##grid on
##
##subplot (row30, col30, 54)
##plot(t, TTMean, 'linewidth',2,'b-')
##set(gca,'FontSize',10)
##xlabel '\bft';
##ylabel '\bf TTMean';
##grid on
%saveas (30, "SMP-F.pdf");






