#coding-Utf-8-*-
from gmshpy import *

#from dG3DpyDebug import*
from dG3Dpy import *


# ===Problem parameters============================================================================
lawnum= 1

#crystalline phase#					
alpha=beta=gamma= 0.				
rho	= 1200.
Eg	= 1250.e6 
Er	= 9.7e6 #3.2e6  
nug	= 0.26 #0.4
nur	= 0.26 #0.4
Gg	= Eg/2./(1+nug)
Gr	= Er/2./(1+nur)
Kg    = Eg/3./(1-2*nug)
Kr    = Er/3./(1-2*nur)
#amorphous phase#					
rhoAM	= rho
EAM	= 1.e5		
nuAM	= nur
GAM	= EAM/2./(1+nuAM)
KAM   = EAM/3./(1-2*nuAM)

Tc0	= 273.+8.	
Tm0	= 273.+45. 
wc0	= 8.	
wm0	= 5. 				

xi	= 0.		# rate parameter for Tt 				
alphag0	= 0 #0.8e-3*EAM/Eg #12.0e-5	
alphar0	= 8.e-4 #15.0e-5 	
alphaAD0= -0.015 #-0.0#-0.024
#crsytal contraction for low zg only
crystallizationRate = piecewiseScalarFunction() #surface should be 1
crystallizationRate.put(0.,20.)
crystallizationRate.put(0.1,0)
crystallizationRate.put(1.,0.)
  
cg	= 2020.*rho #1974210.		
cr	= 2020.*rho #1744200.
Kxg=Kyg=Kzg=Kxr=Kyr=Kzr	= 0.22					  
TaylorQuineyG=TaylorQuineyR=TaylorQuineyAM= 1.		# fraction of plastic energy converted to thermal energy	

cfG	= 1.0		
cfR	= 0.0		

alphaAM0= 1.e-5#alphar0
cAM	= 2.02*rho		
KxAM=KyAM=KzAM = Kxr

cfAM	= 0.0
zAM 	= 0.685  #0.685

tau_y0G = 1000.e6
tau_y0R = 6.e9 #0.6e6
tau_y0AM= 6.e9	
#// R = yield0 + h1 p^h2 if p>H3              OLD ONE
#// R = yield0 + (h1 pth^H2)/h3 p if p<=pth	OLD ONE
#  R = yield0 +h1 * p + h2* (1- exp(-hexp*p))  NEW ONE
HR1	= 1.e4 
HR2	= 0. 
HR3	= 0. 
HAM1	= 1.e4 
HAM2	= 0. 
HAM3	= 0. 
HG1	= 1.e6 #1e8 
HG2	= 0. #0.3 
HG3	= 300.
###

##extra branch
VKAM=2. #30.
VKR= 8.
VKG= 0. #8. 
thetaKAM=1.
thetaKR=1.
thetaKG=1. #6.
zetaKAM=1.2 #1
zetaKR=1.65
zetaKG=1.65 #250. #Has to be > 2.
VMUAM=2. #30.
VMUR= 8.
VMUG= 0. #10.
thetaMUAM=1.
thetaMUG=1.
thetaMUR=1. #0.225 #0.225
zetaMUAM=1.2 #1
zetaMUR=1.65
zetaMUG=1.65 #2.
  
########
#viscous#					
MU1G=4.e7;	KAPPA1G=1.;	TAU1G=15.; 	MU2G=5.e7; 	KAPPA2G=1.;	TAU2G=200.;
MU1R= 2.e5;	KAPPA1R=1.;	TAU1R=15.;	MU2R=2.e5;	KAPPA2R=1.;	TAU2R=300.;  MU3R=2.e4;	KAPPA3R=1.;	TAU3R=1000.;
MU1AM=MU1R*EAM/Er;	KAPPA1AM=KAPPA1R*EAM/Er;	TAU1AM=TAU1R;	MU2AM=MU2R*EAM/Er;	KAPPA2AM=KAPPA2R*EAM/Er;	TAU2AM=TAU2R; MU3AM=MU3R*EAM/Er;	KAPPA3AM=KAPPA3R*EAM/Er;	TAU3AM=TAU3R;
#####

Atc= 23.; #5.; #23.; 
Btm=-2.; 
Cwc=-4.; 
Dwm=-4.; 
alphatc=1.8;
alphatm=1.8;
alphawc=3.8;
alphawm=3.8;
  
#plastic flow during crystallisation
yieldCrystallinityG = piecewiseScalarFunction()
yieldCrystallinityG.put(0.,100000./tau_y0G)
yieldCrystallinityG.put(0.25,100000./tau_y0G)
#yieldCrystallinityG.put(0.,1.)
yieldCrystallinityG.put(1.,1.)
#plastic flow during crystallisation

### kinematic hardening
hardenkG = PolynomialKinematicHardening(4,5)
hardenkG.setCoefficients(1,0.e4)
hardenkG.setCoefficients(2,0.e6)
hardenkG.setCoefficients(3,0.e10)
hardenkG.setCoefficients(4,0.e10)
hardenkG.setCoefficients(5,7.5e10)
  
hardenkR = PolynomialKinematicHardening(5,5)
hardenkR.setCoefficients(1,170.e8)
hardenkR.setCoefficients(2,170.e8)
hardenkR.setCoefficients(3,-440.e8)
hardenkR.setCoefficients(4,1100.e8)

hardenkAM = PolynomialKinematicHardening(6,5)
hardenkAM.setCoefficients(1,170.e8)
hardenkAM.setCoefficients(2,170.e8)
hardenkAM.setCoefficients(3,-440.e8)
hardenkAM.setCoefficients(4,1100.e8)
### kinematic hardening

###param with crystalinity dilatation from Fg
#anisotropicThermalCoefAmplitudeBulk
GBulkDilatationAmplitude=0. #0.; # -2.; 
RBulkDilatationAmplitude=0.; #0.7 #1.5 #-3.; #-0.008*4.; 
CrystalizationAmplitudePositiveEigen=0.; #not use #0.55/2.; 
AMBulkDilatationAmplitude=0.; 
#
#anisotropicThermalCoefAmplitudeShearG
GShearDilatationAmplitude=0.; 
RShearDilatationAmplitude=0.3#0.2; #-0.003*4.; 
CrystalizationAmplitudeNegativeEigen=0.;#not use # 0.28/2.;
AMShearDilatationAmplitude=0.#0.001; 
#
#anisotropicThermalAlphaCoefTanhTempBulk
alphaBulkGDilatationTempCoef=0.02; 
alphaBulkRDilatationTempCoef=0.02; #1.1 
alphaCrystalizationPositiveEigen=0.; #not used 
alphaBulkAMDilatationTempCoef=0.02; 
#
#anisotropicThermalAlphaCoefTanhTempShearG
alphaShearGDilatationTempCoef=0.02; 
alphaShearRDilatationTempCoef=0.02; #1.1; 
alphaCrystalizationNegativeEigen=0.; #not used
alphaShearAMDilatationTempCoef=0.02; 

fieldSource =True  	#True #account for cp dT dt 	
mecaSource  =True 	#False #account for Taylor Quincey	


t0 = 273.-10.
t1 = 273.-10.

_j2IHGt = LinearExponentialJ2IsotropicHardening(lawnum,  tau_y0G, HG1, HG2, HG3)
_j2IHGc = LinearExponentialJ2IsotropicHardening(lawnum+1,  1.2*tau_y0G, 1.2*HG1, 1.2*HG2, HG3)
_j2IHRt = LinearExponentialJ2IsotropicHardening(lawnum+2,  tau_y0R, HR1, HR2, HR3)
_j2IHRc = LinearExponentialJ2IsotropicHardening(lawnum+3,  1.2*tau_y0R, 1.2*HR1, 1.2*HR2, HR3)
_j2IHAMt = LinearExponentialJ2IsotropicHardening(lawnum+4,  tau_y0AM, HAM1, HAM2, HAM3)
_j2IHAMc = LinearExponentialJ2IsotropicHardening(lawnum+5,  1.2*tau_y0AM, 1.2*HAM1, 1.2*HAM2, HAM3)
  
   
  
	
law1= PhenomenologicalSMPDG3DMaterialLaw(lawnum+7, rho, alpha, beta, gamma, t0, 
                                           Kxg, Kyg, Kzg, Kxr, Kyr, Kzr, KxAM, KyAM, KzAM,
                                           cfG, cfR, cfAM, cg, cr, cAM,
                                           Tm0, Tc0, xi, wm0, wc0,
                                           alphag0, alphar0, alphaAM0, alphaAD0,
                                           Eg, Er, EAM, nug, nur, nuAM,
                                           TaylorQuineyG,TaylorQuineyR,TaylorQuineyAM, zAM)

law1.setFunctionCrystallizationVolumeRate(crystallizationRate)
law1.setStrainOrder(-1)												
law1.setJ2IsotropicHardeningTractionG(_j2IHGt)
law1.setJ2IsotropicHardeningCompressionG(_j2IHGc)
law1.setKinematicHardeningG(hardenkG)
law1.setNonAssociatedFlowG(True);
law1.setPlasticPoissonRatioG(nug);
law1.setYieldPowerFactorG(3.5);
law1.setFunctionYieldCrystallinityG(yieldCrystallinityG);
  
law1.setJ2IsotropicHardeningTractionR(_j2IHRt)
law1.setJ2IsotropicHardeningCompressionR(_j2IHRc)
#law1.setKinematicHardeningR(hardenkR) 
law1.setNonAssociatedFlowR(True);
law1.setPlasticPoissonRatioR(nur);
law1.setYieldPowerFactorR(3.5);

law1.setJ2IsotropicHardeningTractionAM(_j2IHAMt)
law1.setJ2IsotropicHardeningCompressionAM(_j2IHAMc)
#law1.setKinematicHardeningAM(hardenkAM) 
law1.setNonAssociatedFlowAM(True);
law1.setPlasticPoissonRatioAM(nuAM);
law1.setYieldPowerFactorAM(3.5);

#law1.setElasticPotentialFunctionAM(EPFunc_AM)
#law1.setElasticPotentialFunctionR(EPFunc_R)
law1.setVolumeCorrectionAM(VKAM, thetaKAM, zetaKAM, VMUAM, thetaMUAM, zetaMUAM)
law1.setVolumeCorrectionR(VKR, thetaKR, zetaKR, VMUR, thetaMUR, zetaMUR)
law1.setVolumeCorrectionG(VKG, thetaKG, zetaKG, VMUG, thetaMUG, zetaMUG)

law1.setViscoelasticMethod(0)
law1.setViscoElasticNumberOfElementG(2)
law1.setViscoElasticData_BulkG(1, KAPPA1G, TAU1G) 
law1.setViscoElasticData_ShearG(1, MU1G, TAU1G) 
law1.setViscoElasticData_BulkG(2, KAPPA2G, TAU2G) 
law1.setViscoElasticData_ShearG(2, MU2G, TAU2G) 

law1.setViscoElasticNumberOfElementR(2)
law1.setViscoElasticData_BulkR(1, KAPPA1R, TAU1R) 
law1.setViscoElasticData_ShearR(1, MU1R, TAU1R) 
law1.setViscoElasticData_BulkR(2, KAPPA2R, TAU2R) 
law1.setViscoElasticData_ShearR(2, MU2R, TAU2R) 
#law1.setViscoElasticData_BulkR(3, KAPPA3R, TAU3R) 
#law1.setViscoElasticData_ShearR(3, MU3R, TAU3R) 

law1.setViscoElasticNumberOfElementAM(2)
law1.setViscoElasticData_BulkAM(1, KAPPA1AM, TAU1AM) 
law1.setViscoElasticData_ShearAM(1, MU1AM, TAU1AM) 
law1.setViscoElasticData_BulkAM(2, KAPPA2AM, TAU2AM) 
law1.setViscoElasticData_ShearAM(2, MU2AM, TAU2AM) 
#law1.setViscoElasticData_BulkAM(3, KAPPA3AM, TAU3AM) 
#law1.setViscoElasticData_ShearAM(3, MU3AM, TAU3AM) 

law1.setTcTmWcWm(Atc, Btm, Cwc, Dwm, alphatc,alphatm, alphawc, alphawm) 
law1.setAlphaParam(GBulkDilatationAmplitude, RBulkDilatationAmplitude, CrystalizationAmplitudePositiveEigen, AMBulkDilatationAmplitude, GShearDilatationAmplitude, RShearDilatationAmplitude, CrystalizationAmplitudeNegativeEigen, AMShearDilatationAmplitude,
       alphaBulkGDilatationTempCoef,alphaBulkRDilatationTempCoef,alphaCrystalizationPositiveEigen,alphaBulkAMDilatationTempCoef,
       alphaShearGDilatationTempCoef,alphaShearRDilatationTempCoef,alphaCrystalizationNegativeEigen,alphaShearAMDilatationTempCoef) 

pertlaw1 = dG3DMaterialLawWithTangentByPerturbation(law1,1e-5)
#===Solver parameters===================================================================================
sol 	= 2  		# Gmm=0 (default) Taucs=1 PETsc=2
soltype = 1 		# StaticLinear=0 (default) StaticNonLinear=1
nstep 	= 200		# number of step (used only if soltype=1)#640
ftime  	= 400.	   	# Final time (used only if soltype=1)22  t = 680
tol	= 1.e-6   	# relative tolerance for NR scheme (used only if soltype=1)
nstepArch= 1 		# Number of step between 2 archiving (used only if soltype=1)
fullDg 	=  False 	# O = CG, 1 = DG
space1 	= 0 		# function space (Lagrange=0)
beta1  	= 40.


#===Domain creation===================================================================================
# creation of ElasticField
nfield = 10 		
myfield1 = dG3DDomain(1000,nfield,space1,lawnum+7,fullDg,3,0,1)
myfield1.setConstitutiveExtraDofDiffusionEqRatio(1.e6)
#myfield1.matrixByPerturbation(0,0,0,1e-8)
#myfield1.matrixByPerturbation(1,1,1,1e-8)
myfield1.stabilityParameters(beta1)
myfield1.setConstitutiveExtraDofDiffusionStabilityParameters(beta1,fullDg)
myfield1.setConstitutiveExtraDofDiffusionAccountSource(fieldSource, mecaSource)


#===Solver creation===================================================================================
# geometry
geofile="cube.geo"	# name of mesh file
meshfile="cube.msh" 	# name of mesh file

# creation of Solver
mysolver = nonLinearMechSolver(1000)
mysolver.createModel(geofile,meshfile,3,2)
mysolver.addDomain(myfield1)
mysolver.addMaterialLaw(law1)
mysolver.Scheme(soltype)
mysolver.Solver(sol)
mysolver.snlData(nstep,ftime/1.,tol,tol)
mysolver.snlManageTimeStep(50, 3, 2, 10)
mysolver.stepBetweenArchiving(nstepArch)


#===Boundary conditions===================================================================================
#mechanical BC
#cyclicFunctionDisp=cycleFunctionTime(0., 0.,	ftime/2., 0.0003,	ftime/1.,0.0003);
#mysolver.displacementBC("Face",3487,1,cyclicFunctionDisp)

#mysolver.displacementBC("Face",1234,0,0.)
#mysolver.displacementBC("Face",1234,1,0.)
mysolver.displacementBC("Face",1234,2,0.)
mysolver.displacementBC("Face",1265,1,0.)
mysolver.displacementBC("Face",4158,0,0.)
#mysolver.displacementBC("Face",5678,1,0.)
#mysolver.displacementBC("Face",5678,0,0.)
#mysolver.displacementBC("Face",5678,2,0.)

#thermal BC
mysolver.initialBC("Volume","Position",nfield,3,t0)
mysolver.scalarFluxBC("Face",3487,3,0.015)
#cyclicFunctionTemp1=cycleFunctionTime(0., t0,	ftime/4., t0-100.,	ftime/2., t0,	3.*ftime/4., t0/2.,	ftime, t0);
#mysolver.displacementBC("Face",3487,3,cyclicFunctionTemp1);
#mysolver.displacementBC("Volume",nfield,3,cyclicFunctionTemp1);


#===Variable storage===================================================================================
mysolver.internalPointBuildView("svm",IPField.SVM, 1, 1)
mysolver.internalPointBuildView("sig_xx",IPField.SIG_XX, 1, 1)
mysolver.internalPointBuildView("sig_yy",IPField.SIG_YY, 1, 1)
mysolver.internalPointBuildView("sig_zz",IPField.SIG_ZZ, 1, 1)
mysolver.internalPointBuildView("sig_xy",IPField.SIG_XY, 1, 1)
mysolver.internalPointBuildView("sig_yz",IPField.SIG_YZ, 1, 1)
mysolver.internalPointBuildView("sig_xz",IPField.SIG_XZ, 1, 1)
mysolver.internalPointBuildView("temperature",IPField.TEMPERATURE, 1, 1)
mysolver.internalPointBuildView("qx",IPField.THERMALFLUX_X, 1, 1)
mysolver.internalPointBuildView("qy",IPField.THERMALFLUX_Y, 1, 1)
mysolver.internalPointBuildView("qz",IPField.THERMALFLUX_Z, 1, 1)

mysolver.archivingNodeDisplacement(1,0,nstepArch);
mysolver.archivingNodeDisplacement(1,1,nstepArch);
mysolver.archivingNodeDisplacement(1,2,nstepArch);
mysolver.archivingNodeDisplacement(1,3,nstepArch);
mysolver.archivingNodeDisplacement(2,0,nstepArch);
mysolver.archivingNodeDisplacement(2,1,nstepArch);
mysolver.archivingNodeDisplacement(2,2,nstepArch);
mysolver.archivingNodeDisplacement(2,3,nstepArch);
mysolver.archivingNodeDisplacement(3,0,nstepArch);
mysolver.archivingNodeDisplacement(3,1,nstepArch);
mysolver.archivingNodeDisplacement(3,2,nstepArch);
mysolver.archivingNodeDisplacement(3,3,nstepArch);
mysolver.archivingNodeDisplacement(4,0,nstepArch);
mysolver.archivingNodeDisplacement(4,1,nstepArch);
mysolver.archivingNodeDisplacement(4,2,nstepArch);
mysolver.archivingNodeDisplacement(4,3,nstepArch);
mysolver.archivingNodeDisplacement(5,0,nstepArch);
mysolver.archivingNodeDisplacement(5,1,nstepArch);
mysolver.archivingNodeDisplacement(5,2,nstepArch);
mysolver.archivingNodeDisplacement(5,3,nstepArch);
mysolver.archivingNodeDisplacement(6,0,nstepArch);
mysolver.archivingNodeDisplacement(6,1,nstepArch);
mysolver.archivingNodeDisplacement(6,2,nstepArch);
mysolver.archivingNodeDisplacement(6,3,nstepArch);
mysolver.archivingNodeDisplacement(7,0,nstepArch);
mysolver.archivingNodeDisplacement(7,1,nstepArch);
mysolver.archivingNodeDisplacement(7,2,nstepArch);
mysolver.archivingNodeDisplacement(7,3,nstepArch);
mysolver.archivingNodeDisplacement(8,0,nstepArch);
mysolver.archivingNodeDisplacement(8,1,nstepArch);
mysolver.archivingNodeDisplacement(8,2,nstepArch);
mysolver.archivingNodeDisplacement(8,3,nstepArch);

mysolver.archivingForceOnPhysicalGroup("Face", 1234, 0)
mysolver.archivingForceOnPhysicalGroup("Face", 1234, 1)
mysolver.archivingForceOnPhysicalGroup("Face", 1234, 2)
mysolver.archivingForceOnPhysicalGroup("Face", 1234, 3)
mysolver.archivingForceOnPhysicalGroup("Face", 4158, 0)
mysolver.archivingForceOnPhysicalGroup("Face", 4158, 3)
mysolver.archivingForceOnPhysicalGroup("Face", 1265, 1)
mysolver.archivingForceOnPhysicalGroup("Face", 1265, 2)
mysolver.archivingForceOnPhysicalGroup("Face", 1265, 3)
mysolver.archivingForceOnPhysicalGroup("Face", 5678, 0)
mysolver.archivingForceOnPhysicalGroup("Face", 5678, 1)
mysolver.archivingForceOnPhysicalGroup("Face", 5678, 2)
mysolver.archivingForceOnPhysicalGroup("Face", 5678, 3)
mysolver.archivingForceOnPhysicalGroup("Face", 2376, 0)
mysolver.archivingForceOnPhysicalGroup("Face", 2376, 3)
mysolver.archivingForceOnPhysicalGroup("Face", 3487, 2)
mysolver.archivingForceOnPhysicalGroup("Face", 3487, 3)

mysolver.archivingIPOnPhysicalGroup("Volume",	nfield, IPField.SVM,		IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	nfield, IPField.PLASTICSTRAIN,	IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	nfield, IPField.epsR,		IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	nfield, IPField.TEMPERATURE,	IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	nfield, IPField.STRAIN_XX,	IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	nfield, IPField.STRAIN_YY,	IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	nfield, IPField.STRAIN_ZZ,	IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	nfield, IPField.STRAIN_XY,	IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	nfield, IPField.STRAIN_YZ,	IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	nfield, IPField.STRAIN_XZ,	IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	nfield, IPField.SIG_XX,		IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	nfield, IPField.SIG_YY,		IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	nfield, IPField.SIG_ZZ,		IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	nfield, IPField.SIG_XY,		IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	nfield, IPField.SIG_YZ,		IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	nfield, IPField.SIG_XZ,		IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	nfield, IPField.F_XX,		IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	nfield, IPField.F_YY,		IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	nfield, IPField.F_ZZ,		IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	nfield, IPField.JACOBIAN,	IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	nfield, IPField.P_XX,		IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	nfield, IPField.P_YY,		IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	nfield, IPField.P_ZZ,		IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	nfield, IPField.S_XX,		IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	nfield, IPField.S_YY,		IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	nfield, IPField.S_ZZ,		IPField.MEAN_VALUE);

mysolver.archivingIPOnPhysicalGroup("Volume",	nfield, IPField.FthI_YY,	IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	nfield, IPField.FthI_ZZ,	IPField.MEAN_VALUE);
#mysolver.archivingIPOnPhysicalGroup("Volume",	nfield, IPField.FthAI_YY,	IPField.MEAN_VALUE);
#mysolver.archivingIPOnPhysicalGroup("Volume",	nfield, IPField.FthAI_ZZ,	IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	nfield, IPField.FthAD_YY,	IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	nfield, IPField.FthAD_ZZ,	IPField.MEAN_VALUE);

mysolver.archivingIPOnPhysicalGroup("Volume",	nfield, IPField.FthG_YY,	IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	nfield, IPField.FthG_ZZ,	IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	nfield, IPField.FthR_YY,	IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	nfield, IPField.FthR_ZZ,	IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	nfield, IPField.FfG_YY,		IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	nfield, IPField.FfG_ZZ,		IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	nfield, IPField.FpG_YY,		IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	nfield, IPField.FpG_ZZ,		IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	nfield, IPField.FveG_YY,	IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	nfield, IPField.FveG_ZZ,	IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	nfield, IPField.FP_YY,		IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	nfield, IPField.FP_ZZ,		IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	nfield, IPField.FpR_YY,		IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	nfield, IPField.FpR_ZZ,		IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	nfield,	IPField.FveR_YY,	IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	nfield,	IPField.FveR_ZZ,	IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	nfield,	IPField.ZG,		IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	nfield,	IPField.PDF,		IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	nfield,	IPField.TT,		IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	nfield,	IPField.WT,		IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	nfield,	IPField.THERMALFLUX_X,	IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	nfield,	IPField.THERMALFLUX_Y,	IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	nfield,	IPField.THERMALFLUX_Z,	IPField.MEAN_VALUE);

mysolver.archivingIPOnPhysicalGroup("Volume",	nfield, IPField.FthAM_YY,	IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	nfield, IPField.FthAM_ZZ,	IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	nfield, IPField.FfAM_YY,	IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	nfield, IPField.FfAM_ZZ,	IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	nfield, IPField.FpAM_YY,	IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	nfield, IPField.FpAM_ZZ,	IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	nfield, IPField.FveAM_YY,	IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	nfield, IPField.FveAM_ZZ,	IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	nfield, IPField.epsAM,		IPField.MEAN_VALUE);

mysolver.archivingIPOnPhysicalGroup("Volume",	nfield, IPField.Tcrys,	IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	nfield, IPField.Tmelt,	IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	nfield, IPField.Wcrys,	IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	nfield, IPField.Wmelt,	IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	nfield, IPField.TRefG,	IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	nfield, IPField.TRefR,	IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	nfield, IPField.TRefAM, IPField.MEAN_VALUE);

#mysolver.archivingIPOnPhysicalGroup("Volume",	nfield, IPField.AGl_YY,	IPField.MEAN_VALUE);
#mysolver.archivingIPOnPhysicalGroup("Volume",	nfield, IPField.AGl_ZZ,	IPField.MEAN_VALUE);
#mysolver.archivingIPOnPhysicalGroup("Volume",	nfield, IPField.ARl_YY,	IPField.MEAN_VALUE);
#mysolver.archivingIPOnPhysicalGroup("Volume",	nfield, IPField.ARl_ZZ,	IPField.MEAN_VALUE);
#mysolver.archivingIPOnPhysicalGroup("Volume",	nfield, IPField.AAMl_YY, IPField.MEAN_VALUE);
#mysolver.archivingIPOnPhysicalGroup("Volume",	nfield, IPField.AAMl_ZZ, IPField.MEAN_VALUE);

mysolver.archivingIPOnPhysicalGroup("Volume",	nfield, IPField.ADl_XX,	IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	nfield, IPField.ADl_YY,	IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	nfield, IPField.ADl_ZZ,	IPField.MEAN_VALUE);

mysolver.archivingIPOnPhysicalGroup("Volume",	nfield, IPField.EnThG_XX,	IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	nfield, IPField.EnThG_YY,	IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	nfield, IPField.EnThG_ZZ,	IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	nfield, IPField.EnThR_XX,	IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	nfield, IPField.EnThR_YY,	IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	nfield, IPField.EnThR_ZZ,	IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	nfield, IPField.EnThAM_XX,	IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	nfield, IPField.EnThAM_YY,	IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	nfield, IPField.EnThAM_ZZ,	IPField.MEAN_VALUE);

mysolver.archivingIPOnPhysicalGroup("Volume",	nfield, IPField.appliedEnergy,	IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	nfield, IPField.DEFO_ENERGY,	IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	nfield, IPField._thermalEnergy,	IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	nfield, IPField.freezedDiss,	IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	nfield, IPField.plasticDiss,	IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	nfield, IPField.viscousDiss,	IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	nfield, IPField.EDiss,		IPField.MEAN_VALUE);

#===Solving===================================================================================
mysolver.solve()


check = TestCheck()
check.equal(-5.000108e-01,mysolver.getArchivedForceOnPhysicalGroup("Face", 5678, 3),1.e-6)
check.equal(2.645715e+02,mysolver.getArchivedNodalValue(1,3,mysolver.displacement),1.e-6)
check.equal(1.079347e-08,mysolver.getArchivedNodalValue(7,0,mysolver.displacement),1.e-6)

