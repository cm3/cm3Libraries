# test file

set(PYFILE electroThermoLaminate.py)

set(FILES2DELETE 
  disp*.msh
  *.csv
  stress*.msh
)

add_cm3python_test(${PYFILE} "${FILES2DELETE}")
