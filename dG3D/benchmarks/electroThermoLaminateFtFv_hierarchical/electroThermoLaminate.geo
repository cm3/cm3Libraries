// Test case a SCB with a vertical load at its free extremity
// Size

//definition of unit
mm = 1e-03;

// volum fraction

x=1./7.5*mm;
y=1.4*mm;
z=1*mm;

// Characteristic length
Lc1=z/2.5;

// definition of points

Point(1) = { 0.0  , 0.0  , 0.0 , Lc1};
Point(2) = { 2.5*x , 0.0  , 0.0 , Lc1};
Point(3) = { 5.*x , 0.0  , 0.0 , Lc1};
Point(4) = { 7.5*x , 0.0  , 0.0 , Lc1};
Point(5) = { 0.0 , 0.0 , z , Lc1};
Point(6) = { 2.5*x , 0.0  ,z , Lc1};
Point(7) = { 5.*x , 0.0  , z , Lc1};
Point(8) = { 7.5*x , 0.0  , z , Lc1};



Line(1) = {5, 1};
Line(2) = {1, 2};
Line(3) = {2, 6};
Line(4) = {6, 5};
Line(5) = {2, 3};
Line(6) = {3, 7};
Line(7) = {7, 6};
Line(8) = {3, 4};
Line(9) = {4, 8};
Line(10) = {8, 7};


Line Loop(11) = {1, 2, 3, 4};
Plane Surface(1) = {11};

Line Loop(13) = {5, 6, 7, -3};
Plane Surface(2) = {13};

Line Loop(15) = {8, 9, 10, -6};
Plane Surface(3) = {15};


//VOlume of fiber and matrix ******************************************************
my_mtrixV[] = Extrude {0.0 , y , 0.0} {Surface {1,3}; Layers{5};Recombine;};
my_fiberV[] = Extrude {0.0 ,y , 0.0} {Surface{2}; Layers{5};Recombine;};


/*Physical Surface(83) = {51, 25, 69};
Physical Surface(84) = {12, 14, 16};
Physical Surface(85) = {47};
Physical Surface(86) = {73};*/


Physical Point(88) = {5};
Physical Point(89) = {6};
Physical Point(90) = {7};
Physical Point(91) = {8};

Physical Line(123) = {9};

Physical Surface(83) = {28, 68, 46};
Physical Surface(84) = {1, 2, 3};
Physical Surface(85) = {24};
Physical Surface(86) = {50};

/*Physical Surface(83) = {50, 24, 68};
Physical Surface(84) = {1, 2, 3};
Physical Surface(85) = {46};
Physical Surface(86) = {72};*/

// Physical objects to applied material*********************
Physical Volume(51) ={my_mtrixV[]};
Physical Volume(52) ={my_fiberV[]};

// define transfinite mesh
//Transfinite Line {1,2,3,4,5,6,7,8,9,10,17,18,19,20,39,40,42,61,62,63} = 2 Using Progression 1;
//Transfinite Line {44,45,31,22,23,27,67,71} = 1 Using Progression 1;

Transfinite Line {22,23,27,31,44,45,53,49} = 2 Using Progression 1;
Transfinite Line {1,3,6,9,17,19,42,40} = 2 Using Progression 1;
Transfinite Line {2,4,5,7,8,10,18,20,39,41,61,63} = 3 Using Progression 1; //8

Transfinite Surface {1};
Transfinite Surface {2};
Transfinite Surface {3};


Recombine Surface {1};
Recombine Surface {2};
Recombine Surface {3};

Transfinite Volume {1};
Transfinite Volume {2};
Transfinite Volume {3};





