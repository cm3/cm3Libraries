#coding-Utf-8-*-
from gmshpy import *

#from dG3DpyDebug import*
from dG3Dpy import*

#script to launch composite problem with a python script

# material law
lawnum= 1 
rho   = 1020.
cv=1.
mu_groundState3=0.75e6
Im3=5.
pi=3.14159
tinitial = 273.+25.  #58
Tr=310.
Nmu_groundState2= 0.045
mu_groundState2=1.38e6
Im2=6.3
Sgl2=58.e6 
Sr2=3.e2
Delta=2.6
m2=0.19
epsilonr=5.2e-4	
n=2.1
epsilonp02=5.2e-4
#alphar1=25.e-5
#alphagl1=13.e-5
Ggl1=156.e6
Gr1=13.4e6
Mgl1=7.4e6
Mr1=0.168e6
Mugl1=0.35
Mur1=0.49
epsilon01=1.73e13
Qgl1=1.4e-19
Qr1=0.2e-21
epsygl1=0.14 
#epsyr1=0.
d1=0.015
Kb=1.3806488e-23
m1=0.17
V1=2.16e-27
alphap=0.058
Sa0=0.
ha1=230.
b1=5850e6
g1=5.8
phai01=0.
Z1=0.083
r1=1.3
s1=0.005
Sb01=0.
Hgl1=1.56e6
Lgl1=0.44e6
Hr1=0.76e6
Lr1=0.006e6
l1=0.5
be1=0.5
alpha = beta=gamma=0.
#v=0.6
wp=0.6
c0=1710.*rho  #1850 #pe
c1=4.1*rho
#alphax=alphay=alphaz=12.e-6 #for smp
Kx=Ky=Kz=0.5#0.5 for smp
lx=ly=lz=0.1#0.098#0.101 for smp 0.098
seebeck=21.e-6#21.e-6#21.e-6#10.e-7#10.e-6 for smp
alphar1=25.e-5
alphagl1=13.e-5
v0=0.


#cf
lawnumcf = 2
rhocf = 1750.
#young =Ex=Ey=Ez =190.e12 #2.1e11
Ecf=40.e9
EA=230.e9
GA=24.e9
nucf=0.2
nu_minor=0.256*Ecf/EA 
Ax=0.#1.
Ay=0.#1.
Az=1.#0.#
kxcf=kycf=kzcf= 2.0# 20.#1.612#24.3  #21-181   !!
alphacf=2.e-6#11.e-6#12.e-6
lxcf=lycf=lzcf=33333.#59880.#8.4e3#7.e6#     5.9e6 #carbon  59880.  !!
seebeckcf=3.e-6##1.941e-4#1.9e-6  #carbon
cpcf= 712.*rhocf #for paper material #for carbon graphit 712 or 544.  CF is 1780


# geometry
geofile="electroThermoLaminate.geo" # name of mesh file
meshfile="electroThermoLaminate.msh" # name of mesh file

# solver
sol = 2  # Gmm=0 (default) Taucs=1 PETsc=2
soltype = 1 # StaticLinear=0 (default) StaticNonLinear=1
#nstep =400#3000   # number of step (used only if soltype=1)#200
#ftime =50#1500 # Final time (used only if soltype=1)#1  50
nstep =10#3000   # number of step (used only if soltype=1)#200 1000
ftime =10.#1500 # Final time (used only if soltype=1)#1  50
tol=1.e-5   # relative tolerance for NR scheme (used only if soltype=1)
nstepArch=1 # Number of step between 2 archiving (used only if soltype=1)
fullDg = 1 #O = CG, 1 = DG
beta1 = 1000.#40
eqRatio =1.e8#1.e8

#  compute solution and BC (given directly to the solver
# creation of law

law2 =mlawAnIsotropicElecTherMechDG3DMaterialLaw(lawnumcf,Ecf,nucf,rhocf,EA,GA,nu_minor,Ax,Ay,Az,alpha, beta,gamma,tinitial, lxcf,lycf,lzcf,seebeckcf,kxcf, kycf,kzcf,cpcf,alphacf,v0)
law2.setUseBarF(True)
law1   = mlawElecSMPDG3DMaterialLaw( lawnum,rho,alpha,beta,gamma,tinitial,Kx,Ky,Kz,mu_groundState3,Im3,pi,Tr,Nmu_groundState2, mu_groundState2,  Im2,Sgl2, Sr2, Delta, m2,epsilonr,n,epsilonp02, alphar1, alphagl1, Ggl1, Gr1, Mgl1, Mr1, Mugl1, Mur1, epsilon01, Qgl1, Qr1, epsygl1 , d1,  m1, V1,  alphap, Sa0, ha1, b1, g1, phai01, Z1,r1, s1, Sb01, Hgl1, Lgl1, Hr1, Lr1, l1, Kb, be1,c0,wp,c1,lx,ly,lz,seebeck,v0)
law1.setUseBarF(True)
# creation of ElasticField
Mfield = 51 # number of the field (physical number of Matrix)
Ffield = 52 # number of the field (physical number of Fiber)

space1 = 1 # function space (Lagrange=0 Hierarchial = 1)

Matixfield = dG3DDomain(10,Mfield,space1,lawnum,fullDg,3,0,2)
Matixfield.gaussIntegration(0,4,5)
Matixfield.setHierarchicalOrder(0,2) # order 2 for displacement field
Matixfield.setHierarchicalOrder(3,1)
Matixfield.setHierarchicalOrder(4,1)
Fiberfield = dG3DDomain(11,Ffield,space1,lawnumcf,fullDg,3,0,2)
Fiberfield.gaussIntegration(0,4,5)
Fiberfield.setHierarchicalOrder(0,2) # order 2 for displacement field
Fiberfield.setHierarchicalOrder(3,1)
Fiberfield.setHierarchicalOrder(4,1)


Matixfield.setConstitutiveExtraDofDiffusionEqRatio(eqRatio)
Fiberfield.setConstitutiveExtraDofDiffusionEqRatio(eqRatio)
Matixfield.stabilityParameters(beta1)
Fiberfield.stabilityParameters(beta1)
Matixfield.setConstitutiveExtraDofDiffusionStabilityParameters(beta1,bool(1))
Fiberfield.setConstitutiveExtraDofDiffusionStabilityParameters(beta1,bool(1))
Matixfield.setConstitutiveExtraDofDiffusionUseEnergyConjugatedField(bool(1))
Fiberfield.setConstitutiveExtraDofDiffusionUseEnergyConjugatedField(bool(1))

#interface // we need to create the ininterdomain after adding the domains
myinterfield=interDomainBetween3D(12,Matixfield,Fiberfield) 
myinterfield.setConstitutiveExtraDofDiffusionEqRatio(eqRatio);
myinterfield.stabilityParameters(beta1)
myinterfield.setConstitutiveExtraDofDiffusionStabilityParameters(beta1,bool(1))
myinterfield.matrixByPerturbation(0,1e-8)
myinterfield.setConstitutiveExtraDofDiffusionUseEnergyConjugatedField(bool(1))

Matixfield.matrixByPerturbation(0,0,0,1e-8)
#Matixfield.matrixByPerturbation(1,1,1,1e-8)

Fiberfield.matrixByPerturbation(0,0,0,1e-8)
#Fiberfield.matrixByPerturbation(1,1,1,1e-8)

# creation of Solver
mysolver = nonLinearMechSolver(1000)
#mysolver.createModel(geofile,meshfile,3,1)
mysolver.loadModel(meshfile)
mysolver.addDomain(Matixfield)
mysolver.addDomain(Fiberfield)
mysolver.addMaterialLaw(law1)
mysolver.addMaterialLaw(law2)
mysolver.addDomain(myinterfield)
mysolver.Scheme(soltype)
mysolver.Solver(sol)
mysolver.snlData(nstep,ftime,tol,1.e-9)#forsmp
#mysolver.options("-petsc_direct")
mysolver.options("-ksp_type preonly")
mysolver.options("-pc_type lu")

#mysolver.snlData(nstep,ftime,tol,1.e-19)
mysolver.stepBetweenArchiving(nstepArch)
mysolver.snlManageTimeStep(25, 3, 2, 10)

# BC
##cyclicFunctionDisp=cycleFunctionTime(0., 0., ftime/5., -0.0001, ftime , -0.0001);
mysolver.displacementBC("Face",83,2,0.)
mysolver.displacementBC("Face",84,1,0.)
mysolver.displacementBC("Face",85,0,0.)

#thermal BC
cyclicFunctionTemp1=cycleFunctionTime(0., 1./tinitial,ftime,1./tinitial);
#cyclicFunctionTemp2=cycleFunctionTime(0., 1./tinitial,ftime,1./(tinitial+25.));
mysolver.displacementBC("Face",85,3,cyclicFunctionTemp1)
#mysolver.displacementBC("Face",86,3,cyclicFunctionTemp2)
mysolver.initialBC("Volume","Position",Mfield,3,1./tinitial)
mysolver.initialBC("Volume","Position",Ffield,3,1./tinitial)
#mysolver.displacementBC("Volume",Mfield,3,1./tinitial)

#electrical BC
cyclicFunctionvolt1=cycleFunctionTime(0., 0.,  ftime,-10./tinitial);#0.058
cyclicFunctionvolt2=cycleFunctionTime(0., 0.,  ftime,0.);
mysolver.displacementBC("Face",85,4,cyclicFunctionvolt1)
mysolver.displacementBC("Face",86,4,cyclicFunctionvolt2)
mysolver.initialBC("Volume","Position",Mfield,4,0.)
mysolver.initialBC("Volume","Position",Ffield,4,0.)


mysolver.internalPointBuildView("svm",IPField.SVM, 1, 1)
mysolver.internalPointBuildView("sig_xx",IPField.SIG_XX, 1, 1)
mysolver.internalPointBuildView("sig_yy",IPField.SIG_YY, 1, 1)
mysolver.internalPointBuildView("sig_zz",IPField.SIG_ZZ, 1, 1)
mysolver.internalPointBuildView("sig_xy",IPField.SIG_XY, 1, 1)
mysolver.internalPointBuildView("sig_yz",IPField.SIG_YZ, 1, 1)
mysolver.internalPointBuildView("sig_xz",IPField.SIG_XZ, 1, 1)
mysolver.internalPointBuildView("temperature",IPField.TEMPERATURE, 1, 1)
mysolver.internalPointBuildView("qx",IPField.THERMALFLUX_X, 1, 1)
mysolver.internalPointBuildView("qy",IPField.THERMALFLUX_Y, 1, 1)
mysolver.internalPointBuildView("qz",IPField.THERMALFLUX_Z, 1, 1)
mysolver.internalPointBuildView("voltage",IPField.VOLTAGE, 1, 1)
mysolver.internalPointBuildView("ex",IPField.ELECTRICALFLUX_X, 1, 1)
mysolver.internalPointBuildView("ey",IPField.ELECTRICALFLUX_Y, 1, 1)
mysolver.internalPointBuildView("ez",IPField.ELECTRICALFLUX_Z, 1, 1)

#mysolver.archivingIPOnPhysicalGroup("Volume",Mfield, IPField.SIG_ZZ,IPField.MEAN_VALUE,nstepArch);
#mysolver.archivingIPOnPhysicalGroup("Volume",Mfield, IPField.STRAIN_ZZ,IPField.MEAN_VALUE,nstepArch);
#mysolver.archivingIPOnPhysicalGroup("Volume",Mfield, IPField.TEMPERATURE,IPField.MEAN_VALUE,nstepArch);
#mysolver.archivingIPOnPhysicalGroup("Volume",Mfield, IPField.VOLTAGE,IPField.MEAN_VALUE,nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",Ffield, IPField.SIG_ZZ,IPField.MEAN_VALUE,nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",Ffield, IPField.STRAIN_ZZ,IPField.MEAN_VALUE,nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",Ffield, IPField.TEMPERATURE,IPField.MEAN_VALUE,nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",Ffield, IPField.VOLTAGE,IPField.MEAN_VALUE,nstepArch);


mysolver.archivingNodeDisplacement(88,3,1);
mysolver.archivingNodeDisplacement(89,3,1);
mysolver.archivingNodeDisplacement(90,3,1);
mysolver.archivingNodeDisplacement(91,3,1);
mysolver.archivingNodeDisplacement(91,0,1);

mysolver.archivingForceOnPhysicalGroup("Face", 85, 3)
mysolver.archivingForceOnPhysicalGroup("Face", 85, 4)


mysolver.archivingNodeDisplacement(88,4,1);
mysolver.archivingNodeDisplacement(89,4,1);
mysolver.archivingNodeDisplacement(90,4,1);
mysolver.archivingNodeDisplacement(91,4,1);

mysolver.archivingNodeIP(88, IPField.THERMALFLUX_X,IPField.MEAN_VALUE,1);
mysolver.archivingNodeIP(89, IPField.THERMALFLUX_X,IPField.MEAN_VALUE,1);
mysolver.archivingNodeIP(90, IPField.THERMALFLUX_X,IPField.MEAN_VALUE,1);
mysolver.archivingNodeIP(91, IPField.THERMALFLUX_X,IPField.MEAN_VALUE,1);


mysolver.archivingNodeIP(88, IPField.ELECTRICALFLUX_X,IPField.MEAN_VALUE,1);
mysolver.archivingNodeIP(89, IPField.ELECTRICALFLUX_X,IPField.MEAN_VALUE,1);
mysolver.archivingNodeIP(90, IPField.ELECTRICALFLUX_X,IPField.MEAN_VALUE,1);
mysolver.archivingNodeIP(91, IPField.ELECTRICALFLUX_X,IPField.MEAN_VALUE,1);

mysolver.solve()


check = TestCheck()
check.equal(-2.830796e+05,mysolver.getArchivedForceOnPhysicalGroup("Face", 85, 3),1.e-5)
check.equal(-2.098550e+05,mysolver.getArchivedForceOnPhysicalGroup("Face", 85, 4),1.e-5)
check.equal(3.355705e-03,mysolver.getArchivedNodalValue(88,3,mysolver.displacement),1.e-5)
check.equal(-3.355705e-02,mysolver.getArchivedNodalValue(88,4,mysolver.displacement),1.e-5)
check.equal(3.286737e-03,mysolver.getArchivedNodalValue(89,3,mysolver.displacement),1.e-5)
check.equal(-1.643541e-02,mysolver.getArchivedNodalValue(89,4,mysolver.displacement),1.e-5)
check.equal(3.275667e-03,mysolver.getArchivedNodalValue(90,3,mysolver.displacement),1.e-5)
check.equal(-1.638000e-02,mysolver.getArchivedNodalValue(90,4,mysolver.displacement),1.e-5)
check.equal(3.252784e-03,mysolver.getArchivedNodalValue(91,3,mysolver.displacement),1.e-5)
check.equal(0.,mysolver.getArchivedNodalValue(91,4,mysolver.displacement),1.e-5)
check.equal(8.695207e-07,mysolver.getArchivedNodalValue(91,0,mysolver.displacement),1.e-5)





 
