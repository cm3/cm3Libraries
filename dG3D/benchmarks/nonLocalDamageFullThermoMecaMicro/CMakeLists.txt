# test file

set(PYFILE run.py)

set(FILES2DELETE 
  rve.msh
  E_0_GP_0_*
)

add_cm3python_test(${PYFILE} "${FILES2DELETE}")
