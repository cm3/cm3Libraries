#coding-Utf-8-*-

from gmshpy import *
from dG3Dpy import*

#script to launch PBC problem with a python script

# material law
lawnum1 = 11
Em = 75.e9 #MPa
num = 0.33
sy0m = 300.e6
Hm = 150.e6;

alpm = 2.36e-5# thermal expansion
kappam =247. #thermal conductivity
rhom = 2700.
cpm = rhom*9.e2
TQfm = 0.9   # convert plastic to heat

Tr = 293. # reference temperature
cl1     = IsotropicCLengthLaw(1, 4e-8)
damlaw1 = SimpleSaturateDamageLaw(1, 300.,0.95,0.,2.)
law1 =  NonLocalDamageJ2FullyCoupledDG3DMaterialLaw (lawnum1,rhom,Em,num,sy0m,Hm,cl1,damlaw1,1e-6,False,1e-8)
law1.setStrainOrder(1)
law1.setReferenceTemperature(Tr)
law1.setReferenceThermalExpansionCoefficient(alpm)
law1.setReferenceThermalConductivity(kappam)
law1.setReferenceCp(cpm)
law1.setTaylorQuineyFactor(TQfm)
# law by perturbation
pertLaw1 = dG3DMaterialLawWithTangentByPerturbation(law1,1e-8)


lawnum2 = 12
Ef = 385.e9 #MPa
nuf = 0.2
sy0f = 300.e100
Hf = 150.e100;

alpf = 5e-6# thermal expansion
kappaf =38. #thermal conductivity
rhof = 2600.
cpf = rhof*1.3e3
TQff = 0.   # convert plastic to heat

cl2     = IsotropicCLengthLaw(2, 1.e-12)
damlaw2 = SimpleSaturateDamageLaw(2, 300.,0.95,1.0,2.)

law2 =  NonLocalDamageJ2FullyCoupledDG3DMaterialLaw(lawnum2,rhof,Ef,nuf,sy0f,Hf,cl2,damlaw2,1e-6,False,1e-8)
law2.setStrainOrder(1)
law2.setReferenceTemperature(Tr)
law2.setReferenceThermalExpansionCoefficient(alpf)
law2.setReferenceThermalConductivity(kappaf)
law2.setReferenceCp(cpf)
law2.setTaylorQuineyFactor(TQff)
# law by perturbation
pertLaw2 = dG3DMaterialLawWithTangentByPerturbation(law2,1e-8)


# geometry
geofile="rve.geo" # name of mesh file
meshfile="rve.msh" # name of mesh file

# creation of part Domain
thermalSource = True
mecaSource = True
ExtraEqRatio = 1.e2

myfield1 = dG3DDomain(1000,11,0,lawnum1,0,3,1,1)
myfield1.setConstitutiveExtraDofDiffusionAccountSource(thermalSource,mecaSource)
myfield1.setNonLocalEqRatio(ExtraEqRatio)
myfield1.setConstitutiveExtraDofDiffusionEqRatio(ExtraEqRatio)

myfield2 = dG3DDomain(1000,12,0,lawnum2,0,3,1,1)
myfield2.setConstitutiveExtraDofDiffusionAccountSource(thermalSource,mecaSource)
myfield2.setNonLocalEqRatio(ExtraEqRatio)
myfield2.setConstitutiveExtraDofDiffusionEqRatio(ExtraEqRatio)
myfield2.setNewIdForComp(3,100)

# solver
sol = 2  # Gmm=0 (default) Taucs=1 PETsc=2
soltype =1 # StaticLinear=0 (default) StaticNonLinear=1
nstep = 500  # number of step (used only if soltype=1)
ftime =1.   # Final time (used only if soltype=1)
tol=1.e-4  # relative tolerance for NR scheme (used only if soltype=1)
nstepArch=1 # Number of step between 2 archiving (used only if soltype=1)
system = 3 # Displacement elimination =0 Multiplier elimination = 1 Displacement+ multiplier = 2
control = 0 # load control = 0 arc length control euler = 1

# creation of Solver
mysolver = nonLinearMechSolver(1000)
mysolver.createModel(geofile,meshfile,3,1)
mysolver.addDomain(myfield1)
mysolver.addMaterialLaw(law1)
#mysolver.addMaterialLaw(pertLaw1)
mysolver.addDomain(myfield2)
mysolver.addMaterialLaw(law2)
#mysolver.addMaterialLaw(pertLaw2)
mysolver.Scheme(soltype)
mysolver.Solver(sol)
mysolver.snlData(nstep,ftime,tol)
mysolver.setSystemType(system)
mysolver.setControlType(control)
mysolver.stiffnessModification(True)
mysolver.iterativeProcedure(True)
mysolver.setMessageView(True)

mysolver.initialBC("Volume","Position",11,4,Tr)
mysolver.initialBC("Volume","Position",12,4,Tr)


#boundary condition
microBC = nonLinearPeriodicBC(10,3)
microBC.setOrder(1)
microBC.setDofPartition(3,1,0,1) # 3 mechnics DOFs, 1 constitutive extra-dof, and 0 non-constitutive extraDof
microBC.setBCPhysical(1,3,5,2,4,6) #Periodic boundary condition
method = 5	# Periodic mesh = 0, Langrange interpolation = 1, Cubic spline interpolation =2,  FE linear= 3, FE Quad = 4
degree = 5	# Order used for polynomial interpolation 
addvertex = False # Polynomial interpolation by mesh vertex = 0, Polynomial interpolation by virtual vertex 
microBC.setPeriodicBCOptions(method, degree,bool(addvertex)) 

microBC.setInitialConstitutiveExtraDofDiffusionValue(0,Tr); #intial value for conExtraDof

 # Deformation gradient
microBC.setDeformationGradient(1.1,0.1,0.0,0.0,1.,0,0,0,1.0)
microBC.setConstitutiveExtraDofDiffusionGradient(0,1.e4, 1.e4, 1e4);
microBC.setConstitutiveExtraDofDiffusionValue(0,Tr+220.);

mysolver.addMicroBC(microBC)

#stress averaging flag and averaging method 0- VOLUME, 1- SURFACE
mysolver.stressAveragingFlag(True) # set stress averaging ON- 0 , OFF-1
mysolver.setStressAveragingMethod(0) # 0 -volume 1- surface
#tangent averaging flag
mysolver.tangentAveragingFlag(True) # set tangent averaging ON -0, OFF -1
mysolver.setTangentAveragingMethod(2,1e-6) # 0- perturbation 1- condensation

#mysolver.setExtractPerturbationToFileFlag(0)	


# build view
mysolver.internalPointBuildView("Green-Lagrange_xx",IPField.STRAIN_XX, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_yy",IPField.STRAIN_YY, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_zz",IPField.STRAIN_ZZ, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_xy",IPField.STRAIN_XY, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_yz",IPField.STRAIN_YZ, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_xz",IPField.STRAIN_XZ, 1, 1);
mysolver.internalPointBuildView("sig_xx",IPField.SIG_XX, 1, 1);
mysolver.internalPointBuildView("sig_yy",IPField.SIG_YY, 1, 1);
mysolver.internalPointBuildView("sig_zz",IPField.SIG_ZZ, 1, 1);
mysolver.internalPointBuildView("sig_xy",IPField.SIG_XY, 1, 1);
mysolver.internalPointBuildView("sig_yz",IPField.SIG_YZ, 1, 1);
mysolver.internalPointBuildView("sig_xz",IPField.SIG_XZ, 1, 1);
mysolver.internalPointBuildView("sig_VM",IPField.SVM, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange equivalent strain",IPField.GL_EQUIVALENT_STRAIN, 1, 1);
mysolver.internalPointBuildView("Equivalent plastic strain",IPField.PLASTICSTRAIN, 1, 1);
mysolver.internalPointBuildView("epl",IPField.PLASTICSTRAIN, 1, 1)
mysolver.internalPointBuildView("Damage",IPField.DAMAGE, 1, 1)
mysolver.internalPointBuildView("active Damage zone",IPField.ACTIVE_DISSIPATION, 1, 1)
mysolver.internalPointBuildView("eps local",IPField.LOCAL_0, 1, 1)
mysolver.internalPointBuildView("eps non local",IPField.NONLOCAL_0, 1, 1)

mysolver.internalPointBuildView("temperature",IPField.TEMPERATURE, 1, 1)
mysolver.internalPointBuildView("qx",IPField.THERMALFLUX_X, 1, 1)
mysolver.internalPointBuildView("qy",IPField.THERMALFLUX_Y, 1, 1)
mysolver.internalPointBuildView("qz",IPField.THERMALFLUX_Z, 1, 1)

mysolver.internalPointBuildView("mecaSource",IPField.MECHANICAL_SOURCE,1,1)
mysolver.internalPointBuildView("field source",IPField.FIELD_SOURCE,1,1)


# solve
mysolver.solve()

# test check
check = TestCheck()
check.equal(7.030523e+08,mysolver.getHomogenizedStress(0,0),1.e-3)
check.equal(1.639034e+07,mysolver.getHomogenizedStress(0,1),1.e-3)
check.equal(7.176379e+08,mysolver.getHomogenizedStress(2,2),1.e-3)

check.equal(-2.730270e+09,mysolver.getHomogenizedTangent(0,0,0,0),1.e-3)
check.equal(1.219572e+10,mysolver.getHomogenizedTangent(0,0,1,1),2.e-3)
check.equal(1.625039e+09,mysolver.getHomogenizedTangent(0,1,0,1),1.e-3)


