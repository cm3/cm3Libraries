# test file

set(PYFILE model.py)

set(FILES2DELETE
	E_*.msh
	*.txt 
  *.csv
  disp*
  stress*
)

add_cm3python_mpi_test(4 ${PYFILE} "${FILES2DELETE}")
