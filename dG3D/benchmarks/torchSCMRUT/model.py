#coding-Utf-8-*-
import torch
from gmshpy import *
from dG3Dpy import*

#script to launch PBC problem with a python script
#DEFINE MICRO PROBLEM

cell=["SC_MRU_T","SC_MRU_I","SMRU"] #available cells
InpType =  cell[0] #choose the cell type
factorStep= 1 #number of steps x 20 during reloading between points B and C



# material law
matnum1 = 1 # unique number of law
rho = 7850e-9 # Bulk mass
Path = '../Model/Bounds_GS'
checkpoint = torch.load(Path)
[XGmin, XGmax] = checkpoint['Input_Bounds']
[YSmin, YSmax] = checkpoint['Output_Bounds']
print(XGmin, XGmax,YSmin, YSmax)

if(InpType == "SC_MRU_T"):
  EXXmean = (XGmax[0]+XGmin[0])/2.0
  EXXstd = (XGmax[0]-XGmin[0])/2.0
  EXYmean = (XGmax[1]+XGmin[1])/2.0
  EXYstd = (XGmax[1]-XGmin[1])/2.0
  EYYmean = (XGmax[2]+XGmin[2])/2.0
  EYYstd = (XGmax[2]-XGmin[2])/2.0
  EYZmean = 0.0
  EYZstd = 1.0
  EZZmean = 0.0
  EZZstd = 1.0
  EZXmean = 0.0
  EZXstd = 1.0
  model= '../Model/DInpFullModel.pt'
  kinematicinput=1
  extranorm=True
  doubleinput=True
elif(InpType == "SC_MRU_I"):
  model= "../Model/IncrementModel.pt"
  kinematicinput=4
  EXXmean = 0.0
  EXXstd = 1.0
  EXYmean = 0.0
  EXYstd = 1.0
  EYYmean = 0.0
  EYYstd = 1.0
  EYZmean = 0.0
  EYZstd = 1.0
  EZZmean = 0.0
  EZZstd = 1.0
  EZXmean = 0.0
  EZXstd = 1.0
  extranorm=False
  doubleinput=False
else:
  EXXmean = (XGmax[0]+XGmin[0])/2.0
  EXXstd = (XGmax[0]-XGmin[0])/2.0
  EXYmean = (XGmax[1]+XGmin[1])/2.0
  EXYstd = (XGmax[1]-XGmin[1])/2.0
  EYYmean = (XGmax[2]+XGmin[2])/2.0
  EYYstd = (XGmax[2]-XGmin[2])/2.0
  EYZmean = 0.0
  EYZstd = 1.0
  EZZmean = 0.0
  EZZstd = 1.0
  EZXmean = 0.0
  EZXstd = 1.0
  model= '../Model/FullModel.pt'#'../Model/model.pt'#
  kinematicinput=1
  extranorm=True
  doubleinput=False

SXXmean = (YSmax[0]+YSmin[0])/2.0
SXXstd = (YSmax[0]-YSmin[0])/2.0
SXYmean = (YSmax[1]+YSmin[1])/2.0
SXYstd = (YSmax[1]-YSmin[1])/2.0
SYYmean = (YSmax[2]+YSmin[2])/2.0
SYYstd = (YSmax[2]-YSmin[2])/2.0
SYZmean = 0.0
SYZstd = 1.0
SZZmean = (YSmax[3]+YSmin[3])/2.0
SZZstd = (YSmax[3]-YSmin[3])/2.0
SZXmean = 0.0
SZXstd = 1.0

numberOfInput = 3
numInternalVars = 120
initial_h = 0.0

macromat1 = torchANNBasedDG3DMaterialLaw(matnum1, rho, numberOfInput, numInternalVars, model, EXXmean, EXXstd, EXYmean, EXYstd, EYYmean, EYYstd, EYZmean, EYZstd, EZZmean, EZZstd, EZXmean, EZXstd, SXXmean, SXXstd, SXYmean, SXYstd, SYYmean, SYYstd, SYZmean, SYZstd, SZZmean,  SZZstd, SZXmean, SZXstd,True,1e-3)


macromat1.setInitialHValue(initial_h)
macromat1.setKinematicInput(kinematicinput)
macromat1.setNeedExtraNorm(extranorm)
macromat1.setDoubleInput(doubleinput)

macrogeofile="model.geo"
macromeshfile="model.msh" # name of mesh file

# solver
sol = 2  # Gmm=0 (default) Taucs=1 PETsc=2
soltype =1 # StaticLinear=0 (default) StaticNonLinear=1
nstep = 200  # number of step (used only if soltype=1)
ftime =1.   # Final time (used only if soltype=1)
tol=1.e-4  # relative tolerance for NR scheme (used only if soltype=1)
nstepArch=10 # Number of step between 2 archiving (used only if soltype=1)


# creation of  macro part Domain

dim =2
beta1 = 50;
fullDG = False;

averageStrainBased = True

# non DG domain
nfield1 = 11
macrodomain1 = dG3DDomain(10,nfield1,0,matnum1,fullDG,dim)
macrodomain1.stabilityParameters(beta1)

# creation of Solver
mysolver = nonLinearMechSolver(1000)
mysolver.createModel(macrogeofile,macromeshfile,2,2)
#mysolver.loadModel(macromeshfile)

mysolver.addDomain(macrodomain1)
mysolver.addMaterialLaw(macromat1)
mysolver.Scheme(soltype)
mysolver.Solver(sol)
mysolver.snlData(nstep,ftime,tol,tol/10.)
mysolver.snlManageTimeStep(50,5,2.,10)
mysolver.stepBetweenArchiving(nstepArch)


# boundary condition
mysolver.displacementBC("Face",11,2,0.0)

mysolver.displacementBC("Edge",2,1,0.0)
mysolver.displacementBC("Edge",5,0,0.0)

fct = PiecewiseLinearFunction()
fct.put(0.,0.)
fct.put(0.1,0.025)
fct.put(0.2,0.02)
fct.put(0.4,0.035)
fct.put(0.6,0.03)
fct.put(0.8,0.05)
fct.put(1.,0.0)
mysolver.displacementBC("Edge",4,1,fct)
unit = 1
mysolver.setNumStepTimeInterval(0.1,20*unit*1)
mysolver.setNumStepTimeInterval(0.2,3*unit)
mysolver.setNumStepTimeInterval(0.4,20*unit*factorStep)
mysolver.setNumStepTimeInterval(0.6,3*unit)
mysolver.setNumStepTimeInterval(0.8,50*unit)
mysolver.setNumStepTimeInterval(1.,20*unit)


# archivage


mysolver.internalPointBuildView("Green-Lagrange_xx",IPField.GL_XX, 1, nstepArch);
mysolver.internalPointBuildView("Green-Lagrange_yy",IPField.GL_YY, 1, nstepArch);
mysolver.internalPointBuildView("Green-Lagrange_zz",IPField.GL_ZZ, 1, nstepArch);
mysolver.internalPointBuildView("Green-Lagrange_xy",IPField.GL_XY, 1, nstepArch);
mysolver.internalPointBuildView("Green-Lagrange_yz",IPField.GL_YZ, 1, nstepArch);
mysolver.internalPointBuildView("Green-Lagrange_xz",IPField.GL_XZ, 1, nstepArch);
mysolver.internalPointBuildView("sig_xx",IPField.SIG_XX, 1, nstepArch);
mysolver.internalPointBuildView("sig_yy",IPField.SIG_YY, 1, nstepArch);
mysolver.internalPointBuildView("sig_zz",IPField.SIG_ZZ, 1, nstepArch);
mysolver.internalPointBuildView("sig_xy",IPField.SIG_XY, 1, nstepArch);
mysolver.internalPointBuildView("sig_yz",IPField.SIG_YZ, 1, nstepArch);
mysolver.internalPointBuildView("sig_xz",IPField.SIG_XZ, 1, nstepArch);
#mysolver.internalPointBuildView("sig_VM",IPField.SVM, 1, 1);
#mysolver.internalPointBuildView("Green-Lagrange equivalent strain",IPField.GL_EQUIVALENT_STRAIN, 1, 1);
#mysolver.internalPointBuildView("Equivalent plastic strain",IPField.PLASTICSTRAIN, 1, 1);

#mysolver.internalPointBuildView("Deformation energy",IPField.DEFO_ENERGY, 1, 1);
#mysolver.internalPointBuildView("Plastic energy",IPField.PLASTIC_ENERGY, 1, 1);

mysolver.archivingForceOnPhysicalGroup("Edge",2,1)
mysolver.archivingForceOnPhysicalGroup("Edge",2,0)
mysolver.archivingNodeDisplacement(6,1)

# solve
mysolver.solve()

check = TestCheck()
check.equal(34.22952279032552,mysolver.getArchivedForceOnPhysicalGroup("Edge", 2, 1),1.e-4)




