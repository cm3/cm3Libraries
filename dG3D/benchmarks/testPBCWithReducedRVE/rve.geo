mm = 1.;
H = 1.*mm;
L = 1*mm;
R = 0.25*L;

lsca1 = 0.1*R;
lsca2 = 3*lsca1;
Point(1) = {0,0,0,lsca1};
Point(2) = {R,0,0,lsca1};
Point(3) = {L,0,0,lsca2};
Point(4) = {L,H,0,lsca2};
Point(5) = {0,H,0,lsca2};
Point(6) = {0,R,0,lsca1};
//+
Line(1) = {2, 3};
//+
Line(2) = {3, 4};
//+
Line(3) = {4, 5};
//+
Line(4) = {5, 6};
//+
Circle(5) = {2, 1, 6};
//+
Curve Loop(1) = {4, -5, 1, 2, 3};
//+
Plane Surface(1) = {1};
//+
Symmetry {1, 0, 0, 0} {
  Duplicata { Surface{1}; }
}
//+
Physical Surface("plate") = {6, 1};
//+
Physical Curve("left") = {10};
//+
Physical Curve("right") = {2};
//+
Physical Curve("topleft") = {11};
//+
Physical Curve("topright") = {3};
//+
Physical Curve("downright") = {1};
//+
Physical Curve("downleft") = {9};
//+
Physical Point("A") = {17};
//+
Physical Point("B") = {3};
//+
Physical Point("C") = {4};
//+
Physical Point("D") = {21};
//+
Physical Point("E") = {2};
//+
Physical Point("F") = {13};
//+
Physical Point("G") = {5};
