#coding-Utf-8-*-

from gmshpy import *
from dG3Dpy import*

#script to launch PBC problem with a python script

# material law
E = 3.2E3
nu = 0.3
K = E/3./(1.-2.*nu)	# Bulk mudulus
mu =E/2./(1.+nu)	  # Shear mudulus
rho = 2.7e-9 # Bulk mass

# creation of material law

law1 = dG3DLinearElasticMaterialLaw(11,rho,E,nu)

# geometry
meshfile="rveFull.msh" # name of mesh file

# creation of part Domain
myfield1 = dG3DDomain(11,11,0,11,0,2)


# solver
sol = 2  # Gmm=0 (default) Taucs=1 PETsc=2
soltype =1 # StaticLinear=0 (default) StaticNonLinear=1
nstep = 10  # number of step (used only if soltype=1)
ftime =1.   # Final time (used only if soltype=1)
tol=1.e-6  # relative tolerance for NR scheme (used only if soltype=1)
nstepArch=1 # Number of step between 2 archiving (used only if soltype=1)


# creation of Solver
mysolver = nonLinearMechSolver(1000)
mysolver.loadModel(meshfile)
mysolver.addDomain(myfield1)
mysolver.addMaterialLaw(law1)
mysolver.Scheme(soltype)
mysolver.Solver(sol)
mysolver.snlData(nstep,ftime,tol)

# bc 1
rh1 = linearCombinationOfVertices()
rh1.setVertex(12,1.)
rh1.setVertex(13,-1.)
vop1 = vertexGroupOperation_periodic(1.,0.,0.)
mysolver.linearConstraintBCBetweenTwoGroups('Edge',2,4,0,rh1,vop1,1.)
mysolver.linearConstraintBCBetweenTwoGroups('Edge',2,4,1,rh1,vop1,1.)

rh2 = linearCombinationOfVertices()
rh2.setVertex(14,1.)
rh2.setVertex(15,-1.)
vop2 = vertexGroupOperation_periodic(0.,1.,0.)
mysolver.linearConstraintBCBetweenTwoGroups('Edge',3,1,0,rh2,vop2,1.)
mysolver.linearConstraintBCBetweenTwoGroups('Edge',3,1,1,rh2,vop2,1.)

mysolver.displacementBC("Face",11,2,0.)
mysolver.displacementBC("Node",15,1,0.)
mysolver.displacementBC("Node",13,0,0)
mysolver.displacementBC("Node",12,1,0)

#mysolver.displacementBC("Node",14,0,0.5)
#mysolver.displacementBC("Node",15,0,0)
mysolver.forceBC('Edge',3,0,1e2)
#mysolver.forceBC('Edge',1,0,-1e2)

mysolver.archivingAverageValue(IPField.P_XX)
mysolver.archivingAverageValue(IPField.P_YY)
mysolver.archivingAverageValue(IPField.P_XY)
mysolver.archivingAverageValue(IPField.P_YX)
mysolver.archivingAverageValue(IPField.P_ZZ)


# build view
mysolver.internalPointBuildView("Green-Lagrange_xx",IPField.STRAIN_XX, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_yy",IPField.STRAIN_YY, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_zz",IPField.STRAIN_ZZ, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_xy",IPField.STRAIN_XY, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_yz",IPField.STRAIN_YZ, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_xz",IPField.STRAIN_XZ, 1, 1);
mysolver.internalPointBuildView("sig_xx",IPField.SIG_XX, 1, 1);
mysolver.internalPointBuildView("sig_yy",IPField.SIG_YY, 1, 1);
mysolver.internalPointBuildView("sig_zz",IPField.SIG_ZZ, 1, 1);
mysolver.internalPointBuildView("sig_xy",IPField.SIG_XY, 1, 1);
mysolver.internalPointBuildView("sig_yz",IPField.SIG_YZ, 1, 1);
mysolver.internalPointBuildView("sig_xz",IPField.SIG_XZ, 1, 1);
mysolver.internalPointBuildView("sig_VM",IPField.SVM, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange equivalent strain",IPField.GL_EQUIVALENT_STRAIN, 1, 1);
mysolver.internalPointBuildView("Equivalent plastic strain",IPField.PLASTICSTRAIN, 1, 1);

# solve
mysolver.solve()

# test check
check = TestCheck()
import linecache
homoStress = linecache.getline('Average_P_XY.csv',2)
val = float(homoStress.split(';')[1])
check.equal(2.871301e+02,val,1.e-4)

