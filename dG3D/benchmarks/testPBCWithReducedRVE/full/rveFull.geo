mm = 1.;
H = 1.*mm;
L = 1*mm;
R = 0.25*L;

lsca1 = 0.2*R;
lsca2 = 2*lsca1;
Point(1) = {0,0,0,lsca1};
Point(2) = {R,0,0,lsca1};
Point(3) = {L,0,0,lsca2};
Point(4) = {L,H,0,lsca2};
Point(5) = {0,H,0,lsca2};
Point(6) = {0,R,0,lsca1};
//+
Line(1) = {2, 3};
//+
Line(2) = {3, 4};
//+
Line(3) = {4, 5};
//+
Line(4) = {5, 6};
//+
Circle(5) = {2, 1, 6};
//+
Curve Loop(1) = {4, -5, 1, 2, 3};
//+
Plane Surface(1) = {1};
//+
Symmetry {1, 0, 0, 0} {
  Duplicata { Surface{1}; }
}

//+
Symmetry {0, 1, 0, 0} {
  Duplicata { Surface{6}; Surface{1}; }
}
//+
Physical Surface(11) = {6, 1, 18, 12};
//+
Physical Curve(1) = {17, 23};
//+
Physical Curve(2) = {22, 2};
//+
Physical Curve(3) = {3, 11};
//+
Physical Curve(4) = {10, 16};
//+
Physical Point(1) = {36};
//+
Physical Point(2) = {57};
//+
Physical Point(3) = {4};
//+
Physical Point(4) = {21};
//+
Physical Point(12) = {3};
//+
Physical Point(13) = {17};
//+
Physical Point(14) = {5};
//+
Physical Point(15) = {22};
