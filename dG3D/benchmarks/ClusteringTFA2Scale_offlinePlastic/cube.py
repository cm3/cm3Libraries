from gmshpy import *
from dG3Dpy import*


################################################################################ OFFLINE ##########################################################################

# material law
lawnum1 = 11 # unique number of law
muEP = 3.0e3
kEP = 10.0e3
E = 9.*kEP*muEP/(3.*kEP+muEP) #MPa
nu = E/(2.*muEP)-1.
rho = 7850e-9 # Bulk mass
law1   = J2SmallStrainDG3DMaterialLaw(lawnum1,rho,E,nu,1e10,1e3) #dG3DLinearElasticMaterialLaw(lawnum1,rho,E,nu)

lawnum2 = 12 # unique number of law
muCF = 6.0e3
kCF = 20.0e3
E2 = 9.*kCF*muCF/(3.*kCF+muCF)
nu2 = E2/(2.*muCF)-1.
law2   = J2SmallStrainDG3DMaterialLaw(lawnum2,rho,E2,nu2,1e10,1e3) #dG3DLinearElasticMaterialLaw(lawnum2,rho,E2,nu2)

# geometry
meshfile="RVE_centerInc.msh" # name of mesh file

myfield1 = dG3DDomain(1000,51,0,lawnum1,0,2)
myfield2 = dG3DDomain(1000,52,0,lawnum2,0,2)
myfield1.stabilityParameters(1000)

# solver
sol = 2  # Gmm=0 (default) Taucs=1 PETsc=2
soltype =1 # StaticLinear=0 (default) StaticNonLinear=1
nstep = 1  # number of step (used only if soltype=1)
ftime =1  # Final time (used only if soltype=1)
tol=1.e-8  # relative tolerance for NR scheme (used only if soltype=1)
nstepArch=1 # Number of step between 2 archiving (used only if soltype=1)



# creation of Solver
mysolver = nonLinearMechSolver(1000)
mysolver.loadModel(meshfile)
mysolver.addDomain(myfield1)
mysolver.addMaterialLaw(law1)
mysolver.addDomain(myfield2)
mysolver.addMaterialLaw(law2)
mysolver.Scheme(soltype)
mysolver.Solver(sol)
mysolver.snlData(nstep,ftime,tol)
mysolver.stepBetweenArchiving(nstepArch)

# micro solver without reestimating stiffness matrix
system =3
mysolver.setSystemType(system)
mysolver.stiffnessModification(False)
mysolver.setMessageView(True)


microBC = nonLinearPeriodicBC(1000,2)
microBC.setOrder(1)
microBC.setBCPhysical(100,110,101,111)
method =5	# Periodic mesh = 0, Langrange interpolation = 1, Cubic spline interpolation =2,  FE linear= 3, FE Quad = 4, projection = 5
degree = 6	# Order used for polynomial interpolation 
addvertex = False # Polynomial interpolation by mesh vertex = 0, Polynomial interpolation by virtual vertex 
microBC.setPeriodicBCOptions(method, degree,False) 

mysolver.addMicroBC(microBC)
mysolver.stressAveragingFlag(False) # set stress averaging ON- 0 , OFF-1
mysolver.tangentAveragingFlag(False) # set tangent averaging ON -0, OFF -1

cl = Clustering()
cl.solveStrainConcentration(mysolver)


#--------------------------PLASTICITY----------------------------------

# materials
lawlinear1plast = lawnum1
sY0 = 10.
H = 50.
m = 0.05
Hardening = PowerLawJ2IsotropicHardening(41, sY0, H, m)
law1plast = J2SmallStrainDG3DMaterialLaw(lawlinear1plast,rho,E,nu,Hardening)

lawlinear2plast = lawnum2
law2plast   = J2SmallStrainDG3DMaterialLaw(lawlinear2plast,rho,E2,nu2,1.e10,1e2) #dG3DLinearElasticMaterialLaw(lawlinear2plast,rho,E2,nu2)

myfield1plast = dG3DDomain(1000,51,0,lawlinear1plast,0,2)
myfield2plast = dG3DDomain(1000,52,0,lawlinear2plast,0,2)
myfield1plast.stabilityParameters(1000)

# creation of Solver
nstep_plast = 50
tol_plast = 1.e-6
nstepArch_plast = 10
mysolverPlast = nonLinearMechSolver(1002)
mysolverPlast.loadModel(meshfile)
mysolverPlast.addDomain(myfield1plast)
mysolverPlast.addMaterialLaw(law1plast)
mysolverPlast.addDomain(myfield2plast)
mysolverPlast.addMaterialLaw(law2plast)
mysolverPlast.Scheme(soltype)
mysolverPlast.Solver(sol)
mysolverPlast.snlData(nstep_plast,ftime,tol_plast)
mysolverPlast.stepBetweenArchiving(nstepArch_plast)

control = 0
mysolverPlast.setControlType(control)
mysolverPlast.stiffnessModification(bool(1))
mysolverPlast.iterativeProcedure(bool(1))
mysolverPlast.setMessageView(bool(1))

# micro solver without reestimating stiffness matrix
system3 =1
mysolverPlast.setSystemType(system3)

microBC3 = nonLinearPeriodicBC(1002,2)
microBC3.setOrder(1)
microBC3.setBCPhysical(100,110,101,111)
method3 =5	# Periodic mesh = 0, Langrange interpolation = 1, Cubic spline interpolation =2,  FE linear= 3, FE Quad = 4, projection = 5
degree3 = 5	# Order used for polynomial interpolation 
addvertex3 = False # Polynomial interpolation by mesh vertex = 0, Polynomial interpolation by virtual vertex 

microBC3.setPeriodicBCOptions(method3, degree3,False) 

mysolverPlast.addMicroBC(microBC3)
mysolverPlast.stressAveragingFlag(True) # set stress averaging ON- 0 , OFF-1
#mysolverPlast.tangentAveragingFlag(False) # set tangent averaging ON -0, OFF -1
mysolverPlast.tangentAveragingFlag(True)
mysolverPlast.setTangentAveragingMethod(2,1e-6)

mysolverPlast.internalPointBuildView("sig_VM",IPField.SVM, 1, 1);
mysolverPlast.internalPointBuildView("Equivalent plastic strain",IPField.PLASTICSTRAIN, 1, 1);

cl.solveStrainConcentrationInelastic(mysolverPlast)


#--------------------------Clustering----------------------------------

cl.loadStrainConcentrationDataFromFile("strainConcentrationData.csv")
cl.loadPlasticStrainConcentrationDataFromFile("plasticStrainConcentrationData.csv")
cl.loadPlasticEqStrainConcentrationDataFromFile("plasticEqstrainConcentrationData.csv")

nbLevels = 2
cl.setNumberOfLevels(nbLevels)
cl.setNumberOfClustersForMaterialHierarchical(lawnum1,1,4)
cl.setNumberOfClustersForMaterialHierarchical(lawnum2,1,1)

ok = cl.computeClustersPlasticityHierarchical(5) # min number of element in each cluster
cl.saveAllView()
cl.saveAllView(0)
cl.saveAllView(1)
cl.saveAllView(2)
cl.saveFieldToView(Clustering.ClusterIndex)
cl.saveFieldToView(Clustering.ClusterIndex,0)
cl.saveFieldToView(Clustering.ClusterIndex,1)
cl.saveFieldToView(Clustering.ClusterIndex,2)
cl.saveFieldToView(Clustering.ClusterIndexHierarchicalGlobal)

#check = TestCheck()
#check.equal(1,ok,1.e-6)


#--------------------------Interaction----------------------------------
law3num = 3
clusterLaw = ClusterDG3DMaterialLaw(law3num,cl)
cl.computeInteractionTensorsHierarchical(mysolver,clusterLaw)


################################################################################ ONLINE ##########################################################################
# material law
lawTFAH =  3
TFAmethod = 0     # 0: Incremental Tangent TFA  2: Incremental Tangent HS             


###material parameters
# material law
lawEP = 11     # unique number of law
rhoEP   = 1000.e-9
muEP = 3.0e3
kEP = 10.0e3
youngEP = 9.*kEP*muEP/(3.*kEP+muEP) #MPa
nuEP = youngEP/(2.*muEP)-1.
sy0EP   = 100.0
H_p     = 1.
m = 1.
harden = PowerLawJ2IsotropicHardening(1,  sy0EP, H_p, m)

lawCF = 12      # unique number of law
rhoCF   = 1780.e-9
muCF = 6.0e3
kCF = 20.0e3
youngCF = 9.*kCF*muCF/(3.*kCF+muCF)
nuCF = youngCF/(2.*muCF)-1.
sy0EP2   = 1000000.0
H_p2    = 100.
m2 = 1.
harden2 = PowerLawJ2IsotropicHardening(2,  sy0EP2, H_p2, m2)


# geometry
geofile="cube.geo"
meshfile="cube.msh" # name of mesh file
dim=2

# solver
sol = 2  # Gmm=0 (default) Taucs=1 PETsc=2
soltype = 1 # StaticLinear=0 (default) StaticNonLinear=1
nstep = 50   # number of step (used only if soltype=1)
ftime =1.   # Final time (used only if soltype=1)
tol=1.e-6   # relative tolerance for NR scheme (used only if soltype=1)
nstepArch=5 # Number of step between 2 archiving (used only if soltype=1)
fullDg = 0 # = CG, 1 = DG
space1 = 0 # function space (Lagrange=0)


law1 = J2SmallStrainDG3DMaterialLaw(lawEP,rhoEP,youngEP,nuEP,harden)
law2 = J2SmallStrainDG3DMaterialLaw(lawCF,rhoCF,youngCF,nuCF,harden2)

lawnumTFA = 100
lawTFA = TFADG3DMaterialLaw(lawnumTFA, rhoEP)

# material law
lawRed = HierarchicalTFADG3DMaterialLaw(lawTFAH, rhoEP)
lawRed.setTFAMethod(TFAmethod,dim)   ## TFA method, dimension, correction: 0 without correction (default) or 1 with correction
lawRed.setTFALawID(lawnumTFA)
  
lawRed.loadClusterSummaryAndInteractionTensorsFromFiles('ClusterDataSummaryHierarchical_nbClusters_2.csv','InteractionTensorDataHierarchical_nbClusters_2.csv')
lawRed.loadClusterSummaryAndInteractionTensorsLevel1FromFiles('ClusterDataSummaryHierarchical_level1_nbClusters_2.csv','InteractionTensorDataHierarchical_level1_nbClusters_2.csv')

  
# creation of ElasticField
nfield = 1234 # number of the field (physical number of surface)

#myfield = dG3DDomain(1000,nfield,space1,lawTFA,fullDg,3,2)
myfield = dG3DDomain(1000,nfield,space1,lawTFAH,fullDg,2)
myfield.stabilityParameters(30.)
#myfield.matrixByPerturbation(1,1,1,1e-8)

# creation of Solver
mysolver = nonLinearMechSolver(1000)
mysolver.createModel(geofile,meshfile,2,2)
#mysolver.loadModel(meshfile)
mysolver.addDomain(myfield)
mysolver.addMaterialLaw(law1)
mysolver.addMaterialLaw(law2)
mysolver.addMaterialLaw(lawTFA)
mysolver.addMaterialLaw(lawRed)

mysolver.Scheme(soltype)
mysolver.Solver(sol)
mysolver.snlData(nstep,ftime,tol)
mysolver.stepBetweenArchiving(nstepArch)

# BC
##tension along x
mysolver.displacementBC("Face",nfield,2,0.)

mysolver.displacementBC("Edge",41,0,0.)
a=1.
eps_xx_max=0.04
d1=eps_xx_max*a
cyclicFunction1=cycleFunctionTime(0.,0.,ftime/4., d1/2., ftime/2., d1, 3.*ftime/4., d1/2., ftime, 0.)
mysolver.displacementBC("Edge",23,0,cyclicFunction1)
mysolver.displacementBC("Edge",12,1,0.)




mysolver.archivingAverageValue(IPField.SIG_XX)
mysolver.archivingAverageValue(IPField.SIG_YY)
mysolver.archivingAverageValue(IPField.SIG_ZZ)
mysolver.archivingAverageValue(IPField.SIG_XY)
mysolver.archivingAverageValue(IPField.SIG_YZ)
mysolver.archivingAverageValue(IPField.SIG_XZ)
mysolver.archivingAverageValue(IPField.STRAIN_XX)
mysolver.archivingAverageValue(IPField.STRAIN_YY)
mysolver.archivingAverageValue(IPField.STRAIN_ZZ)
mysolver.archivingAverageValue(IPField.STRAIN_XY)
mysolver.archivingAverageValue(IPField.STRAIN_YZ)
mysolver.archivingAverageValue(IPField.STRAIN_XZ)
mysolver.archivingAverageValue(IPField.PLASTICSTRAIN)


mysolver.solve()

check = TestCheck()
import csv
data = csv.reader(open('E_1_GP_0_stress.csv'), delimiter=';')
strs = list(data)
check.equal(3.133567e+01,float(strs[-1][1]),1e-6)




