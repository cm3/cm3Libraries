#coding-Utf-8-*-
import pandas as pd
import numpy as np
import sys
import pickle
from gmshpy import *
from dG3Dpy import*
#from dG3DpyDebug import*


lnum1= 11
rho = 1e-9

with open('./Bounds.dat','rb') as data:  
    XGmax = pickle.load(data, encoding="latin1") 
    YSmax = pickle.load(data, encoding="latin1")  
    XGmin = pickle.load(data, encoding="latin1") 
    YSmin = pickle.load(data, encoding="latin1")   
      
EXXmean = (XGmax[0]+XGmin[0])/2.0
EXXstd = (XGmax[0]-XGmin[0])/2.0
EXYmean = (XGmax[1]+XGmin[1])/2.0
EXYstd = (XGmax[1]-XGmin[1])/2.0
EYYmean = (XGmax[2]+XGmin[2])/2.0
EYYstd = (XGmax[2]-XGmin[2])/2.0
EYZmean = 0.0
EYZstd = 1.0
EZZmean = 0.0
EZZstd = 1.0
EZXmean = 0.0
EZXstd = 1.0

SXXmean = (YSmax[0]+YSmin[0])/2.0
SXXstd = (YSmax[0]-YSmin[0])/2.0
SXYmean = (YSmax[1]+YSmin[1])/2.0
SXYstd = (YSmax[1]-YSmin[1])/2.0
SYYmean = (YSmax[2]+YSmin[2])/2.0
SYYstd = (YSmax[2]-YSmin[2])/2.0
SYZmean = 0.0
SYZstd = 1.0
SZZmean = (YSmax[3]+YSmin[3])/2.0
SZZstd = (YSmax[3]-YSmin[3])/2.0
SZXmean = 0.0
SZXstd = 1.0


numberOfInput = 3
numInternalVars = 100

macromat1 = torchANNBasedDG3DMaterialLaw(lnum1,rho, numberOfInput, numInternalVars, "./model.pt", EXXmean, EXXstd, EXYmean, EXYstd, EYYmean, EYYstd, EYZmean, EYZstd, EZZmean, EZZstd, EZXmean, EZXstd, SXXmean, SXXstd, SXYmean, SXYstd, SYYmean, SYYstd, SYZmean, SYZstd, SZZmean,  SZZstd, SZXmean, SZXstd,False,1e-4)
#pertmacromat1 = dG3DMaterialLawWithTangentByPerturbation(macromat1,1e-4)

macromeshfile="squares.msh" # name of mesh file
macrogeofile="squares.geo"
# solver
sol = 2  # Gmm=0 (default) Taucs=1 PETsc=2
soltype =1 # StaticLinear=0 (default) StaticNonLinear=1
nstep = 50  # number of step (used only if soltype=1)
ftime =1.   # Final time (used only if soltype=1)
tol=1.e-4  # relative tolerance for NR scheme (used only if soltype=1)
nstepArch=1 # Number of step between 2 archiving (used only if soltype=1)


# creation of  macro part Domain

beta1 = 50;
fullDG = False;

averageStrainBased = True

# non DG domain
nfield1 = 1265
macrodomain1 = dG3DDomain(10,nfield1,0,lnum1,fullDG,2)
macrodomain1.stabilityParameters(beta1)
#macrodomain1.matrixByPerturbation(1,1,1,1e-8)

# creation of Solver
mysolver = nonLinearMechSolver(1000)
mysolver.createModel(macrogeofile,macromeshfile,2,2)
mysolver.addDomain(macrodomain1)
mysolver.addMaterialLaw(macromat1)
#mysolver.addMaterialLaw(pertmacromat1)
mysolver.Scheme(soltype)
mysolver.Solver(sol)
mysolver.snlData(nstep,ftime,tol,tol/100.)

# boundary condition
mysolver.displacementBC("Face",nfield1,2,0.0)
mysolver.displacementBC("Edge",12,1,0.0)
mysolver.displacementBC("Edge",15,0,0.0)
d1=0.025e-3
cyclicFunction1=cycleFunctionTime(0.,0.,ftime/2., d1, ftime, d1/2.);
mysolver.displacementBC("Edge",56,1,cyclicFunction1)
#mysolver.forceBC("Edge",3,1,1e2)

# archivage
mysolver.internalPointBuildView("Green-Lagrange_xx",IPField.GL_XX, 1, nstepArch);
mysolver.internalPointBuildView("Green-Lagrange_yy",IPField.GL_YY, 1, nstepArch);
mysolver.internalPointBuildView("Green-Lagrange_zz",IPField.GL_ZZ, 1, nstepArch);
mysolver.internalPointBuildView("Green-Lagrange_xy",IPField.GL_XY, 1, nstepArch);
mysolver.internalPointBuildView("Green-Lagrange_yz",IPField.GL_YZ, 1, nstepArch);
mysolver.internalPointBuildView("Green-Lagrange_xz",IPField.GL_XZ, 1, nstepArch);
mysolver.internalPointBuildView("sig_xx",IPField.SIG_XX, 1, nstepArch);
mysolver.internalPointBuildView("sig_yy",IPField.SIG_YY, 1, nstepArch);
mysolver.internalPointBuildView("sig_zz",IPField.SIG_ZZ, 1, nstepArch);
mysolver.internalPointBuildView("sig_xy",IPField.SIG_XY, 1, nstepArch);
mysolver.internalPointBuildView("sig_yz",IPField.SIG_YZ, 1, nstepArch);
mysolver.internalPointBuildView("sig_xz",IPField.SIG_XZ, 1, nstepArch);
#mysolver.internalPointBuildView("sig_VM",IPField.SVM, 1, 1);
#mysolver.internalPointBuildView("Green-Lagrange equivalent strain",IPField.GL_EQUIVALENT_STRAIN, 1, 1);
#mysolver.internalPointBuildView("Equivalent plastic strain",IPField.PLASTICSTRAIN, 1, 1);



mysolver.archivingForceOnPhysicalGroup("Edge",12,1)
mysolver.archivingForceOnPhysicalGroup("Edge",15,0)

mysolver.archivingIPOnPhysicalGroup("Face",nfield1, IPField.SIG_YY,IPField.MEAN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Face",nfield1, IPField.SIG_XY,IPField.MEAN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Face",nfield1, IPField.SIG_XX,IPField.MEAN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Face",nfield1, IPField.GL_YY,IPField.MEAN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Face",nfield1, IPField.GL_XY,IPField.MEAN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Face",nfield1, IPField.GL_XX,IPField.MEAN_VALUE, nstepArch);
# solve
mysolver.solve()

check = TestCheck()
check.equal(6.181404e-03,mysolver.getArchivedForceOnPhysicalGroup("Edge", 12, 1),1.e-4)
check.equal(-3.129223e-09,mysolver.getArchivedForceOnPhysicalGroup("Edge", 15, 0),1.e-2)
