#coding-Utf-8-*-
from gmshpy import *


#from dG3DpyDebug import*
from dG3Dpy import*

#script to launch Coupled SMP problem with a python script

import math
import pickle

# Electro-Magnetic problem in full domain

# material law: SMP
lawnum= 1 
rho   = 270. # material density (Kg/m^3)
tinitial = 273.+47. # (K)
G=299.e6 # Shear modulus (Pa)
Mu=0.29 # Poisson ratio
alpha = beta=gamma=0. # parameter of anisotropy
cp=10.*rho # specific heat capacity 
Kx=Ky=Kz=237. # Thermal conductivity (W/m K)
lx=ly=lz=1.0e4 # electrical conductivity (S/m) 
seebeck=0.0 #21.e-6
alphaTherm=0.0 #13.e-5 # thermal dilatation
v0=0. # initial electric potential
E_matrix = 771.e6 # (Pa)
mag_mu_0 = 4.e-7 * math.pi # vacuum magnetic permeability
mag_r_smp = 20.0 # relative mag permeability of SMP
mag_mu_x = mag_mu_y = mag_mu_z = mag_r_smp * mag_mu_0 # mag permeability smp 
magpot0_x = magpot0_y = magpot0_z = 0.0 # initial magnetic potential
Irms = 325. # (Ampere)
freq = 1000. # (Hz)
nTurnsCoil = 1000
coilLx = coilLy = coilLz = 1. # m
coilW = 3.e-2 # m

# material law: inductor (copper coil)
# Material parameters for copper
lawnumind = 2
rhoind = 8960. 
Eind=120.e9 #youngs modulus
Gind=48.e9 # Shear modulus
Muind = 0.34 # Poisson ratio
alphaind = betaind = gammaind = 0. # parameter of anisotropy
cpind= 385.*rhoind
Kxind=Kyind=Kzind= 401. # thermal conductivity tensor components
lxind=lyind=lzind=5.77e7 # electrical conductivity (S/m)
seebeckind=0.0 #6.5e-6
alphaThermind= 0.0 # thermal dilatation
v0ind = 0.
mag_r_ind = 1.0 # relative mag permeability of inductor
mag_mu_x_ind = mag_mu_y_ind = mag_mu_z_ind = mag_r_ind * mag_mu_0 # mag permeability inductor 
magpot0_x_ind = magpot0_y_ind = magpot0_z_ind = 0.0 # initial magnetic potential
Centroid_x = Centroid_y = Centroid_z = 0.0 # Centroid of inductor coil
centralAxis_x = 0.
centralAxis_y = 1.
centralAxis_z = 0. # Unit vector along central axis of inductor coil

# material law: vacuum/free space region
lawnumvac = 3
rhovac = 1.2 
Gvac=156.e3 # Shear modulus (1.e-5 less than SMP)
Muvac = 0.45 # Poisson ratio (same as SMP)
Evac= 2.0 * Gvac * (1.0 + Muvac) #youngs modulus
alphavac = betavac = gammavac = 0. # parameter of anisotropy
cpvac= 0.0 #1012.*rhovac
Kxvac=Kyvac=Kzvac= 0.0 #26.e-3 # thermal conductivity tensor components
lxvac=lyvac=lzvac= 1.e-12 # electrical conductivity
seebeckvac= 0.
alphaThermvac= 0.0 # thermal dilatation
v0vac = 0.
mag_r_vac = 1.0 # relative mag permeability of inductor
mag_mu_x_vac = mag_mu_y_vac = mag_mu_z_vac = mag_r_vac * mag_mu_0 # mag permeability inductor 
magpot0_x_vac = magpot0_y_vac = magpot0_z_vac = 0.0 # initial magnetic potential

useFluxT=True;
evaluateCurlField = True;
evaluateTemperature = False;
evaluateMecaField = False;

# geometry
with open("EMdatafile.txt", "rb") as data:
  time = pickle.load(data)
  nbCoupling = pickle.load(data)
  EMmeshfile = pickle.load(data)
  EMnstep = pickle.load(data)
  EMncycles = pickle.load(data)

print('time: ', time);
print('Nb coupling: ', nbCoupling);
print('EM solver mesh file: ', EMmeshfile);
print('EM Num step: ',EMnstep);
print('EM Num cycles: ',EMncycles);

# solver
sol = 2  # Gmm=0 (default) Taucs=1 PETsc=2
soltype = 1 # StaticLinear=0 (default) StaticNonLinear=1

EMftime =EMncycles/freq # Final time (used only if soltype=1)

tol=1.e-9   # relative tolerance for NR scheme (used only if soltype=1)
nstepArch=1 # Number of step between 2 archiving (used only if soltype=1)
fullDg = 0 #O = CG, 1 = DG
beta1 = 1000.
eqRatio =1.e0

#  compute solution and BC (given directly to the solver
# creation of law

# material law for smp
lawsmp = LinearElecMagTherMechDG3DMaterialLaw(lawnum,rho,E_matrix,E_matrix,E_matrix,Mu,Mu,Mu,G,G,G,alpha,beta,gamma,
tinitial,Kx,Ky,Kz,alphaTherm,alphaTherm,alphaTherm,lx,ly,lz,seebeck,cp,v0, mag_mu_x, mag_mu_y, mag_mu_z, magpot0_x,
magpot0_y, magpot0_z, Irms, freq, nTurnsCoil, coilLx, coilLy, coilLz, coilW,useFluxT,evaluateCurlField)

#lawsmp.setUseBarF(True)

# material law for inductor region
lawind = LinearElecMagInductorDG3DMaterialLaw(lawnumind,rhoind,Eind,Eind,Eind,Muind,Muind,Muind,Gind,Gind,Gind,alphaind,betaind,gammaind,
tinitial,Kxind,Kyind,Kzind,alphaThermind,alphaThermind,alphaThermind,lxind,lyind,lzind,seebeckind,cpind,v0ind, mag_mu_x_ind, mag_mu_y_ind, mag_mu_z_ind, 
magpot0_x_ind,magpot0_y_ind, magpot0_z_ind, Irms, freq, nTurnsCoil, coilLx, coilLy, coilLz, coilW, 
Centroid_x,Centroid_y,Centroid_z,centralAxis_x,centralAxis_y,centralAxis_z,useFluxT,evaluateCurlField)

#lawind.setUseBarF(True)

#material law for free space (vacuum)
lawvac = LinearElecMagTherMechDG3DMaterialLaw(lawnumvac,rhovac,Evac,Evac,Evac,Muvac,Muvac,Muvac,Gvac,Gvac,Gvac,alphavac,betavac,gammavac,tinitial,
Kxvac,Kyvac,Kzvac,alphaThermvac,alphaThermvac,alphaThermvac,lxvac,lyvac,lzvac,seebeckvac,cpvac,v0vac, mag_mu_x_vac, mag_mu_y_vac, mag_mu_z_vac, magpot0_x_vac,
magpot0_y_vac, magpot0_z_vac, Irms, freq, nTurnsCoil, coilLx, coilLy, coilLz, coilW,useFluxT,evaluateCurlField)

#lawvac.setUseBarF(True)

# creation of ElasticField
SMPfield = 1000 # number of the field (physical number of SMP)
SurfSmp = 11110 # outer surface of conductor SMP
SmpRef=11111 # top face of outer surface of SMP
SmpRefBottom=11115 # bottom face of outer surface of SMP
SmpRefPoint=11112 # single point on top face of SMP
Indfield = 2000 # number of the field (physical number of Inductor region)
Vacfield = 3000 # number of the field (physical number of Vacuum)
SurfVacOut = 3333 # number of the field (outer surface of Vacuum) Used for BC
SurfInd = 2222 # outer surface of inductor region

space1 = 0 # function space (Lagrange=0)

SMP_field = ElecMagTherMechDG3DDomain(10,SMPfield,space1,lawnum,fullDg,eqRatio,3)
Inductor_field = ElecMagTherMechDG3DDomain(10,Indfield,space1,lawnumind,fullDg,eqRatio,3)
Vacuum_field = ElecMagTherMechDG3DDomain(10,Vacfield,space1,lawnumvac,fullDg,eqRatio,3)

SMP_field.setConstitutiveExtraDofDiffusionEqRatio(eqRatio)
Inductor_field.setConstitutiveExtraDofDiffusionEqRatio(eqRatio)
Vacuum_field.setConstitutiveExtraDofDiffusionEqRatio(eqRatio)
SMP_field.setConstitutiveCurlEqRatio(eqRatio)
Inductor_field.setConstitutiveCurlEqRatio(eqRatio)
Vacuum_field.setConstitutiveCurlEqRatio(eqRatio)
SMP_field.stabilityParameters(beta1)
Inductor_field.stabilityParameters(beta1)
Vacuum_field.stabilityParameters(beta1)

# flag to fix the reinitialization and 
# reevaluation of field after restart
# args: boolean flag and field index number
SMP_field.setEvaluateExtraDofDiffusionField(evaluateTemperature,0) # fix temperature and don't compute
SMP_field.setEvaluateMecaField(evaluateMecaField) # fix displacements and don't compute
Inductor_field.setEvaluateExtraDofDiffusionField(evaluateTemperature,0)
Inductor_field.setEvaluateMecaField(evaluateMecaField)
Vacuum_field.setEvaluateExtraDofDiffusionField(evaluateTemperature,0)
Vacuum_field.setEvaluateMecaField(evaluateMecaField)


thermalSource=True
mecaSource=False

SMP_field.setConstitutiveExtraDofDiffusionAccountSource(thermalSource,mecaSource)
Vacuum_field.setConstitutiveExtraDofDiffusionAccountSource(thermalSource,mecaSource)
Inductor_field.setConstitutiveExtraDofDiffusionAccountSource(thermalSource,mecaSource)

#SMP_field.matrixByPerturbation(1,1,1,1.e-3)
#Inductor_field.matrixByPerturbation(1,1,1,1.e-3)
#Vacuum_field.matrixByPerturbation(1,1,1,1.e-3)

# creation of Solver
mysolver = nonLinearMechSolver(1000)
mysolver.loadModel(EMmeshfile)
mysolver.addDomain(SMP_field)
mysolver.addDomain(Inductor_field)
mysolver.addDomain(Vacuum_field)
mysolver.addMaterialLaw(lawsmp)
mysolver.addMaterialLaw(lawind)
mysolver.addMaterialLaw(lawvac)
mysolver.Scheme(soltype)
mysolver.Solver(sol)
mysolver.snlData(EMnstep,EMftime,tol,1.e-9)#forsmp
mysolver.options("-ksp_type preonly")
mysolver.options("-pc_type lu")

mysolver.stepBetweenArchiving(nstepArch)
mysolver.snlManageTimeStep(25, 10, 2, 10)

# BC
mysolver.displacementBC("Volume",SMPfield,0,0.0)
mysolver.displacementBC("Volume",SMPfield,1,0.0)
mysolver.displacementBC("Volume",SMPfield,2,0.0)
mysolver.displacementBC("Volume",Indfield,0,0.0)
mysolver.displacementBC("Volume",Indfield,1,0.0)
mysolver.displacementBC("Volume",Indfield,2,0.0)
mysolver.displacementBC("Volume",Vacfield,0,0.0)
mysolver.displacementBC("Volume",Vacfield,1,0.0)
mysolver.displacementBC("Volume",Vacfield,2,0.0)

#thermal BC
mysolver.displacementBC("Volume",SMPfield,3,tinitial)
mysolver.displacementBC("Volume",Indfield,3,tinitial)
mysolver.displacementBC("Volume",Vacfield,3,tinitial)
mysolver.initialBC("Volume","Position",SMPfield,3,tinitial)
mysolver.initialBC("Volume","Position",Indfield,3,tinitial)
mysolver.initialBC("Volume","Position",Vacfield,3,tinitial)

#electrical BC
mysolver.displacementBC("Volume",Indfield,4,0.0)
mysolver.initialBC("Volume","Position",SMPfield,4,0.0)
mysolver.initialBC("Volume","Position",Vacfield,4,0.0)
mysolver.displacementBC("Face",SurfVacOut,4,0.0)
mysolver.displacementBC("Node",SmpRefPoint,4,0.0)

#magentic
mysolver.curlDisplacementBC("Face",SurfVacOut,5,0.0) # comp may also be 5
#Gauging the edges using tree-cotree method
PhysicalCurves = "" 		# input required as a string of comma separated ints
PhysicalSurfaces = "11110,3333" # input required as a string of comma separated ints
PhysicalVolumes = "1000,2000,3000" 	# input required as a string of comma separated ints
OutputPhysical = 55 		# input required as a int
mysolver.createTreeForBC(PhysicalCurves,PhysicalSurfaces,PhysicalVolumes,OutputPhysical)
mysolver.curlDisplacementBC("Edge",OutputPhysical,5,0.0)

mysolver.internalPointBuildView("w_AV",IPField.EMFIELDSOURCE, 1, 1)
mysolver.internalPointBuildView("w_T",IPField.THERMALSOURCE, 1, 1)
mysolver.internalPointBuildView("jyx",IPField.THERMALFLUX_X, 1, 1)
mysolver.internalPointBuildView("jyy",IPField.THERMALFLUX_Y, 1, 1)
mysolver.internalPointBuildView("jyz",IPField.THERMALFLUX_Z, 1, 1)
mysolver.internalPointBuildView("voltage",IPField.VOLTAGE, 1, 1)
mysolver.internalPointBuildView("temperature",IPField.TEMPERATURE, 1, 1)
mysolver.internalPointBuildView("jex",IPField.ELECTRICALFLUX_X, 1, 1)
mysolver.internalPointBuildView("jey",IPField.ELECTRICALFLUX_Y, 1, 1)
mysolver.internalPointBuildView("jez",IPField.ELECTRICALFLUX_Z, 1, 1)
mysolver.internalPointBuildView("ax",IPField.MAGNETICVECTORPOTENTIAL_X, 1, 1)
mysolver.internalPointBuildView("ay",IPField.MAGNETICVECTORPOTENTIAL_Y, 1, 1)
mysolver.internalPointBuildView("az",IPField.MAGNETICVECTORPOTENTIAL_Z, 1, 1)
mysolver.internalPointBuildView("bx",IPField.MAGNETICINDUCTION_X, 1, 1)
mysolver.internalPointBuildView("by",IPField.MAGNETICINDUCTION_Y, 1, 1)
mysolver.internalPointBuildView("bz",IPField.MAGNETICINDUCTION_Z, 1, 1)
mysolver.internalPointBuildView("hx",IPField.MAGNETICFIELD_X, 1, 1)
mysolver.internalPointBuildView("hy",IPField.MAGNETICFIELD_Y, 1, 1)
mysolver.internalPointBuildView("hz",IPField.MAGNETICFIELD_Z, 1, 1)
mysolver.internalPointBuildView("js0_x",IPField.INDUCTORSOURCEVECTORFIELD_X, 1, 1)
mysolver.internalPointBuildView("js0_y",IPField.INDUCTORSOURCEVECTORFIELD_Y, 1, 1)
mysolver.internalPointBuildView("js0_z",IPField.INDUCTORSOURCEVECTORFIELD_Z, 1, 1)
mysolver.internalPointBuildView("eps_xx",IPField.STRAIN_XX, 1, 1)
mysolver.internalPointBuildView("eps_yy",IPField.STRAIN_YY, 1, 1)
mysolver.internalPointBuildView("eps_zz",IPField.STRAIN_ZZ, 1, 1)
mysolver.internalPointBuildView("eps_xy",IPField.STRAIN_XY, 1, 1)
mysolver.internalPointBuildView("eps_yz",IPField.STRAIN_YZ, 1, 1)
mysolver.internalPointBuildView("eps_xz",IPField.STRAIN_XZ, 1, 1)

mysolver.archivingIPOnPhysicalGroup("Volume",SMPfield, IPField.TEMPERATURE,IPField.MEAN_VALUE,nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",SMPfield, IPField.VOLTAGE,IPField.MEAN_VALUE,nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",SMPfield, IPField.EMFIELDSOURCE,IPField.MEAN_VALUE,nstepArch);

mysolver.archivingForceOnPhysicalGroup("Face", SurfSmp, 3, 1);
mysolver.archivingForceOnPhysicalGroup("Face", SurfSmp, 4, 1);
mysolver.archivingForceOnPhysicalGroup("Face", SurfInd, 4, 1);
mysolver.archivingForceOnPhysicalGroup("Face", SurfVacOut, 4, 1);
mysolver.archivingForceOnPhysicalGroup("Face", SurfSmp, 5, 1);
mysolver.archivingForceOnPhysicalGroup("Face", SurfInd, 5, 1);
mysolver.archivingForceOnPhysicalGroup("Face", SurfVacOut, 5, 1);

mysolver.archivingNodeDisplacement(SmpRefPoint,0,1);
mysolver.archivingNodeDisplacement(SmpRefPoint,1,1);
mysolver.archivingNodeDisplacement(SmpRefPoint,2,1);
mysolver.archivingNodeDisplacement(SmpRefPoint,3,1);
mysolver.archivingNodeDisplacement(SmpRefPoint,4,1);

mysolver.archivingIPDataOnPhysical(IPField.EMFIELDSOURCE,SMPfield,3,1,True,-1)

# assumption that EM solver proceeds smoothly without
# reduced time step size due to no convergence
estimatedTimeStepSize = EMftime/EMnstep;
nstepPerCycle = EMnstep/EMncycles;

tend = EMftime;
# considering one complete sinusoidal cycle 
# over which average would be computed
tstart = tend-((nstepPerCycle-1)*estimatedTimeStepSize);

print('nstepPerCycle: ', nstepPerCycle);
print('estimated delta t: ', estimatedTimeStepSize);
print('tstart: ', tstart);
print('tend: ', tend);

# average computed over single last sinusoidal cycle of EM load
# tstart = start time step for last sinusoidal cycle
# tend = end time step for EM solver
mysolver.averagingIPDataOnPhysicalOverTime(tstart,tend,IPField.EMFIELDSOURCE,SMPfield,3,1,True,-1)

mysolver.setWeakTimeScaleCoupling(True);
mysolver.setRestartDomainTag(SMPfield);
#mysolver.createRestartBySteps(EMnstep);
mysolver.solve()

"""
check = TestCheck()
check.equal(0.000000e+00,mysolver.getArchivedForceOnPhysicalGroup("Face", SurfSmp, 3),1.e-5)
check.equal(0.000000e+00,mysolver.getArchivedForceOnPhysicalGroup("Face", SurfSmp, 4),1.e-5)
check.equal(7.214635e-02,mysolver.getArchivedForceOnPhysicalGroup("Face", SurfInd, 4),1.e-5)
check.equal(9.199883e-03,mysolver.getArchivedForceOnPhysicalGroup("Face", SurfVacOut, 4),1.e-5)
check.equal(1.427543e-01,mysolver.getArchivedForceOnPhysicalGroup("Face", SurfSmp, 5),1.e-5)
check.equal(9.688245e-01,mysolver.getArchivedForceOnPhysicalGroup("Face", SurfInd, 5),1.e-5)
check.equal(1.187590e+04,mysolver.getArchivedForceOnPhysicalGroup("Face", SurfVacOut, 5),1.e-5)
check.equal(0.0,mysolver.getArchivedNodalValue(SmpRefPoint,0,mysolver.displacement),1.e-5)
check.equal(0.0,mysolver.getArchivedNodalValue(SmpRefPoint,1,mysolver.displacement),1.e-5)
check.equal(0.0,mysolver.getArchivedNodalValue(SmpRefPoint,2,mysolver.displacement),1.e-5)
check.equal(2.997321e+02,mysolver.getArchivedNodalValue(SmpRefPoint,3,mysolver.displacement),1.e-5)
check.equal(3.548869e+03,mysolver.getArchivedNodalValue(SmpRefPoint,4,mysolver.displacement),1.e-5)
"""


