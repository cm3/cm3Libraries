import os,csv
import sys
import numpy as np
import pickle
import re

def checkEqual(ref, cur, tol):
  if(abs(ref-cur)>abs(tol*ref)):
    print("Error : reference  current value ",ref," current value ",cur," relative error ", (ref-cur)/ref," tolerance ",tol)

ftime = 5.e-3;
nbCoupling = 5;
timeSteps = np.linspace(ftime/nbCoupling,ftime,nbCoupling,endpoint=True)

movedTMRestartDir = 'movedTMRestartFiles'
os.system('mkdir '+movedTMRestartDir)
#restartDirName = 'beforeRestart'

TMresultsDir = 'TMresults'
os.system('mkdir '+TMresultsDir)

EMresultsDir = 'EMresults'
os.system('mkdir '+EMresultsDir)

initialDisp = 0.0;
deltaDisp = 0.004;

#initialization for first time step
finalDisp = 0.004;

# num of sinusoidal cycles of EM frequency application
EMncycles = 1;

# num of steps used in TM and EM solver
TMnstep = 16;
EMnstep = 40; #8*EMncycles;

stepIter = 0;

for currenttime in timeSteps:
 
  if stepIter ==0:
    # use original full domain with undeformed smp domain
    EMmeshFile = 'undeformedSMPFullDomain.msh';
  else:
    # use mesh generated using deformed smp from TM solver
    EMmeshFile = 'EMmergedMesh.msh';
 
  # use of pickle for EM variables
  with open('EMdatafile.txt','wb') as em:
    pickle.dump(currenttime, em)
    pickle.dump(nbCoupling, em)
    pickle.dump(EMmeshFile, em)
    pickle.dump(EMnstep, em)
    pickle.dump(EMncycles, em)
    
  if sys.version_info[0] < 3:
    os.system('python EM_weakCoupling.py')
  else:
    os.system('python3 EM_weakCoupling.py')
  
  # save EM results to a dir for each time step
  os.system('mkdir '+EMresultsDir+'/'+'t_'+str(currenttime));
  os.system('mv *csv stress_step* disp_step* '+EMresultsDir+'/'+'t_'+str(currenttime)+'/');
  os.system('cp restartInternalState0.ckp '+EMresultsDir+'/'+'t_'+str(currenttime)+'/');
  os.system('cp EMdatafile.txt '+EMresultsDir+'/'+'t_'+str(currenttime)+'/');
  os.system('cp '+str(EMmeshFile)+' '+EMresultsDir+'/'+'t_'+str(currenttime)+'/');
  
  #if dir of moved restart file exists then move them
  #files = os.listdir()
  #print(files)
  #for myfile in files:
    #if myfile == "restart0.ckp":
      #os.system('mv '+myfile+' '+str(EMresultsDir)+'/')
  
  # Start TM solver      

  # use original undeformed smp domain
  TMmeshFile = 'undeformedSMP.msh';
  
  # save through pickle variables to send to run the .py file
  with open('TMdatafile.txt','wb') as tm:
    pickle.dump(currenttime, tm)
    pickle.dump(TMnstep, tm)
    pickle.dump(ftime, tm)
    pickle.dump(nbCoupling, tm)
    pickle.dump(TMmeshFile, tm)
    pickle.dump(finalDisp, tm)
    
  #if dir of moved restart file exists then move them to main dir
  files = os.listdir(movedTMRestartDir+"/")
  #print(files)
  for myfile in files:
    #if myfile.endswith(".ckp"):
    if (myfile == "restart0.ckp") or (myfile == "nonLinearMechSolver0.ckp"):
      os.system('cp '+movedTMRestartDir+'/'+myfile+' '+os.getcwd())
  
  if sys.version_info[0] < 3:
    os.system('python TM_weakCoupling.py')
  else:
    os.system('python3 TM_weakCoupling.py')
    
  # increase step counter for TM 
  # to use deformed mesh and remesh for next iteration of TM
  stepIter+=1;
    
  # increment mechanical load for next time step in TM   
  #initialDisp = finalDisp;
  #finalDisp+=deltaDisp;  
  
  # Backup main restart file to current time step folder of TMresults
  os.system('mkdir '+TMresultsDir+'/'+'t_'+str(currenttime));
  os.system('cp nonLinearMechSolver0.ckp restart0.ckp '+TMresultsDir+'/'+'t_'+str(currenttime)+'/');

  # move restart*.ckp/nonlin*.ckp to a dir so that it doesnt not find file to load in EM and restart
  
  # move restart files of TM problem in movedTMRestartDir
  # so that EM problem does not read them
  x = os.listdir(os.getcwd())
  for i in x:
    #if i.endswith(".ckp"):
    if (i == "restart0.ckp") or (i == "nonLinearMechSolver0.ckp"):
      #print(i)
      os.system('mv '+i+' '+movedTMRestartDir+'/')

  # save TM results to a dir for each time step
  os.system('mv *csv stress_step* disp_step* '+TMresultsDir+'/'+'t_'+str(currenttime)+'/');
  os.system('cp restartInternalState0.ckp '+TMresultsDir+'/'+'t_'+str(currenttime)+'/');
  os.system('cp TMdatafile.txt '+TMresultsDir+'/'+'t_'+str(currenttime)+'/');
  os.system('cp '+str(TMmeshFile)+' '+TMresultsDir+'/'+'t_'+str(currenttime)+'/');
 
  # using latest deformed mesh for SMP 
  # remesh other domains for EM problem   
  fop = open("deformed_mesh_filename.geo","w");
  
  # search for latest deformed mesh filename in main dir
  list_of_deformed_files = []

  x = os.listdir(os.getcwd())
  for i in x:
    if i.startswith("deformed_mesh_"):
      list_of_deformed_files.append(i)
      
  #print('List of deformed: ',list_of_deformed_files);
    
  def extract_number(f):
    s = re.findall("(\d+).msh",f)
    return (int(s[0]) if s else -1,f)

  #print(max(list_of_deformed_files,key=extract_number))

  fop.write('\nMerge "'+str(max(list_of_deformed_files,key=extract_number))+'"; ');
  #fop.write('\nMerge "deformed_mesh_{}.msh"; '.format(TMnstep));
  fop.close();

  os.system('gmsh new_merge_mesh.geo -3 -order 1 -o EMmergedMesh.msh > merge_mesh_output_'+str(stepIter)+'.txt')
  
  #if stepIter>1:
    # copy deformed mesh files to TM dir for backup
    #os.system('cp deformed_mesh_* '+TMresultsDir+'/')

#end for loop for time
data4 = csv.reader(open('EMresults/t_0.005/force2222comp5.csv'), delimiter=';')
force4 = list(data4)
checkEqual(-2693.680876186706,float(force4[10][1]),1e-5)


data5 = csv.reader(open('TMresults/t_0.005/force11110comp5.csv'), delimiter=';')
force5 = list(data5)
checkEqual(4.578674904596331,float(force5[-1][1]),1e-5)

