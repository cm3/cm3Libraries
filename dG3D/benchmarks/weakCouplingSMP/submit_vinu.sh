#!/bin/sh

# script example for dragon2
#SBATCH --job-name WeakCoupledEMTM_5ms
#SBATCH --mail-user=vinayak.gholap@doct.uliege.be
#SBATCH --mail-type=ALL
#SBATCH --output="out.txt"
#SBATCH --ntasks=1
#SBATCH --mem-per-cpu=100000
#SBATCH --time=20-23:59:00
#SBATCH --partition=long

module load OpenMPI/4.1.1-GCC-11.2.0

SUBDIR=$HOME/cm3Libraries/dG3D/benchmarks/weakCouplingSMP
echo "subdir=" $SUBDIR

SCRATCH=$GLOBALSCRATCH/${USER}_$SLURM_JOB_ID
echo "workdir=" $SCRATCH


pyfile=run.py
#mshfile=.msh
echo "node list of job"  >> $SUBDIR/output.po$SLURM_JOB_ID

hostname >> $SUBDIR/output.po$SLURM_JOB_ID

cd $GLOBALSCRATCH
mkdir -p ${USER}_$SLURM_JOB_ID || exit $?
cp $SUBDIR/$pyfile $SCRATCH/ || exit $?
#cp $SUBDIR/$mshfile $SCRATCH/ || exit $? 

cp $SUBDIR/*.py $SCRATCH/ || exit $?
cp $SUBDIR/*.msh $SCRATCH/ || exit $?
cp $SUBDIR/new_merge_mesh.geo $SCRATCH/ || exit $? 

cd $SCRATCH # needed to work in the tmpscrach dir otherwise work on home !!
#ls -artl

export PETSC_DIR=$HOME/local/petsc-13.9.5
export PETSC_ARCH=arch-linux-cxx-opt

export PATH=$HOME/local/bin:$PATH

export PATH=$PATH:$HOME/cm3Libraries/dG3D/release/NonLinearSolver/gmsh


export PYTHONPATH=$PYTHONPATH:$HOME/cm3Libraries/dG3D/release:$HOME/cm3Libraries/dG3D/release/NonLinearSolver/gmsh/utils/wrappers:$HOME/cm3Libraries/dG3D/release/NonLinearSolver/gmsh/utils/wrappers/gmshpy


mpirun python3  $SCRATCH/$pyfile >& $SCRATCH/output.txt

echo -e "\n"

sleep 5

mkdir $SUBDIR/$SLURM_JOB_ID

srun cp -r -f $SCRATCH/* $SUBDIR/$SLURM_JOB_ID/  || exit $? 
srun rm -rf $SCRATCH ||  exit $? 
echo -e "\n"

