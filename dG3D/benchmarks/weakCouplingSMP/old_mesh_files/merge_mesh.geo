//Merge "deformed_mesh_2.msh";
Include "deformed_mesh_filename.geo";
Surface Loop(10) = {1,2,3,4};
//Volume(1) = {10};
//+

cm = 1e-2; // Unit
pp  = "Input/10Geometric dimensions/0";
pp2 = "Input/10Geometric dimensions/1Shell radius/";
colorpp = "Ivory";

// Smp
/*DefineConstant[
  RintSMP = {0.95*cm, Name StrCat[pp, "0SMP inner radius [m]"], Highlight Str[colorpp]},
  RextSMP = {1*cm, Name StrCat[pp, "1SMP outer radius [m]"], Highlight Str[colorpp] },
  H = {2*cm, Name StrCat[pp, "2SMP height along y-axis [m]"], Highlight Str[colorpp]}
];*/

//Coil
//+
SetFactory("Built-in");
//+
Point(5) = {0, 0.5, 0, 1.0};
//+
Point(6) = {0.045, 0.5, 0, 1.0};
//+
Point(7) = {-0.045, 0.5, 0, 1.0};
//+
Point(8) = {0, 0.5, 0.045, 1.0};
//+
Point(9) = {0, 0.5, -0.045, 1.0};
//+
Circle(7) = {9, 5, 7};
//+
Circle(8) = {7, 5, 8};
//+
Circle(9) = {8, 5, 6};
//+
Circle(10) = {6, 5, 9};
//+
Point(10) = {0.075, 0.5, 0, 1.0};
//+
Point(11) = {-0.075, 0.5, 0, 1.0};
//+
Point(12) = {0, 0.5, 0.075, 1.0};
//+
Point(13) = {0, 0.5, -0.075, 1.0};
//+
Circle(11) = {13, 5, 11};
//+
Circle(12) = {11, 5, 12};
//+
Circle(13) = {12, 5, 10};
//+
Circle(14) = {10, 5, 13};
//+
Curve Loop(15) = {11,12,13,14};
//+
Curve Loop(16) = {7,8,9,10};
//+
Surface(19) = {15, 16};
//+
Extrude {0, -1, 0} {
  Surface{19}; 
}

Surface Loop(555) = {19,32,36,40,44,48,52,56,60,61};
//+
Physical Surface(2222) = {19,32,36,40,44,48,52,56,60,61};
Physical Volume(2000) = 8;

DefineConstant[
  Ly    = {100*cm, Name StrCat[pp, "3Coil length along y-axis [m]"], Highlight Str[colorpp]}
  wcoil = {3*cm, Name StrCat[pp, "4Coil width [m]"], Highlight Str[colorpp]}
  RintCoil = {4.5*cm, Name StrCat[pp, "5Coil inner radius [m]"], Highlight Str[colorpp]}
  RextCoil = {7.5*cm, Name StrCat[pp, "6Coil outer radius [m]"], Highlight Str[colorpp]}
];

//+
Point(47) = {0, 0, 0, 1.0};
//+

//Air
//+
Point(52) = {2.8, 0, 0, 1.0};
//+
Point(53) = {-2.8, 0, 0, 1.0};
//+
Point(54) = {0, 2.8, 0, 1.0};
//+
Point(55) = {0, -2.8, 0, 1.0};
//+
Circle(60) = {52, 47, 54};
//+
Circle(61) = {54, 47, 53};
//+
Circle(62) = {53, 47, 55};
//+
Circle(63) = {55, 47, 52};
//+


//+
Point(58) = {0, 0, 2.8, 1.0};
//+
Point(59) = {0, 0, -2.8, 1.0};
//+
Circle(68) = {53, 47, 58};
//+
Circle(69) = {58, 47, 52};
//+
Circle(70) = {52, 47, 59};
//+
Circle(71) = {59, 47, 53};
//+



//+
Circle(76) = {54, 47, 58};
//+
Circle(77) = {58, 47, 55};
//+
Circle(78) = {55, 47, 59};
//+
Circle(79) = {59, 47, 54};
//+


//+
Curve Loop(116) = {60,76,69};
//+
Surface(122) = {116};
//+
Curve Loop(120) = {60,-79,-70};
//+
Surface(126) = {120};
//+
Curve Loop(124) = {63,-69,77};
//+
Surface(130) = {124};
//+
Curve Loop(128) = {63,70,-78};
//+
Surface(134) = {128};
//+
Curve Loop(132) = {76,-68,-61};
//+
Surface(138) = {132};
//+
Curve Loop(136) = {61,-71,79};
//+
Surface(142) = {136};
//+
Curve Loop(140) = {68,77,-62};
//+
Surface(146) = {140};
//+
Curve Loop(144) = {-62,-71,-78};
//+
Surface(150) = {144};

//+
Surface Loop(154) = {122,126,130,134,138,142,146,150};
Volume(120) = {154};

Volume(130) = {154,555,10};
Delete{ Volume{120}; }

Physical Surface(3333) = {122,126,130,134,138,142,146,150};
Physical Volume(3000) = 130;

DefineConstant[
  Flag_Infinity = {0, Choices{0,1},
					Name "Input/01Use shell transformation to infinity"}
];
// radius for surrounding air with transformation to infinity
If(Flag_Infinity==1)
  label_Rext = "1Outer [m]";
Else
  label_Rext = "1[m]";
EndIf

Rint = 200*cm;
Rext = 280*cm;

DefineConstant[
  Rint = {200*cm, Min 0.15, Max 0.9, Step 0.1, Name StrCat[pp2, "7Inner [m]"],
    Visible (Flag_Infinity==1), Highlight Str[colorpp] },
  Rext = {280*cm, Min Rint, Max 1, Step 0.1, Name StrCat[pp2, StrCat["8", label_Rext]],
    Visible 1, Highlight Str[colorpp] }
];

lc0 = Pi*Rint/6; // air
Characteristic Length { PointsOf{ Volume{130}; } } = lc0;

lc1  = 2*wcoil/2; // coil
Characteristic Length { PointsOf{ Volume{8}; } } = lc1;

//Mesh 2;
//Mesh 3;
//Mesh.SaveAll = 1;
//Save "mergedMesh.msh";
