#coding-Utf-8-*-

from gmshpy import *
from dG3Dpy import*

#from dG3DpyDebug import*
#script to launch PBC problem with a python script

#############
E = 32.0
nu = 0.4
K = E/3./(1.-2.*nu)	# Bulk mudulus
mu =E/2./(1.+nu)	  # Shear mudulus
rho = 2.7e-9 # Bulk mass

# creation of material law
appOrder = 11 # approximation order for exp and log operator of a tensor
potential1 = biLogarithmicElasticPotential(11,E,nu,appOrder)

'''E = 32.0
nu = 0.35
K = E/3./(1.-2.*nu)	# Bulk mudulus
mu =E/2./(1.+nu)	  # Shear mudulus
rho = 2.7e-9 # Bulk mass

# creation of material law
K=55.0
c1 = 0.55;
c2 = 0.3;
# creation of material law
potential1 = SuperElasticPotential(11, E, nu)
potential1.setParameters_C1_C2_K(c1,c2,K)'''

law1 = dG3DHyperelasticMaterialLaw(11,rho,potential1)
#law1.setUseBarF(True)

# geometry
geofile="rve.geo"
meshfile="rve.msh" 

# creation of part Domain
myfield1 = dG3DDomain(10,11,0,11,0,2,0,0,0,True,0)
#myfield1.matrixByPerturbation(1,1,1,1e-8)

PlaneStress = True
myfield1.setPlaneStressState(PlaneStress)
# solver
sol = 2  # Gmm=0 (default) Taucs=1 PETsc=2
soltype =1 # StaticLinear=0 (default) StaticNonLinear=1
nstep = 1000  # number of step (used only if soltype=1)
ftime =1.   # Final time (used only if soltype=1)
tol=1.e-6  # relative tolerance for NR scheme (used only if soltype=1)
nstepArch=20 # Number of step between 2 archiving (used only if soltype=1)
system = 3 # Displacement elimination =0 Multiplier elimination = 1 Displacement+ multiplier = 2
control = 0 # load control = 0 arc length control euler = 1

# creation of Solver
mysolver = nonLinearMechSolver(1000)
mysolver.createModel(geofile,meshfile,2,2)
mysolver.addDomain(myfield1)
mysolver.addMaterialLaw(law1)
mysolver.Scheme(soltype)
mysolver.Solver(sol)
mysolver.snlData(nstep,ftime,tol,1e-7)
mysolver.setSystemType(system)
mysolver.setControlType(control)
mysolver.stiffnessModification(bool(1))
mysolver.iterativeProcedure(bool(1))
mysolver.setMessageView(bool(1))

MaxIter=100
StepIncrease=2
StepReducFactor=3.
NumberReduction=15

mysolver.snlManageTimeStep(MaxIter,StepIncrease,StepReducFactor,NumberReduction)
pf=True
mysolver.pathFollowing(pf,0) #GLOBAL_ARC_LENGTH_BASED=0, LOCAL_BASED=1, HYPERELLIPTIC_BASED=2
# time-step adaptation by number of NR iterations
mysolver.setPathFollowingIncrementAdaptation(False,4) #automatic increment, with NR iteration
mysolver.setPathFollowingLocalSteps(2.50e-4,1e-10)
mysolver.setPathFollowingLocalIncrementType(0); #DEFO_ENERGY=0, DISSIPATION_ENERGY=1,PLASTIC_ENERGY=2,DAMAGE_ENERGY=3
mysolver.setPathFollowingIncrementAdaptation(True,4,1.e-4)
mysolver.setPathFollowingControlType(0)
mysolver.setPathFollowingCorrectionMethod(0)
mysolver.setPathFollowingArcLengthStep(1e-5)
mysolver.setBoundsOfPathFollowingArcLengthSteps(0,0.5e-5);

#boundary condition
microBC = nonLinearPeriodicBC(1000,2)
microBC.setOrder(2)
microBC.setBCPhysical(1,2,3,4)


method = 5	# Periodic mesh = 0, Langrange interpolation = 1, Cubic spline interpolation =2,  FE linear= 3, FE Quad = 4
degree = 2	# Order used for polynomial interpolation 
addvertex = 1 # Polynomial interpolation by mesh vertex = 0, Polynomial interpolation by virtual vertex 
microBC.setPeriodicBCOptions(method, degree,bool(addvertex)) 

 # Deformation gradient
microBC.setDeformationGradient(0.7,0.0,0.0,0.7)
#mysolver.activateTest(True)




 # Gradient of deformation gradient
microBC.setGradientOfDeformationGradient(0,0,0,0.035)
#microBC.setGradientOfDeformationGradient(0,0,1,0.035) 
#microBC.setGradientOfDeformationGradient(0,1,0,0.035) 

'''microBC.setGradientOfDeformationGradient(0,1,0,0.25) 
microBC.setGradientOfDeformationGradient(0,1,1,0.0) 
microBC.setGradientOfDeformationGradient(0,2,2,0.0) 

microBC.setGradientOfDeformationGradient(1,0,0,0.0)
microBC.setGradientOfDeformationGradient(1,0,1,0.0) 
microBC.setGradientOfDeformationGradient(1,0,2,0.0) 

microBC.setGradientOfDeformationGradient(1,1,1,0.0) 
microBC.setGradientOfDeformationGradient(1,1,2,0.0) 
microBC.setGradientOfDeformationGradient(1,2,2,0.0) 

microBC.setGradientOfDeformationGradient(2,0,0,0.0)
microBC.setGradientOfDeformationGradient(2,0,1,0.0) 
microBC.setGradientOfDeformationGradient(2,0,2,0.0) 

microBC.setGradientOfDeformationGradient(2,1,1,0.0) 
microBC.setGradientOfDeformationGradient(2,1,2,0.0) 
microBC.setGradientOfDeformationGradient(2,2,2,0.0)''' 


'''mysolver.displacementBC("Face",11,2,0.)
mysolver.displacementBC("Edge",1,0,0)
mysolver.displacementBC("Edge",3,0,0)
mysolver.displacementBC("Edge",3,1,-2.0)'''

'''defoDefoContact1=dG3DNodeOnSurfaceContactDomain(1200, 1, 101, 1,101, E, 1.)
mysolver.defoDefoContactInteraction(defoDefoContact1)
defoDefoContact2=dG3DNodeOnSurfaceContactDomain(1200, 1, 102, 1,102, E, 1.)
mysolver.defoDefoContactInteraction(defoDefoContact2)
defoDefoContact3=dG3DNodeOnSurfaceContactDomain(1200, 1, 103, 1,103, E, 1.)
mysolver.defoDefoContactInteraction(defoDefoContact3)
defoDefoContact4=dG3DNodeOnSurfaceContactDomain(1200, 1, 104, 1,104, E, 1.)
mysolver.defoDefoContactInteraction(defoDefoContact4)'''

mysolver.addMicroBC(microBC)
#stress averaging flag and averaging method 0- VOLUME, 1- SURFACE
mysolver.stressAveragingFlag(bool(1)) # set stress averaging ON- 0 , OFF-1
mysolver.setStressAveragingMethod(0) # 0 -volume 1- surface
#tangent averaging flag
mysolver.tangentAveragingFlag(bool(1)) # set tangent averaging ON -0, OFF -1
mysolver.setTangentAveragingMethod(2)#1,1.0e-6) # 0- perturbation 1- condensation

#mysolver.setExtractPerturbationToFileFlag(0)	
mysolver.stepBetweenArchiving(nstepArch)
# build view
mysolver.internalPointBuildView("Green-Lagrange_xx",IPField.STRAIN_XX, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_yy",IPField.STRAIN_YY, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_zz",IPField.STRAIN_ZZ, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_xy",IPField.STRAIN_XY, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_yz",IPField.STRAIN_YZ, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_xz",IPField.STRAIN_XZ, 1, 1);
mysolver.internalPointBuildView("sig_xx",IPField.SIG_XX, 1, 1);
mysolver.internalPointBuildView("sig_yy",IPField.SIG_YY, 1, 1);
mysolver.internalPointBuildView("sig_zz",IPField.SIG_ZZ, 1, 1);
mysolver.internalPointBuildView("sig_xy",IPField.SIG_XY, 1, 1);
mysolver.internalPointBuildView("sig_yz",IPField.SIG_YZ, 1, 1);
mysolver.internalPointBuildView("sig_xz",IPField.SIG_XZ, 1, 1);
mysolver.internalPointBuildView("sig_VM",IPField.SVM, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange equivalent strain",IPField.GL_EQUIVALENT_STRAIN, 1, 1);
mysolver.internalPointBuildView("Equivalent plastic strain",IPField.PLASTICSTRAIN, 1, 1);

mysolver.archivingForceOnPhysicalGroup("Edge",1,1);
mysolver.archivingNodeDisplacement(1,1,1);
# solve
mysolver.solve()

# test check
check = TestCheck()
import linecache
total_line_number = sum(1 for line in open('E_0_GP_0_stress.csv'))
homoStress = linecache.getline('E_0_GP_0_stress.csv',total_line_number)
print(homoStress)
val = float(homoStress.split(';')[1])
check.equal(1.030085e+02,val,1.e-4)


