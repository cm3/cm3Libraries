# test file

set(PYFILE StrongCouplingEMTM.py)

set(FILES2DELETE 
  disp*.msh
  stress*.msh
  *.csv
  restart*
)

add_cm3python_test(${PYFILE} "${FILES2DELETE}")
