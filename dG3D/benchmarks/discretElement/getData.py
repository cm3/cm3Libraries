import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

u2x = pd.read_csv("NodalDisplacementAtCorner2comp0.csv",sep=";",header=None)
u4y = pd.read_csv("NodalDisplacementAtCorner4comp1.csv",sep=";",header=None)
u5z = pd.read_csv("NodalDisplacementAtCorner5comp2.csv",sep=";",header=None)

f0 = 1 - 0.764190
PXX_phy11 = pd.read_csv("Average_11_P_XX.csv",sep=";",header=None)
PYY_phy11 = pd.read_csv("Average_11_P_YY.csv",sep=";",header=None)
PZZ_phy11 = pd.read_csv("Average_11_P_ZZ.csv",sep=";",header=None)

FXX = 1+ u2x.values[:,1]
FYY = 1+ u4y.values[:,1]
FZZ = 1+ u5z.values[:,1]


PXX = PXX_phy11.values[:,1]/(1-f0)
PYY = PYY_phy11.values[:,1]/(1-f0)
PZZ = PZZ_phy11.values[:,1]/(1-f0)

plt.figure()

plt.plot(FXX,"b--",label="FXX")
plt.plot(FYY,"r-",label="FYY")
plt.plot(FZZ,"g--",label="FZZ")

plt.legend()

J = FXX*FYY*FZZ
sigXX = PXX*FXX/J
sigYY = PYY*FYY/J
sigZZ = PZZ*FZZ/J


plt.figure()

plt.plot(PXX, PYY/PXX, "r-",label="PYY/PXX")
plt.plot(PXX, PZZ/PXX, "g--",label="PZZ/PXX")

plt.plot(PXX, sigYY/sigXX, "rv",label="sigYY/sigXX")
plt.plot(PXX, sigZZ/sigXX, "gv",label="sigZZ/sigXX")
plt.legend()

plt.show()



