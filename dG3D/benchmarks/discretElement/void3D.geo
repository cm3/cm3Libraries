//+
SetFactory("OpenCASCADE");
Mesh.SecondOrderLinear=1;
//+
Box(1) = {0, 0, 0, 1, 1, 1};
//+
Sphere(2) = {0.65, 0.67, 0.65, 0.3, -Pi/2, Pi/2, 2*Pi};
//+
Sphere(3) = {0.25, 0.25, 0.25, 0.2, -Pi/2, Pi/2, 2*Pi};
//+
Sphere(4) = {0.25, 0.7, 0.25, 0.2, -Pi/2, Pi/2, 2*Pi};
//+
Sphere(5) = {0.7, 0.25, 0.25, 0.2, -Pi/2, Pi/2, 2*Pi};
//+
Sphere(6) = {0.7, 0.2, 0.7, 0.15, -Pi/2, Pi/2, 2*Pi};
//+
Sphere(7) = {0.2, 0.2, 0.7, 0.15, -Pi/2, Pi/2, 2*Pi};

//+
BooleanDifference{ Volume{1}; Delete; }{ Volume{2}; Volume{3}; Volume{4}; Volume{5};Volume{6};Volume{7}; Delete; }

lcar1 = .2;
lcar3 = .1;

MeshSize{ PointsOf{ Volume{:}; } } = lcar1;
MeshSize{ PointsOf{ Surface{7,8, 9, 10, 11, 12}; } } = lcar3;

//+
Physical Volume(11) = {1};
//+
Physical Surface(44) = {14};
//+
Physical Surface(45) = {16};
//+
Physical Surface(46) = {13};
//+
Physical Surface(47) = {18};
//+
Physical Surface(48) = {15};
//+
Physical Surface(49) = {17};
//+
Physical Curve(50) = {31};
//+
Physical Curve(51) = {36};
//+
Physical Curve(52) = {41};
//+
Physical Curve(53) = {33};
//+
Physical Curve(54) = {37};
//+
Physical Curve(55) = {35};
//+
Physical Curve(56) = {40};
//+
Physical Curve(57) = {38};
//+
Physical Curve(58) = {39};
//+
Physical Curve(59) = {42};
//+
Physical Curve(60) = {34};
//+
Physical Curve(61) = {32};


//+
Physical Point(62) = {21};
//+
Physical Point(63) = {26};
//+
Physical Point(64) = {25};
//+
Physical Point(65) = {22};
//+
Physical Point(66) = {23};
//+
Physical Point(67) = {27};
//+
Physical Point(68) = {28};
//+
Physical Point(69) = {24};
//+
Physical Surface(70) = {8, 9, 7, 10, 11, 12};
