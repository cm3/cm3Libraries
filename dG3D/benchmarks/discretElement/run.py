from gmshpy import *
from dG3Dpy import*
from math import*
import numpy as np
import pandas as pd
import os
from os.path import isfile, join
from os import listdir


lawnum1 = 11 # unique number of law
E = 3E3 #MPa
nu = 0.3
K = E/3./(1.-2.*nu)	# Bulk mudulus
mu =E/2./(1.+nu)	  # Shear mudulus
rho = 7850e-9 # Bulk mass
sy0 = 100. #MPa
h = E/50.
harden1 = LinearExponentialJ2IsotropicHardening(lawnum1, sy0,h,0.,10.)
smallStrain=False
if smallStrain:
  law1   = J2SmallStrainDG3DMaterialLaw(lawnum1,rho,E,nu,harden1)
else:
  law1   = J2LinearDG3DMaterialLaw(lawnum1,rho,E,nu,harden1)
  law1.setStrainOrder(11)

# geometry
meshfile="void3D.msh" # name of mesh file

myfield1 = dG3DDomain(1000,11,0,lawnum1,0,3)

# solver
sol = 2  # Gmm=0 (default) Taucs=1 PETsc=2
soltype =1 # StaticLinear=0 (default) StaticNonLinear=1
nstep = 100  # number of step (used only if soltype=1)
ftime = 1  # Final time (used only if soltype=1)
tol=1.e-5  # relative tolerance for NR scheme (used only if soltype=1)
nstepArch=1 # Number of step between 2 archiving (used only if soltype=1)

# creation of Solver
mysolver = nonLinearMechSolver(1000)
mysolver.loadModel(meshfile)
mysolver.addDomain(myfield1)
mysolver.addMaterialLaw(law1)
mysolver.Scheme(soltype)
mysolver.Solver(sol)
mysolver.snlData(nstep,ftime,tol)
mysolver.stepBetweenArchiving(nstepArch)

mysolver.snlManageTimeStep(50,15,2.,200)
#stiffModif = NumIterationBasedStiffnessModificationMonitoring(1)
#stiffModif.modifyStiffnessAfterNumIterations(5)
#mysolver.stiffnessModification(stiffModif)

microBC = nonLinearPeriodicBC(10,3)
microBC.setBCPhysical(49,44,46,48,45,47)
method =5	# Periodic mesh = 0
degree = 3 # Order used for polynomial interpolation
addvertex = 0  # Polynomial interpolation by mesh vertex = 0, Polynomial interpolation by virtual vertex
microBC.setPeriodicBCOptions(method, degree, bool(addvertex))
mysolver.addMicroBC(microBC)

mysolver.activateTest(True)
mysolver.setRVEVolume(1.)


mysolver.displacementBCOnControlNode(1,0,0.)
mysolver.displacementBCOnControlNode(1,1,0.)
mysolver.displacementBCOnControlNode(1,2,0.)
mysolver.displacementBCOnControlNode(2,1,0.)
mysolver.displacementBCOnControlNode(2,2,0.)
mysolver.displacementBCOnControlNode(4,0,0.)
mysolver.displacementBCOnControlNode(4,2,0.)
mysolver.displacementBCOnControlNode(5,0,0.)
mysolver.displacementBCOnControlNode(5,1,0.)

fact1 = 0.3
fact2 = 0.5
A = 1e3
"""
mysolver.forceBCOnControlNode(2,0,A)
mysolver.forceBCOnControlNode(4,1,fact1*A)
mysolver.forceBCOnControlNode(5,2,fact2*A)

"""
def myFunc1(x, y):
  return (1+x)*(1+y), 1+y, 1+x
mapping = generalMappingPython(myFunc1)

f2x = MultipleDofNonLinearSpring(1,0,0,0,A,mapping)
f2x.setDependentPoint(0,1,0,1)
f2x.setDependentPoint(0,0,1,2)
mysolver.addDiscreteELement(f2x)

f4y = MultipleDofNonLinearSpring(0,1,0,1,fact1*A,mapping)
f4y.setDependentPoint(1,0,0,0)
f4y.setDependentPoint(0,0,1,2)
mysolver.addDiscreteELement(f4y)

f5z = MultipleDofNonLinearSpring(0,0,1,2,fact2*A,mapping)
f5z.setDependentPoint(1,0,0,0)
f5z.setDependentPoint(0,1,0,1)
mysolver.addDiscreteELement(f5z)


pf= True
method=0
mysolver.pathFollowing(pf,method)
if pf:
	if method==0:
		mysolver.setPathFollowingIncrementAdaptation(True,5,0.15)
		mysolver.setPathFollowingControlType(0)
		mysolver.setPathFollowingCorrectionMethod(0)
		mysolver.setPathFollowingArcLengthStep(1.e-3)
		mysolver.setBoundsOfPathFollowingArcLengthSteps(0,2.);
	elif method==1:
		mysolver.setPathFollowingIncrementAdaptation(False,4)
		mysolver.setPathFollowingLocalSteps(1e-3,5e-11*30)
		mysolver.setPathFollowingLocalIncrementType(3); 
		mysolver.setPathFollowingSwitchCriterion(1e-3)
		
		

# build view
mysolver.internalPointBuildView("Strain_xx",IPField.STRAIN_XX, 1, 1);
mysolver.internalPointBuildView("Strain_yy",IPField.STRAIN_YY, 1, 1);
mysolver.internalPointBuildView("Strain_zz",IPField.STRAIN_ZZ, 1, 1);
mysolver.internalPointBuildView("Strain_xy",IPField.STRAIN_XY, 1, 1);
mysolver.internalPointBuildView("Strain_yz",IPField.STRAIN_YZ, 1, 1);
mysolver.internalPointBuildView("Strain_xz",IPField.STRAIN_XZ, 1, 1);
mysolver.internalPointBuildView("sig_xx",IPField.SIG_XX, 1, 1);
mysolver.internalPointBuildView("sig_yy",IPField.SIG_YY, 1, 1);
mysolver.internalPointBuildView("sig_zz",IPField.SIG_ZZ, 1, 1);
mysolver.internalPointBuildView("sig_xy",IPField.SIG_XY, 1, 1);
mysolver.internalPointBuildView("sig_yz",IPField.SIG_YZ, 1, 1);
mysolver.internalPointBuildView("sig_xz",IPField.SIG_XZ, 1, 1);
mysolver.internalPointBuildView("sig_VM",IPField.SVM, 1, 1);
mysolver.internalPointBuildView("Equivalent plastic strain",IPField.PLASTICSTRAIN, 1, 1);
mysolver.internalPointBuildView("pression",IPField.PRESSION,1,1)
mysolver.internalPointBuildView("plastic possion ratio",IPField.PLASTIC_POISSON_RATIO,1,1)

mysolver.internalPointBuildView("ISO_YIELD",IPField.ISO_YIELD,1,1)

mysolver.internalPointBuildView("Ee_ZZ",IPField.Ee_ZZ,1,1)
mysolver.internalPointBuildView("Ee_YY",IPField.Ee_YY,1,1)
mysolver.internalPointBuildView("Ee_XX",IPField.Ee_XX,1,1)

mysolver.internalPointBuildView("Eve1_ZZ",IPField.Eve1_ZZ,1,1)
mysolver.internalPointBuildView("Eve1_YY",IPField.Eve1_YY,1,1)
mysolver.internalPointBuildView("Eve1_XX",IPField.Eve1_XX,1,1)
Fvars = [IPField.JACOBIAN, IPField.F_XX,IPField.F_XY, IPField.F_XZ,
         IPField.F_YX, IPField.F_YY, IPField.F_YZ,
         IPField.F_ZX, IPField.F_ZY, IPField.F_ZZ,
         IPField.P_XX,IPField.P_XY, IPField.P_XZ,
         IPField.P_YX, IPField.P_YY, IPField.P_YZ,
         IPField.P_ZX, IPField.P_ZY, IPField.P_ZZ,
         IPField.PLASTICSTRAIN,IPField.ISO_YIELD, IPField.PLASTIC_ENERGY, IPField.DEFO_ENERGY,
         IPField.FP_XX,IPField.FP_XY,IPField.FP_XZ,
         IPField.FP_YX, IPField.FP_YY, IPField.FP_YZ,
         IPField.FP_ZX, IPField.FP_ZY, IPField.FP_ZZ]
for i in Fvars:
  mysolver.archivingAverageValueOnPhysical(11,i)

for n in range(1,9):
  mysolver.archivingNodeDisplacementOnControlNode(n,0)
  mysolver.archivingNodeDisplacementOnControlNode(n,1)
  mysolver.archivingNodeDisplacementOnControlNode(n,2)



# solve
mysolver.solve()


