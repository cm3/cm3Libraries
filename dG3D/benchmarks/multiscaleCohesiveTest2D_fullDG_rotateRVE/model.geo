
mm=1.0;
L=1.*mm;
H = 1*mm; 
sl1=0.5*L;
Point(1)={0,0,0,sl1};
Point(2)={L,0,0,sl1};
Point(3)={L,H,0,sl1};
Point(4)={0,H,0,sl1};
Point(5)={2*L,0,0,sl1};
Point(6)={2*L,H,0,sl1};
Line(1)={1,2};
Line(2)={2,3};
Line(3)={3,4};
Line(4)={4,1};
Line(5)={2,5};
Line(6)={5,6};
Line(7)={6,3};

l[0]=newreg;
Line Loop(l[0])={1,2,3,4};
Plane Surface(11)={l[0]};

l[1]=newreg;
Line Loop(l[1])={5,6,7,-2};
Plane Surface(12)={l[1]};

Physical Surface(11)={11};
Physical Point(1)={1}; 
Physical Point(2)={2};
Physical Point(3)={5};
Physical Point(4)={6}; 

Transfinite Line {4, 2,6} = 2 Using Progression 1;
Transfinite Line {3, 1,5,7} = 2 Using Progression 1;
Transfinite Surface {11, 12};
Recombine Surface {11,12};

Physical Surface(12) = {12};
Physical Line(1) = {1, 5};
Physical Line(2) = {6};
Physical Line(3) = {7,3};
Physical Line(4) = {4};
//+
Translate {0.3, 0, 0} {
  Point{3}; 
}
