#coding-Utf-8-*-
from gmshpy import *
from dG3Dpy import*
import os

#script to launch beam problem with a python script
lawnum1 = 1 # unique number of law
rho   = 7850
young = 28.9e9
nu    = 0.3 
sy0   = 150.e6
h     = young/50.


# geometry
geofile="twoHole.geo"
meshfile="twoHole.msh" # name of mesh file

# solver
sol = 2  # Gmm=0 (default) Taucs=1 PETsc=2
soltype = 1 # StaticLinear=0 (default) StaticNonLinear=1
nstep = 25   # number of step (used only if soltype=1)
ftime =1.   # Final time (used only if soltype=1)
tol=1.e-6 # relative tolerance for NR scheme (used only if soltype=1)
nstepArch=1 # Number of step between 2 archiving (used only if soltype=1)



harden = LinearExponentialJ2IsotropicHardening(1, sy0, h, 0., 10.)
law1   = J2LinearDG3DMaterialLaw(lawnum1,rho,young,nu,harden)
law1.setUseBarF(True)


# creation of ElasticField
beta1 = 1e2
fullDG = False;
averageStrainBased = False

myfield1 = dG3DDomain(1000,83,0,lawnum1,fullDG,3)
myfield1.stabilityParameters(beta1)
myfield1.averageStrainBased(averageStrainBased)


# creation of Solver
mysolver = nonLinearMechSolver(1000)
#mysolver.createModel(geofile,meshfile,3,1)
mysolver.loadModel(meshfile)
mysolver.addDomain(myfield1)
mysolver.addMaterialLaw(law1)

mysolver.Scheme(soltype)
mysolver.Solver(sol)
mysolver.snlData(nstep,ftime,tol)
mysolver.stepBetweenArchiving(nstepArch)
mysolver.options("-ksp_type preonly -pc_type lu -pc_factor_mat_solver_type mumps")

mysolver.snlManageTimeStep(15,5,2.,50)
optimalNumber = 5
maximalStep = 3.e-1
minimalStep = 1e-3
power = 1
maximalFailStep = 10
mysolver.setTimeIncrementAdaptation(True,optimalNumber,power,maximalStep,minimalStep,maximalFailStep)
# BC
#mysolver.displacementBC("Volume",83,2,0.)
mysolver.displacementBC("Face",84,0,0.)
mysolver.displacementBC("Face",84,1,0.)
mysolver.displacementBC("Face",84,2,0.)
mysolver.displacementBC("Face",85,0,0.)
mysolver.displacementBC("Face",85,1,5e-4)
mysolver.displacementBC("Face",85,2,0.)


mysolver.internalPointBuildView("svm",IPField.SVM, 1, 1)
mysolver.internalPointBuildView("sig_xx",IPField.SIG_XX, 1, 1)
mysolver.internalPointBuildView("sig_yy",IPField.SIG_YY, 1, 1)
mysolver.internalPointBuildView("sig_zz",IPField.SIG_ZZ, 1, 1)
mysolver.internalPointBuildView("sig_xy",IPField.SIG_XY, 1, 1)
mysolver.internalPointBuildView("sig_yz",IPField.SIG_YZ, 1, 1)
mysolver.internalPointBuildView("sig_xz",IPField.SIG_XZ, 1, 1)
mysolver.internalPointBuildView("epl",IPField.PLASTICSTRAIN, 1, 1)
mysolver.internalPointBuildView("U_NORM",IPField.U_NORM, 1, 1)

mysolver.internalPointBuildViewIncrement("epl",IPField.PLASTICSTRAIN, 1, 1)
mysolver.internalPointBuildViewIncrement("U_NORM",IPField.U_NORM, 1, 1)

mysolver.internalPointBuildViewRate("epl",IPField.PLASTICSTRAIN, 1, 1)
mysolver.internalPointBuildViewRate("U_NORM",IPField.U_NORM, 1, 1)


mysolver.archivingForceOnPhysicalGroup("Face", 84, 1)
mysolver.archivingNodeDisplacement(19,1,1)

mysolver.setWriteDeformedMeshToFile(True)

mysolver.solve()

check = TestCheck()
check.equal(-2.878882e+03,mysolver.getArchivedForceOnPhysicalGroup("Face", 84, 1),1.e-4)


dataTransfer = IPDataTransfer()
trans1 = EqualIPDataTransferMat() # should be dependent on IP type
dataTransfer.add(83,trans1) # physical of domain with transfer

'''
in the second solver
- we load the deformed mesh at the last time step
- copy IPState from first to second solver
'''

if True:
  ####################################
  #####################################
  lawnum1 = 1 # unique number of law
  rho   = 7850
  young = 28.9e9
  nu    = 0.3 
  sy0   = 150.e6
  h     = young/50.


  # solver
  sol = 2  # Gmm=0 (default) Taucs=1 PETsc=2
  soltype = 1 # StaticLinear=0 (default) StaticNonLinear=1
  nstep = 50   # number of step (used only if soltype=1)
  ftime =1.   # Final time (used only if soltype=1)
  tol=1.e-6 # relative tolerance for NR scheme (used only if soltype=1)
  nstepArch=1 # Number of step between 2 archiving (used only if soltype=1)


  harden = LinearExponentialJ2IsotropicHardening(1, sy0, h, 0., 10.)
  law1   = J2LinearDG3DMaterialLaw(lawnum1,rho,young,nu,harden)
  law1.setUseBarF(True)


  # creation of ElasticField
  beta1 = 1e2
  fullDG = False;
  averageStrainBased = False

  myfield1 = dG3DDomain(1000,83,0,lawnum1,fullDG,3)
  myfield1.stabilityParameters(beta1)
  myfield1.averageStrainBased(averageStrainBased)


  # creation of Solver
  mysolver2 = nonLinearMechSolver(1000)
  meshfile = "deformed_mesh_6.msh"
  mysolver2.loadModel(meshfile)
  mysolver2.addDomain(myfield1)
  mysolver2.addMaterialLaw(law1)

  mysolver2.Scheme(soltype)
  mysolver2.Solver(sol)
  mysolver2.snlData(nstep,ftime,tol)
  mysolver2.stepBetweenArchiving(nstepArch)
  mysolver2.options("-ksp_type preonly -pc_type lu -pc_factor_mat_solver_type mumps")

  mysolver2.snlManageTimeStep(15,5,2.,50)
  # BC
  #mysolver2.displacementBC("Volume",83,2,0.)
  mysolver2.displacementBC("Face",84,0,0.)
  mysolver2.displacementBC("Face",84,1,0.)
  mysolver2.displacementBC("Face",84,2,0.)
  mysolver2.displacementBC("Face",85,0,-2e-4)
  mysolver2.displacementBC("Face",85,1,2e-4)
  mysolver2.displacementBC("Face",85,2,0.)


  mysolver2.internalPointBuildView("svm",IPField.SVM, 1, 1)
  mysolver2.internalPointBuildView("sig_xx",IPField.SIG_XX, 1, 1)
  mysolver2.internalPointBuildView("sig_yy",IPField.SIG_YY, 1, 1)
  mysolver2.internalPointBuildView("sig_zz",IPField.SIG_ZZ, 1, 1)
  mysolver2.internalPointBuildView("sig_xy",IPField.SIG_XY, 1, 1)
  mysolver2.internalPointBuildView("sig_yz",IPField.SIG_YZ, 1, 1)
  mysolver2.internalPointBuildView("sig_xz",IPField.SIG_XZ, 1, 1)
  mysolver2.internalPointBuildView("epl",IPField.PLASTICSTRAIN, 1, 1)
  mysolver2.internalPointBuildView("U_NORM",IPField.U_NORM, 1, 1)

  mysolver2.internalPointBuildViewIncrement("epl",IPField.PLASTICSTRAIN, 1, 1)
  mysolver2.internalPointBuildViewIncrement("U_NORM",IPField.U_NORM, 1, 1)

  mysolver2.internalPointBuildViewRate("epl",IPField.PLASTICSTRAIN, 1, 1)
  mysolver2.internalPointBuildViewRate("U_NORM",IPField.U_NORM, 1, 1)


  mysolver2.archivingForceOnPhysicalGroup("Face", 84, 1)
  mysolver2.archivingNodeDisplacement(19,1,1)

  mysolver2.transferIPField(mysolver,dataTransfer)

  mysolver2.solve()
