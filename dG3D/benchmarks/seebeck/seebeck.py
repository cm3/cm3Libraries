#coding-Utf-8-*-
from gmshpy import *

#from dG3DpyDebug import*
from dG3Dpy import*

#script to launch beam problem with a python script
lawnum=num = 1 # unique number of law
#for carbon steel properties
rho = 1750.
E=40.e6
EA=230.e6
GA=24.e6
nu=0.2
t0=273.
nu_minor=0.256*E/EA
Ax=0.#1.
Ay=0.#1.
Az=1.#0.was working
kx=ky=kz=1.612#20.# 20.#1.612#24.3  #21-181   !!
alphacf=2.e-6#11.e-6#12.e-6
alpha = beta=gamma=0.
lx=ly=lz=8.4e4#59880.#8.4e4#7.e6#     5.9e6 #carbon  59880.  !!
seebeck=1.941e-4#3.e-6#1.941e-4#1.9e-6  #carbon
cp= 0.*712.*rho #for paper material #for carbon graphit 712 or 544.  CF is 1780
v0=0.


#for carbon steel propertiesdwdf*=0.;
# geometry
geofile="seebeck.geo"
meshfile="seebeck.msh" # name of mesh file

# solver
sol = 2  # Gmm=0 (default) Taucs=1 PETsc=2
soltype = 1 # StaticLinear=0 (default) StaticNonLinear=1
nstep = 250   # number of step (used only if soltype=1) 500. with cp, 20 work before
ftime =50.   # 1  Final time (used only if soltype=1) 10000. with cp
tol=1.e-4   # relative tolerance for NR scheme (used only if soltype=1)
nstepArch=1 # Number of step between 2 archiving (used only if soltype=1)
fullDg = 1 #O = CG, 1 = DG
space1 = 0 # function space (Lagrange=0)
beta1  = 100.

#  compute solution and BC (given directly to the solver
# creation of law

law1 =mlawAnIsotropicElecTherMechDG3DMaterialLaw(num,E,nu,rho,EA,GA,nu_minor,Ax,Ay,Az,alpha, beta,gamma,t0, lx,ly,lz,seebeck,kx, ky,kz,cp,alphacf,v0)
law1.setUseBarF(True)
# creation of ElasticField

nfield = 167 # number of the field (physical number of surface)
#myfield1 = dG3DDomain(1000,nfield,space1,lawnum,fullDg)
myfield1 = ElecTherMechDG3DDomain(1000,nfield,space1,lawnum,fullDg,1.e6)
myfield1.matrixByPerturbation(0,0,0,1e-8)
#myfield1.matrixByPerturbation(1,1,1,1e-8)
myfield1.stabilityParameters(beta1)
myfield1.ElecTherMechStabilityParameters(beta1,bool(1))
myfield1.setConstitutiveExtraDofDiffusionUseEnergyConjugatedField(bool(0))
# creation of Solver
mysolver = nonLinearMechSolver(1000)
mysolver.createModel(geofile,meshfile,3,1)
#mysolver.loadModel(meshfile)
mysolver.addDomain(myfield1)
mysolver.addMaterialLaw(law1)
mysolver.Scheme(soltype)
mysolver.Solver(sol)
mysolver.snlData(nstep,ftime,tol)
mysolver.stepBetweenArchiving(nstepArch)
# BC

#mechanical BC
#mysolver.displacementBC("Volume",nfield,0,0.)
#mysolver.displacementBC("Volume",nfield,1,0.)
#mysolver.displacementBC("Volume",nfield,2,0.)

mysolver.displacementBC("Face",257,0,0.)
mysolver.displacementBC("Face",239,2,0.)
mysolver.displacementBC("Face",241,2,0.)
mysolver.displacementBC("Face",243,2,0.)
mysolver.displacementBC("Face",245,2,0.)
mysolver.displacementBC("Face",259,1,0.)
mysolver.displacementBC("Face",261,1,0.)
mysolver.displacementBC("Face",263,1,0.)
mysolver.displacementBC("Face",265,1,0.)


#thermal BC
cyclicFunctionTemp1=cycleFunctionTime(0., t0, ftime, t0+26.);#2test
cyclicFunctionTemp2=cycleFunctionTime(0., t0, ftime, t0+26.);#2test
#cyclicFunctionTemp2=cycleFunctionTime(0., 25.+273., ftime, 25.+273.);

mysolver.displacementBC("Face",257,3,cyclicFunctionTemp1)#2test
mysolver.displacementBC("Face",247,3,cyclicFunctionTemp2)#2test
mysolver.initialBC("Volume","Position",nfield,3,t0)#2test
#mysolver.displacementBC("Volume",nfield,3,t0)

#electrical BC
cyclicFunctionV1=cycleFunctionTime(0., 0., ftime, 0.058);#2test
cyclicFunctionV2=cycleFunctionTime(0.,0.0, ftime, 0.0);#2test
mysolver.displacementBC("Face",257,4,cyclicFunctionV1);#2test
mysolver.displacementBC("Face",247,4,cyclicFunctionV2);#2test
mysolver.initialBC("Volume","Position",nfield,4,0.0);#2test
#mysolver.displacementBC("Volume",nfield,4,0.00005)


mysolver.internalPointBuildView("svm",IPField.SVM, 1, 1)
mysolver.internalPointBuildView("sig_xx",IPField.SIG_XX, 1, 1)
mysolver.internalPointBuildView("sig_yy",IPField.SIG_YY, 1, 1)
mysolver.internalPointBuildView("sig_zz",IPField.SIG_ZZ, 1, 1)
mysolver.internalPointBuildView("sig_xy",IPField.SIG_XY, 1, 1)
mysolver.internalPointBuildView("sig_yz",IPField.SIG_YZ, 1, 1)
mysolver.internalPointBuildView("sig_xz",IPField.SIG_XZ, 1, 1)
mysolver.internalPointBuildView("temperature",IPField.TEMPERATURE, 1, 1)
mysolver.internalPointBuildView("qx",IPField.THERMALFLUX_X, 1, 1)
mysolver.internalPointBuildView("qy",IPField.THERMALFLUX_Y, 1, 1)
mysolver.internalPointBuildView("qz",IPField.THERMALFLUX_Z, 1, 1)
mysolver.internalPointBuildView("voltage",IPField.VOLTAGE, 1, 1)
mysolver.internalPointBuildView("ex",IPField.ELECTRICALFLUX_X, 1, 1)
mysolver.internalPointBuildView("ey",IPField.ELECTRICALFLUX_Y, 1, 1)
mysolver.internalPointBuildView("ez",IPField.ELECTRICALFLUX_Z, 1, 1)

mysolver.archivingForceOnPhysicalGroup("Face", 247, 3)
mysolver.archivingForceOnPhysicalGroup("Face", 247, 4)

mysolver.archivingNodeDisplacement(97,3,1);
mysolver.archivingNodeDisplacement(96,3,1);
mysolver.archivingNodeDisplacement(95,3,1);
mysolver.archivingNodeDisplacement(94,3,1);
mysolver.archivingNodeDisplacement(93,3,1);

mysolver.archivingNodeDisplacement(97,4,1);
mysolver.archivingNodeDisplacement(96,4,1);
mysolver.archivingNodeDisplacement(95,4,1);
mysolver.archivingNodeDisplacement(94,4,1);
mysolver.archivingNodeDisplacement(93,4,1);

mysolver.archivingNodeIP(97, IPField.THERMALFLUX_X,IPField.MEAN_VALUE,1);
mysolver.archivingNodeIP(96, IPField.THERMALFLUX_X,IPField.MEAN_VALUE,1);
mysolver.archivingNodeIP(95, IPField.THERMALFLUX_X,IPField.MEAN_VALUE,1);
mysolver.archivingNodeIP(94, IPField.THERMALFLUX_X,IPField.MEAN_VALUE,1);
mysolver.archivingNodeIP(93, IPField.THERMALFLUX_X,IPField.MEAN_VALUE,1);

mysolver.archivingNodeIP(97, IPField.ELECTRICALFLUX_X,IPField.MEAN_VALUE,1);
mysolver.archivingNodeIP(96, IPField.ELECTRICALFLUX_X,IPField.MEAN_VALUE,1);
mysolver.archivingNodeIP(95, IPField.ELECTRICALFLUX_X,IPField.MEAN_VALUE,1);
mysolver.archivingNodeIP(94, IPField.ELECTRICALFLUX_X,IPField.MEAN_VALUE,1);
mysolver.archivingNodeIP(93, IPField.ELECTRICALFLUX_X,IPField.MEAN_VALUE,1);


mysolver.solve()

check = TestCheck()
check.equal(3.854976e+05,mysolver.getArchivedForceOnPhysicalGroup("Face", 247, 3),1.e-6)
check.equal(4.429180e+06,mysolver.getArchivedForceOnPhysicalGroup("Face", 247, 4),1.e-6)
check.equal(2.990000e+02,mysolver.getArchivedNodalValue(97,3,mysolver.displacement),1.e-6)
check.equal(5.800000e-02,mysolver.getArchivedNodalValue(97,4,mysolver.displacement),1.e-6)
check.equal(3.154338e+02,mysolver.getArchivedNodalValue(96,3,mysolver.displacement),1.e-6)
check.equal(4.031044e-02,mysolver.getArchivedNodalValue(96,4,mysolver.displacement),1.e-6)
check.equal(3.209119e+02,mysolver.getArchivedNodalValue(95,3,mysolver.displacement),1.e-6)
check.equal(2.474693e-02,mysolver.getArchivedNodalValue(95,4,mysolver.displacement),1.e-6)
check.equal(3.154339e+02,mysolver.getArchivedNodalValue(94,3,mysolver.displacement),1.e-6)
check.equal(1.131004e-02,mysolver.getArchivedNodalValue(94,4,mysolver.displacement),1.e-6)
check.equal(2.990000e+02,mysolver.getArchivedNodalValue(93,3,mysolver.displacement),1.e-6)
check.equal(0.,mysolver.getArchivedNodalValue(93,4,mysolver.displacement),1.e-6)


