#coding-Utf-8-*-

from gmshpy import *
from dG3Dpy import*

#script to launch PBC problem with a python script

# material law
Em = 3.2E3
num = 0.3
rho = 2.7e-9 # Bulk mass

Ef = 3.2E5
nuf = 0.3

# creation of material law

numLawEP = 11
lawEP = dG3DLinearElasticMaterialLaw(numLawEP,rho,Em,num)
numLawF =12
lawF = dG3DLinearElasticMaterialLaw(numLawF,rho,Ef,nuf)

# geometry
meshfile="rve.msh" # name of mesh file

# creation of part Domain
myfieldEP = dG3DDomain(1000,1001,0,numLawEP,0)
myfieldF = dG3DDomain(1000,1002,0,numLawF,0)
beta1 = 1e5
interfield1 = interDomainBetween3D(1000,myfieldEP,myfieldF,0)
interfield1.stabilityParameters(beta1)

# solver
sol = 2  # Gmm=0 (default) Taucs=1 PETsc=2
soltype =1 # StaticLinear=0 (default) StaticNonLinear=1
nstep = 1  # number of step (used only if soltype=1)
ftime =1.   # Final time (used only if soltype=1)
tol=1.e-6  # relative tolerance for NR scheme (used only if soltype=1)
nstepArch=1 # Number of step between 2 archiving (used only if soltype=1)
system = 1 # Displacement elimination =0 Multiplier elimination = 1 Displacement+ multiplier = 2
control = 0 # load control = 0 arc length control euler = 1

# creation of Solver
mysolver = nonLinearMechSolver(1000)
mysolver.loadModel(meshfile)
mysolver.addDomain(myfieldEP)
mysolver.addDomain(myfieldF)
mysolver.addDomain(interfield1)
mysolver.addMaterialLaw(lawEP)
mysolver.addMaterialLaw(lawF)

mysolver.Scheme(soltype)
mysolver.Solver(sol)
mysolver.snlData(nstep,ftime,tol)
mysolver.setSystemType(system)
mysolver.setControlType(control)
mysolver.stiffnessModification(True)
mysolver.iterativeProcedure(True)
mysolver.setMessageView(True)



#creat microBC
microBC = nonLinearMinimalKinematicBC(1000,3)
microBC.setOrder(1)
microBC.setBCPhysical(101,111,121,102,112,122)
microBC.setDofPartition(3,0,0)
microBC.setDeformationGradient(1.0,0.02,0.0,0.0,1.,0,0,0,1.02)
#add to solver
mysolver.addMicroBC(microBC)

#stress averaging flag and averaging method 0- VOLUME, 1- SURFACE
mysolver.stressAveragingFlag(True) # set stress averaging ON- 0 , OFF-1
mysolver.setStressAveragingMethod(1) # 0 -volume 1- surface
#tangent averaging flag
mysolver.tangentAveragingFlag(True) # set tangent averaging ON -0, OFF -1
mysolver.setTangentAveragingMethod(1,1e-6) # 0- perturbation 1- condensation

#mysolver.setExtractPerturbationToFileFlag(0)	

# build view
mysolver.internalPointBuildView("Green-Lagrange_xx",IPField.STRAIN_XX, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_yy",IPField.STRAIN_YY, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_zz",IPField.STRAIN_ZZ, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_xy",IPField.STRAIN_XY, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_yz",IPField.STRAIN_YZ, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_xz",IPField.STRAIN_XZ, 1, 1);
mysolver.internalPointBuildView("sig_xx",IPField.SIG_XX, 1, 1);
mysolver.internalPointBuildView("sig_yy",IPField.SIG_YY, 1, 1);
mysolver.internalPointBuildView("sig_zz",IPField.SIG_ZZ, 1, 1);
mysolver.internalPointBuildView("sig_xy",IPField.SIG_XY, 1, 1);
mysolver.internalPointBuildView("sig_yz",IPField.SIG_YZ, 1, 1);
mysolver.internalPointBuildView("sig_xz",IPField.SIG_XZ, 1, 1);
mysolver.internalPointBuildView("sig_VM",IPField.SVM, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange equivalent strain",IPField.GL_EQUIVALENT_STRAIN, 1, 1);
mysolver.internalPointBuildView("Equivalent plastic strain",IPField.PLASTICSTRAIN, 1, 1);


# solve#
mysolver.solve()

# test check
check = TestCheck()
check.equal(6.116708e+01,mysolver.getHomogenizedStress(0,0),1.e-4)
check.equal(47.16363708493239,mysolver.getHomogenizedStress(1,0),1.e-4)
check.equal(179.7483769767168,mysolver.getHomogenizedStress(2,2),1.e-4)

check.equal(8202.953257568117,mysolver.getHomogenizedTangent(0,0,0,0),1.e-4)
check.equal(3.320061e+03,mysolver.getHomogenizedTangent(0,0,1,1),1.e-4)
check.equal(2.355687e+03,mysolver.getHomogenizedTangent(0,1,0,1),1.e-4)
check.equal(8984.924024054986,mysolver.getHomogenizedTangent(2,2,2,2),1.e-4)


