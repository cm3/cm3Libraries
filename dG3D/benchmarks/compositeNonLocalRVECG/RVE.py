#coding-Utf-8-*-
from gmshpy import *
from dG3Dpy import*
#from dG3DpyDebug import*

#script to launch composite problem with a python script

# material law
lawnonlocal = 1 # unique number of law
lawlinear = 2 # unique number of law
rho = 7850. # Bulk mass
length =  1.e-7


# geometry
geofile="RVE.geo" # name of mesh file
meshfile="RVE.msh" # name of mesh file
propertiesLC="lemaitreChaboche.i01"#"lemaitreChaboche.i01" #properties file
propertiesLin="linear.i01" #properties file

# solver
sol = 2  # Gmm=0 (default) Taucs=1 PETsc=2
soltype = 1 # StaticLinear=0 (default) StaticNonLinear=1
nstep = 1000   # number of step (used only if soltype=1)
ftime =1.   # Final time (used only if soltype=1)
tol=1.e-6   # relative tolerance for NR scheme (used only if soltype=1)
nstepArch=5 # Number of step between 2 archiving (used only if soltype=1)
fullDg =0
eqRatio =1.e6

#  compute solution and BC (given directly to the solver
# creation of law
law1 = NonLocalDamageDG3DMaterialLaw(lawnonlocal,rho,propertiesLC)
law2 = NonLocalDamageDG3DMaterialLaw(lawlinear,rho,propertiesLin)
law2.setNumberOfNonLocalVariables(1)

# creation of ElasticField
Mfield = 101 # number of the field (physical number of Mtrix)
Ffield = 102 # number of the field (physical number of Fiber)

space1 = 0 # function space (Lagrange=0)

Matixfield = dG3DDomain(1000,Mfield,space1,lawnonlocal,fullDg,3,1)
Fiberfield = dG3DDomain(1000,Ffield,space1,lawlinear,fullDg,3,1)
#Matixfield.setNonLocalStabilityParameters(beta1,True)
Matixfield.setNonLocalEqRatio(eqRatio)
#Fiberfield.setNonLocalStabilityParameters(beta1,True)
Fiberfield.setNonLocalEqRatio(eqRatio)

#myfield1.matrixByPerturbation(1,1e-8)

# creation of Solver
mysolver = nonLinearMechSolver(1000)
mysolver.createModel(geofile,meshfile,3,2)
#mysolver.loadModel(meshfile)
mysolver.addDomain(Matixfield)
mysolver.addDomain(Fiberfield)
mysolver.addMaterialLaw(law1)
mysolver.addMaterialLaw(law2)
mysolver.Scheme(soltype)
mysolver.Solver(sol)
mysolver.snlData(nstep,ftime,tol)
mysolver.stepBetweenArchiving(nstepArch)

# BC
# fixed face
mysolver.displacementBC("Face",1,0,0.)

mysolver.displacementBC("Face",2,1,0.)
mysolver.constraintBC("Face",6,1)

mysolver.displacementBC("Face",4,2,0.)
mysolver.constraintBC("Face",5,2)


# displacement
d1=0.000002;
#0.00002567;
#cyclicFunction1=cycleFunctionTime(ftime/5., d1, 3.*ftime/5., -d1,ftime, d1); 

#mysolver.displacementBC("Face",6,0,cyclicFunction1)
#mysolver.displacementBC("Face",7,0,cyclicFunction1)
#mysolver.displacementBC("Face",8,0,cyclicFunction1)
#mysolver.displacementBC("Face",9,0,cyclicFunction1)
#mysolver.displacementBC("Face",10,0,cyclicFunction1)

#mysolver.displacementBC("Face",3,0,d1)
d2=0.000025
d3=0.
cyclicFunction1=cycleFunctionTime(0.,0.,ftime/2., d2, ftime, d3); 

mysolver.displacementBC("Face",3,0,cyclicFunction1)

# needed resultes
mysolver.internalPointBuildView("svm",IPField.SVM, 1, 1)
mysolver.internalPointBuildView("sig_xx",IPField.SIG_XX, 1, 1)
mysolver.internalPointBuildView("sig_yy",IPField.SIG_YY, 1, 1)
mysolver.internalPointBuildView("sig_zz",IPField.SIG_ZZ, 1, 1)
mysolver.internalPointBuildView("sig_xy",IPField.SIG_XY, 1, 1)
mysolver.internalPointBuildView("sig_yz",IPField.SIG_YZ, 1, 1)
mysolver.internalPointBuildView("sig_xz",IPField.SIG_XZ, 1, 1)
mysolver.internalPointBuildView("damage",IPField.DAMAGE, 1, 1)
mysolver.internalPointBuildView("epl",IPField.PLASTICSTRAIN, 1, 1)
mysolver.archivingForceOnPhysicalGroup("Face", 1, 0)
mysolver.archivingForceOnPhysicalGroup("Face", 3, 0)
mysolver.archivingIPOnPhysicalGroup("Volume",Mfield, IPField.SVM,IPField.MIN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",Mfield, IPField.SVM,IPField.MAX_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",Mfield, IPField.SVM,IPField.MEAN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",Mfield, IPField.SIG_XX,IPField.MIN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",Mfield, IPField.SIG_XX,IPField.MAX_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",Mfield, IPField.SIG_XX,IPField.MEAN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",Mfield, IPField.SIG_YY,IPField.MIN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",Mfield, IPField.SIG_YY,IPField.MAX_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",Mfield, IPField.SIG_YY,IPField.MEAN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",Mfield, IPField.SIG_ZZ,IPField.MIN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",Mfield, IPField.SIG_ZZ,IPField.MAX_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",Mfield, IPField.SIG_ZZ,IPField.MEAN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",Mfield, IPField.SIG_XY,IPField.MIN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",Mfield, IPField.SIG_XY,IPField.MAX_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",Mfield, IPField.SIG_XY,IPField.MEAN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",Mfield, IPField.SIG_XZ,IPField.MIN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",Mfield, IPField.SIG_XZ,IPField.MAX_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",Mfield, IPField.SIG_XZ,IPField.MEAN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",Mfield, IPField.SIG_YZ,IPField.MIN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",Mfield, IPField.SIG_YZ,IPField.MAX_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",Mfield, IPField.SIG_YZ,IPField.MEAN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",Mfield, IPField.PLASTICSTRAIN,IPField.MIN_VALUE,nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",Mfield, IPField.PLASTICSTRAIN,IPField.MAX_VALUE,nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",Mfield, IPField.PLASTICSTRAIN,IPField.MEAN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",Mfield, IPField.DAMAGE,IPField.MIN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",Mfield, IPField.DAMAGE,IPField.MAX_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",Mfield, IPField.DAMAGE,IPField.MEAN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",Ffield, IPField.SVM,IPField.MIN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",Ffield, IPField.SVM,IPField.MAX_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",Ffield, IPField.SVM,IPField.MEAN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",Ffield, IPField.SIG_XX,IPField.MIN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",Ffield, IPField.SIG_XX,IPField.MAX_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",Ffield, IPField.SIG_XX,IPField.MEAN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",Ffield, IPField.SIG_YY,IPField.MIN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",Ffield, IPField.SIG_YY,IPField.MAX_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",Ffield, IPField.SIG_YY,IPField.MEAN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",Ffield, IPField.SIG_ZZ,IPField.MIN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",Ffield, IPField.SIG_ZZ,IPField.MAX_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",Ffield, IPField.SIG_ZZ,IPField.MEAN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",Ffield, IPField.SIG_XY,IPField.MIN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",Ffield, IPField.SIG_XY,IPField.MAX_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",Ffield, IPField.SIG_XY,IPField.MEAN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",Ffield, IPField.SIG_XZ,IPField.MIN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",Ffield, IPField.SIG_XZ,IPField.MAX_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",Ffield, IPField.SIG_XZ,IPField.MEAN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",Ffield, IPField.SIG_YZ,IPField.MIN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",Ffield, IPField.SIG_YZ,IPField.MAX_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",Ffield, IPField.SIG_YZ,IPField.MEAN_VALUE, nstepArch);
mysolver.solve()

check = TestCheck()
check.equal(7.118982e-01,mysolver.getArchivedForceOnPhysicalGroup("Face", 1, 0),1.e-6)

