#coding-Utf-8-*-
from gmshpy import *
from dG3Dpy import*
from math import*


lawnum1 = 11 # unique number of law
rho   = 7850
young = 205.4e3 #MPa
nu    = 0.3 
sy0   = 862.4 #MPa
h = 3399.
p1 = 0.012
n1 = 0.084
p2 = 0.056
n2 = 0.058

harden = LinearFollowedByMultiplePowerLawJ2IsotropicHardening(lawnum1,sy0,h,p1,n1,p2,n2)
law1   = J2LinearDG3DMaterialLaw(lawnum1,rho,young,nu,harden)
law1.setStrainOrder(9)


# geometry
geofile ="model.geo"
meshfile="model.msh" # name of mesh file

# solver
sol = 2  # Gmm=0 (default) Taucs=1 PETsc=2
soltype = 1 # StaticLinear=0 (default) StaticNonLinear=1
nstep = 30   # number of step (used only if soltype=1)
ftime =1.   # Final time (used only if soltype=1)
tol=1.e-6 # relative tolerance for NR scheme (used only if soltype=1)
nstepArch=1 # Number of step between 2 archiving (used only if soltype=1)



# creation of ElasticField
myfield1 = axisymmetricDG3DDomain(11,1,0,lawnum1,0,2,0)

# creation of Solver
mysolver = nonLinearMechSolver(1000)
#mysolver.loadModel(meshfile)
mysolver.createModel(geofile,meshfile,2,2)
mysolver.addDomain(myfield1)
mysolver.addMaterialLaw(law1)

mysolver.Scheme(soltype)
mysolver.Solver(sol)
mysolver.snlData(nstep,ftime,tol)
mysolver.stepBetweenArchiving(nstepArch)
mysolver.options("-ksp_type preonly -pc_type lu -pc_factor_mat_solver_type mumps")

# BC
mysolver.displacementBC("Face",1,1,0.)
mysolver.sameDisplacementBC("Edge",4,8,2)
mysolver.sameDisplacementBC("Edge",3,8,0)
mysolver.displacementBC("Edge",5,0,0.)
mysolver.displacementBC("Edge",2,2,0.)

method=0
mysolver.pathFollowing(True,method)
if method==0:
	mysolver.setPathFollowingIncrementAdaptation(True,6,1)
	mysolver.setPathFollowingControlType(0)
	mysolver.setPathFollowingCorrectionMethod(0)
	mysolver.setPathFollowingArcLengthStep(2e-1)
	mysolver.setBoundsOfPathFollowingArcLengthSteps(0,2.5);
elif method==1:
	mysolver.setPathFollowingIncrementAdaptation(False,5)
	mysolver.setPathFollowingLocalSteps(1e-1,1e-2)
	mysolver.setPathFollowingLocalIncrementType(1); 
	mysolver.setPathFollowingSwitchCriterion(0)


T = 3.
fact = (T-1./3.)/(T+2./3)
mysolver.pressureOnPhysicalGroupBC("Edge",4,-1e3,0.)
mysolver.pressureOnPhysicalGroupBC("Edge",3,-fact*1e3,0.)


mysolver.internalPointBuildView("P_xx",IPField.P_XX, 1, 1)
mysolver.internalPointBuildView("P_yy",IPField.P_YY, 1, 1)
mysolver.internalPointBuildView("P_zz",IPField.P_ZZ, 1, 1)
mysolver.internalPointBuildView("P_xz",IPField.P_XZ, 1, 1)
mysolver.internalPointBuildView("P_zx",IPField.P_ZX, 1, 1)

mysolver.internalPointBuildView("F_xx",IPField.F_XX, 1, 1)
mysolver.internalPointBuildView("F_yy",IPField.F_YY, 1, 1)
mysolver.internalPointBuildView("F_zz",IPField.F_ZZ, 1, 1)
mysolver.internalPointBuildView("F_xz",IPField.F_XZ, 1, 1)
mysolver.internalPointBuildView("F_zx",IPField.F_ZX, 1, 1)

mysolver.internalPointBuildView("svm",IPField.SVM, 1, 1)
mysolver.internalPointBuildView("sig_xx",IPField.SIG_XX, 1, 1)
mysolver.internalPointBuildView("sig_yy",IPField.SIG_YY, 1, 1)
mysolver.internalPointBuildView("sig_zz",IPField.SIG_ZZ, 1, 1)
mysolver.internalPointBuildView("sig_xy",IPField.SIG_XY, 1, 1)
mysolver.internalPointBuildView("sig_yz",IPField.SIG_YZ, 1, 1)
mysolver.internalPointBuildView("sig_xz",IPField.SIG_XZ, 1, 1)
mysolver.internalPointBuildView("epl",IPField.PLASTICSTRAIN, 1, 1)
mysolver.internalPointBuildView("STRESS_TRIAXIALITY",IPField.STRESS_TRIAXIALITY, 1, 1)
mysolver.internalPointBuildView("LODE_PARAMETER",IPField.LODE_PARAMETER, 1, 1)

mysolver.archivingForceOnPhysicalGroup("Edge",2, 2)
mysolver.archivingForceOnPhysicalGroup("Edge",3, 0)
mysolver.archivingNodeDisplacement(8,0)
mysolver.archivingNodeDisplacement(8,2)


mysolver.solve()

check = TestCheck()
check.equal(-1.061197e+04, mysolver.getArchivedForceOnPhysicalGroup("Edge", 2, 2),1.e-4)
