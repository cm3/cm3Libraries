unit = 1.;
W0 = 1.;
lambda0 = 1.;
f0 = 1e-3;
Chi0 = Exp((1./3.)*Log(1.5*f0*lambda0/W0));

Lr0 = 1*unit;
Lz0 = lambda0*Lr0;
Rr0 = Chi0*Lr0;
Rz0 = W0*Rr0;

f0 = 2*Rr0*Rr0*Rz0/(3*Lr0*Lr0*Lz0);
Printf("initial porosity = %e",f0);

//Mesh.RecombineAll = 1;

lsca = 0.07*unit;
Point(1) = {0,0,0,lsca};
Point(2) = {Rr0,0,0,0.05*lsca};
Point(3) = {Lr0,0,0,0.3*lsca};
Point(4) = {Lr0,0,Lz0,lsca};
Point(5) = {0,0,Lz0,lsca};
Point(6) = {0,0,Rz0,lsca*0.5};


//+
Line(1) = {3,1};
//+
Line(2) = {4,3};
//+
Line(3) = {5,4};
//+
Line(4) = {1,5};
//+
Curve Loop(1) = {4, 1, 2, 3};
//+
Plane Surface(1) = {1};
//+
Physical Surface(1) = {1};
//+
Physical Curve(2) = {1};
//+
Physical Curve(3) = {2};
//+
Physical Curve(4) = {3};
//+
Physical Curve(5) = {4};
//+
Physical Point(6) = {1};
//+
Physical Point(7) = {3};
//+
Physical Point(8) = {4};
//+
Physical Point(9) = {5};
