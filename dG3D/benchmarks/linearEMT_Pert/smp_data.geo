// Geometrical data for inductor model

cm = 1e-2; // Unit

pp  = "Input/10Geometric dimensions/0";
pp2 = "Input/10Geometric dimensions/1Shell radius/";
ppm = "Input/11Mesh control (Nbr of divisions)/";

DefineConstant[
   Flag_3Dmodel = 1
   Flag_boolean = 1
   Flag_Symmetry = {0, Choices{0="Full",1="Half",2="One fourth"},
					Name "Input/01Symmetry type", Highlight "Blue"},
   Flag_Infinity = {0, Choices{0,1},
					Name "Input/01Use shell transformation to infinity"}
];

close_menu = 1;
colorro  = "LightGrey";
colorpp = "Ivory";


// Smp
R = 2*cm;
H = 4*cm;

//Coil
RintCoil = 4.5*cm;
RextCoil = 7.5*cm;

DefineConstant[
  //Lx    = {9*cm, Name StrCat[pp, "0Coil length along x-axis [m]"], Highlight Str[colorpp]}
  Ly    = {9*cm, Name StrCat[pp, "0Coil length along y-axis [m]"], Highlight Str[colorpp]}
  //Lz    = {9*cm, Name StrCat[pp, "2Coil length along z-axis [m]"], Highlight Str[colorpp]}
  wcoil = {3*cm, Name StrCat[pp, "1Coil width [m]"], Highlight Str[colorpp]}
  RintCoil = {4.5*cm, Name StrCat[pp, "2Coil inner radius [m]"], Highlight Str[colorpp]}
  RextCoil = {7.5*cm, Name StrCat[pp, "3Coil outer radius [m]"], Highlight Str[colorpp]}
  R     = {2*cm, Name StrCat[pp, "4Sample radius [m]"], Highlight Str[colorpp]}
  H     = {2*cm, Name StrCat[pp, "4Sample height [m]"], Highlight Str[colorpp]}
];

// radious for surrounding air with transformation to infinity
If(Flag_Infinity==1)
  label_Rext = "1Outer [m]";
Else
  label_Rext = "1[m]";
EndIf

DefineConstant[
  Rint = {20*cm, Min 0.15, Max 0.9, Step 0.1, Name StrCat[pp2, "0Inner [m]"],
    Visible (Flag_Infinity==1), Highlight Str[colorpp] },
  Rext = {28*cm, Min Rint, Max 1, Step 0.1, Name StrCat[pp2, StrCat["1", label_Rext]],
    Visible 1, Highlight Str[colorpp] }
];

Val_Rint = Rint;
Val_Rext = Rext;


IA = 10;
Nw = 288;

sigma_al = 3.72e7 ; // conductivity of aluminum [S/m]
sigma_cu = 5.77e7  ; // conductivity of copper [S/m]

// ----------------------------------------------------
// Numbers for physical regions in .geo and .pro files
// ----------------------------------------------------

ECORE = 1000;
SKINECORE = 11110;
REFCORE = 11111;
REFCOREBOTTOM = 11115;
REFCOREPOINT = 11112;

COIL  = 2000;
SKINCOIL = 2222;
SKINCOIL_ = 2223;

SURF_ELEC0 = 2333;
SURF_ELEC1 = 2444;
CUTCOIL = 2555;

AIR    = 3000;
AIRINF = 3100;
AIRGAP = 3200;

// Lines and surfaces for boundary conditions
SURF_AIROUT = 3333;

AXIS_Y = 10000;
CUT_YZ = 11000;
CUT_XY = 12000;
