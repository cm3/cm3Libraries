#coding-Utf-8-*-

from gmshpy import *
from dG3Dpy import*
from math import*

#script to launch PBC problem with a python script

# material law
lawnum = 11 # unique number of law

E = 2.7E3 #MPa
nu = 0.3
K = E/3./(1.-2.*nu)	# Bulk mudulus
mu =E/2./(1.+nu)	  # Shear mudulus
rho = 7850e-9 # Bulk mass

sy0c = 100. #MPa
hc = 300.

sy0t = sy0c*0.78
ht = 300.


# creation of law
hardenc = LinearExponentialJ2IsotropicHardening(1, sy0c, hc, 0., 10.)
hardent = LinearExponentialJ2IsotropicHardening(2, sy0t, ht, 0., 10.)
hardenk = PolynomialKinematicHardening(3,1)
hardenk.setCoefficients(1,370.)

law1   = HyperViscoElastoPlasticPowerYieldDG3DMaterialLaw(lawnum,rho,E,nu,1e-6,False,1e-8)
law1.setCompressionHardening(hardenc)
law1.setTractionHardening(hardent)
law1.setKinematicHardening(hardenk)

law1.setVolumeCorrection(8.,6.,2.,8.,6.,2.)

law1.setStrainOrder(5)

law1.setYieldPowerFactor(3.5)
law1.setNonAssociatedFlow(True)
law1.nonAssociatedFlowRuleFactor(0.5)

law1.setViscoelasticMethod(0)
N = 4;
law1.setViscoElasticNumberOfElement(N)
law1.setViscoElasticData(1,1380,7202.)
law1.setViscoElasticData(2,1190,71.6)
law1.setViscoElasticData(3,195,0.73)
law1.setViscoElasticData(4,148,0.073)

eta = constantViscosityLaw(1,3e4)
law1.setViscosityEffect(eta,0.21)


# geometry
meshfile="cylinder.msh" # name of mesh file

fullDg = 0 #O = CG, 1 = DG
space1 = 0 # function space (Lagrange=0)
beta1  = 100
# creation of part Domain
nfield = 11 # number of the field (physical number of surface)

myfield1 = dG3DDomain(1000,nfield,space1,lawnum,fullDg,3)
#myfield1.matrixByPerturbation(1,1,1,1e-8)
myfield1.stabilityParameters(beta1)

# solver
sol = 2  # Gmm=0 (default) Taucs=1 PETsc=2
soltype =1 # StaticLinear=0 (default) StaticNonLinear=1
nstep = 20  # number of step (used only if soltype=1)
ftime =1.   # Final time (used only if soltype=1)
tol=1.e-8  # relative tolerance for NR scheme (used only if soltype=1)
nstepArch=1 # Number of step between 2 archiving (used only if soltype=1)



# creation of Solver
mysolver = nonLinearMechSolver(1000)
mysolver.loadModel(meshfile)
mysolver.addDomain(myfield1)
mysolver.addMaterialLaw(law1)
mysolver.Scheme(soltype)
mysolver.Solver(sol)
mysolver.snlData(nstep,ftime,tol)

mysolver.displacementBC('Face',1,2,0.)
mysolver.displacementBC('Face',3,1,0.)
mysolver.displacementBC('Face',4,0,0.)

mysolver.displacementBC('Face',1,0,0)
mysolver.displacementBC('Face',1,1,0)
mysolver.displacementBC('Face',2,2,1.)

# build view
mysolver.internalPointBuildView("Strain_xx",IPField.STRAIN_XX, 1, 1);
mysolver.internalPointBuildView("Strain_yy",IPField.STRAIN_YY, 1, 1);
mysolver.internalPointBuildView("Strain_zz",IPField.STRAIN_ZZ, 1, 1);
mysolver.internalPointBuildView("Strain_xy",IPField.STRAIN_XY, 1, 1);
mysolver.internalPointBuildView("Strain_yz",IPField.STRAIN_YZ, 1, 1);
mysolver.internalPointBuildView("Strain_xz",IPField.STRAIN_XZ, 1, 1);
mysolver.internalPointBuildView("sig_xx",IPField.SIG_XX, 1, 1);
mysolver.internalPointBuildView("sig_yy",IPField.SIG_YY, 1, 1);
mysolver.internalPointBuildView("sig_zz",IPField.SIG_ZZ, 1, 1);
mysolver.internalPointBuildView("sig_xy",IPField.SIG_XY, 1, 1);
mysolver.internalPointBuildView("sig_yz",IPField.SIG_YZ, 1, 1);
mysolver.internalPointBuildView("sig_xz",IPField.SIG_XZ, 1, 1);
mysolver.internalPointBuildView("sig_VM",IPField.SVM, 1, 1);
mysolver.internalPointBuildView("Equivalent plastic strain",IPField.PLASTICSTRAIN, 1, 1);
mysolver.internalPointBuildView("pression",IPField.PRESSION,1,1)
mysolver.internalPointBuildView("plastic possion ratio",IPField.PLASTIC_POISSON_RATIO,1,1)

#archiving
mysolver.archivingForceOnPhysicalGroup('Face',1,0)
mysolver.archivingForceOnPhysicalGroup('Face',1,1)
mysolver.archivingForceOnPhysicalGroup('Face',1,2)
mysolver.archivingNodeDisplacement(5,0,1)
mysolver.archivingNodeDisplacement(5,1,1)
mysolver.archivingNodeDisplacement(5,2,1)
# solve
mysolver.solve()

check = TestCheck()
check.equal(-5610.81279947672,mysolver.getArchivedForceOnPhysicalGroup("Face", 1, 2),1.e-4)
