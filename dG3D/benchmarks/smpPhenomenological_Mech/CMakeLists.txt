# test file

set(PYFILE SMP_mech.py)

set(FILES2DELETE 
  cube.msh
  *.csv
  disp*
  stress*
)

add_cm3python_test(${PYFILE} "${FILES2DELETE}")
