#coding-Utf-8-*-
from gmshpy import *
from dG3Dpy import*
#from dG3DpyDebug import*
#script to launch beam problem with a python script

# material law
lawnum   = 1 # unique number of law
rho      = 7850
properties = "property.i01"


# geometry
geofile="cube.geo" # name of mesh file
meshfile="cube.msh" # name of mesh file

# solver
sol = 2  # Gmm=0 (default) Taucs=1 PETsc=2
soltype = 1 # StaticLinear=0 (default) StaticNonLinear=1
nstep = 10000  # number of step (used only if soltype=1)

#fig 5-8
ftime =0.2 #*(2.)**0.5   #3.60e+006   # Final time (used only if soltype=1)
#fig 4
ftime =10. #*(2.)**0.5   #3.60e+006   # Final time (used only if soltype=1)


tol=1.e-6   # relative tolerance for NR scheme (used only if soltype=1)
nstepArch=10 # Number of step between 2 archiving (used only if soltype=1)
fullDg = 0 #O = CG, 1 = DG
space1 = 0 # function space (Lagrange=0)
d1=0.1

#  compute solution and BC (given directly to the solver
# creation of law
#law1 = NonLocalDamageDG3DMaterialLaw(lawnum,rho,properties)
law1 = NonLocalDamageDG3DMaterialLaw(lawnum,rho,properties)
# creation of ElasticField
nfield = 10 # number of the field (physical number of surface)
myfield1 = dG3DDomain(1000,nfield,space1,lawnum,fullDg,3)
myfield1.stabilityParameters(30.)
#myfield1.matrixByPerturbation(1,1,1,1e-8)
# creation of Solver
mysolver = nonLinearMechSolver(1000)
mysolver.createModel(geofile,meshfile,3,2)
#mysolver.loadModel(meshfile)
mysolver.addDomain(myfield1)
mysolver.addMaterialLaw(law1)
mysolver.Scheme(soltype)
mysolver.Solver(sol)
mysolver.snlData(nstep,ftime,tol)
mysolver.stepBetweenArchiving(nstepArch)
# BC
#shearing
#mysolver.displacementBC("Face",1234,0,0.)
#mysolver.displacementBC("Face",1234,1,0.)
#mysolver.displacementBC("Face",1234,2,0.)
#mysolver.displacementBC("Face",5678,1,0.00)
#mysolver.forceBC("Face",2376,2,10000)
#mysolver.forceBC("Face",4158,2,-10000)
#mysolver.forceBC("Face",5678,0,10000)
#tension along z
mysolver.displacementBC("Face",1234,2,0.)
mysolver.displacementBC("Face",1265,1,0.)
mysolver.displacementBC("Face",4158,0,0.)

#fig 5-8
#mysolver.displacementBC("Face",5678,2,0.)
#mysolver.displacementBC("Face",2376,0,d1/ftime)
#mysolver.displacementBC("Face",3487,1,-d1/ftime)

#fig 4
mysolver.displacementBC("Face",2376,0,d1/ftime)


mysolver.internalPointBuildView("svm",IPField.SVM, 1, 1)
mysolver.internalPointBuildView("sig_xx",IPField.SIG_XX, 1, 1)
mysolver.internalPointBuildView("sig_yy",IPField.SIG_YY, 1, 1)
mysolver.internalPointBuildView("sig_zz",IPField.SIG_ZZ, 1, 1)
mysolver.internalPointBuildView("sig_xy",IPField.SIG_XY, 1, 1)
mysolver.internalPointBuildView("sig_yz",IPField.SIG_YZ, 1, 1)
mysolver.internalPointBuildView("sig_xz",IPField.SIG_XZ, 1, 1)
mysolver.internalPointBuildView("epl",IPField.PLASTICSTRAIN, 1, 1)
mysolver.internalPointBuildView("eps_xx",IPField.STRAIN_XX, 1, 1)
mysolver.internalPointBuildView("eps_yy",IPField.STRAIN_YY, 1, 1)
mysolver.internalPointBuildView("eps_zz",IPField.STRAIN_ZZ, 1, 1)
mysolver.internalPointBuildView("eps_xy",IPField.STRAIN_XY, 1, 1)
mysolver.internalPointBuildView("eps_yz",IPField.STRAIN_YZ, 1, 1)
mysolver.internalPointBuildView("eps_xz",IPField.STRAIN_XZ, 1, 1)
 
mysolver.archivingIPOnPhysicalGroup("Volume",nfield, IPField.SIG_XX,IPField.MEAN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",nfield, IPField.STRAIN_XX,IPField.MEAN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",nfield, IPField.SIG_YY,IPField.MEAN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",nfield, IPField.STRAIN_YY,IPField.MEAN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",nfield, IPField.SIG_ZZ,IPField.MEAN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",nfield, IPField.STRAIN_ZZ,IPField.MEAN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",nfield, IPField.MTX_SVM,IPField.MEAN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",nfield, IPField.MTX_SIG_XX,IPField.MEAN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",nfield, IPField.MTX_SIG_YY,IPField.MEAN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",nfield, IPField.MTX_SIG_ZZ,IPField.MEAN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",nfield, IPField.MTX_SIG_XY,IPField.MEAN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",nfield, IPField.MTX_SIG_XZ,IPField.MEAN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",nfield, IPField.MTX_SIG_YZ,IPField.MEAN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",nfield, IPField.INC_SVM,IPField.MEAN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",nfield, IPField.INC_SIG_XX,IPField.MEAN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",nfield, IPField.INC_SIG_YY,IPField.MEAN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",nfield, IPField.INC_SIG_ZZ,IPField.MEAN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",nfield, IPField.INC_SIG_XY,IPField.MEAN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",nfield, IPField.INC_SIG_XZ,IPField.MEAN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",nfield, IPField.INC_SIG_YZ,IPField.MEAN_VALUE, nstepArch);




mysolver.archivingNodeDisplacement(8,2,nstepArch)
mysolver.archivingForceOnPhysicalGroup("Face", 1265, 1,nstepArch)
mysolver.archivingForceOnPhysicalGroup("Face", 4158, 0,nstepArch)

mysolver.solve()

check = TestCheck()
#check.equal( 6.172054e-01,mysolver.getArchivedForceOnPhysicalGroup("Face", 1265, 1),1.e-6)
check.equal(-5.875011e+02,mysolver.getArchivedForceOnPhysicalGroup("Face", 4158, 0),1.e-6)


