#coding-Utf-8-*-
from gmshpy import *
from dG3Dpy import*
from math import*
import numpy as np


# material law
lawnum1 = 11 # unique number of law
rho   = 7850e-9
young = 200e3 #MPa
nu    = 0.3 

sy0   = 300. #MPa
N = 0.2
p0 = 0.0
h = young/sy0
harden1 = SwiftJ2IsotropicHardening(lawnum1,sy0,h,N,p0)

test="Elastic"
if test == "J2":
  law1 = J2LinearDG3DMaterialLaw(lawnum1,rho,young,nu,harden1)
  law1.setOrderForLogExp(9)
  myfield1 = dG3DDomain(10,32,0,lawnum1,0,2,0)

elif test == "GTN":
  q1    = 1.5
  q2    = 1.0
  q3    = q1 
  fVinitial = 1e-3

  l = 100e-3 # mm
  cl = l*l
  lengthLaw1 = IsotropicCLengthLaw(lawnum1, cl)

  # coelascence
  lambda0 = 1.
  kappa = 1.5

  # material law creation
  law1 = NonLocalPorousCoupledWithMPSAndMSSDG3DMaterialLaw(lawnum1, young, nu, rho, q1,q2,q3, fVinitial, lambda0, kappa, harden1,lengthLaw1,1e-8,False,1e-8)
  law1.setOrderForLogExp(9)
  nlmethod = 2
  law1.setNonLocalMethod(nlmethod)
  law1.setSubStepping(True,5)
  law1.setYieldSurfaceExponent(20.)

  regChiFunc = twoVariableExponentialSaturationScalarFunction(0.9,0.975)

  voidEvoLawGrowth = SphericalVoidStateEvolutionLawWithDeviatoricPlasticDeformation(lawnum1,lambda0,kappa)
  voidEvoLawGrowth.setRegularizedFunction(regChiFunc)
  law1.setGrowthVoidEvolutionLaw(voidEvoLawGrowth)

  voidEvoLawNecking = SpheroidalVoidStateEvolutionLawWithAspectRatioEvolution(lawnum1,lambda0,1.,kappa)
  voidEvoLawNecking.setRegularizedFunction(regChiFunc)
  law1.setCoalescenceVoidEvolutionLaw(voidEvoLawNecking)

  rate = 5.
  voidEvoLawShear = VoidStateLinearEvolutionLaw(lawnum1,lambda0,kappa,rate)
  voidEvoLawShear.setRegularizedFunction(regChiFunc)
  law1.setShearVoidEvolutionLaw(voidEvoLawShear)

  law1.useI1J2J3Implementation(True)

  law1.useTwoYieldRegularization(True,20.)
  law1.setStressFormulation(0)

  # epsMc = 1.2
  law1.setShearFactor(1.0354)
  law1.setCoalescenceTolerance(1.)

  # creation of ElasticField
  fullDg = 0 #O = CG, 1 = DG
  space1 = 0 # function space (Lagrange=0)
  beta1  = 100

  myfield1 = dG3DDomain(10,32,0,lawnum1,fullDg,2,3)
  myfield1.stabilityParameters(100.)
  myfield1.setNonLocalStabilityParameters(100.,True)
  myfield1.usePrimaryShapeFunction(3)
  myfield1.usePrimaryShapeFunction(4)
  myfield1.usePrimaryShapeFunction(5)	
  #myfield1.setPlaneStressState(True)
    
elif test=="Elastic":
  law1 = dG3DLinearElasticMaterialLaw(lawnum1,rho,young,nu)
   #matrix
  myfield1 = dG3DDomain(10,32,0,lawnum1,0,2,0)


# geometry
meshfile = "model.msh"


# solver
sol = 2  # Gmm=0 (default) Taucs=1 PETsc=2
soltype = 1 # StaticLinear=0 (default) StaticNonLinear=1
nstep =  10   # number of step (used only if soltype=1)
ftime = 1. # Final time (used only if soltype=1)
tol=1.e-6   # relative tolerance for NR scheme (used only if soltype=1)
tolAbs = 1e-8
nstepArch=1 # Number of step between 2 archiving (used only if soltype=1)
 

# creation of Solver
mysolver = nonLinearMechSolver(1000)
mysolver.loadModel(meshfile)
mysolver.addDomain(myfield1)
mysolver.addMaterialLaw(law1)
mysolver.Scheme(soltype)
mysolver.Solver(sol)
mysolver.snlData(nstep,ftime,tol,tolAbs)
mysolver.stepBetweenArchiving(nstepArch)


# boundary conditions
mysolver.snlManageTimeStep(15,2,2.,100) # maximal 20 times
mysolver.displacementBC("Face",32,2,0.)

# K field boundary
KI = 100.e6 # Pa*sqrt(m)
def ux_Func(x,y,z,t, KI):
  r = np.sqrt(x**2 + y**2)
  theta = np.arctan2(y,x)
  E = 200e3 #MPa
  nu    = 0.3 
  kappa = 3-4*nu # plane strain
  return KI*t*((1+nu)/E)*sqrt(r/2./np.pi)*np.cos(theta/2.)*(kappa-1+2.*np.sin(theta/2)*np.sin(theta/2))
def uy_Func(x,y,z,t, KI):
  r = np.sqrt(x**2 + y**2)
  theta = np.arctan2(y,x)
  E = 200e3 #MPa
  nu    = 0.3 
  kappa = 3-4*nu # plane strain
  return KI*t*((1+nu)/E)*sqrt(r/2./np.pi)*np.sin(theta/2.)*(kappa+1-2.*np.cos(theta/2)*np.cos(theta/2))
  

func_x = PythonBCfunctionDouble(ux_Func, KI)
func_y = PythonBCfunctionDouble(uy_Func, KI)

mysolver.displacementBC("Edge",37,0, func_x)
mysolver.displacementBC("Edge",37,1, func_y)

mysolver.internalPointBuildView("F_xx",IPField.F_XX, 1, 1)
mysolver.internalPointBuildView("F_yy",IPField.F_YY, 1, 1)
mysolver.internalPointBuildView("F_zz",IPField.F_ZZ, 1, 1)
mysolver.internalPointBuildView("F_xz",IPField.F_XZ, 1, 1)
mysolver.internalPointBuildView("F_zx",IPField.F_ZX, 1, 1)
mysolver.internalPointBuildView("F_xy",IPField.F_XY, 1, 1)
mysolver.internalPointBuildView("F_yx",IPField.F_YX, 1, 1)
mysolver.internalPointBuildView("F_yz",IPField.F_YZ, 1, 1)
mysolver.internalPointBuildView("F_zy",IPField.F_ZY, 1, 1)

mysolver.internalPointBuildView("svm",IPField.SVM, 1, 1)
mysolver.internalPointBuildView("sig_1",IPField.FIRST_PRINCIPAL_STRESS, 1, 1)
mysolver.internalPointBuildView("sig_2",IPField.SECOND_PRINCIPAL_STRESS, 1, 1)
mysolver.internalPointBuildView("sig_3",IPField.THIRD_PRINCIPAL_STRESS, 1, 1)
mysolver.internalPointBuildView("sig_xx",IPField.SIG_XX, 1, 1)
mysolver.internalPointBuildView("sig_yy",IPField.SIG_YY, 1, 1)
mysolver.internalPointBuildView("sig_zz",IPField.SIG_ZZ, 1, 1)
mysolver.internalPointBuildView("sig_xz",IPField.SIG_XZ, 1, 1)
mysolver.internalPointBuildView("sig_yz",IPField.SIG_YZ, 1, 1)
mysolver.internalPointBuildView("sig_xy",IPField.SIG_XY, 1, 1)
mysolver.internalPointBuildView("epl",IPField.PLASTICSTRAIN, 1, 1)
mysolver.internalPointBuildView("DEFO_ENERGY",IPField.DEFO_ENERGY, 1, 1)
mysolver.internalPointBuildView("LODE_PARAMETER",IPField.LODE_PARAMETER, 1, 1)  
mysolver.internalPointBuildView("STRESS_TRIAXIALITY",IPField.STRESS_TRIAXIALITY, 1, 1)  
mysolver.internalPointBuildView("PRESSURE",IPField.PRESSION, 1, 1)  

mysolver.internalPointBuildView("NONLOCAL_POROSITY",IPField.NONLOCAL_POROSITY, 1, 1)
mysolver.internalPointBuildView("LOCAL_POROSITY",IPField.LOCAL_POROSITY, 1, 1)
mysolver.internalPointBuildView("CORRECTED_POROSITY",IPField.CORRECTED_POROSITY, 1, 1)
mysolver.internalPointBuildView("YIELD_POROSITY",IPField.YIELD_POROSITY, 1, 1)
mysolver.internalPointBuildView("CORRECTED_POROSITY_MAX",IPField.CORRECTED_POROSITY, 1, IPField.MAX_VALUE)

mysolver.internalPointBuildView("LOCAL_0",IPField.LOCAL_0, 1, 1)
mysolver.internalPointBuildView("LOCAL_1",IPField.LOCAL_1, 1, 1)
mysolver.internalPointBuildView("LOCAL_2",IPField.LOCAL_2, 1, 1)

mysolver.internalPointBuildView("NONLOCAL_0",IPField.NONLOCAL_0, 1, 1)
mysolver.internalPointBuildView("NONLOCAL_1",IPField.NONLOCAL_1, 1, 1)
mysolver.internalPointBuildView("NONLOCAL_2",IPField.NONLOCAL_2, 1, 1)


mysolver.internalPointBuildView("COALESCENCE",IPField.COALESCENCE, 1, 1)
mysolver.internalPointBuildView("chi",IPField.LIGAMENT_RATIO, 1, 1)
mysolver.internalPointBuildView("chi_max",IPField.LIGAMENT_RATIO, 1, IPField.MAX_VALUE)
mysolver.internalPointBuildView("ASPECT_RATIO",IPField.ASPECT_RATIO, 1, 1)
mysolver.internalPointBuildView("SHAPE_FACTOR",IPField.SHAPE_FACTOR, 1, 1)
mysolver.internalPointBuildView("LIGAMENT_RATIO_COALESCENCE_ONSET",IPField.LIGAMENT_RATIO_COALESCENCE_ONSET, 1, IPField.MAX_VALUE)
mysolver.internalPointBuildView("ASPECT_RATIO_COALESCENCE_ONSET",IPField.ASPECT_RATIO_COALESCENCE_ONSET, 1, IPField.MAX_VALUE)
mysolver.internalPointBuildView("SHAPE_FACTOR_COALESCENCE_ONSET",IPField.SHAPE_FACTOR_COALESCENCE_ONSET, 1, IPField.MAX_VALUE)


mysolver.archivingVolumeIntegralValue(IPField.PLASTIC_ENERGY,1)

# coalescence volume
filterCoales = GPFilterCoalescence("CoalesReferenceVolume_")
mysolver.archivingVolumeIntegralValue(IPField.ONE,1,filterCoales)

# plastic volume
plasticZone = GPFilterPlasticZone("PlasticZone_")
mysolver.archivingVolumeIntegralValue(IPField.ONE,1,plasticZone)

	
cases=[
        [[33],[38], [37]],
        [[34],[39], [38]],
        [[35],[40], [39]],
      ]

allComputeJs = []
for case in cases:
    volume = case[0]
    inner = case[1]
    outer = case[2]
    name = f"Contour{cases.index(case)}"
    computeJ = computeJIntegral(name)
    for phys in volume:
        computeJ.setIntegratedDomain(2,phys)
    for phys in inner:
        computeJ.setInnerBoundary(1,phys)   
    for phys in  outer:
        computeJ.setOuterBoundary(1,phys)
    allComputeJs.append(computeJ)

for compJ in allComputeJs:
    mysolver.computeJIntegralByDomainIntegration(compJ)	
mysolver.solve()


