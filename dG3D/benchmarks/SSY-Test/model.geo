unit = 1e-3; 
R0 =1*unit;
Rext = 150*unit;
R1 = 3*unit;
R2 = 5*unit;
R3 = 7*unit;

lsca = 0.1*unit;
lsca2 = Rext/30;
Point(1)={0,0,0,lsca};
Point(2)={-R0,0,0,lsca};
Point(3)={-R0,R0,0,lsca};
x4 = -R0 - Sqrt(R1*R1-R0*R0);
Point(4)={x4,R0,0,lsca2*0.115};
x5 = -R0 - Sqrt(R2*R2-R0*R0);
Point(5)={x5,R0,0,lsca2*0.125};
x6 = -R0 - Sqrt(R3*R3-R0*R0);
Point(6)={x6,R0,0,lsca2*0.25};
x7 = Sqrt(Rext*Rext-R0*R0);
Point(7)={-x7,R0,0,lsca2};
Point(8)={Rext,0,0,lsca2};
Point(9)={R1-R0,0,0,lsca2*0.115};
Point(10)={R2-R0,0,0,lsca2*0.125};
Point(11)={R3-R0,0,0,lsca2*0.25};

//+
Circle(1) = {8, 1, 7};
//+
Circle(2) = {11, 2, 6};
//+
Circle(3) = {10, 2, 5};
//+
Circle(4) = {9, 2, 4};
//+
Circle(5) = {1, 2, 3};
//+
Line(6) = {8, 11};
//+
Line(7) = {11, 10};
//+
Line(8) = {10, 9};
//+
Line(9) = {9, 1};
//+
Line(10) = {3, 4};
//+
Line(11) = {4, 5};
//+
Line(12) = {5, 6};
//+
Line(13) = {6, 7};
//+
Curve Loop(1) = {1, -13, -2, -6};
//+
Plane Surface(1) = {1};
//+
Curve Loop(2) = {2, -12, -3, -7};
//+
Plane Surface(2) = {2};
//+
Curve Loop(3) = {11, -3, 8, 4};
//+
Plane Surface(3) = {3};
//+
Curve Loop(4) = {9, 5, 10, -4};
//+
Plane Surface(4) = {4};
//+
Symmetry {0, 1, 0, 0} {
  Duplicata { Surface{4}; Surface{3}; Surface{2}; Surface{1}; }
}
//+
Physical Surface(32) = {14, 19, 24, 4, 29, 3, 2, 1};
//+
Physical Surface(33) = {1, 29};
//+
Physical Surface(34) = {2, 24};
//+
Physical Surface(35) = {3, 19};
//+
Physical Surface(36) = {4, 14};
//+
Physical Curve(37) = {1, 30};
//+
Physical Curve(38) = {2, 25};
//+
Physical Curve(39) = {3, 21};
//+
Physical Curve(40) = {4, 18};
//+
Physical Curve(41) = {10, 5, 16, 17};
