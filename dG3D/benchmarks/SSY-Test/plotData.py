import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

plt.figure()

E = 200e3 #MPa
nu    = 0.3 
Eprim = E/(1-nu*nu)  
Kapplied = 100

for case in ["JContour0","JContour1","JContour2"]:
  data = pd.read_csv(f"{case}.csv",sep=";")
  print(data)
  time = data["Time"].values*Kapplied
  JI = data["J0"].values
  KI = np.sqrt(JI*Eprim)*1e-6
  plt.plot(time, KI,label=case)
  
plt.plot([0,Kapplied*1.1],[0,Kapplied*1.1],"--",label="equal line")

plt.xlabel(r"KI applied value [$MPa\sqrt{m}$]")
plt.ylabel(r"KI by J-integral [$MPa\sqrt{m}$]")
plt.legend()

plt.show()
