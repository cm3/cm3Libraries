#coding-Utf-8-*-
from gmshpy import *
#from dG3DpyDebug import*
from dG3Dpy import*
import math,csv

#script to launch composite problem with a python script

# material law
lawnonlocal1 = 1 # unique number of law
lawnonlocal2 = 2 # unique number of law

lawmat1 = 3 # ply 45 deg
lawmat2 = 4 # ply -45 deg

lawcoh = 5

lawcoh1 = 6 # ply domain cohesive law
lawcoh2 = 7

lawfracno1 = 8
lawfracno2 = 9

rho = 1750. # Bulk mass
young = 40.e9
youngA = 230.e9
nu = 0.256 #nu_alp.bet
nu_minor = 0.256*young/youngA 
GA = 24.e9 

GcL      = 0.6*100.+0.4*78.;
sigmacL  = 0.6*3600.e6+0.4*83e6;

GcT      = 122.; #to confirm
sigmacT  = 40e6; #to confirm

beta     = 0.87;
mu       = -1.;  ##
fsmin    = 0.999999;
fsmax    = 1.000001;

lawnonlocalcoh1  = 10 # ply domain law
Gc   = 600.
GcII = 1200.
sigmac   = 10.e6
sigmacII = 10.e6
alphaGc = 1.
#fsmin = 0.95
#fsmax = 1.05

#longitudinal loading
#ply -45
Ax1       = math.cos(45); #direction of anisotropy
Ay1       = math.sin(-45);
Az1       = 0.;
#ply 45
Ax2       = math.cos(45);
Ay2       = math.sin(45);
Az2       = 0.;

# geometry
geofile="fullPlateHole.geo"
meshfile="fullPlateHole.msh" # name of mesh file
propertiesC1="properties_an1.i01" #properties file
propertiesC2="properties_an2.i01" #properties file

# solver
sol = 2  # Gmm=0 (default) Taucs=1 PETsc=2
soltype = 3 # StaticLinear=0 (default) StaticNonLinear=1 Multi=3
nstepImpl = 100 #500   # number of step (used only if soltype=1)
#nstepExpl = 2000 #1000


ftime =1.   # Final time (used only if soltype=1)
tol=1.e-4   # relative tolerance for NR scheme (used only if soltype=1)
absTol = 1.e-10    ##
nstepArch= 5 #1500 # Number of step between 2 archiving (used only if soltype=1)
#nstepArch= 2 #1500 # Number of step between 2 archiving (used only if soltype=1)

fulldg = True   # 1 use dg inside a domain
dgnl = True     # 1 use dg for non local epl inside a domain - only if fulldg
beta1 = 40.   # stability parameter for dg
dgnlinter = False # 1 use dg for non local epl between laminate
eqRatio = 1.e4   #### ???

#  compute solution and BC (given directly to the solver
# creation of law
law1 = NonLocalDamageDG3DMaterialLaw(lawnonlocal1,rho,propertiesC1)
law1.setUseBarF(True)
law2 = NonLocalDamageDG3DMaterialLaw(lawnonlocal2,rho,propertiesC2)
law2.setUseBarF(True)

lawTransIso1 = TransverseIsotropicDG3DMaterialLaw(lawmat1,rho,young,nu,youngA,GA,nu_minor,Ax1,Ay1,Az1)
lawTransIso2 = TransverseIsotropicDG3DMaterialLaw(lawmat2,rho,young,nu,youngA,GA,nu_minor,Ax2,Ay2,Az2)

lawIntra  = NonLocalDamageLinearCohesive3DLaw(lawcoh,400.,1.e7,0.2,beta,fsmin,fsmax)
lawCoh1 = TransverseIsotropicLinearCohesive3D(lawcoh1,GcL,sigmacL,GcT,sigmacT,beta,mu,Ax1,Ay1,Az1,fsmin,fsmax) 
#does not require preexistence of crack
#it can detect crack initiation
lawCoh2 = TransverseIsotropicLinearCohesive3D(lawcoh2,GcL,sigmacL,GcT,sigmacT,beta,mu,Ax2,Ay2,Az2,fsmin,fsmax)

lawFrac1 = FractureByCohesive3DLaw(lawfracno1,lawnonlocal1,lawcoh1)
lawFrac2 = FractureByCohesive3DLaw(lawfracno2,lawnonlocal2,lawcoh2)

lawInter  = DelaminationLinearCohesive3DLaw(lawnonlocalcoh1,Gc,GcII,sigmac,sigmacII,alphaGc,fsmin,fsmax)

# creation of ElasticField
nfield1 = 51 # number of the field (physical number of surface)
nfield2 = 52 

space1 = 0 # function space (Lagrange=0)

myfield1 = dG3DDomain(10,nfield1,space1,lawfracno1,fulldg,3,1)
myfield1.stabilityParameters(beta1)  #stabilty parameter on displacement
myfield1.setNonLocalStabilityParameters(beta1,dgnl) #stabilty parameter on non local epl
myfield1.setNonLocalEqRatio(eqRatio)
myfield2 = dG3DDomain(11,nfield2,space1,lawfracno2,fulldg,3,1)
myfield2.stabilityParameters(beta1)  #stabilty parameter on displacement
myfield2.setNonLocalStabilityParameters(beta1,dgnl) #stabilty parameter on non local epl
myfield2.setNonLocalEqRatio(eqRatio)

#in // we need to create the ininterdomain after adding the domains
myinterfield = interDomainBetween3D(12,myfield1,myfield2,lawnonlocalcoh1) #law = 0
myinterfield.stabilityParameters(beta1)
myinterfield.setNonLocalStabilityParameters(beta1,dgnlinter)  #interface not continous
myinterfield.setNonLocalEqRatio(eqRatio)

#myfield1.matrixByPerturbation(1,1e-8)
# creation of Solver
mysolver = nonLinearMechSolver(1000)
#mysolver.createModel(geofile,meshfile,3,1)
mysolver.loadModel(meshfile)
mysolver.addDomain(myfield1)
mysolver.addDomain(myfield2)
mysolver.addMaterialLaw(law1)
mysolver.addMaterialLaw(law2)

mysolver.addMaterialLaw(lawTransIso1)
mysolver.addMaterialLaw(lawTransIso2)

mysolver.addMaterialLaw(lawIntra)
mysolver.addMaterialLaw(lawCoh1)
mysolver.addMaterialLaw(lawCoh2)

mysolver.addMaterialLaw(lawFrac1)
mysolver.addMaterialLaw(lawFrac2)

mysolver.addDomain(myinterfield) # interface domain
mysolver.addMaterialLaw(lawInter)
mysolver.Scheme(soltype)
mysolver.Solver(sol)
#mysolver.lineSearch(1)
mysolver.snlData(nstepImpl,ftime,tol,absTol)
#mysolver.explicitSpectralRadius(ftime,0.5,0.)
#mysolver.explicitTimeStepEvaluation(nstepExpl)

mysolver.snlManageTimeStep(50, 4, 2, 10)

mysolver.stepBetweenArchiving(nstepArch)

#mysolver.pathFollowing(True,0)
#mysolver.setPathFollowingIncrementAdaptation(True,4)
#mysolver.setPathFollowingControlType(0) # 0 1 or 2
#mysolver.setPathFollowingCorrectionMethod(2) # 0 1 or 2
#mysolver.setPathFollowingArcLengthStep(1e-1)  ##To increase
#mysolver.setBoundsOfPathFollowingArcLengthSteps(0,1.);

# BC
#tension along x

d1=0.0005 #0.75
d1impl=0.7*d1 # 10% in static (no damage)

mysolver.displacementBC("Face",101,0,0.)
mysolver.displacementBC("Face",102,0,d1impl)
mysolver.displacementBC("Face",121,2,0.)
mysolver.displacementBC("Edge",31,1,0.)
mysolver.displacementBC("Edge",33,1,0.)
#mysolver.sameDisplacementBC("Face",102,23,0)
#mysolver.forceBC("Node",23,0,1e4)

selUpdate=cohesiveCrackCriticalIPUpdate()
mysolver.setSelectiveUpdate(selUpdate)


#mysolver.constraintBC("Face",112,1)

nstepArchIP = nstepArch
mysolver.internalPointBuildView("svm",IPField.SVM, 1, nstepArchIP)
mysolver.internalPointBuildView("sig_xx",IPField.SIG_XX, 1, nstepArchIP)
mysolver.internalPointBuildView("sig_yy",IPField.SIG_YY, 1, nstepArchIP)
mysolver.internalPointBuildView("sig_zz",IPField.SIG_ZZ, 1, nstepArchIP)
mysolver.internalPointBuildView("sig_xy",IPField.SIG_XY, 1, nstepArchIP)
mysolver.internalPointBuildView("sig_yz",IPField.SIG_YZ, 1, nstepArchIP)
mysolver.internalPointBuildView("sig_xz",IPField.SIG_XZ, 1, nstepArchIP)
mysolver.internalPointBuildView("eps_xx",IPField.STRAIN_XX, 1, nstepArchIP)
mysolver.internalPointBuildView("eps_yy",IPField.STRAIN_YY, 1, nstepArchIP)
mysolver.internalPointBuildView("eps_zz",IPField.STRAIN_ZZ, 1, nstepArchIP)
mysolver.internalPointBuildView("eps_xy",IPField.STRAIN_XY, 1, nstepArchIP)
mysolver.internalPointBuildView("eps_yz",IPField.STRAIN_YZ, 1, nstepArchIP)
mysolver.internalPointBuildView("eps_xz",IPField.STRAIN_XZ, 1, nstepArchIP)
mysolver.internalPointBuildView("damage",IPField.DAMAGE, 1, nstepArchIP)
mysolver.internalPointBuildView("epl",IPField.PLASTICSTRAIN, 1, nstepArchIP)
mysolver.archivingForceOnPhysicalGroup("Face", 101, 0, nstepArch)
mysolver.archivingForceOnPhysicalGroup("Face", 102, 0, nstepArch)

mysolver.archivingIPOnPhysicalGroup("Volume",nfield1, IPField.PLASTICSTRAIN,IPField.MIN_VALUE,nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",nfield1, IPField.PLASTICSTRAIN,IPField.MAX_VALUE,nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",nfield1, IPField.PLASTICSTRAIN,IPField.MEAN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",nfield1, IPField.DAMAGE,IPField.MIN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",nfield1, IPField.DAMAGE,IPField.MAX_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",nfield1, IPField.DAMAGE,IPField.MEAN_VALUE, nstepArch);

mysolver.archivingIPOnPhysicalGroup("Volume",nfield2, IPField.PLASTICSTRAIN,IPField.MIN_VALUE,nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",nfield2, IPField.PLASTICSTRAIN,IPField.MAX_VALUE,nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",nfield2, IPField.PLASTICSTRAIN,IPField.MEAN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",nfield2, IPField.DAMAGE,IPField.MIN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",nfield2, IPField.DAMAGE,IPField.MAX_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",nfield2, IPField.DAMAGE,IPField.MEAN_VALUE, nstepArch);

mysolver.archivingNodeIP(21, IPField.STRAIN_XX, IPField.MAX_VALUE, nstepArch);
mysolver.archivingNodeIP(21, IPField.STRAIN_YY, IPField.MAX_VALUE, nstepArch);
mysolver.archivingNodeIP(21, IPField.DAMAGE, IPField.MAX_VALUE, nstepArch);
mysolver.archivingNodeIP(23, IPField.STRAIN_XX, IPField.MAX_VALUE, nstepArch);
mysolver.archivingNodeIP(23, IPField.STRAIN_YY, IPField.MAX_VALUE, nstepArch);
mysolver.archivingNodeIP(23, IPField.DAMAGE, IPField.MAX_VALUE, nstepArch);


#mysolver.archivingNodeDisplacement(21,0)
#mysolver.archivingNodeDisplacement(23,0)

#t1 = mysolver.solve()

#check = TestCheck()
#check.equal(6.854727e+03,mysolver.getArchivedForceOnPhysicalGroup("Face", 102, 0),5.e-3)

# switch to explicit
nstepImpl = 100 # number of step (used only if soltype=1)
nstepExpl = 100 
oldarch = nstepArch
nstepArch= 100  
ftime = 4.e-6 #0.0006375 # 85% in explicit with a 4x factor on the loading rate #0.000375 # all exp ftime = 1.5e-3 div 4 here as only 1/4 in explicit
#mysolver.Scheme(3)

mysolver.snlData(nstepImpl,ftime,tol) #ns,endtime,reltol,abstol
mysolver.explicitSpectralRadius(ftime,0.75,0.5) #to add numerical damping
mysolver.explicitTimeStepEvaluation(nstepExpl)
mysolver.stepBetweenArchiving(nstepArch)
mysolver.setFactorOnArchivingFiles(int(nstepArch/oldarch))

d1expl = (d1 - d1impl)
functionDisp = simpleFunctionTimeWithConstantDouble(d1expl,True,1.,d1impl) ##
mysolver.resetBoundaryConditions()
mysolver.displacementBC("Face",101,0,0.)
mysolver.displacementBC("Face",102,0,functionDisp)
mysolver.displacementBC("Face",121,2,0.)
mysolver.displacementBC("Edge",31,1,0.)
mysolver.displacementBC("Edge",33,1,0.)
mysolver.createRestartBySteps(300);

mysolver.addSystem(3,2)
mysolver.addSystem(1,1)

mysolver.solve()

check = TestCheck()
check.equal(6.859167e+03,mysolver.getArchivedForceOnPhysicalGroup("Face", 102, 0),5.e-3)

data21 = csv.reader(open('force102comp0_part2.csv'), delimiter=';')
force2 = list(data21)
check.equal(1.235166e+03,float(force2[-1][1]),1e-3)


#mysolver.disableResetRestart() #only battery: the second solve mimics a restart
#mysolver.createRestartBySteps(300);
#mysolver.solve()

#check = TestCheck()
#check.equal(6.859188e+03,mysolver.getArchivedForceOnPhysicalGroup("Face", 102, 0),5.e-3)

