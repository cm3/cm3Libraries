import os,csv
import sys

def checkEqual(ref, cur, tol):
  if(abs(ref-cur)>abs(tol*ref)):
    print("Error : reference  value ",ref," current value ",cur," relative error ", (ref-cur)/ref," tolerance ",tol)


if sys.version_info[0] < 3:
  os.system('mpiexec -np 4 python fullPlateHole.py')
else:
  os.system('mpiexec -np 4 python3 fullPlateHole.py')

data1 = csv.reader(open('force102comp0_part2.csv'), delimiter=';')
force = list(data1)
checkEqual(1235.165748831123,float(force[-1][1]),1e-3)

if sys.version_info[0] < 3:
  os.system('mpiexec -np 4 python fullPlateHoleRes.py')
else:
  os.system('mpiexec -np 4 python3 fullPlateHoleRes.py')

data21 = csv.reader(open('force102comp0_part2.csv'), delimiter=';')
force2 = list(data21)
checkEqual(1235.165748831123,float(force2[-1][1]),1e-3)

