
mm=1.0e-3;
n=1;
L=10*mm;
sl1=0.4*L/n;
Lx = 2*L;
Ly = 4*L;
Point(1)={0,0,0,sl1};
Point(2)={Lx,0,0,sl1};
Point(3)={Lx,Ly,0,sl1};
Point(4)={0,Ly,0,sl1};
Line(1)={1,2};
Line(2)={2,3};
Line(3)={3,4};
Line(4)={4,1};
x0=0.5*Lx/n; y0=0.5*Ly/n; r=0.2*L/n; 

x=0.3*Lx;
y=0.4*Ly;
r = 0.2*L;
sl2=0.7*sl1;
p1=newp; Point(p1)={x-r,y,0,sl2};
p2=newp; Point(p2)={x,y+r,0,sl2};
p3=newp; Point(p3)={x+r,y,0,sl2};
p4=newp; Point(p4)={x,y-r,0,sl2};
pc=newp; Point(pc)={x,y,0,sl2};
c1 = newreg; Circle(c1) = {p1,pc,p2};
c2 = newreg; Circle(c2) = {p2,pc,p3};
c3 = newreg; Circle(c3) = {p3,pc,p4};
c4 = newreg; Circle(c4) = {p4,pc,p1};
l[1]=newreg; Line Loop(l[1]) = {c1,c2,c3,c4}; 


x=0.7*Lx;
y=0.6*Ly;
r = 0.2*L;
sl2=0.4*sl1;
p1=newp; Point(p1)={x-r,y,0,sl2};
p2=newp; Point(p2)={x,y+r,0,sl2};
p3=newp; Point(p3)={x+r,y,0,sl2};
p4=newp; Point(p4)={x,y-r,0,sl2};
pc=newp; Point(pc)={x,y,0,sl2};
c1 = newreg; Circle(c1) = {p1,pc,p2};
c2 = newreg; Circle(c2) = {p2,pc,p3};
c3 = newreg; Circle(c3) = {p3,pc,p4};
c4 = newreg; Circle(c4) = {p4,pc,p1};
l[2]=newreg; Line Loop(l[2]) = {c1,c2,c3,c4}; 


l[0]=newreg;
Line Loop(l[0])={1,2,3,4};
Plane Surface(11)={l[]};
//Recombine Surface{11};

Physical Surface(16) = {11};
Physical Line(17) = {1};
Physical Line(18) = {3};
Physical Point(19) = {4};
Physical Point(20) = {1};
//+
//Recombine Surface {11};
//+
Extrude {0, 0, 1e-3} {
  Surface{11}; Layers{2}; //Recombine;
}

//+
Physical Volume(83) = {1};
//+
Physical Surface(84) = {37};
//+
Physical Surface(85) = {45};
