import matplotlib.pyplot as plt
import pandas as pd
import os
import numpy as np

plt.rcParams.update({'font.size': 14})
plt.close("all")




for step in range(1,7):
  fig, axes = plt.subplots(1,2)
  all_files = [each for each in os.listdir(".") if "Tracking_GPFilterIPValueBounds" in each if f"step{step}" in each]
  
  for ff in all_files:
    data = pd.read_csv(ff,sep=";")
    if not(data.empty):
      axes[0].plot(data["xRef"].values,data["yRef"].values,"r.")
      axes[0].plot(data["xCur"].values,data["yCur"].values,"g.")
      axes[1].plot(data["xCur"].values,data["yCur"].values,"r.")
      
 
plt.show()
