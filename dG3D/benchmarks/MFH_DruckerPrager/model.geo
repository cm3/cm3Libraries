unit = 0.01 ; // NonLocalLength (in mm)
r = 0.2*unit;  
L = 10.*unit;
Lhalf = 0.5*L;

epsilon = 1e-3;
R = (Lhalf*Lhalf)/(2*r*epsilon);
Printf("R/L = %g",R/2./Lhalf);

lsca = 0.1*r;
Point(1) = {0,0,0,lsca};
Point(2) = {Lhalf,0,0,lsca};
Point(3) = {Lhalf,r+R-Sqrt(R*R-Lhalf*Lhalf),0,lsca};
Point(4) = {0,r,0,lsca};
Point(5) = {0,r+R,0,lsca};

//+
Line(1) = {4, 1};
//+
Line(2) = {1, 2};
//+
Line(3) = {2, 3};
//+
Circle(4) = {3, 5, 4};
//+
Line Loop(1) = {2, 3, 4, 1};
//+
Plane Surface(1) = {1};

//+
//Extrude {0, 0, Lhalf} {
//  Line{3};
//}
//+
Physical Surface(11) = {1};
//+
Physical Line(1) = {1};
//+
Physical Line(2) = {2};
//+
Physical Line(3) = {3};
//+
Physical Line(4) = {4};
//+
Physical Point(11) = {1};
//+
Physical Point(12) = {2};
//+
Physical Point(13) = {3};
//+
Physical Point(14) = {4};
//+
//+
Transfinite Line {1, 3} = 2 Using Progression 1;
//+
Transfinite Line {2,-4} = 5 Using Progression 1.045;
//+
Transfinite Surface {1} ;
//+
Recombine Surface {1} ;
//+
