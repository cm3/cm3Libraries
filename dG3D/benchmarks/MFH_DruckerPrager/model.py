#coding-Utf-8-*-
from gmshpy import *
from dG3Dpy import*
import numpy as np
import math
import csv

#nstep = int(np.loadtxt("steps.txt", comments='#', delimiter=None))
nstep = 300


#script to launch beam problem with a python script

# material law
lawnum = 11 # unique number of law
properties = "prop_comp.i01"

rho = 1.3e-9 # Bulk mass  

law2 = NonLocalDamageDG3DMaterialLaw(lawnum,rho,properties)

# geometry
geofile  = "model.geo"
meshfile = "model.msh"

# solver
sol = 2  # Gmm=0 (default) Taucs=1 PETsc=2
soltype = 1 # StaticLinear=0 (default) StaticNonLinear=1
ftime = 6e2   # Final time (used only if soltype=1)
tol=1.8e-6   # relative tolerance for NR scheme (used only if soltype=1)
nstepArch=4 # Number of step between 2 archiving (used only if soltype=1)
fullDg = 0 #O = CG, 1 = DG
space1 = 0 # function space (Lagrange=0)
beta1  = 100

  
# creation of ElasticField
#matrix
myfield1 = dG3DDomain(11,11,0,lawnum,0,2,1)

# creation of Solver
mysolver = nonLinearMechSolver(1000)
mysolver.createModel(geofile,meshfile,2,2)
mysolver.addDomain(myfield1)
mysolver.addMaterialLaw(law2)
mysolver.Scheme(soltype)
mysolver.Solver(sol)
mysolver.snlData(nstep,ftime,tol,tol/1000.)
mysolver.stepBetweenArchiving(nstepArch)

mysolver.snlManageTimeStep(20,2,2.,150) # maximal 20 times

########################## BC BC BC BC BC ##########################

mysolver.displacementBC("Face",11,2,0.)
mysolver.displacementBC("Edge",1,0,0.)
mysolver.displacementBC("Edge",2,1,0.)




pf= True
method=0
mysolver.pathFollowing(pf,method)
if method==0:
	mysolver.setPathFollowingIncrementAdaptation(True,4,0.3)
	mysolver.setPathFollowingControlType(0)
	mysolver.setPathFollowingCorrectionMethod(0)
	mysolver.setPathFollowingArcLengthStep(1e-1)
	mysolver.setBoundsOfPathFollowingArcLengthSteps(0,2.5);
elif method==1:
	mysolver.setPathFollowingIncrementAdaptationLocal(False,5,7)
	mysolver.setPathFollowingLocalSteps(8e-3,0.5e-9) 
	mysolver.setPathFollowingLocalIncrementType(3); 
	mysolver.setPathFollowingSwitchCriterion(1e-3)
	mysolver.setBoundsOfPathFollowingLocalSteps(1e-13,4.)

if pf:
	mysolver.sameDisplacementBC("Edge",3,12,0)
	mysolver.forceBC("Node",12,0,18.e-2)
else:
	L = 20e-3
	mysolver.displacementBC("Edge",3,2,1.14e-3*L)

stopCri = EndSchemeMonitoringWithZeroForceOnPhysical(0.2,"Edge",1,0)
mysolver.endSchemeMonitoring(stopCri)

mysolver.internalPointBuildView("P_xx",IPField.P_XX, 1, 1)
mysolver.internalPointBuildView("P_yy",IPField.P_YY, 1, 1)
mysolver.internalPointBuildView("P_zz",IPField.P_ZZ, 1, 1)
mysolver.internalPointBuildView("P_xz",IPField.P_XZ, 1, 1)
mysolver.internalPointBuildView("P_zx",IPField.P_ZX, 1, 1)

mysolver.internalPointBuildView("F_xx",IPField.F_XX, 1, 1)
mysolver.internalPointBuildView("F_yy",IPField.F_YY, 1, 1)
mysolver.internalPointBuildView("F_zz",IPField.F_ZZ, 1, 1)
mysolver.internalPointBuildView("F_xz",IPField.F_XZ, 1, 1)
mysolver.internalPointBuildView("F_zx",IPField.F_ZX, 1, 1)


mysolver.archivingForceOnPhysicalGroup("Edge", 1, 0)
mysolver.archivingNodeDisplacement(12,0)
mysolver.archivingNodeDisplacement(14,1)

mysolver.archivingAverageValue(IPField.P_XY)
mysolver.archivingAverageValue(IPField.P_YY)
mysolver.archivingAverageValue(IPField.P_XX)

mysolver.archivingAverageValue(IPField.P_XY)
mysolver.archivingAverageValue(IPField.P_YX)
mysolver.archivingAverageValue(IPField.P_ZX)
mysolver.archivingAverageValue(IPField.P_XZ)
mysolver.archivingAverageValue(IPField.P_YZ)
mysolver.archivingAverageValue(IPField.P_ZY)

mysolver.archivingAverageValue(IPField.F_ZZ)
mysolver.archivingAverageValue(IPField.F_YY)
mysolver.archivingAverageValue(IPField.F_XX)

mysolver.archivingAverageValue(IPField.F_XY)
mysolver.archivingAverageValue(IPField.F_YX)
mysolver.archivingAverageValue(IPField.F_ZX)
mysolver.archivingAverageValue(IPField.F_XZ)
mysolver.archivingAverageValue(IPField.F_YZ)
mysolver.archivingAverageValue(IPField.F_ZY)

mysolver.solve()

reader1 = csv.reader(open("Average_P_XX.csv", "r"), delimiter=";")
x1 = list(reader1)
x2 = [0] * len(x1)
for i in range (len(x1)):
	x2[i] = x1[i][1]
x2=[float(i) for i in x2]
Max = max(x2)


check = TestCheck()
check.equal(2.113337e+02,Max,1.e-6)
