import matplotlib.pyplot as plt
import pandas as pd

plt.figure()

u = pd.read_csv("previousScheme_first/NodalDisplacementPhysical19Num4comp1_part2.csv",sep=";", header=None)
f = pd.read_csv("previousScheme_first/force84comp1_part0.csv",sep=";", header=None)

plt.plot(u.values[:,1],-f.values[:,1],"r.", label="first solve")

u = pd.read_csv("previousScheme_second/NodalDisplacementPhysical19Num4comp1_part2.csv",sep=";", header=None)
f = pd.read_csv("previousScheme_second/force84comp1_part0.csv",sep=";", header=None)

plt.plot(u.values[:,1],-f.values[:,1],"g.", label="second solve")

u = pd.read_csv("previousScheme/NodalDisplacementPhysical19Num4comp1_part2.csv",sep=";", header=None)
f = pd.read_csv("previousScheme/force84comp1_part0.csv",sep=";", header=None)

plt.plot(u.values[:,1],-f.values[:,1],"b.", label="third solve")

u = pd.read_csv("NodalDisplacementPhysical19Num4comp1_part2.csv",sep=";", header=None)
f = pd.read_csv("force84comp1_part0.csv",sep=";", header=None)

plt.plot(u.values[:,1],-f.values[:,1],"k.", label="fourth solve")

plt.xlabel("disp")
plt.ylabel("force")

plt.legend()
plt.show()
