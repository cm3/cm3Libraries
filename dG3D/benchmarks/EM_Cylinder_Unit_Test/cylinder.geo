//+
SetFactory("OpenCASCADE");

Cylinder(1) = {0, 0, 0, 0.5, 0, 0, 0.025, 2*Pi};
Cylinder(2) = {0, 0, 0, 0.5, 0, 0, 0.5, 2*Pi};
//+
BooleanFragments{ Volume{1}; Delete; }{ Volume{2}; Delete;}
//+
Physical Volume(10) = {1};
Physical Volume(20) = {2};
//+
Physical Surface(3) = {2};
//+
Physical Surface(4) = {3};
//+
Physical Surface(5) = {5};
//+
Physical Surface(6) = {6};
//+
Physical Surface(7) = {1};
//+
Physical Surface(8) = {4};
//+
//Physical Surface(9) = {2,3,4,5,6};
Transfinite Curve {3, 1} = 10 Using Progression 1;
//+
Transfinite Curve {6, 4} = 15 Using Progression 1;
//+
Transfinite Curve {2} = 20 Using Progression 1;
