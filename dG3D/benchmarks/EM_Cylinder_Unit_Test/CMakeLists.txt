# test file

set(PYFILE cyl_LinearElecMag.py)

set(FILES2DELETE 
  *.msh
  *.csv
)

add_cm3python_test(${PYFILE} "${FILES2DELETE}")
