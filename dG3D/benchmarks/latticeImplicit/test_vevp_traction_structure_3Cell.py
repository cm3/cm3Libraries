#coding-Utf-8-*-
from gmshpy import *
from dG3Dpy import*
# import functions from VEVP.py
#from VEVP import *


# geometry ===================================================================
# Input .msh
meshfile = 'Cell3_5.msh'#'10_128_03_sturcture_CL.msh'
geofile = 'Cell3_5.geo'#"10_128_03_sturcture_CL.geo" # name of mesh file
mysolver = nonLinearMechSolver(1002)
# mysolver.createModel(geofile,meshfile,3,2)

# geometry ===================================================================

# material law data ==========================================================
VEVP = True # "Compression"
Ec = 1195.14

nu = 0.276
Nt = 7  #the number of branches in the generalized Maxwell model 
aP = 1.0 

if(VEVP):
  sy0c = 35.95#MPa, compressive yield stress
  hc = 0.55
  hc2 = 0.3
  kc = 6938.03

  sy0t = 0.29*35.95 #MPa, compressive yield stress
  ht = 0.032
  ht2 = 0.0195
  kt = 590.01


 #Saturation law
  l = 200e-3
  cl = l*l
  H = 0.
  Dinf = 0.
  pi = 0.1


  #Failure law
  pf = 0.025#0.117 #0.15 #0.117
  af = 0.08#0.55
  bf = 7.5#0.55
  cf = 0.07

   
  alpha = 1.82
  beta = 1.5*0.076
  eta = 3037.09
  p = 0.115
    
  #=============================================    
  damlaw1 = SimpleSaturateDamageLaw(1, H, Dinf,pi,alpha)

  fct = linearScalarFunction(0,cl,-cl/(Dinf+0.001))
  cl1     = GeneralVariableIsotropicCLengthLaw(1, fct)


  damlaw2 = PowerBrittleDamagelaw(2,0,pf,1.,0.3)
  damlaw2.setCriticalDamage(0.9)
  cl2     = IsotropicCLengthLaw(2, cl)
  #=============================================    
  lawnum = 11
  rho = 1000.e-12
  double_tol = 1.e-4
  hardenc = LinearExponentialJ2IsotropicHardening(1, sy0c, hc, hc2, kc)
  hardent = LinearExponentialJ2IsotropicHardening(2, sy0t, ht, ht2, kt)
  hardenk = PolynomialKinematicHardening(3,3)
  hardenk.setCoefficients(1,0) #polynomial coefficients
  hardenk.setCoefficients(2,0)
  hardenk.setCoefficients(3,0)

  law1   = NonLocalDamagePowerYieldHyperDG3DMaterialLawWithFailure(lawnum,rho,Ec,nu,hardenc, hardent,cl1,cl2,damlaw1,damlaw2,double_tol)
  law1.setNLDensity(0.)

  law1.setViscoelasticMethod(0)
  law1.setViscoElasticNumberOfElement(Nt)
  G = [26.75, 71.53, 8.26, 10.73, 145.03, 6, 1655.17, 4.61]
  K = [1134.86, 1.25, 265.16, 58.64, 2605.61, 26.1, 3849.62, 2469.14]
  g1 = 2.83
  k1 = 14.09

  for i in range(1,Nt+1):
    law1.setViscoElasticData_Bulk(i, K[i-1], k1*10**(-(i-1)))
    law1.setViscoElasticData_Shear(i, G[i-1], g1*10**(-(i-1)))
  law1.setStrainOrder(5) 
  #-----------------------------------------------------------------------------
if(VEVP):  
  law1.setYieldPowerFactor(alpha)
  law1.setNonAssociatedFlow(True)
  law1.nonAssociatedFlowRuleFactor(beta)
  etac = constantViscosityLaw(1,eta)
  law1.setViscosityEffect(etac,p)
# data law def ============================================================
  failureCr = CritialPlasticDeformationCriteration(1,af,bf,cf)
  law1.setFailureCriterion(failureCr)


# solver info ============================================================
sol = 2  # Gmm=0 (default) Taucs=1 PETsc=2
soltype = 4 # StaticLinear=0 (default) StaticNonLinear=1
nstep =500 # 20000#2000000   # number of step
ftime =9.e-3 #10#400.  # Final time 
nstepArch =10 # 200 # Number of step between 2 archiving (used only if soltype=1)
tol = 1.e-4#1e-4
fullDg = 0 #O = CG, 1 = DG
space1 = 0 # function space (Lagrange=0)
# solver info ============================================================

# creation of ElasticField ==============================================================
nfield = 51 # number of the field (physical number of volume in 3D)
myfield1 = dG3DDomain(1002,nfield,space1,lawnum,fullDg,3,2)
beta1 = 1e2
myfield1.stabilityParameters(beta1)
myfield1.setNonLocalStabilityParameters(beta1,True)
myfield1.setNonLocalEqRatio(1000.)
myfield1.usePrimaryShapeFunction(3)
myfield1.usePrimaryShapeFunction(4)
# creation of ElasticField ==============================================================

# Solver ==============================================================
# mysolver = nonLinearMechSolver(1000)
mysolver.createModel(geofile,meshfile,3,2)
#mysolver.loadModel(meshfile)
mysolver.addDomain(myfield1)
mysolver.addMaterialLaw(law1)
mysolver.Scheme(soltype)
mysolver.Solver(sol)
mysolver.snlData(nstep,ftime,tol,tol/10.)
#mysolver.stepBetweenArchiving(nstepArch)
mysolver.options("-ksp_type preonly -pc_type lu -pc_factor_mat_solver_type mumps")
mysolver.implicitSpectralRadius(0.05)
mysolver.explicitSpectralRadius(ftime,0.3,0.01)
mysolver.explicitTimeStepEvaluation(100)

#mysolver.setNumStepTimeInterval(1.3,40)
#mysolver.setNumStepTimeInterval(3.2,200)
#mysolver.setNumStepTimeInterval(4.2,10000)
#mysolver.setNumStepTimeInterval(ftime,4000)
mysolver.stepBetweenArchiving(nstepArch)
mysolver.snlManageTimeStep(25,5,2,10)
# Solver ==============================================================

# BC ==============================================================
# blocked in z bottom
mysolver.displacementBC("Face",120,2,0.)
# blocked in y side
mysolver.displacementBC("Face",110,1,0.)
# blocked in x side
mysolver.displacementBC("Face",100,0,0.)
# activated in z top with displ function
disp=cycleFunctionTime(0., 0.0, ftime,-2.835/5.)#5.0
mysolver.displacementBC("Face",121,2,disp)
#mysolver.displacementBC("Face",121,0,0.)
#mysolver.displacementBC("Face",121,1,0.)

mysolver.displacementBC("Volume",nfield,3,0.) #no saturation damage
# BC ==============================================================

# save
mysolver.internalPointBuildView("svm",IPField.SVM, 1, 1)
# save stress tensor
mysolver.internalPointBuildView("sig_xx",IPField.SIG_XX, 1, 1)
mysolver.internalPointBuildView("sig_yy",IPField.SIG_YY, 1, 1)
mysolver.internalPointBuildView("sig_zz",IPField.SIG_ZZ, 1, 1)
mysolver.internalPointBuildView("sig_xy",IPField.SIG_XY, 1, 1)
mysolver.internalPointBuildView("sig_yz",IPField.SIG_YZ, 1, 1)
mysolver.internalPointBuildView("sig_xz",IPField.SIG_XZ, 1, 1)
# save platic strain
mysolver.internalPointBuildView("epl",IPField.PLASTICSTRAIN, 1, 1)
# save top nodal force top surface (121)
mysolver.archivingForceOnPhysicalGroup("Face", 121, 2)
mysolver.internalPointBuildView("Damage",IPField.DAMAGE,1,1)

mysolver.internalPointBuildView("failure onset",IPField.FAILURE_ONSET,1,1)

mysolver.internalPointBuildView("nonlocal Plastic strain",IPField.NONLOCAL_PLASTICSTRAIN,1,1)

 

mysolver.internalPointBuildView("Saturation Damage",IPField.DAMAGE_0,1,1)

mysolver.internalPointBuildView("Failure Damage",IPField.DAMAGE_1,1,1)

mysolver.internalPointBuildView("Nonlocal Failure plasticity",IPField.NONLOCAL_FAILURE_PLASTICSTRAIN,1,1)
mysolver.solve()

check = TestCheck()
check.equal(-5.254615e+01,mysolver.getArchivedForceOnPhysicalGroup("Face", 121, 2),1.e-6)

