#coding-Utf-8-*-
from gmshpy import *
from dG3Dpy import*

#script to launch PBC problem with a python script

#DEFINE MICRO PROBLEM

# micro-material law
lawnum1 = 11 # unique number of law

rho   = 7850
young = 28.9e3
nu    = 0.3 
sy0   = 150
h     = 73
hexp  = 60.
 

harden = ExponentialJ2IsotropicHardening(1,sy0, h, hexp)
cl     = IsotropicCLengthLaw(1, 2.)
damlaw = SimpleSaturateDamageLaw(1, 50.,1.,0.,1.)
macromat1   = NonLocalDamageJ2HyperDG3DMaterialLaw(lawnum1,rho,young,nu,harden,cl,damlaw)


macromeshfile="model.msh" # name of mesh file

# solver
sol = 2  # Gmm=0 (default) Taucs=1 PETsc=2
soltype =1 # StaticLinear=0 (default) StaticNonLinear=1
nstep = 50  # number of step (used only if soltype=1)
ftime =1.   # Final time (used only if soltype=1)
tol=1.e-5  # relative tolerance for NR scheme (used only if soltype=1)
nstepArch=1 # Number of step between 2 archiving (used only if soltype=1)


# creation of  macro part Domain

dim =3
beta1 = 50;
fullDG = False;
space1 = 0

# non DG domain
nfield1 = 11
macrodomain1 = dG3DDomain(10,nfield1,space1,lawnum1,fullDG,dim,1)
macrodomain1.stabilityParameters(beta1)
macrodomain1.setNonLocalStabilityParameters(beta1,True)
macrodomain1.setNonLocalEqRatio(1.)


# creation of Solver
mysolver = nonLinearMechSolver(1000)
mysolver.loadModel(macromeshfile)

mysolver.addDomain(macrodomain1)
mysolver.addMaterialLaw(macromat1)
mysolver.Scheme(soltype)
mysolver.Solver(sol)
mysolver.snlData(nstep,ftime,tol)
mysolver.options("-ksp_type preonly -pc_type lu -pc_factor_mat_solver_package petsc")

# boundary condition
mysolver.displacementBC("Face",300,2,0.0)

mysolver.displacementBC("Face",200,1,0.0)
mysolver.displacementBC("Face",100,0,0.0)

mysolver.displacementBC("Face",301,2,2.e-1)


# archivage
mysolver.internalPointBuildView("Green-Lagrange_xx",IPField.STRAIN_XX, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_yy",IPField.STRAIN_YY, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_zz",IPField.STRAIN_ZZ, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_xy",IPField.STRAIN_XY, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_yz",IPField.STRAIN_YZ, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_xz",IPField.STRAIN_XZ, 1, 1);
mysolver.internalPointBuildView("sig_xx",IPField.SIG_XX, 1, 1);
mysolver.internalPointBuildView("sig_yy",IPField.SIG_YY, 1, 1);
mysolver.internalPointBuildView("sig_zz",IPField.SIG_ZZ, 1, 1);
mysolver.internalPointBuildView("sig_xy",IPField.SIG_XY, 1, 1);
mysolver.internalPointBuildView("sig_yz",IPField.SIG_YZ, 1, 1);
mysolver.internalPointBuildView("sig_xz",IPField.SIG_XZ, 1, 1);
mysolver.internalPointBuildView("sig_VM",IPField.SVM, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange equivalent strain",IPField.GL_EQUIVALENT_STRAIN, 1, 1);
mysolver.internalPointBuildView("Equivalent plastic strain",IPField.PLASTICSTRAIN, 1, 1);

mysolver.internalPointBuildView("Deformation energy",IPField.DEFO_ENERGY, 1, 1);
mysolver.internalPointBuildView("Plastic energy",IPField.PLASTIC_ENERGY, 1, 1);

mysolver.archivingForceOnPhysicalGroup("Face",300,2)
mysolver.archivingNodeDisplacement(5,2)

# solve
mysolver.solve()

check = TestCheck()
check.equal( -3.597708e+02,mysolver.getArchivedForceOnPhysicalGroup("Edge", 1, 1),1.e-4)

