#coding-Utf-8-*-
import numpy as npy
import scipy as spy
from gmshpy import *
from dG3Dpy import*
#from dG3DpyDebug import*
import sys
import pickle
from Prop_Reduce_Wove import *

def Write_VT(Vm, VI, Vg, ar, R):
    E0 = 4669.0
    nu0 = 0.39
    sy0 = 32.0
    H0 = 150.0
    m0 = 300.0
    Dmod = 2     #10
    if(Dmod==2):
        '''Beta = 1.0   #700.0
        kd = 1.3    #  0.007
        p0 = 0.0'''
        Beta = 0.21   #700.0
        kd = 2.0    #  0.007
        p0 = 0.0
    else: 
        Beta = 700.0
        kd =  0.007
        p0 = 0.0 

    E_fT = 12.9e3
    E_fL = 231.0e3
    nu_fLT = 0.3
    nu_fTL = E_fT*nu_fLT/E_fL
    nu_fTT = 0.46
    GTT = 4.45e3
    GLT= 11.3e3
    lf = 0.6   	
    l0 = 10.0        
    s0 = 4493.0        
    alpha = 0.6        
    gamma = 5.0	
    nf = 1000000
    GI = 0.0

    Nph = len(Vg)+1
    GrainProp = 48              # number of material parameter for each grain of MT
    k_start = 4+Nph*2+13
    prop_len = k_start + (Nph-1)*GrainProp + 3

    ######### material properties for VT
    iddam = 0
    iddamM0 = 0#9
    iddam_M_0 = 0#15
    iddam_M_90 = 0#15
    iddam_I = 0#34
    idclength = 0

    ### PRINT IN A FILE FOR MFH ####
    fid = open("property.i01", "w") 
    fid.write("%i\n" % (Nph*200))
    fid.write("%i\n" % (prop_len))
    fid.write("%i\n" % (3))
    fid.write("%i\n" % (iddam))
    fid.write("%i\n" % (idclength))
    fid.write("%i\n" % (Nph))
    fid.write("%i\n" % (4+Nph*2))
    fid.write("%.9f\n" % (Vm))
    for i in range(Nph-1):
        fid.write("%i\n" % (k_start + i*GrainProp))
        fid.write("%.9f\n" % (Vg[i]))

    #  matrix material properties
    fid.write("%i\n" % (2))                    
    fid.write("%i\n" % (iddamM0)) 
    fid.write("%i\n" % (idclength))
    fid.write("%.9f \n" % (E0))
    fid.write("%.9f \n" % (nu0))
    fid.write("%.9f \n" % (sy0))
    fid.write("%i\n" % (2))
    fid.write("%.9f \n" % (H0))
    fid.write("%.9f \n" % (m0))
    fid.write("%i\n" % (Dmod))
    fid.write("%.9f \n" % (Beta))
    fid.write("%.9f \n" % (kd))
    fid.write("%.9f \n" % (p0))



    for i in range(Nph-1):
        fid.write("%i\n" % (8))
        fid.write("%i\n" % (iddam))
        fid.write("%i\n" % (idclength))
        fid.write("%i\n" % (6))
        fid.write("%i\n" % (19))
        fid.write("%i\n" % (42))
        fid.write("%i\n" % (2))
        if(R[i][0] < 1.0e-3):
            fid.write("%i\n" % (iddam_M_90))
        else:
            fid.write("%i\n" % (iddam_M_0))
        fid.write("%i\n" % (idclength))
        fid.write("%.9f \n" % (E0))
        fid.write("%.9f \n" % (nu0))
        fid.write("%.9f \n" % (sy0))
        fid.write("%i\n" % (2))
        fid.write("%.9f \n" % (H0))
        fid.write("%.9f \n" % (m0))
        fid.write("%i\n" % (Dmod))
        fid.write("%.9f \n" % (Beta))
        fid.write("%.9f \n" % (kd))
        fid.write("%.9f \n" % (p0))
        fid.write("%i \n" % (7))
        fid.write("%i\n" % (iddam_I))
        fid.write("%i\n" % (idclength))
        fid.write("%.9f\n" % (E_fT))
        fid.write("%.9f\n" % (E_fT))
        fid.write("%.9f\n" % (E_fL)) 
        fid.write("%.9f\n" % (nu_fTT))
        fid.write("%.9f\n" % (nu_fTL))
        fid.write("%.9f\n" % (nu_fTL))
        fid.write("%.9f\n" % (GTT))
        fid.write("%.9f\n" % (GLT))
        fid.write("%.9f\n" % (GLT))
        fid.write("%.9f\n" % (R[i][0]))
        fid.write("%.9f\n" % (R[i][1]))
        fid.write("%.9f\n" % (R[i][2]))  
        fid.write("%i\n" % (7))
        fid.write("%.9f \n" % (lf))
        fid.write("%.9f \n" % (l0))
        fid.write("%.9f \n" % (s0))
        fid.write("%.9f \n" % (alpha))
        fid.write("%.9f \n" % (gamma))
        fid.write("%i\n" % (nf))
        fid.write("%.9f \n" % (GI))  
        fid.write("%.9f\n" % (VI))
        fid.write("%.9f\n" % (ar[i]))
        fid.write("%.9f\n" % (ar[i]))
        fid.write("%.9f\n" % (R[i][0]))
        fid.write("%.9f\n" % (R[i][1]))
        fid.write("%.9f\n" % (R[i][2]))  
    fid.write("%.9f\n" % (0.0)) 
    fid.write("%.9f\n" % (0.0)) 
    fid.write("%.9f\n" % (0.0)) 
    fid.write("%.9f\n" % (0.0)) 
 

    fid.close()

    return 


def Write_LVT(Vm, VI, Vg, ar, R):

    # basic material properties
    E0 = 4669.0
    nu0 = 0.39
    sy0 = 32.0
    H0 = 150.0
    m0 = 300.0
    Dmod = 2     #10
    if(Dmod==2):
        '''Beta = 1.0   #700.0
        kd = 1.3    #  0.007
        p0 = 0.0'''
        Beta = 0.21   #700.0
        kd = 2.0    #  0.007
        p0 = 0.0
    else: 
        Beta = 700.0
        kd =  0.007
        p0 = 0.0 

    E_fT = 12.9e3
    E_fL = 231.0e3
    nu_fLT = 0.3
    nu_fTL = E_fT*nu_fLT/E_fL
    nu_fTT = 0.46
    GTT = 4.45e3
    GLT= 11.3e3
    lf = 0.6   	
    l0 = 10.0        
    s0 = 4493.0        
    alpha = 0.6        
    gamma = 5.0	
    nf = 1000000
    GI = 0.0
  
    euler0 = npy.zeros(3)
    euler90 = npy.zeros(3)
    euler90[0] = 90.0

    LamN = 1
    LamProp = 11
    EP_Prop = 13
    Nph = len(Vg)               # Voigts for fiber grains
    GrainProp = 48              # number of material parameter for each grain of MT

    #### lam_2pl volume
    VA0 = Vm
    VB0 = 1.0-Vm

    ####
    k_start = 4+2*Nph*2 
    prop_len = LamN*LamProp + EP_Prop + k_start + 2*Nph*GrainProp + 3

    ######### material properties for VT
    iddam_lam = 0
    idclength = 0
    ideuler = 8
    iddam_Mpure = 9
    iddam_Mtx = 0#15
    iddam_Fib = 34

    idLA_0 = 11
    idLB_0 = idLA_0 + EP_Prop
     

    ### PRINT IN A FILE FOR MFH ####
    fid = open("property.i01", "w") 
    fid.write("%i\n" % (2*Nph*200 +LamN*40))
    fid.write("%i\n" % (prop_len))
    fid.write("%i\n" % (14))
    fid.write("%i\n" % (iddam_lam))
    fid.write("%i\n" % (idclength))
    fid.write("%i\n" % (ideuler))
    fid.write("%i\n" % (idLA_0))
    fid.write("%.9f\n" % (VA0))
    fid.write("%i\n" % (idLB_0))
    fid.write("%.9f\n" % (VB0))
    for i in range(3):
        fid.write("%.9f\n" % (euler0[i]))

    #  matrix material properties
    fid.write("%i\n" % (2))                    
    fid.write("%i\n" % (iddam_Mpure)) 
    fid.write("%i\n" % (idclength))
    fid.write("%.9f \n" % (E0))
    fid.write("%.9f \n" % (nu0))
    fid.write("%.9f \n" % (sy0))
    fid.write("%i\n" % (2))
    fid.write("%.9f \n" % (H0))
    fid.write("%.9f \n" % (m0))
    fid.write("%i\n" % (Dmod))
    fid.write("%.9f \n" % (Beta))
    fid.write("%.9f \n" % (kd))
    fid.write("%.9f \n" % (p0))

    ### voigt fiber grains
    fid.write("%i\n" % (3))
    fid.write("%i\n" % (iddam_lam))
    fid.write("%i\n" % (idclength))
    fid.write("%i\n" % (2*Nph))
    for i in range(Nph):
        fid.write("%i\n" % (k_start + i*GrainProp))
        fid.write("%.9f\n" % (Vg[i]/2.0))
    for i in range(Nph,2*Nph):
        fid.write("%i\n" % (k_start + i*GrainProp))
        fid.write("%.9f\n" % (Vg[i-Nph]/2.0))
    for i in range(Nph):
        fid.write("%i\n" % (8))
        fid.write("%i\n" % (iddam_lam))
        fid.write("%i\n" % (idclength))
        fid.write("%i\n" % (6))
        fid.write("%i\n" % (19))
        fid.write("%i\n" % (42))
        fid.write("%i\n" % (2))
        fid.write("%i\n" % (iddam_Mtx))        
        fid.write("%i\n" % (idclength))
        fid.write("%.9f \n" % (E0))
        fid.write("%.9f \n" % (nu0))
        fid.write("%.9f \n" % (sy0))
        fid.write("%i\n" % (2))
        fid.write("%.9f \n" % (H0))
        fid.write("%.9f \n" % (m0))
        fid.write("%i\n" % (Dmod))
        fid.write("%.9f \n" % (Beta))
        fid.write("%.9f \n" % (kd))
        fid.write("%.9f \n" % (p0))
        fid.write("%i \n" % (7))
        fid.write("%i\n" % (iddam_Fib))
        fid.write("%i\n" % (idclength))
        fid.write("%.9f\n" % (E_fT))
        fid.write("%.9f\n" % (E_fT))
        fid.write("%.9f\n" % (E_fL)) 
        fid.write("%.9f\n" % (nu_fTT))
        fid.write("%.9f\n" % (nu_fTL))
        fid.write("%.9f\n" % (nu_fTL))
        fid.write("%.9f\n" % (GTT))
        fid.write("%.9f\n" % (GLT))
        fid.write("%.9f\n" % (GLT))
        fid.write("%.9f\n" % (R[i][0]))
        fid.write("%.9f\n" % (R[i][1]))
        fid.write("%.9f\n" % (R[i][2]))  
        fid.write("%i\n" % (7))
        fid.write("%.9f \n" % (lf))
        fid.write("%.9f \n" % (l0))
        fid.write("%.9f \n" % (s0))
        fid.write("%.9f \n" % (alpha))
        fid.write("%.9f \n" % (gamma))
        fid.write("%i\n" % (nf))
        fid.write("%.9f \n" % (GI))  
        fid.write("%.9f\n" % (VI))
        fid.write("%.9f\n" % (ar[i]))
        fid.write("%.9f\n" % (ar[i]))
        fid.write("%.9f\n" % (R[i][0]))
        fid.write("%.9f\n" % (R[i][1]))
        fid.write("%.9f\n" % (R[i][2]))  
    for i in range(Nph):
        fid.write("%i\n" % (8))
        fid.write("%i\n" % (iddam_lam))
        fid.write("%i\n" % (idclength))
        fid.write("%i\n" % (6))
        fid.write("%i\n" % (19))
        fid.write("%i\n" % (42))
        fid.write("%i\n" % (2))
        fid.write("%i\n" % (iddam_Mtx))        
        fid.write("%i\n" % (idclength))
        fid.write("%.9f \n" % (E0))
        fid.write("%.9f \n" % (nu0))
        fid.write("%.9f \n" % (sy0))
        fid.write("%i\n" % (2))
        fid.write("%.9f \n" % (H0))
        fid.write("%.9f \n" % (m0))
        fid.write("%i\n" % (Dmod))
        fid.write("%.9f \n" % (Beta))
        fid.write("%.9f \n" % (kd))
        fid.write("%.9f \n" % (p0))
        fid.write("%i \n" % (7))
        fid.write("%i\n" % (iddam_Fib))
        fid.write("%i\n" % (idclength))
        fid.write("%.9f\n" % (E_fT))
        fid.write("%.9f\n" % (E_fT))
        fid.write("%.9f\n" % (E_fL)) 
        fid.write("%.9f\n" % (nu_fTT))
        fid.write("%.9f\n" % (nu_fTL))
        fid.write("%.9f\n" % (nu_fTL))
        fid.write("%.9f\n" % (GTT))
        fid.write("%.9f\n" % (GLT))
        fid.write("%.9f\n" % (GLT))
        fid.write("%.9f\n" % (90.0))
        fid.write("%.9f\n" % (R[i][1]))
        fid.write("%.9f\n" % (R[i][2]))  
        fid.write("%i\n" % (7))
        fid.write("%.9f \n" % (lf))
        fid.write("%.9f \n" % (l0))
        fid.write("%.9f \n" % (s0))
        fid.write("%.9f \n" % (alpha))
        fid.write("%.9f \n" % (gamma))
        fid.write("%i\n" % (nf))
        fid.write("%.9f \n" % (GI))  
        fid.write("%.9f\n" % (VI))
        fid.write("%.9f\n" % (ar[i]))
        fid.write("%.9f\n" % (ar[i]))
        fid.write("%.9f\n" % (90.0))
        fid.write("%.9f\n" % (R[i][1]))
        fid.write("%.9f\n" % (R[i][2]))  
    fid.write("%.9f\n" % (0.0)) 
    fid.write("%.9f\n" % (0.0)) 
    fid.write("%.9f\n" % (0.0)) 
    fid.write("%.9f\n" % (0.0)) 
 

    fid.close()

    return 

def Write_VLM(VI, Vg, Rg, Vm, Rf, ar):

    # basic material properties
    E0 = 4669.0
    nu0 = 0.39
    sy0 = 32.0
    H0 = 150.0
    m0 = 300.0
    Dmod = 2     #10
    if(Dmod==2):
        '''Beta = 1.0   #700.0
        kd = 1.3    #  0.007
        p0 = 0.0'''
        Beta = 0.21   #700.0
        kd = 2.0    #  0.007
        p0 = 0.0
    else: 
        Beta = 700.0
        kd =  0.007
        p0 = 0.0 

    E_fT = 12.9e3
    E_fL = 231.0e3
    nu_fLT = 0.3
    nu_fTL = E_fT*nu_fLT/E_fL
    nu_fTT = 0.46
    GTT = 4.45e3
    GLT= 11.3e3
    lf = 0.6   	
    l0 = 10.0        
    s0 = 4493.0        
    alpha = 0.6        
    gamma = 5.0	
    nf = 1000000
    GI = 0.0
  
    euler0 = npy.zeros(3)
    euler90 = npy.zeros(3)
    euler90[0] = 90.0

    Nph = len(Vg)               # Voigts for fiber grains
    LamN = len(Vg)
    LamProp = 11
    EP_Prop = 13  
    MFH_Prop = 48  
    GrainProp = 11+13+48              # number of material parameter for each grain of MT

    ####
    k_start = 4+2*Nph 
    prop_len = k_start + Nph*GrainProp + 3

    ######### material properties for VT
    iddam = 0
    iddam_lam = 0
    idclength = 0
    ideuler = 8
    iddam_Mpure = 0#9
    iddam_Mtx = 0#15
    iddam_Fib = 0#34

    idLA_0 = 11
    idLB_0 = idLA_0 + EP_Prop
     

    ### PRINT IN A FILE FOR MFH ####
    fid = open('property.i01', "w") 
    fid.write("%i\n" % (2*Nph*200 + 2*LamN*40))
    fid.write("%i\n" % (prop_len))
    #Votgt law
    fid.write("%i\n" % (3))
    fid.write("%i\n" % (iddam))
    fid.write("%i\n" % (idclength))
    fid.write("%i\n" % (Nph))
    for i in range(Nph):
        fid.write("%i\n" % (k_start + i*GrainProp))
        fid.write("%.9f\n" % (Vg[i]))
    # laminate grain
    for i in range(Nph):
        fid.write("%i\n" % (14))
        fid.write("%i\n" % (iddam_lam))
        fid.write("%i\n" % (idclength))
        fid.write("%i\n" % (ideuler))
        fid.write("%i\n" % (idLA_0))
        fid.write("%.9f\n" % (Vm[i]))
        fid.write("%i\n" % (idLB_0))
        fid.write("%.9f\n" % (1.0-Vm[i]))
        for k in range(3):
            fid.write("%.9f\n" % (Rg[i][k]))
            
        #  matrix material properties
        fid.write("%i\n" % (2))                    
        fid.write("%i\n" % (iddam_Mpure)) 
        fid.write("%i\n" % (idclength))
        fid.write("%.9f \n" % (E0))
        fid.write("%.9f \n" % (nu0))
        fid.write("%.9f \n" % (sy0))
        fid.write("%i\n" % (2))
        fid.write("%.9f \n" % (H0))
        fid.write("%.9f \n" % (m0))
        fid.write("%i\n" % (Dmod))
        fid.write("%.9f \n" % (Beta))
        fid.write("%.9f \n" % (kd))
        fid.write("%.9f \n" % (p0))
        #  MFH ---------------------
        fid.write("%i\n" % (8))
        fid.write("%i\n" % (iddam_lam))
        fid.write("%i\n" % (idclength))
        fid.write("%i\n" % (6))
        fid.write("%i\n" % (19))
        fid.write("%i\n" % (42))
        fid.write("%i\n" % (2))
        fid.write("%i\n" % (iddam_Mtx))        
        fid.write("%i\n" % (idclength))
        fid.write("%.9f \n" % (E0))
        fid.write("%.9f \n" % (nu0))
        fid.write("%.9f \n" % (sy0))
        fid.write("%i\n" % (2))
        fid.write("%.9f \n" % (H0))
        fid.write("%.9f \n" % (m0))
        fid.write("%i\n" % (Dmod))
        fid.write("%.9f \n" % (Beta))
        fid.write("%.9f \n" % (kd))
        fid.write("%.9f \n" % (p0))
        fid.write("%i \n" % (7))
        fid.write("%i\n" % (iddam_Fib))
        fid.write("%i\n" % (idclength))
        fid.write("%.9f\n" % (E_fT))
        fid.write("%.9f\n" % (E_fT))
        fid.write("%.9f\n" % (E_fL)) 
        fid.write("%.9f\n" % (nu_fTT))
        fid.write("%.9f\n" % (nu_fTL))
        fid.write("%.9f\n" % (nu_fTL))
        fid.write("%.9f\n" % (GTT))
        fid.write("%.9f\n" % (GLT))
        fid.write("%.9f\n" % (GLT))
        fid.write("%.9f\n" % (Rf[i][0]))
        fid.write("%.9f\n" % (Rf[i][1]))
        fid.write("%.9f\n" % (Rf[i][2]))  
        fid.write("%i\n" % (7))
        fid.write("%.9f \n" % (lf))
        fid.write("%.9f \n" % (l0))
        fid.write("%.9f \n" % (s0))
        fid.write("%.9f \n" % (alpha))
        fid.write("%.9f \n" % (gamma))
        fid.write("%i\n" % (nf))
        fid.write("%.9f \n" % (GI))  
        fid.write("%.9f\n" % (VI))
        fid.write("%.9f\n" % (ar[i]))
        fid.write("%.9f\n" % (ar[i]))
        fid.write("%.9f\n" % (Rf[i][0]))
        fid.write("%.9f\n" % (Rf[i][1]))
        fid.write("%.9f\n" % (Rf[i][2]))  

    fid.write("%.9f\n" % (0.0)) 
    fid.write("%.9f\n" % (0.0)) 
    fid.write("%.9f\n" % (0.0)) 
    fid.write("%.9f\n" % (0.0)) 

    fid.close()

    return 


def TensileWoven(num,Geo):
    # material law
    lawlinear = 1     # unique number of law
    rhoEP   = 1000.0

    properties = "property.i01"

    # solver
    sol = 2           # Gmm=0 (default) Taucs=1 PETsc=2
    soltype = 1       # StaticLinear=0 (default) StaticNonLinear=1
    nstep = 1000         # number of step (used only if soltype=1)
    ftime =1.         # Final time (used only if soltype=1)
    tol=1.e-5         # relative tolerance for NR scheme (used only if soltype=1)
    nstepArch=40      # Number of step between 2 archiving (used only if soltype=1)
    fulldg = 0
    beta1 = 30.
    eqRatio = 1.e6

    system = 1      # Displacement elimination =0 Multiplier elimination = 1 Displacement+ multiplier = 2
    control = 0     # load control = 0 arc length control euler = 1


    law1 = NonLocalDamageDG3DMaterialLaw(lawlinear, rhoEP, properties) 
    law1.setInitialDmax_Inc(0.95)
    law1.setInitialDmax_Mtx(0.95)
    law1.setUseBarF(True)
 
    Mfield = 51      # number of the field (physical number of Mtrix)
    space1 = 0       # function space (Lagrange=0)
                     
    Matrixfield = dG3DDomain(10,Mfield,space1,lawlinear,fulldg,3,0)
    Matrixfield.stabilityParameters(100.)

    if(Geo == 1):
        geofile = 'WLogC1.geo'
        meshfile = "WLogC1.msh"
    elif(Geo == 2) :
        geofile = 'WLogC2.geo'
        meshfile = "WLogC2.msh"

    mysolver = nonLinearMechSolver(1000)
    mysolver.createModel(geofile,meshfile,3,1)
    #mysolver.loadModel(meshfile)
   
    mysolver.addDomain(Matrixfield)
    mysolver.addMaterialLaw(law1)

    mysolver.Scheme(soltype)
    mysolver.Solver(sol)
    mysolver.snlData(nstep,ftime,tol)
  
    mysolver.stepBetweenArchiving(nstepArch)
    mysolver.setSystemType(system)
    mysolver.setControlType(control)
    mysolver.stiffnessModification(bool(1))
    mysolver.iterativeProcedure(bool(1))
    mysolver.setMessageView(bool(1))


    microBC = nonLinearPeriodicBC(1000,3) 
    microBC.setOrder(1)
    microBC.setBCPhysical(120,100,110,121,101,111)
    method = 5     # Periodic mesh = 0, Langrange interpolation = 1, Cubic spline interpolation =2,  FE linear= 3, FE Quad = 4
    degree = 3      # Order used for polynomial interpolation 
    addvertex = 1   # Polynomial interpolation by mesh vertex = 0, Polynomial interpolation by virtual vertex 
    microBC.setPeriodicBCOptions(method, degree, bool(addvertex))
    mysolver.addMicroBC(microBC)
    mysolver.activateTest(True)
    mysolver.setMicroProblemIndentification(0, num);
    #stress averaging flag and averaging method 0- VOLUME, 1- SURFACE
    mysolver.stressAveragingFlag(bool(1)) # set stress averaging ON- 0 , OFF-1
    mysolver.setStressAveragingMethod(0) # 0 -volume 1- surface
    #tangent averaging flag
  #  mysolver.tangentAveragingFlag(bool(1)) # set tangent averaging ON -0, OFF -1
 #   mysolver.setTangentAveragingMethod(2,1e-6) # 0- perturbation 1- condensation
  #  mysolver.setExtractElasticTangentOperator(True)

    # block rigid mode
    mysolver.displacementBC("Node",1,0,0.)
    mysolver.displacementBC("Node",1,1,0.)
    mysolver.displacementBC("Node",1,2,0.)
    mysolver.displacementBC("Node",3,2,0.)
    #mysolver.displacementBC("Node",3,0,0.)
    mysolver.displacementBC("Node",5,2,0.)

   # mysolver.displacementBC("Node",3,0,0.)
   # mysolver.displacementBC("Node",3,2,0.)

    #mysolver.displacementBC("Face",120,2,0.)

    #defo
    d1 =  1.5e-1
    cyclicFunction=cycleFunctionTime(0.0, 0., ftime/2.0, d1,ftime, 0.0)
    mysolver.displacementBC("Node",3,1,cyclicFunction)
    mysolver.displacementBC("Node",5,0,cyclicFunction)
  #  mysolver.displacementBC("Face",120,2,0.)
  #  mysolver.displacementBC("Face",121,2,0.)

    '''mysolver.displacementBC("Face",100,0,0.)
    mysolver.displacementBC("Face",110,1,0.)
    mysolver.displacementBC("Face",120,2,0.)
    mysolver.displacementBC("Face",101,0, 3.e-1)'''
 



#mysolver.displacementBC("Node",1,0,0.)
#mysolver.displacementBC("Node",1,1,0.)
#mysolver.displacementBC("Node",1,2,0.)
#mysolver.displacementBC("Node",3,1,0.)
#mysolver.displacementBC("Node",3,2,0.)
#mysolver.displacementBC("Node",2,1,0.)

#defo
#mysolver.displacementBC("Node",3,0, 2.e-2)

            
    mysolver.internalPointBuildView("svm",IPField.SVM, 1, 1)
    mysolver.internalPointBuildView("sig_xx",IPField.SIG_XX, 1, 1)
    mysolver.internalPointBuildView("sig_yy",IPField.SIG_YY, 1, 1)
    mysolver.internalPointBuildView("sig_zz",IPField.SIG_ZZ, 1, 1)
    mysolver.internalPointBuildView("sig_xy",IPField.SIG_XY, 1, 1)
    mysolver.internalPointBuildView("sig_yz",IPField.SIG_YZ, 1, 1)
    mysolver.internalPointBuildView("sig_xz",IPField.SIG_XZ, 1, 1)
    mysolver.internalPointBuildView("damage",IPField.DAMAGE, 1, 1)
    mysolver.internalPointBuildView("epl",IPField.PLASTICSTRAIN, 1, 1)
    mysolver.internalPointBuildView("FiberDamage",IPField.INC_DAMAGE, 1, 1)
 #   mysolver.archivingForceOnPhysicalGroup("Face", 101, 0, nstepArch)
    mysolver.archivingNodeDisplacement(3,0,nstepArch)
    mysolver.archivingNodeDisplacement(3,1,nstepArch)
    mysolver.archivingAverageValue(IPField.SIG_XX)
    mysolver.archivingAverageValue(IPField.SIG_YY)
    mysolver.archivingAverageValue(IPField.SIG_ZZ)
    mysolver.archivingAverageValue(IPField.SIG_XY)
    mysolver.archivingAverageValue(IPField.SIG_XZ)
    mysolver.archivingAverageValue(IPField.SIG_YZ)
    mysolver.archivingAverageValue(IPField.STRAIN_XX)
    mysolver.archivingAverageValue(IPField.STRAIN_YY)
    mysolver.archivingAverageValue(IPField.STRAIN_ZZ)
    mysolver.archivingAverageValue(IPField.STRAIN_XY)
    mysolver.archivingAverageValue(IPField.STRAIN_XZ)
    mysolver.archivingAverageValue(IPField.STRAIN_YZ)

    '''mysolver.archivingIPOnPhysicalGroup("Volume",Mfield, IPField.SIG_XX,IPField.MEAN_VALUE, nstepArch);
    mysolver.archivingIPOnPhysicalGroup("Volume",Mfield, IPField.STRAIN_XX,IPField.MEAN_VALUE, nstepArch);
    mysolver.archivingIPOnPhysicalGroup("Volume",Mfield, IPField.SIG_YY,IPField.MEAN_VALUE, nstepArch);
    mysolver.archivingIPOnPhysicalGroup("Volume",Mfield, IPField.STRAIN_YY,IPField.MEAN_VALUE, nstepArch);
    mysolver.archivingIPOnPhysicalGroup("Volume",Mfield, IPField.SIG_ZZ,IPField.MEAN_VALUE, nstepArch);
    mysolver.archivingIPOnPhysicalGroup("Volume",Mfield, IPField.STRAIN_ZZ,IPField.MEAN_VALUE, nstepArch);
    mysolver.archivingIPOnPhysicalGroup("Volume",Mfield, IPField.SIG_XY,IPField.MEAN_VALUE, nstepArch);
    mysolver.archivingIPOnPhysicalGroup("Volume",Mfield, IPField.STRAIN_XY,IPField.MEAN_VALUE, nstepArch);
    mysolver.archivingIPOnPhysicalGroup("Volume",Mfield, IPField.SIG_YZ,IPField.MEAN_VALUE, nstepArch);
    mysolver.archivingIPOnPhysicalGroup("Volume",Mfield, IPField.STRAIN_YZ,IPField.MEAN_VALUE, nstepArch);
    mysolver.archivingIPOnPhysicalGroup("Volume",Mfield, IPField.SIG_XZ,IPField.MEAN_VALUE, nstepArch);
    mysolver.archivingIPOnPhysicalGroup("Volume",Mfield, IPField.STRAIN_XZ,IPField.MEAN_VALUE, nstepArch);

    mysolver.archivingIPOnPhysicalGroup("Volume",Ffield, IPField.SIG_XX,IPField.MEAN_VALUE, nstepArch);
    mysolver.archivingIPOnPhysicalGroup("Volume",Ffield, IPField.STRAIN_XX,IPField.MEAN_VALUE, nstepArch);
    mysolver.archivingIPOnPhysicalGroup("Volume",Ffield, IPField.SIG_YY,IPField.MEAN_VALUE, nstepArch);
    mysolver.archivingIPOnPhysicalGroup("Volume",Ffield, IPField.STRAIN_YY,IPField.MEAN_VALUE, nstepArch);
    mysolver.archivingIPOnPhysicalGroup("Volume",Ffield, IPField.SIG_ZZ,IPField.MEAN_VALUE, nstepArch);
    mysolver.archivingIPOnPhysicalGroup("Volume",Ffield, IPField.STRAIN_ZZ,IPField.MEAN_VALUE, nstepArch);
    mysolver.archivingIPOnPhysicalGroup("Volume",Ffield, IPField.SIG_XY,IPField.MEAN_VALUE, nstepArch);
    mysolver.archivingIPOnPhysicalGroup("Volume",Ffield, IPField.STRAIN_XY,IPField.MEAN_VALUE, nstepArch);
    mysolver.archivingIPOnPhysicalGroup("Volume",Ffield, IPField.SIG_YZ,IPField.MEAN_VALUE, nstepArch);
    mysolver.archivingIPOnPhysicalGroup("Volume",Ffield, IPField.STRAIN_YZ,IPField.MEAN_VALUE, nstepArch);
    mysolver.archivingIPOnPhysicalGroup("Volume",Ffield, IPField.SIG_XZ,IPField.MEAN_VALUE, nstepArch);
    mysolver.archivingIPOnPhysicalGroup("Volume",Ffield, IPField.STRAIN_XZ,IPField.MEAN_VALUE, nstepArch);'''


    mysolver.solve()
    check = TestCheck()
    try:
      import linecache
      linesxx = linecache.getline('Average_SIG_XY.csv',1000)
    except:
      print('Cannot read the results in the files')
      import os
      os._exit(1)
    else:
      check.equal(-118.49395149776,float(linesxx.split(';')[1]))
    return

#######################################################################################################################
Geo = 2
dam = 0
mod = "VLM" # "LVM"# "VM" # 
num = 2000

if(Geo == 1):
    path = './'  
elif(Geo ==2):
    path = './'  

OPT_Para = path+ mod+'clipG'+str(Geo)+'.dat' 
    
Write_Prop_Reduce(Geo, dam, OPT_Para, mod)

TensileWoven(num,Geo)



