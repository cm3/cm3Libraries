SetFactory("OpenCASCADE");
Geometry.NumSubEdges = 100; // nicer display of curve
Mesh.CharacteristicLengthMin = 5;
Mesh.CharacteristicLengthMax = 5;

//ellips geomtry information ----------
b0 = 0.04875;
a = 0.12/b0/3.14159265;
e1 = 0.54;
e2 = 0.0;
L = 4*a + 2*e1;
l = 15.0/L;
cl=b0/1.0;

theta =  1.0/(1.0+Exp(l*L/4.0))-0.5;
b = b0*1.2;
z1 = -2.0*b*theta;

// mesh size --------------
lc1 = 0.100;
lc2 = 0.050;
lc3 = 0.05;

// origin of coordinate

P0 = newp;
Point(P0) = {0.0, 0, 0.0, cl};

b1=newv;
Box(b1) = {0.0,-0.5*L,-2.0*b-e2, L, L, 4.0*b+2.0*e2};



//--------------BC surfaces ---------------

tolBB=5.e-3;

xMin = 0.;
xMax = L;
yMin=-L/2.;
yMax=L/2.;
zMin=-2.*b-e2;
zMax= 2.*b+e2;


surfX0() = Surface In BoundingBox {xMin-tolBB, yMin-tolBB, zMin-tolBB, xMin+tolBB, yMax+tolBB, zMax+tolBB};
Printf("Surface X = 0 ", surfX0());
surfXL() = Surface In BoundingBox {xMax-tolBB, yMin-tolBB, zMin-tolBB, xMax+tolBB, yMax+tolBB, zMax+tolBB};
Printf("Surface X = L ", surfXL());

surfY0() = Surface In BoundingBox {xMin-tolBB, yMin-tolBB, zMin-tolBB, xMax+tolBB, yMin+tolBB, zMax+tolBB};
Printf("Surface Y = 0 ", surfY0());
surfYL() = Surface In BoundingBox {xMin-tolBB, yMax-tolBB, zMin-tolBB, xMax+tolBB, yMax+tolBB, zMax+tolBB};
Printf("Surface Y = L ", surfYL());

surfZ0() = Surface In BoundingBox {xMin-tolBB, yMin-tolBB, zMin-tolBB, xMax+tolBB, yMax+tolBB, zMin+tolBB};
Printf("Surface Z = 0 ", surfZ0());
surfZL() = Surface In BoundingBox {xMin-tolBB, yMin-tolBB, zMax-tolBB, xMax+tolBB, yMax+tolBB, zMax+tolBB};
Printf("Surface Z = L ", surfZL());

edZ0Y0() = Line In BoundingBox {xMin-tolBB, yMin-tolBB, zMin-tolBB, xMax+tolBB, yMin+tolBB, zMin+tolBB};
Printf("Edge Z = 0 Y=0", edZ0Y0());
edZ0YL() = Line In BoundingBox {xMin-tolBB, yMax-tolBB, zMin-tolBB,  xMax+tolBB, yMax+tolBB, zMin+tolBB};
Printf("Edge Z = 0 Y=L", edZ0YL());
edZLY0() = Line In BoundingBox {xMin-tolBB, yMin-tolBB, zMax-tolBB, xMax+tolBB, yMin+tolBB, zMax+tolBB};
Printf("Edge Z = L Y=0", edZLY0());
edZLYL() = Line In BoundingBox {xMin-tolBB, yMax-tolBB, zMax-tolBB, xMax+tolBB, yMax+tolBB, zMax+tolBB};
Printf("Edge Z = L Y=L", edZLYL());

edX0Y0() = Line In BoundingBox {xMin-tolBB, yMin-tolBB, zMin-tolBB, xMin+tolBB, yMin+tolBB, zMax+tolBB};
Printf("Edge X = 0 Y=0", edX0Y0());
edX0YL() = Line In BoundingBox {xMin-tolBB, yMax-tolBB, zMin-tolBB, xMin+tolBB, yMax+tolBB, zMax+tolBB};
Printf("Edge X = 0 Y=L", edX0YL());
edXLY0() = Line In BoundingBox {xMax-tolBB, yMin-tolBB, zMin-tolBB, xMax+tolBB, yMin+tolBB, zMax+tolBB};
Printf("Edge X = L Y=0", edX0Y0());
edXLYL() = Line In BoundingBox {xMax-tolBB, yMax-tolBB, zMin-tolBB, xMax+tolBB, yMax+tolBB, zMax+tolBB};
Printf("Edge X = L Y=L", edXLYL());

edX0Z0() = Line In BoundingBox {xMin-tolBB, yMin-tolBB, zMin-tolBB, xMin+tolBB, yMax+tolBB, zMin+tolBB};
Printf("Edge X = 0 Z=0", edX0Z0());
edX0ZL() = Line In BoundingBox {xMin-tolBB, yMin-tolBB, zMax-tolBB, xMin+tolBB, yMax+tolBB, zMax+tolBB};
Printf("Edge X = 0 Y=L", edX0ZL());
edXLZ0() = Line In BoundingBox {xMax-tolBB, yMin-tolBB, zMin-tolBB, xMax+tolBB, yMax+tolBB, zMin+tolBB};
Printf("Edge X = L Y=0", edX0Z0());
edXLZL() = Line In BoundingBox {xMax-tolBB, yMin-tolBB, zMax-tolBB, xMax+tolBB, yMax+tolBB, zMax+tolBB};
Printf("Edge X = L Y=L", edXLZL());


ptX0Y0Z0() = Point In BoundingBox {xMin-tolBB, yMin-tolBB, zMin-tolBB, xMin+tolBB, yMin+tolBB, zMin+tolBB};
Printf("Point X = 0 Y=0 Z=0", ptX0Y0Z0());
ptXLY0Z0() = Point In BoundingBox {xMax-tolBB, yMin-tolBB, zMin-tolBB, xMax+tolBB, yMin+tolBB, zMin+tolBB};
Printf("Point X = L Y=0 Z=0", ptXLY0Z0());
ptX0YLZ0() = Point In BoundingBox {xMin-tolBB, yMax-tolBB, zMin-tolBB, xMin+tolBB, yMax+tolBB, zMin+tolBB};
Printf("Point X = 0 Y=0 Z=0", ptX0Y0Z0());
ptXLYLZ0() = Point In BoundingBox {xMax-tolBB, yMax-tolBB, zMin-tolBB, xMax+tolBB, yMax+tolBB, zMin+tolBB};
Printf("Point X = L Y=0 Z=0", ptXLY0Z0());

ptX0Y0ZL() = Point In BoundingBox {xMin-tolBB, yMin-tolBB, zMax-tolBB, xMin+tolBB, yMin+tolBB, zMax+tolBB};
Printf("Point X = 0 Y=0 Z=L", ptX0Y0ZL());
ptXLY0ZL() = Point In BoundingBox {xMax-tolBB, yMin-tolBB, zMax-tolBB, xMax+tolBB, yMin+tolBB, zMax+tolBB};
Printf("Point X = L Y=0 Z=L", ptXLY0ZL());
ptX0YLZL() = Point In BoundingBox {xMin-tolBB, yMax-tolBB, zMax-tolBB, xMin+tolBB, yMax+tolBB, zMax+tolBB};
Printf("Point X = 0 Y=0 Z=L", ptX0Y0ZL());
ptXLYLZL() = Point In BoundingBox {xMax-tolBB, yMax-tolBB, zMax-tolBB, xMax+tolBB, yMax+tolBB, zMax+tolBB};
Printf("Point X = L Y=0 Z=L", ptXLY0ZL());

Physical Surface(100) = { surfX0() }; //x=0
Physical Surface(101) = { surfXL() }; //x=L
Physical Surface(110) = { surfY0()  }; //y=0
Physical Surface(111) = { surfYL() }; //y=L
Physical Surface(120) = { surfZ0() }; //z=0
Physical Surface(121) = { surfZL() }; //z=L

Physical Line(100) = { edZ0Y0() }; //y = 0, z = 0
Physical Line(101) = { edZ0YL() }; //y = L, z = 0
Physical Line(102) = { edZLY0() }; //y = 0, z = L
Physical Line(103) = { edZLYL() }; //y = L, z = L
Physical Line(110) = { edX0Z0() }; //x = 0, z = 0
Physical Line(111) = { edX0ZL() }; //x = 0, z = L
Physical Line(112) = { edXLZ0() }; //x = L, z = 0
Physical Line(113) = { edXLZL() }; //x = L, z = L
Physical Line(120) = { edX0Y0() }; //x = 0, y = 0
Physical Line(121) = { edX0YL() }; //x = 0, y = L
Physical Line(122) = { edXLY0() }; //x = L, y = 0
Physical Line(123) = { edXLYL() }; //x = L, y = L


Physical Point(1)= { ptX0Y0Z0() };
Physical Point(2)= { ptX0Y0ZL() };
Physical Point(3)= { ptXLY0Z0() };
Physical Point(4)= { ptXLY0ZL() };
Physical Point(5)= { ptX0YLZ0() };
Physical Point(6)= { ptX0YLZL() };
Physical Point(7)= { ptXLYLZ0() };
Physical Point(8)= { ptXLYLZL() };


//Physical Point(1)= {525 };
//Physical Point(2)= { 529};
//Physical Point(3)= {527};
//Physical Point(4)= { 528 };

//Physical Point(5)= {526};
//Physical Point(6)= {531};
//Physical Point(7)= {532};
//Physical Point(8)= {530};


Physical Volume(51) ={1};


