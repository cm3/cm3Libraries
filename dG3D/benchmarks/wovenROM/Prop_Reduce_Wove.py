#coding-Utf-8-*-
import numpy as npy
import scipy as spy
from gmshpy import *
from dG3Dpy import*
#from dG3DpyDebug import*
import sys
import pickle


################## VM  #######################
def Write_prop_VM(Vm, VI, Vg, ar, R, dam):
    E0 = 4669.0
    nu0 = 0.39
    sy0 = 32.0
    H0 = 150.0
    m0 = 300.0
    Dmod = 2     #10
    if(Dmod==2):
        '''Beta = 1.0   #700.0
        kd = 1.3    #  0.007
        p0 = 0.0'''
        Beta = 0.21   #700.0
        kd = 2.0    #  0.007
        p0 = 0.0
    else: 
        Beta = 700.0
        kd =  0.007
        p0 = 0.0 

    E_fT = 12.9e3
    E_fL = 231.0e3
    nu_fLT = 0.3
    nu_fTL = E_fT*nu_fLT/E_fL
    nu_fTT = 0.46
    GTT = 4.45e3
    GLT= 11.3e3
    lf = 0.6   	
    l0 = 10.0        
    s0 = 4493.0        
    alpha = 0.6        
    gamma = 5.0	
    nf = 1000000
    GI = 0.0

    Nph = len(Vg)+1
    GrainProp = 48              # number of material parameter for each grain of MT
    k_start = 4+Nph*2+13
    prop_len = k_start + (Nph-1)*GrainProp + 3

    ######### material properties for VT
    iddam = 0
    iddamM0 = 0# 9
    iddam_M_0 = 0#15
    iddam_M_90 = 0#15
    iddam_I = 0
    if(dam==1): 
        iddamM0 = 9
        iddam_I = 34 
    idclength = 0

    ### PRINT IN A FILE FOR MFH ####
    fid = open('property.i01', "w") 
    fid.write("%i\n" % (Nph*200))
    fid.write("%i\n" % (prop_len))
    fid.write("%i\n" % (3))
    fid.write("%i\n" % (iddam))
    fid.write("%i\n" % (idclength))
    fid.write("%i\n" % (Nph))
    fid.write("%i\n" % (4+Nph*2))
    fid.write("%.9f\n" % (Vm))
    for i in range(Nph-1):
        fid.write("%i\n" % (k_start + i*GrainProp))
        fid.write("%.9f\n" % (Vg[i]))

    #  matrix material properties
    fid.write("%i\n" % (2))                    
    fid.write("%i\n" % (iddamM0)) 
    fid.write("%i\n" % (idclength))
    fid.write("%.9f \n" % (E0))
    fid.write("%.9f \n" % (nu0))
    fid.write("%.9f \n" % (sy0))
    fid.write("%i\n" % (2))
    fid.write("%.9f \n" % (H0))
    fid.write("%.9f \n" % (m0))
    fid.write("%i\n" % (Dmod))
    fid.write("%.9f \n" % (Beta))
    fid.write("%.9f \n" % (kd))
    fid.write("%.9f \n" % (p0))



    for i in range(Nph-1):
        fid.write("%i\n" % (8))
        fid.write("%i\n" % (iddam))
        fid.write("%i\n" % (idclength))
        fid.write("%i\n" % (6))
        fid.write("%i\n" % (19))
        fid.write("%i\n" % (42))
        fid.write("%i\n" % (2))
        if(R[i][0] < 1.0e-3):
            fid.write("%i\n" % (iddam_M_90))
        else:
            fid.write("%i\n" % (iddam_M_0))
        fid.write("%i\n" % (idclength))
        fid.write("%.9f \n" % (E0))
        fid.write("%.9f \n" % (nu0))
        fid.write("%.9f \n" % (sy0))
        fid.write("%i\n" % (2))
        fid.write("%.9f \n" % (H0))
        fid.write("%.9f \n" % (m0))
        fid.write("%i\n" % (Dmod))
        fid.write("%.9f \n" % (Beta))
        fid.write("%.9f \n" % (kd))
        fid.write("%.9f \n" % (p0))
        fid.write("%i \n" % (7))
        fid.write("%i\n" % (iddam_I))
        fid.write("%i\n" % (idclength))
        fid.write("%.9f\n" % (E_fT))
        fid.write("%.9f\n" % (E_fT))
        fid.write("%.9f\n" % (E_fL)) 
        fid.write("%.9f\n" % (nu_fTT))
        fid.write("%.9f\n" % (nu_fTL))
        fid.write("%.9f\n" % (nu_fTL))
        fid.write("%.9f\n" % (GTT))
        fid.write("%.9f\n" % (GLT))
        fid.write("%.9f\n" % (GLT))
        fid.write("%.9f\n" % (R[i][0]))
        fid.write("%.9f\n" % (R[i][1]))
        fid.write("%.9f\n" % (R[i][2]))  
        fid.write("%i\n" % (7))
        fid.write("%.9f \n" % (lf))
        fid.write("%.9f \n" % (l0))
        fid.write("%.9f \n" % (s0))
        fid.write("%.9f \n" % (alpha))
        fid.write("%.9f \n" % (gamma))
        fid.write("%i\n" % (nf))
        fid.write("%.9f \n" % (GI))  
        fid.write("%.9f\n" % (VI))
        fid.write("%.9f\n" % (ar[i]))
        fid.write("%.9f\n" % (ar[i]))
        fid.write("%.9f\n" % (R[i][0]))
        fid.write("%.9f\n" % (R[i][1]))
        fid.write("%.9f\n" % (R[i][2]))  
    fid.write("%.9f\n" % (0.0)) 
    fid.write("%.9f\n" % (0.0)) 
    fid.write("%.9f\n" % (0.0)) 
    fid.write("%.9f\n" % (0.0)) 
 

    fid.close()

    return 


##########################  LVM  ##################################
def Write_prop_LVM(Vm, VI, Vg, ar, R,dam):

    # basic material properties
    E0 = 4669.0
    nu0 = 0.39
    sy0 = 32.0
    H0 = 150.0
    m0 = 300.0
    Dmod = 2     #10
    if(Dmod==2):
        '''Beta = 1.0   #700.0
        kd = 1.3    #  0.007
        p0 = 0.0'''
        Beta = 0.21   #700.0
        kd = 2.0    #  0.007
        p0 = 0.0
    else: 
        Beta = 700.0
        kd =  0.007
        p0 = 0.0 

    E_fT = 12.9e3
    E_fL = 231.0e3
    nu_fLT = 0.3
    nu_fTL = E_fT*nu_fLT/E_fL
    nu_fTT = 0.46
    GTT = 4.45e3
    GLT= 11.3e3
    lf = 0.6   	
    l0 = 10.0        
    s0 = 4493.0        
    alpha = 0.6        
    gamma = 5.0	
    nf = 1000000
    GI = 0.0
  
    euler0 = npy.zeros(3)
    euler90 = npy.zeros(3)
    euler90[0] = 90.0

    LamN = 1
    LamProp = 11
    EP_Prop = 13
    Nph = len(Vg)               # Voigts for fiber grains
    GrainProp = 48              # number of material parameter for each grain of MT

    #### lam_2pl volume
    VA0 = Vm
    VB0 = 1.0-Vm

    ####
    k_start = 4+2*Nph*2 
    prop_len = LamN*LamProp + EP_Prop + k_start + 2*Nph*GrainProp + 3

    ######### material properties for VT
    iddam_lam = 0
    idclength = 0
    ideuler = 8
    iddam_Mpure = 0#9
    iddam_Mtx = 0#15
    iddam_Fib = 0#34
    if(dam == 1):
        iddam_Mpure = 9
        iddam_Fib = 34

    idLA_0 = 11
    idLB_0 = idLA_0 + EP_Prop
     

    ### PRINT IN A FILE FOR MFH ####
    fid = open('property.i01', "w") 
    fid.write("%i\n" % (2*Nph*200 +LamN*40))
    fid.write("%i\n" % (prop_len))
    fid.write("%i\n" % (14))
    fid.write("%i\n" % (iddam_lam))
    fid.write("%i\n" % (idclength))
    fid.write("%i\n" % (ideuler))
    fid.write("%i\n" % (idLA_0))
    fid.write("%.9f\n" % (VA0))
    fid.write("%i\n" % (idLB_0))
    fid.write("%.9f\n" % (VB0))
    for i in range(3):
        fid.write("%.9f\n" % (euler0[i]))

    #  matrix material properties
    fid.write("%i\n" % (2))                    
    fid.write("%i\n" % (iddam_Mpure)) 
    fid.write("%i\n" % (idclength))
    fid.write("%.9f \n" % (E0))
    fid.write("%.9f \n" % (nu0))
    fid.write("%.9f \n" % (sy0))
    fid.write("%i\n" % (2))
    fid.write("%.9f \n" % (H0))
    fid.write("%.9f \n" % (m0))
    fid.write("%i\n" % (Dmod))
    fid.write("%.9f \n" % (Beta))
    fid.write("%.9f \n" % (kd))
    fid.write("%.9f \n" % (p0))

    ### voigt fiber grains
    fid.write("%i\n" % (3))
    fid.write("%i\n" % (iddam_lam))
    fid.write("%i\n" % (idclength))
    fid.write("%i\n" % (2*Nph))
    for i in range(Nph):
        fid.write("%i\n" % (k_start + i*GrainProp))
        fid.write("%.9f\n" % (Vg[i]/2.0))
    for i in range(Nph,2*Nph):
        fid.write("%i\n" % (k_start + i*GrainProp))
        fid.write("%.9f\n" % (Vg[i-Nph]/2.0))
    for i in range(Nph):
        fid.write("%i\n" % (8))
        fid.write("%i\n" % (iddam_lam))
        fid.write("%i\n" % (idclength))
        fid.write("%i\n" % (6))
        fid.write("%i\n" % (19))
        fid.write("%i\n" % (42))
        fid.write("%i\n" % (2))
        fid.write("%i\n" % (iddam_Mtx))        
        fid.write("%i\n" % (idclength))
        fid.write("%.9f \n" % (E0))
        fid.write("%.9f \n" % (nu0))
        fid.write("%.9f \n" % (sy0))
        fid.write("%i\n" % (2))
        fid.write("%.9f \n" % (H0))
        fid.write("%.9f \n" % (m0))
        fid.write("%i\n" % (Dmod))
        fid.write("%.9f \n" % (Beta))
        fid.write("%.9f \n" % (kd))
        fid.write("%.9f \n" % (p0))
        fid.write("%i \n" % (7))
        fid.write("%i\n" % (iddam_Fib))
        fid.write("%i\n" % (idclength))
        fid.write("%.9f\n" % (E_fT))
        fid.write("%.9f\n" % (E_fT))
        fid.write("%.9f\n" % (E_fL)) 
        fid.write("%.9f\n" % (nu_fTT))
        fid.write("%.9f\n" % (nu_fTL))
        fid.write("%.9f\n" % (nu_fTL))
        fid.write("%.9f\n" % (GTT))
        fid.write("%.9f\n" % (GLT))
        fid.write("%.9f\n" % (GLT))
        fid.write("%.9f\n" % (R[i][0]))
        fid.write("%.9f\n" % (R[i][1]))
        fid.write("%.9f\n" % (R[i][2]))  
        fid.write("%i\n" % (7))
        fid.write("%.9f \n" % (lf))
        fid.write("%.9f \n" % (l0))
        fid.write("%.9f \n" % (s0))
        fid.write("%.9f \n" % (alpha))
        fid.write("%.9f \n" % (gamma))
        fid.write("%i\n" % (nf))
        fid.write("%.9f \n" % (GI))  
        fid.write("%.9f\n" % (VI))
        fid.write("%.9f\n" % (ar[i]))
        fid.write("%.9f\n" % (ar[i]))
        fid.write("%.9f\n" % (R[i][0]))
        fid.write("%.9f\n" % (R[i][1]))
        fid.write("%.9f\n" % (R[i][2]))  
    for i in range(Nph):
        fid.write("%i\n" % (8))
        fid.write("%i\n" % (iddam_lam))
        fid.write("%i\n" % (idclength))
        fid.write("%i\n" % (6))
        fid.write("%i\n" % (19))
        fid.write("%i\n" % (42))
        fid.write("%i\n" % (2))
        fid.write("%i\n" % (iddam_Mtx))        
        fid.write("%i\n" % (idclength))
        fid.write("%.9f \n" % (E0))
        fid.write("%.9f \n" % (nu0))
        fid.write("%.9f \n" % (sy0))
        fid.write("%i\n" % (2))
        fid.write("%.9f \n" % (H0))
        fid.write("%.9f \n" % (m0))
        fid.write("%i\n" % (Dmod))
        fid.write("%.9f \n" % (Beta))
        fid.write("%.9f \n" % (kd))
        fid.write("%.9f \n" % (p0))
        fid.write("%i \n" % (7))
        fid.write("%i\n" % (iddam_Fib))
        fid.write("%i\n" % (idclength))
        fid.write("%.9f\n" % (E_fT))
        fid.write("%.9f\n" % (E_fT))
        fid.write("%.9f\n" % (E_fL)) 
        fid.write("%.9f\n" % (nu_fTT))
        fid.write("%.9f\n" % (nu_fTL))
        fid.write("%.9f\n" % (nu_fTL))
        fid.write("%.9f\n" % (GTT))
        fid.write("%.9f\n" % (GLT))
        fid.write("%.9f\n" % (GLT))
        fid.write("%.9f\n" % (90.0))
        fid.write("%.9f\n" % (R[i][1]))
        fid.write("%.9f\n" % (R[i][2]))  
        fid.write("%i\n" % (7))
        fid.write("%.9f \n" % (lf))
        fid.write("%.9f \n" % (l0))
        fid.write("%.9f \n" % (s0))
        fid.write("%.9f \n" % (alpha))
        fid.write("%.9f \n" % (gamma))
        fid.write("%i\n" % (nf))
        fid.write("%.9f \n" % (GI))  
        fid.write("%.9f\n" % (VI))
        fid.write("%.9f\n" % (ar[i]))
        fid.write("%.9f\n" % (ar[i]))
        fid.write("%.9f\n" % (90.0))
        fid.write("%.9f\n" % (R[i][1]))
        fid.write("%.9f\n" % (R[i][2]))  
    fid.write("%.9f\n" % (0.0)) 
    fid.write("%.9f\n" % (0.0)) 
    fid.write("%.9f\n" % (0.0)) 
    fid.write("%.9f\n" % (0.0)) 
 

    fid.close()

    return 


#######################  VLM  ########################################
def Write_prop_VLM(VI, Vg, Rg, Vm, Rf, ar, dam):

    # basic material properties
    E0 = 4669.0
    nu0 = 0.39
    sy0 = 32.0
    H0 = 150.0
    m0 = 300.0
    Dmod = 2     #10
    if(Dmod==2):
        '''Beta = 1.0   #700.0
        kd = 1.3    #  0.007
        p0 = 0.0'''
        Beta = 0.21   #700.0
        kd = 2.0    #  0.007
        p0 = 0.0
    else: 
        Beta = 700.0
        kd =  0.007
        p0 = 0.0 

    E_fT = 12.9e3
    E_fL = 231.0e3
    nu_fLT = 0.3
    nu_fTL = E_fT*nu_fLT/E_fL
    nu_fTT = 0.46
    GTT = 4.45e3
    GLT= 11.3e3
    lf = 0.6   	
    l0 = 10.0        
    s0 = 4493.0        
    alpha = 0.6        
    gamma = 5.0	
    nf = 1000000
    GI = 0.0
  
    euler0 = npy.zeros(3)
    euler90 = npy.zeros(3)
    euler90[0] = 90.0

    Nph = len(Vg)               # Voigts for fiber grains
    LamN = len(Vg)
    LamProp = 11
    EP_Prop = 13  
    MFH_Prop = 48  
    GrainProp = 11+13+48              # number of material parameter for each grain of MT

    ####
    k_start = 4+2*Nph 
    prop_len = k_start + Nph*GrainProp + 3

    ######### material properties for VT
    iddam = 0
    iddam_lam = 0
    idclength = 0
    ideuler = 8
    iddam_Mpure = 0#9
    iddam_Mtx = 0#15
    iddam_Fib = 0#34
    if(dam ==1):
        iddam_Mpure = 9
        #iddam_Mtx = 0#15
        iddam_Fib = 34

    idLA_0 = 11
    idLB_0 = idLA_0 + EP_Prop
     

    ### PRINT IN A FILE FOR MFH ####
    fid = open('property.i01', "w") 
    fid.write("%i\n" % (2*Nph*200 + 2*LamN*40))
    fid.write("%i\n" % (prop_len))
    #Votgt law
    fid.write("%i\n" % (3))
    fid.write("%i\n" % (iddam))
    fid.write("%i\n" % (idclength))
    fid.write("%i\n" % (Nph))
    for i in range(Nph):
        fid.write("%i\n" % (k_start + i*GrainProp))
        fid.write("%.9f\n" % (Vg[i]))
    # laminate grain
    for i in range(Nph):
        fid.write("%i\n" % (14))
        fid.write("%i\n" % (iddam_lam))
        fid.write("%i\n" % (idclength))
        fid.write("%i\n" % (ideuler))
        fid.write("%i\n" % (idLA_0))
        fid.write("%.9f\n" % (Vm[i]))
        fid.write("%i\n" % (idLB_0))
        fid.write("%.9f\n" % (1.0-Vm[i]))
        for k in range(3):
            fid.write("%.9f\n" % (Rg[i][k]))
            
        #  matrix material properties
        fid.write("%i\n" % (2))                    
        fid.write("%i\n" % (iddam_Mpure)) 
        fid.write("%i\n" % (idclength))
        fid.write("%.9f \n" % (E0))
        fid.write("%.9f \n" % (nu0))
        fid.write("%.9f \n" % (sy0))
        fid.write("%i\n" % (2))
        fid.write("%.9f \n" % (H0))
        fid.write("%.9f \n" % (m0))
        fid.write("%i\n" % (Dmod))
        fid.write("%.9f \n" % (Beta))
        fid.write("%.9f \n" % (kd))
        fid.write("%.9f \n" % (p0))
        #  MFH ---------------------
        fid.write("%i\n" % (8))
        fid.write("%i\n" % (iddam_lam))
        fid.write("%i\n" % (idclength))
        fid.write("%i\n" % (6))
        fid.write("%i\n" % (19))
        fid.write("%i\n" % (42))
        fid.write("%i\n" % (2))
        fid.write("%i\n" % (iddam_Mtx))        
        fid.write("%i\n" % (idclength))
        fid.write("%.9f \n" % (E0))
        fid.write("%.9f \n" % (nu0))
        fid.write("%.9f \n" % (sy0))
        fid.write("%i\n" % (2))
        fid.write("%.9f \n" % (H0))
        fid.write("%.9f \n" % (m0))
        fid.write("%i\n" % (Dmod))
        fid.write("%.9f \n" % (Beta))
        fid.write("%.9f \n" % (kd))
        fid.write("%.9f \n" % (p0))
        fid.write("%i \n" % (7))
        fid.write("%i\n" % (iddam_Fib))
        fid.write("%i\n" % (idclength))
        fid.write("%.9f\n" % (E_fT))
        fid.write("%.9f\n" % (E_fT))
        fid.write("%.9f\n" % (E_fL)) 
        fid.write("%.9f\n" % (nu_fTT))
        fid.write("%.9f\n" % (nu_fTL))
        fid.write("%.9f\n" % (nu_fTL))
        fid.write("%.9f\n" % (GTT))
        fid.write("%.9f\n" % (GLT))
        fid.write("%.9f\n" % (GLT))
        fid.write("%.9f\n" % (Rf[i][0]))
        fid.write("%.9f\n" % (Rf[i][1]))
        fid.write("%.9f\n" % (Rf[i][2]))  
        fid.write("%i\n" % (7))
        fid.write("%.9f \n" % (lf))
        fid.write("%.9f \n" % (l0))
        fid.write("%.9f \n" % (s0))
        fid.write("%.9f \n" % (alpha))
        fid.write("%.9f \n" % (gamma))
        fid.write("%i\n" % (nf))
        fid.write("%.9f \n" % (GI))  
        fid.write("%.9f\n" % (VI))
        fid.write("%.9f\n" % (ar[i]))
        fid.write("%.9f\n" % (ar[i]))
        fid.write("%.9f\n" % (Rf[i][0]))
        fid.write("%.9f\n" % (Rf[i][1]))
        fid.write("%.9f\n" % (Rf[i][2]))  

    fid.write("%.9f\n" % (0.0)) 
    fid.write("%.9f\n" % (0.0)) 
    fid.write("%.9f\n" % (0.0)) 
    fid.write("%.9f\n" % (0.0)) 

    fid.close()

    return 


def Write_Prop_Reduce(Geo, dam, OptFile, mod):

    if(Geo == 1):
        VI_yarn = 0.65         # fiber volume fraction in yarn 
        Vmtx = 0.5168269            # matrix volume fraction in woven composite (excluding yarn) 
    elif(Geo ==2):
        VI_yarn = 0.85         # fiber volume fraction in yarn 
        Vmtx = 0.35444            # matrix volume fraction in woven composite (excluding yarn) 

    if(mod != "VLM"):
        with open(OptFile,'rb') as data:
            list_V = pickle.load(data,encoding="bytes") 
            list_R = pickle.load(data,encoding="bytes") 
            list_VI = pickle.load(data,encoding="bytes") 
            list_ar = pickle.load(data,encoding="bytes") 
    else:
        with open(OptFile,'rb') as data:
            list_Vg = pickle.load(data,encoding="bytes") 
            list_Rg = pickle.load(data,encoding="bytes") 
            list_Vm = pickle.load(data,encoding="bytes") 
            list_Rf = pickle.load(data,encoding="bytes") 
            list_ar = pickle.load(data,encoding="bytes") 


    if(mod == "VM"):
        list_V = (1.0-Vmtx)/2.0*list_V/list_V.sum()
        Vg = npy.zeros(2*len(list_V))
        R = npy.zeros((2*len(list_V),3))
        ar = npy.zeros(2*len(list_V))

        for i in range(len(list_V)):
            Vg[2*i] = list_V[i]
            Vg[2*i+1] = list_V[i]

            R[2*i+1][0] = 90.0
            R[2*i][1] = list_R[i]
            R[2*i+1][1] = list_R[i]

            ar[2*i] = list_ar[i]
            ar[2*i+1] = list_ar[i]
        Write_prop_VM(Vmtx, VI_yarn, Vg, ar, R, dam)

    elif(mod == "LVM"): 
        list_V = list_V/list_V.sum()
        Vg = npy.zeros(len(list_V))
        R = npy.zeros((len(list_V),3))
        ar = npy.zeros(len(list_V))
        for i in range(len(list_V)):
            Vg[i] = list_V[i]
            R[i][1] = list_R[i]
            ar[i] = list_ar[i]
        Write_prop_LVM(Vmtx, VI_yarn, Vg, ar, R, dam)

    else:
        list_Vg = list_Vg/list_Vg.sum()
        c = list_Vg*list_Vm
        list_Vm = list_Vm/c.sum()*Vmtx
        Vg = npy.zeros(2*len(list_Vg))
        Rg = npy.zeros((2*len(list_Vg),3))
        Vm = npy.zeros(2*len(list_Vg))
        Rf = npy.zeros((2*len(list_Vg),3))
        ar = npy.zeros(2*len(list_Vg))

        for i in range(len(list_Vg)):
            Vg[2*i] = list_Vg[i]/2.0
            Vg[2*i+1] = list_Vg[i]/2.0

            Rg[2*i][1] = list_Rg[i]
            Rg[2*i+1][0] = 90.0
            Rg[2*i+1][1] = list_Rg[i]

            Vm[2*i] = list_Vm[i]
            Vm[2*i+1] = list_Vm[i]

            Rf[2*i][1] = list_Rf[i]
            Rf[2*i+1][1] = list_Rf[i]

            ar[2*i] = list_ar[i]
            ar[2*i+1] = list_ar[i]
 
        Write_prop_VLM(VI_yarn, Vg, Rg, Vm, Rf, ar,dam)

    return




