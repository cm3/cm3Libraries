#coding-Utf-8-*-
from gmshpy import*
from dG3Dpy import*
from math import*

# Script for plane notched specimen with Gurson


# Material law creation
# ===================================================================================
BulkLawNum1 = 11
# material parameters - bulk law
rho = 7600. 		# kg/m3:	density
young = 210.e3	# Pa: 	module de young
nu    = 0.3		# -: 	poisson coefficient
sy0   = 354.41 	#  	initial yield stress
h_hard  = 500. 
h_exp = 0.13		#	exponential coefficient

# hardening
Harden1 = SwiftJ2IsotropicHardening(BulkLawNum1, sy0, h_hard, h_exp)


# - non-local law
l = 0.4
cl = l*l
Cl_Law1 = IsotropicCLengthLaw(BulkLawNum1, cl)

# - nucleation
fn     = 0.1;
sn     = 0.3;
epsilonn = 0.1;
Gdnexp1 = GaussianNucleationFunctionLaw(0, fn, sn, epsilonn);

# - gurson
fVinitial = 0.05 # initial porosity

# material law creation
BulkLaw1 = NonLocalPorousThomasonWithMPSDG3DMaterialLaw(BulkLawNum1, young, nu, rho, fVinitial, 3., Harden1, Cl_Law1,1e-8)
BulkLaw1.setOrderForLogExp(3)
BulkLaw1.setNonLocalMethod(2)

# Solver parameters - implicit
# ===================================================================================
soltype = 1 		# StaticLinear=0 (default, StaticNonLinear=1, Explicit=2, # Multi=3, Implicit=4, Eigen=5)
nstep = 150 		# Number of step
ftime =1.0 		# Final time
tol=1.e-6 		# Relative tolerance for NR scheme
tolAbs = 1.e-8		# Absolute tolerance for NR scheme
nstepArchIP=1		# Number of step between 2 archiving
nstepArchForce =1	# Number of step between 2 force archiving
nstepArchEnergy = nstepArchForce # Number of step between 2 energy computation and archiving

MaxIter = 15		# Maximum number of iterations
StepIncrease = 2	# Number of successfull timestep before reincreasing it
StepReducFactor = 2. 	# Timestep reduction factor
NumberReduction = 20	# Maximum number of timespep reduction: max reduction = pow(StepReducFactor,this)

fullDg = False
eqRatio = 1.0		# Ratio between "elastic" and non-local equations 
space1 = 0 		# Function space (Lagrange=0)
beta1  = 100.0		# Penality parameter for DG

# Domain creation
geofile = "model.geo"
meshfile= "model.msh" 	# name of mesh file

## ===================================================================================
field1 = dG3DDomain(11,11,0,BulkLawNum1,fullDg,2,3)
#field1.setBulkDamageBlockedMethod(-1)  # not block
#field1.forceCohesiveInsertionAllIPs(True,0)
field1.stabilityParameters(beta1) 			# Adding stability parameters (for DG)
field1.setNonLocalStabilityParameters(beta1,fullDg) 		# Adding stability parameters (for DG)
field1.setNonLocalEqRatio(eqRatio)
#field1.matrixByPerturbation(1,1,1,1e-7)

# Solver creation
# ===================================================================================
mysolver = nonLinearMechSolver(1000) 		# Solver associated with numSolver1

mysolver.createModel(geofile,meshfile,2,2)		# add mesh
mysolver.addDomain(field1) 		# add domain
mysolver.addMaterialLaw(BulkLaw1) 	# add material law

# solver parametrisation
mysolver.Scheme(soltype) 		# solver scheme
mysolver.Solver(2) 			# Library solver: Gmm=0 (default) Taucs=1 PETsc=2
mysolver.snlData(nstep,ftime,tol,tolAbs) # solver parameters for imp. solving (numb. of step, final time and tolerance)
mysolver.snlManageTimeStep(MaxIter,StepIncrease,StepReducFactor,NumberReduction)

# solver archiving
mysolver.stepBetweenArchiving(nstepArchIP) 	# archiving frequency
mysolver.energyComputation(nstepArchEnergy)	# archiving frequency for energy

# path following
method = 0
withPF = bool(1)
mysolver.pathFollowing(withPF,method)
if method == 0:
	mysolver.setPathFollowingIncrementAdaptation(True,5,0.3)
	mysolver.setPathFollowingControlType(0)
	mysolver.setPathFollowingCorrectionMethod(0)
	mysolver.setPathFollowingArcLengthStep(0.0015)
	mysolver.setBoundsOfPathFollowingArcLengthSteps(1.e-5,5e-3);
else:
	# time-step adaptation by number of NR iterations
	mysolver.setPathFollowingIncrementAdaptation(True,3,0.3)
	mysolver.setPathFollowingLocalSteps(1e-4,1.e-4)
	mysolver.setPathFollowingSwitchCriterion(0.)
	mysolver.setPathFollowingLocalIncrementType(1); 
	mysolver.setBoundsOfPathFollowingLocalSteps(1.e-6,2.e-4)

# Boundary conditions
# ===============================
mysolver.displacementBC("Face",11,2,0.)
mysolver.displacementBC("Edge",1,1,0.)
mysolver.displacementBC("Edge",4,0,0.) 

if withPF == True:
	mysolver.sameDisplacementBC("Edge",2,3,0)
	mysolver.forceBC("Node",3,0,1e4) 
else:
	mysolver.displacementBC("Edge",2,0,1.e-2)


# Variable storage
# ===============================
mysolver.internalPointBuildView("svm",IPField.SVM, 1, 1)
mysolver.internalPointBuildView("sig_xx",IPField.SIG_XX, 1, 1)
mysolver.internalPointBuildView("sig_yy",IPField.SIG_YY, 1, 1)
mysolver.internalPointBuildView("sig_zz",IPField.SIG_ZZ, 1, 1)
mysolver.internalPointBuildView("sig_xy",IPField.SIG_XY, 1, 1)
mysolver.internalPointBuildView("sig_yz",IPField.SIG_YZ, 1, 1)
mysolver.internalPointBuildView("sig_xz",IPField.SIG_XZ, 1, 1)
mysolver.internalPointBuildView("damage",IPField.DAMAGE,1,1)
mysolver.internalPointBuildView("local_fV",IPField.LOCAL_POROSITY,1,1)
mysolver.internalPointBuildView("local_fV_max",IPField.LOCAL_POROSITY,1,IPField.MAX_VALUE)
mysolver.internalPointBuildView("epl_max",IPField.PLASTICSTRAIN, 1, IPField.MAX_VALUE)
mysolver.internalPointBuildView("triaxiality",IPField.STRESS_TRIAXIALITY, 1, 1)
mysolver.internalPointBuildView("COALESCENCE",IPField.COALESCENCE, 1, 1)
mysolver.internalPointBuildView("NONLOCAL_POROSITY",IPField.NONLOCAL_POROSITY, 1, 1)
mysolver.internalPointBuildView("CORRECTED_POROSITY",IPField.CORRECTED_POROSITY, 1, 1)
mysolver.internalPointBuildView("CORRECTED_POROSITY_MAX",IPField.CORRECTED_POROSITY, 1, IPField.MAX_VALUE)
mysolver.internalPointBuildView("LODE_ANGLE",IPField.LODE_ANGLE, 1, IPField.MAX_VALUE)

mysolver.internalPointBuildView("FAILED",IPField.FAILED, 1, 1)
mysolver.internalPointBuildView("chi",IPField.LIGAMENT_RATIO, 1, 1)
mysolver.internalPointBuildView("chi_max",IPField.LIGAMENT_RATIO, 1, IPField.MAX_VALUE)
mysolver.OneUnknownBuildView("nonlocalVar",3,1)

mysolver.archivingForceOnPhysicalGroup("Edge", 4, 0, nstepArchForce)
mysolver.archivingNodeDisplacement(3,0, nstepArchForce)
mysolver.archivingNodeDisplacement(5,1, nstepArchForce)

mysolver.archivingIPOnPhysicalGroup("Face", 11, IPField.NONLOCAL_POROSITY,IPField.MAX_VALUE,nstepArchForce)
mysolver.archivingIPOnPhysicalGroup("Face", 11, IPField.LOCAL_POROSITY,IPField.MAX_VALUE,nstepArchForce)
mysolver.archivingIPOnPhysicalGroup("Face", 11, IPField.CORRECTED_POROSITY,IPField.MAX_VALUE,nstepArchForce)

mysolver.solve()





check = TestCheck()
check.equal(-5.825123e+02,mysolver.getArchivedForceOnPhysicalGroup("Edge", 4, 0),1.e-4)







