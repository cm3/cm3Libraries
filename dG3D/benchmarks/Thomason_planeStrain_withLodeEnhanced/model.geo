mm = 1.;
scale = 1./3.;

B = scale*7.5*mm;
R = scale*12.5*mm;
L = scale*32.5*mm;

lsca1 = B*0.1;
lsca2 = L*0.1;
Point(1) = {0,0,0,lsca1};
Point(2) = {L,0,0,lsca2};
Point(3) = {L,B+R,0,lsca2};
Point(4) = {B+R,B+R,0,0.7*lsca2};
Point(5) = {B,B+R,0,0.5*lsca2};
Point(6) = {B,B,0,0.4*lsca2};
Point(7) = {0,B,0,lsca1};
Point(8) = {B*0.2,0,0,lsca1};
Point(9) = {B*0.2,B,0,lsca1};


//+
Symmetry {1, 0, 0, 0} {
  Duplicata { Point{3}; Point{4}; Point{5}; Point{6}; Point{2}; Point{8}; Point{9}; }
}
//+
Line(1) = {4, 3};
//+
Line(2) = {3, 2};
//+
Line(3) = {2, 8};
//+
Line(4) = {8, 15};
//+
Line(5) = {15, 14};
//+
Line(6) = {14, 10};
//+
Line(7) = {10, 11};
//+
Line(8) = {13, 16};
//+
Line(9) = {16,7};
//+
Line(10) = {7, 9};
//+
Line(11) = {9, 6};
//+
Circle(12) = {11, 12, 13};
//+
Circle(13) = {6, 5, 4};
//+
Line Loop(1) = {13,11, 8, 9, 10, 12, 1, 2, 3, 4, 5, 6, 7};
//+
Plane Surface(1) = {1};
//+
Physical Surface(11) = {1};
//+
Physical Line(1) = {5, 4, 3};
//+
Physical Line(2) = {2};
//+
Physical Line(4) = {6};
//+
Physical Point(1) = {14};
//+
Physical Point(2) = {2};
//+
Physical Point(3) = {3};
//+
Physical Point(4) = {10};
//+
Physical Point(5) = {7};
