#coding-Utf-8-*-
from gmshpy import *
from dG3Dpy import*
import numpy as np
import math
import csv

#nstep = int(np.loadtxt("steps.txt", comments='#', delimiter=None))
nstep = 300


#script to launch beam problem with a python script

# material law
lawnum = 11 # unique number of law
properties = "prop_comp.i01"

rho = 1.3e-9 # Bulk mass  

law2 = NonLocalDamageDG3DMaterialLaw(lawnum,rho,properties)

# geometry
geofile  = "model.geo"
meshfile = "model.msh"

# solver
sol = 2  # Gmm=0 (default) Taucs=1 PETsc=2
soltype = 1 # StaticLinear=0 (default) StaticNonLinear=1
#nstep = 480#12000   # number of step (used only if soltype=1)
ftime = 6e2   # Final time (used only if soltype=1)
tol=1.8e-6   # relative tolerance for NR scheme (used only if soltype=1)
nstepArch=4 # Number of step between 2 archiving (used only if soltype=1)
fullDg = 0 #O = CG, 1 = DG
space1 = 0 # function space (Lagrange=0)
beta1  = 100

  
# creation of ElasticField
#matrix
myfield1 = dG3DDomain(11,11,0,lawnum,0,2,1)

# creation of Solver
mysolver = nonLinearMechSolver(1000)
mysolver.createModel(geofile,meshfile,2,2)
mysolver.addDomain(myfield1)
mysolver.addMaterialLaw(law2)
mysolver.Scheme(soltype)
mysolver.Solver(sol)
mysolver.snlData(nstep,ftime,tol,tol/1000.)
mysolver.stepBetweenArchiving(nstepArch)

mysolver.snlManageTimeStep(20,2,2.,150) # maximal 20 times

########################## BC BC BC BC BC ##########################

mysolver.displacementBC("Face",11,2,0.)
mysolver.displacementBC("Edge",1,0,0.)
mysolver.displacementBC("Edge",2,1,0.)

#mysolver.displacementBC("Edge",3,2,0.03*0.01)######################################


pf= True
method=0
mysolver.pathFollowing(pf,method)
if method==0:
	mysolver.setPathFollowingIncrementAdaptation(True,4,0.3)
	mysolver.setPathFollowingControlType(0)
	mysolver.setPathFollowingCorrectionMethod(0)
	mysolver.setPathFollowingArcLengthStep(1e-1)
	mysolver.setBoundsOfPathFollowingArcLengthSteps(0,2.5);
elif method==1:
	mysolver.setPathFollowingIncrementAdaptationLocal(False,5,7)
	#mysolver.setPathFollowingLocalSteps(5e-5,1e-10) # --> Cambiar aqui
	mysolver.setPathFollowingLocalSteps(8e-3,1e-9) # --> Cambiar aqui
	mysolver.setPathFollowingLocalIncrementType(3); 
	mysolver.setPathFollowingSwitchCriterion(1e-3)
	mysolver.setBoundsOfPathFollowingLocalSteps(1e-13,4.)

if pf:
	mysolver.sameDisplacementBC("Edge",3,12,0)
	mysolver.forceBC("Node",12,0,18.e-2)
else:
	L = 20e-3
	mysolver.displacementBC("Edge",3,2,1.14e-3*L)

stopCri = EndSchemeMonitoringWithZeroForceOnPhysical(0.2,"Edge",1,0)
mysolver.endSchemeMonitoring(stopCri)

mysolver.internalPointBuildView("P_xx",IPField.P_XX, 1, 1)
mysolver.internalPointBuildView("P_yy",IPField.P_YY, 1, 1)
mysolver.internalPointBuildView("P_zz",IPField.P_ZZ, 1, 1)
mysolver.internalPointBuildView("P_xz",IPField.P_XZ, 1, 1)
mysolver.internalPointBuildView("P_zx",IPField.P_ZX, 1, 1)

mysolver.internalPointBuildView("F_xx",IPField.F_XX, 1, 1)
mysolver.internalPointBuildView("F_yy",IPField.F_YY, 1, 1)
mysolver.internalPointBuildView("F_zz",IPField.F_ZZ, 1, 1)
mysolver.internalPointBuildView("F_xz",IPField.F_XZ, 1, 1)
mysolver.internalPointBuildView("F_zx",IPField.F_ZX, 1, 1)


mysolver.internalPointBuildView("svm",IPField.SVM, 1, 1)
mysolver.internalPointBuildView("sig_1",IPField.FIRST_PRINCIPAL_STRESS, 1, 1)
mysolver.internalPointBuildView("sig_2",IPField.SECOND_PRINCIPAL_STRESS, 1, 1)
mysolver.internalPointBuildView("sig_3",IPField.THIRD_PRINCIPAL_STRESS, 1, 1)
mysolver.internalPointBuildView("sig_xx",IPField.SIG_XX, 1, 1)
mysolver.internalPointBuildView("sig_yy",IPField.SIG_YY, 1, 1)
mysolver.internalPointBuildView("sig_zz",IPField.SIG_ZZ, 1, 1)
mysolver.internalPointBuildView("sig_xz",IPField.SIG_XZ, 1, 1)

mysolver.internalPointBuildView("epl",IPField.PLASTICSTRAIN, 1, 1)
mysolver.internalPointBuildView("Damage",IPField.DAMAGE, 1, 1)
mysolver.internalPointBuildView("DEFO_ENERGY",IPField.DEFO_ENERGY, 1, 1)

mysolver.internalPointBuildView("DAMAGE0",IPField.DAMAGE_0, 1, 1)
mysolver.internalPointBuildView("DAMAGE1",IPField.DAMAGE_1, 1, 1) 
mysolver.internalPointBuildView("FAILURE_ONSET",IPField.FAILURE_ONSET, 1, 1)
mysolver.internalPointBuildView("FAILURE_PLASTICSTRAIN",IPField.FAILURE_PLASTICSTRAIN, 1, 1)  
mysolver.internalPointBuildView("STRESS_TRIAXIALITY",IPField.STRESS_TRIAXIALITY, 1, 1)  

mysolver.archivingForceOnPhysicalGroup("Edge", 1, 0)
mysolver.archivingNodeDisplacement(12,0)
mysolver.archivingNodeDisplacement(14,1)

mysolver.archivingAverageValue(IPField.P_XY)
mysolver.archivingAverageValue(IPField.P_YY)
mysolver.archivingAverageValue(IPField.P_XX)

mysolver.archivingAverageValue(IPField.P_XY)
mysolver.archivingAverageValue(IPField.P_YX)
mysolver.archivingAverageValue(IPField.P_ZX)
mysolver.archivingAverageValue(IPField.P_XZ)
mysolver.archivingAverageValue(IPField.P_YZ)
mysolver.archivingAverageValue(IPField.P_ZY)

mysolver.archivingAverageValue(IPField.F_ZZ)
mysolver.archivingAverageValue(IPField.F_YY)
mysolver.archivingAverageValue(IPField.F_XX)

mysolver.archivingAverageValue(IPField.F_XY)
mysolver.archivingAverageValue(IPField.F_YX)
mysolver.archivingAverageValue(IPField.F_ZX)
mysolver.archivingAverageValue(IPField.F_XZ)
mysolver.archivingAverageValue(IPField.F_YZ)
mysolver.archivingAverageValue(IPField.F_ZY)

mysolver.archivingAverageValue(IPField.SIG_YY)
mysolver.archivingAverageValue(IPField.SIG_ZZ)
mysolver.archivingAverageValue(IPField.SIG_XZ)
mysolver.archivingAverageValue(IPField.SIG_XX)

mysolver.archivingVolumeIntegralValue(IPField.DEFO_ENERGY)
mysolver.archivingVolumeIntegralValue(IPField.PLASTIC_ENERGY)
mysolver.archivingVolumeIntegralValue(IPField.DAMAGE_ENERGY)


mysolver.solve()

print ('#########################################Begin Gc Compute')
unit = 0.01 ;
r = 0.2*unit;  
L = 10.*unit;
Lhalf = 0.5*L;

list_Gc = []
list_GcMAX = []

reader4 = csv.reader(open("VolumeIntegral_DEFO_ENERGY.csv", "r"), delimiter=";")
x4 = list(reader4)
readerd1= csv.reader(open("NodalDisplacementPhysical12Num2comp0.csv", "r"), delimiter=";")
d1 = list(readerd1)
readerf1= csv.reader(open("force1comp0.csv", "r"), delimiter=";")
f1 = list(readerf1)

Disp1 = np.array(d1).astype("float")
Force1 = np.array(f1).astype("float")
ElastE = np.array(x4).astype("float")
ElastE2 = ElastE[:,1]

eps1= Disp1[:,1]
sig1= -Force1[:,1]
wext=np.zeros(len(eps1))
dis=np.zeros(len(eps1))
sig=np.zeros(len(eps1))
eps=np.zeros(len(eps1))
for i in range(len(eps1)-1):
  dwext=(eps1[i+1]-eps1[i])*(sig1[i]+sig1[i+1])/2.
  wext[i+1]=wext[i]+dwext
  ddis=max((dwext-ElastE2[i+1]+ElastE2[i]),0.)/r
  dis[i+1]=dis[i]+ddis
  eps[i+1]=eps1[i+1]/Lhalf
  sig[i+1]=sig1[i+1]/r


X=dis

PYY2=sig


### Aprox punto maximo
x1 = PYY2[len(PYY2)-1]
x2 = PYY2[len(PYY2)-3]
y1 = X[len(PYY2)-1]
y2 = X[len(PYY2)-3]
y = (0-x1)/(x2-x1)*(y2-y1)+y1
(m,i) = max((v,i) for i,v in enumerate(PYY2))

GcMax = (max(X)-X[i])
Gc = (y-X[i])
print ('GcMAX', GcMax)
print ('Gc', Gc)

list_Gc.append(Gc)
list_GcMAX.append(GcMax)
np.savetxt("Gc.txt", list_Gc, delimiter=",") 
np.savetxt("GcMAX.txt", list_GcMAX, delimiter=",") 
print ('#########################################End Gc Compute')

check = TestCheck()
check.equal(1.185982e-01,Gc,1.e-6)
