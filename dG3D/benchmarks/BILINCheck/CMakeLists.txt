#test file 
set(PYFILE model.py)

set(FILES2DELETE
  Average_*
  disp_step*
  model.msh
  energy.csv
  force1comp0.csv
  GcMAX.txt
  Gc.txt
  NodalDisplacementPhysical1*
  stress_step*
  VolumeIntegral_*
  pathResult_Time_ControlParameter_StateParameter.csv
)
add_cm3python_test(${PYFILE} "${FILES2DELETE}")
