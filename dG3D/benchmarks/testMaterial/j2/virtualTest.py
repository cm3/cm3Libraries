#coding-Utf-8-*-
from gmshpy import *
from dG3Dpy import*
import pandas as pd
import numpy as np
from math import*
from os.path import isfile, join
from os import listdir
import os
import shutil

#material law
uniaxialStress= True
# micro-material law
lawnum = 11 # uniquenumber of law
K = 76.E3 	# Bulk mudulus
mu =26.E3	  # Shear mudulus
rho = 2700.e-9 # Bulk mass
sy0 = 300
h = 0.01*sy0
E = 9.*K*mu/(3.*K+mu)
nu= (3.*K -2.*mu)/2./(3.*K+mu)

# creation of material law
#law1 = dG3DLinearElasticMaterialLaw(lawnum,rho,E,nu)
law1=  J2LinearDG3DMaterialLaw(lawnum,rho,E,nu,sy0,h)


comMat = computeMaterialLaw(law1)

# state component list
allComp = list()

allComp.append([IPField.F_XX, computeMaterialLaw.Current, computeMaterialLaw.Val])
allComp.append([IPField.F_XY, computeMaterialLaw.Current, computeMaterialLaw.Val])
allComp.append([IPField.F_XZ, computeMaterialLaw.Current, computeMaterialLaw.Val])
allComp.append([IPField.F_YX, computeMaterialLaw.Current, computeMaterialLaw.Val])
allComp.append([IPField.F_YY, computeMaterialLaw.Current, computeMaterialLaw.Val])
allComp.append([IPField.F_YZ, computeMaterialLaw.Current, computeMaterialLaw.Val])
allComp.append([IPField.F_ZX, computeMaterialLaw.Current, computeMaterialLaw.Val])
allComp.append([IPField.F_ZY, computeMaterialLaw.Current, computeMaterialLaw.Val])
allComp.append([IPField.F_ZZ, computeMaterialLaw.Current, computeMaterialLaw.Val])


# current F
allComp.append([IPField.GL_XX, computeMaterialLaw.Current, computeMaterialLaw.Val])
allComp.append([IPField.GL_XY, computeMaterialLaw.Current, computeMaterialLaw.Val])
allComp.append([IPField.GL_XZ, computeMaterialLaw.Current, computeMaterialLaw.Val])
allComp.append([IPField.GL_YY, computeMaterialLaw.Current, computeMaterialLaw.Val])
allComp.append([IPField.GL_YZ, computeMaterialLaw.Current, computeMaterialLaw.Val])
allComp.append([IPField.GL_ZZ, computeMaterialLaw.Current, computeMaterialLaw.Val])


#current stress
allComp.append([IPField.S_XX, computeMaterialLaw.Current, computeMaterialLaw.Val])
allComp.append([IPField.S_XY, computeMaterialLaw.Current, computeMaterialLaw.Val])
allComp.append([IPField.S_XZ, computeMaterialLaw.Current, computeMaterialLaw.Val])
allComp.append([IPField.S_YY, computeMaterialLaw.Current, computeMaterialLaw.Val])
allComp.append([IPField.S_YZ, computeMaterialLaw.Current, computeMaterialLaw.Val])
allComp.append([IPField.S_ZZ, computeMaterialLaw.Current, computeMaterialLaw.Val])

#internal variable
allComp.append([IPField.FP_XX, computeMaterialLaw.Current, computeMaterialLaw.Val])
allComp.append([IPField.FP_XY, computeMaterialLaw.Current, computeMaterialLaw.Val])
allComp.append([IPField.FP_XZ, computeMaterialLaw.Current, computeMaterialLaw.Val])
allComp.append([IPField.FP_YX, computeMaterialLaw.Current, computeMaterialLaw.Val])
allComp.append([IPField.FP_YY, computeMaterialLaw.Current, computeMaterialLaw.Val])
allComp.append([IPField.FP_YZ, computeMaterialLaw.Current, computeMaterialLaw.Val])
allComp.append([IPField.FP_ZX, computeMaterialLaw.Current, computeMaterialLaw.Val])
allComp.append([IPField.FP_ZY, computeMaterialLaw.Current, computeMaterialLaw.Val])
allComp.append([IPField.FP_ZZ, computeMaterialLaw.Current, computeMaterialLaw.Val])
allComp.append([IPField.PLASTICSTRAIN, computeMaterialLaw.Current, computeMaterialLaw.Val])
allComp.append([IPField.ISO_YIELD, computeMaterialLaw.Current, computeMaterialLaw.Val])
#


def writeHeader(comMat,allComp, f):
  for l in allComp:
    st = comMat.getString(l[0],l[1],l[2])
    if (l is allComp[-1]):
      f.write('%s\n'%st);
    else:
      f.write('%s;'%st);

def makeTest(srcFolder, dataFile):
  data = pd.read_csv(srcFolder+'/'+dataFile,sep=';')
  strainPath = data.values
  print(strainPath)
  comMat.resetState(law1)
  i = int(dataFile[len('strainPath'):-4])
  fname = 'data_path'+str(i)+'.csv'
  print('create data: ' +fname)
  fl = open(fname,'w')
  fl.write('Time;')
  writeHeader(comMat,allComp,fl)
  time=0.
  TimeStep = 1.
  N = len(strainPath)
  for ite in range(1,N):
    print(f'step = {ite}')
    time = time + TimeStep
    comMat.setTime(time,TimeStep)
    comMat.nextStep()

    # current plane strain
    FxxCur = 1.+ strainPath[ite][0]
    FyyCur = 1.+ strainPath[ite][1]
    FxyCur = strainPath[ite][3]
    FyxCur = strainPath[ite][3]
    # set new value
    comMat.setDeformationGradient(0,0,FxxCur)
    comMat.setDeformationGradient(1,1,FyyCur)
    comMat.setDeformationGradient(0,1,FxyCur)
    comMat.setDeformationGradient(1,0,FyxCur)
    # compute
    if uniaxialStress:
      comMat.computeStressState_uniaxialStress()
    else:
      comMat.computeStressState()
    # get Data
    fl.write('%s'%str(comMat.getTime()))
    for l in allComp:
      val = comMat.getValue(l[0],l[1],l[2])
      fl.write(';%s'%str(val));
    fl.write('\n')
  fl.close()
  return fname

def getFiles(folder, index):
  # get list of files using index
  # index == first strings in the name of files
  onlyfiles = [f for f in listdir(folder) if isfile(join(folder, f))]
  allFiles = list()
  for fname in onlyfiles:
    if fname[:len(index)]==index:
      allFiles.append(fname)
  return allFiles


saveFolder='run-result'
os.system('rm -rf '+saveFolder)
os.system('mkdir '+saveFolder)

strainPathFolders = '.'
allFiles=getFiles(strainPathFolders,'strainPath')
for fname in allFiles:
  res = makeTest(strainPathFolders,fname)
  os.system('mv '+res+ ' '+saveFolder)
