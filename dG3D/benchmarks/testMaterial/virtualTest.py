#coding-Utf-8-*-
from gmshpy import *
from dG3Dpy import*
import pandas as pd
import numpy as np
from math import*
from os.path import isfile, join
from os import listdir
import os
import shutil

#material law
uniaxialStress= True
# micro-material law
lawnum = 11 # uniquenumber of law
K = 76.E3 	# Bulk mudulus
mu =26.E3	  # Shear mudulus
rho = 2700.e-9 # Bulk mass
sy0 = 300
h = 0.01*sy0
E = 9.*K*mu/(3.*K+mu)
nu= (3.*K -2.*mu)/2./(3.*K+mu)

# micro-geometry
micromeshfile="micro.msh" # name of mesh file

# creation of material law
#law1 = dG3DLinearElasticMaterialLaw(lawnum,rho,E,nu)
law1=  J2LinearDG3DMaterialLaw(lawnum,rho,E,nu,sy0,h)

# creation of  micro part Domain
nfield = 5 # number of the field (physical number of entity)
dim =3
myfield1 = dG3DDomain(10,nfield,0,lawnum,0,dim)

microBC = nonLinearPeriodicBC(10,3)
microBC.setOrder(1)
microBC.setBCPhysical(109,111,113,110,112,114)

# periodiodic BC
method = 0 # Periodic mesh = 0 Langrange interpolation = 1 Cubic spline interpolation =2
degree = 5
addVertex = 0
microBC.setPeriodicBCOptions(method, degree,bool(addVertex))

# DEFINE MACROPROBLEM
matnum = 1;
macromat = dG3DMultiscaleMaterialLaw(matnum, 1000)
macromat.setViewAllMicroProblems(True,0)
#
microSolver = macromat.getMicroSolver()
microSolver.loadModel(micromeshfile);
microSolver.addDomain(myfield1)
microSolver.addMaterialLaw(law1);
microSolver.addMicroBC(microBC)
microSolver.snlData(1,1.,1e-6)
microSolver.setSystemType(1)
microSolver.Solver(2)
microSolver.Scheme(1)


#stress averaging flag and averaging method 0- VOLUME, 1- SURFACE
microSolver.stressAveragingFlag(True) # set stress averaging ON- 0 , OFF-1
microSolver.setStressAveragingMethod(0) # 0 -volume 1- surface

if uniaxialStress:
  microSolver.tangentAveragingFlag(True) # set tangent averaging ON -0, OFF -1
  microSolver.setTangentAveragingMethod(2,1e-6) # 0- perturbation 1- condensation

# view
microSolver.setDisplacementAndIPArchiveFlag(True)
microSolver.internalPointBuildView("Green-Lagrange_xx",IPField.STRAIN_XX, 1, 1);
microSolver.internalPointBuildView("Green-Lagrange_yy",IPField.STRAIN_YY, 1, 1);
microSolver.internalPointBuildView("Green-Lagrange_zz",IPField.STRAIN_ZZ, 1, 1);
microSolver.internalPointBuildView("Green-Lagrange_xy",IPField.STRAIN_XY, 1, 1);
microSolver.internalPointBuildView("Green-Lagrange_yz",IPField.STRAIN_YZ, 1, 1);
microSolver.internalPointBuildView("Green-Lagrange_xz",IPField.STRAIN_XZ, 1, 1);
microSolver.internalPointBuildView("sig_xx",IPField.SIG_XX, 1, 1);
microSolver.internalPointBuildView("sig_yy",IPField.SIG_YY, 1, 1);
microSolver.internalPointBuildView("sig_zz",IPField.SIG_ZZ, 1, 1);
microSolver.internalPointBuildView("sig_xy",IPField.SIG_XY, 1, 1);
microSolver.internalPointBuildView("sig_yz",IPField.SIG_YZ, 1, 1);
microSolver.internalPointBuildView("sig_xz",IPField.SIG_XZ, 1, 1);
microSolver.internalPointBuildView("sig_VM",IPField.SVM, 1, 1);
microSolver.internalPointBuildView("Equivalent plastic strain",IPField.PLASTICSTRAIN, 1, 1);

comMat = computeMaterialLaw(macromat)

# state component list
allComp = list()

allComp.append([IPField.F_XX, computeMaterialLaw.Current, computeMaterialLaw.Val])
allComp.append([IPField.F_XY, computeMaterialLaw.Current, computeMaterialLaw.Val])
allComp.append([IPField.F_XZ, computeMaterialLaw.Current, computeMaterialLaw.Val])
allComp.append([IPField.F_YX, computeMaterialLaw.Current, computeMaterialLaw.Val])
allComp.append([IPField.F_YY, computeMaterialLaw.Current, computeMaterialLaw.Val])
allComp.append([IPField.F_YZ, computeMaterialLaw.Current, computeMaterialLaw.Val])
allComp.append([IPField.F_ZX, computeMaterialLaw.Current, computeMaterialLaw.Val])
allComp.append([IPField.F_ZY, computeMaterialLaw.Current, computeMaterialLaw.Val])
allComp.append([IPField.F_ZZ, computeMaterialLaw.Current, computeMaterialLaw.Val])


# current F
allComp.append([IPField.GL_XX, computeMaterialLaw.Current, computeMaterialLaw.Val])
allComp.append([IPField.GL_XY, computeMaterialLaw.Current, computeMaterialLaw.Val])
allComp.append([IPField.GL_XZ, computeMaterialLaw.Current, computeMaterialLaw.Val])
allComp.append([IPField.GL_YY, computeMaterialLaw.Current, computeMaterialLaw.Val])
allComp.append([IPField.GL_YZ, computeMaterialLaw.Current, computeMaterialLaw.Val])
allComp.append([IPField.GL_ZZ, computeMaterialLaw.Current, computeMaterialLaw.Val])


#current stress
allComp.append([IPField.S_XX, computeMaterialLaw.Current, computeMaterialLaw.Val])
allComp.append([IPField.S_XY, computeMaterialLaw.Current, computeMaterialLaw.Val])
allComp.append([IPField.S_XZ, computeMaterialLaw.Current, computeMaterialLaw.Val])
allComp.append([IPField.S_YY, computeMaterialLaw.Current, computeMaterialLaw.Val])
allComp.append([IPField.S_YZ, computeMaterialLaw.Current, computeMaterialLaw.Val])
allComp.append([IPField.S_ZZ, computeMaterialLaw.Current, computeMaterialLaw.Val])

#internal variable
allComp.append([IPField.FP_XX, computeMaterialLaw.Current, computeMaterialLaw.Val])
allComp.append([IPField.FP_XY, computeMaterialLaw.Current, computeMaterialLaw.Val])
allComp.append([IPField.FP_XZ, computeMaterialLaw.Current, computeMaterialLaw.Val])
allComp.append([IPField.FP_YX, computeMaterialLaw.Current, computeMaterialLaw.Val])
allComp.append([IPField.FP_YY, computeMaterialLaw.Current, computeMaterialLaw.Val])
allComp.append([IPField.FP_YZ, computeMaterialLaw.Current, computeMaterialLaw.Val])
allComp.append([IPField.FP_ZX, computeMaterialLaw.Current, computeMaterialLaw.Val])
allComp.append([IPField.FP_ZY, computeMaterialLaw.Current, computeMaterialLaw.Val])
allComp.append([IPField.FP_ZZ, computeMaterialLaw.Current, computeMaterialLaw.Val])
allComp.append([IPField.PLASTICSTRAIN, computeMaterialLaw.Current, computeMaterialLaw.Val])
allComp.append([IPField.ISO_YIELD, computeMaterialLaw.Current, computeMaterialLaw.Val])
#


def writeHeader(comMat,allComp, f):
  for l in allComp:
    st = comMat.getString(l[0],l[1],l[2])
    if (l is allComp[-1]):
      f.write('%s\n'%st);
    else:
      f.write('%s;'%st);

def makeTest(srcFolder, dataFile):
  data = pd.read_csv(srcFolder+'/'+dataFile,sep=';')
  strainPath = data.values
  print(strainPath)
  comMat.resetState(macromat)
  i = int(dataFile[len('strainPath'):-4])
  fname = 'data_path'+str(i)+'.csv'
  print('create data: ' +fname)
  fl = open(fname,'w')
  fl.write('Time;')
  writeHeader(comMat,allComp,fl)
  time=0.
  TimeStep = 1.
  N = len(strainPath)
  for ite in range(1,N):
    print(f'step = {ite}')
    time = time + TimeStep
    comMat.setTime(time,TimeStep)
    comMat.nextStep()

    # current plane strain
    FxxCur = 1.+ strainPath[ite][0]
    FyyCur = 1.+ strainPath[ite][1]
    FxyCur = strainPath[ite][3]
    FyxCur = strainPath[ite][3]
    # set new value
    comMat.setDeformationGradient(0,0,FxxCur)
    comMat.setDeformationGradient(1,1,FyyCur)
    comMat.setDeformationGradient(0,1,FxyCur)
    comMat.setDeformationGradient(1,0,FyxCur)
    # compute
    if uniaxialStress:
      comMat.computeStressState_uniaxialStress()
    else:
      comMat.computeStressState()
    # get Data
    fl.write('%s'%str(comMat.getTime()))
    for l in allComp:
      val = comMat.getValue(l[0],l[1],l[2])
      fl.write(';%s'%str(val));
    fl.write('\n')
  fl.close()
  return fname

def getFiles(folder, index):
  # get list of files using index
  # index == first strings in the name of files
  onlyfiles = [f for f in listdir(folder) if isfile(join(folder, f))]
  allFiles = list()
  for fname in onlyfiles:
    if fname[:len(index)]==index:
      allFiles.append(fname)
  return allFiles


saveFolder='run-result'
os.system('rm -rf '+saveFolder)
os.system('mkdir '+saveFolder)

strainPathFolders = '.'
allFiles=getFiles(strainPathFolders,'strainPath')
for fname in allFiles:
  res = makeTest(strainPathFolders,fname)
  os.system('mv '+res+ ' '+saveFolder)
