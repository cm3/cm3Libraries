#coding-Utf-8-*-

from gmshpy import *
from dG3Dpy import*
# from dG3DpyDebug import*
from math import*
import csv
import numpy as np
import os

# Load the csv data for relaxation spectrum
with open('TPU_relaxationSpectrum_Et_N27_31_07_24_Trefm30.txt', newline='') as csvfile:
    reader = csv.reader(csvfile, delimiter=';')
    relSpec = np.zeros((1,))
    i = 0
    for row in reader:
        if i == 0:
            relSpec[i] = float(' '.join(row))
        else:
            relSpec = np.append(relSpec, float(' '.join(row)))
        i += 1
# print(relSpec)
N = int(0.5*(i-1))
relSpec[0:N+1] *= 1e-6    # convert to MPa

# material law
lawnum = 11 # unique number of law

E = relSpec[0] #MPa # 2700
nu = 0.4
K = E/3./(1.-2.*nu)	# Bulk mudulus
mu = E/2./(1.+nu)	  # Shear mudulus
rho = 1110e-12 # Bulk mass   1110 kg/m3 -> 1110e-12 tonne/mm3
Cp = rho*1850e+6 # 1850 J/kg/K -> 1850e+6 Nmm/tonne/K
KThCon = 0.2332  # 0.2332 W/m/K -> 0.2332 Nmm/s/mm/K
Alpha = 1.5e-4 # 1/K

Tinitial = 273.15 + 23 # K
C1 = 524.39721767
C2 = 699.76815543

# Temp Settings
Tref = 273.15-30.

# TVP
hardenc = LinearExponentialJ2IsotropicHardening(1, 4.5, 5.0, 5., 5.)
hardent = LinearExponentialJ2IsotropicHardening(2, 4.5, 5.0, 7.5, 12.5)

hardenk = PolynomialKinematicHardening(3,1)
hardenk.setCoefficients(0,20.)
hardenk.setCoefficients(1,25.)

law1 = NonLinearTVENonLinearTVPDG3DMaterialLaw(lawnum,rho,E,nu,1e-6,Tinitial,Alpha,KThCon,Cp,False,1e-8,1e-6)

law1.setCompressionHardening(hardenc)
law1.setTractionHardening(hardent)
law1.setKinematicHardening(hardenk)

law1.setYieldPowerFactor(3.5)
law1.setNonAssociatedFlow(True)
law1.nonAssociatedFlowRuleFactor(0.64)

law1.setStrainOrder(-1)
law1.setViscoelasticMethod(0)
law1.setShiftFactorConstantsWLF(C1,C2)
law1.setReferenceTemperature(Tref)

# extra + TVE
law1.setExtraBranchNLType(5)
law1.useExtraBranchBool(True)
law1.useExtraBranchBool_TVE(True,5)
law1.setVolumeCorrection(0.15, 1500.0, 0.75, 0.15, 150.0, 0.75)
law1.setAdditionalVolumeCorrections(8.5, 0.75, 0.028, 8.5, .075, 0.028)
law1.setExtraBranch_CompressionParameter(1.,0.,0.)
law1.setTensionCompressionRegularisation(100.)
law1.setViscoElasticNumberOfElement(N)
if N > 0:
    for i in range(1, N+1):
        law1.setViscoElasticData(i, relSpec[i], relSpec[i+N])
        law1.setCorrectionsAllBranchesTVE(i, 0.15, 1500.0, 0.75, 0.15, 150.0, 0.75)
        law1.setCompressionCorrectionsAllBranchesTVE(i, 1.0)
        law1.setAdditionalCorrectionsAllBranchesTVE(i, 8.5, 0.75, 0.028, 8.5, .075, 0.028)

mullins = linearScaler(4, 0.99)
law1.setMullinsEffect(mullins)

eta = constantViscosityLaw(1,1000.)
law1.setViscosityEffect(eta,0.21)

# solver
sol = 2  # Gmm=0 (default) Taucs=1 PETsc=2
soltype = 1 # StaticLinear=0 (default) StaticNonLinear=1
nstep = 1000   # number of step (used only if soltype=1)
ftime = 1.8/7.37e-4 # 0.001   # Final time (used only if soltype=1))
tol=1.e-5  # relative tolerance for NR scheme (used only if soltype=1)
nstepArch=1 # Number of step between 2 archiving (used only if soltype=1)
fullDg = 0 #O = CG, 1 = DG
space1 = 0 # function space (Lagrange=0)
beta1  = 100


# geometry
geofile = "cube.geo" # "line.geo"
meshfile = "cube.msh" # "line.msh"
nfield = 29

pertFactor = 1e-8
pertLaw1 = dG3DMaterialLawWithTangentByPerturbation(law1,pertFactor)
ThermoMechanicsEqRatio = 1.e1
thermalSource = True
mecaSource = True
myfield1 = ThermoMechanicsDG3DDomain(1000,nfield,space1,lawnum,fullDg,ThermoMechanicsEqRatio)
myfield1.setConstitutiveExtraDofDiffusionAccountSource(thermalSource,mecaSource)
myfield1.stabilityParameters(beta1)
myfield1.ThermoMechanicsStabilityParameters(beta1,bool(1))
# myfield1.strainSubstep(2, 10)

# creation of Solver
mysolver = nonLinearMechSolver(1000)
mysolver.createModel(geofile,meshfile,3,2)
#mysolver.loadModel(meshfile)
mysolver.addDomain(myfield1)
mysolver.addMaterialLaw(law1)
mysolver.Scheme(soltype)
mysolver.Solver(sol)
mysolver.snlData(nstep,ftime,tol)
mysolver.stepBetweenArchiving(nstepArch)
mysolver.snlManageTimeStep(15,5,2.,20) # maximal 20 times

# BCs
length = 1.
umax = 1.8*length
fu_L = LinearFunctionTime(0,0,ftime,umax);    # Linear Displacement with time

mysolver.displacementBC("Face",30,0,0.)		        # face x = 0   - Left Face fixed
mysolver.displacementBC("Face",31,0,fu_L)         # face x = L   - Right Face moving
mysolver.displacementBC("Face",34,1,0.)		    # face y = 0
mysolver.displacementBC("Face",32,2,0.)		    # face z = 0

# thermal BC
mysolver.initialBC("Volume","Position",nfield,3,Tinitial)
fT = LinearFunctionTime(0,Tinitial,ftime,Tinitial)    # Linear Displacement with time
# mysolver.displacementBC("Volume",nfield,3,fT)

# Field-Output
mysolver.internalPointBuildView("strain_zz", IPField.STRAIN_ZZ, 1, nstepArch);
mysolver.internalPointBuildView("svm",IPField.SVM, 1, nstepArch)
mysolver.internalPointBuildView("sig_xx",IPField.SIG_XX, 1, nstepArch)
mysolver.internalPointBuildView("sig_yy",IPField.SIG_YY, 1, nstepArch)
mysolver.internalPointBuildView("sig_zz",IPField.SIG_ZZ, 1, nstepArch)
mysolver.internalPointBuildView("sig_xy",IPField.SIG_XY, 1, nstepArch)
mysolver.internalPointBuildView("sig_yz",IPField.SIG_YZ, 1, nstepArch)
mysolver.internalPointBuildView("sig_xz",IPField.SIG_XZ, 1, nstepArch)
mysolver.internalPointBuildView("epl",IPField.PLASTICSTRAIN, 1, nstepArch)
mysolver.internalPointBuildView("temperature",IPField.TEMPERATURE, 1, 1)
mysolver.internalPointBuildView("mechSrc",IPField.MECHANICAL_SOURCE, 1, 1)
mysolver.internalPointBuildView("qx",IPField.THERMALFLUX_X, 1, nstepArch)
mysolver.internalPointBuildView("qy",IPField.THERMALFLUX_Y, 1, nstepArch)
mysolver.internalPointBuildView("qz",IPField.THERMALFLUX_Z, 1, nstepArch)
mysolver.archivingAverageValue(IPField.hyperElastic_BulkScalar)
mysolver.archivingAverageValue(IPField.hyperElastic_ShearScalar)

# Print/Archive History Outputs
# mysolver.archivingForceOnPhysicalGroup("Face", 31, 0, nstepArch)  # Force on the right force
mysolver.archivingForceOnPhysicalGroup("Face", 30, 0, nstepArch)  # Force on the left face  === Reaction Force
# mysolver.archivingForceOnPhysicalGroup("Node", 41, 0, nstepArch)  # Force on a point on the left face === Reaction Force
mysolver.archivingNodeDisplacement(43, 0, nstepArch)              # Displacement of a point on the right face
mysolver.archivingIPOnPhysicalGroup("Volume",nfield, IPField.TEMPERATURE,IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",nfield, IPField.MECHANICAL_SOURCE,IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",nfield, IPField.PLASTICSTRAIN,IPField.MEAN_VALUE);

# Solve
mysolver.solve()

# Check
check = TestCheck()
check.equal(-8.741258e+00,mysolver.getArchivedForceOnPhysicalGroup("Face", 30, 0),1.e-4)
