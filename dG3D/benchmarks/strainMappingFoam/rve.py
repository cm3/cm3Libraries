#coding-Utf-8-*-

from gmshpy import *
from dG3Dpy import*

import math

# material law
lawnum1 = 11 # unique number of law
E = 68.9E3 #Young modulus
nu = 0.33 #Poisson ratio
K = E/3./(1.-2.*nu)
mu = E/2./(1+nu)
sy0 = 276. #Yield stress
h = E/100 # hardening modulus
rho = 1.  #density

# creation of material law
harden = LinearExponentialJ2IsotropicHardening(1, sy0, h, 0., 10.)
law1   = J2LinearDG3DMaterialLaw(lawnum1,rho,E,nu,harden)


# geometry
meshfile="rve1.msh" # name of mesh file

# creation of part Domain
nfield = 11 # number of the field (physical number of entity)
dim =2

myfield1 = dG3DDomain(10,11,0,lawnum1,0,2)


# solver
sol = 2  # Gmm=0 (default) Taucs=1 PETsc=2
soltype =1 # StaticLinear=0 (default) StaticNonLinear=1
nstep = 50  # number of step (used only if soltype=1)
ftime =1.   # Final time (used only if soltype=1)
tol=1.e-5  # relative tolerance for NR scheme (used only if soltype=1)
nstepArch=1 # Number of step between 2 archiving (used only if soltype=1)

# creation of Solver
mysolver = nonLinearMechSolver(1000)
mysolver.loadModel(meshfile)
mysolver.addDomain(myfield1)
mysolver.addMaterialLaw(law1)
mysolver.Scheme(soltype)
mysolver.Solver(sol)
mysolver.snlData(nstep,ftime,tol)


mysolver.pathFollowing(True,0)
mysolver.setPathFollowingControlType(1)
mysolver.setPathFollowingArcLengthStep(0.1)

#rve - periodicity
mysolver.displacementBC("Edge",1,1,0)
mysolver.displacementBC("Edge",4,0,0)
mysolver.forceBC("Edge",3,1,-10)

mysolver.createStrainMapping("box.msh",3)


# build view
mysolver.internalPointBuildView("sig_xx",IPField.SIG_XX, 1, 1);
mysolver.internalPointBuildView("sig_yy",IPField.SIG_YY, 1, 1);
mysolver.internalPointBuildView("sig_zz",IPField.SIG_ZZ, 1, 1);
mysolver.internalPointBuildView("sig_xy",IPField.SIG_XY, 1, 1);
mysolver.internalPointBuildView("sig_yz",IPField.SIG_YZ, 1, 1);
mysolver.internalPointBuildView("sig_xz",IPField.SIG_XZ, 1, 1);
mysolver.internalPointBuildView("sig_VM",IPField.SVM, 1, 1);
mysolver.internalPointBuildView("Equivalent plastic strain",IPField.PLASTICSTRAIN, 1, 1);

mysolver.internalPointBuildView("F_xx",IPField.F_XX, 1, 1);
mysolver.internalPointBuildView("F_xy",IPField.F_XY, 1, 1);
mysolver.internalPointBuildView("F_yx",IPField.F_YX, 1, 1);
mysolver.internalPointBuildView("F_yy",IPField.F_YY, 1, 1);



# solve
mysolver.solve()


