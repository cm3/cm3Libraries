#coding-Utf-8-*-
from gmshpy import*
from dG3Dpy import*
from math import*

# Test based on Gurson-Thomason + two nucleation laws for a cube
# = similar to GursonBeremin_Cube_multipleLaws
# - check the resulting force amplitude
# - check the law inhibition thanks to the nucleated amount of both laws
# - check the angle rotation



# Material law creation
# ==========================================================
# general properties
ibulkLaw1 = 11		# law identifier
rho   = 7850 		# density
young = 205.4e9		# module de young
nu    = 0.3		# poisson coefficient

# hardening law
sy0   = 862.4e6
h = 3399. *1e6
p1 = 0.012
n1 = 0.084
p2 = 0.056
n2 = 0.058
harden1 = LinearFollowedByMultiplePowerLawJ2IsotropicHardening(ibulkLaw1,sy0,h,p1,n1,p2,n2)

# n-l law
l = 100.*1.e-6
cl = l*l
lengthLaw1 = IsotropicCLengthLaw(ibulkLaw1, cl)

# nucleation : this law will be active
fn = 2.0e-3
sn = 0.03/6.
en = 1000. #determined by the criterion
function_cracking = GaussianNucleationFunctionLaw(0, fn, sn, en)
bebere_cracking = BereminNucleationCriterionLaw(0, sy0*1.2, sy0*1.2, sy0*1., 0.6, 0.6, 1.6)
bebere_cracking.rotateMaterialAxes(90.,90.,0.)
bebere_cracking.addAnExclusionRule(1)

# this law will be not active
fn_deco = 2.0e-3*1/0.2
sn = 0.03/6.
function_deco = GaussianNucleationFunctionLaw(1, fn_deco, sn, en)
bebere_deco = BereminNucleationCriterionLaw(1, sy0, sy0, sy0*1.2, 0.6, 0.6, 1.6)
bebere_deco.rotateMaterialAxes(90.,90.,0.)
bebere_deco.addAnExclusionRule(0)


# growth law
q1    = 1.414
q2    = 1.
q3    = q1
fVinitial = 1.e-5

# coalescence
alpha = 0.1+0.217*n2+4.83*n2*n2
beta = 1.24
coalLaw = ThomasonCoalescenceLaw(ibulkLaw1)
coalLaw.setThomasonCoefficient(alpha,beta)

# void law
lambda0 = 1.0
kappa = 1.22
voidlaw = SphericalVoidStateEvolutionLawWithMatrixPlasticDeformation(ibulkLaw1,lambda0,kappa)
fct_chi = fourVariableExponentialSaturationScalarFunction(1.e-3, 1.e-5, 0.995, 0.997);
voidlaw.setRegularizedFunction(fct_chi)


bulkLaw1 = NonLocalPorousCoupledDG3DMaterialLaw( ibulkLaw1, young, nu, rho, q1, q2, q3, fVinitial, lambda0, kappa, harden1, lengthLaw1, 1.e-12, False)
bulkLaw1.setYieldSurfaceExponent(10)
bulkLaw1.setOrderForLogExp(9)
bulkLaw1.setNucleationLaw(function_cracking,bebere_cracking)
bulkLaw1.setNucleationLaw(function_deco,bebere_deco)
bulkLaw1.setSubStepping(bool(1),6)
bulkLaw1.setStressFormulation(1)
bulkLaw1.setCoalescenceLaw(coalLaw)
bulkLaw1.setCoalescenceVoidEvolutionLaw(voidlaw)





# Solver parameters
# ===================================================================================
soltype = 1 		# StaticLinear=0 (default, StaticNonLinear=1, Explicit=2, 
			# Multi=3, Implicit=4, Eigen=5)
nstep = 200   		# Number of step
ftime =1.0   		# Final time
tol=1.e-6  		# Relative tolerance for NR scheme
tolAbs = 1.e-16		# Absolute tolerance for NR scheme
nstepArch=10		# Number of step between 2 archiving
nstepArchEnergy = 1	# Number of step between 2 energy computation and archiving
nstepArchForce = 1	# Number of step between 2 force archiving
MaxIter = 25		# Maximum number of iterations
StepIncrease = 3	# Number of successfull timestep before reincreasing it
StepReducFactor = 5.0 	# Timestep reduction factor
NumberReduction = 4	# Maximum number of timespep reduction: max reduction = pow(StepReducFactor,this)
fullDg = bool(1)        # O = CG, 1 = DG
dgnl = bool(1)		# DG for non-local variables inside a domain (only if fullDg)
eqRatio = 1.0e6		# Ratio between "elastic" and non-local equations 
space1 = 0 		# Function space (Lagrange=0)
beta1  = 30.0		# Penality parameter for DG

# Domain creation
## ===================================================================================
numPhysVolume1 = 29 		# Number of a physical volume of the model in .geo
numDomain1 = 1000 		# Number of the domain
field1 = dG3DDomain(numDomain1,numPhysVolume1,space1,ibulkLaw1,fullDg,3,1)
field1.stabilityParameters(beta1) 			# Adding stability parameters (for DG)
field1.setNonLocalStabilityParameters(beta1,dgnl) 		# Adding stability parameters (for DG)
field1.setNonLocalEqRatio(eqRatio)
field1.gaussIntegration(0,-1,-1)
#field1.matrixByPerturbation(1,1,1,1e-8) 		# Tangent computation analytically or by pertubation


# Solver creation
# ===================================================================================
mysolver = nonLinearMechSolver(numDomain1) 		# Solver associated with numSolver1
geofile="cube.geo"
meshfile= "cube.msh" 			# name of mesh file
mysolver.createModel(geofile,meshfile,3,1)
#mysolver.loadModel(meshfile)		# add mesh
mysolver.addDomain(field1) 		# add domain
mysolver.addMaterialLaw(bulkLaw1) 	# add material law
mysolver.Solver(2) 			# Library solving: Gmm=0 (default) Taucs=1 PETsc=2
# solver parametrisation
mysolver.Scheme(soltype) 			# solver scheme	 				
mysolver.snlData(nstep,ftime,tol,tolAbs) 	# solver parameters
mysolver.snlManageTimeStep(MaxIter,StepIncrease,StepReducFactor,NumberReduction) #timestep
# solver archiving
mysolver.stepBetweenArchiving(nstepArch) 	# archiving frequency
mysolver.energyComputation(nstepArchEnergy)	# archiving frequency for energy
mysolver.lineSearch(bool(0))		# lineSearch activation
mysolver.options("-ksp_type preonly -pc_type lu -pc_factor_mat_solver_type mumps")

# Boundary conditions
# ===============================
tot_disp = 2.*1.e-3*0.2 # Max disp == elongation 25 pourcent
mysolver.displacementBC("Face",30,0,0.)		# face x = 0
mysolver.displacementBC("Face",31,0,tot_disp) # face x = L
mysolver.displacementBC("Face",34,1,0.)		# face y = 0
mysolver.displacementBC("Face",32,2,0.)		# face z = 0

mysolver.initialBC("Volume","Position",29,3,fVinitial)

# Variable storage
# ===============================
# IPField
mysolver.internalPointBuildView("svm",IPField.SVM, 1, 1)
mysolver.internalPointBuildView("sig_xx",IPField.SIG_XX, 1, 1)
mysolver.internalPointBuildView("sig_yy",IPField.SIG_YY, 1, 1)
mysolver.internalPointBuildView("sig_zz",IPField.SIG_ZZ, 1, 1)
mysolver.internalPointBuildView("sig_xz",IPField.SIG_XZ, 1, 1)
mysolver.internalPointBuildView("epl",IPField.PLASTICSTRAIN, 1, 1)
mysolver.internalPointBuildView("triaxiality",IPField.STRESS_TRIAXIALITY, 1, 1)
mysolver.internalPointBuildView("lode",IPField.LODE_PARAMETER, 1, 1)
mysolver.internalPointBuildView("local_porosity",IPField.LOCAL_POROSITY, 1, 1)
mysolver.internalPointBuildView("non_local_porosity",IPField.NONLOCAL_POROSITY, 1, 1)
mysolver.internalPointBuildView("fv_cracking",IPField.NUCLEATED_POROSITY_0, 1, 1)
mysolver.internalPointBuildView("fv_deco",IPField.NUCLEATED_POROSITY_1, 1, 1)
mysolver.internalPointBuildView("chi_max",IPField.LIGAMENT_RATIO, 1, 3)
mysolver.internalPointBuildView("coales",IPField.COALESCENCE, 1, 1)


mysolver.archivingForceOnPhysicalGroup("Face", 31, 0, nstepArchForce)
mysolver.archivingNodeDisplacement(43,0, nstepArchForce)
mysolver.archivingIPOnPhysicalGroup("Volume",29, IPField.LOCAL_POROSITY,IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",29, IPField.CORRECTED_POROSITY,IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",29, IPField.NUCLEATED_POROSITY_0,IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",29, IPField.NUCLEATED_POROSITY_1,IPField.MEAN_VALUE);

# Solving
# ===========
mysolver.solve()






# Test :
# ===========
check = TestCheck()

# the force amplitude
check.equal(3.648721e+03, mysolver.getArchivedForceOnPhysicalGroup("Face", 31, 0), 1.e-5)

import csv

# the local porosity amount
data = csv.reader(open('IPVolume29val_LOCAL_POROSITYMean.csv'), delimiter=';')
porosity_loc = list(data)
check.equal(2.400366e-03,float(porosity_loc[-1][1]),1e-5)



# the nucleated amount and the nucleation inhibition
# - this value = 0 by construction (this law is normally inhibited)
data = csv.reader(open('IPVolume29val_NUCLEATED_POROSITY_1Mean.csv'), delimiter=';')
porosity_loc = list(data)
check.equal(0.0,float(porosity_loc[-1][1]),1e-8)

# - check the theoritical value
data = csv.reader(open('IPVolume29val_NUCLEATED_POROSITY_0Mean.csv'), delimiter=';')
porosity_loc = list(data)
check.equal(fn,float(porosity_loc[-1][1]),1e-2)

# - check the numerical reference value
data = csv.reader(open('IPVolume29val_NUCLEATED_POROSITY_0Mean.csv'), delimiter=';')
porosity_loc = list(data)
check.equal(1.997330e-03,float(porosity_loc[-1][1]),1e-5)




