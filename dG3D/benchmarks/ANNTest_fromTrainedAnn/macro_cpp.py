#coding-Utf-8-*-
import pandas as pd
import numpy as np
import sys
from gmshpy import *
from dG3Dpy import*
import tensorflow as tf

def createANNFromWeights(W):
  nlayers = int(len(W)/2)
  ann = ArtificialNN()
  idex = 0
  for il in range(nlayers):
    Wi = W[idex]
    bi = W[idex+1]
    idex = idex+2
    #print(Wi.shape,bi.shape)
    nbInputs = Wi.shape[0]
    nbOutputs = Wi.shape[1]
    
    if il == nlayers-1:
      layer = LinearDenseLayer(nbInputs,nbOutputs)
    else:
      layer = LeakyReluDenseLayer(nbInputs,nbOutputs,0.01)
    
    for i in range(0,nbInputs):
      for j in range(0,nbOutputs):
        s = float(Wi[i,j])
        layer.setWeights(i,j,s)  
    for i in range(0,nbOutputs):
      s = float(bi[i])
      layer.setBias(i,s)
    #    
    ann.addLayer(layer)
  return ann
  

qANNFile = 'ann_tf/bestmodel_q_12X100X100X100X100X100X100X100X100X100X100X6.h5'
sigANNFile = 'ann_tf/bestmodel_sig_9X100X100X100X100X4.h5'
qANN = tf.keras.models.load_model(qANNFile)
sigANN =tf.keras.models.load_model(sigANNFile)
Wq =qANN.get_weights()
Wsig = sigANN.get_weights()
# create ann from weight of tensorflow
ann_q = createANNFromWeights(Wq)
ann_q.print_infos()
ann_sig = createANNFromWeights(Wsig)
ann_sig.print_infos()

GLVars = ["GL_XX", "GL_XY", "GL_YY"] 
loc_EVars = [0,3,1] # position in global position - xx yy zz xy xz yz
QVars = ["FP_XX", "FP_XY", "FP_YX", "FP_YY", "FP_ZZ", "PLASTICSTRAIN"]
SVars = ["S_XX", "S_XY", "S_YY", "S_ZZ"]
loc_SVars = [0,3,1,2] # position in global position - xx yy zz xy xz yz

fct1 = TwoANNMIMOFunction(ann_sig,ann_q)
#fct1.blockInternalState(True)
fct1.setNumberOfStrainVariables(3)
for i, val in enumerate(loc_EVars):
  fct1.setStrainLocation(i,val)
fct1.setNumberOfStressVariables(4)
for i, val in enumerate(loc_SVars):
  fct1.setStressLocation(i,val)

#DEFINE MICRO PROBLEM
lnum1= 11
numInterVar = len(QVars)
rho = 1e-9

QVarsIntialWith1 = ["FP_XX","FP_YY", "FP_ZZ"]

macromat1 = ANNBasedDG3DMaterialLaw(lnum1,rho,numInterVar,fct1,False,1e-8)
macromat1.setKinematicInput(1) # GL is used as kinematic variable
for qV in QVarsIntialWith1:
  index = QVars.index(qV)
  macromat1.setInitialInternalComp(index,1.)

#pertmacromat1 = dG3DMaterialLawWithTangentByPerturbation(macromat1,1e-4)

macromeshfile="macro.msh" # name of mesh file
macrogeofile="macro.geo"
# solver
sol = 2  # Gmm=0 (default) Taucs=1 PETsc=2
soltype =1 # StaticLinear=0 (default) StaticNonLinear=1
nstep = 30  # number of step (used only if soltype=1)
ftime =1.   # Final time (used only if soltype=1)
tol=1.e-6  # relative tolerance for NR scheme (used only if soltype=1)
nstepArch=1 # Number of step between 2 archiving (used only if soltype=1)


# creation of  macro part Domain

beta1 = 50;
fullDG = False;

averageStrainBased = True

# non DG domain
nfield1 = 1
macrodomain1 = dG3DDomain(10,nfield1,0,lnum1,fullDG,2)
macrodomain1.stabilityParameters(beta1)
#macrodomain1.matrixByPerturbation(1,1,1,1e-8)

# creation of Solver
mysolver = nonLinearMechSolver(1000)
mysolver.loadModel(macromeshfile)
mysolver.addDomain(macrodomain1)
mysolver.addMaterialLaw(macromat1)
#mysolver.addMaterialLaw(pertmacromat1)
mysolver.Scheme(soltype)
mysolver.Solver(sol)
mysolver.snlData(nstep,ftime,tol)

#stiffModif = NumIterationBasedStiffnessModificationMonitoring(1)
#stiffModif.modifyStiffnessAfterNumIterations(1)
#mysolver.stiffnessModification(stiffModif)

#mysolver.snlManageTimeStep(250,50,2.,10)

# boundary condition
mysolver.displacementBC("Face",1,2,0.0)
mysolver.displacementBC("Edge",2,0,0.0)
mysolver.displacementBC("Edge",2,1,0.0)
mysolver.displacementBC("Edge",4,0,0.0)
mysolver.displacementBC("Edge",3,1,10.)
#mysolver.forceBC("Edge",3,1,1e2)

# archivage
mysolver.internalPointBuildView("Green-Lagrange_xx",IPField.GL_XX, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_yy",IPField.GL_YY, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_zz",IPField.GL_ZZ, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_xy",IPField.GL_XY, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_yz",IPField.GL_YZ, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_xz",IPField.GL_XZ, 1, 1);
mysolver.internalPointBuildView("sig_xx",IPField.SIG_XX, 1, 1);
mysolver.internalPointBuildView("sig_yy",IPField.SIG_YY, 1, 1);
mysolver.internalPointBuildView("sig_zz",IPField.SIG_ZZ, 1, 1);
mysolver.internalPointBuildView("sig_xy",IPField.SIG_XY, 1, 1);
mysolver.internalPointBuildView("sig_yz",IPField.SIG_YZ, 1, 1);
mysolver.internalPointBuildView("sig_xz",IPField.SIG_XZ, 1, 1);
mysolver.internalPointBuildView("sig_VM",IPField.SVM, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange equivalent strain",IPField.GL_EQUIVALENT_STRAIN, 1, 1);
mysolver.internalPointBuildView("Equivalent plastic strain",IPField.PLASTICSTRAIN, 1, 1);
mysolver.internalPointBuildView("Damage",IPField.DAMAGE,1,1);
mysolver.internalPointBuildView("USER0",IPField.USER0,1,1);
mysolver.internalPointBuildView("USER1",IPField.USER1,1,1);
mysolver.internalPointBuildView("USER2",IPField.USER2,1,1);
mysolver.internalPointBuildView("USER3",IPField.USER3,1,1);
mysolver.internalPointBuildView("USER4",IPField.USER4,1,1);
mysolver.internalPointBuildView("USER5",IPField.USER5,1,1);

mysolver.archivingForceOnPhysicalGroup("Edge",2,0)
mysolver.archivingForceOnPhysicalGroup("Edge",2,1)


# solve
mysolver.solve()

check = TestCheck()
check.equal(1.535876e+03,mysolver.getArchivedForceOnPhysicalGroup("Edge", 2, 0),1.e-4)
