import tensorflow as tf
from gmshpy import *
from dG3Dpy import*
import numpy as np

def createANNFromWeights(W):
  nlayers = int(len(W)/2)
  ann = ArtificialNN()
  idex = 0
  for il in range(nlayers):
    Wi = W[idex]
    bi = W[idex+1]
    idex = idex+2
    #print(Wi.shape,bi.shape)
    nbInputs = Wi.shape[0]
    nbOutputs = Wi.shape[1]
    
    if il == nlayers-1:
      layer = LinearDenseLayer(nbInputs,nbOutputs)
    else:
      layer = LeakyReluDenseLayer(nbInputs,nbOutputs,0.01)
    
    for i in range(0,nbInputs):
      for j in range(0,nbOutputs):
        s = float(Wi[i,j])
        layer.setWeights(i,j,s)  
    for i in range(0,nbOutputs):
      s = float(bi[i])
      layer.setBias(i,s)
    #    
    ann.addLayer(layer)
  return ann



qANNFile = 'ann_tf/bestmodel_q_12X100X100X100X100X100X100X100X100X100X100X6.h5'
sigANNFile = 'ann_tf/bestmodel_sig_9X100X100X100X100X4.h5'
qANN = tf.keras.models.load_model(qANNFile)
sigANN =tf.keras.models.load_model(sigANNFile)

qANN.summary()
sigANN.summary()

Wq =qANN.get_weights()
Wsig = sigANN.get_weights()


myAnn= createANNFromWeights(Wq)
myAnn.print_infos()

numInputs = myAnn.getNumInputs()
numOutputs = myAnn.getNumOutputs()
print(f"numInputs = {numInputs}, numOutputs = {numOutputs}")

x = np.random.rand(1,numInputs)
y = qANN.predict(x)
print("x = ",x)
print("ann-tf: y = ",y)

xMat = fullMatrixDouble(1,numInputs)
for i in range(numInputs):
  xMat.set(0,i,x[0,i])
xMat._print("xMat")
yMat = fullMatrixDouble()
myAnn.predict(xMat,yMat)
yMat._print("yMat")


