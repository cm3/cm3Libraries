#coding-Utf-8-*-
from gmshpy import *
from dG3Dpy import*

#script to launch PBC problem with a python script

#DEFINE MICRO PROBLEM

# micro-material law

E = 3.2E3
nu = 0.3
K = E/3./(1.-2.*nu)	# Bulk mudulus
mu =E/2./(1.+nu)	  # Shear mudulus
rho = 2.7e-9 # Bulk mass

# creation of material law

law1 = dG3DLinearElasticMaterialLaw(11,rho,E,nu)
law1.setUseBarF(True)


micromeshfile="rve.msh" # name of mesh file

# creation of part Domain
myfield1 = dG3DDomain(10,11,0,11,0,3)

microBC = nonLinearPeriodicBC(1000,3)
microBC.setOrder(1)
microBC.setBCPhysical(1,3,5,2,4,6) #Periodic boundary condition

method =0	# Periodic mesh = 0, Langrange interpolation = 1, Cubic spline interpolation =2,  FE linear= 3, FE Quad = 4
degree = 2	# Order used for polynomial interpolation 
addvertex = 0 # Polynomial interpolation by mesh vertex = 0, Polynomial interpolation by virtual vertex 
microBC.setPeriodicBCOptions(method, degree,bool(addvertex)) 

# DEFINE MACROPROBLEM
matnum1 = 2;

macromat1 = dG3DElasticMultiscaleMaterialLaw(matnum1, 1000)
macromat1.setUseBarF(True)

microSolver1 = macromat1.getMicroSolver()
microSolver1.loadModel(micromeshfile);
microSolver1.addDomain(myfield1)
microSolver1.addMaterialLaw(law1);
microSolver1.addMicroBC(microBC)

microSolver1.snlData(1,1.,1e-6,1e-10)
microSolver1.setSystemType(1)
microSolver1.Solver(2)
microSolver1.Scheme(1)
microSolver1.setSameStateCriterion(1e-16)
microSolver1.stressAveragingFlag(True) # set stress averaging ON- 0 , OFF-1
microSolver1.setStressAveragingMethod(0)
microSolver1.tangentAveragingFlag(True) # set tangent averaging ON -0, OFF -1
microSolver1.setTangentAveragingMethod(2,1e-6);

macromeshfile="macro.msh" # name of mesh file
macrogeofile="macro.geo"
# solver
sol = 2  # Gmm=0 (default) Taucs=1 PETsc=2
soltype =1 # StaticLinear=0 (default) StaticNonLinear=1
nstep = 1  # number of step (used only if soltype=1)
ftime =1.   # Final time (used only if soltype=1)
tol=1.e-8  # relative tolerance for NR scheme (used only if soltype=1)
nstepArch=1 # Number of step between 2 archiving (used only if soltype=1)


# creation of  macro part Domain

dim =3
beta1 = 50;
fullDG = False;

averageStrainBased = True

# non DG domain
nfield1 = 11
macrodomain1 = dG3DDomain(10,11,0,matnum1,fullDG,3)
macrodomain1.stabilityParameters(beta1)

# creation of Solver
mysolver = nonLinearMechSolver(1000)
mysolver.createModel(macrogeofile,macromeshfile,3,1)
#mysolver.loadModel(macromeshfile)
mysolver.addDomain(macrodomain1)
mysolver.addMaterialLaw(macromat1)

mysolver.Scheme(soltype)
mysolver.Solver(sol)
mysolver.snlData(nstep,ftime,tol)

# boundary condition
mysolver.displacementBC("Face",45,0,0.0)
mysolver.displacementBC("Face",45,1,0.0)
mysolver.displacementBC("Face",45,2,0.0)

mysolver.displacementBC("Face",46,1,10.)
#mysolver.displacementBC("Face",46,0,0.)

# archivage
mysolver.internalPointBuildView("Green-Lagrange_xx",IPField.STRAIN_XX, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_yy",IPField.STRAIN_YY, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_zz",IPField.STRAIN_ZZ, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_xy",IPField.STRAIN_XY, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_yz",IPField.STRAIN_YZ, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_xz",IPField.STRAIN_XZ, 1, 1);
mysolver.internalPointBuildView("sig_xx",IPField.SIG_XX, 1, 1);
mysolver.internalPointBuildView("sig_yy",IPField.SIG_YY, 1, 1);
mysolver.internalPointBuildView("sig_zz",IPField.SIG_ZZ, 1, 1);
mysolver.internalPointBuildView("sig_xy",IPField.SIG_XY, 1, 1);
mysolver.internalPointBuildView("sig_yz",IPField.SIG_YZ, 1, 1);
mysolver.internalPointBuildView("sig_xz",IPField.SIG_XZ, 1, 1);
mysolver.internalPointBuildView("sig_VM",IPField.SVM, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange equivalent strain",IPField.GL_EQUIVALENT_STRAIN, 1, 1);
mysolver.internalPointBuildView("Equivalent plastic strain",IPField.PLASTICSTRAIN, 1, 1);
mysolver.internalPointBuildView("Damage",IPField.DAMAGE,1,1);

mysolver.archivingForceOnPhysicalGroup("Face",45,0)
mysolver.archivingForceOnPhysicalGroup("Face",45,1)


# solve
mysolver.solve()

check = TestCheck()
check.equal(-2.504277e+04,mysolver.getArchivedForceOnPhysicalGroup("Face", 45, 1),1.e-4)

