#coding-Utf-8-*-
from gmshpy import *
from dG3Dpy import*

#script to launch beam problem with a python script

# material law
lawnum = 11 # unique number of law

E = 2.45E3 #MPa
nu = 0.39
K = E/3./(1.-2.*nu)	# Bulk mudulus
mu =E/2./(1.+nu)	  # Shear mudulus
rho = 7850. # Bulk mass

sy0c = 48.
fact = 0.85;
sy0t = sy0c*fact;


#  compute solution and BC (given directly to the solver
hardenc = ExponentialJ2IsotropicHardening(1,sy0c,70.*2.5,37.)
hardent = ExponentialJ2IsotropicHardening(2,sy0t,70.*fact*2.5,37.)

hardenk = PolynomialKinematicHardening(3,4)
ff = 0.75*(1-0.2)/((1-0.62)*(1-0.62))
hardenk.setCoefficients(2,170.*ff)
hardenk.setCoefficients(3,-440.*ff)
hardenk.setCoefficients(4,1100.*ff)

l = 4e-3
c = l*l
#saturation law
H = 30.
Dinf = 0.62
pi = 0.0
alpha = 1
damlaw1 = SimpleSaturateDamageLaw(1, H, Dinf,pi,alpha)
fct = linearScalarFunction(0,c,-c/Dinf)
cl1     = GeneralVariableIsotropicCLengthLaw(1, fct)


#failure law
pf = 0.0897
damlaw2 = PowerBrittleDamagelaw(2,0,pf,1.,0.3)
regF = twoVariableExponentialSaturationScalarFunction(0.95*pf,0.9999*pf);
damlaw2.setRegulizedFunction(regF)
#damlaw2 = SimpleSaturateDamageLaw(2, 240., 1.,0.,2.)
cl2     = IsotropicCLengthLaw(2, c)

law1   = NonLocalDamagePowerYieldHyperDG3DMaterialLawWithFailure(lawnum,rho,E,nu,hardenc,hardent,hardenk,cl1,cl2,damlaw1,damlaw2)

law1.setYieldPowerFactor(3.5)
law1.setNonAssociatedFlow(True)
law1.nonAssociatedFlowRuleFactor(0.3)
law1.setStrainOrder(7)
law1.setViscoelasticMethod(0)

withRate = False
if withRate:
	N = 4;
	law1.setViscoElasticNumberOfElement(N)
	law1.setViscoElasticData(1,380,7202.)
	law1.setViscoElasticData(2,190,71.6)
	law1.setViscoElasticData(3,95,0.73)
	law1.setViscoElasticData(4,48,0.073)
	eta = constantViscosityLaw(1,3e4)
	law1.setViscosityEffect(eta,0.21)

FS = 3
if FS == 1:
	Xc = ConstantFailureLaw(1,420.)
	Xt = ConstantFailureLaw(2,100.)
 	failureCr = PowerFailureCriterion(1,Xt,Xc,2.)
elif FS== 2:
	Xc = ConstantFailureLaw(1,420.)
	Xt = ConstantFailureLaw(2,100.)
	Xs = ConstantFailureLaw(2,227.26)
 	failureCr = ThreeCriticalStressCriterion(1,Xt,Xc,Xs)
elif FS == 3:
	a = 0.04042*0.848
	b = 7.815
	c = 0.02558*0.848
	failureCr = CritialPlasticDeformationCriteration(1,a,b,c)
else:
	print("Failure surfce is not  correctly set")

law1.setFailureCriterion(failureCr)


lawlinear = 12      # unique number of law
rhoCF   = 1.750e-9
youngCF    = 40.e3 # MPa
youngACF   = 230.e3 # Mpa
nuCF       = 0.2 
nu_minorCF = 0.256*youngCF/youngACF 
GACF       = 24.e3
Ax       = 0.0     # direction of anisotropy
Ay       = 0.0
Az       = 1.0
law2 = TransverseIsotropicDG3DMaterialLaw(lawlinear,rhoCF,youngCF,nuCF,youngACF,GACF,nu_minorCF,Ax,Ay,Az)

nonlocallawlinear = 13
law2Nonlocal = virtualNonLocalDamageDG3DMaterialLaw(nonlocallawlinear,law2,1)
law2Nonlocal.setNonLocalLengthScale(0,cl1)


# geometry
meshfile = "Window20_30_15.msh"

# solver
sol = 2  # Gmm=0 (default) Taucs=1 PETsc=2
soltype = 1 # StaticLinear=0 (default) StaticNonLinear=1
nstep = 1000   # number of step (used only if soltype=1)
ftime =35.   # Final time (used only if soltype=1)
tol=1.e-4   # relative tolerance for NR scheme (used only if soltype=1)
nstepArch=1 # Number of step between 2 archiving (used only if soltype=1)
fullDg = 0 #O = CG, 1 = DG
space1 = 0 # function space (Lagrange=0)
beta1  = 100

  
#matrix
myfield1 = dG3DDomain(10,11,space1,lawnum,fullDg,2,2)
#fiber
myfield2 = dG3DDomain(10,12,space1,nonlocallawlinear,fullDg,2,1)

# creation of Solver
mysolver = nonLinearMechSolver(1000)
mysolver.loadModel(meshfile)
mysolver.addDomain(myfield1)
mysolver.addDomain(myfield2)
mysolver.addMaterialLaw(law1)
mysolver.addMaterialLaw(law2Nonlocal)
mysolver.Scheme(soltype)
mysolver.Solver(sol)
mysolver.snlData(nstep,ftime,tol)
mysolver.stepBetweenArchiving(nstepArch)

mysolver.snlManageTimeStep(15,3,2.,150) # maximal 20 times

mysolver.displacementBC("Face",11,2,0.)
mysolver.displacementBC("Face",12,2,0.)
mysolver.displacementBC("Edge",2,4,0.)
mysolver.displacementBC("Edge",4,4,0.)

BC = "mixedBC"
if BC == "PBC":	
	microBC = nonLinearPeriodicBC(10,2)
	microBC.setOrder(1)
	microBC.setBCPhysical(1,4,3,2) #Periodic boundary condition
	method = 1	# Periodic mesh = 0, Langrange interpolation = 1, Cubic spline interpolation =2,  FE linear= 3, FE Quad = 4
	degree = 7	# Order used for polynomial interpolation 
	addvertex = False # Polynomial interpolation by mesh vertex = 0, Polynomial interpolation by virtual vertex 
	microBC.setPeriodicBCOptions(method, degree,addvertex) 
	mysolver.addMicroBC(microBC)
	mysolver.activateTest(True)
	mysolver.displacementBC("Node",1,0,0.)
	mysolver.displacementBC("Node",1,1,0.)
	mysolver.displacementBC("Node",4,0,0.)
	mysolver.displacementBC("Node",2,1,0.)
	mysolver.forceBC("Node",2,0,1.)

elif BC == "mixedBC":
	mysolver.displacementBC("Edge",4,0,0.)
	mysolver.displacementBC("Edge",1,1,0.)
	mysolver.sameDisplacementBC("Edge",3,4,1)
	mysolver.sameDisplacementBC("Edge",2,2,0)
	mysolver.averagePeriodicBC("Edge",3,1,4,1,0)
	mysolver.averagePeriodicBC("Edge",2,4,2,1,1)
	mysolver.forceBC("Node",2,0,1.)

method = 1
mysolver.pathFollowing(True,method)

if method==0:
	mysolver.setPathFollowingIncrementAdaptation(True,4)
	mysolver.setPathFollowingControlType(0)
	mysolver.setPathFollowingCorrectionMethod(0)
	mysolver.setPathFollowingArcLengthStep(1e-1)
	mysolver.setBoundsOfPathFollowingArcLengthSteps(0,5.);
elif method==1:
	mysolver.setPathFollowingIncrementAdaptationLocal(True,3,4)
	mysolver.setPathFollowingLocalSteps(1e-2,5e-7)
	mysolver.setPathFollowingLocalIncrementType(3);
	mysolver.setPathFollowingSwitchCriterion(5e-4)


#element erosion
bulkEro = True
interfaceEro = False
EroMethod = 1 # 1- erosion when fist IP reach failure Cr, 2-all IPs
mysolver.setElementErosion(bulkEro,interfaceEro,EroMethod)
#cr based on critical damage
Type = 1 # 1- maximal value, 2 -minimal value
cr = GeneralCritialCriterion(1,0.9999,IPField.DAMAGE,Type)

stopCri = EndSchemeMonitoringWithZeroForceOnPhysical(0.02,"Edge",4,0)
mysolver.endSchemeMonitoring(stopCri)

mysolver.internalPointBuildView("svm",IPField.SVM, 1, 1)
mysolver.internalPointBuildView("sig_xx",IPField.SIG_XX, 1, 1)
mysolver.internalPointBuildView("sig_yy",IPField.SIG_YY, 1, 1)
mysolver.internalPointBuildView("sig_zz",IPField.SIG_ZZ, 1, 1)
mysolver.internalPointBuildView("sig_xy",IPField.SIG_XY, 1, 1)
mysolver.internalPointBuildView("sig_yz",IPField.SIG_YZ, 1, 1)
mysolver.internalPointBuildView("sig_xz",IPField.SIG_XZ, 1, 1)
mysolver.internalPointBuildView("epl",IPField.PLASTICSTRAIN, 1, 1)
mysolver.internalPointBuildView("Damage",IPField.DAMAGE, 1, 1)
mysolver.internalPointBuildView("DEFO_ENERGY",IPField.DEFO_ENERGY, 1, 1)

mysolver.internalPointBuildView("DAMAGE0",IPField.DAMAGE_0, 1, 1)
mysolver.internalPointBuildView("DAMAGE1",IPField.DAMAGE_1, 1, 1) 
mysolver.internalPointBuildView("FAILURE_ONSET",IPField.FAILURE_ONSET, 1, 1)
mysolver.internalPointBuildView("FAILURE_PLASTICSTRAIN",IPField.FAILURE_PLASTICSTRAIN, 1, 1)  
mysolver.internalPointBuildView("ACTIVE_DISSIPATION",IPField.ACTIVE_DISSIPATION, 1, 1)  
mysolver.internalPointBuildView("PRESSION",IPField.PRESSION, 1, 1)  

mysolver.internalPointBuildView("LOCAL_0",IPField.LOCAL_0, 1, 1)  
mysolver.internalPointBuildView("LOCAL_1",IPField.LOCAL_1, 1, 1)  

mysolver.internalPointBuildView("NONLOCAL_0",IPField.NONLOCAL_0, 1, 1)  
mysolver.internalPointBuildView("NONLOCAL_1",IPField.NONLOCAL_1, 1, 1)  

mysolver.archivingForceOnPhysicalGroup("Edge", 4, 0)
mysolver.archivingNodeDisplacement(2,0)
mysolver.archivingNodeDisplacement(2,1)

mysolver.archivingAverageValue(IPField.SIG_YY)
mysolver.archivingAverageValue(IPField.SIG_XX)
mysolver.archivingAverageValue(IPField.SIG_XY)

mysolver.archivingAverageValue(IPField.P_XX)
mysolver.archivingAverageValue(IPField.P_YY)
mysolver.archivingAverageValue(IPField.P_XY)
mysolver.archivingAverageValue(IPField.P_YX)

mysolver.archivingAverageValue(IPField.F_XX)
mysolver.archivingAverageValue(IPField.F_YY)
mysolver.archivingAverageValue(IPField.F_XY)
mysolver.archivingAverageValue(IPField.F_YX)

mysolver.archivingVolumeIntegralValue(IPField.DEFO_ENERGY)
mysolver.archivingVolumeIntegralValue(IPField.PLASTIC_ENERGY)
mysolver.archivingVolumeIntegralValue(IPField.DAMAGE_ENERGY)

mysolver.solve()


