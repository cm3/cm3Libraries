import numpy as npy
import pickle

from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
from matplotlib import cm
import matplotlib 
from mpl_toolkits.axes_grid1.inset_locator import zoomed_inset_axes
from mpl_toolkits.axes_grid1.inset_locator import mark_inset

unit=1.e-3;
w=20./1.;

numPart=1

fig, ax = plt.subplots()
#matplotlib.rc('xtick', labelsize=20) 
#matplotlib.rc('ytick', labelsize=20)
plt.xticks(fontsize=20)
plt.yticks(fontsize=20)
strn=[]
strs=[]

strnexp=[] # Delta a [mm], delta [mm], F [daN] 
strnexp.append([[0.0,0.00,0.00],
[3.0,3.45,3.90],
[4.0,4.45,5.06],
[5.0,5.55,5.87],
[8.0,6.80,6.48],
[10.0,7.75,6.39],
[11.0,8.55,6.28],
[12.5,9.55,6.11],
[15.0,10.45,5.96],
[18.0,11.00,5.88],
[20.5,11.60,5.89],
[25.0,12.20,5.70],
[27.0,13.00,5.60],
[29.0,13.90,5.45],
[31.0,14.50,5.39],
[34.0,15.00,5.31],
[36.0,15.60,5.12],
[38.0,16.20,5.05],
[40.0,16.75,4.98],
[41.5,17.75,4.99],
[44.0,18.20,4.80],
[47.0,19.10,4.80],
[49.0,20.00,4.79],
[50.5,20.80,4.72],
[53.0,21.50,4.64],
[56.0,22.25,4.55],
[57.5,28.50,4.41]])


for sim in range(numPart):
    filename='./force102comp2.csv' 
    dispname='./NodalDisplacementPhysical52Num17comp2.csv'
    if(numPart>1):
      filename='./force102comp2_part'+str(sim)+'.csv' 
      dispname='./NodalDisplacementPhysical52Num17comp2_part'+str(sim)+'.csv'

    try:
      with open(filename,'r') as data:
        B_string = data.read()     
    except:
      print('Not found on partition: ',sim)
    else:
      B_string = B_string.split('\n')
      force = []
      for i in range(len(B_string)-1):
        row = B_string[i]
        row = row.split(';')
        force.append(float(row[1]))
      stress = []
      for i in range(len(force)):
        if(force[i]>0.0):
            stress.append( force[i]*w)
      strs.append(stress)

    try:
      with open(dispname,'r') as data:
        A_string = data.read()     
    except:
      print('Not found on partition: ',sim)
    else:
      A_string = A_string.split('\n')
      disp = []
      for i in range(len(A_string)-1):
        row = A_string[i]
        row = row.split(';')
        disp.append(float(row[1]))
      strain = []
      for i in range(len(disp)):
        if(disp[i]>0.0):
            strain.append( disp[i])
      strn.append(strain)

totalStress=[]
#print(strs)
#print(strn)
for j in range(len(strs[0])):
  val=0.
  for i in range(len(strs)):
      val=val+strs[i][j]
  totalStress.append(float(val))

plt.ylim([0.0, 140.0])
plt.xlim([0.0, 0.025])
plt.locator_params(axis='x', nbins=5)
#plt.plot(x,y,'mx', markeredgewidth =3.0,markersize= 13)

#handles, labels = ax.get_legend_handles_labels()
#ax.legend(handles, labels)

plt.legend(fontsize=22, framealpha=0.0)
plt.xlabel('$\delta$ in m', fontsize=22)
plt.ylabel('$F$ in MPa', fontsize=22)

#print(strn[0])
#print(totalStress)

line1, =plt.plot(strn[0],totalStress, linewidth= 1.0, color = 'blue', label="Simulation")
for j in range(len(strnexp)):
  if(j==0):
    line2, =plt.plot([strnexp[j][i][1]/1000. for i in range(len(strnexp[j]))],[strnexp[j][i][2]*10 for i in range(len(strnexp[j]))], linewidth= 0.50, color = 'black', label ="Experimental")
  else:
    line2, =plt.plot([strnexp[j][i][1]/1000. for i in range(len(strnexp[j]))],[strnexp[j][i][2]*10. for i in range(len(strnexp[j]))], linewidth= 0.50, color = 'black')
#plt.annotate('Conf. #1', xy=(0.00227, 583), xytext=(0.0015, 650),
#             arrowprops=dict(arrowstyle='->'),fontsize=22)
#plt.annotate('Conf. #2', xy=(0.0029, 653), xytext=(0.0020, 700),
#             arrowprops=dict(arrowstyle='->'),fontsize=22)

plt.yticks(visible=True)
plt.xticks(visible=True)
#legend([line1,line2], ['Simulation', 'Experimental'])
plt.legend(loc='lower right',prop={'size':22})

plt.savefig('delamForce.pdf',bbox_inches='tight')
plt.show()



