mm = 1e-3;
L = 70*mm;
H = 65*mm;  //crack length
t = 2.1*mm;


lsca = 6.*mm*3.1456/60.; 
W = 1.*mm;
nb=(L/lsca);

Point(1) = {0,0,0,10*lsca};
Point(2) = {W,0,0,10*lsca};
Line(1) = {1, 2};
Extrude {0, L, 0} {
  Line{1}; Layers{nb}; Recombine;
}
Extrude {0, H, 0} {
  Line{2}; Layers{5}; Recombine;
}
Extrude {0, 0, -1.*t} {
  Surface{5, 9}; Layers{1}; Recombine;
}
Extrude {0, 0, t} {
  Surface{5, 9}; Layers{1}; Recombine;
}
Physical Volume(51) = {1};
Physical Volume(52) = {2};
Physical Volume(61) = {3};
Physical Volume(62) = {4};

Physical Line(101) = {35};
Physical Line(102) = {79};
Physical Point(51) = {22};
Physical Point(52) = {42};

Physical Surface(100) = {22,44,66,88};
