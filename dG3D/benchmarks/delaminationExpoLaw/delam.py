#coding-Utf-8-*-
from gmshpy import *
from dG3Dpy import*

#script to launch beam problem with a python script

# material law
lawnum = 2 
rho   = 1000.

lawcnumInt = 3
Gc   = 600.
GcII = 1200.
sigmac   = 25.e6
sigmacII = 25.e6
alphaGc = 1.
fsmin = 1.
fsmax = 1.
Kp = 1e16
DmaxInc=0.925

# geometry
geofile="delam.geo" # name of mesh file
meshfile="delam.msh" # name of mesh file
propertiesC5="old_properties_an90NLSig.i01" #properties file
# solver
sol = 2  # Gmm=0 (default) Taucs=1 PETsc=2
soltype = 1 # StaticLinear=0 (default) StaticNonLinear=1
nstep = 1000   # number of step (used only if soltype=1)
ftime =1 # Final time (used only if soltype=1)
tol=1.e-4   # relative tolerance for NR scheme (used only if soltype=1)
nstepArch=10 # Number of step between 2 archiving (used only if soltype=1)
fullDg =0 #O = CG, 1 = DG
space1 = 0 # function space (Lagrange=0)
beta1  = 100.

useBarF=True
#  compute solution and BC (given directly to the solver
# creation of law
PropMatfile2 = 'MF_MATprop.csv'
lx=10.0
ly= 10.0
law5 = NonLocalDamageDG3DMaterialLaw_Stoch(lawnum,rho,propertiesC5,0.0,0.0, 0.0,lx, ly, 0.0, PropMatfile2, 3) 
law5.setUseBarF(useBarF)
law5.setInitialDmax_Inc(DmaxInc)
law5.setInitialDmax_Mtx(0.925)


elasPotential = linearCohesiveElasticPotential(1)
damageLaw = ExponentialCohesiveDamageLaw(1,1.5)

lawInter  = DelaminationLinearCohesive3DLaw(lawcnumInt,elasPotential,damageLaw,Gc,GcII,sigmac,sigmacII,alphaGc,fsmin,fsmax,Kp)
lawInter.setViscosity(1.e7)

# creation of ElasticField

myfieldEP1 = dG3DDomain(10,51,space1,lawnum,fullDg,3,2)
myfieldEP1.stabilityParameters(beta1)
myfieldEP1.usePrimaryShapeFunction(3)
myfieldEP1.usePrimaryShapeFunction(4)
myfieldEP1.setNonLocalStabilityParameters(beta1,True) #stabilty parameter on non local epl
myfieldEP1.setNonLocalEqRatio(1.e8)

myfieldEP2 = dG3DDomain(10,52,space1,lawnum,fullDg,3,2)
myfieldEP2.stabilityParameters(beta1)
myfieldEP2.usePrimaryShapeFunction(3)
myfieldEP2.usePrimaryShapeFunction(4)
myfieldEP2.setNonLocalStabilityParameters(beta1,True) #stabilty parameter on non local epl
myfieldEP2.setNonLocalEqRatio(1.e8)

myfieldEP3 = dG3DDomain(11,61,space1,lawnum,fullDg,3,2)
myfieldEP3.stabilityParameters(beta1)
myfieldEP3.usePrimaryShapeFunction(3)
myfieldEP3.usePrimaryShapeFunction(4)
myfieldEP3.setNonLocalStabilityParameters(beta1,True) #stabilty parameter on non local epl
myfieldEP3.setNonLocalEqRatio(1.e8)

myfieldEP4 = dG3DDomain(11,62,space1,lawnum,fullDg,3,2)
myfieldEP4.stabilityParameters(beta1)
myfieldEP4.usePrimaryShapeFunction(3)
myfieldEP4.usePrimaryShapeFunction(4)
myfieldEP4.setNonLocalStabilityParameters(beta1,True) #stabilty parameter on non local epl
myfieldEP4.setNonLocalEqRatio(1.e8)

myinterfield = interDomainBetween3D(12,myfieldEP1,myfieldEP3,lawcnumInt)
myinterfield.stabilityParameters(beta1)
myinterfield.setNonLocalStabilityParameters(beta1,False) #stabilty parameter on non local epl
myinterfield.setNonLocalEqRatio(1.e8)
myinterfield.forceCohesiveInsertionAllIPs(False,0.5)
myinterfield.gaussIntegration(1,-1,-1)

# creation of Solver
mysolver = nonLinearMechSolver(1000)
mysolver.createModel(geofile,meshfile,3,1)
mysolver.addDomain(myfieldEP1)
mysolver.addDomain(myfieldEP2)
mysolver.addDomain(myfieldEP3)
mysolver.addDomain(myfieldEP4)
mysolver.addMaterialLaw(law5)

mysolver.addDomain(myinterfield)
mysolver.addMaterialLaw(lawInter)


mysolver.Scheme(soltype)
mysolver.Solver(sol)
mysolver.snlData(nstep,ftime,tol)
mysolver.stepBetweenArchiving(nstepArch)


selUpdate=cohesiveCrackCriticalIPUpdate()
mysolver.setSelectiveUpdate(selUpdate)

mysolver.displacementBC("Edge",101,0,0.)
mysolver.displacementBC("Edge",101,1,0.)
mysolver.displacementBC("Edge",101,2,0.)

mysolver.displacementBC("Edge",102,0,0.)
mysolver.displacementBC("Edge",102,1,0.)
mysolver.displacementBC("Edge",102,2,12.5e-3)

mysolver.displacementBC("Face",100,0,0)



mysolver.internalPointBuildView("svm",IPField.SVM, 1, 1)
mysolver.internalPointBuildView("sig_xx",IPField.SIG_XX, 1, 1)
mysolver.internalPointBuildView("sig_yy",IPField.SIG_YY, 1, 1)
mysolver.internalPointBuildView("sig_zz",IPField.SIG_ZZ, 1, 1)
mysolver.internalPointBuildView("sig_xy",IPField.SIG_XY, 1, 1)
mysolver.internalPointBuildView("sig_yz",IPField.SIG_YZ, 1, 1)
mysolver.internalPointBuildView("sig_xz",IPField.SIG_XZ, 1, 1)
mysolver.internalPointBuildView("epl",IPField.PLASTICSTRAIN, 1, 1)
mysolver.internalPointBuildView("damage",IPField.DAMAGE, 1, nstepArch)
mysolver.internalPointBuildView("FiberDamage",IPField.INC_DAMAGE, 1, nstepArch)


mysolver.archivingForceOnPhysicalGroup("Edge", 101, 2, 1)
mysolver.archivingForceOnPhysicalGroup("Edge", 102, 2, 1)

mysolver.archivingNodeDisplacement(51,2,1)
mysolver.archivingNodeDisplacement(52,2,1)

mysolver.solve()

check = TestCheck()
check.equal(-2.288230e+00,mysolver.getArchivedForceOnPhysicalGroup("Edge", 101, 2),2.e-4)


