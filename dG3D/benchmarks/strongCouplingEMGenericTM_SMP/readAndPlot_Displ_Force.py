import pandas as pd
import os
import numpy as np
import matplotlib.pyplot as plt

Forcefilename = "force11111comp1.csv"
Displacementfilename = "NodalDisplacementPhysical11112Num1comp1.csv"

newdataframeForce = pd.read_csv(Forcefilename,sep=";",header=None).values
print(newdataframeForce.shape)
print()

newdataframeDispl = pd.read_csv(Displacementfilename,sep=";",header=None).values
print(newdataframeDispl.shape)
print()

# convert array into dataframe
DF = pd.DataFrame({'displ':newdataframeDispl[:,1],'force':newdataframeForce[:,1]})

# save the dataframe as a csv file
DF.to_csv("./DisplvsForce.csv",sep=";",header=False,index=False)

plt.figure()
plt.title('Nodal displacement vs Nodal force (Y component) on top face in SMP', size=12)
plt.xlabel('Nodal displacement [m]', size=14)
plt.ylabel('Nodal force [N]', size=14)
plt.plot(newdataframeDispl[:,1],newdataframeForce[:,1])
plt.savefig('./DisplvsForce.png', format='png')
plt.show()
    
