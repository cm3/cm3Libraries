#coding-Utf-8-*-
from gmshpy import *


#from dG3DpyDebug import*
from dG3Dpy import*

#script to launch ElecMag SMP problem with a python script

import math

# material law: SMP
lawnum= 1 
rho   = 270. # material density (Kg/m^3)
tinitial = 273.+47. # (K)
G=299.e6 # Shear modulus (Pa)
Mu=0.29 # Poisson ratio
alpha = beta=gamma=0. # parameter of anisotropy
cp=10.*rho # specific heat capacity 
Kx=Ky=Kz=237. # Thermal conductivity (W/m K)
lx=ly=lz=1.0e4 # electrical conductivity (S/m) 
seebeck=0.0 #21.e-6
alphaTherm=0.0 #13.e-5 # thermal dilatation
v0=0. # initial electric potential
E_matrix = 771.e6 # (Pa)
epsilon_0 = 8.854 * 1.e-12 # vacuum electric permittivity
mag_mu_0 = 4.e-7 * math.pi # vacuum magnetic permeability
epsilon_r_smp = 1.0 # relative elec permittivity of SMP
mag_r_smp = 5.0 # relative mag permeability of SMP
# use of electric permittivity constants to formulate polarization
epsilon_4 = 0.0
epsilon_6 = 0.0
epsilon_5 = 0.0 #(epsilon_0 * (1.0-epsilon_r_smp)/rho)
# use of magnetic permeability constants to formulate magnetization
mu_7 = 0.0 # magnetic permeability constants of SMP
if(mag_r_smp == 1.0):
  mu_8 = 0.0
else:
  mu_8 = (rho * mag_mu_0)/((1.0/mag_r_smp)-1.0) # magnetic permeability constants of SMP
#print("mu_8 = ",mu_8)
mu_9 = 0.0 # magnetic permeability constants of SMP
mag_mu_x = mag_mu_y = mag_mu_z = mag_r_smp * mag_mu_0 # mag permeability smp 
magpot0_x = magpot0_y = magpot0_z = 0.0 # initial magnetic potential
Irms = 325. # (Ampere)
freq = 1000. # (Hz)
nTurnsCoil = 1000
coilLx = coilLy = coilLz = 1. # m
coilW = 3.e-2 # m

# material law: inductor (copper coil)
# Material parameters for copper
lawnumind = 4
rhoind = 8960. 
Eind=120.e9 #youngs modulus
Gind=48.e9 # Shear modulus
Muind = 0.34 # Poisson ratio
alphaind = betaind = gammaind = 0. # parameter of anisotropy
cpind= 385.*rhoind
Kxind=Kyind=Kzind= 401. # thermal conductivity tensor components
lxind=lyind=lzind=5.77e7 # electrical conductivity (S/m)
seebeckind=0.0 #6.5e-6
alphaThermind= 0.0 # thermal dilatation
v0ind = 0.
mag_r_ind = 1.0 # relative mag permeability of inductor
mag_mu_x_ind = mag_mu_y_ind = mag_mu_z_ind = mag_r_ind * mag_mu_0 # mag permeability inductor 
magpot0_x_ind = magpot0_y_ind = magpot0_z_ind = 0.0 # initial magnetic potential
Centroid_x = Centroid_y = Centroid_z = 0.0 # Centroid of inductor coil
centralAxis_x = 0.
centralAxis_y = 1.
centralAxis_z = 0. # Unit vector along central axis of inductor coil

# material law: vacuum/free space region
lawnumvac = 7
rhovac = 1.2 
Gvac=156.e1 # Shear modulus
Muvac = 0.35 # Poisson ratio
Evac= 2.0 * Gvac * (1.0 + Muvac) #youngs modulus
alphavac = betavac = gammavac = 0. # parameter of anisotropy
cpvac= 1.e-12 #1012.*rhovac
Kxvac=Kyvac=Kzvac= 0.0 #26.e-3 # thermal conductivity tensor components
lxvac=lyvac=lzvac= 1.e-12 # electrical conductivity
seebeckvac= 0.
alphaThermvac= 0.0 # thermal dilatation
v0vac = 0.
mag_r_vac = 1.0 # relative mag permeability of inductor
mag_mu_x_vac = mag_mu_y_vac = mag_mu_z_vac = mag_r_vac * mag_mu_0 # mag permeability inductor 
magpot0_x_vac = magpot0_y_vac = magpot0_z_vac = 0.0 # initial magnetic potential

useFluxT=True
evaluateCurlField = True;
evaluateTemperature = True;

# geometry
meshfile="undeformedSMPFullDomain.msh" # name of mesh file

# solver
sol = 2  # Gmm=0 (default) Taucs=1 PETsc=2
soltype = 1 # StaticLinear=0 (default) StaticNonLinear=1
#EMncycles = 5; # num of sinusoidal cycles of EM frequency application
nstep = 400 # 8*EMncycles # number of step (used only if soltype=1)
ftime = 5.e-3 # EMncycles/freq; # according to characteristic time
finalDisp = 0.004
#for battery, full solution cannot be reached with mumps-> one 2 EM cycles
nstep = 40
ftime = 5.e-3
finalDisp = 0.004/4.
#EMftime = EMncycles/freq # Final time (used only if soltype=1)
tol=1.e-9   # relative tolerance for NR scheme (used only if soltype=1)
nstepArch=2 # Number of step between 2 archiving (used only if soltype=1)
fullDg = 0 #O = CG, 1 = DG
beta1 = 1000.
eqRatio =1.e6

#  compute solution and BC (given directly to the solver
# creation of law

# material law for smp
# Mechanical law
sy0=1e8
h=1.e9
harden = LinearExponentialJ2IsotropicHardening(1, sy0, h, 0., 10.)
lawMech   = J2LinearDG3DMaterialLaw(lawnum,rho,E_matrix,Mu,harden)
lawMech.setStrainOrder(-1)

# Generic Thermo-Mech law

lawTM   = GenericThermoMechanicsDG3DMaterialLaw(lawnum+1,rho,alpha,beta,gamma,tinitial,Kx,Ky,Kz,alphaTherm,alphaTherm,alphaTherm,cp)
lawTM.setMechanicalMaterialLaw(lawMech);
lawTM.setApplyReferenceF(False);

_cp = constantScalarFunction(cp);
lawTM.setLawForCp(_cp);

# ElecMag Generic Thermo-Mech law for smp
lawsmp = ElecMagGenericThermoMechanicsDG3DMaterialLaw(lawnum+2,rho,alpha,beta,gamma,lx,ly,lz,seebeck,v0,mag_mu_x,
mag_mu_y,mag_mu_z,epsilon_4,epsilon_5,epsilon_6,mu_7,mu_8,mu_9,magpot0_x,magpot0_y, magpot0_z, Irms, freq, nTurnsCoil, coilLx, coilLy, coilLz, coilW,useFluxT,evaluateCurlField,evaluateTemperature)

lawsmp.setThermoMechanicalMaterialLaw(lawTM);
lawsmp.setApplyReferenceF(False);

pertLawsmp = dG3DMaterialLawWithTangentByPerturbation(lawsmp,1.e-8)

#lawsmp.setUseBarF(True)

# material law for inductor region
#sy0=1e10
#h=1.e9
hardenind = LinearExponentialJ2IsotropicHardening(lawnumind, sy0, h, 0., 10.)
lawMechind   = J2LinearDG3DMaterialLaw(lawnumind,rhoind,Eind,Muind,hardenind)
lawMechind.setStrainOrder(-1)

lawTMind = GenericThermoMechanicsDG3DMaterialLaw(lawnumind+1,rhoind,alphaind,betaind,gammaind,tinitial,
Kxind,Kyind,Kzind,alphaThermind,alphaThermind,alphaThermind,cpind)
lawTMind.setMechanicalMaterialLaw(lawMechind);
lawTMind.setApplyReferenceF(False);

_cpind = constantScalarFunction(cpind);
lawTMind.setLawForCp(_cpind);

lawind = ElecMagInductorDG3DMaterialLaw(lawnumind+2,rhoind,alphaind,betaind,gammaind,lxind,lyind,lzind,seebeckind,
v0ind, mag_mu_x_ind, mag_mu_y_ind, mag_mu_z_ind, magpot0_x_ind,magpot0_y_ind, magpot0_z_ind, Irms, freq, nTurnsCoil, 
coilLx, coilLy, coilLz,coilW,Centroid_x,Centroid_y,Centroid_z,centralAxis_x,centralAxis_y,centralAxis_z,
useFluxT,evaluateCurlField)

lawind.setThermoMechanicalMaterialLaw(lawTMind);
lawind.setApplyReferenceF(False);

#lawind.setUseBarF(True)

#material law for free space (vacuum)
#sy0=1e10
#h=1.e9
hardenvac = LinearExponentialJ2IsotropicHardening(lawnumvac, sy0, h, 0., 10.)
lawMechvac   = J2LinearDG3DMaterialLaw(lawnumvac,rhovac,Evac,Muvac,hardenvac)
lawMechvac.setStrainOrder(-1)

lawTMvac = GenericThermoMechanicsDG3DMaterialLaw(lawnumvac+1,rhovac,alphavac,betavac,gammavac,tinitial,
Kxvac,Kyvac,Kzvac,alphaThermvac,alphaThermvac,alphaThermvac,cpvac)
lawTMvac.setMechanicalMaterialLaw(lawMechvac);
lawTMvac.setApplyReferenceF(False);

_cpvac = constantScalarFunction(cpvac);
lawTMvac.setLawForCp(_cpvac);

lawvac = ElecMagGenericThermoMechanicsDG3DMaterialLaw(lawnumvac+2,rhovac,alphavac,betavac,gammavac,lxvac,lyvac,lzvac,
seebeckvac,v0vac, mag_mu_x_vac, mag_mu_y_vac, mag_mu_z_vac,0.0,0.0,0.0,0.0,0.0,0.0, magpot0_x_vac,
magpot0_y_vac, magpot0_z_vac, Irms, freq, nTurnsCoil, coilLx, coilLy, coilLz, coilW,useFluxT,evaluateCurlField,evaluateTemperature)

lawvac.setThermoMechanicalMaterialLaw(lawTMvac);
lawvac.setApplyReferenceF(False);

pertLawvac = dG3DMaterialLawWithTangentByPerturbation(lawvac,1.e-8)

#lawvac.setUseBarF(True)

# creation of ElasticField
SMPfield = 1000 # number of the field (physical number of SMP)
Indfield = 2000 # number of the field (physical number of Inductor region)
Vacfield = 3000 # number of the field (physical number of Vacuum)
SurfVacOut = 3333 # number of the field (outer surface of Vacuum) Used for BC
SurfInd = 2222 # outer surface of inductor region
SurfSmp = 11110 # outer surface of conductor SMP
SmpRef=11111 # top face of outer surface of SMP
SmpRefBottom=11115 # bottom face of outer surface of SMP
SmpRefPoint=11112 # single point on top face of SMP

space1 = 0 # function space (Lagrange=0)

SMP_field = ElecMagTherMechDG3DDomain(10,SMPfield,space1,lawnum+2,fullDg,eqRatio,3)
Inductor_field = ElecMagTherMechDG3DDomain(10,Indfield,space1,lawnumind+2,fullDg,eqRatio,3)
Vacuum_field = ElecMagTherMechDG3DDomain(10,Vacfield,space1,lawnumvac+2,fullDg,eqRatio,3)

SMP_field.setConstitutiveExtraDofDiffusionEqRatio(eqRatio)
Inductor_field.setConstitutiveExtraDofDiffusionEqRatio(eqRatio)
Vacuum_field.setConstitutiveExtraDofDiffusionEqRatio(eqRatio)
SMP_field.setConstitutiveCurlEqRatio(eqRatio)
Inductor_field.setConstitutiveCurlEqRatio(eqRatio)
Vacuum_field.setConstitutiveCurlEqRatio(eqRatio)
SMP_field.stabilityParameters(beta1)
Inductor_field.stabilityParameters(beta1)
Vacuum_field.stabilityParameters(beta1)

thermalSource=True
mecaSource=False

SMP_field.setConstitutiveExtraDofDiffusionAccountSource(thermalSource,mecaSource)
Vacuum_field.setConstitutiveExtraDofDiffusionAccountSource(thermalSource,mecaSource)
Inductor_field.setConstitutiveExtraDofDiffusionAccountSource(thermalSource,mecaSource)

#SMP_field.matrixByPerturbation(1,1,1,1.e-3)
#Inductor_field.matrixByPerturbation(1,1,1,1.e-3)
#Vacuum_field.matrixByPerturbation(1,1,1,1.e-3)

# creation of Solver
mysolver = nonLinearMechSolver(1000)
mysolver.loadModel(meshfile)
mysolver.addDomain(SMP_field)
mysolver.addDomain(Inductor_field)
mysolver.addDomain(Vacuum_field)
mysolver.addMaterialLaw(lawsmp)
mysolver.addMaterialLaw(lawind)
mysolver.addMaterialLaw(lawvac)
mysolver.Scheme(soltype)
mysolver.Solver(sol)
mysolver.snlData(nstep,ftime,tol,1.e-9)#forsmp   
#we cannot use mumps to keep accuracy
mysolver.options("-ksp_type preonly -pc_type lu -pc_factor_mat_solver_type petsc")
mysolver.stepBetweenArchiving(nstepArch)
mysolver.snlManageTimeStep(45, 10, 2, 10)

# BC

cyclicFunctionDisp=cycleFunctionTime(0., 0., ftime , finalDisp);
mysolver.displacementBC("Face",SmpRefBottom,0,0.)
mysolver.displacementBC("Face",SmpRefBottom,1,0.)
mysolver.displacementBC("Face",SmpRefBottom,2,0.)
mysolver.displacementBC("Face",SmpRef,0,0.)
mysolver.displacementBC("Face",SmpRef,1,cyclicFunctionDisp)
mysolver.displacementBC("Face",SmpRef,2,0.)
mysolver.displacementBC("Volume",Indfield,0,0.0)
mysolver.displacementBC("Volume",Indfield,1,0.0)
mysolver.displacementBC("Volume",Indfield,2,0.0)
mysolver.displacementBC("Face",SurfVacOut,0,0.0)
mysolver.displacementBC("Face",SurfVacOut,1,0.0)
mysolver.displacementBC("Face",SurfVacOut,2,0.0)
"""
mysolver.displacementBC("Volume",SMPfield,0,0.0)
mysolver.displacementBC("Volume",SMPfield,1,0.0)
mysolver.displacementBC("Volume",SMPfield,2,0.0)
mysolver.displacementBC("Volume",Indfield,0,0.0)
mysolver.displacementBC("Volume",Indfield,1,0.0)
mysolver.displacementBC("Volume",Indfield,2,0.0)
mysolver.displacementBC("Volume",Vacfield,0,0.0)
mysolver.displacementBC("Volume",Vacfield,1,0.0)
mysolver.displacementBC("Volume",Vacfield,2,0.0)
"""
#thermal BC
cyclicFunctionTemp1=cycleFunctionTime(0., tinitial,ftime,tinitial);
mysolver.initialBC("Volume","Position",SMPfield,3,tinitial)
mysolver.initialBC("Volume","Position",Indfield,3,tinitial)
mysolver.initialBC("Volume","Position",Vacfield,3,tinitial)
mysolver.displacementBC("Volume",Indfield,3,cyclicFunctionTemp1)
mysolver.displacementBC("Face",SurfVacOut,3,cyclicFunctionTemp1)

#electrical BC
mysolver.displacementBC("Volume",Indfield,4,0.0)
mysolver.initialBC("Volume","Position",SMPfield,4,0.0)
mysolver.initialBC("Volume","Position",Indfield,4,0.0)
mysolver.initialBC("Volume","Position",Vacfield,4,0.0)
mysolver.displacementBC("Face",SurfVacOut,4,0.0)
mysolver.displacementBC("Node",SmpRefPoint,4,0.0)

#magentic
mysolver.curlDisplacementBC("Face",SurfVacOut,5,0.0) # comp may also be 5
#Gauging the edges using tree-cotree method
PhysicalCurves = "" 		# input required as a string of comma separated ints
PhysicalSurfaces = "11110,3333" # input required as a string of comma separated ints
PhysicalVolumes = "1000,2000,3000" 	# input required as a string of comma separated ints
OutputPhysical = 55 		# input required as a int
mysolver.createTreeForBC(PhysicalCurves,PhysicalSurfaces,PhysicalVolumes,OutputPhysical)
mysolver.curlDisplacementBC("Edge",OutputPhysical,5,0.0)

mysolver.internalPointBuildView("svm",IPField.SVM, 1, 1)
mysolver.internalPointBuildView("sig_xx",IPField.SIG_XX, 1, 1)
mysolver.internalPointBuildView("sig_yy",IPField.SIG_YY, 1, 1)
mysolver.internalPointBuildView("sig_zz",IPField.SIG_ZZ, 1, 1)
mysolver.internalPointBuildView("sig_xy",IPField.SIG_XY, 1, 1)
mysolver.internalPointBuildView("sig_yz",IPField.SIG_YZ, 1, 1)
mysolver.internalPointBuildView("sig_xz",IPField.SIG_XZ, 1, 1)
mysolver.internalPointBuildView("eps_xx",IPField.STRAIN_XX, 1, 1)
mysolver.internalPointBuildView("eps_yy",IPField.STRAIN_YY, 1, 1)
mysolver.internalPointBuildView("eps_zz",IPField.STRAIN_ZZ, 1, 1)
mysolver.internalPointBuildView("eps_xy",IPField.STRAIN_XY, 1, 1)
mysolver.internalPointBuildView("eps_yz",IPField.STRAIN_YZ, 1, 1)
mysolver.internalPointBuildView("eps_xz",IPField.STRAIN_XZ, 1, 1)
mysolver.internalPointBuildView("temperature",IPField.TEMPERATURE, 1, 1)
mysolver.internalPointBuildView("w_AV",IPField.EMFIELDSOURCE, 1, 1)
mysolver.internalPointBuildView("w_T",IPField.THERMALSOURCE, 1, 1)
mysolver.internalPointBuildView("jyx",IPField.THERMALFLUX_X, 1, 1)
mysolver.internalPointBuildView("jyy",IPField.THERMALFLUX_Y, 1, 1)
mysolver.internalPointBuildView("jyz",IPField.THERMALFLUX_Z, 1, 1)
mysolver.internalPointBuildView("voltage",IPField.VOLTAGE, 1, 1)
mysolver.internalPointBuildView("jex",IPField.ELECTRICALFLUX_X, 1, 1)
mysolver.internalPointBuildView("jey",IPField.ELECTRICALFLUX_Y, 1, 1)
mysolver.internalPointBuildView("jez",IPField.ELECTRICALFLUX_Z, 1, 1)
mysolver.internalPointBuildView("ax",IPField.MAGNETICVECTORPOTENTIAL_X, 1, 1)
mysolver.internalPointBuildView("ay",IPField.MAGNETICVECTORPOTENTIAL_Y, 1, 1)
mysolver.internalPointBuildView("az",IPField.MAGNETICVECTORPOTENTIAL_Z, 1, 1)
mysolver.internalPointBuildView("bx",IPField.MAGNETICINDUCTION_X, 1, 1)
mysolver.internalPointBuildView("by",IPField.MAGNETICINDUCTION_Y, 1, 1)
mysolver.internalPointBuildView("bz",IPField.MAGNETICINDUCTION_Z, 1, 1)
mysolver.internalPointBuildView("hx",IPField.MAGNETICFIELD_X, 1, 1)
mysolver.internalPointBuildView("hy",IPField.MAGNETICFIELD_Y, 1, 1)
mysolver.internalPointBuildView("hz",IPField.MAGNETICFIELD_Z, 1, 1)
mysolver.internalPointBuildView("js0_x",IPField.INDUCTORSOURCEVECTORFIELD_X, 1, 1)
mysolver.internalPointBuildView("js0_y",IPField.INDUCTORSOURCEVECTORFIELD_Y, 1, 1)
mysolver.internalPointBuildView("js0_z",IPField.INDUCTORSOURCEVECTORFIELD_Z, 1, 1)
mysolver.internalPointBuildView("F_xx",IPField.F_XX, 1, 1)
mysolver.internalPointBuildView("F_xy",IPField.F_XY, 1, 1)
mysolver.internalPointBuildView("F_xz",IPField.F_XZ, 1, 1)
mysolver.internalPointBuildView("F_yx",IPField.F_YX, 1, 1)
mysolver.internalPointBuildView("F_yy",IPField.F_YY, 1, 1)
mysolver.internalPointBuildView("F_yz",IPField.F_YZ, 1, 1)
mysolver.internalPointBuildView("F_zx",IPField.F_ZX, 1, 1)
mysolver.internalPointBuildView("F_zy",IPField.F_ZY, 1, 1)
mysolver.internalPointBuildView("F_zz",IPField.F_ZZ, 1, 1)
mysolver.internalPointBuildView("epl",IPField.PLASTICSTRAIN, 1, 1)
mysolver.internalPointBuildView("jacobian",IPField.JACOBIAN, 1, 1)


mysolver.archivingIPOnPhysicalGroup("Volume",SMPfield, IPField.TEMPERATURE,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",SMPfield, IPField.VOLTAGE,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",SMPfield, IPField.EMFIELDSOURCE,IPField.MEAN_VALUE,1);

mysolver.archivingForceOnPhysicalGroup("Face", SurfSmp, 3, 1);
mysolver.archivingForceOnPhysicalGroup("Face", SurfSmp, 4, 1);
mysolver.archivingForceOnPhysicalGroup("Face", SurfSmp, 5, 1);
mysolver.archivingForceOnPhysicalGroup("Face", SurfInd, 4, 1);
mysolver.archivingForceOnPhysicalGroup("Face", SurfInd, 5, 1);
mysolver.archivingForceOnPhysicalGroup("Face", SurfVacOut, 4, 1);
mysolver.archivingForceOnPhysicalGroup("Face", SurfVacOut, 5, 1);

mysolver.archivingForceOnPhysicalGroup("Face", SmpRef, 0, 1);
mysolver.archivingForceOnPhysicalGroup("Face", SmpRef, 1, 1);
mysolver.archivingForceOnPhysicalGroup("Face", SmpRef, 2, 1);

mysolver.archivingNodeDisplacement(SmpRefPoint,0,1);
mysolver.archivingNodeDisplacement(SmpRefPoint,1,1);
mysolver.archivingNodeDisplacement(SmpRefPoint,2,1);
mysolver.archivingNodeDisplacement(SmpRefPoint,3,1);
mysolver.archivingNodeDisplacement(SmpRefPoint,4,1);

#mysolver.archivingNodeIP(SmpRef, IPField.THERMALFLUX_X,IPField.MEAN_VALUE,1);
#mysolver.archivingNodeIP(SmpRef, IPField.ELECTRICALFLUX_X,IPField.MEAN_VALUE,1);

#mysolver.setWriteDeformedMeshToFile(True);
mysolver.createRestartBySteps(25);
mysolver.solve()


check = TestCheck()
check.equal(1.550164e-01,mysolver.getArchivedForceOnPhysicalGroup("Face", SmpRef, 2),1.e-5)
check.equal(327.5119158512149,mysolver.getArchivedNodalValue(SmpRefPoint,3,mysolver.displacement),1.e-5)


