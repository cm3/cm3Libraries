#!/bin/sh

# script example for dragon2
#SBATCH --job-name StrongCoupledEMGenTM_SMP_20ms_20perDefo
#SBATCH --mail-user=vinayak.gholap@doct.uliege.be
#SBATCH --mail-type=ALL
#SBATCH --output="out.txt"
#SBATCH --ntasks=1
#SBATCH --mem-per-cpu=150000
#SBATCH --time=19-23:59:00
#SBATCH --partition=long

module load OpenMPI/4.1.1-GCC-11.2.0

SUBDIR=$HOME/cm3Libraries/dG3D/benchmarks/strongCouplingEMGenericTM_SMP
echo "subdir=" $SUBDIR

SCRATCH=$GLOBALSCRATCH/${USER}_$SLURM_JOB_ID
echo "workdir=" $SCRATCH


pyfile=StrongCouplingEMGenericTM.py
mshfile=undeformedSMPFullDomain.msh
echo "node list of job"  >> $SUBDIR/output.po$SLURM_JOB_ID

hostname >> $SUBDIR/output.po$SLURM_JOB_ID

cd $GLOBALSCRATCH
mkdir -p ${USER}_$SLURM_JOB_ID || exit $?
cp $SUBDIR/$pyfile $SCRATCH/ || exit $?
cp $SUBDIR/$mshfile $SCRATCH/ || exit $? 

cd $SCRATCH # needed to work in the tmpscrach dir otherwise work on home !!
#ls -artl

export PETSC_DIR=$HOME/local/petsc-3.19.5
export PETSC_ARCH=arch-linux-cxx-opt

export PATH=$HOME/local/bin:$PATH

export buildmode=release
export PATH=$PATH:$HOME/cm3Libraries/dG3D/$buildmode/NonLinearSolver/gmsh


export PYTHONPATH=$PYTHONPATH:$HOME/cm3Libraries/dG3D/$buildmode:$HOME/cm3Libraries/dG3D/$buildmode/NonLinearSolver/gmsh/utils/wrappers:$HOME/cm3Libraries/dG3D/$buildmode/NonLinearSolver/gmsh/utils/wrappers/gmshpy


mpirun python3  $SCRATCH/$pyfile >& $SCRATCH/output.txt

echo -e "\n"

sleep 5

mkdir $SUBDIR/$SLURM_JOB_ID

srun cp -f $SCRATCH/* $SUBDIR/$SLURM_JOB_ID/  || exit $? 
srun rm -rf $SCRATCH ||  exit $? 
echo -e "\n"

