#coding-Utf-8-*-

from gmshpy import *
from dG3Dpy import*

#script to launch PBC problem with a python script

# material law
E = 3.2E3
nu = 0.3
K = E/3./(1.-2.*nu)	# Bulk mudulus
mu =E/2./(1.+nu)	  # Shear mudulus
rho = 2.7e-9 # Bulk mass

# creation of material law

law1 = dG3DLinearElasticMaterialLaw(11,rho,E,nu)
law1.setUseBarF(True)

# geometry
geofile="rve.geo"
meshfile="rve.msh" # name of mesh file

# creation of part Domain
myfield1 = dG3DDomain(10,11,0,11,0,3)


# solver
sol = 2  # Gmm=0 (default) Taucs=1 PETsc=2
soltype =1 # StaticLinear=0 (default) StaticNonLinear=1
nstep = 2  # number of step (used only if soltype=1)
ftime =1.   # Final time (used only if soltype=1)
tol=1.e-6  # relative tolerance for NR scheme (used only if soltype=1)
nstepArch=1 # Number of step between 2 archiving (used only if soltype=1)


# creation of Solver
mysolver = nonLinearMechSolver(1000)
mysolver.createModel(geofile,meshfile,3,2)
mysolver.addDomain(myfield1)
mysolver.addMaterialLaw(law1)
mysolver.Scheme(soltype)
mysolver.Solver(sol)
mysolver.snlData(nstep,ftime,tol)

microBC = nonLinearPeriodicBC(1000,3)
microBC.setOrder(1)
microBC.setBCPhysical(1,3,5,2,4,6) #Periodic boundary condition
method =0	# Periodic mesh = 0, Langrange interpolation = 1, Cubic spline interpolation =2,  FE linear= 3, FE Quad = 4
degree = 5	# Order used for polynomial interpolation 
addvertex = 0 # Polynomial interpolation by mesh vertex = 0, Polynomial interpolation by virtual vertex 
microBC.setPeriodicBCOptions(method, degree,bool(addvertex)) 
mysolver.addMicroBC(microBC)

# activate test flag after inserting microBC

mysolver.activateTest(True)
mysolver.setRVEVolume(1.)

# root Vertex always fix
mysolver.displacementBC("Node",4,0,0.)
mysolver.displacementBC("Node",4,1,0.)
mysolver.displacementBC("Node",4,2,0.)

#to generate pur shear deformed shape
mysolver.displacementBC("Node",3,0,0.)
mysolver.displacementBC("Node",3,1,0.1)
mysolver.displacementBC("Node",3,2,0)

mysolver.displacementBC("Node",1,0,0.)
mysolver.displacementBC("Node",1,2,0.)

mysolver.archivingAverageValue(IPField.P_XY)

# build view
mysolver.internalPointBuildView("Green-Lagrange_xx",IPField.STRAIN_XX, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_yy",IPField.STRAIN_YY, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_zz",IPField.STRAIN_ZZ, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_xy",IPField.STRAIN_XY, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_yz",IPField.STRAIN_YZ, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_xz",IPField.STRAIN_XZ, 1, 1);
mysolver.internalPointBuildView("sig_xx",IPField.SIG_XX, 1, 1);
mysolver.internalPointBuildView("sig_yy",IPField.SIG_YY, 1, 1);
mysolver.internalPointBuildView("sig_zz",IPField.SIG_ZZ, 1, 1);
mysolver.internalPointBuildView("sig_xy",IPField.SIG_XY, 1, 1);
mysolver.internalPointBuildView("sig_yz",IPField.SIG_YZ, 1, 1);
mysolver.internalPointBuildView("sig_xz",IPField.SIG_XZ, 1, 1);
mysolver.internalPointBuildView("sig_VM",IPField.SVM, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange equivalent strain",IPField.GL_EQUIVALENT_STRAIN, 1, 1);
mysolver.internalPointBuildView("Equivalent plastic strain",IPField.PLASTICSTRAIN, 1, 1);

# solve
mysolver.solve()

# test check
check = TestCheck()
import linecache
homoStress = linecache.getline('Average_P_XY.csv',2)
val = float(homoStress.split(';')[1])
check.equal(1.015205e+02,val,1.e-4)
