#coding-Utf-8-*-

from gmshpy import *
from dG3Dpy import*

from math import*

#script to launch PBC problem with a python script

# material law
lawnum = 11 # unique number of law

E = 1410. #MPa 
nu = 0.4
K = E/3./(1.-2.*nu)	# Bulk mudulus
mu =E/2./(1.+nu)	  # Shear mudulus
rho = 7850e-9 # Bulk mass

law1   = HyperViscoElasticDG3DMaterialLaw(lawnum,rho,E,nu,False,1e-8)
law1.setStrainOrder(11)

law1.setViscoelasticMethod(0)
N = 3;
law1.setViscoElasticNumberOfElement(N)
law1.setViscoElasticData(1,700.,250.)
law1.setViscoElasticData(2,240,40)
law1.setViscoElasticData(3,130,2)

# geometry
meshfile="cylinder.msh" # name of mesh file

fullDg = 0 #O = CG, 1 = DG
space1 = 0 # function space (Lagrange=0)
beta1  = 100
# creation of part Domain
nfield = 11 # number of the field (physical number of surface)

myfield1 = dG3DDomain(1000,nfield,space1,lawnum,fullDg,3)
#myfield1.matrixByPerturbation(1,1,1,1e-8)
myfield1.stabilityParameters(beta1)

# solver
sol = 2  # Gmm=0 (default) Taucs=1 PETsc=2
soltype =1 # StaticLinear=0 (default) StaticNonLinear=1
nstep = 200  # number of step (used only if soltype=1)
ftime = 1.e3   # Final time (used only if soltype=1)
tol=1.e-8  # relative tolerance for NR scheme (used only if soltype=1)
nstepArch=1 # Number of step between 2 archiving (used only if soltype=1)



# creation of Solver
mysolver = nonLinearMechSolver(1000)
mysolver.loadModel(meshfile)
mysolver.addDomain(myfield1)
mysolver.addMaterialLaw(law1)
mysolver.Scheme(soltype)
mysolver.Solver(sol)
mysolver.snlData(nstep,ftime,tol)

mysolver.displacementBC('Face',1,2,0.)
mysolver.displacementBC('Face',3,1,0.)
mysolver.displacementBC('Face',4,0,0.)

mysolver.displacementBC('Face',1,0,0)
mysolver.displacementBC('Face',1,1,0)

fct = PiecewiseLinearFunction()
fct.put(0.,0.)
fct.put(0.25*ftime,1.)
fct.put(0.5*ftime,0.)
fct.put(0.75*ftime,1.)
fct.put(ftime,0.)

mysolver.displacementBC('Face',2,2,fct)

# build view
mysolver.internalPointBuildView("Strain_xx",IPField.STRAIN_XX, 1, 1);
mysolver.internalPointBuildView("Strain_yy",IPField.STRAIN_YY, 1, 1);
mysolver.internalPointBuildView("Strain_zz",IPField.STRAIN_ZZ, 1, 1);
mysolver.internalPointBuildView("Strain_xy",IPField.STRAIN_XY, 1, 1);
mysolver.internalPointBuildView("Strain_yz",IPField.STRAIN_YZ, 1, 1);
mysolver.internalPointBuildView("Strain_xz",IPField.STRAIN_XZ, 1, 1);
mysolver.internalPointBuildView("sig_xx",IPField.SIG_XX, 1, 1);
mysolver.internalPointBuildView("sig_yy",IPField.SIG_YY, 1, 1);
mysolver.internalPointBuildView("sig_zz",IPField.SIG_ZZ, 1, 1);
mysolver.internalPointBuildView("sig_xy",IPField.SIG_XY, 1, 1);
mysolver.internalPointBuildView("sig_yz",IPField.SIG_YZ, 1, 1);
mysolver.internalPointBuildView("sig_xz",IPField.SIG_XZ, 1, 1);
mysolver.internalPointBuildView("sig_VM",IPField.SVM, 1, 1);
mysolver.internalPointBuildView("Equivalent plastic strain",IPField.PLASTICSTRAIN, 1, 1);
mysolver.internalPointBuildView("pression",IPField.PRESSION,1,1)
mysolver.internalPointBuildView("plastic possion ratio",IPField.PLASTIC_POISSON_RATIO,1,1)

mysolver.internalPointBuildView("Ee_ZZ",IPField.Ee_ZZ,1,1)
mysolver.internalPointBuildView("Ee_YY",IPField.Ee_YY,1,1)
mysolver.internalPointBuildView("Ee_XX",IPField.Ee_XX,1,1)

mysolver.internalPointBuildView("Eve1_ZZ",IPField.Eve1_ZZ,1,1)
mysolver.internalPointBuildView("Eve1_YY",IPField.Eve1_YY,1,1)
mysolver.internalPointBuildView("Eve1_XX",IPField.Eve1_XX,1,1)

#archiving
mysolver.archivingForceOnPhysicalGroup('Face',1,0)
mysolver.archivingForceOnPhysicalGroup('Face',1,1)
mysolver.archivingForceOnPhysicalGroup('Face',1,2)
mysolver.archivingNodeDisplacement(5,0,1)
mysolver.archivingNodeDisplacement(5,1,1)
mysolver.archivingNodeDisplacement(5,2,1)
# solve
mysolver.solve()

check = TestCheck()
check.equal(1.802054e+03,mysolver.getArchivedForceOnPhysicalGroup("Face", 1, 2),1.e-3)
