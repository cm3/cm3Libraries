import pandas as pd
import os
import numpy as np
import matplotlib.pyplot as plt

filename = "/IPVolume1000val_TEMPERATUREMean.csv"

f = "./"+filename
newdataframe = pd.read_csv(f,sep=";",header=None).values
print(newdataframe.shape)
print()

# convert array into dataframe
DF = pd.DataFrame(newdataframe)

# save the dataframe as a csv file
#DF.to_csv("./TEMPERATUREMean.csv",sep=";",header=False,index=False)

plt.figure()
plt.title('Temperature Mean vs Time', size=14)
plt.xlabel('Time [s]', size=14)
plt.ylabel('Temperature Mean [K]', size=14)
plt.plot(newdataframe[:,0],newdataframe[:,1])
plt.savefig('./TemperatureMean.png', format='png')
plt.show()
