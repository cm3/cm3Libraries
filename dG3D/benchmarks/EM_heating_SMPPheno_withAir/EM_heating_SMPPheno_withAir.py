#coding-Utf-8-*-
from gmshpy import *


#from dG3DpyDebug import*
from dG3Dpy import*

#script to launch ElecMag SMP problem with a python script

import math

#==========Electric & magnetic parameters SMPC========================
rho	= 1200.

lx=ly=lz=1.0e5 # electrical conductivity (S/m) 
seebeck=0.0 #21.e-6
v0=0. # initial electric potential
epsilon_0 = 8.854 * 1.e-12 # vacuum electric permittivity
mag_mu_0 = 4.e-7 * math.pi # vacuum magnetic permeability
epsilon_r_smp = 1.0 # relative elec permittivity of SMP
mag_r_smp = 5.0 # relative mag permeability of SMP
# use of electric permittivity constants to formulate polarization
epsilon_4 = 0.0
epsilon_6 = 0.0
epsilon_5 = 0.0 #(epsilon_0 * (1.0-epsilon_r_smp)/rho)
# use of magnetic permeability constants to formulate magnetization
mu_7 = 0.0 # magnetic permeability constants of SMP
if(mag_r_smp == 1.0):
  mu_8 = 0.0
else:
  mu_8 = (rho * mag_mu_0)/((1.0/mag_r_smp)-1.0) # magnetic permeability constants of SMP
#print("mu_8 = ",mu_8)
mu_9 = 0.0 # magnetic permeability constants of SMP
mag_mu_x = mag_mu_y = mag_mu_z = mag_r_smp * mag_mu_0 # mag permeability smp 
magpot0_x = magpot0_y = magpot0_z = 0.0 # initial magnetic potential
Irms = 100.*325. # (Ampere)
freq = 1000. # (Hz)
nTurnsCoil = 1000
coilLx = coilLy = coilLz = 1. # m
coilW = 3.e-2 # m

# material law: SMP
RintSMP = 0.0095 #m
RextSMP = 0.01 #m
HSMP = 0.02 #m
crossSecAreaBase = math.pi * (RextSMP*RextSMP - RintSMP*RintSMP)
lawnum= 1 

#=====SMP Phenomenological Thermo-Mech=============================================
#crystalline phase#					
alpha=beta=gamma= 0.				
#rho	= 1200.
Eg	= 1250.e6 
Er	= 9.7e6 #3.2e6  
nug	= 0.26 #0.4
nur	= 0.26 #0.4
Gg	= Eg/2./(1+nug)
Gr	= Er/2./(1+nur)
Kg    = Eg/3./(1-2*nug)
Kr    = Er/3./(1-2*nur)

#amorphous phase#					
rhoAM	= rho
EAM	= 1.e5		
nuAM	= nur
GAM	= EAM/2./(1+nuAM)
KAM   = EAM/3./(1-2*nuAM)

Tc0	= 273.+8.	
Tm0	= 273.+45. 
wc0	= 8.	
wm0	= 5. 		

xi	= 0.		# rate parameter for Tt 				
alphag0	= 0 #0.8e-3*EAM/Eg #12.0e-5	
alphar0	= 8.e-4 #15.0e-5 	
alphaAD0= -0.015 #-0.0#-0.024
#crsytal contraction for low zg only
crystallizationRate = piecewiseScalarFunction() #surface should be 1
crystallizationRate.put(0.,20.)
crystallizationRate.put(0.1,0)
crystallizationRate.put(1.,0.)
  
cg	= 2020.*rho #1974210.
cr	= 2020.*rho #1744200.
Kxg=Kyg=Kzg=Kxr=Kyr=Kzr	= 0.22					  
TaylorQuineyG=TaylorQuineyR=TaylorQuineyAM= 1.		# fraction of plastic energy converted to thermal energy	

cfG	= 1.0		
cfR	= 0.0		

alphaAM0= 1.e-5#alphar0
cAM	= 2020.*rho
KxAM=KyAM=KzAM = Kxr

cfAM	= 0.0
zAM 	= 0.685  #0.685

tau_y0G = 1000.e6
tau_y0R = 6.e9 #0.6e6
tau_y0AM= 6.e9	
#// R = yield0 + h1 p^h2 if p>H3              OLD ONE
#// R = yield0 + (h1 pth^H2)/h3 p if p<=pth	OLD ONE
#  R = yield0 +h1 * p + h2* (1- exp(-hexp*p))  NEW ONE
HR1	= 1.e4 
HR2	= 0. 
HR3	= 0. 
HAM1	= 1.e4 
HAM2	= 0. 
HAM3	= 0. 
HG1	= 1.e6 #1e8 
HG2	= 0. #0.3 
HG3	= 300.
###

##extra branch
VKAM=2. #30.
VKR= 8.
VKG= 0. #8. 
thetaKAM=1.
thetaKR=1.
thetaKG=1. #6.
zetaKAM=1.2 #1
zetaKR=1.65
zetaKG=1.65 #250. #Has to be > 2.
VMUAM=2. #30.
VMUR= 8.
VMUG= 0. #10.
thetaMUAM=1.
thetaMUG=1.
thetaMUR=1. #0.225 #0.225
zetaMUAM=1.2 #1
zetaMUR=1.65
zetaMUG=1.65 #2.

########
#viscous#					
MU1G=4.e7;	KAPPA1G=1.;	TAU1G=15.; 	MU2G=5.e7; 	KAPPA2G=1.;	TAU2G=200.;
MU1R= 2.e5;	KAPPA1R=1.;	TAU1R=15.;	MU2R=2.e5;	KAPPA2R=1.;	TAU2R=300.;  MU3R=2.e4;	KAPPA3R=1.;	TAU3R=1000.;
MU1AM=MU1R*EAM/Er;	KAPPA1AM=KAPPA1R*EAM/Er;	TAU1AM=TAU1R;	MU2AM=MU2R*EAM/Er;	KAPPA2AM=KAPPA2R*EAM/Er;	TAU2AM=TAU2R; MU3AM=MU3R*EAM/Er;	KAPPA3AM=KAPPA3R*EAM/Er;	TAU3AM=TAU3R;
#####

Atc= 23.; #5.; #23.; 
Btm=-2.; 
Cwc=-4.; 
Dwm=-4.; 
alphatc=1.8;
alphatm=1.8;
alphawc=3.8;
alphawm=3.8;
  
#plastic flow during crystallisation
yieldCrystallinityG = piecewiseScalarFunction()
yieldCrystallinityG.put(0.,100000./tau_y0G)
yieldCrystallinityG.put(0.25,100000./tau_y0G)
#yieldCrystallinityG.put(0.,1.)
yieldCrystallinityG.put(1.,1.)
#plastic flow during crystallisation

### kinematic hardening
hardenkG = PolynomialKinematicHardening(4,5)
hardenkG.setCoefficients(1,0.e4)
hardenkG.setCoefficients(2,0.e6)
hardenkG.setCoefficients(3,0.e10)
hardenkG.setCoefficients(4,0.e10)
hardenkG.setCoefficients(5,7.5e10)
  
hardenkR = PolynomialKinematicHardening(5,5)
hardenkR.setCoefficients(1,170.e8)
hardenkR.setCoefficients(2,170.e8)
hardenkR.setCoefficients(3,-440.e8)
hardenkR.setCoefficients(4,1100.e8)

hardenkAM = PolynomialKinematicHardening(6,5)
hardenkAM.setCoefficients(1,170.e8)
hardenkAM.setCoefficients(2,170.e8)
hardenkAM.setCoefficients(3,-440.e8)
hardenkAM.setCoefficients(4,1100.e8)
### kinematic hardening

###param with crystalinity dilatation from Fg
#anisotropicThermalCoefAmplitudeBulk
GBulkDilatationAmplitude=0. #0.; # -2.; 
RBulkDilatationAmplitude=0.; #0.7 #1.5 #-3.; #-0.008*4.; 
CrystalizationAmplitudePositiveEigen=0.; #not use #0.55/2.; 
AMBulkDilatationAmplitude=0.; 
#
#anisotropicThermalCoefAmplitudeShearG
GShearDilatationAmplitude=0.; 
RShearDilatationAmplitude=0.3#0.2; #-0.003*4.; 
CrystalizationAmplitudeNegativeEigen=0.;#not use # 0.28/2.;
AMShearDilatationAmplitude=0.#0.001; 
#
#anisotropicThermalAlphaCoefTanhTempBulk
alphaBulkGDilatationTempCoef=0.02; 
alphaBulkRDilatationTempCoef=0.02; #1.1 
alphaCrystalizationPositiveEigen=0.; #not used 
alphaBulkAMDilatationTempCoef=0.02; 
#
#anisotropicThermalAlphaCoefTanhTempShearG
alphaShearGDilatationTempCoef=0.02; 
alphaShearRDilatationTempCoef=0.02; #1.1; 
alphaCrystalizationNegativeEigen=0.; #not used
alphaShearAMDilatationTempCoef=0.02; 

fieldSource =True  	#True #account for cp dT dt 	
mecaSource  =True 	#False #account for Taylor Quincey	

tinitial = 273.-10.
t0 = 273.+60.
t1 = 273.-10.
tair= 273.+20.;

_j2IHGt = LinearExponentialJ2IsotropicHardening(lawnum,  tau_y0G, HG1, HG2, HG3)
_j2IHGc = LinearExponentialJ2IsotropicHardening(lawnum+1,  1.2*tau_y0G, 1.2*HG1, 1.2*HG2, HG3)
_j2IHRt = LinearExponentialJ2IsotropicHardening(lawnum+2,  tau_y0R, HR1, HR2, HR3)
_j2IHRc = LinearExponentialJ2IsotropicHardening(lawnum+3,  1.2*tau_y0R, 1.2*HR1, 1.2*HR2, HR3)
_j2IHAMt = LinearExponentialJ2IsotropicHardening(lawnum+4,  tau_y0AM, HAM1, HAM2, HAM3)
_j2IHAMc = LinearExponentialJ2IsotropicHardening(lawnum+5,  1.2*tau_y0AM, 1.2*HAM1, 1.2*HAM2, HAM3)

lawSMPPheno= PhenomenologicalSMPDG3DMaterialLaw(lawnum+7, rho, alpha, beta, gamma, tinitial, 
                                           Kxg, Kyg, Kzg, Kxr, Kyr, Kzr, KxAM, KyAM, KzAM,
                                           cfG, cfR, cfAM, cg, cr, cAM,
                                           Tm0, Tc0, xi, wm0, wc0,
                                           alphag0, alphar0, alphaAM0, alphaAD0,
                                           Eg, Er, EAM, nug, nur, nuAM,
                                           TaylorQuineyG,TaylorQuineyR,TaylorQuineyAM, zAM)

lawSMPPheno.setFunctionCrystallizationVolumeRate(crystallizationRate)
lawSMPPheno.setStrainOrder(-1)
lawSMPPheno.setJ2IsotropicHardeningTractionG(_j2IHGt)
lawSMPPheno.setJ2IsotropicHardeningCompressionG(_j2IHGc)
lawSMPPheno.setKinematicHardeningG(hardenkG)
lawSMPPheno.setNonAssociatedFlowG(True);
lawSMPPheno.setPlasticPoissonRatioG(nug);
lawSMPPheno.setYieldPowerFactorG(3.5);
lawSMPPheno.setFunctionYieldCrystallinityG(yieldCrystallinityG);
  
lawSMPPheno.setJ2IsotropicHardeningTractionR(_j2IHRt)
lawSMPPheno.setJ2IsotropicHardeningCompressionR(_j2IHRc)
#lawSMPPheno.setKinematicHardeningR(hardenkR) 
lawSMPPheno.setNonAssociatedFlowR(True);
lawSMPPheno.setPlasticPoissonRatioR(nur);
lawSMPPheno.setYieldPowerFactorR(3.5);

lawSMPPheno.setJ2IsotropicHardeningTractionAM(_j2IHAMt)
lawSMPPheno.setJ2IsotropicHardeningCompressionAM(_j2IHAMc)
#lawSMPPheno.setKinematicHardeningAM(hardenkAM) 
lawSMPPheno.setNonAssociatedFlowAM(True);
lawSMPPheno.setPlasticPoissonRatioAM(nuAM);
lawSMPPheno.setYieldPowerFactorAM(3.5);

#lawSMPPheno.setElasticPotentialFunctionAM(EPFunc_AM)
#lawSMPPheno.setElasticPotentialFunctionR(EPFunc_R)
lawSMPPheno.setVolumeCorrectionAM(VKAM, thetaKAM, zetaKAM, VMUAM, thetaMUAM, zetaMUAM)
lawSMPPheno.setVolumeCorrectionR(VKR, thetaKR, zetaKR, VMUR, thetaMUR, zetaMUR)
lawSMPPheno.setVolumeCorrectionG(VKG, thetaKG, zetaKG, VMUG, thetaMUG, zetaMUG)

lawSMPPheno.setViscoelasticMethod(0)
lawSMPPheno.setViscoElasticNumberOfElementG(2)
lawSMPPheno.setViscoElasticData_BulkG(1, KAPPA1G, TAU1G) 
lawSMPPheno.setViscoElasticData_ShearG(1, MU1G, TAU1G) 
lawSMPPheno.setViscoElasticData_BulkG(2, KAPPA2G, TAU2G) 
lawSMPPheno.setViscoElasticData_ShearG(2, MU2G, TAU2G) 

lawSMPPheno.setViscoElasticNumberOfElementR(2)
lawSMPPheno.setViscoElasticData_BulkR(1, KAPPA1R, TAU1R) 
lawSMPPheno.setViscoElasticData_ShearR(1, MU1R, TAU1R) 
lawSMPPheno.setViscoElasticData_BulkR(2, KAPPA2R, TAU2R) 
lawSMPPheno.setViscoElasticData_ShearR(2, MU2R, TAU2R) 
#lawSMPPheno.setViscoElasticData_BulkR(3, KAPPA3R, TAU3R) 
#lawSMPPheno.setViscoElasticData_ShearR(3, MU3R, TAU3R) 

lawSMPPheno.setViscoElasticNumberOfElementAM(2)
lawSMPPheno.setViscoElasticData_BulkAM(1, KAPPA1AM, TAU1AM) 
lawSMPPheno.setViscoElasticData_ShearAM(1, MU1AM, TAU1AM) 
lawSMPPheno.setViscoElasticData_BulkAM(2, KAPPA2AM, TAU2AM) 
lawSMPPheno.setViscoElasticData_ShearAM(2, MU2AM, TAU2AM) 
#lawSMPPheno.setViscoElasticData_BulkAM(3, KAPPA3AM, TAU3AM) 
#lawSMPPheno.setViscoElasticData_ShearAM(3, MU3AM, TAU3AM) 

lawSMPPheno.setTcTmWcWm(Atc, Btm, Cwc, Dwm, alphatc,alphatm, alphawc, alphawm) 
lawSMPPheno.setAlphaParam(GBulkDilatationAmplitude, RBulkDilatationAmplitude, CrystalizationAmplitudePositiveEigen, AMBulkDilatationAmplitude, GShearDilatationAmplitude, RShearDilatationAmplitude, CrystalizationAmplitudeNegativeEigen, AMShearDilatationAmplitude,
       alphaBulkGDilatationTempCoef,alphaBulkRDilatationTempCoef,alphaCrystalizationPositiveEigen,alphaBulkAMDilatationTempCoef,
       alphaShearGDilatationTempCoef,alphaShearRDilatationTempCoef,alphaCrystalizationNegativeEigen,alphaShearAMDilatationTempCoef) 

# material law: inductor (copper coil)
# Material parameters for copper
lawnumind = 10
rhoind = 8960. 
Eind=120.e9 #youngs modulus
Gind=48.e9 # Shear modulus
Muind = 0.34 # Poisson ratio
alphaind = betaind = gammaind = 0. # parameter of anisotropy
cpind= 385.*rhoind
Kxind=Kyind=Kzind= 401. # thermal conductivity tensor components
lxind=lyind=lzind=5.77e7 # electrical conductivity (S/m)
seebeckind=0.0 #6.5e-6
alphaThermind= 0.0 # thermal dilatation
v0ind = 0.
mag_r_ind = 1.0 # relative mag permeability of inductor
mag_mu_x_ind = mag_mu_y_ind = mag_mu_z_ind = mag_r_ind * mag_mu_0 # mag permeability inductor 
magpot0_x_ind = magpot0_y_ind = magpot0_z_ind = 0.0 # initial magnetic potential
Centroid_x = Centroid_y = Centroid_z = 0.0 # Centroid of inductor coil
centralAxis_x = 0.
centralAxis_y = 1.
centralAxis_z = 0. # Unit vector along central axis of inductor coil

# material law: vacuum/free space region
lawnumvac = 13
rhovac = 1.2 
Gvac=156.e1 # Shear modulus
Muvac = 0.35 # Poisson ratio
Evac= 2.0 * Gvac * (1.0 + Muvac) #youngs modulus
alphavac = betavac = gammavac = 0. # parameter of anisotropy
cpvac= 1.e-12 #1012.*rhovac
Kxvac=Kyvac=Kzvac= 0.0 #1.e-12 #26.e-3 # thermal conductivity tensor components
lxvac=lyvac=lzvac= 1.e-12 # electrical conductivity
seebeckvac= 0.
alphaThermvac= 0.0 # thermal dilatation
v0vac = 0.
mag_r_vac = 1.0 # relative mag permeability of inductor
mag_mu_x_vac = mag_mu_y_vac = mag_mu_z_vac = mag_r_vac * mag_mu_0 # mag permeability inductor 
magpot0_x_vac = magpot0_y_vac = magpot0_z_vac = 0.0 # initial magnetic potential
epsilon_4_vac = 0.0
epsilon_6_vac = 0.0
epsilon_5_vac = 0.0
mu_7_vac = 0.0
mu_9_vac = 0.0 
mu_8_vac = 0.0 

useFluxT=True
evaluateCurlField = True;
evaluateTemperature = True;

# geometry
meshfile="undeformedSMPFullDomain.msh" # name of mesh file

# solver
sol = 2  # Gmm=0 (default) Taucs=1 PETsc=2
soltype = 1 # StaticLinear=0 (default) StaticNonLinear=1, Explicit=2, Multi=3, Implicit=4, Eigen=5
nstep = 1600 # 8*EMncycles # number of step (used only if soltype=1)
ftime = 1.e-3 # EMncycles/freq; # according to characteristic time
tol=1.e-3   # relative tolerance for NR scheme (used only if soltype=1)
nstepArch=1 # Number of step between 2 archiving (used only if soltype=1)
fullDg = False #O = CG, 1 = DG
space1 = 0 # function space (Lagrange=0)
beta1 = 40.
eqRatio =1.e6

#  compute solution and BC (given directly to the solver
# creation of law

# ElecMag SMP Phenomenological Thermo-Mech law
lawsmp = ElecMagGenericThermoMechanicsDG3DMaterialLaw(lawnum+8,rho,alpha,beta,gamma,lx,ly,lz,seebeck,v0,mag_mu_x,
mag_mu_y,mag_mu_z,epsilon_4,epsilon_5,epsilon_6,mu_7,mu_8,mu_9,magpot0_x,magpot0_y, magpot0_z, Irms, freq, nTurnsCoil, coilLx, coilLy, coilLz, coilW,useFluxT,evaluateCurlField,evaluateTemperature)

lawsmp.setThermoMechanicalMaterialLaw(lawSMPPheno);
lawsmp.setApplyReferenceF(False);
lawsmp.setUseEMStress(False);

#pertLawsmp = dG3DMaterialLawWithTangentByPerturbation(lawsmp,1.e-8)

#lawsmp.setUseBarF(True)

# material law for inductor region
sy0=1e10
h=1.e9
hardenind = LinearExponentialJ2IsotropicHardening(lawnumind, sy0, h, 0., 10.)
lawMechind   = J2LinearDG3DMaterialLaw(lawnumind,rhoind,Eind,Muind,hardenind)
lawMechind.setStrainOrder(-1)

lawTMind = GenericThermoMechanicsDG3DMaterialLaw(lawnumind+1,rhoind,alphaind,betaind,gammaind,tinitial,
Kxind,Kyind,Kzind,alphaThermind,alphaThermind,alphaThermind,cpind)
lawTMind.setMechanicalMaterialLaw(lawMechind);
lawTMind.setApplyReferenceF(False);

_cpind = constantScalarFunction(cpind);
lawTMind.setLawForCp(_cpind);

lawind = ElecMagInductorDG3DMaterialLaw(lawnumind+2,rhoind,alphaind,betaind,gammaind,lxind,lyind,lzind,seebeckind,
v0ind, mag_mu_x_ind, mag_mu_y_ind, mag_mu_z_ind, magpot0_x_ind,magpot0_y_ind, magpot0_z_ind, Irms, freq, nTurnsCoil, 
coilLx, coilLy, coilLz,coilW,Centroid_x,Centroid_y,Centroid_z,centralAxis_x,centralAxis_y,centralAxis_z,
useFluxT,evaluateCurlField)

lawind.setThermoMechanicalMaterialLaw(lawTMind);
lawind.setApplyReferenceF(False);

#lawind.setUseBarF(True)

#material law for free space (vacuum)
hardenvac = LinearExponentialJ2IsotropicHardening(lawnumvac, sy0, h, 0., 10.)
lawMechvac   = J2LinearDG3DMaterialLaw(lawnumvac,rhovac,Evac,Muvac,hardenvac)
lawMechvac.setStrainOrder(-1)

lawTMvac = GenericThermoMechanicsDG3DMaterialLaw(lawnumvac+1,rhovac,alphavac,betavac,gammavac,tinitial,
Kxvac,Kyvac,Kzvac,alphaThermvac,alphaThermvac,alphaThermvac,cpvac)
lawTMvac.setMechanicalMaterialLaw(lawMechvac);
lawTMvac.setApplyReferenceF(False);

_cpvac = constantScalarFunction(cpvac);
lawTMvac.setLawForCp(_cpvac);

lawvac = ElecMagGenericThermoMechanicsDG3DMaterialLaw(lawnumvac+2,rhovac,alphavac,betavac,gammavac,lxvac,lyvac,lzvac,
seebeckvac,v0vac, mag_mu_x_vac, mag_mu_y_vac, mag_mu_z_vac,epsilon_4_vac,epsilon_5_vac,epsilon_6_vac,mu_7_vac,mu_8_vac,mu_9_vac, magpot0_x_vac,
magpot0_y_vac, magpot0_z_vac, Irms, freq, nTurnsCoil, coilLx, coilLy, coilLz, coilW,useFluxT,evaluateCurlField,evaluateTemperature)

lawvac.setThermoMechanicalMaterialLaw(lawTMvac);
lawvac.setApplyReferenceF(False);

#pertLawvac = dG3DMaterialLawWithTangentByPerturbation(lawvac,1.e-8)

#lawvac.setUseBarF(True)

# creation of ElasticField
SMPfield = 1000 # number of the field (physical number of SMP)
Indfield = 2000 # number of the field (physical number of Inductor region)
Vacfield = 3000 # number of the field (physical number of Vacuum)
SurfVacOut = 3333 # number of the field (outer surface of Vacuum) Used for BC
SurfInd = 2222 # outer surface of inductor region
SurfSmp = 11110 # outer surface of conductor SMP
SmpRef=11111 # top face of outer surface of SMP
SmpRefBottom=11115 # bottom face of outer surface of SMP
SmpRefPoint=11112 # single point on top face of SMP

SMP_field = ElecMagTherMechDG3DDomain(1000,SMPfield,space1,lawnum+8,fullDg,eqRatio,3)
Inductor_field = ElecMagTherMechDG3DDomain(1000,Indfield,space1,lawnumind+2,fullDg,eqRatio,3)
Vacuum_field = ElecMagTherMechDG3DDomain(1000,Vacfield,space1,lawnumvac+2,fullDg,eqRatio,3)

SMP_field.setConstitutiveExtraDofDiffusionEqRatio(eqRatio)
Inductor_field.setConstitutiveExtraDofDiffusionEqRatio(eqRatio)
Vacuum_field.setConstitutiveExtraDofDiffusionEqRatio(eqRatio)
SMP_field.setConstitutiveCurlEqRatio(eqRatio)
Inductor_field.setConstitutiveCurlEqRatio(eqRatio)
Vacuum_field.setConstitutiveCurlEqRatio(eqRatio)
SMP_field.stabilityParameters(beta1)
Inductor_field.stabilityParameters(beta1)
Vacuum_field.stabilityParameters(beta1)

#SMP_field.setConstitutiveExtraDofDiffusionStabilityParameters(beta1,fullDg)
SMP_field.ElecMagTherMechStabilityParameters(beta1,fullDg)
Vacuum_field.ElecMagTherMechStabilityParameters(beta1,fullDg)
Inductor_field.ElecMagTherMechStabilityParameters(beta1,fullDg)

SMP_field.setConstitutiveExtraDofDiffusionAccountSource(fieldSource,mecaSource)
Vacuum_field.setConstitutiveExtraDofDiffusionAccountSource(fieldSource,mecaSource)
Inductor_field.setConstitutiveExtraDofDiffusionAccountSource(fieldSource,mecaSource)

#SMP_field.matrixByPerturbation(0,0,0,1.e-8)

# creation of Solver
mysolver = nonLinearMechSolver(1000)
mysolver.loadModel(meshfile)
mysolver.addDomain(SMP_field)
mysolver.addDomain(Inductor_field)
mysolver.addDomain(Vacuum_field)
mysolver.addMaterialLaw(lawsmp)
mysolver.addMaterialLaw(lawind)
mysolver.addMaterialLaw(lawvac)
mysolver.Scheme(soltype)
mysolver.Solver(sol)
mysolver.snlData(nstep,ftime,tol,tol/100.)
#mysolver.snlData(nstep,ftime*nstep/310960.,tol,tol/100.)
#mysolver.implicitSpectralRadius(0.01)  
mysolver.options("-ksp_type preonly -pc_type lu -pc_factor_mat_solver_type petsc")
mysolver.stepBetweenArchiving(nstepArch)
mysolver.snlManageTimeStep(50, 3, 2, 10)

# BC

#DispYY = 0.5e-2;
#cyclicFunctionDisp=cycleFunctionTime(0., 0., ftime/2., DispYY, ftime, DispYY);
"""mysolver.initialBC("Volume","Position",SMPfield,0,0.0) #-------------
mysolver.initialBC("Volume","Position",SMPfield,1,0.0) #-------------
mysolver.initialBC("Volume","Position",SMPfield,2,0.0) #-------------
mysolver.displacementBC("Face",SmpRefBottom,0,0.) #-------------
mysolver.displacementBC("Face",SmpRefBottom,1,0.) #-------------
mysolver.displacementBC("Face",SmpRefBottom,2,0.) """#-------------
#mysolver.displacementBC("Face",SmpRef,0,0.)
#mysolver.displacementBC("Face",SmpRef,1,cyclicFunctionDisp)
#mysolver.displacementBC("Face",SmpRef,2,0.)
mysolver.displacementBC("Volume",SMPfield,0,0.0) #-------------
mysolver.displacementBC("Volume",SMPfield,1,0.0) #-------------
mysolver.displacementBC("Volume",SMPfield,2,0.0) #-------------
mysolver.displacementBC("Volume",Indfield,0,0.0)
mysolver.displacementBC("Volume",Indfield,1,0.0)
mysolver.displacementBC("Volume",Indfield,2,0.0)
mysolver.displacementBC("Volume",Vacfield,0,0.0) #-------------
mysolver.displacementBC("Volume",Vacfield,1,0.0) #-------------
mysolver.displacementBC("Volume",Vacfield,2,0.0) #-------------
"""mysolver.displacementBC("Face",SurfVacOut,0,0.0)
mysolver.displacementBC("Face",SurfVacOut,1,0.0)
mysolver.displacementBC("Face",SurfVacOut,2,0.0)"""

#thermal BC
mysolver.initialBC("Volume","Position",SMPfield,3,tinitial)
mysolver.initialBC("Volume","Position",Indfield,3,tinitial)
mysolver.initialBC("Volume","Position",Vacfield,3,tinitial)
cyclicFunctionTempAir=cycleFunctionTime(0., tinitial,ftime,tinitial);
#mysolver.displacementBC("Volume",SMPfield,3,cyclicFunctionTemp1) #-------------
mysolver.displacementBC("Volume",Indfield,3,cyclicFunctionTempAir)
#mysolver.displacementBC("Volume",Vacfield,3,cyclicFunctionTemp1) #-------------
mysolver.displacementBC("Face",SurfVacOut,3,cyclicFunctionTempAir)

#electrical BC
mysolver.initialBC("Volume","Position",SMPfield,4,0.0)
mysolver.initialBC("Volume","Position",Indfield,4,0.0)
mysolver.initialBC("Volume","Position",Vacfield,4,0.0)
mysolver.displacementBC("Volume",Indfield,4,0.0)
mysolver.displacementBC("Face",SurfVacOut,4,0.0)
mysolver.displacementBC("Node",SmpRefPoint,4,0.0)

#magentic
mysolver.curlDisplacementBC("Face",SurfVacOut,5,0.0) # comp may also be 5
#Gauging the edges using tree-cotree method
PhysicalCurves = "" 		# input required as a string of comma separated ints
PhysicalSurfaces = "11110,3333" # input required as a string of comma separated ints
PhysicalVolumes = "1000,2000,3000" 	# input required as a string of comma separated ints
OutputPhysical = 55 		# input required as a int
mysolver.createTreeForBC(PhysicalCurves,PhysicalSurfaces,PhysicalVolumes,OutputPhysical)
mysolver.curlDisplacementBC("Edge",OutputPhysical,5,0.0)

mysolver.internalPointBuildView("svm",IPField.SVM, 1, 1)
mysolver.internalPointBuildView("sig_xx",IPField.SIG_XX, 1, 1)
mysolver.internalPointBuildView("sig_yy",IPField.SIG_YY, 1, 1)
mysolver.internalPointBuildView("sig_zz",IPField.SIG_ZZ, 1, 1)
mysolver.internalPointBuildView("sig_xy",IPField.SIG_XY, 1, 1)
mysolver.internalPointBuildView("sig_yz",IPField.SIG_YZ, 1, 1)
mysolver.internalPointBuildView("sig_xz",IPField.SIG_XZ, 1, 1)
mysolver.internalPointBuildView("eps_xx",IPField.STRAIN_XX, 1, 1)
mysolver.internalPointBuildView("eps_yy",IPField.STRAIN_YY, 1, 1)
mysolver.internalPointBuildView("eps_zz",IPField.STRAIN_ZZ, 1, 1)
mysolver.internalPointBuildView("eps_xy",IPField.STRAIN_XY, 1, 1)
mysolver.internalPointBuildView("eps_yz",IPField.STRAIN_YZ, 1, 1)
mysolver.internalPointBuildView("eps_xz",IPField.STRAIN_XZ, 1, 1)
mysolver.internalPointBuildView("temperature",IPField.TEMPERATURE, 1, 1)
mysolver.internalPointBuildView("w_AV",IPField.EMFIELDSOURCE, 1, 1)
mysolver.internalPointBuildView("w_T",IPField.THERMALSOURCE, 1, 1)
mysolver.internalPointBuildView("qx",IPField.THERMALFLUX_X, 1, 1)
mysolver.internalPointBuildView("qy",IPField.THERMALFLUX_Y, 1, 1)
mysolver.internalPointBuildView("qz",IPField.THERMALFLUX_Z, 1, 1)
mysolver.internalPointBuildView("voltage",IPField.VOLTAGE, 1, 1)
mysolver.internalPointBuildView("jex",IPField.ELECTRICALFLUX_X, 1, 1)
mysolver.internalPointBuildView("jey",IPField.ELECTRICALFLUX_Y, 1, 1)
mysolver.internalPointBuildView("jez",IPField.ELECTRICALFLUX_Z, 1, 1)
mysolver.internalPointBuildView("ax",IPField.MAGNETICVECTORPOTENTIAL_X, 1, 1)
mysolver.internalPointBuildView("ay",IPField.MAGNETICVECTORPOTENTIAL_Y, 1, 1)
mysolver.internalPointBuildView("az",IPField.MAGNETICVECTORPOTENTIAL_Z, 1, 1)
mysolver.internalPointBuildView("bx",IPField.MAGNETICINDUCTION_X, 1, 1)
mysolver.internalPointBuildView("by",IPField.MAGNETICINDUCTION_Y, 1, 1)
mysolver.internalPointBuildView("bz",IPField.MAGNETICINDUCTION_Z, 1, 1)
mysolver.internalPointBuildView("hx",IPField.MAGNETICFIELD_X, 1, 1)
mysolver.internalPointBuildView("hy",IPField.MAGNETICFIELD_Y, 1, 1)
mysolver.internalPointBuildView("hz",IPField.MAGNETICFIELD_Z, 1, 1)
mysolver.internalPointBuildView("dx",IPField.ELECDISPLACEMENT_X, 1, 1)
mysolver.internalPointBuildView("dy",IPField.ELECDISPLACEMENT_Y, 1, 1)
mysolver.internalPointBuildView("dz",IPField.ELECDISPLACEMENT_Z, 1, 1)
mysolver.internalPointBuildView("js0_x",IPField.INDUCTORSOURCEVECTORFIELD_X, 1, 1)
mysolver.internalPointBuildView("js0_y",IPField.INDUCTORSOURCEVECTORFIELD_Y, 1, 1)
mysolver.internalPointBuildView("js0_z",IPField.INDUCTORSOURCEVECTORFIELD_Z, 1, 1)
mysolver.internalPointBuildView("F_xx",IPField.F_XX, 1, 1)
mysolver.internalPointBuildView("F_xy",IPField.F_XY, 1, 1)
mysolver.internalPointBuildView("F_xz",IPField.F_XZ, 1, 1)
mysolver.internalPointBuildView("F_yx",IPField.F_YX, 1, 1)
mysolver.internalPointBuildView("F_yy",IPField.F_YY, 1, 1)
mysolver.internalPointBuildView("F_yz",IPField.F_YZ, 1, 1)
mysolver.internalPointBuildView("F_zx",IPField.F_ZX, 1, 1)
mysolver.internalPointBuildView("F_zy",IPField.F_ZY, 1, 1)
mysolver.internalPointBuildView("F_zz",IPField.F_ZZ, 1, 1)
mysolver.internalPointBuildView("epl",IPField.PLASTICSTRAIN, 1, 1)
mysolver.internalPointBuildView("jacobian",IPField.JACOBIAN, 1, 1)

mysolver.internalPointBuildView("FthG_YY", IPField.FthG_YY,    	1, 1)
mysolver.internalPointBuildView("FthG_ZZ", IPField.FthG_ZZ,    	1, 1)
mysolver.internalPointBuildView("FthR_YY", IPField.FthR_YY,    	1, 1)
mysolver.internalPointBuildView("FthR_ZZ", IPField.FthR_ZZ,   	1, 1)
mysolver.internalPointBuildView("FfG_YY", IPField.FfG_YY,    	1, 1)
mysolver.internalPointBuildView("FfG_ZZ", IPField.FfG_ZZ,    	1, 1)
mysolver.internalPointBuildView("FpG_YY", IPField.FpG_YY,    1, 1)
mysolver.internalPointBuildView("FpG_ZZ", IPField.FpG_ZZ,    1, 1)
mysolver.internalPointBuildView("epsG", IPField.epsG,      1, 1)
mysolver.internalPointBuildView("FveG_YY", IPField.FveG_YY,    1, 1)
mysolver.internalPointBuildView("FveG_ZZ", IPField.FveG_ZZ,   1, 1)
mysolver.internalPointBuildView("FP_YY", IPField.FP_YY,     1, 1)
mysolver.internalPointBuildView("FP_ZZ", IPField.FP_ZZ,      1, 1)
mysolver.internalPointBuildView("FpR_YY", IPField.FpR_YY,   1, 1)
mysolver.internalPointBuildView("FpR_ZZ", IPField.FpR_ZZ,    1, 1)
mysolver.internalPointBuildView("FveR_YY", IPField.FveR_YY,   1, 1)
mysolver.internalPointBuildView("FveR_ZZ", IPField.FveR_ZZ,   1, 1)

mysolver.internalPointBuildView("ZG", IPField.ZG,    1, 1)
mysolver.internalPointBuildView("PDF", IPField.PDF,        1, 1)
mysolver.internalPointBuildView("TT", IPField.TT,    1, 1)
mysolver.internalPointBuildView("WT", IPField.WT,    1, 1)
  
mysolver.internalPointBuildView("FthAM_YY", IPField.FthAM_YY,   1, 1)
mysolver.internalPointBuildView("FthAM_ZZ", IPField.FthAM_ZZ,   1, 1)
mysolver.internalPointBuildView("FfAM_YY", IPField.FfAM_YY,   1, 1)
mysolver.internalPointBuildView("FfAM_ZZ", IPField.FfAM_ZZ,   1, 1)

# ===Variable storage==============================================================================

mysolver.archivingIPOnPhysicalGroup("Volume",	SMPfield, IPField.SVM,		IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	SMPfield, IPField.PLASTICSTRAIN,	IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	SMPfield, IPField.epsR,		IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	SMPfield, IPField.TEMPERATURE,	IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	SMPfield, IPField.STRAIN_XX,	IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	SMPfield, IPField.STRAIN_YY,	IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	SMPfield, IPField.STRAIN_ZZ,	IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	SMPfield, IPField.STRAIN_XY,	IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	SMPfield, IPField.STRAIN_YZ,	IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	SMPfield, IPField.STRAIN_XZ,	IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	SMPfield, IPField.SIG_XX,		IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	SMPfield, IPField.SIG_YY,		IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	SMPfield, IPField.SIG_ZZ,		IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	SMPfield, IPField.SIG_XY,		IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	SMPfield, IPField.SIG_YZ,		IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	SMPfield, IPField.SIG_XZ,		IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	SMPfield, IPField.F_XX,		IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	SMPfield, IPField.F_YY,		IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	SMPfield, IPField.F_ZZ,		IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	SMPfield, IPField.JACOBIAN,	IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	SMPfield, IPField.P_XX,		IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	SMPfield, IPField.P_YY,		IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	SMPfield, IPField.P_ZZ,		IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	SMPfield, IPField.S_XX,		IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	SMPfield, IPField.S_YY,		IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	SMPfield, IPField.S_ZZ,		IPField.MEAN_VALUE);

mysolver.archivingIPOnPhysicalGroup("Volume",	SMPfield, IPField.FthI_YY,	IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	SMPfield, IPField.FthI_ZZ,	IPField.MEAN_VALUE);
#mysolver.archivingIPOnPhysicalGroup("Volume",	SMPfield, IPField.FthAI_YY,	IPField.MEAN_VALUE);
#mysolver.archivingIPOnPhysicalGroup("Volume",	SMPfield, IPField.FthAI_ZZ,	IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	SMPfield, IPField.FthAD_YY,	IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	SMPfield, IPField.FthAD_ZZ,	IPField.MEAN_VALUE);

mysolver.archivingIPOnPhysicalGroup("Volume",	SMPfield, IPField.FthG_YY,	IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	SMPfield, IPField.FthG_ZZ,	IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	SMPfield, IPField.FthR_YY,	IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	SMPfield, IPField.FthR_ZZ,	IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	SMPfield, IPField.FfG_YY,		IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	SMPfield, IPField.FfG_ZZ,		IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	SMPfield, IPField.FpG_YY,		IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	SMPfield, IPField.FpG_ZZ,		IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	SMPfield, IPField.FveG_YY,	IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	SMPfield, IPField.FveG_ZZ,	IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	SMPfield, IPField.FP_YY,		IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	SMPfield, IPField.FP_ZZ,		IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	SMPfield, IPField.FpR_YY,		IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	SMPfield, IPField.FpR_ZZ,		IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	SMPfield,	IPField.FveR_YY,	IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	SMPfield,	IPField.FveR_ZZ,	IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	SMPfield,	IPField.ZG,		IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	SMPfield,	IPField.PDF,		IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	SMPfield,	IPField.TT,		IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	SMPfield,	IPField.WT,		IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	SMPfield,	IPField.THERMALFLUX_X,	IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	SMPfield,	IPField.THERMALFLUX_Y,	IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	SMPfield,	IPField.THERMALFLUX_Z,	IPField.MEAN_VALUE);

mysolver.archivingIPOnPhysicalGroup("Volume",	SMPfield, IPField.FthAM_YY,	IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	SMPfield, IPField.FthAM_ZZ,	IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	SMPfield, IPField.FfAM_YY,	IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	SMPfield, IPField.FfAM_ZZ,	IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	SMPfield, IPField.FpAM_YY,	IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	SMPfield, IPField.FpAM_ZZ,	IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	SMPfield, IPField.FveAM_YY,	IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	SMPfield, IPField.FveAM_ZZ,	IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	SMPfield, IPField.epsAM,		IPField.MEAN_VALUE);

mysolver.archivingIPOnPhysicalGroup("Volume",	SMPfield, IPField.Tcrys,	IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	SMPfield, IPField.Tmelt,	IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	SMPfield, IPField.Wcrys,	IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	SMPfield, IPField.Wmelt,	IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	SMPfield, IPField.TRefG,	IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	SMPfield, IPField.TRefR,	IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	SMPfield, IPField.TRefAM, IPField.MEAN_VALUE);

mysolver.archivingIPOnPhysicalGroup("Volume",	SMPfield, IPField.ADl_XX,	IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	SMPfield, IPField.ADl_YY,	IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	SMPfield, IPField.ADl_ZZ,	IPField.MEAN_VALUE);

mysolver.archivingIPOnPhysicalGroup("Volume",	SMPfield, IPField.EnThG_XX,	IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	SMPfield, IPField.EnThG_YY,	IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	SMPfield, IPField.EnThG_ZZ,	IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	SMPfield, IPField.EnThR_XX,	IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	SMPfield, IPField.EnThR_YY,	IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	SMPfield, IPField.EnThR_ZZ,	IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	SMPfield, IPField.EnThAM_XX,	IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	SMPfield, IPField.EnThAM_YY,	IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	SMPfield, IPField.EnThAM_ZZ,	IPField.MEAN_VALUE);

mysolver.archivingIPOnPhysicalGroup("Volume",	SMPfield, IPField.appliedEnergy,	IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	SMPfield, IPField.DEFO_ENERGY,	IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	SMPfield, IPField._thermalEnergy,	IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	SMPfield, IPField.freezedDiss,	IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	SMPfield, IPField.plasticDiss,	IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	SMPfield, IPField.viscousDiss,	IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	SMPfield, IPField.EDiss,		IPField.MEAN_VALUE);

mysolver.archivingIPOnPhysicalGroup("Volume",SMPfield, IPField.VOLTAGE,IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",SMPfield, IPField.EMFIELDSOURCE,IPField.MEAN_VALUE);

mysolver.archivingIPOnPhysicalGroup("Volume",	Indfield,	IPField.INDUCTORSOURCEVECTORFIELD_X,	IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	Indfield,	IPField.INDUCTORSOURCEVECTORFIELD_Y,	IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	Indfield,	IPField.INDUCTORSOURCEVECTORFIELD_Z,	IPField.MEAN_VALUE);

mysolver.archivingIPOnPhysicalGroup("Volume",	SMPfield,	IPField.MAGNETICINDUCTION_X,	IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	SMPfield,	IPField.MAGNETICINDUCTION_Y,	IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	SMPfield,	IPField.MAGNETICINDUCTION_Z,	IPField.MEAN_VALUE);

mysolver.archivingIPOnPhysicalGroup("Volume",	SMPfield,	IPField.ELECTRICALFLUX_X,	IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	SMPfield,	IPField.ELECTRICALFLUX_Y,	IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	SMPfield,	IPField.ELECTRICALFLUX_Z,	IPField.MEAN_VALUE);

mysolver.archivingForceOnPhysicalGroup("Face", SurfSmp, 3, 1);
mysolver.archivingForceOnPhysicalGroup("Face", SurfSmp, 4, 1);
mysolver.archivingForceOnPhysicalGroup("Face", SurfSmp, 5, 1);

mysolver.archivingForceOnPhysicalGroup("Face", SurfInd, 3, 1);
mysolver.archivingForceOnPhysicalGroup("Face", SurfInd, 4, 1);
mysolver.archivingForceOnPhysicalGroup("Face", SurfInd, 5, 1);

mysolver.archivingForceOnPhysicalGroup("Face", SurfVacOut, 3, 1);
mysolver.archivingForceOnPhysicalGroup("Face", SurfVacOut, 4, 1);
mysolver.archivingForceOnPhysicalGroup("Face", SurfVacOut, 5, 1);

mysolver.archivingForceOnPhysicalGroup("Face", SmpRef, 0, 1);
mysolver.archivingForceOnPhysicalGroup("Face", SmpRef, 1, 1);
mysolver.archivingForceOnPhysicalGroup("Face", SmpRef, 2, 1);

mysolver.archivingForceOnPhysicalGroup("Face", SmpRefBottom, 0, 1);
mysolver.archivingForceOnPhysicalGroup("Face", SmpRefBottom, 1, 1);
mysolver.archivingForceOnPhysicalGroup("Face", SmpRefBottom, 2, 1);

mysolver.archivingForceOnPhysicalGroup("Node", SmpRefPoint, 0, nstepArch);
mysolver.archivingForceOnPhysicalGroup("Node", SmpRefPoint, 1, nstepArch);
mysolver.archivingForceOnPhysicalGroup("Node", SmpRefPoint, 2, nstepArch);

mysolver.archivingNodeDisplacement(SmpRefPoint,0,1);
mysolver.archivingNodeDisplacement(SmpRefPoint,1,1);
mysolver.archivingNodeDisplacement(SmpRefPoint,2,1);
mysolver.archivingNodeDisplacement(SmpRefPoint,3,1);
mysolver.archivingNodeDisplacement(SmpRefPoint,4,1);

#mysolver.setWriteDeformedMeshToFile(True);
mysolver.createRestartBySteps(50);
mysolver.solve()

"""
check = TestCheck()

check.equal(-2.396657e-07,mysolver.getArchivedForceOnPhysicalGroup("Face", SurfSmp, 4),1.e-5)
check.equal(-5.608847e-07,mysolver.getArchivedForceOnPhysicalGroup("Face", SurfInd, 4),1.e-5)
check.equal(8.005502e-07,mysolver.getArchivedForceOnPhysicalGroup("Face", SurfVacOut, 4),1.e-5)
check.equal(-9.586306e-06,mysolver.getArchivedForceOnPhysicalGroup("Face", SurfSmp, 5),1.e-5)
check.equal(2.028724e-06,mysolver.getArchivedForceOnPhysicalGroup("Face", SurfInd, 5),1.e-5)
check.equal(2.448701e+00,mysolver.getArchivedForceOnPhysicalGroup("Face", SurfVacOut, 5),1.e-5)
check.equal(0.000000e+00,mysolver.getArchivedNodalValue(SmpRefPoint,1,mysolver.displacement),1.e-5)
check.equal(2.630001e+02,mysolver.getArchivedNodalValue(SmpRefPoint,3,mysolver.displacement),1.e-5)
"""
