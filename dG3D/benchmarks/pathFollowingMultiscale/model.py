#coding-Utf-8-*-
from gmshpy import *
from dG3Dpy import*

#script to launch PBC problem with a python script

#DEFINE MICRO PROBLEM

# micro-material law
lawnum1 = 11 # unique number of law


rho   = 7850e-9
young = 28.9e3
nu    = 0.3 
sy0   = 99.
h     = young/100.

harden = LinearExponentialJ2IsotropicHardening(1, sy0, h, 0., 10.)
law1   = J2LinearDG3DMaterialLaw(lawnum1,rho,young,nu,harden)


micromeshfile="micro.msh" # name of mesh file
#micromeshfile="square.msh" # name of mesh file

# creation of part Domain
nfield = 11 # number of the field (physical number of entity)
myfield1 = dG3DDomain(1000,nfield,0,lawnum1,0,2)

microBC = nonLinearPeriodicBC(1000,2)
microBC.setOrder(1)
microBC.setBCPhysical(1,2,3,4)
# periodiodic BC
method = 0 # Periodic mesh = 0 Langrange interpolation = 1 Cubic spline interpolation =2
degree = 5 
addvertex = False
microBC.setPeriodicBCOptions(method, degree,addvertex)

# DEFINE MACROPROBLEM
matnum1 = 1;
macromat1 = dG3DMultiscaleMaterialLaw(matnum1, 1000)
#for i in range(0,1):
macromat1.setViewAllMicroProblems(True,0)
#macromat1.addViewMicroSolver(46,0)
#macromat1.addViewMicroSolver(47,0)

microSolver1 = macromat1.getMicroSolver()
microSolver1.loadModel(micromeshfile);
microSolver1.addDomain(myfield1)
microSolver1.addMaterialLaw(law1);
microSolver1.addMicroBC(microBC)

microSolver1.snlData(1,1.,1e-6,1e-10)
microSolver1.setSystemType(1)
microSolver1.Solver(2)
microSolver1.setExtractIrreversibleEnergyFlag(True)

microSolver1.Scheme(1)
microSolver1.setSameStateCriterion(1e-16)
microSolver1.stressAveragingFlag(True) # set stress averaging ON- 0 , OFF-1
microSolver1.setStressAveragingMethod(0)
microSolver1.tangentAveragingFlag(True) # set tangent averaging ON -0, OFF -1
microSolver1.setTangentAveragingMethod(2,1e-6);

microSolver1.pathFollowing(True,0)
microSolver1.setPathFollowingControlType(1)

microSolver1.internalPointBuildView("Green-Lagrange_xx",IPField.STRAIN_XX);
microSolver1.internalPointBuildView("Green-Lagrange_yy",IPField.STRAIN_YY);
microSolver1.internalPointBuildView("Green-Lagrange_xy",IPField.STRAIN_XY);
microSolver1.internalPointBuildView("sig_xx",IPField.SIG_XX);
microSolver1.internalPointBuildView("sig_yy",IPField.SIG_YY);
microSolver1.internalPointBuildView("sig_xy",IPField.SIG_XY);
microSolver1.internalPointBuildView("sig_VM",IPField.SVM);

microSolver1.internalPointBuildView("F_xx",IPField.F_XX);
microSolver1.internalPointBuildView("F_yy",IPField.F_YY);
microSolver1.internalPointBuildView("F_xy",IPField.F_XY);
microSolver1.internalPointBuildView("F_yx",IPField.F_YX);

microSolver1.internalPointBuildView("P_xx",IPField.P_XX);
microSolver1.internalPointBuildView("P_yy",IPField.P_YY);
microSolver1.internalPointBuildView("P_xy",IPField.P_XY);
microSolver1.internalPointBuildView("P_yx",IPField.P_YX);

microSolver1.internalPointBuildView("Equivalent plastic strain",IPField.PLASTICSTRAIN);
microSolver1.internalPointBuildView("Deformation energy",IPField.DEFO_ENERGY);
microSolver1.internalPointBuildView("Plastic energy",IPField.PLASTIC_ENERGY);

microSolver1.displacementBC("Face",11,2,0.)


macromeshfile="model.msh" # name of mesh file

# solver
sol = 2  # Gmm=0 (default) Taucs=1 PETsc=2
soltype =1 # StaticLinear=0 (default) StaticNonLinear=1
nstep = 16  # number of step (used only if soltype=1)
ftime =1.   # Final time (used only if soltype=1)
tol=1.e-5  # relative tolerance for NR scheme (used only if soltype=1)
nstepArch=3 # Number of step between 2 archiving (used only if soltype=1)


# creation of  macro part Domain

dim =2
beta1 = 50;
fullDG = False;

averageStrainBased = True

# non DG domain
nfield1 = 11
macrodomain1 = dG3DDomain(10,nfield1,0,matnum1,fullDG,dim)
macrodomain1.stabilityParameters(beta1)
macrodomain1.averageStrainBased(averageStrainBased)
macrodomain1.setDistributedOtherRanks(True)
macrodomain1.distributeOnRootRank(False)

# creation of Solver
mysolver = nonLinearMechSolver(1000)
mysolver.loadModel(macromeshfile)

mysolver.addDomain(macrodomain1)
mysolver.addMaterialLaw(macromat1)
mysolver.setMultiscaleFlag(True)
mysolver.Scheme(soltype)
mysolver.Solver(sol)
mysolver.snlData(nstep,ftime,tol)
mysolver.stepBetweenArchiving(nstepArch)
mysolver.options("-ksp_type preonly -pc_type lu -pc_factor_mat_solver_type petsc")

mysolver.pathFollowing(True,1)
# time-step adaptation by number of NR iterations
mysolver.setPathFollowingIncrementAdaptation(True,5)
mysolver.setPathFollowingLocalSteps(2e-2,1e-6)
mysolver.setPathFollowingLocalIncrementType(1); 

# boundary condition
mysolver.displacementBC("Face",11,2,0.0)

mysolver.displacementBC("Edge",1,1,0.0)
mysolver.displacementBC("Edge",4,0,0.0)

#mysolver.displacementBC("Edge",3,1,1.e-1)

#mysolver.constraintBC("Edge",3,1)
mysolver.forceBC("Edge",3,1,1e2)


# archivage
mysolver.internalPointBuildView("Green-Lagrange_xx",IPField.STRAIN_XX, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_yy",IPField.STRAIN_YY, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_zz",IPField.STRAIN_ZZ, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_xy",IPField.STRAIN_XY, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_yz",IPField.STRAIN_YZ, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_xz",IPField.STRAIN_XZ, 1, 1);
mysolver.internalPointBuildView("sig_xx",IPField.SIG_XX, 1, 1);
mysolver.internalPointBuildView("sig_yy",IPField.SIG_YY, 1, 1);
mysolver.internalPointBuildView("sig_zz",IPField.SIG_ZZ, 1, 1);
mysolver.internalPointBuildView("sig_xy",IPField.SIG_XY, 1, 1);
mysolver.internalPointBuildView("sig_yz",IPField.SIG_YZ, 1, 1);
mysolver.internalPointBuildView("sig_xz",IPField.SIG_XZ, 1, 1);
mysolver.internalPointBuildView("sig_VM",IPField.SVM, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange equivalent strain",IPField.GL_EQUIVALENT_STRAIN, 1, 1);
mysolver.internalPointBuildView("Equivalent plastic strain",IPField.PLASTICSTRAIN, 1, 1);

mysolver.internalPointBuildView("Deformation energy",IPField.DEFO_ENERGY, 1, 1);
mysolver.internalPointBuildView("Plastic energy",IPField.PLASTIC_ENERGY, 1, 1);

mysolver.archivingForceOnPhysicalGroup("Edge",1,1)
mysolver.archivingNodeDisplacement(5,1)

# solve
mysolver.solve()

check = TestCheck()
check.equal(-3.563525e+02,mysolver.getArchivedForceOnPhysicalGroup("Edge", 1, 1),1.e-4)

