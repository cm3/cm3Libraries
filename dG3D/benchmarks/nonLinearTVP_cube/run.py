import os,csv
import shutil
import fnmatch
import pickle
import csv
import pandas as pd
from loadCase_ParaValues_PP_cube import *

def checkEqual(ref, cur, tol):
  if(abs(ref-cur)>abs(tol*ref)):
    print("Error : reference  value ",ref," current value ",cur," relative error ", (ref-cur)/ref," tolerance ",tol)


### list LoadCase(1/-1/2/3, strainrate, Temperature, finalstrain);  #####
# "1"-tensile, "-1"-compresion, "2" - tensile cyclic, "3" - compression cyclic
LoadCase = []
LoadCase.append((1, 0.01, 23, 0.05))
extra = []
extra = extra + extraBranch[0]
extra.append(extraBranch_Comp1[0])
extra = extra + extraBranch_additional[0]
IsoHard = [True, 28.9, 5.188, 5.188, 30, 1., True,
           1.22820269e-02, 0., 0., 0, 9.43, 0, 5.128, 15]
KinHard = [False, kinPara1[0], kinPara2[0],
           kinPara3[0], False, 8.41961004e-03, 0., 0.]
YieldFunctionParas = [1.5, 0.64]
Viscosity = [True, 5388.03, 0.21,
             True, 2.05479459e-02, 0., 0.]
TVE = [True, True]
Mullins = [True, 0.5, False, 0., 0., 0.]
HeatSrc = [True, True]
rotationCorrection = [False]
regularisation = [1000]
MatPara = FillMatPara(extra, IsoHard, KinHard, YieldFunctionParas, Viscosity,
                      TVE, Mullins, extraTVE, HeatSrc, rotationCorrection, regularisation)
# print(MatPara)
print(LoadCase[0])
with open('LoadCases.dat', 'wb') as data1:
  pickle.dump(LoadCase[0], data1)
with open('MatPara.dat', 'wb') as data2:
  pickle.dump(MatPara, data2)

os.system('python3 runCaseScript_PP_cube.py')

# test check

data1 = csv.reader(open('Average_P_XX.csv'), delimiter=';')
force = list(data1)
checkEqual(23.19336685582314,float(force[-1][1]),1e-3)

data2 = csv.reader(open('Average_TEMPERATURE.csv'), delimiter=';')
temperaturefinal = list(data2)
checkEqual(296.368396498814,float(temperaturefinal[-1][1]),1e-3)







