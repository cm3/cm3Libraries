from gmshpy import *
from dG3Dpy import *
from math import*
import csv
import numpy as np
import pandas as pd
import pickle

######## list LoadCase(1/-1/2, strainrate, Temperature, finalstrain);  "1"-tensile, "-1"-compresion, "2" - tensile cyclic
######## list MatPara( (ExtraBranch), (IsoHard), (KinHard), (YieldFunctionParas), (Viscosity), (TVE_Boolean), (Mullins), (extraTVE)  )
#### MatPara[0][...] -> ExtraBranch -> (4 + 1 parameters)
#### MatPara[1][...] -> IsoHard -> (0-boolean,1-yieldStrenght,2-isoPara1,3-isoPara2,4-isoPara3,5-m,6-tempBoolean,7-isoTempFunc1,8-isoTempFunc2,9-isoTempFunc3,10-TYPE)
#### MatPara[2][...] -> KinHard -> (0-boolean,1-kinPara1,2-kinPara2,3-kinPara3,4-tempBoolean,5-kinTempFunc1,6-kinTempFunc2,7-kinTempFunc3)
#### MatPara[3][...] -> YieldFunctionParas -> (0-YieldPowerFactor,1-FlowRuleFactor)
#### MatPara[4][...] -> Viscosity -> (0-boolean,1-viscPara1,2-viscPara2,3-tempBoolean,4-viscoTempFunc1,5-viscoTempFunc2,6-viscoTempFunc3)
#### MatPara[5][...] -> TVE -> (0-TVE_Boolean, 1-ExtraBranch_Boolean)
#### MatPara[6][...] -> Mullins -> (0-boolean,1-mullinsPara1,2-tempBoolean,3-mullinsTempFunc1,4-mullinsTempFunc2,5-mullinsTempFunc3)
#### MatPara[7][...] -> ExtraTVE - material dependent
#### MatPara[8][...] -> heatSrc -> (0-boolean ThermSrc and tempBC, 1-boolean MechSrc)
#### MatPara[9][...] -> rotationCorrection -> (0-boolean)
#### MatPara[10][...] -> regularisation -> (0-regularisation constant)
############################################################################
def RunCase(LoadCase,MatPara):

  # Load the csv data for relaxation spectrum
  with open('relaxationSpectrum_Et_N27_19_04_24_Tref20.txt', newline='') as csvfile:
      reader = csv.reader(csvfile, delimiter=';')
      relSpec = np.zeros((1,))
      i = 0
      for row in reader:
          if i == 0:
              relSpec[i] = float(' '.join(row))
          else:
              relSpec = np.append(relSpec, float(' '.join(row)))
          i += 1
  N = int(0.5*(i-1))
  relSpec[0:N+1] *= 1e-6    # convert to MPa

  nstep = 100 # number of step (used only if soltype=1)
  if (LoadCase[0] == 2 or LoadCase[0] == 3):
       nstep = 1000

  # === Problem parameters ============================================================================
  lawnum = 11
  if (MatPara[5][0] == True):
    E = relSpec[0] #MPa
  else:
    E = 100 # relSpec[0] #MPa
  nu = 0.33  # 0.4
  rho = 905e-12 # Bulk mass   905 kg/m3 -> 905e-12 tonne/mm3
  Cp = rho*1900e+6 # 1900 J/kg/K -> 1900e+6 Nmm/tonne/K
  KThCon = 0.14  # 0.14 W/m/K -> 0.14 Nmm/s/mm/K
  Alpha = 0.6e-6 # 1/K

  Tinitial = 273.15 + LoadCase[2] # K
  C1 = 112.94742152
  C2 = 176.44294359

  # Temp Settings
  Tref = 273.15+20.

  # Default tempfuncs
  Alpha_G = 0.2e-6
  Alpha_R = 1.e-6
  KThCon_G = 0.10
  KThCon_R = 1.5*0.12
  k_gibson = 3.36435338e-02
  m_gibson = 0.0
  Tg = Tref
  Alpha_tempfunc = GlassTransitionScalarFunction(1, Alpha_R/Alpha_G, Tg, k_gibson, m_gibson, 0)
  KThCon_tempfunc = GlassTransitionScalarFunction(1, KThCon_R/KThCon_G, Tg, k_gibson, m_gibson, 0)

  # Hardening and tempfuncs
  isoTempFunc = negativeExponentialFunction(MatPara[1][7],Tref)  # for compression isoHard
  kinTempFunc = negativeExponentialFunction(MatPara[2][5],Tref)  # for tension isoHard (temporarily)
  viscTempFunc = negativeExponentialFunction(MatPara[4][4],Tref)
  mullinsTempFunc = negativeExponentialFunction(MatPara[6][3],Tref)

  hc = MatPara[1][2]
  ht = MatPara[1][2]
  m = MatPara[1][5]
  sy0c = MatPara[1][1]/m
  sy0t = sy0c*m
  if (MatPara[1][10] == 0):
      print("TC Assymetric IsoHard")
      hardenc = LinearExponentialJ2IsotropicHardening(1,MatPara[1][1],MatPara[1][2],MatPara[1][3],MatPara[1][4])
      hardent = LinearExponentialJ2IsotropicHardening(2,MatPara[1][11],MatPara[1][12],MatPara[1][13],MatPara[1][14])

  if (MatPara[1][0] == True) and (MatPara[1][6] == True):
      hardenc.setTemperatureFunction_Sy0(isoTempFunc)
      hardenc.setTemperatureFunction_h1(isoTempFunc)
      hardenc.setTemperatureFunction_h2(isoTempFunc)
      hardent.setTemperatureFunction_Sy0(kinTempFunc)
      hardent.setTemperatureFunction_h1(kinTempFunc)
      hardent.setTemperatureFunction_h2(kinTempFunc)

  hardenk = PolynomialKinematicHardening(3,3)
  hardenk.setCoefficients(1,MatPara[2][1])
  hardenk.setCoefficients(2,MatPara[2][2])
  hardenk.setCoefficients(3,MatPara[2][3])
  if (MatPara[2][0] == True) and (MatPara[2][4] == True):
      hardenk.setTemperatureFunction_K(kinTempFunc)

  # material law
  law1 = NonLinearTVENonLinearTVPDG3DMaterialLaw(lawnum,rho,E,nu,1e-6,Tinitial,Alpha,KThCon,Cp,False,1e-8,1e-9)
  if (MatPara[1][0] == True):
      law1.setCompressionHardening(hardenc)
      law1.setTractionHardening(hardent)
  if (MatPara[2][0] == True):
      law1.setKinematicHardening(hardenk)

  law1.setYieldPowerFactor(MatPara[3][0])
  law1.setNonAssociatedFlow(True)
  law1.nonAssociatedFlowRuleFactor(MatPara[3][1])

  law1.setStrainOrder(-1)
  law1.setShiftFactorConstantsWLF(C1,C2)
  law1.setReferenceTemperature(Tref)

  law1.useRotationCorrectionBool(MatPara[9][0],2)

  if (MatPara[5][1] == True):
      # print(MatPara[0])
      law1.setExtraBranchNLType(4) # 4 -> PP, 5 -> TPU
      law1.useExtraBranchBool(True)
      law1.useExtraBranchBool_TVE(True,4) # 4 -> PP, 5 -> TPU
      law1.setVolumeCorrection(MatPara[0][0], MatPara[0][1], MatPara[0][2], MatPara[0][3], MatPara[0][4], MatPara[0][5])
      law1.setExtraBranch_CompressionParameter(MatPara[0][6],0.,0.)
      law1.setAdditionalVolumeCorrections(MatPara[0][7], MatPara[0][8], MatPara[0][9], MatPara[0][10], MatPara[0][11], MatPara[0][12])
      law1.setTensionCompressionRegularisation(MatPara[10][0])

  if (MatPara[5][0] == True):
      law1.setViscoelasticMethod(0)
      law1.setViscoElasticNumberOfElement(N)
      extraTVE = MatPara[7]
      if N > 0:
          for i in range(1, N+1):
              law1.setViscoElasticData(i, relSpec[i], relSpec[i+N])
              if (MatPara[5][1] == True):
                  law1.setCorrectionsAllBranchesTVE(i, extraTVE[i-1][0], extraTVE[i-1][1], extraTVE[i-1][2], extraTVE[i-1][3], extraTVE[i-1][4], extraTVE[i-1][5])
                  law1.setCompressionCorrectionsAllBranchesTVE(i, extraTVE[i-1][6])
                  law1.setAdditionalCorrectionsAllBranchesTVE(i, extraTVE[i-1][7], extraTVE[i-1][8], extraTVE[i-1][9], extraTVE[i-1][10], extraTVE[i-1][11], extraTVE[i-1][12])
  # law1.setTemperatureFunction_ThermalDilationCoefficient(Alpha_tempfunc)
  # law1.setTemperatureFunction_ThermalConductivity(KThCon_tempfunc)

  if(LoadCase[2] == 23):
      MatPara[6][1] = 0.5
  elif(LoadCase[2] == -10):
      MatPara[6][1] = 0.6
  elif(LoadCase[2] == 70):
      MatPara[6][1] = 0.95

  mullins = linearScaler(4, MatPara[6][1])
  if (MatPara[6][0] == True):
      law1.setMullinsEffect(mullins)
      if (MatPara[6][2] == True):
          mullins.setTemperatureFunction_r(mullins_tempfunc)

  if (MatPara[4][0] == True):
      eta = constantViscosityLaw(1,MatPara[4][1])
      # eta = saturatePowerViscosityLaw(1,MatPara[4][1],2.5)
      law1.setViscosityEffect(eta,MatPara[4][2])
      if (MatPara[4][3] == True):
          eta.setTemperatureFunction(viscTempFunc)
  law1.setIsotropicHardeningCoefficients(1.,1.,1.)

  #-----------------------------------------------------------------------------
  strainrate = LoadCase[1]
  strain_end = LoadCase[3]
  # ftime = 2.*strain_end/strainrate	# Final time (used only if soltype=1)
  ftime = 1.*strain_end/strainrate	# Final time (used only if soltype=1)
  if (LoadCase[0] == 2):
      if(LoadCase[2] == 70):
          ftime = 77.e-2/strainrate
      elif(LoadCase[2] == -10):
          ftime = 15.e-2/strainrate
      elif(LoadCase[2] == 23):
          ftime = 28.e-2/strainrate
  elif (LoadCase[0] == 3):
      ftime = 300.5e-2/strainrate
  # print("######################",strain_end,strainrate,ftime)

  # ===Solver parameters=============================================================================
  sol = 2  # Gmm=0 (default) Taucs=1 PETsc=2
  soltype = 1 # StaticLinear=0 (default) StaticNonLinear=1
  tol=1.e-5  # relative tolerance for NR scheme (used only if soltype=1)
  nstepArch=10 # Number of step between 2 archiving (used only if soltype=1)
  fullDg = 0 #O = CG, 1 = DG
  space1 = 0 # function space (Lagrange=0)
  beta1  = 100
  nfield = 29

  # ===Domain creation===============================================================================
  # creation of ElasticField
  ThermoMechanicsEqRatio = 1.e1
  thermalSource = MatPara[8][0]
  mecaSource = MatPara[8][1]
  myfield1 = ThermoMechanicsDG3DDomain(1000,nfield,space1,lawnum,fullDg,ThermoMechanicsEqRatio)
  myfield1.setConstitutiveExtraDofDiffusionAccountSource(thermalSource,mecaSource)
  myfield1.stabilityParameters(beta1)
  myfield1.ThermoMechanicsStabilityParameters(beta1,bool(1))

  # ===Solver creation===============================================================================
  # geometry & mesh
  geofile = "cube.geo"
  meshfile = "cube.msh"

  # creation of Solver
  mysolver = nonLinearMechSolver(1000)
  mysolver.createModel(geofile,meshfile,3,2)
  #mysolver.loadModel(meshfile)
  mysolver.addDomain(myfield1)
  mysolver.addMaterialLaw(law1)
  mysolver.Scheme(soltype)
  mysolver.Solver(sol)
  mysolver.snlData(nstep,ftime,tol)
  mysolver.stepBetweenArchiving(nstepArch)

  mysolver.snlManageTimeStep(25,5,2.,50) # maximal 20 times

# ===Boundary conditions===========================================================================
  mysolver.displacementBC("Face",30,0,0.)		    # face x = 0   - Left Face fixed
  mysolver.displacementBC("Face",34,1,0.)		    # face y = 0
  mysolver.displacementBC("Face",32,2,0.)		    # face z = 0

  disp = PiecewiseLinearFunction()
  L = 1
  if(LoadCase[0] != 2 and LoadCase[0] != 5):
      disp.put(0.,0.)
      disp.put(ftime, LoadCase[0]*strain_end*L)
      # disp.put(0.5*ftime, LoadCase[0]*strain_end*L)
      # disp.put(ftime,0.)
  elif(LoadCase[0] == 2):
      if(LoadCase[2] == 70):
          disp.put(0.,0.)
          disp.put(1.e-2/strainrate,1e-2*L)
          disp.put(2.e-2/strainrate,0.00)
          disp.put(4.5e-2/strainrate,2.5e-2*L)
          disp.put(7.e-2/strainrate,0.0)
          disp.put(12.e-2/strainrate,5.e-2*L)
          disp.put(17.e-2/strainrate,0.0)
          disp.put(27.e-2/strainrate,10.e-2*L)
          disp.put(37.e-2/strainrate,0.0)
          disp.put(57.e-2/strainrate,20.e-2*L)
          disp.put(77.e-2/strainrate,0.0)
      elif(LoadCase[2] == -10):
          disp.put(0,0)
          disp.put(0.5*1e-2/strainrate,0.5*1e-2*L)
          disp.put(1.e-2/strainrate,0.00) # 1
          disp.put(3.e-2/strainrate,2.e-2*L)
          disp.put(5.e-2/strainrate,0.00) # 2
          disp.put(8.e-2/strainrate,3.e-2*L)
          disp.put(11.e-2/strainrate,0.00)
          disp.put(15.e-2/strainrate,4.e-2*L)
          # disp.put(19.e-2/strainrate,0.00)
      elif(LoadCase[2] == 23):
          disp.put(0,0)
          disp.put(0.5e-2/strainrate,0.5e-2*L)
          disp.put(1.e-2/strainrate,0.00) # 1
          disp.put(2.e-2/strainrate,1.0e-2*L)
          disp.put(3.e-2/strainrate,0.00) # 2
          disp.put(5.e-2/strainrate,2.0e-2*L)
          disp.put(7.e-2/strainrate,0.00)
          disp.put(9.5e-2/strainrate,2.5e-2*L)
          disp.put(12.e-2/strainrate,0.00)
          disp.put(15.e-2/strainrate,3.0e-2*L)
          disp.put(18.e-2/strainrate,0.00)
          disp.put(21.5e-2/strainrate,3.5e-2*L)
          disp.put(24.e-2/strainrate,0.00)
          disp.put(28.e-2/strainrate,4.0e-2*L)
  elif(LoadCase[0] == 3):
      if(LoadCase[2] == 23):
          disp.put(0,0)
          disp.put(0.5e-2/strainrate,0.5e-2*L)
          disp.put(0.5e-2/strainrate,0.00) # 1
          disp.put(300.5e-2/strainrate,0.00) # 1 # Have to use force BC to get dwell time
          """
          disp.put(1.5e-2/strainrate,1.0e-2*L)
          disp.put(1.5e-2/strainrate,0.00) # 2
          disp.put(3.5e-2/strainrate,2.0e-2*L)
          disp.put(3.5e-2/strainrate,0.00)
          disp.put(6.e-2/strainrate,2.5e-2*L)
          disp.put(6.e-2/strainrate,0.00)
          disp.put(9.e-2/strainrate,3.0e-2*L)
          disp.put(9.e-2/strainrate,0.00)
          disp.put(12.5e-2/strainrate,3.5e-2*L)
          disp.put(12.5e-2/strainrate,0.00)
          disp.put(16.5e-2/strainrate,4.0e-2*L)
          """
  mysolver.displacementBC("Face",31,0,disp)         # face x = L   - Right Face moving

  mysolver.initialBC("Volume","Position",nfield,3,Tinitial)
  fT = LinearFunctionTime(0,Tinitial,ftime,Tinitial);
  if (MatPara[8][0] != True and MatPara[8][1] != True):
      mysolver.displacementBC("Volume",nfield,3,fT)

  mysolver.archivingAverageValue(IPField.P_XX)
  mysolver.archivingAverageValue(IPField.F_XX)
  mysolver.archivingAverageValue(IPField.TEMPERATURE)
  mysolver.archivingAverageValue(IPField.MECHANICAL_SOURCE)
  mysolver.archivingAverageValue(IPField.PLASTICSTRAIN)

  mysolver.internalPointBuildView("svm",IPField.SVM, 1, nstepArch)
  mysolver.internalPointBuildView("sig_xx",IPField.SIG_XX, 1, nstepArch)
  mysolver.internalPointBuildView("sig_yy",IPField.SIG_YY, 1, nstepArch)
  mysolver.internalPointBuildView("sig_zz",IPField.SIG_ZZ, 1, nstepArch)
  mysolver.internalPointBuildView("sig_xy",IPField.SIG_XY, 1, nstepArch)
  mysolver.internalPointBuildView("sig_yz",IPField.SIG_YZ, 1, nstepArch)
  mysolver.internalPointBuildView("sig_xz",IPField.SIG_XZ, 1, nstepArch)
  mysolver.internalPointBuildView("epl",IPField.PLASTICSTRAIN, 1, nstepArch)
  mysolver.internalPointBuildView("temperature",IPField.TEMPERATURE, 1, nstepArch)
  mysolver.internalPointBuildView("mechSrc",IPField.MECHANICAL_SOURCE, 1, 1)
  mysolver.internalPointBuildView("plastic possion ratio",IPField.PLASTIC_POISSON_RATIO,1,nstepArch)

  # ===Solving=======================================================================================
  mysolver.SetVerbosity(0)
  mysolver.solve()

  return

with open('LoadCases.dat','rb') as data1:
    print('Read LoadCase')
    LoadCase=pickle.load(data1)

with open('MatPara.dat','rb') as data2:
    print('Read MatPara')
    MatPara=pickle.load(data2)

print('LoadCase:', LoadCase)
print('MatPara:', MatPara)
RunCase(LoadCase,MatPara)
