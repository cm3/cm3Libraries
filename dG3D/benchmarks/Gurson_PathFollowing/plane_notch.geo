// Plane strain specimen tests with parametrable notch


// Geometry parameters
//====================================================
// Units
mm=1.0;
scale = 5./2./0.75;
// Specimen dimension
	// Thickness
T_specimen = scale*2*2.0*mm;
T_notchedpart = scale*2*0.75*mm;
	// Total lenght
Lenght_total = scale*(1.5+1.25*2+1.25*2)*mm;
//Lenght_total = scale*(1.5+1.25*2+0.75*2)*mm; // Original geometry
	// Notch radius
R_notch = scale*1.25*mm;

// Optionnal part
	// Lenght of middle section
Flag_reducedsection = 1;
Lenght_notched = scale*1.5*mm;

// Construction parameter
Ratio_square = 0.5;
XDeltaSquare = scale*0.75*mm;


// Mesh size
//====================================================
TransfiniteOn = 1;
n_center = 21; //81;	// # elements cross thickness
AspectRation_center = 1.;	// Aspect ratio for elements #100-#500
n_notch = 12;			// # elements along the notch curve
n_end = 3;

l100 = T_notchedpart/n_center;
l200 = T_specimen/n_center*2.0;
l300 = l200*2.0;
l500 = l100;




	

// Intermediar variables
//====================================================
a = 0.5*(T_specimen-T_notchedpart);
b = Sqrt( R_notch*R_notch - ((R_notch-a)*(R_notch-a)) );
	// Check for admissible values 
If (a > R_notch)
	Error('The notch radius is too small for both defined specimen diameter');
	Abort;
EndIf
//
If (Ratio_square > 1.)
	Error('The central square is too large');
	Abort;
EndIf
//
If (Flag_reducedsection == 1)
Else
	Lenght_notched = 0.0;
EndIf




// First half : X > 0
//====================================================
// Smaller notched section #100 (order from top side)
X1 = 0.5*Lenght_notched;
	// Internal points
T11 = 0.5*T_notchedpart*Ratio_square;
Point(101)={X1,T11,0.,l100};
Point(102)={X1,-T11,0.,l100};
Line(101)={101,102};
	// External points
T12 = 0.5*T_notchedpart;
Point(111)={X1,T12,0.,l100};
Point(112)={X1,-T12,0.,l100};
	// In-thickness links (from in to ext)
Line(121)={101,111};
Line(122)={102,112};
	// Outer center
R13 = R_notch+0.5*T_notchedpart; // Position of center maybe need to be updated
Point(131)={X1,R13,0,l100};
Point(132)={X1,-R13,0,l100};



// Larger notched section #200
// (order from top side)
X2 = X1+b;
X21 = X1+b+XDeltaSquare;
	// Internal points
T21 = 0.5*T_specimen*Ratio_square;
Point(201)={X21,T21,0.,l200};
Point(202)={X21,-T21,0.,l200};
Line(201)={201,202};
	// External points
T22 = 0.5*T_specimen;
Point(211)={X2,T22,0.,l200};
Point(212)={X2,-T22,0.,l200};
	// In-thickness links (from in to out)
Line(221)={201,211};
Line(222)={202,212};



// Links (between section #100 and #200)
	// Extern notch links 
Circle(151)={111,131,211};
Circle(152)={112,132,212};
	// Inner notch links (between section #100 and #200)
Line(161)={101,201};
Line(162)={102,202};
	// Plane inner surface linking both botch sections
Line Loop(171) = {101,162,-201,-161};
Surface(171) = {171};
Line Loop(172) = {121,151,-221,-161};
Surface(172) = {172};
Line Loop(173) = {122,152,-222,-162};
Surface(173) = {173};



// Final plane section #400
Point(401) = {Lenght_total*0.5,0.5*T_specimen*Ratio_square,0., l300};
Point(411) = {Lenght_total*0.5,0.5*T_specimen,0, l300};
Point(402) = {Lenght_total*0.5,-0.5*T_specimen*Ratio_square,0,l300};
Point(412) = {Lenght_total*0.5,-0.5*T_specimen, 0,l300};
Line(401)={401,402};
Line(421)={401,411};
Line(422)={402,412};



// Links between notched part and final section
// Extern links (between #200 and #300)
Line(251)={211,411};
Line(252)={212,412};
// Intern links (between #200 and #300)
Line(261)={201,401};
Line(262)={202,402};

// Plane inner surface linking both sections (between #200 and #300)
Line Loop(271) = {201,262,-401,-261};
Surface(271) = {271};
Line Loop(272) = {221,251,-421,-261};
Surface(272) = {272};
Line Loop(273) = {222,252,-422,-262};
Surface(273) = {273};



// Part in the notch part of sample (optionnal section #500)
If (Flag_reducedsection == 1)
	X5 = 0.;
	// Center
	// Internal pts
	T51 = 0.5*T_notchedpart*Ratio_square;
	Point(501)={X5,T51,0.,l500};
	Point(502)={X5,-T51,0.,l500};
	Line(501)={501,502};
	// External pts
	T52 = 0.5*T_notchedpart;
	Point(511)={X5,T52,0.,l500};
	Point(512)={X5,-T52,0.,l500};
	Line(521)={501,511};
	Line(522)={502,512};
	// Extern links
	Line(551)={511,111};
	Line(552)={512,112};
	// Intern links
	Line(561)={501,101};
	Line(562)={502,102};
	Line Loop(576) = {521,551,-(121),-(561)};
	Surface(572) = {576};
	Line Loop(577) = {522,552,-(122),-(562)};
	Surface(573) = {577};
	Line Loop(571) = {501,562,-101,-561};
	Surface(571) = {571};
EndIf


// Second half : X < 0
//====================================================
// Smaller notched section #100 (order from top side)
	// Internal points
Point(1101)={-X1,T11,0.,l100};
Point(1102)={-X1,-T11,0.,l100};
Line(1101)={1101,1102};
	// External points
Point(1111)={-X1,T12,0.,l100};
Point(1112)={-X1,-T12,0.,l100};
	// In-thickness links (from in to ext)
Line(1121)={1101,1111};
Line(1122)={1102,1112};
	// Outer center
R13 = R_notch+0.5*T_notchedpart; // Position of center maybe need to be updated
Point(1131)={-X1,R13,0,l100};
Point(1132)={-X1,-R13,0,l100};



// Larger notched section #200
// (order from top side)
	// Internal points
Point(1201)={-X21,T21,0.,l200};
Point(1202)={-X21,-T21,0.,l200};
Line(1201)={1201,1202};
	// External points
T22 = 0.5*T_specimen;
Point(1211)={-X2,T22,0.,l200};
Point(1212)={-X2,-T22,0.,l200};
	// In-thickness links (from in to out)
Line(1221)={1201,1211};
Line(1222)={1202,1212};



// Links (between section #100 and #200)
	// Extern notch links 
Circle(1151)={1111,1131,1211};
Circle(1152)={1112,1132,1212};
	// Inner notch links (between section #100 and #200)
Line(1161)={1101,1201};
Line(1162)={1102,1202};
	// Plane inner surface linking both botch sections
Line Loop(1171) = {1101,1162,-1201,-1161};
Surface(1171) = {1171};
Line Loop(1172) = {1121,1151,-1221,-1161};
Surface(1172) = {1172};
Line Loop(1173) = {1122,1152,-1222,-1162};
Surface(1173) = {1173};



// Final plane section #400
Point(1401) = {-Lenght_total*0.5,0.5*T_specimen*Ratio_square,0., l300};
Point(1411) = {-Lenght_total*0.5,0.5*T_specimen,0, l300};
Point(1402) = {-Lenght_total*0.5,-0.5*T_specimen*Ratio_square,0,l300};
Point(1412) = {-Lenght_total*0.5,-0.5*T_specimen, 0,l300};
Line(1401)={1401,1402};
Line(1421)={1401,1411};
Line(1422)={1402,1412};



// Links between notched part and final section
// Extern links (between #200 and #300)
Line(1251)={1211,1411};
Line(1252)={1212,1412};
// Intern links (between #200 and #300)
Line(1261)={1201,1401};
Line(1262)={1202,1402};

// Plane inner surface linking both sections (between #200 and #300)
Line Loop(1271) = {1201,1262,-1401,-1261};
Surface(1271) = {1271};
Line Loop(1272) = {1221,1251,-1421,-1261};
Surface(1272) = {1272};
Line Loop(1273) = {1222,1252,-1422,-1262};
Surface(1273) = {1273};



// Part in the notch part of sample (optionnal section #500)
If (Flag_reducedsection == 1)
	// Extern links
	Line(1551)={511,1111};
	Line(1552)={512,1112};
	// Intern links
	Line(1561)={501,1101};
	Line(1562)={502,1102};

	Line Loop(1576) = {521,1551,-(1121),-(1561)};
	Surface(1572) = {1576};
	Line Loop(1577) = {522,1552,-(1122),-(1562)};
	Surface(1573) = {1577};
	Line Loop(1571) = {501,1562,-1101,-1561};
	Surface(1571) = {1571};
EndIf


listVol[] = Surface "*";
Recombine Surface{listVol[]};






// Transfinite definition
//====================================================
// Section #100 et 500
If (TransfiniteOn == 1)
	Transfinite Line {101} = Round(n_center*(Ratio_square));
	Transfinite Line {121} = Round(n_center*(0.5-0.5*Ratio_square));
	Transfinite Line {122} = Round(n_center*(0.5-0.5*Ratio_square));
	If (Flag_reducedsection == 1)
		Transfinite Line {501} = Round(n_center*(Ratio_square));
		Transfinite Line {521} = Round(n_center*(0.5-0.5*Ratio_square));
		Transfinite Line {522} = Round(n_center*(0.5-0.5*Ratio_square));
		Transfinite Line {551} = Round(n_center/T_notchedpart*Lenght_notched*0.5*AspectRation_center) Using Progression 1.055;
		Transfinite Line {552} = Round(n_center/T_notchedpart*Lenght_notched*0.5*AspectRation_center) Using Progression 1.055;
		Transfinite Line {561} = Round(n_center/T_notchedpart*Lenght_notched*0.5*AspectRation_center) Using Progression 1.055;
		Transfinite Line {562} = Round(n_center/T_notchedpart*Lenght_notched*0.5*AspectRation_center) Using Progression 1.055;
		Transfinite Surface{571};
		Transfinite Surface{572};
		Transfinite Surface{573};
	EndIf

	// Section # 100 et 200
	Transfinite Line {161} = Round(n_notch) Using Progression 1.10;
	Transfinite Line {162} = Round(n_notch) Using Progression 1.10;
	Transfinite Line {151} = Round(n_notch) Using Progression 1.16;
	Transfinite Line {152} = Round(n_notch) Using Progression 1.16;
	Transfinite Line {201} = Round(n_center*(Ratio_square));
	Transfinite Line {221} = Round(n_center*(0.5-0.5*Ratio_square));
	Transfinite Line {222} = Round(n_center*(0.5-0.5*Ratio_square));
	Transfinite Surface{171};
	Transfinite Surface{172};
	Transfinite Surface{173};

	// Section # 200 et 400
	Transfinite Line {251} = n_end;
	Transfinite Line {252} = n_end;
	Transfinite Line {261} = n_end;
	Transfinite Line {262} = n_end;
	Transfinite Line {401} = Round(n_center*(Ratio_square));
	Transfinite Line {421} = Round(n_center*(0.5-0.5*Ratio_square));
	Transfinite Line {422} = Round(n_center*(0.5-0.5*Ratio_square));
	Transfinite Surface{271};
	Transfinite Surface{272};
	Transfinite Surface{273};


	Transfinite Line {1101} = Round(n_center*(Ratio_square));
	Transfinite Line {1121} = Round(n_center*(0.5-0.5*Ratio_square));
	Transfinite Line {1122} = Round(n_center*(0.5-0.5*Ratio_square));
	If (Flag_reducedsection == 1)
		Transfinite Line {1551} = Round(n_center/T_notchedpart*Lenght_notched*0.5*AspectRation_center) Using Progression 1.055;
		Transfinite Line {1552} = Round(n_center/T_notchedpart*Lenght_notched*0.5*AspectRation_center) Using Progression 1.055;
		Transfinite Line {1561} = Round(n_center/T_notchedpart*Lenght_notched*0.5*AspectRation_center) Using Progression 1.055;
		Transfinite Line {1562} = Round(n_center/T_notchedpart*Lenght_notched*0.5*AspectRation_center) Using Progression 1.055;
		Transfinite Surface{1571};
		Transfinite Surface{1572};
		Transfinite Surface{1573};
	EndIf

	// Section # 100 et 200
	Transfinite Line {1161} = Round(n_notch) Using Progression 1.10;
	Transfinite Line {1162} = Round(n_notch) Using Progression 1.10;
	Transfinite Line {1151} = Round(n_notch) Using Progression 1.16;
	Transfinite Line {1152} = Round(n_notch) Using Progression 1.16;
	Transfinite Line {1201} = Round(n_center*(Ratio_square));
	Transfinite Line {1221} = Round(n_center*(0.5-0.5*Ratio_square));
	Transfinite Line {1222} = Round(n_center*(0.5-0.5*Ratio_square));
	Transfinite Surface{1171};
	Transfinite Surface{1172};
	Transfinite Surface{1173};

	// Section # 200 et 400
	Transfinite Line {1251} = n_end;
	Transfinite Line {1252} = n_end;
	Transfinite Line {1261} = n_end;
	Transfinite Line {1262} = n_end;
	Transfinite Line {1401} = Round(n_center*(Ratio_square));
	Transfinite Line {1421} = Round(n_center*(0.5-0.5*Ratio_square));
	Transfinite Line {1422} = Round(n_center*(0.5-0.5*Ratio_square));
	Transfinite Surface{1271};
	Transfinite Surface{1272};
	Transfinite Surface{1273};
Else

	Transfinite Line {201} = Ceil(T_specimen/l200*(Ratio_square));
	Transfinite Line {221} = Ceil(T_specimen/l200*(0.5-0.5*Ratio_square));
	Transfinite Line {222} = Ceil(T_specimen/l200*(0.5-0.5*Ratio_square));
	Transfinite Line {401} = Ceil(T_specimen/l200*(Ratio_square));
	Transfinite Line {421} = Ceil(T_specimen/l200*(0.5-0.5*Ratio_square));
	Transfinite Line {422} = Ceil(T_specimen/l200*(0.5-0.5*Ratio_square));
	Transfinite Line {251} = n_end;
	Transfinite Line {252} = n_end;
	Transfinite Line {261} = n_end;
	Transfinite Line {262} = n_end;
	Transfinite Surface{271};
	Transfinite Surface{272};
	Transfinite Surface{273};

EndIf




// Physical definiton
//====================================================



	// Symetry plane perimeter (section #500 or #100)
If (Flag_reducedsection == 1)
	Physical Point(100) = {511,512}; 
Else
	Physical Point(100) = {111,112};
EndIf
Physical Line(200) = {501};
Point(103) = {0.,0.,0.,l500};
Physical Point(300) = {103};
	// Extremity 1 (z>0) (section #400)
Physical Line(400) = {401,421,422};
		// Extremity point
Physical Point(401) = {411};


	// Extremity 2 (z<0) (section #600)
Physical Line(1400) = {1401,1421,1422};
		// Extremity point
Physical Point(1401) = {1411};


	// Whole Volume
listVol[] = Surface "*";
Recombine Surface{listVol[]};
Physical Surface(700) = {listVol[]};




// Old code
//Physical Line(401) = {421,423}; // vertical // y
//Physical Line(402) = {422,424}; // horizontal // x
//Printf("test = %g", out[3]);
//Abort;

/*
// Links between notched part and final section
If (XExtrusionSize == 0.)
	// Extern links (between #200 and #300)
	Line(251)={211,411};
	Line(252)={212,412};
	// Intern links (between #200 and #300)
	Line(261)={201,401};
	Line(262)={202,402};

	// Plane inner surface linking both sections (between #200 and #300)
	Line Loop(271) = {201,262,-401,-261};
	Surface(271) = {271};
	Line Loop(272) = {221,251,-421,-261};
	Surface(272) = {272};
	Line Loop(273) = {222,252,-422,-262};
	Surface(273) = {273};
Else
// Inner plane section (not notched) #300 (optionnal)
	X3 = Lenght_total*0.5 - XExtrusionSize;
	// Internal square
	T31 = 0.5*T_specimen*Ratio_square;
	Point(301)={X3,T31,0.,l300};
	Point(302)={X3,-T31,0.,l300};
	Line(301)={301,302};
	// External circle
	T32 = 0.5*T_specimen;
	Point(311)={X3,T32,0.,l300};
	Point(312)={X3,-T32,0.,l300};
	// In-thickness links (from in to ext)
	Line(321)={301,311};
	Line(322)={302,312};
	// Extern links (between #200 and #300)
	Line(251)={211,311};
	Line(252)={212,312};
	// Intern links (between #200 and #300)
	Line(261)={201,301};
	Line(262)={202,302};

	// Plane inner surface linking both sections (between #200 and #300)
	Line Loop(271) = {201,262,-301,-261};
	Surface(271) = {271};
	Line Loop(272) = {221,251,-321,-261};
	Surface(272) = {272};
	Line Loop(273) = {222,252,-322,-262};
	Surface(273) = {273};

	// Extrusion

	out_400[] = Extrude {Lenght_total*0.5-X3,0,0} {
  	Line{301,321,322}; Layers{ {nbl1,nbl2}, {ncut,1.} }; Recombine;
	};

	out_400[] = Extrude {Lenght_total*0.5-X3,0,0} {
  	Line{301,321,322}; Layers{ 1 }; Recombine;
	};


EndIf





// Symmetry part construction
	// Control points and lines
Point(601) = {-Lenght_total*0.5,0.5*T_specimen*Ratio_square, 0 ,l300};
Point(611) = {-Lenght_total*0.5,0.5*T_specimen, 0 ,l300};
Point(602) = {-Lenght_total*0.5,-0.5*T_specimen*Ratio_square, 0 ,l300};
Point(612) = {-Lenght_total*0.5,-0.5*T_specimen, 0 ,l300};
Line(601)={601,602};
Line(621)={601,611};
Line(622)={602,612};
	// Between #100 and #200
Symmetry{ 1., 0., 0., 0.} { Duplicata{ Surface{171,172,173}; }}
	// Between #300 and #400
out_sym300[] = Symmetry{ 1., 0., 0., 0.} { Duplicata{ Line{301,321,322}; }};

out_sym500[] = Extrude {-(Lenght_total*0.5-X3),0,0} {
  Line{out_sym300[0],out_sym300[1],out_sym300[2]}; Layers{ {nbl1,nbl2}, {ncut,1.} }; Recombine;
};

out_sym500[] = Extrude {-(Lenght_total*0.5-X3),0,0} {
  Line{out_sym300[0],out_sym300[1],out_sym300[2]}; Layers{ 1 }; Recombine;
};
	// Between #200 and #300
Symmetry{ 1., 0., 0., 0.} { Duplicata{ Surface{271,272,273}; }}
	// Between #100 and #500
If (Flag_reducedsection == 1)
	Symmetry{ 1., 0., 0., 0.} { Duplicata{ Surface{571,572,573}; }}
	
EndIf








*/




