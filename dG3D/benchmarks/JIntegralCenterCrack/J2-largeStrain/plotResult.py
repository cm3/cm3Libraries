import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

plt.figure()
u = pd.read_csv("NodalDisplacementPhysical115Num8comp1.csv",sep=";",header=None)
f = pd.read_csv("force111comp1.csv",sep=";",header=None)

plt.plot([0]+u.values[:,1].tolist(),[0]+(-1*f.values[:,1]*2).tolist(),"o-")
plt.xlabel("vertical displacement")
plt.ylabel("Force per thickness")
plt.savefig("force-disp.png")

def SIF(sigmaInf, a, W):
    aW = a/W
    f = (1-0.025*aW*aW + 0.06*aW**4)/np.sqrt(np.cos(np.pi*aW/2))
    return sigmaInf*np.sqrt(np.pi*a)*f
    

# SIF handbook with elastoplasity
unit = 1e-2;
h = 7.62*unit;
b = 0.5*3.8*unit;
a = 0.7*unit;
c = b-a
h1=2.51
sy0 = 350e6
n = 5.
alpha = 1.
young = 210e9 #Pa
nu    = 0.3
eps0 = sy0/young
P0 = 4*c*sy0/np.sqrt(3)


P = -2*f.values[:,1]
print(P)
phi = 1/(1+(P/P0)**2)
syy = P/(2*b)

Ke = P*0.
for i in range(Ke.shape[0]):
    Ke[i] = SIF(syy[i], a, b)
    ite = 0
    while ite < 10:
        ite += 1
        ry = (1/(6*np.pi))*((n-1)/(n+1))*(Ke[i]/sy0)**2
        ae = a + phi[i]*ry
        Ke[i] = SIF(syy[i], ae, b)

Je = Ke**2*(1-nu**2)/(young)
Jp = alpha*sy0*eps0*a*(c/b)*h1*(P/P0)**(n+1)


plt.figure()
plt.subplot(1, 2, 1)
plt.plot(P,(Je),'s-',label="Je, handbook")
plt.plot(P,(Jp),'o-',label="Jp, handbook")
plt.plot(P,(Je+Jp),'v-',label="Je+Jp, handbook")

for case in ["Contour0","Contour1","Contour2","Contour3","Contour4","Contour5","Contour6","Contour7"]:
    data = pd.read_csv("J"+case+".csv",sep=";")
    plt.subplot(1, 2, 1)
    plt.plot(P,data.values[:,2],"--",label="J0, "+case)
    plt.subplot(1, 2, 2)
    plt.plot(P,data.values[:,3],".--",label="J1, "+case)

plt.subplot(1, 2, 1)
plt.xlabel("P")
plt.ylabel("J")
plt.legend()
plt.subplot(1, 2, 2)
plt.xlabel("P")
plt.ylabel("J")
plt.legend()
plt.savefig("J.png")

plt.show()
