#coding-Utf-8-*-
from gmshpy import *
from dG3Dpy import*
import numpy as np
import os
import pandas as pd
#script to launch beam problem with a python script


lawnum1 = 1 # unique number of law
rho   = 7850e-9
young = 210e9 #MPa
nu    = 0.3 

linearElastic=True
if linearElastic:
    law1 = dG3DLinearElasticMaterialLaw(lawnum1,rho,young,nu)
else:
    sy0   = 350.e6 #MPa
    N = 0.2
    p0 = 0.0
    h = young/sy0
    harden1 = SwiftJ2IsotropicHardening(lawnum1,sy0,h,N,p0)
    law1   = J2LinearDG3DMaterialLaw(lawnum1,rho,young,nu,harden1)

# geometry
meshfile="centerCrack.msh" # name of mesh file

# solver
sol = 2  # Gmm=0 (default) Taucs=1 PETsc=2
soltype = 1 # StaticLinear=0 (default) StaticNonLinear=1
nstep = 1   # number of step (used only if soltype=1)
ftime =1.   # Final time (used only if soltype=1)
tol=1.e-6 # relative tolerance for NR scheme (used only if soltype=1)
nstepArch=1 # Number of step between 2 archiving (used only if soltype=1)

# creation of ElasticField
fullDG = False;
myfield1 = dG3DDomain(1000,116,0,lawnum1,fullDG,3)


# creation of Solver
mysolver = nonLinearMechSolver(1000)
mysolver.loadModel(meshfile)
mysolver.addDomain(myfield1)
mysolver.addMaterialLaw(law1)

mysolver.Scheme(soltype)
mysolver.Solver(sol)
mysolver.snlData(nstep,ftime,tol)
mysolver.stepBetweenArchiving(nstepArch)
mysolver.options("-ksp_type preonly -pc_type lu -pc_factor_mat_solver_type mumps")

# BC
mysolver.displacementBC("Face",97,2,0.)
mysolver.displacementBC("Face",129,1,0.)
mysolver.displacementBC("Face",130,0,0.)
mysolver.constraintBC("Face",131,1)
mysolver.forceBC("Face",131,1,5.*103.6e6)


mysolver.internalPointBuildView("svm",IPField.SVM, 1, 1)
mysolver.internalPointBuildView("sig_xx",IPField.SIG_XX, 1, 1)
mysolver.internalPointBuildView("sig_yy",IPField.SIG_YY, 1, 1)
mysolver.internalPointBuildView("sig_zz",IPField.SIG_ZZ, 1, 1)
mysolver.internalPointBuildView("sig_xy",IPField.SIG_XY, 1, 1)
mysolver.internalPointBuildView("sig_yz",IPField.SIG_YZ, 1, 1)
mysolver.internalPointBuildView("sig_xz",IPField.SIG_XZ, 1, 1)
mysolver.internalPointBuildView("epl",IPField.PLASTICSTRAIN, 1, 1)
mysolver.internalPointBuildView("U_NORM",IPField.U_NORM, 1, 1)

mysolver.internalPointBuildViewIncrement("epl",IPField.PLASTICSTRAIN, 1, 1)
mysolver.internalPointBuildViewIncrement("U_NORM",IPField.U_NORM, 1, 1)

mysolver.internalPointBuildViewRate("epl",IPField.PLASTICSTRAIN, 1, 1)
mysolver.internalPointBuildViewRate("U_NORM",IPField.U_NORM, 1, 1)


mysolver.archivingForceOnPhysicalGroup("Face", 129, 1)
mysolver.archivingNodeDisplacement(115,1)


#J integral
# case list of faces, list of inner physicals; list of outer physicals
cases=[
       [[118], [123], [124]],
       [[119], [124], [125]],
       [[120], [125], [126]],
       [[121], [126], [127]],
       [[122], [127], [128]],
      ]
numLevels=11
zlevel=np.linspace(0,10e-2,numLevels)
      
allComputeJs = []
for case in cases:
    vol = case[0]
    inner = case[1]
    outer = case[2]
    for j in range(numLevels-1):
        name = f"Contour{cases.index(case)}Level{j}"
        computeJ = computeJIntegral(name)
        for phys in vol:
            computeJ.setIntegratedDomain(3,phys)
        for phys in inner:
            computeJ.setInnerBoundary(2,phys)   
        for phys in  outer:
            computeJ.setOuterBoundary(2,phys)
        filterZ=computeJElementFilterLayer(2,zlevel[j]-1e-4,zlevel[j+1]+1e-4)
        computeJ.setElementFilter(filterZ)
        allComputeJs.append(computeJ)
        

for compJ in allComputeJs:
    mysolver.computeJIntegralByDomainIntegration(compJ)
    
mysolver.solve()

