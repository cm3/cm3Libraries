lsca = 0.5e-3;
unit = 1e-2; //cm

h = 7.62*unit;
b = 0.5*3.8*unit;
a = 0.7*unit;
c1 = 0.1*a;
h1 = 0.1*h;
tol=0.00001*h;

Point(1)={0,0,0,lsca};
Point(2)={c1,0,0,lsca};
Point(4)={-c1,tol,0,lsca};
Point(6)={-a,tol,0,lsca};
Point(7)={b-a,0,0,lsca};
Point(8)={c1,h1,0,lsca};
Point(10)={-c1,h1,0,lsca};
Point(12)={-a,h,0,lsca*100};
Point(13)={b-a,h,0,lsca*100};

//+
Line(1) = {4, 10};
//+
Line(2) = {10, 8};
//+
Line(3) = {8, 2};
//+
Dilate {{0, 0, 0}, {1.5, 1.2, 0}} {
  Duplicata { Curve{1}; Curve{2}; Curve{3}; }
}
//+
Dilate {{0, 0, 0}, {1.5, 1.2, 0}} {
  Duplicata { Curve{5}; Curve{4}; Curve{6}; }
}
//+
Dilate {{0, 0, 0}, {1.5, 1.2, 0}} {
  Duplicata { Curve{8}; Curve{7}; Curve{9}; }
}
//+
Dilate {{0, 0, 0}, {1.5, 1.2, 0}} {
  Duplicata { Curve{10}; Curve{11}; Curve{12}; }
}
//+
Dilate {{0, 0, 0}, {1.5, 1.2, 0}} {
  Duplicata { Curve{13}; Curve{14}; Curve{15}; }
}
//+
Line(19) = {1, 2};
//+
Line(20) = {2, 23};
//+
Line(21) = {23, 33};
//+
Line(22) = {33, 43};
//+
Line(23) = {43, 53};
//+
Line(24) = {53, 63};
//+
Line(25) = {63, 7};
//+
Line(26) = {7, 13};
//+
Line(27) = {13, 12};
//+
Line(28) = {12, 6};
//+
Line(29) = {6, 54};
//+
Line(30) = {54, 44};
//+
Line(31) = {44, 34};
//+
Line(32) = {28, 34};
//+
Line(33) = {28, 14};
//+
Line(34) = {14, 4};
//+
Line(35) = {4, 1};
//+
Curve Loop(1) = {1, 2, 3, -19, -35};
//+
Plane Surface(1) = {1};
//+
Curve Loop(2) = {5, 6, -20, -3, -2, -1, -34, 4};
//+
Plane Surface(2) = {2};
//+
Curve Loop(3) = {5, 6, 21, -9, -7, -8, 33, 4};
//+
Plane Surface(3) = {3};
//+
Curve Loop(4) = {7, 9, 22, -12, -11, -10, -32, 8};
//+
Plane Surface(4) = {4};
//+
Curve Loop(5) = {11, 12, 23, -15, -14, -13, 31, 10};
//+
Plane Surface(5) = {5};
//+
Curve Loop(6) = {14, 15, 24, -18, -17, -16, 30, 13};
//+
Plane Surface(6) = {6};
//+
Curve Loop(7) = {29, 16, 17, 18, 25, 26, 27, 28};
//+
Plane Surface(7) = {7};
//+
Symmetry {0, 1, 0, 0} {
  Duplicata { Surface{7}; Surface{6}; Surface{5}; Surface{4}; Surface{3}; Surface{2}; Surface{1}; }
}
Physical Surface(97) = {1, 2, 3, 4, 5, 6, 7,36,45,54,63,72,81,90};
//+
Physical Surface(98) = {1, 90};
//+
Physical Surface(99) = {2, 81};
//+
Physical Surface(100) = {3, 72};
//+
Physical Surface(101) = {4, 63};
//+
Physical Surface(102) = {5, 54};
//+
Physical Surface(103) = {6, 45};
//+
Physical Surface(104) = {36, 7};
//+
Physical Curve(105) = {1, 2, 3, 85, 87, 86};
//+
Physical Curve(106) = {5, 4, 6, 74, 73, 80};
//+
Physical Curve(107) = {7, 9, 65, 64, 8, 71};
//+
Physical Curve(108) = {10, 11, 12, 56, 55, 62};
//+
Physical Curve(109) = {14, 15, 47, 46, 53, 13};
//+
Physical Curve(110) = {17, 16, 18, 40, 39, 38};
//+
Physical Curve(111) = {43};
//+
Physical Curve(112) = {42, 26};
//+
Physical Curve(113) = {27};
//+
Physical Curve(114) = {28, 44};

Physical Point(115) = {12};
//+
Extrude {0, 0, 10*unit} {
  Surface{7}; Surface{6}; Surface{5}; Surface{4}; Surface{3}; Surface{2}; Surface{1}; Surface{90}; Surface{81}; Surface{72}; Surface{63}; Surface{54}; Surface{45}; Surface{36}; Layers {20}; Recombine;
}
//+
Physical Volume(116) = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14};
//+
Physical Volume(117) = {7, 8};
//+
Physical Volume(118) = {6, 9};
//+
Physical Volume(119) = {10, 5};
//+
Physical Volume(120) = {4, 11};
//+
Physical Volume(121) = {3, 12};
//+
Physical Volume(122) = {2, 13};
//+
Physical Surface(123) = {350, 354, 358, 412, 404, 408};
//+
Physical Surface(124) = {300, 296, 324, 462, 434, 438};
//+
Physical Surface(125) = {258, 254, 282, 496, 492, 488};
//+
Physical Surface(126) = {216, 212, 240, 538, 534, 530};
//+
Physical Surface(127) = {576, 572, 174, 170, 198, 580};
//+
Physical Surface(128) = {140, 136, 132, 622, 618, 614};
//+
Physical Surface(129) = {668};
//+
Physical Surface(130) = {672, 156};
//+
Physical Surface(131) = {152};
