import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

plt.figure()
u = pd.read_csv("NodalDisplacementPhysical115Num8comp1.csv",sep=";",header=None)
f = pd.read_csv("force129comp1.csv",sep=";",header=None)

plt.plot([0]+u.values[:,1].tolist(),[0]+(-1*f.values[:,1]*2).tolist(),"o-")
plt.xlabel("vertical displacement")
plt.ylabel("Force per thickness")
plt.savefig("force-disp.png")

def SIF(sigmaInf, a, W):
    aW = a/W
    f = (1-0.025*aW*aW + 0.06*aW**4)/np.sqrt(np.cos(np.pi*aW/2))
    return sigmaInf*np.sqrt(np.pi*a)*f
    

# SIF handbook with elastoplasity
unit = 1e-2;
h = 7.62*unit;
b = 0.5*3.8*unit;
a = 0.7*unit;
t = 10*unit;
young = 210e9 #Pa
nu    = 0.3

plt.figure()
syy = -1*f.values[:,1]/(b*t)
Ka = np.array([SIF(s, a, b) for s in syy])
Ja = Ka**2*(1-nu**2)/(young) 
plt.subplot(1, 2, 1)
numContours=4
numLevels=10
thicknessLayer=t/10
plt.plot(np.ones(numLevels)*Ja,"o-",label="handbook")

for c in range(numContours):
    valuesI = []
    valuesII = []
    for l in range(numLevels):
        case =f"Contour{c}Level{l}"
        data = pd.read_csv("J"+case+".csv",sep=";")
        valuesI.append(data.values[0,2]/thicknessLayer)
        valuesII.append(data.values[0,3]/thicknessLayer)
    plt.subplot(1, 2, 1)
    plt.plot(valuesI,".--",label="J0, "+f"Contour{c}")
    plt.subplot(1, 2, 2)
    plt.plot(valuesII,".--",label="J1, "+f"Contour{c}")

plt.subplot(1, 2, 1)
plt.xlabel(r"$\sigma_{\infty}$")
plt.ylabel(r"J")
plt.legend()
plt.subplot(1, 2, 2)
plt.xlabel(r"$\sigma_{\infty}$")
plt.ylabel(r"J")
plt.legend()

plt.savefig("J.png")
plt.show()
