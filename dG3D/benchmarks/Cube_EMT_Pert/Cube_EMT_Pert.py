#coding-Utf-8-*-
from gmshpy import *


#from dG3DpyDebug import*
from dG3Dpy import*

#script to launch ElecMag SMP problem with a python script

import math

Irms = 0. # Ampere
freq = 0. # Hz
nTurnsCoil = 0
coilLx = coilLy = coilLz = 0. # m
coilW = 0. # m

# material law: SMP
lawnum= 1 
rho   = 1020. # material density
tinitial = 273.+25.
G=156.e6 # Shear modulus
Mu=0.35 # Poisson ratio
alpha = beta=gamma=0. # parameter of anisotropy
cp=1710.*rho # specific heat capacity 
Kx=Ky=Kz=0.5 # Thermal conductivity
lx=ly=lz=35. # electrical conductivity (S/m) 
seebeck=21.e-6
alphaTherm=0.0 #13.e-5 # thermal dilatation
v0=0. # initial electric potential
E_matrix = 2.0 * G * (1.0 + Mu)
mag_mu_0 = 4.e-7 * math.pi # vacuum magnetic permeability
mag_r_smp = 5.0 # relative mag permeability of SMP
mag_mu_x = mag_mu_y = mag_mu_z = mag_r_smp * mag_mu_0 # mag permeability smp 
magpot0_x = magpot0_y = magpot0_z = 0.0 # initial magnetic potential

useFluxT=True

# geometry
geofile="cube.geo" # name of mesh file
meshfile="cube.msh" # name of mesh file

# solver
sol = 2  # Gmm=0 (default) Taucs=1 PETsc=2
soltype = 1 # StaticLinear=0 (default) StaticNonLinear=1
nstep = 1 # number of step (used only if soltype=1)
ftime = 1. # Final time (used only if soltype=1)
tol=1.e-3   # relative tolerance for NR scheme (used only if soltype=1)
nstepArch=1 # Number of step between 2 archiving (used only if soltype=1)
fullDg = 0 #O = CG, 1 = DG
beta1 = 1000.
eqRatio =1.e8

#  compute solution and BC (given directly to the solver
# creation of law
# material law for smp
lawsmp = LinearElecMagTherMechDG3DMaterialLaw(lawnum,rho,E_matrix,E_matrix,E_matrix,Mu,Mu,Mu,G,G,G,alpha,beta,gamma,
tinitial,Kx,Ky,Kz,alphaTherm,alphaTherm,alphaTherm,lx,ly,lz,seebeck,cp,v0, mag_mu_x, mag_mu_y, mag_mu_z, magpot0_x,
magpot0_y, magpot0_z, Irms, freq, nTurnsCoil, coilLx, coilLy, coilLz, coilW,useFluxT)

pertLawSmp = dG3DMaterialLawWithTangentByPerturbation(lawsmp,1.e-8)

# creation of ElasticField
SMPfield = 29 # number of the field (physical number of SMP region)
SurfSMPStart = 30 # delta voltage surface
SurfSMPEnd = 31 # ground voltage surface

space1 = 0 # function space (Lagrange=0)

SMP_field = ElecMagTherMechDG3DDomain(10,SMPfield,space1,lawnum,fullDg,eqRatio,3)

SMP_field.setConstitutiveExtraDofDiffusionEqRatio(eqRatio)
SMP_field.setConstitutiveCurlEqRatio(eqRatio)
SMP_field.stabilityParameters(beta1)

thermalSource=True
mecaSource=False

SMP_field.setConstitutiveExtraDofDiffusionAccountSource(thermalSource,mecaSource)

#SMP_field.matrixByPerturbation(1,1,1,1.e-3)

# creation of Solver
mysolver = nonLinearMechSolver(1000)
mysolver.createModel(geofile,meshfile,3,1)
#mysolver.loadModel(meshfile)
mysolver.addDomain(SMP_field)
mysolver.addMaterialLaw(pertLawSmp)
mysolver.Scheme(soltype)
mysolver.Solver(sol)
mysolver.snlData(nstep,ftime,tol,1.e-9)#forsmp
mysolver.options("-ksp_type preonly")
mysolver.options("-pc_type lu")

#mysolver.snlData(nstep,ftime,tol,1.e-19)
mysolver.stepBetweenArchiving(nstepArch)
mysolver.snlManageTimeStep(25, 3, 2, 10)

# BC
##cyclicFunctionDisp=cycleFunctionTime(0., 0., ftime/5., -0.0001, ftime , -0.0001);
#mysolver.displacementBC("Face",3,0,0.)
#mysolver.displacementBC("Face",3,1,0.)
#mysolver.displacementBC("Face",3,2,0.)
#mysolver.displacementBC("Face",8,0,0.)
#mysolver.displacementBC("Face",8,1,0.)
#mysolver.displacementBC("Face",8,2,0.)
mysolver.displacementBC("Volume",SMPfield,0,0.0)
mysolver.displacementBC("Volume",SMPfield,1,0.0)
mysolver.displacementBC("Volume",SMPfield,2,0.0)

#thermal BC
cyclicFunctionTemp1=cycleFunctionTime(0., tinitial,ftime,tinitial);
mysolver.initialBC("Volume","Position",SMPfield,3,tinitial)
#mysolver.displacementBC("Face",8,3,cyclicFunctionTemp1)
#mysolver.displacementBC("Face",SurfSMPStart,3,cyclicFunctionTemp1)
#mysolver.displacementBC("Face",SurfSMPEnd,3,cyclicFunctionTemp1)
#mysolver.displacementBC("Volume",SMPfield,3,cyclicFunctionTemp1)

#electrical BC
#mysolver.initialBC("Volume","Position",SMPfield,4,0.)
#mysolver.displacementBC("Volume",Indfield,4,0.0)
cyclicFunctionvolt1=cycleFunctionTime(0., 0.,  ftime,1.e1);
mysolver.displacementBC("Face",SurfSMPStart,4,cyclicFunctionvolt1)
mysolver.displacementBC("Face",SurfSMPEnd,4,0.)


#magentic
mysolver.curlDisplacementBC("Face",SurfSMPStart,5,0.0) # comp may also be 5
mysolver.curlDisplacementBC("Face",SurfSMPEnd,5,0.0) # comp may also be 5
#Gauging the edges using tree-cotree method
PhysicalCurves = "" 		# input required as a string of comma separated ints
PhysicalSurfaces = "" # input required as a string of comma separated ints
PhysicalVolumes = "29" 	# input required as a string of comma separated ints
OutputPhysical = 55 		# input required as a int
mysolver.createTreeForBC(PhysicalCurves,PhysicalSurfaces,PhysicalVolumes,OutputPhysical)
mysolver.curlDisplacementBC("Edge",OutputPhysical,5,0.0)

#mysolver.internalPointBuildView("svm",IPField.SVM, 1, 1)
#mysolver.internalPointBuildView("sig_xx",IPField.SIG_XX, 1, 1)
#mysolver.internalPointBuildView("sig_yy",IPField.SIG_YY, 1, 1)
#mysolver.internalPointBuildView("sig_zz",IPField.SIG_ZZ, 1, 1)
#mysolver.internalPointBuildView("sig_xy",IPField.SIG_XY, 1, 1)
#mysolver.internalPointBuildView("sig_yz",IPField.SIG_YZ, 1, 1)
#mysolver.internalPointBuildView("sig_xz",IPField.SIG_XZ, 1, 1)
mysolver.internalPointBuildView("temperature",IPField.TEMPERATURE, 1, 1)
#mysolver.internalPointBuildView("w_T",IPField.THERMALSOURCE, 1, 1)
mysolver.internalPointBuildView("jy_x",IPField.THERMALFLUX_X, 1, 1)
mysolver.internalPointBuildView("jy_y",IPField.THERMALFLUX_Y, 1, 1)
mysolver.internalPointBuildView("jy_z",IPField.THERMALFLUX_Z, 1, 1)
mysolver.internalPointBuildView("voltage",IPField.VOLTAGE, 1, 1)
mysolver.internalPointBuildView("je_x",IPField.ELECTRICALFLUX_X, 1, 1)
mysolver.internalPointBuildView("je_y",IPField.ELECTRICALFLUX_Y, 1, 1)
mysolver.internalPointBuildView("je_z",IPField.ELECTRICALFLUX_Z, 1, 1)
#mysolver.internalPointBuildView("ax",IPField.MAGNETICVECTORPOTENTIAL_X, 1, 1)
#mysolver.internalPointBuildView("ay",IPField.MAGNETICVECTORPOTENTIAL_Y, 1, 1)
#mysolver.internalPointBuildView("az",IPField.MAGNETICVECTORPOTENTIAL_Z, 1, 1)
mysolver.internalPointBuildView("bx",IPField.MAGNETICINDUCTION_X, 1, 1)
mysolver.internalPointBuildView("by",IPField.MAGNETICINDUCTION_Y, 1, 1)
mysolver.internalPointBuildView("bz",IPField.MAGNETICINDUCTION_Z, 1, 1)
#mysolver.internalPointBuildView("js0_x",IPField.INDUCTORSOURCEVECTORFIELD_X, 1, 1)
#mysolver.internalPointBuildView("js0_y",IPField.INDUCTORSOURCEVECTORFIELD_Y, 1, 1)
#mysolver.internalPointBuildView("js0_z",IPField.INDUCTORSOURCEVECTORFIELD_Z, 1, 1)
mysolver.internalPointBuildView("wAV_x",IPField.EMSOURCEVECTORFIELD_X, 1, 1)
mysolver.internalPointBuildView("wAV_y",IPField.EMSOURCEVECTORFIELD_Y, 1, 1)
mysolver.internalPointBuildView("wAV_z",IPField.EMSOURCEVECTORFIELD_Z, 1, 1)

#mysolver.archivingIPOnPhysicalGroup("Volume",Mfield, IPField.SIG_ZZ,IPField.MEAN_VALUE,nstepArch);
#mysolver.archivingIPOnPhysicalGroup("Volume",Mfield, IPField.STRAIN_ZZ,IPField.MEAN_VALUE,nstepArch);
#mysolver.archivingIPOnPhysicalGroup("Volume",Mfield, IPField.TEMPERATURE,IPField.MEAN_VALUE,nstepArch);
#mysolver.archivingIPOnPhysicalGroup("Volume",Mfield, IPField.VOLTAGE,IPField.MEAN_VALUE,nstepArch);
#mysolver.archivingIPOnPhysicalGroup("Volume",Ffield, IPField.SIG_ZZ,IPField.MEAN_VALUE,nstepArch);
#mysolver.archivingIPOnPhysicalGroup("Volume",Ffield, IPField.STRAIN_ZZ,IPField.MEAN_VALUE,nstepArch);
#mysolver.archivingIPOnPhysicalGroup("Volume",Ffield, IPField.TEMPERATURE,IPField.MEAN_VALUE,nstepArch);
#mysolver.archivingIPOnPhysicalGroup("Volume",Indfield, IPField.VOLTAGE,IPField.MEAN_VALUE,nstepArch);
#mysolver.archivingIPOnPhysicalGroup("Volume",Vacfield, IPField.VOLTAGE,IPField.MEAN_VALUE,nstepArch);

mysolver.archivingForceOnPhysicalGroup("Face", SurfSMPStart, 3, 1);
mysolver.archivingForceOnPhysicalGroup("Face", SurfSMPStart, 4, 1);
mysolver.archivingForceOnPhysicalGroup("Face", SurfSMPEnd, 4, 1);

#mysolver.archivingNodeDisplacement(88,4,1);

#mysolver.archivingNodeIP(88, IPField.THERMALFLUX_X,IPField.MEAN_VALUE,1);

#mysolver.archivingNodeIP(88, IPField.ELECTRICALFLUX_X,IPField.MEAN_VALUE,1);

mysolver.solve()

check = TestCheck()
check.equal(0.000000e+00,mysolver.getArchivedForceOnPhysicalGroup("Face", SurfSMPStart, 3),1.e-5)
check.equal(-3.499807e+07,mysolver.getArchivedForceOnPhysicalGroup("Face", SurfSMPStart, 4),1.e-5)
check.equal(3.499807e+07,mysolver.getArchivedForceOnPhysicalGroup("Face", SurfSMPEnd, 4),1.e-5)


