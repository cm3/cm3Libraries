#coding-Utf-8-*-
from gmshpy import *
from dG3Dpy import*

#script to launch PBC problem with a python script

#DEFINE MICRO PROBLEM

# micro-material law
lawnum = 11 # uniquenumber of law
K = 76.E3 	# Bulk mudulus
mu =26.E3	  # Shear mudulus
rho = 2700. # Bulk mass
sy0 = 300
h = 0.01*sy0
E = 9.*K*mu/(3.*K+mu)
nu= (3.*K -2.*mu)/2./(3.*K+mu)

# micro-geometry
micromeshfile="micro.msh" # name of mesh file

# creation of material law
law1 = dG3DLinearElasticMaterialLaw(lawnum,rho,E,nu)
#law1=  J2LinearDG3DMaterialLaw(lawnum,rho,E,nu,sy0,h)

# creation of  micro part Domain
nfield = 5 # number of the field (physical number of entity)
dim =3
myfield1 = dG3DDomain(10,nfield,0,lawnum,0,dim)

microBC = nonLinearPeriodicBC(10,3)
microBC.setOrder(2)
microBC.setBCPhysical(109,111,113,110,112,114)

# periodiodic BC
method = 0 # Periodic mesh = 0 Langrange interpolation = 1 Cubic spline interpolation =2
degree = 5 
addVertex = 0
microBC.setPeriodicBCOptions(method, degree,bool(addVertex))

# DEFINE MACROPROBLEM
matnum = 1;
macromat = hoDGMultiscaleMaterialLaw(matnum , 1000)
macromat.setViewAllMicroProblems(bool(1),0)

microSolver = macromat.getMicroSolver()
microSolver.loadModel(micromeshfile);
microSolver.addDomain(myfield1)
microSolver.addMaterialLaw(law1);
microSolver.addMicroBC(microBC)

microSolver.snlData(1,1.,1e-6)
microSolver.setSystemType(1)
microSolver.Solver(2)
microSolver.Scheme(1)

microSolver.setStressAveragingMethod(1)
microSolver.setTangentAveragingMethod(1,1e-6)

microSolver.stiffnessModification(bool(1));
microSolver.iterativeProcedure(bool(1));


macromeshfile="macro.msh" # name of mesh file

# solver
sol = 2  # Gmm=0 (default) Taucs=1 PETsc=2
soltype =1 # StaticLinear=0 (default) StaticNonLinear=1
nstep = 1  # number of step (used only if soltype=1)
ftime =1.   # Final time (used only if soltype=1)
tol=1.e-6  # relative tolerance for NR scheme (used only if soltype=1)
nstepArch=1 # Number of step between 2 archiving (used only if soltype=1)


# creation of  macro part Domain
fulldg= 1
beta1  = 100
bulkpert =0
interpert =0
perturbation = 1e-6

nfield1 = 51 # number of the field (physical number of entity)
dim =3
macrodomain1 =hoDGDomain(11,nfield1,0,matnum,fulldg)
macrodomain1.stabilityParameters(beta1)

nfield2 = 52
macrodomain2 =hoDGDomain(12,nfield2,0,matnum,fulldg)
macrodomain2.stabilityParameters(beta1)

iterdomain1 =hoDGInterDomain(13,macrodomain1,macrodomain2)
iterdomain1.stabilityParameters(beta1)



# creation of Solver
mysolver = nonLinearMechSolver(1000)
mysolver.loadModel(macromeshfile)
mysolver.addDomain(macrodomain1)
mysolver.addDomain(macrodomain2)
mysolver.addDomain(iterdomain1)
mysolver.addMaterialLaw(macromat)
mysolver.setMultiscaleFlag(bool(1))
mysolver.Scheme(soltype)
mysolver.Solver(sol)
mysolver.snlData(nstep,ftime,tol)


# boundary condition
mysolver.displacementBC("Face",53,0,0.0)
mysolver.displacementBC("Face",53,1,0.0)
mysolver.displacementBC("Face",53,2,0.0)

mysolver.displacementBC("Face",54,1,0.1)

#mysolver.forceBC("Face",32,2,1)

# archivage
mysolver.internalPointBuildView("Green-Lagrange_xx",IPField.STRAIN_XX, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_yy",IPField.STRAIN_YY, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_zz",IPField.STRAIN_ZZ, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_xy",IPField.STRAIN_XY, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_yz",IPField.STRAIN_YZ, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_xz",IPField.STRAIN_XZ, 1, 1);
mysolver.internalPointBuildView("sig_xx",IPField.SIG_XX, 1, 1);
mysolver.internalPointBuildView("sig_yy",IPField.SIG_YY, 1, 1);
mysolver.internalPointBuildView("sig_zz",IPField.SIG_ZZ, 1, 1);
mysolver.internalPointBuildView("sig_xy",IPField.SIG_XY, 1, 1);
mysolver.internalPointBuildView("sig_yz",IPField.SIG_YZ, 1, 1);
mysolver.internalPointBuildView("sig_xz",IPField.SIG_XZ, 1, 1);
mysolver.internalPointBuildView("sig_VM",IPField.SVM, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange equivalent strain",IPField.GL_EQUIVALENT_STRAIN, 1, 1);
mysolver.internalPointBuildView("Equivalent plastic strain",IPField.PLASTICSTRAIN, 1, 1);

mysolver.internalPointBuildView("High-order strain norm",IPField.HIGHSTRAIN_NORM, 1, 1);
mysolver.internalPointBuildView("High-order stress norm",IPField.HIGHSTRESS_NORM, 1, 1);
mysolver.internalPointBuildView("Green -Lagrange strain norm",IPField.GL_NORM, 1, 1);
mysolver.internalPointBuildView("PK2 stress norm",IPField.STRESS_NORM, 1, 1);

mysolver.archivingForceOnPhysicalGroup("Face",53,0)
mysolver.archivingForceOnPhysicalGroup("Face",53,1)
mysolver.archivingForceOnPhysicalGroup("Face",53,2)

# solve
mysolver.solve()

check = TestCheck()
check.equal(-1.980745e+02,mysolver.getArchivedForceOnPhysicalGroup("Face",53,1),1.e-5)
