1) Micro-scale geometry: micro.geo
   a) we need the geometry (paralleliped) of the RVE
   b) we need to define 1 physical for the volume (here 5)
   c) we need one physical surface for each side of the RVE (here 109,111,113,110,112,114)
   d) if needed you can have different RVEs (but first start with a single one
   e) generate the mesh: gmsh -3 -order 2 micro.geo

2) Macro-scale geometry: macro.geo
   a) the geometry of the structure: keep it coarse in 3D if not excessively slow
   b) we need to define 1 physical per volume corresponding to a micro-structure: at first use only one (here 11 and 12)
   c) we need physical surfaces where we apply the BC
   d) gnerate the mesh: you need quadratic elements: gmsh -3 -order 2 macro.geo

3) execution file: multiscale.py
   a) First define the micro-scale problem (to be duplicated if several micro-structures)
	i) # creation of material law
           linear elasticity: law1 = dG3DLinearElasticMaterialLaw(lawnum,rho,E,nu)
           large deformation and elasto-plasticity:  law1 = J2LinearDG3DMaterialLaw(lawnum,rho,E,nu,sy0,h)

	ii) # creation of  micro part Domain
           nfield = 5 # number of the field (physical number of micro-scale volume)
           dim =3
           myfield1 = dG3DDomain(10,nfield,0,lawnum,0,dim)
	iii) # we need to define the Periodic BC with second order homogenization (the RVE does not have periodic nor its mesh)
           microBC = nonLinearPeriodicBC(10,3)
           microBC.setOrder(2)
           microBC.setBCPhysical(109,111,113,110,112,114) #here the physical of the surface of the micro RVE

           method = 0 # Periodic mesh = 0 Langrange interpolation = 1 Cubic spline interpolation =2
           degree = 5 
           addVertex = 0
           microBC.setPeriodicBCOptions(method, degree,bool(addVertex))

   b) Define the homogenized-scale problem
       i)  The homogenized material law (one for each kind of macro structure)
          matnum = 1;
          macromat = hoDGMultiscaleMaterialLaw(matnum , 1000)
          
          # all options for microstructured law
          macromat.setViewAllMicroProblems(bool(1),0)
          
          #set all optiosn for solver
          microSolver = macromat.getMicroSolver()
          microSolver.loadModel(micromeshfile);
          microSolver.addDomain(myfield1)
          microSolver.addMaterialLaw(law1);
          microSolver.addMicroBC(microBC)
          .. etc

       ii) resolution parameter: in non linear use several time steps!!!

       iii) macro domain (one per different micro-structure)
         nfield1 = 51 # number of the field (physical number of entity)
         dim =3
         macrodomain1 =hoDGDomain(11,nfield1,0,matnum,fulldg)
         macrodomain1.stabilityParameters(beta1)
       iv) if several macro-domain (here you could define only the 11 in macro.geo and in multiscale.py)
         nfield1 = 51 # number of the field (physical number of entity)
         dim =3
         macrodomain1 =hoDGDomain(11,nfield1,0,matnum,fulldg)
         macrodomain1.stabilityParameters(beta1)
       v) solver (add the macro-domains)
       vi) the macro-scale BC




