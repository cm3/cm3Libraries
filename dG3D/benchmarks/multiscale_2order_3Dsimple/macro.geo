Point(1) = {0, 0, 0, 1.0};
Point(2) = {0, 1, 0, 1.0};
Point(3) = {0, 1, 1, 1.0};
Point(4) = {0, 0, 1, 1.0};
Line(1) = {4, 3};
Line(2) = {3, 2};
Line(3) = {2, 1};
Line(4) = {1, 4};
Line Loop(5) = {4, 1, 2, 3};
Plane Surface(6) = {5};
Extrude {1, 0, 0} {
  Surface{6};
}
Extrude {-1, 0, 0} {
  Surface{6};
}

Physical Volume(51) = {2};
Physical Volume(52) = {1};
Physical Surface(53) = {50};
Physical Surface(54) = {28};
Transfinite Line {40, 32, 18, 31, 44, 2, 1, 36, 10, 22, 30, 33, 9, 14, 35, 4, 3, 13, 11, 8} = 2 Using Progression 1;
Transfinite Surface {41, 19, 23, 45, 50, 6, 28, 49, 37, 27, 15};
Recombine Surface {41, 19, 23, 45, 50, 6, 28, 49, 37, 27, 15};
Transfinite Volume {2, 1};


