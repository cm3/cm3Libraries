// Test case a SCB with a vertical load at its free extremity
// Size

//definition of unit
mm = 1e-03;

dx=1.*mm;
dy=1.*mm;
dz=0.5*mm;

// Characteristic length
Lc1=1.*mm;
x={-dx/2.,dx/2.,-dx/2.+0.03*mm,dx/2.-0.03*mm};
y={-dy/2.,dy/2.,-dy/2.+0.03*mm,dy/2.-0.03*mm};
z={0.,dz,dz+0.01*mm,2.*dz+0.01*mm};
// definition of points
For index In {0:1}
  x1=x[0+2*index];
  x2=x[1+2*index];
  y1=y[0+2*index];
  y2=y[1+2*index];
  z1=z[0+2*index];
  z2=z[1+2*index];
  Point(100*index+1) = { x1 , y1 , z1, Lc1};
  Point(100*index+2) = {  x2  , y1 , z1 , Lc1};
  Point(100*index+3) = {  x2  ,  y2  , z1 , Lc1};
  Point(100*index+4) = { x1 ,  y2  , z1 , Lc1};
  Point(100*index+5) = { x1 , y1 , z2 , Lc1};
  Point(100*index+6) = {  x2  , y1 , z2 , Lc1};
  Point(100*index+7) = {  x2  ,  y2  , z2 , Lc1};
  Point(100*index+8) = { x1 ,  y2  , z2 , Lc1};

  // Line between points
  Line(100*index+1) = {100*index+1,100*index+2};
  Line(100*index+2) = {100*index+2,100*index+3};
  Line(100*index+3) = {100*index+3,100*index+4};
  Line(100*index+4) = {100*index+4,100*index+1};
  Line(100*index+5) = {100*index+5,100*index+6};
  Line(100*index+6) = {100*index+6,100*index+7};
  Line(100*index+7) = {100*index+7,100*index+8};
  Line(100*index+8) = {100*index+8,100*index+5};
  Line(100*index+9) = {100*index+1,100*index+5};
  Line(100*index+10)= {100*index+2,100*index+6};
  Line(100*index+11)= {100*index+3,100*index+7};
  Line(100*index+12)= {100*index+4,100*index+8};

  // Surface definition
  Line Loop(100*index+1) = {100*index+1,100*index+2,100*index+3,100*index+4};
  Line Loop(100*index+2) = {100*index+5,100*index+6,100*index+7,100*index+8};
  Line Loop(100*index+3) = {100*index+1,100*index+10,-100*index-5,-100*index-9};
  Line Loop(100*index+4) = {100*index+2,100*index+11,-100*index-6,-100*index-10};
  Line Loop(100*index+5) = {100*index+3,100*index+12,-100*index-7,-100*index-11};
  Line Loop(100*index+6) = {100*index+4,100*index+9,-100*index-8,-100*index-12};

  Plane Surface(100*index+1) = {100*index+1};
  Plane Surface(100*index+2) = {100*index+2};
  Plane Surface(100*index+3) = {100*index+3};
  Plane Surface(100*index+4) = {100*index+4};
  Plane Surface(100*index+5) = {100*index+5};
  Plane Surface(100*index+6) = {100*index+6};

  //VOlume

  Surface Loop(100*index+7) = {100*index+1,100*index+2,100*index+3,100*index+4,100*index+5,100*index+6};
  Volume(100*index+1) = {100*index+7};

  // Physical objects to applied BC and material
  Physical Surface(100*index+1234) = {100*index+1};
  Physical Surface(100*index+5678) = {100*index+2};
  Physical Surface(100*index+1265) = {100*index+3};
  Physical Surface(100*index+2376) = {100*index+4};
  Physical Surface(100*index+3487) = {100*index+5};
  Physical Surface(100*index+4158) = {100*index+6};
  Physical Line(100*index+12) = {100*index+1};
  Physical Line(100*index+23) = {100*index+2};
  Physical Line(100*index+34) = {100*index+3};
  Physical Line(100*index+41) = {100*index+4};
  Physical Line(100*index+56) = {100*index+5};
  Physical Line(100*index+67) = {100*index+6};
  Physical Line(100*index+78) = {100*index+7};
  Physical Line(100*index+85) = {100*index+8};
  Physical Line(100*index+15) = {100*index+9};
  Physical Line(100*index+26) = {100*index+10};
  Physical Line(100*index+37) = {100*index+11};
  Physical Line(100*index+48) = {100*index+12};

  Physical Point(100*index+1) ={100*index+1};
  Physical Point(100*index+2) ={100*index+2};
  Physical Point(100*index+3) ={100*index+3};
  Physical Point(100*index+4) ={100*index+4};
  Physical Point(100*index+5) ={100*index+5};
  Physical Point(100*index+6) ={100*index+6};
  Physical Point(100*index+7) ={100*index+7};
  Physical Point(100*index+8) ={100*index+8};

  Physical Volume(100*index+10) ={100*index+1};

  // define transfinite mesh
  nb=6;
  If(index==1)
    nb=5;
  EndIf
  Transfinite Line {100*index+1,100*index+2,100*index+3,100*index+4,100*index+5,100*index+6,100*index+7,100*index+8} = nb;
  Transfinite Line {100*index+9,100*index+10,100*index+11,100*index+12} = 4;
  Transfinite Surface {100*index+1,100*index+2,100*index+3,100*index+4,100*index+5,100*index+6} ;
  Recombine Surface {100*index+1,100*index+2,100*index+3,100*index+4,100*index+5,100*index+6} ;
  Transfinite Volume {100*index+1};
EndFor
