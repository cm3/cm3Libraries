mm = 1;
L = 1*mm;
R1 = 0.1*mm;
R2 = 0.15*mm;
R3 = 0.2*mm;
R4 = 0.25*mm;

lsca = 0.05*mm;

Point(1) = {0, 0, 0,lsca};
Point(2) = {L, 0, 0,lsca};
Point(3) = {L, L, 0,lsca};
Point(4) = {0, L, 0,lsca};
Point(5) = {R1, 0, 0,lsca};
Point(6) = {0, R1, 0,lsca};
Point(7) = {L-R2, 0, 0,lsca};
Point(8) = {L, R2, 0,lsca};
Point(9) = {L,L-R3,0,lsca};
Point(10) = {L-R3, L, 0,lsca};
Point(11) = {R4, L, 0,lsca};
Point(12) = {0, L-R4, 0,lsca};

x = L/2; y=L/2; r = 0.4*mm; sl2 = 0.5*lsca;
p1=newp; Point(p1)={x+r*Cos(Pi/6),y+r*Sin(Pi/6),0,sl2};
p2=newp; Point(p2)={x+r*Cos(Pi/3),y+r*Sin(Pi/3),0,sl2};
p3=newp; Point(p3)={x-r*Cos(Pi/3),y+r*Sin(Pi/3),0,sl2};
p4=newp; Point(p4)={x-r*Cos(Pi/6),y+r*Sin(Pi/6),0,sl2};
p5=newp; Point(p5)={x-r*Cos(Pi/6),y-r*Sin(Pi/6),0,sl2};
p6=newp; Point(p6)={x-r*Cos(Pi/3),y-r*Sin(Pi/3),0,sl2};
p7=newp; Point(p7)={x+r*Cos(Pi/3),y-r*Sin(Pi/3),0,sl2};
p8=newp; Point(p8)={x+r*Cos(Pi/6),y-r*Sin(Pi/6),0,sl2};
pc=newp; Point(pc)={x,y,0,sl2};



Line(1) = {11, 10};
Line(2) = {10, 14};
Line(3) = {11, 15};
Line(4) = {16, 12};
Line(5) = {17, 6};
Line(6) = {18, 5};
Line(7) = {19, 7};
Line(8) = {7, 5};
Line(9) = {6, 12};
Line(10) = {20, 8};
Line(11) = {8, 9};
Line(12) = {9, 13};
Circle(13) = {15, 21, 16};
Circle(14) = {16, 21, 17};
Circle(15) = {17, 21, 18};
Circle(16) = {18, 21, 19};
Circle(17) = {19, 21, 20};
Circle(18) = {20, 21, 13};
Circle(19) = {13, 21, 14};
Circle(20) = {14, 21, 15};
Circle(21) = {10, 3, 9};
Circle(22) = {8, 2, 7};
Circle(23) = {5, 1, 6};
Circle(24) = {12, 4, 11};
Line Loop(25) = {24, 3, 13, 4};
Plane Surface(26) = {25};
Line Loop(27) = {1, 2, 20, -3};
Plane Surface(28) = {27};
Line Loop(29) = {21, 12, 19, -2};
Plane Surface(30) = {29};
Line Loop(31) = {12, -18, 10, 11};
Plane Surface(32) = {31};
Line Loop(33) = {17, 10, 22, -7};
Plane Surface(34) = {33};
Line Loop(35) = {7, 8, -6, 16};
Plane Surface(36) = {35};
Line Loop(37) = {15, 6, 23, -5};
Plane Surface(38) = {37};
Line Loop(39) = {9, -4, 14, 5};
Plane Surface(40) = {39};
Extrude {0, 0, 0.4} {
  Surface{34, 32, 36, 30, 38, 28, 40, 26};
}

Transfinite Line {108, 67, 64, 44, 111, 110, 65, 43, 69, 45, 42, 154, 152, 155, 113, 21, 198, 89, 11, 52, 130, 176, 70, 12, 56, 22, 122, 2, 19, 175, 196, 10, 18, 48, 17, 7, 47, 88, 87, 20, 166, 157, 1, 3, 174, 132, 133, 184, 13, 16, 100, 15, 135, 14, 24, 4, 180, 6, 8, 96, 144, 5, 9, 23} = 6 Using Progression 1;
Transfinite Surface {128, 84, 62, 172, 83, 115, 71, 127, 216, 30, 53, 57, 159, 75, 32, 123, 106, 61, 34, 49, 167, 150, 194, 28, 171, 105, 211, 137, 189, 203, 26, 185, 97, 36, 101, 149, 38, 40, 181, 145};
Recombine Surface {128, 84, 62, 172, 83, 115, 71, 127, 216, 30, 53, 57, 159, 75, 32, 123, 106, 61, 34, 49, 167, 150, 194, 28, 171, 105, 211, 137, 189, 203, 26, 185, 97, 36, 101, 149, 38, 40, 181, 145};
Transfinite Volume {4, 6, 2, 1, 8, 3, 7, 5};
Physical Volume(217) = {8, 7, 6, 5, 4, 3, 2, 1};
Physical Surface(218) = {97};
Physical Surface(219) = {83};
Physical Surface(220) = {159};
Physical Surface(221) = {181};
Physical Surface(222) = {194, 216, 172, 128, 84, 62, 106, 150};
Physical Surface(223) = {30, 32, 34, 36, 38, 40, 26, 28};
Translate {-0.5*L, -0.5*L, -0.5*L} {
  Volume{4, 2, 1, 6, 3, 8, 7, 5};
}
