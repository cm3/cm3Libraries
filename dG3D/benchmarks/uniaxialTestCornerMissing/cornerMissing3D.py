#coding-Utf-8-*-

from gmshpy import *
from dG3Dpy import*

#script to launch PBC problem with a python script

# material law
lawnum1 = 11 # unique number of law


E = 210E3
nu = 0.3
K = E/3./(1.-2.*nu)	# Bulk mudulus
mu =E/2./(1.+nu)	  # Shear mudulus
rho = 2.7 # Bulk mass
sy0 = 507.
h = E/100.


# creation of material law
harden = LinearExponentialJ2IsotropicHardening(lawnum1, sy0, h, 0., 10.)
law1   = J2LinearDG3DMaterialLaw(lawnum1,rho,E,nu,harden)
law1.setUseBarF(True)

# geometry
geofile="cornerMissing3D.geo"
meshfile="cornerMissing3D.msh" # name of mesh file

# creation of part Domain
nfield = 217 # number of the field (physical number of entity)
dim =3
myfield1 = dG3DDomain(10,nfield,0,lawnum1,0,3)


# solver
sol = 2  # Gmm=0 (default) Taucs=1 PETsc=2
soltype =1 # StaticLinear=0 (default) StaticNonLinear=1
nstep = 25  # number of step (used only if soltype=1)
ftime =1.   # Final time (used only if soltype=1)
tol=1.e-6  # relative tolerance for NR scheme (used only if soltype=1)
nstepArch=1 # Number of step between 2 archiving (used only if soltype=1)

# creation of Solver
mysolver = nonLinearMechSolver(1000)

mysolver.createModel(geofile,meshfile,3,1)
#mysolver.loadModel(meshfile)
mysolver.addDomain(myfield1)
mysolver.addMaterialLaw(law1)
mysolver.Scheme(soltype)
mysolver.Solver(sol)
mysolver.snlData(nstep,ftime,tol)


pf = True
if pf:
	method = 1
	mysolver.pathFollowing(pf,method)
	if method==1:
		# time-step adaptation by number of NR iterations
		mysolver.setPathFollowingIncrementAdaptationLocal(True,5,5)
		mysolver.setPathFollowingLocalSteps(1e-2,1e-6)
		mysolver.setPathFollowingLocalIncrementType(1); 
		mysolver.setPathFollowingSwitchCriterion(1e-3)
	elif method==0:
		mysolver.setPathFollowingIncrementAdaptation(True,5)
		mysolver.setPathFollowingControlType(1)
		mysolver.setPathFollowingTranversalCriterion(0)
		mysolver.setPathFollowingCorrectionMethod(0)
		mysolver.setPathFollowingArcLengthStep(0.14)

# periodiodic BC
microBC = nonLinearPeriodicBC(10,3)
microBC.setOrder(1)
microBC.setBCPhysical(218,219,222,220,221,223) #Periodic boundary condition

method = 1	# Periodic mesh = 0, Langrange interpolation = 1, Cubic spline interpolation =2,  FE linear= 3, FE Quad = 4
degree = 3	# Order used for polynomial interpolation 
addvertex = True # Polynomial interpolation by mesh vertex = 0, Polynomial interpolation by virtual vertex 
microBC.setPeriodicBCOptions(method, degree,addvertex) 
microBC.forceInterpolationDegree(True)
 # Deformation gradient
mysolver.addMicroBC(microBC)

mysolver.activateTest(True)

mysolver.displacementBCOnControlNode(1,0,0.)
mysolver.displacementBCOnControlNode(1,1,0.)
mysolver.displacementBCOnControlNode(1,2,0.)

mysolver.displacementBCOnControlNode(2,1,0.)

mysolver.displacementBCOnControlNode(4,1,0.)
mysolver.displacementBCOnControlNode(4,2,0.)

forceBC = True
if forceBC:
	mysolver.forceBCOnControlNode(4,0,6e1)
else:
	mysolver.displacementBCOnControlNode(4,0,0.005)




mysolver.archivingAverageValue(IPField.P_XX)
mysolver.archivingAverageValue(IPField.P_YY)
mysolver.archivingAverageValue(IPField.P_ZZ)
mysolver.archivingAverageValue(IPField.F_XX)
mysolver.archivingAverageValue(IPField.F_YY)
mysolver.archivingAverageValue(IPField.F_ZZ)

# build view
mysolver.internalPointBuildView("Green-Lagrange_xx",IPField.STRAIN_XX, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_yy",IPField.STRAIN_YY, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_zz",IPField.STRAIN_ZZ, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_xy",IPField.STRAIN_XY, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_yz",IPField.STRAIN_YZ, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_xz",IPField.STRAIN_XZ, 1, 1);
mysolver.internalPointBuildView("sig_xx",IPField.SIG_XX, 1, 1);
mysolver.internalPointBuildView("sig_yy",IPField.SIG_YY, 1, 1);
mysolver.internalPointBuildView("sig_zz",IPField.SIG_ZZ, 1, 1);
mysolver.internalPointBuildView("sig_xy",IPField.SIG_XY, 1, 1);
mysolver.internalPointBuildView("sig_yz",IPField.SIG_YZ, 1, 1);
mysolver.internalPointBuildView("sig_xz",IPField.SIG_XZ, 1, 1);
mysolver.internalPointBuildView("sig_VM",IPField.SVM, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange equivalent strain",IPField.GL_EQUIVALENT_STRAIN, 1, 1);
mysolver.internalPointBuildView("Equivalent plastic strain",IPField.PLASTICSTRAIN, 1, 1);


mysolver.archivingNodeDisplacementOnControlNode(1,0)
mysolver.archivingNodeDisplacementOnControlNode(1,1)
mysolver.archivingNodeDisplacementOnControlNode(1,2)

mysolver.archivingNodeDisplacementOnControlNode(4,0)
mysolver.archivingNodeDisplacementOnControlNode(4,1)
mysolver.archivingNodeDisplacementOnControlNode(4,2)


# solve
mysolver.solve()
check = TestCheck()
import linecache
homoStress = linecache.getline('Average_P_XX.csv',12)
val = float(homoStress.split(';')[1])
check.equal(-2.962196e+02,val,1.e-4)

