//+
SetFactory("OpenCASCADE");

cm = 1e-2; // Unit

// Current carrying wire
Rwire = 0.4*cm;
H_SMP = 5.*cm;

// air
Rextair = 1.*cm;

// Smp
RextSMP = 1.5*cm;

Cylinder(1) = {0, 0, -H_SMP/2., 0, 0, H_SMP, Rwire, 2*Pi};
Cylinder(2) = {0, 0, -H_SMP/2., 0, 0, H_SMP, Rextair, 2*Pi};
Cylinder(3) = {0, 0, -H_SMP/2., 0, 0, H_SMP, RextSMP, 2*Pi};
//+
BooleanFragments{ Volume{3}; Delete; }{ Volume{2}; Delete; }
BooleanFragments{ Volume{2}; Delete; }{ Volume{1}; Delete; }

Coherence;
//+
Physical Surface("REFWIRETOP", 2333) = {11};
//+
Physical Surface("REFWIREBOTTOM", 2444) = {12};
//+
Physical Surface("REFWIREOUT", 2666) = {9};
//+
Physical Surface("REFCORETOP", 11111) = {5};
//+
Physical Surface("REFCOREBOTTOM", 11115) = {6};
//+
Physical Surface("REFCOREOUT", 11113) = {4};
//+
Physical Volume("WIRE", 2000) = {1};
//+
Physical Volume("ECORE", 1000) = {3};
//+
Physical Point("REFWIREPOINT", 2555) = {6};
//+
Physical Point("REFCOREPOINTTOPEXTERIOR_1", 11116) = {3};
//+
Physical Point("REFCOREPOINTBOTTOMEXTERIOR_1", 11124) = {4};
//+

Physical Surface("AIRTOP", 3333) = {7};
//+
Physical Surface("AIRBOTTOM", 3444) = {10};
//+
Physical Surface("AIROUT", 3666) = {8};
//+
Physical Volume("AIR", 3000) = {4};
//+

// Adapting mesh size
// characteristic lengths
//lc0 = Rwire/3; // coil
lc1  = RextSMP/4; // smp 
lc2  = Rextair/6; // air

//Characteristic Length { PointsOf{ Volume{1}; } } = lc0;
Characteristic Length { PointsOf{ Volume{3}; } } = lc1;
Characteristic Length { PointsOf{ Volume{4}; } } = lc2;
