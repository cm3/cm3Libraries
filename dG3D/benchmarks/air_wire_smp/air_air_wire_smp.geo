//+
SetFactory("OpenCASCADE");
Mesh.SecondOrderLinear = 1; 
cm = 1e-2; // Unit

// Current carrying wire
Rwire = 0.2*cm;
H_SMP = 2.*cm;

// air between wire and smp
RextairIn = 1.*cm;

// Smp
RextSMP = 1.5*cm;

// air exterior to smp
RextairOut = 3.*cm;

Cylinder(1) = {0, 0, -H_SMP/2., 0, 0, H_SMP, Rwire, 2*Pi};
Cylinder(2) = {0, 0, -H_SMP/2., 0, 0, H_SMP, RextairIn, 2*Pi};
Cylinder(3) = {0, 0, -H_SMP/2., 0, 0, H_SMP, RextSMP, 2*Pi};
Cylinder(4) = {0, 0, -H_SMP/2., 0, 0, H_SMP, RextairOut, 2*Pi};
//+
BooleanFragments{ Volume{4}; Delete; }{ Volume{3}; Delete; }
BooleanFragments{ Volume{3}; Delete; }{ Volume{2}; Delete; }
BooleanFragments{ Volume{2}; Delete; }{ Volume{1}; Delete; }

Coherence;
//+
Physical Surface("REFWIRETOP", 2333) = {18};
//+
Physical Surface("REFWIREBOTTOM", 2444) = {19};
//+
Physical Surface("REFWIREOUT", 2666) = {16};
//+
Physical Surface("REFCORETOP", 11111) = {10};
//+
Physical Surface("REFCOREBOTTOM", 11115) = {13};
//+
Physical Surface("REFCOREOUT", 11113) = {11};
//+
Physical Volume("WIRE", 2000) = {1};
//+
Physical Volume("ECORE", 1000) = {5};
//+
Physical Point("REFWIREPOINT", 2555) = {11};
//+
Physical Point("REFCOREPOINTTOPEXTERIOR_1", 11116) = {7};
//+
Physical Point("REFCOREPOINTBOTTOMEXTERIOR_1", 11124) = {9};
//+

Physical Surface("AIRINTOP", 3333) = {14};
//+
Physical Surface("AIRINBOTTOM", 3444) = {17};
//+
Physical Surface("AIRINOUTER", 3666) = {15};
//+
Physical Volume("AIRIN", 3000) = {6};
//+

Physical Surface("AIROUTTOP", 4333) = {8};
//+
Physical Surface("AIROUTBOTTOM", 4444) = {9};
//+
Physical Surface("AIROUTOUTER", 4666) = {7};
//+
Physical Volume("AIROUT", 4000) = {4};
//+

// Adapting mesh size
// characteristic lengths
lc0  = Rwire/3; // coil
lc1  = RextSMP/4; // smp 
lc2  = RextairIn/4; // airin
lc3  = RextairOut/8; // airout

Characteristic Length { PointsOf{ Volume{5}; } } = lc1;
Characteristic Length { PointsOf{ Volume{6}; } } = lc2;
Characteristic Length { PointsOf{ Volume{4}; } } = lc3;
Characteristic Length { PointsOf{ Volume{1}; } } = lc0;
