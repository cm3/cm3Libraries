//+
SetFactory("OpenCASCADE");

cm = 1e-2; // Unit

// Current carrying wire
Rwire = 0.4*cm;
H_SMP = 10.*cm;

// Smp
RextSMP = 1.*cm;

Cylinder(1) = {0, 0, -H_SMP/2., 0, 0, H_SMP, Rwire, 2*Pi};
//+
Cylinder(2) = {0, 0, -H_SMP/2., 0, 0, H_SMP, RextSMP, 2*Pi};
//+
BooleanFragments{ Volume{2}; Delete; }{ Volume{1}; Delete; }
Coherence;
//+
Physical Surface("REFWIRETOP", 2333) = {5};
//+
Physical Surface("REFWIREBOTTOM", 2444) = {6};
//+
Physical Surface("REFWIREOUT", 2666) = {4};
//+
Physical Surface("REFCORETOP", 11111) = {2};
//+
Physical Surface("REFCOREBOTTOM", 11115) = {3};
//+
Physical Surface("REFCOREOUT", 11113) = {1};
//+
Physical Volume("WIRE", 2000) = {1};
//+
Physical Volume("ECORE", 1000) = {2};
//+
Physical Point("REFWIREPOINT", 2555) = {3};
//+
Physical Point("REFCOREPOINTTOPEXTERIOR_1", 11116) = {1};
//+
Physical Point("REFCOREPOINTBOTTOMEXTERIOR_1", 11124) = {2};
//+

// Adapting mesh size
// characteristic lengths
lc0 = Rwire/6; // coil
lc1  = RextSMP/6; // smp 

Characteristic Length { PointsOf{ Volume{1}; } } = lc0;
Characteristic Length { PointsOf{ Volume{2}; } } = lc1;
