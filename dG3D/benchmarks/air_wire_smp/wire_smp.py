#coding-Utf-8-*-
from gmshpy import *


#from dG3DpyDebug import*
from dG3Dpy import*

#script to launch Steinmann benchmark problem with a python script

import math
import pickle

flagUseEMStress = False
flagEMFieldDependentShearModulus = False
thermalSource = False
tApplied = 293.
current = 1.0
H_SMP = 1.e-1
Rwire = 4.e-3
RextSMP = 1.e-2

#==========Electric & magnetic parameters Conducting polymer========================
lx=ly=lz= 1.e0 #25. # electrical conductivity (S/m) 
seebeck=0.0 #21.e-6
v0=0. # initial electric potential
epsilon_0 = 8.854 * 1.e-12 # vacuum electric permittivity
mag_mu_0 = 4.e-7 * math.pi # vacuum magnetic permeability
epsilon_r_smp = 1.0 # relative elec permittivity of SMP
mag_r_smp = 1.0 # relative mag permeability of SMP
mag_mu_x = mag_mu_y = mag_mu_z = mag_r_smp * mag_mu_0 # mag permeability smp 
magpot0_x = magpot0_y = magpot0_z = 0.0 # initial magnetic potential
Irms = 0. # (Ampere)
freq = 0. # (Hz)
nTurnsCoil = 0
coilLx = coilLy = coilLz = 0 # m
coilW = 0 # m
lawnum= 1 
rho   = 270. # material density (Kg/m^3)
tinitial = 273.+20. # (K)
G= 1.e5 # Shear modulus (Pa)
nuSMP=0.495 # Poisson ratio
alpha = beta=gamma=0. # parameter of anisotropy
cp=10.*rho # specific heat capacity 
Kx=Ky=Kz=237. # Thermal conductivity (W/m K)
alphaTherm=0.0 #13.e-5 # thermal dilatation
E_matrix = G*2.*(1.+nuSMP) # (Pa)
# use of electric permittivity constants to formulate polarization
epsilon_4 = 0.0
epsilon_6 = 0.0
epsilon_5 = 0.0 #(epsilon_0 * (1.0-epsilon_r_smp)/rho)
# use of magnetic permeability constants to formulate magnetization
mu_7 = 0.0 # magnetic permeability constants of SMP
mu_9 = 0.0 # magnetic permeability constants of SMP
mu_8 = 0.0 #(rho * mag_mu_0)/((1.0/mag_r_smp)-1.0)

# material law: wire (current carrying copper wire)
# Material parameters for copper
lawnumwire = 10
rhowire = 8960. 
Ewire=120.e9 #youngs modulus
Gwire=48.e9 # Shear modulus
Muwire = 0.34 # Poisson ratio
alphawire = betawire = gammawire = 0. # parameter of anisotropy
cpwire= 385.*rhowire
Kxwire=Kywire=Kzwire= 401. # thermal conductivity tensor components
lxwire=lywire=lzwire=5.77e7 # electrical conductivity (S/m)
seebeckwire=0.0 #6.5e-6
alphaThermwire= 0.0 # thermal dilatation
v0wire = 0.
mag_r_wire = 1.0 # relative mag permeability of wire
mag_mu_x_wire = mag_mu_y_wire = mag_mu_z_wire = mag_r_wire * mag_mu_0 # mag permeability wire 
magpot0_x_wire = magpot0_y_wire = magpot0_z_wire = 0.0 # initial magnetic potential
epsilon_4_wire = 0.0
epsilon_6_wire = 0.0
epsilon_5_wire = 0.0
mu_7_wire = 0.0
mu_9_wire = 0.0 
mu_8_wire = 0.0 
#current=1. # (A)
wireLength= H_SMP #5.e-3 # m
wireRadiusOut= Rwire # m
wireCrossSection=math.pi*(wireRadiusOut*wireRadiusOut) # non-hollow wire
wireResistance= wireLength/(lxwire*wireCrossSection)
VoltageDifference= wireResistance*current
print('VoltageDifference: ',VoltageDifference)

useFluxT=True
evaluateCurlField = True;
evaluateTemperature = True;

meshfile = "wire_smp.msh"

# solver
sol = 2  # Gmm=0 (default) Taucs=1 PETsc=2
soltype = 1 # StaticLinear=0 (default) StaticNonLinear=1, Explicit=2, Multi=3, Implicit=4, Eigen=5
nstep = 10 # 8*EMncycles # number of step (used only if soltype=1)
ftime = 1.0 # EMncycles/freq; # according to characteristic time
tol=1.e-6  # relative tolerance for NR scheme (used only if soltype=1)
nstepArch=1 # Number of step between 2 archiving (used only if soltype=1)
fullDg = False #O = CG, 1 = DG
space1 = 0 # function space (Lagrange=0)
beta1 = 40.
eqRatio =1.

#  compute solution and BC (given directly to the solver
# creation of law

# material law for smp
# Mechanical law
elasticPotential = NeoHookeanElasticPotential(lawnum,E_matrix,nuSMP);
lawMech = dG3DHyperelasticMaterialLaw(lawnum, rho, elasticPotential);

# Generic Thermo-Mech law

lawTM   = GenericThermoMechanicsDG3DMaterialLaw(lawnum+1,rho,alpha,beta,gamma,tinitial,Kx,Ky,Kz,alphaTherm,alphaTherm,alphaTherm,cp)
lawTM.setMechanicalMaterialLaw(lawMech);
lawTM.setApplyReferenceF(False);

_cp = constantScalarFunction(cp);
lawTM.setLawForCp(_cp);

# ElecMag Generic Thermo-Mech law for smp
lawsmp = ElecMagGenericThermoMechanicsDG3DMaterialLaw(lawnum+2,rho,alpha,beta,gamma,lx,ly,lz,seebeck,v0,mag_mu_x,
mag_mu_y,mag_mu_z,epsilon_4,epsilon_5,epsilon_6,mu_7,mu_8,mu_9,magpot0_x,magpot0_y, magpot0_z, Irms, freq, nTurnsCoil, coilLx, coilLy, coilLz, coilW,useFluxT,evaluateCurlField,evaluateTemperature)

lawsmp.setThermoMechanicalMaterialLaw(lawTM);
lawsmp.setApplyReferenceF(False);
lawsmp.setUseEMStress(flagUseEMStress);
lawsmp.setEMFieldDependentShearModulus(flagEMFieldDependentShearModulus);

pertLawsmp = dG3DMaterialLawWithTangentByPerturbation(lawsmp,1.e-8)

#lawsmp.setUseBarF(True)

# material law for wire
elasticPotentialwire = NeoHookeanElasticPotential(lawnumwire,Ewire,Muwire);
lawMechwire = dG3DHyperelasticMaterialLaw(lawnumwire,rhowire,elasticPotentialwire);

lawTMwire = GenericThermoMechanicsDG3DMaterialLaw(lawnumwire+1,rhowire,alphawire,betawire,gammawire,tinitial,
Kxwire,Kywire,Kzwire,alphaThermwire,alphaThermwire,alphaThermwire,cpwire)
lawTMwire.setMechanicalMaterialLaw(lawMechwire);
lawTMwire.setApplyReferenceF(False);

_cpwire = constantScalarFunction(cpwire);
lawTMwire.setLawForCp(_cpwire);

lawwire = ElecMagGenericThermoMechanicsDG3DMaterialLaw(lawnumwire+2,rhowire,alphawire,betawire,gammawire,lxwire,lywire,lzwire,seebeckwire,
v0wire, mag_mu_x_wire, mag_mu_y_wire, mag_mu_z_wire,epsilon_4_wire,epsilon_5_wire,epsilon_6_wire,mu_7_wire,mu_8_wire,mu_9_wire,magpot0_x_wire,magpot0_y_wire, magpot0_z_wire, Irms, freq, nTurnsCoil, coilLx, coilLy, coilLz,coilW,useFluxT,evaluateCurlField,evaluateTemperature)

lawwire.setThermoMechanicalMaterialLaw(lawTMwire);
lawwire.setApplyReferenceF(False);
lawwire.setUseEMStress(False);
lawwire.setEMFieldDependentShearModulus(False);

#lawwire.setUseBarF(True)

# creation of ElasticField
SMPfield = 1000 # number of the field (physical number of SMP)
wirefield = 2000 # number of the field (physical number of wire)
SmpRefTop=11111 # top face of outer surface of SMP
SmpOuterShell = 11113 # outer cylindrical surface of smp
SmpRefBottom = 11115 # bottom face of outer surface of SMP

SurfwireTop = 2333 # delta voltage surface for wire
SurfwireBottom = 2444 # ground voltage surface for wire
WireRefPoint = 2555 # single point on top face of wire
SurfwireOuterShell = 2666 # outer cylindrical surface of wire

SmpRefTopPoint_Exterior_1=11116 # single point on top face of SMP at exterior cylindrical face
SmpRefBottomPoint_Exterior_1=11124 # single point on bottom face of SMP at exterior cylindrical face

SMP_field = ElecMagTherMechDG3DDomain(10,SMPfield,space1,lawnum+2,fullDg,eqRatio,3)
Wire_field = ElecMagTherMechDG3DDomain(10,wirefield,space1,lawnumwire+2,fullDg,eqRatio,3)

SMP_field.setConstitutiveExtraDofDiffusionEqRatio(eqRatio)
Wire_field.setConstitutiveExtraDofDiffusionEqRatio(eqRatio)
SMP_field.setConstitutiveCurlEqRatio(eqRatio)
Wire_field.setConstitutiveCurlEqRatio(eqRatio)
SMP_field.stabilityParameters(beta1)
Wire_field.stabilityParameters(beta1)

SMP_field.ElecMagTherMechStabilityParameters(beta1,fullDg)
Wire_field.ElecMagTherMechStabilityParameters(beta1,fullDg)

#thermalSource=True
mecaSource=False

SMP_field.setConstitutiveExtraDofDiffusionAccountSource(thermalSource,mecaSource)
Wire_field.setConstitutiveExtraDofDiffusionAccountSource(thermalSource,mecaSource)

# creation of Solver
mysolver = nonLinearMechSolver(1000)
mysolver.loadModel(meshfile)
mysolver.addDomain(SMP_field)
mysolver.addDomain(Wire_field)
mysolver.addMaterialLaw(lawsmp)
mysolver.addMaterialLaw(lawwire)
mysolver.Scheme(soltype)
mysolver.Solver(sol)
mysolver.snlData(nstep,ftime,tol,tol/10.)#forsmp   
mysolver.options("-ksp_type preonly")
mysolver.options("-pc_type lu")
mysolver.stepBetweenArchiving(nstepArch)
mysolver.snlManageTimeStep(25, 10, 2, 10)

# Mechanical BC
mysolver.displacementBC("Volume",wirefield,0,0.0)
mysolver.displacementBC("Volume",wirefield,1,0.0)
mysolver.displacementBC("Volume",wirefield,2,0.0)
mysolver.displacementBC("Volume",SMPfield,0,0.0)
mysolver.displacementBC("Volume",SMPfield,1,0.0)
mysolver.displacementBC("Volume",SMPfield,2,0.0)

#thermal BC
cyclicFunctionTemp1=cycleFunctionTime(0., tinitial,ftime,tinitial);
mysolver.initialBC("Volume","Position",SMPfield,3,tinitial)
mysolver.initialBC("Volume","Position",wirefield,3,tinitial)
mysolver.displacementBC("Volume",wirefield,3,cyclicFunctionTemp1)
mysolver.displacementBC("Volume",SMPfield,3,cyclicFunctionTemp1)

#electrical BC
#VoltageDifference = 1.e20
mysolver.initialBC("Volume","Position",SMPfield,4,0.0)
mysolver.initialBC("Volume","Position",wirefield,4,0.0)
cyclicFunctionvolt1=cycleFunctionTime(0., 0.,  ftime,0.);
mysolver.displacementBC("Face",SurfwireBottom,4,cyclicFunctionvolt1)
cyclicFunctionvolt2=cycleFunctionTime(0., 0., ftime,VoltageDifference);
mysolver.displacementBC("Face",SurfwireTop,4,cyclicFunctionvolt2)
mysolver.sameDisplacementBCBetweenTwoGroups("Face", SmpRefTop, SmpRefBottom, 4)

#magentic BC
mysolver.curlDisplacementBC("Face",SmpRefTop,5,0.0) # comp may also be 5
mysolver.curlDisplacementBC("Face",SmpRefBottom,5,0.0) # comp may also be 5
mysolver.curlDisplacementBC("Face",SmpOuterShell,5,0.0) # comp may also be 5
mysolver.curlDisplacementBC("Face",SurfwireTop,5,0.0) # comp may also be 5
mysolver.curlDisplacementBC("Face",SurfwireBottom,5,0.0) # comp may also be 5

#Gauging the edges using tree-cotree methods
PhysicalCurves = "" 		# input required as a string of comma separated ints
PhysicalSurfaces = "11111,11113,11115,2333,2444" # input required as a string of comma separated ints
PhysicalVolumes = "1000,2000" # input required as a string of comma separated ints
OutputPhysical = 55 		# input required as a int
mysolver.createTreeForBC(PhysicalCurves,PhysicalSurfaces,PhysicalVolumes,OutputPhysical)
mysolver.curlDisplacementBC("Edge",OutputPhysical,5,0.0)

mysolver.internalPointBuildView("svm",IPField.SVM, 1, 1)
mysolver.internalPointBuildView("sig_xx",IPField.SIG_XX, 1, 1)
mysolver.internalPointBuildView("sig_yy",IPField.SIG_YY, 1, 1)
mysolver.internalPointBuildView("sig_zz",IPField.SIG_ZZ, 1, 1)
mysolver.internalPointBuildView("sig_xy",IPField.SIG_XY, 1, 1)
mysolver.internalPointBuildView("sig_yz",IPField.SIG_YZ, 1, 1)
mysolver.internalPointBuildView("sig_xz",IPField.SIG_XZ, 1, 1)
mysolver.internalPointBuildView("eps_xx",IPField.STRAIN_XX, 1, 1)
mysolver.internalPointBuildView("eps_yy",IPField.STRAIN_YY, 1, 1)
mysolver.internalPointBuildView("eps_zz",IPField.STRAIN_ZZ, 1, 1)
mysolver.internalPointBuildView("eps_xy",IPField.STRAIN_XY, 1, 1)
mysolver.internalPointBuildView("eps_yz",IPField.STRAIN_YZ, 1, 1)
mysolver.internalPointBuildView("eps_xz",IPField.STRAIN_XZ, 1, 1)
mysolver.internalPointBuildView("temperature",IPField.TEMPERATURE, 1, 1)
mysolver.internalPointBuildView("w_AV",IPField.EMFIELDSOURCE, 1, 1)
mysolver.internalPointBuildView("w_T",IPField.THERMALSOURCE, 1, 1)
mysolver.internalPointBuildView("qx",IPField.THERMALFLUX_X, 1, 1)
mysolver.internalPointBuildView("qy",IPField.THERMALFLUX_Y, 1, 1)
mysolver.internalPointBuildView("qz",IPField.THERMALFLUX_Z, 1, 1)
mysolver.internalPointBuildView("voltage",IPField.VOLTAGE, 1, 1)
mysolver.internalPointBuildView("jex",IPField.ELECTRICALFLUX_X, 1, 1)
mysolver.internalPointBuildView("jey",IPField.ELECTRICALFLUX_Y, 1, 1)
mysolver.internalPointBuildView("jez",IPField.ELECTRICALFLUX_Z, 1, 1)
mysolver.internalPointBuildView("ax",IPField.MAGNETICVECTORPOTENTIAL_X, 1, 1)
mysolver.internalPointBuildView("ay",IPField.MAGNETICVECTORPOTENTIAL_Y, 1, 1)
mysolver.internalPointBuildView("az",IPField.MAGNETICVECTORPOTENTIAL_Z, 1, 1)
mysolver.internalPointBuildView("bx",IPField.MAGNETICINDUCTION_X, 1, 1)
mysolver.internalPointBuildView("by",IPField.MAGNETICINDUCTION_Y, 1, 1)
mysolver.internalPointBuildView("bz",IPField.MAGNETICINDUCTION_Z, 1, 1)
mysolver.internalPointBuildView("hx",IPField.MAGNETICFIELD_X, 1, 1)
mysolver.internalPointBuildView("hy",IPField.MAGNETICFIELD_Y, 1, 1)
mysolver.internalPointBuildView("hz",IPField.MAGNETICFIELD_Z, 1, 1)
mysolver.internalPointBuildView("js0_x",IPField.INDUCTORSOURCEVECTORFIELD_X, 1, 1)
mysolver.internalPointBuildView("js0_y",IPField.INDUCTORSOURCEVECTORFIELD_Y, 1, 1)
mysolver.internalPointBuildView("js0_z",IPField.INDUCTORSOURCEVECTORFIELD_Z, 1, 1)
mysolver.internalPointBuildView("F_xx",IPField.F_XX, 1, 1)
mysolver.internalPointBuildView("F_xy",IPField.F_XY, 1, 1)
mysolver.internalPointBuildView("F_xz",IPField.F_XZ, 1, 1)
mysolver.internalPointBuildView("F_yx",IPField.F_YX, 1, 1)
mysolver.internalPointBuildView("F_yy",IPField.F_YY, 1, 1)
mysolver.internalPointBuildView("F_yz",IPField.F_YZ, 1, 1)
mysolver.internalPointBuildView("F_zx",IPField.F_ZX, 1, 1)
mysolver.internalPointBuildView("F_zy",IPField.F_ZY, 1, 1)
mysolver.internalPointBuildView("F_zz",IPField.F_ZZ, 1, 1)
mysolver.internalPointBuildView("epl",IPField.PLASTICSTRAIN, 1, 1)
mysolver.internalPointBuildView("jacobian",IPField.JACOBIAN, 1, 1)

mysolver.solve()

check = TestCheck()

