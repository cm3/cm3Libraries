#coding-Utf-8-*-
from gmshpy import *
from dG3Dpy import *

# material law-----------------------------------------------

rho   = 1400.0
lawnum = 1

PropMat = 'VI_rand.csv'   # for two level computational homogenization
properties1 = "property1.i01"

lx=5.0e-3
ly= 3.0e-3
mylaw1 = NonLocalDamageDG3DMaterialLaw_Stoch(lawnum,rho,properties1,-2.5e-4,-0.075e-4, 0.0,lx, ly, 0.0, PropMat,1) 

mylaw1.setInitialDmax_Inc(0.99)
mylaw1.setInitialDmax_Mtx(0.975)

# law by perturbation
pertLaw1 = dG3DMaterialLawWithTangentByPerturbation(mylaw1,1e-8)

# domain
myfield1 = 120
fulldg = False
beta1 = 30.
eqRatio = 1.e8
space1 = 0       # function space (Lagrange=0)
mydom = dG3DDomain(10,myfield1,space1,lawnum,fulldg,2,2)
mydom.setNonLocalEqRatio(eqRatio)
mydom.stabilityParameters(30.)
PlaneStress = False #True
mydom.setPlaneStressState(PlaneStress)
#mydom.matrixByPerturbation(1,1,1,1e-8)

# solver
sol = 2           # Gmm=0 (default) Taucs=1 PETsc=2
soltype = 1       # StaticLinear=0 (default) StaticNonLinear=1
nstepImpl = 2000   # number of step (used only if soltype=1)
nstepArch= 20 #  Number of step between 2 archiving (used only if soltype=1)
ftime =5.e-2   # Final time (used only if soltype=1)

tol=1.0e-6 
absTol=1.e-9

mysolver = nonLinearMechSolver(10)
geofile = 'Sample.geo'
meshfile = 'Sample.msh'
mysolver.createModel(geofile,meshfile,2,2)

mysolver.addMaterialLaw(pertLaw1)
mysolver.addDomain(mydom)

mysolver.Scheme(soltype)
mysolver.Solver(sol)

mysolver.snlData(nstepImpl,ftime,tol,absTol)
mysolver.snlManageTimeStep(10, 3, 2, 10)
mysolver.stepBetweenArchiving(nstepArch)

# BC
mysolver.displacementBC("Edge",100,0,0.)
mysolver.displacementBC("Face",1041,4,0.)
mysolver.displacementBC("Face",120,2,0.)
mysolver.displacementBC("Node",1011,1,0.)
mysolver.displacementBC("Node",1015,1,0.)
mysolver.displacementBC("Edge",110,1,0.)
mysolver.sameDisplacementBC("Edge",111,1016,1)


method=1
mysolver.pathFollowing(True,method)

if method==0:
	mysolver.setPathFollowingIncrementAdaptation(True,4,0.3)
	mysolver.setPathFollowingControlType(0)
	mysolver.setPathFollowingCorrectionMethod(0)
	mysolver.setPathFollowingArcLengthStep(1e-1)
	mysolver.setBoundsOfPathFollowingArcLengthSteps(0,0.1);
elif method==1:
	mysolver.setPathFollowingIncrementAdaptationLocal(False,5,7)
	mysolver.setPathFollowingLocalSteps(10.,1e-2) 
	mysolver.setPathFollowingLocalIncrementType(1); 
	mysolver.setPathFollowingSwitchCriterion(0.3)
	mysolver.setBoundsOfPathFollowingLocalSteps(1e-13,4.)
elif method==2:
	mysolver.setPathFollowingIncrementAdaptation(True,4,0.3)
	mysolver.setPathFollowingControlType(0)
	mysolver.setPathFollowingCorrectionMethod(0)
	mysolver.setPathFollowingArcLengthStep(1e-1)
	mysolver.setBoundsOfPathFollowingArcLengthSteps(0,2.);

mysolver.sameDisplacementBC("Edge",101,1015,0)
mysolver.forceBC("Node",1015,0,1.e3)
stopCri = EndSchemeMonitoringWithZeroForceOnPhysical(0.3,"Edge",101,0)
mysolver.endSchemeMonitoring(stopCri)

# needed resultes
mysolver.internalPointBuildView("svm",IPField.SVM, 1, 1)
mysolver.internalPointBuildView("sig_xx",IPField.SIG_XX, 1, 1)
mysolver.internalPointBuildView("sig_yy",IPField.SIG_YY, 1, 1)
#mysolver.internalPointBuildView("sig_zz",IPField.SIG_ZZ, 1, 1)
mysolver.internalPointBuildView("sig_xy",IPField.SIG_XY, 1, 1)
#mysolver.internalPointBuildView("sig_yz",IPField.SIG_YZ, 1, 1)
#mysolver.internalPointBuildView("sig_xz",IPField.SIG_XZ, 1, 1)
mysolver.internalPointBuildView("epsilon_xx",IPField.STRAIN_XX, 1, 1)
mysolver.internalPointBuildView("epsilon_yy",IPField.STRAIN_YY, 1, 1)
#mysolver.internalPointBuildView("epsilon_zz",IPField.STRAIN_ZZ, 1, 1)
mysolver.internalPointBuildView("epsilon_xy",IPField.STRAIN_XY, 1, 1)
#mysolver.internalPointBuildView("epsilon_yz",IPField.STRAIN_YZ, 1, 1)
#mysolver.internalPointBuildView("epsilon_xz",IPField.STRAIN_XZ, 1, 1)
mysolver.internalPointBuildView("epl",IPField.PLASTICSTRAIN, 1, 1)
mysolver.internalPointBuildView("damage",IPField.DAMAGE, 1, 1)
mysolver.internalPointBuildView("FiberDamage",IPField.INC_DAMAGE, 1, 1)
mysolver.archivingIPOnPhysicalGroup("Face",myfield1, IPField.DAMAGE,IPField.MIN_VALUE, 1);
mysolver.archivingIPOnPhysicalGroup("Face",myfield1, IPField.DAMAGE,IPField.MAX_VALUE, 1);
mysolver.archivingIPOnPhysicalGroup("Face",myfield1, IPField.DAMAGE,IPField.MEAN_VALUE, 1);
mysolver.archivingIPOnPhysicalGroup("Face",myfield1, IPField.INC_DAMAGE,IPField.MIN_VALUE, 1);
mysolver.archivingIPOnPhysicalGroup("Face",myfield1, IPField.INC_DAMAGE,IPField.MAX_VALUE, 1);
mysolver.archivingIPOnPhysicalGroup("Face",myfield1, IPField.INC_DAMAGE,IPField.MEAN_VALUE, 1);

mysolver.archivingForceOnPhysicalGroup("Edge", 101, 0, 1)
mysolver.archivingForceOnPhysicalGroup("Edge", 100, 0, 1)
mysolver.archivingNodeDisplacement(1015,0,1)

mysolver.solve()

check = TestCheck()
check.equal(3.933209e-05,mysolver.getArchivedNodalValue(1015,0,mysolver.displacement),1.e-6)
check.equal(2.567573e+04,mysolver.getArchivedForceOnPhysicalGroup("Edge", 101, 0),6e-2)






