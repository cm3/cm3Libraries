#coding-Utf-8-*-
from gmshpy import *
from dG3Dpy import*
#from dG3DpyDebug import*
#script to launch beam problem with a python script

# material law
lawnum   = 1 # unique number of law
rho      = 7850
properties = "property.i01"


# geometry
geofile="cube.geo" # name of mesh file
meshfile="cube.msh" # name of mesh file

# solver
sol = 2  # Gmm=0 (default) Taucs=1 PETsc=2
soltype = 1 # StaticLinear=0 (default) StaticNonLinear=1
nstep = 100  # number of step (used only if soltype=1)
ftime =100000.   # Final time (used only if soltype=1)
tol=1.e-6   # relative tolerance for NR scheme (used only if soltype=1)
nstepArch=1 # Number of step between 2 archiving (used only if soltype=1)
fullDg = 0 #O = CG, 1 = DG
space1 = 0 # function space (Lagrange=0)


#  compute solution and BC (given directly to the solver
# creation of law
#law1 = NonLocalDamageDG3DMaterialLaw(lawnum,rho,properties)
law1 = NonLocalDamageDG3DMaterialLaw(lawnum,rho,properties)
# creation of ElasticField
nfield = 10 # number of the field (physical number of surface)
myfield1 = dG3DDomain(1000,nfield,space1,lawnum,fullDg,3)
myfield1.stabilityParameters(30.)
#myfield1.matrixByPerturbation(1,1,1,1e-8)
# creation of Solver
mysolver = nonLinearMechSolver(1000)
mysolver.createModel(geofile,meshfile,3,2)
#mysolver.loadModel(meshfile)
mysolver.addDomain(myfield1)
mysolver.addMaterialLaw(law1)
mysolver.Scheme(soltype)
mysolver.Solver(sol)
mysolver.snlData(nstep,ftime,tol)
mysolver.stepBetweenArchiving(nstepArch)
# BC
#shearing
mysolver.displacementBC("Face",1234,0,0.)
mysolver.displacementBC("Face",1234,1,0.)
mysolver.displacementBC("Face",1234,2,0.)
mysolver.displacementBC("Face",5678,1,0.00)
mysolver.forceBC("Face",2376,2,10000)
mysolver.forceBC("Face",4158,2,-10000)
mysolver.forceBC("Face",5678,0,10000)
#tension along z
#mysolver.displacementBC("Face",1234,2,0.)
#mysolver.displacementBC("Face",5678,0,0.000001)
#mysolver.displacementBC("Face",5678,2,0)
#mysolver.displacementBC("Face",1234,0,0.)
#mysolver.displacementBC("Face",1234,1,0.)
#mysolver.displacementBC("Face",1265,1,0.)
#mysolver.displacementBC("Face",3487,1,0.)


mysolver.internalPointBuildView("svm",IPField.SVM, 1, 1)
mysolver.internalPointBuildView("sig_xx",IPField.SIG_XX, 1, 1)
mysolver.internalPointBuildView("sig_yy",IPField.SIG_YY, 1, 1)
mysolver.internalPointBuildView("sig_zz",IPField.SIG_ZZ, 1, 1)
mysolver.internalPointBuildView("sig_xy",IPField.SIG_XY, 1, 1)
mysolver.internalPointBuildView("sig_yz",IPField.SIG_YZ, 1, 1)
mysolver.internalPointBuildView("sig_xz",IPField.SIG_XZ, 1, 1)
mysolver.internalPointBuildView("epl",IPField.PLASTICSTRAIN, 1, 1)
mysolver.archivingForceOnPhysicalGroup("Face", 1234, 0)
mysolver.archivingForceOnPhysicalGroup("Face", 5678, 0)
mysolver.archivingNodeDisplacement(8,0,1)

mysolver.solve()

check = TestCheck()
check.equal(1.598439e-01,mysolver.getArchivedNodalValue(8,0,mysolver.displacement),1e-6)

