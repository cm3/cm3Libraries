import numpy as npy
import pickle

from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
from matplotlib import cm
import matplotlib 
from mpl_toolkits.axes_grid1.inset_locator import zoomed_inset_axes
from mpl_toolkits.axes_grid1.inset_locator import mark_inset

#definition of unit
unit = 1.0e-3; # millimeters
#characteristic mesh size
h_x = 0.05*unit;
h_y = 0.04*unit;
L = 2.0*0.64*unit;
nx = 1;
ny = 1;

Lx = L + 2.0*nx*h_x;
Ly = ny*h_y;
Lz = 1;
sur=Ly*Lz
#numPart=8

fig, ax = plt.subplots()
#matplotlib.rc('xtick', labelsize=20) 
#matplotlib.rc('ytick', labelsize=20)
plt.xticks(fontsize=20)
plt.yticks(fontsize=20)
strn=[]
strs=[]


filename='./force101comp0.csv' 
dispname='./NodalDisplacementPhysical1015Num7comp0.csv'

try:
  with open(filename,'r') as data:
    B_string = data.read()     
except:
  print('Not found on partition: ',sim)
else:
  B_string = B_string.split('\n')
  force = []
  for i in range(len(B_string)-1):
    row = B_string[i]
    row = row.split(';')
    force.append(float(row[1]))
  stress = []
  for i in range(len(force)):
    if(force[i]>0.0):
      stress.append(force[i]/sur/1.e6)
  strs.append(stress)

try:
  with open(dispname,'r') as data:
    A_string = data.read()     
except:
  print('Not found on partition: ',sim)
else:
  A_string = A_string.split('\n')
  disp = []
  for i in range(len(A_string)-1):
    row = A_string[i]
    row = row.split(';')
    disp.append(float(row[1]))
  strain = []
  for i in range(len(disp)):
    if(disp[i]>0.0):
      strain.append(disp[i]/Lx)
  strn.append(strain)

totalStress=[]
#print(strs)
#print(strn)
for j in range(len(strs[0])):
  val=0.
  for i in range(len(strs)):
    val=val+strs[i][j]
  totalStress.append(float(val))

#plt.ylim([0.0, 500.0])
#plt.xlim([0.0, 0.015])
plt.locator_params(axis='x', nbins=5)
#plt.plot(x,y,'mx', markeredgewidth =3.0,markersize= 13)

#handles, labels = ax.get_legend_handles_labels()
#ax.legend(handles, labels)

plt.legend(fontsize=22, framealpha=0.0)
plt.xlabel('$\\frac{\Delta x}{L_0}$', fontsize=22)
plt.ylabel('$\\frac{F}{S_0}$ in MPa', fontsize=22)

#print(strn[0])
#print(totalStress)

line1, =plt.plot(strn[0],totalStress, linewidth= 1.0, color = 'blue', label="Simulation")
#plt.annotate('Conf. #1', xy=(0.00227, 583), xytext=(0.0015, 650),
#             arrowprops=dict(arrowstyle='->'),fontsize=22)
#plt.annotate('Conf. #2', xy=(0.0029, 653), xytext=(0.0020, 700),
#             arrowprops=dict(arrowstyle='->'),fontsize=22)

plt.yticks(visible=True)
plt.xticks(visible=True)
#legend([line1,line2], ['Simulation', 'Experimental'])
plt.legend(loc='lower right',prop={'size':22})

plt.savefig('NotchedForce.pdf',bbox_inches='tight')
plt.show()



