//Fiber reinforced composite cross-section
Geometry.AutoCoherence= 1.0e-9;
//defination of unit
unit = 1.0e-3;
//characteristic mesh size
h_x=0.05*unit;
h_y=0.04*unit;
h_z = 0.02*unit;
L = 2.0*0.64*unit;
nx= 1;
ny= 1;
cl=0.04*unit;
//point
Point(1) = {0.0 , 0.0 , 0.0, cl};
Point(2) = {0.0 , ny*h_y , 0.0, cl};

Line(1) = {1,2};

Extrude {nx*h_x , 0.0 , 0.0} {Line{1}; Layers{nx}; Recombine;}
Extrude {L , 0.0 , 0.0} {Line{2};Layers{50}; Recombine;}
Extrude {nx*h_x , 0.0 , 0.0} {Line{6}; Layers{nx}; Recombine;}


Physical Line(100) = {1};
Physical Line(101) = {10};
Physical Line(110) = {3,7,11};
Physical Line(111) = {4,8,12};
Physical Surface(120) = {5,9,13};

Physical Point(1011) = {1};
Physical Point(1012) = {2};
Physical Surface(1041) = {5,13};
Physical Point(1015) = {15};
Physical Point(1016) = {16};


Coherence;

