#coding-Utf-8-*-
from gmshpy import*
#from dG3DpyDebug import*
from dG3Dpy import*
from math import*

# Script for testing cube problem with Gurson

# Material law creation
# ===================================================================================
BulkLawNum1 = 1 

# material parameters - bulk law
rho = 7850. 		# kg/m3:	density
young = 210.0*1.e9	# Pa: 	module de young
nu    = 0.3		# -: 	poisson coefficient
sy0   = 400.0e6 	# Pa: 	initial yield stress
h_hard  = young/1000.	# Pa:	hardenning coefficient
h_exp = 1.0		# -: 	exponential coefficient
HardenLawNum1 = 2
Harden1 = PowerLawJ2IsotropicHardening(HardenLawNum1, sy0, h_hard, h_exp)
# - gurson
q1    = 1.5
q2    = 1.
q3    = 1.
fVinitial = 0.007
# - coalesence
fC     = 0.05
ff     = 0.15
ffstar = (q1- sqrt(q1*q1-q3*q3))/(q3*q3)
accRate = (ffstar-fC)/(ff-fC)

# - nucleation
fn     = 0.1;
sn     = 0.1;
epsilonn = 0.3;
Gdnexp1 = GaussianNucleationFunctionLaw(0, fn, sn, epsilonn);

# - non-local law
cl = 1.		# m2: 		non-local parameter (== NL-length^2) 
ClLawNum1 = 4	
Cl_Law1 = IsotropicCLengthLaw(ClLawNum1, cl)
# material law creation
BulkLaw1 = NonLocalDamageGursonDG3DMaterialLaw(BulkLawNum1, rho, young, nu, 
		q1,q2,q3,fVinitial, Harden1, Cl_Law1)
BulkLaw1.setOrderForLogExp(9)
BulkLaw1.setNucleationLaw(Gdnexp1)

BulkLaw1.setUseBarF(True)

CoalLawNum1 = 5
onsetCr = 1
if onsetCr==0:
	Coal_Law1 = FstarCoalescenceLaw(CoalLawNum1,fC,accRate)
	BulkLaw1.setCoalescenceLaw(Coal_Law1)
else:
	Coal_Law1 = ThomasonCoalescenceLaw(CoalLawNum1,accRate)
	BulkLaw1.setCoalescenceLaw(Coal_Law1)
	#
	lambda0= 5.;
	kappa = 1.;
	voidEvoLawNum1 = 6
	voidEvoLaw = SphericalVoidStateEvolutionLawWithMatrixPlasticDeformation(voidEvoLawNum1,lambda0,kappa)
	BulkLaw1.setVoidEvolutionLaw(voidEvoLaw)


interfaceLawNum1 = 2
lBand = 1e-3 # m
Kp = 1e12 # N/m
cohLaw1 = PorosityCohesiveBand3DLaw(interfaceLawNum1,lBand,Kp,True)
cohLaw1.setMaximalAdmissiblePenetrationJump(True, -1e-4)


fracLawNum1 = 6
fracLaw1 = FractureByCohesive3DLaw(fracLawNum1,BulkLawNum1,interfaceLawNum1)

# Solver parameters
# ===================================================================================
soltype = 1 		# StaticLinear=0 (default, StaticNonLinear=1, Explicit=2, 
			# Multi=3, Implicit=4, Eigen=5)
nstep = 300   		# Number of step
ftime =1.0   		# Final time
tol=1.e-6  		# Relative tolerance for NR scheme
tolAbs = 1.e-16		# Absolute tolerance for NR scheme
nstepArch=1	# Number of step between 2 archiving
nstepArchEnergy = 1	# Number of step between 2 energy computation and archiving
nstepArchForce = 1	# Number of step between 2 force archiving
MaxIter = 25		# Maximum number of iterations
StepIncrease = 3	# Number of successfull timestep before reincreasing it
StepReducFactor = 5.0 	# Timestep reduction factor
NumberReduction = 4	# Maximum number of timespep reduction: max reduction = pow(StepReducFactor,this)
fullDg = bool(1)        # O = CG, 1 = DG
dgnl = bool(1)		# DG for non-local variables inside a domain (only if fullDg)
eqRatio = 1.0e6		# Ratio between "elastic" and non-local equations 
space1 = 0 		# Function space (Lagrange=0)
beta1  = 30.0		# Penality parameter for DG

# Domain creation
## ===================================================================================
numPhysVolume1 = 29 		# Number of a physical volume of the model in .geo
numDomain1 = 1000 		# Number of the domain
field1 = dG3DDomain(numDomain1,numPhysVolume1,space1,fracLawNum1,fullDg,3,1)
field1.setBulkDamageBlockedMethod(0)
field1.stabilityParameters(beta1) 			# Adding stability parameters (for DG)
field1.setNonLocalStabilityParameters(beta1,dgnl) 		# Adding stability parameters (for DG)
field1.setNonLocalEqRatio(eqRatio)
field1.gaussIntegration(0,-1,-1)
#field1.matrixByPerturbation(1,1,1,1e-8) 		# Tangent computation analytically or by pertubation


# Solver creation
# ===================================================================================
mysolver = nonLinearMechSolver(numDomain1) 		# Solver associated with numSolver1
geofile="cube.geo"
meshfile= "cube.msh" 			# name of mesh file
mysolver.createModel(geofile,meshfile,3,1)
mysolver.addDomain(field1) 		# add domain
mysolver.addMaterialLaw(BulkLaw1) 	# add material law
mysolver.addMaterialLaw(cohLaw1) 	# add material law
mysolver.addMaterialLaw(fracLaw1) 	# add material law
mysolver.Solver(2) 			# Library solving: Gmm=0 (default) Taucs=1 PETsc=2
# solver parametrisation
mysolver.Scheme(soltype) 			# solver scheme	 				
mysolver.snlData(nstep,ftime,tol,tolAbs) 	# solver parameters
mysolver.snlManageTimeStep(MaxIter,StepIncrease,StepReducFactor,NumberReduction) #timestep
# solver archiving

mysolver.stepBetweenArchiving(nstepArch) 	# archiving frequency
mysolver.energyComputation(nstepArchEnergy)	# archiving frequency for energy
mysolver.lineSearch(bool(0))		# lineSearch activation
mysolver.options("-ksp_type preonly -pc_type lu -pc_factor_mat_solver_type mumps")

# Boundary conditions
# ===============================
tot_disp = 7e-4 # Max disp == elongation 50 pourcent
mysolver.displacementBC("Face",30,0,0.)		# face x = 0
mysolver.displacementBC("Face",31,0,tot_disp) # face x = L
mysolver.displacementBC("Face",34,1,0.)		# face y = 0
mysolver.displacementBC("Face",32,2,0.)		# face z = 0

mysolver.initialBC("Volume","Position",29,3,fVinitial)

# Variable storage
# ===============================
mysolver.internalPointBuildView("svm",IPField.SVM, 1, 1)
mysolver.internalPointBuildView("sig_xx",IPField.SIG_XX, 1, 1)
mysolver.internalPointBuildView("sig_yy",IPField.SIG_YY, 1, 1)
mysolver.internalPointBuildView("sig_zz",IPField.SIG_ZZ, 1, 1)
mysolver.internalPointBuildView("sig_xy",IPField.SIG_XY, 1, 1)
mysolver.internalPointBuildView("sig_yz",IPField.SIG_YZ, 1, 1)
mysolver.internalPointBuildView("sig_xz",IPField.SIG_XZ, 1, 1)
mysolver.internalPointBuildView("damage",IPField.DAMAGE,1,1)
mysolver.internalPointBuildView("epl",IPField.PLASTICSTRAIN, 1, 1)
mysolver.internalPointBuildView("COALESCENCE",IPField.COALESCENCE, 1, 1)

mysolver.archivingForceOnPhysicalGroup("Face", 31, 0, nstepArchForce)
mysolver.archivingNodeDisplacement(43,0, nstepArchForce)
mysolver.archivingIPOnPhysicalGroup("Volume",29, IPField.LOCAL_POROSITY,IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",29, IPField.CORRECTED_POROSITY,IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",29, IPField.DAMAGE,IPField.MEAN_VALUE);

# Solving
# ===========
mysolver.solve()

# Test
# ===========
check = TestCheck()
import csv
data = csv.reader(open('IPVolume29val_DAMAGEMean.csv'), delimiter=';')
porosity = list(data)
check.equal(2.758505e-02,float(porosity[-1][1]),1e-6)






