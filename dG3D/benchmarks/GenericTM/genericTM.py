#coding-Utf-8-*-
from gmshpy import *
from dG3Dpy import*
#from dG3DpyDebug import*

#script to launch beam problem with a python script

# material law
lawnum=num = 1 # unique number of law
rho   = 7850.
young =2.1e11
nu    =0.3

cv=1.
t0=200.
Kx=Ky=Kz=51.9
alphax=alphay=alphaz=12.e-6


# geometry
geofile="cube.geo"
meshfile="cube.msh" # name of mesh file

# solver
sol = 2  # Gmm=0 (default) Taucs=1 PETsc=2
soltype = 1 # StaticLinear=0 (default) StaticNonLinear=1
nstep = 50   # number of step (used only if soltype=1)
ftime =1.   # Final time (used only if soltype=1)
tol=1.e-6   # relative tolerance for NR scheme (used only if soltype=1)
nstepArch=1 # Number of step between 2 archiving (used only if soltype=1)
fullDg = 0 #O = CG, 1 = DG
space1 = 0 # function space (Lagrange=0)
beta1  = 10.
eqRatio =1.e8

alpha = beta=gamma=0.
cp = 0.

fieldSource= True  	#True #account for cp dT dt 	
mecaSource= False   	#True #account for Taylor Quincey

#  compute solution and BC (given directly to the solver
# creation of law

sy0=1e10
h=1.e9
harden = LinearExponentialJ2IsotropicHardening(1, sy0, h, 0., 10.)
lawMech   = J2LinearDG3DMaterialLaw(lawnum,rho,young,nu,harden)

law1   = GenericThermoMechanicsDG3DMaterialLaw(lawnum+1,rho,alpha,beta,gamma,t0,Kx,Ky,Kz,alphax,alphay,alphaz,cp)
law1.setMechanicalMaterialLaw(lawMech);
law1.setApplyReferenceF(False);

_cp = constantScalarFunction(cp);
law1.setLawForCp(_cp);

pertLaw1 = dG3DMaterialLawWithTangentByPerturbation(law1,1.e-8)

nfield = 10 # number of the field (physical number of surface)
myfield1 = dG3DDomain(1000,nfield,space1,lawnum+1,fullDg,3,0,1)

#myfield1.matrixByPerturbation(1,1,1,1.e-8)
myfield1.stabilityParameters(beta1)

myfield1.setConstitutiveExtraDofDiffusionEqRatio(eqRatio) 
myfield1.setConstitutiveExtraDofDiffusionAccountSource(fieldSource, mecaSource)

#myfield1.setConstitutiveExtraDofDiffusionStabilityParameters(beta1,bool(fullDg))

# creation of Solver
mysolver = nonLinearMechSolver(1000)
mysolver.createModel(geofile,meshfile,3,2)
#mysolver.loadModel(meshfile)
mysolver.addDomain(myfield1)
mysolver.addMaterialLaw(law1)
#mysolver.addMaterialLaw(pertLaw1)
mysolver.Scheme(soltype)
mysolver.Solver(sol)
mysolver.snlData(nstep,ftime,tol,tol)
mysolver.stepBetweenArchiving(nstepArch)
mysolver.options("-ksp_type preonly")
mysolver.options("-pc_type lu")

# BC

#mechanical BC
mysolver.displacementBC("Face",1234,2,-1.e-4)
mysolver.displacementBC("Face",5678,0,0.0)
mysolver.displacementBC("Face",5678,1,0.0)
mysolver.displacementBC("Face",5678,2,0.0)
#mysolver.displacementBC("Face",2376,0,0.)
#mysolver.displacementBC("Face",1265,1,0.)
"""
mysolver.displacementBC("Volume",nfield,0,0.0)
mysolver.displacementBC("Volume",nfield,1,0.0)
mysolver.displacementBC("Volume",nfield,2,0.0)
"""

#thermal BC
cyclicFunctionTemp=cycleFunctionTime(0., t0, ftime , t0);
#cyclicFunctionTemp1=cycleFunctionTime(0., t0, ftime , t0+100);
mysolver.displacementBC("Face",5678,3,cyclicFunctionTemp)
#mysolver.displacementBC("Face",1234,3,cyclicFunctionTemp1)
mysolver.scalarFluxBC("Face",1234,3,5.19e6)
mysolver.initialBC("Volume","Position",nfield,3,t0)

#mysolver.displacementBC("Volume",nfield,3,cyclicFunctionTemp)

mysolver.internalPointBuildView("svm",IPField.SVM, 1, 1)
mysolver.internalPointBuildView("sig_xx",IPField.SIG_XX, 1, 1)
mysolver.internalPointBuildView("sig_yy",IPField.SIG_YY, 1, 1)
mysolver.internalPointBuildView("sig_zz",IPField.SIG_ZZ, 1, 1)
mysolver.internalPointBuildView("sig_xy",IPField.SIG_XY, 1, 1)
mysolver.internalPointBuildView("sig_yz",IPField.SIG_YZ, 1, 1)
mysolver.internalPointBuildView("sig_xz",IPField.SIG_XZ, 1, 1)
mysolver.internalPointBuildView("temperature",IPField.TEMPERATURE, 1, 1)
mysolver.internalPointBuildView("qx",IPField.THERMALFLUX_X, 1, 1)
mysolver.internalPointBuildView("qy",IPField.THERMALFLUX_Y, 1, 1)
mysolver.internalPointBuildView("qz",IPField.THERMALFLUX_Z, 1, 1)
mysolver.archivingForceOnPhysicalGroup("Face", 1234, 2)
mysolver.archivingForceOnPhysicalGroup("Face", 5678, 2)
mysolver.archivingForceOnPhysicalGroup("Face", 1234, 3)
mysolver.archivingForceOnPhysicalGroup("Face", 5678, 3)
mysolver.archivingNodeDisplacement(1,3,1)
mysolver.archivingNodeDisplacement(5,2,1)

mysolver.solve()

check = TestCheck()
check.equal(4.431763e+08,mysolver.getArchivedForceOnPhysicalGroup("Face", 5678, 3),1.e-6)
check.equal(3.010568e+02,mysolver.getArchivedNodalValue(1,3,mysolver.displacement),1.e-6)
check.equal(0.000000e+00,mysolver.getArchivedNodalValue(5,2,mysolver.displacement),1.e-6)

