// Script to generate plane strain specimen in 2D

// Input
/// General parameters
unit = 1.0e-3;

/// Geometrical parameters
l0 = 18.0 * unit;   // reference lenght
We =  3.0 * unit;   // external width
W0 =  2.0 * unit;   // referecence width

Rn = 10.0 * unit;   // notch radius

lbox = W0 / 10.0;   // box size


/// Mesh parameters - sub division
ratio_fineZone_notch = 0.065; // w.r.t. notch depth
ratio_fineZone_axis1 = 0.12; // w.r.t. notch length
ratio_fineZone_axis2 = 0.80; // w.r.t. notch length
ratio_fineZone_axis3 = 1.20; // w.r.t. notch length

/// Mesh parameters - length
lc0 = 0.500 * unit;
lc1 = 0.500 * unit;
lc2 = 0.600 * unit;

lc4 = 2.200 * unit;


// Computation of some parameters
deltaW_notch = 0.5*(We - W0);
l_notch =  Sqrt(Rn*Rn - (Rn-deltaW_notch)*(Rn-deltaW_notch));  // position of the notch end along .z

deltaW_finePart = 0.5*(We - W0)*ratio_fineZone_notch;
l_finepart = Sqrt(Rn*Rn - (Rn-deltaW_finePart)*(Rn-deltaW_finePart));

laxis1 = l_notch * ratio_fineZone_axis1;
laxis2 = l_notch * ratio_fineZone_axis2;
laxis3 = l_notch * ratio_fineZone_axis3;


// Draw first a quarter first, 
// starting by its external contour points
Point(1) = {0.0, 0.0, 0.0, lc0};    // center
Point(2) = {0.0, 0.0, 0.5*l0, lc4};
Point(3) = {0.5*We, 0.0, 0.5*l0, lc4};
Point(4) = {0.5*We, 0.0, l_notch, lc2};
/// notch related points
Point(5) = {0.5*W0+deltaW_finePart, 0.0, l_finepart, 0.5*(lc1+lc0)};
Point(6) = {0.5*W0, 0.0, 0.0, lc0}; // notch center
Point(7) = {0.5*W0+Rn, 0.0, 0.0, lc1}; // notch geometrical center

/// center related point
Point(8) = {0.0, 0.0, laxis1, lc0};
Point(9) = {0.0, 0.0, laxis2, lc2};
Point(10) = {0.0, 0.0, laxis3, lc4};
Point(11) = {0.5*We, 0.0, laxis3, lc4};


Symmetry {1, 0, 0, 0} {
  Duplicata { Point{:};}
}

Symmetry {0, 0, 1, 0} {
  Duplicata { Point{:};}
}

// lines
Circle(1) = {6, 7, 5};
Circle(2) = {5, 7, 4};
Line(3) = {4, 11};
Line(4) = {11, 3};
Line(5) = {3, 2};
Line(6) = {2, 14};
Line(7) = {14, 22};
Line(8) = {22, 15};
Circle(9) = {15, 18, 16};
Circle(10) = {16, 18, 17};
Circle(11) = {17, 18, 36};
Circle(12) = {36, 18, 35};
Line(13) = {35, 39};
Line(14) = {39, 34};
Line(15) = {34, 24};
Line(16) = {24, 25};
Line(17) = {25, 33};
Line(18) = {33, 26};
Circle(19) = {26, 7, 27};
Circle(20) = {27, 7, 6};
Line(21) = {11, 10};
Line(22) = {22, 10};
Line(23) = {33, 32};
Line(24) = {32, 39};


// surfaces
Curve Loop(1) = {2, 3, 21, -22, 8, 9, 10, 11, 12, 13, -24, -23, 18, 19, 20, 1};
Plane Surface(1) = {1};
Curve Loop(2) = {21, -22, -7, -6, -5, -4};
Plane Surface(2) = {2};
Curve Loop(3) = {23, 24, 14, 15, 16, 17};
Plane Surface(3) = {3};

Point{8, 30, 9, 31} In Surface{1};


// Physical
Physical Point(1) = {1}; // center point
Physical Point(2) = {6}; // notch (x > 0)
Physical Point(3) = {17}; // notch (x < 0)
Physical Point(4) = {2}; // end (z > 0)
Physical Point(5) = {24}; // end (z < 0)

Physical Curve(104) = {5, 6}; // end (z > 0)
Physical Curve(105) = {16, 15}; // end (z < 0)

Physical Surface(201) = Surface{:}; // all surfaces




























