#coding-Utf-8-*-
from gmshpy import *
from dG3Dpy import*
from math import*

# ==========================================================
transitionFlag = 1   	# choose type of law. 
			# ==0 no crack
			# ==1 with crack

# Material law creation
# ==========================================================
# material parameters - bulk law
bulkLawNum1 = 11
rho   = 7850.0 	# density
young = 205.4e9	# module de young
nu    = 0.3	# poisson coefficient

# hardening law
sy0   = 862.4e6
h = 3399. *1e6
p1 = 0.012
n1 = 0.084
p2 = 0.056
n2 = 0.058
harden1 = LinearFollowedByMultiplePowerLawJ2IsotropicHardening(bulkLawNum1,sy0,h,p1,n1,p2,n2)

# growth law
q1    = 1.414
q2    = 1.
q3    = q1
fVinitial = 1.0e-2
lambda0 = 1.0
kappa = 1.25

cllaw = IsotropicCLengthLaw(0, 1.e-4*1.e-4)

bulkLaw1 = NonLocalPorousCoupledWithMPSAndMSSDG3DMaterialLaw(bulkLawNum1, young, nu, rho, q1,q2,q3, fVinitial, lambda0, kappa, harden1,cllaw,1e-8,False,1e-8)
bulkLaw1.setOrderForLogExp(9)
bulkLaw1.setNonLocalMethod(2)

bulkLaw1.setShearPorosityGrowthFactor(5.)

voidEvoLawGrowth = SphericalVoidStateEvolutionLawWithDeviatoricPlasticDeformation(bulkLawNum1,lambda0,kappa)
bulkLaw1.setGrowthVoidEvolutionLaw(voidEvoLawGrowth)
voidEvoLawNecking = SpheroidalVoidStateEvolutionLawWithAspectRatioEvolution(bulkLawNum1,lambda0,1.,kappa)
bulkLaw1.setCoalescenceVoidEvolutionLaw(voidEvoLawNecking)
rate = 2.
voidEvoLawShear = VoidStateLinearEvolutionLaw(bulkLawNum1,lambda0,kappa,rate)
bulkLaw1.setShearVoidEvolutionLaw(voidEvoLawShear)
bulkLaw1.setShearFactor(1.1)

bulkLaw1.setYieldSurfaceExponent(10)
bulkLaw1.setSubStepping(bool(1),6)
bulkLaw1.setStressFormulation(0)

interfaceLawNum1 = 2
lBand = 32.*1e-6 # m
Kp = 1.e17  #N/m2
cohLaw1 = PorosityCohesiveBand3DLaw(interfaceLawNum1,lBand,Kp,True)
cohLaw1.setMaximalAdmissiblePenetrationJump(True, -1e-8)
cohLaw1.setDeletionOfBadConditionnedIPV(True)
cohLaw1.accountJumpGradientAtInterfaces(0.0)
cohLaw1.setCrackDelayingFactor(0.8)

# fracture law creation
fracLawNum1 = 6
fracLaw1 = FractureByCohesive3DLaw(fracLawNum1,bulkLawNum1,interfaceLawNum1)


# Domain creation
meshfile = "model.msh" 	# name of mesh file
## ===============================================================
beta1  = 30
if transitionFlag == 0:
    hasDG = False
    field1 = dG3DDomain(10,201,0,bulkLawNum1,hasDG,2,3)
if transitionFlag == 1:
    hasDG = True		# Flag for DG activation
    field1 = dG3DDomain(10,201,0,fracLawNum1,hasDG,2,3)
    field1.stabilityParameters(beta1)
    field1.setNonLocalStabilityParameters(beta1,hasDG)
    field1.forceCohesiveInsertionAllIPs(True,0.0)
    field1.setBulkDamageBlockedMethod(1)
    field1.setIncrementNonlocalBased(True)




# Solver creation
# ==============================================================
ftime = 20. 		# Final time
tot_disp =2.0e-3    # Final displ.
soltype = 4 		# Solving mode
nstep = 500 		# Number of step
tolRel = 1.e-8 		# Relative tolerance for NR scheme
tolAbs = 1.e-3		# Absolute tolerance for NR scheme
nstepArchIP = 10		# Number of step between 2 archiving
nstepArchForce = 1	# Number of step between 2 force archiving
nstepArchEnergy = nstepArchForce # Number of step between 2 energy computation and archiving

MaxIter = 18		    # Maximum number of iterations
StepIncrease = 3	# Number of successfull timestep before reincreasing it
StepReducFactor = 4. 	# Timestep reduction factor
NumberReduction = 125	# Maximum number of timespep reduction: max reduction = pow(StepReducFactor,this)


mysolver = nonLinearMechSolver(1000)
mysolver.loadModel(meshfile)
mysolver.addDomain(field1)
mysolver.addMaterialLaw(bulkLaw1)
mysolver.addMaterialLaw(cohLaw1)
mysolver.addMaterialLaw(fracLaw1)
mysolver.Scheme(soltype)
# library solver: Gmm=0 (default) Taucs=1 PETsc=2
mysolver.Solver(2)
# N-R control (numb. of step, final time, rel. tol. and absolute one)
mysolver.snlData(nstep,ftime,tolRel,tolAbs)
# iterations control
mysolver.snlManageTimeStep(MaxIter,StepIncrease,StepReducFactor,NumberReduction)
# numerical damping (0.0 = max)
mysolver.implicitSpectralRadius(0.0)

# archiving frequency
mysolver.stepBetweenArchiving(nstepArchIP) 
mysolver.energyComputation(nstepArchEnergy)	# archiving frequency for energy


# Ending control
# ===============================
#endscheme = MultipleEndSchemeMonitoring(True) # "or" operation between monitoring
#scheme1 = ReduceTimeStepWithNumberOfCoalescenceIPs(9)
#scheme2 = ReduceTimeStepWithMaximalNumberOfBrokenElements(1)
#endscheme.setSchemeMonitoring(scheme1)
#endscheme.setSchemeMonitoring(scheme2)

scheme2 = ReduceTimeStepWithMaximalNumberOfBrokenElements(1)
mysolver.endSchemeMonitoring(scheme2)


# Boundary conditions
# ===============================
mysolver.displacementBC("Face",201,1,0.) # Face blocking along y
mysolver.displacementBC("Node",4,0,0.) # x-disp and y-rotation blocking
mysolver.displacementBC("Node",5,0,0.) # x-disp and y-rotation blocking

# loading
fct_up = LinearFunctionTime(0.,0.,ftime,tot_disp*0.5)
fct_down = LinearFunctionTime(0.,0.,ftime,-tot_disp*0.5)
mysolver.displacementBC("Edge",104,2,fct_up) # loading on z > 0
mysolver.displacementBC("Edge",105,2,fct_down) # loading on z < 0


# Variable storage
# ===============================
# IPField
mysolver.internalPointBuildView("svm",IPField.SVM, 1, 1)
mysolver.internalPointBuildView("sig_xx",IPField.SIG_XX, 1, 1)
mysolver.internalPointBuildView("sig_yy",IPField.SIG_YY, 1, 1)
mysolver.internalPointBuildView("sig_zz",IPField.SIG_ZZ, 1, 1)
mysolver.internalPointBuildView("sig_xz",IPField.SIG_XZ, 1, 1)
mysolver.internalPointBuildView("epl",IPField.PLASTICSTRAIN, 1, 1)
mysolver.internalPointBuildView("nl0",IPField.NONLOCAL_0, 1, 1)
mysolver.internalPointBuildView("nl1",IPField.NONLOCAL_1, 1, 1)
mysolver.internalPointBuildView("nl2",IPField.NONLOCAL_2, 1, 1)
mysolver.internalPointBuildView("mode0",IPField.ACTIVE_YIELD_0, 1, 1)
mysolver.internalPointBuildView("mode1",IPField.ACTIVE_YIELD_1, 1, 1)
mysolver.internalPointBuildView("mode2",IPField.ACTIVE_YIELD_2, 1, 1)
mysolver.internalPointBuildView("yield0",IPField.YIELD_VALUE_0, 1, 1)
mysolver.internalPointBuildView("yield1",IPField.YIELD_VALUE_1, 1, 1)
mysolver.internalPointBuildView("yield2",IPField.YIELD_VALUE_2, 1, 1)
mysolver.internalPointBuildView("triaxiality",IPField.STRESS_TRIAXIALITY, 1, 1)
mysolver.internalPointBuildView("lode",IPField.LODE_PARAMETER, 1, 1)
mysolver.internalPointBuildView("fv",IPField.YIELD_POROSITY, 1, 1)
mysolver.internalPointBuildView("chi_max",IPField.LIGAMENT_RATIO, 1, 3)
mysolver.internalPointBuildView("plastic",IPField.ACTIVE_DISSIPATION, 1, 1)
mysolver.internalPointBuildView("coales",IPField.COALESCENCE, 1, 1)
mysolver.internalPointBuildView("damage_is_blocked",IPField.DAMAGE_IS_BLOCKED, 1, IPField.MAX_VALUE)

# .csv files
    # applied force
mysolver.archivingForceOnPhysicalGroup("Edge", 104, 2, nstepArchForce) 
    # extremity up displ.
mysolver.archivingNodeDisplacement(4,2,nstepArchForce)
    # stricition points
mysolver.archivingNodeDisplacement(2,0,nstepArchForce)
mysolver.archivingNodeDisplacement(3,0,nstepArchForce)

    # arg. prototype: (dim, physical, ipval, frequency=1)

# Launch solving
# ===============================
mysolver.solve()


check = TestCheck()
check.equal(-1.884685e-04,mysolver.getArchivedNodalValue(2,0,mysolver.displacement),1.e-4)
check.equal(1.660954e-04,mysolver.getArchivedNodalValue(3,0,mysolver.displacement),1.e-4)









