#coding-Utf-8-*-
import numpy as npy
#import matplotlib
from gmshpy import *
from dG3Dpy import *
import os
import sys

	
#h_x,h_y,h_z : sides of the volume element
#Ngrain : number of grains of the voronoi
#BC : str defining the boundary conditions : 'SUBC', 'PERIODIC', 'KUBC'
#material : list containing the material properties
#angle : 3 angles defining the direction of the material for each grain

rho=2329.*10**(-15)
E=130000.*10**3;
nu=0.28
mu=75500.*10**3
initialT = 273.
kther = 1.e3
alpha= 2.e-6
cp = 2.

material=[rho,E,E,E,nu,nu,nu,mu,mu,mu,kther,kther,kther,alpha,alpha,alpha,cp]

Ngrain = 4;

h_x = 2.4
h_y = 0.5
h_z = 0.1

angle=[];angle.append([]);angle.append([]);angle.append([]);
for k in range(Ngrain):
    angle[0].append(k*45.)
    angle[1].append(k*90.)
    angle[2].append(0.)

# geometry
geofile="polycrystal.geo" # name of mesh file
meshfile="polycrystal.msh" # name of mesh file

# solver
sol = 2  # Gmm=0 (default) Taucs=1 PETsc=2
soltype = 1 # StaticLinear=0 (default) StaticNonLinear=1
nstep = 1   # number of step (used only if soltype=1)
ftime =1.   # Final time (used only if soltype=1)
tol=1.e-6   # relative tolerance for NR scheme (used only if soltype=1)
nstepArch=1 # Number of step between 2 archiving (used only if soltype=1)
space1 = 0 # function space (Lagrange=0)
system = 1 # Displacement elimination =0 Multiplier elimination = 1 Displacement+ multiplier = 2
control = 0 # load control = 0 arc length control euler = 1
eqRatio = 1.e6
considerSource = False
considerMecaSource = False
# creation of Solver
mysolver = nonLinearMechSolver(1000)
#mysolver.createModel(geofile,meshfile,3,1)
mysolver.loadModel(meshfile)

#Deterministic variables
rho   = material[0]
Ex=material[1];	Ey=material[2];	Ez=material[3];
Vxy=material[4];Vxz=material[5];Vyz=material[6];
MUxy=material[7];MUxz=material[8];MUyz=material[9];
Kx=material[10]; Ky=material[11]; Kz=material[12];
alphax=material[13]; alphay=material[14]; alphaz=material[15];
cp = material[16];


liste_nfield=[]
liste_myfield=[]
liste_law=[]

liste_alpha=angle[0]
liste_beta=angle[1]
liste_gamma=angle[2]

#Grain dependant
lawnum=1
while lawnum<=Ngrain:
  liste_nfield.append(lawnum+10) # number of the field (physical number osrc/mat/interface/matrix.cf surface)
  dom=ThermoMechanicsDG3DDomain(1000,liste_nfield[lawnum-1],0,lawnum,0,eqRatio,3)
  dom.setConstitutiveExtraDofDiffusionAccountSource(considerSource,considerMecaSource)
  liste_myfield.append(dom)
  liste_law.append(LinearThermoMechanicsDG3DMaterialLaw(lawnum,rho,Ex,Ey,Ez,Vxy,Vxz,Vyz,MUxy,MUxz,MUyz,liste_alpha[lawnum-1],liste_beta[lawnum-1],liste_gamma[lawnum-1], initialT, Kx, Ky, Kz, alphax,  alphay, alphaz,cp))
  liste_law[lawnum-1].setUseBarF(True)
  mysolver.addDomain(liste_myfield[lawnum-1])
  mysolver.addMaterialLaw(liste_law[lawnum-1])
  mysolver.initialBC("Volume","Position",liste_nfield[lawnum-1],3,initialT)
  lawnum+=1;
  

mysolver.Scheme(soltype)
mysolver.Solver(sol)

mysolver.snlData(nstep,ftime,tol)
mysolver.stepBetweenArchiving(nstepArch)
mysolver.setMessageView(True)
mysolver.setSystemType(system)
#mysolver.setControlType(control)
mysolver.setMicroProblemIndentification(0, 1000);


#boundary condition
microBC = nonLinearPeriodicBC(1000,3)
microBC.setOrder(1)
microBC.setBCPhysical(110,120,130,101,102,103)
	# periodiodic BC
method = 1	# Periodic mesh = 0, Langrange interpolation = 1, Cubic spline interpolation =2 
degree = 9	# Ordesrc/mat/interface/matrix.cr used for polynomial interpolation 
addvertex = True # Polynomial interpolation by mesh vertex = 0, Polynomial interpolation by virtual vertex 
microBC.setPeriodicBCOptions(method, degree,bool(addvertex))

microBC.setDofPartition(3,1,0) # NumMechaDof, NumConExtraDOf, NumNonConExtraDof
microBC.setInitialConstitutiveExtraDofDiffusionValue(0,initialT); #intial value for conExtraDof

# Deformation gradient

microBC.setDeformationGradient(1.1,0.2,0.5,0.2,1.3,0.1,0.5,0.1,0.9) # Deformation gradient
microBC.setConstitutiveExtraDofDiffusionGradient(0,10., 30., 0.);
microBC.setConstitutiveExtraDofDiffusionValue(0,initialT);

mysolver.addMicroBC(microBC)


mysolver.stressAveragingFlag(True) # set stress averaging OFF- 0 , ON-1
mysolver.setStressAveragingMethod(0) # 0 -volume 1- surface
#tangent averaging flag
mysolver.tangentAveragingFlag(True) # set tangent averaging OFF -0, ON -1
mysolver.setTangentAveragingMethod(0,1e-5) # 0- perturbation 1- condensation

mysolver.internalPointBuildView("svm",IPField.SVM, 1, 1)
mysolver.internalPointBuildView("sig_xx",IPField.SIG_XX, 1, 1)
mysolver.internalPointBuildView("sig_yy",IPField.SIG_YY, 1, 1)
mysolver.internalPointBuildView("sig_zz",IPField.SIG_ZZ, 1, 1)
mysolver.internalPointBuildView("sig_xy",IPField.SIG_XY, 1, 1)
mysolver.internalPointBuildView("sig_yz",IPField.SIG_YZ, 1, 1)
mysolver.internalPointBuildView("sig_xz",IPField.SIG_XZ, 1, 1)
mysolver.internalPointBuildView("eps_xx",IPField.STRAIN_XX, 1, 1)
mysolver.internalPointBuildView("eps_yy",IPField.STRAIN_YY, 1, 1)
mysolver.internalPointBuildView("eps_zz",IPField.STRAIN_ZZ, 1, 1)
mysolver.internalPointBuildView("eps_xy",IPField.STRAIN_XY, 1, 1)
mysolver.internalPointBuildView("eps_yz",IPField.STRAIN_YZ, 1, 1)
mysolver.internalPointBuildView("eps_xz",IPField.STRAIN_XZ, 1, 1)
mysolver.internalPointBuildView("temperature",IPField.TEMPERATURE, 1, 1)
mysolver.internalPointBuildView("qx",IPField.THERMALFLUX_X, 1, 1)
mysolver.internalPointBuildView("qy",IPField.THERMALFLUX_Y, 1, 1)
mysolver.internalPointBuildView("qz",IPField.THERMALFLUX_Z, 1, 1)

mysolver.archivingNodeDisplacement(6, 3)
mysolver.archivingNodeDisplacement(8, 0)
mysolver.archivingNodeDisplacement(8, 1)
mysolver.archivingNodeDisplacement(8, 2)
mysolver.archivingNodeDisplacement(8, 3)


mysolver.solve()   # solve BVP
 	

check = TestCheck()
check.equal(2.533422e+02,mysolver.getArchivedNodalValue(6,3,mysolver.displacement),1.e-4)
check.equal(1.163372e-01,mysolver.getArchivedNodalValue(8,0,mysolver.displacement),1.e-3)
check.equal(2.247941e-01,mysolver.getArchivedNodalValue(8,1,mysolver.displacement),1.e-3)
check.equal(5.204758e-01,mysolver.getArchivedNodalValue(8,2,mysolver.displacement),1.e-4)
check.equal(2.657753e+02,mysolver.getArchivedNodalValue(8,3,mysolver.displacement),1.e-4)

try:
  import linecache
  lconductivity = linecache.getline('E_0_GP_1000_TangentFluxGradExtraDof.csv',3)
  lflux = linecache.getline('E_0_GP_1000_ExtraDofFlux.csv',3)
  ldpdt = linecache.getline('E_0_GP_1000_TangentStressExtraDofValue.csv',3)
  ldedt = linecache.getline('E_0_GP_1000_fieldCapacity.csv',3)
  let   = linecache.getline('E_0_GP_1000_InternalEnergyExtraDof.csv',3)
except:
  print('Cannot get values in the files')
  import os
  os._exit(1)
else:
  conductivity = float(lconductivity.split(';')[1])
  flux = float(lflux.split(';')[2])
  dpdt = float(ldpdt.split(';')[5])
  dedt = float(ldedt.split(';')[1])
  et = float(let.split(';')[1])
  check.equal(-999.9999978390405,conductivity,1.e-4)
  check.equal(-2.974975e+04,flux,1.e-4)
  check.equal(-5.821045e+02,dpdt,1.e-3)
  check.equal(2.,dedt,1.e-4)
  check.equal(546,et,1.e-4)
