# test file

set(PYFILE twoHole.py)

set(FILES2DELETE 
  *.csv
  disp*
  stress*
)

add_cm3python_mpi_test(3 ${PYFILE} "${FILES2DELETE}")
