#coding-Utf-8-*-
from gmshpy import *
from dG3Dpy import*
import csv
#from dG3DpyDebug import*

#script to launch composite problem with a python script

# material law
lawnonlocal = 1 # unique number of law
lawlinear = 2 # unique number of law
rho = 1400. # Bulk mass
length =  1.e-7


# geometry
geofile="Window0.geo" #"RVE.geo" # name of mesh file
meshfile="Window0.msh" #"RVE.msh" # name of mesh file
propertiesLC="Matrix.i01"# Matrix properties file
propertiesLin="Fibre.i01" # Inclusionproperties file

# solver
sol = 2  # Gmm=0 (default) Taucs=1 PETsc=2
soltype = 1 # StaticLinear=0 (default) StaticNonLinear=1
nstep = 100   # number of step (used only if soltype=1)
ftime =1.   # Final time (used only if soltype=1)
tol=1.e-6   # relative tolerance for NR scheme (used only if soltype=1)
nstepArch=50 # Number of step between 2 archiving (used only if soltype=1)
fullDg =0
eqRatio =1.e6

#  compute solution and BC (given directly to the solver
# creation of law
law1 = NonLocalDamageDG3DMaterialLaw(lawnonlocal,rho,propertiesLC)
law1.setNumberOfNonLocalVariables(1)
law2 = NonLocalDamageDG3DMaterialLaw(lawlinear,rho,propertiesLin)
law2.setNumberOfNonLocalVariables(1)

# creation of ElasticField
Mfield = 51 # number of the field (physical number of Mtrix)
Ffield = 52 # number of the field (physical number of Fiber)

space1 = 0 # function space (Lagrange=0)

Matixfield = dG3DDomain(10,Mfield,space1,lawnonlocal,fullDg,2,1)
Fiberfield = dG3DDomain(10,Ffield,space1,lawlinear,fullDg,2,1)
Matixfield.setNonLocalEqRatio(eqRatio)
Fiberfield.setNonLocalEqRatio(eqRatio)



# creation of Solver
mysolver = nonLinearMechSolver(1000)
mysolver.createModel(geofile,meshfile,2,2)
#mysolver.loadModel(meshfile)
mysolver.addDomain(Matixfield)
mysolver.addDomain(Fiberfield)
mysolver.addMaterialLaw(law1)
mysolver.addMaterialLaw(law2)
mysolver.Scheme(soltype)
mysolver.Solver(sol)
mysolver.snlData(nstep,ftime,tol)
mysolver.stepBetweenArchiving(nstepArch)

####### BC ##########################
# fixed face

microBC = nonLinearPeriodicBC(1000,2)
method = 5	   # Periodic mesh = 0, Langrange interpolation = 1, Cubic spline interpolation =2,  FE linear= 3, FE Quad = 4
degree = 3	    # Order used for polynomial interpolation 
#addvertex = 1   # Polynomial interpolation by mesh vertex = 0, Polynomial interpolation by virtual vertex 

microBC.setOrder(1)
microBC.setBCPhysical(100,110,101,111)

mysolver.setMicroProblemIndentification(0, 0);


microBC.setPeriodicBCOptions(method, degree,False) 

mysolver.stressAveragingFlag(True) # set stress averaging ON- 0 , OFF-1
mysolver.tangentAveragingFlag(False) # set tangent averaging ON -0, OFF -1

# BC
a=25.e-03
eps11_max=0.04
eps22_max=0.0
microBC.setDeformationGradient(2.,0., 0.,2.)

fc11 = piecewiseScalarFunction()
fc11.put(0.,0.)
fc11.put(ftime/4.,eps11_max)
fc11.put(3.*ftime/4.,-eps11_max)
fc11.put(ftime,0.)
fc22 = piecewiseScalarFunction()
fc22.put(0.,0.)
fc22.put(ftime,eps22_max)
microBC.setPathFunctionDeformationGradient(0,0,fc11)
microBC.setPathFunctionDeformationGradient(1,1,fc22)

mysolver.addMicroBC(microBC)




####### BC ##########################


# needed resultes
mysolver.internalPointBuildView("svm",IPField.SVM, 1, 1)
mysolver.internalPointBuildView("sig_xx",IPField.SIG_XX, 1, 1)
mysolver.internalPointBuildView("sig_yy",IPField.SIG_YY, 1, 1)
mysolver.internalPointBuildView("sig_zz",IPField.SIG_ZZ, 1, 1)
mysolver.internalPointBuildView("sig_xy",IPField.SIG_XY, 1, 1)
mysolver.internalPointBuildView("sig_yz",IPField.SIG_YZ, 1, 1)
mysolver.internalPointBuildView("sig_xz",IPField.SIG_XZ, 1, 1)
mysolver.internalPointBuildView("damage",IPField.DAMAGE, 1, 1)
mysolver.internalPointBuildView("epl",IPField.PLASTICSTRAIN, 1, 1)
mysolver.archivingForceOnPhysicalGroup("Edge", 110, 0,nstepArch)
mysolver.archivingNodeDisplacement(5,0,nstepArch)
mysolver.archivingNodeDisplacement(5,1,nstepArch)
mysolver.archivingIPOnPhysicalGroup("Face",Mfield, IPField.SVM,IPField.MIN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Face",Mfield, IPField.SVM,IPField.MAX_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Face",Mfield, IPField.SVM,IPField.MEAN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Face",Mfield, IPField.SIG_XX,IPField.MIN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Face",Mfield, IPField.SIG_XX,IPField.MAX_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Face",Mfield, IPField.SIG_XX,IPField.MEAN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Face",Mfield, IPField.SIG_YY,IPField.MIN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Face",Mfield, IPField.SIG_YY,IPField.MAX_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Face",Mfield, IPField.SIG_YY,IPField.MEAN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Face",Mfield, IPField.SIG_ZZ,IPField.MIN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Face",Mfield, IPField.SIG_ZZ,IPField.MAX_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Face",Mfield, IPField.SIG_ZZ,IPField.MEAN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Face",Mfield, IPField.SIG_XY,IPField.MIN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Face",Mfield, IPField.SIG_XY,IPField.MAX_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Face",Mfield, IPField.SIG_XY,IPField.MEAN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Face",Mfield, IPField.SIG_XZ,IPField.MIN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Face",Mfield, IPField.SIG_XZ,IPField.MAX_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Face",Mfield, IPField.SIG_XZ,IPField.MEAN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Face",Mfield, IPField.SIG_YZ,IPField.MIN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Face",Mfield, IPField.SIG_YZ,IPField.MAX_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Face",Mfield, IPField.SIG_YZ,IPField.MEAN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Face",Mfield, IPField.PLASTICSTRAIN,IPField.MIN_VALUE,nstepArch);
mysolver.archivingIPOnPhysicalGroup("Face",Mfield, IPField.PLASTICSTRAIN,IPField.MAX_VALUE,nstepArch);
mysolver.archivingIPOnPhysicalGroup("Face",Mfield, IPField.PLASTICSTRAIN,IPField.MEAN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Face",Mfield, IPField.DAMAGE,IPField.MIN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Face",Mfield, IPField.DAMAGE,IPField.MAX_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Face",Mfield, IPField.DAMAGE,IPField.MEAN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Face",Ffield, IPField.SVM,IPField.MIN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Face",Ffield, IPField.SVM,IPField.MAX_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Face",Ffield, IPField.SVM,IPField.MEAN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Face",Ffield, IPField.SIG_XX,IPField.MIN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Face",Ffield, IPField.SIG_XX,IPField.MAX_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Face",Ffield, IPField.SIG_XX,IPField.MEAN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Face",Ffield, IPField.SIG_YY,IPField.MIN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Face",Ffield, IPField.SIG_YY,IPField.MAX_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Face",Ffield, IPField.SIG_YY,IPField.MEAN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Face",Ffield, IPField.SIG_ZZ,IPField.MIN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Face",Ffield, IPField.SIG_ZZ,IPField.MAX_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Face",Ffield, IPField.SIG_ZZ,IPField.MEAN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Face",Ffield, IPField.SIG_XY,IPField.MIN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Face",Ffield, IPField.SIG_XY,IPField.MAX_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Face",Ffield, IPField.SIG_XY,IPField.MEAN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Face",Ffield, IPField.SIG_XZ,IPField.MIN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Face",Ffield, IPField.SIG_XZ,IPField.MAX_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Face",Ffield, IPField.SIG_XZ,IPField.MEAN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Face",Ffield, IPField.SIG_YZ,IPField.MIN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Face",Ffield, IPField.SIG_YZ,IPField.MAX_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Face",Ffield, IPField.SIG_YZ,IPField.MEAN_VALUE, nstepArch);

mysolver.archivingAverageValue(IPField.SIG_ZZ)
mysolver.archivingAverageValue(IPField.SIG_YY)
mysolver.archivingAverageValue(IPField.SIG_XX)
mysolver.archivingAverageValue(IPField.SIG_XY)

mysolver.archivingAverageValue(IPField.STRAIN_ZZ)
mysolver.archivingAverageValue(IPField.STRAIN_YY)
mysolver.archivingAverageValue(IPField.STRAIN_XX)
mysolver.archivingAverageValue(IPField.STRAIN_XY)

mysolver.archivingAverageValue(IPField.P_XX)
mysolver.archivingAverageValue(IPField.P_YY)
mysolver.archivingAverageValue(IPField.P_ZZ)
mysolver.archivingAverageValue(IPField.P_XY)
mysolver.archivingAverageValue(IPField.P_YX)

mysolver.archivingAverageValue(IPField.F_XX)
mysolver.archivingAverageValue(IPField.F_YY)
mysolver.archivingAverageValue(IPField.F_XY)
mysolver.archivingAverageValue(IPField.F_YX)
mysolver.solve()



reader1 = csv.reader(open("E_0_GP_0_Average_P_XX.csv", "r"), delimiter=";")
x1 = list(reader1)
x2 = [0] * len(x1)
for i in range (len(x1)):
	x2[i] = x1[i][1]
x2=[float(i) for i in x2]
Max = max(x2)
Min = min(x2)
Last = x2[len(x2)-1]


check = TestCheck()
check.equal(1.701972e+02,Max,1.e-6)
check.equal(-3.743093e+02,Min,1.e-6)
check.equal(-9.036870e+01,Last,1.e-6)

