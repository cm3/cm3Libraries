# test file

set(PYFILE cylinder.py)

set(FILES2DELETE 
  disp*.msh
  stress*.msh
  *.csv
)

add_cm3python_test(${PYFILE} "${FILES2DELETE}")
