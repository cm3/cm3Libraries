mm = 1;
D = 12.*mm;
H = 12.*mm;

R = D/2.;
lsca = 0.15*D;

Point(1) = {0,0,0,lsca};
Point(2) = {R,0,0,lsca};
Point(3) = {0,R,0,lsca};

Circle(1) = {3, 1, 2};
Line(2) = {3, 1};
Line(3) = {1, 2};
Line Loop(4) = {1, -3, -2};
Plane Surface(5) = {4};
Extrude {0, 0, H/2} {
  Surface{5}; Layers{5}; //Recombine;
}
Physical Volume(11) = {1};
Physical Surface(1) = {5};
Physical Surface(2) = {22};
Physical Surface(3) = {17};
Physical Surface(4) = {21};
Physical Point(5) = {5};

//Recombine Surface {5};
