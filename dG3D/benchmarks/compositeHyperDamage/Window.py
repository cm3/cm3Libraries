#coding-Utf-8-*-
from gmshpy import *
from dG3Dpy import*

#script to launch beam problem with a python script

# material law
lawnonlocal = 2     # unique number of law
rhoEP   = 1000.e-9
youngEP = 3.2e3
nuEP    = 0.3
sy0EP   = 15.
hEP     = 300.0
hexp    = 100.
    
#lemaitre chaboche
s0 = 1.0e-1
n0 = 1.73
p0 = 0.0
pc = 10.0  

lawlinear = 3      # unique number of law
rhoCF   = 1750.e-9
youngCF    = 40.e3
youngACF   = 230.e3
nuCF       = 0.2 
nu_minorCF = 0.256*youngCF/youngACF 
GACF       = 24.e3
Ax       = 0.0     # direction of anisotropy
Ay       = 0.0
Az       = 1.0



# geometry
geofile="Window.geo" # name of mesh file
meshfile = "Window.msh"

# solver
sol = 2  # Gmm=0 (default) Taucs=1 PETsc=2
soltype = 1 # StaticLinear=0 (default) StaticNonLinear=1
nstep = 20   # number of step (used only if soltype=1)
ftime =1.   # Final time (used only if soltype=1)
tol=1.e-5   # relative tolerance for NR scheme (used only if soltype=1)
nstepArch=1 # Number of step between 2 archiving (used only if soltype=1)
fullDg = 0 #O = CG, 1 = DG
space1 = 0 # function space (Lagrange=0)
beta1  = 20.

  
lawnum = 1
#  compute solution and BC (given directly to the solver
#harden = ExponentialJ2IsotropicHardening(1,sy0EP, hEP, hexp)
#cl     = IsotropicCLengthLaw(1, 0.025*0.025)
#damlaw = LemaitreChabocheDamageLaw(1, p0, pc, n0, s0)
#law1   = NonLocalDamageJ2HyperDG3DMaterialLaw(lawnum,rhoEP,youngEP,nuEP,harden,cl,damlaw)

#elasticLaw1 = NeoHookeanElasticPotential(1,youngEP,nuEP)
elasticLaw1 = biLogarithmicElasticPotential(1,youngEP,nuEP,-1)
cl1     = IsotropicCLengthLaw(1, 0.025*0.025*0.01*0.01)
pinit = 5e-3
b = 0.
K = 25.
alp = 0.5 
damlaw1 = ExponentialDamageLaw(1, pinit, b, K, alp)
law1   = NonLocalAnisotropicDamageHyperelasticDG3DMaterialLaw(lawnum,rhoEP,elasticLaw1,cl1,damlaw1)
law1.setUseBarF(True)

# cohesive law
law2 = TransverseIsotropicDG3DMaterialLaw(lawlinear,rhoCF,youngCF,nuCF,youngACF,GACF,nu_minorCF,Ax,Ay,Az)

# creation of ElasticField
nfield = 51 # number of the field (physical number of surface)
nfieldF = 52 # number of the field (physical number of surface)
myfield1 = dG3DDomain(1000,nfield,space1,lawnum,fullDg,3,1)
#myfield1.setNonLocalStabilityParameters(beta1,True)
myfield1.setNonLocalEqRatio(1.)
myfield2 = dG3DDomain(1000,nfieldF,space1,lawlinear,fullDg) 


# creation of Solver
mysolver = nonLinearMechSolver(1000)
mysolver.createModel(geofile,meshfile,3,1)

#mysolver.loadModel(meshfile)
mysolver.addDomain(myfield1)
mysolver.addDomain(myfield2)
mysolver.addMaterialLaw(law1)
mysolver.addMaterialLaw(law2)
mysolver.Scheme(soltype)
mysolver.Solver(sol)
mysolver.snlData(nstep,ftime,tol)
mysolver.snlManageTimeStep(25,5,10,10)
mysolver.stepBetweenArchiving(nstepArch)

#pathfollowing
pf = True
method = 1
mysolver.pathFollowing(pf,method)
mysolver.setPathFollowingIncrementAdaptation(True,7)
if method==0:
	mysolver.setPathFollowingControlType(0)
	mysolver.setPathFollowingArcLengthStep(1.0e-4)
elif method ==1:
	mysolver.setPathFollowingLocalSteps(5e-5,1e-15)
	mysolver.setPathFollowingLocalIncrementType(1); 


mysolver.setSystemType(2)
mysolver.stiffnessModification(bool(1))
mysolver.iterativeProcedure(bool(1))
mysolver.setMessageView(bool(1))
# BC

microBC = nonLinearPeriodicBC(1000,3)
microBC.setOrder(1)
microBC.setBCPhysical(120,100,110,121,101,111)
method = 5     # Periodic mesh = 0, Langrange interpolation = 1, Cubic spline interpolation =2,  FE linear= 3, FE Quad = 4
degree = 5      # Order used for polynomial interpolation 
addvertex = 0   # Polynomial interpolation by mesh vertex = 0, Polynomial interpolation by virtual vertex 
microBC.setPeriodicBCOptions(method, degree, bool(addvertex))

pbcPartMethod = 1
if pbcPartMethod==0:
	print("pbcPartMethod = 0")
	microBC.clearAllConstraintDofs() # PBC is only applied for disp. 0, 1
	microBC.insertConstrainDof(0)
	microBC.insertConstrainDof(1)
	mysolver.displacementBC("Face",120,2,0.)
	mysolver.displacementBC("Face",121,2,0.)
elif pbcPartMethod==1:
	print("pbcPartMethod = 1")
	# comp 0, 1,2 will be consider by PBC

#-----------------
mysolver.addMicroBC(microBC)
mysolver.activateTest(True)

mysolver.displacementBC("Node",1,0,0.)
mysolver.displacementBC("Node",1,1,0.)
mysolver.displacementBC("Node",1,2,0.)
mysolver.displacementBC("Node",3,1,0.)
mysolver.displacementBC("Node",3,2,0.)
mysolver.displacementBC("Node",2,1,0.)

#defo


#mysolver.displacementBC("Node",3,0,0.025000*0.05)
mysolver.forceBC("Node",3,0,1.)

mysolver.internalPointBuildView("svm",IPField.SVM, 1, 1)
mysolver.internalPointBuildView("sig_xx",IPField.SIG_XX, 1, 1)
mysolver.internalPointBuildView("sig_yy",IPField.SIG_YY, 1, 1)
mysolver.internalPointBuildView("sig_zz",IPField.SIG_ZZ, 1, 1)
mysolver.internalPointBuildView("sig_xy",IPField.SIG_XY, 1, 1)
mysolver.internalPointBuildView("sig_yz",IPField.SIG_YZ, 1, 1)
mysolver.internalPointBuildView("sig_xz",IPField.SIG_XZ, 1, 1)
mysolver.internalPointBuildView("epl",IPField.PLASTICSTRAIN, 1, 1)
mysolver.internalPointBuildView("Damage",IPField.DAMAGE, 1, 1)

mysolver.archivingNodeDisplacement(3,0,1)

mysolver.solve()

check = TestCheck()
check.equal(9.025859e-05,mysolver.getArchivedNodalValue(3, 0,0),1.e-2)


