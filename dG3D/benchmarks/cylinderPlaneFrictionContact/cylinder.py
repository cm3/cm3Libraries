#coding-Utf-8-*-
from gmshpy import *
from dG3Dpy import*

#script to launch PBC problem with a python script

# material law
lawnum = 11 # unique number of law
E = 3.e9
nu= 0.4
sy0 = 120e6
h = E/10.
rho = 100

# creation of material law
law1 = J2LinearDG3DMaterialLaw(lawnum,rho,E,nu,sy0,h)

# geometry
meshfile="cylinder.msh" # name of mesh file

# creation of part Domain
nfield = 52 # number of the field (physical number of entity)
#myfield1 = FSDomain(1000,nfield,lawnum,3)
fullDg = 0
beta1 = 1e2
myfield1 = dG3DDomain(1000,nfield,0,lawnum,fullDg,3)
myfield1.stabilityParameters(beta1)
# solver
sol = 2  # Gmm=0 (default) Taucs=1 PETsc=2
soltype =1 # StaticLinear=0 (default) StaticNonLinear=1
nstep = 50  # number of step (used only if soltype=1)
ftime =1.   # Final time (used only if soltype=1)
tol=1.e-4  # relative tolerance for NR scheme (used only if soltype=1)
tolAbs = 1e-12
nstepArch=1 # Number of step between 2 archiving (used only if soltype=1)



# creation of Solver
mysolver = nonLinearMechSolver(1000)
mysolver.loadModel(meshfile)
mysolver.addDomain(myfield1)
mysolver.addMaterialLaw(law1)

mysolver.Scheme(soltype)
mysolver.Solver(sol)
mysolver.snlData(nstep,ftime,tol,tolAbs)
mysolver.stepBetweenArchiving(nstepArch)

# contact

flaw1 = CoulombFrictionLaw(1,0.,1.,1e11,1e10)

contact1 = dG3DRigidPlaneContactDomain(1000,2,26,3,52,28,0,1,0,1e11,1e-3,1e3)
contact1.setFriction(True)
contact1.addFrictionLaw(flaw1)
mysolver.contactInteraction(contact1)

flaw2 = CoulombFrictionLaw(2,0.,1.,1e11,1e9)
contact2 =dG3DRigidPlaneContactDomain(1000,2,27,3,52,29,0,-1,0,1e11,1e-3,1e3)
contact2.setFriction(True)
contact2.addFrictionLaw(flaw2)
mysolver.contactInteraction(contact2)


# build view
mysolver.internalPointBuildView("Green-Lagrange_xx",IPField.STRAIN_XX, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_yy",IPField.STRAIN_YY, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_zz",IPField.STRAIN_ZZ, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_xy",IPField.STRAIN_XY, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_yz",IPField.STRAIN_YZ, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_xz",IPField.STRAIN_XZ, 1, 1);
mysolver.internalPointBuildView("sig_xx",IPField.SIG_XX, 1, 1);
mysolver.internalPointBuildView("sig_yy",IPField.SIG_YY, 1, 1);
mysolver.internalPointBuildView("sig_zz",IPField.SIG_ZZ, 1, 1);
mysolver.internalPointBuildView("sig_xy",IPField.SIG_XY, 1, 1);
mysolver.internalPointBuildView("sig_yz",IPField.SIG_YZ, 1, 1);
mysolver.internalPointBuildView("sig_xz",IPField.SIG_XZ, 1, 1);
mysolver.internalPointBuildView("sig_VM",IPField.SVM, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange equivalent strain",IPField.GL_EQUIVALENT_STRAIN, 1, 1);
mysolver.internalPointBuildView("Equivalent plastic strain",IPField.PLASTICSTRAIN, 1, 1);

mysolver.displacementBC('Face',25,2,0.)
#mysolver.displacementBC('Edge',13,0,0.)
#mysolver.displacementBC('Node',16,0,0.)
#mysolver.displacementBC('Edge',32,0,1e-3)
#mysolver.displacementBC('Edge',32,1,-1e-3)

mysolver.displacementRigidContactBC(27,2,0)
mysolver.displacementRigidContactBC(27,1,-0.7e-3)
mysolver.displacementRigidContactBC(27,0,0.7e-3)

mysolver.displacementRigidContactBC(26,2,0)
mysolver.displacementRigidContactBC(26,1,0.)
mysolver.displacementRigidContactBC(26,0,0.)


# solve
mysolver.solve()

check = TestCheck()
check.equal(1.315195e-01,mysolver.getEnergy(2),1.e-3)

