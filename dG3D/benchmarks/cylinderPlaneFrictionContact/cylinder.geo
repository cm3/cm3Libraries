mm = 1e-3;

R = 1*mm;
a = 1*mm;

lsca = 0.3*a;
Point(1) = {0,0,0,lsca};
Point(2) = {R,0,0,lsca};
Point(3) = {0,R,0,lsca};
Point(4) = {-R,0,0,lsca};
Point(5) = {0,-R,0,lsca};

Circle(1) = {4, 1, 5};
Circle(2) = {5, 1, 2};
Circle(3) = {2, 1, 3};
Circle(4) = {3, 1, 4};
Line Loop(5) = {4, 1, 2, 3};
Plane Surface(6) = {5};

Point(6) = {-2*R, -R-0.005*R,0,lsca};
Point(7) = {2*R, -R-0.005*R,0,lsca};
Line(15) = {6, 7};
Extrude {0, 0, -1e-3} {
  Line{15};
}
Translate {0,2*(R+0.005*R) , 0} {
  Duplicata { Surface{19}; }
}
Physical Surface(25) = {6};
Physical Surface(26) = {19};
Physical Point(28) = {6};

Physical Surface(27) = {20};
Physical Point(29) = {10};
Transfinite Line {16, 15, 18, 17, 24, 23, 21, 22} = 4 Using Progression 1;
Extrude {0, 0, -1e-3} {
  Surface{6}; Layers{3};
}
Physical Volume(52) = {1};
