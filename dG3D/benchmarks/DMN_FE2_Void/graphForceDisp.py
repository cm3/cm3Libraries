import pandas as pd
import matplotlib.pyplot as plt

plt.rcParams.update({'font.size': 14})


plt.figure(1)

fDNS = pd.read_csv('FE2-void/force2comp1_part0.csv',sep=';',names= ['Time','Val'])
uDNS = pd.read_csv('FE2-void/NodalDisplacementPhysical6Num3comp1_part0.csv',sep=';',names= ['Time','Val'])
x = uDNS['Val'].values
y = fDNS['Val'].values
plt.plot(x,-y,'r--',linewidth=2,label=r'FE${}^2$')

f7 = pd.read_csv('force2comp1.csv',sep=';',names= ['Time','Val'])
u7 = pd.read_csv('NodalDisplacementPhysical6Num3comp1.csv',sep=';',names= ['Time','Val'])
x = u7['Val'].values
y = f7['Val'].values
plt.plot(x,-y,'b-',linewidth=2,label=r'DMN')

plt.xlabel(r'Prescribed displacement $u_y$ (mm)')
plt.ylabel(r'Reaction force $\bar{F}_y$ (N)')
plt.legend(loc='upper left')

plt.savefig("FE_UD_forceDisp.png",bbox_inches='tight')
plt.savefig("FE_UD_forceDisp.eps",bbox_inches='tight')
plt.savefig("FE_UD_forceDisp.pdf",bbox_inches='tight')
plt.show()
