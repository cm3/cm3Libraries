#!/bin/bash
# Submission script for Nic5
#SBATCH --job-name=ms
#SBATCH --time=2-00:00:00 # days-hh:mm:ss
#
#SBATCH --ntasks=300
#SBATCH --mem-per-cpu=3625 # megabytes
#SBATCH --partition=batch
#
#SBATCH --mail-user=vandung.nguyen@ulg.ac.be
#SBATCH --mail-type=ALL
#
#SBATCH --comment=ms2


echo "Working in directory $PWD"

mpirun  python  model.py

echo "Finish"
