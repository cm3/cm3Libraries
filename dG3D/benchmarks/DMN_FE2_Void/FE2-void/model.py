#coding-Utf-8-*-
from gmshpy import *
from dG3Dpy import*

#script to launch PBC problem with a python script
#DEFINE MICRO PROBLEM

# material law
lawnum1 = 11 # unique number of law
E = 3E3 #MPa
nu = 0.3
K = E/3./(1.-2.*nu)	# Bulk mudulus
mu =E/2./(1.+nu)	  # Shear mudulus
rho = 7850e-9 # Bulk mass
sy0 = 100. #MPa
h = E/50.
harden1 = LinearExponentialJ2IsotropicHardening(lawnum1, sy0,h,0.,10.)
smallStrain=True
if smallStrain:
  law1   = J2SmallStrainDG3DMaterialLaw(lawnum1,rho,E,nu,harden1)
else:
  law1   = J2LinearDG3DMaterialLaw(lawnum1,rho,E,nu,harden1)
  law1.setStrainOrder(11)

# geometry
micromeshfile="micro.msh" # name of mesh file

myfield1 = dG3DDomain(1000,11,0,lawnum1,0,2)

microBC = nonLinearPeriodicBC(10,2)
microBC.setBCPhysical(1,2,3,4)
method =5	# Periodic mesh = 0
degree = 3 # Order used for polynomial interpolation
addvertex = 0  # Polynomial interpolation by mesh vertex = 0, Polynomial interpolation by virtual vertex
microBC.setPeriodicBCOptions(method, degree, bool(addvertex))


# DEFINE MACROPROBLEM
matnum1 = 1;
macromat1 = dG3DMultiscaleMaterialLaw(matnum1, 1000)
#for i in range(0,1):
#macromat1.setViewAllMicroProblems(True,0)
#macromat1.addViewMicroSolver(46,0)
#macromat1.addViewMicroSolver(47,0)

microSolver1 = macromat1.getMicroSolver()
microSolver1.loadModel(micromeshfile);
microSolver1.addDomain(myfield1)
microSolver1.addMaterialLaw(law1)
microSolver1.addMicroBC(microBC)

microSolver1.snlData(3,1.,1e-5,1e-10)
microSolver1.setSystemType(1)
microSolver1.Solver(2)

microSolver1.Scheme(1)
microSolver1.setSameStateCriterion(1e-16)
microSolver1.stressAveragingFlag(True) # set stress averaging ON- 0 , OFF-1
microSolver1.setStressAveragingMethod(0)
microSolver1.tangentAveragingFlag(True) # set tangent averaging ON -0, OFF -1
microSolver1.setTangentAveragingMethod(2,1e-6);

microSolver1.internalPointBuildView("Green-Lagrange_xx",IPField.GL_XX);
microSolver1.internalPointBuildView("Green-Lagrange_yy",IPField.GL_YY);
microSolver1.internalPointBuildView("Green-Lagrange_xy",IPField.GL_XY);
microSolver1.internalPointBuildView("sig_xx",IPField.SIG_XX);
microSolver1.internalPointBuildView("sig_yy",IPField.SIG_YY);
microSolver1.internalPointBuildView("sig_xy",IPField.SIG_XY);
microSolver1.internalPointBuildView("sig_VM",IPField.SVM);

microSolver1.internalPointBuildView("Equivalent plastic strain",IPField.PLASTICSTRAIN);
microSolver1.internalPointBuildView("Deformation energy",IPField.DEFO_ENERGY);
microSolver1.internalPointBuildView("Plastic energy",IPField.PLASTIC_ENERGY);

microSolver1.displacementBC("Face",11,2,0.)
microSolver1.displacementBC("Face",12,2,0.)

macromeshfile="model.msh" # name of mesh file

# solver
sol = 2  # Gmm=0 (default) Taucs=1 PETsc=2
soltype =1 # StaticLinear=0 (default) StaticNonLinear=1
nstep = 200  # number of step (used only if soltype=1)
ftime =1.   # Final time (used only if soltype=1)
tol=1.e-4  # relative tolerance for NR scheme (used only if soltype=1)
nstepArch=1 # Number of step between 2 archiving (used only if soltype=1)


# creation of  macro part Domain

dim =2
beta1 = 50;
fullDG = False;

averageStrainBased = True

# non DG domain
nfield1 = 11
macrodomain1 = dG3DDomain(10,nfield1,0,matnum1,fullDG,dim)
macrodomain1.stabilityParameters(beta1)
macrodomain1.averageStrainBased(averageStrainBased)
macrodomain1.setDistributedOtherRanks(True)
macrodomain1.distributeOnRootRank(False)

# creation of Solver
mysolver = nonLinearMechSolver(1000)
mysolver.loadModel(macromeshfile)

mysolver.addDomain(macrodomain1)
mysolver.addMaterialLaw(macromat1)
mysolver.setMultiscaleFlag(True)
mysolver.Scheme(soltype)
mysolver.Solver(sol)
mysolver.snlData(nstep,ftime,tol)
mysolver.stepBetweenArchiving(nstepArch)
mysolver.options("-ksp_type preonly -pc_type lu -pc_factor_mat_solver_type petsc")


# boundary condition
mysolver.displacementBC("Face",11,2,0.0)

mysolver.displacementBC("Edge",2,1,0.0)
mysolver.displacementBC("Edge",3,0,0.0)

fct = PiecewiseLinearFunction()
fct.put(0.,0.)
fct.put(0.5,0.06)
fct.put(0.6,0.04)
fct.put(0.7,0.06)
fct.put(0.9,0.15)
fct.put(1.,0.0)
mysolver.displacementBC("Edge",4,1,fct)
mysolver.setNumStepTimeInterval(0.5,40)
mysolver.setNumStepTimeInterval(0.6,20)
mysolver.setNumStepTimeInterval(0.7,20)
mysolver.setNumStepTimeInterval(0.9,45)
mysolver.setNumStepTimeInterval(1.,30)


# archivage
mysolver.internalPointBuildView("F_XX",IPField.F_XX);
mysolver.internalPointBuildView("F_XY",IPField.F_XY);
mysolver.internalPointBuildView("F_YX",IPField.F_YX);
mysolver.internalPointBuildView("F_YY",IPField.F_YY);
mysolver.internalPointBuildView("P_XX",IPField.P_XX);
mysolver.internalPointBuildView("P_XY",IPField.P_XY);
mysolver.internalPointBuildView("P_YX",IPField.P_YX);
mysolver.internalPointBuildView("P_YY",IPField.P_YY);
mysolver.internalPointBuildView("P_ZZ",IPField.P_ZZ);


mysolver.internalPointBuildViewIncrement("U_XX",IPField.U_XX);
mysolver.internalPointBuildViewIncrement("U_XY",IPField.U_XY);
mysolver.internalPointBuildViewIncrement("U_XZ",IPField.U_XZ);
mysolver.internalPointBuildViewIncrement("U_YY",IPField.U_YY);
mysolver.internalPointBuildViewIncrement("U_YZ",IPField.U_YZ);
mysolver.internalPointBuildViewIncrement("U_ZZ",IPField.U_ZZ);
mysolver.internalPointBuildViewIncrement("U_NORM",IPField.U_NORM);

mysolver.internalPointBuildView("Green-Lagrange_xx",IPField.GL_XX, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_yy",IPField.GL_YY, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_zz",IPField.GL_ZZ, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_xy",IPField.GL_XY, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_yz",IPField.GL_YZ, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_xz",IPField.GL_XZ, 1, 1);
mysolver.internalPointBuildView("sig_xx",IPField.SIG_XX, 1, 1);
mysolver.internalPointBuildView("sig_yy",IPField.SIG_YY, 1, 1);
mysolver.internalPointBuildView("sig_zz",IPField.SIG_ZZ, 1, 1);
mysolver.internalPointBuildView("sig_xy",IPField.SIG_XY, 1, 1);
mysolver.internalPointBuildView("sig_yz",IPField.SIG_YZ, 1, 1);
mysolver.internalPointBuildView("sig_xz",IPField.SIG_XZ, 1, 1);
mysolver.internalPointBuildView("sig_VM",IPField.SVM, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange equivalent strain",IPField.GL_EQUIVALENT_STRAIN, 1, 1);
mysolver.internalPointBuildView("Equivalent plastic strain",IPField.PLASTICSTRAIN, 1, 1);

mysolver.internalPointBuildView("Deformation energy",IPField.DEFO_ENERGY, 1, 1);
mysolver.internalPointBuildView("Plastic energy",IPField.PLASTIC_ENERGY, 1, 1);

mysolver.archivingForceOnPhysicalGroup("Edge",2,1)
mysolver.archivingForceOnPhysicalGroup("Edge",2,0)
mysolver.archivingNodeDisplacement(6,1)

# solve
mysolver.solve()
