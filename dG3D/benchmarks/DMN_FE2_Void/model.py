#coding-Utf-8-*-
from gmshpy import *
from dG3Dpy import*

#script to launch PBC problem with a python script
#DEFINE MICRO PROBLEM

# material law
lawnum1 = 11 # unique number of law
E = 3E3 #MPa
nu = 0.3
K = E/3./(1.-2.*nu)	# Bulk mudulus
mu =E/2./(1.+nu)	  # Shear mudulus
rho = 7850e-9 # Bulk mass
sy0 = 100. #MPa
h = E/50.
harden1 = LinearExponentialJ2IsotropicHardening(lawnum1, sy0,h,0.,10.)
smallStrain=True
if smallStrain:
  law1   = J2SmallStrainDG3DMaterialLaw(lawnum1,rho,E,nu,harden1)
else:
  law1   = J2LinearDG3DMaterialLaw(lawnum1,rho,E,nu,harden1)
  law1.setStrainOrder(11)

# DEFINE MACROPROBLEM
matnum1 = 1;
rho = 1e-9
macromat1 = DMNBasedDG3DMaterialLaw(matnum1, rho, "DMN/offlineTraining/best_tree_trained.txt",False,1e-6)
macromat1.addLaw(0,law1)
macromat1.fixDofsByComp(2)


macromeshfile="model.msh" # name of mesh file

# solver
sol = 2  # Gmm=0 (default) Taucs=1 PETsc=2
soltype =1 # StaticLinear=0 (default) StaticNonLinear=1
nstep = 200  # number of step (used only if soltype=1)
ftime =1.   # Final time (used only if soltype=1)
tol=1.e-4  # relative tolerance for NR scheme (used only if soltype=1)
nstepArch=1 # Number of step between 2 archiving (used only if soltype=1)


# creation of  macro part Domain

dim =2
beta1 = 50;
fullDG = False;

averageStrainBased = True

# non DG domain
nfield1 = 11
macrodomain1 = dG3DDomain(10,nfield1,0,matnum1,fullDG,dim)
macrodomain1.stabilityParameters(beta1)
macrodomain1.averageStrainBased(averageStrainBased)
macrodomain1.setDistributedOtherRanks(True)
macrodomain1.distributeOnRootRank(False)

# creation of Solver
mysolver = nonLinearMechSolver(1000)
mysolver.loadModel(macromeshfile)

mysolver.addDomain(macrodomain1)
mysolver.addMaterialLaw(macromat1)
mysolver.setMultiscaleFlag(True)
mysolver.Scheme(soltype)
mysolver.Solver(sol)
mysolver.snlData(nstep,ftime,tol)
mysolver.stepBetweenArchiving(nstepArch)
mysolver.options("-ksp_type preonly -pc_type lu -pc_factor_mat_solver_type petsc")


# boundary condition
mysolver.displacementBC("Face",11,2,0.0)

mysolver.displacementBC("Edge",2,1,0.0)
mysolver.displacementBC("Edge",3,0,0.0)

fct = PiecewiseLinearFunction()
fct.put(0.,0.)
fct.put(0.5,0.06)
fct.put(0.6,0.04)
fct.put(0.7,0.06)
fct.put(0.9,0.15)
fct.put(1.,0.0)
mysolver.displacementBC("Edge",4,1,fct)
mysolver.setNumStepTimeInterval(0.5,40)
mysolver.setNumStepTimeInterval(0.6,20)
mysolver.setNumStepTimeInterval(0.7,20)
mysolver.setNumStepTimeInterval(0.9,45)
mysolver.setNumStepTimeInterval(1.,30)


# archivage
mysolver.internalPointBuildView("eps_xx",IPField.STRAIN_XX, 1, 1);
mysolver.internalPointBuildView("eps_yy",IPField.STRAIN_YY, 1, 1);
mysolver.internalPointBuildView("eps_zz",IPField.STRAIN_ZZ, 1, 1);
mysolver.internalPointBuildView("eps_xy",IPField.STRAIN_XY, 1, 1);
mysolver.internalPointBuildView("eps_yz",IPField.STRAIN_YZ, 1, 1);
mysolver.internalPointBuildView("eps_xz",IPField.STRAIN_XZ, 1, 1);
mysolver.internalPointBuildView("sig_xx",IPField.SIG_XX, 1, 1);
mysolver.internalPointBuildView("sig_yy",IPField.SIG_YY, 1, 1);
mysolver.internalPointBuildView("sig_zz",IPField.SIG_ZZ, 1, 1);
mysolver.internalPointBuildView("sig_xy",IPField.SIG_XY, 1, 1);
mysolver.internalPointBuildView("sig_yz",IPField.SIG_YZ, 1, 1);
mysolver.internalPointBuildView("sig_xz",IPField.SIG_XZ, 1, 1);
mysolver.internalPointBuildView("sig_VM",IPField.SVM, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange equivalent strain",IPField.GL_EQUIVALENT_STRAIN, 1, 1);
mysolver.internalPointBuildView("Equivalent plastic strain",IPField.PLASTICSTRAIN, 1, 1);

mysolver.internalPointBuildView("Deformation energy",IPField.DEFO_ENERGY, 1, 1);
mysolver.internalPointBuildView("Plastic energy",IPField.PLASTIC_ENERGY, 1, 1);

mysolver.archivingForceOnPhysicalGroup("Edge",2,1)
mysolver.archivingForceOnPhysicalGroup("Edge",2,0)
mysolver.archivingNodeDisplacement(6,1)

# solve
mysolver.solve()

check = TestCheck()
check.equal(4.371632e+01,mysolver.getArchivedForceOnPhysicalGroup("Edge",2, 1),1.e-4)
