from gmshpy import *
from dG3Dpy import*
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import os
from os import listdir
from os.path import isfile, join
import random

def getFiles(folder, index):
  # get list of files using index
  # index == first strings in the name of files
  onlyfiles = [f for f in listdir(folder) if isfile(join(folder, f))]
  allFiles = list()
  for fname in onlyfiles:
    if fname[:len(index)]==index:
      allFiles.append(fname)
  return allFiles

# material law
lawnum1 = 11 # unique number of law
E = 3E3 #MPa
nu = 0.3
K = E/3./(1.-2.*nu)	# Bulk mudulus
mu =E/2./(1.+nu)	  # Shear mudulus
rho = 7850e-9 # Bulk mass
sy0 = 100. #MPa
h = E/50.
harden1 = LinearExponentialJ2IsotropicHardening(lawnum1, sy0,h,0.,10.)
smallStrain=True
if smallStrain:
  law1   = J2SmallStrainDG3DMaterialLaw(lawnum1,rho,E,nu,harden1)
else:
  law1   = J2LinearDG3DMaterialLaw(lawnum1,rho,E,nu,harden1)
  law1.setStrainOrder(11)


# material network
dmn = dG3DDeepMaterialNetwork()
dmn.addLaw(0,law1)
dmn.fixDofsByComp(2)
#dmn.viewIP(IPField.DAMAGE)
#dmn.viewIP(IPField.PLASTICSTRAIN)

# INITIALIZE DMN architecture
ni = NetworkInteraction()
method = 0
if method==0:
  phaseFraction = fullVectorDouble(2)
  fv = np.pi*0.2*0.2
  fs = 1-fv
  phaseFraction.set(0,fs)
  phaseFraction.set(1,fv)

  Nlayers = 3
  nodePerPhaseFullInteraction =2
  numFullInteraction =1
  dim = 2
  ni.createTreeBasedFullInteraction(Nlayers,nodePerPhaseFullInteraction,numFullInteraction,dim,phaseFraction,"linear")
  #os.system('dot  -Teps treeView.txt > treeView.eps')
  #os.system('epstopdf treeView.eps')
  #ni.initializeRandomFullInteraction(20, 10, 2,phaseFraction,"linear");
  ni.saveDataToFile("initialInteraction.txt")
else:
  ni.loadDataFromFile("best_tree_trained.txt")

dmn.addInteraction(ni)


# offline training
reduction = CoefficientReductionFullCoefficient()

#twoGrInters = [0,1,2,3,4,5,6]
#for ii in twoGrInters:
#  reduction.reductionAllInteraction(ii)

offTraining = TrainingDeepMaterialNetworkNonLinear(dmn,reduction)
offTraining.setMaximumStrainStep(1e-2)
offTraining.fixedPhaseFraction(True,"rectifier")

# training data
IPCompPhase = PathDouble()
Fphase = PathSTensor3()
Fref = PathSTensor3()
Pref = PathSTensor3()


trainingDir = '../trainingPaths'
validationDir = '../validationPaths'

trainingPaths = getFiles(trainingDir,'FPath')
validationPaths = getFiles(validationDir,'FPath')
Ntrain = len(trainingPaths)
Ntest =len(validationPaths)

print(f'num of training paths = {Ntrain} num of validation paths ={Ntest}')

# training data
offTraining.trainingDataSize(Ntrain)
offTraining.trainingDataSizeWithPhaseIPComp(Ntrain,0,IPField.VOLUMETRIC_STRAIN, 1.)
for m in range(1,1+Ntrain):
  strainFile = os.path.join(trainingDir,'FPath-part'+str(m)+'.csv')
  stressFile = os.path.join(trainingDir,'PPath-part'+str(m)+'.csv')
  volStrainFile = os.path.join(trainingDir,'VolumetricStrain11Path-part'+str(m)+'.csv')
  print(f'load training files:\n {strainFile}\n {stressFile} and\n {volStrainFile}')
  Fref.fromFile(strainFile)
  Pref.fromFile(stressFile)
  IPCompPhase.fromFile(volStrainFile)
  offTraining.setTrainingSample(m-1,Fref,Pref)
  offTraining.setTrainingSamplePhaseIPComp(m-1,IPCompPhase)

offTraining.testDataSize(Ntest)
for m in range(1,1+Ntest):
  strainFile = os.path.join(validationDir,'FPath-part'+str(m)+'.csv')
  stressFile = os.path.join(validationDir,'PPath-part'+str(m)+'.csv')
  volStrainFile = os.path.join(validationDir,'VolumetricStrain11Path-part'+str(m)+'.csv')
  print(f'load validation files:\n {strainFile}\n {stressFile} and\n {volStrainFile}')
  Fref.fromFile(strainFile)
  Pref.fromFile(stressFile)
  IPCompPhase.fromFile(volStrainFile)
  offTraining.setTestSample(m-1,Fref,Pref)
  offTraining.setTestSamplelePhaseIPComp(m-1,IPCompPhase)



#historyFile = "history.csv"
#saveDMNFileName="DMNtrained.txt"
#saveEpoch = 50
#offTraining.trainBatch(1e-2,500,"rmae",historyFile,saveDMNFileName,saveEpoch)

learningRate = 2.e-3
maxEpoch = 500
historyFile = 'historynem.csv'
treeFile = 'tree_trained.txt'
loss ="rmae"
errorMeasure ="rmse"
nbBatches = Ntrain

optimizer =Adam()
lrScheduler = PowerLearningRateDecay(learningRate,0.5,100)
tf = GradientDescentTraining(offTraining,optimizer,lrScheduler)
#tf.learningRateAdaptivity(True)
tf.fit(loss,learningRate,maxEpoch,nbBatches,errorMeasure,treeFile,historyFile)

hist  = pd.read_csv(historyFile,sep=';')
plt.plot(hist['Epoch'],hist[loss+'-Train'],'-',label='LossTrain')
plt.plot(hist['Epoch'],hist[loss+'-Test'],'--',label='LossTest')

plt.yscale('log')
plt.legend(fontsize = 14, loc='upper right')
plt.ylabel('Error (%)',fontsize=14, )
plt.xlabel('Epoch',fontsize=14)
plt.savefig('error_history.pdf',bbox_inches="tight")
