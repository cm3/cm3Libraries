import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import os
from os.path import isfile, join
from os import listdir
import random
from shutil import copyfile

def getFiles(folder, index):
  # get list of files using index
  # index == first strings in the name of files
  onlyfiles = [f for f in listdir(folder) if isfile(join(folder, f))]
  allFiles = list()
  for fname in onlyfiles:
    #if fname[:len(index)]==index:
    if index in fname:
      allFiles.append(fname)
  allFiles.sort()
  return allFiles

def writeListToFile(listData, fname):
  textfile = open(fname, "w")
  for element in listData:
    textfile. write(element + "\n")
  textfile. close()

def getData(src, dest, paths):
  numPaths = len(paths)
  print(f'numPaths = {numPaths}')
  for strainPath in paths:
    print("dataFrom : ",strainPath)
    locSrcDir = os.path.join(src,strainPath[:-4])
    #print(locSrcDir)
    allFileLoc=getFiles(locSrcDir,'FPath')+getFiles(locSrcDir,'PPath')+getFiles(locSrcDir,'VolumetricStrain')
    print(allFileLoc)
    indexPath = 1+paths.index(strainPath)
    for oldName in allFileLoc:
      newName = oldName[:-4]+'-part'+str(indexPath)+oldName[-4:]
      srcFile =os.path.join(src,strainPath[:-4],oldName)
      desFile =os.path.join(dest,newName)
      copyfile(srcFile, desFile)

numTrain = 20
numValid = 10

srcDir = 'RVESimulations'
destDirTrain = 'trainingPaths'
destDirValid = 'validationPaths'

os.system('rm -rf '+destDirTrain)
os.system('rm -rf '+destDirValid)

os.mkdir(destDirTrain)
os.mkdir(destDirValid)

bestPaths =[14, 27, 17, 20, 56, 85, 69, 60, 38, 12, 65, 99, 23, 48, 10, 8, 100, 76, 39, 80, 98, 51, 9, 24, 64, 32, 92, 45, 30, 50, 22, 47, 31, 90, 42, 94, 88, 7, 16, 43, 63, 54, 6, 77, 97, 25, 4, 34, 72, 66, 55, 81, 79, 15, 86, 2, 59, 87, 70, 58]

allPathBases=getFiles(srcDir,'strainPath')
allPaths = []
for p in allPathBases:
  if int(p[len('strainPath'):-4]) in bestPaths:
    allPaths.append(p)
print(allPaths)
random.shuffle(allPaths)
print(allPaths)

writeListToFile(allPaths[:numTrain],'infos-trainingPaths.txt')
writeListToFile(allPaths[numTrain:numTrain+numValid],'infos-validationPaths.txt')

getData(srcDir,destDirTrain,allPaths[:numTrain])
getData(srcDir,destDirValid,allPaths[numTrain:numTrain+numValid])
