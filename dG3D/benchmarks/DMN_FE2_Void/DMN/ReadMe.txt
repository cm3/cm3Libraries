In RVESimulations,
- run createPath.py to create strain paths
- run runPath.py to run virtual simulation
- run extract.py to collect the offline data

In current folder,
- run getOfflineData.py to get offline data

In offlineTraining,
- run training.py to train a pre-defined DMN

In RVEPrediction,
- make prediction of the trained DMN with a given strain path obtained from a RVE simulation
