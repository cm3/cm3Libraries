from gmshpy import *
from dG3Dpy import*
import matplotlib.pyplot as plt
import pandas as pd
import os
import numpy as np
from mpl_toolkits import mplot3d
import random

def lhssample(n,p):
  x = np.random.uniform(size=[n,p])
  for i in range(0,p):
      x[:,i] = (np.argsort(x[:,i])+0.5)/n
  return x

def makePath(R, phi1, phi2):
  x = R*np.cos(phi1)
  y = R*np.sin(phi1)*np.cos(phi2)
  z = R*np.sin(phi1)*np.sin(phi2)

  return x,y,z

TimeStep = 1. # second
EvalStep = 5e-3
Rmax = 0.2
Nmax =int(Rmax/EvalStep)+1

plt.close('all')

fig = plt.figure()
ax = plt.axes(projection='3d')


pdAll= pd.DataFrame()

maxS = 100
allPt = lhssample(maxS,2)
print(allPt)
ppp = pd.DataFrame(allPt,columns=['v'+str(i) for i in range(2)])
ppp.to_csv("allPt.csv",sep=';',index = False)
k = 0
for ss in range(maxS):
    k = ss+1
    phi1 = allPt[ss,0]*np.pi
    phi2 = allPt[ss,1]*2*np.pi
    print(f'make path {k} with phi1 = {phi1} and phi2 = {phi2}')
    epsXXmax, epsYYmax, epsXYmax =makePath(Rmax,phi1,phi2)
    print(epsXXmax, epsYYmax, epsXYmax)

    strainPath=[]
    for ip in range(0,Nmax+1):
      fact = float(ip)/Nmax
      strainPath.append([epsXXmax*fact, epsYYmax*fact, 0, epsXYmax*fact, 0, 0])


    df = pd.DataFrame(strainPath,columns=['U_XX','U_YY','U_ZZ','U_XY','U_XZ','U_YZ'])
    print(df.values[0,0])
    N = len(df)
    pathFile = PathSTensor3(N-1)
    for ii in range(1,N):
      pathFile.setVal(ii-1,0,0,df.values[ii,0]+1.)
      pathFile.setVal(ii-1,1,1,df.values[ii,1]+1.)
      pathFile.setVal(ii-1,2,2,df.values[ii,2]+1.)
      pathFile.setVal(ii-1,0,1,df.values[ii,3])
      pathFile.setVal(ii-1,1,0,df.values[ii,3])
      pathFile.setVal(ii-1,0,2,df.values[ii,4])
      pathFile.setVal(ii-1,2,0,df.values[ii,4])
      pathFile.setVal(ii-1,1,2,df.values[ii,5])
      pathFile.setVal(ii-1,2,1,df.values[ii,5])

    combineFileName = 'strainPath'+str(k)+'.csv'
    pathFile.toFile(combineFileName,N-1)
