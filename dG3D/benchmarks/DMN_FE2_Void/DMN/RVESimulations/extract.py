from gmshpy import *
from dG3Dpy import*
from math import*
import numpy as np
import pandas as pd
import os
from os.path import isfile, join
from os import listdir


def getVolumetricStrain(phys, fileOutput):
  FXX = pd.read_csv("E_0_GP_0_Average_"+str(phys)+"_F_XX.csv",sep=";",header=None)
  FXY = pd.read_csv("E_0_GP_0_Average_"+str(phys)+"_F_XY.csv",sep=";",header=None)
  FYX = pd.read_csv("E_0_GP_0_Average_"+str(phys)+"_F_YX.csv",sep=";",header=None)
  FYY = pd.read_csv("E_0_GP_0_Average_"+str(phys)+"_F_YY.csv",sep=";",header=None)
  r, c = FXX.shape
  dataPath = PathDouble(r)
  for i in range(1,r):
    volStrain = FXX.values[i,1] + FYY.values[i,1]-1.
    dataPath.setVal(i-1, volStrain)
  dataPath.toFile(fileOutput,r-1)

def convertToPathSTensor3(fileInput, fileOutput):
  data = pd.read_csv(fileInput,sep=";")
  r, c = data.shape
  dataPath = PathSTensor3(r)
  #print(data.values)
  # we start from first row only
  for i in range(1,r):
    for j in range(c):
      for ii in range(3):
        for jj in range(3):
          if ii == jj:
            dataPath.setVal(i-1, ii, jj, data.values[i,1+ii + 3*jj]+1.)
          else:
            dataPath.setVal(i-1, ii, jj, data.values[i,1+ii + 3*jj])
  dataPath.toFile(fileOutput,r-1)

def getFiles(folder, index):
  # get list of files using index
  # index == first strings in the name of files
  onlyfiles = [f for f in listdir(folder) if isfile(join(folder, f))]
  allFiles = list()
  for fname in onlyfiles:
    if fname[:len(index)]==index:
      allFiles.append(fname)
  return allFiles

allPaths=getFiles('.','strainPath')
print(allPaths)
for strainPath in allPaths:
  print("running : ",strainPath)
  saveDir =strainPath[:-4]
  os.chdir(saveDir)
  convertToPathSTensor3("E_0_GP_0_strain.csv","FPath.csv")
  convertToPathSTensor3("E_0_GP_0_stress.csv","PPath.csv")
  getVolumetricStrain(11,"VolumetricStrain11Path.csv")
  os.chdir('..')
