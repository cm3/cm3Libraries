from gmshpy import *
from dG3Dpy import*
from math import*
import numpy as np
import pandas as pd
import os
from os.path import isfile, join
from os import listdir

def makeTest(dataFile, header = None):
  print('start running: '+dataFile)
  data = pd.read_csv(dataFile,sep=" ",header=header)
  strainPath = data.values
  N = len(strainPath)
  print('strainPath',strainPath)

  lawnum1 = 11 # unique number of law
  E = 3E3 #MPa
  nu = 0.3
  K = E/3./(1.-2.*nu)	# Bulk mudulus
  mu =E/2./(1.+nu)	  # Shear mudulus
  rho = 7850e-9 # Bulk mass
  sy0 = 100. #MPa
  h = E/50.
  harden1 = LinearExponentialJ2IsotropicHardening(lawnum1, sy0,h,0.,10.)
  smallStrain=True
  if smallStrain:
    law1   = J2SmallStrainDG3DMaterialLaw(lawnum1,rho,E,nu,harden1)
  else:
    law1   = J2LinearDG3DMaterialLaw(lawnum1,rho,E,nu,harden1)
    law1.setStrainOrder(11)

  # geometry
  meshfile="micro.msh" # name of mesh file

  myfield1 = dG3DDomain(1000,11,0,lawnum1,0,2)

  # solver
  sol = 2  # Gmm=0 (default) Taucs=1 PETsc=2
  soltype =1 # StaticLinear=0 (default) StaticNonLinear=1
  nstep = N  # number of step (used only if soltype=1)
  ftime =N  # Final time (used only if soltype=1)
  tol=1.e-5  # relative tolerance for NR scheme (used only if soltype=1)
  nstepArch=1 # Number of step between 2 archiving (used only if soltype=1)

  # creation of Solver
  mysolver = nonLinearMechSolver(1000)
  mysolver.loadModel(meshfile)
  mysolver.addDomain(myfield1)
  mysolver.addMaterialLaw(law1)
  mysolver.Scheme(soltype)
  mysolver.Solver(sol)
  mysolver.snlData(nstep,ftime,tol)
  mysolver.stepBetweenArchiving(nstepArch)

  mysolver.snlManageTimeStep(10,5,2.,10)

  system = 1   # Displacement elimination =0 Multiplier elimination = 1 Displacement+ multiplier = 2
  control = 0     # load control = 0 arc length control euler = 1
  mysolver.setSystemType(system)
  mysolver.setControlType(control)

  mysolver.setMessageView(True)

  mysolver.displacementBC('Face',11,2,0.)

  microBC = nonLinearPeriodicBC(10,2)
  microBC.setBCPhysical(1,2,3,4)
  method =5	# Periodic mesh = 0
  degree = 3 # Order used for polynomial interpolation
  addvertex = 0  # Polynomial interpolation by mesh vertex = 0, Polynomial interpolation by virtual vertex
  microBC.setPeriodicBCOptions(method, degree, bool(addvertex))

  microBC.setDeformationGradient(2.,1.,1.,1.,2.,1.,1.,1.,2.)
  fctxx = piecewiseScalarFunction()
  fctyx = piecewiseScalarFunction()
  fctzx = piecewiseScalarFunction()
  fctxy = piecewiseScalarFunction()
  fctyy = piecewiseScalarFunction()
  fctzy = piecewiseScalarFunction()
  fctxz = piecewiseScalarFunction()
  fctyz = piecewiseScalarFunction()
  fctzz = piecewiseScalarFunction()

  #initial step
  fctxx.put(0,0.)
  fctyx.put(0,0.)
  fctzx.put(0,0.)
  fctxy.put(0,0.)
  fctyy.put(0,0.)
  fctzy.put(0,0.)
  fctxz.put(0,0.)
  fctyz.put(0,0.)
  fctzz.put(0,0.)

  # all next step from file
  for i in range(0,N):
    print(f"row {i} : ", strainPath[i,:])
    fctxx.put(i+1,strainPath[i,0]-1.)
    fctyx.put(i+1,strainPath[i,1])
    fctzx.put(i+1,strainPath[i,2])
    fctxy.put(i+1,strainPath[i,3])
    fctyy.put(i+1,strainPath[i,4]-1.)
    fctzy.put(i+1,strainPath[i,5])
    fctxz.put(i+1,strainPath[i,6])
    fctyz.put(i+1,strainPath[i,7])
    fctzz.put(i+1,strainPath[i,8]-1.)

  microBC.setPathFunctionDeformationGradient(0,0,fctxx)
  microBC.setPathFunctionDeformationGradient(1,0,fctyx)
  microBC.setPathFunctionDeformationGradient(2,0,fctzx)
  microBC.setPathFunctionDeformationGradient(0,1,fctxy)
  microBC.setPathFunctionDeformationGradient(1,1,fctyy)
  microBC.setPathFunctionDeformationGradient(2,1,fctzy)
  microBC.setPathFunctionDeformationGradient(0,2,fctxz)
  microBC.setPathFunctionDeformationGradient(1,2,fctyz)
  microBC.setPathFunctionDeformationGradient(2,2,fctzz)

  mysolver.addMicroBC(microBC)

  mysolver.stressAveragingFlag(True) # set stress averaging ON- 0 , OFF-1
  mysolver.setStressAveragingMethod(0) # 0 -volume 1- surface
  #tangent averaging flag
  mysolver.tangentAveragingFlag(False) # set tangent averaging ON -0, OFF -1 #** If turned off, Elast Tensor will not be computed
  mysolver.setTangentAveragingMethod(2,1e-6) # 0- perturbation 1- condensation

  # build view
  mysolver.internalPointBuildView("Strain_xx",IPField.STRAIN_XX, 1, 1);
  mysolver.internalPointBuildView("Strain_yy",IPField.STRAIN_YY, 1, 1);
  mysolver.internalPointBuildView("Strain_zz",IPField.STRAIN_ZZ, 1, 1);
  mysolver.internalPointBuildView("Strain_xy",IPField.STRAIN_XY, 1, 1);
  mysolver.internalPointBuildView("Strain_yz",IPField.STRAIN_YZ, 1, 1);
  mysolver.internalPointBuildView("Strain_xz",IPField.STRAIN_XZ, 1, 1);
  mysolver.internalPointBuildView("sig_xx",IPField.SIG_XX, 1, 1);
  mysolver.internalPointBuildView("sig_yy",IPField.SIG_YY, 1, 1);
  mysolver.internalPointBuildView("sig_zz",IPField.SIG_ZZ, 1, 1);
  mysolver.internalPointBuildView("sig_xy",IPField.SIG_XY, 1, 1);
  mysolver.internalPointBuildView("sig_yz",IPField.SIG_YZ, 1, 1);
  mysolver.internalPointBuildView("sig_xz",IPField.SIG_XZ, 1, 1);
  mysolver.internalPointBuildView("sig_VM",IPField.SVM, 1, 1);
  mysolver.internalPointBuildView("Equivalent plastic strain",IPField.PLASTICSTRAIN, 1, 1);
  mysolver.internalPointBuildView("pression",IPField.PRESSION,1,1)
  mysolver.internalPointBuildView("plastic possion ratio",IPField.PLASTIC_POISSON_RATIO,1,1)

  mysolver.internalPointBuildView("ISO_YIELD",IPField.ISO_YIELD,1,1)

  mysolver.internalPointBuildView("Ee_ZZ",IPField.Ee_ZZ,1,1)
  mysolver.internalPointBuildView("Ee_YY",IPField.Ee_YY,1,1)
  mysolver.internalPointBuildView("Ee_XX",IPField.Ee_XX,1,1)

  mysolver.internalPointBuildView("Eve1_ZZ",IPField.Eve1_ZZ,1,1)
  mysolver.internalPointBuildView("Eve1_YY",IPField.Eve1_YY,1,1)
  mysolver.internalPointBuildView("Eve1_XX",IPField.Eve1_XX,1,1)

  Fvars = [IPField.JACOBIAN, IPField.F_XX,IPField.F_XY, IPField.F_XZ,
           IPField.F_YX, IPField.F_YY, IPField.F_YZ,
           IPField.F_ZX, IPField.F_ZY, IPField.F_ZZ,
           IPField.PLASTICSTRAIN,IPField.ISO_YIELD, IPField.PLASTIC_ENERGY, IPField.DEFO_ENERGY,
           IPField.FP_XX,IPField.FP_XY,IPField.FP_XZ,
           IPField.FP_YX, IPField.FP_YY, IPField.FP_YZ,
           IPField.FP_ZX, IPField.FP_ZY, IPField.FP_ZZ]
  for i in Fvars:
    mysolver.archivingAverageValueOnPhysical(11,i)

  # solve
  mysolver.solve()

def getFiles(folder, index):
  # get list of files using index
  # index == first strings in the name of files
  onlyfiles = [f for f in listdir(folder) if isfile(join(folder, f))]
  allFiles = list()
  for fname in onlyfiles:
    if fname[:len(index)]==index:
      allFiles.append(fname)
  return allFiles

allPaths=getFiles('.','strainPath')
print(allPaths)
for strainPath in allPaths:
  print("running : ",strainPath)
  saveDir =strainPath[:-4]
  print("save directory: ",saveDir)
  if not(os.path.exists(saveDir)):
    os.mkdir(saveDir)
  os.system("cp micro.msh "+strainPath+" "+saveDir)
  os.chdir(saveDir)
  makeTest(strainPath)
  os.chdir('..')
