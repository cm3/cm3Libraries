from gmshpy import *
from dG3Dpy import*
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
from os.path import isfile, join
from os import listdir

# material law
lawnum1 = 11 # unique number of law
E = 3E3 #MPa
nu = 0.3
K = E/3./(1.-2.*nu)	# Bulk mudulus
mu =E/2./(1.+nu)	  # Shear mudulus
rho = 7850e-9 # Bulk mass
sy0 = 100. #MPa
h = E/50.
harden1 = LinearExponentialJ2IsotropicHardening(lawnum1, sy0,h,0.,10.)
smallStrain=True
if smallStrain:
  law1   = J2SmallStrainDG3DMaterialLaw(lawnum1,rho,E,nu,harden1)
else:
  law1   = J2LinearDG3DMaterialLaw(lawnum1,rho,E,nu,harden1)
  law1.setStrainOrder(11)

f0 = np.pi*0.2*0.2
# material network
dmn = dG3DDeepMaterialNetwork()
dmn.addLaw(0,law1)
#dmn.addLaw(1,law2)
dmn.fixDofsByComp(2)
dmn.viewIP(IPField.DAMAGE)
dmn.viewIP(IPField.PLASTICSTRAIN)
dmn.viewIPAverage(IPField.F_XX)
dmn.viewIPAverage(IPField.F_XY)
dmn.viewIPAverage(IPField.F_YX)
dmn.viewIPAverage(IPField.F_YY)
dmn.viewIPAveragePerPhase(0,IPField.F_XX)
dmn.viewIPAveragePerPhase(0,IPField.F_XY)
dmn.viewIPAveragePerPhase(0,IPField.F_YX)
dmn.viewIPAveragePerPhase(0,IPField.F_YY)
dmn.viewIPAveragePerPhase(1,IPField.F_XX)
dmn.viewIPAveragePerPhase(1,IPField.F_XY)
dmn.viewIPAveragePerPhase(1,IPField.F_YX)
dmn.viewIPAveragePerPhase(1,IPField.F_YY)

dmn.viewIPAveragePerPhase(0,IPField.VOLUMETRIC_STRAIN)
dmn.viewIPAveragePerPhase(1,IPField.VOLUMETRIC_STRAIN)
dmn.viewIPAveragePerPhase(0,IPField.JACOBIAN)
dmn.viewIPAveragePerPhase(1,IPField.JACOBIAN)
dmn.addInteractionFromFile("../../offlineTraining/best_tree_trained.txt")
dmn.printPhaseFraction()

# offline training
reduction = CoefficientReductionFullCoefficient()
offTraining = TrainingDeepMaterialNetworkNonLinear(dmn,reduction)
offTraining.setMaximumStrainStep(1e-2)

def getHomogenizedTrace(homoStrainFile):
  Fbar = pd.read_csv(homoStrainFile,sep=" ",header=None)
  r, c = Fbar.shape

  Jhomo = np.zeros(r)
  for j in range(0,r):
    vals = Fbar.values[j,:]
    Jhomo[j] = vals[0]+vals[4]+vals[8]-2
  return Jhomo

def getLocalTrace(homoStrainFile):
  FSolid = pd.read_csv(homoStrainFile, header=None)
  r, c = FSolid.values.shape
  return FSolid.values.reshape(r)


#
def getDet(vals):
  # Comp.00;Comp.10;Comp.20;Comp.01;Comp.11;Comp.21;Comp.02;Comp.12;Comp.22
  a = np.zeros((3,3))
  #print(vals)
  a[0,0] = vals[0]+1.
  a[1,0] = vals[1]
  a[2,0] = vals[2]
  a[0,1] = vals[3]
  a[1,1] = vals[4]+1.
  a[2,1] = vals[5]
  a[0,2] = vals[6]
  a[1,2] = vals[7]
  a[2,2] = vals[8]+1.
  return np.linalg.det(a)

def getHomogenizedJacobian(homoStrainFile):
  Fbar = pd.read_csv(homoStrainFile,sep =" ", header=None)
  print(Fbar)
  r, c = Fbar.shape
  Jhomo = np.zeros(r)
  for j in range(0,r):
    Jhomo[j] = getDet(Fbar.values[j,:])
  return Jhomo

def graphDataPorosity(srcFolder):
  Jhomo = getHomogenizedJacobian(srcFolder+"/FPath.csv")
  traceHomo = getHomogenizedTrace(srcFolder+"/FPath.csv")
  traceavg = getLocalTrace(srcFolder+"/VolumetricStrain11Path.csv")
  traceDMN =pd.read_csv("VOLUMETRIC_STRAIN_Phase0_Mean.csv",sep=" ")
  print(len(traceavg))
  print(len(traceHomo))
  fTrace = 1- ((0+traceavg)/(traceHomo))*(1-f0)
  fDMNTrace = 1- ((0+traceDMN.values[:,1])/(traceHomo))*(1-f0)

  df = pd.DataFrame()
  df['DNS']= fTrace
  df['MN']= fDMNTrace
  df['trE']=traceHomo
  df.to_csv("porosity.csv",sep=';',index = False)

  plt.figure()
  plt.plot(Jhomo,fTrace/f0 ,'go',label='DNS - Trace')
  plt.plot(Jhomo,fDMNTrace/f0 ,'g--',label='Material network - Trace')
  plt.ylabel(r'$f/f_0$')
  plt.xlabel(r'$\bar{J}$')
  plt.legend(loc='upper left')
  plt.savefig("porosity.png",bbox_inches = "tight")

# training data

def graphData(srcFolder):
  Fref = PathSTensor3()
  Fref.fromFile(srcFolder+"/FPath.csv")
  stressPath = PathSTensor3()
  nb = offTraining.evaluatePath(Fref,stressPath,False,False)
  stressPath.toFile("Pcur.csv",nb+1)
  P = pd.read_csv("Pcur.csv",sep =" ", header=None)
  F = pd.read_csv(srcFolder+"/FPath.csv",sep =" ", header=None)
  Jhomo = getHomogenizedJacobian(srcFolder+"/FPath.csv")
  Pref = pd.read_csv(srcFolder+"/PPath.csv",sep =" ", header=None)
  print(f'leb Jhomo= {len(Jhomo)}')
  print(Pref.shape)
  fig, axes = plt.subplots(3, 3, sharex=True)
  for i in range(3):
    for j in range(3):
      index = i+3*j
      axes[i, j].plot(Pref.values[:,index],'r--',label=f"REF - comp. {i}{j}")
      axes[i, j].plot(P.values[:,index],'g-',label=f"DMN -comp. {i}{j}")

  axes[0, 2].legend(loc="lower right")

  plt.savefig("stressStrin.png",bbox_inches = "tight")

def getFiles(folder, index):
  # get list of files using index
  # index == first strings in the name of files
  onlyfiles = [f for f in listdir(folder) if isfile(join(folder, f))]
  allFiles = list()
  for fname in onlyfiles:
    if fname[:len(index)]==index:
      allFiles.append(fname)
  return allFiles



plt.close('all')
srcFolder ='../US2-REF'
graphData(srcFolder)
graphDataPorosity(srcFolder)
plt.show()
