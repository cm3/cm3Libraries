from gmshpy import *
from dG3Dpy import*
from math import*
import numpy as np
import pandas as pd
import os
from os.path import isfile, join
from os import listdir

def makeTest(header = None):
  lawnum1 = 11 # unique number of law
  E = 3E3 #MPa
  nu = 0.3
  K = E/3./(1.-2.*nu)	# Bulk mudulus
  mu =E/2./(1.+nu)	  # Shear mudulus
  rho = 7850e-9 # Bulk mass
  sy0 = 100. #MPa
  h = E/50.
  harden1 = LinearExponentialJ2IsotropicHardening(lawnum1, sy0,h,0.,10.)
  smallStrain=True
  if smallStrain:
    law1   = J2SmallStrainDG3DMaterialLaw(lawnum1,rho,E,nu,harden1)
  else:
    law1   = J2LinearDG3DMaterialLaw(lawnum1,rho,E,nu,harden1)
    law1.setStrainOrder(11)

  # geometry
  meshfile="micro.msh" # name of mesh file

  myfield1 = dG3DDomain(1000,11,0,lawnum1,0,2)

  # solver
  sol = 2  # Gmm=0 (default) Taucs=1 PETsc=2
  soltype =1 # StaticLinear=0 (default) StaticNonLinear=1
  nstep = 2000  # number of step (used only if soltype=1)
  ftime =1  # Final time (used only if soltype=1)
  tol=1.e-5  # relative tolerance for NR scheme (used only if soltype=1)
  nstepArch=1 # Number of step between 2 archiving (used only if soltype=1)

  # creation of Solver
  mysolver = nonLinearMechSolver(1000)
  mysolver.loadModel(meshfile)
  mysolver.addDomain(myfield1)
  mysolver.addMaterialLaw(law1)
  mysolver.Scheme(soltype)
  mysolver.Solver(sol)
  mysolver.snlData(nstep,ftime,tol)
  mysolver.stepBetweenArchiving(nstepArch)

  mysolver.snlManageTimeStep(10,3,2.,10)

  mysolver.displacementBC('Face',11,2,0.)

  microBC = nonLinearPeriodicBC(10,2)
  microBC.setBCPhysical(1,2,3,4)
  method =5	# Periodic mesh = 0
  degree = 3 # Order used for polynomial interpolation
  addvertex = 0  # Polynomial interpolation by mesh vertex = 0, Polynomial interpolation by virtual vertex
  microBC.setPeriodicBCOptions(method, degree, bool(addvertex))

  mysolver.addMicroBC(microBC)

  mysolver.activateTest(True)
  mysolver.setRVEVolume(1.)

  # root Vertex always fix
  mysolver.displacementBC("Node",1,0,0.)
  mysolver.displacementBC("Node",1,1,0.)
  mysolver.displacementBC("Node",2,1,0.)
  #to generate pur unixial deformed shape

  fct = PiecewiseLinearFunction()
  fct.put(0.,0.)
  fct.put(0.25,0.15)
  fct.put(0.5,-0.15)
  fct.put(0.75,0.3)
  fct.put(1.,0.0)
  mysolver.setNumStepTimeInterval(0.25,400)
  mysolver.setNumStepTimeInterval(0.5,100)
  mysolver.setNumStepTimeInterval(0.75,1000)
  mysolver.setNumStepTimeInterval(1.,500)

  mysolver.displacementBC("Node",2,0,fct)

  # build view
  mysolver.internalPointBuildView("Strain_xx",IPField.STRAIN_XX, 1, 1);
  mysolver.internalPointBuildView("Strain_yy",IPField.STRAIN_YY, 1, 1);
  mysolver.internalPointBuildView("Strain_zz",IPField.STRAIN_ZZ, 1, 1);
  mysolver.internalPointBuildView("Strain_xy",IPField.STRAIN_XY, 1, 1);
  mysolver.internalPointBuildView("Strain_yz",IPField.STRAIN_YZ, 1, 1);
  mysolver.internalPointBuildView("Strain_xz",IPField.STRAIN_XZ, 1, 1);
  mysolver.internalPointBuildView("sig_xx",IPField.SIG_XX, 1, 1);
  mysolver.internalPointBuildView("sig_yy",IPField.SIG_YY, 1, 1);
  mysolver.internalPointBuildView("sig_zz",IPField.SIG_ZZ, 1, 1);
  mysolver.internalPointBuildView("sig_xy",IPField.SIG_XY, 1, 1);
  mysolver.internalPointBuildView("sig_yz",IPField.SIG_YZ, 1, 1);
  mysolver.internalPointBuildView("sig_xz",IPField.SIG_XZ, 1, 1);
  mysolver.internalPointBuildView("sig_VM",IPField.SVM, 1, 1);
  mysolver.internalPointBuildView("Equivalent plastic strain",IPField.PLASTICSTRAIN, 1, 1);
  mysolver.internalPointBuildView("pression",IPField.PRESSION,1,1)
  mysolver.internalPointBuildView("plastic possion ratio",IPField.PLASTIC_POISSON_RATIO,1,1)

  mysolver.internalPointBuildView("ISO_YIELD",IPField.ISO_YIELD,1,1)

  mysolver.internalPointBuildView("Ee_ZZ",IPField.Ee_ZZ,1,1)
  mysolver.internalPointBuildView("Ee_YY",IPField.Ee_YY,1,1)
  mysolver.internalPointBuildView("Ee_XX",IPField.Ee_XX,1,1)

  mysolver.internalPointBuildView("Eve1_ZZ",IPField.Eve1_ZZ,1,1)
  mysolver.internalPointBuildView("Eve1_YY",IPField.Eve1_YY,1,1)
  mysolver.internalPointBuildView("Eve1_XX",IPField.Eve1_XX,1,1)

  Fvars = [IPField.JACOBIAN, IPField.F_XX,IPField.F_XY, IPField.F_XZ, IPField.F_YX,IPField.F_YY,
           IPField.F_YZ, IPField.F_ZX,IPField.F_ZY, IPField.F_ZZ, IPField.PLASTICSTRAIN,IPField.ISO_YIELD,IPField.PLASTIC_ENERGY,
           IPField.P_XX,IPField.P_XY, IPField.P_XZ, IPField.P_YX,IPField.P_YY, IPField.P_YZ, IPField.P_ZX,IPField.P_ZY, IPField.P_ZZ,]
  for i in Fvars:
    mysolver.archivingAverageValueOnPhysical(11,i)

  Fvars = [IPField.P_XX,IPField.P_XY, IPField.P_XZ, IPField.P_YX,IPField.P_YY, IPField.P_YZ, IPField.P_ZX,IPField.P_ZY, IPField.P_ZZ,
           IPField.F_XX,IPField.F_XY, IPField.F_XZ, IPField.F_YX,IPField.F_YY, IPField.F_YZ, IPField.F_ZX,IPField.F_ZY, IPField.F_ZZ,]
  for i in Fvars:
    mysolver.archivingAverageValue(i)

  mysolver.archivingNodeDisplacement(2,0)
  mysolver.archivingNodeDisplacement(2,1)
  mysolver.archivingNodeDisplacement(4,0)
  mysolver.archivingNodeDisplacement(4,1)
  # solve
  a = mysolver.solve()
  return a



makeTest()

