from gmshpy import *
from dG3Dpy import*
from math import*
import numpy as np
import pandas as pd
import os
from os.path import isfile, join
from os import listdir

fv = np.pi*0.2*0.2
fs = 1-fv

def getStresssHomo(fileOutput):
  PXX = pd.read_csv("Average_P_XX.csv",sep=";",header=None)
  PXY = pd.read_csv("Average_P_XY.csv",sep=";",header=None)
  PXZ = pd.read_csv("Average_P_XZ.csv",sep=";",header=None)
  PYX = pd.read_csv("Average_P_YX.csv",sep=";",header=None)
  PYY = pd.read_csv("Average_P_YY.csv",sep=";",header=None)
  PYZ = pd.read_csv("Average_P_YZ.csv",sep=";",header=None)
  PZX = pd.read_csv("Average_P_ZX.csv",sep=";",header=None)
  PZY = pd.read_csv("Average_P_ZY.csv",sep=";",header=None)
  PZZ = pd.read_csv("Average_P_ZZ.csv",sep=";",header=None)
  r, c = PXX.shape
  dataPath = PathSTensor3(r)
  for i in range(r):
    for ii in range(3):
      for jj in range(3):
        if ii == 0 and jj==0:
          dataPath.setVal(i, ii, jj, fs*PXX.values[i,1])
        elif ii == 1 and jj==0:
          dataPath.setVal(i, ii, jj, fs*PYX.values[i,1])
        elif ii == 2 and jj==0:
          dataPath.setVal(i, ii, jj, fs*PZX.values[i,1])
        elif ii == 0 and jj==1:
          dataPath.setVal(i, ii, jj, fs*PXY.values[i,1])
        elif ii == 1 and jj==1:
          dataPath.setVal(i, ii, jj, fs*PYY.values[i,1])
        elif ii == 2 and jj==1:
          dataPath.setVal(i, ii, jj, fs*PZY.values[i,1])
        elif ii == 0 and jj==2:
          dataPath.setVal(i, ii, jj, fs*PXZ.values[i,1])
        elif ii == 1 and jj==2:
          dataPath.setVal(i, ii, jj, fs*PYZ.values[i,1])
        elif ii == 2 and jj==2:
          dataPath.setVal(i, ii, jj, fs*PZZ.values[i,1])
  dataPath.toFile(fileOutput,r)


def getStrainHomo2D(fileOutput):
  u2X = pd.read_csv("NodalDisplacementPhysical2Num2comp0.csv",sep=";",header=None)
  u4Y = pd.read_csv("NodalDisplacementPhysical4Num4comp1.csv",sep=";",header=None)
  print(u2X)
  print(u4Y)
  r, c = u2X.shape
  dataPath = PathSTensor3(r)
  for i in range(r):
    for ii in range(3):
      for jj in range(3):
        if ii == 0 and jj==0:
          dataPath.setVal(i, ii, jj, 1+u2X.values[i,1])
        elif ii == 1 and jj==1:
          dataPath.setVal(i, ii, jj, 1+u4Y.values[i,1])
        elif ii == 2 and jj==2:
          dataPath.setVal(i, ii, jj, 1)
  dataPath.toFile(fileOutput,r)


def getVolumetricStrain(phys, fileOutput):
  FXX = pd.read_csv("Average_"+str(phys)+"_F_XX.csv",sep=";",header=None)
  FYY = pd.read_csv("Average_"+str(phys)+"_F_YY.csv",sep=";",header=None)
  FZZ = pd.read_csv("Average_"+str(phys)+"_F_ZZ.csv",sep=";",header=None)
  r, c = FXX.shape
  dataPath = PathDouble(r)
  for i in range(r):
    volStrain = FXX.values[i,1] + FYY.values[i,1]+FZZ.values[i,1]-2.
    dataPath.setVal(i, volStrain)
  dataPath.toFile(fileOutput,r)

def getFiles(folder, index):
  # get list of files using index
  # index == first strings in the name of files
  onlyfiles = [f for f in listdir(folder) if isfile(join(folder, f))]
  allFiles = list()
  for fname in onlyfiles:
    if fname[:len(index)]==index:
      allFiles.append(fname)
  return allFiles


print('get homogenized strain')
getStrainHomo2D("FPath.csv")
print('get homogenized stress')
getStresssHomo("PPath.csv")
getVolumetricStrain(11,"VolumetricStrain11Path.csv")
