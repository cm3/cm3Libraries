// Test case a SCB with a vertical load at its free extremity
// Size

//definition of unit
mm = 1e-03;

dx=1.*mm;
dz=0.5*mm;

// Characteristic length
Lc1=1.*mm;
x={-dx/2.,dx/2.,-dx/2.+0.03*mm,dx/2.-0.03*mm};
z={0.,dz,dz+0.01*mm,2.*dz+0.01*mm};
// definition of points
For index In {0:1}
  x1=x[0+2*index];
  x2=x[1+2*index];
  z1=z[0+2*index];
  z2=z[1+2*index];
  Point(100*index+1) = { x1 , z1, 0., Lc1};
  Point(100*index+2) = {  x2 , z1 , 0.,  Lc1};
  Point(100*index+5) = { x1 , z2 , 0., Lc1};
  Point(100*index+6) = {  x2  , z2 , 0., Lc1};

  // Line between points
  Line(100*index+1) = {100*index+1,100*index+2};
  Line(100*index+5) = {100*index+5,100*index+6};
  Line(100*index+9) = {100*index+1,100*index+5};
  Line(100*index+10)= {100*index+2,100*index+6};

  // Surface definition
  Line Loop(100*index+3) = {100*index+1,100*index+10,-100*index-5,-100*index-9};

  Plane Surface(100*index+3) = {100*index+3};

  Physical Surface(100*index+1265) = {100*index+3};

  Physical Line(100*index+12) = {100*index+1};
  Physical Line(100*index+56) = {100*index+5};
  Physical Line(100*index+15) = {100*index+9};
  Physical Line(100*index+26) = {100*index+10};

  Physical Point(100*index+1) ={100*index+1};
  Physical Point(100*index+2) ={100*index+2};
  Physical Point(100*index+5) ={100*index+5};
  Physical Point(100*index+6) ={100*index+6};

  // define transfinite mesh
  nb=6;
  If(index==1)
    nb=5;
  EndIf
  Transfinite Line {100*index+1,100*index+5} = nb;
  Transfinite Line {100*index+9,100*index+10} = 4;
  Transfinite Surface {100*index+3} ;
  Recombine Surface {100*index+3} ;
EndFor
