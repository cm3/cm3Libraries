SetFactory('OpenCASCADE');

lx = 20;
ly = 20;
lz = 20;

nx = 1;
ny = 1;
nz = 1;

//nx11 = 2;
//nx12 = 2;
//nx13 = 2;

//nz11 = 1;

toll = (lx/nx)/2;
e = 2;
Point(1)={0, 0, 0, e};
Point(2)={lx/nx, 0, 0, e};
Point(3)={lx/nx, ly/ny, 0, e};
Point(4)={0, ly/ny, 0, e};


Line(1)={1, 2};
Line(2)={2, 3};
Line(3)={3, 4};
Line(4)={4, 1};


Line Loop(100)={1, 2, 3, 4};
Surface(100)={100};

Extrude {0, 0, lz/nz} {
  Surface{100}; Layers{nz}; Recombine;
}



Transfinite Line{1,3}=nx+1 Using Progression 1;
Transfinite Line{2,4}=ny+1 Using Progression 1;

Transfinite Surface {100};
Recombine Surface{100};
Recombine Volume{1};


Physical Surface(551) = {101};
Physical Surface(552) = {102};
Physical Surface(553) = {103};
Physical Surface(554) = {104};
Physical Surface(555) = {100};
Physical Surface(556) = {105};

Physical Volume(11)={1};
