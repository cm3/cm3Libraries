#coding-Utf-8-*-
from gmshpy import *
from dG3Dpy import*

#script to launch beam problem with a python script

# material law
lawnum = 1 # unique number of law
rho   = 7850
young = 28.9e9
nu    = 0.3 
sy0   = 150e6
h     = 73e6
hexp  = 60.


# geometry
geofile="twoHole.geo"
meshfile="twoHole.msh" # name of mesh file

# solver
sol = 2  # Gmm=0 (default) Taucs=1 PETsc=2
soltype = 1 # StaticLinear=0 (default) StaticNonLinear=1
nstep = 200   # number of step (used only if soltype=1)
ftime =1.   # Final time (used only if soltype=1)
tol=1.e-5   # relative tolerance for NR scheme (used only if soltype=1)
nstepArch=1 # Number of step between 2 archiving (used only if soltype=1)
fullDg = 1 #O = CG, 1 = DG
space1 = 0 # function space (Lagrange=0)
beta1  = 20

#lemaitre chaboch
alpha = 1.e5
n     = 0.5           
p0    = 0.00
pc    = 10       
  

#  compute solution and BC (given directly to the solver
harden = ExponentialJ2IsotropicHardening(1,sy0, h, hexp)
cl     = IsotropicCLengthLaw(1, 2e-6)
damlaw = LemaitreChabocheDamageLaw(1, p0, pc, n, alpha)
law1   = NonLocalDamageJ2HyperDG3DMaterialLaw(lawnum,rho,young,nu,harden,cl,damlaw)

# cohesive law


# creation of ElasticField
nfield = 11 # number of the field (physical number of surface)
myfield1 = dG3DDomain(1000,nfield,space1,lawnum,fullDg,2,1)
#myfield1.matrixByPerturbation(1,1,1,1e-8)
myfield1.stabilityParameters(beta1)
myfield1.setNonLocalStabilityParameters(beta1,True)
myfield1.setNonLocalEqRatio(1.e6)
myfield1.averageStrainBased(False)
myfield1.setPlaneStressState(True)

# creation of Solver
mysolver = nonLinearMechSolver(1000)
#mysolver.createModel(geofile,meshfile,3,2)
mysolver.loadModel(meshfile)
mysolver.addDomain(myfield1)
mysolver.addMaterialLaw(law1)
mysolver.Scheme(soltype)
mysolver.Solver(sol)
mysolver.snlData(nstep,ftime,tol)
mysolver.stepBetweenArchiving(nstepArch)

# BC

mysolver.displacementBC("Face",11,2,0.)
mysolver.displacementBC("Edge",1,1,0.)
mysolver.displacementBC("Edge",4,0,0.)
mysolver.displacementBC("Edge",3,1,1e-3)

mysolver.internalPointBuildView("F_xx",IPField.F_XX, 1, 1)
mysolver.internalPointBuildView("F_yy",IPField.F_YY, 1, 1)
mysolver.internalPointBuildView("F_zz",IPField.F_ZZ, 1, 1)
mysolver.internalPointBuildView("F_xy",IPField.F_XY, 1, 1)
mysolver.internalPointBuildView("F_yx",IPField.F_YX, 1, 1)
mysolver.internalPointBuildView("F_yz",IPField.F_YZ, 1, 1)
mysolver.internalPointBuildView("F_yz",IPField.F_ZY, 1, 1)
mysolver.internalPointBuildView("F_xz",IPField.F_XZ, 1, 1)
mysolver.internalPointBuildView("F_zx",IPField.F_ZX, 1, 1)

mysolver.internalPointBuildView("P_xx",IPField.P_XX, 1, 1)
mysolver.internalPointBuildView("P_yy",IPField.P_YY, 1, 1)
mysolver.internalPointBuildView("P_zz",IPField.P_ZZ, 1, 1)
mysolver.internalPointBuildView("P_xy",IPField.P_XY, 1, 1)
mysolver.internalPointBuildView("P_yx",IPField.P_YX, 1, 1)
mysolver.internalPointBuildView("P_yz",IPField.P_YZ, 1, 1)
mysolver.internalPointBuildView("P_yz",IPField.P_ZY, 1, 1)
mysolver.internalPointBuildView("P_xz",IPField.P_XZ, 1, 1)
mysolver.internalPointBuildView("P_zx",IPField.P_ZX, 1, 1)


mysolver.internalPointBuildView("eq strain",IPField.GL_EQUIVALENT_STRAIN, 1, 1)
mysolver.internalPointBuildView("svm",IPField.SVM, 1, 1)
mysolver.internalPointBuildView("sig_xx",IPField.SIG_XX, 1, 1)
mysolver.internalPointBuildView("sig_yy",IPField.SIG_YY, 1, 1)
mysolver.internalPointBuildView("sig_zz",IPField.SIG_ZZ, 1, 1)
mysolver.internalPointBuildView("sig_xy",IPField.SIG_XY, 1, 1)
mysolver.internalPointBuildView("sig_yz",IPField.SIG_YZ, 1, 1)
mysolver.internalPointBuildView("sig_xz",IPField.SIG_XZ, 1, 1)
mysolver.internalPointBuildView("epl",IPField.PLASTICSTRAIN, 1, 1)
mysolver.internalPointBuildView("Damage",IPField.DAMAGE, 1, 1)

mysolver.archivingForceOnPhysicalGroup("Edge", 1, 1)
mysolver.archivingNodeDisplacement(4,1,1)

mysolver.solve()

check = TestCheck()
check.equal(-1.715290e+06,mysolver.getArchivedForceOnPhysicalGroup("Edge", 1, 1),1.e-4)


