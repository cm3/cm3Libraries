#coding-Utf-8-*-
from gmshpy import *
from dG3Dpy import*

#script to launch beam problem with a python script

# material law
lawnumEP = 2 
rhoEP   = 1000.
youngEP = 32e9
nuEP    = 0.3
sy0EP   = 25.e16
hEP     = 7.1019

lawcnumInt = 3
Gc   = 600.
GcII = 1200.
sigmac   = 20.e6
sigmacII = 20.e6
alphaGc = 1.
fsmin = 1.
fsmax = 1.
Kp = 1e15

# geometry
geofile="delam.geo"
meshfile="delam.msh" # name of mesh file

# solver
sol = 2  # Gmm=0 (default) Taucs=1 PETsc=2
soltype = 1 # StaticLinear=0 (default) StaticNonLinear=1
nstep = 200   # number of step (used only if soltype=1)
ftime =1 # Final time (used only if soltype=1)
tol=1.e-4   # relative tolerance for NR scheme (used only if soltype=1)
nstepArch=10 # Number of step between 2 archiving (used only if soltype=1)
fullDg =0 #O = CG, 1 = DG
space1 = 0 # function space (Lagrange=0)
beta1  = 100

#  compute solution and BC (given directly to the solver
# creation of law
lawEP = J2LinearDG3DMaterialLaw(lawnumEP,rhoEP,youngEP,nuEP,sy0EP,hEP)
lawInter  = DelaminationLinearCohesive3DLaw(lawcnumInt,Gc,GcII,sigmac,sigmacII,alphaGc,fsmin,fsmax,Kp)
lawInter.setViscosity(1.e8)


# creation of ElasticField

myfieldEP1 = dG3DDomain(10,51,space1,lawnumEP,fullDg)
myfieldEP1.stabilityParameters(beta1)

myfieldEP2 = dG3DDomain(11,52,space1,lawnumEP,fullDg)
myfieldEP2.stabilityParameters(beta1)

myinterfield = interDomainBetween3D(12,myfieldEP1,myfieldEP2,lawcnumInt)
myinterfield.stabilityParameters(beta1)

# creation of Solver
mysolver = nonLinearMechSolver(1000)
mysolver.createModel(geofile,meshfile,3,2)
#mysolver.loadModel(meshfile)
mysolver.addDomain(myfieldEP1)
mysolver.addDomain(myfieldEP2)
mysolver.addMaterialLaw(lawEP)

mysolver.addDomain(myinterfield)
mysolver.addMaterialLaw(lawInter)


mysolver.Scheme(soltype)
mysolver.Solver(sol)
mysolver.snlData(nstep,ftime,tol)
mysolver.stepBetweenArchiving(nstepArch)


#mysolver.explicitSpectralRadius(ftime,0.5,0.)
#mysolver.dynamicRelaxation(0.1, ftime, 1.e-3,2)
#mysolver.explicitTimeStepEvaluation(nstep)
#mysolver.iterativeProcedure(0)


# BC
#mysolver.displacementBC("Volume",51,1,0.)
#mysolver.displacementBC("Volume",52,1,0.)


mysolver.displacementBC("Face",53,0,0.)
mysolver.displacementBC("Face",53,1,0.)
mysolver.displacementBC("Face",53,2,0.)

mysolver.displacementBC("Face",56,2,1e-3)



mysolver.internalPointBuildView("svm",IPField.SVM, 1, 1)
mysolver.internalPointBuildView("sig_xx",IPField.SIG_XX, 1, 1)
mysolver.internalPointBuildView("sig_yy",IPField.SIG_YY, 1, 1)
mysolver.internalPointBuildView("sig_zz",IPField.SIG_ZZ, 1, 1)
mysolver.internalPointBuildView("sig_xy",IPField.SIG_XY, 1, 1)
mysolver.internalPointBuildView("sig_yz",IPField.SIG_YZ, 1, 1)
mysolver.internalPointBuildView("sig_xz",IPField.SIG_XZ, 1, 1)
mysolver.internalPointBuildView("epl",IPField.PLASTICSTRAIN, 1, 1)

mysolver.archivingForceOnPhysicalGroup("Face", 53, 2, 1)
mysolver.archivingForceOnPhysicalGroup("Face", 56, 2, 1)

mysolver.archivingNodeDisplacement(58,2,1)

mysolver.solve()

check = TestCheck()
check.equal(1.301290e+00,mysolver.getArchivedForceOnPhysicalGroup("Face", 56, 2),3.e-3)


