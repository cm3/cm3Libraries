#coding-Utf-8-*-
from gmshpy import *
from dG3Dpy import*

#script to launch PBC problem with a python script

# material law
lawnum = 11 # unique number of law
E = 70E3 #Young modulus
nu = 0.3 #Poisson ratio
K = E/3./(1.-2.*nu)
mu = E/2./(1+nu)
sy0 = 300e100 #Yield stress
h = 300. # hardening modulus
rho = 1.  #density
# geometry

geofile="model.geo"
meshfile="model.msh" # name of mesh file

# creation of material law
law1 = J2LinearDG3DMaterialLaw(lawnum,rho,E,nu,sy0,h)
law1.setUseBarF(True)


lawcnumInt = 5
GcInt   = 10.
sigmaInt = 200.
deltacInt= 2*GcInt/sigmaInt
KcInt = sigmaInt/(0.005*deltacInt)

beta =0.87 # ratio KII/KI
mu = 0. # friction coefficient ??
fsmin = 0.999
fsmax = 1.001


law2Int = LinearCohesive3DLaw(lawcnumInt,GcInt,sigmaInt,beta,mu,fsmin,fsmax,KcInt)

# creation of part Domain

beta1 = 50;

myfield1 = dG3DDomain(10,11,0,lawnum,0,3)
myfield1.stabilityParameters(beta1)

myfield2 = dG3DDomain(11,12,0,lawnum,0,3)
myfield2.stabilityParameters(beta1)

interDom1 = interDomainBetween3D(12,myfield1,myfield2,lawcnumInt)
interDom1.stabilityParameters(beta1)
interDom1.setBulkDamageBlockedMethod(0)
interDom1.forceCohesiveInsertionAllIPs(True,0.)

# solver
sol = 2  # Gmm=0 (default) Taucs=1 PETsc=2
soltype =1 # StaticLinear=0 (default) StaticNonLinear=1
nstep = 100  # number of step (used only if soltype=1)
ftime =1.   # Final time (used only if soltype=1)
tol=1.e-5 # relative tolerance for NR scheme (used only if soltype=1)
nstepArch=1 # Number of step between 2 archiving (used only if soltype=1)

# creation of Solver
mysolver = nonLinearMechSolver(1000)
mysolver.createModel(geofile,meshfile,3,1)
mysolver.addDomain(myfield1)
mysolver.addDomain(myfield2)
mysolver.addDomain(interDom1)

mysolver.addMaterialLaw(law1)
mysolver.addMaterialLaw(law2Int)

mysolver.Scheme(soltype)
mysolver.Solver(sol)
mysolver.snlData(nstep,ftime,tol)
mysolver.options("-ksp_type preonly -pc_type lu -pc_factor_mat_solver_type petsc")

method=0
mysolver.pathFollowing(False,method)
if method==1:
	# time-step adaptation by number of NR iterations
	mysolver.setPathFollowingIncrementAdaptation(True,3,0.3)
	mysolver.setPathFollowingLocalSteps(3e-2,1e-6)
	mysolver.setPathFollowingLocalIncrementType(1); 
elif method==0:
	mysolver.setPathFollowingControlType(1)
	#mysolver.setPathFollowingTranversalCriterion(0)
	#mysolver.setPathFollowingCorrectionMethod(0)
	mysolver.setPathFollowingArcLengthStep(2e-1)


selUpdate=cohesiveCrackCriticalIPUpdate()
mysolver.setSelectiveUpdate(selUpdate)



mysolver.displacementBC("Volume",11,2,0.)
mysolver.displacementBC("Volume",12,2,0.)

mysolver.displacementBC("Face",4,0,0.)
#mysolver.displacementBC("Face",1,0,0.)
mysolver.displacementBC("Face",1,1,0.)

#mysolver.displacementBC("Face",3,0,0.)
mysolver.displacementBC("Face",3,1,2e-1)
#mysolver.constraintBC("Face",3,1)
#mysolver.forceBC("Face",3,1,1e2)



# build view
mysolver.internalPointBuildView("Green-Lagrange_xx",IPField.STRAIN_XX, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_yy",IPField.STRAIN_YY, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_zz",IPField.STRAIN_ZZ, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_xy",IPField.STRAIN_XY, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_yz",IPField.STRAIN_YZ, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_xz",IPField.STRAIN_XZ, 1, 1);
mysolver.internalPointBuildView("sig_xx",IPField.SIG_XX, 1, 1);
mysolver.internalPointBuildView("sig_yy",IPField.SIG_YY, 1, 1);
mysolver.internalPointBuildView("sig_zz",IPField.SIG_ZZ, 1, 1);
mysolver.internalPointBuildView("sig_xy",IPField.SIG_XY, 1, 1);
mysolver.internalPointBuildView("sig_yz",IPField.SIG_YZ, 1, 1);
mysolver.internalPointBuildView("sig_xz",IPField.SIG_XZ, 1, 1);
mysolver.internalPointBuildView("sig_VM",IPField.SVM, 1, 1);
mysolver.internalPointBuildView("Equivalent plastic strain",IPField.PLASTICSTRAIN, 1, 1);

mysolver.internalPointBuildView("DissipationBlocked",IPField.DAMAGE_IS_BLOCKED, 1, 1);
mysolver.archivingNodeDisplacement(5,1,1)
mysolver.archivingForceOnPhysicalGroup('Face',1,1)

# solve
mysolver.solve()

check = TestCheck()
check.equal(-7.680655e-11,mysolver.getArchivedForceOnPhysicalGroup("Face", 1, 1),1.e1)
