#coding-Utf-8-*-
from gmshpy import *
from dG3Dpy import*

#script to launch beam problem with a python script

# material law
lawnum   = 1 # unique number of law
lawcoh   = 2 # unique number of law
lawfrac  = 3 # unique number of law
rho      = 7850
young    = 40.e9
youngA   = 230.e9;
nu       = 0.2 #
nu_minor = 0.256*young/youngA 
GA       = 24.e9

GcL      = 2.e3;
sigmacL  = 500.e6;
GcT      = 0.5e3;
sigmacT  = 50.e6;
beta     = 0.87;
mu       = -1.;
fsmin    = 0.999;
fsmax    = 1.001;


#longitudinal loading
Ax       = 1.; #direction of anisotropy
Ay       = 0.;
Az       = 1.;
d1=0.75e-5



# geometry
geofile="cube.geo"
meshfile="cube.msh" # name of mesh file

# solver
sol = 2  # Gmm=0 (default) Taucs=1 PETsc=2
soltype = 1 # StaticLinear=0 (default) StaticNonLinear=1
nstep = 50   # number of step (used only if soltype=1)
ftime =1.   # Final time (used only if soltype=1)
tol=1.e-6   # relative tolerance for NR scheme (used only if soltype=1)
nstepArch=1 # Number of step between 2 archiving (used only if soltype=1)
fullDg = 1 #O = CG, 1 = DG
space1 = 0 # function space (Lagrange=0)


#  compute solution and BC (given directly to the solver
# creation of law
law1 = TransverseIsotropicDG3DMaterialLaw(lawnum,rho,young,nu,youngA,GA,nu_minor,Ax,Ay,Az)
lawcoh1  = TransverseIsotropicLinearCohesive3D(lawcoh,GcL,sigmacL,GcT,sigmacT,beta,mu,Ax,Ay,Az,fsmin,fsmax)
lawfrac1 = FractureByCohesive3DLaw(lawfrac,lawnum,lawcoh)


# creation of ElasticField
nfield = 10 # number of the field (physical number of surface)
myfield1 = dG3DDomain(1000,nfield,space1,lawfrac,fullDg)
myfield1.stabilityParameters(30.)
myfield1.matrixByPerturbation(0,0,0,1e-8)
#myfield1.forceCohesiveInsertionAllIPs(True,0.)
# creation of Solver
mysolver = nonLinearMechSolver(1000)
#mysolver.createModel(geofile,meshfile,3,2)
mysolver.loadModel(meshfile)
mysolver.addDomain(myfield1)
mysolver.addMaterialLaw(law1)
mysolver.addMaterialLaw(lawcoh1)
mysolver.addMaterialLaw(lawfrac1)
mysolver.Scheme(soltype)
mysolver.Solver(sol)
mysolver.snlData(nstep,ftime,tol)
mysolver.stepBetweenArchiving(nstepArch)
#mysolver.snlManageTimeStep(10,5,2,15)
# BC
#shearing
#mysolver.displacementBC("Face",1234,0,0.)
#mysolver.displacementBC("Face",1234,1,0.)
#mysolver.displacementBC("Face",1234,2,0.)
#mysolver.displacementBC("Face",5678,1,0.00)
#mysolver.displacementBC("Face",5678,0,0.00001)
#mysolver.forceBC("Face",2376,2,100000000)
#mysolver.forceBC("Face",4158,2,-100000000)
#mysolver.forceBC("Face",5678,0,100000000)

selUpdate=cohesiveCrackCriticalIPUpdate()
mysolver.setSelectiveUpdate(selUpdate)


#tension along z
cyclicFunction1=cycleFunctionTime(0.,0.,ftime/4., d1/2., ftime/2., d1/4., 3.*ftime/4., d1/2., ftime, d1);

mysolver.displacementBC("Face",1234,2,0.)
mysolver.displacementBC("Face",5678,2,cyclicFunction1)
mysolver.displacementBC("Face",2376,0,0.)
mysolver.displacementBC("Face",1265,1,0.)


mysolver.internalPointBuildView("svm",IPField.SVM, 1, 1)
mysolver.internalPointBuildView("sig_xx",IPField.SIG_XX, 1, 1)
mysolver.internalPointBuildView("sig_yy",IPField.SIG_YY, 1, 1)
mysolver.internalPointBuildView("sig_zz",IPField.SIG_ZZ, 1, 1)
mysolver.internalPointBuildView("sig_xy",IPField.SIG_XY, 1, 1)
mysolver.internalPointBuildView("sig_yz",IPField.SIG_YZ, 1, 1)
mysolver.internalPointBuildView("sig_xz",IPField.SIG_XZ, 1, 1)
mysolver.internalPointBuildView("epl",IPField.PLASTICSTRAIN, 1, 1)
mysolver.archivingForceOnPhysicalGroup("Face", 1234, 2)
mysolver.archivingForceOnPhysicalGroup("Face", 5678, 2)

mysolver.solve()

check = TestCheck()
check.equal(4.010410e-01,mysolver.getArchivedForceOnPhysicalGroup("Face", 5678, 2),1.e-3)


