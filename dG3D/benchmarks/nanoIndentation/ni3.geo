//Specimen
m = 0.015;
a = 2.5;
b = a/10.;
um = 1.0e-3;

//Main top surface
Point(4001) = {0.0, 0.0, 0.0, m};
Point(4002) = {30.0*um, 0.0, 0.0, m};
Point(4004) = {-30.0*um * Tan(30*Pi/180), 30.0*um, 0.0, m};
Point(4003) = {30.0*um, 30.0*um, 0.0, m};
//Inner top surface
Point(4012) = {15.0*um, 0.0, 0.0, m};
Point(4013) = {15.0*um, 15.0*um, 0.0, m};
Point(4014) = {-15.0*um * Tan(30*Pi/180), 15.0*um, 0.0, m};
//Main top surface 2
Point(4005) = {-30.0*um, 30.0*um, 0.0, m};
Point(4006) = {-30.0*um, -30.0*um, 0.0, m};
Point(4007) = {-Tan(30*Pi/180) * 30.0*um, -30.0*um , 0.0, m};
//Inner top surface 2
Point(4015) = {-15.0*um, 15.0*um, 0.0, m};
Point(4016) = {-15.0*um, -15.0*um, 0.0, m};
Point(4017) = {-15.0*um * Tan(30*Pi/180), -15.0*um, 0.0, m};
//Main top surface 3
Point(4008) = {30.0*um, -30.0*um, 0.0, m};
//Inner top surface 3
Point(4018) = {15.0*um, -15.0*um, 0.0, m};
//Indenter
Point(4301) = {0.0, 0.0, um/400.0, 0.00005};
Point(4302) = {25.0*um, 0.0, 25.0*um * Tan(12.95*Pi/180) +um/400.0 , 0.0025};
Point(4303) = {-25.0*um * Sin(30*Pi/180) , 25.0*um * Cos(30*Pi/180) , 25.0*um * Tan(12.95*Pi/180)+um/400.0, 0.0025};
Point(4304) = {-25.0*um * Sin(30*Pi/180) , -25.0*um * Cos(30*Pi/180) , 25.0*um * Tan(12.95*Pi/180)+um/400.0, 0.0025};


//Main top suface
Line(4001) = {4001, 4012};
Line(4002) = {4012, 4002};
Line(4003) = {4002, 4003};
Line(4004) = {4003, 4004};
Line(4005) = {4004, 4014};
Line(4006) = {4014, 4001};
//Inner top surface
Line(4101) = {4013, 4012};
Line(4102) = {4014, 4013};
//Main top surface 2
Line(4007) = {4004, 4005};
Line(4008) = {4005, 4006};
Line(4009) = {4006, 4007};
Line(4010) = {4007, 4017};
Line(4011) = {4017, 4001};
//Inner top surface 2
Line(4103) = {4014, 4015};
Line(4104) = {4015, 4016};
Line(4105) = {4016, 4017};
//Main top surface 3
Line(4012) = {4007, 4008};
Line(4013) = {4008, 4002};
//Inner top surface 3
Line(4106) = {4017, 4018};
Line(4107) = {4018, 4012};
//Indenter
Line(4301) = {4301, 4302};
Line(4302) = {4302, 4303};
Line(4303) = {4303, 4301};
//Indenter 2
Line(4304) = {4301, 4304};
Line(4305) = {4304, 4303};
//Indenter 3
Line(4306) = {4304, 4302};

Curve loop(4001) = {4102, 4101, 4002, 4003, 4004, 4005}; //Outter surface
Curve loop(4002) = {4102, 4101, -4001, -4006}; //Inner surface
Curve loop(4003) = {-4005, 4007, 4008, 4009, 4010, -4105, -4104, -4103}; //Outter surface 2
Curve loop(4004) = {-4103, 4006, -4011, -4105, -4104}; //Inner surface 2
Curve loop(4005) = {4002, -4013, -4012, 4010, 4106, 4107}; //Outter surface 3
Curve loop(4006) = {4001, -4107, -4106, 4011}; //Inner surface 3

Plane Surface(4001) = {4001};
Plane Surface(4002) = {4002};
Plane Surface(4003) = {4003};
Plane Surface(4004) = {4004};
Plane Surface(4005) = {4005};
Plane Surface(4006) = {4006};

Extrude {0.0, 0.0, -30.0*um} {Surface{4001, 4002, 4003, 4004, 4005, 4006};}

//Tip
Curve loop(4101) = {4301, 4302, 4303};
Curve loop(4102) = {-4303, -4305, -4304};
Curve loop(4103) = {4304, 4306, -4301};

Plane Surface(4101) = {4101};
Plane Surface(4102) = {4102};
Plane Surface(4103) = {4103};

//Physical
Physical Point(1) = {4301};
Physical Point(1001) ={4001};
Physical Surface(11) = {4402, 4461, 4338, 4429, 4360, 4483};
Physical Surface(22) = {4001, 4002};
Physical Surface(33) = {4003, 4004};
Physical Surface(44) = {4005, 4006};
Physical Surface(5555) = {4385, 4448};
Physical Surface(6666) = {4381};
Physical Surface(7777) = {4377, 4333};
Physical Surface(8888) = {4444, 4329};
Physical Surface(222) = {4101};
Physical Surface(333) = {4102};
Physical Surface(444) = {4103};
Physical Volume(1) = {1};
Physical Volume(2) = {2};
Physical Volume(3) = {3};
Physical Volume(4) = {4};
Physical Volume(5) = {5};
Physical Volume(6) = {6};

Characteristic Length {4001} = 0.000125;
Characteristic Length {4332} = 0.0025;
Characteristic Length {4017, 4015, 4018, 4016, 4014, 4013, 4012, 4114, 4101, 4112, 4113} = 0.00125;
