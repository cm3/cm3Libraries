#coding-Utf-8-*-
from gmshpy import *
from dG3Dpy import*
import math
#from dG3DpyDebug import*

#script to launch beam problem with a python script
E = 350.e3
penalty = 1.e10
thick    =1.e-3

# material law
lawnum   = 1 # unique number of law
rho      = 7850.e-12
temperature = 293.
# geometry
geofile="ni3.geo"
meshfile="ni3.msh" # name of mesh file

# solver
sol = 2  # Gmm=0 (default) Taucs=1 PETsc=2
soltype = 1 # StaticLinear=0 (default) StaticNonLinear=1
nstep = 1000   # number of step (used only if soltype=1)
ftime = 1.1   # Final time (used only if soltype=1), strain rate = 10e-4
tol=1.e-6   # relative tolerance for NR scheme (used only if soltype=1)
nstepArch=1 # Number of step between 2 archiving (used only if soltype=1)
fullDg = 0 #O = CG, 1 = DG
space1 = 0 # function space (Lagrange=0)


#  compute solution and BC (given directly to the solver
# creation of law
lawSur = crystalPlasticityDG3DMaterialLaw(lawnum,rho,temperature,"grains.inp","material.i01")
lawSur.setUseBarF(True)
pertLawSur = dG3DMaterialLawWithTangentByPerturbation(lawSur,1e-8)


nu= 0.4     # Poisson ratio for the slave
sy0 = 120 # initial yield stress for the slave (Pa)
h = E/10.   # parameter in law LinearExponentialJ2IsotropicHardening: R = yield0 +h1 * p + h2* (1- exp(-hexp*p))

# creation of material law
law1 = J2LinearDG3DMaterialLaw(lawnum+1,rho,E,nu,sy0,h)
law1.setUseBarF(True)


# creation of ElasticField for the surface
nfield1 = 1 # number of the field (physical number of surface)
myfield1 = dG3DDomain(1000,nfield1,space1,lawnum,fullDg)
nfield2 = 2 # number of the field (physical number of surface)
myfield2 = dG3DDomain(1000,nfield2,space1,lawnum,fullDg)
nfield3 = 3 # number of the field (physical number of surface)
myfield3 = dG3DDomain(1000,nfield3,space1,lawnum,fullDg)
nfield4 = 4 # number of the field (physical number of surface)
myfield4 = dG3DDomain(1000,nfield4,space1,lawnum,fullDg)
nfield5 = 5 # number of the field (physical number of surface)
myfield5 = dG3DDomain(1000,nfield5,space1,lawnum,fullDg)
nfield6 = 6 # number of the field (physical number of surface)
myfield6 = dG3DDomain(1000,nfield6,space1,lawnum,fullDg)
# creation of ElasticField for the tip
#myfield1.matrixByPerturbation(1,1,1,1e-8)
# creation of Solver for the surface
mysolver = nonLinearMechSolver(1200)
mysolver.createModel(geofile,meshfile,3,1)
#mysolver.loadModel(meshfile)
mysolver.addDomain(myfield1)
mysolver.addDomain(myfield2)
mysolver.addDomain(myfield3)
mysolver.addDomain(myfield4)
mysolver.addDomain(myfield5)
mysolver.addDomain(myfield6)
mysolver.addMaterialLaw(lawSur)
#mysolver.addMaterialLaw(pertLawSur)
#mysolver.addMaterialLaw(law1)
mysolver.Scheme(soltype)
mysolver.Solver(sol)
mysolver.snlData(nstep,ftime,tol,1.e-3)
mysolver.setNumStepTimeInterval(0.5,20)
mysolver.setNumStepTimeInterval(0.51,20)
mysolver.setNumStepTimeInterval(1.01,20)
mysolver.stepBetweenArchiving(nstepArch)
mysolver.snlManageTimeStep(20,5,2,10)
mysolver.implicitSpectralRadius(0.01)
mysolver.explicitSpectralRadius(ftime,0.1,0.001)
mysolver.explicitTimeStepEvaluation(2000)

# BC
mysolver.displacementBC("Face",11,0,0.);
mysolver.displacementBC("Face",11,1,0.);
mysolver.displacementBC("Face",11,2,0.);
mysolver.displacementBC("Face",5555,1,0.);
mysolver.displacementBC("Face",6666,0,0.);
mysolver.displacementBC("Face",7777,1,0.);
mysolver.displacementBC("Face",8888,0,0.);

#mysolver.displacementBC("Node",1001,0,0.);
#mysolver.displacementBC("Node",1001,1,0.);

d1=3.1e-3
#cyclicFunction1=cycleFunctionTime(0.,0., 5.,-d1, 5.00005, -d1, 10.00005, 0.);
cyclicFunction1=cycleFunctionTime(0.,0., 0.5,-d1/10.,ftime, 0.);


flaw1 = CoulombFrictionLaw(1,0.,0.0,penalty,penalty/1000.)
contact1 = dG3DRigidPlaneContactDomain(1300,2,222,3,2,1,(25.0*math.sqrt(3.0)*10.0e-6*math.tan(12.95*math.pi/180.0))/2.0,(75.0*10.0e-6*math.tan(12.95*math.pi/180.0))/2.0,(-1.0*25.0*math.sqrt(3.0)*10.0e-6)/2.0,penalty,thick/100.,1e3)
contact2 = dG3DRigidPlaneContactDomain(1301,2,333,3,4,1,(-1.0*50.0*math.sqrt(3.0)*math.tan(12.95*math.pi/180.0)*10.0e-6)/2.0,0.0,(-1.0*50.0*math.sqrt(3.0)*10.0e-6)/4.0,penalty,thick/100.,1e3)
contact3 = dG3DRigidPlaneContactDomain(1302,2,444,3,6,1,(25.0*math.sqrt(3.0)*math.tan(12.95*math.pi/180.0)*10.0e-6)/2.0,(-75.0*math.tan(12.95*math.pi/180.0)*10.0e-6)/2.0,(-25.0*math.sqrt(3.0)*10.0e-6)/2.0,penalty,thick/100.,1e3)
contact1.setFriction(False)
contact1.addFrictionLaw(flaw1)
contact2.setFriction(False)
contact2.addFrictionLaw(flaw1)
contact3.setFriction(False)
contact3.addFrictionLaw(flaw1)
mysolver.contactInteraction(contact1)
mysolver.contactInteraction(contact2)
mysolver.contactInteraction(contact3)

mysolver.displacementRigidContactBC(222,0,0.)
mysolver.displacementRigidContactBC(222,2,cyclicFunction1)
mysolver.displacementRigidContactBC(222,1,0.)
mysolver.displacementRigidContactBC(333,0,0.)
mysolver.displacementRigidContactBC(333,2,cyclicFunction1)
mysolver.displacementRigidContactBC(333,1,0.)
mysolver.displacementRigidContactBC(444,0,0.)
mysolver.displacementRigidContactBC(444,2,cyclicFunction1)
mysolver.displacementRigidContactBC(444,1,0.)

mysolver.internalPointBuildView("svm",IPField.SVM,1,1)
mysolver.internalPointBuildView("sig_xx",IPField.SIG_XX,1,1)
mysolver.internalPointBuildView("sig_yy",IPField.SIG_YY,1,1)
mysolver.internalPointBuildView("sig_xy",IPField.SIG_XY,1,1)
mysolver.internalPointBuildView("sig_zz",IPField.SIG_ZZ,1,1)
mysolver.internalPointBuildView("sig_yz",IPField.SIG_YZ,1,1)
mysolver.internalPointBuildView("sig_xz",IPField.SIG_XZ,1,1)
mysolver.internalPointBuildView("epl",IPField.PLASTICSTRAIN,1,1)

mysolver.archivingNodeDisplacement(1001,2,1)

mysolver.archivingForceOnPhysicalGroup("Face", 11, 0)
mysolver.archivingForceOnPhysicalGroup("Face", 11, 1)
mysolver.archivingForceOnPhysicalGroup("Face", 11, 2)

#mysolver.archivingNodeDisplacementOnPhysical(2,2,0,nstepArch)
#mysolver.archivingNodeDisplacementOnPhysical(2,2,1,nstepArch)
#mysolver.archivingNodeDisplacementOnPhysical(2,2,2,nstepArch)

mysolver.archivingRigidContact(222,0,0, 1)
mysolver.archivingRigidContact(222,1,0, 1)
mysolver.archivingRigidContact(222,2,0, 1)
mysolver.archivingRigidContact(333,0,0, 1)
mysolver.archivingRigidContact(333,1,0, 1)
mysolver.archivingRigidContact(333,2,0, 1)
mysolver.archivingRigidContact(444,0,0, 1)
mysolver.archivingRigidContact(444,1,0, 1)
mysolver.archivingRigidContact(444,2,0, 1)
mysolver.archivingRigidContactForce(44, 0, 1)
mysolver.archivingRigidContactForce(44, 1, 1)
mysolver.archivingRigidContactForce(44, 2, 1)
mysolver.archivingRigidContactForce(33, 0, 1)
mysolver.archivingRigidContactForce(33, 1, 1)
mysolver.archivingRigidContactForce(33, 2, 1)
mysolver.archivingRigidContactForce(22, 0, 1)
mysolver.archivingRigidContactForce(22, 1, 1)
mysolver.archivingRigidContactForce(22, 2, 1)

mysolver.solve()

check = TestCheck()
check.equal(-2.711273e-04,mysolver.getArchivedNodalValue(1001,2,mysolver.displacement),1.e-4)



