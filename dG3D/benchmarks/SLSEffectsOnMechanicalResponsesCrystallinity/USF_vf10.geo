SetFactory('OpenCASCADE');
Mesh.SecondOrderLinear = 1;
Mesh.CharacteristicLengthMin =0.525;
Mesh.CharacteristicLengthMax =0.5;
Box(1) = {0,0,0,10.0,10.0,5.0};
Cylinder(2) = {0,0,0,0,10.0,0,0.5,2*Pi};
Cylinder(3) = {10.0,0,0,0.0,10.0,0,0.5,2*Pi};
Cylinder(4) = {0,0,5.0,0,10.0,0.0,0.5,2*Pi};
Cylinder(5) = {10.0,0,5.0,0.0,10.0,0.0,0.5,2*Pi};
Cylinder(6) = {5.0,0,2.5,0.0,10.0,0.0,0.5,2*Pi};
Cylinder(7) = {10.0,10.0,5.0,-10.0,-10.0,-5.0,0.5,2*Pi};
Cylinder(8) = {10.0,10.0,0,-10.0,-10.0,-5.0,0.5,2*Pi};
Cylinder(9) = {10.0,10.0,10.0,-10.0,-10.0,-5.0,0.5,2*Pi};
Cylinder(10) = {20.0,10.0,5.0,-10.0,-10.0,-5.0,0.5,2*Pi};
Cylinder(11) = {0,10.0,5.0,-10.0,-10.0,-5.0,0.5,2*Pi};
Cylinder(12) = {0,10.0,0,10.0,-10.0,5.0,0.5,2*Pi};
Cylinder(13) = {0,10.0,-5.0,10.0,-10.0,5.0,0.5,2*Pi};
Cylinder(14) = {0,10.0,5.0,10.0,-10.0,5.0,0.5,2*Pi};
Cylinder(15) = {10.0,10.0,0,10.0,-10.0,5.0,0.5,2*Pi};
Cylinder(16) = {-10.0,10.0,0,10.0,-10.0,5.0,0.5,2*Pi};
Cylinder(17) = {10.0,5.0,0,-5.0,-5.0,2.5,0.5,2*Pi};
Cylinder(18) = {10.0,5.0,-5.0,-5.0,-5.0,2.5,0.5,2*Pi};
Cylinder(19) = {10.0,5.0,5.0,-5.0,-5.0,2.5,0.5,2*Pi};
Cylinder(20) = {0,5.0,0,-5.0,-5.0,2.5,0.5,2*Pi};
Cylinder(21) = {5.0,10.0,2.5,5.0,-5.0,-2.5,0.5,2*Pi};
Cylinder(22) = {5.0,10.0,-2.5,5.0,-5.0,-2.5,0.5,2*Pi};
Cylinder(23) = {5.0,10.0,7.5,5.0,-5.0,-2.5,0.5,2*Pi};
Cylinder(24) = {-5.0,10.0,2.5,5.0,-5.0,-2.5,0.5,2*Pi};
Cylinder(25) = {5.0,0,2.5,-5.0,5.0,2.5,0.5,2*Pi};
Cylinder(26) = {5.0,0,-2.5,-5.0,5.0,2.5,0.5,2*Pi};
Cylinder(27) = {15.0,0,2.5,-5.0,5.0,2.5,0.5,2*Pi};
Cylinder(28) = {0,5.0,5.0,5.0,5.0,-2.5,0.5,2*Pi};
Cylinder(29) = {0,5.0,0,5.0,5.0,-2.5,0.5,2*Pi};
Cylinder(30) = {10.0,5.0,5.0,5.0,5.0,-2.5,0.5,2*Pi};
BooleanUnion(31) = {Volume{2};Volume{3};Volume{4};Volume{5};Volume{6};Volume{7};Volume{8};Volume{9};Volume{10};Volume{11};Volume{12};Volume{13};Volume{14};Volume{15};Volume{16};Volume{17};Volume{18};Volume{19};Volume{20};Volume{21};Volume{22};Volume{23};Volume{24};Volume{25};Volume{26};Volume{27};Volume{28};Volume{29};Delete; }{ Volume{30}; Delete; };
BooleanIntersection(32)={ Volume{31}; Delete;}{Volume{1}; Delete;};
e=1e-6;
Rx=10.0;
Ry=10.0;
Rz=5.0;
bottomx() = Surface In BoundingBox{-e,-e,-e, e,10.0+e,5.0+e};
bottomy() = Surface In BoundingBox{-e,-e,-e, 10.0+e,e,5.0+e};
bottomz() = Surface In BoundingBox{-e,-e,-e, 10.0+e,10.0+e,e};
For i In {0:#bottomx()-1}
  bb() = BoundingBox Surface { bottomx(i) };
  topx() = Surface In BoundingBox{bb(0)-e+10.0, bb(1)-e, bb(2)-e, bb(3)+e+10.0, bb(4)+e, bb(5)+e };
  For j In {0:#topx()-1}
    bb2() = BoundingBox Surface { topx(j) };
    bb2(0) -= 10.0;
    bb2(3) -= 10.0;
    If(Fabs(bb2(0)-bb(0)) < e && Fabs(bb2(1)-bb(1)) < e &&
       Fabs(bb2(2)-bb(2)) < e && Fabs(bb2(3)-bb(3)) < e &&
       Fabs(bb2(4)-bb(4)) < e && Fabs(bb2(5)-bb(5)) < e)
      Periodic Surface {topx(j)} = {bottomx(i)} Translate {10.0,0,0};
    EndIf
   EndFor
 EndFor
For i In {0:#bottomy()-1}
  cc() = BoundingBox Surface { bottomy(i) };
  topy() = Surface In BoundingBox{cc(0)-e, cc(1)-e+10.0, cc(2)-e, cc(3)+e, cc(4)+e+10.0, cc(5)+e};
  For j In {0:#topy()-1}
    cc2() = BoundingBox Surface { topy(j) };
    cc2(1) -= 10.0;
    cc2(4) -= 10.0;
    If(Fabs(cc2(0)-cc(0)) < e && Fabs(cc2(1)-cc(1)) < e &&
       Fabs(cc2(2)-cc(2)) < e && Fabs(cc2(3)-cc(3)) < e &&
       Fabs(cc2(4)-cc(4)) < e && Fabs(cc2(5)-cc(5)) < e)
      Periodic Surface {topy(j)} = {bottomy(i)} Translate {0,10.0,0};
    EndIf
  EndFor
EndFor
For i In {0:#bottomz()-1}
  dd() = BoundingBox Surface { bottomz(i) };
  topz() = Surface In BoundingBox{ dd(0)-e, dd(1)-e, dd(2)-e+5.0, dd(3)+e, dd(4)+e, dd(5)+e+5.0 };
  For j In {0:#topz()-1}
    dd2() = BoundingBox Surface { topz(j) };
    dd2(2) -= 5.0;
    dd2(5) -= 5.0;
    If(Fabs(dd2(0)-dd(0)) < e && Fabs(dd2(1)-dd(1)) < e &&
       Fabs(dd2(2)-dd(2)) < e && Fabs(dd2(3)-dd(3)) < e &&
       Fabs(dd2(4)-dd(4)) < e && Fabs(dd2(5)-dd(5)) < e)
      Periodic Surface {topz(j)} = {bottomz(i)} Translate {0,0,5.0};
    EndIf
  EndFor
EndFor
Mytopx() = Surface In BoundingBox{Rx-e,-e,-e, Rx+e,Ry+e,Rz+e};
Mytopy() = Surface In BoundingBox{-e,Ry-e,-e, Rx+e,Ry+e,Rz+e};
Mytopz() = Surface In BoundingBox{-e,-e,Rz-e, Rx+e,Ry+e,Rz+e};

AllVol() = Volume{:};

Physical Surface(100) = {bottomx()};
Physical Surface(110) = {bottomy()};
Physical Surface(120) = {bottomz()};

Physical Surface(101) = {Mytopx()};
Physical Surface(111) = {Mytopy()};
Physical Surface(121) = {Mytopz()};

Physical Volume(51) ={AllVol()};
