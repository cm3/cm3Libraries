#coding-Utf-8-*-
from gmshpy import *


#from dG3DpyDebug import*
from dG3Dpy import*

#script to launch ElecMag SMP problem with a python script

import math

Irms = 0. # Ampere
freq = 0. # Hz
nTurnsCoil = 0
coilLx = coilLy = coilLz = 0. # m
coilW = 0. # m
mag_mu_0 = 4.e-7 * math.pi # vacuum magnetic permeability
tinitial = 273.+25.

# material law: inductor (copper coil)
# Material parameters for copper
lawnumind = 2
rhoind = 8960. 
Eind=120.e9 #youngs modulus
Gind=48.e9 # Shear modulus
Muind = 0.34 # Poisson ratio
alphaind = betaind = gammaind = 0. # parameter of anisotropy
cpind= 385.
Kxind=Kyind=Kzind= 401. # thermal conductivity tensor components
lxind=lyind=lzind=5.77e7 # electrical conductivity (S/m)
seebeckind=6.5e-6
alphaThermind= 0.0 # thermal dilatation
v0ind = 0.
mag_r_ind = 1.0 # relative mag permeability of inductor
mag_mu_x_ind = mag_mu_y_ind = mag_mu_z_ind = mag_r_ind * mag_mu_0 # mag permeability inductor 
magpot0_x_ind = magpot0_y_ind = magpot0_z_ind = 0.0 # initial magnetic potential

useFluxT=False

# geometry
geofile="cube.geo" # name of mesh file
meshfile="cube.msh" # name of mesh file

# solver
sol = 2  # Gmm=0 (default) Taucs=1 PETsc=2
soltype = 1 # StaticLinear=0 (default) StaticNonLinear=1
nstep = 1 # number of step (used only if soltype=1)
ftime =1. # Final time (used only if soltype=1)
tol=1.e-3   # relative tolerance for NR scheme (used only if soltype=1)
nstepArch=1 # Number of step between 2 archiving (used only if soltype=1)
fullDg = 0 #O = CG, 1 = DG
beta1 = 1000.
eqRatio =1.e8

#  compute solution and BC (given directly to the solver
# creation of law

# material law for inductor region
lawind = LinearElecMagTherMechDG3DMaterialLaw(lawnumind,rhoind,Eind,Eind,Eind,Muind,Muind,Muind,Gind,Gind,Gind,alphaind,betaind,gammaind,
tinitial,Kxind,Kyind,Kzind,alphaThermind,alphaThermind,alphaThermind,lxind,lyind,lzind,seebeckind,cpind,v0ind, mag_mu_x_ind, mag_mu_y_ind, mag_mu_z_ind, 
magpot0_x_ind,magpot0_y_ind, magpot0_z_ind, Irms, freq, nTurnsCoil, coilLx, coilLy, coilLz, coilW,useFluxT)

#lawind.setUseBarF(True)

# creation of ElasticField
Indfield = 10 # number of the field (physical number of Inductor region)
#SurfIndStart = 1234 # delta voltage surface
#SurfIndEnd = 5678 # ground voltage surface
SurfAirInf = 3487 

space1 = 0 # function space (Lagrange=0)

Inductor_field = ElecMagTherMechDG3DDomain(10,Indfield,space1,lawnumind,fullDg,eqRatio,3)

Inductor_field.setConstitutiveExtraDofDiffusionEqRatio(eqRatio)
Inductor_field.setConstitutiveCurlEqRatio(eqRatio)
Inductor_field.stabilityParameters(beta1)

Inductor_field.matrixByPerturbation(1,1,1,1e-8)

# creation of Solver
mysolver = nonLinearMechSolver(1000)
mysolver.createModel(geofile,meshfile,3,1)
#mysolver.loadModel(meshfile)
mysolver.addDomain(Inductor_field)
mysolver.addMaterialLaw(lawind)
mysolver.Scheme(soltype)
mysolver.Solver(sol)
mysolver.snlData(nstep,ftime,tol,1.e-9)#forsmp
mysolver.options("-ksp_type preonly")
mysolver.options("-pc_type lu")

#mysolver.snlData(nstep,ftime,tol,1.e-19)
mysolver.stepBetweenArchiving(nstepArch)
mysolver.snlManageTimeStep(25, 3, 2, 10)

# BC
##cyclicFunctionDisp=cycleFunctionTime(0., 0., ftime/5., -0.0001, ftime , -0.0001);
#mysolver.displacementBC("Face",83,2,0.)
mysolver.displacementBC("Volume",Indfield,0,0.0)
mysolver.displacementBC("Volume",Indfield,1,0.0)
mysolver.displacementBC("Volume",Indfield,2,0.0)

#thermal BC
#cyclicFunctionTemp1=cycleFunctionTime(0., tinitial,ftime,tinitial);
#mysolver.displacementBC("Face",85,3,cyclicFunctionTemp1)
#mysolver.initialBC("Volume","Position",Mfield,3,tinitial)
mysolver.displacementBC("Volume",Indfield,3,0.0)

#electrical BC
mysolver.displacementBC("Volume",Indfield,4,0.0)
#mysolver.initialBC("Volume","Position",Indfield,4,0.)
cyclicFunctionvolt1=cycleFunctionTime(0., 0.,  ftime,100.);
#mysolver.displacementBC("Face",SurfIndStart,4,cyclicFunctionvolt1)
cyclicFunctionvolt2=cycleFunctionTime(0., 0.,  ftime,0.);
#mysolver.displacementBC("Face",SurfIndEnd,4,cyclicFunctionvolt2)

#magentic
#Gauging the edges using tree-cotree method
PhysicalCurves = "" 		# input required as a string of comma separated ints
PhysicalSurfaces = "3487" # input required as a string of comma separated ints
PhysicalVolumes = "10" 	# input required as a string of comma separated ints
OutputPhysical = 55 		# input required as a int
#mysolver.createTreeForBC(PhysicalCurves,PhysicalSurfaces,PhysicalVolumes,OutputPhysical)
#mysolver.curlDisplacementBC("Edge",OutputPhysical,5,0.0)

#mysolver.internalPointBuildView("svm",IPField.SVM, 1, 1)
#mysolver.internalPointBuildView("sig_xx",IPField.SIG_XX, 1, 1)
#mysolver.internalPointBuildView("sig_yy",IPField.SIG_YY, 1, 1)
#mysolver.internalPointBuildView("sig_zz",IPField.SIG_ZZ, 1, 1)
#mysolver.internalPointBuildView("sig_xy",IPField.SIG_XY, 1, 1)
#mysolver.internalPointBuildView("sig_yz",IPField.SIG_YZ, 1, 1)
#mysolver.internalPointBuildView("sig_xz",IPField.SIG_XZ, 1, 1)
#mysolver.internalPointBuildView("temperature",IPField.TEMPERATURE, 1, 1)
#mysolver.internalPointBuildView("qx",IPField.THERMALFLUX_X, 1, 1)
#mysolver.internalPointBuildView("qy",IPField.THERMALFLUX_Y, 1, 1)
#mysolver.internalPointBuildView("qz",IPField.THERMALFLUX_Z, 1, 1)
#mysolver.internalPointBuildView("voltage",IPField.VOLTAGE, 1, 1)
#mysolver.internalPointBuildView("jex",IPField.ELECTRICALFLUX_X, 1, 1)
#mysolver.internalPointBuildView("jey",IPField.ELECTRICALFLUX_Y, 1, 1)
#mysolver.internalPointBuildView("jez",IPField.ELECTRICALFLUX_Z, 1, 1)
mysolver.internalPointBuildView("ax",IPField.MAGNETICVECTORPOTENTIAL_X, 1, 1)
mysolver.internalPointBuildView("ay",IPField.MAGNETICVECTORPOTENTIAL_Y, 1, 1)
mysolver.internalPointBuildView("az",IPField.MAGNETICVECTORPOTENTIAL_Z, 1, 1)
mysolver.internalPointBuildView("bx",IPField.MAGNETICINDUCTION_X, 1, 1)
mysolver.internalPointBuildView("by",IPField.MAGNETICINDUCTION_Y, 1, 1)
mysolver.internalPointBuildView("bz",IPField.MAGNETICINDUCTION_Z, 1, 1)
mysolver.internalPointBuildView("js0_x",IPField.INDUCTORSOURCEVECTORFIELD_X, 1, 1)
mysolver.internalPointBuildView("js0_y",IPField.INDUCTORSOURCEVECTORFIELD_Y, 1, 1)
mysolver.internalPointBuildView("js0_z",IPField.INDUCTORSOURCEVECTORFIELD_Z, 1, 1)
mysolver.internalPointBuildView("wAV_x",IPField.EMSOURCEVECTORFIELD_X, 1, 1)
mysolver.internalPointBuildView("wAV_y",IPField.EMSOURCEVECTORFIELD_Y, 1, 1)
mysolver.internalPointBuildView("wAV_z",IPField.EMSOURCEVECTORFIELD_Z, 1, 1)

#mysolver.archivingIPOnPhysicalGroup("Volume",Mfield, IPField.SIG_ZZ,IPField.MEAN_VALUE,nstepArch);
#mysolver.archivingIPOnPhysicalGroup("Volume",Mfield, IPField.STRAIN_ZZ,IPField.MEAN_VALUE,nstepArch);
#mysolver.archivingIPOnPhysicalGroup("Volume",Mfield, IPField.TEMPERATURE,IPField.MEAN_VALUE,nstepArch);
#mysolver.archivingIPOnPhysicalGroup("Volume",Mfield, IPField.VOLTAGE,IPField.MEAN_VALUE,nstepArch);
#mysolver.archivingIPOnPhysicalGroup("Volume",Ffield, IPField.SIG_ZZ,IPField.MEAN_VALUE,nstepArch);
#mysolver.archivingIPOnPhysicalGroup("Volume",Ffield, IPField.STRAIN_ZZ,IPField.MEAN_VALUE,nstepArch);
#mysolver.archivingIPOnPhysicalGroup("Volume",Ffield, IPField.TEMPERATURE,IPField.MEAN_VALUE,nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",Indfield, IPField.VOLTAGE,IPField.MEAN_VALUE,nstepArch);

#mysolver.archivingNodeDisplacement(88,3,1);

#mysolver.archivingForceOnPhysicalGroup("Face", 85, 3)

#mysolver.archivingNodeDisplacement(88,4,1);

#mysolver.archivingNodeIP(88, IPField.THERMALFLUX_X,IPField.MEAN_VALUE,1);

#mysolver.archivingNodeIP(88, IPField.ELECTRICALFLUX_X,IPField.MEAN_VALUE,1);

mysolver.solve()

check = TestCheck()


