#coding-Utf-8-*-
from gmshpy import *
from dG3Dpy import*

#script to launch beam problem with a python script

# material law
lawnonlocal1     = 1 # unique number of lawlawnonlocal1     = 1 # unique number of law
lawnonlocalcoh1  = 2 # unique number of law
lawnonlocalfrac1 = 3 # unique number of law
rho = 7850. # Bulk mass

Gc = 14.4e3 # 17.66e3     14.4e3     8.79e3
Dc = 0.99 #now the damage is automatically blocked 0.85       # 0.95       0.99
sigmacCF = 604.2e6 #239.9e6      604.2e6      176.7e7 # fracture limit in tension [Pa]
beta=1

propertiesC1="prop_ep.i01" #properties file



beta =0.87 # ratio KII/KI
mu = -1. # friction coefficient ??
fsmin = 1.
fsmax = 1.0

# geometry
geofile="twoHole.geo"
meshfile="twoHole.msh" # name of mesh file

# solver
sol = 2  # Gmm=0 (default) Taucs=1 PETsc=2
soltype = 4 #3 # StaticLinear=0 (default) StaticNonLinear=1
nstep = 6000   # number of step (used only if soltype=1)
ftime =3.e-2 # Final time (used only if soltype=1)
tol=1.e-6   # relative tolerance for NR scheme (used only if soltype=1)
nstepArch=20 # Number of step between 2 archiving (used only if soltype=1)
fullDg =1 #O = CG, 1 = DG
space1 = 0 # function space (Lagrange=0)
beta1  = 100.
eqRatio = 1.e8
dim=3
nlVar=1


#  compute solution and BC (given directly to the solver
# creation of law
law1     = NonLocalDamageDG3DMaterialLaw(lawnonlocal1,rho,propertiesC1)
elasPotential = linearCohesiveElasticPotential(26)
damageLaw = ExponentialCohesiveDamageLaw(27,1.5)
lawcoh1  = NonLocalDamageLinearCohesive3DLaw(lawnonlocalcoh1,elasPotential,damageLaw,Gc,sigmacCF,Dc,beta,fsmin,fsmax)
lawfrac1 = FractureByCohesive3DLaw(lawnonlocalfrac1,lawnonlocal1,lawnonlocalcoh1)


# creation of ElasticField

myfield = dG3DDomain(1000,54,space1,lawnonlocalfrac1,fullDg,dim,nlVar)
#myfieldEP.matrixByPerturbation(1,1,1,1e-8)
myfield.stabilityParameters(beta1)
myfield.setNonLocalStabilityParameters(beta1,bool(1))
myfield.setNonLocalEqRatio(eqRatio)
myfield.forceCohesiveInsertionAllIPs(False,0.6)
myfield.setBulkDamageBlockedMethod(0)
myfield.usePrimaryShapeFunction(3)

# creation of Solver
mysolver = nonLinearMechSolver(1000)
mysolver.createModel(geofile,meshfile,3,2)
#mysolver.loadModel(meshfile)
mysolver.addDomain(myfield)
mysolver.addMaterialLaw(law1)
mysolver.addMaterialLaw(lawcoh1)
mysolver.addMaterialLaw(lawfrac1)
mysolver.Scheme(soltype)
mysolver.Solver(sol)
mysolver.snlData(nstep,ftime,tol)
mysolver.snlManageTimeStep(30,5,20,10)
#mysolver.lineSearch(bool(1))
mysolver.stepBetweenArchiving(nstepArch)
mysolver.explicitSpectralRadius(ftime,0.5,0.)
mysolver.explicitTimeStepEvaluation(nstep)
mysolver.stepBetweenArchiving(nstepArch)
mysolver.implicitSpectralRadius(0.01)
mysolver.options("-ksp_type preonly -pc_type lu")
#mysolver.addSystem(3,2)
#mysolver.addSystem(1,1)


selUpdate=cohesiveCrackCriticalIPUpdate()
mysolver.setSelectiveUpdate(selUpdate)

#mysolver.explicitSpectralRadius(ftime,0.5,0.)
#mysolver.dynamicRelaxation(0.1, ftime, 1.e-3,2)
#mysolver.explicitTimeStepEvaluation(nstep)


#mysolver.pathFollowing(0)
#mysolver.setPathFollowingControlType(0)

# BC

mysolver.displacementBC("Volume",54,2,0.)

mysolver.displacementBC("Face",55,0,0.)
mysolver.displacementBC("Face",55,1,0.)

mysolver.displacementBC("Face",56,0,0.)
cyclicFunction1=cycleFunctionTime(0.,0.,ftime/5, 5.e-4, ftime, 0.9e-3);
mysolver.displacementBC("Face",56,1,cyclicFunction1)



mysolver.internalPointBuildView("svm",IPField.SVM, 1, 1)
mysolver.internalPointBuildView("sig_xx",IPField.SIG_XX, 1, 1)
mysolver.internalPointBuildView("sig_yy",IPField.SIG_YY, 1, 1)
mysolver.internalPointBuildView("sig_zz",IPField.SIG_ZZ, 1, 1)
mysolver.internalPointBuildView("sig_xy",IPField.SIG_XY, 1, 1)
mysolver.internalPointBuildView("sig_yz",IPField.SIG_YZ, 1, 1)
mysolver.internalPointBuildView("sig_xz",IPField.SIG_XZ, 1, 1)
mysolver.internalPointBuildView("epl",IPField.PLASTICSTRAIN, 1, 1)
mysolver.internalPointBuildView("damage",IPField.DAMAGE, 1, 1)

mysolver.archivingIPOnPhysicalGroup("Volume",54, IPField.DAMAGE,IPField.MAX_VALUE,10)

mysolver.archivingForceOnPhysicalGroup("Face", 55, 1, 10)
mysolver.archivingNodeDisplacement(57,1,10)

mysolver.solve()

check = TestCheck()
check.equal(-8.431679e+01,mysolver.getArchivedForceOnPhysicalGroup("Face", 55, 1),5.e-2)
check.equal(9.2e-04,mysolver.getArchivedNodalValue(57,1,mysolver.displacement),1e-6)

try:
  import linecache
  linesDam = linecache.getline('IPVolume54val_DAMAGEMax.csv',400)
except:
  print('Cannot read the results in the files')
  import os
  os._exit(1)
else:
  check.equal(8.659006e-01,float(linesDam.split(';')[1]))

