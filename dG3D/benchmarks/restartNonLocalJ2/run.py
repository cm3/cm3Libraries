import os,csv
#from gmshpy import *
#from dG3Dpy import*
import sys

def checkEqual(ref, cur, tol):
  if(abs(ref-cur)>abs(tol*ref)):
    print("Error : reference  current value ",ref," current value ",cur," relative error ", (ref-cur)/ref," tolerance ",tol)


if sys.version_info[0] < 3:
  os.system('python twoHole.py')
else:
  os.system('python3 twoHole.py')

data1 = csv.reader(open('force12comp1.csv'), delimiter=';')
force = list(data1)
checkEqual(-2.847517e+03,float(force[-1][1]),1e-5)
data2 = csv.reader(open('NodalDisplacementPhysical19Num4comp1.csv'), delimiter=';')
disp = list(data2)
checkEqual(9.999891e-04,float(disp[-1][1]),1e-5)

if sys.version_info[0] < 3:
  os.system('python twoHole.py')
else:
  os.system('python3 twoHole.py')


data21 = csv.reader(open('force12comp1.csv'), delimiter=';')
force2 = list(data21)
checkEqual(-2.847517e+03,float(force2[-1][1]),1e-5)
data22 = csv.reader(open('NodalDisplacementPhysical19Num4comp1.csv'), delimiter=';')
disp2 = list(data22)
checkEqual(9.999891e-04,float(disp2[-1][1]),1e-5)

