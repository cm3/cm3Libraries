
mm=1.0;
L=5*mm;
H = 0.3*mm; 
sl1=0.5*L/n;
Point(1)={0,0,0,sl1};
Point(2)={L,0,0,sl1};
Point(3)={L,H,0,sl1};
Point(4)={0,H,0,sl1};
Line(1)={1,2};
Line(2)={2,3};
Line(3)={3,4};
Line(4)={4,1};

l[0]=newreg;
Line Loop(l[0])={1,2,3,4};
Plane Surface(11)={l[0]};
Physical Line(1)={1}; 
Physical Line(2)={2};
Physical Line(3)={3};
Physical Line(4)={4}; 
Physical Surface(11)={11};
Physical Point(1)={1}; 
Physical Point(2)={2};
Physical Point(3)={3};
Physical Point(4)={4}; 

Extrude {1, 0, 0} {
  Line{2};
}
Extrude {-1, 0, 0} {
  Line{4};
}
Transfinite Line {16, 4, 2, 12} = 2 Using Progression 1;
Transfinite Line {17, 18, 14, 13} = 2 Using Progression 1;
Transfinite Line {3, 1} = 20 Using Progression 1;
Transfinite Surface {15, 11, 19};
Recombine Surface {15, 11, 19};

Physical Surface(12) = {19};
Physical Surface(13) = {15};
