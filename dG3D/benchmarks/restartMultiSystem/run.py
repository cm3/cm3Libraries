import os,csv
from gmshpy import *
#from dG3Dpy import*

import sys

def checkEqual(ref, cur, tol):
  if(abs(ref-cur)>abs(tol*ref)):
    print("Error : reference  current value ",ref," current value ",cur," relative error ", (ref-cur)/ref," tolerance ",tol)


if sys.version_info[0] < 3:
  os.system('mpiexec -np 4 python notchedBar.py')
else:
  os.system('mpiexec -np 4 python3 notchedBar.py')

data1 = csv.reader(open('force62comp1_part0.csv'), delimiter=';')
force = list(data1)
checkEqual(-1.433547e+01,float(force[-1][1]),1e-5)
data2 = csv.reader(open('NodalDisplacementPhysical73Num7comp1_part0.csv'), delimiter=';')
disp = list(data2)
checkEqual(1.5e-03,float(disp[-1][1]),1e-5)

if sys.version_info[0] < 3:
  os.system('mpiexec -np 4 python notchedBar.py')
else:
  os.system('mpiexec -np 4 python3 notchedBar.py')

data21 = csv.reader(open('force62comp1_part0.csv'), delimiter=';')
force2 = list(data21)
checkEqual(-1.433547e+01,float(force2[-1][1]),1e-5)
data22 = csv.reader(open('NodalDisplacementPhysical73Num7comp1_part0.csv'), delimiter=';')
disp2 = list(data22)
checkEqual(1.5e-03,float(disp2[-1][1]),1e-5)

