#coding-Utf-8-*-
from gmshpy import *


#from dG3DpyDebug import*
from dG3Dpy import*

#script to launch ElecMag SMP problem with a python script

import math

# material law: SMP
lawnum= 1 
rho   = 1020. # material density
tinitial = 273.+25.
G=156.e6 # Shear modulus
Mu=0.35 # Poisson ratio
alpha = beta=gamma=0. # parameter of anisotropy
cp=1710.*rho # specific heat capacity 
Kx=Ky=Kz=0.5 # Thermal conductivity
lx=ly=lz=35. # electrical conductivity (S/m) 
seebeck=21.e-6
alphaTherm=0.0 #13.e-5 # thermal dilatation
v0=0. # initial electric potential
E_matrix = 2.0 * G * (1.0 + Mu)
mag_mu_0 = 4.e-7 * math.pi # vacuum magnetic permeability
mag_r_smp = 5.0 # relative mag permeability of SMP
mag_mu_x = mag_mu_y = mag_mu_z = mag_r_smp * mag_mu_0 # mag permeability smp 
magpot0_x = magpot0_y = magpot0_z = 0.0 # initial magnetic potential
Irms = 10. # Ampere
freq = 1. # Hz
nTurnsCoil = 288
coilLx = coilLy = coilLz = 9.e-2 # m
coilW = 3.e-2 # m

# material law: inductor (copper coil)
# Material parameters for copper
lawnumind = 2
rhoind = 8960. 
Eind=120.e9 #youngs modulus
Gind=48.e9 # Shear modulus
Muind = 0.34 # Poisson ratio
alphaind = betaind = gammaind = 0. # parameter of anisotropy
cpind= 385.
Kxind=Kyind=Kzind= 401. # thermal conductivity tensor components
lxind=lyind=lzind=5.77e7 # electrical conductivity (S/m)
seebeckind=6.5e-6
alphaThermind= 0.0 # thermal dilatation
v0ind = 0.
mag_r_ind = 1.0 # relative mag permeability of inductor
mag_mu_x_ind = mag_mu_y_ind = mag_mu_z_ind = mag_r_ind * mag_mu_0 # mag permeability inductor 
magpot0_x_ind = magpot0_y_ind = magpot0_z_ind = 0.0 # initial magnetic potential
Centroid_x = Centroid_y = Centroid_z = 0.0 # Centroid of inductor coil
centralAxis_x = 0.
centralAxis_y = 1.
centralAxis_z = 0. # Unit vector along central axis of inductor coil

# material law: vacuum/free space region
lawnumvac = 3
rhovac = 1.2 
Gvac=156.e1 # Shear modulus (1.e-5 less than SMP)
Muvac = 0.35 # Poisson ratio (same as SMP)
Evac= 2.0 * Gvac * (1.0 + Muvac) #youngs modulus
alphavac = betavac = gammavac = 0. # parameter of anisotropy
cpvac= 1012.
Kxvac=Kyvac=Kzvac= 26.e-3 # thermal conductivity tensor components
lxvac=lyvac=lzvac= 1.e-12 # electrical conductivity
seebeckvac= 0.
alphaThermvac= 0.0 # thermal dilatation
v0vac = 0.
mag_r_vac = 1.0 # relative mag permeability of inductor
mag_mu_x_vac = mag_mu_y_vac = mag_mu_z_vac = mag_r_vac * mag_mu_0 # mag permeability inductor 
magpot0_x_vac = magpot0_y_vac = magpot0_z_vac = 0.0 # initial magnetic potential

useFluxT=True;
evaluateMecaField = False;

# geometry
geofile="smp.geo" # name of mesh file
meshfile="smp.msh" # name of mesh file

# solver
sol = 2  # Gmm=0 (default) Taucs=1 PETsc=2
soltype = 1 # StaticLinear=0 (default) StaticNonLinear=1
nstep = 10 # number of step (used only if soltype=1)
ftime =1.0/freq # Final time (used only if soltype=1)
tol=1.e-3   # relative tolerance for NR scheme (used only if soltype=1)
nstepArch=1 # Number of step between 2 archiving (used only if soltype=1)
fullDg = 0 #O = CG, 1 = DG
beta1 = 1000.
eqRatio =1.e8

#  compute solution and BC (given directly to the solver
# creation of law

# material law for smp
lawsmp = LinearElecMagTherMechDG3DMaterialLaw(lawnum,rho,E_matrix,E_matrix,E_matrix,Mu,Mu,Mu,G,G,G,alpha,beta,gamma,
tinitial,Kx,Ky,Kz,alphaTherm,alphaTherm,alphaTherm,lx,ly,lz,seebeck,cp,v0, mag_mu_x, mag_mu_y, mag_mu_z, magpot0_x,
magpot0_y, magpot0_z, Irms, freq, nTurnsCoil, coilLx, coilLy, coilLz, coilW,useFluxT)

#lawsmp.setUseBarF(True)

# material law for inductor region
lawind = LinearElecMagInductorDG3DMaterialLaw(lawnumind,rhoind,Eind,Eind,Eind,Muind,Muind,Muind,Gind,Gind,Gind,alphaind,betaind,gammaind,
tinitial,Kxind,Kyind,Kzind,alphaThermind,alphaThermind,alphaThermind,lxind,lyind,lzind,seebeckind,cpind,v0ind, mag_mu_x_ind, mag_mu_y_ind, mag_mu_z_ind, 
magpot0_x_ind,magpot0_y_ind, magpot0_z_ind, Irms, freq, nTurnsCoil, coilLx, coilLy, coilLz, coilW, 
Centroid_x,Centroid_y,Centroid_z,centralAxis_x,centralAxis_y,centralAxis_z,useFluxT)

#lawind.setUseBarF(True)

#material law for free space (vacuum)
lawvac = LinearElecMagTherMechDG3DMaterialLaw(lawnumvac,rhovac,Evac,Evac,Evac,Muvac,Muvac,Muvac,Gvac,Gvac,Gvac,alphavac,betavac,gammavac,tinitial,
Kxvac,Kyvac,Kzvac,alphaThermvac,alphaThermvac,alphaThermvac,lxvac,lyvac,lzvac,seebeckvac,cpvac,v0vac, mag_mu_x_vac, mag_mu_y_vac, mag_mu_z_vac, magpot0_x_vac,
magpot0_y_vac, magpot0_z_vac, Irms, freq, nTurnsCoil, coilLx, coilLy, coilLz, coilW,useFluxT)

#lawvac.setUseBarF(True)

# creation of ElasticField
SMPfield = 1000 # number of the field (physical number of SMP)
Indfield = 2000 # number of the field (physical number of Inductor region)
Vacfield = 3000 # number of the field (physical number of Vacuum)
SurfVacOut = 3333 # number of the field (outer surface of Vacuum) Used for BC
SurfInd = 2222 # outer surface of inductor region
SurfSmp = 11110 # outer surface of conductor SMP
SmpRef=11111 # top face of outer surface of SMP
SmpRefBottom=11115 # bottom face of outer surface of SMP
SmpRefPoint=11112 # single point on top face of SMP

space1 = 0 # function space (Lagrange=0)

SMP_field = ElecMagTherMechDG3DDomain(10,SMPfield,space1,lawnum,fullDg,eqRatio,3)
Inductor_field = ElecMagTherMechDG3DDomain(10,Indfield,space1,lawnumind,fullDg,eqRatio,3)
Vacuum_field = ElecMagTherMechDG3DDomain(10,Vacfield,space1,lawnumvac,fullDg,eqRatio,3)

SMP_field.setConstitutiveExtraDofDiffusionEqRatio(eqRatio)
Inductor_field.setConstitutiveExtraDofDiffusionEqRatio(eqRatio)
Vacuum_field.setConstitutiveExtraDofDiffusionEqRatio(eqRatio)
SMP_field.setConstitutiveCurlEqRatio(eqRatio)
Inductor_field.setConstitutiveCurlEqRatio(eqRatio)
Vacuum_field.setConstitutiveCurlEqRatio(eqRatio)
SMP_field.stabilityParameters(beta1)
Inductor_field.stabilityParameters(beta1)
Vacuum_field.stabilityParameters(beta1)
#Matixfield.setConstitutiveExtraDofDiffusionStabilityParameters(beta1,bool(1))
#Fiberfield.setConstitutiveExtraDofDiffusionStabilityParameters(beta1,bool(1))


# flag to fix the reinitialization and 
# reevaluation of field after restart
# args: boolean flag and field index number
SMP_field.setEvaluateMecaField(evaluateMecaField) # fix displacements and don't compute
Inductor_field.setEvaluateMecaField(evaluateMecaField)
Vacuum_field.setEvaluateMecaField(evaluateMecaField)


thermalSource=True
mecaSource=False

SMP_field.setConstitutiveExtraDofDiffusionAccountSource(thermalSource,mecaSource)
Vacuum_field.setConstitutiveExtraDofDiffusionAccountSource(thermalSource,mecaSource)
Inductor_field.setConstitutiveExtraDofDiffusionAccountSource(thermalSource,mecaSource)

#interface // we need to create the ininterdomain after adding the domains
#myinterfield=interDomainBetween3D(12,Matixfield,Fiberfield) 
#myinterfield.setConstitutiveExtraDofDiffusionEqRatio(eqRatio)
#myinterfield.stabilityParameters(beta1)
#myinterfield.setConstitutiveExtraDofDiffusionStabilityParameters(beta1,bool(1))
#myinterfield.matrixByPerturbation(0,1e-8)

#SMP_field.matrixByPerturbation(1,1,1,1.e-3)
#Inductor_field.matrixByPerturbation(1,1,1,1.e-3)
#Vacuum_field.matrixByPerturbation(1,1,1,1.e-3)

# creation of Solver
mysolver = nonLinearMechSolver(1000)
#mysolver.createModel(geofile,meshfile,3,1)
mysolver.loadModel(meshfile)
mysolver.addDomain(SMP_field)
mysolver.addDomain(Inductor_field)
mysolver.addDomain(Vacuum_field)
mysolver.addMaterialLaw(lawsmp)
mysolver.addMaterialLaw(lawind)
mysolver.addMaterialLaw(lawvac)
mysolver.Scheme(soltype)
mysolver.Solver(sol)
mysolver.snlData(nstep,ftime,tol,1.e-9)#forsmp
mysolver.options("-ksp_type preonly")
mysolver.options("-pc_type lu")

#mysolver.snlData(nstep,ftime,tol,1.e-19)
mysolver.stepBetweenArchiving(nstepArch)
mysolver.snlManageTimeStep(25, 3, 2, 10)

# BC
##cyclicFunctionDisp=cycleFunctionTime(0., 0., ftime/5., -0.0001, ftime , -0.0001);
#mysolver.displacementBC("Face",83,2,0.)
mysolver.displacementBC("Volume",SMPfield,0,0.0)
mysolver.displacementBC("Volume",SMPfield,1,0.0)
mysolver.displacementBC("Volume",SMPfield,2,0.0)
mysolver.displacementBC("Volume",Indfield,0,0.0)
mysolver.displacementBC("Volume",Indfield,1,0.0)
mysolver.displacementBC("Volume",Indfield,2,0.0)
mysolver.displacementBC("Volume",Vacfield,0,0.0)
mysolver.displacementBC("Volume",Vacfield,1,0.0)
mysolver.displacementBC("Volume",Vacfield,2,0.0)

#thermal BC
cyclicFunctionTemp1=cycleFunctionTime(0., tinitial,ftime,tinitial);
#mysolver.displacementBC("Face",85,3,cyclicFunctionTemp1)
#mysolver.initialBC("Volume","Position",Mfield,3,tinitial)
mysolver.initialBC("Volume","Position",SMPfield,3,tinitial)
mysolver.initialBC("Volume","Position",Indfield,3,tinitial)
mysolver.initialBC("Volume","Position",Vacfield,3,tinitial)
mysolver.displacementBC("Volume",Indfield,3,cyclicFunctionTemp1)
#mysolver.displacementBC("Volume",SMPfield,3,cyclicFunctionTemp1)
#mysolver.displacementBC("Volume",Vacfield,3,cyclicFunctionTemp1)
mysolver.displacementBC("Face",SurfVacOut,3,cyclicFunctionTemp1)
#mysolver.displacementBC("Face",SmpRef,3,cyclicFunctionTemp1)
#mysolver.displacementBC("Face",SmpRefBottom,3,cyclicFunctionTemp1)

#electrical BC
#cyclicFunctionvolt1=cycleFunctionTime(0., 0.,  ftime,10.);
#mysolver.displacementBC("Face",85,4,cyclicFunctionvolt1)
mysolver.displacementBC("Volume",Indfield,4,0.0)
mysolver.initialBC("Volume","Position",SMPfield,4,0.0)
mysolver.initialBC("Volume","Position",Vacfield,4,0.0)
mysolver.displacementBC("Face",SurfVacOut,4,0.0)
#mysolver.displacementBC("Face",SmpRef,4,0.0)
mysolver.displacementBC("Node",SmpRefPoint,4,0.0)

#magentic
mysolver.curlDisplacementBC("Face",SurfVacOut,5,0.0) # comp may also be 5
#Gauging the edges using tree-cotree methodcd
PhysicalCurves = "" 		# input required as a string of comma separated ints
PhysicalSurfaces = "11110,2222,3333" # input required as a string of comma separated ints
PhysicalVolumes = "1000,2000,3000" 	# input required as a string of comma separated ints
OutputPhysical = 55 		# input required as a int
mysolver.createTreeForBC(PhysicalCurves,PhysicalSurfaces,PhysicalVolumes,OutputPhysical)
mysolver.curlDisplacementBC("Edge",OutputPhysical,5,0.0)

#mysolver.internalPointBuildView("svm",IPField.SVM, 1, 1)
#mysolver.internalPointBuildView("sig_xx",IPField.SIG_XX, 1, 1)
#mysolver.internalPointBuildView("sig_yy",IPField.SIG_YY, 1, 1)
#mysolver.internalPointBuildView("sig_zz",IPField.SIG_ZZ, 1, 1)
#mysolver.internalPointBuildView("sig_xy",IPField.SIG_XY, 1, 1)
#mysolver.internalPointBuildView("sig_yz",IPField.SIG_YZ, 1, 1)
#mysolver.internalPointBuildView("sig_xz",IPField.SIG_XZ, 1, 1)
mysolver.internalPointBuildView("temperature",IPField.TEMPERATURE, 1, 1)
mysolver.internalPointBuildView("w_AV",IPField.EMFIELDSOURCE, 1, 1)
mysolver.internalPointBuildView("w_T",IPField.THERMALSOURCE, 1, 1)
mysolver.internalPointBuildView("jyx",IPField.THERMALFLUX_X, 1, 1)
mysolver.internalPointBuildView("jyy",IPField.THERMALFLUX_Y, 1, 1)
mysolver.internalPointBuildView("jyz",IPField.THERMALFLUX_Z, 1, 1)
mysolver.internalPointBuildView("voltage",IPField.VOLTAGE, 1, 1)
mysolver.internalPointBuildView("jex",IPField.ELECTRICALFLUX_X, 1, 1)
mysolver.internalPointBuildView("jey",IPField.ELECTRICALFLUX_Y, 1, 1)
mysolver.internalPointBuildView("jez",IPField.ELECTRICALFLUX_Z, 1, 1)
#mysolver.internalPointBuildView("ax",IPField.MAGNETICVECTORPOTENTIAL_X, 1, 1)
#mysolver.internalPointBuildView("ay",IPField.MAGNETICVECTORPOTENTIAL_Y, 1, 1)
#mysolver.internalPointBuildView("az",IPField.MAGNETICVECTORPOTENTIAL_Z, 1, 1)
mysolver.internalPointBuildView("bx",IPField.MAGNETICINDUCTION_X, 1, 1)
mysolver.internalPointBuildView("by",IPField.MAGNETICINDUCTION_Y, 1, 1)
mysolver.internalPointBuildView("bz",IPField.MAGNETICINDUCTION_Z, 1, 1)
mysolver.internalPointBuildView("hx",IPField.MAGNETICFIELD_X, 1, 1)
mysolver.internalPointBuildView("hy",IPField.MAGNETICFIELD_Y, 1, 1)
mysolver.internalPointBuildView("hz",IPField.MAGNETICFIELD_Z, 1, 1)
mysolver.internalPointBuildView("js0_x",IPField.INDUCTORSOURCEVECTORFIELD_X, 1, 1)
mysolver.internalPointBuildView("js0_y",IPField.INDUCTORSOURCEVECTORFIELD_Y, 1, 1)
mysolver.internalPointBuildView("js0_z",IPField.INDUCTORSOURCEVECTORFIELD_Z, 1, 1)
#mysolver.internalPointBuildView("wEM_x",IPField.EMSOURCEVECTORFIELD_X, 1, 1)
#mysolver.internalPointBuildView("wEM_y",IPField.EMSOURCEVECTORFIELD_Y, 1, 1)
#mysolver.internalPointBuildView("wEM_z",IPField.EMSOURCEVECTORFIELD_Z, 1, 1)

#mysolver.archivingIPOnPhysicalGroup("Volume",Mfield, IPField.SIG_ZZ,IPField.MEAN_VALUE,nstepArch);
#mysolver.archivingIPOnPhysicalGroup("Volume",Mfield, IPField.STRAIN_ZZ,IPField.MEAN_VALUE,nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",SMPfield, IPField.TEMPERATURE,IPField.MEAN_VALUE,nstepArch);
#mysolver.archivingIPOnPhysicalGroup("Volume",Mfield, IPField.VOLTAGE,IPField.MEAN_VALUE,nstepArch);
#mysolver.archivingIPOnPhysicalGroup("Volume",Ffield, IPField.SIG_ZZ,IPField.MEAN_VALUE,nstepArch);
#mysolver.archivingIPOnPhysicalGroup("Volume",Ffield, IPField.STRAIN_ZZ,IPField.MEAN_VALUE,nstepArch);
#mysolver.archivingIPOnPhysicalGroup("Volume",Ffield, IPField.TEMPERATURE,IPField.MEAN_VALUE,nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",SMPfield, IPField.VOLTAGE,IPField.MEAN_VALUE,nstepArch);
#mysolver.archivingIPOnPhysicalGroup("Volume",Indfield, IPField.VOLTAGE,IPField.MEAN_VALUE,nstepArch);
#mysolver.archivingIPOnPhysicalGroup("Volume",Vacfield, IPField.VOLTAGE,IPField.MEAN_VALUE,nstepArch);

mysolver.archivingForceOnPhysicalGroup("Face", SurfSmp, 3, 1);
mysolver.archivingForceOnPhysicalGroup("Face", SurfSmp, 4, 1);
mysolver.archivingForceOnPhysicalGroup("Face", SurfInd, 4, 1);
mysolver.archivingForceOnPhysicalGroup("Face", SurfVacOut, 4, 1);
mysolver.archivingForceOnPhysicalGroup("Face", SurfSmp, 5, 1);
mysolver.archivingForceOnPhysicalGroup("Face", SurfInd, 5, 1);
mysolver.archivingForceOnPhysicalGroup("Face", SurfVacOut, 5, 1);

mysolver.archivingNodeDisplacement(SmpRefPoint,0,1);
mysolver.archivingNodeDisplacement(SmpRefPoint,1,1);
mysolver.archivingNodeDisplacement(SmpRefPoint,2,1);
mysolver.archivingNodeDisplacement(SmpRefPoint,3,1);
mysolver.archivingNodeDisplacement(SmpRefPoint,4,1);

#mysolver.archivingNodeIP(SmpRef, IPField.THERMALFLUX_X,IPField.MEAN_VALUE,1);
#mysolver.archivingNodeIP(SmpRef, IPField.ELECTRICALFLUX_X,IPField.MEAN_VALUE,1);

mysolver.solve()
check = TestCheck()
check.equal(0.000000e+00,mysolver.getArchivedForceOnPhysicalGroup("Face", SurfSmp, 3),1.e-5)
check.equal(-9.427355e-10,mysolver.getArchivedForceOnPhysicalGroup("Face", SurfSmp, 4),1.e1)
check.equal(9.484542e-05,mysolver.getArchivedForceOnPhysicalGroup("Face", SurfInd, 4),1.e1)
check.equal(-7.110362e-08,mysolver.getArchivedForceOnPhysicalGroup("Face", SurfVacOut, 4),1.e-1)
check.equal(-4.387605e-01,mysolver.getArchivedForceOnPhysicalGroup("Face", SurfSmp, 5),1.e-5)

