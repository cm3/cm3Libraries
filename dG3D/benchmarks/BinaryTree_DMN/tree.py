from gmshpy import *
from dG3Dpy import*
import matplotlib.pyplot as plt
import pandas as pd

tree = Tree()
tree.createPerfectTree(3,2,1)
tree.initialize()
tree.printTree()

node = tree.getNode(3,0)

tree.printAssociatedLeavesForNode(node)
tree.printAllRegularNodes()

x = Tensor26(1)
x.printData("x")
x.setVal(0,1,1000)
x.printData("x")
