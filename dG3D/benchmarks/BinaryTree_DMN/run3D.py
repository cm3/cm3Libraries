from gmshpy import *
from dG3Dpy import*
import matplotlib.pyplot as plt
import pandas as pd

tree = Tree()
tree.createPerfectTree(10,2,2)
tree.initialize()
#tree.printTree()

# material law
E1 = 32E2
nu1 = 0.2
rho1 = 2.7e-9 # Bulk mass
# creation of material law
#law1 = dG3DLinearElasticMaterialLaw(11,rho1,E1,nu1)
pot =  biLogarithmicElasticPotential(11,E1,nu1,9)
law1 = dG3DHyperelasticMaterialLaw(11,rho1,pot)


# material law
E2 = 3.2E3
nu2 = 0.3
rho2 = 2.7e-9 # Bulk mass
sy02   = 100.
h2    = E2/500.
# creation of material law
#law2 = dG3DLinearElasticMaterialLaw(12,rho2,E2,nu2)
#law2 = J2SmallStrainDG3DMaterialLaw(12,rho2,E2,nu2,sy02, h2)
law2 = J2LinearDG3DMaterialLaw(12,rho2,E2,nu2,sy02, h2)

dmn = DeepMaterialNetwork(tree, law1, law2)

#init solver
dmn.initSolve()

eps00 = []
sig00 = []

eps11 = []
sig11 = []

eps22 = []
sig22 = []

eps01 = []
sig01 = []


epsMat = fullMatrixDouble(3,3)
epsMat.set(0,0,1.)
epsMat.set(1,1,1.)
epsMat.set(2,2,1.)

deps = 1e-3
maxFailed = 5
failed = 0

for i in range(0,300):
  neweps01 = epsMat(0,1)+deps
  neweps00 = epsMat(0,0)+deps*0.3
  neweps22 = epsMat(2,2)+deps*0.5
  epsMat.set(0,0,neweps00)
  epsMat.set(0,1,neweps01)
  epsMat.set(2,2,neweps22)
  sigMat = fullMatrixDouble(3,3)
  ok = dmn.solve(epsMat,sigMat,10)
  if ok:
    eps00.append([epsMat(0,0)])
    sig00.append([sigMat(0,0)])
    
    eps11.append([epsMat(1,1)])
    sig11.append([sigMat(1,1)])
    
    eps22.append([epsMat(2,2)])
    sig22.append([sigMat(2,2)])
    
    eps01.append([epsMat(0,1)])
    sig01.append([sigMat(0,1)])
    
  else:
    neweps01 = epsMat(0,1)-deps
    neweps00 = epsMat(0,0)-deps*0.3
    neweps22 = epsMat(2,2)-deps*0.5
    epsMat.set(0,0,neweps00)
    epsMat.set(0,1,neweps01)
    epsMat.set(2,2,neweps22)
    deps *= 0.5
    failed += 1
    if failed > maxFailed:
      break
 
plt.close('all')
plt.plot(eps00, sig00,'o-',label='P00')
plt.plot(eps00, sig01,'v--',label='P01')
plt.plot(eps00, sig11,'s-',label='P11')
plt.plot(eps00, sig22,'*--',label='P22')
plt.legend(loc='lower right')

plt.xlabel('F00')
plt.show()


