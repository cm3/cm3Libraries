#coding-Utf-8-*-
from gmshpy import *
from dG3Dpy import*

#script to launch beam problem with a python script

# material law
lawnum = 1 # unique number of law
rho   = 7850
young = 210.e9
nu    = 0.3 
sy0   = 500e6
h     = 400e8


# geometry
geofile="cubeTet_ord3.geo" # name of mesh file
meshfile="cubeTet_ord3.msh" # name of mesh file

# solver
sol = 2  # Gmm=0 (default) Taucs=1 PETsc=2
soltype = 2 # StaticLinear=0 (default) StaticNonLinear=1
nstep = 500   # number of step (used only if soltype=1)
ftime =1.e-7   # Final time (used only if soltype=1)
tol=1.e-6   # relative tolerance for NR scheme (used only if soltype=1)
nstepArch=500 # Number of step between 2 archiving (used only if soltype=1)
fullDg = 1 #O = CG, 1 = DG
space1 = 0 # function space (Lagrange=0)
beta1 = 200

#  compute solution and BC (given directly to the solver
# creation of law
law1 = J2LinearDG3DMaterialLaw(lawnum,rho,young,nu,sy0,h)

# creation of ElasticField
nfield = 10 # number of the field (physical number of surface)
myfield1 = dG3DDomain(1000,nfield,space1,lawnum,fullDg)
myfield1.stabilityParameters(beta1)
#myfield1.matrixByPerturbation(1,1,1,1e-8)
# creation of Solver
mysolver = nonLinearMechSolver(1000)
#mysolver.createModel(geofile,meshfile,3,3)
mysolver.loadModel(meshfile)
mysolver.addDomain(myfield1)
mysolver.addMaterialLaw(law1)
mysolver.Scheme(soltype)
mysolver.Solver(sol)
#mysolver.snlData(nstep,ftime,tol)
mysolver.stepBetweenArchiving(nstepArch)
mysolver.explicitSpectralRadius(ftime,0.8,1.)
mysolver.explicitTimeStepEvaluation(nstep)
# BC
#shearing
#mysolver.displacementBC("Face",1234,0,0.)
#mysolver.displacementBC("Face",1234,1,0.)
#mysolver.displacementBC("Face",1234,2,0.)
#mysolver.displacementBC("Face",5678,1,0.00)
#mysolver.displacementBC("Face",5678,0,0.00001)
#mysolver.forceBC("Face",2376,2,100000000)
#mysolver.forceBC("Face",4158,2,-100000000)
#mysolver.forceBC("Face",5678,0,100000000)
#tension along z
mysolver.displacementBC("Face",1234,2,0.)
mysolver.displacementBC("Face",5678,2,0.00005/ftime)
mysolver.displacementBC("Face",2376,0,0.)
mysolver.displacementBC("Face",1265,1,0.)


#mysolver.internalPointBuildView("svm",IPField.SVM, 1, 1)
mysolver.internalPointBuildView("sig_xx",IPField.SIG_XX, 1, 1)
mysolver.internalPointBuildView("sig_yy",IPField.SIG_YY, 1, 1)
mysolver.internalPointBuildView("sig_zz",IPField.SIG_ZZ, 1, 1)
#mysolver.internalPointBuildView("sig_xy",IPField.SIG_XY, 1, 1)
#mysolver.internalPointBuildView("sig_yz",IPField.SIG_YZ, 1, 1)
#mysolver.internalPointBuildView("sig_xz",IPField.SIG_XZ, 1, 1)
#mysolver.internalPointBuildView("epl",IPField.PLASTICSTRAIN, 1, 1)
mysolver.archivingForceOnPhysicalGroup("Face", 1234, 2)
#mysolver.archivingForceOnPhysicalGroup("Face", 5678, 2)


mysolver.solve()

try:
  import linecache
  linedisp = linecache.getline('force1234comp2_part0.csv',27077)
except:
  print('Cannot read the results in the files')
  import os
  os._exit(1)
else:
  check = TestCheck()
  check.equal(-8.877662e-05,float(linedisp.split(';')[1]))




