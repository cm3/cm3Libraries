SetFactory('OpenCASCADE');
Mesh.CharacteristicLengthMin =0.3;
Mesh.CharacteristicLengthMax =0.4;
Mesh.SecondOrderLinear = 1;
rc=0.5;

nx=1;
ny=1;
nz=1;

Box(1) = {0,0,0,7.0,7.0,7.0};
Cylinder(2) = {0,0,0,3.5,3.5,3.5,rc,2*Pi};
Rotate {{3.5,3.5,3.5}, {0,0,0}, Pi} {Volume{2}; }
Cylinder(3) = {0,7.0,0,3.5,-3.5,3.5,rc,2*Pi};
Rotate {{3.5,-3.5,3.5}, {0,7.0,0}, Pi} {Volume{3}; }
Cylinder(4) = {3.5,3.5,3.5,-3.5,3.5,3.5,rc,2*Pi};
Rotate {{-3.5,3.5,3.5}, {3.5,3.5,3.5}, Pi} {Volume{4}; }
Cylinder(5) = {3.5,3.5,3.5,-3.5,-3.5,3.5,rc,2*Pi};
Rotate {{-3.5,-3.5,3.5}, {3.5,3.5,3.5}, Pi} {Volume{5}; }
Cylinder(6) = {7.0,0,0,-3.5,3.5,3.5,rc,2*Pi};
Cylinder(7) = {7.0,7.0,0,-3.5,-3.5,3.5,rc,2*Pi};
Cylinder(8) = {3.5,3.5,3.5,3.5,3.5,3.5,rc,2*Pi};
Cylinder(9) = {3.5,3.5,3.5,3.5,-3.5,3.5,rc,2*Pi};
Cylinder(10) = {0,0,0,0,7.0,0,rc,2*Pi};
Cylinder(11) = {7.0,0,0,0,7.0,0,rc,2*Pi};
Cylinder(12) = {7.0,7.0,7.0,-7.0,0,0,rc,2*Pi};
Cylinder(13) = {7.0,7.0,7.0,0,0,-7.0,rc,2*Pi};
Cylinder(14) = {7.0,7.0,7.0,0,-7.0,0,rc,2*Pi};
Cylinder(15) = {0,0,7.0,7.0,0,0,rc,2*Pi};
Cylinder(16) = {0,0,7.0,0,0,-7.0,rc,2*Pi};
Cylinder(17) = {0,0,7.0,0,7.0,0,rc,2*Pi};
Cylinder(18) = {0,0,0,7.0,0,0,rc,2*Pi};
Cylinder(19) = {0,7.0,0,7.0,0,0,rc,2*Pi};
Cylinder(20) = {0,7.0,0,0,0,7.0,rc,2*Pi};
Cylinder(21) = {7.0,0,0,0,0,7.0,rc,2*Pi};
BooleanUnion(22) = {Volume{2};Volume{3};Volume{4};Volume{5};Volume{6};Volume{7};Volume{8};Volume{10};Volume{11};Volume{12};Volume{13};Volume{14};Volume{15};Volume{16};Volume{17};Volume{18};Volume{19};Volume{20};Volume{21};Delete; }{ Volume{9}; Delete; };
BooleanIntersection(23)={Volume{22}; Delete;}{Volume{1}; Delete;};
vollist()={};

For i In {0:nx-1:1}
  For j In {0:ny-1:1}
    For k In {0:nz-1:1}
      vol[]=Translate {i*7, j*7, k*7} {Duplicata { Volume{23}; }};
      //Printf("vol",vol());
      vollist+=vol[0];
    EndFor
  EndFor
EndFor

//Printf("volume",vollist());

finalvol=newv;
BooleanUnion(finalvol)={ Volume{ vollist[{0:#vollist()-1}] }; Delete; }{ Volume{23}; Delete; };

e=1.e-6;
Rx=7.0*nx;
Ry=7.0*ny;
Rz=7.0*nz;
bottomx() = Surface In BoundingBox{-e,-e,-e, e,Ry+e,Rz+e};
bottomy() = Surface In BoundingBox{-e,-e,-e, Rx+e,e,Rz+e};
bottomz() = Surface In BoundingBox{-e,-e,-e, Rx+e,Ry+e,e};
Mytopx() = Surface In BoundingBox{Rx-e,-e,-e, Rx+e,Ry+e,Rz+e};
Mytopy() = Surface In BoundingBox{-e,Ry-e,-e, Rx+e,Ry+e,Rz+e};
Mytopz() = Surface In BoundingBox{-e,-e,Rz-e, Rx+e,Ry+e,Rz+e};

Physical Surface(100) = {bottomx()};
Physical Surface(110) = {bottomy()};
Physical Surface(120) = {bottomz()};

Physical Surface(101) = {Mytopx()};
Physical Surface(111) = {Mytopy()};
Physical Surface(121) = {Mytopz()};

Physical Volume(51) ={finalvol};
