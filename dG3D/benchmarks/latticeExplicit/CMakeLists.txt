# test file

set(PYFILE test_vevp_traction_structure_3Cell.py)

set(FILES2DELETE 
  *.csv
  disp*
  stress*
)

add_cm3python_mpi_test(4 ${PYFILE} "${FILES2DELETE}")
