clear
clc
clf
close all

%---TEMPERATURE-----------------------------------------------------------------
XYZ= dlmread(strcat(pwd(),'/previousScheme/','IPVolume10val_TEMPERATUREMean.csv'));
t1= XYZ(:, 1);
dlmwrite('temp.csv',XYZ,'delimiter',';');
XYZ =dlmread('IPVolume10val_TEMPERATUREMean.csv');
t2= max(t1)+XYZ(:, 1);
dlmwrite('temp.csv',XYZ,'delimiter',';','-append');
XYZ =dlmread('temp.csv');

t=[t1;t2];
DATA(:,1)= t;
T_Mean= XYZ(:, 2);
DATA(:,2)= T_Mean;
delete('temp.csv')

%---STRAIN----------------------------------------------------------------------
XYZ= dlmread(strcat(pwd(),'/previousScheme/','IPVolume10val_STRAIN_ZZMean.csv'));
dlmwrite('temp.csv',XYZ,'delimiter',';');
XYZ =dlmread('IPVolume10val_STRAIN_ZZMean.csv');
dlmwrite('temp.csv',XYZ,'delimiter',';','-append');
XYZ =dlmread('temp.csv');
STRAIN_ZZMean= XYZ(:, 2);
DATA(:,10)= STRAIN_ZZMean;


%---STRESS----------------------------------------------------------------------
XYZ= dlmread(strcat(pwd(),'/previousScheme/','IPVolume10val_SIG_ZZMean.csv'));
dlmwrite('temp.csv',XYZ,'delimiter',';');
XYZ =dlmread('IPVolume10val_SIG_ZZMean.csv');
dlmwrite('temp.csv',XYZ,'delimiter',';','-append');
XYZ =dlmread('temp.csv');
SIG_ZZMean= XYZ(:, 2);
DATA(:,16)= SIG_ZZMean;

%%---FORCE-----------------------------------------------------------------------
XYZ= dlmread(strcat(pwd(),'/previousScheme/','force1234comp2.csv'));
dlmwrite('temp.csv',XYZ,'delimiter',';');
XYZ =dlmread('force1234comp2.csv');
dlmwrite('temp.csv',XYZ,'delimiter',';','-append');
XYZ =dlmread('temp.csv');
F1234_Y= XYZ(:, 2);
DATA(:,21)= F1234_Y;

XYZ= dlmread(strcat(pwd(),'/previousScheme/','force1234comp3.csv'));
dlmwrite('temp.csv',XYZ,'delimiter',';');
XYZ =dlmread('force1234comp3.csv');
dlmwrite('temp.csv',XYZ,'delimiter',';','-append');
XYZ =dlmread('temp.csv');
F1234_Z= XYZ(:, 2);
DATA(:,22)= F1234_Z;
delete('temp.csv')


%---problem specific--------------------------------------------------------
figure (20)
subplot (3, 4, 1)
plot(t, T_Mean, 'linewidth',2,'b-')
set(gca,'FontSize',14)
%title ("t & T-Mean");
xlabel '\bft';
ylabel '\bfT-Mean [K]';
grid on

subplot (3, 4, 2)
plot(t, -F1234_Z, 'linewidth',2,'b-')
set(gca,'FontSize',14)
xlabel '\bft';
ylabel '\bf-FZ-Face1234 [N]';
grid on

subplot (3, 4, 3)
plot(t, STRAIN_ZZMean, 'linewidth',2,'b-')
set(gca,'FontSize',14)
xlabel '\bft';
ylabel '\bf\epsilonZZ-Mean';
grid on

subplot (3, 4, 4)
plot(t, SIG_ZZMean/1e6, 'linewidth',2,'b-')
set(gca,'FontSize',14)
xlabel '\bft';
ylabel '\bf\sigmaZZ-Mean [MPa]';
grid on

subplot (3, 4, 5)
plot(STRAIN_ZZMean, SIG_ZZMean/1e6, 'linewidth',2,'b-')
set(gca,'FontSize',14)
xlabel '\bf\epsilonZZ-Mean';
ylabel '\bf\sigmaZZ-Mean [MPa]';
grid on

subplot (3, 4, 6)
plot(T_Mean, SIG_ZZMean/1e6, 'linewidth',2,'b-')
set(gca,'FontSize',14)
xlabel '\bfT-Mean [K]';
ylabel '\bf\sigmaZZ-Mean [MPa]';
grid on

subplot (3, 4, 7)
plot(T_Mean, STRAIN_ZZMean, 'linewidth',2,'b-')
set(gca,'FontSize',14)
xlabel '\bfT-Mean [K]';
ylabel '\bf\epsilonZZ-Mean';
grid on

