#coding-Utf-8-*-
from gmshpy import *
from dG3Dpy import*

#script to launch PBC problem with a python script

#DEFINE MICRO PROBLEM

# micro-material law
lawnum = 11 # unique number of law
K = 76.E3 	# Bulk mudulus
mu =26.E3	  # Shear mudulus
rho = 2700. # Bulk mass
sy0 = 200
h = 0.5*sy0
E = 9.*K*mu/(3.*K+mu)
nu= (3.*K -2.*mu)/2./(3.*K+mu)

# micro-geometry
micromeshfile="micro.msh" # name of mesh file

# creation of material law
law1 = dG3DLinearElasticMaterialLaw(lawnum,rho,E,nu)

# creation of  micro part Domain
nfield = 11 # number of the field (physical number of entity)
dim =2
myfield1 = dG3DDomain(10,nfield,0,lawnum,0,dim)

microBC = nonLinearPeriodicBC(10,2)
microBC.setOrder(2)
microBC.setBCPhysical(1,2,3,4)

# periodiodic BC
method = 0 # Periodic mesh = 0 Langrange interpolation = 1 Cubic spline interpolation =2
degree = 5 
addvertex = 0
microBC.setPeriodicBCOptions(method, degree,bool(addvertex))

# DEFINE MACROPROBLEM
matnum = 1;
macromat = hoDGMultiscaleMaterialLaw(matnum, 1000)
macromat.addViewMicroSolver(45,0)
macromat.addViewMicroSolver(55,0)

baseSolver1 = macromat.getMicroSolver()
baseSolver1.loadModel(micromeshfile);
baseSolver1.addDomain(myfield1)
baseSolver1.addMaterialLaw(law1);
baseSolver1.addMicroBC(microBC)

#baseSolver1.snlData(1,1,1e-6,1e-10)
#baseSolver1.setSystemType(1)
#baseSolver1.Solver(2)
#baseSolver1.Scheme(1)

baseSolver1.displacementBC("Face",11,2,0.) # fix out-of-plan displacement 

macromeshfile="shearlayer.msh" # name of mesh file

# solver
sol = 2  # Gmm=0 (default) Taucs=1 PETsc=2
soltype =1 # StaticLinear=0 (default) StaticNonLinear=1
nstep = 1  # number of step (used only if soltype=1)
ftime =1.   # Final time (used only if soltype=1)
tol=1.e-6  # relative tolerance for NR scheme (used only if soltype=1)
nstepArch=1 # Number of step between 2 archiving (used only if soltype=1)


# creation of  macro part Domain

dim =2
beta1 = 150;
fdg=0

nfield1 = 11
macrodomain1 = hoDGDomain(11,nfield1,0,matnum,fdg,dim)
macrodomain1.stabilityParameters(beta1)

nfield2 = 12
macrodomain2 = hoDGDomain(12,nfield2,0,matnum,fdg,dim)
macrodomain2.stabilityParameters(beta1)

nfield3 = 13
macrodomain3 = hoDGDomain(13,nfield3,0,matnum,fdg,dim)
macrodomain3.stabilityParameters(beta1)

interdomain1 = hoDGInterDomain(14,macrodomain1,macrodomain2)
interdomain1.stabilityParameters(beta1)

interdomain2 = hoDGInterDomain(15,macrodomain1,macrodomain3)
interdomain2.stabilityParameters(beta1)

# creation of Solver
mysolver = nonLinearMechSolver(1000)
mysolver.loadModel(macromeshfile)
mysolver.addDomain(macrodomain1)
mysolver.addDomain(macrodomain2)
mysolver.addDomain(macrodomain3)
mysolver.addDomain(interdomain1)
mysolver.addDomain(interdomain2)
mysolver.addMaterialLaw(macromat)
mysolver.setMultiscaleFlag(bool(1))
mysolver.Scheme(soltype)
mysolver.Solver(sol)
mysolver.snlData(nstep,ftime,tol)


# boundary condition
mysolver.displacementBC("Face",11,2,0.0)
mysolver.displacementBC("Face",12,2,0.0)
mysolver.displacementBC("Face",13,2,0.0)

mysolver.displacementBC("Face",12,1,0.0)
mysolver.displacementBC("Face",12,0,0.0)
mysolver.displacementBC("Face",13,0,0.0)
mysolver.displacementBC("Face",13,1,0.5)

mysolver.periodicBC("Edge",1,3,1,4,0)
mysolver.periodicBC("Edge",1,3,1,4,1)

# archivage
mysolver.internalPointBuildView("Green-Lagrange_xx",IPField.STRAIN_XX, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_yy",IPField.STRAIN_YY, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_zz",IPField.STRAIN_ZZ, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_xy",IPField.STRAIN_XY, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_yz",IPField.STRAIN_YZ, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_xz",IPField.STRAIN_XZ, 1, 1);
mysolver.internalPointBuildView("sig_xx",IPField.SIG_XX, 1, 1);
mysolver.internalPointBuildView("sig_yy",IPField.SIG_YY, 1, 1);
mysolver.internalPointBuildView("sig_zz",IPField.SIG_ZZ, 1, 1);
mysolver.internalPointBuildView("sig_xy",IPField.SIG_XY, 1, 1);
mysolver.internalPointBuildView("sig_yz",IPField.SIG_YZ, 1, 1);
mysolver.internalPointBuildView("sig_xz",IPField.SIG_XZ, 1, 1);
mysolver.internalPointBuildView("sig_VM",IPField.SVM, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange equivalent strain",IPField.GL_EQUIVALENT_STRAIN, 1, 1);
mysolver.internalPointBuildView("Equivalent plastic strain",IPField.PLASTICSTRAIN, 1, 1);

mysolver.archivingForceOnPhysicalGroup("Edge",2,0)
mysolver.archivingForceOnPhysicalGroup("Edge",2,1)

# solve
mysolver.solve()

#test check
check = TestCheck()
check.equal(1.012053e+03,mysolver.getArchivedForceOnPhysicalGroup("Edge",2,1),1.e-5)

