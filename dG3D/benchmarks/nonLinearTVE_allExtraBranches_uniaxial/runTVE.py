#coding-Utf-8-*-

from gmshpy import *
from dG3Dpy import*
# from dG3DpyDebug import*
from math import*
import csv
import numpy as np
import os

# Load the csv data for relaxation spectrum
with open('relaxationSpectrum_Et_N27_19_04_24_Tref20.txt', newline='') as csvfile:
    reader = csv.reader(csvfile, delimiter=';')
    relSpec = np.zeros((1,))
    i = 0
    for row in reader:
        if i == 0:
            relSpec[i] = float(' '.join(row))
        else:
            relSpec = np.append(relSpec, float(' '.join(row)))
        i += 1
# print(relSpec)
N = int(0.5*(i-1))
relSpec[0:N+1] *= 1e-6

# material law
lawnum = 11 # unique number of law

E = relSpec[0] #MPa # 1300
nu = 0.33
K = E/3./(1.-2.*nu)	# Bulk mudulus
mu = E/2./(1.+nu)	  # Shear mudulus
rho = 905e-12 # Bulk mass   905 kg/m3 -> 905e-12 tonne/mm3
Cp = rho*1900e+6 # 1900 J/kg/K -> 1900e+6 Nmm/tonne/K
KThCon = 0.14  # 0.14 W/m/K -> 0.14 Nmm/s/mm/K
Alpha = 0.6e-6 # 1/K

Tinitial = 273.15 + 23 # K
C1 = 112.94742152
C2 = 176.44294359

# Temp Settings
Tref = 273.15 + 20.

# Default tempfuncs
Alpha_G = 0.2e-6
Alpha_R = 1.e-6
KThCon_G = 0.10
KThCon_R = 1.5*0.12
k_gibson = 3.36435338e-02
m_gibson = 0.0
Tg = Tref
Alpha_tempfunc = GlassTransitionScalarFunction(1, Alpha_R/Alpha_G, Tg, k_gibson, m_gibson, 0)
KThCon_tempfunc = GlassTransitionScalarFunction(1, KThCon_R/KThCon_G, Tg, k_gibson, m_gibson, 0)


law1 = NonLinearTVMDG3DMaterialLaw(lawnum,rho,E,nu,Tinitial,Alpha,KThCon,Cp,False,1e-8)
law1.setStrainOrder(-1)
law1.setViscoelasticMethod(0)
law1.setShiftFactorConstantsWLF(C1,C2)
law1.setReferenceTemperature(Tref)

extraTVE = np.ones((int(0.5*(relSpec.size-1)),13)) # 3 vol + 3 dev + 1 comp
# tension
extraTVE[:,0] = (np.concatenate((np.linspace(0.0,0.0,14),np.linspace(0.0,0.05,13)))) # V0 first at -10
extraTVE[:,1] = np.flip(np.concatenate((np.linspace(1500,10000,13),np.linspace(10000,5000,14)))) # V1 - flipped, last at -10
extraTVE[:,2] = (np.concatenate((np.linspace(0.5,0.5,14),np.linspace(0.5,0.75,13)))) # V2
extraTVE[:,3] = (np.concatenate((np.linspace(0.0,0.0,14),np.linspace(0.0,0.05,13)))) # D0
extraTVE[:,4] =  np.flip(np.concatenate((np.linspace(1500,10000,13),np.linspace(10000,5000,14)))) # D1 - flipped, last at -10
extraTVE[:,5] = (np.concatenate((np.linspace(0.5,0.5,14),np.linspace(0.5,0.75,13)))) # D2
extraTVE[:,6] = np.flip(np.concatenate((np.linspace(1.8,1.8,13),np.linspace(1.8,5.,14)))) # Ci

# compression
extraTVE[:,7] = (np.concatenate((np.linspace(1.0,0.1,14),np.linspace(0.1,0.05,13)))) # extraTVE[:,0]  # V3
extraTVE[:,8] = np.flip(np.concatenate((np.linspace(4500,8500,13),np.linspace(8500,40000,14)))) # extraTVE[:,1]
extraTVE[:,9] = (np.concatenate((np.linspace(1.5,1.25,14),np.linspace(1.25,1.35,13)))) # extraTVE[:,2]
extraTVE[:,10] = (np.concatenate((np.linspace(1.0,0.1,14),np.linspace(0.1,0.05,13)))) # extraTVE[:,3]
extraTVE[:,11] = np.flip(np.concatenate((np.linspace(4500,8500,13),np.linspace(8500,40000,14)))) # extraTVE[:,4]
extraTVE[:,12] = (np.concatenate((np.linspace(1.5,1.25,14),np.linspace(1.25,1.35,13)))) # extraTVE[:,5]
extraTVE.tolist()


law1.setExtraBranchNLType(4)
law1.useExtraBranchBool(True)
law1.useExtraBranchBool_TVE(True,4)
law1.setVolumeCorrection(0.0,1000,1.5,0.0,1000.,1.5)
law1.setAdditionalVolumeCorrections(0.,2000,1.35,0.,2000.,1.35)
law1.setExtraBranch_CompressionParameter(1.8,0.,0.)
law1.setTensionCompressionRegularisation(1000.)
law1.setViscoElasticNumberOfElement(N)
if N > 0:
    for i in range(1, N+1):
        law1.setViscoElasticData(i, relSpec[i], relSpec[i+N])
        law1.setCorrectionsAllBranchesTVE(i, extraTVE[i-1,0],extraTVE[i-1,1],extraTVE[i-1,2],extraTVE[i-1,3],extraTVE[i-1,4],extraTVE[i-1,5])
        law1.setCompressionCorrectionsAllBranchesTVE(i, extraTVE[i-1,6])
        law1.setAdditionalCorrectionsAllBranchesTVE(i, extraTVE[i-1,7],extraTVE[i-1,8],extraTVE[i-1,9],extraTVE[i-1,10],extraTVE[i-1,11],extraTVE[i-1,12])
law1.setTemperatureFunction_ThermalDilationCoefficient(Alpha_tempfunc)
law1.setTemperatureFunction_ThermalConductivity(KThCon_tempfunc)

# geometry
geofile = "line.geo"
meshfile = "line.msh"

# solver
sol = 2  # Gmm=0 (default) Taucs=1 PETsc=2
soltype = 1 # StaticLinear=0 (default) StaticNonLinear=1
nstep = 100   # number of step (used only if soltype=1)
ftime = 50   # Final time (used only if soltype=1)
tol=1.e-5  # relative tolerance for NR scheme (used only if soltype=1)
nstepArch=1 # Number of step between 2 archiving (used only if soltype=1)
fullDg = 0 #O = CG, 1 = DG
space1 = 0 # function space (Lagrange=0)
beta1  = 100

ThermoMechanicsEqRatio = 1.e1
thermalSource = True
mecaSource = True
myfield1 = dG3D1DDomain(11,11,space1,lawnum,0,0,1) #for non local
myfield1.setConstitutiveExtraDofDiffusionAccountSource(thermalSource,mecaSource)
myfield1.stabilityParameters(beta1)
myfield1.ThermoMechanicsStabilityParameters(beta1,bool(1))    # Added
myfield1.setThermoMechanicsEqRatio(ThermoMechanicsEqRatio)

myfield1.setState(1) #UniaxialStrain=0, UniaxialStress =1, ConstantTriaxiality=2
L = 1.
x0 = 0;
A0 = 0.99
xL = L
AL = 1
a = (AL - A0)/(xL-x0)

crossSection = linearScalarFunction(x0,A0,a)
myfield1.setCrossSectionDistribution(crossSection)

# creation of Solver
mysolver = nonLinearMechSolver(1000)
mysolver.createModel(geofile,meshfile,1,1)
mysolver.addDomain(myfield1)
mysolver.addMaterialLaw(law1)
mysolver.Scheme(soltype)
mysolver.Solver(sol)
mysolver.snlData(nstep,ftime,tol)
mysolver.stepBetweenArchiving(nstepArch)

mysolver.snlManageTimeStep(15,5,2.,20) # maximal 20 times

mysolver.displacementBC("Edge",11,1,0.)
mysolver.displacementBC("Edge",11,2,0.)
mysolver.displacementBC("Node",1,0,0.)

fu_L = PiecewiseLinearFunction()
fu_L.put(0,0)
fu_L.put(50.0,0.5) # strain rate = 1e-2

method = 0
withPF = False #True
mysolver.pathFollowing(withPF,method)
if method == 0:
	mysolver.setPathFollowingIncrementAdaptation(True,4)
	mysolver.setPathFollowingControlType(0)
	mysolver.setPathFollowingCorrectionMethod(0)
	mysolver.setPathFollowingArcLengthStep(1e-1)
	mysolver.setBoundsOfPathFollowingArcLengthSteps(0,0.1);
else:
	# time-step adaptation by number of NR iterations
	mysolver.setPathFollowingIncrementAdaptation(True,4)
	mysolver.setPathFollowingLocalSteps(1e-2,1.)
	mysolver.setPathFollowingSwitchCriterion(0.)
	mysolver.setPathFollowingLocalIncrementType(1);
	mysolver.setBoundsOfPathFollowingLocalSteps(1.,3.)

if withPF:
	mysolver.forceBC("Node",2,0,1e2)
else:
    mysolver.displacementBC("Node",2,0,fu_L) # strain rate = 0.04/4. sec^-1

# Temp BCs
mysolver.initialBC("Volume","Position",11,3,Tinitial)
fT = LinearFunctionTime(0,Tinitial,ftime,Tinitial);    # Linear Displacement with time -> Not used, activate to fix temperature.

# Archiving
mysolver.archivingForceOnPhysicalGroup("Node", 1, 0)
mysolver.archivingNodeDisplacement(2,0)
mysolver.archivingAverageValue(IPField.P_XX)
mysolver.archivingAverageValue(IPField.F_XX)

mysolver.solve()

check = TestCheck()
check.equal(-2.691121e+01,mysolver.getArchivedForceOnPhysicalGroup("Node", 1, 0),1.e-4)
