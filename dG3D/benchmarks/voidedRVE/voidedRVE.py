#coding-Utf-8-*-
from gmshpy import *
#from dG3DpyDebug import*
from dG3Dpy import*

#script to launch composite problem with a python script

# material law
rho = 7850. # Bulk mass

# geometry
geofile="void.geo"
meshfile="void.msh" # name of mesh file

lawep = 3 # unique number of law
rhoEP   = 1000.
youngEP = 75.e3
nuEP    = 0.3
sy0EP   = 75.
hEP     = 416.
hexp    = 0.3895

# solver
sol = 2  # Gmm=0 (default) Taucs=1 PETsc=2
soltype = 1 # StaticLinear=0 (default) StaticNonLinear=1
nstep = 8   # number of step (used only if soltype=1)
ftime =1.   # Final time (used only if soltype=1)
tol=1.e-6  # relative tolerance for NR scheme (used only if soltype=1)
nstepArch=1 # Number of step between 2 archiving (used only if soltype=1)
fulldg = 0
beta1 = 30.
eqRatio = 1.e6

BC="PERIODIC"
number=1
#  compute solution and BC (given directly to the solver
# creation of law
# creation of law
harden = PowerLawJ2IsotropicHardening(1,sy0EP, hEP, hexp,0.01)
law1 = J2LinearDG3DMaterialLaw(lawep,rhoEP,youngEP,nuEP,harden)

# creation of ElasticField
Mfield = 12 # number of the field (physical number of Mtrix)

space1 = 0 # function space (Lagrange=0)


Matixfield = dG3DDomain(10,Mfield,space1,lawep,fulldg)
#myfieldEP.matrixByPerturbation(0,1,1,1e-15)
Matixfield.stabilityParameters(beta1)
#myfield1.matrixByPerturbation(1,1e-8)

# creation of Solver
mysolver = nonLinearMechSolver(1000)
#mysolver.createModel(geofile,meshfile,3,2)
mysolver.loadModel(meshfile)
mysolver.addDomain(Matixfield)
mysolver.addMaterialLaw(law1)
mysolver.Scheme(soltype)
mysolver.Solver(sol)
mysolver.snlData(nstep,ftime,tol,1.e-12)
#mysolver.lineSearch(bool(1))
#mysolver.snlManageTimeStep(16, 3, 2, 10)
mysolver.stepBetweenArchiving(nstepArch)


if BC=="KUBC":
  mysolver.setSystemType(1)
elif BC=="SUBC":
  mysolver.setSystemType(2)
elif BC=="PERIODIC":
  mysolver.setSystemType(1)
elif BC=="MIXED":
  mysolver.setSystemType(1)

mysolver.stiffnessModification(bool(1))
mysolver.iterativeProcedure(bool(1))
mysolver.setMessageView(bool(1))


#boundary condition
runTest=0

if(runTest==1):
  microBC = nonLinearPeriodicBC(1000,3)
  microBC.setOrder(1)
  microBC.setBCPhysical(120,100,110,121,101,111)
  method = 5     # Periodic mesh = 0, Langrange interpolation = 1, Cubic spline interpolation =2,  FE linear= 3, FE Quad = 4
  degree = 3      # Order used for polynomial interpolation 
  addvertex = 1   # Polynomial interpolation by mesh vertex = 0, Polynomial interpolation by virtual vertex 
  microBC.setPeriodicBCOptions(method, degree, bool(addvertex))
  mysolver.addMicroBC(microBC)
  mysolver.activateTest(True)
  mysolver.setRVEVolume(1.e-2*1.e-2*1.e-3)

  # block rigid mode
  mysolver.displacementBC("Node",1,0,0.)
  mysolver.displacementBC("Node",1,1,0.)
  mysolver.displacementBC("Node",1,2,0.)
  mysolver.displacementBC("Node",3,1,0.)
  mysolver.displacementBC("Node",3,2,0.)
  mysolver.displacementBC("Node",2,1,0.)

  #defo
  mysolver.displacementBC("Node",3,0,3e-5)
  mysolver.displacementBC("Face",120,2,0.)
  mysolver.displacementBC("Face",121,2,0.)


if(runTest==0):
  if BC=="KUBC":
    #linear displacement BC
    microBC = nonLinearDisplacementBC(1000,3)

  elif BC=="SUBC":
    #minimal kinematical BC
    microBC = nonLinearMinimalKinematicBC(1000,3)

  elif BC=="PERIODIC":
    #boundary condition
    microBC = nonLinearPeriodicBC(1000,3)
    method = 5     # Periodic mesh = 0, Langrange interpolation = 1, Cubic spline interpolation =2,  FE linear= 3, FE Quad = 4
    degree = 3      # Order used for polynomial interpolation 
    addvertex = 1   # Polynomial interpolation by mesh vertex = 0, Polynomial interpolation by virtual vertex 
    microBC.setPeriodicBCOptions(method, degree, bool(addvertex))

  elif BC=="MIXED":
    microBC = nonLinearMixedBC(1000,3) 
    microBC.setKinematicPhysical(100,0)
    microBC.setKinematicPhysical(100,1)
    microBC.setKinematicPhysical(100,2)
    microBC.setKinematicPhysical(101,0)
    microBC.setKinematicPhysical(101,1)
    microBC.setKinematicPhysical(101,2)
    microBC.setKinematicPhysical(110,0)
    microBC.setKinematicPhysical(110,1)
    microBC.setKinematicPhysical(110,2)
    microBC.setKinematicPhysical(111,0)
    microBC.setKinematicPhysical(111,1)
    microBC.setKinematicPhysical(111,2)
    microBC.setStaticPhysical(120,0)
    microBC.setStaticPhysical(120,1)
    microBC.setKinematicPhysical(120,2)
    microBC.setStaticPhysical(121,0)
    microBC.setStaticPhysical(121,1)
    microBC.setKinematicPhysical(121,2)

  microBC.setOrder(1)
  microBC.setBCPhysical(120,100,110,121,101,111)
  # Deformation gradient
  mysolver.setMicroProblemIndentification(0, number);
  #stress averaging flag and averaging method 0- VOLUME, 1- SURFACE
  mysolver.stressAveragingFlag(bool(1)) # set stress averaging ON- 0 , OFF-1
  mysolver.setStressAveragingMethod(0) # 0 -volume 1- surface
  #tangent averaging flag
  mysolver.tangentAveragingFlag(bool(1)) # set tangent averaging ON -0, OFF -1
  if BC=="KUBC":
    mysolver.setTangentAveragingMethod(2,1e-6) # 0- perturbation 1- condensation
  elif BC=="SUBC":
    mysolver.setTangentAveragingMethod(0,1e-6) # 0- perturbation 1- condensation
  elif BC=="PERIODIC":
    mysolver.setTangentAveragingMethod(2,1e-6) # 0- perturbation 1- condensation
  elif BC=="MIXED":
    mysolver.setTangentAveragingMethod(2,1e-6) # 0- perturbation 1- condensation
  # BC
  microBC.setDeformationGradient(1.0,0.02,0.0,0.0,1.0,0,0,0,1.0)
  mysolver.addMicroBC(microBC)



# needed resultes
mysolver.internalPointBuildView("svm",IPField.SVM, 1, 1)
mysolver.internalPointBuildView("sig_xx",IPField.SIG_XX, 1, 1)
mysolver.internalPointBuildView("sig_yy",IPField.SIG_YY, 1, 1)
mysolver.internalPointBuildView("sig_zz",IPField.SIG_ZZ, 1, 1)
mysolver.internalPointBuildView("sig_xy",IPField.SIG_XY, 1, 1)
mysolver.internalPointBuildView("sig_yz",IPField.SIG_YZ, 1, 1)
mysolver.internalPointBuildView("sig_xz",IPField.SIG_XZ, 1, 1)
mysolver.internalPointBuildView("damage",IPField.DAMAGE, 1, 1)
mysolver.internalPointBuildView("epl",IPField.PLASTICSTRAIN, 1, 1)
mysolver.archivingForceOnPhysicalGroup("Face", 101, 0,nstepArch)
mysolver.archivingForceOnPhysicalGroup("Face", 101, 1,nstepArch)
mysolver.archivingForceOnPhysicalGroup("Face", 101, 2,nstepArch)
mysolver.archivingForceOnPhysicalGroup("Face", 111, 0,nstepArch)
mysolver.archivingForceOnPhysicalGroup("Face", 111, 1,nstepArch)
mysolver.archivingForceOnPhysicalGroup("Face", 111, 2,nstepArch)
mysolver.archivingForceOnPhysicalGroup("Face", 121, 0,nstepArch)
mysolver.archivingForceOnPhysicalGroup("Face", 121, 1,nstepArch)
mysolver.archivingForceOnPhysicalGroup("Face", 121, 2,nstepArch)


mysolver.archivingAverageValue(IPField.P_XY)
mysolver.archivingAverageValue(IPField.F_XY)

mysolver.solve()

# test check
check = TestCheck()

try:
    import linecache
    lstress = linecache.getline('E_0_GP_1_stress.csv',7)
except:
    print('Cannot get values in the files')
    import os
    os._exit(1)
else:
    st = float(lstress.split(';')[2])
    check.equal(3.951369e+01,st,1.e-4)

