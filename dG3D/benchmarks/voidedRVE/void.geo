SetFactory("OpenCASCADE");
Mesh.Algorithm = 6;
mm=1.;

L=1.*mm;
v=0.25;

Mesh.CharacteristicLengthMin = 0.2*mm;
Mesh.CharacteristicLengthMax = 0.2*mm;

R=L/(4.*Pi/3./v)^(1./3.);

Box(10)={0.,0.,0.,L,L,L};
Sphere(11) = {L/2., L/2., L/2., R};
BooleanDifference(12) = { Volume{10}; Delete; }{ Volume{11}; Delete; };


//between kink and clamp
fdown_[] = Abs(Boundary{ Volume{12}; });

Physical Volume(12) = { 12 };
Physical Surface(100) = {fdown_[0]}; //x=0
Physical Surface(101) = {fdown_[5]}; //x=L
Physical Surface(110) = {fdown_[1]}; //y=0
Physical Surface(111) = {fdown_[3]}; //y=L
Physical Surface(120) = {fdown_[4]}; //z=0
Physical Surface(121) = {fdown_[2]}; //z=L