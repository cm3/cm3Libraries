# test file

set(PYFILE cube.py)

set(FILES2DELETE 
  *.csv
  disp*
  stress*
)

add_cm3python_test(${PYFILE} "${FILES2DELETE}")
