#coding-Utf-8-*-
from gmshpy import*
#from dG3DpyDebug import*
from dG3Dpy import*



# Script to launch cube problem with a python script


# Material law creation
# ===================================================================================
# numbering of material law (has to be unique for each law !)
BulkLawNum1 = 1 
ClLawNum1 = 2
DamLawNum1 = 3
InterfaceLawNum1 = 4
FracLawNum1 = 5


# material parameters - bulk law
rho = 7850. 		# kg/m3 :	density
E = 3.2e9		# Pa : 		Young modulus
nu = 0.28 		# - : 		poisson coefficient
pi = 0.011 		# - : 		initial damage strain value
pc = 0.50 		# - : 		failure strain value
alpha = 5.0 		# - : 		damage exponent
beta = 0.75 		# - : 		damage exponent bis
cl = 2.0e-4 		# m2: 		non-local parameter (== NL-length^2) 

# material parameters (2) - cohesive law
sigmac = 0.08*E		# Pa		critical effective stress value
Dc = 0.99		# - 		critical damage value
damageCheck = bool(0)	# bool 		actived crack insertion by damage value
h =  2.0*1e-3   	# m 		band thickness
mu = -10000. 		# - 		friction coefficient
fmin = 1.00 		# - 		spread min bond
fmax =1.00		# - 		spread max bond
betac = 0.87		# -
Kp = 5.e09		# Pa/m2		penatly parameter (F = -K_p u_n^2)
			# ==O(E * beta_DG / mesh_size^2)
GradFactor = 0.5	# -		multiply grad of jump
useFc = bool(1) 	# -		activate normal comp. blocking of F
delta0 = 1.0e-5

# material law creation
Cl_Law1 = IsotropicCLengthLaw(ClLawNum1,cl)
Dam_Law1 = PowerDamageLaw(DamLawNum1, pi, pc, alpha, beta)
BulkLaw1 = NonLocalDamageIsotropicElasticityDG3DMaterialLaw(BulkLawNum1,rho,E,nu,Cl_Law1,Dam_Law1)
BulkLaw1.setNonLocalLimitingMethod(0)
InterfaceLaw1 = CohesiveBand3DLaw(InterfaceLawNum1,sigmac,Dc,h,mu,fmin,fmax, betac,Kp,damageCheck,GradFactor,useFc,delta0)
FracLaw1 = FractureByCohesive3DLaw(FracLawNum1,BulkLawNum1,InterfaceLawNum1)


# Solver parameters
# ===================================================================================
sol = 2  		# Library for solving: Gmm=0 (default) Taucs=1 PETsc=2
soltype = 1 		# StaticLinear=0 (default, StaticNonLinear=1, Explicit=2, 				# Multi=3, Implicit=4, Eigen=5)
nstep = 100   		# Number of step
ftime =1.0   		# Final time
tol=1.e-5  		# Relative tolerance for NR scheme
tolAbs = 1.e-8		# Absolute tolerance for NR scheme
nstepArch=10		# Number of step between 2 archiving
nstepArchEnergy = 1	# Number of step between 2 energy computation and archiving
nstepArchForce = 1	# Number of step between 2 force archiving
MaxIter = 20		# Maximum number of iterations
StepIncrease = 3	# Number of successfull timestep before reincreasing it
StepReducFactor = 5.0 	# Timestep reduction factor
NumberReduction = 4	# Maximum number of timespep reduction: max reduction = pow(StepReducFactor,this)
fullDg = bool(1)        # O = CG, 1 = DG
dgnl = bool(1)		# DG for non-local variables inside a domain (only if fullDg)
eqRatio = 1.0e6		# Ratio between "elastic" and non-local equations 
space1 = 1 		# Function space (Lagrange=0 Hierarchical=1)
beta1  = 40.0		# Penality parameter for DG


# Domain creation
## ===================================================================================
# Example : field1 = dG3DDomain(numDomain, numPhysVolume, space, LawNum, fullDgFlag, dim=3, numNonlocalVar=1)
# with:	- numDomain : domain tag
# 	- numPhysVolume : number of physical entities
# 	- space : function space (Lagrange=0)
#	- LawNum : number of material law 
# 	- fullDgFlag :  O = CG, 1 = DG
# 	- nonLocalEqRatio : ratio between elastic and non-local system
# 	- dim : dimension of displacment
# 	- numNonlocalVar : numbver of non-local variable

numPhysVolume1 = 29 		# Number of a physical volume of the model in .geo
numDomain1 = 1000 		# Number of the domain
field1 = dG3DDomain(numDomain1,numPhysVolume1,space1,FracLawNum1,fullDg,3,1)
field1.stabilityParameters(beta1) 			# Adding stability parameters (for DG)
field1.setNonLocalStabilityParameters(beta1,dgnl) 		# Adding stability parameters (for DG)
field1.setNonLocalEqRatio(eqRatio)
field1.gaussIntegration(0,6,6)
field1.setBulkDamageBlockedMethod(1)
field1.forceCohesiveInsertionAllIPs(bool(1),2./3.)
#field1.matrixByPerturbation(1,1,1,1e-8) 		# Tangent computation analytically or by pertubation

field1.setHierarchicalOrder(0,3)



# Solver creation
# ===================================================================================
mysolver = nonLinearMechSolver(numDomain1) 		# Solver associated with numSolver1
# Geometry and mesh loading
# ===================================================================================
geofile="cube.geo"
meshfile= "cube.msh"
mysolver.createModel(geofile,meshfile,3,1)
#mysolver.loadModel(meshfile)		# add mesh
mysolver.addDomain(field1) 		# add domain
mysolver.addMaterialLaw(BulkLaw1) 	# add material law
mysolver.addMaterialLaw(InterfaceLaw1)
mysolver.addMaterialLaw(FracLaw1)
mysolver.Scheme(soltype) 			# solver scheme
mysolver.Solver(sol) 				# solver choice
mysolver.snlData(nstep,ftime,tol,tolAbs) 	# solver parameters
mysolver.snlManageTimeStep(MaxIter,StepIncrease,StepReducFactor,NumberReduction) #timestep
mysolver.lineSearch(bool(0))		# lineSearch activation
# solver archiving
mysolver.stepBetweenArchiving(nstepArch) 	# archiving frequency for IPField
mysolver.energyComputation(nstepArchEnergy)	# archiving frequency for energy







# Boundary conditions
# ===============================
# Example with constrained displacement : mysolver.displacementBC("type", numphys, comp, value)
# with:	- type : Node / Edge / Face / Volume
# 	- numphys : number of physical entities
# 	- comp : number of variables on which it's applied (0 = u_x; 1 = u_y; 2 = u_z)
# 	- value : value at the end of the simulation
# NB: if explicit solving: displacment is replaced by a speed !
# Example with applied forces : mysolver.forceBC("type", numphys, comp, value)

tot_disp = 2.*1.e-3*0.2 # Max disp == elongation 50 pourcent

mysolver.displacementBC("Face",30,0,0.)		# face x = 0
mysolver.displacementBC("Face",31,0,tot_disp/ftime) # face x = L
mysolver.displacementBC("Face",34,1,0.)		# face y = 0
mysolver.displacementBC("Face",32,2,0.)		# face z = 0










# Variable storage
# ===============================
# Example : mysolver.internalPointBuildView("file_name", "comp", nbstep, ev)
# with:	- file_name : the name of variable storage file 
# 	- comp : saved values (IPField.SVM, IPField.SIG_XX, IPField::STRAIN_XZ,... 
#  	see ipField.h for possibilities)
# 	- nbstep : storage frequency ?
# 	- ev : value type (default : mean)
#
# Example : mysolver.archivingForceOnPhysicalGroup("type", numphys, comp, nstep)
# with:	- type : Node / Edge / Face / Volume
# 	- numphys : number of physical entities
# 	- comp : number of variables on which it's applied (0 = u_x; 1 = u_y; 2 = u_z)
# 	- nstep : step number between force archivingmysolver.internalPointBuildView("svm",IPField.SVM, 1, 1)
#
mysolver.internalPointBuildView("svm",IPField.SVM, 1, 1)
mysolver.internalPointBuildView("sig_xx",IPField.SIG_XX, 1, 1)
mysolver.internalPointBuildView("sig_yy",IPField.SIG_YY, 1, 1)
mysolver.internalPointBuildView("sig_zz",IPField.SIG_ZZ, 1, 1)
mysolver.internalPointBuildView("sig_xy",IPField.SIG_XY, 1, 1)
mysolver.internalPointBuildView("sig_yz",IPField.SIG_YZ, 1, 1)
mysolver.internalPointBuildView("sig_xz",IPField.SIG_XZ, 1, 1)
mysolver.internalPointBuildView("damage",IPField.DAMAGE,1,1)
mysolver.internalPointBuildView("eff_eps",IPField.LOCAL_EQUIVALENT_STRAINS,1,1)
mysolver.internalPointBuildView("NL_eps",IPField.NONLOCAL_EQUIVALENT_STRAINS,1,1)



mysolver.archivingForceOnPhysicalGroup("Face", 31, 0, nstepArchForce)
mysolver.archivingIPOnPhysicalGroup("Volume",29, IPField.DAMAGE,IPField.MEAN_VALUE)


# Solving
# ===========
mysolver.solve()



# Test
# ===========
check = TestCheck()
check.equal(8.818651e+00,mysolver.getArchivedForceOnPhysicalGroup("Face", 31, 0),1.e-5)

import csv
data = csv.reader(open('IPVolume29val_DAMAGEMean.csv'), delimiter=';')
porosity = list(data)
check.equal(8.898482e-01,float(porosity[-1][1]),1e-5)

















