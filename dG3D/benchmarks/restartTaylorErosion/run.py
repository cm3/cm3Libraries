import os,csv
#from gmshpy import *
#from dG3Dpy import*

import sys
def checkEqual(ref, cur, tol):
  if(abs(ref-cur)>abs(tol*ref)):
    print("Error : reference  current value ",ref," current value ",cur," relative error ", (ref-cur)/ref," tolerance ",tol)


if sys.version_info[0] < 3:
  os.system('mpiexec -np 4 python taylorTetDGDyn.py')
else:
  os.system('mpiexec -np 4 python3 taylorTetDGDyn.py')

data1 = csv.reader(open('force3comp2_part2.csv'), delimiter=';')
force = list(data1)
checkEqual(0.,float(force[-1][1]),1e-5)
data2 = csv.reader(open('NodalDisplacementPhysical5Num1comp0_part2.csv'), delimiter=';')
disp = list(data2)
checkEqual(1.016079e-03,float(disp[-1][1]),1e-5)

if sys.version_info[0] < 3:
  os.system('mpiexec -np 4 python taylorTetDGDyn.py')
else:
  os.system('mpiexec -np 4 python3 taylorTetDGDyn.py')

data21 = csv.reader(open('force3comp2_part2.csv'), delimiter=';')
force2 = list(data21)
checkEqual(0.,float(force2[-1][1]),1e-5)
data22 = csv.reader(open('NodalDisplacementPhysical5Num1comp0_part2.csv'), delimiter=';')
disp2 = list(data22)
checkEqual(1.016079e-03,float(disp2[-1][1]),1e-5)

