#coding-Utf-8-*-
from gmshpy import *
from dG3Dpy import*

#script to launch beam problem with a python script

# material law
lawnum = 1 # unique number of law
rho   = 8930
K = 130.e9
G = 43.333e9
young = 9*K*G/(3*K+G)
print("%e"%young)
nu    = (3*K-2*G)/(2*(3*K+G))
print("%e"%nu)
sy0   = 400.e6
h     = 100.e6
beta1 = 20


# geometry
meshfile="taylor.msh" # name of mesh file
geofile="taylor.geo"

# solver
sol = 2  # Gmm=0 (default) Taucs=1 PETsc=2
soltype = 2 # StaticLinear=0 (default) StaticNonLinear=1
nstep = 10   # number of step
ftime = 8.e-6 #shortened test 80.e-6   # Final time 
nstepArch=100 # Number of step between 2 archiving (used only if soltype=1)
fullDg = 1 #O = CG, 1 = DG
space1 = 0 # function space (Lagrange=0)


#  compute solution and BC (given directly to the solver
# creation of law
law1 = J2LinearDG3DMaterialLaw(lawnum,rho,young,nu,sy0,h)

# creation of ElasticField
nfield = 102 # number of the field (physical number of surface)
myfield1 = dG3DDomain(1000,nfield,space1,lawnum,fullDg)
#myfield1.matrixByPerturbation(1,1,1,1e-8)
myfield1.stabilityParameters(beta1)
# creation of Solver
mysolver = nonLinearMechSolver(1000)
#mysolver.createModel(geofile,meshfile,3,2)
mysolver.loadModel(meshfile)
mysolver.addDomain(myfield1)
mysolver.addMaterialLaw(law1)
mysolver.Scheme(soltype)
mysolver.Solver(sol)
mysolver.explicitSpectralRadius(ftime,0.8,1.)
mysolver.explicitTimeStepEvaluation(nstep)
mysolver.stepBetweenArchiving(nstepArch)
# BC
mysolver.displacementBC("Face",1,0,0.)
mysolver.displacementBC("Face",2,1,0.)
mysolver.displacementBC("Face",3,2,0.)
mysolver.initialBC("Volume","Velocity",102,2,-227.)

mysolver.internalPointBuildView("svm",IPField.SVM, 1, 1)
mysolver.internalPointBuildView("sig_xx",IPField.SIG_XX, 1, 1)
mysolver.internalPointBuildView("sig_yy",IPField.SIG_YY, 1, 1)
mysolver.internalPointBuildView("sig_zz",IPField.SIG_ZZ, 1, 1)
mysolver.internalPointBuildView("sig_xy",IPField.SIG_XY, 1, 1)
mysolver.internalPointBuildView("sig_yz",IPField.SIG_YZ, 1, 1)
mysolver.internalPointBuildView("sig_xz",IPField.SIG_XZ, 1, 1)
mysolver.internalPointBuildView("epl",IPField.PLASTICSTRAIN, 1, 1)
mysolver.archivingForceOnPhysicalGroup("Face", 3, 2)
mysolver.archivingNodeDisplacement(5, 0)

bulkEro = True
interfaceEro = True
EroMethod = 1 # 1- erosion when fist IP reach failure Cr, 2-all IPs
mysolver.setElementErosion(bulkEro,interfaceEro,EroMethod)
#cr based on critical damage
Type = 1 # 1- maximal value, 2 -minimal value
cr = GeneralCritialCriterion(1,0.1,IPField.PLASTICSTRAIN,Type)
# set global Cr
mysolver.setGlobalErosionCheck(True,cr)



mysolver.createRestartBySteps(1050);

mysolver.solve()

#try:
#  import linecache
#  linedisp = linecache.getline('NodalDisplacementPhysical5Num1comp0_part2.csv',2552)
#except:
#  print('Cannot read the results in the files')
#  import os
#  os._exit(1)
#else:
#  check = TestCheck()
#  check.equal(1.016427e-03,float(linedisp.split(';')[1]))

#mysolver.disableResetRestart() #only battery: the second solve mimics a restart
#mysolver.solve()

#check = TestCheck()

#try:
#  import linecache
#  linecache.clearcache()
#  linedisp2 = linecache.getline('NodalDisplacementPhysical5Num1comp0_part2.csv',452)
#except:
#  print('Cannot read the results in the files')
#  import os
#  os._exit(1)
#else:
#  check2 = TestCheck()
#  check2.equal(1.015601e-03,float(linedisp2.split(';')[1]))


