#coding-Utf-8-*-

from gmshpy import *
from dG3Dpy import*
# from dG3DpyDebug import*
from math import*
import csv
import numpy as np
import pandas as pd
import os

# Load the csv data for relaxation spectrum
with open('relaxationSpectrum_Et_N27_19_04_24_Tref20.txt', newline='') as csvfile:
    reader = csv.reader(csvfile, delimiter=';')
    relSpec = np.zeros((1,))
    i = 0
    for row in reader:
        if i == 0:
            relSpec[i] = float(' '.join(row))
        else:
            relSpec = np.append(relSpec, float(' '.join(row)))
        i += 1
# print(relSpec)
N = int(0.5*(i-1))
relSpec[0:N+1] *= 1e-6    # convert to MPa

# material law
lawnum = 11 # unique number of law

E = relSpec[0] #MPa # 2700
nu = 0.33 #0.33
K = E/3./(1.-2.*nu)	# Bulk mudulus
mu = E/2./(1.+nu)	  # Shear mudulus
rho = 905e-12 # Bulk mass   905 kg/m3 -> 905e-12 tonne/mm3
Cp = rho*1900e+6 # 1900 J/kg/K -> 1900e+6 Nmm/tonne/K
KThCon = 0.14  # 0.14 W/m/K -> 0.14 Nmm/s/mm/K
Alpha = 0.6e-6 # 1/K

Tinitial = 273.15 + 23 # K
C1 = 112.94742152
C2 = 176.44294359

# Temp Settings
Tref = 273.15+20.

# Default tempfuncs
Alpha_G = 0.2e-6
Alpha_R = 1.e-6
KThCon_G = 0.10
KThCon_R = 1.5*0.12
k_gibson = 3.36435338e-02
m_gibson = 0.0
Tg = Tref
Alpha_tempfunc = GlassTransitionScalarFunction(1, Alpha_R/Alpha_G, Tg, k_gibson, m_gibson, 0)
KThCon_tempfunc = GlassTransitionScalarFunction(1, KThCon_R/KThCon_G, Tg, k_gibson, m_gibson, 0)

C = 0.0228422
sy0c = 6.02534075/0.78
negExp_tempfunc = negativeExponentialFunction(C,Tref)

a = -0.9/(90.)
x0 = 273.15-20
linear_tempfunc = linearScalarFunction(x0,0.9,a)

hardenc = LinearExponentialJ2IsotropicHardening(1, 1., 0.01, 0.01, 30.)
hardent = LinearExponentialJ2IsotropicHardening(2, 1., 0.01, 0.01, 30.)
# hardenc.setTemperatureFunction_h1(linear_tempfunc)
# hardenc.setTemperatureFunction_h2(linear_tempfunc)
# hardenc.setTemperatureFunction_hexp(ShiftFactor_tempfunc)
# hardent.setTemperatureFunction_h1(linear_tempfunc)
# hardent.setTemperatureFunction_h2(linear_tempfunc)
# hardent.setTemperatureFunction_hexp(ShiftFactor_tempfunc)

hardenk = PolynomialKinematicHardening(3,3) # 1 -> PP
hardenk.setCoefficients(0,1.) # 300.)
hardenk.setCoefficients(1,2.) # 200.)
hardenk.setCoefficients(2,5.)
# hardenk.setTemperatureFunction_K(linear_tempfunc)

law1 = NonLinearTVENonLinearTVPDG3DMaterialLaw(lawnum,rho,E,nu,1e-6,Tinitial,Alpha,KThCon,Cp,False,1e-5,1e-6)

law1.setCompressionHardening(hardenc)
law1.setTractionHardening(hardent)
law1.setKinematicHardening(hardenk)

law1.setYieldPowerFactor(1.25) # .5)
law1.setNonAssociatedFlow(True)
law1.nonAssociatedFlowRuleFactor(0.64) # 5)

law1.setStrainOrder(-1)
law1.setViscoelasticMethod(0)
law1.setShiftFactorConstantsWLF(C1,C2)
law1.setReferenceTemperature(Tref)

law1.useRotationCorrectionBool(True,2) # bool, int_Scheme = 0 -> R0, 1 -> R1


extraTVE = np.ones((int(0.5*(relSpec.size-1)),13)) # 3 vol + 3 dev + 1 comp
# tension
extraTVE[:,0] = (np.concatenate((np.linspace(0.0,0.0,14),np.linspace(0.0,0.05,13)))) # V0 first at -10
extraTVE[:,1] = np.flip(np.concatenate((np.linspace(1500,10000,13),np.linspace(10000,5000,14)))) # V1 - flipped, last at -10
extraTVE[:,2] = (np.concatenate((np.linspace(0.5,0.5,14),np.linspace(0.5,0.75,13)))) # V2
extraTVE[:,3] = (np.concatenate((np.linspace(0.0,0.0,14),np.linspace(0.0,0.05,13)))) # D0
extraTVE[:,4] =  np.flip(np.concatenate((np.linspace(1500,10000,13),np.linspace(10000,5000,14)))) # D1 - flipped, last at -10
extraTVE[:,5] = (np.concatenate((np.linspace(0.5,0.5,14),np.linspace(0.5,0.75,13)))) # D2
extraTVE[:,6] = np.flip(np.concatenate((np.linspace(1.8,1.8,13),np.linspace(1.8,5.,14)))) # Ci

# compression
extraTVE[:,7] = (np.concatenate((np.linspace(1.0,0.1,14),np.linspace(0.1,0.05,13)))) # extraTVE[:,0]  # V3
extraTVE[:,8] = np.flip(np.concatenate((np.linspace(4500,8500,13),np.linspace(8500,40000,14)))) # extraTVE[:,1]
extraTVE[:,9] = (np.concatenate((np.linspace(1.5,1.25,14),np.linspace(1.25,1.35,13)))) # extraTVE[:,2]
extraTVE[:,10] = (np.concatenate((np.linspace(1.0,0.1,14),np.linspace(0.1,0.05,13)))) # extraTVE[:,3]
extraTVE[:,11] = np.flip(np.concatenate((np.linspace(4500,8500,13),np.linspace(8500,40000,14)))) # extraTVE[:,4]
extraTVE[:,12] = (np.concatenate((np.linspace(1.5,1.25,14),np.linspace(1.25,1.35,13)))) # extraTVE[:,5]
extraTVE.tolist()


law1.setExtraBranchNLType(4)
law1.useExtraBranchBool(True)
law1.useExtraBranchBool_TVE(True,4)
law1.setVolumeCorrection(0.0,1000,1.5,0.0,1000.,1.5)
law1.setAdditionalVolumeCorrections(0.,2000,1.35,0.,2000.,1.35)
law1.setExtraBranch_CompressionParameter(1.8,0.,0.)
law1.setTensionCompressionRegularisation(1000.)
law1.setViscoElasticNumberOfElement(N)
if N > 0:
    for i in range(1, N+1):
        law1.setViscoElasticData(i, relSpec[i], relSpec[i+N])
        law1.setCorrectionsAllBranchesTVE(i, extraTVE[i-1,0],extraTVE[i-1,1],extraTVE[i-1,2],extraTVE[i-1,3],extraTVE[i-1,4],extraTVE[i-1,5])
        law1.setCompressionCorrectionsAllBranchesTVE(i, extraTVE[i-1,6])
        law1.setAdditionalCorrectionsAllBranchesTVE(i, extraTVE[i-1,7],extraTVE[i-1,8],extraTVE[i-1,9],extraTVE[i-1,10],extraTVE[i-1,11],extraTVE[i-1,12])
law1.setTemperatureFunction_ThermalDilationCoefficient(Alpha_tempfunc)
law1.setTemperatureFunction_ThermalConductivity(KThCon_tempfunc)

law1.setTemperatureFunction_ThermalDilationCoefficient(Alpha_tempfunc)
law1.setTemperatureFunction_ThermalConductivity(KThCon_tempfunc)

mullins = linearScaler(4, 0.99)
a = -0.9/(90.)
x0 = 273.15-20
linear_tempfunc = linearScalarFunction(x0,0.99,a)
# mullins.setTemperatureFunction_r(linear_tempfunc)
# law1.setMullinsEffect(mullins)


eta = constantViscosityLaw(1,10000.) # 3e4) #(1,3e4)
law1.setViscosityEffect(eta,0.21) # 0.21)
# eta.setTemperatureFunction(negExp_tempfunc)
# law1.setIsotropicHardeningCoefficients(1.,1.,1.)


# solver
sol = 2  # Gmm=0 (default) Taucs=1 PETsc=2
soltype = 1 # StaticLinear=0 (default) StaticNonLinear=1
nstep = 2000   # number of step (used only if soltype=1)
# ftime = 2*np.sum(elongations[:,0])/2.78e-3 # 0.65/2.78e-3 # 4.8 4.   # Final time (used only if soltype=1)
ftime = 50.#0.2/0.01 # 0.65/2.78e-3 # 4.8 4.   # Final time (used only if soltype=1)
tol=1.e-5  # relative tolerance for NR scheme (used only if soltype=1)
nstepArch=50 # Number of step between 2 archiving (used only if soltype=1)
fullDg = 0 #O = CG, 1 = DG
space1 = 0 # function space (Lagrange=0)
beta1  = 100


# geometry
geofile = "cube.geo" # "line.geo"
meshfile = "cube.msh" # "line.msh"
nfield = 29


pertLaw1 = dG3DMaterialLawWithTangentByPerturbation(law1,1e-8) # use with _pert*T0 (b/c In tangent_by_perturbation_TVP u use _pert*T0)
ThermoMechanicsEqRatio = 1.e1 # setConstitutiveExtraDofDiffusionEqRatio(ThermoMechanicsEqRatio)
thermalSource = True
mecaSource = True
myfield1 = ThermoMechanicsDG3DDomain(1000,nfield,space1,lawnum,fullDg,ThermoMechanicsEqRatio)
myfield1.setConstitutiveExtraDofDiffusionAccountSource(thermalSource,mecaSource)
myfield1.stabilityParameters(beta1)
myfield1.ThermoMechanicsStabilityParameters(beta1,bool(1))
# myfield1.strainSubstep(2, 10)

# creation of Solver
mysolver = nonLinearMechSolver(1000)
mysolver.createModel(geofile,meshfile,3,2)
#mysolver.loadModel(meshfile)
mysolver.addDomain(myfield1)
mysolver.addMaterialLaw(law1)
#mysolver.addMaterialLaw(pertLaw1)
mysolver.Scheme(soltype)
mysolver.Solver(sol)
mysolver.snlData(nstep,ftime,tol,tol/100)
mysolver.stepBetweenArchiving(nstepArch)
mysolver.snlManageTimeStep(15,5,2.,50)

# BCs
length = 1
umax = 0.5*length # 0.65*length # 0.04 -> PP
fu_L = LinearFunctionTime(0,0,ftime,umax);    # Linear Displacement with time

mysolver.displacementBC("Face",30,0,0.)		        # face x = 0   - Left Face fixed
mysolver.displacementBC("Face",30,1,0.)		        # face x = 0   - Left Face fixed
mysolver.displacementBC("Face",30,2,0.)		        # face x = 0   - Left Face fixed
# mysolver.displacementBC("Face",31,0,fu_L)
mysolver.displacementBC("Face",31,1,fu_L)         # face x = L   - Right Face moving
mysolver.displacementBC("Face",31,2,fu_L)         # face x = L   - Right Face moving
# mysolver.displacementBC("Face",34,1,0.)		    # face y = 0
# mysolver.displacementBC("Face",32,2,0.)		    # face z = 0

# thermal BC
mysolver.initialBC("Volume","Position",nfield,3,Tinitial)
fT = LinearFunctionTime(0,Tinitial,ftime,Tinitial)    # Linear Displacement with time
# mysolver.displacementBC("Volume",nfield,3,fT)

# Field-Output

mysolver.internalPointBuildView("strain_zz", IPField.STRAIN_ZZ, 1, nstepArch);
mysolver.internalPointBuildView("svm",IPField.SVM, 1, nstepArch)
mysolver.internalPointBuildView("sig_xx",IPField.SIG_XX, 1, nstepArch)
mysolver.internalPointBuildView("sig_yy",IPField.SIG_YY, 1, nstepArch)
mysolver.internalPointBuildView("sig_zz",IPField.SIG_ZZ, 1, nstepArch)
mysolver.internalPointBuildView("sig_xy",IPField.SIG_XY, 1, nstepArch)
mysolver.internalPointBuildView("sig_yz",IPField.SIG_YZ, 1, nstepArch)
mysolver.internalPointBuildView("sig_xz",IPField.SIG_XZ, 1, nstepArch)
mysolver.internalPointBuildView("mandel_xx",IPField.mandel_XX, 1, nstepArch)
mysolver.internalPointBuildView("mandel_yy",IPField.mandel_YY, 1, nstepArch)
mysolver.internalPointBuildView("mandel_zz",IPField.mandel_ZZ, 1, nstepArch)
mysolver.internalPointBuildView("mandel_xy",IPField.mandel_XY, 1, nstepArch)
mysolver.internalPointBuildView("mandel_yz",IPField.mandel_YZ, 1, nstepArch)
mysolver.internalPointBuildView("mandel_xz",IPField.mandel_XZ, 1, nstepArch)
mysolver.internalPointBuildView("mandel_yx",IPField.mandel_YX, 1, nstepArch)
mysolver.internalPointBuildView("mandel_zy",IPField.mandel_ZY, 1, nstepArch)
mysolver.internalPointBuildView("mandel_zx",IPField.mandel_ZX, 1, nstepArch)
mysolver.internalPointBuildView("epl",IPField.PLASTICSTRAIN, 1, nstepArch)

# Print/Archive History Outputs
mysolver.archivingAverageValue(IPField.hyperElastic_BulkScalar)
mysolver.archivingAverageValue(IPField.hyperElastic_ShearScalar)
mysolver.archivingAverageValue(IPField.mandel_XX)
mysolver.archivingAverageValue(IPField.mandel_YY)
mysolver.archivingAverageValue(IPField.mandel_ZZ)
mysolver.archivingAverageValue(IPField.mandel_XY)
mysolver.archivingAverageValue(IPField.mandel_XZ)
mysolver.archivingAverageValue(IPField.mandel_YZ)
mysolver.archivingAverageValue(IPField.mandel_YX)
mysolver.archivingAverageValue(IPField.mandel_ZX)
mysolver.archivingAverageValue(IPField.mandel_ZY)
mysolver.archivingAverageValue(IPField.mandelCommuteChecker)

# mysolver.archivingForceOnPhysicalGroup("Face", 31, 0, nstepArch)  # Force on the right force
mysolver.archivingForceOnPhysicalGroup("Face", 30, 0, nstepArch)  # Force on the left face  === Reaction Force
mysolver.archivingForceOnPhysicalGroup("Face", 30, 1, nstepArch)  # Force on the left face  === Reaction Force
mysolver.archivingForceOnPhysicalGroup("Face", 30, 2, nstepArch)  # Force on the left face  === Reaction Force
# mysolver.archivingForceOnPhysicalGroup("Node", 41, 0, nstepArch)  # Force on a point on the left face === Reaction Force
mysolver.archivingNodeDisplacement(43, 0, nstepArch)              # Displacement of a point on the right face
mysolver.archivingIPOnPhysicalGroup("Volume",nfield, IPField.PLASTICSTRAIN,IPField.MEAN_VALUE);

# Solve
mysolver.solve()

check = TestCheck()
check.equal(-1.022490e-07,mysolver.getArchivedForceOnPhysicalGroup("Face", 30, 0),1.e-4)
