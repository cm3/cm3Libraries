#coding-Utf-8-*-
from gmshpy import *
from dG3Dpy import*
import shutil
import os
#from dG3DpyDebug import*
#script to launch beam problem with a python script

# material law
lawnum   = 1 # unique number of law
rho      = 7850
properties = "property.i01"


# geometry
geofile="cube.geo" # name of mesh file
meshfile="cube.msh" # name of mesh file

# solver
sol = 2  # Gmm=0 (default) Taucs=1 PETsc=2
soltype = 1 # StaticLinear=0 (default) StaticNonLinear=1
nstep = 3000  # number of step (used only if soltype=1)

t1=1.
peak = 30.
ftime =3.600000000000000e+006   # Final time (used only if soltype=1)
tol=1.e-6   # relative tolerance for NR scheme (used only if soltype=1)
nstepArch=300 # Number of step between 2 archiving (used only if soltype=1)
fullDg = 0 #O = CG, 1 = DG
space1 = 0 # function space (Lagrange=0)


#  compute solution and BC (given directly to the solver
# creation of law
#law1 = NonLocalDamageDG3DMaterialLaw(lawnum,rho,properties)
law1 = NonLocalDamageDG3DMaterialLaw(lawnum,rho,properties)
# creation of ElasticField
nfield = 10 # number of the field (physical number of surface)
myfield1 = dG3DDomain(1000,nfield,space1,lawnum,fullDg,3)
myfield1.stabilityParameters(30.)
#myfield1.matrixByPerturbation(1,1,1,1e-8)
# creation of Solver
mysolver = nonLinearMechSolver(1000)
mysolver.createModel(geofile,meshfile,3,2)
#mysolver.loadModel(meshfile)
mysolver.addDomain(myfield1)
mysolver.addMaterialLaw(law1)
mysolver.Scheme(soltype)
mysolver.Solver(sol)
mysolver.snlData(nstep,t1,tol)
mysolver.stepBetweenArchiving(nstepArch)
# BC
#shearing
#mysolver.displacementBC("Face",1234,0,0.)
#mysolver.displacementBC("Face",1234,1,0.)
#mysolver.displacementBC("Face",1234,2,0.)
#mysolver.displacementBC("Face",5678,1,0.00)
#mysolver.forceBC("Face",2376,2,10000)
#mysolver.forceBC("Face",4158,2,-10000)
#mysolver.forceBC("Face",5678,0,10000)
#tension along z
mysolver.displacementBC("Face",1234,2,0.)
mysolver.displacementBC("Face",1265,1,0.)
mysolver.displacementBC("Face",4158,0,0.)

creepFucntion=cycleFunctionTime(0., 0., t1, peak, ftime,peak);
mysolver.forceBC("Face",5678,2,creepFucntion)


mysolver.internalPointBuildView("svm",IPField.SVM, 1, 1)
mysolver.internalPointBuildView("sig_xx",IPField.SIG_XX, 1, 1)
mysolver.internalPointBuildView("sig_yy",IPField.SIG_YY, 1, 1)
mysolver.internalPointBuildView("sig_zz",IPField.SIG_ZZ, 1, 1)
mysolver.internalPointBuildView("sig_xy",IPField.SIG_XY, 1, 1)
mysolver.internalPointBuildView("sig_yz",IPField.SIG_YZ, 1, 1)
mysolver.internalPointBuildView("sig_xz",IPField.SIG_XZ, 1, 1)
mysolver.internalPointBuildView("epl",IPField.PLASTICSTRAIN, 1, 1)
mysolver.internalPointBuildView("eps_xx",IPField.STRAIN_XX, 1, 1)
mysolver.internalPointBuildView("eps_yy",IPField.STRAIN_YY, 1, 1)
mysolver.internalPointBuildView("eps_zz",IPField.STRAIN_ZZ, 1, 1)
mysolver.internalPointBuildView("eps_xy",IPField.STRAIN_XY, 1, 1)
mysolver.internalPointBuildView("eps_yz",IPField.STRAIN_YZ, 1, 1)
mysolver.internalPointBuildView("eps_xz",IPField.STRAIN_XZ, 1, 1)
mysolver.archivingIPOnPhysicalGroup("Volume",nfield, IPField.SIG_ZZ,IPField.MEAN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",nfield, IPField.STRAIN_ZZ,IPField.MEAN_VALUE, nstepArch);

mysolver.archivingForceOnPhysicalGroup("Face", 1234, 2,nstepArch)
mysolver.archivingNodeDisplacement(8,2,nstepArch)


mysolver.solve()
check = TestCheck()
check.equal(9.282136e-03,mysolver.getArchivedNodalValue(8,2,mysolver.displacement),1e-5)

mysolver.resetBoundaryConditions()
mysolver.displacementBC("Face",1234,2,0.)
mysolver.displacementBC("Face",1265,1,0.)
mysolver.displacementBC("Face",4158,0,0.)

creepFucntionBis=cycleFunctionTime(0., peak, ftime,peak);
mysolver.forceBC("Face",5678,2,creepFucntionBis)


mysolver.snlData(nstep,10.-t1,tol)
mysolver.stepBetweenArchiving(nstepArch)
mysolver.archivingIPOnPhysicalGroup("Volume",nfield, IPField.SIG_ZZ,IPField.MEAN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",nfield, IPField.STRAIN_ZZ,IPField.MEAN_VALUE, nstepArch);
mysolver.archivingNodeDisplacement(8,2,nstepArch)
mysolver.archivingForceOnPhysicalGroup("Face", 1234, 2,nstepArch)

mysolver.solve()

check = TestCheck()
check.equal(1.118867e-02,mysolver.getArchivedNodalValue(8,2,mysolver.displacement),1e-5)
shutil.copytree("previousScheme","previousScheme_1")
shutil.rmtree("previousScheme")


mysolver.snlData(nstep,1.e2-10.,tol)
mysolver.stepBetweenArchiving(nstepArch)
mysolver.archivingIPOnPhysicalGroup("Volume",nfield, IPField.SIG_ZZ,IPField.MEAN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",nfield, IPField.STRAIN_ZZ,IPField.MEAN_VALUE, nstepArch);
mysolver.archivingNodeDisplacement(8,2,nstepArch)
mysolver.archivingForceOnPhysicalGroup("Face", 1234, 2,nstepArch)

mysolver.solve()

check = TestCheck()
check.equal(1.436662e-02,mysolver.getArchivedNodalValue(8,2,mysolver.displacement),1e-5)
shutil.copytree("previousScheme","previousScheme_2")
shutil.rmtree("previousScheme")

mysolver.snlData(nstep,1.e3-1.e2,tol)
mysolver.stepBetweenArchiving(nstepArch)
mysolver.archivingIPOnPhysicalGroup("Volume",nfield, IPField.SIG_ZZ,IPField.MEAN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",nfield, IPField.STRAIN_ZZ,IPField.MEAN_VALUE, nstepArch);
mysolver.archivingNodeDisplacement(8,2,nstepArch)
mysolver.archivingForceOnPhysicalGroup("Face", 1234, 2,nstepArch)

mysolver.solve()

check = TestCheck()
check.equal(2.011454e-02,mysolver.getArchivedNodalValue(8,2,mysolver.displacement),1e-5)
shutil.copytree("previousScheme","previousScheme_3")
shutil.rmtree("previousScheme")

mysolver.snlData(nstep,1.e4-1.e3,tol)
mysolver.stepBetweenArchiving(nstepArch)
mysolver.archivingIPOnPhysicalGroup("Volume",nfield, IPField.SIG_ZZ,IPField.MEAN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",nfield, IPField.STRAIN_ZZ,IPField.MEAN_VALUE, nstepArch);
mysolver.archivingNodeDisplacement(8,2,nstepArch)
mysolver.archivingForceOnPhysicalGroup("Face", 1234, 2,nstepArch)

mysolver.solve()

check = TestCheck()
check.equal(2.668493e-02,mysolver.getArchivedNodalValue(8,2,mysolver.displacement),1e-5)
shutil.copytree("previousScheme","previousScheme_4")
shutil.rmtree("previousScheme")

mysolver.snlData(nstep,1.e5-1.e4,tol)
mysolver.stepBetweenArchiving(nstepArch)
mysolver.archivingIPOnPhysicalGroup("Volume",nfield, IPField.SIG_ZZ,IPField.MEAN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",nfield, IPField.STRAIN_ZZ,IPField.MEAN_VALUE, nstepArch);
mysolver.archivingNodeDisplacement(8,2,nstepArch)
mysolver.archivingForceOnPhysicalGroup("Face", 1234, 2,nstepArch)

mysolver.solve()

check = TestCheck()
check.equal(3.153659e-02,mysolver.getArchivedNodalValue(8,2,mysolver.displacement),1e-5)
shutil.copytree("previousScheme","previousScheme_5")
shutil.rmtree("previousScheme")

mysolver.snlData(nstep,1.e6-1.e5,tol)
mysolver.stepBetweenArchiving(nstepArch)
mysolver.archivingIPOnPhysicalGroup("Volume",nfield, IPField.SIG_ZZ,IPField.MEAN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",nfield, IPField.STRAIN_ZZ,IPField.MEAN_VALUE, nstepArch);
mysolver.archivingNodeDisplacement(8,2,nstepArch)
mysolver.archivingForceOnPhysicalGroup("Face", 1234, 2,nstepArch)

mysolver.solve()

check = TestCheck()
check.equal(3.406729e-02,mysolver.getArchivedNodalValue(8,2,mysolver.displacement),1e-5)
shutil.copytree("previousScheme","previousScheme_6")
shutil.rmtree("previousScheme")

mysolver.snlData(nstep,ftime-1.e6,tol)
mysolver.stepBetweenArchiving(nstepArch)
mysolver.archivingIPOnPhysicalGroup("Volume",nfield, IPField.SIG_ZZ,IPField.MEAN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",nfield, IPField.STRAIN_ZZ,IPField.MEAN_VALUE, nstepArch);
mysolver.archivingNodeDisplacement(8,2,nstepArch)
mysolver.archivingForceOnPhysicalGroup("Face", 1234, 2,nstepArch)

mysolver.solve()

check = TestCheck()
check.equal(3.475814e-02,mysolver.getArchivedNodalValue(8,2,mysolver.displacement),1e-5)
shutil.copytree("previousScheme","previousScheme_7")
shutil.rmtree("previousScheme")

