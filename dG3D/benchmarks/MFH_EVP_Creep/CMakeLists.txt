# test file

set(PYFILE cube.py)

set(FILES2DELETE 
  *.msh
  *.csv
  previousScheme*
)

add_cm3python_test(${PYFILE} "${FILES2DELETE}")
