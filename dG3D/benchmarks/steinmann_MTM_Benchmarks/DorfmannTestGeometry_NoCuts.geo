SetFactory("OpenCASCADE");

Mesh.Optimize = 1;

cm = 1e-2; // Unit

pp  = "Input/10Geometric dimensions/0";

close_menu = 1;
colorpp = "Ivory";

// Current carrying wire
Rwire = 0.15*cm;
H = 0.5*cm; //10*cm;

// Smp
RintSMP = 1*cm;
RextSMP = 1.5*cm;

// Air outside smp
RextAirExterior = 2.5*cm;

ECORE = 1000;
REFCORETOP = 11111;
REFCOREOUT = 11113;
REFCOREIN = 11114;
REFCOREBOTTOM = 11115;
REFCOREPOINTTOPEXTERIOR_1 = 11116;
REFCOREPOINTTOPINTERIOR_1 = 11120;
REFCOREPOINTBOTTOMEXTERIOR_1 = 11124;
REFCOREPOINTBOTTOMINTERIOR_1 = 11128;

WIRE  = 2000;
REFWIRETOP = 2333;
REFWIREBOTTOM = 2444;
REFWIREPOINT = 2555;
REFWIREOUT = 2666;

AIRINTERIOR = 3000;
REFAIRINTERIORTOP = 3333;
REFAIRINTERIORBOTTOM = 3444;

AIREXTERIOR = 4000;
REFAIREXTERIORTOP = 4333;
REFAIREXTERIORBOTTOM = 4444;
REFAIREXTERIORPOINTTOP = 4555;
REFAIREXTERIORPOINTBOTTOM = 4666;
REFAIREXTERIOROUT = 43333;

// outer air
v()+=newv;  Cylinder(newv) = {0,0,-H/2, 0,0,H, RextAirExterior};
// Smp 
v()+=newv; Cylinder(newv) = {0,0,-H/2, 0,0,H, RextSMP};
// inner air
v()+=newv; Cylinder(newv) = {0,0,-H/2, 0,0,H, RintSMP};
// wire
v()+=newv; Cylinder(newv) = {0,0,-H/2, 0,0,H, Rwire};
	
frag()= BooleanFragments{ Volume{ v() }; Delete;} { };
//Printf("volumes  ",frag());

//Wire 
Physical Volume(WIRE) = frag(0) ;

surfWire_[] = Abs(Boundary{ Volume{frag(0)}; });
//Printf("wire surface ",surfWire_());

Physical Surface(REFWIRETOP)= surfWire_(0);
Physical Surface(REFWIREBOTTOM)= surfWire_(2);

Physical Surface(REFWIREOUT)= surfWire_(1);

ptsWire() = PointsOf { Surface { surfWire_(0) }; };
Physical Point(REFWIREPOINT)= ptsWire(0);

lcwire = Rwire/2;
Characteristic Length { PointsOf{ Volume{frag(0)}; }} = lcwire;
Recursive Color Red {Volume{frag(0)};}

// SMP
surfCore_[] = Abs(Boundary{ Volume{frag(2)}; });
//Printf("SMP core surface ",surfCore_());

Physical Volume(ECORE) = {frag(2)};

Physical Surface(REFCORETOP)= {surfCore_(0)};
Physical Surface(REFCOREBOTTOM)= {surfCore_(3)};

Physical Surface(REFCOREOUT)= {surfCore_(1)};
Physical Surface(REFCOREIN)= {surfCore_(2)};

pts1() = PointsOf {Surface { surfCore_(0) }; };
//Printf("SMP points top surface ",pts1());
Physical Point(REFCOREPOINTTOPEXTERIOR_1)= pts1(0);
Physical Point(REFCOREPOINTTOPINTERIOR_1)= pts1(1);

pts2() = PointsOf {Surface { surfCore_(3) }; };
//Printf("SMP points bottom surface ",pts2());
Physical Point(REFCOREPOINTBOTTOMEXTERIOR_1)= pts2(0);
Physical Point(REFCOREPOINTBOTTOMINTERIOR_1)= pts2(1);

lcSMP  = RextSMP/12;
Characteristic Length { PointsOf{ Volume{frag(2)}; } } = lcSMP;
Recursive Color SteelBlue {Volume{frag(2)};}

// Air interior between wire and SMP Core
Physical Volume(AIRINTERIOR) = frag(3) ;

surfAirInterior_[] = Abs(Boundary{ Volume{frag(3)}; });
//Printf("Air interior surface ",surfAirInterior_());

Physical Surface(REFAIRINTERIORTOP)= surfAirInterior_(0);
Physical Surface(REFAIRINTERIORBOTTOM)= surfAirInterior_(3);

//lcAIRINTERIOR  = RintSMP/14;
//Characteristic Length { PointsOf{ Volume{frag(3)}; }} = lcAIRINTERIOR;

// Air exterior to SMP Core
Physical Volume(AIREXTERIOR) = frag(1) ;

surfAirExterior_[] = Abs(Boundary{ Volume{frag(1)}; });
//Printf("Air Exterior surface ",surfAirExterior_());

Physical Surface(REFAIREXTERIORTOP)= surfAirExterior_(1);
Physical Surface(REFAIREXTERIORBOTTOM)= surfAirExterior_(2);

Physical Surface(REFAIREXTERIOROUT)= surfAirExterior_(0);

ptsAIREXTERIOR() = PointsOf {Surface { surfAirExterior_(1) }; };
Physical Point(REFAIREXTERIORPOINTTOP)= ptsAIREXTERIOR(0);

ptsAIREXTERIORBOTTOM() = PointsOf {Surface { surfAirExterior_(2) }; };
Physical Point(REFAIREXTERIORPOINTBOTTOM)= ptsAIREXTERIORBOTTOM(0);

lcAIREXTERIOR  = RextAirExterior/15;
Characteristic Length { PointsOf{ Volume{frag(1)}; }} = lcAIREXTERIOR;
Recursive Color Green {Volume{frag(1)};}

