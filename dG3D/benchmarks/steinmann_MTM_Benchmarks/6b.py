import numpy as npy
import matplotlib
import matplotlib.pyplot as plt
import pandas as pd
import openpyxl
import matplotlib.ticker as ticker
import pickle
import os
from utils import *

# from scipy.optimize import curve_fit

mehnertDataFolderName = 'mehnert_steinmann_2017_data'
mehnertFileName = '6b.csv'
os.system('cp ' + mehnertDataFolderName + '/' + mehnertFileName + ' ' + os.getcwd())

# Read Mehnert and Steinmann 2017 data to plot
df1 = pd.read_csv(mehnertFileName)
# print(df1)

l1 = 2.0 # values from Mehnert for 2b always at lambda_i = 2
# data for tests with no EM fields (c=0)
p1 = df1.iloc[0, 1] # zeta=1.5, lambda_z = 0.5
p2 = df1.iloc[0, 3] # zeta=2, lambda_z = 0.5
p3 = df1.iloc[0, 5] # zeta=2.5, lambda_z = 0.5
p4 = df1.iloc[4, 1] # zeta=1.5, lambda_z = 1.5
p5 = df1.iloc[4, 3] # zeta=2, lambda_z = 1.5
p6 = df1.iloc[4, 5] # zeta=2.5, lambda_z = 1.5
p7 = df1.iloc[6, 1] # zeta=1.5, lambda_z = 2.
p8 = df1.iloc[6, 3] # zeta=2, lambda_z = 2.
p9 = df1.iloc[6, 5] # zeta=2.5, lambda_z = 2.
# data for tests with EM fields (c=1)
p10 = df1.iloc[0, 7] # zeta=1.5, lambda_z = 0.5
p11 = df1.iloc[0, 9] # zeta=2, lambda_z = 0.5
p12 = df1.iloc[0, 11] # zeta=2.5, lambda_z = 0.5
p13 = df1.iloc[4, 7] # zeta=1.5, lambda_z = 1.5
p14 = df1.iloc[4, 9] # zeta=2, lambda_z = 1.5
p15 = df1.iloc[4, 11] # zeta=2.5, lambda_z = 1.5
p16 = df1.iloc[6, 7] # zeta=1.5, lambda_z = 2.
p17 = df1.iloc[6, 9] # zeta=2, lambda_z = 2.
p18 = df1.iloc[6, 11] # zeta=2.5, lambda_z = 2.

# Read Gholap et al. data to plot
gholapDataFolderName = 'gholap_data'

# no EM fields (pure TM simulation)
gholapFileName2 = 'combinedRadialAxialStretch_lambda_z_50_zeta_15_c_0_temp_293_NormalPressure.csv'
os.system('cp ' + gholapDataFolderName + '/' + gholapFileName2 + ' ' + os.getcwd())
df2 = pd.read_csv(gholapFileName2,sep=';')
timeStep2 = df2.iloc[:, 0]
radial_Stretch2 = df2.iloc[:, 1]
pressure2 = df2.iloc[:, 2]

gholapFileName3 = 'combinedRadialAxialStretch_lambda_z_50_zeta_20_c_0_temp_293_NormalPressure.csv'
os.system('cp ' + gholapDataFolderName + '/' + gholapFileName3 + ' ' + os.getcwd())
df3 = pd.read_csv(gholapFileName3,sep=';')
timeStep3 = df3.iloc[:, 0]
radial_Stretch3 = df3.iloc[:, 1]
pressure3 = df3.iloc[:, 2]

gholapFileName4 = 'combinedRadialAxialStretch_lambda_z_50_zeta_25_c_0_temp_293_NormalPressure.csv'
os.system('cp ' + gholapDataFolderName + '/' + gholapFileName4 + ' ' + os.getcwd())
df4 = pd.read_csv(gholapFileName4,sep=';')
timeStep4 = df4.iloc[:, 0]
radial_Stretch4 = df4.iloc[:, 1]
pressure4 = df4.iloc[:, 2]

gholapFileName5 = 'combinedRadialAxialStretch_lambda_z_150_zeta_15_c_0_temp_293_NormalPressure.csv'
os.system('cp ' + gholapDataFolderName + '/' + gholapFileName5 + ' ' + os.getcwd())
df5 = pd.read_csv(gholapFileName5,sep=';')
timeStep5 = df5.iloc[:, 0]
radial_Stretch5 = df5.iloc[:, 1]
pressure5 = df5.iloc[:, 2]

gholapFileName6 = 'combinedRadialAxialStretch_lambda_z_150_zeta_20_c_0_temp_293_NormalPressure.csv'
os.system('cp ' + gholapDataFolderName + '/' + gholapFileName6 + ' ' + os.getcwd())
df6 = pd.read_csv(gholapFileName6,sep=';')
timeStep6 = df6.iloc[:, 0]
radial_Stretch6 = df6.iloc[:, 1]
pressure6 = df6.iloc[:, 2]

gholapFileName7 = 'combinedRadialAxialStretch_lambda_z_150_zeta_25_c_0_temp_293_NormalPressure.csv'
os.system('cp ' + gholapDataFolderName + '/' + gholapFileName7 + ' ' + os.getcwd())
df7 = pd.read_csv(gholapFileName7,sep=';')
timeStep7 = df7.iloc[:, 0]
radial_Stretch7 = df7.iloc[:, 1]
pressure7 = df7.iloc[:, 2]

gholapFileName8 = 'combinedRadialAxialStretch_lambda_z_200_zeta_15_c_0_temp_293_NormalPressure.csv'
os.system('cp ' + gholapDataFolderName + '/' + gholapFileName8 + ' ' + os.getcwd())
df8 = pd.read_csv(gholapFileName8,sep=';')
timeStep8 = df8.iloc[:, 0]
radial_Stretch8 = df8.iloc[:, 1]
pressure8 = df8.iloc[:, 2]

gholapFileName9 = 'combinedRadialAxialStretch_lambda_z_200_zeta_20_c_0_temp_293_NormalPressure.csv'
os.system('cp ' + gholapDataFolderName + '/' + gholapFileName9 + ' ' + os.getcwd())
df9 = pd.read_csv(gholapFileName9,sep=';')
timeStep9 = df9.iloc[:, 0]
radial_Stretch9 = df9.iloc[:, 1]
pressure9 = df9.iloc[:, 2]

gholapFileName10 = 'combinedRadialAxialStretch_lambda_z_200_zeta_25_c_0_temp_293_NormalPressure.csv'
os.system('cp ' + gholapDataFolderName + '/' + gholapFileName10 + ' ' + os.getcwd())
df10 = pd.read_csv(gholapFileName10,sep=';')
timeStep10 = df10.iloc[:, 0]
radial_Stretch10 = df10.iloc[:, 1]
pressure10 = df10.iloc[:, 2]

# with EM field-dependent shear modulus test results
# gholapFileName11 = 'combinedRadialAxialStretch_lambda_z_50_zeta_15_c_6_temp_293_NormalPressure.csv'
# os.system('cp ' + gholapDataFolderName + '/' + gholapFileName11 + ' ' + os.getcwd())
# df11 = pd.read_csv(gholapFileName11,sep=';')
# timeStep11 = df11.iloc[:, 0]
# radial_Stretch11 = df11.iloc[:, 1]
# pressure11 = df11.iloc[:, 2]

# gholapFileName12 = 'combinedRadialAxialStretch_lambda_z_50_zeta_20_c_6_temp_293_NormalPressure.csv'
# os.system('cp ' + gholapDataFolderName + '/' + gholapFileName12 + ' ' + os.getcwd())
# df12 = pd.read_csv(gholapFileName12,sep=';')
# timeStep12 = df12.iloc[:, 0]
# radial_Stretch12 = df12.iloc[:, 1]
# pressure12 = df12.iloc[:, 2]

# gholapFileName13 = 'combinedRadialAxialStretch_lambda_z_50_zeta_25_c_6_temp_293_NormalPressure.csv'
# os.system('cp ' + gholapDataFolderName + '/' + gholapFileName13 + ' ' + os.getcwd())
# df13 = pd.read_csv(gholapFileName13,sep=';')
# timeStep13 = df13.iloc[:, 0]
# radial_Stretch13 = df13.iloc[:, 1]
# pressure13 = df13.iloc[:, 2]

# gholapFileName14 = 'combinedRadialAxialStretch_lambda_z_150_zeta_15_c_6_temp_293_NormalPressure.csv'
# os.system('cp ' + gholapDataFolderName + '/' + gholapFileName14 + ' ' + os.getcwd())
# df14 = pd.read_csv(gholapFileName14,sep=';')
# timeStep14 = df14.iloc[:, 0]
# radial_Stretch14 = df14.iloc[:, 1]
# pressure14 = df14.iloc[:, 2]

# gholapFileName15 = 'combinedRadialAxialStretch_lambda_z_150_zeta_20_c_6_temp_293_NormalPressure.csv'
# os.system('cp ' + gholapDataFolderName + '/' + gholapFileName15 + ' ' + os.getcwd())
# df15 = pd.read_csv(gholapFileName15,sep=';')
# timeStep15 = df15.iloc[:, 0]
# radial_Stretch15 = df15.iloc[:, 1]
# pressure15 = df15.iloc[:, 2]

# gholapFileName16 = 'combinedRadialAxialStretch_lambda_z_150_zeta_25_c_6_temp_293_NormalPressure.csv'
# os.system('cp ' + gholapDataFolderName + '/' + gholapFileName16 + ' ' + os.getcwd())
# df16 = pd.read_csv(gholapFileName16,sep=';')
# timeStep16 = df16.iloc[:, 0]
# radial_Stretch16 = df16.iloc[:, 1]
# pressure16 = df16.iloc[:, 2]

# gholapFileName17 = 'combinedRadialAxialStretch_lambda_z_200_zeta_15_c_6_temp_293_NormalPressure.csv'
# os.system('cp ' + gholapDataFolderName + '/' + gholapFileName17 + ' ' + os.getcwd())
# df17 = pd.read_csv(gholapFileName17,sep=';')
# timeStep17 = df17.iloc[:, 0]
# radial_Stretch17 = df17.iloc[:, 1]
# pressure17 = df17.iloc[:, 2]

# gholapFileName18 = 'combinedRadialAxialStretch_lambda_z_200_zeta_20_c_6_temp_293_NormalPressure.csv'
# os.system('cp ' + gholapDataFolderName + '/' + gholapFileName18 + ' ' + os.getcwd())
# df18 = pd.read_csv(gholapFileName18,sep=';')
# timeStep18 = df18.iloc[:, 0]
# radial_Stretch18 = df18.iloc[:, 1]
# pressure18 = df18.iloc[:, 2]

# gholapFileName19 = 'combinedRadialAxialStretch_lambda_z_200_zeta_25_c_6_temp_293_NormalPressure.csv'
# os.system('cp ' + gholapDataFolderName + '/' + gholapFileName19 + ' ' + os.getcwd())
# df19 = pd.read_csv(gholapFileName19,sep=';')
# timeStep19 = df19.iloc[:, 0]
# radial_Stretch19 = df19.iloc[:, 1]
# pressure19 = df19.iloc[:, 2]

# plot data
matplotlib.rc('text', usetex=True)
matplotlib.rc('font', **{'family': 'serif', 'serif': ['Computer Modern Roman'],
                         'monospace': ['Computer Modern Typewriter']})

fig2 = plt.figure(figsize=(6, 6), layout="constrained")
# no EM fields
plt.plot(l1, p1, marker='o',c='blue',linestyle='none',fillstyle='full', label='Mehnert [2017] $c=0$ A, $\zeta=1.5$', linewidth=1.)
plt.plot(l1, p2, marker='X',c='red',linestyle='none',fillstyle='full', label='Mehnert [2017] $c=0$ A, $\zeta=2$', linewidth=1.)
plt.plot(l1, p3, marker='s',c='black',linestyle='none',fillstyle='full', label='Mehnert [2017] $c=0$ A, $\zeta=2.5$', linewidth=1.)
plt.plot(l1, p4, marker='o',c='blue',linestyle='none',fillstyle='full', linewidth=1.)
plt.plot(l1, p5, marker='X',c='red',linestyle='none',fillstyle='full', linewidth=1.)
plt.plot(l1, p6, marker='s',c='black',linestyle='none',fillstyle='full', linewidth=1.)
plt.plot(l1, p7, marker='o',c='blue',linestyle='none',fillstyle='full', linewidth=1.)
plt.plot(l1, p8, marker='X',c='red',linestyle='none',fillstyle='full', linewidth=1.)
plt.plot(l1, p9, marker='s',c='black',linestyle='none',fillstyle='full', linewidth=1.)
# with EM fields
plt.plot(l1, p10, marker='o',c='blue',linestyle='none', fillstyle='none', label='Mehnert [2017] $c=1$ A, $\zeta=1.5$', linewidth=1.)
plt.plot(l1, p11, marker='X',c='red',linestyle='none', fillstyle='none', label='Mehnert [2017] $c=1$ A, $\zeta=2$', linewidth=1.)
plt.plot(l1, p12, marker='s',c='black',linestyle='none', fillstyle='none', label='Mehnert [2017] $c=1$ A, $\zeta=2.5$', linewidth=1.)
plt.plot(l1, p13, marker='o',c='blue',linestyle='none', fillstyle='none', linewidth=1.)
plt.plot(l1, p14, marker='X',c='red',linestyle='none', fillstyle='none', linewidth=1.)
plt.plot(l1, p15, marker='s',c='black',linestyle='none', fillstyle='none', linewidth=1.)
plt.plot(l1, p16, marker='o',c='blue',linestyle='none', fillstyle='none', linewidth=1.)
plt.plot(l1, p17, marker='X',c='red',linestyle='none', fillstyle='none', linewidth=1.)
plt.plot(l1, p18, marker='s',c='black',linestyle='none', fillstyle='none', linewidth=1.)

# no EM fields (pure TM simulation)
plt.plot(radial_Stretch2,pressure2/1.e3,c='blue',linestyle='-',fillstyle='full',label='Gholap [2024] $c=0$ A, $\zeta=1.5$',linewidth=1.)
plt.plot(radial_Stretch3,pressure3/1.e3,c='red',linestyle='-',fillstyle='full',label='Gholap [2024] $c=0$ A, $\zeta=2$',linewidth=1.)
plt.plot(radial_Stretch4,pressure4/1.e3,c='black',linestyle='-',fillstyle='full',label='Gholap [2024] $c=0$ A, $\zeta=2.5$',linewidth=1.)
plt.plot(radial_Stretch5,pressure5/1.e3,c='blue',linestyle='-',fillstyle='full', linewidth=1.)
plt.plot(radial_Stretch6,pressure6/1.e3,c='red',linestyle='-',fillstyle='full', linewidth=1.)
plt.plot(radial_Stretch7,pressure7/1.e3,c='black',linestyle='-',fillstyle='full', linewidth=1.)
plt.plot(radial_Stretch8,pressure8/1.e3,c='blue',linestyle='-',fillstyle='full', linewidth=1.)
plt.plot(radial_Stretch9,pressure9/1.e3,c='red',linestyle='-',fillstyle='full', linewidth=1.)
plt.plot(radial_Stretch10,pressure10/1.e3,c='black',linestyle='-',fillstyle='full', linewidth=1.)

# with EM field-dependent shear modulus test results
# plt.plot(radial_Stretch11,pressure11/1.e3,c='blue',linestyle='--',fillstyle='full',label='Gholap [2024] $c=1$ A, $\zeta=1.5$',linewidth=1.5)
# plt.plot(radial_Stretch12,pressure12/1.e3,c='red',linestyle='--',fillstyle='full',label='Gholap [2024] $c=1$ A, $\zeta=2$',linewidth=1.5)
# plt.plot(radial_Stretch13,pressure13/1.e3,c='black',linestyle='--',fillstyle='full',label='Gholap [2024] $c=1$ A, $\zeta=2.5$',linewidth=1.5)
# plt.plot(radial_Stretch14,pressure14/1.e3,c='blue',linestyle='--',fillstyle='full', linewidth=1.5)
# plt.plot(radial_Stretch15,pressure15/1.e3,c='red',linestyle='--',fillstyle='full', linewidth=1.5)
# plt.plot(radial_Stretch16,pressure16/1.e3,c='black',linestyle='--',fillstyle='full', linewidth=1.5)
# plt.plot(radial_Stretch17,pressure17/1.e3,c='blue',linestyle='--',fillstyle='full', linewidth=1.5)
# plt.plot(radial_Stretch18,pressure18/1.e3,c='red',linestyle='--',fillstyle='full', linewidth=1.5)
# plt.plot(radial_Stretch19,pressure19/1.e3,c='black',linestyle='--',fillstyle='full', linewidth=1.5)

ax2 = plt.gca()

# popt, pcov = curve_fit(fit_func_,l1,p1)
# ax2.plot(l1,fit_func_(l1,*popt),'r--',label='Curve fit',linewidth=2.)

ax2.set_xlabel(u"$\lambda_i$", fontsize=16)
ax2.set_ylabel(u"$\overline{N}$ [kPa]", fontsize=16)
ax2.legend(loc='lower center', bbox_to_anchor=(0.5, -0.3), ncols=2, frameon=False, fontsize=9, labelspacing=0.1)

ratio = 1
xleft, xright = ax2.get_xlim()
ybottom, ytop = ax2.get_ylim()
# ax2.set_aspect(abs((xright-xleft)/(ybottom-ytop))*ratio)

# ax2.text(2.0, -150, "$\lambda_i = 2$", size=16)
# ax2.text(1.2, 10, "$\\alpha_e = 0.15$, $m_e = \mu_0^2$ T$^2$", size=12)
ax2.text(1.7, -1150, "$\lambda_z=0.5$", size=16)
ax2.text(1.5, 690, "$\lambda_z=1.5$", size=16)
ax2.text(0.7, 990, "$\lambda_z=2$", size=16)

ax2.tick_params(direction='in', labelsize=14, axis='both', which='major', pad=7)
# ax2.grid(linestyle='--', linewidth=.3)
ax2.tick_params(bottom=True, top=True, left=True, right=True)

ax2.margins(0.05)
# plt.xticks(npy.arange(0.5,3.1,0.5))
# plt.yticks(npy.arange(-200,101,50))
ax2.xaxis.set_major_locator(ticker.MultipleLocator(0.1))
ax2.yaxis.set_major_locator(ticker.MultipleLocator(500))

# plt.tight_layout(pad=0.)
fig2.savefig('6b_comparison.png', bbox_inches='tight', dpi=1000)
plt.show()
