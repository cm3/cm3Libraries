import os,csv
import sys
import numpy as np
import pickle
import math

RextArray = np.array([1.5e-2, 2.e-2, 2.5e-2]);
meshfileArray = np.array(['newDorfmannTestGeom_1.msh', 'newDorfmannTestGeom_2.msh', 'newDorfmannTestGeom_3.msh']);
currentArray = np.array([0.0, 1.0]); # A
tAppliedArray = np.array([193., 293., 393.]);

ftime = 1.0
nuSMP = 0.495 # Poisson ratio
nuVac = 0.25 # Poisson ratio
RintSMP = 1.e-2 # m
H_SMP = 2.e-3 # m
# flagCompressionTest = False # if compression stretch radial

# depending on external radius of smp, choose relevant mesh file
# RextSMP = RextArray[0] # m
# meshfile = meshfileArray[0] # name of mesh file
# flagUseEMStress = True
# flagEMFieldDependentShearModulus = True
#lambda_zArray = np.array([0.5,0.75,1.0,1.25,1.5,1.75,2.0]);
# lambda_z = 1.0 # axial stretch = l/L

# use thermal source or not: (capacitive + meca + EM) thermal source
thermalSource = False 

# to apply appropriate pressure BC:
# either on inner surface for radial inflation or compression
# or on start and end surface for axial tensile or compressive stretch
# or both in case of some radial stretch and then axial stretch
# applyPressureBCForRadialStretch = True
# applyPressureBCWithAxialStretch = False

#pressure data from mehnert et al.
# plot 2a lambda_z = 1 purely radial stretch lambda_i = 2: 
#zeta=1.5; RextSMP = RextArray[0]; meshfile = meshfileArray[0]; c=0.; flagUseEMStress = False; flagEMFieldDependentShearModulus = False; applyPressureBCForRadialStretch = True; applyPressureBCWithAxialStretch = False; lambda_z = 1.0; flagCompressionTest = False; Pmax = 38775 #Pa
#zeta=2.0; RextSMP = RextArray[1]; meshfile = meshfileArray[1]; c=0.; flagUseEMStress = False; flagEMFieldDependentShearModulus = False; applyPressureBCForRadialStretch = True; applyPressureBCWithAxialStretch = False; lambda_z = 1.0; flagCompressionTest = False; Pmax = 65306
#zeta=2.5; RextSMP = RextArray[2]; meshfile = meshfileArray[2]; c=0.; flagUseEMStress = False; flagEMFieldDependentShearModulus = False; applyPressureBCForRadialStretch = True; applyPressureBCWithAxialStretch = False; lambda_z = 1.0; flagCompressionTest = False; Pmax = 84183
zeta=1.5; RextSMP = RextArray[0]; meshfile = meshfileArray[0]; c=1.; flagUseEMStress = True; flagEMFieldDependentShearModulus = True; applyPressureBCForRadialStretch = True; applyPressureBCWithAxialStretch = False; lambda_z = 1.0; flagCompressionTest = False; Pmax = 46938
#zeta=2.0; RextSMP = RextArray[1]; meshfile = meshfileArray[1]; c=1.; flagUseEMStress = True; flagEMFieldDependentShearModulus = True; applyPressureBCForRadialStretch = True; applyPressureBCWithAxialStretch = False; lambda_z = 1.0; flagCompressionTest = False; Pmax = 76020
#zeta=2.5; RextSMP = RextArray[2]; meshfile = meshfileArray[2]; c=1.; flagUseEMStress = True; flagEMFieldDependentShearModulus = True; applyPressureBCForRadialStretch = True; applyPressureBCWithAxialStretch = False; lambda_z = 1.0; flagCompressionTest = False; Pmax = 96428
# plot 2a lambda_z = 1 purely radial compression lambda_i = 0.5:
#zeta=1.5; RextSMP = RextArray[0]; meshfile = meshfileArray[0]; c=0.; flagUseEMStress = False; flagEMFieldDependentShearModulus = False; applyPressureBCForRadialStretch = True; applyPressureBCWithAxialStretch = False; lambda_z = 1.0; flagCompressionTest = True; Pmax = -173980 #Pa
#zeta=2.0; RextSMP = RextArray[1]; meshfile = meshfileArray[1]; c=0.; flagUseEMStress = False; flagEMFieldDependentShearModulus = False; applyPressureBCForRadialStretch = True; applyPressureBCWithAxialStretch = False; lambda_z = 1.0; flagCompressionTest = True; Pmax = -196940
#zeta=2.5; RextSMP = RextArray[2]; meshfile = meshfileArray[2]; c=0.; flagUseEMStress = False; flagEMFieldDependentShearModulus = False; applyPressureBCForRadialStretch = True; applyPressureBCWithAxialStretch = False; lambda_z = 1.0; flagCompressionTest = True; Pmax = -199491
#zeta=1.5; RextSMP = RextArray[0]; meshfile = meshfileArray[0]; c=1.; flagUseEMStress = True; flagEMFieldDependentShearModulus = True; applyPressureBCForRadialStretch = True; applyPressureBCWithAxialStretch = False; lambda_z = 1.0; flagCompressionTest = True; Pmax = -200004
#zeta=2.0; RextSMP = RextArray[1]; meshfile = meshfileArray[1]; c=1.; flagUseEMStress = True; flagEMFieldDependentShearModulus = True; applyPressureBCForRadialStretch = True; applyPressureBCWithAxialStretch = False; lambda_z = 1.0; flagCompressionTest = True; Pmax = -200008
#zeta=2.5; RextSMP = RextArray[2]; meshfile = meshfileArray[2]; c=1.; flagUseEMStress = True; flagEMFieldDependentShearModulus = True; applyPressureBCForRadialStretch = True; applyPressureBCWithAxialStretch = False; lambda_z = 1.0; flagCompressionTest = True; Pmax = -199500

# plot 2b lambda_i = 2 combined radial and axial stretch:
#zeta=1.5; RextSMP = RextArray[0]; meshfile = meshfileArray[0]; c=0.; flagUseEMStress = False; flagEMFieldDependentShearModulus = False; applyPressureBCForRadialStretch = False; applyPressureBCWithAxialStretch = True; lambda_z = 0.5; flagCompressionTest = False; Pmax = 51758 #Pa
#zeta=1.5; RextSMP = RextArray[0]; meshfile = meshfileArray[0]; c=0.; flagUseEMStress = False; flagEMFieldDependentShearModulus = False; applyPressureBCForRadialStretch = False; applyPressureBCWithAxialStretch = True; lambda_z = 1.0; flagCompressionTest = False; Pmax = 35970
#zeta=1.5; RextSMP = RextArray[0]; meshfile = meshfileArray[0]; c=0.; flagUseEMStress = False; flagEMFieldDependentShearModulus = False; applyPressureBCForRadialStretch = False; applyPressureBCWithAxialStretch = True; lambda_z = 1.5; flagCompressionTest = False; Pmax = 25358
#zeta=1.5; RextSMP = RextArray[0]; meshfile = meshfileArray[0]; c=0.; flagUseEMStress = False; flagEMFieldDependentShearModulus = False; applyPressureBCForRadialStretch = False; applyPressureBCWithAxialStretch = True; lambda_z = 2.0; flagCompressionTest = False; Pmax = 19348
#zeta=2.0; RextSMP = RextArray[1]; meshfile = meshfileArray[1]; c=0.; flagUseEMStress = False; flagEMFieldDependentShearModulus = False; applyPressureBCForRadialStretch = False; applyPressureBCWithAxialStretch = True; lambda_z = 0.5; flagCompressionTest = False; Pmax = 76869
#zeta=2.0; RextSMP = RextArray[1]; meshfile = meshfileArray[1]; c=0.; flagUseEMStress = False; flagEMFieldDependentShearModulus = False; applyPressureBCForRadialStretch = False; applyPressureBCWithAxialStretch = True; lambda_z = 1.0; flagCompressionTest = False; Pmax = 57248
#zeta=2.0; RextSMP = RextArray[1]; meshfile = meshfileArray[1]; c=0.; flagUseEMStress = False; flagEMFieldDependentShearModulus = False; applyPressureBCForRadialStretch = False; applyPressureBCWithAxialStretch = True; lambda_z = 1.5; flagCompressionTest = False; Pmax = 41844
#zeta=2.0; RextSMP = RextArray[1]; meshfile = meshfileArray[1]; c=0.; flagUseEMStress = False; flagEMFieldDependentShearModulus = False; applyPressureBCForRadialStretch = False; applyPressureBCWithAxialStretch = True; lambda_z = 2.0; flagCompressionTest = False; Pmax = 32574
#zeta=2.5; RextSMP = RextArray[2]; meshfile = meshfileArray[2]; c=0.; flagUseEMStress = False; flagEMFieldDependentShearModulus = False; applyPressureBCForRadialStretch = False; applyPressureBCWithAxialStretch = True; lambda_z = 0.5; flagCompressionTest = False; Pmax = 90671
#zeta=2.5; RextSMP = RextArray[2]; meshfile = meshfileArray[2]; c=0.; flagUseEMStress = False; flagEMFieldDependentShearModulus = False; applyPressureBCForRadialStretch = False; applyPressureBCWithAxialStretch = True; lambda_z = 1.0; flagCompressionTest = False; Pmax = 71050
#zeta=2.5; RextSMP = RextArray[2]; meshfile = meshfileArray[2]; c=0.; flagUseEMStress = False; flagEMFieldDependentShearModulus = False; applyPressureBCForRadialStretch = False; applyPressureBCWithAxialStretch = True; lambda_z = 1.5; flagCompressionTest = False; Pmax = 52962
#zeta=2.5; RextSMP = RextArray[2]; meshfile = meshfileArray[2]; c=0.; flagUseEMStress = False; flagEMFieldDependentShearModulus = False; applyPressureBCForRadialStretch = False; applyPressureBCWithAxialStretch = True; lambda_z = 2.0; flagCompressionTest = False; Pmax = 41776
#zeta=1.5; RextSMP = RextArray[0]; meshfile = meshfileArray[0]; c=1.0; flagUseEMStress = True; flagEMFieldDependentShearModulus = True; applyPressureBCForRadialStretch = False; applyPressureBCWithAxialStretch = True; lambda_z = 0.5; flagCompressionTest = False; Pmax = 65559
#zeta=1.5; RextSMP = RextArray[0]; meshfile = meshfileArray[0]; c=1.0; flagUseEMStress = True; flagEMFieldDependentShearModulus = True; applyPressureBCForRadialStretch = False; applyPressureBCWithAxialStretch = True; lambda_z = 1.0; flagCompressionTest = False; Pmax = 43254
#zeta=1.5; RextSMP = RextArray[0]; meshfile = meshfileArray[0]; c=1.0; flagUseEMStress = True; flagEMFieldDependentShearModulus = True; applyPressureBCForRadialStretch = False; applyPressureBCWithAxialStretch = True; lambda_z = 1.5; flagCompressionTest = False; Pmax = 30534
#zeta=1.5; RextSMP = RextArray[0]; meshfile = meshfileArray[0]; c=1.0; flagUseEMStress = True; flagEMFieldDependentShearModulus = True; applyPressureBCForRadialStretch = False; applyPressureBCWithAxialStretch = True; lambda_z = 2.0; flagCompressionTest = False; Pmax = 23373
#zeta=2.0; RextSMP = RextArray[1]; meshfile = meshfileArray[1]; c=1.0; flagUseEMStress = True; flagEMFieldDependentShearModulus = True; applyPressureBCForRadialStretch = False; applyPressureBCWithAxialStretch = True; lambda_z = 0.5; flagCompressionTest = False; Pmax = 95080
#zeta=2.0; RextSMP = RextArray[1]; meshfile = meshfileArray[1]; c=1.0; flagUseEMStress = True; flagEMFieldDependentShearModulus = True; applyPressureBCForRadialStretch = False; applyPressureBCWithAxialStretch = True; lambda_z = 1.0; flagCompressionTest = False; Pmax = 66833
#zeta=2.0; RextSMP = RextArray[1]; meshfile = meshfileArray[1]; c=1.0; flagUseEMStress = True; flagEMFieldDependentShearModulus = True; applyPressureBCForRadialStretch = False; applyPressureBCWithAxialStretch = True; lambda_z = 1.5; flagCompressionTest = False; Pmax = 48362
#zeta=2.0; RextSMP = RextArray[1]; meshfile = meshfileArray[1]; c=1.0; flagUseEMStress = True; flagEMFieldDependentShearModulus = True; applyPressureBCForRadialStretch = False; applyPressureBCWithAxialStretch = True; lambda_z = 2.0; flagCompressionTest = False; Pmax = 37366
#zeta=2.5; RextSMP = RextArray[2]; meshfile = meshfileArray[2]; c=1.0; flagUseEMStress = True; flagEMFieldDependentShearModulus = True; applyPressureBCForRadialStretch = False; applyPressureBCWithAxialStretch = True; lambda_z = 0.5; flagCompressionTest = False; Pmax = 110415
#zeta=2.5; RextSMP = RextArray[2]; meshfile = meshfileArray[2]; c=1.0; flagUseEMStress = True; flagEMFieldDependentShearModulus = True; applyPressureBCForRadialStretch = False; applyPressureBCWithAxialStretch = True; lambda_z = 1.0; flagCompressionTest = False; Pmax = 81401
#zeta=2.5; RextSMP = RextArray[2]; meshfile = meshfileArray[2]; c=1.0; flagUseEMStress = True; flagEMFieldDependentShearModulus = True; applyPressureBCForRadialStretch = False; applyPressureBCWithAxialStretch = True; lambda_z = 1.5; flagCompressionTest = False; Pmax = 60247
#zeta=2.5; RextSMP = RextArray[2]; meshfile = meshfileArray[2]; c=1.0; flagUseEMStress = True; flagEMFieldDependentShearModulus = True; applyPressureBCForRadialStretch = False; applyPressureBCWithAxialStretch = True; lambda_z = 2.0; flagCompressionTest = False; Pmax = 47143

# plot 4b current sweep with lambda_z = 1 purely radial stretch lambda_i = 2:
#zeta=2.5; RextSMP = RextArray[2]; meshfile = meshfileArray[2]; c=7.5; flagUseEMStress = True; flagEMFieldDependentShearModulus = True; applyPressureBCForRadialStretch = True; applyPressureBCWithAxialStretch = False; lambda_z = 1.0; flagCompressionTest = False; Pmax = 643000
#zeta=2.5; RextSMP = RextArray[2]; meshfile = meshfileArray[2]; c=15.0; flagUseEMStress = True; flagEMFieldDependentShearModulus = True; applyPressureBCForRadialStretch = True; applyPressureBCWithAxialStretch = False; lambda_z = 1.0; flagCompressionTest = False; Pmax = 1621302
#zeta=2.5; RextSMP = RextArray[2]; meshfile = meshfileArray[2]; c=22.5; flagUseEMStress = True; flagEMFieldDependentShearModulus = True; applyPressureBCForRadialStretch = True; applyPressureBCWithAxialStretch = False; lambda_z = 1.0; flagCompressionTest = False; Pmax = 2063116
#zeta=2.5; RextSMP = RextArray[2]; meshfile = meshfileArray[2]; c=30.0; flagUseEMStress = True; flagEMFieldDependentShearModulus = True; applyPressureBCForRadialStretch = True; applyPressureBCWithAxialStretch = False; lambda_z = 1.0; flagCompressionTest = False; Pmax = 2173570

# plot 4b current sweep with lambda_z = 1 purely radial compression lambda_i = 0.5:
#zeta=2.5; RextSMP = RextArray[2]; meshfile = meshfileArray[2]; c=7.5; flagUseEMStress = True; flagEMFieldDependentShearModulus = True; applyPressureBCForRadialStretch = True; applyPressureBCWithAxialStretch = False; lambda_z = 1.0; flagCompressionTest = True; Pmax = -2544379
#zeta=2.5; RextSMP = RextArray[2]; meshfile = meshfileArray[2]; c=15.0; flagUseEMStress = True; flagEMFieldDependentShearModulus = True; applyPressureBCForRadialStretch = True; applyPressureBCWithAxialStretch = False; lambda_z = 1.0; flagCompressionTest = True; Pmax = -5637081
#zeta=2.5; RextSMP = RextArray[2]; meshfile = meshfileArray[2]; c=22.5; flagUseEMStress = True; flagEMFieldDependentShearModulus = True; applyPressureBCForRadialStretch = True; applyPressureBCWithAxialStretch = False; lambda_z = 1.0; flagCompressionTest = True; Pmax = -6268245
#zeta=2.5; RextSMP = RextArray[2]; meshfile = meshfileArray[2]; c=30.0; flagUseEMStress = True; flagEMFieldDependentShearModulus = True; applyPressureBCForRadialStretch = True; applyPressureBCWithAxialStretch = False; lambda_z = 1.0; flagCompressionTest = True; Pmax = -6362919

# to apply appropriate thermal BC
# temperature prescribed on outer cylindrical surface of smp
# with tinitial = 293. K applied on inner cylindrical surface of smp
# depending on tApplied, either a temperature gradient is prescribed or none 
tApplied = tAppliedArray[1] # K

# to apply current BC to generate magnetic field
# if nonzero current applied, there is electric and magnetic fields in the problem
current = c*2.0*math.pi # A

# directory to copy results files after run
print('Creating folder to copy results files.')

if(meshfile=='newDorfmannTestGeom_1.msh'):
  zeta_ = 15 # 1.5 acutally but cannot have dot in folder name
elif(meshfile=='newDorfmannTestGeom_2.msh'):
  zeta_ = 20 # 2.0
elif(meshfile=='newDorfmannTestGeom_3.msh'):
  zeta_ = 25 # 2.5
else:
  print('Warning! Unknown mesh file. Using zeta_ = 0 to generate results folder.')
  zeta_ = 0 #
  
if(applyPressureBCForRadialStretch):
  tempName = 'pureRadialStretch'
elif(applyPressureBCWithAxialStretch):
  tempName = 'combinedRadialAxialStretch'
else:
  print('Warning! Unknown mechanical test scenario.')
  tempName = 'unknownTest'
    
# adapt folder name for compression test
if(flagCompressionTest):
  tempName += 'Compression'
  
# adapt folder name for combined axial + radial stretch test
if(applyPressureBCWithAxialStretch):
  tempName += '_lambda_z_'
  if(lambda_z==0.5):
    tempName += '50' # 0.5 acutally but cannot have dot in folder name
  elif(lambda_z==0.75):
    tempName += '75'
  elif(lambda_z==1.0):
    tempName += '100'
  elif(lambda_z==1.25):
    tempName += '125'
  elif(lambda_z==1.5):
    tempName += '150'
  elif(lambda_z==1.75):
    tempName += '175'
  elif(lambda_z==2.0):
    tempName += '200'
  else: # lambda_z==1.0
    tempName += '1'
  
resultsDir = tempName+'_zeta_'+str(int(zeta_))+'_c_'+str(int(current))+'_temp_'+str(int(tApplied))
os.system('mkdir '+resultsDir)

print('Results folder '+resultsDir+' created.')

# use of pickle for inputs to test
with open('inputdatafile.txt','wb') as data:
  pickle.dump(ftime, data)
  pickle.dump(nuSMP, data)
  pickle.dump(nuVac, data)
  pickle.dump(RintSMP, data)
  pickle.dump(RextSMP, data)
  pickle.dump(meshfile, data)
  pickle.dump(flagUseEMStress, data)
  pickle.dump(flagEMFieldDependentShearModulus, data)
  pickle.dump(thermalSource, data)
  pickle.dump(applyPressureBCForRadialStretch, data)
  pickle.dump(applyPressureBCWithAxialStretch, data)
  pickle.dump(Pmax, data)
  pickle.dump(tApplied, data)
  pickle.dump(current, data)
  pickle.dump(lambda_z, data)
  pickle.dump(H_SMP, data)
  pickle.dump(flagCompressionTest, data)
    
if sys.version_info[0] < 3:
  os.system('python Steinmann_MTM_Benchmarks.py')
else:
  os.system('python3 Steinmann_MTM_Benchmarks.py')
  
print('Copying results files to folder.')
# copy results files to directory
os.system('cp *csv disp_step* stress_step* inputdatafile.txt '+resultsDir+'/')
print('Done copying results files to folder.')
