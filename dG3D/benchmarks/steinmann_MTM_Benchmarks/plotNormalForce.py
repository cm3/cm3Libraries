import os
import pickle

import matplotlib
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
import numpy as npy
import openpyxl
import pandas as pd
from utils import *

testFolderName = 'combinedRadialAxialStretch_lambda_z_200_zeta_25_c_0_temp_293'
# testFolderName = 'pureRadialStretchCompression_zeta_25_c_0_temp_293'
PATH_RESULTS = './'+testFolderName+'/'
outputFileName = testFolderName+'_NormalPressure'+'.csv'

inputFile = PATH_RESULTS + 'inputdatafile.txt'
UxBefore = PATH_RESULTS + 'NodalDisplacementPhysical11120Num19comp0.csv'
timeStep_old, Ux_old = read_2ColumnCsv(UxBefore)
UyBefore = PATH_RESULTS + 'NodalDisplacementPhysical11120Num19comp1.csv'
timeStep_old, Uy_old = read_2ColumnCsv(UyBefore)
normalForceTopSurfaceBefore = PATH_RESULTS + 'force11111comp2.csv'
timeStep_old, normalForceTopSurface_z_old = read_2ColumnCsv(normalForceTopSurfaceBefore)

with open(inputFile, "rb") as data:
    ftime = pickle.load(data)
    nuSMP = pickle.load(data)
    nuVac = pickle.load(data)
    Rint_undeformed = pickle.load(data)
    RextSMP = pickle.load(data)
    meshfile = pickle.load(data)
    flagUseEMStress = pickle.load(data)
    flagEMFieldDependentShearModulus = pickle.load(data)
    thermalSource = pickle.load(data)
    applyPressureBCForRadialStretch = pickle.load(data)
    applyPressureBCForAxialStretch = pickle.load(data)
    Pmax = pickle.load(data)
    tApplied = pickle.load(data)
    current = pickle.load(data)
    lambda_z = pickle.load(data)
    H_SMP = pickle.load(data)
    flagCompressionTest = pickle.load(data)

# take entries only in mechanical load application stage
# excluding EM loading time steps
if(applyPressureBCForRadialStretch):
    timeStart = 0.1 # start of application of pressure for radial inflation/compression
elif(applyPressureBCForAxialStretch):
    timeStart = 0.2  # start of application of pressure for radial inflation/compression
else:
    timeStart = 0.0 # default
n_, = npy.where(npy.isclose(timeStep_old, timeStart)) # num of steps to exclude in which application of thermal, EM load (and axial stretch if considered)
timeStep = timeStep_old[n_[0]+1:] # new timeStep array excluding EM, thermal (and axial stretch) loading time steps
Ux = Ux_old[n_[0]+1:] # new Ux
Uy = Uy_old[n_[0]+1:] # new Uy
normalForceTopSurface_z = normalForceTopSurface_z_old[n_[0]+1:] # new normal force

# print('Ux = ',Ux)
# print('Uy = ',Uy)
# print('NormalForce = ',normalForceTopSurface_z)

# this is going to give +ve displacement increment (delta_U) always
# what about negative displacement increment (delta_U)?
radial_U = (Ux ** 2. + Uy ** 2.) ** 0.5
# print('radial_U = ',radial_U)

# Sign of Ux only due to Uy = 0. always, since point 11120 located along xaxis and no displacement in y direction
radial_Stretch = (npy.sign(Ux) * radial_U) / Rint_undeformed + 1.
# print('radial stretch = ',radial_Stretch)

# normal pressure = normal force/undeformed surface area
normalPressureTopSurface_z = normalForceTopSurface_z/(npy.pi * Rint_undeformed * Rint_undeformed)
# print(normalPressureTopSurface_z)

# convert array into dataframe
# writing columns with column head names
df = pd.DataFrame({'t': timeStep, 'radial_Stretch': radial_Stretch, 'normal_pressure': normalPressureTopSurface_z})

# save the dataframe as a csv file
# header = True if column header names to be written to csv
df.to_csv(outputFileName, sep=";", header=True, index=False)

# copy file to results directory
resultsDir = 'gholap_data'
if not os.path.exists(resultsDir):
    # if directory is not present
    # then create it
    os.makedirs(resultsDir)
os.system('cp '+outputFileName+' '+resultsDir+'/')

# plot data
matplotlib.rc('text', usetex=True)
matplotlib.rc('font', **{'family': 'serif', 'serif': ['Computer Modern Roman'],
                         'monospace': ['Computer Modern Typewriter']})

fig1 = plt.figure(figsize=(6, 6), layout='constrained')
plt.plot(radial_Stretch, normalPressureTopSurface_z / 1.e3, 'g', label='Gholap et al.', linewidth=1.)
ax1 = plt.gca()

ax1.set_xlabel(u"$\lambda_i$", fontsize=16)
ax1.set_ylabel(u"Normal pressure [kPa]", fontsize=16)
ax1.legend(loc='lower center', bbox_to_anchor=(0.5, -0.3), ncols=2, frameon=False, fontsize=11, labelspacing=0.1)

ratio = 1
xleft, xright = ax1.get_xlim()
ybottom, ytop = ax1.get_ylim()
# ax1.set_aspect(abs((xright-xleft)/(ybottom-ytop))*ratio)

#ax1.text(0.97, -10, "$\lambda_z = 1$", size=16)

ax1.tick_params(direction='in', labelsize=14, axis='both', which='major', pad=7)
# ax1.grid(linestyle='--', linewidth=.3)
ax1.tick_params(bottom=True, top=True, left=True, right=True)

ax1.margins(0.1)
ax1.xaxis.set_major_locator(ticker.MultipleLocator(0.5))
ax1.yaxis.set_major_locator(ticker.MultipleLocator(50))

# plt.tight_layout(pad=2.)
#fig1.savefig('normal_pressure_radial_stretch.png', bbox_inches='tight', dpi=1000)
plt.show()
