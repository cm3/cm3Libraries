import numpy as npy
import matplotlib
import matplotlib.pyplot as plt
import pandas as pd
import openpyxl
import matplotlib.ticker as ticker
import pickle
import os
from utils import *

# from scipy.optimize import curve_fit

mehnertDataFolderName = 'mehnert_steinmann_2017_data'
mehnertFileName = '8a.csv'
os.system('cp ' + mehnertDataFolderName + '/' + mehnertFileName + ' ' + os.getcwd())

# Read Mehnert and Steinmann 2017 data to plot
df1 = pd.read_csv(mehnertFileName)
# print(df1)

l1 = df1.iloc[:, 0]
p1 = df1.iloc[:, 1]
l2 = df1.iloc[:, 2]
p2 = df1.iloc[:, 3]

# ratio of external to internal radius of conducting material
zeta_ = [1.5, 2.0, 2.5]

# Read Gholap et al. data to plot
gholapDataFolderName = 'gholap_data'

# no EM fields (pure TM simulation)
gholapFileName1 = 'pureRadialStretch_zeta_15_c_0_temp_293_NormalPressure.csv'
os.system('cp ' + gholapDataFolderName + '/' + gholapFileName1 + ' ' + os.getcwd())
df1 = pd.read_csv(gholapFileName1,sep=';')
timeStep1 = df1.iloc[:, 0]
radial_Stretch1 = df1.iloc[:, 1]
pressure1 = df1.iloc[:, 2]

gholapFileName2 = 'pureRadialStretch_zeta_20_c_0_temp_293_NormalPressure.csv'
os.system('cp ' + gholapDataFolderName + '/' + gholapFileName2 + ' ' + os.getcwd())
df2 = pd.read_csv(gholapFileName2,sep=';')
timeStep2 = df2.iloc[:, 0]
radial_Stretch2 = df2.iloc[:, 1]
pressure2 = df2.iloc[:, 2]

gholapFileName3 = 'pureRadialStretch_zeta_25_c_0_temp_293_NormalPressure.csv'
os.system('cp ' + gholapDataFolderName + '/' + gholapFileName3 + ' ' + os.getcwd())
df3 = pd.read_csv(gholapFileName3,sep=';')
timeStep3 = df3.iloc[:, 0]
radial_Stretch3 = df3.iloc[:, 1]
pressure3 = df3.iloc[:, 2]

# with EM field-dependent shear modulus test results
gholapFileName4 = 'pureRadialStretch_zeta_15_c_6_temp_293_NormalPressure.csv'
os.system('cp ' + gholapDataFolderName + '/' + gholapFileName4 + ' ' + os.getcwd())
df4 = pd.read_csv(gholapFileName4,sep=';')
timeStep4 = df4.iloc[:, 0]
radial_Stretch4 = df4.iloc[:, 1]
pressure4 = df4.iloc[:, 2]

gholapFileName5 = 'pureRadialStretch_zeta_20_c_6_temp_293_NormalPressure.csv'
os.system('cp ' + gholapDataFolderName + '/' + gholapFileName5 + ' ' + os.getcwd())
df5 = pd.read_csv(gholapFileName5,sep=';')
timeStep5 = df5.iloc[:, 0]
radial_Stretch5 = df5.iloc[:, 1]
pressure5 = df5.iloc[:, 2]

gholapFileName6 = 'pureRadialStretch_zeta_25_c_6_temp_293_NormalPressure.csv'
os.system('cp ' + gholapDataFolderName + '/' + gholapFileName6 + ' ' + os.getcwd())
df6 = pd.read_csv(gholapFileName6,sep=';')
timeStep6 = df6.iloc[:, 0]
radial_Stretch6 = df6.iloc[:, 1]
pressure6 = df6.iloc[:, 2]

lambda_i = 2.00
#print(npy.isclose(radial_Stretch1, lambda_i, atol=0.01).any())
n1_, = npy.where(npy.isclose(radial_Stretch1, lambda_i, rtol=0.01))
#print(n1_)
if (radial_Stretch1[n1_[0]]>lambda_i):
    press1 = pressure1[n1_[0]]
else:
    press1 = pressure1[n1_[0]+1]
n2_, = npy.where(npy.isclose(radial_Stretch2, lambda_i, rtol=0.01))
if (radial_Stretch2[n2_[0]]>lambda_i):
    press2 = pressure2[n2_[0]]
else:
    press2 = pressure2[n2_[0]+1]
n3_, = npy.where(npy.isclose(radial_Stretch3, lambda_i, rtol=0.01))
if (radial_Stretch3[n3_[0]]>lambda_i):
    press3 = pressure3[n3_[0]]
else:
    press3 = pressure3[n3_[0]+1]
n4_, = npy.where(npy.isclose(radial_Stretch4, lambda_i, rtol=0.01))
if (radial_Stretch4[n4_[0]]>lambda_i):
    press4 = pressure4[n4_[0]]
else:
    press4 = pressure4[n4_[0]+1]
n5_, = npy.where(npy.isclose(radial_Stretch5, lambda_i, rtol=0.01))
if (radial_Stretch5[n5_[0]]>lambda_i):
    press5 = pressure5[n5_[0]]
else:
    press5 = pressure5[n5_[0]+1]
n6_, = npy.where(npy.isclose(radial_Stretch6, lambda_i, rtol=0.01))
if (radial_Stretch6[n6_[0]]>lambda_i):
    press6 = pressure6[n6_[0]]
else:
    press6 = pressure6[n6_[0]+1]

# plot data
matplotlib.rc('text', usetex=True)
matplotlib.rc('font', **{'family': 'serif', 'serif': ['Computer Modern Roman'],
                         'monospace': ['Computer Modern Typewriter']})

fig2 = plt.figure(figsize=(6, 6), layout="constrained")
plt.plot(l1, p1, marker='X',c='blue',linestyle='none',fillstyle='full', label='Mehnert [2017] $c=0$ A, $\Theta_e=293$ K', linewidth=1.)
plt.plot(l2, p2, marker='X',c='blue',linestyle='none',fillstyle='none', label='Mehnert [2017] $c=1$ A, $\Theta_e=293$ K', linewidth=1.)

# no EM fields (pure TM simulation)
# inflation test results data plot
plt.plot(zeta_[0],press1/1.e3,marker='X',c='red',linestyle='none',fillstyle='full',label='Gholap [2024] $c=0$ A, $\Theta_e=293$ K', linewidth=1.)
plt.plot(zeta_[1],press2/1.e3,marker='X',c='red',linestyle='none',fillstyle='full')
plt.plot(zeta_[2],press3/1.e3,marker='X',c='red',linestyle='none',fillstyle='full')

# with EM field-dependent shear modulus test results
# inflation test results data plot
plt.plot(zeta_[0],press4/1.e3,marker='X',c='red',linestyle='none',fillstyle='none',label='Gholap [2024] $c=1$ A, $\Theta_e=293$ K', linewidth=1.5)
plt.plot(zeta_[1],press5/1.e3,marker='X',c='red',linestyle='none',fillstyle='none')
plt.plot(zeta_[2],press6/1.e3,marker='X',c='red',linestyle='none',fillstyle='none')

ax2 = plt.gca()

# popt, pcov = curve_fit(fit_func_,l1,p1)
# ax2.plot(l1,fit_func_(l1,*popt),'r--',label='Curve fit',linewidth=2.)

ax2.set_xlabel(u"$\zeta$", fontsize=16)
ax2.set_ylabel(u"$\overline{N}$ [kPa]", fontsize=16)
ax2.legend(loc='lower center', bbox_to_anchor=(0.5, -0.25), ncols=2, frameon=False, fontsize=11, labelspacing=0.1)

ratio = 1
xleft, xright = ax2.get_xlim()
ybottom, ytop = ax2.get_ylim()
# ax2.set_aspect(abs((xright-xleft)/(ybottom-ytop))*ratio)

ax2.text(2.15, 75, "$\lambda_z = 1$, \n $\lambda_i = 2$", size=16)

ax2.tick_params(direction='in', labelsize=14, axis='both', which='major', pad=7)
# ax2.grid(linestyle='--', linewidth=.3)
ax2.tick_params(bottom=True, top=True, left=True, right=True)

ax2.margins(0.05)
# plt.xticks(npy.arange(0.5,3.1,0.5))
# plt.yticks(npy.arange(-200,101,50))
ax2.xaxis.set_major_locator(ticker.MultipleLocator(0.5))
ax2.yaxis.set_major_locator(ticker.MultipleLocator(20))

# plt.tight_layout(pad=0.)
fig2.savefig('8a_comparison.png', bbox_inches='tight', dpi=1000)
plt.show()
