Mesh.Optimize = 1;

cm = 1e-2; // Unit

pp  = "Input/10Geometric dimensions/0";

close_menu = 1;
colorpp = "Ivory";

// Current carrying wire
Rwire = 0.4*cm;
H = 0.2*cm;

Rhole = Rwire/2.0;

// Smp
RintSMP = 1*cm;
RextSMP = 1.5*cm;

// Air outside smp
RextAirExterior = RextSMP+1.0*cm; //2.5*cm;

Point(1) = {0, 0., H/2, 1.0};
//+
Point(2) = {0, Rhole, H/2, 1.0};
//+
Point(3) = {0, Rwire, H/2, 1.0};
//+
Point(4) = {0, RintSMP, H/2, 1.0};
//+
Point(5) = {0, RextSMP, H/2, 1.0};
//+
Point(6) = {0, RextAirExterior, H/2, 1.0};
//+
Line(1) = {1, 2};
//+
Line(2) = {2, 3};
//+
Line(3) = {3, 4};
//+
Line(4) = {4, 5};
//+
Line(5) = {5, 6};
//+
Extrude {{0, 0, 1}, {0, 0, 0}, Pi/2} {
  Curve{1};  Curve{2}; Curve{3}; Curve{4}; Curve{5};  
}
//+
Extrude {{0, 0, 1}, {0, 0, 0}, Pi/2} {
  Curve{6}; Curve{9}; Curve{13}; Curve{17}; Curve{21}; 
}
//+
Extrude {{0, 0, 1}, {0, 0, 0}, Pi/2} {
  Curve{25}; Curve{28}; Curve{32}; Curve{36}; Curve{40};
}
//+
Extrude {{0, 0, 1}, {0, 0, 0}, Pi/2} {
  Curve{44}; Curve{47}; Curve{51}; Curve{55}; Curve{59}; 
}

// cylindrical curves on wire top surface
Transfinite Curve {7,26,45,64,11,30,49,68} = 4 Using Progression 1;
// cylindrical curves on all top surface (except wire)
Transfinite Curve {15,34,53,72,19,38,57,76,23,42,61,80} = 6 Using Progression 1;
// line curves across thickness of wire center on top surface (possibly prisms in case of hexahedral meshing)
Transfinite Curve {1,6,25,44} = 3 Using Progression 1;
// line curves across thickness of wire cylinder outside center on top surface
Transfinite Curve {2,9,28,47} = 3 Using Progression 1;
// air inside across thickness on top surface
Transfinite Curve {3,13,32,51} = 3 Using Progression 1;
// smp across thickness on top surface
Transfinite Curve {4,17,36,55} = 3 Using Progression 1;
// air outside across thickness on top surface
Transfinite Curve {5,21,40,59} = 3 Using Progression 1;

// all top surfaces except wire center 
//Transfinite Surface {12,31,50,69,16,35,54,73,20,39,58,77,24,43,62,81}; 
//Recombine Surface {12,31,50,69,16,35,54,73,20,39,58,77,24,43,62,81}; 

// wire center top surfaces
//Recombine Surface {8,27,46,65};
//+
Extrude {0, 0, -H} {
  Surface{8}; Surface{27}; Surface{46}; Surface{65};
  Surface{12}; Surface{31}; Surface{50}; Surface{69};
  Surface{16}; Surface{35}; Surface{54}; Surface{73};
  Surface{20}; Surface{39}; Surface{58}; Surface{77};
  Surface{24}; Surface{43}; Surface{62}; Surface{81}; 
  Layers{3};
  //Recombine;
}
//+
Physical Surface("REFWIRETOP", 2333) = {8, 27, 46, 65,12,31,50,69};
//+
Physical Surface("REFWIREBOTTOM", 2444) = {98,115,132,149,171,193,215,237};
//+
Physical Surface("REFWIREOUT", 2666) = {162,184,206,228};
//+
Physical Volume("WIRE", 2000) = {1, 2, 3, 4,5,6,7,8};
Recursive Color Red {Volume{1, 2, 3, 4,5,6,7,8};}
//+
Physical Point("REFWIREPOINT", 2555) = {3};
//+
Physical Surface("REFAIRINTERIORTOP", 3333) = {16,35,54,73};
//+
Physical Surface("REFAIRINTERIORBOTTOM", 3444) = {259,281,303,325};
//+
Physical Volume("AIRINTERIOR", 3000) = {9,10,11,12};
//+
Physical Volume("ECORE", 1000) = {13,14,15,16};
Recursive Color Orange {Volume{13,14,15,16};}
//+
Physical Surface("REFCORETOP", 11111) = {20, 39, 58, 77};
//+
Physical Surface("REFCOREBOTTOM", 11115) = {347,369,391,413};
//+
Physical Surface("REFCOREOUT", 11113) = {338,360,382,404};
//+
Physical Surface("REFCOREIN", 11114) = {250,272,294,316};
//+
Physical Surface("SURFCORECUT_1", 11132) = {334};
//+
Physical Surface("SURFCORECUT_2", 11133) = {386};
//+
Physical Surface("SURFCORECUT_3", 11134) = {364};
//+
Physical Surface("SURFCORECUT_4", 11135) = {342};
//+
Physical Point("REFCOREPOINTTOPEXTERIOR_1", 11116) = {34};
//+
Physical Point("REFCOREPOINTTOPINTERIOR_1", 11120) = {32};
//+
Physical Point("REFCOREPOINTBOTTOMEXTERIOR_1", 11124) = {120};
//+
Physical Point("REFCOREPOINTBOTTOMINTERIOR_1", 11128) = {99};
//+
Physical Point("REFCOREPOINTTOPEXTERIOR_2", 11117) = {24};
//+
Physical Point("REFCOREPOINTTOPINTERIOR_2", 11121) = {22};
//+
Physical Point("REFCOREPOINTBOTTOMEXTERIOR_2", 11125) = {113};
//+
Physical Point("REFCOREPOINTBOTTOMINTERIOR_2", 11129) = {92};
//+
Physical Volume("AIREXTERIOR", 4000) = {17,18,19,20};
Recursive Color Brown {Volume{17,18,19,20};}
//+
Physical Surface("REFAIREXTERIORTOP", 4333) = {24,43,62,81};
//+
Physical Surface("REFAIREXTERIORBOTTOM", 4444) = {435,457,479,501};
//+
Physical Surface("REFAIREXTERIOROUT", 43333) = {426,448,470,492};
//+
Physical Point("REFAIREXTERIORPOINTTOP", 4555) = {36};
//+
Physical Point("REFAIREXTERIORPOINTBOTTOM", 4666) = {141};
//+

// all volumes 
//Recombine Volume {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20};




