# test file

set(PYFILE idealHole.py)

set(FILES2DELETE 
	idealHole.msh 
  E_0_GP_0_*
)

add_cm3python_test(${PYFILE} "${FILES2DELETE}")
