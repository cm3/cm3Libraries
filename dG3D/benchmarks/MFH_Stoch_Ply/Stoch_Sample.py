#coding-Utf-8-*-
import numpy as npy
import scipy as spy
from gmshpy import *
from dG3Dpy import*
import sys
import pickle


#h_x,h_y,h_z : sides of the volume element
#Nfiber : number of fibers
#BC : str defining the boundary conditions : 'PERIODIC'
#material : list containing the material properties
# meshfile : name of mesh file


##### with interpolyted materiasl properties 
# material law
rho   = 1400.
# solver
sol = 2           # Gmm=0 (default) Taucs=1 PETsc=2
soltype = 1       # StaticLinear=0 (default) StaticNonLinear=1
nstep = 10         # number of step (used only if soltype=1)
ftime = 1.0         # Final time (used only if soltype=1)
tol=1.e-4         # relative tolerance for NR scheme (used only if soltype=1)
nstepArch=1       # Number of step between 2 archiving (used only if soltype=1)
fulldg = 0
beta1 = 30.
eqRatio = 1.e6
space1 = 0       # function space (Lagrange=0)

# creation of Solver
meshfile = 'Sample.msh'
geofile = 'Sample.geo'
mysolver = nonLinearMechSolver(1000)
mysolver.createModel(geofile,meshfile,3,1)
#mysolver.loadModel(meshfile)

lawnum = 1
myfield = 1011

PropMatfile1 = 'FE_MATprop.csv'   # for two level computational homogenization
PropMatfile2 = 'MF_MATprop.csv'   # For stochastic MFH
properties = "property.i01"

#mylaw = AnisotropicStochDG3DMaterialLaw(lawnum,rho,0.0,0.0,0.0,-12.5e-6,-12.5e-6,PropMatfile1, 3)

mylaw = NonLocalDamageDG3DMaterialLaw_Stoch(lawnum,rho,properties,-12.5e-6,-12.5e-6, 0.0, 1000.0e-6, 250.e-6, 0.0, PropMatfile2, 3) 
mylaw.setUseBarF(True)

mysolver.addMaterialLaw(mylaw)
#mydom = dG3DDomain(10,myfield,space1,lawnum,fulldg)
mydom = dG3DDomain(10,myfield,space1,lawnum,fulldg,3)
mydom.setNonLocalStabilityParameters(beta1,True)
mydom.setNonLocalEqRatio(eqRatio)
mydom.stabilityParameters(30.)
mysolver.addDomain(mydom)

mysolver.Scheme(soltype)
mysolver.Solver(sol)
mysolver.snlData(nstep,ftime,tol,1.e-4)  
mysolver.stepBetweenArchiving(nstepArch)


# BC
#shearing
mysolver.displacementBC("Face",120,2,0.)
mysolver.displacementBC("Face",121,2,0.)
mysolver.displacementBC("Face",110,1,0.)
mysolver.displacementBC("Face",100,0,0.)
mysolver.displacementBC("Face",101,0,0.00006)


# needed resultes
mysolver.internalPointBuildView("svm",IPField.SVM, 1, 1)
mysolver.internalPointBuildView("sig_xx",IPField.SIG_XX, 1, 1)
mysolver.internalPointBuildView("sig_yy",IPField.SIG_YY, 1, 1)
mysolver.internalPointBuildView("sig_zz",IPField.SIG_ZZ, 1, 1)
mysolver.internalPointBuildView("sig_xy",IPField.SIG_XY, 1, 1)
mysolver.internalPointBuildView("sig_yz",IPField.SIG_YZ, 1, 1)
mysolver.internalPointBuildView("sig_xz",IPField.SIG_XZ, 1, 1)
mysolver.internalPointBuildView("epl",IPField.PLASTICSTRAIN, 1, 1)
mysolver.internalPointBuildView("damage",IPField.DAMAGE, 1, 1)
mysolver.archivingForceOnPhysicalGroup("Face", 101, 0, nstepArch)
mysolver.archivingIPOnPhysicalGroup("Volume",myfield, IPField.SIG_XX,IPField.MEAN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",myfield, IPField.SIG_XX,IPField.MAX_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",myfield, IPField.SIG_XX,IPField.MIN_VALUE, nstepArch);

mysolver.solve()



check = TestCheck()
check.equal(1.353881e-01,mysolver.getArchivedForceOnPhysicalGroup("Face", 101, 0),1.e-5)

try:
  import linecache
  linesmax = linecache.getline('IPVolume1011val_SIG_XXMax.csv',10)
  linesmin = linecache.getline('IPVolume1011val_SIG_XXMin.csv',10)
  linesmean = linecache.getline('IPVolume1011val_SIG_XXMean.csv',10)
except:
  print('Cannot read the results in the files')
  import os
  os._exit(1)
else:
  check.equal(6.886981e+08,float(linesmax.split(';')[1]))
  check.equal(4.029402e+08,float(linesmin.split(';')[1]))
  check.equal(5.415527e+08,float(linesmean.split(';')[1]))




