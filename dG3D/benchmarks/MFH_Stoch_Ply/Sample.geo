//Fiber reinforced composite cross-section
Geometry.AutoCoherence= 1.0e-9;
//defination of unit
unit = 1.0e-06;
//characteristic mesh size
n_x = 1;
n_y = 1;
cl = 11.0*unit;
lays = 1;

h_x = 1000.000000 * unit;
h_y = 250.000000 * unit;
h_z = 1.00000 * unit;
dx = h_x/n_x;
dy = h_y/n_y;

//point
For i In {1:n_x+1}
For j In {1:n_y+1}

Point(i+(j-1)*(n_x+1)) = {(i-1)*dx , (j-1)*dy , 0.0, cl};

EndFor
EndFor

//line
For i In {1:n_x}
For j In {1:n_y+1}

Line(i+(j-1)*(n_x)) = {i+(j-1)*(n_x+1),i+(j-1)*(n_x+1)+1};

EndFor
EndFor

For i In {1:n_x+1}
For j In {1:n_y}

Line((n_y+1)*(n_x)+i+(j-1)*(n_x+1)) = {i+(j-1)*(n_x+1),i+j*(n_x+1)};

EndFor
EndFor

//line loop //surface

For i In {1:n_x}
For j In {1:n_y}

Line Loop(i+(j-1)*n_x) = {i+(j-1)*(n_x),(n_y+1)*(n_x)+(j-1)*(n_x+1)+i+1,-(i+j*(n_x)),-((n_y+1)*(n_x)+(j-1)*(n_x+1)+i)};
Plane Surface(i+(j-1)*n_x) = {i+(j-1)*n_x};

EndFor
EndFor

t=0;
For i In {1:n_x}
t = t+1;
edg21[t] = i;
edg22[t] = i+n_x*n_y;
EndFor

t=0;
For i In {1:n_y}
t = t+1;
edg11[t] = n_x*(n_y+1)+(i-1)*(n_x+1)+1;
edg12[t] = n_x*(n_y+2)+(i-1)*(n_x+1)+1;
EndFor

outX0[] =  Extrude{ 0, 0, h_z}{ Line{edg11[]};Layers{lays};};
outX1[] =  Extrude{ 0, 0, h_z}{ Line{edg12[]};Layers{lays};};
outY0[] =  Extrude{ 0, 0, h_z}{ Line{edg21[]};Layers{lays};};
outY1[] =  Extrude{ 0, 0, h_z}{ Line{edg22[]};Layers{lays};};

Physical Surface(100) = { outX0[] };
Physical Surface(101) = { outX1[] };
Physical Surface(110) = { outY0[] };
Physical Surface(111) = { outY1[] };

my_Volume[] = Extrude {0.0 , 0.0 , h_z} {Surface{1}; Layers{lays}; Recombine;};
Physical Surface(120) = {1};
Constrain_z[] = Translate{0, 0, h_z}{ Duplicata{ Surface{1}; } };
Physical Surface(121) = { Constrain_z[]};

For i In {1:n_x}
For j In {1:n_y}
Physical Volume( 1000+j*10+i ) ={ i+(j-1)*n_x };
EndFor
EndFor

Coherence;
