import pandas as pd
import numpy as np
import sys
import pickle
from gmshpy import *
#from dG3DpyDebug import*
from dG3Dpy import*
#from input import *


lnum1 = 11
rho = 1e-9


with open('Bounds.dat', 'rb') as data:
    XGmax = pickle.load(data, encoding="latin1")
    YSmax = pickle.load(data, encoding="latin1")
    XGmin = pickle.load(data, encoding="latin1")
    YSmin = pickle.load(data, encoding="latin1")
      
# ----- Inputs ----------#
VolumFracmean = (XGmax[0] + XGmin[0]) / 2.0
VolumFracstd = (XGmax[0] - XGmin[0]) / 2.0
#
Radiusmean = (XGmax[1] + XGmin[1]) / 2.0
Radiusstd = (XGmax[1] - XGmin[1]) / 2.0
#
CellSizemean = (XGmax[2] + XGmin[2]) / 2.0
CellSizestd = (XGmax[2] - XGmin[2]) / 2.0

TSTEPmean = (XGmax[3] + XGmin[3]) / 2.0
TSTEPstd = (XGmax[3] - XGmin[3]) / 2.0

EXXmean = (XGmax[4] + XGmin[4]) / 2.0
EXXstd = (XGmax[4] - XGmin[4]) / 2.0

EYYmean = (XGmax[5] + XGmin[5]) / 2.0
EYYstd = (XGmax[5] - XGmin[5]) / 2.0

EZZmean = (XGmax[6] + XGmin[6]) / 2.0
EZZstd = (XGmax[6] - XGmin[6]) / 2.0

EXYmean = (XGmax[7] + XGmin[7]) / 2.0
EXYstd = (XGmax[7] - XGmin[7]) / 2.0

EZXmean = (XGmax[8] + XGmin[8]) / 2.0
EZXstd = (XGmax[8] - XGmin[8]) / 2.0

EYZmean = (XGmax[9] + XGmin[9]) / 2.0
EYZstd = (XGmax[9] - XGmin[9]) / 2.0

# ----- Outputs ----------#
SXXmean = (YSmax[0] + YSmin[0]) / 2.0
SXXstd = (YSmax[0] - YSmin[0]) / 2.0

SYYmean = (YSmax[1]+YSmin[1]) / 2.0
SYYstd = (YSmax[1]-YSmin[1]) / 2.0

SZZmean = (YSmax[2]+YSmin[2]) / 2.0
SZZstd = (YSmax[2]-YSmin[2]) / 2.0

SXYmean = (YSmax[3]+YSmin[3]) / 2.0
SXYstd = (YSmax[3]-YSmin[3]) / 2.0

SZXmean = (YSmax[4]+YSmin[4]) / 2.0
SZXstd = (YSmax[4]-YSmin[4]) / 2.0

SYZmean = (YSmax[5]+YSmin[5]) / 2.0
SYZstd = (YSmax[5]-YSmin[5]) / 2.0


# Set total no of inputs param for RNN (6 EGL + 3 Extra: radius, cellsize, time)
numberOfInput = 9
numInternalVars = 75

surrogate = "ModelGRU_GS_0f1_50_f2_75_model.pt"

macromat1 = torchANNBasedDG3DMaterialLaw(lnum1, rho, numberOfInput, numInternalVars, surrogate,
                EXXmean, EXXstd, EXYmean, EXYstd, EYYmean, EYYstd,
                EYZmean, EYZstd, EZZmean, EZZstd, EZXmean, EZXstd,
                SXXmean, SXXstd, SXYmean, SXYstd, SYYmean, SYYstd,
                SYZmean, SYZstd, SZZmean, SZZstd, SZXmean, SZXstd,
                True, 1e-5)

# Set type of Kinematic input to EGL (Non Incremental)
macromat1.setKinematicInput(1)
# Set no of additional inputs
macromat1.setNumExtraInput(3)
# Set the location of time in the extra array
macromat1.setTimeArg(2)

# Set initial vcalues for extra inputs in order Radius, cellsize, time
macromat1.setInitialExtraInput(0.6)
macromat1.setInitialExtraInput(10.0)
macromat1.setInitialExtraInput(0.0)

# Set normalization values for extra inputs in order Radius, cellsize, time
macromat1.setNormExtraInp(Radiusmean, Radiusstd)
macromat1.setNormExtraInp(CellSizemean, CellSizestd)
macromat1.setNormExtraInp(TSTEPmean, TSTEPstd)

# Set macro geometry and mesh
macromeshfile="rubics.msh" # name of mesh file
macrogeofile="rubics.geo"

# solver
sol = 2  # Gmm=0 (default) Taucs=1 PETsc=2
soltype =1 # StaticLinear=0 (default) StaticNonLinear=1
nstep = 100  # number of step (used only if soltype=1)
ftime = 1  # Final time (used only if soltype=1)
tol=1.e-5  # relative tolerance for NR scheme (used only if soltype=1)
nstepArch=5 # Number of step between 2 archiving (used only if soltype=1)


# creation of  macro part Domain
beta1 = 1e2
fullDG = False

averageStrainBased = True

# non DG domain
nfield1 = 11
macrodomain1 = dG3DDomain(10,nfield1,0,lnum1,fullDG,3)
macrodomain1.stabilityParameters(beta1)

#macrodomain1.matrixByPerturbation(1,1,1,1e-8)

# creation of Solver
mysolver = nonLinearMechSolver(10)
mysolver.loadModel(macromeshfile)
mysolver.addDomain(macrodomain1)
mysolver.addMaterialLaw(macromat1)
mysolver.Scheme(soltype)
mysolver.Solver(sol)
mysolver.snlData(nstep,ftime,tol)
mysolver.stepBetweenArchiving(nstepArch)

# boundary condition
mysolver.displacementBC("Face",551,1,0.0)
mysolver.displacementBC("Face",554,0,0.0)
mysolver.displacementBC("Face",555,2,0.0)
mysolver.displacementBC("Face",553,1,-5.0)


# archivage
mysolver.internalPointBuildView("Green-Lagrange_xx",IPField.GL_XX, 1, nstepArch);
mysolver.internalPointBuildView("Green-Lagrange_yy",IPField.GL_YY, 1, nstepArch);
mysolver.internalPointBuildView("Green-Lagrange_zz",IPField.GL_ZZ, 1, nstepArch);
mysolver.internalPointBuildView("Green-Lagrange_xy",IPField.GL_XY, 1, nstepArch);
mysolver.internalPointBuildView("Green-Lagrange_yz",IPField.GL_YZ, 1, nstepArch);
mysolver.internalPointBuildView("Green-Lagrange_xz",IPField.GL_XZ, 1, nstepArch);
mysolver.internalPointBuildView("sig_xx",IPField.SIG_XX, 1, nstepArch);
mysolver.internalPointBuildView("sig_yy",IPField.SIG_YY, 1, nstepArch);
mysolver.internalPointBuildView("sig_zz",IPField.SIG_ZZ, 1, nstepArch);
mysolver.internalPointBuildView("sig_xy",IPField.SIG_XY, 1, nstepArch);
mysolver.internalPointBuildView("sig_yz",IPField.SIG_YZ, 1, nstepArch);
mysolver.internalPointBuildView("sig_xz",IPField.SIG_XZ, 1, nstepArch);
mysolver.internalPointBuildView("sig_VM",IPField.SVM, 1, 1);
#mysolver.internalPointBuildView("Green-Lagrange equivalent strain",IPField.GL_EQUIVALENT_STRAIN, 1, 1);
#mysolver.internalPointBuildView("Equivalent plastic strain",IPField.PLASTICSTRAIN, 1, 1);

mysolver.archivingForceOnPhysicalGroup("Face",551, 1)
mysolver.archivingNodeDisplacementOnPhysical(2, 553, 1, 1)

# solve
mysolver.solve()

check = TestCheck()
check.equal(70.44289357841578,mysolver.getArchivedForceOnPhysicalGroup("Face", 551, 1),1.e-4)

