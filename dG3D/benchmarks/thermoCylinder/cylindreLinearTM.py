#coding-Utf-8-*-
from gmshpy import *
#from dG3DpyDebug import*
from dG3Dpy import*
import math
	
t0=273.+23.

# material laws

#Tisse
rhoCFRP = 1400. # kg/m^3 # valeur mesuree par moi-meme
cvCFRP  =1.
cpCFRP  =0.*1000.*rhoCFRP 

ExCFRP_ant = 75520.e6 #Pa # Homog GMSH
EyCFRP_ant = 75520.e6 #Pa # Homog GMSH
EzCFRP_ant = 12.e9 #Pa # Homog GMSH

VxyCFRP_ant = 0.055924 # homog cours composite
VxzCFRP_ant = 0.41474 # homog cours composite
VyzCFRP_ant = 0.41474 # homog cours composite

MUxyCFRP_ant = 4828.1e6 #Pa homog cours composite
MUxzCFRP_ant = 4731.7e6 #Pa homog cours composite
MUyzCFRP_ant = 4731.7e6 #Pa homog cours composite

KxCFRP_ant = 3.5 #W/m.K # Homog GMSH
KyCFRP_ant = 3.5 #W/m.K # Homog GMSH
KzCFRP_ant = 0.44 #W/m.K # Homog GMSH

alphaxCFRP_ant = 3.e-6 # Homog GMSH
alphayCFRP_ant = 3.e-6 # Homog GMSH
alphazCFRP_ant = 40.e-6 # Homog GMSH


ExCFRP = EzCFRP_ant
EyCFRP = ExCFRP_ant
EzCFRP = EyCFRP_ant

VxyCFRP = VxzCFRP_ant 
VxzCFRP = VyzCFRP_ant
VyzCFRP = VxyCFRP_ant

MUxyCFRP = MUxzCFRP_ant
MUxzCFRP = MUyzCFRP_ant 
MUyzCFRP = MUxyCFRP_ant

KxCFRP = KzCFRP_ant
KyCFRP = KxCFRP_ant
KzCFRP = KyCFRP_ant

alphaxCFRP = alphazCFRP_ant
alphayCFRP = alphaxCFRP_ant
alphazCFRP = alphayCFRP_ant
 

#Extra couche
#rhoEXTRA   = . # kg/m^3
#ExEXTRA=EyEXTRA= .*.e9 #Pa
#EzEXTRA =.*.e9 #Pa
#VxyEXTRA=
#VxzEXTRA=
#VyzEXTRA=
#MUxyEXTRA= ExEXTRA/(2*(1+VxyEXTRA)) #Pa
#MUxzEXTRA= EyEXTRA/(2*(1+VxzEXTRA)) #Pa
#MUyzCFRP= EzEXTRA/(2*(1+VyzEXTRA)) #Pa
#cvEXTRA=1.
#KxEXTRA=KyEXTRA= #W/m.K
#KzEXTRA= #W/m.K
#alphaxEXTRA=alphayEXTRA= 
#alphazEXTRA=
#cpEXTRA=0. 

# geometry
meshfile="cylindreLinearTM.msh" # name of mesh file
geofile ="cylindreLinearTM.geo"

# solver
sol = 2     # Gmm=0 (default) Taucs=1 PETsc=2
soltype = 1 # StaticLinear=0 (defashult) StaticNonLinear=1
nstep = 140  # number of step (used only if soltype=1)
ftime =7.  # Final time (used only if soltype=1)
tol=1.e-5   # relative tolerance for NR scheme (used only if soltype=1)
nstepArch=1 # Number of step between 2 archiving (used only if soltype=1)
fullDg = 1  #O = CG, 1 = DG
space1 = 0  # function space (Lagrange=0)
beta1  = 100
eqRatio = 1.e6 #1.e8

# compute solution and BC (given directly to the solver)
# creation of laws
phi_eul_degree 		= [ [j for j in range(6)] for i in range (6)]
theta_eul_degree 	= [ [j for j in range(6)] for i in range (6)]
psi_eul_degree 		= [ [j for j in range(6)] for i in range (6)]
lawnum		= [ [j for j in range(6)] for i in range (6)]
law		= [ [j for j in range(6)] for i in range (6)]    
alpha 		= [ [j for j in range(6)] for i in range (6)]
beta 		= [ [j for j in range(6)] for i in range (6)]
gamma 		= [ [j for j in range(6)] for i in range (6)]
numfield	= [ [j for j in range(6)] for i in range (6)]
field 		= [ [j for j in range(6)] for i in range (6)]
FEinterfieldx 	= [ [j for j in range(6)] for i in range (6)]
FEinterfieldz 	= [ [j for j in range(6)] for i in range (6)]
                                                     
for i in range(0,6): # pour i allant de 0 jusque 5
    for j in range(0,6): # pour j allant de 0 jusque 5
        # pour une couche d'isolant ou conducteur	
        lawnum[i][j]	= (i+1)*100+(j+1)
        alpha[i][j] 	= 7.5+i*15
        beta[i][0] 	= float(0.0)
        beta[i][1] 	= float(45.0)
        beta[i][2] 	= float(0.0)
        beta[i][3] 	= float(0.0)
        beta[i][4] 	= float(45.0)
        beta[i][5] 	= float(0.0)
        gamma[i][j] 	= float(0.0)

for i in range(0,6): # pour i allant de 0 jusque 5
    for j in range(0,6): # pour j allant de 0 jusque 5
	#if j!=2:
	#law[i][j]   = LinearThermoMechanicsDG3DMaterialLaw(lawnum[i][j],rhoEXTRA,ExEXTRA,EyEXTRA,EzEXTRA,VxyEXTRA,VxzEXTRA,VyzEXTRA,MUxyEXTRA,MUxzEXTRA,MUyzEXTRA,alpha[i][j],beta[i][j],gamma[i][j],t0,KxEXTRA,KyEXTRA,KzEXTRA,alphaxEXTRA,alphayEXTRA,alphazEXTRA,cpEXTRA)
	#else:
        law[i][j]   = LinearThermoMechanicsDG3DMaterialLaw(lawnum[i][j],rhoCFRP,ExCFRP,EyCFRP,EzCFRP,VxyCFRP,VxzCFRP,VyzCFRP,MUxyCFRP,MUxzCFRP,MUyzCFRP,alpha[i][j],beta[i][j],gamma[i][j],t0,KxCFRP,KyCFRP,KzCFRP,alphaxCFRP,alphayCFRP,alphazCFRP,cpCFRP)
	
# creation of ElasticField
for i in range(0,6): # pour i allant de 0 jusque 5
    for j in range(0,6): # pour j allant de 0 jusque 5
        numfield[i][j] = (i+1)*100+(j+1) # number of the field 
        field[i][j] = ThermoMechanicsDG3DDomain(1000,numfield[i][j],space1,lawnum[i][j],fullDg,1.e6)
        field[i][j].matrixByPerturbation(0,0,0,1.e-8)
        field[i][j].stabilityParameters(beta1)
        field[i][j].ThermoMechanicsStabilityParameters(beta1,bool(1))

#interface
for i in range(0,5): # pour i allant de 0 jusque 4
    for j in range(0,6): # pour j allant de 0 jusque 5
        FEinterfieldx[i][j]=ThermoMechanicsInterDomainBetween3D(1000,field[i][j],field[i+1][j],eqRatio,0) 
        FEinterfieldx[i][j].stabilityParameters(beta1)
        FEinterfieldx[i][j].ThermoMechanicsStabilityParameters(beta1,bool(1))
        FEinterfieldx[i][j].matrixByPerturbation(0,1.e-8)

for i in range(0,6): # pour i allant de 0 jusque 5
    for j in range(0,5): # pour j allant de 0 jusque 4
        FEinterfieldz[i][j]=ThermoMechanicsInterDomainBetween3D(1000,field[i][j],field[i][j+1],eqRatio,0) 
        FEinterfieldz[i][j].stabilityParameters(beta1)
        FEinterfieldz[i][j].ThermoMechanicsStabilityParameters(beta1,bool(1))
        FEinterfieldz[i][j].matrixByPerturbation(0,1.e-8)

# creation of Solver
mysolver = nonLinearMechSolver(1000)
mysolver.createModel(geofile,meshfile,3,1)
#mysolver.loadModel(meshfile)

for i in range(0,6): # pour i allant de 0 jusque 5
    for j in range(0,6): # pour j allant de 0 jusque 5
        mysolver.addDomain(field[i][j])

for i in range(0,5): # pour i allant de 0 jusque 4
    for j in range(0,6): # pour j allant de 0 jusque 5
        mysolver.addDomain(FEinterfieldx[i][j])

for i in range(0,6): # pour i allant de 0 jusque 5
    for j in range(0,5): # pour j allant de 0 jusque 4
        mysolver.addDomain(FEinterfieldz[i][j])

for i in range(0,6): # pour i allant de 0 jusque 5
    for j in range(0,6): # pour j allant de 0 jusque 5
        mysolver.addMaterialLaw(law[i][j])

mysolver.Scheme(soltype)
mysolver.Solver(sol)
mysolver.snlData(nstep,ftime,tol)
#mysolver.snlData(nstep,ftime,tol,1e-3)
mysolver.stepBetweenArchiving(nstepArch)

# BC
#mechanical BC
cyclicFunctionDisp1=cycleFunctionTime(0., 0.0, ftime, 0.0);
mysolver.displacementBC("Face",1002,1,cyclicFunctionDisp1)
mysolver.displacementBC("Face",1003,0,cyclicFunctionDisp1)
mysolver.displacementBC("Face",1005,2,cyclicFunctionDisp1)
mysolver.displacementBC("Face",1006,2,cyclicFunctionDisp1)

cyclicFunctionPressure1=cycleFunctionTime(0., 0., 0.85, -30.e5, 0.90, -20.e5, 1., -15.e5, 1.5, -7.5e5, ftime, -7.5e5);
mysolver.pressureOnPhysicalGroupBC("Face",1001,cyclicFunctionPressure1)
#mysolver.pressureOnPhysicalGroupBC("Face",1001,cyclicFunctionPressure1)

#thermal BC
for i in range(0,6): # pour i allant de 0 jusque 5
    for j in range(0,6):
        mysolver.initialBC("Volume","Position",numfield[i][j],3,t0)

cyclicFunctionTemp1=cycleFunctionTime(0., t0, 0.85, 475.+273., 1.45, 350.+273., 2.55, 110.+273., 6.55, 50.+273., ftime, 50.+273.);
mysolver.displacementBC("Face",1001,3,cyclicFunctionTemp1)

#Fonction de temperature imaginee pour dtm Q sur la face sup
#cyclicFunctionTemp2=cycleFunctionTime(0., t0, 0.95, 325+273, 1.55, 175+273, 2.55, 75+273, 6.55, 40+273, ftime, 40+273);
#mysolver.displacementBC("Face",1004,3,cyclicFunctionTemp2)

A=6.*(2.*0.186*math.sin(math.radians(7.5)))*0.4
cyclicFunctionTemp2=cycleFunctionTime(0., 0.e6, 0.95, (40.*(218.4+273.-t0)), 1.55, (40.*(161.35+273.-t0)), 2.55, (40.*(62.45+273.-t0)), 6.55, (30.*(37.15+273.-t0)), ftime, (30.*(37.15+273.-t0))); 
mysolver.forceBC("Face",1004,3,cyclicFunctionTemp2)

mysolver.internalPointBuildView("svm",IPField.SVM, 1, 1)
mysolver.internalPointBuildView("sig_xx",IPField.SIG_XX, 1, 1)
mysolver.internalPointBuildView("sig_yy",IPField.SIG_YY, 1, 1)
mysolver.internalPointBuildView("sig_zz",IPField.SIG_ZZ, 1, 1)
mysolver.internalPointBuildView("sig_xy",IPField.SIG_XY, 1, 1)
mysolver.internalPointBuildView("sig_yz",IPField.SIG_YZ, 1, 1)
mysolver.internalPointBuildView("sig_xz",IPField.SIG_XZ, 1, 1)
mysolver.internalPointBuildView("temperature",IPField.TEMPERATURE, 1, 1)
mysolver.internalPointBuildView("qx",IPField.THERMALFLUX_X, 1, 1)
mysolver.internalPointBuildView("qy",IPField.THERMALFLUX_Y, 1, 1)
mysolver.internalPointBuildView("qz",IPField.THERMALFLUX_Z, 1, 1)

#Thermal
mysolver.archivingNodeDisplacement(107,3,1)
mysolver.archivingNodeDisplacement(207,3,1)
mysolver.archivingNodeDisplacement(307,3,1)
mysolver.archivingNodeDisplacement(407,3,1)
mysolver.archivingNodeDisplacement(507,3,1)
mysolver.archivingNodeDisplacement(607,3,1)
mysolver.archivingNodeDisplacement(707,3,1)

mysolver.archivingIPOnPhysicalGroup("Volume",101, IPField.TEMPERATURE,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",102, IPField.TEMPERATURE,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",103, IPField.TEMPERATURE,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",104, IPField.TEMPERATURE,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",105, IPField.TEMPERATURE,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",106, IPField.TEMPERATURE,IPField.MEAN_VALUE,1);

mysolver.archivingIPOnPhysicalGroup("Volume",201, IPField.TEMPERATURE,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",202, IPField.TEMPERATURE,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",203, IPField.TEMPERATURE,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",204, IPField.TEMPERATURE,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",205, IPField.TEMPERATURE,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",206, IPField.TEMPERATURE,IPField.MEAN_VALUE,1);

mysolver.archivingIPOnPhysicalGroup("Volume",301, IPField.TEMPERATURE,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",302, IPField.TEMPERATURE,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",303, IPField.TEMPERATURE,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",304, IPField.TEMPERATURE,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",305, IPField.TEMPERATURE,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",306, IPField.TEMPERATURE,IPField.MEAN_VALUE,1);

mysolver.archivingIPOnPhysicalGroup("Volume",401, IPField.TEMPERATURE,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",402, IPField.TEMPERATURE,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",403, IPField.TEMPERATURE,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",404, IPField.TEMPERATURE,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",405, IPField.TEMPERATURE,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",406, IPField.TEMPERATURE,IPField.MEAN_VALUE,1);

mysolver.archivingIPOnPhysicalGroup("Volume",501, IPField.TEMPERATURE,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",502, IPField.TEMPERATURE,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",503, IPField.TEMPERATURE,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",504, IPField.TEMPERATURE,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",505, IPField.TEMPERATURE,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",506, IPField.TEMPERATURE,IPField.MEAN_VALUE,1);

mysolver.archivingIPOnPhysicalGroup("Volume",601, IPField.TEMPERATURE,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",602, IPField.TEMPERATURE,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",603, IPField.TEMPERATURE,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",604, IPField.TEMPERATURE,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",605, IPField.TEMPERATURE,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",606, IPField.TEMPERATURE,IPField.MEAN_VALUE,1);

#Mechanical

#sigma_xx
mysolver.archivingIPOnPhysicalGroup("Volume",101, IPField.SIG_XX,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",102, IPField.SIG_XX,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",103, IPField.SIG_XX,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",104, IPField.SIG_XX,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",105, IPField.SIG_XX,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",106, IPField.SIG_XX,IPField.MEAN_VALUE,1);

mysolver.archivingIPOnPhysicalGroup("Volume",201, IPField.SIG_XX,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",202, IPField.SIG_XX,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",203, IPField.SIG_XX,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",204, IPField.SIG_XX,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",205, IPField.SIG_XX,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",206, IPField.SIG_XX,IPField.MEAN_VALUE,1);

mysolver.archivingIPOnPhysicalGroup("Volume",301, IPField.SIG_XX,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",302, IPField.SIG_XX,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",303, IPField.SIG_XX,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",304, IPField.SIG_XX,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",305, IPField.SIG_XX,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",306, IPField.SIG_XX,IPField.MEAN_VALUE,1);

mysolver.archivingIPOnPhysicalGroup("Volume",401, IPField.SIG_XX,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",402, IPField.SIG_XX,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",403, IPField.SIG_XX,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",404, IPField.SIG_XX,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",405, IPField.SIG_XX,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",406, IPField.SIG_XX,IPField.MEAN_VALUE,1);

mysolver.archivingIPOnPhysicalGroup("Volume",501, IPField.SIG_XX,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",502, IPField.SIG_XX,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",503, IPField.SIG_XX,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",504, IPField.SIG_XX,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",505, IPField.SIG_XX,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",506, IPField.SIG_XX,IPField.MEAN_VALUE,1);

mysolver.archivingIPOnPhysicalGroup("Volume",601, IPField.SIG_XX,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",602, IPField.SIG_XX,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",603, IPField.SIG_XX,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",604, IPField.SIG_XX,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",605, IPField.SIG_XX,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",606, IPField.SIG_XX,IPField.MEAN_VALUE,1);

#sigma_yy
mysolver.archivingIPOnPhysicalGroup("Volume",101, IPField.SIG_YY,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",102, IPField.SIG_YY,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",103, IPField.SIG_YY,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",104, IPField.SIG_YY,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",105, IPField.SIG_YY,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",106, IPField.SIG_YY,IPField.MEAN_VALUE,1);

mysolver.archivingIPOnPhysicalGroup("Volume",201, IPField.SIG_YY,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",202, IPField.SIG_YY,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",203, IPField.SIG_YY,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",204, IPField.SIG_YY,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",205, IPField.SIG_YY,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",206, IPField.SIG_YY,IPField.MEAN_VALUE,1);

mysolver.archivingIPOnPhysicalGroup("Volume",301, IPField.SIG_YY,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",302, IPField.SIG_YY,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",303, IPField.SIG_YY,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",304, IPField.SIG_YY,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",305, IPField.SIG_YY,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",306, IPField.SIG_YY,IPField.MEAN_VALUE,1);

mysolver.archivingIPOnPhysicalGroup("Volume",401, IPField.SIG_YY,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",402, IPField.SIG_YY,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",403, IPField.SIG_YY,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",404, IPField.SIG_YY,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",405, IPField.SIG_YY,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",406, IPField.SIG_YY,IPField.MEAN_VALUE,1);

mysolver.archivingIPOnPhysicalGroup("Volume",501, IPField.SIG_YY,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",502, IPField.SIG_YY,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",503, IPField.SIG_YY,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",504, IPField.SIG_YY,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",505, IPField.SIG_YY,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",506, IPField.SIG_YY,IPField.MEAN_VALUE,1);

mysolver.archivingIPOnPhysicalGroup("Volume",601, IPField.SIG_YY,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",602, IPField.SIG_YY,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",603, IPField.SIG_YY,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",604, IPField.SIG_YY,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",605, IPField.SIG_YY,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",606, IPField.SIG_YY,IPField.MEAN_VALUE,1);

#sigma_zz
mysolver.archivingIPOnPhysicalGroup("Volume",101, IPField.SIG_ZZ,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",102, IPField.SIG_ZZ,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",103, IPField.SIG_ZZ,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",104, IPField.SIG_ZZ,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",105, IPField.SIG_ZZ,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",106, IPField.SIG_ZZ,IPField.MEAN_VALUE,1);

mysolver.archivingIPOnPhysicalGroup("Volume",201, IPField.SIG_ZZ,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",202, IPField.SIG_ZZ,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",203, IPField.SIG_ZZ,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",204, IPField.SIG_ZZ,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",205, IPField.SIG_ZZ,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",206, IPField.SIG_ZZ,IPField.MEAN_VALUE,1);

mysolver.archivingIPOnPhysicalGroup("Volume",301, IPField.SIG_ZZ,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",302, IPField.SIG_ZZ,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",303, IPField.SIG_ZZ,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",304, IPField.SIG_ZZ,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",305, IPField.SIG_ZZ,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",306, IPField.SIG_ZZ,IPField.MEAN_VALUE,1);

mysolver.archivingIPOnPhysicalGroup("Volume",401, IPField.SIG_ZZ,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",402, IPField.SIG_ZZ,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",403, IPField.SIG_ZZ,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",404, IPField.SIG_ZZ,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",405, IPField.SIG_ZZ,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",406, IPField.SIG_ZZ,IPField.MEAN_VALUE,1);

mysolver.archivingIPOnPhysicalGroup("Volume",501, IPField.SIG_ZZ,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",502, IPField.SIG_ZZ,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",503, IPField.SIG_ZZ,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",504, IPField.SIG_ZZ,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",505, IPField.SIG_ZZ,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",506, IPField.SIG_ZZ,IPField.MEAN_VALUE,1);

mysolver.archivingIPOnPhysicalGroup("Volume",601, IPField.SIG_ZZ,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",602, IPField.SIG_ZZ,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",603, IPField.SIG_ZZ,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",604, IPField.SIG_ZZ,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",605, IPField.SIG_ZZ,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",606, IPField.SIG_ZZ,IPField.MEAN_VALUE,1);

#sigma_xy
mysolver.archivingIPOnPhysicalGroup("Volume",101, IPField.SIG_XY,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",102, IPField.SIG_XY,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",103, IPField.SIG_XY,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",104, IPField.SIG_XY,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",105, IPField.SIG_XY,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",106, IPField.SIG_XY,IPField.MEAN_VALUE,1);

mysolver.archivingIPOnPhysicalGroup("Volume",201, IPField.SIG_XY,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",202, IPField.SIG_XY,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",203, IPField.SIG_XY,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",204, IPField.SIG_XY,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",205, IPField.SIG_XY,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",206, IPField.SIG_XY,IPField.MEAN_VALUE,1);

mysolver.archivingIPOnPhysicalGroup("Volume",301, IPField.SIG_XY,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",302, IPField.SIG_XY,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",303, IPField.SIG_XY,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",304, IPField.SIG_XY,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",305, IPField.SIG_XY,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",306, IPField.SIG_XY,IPField.MEAN_VALUE,1);

mysolver.archivingIPOnPhysicalGroup("Volume",401, IPField.SIG_XY,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",402, IPField.SIG_XY,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",403, IPField.SIG_XY,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",404, IPField.SIG_XY,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",405, IPField.SIG_XY,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",406, IPField.SIG_XY,IPField.MEAN_VALUE,1);

mysolver.archivingIPOnPhysicalGroup("Volume",501, IPField.SIG_XY,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",502, IPField.SIG_XY,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",503, IPField.SIG_XY,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",504, IPField.SIG_XY,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",505, IPField.SIG_XY,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",506, IPField.SIG_XY,IPField.MEAN_VALUE,1);

mysolver.archivingIPOnPhysicalGroup("Volume",601, IPField.SIG_XY,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",602, IPField.SIG_XY,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",603, IPField.SIG_XY,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",604, IPField.SIG_XY,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",605, IPField.SIG_XY,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",606, IPField.SIG_XY,IPField.MEAN_VALUE,1);

#sigma_yz
mysolver.archivingIPOnPhysicalGroup("Volume",101, IPField.SIG_YZ,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",102, IPField.SIG_YZ,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",103, IPField.SIG_YZ,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",104, IPField.SIG_YZ,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",105, IPField.SIG_YZ,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",106, IPField.SIG_YZ,IPField.MEAN_VALUE,1);

mysolver.archivingIPOnPhysicalGroup("Volume",201, IPField.SIG_YZ,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",202, IPField.SIG_YZ,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",203, IPField.SIG_YZ,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",204, IPField.SIG_YZ,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",205, IPField.SIG_YZ,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",206, IPField.SIG_YZ,IPField.MEAN_VALUE,1);

mysolver.archivingIPOnPhysicalGroup("Volume",301, IPField.SIG_YZ,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",302, IPField.SIG_YZ,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",303, IPField.SIG_YZ,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",304, IPField.SIG_YZ,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",305, IPField.SIG_YZ,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",306, IPField.SIG_YZ,IPField.MEAN_VALUE,1);

mysolver.archivingIPOnPhysicalGroup("Volume",401, IPField.SIG_YZ,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",402, IPField.SIG_YZ,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",403, IPField.SIG_YZ,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",404, IPField.SIG_YZ,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",405, IPField.SIG_YZ,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",406, IPField.SIG_YZ,IPField.MEAN_VALUE,1);

mysolver.archivingIPOnPhysicalGroup("Volume",501, IPField.SIG_YZ,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",502, IPField.SIG_YZ,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",503, IPField.SIG_YZ,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",504, IPField.SIG_YZ,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",505, IPField.SIG_YZ,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",506, IPField.SIG_YZ,IPField.MEAN_VALUE,1);

mysolver.archivingIPOnPhysicalGroup("Volume",601, IPField.SIG_YZ,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",602, IPField.SIG_YZ,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",603, IPField.SIG_YZ,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",604, IPField.SIG_YZ,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",605, IPField.SIG_YZ,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",606, IPField.SIG_YZ,IPField.MEAN_VALUE,1);

#sigma_xz
mysolver.archivingIPOnPhysicalGroup("Volume",101, IPField.SIG_XZ,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",102, IPField.SIG_XZ,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",103, IPField.SIG_XZ,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",104, IPField.SIG_XZ,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",105, IPField.SIG_XZ,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",106, IPField.SIG_XZ,IPField.MEAN_VALUE,1);

mysolver.archivingIPOnPhysicalGroup("Volume",201, IPField.SIG_XZ,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",202, IPField.SIG_XZ,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",203, IPField.SIG_XZ,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",204, IPField.SIG_XZ,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",205, IPField.SIG_XZ,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",206, IPField.SIG_XZ,IPField.MEAN_VALUE,1);

mysolver.archivingIPOnPhysicalGroup("Volume",301, IPField.SIG_XZ,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",302, IPField.SIG_XZ,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",303, IPField.SIG_XZ,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",304, IPField.SIG_XZ,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",305, IPField.SIG_XZ,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",306, IPField.SIG_XZ,IPField.MEAN_VALUE,1);

mysolver.archivingIPOnPhysicalGroup("Volume",401, IPField.SIG_XZ,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",402, IPField.SIG_XZ,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",403, IPField.SIG_XZ,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",404, IPField.SIG_XZ,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",405, IPField.SIG_XZ,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",406, IPField.SIG_XZ,IPField.MEAN_VALUE,1);

mysolver.archivingIPOnPhysicalGroup("Volume",501, IPField.SIG_XZ,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",502, IPField.SIG_XZ,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",503, IPField.SIG_XZ,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",504, IPField.SIG_XZ,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",505, IPField.SIG_XZ,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",506, IPField.SIG_XZ,IPField.MEAN_VALUE,1);

mysolver.archivingIPOnPhysicalGroup("Volume",601, IPField.SIG_XZ,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",602, IPField.SIG_XZ,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",603, IPField.SIG_XZ,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",604, IPField.SIG_XZ,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",605, IPField.SIG_XZ,IPField.MEAN_VALUE,1);
mysolver.archivingIPOnPhysicalGroup("Volume",606, IPField.SIG_XZ,IPField.MEAN_VALUE,1);

mysolver.archivingForceOnPhysicalGroup("Face", 1002, 1)
mysolver.archivingForceOnPhysicalGroup("Face", 1001, 3)

mysolver.solve()

check = TestCheck()
check.equal(-4.095538e+07,mysolver.getArchivedForceOnPhysicalGroup("Face", 1001, 3),1.e-6)
check.equal(-4.485655e+04,mysolver.getArchivedForceOnPhysicalGroup("Face", 1002, 1),1.e-4)
check.equal(3.200269e+02,mysolver.getArchivedNodalValue(107,3,mysolver.displacement),1.e-6)

