// cylindre 

theta = Pi/(12.);
max = Pi/(2.*theta);

R1 = 0.15;
R2 = R1+0.0006;
R3 = R1+0.0012;
R4 = R1+0.0018;
R5 = R1+0.0024;
R6 = R1+0.0030;
R7 = R1+0.0036;

Lc1 = 0.006;

z= 0.4;

Point(100) = {0, 0.0, 0.0, Lc1};
Point(101) = {R1, 0.0, 0.0, Lc1};
Point(102) = {R2,  0.0, 0.0, Lc1};
Point(103) = {R3, 0.0, 0.0, Lc1};
Point(104) = {R4, 0.0, 0.0, Lc1};
Point(105) = {R5, 0.0, 0.0, Lc1};
Point(106) = {R6, 0.0, 0.0, Lc1};
Point(107) = {R7, 0.0, 0.0, Lc1};

Line(101) = {101,102};
Line(102) = {102,103};
Line(103) = {103,104};
Line(104) = {104,105};
Line(105) = {105,106};
Line(106) = {106,107};

Transfinite Line{101, 102, 103, 104, 105, 106} = 2;

For i In {2:max+1}

Point(i*100+1)  = {R1-(Sin((i-1)*Pi/24.))*(2.*R1*Sin((i-1)*Pi/24.)),0.+(Cos((i-1)*Pi/24.))*(2.*R1*Sin((i-1)*Pi/24.)) , 0.0,  Lc1};
Point(i*100+2)  = {R2-(Sin((i-1)*Pi/24.))*(2.*R2*Sin((i-1)*Pi/24.)),0.+(Cos((i-1)*Pi/24.))*(2.*R2*Sin((i-1)*Pi/24.)) , 0.0,  Lc1};
Point(i*100+3)  = {R3-(Sin((i-1)*Pi/24.))*(2.*R3*Sin((i-1)*Pi/24.)),0.+(Cos((i-1)*Pi/24.))*(2.*R3*Sin((i-1)*Pi/24.)) , 0.0,  Lc1};
Point(i*100+4)  = {R4-(Sin((i-1)*Pi/24.))*(2.*R4*Sin((i-1)*Pi/24.)),0.+(Cos((i-1)*Pi/24.))*(2.*R4*Sin((i-1)*Pi/24.)) , 0.0,  Lc1};
Point(i*100+5)  = {R5-(Sin((i-1)*Pi/24.))*(2.*R5*Sin((i-1)*Pi/24.)),0.+(Cos((i-1)*Pi/24.))*(2.*R5*Sin((i-1)*Pi/24.)) , 0.0,  Lc1};
Point(i*100+6)  = {R6-(Sin((i-1)*Pi/24.))*(2.*R6*Sin((i-1)*Pi/24.)),0.+(Cos((i-1)*Pi/24.))*(2.*R6*Sin((i-1)*Pi/24.)) , 0.0,  Lc1};
Point(i*100+7)  = {R7-(Sin((i-1)*Pi/24.))*(2.*R7*Sin((i-1)*Pi/24.)),0.+(Cos((i-1)*Pi/24.))*(2.*R7*Sin((i-1)*Pi/24.)) , 0.0,  Lc1};

EndFor

For i In {2:max+1}

Line(i*100+1)    = {i*100+1, i*100+2};
Line(i*100+2)    = {i*100+2, i*100+3};
Line(i*100+3)    = {i*100+3, i*100+4};
Line(i*100+4)    = {i*100+4, i*100+5};
Line(i*100+5)    = {i*100+5, i*100+6};
Line(i*100+6)    = {i*100+6, i*100+7};

Circle((i-1)*1000+1)    = {(i-1)*100+1, 100, i*100+1};
Circle((i-1)*1000+2)    = {(i-1)*100+2, 100, i*100+2};
Circle((i-1)*1000+3)    = {(i-1)*100+3, 100, i*100+3};
Circle((i-1)*1000+4)    = {(i-1)*100+4, 100, i*100+4};
Circle((i-1)*1000+5)    = {(i-1)*100+5, 100, i*100+5};
Circle((i-1)*1000+6)    = {(i-1)*100+6, 100, i*100+6};
Circle((i-1)*1000+7)    = {(i-1)*100+7, 100, i*100+7};

EndFor

For i In {2:max+1}

Line Loop((i-1)*100+1) = {-((i-1)*100+1),(i-1)*1000+1, i*100+1, -((i-1)*1000+2)};
Line Loop((i-1)*100+2) = {-((i-1)*100+2),(i-1)*1000+2, i*100+2, -((i-1)*1000+3)};
Line Loop((i-1)*100+3) = {-((i-1)*100+3),(i-1)*1000+3, i*100+3, -((i-1)*1000+4)};
Line Loop((i-1)*100+4) = {-((i-1)*100+4),(i-1)*1000+4, i*100+4, -((i-1)*1000+5)};
Line Loop((i-1)*100+5) = {-((i-1)*100+5),(i-1)*1000+5, i*100+5, -((i-1)*1000+6)};
Line Loop((i-1)*100+6) = {-((i-1)*100+6),(i-1)*1000+6, i*100+6, -((i-1)*1000+7)};

Plane Surface((i-1)*100+1) = {(i-1)*100+1};
Plane Surface((i-1)*100+2) = {(i-1)*100+2};
Plane Surface((i-1)*100+3) = {(i-1)*100+3};
Plane Surface((i-1)*100+4) = {(i-1)*100+4};
Plane Surface((i-1)*100+5) = {(i-1)*100+5};
Plane Surface((i-1)*100+6) = {(i-1)*100+6};

Transfinite Line{(i-1)*1000+1, (i-1)*1000+2, (i-1)*1000+3, (i-1)*1000+4, (i-1)*1000+5, (i-1)*1000+6, (i-1)*1000+7} = 4;

Transfinite Line{i*100+1, i*100+2, i*100+3, i*100+4, i*100+5, i*100+6} = 2;

Transfinite Surface {(i-1)*100+1, (i-1)*100+2, (i-1)*100+3, (i-1)*100+4, (i-1)*100+5, (i-1)*100+6} ;

Recombine Surface {(i-1)*100+1, (i-1)*100+2, (i-1)*100+3, (i-1)*100+4, (i-1)*100+5, (i-1)*100+6} ;
EndFor

my_mat_101[] = Extrude {0.0 , 0.0, z} {Surface{101}; Layers{3}; Recombine;};
my_mat_102[] = Extrude {0.0 , 0.0, z} {Surface{102}; Layers{3}; Recombine;};
my_mat_103[] = Extrude {0.0 , 0.0, z} {Surface{103}; Layers{3}; Recombine;};
my_mat_104[] = Extrude {0.0 , 0.0, z} {Surface{104}; Layers{3}; Recombine;};
my_mat_105[] = Extrude {0.0 , 0.0, z} {Surface{105}; Layers{3}; Recombine;};
my_mat_106[] = Extrude {0.0 , 0.0, z} {Surface{106}; Layers{3}; Recombine;};
Recombine  Surface {my_mat_101[0], my_mat_102[0], my_mat_103[0],my_mat_104[0],my_mat_105[0],my_mat_106[0]};
Transfinite Volume {my_mat_101[1], my_mat_102[1], my_mat_103[1],my_mat_104[1],my_mat_105[1],my_mat_106[1]};

my_mat_201[] = Extrude {0.0 , 0.0, z} {Surface{201}; Layers{3}; Recombine;};
my_mat_202[] = Extrude {0.0 , 0.0, z} {Surface{202}; Layers{3}; Recombine;};
my_mat_203[] = Extrude {0.0 , 0.0, z} {Surface{203}; Layers{3}; Recombine;};
my_mat_204[] = Extrude {0.0 , 0.0, z} {Surface{204}; Layers{3}; Recombine;};
my_mat_205[] = Extrude {0.0 , 0.0, z} {Surface{205}; Layers{3}; Recombine;};
my_mat_206[] = Extrude {0.0 , 0.0, z} {Surface{206}; Layers{3}; Recombine;};
Recombine  Surface {my_mat_201[0], my_mat_202[0], my_mat_203[0],my_mat_204[0],my_mat_205[0],my_mat_206[0]};
Transfinite Volume {my_mat_201[1], my_mat_202[1], my_mat_203[1],my_mat_204[1],my_mat_205[1],my_mat_206[1]};

my_mat_301[] = Extrude {0.0 , 0.0, z} {Surface{301}; Layers{3}; Recombine;};
my_mat_302[] = Extrude {0.0 , 0.0, z} {Surface{302}; Layers{3}; Recombine;};
my_mat_303[] = Extrude {0.0 , 0.0, z} {Surface{303}; Layers{3}; Recombine;};
my_mat_304[] = Extrude {0.0 , 0.0, z} {Surface{304}; Layers{3}; Recombine;};
my_mat_305[] = Extrude {0.0 , 0.0, z} {Surface{305}; Layers{3}; Recombine;};
my_mat_306[] = Extrude {0.0 , 0.0, z} {Surface{306}; Layers{3}; Recombine;};
Recombine  Surface {my_mat_301[0], my_mat_302[0], my_mat_303[0],my_mat_304[0],my_mat_305[0],my_mat_306[0]};
Transfinite Volume {my_mat_301[1], my_mat_302[1], my_mat_303[1],my_mat_304[1],my_mat_305[1],my_mat_306[1]};

my_mat_401[] = Extrude {0.0 , 0.0, z} {Surface{401}; Layers{3}; Recombine;};
my_mat_402[] = Extrude {0.0 , 0.0, z} {Surface{402}; Layers{3}; Recombine;};
my_mat_403[] = Extrude {0.0 , 0.0, z} {Surface{403}; Layers{3}; Recombine;};
my_mat_404[] = Extrude {0.0 , 0.0, z} {Surface{404}; Layers{3}; Recombine;};
my_mat_405[] = Extrude {0.0 , 0.0, z} {Surface{405}; Layers{3}; Recombine;};
my_mat_406[] = Extrude {0.0 , 0.0, z} {Surface{406}; Layers{3}; Recombine;};
Recombine  Surface {my_mat_401[0], my_mat_402[0], my_mat_403[0],my_mat_404[0],my_mat_405[0],my_mat_406[0]};
Transfinite Volume {my_mat_401[1], my_mat_402[1], my_mat_403[1],my_mat_404[1],my_mat_405[1],my_mat_406[1]};

my_mat_501[] = Extrude {0.0 , 0.0, z} {Surface{501}; Layers{3}; Recombine;};
my_mat_502[] = Extrude {0.0 , 0.0, z} {Surface{502}; Layers{3}; Recombine;};
my_mat_503[] = Extrude {0.0 , 0.0, z} {Surface{503}; Layers{3}; Recombine;};
my_mat_504[] = Extrude {0.0 , 0.0, z} {Surface{504}; Layers{3}; Recombine;};
my_mat_505[] = Extrude {0.0 , 0.0, z} {Surface{505}; Layers{3}; Recombine;};
my_mat_506[] = Extrude {0.0 , 0.0, z} {Surface{506}; Layers{3}; Recombine;};
Recombine  Surface {my_mat_501[0], my_mat_502[0], my_mat_503[0],my_mat_504[0],my_mat_505[0],my_mat_506[0]};
Transfinite Volume {my_mat_501[1], my_mat_502[1], my_mat_503[1],my_mat_504[1],my_mat_505[1],my_mat_506[1]};

my_mat_601[] = Extrude {0.0 , 0.0, z} {Surface{601}; Layers{3}; Recombine;};
my_mat_602[] = Extrude {0.0 , 0.0, z} {Surface{602}; Layers{3}; Recombine;};
my_mat_603[] = Extrude {0.0 , 0.0, z} {Surface{603}; Layers{3}; Recombine;};
my_mat_604[] = Extrude {0.0 , 0.0, z} {Surface{604}; Layers{3}; Recombine;};
my_mat_605[] = Extrude {0.0 , 0.0, z} {Surface{605}; Layers{3}; Recombine;};
my_mat_606[] = Extrude {0.0 , 0.0, z} {Surface{606}; Layers{3}; Recombine;};
Recombine  Surface {my_mat_601[0], my_mat_602[0], my_mat_603[0],my_mat_604[0],my_mat_605[0],my_mat_606[0]};
Transfinite Volume {my_mat_601[1], my_mat_602[1], my_mat_603[1],my_mat_604[1],my_mat_605[1],my_mat_606[1]};

//For i In {2:max+1}

//my_mat_[][(i-1)*100+1] = Extrude {0.0 , 0.0 , -z} {Surface{(i-1)*100+1}; Layers{3}; Recombine;};
//my_mat_(i-1)*100+2[] = Extrude {0.0 , 0.0 , -z} {Surface{(i-1)*100+2}; Layers{3}; Recombine;};
//my_mat_(i-1)*100+3[] = Extrude {0.0 , 0.0 , -z} {Surface{(i-1)*100+3}; Layers{3}; Recombine;};
//my_mat_(i-1)*100+4[] = Extrude {0.0 , 0.0 , -z} {Surface{(i-1)*100+4}; Layers{3}; Recombine;};
//Recombine  Surface {my_mat_(i-1)*100+1[0], my_mat_(i-1)*100+2[0], my_mat_(i-1)*100+3[0],my_mat_(i-1)*100+4[0]};
//Transfinite Volume {my_mat_(i-1)*100+1[1], my_mat_(i-1)*100+2[1], my_mat_(i-1)*100+3[1],my_mat_(i-1)*100+4[1]};

//EndFor

Physical Volume(101) = my_mat_101[1];
Physical Volume(102) = my_mat_102[1];
Physical Volume(103) = my_mat_103[1];
Physical Volume(104) = my_mat_104[1];
Physical Volume(105) = my_mat_105[1];
Physical Volume(106) = my_mat_106[1];

Physical Volume(201) = my_mat_201[1];
Physical Volume(202) = my_mat_202[1];
Physical Volume(203) = my_mat_203[1];
Physical Volume(204) = my_mat_204[1];
Physical Volume(205) = my_mat_205[1];
Physical Volume(206) = my_mat_206[1];

Physical Volume(301) = my_mat_301[1];
Physical Volume(302) = my_mat_302[1];
Physical Volume(303) = my_mat_303[1];
Physical Volume(304) = my_mat_304[1];
Physical Volume(305) = my_mat_305[1];
Physical Volume(306) = my_mat_306[1];

Physical Volume(401) = my_mat_401[1];
Physical Volume(402) = my_mat_402[1];
Physical Volume(403) = my_mat_403[1];
Physical Volume(404) = my_mat_404[1];
Physical Volume(405) = my_mat_405[1];
Physical Volume(406) = my_mat_406[1];

Physical Volume(501) = my_mat_501[1];
Physical Volume(502) = my_mat_502[1];
Physical Volume(503) = my_mat_503[1];
Physical Volume(504) = my_mat_504[1];
Physical Volume(505) = my_mat_505[1];
Physical Volume(506) = my_mat_506[1];

Physical Volume(601) = my_mat_601[1];
Physical Volume(602) = my_mat_602[1];
Physical Volume(603) = my_mat_603[1];
Physical Volume(604) = my_mat_604[1];
Physical Volume(605) = my_mat_605[1];
Physical Volume(606) = my_mat_606[1];

Physical Volume(1) = {my_mat_101[1], my_mat_102[1], my_mat_103[1], my_mat_104[1], my_mat_105[1], my_mat_106[1], my_mat_201[1], my_mat_202[1], my_mat_203[1], my_mat_204[1], my_mat_205[1], my_mat_206[1], my_mat_301[1], my_mat_302[1], my_mat_303[1], my_mat_304[1], my_mat_305[1], my_mat_306[1], my_mat_401[1], my_mat_402[1], my_mat_403[1], my_mat_404[1], my_mat_405[1], my_mat_406[1], my_mat_501[1], my_mat_502[1], my_mat_503[1], my_mat_504[1], my_mat_505[1], my_mat_506[1], my_mat_601[1], my_mat_602[1], my_mat_603[1], my_mat_604[1], my_mat_605[1], my_mat_606[1]};

Physical Volume(1001) = {my_mat_101[1], my_mat_201[1], my_mat_301[1],my_mat_401[1],my_mat_501[1],my_mat_601[1]};
Physical Volume(1002) = {my_mat_102[1], my_mat_202[1], my_mat_302[1],my_mat_402[1],my_mat_502[1],my_mat_602[1]};
Physical Volume(1003) = {my_mat_103[1], my_mat_203[1], my_mat_303[1],my_mat_403[1],my_mat_503[1],my_mat_603[1]};
Physical Volume(1004) = {my_mat_104[1], my_mat_204[1], my_mat_304[1],my_mat_404[1],my_mat_504[1],my_mat_604[1]};
Physical Volume(1005) = {my_mat_105[1], my_mat_205[1], my_mat_305[1],my_mat_405[1],my_mat_505[1],my_mat_605[1]};
Physical Volume(1006) = {my_mat_106[1], my_mat_206[1], my_mat_306[1],my_mat_406[1],my_mat_506[1],my_mat_606[1]};

Physical Surface(1001) = {6020,6152,6284,6416,6548,6680}; // Face sur laquelle la pression et la température seront appliquées
Physical Surface(1002) = {6016,6038,6060,6082,6104,6126}; // Côté gauche
Physical Surface(1003) = {6684,6706,6728,6750,6772,6794}; // Côté droit
Physical Surface(1004) = {6138,6270,6402,6534,6666,6798}; //Couche 6

Physical Surface(1005) ={101,102,103,104,105,106,201,202,203,204,205,206,301,302,303,304,305,306,401,402,403,404,405,406,501,502,503,504,505,506,601,602,603,604,605,606};
Physical Surface(1006) = {6029,6051,6073,6095,6117,6139,6161,6183,6205,6227,6249,6271,6293,6315,6337,6359,6381,6403,6425,6447,6469,6491,6513,6535,6557,6579,6601,6623,6645,6667,6689,6711,6733,6755,6777,6799};

Physical Point(107) ={107};
Physical Point(207) ={207};
Physical Point(307) ={307};
Physical Point(407) ={407};
Physical Point(507) ={507};
Physical Point(607) ={607};
Physical Point(707) ={707};

