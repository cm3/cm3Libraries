from gmshpy import *
from dG3Dpy import*

# SLS result file
SLSresultsFile = "SLSResults/SLSResultsPowderBedT150.csv"
damage = False
ftimeRamp=0.2
ftime = 0.5+ftimeRamp  # Final time (used only if soltype=1)


# Mechanical material law
E0 = 1366.7#MatPara[0][0]
nu0 = 0.276#MatPara[0][1]
Nt = 8#len(MatPara[1])    #the number of branches in the generalized Maxwell model 

sy0c = 30.04#MatPara[indxplastic][0]  #MPa, compressive yield stress
hc = 7.85e-3#MatPara[indxplastic][1]
hc2 = 14.41#MatPara[indxplastic][2]
kc = 296.38#MatPara[indxplastic][3]
  
m=0.8
sy0t = m*sy0c#MatPara[indxplastic+1][0]*sy0c #MPa, compressive yield stress
ht = 146.34#MatPara[indxplastic+1][1]
ht2 = 5.24e-4#MatPara[indxplastic+1][2]
kt = 9955.79#MatPara[indxplastic+1][3]
  
alpha = 3.63#MatPara[indxplastic+2][0]
beta = 1.5*0.18#MatPara[indxplastic+2][1]
eta = 1.33e5#MatPara[indxplastic+2][2]
p = 0.18#MatPara[indxplastic+2][3]

#kinematic hardening#
hk1=0.#14
hk2=0.#13.0
hk3=0.#13.0
#Saturation law
l = 700e-3
cl = l*l
H = 0.
Dinf = 0.
pi = 0.1
alpha2=3.


#Failure law
pf = 0.00725 # 0.117#0.01
af = 0.08#0.55
bf = 7.5#0.55
cf = 0.07

damlaw1 = SimpleSaturateDamageLaw(1, H, Dinf,pi,alpha2)

fct = linearScalarFunction(0,cl,-cl/(Dinf+0.001))
cl1     = GeneralVariableIsotropicCLengthLaw(1, fct)


damlaw2 = PowerBrittleDamagelaw(2,0,pf,1.,0.3)
damlaw2.setCriticalDamage(0.97)
cl2     = IsotropicCLengthLaw(2, cl)
#=============================================    
lawnum = 11
rho = 7850e-9
hardenc = LinearExponentialJ2IsotropicHardening(1, sy0c, hc, hc2, kc)
hardent = LinearExponentialJ2IsotropicHardening(2, sy0t, ht, ht2, kt)
# hardenk = PolynomialKinematicHardening(3,3)
# hardenk.setCoefficients(1,hk1) #polynomial coefficients
# hardenk.setCoefficients(2,hk2)
# hardenk.setCoefficients(3,hk3)

if damage:
  law1   = NonLocalDamagePowerYieldHyperDG3DMaterialLawWithFailure(lawnum,rho,E0,nu0,hardenc, hardent,cl1,cl2,damlaw1,damlaw2,1.e-6, False, 1.e-8)
else:
  law1   = HyperViscoElastoPlasticPowerYieldDG3DMaterialLaw(lawnum,rho,E0,nu0)
  law1.setCompressionHardening(hardenc)
  law1.setTractionHardening(hardent)
       
law1.setViscoelasticMethod(0)
law1.setViscoElasticNumberOfElement(Nt)
G = [0.33, 27.23, 18.55, 1.62, 21.66, 9.91, 1722.24, 6.13]
K = [28281.49, 0.30, 747.17, 467.82, 0.51, 7874.44, 39.13, 2422.40]
g1 = 7.36
k1 = 9.12

for i in range(1,Nt+1):
  law1.setViscoElasticData_Bulk(i, K[i-1], k1*10**(-(i-1)))
  law1.setViscoElasticData_Shear(i, G[i-1], g1*10**(-(i-1)))
law1.setStrainOrder(5) 

law1.setYieldPowerFactor(alpha)
law1.setNonAssociatedFlow(True)
law1.nonAssociatedFlowRuleFactor(beta)
etac = constantViscosityLaw(1,eta)
law1.setViscosityEffect(etac,p)

if damage:
    failureCr = CritialPlasticDeformationCriteration(1,af,bf,cf)
    law1.setFailureCriterion(failureCr)



# Mechanical law with manufacturing effects included 
lawnum2=lawnum+1
law2   = GenericResidualMechanicsDG3DMaterialLaw(lawnum2, rho, SLSresultsFile, True)
law2.setMechanicalMaterialLaw(law1)
#_cp = constantScalarFunction(1.0)
#_cp = linearScalarFunction(0,0,1)
_cp = piecewiseScalarFunction()
_cp.put(0., 0.)
_cp.put(ftimeRamp,1.)
_cp.put(ftime,1.)
law2.setLawForFRes(_cp)
#pertLaw2 = dG3DMaterialLawWithTangentByPerturbation(law2,1.e-8)

# creation of geometry

geofile="USF_vf10.geo" # "Cell3_1.geo"
meshfile="USF_vf10.msh" # "Cell3_1.msh" # name of mesh file

# creation of part Domain
dim = 3
beta1 = 50
fdg = 0

if damage:
  myfield1 = dG3DDomain(1000, 51, 0, lawnum2, fdg, dim,2)
  myfield1.matrixByPerturbation(1,1,1,1.e-8)
  myfield1.stabilityParameters(10)
  myfield1.setNonLocalEqRatio(1000.)
  myfield1.usePrimaryShapeFunction(3)
  myfield1.usePrimaryShapeFunction(4)
else:
  myfield1 = dG3DDomain(1000, 51, 0, lawnum2, fdg, dim) 
myfield1.strainSubstep(2,2)

# solver
sol = 2  # Gmm=0 (default) Taucs=1 PETsc=2
soltype =1 # StaticLinear=0 (default) StaticNonLinear=1
nstep = 1000  # number of step (used only if soltype=1)
tol=1.e-4 # relative tolerance for NR scheme (used only if soltype=1)
nstepArch=2 # Number of step between 2 archiving (used only if soltype=1)

# creation of Solver
mysolver = nonLinearMechSolver(1000)
mysolver.createModel(geofile,meshfile,3,2)
mysolver.addDomain(myfield1)
#mysolver.addDomain(myfield2)
mysolver.addMaterialLaw(law2)
#mysolver.addMaterialLaw(pertLaw2)
mysolver.Scheme(soltype)
mysolver.Solver(sol)
mysolver.snlData(nstep,ftime,tol,tol/100.)
mysolver.stepBetweenArchiving(nstepArch)
mysolver.options("-ksp_type preonly -pc_type lu -pc_factor_mat_solver_type petsc")
mysolver.setNumStepTimeInterval(ftimeRamp,10)
mysolver.setNumStepTimeInterval(ftime,nstep)


mysolver.snlManageTimeStep(150,2,2.,500)
stiffModif = BFGSStiffnessModificationMonitoring(20, False) # stiffness will be re-computed after 20 iterations in each step
mysolver.stiffnessModification(stiffModif)


#mysolver.displacementBC("Face",110,1,0.)

'''
mysolver.displacementBC("Face",120,0,0.)
mysolver.displacementBC("Face",120,1,0.)
mysolver.displacementBC("Face",120,2,0.)

mysolver.displacementBC("Face",121,2,-1.00)
mysolver.displacementBC("Face",121,1,0.00)
mysolver.displacementBC("Face",121,0,0.00)'''

if damage:
  microBC = nonLinearPeriodicBC(10,3,2)
else:
  microBC = nonLinearPeriodicBC(10,3)

microBC.setOrder(1)
microBC.setBCPhysical(100,110,120,101,111,121) #Periodic boundary condition

method = 0	# Periodic mesh = 0, Langrange interpolation = 1, Cubic spline interpolation =2,  FE linear= 3, FE Quad = 4, Projetion=5, highorder FE=6
degree = 3	# Order used for polynomial interpolation 
addvertex = False # Polynomial interpolation by mesh vertex = 0, Polynomial interpolation by virtual vertex 

microBC.setPeriodicBCOptions(method, degree,addvertex) 
serendip = False
microBC.usingSerendip(serendip)

# Deformation gradient
mysolver.addMicroBC(microBC)

mysolver.activateTest(True)

mysolver.displacementBC("Volume",51,3,0.) #no saturation damage
#mysolver.sameDisplacementBCBetweenTwoGroups("Face", 100, 101, 4)
#mysolver.sameDisplacementBCBetweenTwoGroups("Face", 110, 111, 4)
#mysolver.sameDisplacementBCBetweenTwoGroups("Face", 120, 121, 4)

mysolver.displacementBCOnControlNode(1,0,0.)
mysolver.displacementBCOnControlNode(1,1,0.)
mysolver.displacementBCOnControlNode(1,2,0.)

#mysolver.displacementBCOnControlNode(2,1,0.)
mysolver.displacementBCOnControlNode(5,2,0.)

mysolver.displacementBCOnControlNode(4,0,0.)
mysolver.displacementBCOnControlNode(4,2,0.)

forceBC = False
dispFunction=cycleFunctionTime(0., 0.,ftimeRamp,0.,ftime,-5.*(ftime-ftimeRamp));
if forceBC:
	mysolver.forceBCOnControlNode(2,2,-6e1)
else:
	mysolver.displacementBCOnControlNode(2,2,dispFunction)

#mysolver.stressAveragingFlag(True)
#mysolver.setStressAveragingMethod(0) 

# build view
mysolver.internalPointBuildView("Green-Lagrange_xx",IPField.STRAIN_XX, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_yy",IPField.STRAIN_YY, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_zz",IPField.STRAIN_ZZ, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_xy",IPField.STRAIN_XY, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_yz",IPField.STRAIN_YZ, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_xz",IPField.STRAIN_XZ, 1, 1);
mysolver.internalPointBuildView("sig_xx",IPField.SIG_XX, 1, 1);
mysolver.internalPointBuildView("sig_yy",IPField.SIG_YY, 1, 1);
mysolver.internalPointBuildView("sig_zz",IPField.SIG_ZZ, 1, 1);
mysolver.internalPointBuildView("sig_xy",IPField.SIG_XY, 1, 1);
mysolver.internalPointBuildView("sig_yz",IPField.SIG_YZ, 1, 1);
mysolver.internalPointBuildView("sig_xz",IPField.SIG_XZ, 1, 1);
mysolver.internalPointBuildView("damage",IPField.DAMAGE, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange equivalent strain",IPField.GL_NORM, 1, 1);

mysolver.internalPointBuildView("Res_xx",IPField.FfAM_XX,1,1);
mysolver.internalPointBuildView("Res_yy",IPField.FfAM_YY,1,1);
mysolver.internalPointBuildView("Res_zz",IPField.FfAM_ZZ,1,1);
mysolver.internalPointBuildView("Res_xy",IPField.FfAM_XY,1,1);
mysolver.internalPointBuildView("Res_zx",IPField.FfAM_ZX,1,1);
mysolver.internalPointBuildView("Res_yz",IPField.FfAM_YZ,1,1);


#mysolver.archivingForceOnPhysicalGroup("Edge",1,1)
#mysolver.archivingNodeDisplacement(4,1,1)

mysolver.archivingNodeDisplacementOnControlNode(2,0)
mysolver.archivingNodeDisplacementOnControlNode(2,1)
mysolver.archivingNodeDisplacementOnControlNode(2,2)

mysolver.archivingNodeDisplacementOnControlNode(4,0)
mysolver.archivingNodeDisplacementOnControlNode(4,1)
mysolver.archivingNodeDisplacementOnControlNode(4,2)


mysolver.archivingAverageValue(IPField.P_XX)
mysolver.archivingAverageValue(IPField.P_YY)
mysolver.archivingAverageValue(IPField.P_ZZ)
mysolver.archivingAverageValue(IPField.P_XZ)
mysolver.archivingAverageValue(IPField.P_XY)
mysolver.archivingAverageValue(IPField.P_YX)
mysolver.archivingAverageValue(IPField.P_ZX)
mysolver.archivingAverageValue(IPField.P_YZ)
mysolver.archivingAverageValue(IPField.P_ZY)


mysolver.archivingAverageValue(IPField.DEFO_ENERGY)
mysolver.archivingAverageValue(IPField.PLASTIC_ENERGY)
mysolver.archivingAverageValue(IPField.IRREVERSIBLE_ENERGY)
mysolver.archivingAverageValue(IPField.DAMAGE_ENERGY)
mysolver.archivingAverageValue(IPField.FfAM_XX)
mysolver.archivingAverageValue(IPField.FfAM_YY)
mysolver.archivingAverageValue(IPField.FfAM_ZZ)
mysolver.archivingAverageValue(IPField.FfAM_XY)
mysolver.archivingAverageValue(IPField.FfAM_YZ)
mysolver.archivingAverageValue(IPField.FfAM_ZX)

# solve
mysolver.solve()


check = TestCheck()
try:
  import linecache
  linesxx = linecache.getline('Average_P_ZZ.csv',-1)
except:
  print('Cannot read the results in the files')
  import os
  os._exit(1)
else:
  check.equal(-118.49395149776,float(linesxx.split(';')[1]))

