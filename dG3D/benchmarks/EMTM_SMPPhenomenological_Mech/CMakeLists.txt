# test file

set(PYFILE EMTM_SMP_mech.py)

set(FILES2DELETE 
  disp*.msh
  stres*.msh
  *.csv
  previous*
)

add_cm3python_test(${PYFILE} "${FILES2DELETE}")

