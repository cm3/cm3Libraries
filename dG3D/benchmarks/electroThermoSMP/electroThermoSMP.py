#coding-Utf-8-*-
from gmshpy import *



#from dG3DpyDebug import*
from dG3Dpy import *


#script to launch beam problem with a python script

# material law#
lawnum= 1 # unique number of law
rho   = 1020.
#young =Ex=Ey=Ez =2.1e11
#nu    =Vxy=Vxz=Vyz= 0.3
#MUxy=MUxz=MUyz=Ex/(2.*(1.+Vxy)) 
cv=1.
#Kx=Ky=Kz=51.9

mu_groundState3=0.75e6
Im3=5.
pi=3.14159

tinitial =t0=273.+58+25# 273.+58
Tr=310
Nmu_groundState2= 0.045
mu_groundState2=1.38e6
Im2=6.3
Sgl2=58.e6 
Sr2=3.e2
Delta=2.6
m2=0.19
epsilonr=5.2e-4	
n=2.1
epsilonp02=5.2e-4
Ggl1=156.e6
Gr1=13.4e6
Mgl1=7.4e6
Mr1=0.168e6
Mugl1=0.35
Mur1=0.49
epsilon01=1.73e13
Qgl1=1.4e-19
Qr1=0.2e-21
epsygl1=0.14 
#epsyr1=0.
d1=0.015
Kb=1.3806488e-23
m1=0.17
V1=2.16e-27
alphap=0.058
Sa0=0.
ha1=230.
b1=5850e6
g1=5.8
phai01=0.
Z1=0.083
r1=1.3
s1=0.005
Sb01=0.
Hgl1=1.56e6
Lgl1=0.44e6
Hr1=0.76e6
Lr1=0.006e6
l1=0.5
be1=0.5
#kb=1.3806488e-23
alpha = beta=gamma=0.
cp=0.;# i don't use it here 
wp=0.#0.6
c0=1710.*rho
c1=4.1*rho
v0=0.
#alphax=alphay=alphaz=12.e-6 #for smp
#Kx=Ky=Kz=0.2#0.5 for smp
#lx=ly=lz=3.3#0.101 for smp
#seebeck=10.e-5#10.e-6 for smp
#alphar1=25.e-5
#alphagl1=13.e-5
alphax=alphay=alphaz=2.e-6
Kx=Ky=Kz=1.612#24.3  
lx=ly=lz=8.4e3#8.4e4 the right value
seebeck=1.941e-4#1.9e-6  #carbon
#cp= 0.#712.*rho #for paper material #for carbon graphit 712 or 544.
alphar1=alphagl1=2.e-6



# geometry
geofile="electroThermoSMP.geo"
meshfile="electroThermoSMP.msh" # name of mesh file

# solver
sol = 2  # Gmm=0 (default) Taucs=1 PETsc=2
soltype = 1 # StaticLinear=0 (default) StaticNonLinear=1
nstep = 40  # number of step (used only if soltype=1) 500. with cp, 20 work before
ftime =10.   # 1  Final time (used only if soltype=1) 10000. with cp 10
tol=1.e-4   # relative tolerance for NR scheme (used only if soltype=1)
nstepArch=1 # Number of step between 2 archiving (used only if soltype=1)
fullDg = 1 #O = CG, 1 = DG
space1 = 0 # function space (Lagrange=0)
beta1  = 100#100


#Tangent2

#  compute solution and BC (given directly to the solver
# creation of law

law1   = mlawElecSMPDG3DMaterialLaw( lawnum,rho,alpha,beta,gamma,tinitial,Kx,Ky,Kz,mu_groundState3,Im3,pi,
Tr,Nmu_groundState2, mu_groundState2,  Im2,Sgl2, Sr2, Delta, m2,epsilonr,n,epsilonp02, alphar1, alphagl1, Ggl1, Gr1, Mgl1, Mr1, Mugl1, Mur1, epsilon01, Qgl1,
		           Qr1, epsygl1 , d1,  m1, V1,  alphap, Sa0, ha1, b1, g1, phai01, Z1,r1, s1, Sb01, Hgl1, Lgl1, Hr1, Lr1, l1, Kb, be1,c0,wp,c1,lx,ly,lz,seebeck,v0)

law1.setUseBarF(True)

nfield = 167 # number of the field (physical number of surface)

myfield1 = dG3DDomain(1000,nfield,space1,lawnum,fullDg,3,0,2)
myfield1.setConstitutiveExtraDofDiffusionEqRatio(1.e6);
myfield1.matrixByPerturbation(0,0,0,1e-8)
#myfield1.matrixByPerturbation(1,1,1,1e-8)
myfield1.stabilityParameters(beta1)
myfield1.setConstitutiveExtraDofDiffusionStabilityParameters(beta1,bool(1))
myfield1.setConstitutiveExtraDofDiffusionUseEnergyConjugatedField(bool(0))
# creation of Solver
mysolver = nonLinearMechSolver(1000)
mysolver.createModel(geofile,meshfile,3,1)
#mysolver.loadModel(meshfile)
mysolver.addDomain(myfield1)
mysolver.addMaterialLaw(law1)
mysolver.Scheme(soltype)
mysolver.Solver(sol)
mysolver.snlData(nstep,ftime,tol)
mysolver.stepBetweenArchiving(nstepArch)
# BC

#mechanical BC


#mysolver.displacementBC("Volume",nfield,0,0.)
#mysolver.displacementBC("Volume",nfield,1,0.)
#mysolver.displacementBC("Volume",nfield,2,0.)


mysolver.displacementBC("Face",257,0,0.)
mysolver.displacementBC("Face",239,2,0.)
mysolver.displacementBC("Face",241,2,0.)
mysolver.displacementBC("Face",243,2,0.)
mysolver.displacementBC("Face",245,2,0.)
mysolver.displacementBC("Face",259,1,0.)
mysolver.displacementBC("Face",261,1,0.)
mysolver.displacementBC("Face",263,1,0.)
mysolver.displacementBC("Face",265,1,0.)

#thermal BC
cyclicFunctionTemp1=cycleFunctionTime(0., t0, ftime, t0+1);#2test   28,29
cyclicFunctionTemp2=cycleFunctionTime(0., t0, ftime, t0+1);#2test
#cyclicFunctionTemp1=cycleFunctionTime(0., t0+25., ftime, t0+26.);#2test
#cyclicFunctionTemp2=cycleFunctionTime(0., t0+25., ftime, t0+26.);#2test
mysolver.displacementBC("Face",257,3,cyclicFunctionTemp1)#2test
mysolver.displacementBC("Face",247,3,cyclicFunctionTemp2)#2test
mysolver.initialBC("Volume","Position",nfield,3,t0)#2test

#electrical BC
cyclicFunctionV1=cycleFunctionTime(0., 0.0, ftime, 0.058);#2test
cyclicFunctionV2=cycleFunctionTime(0.,0.0, ftime, 0.0);#2test
mysolver.displacementBC("Face",257,4,cyclicFunctionV1);#2test
mysolver.displacementBC("Face",247,4,cyclicFunctionV2);#2test
mysolver.initialBC("Volume","Position",nfield,4,0.0);#2test

mysolver.internalPointBuildView("svm",IPField.SVM, 1, 1)
mysolver.internalPointBuildView("sig_xx",IPField.SIG_XX, 1, 1)
mysolver.internalPointBuildView("sig_yy",IPField.SIG_YY, 1, 1)
mysolver.internalPointBuildView("sig_zz",IPField.SIG_ZZ, 1, 1)
mysolver.internalPointBuildView("sig_xy",IPField.SIG_XY, 1, 1)
mysolver.internalPointBuildView("sig_yz",IPField.SIG_YZ, 1, 1)
mysolver.internalPointBuildView("sig_xz",IPField.SIG_XZ, 1, 1)
mysolver.internalPointBuildView("temperature",IPField.TEMPERATURE, 1, 1)
mysolver.internalPointBuildView("qx",IPField.THERMALFLUX_X, 1, 1)
mysolver.internalPointBuildView("qy",IPField.THERMALFLUX_Y, 1, 1)
mysolver.internalPointBuildView("qz",IPField.THERMALFLUX_Z, 1, 1)
mysolver.internalPointBuildView("voltage",IPField.VOLTAGE, 1, 1)
mysolver.internalPointBuildView("ex",IPField.ELECTRICALFLUX_X, 1, 1)
mysolver.internalPointBuildView("ey",IPField.ELECTRICALFLUX_Y, 1, 1)
mysolver.internalPointBuildView("ez",IPField.ELECTRICALFLUX_Z, 1, 1)

mysolver.archivingNodeDisplacement(97,0,1);
mysolver.archivingNodeDisplacement(93,0,1);

mysolver.archivingNodeDisplacement(97,3,1);
mysolver.archivingNodeDisplacement(96,3,1);
mysolver.archivingNodeDisplacement(95,3,1);
mysolver.archivingNodeDisplacement(94,3,1);
mysolver.archivingNodeDisplacement(93,3,1);

mysolver.archivingNodeDisplacement(97,4,1);
mysolver.archivingNodeDisplacement(96,4,1);
mysolver.archivingNodeDisplacement(95,4,1);
mysolver.archivingNodeDisplacement(94,4,1);
mysolver.archivingNodeDisplacement(93,4,1);

mysolver.archivingForceOnPhysicalGroup("Face", 247, 0)
mysolver.archivingForceOnPhysicalGroup("Face", 247, 1)
mysolver.archivingForceOnPhysicalGroup("Face", 247, 2)
mysolver.archivingForceOnPhysicalGroup("Face", 247, 3)
mysolver.archivingForceOnPhysicalGroup("Face", 247, 4)

mysolver.archivingNodeIP(97, IPField.THERMALFLUX_X,IPField.MEAN_VALUE,1);
mysolver.archivingNodeIP(96, IPField.THERMALFLUX_X,IPField.MEAN_VALUE,1);
mysolver.archivingNodeIP(95, IPField.THERMALFLUX_X,IPField.MEAN_VALUE,1);
mysolver.archivingNodeIP(94, IPField.THERMALFLUX_X,IPField.MEAN_VALUE,1);
mysolver.archivingNodeIP(93, IPField.THERMALFLUX_X,IPField.MEAN_VALUE,1);

mysolver.archivingNodeIP(97, IPField.ELECTRICALFLUX_X,IPField.MEAN_VALUE,1);
mysolver.archivingNodeIP(96, IPField.ELECTRICALFLUX_X,IPField.MEAN_VALUE,1);
mysolver.archivingNodeIP(95, IPField.ELECTRICALFLUX_X,IPField.MEAN_VALUE,1);
mysolver.archivingNodeIP(94, IPField.ELECTRICALFLUX_X,IPField.MEAN_VALUE,1);
mysolver.archivingNodeIP(93, IPField.ELECTRICALFLUX_X,IPField.MEAN_VALUE,1);

mysolver.solve()

check = TestCheck()
check.equal(0.,mysolver.getArchivedForceOnPhysicalGroup("Face", 247, 0),1.e-6)
check.equal(-2.462150e-05,mysolver.getArchivedForceOnPhysicalGroup("Face", 247, 1),1.e-4)
check.equal(3.584923e-05,mysolver.getArchivedForceOnPhysicalGroup("Face", 247, 2),1.e-4)
check.equal(4.282709e+04,mysolver.getArchivedForceOnPhysicalGroup("Face", 247, 3),1.e-6)
check.equal(4.429112e+05,mysolver.getArchivedForceOnPhysicalGroup("Face", 247, 4),1.e-6)
check.equal(6.028935e-09,mysolver.getArchivedNodalValue(93,0,mysolver.displacement),1.e-1)
check.equal(3.570000e+02,mysolver.getArchivedNodalValue(97,3,mysolver.displacement),1.e-6)
check.equal(5.800000e-02,mysolver.getArchivedNodalValue(97,4,mysolver.displacement),1.e-6)
check.equal(3.585401e+02,mysolver.getArchivedNodalValue(96,3,mysolver.displacement),1.e-6)
check.equal(4.320110e-02,mysolver.getArchivedNodalValue(96,4,mysolver.displacement),1.e-6)
check.equal(3.590479e+02,mysolver.getArchivedNodalValue(95,3,mysolver.displacement),1.e-6)
check.equal(2.860252e-02,mysolver.getArchivedNodalValue(95,4,mysolver.displacement),1.e-6)
check.equal(3.585401e+02,mysolver.getArchivedNodalValue(94,3,mysolver.displacement),1.e-6)
check.equal(1.420107e-02,mysolver.getArchivedNodalValue(94,4,mysolver.displacement),1.e-6)
check.equal(3.570000e+02,mysolver.getArchivedNodalValue(93,3,mysolver.displacement),1.e-6)
check.equal(0.,mysolver.getArchivedNodalValue(93,4,mysolver.displacement),1.e-6)


