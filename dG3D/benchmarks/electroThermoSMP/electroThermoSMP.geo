// Test case a SCB with a vertical load at its free extremity
// Size

//definition of unit
mm = 1e-03;

// volum fraction

x=1.54*mm/10.;
y=1.4*mm;
z=1*mm;

// Characteristic length
Lc1=z/2.5;

// definition of points
Point(1) = { 0.0  , 0.0  , 0.0 , Lc1};
Point(2) = { 2.5*x , 0.0  , 0.0 , Lc1};
Point(3) = { 5.*x , 0.0  , 0.0 , Lc1};
Point(4) = { 7.5*x , 0.0  , 0.0 , Lc1};
Point(5) = {10.*x , 0.0   , 0.0 , Lc1};
//Point(7) = {10.*x , 2.*y , 0.0 , Lc1};
//Point(8) = {10.*x , 3.*y , 0.0 , Lc1};
//Point(9) = {10.*x , 4.*y , 0.0 , Lc1};
Point(6) = {10.*x , y    , 0.0 , Lc1};
Point(7) = { 7.5*x ,y   , 0.0 , Lc1};
Point(8) = { 5.*x , y   , 0.0 , Lc1};
Point(9) = { 2.5*x ,y   , 0.0 , Lc1};


Point(10) = { 0.0 ,  y  , 0.0 , Lc1};
Point(11) = { 0.0 , 0.0 , z , Lc1};
Point(12) = { 2.5*x , 0.0  ,z , Lc1};
Point(13) = { 5.*x , 0.0  , z , Lc1};
Point(14) = { 7.5*x , 0.0  , z , Lc1};
Point(15) = { 10.*x  , 0.0 , z , Lc1};
Point(16) = { 2.5*x , y  ,z , Lc1};
Point(17) = { 5.*x , y  , z , Lc1};
Point(18) = { 7.5*x , y  , z , Lc1};
Point(19) = {10.*x   ,  y  , z , Lc1};
Point(20) = { 0.0 ,  y  , z , Lc1};



Line(1) = {20, 16};
Line(2) = {16, 17};
Line(3) = {17, 18};
Line(4) = {18, 19};
Line(5) = {19, 15};
Line(6) = {15, 14};
Line(7) = {14, 13};
Line(8) = {13, 12};
Line(9) = {12, 11};
Line(11) = {11, 20};
Line(12) = {20, 10};
Line(13) = {10, 9};
Line(14) = {9, 8};
Line(15) = {8, 7};
Line(16) = {7, 6};
Line(17) = {6, 19};
Line(18) = {6, 5};
Line(19) = {5, 4};
Line(20) = {4, 3};
Line(21) = {3, 2};
Line(22) = {2, 1};
Line(23) = {1, 10};
Line(24) = {1, 11};
Line(25) = {15, 5};
Line(26) = {14, 4};
Line(27) = {13, 3};
Line(28) = {12, 2};
Line(29) = {16, 9};
Line(30) = {17, 8};
Line(31) = {18, 7};
Line(32) = {14, 18};
Line(33) = {17, 13};
Line(34) = {12, 16};
Line(35) = {7, 4};
Line(36) = {8, 3};
Line(37) = {9, 2};
Line Loop(38) = {9, 11, 1, -34};
Plane Surface(39) = {38};
Line Loop(40) = {8, 34, 2, 33};
Plane Surface(41) = {40};
Line Loop(42) = {7, -33, 3,-32};
Plane Surface(43) = {42};
Line Loop(44) = {6, 32, 4, 5};
Plane Surface(45) = {44};
Line Loop(46) = {-25, 18, -17, -5};
Plane Surface(47) = {46};
Line Loop(48) = {19, -35, 16, 18};
Plane Surface(49) = {48};
Line Loop(50) = {20, -36, 15, 35};
Plane Surface(51) = {50};
Line Loop(52) = {21, -37, 14, 36};
Plane Surface(53) = {52};
Line Loop(54) = {22, 23, 13, 37};
Plane Surface(55) = {54};
Line Loop(56) = {24, 11, 12, -23};
Plane Surface(57) = {56};
Line Loop(58) = {9, -24, -22, -28};
Plane Surface(59) = {58};

Line Loop(60) = {28, -21, -27, 8};
Plane Surface(61) = {60};
Line Loop(62) = {27, -20, -26, 7};
Plane Surface(63) = {62};
Line Loop(64) = {26, -19, -25, 6};
Plane Surface(65) = {64};
Line Loop(66) = {12, 13, -29, -1};
Plane Surface(67) = {66};
Line Loop(68) = {29, 14, -30, -2};
Plane Surface(69) = {68};
Line Loop(70) = {30, 15, -31, -3};
Plane Surface(71) = {70};
Line Loop(72) = {31, 16, 17, -4};
Plane Surface(73) = {72};
Line Loop(74) = {34, 29, 37, -28};
Plane Surface(75) = {74};
Line Loop(76) = {33, 27, -36, -30};
Plane Surface(77) = {76};
Line Loop(78) = {32, 31, 35, -26};
Plane Surface(79) = {78};


/*Surface Loop(80) = {-39, 59, 57, -67, 55, -75};
Volume(81) = {80};
Surface Loop(82) = {-41, 61, 53, -69, 75, 77};
Volume(83) = {82};
Surface Loop(84) = {-43, 63, 51, -71, -77, -79};
Volume(85) = {84};
Surface Loop(86) = {79, 45, 65, 49, 73, 47};
Volume(87) = {86};*/

Surface Loop(80) = {39, 59, 57, 67, 55, 75};
Volume(81) = {80};
Surface Loop(82) = {41, 61, 53, 69, 75, 77};
Volume(83) = {82};
Surface Loop(84) = {43, 63, 51, 71, 77, 79};
Volume(85) = {84};
Surface Loop(86) = {79, 45, 65, 49, 73, 47};
Volume(87) = {86};

//Surface Loop(108) = {39, 59, 57, 67, 55, 41, 61, 53, 69, 43, 63, 51, 71, 45, 65, 49, 73, 47};
//Volume(109) = {108};


// Physical objects to applied BC and material
Physical Point(88) = {20};
Physical Point(89) = {16};
Physical Point(90) = {17};
Physical Point(91) = {18};
Physical Point(92) = {19};
Physical Point(93) = {15};
Physical Point(94) = {14};
Physical Point(95) = {13};
Physical Point(96) = {12};
Physical Point(97) = {11};
Physical Point(98) = {10};
Physical Point(99) = {9};
Physical Point(100) = {8};
Physical Point(101) = {7};
Physical Point(102) = {6};
Physical Point(103) = {5};
Physical Point(104) = {4};
Physical Point(105) = {3};
Physical Point(106) = {2};
Physical Point(107) = {1};

Physical Line(110) = {11};
Physical Line(111) = {1};
Physical Line(112) = {34};
Physical Line(113) = {9};
Physical Line(114) = {2};
Physical Line(115) = {33};
Physical Line(116) = {8};
Physical Line(117) = {3};
Physical Line(118) = {32};
Physical Line(119) = {7};
Physical Line(120) = {4};
Physical Line(121) = {5};
Physical Line(122) = {6};
Physical Line(123) = {25};
Physical Line(124) = {26};
Physical Line(125) = {27};
Physical Line(126) = {28};
Physical Line(127) = {24};
Physical Line(128) = {16};
Physical Line(129) = {35};
Physical Line(130) = {19};
Physical Line(131) = {18};
Physical Line(132) = {15};
Physical Line(133) = {36};
Physical Line(134) = {20};
Physical Line(135) = {14};
Physical Line(136) = {37};
Physical Line(137) = {21};
Physical Line(138) = {13};
Physical Line(139) = {23};
Physical Line(140) = {22};
Physical Line(141) = {12};
Physical Line(142) = {29};
Physical Line(143) = {30};
Physical Line(144) = {31};
Physical Line(145) = {17};
Physical Line(168) = {22};

Physical Surface(239) = {39};
Physical Surface(241) = {41};
Physical Surface(243) = {43};
Physical Surface(245) = {45};
Physical Surface(265) = {65};
Physical Surface(263) = {63};
Physical Surface(261) = {61};
Physical Surface(259) = {59};
Physical Surface(257) = {57};
Physical Surface(275) = {75};
Physical Surface(277) = {77};
Physical Surface(279) = {79};
Physical Surface(247) = {47};
Physical Surface(249) = {49};
Physical Surface(251) = {51};
Physical Surface(253) = {53};
Physical Surface(255) = {55};
Physical Surface(267) = {67};
Physical Surface(269) = {69};
Physical Surface(271) = {71};
Physical Surface(273) = {73};

Physical Volume(167) = {81,83,85,87};



Transfinite Line {1,2,3,4,6,7,8,9,12,13,14,15,16,17,19,20,21,22,24,25,26,27,28,29,30,31} = 3;//3
Transfinite Line {5,11,18,23,32,33,34,35,36,37} = 6;//6
Transfinite Surface {39,41,43,45,47,49,51,53,55,57,59,61,63,65,67,69,71,73,75,77,79} ;
Recombine Surface {39,41,43,45,47,49,51,53,55,57,59,61,63,65,67,69,71,73,75,77,79} ;
Transfinite Volume {81,83,85,87};
