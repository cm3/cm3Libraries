# test file

set(PYFILE twoHole.py)

set(FILES2DELETE 
  *.csv
  disp*
  stress*
  twoHole.msh
)

add_cm3python_mpi_test(4 ${PYFILE} "${FILES2DELETE}")
