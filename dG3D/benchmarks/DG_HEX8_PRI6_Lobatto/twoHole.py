#coding-Utf-8-*-
from gmshpy import *
from dG3Dpy import*

#script to launch beam problem with a python script


lawnum1 = 1 # unique number of law
rho   = 7850
young = 28.9e9
nu    = 0.3 
sy0   = 150.e6
h     = young/50.


# geometry
geofile="twoHole.geo"
meshfile="twoHole.msh" # name of mesh file

# solver
sol = 2  # Gmm=0 (default) Taucs=1 PETsc=2
soltype = 1 # StaticLinear=0 (default) StaticNonLinear=1
nstep = 25   # number of step (used only if soltype=1)
ftime =1.   # Final time (used only if soltype=1)
tol=1.e-6 # relative tolerance for NR scheme (used only if soltype=1)
nstepArch=1 # Number of step between 2 archiving (used only if soltype=1)



harden = LinearExponentialJ2IsotropicHardening(1, sy0, h, 0., 10.)
law1   = J2LinearDG3DMaterialLaw(lawnum1,rho,young,nu,harden)
#law1.setUseBarF(True)


# creation of ElasticField
beta1 = 1e2
fullDG = True;
averageStrainBased = False

myfield1 = dG3DDomain(1000,11,0,lawnum1,fullDG,3)
myfield1.stabilityParameters(beta1)
myfield1.averageStrainBased(averageStrainBased)
# LOBATTO instead of Gauss integration
myfield1.gaussIntegration(1,-1,-1)

#myfield1.matrixByPerturbation(0,1,1,1e-8)


# creation of Solver
mysolver = nonLinearMechSolver(1000)
mysolver.createModel(geofile,meshfile,3,1)
mysolver.addDomain(myfield1)
mysolver.addMaterialLaw(law1)

mysolver.Scheme(soltype)
mysolver.Solver(sol)
mysolver.snlData(nstep,ftime,tol)
mysolver.stepBetweenArchiving(nstepArch)
mysolver.options("-ksp_type preonly -pc_type lu -pc_factor_mat_solver_type mumps")

# BC
#mysolver.displacementBC("Volume",83,2,0.)
mysolver.displacementBC("Face",12,0,0.)
mysolver.displacementBC("Face",12,1,0.)
mysolver.displacementBC("Face",12,2,0.)
mysolver.displacementBC("Face",13,0,0.)
mysolver.displacementBC("Face",13,1,5e-4)
mysolver.displacementBC("Face",13,2,0.)


mysolver.internalPointBuildView("svm",IPField.SVM, 1, 1)
mysolver.internalPointBuildView("sig_xx",IPField.SIG_XX, 1, 1)
mysolver.internalPointBuildView("sig_yy",IPField.SIG_YY, 1, 1)
mysolver.internalPointBuildView("sig_zz",IPField.SIG_ZZ, 1, 1)
mysolver.internalPointBuildView("sig_xy",IPField.SIG_XY, 1, 1)
mysolver.internalPointBuildView("sig_yz",IPField.SIG_YZ, 1, 1)
mysolver.internalPointBuildView("sig_xz",IPField.SIG_XZ, 1, 1)
mysolver.internalPointBuildView("epl",IPField.PLASTICSTRAIN, 1, 1)
mysolver.internalPointBuildView("Damage",IPField.DAMAGE, 1, 1)

mysolver.archivingForceOnPhysicalGroup("Face", 12, 1)


mysolver.solve()

check = TestCheck()
check.equal(-5.699218e+03,mysolver.getArchivedForceOnPhysicalGroup("Face", 12, 1),1.e-4)

