#coding-Utf-8-*-
from gmshpy import *
from dG3Dpy import*

import math
#script to launch beam problem with a python script

# material law
lawnum=num = 1 # unique number of law
#value for 80Pt 20Rh
rho   = 18800.
young =Ex=Ey=Ez =140.e9
nu    =Vxy=Vxz=Vyz= 0.4
MUxy=MUxz=MUyz=Ex/(2.*(1.+Vxy)) 
cv=1.
t0=273+25.
Kx=Ky=Kz=60
alphax=alphay=alphaz=11.e-6
cp=105. #check????

# material law
lawnumCr = numCr= 2 # unique number of law
#value for 80Pt 20Rh
rhoCr   = 7200.
youngCr =ExCr=EyCr=EzCr =130.e9
nuCr    =VxyCr=VxzCr=VyzCr= 0.3
MUxyCr=MUxzCr=MUyzCr=ExCr/(2.*(1.+VxyCr)) 
cvCr=1.
KxCr=KyCr=KzCr=8 #??
alphaxCr=alphayCr=alphazCr=7.5e-6
cpCr=380. #check????

addBlocks=0
coverEffectOnly=1

print(addBlocks)
print(coverEffectOnly)

explicit =0
# geometry
geofile ="supportFull.geo"
meshfile="supportFull.msh"

# solver
if (explicit ==0):
  sol = 2  # Gmm=0 (default) Taucs=1 PETsc=2
  soltype = 1 # StaticLinear=0 (default) StaticNonLinear=1
  nstep = 100   # number of step (used only if soltype=1)# 
  ftime1=0.3333/100. #0.3333   # Final time (used only if soltype=1)
  ftime2=0.6666   # Final time (used only if soltype=1)
  ftime =1.   # Final time (used only if soltype=1)
  tol=1.e-4   # relative tolerance for NR scheme (used only if soltype=1)
  nstepArch=1 # Number of step between 2 archiving (used only if soltype=1)
if (explicit ==1):
  sol = 2  # Gmm=0 (default) Taucs=1 PETsc=2
  soltype = 2 # StaticLinear=0 (default) StaticNonLinear=1
  nstep = 100   # number of step (used only if soltype=1)#
  ftime1=0.3333e-4 #0.3333   # Final time (used only if soltype=1)
  ftime2=0.6666e-4   # Final time (used only if soltype=1)
  ftime =1.e-4   # Final time (used only if soltype=1)
  tol=1.e-3   # relative tolerance for NR scheme (used only if soltype=1)
  nstepArch=3000 # Number of step between 2 archiving (used only if soltype=1)


fullDg = 1 #O = CG, 1 = DG
space1 = 1 # function space (Lagrange=0)
beta1  = 10

alpha = beta=gamma=0.

pressure  = -(2420.*9.81*0.35); #x cm of glass

pressure2 = -(2420.*9.81*0.325); #x cm of glass
tanang    = 0.05005/0.0694; #change with the geometry

preconstraint = -0.000;  #due to bolds

# creation of law

harden1 = LinearExponentialJ2IsotropicHardening(1, 50.e6, 100.e6, 0., 10.);
law1   = J2ThermoMechanicsDG3DMaterialLaw(num,rho,young,nu,harden1,t0,Kx,alphax,cp)
#law1   = LinearThermoMechanicsDG3DMaterialLaw(num,rho,Ex,Ey,Ez,Vxy,Vxz,Vyz,MUxy,MUxz,MUyz,alpha,beta,gamma,t0,Kx,Ky,Kz,alphax,alphay,alphaz,cp)
harden2 = LinearExponentialJ2IsotropicHardening(2, 100.e6, 100.e6, 0., 10.);
law2   = LinearThermoMechanicsDG3DMaterialLaw(numCr,rhoCr,ExCr,EyCr,EzCr,VxyCr,VxzCr,VyzCr,MUxyCr,MUxzCr,MUyzCr,alpha,beta,gamma,t0,KxCr,KyCr,KzCr,alphaxCr,alphayCr,alphazCr,cpCr)
#law2   = J2ThermoMechanicsDG3DMaterialLaw(numCr,rhoCr,youngCr,nuCr,harden2,t0,KxCr,alphaxCr,cpCr)
law1.setUseBarF(True)
law2.setUseBarF(True)


# creation of ElasticField
nfield = 10 # number of the field (physical number of surface)
nfieldCr1 = 11 # number of the field (physical number of surface)
nfieldCr2 = 12 # number of the field (physical number of surface)
myfield1 = ThermoMechanicsDG3DDomain(10,nfield,space1,lawnum,fullDg,1.e6)
myfield1.matrixByPerturbation(0,0,0,1e-8)
myfield1.stabilityParameters(beta1)
myfield1.ThermoMechanicsStabilityParameters(beta1,bool(1))
myfield1.setHierarchicalOrder(0,2)

if (addBlocks > 0):
  myfield2 = ThermoMechanicsDG3DDomain(11,nfieldCr1,space1,lawnumCr,fullDg,1.e6)
  myfield3 = ThermoMechanicsDG3DDomain(12,nfieldCr2,space1,lawnumCr,fullDg,1.e6)

  myfield2.matrixByPerturbation(0,0,0,1e-8)
  myfield2.stabilityParameters(beta1)
  myfield2.ThermoMechanicsStabilityParameters(beta1,bool(1))
  myfield3.matrixByPerturbation(0,0,0,1e-8)
  myfield3.stabilityParameters(beta1)
  myfield3.ThermoMechanicsStabilityParameters(beta1,bool(1))

  myinterfield = ThermoMechanicsInterDomainBetween3D(13,myfield1,myfield2,1.e6,2) #law = 0
  myinterfield.stabilityParameters(beta1)
  myinterfield.ThermoMechanicsStabilityParameters(beta1,bool(1))
  myinterfield2 = ThermoMechanicsInterDomainBetween3D(14,myfield2,myfield3,1.e6,2) #law = 0
  myinterfield2.stabilityParameters(beta1)
  myinterfield2.ThermoMechanicsStabilityParameters(beta1,bool(1))

# creation of Solver
mysolver = nonLinearMechSolver(1000)
mysolver.createModel(geofile,meshfile,3,1)
#mysolver.loadModel(meshfile)
mysolver.addDomain(myfield1)
if ( addBlocks > 0) :
  mysolver.addDomain(myfield2)
  mysolver.addDomain(myfield3)
  mysolver.addDomain(myinterfield)
  mysolver.addDomain(myinterfield2)
mysolver.addMaterialLaw(law1)
mysolver.addMaterialLaw(law2)
mysolver.Scheme(soltype)
mysolver.Solver(sol)
mysolver.snlData(nstep,ftime,tol,1.e-3)
mysolver.explicitSpectralRadius(ftime,0.5,0.)
mysolver.explicitTimeStepEvaluation(nstep)
mysolver.stepBetweenArchiving(nstepArch)
#we cannot use mumps to keep accuracy
mysolver.options("-ksp_type preonly -pc_type lu -pc_factor_mat_solver_type petsc")

#mysolver.options("-ksp_type preonly -pc_type lu")
#mysolver.options("-pc_type jacobI -ksp_gmres_restart 200")
#mysolver.options("-petsc_direct")
# BC

#mechanical BC
#H side
mysolver.displacementBC("Face",101,0,0.)
#mysolver.displacementBC("Face",101,2,0.)
#mysolver.displacementBC("Face",102,0,0.)
#mysolver.displacementBC("Face",102,2,0.)
mysolver.displacementBC("Edge",103,2,0.)

#support side
mysolver.displacementBC("Edge",104,2,0.)
#mysolver.displacementBC("Face",104,2,0.)
#mysolver.displacementBC("Face",104,0,0.)


#symmetry gusset
mysolver.displacementBC("Face",204,1,0.)
#symmetry plate
mysolver.constraintBC("Face",201,1)

#block
cyclicFunctionD=cycleFunctionTime(0., 0., ftime1, preconstraint, ftime2, preconstraint, ftime, preconstraint);
if (addBlocks>0 and coverEffectOnly ==0):
#cannot be constraint along y because of the y symmetry BC
  #mysolver.constraintBC("Volume",nfieldCr1,0)
  #mysolver.constraintBC("Volume",nfieldCr2,0)
  mysolver.displacementBC("Volume",nfieldCr1,0,0.)
  mysolver.displacementBC("Volume",nfieldCr2,0,0.)
  mysolver.displacementBC("Face",308,2,cyclicFunctionD)


if ( addBlocks == 0 and coverEffectOnly ==0) :
  mysolver.displacementBC("Edge",308,2,cyclicFunctionD)
  #mysolver.displacementBC("Edge",308,0,cyclicFunctionD)

if ( coverEffectOnly == 1) :
  SigPl=50.e6;
  cyclicFunctionEplx=cycleFunctionTime(0., 0., ftime1, 0., ftime2, SigPl*math.cos(math.atan(tanang)), ftime, SigPl*math.cos(math.atan(tanang)));
  cyclicFunctionEplz=cycleFunctionTime(0., 0., ftime1, 0., ftime2, -1.*SigPl*math.sin(math.atan(tanang)), ftime, -1.*SigPl*math.sin(math.atan(tanang)));
  mysolver.forceBC("Face",309,0,cyclicFunctionEplx)
  mysolver.forceBC("Face",309,2,cyclicFunctionEplz)



#loading
cyclicFunctionP=cycleFunctionTime(0., 0., ftime1, 0., ftime2, 0., ftime, pressure);
mysolver.forceBC("Face",301,2,cyclicFunctionP)
mysolver.forceBC("Face",303,0,cyclicFunctionP)

cyclicFunctionP2=cycleFunctionTime(0., 0., ftime1, 0., ftime2, 0., ftime, pressure2);
cyclicFunctionP2x=cycleFunctionTime(0., 0., ftime1, 0., ftime2, 0., ftime, -1.*pressure2*math.sin(math.atan(tanang)));
cyclicFunctionP2z=cycleFunctionTime(0., 0., ftime1, 0., ftime2, 0., ftime, -1.*pressure2*math.cos(math.atan(tanang)));
#mysolver.pressureOnPhysicalGroupBC("Face",302,cyclicFunctionP2) #one direction is enough
mysolver.forceBC("Face",302,2,cyclicFunctionP2z)
mysolver.forceBC("Face",302,0,cyclicFunctionP2x)

#thermal BC
cyclicFunctionTemp1=cycleFunctionTime(0., t0, ftime1, t0, ftime2, t0+1350., ftime, t0+1350);
cyclicFunctionTempCr1=cycleFunctionTime(0., t0, ftime1, t0, ftime2, t0+1000., ftime, t0+1000);
cyclicFunctionTempCr2=cycleFunctionTime(0., t0, ftime1, t0, ftime2, t0+400., ftime, t0+400);
mysolver.displacementBC("Volume",nfield,3,cyclicFunctionTemp1)
if (addBlocks>0):
  mysolver.displacementBC("Volume",nfieldCr1,3,cyclicFunctionTempCr1)
  mysolver.displacementBC("Volume",nfieldCr2,3,cyclicFunctionTempCr2)

mysolver.internalPointBuildView("svm",IPField.SVM, 1, 1)
mysolver.internalPointBuildView("sig_xx",IPField.SIG_XX, 1, 1)
mysolver.internalPointBuildView("sig_yy",IPField.SIG_YY, 1, 1)
mysolver.internalPointBuildView("sig_zz",IPField.SIG_ZZ, 1, 1)
mysolver.internalPointBuildView("sig_xy",IPField.SIG_XY, 1, 1)
mysolver.internalPointBuildView("sig_yz",IPField.SIG_YZ, 1, 1)
mysolver.internalPointBuildView("sig_xz",IPField.SIG_XZ, 1, 1)
mysolver.internalPointBuildView("temperature",IPField.TEMPERATURE, 1, 1)
mysolver.internalPointBuildView("qx",IPField.THERMALFLUX_X, 1, 1)
mysolver.internalPointBuildView("qy",IPField.THERMALFLUX_Y, 1, 1)
mysolver.internalPointBuildView("qz",IPField.THERMALFLUX_Z, 1, 1)
mysolver.internalPointBuildView("epl",IPField.PLASTICSTRAIN, 1, 1)
mysolver.archivingForceOnPhysicalGroup("Face", 101, 2)
mysolver.archivingForceOnPhysicalGroup("Face", 102, 2)
mysolver.archivingForceOnPhysicalGroup("Edge", 103, 2)
mysolver.archivingForceOnPhysicalGroup("Face", 104, 2)
mysolver.archivingForceOnPhysicalGroup("Face", 104, 0)
#mysolver.archivingForceOnPhysicalGroup("Face", 308, 2)
mysolver.archivingForceOnPhysicalGroup("Face", 101, 0)
mysolver.archivingForceOnPhysicalGroup("Face", 201, 1)
#mysolver.archivingForceOnPhysicalGroup("Volume", nfieldCr1, 0)
#mysolver.archivingForceOnPhysicalGroup("Volume", nfieldCr2, 0)
mysolver.archivingNodeDisplacement(111,2)
mysolver.archivingNodeDisplacement(131,2)
mysolver.archivingNodeDisplacement(151,0)
mysolver.archivingNodeDisplacement(151,2)
mysolver.archivingIPOnPhysicalGroup("Volume",nfield, IPField.SVM,IPField.MAX_VALUE);


mysolver.solve()

check = TestCheck()
check.equal( 2.199592e+00,mysolver.getArchivedForceOnPhysicalGroup("Face", 101, 0),1.e-5)
check.equal(2.013292e-03,mysolver.getArchivedNodalValue(151,0,mysolver.displacement),1.e-5)

