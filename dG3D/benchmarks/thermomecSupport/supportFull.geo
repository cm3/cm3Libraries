//support

addFirstFlange = 0 ;
addSecondFlange = 0;
addThirdFlange = 1 ;
addLowerSup = 1 ;
addUpperSup = 0 ;
addWebSup = 0 ;
gableGusset = 0;
addCover = 0;
addBlocks = 0;
addRefincorce =0; // 0 (no reinf), 1 (guset side), 2 (Hside)
addCoverEffectOnly =0;

If(addCoverEffectOnly>0)
  addCover = 1;
EndIf

mm = 1.e-3;
inch = 0.0254;

L  = 9.65*inch/2.; //9.21*inch/2.; //300*mm;    // length
LH = 0.25*inch; //30*mm;     // length of the H support
Lc = 1.75*inch; // distance of cover hole
tp = 0.06*inch; //0.05*inch; //1.5*mm;    // thickness of the plate
b  = 0.918*inch; //0.87*inch; //60*mm;     // width of the plate
te = 0.06*inch; //0.05*inch; //1.5*mm;     // thickness of the left/right flange
tc = 0.025*inch; // thickness of the cover
tmissing = 0.015*inch; //complete to avoid zeo thickness at cover junctions

hfl1  = 0.48*inch; //30*mm;    // left height of the flange at support 2
hfl2  = 0.9*inch; //0.76*inch; //45*mm;    // left height of the flange at web
hfl3  = 0.98*inch; //0.86*inch;    // left height of the flange at support 2

hfr1 = 0.48*inch; //30*mm;    // right height of the flange at support 1
hfr2 = 0.55*inch; //45*mm;    // right height of the flange at web
hfr3 = 0.98*inch; //25*mm; //1.2*inch; //60*mm;    // right height of the flange at web 2
hfrc = 2.5*inch; // right position of cover
hfrtop = hfrc+1.15*inch; // right position of cover
hfrCr1 = hfrtop+50*mm; // right position of cover
hfrCr2 = hfrCr1+75*mm; // right position of cover
DzCover =0;// 0.5*inch; // kink in the cover


//mid-position of the gusset
hfm1 = (hfr1+hfl1)/2.;
hfm2 = (hfr2+hfl2)/2.; //45*mm;    // right height of the flange at web
hfm3 = (hfr3+hfl3)/2.; //1.2*inch; //60*mm;    // right height of the flange at web 2
If (gableGusset==1)
  hfm1 = (hfr1+hfl1)/2.;
  hfm2 = 1.05*inch; //45*mm;    // right height of the flange at web
  hfm3 = hfm2+((hfr3+hfl3-hfr2-hfl2)/2.); //1.2*inch; //60*mm;    // right height of the flange at web 2
EndIf

tf   = 0.05*inch; //2.*mm;     // thickness of gusset web
tg1 = 0.05*inch; //2.*mm;      // thickness of the support 1
tg2 = 0.05*inch; //2.*mm;      // thickness of the support 2/gusset flange 1
tg3 = 0.05*inch; //2.*mm;      // thickness of the gusset flange 2

Lsu  = 0.47*inch; //30*mm;    // support

gf   = 0.3*inch; //8*mm;    //width of gusset flange


supCover = 3*tp; //Lsu/2.; //3*tp; // extra part for corver attach


CLp = 0.05*inch; //10*mm;
CLf = 0.5*inch; //1.*mm;
//number element plate
elL   = 22; //33;//should be odd to keep the physical node
elb   = 7;
eltp  = 5;

//number element flange
elte = 2;//4;
elhfl = 5;//7;
elhfr1 = 5;//7;
elhfr2 = 3;
elhfr3 = 4;
elteg3 = 4; //eleement on the thickness of third gousset
elReinf = 2;

//cover
If (addCover >0 && addThirdFlange==0)
  tg3 = tmissing; // high enough to make sure FE are good
  hfl3 = hfl2+tg3+2.*tmissing;
  If (addRefincorce==0)
    elhfr3 = 2;
    elteg3 = 2;
  EndIf
  If (addRefincorce >0)
    elReinf = 6;
  EndIf
EndIf
dec = tc/Cos(Atan((hfrc-hfl3)/(L-Lc)));
hflc=hfl3+2.*tmissing+dec;
eltfc = 13;
//block cr
elcr=3;

//number element gusset
elgf = 4;

//number support
elLsu = 7;

//nember element cover
eltc = 3;//4;


//upper skin of the plate

For i In {1:2}
  Point ((i-1)*100+1)  = { L-2.*te , 0.0       , (1.-i)*tp,   CLp};
  Point ((i-1)*100+2)  = { L-2.*te , (b-tf)/2.-gf/2 , (1.-i)*tp,   CLf};
  Point ((i-1)*100+3)  = { L-2.*te , (b-tf)/2.,  (1.-i)*tp,   CLf};
  Point ((i-1)*100+4)  = { L-2.*te ,  b/2        , (1.-i)*tp,   CLp};
  Point ((i-1)*100+5)  = { LH ,    b/2      , (1.-i)*tp,   CLp};
  Point ((i-1)*100+6)  = { LH , (b-tf)/2. , (1.-i)*tp,   CLf};
  Point ((i-1)*100+7)  = { LH , (b-tf)/2.-gf/2 , (1.-i)*tp,   CLf};
  Point ((i-1)*100+8)  = { LH , 0.0       , (1.-i)*tp,   CLp};
  Point ((i-1)*100+31) = { (L-2.*te)/2. , 0.0       , (1.-i)*tp,   CLp};
  Point ((i-1)*100+32) = { (L-2.*te)/2. , (b-tf)/2.-gf/2       , (1.-i)*tp,   CLp};
  Point ((i-1)*100+33) = { (L-2.*te)/2. , (b-tf)/2.       , (1.-i)*tp,   CLp};
  Point ((i-1)*100+34)  = { (L-2.*te)/2. ,  b/2        , (1.-i)*tp,   CLp};

  Point ((i-1)*100+11)  = { L , 0.0       , (1.-i)*tp,   CLf};
  Point ((i-1)*100+12)  = { L , (b-tf)/2.-gf/2 , (1.-i)*tp,   CLf};
  Point ((i-1)*100+13)  = { L , (b-tf)/2. , (1.-i)*tp,   CLf};
  Point ((i-1)*100+14)  = { L ,  b/2.        , (1.-i)*tp,   CLf};
  Point ((i-1)*100+15)  = { LH-te/Sin(Atan(hfr1/LH)) ,    b/2      , (1.-i)*tp,   CLp};
  Point ((i-1)*100+16)  = { LH-te/Sin(Atan(hfr1/LH)) , (b-tf)/2. , (1.-i)*tp,   CLf};
  Point ((i-1)*100+17)  = { LH-te/Sin(Atan(hfr1/LH)) , (b-tf)/2.-gf/2 , (1.-i)*tp,   CLf};
  Point ((i-1)*100+18)  = { LH-te/Sin(Atan(hfr1/LH)) , 0.0       , (1.-i)*tp,   CLp};


  Line((i-1)*100+1)    = {(i-1)*100+1, (i-1)*100+2};
  Line((i-1)*100+2)    = {(i-1)*100+2, (i-1)*100+3};
  Line((i-1)*100+3)    = {(i-1)*100+3, (i-1)*100+4};
  Line((i-1)*100+4)    = {(i-1)*100+34, (i-1)*100+5};
  Line((i-1)*100+5)    = {(i-1)*100+5, (i-1)*100+6};
  Line((i-1)*100+6)    = {(i-1)*100+6, (i-1)*100+7};
  Line((i-1)*100+7)    = {(i-1)*100+7, (i-1)*100+8};
  Line((i-1)*100+8)    = {(i-1)*100+8, (i-1)*100+31};
  Line((i-1)*100+9)    = {(i-1)*100+6, (i-1)*100+33};
  Line((i-1)*100+10)   = {(i-1)*100+7, (i-1)*100+32};
  Line((i-1)*100+31)   = {(i-1)*100+31, (i-1)*100+32};
  Line((i-1)*100+32)   = {(i-1)*100+32, (i-1)*100+33};
  Line((i-1)*100+33)   = {(i-1)*100+33, (i-1)*100+34};
  Line((i-1)*100+34)   = {(i-1)*100+31, (i-1)*100+1};
  Line((i-1)*100+35)   = {(i-1)*100+32, (i-1)*100+2};
  Line((i-1)*100+36)   = {(i-1)*100+33, (i-1)*100+3};
  Line((i-1)*100+37)   = {(i-1)*100+34, (i-1)*100+4};

  Line((i-1)*100+11)   = {(i-1)*100+11, (i-1)*100+12};
  Line((i-1)*100+12)   = {(i-1)*100+12, (i-1)*100+13};
  Line((i-1)*100+13)   = {(i-1)*100+13, (i-1)*100+14};
  Line((i-1)*100+14)   = {(i-1)*100+1,  (i-1)*100+11};
  Line((i-1)*100+15)   = {(i-1)*100+2,  (i-1)*100+12};
  Line((i-1)*100+16)   = {(i-1)*100+3,  (i-1)*100+13};
  Line((i-1)*100+17)   = {(i-1)*100+4,  (i-1)*100+14};
 
  Line((i-1)*100+18)    = {(i-1)*100+5,  (i-1)*100+15};
  Line((i-1)*100+19)    = {(i-1)*100+6,  (i-1)*100+16};
  Line((i-1)*100+20)    = {(i-1)*100+7,  (i-1)*100+17};
  Line((i-1)*100+21)    = {(i-1)*100+8,  (i-1)*100+18};
  Line((i-1)*100+22)    = {(i-1)*100+15, (i-1)*100+16};
  Line((i-1)*100+23)    = {(i-1)*100+16, (i-1)*100+17};
  Line((i-1)*100+24)    = {(i-1)*100+17, (i-1)*100+18};

  Line Loop((i-1)*100+1) = {(i-1)*100+1,-((i-1)*100+35), -((i-1)*100+31), (i-1)*100+34};
  Line Loop((i-1)*100+31) = {(i-1)*100+31,-((i-1)*100+10), (i-1)*100+7, (i-1)*100+8};
  Line Loop((i-1)*100+2) = {(i-1)*100+2, -((i-1)*100+36), -((i-1)*100+32), (i-1)*100+35};
  Line Loop((i-1)*100+32) = {(i-1)*100+32, -((i-1)*100+9), (i-1)*100+6, (i-1)*100+10};
  Line Loop((i-1)*100+3) = {(i-1)*100+3, -((i-1)*100+37), -((i-1)*100+33), (i-1)*100+36};
  Line Loop((i-1)*100+33) = {(i-1)*100+33, (i-1)*100+4, (i-1)*100+5, (i-1)*100+9};

  Line Loop((i-1)*100+4) = {(i-1)*100+1, (i-1)*100+15, -((i-1)*100+11), -((i-1)*100+14)};
  Line Loop((i-1)*100+5) = {(i-1)*100+2, (i-1)*100+16, -((i-1)*100+12), -((i-1)*100+15)};
  Line Loop((i-1)*100+6) = {(i-1)*100+3, (i-1)*100+17, -((i-1)*100+13), -((i-1)*100+16)};
  Line Loop((i-1)*100+7) = {(i-1)*100+5, (i-1)*100+19, -((i-1)*100+22), -((i-1)*100+18)};
  Line Loop((i-1)*100+8) = {(i-1)*100+6, (i-1)*100+20, -((i-1)*100+23), -((i-1)*100+19)};
  Line Loop((i-1)*100+9) = {(i-1)*100+7, (i-1)*100+21, -((i-1)*100+24), -((i-1)*100+20)};


  Plane Surface((i-1)*100+1) = {(i-1)*100+1};
  Plane Surface((i-1)*100+31) = {(i-1)*100+31};
  Plane Surface((i-1)*100+2) = {(i-1)*100+2};
  Plane Surface((i-1)*100+32) = {(i-1)*100+32};
  Plane Surface((i-1)*100+3) = {(i-1)*100+3};
  Plane Surface((i-1)*100+33) = {(i-1)*100+33};

  Plane Surface((i-1)*100+4) = {(i-1)*100+4};
  Plane Surface((i-1)*100+5) = {(i-1)*100+5};
  Plane Surface((i-1)*100+6) = {(i-1)*100+6};
 
  Plane Surface((i-1)*100+7) = {(i-1)*100+7};
  Plane Surface((i-1)*100+8) = {(i-1)*100+8};
  Plane Surface((i-1)*100+9) = {(i-1)*100+9};

EndFor

//complete the plate
For i In {1:8}
  Line(200+i) = {i,100+i};
EndFor
For i In {31:34}
  Line(200+i) = {i,100+i};
EndFor
For i In {11:18}
  Line(200+i) = {i,100+i};
EndFor

//front part of the plate
Line Loop(201) = {1,202,-101,-201};
Line Loop(202) = {2,203,-102,-202};
Line Loop(203) = {3,204,-103,-203};
Line Loop(211) = {11,212,-111,-211};
Line Loop(212) = {12,213,-112,-212};
Line Loop(213) = {13,214,-113,-213};

For i In {201:203} 
  Plane Surface(i) = {i};
EndFor
For i In {211:213} 
  Plane Surface(i) = {i};
EndFor

//back part of the plate
Line Loop(205) = {5,206,-105,-205};
Line Loop(206) = {6,207,-106,-206};
Line Loop(207) = {7,208,-107,-207};
Line Loop(225) = {22,216,-122,-215};
Line Loop(226) = {23,217,-123,-216};
Line Loop(227) = {24,218,-124,-217};


For i In {205:207} 
  Plane Surface(i) = {i};
EndFor
For i In {225:227} 
  Plane Surface(i) = {i};
EndFor

//back midline of the plate
Line Loop(231) = {31,232,-131,-231};
Line Loop(232) = {32,233,-132,-232};
Line Loop(233) = {33,234,-133,-233};

For i In {231:233} 
  Plane Surface(i) = {i};
EndFor
//latteral surfaces of the plate
Line Loop(301) = {34,201,-134,-231};
Line Loop(311) = {14,211,-114,-201};
Line Loop(321) = {21,218,-121,-208};
Line Loop(331) = {8,231,-108,-208};
Plane Surface(301) = {301};
Plane Surface(311) = {311};
Plane Surface(321) = {321};
Plane Surface(331) = {331};

//midsurface of the plate
Line Loop(302) = {35,202,-135,-232};
Line Loop(312) = {15,212,-115,-202};
Line Loop(322) = {20,217,-120,-207};
Line Loop(332) = {10,232,-110,-207};
Plane Surface(302) = {302};
Plane Surface(312) = {312};
Plane Surface(322) = {322};
Plane Surface(332) = {332};
Line Loop(303) = {36,203,-136,-233};
Line Loop(313) = {16,213,-116,-203};
Line Loop(323) = {19,216,-119,-206};
Line Loop(333) = {9,233,-109,-206};
Plane Surface(303) = {303};
Plane Surface(313) = {313};
Plane Surface(323) = {323};
Plane Surface(333) = {333};

//latteral surfaces of the plate
Line Loop(304) = {-37,234,137,-204};
Line Loop(314) = {17,214,-117,-204};
Line Loop(324) = {18,215,-118,-205};
Line Loop(334) = {4,205,-104,-234};
Plane Surface(304) = {304};
Plane Surface(314) = {314};
Plane Surface(324) = {324};
Plane Surface(334) = {334};

// left flange
For i In {1:8}
  j=i*1000;
  jm1=(i-1)*1000;
  posCover = 0.;
  zfl=hfl1-(2.-i)*tg1;
  If (i>2)
    zfl=hfl2-(4.-i)*tg2;
    If (addCover>0 && i>3)
      posCover = supCover;
    EndIf
    If (i>4)
      zfl=hfl3-(6.-i)*tg3;
    EndIf
    If (i>6)
      zfl=hflc-(8-i)*dec;
    EndIf
  EndIf
  Point (j+1)  = { L-2.*te , 0.0       , zfl,   CLp};
  Point (j+2)  = { L-2.*te , (b-tf)/2.-gf/2 , zfl,   CLf};
  Point (j+3)  = { L-2.*te , (b-tf)/2.    , zfl,   CLf};
  Point (j+4)  = { L-2.*te , (b)/2.    , zfl,   CLf};
  Point (j+11)  = { L+posCover , 0.0       , zfl,   CLp};
  Point (j+12)  = { L+posCover , (b-tf)/2-gf/2 , zfl,   CLf};
  Point (j+13)  = { L+posCover , (b-tf)/2.    , zfl,   CLf}; 
  Point (j+14)  = { L+posCover , (b)/2.    , zfl,   CLf};

  Line  (j+1)  = {j+1, j+2};
  Line  (j+2)  = {j+2, j+3};
  Line  (j+3)  = {j+3, j+4};
  Line  (j+11)  = {j+11, j+12};
  Line  (j+12)  = {j+12, j+13};
  Line  (j+13)  = {j+13, j+14};
  Line  (j+14)  = {j+1, j+11};
  Line  (j+15)  = {j+2, j+12};
  Line  (j+16)  = {j+3, j+13};
  Line  (j+17)  = {j+4, j+14};
  Line  (j+201)  = {jm1+1, j+1};
  Line  (j+202)  = {jm1+2, j+2};
  Line  (j+203)  = {jm1+3, j+3};
  Line  (j+204)  = {jm1+4, j+4};
  Line  (j+211)  = {jm1+11, j+11};
  Line  (j+212)  = {jm1+12, j+12};
  Line  (j+213)  = {jm1+13, j+13};
  Line  (j+214)  = {jm1+14, j+14};
  //
  Line Loop(j+4) = {j+1,j+15,-(j+11),-(j+14)};
  Line Loop(j+5) = {j+2,j+16,-(j+12),-(j+15)};
  Line Loop(j+6) = {j+3,j+17,-(j+13),-(j+16)};
  Line Loop(j+201) = {jm1+1,j+202,-(j+1),-(j+201)};
  Line Loop(j+202) = {jm1+2,j+203,-(j+2),-(j+202)};
  Line Loop(j+203) = {jm1+3,j+204,-(j+3),-(j+203)};
  Line Loop(j+211) = {jm1+11,j+212,-(j+11),-(j+211)};
  Line Loop(j+212) = {jm1+12,j+213,-(j+12),-(j+212)};
  Line Loop(j+213) = {jm1+13,j+214,-(j+13),-(j+213)};
  Line Loop(j+311) = {jm1+14,j+211,-(j+14),-(j+201)};
  Line Loop(j+312) = {jm1+15,j+212,-(j+15),-(j+202)};
  Line Loop(j+313) = {jm1+16,j+213,-(j+16),-(j+203)};
  Line Loop(j+314) = {jm1+17,j+214,-(j+17),-(j+204)};
  //
  Plane Surface(j+4) = {j+4};
  Plane Surface(j+5) = {j+5};
  Plane Surface(j+6) = {j+6};
  Plane Surface(j+201) = {j+201};
  Plane Surface(j+202) = {j+202};
  Plane Surface(j+203) = {j+203};
  Plane Surface(j+211) = {j+211};
  Plane Surface(j+212) = {j+212};
  Plane Surface(j+213) = {j+213};
  Plane Surface(j+311) = {j+311};
  Plane Surface(j+312) = {j+312};
  Plane Surface(j+313) = {j+313};
  Plane Surface(j+314) = {j+314};
EndFor
//support left flange
For i In {1:4}
  j=i*1000;
  jm1=(i-1)*1000;
  zfl=hfl1-(2.-i)*tg1;
  If (i>2)
    zfl=hfl2-(4.-i)*tg2;
  EndIf
  Point (j+51)  = { L+Lsu , 0.0       , zfl,   CLp};
  Point (j+52)  = { L+Lsu , (b-tf)/2.-gf/2 , zfl,   CLf};
  Point (j+53)  = { L+Lsu , (b-tf)/2.    , zfl,   CLf};
  Point (j+54)  = { L+Lsu , (b)/2.    , zfl,   CLf};

  Line  (j+51)  = {j+51, j+52};
  Line  (j+52)  = {j+52, j+53};
  Line  (j+53)  = {j+53, j+54};
  Line  (j+54)  = {j+11, j+51};
  Line  (j+55)  = {j+12, j+52};
  Line  (j+56)  = {j+13, j+53};
  Line  (j+57)  = {j+14, j+54};
  If (i>1)
    Line  (j+251)  = {jm1+51, j+51};
    Line  (j+252)  = {jm1+52, j+52};
    Line  (j+253)  = {jm1+53, j+53};
    Line  (j+254)  = {jm1+54, j+54};
  EndIf
  //
  Line Loop(j+54) = {j+51,-(j+55),-(j+11),(j+54)};
  Line Loop(j+55) = {j+52,-(j+56),-(j+12),(j+55)};
  Line Loop(j+56) = {j+53,-(j+57),-(j+13),(j+56)};
  //
  Plane Surface(j+54) = {j+54};
  Plane Surface(j+55) = {j+55};
  Plane Surface(j+56) = {j+56};
  If (i>1)
    Line Loop(j+251) = {jm1+51,j+252,-(j+51),-(j+251)};
    Line Loop(j+252) = {jm1+52,j+253,-(j+52),-(j+252)};
    Line Loop(j+253) = {jm1+53,j+254,-(j+53),-(j+253)};
    Plane Surface(j+251) = {j+251};
    Plane Surface(j+252) = {j+252};
    Plane Surface(j+253) = {j+253};
    Line Loop(j+351) = {jm1+54,j+251,-(j+54),-(j+211)};
    Line Loop(j+352) = {jm1+55,j+252,-(j+55),-(j+212)};
    Line Loop(j+353) = {jm1+56,j+253,-(j+56),-(j+213)};
    Line Loop(j+354) = {jm1+57,j+254,-(j+57),-(j+214)};
    Plane Surface(j+351) = {j+351};
    Plane Surface(j+352) = {j+352};
    Plane Surface(j+353) = {j+353};
    Plane Surface(j+354) = {j+354};
  EndIf
EndFor

//right flange
For i In {1:12}
  j=i*1000;
  jm1=(i-1)*1000;
  de=te/Sin(Atan(hfr2/LH));
  xfr = LH*(1-hfr1/hfr2);
  zfr =  hfr1-(2.-i)*tg1;
  If (i>1)
    xfr = LH*(1-(hfr1+tg1)/hfr2);
  EndIf
  If (i>2)
    xfr = 0;
    zfr =  hfr2-(4.-i)*tg2;
  EndIf
  If (i>4)
    xfr = 0;
    zfr =  hfr3-(6.-i)*tg3;
  EndIf
  If (i>6)
    xfr = Lc;
    de  = tc;
    zfr =  hfrc-(8.-i)*dec;
    If (i>8)
      zfr =  hfrtop-(10-i)*tc;
    EndIf
    If (i>10)
      zfr =  hfrCr1;
    EndIf
    If (i>11)
      zfr =  hfrCr2;
    EndIf
  EndIf
  Point (j+5)  = { xfr+de, (b)/2. ,zfr,   CLf};
  Point (j+6)  = { xfr+de , (b-tf)/2. ,zfr,   CLf};
  Point (j+7)  = { xfr+de , (b-tf)/2.-gf/2 , zfr,   CLf};
  Point (j+8)  = { xfr+de , 0.0       , zfr,   CLp};
  Point (j+15)  = { xfr , (b)/2. , zfr,   CLf};
  Point (j+16)  = { xfr , (b-tf)/2. , zfr,   CLf};
  Point (j+17)  = { xfr , (b-tf)/2.-gf/2 , zfr,   CLf};
  Point (j+18)  = { xfr , 0.0       , zfr,   CLp};

  Line  (j+5)  = {j+5, j+6};
  Line  (j+6)  = {j+6, j+7};
  Line  (j+7)  = {j+7, j+8};
  Line  (j+22)  = {j+15, j+16};
  Line  (j+23)  = {j+16, j+17};
  Line  (j+24)  = {j+17, j+18};
  Line  (j+18)  = {j+5, j+15};
  Line  (j+19)  = {j+6, j+16};
  Line  (j+20)  = {j+7, j+17};
  Line  (j+21)  = {j+8, j+18};
  Line  (j+205)  = {jm1+5, j+5};
  Line  (j+206)  = {jm1+6, j+6};
  Line  (j+207)  = {jm1+7, j+7};
  Line  (j+208)  = {jm1+8, j+8};
  Line  (j+215)  = {jm1+15, j+15};
  Line  (j+216)  = {jm1+16, j+16};
  Line  (j+217)  = {jm1+17, j+17};
  Line  (j+218)  = {jm1+18, j+18};
  Line Loop(j+7) = {j+5,j+19,-(j+22),-(j+18)};
  Line Loop(j+8) = {j+6,j+20,-(j+23),-(j+19)};
  Line Loop(j+9) = {j+7,j+21,-(j+24),-(j+20)};
  Line Loop(j+205) = {jm1+5,j+206,-(j+5),-(j+205)};
  Line Loop(j+206) = {jm1+6,j+207,-(j+6),-(j+206)};
  Line Loop(j+207) = {jm1+7,j+208,-(j+7),-(j+207)};
  Line Loop(j+225) = {jm1+22,j+216,-(j+22),-(j+215)};
  Line Loop(j+226) = {jm1+23,j+217,-(j+23),-(j+216)};
  Line Loop(j+227) = {jm1+24,j+218,-(j+24),-(j+217)};
  Line Loop(j+321) = {jm1+21,j+218,-(j+21),-(j+208)};
  Line Loop(j+322) = {jm1+20,j+217,-(j+20),-(j+207)};
  Line Loop(j+323) = {jm1+19,j+216,-(j+19),-(j+206)};
  Line Loop(j+324) = {jm1+18,j+215,-(j+18),-(j+205)};
  Plane Surface(j+7) = {j+7};
  Plane Surface(j+8) = {j+8};
  Plane Surface(j+9) = {j+9};
  Plane Surface(j+205) = {j+205};
  Plane Surface(j+206) = {j+206};
  Plane Surface(j+207) = {j+207};
  Plane Surface(j+225) = {j+225};
  Plane Surface(j+226) = {j+226};
  Plane Surface(j+227) = {j+227};
  Plane Surface(j+321) = {j+321};
  Plane Surface(j+322) = {j+322};
  Plane Surface(j+323) = {j+323};
  Plane Surface(j+324) = {j+324};
EndFor

//gusset
For i In {1:8}
  j=i*1000;
  jm1=(i-1)*1000;
  xfm = (L-te)/2.;
  zfm=hfm1-(2.-i)*tg1;
  If (i>2)
    zfm=hfm2-(4.-i)*tg2;
  EndIf
  If (i>4)
    zfm=hfm3-(6.-i)*tg3;
  EndIf
  If (i>6)
    xfm=Lc+(L-Lc)/2.;
    zfm=hfrc+(hflc-hfrc)/(L-Lc)*(L/2.-Lc/2.)-(8.-i)*dec-DzCover;
  EndIf
  Point (j+31) = { xfm , 0.0       , zfm,   CLp};
  Point (j+32) = { xfm , (b-tf)/2.-gf/2       , zfm,   CLp};
  Point (j+33) = { xfm , (b-tf)/2.       , zfm,   CLp};
  Point (j+34)  = { xfm,  b/2        , zfm,   CLp};
  Line(j+401) = {j+1,j+31};
  Line(j+431) = {j+31,j+8};
  Line(j+402) = {j+2,j+32};
  Line(j+432) = {j+32,j+7};
  Line(j+403) = {j+3,j+33};
  Line(j+433) = {j+33,j+6};
  Line(j+404) = {j+4,j+34};
  Line(j+434) = {j+34,j+5};
  For k In {31:34}
    Line(j+200+k) = {jm1+k,j+k};
    If (k<34)
      Line(j+k)   = {j+k, j+k+1};
    EndIf
  EndFor
EndFor
For i In {1:8}
  j=i*1000;
  jm1=(i-1)*1000;
  Line Loop(j+1) = {j+1, j+402, -(j+31), -(j+401)};
  Line Loop(j+31) = {j+31, j+432, j+7, -(j+431)};
  Line Loop(j+2) = {j+2, j+403, -(j+32), -(j+402)};
  Line Loop(j+32) = {j+32, j+433, j+6, -(j+432)};
  Line Loop(j+3) = {j+3, j+404, -(j+33), -(j+403)};
  Line Loop(j+33) = {j+33, j+434, j+5, -(j+433)};
  Plane Surface(j+1) = {j+1};
  Plane Surface(j+31) = {j+31};
  Plane Surface(j+2) = {j+2};
  Plane Surface(j+32) = {j+32};
  Plane Surface(j+3) = {j+3};
  Plane Surface(j+33) = {j+33};
  If (i==1)
    Line Loop(j+303) = {j+403, -(j+233),  36, j+203};
    Line Loop(j+333) = {j+433, -(j+206),  9, j+233};
    Line Loop(j+304) = {j+404, -(j+234), 37, j+204};
    Line Loop(j+334) = {j+434, -(j+205), -4, j+234};
  EndIf
  If (i>1)
    Line Loop(j+303) = {j+403, -(j+233), -(jm1+403), j+203};
    Line Loop(j+333) = {j+433, -(j+206), -(jm1+433), j+233};
    Line Loop(j+304) = {j+404, -(j+234), -(jm1+404), j+204};
    Line Loop(j+334) = {j+434, -(j+205), -(jm1+434), j+234};
    Line Loop(j+302) = {j+402, -(j+232), -(jm1+402), j+202};
    Line Loop(j+332) = {j+432, -(j+207), -(jm1+432), j+232};
    Line Loop(j+301) = {j+401, -(j+231), -(jm1+401), j+201};
    Line Loop(j+331) = {j+431, -(j+208), -(jm1+431), j+231};
    Plane Surface(j+301) = {j+301};
    Plane Surface(j+331) = {j+331};
    Plane Surface(j+302) = {j+302};
    Plane Surface(j+332) = {j+332};
  EndIf
  Plane Surface(j+303) = {j+303};
  Plane Surface(j+304) = {j+304};
  Plane Surface(j+333) = {j+333};
  Plane Surface(j+334) = {j+334};
//midside
  Line Loop(j+231) = {jm1+31,j+232,-(j+31),-(j+231)};
  Line Loop(j+232) = {jm1+32,j+233,-(j+32),-(j+232)};
  Line Loop(j+233) = {jm1+33,j+234,-(j+33),-(j+233)};
  Plane Surface(j+231) = {j+231};
  Plane Surface(j+232) = {j+232};
  Plane Surface(j+233) = {j+233};
EndFor

//element on the plate
Transfinite Line {4,8,9,10,34,35,36,37,104,108,109,110,134,135,136,137} = elL/2;
Transfinite Line {1,7,11,24,101,106,107,111,124,31,131,1031,2031,3031} =elb;
Transfinite Line {4031,5031,6031,7031,8031} = elb;
Transfinite Line {201,202,203,204,205,206,207,208,211,231,232,233,234}=eltp;
Transfinite Line {214,215,216,217,218,211,212,213} =eltp;

//element on the flange
Transfinite Line {2,6,102,106,32,132,12,112,1002,1012,2002,2012,1032,2032,3032,4032,5032,6032,7032,8032}= elgf;
Transfinite Line {3002,3012,4002,4012}= elgf;
Transfinite Line {1052,2052,3052,4052}= elgf;
Transfinite Line {5002,5012,6002,6012}= elgf;
Transfinite Line {7002,7012,8002,8012}= elgf;
Transfinite Line {23,123,1006,1023,2006,2023} = elgf;
Transfinite Line {3006,3023,4006,4023} = elgf;
Transfinite Line {5006,5023,6006,6023} = elgf;
Transfinite Line {7006,7023,8006,8023} = elgf;
Transfinite Line {9006,9023,10006,10023} = elgf;
Transfinite Line {11006,11023,12006,12023} = elgf;
Transfinite Line {1402,1403,1404,2402,2403,2404,1432,1433,1434,2432,2433,2434} = elL/2;
Transfinite Line {3402,3403,3404,4402,4403,4404,3432,3433,3434,4432,4433,4434} = elL/2;
Transfinite Line {5402,5403,5404,6402,6403,6404,5432,5433,5434,6432,6433,6434} = elL/2;
Transfinite Line {7401,7402,7403,7404,8401,8402,8403,8404,7431,7432,7433,7434} = elL/2;
Transfinite Line {8431,8432,8433,8434} = elL/2;
Transfinite Line {3,5,33,133,13,18,19,20,21,22,1033,2033,3033,4033,5033,6033,7033,8033} = elte;
Transfinite Line {1003,1005,1013,1022,2003,2013,2005,2022} = elte;
Transfinite Line {3005,3022,4005,4022} = elte;
Transfinite Line {5005,5022,6005,6022} = elte;
Transfinite Line {7005,7022,8005,8022} = elte;
Transfinite Line {9005,9022,10005,10022} = elte;
Transfinite Line {11005,11022,12005,12022} = elte;
Transfinite Line {3003,3013,4003,4013} = elte;
Transfinite Line {1053,2053,3053,4053} = elte;
Transfinite Line {5003,5013,6003,6013} = elte;
Transfinite Line {7003,7013,8003,8013} = elte;
Transfinite Line {103,105,113,118,119,120,121,122} = elte;
Transfinite Line {14,15,16,17,114,115,116,117} = elte;
Transfinite Line {1001,1011,1007,1024,2001,2011,2007,2024} = elb;
Transfinite Line {3001,3011,4001,4011} = elb;
Transfinite Line {1051,2051,3051,4051} = elb;
Transfinite Line {5001,5011,6001,6011} = elb;
Transfinite Line {7001,7011,8001,8011} = elb;
Transfinite Line {3007,3024,4007,4024} = elb;
Transfinite Line {5007,5024,6007,6024} = elb;
Transfinite Line {7007,7024,8007,8024} = elb;
Transfinite Line {9007,9024,10007,10024} = elb;
Transfinite Line {11007,11024,12007,12024} = elb;
Transfinite Line {1014,1015,1016,1017,2014,2015,2016,2017} = elte;
Transfinite Line {3014,3015,3016,3017,4014,4015,4016,4017} = elte;
Transfinite Line {5014,5015,5016,5017,6014,6015,6016,6017} = elte;
Transfinite Line {7014,7015,7016,7017,8014,8015,8016,8017} = elte;
Transfinite Line {1018,1019,1020,1021} = elte;
Transfinite Line {2018,2019,2020,2021} = elte;
Transfinite Line {3018,3019,3020,3021} = elte;
Transfinite Line {4018,4019,4020,4021} = elte;
Transfinite Line {5018,5019,5020,5021} = elte;
Transfinite Line {6018,6019,6020,6021} = elte;
Transfinite Line {7018,7019,7020,7021} = elte;
Transfinite Line {8018,8019,8020,8021} = elte;
Transfinite Line {9018,9019,9020,9021} = elte;
Transfinite Line {10018,10019,10020,10021} = elte;
Transfinite Line {11018,11019,11020,11021} = elte;
Transfinite Line {12018,12019,12020,12021} = elte;
Transfinite Line {1201,1202,1203,1204,1211,1212,1213,1214,1231,1232,1233,1234} = elhfr1;
Transfinite Line {2201,2202,2203,2204,2211,2212,2213,2214,2231,2232,2233,2234} = elte;
Transfinite Line {1205,1206,1207,1208,1215,1216,1217,1218} = elhfr1;
Transfinite Line {3205,3206,3207,3208,3215,3216,3217,3218,3231,3232,3233,3234} = elhfr2;
Transfinite Line {2205,2206,2207,2208,2215,2216,2217,2218} = elte;
Transfinite Line {4205,4206,4207,4208,4215,4216,4217,4218,4231,4232,4233,4234} = elte;
Transfinite Line {5205,5206,5207,5208,5215,5216,5217,5218,5231,5232,5233,5234} = elhfr3;
Transfinite Line {6205,6206,6207,6208,6215,6216,6217,6218} = elteg3;
Transfinite Line {6201,6202,6203,6204,6211,6212,6213,6214,6231,6232,6233,6234} = elteg3;
Transfinite Line {3201,3202,3203,3204,3211,3212,3213,3214} = elhfr2;
Transfinite Line {3251,3252,3253,3254} = elhfr2;
Transfinite Line {4201,4202,4203,4204,4211,4212,4213,4214} = elte;
Transfinite Line {2251,2252,2253,2254,4251,4252,4253,4254} = elte;
Transfinite Line {5201,5202,5203,5204,5211,5212,5213,5214} = elhfr3;

//element support
Transfinite Line {3054,3055,3056,3057,4054,4055,4056,4057} = elLsu;
Transfinite Line {1054,1055,1056,1057,2054,2055,2056,2057} = elLsu;

//element cover
Transfinite Line {7201,7211,7202,7212,7203,7213,7204,7214} = elReinf;
Transfinite Line {7231,7232,7233,7234} = elReinf;
Transfinite Line {7205,7215,7206,7216,7207,7217,7208,7218} = elReinf;
Transfinite Line {8201,8211,8202,8212,8203,8213,8204,8214} = eltc;
Transfinite Line {8205,8215,8206,8216,8207,8217,8208,8218} = eltc;
Transfinite Line {10205,10215,10206,10216,10207,10217,10208,10218} = eltc;
Transfinite Line {9205,9215,9206,9216,9207,9217,9208,9218} = eltfc;
Transfinite Line {8231,8232,8233,8234} = eltc;

//element bloc Cr
Transfinite Line {11205,11215,11206,11216,11207,11217,11208,11218} = elcr;
Transfinite Line {12205,12215,12206,12216,12207,12217,12208,12218} = elcr;



//plate
Surface Loop(1) = {1,101,301,302,201,231};
Transfinite Surface {1,101,301,302,201,231};
Recombine Surface  {1,101,301,302,201,231};
Volume(1) = {1};
Transfinite Volume {1};
Surface Loop(31) = {31,131,331,332,231,207};
Transfinite Surface {31,131,331,332,207};
Recombine Surface  {31,131,331,332,207};
Volume(31) = {31};
Transfinite Volume {31};
Surface Loop(2) = {2,102,302,303,202,232};
Transfinite Surface {2,102,303,202,232}; 
Recombine Surface {2,102,303,202,232}; 
Volume(2) = {2};
Transfinite Volume {2};
Surface Loop(32) = {32,132,332,333,232,206};
Transfinite Surface {32,132,333,206}; 
Recombine Surface {32,132,333,206}; 
Volume(32) = {32};
Transfinite Volume {32};
Surface Loop(3) = {3,103,303,304,203,233};
Transfinite Surface {3,103,304,203,233};
Recombine Surface  {3,103,304,203,233};
Volume(3) = {3};
Transfinite Volume {3};
Surface Loop(33) = {33,133,333,334,233,205};
Transfinite Surface {33,133,334,205};
Recombine Surface  {33,133,334,205};
Volume(33) = {33};
Transfinite Volume {33};

Surface Loop(11) = {4,104,311,312,201,211};
Transfinite Surface {4,104,311,312,211};
Recombine Surface  {4,104,311,312,211};
Volume(11) = {11};
Transfinite Volume {11};
Surface Loop(12) = {5,105,312,313,202,212};
Transfinite Surface {5,105,313,212};
Recombine Surface  {5,105,313,212};
Volume(12) = {12};
Transfinite Volume {12};
Surface Loop(13) = {6,106,313,314,203,213};
Transfinite Surface {6,106,314,213};
Recombine Surface  {6,106,314,213};
Volume(13) = {13};
Transfinite Volume {13};
Surface Loop(21) = {9,109,321,322,207,227};
Transfinite Surface {9,109,321,322,227};
Recombine Surface  {9,109,321,322,227};
Volume(21) = {21};
Transfinite Volume {21};
Surface Loop(22) = {8,108,322,323,206,226};
Transfinite Surface {8,108,323,226};
Recombine Surface  {8,108,323,226};
Volume(22) = {22};
Transfinite Volume {22};
Surface Loop(23) = {7,107,323,324,205,225};
Transfinite Surface {7,107,324,225};
Recombine Surface  {7,107,324,225};
Volume(23) = {23};
Transfinite Volume {23};

//left flange
For i In {1:8}
  j=i*1000;
  jm1=(i-1)*1000;
  Surface Loop(j+11) = {jm1+4,j+4,j+311,j+312,j+201,j+211};
  Transfinite Surface {j+4,j+311,j+312,j+201,j+211};
  Recombine Surface  {j+4,j+311,j+312,j+201,j+211};
  Volume(j+11) = {j+11};
  Transfinite Volume {j+11};
  Surface Loop(j+12) = {jm1+5,j+5,j+312,j+313,j+202,j+212};
  Transfinite Surface {j+5,j+313,j+202,j+212};
  Recombine Surface  {j+5,j+313,j+202,j+212};
  Volume(j+12) = {j+12};
  Transfinite Volume {j+12};
  Surface Loop(j+13) = {jm1+6,j+6,j+313,j+314,j+203,j+213};
  Transfinite Surface {j+6,j+314,j+203,j+213};
  Recombine Surface  {j+6,j+314,j+203,j+213};
  Volume(j+13) = {j+13};
  Transfinite Volume {j+13};
EndFor
//support
For i In {2:4}
  j=i*1000;
  jm1=(i-1)*1000;
  If (i==2 || i==4)
    Surface Loop(j+51) = {jm1+54,j+54,j+351,j+352,j+211,j+251};
    Transfinite Surface {jm1+54,j+54,j+351,j+352,j+251}; 
    Recombine Surface  {jm1+54,j+54,j+351,j+352,j+251};
    Volume(j+51) = {j+51};
    Transfinite Volume {j+51};
    Surface Loop(j+52) = {jm1+55,j+55,j+352,j+353,j+212,j+252};
    Transfinite Surface {jm1+55,j+55,j+353,j+212,j+252};
    Recombine Surface  {jm1+55,j+55,j+353,j+212,j+252};
    Volume(j+52) = {j+52};
    Transfinite Volume {j+52};
  EndIf
  If (i>1)
    Surface Loop(j+53) = {jm1+56,j+56,j+353,j+354,j+213,j+253};
    Transfinite Surface {jm1+56,j+56,j+353,j+354,j+253};
    Recombine Surface  {jm1+56,j+56,j+353,j+354,j+253};
    Volume(j+53) = {j+53};
    Transfinite Volume {j+53};
  EndIf
EndFor
//right flange
For i In {1:12}
  j=i*1000;
  jm1=(i-1)*1000;
  Surface Loop(j+21) = {jm1+9,j+9,j+321,j+322,j+207,j+227};
  Transfinite Surface {j+9,j+321,j+322,j+207,j+227};
  Recombine Surface  {j+9,j+321,j+322,j+207,j+227};
  Volume(j+21) = {j+21};
  Transfinite Volume {j+21};
  Surface Loop(j+22) = {jm1+8,j+8,j+322,j+323,j+206,j+226};
  Transfinite Surface {j+8,j+323,j+206,j+226};
  Recombine Surface  {j+8,j+323,j+206,j+226};
  Volume(j+22) = {j+22};
  Transfinite Volume {j+22};
  Surface Loop(j+23) = {jm1+7,j+7,j+323,j+324,j+205,j+225};
  Transfinite Surface {j+7,j+324,j+205,j+225};
  Recombine Surface  {j+7,j+324,j+205,j+225};
  Volume(j+23) = {j+23};
  Transfinite Volume {j+23};
EndFor

//gusset
For i In {1:8}
  j=i*1000;
  jm1=(i-1)*1000;
  Surface Loop(j+3) = {jm1+3,j+3,j+303,j+304,j+233,j+203};
  Transfinite Surface {j+3,j+303,j+304,j+233};
  Recombine Surface  {j+3,j+303,j+304,j+233};
  Volume(j+3) = {j+3};
  Transfinite Volume {j+3};
  Surface Loop(j+33) = {jm1+33,j+33,j+333,j+334,j+205,j+233};
  Transfinite Surface {j+33,j+333,j+334};
  Recombine Surface  {j+33,j+333,j+334};
  Volume(j+33) = {j+33};
  Transfinite Volume {j+33};
  If (i==2 || i==4 || i==6 || i==8)
    Surface Loop(j+2) = {jm1+2,j+2,j+302,j+303,j+232,j+202};
    Transfinite Surface {jm1+2,j+2,j+302,j+232};
    Recombine Surface  {jm1+2,j+2,j+302,j+232};
    Volume(j+2) = {j+2};
    Transfinite Volume {j+2};
    Surface Loop(j+32) = {jm1+32,j+32,j+332,j+333,j+206,j+232};
    Transfinite Surface {jm1+32,j+32,j+332};
    Recombine Surface  {jm1+32,j+32,j+332};
    Volume(j+32) = {j+32};
    Transfinite Volume {j+32};
  EndIf
  If (i==8)
    Surface Loop(j+1) = {jm1+1,j+1,j+301,j+302,j+231,j+201};
    Transfinite Surface {jm1+1,j+1,j+301,j+231,j+201};
    Recombine Surface  {jm1+1,j+1,j+301,j+231,j+201};
    Volume(j+1) = {j+1};
    Transfinite Volume {j+1};
    Surface Loop(j+31) = {jm1+31,j+31,j+331,j+332,j+207,j+231};
    Transfinite Surface {jm1+31,j+32,j+331,j+231,j+31};
    Recombine Surface  {jm1+31,j+31,j+331,j+231,j+31};
    Volume(j+31) = {j+31};
    Transfinite Volume {j+31};  
  EndIf
EndFor

myvol[]={1,2,3,31,32,33,11,12,13,21,22,23,1011,1012,1013,2011,  2012,2013,1021,1022,1023,2021,2022,2023,3021,3022,3023,4021,4022,4023, 3011,3012,3013,4011,4012,4013,1003,2003, 3003,4003,1033,2033, 3033,4033};
myvolCr1[] ={};
myvolCr2[] ={};
//lower support
If (addLowerSup>0)
  myvol+={2051,2052,2053};
EndIf
//first flange
If (addFirstFlange>0)
  myvol+={2002,2032};
EndIf
//upper support
If (addUpperSup>0)
  myvol+={4051,4052,4053};
EndIf
//support web
If (addWebSup>0)
  myvol+={3053};
EndIf
//original flange
If (addSecondFlange>0)
  myvol+={4002,4032};
EndIf
//third flange
If (addThirdFlange>0)
  myvol+={5011,5012,5013,6011,6012,6013,5023,6022,6023,5003,6002,6003,5033,6032,6033};
  //myvol+={5021,5022,6021};
EndIf
If (addCover>0)
  myvol+={5011,5012,5013,6011,6012,6013,7011,7012,7013,8011,8012,8013};
  If (addCoverEffectOnly==0)
    myvol+={8021,8022,8023};
    myvol+={9021,9022,9023,8001,8002,8003,8031,8032,8033};
    myvol+={10021,10022,10023};
  EndIf
  If (addBlocks>0)
    myvolCr1+={11021,11022,11023}; //we add 10021,10022,10023 to  the chrome for vizualization as highly constrained
    myvolCr2+={12021,12022,12023};
  EndIf
EndIf
If (addRefincorce >0)
  If (addRefincorce ==2)
    myvol+={5021,5022,5023,6021,6022,6023,7021,7022,7023};
  EndIf
  If (addRefincorce ==1)
    myvol+={5023,6023,7023,5003,6003,7003,5033,6033,7033};
  EndIf
EndIf
Physical Volume(10)=myvol[];
Physical Volume(11)=myvolCr1[];
Physical Volume(12)=myvolCr2[];


//symmetry gusset side
gusSym[]= {4314,3314,2314, 1314,314, 304, 334,1304,1334, 2304, 2334, 2324,1324,3304,3334, 4304, 4334,3324,3324,4304, 4304, 4324,4324,24};
//lower support
If (addLowerSup>0)
  gusSym+={2354};
EndIf
//upper support
If (addUpperSup>0)
  gusSym+={4354};
EndIf
//support web
If (addWebSup>0)
  gusSym+={3354};
EndIf
//third flange
If (addThirdFlange>0)
  gusSym+={5324,6324,5314,6314,5304,5334, 6304, 6334};
EndIf
If (addCover>0)
  gusSym+={5314,6314,7314,8314};
  If (addCoverEffectOnly==0)
    gusSym+={9324,10324,8304,8334};
  EndIf
  If(addBlocks>0)
    gusSym+={11324,12324};
  EndIf
EndIf
If (addRefincorce>0)
  If (addRefincorce ==2)
    gusSym+={5324,6324,7324};
  EndIf
  If (addRefincorce ==1)
    gusSym+={5324,6324,7324,5304,5334, 6304, 6334, 7304, 7334};
  EndIf
EndIf
Physical Surface(204) = gusSym[];


//symmetry plate side
plateSym[] = {4311,3311,2311, 1311,311, 301, 331,4321,3321, 2321,1321,21};
//lower support
If (addLowerSup>0)
  plateSym+={2351};
EndIf
//upper support
If (addUpperSup>0)
  plateSym+={4351};
EndIf
//third flange
If (addThirdFlange>0)
  plateSym+={5311,6311};
  //plateSym+={5321,6321};
EndIf
If (addCover>0)
  plateSym+={5311,6311,7311,8311};
  If (addCoverEffectOnly==0)
    plateSym +={8301,8331,8321,9321,10321};
  EndIf
  If(addBlocks)
    plateSym+={11321,12321};
  EndIf
EndIf
If (addRefincorce>0)
  If (addRefincorce ==2)
    plateSym+={5321,6321,7321};
  EndIf
EndIf
Physical Surface(201) = plateSym[];

//symmetry Hside (up and down)
HSym[] = {4225,4226,4227};
//third flange
If (addThirdFlange>0)
  HSym+={5225,6225,6226};
  //HSym+={5226,5227,6227};
EndIf
If (addRefincorce>0)
  If (addRefincorce ==2)
    HSym+={5225,5226,5227,6225,6226,6227};
  EndIf
  If (addRefincorce ==1)
    HSym+={5225,6225};
  EndIf
EndIf


Physical Surface(101) = HSym[];
Physical Surface(102) = {1225,1226,1227,2225,2226,2227,3225,3226,3227};
Physical Line(103) = {3022,3023,3024};

//Block cr
BlockCrx[] = {};
BlockCrz[] = {};
LineCr[] = {};
SurfCoverEffect[] = {};
If (addCover>0 && addBlocks>0)
  BlockCrx += {11205,11206,11207,12205,12206,12207};
  BlockCrz += {12007,12008,12009};
EndIf
If (addCover>0 && addBlocks==0)
  If (addCoverEffectOnly==0)
    BlockCrx += {10205,10206,10207};
    BlockCrz += {10007,10008,10009};
    LineCr += {9005,9006,9007};
  EndIf
  If (addCoverEffectOnly==1)
   SurfCoverEffect += {8201,8202,8203};
  EndIf
EndIf
Physical Surface(108) = BlockCrx[];
Physical Line(308) = LineCr[];
Physical Surface(308) = BlockCrz[];
Physical Surface(309) = SurfCoverEffect[];


//pressure
Physical Surface(301) = {1,2,31,32};

//pressure cover
coverP[] ={};
If (addCover>0 && addCoverEffectOnly==0)
  coverP += {7001,7002,7003,7031,7032,7033};
EndIf
Physical Surface(302) = coverP[];

//pressure right flange
rightFl[] ={1201,1202,2201,3201,3202,4201};
If (addFirstFlange == 0)
  rightFl[]+={2202};
EndIf
If (addThirdFlange>0)
  rightFl[]+={5201,5202,6201};
EndIf
If (addCover>0)
  rightFl[]+={5201,5202,6201};
  If (addThirdFlange == 0)
    rightFl[]+={6202};
  EndIf
EndIf
Physical Surface(303) = rightFl[];


//support
//lower sup
LineSup [] = {};
SurfSup [] = {};
SurfSup2 [] = {};
PointSup[] = {};
If (addLowerSup>0)
  LineSup [] += {1051,1052,1053};
  SurfSup [] += {1054,1055,1056};
  SurfSup2 [] += {2054,2055,2056};
  PointSup[] += {1051};
EndIf
//upperSup 
If (addUpperSup>0)
  LineSup [] += {3051,3052,3053};
  SurfSup [] += {3054,3055,3056};
  SurfSup2 [] += {4054,4055,4056};
  PointSup[] += {3051};
EndIf

Physical Line(104) = LineSup [];
Physical Surface(104) = SurfSup[];
Physical Surface(105) = SurfSup2[];

//lower part of flange
Physical Point(111) = {111};
//mid plate
Physical Point(131) = {131};
//support
Physical Point(151) = PointSup[];
