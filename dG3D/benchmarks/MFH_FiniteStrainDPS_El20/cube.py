#coding-Utf-8-*-
from gmshpy import *
from dG3Dpy import*
#from dG3DpyDebug import*
#script to launch beam problem with a python script

# material law
lawnum   = 1 # unique number of law
rho      = 7850
properties = "property.i01"

# geometry
geofile="cube.geo" # name of mesh file
meshfile="cube.msh" # name of mesh file

# solver
sol = 2  # Gmm=0 (default) Taucs=1 PETsc=2
soltype = 1 # StaticLinear=0 (default) StaticNonLinear=1
nstep = 1000  # number of step (used only if soltype=1)
ftime =1.   # Final time (used only if soltype=1)
tol=1.e-6   # relative tolerance for NR scheme (used only if soltype=1)
nstepArch=5 # Number of step between 2 archiving (used only if soltype=1)
fullDg = 0 #O = CG, 1 = DG
space1 = 0 # function space (Lagrange=0)


#  compute solution and BC (given directly to the solver
# creation of law
law1 = NonLocalDamageDG3DMaterialLaw(lawnum,rho,properties)
law1.setUseBarF(True)
# creation of ElasticField
nfield = 10 # number of the field (physical number of surface)
myfield1 = dG3DDomain(1000,nfield,space1,lawnum,fullDg,3)
myfield1.stabilityParameters(30.)
myfield1.matrixByPerturbation(1,1,1,1e-8)
# creation of Solver
mysolver = nonLinearMechSolver(1000)
mysolver.createModel(geofile,meshfile,3,1)
#mysolver.loadModel(meshfile)
mysolver.addDomain(myfield1)
mysolver.addMaterialLaw(law1)
mysolver.Scheme(soltype)
mysolver.Solver(sol)
mysolver.snlData(nstep,ftime,tol)
mysolver.stepBetweenArchiving(nstepArch)
# BC
#shearing
#mysolver.displacementBC("Face",1234,0,0.)
#mysolver.displacementBC("Face",1234,1,0.)
#mysolver.displacementBC("Face",1234,2,0.)
#mysolver.displacementBC("Face",5678,1,0.00)
#mysolver.displacementBC("Face",5678,0,0.00001)
#mysolver.forceBC("Face",2376,2,100000000)
#mysolver.forceBC("Face",4158,2,-100000000)
#mysolver.forceBC("Face",5678,0,100000000)
#tension along z
mysolver.displacementBC("Face",1234,2,0.)
#program1Fucntion=cycleFunctionTime(0., 0., ftime, 0.2e-3, ftime, 0.);
mysolver.displacementBC("Face",5678,2,0.2e-3)
mysolver.displacementBC("Face",2376,0,0.)
mysolver.displacementBC("Face",1265,1,0.)

#mysolver.displacementBC("Face",3487,1,0.0)
#mysolver.displacementBC("Face",4158,0,0.0)


mysolver.internalPointBuildView("svm",IPField.SVM, 1, 1)
mysolver.internalPointBuildView("sig_xx",IPField.SIG_XX, 1, 1)
mysolver.internalPointBuildView("sig_yy",IPField.SIG_YY, 1, 1)
mysolver.internalPointBuildView("sig_zz",IPField.SIG_ZZ, 1, 1)
mysolver.internalPointBuildView("sig_xy",IPField.SIG_XY, 1, 1)
mysolver.internalPointBuildView("sig_yz",IPField.SIG_YZ, 1, 1)
mysolver.internalPointBuildView("sig_xz",IPField.SIG_XZ, 1, 1)
mysolver.internalPointBuildView("epl",IPField.PLASTICSTRAIN, 1, 1)
mysolver.archivingIPOnPhysicalGroup("Volume",nfield, IPField.SIG_XX,IPField.MEAN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",nfield, IPField.STRAIN_XX,IPField.MEAN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",nfield, IPField.SIG_YY,IPField.MEAN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",nfield, IPField.STRAIN_YY,IPField.MEAN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",nfield, IPField.SIG_ZZ,IPField.MEAN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",nfield, IPField.STRAIN_ZZ,IPField.MEAN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",nfield, IPField.SIG_XY,IPField.MEAN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",nfield, IPField.STRAIN_XY,IPField.MEAN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",nfield, IPField.SIG_YZ,IPField.MEAN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",nfield, IPField.STRAIN_YZ,IPField.MEAN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",nfield, IPField.SIG_XZ,IPField.MEAN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",nfield, IPField.STRAIN_XZ,IPField.MEAN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",nfield, IPField.INC_VOLUME_RATIO,IPField.MEAN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",nfield, IPField.INC_ASPECT_RATIO_1,IPField.MEAN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",nfield, IPField.INC_ASPECT_RATIO_2,IPField.MEAN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",nfield, IPField.INC_R_XX,IPField.MEAN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",nfield, IPField.INC_R_XY,IPField.MEAN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",nfield, IPField.INC_R_XZ,IPField.MEAN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",nfield, IPField.INC_R_YX,IPField.MEAN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",nfield, IPField.INC_R_YY,IPField.MEAN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",nfield, IPField.INC_R_YZ,IPField.MEAN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",nfield, IPField.INC_R_ZX,IPField.MEAN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",nfield, IPField.INC_R_ZY,IPField.MEAN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",nfield, IPField.INC_R_ZZ,IPField.MEAN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",nfield, IPField.MTX_SIG_XX,IPField.MEAN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",nfield, IPField.MTX_SIG_YY,IPField.MEAN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",nfield, IPField.MTX_SIG_ZZ,IPField.MEAN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",nfield, IPField.INC_SIG_ZZ,IPField.MEAN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",nfield, IPField.INC_SIG_YY,IPField.MEAN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",nfield, IPField.INC_SIG_XX,IPField.MEAN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",nfield, IPField.MTX_SIG_XY,IPField.MEAN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",nfield, IPField.MTX_SIG_YZ,IPField.MEAN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",nfield, IPField.MTX_SIG_XZ,IPField.MEAN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",nfield, IPField.INC_SIG_XZ,IPField.MEAN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",nfield, IPField.INC_SIG_YZ,IPField.MEAN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",nfield, IPField.INC_SIG_XY,IPField.MEAN_VALUE, nstepArch);

mysolver.archivingForceOnPhysicalGroup("Face", 1234, 2)
mysolver.archivingForceOnPhysicalGroup("Face", 5678, 2)

mysolver.solve()

check = TestCheck()
check.equal(9.644039e+02,mysolver.getArchivedForceOnPhysicalGroup("Face", 5678, 2),1.e-3)
try:
  import linecache
  linesVF = linecache.getline('IPVolume10val_INC_VOLUME_RATIOMean.csv',200)
  linesAR1 = linecache.getline('IPVolume10val_INC_ASPECT_RATIO_1Mean.csv',200)
  linesAR2 = linecache.getline('IPVolume10val_INC_ASPECT_RATIO_2Mean.csv',200)
  linesRXX = linecache.getline('IPVolume10val_INC_R_XXMean.csv',200)
  linesISIG = linecache.getline('IPVolume10val_INC_SIG_ZZMean.csv',200)
  linesMSIG = linecache.getline('IPVolume10val_MTX_SIG_ZZMean.csv',200)
except:
  print('Cannot read the results in the files')
  import os
  os._exit(1)
else:
  check.equal(2.002383e-01,float(linesVF.split(';')[1]))
  check.equal(3.033909e+00,float(linesAR1.split(';')[1]))
  check.equal(3.033909e+00,float(linesAR2.split(';')[1]))
  check.equal(1.723483e+09,float(linesISIG.split(';')[1]))
  check.equal(1.012394e+09,float(linesMSIG.split(';')[1]))

