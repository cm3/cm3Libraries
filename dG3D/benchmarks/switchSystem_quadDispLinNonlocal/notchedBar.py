#coding-Utf-8-*-
from gmshpy import*
from dG3Dpy import*


# Material law creation
# ===================================================================================

# material parameters - bulk law
rho = 7850. 		# kg/m3 :	density
E = 3.2e9		# Pa : 		Young modulus
nu = 0.28 		# - : 		poisson coefficient
pi = 0.011 		# - : 		initial damage strain value
pc = 0.50 		# - : 		failure strain value
alpha = 5.0 		# - : 		damage exponent
beta = 0.75 		# - : 		damage exponent bis
cl = 2.0e-6 		# m2: 		non-local parameter (== NL-length^2) 

# material law creation
BulkLawNum1 = 1 
Cl_Law1 = IsotropicCLengthLaw(1,cl)
Dam_Law1 = PowerDamageLaw(1, pi, pc, alpha, beta)
BulkLaw1 = NonLocalDamageIsotropicElasticityDG3DMaterialLaw(BulkLawNum1,rho,E,nu,Cl_Law1,Dam_Law1)
BulkLaw1.setUseBarF(True)


# Solver parameters
# ===================================================================================
sol = 2  		# Library for solving: Gmm=0 (default) Taucs=1 PETsc=2
soltype = 1 		# StaticLinear=0 (default, StaticNonLinear=1, Explicit=2, # Multi=3, Implicit=4, Eigen=5
nstep = 30   		# Number of step between implicit iterations
ftime =1.0e-3   	# Final time
tol=1.e-4   		# Relative tolerance for NR scheme


MaxIter = 25		# Maximum number of iterations
StepIncrease = 3	# Number of successfull timestep before reincreasing it
StepReducFactor = 4.0 	# Timestep reduction factor
NumberReduction = 5	# Maximum number of timespep reduction: max reduction = pow(StepReducFactor,this)
fullDg = 0       # O = CG, 1 = DG
dgnl = True		# DG for non-local variables inside a domain (only if fullDg)
eqRatio = 1.0e6		# Ratio between "elastic" and non-local equations 
space1 = 0 		# Function space (Lagrange=0)
beta1  = 30.0		# Penality parameter for DG

# Domain creation
numPhysVolume1 = 51 		# Number of a physical volume of the model in .geo
numDomain1 = 1000 		# Number of the domain
field1 = dG3DDomain(numDomain1,numPhysVolume1,space1,BulkLawNum1,fullDg,3,1)
field1.stabilityParameters(beta1) 			# Adding stability parameters (for DG)
field1.setNonLocalStabilityParameters(beta1,dgnl)
field1.setNonLocalEqRatio(eqRatio)
#field1.matrixByPerturbation(1,1,1,1.e-10) 		# Tangent computation analytically or by pertubation
field1.usePrimaryShapeFunction(3)

# Solver creation
# ===================================================================================
mysolver = nonLinearMechSolver(numDomain1) 		# Solver associated with numSolver
# Geometry and mesh loading
# ===================================================================================
meshfile="notchedBar.msh"		# name of mesh file
geofile="notchedBar.geo"		# name of geo file
#mysolver.loadModel(meshfile)		# add mesh
mysolver.createModel(geofile,meshfile,3,2)
mysolver.addDomain(field1) 		# add domain
mysolver.addMaterialLaw(BulkLaw1) 	# add material law
		
# solver parametrisation 		
mysolver.Scheme(soltype) 			# solver scheme
mysolver.Solver(sol) 				# solver choice
mysolver.snlData(nstep,ftime,tol) 		# solver parameters
mysolver.snlManageTimeStep(MaxIter,StepIncrease,StepReducFactor,NumberReduction)

#element erosion
tot_disp = 5e-4
funct = LinearFunctionTime(0.0,0.0,ftime,tot_disp)
mysolver.displacementBC("Face",61,1,0.)	# Lower face (y = 0)
mysolver.displacementBC("Face",62,1,funct)	# Upper face (y = Ly)

mysolver.displacementBC("Face",61,0,0)
mysolver.displacementBC("Face",62,0,0)
mysolver.displacementBC("Volume",51,2,0)


mysolver.internalPointBuildView("svm",IPField.SVM, 1, 1)
mysolver.internalPointBuildView("sig_xx",IPField.SIG_XX, 1, 1)
mysolver.internalPointBuildView("sig_yy",IPField.SIG_YY, 1, 1)
mysolver.internalPointBuildView("sig_zz",IPField.SIG_ZZ, 1, 1)
mysolver.internalPointBuildView("sig_xy",IPField.SIG_XY, 1, 1)
mysolver.internalPointBuildView("sig_yz",IPField.SIG_YZ, 1, 1)
mysolver.internalPointBuildView("sig_xz",IPField.SIG_XZ, 1, 1)
mysolver.internalPointBuildView("damage",IPField.DAMAGE,1,1)
mysolver.internalPointBuildView("eff_eps",IPField.LOCAL_EQUIVALENT_STRAINS,1,1)
mysolver.internalPointBuildView("NL_eps",IPField.NONLOCAL_EQUIVALENT_STRAINS,1,1)
mysolver.internalPointBuildView("Deleted",IPField.DELETED,1,1)

mysolver.archivingForceOnPhysicalGroup("Face", 62, 1,1)
mysolver.archivingNodeDisplacement(73,1,1)

mysolver.solve()

check = TestCheck()
check.equal(2.519787e-02,mysolver.getEnergy(energeticField.damage),1.e-4)

# new time
ftime = 1e-5
# switch to multi system
soltype = 3 		# StaticLinear=0 (default, StaticNonLinear=1, Explicit=2, # Multi=3, Implicit=4, Eigen=5
mysolver.Scheme(soltype) 			# solver scheme

# add explicit and implicit
mysolver.addSystem(3,2)				# adding system (number of variables, solving scheme)
mysolver.addSystem(1,1)

nstep = 50   		# Number of step between implicit iterations
nstepExpl = 150		# Number of step between time step evaluation
mysolver.explicitSpectralRadius(ftime,0.3,0.)	# Explicit integration param.(final time, security ./. critical
mysolver.explicitTimeStepEvaluation(nstepExpl)

mysolver.snlData(nstep,ftime,tol) 		# solver parameters
mysolver.snlManageTimeStep(MaxIter,StepIncrease,StepReducFactor,NumberReduction)

nstepArch=150 		# Number of step between 2 archiving
nstepArchEnergy = 30 # Number of step between 2 energy computation and archiving
mysolver.stepBetweenArchiving(nstepArch) 	# archiving frequency
mysolver.energyComputation(nstepArchEnergy)	# archiving frequency for energy

# define new BC, same BC, values must start from the last step of previous solve()
mysolver.resetBoundaryConditions()
funct = LinearFunctionTime(0.0,tot_disp,ftime,1.2*tot_disp)
mysolver.displacementBC("Face",61,1,0.)	# Lower face (y = 0)
mysolver.displacementBC("Face",62,1,funct)	# Upper face (y = Ly)

mysolver.displacementBC("Face",61,0,0)
mysolver.displacementBC("Face",62,0,0)
mysolver.displacementBC("Volume",51,2,0)

mysolver.solve()
# new BC

check = TestCheck()
check.equal(3.950682e-02,mysolver.getEnergy(energeticField.damage),1.e-4)

