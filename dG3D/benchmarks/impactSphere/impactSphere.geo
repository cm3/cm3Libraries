SetFactory("OpenCASCADE");

Mesh.Optimize = 1;
Mesh.SecondOrderLinear = 1;
mm = 1.; // Unit
//Geometry.AutoCoherence = 0.000000001*mm;
//Geometry.Tolerance = 0.000000001*mm; // adjust value here for correct merge result
Mesh.CharacteristicLengthMin = 1.*mm;
Mesh.CharacteristicLengthMax = 25.*mm;






RextLattice = 100*mm;
HLattice    = 4.*7.5*mm;
LLattice    = 7.5*mm; //mesh size


LowerPlate  = 2.*mm;
UpperPlate  = 2.*mm;
LPlate      = 7.5*mm; //mesh size

RextImpactor = 60*mm;
RintImpactor = 50.*mm;
LImpactor    = 10*mm; //mesh size


// lower plate
plate()+=newv;  Cylinder(newv) = {0,0,0, 0,0,LowerPlate, RextLattice, Pi/2.};
// upper plate
plate()+=newv;  Cylinder(newv) = {0,0,LowerPlate+HLattice, 0,0,UpperPlate, RextLattice, Pi/2.};

Characteristic Length { PointsOf{ Volume{ plate[] };  } } = LPlate;

// lattice part
lattice()+=newv;  Cylinder(newv) = {0,0,LowerPlate, 0,0,HLattice, RextLattice, Pi/2.};
Characteristic Length { PointsOf{ Volume{ lattice[] };  } } = LLattice;




//impactor
//+
impactortmp()+=newv; Sphere(newv) = {0.-0.0001*mm, 0.-0.0001*mm, LowerPlate+HLattice+UpperPlate+RextImpactor+0.000001*mm, RextImpactor, -Pi/2, 0., Pi/2.};
//+
impactortmp()+=newv; Sphere(newv) = {0.-0.0001*mm, 0.-0.0001*mm, LowerPlate+HLattice+UpperPlate+RextImpactor+0.000001*mm, RintImpactor, -Pi/2, 0., Pi/2.};

impactor()+=newv; BooleanDifference(newv) = { Volume{impactortmp(0)}; Delete; }{ Volume{impactortmp(1)}; Delete; };
Characteristic Length { PointsOf{ Volume{ impactor[] };  } } = LImpactor;


Coherence;

surflowerplate[] = Abs(Boundary{ Volume{plate(0)}; });
Printf("surf lower plate ",surflowerplate());
surfupperplate[] = Abs(Boundary{ Volume{plate(1)}; });
Printf("surf upper plate ",surfupperplate());
surflattice[]    = Abs(Boundary{ Volume{lattice(0)}; });
Printf("surf lattice ",surflattice());
surfimpactor[]    = Abs(Boundary{ Volume{impactor(0)}; });

Printf("surf impactor ",surfimpactor());

ptimpactor[] = PointsOf{ Surface{surfimpactor(1)}; };
Printf("pts impactor ",ptimpactor());





OXZ=101;
Physical Surface(OXZ) = {surflowerplate(3), surflattice(3),surfupperplate(3)};
OYZ=100;
Physical Surface(OYZ) = {surflowerplate(4), surflattice(4),surfupperplate(4)};

OXYLOW=200;
Physical Surface(OXYLOW) = {surflowerplate(2)};
OXYUP=201;
Physical Surface(OXYUP) = {surfupperplate(1)};

LOWERPLATE=51;
Physical Volume(LOWERPLATE) = {plate(0)};
UPPERPLATE=52;
Physical Volume(UPPERPLATE) = {plate(1)};

LATTICE=53;
Physical Volume(LATTICE) = {lattice(0)};


OXZIMPACTOR=1001;
Physical Surface(OXZIMPACTOR) = {surfimpactor(3)};
OYZIMPACTOR=1000;
Physical Surface(OYZIMPACTOR) = {surfimpactor(2)};

OXYIMPACTORLOW=2000;
Physical Surface(OXYIMPACTORLOW) ={surfimpactor(0)};
OXYIMPACTORUP=2001;
Physical Surface(OXYIMPACTORUP) = {surfimpactor(1)};

IMPACTOR=1001;
Physical Volume(IMPACTOR) = {impactor(0)};

PTIMPACTOR = 1002;
Physical Point(PTIMPACTOR) = {ptimpactor(2)};


//rigid contact
X_contact=-0.1*mm;
Y_contact=-0.1*mm;
Z_contact=-0.0000001*mm;
L_contact=RextLattice+2.*mm;
Point(11001) = {X_contact,Y_contact,Z_contact};
Point(11002) = {X_contact,Y_contact+L_contact,Z_contact};
Point(11003) = {X_contact+L_contact,Y_contact+L_contact,Z_contact};
Point(11004) = {X_contact+L_contact,Y_contact,Z_contact};
Line(10001) = {11001, 11002};
Line(10002) = {11002, 11003};
Line(10003) = {11003, 11004};
Line(10004) = {11004, 11001};
Line Loop(10001) = {10004, 10001, 10002, 10003};
Physical Point(11001) = {11001};
Plane Surface(10001) = {10001};
Transfinite Line {10001,10002,10003,10004} = 2 Using Progression 1;
Transfinite Surface {10001};
Recombine Surface {10001};
RIGIDCONTACT=10001;
Physical Surface(RIGIDCONTACT) = {10001};

Coherence;


