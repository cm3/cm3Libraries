#coding-Utf-8-*-
from gmshpy import *	
from dG3Dpy import *			
						
import pickle
import numpy as np


def FillMatPara(x_E,x_P,VEVP):    #x = [E0,nu0,K1,G1,....,tk,tg, aP]
  tk = np.power(10.0, x_E[int(len(x_E)-3)])
  tg = np.power(10.0, x_E[int(len(x_E)-2)])
  MatPara = []
  MatPara.append([x_E[0], x_E[1]])
  Ki = []
  Gi = []
  N = int((len(x_E)-5)/2)
  for i in range(N):
    a = 1.0/np.power(10.0, i)
    Ki.append([np.power(10.0,x_E[2+2*i]), a*tk])
    Gi.append([np.power(10.0,x_E[2+2*i+1]), a*tg])
  MatPara.append(Ki)
  MatPara.append(Gi)
 
  
  if(VEVP):
    Compression = []
    Tension = []
    ViscoPlastic = []
    
    Compression.append(x_P[0])
    Tension.append(x_P[4])
    for i in range(1,3):
      Compression.append(np.power(10.0, x_P[i]))
      Tension.append(np.power(10.0, x_P[4+i]))
    Compression.append(x_P[3])
    Tension.append(x_P[7])  
      
    for i in range(4):  
      ViscoPlastic.append(x_P[8+i])       
      
    MatPara.append(Compression)
    MatPara.append(Tension) 
    MatPara.append(ViscoPlastic) 
  return MatPara
############################################################################  




def RunCase(LoadCase,MatPara, VEVP):
  
  nstep = 800 # number of step (used only if soltype=1)
  # === Problem parameters ============================================================================ 
  E0 = MatPara[0][0]
  nu0 = MatPara[0][1]
  Nt = len(MatPara[1])    #the number of branches in the generalized Maxwell model 
  aP = 1.0 

  if(VEVP):
    indxplastic = 3 
    sy0c = MatPara[indxplastic][0]  #MPa, compressive yield stress
    hc = MatPara[indxplastic][1]
    hc2 = MatPara[indxplastic][2]
    kc = MatPara[indxplastic][3]
  
    sy0t = MatPara[indxplastic+1][0]*sy0c   #MPa, compressive yield stress
    ht = MatPara[indxplastic+1][1]
    ht2 = MatPara[indxplastic+1][2]
    kt = MatPara[indxplastic+1][3]
   
    alpha = MatPara[indxplastic+2][0]
    beta = MatPara[indxplastic+2][1]
    eta = MatPara[indxplastic+2][2]
    p = MatPara[indxplastic+2][3]
    
  #=============================================    
  lawnum = 11
  rho = 7850e-9
  
  if(VEVP):
    law1 = HyperViscoElastoPlasticPowerYieldDG3DMaterialLaw(lawnum,rho,E0,nu0)
  else:
    law1 = HyperViscoElasticDG3DMaterialLaw(lawnum,rho,E0,nu0)
      
  law1.setViscoelasticMethod(0)
  law1.setViscoElasticNumberOfElement(Nt)
  for i in range(Nt):
    law1.setViscoElasticData_Bulk(i+1, MatPara[1][i][0], aP*MatPara[1][i][1])
    law1.setViscoElasticData_Shear(i+1, MatPara[2][i][0], aP*MatPara[2][i][1])
  law1.setStrainOrder(5) 
  #-----------------------------------------------------------------------------
  if(VEVP):  
    hardenc = LinearExponentialJ2IsotropicHardening(1, sy0c, hc, hc2, kc)
    hardent = LinearExponentialJ2IsotropicHardening(2, sy0t, ht, ht2, kt)
  
    law1.setCompressionHardening(hardenc)
    law1.setTractionHardening(hardent)
  
    law1.setYieldPowerFactor(alpha)
    law1.setNonAssociatedFlow(True)
    law1.nonAssociatedFlowRuleFactor(beta)
    etac = constantViscosityLaw(1,eta)
    law1.setViscosityEffect(etac,p)

  
  if(LoadCase[0] == 0):
    ft1 = 5.732	
    eps = -0.0157 
    if(VEVP):
      ft1 = 106.5
      eps = -0.296	
    ftime = LoadCase[2]
  else:
    strainrate = LoadCase[1]
    strain_end = LoadCase[2]
    ftime = strain_end/strainrate	# Final time (used only if soltype=1)  
 # print("######################",strain_end,strainrate,ftime)
  # ===Solver parameters=============================================================================
  sol = 2  # Gmm=0 (default) Taucs=1 PETsc=2
  soltype = 1 # StaticLinear=0 (default) StaticNonLinear=1
  tol=1.e-5  # relative tolerance for NR scheme (used only if soltype=1)
  nstepArch=10 # Number of step between 2 archiving (used only if soltype=1)
  fullDg = 0 #O = CG, 1 = DG
  space1 = 0 # function space (Lagrange=0)
  beta1  = 100						

  # ===Domain creation===============================================================================
  # creation of ElasticField
  myfield1 = dG3D1DDomain(11,11,space1,lawnum, False) #for non local
  myfield1.setState(1) #UniaxialStrain=0, UniaxialStress =1,ConstantTriaxiality=2
  L = 5.
  
  # ===Solver creation===============================================================================
  # geometry & mesh
  geofile = "line.geo"
  meshfile = "line.msh"
  
  # creation of Solver
  mysolver = nonLinearMechSolver(1000)
  #mysolver.createModel(geofile,meshfile,1,1)
  mysolver.loadModel(meshfile)
  mysolver.addDomain(myfield1)
  mysolver.addMaterialLaw(law1)
  mysolver.Scheme(soltype)
  mysolver.Solver(sol)
  mysolver.snlData(nstep,ftime,tol)
  mysolver.stepBetweenArchiving(nstepArch)

  mysolver.snlManageTimeStep(15,5,2.,20) # maximal 20 times

# ===Boundary conditions===========================================================================
  mysolver.displacementBC("Edge",11,1,0.)
  mysolver.displacementBC("Edge",11,2,0.)
  mysolver.displacementBC("Node",1,0,0.)
  
  disp = PiecewiseLinearFunction()
  disp.put(0.,0.) 
  if(LoadCase[0] != 0):
    disp.put(ftime, LoadCase[0]*strain_end*L)
  else:  
    disp.put(ft1,eps*L)  
    disp.put(ftime,eps*L) 
    
  mysolver.displacementBC("Node",2,0,disp)	
  mysolver.archivingAverageValue(IPField.F_XX)
  mysolver.archivingAverageValue(IPField.P_XX)

  # ===Solving=======================================================================================
  mysolver.SetVerbosity(0)
  mysolver.solve()   
  return  


#def test():
#  print("It works!!")


#MatPara=   
#RunCase(L_Case,MatPara,VEVP)    	





















