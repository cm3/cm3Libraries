#coding-Utf-8-*-
from gmshpy import *
from dG3Dpy import*
#from dG3DpyDebug import*

#script to launch beam problem with a python script

# material law
lawnum=num = 1 # unique number of law
rho   = 7850.
young =Ex=Ey=Ez =2.1e11
nu    =Vxy=Vxz=Vyz= 0.3
MUxy=MUxz=MUyz=Ex/(2.*(1.+Vxy)) 
cv=1.
t0=200.
Kx=Ky=Kz=51.9
alphax=alphay=alphaz=12.e-6


# geometry
geofile="cube.geo"
meshfile="cube.msh" # name of mesh file

# solver
sol = 2  # Gmm=0 (default) Taucs=1 PETsc=2
soltype = 1 # StaticLinear=0 (default) StaticNonLinear=1
nstep = 50   # number of step (used only if soltype=1)
ftime =1.   # Final time (used only if soltype=1)
tol=1.e-6   # relative tolerance for NR scheme (used only if soltype=1)
nstepArch=1 # Number of step between 2 archiving (used only if soltype=1)
fullDg = 1 #O = CG, 1 = DG
space1 = 0 # function space (Lagrange=0)
beta1  = 10

alpha = beta=gamma=0.
cp = 0.

#  compute solution and BC (given directly to the solver
# creation of law


law1   = LinearThermoMechanicsDG3DMaterialLaw(num,rho,Ex,Ey,Ez,Vxy,Vxz,Vyz,MUxy,MUxz,MUyz,alpha,beta,gamma,t0,Kx,Ky,Kz,alphax,alphay,alphaz,cp)

nfield = 10 # number of the field (physical number of surface)
myfield1 = ThermoMechanicsDG3DDomain(1000,nfield,space1,lawnum,fullDg,1.e6)
myfield1.matrixByPerturbation(0,0,0,1e-8)
myfield1.stabilityParameters(beta1)
myfield1.ThermoMechanicsStabilityParameters(beta1,bool(1))
# creation of Solver
mysolver = nonLinearMechSolver(1000)
mysolver.createModel(geofile,meshfile,3,2)
mysolver.loadModel(meshfile)
mysolver.addDomain(myfield1)
mysolver.addMaterialLaw(law1)
mysolver.Scheme(soltype)
mysolver.Solver(sol)
mysolver.snlData(nstep,ftime,tol,tol)
mysolver.stepBetweenArchiving(nstepArch)
# BC

#mechanical BC
mysolver.displacementBC("Face",1234,2,0.)
mysolver.displacementBC("Face",2376,0,0.)
mysolver.displacementBC("Face",1265,1,0.)

#thermal BC
cyclicFunctionTemp=cycleFunctionTime(0., t0, ftime , t0);
cyclicFunctionTemp1=cycleFunctionTime(0., t0, ftime , t0+100);
mysolver.displacementBC("Face",5678,3,cyclicFunctionTemp)
#mysolver.displacementBC("Face",1234,3,cyclicFunctionTemp1)
mysolver.scalarFluxBC("Face",1234,3,5.19e6)
mysolver.initialBC("Volume","Position",nfield,3,t0)

mysolver.internalPointBuildView("svm",IPField.SVM, 1, 1)
mysolver.internalPointBuildView("sig_xx",IPField.SIG_XX, 1, 1)
mysolver.internalPointBuildView("sig_yy",IPField.SIG_YY, 1, 1)
mysolver.internalPointBuildView("sig_zz",IPField.SIG_ZZ, 1, 1)
mysolver.internalPointBuildView("sig_xy",IPField.SIG_XY, 1, 1)
mysolver.internalPointBuildView("sig_yz",IPField.SIG_YZ, 1, 1)
mysolver.internalPointBuildView("sig_xz",IPField.SIG_XZ, 1, 1)
mysolver.internalPointBuildView("temperature",IPField.TEMPERATURE, 1, 1)
mysolver.internalPointBuildView("qx",IPField.THERMALFLUX_X, 1, 1)
mysolver.internalPointBuildView("qy",IPField.THERMALFLUX_Y, 1, 1)
mysolver.internalPointBuildView("qz",IPField.THERMALFLUX_Z, 1, 1)
mysolver.archivingForceOnPhysicalGroup("Face", 1234, 2)
mysolver.archivingForceOnPhysicalGroup("Face", 5678, 2)
mysolver.archivingForceOnPhysicalGroup("Face", 1234, 3)
mysolver.archivingForceOnPhysicalGroup("Face", 5678, 3)
mysolver.archivingNodeDisplacement(1,3,1)
mysolver.archivingNodeDisplacement(5,2,1)

mysolver.solve()

check = TestCheck()
check.equal(5.200204e+06,mysolver.getArchivedForceOnPhysicalGroup("Face", 5678, 3),1.e-6)
check.equal(3.001975e+02,mysolver.getArchivedNodalValue(1,3,mysolver.displacement),1.e-6)
check.equal(7.756259e-07,mysolver.getArchivedNodalValue(5,2,mysolver.displacement),1.e-6)

