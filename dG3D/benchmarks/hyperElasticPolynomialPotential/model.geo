a = {-8.17501805999999930918420432, -7.88733414000000010446456145, -7.81541316000000030328465073};
b = {22.60716137999999730823219579, 21.81603059999999771889633848, 22.82292431999999848812876735};

s = 40.0;

// "Foam" strut
Point(1) = {a[0], a[1], a[2], s};
Point(2) = {b[0], a[1], a[2], s};
Point(3) = {b[0], b[1], a[2], s};
Point(4) = {a[0], b[1], a[2], s};
Point(5) = {a[0], a[1], b[2], s};
Point(6) = {b[0], a[1], b[2], s};
Point(7) = {b[0], b[1], b[2], s};
Point(8) = {a[0], b[1], b[2], s};

Line(1) = {1, 2};
Line(2) = {2, 3};
Line(3) = {3, 4};
Line(4) = {4, 1};
Line(5) = {5, 6};
Line(6) = {6, 7};
Line(7) = {7, 8};
Line(8) = {8, 5};
Line(9) = {1, 5};
Line(10) = {2, 6};
Line(11) = {3, 7};
Line(12) = {4, 8};

Line Loop(1) = {1, 10, -5, -9};
Plane Surface(2) = {1};
Line Loop(3) = {2, 11, -6, -10};
Plane Surface(4) = {3};
Line Loop(5) = {3, 12, -7, -11};
Plane Surface(6) = {5};
Line Loop(7) = {4, 9, -8, -12};
Plane Surface(8) = {7};
Line Loop(9) = {-1, -4, -3, -2};
Plane Surface(10) = {9};
Line Loop(11) = {5, 6, 7, 8};
Plane Surface(12) = {11};

Surface Loop(1) = {2, 4, 6, 8, 10, 12};
Volume(2) = {1};

// Impose periodic mesh.
Periodic Surface {2} = {6} Translate{0, -(b[1]-a[1]), 0};
Periodic Surface {10} = {12} Translate{0, 0, -(b[2]-a[2])};
Periodic Surface {8} = {4} Translate{-(b[0]-a[0]), 0, 0};

// Physical entities.
Physical Volume("Foam domain") = {2};
Physical Surface(1) = {8};
Physical Surface(2) = {4};
Physical Surface(3) = {10};
Physical Surface(4) = {12};
Physical Surface(5) = {2};
Physical Surface(6) = {6};
Physical Point(20) = {5};
