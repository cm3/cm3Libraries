#coding-Utf-8-*-
#need petsc 3.9 or 3.10 in order to compile (not more, not tested).

# COMMENTS
# source open_foam_25_model3 in cm3Libraries
# No load reversal
# PBC
# Contact enabled

# visualize on gmsh: 
# gmsh mesh.mesh disp_step*

import sys
from gmshpy import*
from dG3Dpy import*

# material law
lawnum = 11

# Material law values.
phi = 0.93
E = 3968.0*(1-phi)/12 # Young modulus
nu = 0.001
K = E/3.0/(1-2.0*nu) # bulk modulus
mu = E/2.0/(1+nu) # shear modulus
rho = 2600*(1-phi)

# Creation of the material law.
potential = HyperElasticPolynomialPotential(lawnum, E, nu, False)

C10 = 90.702425492481495
C01 = mu/2.0-C10
C11 = -3.004040098436935
C02 = 0.319855792455043
C20 = 30.715773677746309
C12 = 0.0
C21 = 0.0
C22 = 0.0

D1 = K/2.0
D2 = 78.347862337382026

potential.setSizeDistortionalParameters(3, 3)
potential.setSizeVolumetricParameters(2)

potential.setVolumetricParameter(0, D1);
potential.setVolumetricParameter(1, D2);

potential.setDistortionalParameter(0, 0, 0.0)
potential.setDistortionalParameter(1, 0, C10)
potential.setDistortionalParameter(2, 0, C20)
potential.setDistortionalParameter(0, 1, C01)
potential.setDistortionalParameter(1, 1, C11)
potential.setDistortionalParameter(2, 1, C21)
potential.setDistortionalParameter(0, 2, C02)
potential.setDistortionalParameter(1, 2, C12)
potential.setDistortionalParameter(2, 2, C22)


law = dG3DHyperelasticMaterialLaw(lawnum, rho, potential)
#law.setUseBarF(True)

# Geometry.
geofile  = "model.geo"
meshfile = "model.msh"

# Creation of part domain
nfield = 1 # physical number of volume
dim = 3

myfield = dG3DDomain(1000, nfield, 0, lawnum, 0, dim)

# Solver parameters.
sol = 2		# Gmm=0 (default) Taucs=1 PETsc=2
soltype = 1	# StaticLinear=0 (default) StaticNonLinear=1
nstep = 10000	# number of step (used only if soltype=1)
ftime = 1.0	# Final time (used only if soltype=1)
tol = 1e-6	# relative tolerance for NR scheme (used only if soltype=1)
nstepArch = 100	# Number of step between 2 archiving (used only if soltype=1)

# Creation of solver.
mysolver = nonLinearMechSolver(1000)

mysolver.createModel(geofile,meshfile,3,2)
mysolver.addDomain(myfield)
mysolver.addMaterialLaw(law)
mysolver.Scheme(soltype)
mysolver.Solver(sol)
mysolver.snlData(nstep, ftime, tol)
mysolver.stepBetweenArchiving(nstepArch)

# Time management.
MaxIter = 20		# Maximum number of iterations
StepIncrease = 5	# Number of successfull timestep before reincreasing it
StepReducFactor = 2.0	# Timestep reduction factor
NumberReduction = 50	# Maximum number of timestep reduction: max reduction = pow(StepReducFactor,this)
mysolver.snlManageTimeStep(MaxIter, StepIncrease, StepReducFactor, NumberReduction)


# Boundary conditions (periodic).
mysolver.setSystemType(3)
#mysolver.stiffnessModification(False)

microBC = nonLinearPeriodicBC(1000, 3)
microBC.setOrder(1)
microBC.setBCPhysical(1, 5, 3, 2, 6, 4) # Periodic boundary conditions

method = 0	# Periodic mesh = 0, Lagrange interpolation = 1, Cubic spline interpolation =2,  FE linear= 3, FE Quad = 4
degree = 0	# Order used for polynomial interpolation 
addvertex = True # Polynomial interpolation by mesh vertex = 0, Polynomial interpolation by virtual vertex 

microBC.setPeriodicBCOptions(method, degree, addvertex) 

mysolver.addMicroBC(microBC)
mysolver.activateTest(True)

mysolver.displacementBC("Face", 3, 0, 0.0)
mysolver.displacementBC("Face", 3, 1, 0.0)
mysolver.displacementBC("Face", 3, 2, 0.0)
mysolver.displacementBC("Face", 4, 0, 0.0)
mysolver.displacementBC("Face", 4, 1, 0.0)
mysolver.displacementBC("Face", 4, 2, -24.511) # -24.511

mysolver.archivingForceOnPhysicalGroup("Face",4,2)
mysolver.archivingNodeDisplacement(20, 2, 1)

mysolver.archivingAverageValue(IPField.P_XX)
mysolver.archivingAverageValue(IPField.P_YY)
mysolver.archivingAverageValue(IPField.P_ZZ)
mysolver.archivingAverageValue(IPField.F_XX)
mysolver.archivingAverageValue(IPField.F_YY)
mysolver.archivingAverageValue(IPField.F_ZZ)

# Build views.
mysolver.internalPointBuildView("Green-Lagrange_xx",IPField.STRAIN_XX, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_yy",IPField.STRAIN_YY, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_zz",IPField.STRAIN_ZZ, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_xy",IPField.STRAIN_XY, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_yz",IPField.STRAIN_YZ, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_xz",IPField.STRAIN_XZ, 1, 1);
mysolver.internalPointBuildView("sig_xx",IPField.SIG_XX, 1, 1);
mysolver.internalPointBuildView("sig_yy",IPField.SIG_YY, 1, 1);
mysolver.internalPointBuildView("sig_zz",IPField.SIG_ZZ, 1, 1);
mysolver.internalPointBuildView("sig_xy",IPField.SIG_XY, 1, 1);
mysolver.internalPointBuildView("sig_yz",IPField.SIG_YZ, 1, 1);
mysolver.internalPointBuildView("sig_xz",IPField.SIG_XZ, 1, 1);
mysolver.internalPointBuildView("sig_VM",IPField.SVM, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange equivalent strain",IPField.GL_EQUIVALENT_STRAIN, 1, 1);
mysolver.internalPointBuildView("Equivalent plastic strain",IPField.PLASTICSTRAIN, 1, 1);

# Solve.
mysolver.solve()

check = TestCheck()
check.equal(-5.632180e+03,mysolver.getArchivedForceOnPhysicalGroup("Face", 4, 2),1.e-4)

