% Load experimental data.
a = [-8.17501805999999930918420432, -7.88733414000000010446456145, -7.81541316000000030328465073];
b = [22.60716137999999730823219579, 21.81603059999999771889633848, 22.82292431999999848812876735];
zdim = b(3)-a(3);

data = dlmread('data_jung.csv'); % Height columns for 4 experimental measures
                                 % (2 columns per measure).
                                 % Column 1: relative displacement in [0, 1]
                                 % Column 2: stress in kPa.

% Partition the data for each measurement and convert it to MPa.
dataref = data(1:2391, 3:4);
data2 = data(1:2391, 1:2);
data3 = data(1:2391, 5:6);
data4 = data(1:2391, 7:8);

dataref(:, 2) = dataref(:, 2)/1000;
dataref = dataref(dataref(:, 2)>0, :);

data2(:, 2) = data2(:, 2)/1000;
data2 = data2(data2(:, 2)>0, :);

data3(:, 2) = data3(:, 2)/1000;
data3 = data3(data3(:, 2)>0, :);

data4(:, 2) = data4(:, 2)/1000;
data4 = data4(data4(:, 2)>0, :);

% Load simulation results
F = dlmread('NodalDisplacementPhysical20Num5comp2.csv');
F = -F(:, 2)/zdim;
P = dlmread('Average_P_ZZ.csv');
P = -P(:, 2);

% Plot result
hold on;
plot(dataref(:, 1), dataref(:, 2), '.b', data2(:, 1), data2(:, 2), '.r');
plot(data3(:, 1), data3(:, 2), 'ob', data4(:, 1), data4(:, 2), 'or');
plot(F, P, 'k', 'linewidth', 3);
title('Polynomial hyperelastic model - Periodic BC', 'Fontsize', 18);
xlabel('relative strain', 'Fontsize', 18);
ylabel('stress (MPa)', 'Fontsize', 18);
hl = legend('Data 1', 'Data 2', 'Data 3', 'Data 4', 'Hyper. el. model', 'Location', 'northwest');
set(hl, 'Fontsize', 12);
set(gca, 'Fontsize', 16);
