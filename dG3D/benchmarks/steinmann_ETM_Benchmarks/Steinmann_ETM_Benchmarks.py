#coding-Utf-8-*-
from gmshpy import *


#from dG3DpyDebug import*
from dG3Dpy import*

#script to launch Steinmann benchmark problem with a python script

import math
import pickle

with open("inputdatafile.txt", "rb") as data:
  ftime = pickle.load(data)
  nuSMP = pickle.load(data)
  nuVac = pickle.load(data)
  RintSMP = pickle.load(data)
  RextSMP = pickle.load(data)
  meshfile = pickle.load(data)
  flagUseEMStress = pickle.load(data)
  flagEMFieldDependentShearModulus = pickle.load(data)
  thermalSource = pickle.load(data)
  applyPressureBCForRadialStretch = pickle.load(data)
  applyPressureBCWithAxialStretch = pickle.load(data)
  Pmax = pickle.load(data)
  tApplied = pickle.load(data)
  electricField = pickle.load(data)
  lambda_z = pickle.load(data)
  H_SMP = pickle.load(data)
  flagCompressionTest = pickle.load(data)
  
print('ftime: ', ftime) 
print('nuSMP: ', nuSMP) 
print('nuVac: ', nuVac)
print('RintSMP: ', RintSMP) 
print('RextSMP: ', RextSMP) 
print('meshfile: ', meshfile)
print('flagUseEMStress: ', flagUseEMStress) 
print('flagEMFieldDependentShearModulus: ', flagEMFieldDependentShearModulus)
print('thermalSource: ', thermalSource)
print('applyPressureBCForRadialStretch: ', applyPressureBCForRadialStretch)
print('applyPressureBCWithAxialStretch: ', applyPressureBCWithAxialStretch)
print('Pmax: ', Pmax)
print('tApplied: ', tApplied) 
print('electricField: ', electricField)
print('lambda_z: ', lambda_z)
print('H_SMP: ', H_SMP)
print('flagCompressionTest: ', flagCompressionTest)
  
def disp_radial_x(x,y,z,t,extra):
  theta=math.atan(z/x)
  #print('x=',x,'z=',z)
  #print('theta=',theta)
  #print('radial_x=',extra*math.cos(theta)*t)
  return extra*math.cos(theta)*t

       
def disp_radial_z(x,y,z,t,extra):
  theta=math.atan(z/x)
  #print('x=',x,'z=',z)
  #print('theta=',theta)
  #print('radial_z=',extra*math.sin(theta)*t)
  return extra*math.sin(theta)*t

#==========Electric & magnetic parameters Conducting polymer========================
lx=ly=lz=1.0 # electrical conductivity (S/m) 
seebeck=0.0 #21.e-6
v0=0. # initial electric potential
epsilon_0 = 8.854 * 1.e-12 # vacuum electric permittivity
mag_mu_0 = 4.e-7 * math.pi # vacuum magnetic permeability
epsilon_r_smp = 1.0 # relative elec permittivity of SMP
mag_r_smp = 1.0 # relative mag permeability of SMP
mag_mu_x = mag_mu_y = mag_mu_z = mag_r_smp * mag_mu_0 # mag permeability smp 
magpot0_x = magpot0_y = magpot0_z = 0.0 # initial magnetic potential
Irms = 0. # (Ampere)
freq = 0. # (Hz)
nTurnsCoil = 0
coilLx = coilLy = coilLz = 0 # m
coilW = 0 # m
lawnum= 1 
rho   = 270. # material density (Kg/m^3)
tinitial = 273.+20. # (K)
G= 1.e5 # Shear modulus (Pa)
#nuSMP=0.495 # Poisson ratio
alpha = beta=gamma=0. # parameter of anisotropy
cp=10.*rho # specific heat capacity 
Kx=Ky=Kz=237. # Thermal conductivity (W/m K)
alphaTherm=0.0 #13.e-5 # thermal dilatation
E_matrix = G*2.*(1.+nuSMP) # (Pa)
# use of electric permittivity constants to formulate polarization
epsilon_4 = 0.0
epsilon_6 = 0.0
epsilon_5 = 0.0 #(epsilon_0 * (1.0-epsilon_r_smp)/rho)
# use of magnetic permeability constants to formulate magnetization
mu_7 = 0.0 # magnetic permeability constants of SMP
mu_9 = 0.0 # magnetic permeability constants of SMP
mu_8 = 0.0 #(rho * mag_mu_0)/((1.0/mag_r_smp)-1.0)
# for EM field-dependent shear modulus
# g(I7) = g(1+alpha_e tanh(I7/m_e))
# psiEMMeca = (1+alpha_e tanh(I7/m_e)) psiMeca + I7/(2 mu_7) + I8/(2 mu_8)
alpha_e = 0.0 # dimensionless; check for value in Saxena 2013, Mehnert 2017
m_e = mag_mu_0*mag_mu_0 # T²; check for value in Saxena 2013, Mehnert 2017
# g(I4) = g(1 + g1 I4/g)
#psiEMMeca = (1 + g1 I4/g) psiMeca + epsilon_4 I4/2 + epsilon_5 I5/2
g1 = -1.e-6; # Pa/(Vm^-1)^2 // Mehnert 2016
# application of axial electric field in SMP
# E = VoltageDifference / L (V/m)
VoltageDifference = electricField*H_SMP # to be applied on one lateral surface of SMP
# with 0 V on other surface of SMP
print('VoltageDifference: ',VoltageDifference)

# material law: wire (current carrying copper wire)
# Material parameters for copper
lawnumwire = 10
rhowire = 8960. 
Ewire=120.e9 #youngs modulus
Gwire=48.e9 # Shear modulus
Muwire = 0.34 # Poisson ratio
alphawire = betawire = gammawire = 0. # parameter of anisotropy
cpwire= 385.*rhowire
Kxwire=Kywire=Kzwire= 401. # thermal conductivity tensor components
lxwire=lywire=lzwire=5.77e7 # electrical conductivity (S/m)
seebeckwire=0.0 #6.5e-6
alphaThermwire= 0.0 # thermal dilatation
v0wire = 0.
mag_r_wire = 1.0 # relative mag permeability of wire
mag_mu_x_wire = mag_mu_y_wire = mag_mu_z_wire = mag_r_wire * mag_mu_0 # mag permeability wire 
magpot0_x_wire = magpot0_y_wire = magpot0_z_wire = 0.0 # initial magnetic potential
epsilon_4_wire = 0.0
epsilon_6_wire = 0.0
epsilon_5_wire = 0.0
mu_7_wire = 0.0
mu_9_wire = 0.0 
mu_8_wire = 0.0 
#current=1. # (A)
wireLength= H_SMP # m
wireRadiusOut=4.e-3 # m

# material law: vacuum region interior between wire and smp
lawnumvacIn = 13
rhovac = 1.2 
Gvac=1.e-1 # Shear modulus
#nuVac = 0.25 # Poisson ratio
Evac= 2.0 * Gvac * (1.0 + nuVac) #youngs modulus
alphavac = betavac = gammavac = 0. # parameter of anisotropy
cpvac= 1.e-12 #1012.*rhovac
Kxvac=Kyvac=Kzvac= 1.e-6 #26.e-3 # thermal conductivity tensor components
lxvac=lyvac=lzvac= 1.e-6 # electrical conductivity
seebeckvac= 0.
alphaThermvac= 0.0 # thermal dilatation
v0vac = 0.
mag_r_vac = 1.0 # relative mag permeability of wire
mag_mu_x_vac = mag_mu_y_vac = mag_mu_z_vac = mag_r_vac * mag_mu_0 # mag permeability wire 
magpot0_x_vac = magpot0_y_vac = magpot0_z_vac = 0.0 # initial magnetic potential
epsilon_4_vac = 0.0
epsilon_6_vac = 0.0
epsilon_5_vac = 0.0
mu_7_vac = 0.0
mu_9_vac = 0.0 
mu_8_vac = 0.0 

# material law: vacuum exterior to smp
lawnumvacOut = 16

useFluxT=True
evaluateCurlField = True;
evaluateTemperature = True;

# solver
sol = 2  # Gmm=0 (default) Taucs=1 PETsc=2
soltype = 1 # StaticLinear=0 (default) StaticNonLinear=1, Explicit=2, Multi=3, Implicit=4, Eigen=5
nstep = 100 # 8*EMncycles # number of step (used only if soltype=1)
#ftime = 1.0 # EMncycles/freq; # according to characteristic time
tol=1.e-6  # relative tolerance for NR scheme (used only if soltype=1)
nstepArch=1 # Number of step between 2 archiving (used only if soltype=1)
fullDg = False #O = CG, 1 = DG
space1 = 0 # function space (Lagrange=0)
beta1 = 40.
eqRatio =1.

#  compute solution and BC (given directly to the solver
# creation of law

# material law for smp
# Mechanical law
elasticPotential = NeoHookeanElasticPotential(lawnum,E_matrix,nuSMP);
lawMech = dG3DHyperelasticMaterialLaw(lawnum, rho, elasticPotential);

# Generic Thermo-Mech law

lawTM   = GenericThermoMechanicsDG3DMaterialLaw(lawnum+1,rho,alpha,beta,gamma,tinitial,Kx,Ky,Kz,alphaTherm,alphaTherm,alphaTherm,cp)
lawTM.setMechanicalMaterialLaw(lawMech);
lawTM.setApplyReferenceF(False);

_cp = constantScalarFunction(cp);
lawTM.setLawForCp(_cp);

# ElecMag Generic Thermo-Mech law for smp
lawsmp = ElecMagGenericThermoMechanicsDG3DMaterialLaw(lawnum+2,rho,alpha,beta,gamma,lx,ly,lz,seebeck,v0,mag_mu_x,
mag_mu_y,mag_mu_z,epsilon_4,epsilon_5,epsilon_6,mu_7,mu_8,mu_9,magpot0_x,magpot0_y, magpot0_z, Irms, freq, nTurnsCoil, coilLx, coilLy, coilLz, coilW,useFluxT,evaluateCurlField,evaluateTemperature)

lawsmp.setThermoMechanicalMaterialLaw(lawTM);
lawsmp.setApplyReferenceF(False);
lawsmp.setUseEMStress(flagUseEMStress);
lawsmp.setEMFieldDependentShearModulus(flagEMFieldDependentShearModulus);
if(flagEMFieldDependentShearModulus):
  lawsmp.setEMFieldDependentShearModulusTestParameters(alpha_e,m_e,g1);

pertLawsmp = dG3DMaterialLawWithTangentByPerturbation(lawsmp,1.e-8)

#lawsmp.setUseBarF(True)

# material law for wire
elasticPotentialwire = NeoHookeanElasticPotential(lawnumwire,Ewire,Muwire);
lawMechwire = dG3DHyperelasticMaterialLaw(lawnumwire,rhowire,elasticPotentialwire);

lawTMwire = GenericThermoMechanicsDG3DMaterialLaw(lawnumwire+1,rhowire,alphawire,betawire,gammawire,tinitial,
Kxwire,Kywire,Kzwire,alphaThermwire,alphaThermwire,alphaThermwire,cpwire)
lawTMwire.setMechanicalMaterialLaw(lawMechwire);
lawTMwire.setApplyReferenceF(False);

_cpwire = constantScalarFunction(cpwire);
lawTMwire.setLawForCp(_cpwire);

lawwire = ElecMagGenericThermoMechanicsDG3DMaterialLaw(lawnumwire+2,rhowire,alphawire,betawire,gammawire,lxwire,lywire,lzwire,seebeckwire,
v0wire, mag_mu_x_wire, mag_mu_y_wire, mag_mu_z_wire,epsilon_4_wire,epsilon_5_wire,epsilon_6_wire,mu_7_wire,mu_8_wire,mu_9_wire,magpot0_x_wire,magpot0_y_wire, magpot0_z_wire, Irms, freq, nTurnsCoil, coilLx, coilLy, coilLz,coilW,useFluxT,evaluateCurlField,evaluateTemperature)

lawwire.setThermoMechanicalMaterialLaw(lawTMwire);
lawwire.setApplyReferenceF(False);
lawwire.setUseEMStress(False);
lawwire.setEMFieldDependentShearModulus(False);

#lawwire.setUseBarF(True)

#material law for free space (vacuum) interior between wire and smp
elasticPotentialvacIn = NeoHookeanElasticPotential(lawnumvacIn,Evac,nuVac);
lawMechvacIn = dG3DHyperelasticMaterialLaw(lawnumvacIn, rhovac, elasticPotentialvacIn);

lawTMvacIn = GenericThermoMechanicsDG3DMaterialLaw(lawnumvacIn+1,rhovac,alphavac,betavac,gammavac,tinitial,
Kxvac,Kyvac,Kzvac,alphaThermvac,alphaThermvac,alphaThermvac,cpvac)
lawTMvacIn.setMechanicalMaterialLaw(lawMechvacIn);
lawTMvacIn.setApplyReferenceF(False);

_cpvac = constantScalarFunction(cpvac);
lawTMvacIn.setLawForCp(_cpvac);

lawvacIn = ElecMagGenericThermoMechanicsDG3DMaterialLaw(lawnumvacIn+2,rhovac,alphavac,betavac,gammavac,lxvac,lyvac,lzvac,
seebeckvac,v0vac, mag_mu_x_vac, mag_mu_y_vac, mag_mu_z_vac,epsilon_4_vac,epsilon_5_vac,epsilon_6_vac,mu_7_vac,mu_8_vac,mu_9_vac, magpot0_x_vac,
magpot0_y_vac, magpot0_z_vac, Irms, freq, nTurnsCoil, coilLx, coilLy, coilLz, coilW,useFluxT,evaluateCurlField,evaluateTemperature)

lawvacIn.setThermoMechanicalMaterialLaw(lawTMvacIn);
lawvacIn.setApplyReferenceF(False);
lawvacIn.setUseEMStress(False);
lawvacIn.setEMFieldDependentShearModulus(False);

pertLawvacIn = dG3DMaterialLawWithTangentByPerturbation(lawvacIn,1.e-8)

#lawvacIn.setUseBarF(True)

# creation of ElasticField
SMPfield = 1000 # number of the field (physical number of SMP)
wirefield = 2000 # number of the field (physical number of wire)
VacfieldIn = 3000 # number of the field (physical number of Vacuum interior)
VacfieldOut = 4000 # number of the field (physical number of Vacuum exterior)
SmpRefTop=11111 # top face of outer surface of SMP
SmpOuterShell = 11113 # outer cylindrical surface of smp
SmpInnerShell = 11114 # inner cylindrical surface of smp
SmpRefBottom = 11115 # bottom face of outer surface of SMP
SurfSmpCut_1 = 11132 # cut surface in SMP
SurfSmpCut_2 = 11133 # cut surface in SMP
SurfSmpCut_3 = 11134 # cut surface in SMP
SurfSmpCut_4 = 11135 # cut surface in SMP

SurfwireTop = 2333 # delta voltage surface for wire
SurfwireBottom = 2444 # ground voltage surface for wire
WireRefPoint = 2555 # single point on top face of wire
SurfwireOuterShell = 2666 # outer cylindrical surface of wire
SurfVacInTop = 3333 # top face of vacuum interior
SurfVacInBottom = 3444 # bottom face of vacuum interior
SurfVacOutTop = 4333 # top face of vacuum exterior
SurfVacOutBottom = 4444 # bottom face of vacuum exterior
SurfVacOutOuterShell = 43333 # outer most cylindrical surface of vacuum exterior

VacOutPointTop = 4555 # single point on top face of vacuum exterior at exterior cylindrical face
VacOutPointBottom = 4666 # single point on bottom face of vacuum exterior at exterior cylindrical face

SmpRefTopPoint_Exterior_1=11116 # single point on top face of SMP at exterior cylindrical face
SmpRefTopPoint_Exterior_2=11117 # single point on top face of SMP at exterior cylindrical face
SmpRefTopPoint_Interior_1=11120 # single point on top face of SMP at interior cylindrical face
SmpRefTopPoint_Interior_2=11121 # single point on top face of SMP at interior cylindrical face
SmpRefBottomPoint_Exterior_1=11124 # single point on bottom face of SMP at exterior cylindrical face
SmpRefBottomPoint_Exterior_2=11125 # single point on bottom face of SMP at exterior cylindrical face
SmpRefBottomPoint_Interior_1=11128 # single point on bottom face of SMP at interior cylindrical face
SmpRefBottomPoint_Interior_2=11129 # single point on bottom face of SMP at interior cylindrical face

SMP_field = ElecMagTherMechDG3DDomain(10,SMPfield,space1,lawnum+2,fullDg,eqRatio,3)
Wire_field = ElecMagTherMechDG3DDomain(10,wirefield,space1,lawnumwire+2,fullDg,eqRatio,3)
Vacuum_field_In = ElecMagTherMechDG3DDomain(10,VacfieldIn,space1,lawnumvacIn+2,fullDg,eqRatio,3)
Vacuum_field_Out = ElecMagTherMechDG3DDomain(10,VacfieldOut,space1,lawnumvacIn+2,fullDg,eqRatio,3)

SMP_field.setConstitutiveExtraDofDiffusionEqRatio(eqRatio)
Wire_field.setConstitutiveExtraDofDiffusionEqRatio(eqRatio)
Vacuum_field_In.setConstitutiveExtraDofDiffusionEqRatio(eqRatio)
Vacuum_field_Out.setConstitutiveExtraDofDiffusionEqRatio(eqRatio)
SMP_field.setConstitutiveCurlEqRatio(eqRatio)
Wire_field.setConstitutiveCurlEqRatio(eqRatio)
Vacuum_field_In.setConstitutiveCurlEqRatio(eqRatio)
Vacuum_field_Out.setConstitutiveCurlEqRatio(eqRatio)
SMP_field.stabilityParameters(beta1)
Wire_field.stabilityParameters(beta1)
Vacuum_field_In.stabilityParameters(beta1)
Vacuum_field_Out.stabilityParameters(beta1)

SMP_field.ElecMagTherMechStabilityParameters(beta1,fullDg)
Vacuum_field_In.ElecMagTherMechStabilityParameters(beta1,fullDg)
Vacuum_field_Out.ElecMagTherMechStabilityParameters(beta1,fullDg)
Wire_field.ElecMagTherMechStabilityParameters(beta1,fullDg)

#thermalSource=True
mecaSource=False

SMP_field.setConstitutiveExtraDofDiffusionAccountSource(thermalSource,mecaSource)
Wire_field.setConstitutiveExtraDofDiffusionAccountSource(thermalSource,mecaSource)
Vacuum_field_In.setConstitutiveExtraDofDiffusionAccountSource(thermalSource,mecaSource)
Vacuum_field_Out.setConstitutiveExtraDofDiffusionAccountSource(thermalSource,mecaSource)

#SMP_field.matrixByPerturbation(1,1,1,1.e-3)
#Wire_field.matrixByPerturbation(1,1,1,1.e-3)
#Vacuum_field_In.matrixByPerturbation(1,1,1,1.e-3)
#Vacuum_field_Out.matrixByPerturbation(1,1,1,1.e-3)

# creation of Solver
mysolver = nonLinearMechSolver(1000)
mysolver.loadModel(meshfile)
mysolver.addDomain(SMP_field)
mysolver.addDomain(Wire_field)
mysolver.addDomain(Vacuum_field_In)
mysolver.addDomain(Vacuum_field_Out)
mysolver.addMaterialLaw(lawsmp)
mysolver.addMaterialLaw(lawwire)
mysolver.addMaterialLaw(lawvacIn)
#mysolver.addMaterialLaw(lawvacOut)
mysolver.Scheme(soltype)
mysolver.Solver(sol)
mysolver.options("-ksp_type preonly")
mysolver.options("-pc_type lu")
mysolver.stepBetweenArchiving(nstepArch)
mysolver.snlManageTimeStep(25, 10, 2, 10)
# BC

# Mechanical BC
#Pmax = 47.e3 # Pa

# getting radial stretch lambda_i with fixing axial stretch lambda_z = 1
if(applyPressureBCForRadialStretch):
  mysolver.snlData(nstep, ftime, tol, tol / 10.)  # forsmp
  mysolver.setNumStepTimeInterval(ftime / 10., 1)
  mysolver.setNumStepTimeInterval(ftime, nstep - 1)
  mysolver.displacementBC("Volume",wirefield,0,0.0)
  mysolver.displacementBC("Volume",wirefield,1,0.0)
  mysolver.displacementBC("Volume",wirefield,2,0.0)
  # fix lambda_z = 1 -----------
  mysolver.displacementBC("Face",SmpRefTop,2,0.0)
  mysolver.displacementBC("Face",SmpRefBottom,2,0.0)
  mysolver.displacementBC("Face",SurfVacInTop, 2, 0.0)
  mysolver.displacementBC("Face",SurfVacInBottom, 2, 0.0)
  mysolver.displacementBC("Face",SurfVacOutTop, 2, 0.0)
  mysolver.displacementBC("Face",SurfVacOutBottom, 2, 0.0)

  # get rid of rigid body modes
  mysolver.displacementBC("Face",SurfSmpCut_1,0,0.0)
  mysolver.displacementBC("Face",SurfSmpCut_2,1,0.0)
  mysolver.displacementBC("Face",SurfSmpCut_3,0,0.0)
  mysolver.displacementBC("Face",SurfSmpCut_4,1,0.0)

  mysolver.displacementBC("Face",SurfVacOutOuterShell,2,0.0)

  # pressure BC to get radial stretch
  cyclicFunctionPressure=cycleFunctionTime(0., 0., ftime/10., 0., ftime, Pmax); # Pa
  mysolver.pressureOnPhysicalGroupBC("Face",SmpInnerShell,cyclicFunctionPressure)
  
# combined axial stretch lambda_z with radial stretch lambda_i = 1
elif(applyPressureBCWithAxialStretch):
  mysolver.snlData(nstep, ftime, tol, tol / 10.)  # forsmp
  mysolver.setNumStepTimeInterval(ftime / 10., 1)
  mysolver.setNumStepTimeInterval(2.0*ftime / 10., 100)
  mysolver.setNumStepTimeInterval(ftime, nstep - 1)
  # get lambda_z axial stretch -----------
  # lambda_z = l/L = 1 + Delta_l/L 
  halfFinalDisp_z = 0.5*(lambda_z-1.0)*H_SMP
  print('half disp_z: ', halfFinalDisp_z)
  
  cyclicFunctionDisp1=cycleFunctionTime(0., 0., ftime/10., 0., 2.0*ftime/10., halfFinalDisp_z, ftime, halfFinalDisp_z);
  cyclicFunctionDisp2=cycleFunctionTime(0., 0., ftime/10., 0., 2.0*ftime/10., -halfFinalDisp_z, ftime, -halfFinalDisp_z);
  
  mysolver.displacementBC("Face",SmpRefTop,2,cyclicFunctionDisp1)
  mysolver.displacementBC("Face",SmpRefBottom,2,cyclicFunctionDisp2)
  mysolver.displacementBC("Face",SurfVacInTop, 2, cyclicFunctionDisp1)
  mysolver.displacementBC("Face",SurfVacInBottom, 2, cyclicFunctionDisp2)
  mysolver.displacementBC("Face",SurfVacOutTop, 2, cyclicFunctionDisp1)
  mysolver.displacementBC("Face",SurfVacOutBottom, 2, cyclicFunctionDisp2)

  # pressure BC to get radial stretch
  cyclicFunctionPressure=cycleFunctionTime(0., 0., 2.0*ftime/10., 0., ftime, Pmax); # Pa
  mysolver.pressureOnPhysicalGroupBC("Face",SmpInnerShell,cyclicFunctionPressure)
  
  # get rid of rigid body modes
  mysolver.displacementBC("Face",SurfSmpCut_1,0,0.0)
  mysolver.displacementBC("Face",SurfSmpCut_2,1,0.0)
  mysolver.displacementBC("Face",SurfSmpCut_3,0,0.0)
  mysolver.displacementBC("Face",SurfSmpCut_4,1,0.0)  
  
  # mysolver.displacementBC("Face",SurfVacOutOuterShell,2,0.0)
  mysolver.displacementBC("Volume",wirefield,0,0.0)
  mysolver.displacementBC("Volume",wirefield,1,0.0)
  mysolver.displacementBC("Volume",wirefield,2,0.0)
  
else:
  # default: fix all mechanical displacements
  print('Warning! Unknown mechanical BCs. Fixing all mechanical displacements.')
  mysolver.snlData(nstep, ftime, tol, tol / 10.)  # forsmp
  mysolver.displacementBC("Volume",wirefield,0,0.0)
  mysolver.displacementBC("Volume",wirefield,1,0.0)
  mysolver.displacementBC("Volume",wirefield,2,0.0)
  mysolver.displacementBC("Volume",SMPfield,0,0.0)
  mysolver.displacementBC("Volume",SMPfield,1,0.0)
  mysolver.displacementBC("Volume",SMPfield,2,0.0)
  mysolver.displacementBC("Volume",VacfieldIn,0,0.0)
  mysolver.displacementBC("Volume",VacfieldIn,1,0.0)
  mysolver.displacementBC("Volume",VacfieldIn,2,0.0)
  mysolver.displacementBC("Volume",VacfieldOut,0,0.0)
  mysolver.displacementBC("Volume",VacfieldOut,1,0.0)
  mysolver.displacementBC("Volume",VacfieldOut,2,0.0)

"""
# prescribe radial displacement BC
delta_r=2.0
fct_radial_x = PythonBCfunctionDouble(disp_radial_x,delta_r)
fct_radial_z = PythonBCfunctionDouble(disp_radial_z,delta_r)

mysolver.displacementBC("Face",SmpInnerShell,0,fct_radial_x)
mysolver.displacementBC("Face",SmpInnerShell,2,fct_radial_z)
"""

#thermal BC
cyclicFunctionTemp1=cycleFunctionTime(0., tinitial,ftime,tinitial);

mysolver.initialBC("Volume","Position",SMPfield,3,tinitial)
mysolver.initialBC("Volume","Position",wirefield,3,tinitial)
mysolver.initialBC("Volume","Position",VacfieldIn,3,tinitial)
mysolver.initialBC("Volume","Position",VacfieldOut,3,tinitial)
mysolver.displacementBC("Volume",wirefield,3,cyclicFunctionTemp1)
"""
mysolver.displacementBC("Volume",SMPfield,3,cyclicFunctionTemp1)
mysolver.displacementBC("Volume",VacfieldIn,3,cyclicFunctionTemp1)
mysolver.displacementBC("Volume",VacfieldOut,3,cyclicFunctionTemp1)
"""
#tApplied = 393.; # K
cyclicFunctionTemp2=cycleFunctionTime(0., tinitial, ftime/10., tApplied, ftime,tApplied);
mysolver.displacementBC("Face",SmpOuterShell,3,cyclicFunctionTemp2)
mysolver.displacementBC("Face",SmpInnerShell,3,cyclicFunctionTemp1)
mysolver.displacementBC("Face",SurfVacOutOuterShell,3,cyclicFunctionTemp1)

#electrical BC
#VoltageDifference = 1.e20
mysolver.initialBC("Volume","Position",SMPfield,4,0.0)
mysolver.initialBC("Volume","Position",wirefield,4,0.0)
mysolver.initialBC("Volume","Position",VacfieldIn,4,0.0)
mysolver.initialBC("Volume","Position",VacfieldOut,4,0.0)
cyclicFunctionvolt1=cycleFunctionTime(0., 0., ftime,0.);
mysolver.displacementBC("Face",SmpRefBottom,4,cyclicFunctionvolt1)
cyclicFunctionvolt2=cycleFunctionTime(0., 0., ftime/10., VoltageDifference, ftime, VoltageDifference);
mysolver.displacementBC("Face",SmpRefTop,4,cyclicFunctionvolt2)
mysolver.sameDisplacementBCBetweenTwoGroups("Face", SurfwireTop, SurfwireBottom, 4)
# mysolver.sameDisplacementBCBetweenTwoGroups("Face", SurfVacInTop, SurfVacInBottom, 4)
# mysolver.sameDisplacementBCBetweenTwoGroups("Face", SurfVacOutTop, SurfVacOutBottom, 4)
"""
mysolver.displacementBC("Volume",wirefield,4,cyclicFunctionvolt1)
mysolver.displacementBC("Volume",SMPfield,4,cyclicFunctionvolt1)
mysolver.displacementBC("Volume",VacfieldIn,4,cyclicFunctionvolt1)
mysolver.displacementBC("Volume",VacfieldOut,4,cyclicFunctionvolt1)
"""
#magentic BC
mysolver.curlDisplacementBC("Face",SurfVacOutTop,5,0.0) # comp may also be 5
mysolver.curlDisplacementBC("Face",SurfVacOutBottom,5,0.0) # comp may also be 5
mysolver.curlDisplacementBC("Face",SurfVacInTop,5,0.0) # comp may also be 5
mysolver.curlDisplacementBC("Face",SurfVacInBottom,5,0.0) # comp may also be 5
mysolver.curlDisplacementBC("Face",SmpRefTop,5,0.0) # comp may also be 5
mysolver.curlDisplacementBC("Face",SmpRefBottom,5,0.0) # comp may also be 5

mysolver.curlDisplacementBC("Face",SurfwireTop,5,0.0) # comp may also be 5
mysolver.curlDisplacementBC("Face",SurfwireBottom,5,0.0) # comp may also be 5

#mysolver.curlDisplacementBC("Volume",wirefield,5,0.0) # comp may also be 5
mysolver.curlDisplacementBC("Face",SurfVacOutOuterShell,5,0.0) # comp may also be 5

#Gauging the edges using tree-cotree methods
PhysicalCurves = "" 		# input required as a string of comma separated ints
PhysicalSurfaces = "11111,11115,2333,2444,3333,3444,4333,4444,43333" # input required as a string of comma separated ints
PhysicalVolumes = "1000,2000,3000,4000" # input required as a string of comma separated ints
OutputPhysical = 55 		# input required as a int
mysolver.createTreeForBC(PhysicalCurves,PhysicalSurfaces,PhysicalVolumes,OutputPhysical)
mysolver.curlDisplacementBC("Edge",OutputPhysical,5,0.0)

mysolver.internalPointBuildView("svm",IPField.SVM, 1, 1)
mysolver.internalPointBuildView("sig_xx",IPField.SIG_XX, 1, 1)
mysolver.internalPointBuildView("sig_yy",IPField.SIG_YY, 1, 1)
mysolver.internalPointBuildView("sig_zz",IPField.SIG_ZZ, 1, 1)
mysolver.internalPointBuildView("sig_xy",IPField.SIG_XY, 1, 1)
mysolver.internalPointBuildView("sig_yz",IPField.SIG_YZ, 1, 1)
mysolver.internalPointBuildView("sig_xz",IPField.SIG_XZ, 1, 1)
mysolver.internalPointBuildView("eps_xx",IPField.STRAIN_XX, 1, 1)
mysolver.internalPointBuildView("eps_yy",IPField.STRAIN_YY, 1, 1)
mysolver.internalPointBuildView("eps_zz",IPField.STRAIN_ZZ, 1, 1)
mysolver.internalPointBuildView("eps_xy",IPField.STRAIN_XY, 1, 1)
mysolver.internalPointBuildView("eps_yz",IPField.STRAIN_YZ, 1, 1)
mysolver.internalPointBuildView("eps_xz",IPField.STRAIN_XZ, 1, 1)
mysolver.internalPointBuildView("temperature",IPField.TEMPERATURE, 1, 1)
mysolver.internalPointBuildView("w_AV",IPField.EMFIELDSOURCE, 1, 1)
mysolver.internalPointBuildView("w_T",IPField.THERMALSOURCE, 1, 1)
mysolver.internalPointBuildView("qx",IPField.THERMALFLUX_X, 1, 1)
mysolver.internalPointBuildView("qy",IPField.THERMALFLUX_Y, 1, 1)
mysolver.internalPointBuildView("qz",IPField.THERMALFLUX_Z, 1, 1)
mysolver.internalPointBuildView("voltage",IPField.VOLTAGE, 1, 1)
mysolver.internalPointBuildView("jex",IPField.ELECTRICALFLUX_X, 1, 1)
mysolver.internalPointBuildView("jey",IPField.ELECTRICALFLUX_Y, 1, 1)
mysolver.internalPointBuildView("jez",IPField.ELECTRICALFLUX_Z, 1, 1)
mysolver.internalPointBuildView("ax",IPField.MAGNETICVECTORPOTENTIAL_X, 1, 1)
mysolver.internalPointBuildView("ay",IPField.MAGNETICVECTORPOTENTIAL_Y, 1, 1)
mysolver.internalPointBuildView("az",IPField.MAGNETICVECTORPOTENTIAL_Z, 1, 1)
mysolver.internalPointBuildView("bx",IPField.MAGNETICINDUCTION_X, 1, 1)
mysolver.internalPointBuildView("by",IPField.MAGNETICINDUCTION_Y, 1, 1)
mysolver.internalPointBuildView("bz",IPField.MAGNETICINDUCTION_Z, 1, 1)
mysolver.internalPointBuildView("hx",IPField.MAGNETICFIELD_X, 1, 1)
mysolver.internalPointBuildView("hy",IPField.MAGNETICFIELD_Y, 1, 1)
mysolver.internalPointBuildView("hz",IPField.MAGNETICFIELD_Z, 1, 1)
mysolver.internalPointBuildView("dx",IPField.ELECDISPLACEMENT_X, 1, 1)
mysolver.internalPointBuildView("dy",IPField.ELECDISPLACEMENT_Y, 1, 1)
mysolver.internalPointBuildView("dz",IPField.ELECDISPLACEMENT_Z, 1, 1)
mysolver.internalPointBuildView("js0_x",IPField.INDUCTORSOURCEVECTORFIELD_X, 1, 1)
mysolver.internalPointBuildView("js0_y",IPField.INDUCTORSOURCEVECTORFIELD_Y, 1, 1)
mysolver.internalPointBuildView("js0_z",IPField.INDUCTORSOURCEVECTORFIELD_Z, 1, 1)
mysolver.internalPointBuildView("F_xx",IPField.F_XX, 1, 1)
mysolver.internalPointBuildView("F_xy",IPField.F_XY, 1, 1)
mysolver.internalPointBuildView("F_xz",IPField.F_XZ, 1, 1)
mysolver.internalPointBuildView("F_yx",IPField.F_YX, 1, 1)
mysolver.internalPointBuildView("F_yy",IPField.F_YY, 1, 1)
mysolver.internalPointBuildView("F_yz",IPField.F_YZ, 1, 1)
mysolver.internalPointBuildView("F_zx",IPField.F_ZX, 1, 1)
mysolver.internalPointBuildView("F_zy",IPField.F_ZY, 1, 1)
mysolver.internalPointBuildView("F_zz",IPField.F_ZZ, 1, 1)
mysolver.internalPointBuildView("epl",IPField.PLASTICSTRAIN, 1, 1)
mysolver.internalPointBuildView("jacobian",IPField.JACOBIAN, 1, 1)

mysolver.archivingForceOnPhysicalGroup("Face", SmpInnerShell, 0, 1);
mysolver.archivingForceOnPhysicalGroup("Face", SmpInnerShell, 1, 1);
mysolver.archivingForceOnPhysicalGroup("Face", SmpInnerShell, 2, 1);

mysolver.archivingForceOnPhysicalGroup("Face", SmpRefTop, 0, 1);
mysolver.archivingForceOnPhysicalGroup("Face", SmpRefTop, 1, 1);
mysolver.archivingForceOnPhysicalGroup("Face", SmpRefTop, 2, 1);

mysolver.archivingForceOnPhysicalGroup("Face", SmpRefBottom, 0, 1);
mysolver.archivingForceOnPhysicalGroup("Face", SmpRefBottom, 1, 1);
mysolver.archivingForceOnPhysicalGroup("Face", SmpRefBottom, 2, 1);

mysolver.archivingNodeDisplacement(SmpRefTopPoint_Interior_1,0,1);
mysolver.archivingNodeDisplacement(SmpRefTopPoint_Interior_1,1,1);
mysolver.archivingNodeDisplacement(SmpRefTopPoint_Interior_1,2,1);
mysolver.archivingNodeDisplacement(SmpRefTopPoint_Interior_1,3,1);
mysolver.archivingNodeDisplacement(SmpRefTopPoint_Interior_1,4,1);

mysolver.archivingNodeDisplacement(SmpRefTopPoint_Exterior_1,0,1);
mysolver.archivingNodeDisplacement(SmpRefTopPoint_Exterior_1,1,1);
mysolver.archivingNodeDisplacement(SmpRefTopPoint_Exterior_1,2,1);
mysolver.archivingNodeDisplacement(SmpRefTopPoint_Exterior_1,3,1);
mysolver.archivingNodeDisplacement(SmpRefTopPoint_Exterior_1,4,1);

mysolver.archivingNodeDisplacement(SmpRefBottomPoint_Interior_1,0,1);
mysolver.archivingNodeDisplacement(SmpRefBottomPoint_Interior_1,1,1);
mysolver.archivingNodeDisplacement(SmpRefBottomPoint_Interior_1,2,1);
mysolver.archivingNodeDisplacement(SmpRefBottomPoint_Interior_1,3,1);
mysolver.archivingNodeDisplacement(SmpRefBottomPoint_Interior_1,4,1);

mysolver.archivingNodeDisplacement(SmpRefBottomPoint_Exterior_1,0,1);
mysolver.archivingNodeDisplacement(SmpRefBottomPoint_Exterior_1,1,1);
mysolver.archivingNodeDisplacement(SmpRefBottomPoint_Exterior_1,2,1);
mysolver.archivingNodeDisplacement(SmpRefBottomPoint_Exterior_1,3,1);
mysolver.archivingNodeDisplacement(SmpRefBottomPoint_Exterior_1,4,1);

mysolver.archivingIPOnPhysicalGroup("Volume",SMPfield,IPField.TEMPERATURE,IPField.MEAN_VALUE,nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",SMPfield,IPField.VOLTAGE,IPField.MEAN_VALUE,nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",SMPfield,IPField.EMFIELDSOURCE,IPField.MEAN_VALUE,nstepArch);

mysolver.archivingIPOnPhysicalGroup("Volume",	SMPfield,	IPField.MAGNETICINDUCTION_X,	IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	SMPfield,	IPField.MAGNETICINDUCTION_Y,	IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	SMPfield,	IPField.MAGNETICINDUCTION_Z,	IPField.MEAN_VALUE);

mysolver.archivingIPOnPhysicalGroup("Volume",	SMPfield,	IPField.ELECTRICALFLUX_X,	IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	SMPfield,	IPField.ELECTRICALFLUX_Y,	IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	SMPfield,	IPField.ELECTRICALFLUX_Z,	IPField.MEAN_VALUE);

mysolver.archivingIPOnPhysicalGroup("Volume",	SMPfield, IPField.SVM,		IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	SMPfield, IPField.PLASTICSTRAIN,	IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	SMPfield, IPField.TEMPERATURE,	IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	SMPfield, IPField.STRAIN_XX,	IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	SMPfield, IPField.STRAIN_YY,	IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	SMPfield, IPField.STRAIN_ZZ,	IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	SMPfield, IPField.STRAIN_XY,	IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	SMPfield, IPField.STRAIN_YZ,	IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	SMPfield, IPField.STRAIN_XZ,	IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	SMPfield, IPField.SIG_XX,		IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	SMPfield, IPField.SIG_YY,		IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	SMPfield, IPField.SIG_ZZ,		IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	SMPfield, IPField.SIG_XY,		IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	SMPfield, IPField.SIG_YZ,		IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	SMPfield, IPField.SIG_XZ,		IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	SMPfield, IPField.F_XX,		IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	SMPfield, IPField.F_YY,		IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	SMPfield, IPField.F_ZZ,		IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	SMPfield, IPField.JACOBIAN,	IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	SMPfield, IPField.P_XX,		IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	SMPfield, IPField.P_YY,		IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	SMPfield, IPField.P_ZZ,		IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	SMPfield, IPField.S_XX,		IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	SMPfield, IPField.S_YY,		IPField.MEAN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",	SMPfield, IPField.S_ZZ,		IPField.MEAN_VALUE);

mysolver.solve()

check = TestCheck()
check.equal(0.02317180438387196,mysolver.getArchivedNodalValue(SmpRefTopPoint_Interior_1,0,mysolver.displacement),1.e-6)
check.equal(0.0,mysolver.getArchivedNodalValue(SmpRefTopPoint_Interior_1,1,mysolver.displacement),1.e-6)
check.equal(0.0,mysolver.getArchivedNodalValue(SmpRefTopPoint_Interior_1,2,mysolver.displacement),1.e-6)
check.equal(293.0,mysolver.getArchivedNodalValue(SmpRefTopPoint_Interior_1,3,mysolver.displacement),1.e-6)
check.equal(293.0,mysolver.getArchivedNodalValue(SmpRefTopPoint_Exterior_1,3,mysolver.displacement),1.e-6)
check.equal(2.829408071573597e-06,mysolver.getArchivedNodalValue(SmpRefTopPoint_Interior_1,4,mysolver.displacement),1.e-6)



