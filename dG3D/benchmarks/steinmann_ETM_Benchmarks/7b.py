import numpy as npy
import matplotlib
import matplotlib.pyplot as plt
import pandas as pd
import openpyxl
import matplotlib.ticker as ticker
import pickle
import os
from utils import *

# from scipy.optimize import curve_fit

mehnertDataFolderName = 'mehnert_steinmann_2016_data'
mehnertFileName = '7b.csv'
os.system('cp ' + mehnertDataFolderName + '/' + mehnertFileName + ' ' + os.getcwd())

# Read Mehnert and Steinmann 2017 data to plot
df1 = pd.read_csv(mehnertFileName)
# print(df1)

l1 = df1.iloc[:, 0]
p1 = df1.iloc[:, 1]
l2 = df1.iloc[:, 2]
p2 = df1.iloc[:, 3]

# Magnitude of applied electric fields [kVm-1]
E_field = [0, 15, 30, 45, 60, 75, 90, 105, 120, 135, 150]

# Read Gholap et al. data to plot
gholapDataFolderName = 'gholap_data'

# Tests with lambda_i = 2.0, lambda_z = 1.0 for zeta = 2.5
radial_Stretch_array = []
pressure_array = []

gholapFileName1 = 'pureRadialStretch_zeta_25_E_0_temp_293_NormalPressure.csv'
os.system('cp ' + gholapDataFolderName + '/' + gholapFileName1 + ' ' + os.getcwd())
df1 = pd.read_csv(gholapFileName1,sep=';')
timeStep1 = df1.iloc[:, 0]
radial_Stretch1 = df1.iloc[:, 1]
pressure1 = df1.iloc[:, 2]
radial_Stretch_array.append(radial_Stretch1)
pressure_array.append(pressure1)
# print(pressure1.iat[-1])

gholapFileName2 = 'pureRadialStretch_zeta_25_E_15000_temp_293_NormalPressure.csv'
os.system('cp ' + gholapDataFolderName + '/' + gholapFileName2 + ' ' + os.getcwd())
df2 = pd.read_csv(gholapFileName2,sep=';')
timeStep2 = df2.iloc[:, 0]
radial_Stretch2 = df2.iloc[:, 1]
pressure2 = df2.iloc[:, 2]
radial_Stretch_array.append(radial_Stretch2)
pressure_array.append(pressure2)

gholapFileName3 = 'pureRadialStretch_zeta_25_E_30000_temp_293_NormalPressure.csv'
os.system('cp ' + gholapDataFolderName + '/' + gholapFileName3 + ' ' + os.getcwd())
df3 = pd.read_csv(gholapFileName3,sep=';')
timeStep3 = df3.iloc[:, 0]
radial_Stretch3 = df3.iloc[:, 1]
pressure3 = df3.iloc[:, 2]
radial_Stretch_array.append(radial_Stretch3)
pressure_array.append(pressure3)

gholapFileName4 = 'pureRadialStretch_zeta_25_E_45000_temp_293_NormalPressure.csv'
os.system('cp ' + gholapDataFolderName + '/' + gholapFileName4 + ' ' + os.getcwd())
df4 = pd.read_csv(gholapFileName4,sep=';')
timeStep4 = df4.iloc[:, 0]
radial_Stretch4 = df4.iloc[:, 1]
pressure4 = df4.iloc[:, 2]
radial_Stretch_array.append(radial_Stretch4)
pressure_array.append(pressure4)

gholapFileName5 = 'pureRadialStretch_zeta_25_E_60000_temp_293_NormalPressure.csv'
os.system('cp ' + gholapDataFolderName + '/' + gholapFileName5 + ' ' + os.getcwd())
df5 = pd.read_csv(gholapFileName5,sep=';')
timeStep5 = df5.iloc[:, 0]
radial_Stretch5 = df5.iloc[:, 1]
pressure5 = df5.iloc[:, 2]
radial_Stretch_array.append(radial_Stretch5)
pressure_array.append(pressure5)

gholapFileName6 = 'pureRadialStretch_zeta_25_E_75000_temp_293_NormalPressure.csv'
os.system('cp ' + gholapDataFolderName + '/' + gholapFileName6 + ' ' + os.getcwd())
df6 = pd.read_csv(gholapFileName6,sep=';')
timeStep6 = df6.iloc[:, 0]
radial_Stretch6 = df6.iloc[:, 1]
pressure6 = df6.iloc[:, 2]
radial_Stretch_array.append(radial_Stretch6)
pressure_array.append(pressure6)

gholapFileName7 = 'pureRadialStretch_zeta_25_E_90000_temp_293_NormalPressure.csv'
os.system('cp ' + gholapDataFolderName + '/' + gholapFileName7 + ' ' + os.getcwd())
df7 = pd.read_csv(gholapFileName7,sep=';')
timeStep7 = df7.iloc[:, 0]
radial_Stretch7 = df7.iloc[:, 1]
pressure7 = df7.iloc[:, 2]
radial_Stretch_array.append(radial_Stretch7)
pressure_array.append(pressure7)

gholapFileName8 = 'pureRadialStretch_zeta_25_E_105000_temp_293_NormalPressure.csv'
os.system('cp ' + gholapDataFolderName + '/' + gholapFileName8 + ' ' + os.getcwd())
df8 = pd.read_csv(gholapFileName8,sep=';')
timeStep8 = df8.iloc[:, 0]
radial_Stretch8 = df8.iloc[:, 1]
pressure8 = df8.iloc[:, 2]
radial_Stretch_array.append(radial_Stretch8)
pressure_array.append(pressure8)

gholapFileName9 = 'pureRadialStretch_zeta_25_E_120000_temp_293_NormalPressure.csv'
os.system('cp ' + gholapDataFolderName + '/' + gholapFileName9 + ' ' + os.getcwd())
df9 = pd.read_csv(gholapFileName9,sep=';')
timeStep9 = df9.iloc[:, 0]
radial_Stretch9 = df9.iloc[:, 1]
pressure9 = df9.iloc[:, 2]
radial_Stretch_array.append(radial_Stretch9)
pressure_array.append(pressure9)

gholapFileName10 = 'pureRadialStretch_zeta_25_E_135000_temp_293_NormalPressure.csv'
os.system('cp ' + gholapDataFolderName + '/' + gholapFileName10 + ' ' + os.getcwd())
df10 = pd.read_csv(gholapFileName10,sep=';')
timeStep10 = df10.iloc[:, 0]
radial_Stretch10 = df10.iloc[:, 1]
pressure10 = df10.iloc[:, 2]
radial_Stretch_array.append(radial_Stretch10)
pressure_array.append(pressure10)

gholapFileName11 = 'pureRadialStretch_zeta_25_E_150000_temp_293_NormalPressure.csv'
os.system('cp ' + gholapDataFolderName + '/' + gholapFileName11 + ' ' + os.getcwd())
df11 = pd.read_csv(gholapFileName11,sep=';')
timeStep11 = df11.iloc[:, 0]
radial_Stretch11 = df11.iloc[:, 1]
pressure11 = df11.iloc[:, 2]
radial_Stretch_array.append(radial_Stretch11)
pressure_array.append(pressure11)

# Tests with lambda_i = 0.5, lambda_z = 1.0 for zeta = 2.5
gholapFileName12 = 'pureRadialStretchCompression_zeta_25_E_0_temp_293_NormalPressure.csv'
os.system('cp ' + gholapDataFolderName + '/' + gholapFileName12 + ' ' + os.getcwd())
df12 = pd.read_csv(gholapFileName12,sep=';')
timeStep12 = df12.iloc[:, 0]
radial_Stretch12 = df12.iloc[:, 1]
pressure12 = df12.iloc[:, 2]

gholapFileName13 = 'pureRadialStretchCompression_zeta_25_E_15000_temp_293_NormalPressure.csv'
os.system('cp ' + gholapDataFolderName + '/' + gholapFileName13 + ' ' + os.getcwd())
df13 = pd.read_csv(gholapFileName13,sep=';')
timeStep13 = df13.iloc[:, 0]
radial_Stretch13 = df13.iloc[:, 1]
pressure13 = df13.iloc[:, 2]

gholapFileName14 = 'pureRadialStretchCompression_zeta_25_E_30000_temp_293_NormalPressure.csv'
os.system('cp ' + gholapDataFolderName + '/' + gholapFileName14 + ' ' + os.getcwd())
df14 = pd.read_csv(gholapFileName14,sep=';')
timeStep14 = df14.iloc[:, 0]
radial_Stretch14 = df14.iloc[:, 1]
pressure14 = df14.iloc[:, 2]

gholapFileName15 = 'pureRadialStretchCompression_zeta_25_E_45000_temp_293_NormalPressure.csv'
os.system('cp ' + gholapDataFolderName + '/' + gholapFileName15 + ' ' + os.getcwd())
df15 = pd.read_csv(gholapFileName15,sep=';')
timeStep15 = df15.iloc[:, 0]
radial_Stretch15 = df15.iloc[:, 1]
pressure15 = df15.iloc[:, 2]

gholapFileName16 = 'pureRadialStretchCompression_zeta_25_E_60000_temp_293_NormalPressure.csv'
os.system('cp ' + gholapDataFolderName + '/' + gholapFileName16 + ' ' + os.getcwd())
df16 = pd.read_csv(gholapFileName16,sep=';')
timeStep16 = df16.iloc[:, 0]
radial_Stretch16 = df16.iloc[:, 1]
pressure16 = df16.iloc[:, 2]

gholapFileName17 = 'pureRadialStretchCompression_zeta_25_E_75000_temp_293_NormalPressure.csv'
os.system('cp ' + gholapDataFolderName + '/' + gholapFileName17 + ' ' + os.getcwd())
df17 = pd.read_csv(gholapFileName17,sep=';')
timeStep17 = df17.iloc[:, 0]
radial_Stretch17 = df17.iloc[:, 1]
pressure17 = df17.iloc[:, 2]

gholapFileName18 = 'pureRadialStretchCompression_zeta_25_E_90000_temp_293_NormalPressure.csv'
os.system('cp ' + gholapDataFolderName + '/' + gholapFileName18 + ' ' + os.getcwd())
df18 = pd.read_csv(gholapFileName18,sep=';')
timeStep18 = df18.iloc[:, 0]
radial_Stretch18 = df18.iloc[:, 1]
pressure18 = df18.iloc[:, 2]

gholapFileName19 = 'pureRadialStretchCompression_zeta_25_E_105000_temp_293_NormalPressure.csv'
os.system('cp ' + gholapDataFolderName + '/' + gholapFileName19 + ' ' + os.getcwd())
df19 = pd.read_csv(gholapFileName19,sep=';')
timeStep19 = df19.iloc[:, 0]
radial_Stretch19 = df19.iloc[:, 1]
pressure19 = df19.iloc[:, 2]

gholapFileName20 = 'pureRadialStretchCompression_zeta_25_E_120000_temp_293_NormalPressure.csv'
os.system('cp ' + gholapDataFolderName + '/' + gholapFileName20 + ' ' + os.getcwd())
df20 = pd.read_csv(gholapFileName20,sep=';')
timeStep20 = df20.iloc[:, 0]
radial_Stretch20 = df20.iloc[:, 1]
pressure20 = df20.iloc[:, 2]

gholapFileName21 = 'pureRadialStretchCompression_zeta_25_E_135000_temp_293_NormalPressure.csv'
os.system('cp ' + gholapDataFolderName + '/' + gholapFileName21 + ' ' + os.getcwd())
df21 = pd.read_csv(gholapFileName21,sep=';')
timeStep21 = df21.iloc[:, 0]
radial_Stretch21 = df21.iloc[:, 1]
pressure21 = df21.iloc[:, 2]

gholapFileName22 = 'pureRadialStretchCompression_zeta_25_E_150000_temp_293_NormalPressure.csv'
os.system('cp ' + gholapDataFolderName + '/' + gholapFileName22 + ' ' + os.getcwd())
df22 = pd.read_csv(gholapFileName22,sep=';')
timeStep22 = df22.iloc[:, 0]
radial_Stretch22 = df22.iloc[:, 1]
pressure22 = df22.iloc[:, 2]

lambda_i = 2.00
press_i_array = []
for i in range(0, len(radial_Stretch_array)):
    radial_Stretch_i = radial_Stretch_array[i]
    pressure_i = pressure_array[i]
    ni_, = npy.where(npy.isclose(radial_Stretch_i, lambda_i, rtol=0.01))
    if (radial_Stretch_i[ni_[0]]>lambda_i):
        press_i = pressure_i[ni_[0]]
    elif ((ni_[0] < len(radial_Stretch_i) - 1) and (radial_Stretch_i[ni_[0] + 1] > lambda_i)):
        press_i = pressure_i[ni_[0] + 1]
    else:
        press_i = pressure_i[ni_[0]]
    press_i_array.append(press_i)

# plot data
matplotlib.rc('text', usetex=True)
matplotlib.rc('font', **{'family': 'serif', 'serif': ['Computer Modern Roman'],
                         'monospace': ['Computer Modern Typewriter']})

fig2 = plt.figure(figsize=(6, 6), layout="constrained")
# plt.plot(l1, p1, marker='o',c='blue',linestyle='none',fillstyle='none', label='Mehnert [2016] $\lambda_i=0.5$, $\Theta_e=293$ K', linewidth=1.)
plt.plot(l2, p2, marker='o',c='red',linestyle='none',fillstyle='none', label='Mehnert [2016] $\lambda_i=2.0$, $\Theta_e=293$ K', linewidth=1.)

# inflation test results data plot
# plt.plot(E_field[0],pressure12.iat[-1]/1.e3,marker='X',c='blue',linestyle='none',fillstyle='none',label='Gholap [2024] $\lambda_i=0.5$, $\Theta_e=293$ K', linewidth=1.)
# plt.plot(E_field[1],pressure13.iat[-1]/1.e3,marker='X',c='blue',linestyle='none',fillstyle='none')
# plt.plot(E_field[2],pressure14.iat[-1]/1.e3,marker='X',c='blue',linestyle='none',fillstyle='none')
# plt.plot(E_field[3],pressure15.iat[-1]/1.e3,marker='X',c='blue',linestyle='none',fillstyle='none')
# plt.plot(E_field[4],pressure16.iat[-1]/1.e3,marker='X',c='blue',linestyle='none',fillstyle='none')
# plt.plot(E_field[5],pressure17.iat[-1]/1.e3,marker='X',c='blue',linestyle='none',fillstyle='none')
# plt.plot(E_field[6],pressure18.iat[-1]/1.e3,marker='X',c='blue',linestyle='none',fillstyle='none')
# plt.plot(E_field[7],pressure19.iat[-1]/1.e3,marker='X',c='blue',linestyle='none',fillstyle='none')
# plt.plot(E_field[8],pressure20.iat[-1]/1.e3,marker='X',c='blue',linestyle='none',fillstyle='none')
# plt.plot(E_field[9],pressure21.iat[-1]/1.e3,marker='X',c='blue',linestyle='none',fillstyle='none')
# plt.plot(E_field[10],pressure22.iat[-1]/1.e3,marker='X',c='blue',linestyle='none',fillstyle='none')

plt.plot(E_field[0],press_i_array[0]/1.e3,marker='X',c='red',linestyle='none',fillstyle='none',label='Gholap [2024] $\lambda_i=2.0$, $\Theta_e=293$ K', linewidth=1.)
plt.plot(E_field[1],press_i_array[1]/1.e3,marker='X',c='red',linestyle='none',fillstyle='none')
plt.plot(E_field[2],press_i_array[2]/1.e3,marker='X',c='red',linestyle='none',fillstyle='none')
plt.plot(E_field[3],press_i_array[3]/1.e3,marker='X',c='red',linestyle='none',fillstyle='none')
plt.plot(E_field[4],press_i_array[4]/1.e3,marker='X',c='red',linestyle='none',fillstyle='none')
plt.plot(E_field[5],press_i_array[5]/1.e3,marker='X',c='red',linestyle='none',fillstyle='none')
plt.plot(E_field[6],press_i_array[6]/1.e3,marker='X',c='red',linestyle='none',fillstyle='none')
plt.plot(E_field[7],press_i_array[7]/1.e3,marker='X',c='red',linestyle='none',fillstyle='none')
plt.plot(E_field[8],press_i_array[8]/1.e3,marker='X',c='red',linestyle='none',fillstyle='none')
plt.plot(E_field[9],press_i_array[9]/1.e3,marker='X',c='red',linestyle='none',fillstyle='none')
plt.plot(E_field[10],press_i_array[10]/1.e3,marker='X',c='red',linestyle='none',fillstyle='none')

ax2 = plt.gca()

# popt, pcov = curve_fit(fit_func_,l1,p1)
# ax2.plot(l1,fit_func_(l1,*popt),'r--',label='Curve fit',linewidth=2.)

ax2.set_xlabel(u"$E_0$ [kVm$^{-1}$]", fontsize=16)
ax2.set_ylabel(u"$\overline{N}$ [kPa]", fontsize=16)
ax2.legend(loc='lower center', bbox_to_anchor=(0.5, -0.25), ncols=2, frameon=False, fontsize=9, labelspacing=0.1)

ratio = 1
xleft, xright = ax2.get_xlim()
ybottom, ytop = ax2.get_ylim()
# ax2.set_aspect(abs((xright-xleft)/(ybottom-ytop))*ratio)

ax2.text(70, 110, "$\lambda_i = 2$, \n $\lambda_z = 1$, \n $\zeta = 2.5$", size=16)

ax2.tick_params(direction='in', labelsize=14, axis='both', which='major', pad=7)
# ax2.grid(linestyle='--', linewidth=.3)
ax2.tick_params(bottom=True, top=True, left=True, right=True)

ax2.margins(0.05)
# plt.xticks(npy.arange(0.5,3.1,0.5))
# plt.yticks(npy.arange(-200,101,50))
ax2.xaxis.set_major_locator(ticker.MultipleLocator(20))
ax2.yaxis.set_major_locator(ticker.MultipleLocator(5))

# plt.tight_layout(pad=0.)
fig2.savefig('7b_comparison.png', bbox_inches='tight', dpi=1000)
plt.show()

