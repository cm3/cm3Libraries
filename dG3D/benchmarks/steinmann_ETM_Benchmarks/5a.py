import numpy as npy
import matplotlib
import matplotlib.pyplot as plt
import pandas as pd
import openpyxl
import matplotlib.ticker as ticker
import pickle
import os
from utils import *

# from scipy.optimize import curve_fit

mehnertDataFolderName = 'mehnert_steinmann_2016_data'
mehnertFileName = '5a.csv'
os.system('cp ' + mehnertDataFolderName + '/' + mehnertFileName + ' ' + os.getcwd())

# Read Mehnert and Steinmann 2017 data to plot
df1 = pd.read_csv(mehnertFileName)
# print(df1)

l1 = df1.iloc[:, 0]
p1 = df1.iloc[:, 1]
l2 = df1.iloc[:, 2]
p2 = df1.iloc[:, 3]
l3 = df1.iloc[:, 4]
p3 = df1.iloc[:, 5]
l4 = df1.iloc[:, 6]
p4 = df1.iloc[:, 7]
l5 = df1.iloc[:, 8]
p5 = df1.iloc[:, 9]
l6 = df1.iloc[:, 10]
p6 = df1.iloc[:, 11]

# Read Gholap et al. data to plot
gholapDataFolderName = 'gholap_data'

# no EM fields (pure TM simulation)
gholapFileName2 = 'pureRadialStretchCompression_zeta_15_E_0_temp_293_NormalPressure.csv'
os.system('cp ' + gholapDataFolderName + '/' + gholapFileName2 + ' ' + os.getcwd())
df2 = pd.read_csv(gholapFileName2,sep=';')
timeStep2 = df2.iloc[:, 0]
radial_Stretch2 = df2.iloc[:, 1]
pressure2 = df2.iloc[:, 2]

gholapFileName3 = 'pureRadialStretchCompression_zeta_20_E_0_temp_293_NormalPressure.csv'
os.system('cp ' + gholapDataFolderName + '/' + gholapFileName3 + ' ' + os.getcwd())
df3 = pd.read_csv(gholapFileName3,sep=';')
timeStep3 = df3.iloc[:, 0]
radial_Stretch3 = df3.iloc[:, 1]
pressure3 = df3.iloc[:, 2]

gholapFileName4 = 'pureRadialStretchCompression_zeta_25_E_0_temp_293_NormalPressure.csv'
os.system('cp ' + gholapDataFolderName + '/' + gholapFileName4 + ' ' + os.getcwd())
df4 = pd.read_csv(gholapFileName4,sep=';')
timeStep4 = df4.iloc[:, 0]
radial_Stretch4 = df4.iloc[:, 1]
pressure4 = df4.iloc[:, 2]

gholapFileName5 = 'pureRadialStretch_zeta_15_E_0_temp_293_NormalPressure.csv'
os.system('cp ' + gholapDataFolderName + '/' + gholapFileName5 + ' ' + os.getcwd())
df5 = pd.read_csv(gholapFileName5,sep=';')
timeStep5 = df5.iloc[:, 0]
radial_Stretch5 = df5.iloc[:, 1]
pressure5 = df5.iloc[:, 2]

gholapFileName6 = 'pureRadialStretch_zeta_20_E_0_temp_293_NormalPressure.csv'
os.system('cp ' + gholapDataFolderName + '/' + gholapFileName6 + ' ' + os.getcwd())
df6 = pd.read_csv(gholapFileName6,sep=';')
timeStep6 = df6.iloc[:, 0]
radial_Stretch6 = df6.iloc[:, 1]
pressure6 = df6.iloc[:, 2]

gholapFileName7 = 'pureRadialStretch_zeta_25_E_0_temp_293_NormalPressure.csv'
os.system('cp ' + gholapDataFolderName + '/' + gholapFileName7 + ' ' + os.getcwd())
df7 = pd.read_csv(gholapFileName7,sep=';')
timeStep7 = df7.iloc[:, 0]
radial_Stretch7 = df7.iloc[:, 1]
pressure7 = df7.iloc[:, 2]

# with EM field-dependent shear modulus test results
gholapFileName8 = 'pureRadialStretchCompression_zeta_15_E_100000_temp_293_NormalPressure.csv'
os.system('cp ' + gholapDataFolderName + '/' + gholapFileName8 + ' ' + os.getcwd())
df8 = pd.read_csv(gholapFileName8,sep=';')
timeStep8 = df8.iloc[:, 0]
radial_Stretch8 = df8.iloc[:, 1]
pressure8 = df8.iloc[:, 2]

gholapFileName9= 'pureRadialStretchCompression_zeta_20_E_100000_temp_293_NormalPressure.csv'
os.system('cp ' + gholapDataFolderName + '/' + gholapFileName9 + ' ' + os.getcwd())
df9 = pd.read_csv(gholapFileName9,sep=';')
timeStep9 = df9.iloc[:, 0]
radial_Stretch9 = df9.iloc[:, 1]
pressure9 = df9.iloc[:, 2]

gholapFileName10 = 'pureRadialStretchCompression_zeta_25_E_100000_temp_293_NormalPressure.csv'
os.system('cp ' + gholapDataFolderName + '/' + gholapFileName10 + ' ' + os.getcwd())
df10 = pd.read_csv(gholapFileName10,sep=';')
timeStep10 = df10.iloc[:, 0]
radial_Stretch10 = df10.iloc[:, 1]
pressure10 = df10.iloc[:, 2]

gholapFileName11 = 'pureRadialStretch_zeta_15_E_100000_temp_293_NormalPressure.csv'
os.system('cp ' + gholapDataFolderName + '/' + gholapFileName11 + ' ' + os.getcwd())
df11 = pd.read_csv(gholapFileName11,sep=';')
timeStep11 = df11.iloc[:, 0]
radial_Stretch11 = df11.iloc[:, 1]
pressure11 = df11.iloc[:, 2]

gholapFileName12 = 'pureRadialStretch_zeta_20_E_100000_temp_293_NormalPressure.csv'
os.system('cp ' + gholapDataFolderName + '/' + gholapFileName12 + ' ' + os.getcwd())
df12 = pd.read_csv(gholapFileName12,sep=';')
timeStep12 = df12.iloc[:, 0]
radial_Stretch12 = df12.iloc[:, 1]
pressure12 = df12.iloc[:, 2]

gholapFileName13 = 'pureRadialStretch_zeta_25_E_100000_temp_293_NormalPressure.csv'
os.system('cp ' + gholapDataFolderName + '/' + gholapFileName13 + ' ' + os.getcwd())
df13 = pd.read_csv(gholapFileName13,sep=';')
timeStep13 = df13.iloc[:, 0]
radial_Stretch13 = df13.iloc[:, 1]
pressure13 = df13.iloc[:, 2]

# plot data
matplotlib.rc('text', usetex=True)
matplotlib.rc('font', **{'family': 'serif', 'serif': ['Computer Modern Roman'],
                         'monospace': ['Computer Modern Typewriter']})

fig2 = plt.figure(figsize=(6, 6), layout="constrained")
plt.plot(l1, p1, marker='o',c='blue',linestyle='none',fillstyle='full', label='Mehnert [2016] $E_0=0$  kVm$^{-1}$, $\zeta=1.5$', linewidth=1.)
plt.plot(l2, p2, marker='X',c='red',linestyle='none',fillstyle='full', label='Mehnert [2016] $E_0=0$  kVm$^{-1}$, $\zeta=2$', linewidth=1.)
plt.plot(l3, p3, marker='s',c='black',linestyle='none',fillstyle='full', label='Mehnert [2016] $E_0=0$  kVm$^{-1}$, $\zeta=2.5$', linewidth=1.)
plt.plot(l4, p4, marker='o',c='blue',linestyle='none', fillstyle='none', label='Mehnert [2016] $E_0=100$  kVm$^{-1}$, $\zeta=1.5$', linewidth=1.)
plt.plot(l5, p5, marker='X',c='red',linestyle='none', fillstyle='none', label='Mehnert [2016] $E_0=100$  kVm$^{-1}$, $\zeta=2$', linewidth=1.)
plt.plot(l6, p6, marker='s',c='black',linestyle='none', fillstyle='none', label='Mehnert [2016] $E_0=100$  kVm$^{-1}$, $\zeta=2.5$', linewidth=1.)

# no EM fields (pure TM simulation)
# compression test results data plot
plt.plot(radial_Stretch2,pressure2/1.e3,c='blue',linestyle='-',fillstyle='full',linewidth=1.)
plt.plot(radial_Stretch3,pressure3/1.e3,c='red',linestyle='-',fillstyle='full',linewidth=1.)
plt.plot(radial_Stretch4,pressure4/1.e3,c='black',linestyle='-',fillstyle='full',linewidth=1.)
# inflation test results data plot
plt.plot(radial_Stretch5,pressure5/1.e3,c='blue',linestyle='-',fillstyle='full',label='Gholap [2024] $E_0=0$  kVm$^{-1}$, $\zeta=1.5$', linewidth=1.)
plt.plot(radial_Stretch6,pressure6/1.e3,c='red',linestyle='-',fillstyle='full',label='Gholap [2024] $E_0=0$  kVm$^{-1}$, $\zeta=2$', linewidth=1.)
plt.plot(radial_Stretch7,pressure7/1.e3,c='black',linestyle='-',fillstyle='full',label='Gholap [2024] $E_0=0$  kVm$^{-1}$, $\zeta=2.5$', linewidth=1.)

# with EM field-dependent shear modulus test results
# compression test results data plot
plt.plot(radial_Stretch8,pressure8/1.e3,c='blue',linestyle='--',fillstyle='full',linewidth=1.5)
plt.plot(radial_Stretch9,pressure9/1.e3,c='red',linestyle='--',fillstyle='full',linewidth=1.5)
plt.plot(radial_Stretch10,pressure10/1.e3,c='black',linestyle='--',fillstyle='full',linewidth=1.5)
# inflation test results data plot
plt.plot(radial_Stretch11,pressure11/1.e3,c='blue',linestyle='--',fillstyle='full',label='Gholap [2024] $E_0=100$  kVm$^{-1}$, $\zeta=1.5$', linewidth=1.5)
plt.plot(radial_Stretch12,pressure12/1.e3,c='red',linestyle='--',fillstyle='full',label='Gholap [2024] $E_0=100$  kVm$^{-1}$, $\zeta=2$', linewidth=1.5)
plt.plot(radial_Stretch13,pressure13/1.e3,c='black',linestyle='--',fillstyle='full',label='Gholap [2024] $E_0=100$  kVm$^{-1}$, $\zeta=2.5$', linewidth=1.5)

ax2 = plt.gca()

# popt, pcov = curve_fit(fit_func_,l1,p1)
# ax2.plot(l1,fit_func_(l1,*popt),'r--',label='Curve fit',linewidth=2.)

ax2.set_xlabel(u"$\lambda_i$", fontsize=16)
ax2.set_ylabel(u"$\overline{N}$ [kPa]", fontsize=16)
ax2.legend(loc='lower center', bbox_to_anchor=(0.5, -0.45), ncols=2, frameon=False, fontsize=9, labelspacing=0.1)

ratio = 1
xleft, xright = ax2.get_xlim()
ybottom, ytop = ax2.get_ylim()
# ax2.set_aspect(abs((xright-xleft)/(ybottom-ytop))*ratio)

ax2.text(1.8, -50, "$\lambda_z = 1$", size=16)

ax2.tick_params(direction='in', labelsize=14, axis='both', which='major', pad=7)
# ax2.grid(linestyle='--', linewidth=.3)
ax2.tick_params(bottom=True, top=True, left=True, right=True)

ax2.margins(0.05)
# plt.xticks(npy.arange(0.5,3.1,0.5))
# plt.yticks(npy.arange(-200,101,50))
ax2.xaxis.set_major_locator(ticker.MultipleLocator(0.5))
ax2.yaxis.set_major_locator(ticker.MultipleLocator(50))

# plt.tight_layout(pad=0.)
fig2.savefig('5a_comparison.png', bbox_inches='tight', dpi=1000)
plt.show()