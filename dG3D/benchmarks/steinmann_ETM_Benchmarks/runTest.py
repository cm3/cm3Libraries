import os,csv
import sys
import numpy as np
import pickle
import math

RextArray = np.array([1.5e-2, 2.e-2, 2.5e-2]);
meshfileArray = np.array(['newDorfmannTestGeom_1.msh', 'newDorfmannTestGeom_2.msh', 'newDorfmannTestGeom_3.msh']);
electricFieldArray = np.array([0.0, 1.e5]); # V/m
tAppliedArray = np.array([193., 293., 393.]);

ftime = 1.0
nuSMP = 0.495 # Poisson ratio
nuVac = 0.25 # Poisson ratio
RintSMP = 1.e-2 # m
H_SMP = 2.e-3 # m
# flagCompressionTest = False # if compression stretch radial

# depending on external radius of smp, choose relevant mesh file
# RextSMP = RextArray[0] # m
# meshfile = meshfileArray[0] # name of mesh file
# flagUseEMStress = True
# flagEMFieldDependentShearModulus = True
#lambda_zArray = np.array([0.5,0.75,1.0,1.25,1.5,1.75,2.0]);
# lambda_z = 1.0 # axial stretch = l/L

# use thermal source or not: (capacitive + meca + EM) thermal source
thermalSource = False 

# to apply appropriate pressure BC:
# either on inner surface for radial inflation or compression
# or on start and end surface for axial tensile or compressive stretch
# or both in case of some radial stretch and then axial stretch
# applyPressureBCForRadialStretch = True
# applyPressureBCWithAxialStretch = False

#pressure data from mehnert et al.
# plot 2a lambda_z = 1 purely radial stretch lambda_i = 2.5:
#zeta=1.5; RextSMP = RextArray[0]; meshfile = meshfileArray[0]; electricField=0.; flagUseEMStress = False; flagEMFieldDependentShearModulus = False; applyPressureBCForRadialStretch = True; applyPressureBCWithAxialStretch = False; lambda_z = 1.0; flagCompressionTest = False; Pmax = 38761 #Pa
#zeta=2.0; RextSMP = RextArray[1]; meshfile = meshfileArray[1]; electricField=0.; flagUseEMStress = False; flagEMFieldDependentShearModulus = False; applyPressureBCForRadialStretch = True; applyPressureBCWithAxialStretch = False; lambda_z = 1.0; flagCompressionTest = False; Pmax = 63615
#zeta=2.5; RextSMP = RextArray[2]; meshfile = meshfileArray[2]; electricField=0.; flagUseEMStress = False; flagEMFieldDependentShearModulus = False; applyPressureBCForRadialStretch = True; applyPressureBCWithAxialStretch = False; lambda_z = 1.0; flagCompressionTest = False; Pmax = 80616
zeta=1.5; RextSMP = RextArray[0]; meshfile = meshfileArray[0]; electricField=1.e5; flagUseEMStress = True; flagEMFieldDependentShearModulus = True; applyPressureBCForRadialStretch = True; applyPressureBCWithAxialStretch = False; lambda_z = 1.0; flagCompressionTest = False; Pmax = 34400
#zeta=2.0; RextSMP = RextArray[1]; meshfile = meshfileArray[1]; electricField=1.e5; flagUseEMStress = True; flagEMFieldDependentShearModulus = True; applyPressureBCForRadialStretch = True; applyPressureBCWithAxialStretch = False; lambda_z = 1.0; flagCompressionTest = False; Pmax = 56638
#zeta=2.5; RextSMP = RextArray[2]; meshfile = meshfileArray[2]; electricField=1.e5; flagUseEMStress = True; flagEMFieldDependentShearModulus = True; applyPressureBCForRadialStretch = True; applyPressureBCWithAxialStretch = False; lambda_z = 1.0; flagCompressionTest = False; Pmax = 72336
# plot 2a lambda_z = 1 purely radial compression lambda_i = 0.5:
#zeta=1.5; RextSMP = RextArray[0]; meshfile = meshfileArray[0]; electricField=0.; flagUseEMStress = False; flagEMFieldDependentShearModulus = False; applyPressureBCForRadialStretch = True; applyPressureBCWithAxialStretch = False; lambda_z = 1.0; flagCompressionTest = True; Pmax = -174709 #Pa
#zeta=2.0; RextSMP = RextArray[1]; meshfile = meshfileArray[1]; electricField=0.; flagUseEMStress = False; flagEMFieldDependentShearModulus = False; applyPressureBCForRadialStretch = True; applyPressureBCWithAxialStretch = False; lambda_z = 1.0; flagCompressionTest = True; Pmax = -197820
#zeta=2.5; RextSMP = RextArray[2]; meshfile = meshfileArray[2]; electricField=0.; flagUseEMStress = False; flagEMFieldDependentShearModulus = False; applyPressureBCForRadialStretch = True; applyPressureBCWithAxialStretch = False; lambda_z = 1.0; flagCompressionTest = True; Pmax = -185611
#zeta=1.5; RextSMP = RextArray[0]; meshfile = meshfileArray[0]; electricField=1.e5; flagUseEMStress = True; flagEMFieldDependentShearModulus = True; applyPressureBCForRadialStretch = True; applyPressureBCWithAxialStretch = False; lambda_z = 1.0; flagCompressionTest = True; Pmax = -156832
#zeta=2.0; RextSMP = RextArray[1]; meshfile = meshfileArray[1]; electricField=1.e5; flagUseEMStress = True; flagEMFieldDependentShearModulus = True; applyPressureBCForRadialStretch = True; applyPressureBCWithAxialStretch = False; lambda_z = 1.0; flagCompressionTest = True; Pmax = -177762
#zeta=2.5; RextSMP = RextArray[2]; meshfile = meshfileArray[2]; electricField=1.e5; flagUseEMStress = True; flagEMFieldDependentShearModulus = True; applyPressureBCForRadialStretch = True; applyPressureBCWithAxialStretch = False; lambda_z = 1.0; flagCompressionTest = True; Pmax = -162941

# plot 2b lambda_i = 2 combined radial and axial stretch: 
#zeta=1.5; RextSMP = RextArray[0]; meshfile = meshfileArray[0]; electricField=0.; flagUseEMStress = False; flagEMFieldDependentShearModulus = False; applyPressureBCForRadialStretch = False; applyPressureBCWithAxialStretch = True; lambda_z = 0.5; flagCompressionTest = False; Pmax = 51814 #Pa
#zeta=1.5; RextSMP = RextArray[0]; meshfile = meshfileArray[0]; electricField=0.; flagUseEMStress = False; flagEMFieldDependentShearModulus = False; applyPressureBCForRadialStretch = False; applyPressureBCWithAxialStretch = True; lambda_z = 1.0; flagCompressionTest = False; Pmax = 35965
#zeta=1.5; RextSMP = RextArray[0]; meshfile = meshfileArray[0]; electricField=0.; flagUseEMStress = False; flagEMFieldDependentShearModulus = False; applyPressureBCForRadialStretch = False; applyPressureBCWithAxialStretch = True; lambda_z = 1.5; flagCompressionTest = False; Pmax = 25486
#zeta=1.5; RextSMP = RextArray[0]; meshfile = meshfileArray[0]; electricField=0.; flagUseEMStress = False; flagEMFieldDependentShearModulus = False; applyPressureBCForRadialStretch = False; applyPressureBCWithAxialStretch = True; lambda_z = 2.0; flagCompressionTest = False; Pmax = 19652
#zeta=2.0; RextSMP = RextArray[1]; meshfile = meshfileArray[1]; electricField=0.; flagUseEMStress = False; flagEMFieldDependentShearModulus = False; applyPressureBCForRadialStretch = False; applyPressureBCWithAxialStretch = True; lambda_z = 0.5; flagCompressionTest = False; Pmax = 76923
#zeta=2.0; RextSMP = RextArray[1]; meshfile = meshfileArray[1]; electricField=0.; flagUseEMStress = False; flagEMFieldDependentShearModulus = False; applyPressureBCForRadialStretch = False; applyPressureBCWithAxialStretch = True; lambda_z = 1.0; flagCompressionTest = False; Pmax = 57446
#zeta=2.0; RextSMP = RextArray[1]; meshfile = meshfileArray[1]; electricField=0.; flagUseEMStress = False; flagEMFieldDependentShearModulus = False; applyPressureBCForRadialStretch = False; applyPressureBCWithAxialStretch = True; lambda_z = 1.5; flagCompressionTest = False; Pmax = 41887
#zeta=2.0; RextSMP = RextArray[1]; meshfile = meshfileArray[1]; electricField=0.; flagUseEMStress = False; flagEMFieldDependentShearModulus = False; applyPressureBCForRadialStretch = False; applyPressureBCWithAxialStretch = True; lambda_z = 2.0; flagCompressionTest = False; Pmax = 32569
#zeta=2.5; RextSMP = RextArray[2]; meshfile = meshfileArray[2]; electricField=0.; flagUseEMStress = False; flagEMFieldDependentShearModulus = False; applyPressureBCForRadialStretch = False; applyPressureBCWithAxialStretch = True; lambda_z = 0.5; flagCompressionTest = False; Pmax = 90711
#zeta=2.5; RextSMP = RextArray[2]; meshfile = meshfileArray[2]; electricField=0.; flagUseEMStress = False; flagEMFieldDependentShearModulus = False; applyPressureBCForRadialStretch = False; applyPressureBCWithAxialStretch = True; lambda_z = 1.0; flagCompressionTest = False; Pmax = 70943
#zeta=2.5; RextSMP = RextArray[2]; meshfile = meshfileArray[2]; electricField=0.; flagUseEMStress = False; flagEMFieldDependentShearModulus = False; applyPressureBCForRadialStretch = False; applyPressureBCWithAxialStretch = True; lambda_z = 1.5; flagCompressionTest = False; Pmax = 53063
#zeta=2.5; RextSMP = RextArray[2]; meshfile = meshfileArray[2]; electricField=0.; flagUseEMStress = False; flagEMFieldDependentShearModulus = False; applyPressureBCForRadialStretch = False; applyPressureBCWithAxialStretch = True; lambda_z = 2.0; flagCompressionTest = False; Pmax = 41858
#zeta=1.5; RextSMP = RextArray[0]; meshfile = meshfileArray[0]; electricField=1.e5; flagUseEMStress = True; flagEMFieldDependentShearModulus = True; applyPressureBCForRadialStretch = False; applyPressureBCWithAxialStretch = True; lambda_z = 0.5; flagCompressionTest = False; Pmax = 46589
#zeta=1.5; RextSMP = RextArray[0]; meshfile = meshfileArray[0]; electricField=1.e5; flagUseEMStress = True; flagEMFieldDependentShearModulus = True; applyPressureBCForRadialStretch = False; applyPressureBCWithAxialStretch = True; lambda_z = 1.0; flagCompressionTest = False; Pmax = 32337
#zeta=1.5; RextSMP = RextArray[0]; meshfile = meshfileArray[0]; electricField=1.e5; flagUseEMStress = True; flagEMFieldDependentShearModulus = True; applyPressureBCForRadialStretch = False; applyPressureBCWithAxialStretch = True; lambda_z = 1.5; flagCompressionTest = False; Pmax = 23019
#zeta=1.5; RextSMP = RextArray[0]; meshfile = meshfileArray[0]; electricField=1.e5; flagUseEMStress = True; flagEMFieldDependentShearModulus = True; applyPressureBCForRadialStretch = False; applyPressureBCWithAxialStretch = True; lambda_z = 2.0; flagCompressionTest = False; Pmax = 17620
#zeta=2.0; RextSMP = RextArray[1]; meshfile = meshfileArray[1]; electricField=1.e5; flagUseEMStress = True; flagEMFieldDependentShearModulus = True; applyPressureBCForRadialStretch = False; applyPressureBCWithAxialStretch = True; lambda_z = 0.5; flagCompressionTest = False; Pmax = 69376
#zeta=2.0; RextSMP = RextArray[1]; meshfile = meshfileArray[1]; electricField=1.e5; flagUseEMStress = True; flagEMFieldDependentShearModulus = True; applyPressureBCForRadialStretch = False; applyPressureBCWithAxialStretch = True; lambda_z = 1.0; flagCompressionTest = False; Pmax = 51640
#zeta=2.0; RextSMP = RextArray[1]; meshfile = meshfileArray[1]; electricField=1.e5; flagUseEMStress = True; flagEMFieldDependentShearModulus = True; applyPressureBCForRadialStretch = False; applyPressureBCWithAxialStretch = True; lambda_z = 1.5; flagCompressionTest = False; Pmax = 37823
#zeta=2.0; RextSMP = RextArray[1]; meshfile = meshfileArray[1]; electricField=1.e5; flagUseEMStress = True; flagEMFieldDependentShearModulus = True; applyPressureBCForRadialStretch = False; applyPressureBCWithAxialStretch = True; lambda_z = 2.0; flagCompressionTest = False; Pmax = 29376
#zeta=2.5; RextSMP = RextArray[2]; meshfile = meshfileArray[2]; electricField=1.e5; flagUseEMStress = True; flagEMFieldDependentShearModulus = True; applyPressureBCForRadialStretch = False; applyPressureBCWithAxialStretch = True; lambda_z = 0.5; flagCompressionTest = False; Pmax = 81568
#zeta=2.5; RextSMP = RextArray[2]; meshfile = meshfileArray[2]; electricField=1.e5; flagUseEMStress = True; flagEMFieldDependentShearModulus = True; applyPressureBCForRadialStretch = False; applyPressureBCWithAxialStretch = True; lambda_z = 1.0; flagCompressionTest = False; Pmax = 63832
#zeta=2.5; RextSMP = RextArray[2]; meshfile = meshfileArray[2]; electricField=1.e5; flagUseEMStress = True; flagEMFieldDependentShearModulus = True; applyPressureBCForRadialStretch = False; applyPressureBCWithAxialStretch = True; lambda_z = 1.5; flagCompressionTest = False; Pmax = 47838
#zeta=2.5; RextSMP = RextArray[2]; meshfile = meshfileArray[2]; electricField=1.e5; flagUseEMStress = True; flagEMFieldDependentShearModulus = True; applyPressureBCForRadialStretch = False; applyPressureBCWithAxialStretch = True; lambda_z = 2.0; flagCompressionTest = False; Pmax = 37649

# plot 4b lambda_z = 1 purely radial stretch lambda_i = 2.0:
#zeta=2.5; RextSMP = RextArray[2]; meshfile = meshfileArray[2]; electricField=15.e3; flagUseEMStress = True; flagEMFieldDependentShearModulus = True; applyPressureBCForRadialStretch = True; applyPressureBCWithAxialStretch = False; lambda_z = 1.0; flagCompressionTest = False; Pmax = 71399
#zeta=2.5; RextSMP = RextArray[2]; meshfile = meshfileArray[2]; electricField=30.e3; flagUseEMStress = True; flagEMFieldDependentShearModulus = True; applyPressureBCForRadialStretch = True; applyPressureBCWithAxialStretch = False; lambda_z = 1.0; flagCompressionTest = False; Pmax = 70355
#zeta=2.5; RextSMP = RextArray[2]; meshfile = meshfileArray[2]; electricField=45.e3; flagUseEMStress = True; flagEMFieldDependentShearModulus = True; applyPressureBCForRadialStretch = True; applyPressureBCWithAxialStretch = False; lambda_z = 1.0; flagCompressionTest = False; Pmax = 69833
#zeta=2.5; RextSMP = RextArray[2]; meshfile = meshfileArray[2]; electricField=60.e3; flagUseEMStress = True; flagEMFieldDependentShearModulus = True; applyPressureBCForRadialStretch = True; applyPressureBCWithAxialStretch = False; lambda_z = 1.0; flagCompressionTest = False; Pmax = 68267
#zeta=2.5; RextSMP = RextArray[2]; meshfile = meshfileArray[2]; electricField=75.e3; flagUseEMStress = True; flagEMFieldDependentShearModulus = True; applyPressureBCForRadialStretch = True; applyPressureBCWithAxialStretch = False; lambda_z = 1.0; flagCompressionTest = False; Pmax = 67223
#zeta=2.5; RextSMP = RextArray[2]; meshfile = meshfileArray[2]; electricField=90.e3; flagUseEMStress = True; flagEMFieldDependentShearModulus = True; applyPressureBCForRadialStretch = True; applyPressureBCWithAxialStretch = False; lambda_z = 1.0; flagCompressionTest = False; Pmax = 65136
#zeta=2.5; RextSMP = RextArray[2]; meshfile = meshfileArray[2]; electricField=105.e3; flagUseEMStress = True; flagEMFieldDependentShearModulus = True; applyPressureBCForRadialStretch = True; applyPressureBCWithAxialStretch = False; lambda_z = 1.0; flagCompressionTest = False; Pmax = 63570
#zeta=2.5; RextSMP = RextArray[2]; meshfile = meshfileArray[2]; electricField=120.e3; flagUseEMStress = True; flagEMFieldDependentShearModulus = True; applyPressureBCForRadialStretch = True; applyPressureBCWithAxialStretch = False; lambda_z = 1.0; flagCompressionTest = False; Pmax = 60961
#zeta=2.5; RextSMP = RextArray[2]; meshfile = meshfileArray[2]; electricField=135.e3; flagUseEMStress = True; flagEMFieldDependentShearModulus = True; applyPressureBCForRadialStretch = True; applyPressureBCWithAxialStretch = False; lambda_z = 1.0; flagCompressionTest = False; Pmax = 58351
#zeta=2.5; RextSMP = RextArray[2]; meshfile = meshfileArray[2]; electricField=150.e3; flagUseEMStress = True; flagEMFieldDependentShearModulus = True; applyPressureBCForRadialStretch = True; applyPressureBCWithAxialStretch = False; lambda_z = 1.0; flagCompressionTest = False; Pmax = 54697

# plot 4b lambda_z = 1 purely radial compression lambda_i = 0.5:
#zeta=2.5; RextSMP = RextArray[2]; meshfile = meshfileArray[2]; electricField=15.e3; flagUseEMStress = True; flagEMFieldDependentShearModulus = True; applyPressureBCForRadialStretch = True; applyPressureBCWithAxialStretch = False; lambda_z = 1.0; flagCompressionTest = True; Pmax = -205741
#zeta=2.5; RextSMP = RextArray[2]; meshfile = meshfileArray[2]; electricField=30.e3; flagUseEMStress = True; flagEMFieldDependentShearModulus = True; applyPressureBCForRadialStretch = True; applyPressureBCWithAxialStretch = False; lambda_z = 1.0; flagCompressionTest = True; Pmax = -204175
#zeta=2.5; RextSMP = RextArray[2]; meshfile = meshfileArray[2]; electricField=45.e3; flagUseEMStress = True; flagEMFieldDependentShearModulus = True; applyPressureBCForRadialStretch = True; applyPressureBCWithAxialStretch = False; lambda_z = 1.0; flagCompressionTest = True; Pmax = -202088
#zeta=2.5; RextSMP = RextArray[2]; meshfile = meshfileArray[2]; electricField=60.e3; flagUseEMStress = True; flagEMFieldDependentShearModulus = True; applyPressureBCForRadialStretch = True; applyPressureBCWithAxialStretch = False; lambda_z = 1.0; flagCompressionTest = True; Pmax = -198956
#zeta=2.5; RextSMP = RextArray[2]; meshfile = meshfileArray[2]; electricField=75.e3; flagUseEMStress = True; flagEMFieldDependentShearModulus = True; applyPressureBCForRadialStretch = True; applyPressureBCWithAxialStretch = False; lambda_z = 1.0; flagCompressionTest = True; Pmax = -194259
#zeta=2.5; RextSMP = RextArray[2]; meshfile = meshfileArray[2]; electricField=90.e3; flagUseEMStress = True; flagEMFieldDependentShearModulus = True; applyPressureBCForRadialStretch = True; applyPressureBCWithAxialStretch = False; lambda_z = 1.0; flagCompressionTest = True; Pmax = -189040
#zeta=2.5; RextSMP = RextArray[2]; meshfile = meshfileArray[2]; electricField=105.e3; flagUseEMStress = True; flagEMFieldDependentShearModulus = True; applyPressureBCForRadialStretch = True; applyPressureBCWithAxialStretch = False; lambda_z = 1.0; flagCompressionTest = True; Pmax = -183300
#zeta=2.5; RextSMP = RextArray[2]; meshfile = meshfileArray[2]; electricField=120.e3; flagUseEMStress = True; flagEMFieldDependentShearModulus = True; applyPressureBCForRadialStretch = True; applyPressureBCWithAxialStretch = False; lambda_z = 1.0; flagCompressionTest = True; Pmax = -176514
#zeta=2.5; RextSMP = RextArray[2]; meshfile = meshfileArray[2]; electricField=135.e3; flagUseEMStress = True; flagEMFieldDependentShearModulus = True; applyPressureBCForRadialStretch = True; applyPressureBCWithAxialStretch = False; lambda_z = 1.0; flagCompressionTest = True; Pmax = -168685
#zeta=2.5; RextSMP = RextArray[2]; meshfile = meshfileArray[2]; electricField=150.e3; flagUseEMStress = True; flagEMFieldDependentShearModulus = True; applyPressureBCForRadialStretch = True; applyPressureBCWithAxialStretch = False; lambda_z = 1.0; flagCompressionTest = True; Pmax = -159812

# to apply appropriate thermal BC
# temperature prescribed on outer cylindrical surface of smp
# with tinitial = 293. K applied on inner cylindrical surface of smp
# depending on tApplied, either a temperature gradient is prescribed or none 
tApplied = tAppliedArray[1] # K

# to generate axial electric field in SMP
# if nonzero voltage difference applied, there is electric and magnetic fields in the problem
# electricField = electricFieldArray[1] # V/m

# directory to copy results files after run
print('Creating folder to copy results files.')

if(meshfile=='newDorfmannTestGeom_1.msh'):
  zeta_ = 15 # 1.5 acutally but cannot have dot in folder name
elif(meshfile=='newDorfmannTestGeom_2.msh'):
  zeta_ = 20 # 2.0
elif(meshfile=='newDorfmannTestGeom_3.msh'):
  zeta_ = 25 # 2.5
else:
  print('Warning! Unknown mesh file. Using zeta_ = 0 to generate results folder.')
  zeta_ = 0 #
  
if(applyPressureBCForRadialStretch):
  tempName = 'pureRadialStretch'
elif(applyPressureBCWithAxialStretch):
  tempName = 'combinedRadialAxialStretch'
else:
  print('Warning! Unknown mechanical test scenario.')
  tempName = 'unknownTest'
    
# adapt folder name for compression test
if(flagCompressionTest):
  tempName += 'Compression'
  
# adapt folder name for combined axial + radial stretch test
if(applyPressureBCWithAxialStretch):
  tempName += '_lambda_z_'
  if(lambda_z==0.5):
    tempName += '50' # 0.5 acutally but cannot have dot in folder name
  elif(lambda_z==0.75):
    tempName += '75'
  elif(lambda_z==1.0):
    tempName += '100'
  elif(lambda_z==1.25):
    tempName += '125'
  elif(lambda_z==1.5):
    tempName += '150'
  elif(lambda_z==1.75):
    tempName += '175'
  elif(lambda_z==2.0):
    tempName += '200'
  else: # lambda_z==1.0
    tempName += '1'
  
resultsDir = tempName+'_zeta_'+str(int(zeta_))+'_E_'+str(int(electricField))+'_temp_'+str(int(tApplied))
os.system('mkdir '+resultsDir)

print('Results folder '+resultsDir+' created.')

# use of pickle for inputs to test
with open('inputdatafile.txt','wb') as data:
  pickle.dump(ftime, data)
  pickle.dump(nuSMP, data)
  pickle.dump(nuVac, data)
  pickle.dump(RintSMP, data)
  pickle.dump(RextSMP, data)
  pickle.dump(meshfile, data)
  pickle.dump(flagUseEMStress, data)
  pickle.dump(flagEMFieldDependentShearModulus, data)
  pickle.dump(thermalSource, data)
  pickle.dump(applyPressureBCForRadialStretch, data)
  pickle.dump(applyPressureBCWithAxialStretch, data)
  pickle.dump(Pmax, data)
  pickle.dump(tApplied, data)
  pickle.dump(electricField, data)
  pickle.dump(lambda_z, data)
  pickle.dump(H_SMP, data)
  pickle.dump(flagCompressionTest, data)
    
if sys.version_info[0] < 3:
  os.system('python Steinmann_ETM_Benchmarks.py')
else:
  os.system('python3 Steinmann_ETM_Benchmarks.py')
  
print('Copying results files to folder.')
# copy results files to directory
os.system('cp *csv disp_step* stress_step* inputdatafile.txt '+resultsDir+'/')
print('Done copying results files to folder.')
