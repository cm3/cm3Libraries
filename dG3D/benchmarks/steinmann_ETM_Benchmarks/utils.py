import os.path

import numpy as npy
import pickle
import math

# Function to read a csv (whose name is filename) with data written in the first two columns.
# The output is a tuple with both columns in two list.
# The delimiter ( ; by default) can be specified.
def read_2ColumnCsv(filename, delimiter=';'):
    # Reading csv ...
    with open(filename, 'r') as file_object:
        data = file_object.read()

    # Getting all lines
    lines = data.split('\n')

    # Putting the data in the corresponding list
    x = []
    y = []
    for line in lines:
        tmp = line.split(delimiter)
        if len(tmp) >= 2:
            x.append(float(tmp[0]))
            y.append(float(tmp[1]))

    # Return both lists
    return npy.array(x), npy.array(y)

def closest_value(input_list, input_value):

    difference = lambda input_list : abs(input_list - float(input_value))

    min_value = min(input_list, key=difference)
    ind = input_list.index(min_value)

    return ind, min_value

def second_smallest(numbers):
    m1 = m2 = float('inf')
    for x in numbers:
        if x <= float(m1):
            m1, m2 = float(x), float(m1)
        elif x < m2:
            m2 = float(x)
    return m2

def second_closest_value(input_list, input_value):

    difference = [abs(input_list[_] - float(input_value)) for _ in range(len(input_list))]

    min_value = second_smallest(difference)
    ind = difference.index(min_value)

    return ind, input_list[ind]


def reduceList1ToShorterSize(list1_X, list1_Y, list2_X, list2_Y):
    perm=False
    t1=[]
    d1=[]
    t2=[]
    d2=[]
    if len(list1_X)<len(list2_X):
        print("permute lists")
        list1_X_tmp=list2_X
        list2_X=list1_X
        list1_X=list1_X_tmp
        list1_Y_tmp=list2_Y
        list2_Y=list1_Y
        list1_Y=list1_Y_tmp
        perm=True
    for ind2 in range(len(list2_X)):
        x2=list2_X[ind2]
        y2=list2_Y[ind2]
        ind1, x1 = closest_value(list1_X, x2)
        ind1bis, x1bis = second_closest_value(list1_X, x2)
        #print(x2,x1,x1bis)
        y1=(x2-x1)/(x1bis-x1)*list1_Y[ind1]+(x1bis-x2)/(x1bis-x1)*list1_Y[ind1bis]
        t1.append(x2)
        d1.append(y1)
        t2.append(x2)
        d2.append(y2)
    if perm:
        return t2, d2, t1, d1
    return t1, d1, t2, d2

def GLToEngStrain(listStrain):
    eng=[]
    for ind in range(len(listStrain)):
        eng.append( (1.+2.*listStrain[ind])**0.5-1.)

    return eng


def TrueStressToEngStress(listEngStrain,listStress,poisson):
    eng=[]
    for ind in range(len(listStress)):
        eng.append( listStress[ind]*(1.-poisson*listEngStrain[ind])**2)

    return eng
