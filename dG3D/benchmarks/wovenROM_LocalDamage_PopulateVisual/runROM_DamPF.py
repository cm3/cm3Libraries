### benchmark for the validation of 
### 1.wovenROM with damage
### 2.populateMaxD function for VT material
### 3.visualizations of strain/stress/damage of ROM pseudo-grains

#coding-Utf-8-*-
from gmshpy import *
from dG3Dpy import*
#from dG3DpyDebug import*
from Prop_Reduce_Woven import *


def TensileWoven(Geo, BC, ROMMod, order, pert, microNum):
    # geometry -------------------------------------------------------------------------------------------------------------
    dimDomain = 3  # dimension of domain
    if Geo == "1":
        geofile = 'WLogC1_ROM.geo'
        meshfile = "WLogC1_ROM.msh"
    elif Geo == "2":
        geofile = 'WLogC2_ROM.geo'
        meshfile = "WLogC2_ROM.msh"
    
    # key material properties --------------------------------------------------------------------------------------------------
    propROM = "propROM.i01"
    nbNonLocalVar = 0
    nstep = 5000  # number of step (used only if soltype=1) # 1000
    DmaxMtx = 0.975  # 0.95 # 0.975 # 0.99
    DmaxInc = 0.975  # 0.95 # 0.975 # 0.99
    if ROMMod == "VM":
        PFALStep = 1e-3  # path following arc length
        PFALStepMaxBd = 1e-3  # path following arc length max. bound
        nstepArch = 100
    elif ROMMod == "VLM":
        PFALStep = 1e-2  # path following arc length
        PFALStepMaxBd = 1e-2  # path following arc length max. bound
        nstepArch = 20
    nstepArchForce = 1

    
    # unique physical numbers ----------------------------------------------------------------------------------------------
    # boundary condition surface
    numPhyList_BCFace = [100, 101, 110, 111, 120, 121]
    # elasto-plastic field with non-local damage of unit cell with ROMs
    numDomainROM = 51
    # unique law tags ------------------------------------------------------------------------------------------------------
    # elasto-plastic law with non-local damage of unit cell with ROMs
    tagLawROM = 1
    # unique field tags ----------------------------------------------------------------------------------------------------
    # elasto-plastic field with non-local damage of unit cell with ROMs
    tagDomain = 10
    # unique solver tag ----------------------------------------------------------------------------------------------------
    tagSolver = 1000
    
    # parameters for material law ------------------------------------------------------------------------------------------
    rhoROM = 1000.  # Bulk mass for the slave (density in kg/m3)
    if order == 1:
        useBarF = True  # useBarF to avoid locking when order=1
    elif order == 2:
        useBarF = False
    if pert:    
        pertFactor = 1e-8  # law with tangent by perturbation
    # parameters for domains and interdomains ------------------------------------------------------------------------------
    space1 = 0  # function space (Lagrange=0)
    fulldg = False  # False = CG, True = DG inside a domain
    if order == 1:
        beta1 = 100.  # stability/penalty parameter for displacement when DG
    elif order == 2:
        beta1 = 30.
    # parameters for solver ------------------------------------------------------------------------------------------------
    sol = 2  # Gmm=0 (default) Taucs=1 PETsc=2
    soltype = 1  # StaticLinear=0 (default); StaticNonLinear=1; Explicit=2; Multi=3; Implicit=4; Eigen=5
    ftime = 1.  # final time (used only if soltype=1)
    tol = 1.e-5  # 1.e-6  # relative tolerance for NR scheme (used only if soltype=1)
    tolAbs = 1.e-9  # 1.e-12  # absolu tolerance for NR scheme (used only if soltype=1)
    # system & control type
    system = 1  # Displacement elimination =0 Multiplier elimination = 1 Displacement+ multiplier = 2
    control = 0  # load control = 0 arc length control euler = 1
    # path following option
    pathFollowing = True  # True
    methodPF = 2
    # manage time steps & archivage steps
    maxIter = 100
    stepIncNb = 3
    stepRedFac = 2
    maxRedNb = 30
    # micro BC parameters    
    methodBC = 5  # PeriodicMesh=0, LangrangeInterpolation=1, CubicSplineInterpolation=2, FELinear=3, FEQuad=4, PROJECT=5, FE_HIGH_ORDER=6 (for constructing linear constraint)
    degreeBC = 3  # Order used for polynomial interpolation
    addvertex = 1  # Polynomial interpolation by mesh vertex = 0, Polynomial interpolation by virtual vertex    


    # creation of law
    lawROM = NonLocalDamageDG3DMaterialLaw(tagLawROM, rhoROM, propROM)
    lawROM.setUseBarF(useBarF)
    lawROM.setInitialDmax_Inc(DmaxInc)  # fibres in MT for VT or LAM_2PLY
    lawROM.setInitialDmax_Mtx(DmaxMtx)  # matrix in MT or EP for VT or LAM_2PLY
    if pert:
        pertLawROM = dG3DMaterialLawWithTangentByPerturbation(lawROM, pertFactor)
        
    # creation of field
    domainROM = dG3DDomain(tagDomain, numDomainROM, space1, tagLawROM, fulldg, dimDomain, nbNonLocalVar)
    domainROM.stabilityParameters(beta1)

    # creation of solver
    mysolver = nonLinearMechSolver(tagSolver)
    mysolver.createModel(geofile, meshfile, dimDomain, order)
    #mysolver.loadModel(meshfile)
    
    mysolver.Solver(sol)
    mysolver.Scheme(soltype)
    mysolver.snlData(nstep, ftime, tol, tolAbs)
    mysolver.snlManageTimeStep(maxIter, stepIncNb, stepRedFac, maxRedNb)
    mysolver.setSystemType(system)
    mysolver.setControlType(control)
    
    if pert:
        mysolver.addMaterialLaw(pertLawROM)
    elif not pert:
        mysolver.addMaterialLaw(lawROM)
    mysolver.addDomain(domainROM)
    
    # BC panel
    mysolver.stiffnessModification(bool(1))
    mysolver.iterativeProcedure(bool(1))
    mysolver.setMessageView(bool(1))
    microBC = nonLinearPeriodicBC(tagSolver, dimDomain)
    microBC.setOrder(order)
    microBC.setBCPhysical(numPhyList_BCFace[4], numPhyList_BCFace[0], numPhyList_BCFace[2], numPhyList_BCFace[5], 
                          numPhyList_BCFace[1], numPhyList_BCFace[3])
    microBC.setPeriodicBCOptions(methodBC, degreeBC, bool(addvertex))
    mysolver.addMicroBC(microBC)
    mysolver.activateTest(True)
    mysolver.setMicroProblemIndentification(0, microNum)
    # stress averaging flag and averaging method 0- VOLUME, 1- SURFACE
    mysolver.stressAveragingFlag(bool(1))  # set stress averaging ON- 0 , OFF-1
    mysolver.setStressAveragingMethod(0)  # 0 -volume 1- surface
    
    # loading BC
    mysolver.pathFollowing(pathFollowing, methodPF)
    if methodPF == 0:
        mysolver.setPathFollowingIncrementAdaptation(True, 4, 0.3)
        mysolver.setPathFollowingControlType(0)
        mysolver.setPathFollowingCorrectionMethod(0)
        mysolver.setPathFollowingArcLengthStep(1e-1)  # 1e-2
        mysolver.setBoundsOfPathFollowingArcLengthSteps(0, 0.1)  # 0, 2.5
    elif methodPF == 1:
        mysolver.setPathFollowingIncrementAdaptationLocal(False, 3, 6)  # 5, 7
        mysolver.setPathFollowingLocalSteps(2.5e-2, 1.e-2)  # 1e-2, 1e-8 # 5e-5, 1e-10
        mysolver.setPathFollowingLocalIncrementType(1)
        mysolver.setPathFollowingSwitchCriterion(0.3)  # 2e-3
        mysolver.setBoundsOfPathFollowingLocalSteps(1e-13, 1.e-2)  # 1e-13, 4.
    elif methodPF == 2:
        mysolver.setPathFollowingIncrementAdaptation(True, 4, 0.3)
        mysolver.setPathFollowingControlType(0)
        mysolver.setPathFollowingCorrectionMethod(0)
        mysolver.setPathFollowingArcLengthStep(PFALStep)  # 1e-3
        mysolver.setBoundsOfPathFollowingArcLengthSteps(0, PFALStepMaxBd)  # 0, 0.01 # 0, 2.5
    # mysolver.sameDisplacementBC("Face", 101, 2, 0)
    mysolver.forceBC("Node", 2, 0, 1.e3)
    #mysolver.sameDisplacementBCBetweenTwoGroups("Face", 120, 121, 3)
    #mysolver.sameDisplacementBCBetweenTwoGroups("Face", 100, 101, 3)
    #mysolver.sameDisplacementBCBetweenTwoGroups("Face", 110, 111, 3)
    #mysolver.sameDisplacementBCBetweenTwoGroups("Face", 120, 121, 4)
    #mysolver.sameDisplacementBCBetweenTwoGroups("Face", 100, 101, 4)
    #mysolver.sameDisplacementBCBetweenTwoGroups("Face", 110, 111, 4)
    if BC == "MBC":
        mysolver.displacementBC("Face", 120, 2, 0.)
    # block rigid mode
    mysolver.displacementBC("Node", 1, 0, 0.)
    mysolver.displacementBC("Node", 1, 1, 0.)
    mysolver.displacementBC("Node", 1, 2, 0.)
    mysolver.displacementBC("Node", 4, 0, 0.)
    mysolver.displacementBC("Node", 4, 2, 0.)
    mysolver.displacementBC("Node", 2, 2, 0.)    
    
    stopCri = EndSchemeMonitoringWithZeroForceOnPhysical(0.2, "Face", 101, 0)
    mysolver.endSchemeMonitoring(stopCri)

    # archiving
    mysolver.stepBetweenArchiving(nstepArch)
    
    mysolver.internalPointBuildView("svm",IPField.SVM, 1, 1)
    mysolver.internalPointBuildView("sig_xx",IPField.SIG_XX, 1, 1)
    mysolver.internalPointBuildView("sig_yy",IPField.SIG_YY, 1, 1)
    mysolver.internalPointBuildView("sig_zz",IPField.SIG_ZZ, 1, 1)
    mysolver.internalPointBuildView("sig_xy",IPField.SIG_XY, 1, 1)
    mysolver.internalPointBuildView("sig_yz",IPField.SIG_YZ, 1, 1)
    mysolver.internalPointBuildView("sig_xz",IPField.SIG_XZ, 1, 1)
    #mysolver.internalPointBuildView("damage",IPField.DAMAGE, 1, 1)
    mysolver.internalPointBuildView("epl",IPField.PLASTICSTRAIN, 1, 1)
    #mysolver.internalPointBuildView("FiberDamage",IPField.INC_DAMAGE, 1, 1)
    
    #mysolver.archivingIPOnPhysicalGroup("Volume", numDomainROM, IPField.DAMAGE, IPField.MAX_VALUE, nstepArchForce)
    #mysolver.archivingIPOnPhysicalGroup("Volume", numDomainROM, IPField.INC_DAMAGE, IPField.MAX_VALUE, nstepArchForce)
    
    mysolver.archivingForceOnPhysicalGroup("Face", 100, 0, nstepArchForce)
    mysolver.archivingForceOnPhysicalGroup("Face", 101, 0, nstepArchForce)
    mysolver.archivingNodeDisplacement(2, 0, nstepArchForce)
    
    mysolver.archivingAverageValue(IPField.P_XX)
    mysolver.archivingAverageValue(IPField.P_YY)
    mysolver.archivingAverageValue(IPField.P_ZZ)
    mysolver.archivingAverageValue(IPField.P_XY)
    mysolver.archivingAverageValue(IPField.P_XZ)
    mysolver.archivingAverageValue(IPField.P_YZ)
    #mysolver.archivingAverageValue(IPField.SIG_XX)
    #mysolver.archivingAverageValue(IPField.SIG_YY)
    #mysolver.archivingAverageValue(IPField.SIG_ZZ)
    #mysolver.archivingAverageValue(IPField.SIG_XY)
    #mysolver.archivingAverageValue(IPField.SIG_XZ)
    #mysolver.archivingAverageValue(IPField.SIG_YZ)
    mysolver.archivingAverageValue(IPField.STRAIN_XX)
    mysolver.archivingAverageValue(IPField.STRAIN_YY)
    mysolver.archivingAverageValue(IPField.STRAIN_ZZ)
    mysolver.archivingAverageValue(IPField.STRAIN_XY)
    mysolver.archivingAverageValue(IPField.STRAIN_XZ)
    mysolver.archivingAverageValue(IPField.STRAIN_YZ)
    #mysolver.archivingAverageValue(IPField.DAMAGE)
    
    
    
    # for each VT phase
    # strain
    if ROMMod == "VM":
    	mysolver.archivingAverageValue(IPField.STRAIN_XX_VTM0)
    	mysolver.archivingAverageValue(IPField.STRAIN_XX_VTMT1)
    	mysolver.archivingAverageValue(IPField.STRAIN_XX_VTMT2)
    	mysolver.archivingAverageValue(IPField.STRAIN_XX_VTMT3)
    	mysolver.archivingAverageValue(IPField.STRAIN_XX_VTMT4)
    	mysolver.archivingAverageValue(IPField.STRAIN_XX_VTMT5)
    	mysolver.archivingAverageValue(IPField.STRAIN_XX_VTMT6)
    elif ROMMod == "VLM":
    	mysolver.archivingAverageValue(IPField.STRAIN_XX_VTLAM1)
    	mysolver.archivingAverageValue(IPField.STRAIN_XX_VTLAM2)
    	mysolver.archivingAverageValue(IPField.STRAIN_XX_VTLAM3)
    	mysolver.archivingAverageValue(IPField.STRAIN_XX_VTLAM4)
    	mysolver.archivingAverageValue(IPField.STRAIN_XX_VTLAM5)
    	mysolver.archivingAverageValue(IPField.STRAIN_XX_VTLAM6)
    	mysolver.archivingAverageValue(IPField.STRAIN_XX_VTLAM7)
    	mysolver.archivingAverageValue(IPField.STRAIN_XX_VTLAM8)
    	mysolver.archivingAverageValue(IPField.STRAIN_XX_VTLAM9)
    	mysolver.archivingAverageValue(IPField.STRAIN_XX_VTLAM10)
    	mysolver.archivingAverageValue(IPField.STRAIN_XX_VTLAM11)
    	mysolver.archivingAverageValue(IPField.STRAIN_XX_VTLAM12)
    # stress
    if ROMMod == "VM":
    	mysolver.archivingAverageValue(IPField.STRESS_XX_VTM0)
    	mysolver.archivingAverageValue(IPField.STRESS_XX_VTMT1)
    	mysolver.archivingAverageValue(IPField.STRESS_XX_VTMT2)
    	mysolver.archivingAverageValue(IPField.STRESS_XX_VTMT3)
    	mysolver.archivingAverageValue(IPField.STRESS_XX_VTMT4)
    	mysolver.archivingAverageValue(IPField.STRESS_XX_VTMT5)
    	mysolver.archivingAverageValue(IPField.STRESS_XX_VTMT6)
    elif ROMMod == "VLM":
    	mysolver.archivingAverageValue(IPField.STRESS_XX_VTLAM1)
    	mysolver.archivingAverageValue(IPField.STRESS_XX_VTLAM2)
    	mysolver.archivingAverageValue(IPField.STRESS_XX_VTLAM3)
    	mysolver.archivingAverageValue(IPField.STRESS_XX_VTLAM4)
    	mysolver.archivingAverageValue(IPField.STRESS_XX_VTLAM5)
    	mysolver.archivingAverageValue(IPField.STRESS_XX_VTLAM6)
    	mysolver.archivingAverageValue(IPField.STRESS_XX_VTLAM7)
    	mysolver.archivingAverageValue(IPField.STRESS_XX_VTLAM8)
    	mysolver.archivingAverageValue(IPField.STRESS_XX_VTLAM9)
    	mysolver.archivingAverageValue(IPField.STRESS_XX_VTLAM10)
    	mysolver.archivingAverageValue(IPField.STRESS_XX_VTLAM11)
    	mysolver.archivingAverageValue(IPField.STRESS_XX_VTLAM12)
    # damage
    if ROMMod == "VM":
    	mysolver.archivingAverageValue(IPField.DAMAGE_VTM0)
    # for each laminate ply
    if ROMMod == "VLM":
    	# strain
    	mysolver.archivingAverageValue(IPField.STRAIN_XX_VTLAM1_M0)
    	mysolver.archivingAverageValue(IPField.STRAIN_XX_VTLAM1_MT)
    	mysolver.archivingAverageValue(IPField.STRAIN_XX_VTLAM2_M0)
    	mysolver.archivingAverageValue(IPField.STRAIN_XX_VTLAM2_MT)
    	mysolver.archivingAverageValue(IPField.STRAIN_XX_VTLAM3_M0)
    	mysolver.archivingAverageValue(IPField.STRAIN_XX_VTLAM3_MT)
    	mysolver.archivingAverageValue(IPField.STRAIN_XX_VTLAM4_M0)
    	mysolver.archivingAverageValue(IPField.STRAIN_XX_VTLAM4_MT)
    	mysolver.archivingAverageValue(IPField.STRAIN_XX_VTLAM5_M0)
    	mysolver.archivingAverageValue(IPField.STRAIN_XX_VTLAM5_MT)
    	mysolver.archivingAverageValue(IPField.STRAIN_XX_VTLAM6_M0)
    	mysolver.archivingAverageValue(IPField.STRAIN_XX_VTLAM6_MT)
    	mysolver.archivingAverageValue(IPField.STRAIN_XX_VTLAM7_M0)
    	mysolver.archivingAverageValue(IPField.STRAIN_XX_VTLAM7_MT)
    	mysolver.archivingAverageValue(IPField.STRAIN_XX_VTLAM8_M0)
    	mysolver.archivingAverageValue(IPField.STRAIN_XX_VTLAM8_MT)
    	mysolver.archivingAverageValue(IPField.STRAIN_XX_VTLAM9_M0)
    	mysolver.archivingAverageValue(IPField.STRAIN_XX_VTLAM9_MT)
    	mysolver.archivingAverageValue(IPField.STRAIN_XX_VTLAM10_M0)
    	mysolver.archivingAverageValue(IPField.STRAIN_XX_VTLAM10_MT)
    	mysolver.archivingAverageValue(IPField.STRAIN_XX_VTLAM11_M0)
    	mysolver.archivingAverageValue(IPField.STRAIN_XX_VTLAM11_MT)
    	mysolver.archivingAverageValue(IPField.STRAIN_XX_VTLAM12_M0)
    	mysolver.archivingAverageValue(IPField.STRAIN_XX_VTLAM12_MT)
    	# stress
    	mysolver.archivingAverageValue(IPField.STRESS_XX_VTLAM1_M0)
    	mysolver.archivingAverageValue(IPField.STRESS_XX_VTLAM1_MT)
    	mysolver.archivingAverageValue(IPField.STRESS_XX_VTLAM2_M0)
    	mysolver.archivingAverageValue(IPField.STRESS_XX_VTLAM2_MT)
    	mysolver.archivingAverageValue(IPField.STRESS_XX_VTLAM3_M0)
    	mysolver.archivingAverageValue(IPField.STRESS_XX_VTLAM3_MT)
    	mysolver.archivingAverageValue(IPField.STRESS_XX_VTLAM4_M0)
    	mysolver.archivingAverageValue(IPField.STRESS_XX_VTLAM4_MT)
    	mysolver.archivingAverageValue(IPField.STRESS_XX_VTLAM5_M0)
    	mysolver.archivingAverageValue(IPField.STRESS_XX_VTLAM5_MT)
    	mysolver.archivingAverageValue(IPField.STRESS_XX_VTLAM6_M0)
    	mysolver.archivingAverageValue(IPField.STRESS_XX_VTLAM6_MT)
    	mysolver.archivingAverageValue(IPField.STRESS_XX_VTLAM7_M0)
    	mysolver.archivingAverageValue(IPField.STRESS_XX_VTLAM7_MT)
    	mysolver.archivingAverageValue(IPField.STRESS_XX_VTLAM8_M0)
    	mysolver.archivingAverageValue(IPField.STRESS_XX_VTLAM8_MT)
    	mysolver.archivingAverageValue(IPField.STRESS_XX_VTLAM9_M0)
    	mysolver.archivingAverageValue(IPField.STRESS_XX_VTLAM9_MT)
    	mysolver.archivingAverageValue(IPField.STRESS_XX_VTLAM10_M0)
    	mysolver.archivingAverageValue(IPField.STRESS_XX_VTLAM10_MT)
    	mysolver.archivingAverageValue(IPField.STRESS_XX_VTLAM11_M0)
    	mysolver.archivingAverageValue(IPField.STRESS_XX_VTLAM11_MT)
    	mysolver.archivingAverageValue(IPField.STRESS_XX_VTLAM12_M0)
    	mysolver.archivingAverageValue(IPField.STRESS_XX_VTLAM12_MT)
    	# damage
    	mysolver.archivingAverageValue(IPField.DAMAGE_VTLAM1_M0)
    	mysolver.archivingAverageValue(IPField.DAMAGE_VTLAM2_M0)
    	mysolver.archivingAverageValue(IPField.DAMAGE_VTLAM3_M0)
    	mysolver.archivingAverageValue(IPField.DAMAGE_VTLAM4_M0)
    	mysolver.archivingAverageValue(IPField.DAMAGE_VTLAM5_M0)
    	mysolver.archivingAverageValue(IPField.DAMAGE_VTLAM6_M0)
    	mysolver.archivingAverageValue(IPField.DAMAGE_VTLAM7_M0)
    	mysolver.archivingAverageValue(IPField.DAMAGE_VTLAM8_M0)
    	mysolver.archivingAverageValue(IPField.DAMAGE_VTLAM9_M0)
    	mysolver.archivingAverageValue(IPField.DAMAGE_VTLAM10_M0)
    	mysolver.archivingAverageValue(IPField.DAMAGE_VTLAM11_M0)
    	mysolver.archivingAverageValue(IPField.DAMAGE_VTLAM12_M0)
    # for each material phase
    # strain
    if ROMMod == "VM":
    	mysolver.archivingAverageValue(IPField.STRAIN_XX_VTMT1_MTX)
    	mysolver.archivingAverageValue(IPField.STRAIN_XX_VTMT1_INC)
    	mysolver.archivingAverageValue(IPField.STRAIN_XX_VTMT2_MTX)
    	mysolver.archivingAverageValue(IPField.STRAIN_XX_VTMT2_INC)
    	mysolver.archivingAverageValue(IPField.STRAIN_XX_VTMT3_MTX)
    	mysolver.archivingAverageValue(IPField.STRAIN_XX_VTMT3_INC)
    	mysolver.archivingAverageValue(IPField.STRAIN_XX_VTMT4_MTX)
    	mysolver.archivingAverageValue(IPField.STRAIN_XX_VTMT4_INC)
    	mysolver.archivingAverageValue(IPField.STRAIN_XX_VTMT5_MTX)
    	mysolver.archivingAverageValue(IPField.STRAIN_XX_VTMT5_INC)
    	mysolver.archivingAverageValue(IPField.STRAIN_XX_VTMT6_MTX)
    	mysolver.archivingAverageValue(IPField.STRAIN_XX_VTMT6_INC)
    elif ROMMod == "VLM":
    	mysolver.archivingAverageValue(IPField.STRAIN_XX_VTLAM1_MT_MTX)
    	mysolver.archivingAverageValue(IPField.STRAIN_XX_VTLAM1_MT_INC)
    	mysolver.archivingAverageValue(IPField.STRAIN_XX_VTLAM2_MT_MTX)
    	mysolver.archivingAverageValue(IPField.STRAIN_XX_VTLAM2_MT_INC)
    	mysolver.archivingAverageValue(IPField.STRAIN_XX_VTLAM3_MT_MTX)
    	mysolver.archivingAverageValue(IPField.STRAIN_XX_VTLAM3_MT_INC)
    	mysolver.archivingAverageValue(IPField.STRAIN_XX_VTLAM4_MT_MTX)
    	mysolver.archivingAverageValue(IPField.STRAIN_XX_VTLAM4_MT_INC)
    	mysolver.archivingAverageValue(IPField.STRAIN_XX_VTLAM5_MT_MTX)
    	mysolver.archivingAverageValue(IPField.STRAIN_XX_VTLAM5_MT_INC)
    	mysolver.archivingAverageValue(IPField.STRAIN_XX_VTLAM6_MT_MTX)
    	mysolver.archivingAverageValue(IPField.STRAIN_XX_VTLAM6_MT_INC)
    	mysolver.archivingAverageValue(IPField.STRAIN_XX_VTLAM7_MT_MTX)
    	mysolver.archivingAverageValue(IPField.STRAIN_XX_VTLAM7_MT_INC)
    	mysolver.archivingAverageValue(IPField.STRAIN_XX_VTLAM8_MT_MTX)
    	mysolver.archivingAverageValue(IPField.STRAIN_XX_VTLAM8_MT_INC)
    	mysolver.archivingAverageValue(IPField.STRAIN_XX_VTLAM9_MT_MTX)
    	mysolver.archivingAverageValue(IPField.STRAIN_XX_VTLAM9_MT_INC)
    	mysolver.archivingAverageValue(IPField.STRAIN_XX_VTLAM10_MT_MTX)
    	mysolver.archivingAverageValue(IPField.STRAIN_XX_VTLAM10_MT_INC)
    	mysolver.archivingAverageValue(IPField.STRAIN_XX_VTLAM11_MT_MTX)
    	mysolver.archivingAverageValue(IPField.STRAIN_XX_VTLAM11_MT_INC)
    	mysolver.archivingAverageValue(IPField.STRAIN_XX_VTLAM12_MT_MTX)
    	mysolver.archivingAverageValue(IPField.STRAIN_XX_VTLAM12_MT_INC)
    # stress
    if ROMMod == "VM":
    	mysolver.archivingAverageValue(IPField.STRESS_XX_VTMT1_MTX)
    	mysolver.archivingAverageValue(IPField.STRESS_XX_VTMT1_INC)
    	mysolver.archivingAverageValue(IPField.STRESS_XX_VTMT2_MTX)
    	mysolver.archivingAverageValue(IPField.STRESS_XX_VTMT2_INC)
    	mysolver.archivingAverageValue(IPField.STRESS_XX_VTMT3_MTX)
    	mysolver.archivingAverageValue(IPField.STRESS_XX_VTMT3_INC)
    	mysolver.archivingAverageValue(IPField.STRESS_XX_VTMT4_MTX)
    	mysolver.archivingAverageValue(IPField.STRESS_XX_VTMT4_INC)
    	mysolver.archivingAverageValue(IPField.STRESS_XX_VTMT5_MTX)
    	mysolver.archivingAverageValue(IPField.STRESS_XX_VTMT5_INC)
    	mysolver.archivingAverageValue(IPField.STRESS_XX_VTMT6_MTX)
    	mysolver.archivingAverageValue(IPField.STRESS_XX_VTMT6_INC)
    elif ROMMod == "VLM":
    	mysolver.archivingAverageValue(IPField.STRESS_XX_VTLAM1_MT_MTX)
    	mysolver.archivingAverageValue(IPField.STRESS_XX_VTLAM1_MT_INC)
    	mysolver.archivingAverageValue(IPField.STRESS_XX_VTLAM2_MT_MTX)
    	mysolver.archivingAverageValue(IPField.STRESS_XX_VTLAM2_MT_INC)
    	mysolver.archivingAverageValue(IPField.STRESS_XX_VTLAM3_MT_MTX)
    	mysolver.archivingAverageValue(IPField.STRESS_XX_VTLAM3_MT_INC)
    	mysolver.archivingAverageValue(IPField.STRESS_XX_VTLAM4_MT_MTX)
    	mysolver.archivingAverageValue(IPField.STRESS_XX_VTLAM4_MT_INC)
    	mysolver.archivingAverageValue(IPField.STRESS_XX_VTLAM5_MT_MTX)
    	mysolver.archivingAverageValue(IPField.STRESS_XX_VTLAM5_MT_INC)
    	mysolver.archivingAverageValue(IPField.STRESS_XX_VTLAM6_MT_MTX)
    	mysolver.archivingAverageValue(IPField.STRESS_XX_VTLAM6_MT_INC)
    	mysolver.archivingAverageValue(IPField.STRESS_XX_VTLAM7_MT_MTX)
    	mysolver.archivingAverageValue(IPField.STRESS_XX_VTLAM7_MT_INC)
    	mysolver.archivingAverageValue(IPField.STRESS_XX_VTLAM8_MT_MTX)
    	mysolver.archivingAverageValue(IPField.STRESS_XX_VTLAM8_MT_INC)
    	mysolver.archivingAverageValue(IPField.STRESS_XX_VTLAM9_MT_MTX)
    	mysolver.archivingAverageValue(IPField.STRESS_XX_VTLAM9_MT_INC)
    	mysolver.archivingAverageValue(IPField.STRESS_XX_VTLAM10_MT_MTX)
    	mysolver.archivingAverageValue(IPField.STRESS_XX_VTLAM10_MT_INC)
    	mysolver.archivingAverageValue(IPField.STRESS_XX_VTLAM11_MT_MTX)
    	mysolver.archivingAverageValue(IPField.STRESS_XX_VTLAM11_MT_INC)
    	mysolver.archivingAverageValue(IPField.STRESS_XX_VTLAM12_MT_MTX)
    	mysolver.archivingAverageValue(IPField.STRESS_XX_VTLAM12_MT_INC)
    # damage
    if ROMMod == "VM":
    	mysolver.archivingAverageValue(IPField.DAMAGE_VTMT1_MTX)
    	mysolver.archivingAverageValue(IPField.DAMAGE_VTMT1_INC)
    	mysolver.archivingAverageValue(IPField.DAMAGE_VTMT2_MTX)
    	mysolver.archivingAverageValue(IPField.DAMAGE_VTMT2_INC)
    	mysolver.archivingAverageValue(IPField.DAMAGE_VTMT3_MTX)
    	mysolver.archivingAverageValue(IPField.DAMAGE_VTMT3_INC)
    	mysolver.archivingAverageValue(IPField.DAMAGE_VTMT4_MTX)
    	mysolver.archivingAverageValue(IPField.DAMAGE_VTMT4_INC)
    	mysolver.archivingAverageValue(IPField.DAMAGE_VTMT5_MTX)
    	mysolver.archivingAverageValue(IPField.DAMAGE_VTMT5_INC)
    	mysolver.archivingAverageValue(IPField.DAMAGE_VTMT6_MTX)
    	mysolver.archivingAverageValue(IPField.DAMAGE_VTMT6_INC)
    elif ROMMod == "VLM":
    	mysolver.archivingAverageValue(IPField.DAMAGE_VTLAM1_MT_MTX)
    	mysolver.archivingAverageValue(IPField.DAMAGE_VTLAM1_MT_INC)
    	mysolver.archivingAverageValue(IPField.DAMAGE_VTLAM2_MT_MTX)
    	mysolver.archivingAverageValue(IPField.DAMAGE_VTLAM2_MT_INC)
    	mysolver.archivingAverageValue(IPField.DAMAGE_VTLAM3_MT_MTX)
    	mysolver.archivingAverageValue(IPField.DAMAGE_VTLAM3_MT_INC)
    	mysolver.archivingAverageValue(IPField.DAMAGE_VTLAM4_MT_MTX)
    	mysolver.archivingAverageValue(IPField.DAMAGE_VTLAM4_MT_INC)
    	mysolver.archivingAverageValue(IPField.DAMAGE_VTLAM5_MT_MTX)
    	mysolver.archivingAverageValue(IPField.DAMAGE_VTLAM5_MT_INC)
    	mysolver.archivingAverageValue(IPField.DAMAGE_VTLAM6_MT_MTX)
    	mysolver.archivingAverageValue(IPField.DAMAGE_VTLAM6_MT_INC)
    	mysolver.archivingAverageValue(IPField.DAMAGE_VTLAM7_MT_MTX)
    	mysolver.archivingAverageValue(IPField.DAMAGE_VTLAM7_MT_INC)
    	mysolver.archivingAverageValue(IPField.DAMAGE_VTLAM8_MT_MTX)
    	mysolver.archivingAverageValue(IPField.DAMAGE_VTLAM8_MT_INC)
    	mysolver.archivingAverageValue(IPField.DAMAGE_VTLAM9_MT_MTX)
    	mysolver.archivingAverageValue(IPField.DAMAGE_VTLAM9_MT_INC)
    	mysolver.archivingAverageValue(IPField.DAMAGE_VTLAM10_MT_MTX)
    	mysolver.archivingAverageValue(IPField.DAMAGE_VTLAM10_MT_INC)
    	mysolver.archivingAverageValue(IPField.DAMAGE_VTLAM11_MT_MTX)
    	mysolver.archivingAverageValue(IPField.DAMAGE_VTLAM11_MT_INC)
    	mysolver.archivingAverageValue(IPField.DAMAGE_VTLAM12_MT_MTX)
    	mysolver.archivingAverageValue(IPField.DAMAGE_VTLAM12_MT_INC)
    


    mysolver.solve()

    '''check = TestCheck()
    try:
      import linecache
      linesxx = linecache.getline('Average_P_XX.csv',-1)
    except:
      print('Cannot read the results in the files')
      import os
      os._exit(1)
    else:
      if ROMMod == "VM":
        check.equal(1045.066772284084,float(linesxx.split(';')[1]))
      if ROMMod == "VLM":
        check.equal(1074.40647624053,float(linesxx.split(';')[1]))'''

    return


#######################################################################################################################
Geo = "2"  # "1" for cell 1 and "2" for cell 2
BC = "MBC"  #"MBC" for mixed and "PBC" periodic
ROMMod = "VLM"  #"VM" for VM ROM  "LVM" for LVM ROM and "VLM" for VLM ROM

OPT_ParaLam ='./LamOptResultC'+Geo+BC+'_'+ROMMod+'_clip.dat'

dam = True  # damage both in warp ([90,90,0]) and in weft ([0,90,0])
damMode_Mtx = 10  # 2: LCN; 10: Sigmoid
damPara_Mtx = [700.0, 0.007, 0.0]  # non-local damage value, used for validation purpose
damPara_IncYarn = [0.6, 10.0, 4493.0, 0.6, 5.0]  # non-local damage value, used for validation purpose

Write_Prop_Reduce(Geo, ROMMod, OPT_ParaLam, dam, damMode_Mtx, damPara_Mtx, damPara_IncYarn)

order = 1
pert = False
microNum = 2000
TensileWoven(Geo, BC, ROMMod, order, pert, microNum)




