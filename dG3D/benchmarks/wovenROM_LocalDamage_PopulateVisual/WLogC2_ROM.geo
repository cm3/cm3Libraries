//ellips geomtry information ----------
b0 = 0.04875;
a = 0.12/b0/3.14159265;
e1 = 0.08;
e2 = 0.005;
L = 4*a + 2*e1;
b = b0*1.1;

pCL = L/15.; // panel characteristic length (control element size)
sspTL = 2; // sub sub-panel transfinite line value (control nodes number by line)

//
//////////////////////////
//    CompositePanel	//
//////////////////////////
// GEOMETRY
// panel
Point(1)={0., -L/2, -2.0*b-e2, pCL};
Point(2)={L, -L/2, -2.0*b-e2, pCL};
Point(3)={L, L/2, -2.0*b-e2, pCL};
Point(4)={0., L/2, -2.0*b-e2, pCL};

Line(1)={1,2};
Line(2)={2,3};
Line(3)={3,4};
Line(4)={4,1};

// surfaces
Line Loop(1)={1,2,3,4};
Plane Surface(1)={1};

// replace unstructured grid by structured grid (with controlled nodes number by line)
Transfinite Line {1,2,3,4} = sspTL Using Progression 1;

// transfinite surface guarantee a structured grid on a surface
Transfinite Surface {1};

// replace tri/tetra by quad/hexa elements
Recombine Surface{1};

// extrusion of surfaces along z direction to create volumes
my_UC[] = Extrude {0.0, 0.0, 4.0*b+2.0*e2} {Surface{1}; Layers{1}; Recombine;};


//--------------BC surfaces ---------------

tolBB = 3.e-3;

xMin = 0.;
xMax = L;
yMin = -L/2.;
yMax = L/2.;
zMin = -2.*b-e2;
zMax = 2.*b+e2;


surfX0() = Surface In BoundingBox {xMin-tolBB, yMin-tolBB, zMin-tolBB, xMin+tolBB, yMax+tolBB, zMax+tolBB};
Printf("Surface X = 0 ", surfX0());
surfXL() = Surface In BoundingBox {xMax-tolBB, yMin-tolBB, zMin-tolBB, xMax+tolBB, yMax+tolBB, zMax+tolBB};
Printf("Surface X = L ", surfXL());

surfY0() = Surface In BoundingBox {xMin-tolBB, yMin-tolBB, zMin-tolBB, xMax+tolBB, yMin+tolBB, zMax+tolBB};
Printf("Surface Y = 0 ", surfY0());
surfYL() = Surface In BoundingBox {xMin-tolBB, yMax-tolBB, zMin-tolBB, xMax+tolBB, yMax+tolBB, zMax+tolBB};
Printf("Surface Y = L ", surfYL());

surfZ0() = Surface In BoundingBox {xMin-tolBB, yMin-tolBB, zMin-tolBB, xMax+tolBB, yMax+tolBB, zMin+tolBB};
Printf("Surface Z = 0 ", surfZ0());
surfZL() = Surface In BoundingBox {xMin-tolBB, yMin-tolBB, zMax-tolBB, xMax+tolBB, yMax+tolBB, zMax+tolBB};
Printf("Surface Z = L ", surfZL());

edZ0Y0() = Line In BoundingBox {xMin-tolBB, yMin-tolBB, zMin-tolBB, xMax+tolBB, yMin+tolBB, zMin+tolBB};
Printf("Edge Z = 0 Y=0", edZ0Y0());
edZ0YL() = Line In BoundingBox {xMin-tolBB, yMax-tolBB, zMin-tolBB,  xMax+tolBB, yMax+tolBB, zMin+tolBB};
Printf("Edge Z = 0 Y=L", edZ0YL());
edZLY0() = Line In BoundingBox {xMin-tolBB, yMin-tolBB, zMax-tolBB, xMax+tolBB, yMin+tolBB, zMax+tolBB};
Printf("Edge Z = L Y=0", edZLY0());
edZLYL() = Line In BoundingBox {xMin-tolBB, yMax-tolBB, zMax-tolBB, xMax+tolBB, yMax+tolBB, zMax+tolBB};
Printf("Edge Z = L Y=L", edZLYL());

edX0Y0() = Line In BoundingBox {xMin-tolBB, yMin-tolBB, zMin-tolBB, xMin+tolBB, yMin+tolBB, zMax+tolBB};
Printf("Edge X = 0 Y=0", edX0Y0());
edX0YL() = Line In BoundingBox {xMin-tolBB, yMax-tolBB, zMin-tolBB, xMin+tolBB, yMax+tolBB, zMax+tolBB};
Printf("Edge X = 0 Y=L", edX0YL());
edXLY0() = Line In BoundingBox {xMax-tolBB, yMin-tolBB, zMin-tolBB, xMax+tolBB, yMin+tolBB, zMax+tolBB};
Printf("Edge X = L Y=0", edX0Y0());
edXLYL() = Line In BoundingBox {xMax-tolBB, yMax-tolBB, zMin-tolBB, xMax+tolBB, yMax+tolBB, zMax+tolBB};
Printf("Edge X = L Y=L", edXLYL());

edX0Z0() = Line In BoundingBox {xMin-tolBB, yMin-tolBB, zMin-tolBB, xMin+tolBB, yMax+tolBB, zMin+tolBB};
Printf("Edge X = 0 Z=0", edX0Z0());
edX0ZL() = Line In BoundingBox {xMin-tolBB, yMin-tolBB, zMax-tolBB, xMin+tolBB, yMax+tolBB, zMax+tolBB};
Printf("Edge X = 0 Y=L", edX0ZL());
edXLZ0() = Line In BoundingBox {xMax-tolBB, yMin-tolBB, zMin-tolBB, xMax+tolBB, yMax+tolBB, zMin+tolBB};
Printf("Edge X = L Y=0", edX0Z0());
edXLZL() = Line In BoundingBox {xMax-tolBB, yMin-tolBB, zMax-tolBB, xMax+tolBB, yMax+tolBB, zMax+tolBB};
Printf("Edge X = L Y=L", edXLZL());


ptX0Y0Z0() = Point In BoundingBox {xMin-tolBB, yMin-tolBB, zMin-tolBB, xMin+tolBB, yMin+tolBB, zMin+tolBB};
Printf("Point X = 0 Y=0 Z=0", ptX0Y0Z0());
ptXLY0Z0() = Point In BoundingBox {xMax-tolBB, yMin-tolBB, zMin-tolBB, xMax+tolBB, yMin+tolBB, zMin+tolBB};
Printf("Point X = L Y=0 Z=0", ptXLY0Z0());
ptX0YLZ0() = Point In BoundingBox {xMin-tolBB, yMax-tolBB, zMin-tolBB, xMin+tolBB, yMax+tolBB, zMin+tolBB};
Printf("Point X = 0 Y=0 Z=0", ptX0Y0Z0());
ptXLYLZ0() = Point In BoundingBox {xMax-tolBB, yMax-tolBB, zMin-tolBB, xMax+tolBB, yMax+tolBB, zMin+tolBB};
Printf("Point X = L Y=0 Z=0", ptXLY0Z0());

ptX0Y0ZL() = Point In BoundingBox {xMin-tolBB, yMin-tolBB, zMax-tolBB, xMin+tolBB, yMin+tolBB, zMax+tolBB};
Printf("Point X = 0 Y=0 Z=L", ptX0Y0ZL());
ptXLY0ZL() = Point In BoundingBox {xMax-tolBB, yMin-tolBB, zMax-tolBB, xMax+tolBB, yMin+tolBB, zMax+tolBB};
Printf("Point X = L Y=0 Z=L", ptXLY0ZL());
ptX0YLZL() = Point In BoundingBox {xMin-tolBB, yMax-tolBB, zMax-tolBB, xMin+tolBB, yMax+tolBB, zMax+tolBB};
Printf("Point X = 0 Y=0 Z=L", ptX0Y0ZL());
ptXLYLZL() = Point In BoundingBox {xMax-tolBB, yMax-tolBB, zMax-tolBB, xMax+tolBB, yMax+tolBB, zMax+tolBB};
Printf("Point X = L Y=0 Z=L", ptXLY0ZL());

Physical Surface(100) = { surfX0() }; //x=0
Physical Surface(101) = { surfXL() }; //x=L
Physical Surface(110) = { surfY0()  }; //y=0
Physical Surface(111) = { surfYL() }; //y=L
Physical Surface(120) = { surfZ0() }; //z=0
Physical Surface(121) = { surfZL() }; //z=L

Physical Line(100) = { edZ0Y0() }; //y = 0, z = 0
Physical Line(101) = { edZ0YL() }; //y = L, z = 0
Physical Line(102) = { edZLY0() }; //y = 0, z = L
Physical Line(103) = { edZLYL() }; //y = L, z = L
Physical Line(110) = { edX0Z0() }; //x = 0, z = 0
Physical Line(111) = { edX0ZL() }; //x = 0, z = L
Physical Line(112) = { edXLZ0() }; //x = L, z = 0
Physical Line(113) = { edXLZL() }; //x = L, z = L
Physical Line(120) = { edX0Y0() }; //x = 0, y = 0
Physical Line(121) = { edX0YL() }; //x = 0, y = L
Physical Line(122) = { edXLY0() }; //x = L, y = 0
Physical Line(123) = { edXLYL() }; //x = L, y = L


Physical Point(1)= { ptX0Y0Z0() };
Physical Point(2)= { ptXLY0Z0() };
Physical Point(3)= { ptXLYLZ0() };
Physical Point(4)= { ptX0YLZ0() };
Physical Point(5)= { ptX0Y0ZL() };
Physical Point(6)= { ptXLY0ZL() };
Physical Point(7)= { ptXLYLZL() };
Physical Point(8)= { ptX0YLZL() };


Physical Volume(51) ={1};

