# coding-Utf-8-*-
import numpy as npy
import scipy as spy
#from gmshpy import *
#from dG3Dpy import *
# from dG3DpyDebug import*
import sys
import pickle


################## VM  #######################
def Write_prop_VM(Vm, VI, Vg, ar, R, dam, damMode_Mtx, damPara_Mtx, damPara_IncYarn):
    E0 = 4669.0
    nu0 = 0.39
    sy0 = 32.0
    H0 = 300.0  # 150.0
    m0 = 100.0  # 300.0
    #Dmod = 10  # D_LCN 2 / D_SIGMOID 10
    '''if damMode_Mtx == 2:
        Beta = 0.21  # 1.0
        kd = 2.0  # 1.3
        p0 = 0.0
    elif damMode_Mtx == 10:
        Beta = 700.0
        kd = 0.007
        p0 = 0.0'''

    E_fT = 12.9e3
    E_fL = 231.0e3
    nu_fLT = 0.3
    nu_fTL = E_fT * nu_fLT / E_fL
    nu_fTL = 0.01675
    nu_fTT = 0.46
    GTT = 4.45e3
    GLT = 11.3e3
    #lf = 0.624  # 0.6
    #l0 = 10.0
    #s0 = 3493.0  # 3493.0 # 3783363379.756531 (3783.36) # 4493
    #alpha = 0.6  # 0.6 # 0.5831884147991657 (0.58) # 0.6
    #gamma = 3.0  # 3.0 # 4.677314141907828 (4.68) # 5.0
    nf = 1000000
    GI = 0.0

    Nph = len(Vg) + 1  # nb of grains (one pure matrix + len(Vg) fibres)
    GrainProp = 48  # nb of material parameters for each grain of MT
    k_start = 4 + Nph * 2 + 13  # start id for grains: 13-nb of matrix parameter
    prop_len = k_start + (Nph - 1) * GrainProp + 3  # nb of USER MATERIAL CONSTANTS

    ######### material properties for VT
    iddam = 0
    idclength = 0
    iddam_Mpure = 0  # 9
    iddam_Fib = 0  # 34
    iddam_M_0 = 0  # 15
    iddam_M_90 = 0  # 15
    if dam:
        iddam_Mpure = 9
        iddam_Fib = 34
        iddam_M_0 = 15
        iddam_M_90 = 15
        


    ### PRINT IN A FILE FOR MFH ####
    fid = open('propROM.i01', "w")
    fid.write("%i\n" % int(Nph * 200))  # DEPVAR  (nb max. of state variables allocated and can be used)
    fid.write("%i\n" % int(prop_len))  # nb of USER MATERIAL CONSTANTS
    fid.write("%i\n" % int(3))  # composite material model (3: VT)
    fid.write("%i\n" % int(iddam))  # position for composite local damage parameters (+3 / =0 if no damage)
    fid.write("%i\n" % int(idclength))  # position for composite non-local damage parameters (+3 / =0 if no damage)
    fid.write("%i\n" % int(Nph))  # nb of pseudo grains
    fid.write("%i\n" % int(4 + Nph * 2))  # position for Mtx p-g properties in props vector (+3)
    fid.write("%.9f\n" % float(Vm))  # Mtx p-g volume fraction
    for i in range(Nph - 1):
        fid.write("%i\n" % int(k_start + i * GrainProp))  # position for Fib-Mtx p-g properties in props vector (+3)
        fid.write("%.9f\n" % float(Vg[i]))  # Fib-Mtx p-g volume fraction

    # Mtx p-g material properties
    fid.write("%i\n" % int(2))  # Mtx p-g material model (2: EP)
    fid.write("%i\n" % int(iddam_Mpure))  # position for Mtx p-g local damage parameters (+3 / =0 if no damage)
    fid.write("%i\n" % int(idclength))  # position for Mtx p-g non-local damage parameters (+3 / =0 if no damage)
    fid.write("%.9f\n" % float(E0))  # matrix Young's modulus E0 << Set 0.0 for random variable >>
    fid.write("%.9f\n" % float(nu0))  # matrix Poisson's ratio nu0 << Set 0.0 for random variable >>
    fid.write("%.9f\n" % float(sy0))  # matrix initial yield stress sigmaYm0 << Set 0.0 for random variable >>
    fid.write("%i\n" % int(2))  # matrix hardening law model (2: H_EXP)
    fid.write("%.9f\n" % float(H0))  # matrix hardening modulus h0 << Set 0.0 for random vaiable >>
    fid.write("%.9f\n" % float(m0))  # matrix hardening exponent m0 << Set 0.0 for random vaiable >>
    fid.write("%i\n" % int(damMode_Mtx))  # matrix damage model (10 SIGMOID: could be local or non local damage model)
    fid.write("%.9f\n" % float(damPara_Mtx[0]))  # matrix damage exponent beta0 (for damage model 10)
    fid.write("%.9f\n" % float(damPara_Mtx[1]))  # matrix damage initial strain pc0 (for model 10)
    fid.write("%.9f\n" % float(damPara_Mtx[2]))  # (pc0=0 for damage model 2, not used in damage model 10)

    # Fib-Mtx p-g material properties
    for i in range(Nph - 1):
        fid.write("%i\n" % int(8))  # Fib-Mtx p-g material model (8: MFH Fisrst order secant)
        fid.write("%i\n" % int(iddam))  # position for Fib-Mtx p-g local damage parameters (+3 / =0 if no damage)
        fid.write("%i\n" % int(idclength))  # position for Fib-Mtx p-g non-local damage parameters (+3 / =0 if no damage)
        fid.write("%i\n" % int(6))  # position for matrix properties in props vector (+3)
        fid.write("%i\n" % int(19))  # position for inclusion properties in props vector (+3)
        fid.write("%i\n" % int(42))  # position for geometry parameters of inclusion in props vector (+3)
        fid.write("%i\n" % int(2))  # matrix material model (2: EP)
        if R[i][0] < 1.0e-3:
            fid.write("%i\n" % int(iddam_M_90))  # position for matrix local damage parameters (+3 / =0 if no damage)
        else:
            fid.write("%i\n" % int(iddam_M_0))  # position for matrix local damage parameters (+3 / =0 if no damage)
        fid.write("%i\n" % int(idclength))  # position for matrix non-local damage parameters (+3 / =0 if no damage)
        fid.write("%.9f\n" % float(E0))  # matrix Young's modulus E0 << Set 0.0 for random variable >>
        fid.write("%.9f\n" % float(nu0))  # matrix Poisson's ratio nu0 << Set 0.0 for random variable >>
        fid.write("%.9f\n" % float(sy0))  # matrix initial yield stress sigmaYm0 << Set 0.0 for random variable >>
        fid.write("%i\n" % int(2))  # matrix hardening law model (2: H_EXP)
        fid.write("%.9f\n" % float(H0))  # matrix hardening modulus h0 << Set 0.0 for random vaiable >>
        fid.write("%.9f\n" % float(m0))  # matrix hardening exponent m0 << Set 0.0 for random vaiable >>
        fid.write("%i\n" % int(damMode_Mtx))  # matrix damage model (10 SIGMOID: could be local or non local damage model)
        fid.write("%.9f\n" % float(damPara_Mtx[0]))  # matrix damage exponent beta0 (for damage model 10)
        fid.write("%.9f\n" % float(damPara_Mtx[1]))  # matrix damage initial strain pc0 (for model 10)
        fid.write("%.9f\n" % float(damPara_Mtx[2]))  # (pc0=0 for damage model 2, not used in damage model 10)
        fid.write("%i\n" % int(7))  # inclusion material model (ANEL 7)
        fid.write("%i\n" % int(iddam_Fib))  # position for inclusion local damage parameters (+3 / =0 if no damage)
        fid.write("%i\n" % int(idclength))  # position for inclusion non-local damage parameters (+3 / =0 if no damage)
        fid.write("%.9f\n" % float(E_fT))  # inclusion Young's modulus E1 (ETI)
        fid.write("%.9f\n" % float(E_fT))  # inclusion Young's modulus E2 (ETI)
        fid.write("%.9f\n" % float(E_fL))  # inclusion Young's modulus E3 (ELI)
        fid.write("%.9f\n" % float(nu_fTT))  # inclusion Poisson's ratio nu12 (nuTTI)
        fid.write("%.9f\n" % float(nu_fTL))  # inclusion minor Poisson's ratio nu13 (nuLTI)
        fid.write("%.9f\n" % float(nu_fTL))  # inclusion minor Poisson's ratio nu23 (nuLTI)
        fid.write("%.9f\n" % float(GTT))  # inclusion shear modulus G12 (muTTI)
        fid.write("%.9f\n" % float(GLT))  # inclusion shear modulus G13 (muLTI)
        fid.write("%.9f\n" % float(GLT))  # inclusion shear modulus G23 (muLTI)
        fid.write("%.9f\n" % float(R[i][0]))  # inclusion anisotropic Euler angles << Set >180.0 for random vaiable >>
        fid.write("%.9f\n" % float(R[i][1]))  # inclusion anisotropic Euler angles << Set >180.0 for random vaiable >>
        fid.write("%.9f\n" % float(R[i][2]))  # inclusion anisotropic Euler angles << Set >180.0 for random vaiable >>
        fid.write("%i\n" % int(7))  # inclusion damage model (7 Weilbull: could be local or non local damage model)
        fid.write("%.9f\n" % float(damPara_IncYarn[0]))  # inclusion local damage parameter
        fid.write("%.9f\n" % float(damPara_IncYarn[1]))  # inclusion local damage parameter
        fid.write("%.9f\n" % float(damPara_IncYarn[2]))  # inclusion local damage parameter
        fid.write("%.9f\n" % float(damPara_IncYarn[3]))  # inclusion local damage parameter
        fid.write("%.9f\n" % float(damPara_IncYarn[4]))  # inclusion local damage parameter
        fid.write("%i\n" % int(nf))  # inclusion local damage parameter (nb of fibers)
        fid.write("%.9f\n" % float(GI))  # inclusion non-local damage parameter GcI (=0.0 if local damage model)
        fid.write("%.9f\n" % float(VI))  # inclusion volume fraction << Set >1.0 for random vaiable >>
        fid.write("%.9f\n" % float(ar[i]))  # inclusion aspect ration ar1 << Set 0.0 for random vaiable >>
        fid.write("%.9f\n" % float(ar[i]))  # inclusion aspect ration ar1 << Set 0.0 for random vaiable >>
        fid.write("%.9f\n" % float(R[i][0]))  # inclusion anisotropic Euler angles << Set >180.0 for random vaiable >>
        fid.write("%.9f\n" % float(R[i][1]))  # inclusion anisotropic Euler angles << Set >180.0 for random vaiable >>
        fid.write("%.9f\n" % float(R[i][2]))  # inclusion anisotropic Euler angles << Set >180.0 for random vaiable >>

    fid.close()

    return


##########################  LVM  ##################################
def Write_prop_LVM(Vm, VI, Vg, ar, R, dam, damMode_Mtx, damPara_Mtx, damPara_IncYarn):
    # basic material properties
    E0 = 4669.0
    nu0 = 0.39
    sy0 = 32.0
    H0 = 300.0  # 150.0
    m0 = 100.0  # 300.0
    #Dmod = 10  # D_LCN 2 / D_SIGMOID 10
    '''if damMode_Mtx == 2:
        Beta = 0.21  # 1.0
        kd = 2.0  # 1.3
        p0 = 0.0
    elif damMode_Mtx == 10:
        Beta = 700.0
        kd = 0.007
        p0 = 0.0'''

    E_fT = 12.9e3
    E_fL = 231.0e3
    nu_fLT = 0.3
    nu_fTL = E_fT * nu_fLT / E_fL
    nu_fTL = 0.01675
    nu_fTT = 0.46
    GTT = 4.45e3
    GLT = 11.3e3
    #lf = 0.624  # 0.6
    #l0 = 10.0
    #s0 = 3493.0  # 3493.0 # 3783363379.756531 (3783.36) # 4493
    #alpha = 0.6  # 0.6 # 0.5831884147991657 (0.58) # 0.6
    #gamma = 3.0  # 3.0 # 4.677314141907828 (4.68) # 5.0
    nf = 1000000
    GI = 0.0

    euler0 = npy.zeros(3)

    nbLam = 1  # nb of laminates
    LamProp = 11  # nb of laminate parameters
    EP_Prop = 13  # nb of pure matrix ply parameters
    Nph = len(Vg)  # nb of grains
    GrainProp = 48  # nb of material parameters for each grain of MT
    
    idLA_0 = 11  # position for 1st layer properties in props vector (+3)
    idLB_0 = idLA_0 + EP_Prop  # position for 2nd layer properties in props vector (+3)

    #### lam_2pl volume
    VA0 = Vm  # volume fraction for pure matrix ply
    VB0 = 1.0 - Vm  # volume fraction for fibre grains ply (yarns)

    ####
    k_start = 4 + Nph * 2  # start id for grains
    prop_len = nbLam * LamProp + EP_Prop + k_start + Nph * GrainProp + 3  # nb of USER MATERIAL CONSTANTS

    ######### material properties for VT
    iddam_lam = 0
    idclength = 0
    ideuler = 8
    iddam_Mpure = 0  # 9
    iddam_Fib = 0  # 34
    iddam_M_0 = 0  # 15
    iddam_M_90 = 0  # 15
    if dam:
        iddam_Mpure = 9
        iddam_Fib = 34
        iddam_M_0 = 15
        iddam_M_90 = 15 
        


    ### PRINT IN A FILE FOR MFH ####
    fid = open('propROM.i01', "w")
    fid.write("%i\n" % int(Nph * 200 + nbLam * 40))  # DEPVAR  (nb max. of state variables allocated and can be used)
    fid.write("%i\n" % int(prop_len))  # nb of USER MATERIAL CONSTANTS
    fid.write("%i\n" % int(14))  # composite material model (14: LAM_2PLY)
    fid.write("%i\n" % int(iddam_lam))  # position for composite local damage parameters (+3 / =0 if no damage)
    fid.write("%i\n" % int(idclength))  # position for composite non-local damage parameters (+3 / =0 if no damage)
    fid.write("%i\n" % int(ideuler))  # position for composite anisotropic Euler angles (+3)
    fid.write("%i\n" % int(idLA_0))  # position for 1st layer properties in props vector (+3)
    fid.write("%.9f\n" % float(VA0))  # 1st layer matrix volume fraction
    fid.write("%i\n" % int(idLB_0))  # position for 2nd layer properties in props vector (+3)
    fid.write("%.9f\n" % float(VB0))  # 1st layer matrix volume fraction
    for i in range(3):
        fid.write("%.9f\n" % float(euler0[i]))  # composite anisotropic Euler angles << Set >180.0 for random vaiable >>

    #  matrix material properties
    fid.write("%i\n" % int(2))  # pure mtx laminate material model (2: EP)
    fid.write("%i\n" % int(iddam_Mpure))  # position for Mtx laminate local damage parameters (+3 / =0 if no damage)
    fid.write("%i\n" % int(idclength))  # position for Mtx laminate non-local damage parameters (+3 / =0 if no damage)
    fid.write("%.9f\n" % float(E0))  # matrix Young's modulus E0 << Set 0.0 for random variable >>
    fid.write("%.9f\n" % float(nu0))  # matrix Poisson's ratio nu0 << Set 0.0 for random variable >>
    fid.write("%.9f\n" % float(sy0))  # matrix initial yield stress sigmaYm0 << Set 0.0 for random variable >>
    fid.write("%i\n" % int(2))  # matrix hardening law model (2: H_EXP)
    fid.write("%.9f\n" % float(H0))  # matrix hardening modulus h0 << Set 0.0 for random vaiable >>
    fid.write("%.9f\n" % float(m0))  # matrix hardening exponent m0 << Set 0.0 for random vaiable >>
    fid.write("%i\n" % int(damMode_Mtx))  # matrix damage model (10 SIGMOID: could be local or non local damage model)
    fid.write("%.9f\n" % float(damPara_Mtx[0]))  # matrix damage exponent beta0 (for damage model 10)
    fid.write("%.9f\n" % float(damPara_Mtx[1]))  # matrix damage initial strain pc0 (for model 10)
    fid.write("%.9f\n" % float(damPara_Mtx[2]))  # (pc0=0 for damage model 2, not used in damage model 10)

    ### voigt fiber grains
    fid.write("%i\n" % int(3))  # fibre grains laminate material model (VT 3)
    fid.write("%i\n" % int(iddam_lam))  # position for fibre grains laminate local damage parameters (+3 / =0 if no damage)
    fid.write("%i\n" % int(idclength))  # position for fibre grains laminate non-local damage parameters (+3 / =0 if no damage)
    fid.write("%i\n" % int(Nph))  # nb of fibre grains
    for i in range(Nph):
        fid.write("%i\n" % int(k_start + i * GrainProp))  # position for 1st half of fibre grains properties in props vector (+3)
        fid.write("%.9f\n" % float(Vg[i]))  # 1st half of fibre grains volume fraction
    for i in range(Nph):
        fid.write("%i\n" % int(8))  # fibre grains laminate material model (8: MFH Fisrst order secant)
        fid.write("%i\n" % int(iddam_lam))  # position for fibre grains local damage parameters (+3 / =0 if no damage)
        fid.write("%i\n" % int(idclength))  # position for fibre grains non-local damage parameters (+3 / =0 if no damage)
        fid.write("%i\n" % int(6))  # position for matrix properties in props vector (+3)
        fid.write("%i\n" % int(19))  # position for inclusion properties in props vector (+3)
        fid.write("%i\n" % int(42))  # position for geometry parameters of inclusion in props vector (+3)
        fid.write("%i\n" % int(2))  # matrix material model (2: EP)
        if R[i][0] < 1.0e-3:
            fid.write("%i\n" % int(iddam_M_90))  # position for matrix local damage parameters (+3 / =0 if no damage)
        else:
            fid.write("%i\n" % int(iddam_M_0))  # position for matrix local damage parameters (+3 / =0 if no damage)
        fid.write("%i\n" % int(idclength))  # position for matrix non-local damage parameters (+3 / =0 if no damage)
        fid.write("%.9f\n" % float(E0))  # matrix Young's modulus E0 << Set 0.0 for random variable >>
        fid.write("%.9f\n" % float(nu0))  # matrix Poisson's ratio nu0 << Set 0.0 for random variable >>
        fid.write("%.9f\n" % float(sy0))  # matrix initial yield stress sigmaYm0 << Set 0.0 for random variable >>
        fid.write("%i\n" % int(2))  # matrix hardening law model (2: H_EXP)
        fid.write("%.9f\n" % float(H0))  # matrix hardening modulus h0 << Set 0.0 for random vaiable >>
        fid.write("%.9f\n" % float(m0))  # matrix hardening exponent m0 << Set 0.0 for random vaiable >>
        fid.write("%i\n" % int(damMode_Mtx))  # matrix damage model (10 SIGMOID: could be local or non local damage model)
        fid.write("%.9f\n" % float(damPara_Mtx[0]))  # matrix damage exponent beta0 (for damage model 10)
        fid.write("%.9f\n" % float(damPara_Mtx[1]))  # matrix damage initial strain pc0 (for model 10)
        fid.write("%.9f\n" % float(damPara_Mtx[2]))  # (pc0=0 for damage model 2, not used in damage model 10)
        fid.write("%i\n" % int(7))  # inclusion material model (ANEL 7)
        fid.write("%i\n" % int(iddam_Fib))  # position for inclusion local damage parameters (+3 / =0 if no damage)
        fid.write("%i\n" % int(idclength))  # position for inclusion non-local damage parameters (+3 / =0 if no damage)
        fid.write("%.9f\n" % float(E_fT))  # inclusion Young's modulus E1 (ETI)
        fid.write("%.9f\n" % float(E_fT))  # inclusion Young's modulus E2 (ETI)
        fid.write("%.9f\n" % float(E_fL))  # inclusion Young's modulus E3 (ELI)
        fid.write("%.9f\n" % float(nu_fTT))  # inclusion Poisson's ratio nu12 (nuTTI)
        fid.write("%.9f\n" % float(nu_fTL))  # inclusion minor Poisson's ratio nu13 (nuLTI)
        fid.write("%.9f\n" % float(nu_fTL))  # inclusion minor Poisson's ratio nu23 (nuLTI)
        fid.write("%.9f\n" % float(GTT))  # inclusion shear modulus G12 (muTTI)
        fid.write("%.9f\n" % float(GLT))  # inclusion shear modulus G13 (muLTI)
        fid.write("%.9f\n" % float(GLT))  # inclusion shear modulus G23 (muLTI)
        fid.write("%.9f\n" % float(R[i][0]))  # inclusion anisotropic Euler angles << Set >180.0 for random vaiable >>
        fid.write("%.9f\n" % float(R[i][1]))  # inclusion anisotropic Euler angles << Set >180.0 for random vaiable >>
        fid.write("%.9f\n" % float(R[i][2]))  # inclusion anisotropic Euler angles << Set >180.0 for random vaiable >>
        fid.write("%i\n" % int(7))  # inclusion damage model (7 Weilbull: could be local or non local damage model)
        fid.write("%.9f\n" % float(damPara_IncYarn[0]))  # inclusion local damage parameter
        fid.write("%.9f\n" % float(damPara_IncYarn[1]))  # inclusion local damage parameter
        fid.write("%.9f\n" % float(damPara_IncYarn[2]))  # inclusion local damage parameter
        fid.write("%.9f\n" % float(damPara_IncYarn[3]))  # inclusion local damage parameter
        fid.write("%.9f\n" % float(damPara_IncYarn[4]))  # inclusion local damage parameter
        fid.write("%i\n" % int(nf))  # inclusion local damage parameter (nb of fibers)
        fid.write("%.9f\n" % float(GI))  # inclusion non-local damage parameter GcI (=0.0 if local damage model)
        fid.write("%.9f\n" % float(VI))  # inclusion volume fraction << Set >1.0 for random vaiable >>
        fid.write("%.9f\n" % float(ar[i]))  # inclusion aspect ration ar1 << Set 0.0 for random vaiable >>
        fid.write("%.9f\n" % float(ar[i]))  # inclusion aspect ration ar1 << Set 0.0 for random vaiable >>
        fid.write("%.9f\n" % float(R[i][0]))  # inclusion anisotropic Euler angles << Set >180.0 for random vaiable >>
        fid.write("%.9f\n" % float(R[i][1]))  # inclusion anisotropic Euler angles << Set >180.0 for random vaiable >>
        fid.write("%.9f\n" % float(R[i][2]))  # inclusion anisotropic Euler angles << Set >180.0 for random vaiable >>

    fid.close()

    return


#######################  VLM  ########################################
def Write_prop_VLM(VI, Vg, Rg, Vm, Rf, ar, dam, damMode_Mtx, damPara_Mtx, damPara_IncYarn):
    # basic material properties
    E0 = 4669.0
    nu0 = 0.39
    sy0 = 32.0
    H0 = 300.0  # 150.0
    m0 = 100.0  # 300.0
    #Dmod = 2  # 10
    '''if damMode_Mtx == 2:
        Beta = 0.21  # 1.0
        kd = 2.0  # 1.3
        p0 = 0.0
    elif damMode_Mtx == 10:
        Beta = 700.0
        kd = 0.007
        p0 = 0.0'''

    E_fT = 12.9e3
    E_fL = 231.0e3
    nu_fLT = 0.3
    nu_fTL = E_fT * nu_fLT / E_fL
    nu_fTL = 0.01675
    nu_fTT = 0.46
    GTT = 4.45e3
    GLT = 11.3e3
    #lf = 0.624  # 0.6
    #l0 = 10.0
    #s0 = 3493.0  # 3493.0 # 3783363379.756531 (3783.36) # 4493
    #alpha = 0.6  # 0.6 # 0.5831884147991657 (0.58) # 0.6
    #gamma = 3.0  # 3.0 # 4.677314141907828 (4.68) # 5.0
    nf = 1000000
    GI = 0.0

    Nph = len(Vg)  # nb of grains
    nbLam = len(Vg)  # nb of laminates
    LamProp = 11  # nb of laminate parameters
    EP_Prop = 13  # nb of pure matrix ply parameters
    MFH_Prop = 48  # nb of fibre grains ply parameters
    GrainProp = 11 + 13 + 48  # nb of material parameter for each grain of MT
    
    idLA_0 = 11  # position for 1st layer properties in props vector (+3)
    idLB_0 = idLA_0 + EP_Prop  # position for 2nd layer properties in props vector (+3)

    ####
    k_start = 4 + 2 * Nph  # start id for grains
    prop_len = k_start + Nph * GrainProp + 3  # nb of USER MATERIAL CONSTANTS

    ######### material properties for VT
    iddam = 0
    iddam_lam = 0
    idclength = 0
    ideuler = 8
    iddam_Mpure = 0  # 9
    iddam_Fib = 0  # 34
    iddam_M_0 = 0  # 15
    iddam_M_90 = 0  # 15
    if dam:
        iddam_Mpure = 9
        iddam_Fib = 34
        iddam_M_0 = 15
        iddam_M_90 = 15



    ### PRINT IN A FILE FOR MFH ####
    fid = open('propROM.i01', "w")
    fid.write("%i\n" % int(2 * Nph * 200 + 2 * nbLam * 40))  # DEPVAR  (nb max. of state variables allocated and can be used)
    fid.write("%i\n" % int(prop_len))  # nb of USER MATERIAL CONSTANTS
    # Votgt law
    fid.write("%i\n" % int(3))  # composite material model (3: VT)
    fid.write("%i\n" % int(iddam))  # position for composite local damage parameters (+3 / =0 if no damage)
    fid.write("%i\n" % int(idclength))  # position for composite (used for matrix) non-local damage parameters (+3 / =0 if no damage) ?
    fid.write("%i\n" % int(Nph))  # nb of grains
    for i in range(Nph):
        fid.write("%i\n" % int(k_start + i * GrainProp))  # position for laminate p-g properties in props vector (+3)
        fid.write("%.9f\n" % float(Vg[i]))  # laminate p-g volume fraction
    # laminate grain
    for i in range(Nph):
        fid.write("%i\n" % int(14))  # laminate p-g material model (14: LAM_2PLY)
        fid.write("%i\n" % int(iddam_lam))  # position for laminate p-g local damage parameters (+3 / =0 if no damage)
        fid.write("%i\n" % int(idclength))  # position for laminate p-g non-local damage parameters (+3 / =0 if no damage)
        fid.write("%i\n" % int(ideuler))  # position for laminate p-g anisotropic Euler angles (+3)
        fid.write("%i\n" % int(idLA_0))  # position for 1st layer properties in props vector (+3)
        fid.write("%.9f\n" % float(Vm[i]))  # 1st layer matrix volume fraction
        fid.write("%i\n" % int(idLB_0))  # position for 2nd layer properties in props vector (+3)
        fid.write("%.9f\n" % float(1.0 - Vm[i]))  # 1st layer matrix volume fraction
        for k in range(3):
            fid.write("%.9f\n" % float(Rg[i][k]))  # laminate p-g anisotropic Euler angles << Set >180.0 for random vaiable >>

        #  matrix material properties
        fid.write("%i\n" % int(2))  # Mtx laminate material model (2: EP)
        fid.write("%i\n" % int(iddam_Mpure))  # position for Mtx laminate local damage parameters (+3 / =0 if no damage)
        fid.write("%i\n" % int(idclength))  # position for Mtx laminate non-local damage parameters (+3 / =0 if no damage)
        fid.write("%.9f\n" % float(E0))  # matrix Young's modulus E0 << Set 0.0 for random variable >>
        fid.write("%.9f\n" % float(nu0))  # matrix Poisson's ratio nu0 << Set 0.0 for random variable >>
        fid.write("%.9f\n" % float(sy0))  # matrix initial yield stress sigmaYm0 << Set 0.0 for random variable >>
        fid.write("%i\n" % int(2))  # matrix hardening law model (2: H_EXP)
        fid.write("%.9f\n" % float(H0))  # matrix hardening modulus h0 << Set 0.0 for random vaiable >>
        fid.write("%.9f\n" % float(m0))  # matrix hardening exponent m0 << Set 0.0 for random vaiable >>
        fid.write("%i\n" % int(damMode_Mtx))  # matrix damage model (10 SIGMOID: could be local or non local damage model)
        fid.write("%.9f\n" % float(damPara_Mtx[0]))  # matrix damage exponent beta0 (for damage model 10)
        fid.write("%.9f\n" % float(damPara_Mtx[1]))  # matrix damage initial strain pc0 (for model 10)
        fid.write("%.9f\n" % float(damPara_Mtx[2]))  # (pc0=0 for damage model 2, not used in damage model 10)
        #  MFH ---------------------
        fid.write("%i\n" % int(8))  # Fib-Mtx laminate material model (8: MFH Fisrst order secant)
        fid.write("%i\n" % int(iddam_lam))  # position for Fib-Mtx laminate local damage parameters (+3 / =0 if no damage)
        fid.write("%i\n" % int(idclength))  # position for Fib-Mtx laminate non-local damage parameters (+3 / =0 if no damage)
        fid.write("%i\n" % int(6))  # position for matrix properties in props vector (+3)
        fid.write("%i\n" % int(19))  # position for inclusion properties in props vector (+3)
        fid.write("%i\n" % int(42))  # position for geometry parameters of inclusion in props vector (+3)
        fid.write("%i\n" % int(2))  # matrix material model (2: EP)
        if Rg[i][0] < 1.0e-3:
            fid.write("%i\n" % int(iddam_M_90))  # position for matrix local damage parameters (+3 / =0 if no damage)
        else:
            fid.write("%i\n" % int(iddam_M_0))  # position for matrix local damage parameters (+3 / =0 if no damage)
        fid.write("%i\n" % int(idclength))  # position for matrix non-local damage parameters (+3 / =0 if no damage)
        fid.write("%.9f\n" % float(E0))  # matrix Young's modulus E0 << Set 0.0 for random variable >>
        fid.write("%.9f\n" % float(nu0))  # matrix Poisson's ratio nu0 << Set 0.0 for random variable >>
        fid.write("%.9f\n" % float(sy0))  # matrix initial yield stress sigmaYm0 << Set 0.0 for random variable >>
        fid.write("%i\n" % int(2))  # matrix hardening law model (2: H_EXP)
        fid.write("%.9f\n" % float(H0))  # matrix hardening modulus h0 << Set 0.0 for random vaiable >>
        fid.write("%.9f\n" % float(m0))  # matrix hardening exponent m0 << Set 0.0 for random vaiable >>
        fid.write("%i\n" % int(damMode_Mtx))  # matrix damage model (10 SIGMOID: could be local or non local damage model)
        fid.write("%.9f\n" % float(damPara_Mtx[0]))  # matrix damage exponent beta0 (for damage model 10)
        fid.write("%.9f\n" % float(damPara_Mtx[1]))  # matrix damage initial strain pc0 (for model 10)
        fid.write("%.9f\n" % float(damPara_Mtx[2]))  # (pc0=0 for damage model 2, not used in damage model 10)
        fid.write("%i\n" % int(7))  # inclusion material model (ANEL 7)
        fid.write("%i\n" % int(iddam_Fib))  # position for inclusion local damage parameters (+3 / =0 if no damage)
        fid.write("%i\n" % int(idclength))  # position for inclusion non-local damage parameters (+3 / =0 if no damage)
        fid.write("%.9f\n" % float(E_fT))  # inclusion Young's modulus E1 (ETI)
        fid.write("%.9f\n" % float(E_fT))  # inclusion Young's modulus E2 (ETI)
        fid.write("%.9f\n" % float(E_fL))  # inclusion Young's modulus E3 (ELI)
        fid.write("%.9f\n" % float(nu_fTT))  # inclusion Poisson's ratio nu12 (nuTTI)
        fid.write("%.9f\n" % float(nu_fTL))  # inclusion minor Poisson's ratio nu13 (nuLTI)
        fid.write("%.9f\n" % float(nu_fTL))  # inclusion minor Poisson's ratio nu23 (nuLTI)
        fid.write("%.9f\n" % float(GTT))  # inclusion shear modulus G12 (muTTI)
        fid.write("%.9f\n" % float(GLT))  # inclusion shear modulus G13 (muLTI)
        fid.write("%.9f\n" % float(GLT))  # inclusion shear modulus G23 (muLTI)
        fid.write("%.9f\n" % float(Rf[i][0]))  # inclusion anisotropic Euler angles << Set >180.0 for random vaiable >>
        fid.write("%.9f\n" % float(Rf[i][1]))  # inclusion anisotropic Euler angles << Set >180.0 for random vaiable >>
        fid.write("%.9f\n" % float(Rf[i][2]))  # inclusion anisotropic Euler angles << Set >180.0 for random vaiable >>
        fid.write("%i\n" % int(7))  # inclusion damage model (7 Weilbull: could be local or non local damage model)
        fid.write("%.9f\n" % float(damPara_IncYarn[0]))  # inclusion local damage parameter
        fid.write("%.9f\n" % float(damPara_IncYarn[1]))  # inclusion local damage parameter
        fid.write("%.9f\n" % float(damPara_IncYarn[2]))  # inclusion local damage parameter
        fid.write("%.9f\n" % float(damPara_IncYarn[3]))  # inclusion local damage parameter
        fid.write("%.9f\n" % float(damPara_IncYarn[4]))  # inclusion local damage parameter
        fid.write("%i\n" % int(nf))  # inclusion local damage parameter (nb of fibers)
        fid.write("%.9f\n" % float(GI))  # inclusion non-local damage parameter GcI (=0.0 if local damage model)
        fid.write("%.9f\n" % float(VI))  # inclusion volume fraction << Set >1.0 for random vaiable >>
        fid.write("%.9f\n" % float(ar[i]))  # inclusion aspect ration ar1 << Set 0.0 for random vaiable >>
        fid.write("%.9f\n" % float(ar[i]))  # inclusion aspect ration ar1 << Set 0.0 for random vaiable >>
        fid.write("%.9f\n" % float(Rf[i][0]))  # inclusion anisotropic Euler angles << Set >180.0 for random vaiable >>
        fid.write("%.9f\n" % float(Rf[i][1]))  # inclusion anisotropic Euler angles << Set >180.0 for random vaiable >>
        fid.write("%.9f\n" % float(Rf[i][2]))  # inclusion anisotropic Euler angles << Set >180.0 for random vaiable >>

    fid.close()

    return


#######################################################################################################################
def Write_Prop_Reduce(Geo, ROMMod, OptFile, dam, damMode_Mtx, damPara_Mtx, damPara_IncYarn):
    if Geo == "1":
        VI_yarn = 0.65  # fiber volume fraction in yarn
        Vmtx = 0.5168269  # matrix volume fraction in woven composite (excluding yarn)
    elif Geo == "2":
        VI_yarn = 0.85  # fiber volume fraction in yarn
        Vmtx = 0.35444  # matrix volume fraction in woven composite (excluding yarn)

    if ROMMod == "VLM":
        with open(OptFile, 'rb') as data:
            list_Vg = pickle.load(data, encoding="bytes")
            list_Rg = pickle.load(data, encoding="bytes")
            list_Vm = pickle.load(data, encoding="bytes")
            list_Rf = pickle.load(data, encoding="bytes")
            list_ar = pickle.load(data, encoding="bytes")
    else:
        with open(OptFile, 'rb') as data:
            list_V = pickle.load(data, encoding="bytes")
            list_R = pickle.load(data, encoding="bytes")
            list_VI = pickle.load(data, encoding="bytes")
            list_ar = pickle.load(data, encoding="bytes")

    if ROMMod == "VM":
        list_V = (1.0 - Vmtx) / 2.0 * list_V / list_V.sum()
        Vg = npy.zeros(2 * len(list_V))
        R = npy.zeros((2 * len(list_V), 3))
        ar = npy.zeros(2 * len(list_V))

        for i in range(len(list_V)):
            Vg[2 * i] = list_V[i]
            Vg[2 * i + 1] = list_V[i]
           
            R[2 * i][1] = list_R[i]
            R[2 * i + 1][0] = 90.0
            R[2 * i + 1][1] = list_R[i]

            ar[2 * i] = list_ar[i]
            ar[2 * i + 1] = list_ar[i]

        Write_prop_VM(Vmtx, VI_yarn, Vg, ar, R, dam, damMode_Mtx, damPara_Mtx, damPara_IncYarn)

    elif ROMMod == "LVM":
        list_V = list_V / 2.0 / list_V.sum()
        Vg = npy.zeros(2 * len(list_V))
        R = npy.zeros((2 * len(list_V), 3))
        ar = npy.zeros(2 * len(list_V))

        for i in range(len(list_V)):
            Vg[2 * i] = list_V[i]
            Vg[2 * i + 1] = list_V[i]
                        
            R[2 * i][1] = list_R[i]
            R[2 * i + 1][0] = 90.0
            R[2 * i + 1][1] = list_R[i]
            
            ar[2 * i] = list_ar[i]
            ar[2 * i + 1] = list_ar[i]

        Write_prop_LVM(Vmtx, VI_yarn, Vg, ar, R, dam, damMode_Mtx, damPara_Mtx, damPara_IncYarn)

    elif ROMMod == "VLM":
        list_Vg = list_Vg / 2.0 / list_Vg.sum()
        c = list_Vg * 2.0 * list_Vm
        list_Vm = list_Vm / c.sum() * Vmtx
        Vg = npy.zeros(2 * len(list_Vg))
        Rg = npy.zeros((2 * len(list_Vg), 3))
        Vm = npy.zeros(2 * len(list_Vg))
        Rf = npy.zeros((2 * len(list_Vg), 3))
        ar = npy.zeros(2 * len(list_Vg))

        for i in range(len(list_Vg)):
            Vg[2 * i] = list_Vg[i]
            Vg[2 * i + 1] = list_Vg[i]

            Rg[2 * i][1] = list_Rg[i]
            Rg[2 * i + 1][0] = 90.0
            Rg[2 * i + 1][1] = list_Rg[i]

            Vm[2 * i] = list_Vm[i]
            Vm[2 * i + 1] = list_Vm[i]

            Rf[2 * i][1] = list_Rf[i]
            Rf[2 * i + 1][1] = list_Rf[i]

            ar[2 * i] = list_ar[i]
            ar[2 * i + 1] = list_ar[i]

        Write_prop_VLM(VI_yarn, Vg, Rg, Vm, Rf, ar, dam, damMode_Mtx, damPara_Mtx, damPara_IncYarn)

    return
    
    

    
